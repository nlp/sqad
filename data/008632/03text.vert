<p>
<s>
Bartang	Bartang	k1gInSc1	Bartang
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Б	Б	k?	Б
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
v	v	k7c6	v
Tádžikistánu	Tádžikistán	k1gInSc6	Tádžikistán
(	(	kIx(	(
<g/>
Horský	Horský	k1gMnSc1	Horský
Badachšán	Badachšán	k1gMnSc1	Badachšán
<g/>
)	)	kIx)	)
s	s	k7c7	s
prameny	pramen	k1gInPc7	pramen
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
(	(	kIx(	(
<g/>
Badachšán	Badachšán	k1gMnSc1	Badachšán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
řeky	řeka	k1gFnSc2	řeka
je	být	k5eAaImIp3nS	být
528	[number]	k4	528
km	km	kA	km
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
133	[number]	k4	133
km	km	kA	km
teče	téct	k5eAaImIp3nS	téct
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Bartang	Bartanga	k1gFnPc2	Bartanga
<g/>
.	.	kIx.	.
</s>
<s>
Povodí	povodí	k1gNnSc1	povodí
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
24	[number]	k4	24
700	[number]	k4	700
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
toku	tok	k1gInSc6	tok
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Murgab	Murgab	k1gInSc1	Murgab
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
М	М	k?	М
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
Aksu	Aksus	k1gInSc2	Aksus
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
Aк	Aк	k1gMnSc2	Aк
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc1	průběh
toku	tok	k1gInSc2	tok
==	==	k?	==
</s>
</p>
<p>
<s>
Řeka	řeka	k1gFnSc1	řeka
vytéká	vytékat	k5eAaImIp3nS	vytékat
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Aksu	Aksus	k1gInSc2	Aksus
(	(	kIx(	(
<g/>
Bílá	bílý	k2eAgFnSc1d1	bílá
řeka	řeka	k1gFnSc1	řeka
<g/>
)	)	kIx)	)
z	z	k7c2	z
jezera	jezero	k1gNnSc2	jezero
Čakmaktyn	Čakmaktyna	k1gFnPc2	Čakmaktyna
v	v	k7c6	v
afghánské	afghánský	k2eAgFnSc6d1	afghánská
části	část	k1gFnSc6	část
Wáchánského	Wáchánský	k2eAgNnSc2d1	Wáchánský
údolí	údolí	k1gNnSc2	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
zdroje	zdroj	k1gInSc2	zdroj
sleduje	sledovat	k5eAaImIp3nS	sledovat
severovýchodní	severovýchodní	k2eAgInSc4d1	severovýchodní
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
po	po	k7c6	po
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
Tádžikistánu	Tádžikistán	k1gInSc2	Tádžikistán
změní	změnit	k5eAaPmIp3nS	změnit
na	na	k7c4	na
severní	severní	k2eAgFnSc4d1	severní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
toku	tok	k1gInSc2	tok
na	na	k7c6	na
území	území	k1gNnSc6	území
Tádžikistánu	Tádžikistán	k1gInSc2	Tádžikistán
si	se	k3xPyFc3	se
razí	razit	k5eAaImIp3nP	razit
cestu	cesta	k1gFnSc4	cesta
Pamírským	pamírský	k2eAgNnPc3d1	pamírský
náhorním	náhorní	k2eAgNnPc3d1	náhorní
plató	plató	k1gNnPc3	plató
<g/>
,	,	kIx,	,
přinášejíc	přinášet	k5eAaImSgNnS	přinášet
tak	tak	k6eAd1	tak
životodárnou	životodárný	k2eAgFnSc4d1	životodárná
vláhu	vláha	k1gFnSc4	vláha
do	do	k7c2	do
vyprahlé	vyprahlý	k2eAgFnSc2d1	vyprahlá
vysokohorské	vysokohorský	k2eAgFnSc2d1	vysokohorská
pouště	poušť	k1gFnSc2	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	řeka	k1gFnSc1	řeka
protéká	protékat	k5eAaImIp3nS	protékat
nevelkým	velký	k2eNgNnSc7d1	nevelké
správním	správní	k2eAgNnSc7d1	správní
střediskem	středisko	k1gNnSc7	středisko
východního	východní	k2eAgInSc2d1	východní
Pamíru	Pamír	k1gInSc2	Pamír
Murgabem	Murgab	k1gInSc7	Murgab
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
zapomenuté	zapomenutý	k2eAgFnSc2d1	zapomenutá
lokality	lokalita	k1gFnSc2	lokalita
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nS	změnit
její	její	k3xOp3gNnSc4	její
jméno	jméno	k1gNnSc4	jméno
na	na	k7c4	na
Murgab	Murgab	k1gInSc4	Murgab
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
nabere	nabrat	k5eAaPmIp3nS	nabrat
striktně	striktně	k6eAd1	striktně
východo-západní	východoápadní	k2eAgInSc4d1	východo-západní
směr	směr	k1gInSc4	směr
a	a	k8xC	a
namíří	namířit	k5eAaPmIp3nS	namířit
své	svůj	k3xOyFgFnSc2	svůj
kalné	kalný	k2eAgFnSc2d1	kalná
vody	voda	k1gFnSc2	voda
k	k	k7c3	k
zaledněným	zaledněný	k2eAgMnPc3d1	zaledněný
velikánům	velikán	k1gMnPc3	velikán
Vysokého	vysoký	k2eAgInSc2d1	vysoký
Pamíru	Pamír	k1gInSc2	Pamír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Postupně	postupně	k6eAd1	postupně
si	se	k3xPyFc3	se
klestí	klestit	k5eAaImIp3nS	klestit
cestu	cesta	k1gFnSc4	cesta
stále	stále	k6eAd1	stále
hlubšími	hluboký	k2eAgNnPc7d2	hlubší
údolími	údolí	k1gNnPc7	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
200	[number]	k4	200
km	km	kA	km
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
Murgabu	Murgab	k1gInSc2	Murgab
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
její	její	k3xOp3gNnSc1	její
tok	tok	k1gInSc1	tok
přehrazen	přehrazen	k2eAgInSc1d1	přehrazen
mohutným	mohutný	k2eAgInSc7d1	mohutný
sesuvem	sesuv	k1gInSc7	sesuv
půdy	půda	k1gFnSc2	půda
vyvolaným	vyvolaný	k2eAgNnSc7d1	vyvolané
silným	silný	k2eAgNnSc7d1	silné
zemětřesením	zemětřesení	k1gNnSc7	zemětřesení
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Usojský	Usojský	k2eAgInSc1d1	Usojský
zával	zával	k1gInSc1	zával
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přehrazením	přehrazení	k1gNnSc7	přehrazení
toku	tok	k1gInSc2	tok
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
velkolepé	velkolepý	k2eAgNnSc4d1	velkolepé
Sarezské	Sarezský	k2eAgNnSc4d1	Sarezské
jezero	jezero	k1gNnSc4	jezero
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
nemnoha	nemnoha	k1gFnSc1	nemnoha
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
nevšední	všední	k2eNgNnSc4d1	nevšední
podívanou	podívaná	k1gFnSc7	podívaná
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
však	však	k9	však
vpravdě	vpravdě	k9	vpravdě
časovanou	časovaný	k2eAgFnSc7d1	časovaná
bombou	bomba	k1gFnSc7	bomba
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
další	další	k2eAgNnSc1d1	další
zemětřesení	zemětřesení	k1gNnSc1	zemětřesení
může	moct	k5eAaImIp3nS	moct
porušit	porušit	k5eAaPmF	porušit
nynější	nynější	k2eAgFnSc4d1	nynější
hráz	hráz	k1gFnSc4	hráz
a	a	k8xC	a
způsobit	způsobit	k5eAaPmF	způsobit
tak	tak	k6eAd1	tak
apokalyptické	apokalyptický	k2eAgFnPc4d1	apokalyptická
záplavy	záplava	k1gFnPc4	záplava
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Tádžikistánu	Tádžikistán	k1gInSc2	Tádžikistán
a	a	k8xC	a
příhraničních	příhraniční	k2eAgFnPc6d1	příhraniční
oblastech	oblast	k1gFnPc6	oblast
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
a	a	k8xC	a
Uzbekistánu	Uzbekistán	k1gInSc2	Uzbekistán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
hrází	hráz	k1gFnSc7	hráz
Sarezského	Sarezský	k2eAgNnSc2d1	Sarezské
jezera	jezero	k1gNnSc2	jezero
přibírá	přibírat	k5eAaImIp3nS	přibírat
Murgab	Murgab	k1gInSc1	Murgab
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
řeku	řeka	k1gFnSc4	řeka
Kudaru	Kudar	k1gInSc2	Kudar
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
vodní	vodní	k2eAgInSc1d1	vodní
tok	tok	k1gInSc1	tok
s	s	k7c7	s
názvem	název	k1gInSc7	název
Bartang	Bartang	k1gInSc1	Bartang
tekoucí	tekoucí	k2eAgFnSc1d1	tekoucí
směrem	směr	k1gInSc7	směr
jihozápadním	jihozápadní	k2eAgFnPc3d1	jihozápadní
zbývajících	zbývající	k2eAgInPc2d1	zbývající
133	[number]	k4	133
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Údolí	údolí	k1gNnSc1	údolí
řeky	řeka	k1gFnSc2	řeka
Bartang	Bartanga	k1gFnPc2	Bartanga
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
hustě	hustě	k6eAd1	hustě
osídleno	osídlit	k5eAaPmNgNnS	osídlit
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
návštěva	návštěva	k1gFnSc1	návštěva
poskytne	poskytnout	k5eAaPmIp3nS	poskytnout
jak	jak	k6eAd1	jak
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
krajinný	krajinný	k2eAgInSc1d1	krajinný
tak	tak	k8xS	tak
i	i	k9	i
kulturně-folklorní	kulturněolklorní	k2eAgInSc4d1	kulturně-folklorní
zážitek	zážitek	k1gInSc4	zážitek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bartang	Bartang	k1gMnSc1	Bartang
končí	končit	k5eAaImIp3nS	končit
svou	svůj	k3xOyFgFnSc4	svůj
pouť	pouť	k1gFnSc4	pouť
jako	jako	k8xS	jako
pravý	pravý	k2eAgInSc4d1	pravý
přítok	přítok	k1gInSc4	přítok
hraniční	hraniční	k2eAgFnSc2d1	hraniční
řeky	řeka	k1gFnSc2	řeka
Pandž	Pandž	k1gFnSc1	Pandž
(	(	kIx(	(
<g/>
zdrojnice	zdrojnice	k1gFnSc1	zdrojnice
Amudarji	Amudarje	k1gFnSc4	Amudarje
<g/>
)	)	kIx)	)
nedaleko	nedaleko	k7c2	nedaleko
tádžického	tádžický	k2eAgNnSc2d1	tádžické
města	město	k1gNnSc2	město
Vomar	Vomara	k1gFnPc2	Vomara
(	(	kIx(	(
<g/>
aka	aka	k?	aka
Rušon	Rušon	k1gInSc1	Rušon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přístup	přístup	k1gInSc1	přístup
==	==	k?	==
</s>
</p>
<p>
<s>
Nejsnadnější	snadný	k2eAgInSc1d3	nejsnazší
přístup	přístup	k1gInSc1	přístup
do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
pro	pro	k7c4	pro
motorová	motorový	k2eAgNnPc4d1	motorové
vozidla	vozidlo	k1gNnPc4	vozidlo
představuje	představovat	k5eAaImIp3nS	představovat
částečně	částečně	k6eAd1	částečně
zpevněná	zpevněný	k2eAgFnSc1d1	zpevněná
silnice	silnice	k1gFnSc1	silnice
vedoucí	vedoucí	k1gFnSc1	vedoucí
z	z	k7c2	z
Vomaru	Vomar	k1gInSc2	Vomar
(	(	kIx(	(
<g/>
Rušonu	Rušon	k1gInSc2	Rušon
<g/>
)	)	kIx)	)
vzhůru	vzhůru	k6eAd1	vzhůru
údolím	údolí	k1gNnSc7	údolí
až	až	k9	až
do	do	k7c2	do
vesnice	vesnice	k1gFnSc2	vesnice
Kudara	Kudar	k1gMnSc2	Kudar
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
přijet	přijet	k5eAaPmF	přijet
i	i	k9	i
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Murgabu	Murgab	k1gInSc2	Murgab
od	od	k7c2	od
jezera	jezero	k1gNnSc2	jezero
Kara-kul	Karaula	k1gFnPc2	Kara-kula
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
příjezdová	příjezdový	k2eAgFnSc1d1	příjezdová
trasa	trasa	k1gFnSc1	trasa
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
bídném	bídný	k2eAgInSc6d1	bídný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
toku	tok	k1gInSc6	tok
řeky	řeka	k1gFnSc2	řeka
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
nazývá	nazývat	k5eAaImIp3nS	nazývat
Aksu	Aksa	k1gFnSc4	Aksa
<g/>
)	)	kIx)	)
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Murgabu	Murgab	k1gInSc2	Murgab
až	až	k9	až
k	k	k7c3	k
afghánským	afghánský	k2eAgFnPc3d1	afghánská
hranicím	hranice	k1gFnPc3	hranice
zpevněná	zpevněný	k2eAgFnSc1d1	zpevněná
silnice	silnice	k1gFnSc1	silnice
s	s	k7c7	s
odbočkou	odbočka	k1gFnSc7	odbočka
na	na	k7c4	na
hraniční	hraniční	k2eAgInSc4d1	hraniční
přechod	přechod	k1gInSc4	přechod
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
v	v	k7c6	v
sedle	sedlo	k1gNnSc6	sedlo
Kulma	kulma	k1gFnSc1	kulma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vodní	vodní	k2eAgInSc1d1	vodní
režim	režim	k1gInSc1	režim
==	==	k?	==
</s>
</p>
<p>
<s>
Zdrojem	zdroj	k1gInSc7	zdroj
vody	voda	k1gFnSc2	voda
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
sněhové	sněhový	k2eAgFnPc1d1	sněhová
srážky	srážka	k1gFnPc1	srážka
a	a	k8xC	a
horské	horský	k2eAgInPc1d1	horský
ledovce	ledovec	k1gInPc1	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
průtok	průtok	k1gInSc1	průtok
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
ústí	ústí	k1gNnSc6	ústí
činí	činit	k5eAaImIp3nS	činit
128	[number]	k4	128
m3	m3	k4	m3
<g/>
/	/	kIx~	/
<g/>
s.	s.	k?	s.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
byla	být	k5eAaImAgFnS	být
vybudována	vybudován	k2eAgFnSc1d1	vybudována
Tudžanská	Tudžanský	k2eAgFnSc1d1	Tudžanský
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Б	Б	k?	Б
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
