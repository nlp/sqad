<p>
<s>
Pneumatika	pneumatika	k1gFnSc1	pneumatika
<g/>
,	,	kIx,	,
plášť	plášť	k1gInSc1	plášť
neboli	neboli	k8xC	neboli
obruč	obruč	k1gFnSc1	obruč
je	být	k5eAaImIp3nS	být
vzduchem	vzduch	k1gInSc7	vzduch
plněná	plněný	k2eAgFnSc1d1	plněná
pružná	pružný	k2eAgFnSc1d1	pružná
součást	součást	k1gFnSc1	součást
kol	kol	k7c2	kol
dopravních	dopravní	k2eAgInPc2d1	dopravní
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
tvar	tvar	k1gInSc4	tvar
toroidu	toroid	k1gInSc2	toroid
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nasazena	nasadit	k5eAaPmNgFnS	nasadit
na	na	k7c6	na
vnějším	vnější	k2eAgInSc6d1	vnější
obvodu	obvod	k1gInSc6	obvod
ráfku	ráfek	k1gInSc2	ráfek
<g/>
.	.	kIx.	.
</s>
<s>
Zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
přenos	přenos	k1gInSc1	přenos
sil	síla	k1gFnPc2	síla
mezi	mezi	k7c7	mezi
koly	kolo	k1gNnPc7	kolo
a	a	k8xC	a
vozovkou	vozovka	k1gFnSc7	vozovka
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
také	také	k9	také
jako	jako	k9	jako
primární	primární	k2eAgNnSc1d1	primární
odpružení	odpružení	k1gNnSc1	odpružení
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
pneumatiky	pneumatika	k1gFnSc2	pneumatika
bývá	bývat	k5eAaImIp3nS	bývat
vzdušnice	vzdušnice	k1gFnSc1	vzdušnice
(	(	kIx(	(
<g/>
lidově	lidově	k6eAd1	lidově
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
duše	duše	k1gFnSc1	duše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
často	často	k6eAd1	často
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
bezdušové	bezdušový	k2eAgFnPc1d1	bezdušová
pneumatiky	pneumatika	k1gFnPc1	pneumatika
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
cyklistické	cyklistický	k2eAgFnSc2d1	cyklistická
pneumatiky	pneumatika	k1gFnSc2	pneumatika
se	se	k3xPyFc4	se
vnější	vnější	k2eAgInSc1d1	vnější
díl	díl	k1gInSc1	díl
nazývá	nazývat	k5eAaImIp3nS	nazývat
plášť	plášť	k1gInSc1	plášť
či	či	k8xC	či
galuska	galuska	k1gFnSc1	galuska
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnějším	běžný	k2eAgInSc7d3	nejběžnější
materiálem	materiál	k1gInSc7	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
pneumatik	pneumatika	k1gFnPc2	pneumatika
je	být	k5eAaImIp3nS	být
vulkanizovaná	vulkanizovaný	k2eAgFnSc1d1	vulkanizovaná
pryž	pryž	k1gFnSc1	pryž
(	(	kIx(	(
<g/>
plnivem	plnivo	k1gNnSc7	plnivo
jsou	být	k5eAaImIp3nP	být
saze	saze	k1gFnPc4	saze
<g/>
,	,	kIx,	,
dávající	dávající	k2eAgFnSc4d1	dávající
typickou	typický	k2eAgFnSc4d1	typická
barvu	barva	k1gFnSc4	barva
<g/>
)	)	kIx)	)
na	na	k7c6	na
kostře	kostra	k1gFnSc6	kostra
z	z	k7c2	z
textilních	textilní	k2eAgInPc2d1	textilní
a	a	k8xC	a
ocelových	ocelový	k2eAgInPc2d1	ocelový
kordů	kord	k1gInPc2	kord
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
provedení	provedení	k1gNnSc2	provedení
kostry	kostra	k1gFnSc2	kostra
se	se	k3xPyFc4	se
pneumatiky	pneumatika	k1gFnPc1	pneumatika
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
diagonální	diagonální	k2eAgFnSc4d1	diagonální
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
nejpoužívanější	používaný	k2eAgNnSc4d3	nejpoužívanější
radiální	radiální	k2eAgNnSc4d1	radiální
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Patent	patent	k1gInSc1	patent
na	na	k7c4	na
moderní	moderní	k2eAgInSc4d1	moderní
vzduchem	vzduch	k1gInSc7	vzduch
plněnou	plněný	k2eAgFnSc4d1	plněná
pneumatiku	pneumatika	k1gFnSc4	pneumatika
získal	získat	k5eAaPmAgMnS	získat
John	John	k1gMnSc1	John
Boyd	Boyd	k1gMnSc1	Boyd
Dunlop	Dunlop	k1gInSc4	Dunlop
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
<g/>
.	.	kIx.	.
</s>
<s>
Vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
ji	on	k3xPp3gFnSc4	on
o	o	k7c4	o
rok	rok	k1gInSc4	rok
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
vyrobil	vyrobit	k5eAaPmAgMnS	vyrobit
ze	z	k7c2	z
zahradní	zahradní	k2eAgFnSc2d1	zahradní
hadice	hadice	k1gFnSc2	hadice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zlepšil	zlepšit	k5eAaPmAgMnS	zlepšit
jízdní	jízdní	k2eAgFnPc4d1	jízdní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
tříkolky	tříkolka	k1gFnSc2	tříkolka
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
ale	ale	k9	ale
první	první	k4xOgMnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1845	[number]	k4	1845
získal	získat	k5eAaPmAgInS	získat
podobný	podobný	k2eAgInSc1d1	podobný
patent	patent	k1gInSc1	patent
Robert	Robert	k1gMnSc1	Robert
William	William	k1gInSc1	William
Thomson	Thomson	k1gInSc1	Thomson
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nebyla	být	k5eNaImAgNnP	být
vhodná	vhodný	k2eAgNnPc1d1	vhodné
vozidla	vozidlo	k1gNnPc1	vozidlo
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgNnSc4	jenž
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
pneumatika	pneumatika	k1gFnSc1	pneumatika
hodila	hodit	k5eAaPmAgFnS	hodit
<g/>
,	,	kIx,	,
vynález	vynález	k1gInSc1	vynález
zapadl	zapadnout	k5eAaPmAgInS	zapadnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Dunlop	Dunlop	k1gInSc1	Dunlop
pneumatiku	pneumatika	k1gFnSc4	pneumatika
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
vyráběna	vyráběn	k2eAgNnPc4d1	vyráběno
jízdní	jízdní	k2eAgNnPc4d1	jízdní
kola	kolo	k1gNnPc4	kolo
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
vynález	vynález	k1gInSc1	vynález
uplatnil	uplatnit	k5eAaPmAgInS	uplatnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozměry	rozměr	k1gInPc1	rozměr
a	a	k8xC	a
značení	značení	k1gNnSc1	značení
pneumatik	pneumatika	k1gFnPc2	pneumatika
pro	pro	k7c4	pro
motorová	motorový	k2eAgNnPc4d1	motorové
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc1	jejich
přípojná	přípojný	k2eAgNnPc1d1	přípojné
vozidla	vozidlo	k1gNnPc1	vozidlo
==	==	k?	==
</s>
</p>
<p>
<s>
Následující	následující	k2eAgInSc1d1	následující
obrázek	obrázek	k1gInSc1	obrázek
popisuje	popisovat	k5eAaImIp3nS	popisovat
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
označení	označení	k1gNnPc4	označení
používaná	používaný	k2eAgNnPc4d1	používané
výrobcem	výrobce	k1gMnSc7	výrobce
(	(	kIx(	(
<g/>
číslice	číslice	k1gFnPc1	číslice
označují	označovat	k5eAaImIp3nP	označovat
šířku	šířka	k1gFnSc4	šířka
<g/>
,	,	kIx,	,
profil	profil	k1gInSc4	profil
<g/>
,	,	kIx,	,
průměr	průměr	k1gInSc4	průměr
a	a	k8xC	a
zátěžový	zátěžový	k2eAgInSc4d1	zátěžový
index	index	k1gInSc4	index
pneumatiky	pneumatika	k1gFnPc4	pneumatika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Standardní	standardní	k2eAgInPc1d1	standardní
rozměry	rozměr	k1gInPc1	rozměr
jsou	být	k5eAaImIp3nP	být
teoretické	teoretický	k2eAgFnSc2d1	teoretická
hodnoty	hodnota	k1gFnSc2	hodnota
platné	platný	k2eAgFnSc2d1	platná
pro	pro	k7c4	pro
novou	nový	k2eAgFnSc4d1	nová
pneumatiku	pneumatika	k1gFnSc4	pneumatika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
205	[number]	k4	205
/	/	kIx~	/
55	[number]	k4	55
R	R	kA	R
16	[number]	k4	16
91	[number]	k4	91
W	W	kA	W
</s>
</p>
<p>
<s>
Popis	popis	k1gInSc1	popis
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
údajů	údaj	k1gInPc2	údaj
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
205	[number]	k4	205
–	–	k?	–
nominální	nominální	k2eAgFnSc1d1	nominální
šířka	šířka	k1gFnSc1	šířka
pneumatiky	pneumatika	k1gFnSc2	pneumatika
v	v	k7c6	v
milimetrech	milimetr	k1gInPc6	milimetr
<g/>
,	,	kIx,	,
od	od	k7c2	od
bočnice	bočnice	k1gFnSc2	bočnice
k	k	k7c3	k
bočnici	bočnice	k1gFnSc3	bočnice
(	(	kIx(	(
<g/>
205	[number]	k4	205
mm	mm	kA	mm
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
55	[number]	k4	55
–	–	k?	–
poměr	poměr	k1gInSc1	poměr
nominální	nominální	k2eAgFnSc2d1	nominální
výšky	výška	k1gFnSc2	výška
pneu	pneu	k1gFnSc2	pneu
k	k	k7c3	k
nominální	nominální	k2eAgFnSc3d1	nominální
šířce	šířka	k1gFnSc3	šířka
v	v	k7c6	v
procentech	procento	k1gNnPc6	procento
</s>
</p>
<p>
<s>
R	R	kA	R
–	–	k?	–
typ	typ	k1gInSc1	typ
konstrukce	konstrukce	k1gFnSc2	konstrukce
kostry	kostra	k1gFnSc2	kostra
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
R	R	kA	R
<g/>
"	"	kIx"	"
–	–	k?	–
radiální	radiální	k2eAgFnSc1d1	radiální
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
D	D	kA	D
<g/>
"	"	kIx"	"
–	–	k?	–
diagonální	diagonální	k2eAgInSc4d1	diagonální
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
B	B	kA	B
<g/>
"	"	kIx"	"
–	–	k?	–
bias	bias	k1gInSc1	bias
belted	belted	k1gInSc1	belted
</s>
</p>
<p>
<s>
16	[number]	k4	16
–	–	k?	–
nominální	nominální	k2eAgInSc1d1	nominální
průměr	průměr	k1gInSc1	průměr
příslušného	příslušný	k2eAgInSc2d1	příslušný
disku	disk	k1gInSc2	disk
v	v	k7c6	v
palcích	palec	k1gInPc6	palec
–	–	k?	–
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
průměr	průměr	k1gInSc4	průměr
pneu	pneu	k1gFnSc2	pneu
</s>
</p>
<p>
<s>
91	[number]	k4	91
–	–	k?	–
index	index	k1gInSc1	index
nosnosti	nosnost	k1gFnSc2	nosnost
(	(	kIx(	(
<g/>
numerický	numerický	k2eAgInSc1d1	numerický
kód	kód	k1gInSc1	kód
"	"	kIx"	"
<g/>
91	[number]	k4	91
<g/>
"	"	kIx"	"
=	=	kIx~	=
615	[number]	k4	615
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
W	W	kA	W
–	–	k?	–
index	index	k1gInSc1	index
rychlosti	rychlost	k1gFnSc2	rychlost
(	(	kIx(	(
<g/>
kód	kód	k1gInSc1	kód
"	"	kIx"	"
<g/>
W	W	kA	W
<g/>
"	"	kIx"	"
=	=	kIx~	=
270	[number]	k4	270
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
pneumatikách	pneumatika	k1gFnPc6	pneumatika
vyznačovány	vyznačován	k2eAgInPc4d1	vyznačován
texty	text	k1gInPc4	text
označující	označující	k2eAgFnSc2d1	označující
specifické	specifický	k2eAgFnSc2d1	specifická
vlastnosti	vlastnost	k1gFnSc2	vlastnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
dušová	dušová	k1gFnSc1	dušová
<g/>
/	/	kIx~	/
<g/>
bezdušová	bezdušový	k2eAgFnSc1d1	bezdušová
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
TUBELESS	TUBELESS	kA	TUBELESS
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
TL	TL	kA	TL
<g/>
"	"	kIx"	"
–	–	k?	–
bezdušová	bezdušový	k2eAgFnSc1d1	bezdušová
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
TUBE	tubus	k1gInSc5	tubus
TYPE	typ	k1gInSc5	typ
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
TT	TT	kA	TT
<g/>
"	"	kIx"	"
–	–	k?	–
dušová	dušovat	k5eAaImIp3nS	dušovat
</s>
</p>
<p>
<s>
M	M	kA	M
<g/>
+	+	kIx~	+
<g/>
S	s	k7c7	s
nebo	nebo	k8xC	nebo
MUD	MUD	kA	MUD
<g/>
+	+	kIx~	+
<g/>
SNOW	SNOW	kA	SNOW
–	–	k?	–
(	(	kIx(	(
<g/>
bláto	bláto	k1gNnSc1	bláto
<g/>
+	+	kIx~	+
<g/>
sníh	sníh	k1gInSc1	sníh
<g/>
)	)	kIx)	)
pneu	pneu	k1gFnSc1	pneu
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
zimní	zimní	k2eAgInSc4d1	zimní
provoz	provoz	k1gInSc4	provoz
</s>
</p>
<p>
<s>
RF	RF	kA	RF
<g/>
,	,	kIx,	,
XL	XL	kA	XL
<g/>
,	,	kIx,	,
C	C	kA	C
REINFORCED	REINFORCED	kA	REINFORCED
–	–	k?	–
zesílená	zesílený	k2eAgFnSc1d1	zesílená
kostra	kostra	k1gFnSc1	kostra
pro	pro	k7c4	pro
dodávky	dodávka	k1gFnPc4	dodávka
</s>
</p>
<p>
<s>
DOT	DOT	kA	DOT
2401	[number]	k4	2401
–	–	k?	–
týden	týden	k1gInSc1	týden
a	a	k8xC	a
rok	rok	k1gInSc1	rok
výroby	výroba	k1gFnSc2	výroba
pneumatiky	pneumatika	k1gFnSc2	pneumatika
(	(	kIx(	(
<g/>
dvacátý	dvacátý	k4xOgInSc4	dvacátý
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
týden	týden	k1gInSc4	týden
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
Zkratka	zkratka	k1gFnSc1	zkratka
DOT	DOT	kA	DOT
znamená	znamenat	k5eAaImIp3nS	znamenat
Department	department	k1gInSc1	department
Of	Of	k1gFnPc2	Of
Transportation	Transportation	k1gInSc1	Transportation
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
začalo	začít	k5eAaPmAgNnS	začít
vyžadovat	vyžadovat	k5eAaImF	vyžadovat
identifikační	identifikační	k2eAgInSc4d1	identifikační
kód	kód	k1gInSc4	kód
pneumatiky	pneumatika	k1gFnSc2	pneumatika
obsahující	obsahující	k2eAgInSc4d1	obsahující
datum	datum	k1gInSc4	datum
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
formátu	formát	k1gInSc6	formát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
OUTSIDE	OUTSIDE	kA	OUTSIDE
<g/>
,	,	kIx,	,
INSIDE	INSIDE	kA	INSIDE
–	–	k?	–
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
a	a	k8xC	a
venkovní	venkovní	k2eAgFnSc1d1	venkovní
bočnice	bočnice	k1gFnSc1	bočnice
</s>
</p>
<p>
<s>
ROTATION	ROTATION	kA	ROTATION
–	–	k?	–
směr	směr	k1gInSc1	směr
otáčení	otáčení	k1gNnSc2	otáčení
</s>
</p>
<p>
<s>
==	==	k?	==
Štítkování	štítkování	k1gNnPc1	štítkování
pneumatik	pneumatika	k1gFnPc2	pneumatika
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2012	[number]	k4	2012
bylo	být	k5eAaImAgNnS	být
nařízením	nařízení	k1gNnSc7	nařízení
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
č.	č.	k?	č.
1222	[number]	k4	1222
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
zavedeno	zavést	k5eAaPmNgNnS	zavést
povinné	povinný	k2eAgNnSc1d1	povinné
značení	značení	k1gNnSc1	značení
pneumatik	pneumatika	k1gFnPc2	pneumatika
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
štítkování	štítkování	k1gNnSc1	štítkování
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
hodnocení	hodnocení	k1gNnSc6	hodnocení
podílu	podíl	k1gInSc2	podíl
na	na	k7c6	na
spotřebě	spotřeba	k1gFnSc6	spotřeba
paliva	palivo	k1gNnPc4	palivo
<g/>
,	,	kIx,	,
přilnavosti	přilnavost	k1gFnPc4	přilnavost
na	na	k7c6	na
mokré	mokrý	k2eAgFnSc6d1	mokrá
vozovce	vozovka	k1gFnSc6	vozovka
a	a	k8xC	a
vnější	vnější	k2eAgFnSc3d1	vnější
hlučnosti	hlučnost	k1gFnSc3	hlučnost
pneumatiky	pneumatika	k1gFnSc2	pneumatika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
nařízení	nařízení	k1gNnSc2	nařízení
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
pneumatiky	pneumatika	k1gFnPc4	pneumatika
pro	pro	k7c4	pro
osobní	osobní	k2eAgInPc4d1	osobní
vozy	vůz	k1gInPc4	vůz
a	a	k8xC	a
lehké	lehký	k2eAgInPc4d1	lehký
užitkové	užitkový	k2eAgInPc4d1	užitkový
vozy	vůz	k1gInPc4	vůz
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
vyrobené	vyrobený	k2eAgFnSc2d1	vyrobená
po	po	k7c6	po
30	[number]	k4	30
<g/>
.	.	kIx.	.
červnu	červen	k1gInSc3	červen
2012	[number]	k4	2012
a	a	k8xC	a
uvedené	uvedený	k2eAgNnSc4d1	uvedené
na	na	k7c4	na
trh	trh	k1gInSc4	trh
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
označeny	označit	k5eAaPmNgInP	označit
na	na	k7c6	na
veškerých	veškerý	k3xTgNnPc6	veškerý
prodejních	prodejní	k2eAgNnPc6d1	prodejní
místech	místo	k1gNnPc6	místo
samolepkou	samolepka	k1gFnSc7	samolepka
nebo	nebo	k8xC	nebo
štítkem	štítek	k1gInSc7	štítek
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
jejich	jejich	k3xOp3gFnPc2	jejich
vlastností	vlastnost	k1gFnPc2	vlastnost
(	(	kIx(	(
<g/>
avšak	avšak	k8xC	avšak
nesměly	smět	k5eNaImAgInP	smět
být	být	k5eAaImF	být
takto	takto	k6eAd1	takto
označeny	označit	k5eAaPmNgInP	označit
před	před	k7c4	před
před	před	k7c4	před
30	[number]	k4	30
<g/>
.	.	kIx.	.
květnem	květen	k1gInSc7	květen
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Text	text	k1gInSc1	text
upřesňuje	upřesňovat	k5eAaImIp3nS	upřesňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc4	tento
informace	informace	k1gFnPc4	informace
budou	být	k5eAaImBp3nP	být
muset	muset	k5eAaImF	muset
figurovat	figurovat	k5eAaImF	figurovat
i	i	k9	i
na	na	k7c6	na
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
dodavatelů	dodavatel	k1gMnPc2	dodavatel
<g/>
,	,	kIx,	,
na	na	k7c6	na
brožurách	brožura	k1gFnPc6	brožura
a	a	k8xC	a
cenících	ceník	k1gInPc6	ceník
i	i	k8xC	i
fakturách	faktura	k1gFnPc6	faktura
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
typ	typ	k1gInSc4	typ
a	a	k8xC	a
každou	každý	k3xTgFnSc4	každý
velikost	velikost	k1gFnSc4	velikost
zvlášť	zvlášť	k6eAd1	zvlášť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
nařízení	nařízení	k1gNnSc1	nařízení
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
třídy	třída	k1gFnSc2	třída
pneumatik	pneumatika	k1gFnPc2	pneumatika
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
C2	C2	k1gFnSc1	C2
a	a	k8xC	a
C3	C3	k1gFnSc1	C3
podle	podle	k7c2	podle
článku	článek	k1gInSc2	článek
8	[number]	k4	8
Nařízení	nařízení	k1gNnSc2	nařízení
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
ES	ES	kA	ES
<g/>
)	)	kIx)	)
č.	č.	k?	č.
661	[number]	k4	661
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
pneumatiky	pneumatika	k1gFnPc1	pneumatika
pro	pro	k7c4	pro
vozidla	vozidlo	k1gNnPc4	vozidlo
kategorií	kategorie	k1gFnPc2	kategorie
M	M	kA	M
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
N	N	kA	N
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
O1	O1	k1gFnSc1	O1
a	a	k8xC	a
O2	O2	k1gFnSc1	O2
jako	jako	k8xC	jako
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kategorie	kategorie	k1gFnPc4	kategorie
M	M	kA	M
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
N	N	kA	N
<g/>
,	,	kIx,	,
O3	O3	k1gFnSc1	O3
a	a	k8xC	a
O4	O4	k1gFnSc1	O4
s	s	k7c7	s
indexem	index	k1gInSc7	index
nosnosti	nosnost	k1gFnSc2	nosnost
do	do	k7c2	do
121	[number]	k4	121
včetně	včetně	k7c2	včetně
jako	jako	k9	jako
C2	C2	k1gFnPc2	C2
a	a	k8xC	a
nad	nad	k7c7	nad
121	[number]	k4	121
jako	jako	k8xS	jako
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Třída	třída	k1gFnSc1	třída
palivové	palivový	k2eAgFnSc2d1	palivová
účinnosti	účinnost	k1gFnSc2	účinnost
===	===	k?	===
</s>
</p>
<p>
<s>
Štítek	štítek	k1gInSc1	štítek
"	"	kIx"	"
<g/>
úspory	úspora	k1gFnPc1	úspora
paliva	palivo	k1gNnSc2	palivo
<g/>
"	"	kIx"	"
označuje	označovat	k5eAaImIp3nS	označovat
navýšení	navýšení	k1gNnSc1	navýšení
spotřeby	spotřeba	k1gFnSc2	spotřeba
paliva	palivo	k1gNnSc2	palivo
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
valivém	valivý	k2eAgInSc6d1	valivý
odporu	odpor	k1gInSc6	odpor
pneumatiky	pneumatika	k1gFnSc2	pneumatika
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
třída	třída	k1gFnSc1	třída
A	A	kA	A
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
nejnižší	nízký	k2eAgInSc4d3	nejnižší
koeficient	koeficient	k1gInSc4	koeficient
valivého	valivý	k2eAgInSc2d1	valivý
odporu	odpor	k1gInSc2	odpor
RRC	RRC	kA	RRC
(	(	kIx(	(
<g/>
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
spotřeba	spotřeba	k1gFnSc1	spotřeba
paliva	palivo	k1gNnSc2	palivo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozmezí	rozmezí	k1gNnSc1	rozmezí
koeficientů	koeficient	k1gInPc2	koeficient
stoupá	stoupat	k5eAaImIp3nS	stoupat
až	až	k9	až
ke	k	k7c3	k
třídě	třída	k1gFnSc3	třída
G	G	kA	G
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
nejvyšší	vysoký	k2eAgFnPc4d3	nejvyšší
hodnoty	hodnota	k1gFnPc4	hodnota
koeficientu	koeficient	k1gInSc2	koeficient
(	(	kIx(	(
<g/>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
spotřeba	spotřeba	k1gFnSc1	spotřeba
paliva	palivo	k1gNnSc2	palivo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
písmeno	písmeno	k1gNnSc1	písmeno
D	D	kA	D
je	být	k5eAaImIp3nS	být
použito	použít	k5eAaPmNgNnS	použít
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
třídy	třída	k1gFnSc2	třída
pneumatik	pneumatika	k1gFnPc2	pneumatika
C3	C3	k1gFnSc2	C3
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
u	u	k7c2	u
této	tento	k3xDgFnSc2	tento
není	být	k5eNaImIp3nS	být
použito	použit	k2eAgNnSc4d1	použito
písmeno	písmeno	k1gNnSc4	písmeno
G.	G.	kA	G.
Údaj	údaj	k1gInSc1	údaj
o	o	k7c6	o
navýšení	navýšení	k1gNnSc6	navýšení
spotřeby	spotřeba	k1gFnSc2	spotřeba
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
tabulce	tabulka	k1gFnSc6	tabulka
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
orientační	orientační	k2eAgFnSc1d1	orientační
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
ideální	ideální	k2eAgInSc4d1	ideální
model	model	k1gInSc4	model
osobního	osobní	k2eAgInSc2d1	osobní
vozu	vůz	k1gInSc2	vůz
se	s	k7c7	s
spotřebou	spotřeba	k1gFnSc7	spotřeba
6,6	[number]	k4	6,6
l	l	kA	l
benzínu	benzín	k1gInSc2	benzín
na	na	k7c6	na
100	[number]	k4	100
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Třída	třída	k1gFnSc1	třída
přilnavosti	přilnavost	k1gFnSc2	přilnavost
za	za	k7c2	za
mokra	mokro	k1gNnSc2	mokro
===	===	k?	===
</s>
</p>
<p>
<s>
Štítek	štítek	k1gInSc1	štítek
přilnavosti	přilnavost	k1gFnSc2	přilnavost
za	za	k7c2	za
mokra	mokro	k1gNnSc2	mokro
lze	lze	k6eAd1	lze
chápat	chápat	k5eAaImF	chápat
jako	jako	k9	jako
rozdíl	rozdíl	k1gInSc4	rozdíl
v	v	k7c6	v
brzdné	brzdný	k2eAgFnSc6d1	brzdná
dráze	dráha	k1gFnSc6	dráha
na	na	k7c6	na
mokré	mokrý	k2eAgFnSc6d1	mokrá
vozovce	vozovka	k1gFnSc6	vozovka
<g/>
.	.	kIx.	.
</s>
<s>
Třídy	třída	k1gFnPc1	třída
přilnavosti	přilnavost	k1gFnSc2	přilnavost
se	se	k3xPyFc4	se
stanoví	stanovit	k5eAaPmIp3nS	stanovit
pro	pro	k7c4	pro
třídu	třída	k1gFnSc4	třída
pneumatik	pneumatika	k1gFnPc2	pneumatika
C1	C1	k1gFnPc2	C1
od	od	k7c2	od
A	A	kA	A
do	do	k7c2	do
G	G	kA	G
podle	podle	k7c2	podle
indexu	index	k1gInSc2	index
přilnavosti	přilnavost	k1gFnSc2	přilnavost
za	za	k7c2	za
mokra	mokro	k1gNnSc2	mokro
G	G	kA	G
<g/>
,	,	kIx,	,
od	od	k7c2	od
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
(	(	kIx(	(
<g/>
nejkratší	krátký	k2eAgFnSc1d3	nejkratší
brzdná	brzdný	k2eAgFnSc1d1	brzdná
dráha	dráha	k1gFnSc1	dráha
<g/>
)	)	kIx)	)
po	po	k7c6	po
nejnižší	nízký	k2eAgFnSc6d3	nejnižší
(	(	kIx(	(
<g/>
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
brzdná	brzdný	k2eAgFnSc1d1	brzdná
dráha	dráha	k1gFnSc1	dráha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ale	ale	k9	ale
D	D	kA	D
a	a	k8xC	a
G	G	kA	G
není	být	k5eNaImIp3nS	být
použito	použít	k5eAaPmNgNnS	použít
<g/>
.	.	kIx.	.
</s>
<s>
Brzdná	brzdný	k2eAgFnSc1d1	brzdná
dráha	dráha	k1gFnSc1	dráha
se	se	k3xPyFc4	se
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
pro	pro	k7c4	pro
brždění	bržděný	k2eAgMnPc1d1	bržděný
z	z	k7c2	z
rychlosti	rychlost	k1gFnSc2	rychlost
80	[number]	k4	80
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
</s>
</p>
<p>
<s>
===	===	k?	===
Třída	třída	k1gFnSc1	třída
a	a	k8xC	a
naměřená	naměřený	k2eAgFnSc1d1	naměřená
hodnota	hodnota	k1gFnSc1	hodnota
vnějšího	vnější	k2eAgInSc2d1	vnější
hluku	hluk	k1gInSc2	hluk
při	při	k7c6	při
odvalování	odvalování	k1gNnSc6	odvalování
===	===	k?	===
</s>
</p>
<p>
<s>
Štítek	štítek	k1gInSc1	štítek
vnějšího	vnější	k2eAgInSc2d1	vnější
hluku	hluk	k1gInSc2	hluk
při	při	k7c6	při
odvalování	odvalování	k1gNnSc6	odvalování
uvádí	uvádět	k5eAaImIp3nS	uvádět
přímo	přímo	k6eAd1	přímo
naměřenou	naměřený	k2eAgFnSc4d1	naměřená
hodnotu	hodnota	k1gFnSc4	hodnota
v	v	k7c6	v
decibelech	decibel	k1gInPc6	decibel
a	a	k8xC	a
piktogram	piktogram	k1gInSc4	piktogram
vyjadřující	vyjadřující	k2eAgInSc1d1	vyjadřující
vztah	vztah	k1gInSc1	vztah
této	tento	k3xDgFnSc2	tento
hodnoty	hodnota	k1gFnSc2	hodnota
k	k	k7c3	k
povoleným	povolený	k2eAgFnPc3d1	povolená
hodnotám	hodnota	k1gFnPc3	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
jsou	být	k5eAaImIp3nP	být
stanoveny	stanovit	k5eAaPmNgInP	stanovit
v	v	k7c4	v
části	část	k1gFnPc4	část
C	C	kA	C
přílohy	příloha	k1gFnSc2	příloha
II	II	kA	II
nařízení	nařízení	k1gNnSc4	nařízení
(	(	kIx(	(
<g/>
ES	ES	kA	ES
<g/>
)	)	kIx)	)
č.	č.	k?	č.
661	[number]	k4	661
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
a	a	k8xC	a
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
třídě	třída	k1gFnSc6	třída
pneumatiky	pneumatika	k1gFnSc2	pneumatika
<g/>
,	,	kIx,	,
jejích	její	k3xOp3gInPc6	její
rozměrech	rozměr	k1gInPc6	rozměr
<g/>
,	,	kIx,	,
provedení	provedení	k1gNnSc6	provedení
a	a	k8xC	a
kategorii	kategorie	k1gFnSc6	kategorie
použití	použití	k1gNnSc2	použití
<g/>
.	.	kIx.	.
</s>
<s>
Rozmezí	rozmezí	k1gNnSc1	rozmezí
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
70	[number]	k4	70
dB	db	kA	db
<g/>
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
úzké	úzký	k2eAgFnPc4d1	úzká
pneumatiky	pneumatika	k1gFnPc4	pneumatika
třídy	třída	k1gFnSc2	třída
C1A	C1A	k1gFnPc2	C1A
až	až	k9	až
po	po	k7c4	po
78	[number]	k4	78
dB	db	kA	db
<g/>
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
u	u	k7c2	u
pneumatik	pneumatika	k1gFnPc2	pneumatika
třídy	třída	k1gFnSc2	třída
C3	C3	k1gFnSc2	C3
pro	pro	k7c4	pro
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
užití	užití	k1gNnSc4	užití
současně	současně	k6eAd1	současně
s	s	k7c7	s
určením	určení	k1gNnSc7	určení
pro	pro	k7c4	pro
jízdu	jízda	k1gFnSc4	jízda
na	na	k7c6	na
sněhu	sníh	k1gInSc6	sníh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Piktogram	piktogram	k1gInSc1	piktogram
reproduktoru	reproduktor	k1gInSc2	reproduktor
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
vlnou	vlna	k1gFnSc7	vlna
značí	značit	k5eAaImIp3nS	značit
pneumatiku	pneumatika	k1gFnSc4	pneumatika
tišší	tichý	k2eAgFnSc4d2	tišší
o	o	k7c4	o
3	[number]	k4	3
a	a	k8xC	a
více	hodně	k6eAd2	hodně
dB	db	kA	db
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
požadováno	požadován	k2eAgNnSc1d1	požadováno
<g/>
.	.	kIx.	.
</s>
<s>
Piktogram	piktogram	k1gInSc1	piktogram
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
vlnami	vlna	k1gFnPc7	vlna
značí	značit	k5eAaImIp3nS	značit
pneumatiku	pneumatika	k1gFnSc4	pneumatika
splňující	splňující	k2eAgFnSc4d1	splňující
normu	norma	k1gFnSc4	norma
nebo	nebo	k8xC	nebo
tišší	tichý	k2eAgInSc4d2	tišší
o	o	k7c4	o
méně	málo	k6eAd2	málo
než	než	k8xS	než
3	[number]	k4	3
dB	db	kA	db
<g/>
.	.	kIx.	.
</s>
<s>
Piktogram	piktogram	k1gInSc1	piktogram
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
vlnami	vlna	k1gFnPc7	vlna
značí	značit	k5eAaImIp3nS	značit
pneumatiku	pneumatika	k1gFnSc4	pneumatika
hlučnější	hlučný	k2eAgFnSc1d2	hlučnější
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
dezénů	dezén	k1gInPc2	dezén
==	==	k?	==
</s>
</p>
<p>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
několik	několik	k4yIc4	několik
základních	základní	k2eAgInPc2d1	základní
typů	typ	k1gInPc2	typ
dezénů	dezén	k1gInPc2	dezén
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
symetrický	symetrický	k2eAgInSc4d1	symetrický
<g/>
,	,	kIx,	,
asymetrický	asymetrický	k2eAgInSc4d1	asymetrický
a	a	k8xC	a
směrový	směrový	k2eAgInSc4d1	směrový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Symetrický	symetrický	k2eAgInSc1d1	symetrický
</s>
</p>
<p>
<s>
Symetrickou	symetrický	k2eAgFnSc4d1	symetrická
pneumatiku	pneumatika	k1gFnSc4	pneumatika
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
umístit	umístit	k5eAaPmF	umístit
na	na	k7c6	na
vozidle	vozidlo	k1gNnSc6	vozidlo
na	na	k7c4	na
libovolnou	libovolný	k2eAgFnSc4d1	libovolná
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Asymetrický	asymetrický	k2eAgInSc1d1	asymetrický
</s>
</p>
<p>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
strana	strana	k1gFnSc1	strana
dezénu	dezén	k1gInSc2	dezén
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
strany	strana	k1gFnSc2	strana
vnější	vnější	k2eAgInSc4d1	vnější
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
má	mít	k5eAaImIp3nS	mít
svoji	svůj	k3xOyFgFnSc4	svůj
specifickou	specifický	k2eAgFnSc4d1	specifická
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
strana	strana	k1gFnSc1	strana
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
odvod	odvod	k1gInSc4	odvod
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
přenos	přenos	k1gInSc1	přenos
záběrových	záběrový	k2eAgFnPc2d1	záběrová
a	a	k8xC	a
brzdných	brzdný	k2eAgFnPc2d1	brzdná
sil	síla	k1gFnPc2	síla
na	na	k7c4	na
vozovku	vozovka	k1gFnSc4	vozovka
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
vnější	vnější	k2eAgNnSc1d1	vnější
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
vedení	vedení	k1gNnSc1	vedení
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
směru	směr	k1gInSc6	směr
a	a	k8xC	a
stabilitu	stabilita	k1gFnSc4	stabilita
v	v	k7c6	v
zatáčkách	zatáčka	k1gFnPc6	zatáčka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
asymetrických	asymetrický	k2eAgFnPc2d1	asymetrická
pneumatik	pneumatika	k1gFnPc2	pneumatika
nerozlišujeme	rozlišovat	k5eNaImIp1nP	rozlišovat
pravou	pravá	k1gFnSc4	pravá
a	a	k8xC	a
levou	levý	k2eAgFnSc4d1	levá
pneumatiku	pneumatika	k1gFnSc4	pneumatika
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
má	mít	k5eAaImIp3nS	mít
vždy	vždy	k6eAd1	vždy
označenou	označený	k2eAgFnSc4d1	označená
vnější	vnější	k2eAgFnSc4d1	vnější
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
stranu	strana	k1gFnSc4	strana
(	(	kIx(	(
<g/>
outside	outsid	k1gMnSc5	outsid
<g/>
,	,	kIx,	,
inside	insid	k1gMnSc5	insid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
při	při	k7c6	při
montáži	montáž	k1gFnSc6	montáž
dodržet	dodržet	k5eAaPmF	dodržet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Směrový	směrový	k2eAgInSc4d1	směrový
(	(	kIx(	(
<g/>
šípový	šípový	k2eAgInSc4d1	šípový
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Každá	každý	k3xTgFnSc1	každý
pneumatika	pneumatika	k1gFnSc1	pneumatika
má	mít	k5eAaImIp3nS	mít
šipkou	šipka	k1gFnSc7	šipka
označen	označit	k5eAaPmNgInS	označit
směr	směr	k1gInSc1	směr
otáčení	otáčení	k1gNnSc2	otáčení
pro	pro	k7c4	pro
jízdu	jízda	k1gFnSc4	jízda
vpřed	vpřed	k6eAd1	vpřed
<g/>
.	.	kIx.	.
<g/>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
např.	např.	kA	např.
s	s	k7c7	s
typem	typ	k1gInSc7	typ
asymetrická	asymetrický	k2eAgFnSc1d1	asymetrická
směrová	směrový	k2eAgFnSc1d1	směrová
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
na	na	k7c4	na
super	super	k2eAgInPc4d1	super
sporty	sport	k1gInPc4	sport
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dojezdové	dojezdový	k2eAgFnPc4d1	dojezdová
pneumatiky	pneumatika	k1gFnPc4	pneumatika
==	==	k?	==
</s>
</p>
<p>
<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
konstrukce	konstrukce	k1gFnSc1	konstrukce
na	na	k7c6	na
dojezdových	dojezdový	k2eAgFnPc6d1	dojezdová
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
run-flat	runle	k1gNnPc2	run-fle
–	–	k?	–
doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
jízda	jízda	k1gFnSc1	jízda
s	s	k7c7	s
prázdnou	prázdný	k2eAgFnSc7d1	prázdná
pneumatikou	pneumatika	k1gFnSc7	pneumatika
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
pneumatikách	pneumatika	k1gFnPc6	pneumatika
<g/>
,	,	kIx,	,
spočívající	spočívající	k2eAgFnSc7d1	spočívající
v	v	k7c6	v
zesílené	zesílený	k2eAgFnSc6d1	zesílená
bočnici	bočnice	k1gFnSc6	bočnice
<g/>
,	,	kIx,	,
umožní	umožnit	k5eAaPmIp3nS	umožnit
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
pneumatikami	pneumatika	k1gFnPc7	pneumatika
i	i	k8xC	i
jízdu	jízda	k1gFnSc4	jízda
s	s	k7c7	s
defektem	defekt	k1gInSc7	defekt
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
takto	takto	k6eAd1	takto
poškozenou	poškozený	k2eAgFnSc7d1	poškozená
pneumatikou	pneumatika	k1gFnSc7	pneumatika
lze	lze	k6eAd1	lze
jet	jet	k5eAaImF	jet
až	až	k9	až
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
80	[number]	k4	80
km	km	kA	km
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
rychlostí	rychlost	k1gFnSc7	rychlost
max	max	kA	max
<g/>
.	.	kIx.	.
80	[number]	k4	80
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Poškozenou	poškozený	k2eAgFnSc7d1	poškozená
run-flat	runlat	k5eAaPmF	run-flat
pneumatiku	pneumatika	k1gFnSc4	pneumatika
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
vyměnit	vyměnit	k5eAaPmF	vyměnit
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
oprava	oprava	k1gFnSc1	oprava
není	být	k5eNaImIp3nS	být
možná	možný	k2eAgFnSc1d1	možná
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
montovat	montovat	k5eAaImF	montovat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
vozy	vůz	k1gInPc4	vůz
s	s	k7c7	s
kontrolou	kontrola	k1gFnSc7	kontrola
tlaku	tlak	k1gInSc2	tlak
v	v	k7c6	v
pneumatikách	pneumatika	k1gFnPc6	pneumatika
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
řidič	řidič	k1gMnSc1	řidič
nemusí	muset	k5eNaImIp3nS	muset
poznat	poznat	k5eAaPmF	poznat
defekt	defekt	k1gInSc4	defekt
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
kódů	kód	k1gInPc2	kód
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
run-flat	runle	k1gNnPc2	run-fle
pneumatik	pneumatika	k1gFnPc2	pneumatika
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
výrobce	výrobce	k1gMnSc2	výrobce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Bridgestone	Bridgeston	k1gInSc5	Bridgeston
<g/>
,	,	kIx,	,
Pirelli	Pirelle	k1gFnSc3	Pirelle
–	–	k?	–
RFT	RFT	kA	RFT
</s>
</p>
<p>
<s>
Continental	Continental	k1gMnSc1	Continental
–	–	k?	–
SSR	SSR	kA	SSR
<g/>
,	,	kIx,	,
CSR	CSR	kA	CSR
</s>
</p>
<p>
<s>
Dunlop	Dunlop	k1gInSc1	Dunlop
–	–	k?	–
DSST	DSST	kA	DSST
</s>
</p>
<p>
<s>
Goodyear	Goodyear	k1gInSc1	Goodyear
–	–	k?	–
ROF	ROF	kA	ROF
</s>
</p>
<p>
<s>
Michelin	Michelin	k2eAgMnSc1d1	Michelin
–	–	k?	–
ZP	ZP	kA	ZP
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Obruč	obruč	k1gFnSc1	obruč
</s>
</p>
<p>
<s>
Kolo	kolo	k1gNnSc1	kolo
</s>
</p>
<p>
<s>
Ráfek	ráfek	k1gInSc1	ráfek
</s>
</p>
<p>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
(	(	kIx(	(
<g/>
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bar	bar	k1gInSc1	bar
(	(	kIx(	(
<g/>
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
pneumatika	pneumatika	k1gFnSc1	pneumatika
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
pneumatika	pneumatika	k1gFnSc1	pneumatika
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
