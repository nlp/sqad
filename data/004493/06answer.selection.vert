<s>
Staroměstský	staroměstský	k2eAgInSc1d1	staroměstský
orloj	orloj	k1gInSc1	orloj
nebo	nebo	k8xC	nebo
také	také	k9	také
Pražský	pražský	k2eAgInSc4d1	pražský
orloj	orloj	k1gInSc4	orloj
jsou	být	k5eAaImIp3nP	být
středověké	středověký	k2eAgFnPc1d1	středověká
astronomické	astronomický	k2eAgFnPc1d1	astronomická
hodiny	hodina	k1gFnPc1	hodina
<g/>
,	,	kIx,	,
umístěné	umístěný	k2eAgInPc1d1	umístěný
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
věže	věž	k1gFnSc2	věž
Staroměstské	staroměstský	k2eAgFnSc2d1	Staroměstská
radnice	radnice	k1gFnSc2	radnice
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
