<s>
Máslo	máslo	k1gNnSc1	máslo
je	být	k5eAaImIp3nS	být
mléčný	mléčný	k2eAgInSc4d1	mléčný
výrobek	výrobek	k1gInSc4	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
zakoncentrováním	zakoncentrování	k1gNnSc7	zakoncentrování
mléčného	mléčný	k2eAgInSc2d1	mléčný
tuku	tuk	k1gInSc2	tuk
(	(	kIx(	(
<g/>
smetany	smetana	k1gFnSc2	smetana
<g/>
)	)	kIx)	)
a	a	k8xC	a
následnou	následný	k2eAgFnSc7d1	následná
reverzí	reverze	k1gFnSc7	reverze
fází	fáze	k1gFnPc2	fáze
na	na	k7c4	na
emulzi	emulze	k1gFnSc4	emulze
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
oleji	olej	k1gInSc6	olej
<g/>
.	.	kIx.	.
</s>
<s>
Klasické	klasický	k2eAgNnSc1d1	klasické
máslo	máslo	k1gNnSc1	máslo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
minimálně	minimálně	k6eAd1	minimálně
80	[number]	k4	80
%	%	kIx~	%
mléčného	mléčný	k2eAgInSc2d1	mléčný
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
tvoří	tvořit	k5eAaImIp3nS	tvořit
voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
do	do	k7c2	do
16	[number]	k4	16
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
mléčná	mléčný	k2eAgFnSc1d1	mléčná
sušina	sušina	k1gFnSc1	sušina
(	(	kIx(	(
<g/>
bílkoviny	bílkovina	k1gFnPc1	bílkovina
<g/>
,	,	kIx,	,
laktóza	laktóza	k1gFnSc1	laktóza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čerstvé	čerstvý	k2eAgNnSc1d1	čerstvé
máslo	máslo	k1gNnSc1	máslo
-	-	kIx~	-
na	na	k7c6	na
obalu	obal	k1gInSc6	obal
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
čerstvé	čerstvý	k2eAgNnSc1d1	čerstvé
<g/>
"	"	kIx"	"
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
máslo	máslo	k1gNnSc1	máslo
má	mít	k5eAaImIp3nS	mít
trvanlivost	trvanlivost	k1gFnSc4	trvanlivost
pouze	pouze	k6eAd1	pouze
20	[number]	k4	20
dnů	den	k1gInPc2	den
od	od	k7c2	od
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
skladování	skladování	k1gNnSc2	skladování
je	být	k5eAaImIp3nS	být
max	max	kA	max
8	[number]	k4	8
°	°	k?	°
<g/>
C	C	kA	C
Stolní	stolní	k2eAgNnSc1d1	stolní
máslo	máslo	k1gNnSc1	máslo
-	-	kIx~	-
už	už	k6eAd1	už
není	být	k5eNaImIp3nS	být
označeno	označit	k5eAaPmNgNnS	označit
jako	jako	k8xC	jako
čerstvé	čerstvý	k2eAgNnSc1d1	čerstvé
a	a	k8xC	a
doba	doba	k1gFnSc1	doba
skladování	skladování	k1gNnSc2	skladování
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
delší	dlouhý	k2eAgFnSc1d2	delší
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
skladovat	skladovat	k5eAaImF	skladovat
při	při	k7c6	při
chladírenských	chladírenský	k2eAgFnPc6d1	chladírenská
teplotách	teplota	k1gFnPc6	teplota
(	(	kIx(	(
<g/>
2	[number]	k4	2
-	-	kIx~	-
8	[number]	k4	8
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
asi	asi	k9	asi
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
nebo	nebo	k8xC	nebo
při	při	k7c6	při
mrazírenských	mrazírenský	k2eAgFnPc6d1	mrazírenská
teplotách	teplota	k1gFnPc6	teplota
(	(	kIx(	(
<g/>
-	-	kIx~	-
18	[number]	k4	18
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
více	hodně	k6eAd2	hodně
než	než	k8xS	než
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Máslo	máslo	k1gNnSc1	máslo
-	-	kIx~	-
také	také	k9	také
není	být	k5eNaImIp3nS	být
označeno	označit	k5eAaPmNgNnS	označit
jako	jako	k8xC	jako
čerstvé	čerstvý	k2eAgNnSc1d1	čerstvé
a	a	k8xC	a
doba	doba	k1gFnSc1	doba
skladování	skladování	k1gNnSc2	skladování
je	být	k5eAaImIp3nS	být
cca	cca	kA	cca
35	[number]	k4	35
-	-	kIx~	-
55	[number]	k4	55
dní	den	k1gInPc2	den
od	od	k7c2	od
data	datum	k1gNnSc2	datum
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obale	obal	k1gInSc6	obal
je	být	k5eAaImIp3nS	být
vyznačeno	vyznačen	k2eAgNnSc1d1	vyznačeno
datum	datum	k1gNnSc1	datum
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
<g/>
trvanlivosti	trvanlivost	k1gFnPc1	trvanlivost
při	při	k7c6	při
dodržení	dodržení	k1gNnSc6	dodržení
skladovacích	skladovací	k2eAgFnPc2d1	skladovací
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Máslo	máslo	k1gNnSc1	máslo
přepuštěné	přepuštěný	k2eAgFnPc1d1	přepuštěný
-	-	kIx~	-
v	v	k7c6	v
legislativě	legislativa	k1gFnSc6	legislativa
nazývané	nazývaný	k2eAgNnSc1d1	nazývané
"	"	kIx"	"
<g/>
mléčný	mléčný	k2eAgInSc4d1	mléčný
bezvodý	bezvodý	k2eAgInSc4d1	bezvodý
tuk	tuk	k1gInSc4	tuk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
obsah	obsah	k1gInSc1	obsah
mléčného	mléčný	k2eAgInSc2d1	mléčný
tuku	tuk	k1gInSc2	tuk
více	hodně	k6eAd2	hodně
než	než	k8xS	než
99,9	[number]	k4	99,9
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
ČR	ČR	kA	ČR
ho	on	k3xPp3gInSc4	on
momentálně	momentálně	k6eAd1	momentálně
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
společnost	společnost	k1gFnSc1	společnost
Mádhava	Mádhava	k1gFnSc1	Mádhava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
ho	on	k3xPp3gMnSc4	on
vyráběla	vyrábět	k5eAaImAgFnS	vyrábět
společnost	společnost	k1gFnSc1	společnost
A	A	kA	A
<g/>
+	+	kIx~	+
<g/>
Z	z	k7c2	z
Milk	Milk	k1gMnSc1	Milk
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
Máslo	máslo	k1gNnSc1	máslo
-	-	kIx~	-
klasické	klasický	k2eAgNnSc1d1	klasické
máslo	máslo	k1gNnSc1	máslo
vyrobené	vyrobený	k2eAgNnSc1d1	vyrobené
pouze	pouze	k6eAd1	pouze
ze	z	k7c2	z
smetany	smetana	k1gFnSc2	smetana
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
minimálně	minimálně	k6eAd1	minimálně
80	[number]	k4	80
%	%	kIx~	%
mléčného	mléčný	k2eAgInSc2d1	mléčný
tuku	tuk	k1gInSc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Zakotveno	zakotven	k2eAgNnSc1d1	zakotveno
v	v	k7c6	v
dodatku	dodatek	k1gInSc6	dodatek
II	II	kA	II
nařízení	nařízení	k1gNnSc2	nařízení
EU	EU	kA	EU
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
se	se	k3xPyFc4	se
stanoví	stanovit	k5eAaPmIp3nS	stanovit
společná	společný	k2eAgFnSc1d1	společná
organizace	organizace	k1gFnSc1	organizace
trhů	trh	k1gInPc2	trh
se	s	k7c7	s
zemědělskými	zemědělský	k2eAgInPc7d1	zemědělský
produkty	produkt	k1gInPc7	produkt
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Máslo	máslo	k1gNnSc1	máslo
se	s	k7c7	s
smetanovým	smetanový	k2eAgInSc7d1	smetanový
zákysem	zákys	k1gInSc7	zákys
-	-	kIx~	-
minimálně	minimálně	k6eAd1	minimálně
75	[number]	k4	75
%	%	kIx~	%
mléčného	mléčný	k2eAgInSc2d1	mléčný
tuku	tuk	k1gInSc2	tuk
Máslo	máslo	k1gNnSc4	máslo
se	s	k7c7	s
sníženým	snížený	k2eAgInSc7d1	snížený
obsahem	obsah	k1gInSc7	obsah
tuku	tuk	k1gInSc2	tuk
-	-	kIx~	-
minimálně	minimálně	k6eAd1	minimálně
61	[number]	k4	61
%	%	kIx~	%
mléčného	mléčný	k2eAgInSc2d1	mléčný
tuku	tuk	k1gInSc2	tuk
Máslo	máslo	k1gNnSc4	máslo
s	s	k7c7	s
nízkým	nízký	k2eAgInSc7d1	nízký
obsahem	obsah	k1gInSc7	obsah
tuku	tuk	k1gInSc2	tuk
-	-	kIx~	-
minimálně	minimálně	k6eAd1	minimálně
41	[number]	k4	41
%	%	kIx~	%
mléčného	mléčný	k2eAgInSc2d1	mléčný
tuku	tuk	k1gInSc2	tuk
Smetana	smetana	k1gFnSc1	smetana
získaná	získaný	k2eAgFnSc1d1	získaná
z	z	k7c2	z
mléka	mléko	k1gNnSc2	mléko
odstřeďováním	odstřeďování	k1gNnPc3	odstřeďování
o	o	k7c6	o
tučnosti	tučnost	k1gFnSc6	tučnost
asi	asi	k9	asi
40	[number]	k4	40
<g/>
%	%	kIx~	%
(	(	kIx(	(
<g/>
optimálně	optimálně	k6eAd1	optimálně
37	[number]	k4	37
-	-	kIx~	-
42	[number]	k4	42
%	%	kIx~	%
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pasteruje	pasterovat	k5eAaBmIp3nS	pasterovat
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
nad	nad	k7c7	nad
95	[number]	k4	95
°	°	k?	°
<g/>
C	C	kA	C
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
několika	několik	k4yIc2	několik
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
této	tento	k3xDgFnSc2	tento
vyšší	vysoký	k2eAgFnSc2d2	vyšší
teploty	teplota	k1gFnSc2	teplota
je	být	k5eAaImIp3nS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
smetana	smetana	k1gFnSc1	smetana
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
horší	zlý	k2eAgFnPc4d2	horší
tepelné	tepelný	k2eAgFnPc4d1	tepelná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
oproti	oproti	k7c3	oproti
mléku	mléko	k1gNnSc3	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgNnSc4d1	fyzikální
zrání	zrání	k1gNnSc4	zrání
smetany	smetana	k1gFnSc2	smetana
je	být	k5eAaImIp3nS	být
klíčovým	klíčový	k2eAgInSc7d1	klíčový
krokem	krok	k1gInSc7	krok
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
másla	máslo	k1gNnSc2	máslo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
kroku	krok	k1gInSc6	krok
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
částečné	částečný	k2eAgFnSc3d1	částečná
krystalizaci	krystalizace	k1gFnSc3	krystalizace
mléčného	mléčný	k2eAgInSc2d1	mléčný
tuku	tuk	k1gInSc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgNnSc4d1	fyzikální
zrání	zrání	k1gNnSc4	zrání
másla	máslo	k1gNnSc2	máslo
by	by	kYmCp3nP	by
mělo	mít	k5eAaImAgNnS	mít
probíhat	probíhat	k5eAaImF	probíhat
alespoň	alespoň	k9	alespoň
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
před	před	k7c7	před
stloukáním	stloukání	k1gNnSc7	stloukání
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
fyzikálního	fyzikální	k2eAgNnSc2d1	fyzikální
zrání	zrání	k1gNnSc2	zrání
dochází	docházet	k5eAaImIp3nS	docházet
také	také	k9	také
ke	k	k7c3	k
zvyšování	zvyšování	k1gNnSc3	zvyšování
viskozity	viskozita	k1gFnSc2	viskozita
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
krystalizovaný	krystalizovaný	k2eAgInSc1d1	krystalizovaný
mléčný	mléčný	k2eAgInSc1d1	mléčný
tuk	tuk	k1gInSc1	tuk
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
proces	proces	k1gInSc4	proces
stloukání	stloukání	k1gNnSc2	stloukání
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatečně	dostatečně	k6eNd1	dostatečně
proběhnuté	proběhnutý	k2eAgNnSc4d1	proběhnuté
fyzikální	fyzikální	k2eAgNnSc4d1	fyzikální
zrání	zrání	k1gNnSc4	zrání
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
ztráty	ztráta	k1gFnSc2	ztráta
mléčného	mléčný	k2eAgInSc2d1	mléčný
tuku	tuk	k1gInSc2	tuk
do	do	k7c2	do
podmáslí	podmáslí	k1gNnSc2	podmáslí
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
také	také	k9	také
teplota	teplota	k1gFnSc1	teplota
fyzikálního	fyzikální	k2eAgNnSc2d1	fyzikální
zrání	zrání	k1gNnSc2	zrání
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
pohybovat	pohybovat	k5eAaImF	pohybovat
pod	pod	k7c7	pod
8	[number]	k4	8
°	°	k?	°
<g/>
C.	C.	kA	C.
Fyzikální	fyzikální	k2eAgNnSc1d1	fyzikální
zrání	zrání	k1gNnSc1	zrání
smetany	smetana	k1gFnSc2	smetana
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
uzravačích	uzravač	k1gMnPc6	uzravač
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
speciálně	speciálně	k6eAd1	speciálně
navržené	navržený	k2eAgInPc1d1	navržený
tanky	tank	k1gInPc1	tank
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
efektivního	efektivní	k2eAgNnSc2d1	efektivní
chlazení	chlazení	k1gNnSc3	chlazení
smetany	smetana	k1gFnSc2	smetana
a	a	k8xC	a
uvnitř	uvnitř	k7c2	uvnitř
tanků	tank	k1gInPc2	tank
jsou	být	k5eAaImIp3nP	být
pomaloběžná	pomaloběžný	k2eAgNnPc1d1	pomaloběžný
míchadla	míchadlo	k1gNnPc1	míchadlo
<g/>
,	,	kIx,	,
zajišťující	zajišťující	k2eAgFnSc1d1	zajišťující
šetrné	šetrný	k2eAgNnSc4d1	šetrné
promíchání	promíchání	k1gNnSc4	promíchání
obsahu	obsah	k1gInSc2	obsah
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
porušení	porušení	k1gNnSc3	porušení
krystalické	krystalický	k2eAgFnSc2d1	krystalická
struktury	struktura	k1gFnSc2	struktura
mléčného	mléčný	k2eAgInSc2d1	mléčný
tuku	tuk	k1gInSc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Dobré	dobrý	k2eAgNnSc1d1	dobré
chlazení	chlazení	k1gNnSc1	chlazení
během	během	k7c2	během
fyzikálního	fyzikální	k2eAgNnSc2d1	fyzikální
zrání	zrání	k1gNnSc2	zrání
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
také	také	k9	také
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
uvolňování	uvolňování	k1gNnSc1	uvolňování
krystalizačního	krystalizační	k2eAgNnSc2d1	krystalizační
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
odvádět	odvádět	k5eAaImF	odvádět
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
teplo	teplo	k1gNnSc1	teplo
neodvádělo	odvádět	k5eNaImAgNnS	odvádět
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
nežádoucímu	žádoucí	k2eNgNnSc3d1	nežádoucí
zvýšení	zvýšení	k1gNnSc3	zvýšení
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
způsobit	způsobit	k5eAaPmF	způsobit
tání	tání	k1gNnSc4	tání
krystalů	krystal	k1gInPc2	krystal
tuku	tuk	k1gInSc2	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Biologické	biologický	k2eAgNnSc4d1	biologické
zrání	zrání	k1gNnSc4	zrání
se	se	k3xPyFc4	se
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
mlékárnách	mlékárna	k1gFnPc6	mlékárna
používá	používat	k5eAaImIp3nS	používat
pouze	pouze	k6eAd1	pouze
okrajově	okrajově	k6eAd1	okrajově
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
přídavek	přídavek	k1gInSc4	přídavek
smetanového	smetanový	k2eAgInSc2d1	smetanový
zákysu	zákys	k1gInSc2	zákys
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
částečnému	částečný	k2eAgNnSc3d1	částečné
snížení	snížení	k1gNnSc3	snížení
pH	ph	kA	ph
smetany	smetan	k1gInPc4	smetan
a	a	k8xC	a
takto	takto	k6eAd1	takto
vyrobené	vyrobený	k2eAgNnSc1d1	vyrobené
máslo	máslo	k1gNnSc1	máslo
má	mít	k5eAaImIp3nS	mít
výraznější	výrazný	k2eAgFnSc4d2	výraznější
<g/>
,	,	kIx,	,
nakyslou	nakyslý	k2eAgFnSc4d1	nakyslá
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
másla	máslo	k1gNnSc2	máslo
ze	z	k7c2	z
zakysané	zakysaný	k2eAgFnSc2d1	zakysaná
smetany	smetana	k1gFnSc2	smetana
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
delší	dlouhý	k2eAgFnSc4d2	delší
trvanlivost	trvanlivost	k1gFnSc4	trvanlivost
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
nižšímu	nízký	k2eAgInSc3d2	nižší
pH	ph	kA	ph
oproti	oproti	k7c3	oproti
nezakysanému	zakysaný	k2eNgNnSc3d1	zakysaný
máslu	máslo	k1gNnSc3	máslo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
nemožnost	nemožnost	k1gFnSc1	nemožnost
využít	využít	k5eAaPmF	využít
podmáslí	podmáslí	k1gNnSc4	podmáslí
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
šlehaného	šlehaný	k2eAgNnSc2d1	šlehané
podmáslí	podmáslí	k1gNnSc2	podmáslí
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
másla	máslo	k1gNnSc2	máslo
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nP	muset
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
reverzi	reverze	k1gFnSc3	reverze
fází	fáze	k1gFnPc2	fáze
z	z	k7c2	z
typu	typ	k1gInSc2	typ
olej	olej	k1gInSc4	olej
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
na	na	k7c4	na
typ	typ	k1gInSc4	typ
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
oleji	olej	k1gInSc6	olej
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
lze	lze	k6eAd1	lze
průmyslově	průmyslově	k6eAd1	průmyslově
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
několika	několik	k4yIc7	několik
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
zpěňovací	zpěňovací	k2eAgInSc1d1	zpěňovací
způsob	způsob	k1gInSc1	způsob
-	-	kIx~	-
probíhá	probíhat	k5eAaImIp3nS	probíhat
kontinuálně	kontinuálně	k6eAd1	kontinuálně
ve	v	k7c6	v
zmáselňovači	zmáselňovač	k1gInSc6	zmáselňovač
nebo	nebo	k8xC	nebo
diskontinuálně	diskontinuálně	k6eAd1	diskontinuálně
v	v	k7c6	v
máselnici	máselnice	k1gFnSc6	máselnice
koncentrační	koncentrační	k2eAgInSc4d1	koncentrační
způsob	způsob	k1gInSc4	způsob
-	-	kIx~	-
odstřeďováním	odstřeďování	k1gNnSc7	odstřeďování
smetany	smetana	k1gFnSc2	smetana
na	na	k7c4	na
tučnost	tučnost	k1gFnSc4	tučnost
másla	máslo	k1gNnSc2	máslo
a	a	k8xC	a
následná	následný	k2eAgFnSc1d1	následná
reverze	reverze	k1gFnSc1	reverze
fází	fáze	k1gFnPc2	fáze
emulgační	emulgační	k2eAgInSc1d1	emulgační
způsob	způsob	k1gInSc1	způsob
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
zpěňovací	zpěňovací	k2eAgInSc4d1	zpěňovací
způsob	způsob	k1gInSc4	způsob
výroby	výroba	k1gFnSc2	výroba
másla	máslo	k1gNnSc2	máslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
převažuje	převažovat	k5eAaImIp3nS	převažovat
kontinuální	kontinuální	k2eAgFnSc1d1	kontinuální
výroba	výroba	k1gFnSc1	výroba
ve	v	k7c6	v
zmáselňovači	zmáselňovač	k1gInSc6	zmáselňovač
<g/>
.	.	kIx.	.
</s>
<s>
Zmáselňovač	zmáselňovač	k1gInSc1	zmáselňovač
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
stloukací	stloukací	k2eAgInSc1d1	stloukací
válec	válec	k1gInSc1	válec
<g/>
,	,	kIx,	,
odlučovací	odlučovací	k2eAgInSc1d1	odlučovací
válec	válec	k1gInSc1	válec
a	a	k8xC	a
hnětač	hnětač	k1gInSc1	hnětač
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
stloukáním	stloukání	k1gNnSc7	stloukání
másla	máslo	k1gNnSc2	máslo
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
smetanu	smetana	k1gFnSc4	smetana
ohřát	ohřát	k5eAaPmF	ohřát
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
10	[number]	k4	10
až	až	k9	až
12	[number]	k4	12
°	°	k?	°
<g/>
C.	C.	kA	C.
Teplota	teplota	k1gFnSc1	teplota
předehřevu	předehřev	k1gInSc2	předehřev
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
obsahu	obsah	k1gInSc6	obsah
tuku	tuk	k1gInSc2	tuk
a	a	k8xC	a
ročním	roční	k2eAgNnSc6d1	roční
období	období	k1gNnSc6	období
(	(	kIx(	(
<g/>
obsahu	obsah	k1gInSc2	obsah
volných	volný	k2eAgFnPc2d1	volná
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stloukací	Stloukací	k2eAgInSc1d1	Stloukací
válec	válec	k1gInSc1	válec
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
našlehání	našlehání	k1gNnSc3	našlehání
smetany	smetana	k1gFnSc2	smetana
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
agregaci	agregace	k1gFnSc3	agregace
vykrystalizovaných	vykrystalizovaný	k2eAgInPc2d1	vykrystalizovaný
tukových	tukový	k2eAgInPc2d1	tukový
shluků	shluk	k1gInPc2	shluk
a	a	k8xC	a
tyto	tento	k3xDgInPc1	tento
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
koncentrují	koncentrovat	k5eAaBmIp3nP	koncentrovat
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
máselné	máselný	k2eAgNnSc4d1	máselné
zrno	zrno	k1gNnSc4	zrno
<g/>
.	.	kIx.	.
otáčky	otáčka	k1gFnSc2	otáčka
stloukacího	stloukací	k2eAgInSc2d1	stloukací
válce	válec	k1gInSc2	válec
mohou	moct	k5eAaImIp3nP	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
až	až	k9	až
3000	[number]	k4	3000
ot	ot	k1gMnSc1	ot
<g/>
/	/	kIx~	/
<g/>
min	min	kA	min
<g/>
.	.	kIx.	.
Počet	počet	k1gInSc1	počet
otáček	otáčka	k1gFnPc2	otáčka
se	se	k3xPyFc4	se
reguluje	regulovat	k5eAaImIp3nS	regulovat
podle	podle	k7c2	podle
tučnosti	tučnost	k1gFnSc2	tučnost
smetany	smetana	k1gFnSc2	smetana
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vytvořené	vytvořený	k2eAgNnSc1d1	vytvořené
máselné	máselný	k2eAgNnSc1d1	máselné
zrno	zrno	k1gNnSc1	zrno
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
odlučovacího	odlučovací	k2eAgInSc2d1	odlučovací
válce	válec	k1gInSc2	válec
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
shlukování	shlukování	k1gNnSc3	shlukování
máselného	máselný	k2eAgNnSc2d1	máselné
zrna	zrno	k1gNnSc2	zrno
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
mléčná	mléčný	k2eAgFnSc1d1	mléčná
plazma	plazma	k1gFnSc1	plazma
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
podmáslí	podmáslí	k1gNnSc1	podmáslí
<g/>
.	.	kIx.	.
</s>
<s>
Podmáslí	podmáslí	k1gNnSc1	podmáslí
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
bílkoviny	bílkovina	k1gFnPc4	bílkovina
<g/>
,	,	kIx,	,
volný	volný	k2eAgInSc4d1	volný
tuk	tuk	k1gInSc4	tuk
a	a	k8xC	a
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
rozpustné	rozpustný	k2eAgFnSc2d1	rozpustná
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
odlučovacím	odlučovací	k2eAgInSc6d1	odlučovací
válci	válec	k1gInSc6	válec
lze	lze	k6eAd1	lze
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
praní	praní	k1gNnSc4	praní
máselného	máselný	k2eAgNnSc2d1	máselné
zrna	zrno	k1gNnSc2	zrno
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
dokonalejší	dokonalý	k2eAgNnSc4d2	dokonalejší
odstranění	odstranění	k1gNnSc4	odstranění
mléčné	mléčný	k2eAgFnSc2d1	mléčná
plazmy	plazma	k1gFnSc2	plazma
propíráním	propírání	k1gNnSc7	propírání
máselného	máselný	k2eAgNnSc2d1	máselné
zrna	zrno	k1gNnSc2	zrno
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
trvanlivost	trvanlivost	k1gFnSc1	trvanlivost
másla	máslo	k1gNnSc2	máslo
a	a	k8xC	a
menší	malý	k2eAgInSc1d2	menší
obsah	obsah	k1gInSc1	obsah
laktózy	laktóza	k1gFnSc2	laktóza
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
osoby	osoba	k1gFnPc4	osoba
s	s	k7c7	s
laktózovou	laktózový	k2eAgFnSc7d1	laktózová
intolerancí	intolerance	k1gFnSc7	intolerance
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
výrazná	výrazný	k2eAgFnSc1d1	výrazná
chuť	chuť	k1gFnSc1	chuť
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
propíráním	propírání	k1gNnSc7	propírání
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
senzorických	senzorický	k2eAgFnPc2d1	senzorická
složek	složka	k1gFnPc2	složka
mléčné	mléčný	k2eAgFnSc2d1	mléčná
plazmy	plazma	k1gFnSc2	plazma
(	(	kIx(	(
<g/>
bílkoviny	bílkovina	k1gFnPc1	bílkovina
<g/>
,	,	kIx,	,
laktóza	laktóza	k1gFnSc1	laktóza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hnětači	hnětač	k1gInSc6	hnětač
je	být	k5eAaImIp3nS	být
umístěný	umístěný	k2eAgInSc1d1	umístěný
šnekový	šnekový	k2eAgInSc1d1	šnekový
dopravník	dopravník	k1gInSc1	dopravník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
protlačuje	protlačovat	k5eAaImIp3nS	protlačovat
máslo	máslo	k1gNnSc4	máslo
přes	přes	k7c4	přes
perforovanou	perforovaný	k2eAgFnSc4d1	perforovaná
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
Šnekový	šnekový	k2eAgInSc1d1	šnekový
dopravník	dopravník	k1gInSc1	dopravník
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
vzduchových	vzduchový	k2eAgFnPc2d1	vzduchová
bublin	bublina	k1gFnPc2	bublina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
při	při	k7c6	při
shlukování	shlukování	k1gNnSc6	shlukování
máselného	máselný	k2eAgNnSc2d1	máselné
zrna	zrno	k1gNnSc2	zrno
<g/>
.	.	kIx.	.
</s>
<s>
Perforovaná	perforovaný	k2eAgFnSc1d1	perforovaná
deska	deska	k1gFnSc1	deska
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
rovnoměrnému	rovnoměrný	k2eAgNnSc3d1	rovnoměrné
rozmístění	rozmístění	k1gNnSc3	rozmístění
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
másle	máslo	k1gNnSc6	máslo
a	a	k8xC	a
ke	k	k7c3	k
zmenšení	zmenšení	k1gNnSc3	zmenšení
kapének	kapénka	k1gFnPc2	kapénka
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgMnSc1d1	významný
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
trvanlivosti	trvanlivost	k1gFnSc2	trvanlivost
másla	máslo	k1gNnSc2	máslo
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
kapky	kapka	k1gFnPc1	kapka
vody	voda	k1gFnSc2	voda
by	by	kYmCp3nP	by
byly	být	k5eAaImAgInP	být
dobrým	dobrý	k2eAgInSc7d1	dobrý
zdrojem	zdroj	k1gInSc7	zdroj
živin	živina	k1gFnPc2	živina
pro	pro	k7c4	pro
bakterie	bakterie	k1gFnPc4	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
hnětení	hnětení	k1gNnSc2	hnětení
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
standardizaci	standardizace	k1gFnSc3	standardizace
(	(	kIx(	(
<g/>
úpravě	úprava	k1gFnSc3	úprava
obsahu	obsah	k1gInSc2	obsah
<g/>
)	)	kIx)	)
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
másle	máslo	k1gNnSc6	máslo
<g/>
.	.	kIx.	.
</s>
<s>
Máslo	máslo	k1gNnSc1	máslo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
projde	projít	k5eAaPmIp3nS	projít
hnětačem	hnětač	k1gInSc7	hnětač
je	být	k5eAaImIp3nS	být
dopravováno	dopravován	k2eAgNnSc1d1	dopravováno
na	na	k7c4	na
balicí	balicí	k2eAgFnSc4d1	balicí
linku	linka	k1gFnSc4	linka
<g/>
.	.	kIx.	.
</s>
<s>
Balení	balení	k1gNnSc1	balení
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
do	do	k7c2	do
klasických	klasický	k2eAgFnPc2d1	klasická
kostek	kostka	k1gFnPc2	kostka
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
250	[number]	k4	250
g	g	kA	g
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
máslo	máslo	k1gNnSc1	máslo
tvarováno	tvarovat	k5eAaImNgNnS	tvarovat
do	do	k7c2	do
větších	veliký	k2eAgInPc2d2	veliký
bloků	blok	k1gInPc2	blok
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
25	[number]	k4	25
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zabalení	zabalení	k1gNnSc6	zabalení
másla	máslo	k1gNnSc2	máslo
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
nutné	nutný	k2eAgNnSc1d1	nutné
nechat	nechat	k5eAaPmF	nechat
vyzrát	vyzrát	k5eAaPmF	vyzrát
krystalickou	krystalický	k2eAgFnSc4d1	krystalická
síť	síť	k1gFnSc4	síť
mléčného	mléčný	k2eAgInSc2d1	mléčný
tuku	tuk	k1gInSc2	tuk
v	v	k7c6	v
másle	máslo	k1gNnSc6	máslo
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
skladování	skladování	k1gNnSc2	skladování
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
získá	získat	k5eAaPmIp3nS	získat
máslo	máslo	k1gNnSc1	máslo
svoji	svůj	k3xOyFgFnSc4	svůj
konzistenci	konzistence	k1gFnSc4	konzistence
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
výroby	výroba	k1gFnSc2	výroba
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
šaržový	šaržový	k2eAgInSc4d1	šaržový
diskontinuální	diskontinuální	k2eAgInSc4d1	diskontinuální
proces	proces	k1gInSc4	proces
<g/>
.	.	kIx.	.
</s>
<s>
Smetana	smetana	k1gFnSc1	smetana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
prošla	projít	k5eAaPmAgFnS	projít
fyzikálním	fyzikální	k2eAgNnSc7d1	fyzikální
zráním	zrání	k1gNnSc7	zrání
je	být	k5eAaImIp3nS	být
dávkována	dávkován	k2eAgFnSc1d1	dávkována
do	do	k7c2	do
velkého	velký	k2eAgInSc2d1	velký
bubnu	buben	k1gInSc2	buben
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
uzavře	uzavřít	k5eAaPmIp3nS	uzavřít
a	a	k8xC	a
otáčením	otáčení	k1gNnSc7	otáčení
kolem	kolem	k7c2	kolem
vodorovné	vodorovný	k2eAgFnSc2d1	vodorovná
osy	osa	k1gFnSc2	osa
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
stloukání	stloukání	k1gNnSc3	stloukání
<g/>
.	.	kIx.	.
</s>
<s>
Otáčky	otáčka	k1gFnPc1	otáčka
bubnu	buben	k1gInSc2	buben
jsou	být	k5eAaImIp3nP	být
voleny	volit	k5eAaImNgFnP	volit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
převažovala	převažovat	k5eAaImAgFnS	převažovat
gravitace	gravitace	k1gFnSc1	gravitace
nad	nad	k7c7	nad
odstředivou	odstředivý	k2eAgFnSc7d1	odstředivá
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stlučení	stlučení	k1gNnSc6	stlučení
másla	máslo	k1gNnSc2	máslo
je	být	k5eAaImIp3nS	být
vypuštěno	vypuštěn	k2eAgNnSc1d1	vypuštěno
podmáslí	podmáslí	k1gNnSc1	podmáslí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
nashromáždilo	nashromáždit	k5eAaPmAgNnS	nashromáždit
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
válce	válec	k1gInSc2	válec
<g/>
.	.	kIx.	.
</s>
<s>
Máslo	máslo	k1gNnSc1	máslo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
propráno	proprán	k2eAgNnSc1d1	propráno
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
hnětení	hnětení	k1gNnSc1	hnětení
a	a	k8xC	a
úprava	úprava	k1gFnSc1	úprava
obsahu	obsah	k1gInSc2	obsah
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
balení	balení	k1gNnSc2	balení
<g/>
.	.	kIx.	.
</s>
<s>
Stloukání	stloukání	k1gNnSc1	stloukání
másla	máslo	k1gNnSc2	máslo
v	v	k7c6	v
máselnici	máselnice	k1gFnSc6	máselnice
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
pro	pro	k7c4	pro
malé	malý	k2eAgNnSc4d1	malé
množství	množství	k1gNnSc4	množství
smetany	smetana	k1gFnSc2	smetana
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nevyplatí	vyplatit	k5eNaPmIp3nS	vyplatit
rozjíždět	rozjíždět	k5eAaImF	rozjíždět
zmáselňovač	zmáselňovač	k1gInSc4	zmáselňovač
<g/>
.	.	kIx.	.
</s>
<s>
Kapacita	kapacita	k1gFnSc1	kapacita
máselnice	máselnice	k1gFnSc2	máselnice
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
např.	např.	kA	např.
500	[number]	k4	500
litrů	litr	k1gInPc2	litr
smetany	smetana	k1gFnSc2	smetana
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgFnPc1d1	Staré
technologie	technologie	k1gFnPc1	technologie
výroby	výroba	k1gFnSc2	výroba
másla	máslo	k1gNnSc2	máslo
lze	lze	k6eAd1	lze
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
másla	máslo	k1gNnSc2	máslo
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Máslovice	Máslovice	k1gFnSc2	Máslovice
severně	severně	k6eAd1	severně
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Máslo	máslo	k1gNnSc1	máslo
je	být	k5eAaImIp3nS	být
plnohodnotný	plnohodnotný	k2eAgInSc4d1	plnohodnotný
mléčný	mléčný	k2eAgInSc4d1	mléčný
tuk	tuk	k1gInSc4	tuk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
specifické	specifický	k2eAgNnSc4d1	specifické
aroma	aroma	k1gNnSc4	aroma
a	a	k8xC	a
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Aroma	aroma	k1gNnSc1	aroma
másla	máslo	k1gNnSc2	máslo
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
obsahem	obsah	k1gInSc7	obsah
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
s	s	k7c7	s
krátkou	krátký	k2eAgFnSc7d1	krátká
délkou	délka	k1gFnSc7	délka
řetězce	řetězec	k1gInSc2	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Chuť	chuť	k1gFnSc1	chuť
másla	máslo	k1gNnSc2	máslo
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
přítomnost	přítomnost	k1gFnSc1	přítomnost
mléčných	mléčný	k2eAgFnPc2d1	mléčná
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Máslo	máslo	k1gNnSc1	máslo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vitaminy	vitamin	k1gInPc4	vitamin
rozpustné	rozpustný	k2eAgInPc1d1	rozpustný
v	v	k7c6	v
tucích	tuk	k1gInPc6	tuk
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
A	A	kA	A
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
,	,	kIx,	,
K.	K.	kA	K.
Máslo	máslo	k1gNnSc1	máslo
také	také	k9	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
významný	významný	k2eAgInSc4d1	významný
podíl	podíl	k1gInSc4	podíl
trans	trans	k1gInSc1	trans
nenasycených	nasycený	k2eNgFnPc2d1	nenasycená
mastných	mastný	k2eAgFnPc2d1	mastná
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
konzumci	konzumec	k1gMnPc1	konzumec
másla	máslo	k1gNnSc2	máslo
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
omezit	omezit	k5eAaPmF	omezit
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
cholesterolu	cholesterol	k1gInSc2	cholesterol
a	a	k8xC	a
cévními	cévní	k2eAgFnPc7d1	cévní
potížemi	potíž	k1gFnPc7	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
závěry	závěr	k1gInPc1	závěr
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
diskutabilní	diskutabilní	k2eAgMnSc1d1	diskutabilní
<g/>
.	.	kIx.	.
</s>
<s>
Zdravým	zdravý	k2eAgInSc7d1	zdravý
lidem	lid	k1gInSc7	lid
by	by	kYmCp3nS	by
nemělo	mít	k5eNaImAgNnS	mít
máslo	máslo	k1gNnSc1	máslo
uškodit	uškodit	k5eAaPmF	uškodit
<g/>
.	.	kIx.	.
</s>
<s>
Problematika	problematika	k1gFnSc1	problematika
je	být	k5eAaImIp3nS	být
však	však	k9	však
individuální	individuální	k2eAgMnSc1d1	individuální
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
dodržování	dodržování	k1gNnSc1	dodržování
správné	správný	k2eAgFnSc2d1	správná
životosprávy	životospráva	k1gFnSc2	životospráva
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
složení	složení	k1gNnSc3	složení
je	být	k5eAaImIp3nS	být
máslo	máslo	k1gNnSc1	máslo
vhodné	vhodný	k2eAgNnSc1d1	vhodné
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
studenou	studený	k2eAgFnSc4d1	studená
kuchyni	kuchyně	k1gFnSc4	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Máslo	máslo	k1gNnSc1	máslo
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
nemělo	mít	k5eNaImAgNnS	mít
používat	používat	k5eAaImF	používat
pro	pro	k7c4	pro
smažení	smažení	k1gNnSc4	smažení
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
udává	udávat	k5eAaImIp3nS	udávat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
vitamínů	vitamín	k1gInPc2	vitamín
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
nutričních	nutriční	k2eAgInPc2d1	nutriční
parametrů	parametr	k1gInPc2	parametr
zjištěných	zjištěný	k2eAgInPc2d1	zjištěný
v	v	k7c6	v
soleném	solený	k2eAgNnSc6d1	solené
másle	máslo	k1gNnSc6	máslo
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
kdysi	kdysi	k6eAd1	kdysi
na	na	k7c6	na
Hané	Haná	k1gFnSc6	Haná
postupovalo	postupovat	k5eAaImAgNnS	postupovat
líčí	líčit	k5eAaImIp3nS	líčit
se	se	k3xPyFc4	se
všemi	všecek	k3xTgInPc7	všecek
zvyky	zvyk	k1gInPc7	zvyk
a	a	k8xC	a
pověrami	pověra	k1gFnPc7	pověra
J.	J.	kA	J.
Vyhlídal	vyhlídat	k5eAaImAgMnS	vyhlídat
<g/>
:	:	kIx,	:
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pomazánkové	pomazánkový	k2eAgNnSc1d1	pomazánkové
máslo	máslo	k1gNnSc1	máslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
dodnes	dodnes	k6eAd1	dodnes
populární	populární	k2eAgInSc4d1	populární
mlékárenský	mlékárenský	k2eAgInSc4d1	mlékárenský
výrobek	výrobek	k1gInSc4	výrobek
alternativní	alternativní	k2eAgInSc4d1	alternativní
k	k	k7c3	k
máslu	máslo	k1gNnSc3	máslo
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
dobře	dobře	k6eAd1	dobře
roztírá	roztírat	k5eAaImIp3nS	roztírat
v	v	k7c6	v
nízkých	nízký	k2eAgFnPc6d1	nízká
teplotách	teplota	k1gFnPc6	teplota
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
vyjmutí	vyjmutí	k1gNnSc6	vyjmutí
z	z	k7c2	z
chladničky	chladnička	k1gFnSc2	chladnička
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
nižší	nízký	k2eAgMnSc1d2	nižší
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
spotřebitele	spotřebitel	k1gMnPc4	spotřebitel
zajímavější	zajímavý	k2eAgFnSc2d2	zajímavější
energetické	energetický	k2eAgFnSc2d1	energetická
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
snadné	snadný	k2eAgFnSc3d1	snadná
roztíratelnosti	roztíratelnost	k1gFnSc3	roztíratelnost
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
máselná	máselný	k2eAgFnSc1d1	máselná
pomazánka	pomazánka	k1gFnSc1	pomazánka
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
jako	jako	k8xS	jako
pomazánkové	pomazánkový	k2eAgNnSc1d1	pomazánkové
máslo	máslo	k1gNnSc1	máslo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oficiálním	oficiální	k2eAgInSc6d1	oficiální
styku	styk	k1gInSc6	styk
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
název	název	k1gInSc1	název
zakázán	zakázán	k2eAgInSc1d1	zakázán
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
kvůli	kvůli	k7c3	kvůli
směrnici	směrnice	k1gFnSc3	směrnice
definující	definující	k2eAgInSc1d1	definující
pojem	pojem	k1gInSc1	pojem
máslo	máslo	k1gNnSc1	máslo
výhradně	výhradně	k6eAd1	výhradně
jako	jako	k8xS	jako
pravé	pravý	k2eAgNnSc1d1	pravé
máslo	máslo	k1gNnSc1	máslo
a	a	k8xC	a
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
tak	tak	k6eAd1	tak
změnila	změnit	k5eAaPmAgFnS	změnit
oficiální	oficiální	k2eAgInSc4d1	oficiální
standardizovaný	standardizovaný	k2eAgInSc4d1	standardizovaný
název	název	k1gInSc4	název
na	na	k7c4	na
tradiční	tradiční	k2eAgInPc4d1	tradiční
pomazánkové	pomazánkový	k2eAgInPc4d1	pomazánkový
a	a	k8xC	a
Slovensko	Slovensko	k1gNnSc4	Slovensko
na	na	k7c4	na
několik	několik	k4yIc4	několik
různých	různý	k2eAgInPc2d1	různý
nejednotných	jednotný	k2eNgInPc2d1	nejednotný
názvů	název	k1gInPc2	název
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Úlehlová-Tilschová	Úlehlová-Tilschová	k1gFnSc1	Úlehlová-Tilschová
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
strava	strava	k1gFnSc1	strava
lidová	lidový	k2eAgFnSc1d1	lidová
<g/>
,	,	kIx,	,
Družstevní	družstevní	k2eAgFnSc1d1	družstevní
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1945	[number]	k4	1945
Bambucké	Bambucké	k2eAgMnPc2d1	Bambucké
máslo	máslo	k1gNnSc4	máslo
Přepuštěné	přepuštěný	k2eAgNnSc1d1	přepuštěné
máslo	máslo	k1gNnSc1	máslo
Burákové	Burákové	k2eAgNnSc1d1	Burákové
máslo	máslo	k1gNnSc1	máslo
Podmáslí	podmáslí	k1gNnSc2	podmáslí
Máselnice	máselnice	k1gFnSc2	máselnice
Máslovník	Máslovník	k1gInSc1	Máslovník
africký	africký	k2eAgInSc1d1	africký
Chléb	chléb	k1gInSc4	chléb
s	s	k7c7	s
máslem	máslo	k1gNnSc7	máslo
Margarín	margarín	k1gInSc4	margarín
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
máslo	máslo	k1gNnSc4	máslo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
máslo	máslo	k1gNnSc4	máslo
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Malé	Malé	k2eAgNnSc4d1	Malé
máslovické	máslovický	k2eAgNnSc4d1	máslovické
muzeum	muzeum	k1gNnSc4	muzeum
másla	máslo	k1gNnSc2	máslo
</s>
