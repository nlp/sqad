<s>
Klasické	klasický	k2eAgNnSc1d1	klasické
máslo	máslo	k1gNnSc1	máslo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
minimálně	minimálně	k6eAd1	minimálně
80	[number]	k4	80
%	%	kIx~	%
mléčného	mléčný	k2eAgInSc2d1	mléčný
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
tvoří	tvořit	k5eAaImIp3nS	tvořit
voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
do	do	k7c2	do
16	[number]	k4	16
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
mléčná	mléčný	k2eAgFnSc1d1	mléčná
sušina	sušina	k1gFnSc1	sušina
(	(	kIx(	(
<g/>
bílkoviny	bílkovina	k1gFnPc1	bílkovina
<g/>
,	,	kIx,	,
laktóza	laktóza	k1gFnSc1	laktóza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
