<s>
Objev	objev	k1gInSc1	objev
krevních	krevní	k2eAgFnPc2d1	krevní
skupin	skupina	k1gFnPc2	skupina
AB0	AB0	k1gFnSc2	AB0
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
připisován	připisovat	k5eAaImNgInS	připisovat
vídeňskému	vídeňský	k2eAgMnSc3d1	vídeňský
vědci	vědec	k1gMnSc3	vědec
Karlu	Karel	k1gMnSc3	Karel
Landsteinerovi	Landsteiner	k1gMnSc3	Landsteiner
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
objevil	objevit	k5eAaPmAgMnS	objevit
tři	tři	k4xCgFnPc4	tři
krevní	krevní	k2eAgFnPc4d1	krevní
skupiny	skupina	k1gFnPc4	skupina
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
a	a	k8xC	a
C	C	kA	C
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgMnSc1d1	dnešní
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
a	a	k8xC	a
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
