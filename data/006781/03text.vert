<s>
Krevní	krevní	k2eAgFnSc1d1	krevní
skupina	skupina	k1gFnSc1	skupina
nebo	nebo	k8xC	nebo
přesněji	přesně	k6eAd2	přesně
krevní	krevní	k2eAgInSc1d1	krevní
typ	typ	k1gInSc1	typ
je	být	k5eAaImIp3nS	být
popis	popis	k1gInSc4	popis
vlastností	vlastnost	k1gFnPc2	vlastnost
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
jedince	jedinec	k1gMnSc2	jedinec
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
sacharidů	sacharid	k1gInPc2	sacharid
a	a	k8xC	a
bílkovin	bílkovina	k1gFnPc2	bílkovina
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
buněčné	buněčný	k2eAgFnSc6d1	buněčná
membráně	membrána	k1gFnSc6	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
nejdůležitější	důležitý	k2eAgFnPc1d3	nejdůležitější
klasifikace	klasifikace	k1gFnPc1	klasifikace
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
lidských	lidský	k2eAgFnPc2d1	lidská
krevních	krevní	k2eAgFnPc2d1	krevní
skupin	skupina	k1gFnPc2	skupina
jsou	být	k5eAaImIp3nP	být
AB0	AB0	k1gFnSc4	AB0
a	a	k8xC	a
Rhesus	Rhesus	k1gInSc4	Rhesus
faktor	faktor	k1gInSc1	faktor
(	(	kIx(	(
<g/>
Rh	Rh	k1gFnSc1	Rh
faktor	faktor	k1gInSc1	faktor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
známo	znám	k2eAgNnSc1d1	známo
dalších	další	k2eAgNnPc2d1	další
zhruba	zhruba	k6eAd1	zhruba
50	[number]	k4	50
systémů	systém	k1gInPc2	systém
krevních	krevní	k2eAgInPc2d1	krevní
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
než	než	k8xS	než
AB0	AB0	k1gFnSc7	AB0
a	a	k8xC	a
Rh	Rh	k1gFnSc7	Rh
<g/>
.	.	kIx.	.
</s>
<s>
Krevní	krevní	k2eAgFnSc1d1	krevní
skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
antigeny	antigen	k1gInPc7	antigen
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
antigenů	antigen	k1gInPc2	antigen
jsou	být	k5eAaImIp3nP	být
čisté	čistý	k2eAgFnPc1d1	čistá
bílkoviny	bílkovina	k1gFnPc1	bílkovina
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořen	k2eAgFnPc1d1	tvořena
bílkovinami	bílkovina	k1gFnPc7	bílkovina
s	s	k7c7	s
polysacharidy	polysacharid	k1gInPc7	polysacharid
<g/>
.	.	kIx.	.
</s>
<s>
Nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
některého	některý	k3yIgInSc2	některý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
znaků	znak	k1gInPc2	znak
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
přirozené	přirozený	k2eAgFnSc3d1	přirozená
produkci	produkce	k1gFnSc3	produkce
příslušných	příslušný	k2eAgFnPc2d1	příslušná
protilátek	protilátka	k1gFnPc2	protilátka
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krevní	krevní	k2eAgFnSc1d1	krevní
transfúze	transfúze	k1gFnSc1	transfúze
neidentické	identický	k2eNgFnSc2d1	neidentická
krevní	krevní	k2eAgFnSc2d1	krevní
skupiny	skupina	k1gFnSc2	skupina
by	by	kYmCp3nS	by
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
okamžité	okamžitý	k2eAgFnSc3d1	okamžitá
imunologické	imunologický	k2eAgFnSc3d1	imunologická
reakci	reakce	k1gFnSc3	reakce
a	a	k8xC	a
shlukování	shlukování	k1gNnSc3	shlukování
a	a	k8xC	a
rozpadu	rozpad	k1gInSc3	rozpad
krvinek	krvinka	k1gFnPc2	krvinka
darované	darovaný	k2eAgFnSc2d1	darovaná
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Darovaná	darovaný	k2eAgFnSc1d1	darovaná
krev	krev	k1gFnSc1	krev
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
organismem	organismus	k1gInSc7	organismus
odmítnuta	odmítnout	k5eAaPmNgFnS	odmítnout
a	a	k8xC	a
výsledný	výsledný	k2eAgInSc4d1	výsledný
produkt	produkt	k1gInSc4	produkt
chemické	chemický	k2eAgFnSc2d1	chemická
reakce	reakce	k1gFnSc2	reakce
by	by	kYmCp3nS	by
způsobil	způsobit	k5eAaPmAgInS	způsobit
akutní	akutní	k2eAgInPc4d1	akutní
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
vést	vést	k5eAaImF	vést
až	až	k9	až
k	k	k7c3	k
hemolytické	hemolytický	k2eAgFnSc3d1	hemolytická
anémii	anémie	k1gFnSc3	anémie
<g/>
,	,	kIx,	,
selhání	selhání	k1gNnSc4	selhání
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
,	,	kIx,	,
šoku	šok	k1gInSc2	šok
a	a	k8xC	a
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
kritérium	kritérium	k1gNnSc1	kritérium
pro	pro	k7c4	pro
dělení	dělení	k1gNnSc4	dělení
krve	krev	k1gFnSc2	krev
do	do	k7c2	do
skupin	skupina	k1gFnPc2	skupina
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
systém	systém	k1gInSc1	systém
AB	AB	kA	AB
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Lidská	lidský	k2eAgFnSc1d1	lidská
krev	krev	k1gFnSc1	krev
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
podle	podle	k7c2	podle
přítomnosti	přítomnost	k1gFnSc2	přítomnost
antigenů	antigen	k1gInPc2	antigen
(	(	kIx(	(
<g/>
aglutinogenů	aglutinogen	k1gInPc2	aglutinogen
<g/>
)	)	kIx)	)
A	A	kA	A
a	a	k8xC	a
B	B	kA	B
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
skupinu	skupina	k1gFnSc4	skupina
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
určitá	určitý	k2eAgFnSc1d1	určitá
kombinace	kombinace	k1gFnSc1	kombinace
protilátek	protilátka	k1gFnPc2	protilátka
(	(	kIx(	(
<g/>
aglutininů	aglutinin	k1gInPc2	aglutinin
<g/>
)	)	kIx)	)
vůči	vůči	k7c3	vůči
chybějícím	chybějící	k2eAgInPc3d1	chybějící
antigenům	antigen	k1gInPc3	antigen
<g/>
:	:	kIx,	:
krev	krev	k1gFnSc1	krev
typu	typ	k1gInSc2	typ
A	A	kA	A
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
antigen	antigen	k1gInSc4	antigen
A	a	k9	a
a	a	k8xC	a
protilátky	protilátka	k1gFnPc1	protilátka
anti-B	anti-B	k?	anti-B
krev	krev	k1gFnSc1	krev
typu	typ	k1gInSc2	typ
B	B	kA	B
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
antigen	antigen	k1gInSc4	antigen
B	B	kA	B
a	a	k8xC	a
protilátky	protilátka	k1gFnPc1	protilátka
anti-A	anti-A	k?	anti-A
krev	krev	k1gFnSc1	krev
typu	typ	k1gInSc2	typ
AB	AB	kA	AB
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
antigeny	antigen	k1gInPc4	antigen
A	a	k8xC	a
i	i	k9	i
B	B	kA	B
a	a	k8xC	a
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
protilátky	protilátka	k1gFnPc4	protilátka
anti-A	anti-A	k?	anti-A
ani	ani	k8xC	ani
anti-B	anti-B	k?	anti-B
krev	krev	k1gFnSc1	krev
typu	typ	k1gInSc2	typ
0	[number]	k4	0
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
antigeny	antigen	k1gInPc4	antigen
A	A	kA	A
ani	ani	k8xC	ani
B	B	kA	B
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
protilátky	protilátka	k1gFnPc4	protilátka
anti-A	anti-A	k?	anti-A
i	i	k8xC	i
anti-B	anti-B	k?	anti-B
Na	na	k7c6	na
světě	svět	k1gInSc6	svět
je	být	k5eAaImIp3nS	být
nejčastější	častý	k2eAgInSc1d3	nejčastější
krevní	krevní	k2eAgInSc1d1	krevní
typ	typ	k1gInSc1	typ
0	[number]	k4	0
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
a	a	k8xC	a
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejběžnější	běžný	k2eAgInSc1d3	nejběžnější
typ	typ	k1gInSc1	typ
A.	A.	kA	A.
Typ	typ	k1gInSc1	typ
AB	AB	kA	AB
je	být	k5eAaImIp3nS	být
nejméně	málo	k6eAd3	málo
častý	častý	k2eAgInSc1d1	častý
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
popsána	popsán	k2eAgNnPc1d1	popsáno
určitá	určitý	k2eAgNnPc1d1	určité
regionální	regionální	k2eAgNnPc1d1	regionální
a	a	k8xC	a
rasová	rasový	k2eAgNnPc1d1	rasové
rozdělení	rozdělení	k1gNnPc1	rozdělení
lidské	lidský	k2eAgFnSc2d1	lidská
krve	krev	k1gFnSc2	krev
podle	podle	k7c2	podle
přítomnosti	přítomnost	k1gFnSc2	přítomnost
antigenů	antigen	k1gInPc2	antigen
AB0	AB0	k1gMnSc1	AB0
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
uvádí	uvádět	k5eAaImIp3nS	uvádět
tento	tento	k3xDgInSc4	tento
podíl	podíl	k1gInSc4	podíl
krevních	krevní	k2eAgFnPc2d1	krevní
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
45	[number]	k4	45
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
má	mít	k5eAaImIp3nS	mít
skupinu	skupina	k1gFnSc4	skupina
A	A	kA	A
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
%	%	kIx~	%
skupinu	skupina	k1gFnSc4	skupina
B	B	kA	B
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
%	%	kIx~	%
skupinu	skupina	k1gFnSc4	skupina
0	[number]	k4	0
a	a	k8xC	a
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
má	mít	k5eAaImIp3nS	mít
krevní	krevní	k2eAgFnSc4d1	krevní
skupinu	skupina	k1gFnSc4	skupina
AB	AB	kA	AB
<g/>
.	.	kIx.	.
</s>
<s>
Přesný	přesný	k2eAgInSc1d1	přesný
důvod	důvod	k1gInSc1	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
lidský	lidský	k2eAgInSc1d1	lidský
organismus	organismus	k1gInSc1	organismus
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
protilátky	protilátka	k1gFnPc4	protilátka
k	k	k7c3	k
antigenu	antigen	k1gInSc3	antigen
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgMnSc7	který
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nesetkal	setkat	k5eNaPmAgMnS	setkat
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
vědecky	vědecky	k6eAd1	vědecky
popsán	popsat	k5eAaPmNgInS	popsat
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
určité	určitý	k2eAgInPc1d1	určitý
bakteriální	bakteriální	k2eAgInPc1d1	bakteriální
antigeny	antigen	k1gInPc1	antigen
jsou	být	k5eAaImIp3nP	být
stejné	stejný	k2eAgInPc1d1	stejný
u	u	k7c2	u
glykoproteinů	glykoprotein	k1gInPc2	glykoprotein
A	a	k9	a
i	i	k9	i
B	B	kA	B
a	a	k8xC	a
protilátky	protilátka	k1gFnPc1	protilátka
vytvořené	vytvořený	k2eAgFnPc1d1	vytvořená
proti	proti	k7c3	proti
těmto	tento	k3xDgFnPc3	tento
bakteriím	bakterie	k1gFnPc3	bakterie
reagují	reagovat	k5eAaBmIp3nP	reagovat
s	s	k7c7	s
krví	krev	k1gFnSc7	krev
cizího	cizí	k2eAgInSc2d1	cizí
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Antigeny	antigen	k1gInPc1	antigen
AB0	AB0	k1gMnPc2	AB0
se	se	k3xPyFc4	se
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
červených	červený	k2eAgFnPc6d1	červená
krvinkách	krvinka	k1gFnPc6	krvinka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
tkáních	tkáň	k1gFnPc6	tkáň
(	(	kIx(	(
<g/>
játra	játra	k1gNnPc4	játra
<g/>
,	,	kIx,	,
ledviny	ledvina	k1gFnPc4	ledvina
<g/>
,	,	kIx,	,
plíce	plíce	k1gFnPc4	plíce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgFnPc1d1	jiná
také	také	k9	také
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
krvácení	krvácení	k1gNnSc4	krvácení
a	a	k8xC	a
srážení	srážení	k1gNnSc4	srážení
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Krevní	krevní	k2eAgFnPc1d1	krevní
skupiny	skupina	k1gFnPc1	skupina
se	se	k3xPyFc4	se
dědí	dědit	k5eAaImIp3nP	dědit
po	po	k7c6	po
obou	dva	k4xCgMnPc6	dva
rodičích	rodič	k1gMnPc6	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Typ	typ	k1gInSc1	typ
krve	krev	k1gFnSc2	krev
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
jediným	jediný	k2eAgInSc7d1	jediný
genem	gen	k1gInSc7	gen
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
alelami	alela	k1gFnPc7	alela
<g/>
:	:	kIx,	:
i	i	k9	i
<g/>
,	,	kIx,	,
IA	ia	k0	ia
a	a	k8xC	a
IB	IB	kA	IB
<g/>
.	.	kIx.	.
</s>
<s>
Gen	gen	k1gInSc1	gen
kóduje	kódovat	k5eAaBmIp3nS	kódovat
enzym	enzym	k1gInSc1	enzym
glykosyltransferázu	glykosyltransferáza	k1gFnSc4	glykosyltransferáza
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mění	měnit	k5eAaImIp3nS	měnit
sacharidy	sacharid	k1gInPc4	sacharid
antigenů	antigen	k1gInPc2	antigen
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
.	.	kIx.	.
</s>
<s>
Gen	gen	k1gInSc1	gen
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
rameni	rameno	k1gNnSc3	rameno
devátého	devátý	k4xOgInSc2	devátý
chromozomu	chromozom	k1gInSc2	chromozom
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
q	q	k?	q
<g/>
34	[number]	k4	34
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alela	alela	k1gFnSc1	alela
IA	ia	k0	ia
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
typu	typa	k1gFnSc4	typa
A	A	kA	A
<g/>
,	,	kIx,	,
IB	IB	kA	IB
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
typu	typa	k1gFnSc4	typa
B	B	kA	B
a	a	k8xC	a
i	i	k9	i
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
typu	typ	k1gInSc2	typ
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
IA	ia	k0	ia
a	a	k8xC	a
IB	IB	kA	IB
jsou	být	k5eAaImIp3nP	být
dominantní	dominantní	k2eAgMnPc1d1	dominantní
nad	nad	k7c7	nad
i	i	k9	i
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
alelami	alela	k1gFnPc7	alela
ii	ii	k?	ii
mají	mít	k5eAaImIp3nP	mít
typ	typ	k1gInSc4	typ
0	[number]	k4	0
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
lelami	lela	k1gFnPc7	lela
IAIA	IAIA	kA	IAIA
nebo	nebo	k8xC	nebo
IAi	IAi	k1gMnPc1	IAi
mají	mít	k5eAaImIp3nP	mít
typ	typ	k1gInSc4	typ
A	a	k9	a
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
alelami	alela	k1gFnPc7	alela
IBIB	IBIB	kA	IBIB
nebo	nebo	k8xC	nebo
IBi	IBi	k1gMnPc1	IBi
mají	mít	k5eAaImIp3nP	mít
typ	typ	k1gInSc4	typ
B.	B.	kA	B.
Lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
alelami	alela	k1gFnPc7	alela
IAIB	IAIB	kA	IAIB
mají	mít	k5eAaImIp3nP	mít
oba	dva	k4xCgInPc1	dva
fenotypy	fenotyp	k1gInPc1	fenotyp
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
A	A	kA	A
a	a	k8xC	a
B	B	kA	B
jsou	být	k5eAaImIp3nP	být
kodominantní	kodominantní	k2eAgFnPc1d1	kodominantní
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
rodiče	rodič	k1gMnPc4	rodič
s	s	k7c7	s
krevním	krevní	k2eAgInSc7d1	krevní
typem	typ	k1gInSc7	typ
AB	AB	kA	AB
prakticky	prakticky	k6eAd1	prakticky
nemožné	možný	k2eNgNnSc1d1	nemožné
mít	mít	k5eAaImF	mít
dítě	dítě	k1gNnSc4	dítě
s	s	k7c7	s
typem	typ	k1gInSc7	typ
0	[number]	k4	0
(	(	kIx(	(
<g/>
i	i	k8xC	i
když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
přímý	přímý	k2eAgInSc4d1	přímý
důkaz	důkaz	k1gInSc4	důkaz
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dítě	dítě	k1gNnSc1	dítě
není	být	k5eNaImIp3nS	být
jeho	jeho	k3xOp3gNnSc4	jeho
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Evoluční	evoluční	k2eAgMnPc1d1	evoluční
biologové	biolog	k1gMnPc1	biolog
přijímají	přijímat	k5eAaImIp3nP	přijímat
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
alela	alela	k1gFnSc1	alela
IA	ia	k0	ia
se	se	k3xPyFc4	se
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
první	první	k4xOgFnSc1	první
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
alela	alela	k1gFnSc1	alela
i	i	k9	i
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
stačilo	stačit	k5eAaBmAgNnS	stačit
odstranění	odstranění	k1gNnSc4	odstranění
jediného	jediný	k2eAgInSc2d1	jediný
nukleotidu	nukleotid	k1gInSc2	nukleotid
<g/>
,	,	kIx,	,
což	což	k9	což
zbývající	zbývající	k2eAgInPc4d1	zbývající
nukleotidy	nukleotid	k1gInPc4	nukleotid
posunulo	posunout	k5eAaPmAgNnS	posunout
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
poslední	poslední	k2eAgFnSc1d1	poslední
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
alela	alela	k1gFnSc1	alela
IB	IB	kA	IB
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
chronologii	chronologie	k1gFnSc3	chronologie
také	také	k9	také
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
zastoupení	zastoupení	k1gNnSc4	zastoupení
krevních	krevní	k2eAgInPc2d1	krevní
typů	typ	k1gInPc2	typ
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
konzistentní	konzistentní	k2eAgInSc1d1	konzistentní
s	s	k7c7	s
obecně	obecně	k6eAd1	obecně
přijímanými	přijímaný	k2eAgInPc7d1	přijímaný
přesuny	přesun	k1gInPc7	přesun
populace	populace	k1gFnSc2	populace
a	a	k8xC	a
převládajícími	převládající	k2eAgInPc7d1	převládající
krevními	krevní	k2eAgInPc7d1	krevní
typy	typ	k1gInPc7	typ
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
typ	typ	k1gInSc1	typ
B	B	kA	B
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
častý	častý	k2eAgInSc1d1	častý
v	v	k7c6	v
asijských	asijský	k2eAgFnPc6d1	asijská
populacích	populace	k1gFnPc6	populace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
příliš	příliš	k6eAd1	příliš
častý	častý	k2eAgInSc1d1	častý
v	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
<g/>
.	.	kIx.	.
</s>
<s>
Objev	objev	k1gInSc1	objev
krevních	krevní	k2eAgFnPc2d1	krevní
skupin	skupina	k1gFnPc2	skupina
AB0	AB0	k1gFnSc2	AB0
je	být	k5eAaImIp3nS	být
všeobecně	všeobecně	k6eAd1	všeobecně
připisován	připisovat	k5eAaImNgInS	připisovat
vídeňskému	vídeňský	k2eAgMnSc3d1	vídeňský
vědci	vědec	k1gMnSc3	vědec
Karlu	Karel	k1gMnSc3	Karel
Landsteinerovi	Landsteiner	k1gMnSc3	Landsteiner
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
objevil	objevit	k5eAaPmAgMnS	objevit
tři	tři	k4xCgFnPc4	tři
krevní	krevní	k2eAgFnPc4d1	krevní
skupiny	skupina	k1gFnPc4	skupina
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
a	a	k8xC	a
C	C	kA	C
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgMnSc1d1	dnešní
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
a	a	k8xC	a
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
objev	objev	k1gInSc4	objev
dostal	dostat	k5eAaPmAgMnS	dostat
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziologii	fyziologie	k1gFnSc4	fyziologie
a	a	k8xC	a
lékařství	lékařství	k1gNnSc4	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
americká	americký	k2eAgFnSc1d1	americká
lékařská	lékařský	k2eAgFnSc1d1	lékařská
komise	komise	k1gFnSc1	komise
dala	dát	k5eAaPmAgFnS	dát
přednost	přednost	k1gFnSc4	přednost
Janskému	janský	k2eAgInSc3d1	janský
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
sice	sice	k8xC	sice
krevní	krevní	k2eAgFnSc2d1	krevní
skupiny	skupina	k1gFnSc2	skupina
objevil	objevit	k5eAaPmAgMnS	objevit
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Landsteinera	Landsteiner	k1gMnSc2	Landsteiner
všechny	všechen	k3xTgMnPc4	všechen
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
popsali	popsat	k5eAaPmAgMnP	popsat
rakouští	rakouský	k2eAgMnPc1d1	rakouský
vědci	vědec	k1gMnPc1	vědec
A.	A.	kA	A.
van	van	k1gInSc1	van
Decastello	Decastello	k1gNnSc1	Decastello
a	a	k8xC	a
A.	A.	kA	A.
Sturli	Sturl	k1gMnPc1	Sturl
krevní	krevní	k2eAgFnSc4d1	krevní
skupinu	skupina	k1gFnSc4	skupina
AB	AB	kA	AB
jako	jako	k9	jako
"	"	kIx"	"
<g/>
výjimku	výjimka	k1gFnSc4	výjimka
z	z	k7c2	z
Ladsteinerova	Ladsteinerův	k2eAgNnSc2d1	Ladsteinerův
pravidla	pravidlo	k1gNnSc2	pravidlo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1907	[number]	k4	1907
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
popsal	popsat	k5eAaPmAgMnS	popsat
český	český	k2eAgMnSc1d1	český
psychiatr	psychiatr	k1gMnSc1	psychiatr
Jan	Jan	k1gMnSc1	Jan
Janský	janský	k2eAgInSc4d1	janský
čtvrtou	čtvrtá	k1gFnSc7	čtvrtá
krevní	krevní	k2eAgFnSc7d1	krevní
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
znaky	znak	k1gInPc4	znak
A	A	kA	A
i	i	k8xC	i
B.	B.	kA	B.
Janský	janský	k2eAgMnSc1d1	janský
používal	používat	k5eAaImAgInS	používat
označení	označení	k1gNnSc4	označení
skupin	skupina	k1gFnPc2	skupina
I	I	kA	I
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
,	,	kIx,	,
III	III	kA	III
a	a	k8xC	a
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
Janském	janský	k2eAgInSc6d1	janský
popsal	popsat	k5eAaPmAgMnS	popsat
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
čtyři	čtyři	k4xCgFnPc4	čtyři
krevní	krevní	k2eAgFnSc2d1	krevní
skupiny	skupina	k1gFnSc2	skupina
Američan	Američan	k1gMnSc1	Američan
William	William	k1gInSc1	William
Lorenzo	Lorenza	k1gFnSc5	Lorenza
Moss	Moss	k1gInSc1	Moss
<g/>
.	.	kIx.	.
</s>
<s>
Použil	použít	k5eAaPmAgMnS	použít
také	také	k9	také
označení	označení	k1gNnSc1	označení
římskými	římský	k2eAgFnPc7d1	římská
číslicemi	číslice	k1gFnPc7	číslice
I	i	k8xC	i
<g/>
–	–	k?	–
<g/>
IV	Iva	k1gFnPc2	Iva
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
opačném	opačný	k2eAgNnSc6d1	opačné
pořadí	pořadí	k1gNnSc6	pořadí
než	než	k8xS	než
Janský	janský	k2eAgInSc1d1	janský
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
sjednotilo	sjednotit	k5eAaPmAgNnS	sjednotit
označování	označování	k1gNnSc1	označování
krevních	krevní	k2eAgInPc2d1	krevní
typů	typ	k1gInPc2	typ
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
AB	AB	kA	AB
a	a	k8xC	a
0	[number]	k4	0
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
označení	označení	k1gNnSc2	označení
Landsteinerova	Landsteinerův	k2eAgNnSc2d1	Landsteinerův
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jazycích	jazyk	k1gInPc6	jazyk
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
angličtině	angličtina	k1gFnSc6	angličtina
,	,	kIx,	,
španělštině	španělština	k1gFnSc6	španělština
nebo	nebo	k8xC	nebo
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
zápis	zápis	k1gInSc4	zápis
ABO	ABO	kA	ABO
s	s	k7c7	s
písmenem	písmeno	k1gNnSc7	písmeno
O	O	kA	O
(	(	kIx(	(
<g/>
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
old	old	k?	old
<g/>
=	=	kIx~	=
<g/>
starý	starý	k2eAgMnSc1d1	starý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiných	jiná	k1gFnPc6	jiná
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
němčině	němčina	k1gFnSc6	němčina
nebo	nebo	k8xC	nebo
slovenštině	slovenština	k1gFnSc6	slovenština
<g/>
)	)	kIx)	)
zápis	zápis	k1gInSc1	zápis
AB0	AB0	k1gFnSc2	AB0
s	s	k7c7	s
nulou	nula	k1gFnSc7	nula
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
anglicky	anglicky	k6eAd1	anglicky
psaných	psaný	k2eAgInPc6d1	psaný
materiálech	materiál	k1gInPc6	materiál
používá	používat	k5eAaImIp3nS	používat
zápis	zápis	k1gInSc1	zápis
ABO	ABO	kA	ABO
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
významný	významný	k2eAgInSc1d1	významný
krevní	krevní	k2eAgInSc1d1	krevní
typ	typ	k1gInSc1	typ
rozlišující	rozlišující	k2eAgFnSc1d1	rozlišující
krev	krev	k1gFnSc1	krev
podle	podle	k7c2	podle
tzv.	tzv.	kA	tzv.
Rhesus	Rhesus	k1gInSc1	Rhesus
faktoru	faktor	k1gInSc2	faktor
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
Rh	Rh	k1gFnPc4	Rh
faktoru	faktor	k1gInSc2	faktor
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
popsán	popsat	k5eAaPmNgInS	popsat
Karlem	Karel	k1gMnSc7	Karel
Landsteinerem	Landsteiner	k1gMnSc7	Landsteiner
a	a	k8xC	a
Alexanderem	Alexander	k1gMnSc7	Alexander
Wienerem	Wiener	k1gMnSc7	Wiener
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Rh	Rh	k?	Rh
faktor	faktor	k1gInSc1	faktor
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
podle	podle	k7c2	podle
makaků	makak	k1gMnPc2	makak
Macaca	Macacus	k1gMnSc2	Macacus
mulatta	mulatt	k1gMnSc2	mulatt
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Rhesus	Rhesus	k1gMnSc1	Rhesus
Macaque	Macaqu	k1gFnSc2	Macaqu
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgFnPc2	který
jej	on	k3xPp3gInSc4	on
Landsteiner	Landsteiner	k1gInSc4	Landsteiner
a	a	k8xC	a
Wiener	Wiener	k1gInSc4	Wiener
objevili	objevit	k5eAaPmAgMnP	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Rh	Rh	k?	Rh
faktor	faktor	k1gInSc1	faktor
je	být	k5eAaImIp3nS	být
zapříčiněn	zapříčinit	k5eAaPmNgInS	zapříčinit
skupinou	skupina	k1gFnSc7	skupina
zhruba	zhruba	k6eAd1	zhruba
40	[number]	k4	40
antigenů	antigen	k1gInPc2	antigen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejvýznačnějších	význačný	k2eAgInPc6d3	nejvýznačnější
je	být	k5eAaImIp3nS	být
pět	pět	k4xCc1	pět
antigenů	antigen	k1gInPc2	antigen
uložených	uložený	k2eAgInPc2d1	uložený
na	na	k7c6	na
třech	tři	k4xCgInPc6	tři
genech	gen	k1gInPc6	gen
<g/>
:	:	kIx,	:
antigen	antigen	k1gInSc1	antigen
C	C	kA	C
<g/>
:	:	kIx,	:
genotypy	genotyp	k1gInPc1	genotyp
CC	CC	kA	CC
nebo	nebo	k8xC	nebo
Cc	Cc	k1gMnSc1	Cc
antigen	antigen	k1gInSc4	antigen
c	c	k0	c
<g/>
:	:	kIx,	:
genotyp	genotyp	k1gInSc1	genotyp
cc	cc	k?	cc
antigen	antigen	k1gInSc1	antigen
D	D	kA	D
<g/>
:	:	kIx,	:
genotypy	genotyp	k1gInPc1	genotyp
DD	DD	kA	DD
nebo	nebo	k8xC	nebo
Dd	Dd	k1gFnSc1	Dd
antigen	antigen	k1gInSc1	antigen
E	E	kA	E
<g/>
:	:	kIx,	:
genotypy	genotyp	k1gInPc1	genotyp
EE	EE	kA	EE
nebo	nebo	k8xC	nebo
Ee	Ee	k1gMnSc1	Ee
antigen	antigen	k1gInSc4	antigen
e	e	k0	e
<g/>
:	:	kIx,	:
genotyp	genotyp	k1gInSc1	genotyp
ee	ee	k?	ee
Nejsilnější	silný	k2eAgInSc1d3	nejsilnější
je	být	k5eAaImIp3nS	být
antigen	antigen	k1gInSc1	antigen
D	D	kA	D
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
antigen	antigen	k1gInSc4	antigen
D	D	kA	D
přítomen	přítomen	k2eAgMnSc1d1	přítomen
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
červených	červený	k2eAgFnPc2d1	červená
krvinek	krvinka	k1gFnPc2	krvinka
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
krev	krev	k1gFnSc1	krev
jako	jako	k8xS	jako
Rh	Rh	k1gFnSc1	Rh
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
v	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
pak	pak	k6eAd1	pak
Rh	Rh	k1gMnSc1	Rh
<g/>
−	−	k?	−
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
Rh	Rh	k1gFnSc2	Rh
faktoru	faktor	k1gInSc2	faktor
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
typem	typ	k1gInSc7	typ
AB0	AB0	k1gFnSc2	AB0
a	a	k8xC	a
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
se	se	k3xPyFc4	se
např.	např.	kA	např.
jako	jako	k8xC	jako
A	A	kA	A
<g/>
−	−	k?	−
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
krví	krev	k1gFnSc7	krev
bez	bez	k7c2	bez
antigenu	antigen	k1gInSc2	antigen
D	D	kA	D
nemohou	moct	k5eNaImIp3nP	moct
přijímat	přijímat	k5eAaImF	přijímat
krev	krev	k1gFnSc4	krev
Rh	Rh	k1gFnSc2	Rh
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
proti	proti	k7c3	proti
antigenu	antigen	k1gInSc3	antigen
D	D	kA	D
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
protilátky	protilátka	k1gFnPc4	protilátka
a	a	k8xC	a
darovanou	darovaný	k2eAgFnSc4d1	darovaná
krev	krev	k1gFnSc4	krev
by	by	kYmCp3nP	by
nepřijali	přijmout	k5eNaPmAgMnP	přijmout
–	–	k?	–
došlo	dojít	k5eAaPmAgNnS	dojít
by	by	k9	by
k	k	k7c3	k
hemolytické	hemolytický	k2eAgFnSc3d1	hemolytická
reakci	reakce	k1gFnSc3	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
antigenu	antigen	k1gInSc2	antigen
D	D	kA	D
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
tzv.	tzv.	kA	tzv.
hemolytické	hemolytický	k2eAgFnSc2d1	hemolytická
nemoci	nemoc	k1gFnSc2	nemoc
novorozenců	novorozenec	k1gMnPc2	novorozenec
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
projevit	projevit	k5eAaPmF	projevit
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
matky	matka	k1gFnSc2	matka
s	s	k7c7	s
krví	krev	k1gFnSc7	krev
Rh	Rh	k1gMnSc2	Rh
<g/>
−	−	k?	−
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
již	již	k6eAd1	již
porodila	porodit	k5eAaPmAgFnS	porodit
dítě	dítě	k1gNnSc4	dítě
s	s	k7c7	s
Rh	Rh	k1gMnPc7	Rh
<g/>
+	+	kIx~	+
otcem	otec	k1gMnSc7	otec
(	(	kIx(	(
<g/>
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
dítě	dítě	k1gNnSc1	dítě
mělo	mít	k5eAaImAgNnS	mít
Rh	Rh	k1gFnSc7	Rh
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
obdržela	obdržet	k5eAaPmAgFnS	obdržet
transfúzi	transfúze	k1gFnSc4	transfúze
krve	krev	k1gFnSc2	krev
Rh	Rh	k1gFnSc2	Rh
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
těle	tělo	k1gNnSc6	tělo
jsou	být	k5eAaImIp3nP	být
již	již	k6eAd1	již
vytvořeny	vytvořit	k5eAaPmNgFnP	vytvořit
protilátky	protilátka	k1gFnPc1	protilátka
anti-D	anti-D	k?	anti-D
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
ohrozit	ohrozit	k5eAaPmF	ohrozit
nový	nový	k2eAgInSc4d1	nový
plod	plod	k1gInSc4	plod
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
neřešitelná	řešitelný	k2eNgFnSc1d1	neřešitelná
situace	situace	k1gFnSc1	situace
(	(	kIx(	(
<g/>
protože	protože	k8xS	protože
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
úmrtím	úmrť	k1gFnPc3	úmrť
dalších	další	k2eAgFnPc2d1	další
dětí	dítě	k1gFnPc2	dítě
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
řeší	řešit	k5eAaImIp3nS	řešit
velice	velice	k6eAd1	velice
jednoduše	jednoduše	k6eAd1	jednoduše
injekčním	injekční	k2eAgNnSc7d1	injekční
podáním	podání	k1gNnSc7	podání
antiglobulinu	antiglobulin	k2eAgFnSc4d1	antiglobulin
matce	matka	k1gFnSc3	matka
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
s	s	k7c7	s
Rh	Rh	k1gFnSc7	Rh
<g/>
+	+	kIx~	+
narozené	narozený	k2eAgFnSc3d1	narozená
matce	matka	k1gFnSc3	matka
s	s	k7c7	s
Rh-	Rh-	k1gMnPc7	Rh-
pak	pak	k6eAd1	pak
mají	mít	k5eAaImIp3nP	mít
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
pouze	pouze	k6eAd1	pouze
menší	malý	k2eAgFnSc1d2	menší
komplikace	komplikace	k1gFnSc1	komplikace
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
novorozenecké	novorozenecký	k2eAgFnSc2d1	novorozenecká
žloutenky	žloutenka	k1gFnSc2	žloutenka
<g/>
.	.	kIx.	.
</s>
<s>
Genetické	genetický	k2eAgFnPc4d1	genetická
informace	informace	k1gFnPc4	informace
ovlivňující	ovlivňující	k2eAgFnSc1d1	ovlivňující
Rh	Rh	k1gFnSc1	Rh
faktor	faktor	k1gInSc1	faktor
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
podle	podle	k7c2	podle
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
uvedená	uvedený	k2eAgFnSc1d1	uvedená
tabulka	tabulka	k1gFnSc1	tabulka
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
předpokládanou	předpokládaný	k2eAgFnSc4d1	předpokládaná
četnost	četnost	k1gFnSc4	četnost
Rh	Rh	k1gFnSc2	Rh
typů	typ	k1gInPc2	typ
krve	krev	k1gFnSc2	krev
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
výskytu	výskyt	k1gInSc6	výskyt
příslušných	příslušný	k2eAgInPc2d1	příslušný
genotypů	genotyp	k1gInPc2	genotyp
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dat	datum	k1gNnPc2	datum
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
s	s	k7c7	s
krví	krev	k1gFnSc7	krev
Rh	Rh	k1gMnSc2	Rh
<g/>
−	−	k?	−
je	být	k5eAaImIp3nS	být
riskantní	riskantní	k2eAgMnSc1d1	riskantní
cestovat	cestovat	k5eAaImF	cestovat
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
částí	část	k1gFnPc2	část
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
zásoby	zásoba	k1gFnPc1	zásoba
krve	krev	k1gFnSc2	krev
Rh	Rh	k1gFnSc1	Rh
<g/>
−	−	k?	−
jen	jen	k9	jen
malé	malý	k2eAgNnSc1d1	malé
(	(	kIx(	(
<g/>
obzvláště	obzvláště	k6eAd1	obzvláště
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Asii	Asie	k1gFnSc6	Asie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
transfúzní	transfúzní	k2eAgFnPc1d1	transfúzní
stanice	stanice	k1gFnPc1	stanice
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
podporovat	podporovat	k5eAaImF	podporovat
dárce	dárce	k1gMnSc1	dárce
krve	krev	k1gFnSc2	krev
evropského	evropský	k2eAgInSc2d1	evropský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
populaci	populace	k1gFnSc6	populace
je	být	k5eAaImIp3nS	být
nejčastější	častý	k2eAgFnSc1d3	nejčastější
krevní	krevní	k2eAgFnSc1d1	krevní
skupina	skupina	k1gFnSc1	skupina
A	A	kA	A
(	(	kIx(	(
<g/>
45	[number]	k4	45
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
dále	daleko	k6eAd2	daleko
skupina	skupina	k1gFnSc1	skupina
0	[number]	k4	0
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
B	B	kA	B
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
AB	AB	kA	AB
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
90	[number]	k4	90
%	%	kIx~	%
české	český	k2eAgFnSc2d1	Česká
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
Rh	Rh	k1gMnSc1	Rh
pozitivní	pozitivní	k2eAgMnSc1d1	pozitivní
<g/>
,	,	kIx,	,
Rh	Rh	k1gMnSc1	Rh
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
A	a	k9	a
(	(	kIx(	(
<g/>
40	[number]	k4	40
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
0	[number]	k4	0
(	(	kIx(	(
<g/>
40	[number]	k4	40
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
B	B	kA	B
(	(	kIx(	(
<g/>
10	[number]	k4	10
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
AB	AB	kA	AB
(	(	kIx(	(
<g/>
10	[number]	k4	10
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
Rh	Rh	k1gFnSc1	Rh
<g/>
+	+	kIx~	+
84	[number]	k4	84
%	%	kIx~	%
a	a	k8xC	a
Rh-	Rh-	k1gFnSc1	Rh-
16	[number]	k4	16
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Krevní	krevní	k2eAgFnSc2d1	krevní
transfúze	transfúze	k1gFnSc2	transfúze
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
krevní	krevní	k2eAgFnSc6d1	krevní
transfúzi	transfúze	k1gFnSc6	transfúze
je	být	k5eAaImIp3nS	být
životně	životně	k6eAd1	životně
důležité	důležitý	k2eAgNnSc1d1	důležité
použít	použít	k5eAaPmF	použít
pouze	pouze	k6eAd1	pouze
krevní	krevní	k2eAgFnSc4d1	krevní
skupinu	skupina	k1gFnSc4	skupina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
příjemce	příjemce	k1gMnPc4	příjemce
nepoškodí	poškodit	k5eNaPmIp3nS	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
uvedená	uvedený	k2eAgFnSc1d1	uvedená
tabulka	tabulka	k1gFnSc1	tabulka
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
povolené	povolený	k2eAgFnSc2d1	povolená
kombinace	kombinace	k1gFnSc2	kombinace
krve	krev	k1gFnSc2	krev
dárce	dárce	k1gMnSc2	dárce
a	a	k8xC	a
příjemce	příjemce	k1gMnSc2	příjemce
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
názorně	názorně	k6eAd1	názorně
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
příjemce	příjemce	k1gMnSc1	příjemce
s	s	k7c7	s
krví	krev	k1gFnSc7	krev
AB	AB	kA	AB
<g/>
+	+	kIx~	+
může	moct	k5eAaImIp3nS	moct
obdržet	obdržet	k5eAaPmF	obdržet
krev	krev	k1gFnSc4	krev
jakéhokoliv	jakýkoliv	k3yIgMnSc2	jakýkoliv
dárce	dárce	k1gMnSc2	dárce
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
říká	říkat	k5eAaImIp3nS	říkat
univerzální	univerzální	k2eAgMnPc4d1	univerzální
příjemce	příjemce	k1gMnPc4	příjemce
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
krev	krev	k1gFnSc1	krev
skupiny	skupina	k1gFnSc2	skupina
0	[number]	k4	0
<g/>
−	−	k?	−
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
darována	darovat	k5eAaPmNgFnS	darovat
všem	všecek	k3xTgMnPc3	všecek
příjemcům	příjemce	k1gMnPc3	příjemce
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
univerzálního	univerzální	k2eAgMnSc4d1	univerzální
dárce	dárce	k1gMnSc4	dárce
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Kompatibilita	kompatibilita	k1gFnSc1	kompatibilita
základních	základní	k2eAgFnPc2d1	základní
krevních	krevní	k2eAgFnPc2d1	krevní
skupin	skupina	k1gFnPc2	skupina
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
plazmy	plazma	k1gFnSc2	plazma
je	být	k5eAaImIp3nS	být
přesně	přesně	k6eAd1	přesně
opačná	opačný	k2eAgFnSc1d1	opačná
než	než	k8xS	než
pro	pro	k7c4	pro
přenos	přenos	k1gInSc4	přenos
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
výše	výše	k1gFnSc2	výše
uvedených	uvedený	k2eAgInPc2d1	uvedený
dvou	dva	k4xCgInPc2	dva
nejvýznačnějších	význačný	k2eAgInPc2d3	nejvýznačnější
krevních	krevní	k2eAgInPc2d1	krevní
typů	typ	k1gInPc2	typ
je	být	k5eAaImIp3nS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
kolem	kolem	k7c2	kolem
50	[number]	k4	50
dalších	další	k2eAgInPc2d1	další
krevních	krevní	k2eAgInPc2d1	krevní
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
antigeny	antigen	k1gInPc1	antigen
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
vedle	vedle	k7c2	vedle
antigenů	antigen	k1gInPc2	antigen
AB0	AB0	k1gFnSc2	AB0
a	a	k8xC	a
D	D	kA	D
–	–	k?	–
krev	krev	k1gFnSc1	krev
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
přesněji	přesně	k6eAd2	přesně
popsána	popsat	k5eAaPmNgFnS	popsat
např.	např.	kA	např.
jako	jako	k8xC	jako
A	a	k9	a
Rh	Rh	k1gFnSc1	Rh
<g/>
−	−	k?	−
ccee	cceat	k5eAaPmIp3nS	cceat
K	k	k7c3	k
<g/>
−	−	k?	−
<g/>
.	.	kIx.	.
</s>
<s>
Diego	Diego	k6eAd1	Diego
pozitivní	pozitivní	k2eAgFnSc4d1	pozitivní
krev	krev	k1gFnSc4	krev
mají	mít	k5eAaImIp3nP	mít
pouze	pouze	k6eAd1	pouze
některé	některý	k3yIgFnPc1	některý
skupiny	skupina	k1gFnPc1	skupina
obyvatel	obyvatel	k1gMnPc2	obyvatel
Východní	východní	k2eAgFnSc2d1	východní
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
američtí	americký	k2eAgMnPc1d1	americký
Indiáni	Indián	k1gMnPc1	Indián
Duffy	Duff	k1gInPc4	Duff
negativní	negativní	k2eAgFnSc4d1	negativní
krev	krev	k1gFnSc4	krev
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
imunní	imunní	k2eAgMnSc1d1	imunní
vůči	vůči	k7c3	vůči
malárii	malárie	k1gFnSc3	malárie
Fisherův	Fisherův	k2eAgInSc1d1	Fisherův
systém	systém	k1gInSc1	systém
upřesňuje	upřesňovat	k5eAaImIp3nS	upřesňovat
poměrně	poměrně	k6eAd1	poměrně
složitý	složitý	k2eAgMnSc1d1	složitý
Rh	Rh	k1gMnSc1	Rh
faktor	faktor	k1gMnSc1	faktor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zapříčiněn	zapříčinit	k5eAaPmNgInS	zapříčinit
výskytem	výskyt	k1gInSc7	výskyt
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
tří	tři	k4xCgInPc2	tři
genů	gen	k1gInPc2	gen
C	C	kA	C
<g/>
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
,	,	kIx,	,
D	D	kA	D
<g/>
/	/	kIx~	/
<g/>
<g />
.	.	kIx.	.
</s>
<s>
d	d	k?	d
a	a	k8xC	a
E	E	kA	E
<g/>
/	/	kIx~	/
<g/>
e	e	k0	e
na	na	k7c6	na
chromozomu	chromozom	k1gInSc6	chromozom
1	[number]	k4	1
Další	další	k2eAgInPc4d1	další
systémy	systém	k1gInPc4	systém
krevních	krevní	k2eAgInPc2d1	krevní
typů	typ	k1gInPc2	typ
nejsou	být	k5eNaImIp3nP	být
významné	významný	k2eAgFnPc1d1	významná
při	při	k7c6	při
krevních	krevní	k2eAgFnPc6d1	krevní
transfúzích	transfúze	k1gFnPc6	transfúze
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
význam	význam	k1gInSc4	význam
ve	v	k7c6	v
forenzní	forenzní	k2eAgFnSc6d1	forenzní
analýze	analýza	k1gFnSc6	analýza
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
při	při	k7c6	při
určování	určování	k1gNnSc6	určování
pachatelů	pachatel	k1gMnPc2	pachatel
trestných	trestný	k2eAgInPc2d1	trestný
činů	čin	k1gInPc2	čin
<g/>
.	.	kIx.	.
krev	krev	k1gFnSc1	krev
darování	darování	k1gNnSc2	darování
krve	krev	k1gFnSc2	krev
krevní	krevní	k2eAgFnSc2d1	krevní
transfúze	transfúze	k1gFnSc2	transfúze
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
krevní	krevní	k2eAgFnSc1d1	krevní
skupina	skupina	k1gFnSc1	skupina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Seznam	seznam	k1gInSc1	seznam
systémů	systém	k1gInPc2	systém
krevních	krevní	k2eAgInPc2d1	krevní
typů	typ	k1gInPc2	typ
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
