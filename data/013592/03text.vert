<s>
Domingo	Domingo	k1gMnSc1
Tejera	Tejero	k1gNnSc2
</s>
<s>
Domingo	Domingo	k1gMnSc1
Tejera	Tejer	k1gMnSc2
Narození	narození	k1gNnSc2
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1899	#num#	k4
nebo	nebo	k8xC
22	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1899	#num#	k4
<g/>
Montevideo	Montevideo	k1gNnSc4
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1969	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
69	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Montevideo	Montevideo	k1gNnSc4
Povolání	povolání	k1gNnSc2
</s>
<s>
fotbalista	fotbalista	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
</s>
<s>
MS	MS	kA
1930	#num#	k4
</s>
<s>
Uruguay	Uruguay	k1gFnSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
</s>
<s>
1920	#num#	k4
</s>
<s>
Uruguay	Uruguay	k1gFnSc1
</s>
<s>
1926	#num#	k4
</s>
<s>
Uruguay	Uruguay	k1gFnSc1
</s>
<s>
Domingo	Domingo	k1gMnSc1
Tejera	Tejera	k1gMnSc1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1899	#num#	k4
<g/>
,	,	kIx,
Montevideo	Montevideo	k1gNnSc1
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
1969	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
uruguayský	uruguayský	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nastupoval	nastupovat	k5eAaImAgMnS
především	především	k9
na	na	k7c6
postu	post	k1gInSc6
obránce	obránce	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
uruguayskou	uruguayský	k2eAgFnSc7d1
reprezentací	reprezentace	k1gFnSc7
vyhrál	vyhrát	k5eAaPmAgMnS
mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
1930	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1920	#num#	k4
též	též	k9
mistrovství	mistrovství	k1gNnSc4
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
národním	národní	k2eAgInSc6d1
týmu	tým	k1gInSc6
působil	působit	k5eAaImAgMnS
v	v	k7c6
letech	let	k1gInPc6
1922	#num#	k4
<g/>
–	–	k?
<g/>
1932	#num#	k4
a	a	k8xC
odehrál	odehrát	k5eAaPmAgMnS
za	za	k7c2
něj	on	k3xPp3gNnSc2
20	#num#	k4
utkání	utkání	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Celou	celý	k2eAgFnSc4d1
svou	svůj	k3xOyFgFnSc4
kariéru	kariéra	k1gFnSc4
strávil	strávit	k5eAaPmAgMnS
v	v	k7c6
klubu	klub	k1gInSc6
Montevideo	Montevideo	k1gNnSc1
Wanderers	Wanderers	k1gInSc1
<g/>
,	,	kIx,
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
s	s	k7c7
ním	on	k3xPp3gMnSc7
jednou	jednou	k6eAd1
mistrem	mistr	k1gMnSc7
Uruguaye	Uruguay	k1gFnSc2
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.rsssf.com/miscellaneous/wcwinners.html	http://www.rsssf.com/miscellaneous/wcwinners.html	k1gMnSc1
<g/>
↑	↑	k?
http://www.rsssf.com/tables/20safull.html	http://www.rsssf.com/tables/20safull.html	k1gMnSc1
<g/>
↑	↑	k?
http://www.rsssf.com/miscellaneous/uru-recintlp.html	http://www.rsssf.com/miscellaneous/uru-recintlp.html	k1gMnSc1
<g/>
↑	↑	k?
http://www.national-football-teams.com/player/33737/Domingo_Tejera.html	http://www.national-football-teams.com/player/33737/Domingo_Tejera.html	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Medailisté	medailista	k1gMnPc1
–	–	k?
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
1930	#num#	k4
Uruguay	Uruguay	k1gFnSc1
</s>
<s>
José	José	k6eAd1
Andrade	Andrad	k1gInSc5
•	•	k?
Peregrino	Peregrin	k2eAgNnSc1d1
Anselmo	Anselma	k1gFnSc5
•	•	k?
Enrique	Enriqu	k1gInPc4
Ballestrero	Ballestrero	k1gNnSc4
•	•	k?
Juan	Juan	k1gMnSc1
Carlos	Carlos	k1gMnSc1
Calvo	Calvo	k1gNnSc1
•	•	k?
Miguel	Miguel	k1gMnSc1
Capuccini	Capuccin	k1gMnPc1
•	•	k?
Héctor	Héctor	k1gMnSc1
Castro	Castro	k1gNnSc1
•	•	k?
Pedro	Pedro	k1gNnSc1
Cea	Cea	k1gMnSc1
•	•	k?
Pablo	Pablo	k1gNnSc4
Dorado	Dorada	k1gFnSc5
•	•	k?
Lorenzo	Lorenza	k1gFnSc5
Fernández	Fernández	k1gInSc1
•	•	k?
Álvaro	Álvara	k1gFnSc5
Gestido	Gestida	k1gFnSc5
•	•	k?
Santos	Santosa	k1gFnPc2
Iriarte	Iriart	k1gInSc5
•	•	k?
Ernesto	Ernesta	k1gMnSc5
Mascheroni	Mascheroň	k1gFnSc3
•	•	k?
Ángel	Ángel	k1gMnSc1
Melogno	Melogno	k1gNnSc1
•	•	k?
José	Josý	k2eAgFnSc3d1
Nasazzi	Nasazze	k1gFnSc3
–	–	k?
•	•	k?
Pedro	Pedra	k1gMnSc5
Petrone	Petron	k1gMnSc5
•	•	k?
Conduelo	Conduela	k1gFnSc5
Píriz	Píriz	k1gMnSc1
•	•	k?
Emilio	Emilio	k1gMnSc1
Recoba	Recoba	k1gFnSc1
•	•	k?
Carlos	Carlos	k1gMnSc1
Riolfo	Riolfo	k1gMnSc1
•	•	k?
Zoilo	Zoila	k1gMnSc5
Saldombide	Saldombid	k1gMnSc5
•	•	k?
Héctor	Héctor	k1gMnSc1
Scarone	Scaron	k1gInSc5
•	•	k?
Domingo	Domingo	k1gMnSc1
Tejera	Tejero	k1gNnSc2
•	•	k?
Santos	Santos	k1gMnSc1
Urdinarán	Urdinarán	k2eAgMnSc1d1
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Alberto	Alberta	k1gFnSc5
Suppici	Suppik	k1gMnPc5
Argentina	Argentina	k1gFnSc1
</s>
<s>
Ángel	Ángel	k1gMnSc1
Bossio	Bossio	k1gMnSc1
•	•	k?
Juan	Juan	k1gMnSc1
Botasso	Botassa	k1gFnSc5
•	•	k?
Roberto	Roberta	k1gFnSc5
Cherro	Cherra	k1gFnSc5
•	•	k?
Alberto	Alberta	k1gFnSc5
Chividini	Chividin	k2eAgMnPc1d1
•	•	k?
Attilio	Attilio	k6eAd1
Demaría	Demaríum	k1gNnSc2
•	•	k?
José	José	k1gNnSc2
Della	Dell	k1gMnSc2
Torre	torr	k1gInSc5
•	•	k?
Juan	Juan	k1gMnSc1
Evaristo	Evarista	k1gMnSc5
•	•	k?
Mario	Mario	k1gMnSc5
Evaristo	Evarista	k1gMnSc5
•	•	k?
Manuel	Manuel	k1gMnSc1
Ferreira	Ferreira	k1gMnSc1
–	–	k?
•	•	k?
Luis	Luisa	k1gFnPc2
Monti	Monti	k1gNnSc1
•	•	k?
Ramón	Ramón	k1gMnSc1
Muttis	Muttis	k1gFnSc2
•	•	k?
Rodolfo	Rodolfo	k6eAd1
Orlandini	Orlandin	k2eAgMnPc1d1
•	•	k?
Fernando	Fernanda	k1gFnSc5
Paternoster	Paternoster	k1gMnSc1
•	•	k?
Natalio	Natalio	k1gMnSc1
Perinetti	Perinetť	k1gFnSc2
•	•	k?
Carlos	Carlos	k1gMnSc1
Peucelle	Peucelle	k1gFnSc2
•	•	k?
Edmundo	Edmundo	k1gNnSc1
Piaggio	Piaggio	k1gMnSc1
•	•	k?
Alejandro	Alejandra	k1gFnSc5
Scopelli	Scopell	k1gMnSc3
•	•	k?
Carlos	Carlos	k1gMnSc1
Spadaro	Spadara	k1gFnSc5
•	•	k?
Guillermo	Guillerma	k1gFnSc5
Stábile	Stábila	k1gFnSc3
•	•	k?
Pedro	Pedro	k1gNnSc1
Suárez	Suárez	k1gMnSc1
•	•	k?
Francisco	Francisco	k1gMnSc1
Varallo	Varallo	k1gNnSc1
•	•	k?
Adolfo	Adolfo	k1gMnSc1
Zumelzú	Zumelzú	k1gMnSc1
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Francisco	Francisco	k1gMnSc1
Olazar	Olazar	k1gMnSc1
a	a	k8xC
Juan	Juan	k1gMnSc1
José	Josá	k1gFnSc2
Tramutola	Tramutola	k1gFnSc1
USA	USA	kA
</s>
<s>
Jimmy	Jimm	k1gInPc1
Douglas	Douglasa	k1gFnPc2
•	•	k?
George	George	k1gNnSc4
Moorhouse	Moorhouse	k1gFnSc2
•	•	k?
Frank	Frank	k1gMnSc1
Vaughn	Vaughn	k1gNnSc1
•	•	k?
Alexander	Alexandra	k1gFnPc2
Wood	Wood	k1gMnSc1
•	•	k?
Andy	Anda	k1gFnSc2
Auld	Auld	k1gMnSc1
•	•	k?
Jimmy	Jimma	k1gFnSc2
Gallagher	Gallaghra	k1gFnPc2
•	•	k?
Philip	Philip	k1gMnSc1
Slone	slon	k1gMnSc5
•	•	k?
Mike	Mike	k1gNnSc1
Bookmaker	bookmaker	k1gMnSc1
•	•	k?
Jim	on	k3xPp3gMnPc3
Brown	Brown	k1gMnSc1
•	•	k?
Tom	Tom	k1gMnSc1
Florie	Florie	k1gFnSc2
–	–	k?
•	•	k?
James	James	k1gMnSc1
Gentle	Gentle	k1gFnSc2
•	•	k?
Billy	Bill	k1gMnPc4
Gonsalves	Gonsalvesa	k1gFnPc2
•	•	k?
Bart	Bart	k1gMnSc1
McGhee	McGhe	k1gFnSc2
•	•	k?
Arnie	Arnie	k1gFnSc2
Oliver	Olivra	k1gFnPc2
•	•	k?
Bert	Berta	k1gFnPc2
Patenaude	Patenaud	k1gInSc5
•	•	k?
Raphael	Raphael	k1gInSc4
Tracey	Tracea	k1gFnSc2
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Robert	Robert	k1gMnSc1
Millar	Millar	k1gMnSc1
Jugoslávie	Jugoslávie	k1gFnSc2
</s>
<s>
Milorad	Milorad	k1gInSc1
Arsenijevič	Arsenijevič	k1gMnSc1
•	•	k?
Ivan	Ivan	k1gMnSc1
Bek	bek	k1gMnSc1
•	•	k?
Momčilo	Momčilo	k1gMnSc1
Đokič	Đokič	k1gMnSc1
•	•	k?
Branislav	Branislav	k1gMnSc1
Hrnjiček	Hrnjička	k1gFnPc2
•	•	k?
Milutin	Milutin	k1gMnSc1
Ivković	Ivković	k1gMnSc1
–	–	k?
•	•	k?
Milovan	Milovan	k1gMnSc1
Jakšič	Jakšič	k1gMnSc1
•	•	k?
Blagoje	Blagoj	k1gInPc1
Marjanovič	Marjanovič	k1gInSc1
•	•	k?
Dušan	Dušan	k1gMnSc1
Markovič	Markovič	k1gMnSc1
•	•	k?
Dragoslav	Dragoslav	k1gMnSc1
Mihajlovič	Mihajlovič	k1gMnSc1
•	•	k?
Dragutin	Dragutin	k1gMnSc1
Najdanovič	Najdanovič	k1gMnSc1
•	•	k?
Branislav	Branislav	k1gMnSc1
Sekulič	Sekulič	k1gMnSc1
•	•	k?
Teofilo	Teofila	k1gFnSc5
Spasojevič	Spasojevič	k1gMnSc1
•	•	k?
Ljubiša	Ljubiša	k1gMnSc1
Stefanovič	Stefanovič	k1gMnSc1
•	•	k?
Milan	Milan	k1gMnSc1
Stojanovič	Stojanovič	k1gMnSc1
•	•	k?
Aleksandar	Aleksandar	k1gMnSc1
Tirnanič	Tirnanič	k1gMnSc1
•	•	k?
Dragomir	Dragomir	k1gMnSc1
Tošić	Tošić	k1gMnSc2
•	•	k?
Đorđe	Đorđ	k1gMnSc2
Vujadinović	Vujadinović	k1gMnSc2
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Bosko	Bosko	k1gMnSc1
Simonovič	Simonovič	k1gMnSc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
1920	#num#	k4
–	–	k?
Uruguay	Uruguay	k1gFnSc1
Brankáři	brankář	k1gMnSc5
</s>
<s>
Beloutas	Beloutas	k1gMnSc1
•	•	k?
Legnazzi	Legnazze	k1gFnSc4
Hráči	hráč	k1gMnPc1
</s>
<s>
Cámpolo	Cámpola	k1gFnSc5
•	•	k?
Foglino	Foglin	k2eAgNnSc1d1
•	•	k?
Marroche	Marroche	k1gNnSc1
•	•	k?
Pérez	Pérez	k1gMnSc1
•	•	k?
Piendibene	Piendiben	k1gInSc5
•	•	k?
Ravera	Ravero	k1gNnSc2
•	•	k?
Romano	Romano	k1gMnSc1
•	•	k?
Ruotta	Ruotta	k1gMnSc1
•	•	k?
Scarone	Scaron	k1gInSc5
•	•	k?
Somma	Somm	k1gMnSc2
•	•	k?
Tejera	Tejer	k1gMnSc2
•	•	k?
Urdinarán	Urdinarán	k1gInSc1
•	•	k?
Villar	Villar	k1gInSc1
•	•	k?
Alf	alfa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zibechi	Zibech	k1gFnSc2
•	•	k?
Arm	Arm	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zibechi	Zibechi	k1gNnSc1
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Fígoli	Fígoli	k6eAd1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
1926	#num#	k4
–	–	k?
Uruguay	Uruguay	k1gFnSc1
Brankáři	brankář	k1gMnSc3
</s>
<s>
Batignani	Batignan	k1gMnPc1
•	•	k?
Mazali	mazat	k5eAaImAgMnP
Hráči	hráč	k1gMnPc1
</s>
<s>
Andrade	Andrad	k1gInSc5
•	•	k?
Borjas	Borjas	k1gInSc1
•	•	k?
Cabrera	Cabrera	k1gFnSc1
•	•	k?
Castro	Castro	k1gNnSc1
•	•	k?
Conti	Conť	k1gFnSc2
•	•	k?
Fernández	Fernández	k1gMnSc1
•	•	k?
Ghierra	Ghierra	k1gMnSc1
•	•	k?
Lobos	Lobos	k1gMnSc1
•	•	k?
Nasazzi	Nasazze	k1gFnSc4
•	•	k?
Recoba	Recoba	k1gFnSc1
•	•	k?
Romano	Romano	k1gMnSc1
•	•	k?
Saldombide	Saldombid	k1gInSc5
•	•	k?
Scarone	Scaron	k1gInSc5
•	•	k?
Tejera	Tejer	k1gMnSc2
•	•	k?
Urdinarán	Urdinarán	k1gInSc1
•	•	k?
Vanzzino	Vanzzino	k1gNnSc1
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Fígoli	Fígoli	k6eAd1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
</s>
