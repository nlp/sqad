<s>
Slovenské	slovenský	k2eAgNnSc1d1	slovenské
národní	národní	k2eAgNnSc1d1	národní
povstání	povstání	k1gNnSc1	povstání
(	(	kIx(	(
<g/>
známé	známý	k2eAgNnSc1d1	známé
také	také	k9	také
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
SNP	SNP	kA	SNP
<g/>
,	,	kIx,	,
slovensky	slovensky	k6eAd1	slovensky
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národné	národný	k2eAgFnSc2d1	národná
povstanie	povstanie	k1gFnSc2	povstanie
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
ozbrojené	ozbrojený	k2eAgNnSc1d1	ozbrojené
vystoupení	vystoupení	k1gNnSc1	vystoupení
protifašistických	protifašistický	k2eAgFnPc2d1	protifašistická
sil	síla	k1gFnPc2	síla
na	na	k7c6	na
území	území	k1gNnSc6	území
Slovenska	Slovensko	k1gNnSc2	Slovensko
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
