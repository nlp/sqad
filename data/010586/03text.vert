<p>
<s>
Slovenské	slovenský	k2eAgNnSc1d1	slovenské
národní	národní	k2eAgNnSc1d1	národní
povstání	povstání	k1gNnSc1	povstání
(	(	kIx(	(
<g/>
známé	známý	k2eAgNnSc1d1	známé
také	také	k9	také
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
SNP	SNP	kA	SNP
<g/>
,	,	kIx,	,
slovensky	slovensky	k6eAd1	slovensky
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národné	národný	k2eAgFnSc2d1	národná
povstanie	povstanie	k1gFnSc2	povstanie
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
ozbrojené	ozbrojený	k2eAgNnSc1d1	ozbrojené
vystoupení	vystoupení	k1gNnSc1	vystoupení
protifašistických	protifašistický	k2eAgFnPc2d1	protifašistická
sil	síla	k1gFnPc2	síla
na	na	k7c6	na
území	území	k1gNnSc6	území
Slovenska	Slovensko	k1gNnSc2	Slovensko
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
veliteli	velitel	k1gMnPc7	velitel
povstání	povstání	k1gNnSc2	povstání
byli	být	k5eAaImAgMnP	být
generálové	generál	k1gMnPc1	generál
Rudolf	Rudolf	k1gMnSc1	Rudolf
Viest	Viest	k1gMnSc1	Viest
a	a	k8xC	a
Ján	Ján	k1gMnSc1	Ján
Golian	Golian	k1gMnSc1	Golian
<g/>
.	.	kIx.	.
</s>
<s>
Centrem	centr	k1gInSc7	centr
povstaleckého	povstalecký	k2eAgNnSc2d1	povstalecké
území	území	k1gNnSc2	území
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
obsazení	obsazení	k1gNnSc6	obsazení
německými	německý	k2eAgFnPc7d1	německá
silami	síla	k1gFnPc7	síla
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1944	[number]	k4	1944
přešla	přejít	k5eAaPmAgFnS	přejít
menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
povstalců	povstalec	k1gMnPc2	povstalec
do	do	k7c2	do
partyzánské	partyzánský	k2eAgFnSc2d1	Partyzánská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
základnami	základna	k1gFnPc7	základna
roztroušenými	roztroušený	k2eAgFnPc7d1	roztroušená
v	v	k7c6	v
pohořích	pohoří	k1gNnPc6	pohoří
středního	střední	k2eAgNnSc2d1	střední
Slovenska	Slovensko	k1gNnSc2	Slovensko
(	(	kIx(	(
<g/>
Nízké	nízký	k2eAgFnPc1d1	nízká
Tatry	Tatra	k1gFnPc1	Tatra
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Fatra	Fatra	k1gFnSc1	Fatra
<g/>
,	,	kIx,	,
Javorníky	Javorník	k1gInPc1	Javorník
<g/>
,	,	kIx,	,
Kremnické	kremnický	k2eAgInPc1d1	kremnický
vrchy	vrch	k1gInPc1	vrch
<g/>
,	,	kIx,	,
Slovenské	slovenský	k2eAgNnSc1d1	slovenské
rudohoří	rudohoří	k1gNnSc1	rudohoří
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
SNP	SNP	kA	SNP
bojovalo	bojovat	k5eAaImAgNnS	bojovat
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
Slováků	Slovák	k1gMnPc2	Slovák
<g/>
,	,	kIx,	,
také	také	k9	také
přibližně	přibližně	k6eAd1	přibližně
2000	[number]	k4	2000
Čechů	Čech	k1gMnPc2	Čech
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
bojovali	bojovat	k5eAaImAgMnP	bojovat
Poláci	Polák	k1gMnPc1	Polák
<g/>
,	,	kIx,	,
Rumuni	Rumun	k1gMnPc1	Rumun
<g/>
,	,	kIx,	,
Rusíni	Rusín	k1gMnPc1	Rusín
<g/>
,	,	kIx,	,
Ukrajinci	Ukrajinec	k1gMnPc1	Ukrajinec
<g/>
,	,	kIx,	,
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
,	,	kIx,	,
Bulhaři	Bulhar	k1gMnPc1	Bulhar
<g/>
,	,	kIx,	,
Jugoslávci	Jugoslávec	k1gMnPc1	Jugoslávec
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
podporu	podpora	k1gFnSc4	podpora
povstání	povstání	k1gNnSc2	povstání
znamenalo	znamenat	k5eAaImAgNnS	znamenat
působení	působení	k1gNnSc1	působení
vojáků	voják	k1gMnPc2	voják
letecky	letecky	k6eAd1	letecky
dopravených	dopravený	k2eAgMnPc2d1	dopravený
na	na	k7c4	na
povstalecké	povstalecký	k2eAgNnSc4d1	povstalecké
území	území	k1gNnSc4	území
<g/>
;	;	kIx,	;
šlo	jít	k5eAaImAgNnS	jít
zejména	zejména	k9	zejména
o	o	k7c4	o
vojáky	voják	k1gMnPc4	voják
a	a	k8xC	a
partyzány	partyzána	k1gFnPc4	partyzána
ze	z	k7c2	z
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Povstání	povstání	k1gNnSc1	povstání
bylo	být	k5eAaImAgNnS	být
podporováno	podporovat	k5eAaImNgNnS	podporovat
i	i	k9	i
leteckými	letecký	k2eAgFnPc7d1	letecká
dodávkami	dodávka	k1gFnPc7	dodávka
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
ze	z	k7c2	z
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
i	i	k9	i
od	od	k7c2	od
západních	západní	k2eAgMnPc2d1	západní
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Situace	situace	k1gFnSc1	situace
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
před	před	k7c7	před
SNP	SNP	kA	SNP
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
jara	jaro	k1gNnSc2	jaro
1944	[number]	k4	1944
vedly	vést	k5eAaImAgFnP	vést
skupiny	skupina	k1gFnPc1	skupina
politiků	politik	k1gMnPc2	politik
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
úvahy	úvaha	k1gFnSc2	úvaha
o	o	k7c6	o
dalším	další	k2eAgInSc6d1	další
osudu	osud	k1gInSc6	osud
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
nabýval	nabývat	k5eAaImAgMnS	nabývat
vrchu	vrch	k1gInSc2	vrch
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pasivně	pasivně	k6eAd1	pasivně
čekat	čekat	k5eAaImF	čekat
na	na	k7c4	na
konec	konec	k1gInSc4	konec
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
o	o	k7c6	o
úspěchu	úspěch	k1gInSc6	úspěch
nacistů	nacista	k1gMnPc2	nacista
si	se	k3xPyFc3	se
už	už	k6eAd1	už
nedělal	dělat	k5eNaImAgMnS	dělat
nikdo	nikdo	k3yNnSc1	nikdo
iluze	iluze	k1gFnPc4	iluze
<g/>
)	)	kIx)	)
a	a	k8xC	a
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
připravit	připravit	k5eAaPmF	připravit
akci	akce	k1gFnSc4	akce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
nejen	nejen	k6eAd1	nejen
napomohla	napomoct	k5eAaPmAgFnS	napomoct
postupu	postup	k1gInSc3	postup
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
likvidovala	likvidovat	k5eAaBmAgFnS	likvidovat
i	i	k9	i
ostudné	ostudný	k2eAgNnSc4d1	ostudné
spojení	spojení	k1gNnSc4	spojení
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
s	s	k7c7	s
nacisty	nacista	k1gMnPc7	nacista
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
pokusila	pokusit	k5eAaPmAgFnS	pokusit
rehabilitovat	rehabilitovat	k5eAaBmF	rehabilitovat
slovenský	slovenský	k2eAgInSc4d1	slovenský
národ	národ	k1gInSc4	národ
<g/>
.	.	kIx.	.
</s>
<s>
Povstání	povstání	k1gNnSc4	povstání
iniciovala	iniciovat	k5eAaBmAgFnS	iniciovat
ilegální	ilegální	k2eAgFnSc1d1	ilegální
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Vojenský	vojenský	k2eAgInSc1d1	vojenský
převrat	převrat	k1gInSc1	převrat
měl	mít	k5eAaImAgInS	mít
vypuknout	vypuknout	k5eAaPmF	vypuknout
<g/>
,	,	kIx,	,
až	až	k8xS	až
se	se	k3xPyFc4	se
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
měli	mít	k5eAaImAgMnP	mít
vojáci	voják	k1gMnPc1	voják
dvou	dva	k4xCgFnPc2	dva
pěších	pěší	k2eAgFnPc2d1	pěší
divizí	divize	k1gFnPc2	divize
bránících	bránící	k2eAgFnPc2d1	bránící
linii	linie	k1gFnSc4	linie
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
Slovensku	Slovensko	k1gNnSc6	Slovensko
navázat	navázat	k5eAaPmF	navázat
styk	styk	k1gInSc1	styk
se	s	k7c7	s
sovětskými	sovětský	k2eAgNnPc7d1	sovětské
vojsky	vojsko	k1gNnPc7	vojsko
a	a	k8xC	a
umožnit	umožnit	k5eAaPmF	umožnit
jim	on	k3xPp3gMnPc3	on
přechod	přechod	k1gInSc4	přechod
přes	přes	k7c4	přes
komplikovaný	komplikovaný	k2eAgInSc4d1	komplikovaný
terén	terén	k1gInSc4	terén
Karpat	Karpaty	k1gInPc2	Karpaty
<g/>
,	,	kIx,	,
přinejmenším	přinejmenším	k6eAd1	přinejmenším
na	na	k7c4	na
střední	střední	k2eAgNnSc4d1	střední
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgNnPc1d1	sovětské
vojska	vojsko	k1gNnPc1	vojsko
by	by	kYmCp3nP	by
tak	tak	k6eAd1	tak
vpadla	vpadnout	k5eAaPmAgFnS	vpadnout
do	do	k7c2	do
týlu	týl	k1gInSc2	týl
německých	německý	k2eAgNnPc2d1	německé
vojsk	vojsko	k1gNnPc2	vojsko
ve	v	k7c6	v
východních	východní	k2eAgInPc6d1	východní
Karpatech	Karpaty	k1gInPc6	Karpaty
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
by	by	kYmCp3nS	by
výrazně	výrazně	k6eAd1	výrazně
narušila	narušit	k5eAaPmAgFnS	narušit
ústupové	ústupový	k2eAgInPc4d1	ústupový
plány	plán	k1gInPc4	plán
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
tankové	tankový	k2eAgFnSc2d1	tanková
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc4	území
Slovenska	Slovensko	k1gNnSc2	Slovensko
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
výrazně	výrazně	k6eAd1	výrazně
ušetřilo	ušetřit	k5eAaPmAgNnS	ušetřit
před	před	k7c7	před
hrůzami	hrůza	k1gFnPc7	hrůza
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Benešova	Benešův	k2eAgFnSc1d1	Benešova
koncepce	koncepce	k1gFnSc1	koncepce
přípravy	příprava	k1gFnSc2	příprava
SNP	SNP	kA	SNP
==	==	k?	==
</s>
</p>
<p>
<s>
Obnovu	obnova	k1gFnSc4	obnova
československého	československý	k2eAgInSc2d1	československý
státu	stát	k1gInSc2	stát
začala	začít	k5eAaPmAgFnS	začít
exilová	exilový	k2eAgFnSc1d1	exilová
Benešova	Benešův	k2eAgFnSc1d1	Benešova
vláda	vláda	k1gFnSc1	vláda
připravovat	připravovat	k5eAaImF	připravovat
již	již	k6eAd1	již
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
</s>
<s>
Předběžná	předběžný	k2eAgNnPc1d1	předběžné
jednání	jednání	k1gNnPc1	jednání
se	s	k7c7	s
západními	západní	k2eAgMnPc7d1	západní
spojenci	spojenec	k1gMnPc7	spojenec
o	o	k7c4	o
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
pomoc	pomoc	k1gFnSc4	pomoc
Slovenskému	slovenský	k2eAgInSc3d1	slovenský
povstání	povstání	k1gNnSc6	povstání
začaly	začít	k5eAaPmAgInP	začít
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc1	jednání
však	však	k9	však
uvízla	uvíznout	k5eAaPmAgFnS	uvíznout
na	na	k7c6	na
mrtvém	mrtvý	k2eAgInSc6d1	mrtvý
bodě	bod	k1gInSc6	bod
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
vyčlenit	vyčlenit	k5eAaPmF	vyčlenit
síly	síla	k1gFnPc4	síla
čs	čs	kA	čs
<g/>
.	.	kIx.	.
letectva	letectvo	k1gNnSc2	letectvo
pro	pro	k7c4	pro
připravovanou	připravovaný	k2eAgFnSc4d1	připravovaná
akci	akce	k1gFnSc4	akce
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1943	[number]	k4	1943
Beneš	Beneš	k1gMnSc1	Beneš
obrátil	obrátit	k5eAaPmAgMnS	obrátit
pro	pro	k7c4	pro
pomoc	pomoc	k1gFnSc4	pomoc
k	k	k7c3	k
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Žádal	žádat	k5eAaImAgInS	žádat
o	o	k7c4	o
přepravu	přeprava	k1gFnSc4	přeprava
vojenského	vojenský	k2eAgInSc2d1	vojenský
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
osob	osoba	k1gFnPc2	osoba
před	před	k7c7	před
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
přepravu	přeprava	k1gFnSc4	přeprava
1000	[number]	k4	1000
čs	čs	kA	čs
<g/>
.	.	kIx.	.
vojáků	voják	k1gMnPc2	voják
na	na	k7c4	na
povstalecké	povstalecký	k2eAgNnSc4d1	povstalecké
území	území	k1gNnSc4	území
a	a	k8xC	a
podpůrný	podpůrný	k2eAgInSc4d1	podpůrný
zásah	zásah	k1gInSc4	zásah
výsadkářů	výsadkář	k1gMnPc2	výsadkář
a	a	k8xC	a
amerického	americký	k2eAgNnSc2d1	americké
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
úvahy	úvaha	k1gFnPc1	úvaha
o	o	k7c6	o
možné	možný	k2eAgFnSc6d1	možná
československo-sovětské	československoovětský	k2eAgFnSc6d1	československo-sovětská
spojenecké	spojenecký	k2eAgFnSc6d1	spojenecká
smlouvě	smlouva	k1gFnSc6	smlouva
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
proudu	proud	k1gInSc6	proud
<g/>
,	,	kIx,	,
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
do	do	k7c2	do
československých	československý	k2eAgFnPc2d1	Československá
žádostí	žádost	k1gFnPc2	žádost
o	o	k7c4	o
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
pomoc	pomoc	k1gFnSc4	pomoc
Sovětský	sovětský	k2eAgInSc4d1	sovětský
svaz	svaz	k1gInSc4	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
iniciativy	iniciativa	k1gFnSc2	iniciativa
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
čs	čs	kA	čs
<g/>
.	.	kIx.	.
vojenským	vojenský	k2eAgMnPc3d1	vojenský
činitelům	činitel	k1gMnPc3	činitel
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
sovětské	sovětský	k2eAgInPc4d1	sovětský
podněty	podnět	k1gInPc4	podnět
čs	čs	kA	čs
<g/>
.	.	kIx.	.
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
národní	národní	k2eAgFnSc2d1	národní
obrany	obrana	k1gFnSc2	obrana
nereagovalo	reagovat	k5eNaBmAgNnS	reagovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
stále	stále	k6eAd1	stále
počítalo	počítat	k5eAaImAgNnS	počítat
s	s	k7c7	s
britskou	britský	k2eAgFnSc7d1	britská
pomocí	pomoc	k1gFnSc7	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
však	však	k9	však
britský	britský	k2eAgInSc4d1	britský
zdrženlivý	zdrženlivý	k2eAgInSc4d1	zdrženlivý
postoj	postoj	k1gInSc4	postoj
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zvrátit	zvrátit	k5eAaPmF	zvrátit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
a	a	k8xC	a
srpna	srpen	k1gInSc2	srpen
1943	[number]	k4	1943
zahájena	zahájit	k5eAaPmNgFnS	zahájit
jednání	jednání	k1gNnPc4	jednání
se	s	k7c7	s
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Beneš	Beneš	k1gMnSc1	Beneš
již	již	k9	již
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1943	[number]	k4	1943
žádal	žádat	k5eAaImAgMnS	žádat
slovenské	slovenský	k2eAgMnPc4d1	slovenský
odbojáře	odbojář	k1gMnPc4	odbojář
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
objasňovali	objasňovat	k5eAaImAgMnP	objasňovat
slovenské	slovenský	k2eAgFnSc3d1	slovenská
veřejnosti	veřejnost	k1gFnSc3	veřejnost
nepříznivé	příznivý	k2eNgNnSc1d1	nepříznivé
postavení	postavení	k1gNnSc1	postavení
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
slovenský	slovenský	k2eAgInSc1d1	slovenský
lid	lid	k1gInSc1	lid
v	v	k7c6	v
očích	oko	k1gNnPc6	oko
demokratického	demokratický	k2eAgInSc2d1	demokratický
a	a	k8xC	a
kulturního	kulturní	k2eAgInSc2d1	kulturní
světa	svět	k1gInSc2	svět
dostal	dostat	k5eAaPmAgInS	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Jedině	jedině	k6eAd1	jedině
boj	boj	k1gInSc1	boj
čs	čs	kA	čs
<g/>
.	.	kIx.	.
emigrace	emigrace	k1gFnSc2	emigrace
za	za	k7c4	za
obnovu	obnova	k1gFnSc4	obnova
ČSR	ČSR	kA	ČSR
"	"	kIx"	"
<g/>
zachrání	zachránit	k5eAaPmIp3nP	zachránit
Slovensko	Slovensko	k1gNnSc4	Slovensko
a	a	k8xC	a
Slováky	Slovák	k1gMnPc4	Slovák
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nebyli	být	k5eNaImAgMnP	být
pokládáni	pokládat	k5eAaImNgMnP	pokládat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
za	za	k7c4	za
poražený	poražený	k2eAgInSc4d1	poražený
národ	národ	k1gInSc4	národ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
česky	česky	k6eAd1	česky
a	a	k8xC	a
slovensky	slovensky	k6eAd1	slovensky
proneseném	pronesený	k2eAgInSc6d1	pronesený
rozhlasovém	rozhlasový	k2eAgInSc6d1	rozhlasový
projevu	projev	k1gInSc6	projev
ze	z	k7c2	z
dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1943	[number]	k4	1943
ke	k	k7c3	k
Slovákům	Slováky	k1gInPc3	Slováky
je	být	k5eAaImIp3nS	být
čs	čs	kA	čs
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
potírali	potírat	k5eAaImAgMnP	potírat
Tisův	Tisův	k2eAgInSc4d1	Tisův
klerofašistický	klerofašistický	k2eAgInSc4d1	klerofašistický
režim	režim	k1gInSc4	režim
a	a	k8xC	a
podporovali	podporovat	k5eAaImAgMnP	podporovat
osvobozenecké	osvobozenecký	k2eAgNnSc4d1	osvobozenecké
úsilí	úsilí	k1gNnSc4	úsilí
Spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
upozornil	upozornit	k5eAaPmAgMnS	upozornit
na	na	k7c4	na
možné	možný	k2eAgFnPc4d1	možná
další	další	k2eAgFnPc4d1	další
slovenské	slovenský	k2eAgFnPc4d1	slovenská
územní	územní	k2eAgFnPc4d1	územní
ztráty	ztráta	k1gFnPc4	ztráta
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
vypukne	vypuknout	k5eAaPmIp3nS	vypuknout
protifašistický	protifašistický	k2eAgInSc1d1	protifašistický
převrat	převrat	k1gInSc1	převrat
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Československu	Československo	k1gNnSc6	Československo
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
způsobeny	způsoben	k2eAgFnPc4d1	způsobena
vážné	vážný	k2eAgFnPc4d1	vážná
územní	územní	k2eAgFnPc4d1	územní
ztráty	ztráta	k1gFnPc4	ztráta
-	-	kIx~	-
od	od	k7c2	od
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
uchvácených	uchvácený	k2eAgFnPc2d1	uchvácená
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
celé	celý	k2eAgNnSc4d1	celé
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
důvodů	důvod	k1gInPc2	důvod
mělo	mít	k5eAaImAgNnS	mít
pro	pro	k7c4	pro
čs	čs	kA	čs
<g/>
.	.	kIx.	.
vládu	vláda	k1gFnSc4	vláda
povstání	povstání	k1gNnSc2	povstání
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
mezinárodněpolitický	mezinárodněpolitický	k2eAgInSc4d1	mezinárodněpolitický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Povstalecké	povstalecký	k2eAgNnSc1d1	povstalecké
vystoupení	vystoupení	k1gNnSc1	vystoupení
mělo	mít	k5eAaImAgNnS	mít
před	před	k7c7	před
celým	celý	k2eAgInSc7d1	celý
světem	svět	k1gInSc7	svět
stvrdit	stvrdit	k5eAaPmF	stvrdit
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
negaci	negace	k1gFnSc4	negace
ze	z	k7c2	z
dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vánocemi	vánoce	k1gFnPc7	vánoce
1943	[number]	k4	1943
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
podepsána	podepsat	k5eAaPmNgFnS	podepsat
tzv.	tzv.	kA	tzv.
Vánoční	vánoční	k2eAgFnSc1d1	vánoční
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
sjednotila	sjednotit	k5eAaPmAgFnS	sjednotit
zejména	zejména	k9	zejména
demokratický	demokratický	k2eAgInSc4d1	demokratický
a	a	k8xC	a
komunistický	komunistický	k2eAgInSc4d1	komunistický
odboj	odboj	k1gInSc4	odboj
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
zástupci	zástupce	k1gMnPc1	zástupce
občanských	občanský	k2eAgFnPc2d1	občanská
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
sociální	sociální	k2eAgFnSc2d1	sociální
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
tak	tak	k6eAd1	tak
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
vůdčím	vůdčí	k2eAgInSc7d1	vůdčí
orgánem	orgán	k1gInSc7	orgán
přípravy	příprava	k1gFnSc2	příprava
a	a	k8xC	a
vedení	vedení	k1gNnSc2	vedení
povstání	povstání	k1gNnSc2	povstání
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
předurčena	předurčit	k5eAaPmNgFnS	předurčit
k	k	k7c3	k
převzetí	převzetí	k1gNnSc3	převzetí
politické	politický	k2eAgFnSc2d1	politická
moci	moc	k1gFnSc2	moc
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
po	po	k7c6	po
svržení	svržení	k1gNnSc6	svržení
vlády	vláda	k1gFnSc2	vláda
Josefa	Josefa	k1gFnSc1	Josefa
Tisa	Tisa	k1gFnSc1	Tisa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
plán	plán	k1gInSc4	plán
SNP	SNP	kA	SNP
československé	československý	k2eAgFnSc2d1	Československá
vlády	vláda	k1gFnSc2	vláda
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1943	[number]	k4	1943
čs	čs	kA	čs
<g/>
.	.	kIx.	.
vláda	vláda	k1gFnSc1	vláda
vydala	vydat	k5eAaPmAgFnS	vydat
první	první	k4xOgInSc4	první
plán	plán	k1gInSc4	plán
na	na	k7c4	na
vojenské	vojenský	k2eAgNnSc4d1	vojenské
povstalecké	povstalecký	k2eAgNnSc4d1	povstalecké
vystoupení	vystoupení	k1gNnSc4	vystoupení
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
-	-	kIx~	-
nastolení	nastolení	k1gNnSc6	nastolení
československé	československý	k2eAgFnSc2d1	Československá
svrchovanosti	svrchovanost	k1gFnSc2	svrchovanost
<g/>
.	.	kIx.	.
</s>
<s>
Povstání	povstání	k1gNnSc1	povstání
mělo	mít	k5eAaImAgNnS	mít
proběhnout	proběhnout	k5eAaPmF	proběhnout
nejpozději	pozdě	k6eAd3	pozdě
při	při	k7c6	při
kapitulaci	kapitulace	k1gFnSc6	kapitulace
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pádu	pád	k1gInSc3	pád
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1944	[number]	k4	1944
Beneš	Beneš	k1gMnSc1	Beneš
ve	v	k7c6	v
zmíněném	zmíněný	k2eAgInSc6d1	zmíněný
projevu	projev	k1gInSc6	projev
ve	v	k7c6	v
Státní	státní	k2eAgFnSc6d1	státní
radě	rada	k1gFnSc6	rada
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
přípravou	příprava	k1gFnSc7	příprava
povstání	povstání	k1gNnSc2	povstání
již	již	k6eAd1	již
mohl	moct	k5eAaImAgInS	moct
konstatovat	konstatovat	k5eAaBmF	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
současný	současný	k2eAgInSc1d1	současný
vývoj	vývoj	k1gInSc1	vývoj
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
jej	on	k3xPp3gMnSc4	on
naplnil	naplnit	k5eAaPmAgInS	naplnit
"	"	kIx"	"
<g/>
přímo	přímo	k6eAd1	přímo
nečekaným	čekaný	k2eNgNnSc7d1	nečekané
uspokojením	uspokojení	k1gNnSc7	uspokojení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1944	[number]	k4	1944
Štáb	štáb	k1gInSc1	štáb
pro	pro	k7c4	pro
vybudování	vybudování	k1gNnSc4	vybudování
branné	branný	k2eAgFnSc2d1	Branná
moci	moc	k1gFnSc2	moc
při	při	k7c6	při
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
národní	národní	k2eAgFnSc2d1	národní
obrany	obrana	k1gFnSc2	obrana
dokončil	dokončit	k5eAaPmAgMnS	dokončit
obecné	obecný	k2eAgFnSc2d1	obecná
směrnice	směrnice	k1gFnSc2	směrnice
pro	pro	k7c4	pro
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
akci	akce	k1gFnSc4	akce
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
plány	plán	k1gInPc4	plán
vojenského	vojenský	k2eAgNnSc2d1	vojenské
povstání	povstání	k1gNnSc2	povstání
<g/>
,	,	kIx,	,
řízeného	řízený	k2eAgNnSc2d1	řízené
londýnským	londýnský	k2eAgNnSc7d1	Londýnské
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
skupinu	skupina	k1gFnSc4	skupina
slovenských	slovenský	k2eAgMnPc2d1	slovenský
důstojníků	důstojník	k1gMnPc2	důstojník
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
pplk.	pplk.	kA	pplk.
Jánem	Ján	k1gMnSc7	Ján
Golianem	Golian	k1gMnSc7	Golian
<g/>
.	.	kIx.	.
</s>
<s>
Konspirativní	konspirativní	k2eAgNnSc1d1	konspirativní
Slovenské	slovenský	k2eAgNnSc1d1	slovenské
vojenské	vojenský	k2eAgNnSc1d1	vojenské
ústředí	ústředí	k1gNnSc1	ústředí
pracovalo	pracovat	k5eAaImAgNnS	pracovat
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Velitelství	velitelství	k1gNnSc2	velitelství
pozemního	pozemní	k2eAgNnSc2d1	pozemní
vojska	vojsko	k1gNnSc2	vojsko
v	v	k7c6	v
Banské	banský	k2eAgFnSc6d1	Banská
Bystrici	Bystrica	k1gFnSc6	Bystrica
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
definitivnímu	definitivní	k2eAgNnSc3d1	definitivní
ustanovení	ustanovení	k1gNnSc3	ustanovení
vojenského	vojenský	k2eAgNnSc2d1	vojenské
a	a	k8xC	a
politického	politický	k2eAgNnSc2d1	politické
vedení	vedení	k1gNnSc2	vedení
povstání	povstání	k1gNnSc2	povstání
došlo	dojít	k5eAaPmAgNnS	dojít
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
pplk.	pplk.	kA	pplk.
Golian	Golian	k1gMnSc1	Golian
jako	jako	k8xC	jako
velitel	velitel	k1gMnSc1	velitel
Slovenského	slovenský	k2eAgNnSc2d1	slovenské
vojenského	vojenský	k2eAgNnSc2d1	vojenské
ústředí	ústředí	k1gNnSc2	ústředí
(	(	kIx(	(
<g/>
SVÚ	SVÚ	kA	SVÚ
<g/>
)	)	kIx)	)
pověřen	pověřen	k2eAgMnSc1d1	pověřen
Benešem	Beneš	k1gMnSc7	Beneš
k	k	k7c3	k
vedení	vedení	k1gNnSc3	vedení
vojenských	vojenský	k2eAgFnPc2d1	vojenská
akcí	akce	k1gFnPc2	akce
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
zvýšení	zvýšení	k1gNnSc6	zvýšení
aktivity	aktivita	k1gFnSc2	aktivita
partyzánů	partyzán	k1gMnPc2	partyzán
vyslaných	vyslaný	k2eAgMnPc2d1	vyslaný
z	z	k7c2	z
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
nevídaném	vídaný	k2eNgInSc6d1	nevídaný
teroru	teror	k1gInSc6	teror
nejen	nejen	k6eAd1	nejen
proti	proti	k7c3	proti
německému	německý	k2eAgNnSc3d1	německé
civilnímu	civilní	k2eAgNnSc3d1	civilní
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
příslušníkům	příslušník	k1gMnPc3	příslušník
Hlinkovy	Hlinkův	k2eAgFnSc2d1	Hlinkova
gardy	garda	k1gFnSc2	garda
a	a	k8xC	a
některým	některý	k3yIgMnPc3	některý
katolickým	katolický	k2eAgMnPc3d1	katolický
kněžím	kněz	k1gMnPc3	kněz
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zhroucení	zhroucení	k1gNnSc3	zhroucení
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
Tisova	Tisův	k2eAgInSc2d1	Tisův
Slovenského	slovenský	k2eAgInSc2d1	slovenský
štátu	štát	k1gInSc2	štát
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
klerofašistická	klerofašistický	k2eAgFnSc1d1	klerofašistická
vláda	vláda	k1gFnSc1	vláda
nucena	nutit	k5eAaImNgFnS	nutit
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
stanné	stanný	k2eAgNnSc4d1	stanné
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Zavražděním	zavraždění	k1gNnSc7	zavraždění
všech	všecek	k3xTgMnPc2	všecek
členů	člen	k1gMnPc2	člen
německé	německý	k2eAgFnSc2d1	německá
vojenské	vojenský	k2eAgFnSc2d1	vojenská
mise	mise	k1gFnSc2	mise
vracející	vracející	k2eAgFnSc2d1	vracející
se	se	k3xPyFc4	se
z	z	k7c2	z
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
partyzánskou	partyzánský	k2eAgFnSc7d1	Partyzánská
skupinou	skupina	k1gFnSc7	skupina
kpt.	kpt.	k?	kpt.
Veličky	Velička	k1gFnSc2	Velička
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
vojenskými	vojenský	k2eAgInPc7d1	vojenský
incidenty	incident	k1gInPc7	incident
<g/>
,	,	kIx,	,
dospěla	dochvít	k5eAaPmAgFnS	dochvít
situace	situace	k1gFnSc1	situace
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
do	do	k7c2	do
kritického	kritický	k2eAgInSc2d1	kritický
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgMnSc1d1	německý
vyslanec	vyslanec	k1gMnSc1	vyslanec
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
Hanns	Hannsa	k1gFnPc2	Hannsa
Ludin	Ludin	k1gMnSc1	Ludin
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
německý	německý	k2eAgInSc4d1	německý
vojenský	vojenský	k2eAgInSc4d1	vojenský
zásah	zásah	k1gInSc4	zásah
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
nakonec	nakonec	k6eAd1	nakonec
slovenským	slovenský	k2eAgMnSc7d1	slovenský
prezidentem	prezident	k1gMnSc7	prezident
Tisem	tis	k1gInSc7	tis
schválen	schválit	k5eAaPmNgInS	schválit
<g/>
.	.	kIx.	.
</s>
<s>
Ukrajinský	ukrajinský	k2eAgInSc1d1	ukrajinský
štáb	štáb	k1gInSc1	štáb
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vysílal	vysílat	k5eAaImAgInS	vysílat
během	během	k7c2	během
léta	léto	k1gNnSc2	léto
partyzánské	partyzánský	k2eAgFnSc2d1	Partyzánská
jednotky	jednotka	k1gFnSc2	jednotka
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
<g/>
,	,	kIx,	,
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
důsledku	důsledek	k1gInSc6	důsledek
přivolal	přivolat	k5eAaPmAgInS	přivolat
do	do	k7c2	do
země	zem	k1gFnSc2	zem
německá	německý	k2eAgNnPc1d1	německé
vojska	vojsko	k1gNnPc1	vojsko
před	před	k7c7	před
dokončením	dokončení	k1gNnSc7	dokončení
příprav	příprava	k1gFnPc2	příprava
povstání	povstání	k1gNnSc2	povstání
čs	čs	kA	čs
<g/>
.	.	kIx.	.
<g/>
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1944	[number]	k4	1944
ministr	ministr	k1gMnSc1	ministr
vnitra	vnitro	k1gNnSc2	vnitro
slovenské	slovenský	k2eAgFnSc2d1	slovenská
vlády	vláda	k1gFnSc2	vláda
Alexander	Alexandra	k1gFnPc2	Alexandra
Mach	Mach	k1gMnSc1	Mach
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
sděloval	sdělovat	k5eAaImAgMnS	sdělovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
německé	německý	k2eAgInPc4d1	německý
vojska	vojsko	k1gNnPc1	vojsko
budou	být	k5eAaImBp3nP	být
okupovat	okupovat	k5eAaBmF	okupovat
slovenské	slovenský	k2eAgNnSc4d1	slovenské
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
to	ten	k3xDgNnSc4	ten
oznámil	oznámit	k5eAaPmAgMnS	oznámit
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
Slovenského	slovenský	k2eAgInSc2d1	slovenský
štátu	štát	k1gInSc2	štát
generál	generál	k1gMnSc1	generál
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Čatloš	Čatloš	k1gMnSc1	Čatloš
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
den	den	k1gInSc4	den
začala	začít	k5eAaPmAgFnS	začít
vojenská	vojenský	k2eAgFnSc1d1	vojenská
okupace	okupace	k1gFnSc1	okupace
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Pzn	Pzn	k1gFnPc6	Pzn
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
povstání	povstání	k1gNnSc2	povstání
československou	československý	k2eAgFnSc7d1	Československá
exilovou	exilový	k2eAgFnSc7d1	exilová
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
politicky	politicky	k6eAd1	politicky
narušována	narušovat	k5eAaImNgFnS	narušovat
činností	činnost	k1gFnSc7	činnost
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
podporované	podporovaný	k2eAgNnSc1d1	podporované
KSČ	KSČ	kA	KSČ
vedené	vedený	k2eAgNnSc1d1	vedené
Klementem	Klement	k1gMnSc7	Klement
Gottwaldem	Gottwald	k1gMnSc7	Gottwald
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
a	a	k8xC	a
také	také	k9	také
slovenskými	slovenský	k2eAgMnPc7d1	slovenský
nacionalisty	nacionalista	k1gMnPc7	nacionalista
působícími	působící	k2eAgMnPc7d1	působící
v	v	k7c6	v
politických	politický	k2eAgFnPc6d1	politická
stranách	strana	k1gFnPc6	strana
i	i	k8xC	i
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Ilegální	ilegální	k2eAgFnSc1d1	ilegální
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
byla	být	k5eAaImAgFnS	být
pod	pod	k7c7	pod
stále	stále	k6eAd1	stále
větším	veliký	k2eAgInSc7d2	veliký
vlivem	vliv	k1gInSc7	vliv
KSS	KSS	kA	KSS
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Gustávem	Gustáv	k1gMnSc7	Gustáv
Husákem	Husák	k1gMnSc7	Husák
a	a	k8xC	a
Karlom	Karlom	k1gInSc4	Karlom
Šmidkem	Šmidek	k1gInSc7	Šmidek
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nechtěli	chtít	k5eNaImAgMnP	chtít
plnou	plný	k2eAgFnSc4d1	plná
obnovu	obnova	k1gFnSc4	obnova
demokratické	demokratický	k2eAgFnSc2d1	demokratická
čs	čs	kA	čs
<g/>
.	.	kIx.	.
<g/>
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
prosazovali	prosazovat	k5eAaImAgMnP	prosazovat
větší	veliký	k2eAgFnSc4d2	veliký
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Slovenska	Slovensko	k1gNnSc2	Slovensko
svými	svůj	k3xOyFgInPc7	svůj
kroky	krok	k1gInPc7	krok
posilovala	posilovat	k5eAaImAgFnS	posilovat
moc	moc	k1gFnSc1	moc
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
často	často	k6eAd1	často
oslabovala	oslabovat	k5eAaImAgFnS	oslabovat
Benešovu	Benešův	k2eAgFnSc4d1	Benešova
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
vizi	vize	k1gFnSc4	vize
a	a	k8xC	a
její	její	k3xOp3gFnSc4	její
koncepci	koncepce	k1gFnSc4	koncepce
<g/>
.	.	kIx.	.
</s>
<s>
Vrchní	vrchní	k2eAgNnSc1d1	vrchní
velení	velení	k1gNnSc1	velení
sovětských	sovětský	k2eAgNnPc2d1	sovětské
vojsk	vojsko	k1gNnPc2	vojsko
původně	původně	k6eAd1	původně
neplánovalo	plánovat	k5eNaImAgNnS	plánovat
osvobození	osvobození	k1gNnSc4	osvobození
slovenského	slovenský	k2eAgNnSc2d1	slovenské
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
dramaticky	dramaticky	k6eAd1	dramaticky
změnila	změnit	k5eAaPmAgFnS	změnit
po	po	k7c4	po
kapitulaci	kapitulace	k1gFnSc4	kapitulace
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
a	a	k8xC	a
situaci	situace	k1gFnSc4	situace
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
frontě	fronta	k1gFnSc6	fronta
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
hranice	hranice	k1gFnPc1	hranice
Slovenska	Slovensko	k1gNnSc2	Slovensko
staly	stát	k5eAaPmAgFnP	stát
z	z	k7c2	z
vojenského	vojenský	k2eAgNnSc2d1	vojenské
hlediska	hledisko	k1gNnSc2	hledisko
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgNnSc1d1	důležité
.	.	kIx.	.
</s>
<s>
J.V.	J.V.	k?	J.V.
Stalin	Stalin	k1gMnSc1	Stalin
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
Benešovu	Benešův	k2eAgFnSc4d1	Benešova
koncepci	koncepce	k1gFnSc4	koncepce
SNP	SNP	kA	SNP
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
hlavních	hlavní	k2eAgInPc6d1	hlavní
cílech	cíl	k1gInPc6	cíl
nepodpořit	podpořit	k5eNaPmF	podpořit
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
ji	on	k3xPp3gFnSc4	on
de	de	k?	de
facto	fact	k2eAgNnSc1d1	facto
rozbít	rozbít	k5eAaPmF	rozbít
i	i	k9	i
s	s	k7c7	s
vědomím	vědomí	k1gNnSc7	vědomí
nesouhlasného	souhlasný	k2eNgInSc2d1	nesouhlasný
postoje	postoj	k1gInSc2	postoj
zejména	zejména	k9	zejména
Gustáva	Gustáva	k1gFnSc1	Gustáva
Husáka	Husák	k1gMnSc2	Husák
a	a	k8xC	a
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
důsledku	důsledek	k1gInSc6	důsledek
J.V.	J.V.	k1gMnSc1	J.V.
Stalin	Stalin	k1gMnSc1	Stalin
opět	opět	k6eAd1	opět
prokázal	prokázat	k5eAaPmAgMnS	prokázat
svoji	svůj	k3xOyFgFnSc4	svůj
schopnost	schopnost	k1gFnSc4	schopnost
dokonalé	dokonalý	k2eAgFnSc2d1	dokonalá
strategické	strategický	k2eAgFnSc2d1	strategická
manipulace	manipulace	k1gFnSc2	manipulace
<g/>
.	.	kIx.	.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
vyslala	vyslat	k5eAaPmAgFnS	vyslat
k	k	k7c3	k
jednání	jednání	k1gNnSc3	jednání
s	s	k7c7	s
vládou	vláda	k1gFnSc7	vláda
SSSR	SSSR	kA	SSSR
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
své	svůj	k3xOyFgFnSc2	svůj
poslance	poslanec	k1gMnSc2	poslanec
kpt.	kpt.	k?	kpt.
Ferjenčíka	Ferjenčík	k1gMnSc2	Ferjenčík
a	a	k8xC	a
Karla	Karel	k1gMnSc2	Karel
Šmidkeho	Šmidke	k1gMnSc2	Šmidke
již	již	k6eAd1	již
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
vědomí	vědomí	k1gNnSc2	vědomí
Beneše	Beneš	k1gMnSc2	Beneš
poslanci	poslanec	k1gMnPc1	poslanec
tajně	tajně	k6eAd1	tajně
jednali	jednat	k5eAaImAgMnP	jednat
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
NKVD	NKVD	kA	NKVD
s	s	k7c7	s
vládou	vláda	k1gFnSc7	vláda
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
až	až	k9	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Komunistický	komunistický	k2eAgMnSc1d1	komunistický
diktátor	diktátor	k1gMnSc1	diktátor
J.V.	J.V.	k1gMnSc1	J.V.
Stalin	Stalin	k1gMnSc1	Stalin
chtěl	chtít	k5eAaImAgMnS	chtít
Slovensko	Slovensko	k1gNnSc4	Slovensko
dokonce	dokonce	k9	dokonce
začlenit	začlenit	k5eAaPmF	začlenit
mezi	mezi	k7c4	mezi
svazové	svazový	k2eAgFnPc4d1	svazová
republiky	republika	k1gFnPc4	republika
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
Sovětské	sovětský	k2eAgNnSc1d1	sovětské
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
vyslala	vyslat	k5eAaPmAgFnS	vyslat
své	svůj	k3xOyFgMnPc4	svůj
zplnomocněnce	zplnomocněnec	k1gMnPc4	zplnomocněnec
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
až	až	k9	až
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
.	.	kIx.	.
</s>
<s>
Sovětská	sovětský	k2eAgFnSc1d1	sovětská
vláda	vláda	k1gFnSc1	vláda
tajně	tajně	k6eAd1	tajně
vyjednávala	vyjednávat	k5eAaImAgFnS	vyjednávat
s	s	k7c7	s
poslanci	poslanec	k1gMnPc7	poslanec
SNR	SNR	kA	SNR
i	i	k8xC	i
čs	čs	kA	čs
<g/>
.	.	kIx.	.
<g/>
komunisty	komunista	k1gMnSc2	komunista
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
přítomnost	přítomnost	k1gFnSc1	přítomnost
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
Benešovým	Benešová	k1gFnPc3	Benešová
poslancům	poslanec	k1gMnPc3	poslanec
utajila	utajit	k5eAaPmAgFnS	utajit
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
skutečnost	skutečnost	k1gFnSc4	skutečnost
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
snažil	snažit	k5eAaImAgMnS	snažit
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
vyslanec	vyslanec	k1gMnSc1	vyslanec
SSSR	SSSR	kA	SSSR
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
přímo	přímo	k6eAd1	přímo
Edvardu	Edvard	k1gMnSc3	Edvard
Benešovi	Beneš	k1gMnSc3	Beneš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zahájení	zahájení	k1gNnSc1	zahájení
SNP	SNP	kA	SNP
==	==	k?	==
</s>
</p>
<p>
<s>
Vojenské	vojenský	k2eAgNnSc1d1	vojenské
ústředí	ústředí	k1gNnSc1	ústředí
v	v	k7c6	v
Banské	banský	k2eAgFnSc6d1	Banská
Bystrici	Bystrica	k1gFnSc6	Bystrica
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
muselo	muset	k5eAaImAgNnS	muset
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
postup	postup	k1gInSc4	postup
německých	německý	k2eAgNnPc2d1	německé
vojsk	vojsko	k1gNnPc2	vojsko
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
a	a	k8xC	a
vydalo	vydat	k5eAaPmAgNnS	vydat
předčasně	předčasně	k6eAd1	předčasně
rozkaz	rozkaz	k1gInSc4	rozkaz
k	k	k7c3	k
zahájení	zahájení	k1gNnSc3	zahájení
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Dohodnutý	dohodnutý	k2eAgInSc1d1	dohodnutý
signál	signál	k1gInSc1	signál
zněl	znět	k5eAaImAgInS	znět
:	:	kIx,	:
Začněte	začít	k5eAaPmRp2nP	začít
s	s	k7c7	s
vystěhováním	vystěhování	k1gNnSc7	vystěhování
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
od	od	k7c2	od
20.00	[number]	k4	20.00
dnešního	dnešní	k2eAgInSc2d1	dnešní
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Povstání	povstání	k1gNnSc1	povstání
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
tedy	tedy	k9	tedy
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
bez	bez	k7c2	bez
přímého	přímý	k2eAgInSc2d1	přímý
rozkazu	rozkaz	k1gInSc2	rozkaz
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
či	či	k8xC	či
zahraničního	zahraniční	k2eAgNnSc2d1	zahraniční
ústředí	ústředí	k1gNnSc2	ústředí
čs	čs	kA	čs
<g/>
.	.	kIx.	.
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
generál	generál	k1gMnSc1	generál
Ján	Ján	k1gMnSc1	Ján
Golián	Golián	k1gMnSc1	Golián
písemně	písemně	k6eAd1	písemně
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
zahájení	zahájení	k1gNnSc4	zahájení
povstání	povstání	k1gNnSc2	povstání
všem	všecek	k3xTgFnPc3	všecek
posádkám	posádka	k1gFnPc3	posádka
a	a	k8xC	a
důrazně	důrazně	k6eAd1	důrazně
žádal	žádat	k5eAaImAgMnS	žádat
východní	východní	k2eAgFnSc4d1	východní
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
nenechala	nechat	k5eNaPmAgFnS	nechat
zaskočit	zaskočit	k5eAaPmF	zaskočit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
po	po	k7c6	po
obdržení	obdržení	k1gNnSc6	obdržení
zpráv	zpráva	k1gFnPc2	zpráva
o	o	k7c6	o
ozbrojeném	ozbrojený	k2eAgNnSc6d1	ozbrojené
vystoupení	vystoupení	k1gNnSc6	vystoupení
povstalců	povstalec	k1gMnPc2	povstalec
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
sešla	sejít	k5eAaPmAgFnS	sejít
čs	čs	kA	čs
<g/>
.	.	kIx.	.
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
na	na	k7c6	na
mimořádném	mimořádný	k2eAgNnSc6d1	mimořádné
zasedání	zasedání	k1gNnSc6	zasedání
a	a	k8xC	a
proklamativně	proklamativně	k6eAd1	proklamativně
se	se	k3xPyFc4	se
postavila	postavit	k5eAaPmAgFnS	postavit
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
veškeré	veškerý	k3xTgFnPc4	veškerý
vojenské	vojenský	k2eAgFnPc4d1	vojenská
<g/>
,	,	kIx,	,
paradesantní	paradesantní	k2eAgFnPc4d1	paradesantní
a	a	k8xC	a
partyzánské	partyzánský	k2eAgFnPc4d1	Partyzánská
jednotky	jednotka	k1gFnPc4	jednotka
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
za	za	k7c2	za
člena	člen	k1gMnSc2	člen
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
armád	armáda	k1gFnPc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
v	v	k7c6	v
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
45	[number]	k4	45
se	se	k3xPyFc4	se
čs	čs	kA	čs
<g/>
.	.	kIx.	.
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
Sergej	Sergej	k1gMnSc1	Sergej
lngr	lngr	k1gMnSc1	lngr
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
rozhlase	rozhlas	k1gInSc6	rozhlas
obrátil	obrátit	k5eAaPmAgMnS	obrátit
s	s	k7c7	s
pozdravem	pozdrav	k1gInSc7	pozdrav
k	k	k7c3	k
povstalcům	povstalec	k1gMnPc3	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
Zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
povstání	povstání	k1gNnSc1	povstání
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
legionářskou	legionářský	k2eAgFnSc4d1	legionářská
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
ze	z	k7c2	z
současnosti	současnost	k1gFnSc2	současnost
pak	pak	k6eAd1	pak
na	na	k7c4	na
boje	boj	k1gInPc4	boj
čs	čs	kA	čs
<g/>
.	.	kIx.	.
letců	letec	k1gMnPc2	letec
a	a	k8xC	a
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
armád	armáda	k1gFnPc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
důraz	důraz	k1gInSc1	důraz
kladl	klást	k5eAaImAgInS	klást
na	na	k7c4	na
bezpodmínečnou	bezpodmínečný	k2eAgFnSc4d1	bezpodmínečná
poslušnost	poslušnost	k1gFnSc4	poslušnost
vojenskému	vojenský	k2eAgInSc3d1	vojenský
velení	velení	k1gNnSc2	velení
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
prezident	prezident	k1gMnSc1	prezident
Jozef	Jozef	k1gMnSc1	Jozef
Tiso	Tisa	k1gFnSc5	Tisa
požádal	požádat	k5eAaPmAgInS	požádat
slovenský	slovenský	k2eAgInSc1d1	slovenský
lid	lid	k1gInSc1	lid
o	o	k7c6	o
kolaboraci	kolaborace	k1gFnSc6	kolaborace
s	s	k7c7	s
německým	německý	k2eAgNnSc7d1	německé
okupačním	okupační	k2eAgNnSc7d1	okupační
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dokonalému	dokonalý	k2eAgInSc3d1	dokonalý
zmatku	zmatek	k1gInSc3	zmatek
zejména	zejména	k9	zejména
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
přispěl	přispět	k5eAaPmAgMnS	přispět
svým	svůj	k3xOyFgNnSc7	svůj
rádiovým	rádiový	k2eAgNnSc7d1	rádiové
prohlášením	prohlášení	k1gNnSc7	prohlášení
velitel	velitel	k1gMnSc1	velitel
východoslovenské	východoslovenský	k2eAgFnSc2d1	Východoslovenská
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
nejuznávanější	uznávaný	k2eAgMnSc1d3	nejuznávanější
slovenský	slovenský	k2eAgMnSc1d1	slovenský
generál	generál	k1gMnSc1	generál
Augustín	Augustín	k1gMnSc1	Augustín
Malár	Malár	k1gMnSc1	Malár
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nic	nic	k3yNnSc1	nic
nepodnikala	podnikat	k5eNaImAgFnS	podnikat
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
ještě	ještě	k6eAd1	ještě
nenastal	nastat	k5eNaPmAgInS	nastat
její	její	k3xOp3gInSc4	její
pravý	pravý	k2eAgInSc4d1	pravý
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc3d1	východní
armádě	armáda	k1gFnSc3	armáda
vzkázal	vzkázat	k5eAaPmAgMnS	vzkázat
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
vyčká	vyčkat	k5eAaPmIp3nS	vyčkat
jeho	on	k3xPp3gInSc2	on
příjezdu	příjezd	k1gInSc2	příjezd
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
tak	tak	k9	tak
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
učinil	učinit	k5eAaImAgMnS	učinit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Němci	Němec	k1gMnSc3	Němec
v	v	k7c6	v
Prešově	Prešov	k1gInSc6	Prešov
zajat	zajmout	k5eAaPmNgMnS	zajmout
<g/>
.	.	kIx.	.
</s>
<s>
Zástupce	zástupce	k1gMnSc1	zástupce
velitele	velitel	k1gMnSc2	velitel
východní	východní	k2eAgFnSc2d1	východní
armády	armáda	k1gFnSc2	armáda
plk.	plk.	kA	plk.
Viliam	Viliam	k1gMnSc1	Viliam
Talský	Talský	k1gMnSc1	Talský
neprovedl	provést	k5eNaPmAgMnS	provést
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
mobilizaci	mobilizace	k1gFnSc4	mobilizace
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
mu	on	k3xPp3gMnSc3	on
nařídil	nařídit	k5eAaPmAgMnS	nařídit
plk.	plk.	kA	plk.
<g/>
Golian	Golian	k1gMnSc1	Golian
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
velmi	velmi	k6eAd1	velmi
bouřlivé	bouřlivý	k2eAgFnSc3d1	bouřlivá
atmosféře	atmosféra	k1gFnSc3	atmosféra
ve	v	k7c6	v
štábu	štáb	k1gInSc6	štáb
v	v	k7c6	v
Prešově	Prešov	k1gInSc6	Prešov
se	se	k3xPyFc4	se
Talský	Talský	k1gMnSc1	Talský
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
časně	časně	k6eAd1	časně
ráno	ráno	k6eAd1	ráno
ve	v	k7c6	v
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
odletět	odletět	k5eAaPmF	odletět
do	do	k7c2	do
Lvova	Lvov	k1gInSc2	Lvov
k	k	k7c3	k
veliteli	velitel	k1gMnSc3	velitel
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
frontu	front	k1gInSc2	front
<g/>
.	.	kIx.	.
</s>
<s>
Odpoledne	odpoledne	k6eAd1	odpoledne
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
ve	v	k7c6	v
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
byl	být	k5eAaImAgInS	být
maršálem	maršál	k1gMnSc7	maršál
I.	I.	kA	I.
<g/>
S.	S.	kA	S.
Koněvem	Koněv	k1gInSc7	Koněv
přijat	přijat	k2eAgInSc1d1	přijat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
konzultoval	konzultovat	k5eAaImAgMnS	konzultovat
hlavně	hlavně	k6eAd1	hlavně
součinnost	součinnost	k1gFnSc4	součinnost
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
divize	divize	k1gFnSc2	divize
se	s	k7c7	s
sovětskými	sovětský	k2eAgNnPc7d1	sovětské
vojsky	vojsko	k1gNnPc7	vojsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
z	z	k7c2	z
ilegality	ilegalita	k1gFnSc2	ilegalita
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
a	a	k8xC	a
v	v	k7c6	v
Banské	banský	k2eAgFnSc6d1	Banská
Bystrici	Bystrica	k1gFnSc6	Bystrica
převzala	převzít	k5eAaPmAgFnS	převzít
pravomoc	pravomoc	k1gFnSc4	pravomoc
vrcholného	vrcholný	k2eAgInSc2d1	vrcholný
orgánu	orgán	k1gInSc2	orgán
revoluční	revoluční	k2eAgFnSc2d1	revoluční
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Vládním	vládní	k2eAgInSc7d1	vládní
a	a	k8xC	a
výkonným	výkonný	k2eAgInSc7d1	výkonný
orgánem	orgán	k1gInSc7	orgán
SNR	SNR	kA	SNR
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Sbor	sbor	k1gInSc1	sbor
pověřenců	pověřenec	k1gMnPc2	pověřenec
s	s	k7c7	s
9	[number]	k4	9
až	až	k9	až
11	[number]	k4	11
pověřenci	pověřenec	k1gMnPc7	pověřenec
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgInSc3	jenž
předsedali	předsedat	k5eAaImAgMnP	předsedat
Karol	Karol	k1gInSc4	Karol
Šmidke	Šmidk	k1gFnSc2	Šmidk
a	a	k8xC	a
Vavro	Vavro	k1gNnSc4	Vavro
Šrobár	Šrobár	k1gMnSc1	Šrobár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odzbrojení	odzbrojení	k1gNnSc3	odzbrojení
východoslovenské	východoslovenský	k2eAgFnSc2d1	Východoslovenská
armády	armáda	k1gFnSc2	armáda
==	==	k?	==
</s>
</p>
<p>
<s>
Velitel	velitel	k1gMnSc1	velitel
slovenské	slovenský	k2eAgFnSc2d1	slovenská
povstalecké	povstalecký	k2eAgFnSc2d1	povstalecká
armády	armáda	k1gFnSc2	armáda
Ján	Ján	k1gMnSc1	Ján
Golian	Golian	k1gMnSc1	Golian
radiogramem	radiogram	k1gInSc7	radiogram
burcoval	burcovat	k5eAaImAgMnS	burcovat
východoslovenské	východoslovenský	k2eAgFnSc2d1	Východoslovenská
divize	divize	k1gFnSc2	divize
:	:	kIx,	:
"	"	kIx"	"
<g/>
Už	už	k6eAd1	už
tři	tři	k4xCgInPc1	tři
dny	den	k1gInPc1	den
jsme	být	k5eAaImIp1nP	být
v	v	k7c6	v
boji	boj	k1gInSc6	boj
s	s	k7c7	s
Němci	Němec	k1gMnPc7	Němec
a	a	k8xC	a
vy	vy	k3xPp2nPc1	vy
dosud	dosud	k6eAd1	dosud
ležíte	ležet	k5eAaImIp2nP	ležet
v	v	k7c6	v
nečinnosti	nečinnost	k1gFnSc6	nečinnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nařídil	nařídit	k5eAaPmAgMnS	nařídit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
1	[number]	k4	1
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc1	divize
probila	probít	k5eAaPmAgFnS	probít
do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
Váhu	Váh	k1gInSc2	Váh
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc1	divize
pronikla	proniknout	k5eAaPmAgFnS	proniknout
do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
Hronu	Hron	k1gInSc2	Hron
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rozkaz	rozkaz	k1gInSc1	rozkaz
však	však	k9	však
velení	velení	k1gNnSc2	velení
ani	ani	k9	ani
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
divizí	divize	k1gFnPc2	divize
východoslovenské	východoslovenský	k2eAgFnSc2d1	Východoslovenská
armády	armáda	k1gFnSc2	armáda
neuposlechlo	uposlechnout	k5eNaPmAgNnS	uposlechnout
<g/>
.	.	kIx.	.
</s>
<s>
Nejlépe	dobře	k6eAd3	dobře
vyzbrojená	vyzbrojený	k2eAgFnSc1d1	vyzbrojená
a	a	k8xC	a
vycvičená	vycvičený	k2eAgFnSc1d1	vycvičená
východoslovenská	východoslovenský	k2eAgFnSc1d1	Východoslovenská
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
pro	pro	k7c4	pro
úspěch	úspěch	k1gInSc4	úspěch
povstání	povstání	k1gNnSc2	povstání
sehrát	sehrát	k5eAaPmF	sehrát
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Slovenska	Slovensko	k1gNnSc2	Slovensko
již	již	k6eAd1	již
od	od	k7c2	od
února	únor	k1gInSc2	únor
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
divize	divize	k1gFnPc1	divize
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třiceti	třicet	k4xCc2	třicet
tisíc	tisíc	k4xCgInSc4	tisíc
vojáků	voják	k1gMnPc2	voják
měly	mít	k5eAaImAgInP	mít
zajistit	zajistit	k5eAaPmF	zajistit
průchod	průchod	k1gInSc4	průchod
Dukelským	dukelský	k2eAgInSc7d1	dukelský
průsmykem	průsmyk	k1gInSc7	průsmyk
sovětským	sovětský	k2eAgInSc7d1	sovětský
vojskům	vojsko	k1gNnPc3	vojsko
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
obsadila	obsadit	k5eAaPmAgFnS	obsadit
střed	střed	k1gInSc4	střed
Slovenska	Slovensko	k1gNnSc2	Slovensko
během	během	k7c2	během
několika	několik	k4yIc2	několik
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgMnSc1d1	další
zástupce	zástupce	k1gMnSc1	zástupce
východní	východní	k2eAgFnSc2d1	východní
armády	armáda	k1gFnSc2	armáda
pplk.	pplk.	kA	pplk.
František	František	k1gMnSc1	František
Urban	Urban	k1gMnSc1	Urban
nedostal	dostat	k5eNaPmAgMnS	dostat
od	od	k7c2	od
Tálského	Tálské	k1gNnSc2	Tálské
žádné	žádný	k3yNgFnSc2	žádný
vojenské	vojenský	k2eAgFnSc2d1	vojenská
instrukce	instrukce	k1gFnSc2	instrukce
a	a	k8xC	a
armáda	armáda	k1gFnSc1	armáda
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
bez	bez	k7c2	bez
vrchního	vrchní	k2eAgNnSc2d1	vrchní
velení	velení	k1gNnSc2	velení
<g/>
.	.	kIx.	.
</s>
<s>
Pplk.	pplk.	kA	pplk.
Urban	Urban	k1gMnSc1	Urban
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
události	událost	k1gFnPc4	událost
adekvátně	adekvátně	k6eAd1	adekvátně
vojensky	vojensky	k6eAd1	vojensky
reagovat	reagovat	k5eAaBmF	reagovat
a	a	k8xC	a
převzít	převzít	k5eAaPmF	převzít
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
politickou	politický	k2eAgFnSc4d1	politická
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
<g/>
.	.	kIx.	.
<g/>
Také	také	k9	také
další	další	k2eAgMnPc1d1	další
velitelé	velitel	k1gMnPc1	velitel
divizí	divize	k1gFnPc2	divize
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
podniknout	podniknout	k5eAaPmF	podniknout
účinnou	účinný	k2eAgFnSc4d1	účinná
a	a	k8xC	a
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
obranu	obrana	k1gFnSc4	obrana
svých	svůj	k3xOyFgFnPc2	svůj
brigád	brigáda	k1gFnPc2	brigáda
<g/>
.	.	kIx.	.
</s>
<s>
Velitel	velitel	k1gMnSc1	velitel
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
divize	divize	k1gFnSc2	divize
pplk.	pplk.	kA	pplk.
Josef	Josef	k1gMnSc1	Josef
Husár	Husár	k1gMnSc1	Husár
v	v	k7c4	v
tyto	tento	k3xDgInPc4	tento
dny	den	k1gInPc4	den
uvedl	uvést	k5eAaPmAgMnS	uvést
:	:	kIx,	:
"	"	kIx"	"
<g/>
Odmítám	odmítat	k5eAaImIp1nS	odmítat
se	se	k3xPyFc4	se
připojit	připojit	k5eAaPmF	připojit
k	k	k7c3	k
povstání	povstání	k1gNnSc3	povstání
!	!	kIx.	!
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
nepřehledná	přehledný	k2eNgFnSc1d1	nepřehledná
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
velmi	velmi	k6eAd1	velmi
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
<g/>
.	.	kIx.	.
</s>
<s>
Jsme	být	k5eAaImIp1nP	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
velmi	velmi	k6eAd1	velmi
slabí	slabit	k5eAaImIp3nP	slabit
a	a	k8xC	a
nedokážeme	dokázat	k5eNaPmIp1nP	dokázat
dlouho	dlouho	k6eAd1	dlouho
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
německým	německý	k2eAgFnPc3d1	německá
jednotkám	jednotka	k1gFnPc3	jednotka
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Velitel	velitel	k1gMnSc1	velitel
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
divize	divize	k1gFnSc2	divize
pplk.	pplk.	kA	pplk.
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Markus	Markus	k1gMnSc1	Markus
se	se	k3xPyFc4	se
však	však	k9	však
Husárovým	Husárův	k2eAgNnSc7d1	Husárův
prohlášením	prohlášení	k1gNnSc7	prohlášení
nenechal	nechat	k5eNaPmAgMnS	nechat
zlomit	zlomit	k5eAaPmF	zlomit
<g/>
.	.	kIx.	.
</s>
<s>
Rozjel	rozjet	k5eAaPmAgMnS	rozjet
se	se	k3xPyFc4	se
ihned	ihned	k6eAd1	ihned
autem	auto	k1gNnSc7	auto
do	do	k7c2	do
Vyšného	vyšný	k2eAgInSc2d1	vyšný
Svidníku	Svidník	k1gInSc2	Svidník
na	na	k7c6	na
velitelství	velitelství	k1gNnSc6	velitelství
2	[number]	k4	2
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc2	divize
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zabránil	zabránit	k5eAaPmAgInS	zabránit
rozpadu	rozpad	k1gInSc3	rozpad
celého	celý	k2eAgNnSc2d1	celé
velení	velení	k1gNnSc2	velení
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
<g/>
,	,	kIx,	,
této	tento	k3xDgFnSc6	tento
klíčové	klíčový	k2eAgFnSc6d1	klíčová
tragédii	tragédie	k1gFnSc6	tragédie
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
již	již	k6eAd1	již
nepodařilo	podařit	k5eNaPmAgNnS	podařit
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
Otálení	otálení	k1gNnSc1	otálení
a	a	k8xC	a
neakceschopnost	neakceschopnost	k1gFnSc1	neakceschopnost
důstojníků	důstojník	k1gMnPc2	důstojník
východoslovenské	východoslovenský	k2eAgFnSc2d1	Východoslovenská
armády	armáda	k1gFnSc2	armáda
způsobila	způsobit	k5eAaPmAgFnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
napomohla	napomoct	k5eAaPmAgFnS	napomoct
jednotce	jednotka	k1gFnSc3	jednotka
"	"	kIx"	"
<g/>
Heinrici	Heinrice	k1gFnSc3	Heinrice
<g/>
"	"	kIx"	"
zahájit	zahájit	k5eAaPmF	zahájit
odzbrojovaci	odzbrojovace	k1gFnSc4	odzbrojovace
akci	akce	k1gFnSc3	akce
Kartoffelernte	Kartoffelernt	k1gInSc5	Kartoffelernt
(	(	kIx(	(
<g/>
sběr	sběr	k1gInSc4	sběr
brambor	brambora	k1gFnPc2	brambora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
jednotka	jednotka	k1gFnSc1	jednotka
společně	společně	k6eAd1	společně
s	s	k7c7	s
24	[number]	k4	24
<g/>
.	.	kIx.	.
tankovým	tankový	k2eAgInSc7d1	tankový
sborem	sbor	k1gInSc7	sbor
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
odzbrojila	odzbrojit	k5eAaPmAgFnS	odzbrojit
téměř	téměř	k6eAd1	téměř
kompletně	kompletně	k6eAd1	kompletně
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
slovenské	slovenský	k2eAgFnSc2d1	slovenská
divize	divize	k1gFnSc2	divize
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
zajato	zajmout	k5eAaPmNgNnS	zajmout
asi	asi	k9	asi
22	[number]	k4	22
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
asi	asi	k9	asi
1500	[number]	k4	1500
vojáků	voják	k1gMnPc2	voják
přešlo	přejít	k5eAaPmAgNnS	přejít
do	do	k7c2	do
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
přidalo	přidat	k5eAaPmAgNnS	přidat
k	k	k7c3	k
povstaleckým	povstalecký	k2eAgNnPc3d1	povstalecké
vojskům	vojsko	k1gNnPc3	vojsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Odzbrojením	odzbrojení	k1gNnSc7	odzbrojení
dvou	dva	k4xCgFnPc2	dva
východoslovenských	východoslovenský	k2eAgFnPc2d1	Východoslovenská
divizí	divize	k1gFnPc2	divize
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
získala	získat	k5eAaPmAgFnS	získat
219	[number]	k4	219
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
91	[number]	k4	91
minometů	minomet	k1gInPc2	minomet
<g/>
,130	,130	k4	,130
samopalů	samopal	k1gInPc2	samopal
<g/>
,	,	kIx,	,
30	[number]	k4	30
tanků	tank	k1gInPc2	tank
<g/>
,	,	kIx,	,
20	[number]	k4	20
samohybných	samohybný	k2eAgNnPc2d1	samohybné
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
42	[number]	k4	42
letadel	letadlo	k1gNnPc2	letadlo
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgInPc1d1	důležitý
sklady	sklad	k1gInPc1	sklad
munice	munice	k1gFnSc2	munice
a	a	k8xC	a
dalšího	další	k2eAgInSc2d1	další
vojenského	vojenský	k2eAgInSc2d1	vojenský
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgFnPc4d1	německá
divize	divize	k1gFnPc4	divize
okamžitě	okamžitě	k6eAd1	okamžitě
nasazené	nasazený	k2eAgFnPc4d1	nasazená
na	na	k7c6	na
Dukelském	dukelský	k2eAgInSc6d1	dukelský
průsmyku	průsmyk	k1gInSc6	průsmyk
nemusely	muset	k5eNaImAgFnP	muset
řešit	řešit	k5eAaImF	řešit
otázku	otázka	k1gFnSc4	otázka
zásobování	zásobování	k1gNnSc2	zásobování
dokonce	dokonce	k9	dokonce
ani	ani	k8xC	ani
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přes	přes	k7c4	přes
střední	střední	k2eAgFnSc4d1	střední
povstalecké	povstalecký	k2eAgNnSc1d1	povstalecké
Slovensko	Slovensko	k1gNnSc1	Slovensko
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
využívat	využívat	k5eAaPmF	využívat
silniční	silniční	k2eAgFnPc4d1	silniční
a	a	k8xC	a
železniční	železniční	k2eAgFnPc4d1	železniční
spoje	spoj	k1gFnPc4	spoj
<g/>
.	.	kIx.	.
</s>
<s>
Armádní	armádní	k2eAgInPc1d1	armádní
sklady	sklad	k1gInPc1	sklad
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
je	on	k3xPp3gMnPc4	on
dostatečně	dostatečně	k6eAd1	dostatečně
zásobily	zásobit	k5eAaPmAgInP	zásobit
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
další	další	k2eAgFnSc1d1	další
těžká	těžký	k2eAgFnSc1d1	těžká
rána	rána	k1gFnSc1	rána
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
samotné	samotný	k2eAgNnSc4d1	samotné
povstání	povstání	k1gNnSc4	povstání
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
zdaleka	zdaleka	k6eAd1	zdaleka
nebyl	být	k5eNaImAgInS	být
konec	konec	k1gInSc1	konec
dalších	další	k2eAgFnPc2d1	další
armádních	armádní	k2eAgFnPc2d1	armádní
pohrom	pohroma	k1gFnPc2	pohroma
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
vojska	vojsko	k1gNnSc2	vojsko
získala	získat	k5eAaPmAgFnS	získat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
sklady	sklad	k1gInPc7	sklad
a	a	k8xC	a
výzbrojí	výzbroj	k1gFnSc7	výzbroj
záložní	záložní	k2eAgFnSc1d1	záložní
–	–	k?	–
takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
zápolné	zápolný	k2eAgFnSc2d1	zápolný
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
asi	asi	k9	asi
deset	deset	k4xCc4	deset
tisíc	tisíc	k4xCgInSc4	tisíc
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
jen	jen	k9	jen
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c6	na
povstání	povstání	k1gNnSc6	povstání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Němci	Němec	k1gMnPc1	Němec
získali	získat	k5eAaPmAgMnP	získat
pod	pod	k7c4	pod
kontrolu	kontrola	k1gFnSc4	kontrola
celkem	celkem	k6eAd1	celkem
74	[number]	k4	74
900	[number]	k4	900
pušek	puška	k1gFnPc2	puška
<g/>
,	,	kIx,	,
640	[number]	k4	640
samopalů	samopal	k1gInPc2	samopal
<g/>
,	,	kIx,	,
24	[number]	k4	24
800	[number]	k4	800
pistolí	pistol	k1gFnPc2	pistol
<g/>
,	,	kIx,	,
960	[number]	k4	960
těžkých	těžký	k2eAgFnPc2d1	těžká
a	a	k8xC	a
3800	[number]	k4	3800
lehkých	lehký	k2eAgInPc2d1	lehký
kulometů	kulomet	k1gInPc2	kulomet
<g/>
,	,	kIx,	,
180	[number]	k4	180
minometů	minomet	k1gInPc2	minomet
<g/>
,	,	kIx,	,
80	[number]	k4	80
protiletadlových	protiletadlový	k2eAgNnPc2d1	protiletadlové
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
440	[number]	k4	440
děl	dělo	k1gNnPc2	dělo
různé	různý	k2eAgFnSc2d1	různá
ráže	ráže	k1gFnSc2	ráže
<g/>
,	,	kIx,	,
9	[number]	k4	9
tanků	tank	k1gInPc2	tank
<g/>
,	,	kIx,	,
2	[number]	k4	2
samohybná	samohybný	k2eAgNnPc4d1	samohybné
děla	dělo	k1gNnPc4	dělo
a	a	k8xC	a
23	[number]	k4	23
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
ztráty	ztráta	k1gFnPc1	ztráta
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armáda	armáda	k1gFnSc1	armáda
nemohla	moct	k5eNaImAgFnS	moct
v	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
době	doba	k1gFnSc6	doba
nikdy	nikdy	k6eAd1	nikdy
nahradit	nahradit	k5eAaPmF	nahradit
žádným	žádný	k3yNgInSc7	žádný
leteckým	letecký	k2eAgInSc7d1	letecký
konvojem	konvoj	k1gInSc7	konvoj
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
ani	ani	k8xC	ani
z	z	k7c2	z
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Pzn	Pzn	k1gMnSc1	Pzn
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
zpočátku	zpočátku	k6eAd1	zpočátku
nevěděla	vědět	k5eNaImAgFnS	vědět
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
skutečně	skutečně	k6eAd1	skutečně
bojuje	bojovat	k5eAaImIp3nS	bojovat
pouze	pouze	k6eAd1	pouze
proti	proti	k7c3	proti
fašismu	fašismus	k1gInSc3	fašismus
a	a	k8xC	a
přímé	přímý	k2eAgFnSc3d1	přímá
německé	německý	k2eAgFnSc3d1	německá
okupaci	okupace	k1gFnSc3	okupace
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
chtěla	chtít	k5eAaImAgFnS	chtít
pouze	pouze	k6eAd1	pouze
bojovat	bojovat	k5eAaImF	bojovat
za	za	k7c4	za
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
samostatnost	samostatnost	k1gFnSc4	samostatnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
zastávala	zastávat	k5eAaImAgFnS	zastávat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
složité	složitý	k2eAgFnSc6d1	složitá
politické	politický	k2eAgFnSc6d1	politická
situaci	situace	k1gFnSc6	situace
převážně	převážně	k6eAd1	převážně
neutrální	neutrální	k2eAgInSc4d1	neutrální
postoj	postoj	k1gInSc4	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
vojáci	voják	k1gMnPc1	voják
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
prezidentovi	prezident	k1gMnSc3	prezident
Tisovi	Tisa	k1gMnSc3	Tisa
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc3	jeho
Hlinkovým	Hlinkův	k2eAgFnPc3d1	Hlinkova
gardám	garda	k1gFnPc3	garda
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
důstojníků	důstojník	k1gMnPc2	důstojník
a	a	k8xC	a
vojáků	voják	k1gMnPc2	voják
tak	tak	k6eAd1	tak
v	v	k7c6	v
nepřehledné	přehledný	k2eNgFnSc6d1	nepřehledná
politické	politický	k2eAgFnSc3d1	politická
a	a	k8xC	a
vojenské	vojenský	k2eAgFnSc3d1	vojenská
situaci	situace	k1gFnSc3	situace
ztratila	ztratit	k5eAaPmAgFnS	ztratit
motivaci	motivace	k1gFnSc4	motivace
k	k	k7c3	k
boji	boj	k1gInSc3	boj
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
dezercí	dezerce	k1gFnPc2	dezerce
v	v	k7c6	v
počátku	počátek	k1gInSc6	počátek
povstání	povstání	k1gNnSc2	povstání
narostl	narůst	k5eAaPmAgInS	narůst
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
donutil	donutit	k5eAaPmAgMnS	donutit
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armádu	armáda	k1gFnSc4	armáda
k	k	k7c3	k
nejtvrdším	tvrdý	k2eAgInPc3d3	nejtvrdší
postihům	postih	k1gInPc3	postih
-	-	kIx~	-
trestům	trest	k1gInPc3	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
těžko	těžko	k6eAd1	těžko
popsatelný	popsatelný	k2eAgInSc1d1	popsatelný
zejména	zejména	k9	zejména
politický	politický	k2eAgInSc1d1	politický
zmatek	zmatek	k1gInSc1	zmatek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
nekázeň	nekázeň	k1gFnSc4	nekázeň
a	a	k8xC	a
destrukci	destrukce	k1gFnSc4	destrukce
hlavních	hlavní	k2eAgFnPc2d1	hlavní
povstaleckých	povstalecký	k2eAgFnPc2d1	povstalecká
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odzbrojení	odzbrojení	k1gNnSc3	odzbrojení
bratislavské	bratislavský	k2eAgFnSc2d1	Bratislavská
posádky	posádka	k1gFnSc2	posádka
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
německý	německý	k2eAgInSc1d1	německý
pluk	pluk	k1gInSc1	pluk
Schill	Schilla	k1gFnPc2	Schilla
<g/>
,	,	kIx,	,
speciálně	speciálně	k6eAd1	speciálně
vycvičený	vycvičený	k2eAgInSc1d1	vycvičený
pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
s	s	k7c7	s
partyzány	partyzán	k1gMnPc7	partyzán
<g/>
,	,	kIx,	,
odzbrojil	odzbrojit	k5eAaPmAgMnS	odzbrojit
<g/>
,	,	kIx,	,
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
Josefa	Josefa	k1gFnSc1	Josefa
Tisa	Tisa	k1gFnSc1	Tisa
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
početnější	početní	k2eAgFnSc4d2	početnější
bratislavskou	bratislavský	k2eAgFnSc4d1	Bratislavská
posádku	posádka	k1gFnSc4	posádka
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
SS	SS	kA	SS
byly	být	k5eAaImAgFnP	být
překvapeny	překvapit	k5eAaPmNgFnP	překvapit
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
vrátnice	vrátnice	k1gFnSc2	vrátnice
kasáren	kasárny	k1gFnPc2	kasárny
nebyla	být	k5eNaImAgFnS	být
dokonce	dokonce	k9	dokonce
přítomna	přítomen	k2eAgFnSc1d1	přítomna
strážní	strážní	k2eAgFnSc1d1	strážní
služba	služba	k1gFnSc1	služba
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
vojska	vojsko	k1gNnSc2	vojsko
poklidně	poklidně	k6eAd1	poklidně
pobývala	pobývat	k5eAaImAgFnS	pobývat
v	v	k7c6	v
místních	místní	k2eAgFnPc6d1	místní
pivnicích	pivnice	k1gFnPc6	pivnice
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
vojska	vojsko	k1gNnSc2	vojsko
popíjel	popíjet	k5eAaImAgInS	popíjet
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
kasáren	kasárny	k1gFnPc2	kasárny
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
Golianem	Golian	k1gMnSc7	Golian
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
bojová	bojový	k2eAgFnSc1d1	bojová
pohotovost	pohotovost	k1gFnSc1	pohotovost
již	již	k9	již
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
všem	všecek	k3xTgFnPc3	všecek
posádkám	posádka	k1gFnPc3	posádka
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
ostré	ostrý	k2eAgNnSc1d1	ostré
–	–	k?	–
bojové	bojový	k2eAgNnSc4d1	bojové
střelivo	střelivo	k1gNnSc4	střelivo
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
ochromení	ochromení	k1gNnSc3	ochromení
bojeschopnosti	bojeschopnost	k1gFnSc2	bojeschopnost
povstaleckých	povstalecký	k2eAgNnPc2d1	povstalecké
vojsk	vojsko	k1gNnPc2	vojsko
jako	jako	k8xS	jako
celku	celek	k1gInSc2	celek
<g/>
,	,	kIx,	,
řízeného	řízený	k2eAgNnSc2d1	řízené
centrálními	centrální	k2eAgInPc7d1	centrální
správními	správní	k2eAgInPc7d1	správní
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
měla	mít	k5eAaImAgFnS	mít
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
oddíly	oddíl	k1gInPc1	oddíl
SS	SS	kA	SS
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc1	všechen
posádky	posádka	k1gFnPc1	posádka
západního	západní	k2eAgNnSc2d1	západní
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
nepřipojily	připojit	k5eNaPmAgInP	připojit
k	k	k7c3	k
povstání	povstání	k1gNnSc3	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
velkou	velký	k2eAgFnSc7d1	velká
ztrátou	ztráta	k1gFnSc7	ztráta
byla	být	k5eAaImAgFnS	být
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
velká	velký	k2eAgFnSc1d1	velká
posádka	posádka	k1gFnSc1	posádka
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Meste	Meste	k?	Meste
nad	nad	k7c7	nad
Váhom	Váhom	k1gInSc1	Váhom
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlilo	sídlit	k5eAaImAgNnS	sídlit
hlavní	hlavní	k2eAgNnSc1d1	hlavní
ženijní	ženijní	k2eAgNnSc1d1	ženijní
vojsko	vojsko	k1gNnSc1	vojsko
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
velké	velký	k2eAgFnPc4d1	velká
ztráty	ztráta	k1gFnPc4	ztráta
patřil	patřit	k5eAaImAgMnS	patřit
také	také	k9	také
velký	velký	k2eAgInSc4d1	velký
vojenský	vojenský	k2eAgInSc4d1	vojenský
sklad	sklad	k1gInSc4	sklad
v	v	k7c6	v
Kvetnici	Kvetnice	k1gFnSc6	Kvetnice
u	u	k7c2	u
Popradu	Poprad	k1gInSc2	Poprad
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
40	[number]	k4	40
000	[number]	k4	000
nábojů	náboj	k1gInPc2	náboj
<g/>
,	,	kIx,	,
10	[number]	k4	10
000	[number]	k4	000
ručních	ruční	k2eAgInPc2d1	ruční
granátů	granát	k1gInPc2	granát
<g/>
,	,	kIx,	,
181	[number]	k4	181
těžkých	těžký	k2eAgInPc2d1	těžký
kulometů	kulomet	k1gInPc2	kulomet
<g/>
,	,	kIx,	,
630	[number]	k4	630
lehkých	lehký	k2eAgInPc2d1	lehký
kulometů	kulomet	k1gInPc2	kulomet
<g/>
,	,	kIx,	,
370	[number]	k4	370
pušek	puška	k1gFnPc2	puška
a	a	k8xC	a
5776	[number]	k4	5776
pistolí	pistol	k1gFnPc2	pistol
<g/>
.	.	kIx.	.
</s>
<s>
Následky	následek	k1gInPc1	následek
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
velitelského	velitelský	k2eAgNnSc2d1	velitelské
fiaska	fiasko	k1gNnSc2	fiasko
důstojníků	důstojník	k1gMnPc2	důstojník
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
sídlících	sídlící	k2eAgInPc2d1	sídlící
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
tragické	tragický	k2eAgInPc1d1	tragický
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
uvedených	uvedený	k2eAgFnPc2d1	uvedená
katastrof	katastrofa	k1gFnPc2	katastrofa
mělo	mít	k5eAaImAgNnS	mít
velení	velení	k1gNnSc1	velení
1	[number]	k4	1
<g/>
.	.	kIx.	.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
počátkem	počátkem	k7c2	počátkem
září	září	k1gNnSc2	září
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
jen	jen	k9	jen
16	[number]	k4	16
praporů	prapor	k1gInPc2	prapor
a	a	k8xC	a
8	[number]	k4	8
samostatných	samostatný	k2eAgFnPc2d1	samostatná
rot	rota	k1gFnPc2	rota
pěchoty	pěchota	k1gFnSc2	pěchota
!	!	kIx.	!
</s>
<s>
Dále	daleko	k6eAd2	daleko
asi	asi	k9	asi
200	[number]	k4	200
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
minometů	minomet	k1gInPc2	minomet
<g/>
,	,	kIx,	,
24	[number]	k4	24
lehkých	lehký	k2eAgInPc2d1	lehký
tanků	tank	k1gInPc2	tank
<g/>
,	,	kIx,	,
jen	jen	k9	jen
13	[number]	k4	13
letadel	letadlo	k1gNnPc2	letadlo
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
2	[number]	k4	2
samohybná	samohybný	k2eAgFnSc1d1	samohybná
děla	dít	k5eAaBmAgFnS	dít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
Pzn	Pzn	k1gFnPc6	Pzn
<g/>
.	.	kIx.	.
</s>
<s>
Selhání	selhání	k1gNnSc4	selhání
velitelů	velitel	k1gMnPc2	velitel
vojenských	vojenský	k2eAgFnPc2d1	vojenská
posádek	posádka	k1gFnPc2	posádka
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
dodnes	dodnes	k6eAd1	dodnes
plně	plně	k6eAd1	plně
neobjasněno	objasněn	k2eNgNnSc1d1	neobjasněno
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
útvary	útvar	k1gInPc1	útvar
byly	být	k5eAaImAgInP	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
na	na	k7c6	na
povstání	povstání	k1gNnSc6	povstání
politicky	politicky	k6eAd1	politicky
i	i	k8xC	i
vojensky	vojensky	k6eAd1	vojensky
připravovány	připravován	k2eAgFnPc1d1	připravována
instrukcemi	instrukce	k1gFnPc7	instrukce
SNR	SNR	kA	SNR
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
velitelů	velitel	k1gMnPc2	velitel
posádek	posádka	k1gFnPc2	posádka
nevydala	vydat	k5eNaPmAgFnS	vydat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
prvních	první	k4xOgInPc2	první
dnů	den	k1gInPc2	den
svým	svůj	k3xOyFgInSc7	svůj
jednotkám	jednotka	k1gFnPc3	jednotka
žádné	žádný	k3yNgInPc4	žádný
rozkazy	rozkaz	k1gInPc4	rozkaz
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
materiálně	materiálně	k6eAd1	materiálně
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
posílila	posílit	k5eAaPmAgFnS	posílit
nepřítele	nepřítel	k1gMnSc4	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Posádky	posádka	k1gFnPc4	posádka
zejména	zejména	k9	zejména
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
Slovensku	Slovensko	k1gNnSc6	Slovensko
nedokázaly	dokázat	k5eNaPmAgFnP	dokázat
vystěhovat	vystěhovat	k5eAaPmF	vystěhovat
nejen	nejen	k6eAd1	nejen
svoje	svůj	k3xOyFgFnPc4	svůj
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
další	další	k2eAgInSc4d1	další
vojenský	vojenský	k2eAgInSc4d1	vojenský
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
povstalecké	povstalecký	k2eAgFnSc2d1	povstalecká
jednotky	jednotka	k1gFnPc1	jednotka
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
útvarů	útvar	k1gInPc2	útvar
často	často	k6eAd1	často
marně	marně	k6eAd1	marně
čekaly	čekat	k5eAaImAgFnP	čekat
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc4	průběh
SNP	SNP	kA	SNP
==	==	k?	==
</s>
</p>
<p>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tento	tento	k3xDgInSc4	tento
obrovský	obrovský	k2eAgInSc4d1	obrovský
krach	krach	k1gInSc4	krach
hlavních	hlavní	k2eAgFnPc2d1	hlavní
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
struktury	struktura	k1gFnSc2	struktura
slovenské	slovenský	k2eAgFnSc2d1	slovenská
armády	armáda	k1gFnSc2	armáda
se	se	k3xPyFc4	se
vojenské	vojenský	k2eAgNnSc1d1	vojenské
povstání	povstání	k1gNnSc1	povstání
rychle	rychle	k6eAd1	rychle
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
zejména	zejména	k9	zejména
na	na	k7c6	na
středním	střední	k2eAgNnSc6d1	střední
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Zasáhlo	zasáhnout	k5eAaPmAgNnS	zasáhnout
30	[number]	k4	30
okresů	okres	k1gInPc2	okres
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
asi	asi	k9	asi
20	[number]	k4	20
000	[number]	k4	000
km	km	kA	km
<g/>
2	[number]	k4	2
s	s	k7c7	s
přibližně	přibližně	k6eAd1	přibližně
1,7	[number]	k4	1,7
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
politickou	politický	k2eAgFnSc4d1	politická
moc	moc	k1gFnSc4	moc
přebíraly	přebírat	k5eAaImAgInP	přebírat
nové	nový	k2eAgInPc1d1	nový
orgány	orgán	k1gInPc1	orgán
–	–	k?	–
národní	národní	k2eAgInPc1d1	národní
výbory	výbor	k1gInPc1	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
složkou	složka	k1gFnSc7	složka
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
povstání	povstání	k1gNnSc2	povstání
byly	být	k5eAaImAgFnP	být
zbytky	zbytek	k1gInPc4	zbytek
výzbroje	výzbroj	k1gFnSc2	výzbroj
1	[number]	k4	1
<g/>
.	.	kIx.	.
československé	československý	k2eAgFnSc2d1	Československá
armády	armáda	k1gFnSc2	armáda
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
partyzány	partyzán	k1gMnPc7	partyzán
<g/>
,	,	kIx,	,
částí	část	k1gFnSc7	část
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
finanční	finanční	k2eAgFnSc2d1	finanční
stráže	stráž	k1gFnSc2	stráž
a	a	k8xC	a
milicemi	milice	k1gFnPc7	milice
Národní	národní	k2eAgFnSc2d1	národní
stráže	stráž	k1gFnSc2	stráž
bránila	bránit	k5eAaImAgFnS	bránit
tato	tento	k3xDgFnSc1	tento
armáda	armáda	k1gFnSc1	armáda
povstalecké	povstalecký	k2eAgNnSc4d1	povstalecké
území	území	k1gNnSc4	území
na	na	k7c6	na
souvislé	souvislý	k2eAgFnSc6d1	souvislá
frontě	fronta	k1gFnSc6	fronta
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Exilová	exilový	k2eAgFnSc1d1	exilová
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
1	[number]	k4	1
<g/>
.	.	kIx.	.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armádu	armáda	k1gFnSc4	armáda
za	za	k7c4	za
součást	součást	k1gFnSc4	součást
čs	čs	kA	čs
<g/>
.	.	kIx.	.
branné	branný	k2eAgFnSc2d1	Branná
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
za	za	k7c4	za
spojeneckou	spojenecký	k2eAgFnSc4d1	spojenecká
ji	on	k3xPp3gFnSc4	on
uznaly	uznat	k5eAaPmAgFnP	uznat
i	i	k8xC	i
velmoci	velmoc	k1gFnPc1	velmoc
protihitlerovské	protihitlerovský	k2eAgFnSc2d1	protihitlerovská
koalice	koalice	k1gFnSc2	koalice
–	–	k?	–
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
USA	USA	kA	USA
a	a	k8xC	a
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
postupně	postupně	k6eAd1	postupně
vyslaly	vyslat	k5eAaPmAgInP	vyslat
své	svůj	k3xOyFgFnPc4	svůj
styčné	styčný	k2eAgFnPc4d1	styčná
vojenské	vojenský	k2eAgFnPc4d1	vojenská
mise	mise	k1gFnPc4	mise
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
povstání	povstání	k1gNnSc2	povstání
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
posádek	posádka	k1gFnPc2	posádka
v	v	k7c6	v
Trnavě	Trnava	k1gFnSc6	Trnava
<g/>
,	,	kIx,	,
Piešťanech	Piešťany	k1gInPc6	Piešťany
a	a	k8xC	a
části	část	k1gFnSc6	část
posádky	posádka	k1gFnSc2	posádka
v	v	k7c6	v
Topolčanech	Topolčan	k1gMnPc6	Topolčan
<g/>
,	,	kIx,	,
nezapojily	zapojit	k5eNaPmAgInP	zapojit
žádné	žádný	k3yNgInPc1	žádný
útvary	útvar	k1gInPc1	útvar
ze	z	k7c2	z
západního	západní	k2eAgNnSc2d1	západní
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
ztrátu	ztráta	k1gFnSc4	ztráta
však	však	k9	však
znamenalo	znamenat	k5eAaImAgNnS	znamenat
nečekané	čekaný	k2eNgNnSc4d1	nečekané
odzbrojení	odzbrojení	k1gNnSc4	odzbrojení
východoslovenské	východoslovenský	k2eAgFnSc2d1	Východoslovenská
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
bylo	být	k5eAaImAgNnS	být
znemožněno	znemožněn	k2eAgNnSc4d1	znemožněno
rychlé	rychlý	k2eAgNnSc4d1	rychlé
spojení	spojení	k1gNnSc4	spojení
povstání	povstání	k1gNnSc2	povstání
s	s	k7c7	s
jednotkami	jednotka	k1gFnPc7	jednotka
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
operujícími	operující	k2eAgInPc7d1	operující
u	u	k7c2	u
slovenských	slovenský	k2eAgFnPc2d1	slovenská
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
18	[number]	k4	18
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
důstojníků	důstojník	k1gMnPc2	důstojník
1	[number]	k4	1
<g/>
.	.	kIx.	.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
po	po	k7c6	po
mobilizaci	mobilizace	k1gFnSc6	mobilizace
z	z	k7c2	z
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
na	na	k7c4	na
47	[number]	k4	47
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Mobilizací	mobilizace	k1gFnSc7	mobilizace
mužů	muž	k1gMnPc2	muž
do	do	k7c2	do
40	[number]	k4	40
let	léto	k1gNnPc2	léto
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
a	a	k8xC	a
nástupem	nástup	k1gInSc7	nástup
branců	branec	k1gMnPc2	branec
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
se	se	k3xPyFc4	se
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
asi	asi	k9	asi
na	na	k7c4	na
65	[number]	k4	65
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
kontrolované	kontrolovaný	k2eAgNnSc1d1	kontrolované
povstalci	povstalec	k1gMnPc7	povstalec
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
září	zářit	k5eAaImIp3nS	zářit
1944	[number]	k4	1944
ohraničeno	ohraničit	k5eAaPmNgNnS	ohraničit
na	na	k7c6	na
východě	východ	k1gInSc6	východ
linií	linie	k1gFnPc2	linie
Levoča	Levoča	k1gFnSc1	Levoča
–	–	k?	–
Spišská	spišský	k2eAgFnSc1d1	Spišská
Nová	nový	k2eAgFnSc1d1	nová
Ves	ves	k1gFnSc1	ves
–	–	k?	–
Dobšiná	Dobšiná	k1gFnSc1	Dobšiná
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
linií	linie	k1gFnPc2	linie
Žilina	Žilina	k1gFnSc1	Žilina
–	–	k?	–
Bánovce	Bánovec	k1gInPc1	Bánovec
nad	nad	k7c7	nad
Bebravou	Bebrava	k1gFnSc7	Bebrava
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
dosahovalo	dosahovat	k5eAaImAgNnS	dosahovat
hranic	hranice	k1gFnPc2	hranice
Slovenského	slovenský	k2eAgInSc2d1	slovenský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
boje	boj	k1gInPc1	boj
vzplály	vzplát	k5eAaPmAgInP	vzplát
již	již	k6eAd1	již
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
povstání	povstání	k1gNnSc2	povstání
na	na	k7c6	na
přístupech	přístup	k1gInPc6	přístup
k	k	k7c3	k
Žilině	Žilina	k1gFnSc3	Žilina
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
boje	boj	k1gInPc1	boj
probíhaly	probíhat	k5eAaImAgInP	probíhat
v	v	k7c6	v
Popradu	Poprad	k1gInSc6	Poprad
a	a	k8xC	a
Kežmarku	Kežmarok	k1gInSc6	Kežmarok
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
silná	silný	k2eAgFnSc1d1	silná
německá	německý	k2eAgFnSc1d1	německá
menšina	menšina	k1gFnSc1	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdé	Tvrdé	k2eAgInPc1d1	Tvrdé
boje	boj	k1gInPc1	boj
probíhaly	probíhat	k5eAaImAgInP	probíhat
o	o	k7c4	o
Telgárt	Telgárt	k1gInSc4	Telgárt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsadily	obsadit	k5eAaPmAgFnP	obsadit
německé	německý	k2eAgFnPc1d1	německá
jednotky	jednotka	k1gFnPc1	jednotka
již	již	k6eAd1	již
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
dobyt	dobýt	k5eAaPmNgInS	dobýt
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
však	však	k9	však
začali	začít	k5eAaPmAgMnP	začít
útočit	útočit	k5eAaImF	útočit
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Liptov	Liptov	k1gInSc4	Liptov
<g/>
,	,	kIx,	,
Štrbu	Štrba	k1gFnSc4	Štrba
a	a	k8xC	a
Važec	Važec	k1gInSc4	Važec
a	a	k8xC	a
po	po	k7c6	po
obejítí	obejítí	k1gNnSc6	obejítí
obrany	obrana	k1gFnSc2	obrana
u	u	k7c2	u
Liptovského	liptovský	k2eAgInSc2d1	liptovský
Hrádku	Hrádok	k1gInSc2	Hrádok
obsadili	obsadit	k5eAaPmAgMnP	obsadit
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
Liptovský	liptovský	k2eAgMnSc1d1	liptovský
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
<g/>
.	.	kIx.	.
</s>
<s>
Obrana	obrana	k1gFnSc1	obrana
povstalců	povstalec	k1gMnPc2	povstalec
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
stadiu	stadion	k1gNnSc6	stadion
omezována	omezován	k2eAgFnSc1d1	omezována
příkazem	příkaz	k1gInSc7	příkaz
londýnské	londýnský	k2eAgFnSc2d1	londýnská
československé	československý	k2eAgFnSc2d1	Československá
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
doporučoval	doporučovat	k5eAaImAgMnS	doporučovat
soustředit	soustředit	k5eAaPmF	soustředit
síly	síla	k1gFnPc4	síla
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
středního	střední	k2eAgNnSc2d1	střední
Slovenska	Slovensko	k1gNnSc2	Slovensko
zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Zvolen	Zvolen	k1gInSc1	Zvolen
<g/>
,	,	kIx,	,
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
a	a	k8xC	a
Brezno	Brezna	k1gFnSc5	Brezna
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
bránit	bránit	k5eAaImF	bránit
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
bylo	být	k5eAaImAgNnS	být
nad	nad	k7c4	nad
síly	síla	k1gFnPc4	síla
1	[number]	k4	1
<g/>
.	.	kIx.	.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Němcům	Němec	k1gMnPc3	Němec
se	se	k3xPyFc4	se
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
podařilo	podařit	k5eAaPmAgNnS	podařit
prorazit	prorazit	k5eAaPmF	prorazit
obranou	obrana	k1gFnSc7	obrana
u	u	k7c2	u
Strečna	Strečno	k1gNnSc2	Strečno
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Váhu	Váh	k1gInSc2	Váh
a	a	k8xC	a
postupovat	postupovat	k5eAaImF	postupovat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Vrútkám	Vrútka	k1gFnPc3	Vrútka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
obsadit	obsadit	k5eAaPmF	obsadit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
těžké	těžký	k2eAgInPc1d1	těžký
boje	boj	k1gInPc1	boj
probíhaly	probíhat	k5eAaImAgInP	probíhat
o	o	k7c4	o
Priekopu	Priekopa	k1gFnSc4	Priekopa
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
Sučany	Sučan	k1gMnPc4	Sučan
<g/>
,	,	kIx,	,
Baťovany	Baťovan	k1gMnPc4	Baťovan
<g/>
,	,	kIx,	,
Telgárt	Telgárt	k1gInSc1	Telgárt
a	a	k8xC	a
Čremošné	Čremošný	k2eAgNnSc1d1	Čremošné
<g/>
,	,	kIx,	,
u	u	k7c2	u
Jalné	Jalná	k1gFnSc2	Jalná
<g/>
,	,	kIx,	,
u	u	k7c2	u
vrchu	vrch	k1gInSc2	vrch
Ostrô	Ostrô	k1gFnSc2	Ostrô
a	a	k8xC	a
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Povstalci	povstalec	k1gMnPc1	povstalec
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
povstání	povstání	k1gNnSc2	povstání
disponovali	disponovat	k5eAaBmAgMnP	disponovat
čs	čs	kA	čs
<g/>
.	.	kIx.	.
tanky	tank	k1gInPc1	tank
LT-	LT-	k1gFnSc1	LT-
<g/>
35	[number]	k4	35
<g/>
,	,	kIx,	,
LT-38	LT-38	k1gFnSc1	LT-38
a	a	k8xC	a
LT-	LT-	k1gFnSc1	LT-
<g/>
40	[number]	k4	40
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
německými	německý	k2eAgFnPc7d1	německá
Pzkpfw	Pzkpfw	k1gFnPc7	Pzkpfw
III	III	kA	III
N	N	kA	N
a	a	k8xC	a
stíhači	stíhač	k1gMnPc1	stíhač
tanků	tank	k1gInPc2	tank
Marder	Mardero	k1gNnPc2	Mardero
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
měli	mít	k5eAaImAgMnP	mít
také	také	k6eAd1	také
ještě	ještě	k6eAd1	ještě
dělostřelectvo	dělostřelectvo	k1gNnSc1	dělostřelectvo
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jejich	jejich	k3xOp3gNnSc1	jejich
letectvo	letectvo	k1gNnSc1	letectvo
<g/>
,	,	kIx,	,
sestávající	sestávající	k2eAgMnSc1d1	sestávající
ze	z	k7c2	z
zastaralých	zastaralý	k2eAgInPc2d1	zastaralý
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
nedokázalo	dokázat	k5eNaPmAgNnS	dokázat
Němcům	Němec	k1gMnPc3	Němec
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
konkurovat	konkurovat	k5eAaImF	konkurovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
přesunu	přesun	k1gInSc6	přesun
1	[number]	k4	1
<g/>
.	.	kIx.	.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
stíhacího	stíhací	k2eAgInSc2d1	stíhací
leteckého	letecký	k2eAgInSc2d1	letecký
pluku	pluk	k1gInSc2	pluk
na	na	k7c4	na
letiště	letiště	k1gNnSc4	letiště
Zolná	Zolná	k1gFnSc1	Zolná
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
den	den	k1gInSc1	den
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
příletu	přílet	k1gInSc6	přílet
<g/>
,	,	kIx,	,
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
,	,	kIx,	,
náletem	nálet	k1gInSc7	nálet
na	na	k7c4	na
piešťanské	piešťanský	k2eAgNnSc4d1	Piešťanské
letiště	letiště	k1gNnPc4	letiště
paralyzoval	paralyzovat	k5eAaBmAgInS	paralyzovat
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
německé	německý	k2eAgNnSc1d1	německé
letectvo	letectvo	k1gNnSc1	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1944	[number]	k4	1944
začaly	začít	k5eAaPmAgInP	začít
být	být	k5eAaImF	být
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
SNP	SNP	kA	SNP
letecky	letecky	k6eAd1	letecky
dopravovány	dopravován	k2eAgFnPc4d1	dopravována
jednotky	jednotka	k1gFnPc4	jednotka
2	[number]	k4	2
<g/>
.	.	kIx.	.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
samostatné	samostatný	k2eAgFnSc2d1	samostatná
paradesantní	paradesantní	k2eAgFnSc2d1	paradesantní
brigády	brigáda	k1gFnSc2	brigáda
<g/>
.	.	kIx.	.
</s>
<s>
Povstalci	povstalec	k1gMnPc1	povstalec
již	již	k6eAd1	již
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
trpěli	trpět	k5eAaImAgMnP	trpět
přitom	přitom	k6eAd1	přitom
vážným	vážný	k2eAgInSc7d1	vážný
nedostatkem	nedostatek	k1gInSc7	nedostatek
těžkých	těžký	k2eAgFnPc2d1	těžká
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
zmírňovaly	zmírňovat	k5eAaImAgFnP	zmírňovat
letecké	letecký	k2eAgFnPc1d1	letecká
dodávky	dodávka	k1gFnPc1	dodávka
od	od	k7c2	od
Sovětů	Sovět	k1gMnPc2	Sovět
a	a	k8xC	a
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
této	tento	k3xDgFnSc2	tento
pomoci	pomoc	k1gFnSc2	pomoc
by	by	kYmCp3nS	by
povstalecká	povstalecký	k2eAgFnSc1d1	povstalecká
vojska	vojsko	k1gNnPc4	vojsko
nebyla	být	k5eNaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
takového	takový	k3xDgInSc2	takový
aktivního	aktivní	k2eAgInSc2d1	aktivní
boje	boj	k1gInSc2	boj
a	a	k8xC	a
dlouhého	dlouhý	k2eAgInSc2d1	dlouhý
vzdoru	vzdor	k1gInSc2	vzdor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejúčinnější	účinný	k2eAgFnSc7d3	nejúčinnější
pomocí	pomoc	k1gFnSc7	pomoc
povstání	povstání	k1gNnSc2	povstání
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
stát	stát	k5eAaPmF	stát
Karpatsko-dukelská	karpatskoukelský	k2eAgFnSc1d1	karpatsko-dukelská
operace	operace	k1gFnSc1	operace
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
počáteční	počáteční	k2eAgFnSc6d1	počáteční
fázi	fáze	k1gFnSc6	fáze
povstání	povstání	k1gNnSc2	povstání
nepodařilo	podařit	k5eNaPmAgNnS	podařit
východoslovenským	východoslovenský	k2eAgFnPc3d1	Východoslovenská
divizím	divize	k1gFnPc3	divize
obsadit	obsadit	k5eAaPmF	obsadit
pohraniční	pohraniční	k2eAgInPc4d1	pohraniční
průsmyky	průsmyk	k1gInPc4	průsmyk
<g/>
,	,	kIx,	,
neměla	mít	k5eNaImAgFnS	mít
tato	tento	k3xDgFnSc1	tento
operace	operace	k1gFnSc1	operace
na	na	k7c4	na
boje	boj	k1gInPc4	boj
povstalců	povstalec	k1gMnPc2	povstalec
žádný	žádný	k3yNgInSc4	žádný
přímý	přímý	k2eAgInSc4d1	přímý
aktivní	aktivní	k2eAgInSc4d1	aktivní
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
však	však	k9	však
Němcům	Němec	k1gMnPc3	Němec
vázala	vázat	k5eAaImAgNnP	vázat
vojska	vojsko	k1gNnPc1	vojsko
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nemohla	moct	k5eNaImAgFnS	moct
být	být	k5eAaImF	být
použita	použit	k2eAgFnSc1d1	použita
proti	proti	k7c3	proti
nim	on	k3xPp3gFnPc3	on
a	a	k8xC	a
také	také	k9	také
na	na	k7c6	na
jiných	jiný	k2eAgInPc6d1	jiný
úsecích	úsek	k1gInPc6	úsek
fronty	fronta	k1gFnSc2	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
podpory	podpora	k1gFnSc2	podpora
povstání	povstání	k1gNnSc2	povstání
se	s	k7c7	s
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
zapojilo	zapojit	k5eAaPmAgNnS	zapojit
i	i	k9	i
150	[number]	k4	150
amerických	americký	k2eAgFnPc2d1	americká
létajících	létající	k2eAgFnPc2d1	létající
pevností	pevnost	k1gFnPc2	pevnost
B-	B-	k1gMnPc2	B-
<g/>
17	[number]	k4	17
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
zlikvidovaly	zlikvidovat	k5eAaPmAgInP	zlikvidovat
německé	německý	k2eAgInPc1d1	německý
hangáry	hangár	k1gInPc1	hangár
<g/>
,	,	kIx,	,
dílny	dílna	k1gFnPc1	dílna
a	a	k8xC	a
letouny	letoun	k1gInPc1	letoun
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
Nový	nový	k2eAgInSc1d1	nový
Dvor	Dvor	k1gInSc1	Dvor
u	u	k7c2	u
Malacek	Malacky	k1gFnPc2	Malacky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
září	září	k1gNnSc2	září
probíhaly	probíhat	k5eAaImAgInP	probíhat
těžké	těžký	k2eAgInPc1d1	těžký
boje	boj	k1gInPc1	boj
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
přístupech	přístup	k1gInPc6	přístup
k	k	k7c3	k
centru	centr	k1gInSc3	centr
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
posílení	posílení	k1gNnSc1	posílení
o	o	k7c4	o
jednotky	jednotka	k1gFnPc4	jednotka
Waffen-SS	Waffen-SS	k1gMnPc2	Waffen-SS
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
dostat	dostat	k5eAaPmF	dostat
k	k	k7c3	k
Banské	banský	k2eAgFnSc3d1	Banská
Bystrici	Bystrica	k1gFnSc3	Bystrica
a	a	k8xC	a
Zvolenu	Zvolen	k1gInSc3	Zvolen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
povstaleckého	povstalecký	k2eAgNnSc2d1	povstalecké
území	území	k1gNnSc2	území
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
podařilo	podařit	k5eAaPmAgNnS	podařit
1	[number]	k4	1
<g/>
.	.	kIx.	.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armádě	armáda	k1gFnSc3	armáda
provést	provést	k5eAaPmF	provést
útok	útok	k1gInSc4	útok
a	a	k8xC	a
donutit	donutit	k5eAaPmF	donutit
Němce	Němec	k1gMnSc4	Němec
k	k	k7c3	k
ústupu	ústup	k1gInSc3	ústup
až	až	k9	až
k	k	k7c3	k
Popradu	Poprad	k1gInSc3	Poprad
<g/>
.	.	kIx.	.
</s>
<s>
Povstalcům	povstalec	k1gMnPc3	povstalec
se	se	k3xPyFc4	se
též	též	k9	též
podařilo	podařit	k5eAaPmAgNnS	podařit
postavit	postavit	k5eAaPmF	postavit
tři	tři	k4xCgInPc1	tři
obrněné	obrněný	k2eAgInPc1d1	obrněný
vlaky	vlak	k1gInPc1	vlak
"	"	kIx"	"
<g/>
Štefánik	Štefánik	k1gInSc1	Štefánik
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Hurban	Hurban	k1gInSc1	Hurban
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Masaryk	Masaryk	k1gMnSc1	Masaryk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jim	on	k3xPp3gMnPc3	on
poskytovaly	poskytovat	k5eAaImAgInP	poskytovat
silnou	silný	k2eAgFnSc4d1	silná
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
povstání	povstání	k1gNnSc2	povstání
neprobíhaly	probíhat	k5eNaImAgFnP	probíhat
jen	jen	k9	jen
boje	boj	k1gInPc4	boj
v	v	k7c6	v
souvislém	souvislý	k2eAgNnSc6d1	souvislé
povstaleckém	povstalecký	k2eAgNnSc6d1	povstalecké
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vně	vně	k6eAd1	vně
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
neustále	neustále	k6eAd1	neustále
útočily	útočit	k5eAaImAgFnP	útočit
partyzánské	partyzánský	k2eAgFnPc1d1	Partyzánská
skupiny	skupina	k1gFnPc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Partyzáni	partyzán	k1gMnPc1	partyzán
např.	např.	kA	např.
neustále	neustále	k6eAd1	neustále
útočili	útočit	k5eAaImAgMnP	útočit
na	na	k7c4	na
důležitou	důležitý	k2eAgFnSc4d1	důležitá
železniční	železniční	k2eAgFnSc4d1	železniční
trať	trať	k1gFnSc4	trať
Košice	Košice	k1gInPc4	Košice
<g/>
–	–	k?	–
<g/>
Žilina	Žilina	k1gFnSc1	Žilina
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Košicko-bohumínské	košickoohumínský	k2eAgFnSc2d1	košicko-bohumínská
dráhy	dráha	k1gFnSc2	dráha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
i	i	k9	i
po	po	k7c6	po
zprovoznění	zprovoznění	k1gNnSc6	zprovoznění
stále	stále	k6eAd1	stále
přerušována	přerušován	k2eAgFnSc1d1	přerušována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Problémem	problém	k1gInSc7	problém
povstání	povstání	k1gNnSc2	povstání
byla	být	k5eAaImAgFnS	být
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
koordinace	koordinace	k1gFnSc1	koordinace
mezi	mezi	k7c7	mezi
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
partyzány	partyzán	k1gMnPc7	partyzán
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
zbytečným	zbytečný	k2eAgFnPc3d1	zbytečná
ztrátám	ztráta	k1gFnPc3	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Partyzáni	partyzán	k1gMnPc1	partyzán
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c6	na
povstaleckém	povstalecký	k2eAgNnSc6d1	povstalecké
území	území	k1gNnSc6	území
dopouštěli	dopouštět	k5eAaImAgMnP	dopouštět
násilí	násilí	k1gNnSc4	násilí
na	na	k7c6	na
civilním	civilní	k2eAgNnSc6d1	civilní
obyvatelstvu	obyvatelstvo	k1gNnSc6	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
zločinů	zločin	k1gInPc2	zločin
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
zločiny	zločin	k1gInPc4	zločin
proti	proti	k7c3	proti
lidskosti	lidskost	k1gFnSc3	lidskost
<g/>
.	.	kIx.	.
</s>
<s>
Partyzáni	partyzán	k1gMnPc1	partyzán
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
čs	čs	kA	čs
partyzánské	partyzánský	k2eAgFnSc2d1	Partyzánská
brigády	brigáda	k1gFnSc2	brigáda
M.	M.	kA	M.
R.	R.	kA	R.
Štefánika	Štefánik	k1gMnSc2	Štefánik
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
poblíž	poblíž	k7c2	poblíž
Zvolena	Zvolen	k1gInSc2	Zvolen
zavraždili	zavraždit	k5eAaPmAgMnP	zavraždit
jedenáct	jedenáct	k4xCc4	jedenáct
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
katolického	katolický	k2eAgMnSc2d1	katolický
kněze	kněz	k1gMnSc2	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
pachatelé	pachatel	k1gMnPc1	pachatel
byli	být	k5eAaImAgMnP	být
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
zločin	zločin	k1gInSc4	zločin
povstaleckými	povstalecký	k2eAgFnPc7d1	povstalecká
orgány	orgán	k1gInPc7	orgán
popraveni	popravit	k5eAaPmNgMnP	popravit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejhoršímu	zlý	k2eAgInSc3d3	Nejhorší
případu	případ	k1gInSc3	případ
došlo	dojít	k5eAaPmAgNnS	dojít
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Sklené	sklený	k2eAgFnSc2d1	sklená
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nP	zářit
příslušníci	příslušník	k1gMnPc1	příslušník
1	[number]	k4	1
<g/>
.	.	kIx.	.
čs	čs	kA	čs
partyzánské	partyzánský	k2eAgFnSc2d1	Partyzánská
brigády	brigáda	k1gFnSc2	brigáda
J.	J.	kA	J.
V.	V.	kA	V.
Stalina	Stalin	k1gMnSc2	Stalin
postříleli	postřílet	k5eAaPmAgMnP	postřílet
187	[number]	k4	187
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
německé	německý	k2eAgFnSc2d1	německá
národnosti	národnost	k1gFnSc2	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zahájeno	zahájit	k5eAaPmNgNnS	zahájit
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
potrestání	potrestání	k1gNnSc3	potrestání
pachatelů	pachatel	k1gMnPc2	pachatel
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
případy	případ	k1gInPc1	případ
také	také	k9	také
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
povstání	povstání	k1gNnSc2	povstání
posloužily	posloužit	k5eAaPmAgFnP	posloužit
jako	jako	k9	jako
záminka	záminka	k1gFnSc1	záminka
pro	pro	k7c4	pro
německé	německý	k2eAgFnPc4d1	německá
represálie	represálie	k1gFnPc4	represálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Německá	německý	k2eAgFnSc1d1	německá
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
==	==	k?	==
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
situace	situace	k1gFnSc2	situace
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
divize	divize	k1gFnSc1	divize
"	"	kIx"	"
<g/>
Tatra	Tatra	k1gFnSc1	Tatra
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
posílena	posílit	k5eAaPmNgFnS	posílit
o	o	k7c4	o
těžkou	těžký	k2eAgFnSc4d1	těžká
bojovou	bojový	k2eAgFnSc4d1	bojová
techniku	technika	k1gFnSc4	technika
a	a	k8xC	a
podnikla	podniknout	k5eAaPmAgFnS	podniknout
nové	nový	k2eAgInPc4d1	nový
útoky	útok	k1gInPc4	útok
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
skončily	skončit	k5eAaPmAgInP	skončit
neúspěchem	neúspěch	k1gInSc7	neúspěch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
poradě	porada	k1gFnSc6	porada
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
Heinrich	Heinrich	k1gMnSc1	Heinrich
Himmler	Himmler	k1gMnSc1	Himmler
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
s	s	k7c7	s
dosavadním	dosavadní	k2eAgInSc7d1	dosavadní
průběhem	průběh	k1gInSc7	průběh
bojových	bojový	k2eAgFnPc2d1	bojová
akcí	akce	k1gFnPc2	akce
proti	proti	k7c3	proti
povstalcům	povstalec	k1gMnPc3	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
potlačení	potlačení	k1gNnSc1	potlačení
povstání	povstání	k1gNnSc2	povstání
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
vyslána	vyslat	k5eAaPmNgFnS	vyslat
SS-Sturmbrigade	SS-Sturmbrigad	k1gInSc5	SS-Sturmbrigad
"	"	kIx"	"
<g/>
Dirlewanger	Dirlewangra	k1gFnPc2	Dirlewangra
<g/>
"	"	kIx"	"
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc1	divize
pancéřových	pancéřový	k2eAgMnPc2d1	pancéřový
granátníků	granátník	k1gMnPc2	granátník
SS	SS	kA	SS
"	"	kIx"	"
<g/>
Horst	Horst	k1gMnSc1	Horst
Wessel	Wessel	k1gMnSc1	Wessel
<g/>
"	"	kIx"	"
z	z	k7c2	z
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
značná	značný	k2eAgFnSc1d1	značná
bojová	bojový	k2eAgFnSc1d1	bojová
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
straně	strana	k1gFnSc6	strana
povstalců	povstalec	k1gMnPc2	povstalec
stálo	stát	k5eAaImAgNnS	stát
18	[number]	k4	18
pěších	pěší	k2eAgInPc2d1	pěší
praporů	prapor	k1gInPc2	prapor
<g/>
,	,	kIx,	,
27	[number]	k4	27
dělostřeleckých	dělostřelecký	k2eAgFnPc2d1	dělostřelecká
baterií	baterie	k1gFnPc2	baterie
<g/>
,	,	kIx,	,
3	[number]	k4	3
obrněné	obrněný	k2eAgInPc1d1	obrněný
vlaky	vlak	k1gInPc1	vlak
<g/>
,	,	kIx,	,
10	[number]	k4	10
–	–	k?	–
12	[number]	k4	12
lehkých	lehký	k2eAgInPc2d1	lehký
tanků	tank	k1gInPc2	tank
a	a	k8xC	a
20	[number]	k4	20
letadel	letadlo	k1gNnPc2	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Pěchota	pěchota	k1gFnSc1	pěchota
měla	mít	k5eAaImAgFnS	mít
sílu	síla	k1gFnSc4	síla
60	[number]	k4	60
tisíc	tisíc	k4xCgInPc2	tisíc
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
12	[number]	k4	12
tisíc	tisíc	k4xCgInPc2	tisíc
partyzánů	partyzán	k1gMnPc2	partyzán
<g/>
,	,	kIx,	,
vyčerpaných	vyčerpaný	k2eAgFnPc2d1	vyčerpaná
předcházejícími	předcházející	k2eAgInPc7d1	předcházející
boji	boj	k1gInPc7	boj
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgFnPc1	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byly	být	k5eAaImAgFnP	být
ozbrojeny	ozbrojit	k5eAaPmNgFnP	ozbrojit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Německá	německý	k2eAgFnSc1d1	německá
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
začala	začít	k5eAaPmAgFnS	začít
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
předehru	předehra	k1gFnSc4	předehra
k	k	k7c3	k
ofenzívě	ofenzíva	k1gFnSc3	ofenzíva
Němci	Němec	k1gMnPc1	Němec
bombardovali	bombardovat	k5eAaImAgMnP	bombardovat
Banskou	banský	k2eAgFnSc4d1	Banská
Bystrici	Bystrica	k1gFnSc4	Bystrica
<g/>
,	,	kIx,	,
Liptovskou	liptovský	k2eAgFnSc4d1	Liptovská
Osadu	osada	k1gFnSc4	osada
a	a	k8xC	a
letiště	letiště	k1gNnSc4	letiště
Tri	Tri	k1gMnSc2	Tri
Duby	Duba	k1gMnSc2	Duba
<g/>
.	.	kIx.	.
</s>
<s>
Brigáda	brigáda	k1gFnSc1	brigáda
Dirlewanger	Dirlewangra	k1gFnPc2	Dirlewangra
zaútočila	zaútočit	k5eAaPmAgFnS	zaútočit
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
na	na	k7c4	na
Biely	Biela	k1gFnPc4	Biela
Potok	potok	k1gInSc4	potok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
těžkých	těžký	k2eAgInPc6d1	těžký
bojích	boj	k1gInPc6	boj
zastavena	zastavit	k5eAaPmNgFnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Útoky	útok	k1gInPc4	útok
ostatních	ostatní	k2eAgFnPc2d1	ostatní
německých	německý	k2eAgFnPc2d1	německá
sil	síla	k1gFnPc2	síla
však	však	k9	však
byly	být	k5eAaImAgInP	být
úspěšné	úspěšný	k2eAgInPc1d1	úspěšný
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
obsadit	obsadit	k5eAaPmF	obsadit
některé	některý	k3yIgInPc4	některý
opěrné	opěrný	k2eAgInPc4d1	opěrný
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
svolal	svolat	k5eAaPmAgMnS	svolat
generál	generál	k1gMnSc1	generál
Viest	Viest	k1gMnSc1	Viest
poradu	porada	k1gFnSc4	porada
svého	svůj	k3xOyFgInSc2	svůj
štábu	štáb	k1gInSc2	štáb
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
určovala	určovat	k5eAaImAgFnS	určovat
další	další	k2eAgFnSc4d1	další
taktiku	taktika	k1gFnSc4	taktika
bojů	boj	k1gInPc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příštích	příští	k2eAgInPc6d1	příští
dvou	dva	k4xCgInPc6	dva
dnech	den	k1gInPc6	den
klidu	klid	k1gInSc2	klid
byly	být	k5eAaImAgFnP	být
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
boje	boj	k1gInSc2	boj
obnoveny	obnovit	k5eAaPmNgFnP	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Slovenská	slovenský	k2eAgNnPc1d1	slovenské
letiště	letiště	k1gNnPc1	letiště
Zolná	Zolný	k2eAgNnPc1d1	Zolný
a	a	k8xC	a
Tri	Tri	k1gMnSc2	Tri
Duby	Duba	k1gMnSc2	Duba
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
dosahu	dosah	k1gInSc2	dosah
palby	palba	k1gFnSc2	palba
německého	německý	k2eAgNnSc2d1	německé
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
stíhací	stíhací	k2eAgInSc1d1	stíhací
pluk	pluk	k1gInSc1	pluk
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
evakuován	evakuovat	k5eAaBmNgInS	evakuovat
<g/>
.	.	kIx.	.
</s>
<s>
Povstalci	povstalec	k1gMnPc1	povstalec
tak	tak	k6eAd1	tak
ztratili	ztratit	k5eAaPmAgMnP	ztratit
veškerou	veškerý	k3xTgFnSc4	veškerý
leteckou	letecký	k2eAgFnSc4d1	letecká
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
postupovala	postupovat	k5eAaImAgFnS	postupovat
německá	německý	k2eAgFnSc1d1	německá
divize	divize	k1gFnSc1	divize
Tatra	Tatra	k1gFnSc1	Tatra
na	na	k7c4	na
Banskou	banský	k2eAgFnSc4d1	Banská
Bystricu	Bystrica	k1gFnSc4	Bystrica
od	od	k7c2	od
Zvolena	Zvolen	k1gInSc2	Zvolen
i	i	k8xC	i
od	od	k7c2	od
Brezna	Brezna	k1gFnSc1	Brezna
<g/>
.	.	kIx.	.
</s>
<s>
Velitelství	velitelství	k1gNnSc1	velitelství
1	[number]	k4	1
<g/>
.	.	kIx.	.
čs	čs	kA	čs
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
se	se	k3xPyFc4	se
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
do	do	k7c2	do
Donoval	Donoval	k1gFnSc2	Donoval
a	a	k8xC	a
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
byla	být	k5eAaImAgFnS	být
vyklizena	vyklidit	k5eAaPmNgFnS	vyklidit
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
Viest	Viest	k1gMnSc1	Viest
vydal	vydat	k5eAaPmAgMnS	vydat
svůj	svůj	k3xOyFgInSc4	svůj
poslední	poslední	k2eAgInSc4d1	poslední
rozkaz	rozkaz	k1gInSc4	rozkaz
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
v	v	k7c6	v
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Přijal	přijmout	k5eAaPmAgMnS	přijmout
porážku	porážka	k1gFnSc4	porážka
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
všechny	všechen	k3xTgMnPc4	všechen
vojáky	voják	k1gMnPc4	voják
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
odešli	odejít	k5eAaPmAgMnP	odejít
do	do	k7c2	do
hor	hora	k1gFnPc2	hora
a	a	k8xC	a
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
partyzánské	partyzánský	k2eAgFnSc6d1	Partyzánská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Partyzánská	partyzánský	k2eAgFnSc1d1	Partyzánská
válka	válka	k1gFnSc1	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Němci	Němec	k1gMnPc1	Němec
začali	začít	k5eAaPmAgMnP	začít
hned	hned	k6eAd1	hned
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
s	s	k7c7	s
operacemi	operace	k1gFnPc7	operace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
zlikvidovat	zlikvidovat	k5eAaPmF	zlikvidovat
jak	jak	k8xS	jak
partyzány	partyzána	k1gFnSc2	partyzána
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zbytky	zbytek	k1gInPc4	zbytek
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
byli	být	k5eAaImAgMnP	být
zradou	zrada	k1gFnSc7	zrada
zajati	zajat	k2eAgMnPc1d1	zajat
velitelé	velitel	k1gMnPc1	velitel
povstání	povstání	k1gNnSc2	povstání
Viest	Viest	k1gMnSc1	Viest
a	a	k8xC	a
Golián	Golián	k1gMnSc1	Golián
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
odvezeni	odvézt	k5eAaPmNgMnP	odvézt
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
tam	tam	k6eAd1	tam
popraveni	popravit	k5eAaPmNgMnP	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ustoupily	ustoupit	k5eAaPmAgFnP	ustoupit
do	do	k7c2	do
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
bojů	boj	k1gInPc2	boj
vyřazeny	vyřazen	k2eAgFnPc1d1	vyřazena
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
měly	mít	k5eAaImAgInP	mít
málo	málo	k1gNnSc4	málo
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
munice	munice	k1gFnSc2	munice
a	a	k8xC	a
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
nebyly	být	k5eNaImAgFnP	být
vybaveny	vybavit	k5eAaPmNgFnP	vybavit
zimním	zimní	k2eAgNnSc7d1	zimní
oblečením	oblečení	k1gNnSc7	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
partyzánské	partyzánský	k2eAgFnPc1d1	Partyzánská
skupiny	skupina	k1gFnPc1	skupina
však	však	k9	však
vedly	vést	k5eAaImAgInP	vést
boje	boj	k1gInPc1	boj
s	s	k7c7	s
Němci	Němec	k1gMnPc7	Němec
prakticky	prakticky	k6eAd1	prakticky
celou	celý	k2eAgFnSc4d1	celá
zimu	zima	k1gFnSc4	zima
a	a	k8xC	a
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1945	[number]	k4	1945
se	se	k3xPyFc4	se
účastnily	účastnit	k5eAaImAgFnP	účastnit
pomocných	pomocný	k2eAgInPc2d1	pomocný
akcí	akce	k1gFnPc2	akce
při	při	k7c6	při
osvobozování	osvobozování	k1gNnSc6	osvobozování
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
partyzánskému	partyzánský	k2eAgInSc3d1	partyzánský
boji	boj	k1gInSc3	boj
byla	být	k5eAaImAgFnS	být
donucena	donucen	k2eAgFnSc1d1	donucena
2	[number]	k4	2
<g/>
.	.	kIx.	.
československá	československý	k2eAgFnSc1d1	Československá
paradesantní	paradesantní	k2eAgFnSc1d1	paradesantní
brigáda	brigáda	k1gFnSc1	brigáda
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
partyzánů	partyzán	k1gMnPc2	partyzán
a	a	k8xC	a
bývalých	bývalý	k2eAgMnPc2d1	bývalý
vojáků	voják	k1gMnPc2	voják
armády	armáda	k1gFnSc2	armáda
při	při	k7c6	při
boji	boj	k1gInSc6	boj
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
hladem	hlad	k1gInSc7	hlad
a	a	k8xC	a
vyčerpáním	vyčerpání	k1gNnSc7	vyčerpání
<g/>
.	.	kIx.	.
</s>
<s>
Partyzánské	partyzánský	k2eAgFnPc1d1	Partyzánská
jednotky	jednotka	k1gFnPc1	jednotka
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
však	však	k9	však
stále	stále	k6eAd1	stále
tvořily	tvořit	k5eAaImAgFnP	tvořit
velkou	velký	k2eAgFnSc4d1	velká
bojovou	bojový	k2eAgFnSc4d1	bojová
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
ale	ale	k9	ale
vybaveny	vybavit	k5eAaPmNgFnP	vybavit
pouze	pouze	k6eAd1	pouze
lehkými	lehký	k2eAgFnPc7d1	lehká
zbraněmi	zbraň	k1gFnPc7	zbraň
a	a	k8xC	a
mohly	moct	k5eAaImAgFnP	moct
provádět	provádět	k5eAaImF	provádět
jen	jen	k9	jen
omezené	omezený	k2eAgInPc4d1	omezený
útoky	útok	k1gInPc4	útok
a	a	k8xC	a
záškodnické	záškodnický	k2eAgFnPc4d1	záškodnická
akce	akce	k1gFnPc4	akce
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
štáb	štáb	k1gInSc1	štáb
sídlil	sídlit	k5eAaImAgInS	sídlit
v	v	k7c6	v
Lomnisté	Lomnista	k1gMnPc1	Lomnista
dolině	dolina	k1gFnSc6	dolina
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
Nízkých	nízký	k2eAgFnPc2d1	nízká
Tater	Tatra	k1gFnPc2	Tatra
<g/>
.	.	kIx.	.
</s>
<s>
Partyzánská	partyzánský	k2eAgFnSc1d1	Partyzánská
válka	válka	k1gFnSc1	válka
vázala	vázat	k5eAaImAgFnS	vázat
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
a	a	k8xC	a
prosinci	prosinec	k1gInSc6	prosinec
1944	[number]	k4	1944
německé	německý	k2eAgFnPc1d1	německá
síly	síla	k1gFnPc1	síla
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
zhruba	zhruba	k6eAd1	zhruba
35	[number]	k4	35
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
partyzánům	partyzán	k1gMnPc3	partyzán
podařilo	podařit	k5eAaPmAgNnS	podařit
provést	provést	k5eAaPmF	provést
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
72	[number]	k4	72
destrukcí	destrukce	k1gFnPc2	destrukce
na	na	k7c6	na
železnicích	železnice	k1gFnPc6	železnice
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
tak	tak	k6eAd1	tak
omezit	omezit	k5eAaPmF	omezit
přepravu	přeprava	k1gFnSc4	přeprava
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
,	,	kIx,	,
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
zásob	zásoba	k1gFnPc2	zásoba
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
úlohu	úloha	k1gFnSc4	úloha
partyzáni	partyzán	k1gMnPc1	partyzán
sehráli	sehrát	k5eAaPmAgMnP	sehrát
i	i	k9	i
při	při	k7c6	při
Východoslovenské	východoslovenský	k2eAgFnSc6d1	Východoslovenská
operaci	operace	k1gFnSc6	operace
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
bojích	boj	k1gInPc6	boj
při	při	k7c6	při
osvobozování	osvobozování	k1gNnSc6	osvobozování
území	území	k1gNnSc2	území
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
jejich	jejich	k3xOp3gFnSc4	jejich
činnost	činnost	k1gFnSc4	činnost
s	s	k7c7	s
postupujícími	postupující	k2eAgInPc7d1	postupující
sovětskými	sovětský	k2eAgInPc7d1	sovětský
a	a	k8xC	a
československými	československý	k2eAgNnPc7d1	Československé
vojsky	vojsko	k1gNnPc7	vojsko
koordinována	koordinován	k2eAgFnSc1d1	koordinována
ze	z	k7c2	z
štábu	štáb	k1gInSc2	štáb
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Německý	německý	k2eAgInSc1d1	německý
teror	teror	k1gInSc1	teror
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
povstání	povstání	k1gNnSc6	povstání
Němci	Němec	k1gMnPc1	Němec
zformovali	zformovat	k5eAaPmAgMnP	zformovat
jednotku	jednotka	k1gFnSc4	jednotka
Einsatzgruppe	Einsatzgrupp	k1gInSc5	Einsatzgrupp
H	H	kA	H
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
byla	být	k5eAaImAgFnS	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
příslušníků	příslušník	k1gMnPc2	příslušník
Sicherheitsdienstu	Sicherheitsdienst	k1gInSc2	Sicherheitsdienst
a	a	k8xC	a
Sicherheitspolizei	Sicherheitspolize	k1gFnSc2	Sicherheitspolize
a	a	k8xC	a
jejím	její	k3xOp3gInSc7	její
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
pacifikovat	pacifikovat	k5eAaBmF	pacifikovat
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Sestávala	sestávat	k5eAaImAgFnS	sestávat
s	s	k7c7	s
Einsatzkommanda	Einsatzkommando	k1gNnSc2	Einsatzkommando
<g/>
,	,	kIx,	,
Sonderkommanda	Sonderkommando	k1gNnSc2	Sonderkommando
a	a	k8xC	a
příslušníků	příslušník	k1gMnPc2	příslušník
ZbV	ZbV	k1gFnSc2	ZbV
(	(	kIx(	(
Zur	Zur	k1gMnSc1	Zur
besondere	besondrat	k5eAaPmIp3nS	besondrat
Verwendung	Verwendung	k1gMnSc1	Verwendung
)	)	kIx)	)
jednotkám	jednotka	k1gFnPc3	jednotka
mimořádného	mimořádný	k2eAgNnSc2d1	mimořádné
použití	použití	k1gNnSc2	použití
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
německé	německý	k2eAgFnPc1d1	německá
skupiny	skupina	k1gFnPc1	skupina
spolupracovaly	spolupracovat	k5eAaImAgFnP	spolupracovat
s	s	k7c7	s
Pohotovostními	pohotovostní	k2eAgInPc7d1	pohotovostní
oddíly	oddíl	k1gInPc7	oddíl
Hlinkovy	Hlinkův	k2eAgFnSc2d1	Hlinkova
gardy	garda	k1gFnSc2	garda
a	a	k8xC	a
s	s	k7c7	s
oddíly	oddíl	k1gInPc7	oddíl
četnictva	četnictvo	k1gNnSc2	četnictvo
<g/>
.	.	kIx.	.
</s>
<s>
Hledaly	hledat	k5eAaImAgFnP	hledat
nejen	nejen	k6eAd1	nejen
povstalce	povstalec	k1gMnPc4	povstalec
a	a	k8xC	a
partyzány	partyzán	k1gMnPc4	partyzán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zasahovaly	zasahovat	k5eAaImAgFnP	zasahovat
i	i	k9	i
proti	proti	k7c3	proti
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jim	on	k3xPp3gMnPc3	on
pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
mnohdy	mnohdy	k6eAd1	mnohdy
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
domnělou	domnělý	k2eAgFnSc4d1	domnělá
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnPc1	Němec
rozpoutali	rozpoutat	k5eAaPmAgMnP	rozpoutat
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
teror	teror	k1gInSc4	teror
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgNnSc6	který
byli	být	k5eAaImAgMnP	být
zabíjeni	zabíjen	k2eAgMnPc1d1	zabíjen
nejen	nejen	k6eAd1	nejen
partyzáni	partyzán	k1gMnPc1	partyzán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
masakrům	masakr	k1gInPc3	masakr
mnoha	mnoho	k4c2	mnoho
civilistů	civilista	k1gMnPc2	civilista
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgInPc1d1	německý
oddíly	oddíl	k1gInPc1	oddíl
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgInPc3	jenž
patřily	patřit	k5eAaImAgInP	patřit
i	i	k9	i
zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
jednotky	jednotka	k1gFnPc1	jednotka
"	"	kIx"	"
<g/>
Edelweiss	Edelweiss	k1gInSc1	Edelweiss
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Jozef	Jozef	k1gInSc1	Jozef
<g/>
"	"	kIx"	"
povraždily	povraždit	k5eAaPmAgInP	povraždit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5000	[number]	k4	5000
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
sadistických	sadistický	k2eAgFnPc2d1	sadistická
jednotek	jednotka	k1gFnPc2	jednotka
byly	být	k5eAaImAgInP	být
povolány	povolat	k5eAaPmNgInP	povolat
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
Balkánu	Balkán	k1gInSc2	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
bylo	být	k5eAaImAgNnS	být
vypáleno	vypálit	k5eAaPmNgNnS	vypálit
90	[number]	k4	90
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
osad	osada	k1gFnPc2	osada
a	a	k8xC	a
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
odkryto	odkrýt	k5eAaPmNgNnS	odkrýt
211	[number]	k4	211
masových	masový	k2eAgInPc2d1	masový
hrobů	hrob	k1gInPc2	hrob
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
největším	veliký	k2eAgInPc3d3	veliký
masakrům	masakr	k1gInPc3	masakr
patřily	patřit	k5eAaImAgInP	patřit
popravy	poprava	k1gFnPc4	poprava
v	v	k7c6	v
Kremničce	Kremnička	k1gFnSc6	Kremnička
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
přibližně	přibližně	k6eAd1	přibližně
700	[number]	k4	700
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
k	k	k7c3	k
nejotřesnějším	otřesný	k2eAgFnPc3d3	nejotřesnější
exekucím	exekuce	k1gFnPc3	exekuce
docházelo	docházet	k5eAaImAgNnS	docházet
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Nemecká	Nemecká	k1gFnSc1	Nemecká
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Němci	Němec	k1gMnPc1	Němec
prováděli	provádět	k5eAaImAgMnP	provádět
exekuce	exekuce	k1gFnPc4	exekuce
v	v	k7c4	v
místní	místní	k2eAgInPc4d1	místní
vápence	vápenec	k1gInPc4	vápenec
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
byli	být	k5eAaImAgMnP	být
nejprve	nejprve	k6eAd1	nejprve
okradeni	okrást	k5eAaPmNgMnP	okrást
o	o	k7c4	o
cennosti	cennost	k1gFnPc4	cennost
a	a	k8xC	a
poté	poté	k6eAd1	poté
byli	být	k5eAaImAgMnP	být
popraveni	popravit	k5eAaPmNgMnP	popravit
ranou	rána	k1gFnSc7	rána
do	do	k7c2	do
týlu	týl	k1gInSc2	týl
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jejich	jejich	k3xOp3gNnPc1	jejich
těla	tělo	k1gNnPc1	tělo
padala	padat	k5eAaImAgNnP	padat
do	do	k7c2	do
rozpálené	rozpálený	k2eAgFnSc2d1	rozpálená
pece	pec	k1gFnSc2	pec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
až	až	k9	až
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1945	[number]	k4	1945
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
několik	několik	k4yIc1	několik
stovek	stovka	k1gFnPc2	stovka
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
byly	být	k5eAaImAgFnP	být
i	i	k9	i
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
osoby	osoba	k1gFnPc1	osoba
"	"	kIx"	"
<g/>
rasově	rasově	k6eAd1	rasově
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
<g/>
"	"	kIx"	"
či	či	k8xC	či
zajatí	zajatý	k2eAgMnPc1d1	zajatý
američtí	americký	k2eAgMnPc1d1	americký
letci	letec	k1gMnPc1	letec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
předtím	předtím	k6eAd1	předtím
pomáhali	pomáhat	k5eAaImAgMnP	pomáhat
povstalcům	povstalec	k1gMnPc3	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
Odhady	odhad	k1gInPc1	odhad
počtu	počet	k1gInSc2	počet
obětí	oběť	k1gFnSc7	oběť
v	v	k7c6	v
Nemecké	Nemecká	k1gFnSc6	Nemecká
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
450	[number]	k4	450
až	až	k9	až
900	[number]	k4	900
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
německé	německý	k2eAgInPc1d1	německý
válečné	válečný	k2eAgInPc1d1	válečný
zločiny	zločin	k1gInPc1	zločin
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
nejhorších	zlý	k2eAgFnPc2d3	nejhorší
kapitol	kapitola	k1gFnPc2	kapitola
historie	historie	k1gFnSc2	historie
Slovenska	Slovensko	k1gNnSc2	Slovensko
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
ovšem	ovšem	k9	ovšem
zmínit	zmínit	k5eAaPmF	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
masakrech	masakr	k1gInPc6	masakr
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
na	na	k7c6	na
masakrech	masakr	k1gInPc6	masakr
v	v	k7c6	v
Nemcové	Nemcová	k1gFnSc6	Nemcová
a	a	k8xC	a
Kremničce	Kremnička	k1gFnSc6	Kremnička
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
významnou	významný	k2eAgFnSc7d1	významná
měrou	míra	k1gFnSc7wR	míra
podílely	podílet	k5eAaImAgFnP	podílet
Hlinkovy	Hlinkův	k2eAgFnPc1d1	Hlinkova
gardy	garda	k1gFnPc1	garda
<g/>
.	.	kIx.	.
</s>
<s>
Zpolitizovaný	zpolitizovaný	k2eAgInSc4d1	zpolitizovaný
proces	proces	k1gInSc4	proces
s	s	k7c7	s
některými	některý	k3yIgMnPc7	některý
členy	člen	k1gMnPc7	člen
Hlinkovy	Hlinkův	k2eAgFnSc2d1	Hlinkova
gardy	garda	k1gFnSc2	garda
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k8xC	i
představiteli	představitel	k1gMnPc7	představitel
Slovenského	slovenský	k2eAgInSc2d1	slovenský
štátu	štát	k1gInSc2	štát
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
natočen	natočit	k5eAaBmNgInS	natočit
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
silně	silně	k6eAd1	silně
ideologicky	ideologicky	k6eAd1	ideologicky
podkreslený	podkreslený	k2eAgInSc1d1	podkreslený
<g/>
,	,	kIx,	,
dokument	dokument	k1gInSc1	dokument
Nikdy	nikdy	k6eAd1	nikdy
viac	viac	k6eAd1	viac
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
historicky	historicky	k6eAd1	historicky
cenné	cenný	k2eAgFnPc1d1	cenná
jsou	být	k5eAaImIp3nP	být
autentické	autentický	k2eAgFnPc1d1	autentická
výpovědi	výpověď	k1gFnPc1	výpověď
hlinkovců	hlinkovec	k1gMnPc2	hlinkovec
<g/>
,	,	kIx,	,
přímých	přímý	k2eAgMnPc2d1	přímý
účastníků	účastník	k1gMnPc2	účastník
zmíněných	zmíněný	k2eAgInPc2d1	zmíněný
masakrů	masakr	k1gInPc2	masakr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
SNP	SNP	kA	SNP
v	v	k7c6	v
názvech	název	k1gInPc6	název
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	a
v	v	k7c6	v
místních	místní	k2eAgInPc6d1	místní
názvech	název	k1gInPc6	název
==	==	k?	==
</s>
</p>
<p>
<s>
Památník	památník	k1gInSc1	památník
SNP	SNP	kA	SNP
(	(	kIx(	(
<g/>
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Most	most	k1gInSc1	most
SNP	SNP	kA	SNP
(	(	kIx(	(
<g/>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Janu	Jan	k1gMnSc6	Jan
Švermovi	Šverm	k1gMnSc6	Šverm
<g/>
,	,	kIx,	,
komunistickém	komunistický	k2eAgMnSc6d1	komunistický
politiku	politik	k1gMnSc6	politik
a	a	k8xC	a
účastníku	účastník	k1gMnSc6	účastník
SNP	SNP	kA	SNP
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
během	během	k7c2	během
něj	on	k3xPp3gInSc2	on
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
během	během	k7c2	během
socialistické	socialistický	k2eAgFnSc2d1	socialistická
éry	éra	k1gFnSc2	éra
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
vesnice	vesnice	k1gFnSc1	vesnice
Telgárt	Telgárta	k1gFnPc2	Telgárta
(	(	kIx(	(
<g/>
Švermovo	Švermův	k2eAgNnSc1d1	Švermovo
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
např.	např.	kA	např.
Švermova	Švermův	k2eAgFnSc1d1	Švermova
ulice	ulice	k1gFnSc1	ulice
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
(	(	kIx(	(
<g/>
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
Vrobelovu	Vrobelův	k2eAgFnSc4d1	Vrobelova
<g/>
,	,	kIx,	,
mnohé	mnohé	k1gNnSc4	mnohé
ulice	ulice	k1gFnSc2	ulice
v	v	k7c6	v
ČR	ČR	kA	ČR
ale	ale	k8xC	ale
dosud	dosud	k6eAd1	dosud
nesou	nést	k5eAaImIp3nP	nést
tento	tento	k3xDgInSc4	tento
název	název	k1gInSc4	název
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
název	název	k1gInSc4	název
ulice	ulice	k1gFnSc2	ulice
'	'	kIx"	'
<g/>
SNP	SNP	kA	SNP
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
Kladna	Kladno	k1gNnSc2	Kladno
nese	nést	k5eAaImIp3nS	nést
dodnes	dodnes	k6eAd1	dodnes
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
název	název	k1gInSc1	název
Švermov	Švermov	k1gInSc1	Švermov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
TÓTH	TÓTH	kA	TÓTH
<g/>
,	,	kIx,	,
Dezider	Dezider	k1gInSc1	Dezider
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
Golian	Golian	k1gMnSc1	Golian
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
doba	doba	k1gFnSc1	doba
:	:	kIx,	:
materiály	materiál	k1gInPc7	materiál
z	z	k7c2	z
odborného	odborný	k2eAgMnSc2d1	odborný
seminára	seminár	k1gMnSc2	seminár
k	k	k7c3	k
100	[number]	k4	100
<g/>
.	.	kIx.	.
výročiu	výročium	k1gNnSc6	výročium
narodenia	narodenium	k1gNnSc2	narodenium
Jána	Ján	k1gMnSc2	Ján
Goliana	Golian	k1gMnSc2	Golian
<g/>
,	,	kIx,	,
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
31	[number]	k4	31
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
:	:	kIx,	:
zborník	zborník	k1gMnSc1	zborník
Múzea	Múzeus	k1gMnSc2	Múzeus
Slovenského	slovenský	k2eAgNnSc2d1	slovenské
národného	národný	k2eAgNnSc2d1	národné
povstania	povstanium	k1gNnSc2	povstanium
v	v	k7c6	v
Banskej	Banskej	k?	Banskej
Bystrici	Bystrica	k1gFnSc6	Bystrica
<g/>
.	.	kIx.	.
</s>
<s>
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
<g/>
:	:	kIx,	:
Múzeum	Múzeum	k1gNnSc1	Múzeum
Slovenského	slovenský	k2eAgNnSc2d1	slovenské
národného	národný	k2eAgNnSc2d1	národné
povstania	povstanium	k1gNnSc2	povstanium
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
286	[number]	k4	286
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
970072	[number]	k4	970072
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
Československé	československý	k2eAgFnPc1d1	Československá
vojenské	vojenský	k2eAgFnPc1d1	vojenská
dějiny	dějiny	k1gFnPc1	dějiny
<g/>
,	,	kIx,	,
Naše	náš	k3xOp1gFnPc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
1989	[number]	k4	1989
</s>
</p>
<p>
<s>
KLIMENT	Kliment	k1gMnSc1	Kliment
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
K.	K.	kA	K.
<g/>
;	;	kIx,	;
NAKLÁDAL	nakládat	k5eAaImAgMnS	nakládat
<g/>
,	,	kIx,	,
Břetislav	Břetislav	k1gMnSc1	Břetislav
<g/>
.	.	kIx.	.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
armáda	armáda	k1gFnSc1	armáda
1939	[number]	k4	1939
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Levné	levný	k2eAgFnSc2d1	levná
knihy	kniha	k1gFnSc2	kniha
KMa	KMa	k1gFnSc2	KMa
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7309	[number]	k4	7309
<g/>
-	-	kIx~	-
<g/>
395	[number]	k4	395
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOL	kol	k6eAd1	kol
<g/>
.	.	kIx.	.
</s>
<s>
Dějinná	dějinný	k2eAgFnSc1d1	dějinná
křižovatka	křižovatka	k1gFnSc1	křižovatka
:	:	kIx,	:
Slovenské	slovenský	k2eAgNnSc1d1	slovenské
národní	národní	k2eAgNnSc1d1	národní
povstání	povstání	k1gNnSc1	povstání
<g/>
,	,	kIx,	,
předpoklady	předpoklad	k1gInPc1	předpoklad
a	a	k8xC	a
výsledky	výsledek	k1gInPc1	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
politické	politický	k2eAgFnSc2d1	politická
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
515	[number]	k4	515
s.	s.	k?	s.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
KOL	kol	k6eAd1	kol
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jasných	jasný	k2eAgFnPc6d1	jasná
cestách	cesta	k1gFnPc6	cesta
:	:	kIx,	:
K	k	k7c3	k
štvrtému	štvrtý	k2eAgNnSc3d1	štvrtý
výročiu	výročium	k1gNnSc3	výročium
Slovenského	slovenský	k2eAgNnSc2d1	slovenské
národného	národný	k2eAgNnSc2d1	národné
povstania	povstanium	k1gNnSc2	povstanium
1944	[number]	k4	1944
<g/>
-	-	kIx~	-
<g/>
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Tatran	Tatran	k1gInSc1	Tatran
<g/>
,	,	kIx,	,
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
18	[number]	k4	18
s.	s.	k?	s.
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
HUSÁK	Husák	k1gMnSc1	Husák
<g/>
,	,	kIx,	,
Gustáv	Gustáva	k1gFnPc2	Gustáva
<g/>
.	.	kIx.	.
</s>
<s>
Svedectvo	Svedectvo	k1gNnSc1	Svedectvo
o	o	k7c4	o
Slovenskom	Slovenskom	k1gInSc4	Slovenskom
národnom	národnom	k1gInSc4	národnom
povstaní	povstaný	k2eAgMnPc5d1	povstaný
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Vydavateľstvo	Vydavateľstvo	k1gNnSc1	Vydavateľstvo
politickej	politickej	k?	politickej
literatúry	literatúra	k1gMnSc2	literatúra
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
617	[number]	k4	617
s.	s.	k?	s.
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
KROPILÁK	KROPILÁK	kA	KROPILÁK
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Slovenské	slovenský	k2eAgNnSc1d1	slovenské
národní	národní	k2eAgNnSc1d1	národní
povstání	povstání	k1gNnSc1	povstání
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
30	[number]	k4	30
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
jeho	on	k3xPp3gInSc2	on
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
269	[number]	k4	269
s.	s.	k?	s.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
povstání	povstání	k1gNnSc4	povstání
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
povstání	povstání	k1gNnSc4	povstání
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
povstání	povstání	k1gNnSc2	povstání
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Slovenský	slovenský	k2eAgInSc1d1	slovenský
svaz	svaz	k1gInSc1	svaz
protifašistických	protifašistický	k2eAgMnPc2d1	protifašistický
bojovníků	bojovník	k1gMnPc2	bojovník
</s>
</p>
<p>
<s>
Fašistické	fašistický	k2eAgFnPc1d1	fašistická
represálie	represálie	k1gFnPc1	represálie
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
</s>
</p>
<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
Slovenského	slovenský	k2eAgNnSc2d1	slovenské
národního	národní	k2eAgNnSc2d1	národní
povstání	povstání	k1gNnSc2	povstání
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
</s>
</p>
<p>
<s>
Masakr	masakr	k1gInSc1	masakr
v	v	k7c6	v
Kľaku	Kľak	k1gInSc6	Kľak
a	a	k8xC	a
Ostrém	ostrý	k2eAgInSc6d1	ostrý
Grúňu	Grúňus	k1gInSc6	Grúňus
<g/>
:	:	kIx,	:
slovenské	slovenský	k2eAgInPc1d1	slovenský
Lidice	Lidice	k1gInPc1	Lidice
</s>
</p>
<p>
<s>
http://www.ceskenarodnilisty.cz/	[url]	k?	http://www.ceskenarodnilisty.cz/
Prof.	prof.	kA	prof.
PhDr.	PhDr.	kA	PhDr.
Jiří	Jiří	k1gMnSc1	Jiří
Frajdl	Frajdl	k1gMnSc1	Frajdl
<g/>
,	,	kIx,	,
CSc	CSc	kA	CSc
–	–	k?	–
Krize	krize	k1gFnPc4	krize
nacistického	nacistický	k2eAgInSc2d1	nacistický
režimu	režim	k1gInSc2	režim
1944	[number]	k4	1944
a	a	k8xC	a
SNP	SNP	kA	SNP
<g/>
,	,	kIx,	,
Koncepce	koncepce	k1gFnPc1	koncepce
prezidenta	prezident	k1gMnSc2	prezident
E.	E.	kA	E.
<g/>
Beneše	Beneš	k1gMnSc2	Beneš
na	na	k7c4	na
obnovu	obnova	k1gFnSc4	obnova
čs	čs	kA	čs
<g/>
.	.	kIx.	.
<g/>
suverenity	suverenita	k1gFnSc2	suverenita
<g/>
,	,	kIx,	,
Několik	několik	k4yIc4	několik
poznámek	poznámka	k1gFnPc2	poznámka
k	k	k7c3	k
SNP	SNP	kA	SNP
</s>
</p>
