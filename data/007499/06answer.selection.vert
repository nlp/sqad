<s>
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
posádka	posádka	k1gFnSc1	posádka
<g/>
,	,	kIx,	,
tvořená	tvořený	k2eAgFnSc1d1	tvořená
500	[number]	k4	500
vojáky	voják	k1gMnPc7	voják
a	a	k8xC	a
asi	asi	k9	asi
tisícovkou	tisícovka	k1gFnSc7	tisícovka
příslušníků	příslušník	k1gMnPc2	příslušník
městské	městský	k2eAgFnSc2d1	městská
milice	milice	k1gFnSc2	milice
<g/>
,	,	kIx,	,
ubránila	ubránit	k5eAaPmAgFnS	ubránit
město	město	k1gNnSc4	město
proti	proti	k7c3	proti
asi	asi	k9	asi
28	[number]	k4	28
tisícům	tisíc	k4xCgInPc3	tisíc
vojáků	voják	k1gMnPc2	voják
generála	generál	k1gMnSc2	generál
Lennarta	Lennart	k1gMnSc2	Lennart
Torstensona	Torstenson	k1gMnSc2	Torstenson
<g/>
.	.	kIx.	.
</s>
