<p>
<s>
Dubai	Dubai	k6eAd1	Dubai
Aviation	Aviation	k1gInSc1	Aviation
Corporation	Corporation	k1gInSc1	Corporation
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
م	م	k?	م
د	د	k?	د
ل	ل	k?	ل
<g/>
)	)	kIx)	)
vystupující	vystupující	k2eAgInSc1d1	vystupující
pod	pod	k7c7	pod
obchodním	obchodní	k2eAgNnSc7d1	obchodní
jménem	jméno	k1gNnSc7	jméno
flydubai	flyduba	k1gFnSc2	flyduba
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
ف	ف	k?	ف
د	د	k?	د
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nízkonákladová	nízkonákladový	k2eAgFnSc1d1	nízkonákladová
letecká	letecký	k2eAgFnSc1d1	letecká
společnost	společnost	k1gFnSc1	společnost
se	s	k7c7	s
základnou	základna	k1gFnSc7	základna
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
v	v	k7c6	v
Dubaji	Dubaj	k1gFnSc6	Dubaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2016	[number]	k4	2016
létala	létat	k5eAaImAgFnS	létat
flydubai	flydubai	k6eAd1	flydubai
do	do	k7c2	do
95	[number]	k4	95
destinací	destinace	k1gFnPc2	destinace
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
<g/>
Společnost	společnost	k1gFnSc1	společnost
létá	létat	k5eAaImIp3nS	létat
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
pravidelně	pravidelně	k6eAd1	pravidelně
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
Bratislavy	Bratislava	k1gFnSc2	Bratislava
<g/>
,	,	kIx,	,
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
od	od	k7c2	od
léta	léto	k1gNnSc2	léto
2016	[number]	k4	2016
každodenně	každodenně	k6eAd1	každodenně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
založila	založit	k5eAaPmAgFnS	založit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2008	[number]	k4	2008
dubajská	dubajský	k2eAgFnSc1d1	dubajská
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
flydubai	flydubai	k6eAd1	flydubai
není	být	k5eNaImIp3nS	být
členem	člen	k1gMnSc7	člen
The	The	k1gMnSc2	The
Emirates	Emirates	k1gMnSc1	Emirates
Group	Group	k1gMnSc1	Group
<g/>
,	,	kIx,	,
společnost	společnost	k1gFnSc1	společnost
Emirates	Emiratesa	k1gFnPc2	Emiratesa
ji	on	k3xPp3gFnSc4	on
podporovala	podporovat	k5eAaImAgFnS	podporovat
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2008	[number]	k4	2008
si	se	k3xPyFc3	se
společnost	společnost	k1gFnSc1	společnost
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
aerosalonu	aerosalon	k1gInSc6	aerosalon
ve	v	k7c4	v
Farnborough	Farnborough	k1gInSc4	Farnborough
závazně	závazně	k6eAd1	závazně
objednala	objednat	k5eAaPmAgFnS	objednat
50	[number]	k4	50
letadel	letadlo	k1gNnPc2	letadlo
typu	typ	k1gInSc2	typ
Boeing	boeing	k1gInSc4	boeing
737	[number]	k4	737
<g/>
-	-	kIx~	-
<g/>
800	[number]	k4	800
<g/>
,	,	kIx,	,
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
hodnotě	hodnota	k1gFnSc6	hodnota
3,74	[number]	k4	3,74
miliard	miliarda	k4xCgFnPc2	miliarda
$	$	kIx~	$
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
letadlo	letadlo	k1gNnSc1	letadlo
bylo	být	k5eAaImAgNnS	být
společnosti	společnost	k1gFnPc4	společnost
dodané	dodaná	k1gFnSc2	dodaná
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
zahájila	zahájit	k5eAaPmAgFnS	zahájit
provoz	provoz	k1gInSc4	provoz
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
otevřením	otevření	k1gNnSc7	otevření
linky	linka	k1gFnSc2	linka
z	z	k7c2	z
Dubaje	Dubaj	k1gInSc2	Dubaj
do	do	k7c2	do
Ammánu	Ammán	k1gInSc2	Ammán
<g/>
,	,	kIx,	,
o	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgInP	být
zahájeny	zahájen	k2eAgInPc4d1	zahájen
lety	let	k1gInPc4	let
do	do	k7c2	do
Bejrútu	Bejrút	k1gInSc2	Bejrút
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
linek	linka	k1gFnPc2	linka
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
začala	začít	k5eAaPmAgFnS	začít
rychle	rychle	k6eAd1	rychle
rozrůstat	rozrůstat	k5eAaImF	rozrůstat
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
létala	létat	k5eAaImAgFnS	létat
již	již	k6eAd1	již
do	do	k7c2	do
51	[number]	k4	51
destinací	destinace	k1gFnPc2	destinace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2013	[number]	k4	2013
společnost	společnost	k1gFnSc1	společnost
přidala	přidat	k5eAaPmAgFnS	přidat
do	do	k7c2	do
svých	svůj	k3xOyFgNnPc2	svůj
letadel	letadlo	k1gNnPc2	letadlo
možnost	možnost	k1gFnSc4	možnost
nové	nový	k2eAgInPc4d1	nový
cestovní	cestovní	k2eAgInPc4d1	cestovní
business	business	k1gInSc4	business
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
si	se	k3xPyFc3	se
získala	získat	k5eAaPmAgFnS	získat
nové	nový	k2eAgMnPc4d1	nový
zákazníky	zákazník	k1gMnPc4	zákazník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Dubai	Duba	k1gInSc6	Duba
Airshow	Airshow	k1gFnSc2	Airshow
2016	[number]	k4	2016
oznámila	oznámit	k5eAaPmAgFnS	oznámit
společnost	společnost	k1gFnSc1	společnost
objednávku	objednávka	k1gFnSc4	objednávka
75	[number]	k4	75
Boeingů	boeing	k1gInPc2	boeing
737	[number]	k4	737
MAX	max	kA	max
a	a	k8xC	a
11	[number]	k4	11
Boeingů	boeing	k1gInPc2	boeing
737-800	[number]	k4	737-800
za	za	k7c4	za
8,8	[number]	k4	8,8
miliard	miliarda	k4xCgFnPc2	miliarda
$	$	kIx~	$
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Flotila	flotila	k1gFnSc1	flotila
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2017	[number]	k4	2017
se	se	k3xPyFc4	se
flotila	flotila	k1gFnSc1	flotila
flydubai	flydubai	k6eAd1	flydubai
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
60	[number]	k4	60
letadel	letadlo	k1gNnPc2	letadlo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Letecké	letecký	k2eAgFnPc1d1	letecká
nehody	nehoda	k1gFnPc1	nehoda
==	==	k?	==
</s>
</p>
<p>
<s>
Let	let	k1gInSc1	let
Flydubai	Flydubai	k1gNnSc1	Flydubai
981	[number]	k4	981
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2016	[number]	k4	2016
havaroval	havarovat	k5eAaPmAgInS	havarovat
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
let	let	k1gInSc1	let
981	[number]	k4	981
na	na	k7c6	na
lince	linka	k1gFnSc6	linka
z	z	k7c2	z
Dubaje	Dubaj	k1gInSc2	Dubaj
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
přistání	přistání	k1gNnSc4	přistání
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
v	v	k7c6	v
Rostově	Rostův	k2eAgFnSc6d1	Rostova
na	na	k7c4	na
Donu	dona	k1gFnSc4	dona
<g/>
,	,	kIx,	,
všech	všecek	k3xTgInPc2	všecek
62	[number]	k4	62
osob	osoba	k1gFnPc2	osoba
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
prvá	prvý	k4xOgFnSc1	prvý
nehoda	nehoda	k1gFnSc1	nehoda
s	s	k7c7	s
fatálními	fatální	k2eAgInPc7d1	fatální
následky	následek	k1gInPc7	následek
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Příčiny	příčina	k1gFnPc1	příčina
se	se	k3xPyFc4	se
vyšetřují	vyšetřovat	k5eAaImIp3nP	vyšetřovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Flydubai	Flyduba	k1gFnSc2	Flyduba
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
