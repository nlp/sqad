<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Rostislav	Rostislav	k1gMnSc1	Rostislav
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
Rastislav	Rastislava	k1gFnPc2	Rastislava
<g/>
,	,	kIx,	,
Rastic	Rastice	k1gFnPc2	Rastice
<g/>
,	,	kIx,	,
Rasticlao	Rasticlao	k1gMnSc1	Rasticlao
<g/>
,	,	kIx,	,
Rastislaus	Rastislaus	k1gMnSc1	Rastislaus
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
Radislav	Radislav	k1gMnSc1	Radislav
či	či	k8xC	či
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
)	)	kIx)	)
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Mojmírovců	Mojmírovec	k1gMnPc2	Mojmírovec
(	(	kIx(	(
<g/>
datum	datum	k1gNnSc1	datum
narození	narození	k1gNnSc2	narození
neznámé	známý	k2eNgFnSc2d1	neznámá
–	–	k?	–
po	po	k7c6	po
870	[number]	k4	870
<g/>
,	,	kIx,	,
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
846	[number]	k4	846
<g/>
–	–	k?	–
<g/>
870	[number]	k4	870
druhým	druhý	k4xOgNnSc7	druhý
velkomoravským	velkomoravský	k2eAgNnSc7d1	velkomoravské
knížetem	kníže	k1gNnSc7wR	kníže
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
Velkomoravské	velkomoravský	k2eAgFnSc2d1	Velkomoravská
říše	říš	k1gFnSc2	říš
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
cyrilometodějská	cyrilometodějský	k2eAgFnSc1d1	Cyrilometodějská
misie	misie	k1gFnSc1	misie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1994	[number]	k4	1994
byl	být	k5eAaImAgInS	být
pravoslavnou	pravoslavný	k2eAgFnSc7d1	pravoslavná
církví	církev	k1gFnSc7	církev
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
prohlášen	prohlášen	k2eAgMnSc1d1	prohlášen
za	za	k7c4	za
svatého	svatý	k2eAgMnSc4d1	svatý
<g/>
.	.	kIx.	.
</s>
<s>
Slavnost	slavnost	k1gFnSc1	slavnost
svatořečení	svatořečení	k1gNnSc2	svatořečení
Rostislava	Rostislava	k1gFnSc1	Rostislava
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1994	[number]	k4	1994
v	v	k7c6	v
Prešově	Prešov	k1gInSc6	Prešov
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1994	[number]	k4	1994
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
pravoslavného	pravoslavný	k2eAgInSc2d1	pravoslavný
kalendáře	kalendář	k1gInSc2	kalendář
má	mít	k5eAaImIp3nS	mít
církevní	církevní	k2eAgInSc4d1	církevní
svátek	svátek	k1gInSc4	svátek
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Rostislav	Rostislav	k1gMnSc1	Rostislav
byl	být	k5eAaImAgMnS	být
synovcem	synovec	k1gMnSc7	synovec
Mojmíra	Mojmír	k1gMnSc4	Mojmír
I.	I.	kA	I.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
846	[number]	k4	846
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
po	po	k7c6	po
Mojmírově	Mojmírův	k2eAgFnSc6d1	Mojmírova
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Rostislav	Rostislav	k1gMnSc1	Rostislav
na	na	k7c4	na
velkomoravský	velkomoravský	k2eAgInSc4d1	velkomoravský
královský	královský	k2eAgInSc4d1	královský
stolec	stolec	k1gInSc4	stolec
dosazen	dosazen	k2eAgInSc4d1	dosazen
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
Němcem	Němec	k1gMnSc7	Němec
<g/>
,	,	kIx,	,
panovníkem	panovník	k1gMnSc7	panovník
východofranské	východofranský	k2eAgFnSc2d1	Východofranská
říše	říš	k1gFnSc2	říš
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
rozpadlé	rozpadlý	k2eAgFnSc2d1	rozpadlá
Franské	franský	k2eAgFnSc2d1	Franská
říše	říš	k1gFnSc2	říš
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
Němec	Němec	k1gMnSc1	Němec
považoval	považovat	k5eAaImAgMnS	považovat
Rostislava	Rostislav	k1gMnSc4	Rostislav
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
chráněnce	chráněnec	k1gMnSc4	chráněnec
<g/>
,	,	kIx,	,
ne	ne	k9	ne
<g/>
-li	i	k?	-li
vazala	vazal	k1gMnSc4	vazal
<g/>
,	,	kIx,	,
a	a	k8xC	a
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
velkomoravský	velkomoravský	k2eAgMnSc1d1	velkomoravský
kníže	kníže	k1gMnSc1	kníže
bude	být	k5eAaImBp3nS	být
podřizovat	podřizovat	k5eAaImF	podřizovat
jeho	jeho	k3xOp3gInPc3	jeho
politickým	politický	k2eAgInPc3d1	politický
zájmům	zájem	k1gInPc3	zájem
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
850	[number]	k4	850
však	však	k9	však
Rostislav	Rostislav	k1gMnSc1	Rostislav
styky	styk	k1gInPc4	styk
s	s	k7c7	s
východofranskou	východofranský	k2eAgFnSc7d1	Východofranská
říší	říš	k1gFnSc7	říš
přerušil	přerušit	k5eAaPmAgMnS	přerušit
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
podporovat	podporovat	k5eAaImF	podporovat
Ludvíkovu	Ludvíkův	k2eAgFnSc4d1	Ludvíkova
opozici	opozice	k1gFnSc4	opozice
a	a	k8xC	a
na	na	k7c4	na
Velkou	velký	k2eAgFnSc4d1	velká
Moravu	Morava	k1gFnSc4	Morava
přijímal	přijímat	k5eAaImAgInS	přijímat
Ludvíkovy	Ludvíkův	k2eAgMnPc4d1	Ludvíkův
odpůrce	odpůrce	k1gMnPc4	odpůrce
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
od	od	k7c2	od
Franků	Franky	k1gInPc2	Franky
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
likvidaci	likvidace	k1gFnSc3	likvidace
východofranského	východofranský	k2eAgInSc2d1	východofranský
vlivu	vliv	k1gInSc2	vliv
Rostislav	Rostislav	k1gMnSc1	Rostislav
vyhnal	vyhnat	k5eAaPmAgMnS	vyhnat
latinské	latinský	k2eAgMnPc4d1	latinský
kněze	kněz	k1gMnPc4	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
855	[number]	k4	855
se	se	k3xPyFc4	se
Ludvík	Ludvík	k1gMnSc1	Ludvík
Němec	Němec	k1gMnSc1	Němec
pokusil	pokusit	k5eAaPmAgMnS	pokusit
Rostislavovu	Rostislavův	k2eAgFnSc4d1	Rostislavova
neposlušnost	neposlušnost	k1gFnSc4	neposlušnost
potrestat	potrestat	k5eAaPmF	potrestat
a	a	k8xC	a
na	na	k7c4	na
Velkou	velký	k2eAgFnSc4d1	velká
Moravu	Morava	k1gFnSc4	Morava
vpadl	vpadnout	k5eAaPmAgMnS	vpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
voje	voj	k1gInPc1	voj
došly	dojít	k5eAaPmAgInP	dojít
až	až	k9	až
k	k	k7c3	k
Rostislavově	Rostislavův	k2eAgFnSc3d1	Rostislavova
hlavní	hlavní	k2eAgFnSc3d1	hlavní
pevnosti	pevnost	k1gFnSc3	pevnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
velkomoravské	velkomoravský	k2eAgNnSc1d1	velkomoravské
vojsko	vojsko	k1gNnSc1	vojsko
podniklo	podniknout	k5eAaPmAgNnS	podniknout
úspěšný	úspěšný	k2eAgInSc4d1	úspěšný
protiútok	protiútok	k1gInSc4	protiútok
<g/>
.	.	kIx.	.
</s>
<s>
Rostislav	Rostislav	k1gMnSc1	Rostislav
pak	pak	k6eAd1	pak
pronásledoval	pronásledovat	k5eAaImAgMnS	pronásledovat
východofranské	východofranský	k2eAgMnPc4d1	východofranský
vojáky	voják	k1gMnPc4	voják
až	až	k9	až
za	za	k7c4	za
hraniční	hraniční	k2eAgFnSc4d1	hraniční
řeku	řeka	k1gFnSc4	řeka
Dunaj	Dunaj	k1gInSc1	Dunaj
a	a	k8xC	a
vyplenil	vyplenit	k5eAaPmAgInS	vyplenit
východofranské	východofranský	k2eAgNnSc4d1	východofranský
území	území	k1gNnSc4	území
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
břehu	břeh	k1gInSc6	břeh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
858	[number]	k4	858
se	se	k3xPyFc4	se
Rostislav	Rostislav	k1gMnSc1	Rostislav
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
Karlomanem	Karloman	k1gMnSc7	Karloman
(	(	kIx(	(
<g/>
Karlmannem	Karlmann	k1gInSc7	Karlmann
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
správcem	správce	k1gMnSc7	správce
Východní	východní	k2eAgFnSc2d1	východní
marky	marka	k1gFnSc2	marka
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
část	část	k1gFnSc1	část
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
)	)	kIx)	)
a	a	k8xC	a
synem	syn	k1gMnSc7	syn
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Němce	Němec	k1gMnSc2	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Rostislav	Rostislav	k1gMnSc1	Rostislav
získal	získat	k5eAaPmAgMnS	získat
díky	díky	k7c3	díky
tomuto	tento	k3xDgNnSc3	tento
spojenectví	spojenectví	k1gNnSc3	spojenectví
některá	některý	k3yIgNnPc1	některý
území	území	k1gNnPc1	území
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
připojil	připojit	k5eAaPmAgInS	připojit
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
861	[number]	k4	861
Rostislav	Rostislava	k1gFnPc2	Rostislava
s	s	k7c7	s
Karlomanem	Karloman	k1gMnSc7	Karloman
znovu	znovu	k6eAd1	znovu
zaútočili	zaútočit	k5eAaPmAgMnP	zaútočit
na	na	k7c4	na
síly	síla	k1gFnPc4	síla
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Němce	Němec	k1gMnSc2	Němec
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yRgFnPc3	který
náležel	náležet	k5eAaImAgMnS	náležet
i	i	k8xC	i
Pribina	Pribina	k1gFnSc1	Pribina
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
kníže	kníže	k1gMnSc1	kníže
blatenského	blatenský	k2eAgNnSc2d1	Blatenské
knížectví	knížectví	k1gNnSc2	knížectví
<g/>
.	.	kIx.	.
</s>
<s>
Rostislav	Rostislav	k1gMnSc1	Rostislav
Blatenské	blatenský	k2eAgNnSc4d1	Blatenské
knížectví	knížectví	k1gNnSc4	knížectví
napadl	napadnout	k5eAaPmAgMnS	napadnout
<g/>
,	,	kIx,	,
Pribinu	Pribina	k1gFnSc4	Pribina
sesadil	sesadit	k5eAaPmAgMnS	sesadit
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
patrně	patrně	k6eAd1	patrně
zabit	zabít	k5eAaPmNgMnS	zabít
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
knížecí	knížecí	k2eAgInSc4d1	knížecí
stolec	stolec	k1gInSc4	stolec
dosadil	dosadit	k5eAaPmAgMnS	dosadit
Pribinova	Pribinův	k2eAgMnSc4d1	Pribinův
syna	syn	k1gMnSc4	syn
Kocela	Kocel	k1gMnSc4	Kocel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
tímto	tento	k3xDgMnSc7	tento
stal	stát	k5eAaPmAgMnS	stát
Rostislavovým	Rostislavův	k2eAgMnSc7d1	Rostislavův
chráněncem	chráněnec	k1gMnSc7	chráněnec
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
spojenectví	spojenectví	k1gNnSc3	spojenectví
Karlomana	Karloman	k1gMnSc2	Karloman
s	s	k7c7	s
Rostislavem	Rostislav	k1gMnSc7	Rostislav
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
Ludvík	Ludvík	k1gMnSc1	Ludvík
spojenectví	spojenectví	k1gNnSc4	spojenectví
s	s	k7c7	s
Bulhary	Bulhar	k1gMnPc7	Bulhar
<g/>
.	.	kIx.	.
</s>
<s>
Karloman	Karloman	k1gMnSc1	Karloman
je	být	k5eAaImIp3nS	být
nakonec	nakonec	k6eAd1	nakonec
poražen	porazit	k5eAaPmNgMnS	porazit
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
se	se	k3xPyFc4	se
svému	svůj	k3xOyFgMnSc3	svůj
otci	otec	k1gMnSc3	otec
podrobit	podrobit	k5eAaPmF	podrobit
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
po	po	k7c6	po
roce	rok	k1gInSc6	rok
860	[number]	k4	860
vyslal	vyslat	k5eAaPmAgMnS	vyslat
Rostislav	Rostislav	k1gMnSc1	Rostislav
poselstvo	poselstvo	k1gNnSc4	poselstvo
k	k	k7c3	k
papeži	papež	k1gMnSc3	papež
Mikuláši	Mikuláš	k1gMnSc3	Mikuláš
I.	I.	kA	I.
a	a	k8xC	a
požádal	požádat	k5eAaPmAgMnS	požádat
jej	on	k3xPp3gMnSc4	on
o	o	k7c4	o
"	"	kIx"	"
<g/>
učitele	učitel	k1gMnPc4	učitel
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
zajistit	zajistit	k5eAaPmF	zajistit
prohloubení	prohloubení	k1gNnSc4	prohloubení
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
víry	víra	k1gFnSc2	víra
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
zajistit	zajistit	k5eAaPmF	zajistit
vzdělání	vzdělání	k1gNnSc4	vzdělání
dostatečného	dostatečný	k2eAgNnSc2d1	dostatečné
množství	množství	k1gNnSc2	množství
domácích	domácí	k2eAgMnPc2d1	domácí
kněží	kněz	k1gMnPc2	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Využil	využít	k5eAaPmAgInS	využít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
po	po	k7c6	po
záchvatu	záchvat	k1gInSc6	záchvat
mrtvice	mrtvice	k1gFnSc2	mrtvice
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
pasovského	pasovský	k2eAgInSc2d1	pasovský
biskupa	biskup	k1gInSc2	biskup
Hartwiga	Hartwiga	k1gFnSc1	Hartwiga
upadla	upadnout	k5eAaPmAgFnS	upadnout
celá	celý	k2eAgFnSc1d1	celá
pasovská	pasovský	k2eAgFnSc1d1	pasovská
diecéze	diecéze	k1gFnSc1	diecéze
včetně	včetně	k7c2	včetně
misijních	misijní	k2eAgNnPc2d1	misijní
území	území	k1gNnPc2	území
do	do	k7c2	do
chaosu	chaos	k1gInSc2	chaos
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
vyslání	vyslání	k1gNnSc1	vyslání
učitele	učitel	k1gMnSc2	učitel
se	se	k3xPyFc4	se
však	však	k9	však
mělo	mít	k5eAaImAgNnS	mít
stát	stát	k5eAaImF	stát
jen	jen	k6eAd1	jen
prvním	první	k4xOgInSc7	první
krokem	krok	k1gInSc7	krok
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
samostatné	samostatný	k2eAgFnSc2d1	samostatná
církevní	církevní	k2eAgFnSc2d1	církevní
organizace	organizace	k1gFnSc2	organizace
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
však	však	k9	však
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
spojenectví	spojenectví	k1gNnSc4	spojenectví
císaře	císař	k1gMnSc2	císař
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Němce	Němec	k1gMnSc2	Němec
a	a	k8xC	a
nedovolil	dovolit	k5eNaPmAgMnS	dovolit
si	se	k3xPyFc3	se
podniknout	podniknout	k5eAaPmF	podniknout
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
poškodit	poškodit	k5eAaPmF	poškodit
bavorský	bavorský	k2eAgInSc4d1	bavorský
episkopát	episkopát	k1gInSc4	episkopát
a	a	k8xC	a
popudit	popudit	k5eAaPmF	popudit
Ludvíka	Ludvík	k1gMnSc4	Ludvík
Němce	Němec	k1gMnSc4	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
Rostislavovu	Rostislavův	k2eAgFnSc4d1	Rostislavova
žádost	žádost	k1gFnSc4	žádost
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
negativní	negativní	k2eAgFnSc1d1	negativní
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěchu	neúspěch	k1gInSc6	neúspěch
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
vyslal	vyslat	k5eAaPmAgMnS	vyslat
Rostislav	Rostislav	k1gMnSc1	Rostislav
obdobné	obdobný	k2eAgNnSc4d1	obdobné
poselstvo	poselstvo	k1gNnSc4	poselstvo
k	k	k7c3	k
byzantskému	byzantský	k2eAgMnSc3d1	byzantský
císaři	císař	k1gMnSc3	císař
Michalovi	Michal	k1gMnSc3	Michal
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
jeho	jeho	k3xOp3gFnSc4	jeho
žádosti	žádost	k1gFnSc6	žádost
vyhověl	vyhovět	k5eAaPmAgInS	vyhovět
a	a	k8xC	a
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
vyslal	vyslat	k5eAaPmAgMnS	vyslat
misii	misie	k1gFnSc4	misie
vedenou	vedený	k2eAgFnSc4d1	vedená
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
tehdy	tehdy	k6eAd1	tehdy
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
byzantských	byzantský	k2eAgMnPc2d1	byzantský
učenců	učenec	k1gMnPc2	učenec
Konstantinem	Konstantin	k1gMnSc7	Konstantin
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
bratrem	bratr	k1gMnSc7	bratr
Metodějem	Metoděj	k1gMnSc7	Metoděj
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
z	z	k7c2	z
Byzance	Byzanc	k1gFnSc2	Byzanc
nešlo	jít	k5eNaImAgNnS	jít
očekávat	očekávat	k5eAaImF	očekávat
založení	založení	k1gNnSc1	založení
(	(	kIx(	(
<g/>
arci	arci	k0	arci
<g/>
)	)	kIx)	)
<g/>
biskupství	biskupství	k1gNnSc1	biskupství
<g/>
,	,	kIx,	,
několikaletá	několikaletý	k2eAgFnSc1d1	několikaletá
intenzivní	intenzivní	k2eAgFnSc1d1	intenzivní
misijní	misijní	k2eAgFnSc1d1	misijní
činnost	činnost	k1gFnSc1	činnost
Konstantina	Konstantin	k1gMnSc2	Konstantin
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
položila	položit	k5eAaPmAgFnS	položit
nezbytné	zbytný	k2eNgInPc4d1	zbytný
základy	základ	k1gInPc4	základ
pro	pro	k7c4	pro
následné	následný	k2eAgNnSc4d1	následné
budování	budování	k1gNnSc4	budování
samostatné	samostatný	k2eAgNnSc4d1	samostatné
církevní	církevní	k2eAgFnPc4d1	církevní
organizace	organizace	k1gFnPc4	organizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
roku	rok	k1gInSc2	rok
864	[number]	k4	864
Ludvík	Ludvík	k1gMnSc1	Ludvík
Němec	Němec	k1gMnSc1	Němec
překročil	překročit	k5eAaPmAgMnS	překročit
Dunaj	Dunaj	k1gInSc4	Dunaj
a	a	k8xC	a
napadl	napadnout	k5eAaPmAgMnS	napadnout
Velkomoravskou	velkomoravský	k2eAgFnSc4d1	Velkomoravská
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
Obklíčil	obklíčit	k5eAaPmAgMnS	obklíčit
Rostislava	Rostislav	k1gMnSc4	Rostislav
na	na	k7c6	na
hradišti	hradiště	k1gNnSc6	hradiště
Dowina	Dowino	k1gNnSc2	Dowino
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Devín	Devín	k1gInSc1	Devín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rostislav	Rostislav	k1gMnSc1	Rostislav
byl	být	k5eAaImAgMnS	být
přinucen	přinutit	k5eAaPmNgMnS	přinutit
uznat	uznat	k5eAaPmF	uznat
své	svůj	k3xOyFgNnSc4	svůj
vazalství	vazalství	k1gNnSc4	vazalství
vůči	vůči	k7c3	vůči
východofranské	východofranský	k2eAgFnSc3d1	Východofranská
říši	říš	k1gFnSc3	říš
a	a	k8xC	a
umožnit	umožnit	k5eAaPmF	umožnit
návrat	návrat	k1gInSc1	návrat
latinským	latinský	k2eAgMnPc3d1	latinský
kněžím	kněz	k1gMnPc3	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Cyrilometodějská	cyrilometodějský	k2eAgFnSc1d1	Cyrilometodějská
misie	misie	k1gFnSc1	misie
však	však	k9	však
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
nerušeně	nerušeně	k6eAd1	nerušeně
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
Němec	Němec	k1gMnSc1	Němec
nespokojen	spokojit	k5eNaPmNgMnS	spokojit
s	s	k7c7	s
politickou	politický	k2eAgFnSc7d1	politická
situací	situace	k1gFnSc7	situace
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
nepříznivou	příznivý	k2eNgFnSc4d1	nepříznivá
v	v	k7c6	v
roce	rok	k1gInSc6	rok
869	[number]	k4	869
zaútočil	zaútočit	k5eAaPmAgInS	zaútočit
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
až	až	k9	až
k	k	k7c3	k
Rostislavově	Rostislavův	k2eAgFnSc3d1	Rostislavova
hlavní	hlavní	k2eAgFnSc3d1	hlavní
pevnosti	pevnost	k1gFnSc3	pevnost
a	a	k8xC	a
opět	opět	k6eAd1	opět
ji	on	k3xPp3gFnSc4	on
nedobyl	dobýt	k5eNaPmAgMnS	dobýt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
čase	čas	k1gInSc6	čas
papež	papež	k1gMnSc1	papež
uznal	uznat	k5eAaPmAgMnS	uznat
sv.	sv.	kA	sv.
Metoděje	Metoděj	k1gMnSc4	Metoděj
za	za	k7c2	za
moravsko-panonského	moravskoanonský	k2eAgInSc2d1	moravsko-panonský
biskupa	biskup	k1gInSc2	biskup
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
celé	celý	k2eAgNnSc4d1	celé
úsilí	úsilí	k1gNnSc4	úsilí
misie	misie	k1gFnSc2	misie
i	i	k8xC	i
svébytné	svébytný	k2eAgNnSc1d1	svébytné
kulturně-politické	kulturněolitický	k2eAgNnSc1d1	kulturně-politické
postavení	postavení	k1gNnSc1	postavení
Velkomoravské	velkomoravský	k2eAgFnSc2d1	Velkomoravská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
na	na	k7c6	na
Východofranské	východofranský	k2eAgFnSc6d1	Východofranská
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Rostislav	Rostislav	k1gMnSc1	Rostislav
pověřil	pověřit	k5eAaPmAgMnS	pověřit
panováním	panování	k1gNnSc7	panování
nad	nad	k7c7	nad
nitranským	nitranský	k2eAgNnSc7d1	Nitranské
knížectvím	knížectví	k1gNnSc7	knížectví
(	(	kIx(	(
<g/>
samosprávná	samosprávný	k2eAgFnSc1d1	samosprávná
lenní	lenní	k2eAgFnSc1d1	lenní
součást	součást	k1gFnSc1	součást
Velkomoravské	velkomoravský	k2eAgFnSc2d1	Velkomoravská
říše	říš	k1gFnSc2	říš
<g/>
)	)	kIx)	)
svého	svůj	k3xOyFgMnSc4	svůj
synovce	synovec	k1gMnSc4	synovec
Svatopluka	Svatopluk	k1gMnSc4	Svatopluk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
tak	tak	k9	tak
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Rostislavem	Rostislav	k1gMnSc7	Rostislav
začal	začít	k5eAaPmAgMnS	začít
podílet	podílet	k5eAaImF	podílet
na	na	k7c6	na
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
870	[number]	k4	870
se	se	k3xPyFc4	se
ale	ale	k9	ale
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
východofranskou	východofranský	k2eAgFnSc7d1	Východofranská
říší	říš	k1gFnSc7	říš
proti	proti	k7c3	proti
Rostislavovi	Rostislav	k1gMnSc3	Rostislav
a	a	k8xC	a
své	svůj	k3xOyFgNnSc4	svůj
nitranské	nitranský	k2eAgNnSc4d1	Nitranské
knížectví	knížectví	k1gNnSc4	knížectví
svěřil	svěřit	k5eAaPmAgMnS	svěřit
pod	pod	k7c4	pod
ochranu	ochrana	k1gFnSc4	ochrana
Ludvíku	Ludvík	k1gMnSc3	Ludvík
Němci	Němec	k1gMnSc3	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Rostislav	Rostislav	k1gMnSc1	Rostislav
reagoval	reagovat	k5eAaBmAgInS	reagovat
pokusem	pokus	k1gInSc7	pokus
o	o	k7c4	o
zabití	zabití	k1gNnSc4	zabití
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
na	na	k7c6	na
hostině	hostina	k1gFnSc6	hostina
<g/>
.	.	kIx.	.
</s>
<s>
Rostislav	Rostislav	k1gMnSc1	Rostislav
byl	být	k5eAaImAgMnS	být
napřed	napřed	k6eAd1	napřed
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
"	"	kIx"	"
<g/>
soudem	soud	k1gInSc7	soud
Franků	Franky	k1gInPc2	Franky
<g/>
,	,	kIx,	,
Bavorů	Bavor	k1gMnPc2	Bavor
i	i	k8xC	i
Slovanů	Slovan	k1gMnPc2	Slovan
<g/>
"	"	kIx"	"
k	k	k7c3	k
trestu	trest	k1gInSc3	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Ludvík	Ludvík	k1gMnSc1	Ludvík
jej	on	k3xPp3gInSc4	on
nechal	nechat	k5eAaPmAgMnS	nechat
nakonec	nakonec	k6eAd1	nakonec
pouze	pouze	k6eAd1	pouze
oslepit	oslepit	k5eAaPmF	oslepit
a	a	k8xC	a
vykastrovat	vykastrovat	k5eAaPmF	vykastrovat
<g/>
.	.	kIx.	.
</s>
<s>
Ludvík	Ludvík	k1gMnSc1	Ludvík
ho	on	k3xPp3gMnSc4	on
pak	pak	k6eAd1	pak
nechal	nechat	k5eAaPmAgMnS	nechat
věznit	věznit	k5eAaImF	věznit
v	v	k7c6	v
Klášteře	klášter	k1gInSc6	klášter
sv.	sv.	kA	sv.
Jimráma	Jimrámum	k1gNnSc2	Jimrámum
(	(	kIx(	(
<g/>
sv.	sv.	kA	sv.
Emeramma	Emeramma	k1gFnSc1	Emeramma
<g/>
)	)	kIx)	)
v	v	k7c6	v
Řezně	Řezno	k1gNnSc6	Řezno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
někdy	někdy	k6eAd1	někdy
po	po	k7c6	po
roce	rok	k1gInSc6	rok
870	[number]	k4	870
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Rostislavově	Rostislavův	k2eAgFnSc6d1	Rostislavova
likvidaci	likvidace	k1gFnSc6	likvidace
započal	započnout	k5eAaPmAgInS	započnout
pak	pak	k6eAd1	pak
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
Moravě	Morava	k1gFnSc6	Morava
spor	spor	k1gInSc1	spor
o	o	k7c4	o
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
nárokoval	nárokovat	k5eAaImAgMnS	nárokovat
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
(	(	kIx(	(
<g/>
který	který	k3yQgMnSc1	který
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
nakrátko	nakrátko	k6eAd1	nakrátko
rovněž	rovněž	k9	rovněž
vězněn	vězněn	k2eAgInSc4d1	vězněn
Franky	Franky	k1gInPc4	Franky
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Ludvík	Ludvík	k1gMnSc1	Ludvík
Němec	Němec	k1gMnSc1	Němec
svěřil	svěřit	k5eAaPmAgMnS	svěřit
správu	správa	k1gFnSc4	správa
nad	nad	k7c7	nad
slovanským	slovanský	k2eAgInSc7d1	slovanský
státem	stát	k1gInSc7	stát
franským	franský	k2eAgInSc7d1	franský
markrabím	markrabí	k1gMnSc6	markrabí
Vilémovi	Vilém	k1gMnSc6	Vilém
a	a	k8xC	a
Engelšalkovi	Engelšalka	k1gMnSc6	Engelšalka
<g/>
.	.	kIx.	.
</s>
