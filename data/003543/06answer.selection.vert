<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Rostislav	Rostislav	k1gMnSc1	Rostislav
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
Rastislav	Rastislava	k1gFnPc2	Rastislava
<g/>
,	,	kIx,	,
Rastic	Rastice	k1gFnPc2	Rastice
<g/>
,	,	kIx,	,
Rasticlao	Rasticlao	k1gMnSc1	Rasticlao
<g/>
,	,	kIx,	,
Rastislaus	Rastislaus	k1gMnSc1	Rastislaus
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
Radislav	Radislav	k1gMnSc1	Radislav
či	či	k8xC	či
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
)	)	kIx)	)
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Mojmírovců	Mojmírovec	k1gMnPc2	Mojmírovec
(	(	kIx(	(
<g/>
datum	datum	k1gNnSc1	datum
narození	narození	k1gNnSc2	narození
neznámé	známý	k2eNgFnSc2d1	neznámá
–	–	k?	–
po	po	k7c6	po
870	[number]	k4	870
<g/>
,	,	kIx,	,
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
846	[number]	k4	846
<g/>
–	–	k?	–
<g/>
870	[number]	k4	870
druhým	druhý	k4xOgNnSc7	druhý
velkomoravským	velkomoravský	k2eAgNnSc7d1	velkomoravské
knížetem	kníže	k1gNnSc7wR	kníže
<g/>
.	.	kIx.	.
</s>
