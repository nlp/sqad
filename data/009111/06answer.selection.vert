<s>
Černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
klasické	klasický	k2eAgFnSc2d1	klasická
fyziky	fyzika	k1gFnSc2	fyzika
je	být	k5eAaImIp3nS	být
těleso	těleso	k1gNnSc1	těleso
velmi	velmi	k6eAd1	velmi
stálé	stálý	k2eAgNnSc1d1	stálé
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nemůže	moct	k5eNaImIp3nS	moct
zaniknout	zaniknout	k5eAaPmF	zaniknout
vlivem	vlivem	k7c2	vlivem
ztráty	ztráta	k1gFnSc2	ztráta
své	svůj	k3xOyFgFnSc2	svůj
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
žádná	žádný	k3yNgFnSc1	žádný
částice	částice	k1gFnSc1	částice
není	být	k5eNaImIp3nS	být
podle	podle	k7c2	podle
klasické	klasický	k2eAgFnSc2d1	klasická
fyziky	fyzika	k1gFnSc2	fyzika
schopna	schopen	k2eAgFnSc1d1	schopna
překonat	překonat	k5eAaPmF	překonat
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
jedinou	jediný	k2eAgFnSc7d1	jediná
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
cestou	cesta	k1gFnSc7	cesta
zániku	zánik	k1gInSc2	zánik
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
jeví	jevit	k5eAaImIp3nS	jevit
její	její	k3xOp3gNnSc4	její
pohlcení	pohlcení	k1gNnSc4	pohlcení
jinou	jiný	k2eAgFnSc7d1	jiná
černou	černý	k2eAgFnSc7d1	černá
dírou	díra	k1gFnSc7	díra
tzv.	tzv.	kA	tzv.
gravitační	gravitační	k2eAgFnSc1d1	gravitační
srážka	srážka	k1gFnSc1	srážka
<g/>
.	.	kIx.	.
</s>
