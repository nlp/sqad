<p>
<s>
Wahlbach	Wahlbach	k1gInSc1	Wahlbach
je	být	k5eAaImIp3nS	být
francouzská	francouzský	k2eAgFnSc1d1	francouzská
obec	obec	k1gFnSc1	obec
v	v	k7c6	v
departementu	departement	k1gInSc6	departement
Haut-Rhin	Haut-Rhina	k1gFnPc2	Haut-Rhina
v	v	k7c6	v
regionu	region	k1gInSc6	region
Grand	grand	k1gMnSc1	grand
Est	Est	k1gMnSc1	Est
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
488	[number]	k4	488
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poloha	poloha	k1gFnSc1	poloha
obce	obec	k1gFnSc2	obec
==	==	k?	==
</s>
</p>
<p>
<s>
Sousední	sousední	k2eAgFnPc1d1	sousední
obce	obec	k1gFnPc1	obec
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Franken	Franken	k1gInSc1	Franken
<g/>
,	,	kIx,	,
Hausgauen	Hausgauen	k1gInSc1	Hausgauen
<g/>
,	,	kIx,	,
Heiwiller	Heiwiller	k1gMnSc1	Heiwiller
<g/>
,	,	kIx,	,
Hundsbach	Hundsbach	k1gMnSc1	Hundsbach
<g/>
,	,	kIx,	,
Obermorschwiller	Obermorschwiller	k1gMnSc1	Obermorschwiller
<g/>
,	,	kIx,	,
Rantzwiller	Rantzwiller	k1gMnSc1	Rantzwiller
<g/>
,	,	kIx,	,
Steinbrunn-le-Haut	Steinbrunne-Haut	k1gMnSc1	Steinbrunn-le-Haut
a	a	k8xC	a
Zaessingue	Zaessingue	k1gFnSc1	Zaessingue
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
==	==	k?	==
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
departementu	departement	k1gInSc6	departement
Haut-Rhin	Haut-Rhina	k1gFnPc2	Haut-Rhina
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Wahlbach	Wahlbacha	k1gFnPc2	Wahlbacha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
obce	obec	k1gFnSc2	obec
</s>
</p>
