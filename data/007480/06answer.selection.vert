<s>
Lokalita	lokalita	k1gFnSc1	lokalita
Kamenný	kamenný	k2eAgInSc1d1	kamenný
Vrch	vrch	k1gInSc1	vrch
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
chráněnou	chráněný	k2eAgFnSc7d1	chráněná
přírodní	přírodní	k2eAgFnSc7d1	přírodní
památkou	památka	k1gFnSc7	památka
usnesením	usnesení	k1gNnSc7	usnesení
Národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
vyhláška	vyhláška	k1gFnSc1	vyhláška
ze	z	k7c2	z
dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1989	[number]	k4	1989
ji	on	k3xPp3gFnSc4	on
určila	určit	k5eAaPmAgFnS	určit
jako	jako	k9	jako
chráněný	chráněný	k2eAgInSc4d1	chráněný
přírodní	přírodní	k2eAgInSc4d1	přírodní
výtvor	výtvor	k1gInSc4	výtvor
<g/>
.	.	kIx.	.
</s>
