<p>
<s>
Hraniční	hraniční	k2eAgNnSc1d1	hraniční
pásmo	pásmo	k1gNnSc1	pásmo
XI	XI	kA	XI
bylo	být	k5eAaImAgNnS	být
vyšší	vysoký	k2eAgFnSc7d2	vyšší
jednotkou	jednotka	k1gFnSc7	jednotka
Československé	československý	k2eAgFnSc2d1	Československá
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
velikostí	velikost	k1gFnSc7	velikost
odpovídající	odpovídající	k2eAgMnSc1d1	odpovídající
zhruba	zhruba	k6eAd1	zhruba
sboru	sbor	k1gInSc2	sbor
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
všeobecné	všeobecný	k2eAgFnSc2d1	všeobecná
mobilizace	mobilizace	k1gFnSc2	mobilizace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
působící	působící	k2eAgFnSc2d1	působící
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
1	[number]	k4	1
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
bránící	bránící	k2eAgNnSc4d1	bránící
hlavní	hlavní	k2eAgNnSc4d1	hlavní
obranné	obranný	k2eAgNnSc4d1	obranné
pásmo	pásmo	k1gNnSc4	pásmo
v	v	k7c6	v
severozápadních	severozápadní	k2eAgFnPc6d1	severozápadní
Čechách	Čechy	k1gFnPc6	Čechy
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
od	od	k7c2	od
Toužimi	Touži	k1gFnPc7	Touži
po	po	k7c6	po
Rtyni	Rtyeň	k1gFnSc6	Rtyeň
nad	nad	k7c7	nad
Bílinou	Bílina	k1gFnSc7	Bílina
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
hlavního	hlavní	k2eAgNnSc2d1	hlavní
obranného	obranný	k2eAgNnSc2d1	obranné
postavení	postavení	k1gNnSc2	postavení
činila	činit	k5eAaImAgFnS	činit
114	[number]	k4	114
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velitelem	velitel	k1gMnSc7	velitel
Hraničního	hraniční	k2eAgNnSc2d1	hraniční
pásma	pásmo	k1gNnSc2	pásmo
XI	XI	kA	XI
byl	být	k5eAaImAgMnS	být
divizní	divizní	k2eAgMnSc1d1	divizní
generál	generál	k1gMnSc1	generál
Emil	Emil	k1gMnSc1	Emil
Václav	Václav	k1gMnSc1	Václav
Linhart	Linhart	k1gMnSc1	Linhart
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stanoviště	stanoviště	k1gNnSc1	stanoviště
velitele	velitel	k1gMnSc2	velitel
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
==	==	k?	==
Úkoly	úkol	k1gInPc7	úkol
Hraničního	hraniční	k2eAgNnSc2d1	hraniční
pásma	pásmo	k1gNnSc2	pásmo
XI	XI	kA	XI
==	==	k?	==
</s>
</p>
<p>
<s>
Úkolem	úkol	k1gInSc7	úkol
Hraničního	hraniční	k2eAgNnSc2d1	hraniční
pásma	pásmo	k1gNnSc2	pásmo
XI	XI	kA	XI
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
jen	jen	k9	jen
HP	HP	kA	HP
XI	XI	kA	XI
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
obrana	obrana	k1gFnSc1	obrana
hlavního	hlavní	k2eAgNnSc2d1	hlavní
obranného	obranný	k2eAgNnSc2d1	obranné
postavení	postavení	k1gNnSc2	postavení
v	v	k7c6	v
linii	linie	k1gFnSc6	linie
Klášterec	Klášterec	k1gInSc4	Klášterec
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
-	-	kIx~	-
Most-	Most-	k1gFnSc1	Most-
Bílina	Bílina	k1gFnSc1	Bílina
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
obranného	obranný	k2eAgNnSc2d1	obranné
postavení	postavení	k1gNnSc2	postavení
na	na	k7c6	na
Blšance	Blšanka	k1gFnSc6	Blšanka
a	a	k8xC	a
Ohři	Ohře	k1gFnSc6	Ohře
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
úkol	úkol	k1gInSc1	úkol
prvosledové	prvosledový	k2eAgFnSc2d1	prvosledová
Skupiny	skupina	k1gFnSc2	skupina
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
obranným	obranný	k2eAgNnSc7d1	obranné
postavením	postavení	k1gNnSc7	postavení
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
tzv.	tzv.	kA	tzv.
Pražská	pražský	k2eAgFnSc1d1	Pražská
čára	čára	k1gFnSc1	čára
<g/>
,	,	kIx,	,
obsazená	obsazený	k2eAgFnSc1d1	obsazená
jednotkami	jednotka	k1gFnPc7	jednotka
Velitelství	velitelství	k1gNnSc2	velitelství
okrsku	okrsek	k1gInSc2	okrsek
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
jednotka	jednotka	k1gFnSc1	jednotka
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
síle	síla	k1gFnSc6	síla
divize	divize	k1gFnSc2	divize
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
též	též	k9	též
do	do	k7c2	do
sestavy	sestava	k1gFnSc2	sestava
HP	HP	kA	HP
XI	XI	kA	XI
a	a	k8xC	a
bránící	bránící	k2eAgInPc4d1	bránící
bezprostřední	bezprostřední	k2eAgInPc4d1	bezprostřední
přístupy	přístup	k1gInPc4	přístup
k	k	k7c3	k
Praze	Praha	k1gFnSc3	Praha
od	od	k7c2	od
západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obranu	obrana	k1gFnSc4	obrana
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
mohla	moct	k5eAaImAgFnS	moct
také	také	k9	také
podpořit	podpořit	k5eAaPmF	podpořit
18	[number]	k4	18
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc2	divize
ze	z	k7c2	z
zálohy	záloha	k1gFnSc2	záloha
velitele	velitel	k1gMnSc2	velitel
1	[number]	k4	1
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Kladna	Kladno	k1gNnSc2	Kladno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podřízené	podřízený	k2eAgFnPc1d1	podřízená
jednotky	jednotka	k1gFnPc1	jednotka
==	==	k?	==
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgFnPc1d2	vyšší
jednotky	jednotka	k1gFnPc1	jednotka
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
1	[number]	k4	1
</s>
</p>
<p>
<s>
Velitelství	velitelství	k1gNnSc1	velitelství
okrsku	okrsek	k1gInSc2	okrsek
PrahaOstatní	PrahaOstatní	k2eAgFnSc2d1	PrahaOstatní
jednotky	jednotka	k1gFnSc2	jednotka
</s>
</p>
<p>
<s>
hraničářský	hraničářský	k2eAgInSc1d1	hraničářský
prapor	prapor	k1gInSc1	prapor
21	[number]	k4	21
</s>
</p>
<p>
<s>
hraničářský	hraničářský	k2eAgInSc1d1	hraničářský
prapor	prapor	k1gInSc1	prapor
25	[number]	k4	25
</s>
</p>
<p>
<s>
telegrafní	telegrafní	k2eAgInSc1d1	telegrafní
prapor	prapor	k1gInSc1	prapor
61	[number]	k4	61
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
