<p>
<s>
Čarodějnictví	čarodějnictví	k1gNnSc1	čarodějnictví
neboli	neboli	k8xC	neboli
čarodějství	čarodějství	k1gNnSc1	čarodějství
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
nadpřirozené	nadpřirozený	k2eAgNnSc4d1	nadpřirozené
působení	působení	k1gNnSc4	působení
známé	známý	k2eAgNnSc4d1	známé
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
světových	světový	k2eAgFnPc6d1	světová
kulturách	kultura	k1gFnPc6	kultura
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
však	však	k9	však
nemá	mít	k5eNaImIp3nS	mít
žádnou	žádný	k3yNgFnSc4	žádný
obecně	obecně	k6eAd1	obecně
přijímanou	přijímaný	k2eAgFnSc4d1	přijímaná
definici	definice	k1gFnSc4	definice
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
chápáno	chápat	k5eAaImNgNnS	chápat
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
magie	magie	k1gFnSc2	magie
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
oddělováno	oddělován	k2eAgNnSc1d1	oddělováno
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
méně	málo	k6eAd2	málo
intelektuální	intelektuální	k2eAgMnSc1d1	intelektuální
a	a	k8xC	a
více	hodně	k6eAd2	hodně
intuitivní	intuitivní	k2eAgFnSc1d1	intuitivní
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
kultur	kultura	k1gFnPc2	kultura
je	být	k5eAaImIp3nS	být
spojováno	spojovat	k5eAaImNgNnS	spojovat
především	především	k9	především
s	s	k7c7	s
ženami	žena	k1gFnPc7	žena
a	a	k8xC	a
také	také	k9	také
s	s	k7c7	s
působením	působení	k1gNnSc7	působení
zla	zlo	k1gNnSc2	zlo
pomocí	pomoc	k1gFnSc7	pomoc
magie	magie	k1gFnSc2	magie
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
pronásledování	pronásledování	k1gNnSc3	pronásledování
skutečných	skutečný	k2eAgMnPc2d1	skutečný
či	či	k8xC	či
domnělých	domnělý	k2eAgMnPc2d1	domnělý
praktikantů	praktikant	k1gMnPc2	praktikant
čarodějnictví	čarodějnictví	k1gNnPc2	čarodějnictví
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
raně	raně	k6eAd1	raně
novověké	novověký	k2eAgFnSc6d1	novověká
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
však	však	k9	však
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
subsaharské	subsaharský	k2eAgFnSc2d1	subsaharská
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osoby	osoba	k1gFnSc2	osoba
provádějící	provádějící	k2eAgNnSc4d1	provádějící
pozitivní	pozitivní	k2eAgNnSc4d1	pozitivní
čarodějnictví	čarodějnictví	k1gNnSc4	čarodějnictví
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
čarodějové	čaroděj	k1gMnPc1	čaroděj
a	a	k8xC	a
čarodějky	čarodějka	k1gFnPc1	čarodějka
<g/>
;	;	kIx,	;
negativní	negativní	k2eAgInSc4d1	negativní
pak	pak	k8xC	pak
černokněžníci	černokněžník	k1gMnPc1	černokněžník
a	a	k8xC	a
čarodějnice	čarodějnice	k1gFnPc1	čarodějnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
nejčastějším	častý	k2eAgFnPc3d3	nejčastější
schopnostem	schopnost	k1gFnPc3	schopnost
přisuzovaným	přisuzovaný	k2eAgFnPc3d1	přisuzovaná
čarodějnicím	čarodějnice	k1gFnPc3	čarodějnice
patří	patřit	k5eAaImIp3nS	patřit
záškodná	záškodný	k2eAgFnSc1d1	záškodná
magie	magie	k1gFnSc1	magie
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
kletby	kletba	k1gFnPc1	kletba
nebo	nebo	k8xC	nebo
uhranutí	uhranutí	k1gNnSc1	uhranutí
<g/>
,	,	kIx,	,
milostná	milostný	k2eAgFnSc1d1	milostná
magie	magie	k1gFnSc1	magie
a	a	k8xC	a
věštění	věštění	k1gNnSc1	věštění
–	–	k?	–
včetně	včetně	k7c2	včetně
nekromantie	nekromantie	k1gFnSc2	nekromantie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Formy	forma	k1gFnPc1	forma
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
čarodějnictví	čarodějnictví	k1gNnSc1	čarodějnictví
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
několik	několik	k4yIc1	několik
fenoménů	fenomén	k1gInPc2	fenomén
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k6eAd1	především
následující	následující	k2eAgInSc1d1	následující
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Prvním	první	k4xOgMnSc7	první
je	být	k5eAaImIp3nS	být
nadpřirozené	nadpřirozený	k2eAgNnSc1d1	nadpřirozené
působení	působení	k1gNnSc1	působení
neštěstí	neštěstí	k1gNnSc2	neštěstí
<g/>
,	,	kIx,	,
víra	víra	k1gFnSc1	víra
v	v	k7c4	v
něj	on	k3xPp3gMnSc4	on
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
antropologů	antropolog	k1gMnPc2	antropolog
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
lidské	lidský	k2eAgNnSc4d1	lidské
neštěstí	neštěstí	k1gNnSc4	neštěstí
a	a	k8xC	a
regulovat	regulovat	k5eAaImF	regulovat
konflikty	konflikt	k1gInPc4	konflikt
v	v	k7c6	v
komunitě	komunita	k1gFnSc6	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pojetí	pojetí	k1gNnSc1	pojetí
čarodějnictví	čarodějnictví	k1gNnSc2	čarodějnictví
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
proč	proč	k6eAd1	proč
byly	být	k5eAaImAgFnP	být
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
čarodějnice	čarodějnice	k1gFnPc4	čarodějnice
pronásledovány	pronásledován	k2eAgFnPc4d1	pronásledována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
skupiny	skupina	k1gFnSc2	skupina
lze	lze	k6eAd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
praktikanty	praktikant	k1gMnPc4	praktikant
specifických	specifický	k2eAgInPc2d1	specifický
druhů	druh	k1gInPc2	druh
magie	magie	k1gFnSc2	magie
jako	jako	k8xS	jako
byly	být	k5eAaImAgFnP	být
thessálské	thessálský	k2eAgFnPc4d1	thessálský
čarodějnice	čarodějnice	k1gFnPc4	čarodějnice
v	v	k7c6	v
antickém	antický	k2eAgNnSc6d1	antické
Řecku	Řecko	k1gNnSc6	Řecko
nebo	nebo	k8xC	nebo
severské	severský	k2eAgFnPc4d1	severská
čarodějnice	čarodějnice	k1gFnPc4	čarodějnice
praktikující	praktikující	k2eAgFnSc1d1	praktikující
seidr	seidr	k1gInSc1	seidr
<g/>
.	.	kIx.	.
</s>
<s>
Jistou	jistý	k2eAgFnSc4d1	jistá
podobnost	podobnost	k1gFnSc4	podobnost
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
i	i	k9	i
extatické	extatický	k2eAgInPc1d1	extatický
kulty	kult	k1gInPc1	kult
jako	jako	k9	jako
byly	být	k5eAaImAgInP	být
italští	italský	k2eAgMnPc1d1	italský
benandanti	benandant	k1gMnPc1	benandant
obvinění	obviněný	k2eAgMnPc1d1	obviněný
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
inkvizicí	inkvizice	k1gFnPc2	inkvizice
z	z	k7c2	z
čarodějnictví	čarodějnictví	k1gNnSc2	čarodějnictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
čarodějnictví	čarodějnictví	k1gNnSc2	čarodějnictví
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
také	také	k9	také
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
lidové	lidový	k2eAgFnSc6d1	lidová
kultuře	kultura	k1gFnSc6	kultura
a	a	k8xC	a
v	v	k7c6	v
několika	několik	k4yIc6	několik
formách	forma	k1gFnPc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
čarodějnictví	čarodějnictví	k1gNnSc2	čarodějnictví
označována	označovat	k5eAaImNgFnS	označovat
například	například	k6eAd1	například
lidová	lidový	k2eAgFnSc1d1	lidová
magie	magie	k1gFnSc1	magie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
praktikanti	praktikant	k1gMnPc1	praktikant
bývají	bývat	k5eAaImIp3nP	bývat
tradičně	tradičně	k6eAd1	tradičně
nazýváni	nazýván	k2eAgMnPc1d1	nazýván
mudrci	mudrc	k1gMnPc1	mudrc
a	a	k8xC	a
vědmy	vědma	k1gFnPc1	vědma
<g/>
,	,	kIx,	,
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
jako	jako	k9	jako
cunning	cunning	k1gInSc1	cunning
folk	folk	k1gInSc1	folk
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
mazaní	mazaný	k2eAgMnPc1d1	mazaný
lidé	člověk	k1gMnPc1	člověk
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
wise	wise	k1gInSc1	wise
men	men	k?	men
<g/>
/	/	kIx~	/
<g/>
women	women	k1gInSc1	women
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
moudří	moudrý	k2eAgMnPc1d1	moudrý
muži	muž	k1gMnPc1	muž
<g/>
/	/	kIx~	/
<g/>
ženy	žena	k1gFnPc1	žena
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
existují	existovat	k5eAaImIp3nP	existovat
čarodějnice	čarodějnice	k1gFnPc1	čarodějnice
mýtické	mýtický	k2eAgFnPc1d1	mýtická
<g/>
,	,	kIx,	,
v	v	k7c6	v
slovanském	slovanský	k2eAgNnSc6d1	slovanské
prostředí	prostředí	k1gNnSc6	prostředí
je	být	k5eAaImIp3nS	být
typickou	typický	k2eAgFnSc7d1	typická
představitelkou	představitelka	k1gFnSc7	představitelka
ježibaba	ježibaba	k1gFnSc1	ježibaba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
typem	typ	k1gInSc7	typ
je	být	k5eAaImIp3nS	být
moderní	moderní	k2eAgNnSc1d1	moderní
čarodějnictví	čarodějnictví	k1gNnSc1	čarodějnictví
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
kult	kult	k1gInSc1	kult
Wicca	Wiccum	k1gNnSc2	Wiccum
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
pohanských	pohanský	k2eAgNnPc2d1	pohanské
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
,	,	kIx,	,
esoterismu	esoterismus	k1gInSc2	esoterismus
a	a	k8xC	a
lidové	lidový	k2eAgFnSc2d1	lidová
magie	magie	k1gFnSc2	magie
<g/>
.	.	kIx.	.
</s>
<s>
Samo	sám	k3xTgNnSc1	sám
se	se	k3xPyFc4	se
chápe	chápat	k5eAaImIp3nS	chápat
jako	jako	k8xS	jako
náboženství	náboženství	k1gNnSc2	náboženství
kladoucí	kladoucí	k2eAgInSc4d1	kladoucí
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
praktikování	praktikování	k1gNnSc4	praktikování
magie	magie	k1gFnSc2	magie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podle	podle	k7c2	podle
regionu	region	k1gInSc2	region
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Evropa	Evropa	k1gFnSc1	Evropa
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Antika	antika	k1gFnSc1	antika
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
antickém	antický	k2eAgNnSc6d1	antické
Řecku	Řecko	k1gNnSc6	Řecko
existoval	existovat	k5eAaImAgInS	existovat
na	na	k7c4	na
čarodějnice	čarodějnice	k1gFnPc4	čarodějnice
ambivalentní	ambivalentní	k2eAgInSc1d1	ambivalentní
pohled	pohled	k1gInSc1	pohled
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
jim	on	k3xPp3gMnPc3	on
přisuzovány	přisuzován	k2eAgInPc1d1	přisuzován
zlé	zlý	k2eAgInPc1d1	zlý
úmysly	úmysl	k1gInPc1	úmysl
<g/>
,	,	kIx,	,
sexuální	sexuální	k2eAgFnSc1d1	sexuální
nenasytnost	nenasytnost	k1gFnSc1	nenasytnost
a	a	k8xC	a
ohrožování	ohrožování	k1gNnSc1	ohrožování
společenského	společenský	k2eAgInSc2d1	společenský
řadu	řad	k1gInSc2	řad
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
však	však	k9	však
byly	být	k5eAaImAgInP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
krásné	krásný	k2eAgInPc4d1	krásný
<g/>
,	,	kIx,	,
mocné	mocný	k2eAgInPc4d1	mocný
a	a	k8xC	a
laskavé	laskavý	k2eAgInPc4d1	laskavý
<g/>
.	.	kIx.	.
</s>
<s>
Magie	magie	k1gFnSc1	magie
provozovaná	provozovaný	k2eAgFnSc1d1	provozovaná
lidovými	lidový	k2eAgFnPc7d1	lidová
vrstvami	vrstva	k1gFnPc7	vrstva
byla	být	k5eAaImAgFnS	být
nazývána	nazýván	k2eAgFnSc1d1	nazývána
goetia	goetia	k1gFnSc1	goetia
a	a	k8xC	a
patřili	patřit	k5eAaImAgMnP	patřit
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
praktiky	praktika	k1gFnSc2	praktika
tradičně	tradičně	k6eAd1	tradičně
spojované	spojovaný	k2eAgInPc1d1	spojovaný
s	s	k7c7	s
čarodějnictvím	čarodějnictví	k1gNnSc7	čarodějnictví
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
kletby	kletba	k1gFnPc4	kletba
nebo	nebo	k8xC	nebo
milostná	milostný	k2eAgNnPc4d1	milostné
kouzla	kouzlo	k1gNnPc4	kouzlo
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
starém	starý	k2eAgNnSc6d1	staré
Řecku	Řecko	k1gNnSc6	Řecko
existovalo	existovat	k5eAaImAgNnS	existovat
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
pronásledování	pronásledování	k1gNnSc2	pronásledování
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
byla	být	k5eAaImAgFnS	být
v	v	k7c4	v
5	[number]	k4	5
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
upálena	upálen	k2eAgFnSc1d1	upálena
žena	žena	k1gFnSc1	žena
za	za	k7c4	za
praktikování	praktikování	k1gNnSc4	praktikování
nekromantie	nekromantie	k1gFnSc2	nekromantie
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějšími	slavný	k2eAgFnPc7d3	nejslavnější
řeckými	řecký	k2eAgFnPc7d1	řecká
čarodějnicemi	čarodějnice	k1gFnPc7	čarodějnice
byly	být	k5eAaImAgFnP	být
thesálské	thesálský	k2eAgFnPc4d1	thesálský
čarodějnice	čarodějnice	k1gFnPc4	čarodějnice
uctívající	uctívající	k2eAgFnSc3d1	uctívající
bohyni	bohyně	k1gFnSc3	bohyně
Hekaté	Hekatý	k2eAgFnSc6d1	Hekatý
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yQgFnPc6	který
se	se	k3xPyFc4	se
tradovalo	tradovat	k5eAaImAgNnS	tradovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokáží	dokázat	k5eAaPmIp3nP	dokázat
z	z	k7c2	z
nebe	nebe	k1gNnSc2	nebe
stáhnout	stáhnout	k5eAaPmF	stáhnout
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
řeckých	řecký	k2eAgInPc6d1	řecký
mýtech	mýtus	k1gInPc6	mýtus
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
mocné	mocný	k2eAgFnPc1d1	mocná
čarodějky	čarodějka	k1gFnPc1	čarodějka
jako	jako	k8xC	jako
Kirké	Kirká	k1gFnPc1	Kirká
nebo	nebo	k8xC	nebo
kolchidská	kolchidský	k2eAgFnSc1d1	Kolchidská
princezna	princezna	k1gFnSc1	princezna
Médeia	Médeia	k1gFnSc1	Médeia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výrazně	výrazně	k6eAd1	výrazně
negativní	negativní	k2eAgInSc4d1	negativní
názor	názor	k1gInSc4	názor
na	na	k7c4	na
čarodějnictví	čarodějnictví	k1gNnSc4	čarodějnictví
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
na	na	k7c4	na
záškodnou	záškodný	k2eAgFnSc4d1	záškodná
a	a	k8xC	a
neoficiální	oficiální	k2eNgFnSc4d1	neoficiální
magii	magie	k1gFnSc4	magie
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
Římané	Říman	k1gMnPc1	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
450	[number]	k4	450
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
zákon	zákon	k1gInSc1	zákon
proti	proti	k7c3	proti
venefica	venefic	k1gInSc2	venefic
<g/>
,	,	kIx,	,
škodlivé	škodlivý	k2eAgFnSc3d1	škodlivá
magii	magie	k1gFnSc3	magie
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
praktikanti	praktikant	k1gMnPc1	praktikant
magie	magie	k1gFnSc2	magie
dokonce	dokonce	k9	dokonce
hromadně	hromadně	k6eAd1	hromadně
pronásledováni	pronásledovat	k5eAaImNgMnP	pronásledovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
čarodějnictví	čarodějnictví	k1gNnSc2	čarodějnictví
obžalován	obžalovat	k5eAaPmNgMnS	obžalovat
slavný	slavný	k2eAgMnSc1d1	slavný
římský	římský	k2eAgMnSc1d1	římský
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
filozof	filozof	k1gMnSc1	filozof
Lucius	Lucius	k1gMnSc1	Lucius
Apuleius	Apuleius	k1gMnSc1	Apuleius
<g/>
,	,	kIx,	,
před	před	k7c7	před
soudním	soudní	k2eAgInSc7d1	soudní
tribunálem	tribunál	k1gInSc7	tribunál
se	se	k3xPyFc4	se
však	však	k9	však
dokázal	dokázat	k5eAaPmAgMnS	dokázat
obhájit	obhájit	k5eAaPmF	obhájit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Diokleciána	Dioklecián	k1gMnSc2	Dioklecián
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
n.	n.	k?	n.
l.	l.	k?	l.
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
za	za	k7c4	za
čarodějnictví	čarodějnictví	k1gNnPc4	čarodějnictví
ustanoven	ustanoven	k2eAgInSc4d1	ustanoven
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
upálením	upálení	k1gNnSc7	upálení
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
opatření	opatření	k1gNnPc1	opatření
byla	být	k5eAaImAgNnP	být
způsobena	způsobit	k5eAaPmNgNnP	způsobit
strachem	strach	k1gInSc7	strach
elit	elita	k1gFnPc2	elita
z	z	k7c2	z
magie	magie	k1gFnSc2	magie
<g/>
,	,	kIx,	,
neměla	mít	k5eNaImAgFnS	mít
však	však	k9	však
žádný	žádný	k3yNgInSc1	žádný
náboženský	náboženský	k2eAgInSc1d1	náboženský
základ	základ	k1gInSc1	základ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Severní	severní	k2eAgFnSc1d1	severní
Evropa	Evropa	k1gFnSc1	Evropa
====	====	k?	====
</s>
</p>
<p>
<s>
Vikingům	Viking	k1gMnPc3	Viking
byla	být	k5eAaImAgFnS	být
známa	znám	k2eAgFnSc1d1	známa
magie	magie	k1gFnSc1	magie
zvaná	zvaný	k2eAgFnSc1d1	zvaná
seidr	seidr	k1gInSc1	seidr
<g/>
,	,	kIx,	,
praktikovaná	praktikovaný	k2eAgFnSc1d1	praktikovaná
především	především	k9	především
ženami	žena	k1gFnPc7	žena
a	a	k8xC	a
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
praktikantky	praktikantka	k1gFnPc1	praktikantka
se	se	k3xPyFc4	se
nazývaly	nazývat	k5eAaImAgInP	nazývat
valy	val	k1gInPc1	val
či	či	k8xC	či
völvy	völv	k1gInPc1	völv
a	a	k8xC	a
mýty	mýtus	k1gInPc1	mýtus
a	a	k8xC	a
pověsti	pověst	k1gFnPc1	pověst
o	o	k7c6	o
nich	on	k3xPp3gInPc6	on
mluví	mluvit	k5eAaImIp3nS	mluvit
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
příznivě	příznivě	k6eAd1	příznivě
<g/>
.	.	kIx.	.
</s>
<s>
Věřilo	věřit	k5eAaImAgNnS	věřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
sedlají	sedlat	k5eAaImIp3nP	sedlat
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vlky	vlk	k1gMnPc4	vlk
a	a	k8xC	a
medvědy	medvěd	k1gMnPc4	medvěd
<g/>
,	,	kIx,	,
či	či	k8xC	či
dřevěné	dřevěný	k2eAgFnPc4d1	dřevěná
hole	hole	k1gFnPc4	hole
a	a	k8xC	a
jezdí	jezdit	k5eAaImIp3nP	jezdit
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
na	na	k7c4	na
svá	svůj	k3xOyFgNnPc4	svůj
setkání	setkání	k1gNnPc4	setkání
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
seidrem	seidr	k1gInSc7	seidr
byla	být	k5eAaImAgFnS	být
spojována	spojovat	k5eAaImNgFnS	spojovat
především	především	k9	především
bohyně	bohyně	k1gFnSc1	bohyně
Freya	Freya	k1gFnSc1	Freya
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
seznámila	seznámit	k5eAaPmAgFnS	seznámit
ostatní	ostatní	k2eAgMnPc4d1	ostatní
bohy	bůh	k1gMnPc4	bůh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
Slovanů	Slovan	k1gMnPc2	Slovan
byl	být	k5eAaImAgMnS	být
praktikant	praktikant	k1gMnSc1	praktikant
záškodné	záškodný	k2eAgFnSc2d1	záškodná
magie	magie	k1gFnSc2	magie
nazýván	nazývat	k5eAaImNgInS	nazývat
koldun	koldun	k1gInSc1	koldun
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ženy	žena	k1gFnSc2	žena
kolduňa	kolduňa	k6eAd1	kolduňa
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ostatní	ostatní	k2eAgMnPc1d1	ostatní
uživatelé	uživatel	k1gMnPc1	uživatel
magie	magie	k1gFnSc2	magie
byli	být	k5eAaImAgMnP	být
nazýváni	nazýván	k2eAgMnPc1d1	nazýván
vedun	veduna	k1gFnPc2	veduna
<g/>
,	,	kIx,	,
vědma	vědma	k1gFnSc1	vědma
a	a	k8xC	a
mnoha	mnoho	k4c7	mnoho
dalšími	další	k2eAgInPc7d1	další
výrazy	výraz	k1gInPc7	výraz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1071	[number]	k4	1071
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
pohanské	pohanský	k2eAgFnSc6d1	pohanská
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
Rostově	Rostův	k2eAgFnSc6d1	Rostova
dvěma	dva	k4xCgMnPc7	dva
kouzelníky	kouzelník	k1gMnPc7	kouzelník
z	z	k7c2	z
neúrody	neúroda	k1gFnSc2	neúroda
obviněny	obviněn	k2eAgFnPc4d1	obviněna
čarodějnice	čarodějnice	k1gFnPc4	čarodějnice
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
činnost	činnost	k1gFnSc1	činnost
nakonec	nakonec	k6eAd1	nakonec
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
úmrtí	úmrtí	k1gNnSc2	úmrtí
mnoha	mnoho	k4c2	mnoho
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
<g/>
Jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
chápáno	chápat	k5eAaImNgNnS	chápat
čarodějnictví	čarodějnictví	k1gNnSc4	čarodějnictví
u	u	k7c2	u
Keltů	Kelt	k1gMnPc2	Kelt
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
Tacitus	Tacitus	k1gInSc1	Tacitus
ovšem	ovšem	k9	ovšem
popisuje	popisovat	k5eAaImIp3nS	popisovat
ženy	žena	k1gFnPc4	žena
v	v	k7c6	v
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
černých	černý	k2eAgFnPc6d1	černá
řízách	říza	k1gFnPc6	říza
s	s	k7c7	s
pochodněmi	pochodeň	k1gFnPc7	pochodeň
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
společně	společně	k6eAd1	společně
s	s	k7c7	s
druidskými	druidský	k2eAgMnPc7d1	druidský
hudebníky	hudebník	k1gMnPc7	hudebník
povzbuzovaly	povzbuzovat	k5eAaImAgInP	povzbuzovat
keltské	keltský	k2eAgMnPc4d1	keltský
válečníky	válečník	k1gMnPc4	válečník
<g/>
.	.	kIx.	.
</s>
<s>
Zda	zda	k8xS	zda
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
druidky	druidka	k1gFnPc4	druidka
či	či	k8xC	či
někoho	někdo	k3yInSc2	někdo
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
Evropa	Evropa	k1gFnSc1	Evropa
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
éry	éra	k1gFnSc2	éra
bylo	být	k5eAaImAgNnS	být
čarodějnictví	čarodějnictví	k1gNnSc1	čarodějnictví
chápáno	chápat	k5eAaImNgNnS	chápat
jako	jako	k8xC	jako
praktikování	praktikování	k1gNnSc1	praktikování
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
,	,	kIx,	,
věštění	věštění	k1gNnSc1	věštění
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
démona	démon	k1gMnSc2	démon
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
známým	známý	k2eAgInSc7d1	známý
případem	případ	k1gInSc7	případ
odsouzení	odsouzení	k1gNnSc4	odsouzení
a	a	k8xC	a
popravy	poprava	k1gFnPc4	poprava
pro	pro	k7c4	pro
čarodějnictví	čarodějnictví	k1gNnSc4	čarodějnictví
v	v	k7c6	v
křesťanském	křesťanský	k2eAgNnSc6d1	křesťanské
období	období	k1gNnSc6	období
je	být	k5eAaImIp3nS	být
případ	případ	k1gInSc1	případ
Priscilliana	Priscillian	k1gMnSc4	Priscillian
<g/>
,	,	kIx,	,
biskupa	biskup	k1gMnSc4	biskup
z	z	k7c2	z
Ávily	Ávila	k1gFnSc2	Ávila
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
385	[number]	k4	385
uškrcen	uškrtit	k5eAaPmNgMnS	uškrtit
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
vzdorocísaře	vzdorocísař	k1gMnSc2	vzdorocísař
Magna	Magn	k1gMnSc2	Magn
Maxima	Maxim	k1gMnSc2	Maxim
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
jej	on	k3xPp3gMnSc4	on
udali	udat	k5eAaPmAgMnP	udat
galští	galský	k2eAgMnPc1d1	galský
biskupové	biskup	k1gMnPc1	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
i	i	k9	i
šest	šest	k4xCc1	šest
dalších	další	k2eAgMnPc2d1	další
hispánských	hispánský	k2eAgMnPc2d1	hispánský
kněží	kněz	k1gMnPc2	kněz
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
názory	názor	k1gInPc1	názor
se	se	k3xPyFc4	se
blížily	blížit	k5eAaImAgInP	blížit
gnosticismu	gnosticismus	k1gInSc3	gnosticismus
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
církevní	církevní	k2eAgFnPc1d1	církevní
autority	autorita	k1gFnPc1	autorita
včetně	včetně	k7c2	včetně
papeže	papež	k1gMnSc2	papež
Siricia	Siricius	k1gMnSc2	Siricius
a	a	k8xC	a
biskupů	biskup	k1gMnPc2	biskup
Martina	Martin	k1gInSc2	Martin
z	z	k7c2	z
Tours	Toursa	k1gFnPc2	Toursa
a	a	k8xC	a
Ambrože	Ambrož	k1gMnSc2	Ambrož
z	z	k7c2	z
Milána	Milán	k1gInSc2	Milán
proti	proti	k7c3	proti
popravě	poprava	k1gFnSc3	poprava
protestovaly	protestovat	k5eAaBmAgInP	protestovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Představy	představa	k1gFnPc1	představa
o	o	k7c6	o
smlouvě	smlouva	k1gFnSc6	smlouva
s	s	k7c7	s
Ďáblem	ďábel	k1gMnSc7	ďábel
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
až	až	k9	až
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ve	v	k7c6	v
spisu	spis	k1gInSc6	spis
Canon	Canon	kA	Canon
Episcopi	Episcopi	k1gNnSc2	Episcopi
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
čarodějnice	čarodějnice	k1gFnSc2	čarodějnice
běžně	běžně	k6eAd1	běžně
nazývány	nazývat	k5eAaImNgFnP	nazývat
bonae	bona	k1gFnPc1	bona
feminae	femina	k1gInSc2	femina
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
dobré	dobrý	k2eAgFnPc1d1	dobrá
ženy	žena	k1gFnPc1	žena
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
křesťanství	křesťanství	k1gNnSc2	křesťanství
byla	být	k5eAaImAgFnS	být
víra	víra	k1gFnSc1	víra
v	v	k7c6	v
čarodějnictví	čarodějnictví	k1gNnSc6	čarodějnictví
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
pohanskou	pohanský	k2eAgFnSc4d1	pohanská
pověru	pověra	k1gFnSc4	pověra
<g/>
.	.	kIx.	.
</s>
<s>
Zážitky	zážitek	k1gInPc1	zážitek
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
byly	být	k5eAaImAgInP	být
považovány	považován	k2eAgInPc1d1	považován
podle	podle	k7c2	podle
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
učení	učení	k1gNnSc2	učení
za	za	k7c4	za
halucinace	halucinace	k1gFnPc4	halucinace
a	a	k8xC	a
přestože	přestože	k8xS	přestože
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
už	už	k6eAd1	už
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
zavedeny	zaveden	k2eAgInPc4d1	zaveden
zákony	zákon	k1gInPc4	zákon
trestající	trestající	k2eAgNnSc4d1	trestající
čarodějnictví	čarodějnictví	k1gNnSc4	čarodějnictví
smrtí	smrt	k1gFnPc2	smrt
<g/>
,	,	kIx,	,
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
žádnému	žádný	k3yNgNnSc3	žádný
rozsáhlému	rozsáhlý	k2eAgNnSc3d1	rozsáhlé
pronásledování	pronásledování	k1gNnSc3	pronásledování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
psal	psát	k5eAaImAgMnS	psát
biskup	biskup	k1gMnSc1	biskup
Burchard	Burchard	k1gMnSc1	Burchard
z	z	k7c2	z
Wormsu	Worms	k1gInSc2	Worms
o	o	k7c6	o
ženách	žena	k1gFnPc6	žena
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
uctívají	uctívat	k5eAaImIp3nP	uctívat
bohyni	bohyně	k1gFnSc4	bohyně
Dianu	Diana	k1gFnSc4	Diana
a	a	k8xC	a
opouštějí	opouštět	k5eAaImIp3nP	opouštět
své	svůj	k3xOyFgNnSc4	svůj
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
letět	letět	k5eAaImF	letět
na	na	k7c4	na
noční	noční	k2eAgNnSc4d1	noční
setkání	setkání	k1gNnSc4	setkání
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
fyzicky	fyzicky	k6eAd1	fyzicky
přítomny	přítomen	k2eAgInPc1d1	přítomen
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
představa	představa	k1gFnSc1	představa
o	o	k7c6	o
sletu	slet	k1gInSc6	slet
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pronásledoval	pronásledovat	k5eAaImAgMnS	pronásledovat
kacíře	kacíř	k1gMnPc4	kacíř
i	i	k9	i
čaroděje	čaroděj	k1gMnPc4	čaroděj
v	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
zemích	zem	k1gFnPc6	zem
mnich	mnich	k1gMnSc1	mnich
Konrád	Konrád	k1gMnSc1	Konrád
z	z	k7c2	z
Marburgu	Marburg	k1gInSc2	Marburg
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
spíš	spíš	k9	spíš
o	o	k7c4	o
exces	exces	k1gInSc4	exces
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
skončil	skončit	k5eAaPmAgInS	skončit
Konrádovou	Konrádův	k2eAgFnSc7d1	Konrádova
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1231	[number]	k4	1231
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
císař	císař	k1gMnSc1	císař
Fridrich	Fridrich	k1gMnSc1	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Sicilský	sicilský	k2eAgMnSc1d1	sicilský
za	za	k7c2	za
čarodějnictví	čarodějnictví	k1gNnSc2	čarodějnictví
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
upálením	upálení	k1gNnSc7	upálení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
začalo	začít	k5eAaPmAgNnS	začít
pro	pro	k7c4	pro
noční	noční	k2eAgInPc4d1	noční
slety	slet	k1gInPc4	slet
používat	používat	k5eAaImF	používat
slovo	slovo	k1gNnSc4	slovo
sabat	sabat	k1gInSc4	sabat
<g/>
.	.	kIx.	.
</s>
<s>
Začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
věřit	věřit	k5eAaImF	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
čarodějnice	čarodějnice	k1gFnPc1	čarodějnice
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
sabatu	sabat	k1gInSc6	sabat
fyzicky	fyzicky	k6eAd1	fyzicky
přítomny	přítomen	k2eAgInPc1d1	přítomen
a	a	k8xC	a
obcují	obcovat	k5eAaImIp3nP	obcovat
tam	tam	k6eAd1	tam
s	s	k7c7	s
Ďáblem	ďábel	k1gMnSc7	ďábel
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
o	o	k7c6	o
nich	on	k3xPp3gFnPc6	on
objevil	objevit	k5eAaPmAgMnS	objevit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc1d1	podobný
představě	představa	k1gFnSc3	představa
o	o	k7c4	o
kacířích	kacíř	k1gMnPc6	kacíř
a	a	k8xC	a
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
čarodějnictví	čarodějnictví	k1gNnSc1	čarodějnictví
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
herezi	hereze	k1gFnSc4	hereze
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
kázáních	kázání	k1gNnPc6	kázání
mnicha	mnich	k1gMnSc2	mnich
Bernardina	bernardin	k1gMnSc4	bernardin
ze	z	k7c2	z
Sieny	Siena	k1gFnSc2	Siena
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
byl	být	k5eAaImAgInS	být
setřen	setřen	k2eAgInSc1d1	setřen
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
černou	černý	k2eAgFnSc7d1	černá
a	a	k8xC	a
bílou	bílý	k2eAgFnSc7d1	bílá
magií	magie	k1gFnSc7	magie
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
měla	mít	k5eAaImAgFnS	mít
kouzla	kouzlo	k1gNnPc4	kouzlo
přinášet	přinášet	k5eAaImF	přinášet
zdar	zdar	k1gInSc4	zdar
a	a	k8xC	a
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
tolerována	tolerován	k2eAgFnSc1d1	tolerována
<g/>
,	,	kIx,	,
podezřelé	podezřelý	k2eAgNnSc1d1	podezřelé
bylo	být	k5eAaImAgNnS	být
vyvolávání	vyvolávání	k1gNnSc1	vyvolávání
zlých	zlá	k1gFnPc2	zlá
duchů	duch	k1gMnPc2	duch
<g/>
.	.	kIx.	.
</s>
<s>
Rituály	rituál	k1gInPc1	rituál
měly	mít	k5eAaImAgInP	mít
pohanské	pohanský	k2eAgInPc1d1	pohanský
a	a	k8xC	a
mystické	mystický	k2eAgInPc1d1	mystický
původy	původ	k1gInPc1	původ
<g/>
,	,	kIx,	,
kombinované	kombinovaný	k2eAgInPc1d1	kombinovaný
s	s	k7c7	s
křesťanskými	křesťanský	k2eAgInPc7d1	křesťanský
motivy	motiv	k1gInPc7	motiv
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnohých	mnohý	k2eAgNnPc6d1	mnohé
zaklínadlech	zaklínadlo	k1gNnPc6	zaklínadlo
vystupovaly	vystupovat	k5eAaImAgInP	vystupovat
jak	jak	k6eAd1	jak
křesťanští	křesťanský	k2eAgMnPc1d1	křesťanský
světci	světec	k1gMnPc1	světec
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
mytologické	mytologický	k2eAgFnPc4d1	mytologická
postavy	postava	k1gFnPc4	postava
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
problematickou	problematický	k2eAgFnSc4d1	problematická
se	se	k3xPyFc4	se
také	také	k9	také
jeví	jevit	k5eAaImIp3nS	jevit
hranice	hranice	k1gFnPc4	hranice
mezi	mezi	k7c7	mezi
povoleným	povolený	k2eAgNnSc7d1	povolené
kouzelnictvím	kouzelnictví	k1gNnSc7	kouzelnictví
a	a	k8xC	a
zakázaným	zakázaný	k2eAgNnSc7d1	zakázané
čarodějnictvím	čarodějnictví	k1gNnSc7	čarodějnictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Soustavné	soustavný	k2eAgNnSc1d1	soustavné
pronásledování	pronásledování	k1gNnSc1	pronásledování
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
začalo	začít	k5eAaPmAgNnS	začít
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1340	[number]	k4	1340
<g/>
,	,	kIx,	,
zesílilo	zesílit	k5eAaPmAgNnS	zesílit
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1484	[number]	k4	1484
vydal	vydat	k5eAaPmAgMnS	vydat
papež	papež	k1gMnSc1	papež
Inocenc	Inocenc	k1gMnSc1	Inocenc
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
bulu	bula	k1gFnSc4	bula
Sumus	Sumus	k1gInSc4	Sumus
desidernatis	desidernatis	k1gFnSc2	desidernatis
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
pronásledováním	pronásledování	k1gNnSc7	pronásledování
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
pověřil	pověřit	k5eAaPmAgInS	pověřit
Svatou	svatý	k2eAgFnSc4d1	svatá
inkvizici	inkvizice	k1gFnSc4	inkvizice
<g/>
.	.	kIx.	.
</s>
<s>
Návodem	návod	k1gInSc7	návod
k	k	k7c3	k
rozpoznávání	rozpoznávání	k1gNnSc3	rozpoznávání
<g/>
,	,	kIx,	,
pronásledování	pronásledování	k1gNnSc1	pronásledování
a	a	k8xC	a
trestání	trestání	k1gNnSc1	trestání
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
stal	stát	k5eAaPmAgInS	stát
spis	spis	k1gInSc1	spis
Malleus	Malleus	k1gMnSc1	Malleus
maleficarum	maleficarum	k1gInSc1	maleficarum
z	z	k7c2	z
pera	pero	k1gNnSc2	pero
fanatického	fanatický	k2eAgMnSc2d1	fanatický
inkvizitora	inkvizitor	k1gMnSc2	inkvizitor
Heinricha	Heinrich	k1gMnSc2	Heinrich
Institoria	Institorium	k1gNnSc2	Institorium
<g/>
.	.	kIx.	.
</s>
<s>
Pronásledování	pronásledování	k1gNnSc1	pronásledování
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
popraven	popraven	k2eAgInSc1d1	popraven
největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
zejména	zejména	k9	zejména
v	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
a	a	k8xC	a
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
.	.	kIx.	.
</s>
<s>
Procesy	proces	k1gInPc1	proces
ale	ale	k9	ale
víceméně	víceméně	k9	víceméně
probíhaly	probíhat	k5eAaImAgFnP	probíhat
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
účastnili	účastnit	k5eAaImAgMnP	účastnit
se	se	k3xPyFc4	se
jich	on	k3xPp3gMnPc2	on
protestanti	protestant	k1gMnPc1	protestant
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
katolíci	katolík	k1gMnPc1	katolík
<g/>
.	.	kIx.	.
</s>
<s>
Pronásledování	pronásledování	k1gNnSc1	pronásledování
probíhalo	probíhat	k5eAaImAgNnS	probíhat
ve	v	k7c6	v
vlnách	vlna	k1gFnPc6	vlna
<g/>
,	,	kIx,	,
spojených	spojený	k2eAgFnPc6d1	spojená
s	s	k7c7	s
lidskou	lidský	k2eAgFnSc7d1	lidská
nespokojeností	nespokojenost	k1gFnSc7	nespokojenost
(	(	kIx(	(
<g/>
hladomory	hladomor	k1gInPc1	hladomor
<g/>
,	,	kIx,	,
epidemie	epidemie	k1gFnPc1	epidemie
<g/>
,	,	kIx,	,
etc	etc	k?	etc
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
ale	ale	k9	ale
o	o	k7c4	o
úzkou	úzký	k2eAgFnSc4d1	úzká
spolupráci	spolupráce	k1gFnSc4	spolupráce
všech	všecek	k3xTgFnPc2	všecek
společenských	společenský	k2eAgFnPc2d1	společenská
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgMnS	angažovat
stát	stát	k5eAaPmF	stát
<g/>
,	,	kIx,	,
ke	k	k7c3	k
stíhání	stíhání	k1gNnSc3	stíhání
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
speciální	speciální	k2eAgInSc1d1	speciální
soudní	soudní	k2eAgInSc1d1	soudní
aparát	aparát	k1gInSc1	aparát
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
se	se	k3xPyFc4	se
využívalo	využívat	k5eAaPmAgNnS	využívat
mučení	mučení	k1gNnSc1	mučení
a	a	k8xC	a
po	po	k7c6	po
jednom	jeden	k4xCgNnSc6	jeden
obvinění	obvinění	k1gNnSc6	obvinění
následovalo	následovat	k5eAaImAgNnS	následovat
mnoho	mnoho	k6eAd1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
,	,	kIx,	,
založených	založený	k2eAgInPc2d1	založený
na	na	k7c6	na
vynuceném	vynucený	k2eAgNnSc6d1	vynucené
doznání	doznání	k1gNnSc6	doznání
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejkrutějších	krutý	k2eAgInPc2d3	nejkrutější
procesů	proces	k1gInPc2	proces
nastal	nastat	k5eAaPmAgInS	nastat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Bambergu	Bamberg	k1gInSc6	Bamberg
<g/>
,	,	kIx,	,
popraveno	popraven	k2eAgNnSc1d1	popraveno
bylo	být	k5eAaImAgNnS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
lidí	člověk	k1gMnPc2	člověk
včetně	včetně	k7c2	včetně
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
pronásledování	pronásledování	k1gNnSc1	pronásledování
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
i	i	k9	i
do	do	k7c2	do
amerických	americký	k2eAgFnPc2d1	americká
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Neblaze	blaze	k6eNd1	blaze
proslulé	proslulý	k2eAgFnPc1d1	proslulá
jsou	být	k5eAaImIp3nP	být
zvláště	zvláště	k9	zvláště
procesy	proces	k1gInPc1	proces
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Salemu	Salem	k1gInSc2	Salem
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
procesů	proces	k1gInPc2	proces
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
obětí	oběť	k1gFnSc7	oběť
čarodějnických	čarodějnický	k2eAgInPc2d1	čarodějnický
procesů	proces	k1gInPc2	proces
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
byla	být	k5eAaImAgFnS	být
švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
služka	služka	k1gFnSc1	služka
Anna	Anna	k1gFnSc1	Anna
Göldi	Göld	k1gMnPc1	Göld
<g/>
,	,	kIx,	,
popravená	popravený	k2eAgFnSc1d1	popravená
roku	rok	k1gInSc2	rok
1782	[number]	k4	1782
<g/>
.	.	kIx.	.
</s>
<s>
Odhady	odhad	k1gInPc1	odhad
počtu	počet	k1gInSc2	počet
obětí	oběť	k1gFnPc2	oběť
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c7	mezi
40	[number]	k4	40
000	[number]	k4	000
a	a	k8xC	a
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
údaj	údaj	k1gInSc4	údaj
devět	devět	k4xCc4	devět
milionů	milion	k4xCgInPc2	milion
,	,	kIx,	,
<g/>
tradovaný	tradovaný	k2eAgInSc1d1	tradovaný
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
naprosto	naprosto	k6eAd1	naprosto
nepodložený	podložený	k2eNgInSc1d1	nepodložený
<g/>
.	.	kIx.	.
</s>
<s>
Některých	některý	k3yIgFnPc2	některý
oblastí	oblast	k1gFnPc2	oblast
se	se	k3xPyFc4	se
pronásledování	pronásledování	k1gNnSc1	pronásledování
dotklo	dotknout	k5eAaPmAgNnS	dotknout
velice	velice	k6eAd1	velice
mírně	mírně	k6eAd1	mírně
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
území	území	k1gNnPc1	území
obývaná	obývaný	k2eAgNnPc1d1	obývané
vyznavači	vyznavač	k1gMnPc7	vyznavač
pravoslaví	pravoslaví	k1gNnSc2	pravoslaví
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
Polskolitevský	polskolitevský	k2eAgInSc1d1	polskolitevský
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
Uhry	Uhry	k1gFnPc1	Uhry
nebo	nebo	k8xC	nebo
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příčina	příčina	k1gFnSc1	příčina
tohoto	tento	k3xDgNnSc2	tento
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
pronásledování	pronásledování	k1gNnSc2	pronásledování
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasná	jasný	k2eAgFnSc1d1	jasná
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
možné	možný	k2eAgInPc4d1	možný
vlivy	vliv	k1gInPc4	vliv
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
napětí	napětí	k1gNnSc1	napětí
mezi	mezi	k7c7	mezi
katolíky	katolík	k1gMnPc7	katolík
a	a	k8xC	a
protestanty	protestant	k1gMnPc7	protestant
<g/>
,	,	kIx,	,
renesanční	renesanční	k2eAgFnSc7d1	renesanční
zájem	zájem	k1gInSc4	zájem
o	o	k7c6	o
magii	magie	k1gFnSc6	magie
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
mohl	moct	k5eAaImAgMnS	moct
oživit	oživit	k5eAaPmF	oživit
víru	víra	k1gFnSc4	víra
v	v	k7c6	v
čarodějnictví	čarodějnictví	k1gNnSc6	čarodějnictví
<g/>
,	,	kIx,	,
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
neúroda	neúroda	k1gFnSc1	neúroda
a	a	k8xC	a
očekávání	očekávání	k1gNnSc4	očekávání
příchodu	příchod	k1gInSc2	příchod
Antikrista	Antikrist	k1gMnSc2	Antikrist
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
procesy	proces	k1gInPc7	proces
a	a	k8xC	a
obzvláště	obzvláště	k6eAd1	obzvláště
s	s	k7c7	s
knihou	kniha	k1gFnSc7	kniha
Malleus	Malleus	k1gInSc4	Malleus
maleficarum	maleficarum	k1gInSc1	maleficarum
se	se	k3xPyFc4	se
také	také	k9	také
často	často	k6eAd1	často
pojí	pojit	k5eAaImIp3nS	pojit
nařčení	nařčení	k1gNnSc1	nařčení
z	z	k7c2	z
antifeminismu	antifeminismus	k1gInSc2	antifeminismus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
však	však	k9	však
převládl	převládnout	k5eAaPmAgMnS	převládnout
skeptický	skeptický	k2eAgInSc4d1	skeptický
pohled	pohled	k1gInSc4	pohled
a	a	k8xC	a
popravy	poprava	k1gFnPc1	poprava
postupně	postupně	k6eAd1	postupně
opadly	opadnout	k5eAaPmAgFnP	opadnout
<g/>
,	,	kIx,	,
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
mohly	moct	k5eAaImAgFnP	moct
mít	mít	k5eAaImF	mít
obavy	obava	k1gFnPc1	obava
elit	elita	k1gFnPc2	elita
o	o	k7c4	o
vlastní	vlastní	k2eAgNnPc4d1	vlastní
bezpečí	bezpečí	k1gNnPc4	bezpečí
a	a	k8xC	a
společenský	společenský	k2eAgInSc4d1	společenský
řád	řád	k1gInSc4	řád
a	a	k8xC	a
také	také	k9	také
nástup	nástup	k1gInSc4	nástup
osvícenství	osvícenství	k1gNnSc2	osvícenství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
začala	začít	k5eAaPmAgFnS	začít
svobodně	svobodně	k6eAd1	svobodně
praktikovat	praktikovat	k5eAaImF	praktikovat
lidová	lidový	k2eAgFnSc1d1	lidová
magie	magie	k1gFnSc1	magie
spojovaná	spojovaný	k2eAgFnSc1d1	spojovaná
s	s	k7c7	s
čarodějnictvím	čarodějnictví	k1gNnSc7	čarodějnictví
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
vznik	vznik	k1gInSc4	vznik
Wiccy	Wicca	k1gFnSc2	Wicca
a	a	k8xC	a
novopohanství	novopohanství	k1gNnSc2	novopohanství
<g/>
.	.	kIx.	.
</s>
<s>
Dobové	dobový	k2eAgInPc1d1	dobový
zákony	zákon	k1gInPc1	zákon
postihovaly	postihovat	k5eAaImAgInP	postihovat
pouze	pouze	k6eAd1	pouze
šarlatánství	šarlatánství	k1gNnPc2	šarlatánství
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
po	po	k7c4	po
obvinění	obvinění	k1gNnSc4	obvinění
nespokojeným	spokojený	k2eNgMnSc7d1	nespokojený
klientem	klient	k1gMnSc7	klient
<g/>
,	,	kIx,	,
a	a	k8xC	a
přestože	přestože	k8xS	přestože
sporadicky	sporadicky	k6eAd1	sporadicky
stále	stále	k6eAd1	stále
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
lynčování	lynčování	k1gNnSc3	lynčování
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
takové	takový	k3xDgNnSc4	takový
jednání	jednání	k1gNnSc4	jednání
stát	stát	k5eAaImF	stát
trestal	trestat	k5eAaImAgMnS	trestat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Teorie	teorie	k1gFnSc1	teorie
o	o	k7c6	o
původu	původ	k1gInSc6	původ
====	====	k?	====
</s>
</p>
<p>
<s>
Okolo	okolo	k7c2	okolo
původu	původ	k1gInSc2	původ
fenoménu	fenomén	k1gInSc2	fenomén
čarodějnictví	čarodějnictví	k1gNnSc1	čarodějnictví
středověké	středověký	k2eAgFnSc2d1	středověká
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
rozpoutala	rozpoutat	k5eAaPmAgFnS	rozpoutat
během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
odborná	odborný	k2eAgFnSc1d1	odborná
diskuse	diskuse	k1gFnSc1	diskuse
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
vyvolány	vyvolat	k5eAaPmNgFnP	vyvolat
především	především	k6eAd1	především
britskou	britský	k2eAgFnSc7d1	britská
antropoložkou	antropoložka	k1gFnSc7	antropoložka
Margaret	Margareta	k1gFnPc2	Margareta
Murray	Murraa	k1gFnSc2	Murraa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
The	The	k1gMnSc1	The
Witch-Cult	Witch-Cult	k1gMnSc1	Witch-Cult
in	in	k?	in
Western	Western	kA	Western
Europe	Europ	k1gInSc5	Europ
(	(	kIx(	(
<g/>
Čarodějnický	čarodějnický	k2eAgInSc1d1	čarodějnický
kult	kult	k1gInSc1	kult
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
)	)	kIx)	)
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
přišla	přijít	k5eAaPmAgFnS	přijít
s	s	k7c7	s
teorií	teorie	k1gFnSc7	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
středověké	středověký	k2eAgFnPc1d1	středověká
čarodějnice	čarodějnice	k1gFnPc1	čarodějnice
přímo	přímo	k6eAd1	přímo
navazovaly	navazovat	k5eAaImAgFnP	navazovat
na	na	k7c6	na
pohanství	pohanství	k1gNnSc6	pohanství
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
na	na	k7c4	na
neolitický	neolitický	k2eAgInSc4d1	neolitický
kult	kult	k1gInSc4	kult
plodnosti	plodnost	k1gFnSc2	plodnost
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
její	její	k3xOp3gFnPc1	její
teze	teze	k1gFnPc1	teze
byly	být	k5eAaImAgFnP	být
všeobecně	všeobecně	k6eAd1	všeobecně
odmítnuty	odmítnut	k2eAgFnPc1d1	odmítnuta
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgFnP	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
vznik	vznik	k1gInSc4	vznik
kultu	kult	k1gInSc2	kult
Wicca	Wicc	k1gInSc2	Wicc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
však	však	k9	však
italský	italský	k2eAgMnSc1d1	italský
historik	historik	k1gMnSc1	historik
Carlo	Carlo	k1gNnSc1	Carlo
Ginzburg	Ginzburg	k1gMnSc1	Ginzburg
prokázal	prokázat	k5eAaPmAgInS	prokázat
existenci	existence	k1gFnSc4	existence
kultu	kult	k1gInSc2	kult
plodnosti	plodnost	k1gFnSc2	plodnost
jménem	jméno	k1gNnSc7	jméno
Benandanti	Benandant	k1gMnPc1	Benandant
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterým	který	k3yQgMnSc7	který
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
inkviziční	inkviziční	k2eAgInSc4d1	inkviziční
proces	proces	k1gInSc4	proces
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
byla	být	k5eAaImAgFnS	být
dokázána	dokázán	k2eAgFnSc1d1	dokázána
existence	existence	k1gFnSc1	existence
dalších	další	k2eAgInPc2d1	další
podobných	podobný	k2eAgInPc2d1	podobný
kultů	kult	k1gInPc2	kult
a	a	k8xC	a
kořeny	kořen	k1gInPc1	kořen
evropské	evropský	k2eAgFnSc2d1	Evropská
čarodějnické	čarodějnický	k2eAgFnSc2d1	čarodějnická
praxe	praxe	k1gFnSc2	praxe
a	a	k8xC	a
jejího	její	k3xOp3gInSc2	její
obrazu	obraz	k1gInSc2	obraz
jsou	být	k5eAaImIp3nP	být
hledány	hledat	k5eAaImNgFnP	hledat
v	v	k7c6	v
šamanismu	šamanismus	k1gInSc6	šamanismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Asie	Asie	k1gFnSc2	Asie
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Starověký	starověký	k2eAgInSc1d1	starověký
Přední	přední	k2eAgInSc1d1	přední
východ	východ	k1gInSc1	východ
a	a	k8xC	a
Indie	Indie	k1gFnSc1	Indie
====	====	k?	====
</s>
</p>
<p>
<s>
Starověké	starověký	k2eAgInPc1d1	starověký
prameny	pramen	k1gInPc1	pramen
o	o	k7c6	o
čarodějnictví	čarodějnictví	k1gNnSc6	čarodějnictví
na	na	k7c6	na
Předním	přední	k2eAgInSc6d1	přední
východě	východ	k1gInSc6	východ
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
především	především	k9	především
Mezopotámie	Mezopotámie	k1gFnPc1	Mezopotámie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
existovalo	existovat	k5eAaImAgNnS	existovat
v	v	k7c6	v
opozici	opozice	k1gFnSc6	opozice
vůči	vůči	k7c3	vůči
oficiální	oficiální	k2eAgFnSc3d1	oficiální
kněžské	kněžský	k2eAgFnSc3d1	kněžská
magii	magie	k1gFnSc3	magie
<g/>
,	,	kIx,	,
Chammurapiho	Chammurapi	k1gMnSc4	Chammurapi
zákoník	zákoník	k1gInSc1	zákoník
trestal	trestat	k5eAaImAgInS	trestat
záškodná	záškodný	k2eAgNnPc4d1	záškodný
kouzla	kouzlo	k1gNnPc4	kouzlo
trestem	trest	k1gInSc7	trest
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
i	i	k8xC	i
zkouška	zkouška	k1gFnSc1	zkouška
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Čarodějnická	čarodějnický	k2eAgFnSc1d1	čarodějnická
praxe	praxe	k1gFnSc1	praxe
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
především	především	k6eAd1	především
magií	magie	k1gFnSc7	magie
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
manipulací	manipulace	k1gFnPc2	manipulace
s	s	k7c7	s
figurkami	figurka	k1gFnPc7	figurka
či	či	k8xC	či
panenkami	panenka	k1gFnPc7	panenka
a	a	k8xC	a
čarodějnicím	čarodějnice	k1gFnPc3	čarodějnice
byla	být	k5eAaImAgFnS	být
přisuzována	přisuzovat	k5eAaImNgFnS	přisuzovat
schopnost	schopnost	k1gFnSc1	schopnost
neviditelnosti	neviditelnost	k1gFnSc2	neviditelnost
<g/>
,	,	kIx,	,
procházení	procházení	k1gNnSc1	procházení
zdí	zeď	k1gFnPc2	zeď
<g/>
,	,	kIx,	,
narušování	narušování	k1gNnSc1	narušování
kosmického	kosmický	k2eAgInSc2d1	kosmický
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
přivození	přivození	k1gNnSc2	přivození
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
neplodnosti	neplodnost	k1gFnSc2	neplodnost
či	či	k8xC	či
rituální	rituální	k2eAgFnSc2d1	rituální
nečistoty	nečistota	k1gFnSc2	nečistota
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
čarodějnicemi	čarodějnice	k1gFnPc7	čarodějnice
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
spojován	spojován	k2eAgMnSc1d1	spojován
noční	noční	k2eAgMnSc1d1	noční
démon	démon	k1gMnSc1	démon
Lilitu	Lilit	k1gInSc2	Lilit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čarodějnictví	čarodějnictví	k1gNnSc1	čarodějnictví
bylo	být	k5eAaImAgNnS	být
nazýváno	nazývat	k5eAaImNgNnS	nazývat
kašapu	kašap	k1gInSc6	kašap
"	"	kIx"	"
<g/>
otrávení	otrávení	k1gNnSc2	otrávení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kašu	kaša	k1gFnSc4	kaša
"	"	kIx"	"
<g/>
svázání	svázání	k1gNnSc2	svázání
<g/>
"	"	kIx"	"
a	a	k8xC	a
množstvím	množství	k1gNnSc7	množství
výrazům	výraz	k1gInPc3	výraz
související	související	k2eAgFnPc4d1	související
se	s	k7c7	s
slinami	slina	k1gFnPc7	slina
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
použitím	použití	k1gNnSc7	použití
k	k	k7c3	k
záškodné	záškodný	k2eAgFnSc3d1	záškodná
magii	magie	k1gFnSc3	magie
<g/>
.	.	kIx.	.
</s>
<s>
Čaroděj	čaroděj	k1gMnSc1	čaroděj
byl	být	k5eAaImAgMnS	být
nazýván	nazýván	k2eAgMnSc1d1	nazýván
rakhu	rakh	k1gInSc2	rakh
"	"	kIx"	"
<g/>
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
používá	používat	k5eAaImIp3nS	používat
slin	slina	k1gFnPc2	slina
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
sumerštině	sumerština	k1gFnSc6	sumerština
gal-ukh-zu	galkh	k1gInSc2	gal-ukh-z
"	"	kIx"	"
<g/>
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
to	ten	k3xDgNnSc4	ten
umí	umět	k5eAaImIp3nS	umět
se	s	k7c7	s
slinou	slina	k1gFnSc7	slina
<g/>
"	"	kIx"	"
a	a	k8xC	a
ugh-dugga	ughugga	k1gFnSc1	ugh-dugga
"	"	kIx"	"
<g/>
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mumlá	mumlat	k5eAaImIp3nS	mumlat
při	při	k7c6	při
plivání	plivání	k1gNnSc6	plivání
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
kadištu	kadišt	k1gInSc2	kadišt
<g/>
,	,	kIx,	,
ištaritu	ištarit	k1gInSc2	ištarit
nebo	nebo	k8xC	nebo
ba	ba	k9	ba
<g/>
́	́	k?	́
<g/>
arum	arum	k?	arum
ša	ša	k?	ša
muši	muš	k1gFnSc2	muš
"	"	kIx"	"
<g/>
noční	noční	k2eAgFnSc2d1	noční
lovkyně	lovkyně	k1gFnSc2	lovkyně
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
Chetitů	Chetit	k1gMnPc2	Chetit
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
praktikantem	praktikant	k1gMnSc7	praktikant
čarodějnictví	čarodějnictví	k1gNnPc1	čarodějnictví
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nenáleží	náležet	k5eNaImIp3nS	náležet
k	k	k7c3	k
oficiálnímu	oficiální	k2eAgNnSc3d1	oficiální
kněžstvu	kněžstvo	k1gNnSc3	kněžstvo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgNnP	být
znalá	znalý	k2eAgNnPc1d1	znalé
lidových	lidový	k2eAgFnPc2d1	lidová
praktik	praktika	k1gFnPc2	praktika
<g/>
.	.	kIx.	.
</s>
<s>
Označovala	označovat	k5eAaImAgFnS	označovat
se	se	k3xPyFc4	se
ideogramem	ideogram	k1gInSc7	ideogram
shugi	shugi	k1gNnPc2	shugi
"	"	kIx"	"
<g/>
stará	starý	k2eAgNnPc1d1	staré
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
starověké	starověký	k2eAgFnSc6d1	starověká
Indii	Indie	k1gFnSc6	Indie
bylo	být	k5eAaImAgNnS	být
čarodějnictví	čarodějnictví	k1gNnSc1	čarodějnictví
velmi	velmi	k6eAd1	velmi
rozšířeno	rozšířit	k5eAaPmNgNnS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
se	se	k3xPyFc4	se
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
i	i	k9	i
náboženské	náboženský	k2eAgFnPc4d1	náboženská
sbírky	sbírka	k1gFnPc4	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Atharvavéda	Atharvavéda	k1gMnSc1	Atharvavéda
<g/>
,	,	kIx,	,
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
védská	védský	k2eAgFnSc1d1	védská
sbírka	sbírka	k1gFnSc1	sbírka
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnoho	mnoho	k4c4	mnoho
zaklínadel	zaklínadlo	k1gNnPc2	zaklínadlo
<g/>
,	,	kIx,	,
používaných	používaný	k2eAgInPc2d1	používaný
proti	proti	k7c3	proti
chorobám	choroba	k1gFnPc3	choroba
<g/>
,	,	kIx,	,
k	k	k7c3	k
ochraně	ochrana	k1gFnSc3	ochrana
polí	pole	k1gFnPc2	pole
<g/>
,	,	kIx,	,
milostným	milostný	k2eAgNnPc3d1	milostné
kouzlům	kouzlo	k1gNnPc3	kouzlo
nebo	nebo	k8xC	nebo
zničení	zničení	k1gNnSc1	zničení
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Staroindické	staroindický	k2eAgInPc4d1	staroindický
zákoníky	zákoník	k1gInPc4	zákoník
či	či	k8xC	či
politický	politický	k2eAgInSc4d1	politický
spis	spis	k1gInSc4	spis
Arthašástra	Arthašástrum	k1gNnSc2	Arthašástrum
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
čarodějných	čarodějný	k2eAgFnPc2d1	čarodějná
praktik	praktika	k1gFnPc2	praktika
používalo	používat	k5eAaImAgNnS	používat
ke	k	k7c3	k
špionáži	špionáž	k1gFnSc3	špionáž
nebo	nebo	k8xC	nebo
tajnému	tajný	k2eAgNnSc3d1	tajné
osvobozování	osvobozování	k1gNnSc3	osvobozování
vězňů	vězeň	k1gMnPc2	vězeň
<g/>
.	.	kIx.	.
</s>
<s>
Učebnice	učebnice	k1gFnSc1	učebnice
erotiky	erotika	k1gFnSc2	erotika
Kámasútra	Kámasútra	k1gFnSc1	Kámasútra
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
návody	návod	k1gInPc4	návod
k	k	k7c3	k
provádění	provádění	k1gNnSc3	provádění
milostné	milostný	k2eAgFnSc2d1	milostná
magie	magie	k1gFnSc2	magie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
se	se	k3xPyFc4	se
rozlišovaly	rozlišovat	k5eAaImAgFnP	rozlišovat
magické	magický	k2eAgFnPc1d1	magická
a	a	k8xC	a
mystické	mystický	k2eAgFnPc1d1	mystická
schopnosti	schopnost	k1gFnPc1	schopnost
siddhi	siddhi	k6eAd1	siddhi
dosažené	dosažený	k2eAgFnPc1d1	dosažená
"	"	kIx"	"
<g/>
čistými	čistý	k2eAgInPc7d1	čistý
<g/>
"	"	kIx"	"
prostředky	prostředek	k1gInPc7	prostředek
<g/>
,	,	kIx,	,
především	především	k9	především
pomocí	pomocí	k7c2	pomocí
jógových	jógový	k2eAgNnPc2d1	jógové
cvičení	cvičení	k1gNnPc2	cvičení
a	a	k8xC	a
askeze	askeze	k1gFnSc2	askeze
<g/>
,	,	kIx,	,
od	od	k7c2	od
zlovolného	zlovolný	k2eAgNnSc2d1	zlovolné
čarodějnictví	čarodějnictví	k1gNnSc2	čarodějnictví
<g/>
,	,	kIx,	,
spojeného	spojený	k2eAgNnSc2d1	spojené
s	s	k7c7	s
démony	démon	k1gMnPc7	démon
a	a	k8xC	a
konaného	konaný	k2eAgInSc2d1	konaný
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
na	na	k7c6	na
žárovištích	žároviště	k1gNnPc6	žároviště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
spalovali	spalovat	k5eAaImAgMnP	spalovat
mrtví	mrtvý	k1gMnPc1	mrtvý
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
lidských	lidský	k2eAgInPc2d1	lidský
ostatků	ostatek	k1gInPc2	ostatek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dochoval	dochovat	k5eAaPmAgInS	dochovat
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
návod	návod	k1gInSc1	návod
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
může	moct	k5eAaImIp3nS	moct
čaroděj	čaroděj	k1gMnSc1	čaroděj
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
stavu	stav	k1gInSc3	stav
vidjádhárů	vidjádhár	k1gMnPc2	vidjádhár
<g/>
,	,	kIx,	,
nadpozemských	nadpozemský	k2eAgFnPc2d1	nadpozemská
bytostí	bytost	k1gFnPc2	bytost
<g/>
,	,	kIx,	,
nadaných	nadaný	k2eAgFnPc2d1	nadaná
znalostí	znalost	k1gFnPc2	znalost
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
,	,	kIx,	,
dlouhověkostí	dlouhověkost	k1gFnSc7	dlouhověkost
a	a	k8xC	a
schopností	schopnost	k1gFnSc7	schopnost
létat	létat	k5eAaImF	létat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
navázat	navázat	k5eAaPmF	navázat
styk	styk	k1gInSc4	styk
s	s	k7c7	s
vidjádhárskou	vidjádhárský	k2eAgFnSc7d1	vidjádhárský
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
počne	počnout	k5eAaPmIp3nS	počnout
<g/>
,	,	kIx,	,
vyjmout	vyjmout	k5eAaPmF	vyjmout
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
plod	plod	k1gInSc4	plod
a	a	k8xC	a
ten	ten	k3xDgInSc4	ten
sníst	sníst	k5eAaPmF	sníst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čarodějnictví	čarodějnictví	k1gNnSc1	čarodějnictví
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
pronásledováno	pronásledován	k2eAgNnSc1d1	pronásledováno
<g/>
.	.	kIx.	.
</s>
<s>
Trestem	trest	k1gInSc7	trest
bylo	být	k5eAaImAgNnS	být
vyhnanství	vyhnanství	k1gNnSc1	vyhnanství
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
pomocí	pomocí	k7c2	pomocí
kouzel	kouzlo	k1gNnPc2	kouzlo
spáchán	spáchán	k2eAgInSc4d1	spáchán
jiný	jiný	k2eAgInSc4d1	jiný
zločin	zločin	k1gInSc4	zločin
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
smrt	smrt	k1gFnSc4	smrt
nebo	nebo	k8xC	nebo
vysoké	vysoký	k2eAgFnPc4d1	vysoká
finanční	finanční	k2eAgFnPc4d1	finanční
kompenzace	kompenzace	k1gFnPc4	kompenzace
<g/>
.	.	kIx.	.
</s>
<s>
Zákoníky	zákoník	k1gInPc1	zákoník
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
použije	použít	k5eAaPmIp3nS	použít
magii	magie	k1gFnSc4	magie
na	na	k7c4	na
vůči	vůči	k7c3	vůči
své	svůj	k3xOyFgFnSc3	svůj
ženě	žena	k1gFnSc3	žena
či	či	k8xC	či
snoubence	snoubenka	k1gFnSc3	snoubenka
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
být	být	k5eAaImF	být
trestán	trestat	k5eAaImNgMnS	trestat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Judaismus	judaismus	k1gInSc4	judaismus
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
judaismu	judaismus	k1gInSc6	judaismus
byl	být	k5eAaImAgInS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
směšován	směšován	k2eAgInSc4d1	směšován
pojem	pojem	k1gInSc4	pojem
čarodějnictví	čarodějnictví	k1gNnSc2	čarodějnictví
a	a	k8xC	a
černé	černý	k2eAgFnSc2d1	černá
magie	magie	k1gFnSc2	magie
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
praktiky	praktika	k1gFnPc4	praktika
nekromantů	nekromant	k1gMnPc2	nekromant
nazývaných	nazývaný	k2eAgMnPc2d1	nazývaný
slovem	slovo	k1gNnSc7	slovo
šoel	šoel	k1gInSc1	šoel
́	́	k?	́
<g/>
ov	ov	k?	ov
"	"	kIx"	"
<g/>
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
zpovídá	zpovídat	k5eAaImIp3nS	zpovídat
přízrak	přízrak	k1gInSc4	přízrak
<g/>
"	"	kIx"	"
a	a	k8xC	a
doreš	dorat	k5eAaBmIp2nS	dorat
el	ela	k1gFnPc2	ela
ha-metim	haetim	k1gMnSc1	ha-metim
"	"	kIx"	"
<g/>
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
zpovídá	zpovídat	k5eAaImIp3nS	zpovídat
mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejznámější	známý	k2eAgFnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
z	z	k7c2	z
Endoru	Endor	k1gInSc2	Endor
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
formou	forma	k1gFnSc7	forma
bylo	být	k5eAaImAgNnS	být
kášaf	kášaf	k1gInSc4	kášaf
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
skrytem	skryt	k1gInSc7	skryt
či	či	k8xC	či
temnotou	temnota	k1gFnSc7	temnota
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
praktikant	praktikant	k1gMnSc1	praktikant
se	se	k3xPyFc4	se
nazýval	nazývat	k5eAaImAgMnS	nazývat
mekhashshef	mekhashshef	k1gMnSc1	mekhashshef
<g/>
,	,	kIx,	,
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
ze	z	k7c2	z
života	život	k1gInSc2	život
společenstv	společenstva	k1gFnPc2	společenstva
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
praktikantka	praktikantka	k1gFnSc1	praktikantka
mekashshefah	mekashshefaha	k1gFnPc2	mekashshefaha
zasluhuje	zasluhovat	k5eAaImIp3nS	zasluhovat
trest	trest	k1gInSc1	trest
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Židům	Žid	k1gMnPc3	Žid
bylo	být	k5eAaImAgNnS	být
známé	známý	k2eAgNnSc1d1	známé
také	také	k9	také
uhranutí	uhranutí	k1gNnSc1	uhranutí
quiah	quiaha	k1gFnPc2	quiaha
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
závist	závist	k1gFnSc1	závist
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
žárlivost	žárlivost	k1gFnSc1	žárlivost
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
zlé	zlý	k2eAgNnSc1d1	zlé
oko	oko	k1gNnSc1	oko
́	́	k?	́
<g/>
ajin	ajin	k1gMnSc1	ajin
ra	ra	k0	ra
<g/>
́	́	k?	́
<g/>
ah	ah	k0	ah
<g/>
.	.	kIx.	.
</s>
<s>
Čarodějové	čaroděj	k1gMnPc1	čaroděj
byly	být	k5eAaImAgFnP	být
jinak	jinak	k6eAd1	jinak
nazýváni	nazývat	k5eAaImNgMnP	nazývat
n	n	k0	n
<g/>
́	́	k?	́
<g/>
hašim	hašim	k1gInSc1	hašim
"	"	kIx"	"
<g/>
hadi	had	k1gMnPc1	had
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
zákoně	zákon	k1gInSc6	zákon
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
několik	několik	k4yIc4	několik
přímých	přímý	k2eAgInPc2d1	přímý
a	a	k8xC	a
jednoznačných	jednoznačný	k2eAgInPc2d1	jednoznačný
zákazů	zákaz	k1gInPc2	zákaz
čarodějnictví	čarodějnictví	k1gNnSc2	čarodějnictví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Východní	východní	k2eAgFnSc2d1	východní
Asie	Asie	k1gFnSc2	Asie
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
existovali	existovat	k5eAaImAgMnP	existovat
čarodějnice	čarodějnice	k1gFnPc4	čarodějnice
i	i	k8xC	i
čarodějové	čaroděj	k1gMnPc1	čaroděj
zvaní	zvaní	k1gNnSc2	zvaní
wu	wu	k?	wu
<g/>
,	,	kIx,	,
známí	známit	k5eAaImIp3nS	známit
už	už	k6eAd1	už
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
historických	historický	k2eAgInPc2d1	historický
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
období	období	k1gNnSc2	období
pozdní	pozdní	k2eAgFnSc2d1	pozdní
dynastie	dynastie	k1gFnSc2	dynastie
Čou	Čou	k1gFnSc2	Čou
existují	existovat	k5eAaImIp3nP	existovat
záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
odstupu	odstup	k1gInSc6	odstup
státních	státní	k2eAgMnPc2d1	státní
kněží	kněz	k1gMnPc2	kněz
vůči	vůči	k7c3	vůči
čarodějům	čaroděj	k1gMnPc3	čaroděj
<g/>
.	.	kIx.	.
</s>
<s>
Wu	Wu	k?	Wu
vypuzovali	vypuzovat	k5eAaImAgMnP	vypuzovat
zlo	zlo	k1gNnSc4	zlo
a	a	k8xC	a
choroby	choroba	k1gFnPc4	choroba
z	z	k7c2	z
obydlí	obydlí	k1gNnSc2	obydlí
a	a	k8xC	a
provozovali	provozovat	k5eAaImAgMnP	provozovat
své	svůj	k3xOyFgNnSc4	svůj
řemeslo	řemeslo	k1gNnSc4	řemeslo
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
transu	trans	k1gInSc2	trans
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
ústy	ústa	k1gNnPc7	ústa
mohli	moct	k5eAaImAgMnP	moct
mluvit	mluvit	k5eAaImF	mluvit
mrtví	mrtvý	k2eAgMnPc1d1	mrtvý
<g/>
,	,	kIx,	,
posedlí	posedlit	k5eAaPmIp3nP	posedlit
byly	být	k5eAaImAgFnP	být
nazýváni	nazývat	k5eAaImNgMnP	nazývat
ling-pao	lingao	k6eAd1	ling-pao
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
jsou	být	k5eAaImIp3nP	být
čarodějkami	čarodějka	k1gFnPc7	čarodějka
vždy	vždy	k6eAd1	vždy
ženy	žena	k1gFnPc1	žena
nazývané	nazývaný	k2eAgFnPc1d1	nazývaná
mudang	mudang	k1gInSc4	mudang
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
slepé	slepý	k2eAgNnSc1d1	slepé
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
praxe	praxe	k1gFnSc1	praxe
je	být	k5eAaImIp3nS	být
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
extatickým	extatický	k2eAgInSc7d1	extatický
tancem	tanec	k1gInSc7	tanec
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
sibiřskému	sibiřský	k2eAgInSc3d1	sibiřský
šamanismu	šamanismus	k1gInSc3	šamanismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
existují	existovat	k5eAaImIp3nP	existovat
dodnes	dodnes	k6eAd1	dodnes
praktikující	praktikující	k2eAgInSc4d1	praktikující
mudang	mudang	k1gInSc4	mudang
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
ale	ale	k9	ale
k	k	k7c3	k
nejnižším	nízký	k2eAgFnPc3d3	nejnižší
sociálním	sociální	k2eAgFnPc3d1	sociální
vrstvám	vrstva	k1gFnPc3	vrstva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Moderní	moderní	k2eAgNnSc1d1	moderní
čarodějnictví	čarodějnictví	k1gNnSc1	čarodějnictví
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
k	k	k7c3	k
čarodějnictví	čarodějnictví	k1gNnSc3	čarodějnictví
hlásí	hlásit	k5eAaImIp3nP	hlásit
především	především	k9	především
vyznavači	vyznavač	k1gMnPc1	vyznavač
kultu	kult	k1gInSc2	kult
Wicca	Wiccum	k1gNnSc2	Wiccum
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
novopohanských	novopohanský	k2eAgNnPc2d1	novopohanské
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
praktikováno	praktikován	k2eAgNnSc1d1	praktikováno
i	i	k9	i
bez	bez	k7c2	bez
náboženského	náboženský	k2eAgInSc2d1	náboženský
obsahu	obsah	k1gInSc2	obsah
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
věštěním	věštění	k1gNnSc7	věštění
a	a	k8xC	a
bylinkářstvím	bylinkářství	k1gNnSc7	bylinkářství
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
blízko	blízko	k6eAd1	blízko
zmíněnému	zmíněný	k2eAgNnSc3d1	zmíněné
novopohanství	novopohanství	k1gNnSc3	novopohanství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
k	k	k7c3	k
neošamanismu	neošamanismus	k1gInSc3	neošamanismus
a	a	k8xC	a
New	New	k1gFnSc3	New
Age	Age	k1gFnSc3	Age
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Wicca	Wicca	k6eAd1	Wicca
je	být	k5eAaImIp3nS	být
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
Gerald	Geralda	k1gFnPc2	Geralda
Gardner	Gardnra	k1gFnPc2	Gardnra
jako	jako	k8xS	jako
mysterijní	mysterijní	k2eAgFnSc4d1	mysterijní
tradici	tradice	k1gFnSc4	tradice
uctívající	uctívající	k2eAgFnSc4d1	uctívající
Bohyni	bohyně	k1gFnSc4	bohyně
a	a	k8xC	a
Boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
inspirovanou	inspirovaný	k2eAgFnSc4d1	inspirovaná
pohanstvím	pohanství	k1gNnSc7	pohanství
<g/>
,	,	kIx,	,
lidovým	lidový	k2eAgNnSc7d1	lidové
čarodějnictvím	čarodějnictví	k1gNnSc7	čarodějnictví
i	i	k8xC	i
klasickým	klasický	k2eAgInSc7d1	klasický
okultismem	okultismus	k1gInSc7	okultismus
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vzešli	vzejít	k5eAaPmAgMnP	vzejít
další	další	k2eAgFnPc4d1	další
tradice	tradice	k1gFnPc4	tradice
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
k	k	k7c3	k
nejznámějším	známý	k2eAgInSc7d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
Saská	saský	k2eAgFnSc1d1	saská
Wicca	Wicca	k1gFnSc1	Wicca
<g/>
,	,	kIx,	,
Dianická	Dianický	k2eAgFnSc1d1	Dianická
Wicca	Wicca	k1gFnSc1	Wicca
nebo	nebo	k8xC	nebo
Reclaiming	Reclaiming	k1gInSc1	Reclaiming
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ostatním	ostatní	k2eAgFnPc3d1	ostatní
významným	významný	k2eAgFnPc3d1	významná
čarodějnickým	čarodějnický	k2eAgFnPc3d1	čarodějnická
tradicím	tradice	k1gFnPc3	tradice
patří	patřit	k5eAaImIp3nP	patřit
italská	italský	k2eAgNnPc1d1	italské
Stregheria	Stregherium	k1gNnPc1	Stregherium
a	a	k8xC	a
Feri	Fer	k1gFnPc1	Fer
<g/>
,	,	kIx,	,
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
na	na	k7c4	na
extatické	extatický	k2eAgFnPc4d1	extatická
zkušenosti	zkušenost	k1gFnPc4	zkušenost
a	a	k8xC	a
sexuální	sexuální	k2eAgFnSc4d1	sexuální
mystiku	mystika	k1gFnSc4	mystika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
pronásledování	pronásledování	k1gNnSc3	pronásledování
a	a	k8xC	a
zabíjení	zabíjení	k1gNnSc4	zabíjení
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
obviněných	obviněný	k1gMnPc2	obviněný
z	z	k7c2	z
čarodějnictví	čarodějnictví	k1gNnSc2	čarodějnictví
stále	stále	k6eAd1	stále
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
subsaharské	subsaharský	k2eAgFnSc6d1	subsaharská
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
v	v	k7c6	v
Saúdské	saúdský	k2eAgFnSc6d1	Saúdská
Arábii	Arábie	k1gFnSc6	Arábie
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Papui-Nové	Papui-Nové	k2eAgFnSc6d1	Papui-Nové
Guineji	Guinea	k1gFnSc6	Guinea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Populární	populární	k2eAgFnSc1d1	populární
kultura	kultura	k1gFnSc1	kultura
===	===	k?	===
</s>
</p>
<p>
<s>
Čarodějnictví	čarodějnictví	k1gNnSc1	čarodějnictví
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
mýtů	mýtus	k1gInPc2	mýtus
<g/>
,	,	kIx,	,
pověstí	pověst	k1gFnPc2	pověst
a	a	k8xC	a
pohádek	pohádka	k1gFnPc2	pohádka
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
populární	populární	k2eAgFnSc6d1	populární
kultuře	kultura	k1gFnSc6	kultura
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c4	v
fantasy	fantas	k1gInPc4	fantas
a	a	k8xC	a
horroru	horror	k1gInSc2	horror
<g/>
,	,	kIx,	,
příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
film	film	k1gInSc4	film
Čarodějky	čarodějka	k1gFnSc2	čarodějka
z	z	k7c2	z
Eastwicku	Eastwick	k1gInSc2	Eastwick
nebo	nebo	k8xC	nebo
knihy	kniha	k1gFnSc2	kniha
jako	jako	k8xC	jako
Čaroděj	čaroděj	k1gMnSc1	čaroděj
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
Oz	Oz	k1gFnPc1	Oz
či	či	k8xC	či
Čarodějky	čarodějka	k1gFnPc1	čarodějka
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
děl	dělo	k1gNnPc2	dělo
je	být	k5eAaImIp3nS	být
také	také	k9	také
inspirováno	inspirovat	k5eAaBmNgNnS	inspirovat
čarodějnickým	čarodějnický	k2eAgNnSc7d1	čarodějnické
procesy	proces	k1gInPc1	proces
jako	jako	k8xS	jako
filmy	film	k1gInPc1	film
Čarodějky	čarodějka	k1gFnSc2	čarodějka
ze	z	k7c2	z
Salemu	Salem	k1gInSc2	Salem
nebo	nebo	k8xC	nebo
Kladivo	kladivo	k1gNnSc1	kladivo
na	na	k7c4	na
čarodějnice	čarodějnice	k1gFnPc4	čarodějnice
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgNnSc1d1	moderní
čarodějnictví	čarodějnictví	k1gNnSc1	čarodějnictví
se	se	k3xPyFc4	se
zas	zas	k6eAd1	zas
odráží	odrážet	k5eAaImIp3nS	odrážet
v	v	k7c6	v
seriálech	seriál	k1gInPc6	seriál
Buffy	buffa	k1gFnSc2	buffa
<g/>
,	,	kIx,	,
přemožitelka	přemožitelka	k1gFnSc1	přemožitelka
upírů	upír	k1gMnPc2	upír
nebo	nebo	k8xC	nebo
Čarodějky	čarodějka	k1gFnSc2	čarodějka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
kniha	kniha	k1gFnSc1	kniha
o	o	k7c4	o
mytologii	mytologie	k1gFnSc4	mytologie
starých	starý	k2eAgInPc2d1	starý
Slovanů	Slovan	k1gInPc2	Slovan
<g/>
,	,	kIx,	,
o	o	k7c6	o
čarodějnicích	čarodějnice	k1gFnPc6	čarodějnice
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
části	část	k1gFnSc2	část
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
kapitole	kapitola	k1gFnSc6	kapitola
-	-	kIx~	-
HOSTINSKÝ	hostinský	k1gMnSc1	hostinský
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Záboj	Záboj	k1gMnSc1	Záboj
<g/>
.	.	kIx.	.
</s>
<s>
Stará	Stará	k1gFnSc1	Stará
vieronauka	vieronauk	k1gMnSc2	vieronauk
slovenská	slovenský	k2eAgNnPc1d1	slovenské
:	:	kIx,	:
Vek	veka	k1gFnPc2	veka
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
kniha	kniha	k1gFnSc1	kniha
1	[number]	k4	1
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
Pešť	Pešť	k1gFnSc1	Pešť
<g/>
:	:	kIx,	:
Minerva	Minerva	k1gFnSc1	Minerva
<g/>
,	,	kIx,	,
1871	[number]	k4	1871
<g/>
.	.	kIx.	.
122	[number]	k4	122
s.	s.	k?	s.
-	-	kIx~	-
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
v	v	k7c6	v
Digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
UKB	UKB	kA	UKB
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
čarodějnictví	čarodějnictví	k1gNnSc2	čarodějnictví
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Čaroději	čaroděj	k1gMnPc7	čaroděj
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Čarodějnice	čarodějnice	k1gFnSc2	čarodějnice
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
DINZELBACHER	DINZELBACHER	kA	DINZELBACHER
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
<g/>
.	.	kIx.	.
</s>
<s>
Světice	světice	k1gFnSc1	světice
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
čarodějky	čarodějka	k1gFnPc1	čarodějka
<g/>
?	?	kIx.	?
</s>
<s>
:	:	kIx,	:
osudy	osud	k1gInPc4	osud
"	"	kIx"	"
<g/>
jiných	jiný	k2eAgFnPc2d1	jiná
<g/>
"	"	kIx"	"
žen	žena	k1gFnPc2	žena
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
a	a	k8xC	a
novověku	novověk	k1gInSc6	novověk
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
283	[number]	k4	283
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
650	[number]	k4	650
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GARDNER	GARDNER	kA	GARDNER
<g/>
,	,	kIx,	,
Gerald	Gerald	k1gMnSc1	Gerald
B.	B.	kA	B.
<g/>
,	,	kIx,	,
Moderní	moderní	k2eAgNnSc1d1	moderní
čarodějnictví	čarodějnictví	k1gNnSc1	čarodějnictví
<g/>
,	,	kIx,	,
GRADA	GRADA	kA	GRADA
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-247-3218-3	[number]	k4	978-80-247-3218-3
</s>
</p>
<p>
<s>
KOBRLOVÁ	KOBRLOVÁ	kA	KOBRLOVÁ
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
<g/>
.	.	kIx.	.
</s>
<s>
Čarodějnictví	čarodějnictví	k1gNnSc1	čarodějnictví
-	-	kIx~	-
iluze	iluze	k1gFnSc1	iluze
<g/>
,	,	kIx,	,
za	za	k7c4	za
niž	jenž	k3xRgFnSc4	jenž
se	se	k3xPyFc4	se
platilo	platit	k5eAaImAgNnS	platit
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
:	:	kIx,	:
časopis	časopis	k1gInSc1	časopis
pro	pro	k7c4	pro
výuku	výuka	k1gFnSc4	výuka
dějepisu	dějepis	k1gInSc2	dějepis
a	a	k8xC	a
popularizaci	popularizace	k1gFnSc4	popularizace
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
194	[number]	k4	194
<g/>
-	-	kIx~	-
<g/>
203	[number]	k4	203
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210-6097	[number]	k4	1210-6097
</s>
</p>
<p>
<s>
M.	M.	kA	M.
NOLA	NOLA	kA	NOLA
<g/>
,	,	kIx,	,
Alfonso	Alfonso	k1gMnSc1	Alfonso
<g/>
,	,	kIx,	,
Ďábel	ďábel	k1gMnSc1	ďábel
a	a	k8xC	a
podoby	podoba	k1gFnPc1	podoba
zla	zlo	k1gNnSc2	zlo
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
lidstva	lidstvo	k1gNnSc2	lidstvo
<g/>
,	,	kIx,	,
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7207-114-9	[number]	k4	80-7207-114-9
</s>
</p>
<p>
<s>
STREERER	STREERER	kA	STREERER
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
<g/>
,	,	kIx,	,
Čarodějnictví	čarodějnictví	k1gNnSc1	čarodějnictví
:	:	kIx,	:
Tajné	tajný	k2eAgFnPc1d1	tajná
dějiny	dějiny	k1gFnPc1	dějiny
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-204-1650-6	[number]	k4	978-80-204-1650-6
</s>
</p>
<p>
<s>
STEJSKAL	Stejskal	k1gMnSc1	Stejskal
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
,	,	kIx,	,
Labyrintem	labyrint	k1gInSc7	labyrint
míst	místo	k1gNnPc2	místo
klatých	klatý	k2eAgMnPc2d1	klatý
<g/>
,	,	kIx,	,
Eminent	Eminent	k1gMnSc1	Eminent
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Čarodějnické	čarodějnický	k2eAgInPc1d1	čarodějnický
procesy	proces	k1gInPc1	proces
</s>
</p>
<p>
<s>
Magie	magie	k1gFnSc1	magie
</s>
</p>
<p>
<s>
Wicca	Wicca	k6eAd1	Wicca
</s>
</p>
