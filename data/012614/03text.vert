<p>
<s>
Zuzana	Zuzana	k1gFnSc1	Zuzana
Navarová	Navarový	k2eAgFnSc1d1	Navarová
de	de	k?	de
Tejada	Tejada	k1gFnSc1	Tejada
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1959	[number]	k4	1959
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
–	–	k?	–
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2004	[number]	k4	2004
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
folková	folkový	k2eAgFnSc1d1	folková
skladatelka	skladatelka	k1gFnSc1	skladatelka
a	a	k8xC	a
textařka	textařka	k1gFnSc1	textařka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stručný	stručný	k2eAgInSc4d1	stručný
životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
žila	žít	k5eAaImAgFnS	žít
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studovala	studovat	k5eAaImAgFnS	studovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
J.	J.	kA	J.
K.	K.	kA	K.
Tyla	Tyl	k1gMnSc2	Tyl
<g/>
.	.	kIx.	.
</s>
<s>
Vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
španělštinu	španělština	k1gFnSc4	španělština
a	a	k8xC	a
češtinu	čeština	k1gFnSc4	čeština
(	(	kIx(	(
<g/>
učitelský	učitelský	k2eAgInSc1d1	učitelský
obor	obor	k1gInSc1	obor
<g/>
)	)	kIx)	)
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiu	studio	k1gNnSc6	studio
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
jako	jako	k9	jako
učitelka	učitelka	k1gFnSc1	učitelka
španělštiny	španělština	k1gFnSc2	španělština
na	na	k7c6	na
jazykové	jazykový	k2eAgFnSc6d1	jazyková
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
studovala	studovat	k5eAaImAgFnS	studovat
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
vystupováním	vystupování	k1gNnSc7	vystupování
konzervatoř	konzervatoř	k1gFnSc1	konzervatoř
obor	obor	k1gInSc4	obor
populární	populární	k2eAgInSc1d1	populární
zpěv	zpěv	k1gInSc1	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
zpívala	zpívat	k5eAaImAgFnS	zpívat
se	se	k3xPyFc4	se
skupinou	skupina	k1gFnSc7	skupina
Nerez	nerez	k1gInSc1	nerez
<g/>
,	,	kIx,	,
její	její	k3xOp3gNnSc1	její
první	první	k4xOgNnSc1	první
sólové	sólový	k2eAgNnSc1d1	sólové
album	album	k1gNnSc1	album
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
nahrála	nahrát	k5eAaPmAgFnS	nahrát
album	album	k1gNnSc4	album
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
Tres	tresa	k1gFnPc2	tresa
a	a	k8xC	a
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
dalších	další	k2eAgInPc2d1	další
pět	pět	k4xCc1	pět
alb	album	k1gNnPc2	album
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Koa	Koa	k1gFnPc2	Koa
<g/>
.	.	kIx.	.
</s>
<s>
Skládala	skládat	k5eAaImAgFnS	skládat
také	také	k9	také
scénickou	scénický	k2eAgFnSc4d1	scénická
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
divadelním	divadelní	k2eAgNnPc3d1	divadelní
představením	představení	k1gNnPc3	představení
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
producentkou	producentka	k1gFnSc7	producentka
(	(	kIx(	(
<g/>
Věra	Věra	k1gFnSc1	Věra
Bílá	bílý	k2eAgFnSc1d1	bílá
<g/>
,	,	kIx,	,
Rom	Rom	k1gMnSc1	Rom
-	-	kIx~	-
Pop	pop	k1gInSc1	pop
<g/>
,	,	kIx,	,
Kale	kale	k6eAd1	kale
<g/>
)	)	kIx)	)
a	a	k8xC	a
objevila	objevit	k5eAaPmAgFnS	objevit
zpěvačku	zpěvačka	k1gFnSc4	zpěvačka
Radůzu	Radůz	k1gInSc2	Radůz
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
stála	stát	k5eAaImAgFnS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Nadace	nadace	k1gFnSc2	nadace
Život	život	k1gInSc4	život
umělce	umělec	k1gMnSc2	umělec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dlouho	dlouho	k6eAd1	dlouho
pobývala	pobývat	k5eAaImAgFnS	pobývat
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
potkala	potkat	k5eAaPmAgFnS	potkat
svého	svůj	k3xOyFgMnSc4	svůj
manžela	manžel	k1gMnSc4	manžel
Luíse	Luís	k1gMnSc4	Luís
<g/>
.	.	kIx.	.
</s>
<s>
Kubánská	kubánský	k2eAgFnSc1d1	kubánská
hudba	hudba	k1gFnSc1	hudba
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
její	její	k3xOp3gFnPc4	její
vlastní	vlastní	k2eAgFnPc4d1	vlastní
skladby	skladba	k1gFnPc4	skladba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
45	[number]	k4	45
let	léto	k1gNnPc2	léto
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
spojenou	spojený	k2eAgFnSc4d1	spojená
s	s	k7c7	s
cirhózou	cirhóza	k1gFnSc7	cirhóza
jater	játra	k1gNnPc2	játra
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pohřbena	pohřbít	k5eAaPmNgFnS	pohřbít
na	na	k7c6	na
Vyšehradském	vyšehradský	k2eAgInSc6d1	vyšehradský
hřbitově	hřbitov	k1gInSc6	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
počest	počest	k1gFnSc4	počest
bylo	být	k5eAaImAgNnS	být
ocenění	ocenění	k1gNnSc1	ocenění
Nadace	nadace	k1gFnSc2	nadace
Život	život	k1gInSc1	život
umělce	umělec	k1gMnSc2	umělec
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zakládala	zakládat	k5eAaImAgFnS	zakládat
Zuzana	Zuzana	k1gFnSc1	Zuzana
Navarová	Navarová	k1gFnSc1	Navarová
<g/>
,	,	kIx,	,
pojmenováno	pojmenován	k2eAgNnSc1d1	pojmenováno
Cena	cena	k1gFnSc1	cena
Zuzany	Zuzana	k1gFnSc2	Zuzana
Navarové	Navarová	k1gFnSc2	Navarová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tvorba	tvorba	k1gFnSc1	tvorba
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Hudební	hudební	k2eAgFnSc1d1	hudební
tvorba	tvorba	k1gFnSc1	tvorba
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
zpívala	zpívat	k5eAaImAgFnS	zpívat
se	se	k3xPyFc4	se
skupinou	skupina	k1gFnSc7	skupina
Nerez	nerez	k1gInSc1	nerez
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
oceňována	oceňovat	k5eAaImNgFnS	oceňovat
jako	jako	k8xC	jako
výjimečná	výjimečný	k2eAgFnSc1d1	výjimečná
sólistka	sólistka	k1gFnSc1	sólistka
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
si	se	k3xPyFc3	se
uznání	uznání	k1gNnSc4	uznání
odborné	odborný	k2eAgFnSc2d1	odborná
kritiky	kritika	k1gFnSc2	kritika
i	i	k8xC	i
posluchačů	posluchač	k1gMnPc2	posluchač
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Nerez	nerez	k2eAgFnSc7d1	nerez
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
první	první	k4xOgNnSc4	první
sólové	sólový	k2eAgNnSc4d1	sólové
album	album	k1gNnSc4	album
Caribe	Carib	k1gInSc5	Carib
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
navázalo	navázat	k5eAaPmAgNnS	navázat
koncertování	koncertování	k1gNnSc1	koncertování
s	s	k7c7	s
příležitostným	příležitostný	k2eAgNnSc7d1	příležitostné
seskupením	seskupení	k1gNnSc7	seskupení
Caribe	Carib	k1gInSc5	Carib
Jazz	jazz	k1gInSc4	jazz
Quintet	Quinteta	k1gFnPc2	Quinteta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ze	z	k7c2	z
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
začala	začít	k5eAaPmAgFnS	začít
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
kolumbijským	kolumbijský	k2eAgMnSc7d1	kolumbijský
písničkářem	písničkář	k1gMnSc7	písničkář
a	a	k8xC	a
kytaristou	kytarista	k1gMnSc7	kytarista
Ivánem	Iván	k1gMnSc7	Iván
Gutiérrezem	Gutiérrez	k1gInSc7	Gutiérrez
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
baskytaristou	baskytarista	k1gMnSc7	baskytarista
Karlem	Karel	k1gMnSc7	Karel
Cábou	Cába	k1gMnSc7	Cába
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
trojčlenné	trojčlenný	k2eAgNnSc4d1	trojčlenné
uskupení	uskupení	k1gNnSc4	uskupení
Tres	tresa	k1gFnPc2	tresa
a	a	k8xC	a
vydali	vydat	k5eAaPmAgMnP	vydat
stejnojmenné	stejnojmenný	k2eAgNnSc4d1	stejnojmenné
album	album	k1gNnSc4	album
se	s	k7c7	s
staršími	starý	k2eAgFnPc7d2	starší
Ivánovými	Ivánův	k2eAgFnPc7d1	Ivánův
písněmi	píseň	k1gFnPc7	píseň
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
nahráli	nahrát	k5eAaBmAgMnP	nahrát
ještě	ještě	k9	ještě
jednu	jeden	k4xCgFnSc4	jeden
desku	deska	k1gFnSc4	deska
s	s	k7c7	s
písněmi	píseň	k1gFnPc7	píseň
Navarové	Navarová	k1gFnSc2	Navarová
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
však	však	k9	však
dodnes	dodnes	k6eAd1	dodnes
nevyšla	vyjít	k5eNaPmAgFnS	vyjít
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
dvojice	dvojice	k1gFnSc2	dvojice
Zuzana	Zuzana	k1gFnSc1	Zuzana
Navarová	Navarová	k1gFnSc1	Navarová
–	–	k?	–
Iván	Iván	k1gMnSc1	Iván
Gutiérrez	Gutiérrez	k1gMnSc1	Gutiérrez
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
skupina	skupina	k1gFnSc1	skupina
Koa	Koa	k1gFnSc1	Koa
–	–	k?	–
Mário	Mário	k6eAd1	Mário
Bihári	Bihári	k1gNnSc1	Bihári
(	(	kIx(	(
<g/>
akordeon	akordeon	k1gInSc1	akordeon
<g/>
,	,	kIx,	,
klavír	klavír	k1gInSc1	klavír
<g/>
,	,	kIx,	,
klarinet	klarinet	k1gInSc1	klarinet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Camilo	Camila	k1gFnSc5	Camila
Caller	Caller	k1gInSc1	Caller
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgFnSc1d1	bicí
a	a	k8xC	a
perkuse	perkuse	k1gFnSc1	perkuse
<g/>
)	)	kIx)	)
a	a	k8xC	a
František	František	k1gMnSc1	František
Raba	rab	k1gMnSc2	rab
(	(	kIx(	(
<g/>
kontrabas	kontrabas	k1gInSc1	kontrabas
<g/>
)	)	kIx)	)
a	a	k8xC	a
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
album	album	k1gNnSc4	album
Skleněná	skleněný	k2eAgFnSc1d1	skleněná
vrba	vrba	k1gFnSc1	vrba
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yQgNnSc4	který
dostala	dostat	k5eAaPmAgFnS	dostat
cenu	cena	k1gFnSc4	cena
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
ponorka	ponorka	k1gFnSc1	ponorka
<g/>
)	)	kIx)	)
a	a	k8xC	a
koncertní	koncertní	k2eAgNnSc1d1	koncertní
Zelené	Zelené	k2eAgNnSc1d1	Zelené
album	album	k1gNnSc1	album
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Gutiérrezově	Gutiérrezův	k2eAgInSc6d1	Gutiérrezův
odchodu	odchod	k1gInSc6	odchod
do	do	k7c2	do
Španělska	Španělsko	k1gNnSc2	Španělsko
vyplnil	vyplnit	k5eAaPmAgInS	vyplnit
uprázdněné	uprázdněný	k2eAgNnSc4d1	uprázdněné
místo	místo	k1gNnSc4	místo
kytaristy	kytarista	k1gMnSc2	kytarista
Omar	Omar	k1gMnSc1	Omar
Khaouaj	Khaouaj	k1gMnSc1	Khaouaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovéto	takovýto	k3xDgFnSc6	takovýto
sestavě	sestava	k1gFnSc6	sestava
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
dvě	dva	k4xCgNnPc4	dva
alba	album	k1gNnPc4	album
–	–	k?	–
Barvy	barva	k1gFnSc2	barva
všecky	všecek	k3xTgFnPc1	všecek
(	(	kIx(	(
<g/>
oceněna	ocenit	k5eAaPmNgFnS	ocenit
českým	český	k2eAgMnSc7d1	český
Andělem	Anděl	k1gMnSc7	Anděl
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
a	a	k8xC	a
poslední	poslední	k2eAgNnSc1d1	poslední
album	album	k1gNnSc1	album
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
Jako	jako	k8xS	jako
Šántidéví	Šántidéví	k1gNnSc1	Šántidéví
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
;	;	kIx,	;
kromě	kromě	k7c2	kromě
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
romštiny	romština	k1gFnSc2	romština
<g/>
,	,	kIx,	,
slovenštiny	slovenština	k1gFnSc2	slovenština
<g/>
,	,	kIx,	,
ruštiny	ruština	k1gFnSc2	ruština
a	a	k8xC	a
jidiš	jidiš	k6eAd1	jidiš
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
Zuzana	Zuzana	k1gFnSc1	Zuzana
Navarová	Navarový	k2eAgFnSc1d1	Navarová
zpívá	zpívat	k5eAaImIp3nS	zpívat
i	i	k9	i
kečuánsky	kečuánsky	k6eAd1	kečuánsky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Méně	málo	k6eAd2	málo
přízně	přízně	k6eAd1	přízně
než	než	k8xS	než
u	u	k7c2	u
kritiky	kritika	k1gFnSc2	kritika
a	a	k8xC	a
posluchačů	posluchač	k1gMnPc2	posluchač
si	se	k3xPyFc3	se
získala	získat	k5eAaPmAgFnS	získat
u	u	k7c2	u
nahrávacích	nahrávací	k2eAgFnPc2d1	nahrávací
firem	firma	k1gFnPc2	firma
a	a	k8xC	a
v	v	k7c6	v
rozhlasových	rozhlasový	k2eAgFnPc6d1	rozhlasová
stanicích	stanice	k1gFnPc6	stanice
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
její	její	k3xOp3gFnSc4	její
hudbu	hudba	k1gFnSc4	hudba
zařadily	zařadit	k5eAaPmAgInP	zařadit
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
menšinových	menšinový	k2eAgInPc2d1	menšinový
žánrů	žánr	k1gInPc2	žánr
a	a	k8xC	a
hrály	hrát	k5eAaImAgFnP	hrát
ji	on	k3xPp3gFnSc4	on
jen	jen	k9	jen
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Navarová	Navarová	k1gFnSc1	Navarová
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
Koa	Koa	k1gFnSc1	Koa
prosadila	prosadit	k5eAaPmAgFnS	prosadit
do	do	k7c2	do
vysílání	vysílání	k1gNnSc2	vysílání
mainstreamových	mainstreamový	k2eAgNnPc2d1	mainstreamové
radií	radio	k1gNnPc2	radio
se	s	k7c7	s
skladbou	skladba	k1gFnSc7	skladba
"	"	kIx"	"
<g/>
Marie	Marie	k1gFnSc1	Marie
<g/>
"	"	kIx"	"
z	z	k7c2	z
alba	album	k1gNnSc2	album
Jako	jako	k8xS	jako
Šántidéví	Šántidéví	k1gNnSc2	Šántidéví
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
jí	on	k3xPp3gFnSc7	on
však	však	k9	však
zbývalo	zbývat	k5eAaImAgNnS	zbývat
už	už	k6eAd1	už
jen	jen	k9	jen
několik	několik	k4yIc4	několik
posledních	poslední	k2eAgInPc2d1	poslední
měsíců	měsíc	k1gInPc2	měsíc
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
posledních	poslední	k2eAgNnPc6d1	poslední
albech	album	k1gNnPc6	album
se	se	k3xPyFc4	se
psala	psát	k5eAaImAgFnS	psát
jako	jako	k9	jako
Zuzana	Zuzana	k1gFnSc1	Zuzana
Navarová	Navarová	k1gFnSc1	Navarová
<g/>
,	,	kIx,	,
d.	d.	k?	d.
T.	T.	kA	T.
(	(	kIx(	(
<g/>
de	de	k?	de
Tejada	Tejada	k1gFnSc1	Tejada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Texty	text	k1gInPc1	text
písní	píseň	k1gFnPc2	píseň
===	===	k?	===
</s>
</p>
<p>
<s>
Pozoruhodné	pozoruhodný	k2eAgInPc1d1	pozoruhodný
jsou	být	k5eAaImIp3nP	být
její	její	k3xOp3gInPc1	její
písňové	písňový	k2eAgInPc1d1	písňový
texty	text	k1gInPc1	text
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
dokázala	dokázat	k5eAaPmAgNnP	dokázat
oprostit	oprostit	k5eAaPmF	oprostit
od	od	k7c2	od
charakteristických	charakteristický	k2eAgNnPc2d1	charakteristické
folkových	folkový	k2eAgNnPc2d1	folkové
a	a	k8xC	a
countryových	countryový	k2eAgNnPc2d1	countryové
klišé	klišé	k1gNnPc2	klišé
(	(	kIx(	(
<g/>
jimž	jenž	k3xRgNnPc3	jenž
se	se	k3xPyFc4	se
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
vysmívala	vysmívat	k5eAaImAgFnS	vysmívat
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
parodická	parodický	k2eAgFnSc1d1	parodická
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Cesta	cesta	k1gFnSc1	cesta
dom	dom	k?	dom
<g/>
"	"	kIx"	"
z	z	k7c2	z
alba	album	k1gNnSc2	album
Jako	jako	k8xS	jako
Šántidéví	Šántidéví	k1gNnSc2	Šántidéví
<g/>
)	)	kIx)	)
a	a	k8xC	a
vdechnout	vdechnout	k5eAaPmF	vdechnout
textu	text	k1gInSc2	text
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
poetiku	poetika	k1gFnSc4	poetika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
textu	text	k1gInSc6	text
využila	využít	k5eAaPmAgFnS	využít
nápadité	nápaditý	k2eAgInPc4d1	nápaditý
anakoluty	anakolut	k1gInPc4	anakolut
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
básnické	básnický	k2eAgInPc1d1	básnický
prvky	prvek	k1gInPc1	prvek
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
hubená	hubený	k2eAgFnSc1d1	hubená
noc	noc	k1gFnSc1	noc
přisedla	přisednout	k5eAaPmAgFnS	přisednout
si	se	k3xPyFc3	se
s	s	k7c7	s
otráveným	otrávený	k2eAgNnSc7d1	otrávené
vínem	víno	k1gNnSc7	víno
<g/>
/	/	kIx~	/
bledá	bledý	k2eAgFnSc1d1	bledá
holka	holka	k1gFnSc1	holka
přikryla	přikrýt	k5eAaPmAgFnS	přikrýt
tmu	tma	k1gFnSc4	tma
černým	černý	k2eAgInSc7d1	černý
baldachýnem	baldachýn	k1gInSc7	baldachýn
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zařadila	zařadit	k5eAaPmAgFnS	zařadit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
několik	několik	k4yIc4	několik
málo	málo	k4c4	málo
českých	český	k2eAgMnPc2d1	český
textařů	textař	k1gMnPc2	textař
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
texty	text	k1gInPc1	text
dokážou	dokázat	k5eAaPmIp3nP	dokázat
obstát	obstát	k5eAaPmF	obstát
i	i	k9	i
samy	sám	k3xTgFnPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
jako	jako	k9	jako
poezie	poezie	k1gFnSc2	poezie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Texty	text	k1gInPc4	text
písní	píseň	k1gFnPc2	píseň
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
vydalo	vydat	k5eAaPmAgNnS	vydat
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2009	[number]	k4	2009
nakladatelství	nakladatelství	k1gNnPc2	nakladatelství
Host	host	k1gMnSc1	host
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Andělská	andělský	k2eAgNnPc4d1	andělské
počta	počta	k?	počta
<g/>
.	.	kIx.	.
</s>
<s>
Editorkou	editorka	k1gFnSc7	editorka
publikace	publikace	k1gFnSc2	publikace
je	být	k5eAaImIp3nS	být
Hana	Hana	k1gFnSc1	Hana
Svanovská	Svanovský	k2eAgFnSc1d1	Svanovská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
s	s	k7c7	s
Nerezem	nerez	k1gInSc7	nerez
</s>
</p>
<p>
<s>
Porta	porta	k1gFnSc1	porta
'	'	kIx"	'
<g/>
83	[number]	k4	83
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
–	–	k?	–
různí	různý	k2eAgMnPc1d1	různý
interpreti	interpret	k1gMnPc1	interpret
</s>
</p>
<p>
<s>
Dostavník	dostavník	k1gInSc1	dostavník
21	[number]	k4	21
<g/>
:	:	kIx,	:
Tisíc	tisíc	k4xCgInSc1	tisíc
dnů	den	k1gInPc2	den
mezi	mezi	k7c7	mezi
námi	my	k3xPp1nPc7	my
/	/	kIx~	/
Za	za	k7c4	za
poledne	poledne	k1gNnSc4	poledne
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
–	–	k?	–
SP	SP	kA	SP
</s>
</p>
<p>
<s>
Imaginární	imaginární	k2eAgFnSc1d1	imaginární
hospoda	hospoda	k?	hospoda
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
–	–	k?	–
EP	EP	kA	EP
<g/>
;	;	kIx,	;
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Plíhalem	Plíhal	k1gMnSc7	Plíhal
a	a	k8xC	a
Slávkem	Slávek	k1gMnSc7	Slávek
Janouškem	Janoušek	k1gMnSc7	Janoušek
</s>
</p>
<p>
<s>
Masopust	masopust	k1gInSc1	masopust
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Na	na	k7c6	na
vařený	vařený	k2eAgInSc1d1	vařený
nudli	nudle	k1gFnSc3	nudle
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
zdi	zeď	k1gFnSc3	zeď
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
nevešlo	vejít	k5eNaPmAgNnS	vejít
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
–	–	k?	–
Nerez	nerez	k1gInSc1	nerez
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c6	na
kazetě	kazeta	k1gFnSc6	kazeta
vlastním	vlastní	k2eAgInSc7d1	vlastní
nákladem	náklad	k1gInSc7	náklad
pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
"	"	kIx"	"
<g/>
Nerez	nerez	k1gInSc1	nerez
a	a	k8xC	a
Vy	vy	k3xPp2nPc1	vy
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Stará	starý	k2eAgFnSc1d1	stará
láska	láska	k1gFnSc1	láska
Nerez	nerez	k1gFnSc1	nerez
a	a	k8xC	a
vy	vy	k3xPp2nPc1	vy
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nerez	nerez	k1gFnSc1	nerez
v	v	k7c6	v
Betlémě	Betlém	k1gInSc6	Betlém
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nerez	nerez	k2eAgFnSc1d1	nerez
antologie	antologie	k1gFnSc1	antologie
<g/>
:	:	kIx,	:
Masopust	masopust	k1gInSc1	masopust
<g/>
,	,	kIx,	,
Na	na	k7c6	na
vařený	vařený	k2eAgInSc1d1	vařený
nudli	nudle	k1gFnSc3	nudle
<g/>
,	,	kIx,	,
Ke	k	k7c3	k
zdi	zeď	k1gFnSc3	zeď
(	(	kIx(	(
<g/>
+	+	kIx~	+
bonusy	bonus	k1gInPc1	bonus
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
nevešlo	vejít	k5eNaPmAgNnS	vejít
(	(	kIx(	(
<g/>
pozdní	pozdní	k2eAgInSc1d1	pozdní
sběr	sběr	k1gInSc1	sběr
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nej	Nej	k?	Nej
nej	nej	k?	nej
nej	nej	k?	nej
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Smutkům	smutek	k1gInPc3	smutek
na	na	k7c4	na
kabát	kabát	k1gInSc4	kabát
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
–	–	k?	–
výběr	výběr	k1gInSc1	výběr
písní	píseň	k1gFnPc2	píseň
Zuzany	Zuzana	k1gFnSc2	Zuzana
Navarové	Navarová	k1gFnPc1	Navarová
včetně	včetně	k7c2	včetně
několika	několik	k4yIc2	několik
písní	píseň	k1gFnPc2	píseň
Nerezu	nerez	k1gInSc2	nerez
</s>
</p>
<p>
<s>
Do	do	k7c2	do
posledního	poslední	k2eAgInSc2d1	poslední
dechu	dech	k1gInSc2	dech
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
–	–	k?	–
výběr	výběr	k1gInSc1	výběr
písní	píseň	k1gFnPc2	píseň
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Vřešťála	Vřešťál	k1gMnSc2	Vřešťál
včetně	včetně	k7c2	včetně
několika	několik	k4yIc2	několik
písní	píseň	k1gFnPc2	píseň
Nerezu	nerez	k1gInSc2	nerez
</s>
</p>
<p>
<s>
...	...	k?	...
<g/>
a	a	k8xC	a
bastafidli	bastafidnout	k5eAaPmAgMnP	bastafidnout
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
–	–	k?	–
znovu	znovu	k6eAd1	znovu
Masopust	masopust	k1gInSc4	masopust
<g/>
,	,	kIx,	,
Na	na	k7c4	na
vařený	vařený	k2eAgInSc4d1	vařený
nudli	nudle	k1gFnSc3	nudle
a	a	k8xC	a
Ke	k	k7c3	k
zdi	zeď	k1gFnSc3	zeď
+	+	kIx~	+
bonusy	bonus	k1gInPc1	bonus
<g/>
,	,	kIx,	,
DVD	DVD	kA	DVD
a	a	k8xC	a
zpěvník	zpěvník	k1gInSc1	zpěvník
</s>
</p>
<p>
<s>
Nerez	nerez	k1gFnSc1	nerez
v	v	k7c6	v
Betlémě	Betlém	k1gInSc6	Betlém
/	/	kIx~	/
Koncert	koncert	k1gInSc1	koncert
v	v	k7c6	v
Orlové	Orlová	k1gFnSc6	Orlová
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
–	–	k?	–
reedice	reedice	k1gFnSc2	reedice
alba	album	k1gNnSc2	album
Nerez	nerez	k2eAgNnSc2d1	nerez
v	v	k7c6	v
Betlémě	Betlém	k1gInSc6	Betlém
+	+	kIx~	+
dosud	dosud	k6eAd1	dosud
nevydaný	vydaný	k2eNgInSc1d1	nevydaný
koncert	koncert	k1gInSc1	koncert
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
</s>
</p>
<p>
<s>
sólový	sólový	k2eAgInSc1d1	sólový
projekt	projekt	k1gInSc1	projekt
Caribe	Carib	k1gMnSc5	Carib
</s>
</p>
<p>
<s>
Caribe	Caribat	k5eAaPmIp3nS	Caribat
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
Tres	tresa	k1gFnPc2	tresa
</s>
</p>
<p>
<s>
Tres	tresa	k1gFnPc2	tresa
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
s	s	k7c7	s
kapelou	kapela	k1gFnSc7	kapela
Koa	Koa	k1gFnSc2	Koa
</s>
</p>
<p>
<s>
Skleněná	skleněný	k2eAgFnSc1d1	skleněná
vrba	vrba	k1gFnSc1	vrba
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zelené	Zelené	k2eAgNnSc1d1	Zelené
album	album	k1gNnSc1	album
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Barvy	barva	k1gFnPc1	barva
všecky	všecek	k3xTgFnPc4	všecek
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
Šántidéví	Šántidéví	k1gNnSc1	Šántidéví
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Koa	Koa	k?	Koa
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
–	–	k?	–
album	album	k1gNnSc1	album
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tři	tři	k4xCgFnPc4	tři
písně	píseň	k1gFnPc4	píseň
Zuzany	Zuzana	k1gFnSc2	Zuzana
Navarové	Navarový	k2eAgFnSc2d1	Navarová
</s>
</p>
<p>
<s>
další	další	k2eAgFnPc1d1	další
</s>
</p>
<p>
<s>
Vánoční	vánoční	k2eAgFnPc1d1	vánoční
písně	píseň	k1gFnPc1	píseň
a	a	k8xC	a
koledy	koleda	k1gFnPc1	koleda
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Morytáty	morytát	k1gInPc1	morytát
a	a	k8xC	a
balady	balada	k1gFnPc1	balada
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
–	–	k?	–
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
Janem	Jan	k1gMnSc7	Jan
Nedvědem	Nedvěd	k1gMnSc7	Nedvěd
<g/>
,	,	kIx,	,
Vítem	Vít	k1gMnSc7	Vít
Sázavským	sázavský	k2eAgMnSc7d1	sázavský
a	a	k8xC	a
Vlastimilem	Vlastimil	k1gMnSc7	Vlastimil
Peškou	Peška	k1gMnSc7	Peška
</s>
</p>
<p>
<s>
Sloni	slon	k1gMnPc1	slon
v	v	k7c6	v
porcelánu	porcelán	k1gInSc6	porcelán
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
–	–	k?	–
sampler	sampler	k1gInSc1	sampler
</s>
</p>
<p>
<s>
Nebe	nebe	k1gNnSc1	nebe
počká	počkat	k5eAaPmIp3nS	počkat
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
–	–	k?	–
Zuzana	Zuzana	k1gFnSc1	Zuzana
Navarová	Navarová	k1gFnSc1	Navarová
zpívá	zpívat	k5eAaImIp3nS	zpívat
pět	pět	k4xCc4	pět
písní	píseň	k1gFnPc2	píseň
na	na	k7c6	na
albu	album	k1gNnSc6	album
Karla	Karel	k1gMnSc2	Karel
Plíhala	Plíhal	k1gMnSc2	Plíhal
s	s	k7c7	s
texty	text	k1gInPc7	text
Josefa	Josef	k1gMnSc2	Josef
Kainara	Kainar	k1gMnSc2	Kainar
</s>
</p>
<p>
<s>
Smutkům	smutek	k1gInPc3	smutek
na	na	k7c4	na
kabát	kabát	k1gInSc4	kabát
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
–	–	k?	–
výběr	výběr	k1gInSc1	výběr
<g/>
,	,	kIx,	,
reedice	reedice	k1gFnSc1	reedice
na	na	k7c4	na
2LP	[number]	k4	2LP
(	(	kIx(	(
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dávný	dávný	k2eAgInSc1d1	dávný
příběh	příběh	k1gInSc1	příběh
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
Zuzanu	Zuzana	k1gFnSc4	Zuzana
Navarovou	Navarová	k1gFnSc4	Navarová
můžeme	moct	k5eAaImIp1nP	moct
slyšet	slyšet	k5eAaImF	slyšet
také	také	k9	také
na	na	k7c6	na
albu	album	k1gNnSc6	album
Radůzy	Radůza	k1gFnSc2	Radůza
Andělové	Andělová	k1gFnSc2	Andělová
z	z	k7c2	z
nebe	nebe	k1gNnSc2	nebe
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Marie	Marie	k1gFnSc1	Marie
Rottrová	Rottrový	k2eAgFnSc1d1	Rottrová
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
albu	album	k1gNnSc6	album
Podívej	podívat	k5eAaPmRp2nS	podívat
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
zpívá	zpívat	k5eAaImIp3nS	zpívat
její	její	k3xOp3gFnPc4	její
dvě	dva	k4xCgFnPc4	dva
písně	píseň	k1gFnPc4	píseň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Scénická	scénický	k2eAgFnSc1d1	scénická
hudba	hudba	k1gFnSc1	hudba
==	==	k?	==
</s>
</p>
<p>
<s>
Komponovala	komponovat	k5eAaImAgFnS	komponovat
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
divadelním	divadelní	k2eAgFnPc3d1	divadelní
inscenacím	inscenace	k1gFnPc3	inscenace
Střemhlav	střemhlav	k6eAd1	střemhlav
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
Státní	státní	k2eAgNnSc1d1	státní
divadlo	divadlo	k1gNnSc1	divadlo
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc1	soubor
HaDivadlo	HaDivadlo	k1gNnSc1	HaDivadlo
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Anděl	Anděl	k1gMnSc1	Anděl
přichází	přicházet	k5eAaImIp3nS	přicházet
do	do	k7c2	do
Babylónu	babylón	k1gInSc2	babylón
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
Vítězného	vítězný	k2eAgInSc2d1	vítězný
února	únor	k1gInSc2	únor
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sevillský	sevillský	k2eAgMnSc1d1	sevillský
svůdce	svůdce	k1gMnSc1	svůdce
a	a	k8xC	a
kamenný	kamenný	k2eAgMnSc1d1	kamenný
host	host	k1gMnSc1	host
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vytetovaná	vytetovaný	k2eAgFnSc1d1	vytetovaná
růže	růže	k1gFnSc1	růže
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Řeznické	řeznická	k1gFnSc6	řeznická
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Láska	láska	k1gFnSc1	láska
dona	dona	k1gFnSc1	dona
Perlimplína	Perlimplína	k1gFnSc1	Perlimplína
a	a	k8xC	a
vášnivost	vášnivost	k1gFnSc1	vášnivost
Belisina	Belisina	k1gFnSc1	Belisina
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Motivy	motiv	k1gInPc1	motiv
z	z	k7c2	z
písní	píseň	k1gFnPc2	píseň
Zuzany	Zuzana	k1gFnSc2	Zuzana
Navarové	Navarová	k1gFnPc1	Navarová
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgFnP	použít
v	v	k7c6	v
iscenaci	iscenace	k1gFnSc6	iscenace
Rozmarné	rozmarný	k2eAgNnSc4d1	Rozmarné
léto	léto	k1gNnSc4	léto
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
Horácké	horácký	k2eAgNnSc1d1	Horácké
divadlo	divadlo	k1gNnSc1	divadlo
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmová	filmový	k2eAgFnSc1d1	filmová
hudba	hudba	k1gFnSc1	hudba
==	==	k?	==
</s>
</p>
<p>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Vítem	Vít	k1gMnSc7	Vít
Sázavským	sázavský	k2eAgMnSc7d1	sázavský
složila	složit	k5eAaPmAgFnS	složit
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
animovaným	animovaný	k2eAgInPc3d1	animovaný
filmům	film	k1gInPc3	film
režisérky	režisérka	k1gFnSc2	režisérka
Michaely	Michaela	k1gFnSc2	Michaela
Pavlátové	Pavlátový	k2eAgFnSc2d1	Pavlátová
Křížovka	křížovka	k1gFnSc1	křížovka
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
a	a	k8xC	a
Řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
řeči	řeč	k1gFnSc2	řeč
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Nazpívala	nazpívat	k5eAaPmAgFnS	nazpívat
také	také	k9	také
titulní	titulní	k2eAgFnSc4d1	titulní
píseň	píseň	k1gFnSc4	píseň
k	k	k7c3	k
filmu	film	k1gInSc3	film
Čert	čert	k1gMnSc1	čert
ví	vědět	k5eAaImIp3nS	vědět
proč	proč	k6eAd1	proč
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
hlavní	hlavní	k2eAgFnSc1d1	hlavní
cena	cena	k1gFnSc1	cena
folkového	folkový	k2eAgInSc2d1	folkový
festivalu	festival	k1gInSc2	festival
Porta	porta	k1gFnSc1	porta
skupině	skupina	k1gFnSc3	skupina
Nerez	nerez	k1gFnSc4	nerez
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
vokální	vokální	k2eAgInSc4d1	vokální
projev	projev	k1gInSc4	projev
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
Vokalíze	vokalíza	k1gFnSc6	vokalíza
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
hlavní	hlavní	k2eAgFnSc1d1	hlavní
cena	cena	k1gFnSc1	cena
folkového	folkový	k2eAgInSc2d1	folkový
festivalu	festival	k1gInSc2	festival
Porta	porta	k1gFnSc1	porta
skupině	skupina	k1gFnSc3	skupina
Nerez	nerez	k1gFnSc4	nerez
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
hlavní	hlavní	k2eAgFnSc1d1	hlavní
cena	cena	k1gFnSc1	cena
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
Vokalíze	vokalíza	k1gFnSc6	vokalíza
skupině	skupina	k1gFnSc6	skupina
Nerez	nerez	k1gFnSc1	nerez
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
ocenění	ocenění	k1gNnPc4	ocenění
Českého	český	k2eAgInSc2d1	český
hudebního	hudební	k2eAgInSc2d1	hudební
fondu	fond	k1gInSc2	fond
"	"	kIx"	"
<g/>
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
libretistka	libretistka	k1gFnSc1	libretistka
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
nominace	nominace	k1gFnSc1	nominace
časopisem	časopis	k1gInSc7	časopis
Melodie	melodie	k1gFnSc2	melodie
na	na	k7c4	na
ocenění	ocenění	k1gNnSc4	ocenění
"	"	kIx"	"
<g/>
Nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
navržena	navržen	k2eAgFnSc1d1	navržena
hudební	hudební	k2eAgFnSc7d1	hudební
kritikou	kritika	k1gFnSc7	kritika
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
České	český	k2eAgFnSc2d1	Česká
Grammy	Gramma	k1gFnSc2	Gramma
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
zpěvaček	zpěvačka	k1gFnPc2	zpěvačka
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
ponorka	ponorka	k1gFnSc1	ponorka
–	–	k?	–
Zuzana	Zuzana	k1gFnSc1	Zuzana
Navarová	Navarová	k1gFnSc1	Navarová
<g/>
,	,	kIx,	,
Iván	Iván	k1gNnSc1	Iván
Gutiérrez	Gutiérreza	k1gFnPc2	Gutiérreza
a	a	k8xC	a
Koa	Koa	k1gFnPc2	Koa
<g/>
,	,	kIx,	,
album	album	k1gNnSc4	album
Skleněná	skleněný	k2eAgFnSc1d1	skleněná
vrba	vrba	k1gFnSc1	vrba
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
cena	cena	k1gFnSc1	cena
Anděl	Anděla	k1gFnPc2	Anděla
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
folk	folk	k1gInSc4	folk
udělená	udělený	k2eAgFnSc1d1	udělená
Akademií	akademie	k1gFnSc7	akademie
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
časopis	časopis	k1gInSc1	časopis
Folk	folk	k1gInSc4	folk
&	&	k?	&
Country	country	k2eAgNnSc1d1	country
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
odměnil	odměnit	k5eAaPmAgInS	odměnit
výroční	výroční	k2eAgFnSc7d1	výroční
cenou	cena	k1gFnSc7	cena
Zlatý	zlatý	k2eAgInSc1d1	zlatý
klíč	klíč	k1gInSc1	klíč
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
zlatá	zlatý	k2eAgFnSc1d1	zlatá
deska	deska	k1gFnSc1	deska
od	od	k7c2	od
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
Indies	Indiesa	k1gFnPc2	Indiesa
Records	Recordsa	k1gFnPc2	Recordsa
za	za	k7c4	za
prodej	prodej	k1gInSc4	prodej
alba	album	k1gNnSc2	album
Barvy	barva	k1gFnSc2	barva
všecky	všecek	k3xTgFnPc4	všecek
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
při	při	k7c6	při
vyhlašování	vyhlašování	k1gNnSc6	vyhlašování
Cen	cena	k1gFnPc2	cena
Akademie	akademie	k1gFnSc2	akademie
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2004	[number]	k4	2004
posmrtně	posmrtně	k6eAd1	posmrtně
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
Síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HARTMAN	Hartman	k1gMnSc1	Hartman
<g/>
,	,	kIx,	,
Ivan	Ivan	k1gMnSc1	Ivan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nejvyšších	vysoký	k2eAgNnPc6d3	nejvyšší
špruslích	špruslích	k?	špruslích
<g/>
:	:	kIx,	:
výročí	výročí	k1gNnPc4	výročí
psaná	psaný	k2eAgNnPc4d1	psané
červnem	červen	k1gInSc7	červen
–	–	k?	–
Zuzana	Zuzana	k1gFnSc1	Zuzana
Navarová	Navarová	k1gFnSc1	Navarová
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Folk	folk	k1gInSc1	folk
&	&	k?	&
Country	country	k2eAgInSc2d1	country
<g/>
.	.	kIx.	.
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
č.	č.	k?	č.
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
také	také	k9	také
z	z	k7c2	z
<g/>
:	:	kIx,	:
http://navarova.wz.cz/	[url]	k?	http://navarova.wz.cz/
</s>
</p>
<p>
<s>
JEHNE	JEHNE	kA	JEHNE
<g/>
,	,	kIx,	,
Leo	Leo	k1gMnSc1	Leo
<g/>
.	.	kIx.	.
</s>
<s>
Vokalíza	vokalíza	k1gFnSc1	vokalíza
značky	značka	k1gFnSc2	značka
Nerez	nerez	k1gFnSc1	nerez
<g/>
.	.	kIx.	.
</s>
<s>
Melodie	melodie	k1gFnSc1	melodie
<g/>
.	.	kIx.	.
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
č.	č.	k?	č.
12	[number]	k4	12
<g/>
,	,	kIx,	,
s.	s.	k?	s.
372	[number]	k4	372
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NAVAROVÁ	NAVAROVÁ	kA	NAVAROVÁ
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
a	a	k8xC	a
SVANOVSKÁ	SVANOVSKÁ	kA	SVANOVSKÁ
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Andělská	andělský	k2eAgFnSc1d1	Andělská
počta	počta	k?	počta
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
písňové	písňový	k2eAgInPc1d1	písňový
texty	text	k1gInPc1	text
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
písňové	písňový	k2eAgInPc1d1	písňový
texty	text	k1gInPc1	text
<g/>
,	,	kIx,	,
vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
<g/>
,	,	kIx,	,
fotografie	fotografia	k1gFnPc1	fotografia
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Host	host	k1gMnSc1	host
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
167	[number]	k4	167
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7294	[number]	k4	7294
<g/>
-	-	kIx~	-
<g/>
267	[number]	k4	267
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NAVAROVÁ	NAVAROVÁ	kA	NAVAROVÁ
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
a	a	k8xC	a
SVANOVSKÁ	SVANOVSKÁ	kA	SVANOVSKÁ
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Andělská	andělský	k2eAgFnSc1d1	Andělská
počta	počta	k?	počta
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
písňové	písňový	k2eAgInPc1d1	písňový
texty	text	k1gInPc1	text
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
písňové	písňový	k2eAgInPc4d1	písňový
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
<g/>
,	,	kIx,	,
fotografie	fotografia	k1gFnPc4	fotografia
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Host	host	k1gMnSc1	host
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
167	[number]	k4	167
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7577	[number]	k4	7577
<g/>
-	-	kIx~	-
<g/>
333	[number]	k4	333
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SVANOVSKÁ	SVANOVSKÁ	kA	SVANOVSKÁ
<g/>
,	,	kIx,	,
Hana	Hana	k1gFnSc1	Hana
a	a	k8xC	a
Helena	Helena	k1gFnSc1	Helena
BRETFELDOVÁ	BRETFELDOVÁ	kA	BRETFELDOVÁ
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
psala	psát	k5eAaImAgFnS	psát
Zuzana	Zuzana	k1gFnSc1	Zuzana
Navarová	Navarová	k1gFnSc1	Navarová
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
Dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
,	,	kIx,	,
č.	č.	k?	č.
202	[number]	k4	202
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
s.	s.	k?	s.
D	D	kA	D
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
1168	[number]	k4	1168
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VŘEŠŤÁL	VŘEŠŤÁL	kA	VŘEŠŤÁL
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
<g/>
,	,	kIx,	,
Nerez	nerez	k1gFnSc1	nerez
nerezne	reznout	k5eNaPmIp3nS	reznout
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Galén	Galén	k1gInSc1	Galén
<g/>
,	,	kIx,	,
©	©	k?	©
<g/>
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
209	[number]	k4	209
s.	s.	k?	s.
Edice	edice	k1gFnSc2	edice
Olivovníky	olivovník	k1gInPc1	olivovník
<g/>
,	,	kIx,	,
sv.	sv.	kA	sv.
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7262	[number]	k4	7262
<g/>
-	-	kIx~	-
<g/>
611	[number]	k4	611
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Film	film	k1gInSc1	film
===	===	k?	===
</s>
</p>
<p>
<s>
Kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
kdo	kdo	k3yQnSc1	kdo
–	–	k?	–
Zuzana	Zuzana	k1gFnSc1	Zuzana
Navarová	Navarová	k1gFnSc1	Navarová
[	[	kIx(	[
<g/>
TV	TV	kA	TV
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
30	[number]	k4	30
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
Režie	režie	k1gFnSc1	režie
Judita	Judita	k1gFnSc1	Judita
Křížová	Křížová	k1gFnSc1	Křížová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
koštěti	koště	k1gNnSc6	koště
do	do	k7c2	do
nebe	nebe	k1gNnSc2	nebe
[	[	kIx(	[
<g/>
TV	TV	kA	TV
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
40	[number]	k4	40
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
Režie	režie	k1gFnSc1	režie
Radovan	Radovan	k1gMnSc1	Radovan
Urban	Urban	k1gMnSc1	Urban
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zuzana	Zuzana	k1gFnSc1	Zuzana
Navarová	Navarová	k1gFnSc1	Navarová
<g/>
,	,	kIx,	,
Iván	Iván	k1gMnSc1	Iván
Gutiérrez	Gutiérrez	k1gMnSc1	Gutiérrez
&	&	k?	&
Koa	Koa	k1gMnSc1	Koa
(	(	kIx(	(
<g/>
koncert	koncert	k1gInSc1	koncert
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
film	film	k1gInSc1	film
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
50	[number]	k4	50
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
Režie	režie	k1gFnSc1	režie
Rudolf	Rudolf	k1gMnSc1	Rudolf
Chudoba	Chudoba	k1gMnSc1	Chudoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Předčasná	předčasný	k2eAgNnPc1d1	předčasné
úmrtí	úmrtí	k1gNnPc1	úmrtí
<g/>
:	:	kIx,	:
Barvy	barva	k1gFnSc2	barva
všecky	všecek	k3xTgFnPc1	všecek
[	[	kIx(	[
<g/>
TV	TV	kA	TV
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
52	[number]	k4	52
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
Režie	režie	k1gFnSc1	režie
Tomáš	Tomáš	k1gMnSc1	Tomáš
Váňa	Váňa	k1gMnSc1	Váňa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
TV	TV	kA	TV
pořady	pořad	k1gInPc1	pořad
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Kloboučku	klobouček	k1gInSc6	klobouček
[	[	kIx(	[
<g/>
TV	TV	kA	TV
pořad	pořad	k1gInSc1	pořad
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
39	[number]	k4	39
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Rockové	rockový	k2eAgNnSc4d1	rockové
posezení	posezení	k1gNnSc4	posezení
s	s	k7c7	s
Michalem	Michal	k1gMnSc7	Michal
Pavlíčkem	Pavlíček	k1gMnSc7	Pavlíček
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc4	jeho
hosty	host	k1gMnPc4	host
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Crescendo	crescendo	k1gNnSc1	crescendo
[	[	kIx(	[
<g/>
TV	TV	kA	TV
pořad	pořad	k1gInSc1	pořad
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
56	[number]	k4	56
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Nerez	nerez	k1gFnSc1	nerez
</s>
</p>
<p>
<s>
Radůza	Radůza	k1gFnSc1	Radůza
</s>
</p>
<p>
<s>
Koa	Koa	k?	Koa
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zuzana	Zuzana	k1gFnSc1	Zuzana
Navarová	Navarový	k2eAgFnSc1d1	Navarová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Zuzana	Zuzana	k1gFnSc1	Zuzana
Navarová	Navarový	k2eAgFnSc1d1	Navarová
</s>
</p>
<p>
<s>
Neoficiální	neoficiální	k2eAgFnPc1d1	neoficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Zuzana	Zuzana	k1gFnSc1	Zuzana
Navarová	Navarový	k2eAgFnSc1d1	Navarová
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Crescendo	crescendo	k6eAd1	crescendo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
-	-	kIx~	-
archiv	archiv	k1gInSc1	archiv
ČT	ČT	kA	ČT
</s>
</p>
