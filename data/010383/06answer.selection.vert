<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
Brno	Brno	k1gNnSc1	Brno
železničním	železniční	k2eAgInSc7d1	železniční
uzlem	uzel	k1gInSc7	uzel
nadregionálního	nadregionální	k2eAgInSc2d1	nadregionální
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
osobní	osobní	k2eAgFnSc4d1	osobní
dopravu	doprava	k1gFnSc4	doprava
zde	zde	k6eAd1	zde
slouží	sloužit	k5eAaImIp3nP	sloužit
devět	devět	k4xCc4	devět
železničních	železniční	k2eAgFnPc2d1	železniční
stanic	stanice	k1gFnPc2	stanice
a	a	k8xC	a
zastávek	zastávka	k1gFnPc2	zastávka
<g/>
.	.	kIx.	.
</s>
