<s>
Vysoké	vysoký	k2eAgNnSc1d1
učení	učení	k1gNnSc1
technické	technický	k2eAgNnSc1d1
v	v	k7c6
Brně	Brno	k1gNnSc6
(	(	kIx(
<g/>
ofic	ofic	k6eAd1
<g/>
.	.	kIx.
VUT	VUT	kA
<g/>
,	,	kIx,
příp	příp	kA
<g/>
.	.	kIx.
zkrácený	zkrácený	k2eAgInSc1d1
název	název	k1gInSc1
VUT	VUT	kA
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
veřejná	veřejný	k2eAgFnSc1d1
vysoká	vysoký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
zaměřená	zaměřený	k2eAgFnSc1d1
na	na	k7c6
technické	technický	k2eAgFnSc6d1
<g/>
,	,	kIx,
ekonomické	ekonomický	k2eAgFnSc3d1
a	a	k8xC
umělecké	umělecký	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
<g/>
.	.	kIx.
</s>