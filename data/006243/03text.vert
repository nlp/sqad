<s>
Vysoké	vysoký	k2eAgNnSc1d1	vysoké
učení	učení	k1gNnSc1	učení
technické	technický	k2eAgNnSc1d1	technické
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
ofic	ofic	k6eAd1	ofic
<g/>
.	.	kIx.	.
VUT	VUT	kA	VUT
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
zkrácený	zkrácený	k2eAgInSc1d1	zkrácený
název	název	k1gInSc1	název
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
veřejná	veřejný	k2eAgFnSc1d1	veřejná
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c6	na
technické	technický	k2eAgFnSc6d1	technická
<g/>
,	,	kIx,	,
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
a	a	k8xC	a
umělecké	umělecký	k2eAgFnSc2d1	umělecká
vědy	věda	k1gFnSc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgInSc1d3	nejstarší
vysokou	vysoký	k2eAgFnSc7d1	vysoká
školou	škola	k1gFnSc7	škola
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
nabízí	nabízet	k5eAaImIp3nS	nabízet
studium	studium	k1gNnSc1	studium
na	na	k7c6	na
osmi	osm	k4xCc6	osm
fakultách	fakulta	k1gFnPc6	fakulta
a	a	k8xC	a
dvou	dva	k4xCgInPc6	dva
vysokoškolských	vysokoškolský	k2eAgInPc6d1	vysokoškolský
ústavech	ústav	k1gInPc6	ústav
v	v	k7c6	v
bakalářských	bakalářský	k2eAgInPc6d1	bakalářský
<g/>
,	,	kIx,	,
magisterských	magisterský	k2eAgInPc6d1	magisterský
i	i	k8xC	i
doktorských	doktorský	k2eAgInPc6d1	doktorský
studijních	studijní	k2eAgInPc6d1	studijní
oborech	obor	k1gInPc6	obor
téměř	téměř	k6eAd1	téměř
24	[number]	k4	24
tisícům	tisíc	k4xCgInPc3	tisíc
posluchačů	posluchač	k1gMnPc2	posluchač
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
se	se	k3xPyFc4	se
umísťuje	umísťovat	k5eAaImIp3nS	umísťovat
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
světových	světový	k2eAgFnPc2d1	světová
univerzit	univerzita	k1gFnPc2	univerzita
QS	QS	kA	QS
TopUniversities	TopUniversities	k1gInSc1	TopUniversities
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1849	[number]	k4	1849
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
díky	díky	k7c3	díky
přesunu	přesun	k1gInSc3	přesun
Stavovské	stavovský	k2eAgFnSc2d1	stavovská
akademie	akademie	k1gFnSc2	akademie
z	z	k7c2	z
Olomouce	Olomouc	k1gFnSc2	Olomouc
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
(	(	kIx(	(
<g/>
1847	[number]	k4	1847
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
moravského	moravský	k2eAgInSc2d1	moravský
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
dvoujazyčné	dvoujazyčný	k2eAgNnSc1d1	dvoujazyčné
česko-německé	českoěmecký	k2eAgNnSc1d1	česko-německé
Technické	technický	k2eAgNnSc1d1	technické
učiliště	učiliště	k1gNnSc1	učiliště
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
zárodek	zárodek	k1gInSc4	zárodek
vyššího	vysoký	k2eAgNnSc2d2	vyšší
technického	technický	k2eAgNnSc2d1	technické
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
nevysokoškolské	vysokoškolský	k2eNgNnSc4d1	vysokoškolský
učiliště	učiliště	k1gNnSc4	učiliště
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k9	už
roku	rok	k1gInSc2	rok
1873	[number]	k4	1873
bylo	být	k5eAaImAgNnS	být
povýšeno	povýšit	k5eAaPmNgNnS	povýšit
na	na	k7c4	na
vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
s	s	k7c7	s
názvem	název	k1gInSc7	název
C.	C.	kA	C.
k.	k.	k?	k.
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
technická	technický	k2eAgFnSc1d1	technická
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
však	však	k9	však
již	již	k6eAd1	již
nebyla	být	k5eNaImAgFnS	být
dvojjazyčná	dvojjazyčný	k2eAgFnSc1d1	dvojjazyčná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
německého	německý	k2eAgInSc2d1	německý
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
po	po	k7c6	po
účasti	účast	k1gFnSc6	účast
její	její	k3xOp3gFnSc2	její
akademické	akademický	k2eAgFnSc2d1	akademická
obce	obec	k1gFnSc2	obec
na	na	k7c6	na
revoluci	revoluce	k1gFnSc6	revoluce
a	a	k8xC	a
národním	národní	k2eAgNnSc6d1	národní
obrození	obrození	k1gNnSc6	obrození
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
chyběla	chybět	k5eAaImAgFnS	chybět
instituce	instituce	k1gFnSc1	instituce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
řádné	řádný	k2eAgNnSc4d1	řádné
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
německá	německý	k2eAgFnSc1d1	německá
<g/>
,	,	kIx,	,
nemohla	moct	k5eNaImAgFnS	moct
tuto	tento	k3xDgFnSc4	tento
chybějící	chybějící	k2eAgFnSc4d1	chybějící
potřebu	potřeba	k1gFnSc4	potřeba
pokrýt	pokrýt	k5eAaPmF	pokrýt
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
studenti	student	k1gMnPc1	student
převážně	převážně	k6eAd1	převážně
odcházeli	odcházet	k5eAaImAgMnP	odcházet
jinam	jinam	k6eAd1	jinam
–	–	k?	–
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
Vídně	Vídeň	k1gFnSc2	Vídeň
nebo	nebo	k8xC	nebo
Krakova	Krakov	k1gInSc2	Krakov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
sílily	sílit	k5eAaImAgFnP	sílit
hlasy	hlas	k1gInPc4	hlas
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zřízení	zřízení	k1gNnSc4	zřízení
univerzity	univerzita	k1gFnPc1	univerzita
požadovaly	požadovat	k5eAaImAgFnP	požadovat
<g/>
,	,	kIx,	,
ne	ne	k9	ne
však	však	k9	však
již	již	k6eAd1	již
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
válkách	válka	k1gFnPc6	válka
o	o	k7c6	o
slezské	slezský	k2eAgFnSc6d1	Slezská
dědictví	dědictví	k1gNnSc6	dědictví
přeměněna	přeměnit	k5eAaPmNgFnS	přeměnit
v	v	k7c4	v
městskou	městský	k2eAgFnSc4d1	městská
pevnost	pevnost	k1gFnSc4	pevnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
větším	veliký	k2eAgMnSc6d2	veliký
a	a	k8xC	a
hlavním	hlavní	k2eAgNnSc6d1	hlavní
zemském	zemský	k2eAgNnSc6d1	zemské
městě	město	k1gNnSc6	město
–	–	k?	–
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Moravští	moravský	k2eAgMnPc1d1	moravský
Němci	Němec	k1gMnPc1	Němec
ale	ale	k8xC	ale
druhou	druhý	k4xOgFnSc4	druhý
českou	český	k2eAgFnSc4d1	Česká
univerzitu	univerzita	k1gFnSc4	univerzita
zcela	zcela	k6eAd1	zcela
odmítali	odmítat	k5eAaImAgMnP	odmítat
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
o	o	k7c4	o
její	její	k3xOp3gNnSc4	její
zřízení	zřízení	k1gNnSc4	zřízení
vedly	vést	k5eAaImAgInP	vést
dlouholeté	dlouholetý	k2eAgInPc1d1	dlouholetý
neúspěšné	úspěšný	k2eNgInPc1d1	neúspěšný
spory	spor	k1gInPc1	spor
(	(	kIx(	(
<g/>
k	k	k7c3	k
vyřešení	vyřešení	k1gNnSc3	vyřešení
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
založením	založení	k1gNnSc7	založení
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vídeňská	vídeňský	k2eAgFnSc1d1	Vídeňská
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
vyřešit	vyřešit	k5eAaPmF	vyřešit
spor	spor	k1gInSc4	spor
kompromisem	kompromis	k1gInSc7	kompromis
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
univerzita	univerzita	k1gFnSc1	univerzita
sice	sice	k8xC	sice
zřízena	zřízen	k2eAgFnSc1d1	zřízena
nebude	být	k5eNaImBp3nS	být
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
ale	ale	k8xC	ale
založena	založen	k2eAgFnSc1d1	založena
česká	český	k2eAgFnSc1d1	Česká
technika	technika	k1gFnSc1	technika
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1899	[number]	k4	1899
byla	být	k5eAaImAgFnS	být
proto	proto	k6eAd1	proto
císařským	císařský	k2eAgInSc7d1	císařský
výnosem	výnos	k1gInSc7	výnos
založena	založit	k5eAaPmNgFnS	založit
C.	C.	kA	C.
k.	k.	k?	k.
česká	český	k2eAgFnSc1d1	Česká
technická	technický	k2eAgFnSc1d1	technická
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
začátek	začátek	k1gInSc4	začátek
si	se	k3xPyFc3	se
musela	muset	k5eAaImAgFnS	muset
vystačit	vystačit	k5eAaBmF	vystačit
se	s	k7c7	s
provizorními	provizorní	k2eAgInPc7d1	provizorní
prostory	prostor	k1gInPc7	prostor
v	v	k7c6	v
Augustinské	Augustinský	k2eAgFnSc6d1	Augustinská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
se	s	k7c7	s
4	[number]	k4	4
profesory	profesor	k1gMnPc7	profesor
a	a	k8xC	a
47	[number]	k4	47
studenty	student	k1gMnPc7	student
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mohli	moct	k5eAaImAgMnP	moct
studovat	studovat	k5eAaImF	studovat
jediný	jediný	k2eAgInSc1d1	jediný
–	–	k?	–
stavební	stavební	k2eAgInSc1d1	stavební
–	–	k?	–
obor	obor	k1gInSc1	obor
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
výuka	výuka	k1gFnSc1	výuka
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
strojním	strojní	k2eAgInSc6d1	strojní
<g/>
,	,	kIx,	,
následovaly	následovat	k5eAaImAgInP	následovat
pak	pak	k6eAd1	pak
obory	obor	k1gInPc1	obor
kulturního	kulturní	k2eAgNnSc2d1	kulturní
inženýrství	inženýrství	k1gNnSc2	inženýrství
(	(	kIx(	(
<g/>
tvorba	tvorba	k1gFnSc1	tvorba
krajiny	krajina	k1gFnSc2	krajina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
elektroinženýrství	elektroinženýrství	k1gNnSc2	elektroinženýrství
<g/>
,	,	kIx,	,
chemického	chemický	k2eAgNnSc2d1	chemické
inženýrství	inženýrství	k1gNnSc2	inženýrství
a	a	k8xC	a
po	po	k7c6	po
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
také	také	k9	také
obor	obor	k1gInSc4	obor
architektura	architektura	k1gFnSc1	architektura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
se	se	k3xPyFc4	se
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
nově	nově	k6eAd1	nově
postavené	postavený	k2eAgFnSc2d1	postavená
honosné	honosný	k2eAgFnSc2d1	honosná
budovy	budova	k1gFnSc2	budova
na	na	k7c4	na
Veveří	veveří	k2eAgFnSc4d1	veveří
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
dodnes	dodnes	k6eAd1	dodnes
používá	používat	k5eAaImIp3nS	používat
její	její	k3xOp3gFnSc1	její
stavební	stavební	k2eAgFnSc1d1	stavební
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
spojila	spojit	k5eAaPmAgFnS	spojit
s	s	k7c7	s
Německou	německý	k2eAgFnSc7d1	německá
vysokou	vysoký	k2eAgFnSc7d1	vysoká
školou	škola	k1gFnSc7	škola
technickou	technický	k2eAgFnSc7d1	technická
a	a	k8xC	a
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
se	se	k3xPyFc4	se
na	na	k7c4	na
Vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
technickou	technický	k2eAgFnSc4d1	technická
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
tato	tento	k3xDgFnSc1	tento
škola	škola	k1gFnSc1	škola
používala	používat	k5eAaImAgFnS	používat
název	název	k1gInSc4	název
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
technická	technický	k2eAgFnSc1d1	technická
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
E.	E.	kA	E.
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
českojazyčná	českojazyčný	k2eAgFnSc1d1	českojazyčná
technická	technický	k2eAgFnSc1d1	technická
univerzita	univerzita	k1gFnSc1	univerzita
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
ostatní	ostatní	k2eAgFnPc1d1	ostatní
české	český	k2eAgFnPc1d1	Česká
vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
některé	některý	k3yIgInPc1	některý
provozy	provoz	k1gInPc1	provoz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
okupační	okupační	k2eAgFnSc1d1	okupační
správa	správa	k1gFnSc1	správa
uznala	uznat	k5eAaPmAgFnS	uznat
za	za	k7c4	za
důležité	důležitý	k2eAgNnSc4d1	důležité
pro	pro	k7c4	pro
hospodářství	hospodářství	k1gNnSc4	hospodářství
Říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
zůstaly	zůstat	k5eAaPmAgInP	zůstat
a	a	k8xC	a
fungovaly	fungovat	k5eAaImAgInP	fungovat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
.	.	kIx.	.
</s>
<s>
Německá	německý	k2eAgFnSc1d1	německá
technika	technika	k1gFnSc1	technika
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
fungovala	fungovat	k5eAaImAgFnS	fungovat
i	i	k9	i
během	během	k7c2	během
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1945	[number]	k4	1945
byly	být	k5eAaImAgFnP	být
německé	německý	k2eAgFnPc1d1	německá
techniky	technika	k1gFnPc1	technika
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
zrušeny	zrušit	k5eAaPmNgInP	zrušit
dekretem	dekret	k1gInSc7	dekret
presidenta	president	k1gMnSc2	president
republiky	republika	k1gFnSc2	republika
č.	č.	k?	č.
123	[number]	k4	123
<g/>
/	/	kIx~	/
<g/>
1945	[number]	k4	1945
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
činnost	činnost	k1gFnSc1	činnost
školy	škola	k1gFnSc2	škola
obnovena	obnoven	k2eAgFnSc1d1	obnovena
pod	pod	k7c7	pod
starším	starý	k2eAgInSc7d2	starší
názvem	název	k1gInSc7	název
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
technická	technický	k2eAgFnSc1d1	technická
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
E.	E.	kA	E.
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
školy	škola	k1gFnSc2	škola
byl	být	k5eAaImAgInS	být
ukončen	ukončit	k5eAaPmNgInS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
součásti	součást	k1gFnPc1	součást
byly	být	k5eAaImAgFnP	být
převedeny	převést	k5eAaPmNgFnP	převést
na	na	k7c6	na
nově	nově	k6eAd1	nově
vzniklou	vzniklý	k2eAgFnSc4d1	vzniklá
Vojenskou	vojenský	k2eAgFnSc4d1	vojenská
technickou	technický	k2eAgFnSc4d1	technická
akademii	akademie	k1gFnSc4	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Jedinými	jediný	k2eAgFnPc7d1	jediná
fakultami	fakulta	k1gFnPc7	fakulta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
poskytovaly	poskytovat	k5eAaImAgFnP	poskytovat
civilní	civilní	k2eAgFnSc4d1	civilní
výuku	výuka	k1gFnSc4	výuka
<g/>
,	,	kIx,	,
zůstala	zůstat	k5eAaPmAgFnS	zůstat
Fakulta	fakulta	k1gFnSc1	fakulta
stavební	stavební	k2eAgFnSc1d1	stavební
a	a	k8xC	a
Fakulta	fakulta	k1gFnSc1	fakulta
architektury	architektura	k1gFnSc2	architektura
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
stavitelství	stavitelství	k1gNnSc2	stavitelství
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
se	se	k3xPyFc4	se
činnost	činnost	k1gFnSc1	činnost
univerzity	univerzita	k1gFnSc2	univerzita
začala	začít	k5eAaPmAgFnS	začít
postupně	postupně	k6eAd1	postupně
obnovovat	obnovovat	k5eAaImF	obnovovat
pod	pod	k7c7	pod
dnešním	dnešní	k2eAgInSc7d1	dnešní
názvem	název	k1gInSc7	název
Vysoké	vysoký	k2eAgNnSc4d1	vysoké
učení	učení	k1gNnSc4	učení
technické	technický	k2eAgNnSc4d1	technické
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
podoba	podoba	k1gFnSc1	podoba
se	se	k3xPyFc4	se
víceméně	víceméně	k9	víceméně
ustálila	ustálit	k5eAaPmAgFnS	ustálit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
reorganizaci	reorganizace	k1gFnSc3	reorganizace
některých	některý	k3yIgFnPc2	některý
fakult	fakulta	k1gFnPc2	fakulta
a	a	k8xC	a
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
řady	řada	k1gFnSc2	řada
nových	nový	k2eAgMnPc2d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Znovuobnovena	Znovuobnoven	k2eAgFnSc1d1	Znovuobnoven
byla	být	k5eAaImAgFnS	být
Fakulta	fakulta	k1gFnSc1	fakulta
chemická	chemický	k2eAgFnSc1d1	chemická
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
technických	technický	k2eAgInPc2d1	technický
oborů	obor	k1gInPc2	obor
se	se	k3xPyFc4	se
VUT	VUT	kA	VUT
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
i	i	k9	i
na	na	k7c4	na
ekonomiku	ekonomika	k1gFnSc4	ekonomika
(	(	kIx(	(
<g/>
Fakulta	fakulta	k1gFnSc1	fakulta
podnikatelská	podnikatelský	k2eAgFnSc1d1	podnikatelská
založena	založen	k2eAgFnSc1d1	založena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
umění	umění	k1gNnSc4	umění
(	(	kIx(	(
<g/>
Fakulta	fakulta	k1gFnSc1	fakulta
výtvarných	výtvarný	k2eAgFnPc2d1	výtvarná
umění	umění	k1gNnSc6	umění
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
milník	milník	k1gInSc1	milník
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
rokem	rok	k1gInSc7	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
od	od	k7c2	od
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
dvě	dva	k4xCgFnPc1	dva
fakulty	fakulta	k1gFnPc1	fakulta
dislokované	dislokovaný	k2eAgFnPc1d1	dislokovaná
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
–	–	k?	–
Fakulta	fakulta	k1gFnSc1	fakulta
technologická	technologický	k2eAgFnSc1d1	technologická
a	a	k8xC	a
Fakulta	fakulta	k1gFnSc1	fakulta
managementu	management	k1gInSc2	management
a	a	k8xC	a
ekonomiky	ekonomika	k1gFnSc2	ekonomika
–	–	k?	–
a	a	k8xC	a
zakládají	zakládat	k5eAaImIp3nP	zakládat
tak	tak	k9	tak
Univerzitu	univerzita	k1gFnSc4	univerzita
Tomáše	Tomáš	k1gMnSc2	Tomáš
Bati	Baťa	k1gMnSc2	Baťa
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgFnSc7d1	poslední
výraznou	výrazný	k2eAgFnSc7d1	výrazná
organizační	organizační	k2eAgFnSc7d1	organizační
změnou	změna	k1gFnSc7	změna
je	být	k5eAaImIp3nS	být
rozštěpení	rozštěpení	k1gNnSc1	rozštěpení
Fakulty	fakulta	k1gFnSc2	fakulta
elektrotechniky	elektrotechnika	k1gFnSc2	elektrotechnika
a	a	k8xC	a
informatiky	informatika	k1gFnSc2	informatika
na	na	k7c4	na
Fakultu	fakulta	k1gFnSc4	fakulta
elektrotechniky	elektrotechnika	k1gFnSc2	elektrotechnika
a	a	k8xC	a
komunikačních	komunikační	k2eAgFnPc2d1	komunikační
technologií	technologie	k1gFnPc2	technologie
a	a	k8xC	a
na	na	k7c4	na
Fakultu	fakulta	k1gFnSc4	fakulta
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
budov	budova	k1gFnPc2	budova
VUT	VUT	kA	VUT
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
Pod	pod	k7c7	pod
Palackého	Palackého	k2eAgInSc7d1	Palackého
vrchem	vrch	k1gInSc7	vrch
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Královo	Králův	k2eAgNnSc4d1	Královo
Pole	pole	k1gNnSc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
budovy	budova	k1gFnSc2	budova
Fakulty	fakulta	k1gFnSc2	fakulta
strojního	strojní	k2eAgNnSc2d1	strojní
inženýrství	inženýrství	k1gNnSc2	inženýrství
<g/>
,	,	kIx,	,
Fakulty	fakulta	k1gFnSc2	fakulta
podnikatelské	podnikatelský	k2eAgFnSc2d1	podnikatelská
<g/>
,	,	kIx,	,
Fakulty	fakulta	k1gFnPc4	fakulta
chemické	chemický	k2eAgFnPc4d1	chemická
i	i	k8xC	i
zcela	zcela	k6eAd1	zcela
nového	nový	k2eAgInSc2d1	nový
objektu	objekt	k1gInSc2	objekt
Fakulty	fakulta	k1gFnSc2	fakulta
elektrotechniky	elektrotechnika	k1gFnSc2	elektrotechnika
a	a	k8xC	a
komunikačních	komunikační	k2eAgFnPc2d1	komunikační
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
dva	dva	k4xCgInPc1	dva
kolejní	kolejní	k2eAgInPc1d1	kolejní
areály	areál	k1gInPc1	areál
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
bývalého	bývalý	k2eAgInSc2d1	bývalý
kartuziánského	kartuziánský	k2eAgInSc2d1	kartuziánský
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
Božetěchově	Božetěchův	k2eAgFnSc6d1	Božetěchova
ulici	ulice	k1gFnSc6	ulice
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
komplexu	komplex	k1gInSc6	komplex
přes	přes	k7c4	přes
ulici	ulice	k1gFnSc4	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Fakultu	fakulta	k1gFnSc4	fakulta
stavební	stavební	k2eAgFnPc4d1	stavební
najdete	najít	k5eAaPmIp2nP	najít
v	v	k7c6	v
rozsáhlém	rozsáhlý	k2eAgInSc6d1	rozsáhlý
zrekonstruovaném	zrekonstruovaný	k2eAgInSc6d1	zrekonstruovaný
areálu	areál	k1gInSc6	areál
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Veveří	veveří	k2eAgNnSc4d1	veveří
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
architektury	architektura	k1gFnSc2	architektura
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Poříčí	Poříčí	k1gNnSc2	Poříčí
<g/>
,	,	kIx,	,
Fakulta	fakulta	k1gFnSc1	fakulta
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
pak	pak	k6eAd1	pak
v	v	k7c6	v
Rybářské	rybářský	k2eAgFnSc6d1	rybářská
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
VUT	VUT	kA	VUT
dále	daleko	k6eAd2	daleko
používá	používat	k5eAaImIp3nS	používat
areál	areál	k1gInSc1	areál
v	v	k7c6	v
Údolní	údolní	k2eAgFnSc6d1	údolní
ulici	ulice	k1gFnSc6	ulice
a	a	k8xC	a
třetí	třetí	k4xOgInSc1	třetí
kolejní	kolejní	k2eAgInSc1d1	kolejní
areál	areál	k1gInSc1	areál
v	v	k7c6	v
Kounicově	Kounicově	k1gFnSc6	Kounicově
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Rektorát	rektorát	k1gInSc1	rektorát
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
nově	nově	k6eAd1	nově
zrekonstruované	zrekonstruovaný	k2eAgFnSc6d1	zrekonstruovaná
novobarokní	novobarokní	k2eAgFnSc6d1	novobarokní
budově	budova	k1gFnSc6	budova
v	v	k7c6	v
Antonínské	Antonínský	k2eAgFnSc6d1	Antonínská
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
univerzity	univerzita	k1gFnSc2	univerzita
stojí	stát	k5eAaImIp3nS	stát
rektor	rektor	k1gMnSc1	rektor
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
oblastech	oblast	k1gFnPc6	oblast
jeho	jeho	k3xOp3gFnSc2	jeho
činnosti	činnost	k1gFnSc2	činnost
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
pět	pět	k4xCc4	pět
prorektorů	prorektor	k1gMnPc2	prorektor
<g/>
.	.	kIx.	.
</s>
<s>
Finanční	finanční	k2eAgFnSc3d1	finanční
záležitosti	záležitost	k1gFnSc3	záležitost
VUT	VUT	kA	VUT
řídí	řídit	k5eAaImIp3nS	řídit
kvestor	kvestor	k1gMnSc1	kvestor
<g/>
,	,	kIx,	,
o	o	k7c4	o
komunikaci	komunikace	k1gFnSc4	komunikace
a	a	k8xC	a
propagaci	propagace	k1gFnSc4	propagace
univerzity	univerzita	k1gFnSc2	univerzita
se	se	k3xPyFc4	se
stará	starý	k2eAgFnSc1d1	stará
tisková	tiskový	k2eAgFnSc1d1	tisková
mluvčí	mluvčí	k1gFnSc1	mluvčí
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Odborem	odbor	k1gInSc7	odbor
marketingu	marketing	k1gInSc2	marketing
a	a	k8xC	a
vnějších	vnější	k2eAgInPc2d1	vnější
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgInPc1d1	důležitý
dokumenty	dokument	k1gInPc1	dokument
a	a	k8xC	a
směrnice	směrnice	k1gFnSc1	směrnice
projednává	projednávat	k5eAaImIp3nS	projednávat
a	a	k8xC	a
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
akademický	akademický	k2eAgInSc1d1	akademický
senát	senát	k1gInSc1	senát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
zaměstnaneckou	zaměstnanecký	k2eAgFnSc4d1	zaměstnanecká
a	a	k8xC	a
studentskou	studentský	k2eAgFnSc4d1	studentská
komoru	komora	k1gFnSc4	komora
<g/>
.	.	kIx.	.
</s>
<s>
Vědecké	vědecký	k2eAgNnSc1d1	vědecké
směřování	směřování	k1gNnSc1	směřování
VUT	VUT	kA	VUT
určuje	určovat	k5eAaImIp3nS	určovat
vědecká	vědecký	k2eAgFnSc1d1	vědecká
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
působí	působit	k5eAaImIp3nP	působit
odborníci	odborník	k1gMnPc1	odborník
z	z	k7c2	z
VUT	VUT	kA	VUT
<g/>
,	,	kIx,	,
jiných	jiný	k2eAgFnPc2d1	jiná
univerzit	univerzita	k1gFnPc2	univerzita
i	i	k9	i
z	z	k7c2	z
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnSc2d1	jednotlivá
fakulty	fakulta	k1gFnSc2	fakulta
řídí	řídit	k5eAaImIp3nP	řídit
děkani	děkan	k1gMnPc1	děkan
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
oblastech	oblast	k1gFnPc6	oblast
činnosti	činnost	k1gFnSc2	činnost
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
několik	několik	k4yIc4	několik
proděkanů	proděkan	k1gMnPc2	proděkan
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
fakulty	fakulta	k1gFnSc2	fakulta
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgInPc4	svůj
akademické	akademický	k2eAgInPc4d1	akademický
senáty	senát	k1gInPc4	senát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
projednávají	projednávat	k5eAaImIp3nP	projednávat
směrnice	směrnice	k1gFnPc4	směrnice
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
výhradně	výhradně	k6eAd1	výhradně
fakult	fakulta	k1gFnPc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
mají	mít	k5eAaImIp3nP	mít
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
fakulty	fakulta	k1gFnPc1	fakulta
vlastní	vlastní	k2eAgFnSc2d1	vlastní
vědecké	vědecký	k2eAgFnSc2d1	vědecká
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
VUT	VUT	kA	VUT
působí	působit	k5eAaImIp3nS	působit
řada	řada	k1gFnSc1	řada
studentských	studentský	k2eAgFnPc2d1	studentská
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
z	z	k7c2	z
historických	historický	k2eAgInPc2d1	historický
důvodů	důvod	k1gInPc2	důvod
nazývaných	nazývaný	k2eAgInPc2d1	nazývaný
studentské	studentský	k2eAgFnSc2d1	studentská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
fakulta	fakulta	k1gFnSc1	fakulta
má	mít	k5eAaImIp3nS	mít
svoji	svůj	k3xOyFgFnSc4	svůj
studentskou	studentský	k2eAgFnSc4d1	studentská
komoru	komora	k1gFnSc4	komora
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
studenty	student	k1gMnPc4	student
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Akademického	akademický	k2eAgInSc2d1	akademický
senátu	senát	k1gInSc2	senát
–	–	k?	–
studenti	student	k1gMnPc1	student
tak	tak	k6eAd1	tak
mají	mít	k5eAaImIp3nP	mít
možnost	možnost	k1gFnSc4	možnost
podílet	podílet	k5eAaImF	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
řízení	řízení	k1gNnSc4	řízení
své	svůj	k3xOyFgFnSc2	svůj
fakulty	fakulta	k1gFnSc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgNnSc1d1	vysoké
učení	učení	k1gNnSc1	učení
technické	technický	k2eAgNnSc1d1	technické
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
následujících	následující	k2eAgFnPc2d1	následující
osmi	osm	k4xCc2	osm
fakult	fakulta	k1gFnPc2	fakulta
(	(	kIx(	(
<g/>
řazeno	řadit	k5eAaImNgNnS	řadit
abecedně	abecedně	k6eAd1	abecedně
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fakulta	fakulta	k1gFnSc1	fakulta
architektury	architektura	k1gFnSc2	architektura
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
fakult	fakulta	k1gFnPc2	fakulta
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
byla	být	k5eAaImAgFnS	být
sloučena	sloučit	k5eAaPmNgFnS	sloučit
s	s	k7c7	s
fakultou	fakulta	k1gFnSc7	fakulta
stavební	stavební	k2eAgFnSc7d1	stavební
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
výuku	výuka	k1gFnSc4	výuka
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
Architektura	architektura	k1gFnSc1	architektura
a	a	k8xC	a
urbanismus	urbanismus	k1gInSc1	urbanismus
skoro	skoro	k6eAd1	skoro
osmi	osm	k4xCc3	osm
stovkám	stovka	k1gFnPc3	stovka
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fakulta	fakulta	k1gFnSc1	fakulta
chemická	chemický	k2eAgNnPc1d1	chemické
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
chemická	chemický	k2eAgFnSc1d1	chemická
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
navazuje	navazovat	k5eAaImIp3nS	navazovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
svou	svůj	k3xOyFgFnSc7	svůj
činností	činnost	k1gFnSc7	činnost
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
tradici	tradice	k1gFnSc4	tradice
chemického	chemický	k2eAgNnSc2d1	chemické
vysokého	vysoký	k2eAgNnSc2d1	vysoké
školství	školství	k1gNnSc2	školství
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
zahájenou	zahájený	k2eAgFnSc4d1	zahájená
zřízením	zřízení	k1gNnSc7	zřízení
chemického	chemický	k2eAgInSc2d1	chemický
odboru	odbor	k1gInSc2	odbor
České	český	k2eAgFnSc2d1	Česká
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
technické	technický	k2eAgFnSc2d1	technická
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1911	[number]	k4	1911
a	a	k8xC	a
přerušenou	přerušený	k2eAgFnSc4d1	přerušená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
přeměnou	přeměna	k1gFnSc7	přeměna
brněnské	brněnský	k2eAgFnSc2d1	brněnská
techniky	technika	k1gFnSc2	technika
na	na	k7c4	na
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
Technickou	technický	k2eAgFnSc4d1	technická
akademii	akademie	k1gFnSc4	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
znovu	znovu	k6eAd1	znovu
zahájila	zahájit	k5eAaPmAgFnS	zahájit
svoji	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
ve	v	k7c6	v
školním	školní	k2eAgInSc6d1	školní
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
/	/	kIx~	/
<g/>
93	[number]	k4	93
s	s	k7c7	s
omezeným	omezený	k2eAgInSc7d1	omezený
počtem	počet	k1gInSc7	počet
studentů	student	k1gMnPc2	student
a	a	k8xC	a
minimálními	minimální	k2eAgFnPc7d1	minimální
počty	počet	k1gInPc4	počet
vědeckopedagogických	vědeckopedagogický	k2eAgMnPc2d1	vědeckopedagogický
pracovníků	pracovník	k1gMnPc2	pracovník
podle	podle	k7c2	podle
dokumentu	dokument	k1gInSc2	dokument
<g/>
,	,	kIx,	,
předloženého	předložený	k2eAgNnSc2d1	předložené
k	k	k7c3	k
úspěšnému	úspěšný	k2eAgNnSc3d1	úspěšné
akreditačnímu	akreditační	k2eAgNnSc3d1	akreditační
jednání	jednání	k1gNnSc3	jednání
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Počáteční	počáteční	k2eAgInSc1d1	počáteční
rozvoj	rozvoj	k1gInSc1	rozvoj
fakulty	fakulta	k1gFnSc2	fakulta
probíhal	probíhat	k5eAaImAgInS	probíhat
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
brněnské	brněnský	k2eAgFnSc2d1	brněnská
chemické	chemický	k2eAgFnSc2d1	chemická
komunity	komunita	k1gFnSc2	komunita
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
chemickým	chemický	k2eAgInSc7d1	chemický
odborem	odbor	k1gInSc7	odbor
PřF	PřF	k1gFnSc2	PřF
MU	MU	kA	MU
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
tak	tak	k6eAd1	tak
vrátila	vrátit	k5eAaPmAgFnS	vrátit
chemické	chemický	k2eAgFnPc4d1	chemická
fakultě	fakulta	k1gFnSc6	fakulta
její	její	k3xOp3gFnSc4	její
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
chemickým	chemický	k2eAgInPc3d1	chemický
oborům	obor	k1gInPc3	obor
po	po	k7c6	po
zřízení	zřízení	k1gNnSc6	zřízení
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
r.	r.	kA	r.
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
se	se	k3xPyFc4	se
fakulta	fakulta	k1gFnSc1	fakulta
úspěšně	úspěšně	k6eAd1	úspěšně
podrobila	podrobit	k5eAaPmAgFnS	podrobit
druhé	druhý	k4xOgFnSc6	druhý
akreditaci	akreditace	k1gFnSc6	akreditace
společně	společně	k6eAd1	společně
s	s	k7c7	s
chemicko-technologickými	chemickoechnologický	k2eAgFnPc7d1	chemicko-technologická
fakultami	fakulta	k1gFnPc7	fakulta
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fakulta	fakulta	k1gFnSc1	fakulta
elektrotechniky	elektrotechnika	k1gFnSc2	elektrotechnika
a	a	k8xC	a
komunikačních	komunikační	k2eAgFnPc2d1	komunikační
technologií	technologie	k1gFnPc2	technologie
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
elektrotechnický	elektrotechnický	k2eAgInSc1d1	elektrotechnický
obor	obor	k1gInSc1	obor
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
VUT	VUT	kA	VUT
vyučován	vyučovat	k5eAaImNgInS	vyučovat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obnovení	obnovení	k1gNnSc6	obnovení
univerzity	univerzita	k1gFnSc2	univerzita
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
zřízena	zřízen	k2eAgFnSc1d1	zřízena
samostatná	samostatný	k2eAgFnSc1d1	samostatná
fakulta	fakulta	k1gFnSc1	fakulta
energetická	energetický	k2eAgFnSc1d1	energetická
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
následně	následně	k6eAd1	následně
transformována	transformovat	k5eAaBmNgFnS	transformovat
na	na	k7c4	na
fakultu	fakulta	k1gFnSc4	fakulta
elektrotechnickou	elektrotechnický	k2eAgFnSc4d1	elektrotechnická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
fakulta	fakulta	k1gFnSc1	fakulta
podchytila	podchytit	k5eAaPmAgFnS	podchytit
masivní	masivní	k2eAgInSc4d1	masivní
nástup	nástup	k1gInSc4	nástup
výpočetní	výpočetní	k2eAgFnSc2d1	výpočetní
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
změnila	změnit	k5eAaPmAgFnS	změnit
svou	svůj	k3xOyFgFnSc4	svůj
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
se	se	k3xPyFc4	se
na	na	k7c4	na
Fakultu	fakulta	k1gFnSc4	fakulta
elektrotechniky	elektrotechnika	k1gFnSc2	elektrotechnika
a	a	k8xC	a
informatiky	informatika	k1gFnSc2	informatika
(	(	kIx(	(
<g/>
FEI	FEI	kA	FEI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
nestačilo	stačit	k5eNaBmAgNnS	stačit
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
vzniká	vznikat	k5eAaImIp3nS	vznikat
samostatná	samostatný	k2eAgFnSc1d1	samostatná
Fakulta	fakulta	k1gFnSc1	fakulta
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
a	a	k8xC	a
mateřská	mateřský	k2eAgFnSc1d1	mateřská
fakulta	fakulta	k1gFnSc1	fakulta
mění	měnit	k5eAaImIp3nS	měnit
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
na	na	k7c4	na
Fakultu	fakulta	k1gFnSc4	fakulta
elektrotechniky	elektrotechnika	k1gFnSc2	elektrotechnika
a	a	k8xC	a
komunikačních	komunikační	k2eAgFnPc2d1	komunikační
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
získala	získat	k5eAaPmAgFnS	získat
akreditaci	akreditace	k1gFnSc4	akreditace
nových	nový	k2eAgInPc2d1	nový
moderně	moderně	k6eAd1	moderně
pojatých	pojatý	k2eAgInPc2d1	pojatý
studijních	studijní	k2eAgInPc2d1	studijní
programů	program	k1gInPc2	program
ve	v	k7c6	v
strukturovaném	strukturovaný	k2eAgNnSc6d1	strukturované
studiu	studio	k1gNnSc6	studio
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
studuje	studovat	k5eAaImIp3nS	studovat
asi	asi	k9	asi
4	[number]	k4	4
000	[number]	k4	000
studentů	student	k1gMnPc2	student
v	v	k7c6	v
bakalářském	bakalářský	k2eAgMnSc6d1	bakalářský
<g/>
,	,	kIx,	,
magisterském	magisterský	k2eAgInSc6d1	magisterský
a	a	k8xC	a
doktorském	doktorský	k2eAgInSc6d1	doktorský
studijním	studijní	k2eAgInSc6d1	studijní
programu	program	k1gInSc6	program
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
je	být	k5eAaImIp3nS	být
orientováno	orientovat	k5eAaBmNgNnS	orientovat
na	na	k7c4	na
široké	široký	k2eAgNnSc4d1	široké
spektrum	spektrum	k1gNnSc4	spektrum
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
oblastí	oblast	k1gFnPc2	oblast
<g/>
:	:	kIx,	:
řídicí	řídicí	k2eAgFnSc1d1	řídicí
technika	technika	k1gFnSc1	technika
a	a	k8xC	a
robotika	robotika	k1gFnSc1	robotika
<g/>
,	,	kIx,	,
biomedicínské	biomedicínský	k2eAgNnSc1d1	biomedicínské
inženýrství	inženýrství	k1gNnSc1	inženýrství
<g/>
,	,	kIx,	,
silnoproudá	silnoproudý	k2eAgFnSc1d1	silnoproudá
elektrotechnika	elektrotechnika	k1gFnSc1	elektrotechnika
a	a	k8xC	a
elektroenergetika	elektroenergetika	k1gFnSc1	elektroenergetika
<g/>
,	,	kIx,	,
elektronika	elektronika	k1gFnSc1	elektronika
a	a	k8xC	a
elektrotechnologie	elektrotechnologie	k1gFnSc1	elektrotechnologie
<g/>
,	,	kIx,	,
mikroelektronika	mikroelektronika	k1gFnSc1	mikroelektronika
<g/>
,	,	kIx,	,
radioelektronika	radioelektronika	k1gFnSc1	radioelektronika
<g/>
,	,	kIx,	,
teleinformatika	teleinformatika	k1gFnSc1	teleinformatika
a	a	k8xC	a
audioinženýrství	audioinženýrství	k1gNnSc1	audioinženýrství
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fakulta	fakulta	k1gFnSc1	fakulta
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
elektrotechnické	elektrotechnický	k2eAgFnSc2d1	elektrotechnická
katedra	katedra	k1gFnSc1	katedra
samočinných	samočinný	k2eAgMnPc2d1	samočinný
počítačů	počítač	k1gMnPc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ústav	ústav	k1gInSc1	ústav
informatiky	informatika	k1gFnSc2	informatika
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
transformován	transformovat	k5eAaBmNgMnS	transformovat
na	na	k7c4	na
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
Fakultu	fakulta	k1gFnSc4	fakulta
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Kampus	kampus	k1gInSc1	kampus
FIT	fit	k2eAgMnPc2d1	fit
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
bývalého	bývalý	k2eAgInSc2d1	bývalý
kartuziánského	kartuziánský	k2eAgInSc2d1	kartuziánský
kláštera	klášter	k1gInSc2	klášter
a	a	k8xC	a
na	na	k7c6	na
území	území	k1gNnSc6	území
bývalého	bývalý	k2eAgInSc2d1	bývalý
velkostatku	velkostatek	k1gInSc2	velkostatek
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
fakulty	fakulta	k1gFnSc2	fakulta
jsou	být	k5eAaImIp3nP	být
čtyři	čtyři	k4xCgInPc1	čtyři
ústavy	ústav	k1gInPc1	ústav
<g/>
:	:	kIx,	:
Ústav	ústav	k1gInSc1	ústav
počítačových	počítačový	k2eAgInPc2d1	počítačový
systémů	systém	k1gInPc2	systém
Ústav	ústava	k1gFnPc2	ústava
informačních	informační	k2eAgInPc2d1	informační
systémů	systém	k1gInPc2	systém
Ústav	ústava	k1gFnPc2	ústava
inteligentních	inteligentní	k2eAgInPc2d1	inteligentní
systémů	systém	k1gInPc2	systém
Ústav	ústav	k1gInSc1	ústav
počítačové	počítačový	k2eAgFnSc2d1	počítačová
grafiky	grafika	k1gFnSc2	grafika
a	a	k8xC	a
multimédií	multimédium	k1gNnPc2	multimédium
Fakulta	fakulta	k1gFnSc1	fakulta
nabízí	nabízet	k5eAaImIp3nS	nabízet
dva	dva	k4xCgInPc4	dva
základní	základní	k2eAgInPc4d1	základní
stupně	stupeň	k1gInPc4	stupeň
studia	studio	k1gNnSc2	studio
–	–	k?	–
tříletý	tříletý	k2eAgInSc1d1	tříletý
bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
a	a	k8xC	a
navazující	navazující	k2eAgInSc1d1	navazující
dvouletý	dvouletý	k2eAgInSc1d1	dvouletý
magisterský	magisterský	k2eAgInSc1d1	magisterský
studijní	studijní	k2eAgInSc1d1	studijní
program	program	k1gInSc1	program
<g/>
.	.	kIx.	.
</s>
<s>
Třetím	třetí	k4xOgInSc7	třetí
stupněm	stupeň	k1gInSc7	stupeň
je	být	k5eAaImIp3nS	být
doktorský	doktorský	k2eAgInSc1d1	doktorský
studijní	studijní	k2eAgInSc1d1	studijní
program	program	k1gInSc1	program
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
realizuje	realizovat	k5eAaBmIp3nS	realizovat
studijní	studijní	k2eAgInPc4d1	studijní
programy	program	k1gInPc4	program
Informační	informační	k2eAgFnSc2d1	informační
technologie	technologie	k1gFnSc2	technologie
a	a	k8xC	a
Výpočetní	výpočetní	k2eAgFnSc1d1	výpočetní
technika	technika	k1gFnSc1	technika
a	a	k8xC	a
informatika	informatika	k1gFnSc1	informatika
pro	pro	k7c4	pro
necelé	celý	k2eNgNnSc4d1	necelé
2	[number]	k4	2
500	[number]	k4	500
posluchačů	posluchač	k1gMnPc2	posluchač
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fakulta	fakulta	k1gFnSc1	fakulta
podnikatelská	podnikatelský	k2eAgNnPc1d1	podnikatelské
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejmladších	mladý	k2eAgFnPc2d3	nejmladší
fakult	fakulta	k1gFnPc2	fakulta
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgInPc7	svůj
studijními	studijní	k2eAgInPc7d1	studijní
obory	obor	k1gInPc7	obor
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
ekonomiku	ekonomika	k1gFnSc4	ekonomika
a	a	k8xC	a
podnikání	podnikání	k1gNnSc4	podnikání
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
vyčlenila	vyčlenit	k5eAaPmAgFnS	vyčlenit
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
Fakulty	fakulta	k1gFnSc2	fakulta
strojní	strojní	k2eAgFnSc2d1	strojní
<g/>
,	,	kIx,	,
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2004	[number]	k4	2004
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
komplexu	komplex	k1gInSc2	komplex
budov	budova	k1gFnPc2	budova
VUT	VUT	kA	VUT
na	na	k7c6	na
Kolejní	kolejní	k2eAgFnSc6d1	kolejní
ulici	ulice	k1gFnSc6	ulice
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
bakalářských	bakalářský	k2eAgInPc2d1	bakalářský
<g/>
,	,	kIx,	,
magisterských	magisterský	k2eAgInPc2d1	magisterský
a	a	k8xC	a
doktorských	doktorský	k2eAgInPc2d1	doktorský
programů	program	k1gInPc2	program
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
oboru	obor	k1gInSc6	obor
nabízí	nabízet	k5eAaImIp3nS	nabízet
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
zahraničními	zahraniční	k2eAgFnPc7d1	zahraniční
univerzitami	univerzita	k1gFnPc7	univerzita
také	také	k9	také
postgraduální	postgraduální	k2eAgNnSc4d1	postgraduální
studium	studium	k1gNnSc4	studium
MBA	MBA	kA	MBA
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
3	[number]	k4	3
500	[number]	k4	500
posluchačů	posluchač	k1gMnPc2	posluchač
studuje	studovat	k5eAaImIp3nS	studovat
ve	v	k7c6	v
studijních	studijní	k2eAgInPc6d1	studijní
programech	program	k1gInPc6	program
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
management	management	k1gInSc1	management
<g/>
,	,	kIx,	,
účetnictví	účetnictví	k1gNnSc1	účetnictví
<g/>
,	,	kIx,	,
podnikové	podnikový	k2eAgFnPc1d1	podniková
finance	finance	k1gFnPc1	finance
<g/>
,	,	kIx,	,
daně	daň	k1gFnPc1	daň
či	či	k8xC	či
manažerská	manažerský	k2eAgFnSc1d1	manažerská
informatika	informatika	k1gFnSc1	informatika
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fakulta	fakulta	k1gFnSc1	fakulta
stavební	stavební	k2eAgFnSc2d1	stavební
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
fakultou	fakulta	k1gFnSc7	fakulta
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
fakulta	fakulta	k1gFnSc1	fakulta
stavební	stavební	k2eAgFnSc1d1	stavební
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
se	s	k7c7	s
stavebním	stavební	k2eAgInSc7d1	stavební
odborem	odbor	k1gInSc7	odbor
zahajovala	zahajovat	k5eAaImAgFnS	zahajovat
technika	technika	k1gFnSc1	technika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
fakulta	fakulta	k1gFnSc1	fakulta
byla	být	k5eAaImAgFnS	být
i	i	k9	i
jediná	jediný	k2eAgFnSc1d1	jediná
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přežila	přežít	k5eAaPmAgFnS	přežít
násilnou	násilný	k2eAgFnSc4d1	násilná
změnu	změna	k1gFnSc4	změna
VUT	VUT	kA	VUT
na	na	k7c4	na
Vojenskou	vojenský	k2eAgFnSc4d1	vojenská
technickou	technický	k2eAgFnSc4d1	technická
akademii	akademie	k1gFnSc4	akademie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
také	také	k9	také
největší	veliký	k2eAgFnSc7d3	veliký
fakultou	fakulta	k1gFnSc7	fakulta
co	co	k3yRnSc4	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
studentů	student	k1gMnPc2	student
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
než	než	k8xS	než
6	[number]	k4	6
500	[number]	k4	500
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zájemci	zájemce	k1gMnPc1	zájemce
zde	zde	k6eAd1	zde
mohou	moct	k5eAaImIp3nP	moct
studovat	studovat	k5eAaImF	studovat
obory	obor	k1gInPc4	obor
<g/>
:	:	kIx,	:
Architektura	architektura	k1gFnSc1	architektura
pozemních	pozemní	k2eAgFnPc2d1	pozemní
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
Stavební	stavební	k2eAgNnSc1d1	stavební
inženýrství	inženýrství	k1gNnSc1	inženýrství
<g/>
,	,	kIx,	,
Stavitelství	stavitelství	k1gNnSc1	stavitelství
<g/>
,	,	kIx,	,
Geodézie	geodézie	k1gFnSc1	geodézie
a	a	k8xC	a
kartografie	kartografie	k1gFnSc1	kartografie
a	a	k8xC	a
Architektura	architektura	k1gFnSc1	architektura
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
sídel	sídlo	k1gNnPc2	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fakulta	fakulta	k1gFnSc1	fakulta
strojního	strojní	k2eAgNnSc2d1	strojní
inženýrství	inženýrství	k1gNnSc2	inženýrství
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
otevření	otevření	k1gNnSc3	otevření
strojírenského	strojírenský	k2eAgInSc2d1	strojírenský
odboru	odbor	k1gInSc2	odbor
došlo	dojít	k5eAaPmAgNnS	dojít
hned	hned	k6eAd1	hned
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc7	druhý
nejstarší	starý	k2eAgFnSc7d3	nejstarší
fakultou	fakulta	k1gFnSc7	fakulta
Vysokého	vysoký	k2eAgNnSc2d1	vysoké
učení	učení	k1gNnSc2	učení
technického	technický	k2eAgNnSc2d1	technické
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
strojní	strojní	k2eAgNnSc4d1	strojní
vyučován	vyučovat	k5eAaImNgInS	vyučovat
také	také	k9	také
obor	obor	k1gInSc1	obor
energetický	energetický	k2eAgInSc1d1	energetický
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
později	pozdě	k6eAd2	pozdě
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
samostatná	samostatný	k2eAgFnSc1d1	samostatná
fakulta	fakulta	k1gFnSc1	fakulta
elektrotechniky	elektrotechnika	k1gFnSc2	elektrotechnika
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
Fakulta	fakulta	k1gFnSc1	fakulta
strojního	strojní	k2eAgNnSc2d1	strojní
inženýrství	inženýrství	k1gNnSc2	inženýrství
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
více	hodně	k6eAd2	hodně
než	než	k8xS	než
4	[number]	k4	4
500	[number]	k4	500
studenty	student	k1gMnPc7	student
druhou	druhý	k4xOgFnSc7	druhý
největší	veliký	k2eAgFnSc7d3	veliký
fakultou	fakulta	k1gFnSc7	fakulta
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ji	on	k3xPp3gFnSc4	on
celkem	celkem	k6eAd1	celkem
15	[number]	k4	15
odborných	odborný	k2eAgNnPc2d1	odborné
pracovišť	pracoviště	k1gNnPc2	pracoviště
a	a	k8xC	a
studium	studium	k1gNnSc1	studium
je	být	k5eAaImIp3nS	být
realizováno	realizovat	k5eAaBmNgNnS	realizovat
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
stupních	stupeň	k1gInPc6	stupeň
–	–	k?	–
bakalářském	bakalářský	k2eAgMnSc6d1	bakalářský
<g/>
,	,	kIx,	,
magisterském	magisterský	k2eAgInSc6d1	magisterský
a	a	k8xC	a
doktorském	doktorský	k2eAgInSc6d1	doktorský
<g/>
.	.	kIx.	.
</s>
<s>
Fakulta	fakulta	k1gFnSc1	fakulta
nabízí	nabízet	k5eAaImIp3nS	nabízet
studium	studium	k1gNnSc4	studium
takových	takový	k3xDgInPc2	takový
oborů	obor	k1gInPc2	obor
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
strojírenství	strojírenství	k1gNnSc2	strojírenství
<g/>
,	,	kIx,	,
aplikované	aplikovaný	k2eAgFnSc2d1	aplikovaná
vědy	věda	k1gFnSc2	věda
v	v	k7c6	v
inženýrství	inženýrství	k1gNnSc6	inženýrství
<g/>
,	,	kIx,	,
výrobní	výrobní	k2eAgInPc1d1	výrobní
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
fyzikální	fyzikální	k2eAgNnSc1d1	fyzikální
a	a	k8xC	a
materiálové	materiálový	k2eAgNnSc1d1	materiálové
inženýrství	inženýrství	k1gNnSc1	inženýrství
nebo	nebo	k8xC	nebo
metrologie	metrologie	k1gFnSc1	metrologie
a	a	k8xC	a
zkušebnictví	zkušebnictví	k1gNnSc1	zkušebnictví
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fakulta	fakulta	k1gFnSc1	fakulta
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejmladších	mladý	k2eAgFnPc2d3	nejmladší
fakult	fakulta	k1gFnPc2	fakulta
je	být	k5eAaImIp3nS	být
fakulta	fakulta	k1gFnSc1	fakulta
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
ústavu	ústav	k1gInSc2	ústav
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
architektury	architektura	k1gFnSc2	architektura
založen	založit	k5eAaPmNgInS	založit
o	o	k7c4	o
pouhý	pouhý	k2eAgInSc4d1	pouhý
rok	rok	k1gInSc4	rok
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
studentů	student	k1gMnPc2	student
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nejmenší	malý	k2eAgFnSc4d3	nejmenší
fakultu	fakulta	k1gFnSc4	fakulta
současného	současný	k2eAgInSc2d1	současný
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
cca	cca	kA	cca
300	[number]	k4	300
posluchačů	posluchač	k1gMnPc2	posluchač
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
Fakulta	fakulta	k1gFnSc1	fakulta
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
(	(	kIx(	(
<g/>
FaVU	FaVU	k1gMnPc1	FaVU
<g/>
)	)	kIx)	)
nejmladší	mladý	k2eAgMnPc1d3	nejmladší
<g/>
,	,	kIx,	,
výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
dlouholetou	dlouholetý	k2eAgFnSc4d1	dlouholetá
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
ústav	ústav	k1gInSc1	ústav
kreslení	kreslení	k1gNnSc2	kreslení
a	a	k8xC	a
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
chvíle	chvíle	k1gFnSc2	chvíle
se	se	k3xPyFc4	se
na	na	k7c6	na
brněnské	brněnský	k2eAgFnSc6d1	brněnská
technice	technika	k1gFnSc6	technika
začínají	začínat	k5eAaImIp3nP	začínat
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
umělecké	umělecký	k2eAgInPc4d1	umělecký
obory	obor	k1gInPc4	obor
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	on	k3xPp3gNnSc4	on
VUT	VUT	kA	VUT
technicky	technicky	k6eAd1	technicky
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
právě	právě	k9	právě
tato	tento	k3xDgFnSc1	tento
fakulta	fakulta	k1gFnSc1	fakulta
propojit	propojit	k5eAaPmF	propojit
techniku	technika	k1gFnSc4	technika
a	a	k8xC	a
umění	umění	k1gNnSc4	umění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
na	na	k7c6	na
FaVU	FaVU	k1gFnSc6	FaVU
vyučuje	vyučovat	k5eAaImIp3nS	vyučovat
sedm	sedm	k4xCc1	sedm
oborů	obor	k1gInPc2	obor
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
:	:	kIx,	:
malířství	malířství	k1gNnSc1	malířství
<g/>
,	,	kIx,	,
sochařství	sochařství	k1gNnSc1	sochařství
<g/>
,	,	kIx,	,
grafika	grafika	k1gFnSc1	grafika
<g/>
,	,	kIx,	,
grafický	grafický	k2eAgInSc1d1	grafický
design	design	k1gInSc1	design
<g/>
,	,	kIx,	,
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
design	design	k1gInSc1	design
<g/>
,	,	kIx,	,
konceptuální	konceptuální	k2eAgFnSc1d1	konceptuální
tendence	tendence	k1gFnSc1	tendence
<g/>
,	,	kIx,	,
fotografie	fotografie	k1gFnSc1	fotografie
a	a	k8xC	a
VMP	VMP	kA	VMP
(	(	kIx(	(
<g/>
video-multimédia-performance	videoultimédiaerformanka	k1gFnSc3	video-multimédia-performanka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ústav	ústava	k1gFnPc2	ústava
soudního	soudní	k2eAgNnSc2d1	soudní
inženýrství	inženýrství	k1gNnSc2	inženýrství
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Náplní	náplň	k1gFnSc7	náplň
ústavu	ústav	k1gInSc2	ústav
je	být	k5eAaImIp3nS	být
výuka	výuka	k1gFnSc1	výuka
soudních	soudní	k2eAgMnPc2d1	soudní
znalců	znalec	k1gMnPc2	znalec
v	v	k7c6	v
magisterských	magisterský	k2eAgInPc6d1	magisterský
studijních	studijní	k2eAgInPc6d1	studijní
programech	program	k1gInPc6	program
Rizikové	rizikový	k2eAgNnSc1d1	rizikové
inženýrství	inženýrství	k1gNnSc1	inženýrství
a	a	k8xC	a
Soudní	soudní	k2eAgNnSc1d1	soudní
inženýrství	inženýrství	k1gNnSc1	inženýrství
(	(	kIx(	(
<g/>
expertní	expertní	k2eAgNnSc1d1	expertní
inženýrství	inženýrství	k1gNnSc1	inženýrství
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
a	a	k8xC	a
realitách	realita	k1gFnPc6	realita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
programu	program	k1gInSc6	program
Soudní	soudní	k2eAgNnSc1d1	soudní
inženýrství	inženýrství	k1gNnSc1	inženýrství
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
možné	možný	k2eAgNnSc1d1	možné
navázat	navázat	k5eAaPmF	navázat
také	také	k9	také
doktorským	doktorský	k2eAgNnSc7d1	doktorské
studiem	studio	k1gNnSc7	studio
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přípravě	příprava	k1gFnSc6	příprava
je	být	k5eAaImIp3nS	být
transformace	transformace	k1gFnSc1	transformace
ústavu	ústav	k1gInSc2	ústav
na	na	k7c4	na
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
fakultu	fakulta	k1gFnSc4	fakulta
s	s	k7c7	s
pracovním	pracovní	k2eAgInSc7d1	pracovní
názvem	název	k1gInSc7	název
Fakulta	fakulta	k1gFnSc1	fakulta
soudního	soudní	k2eAgNnSc2d1	soudní
inženýrství	inženýrství	k1gNnSc2	inženýrství
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Centrum	centrum	k1gNnSc1	centrum
sportovních	sportovní	k2eAgFnPc2d1	sportovní
aktivit	aktivita	k1gFnPc2	aktivita
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
CESA	CESA	kA	CESA
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
sportovní	sportovní	k2eAgFnPc4d1	sportovní
aktivity	aktivita	k1gFnPc4	aktivita
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
studenty	student	k1gMnPc4	student
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
–	–	k?	–
zejména	zejména	k9	zejména
seniory	senior	k1gMnPc4	senior
a	a	k8xC	a
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Spravuje	spravovat	k5eAaImIp3nS	spravovat
celkem	celkem	k6eAd1	celkem
5	[number]	k4	5
sportovních	sportovní	k2eAgNnPc2d1	sportovní
zařízení	zařízení	k1gNnPc2	zařízení
–	–	k?	–
dva	dva	k4xCgInPc4	dva
sportovní	sportovní	k2eAgInPc4d1	sportovní
areály	areál	k1gInPc4	areál
<g/>
,	,	kIx,	,
loděnici	loděnice	k1gFnSc4	loděnice
a	a	k8xC	a
dvě	dva	k4xCgNnPc4	dva
fitcentra	fitcentrum	k1gNnPc4	fitcentrum
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
vybrat	vybrat	k5eAaPmF	vybrat
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
sportů	sport	k1gInPc2	sport
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
Centrum	centrum	k1gNnSc1	centrum
sportovních	sportovní	k2eAgFnPc2d1	sportovní
aktivit	aktivita	k1gFnPc2	aktivita
nabízí	nabízet	k5eAaImIp3nS	nabízet
<g/>
.	.	kIx.	.
</s>
<s>
Najdou	najít	k5eAaPmIp3nP	najít
zde	zde	k6eAd1	zde
jak	jak	k6eAd1	jak
tradiční	tradiční	k2eAgInPc4d1	tradiční
sporty	sport	k1gInPc4	sport
(	(	kIx(	(
<g/>
plavání	plavání	k1gNnSc1	plavání
<g/>
,	,	kIx,	,
hokej	hokej	k1gInSc1	hokej
<g/>
,	,	kIx,	,
basketbal	basketbal	k1gInSc1	basketbal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
i	i	k9	i
ty	ten	k3xDgInPc4	ten
méně	málo	k6eAd2	málo
tradiční	tradiční	k2eAgInPc4d1	tradiční
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
vyzkoušet	vyzkoušet	k5eAaPmF	vyzkoušet
(	(	kIx(	(
<g/>
žonglování	žonglování	k1gNnSc1	žonglování
<g/>
,	,	kIx,	,
potápění	potápění	k1gNnSc1	potápění
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
CEITEC	CEITEC	kA	CEITEC
<g/>
.	.	kIx.	.
</s>
<s>
CEITEC	CEITEC	kA	CEITEC
(	(	kIx(	(
<g/>
akronym	akronym	k1gInSc1	akronym
z	z	k7c2	z
Central	Central	k1gFnSc2	Central
European	Europeany	k1gInPc2	Europeany
Institute	institut	k1gInSc5	institut
of	of	k?	of
Technology	technolog	k1gMnPc7	technolog
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Středoevropský	středoevropský	k2eAgInSc1d1	středoevropský
technologický	technologický	k2eAgInSc1d1	technologický
institut	institut	k1gInSc1	institut
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
projekt	projekt	k1gInSc4	projekt
usilující	usilující	k2eAgInSc4d1	usilující
o	o	k7c6	o
zbudování	zbudování	k1gNnSc6	zbudování
evropského	evropský	k2eAgNnSc2d1	Evropské
centra	centrum	k1gNnSc2	centrum
excelentní	excelentní	k2eAgFnSc2d1	excelentní
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
živé	živý	k2eAgFnSc2d1	živá
přírody	příroda	k1gFnSc2	příroda
i	i	k8xC	i
pokročilých	pokročilý	k2eAgInPc2d1	pokročilý
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
technologií	technologie	k1gFnPc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
společně	společně	k6eAd1	společně
připravuje	připravovat	k5eAaImIp3nS	připravovat
skupina	skupina	k1gFnSc1	skupina
brněnských	brněnský	k2eAgFnPc2d1	brněnská
univerzit	univerzita	k1gFnPc2	univerzita
včetně	včetně	k7c2	včetně
VUT	VUT	kA	VUT
a	a	k8xC	a
výzkumných	výzkumný	k2eAgFnPc2d1	výzkumná
institucí	instituce	k1gFnPc2	instituce
a	a	k8xC	a
podporují	podporovat	k5eAaImIp3nP	podporovat
ho	on	k3xPp3gInSc4	on
Jihomoravský	jihomoravský	k2eAgInSc4d1	jihomoravský
kraj	kraj	k1gInSc4	kraj
a	a	k8xC	a
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
naplnění	naplnění	k1gNnSc2	naplnění
projektu	projekt	k1gInSc2	projekt
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
výstavbě	výstavba	k1gFnSc3	výstavba
řady	řada	k1gFnSc2	řada
laboratoří	laboratoř	k1gFnPc2	laboratoř
s	s	k7c7	s
prvotřídním	prvotřídní	k2eAgNnSc7d1	prvotřídní
přístrojovým	přístrojový	k2eAgNnSc7d1	přístrojové
vybavením	vybavení	k1gNnSc7	vybavení
a	a	k8xC	a
zázemím	zázemí	k1gNnSc7	zázemí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zajistí	zajistit	k5eAaPmIp3nP	zajistit
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
provádění	provádění	k1gNnSc4	provádění
základního	základní	k2eAgInSc2d1	základní
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
i	i	k9	i
aplikovaného	aplikovaný	k2eAgInSc2d1	aplikovaný
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
výpočetních	výpočetní	k2eAgFnPc2d1	výpočetní
a	a	k8xC	a
informačních	informační	k2eAgFnPc2d1	informační
služeb	služba	k1gFnPc2	služba
(	(	kIx(	(
<g/>
CVIS	CVIS	kA	CVIS
<g/>
)	)	kIx)	)
Koleje	kolej	k1gFnPc1	kolej
a	a	k8xC	a
menzy	menza	k1gFnPc1	menza
(	(	kIx(	(
<g/>
KaM	kam	k6eAd1	kam
<g/>
)	)	kIx)	)
Ústřední	ústřední	k2eAgFnSc1d1	ústřední
knihovna	knihovna	k1gFnSc1	knihovna
(	(	kIx(	(
<g/>
ÚK	ÚK	kA	ÚK
<g/>
)	)	kIx)	)
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
VUTIUM	VUTIUM	kA	VUTIUM
Institut	institut	k1gInSc1	institut
celoživotního	celoživotní	k2eAgNnSc2d1	celoživotní
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
Rektorát	rektorát	k1gInSc1	rektorát
Škola	škola	k1gFnSc1	škola
je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
certifikátů	certifikát	k1gInPc2	certifikát
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
ECTS	ECTS	kA	ECTS
(	(	kIx(	(
<g/>
European	European	k1gMnSc1	European
Credit	Credit	k1gMnSc1	Credit
Transfer	transfer	k1gInSc4	transfer
System	Syst	k1gInSc7	Syst
<g/>
)	)	kIx)	)
Label	Label	k1gInSc1	Label
a	a	k8xC	a
DS	DS	kA	DS
(	(	kIx(	(
<g/>
Diploma	Diplom	k1gMnSc2	Diplom
Supplement	Supplement	k1gMnSc1	Supplement
<g/>
)	)	kIx)	)
Label	Label	k1gMnSc1	Label
<g/>
,	,	kIx,	,
vyjadřujících	vyjadřující	k2eAgNnPc2d1	vyjadřující
ocenění	ocenění	k1gNnPc2	ocenění
kvality	kvalita	k1gFnSc2	kvalita
vysokoškolské	vysokoškolský	k2eAgFnSc2d1	vysokoškolská
instituce	instituce	k1gFnSc2	instituce
v	v	k7c6	v
intencích	intence	k1gFnPc6	intence
zásad	zásada	k1gFnPc2	zásada
Boloňské	boloňský	k2eAgFnSc2d1	Boloňská
deklarace	deklarace	k1gFnSc2	deklarace
<g/>
.	.	kIx.	.
</s>
<s>
Certifikát	certifikát	k1gInSc1	certifikát
ECTS	ECTS	kA	ECTS
Label	Label	k1gInSc1	Label
podporuje	podporovat	k5eAaImIp3nS	podporovat
studijní	studijní	k2eAgInPc4d1	studijní
pobyty	pobyt	k1gInPc4	pobyt
na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Certifikát	certifikát	k1gInSc1	certifikát
DS	DS	kA	DS
Label	Label	k1gInSc1	Label
byl	být	k5eAaImAgInS	být
udělen	udělit	k5eAaPmNgInS	udělit
za	za	k7c4	za
správné	správný	k2eAgNnSc4d1	správné
bezplatné	bezplatný	k2eAgNnSc4d1	bezplatné
udílení	udílení	k1gNnSc4	udílení
dodatku	dodatek	k1gInSc2	dodatek
k	k	k7c3	k
diplomu	diplom	k1gInSc2	diplom
všem	všecek	k3xTgMnPc3	všecek
absolventům	absolvent	k1gMnPc3	absolvent
podle	podle	k7c2	podle
zásad	zásada	k1gFnPc2	zásada
Evropské	evropský	k2eAgFnSc2d1	Evropská
komise	komise	k1gFnSc2	komise
<g/>
.	.	kIx.	.
</s>
<s>
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
nabízí	nabízet	k5eAaImIp3nS	nabízet
svým	svůj	k3xOyFgMnPc3	svůj
studentům	student	k1gMnPc3	student
<g/>
:	:	kIx,	:
74	[number]	k4	74
studijních	studijní	k2eAgInPc2d1	studijní
programů	program	k1gInPc2	program
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
19	[number]	k4	19
akreditováno	akreditován	k2eAgNnSc4d1	akreditováno
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
)	)	kIx)	)
Možnost	možnost	k1gFnSc1	možnost
zapojit	zapojit	k5eAaPmF	zapojit
se	se	k3xPyFc4	se
do	do	k7c2	do
významných	významný	k2eAgInPc2d1	významný
vědeckých	vědecký	k2eAgInPc2d1	vědecký
a	a	k8xC	a
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
projektů	projekt	k1gInPc2	projekt
Studium	studium	k1gNnSc1	studium
na	na	k7c6	na
zahraničních	zahraniční	k2eAgFnPc6d1	zahraniční
a	a	k8xC	a
partnerských	partnerský	k2eAgFnPc6d1	partnerská
univerzitách	univerzita	k1gFnPc6	univerzita
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
sportovních	sportovní	k2eAgFnPc2d1	sportovní
specializací	specializace	k1gFnPc2	specializace
v	v	k7c6	v
5	[number]	k4	5
vlastních	vlastní	k2eAgNnPc6d1	vlastní
sportovních	sportovní	k2eAgNnPc6d1	sportovní
centrech	centrum	k1gNnPc6	centrum
Ubytování	ubytování	k1gNnSc2	ubytování
na	na	k7c6	na
kolejích	kolej	k1gFnPc6	kolej
pro	pro	k7c4	pro
<g />
.	.	kIx.	.
</s>
<s>
většinu	většina	k1gFnSc4	většina
žadatelů	žadatel	k1gMnPc2	žadatel
9	[number]	k4	9
knihoven	knihovna	k1gFnPc2	knihovna
Stavební	stavební	k2eAgFnPc1d1	stavební
<g/>
,	,	kIx,	,
strojní	strojní	k2eAgFnPc1d1	strojní
<g/>
,	,	kIx,	,
elektrotechnické	elektrotechnický	k2eAgFnPc1d1	elektrotechnická
a	a	k8xC	a
soudní	soudní	k2eAgNnSc1d1	soudní
inženýrství	inženýrství	k1gNnSc1	inženýrství
Informační	informační	k2eAgFnSc2d1	informační
technologie	technologie	k1gFnSc2	technologie
Chemie	chemie	k1gFnSc2	chemie
Ekonomie	ekonomie	k1gFnSc2	ekonomie
a	a	k8xC	a
management	management	k1gInSc4	management
Výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
umění	umění	k1gNnSc4	umění
Architektura	architektura	k1gFnSc1	architektura
Rámcové	rámcový	k2eAgFnSc2d1	rámcová
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
univerzitami	univerzita	k1gFnPc7	univerzita
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
Mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
vzdělávací	vzdělávací	k2eAgInPc4d1	vzdělávací
programy	program	k1gInPc4	program
výzkumu	výzkum	k1gInSc2	výzkum
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
Účast	účast	k1gFnSc1	účast
na	na	k7c6	na
programech	program	k1gInPc6	program
EU	EU	kA	EU
pro	pro	k7c4	pro
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
:	:	kIx,	:
CEEPUS	CEEPUS	kA	CEEPUS
LLP	LLP	kA	LLP
<g/>
/	/	kIx~	/
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Erasmus	Erasmus	k1gMnSc1	Erasmus
Tempus	Tempus	k1gMnSc1	Tempus
Joint	Joint	k1gMnSc1	Joint
a	a	k8xC	a
double	double	k2eAgFnPc4d1	double
degree	degre	k1gFnPc4	degre
programy	program	k1gInPc1	program
Poskytují	poskytovat	k5eAaImIp3nP	poskytovat
fakulty	fakulta	k1gFnPc4	fakulta
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
předmětů	předmět	k1gInPc2	předmět
akreditovaných	akreditovaný	k2eAgInPc2d1	akreditovaný
studijních	studijní	k2eAgInPc2d1	studijní
programů	program	k1gInPc2	program
a	a	k8xC	a
na	na	k7c4	na
zakázku	zakázka	k1gFnSc4	zakázka
podnikové	podnikový	k2eAgFnSc2d1	podniková
sféry	sféra	k1gFnSc2	sféra
Manažerské	manažerský	k2eAgNnSc4d1	manažerské
vzdělání	vzdělání	k1gNnSc4	vzdělání
MBA	MBA	kA	MBA
(	(	kIx(	(
<g/>
Master	master	k1gMnSc1	master
of	of	k?	of
Business	business	k1gInSc1	business
Administration	Administration	k1gInSc1	Administration
<g/>
)	)	kIx)	)
Institut	institut	k1gInSc1	institut
celoživotního	celoživotní	k2eAgNnSc2d1	celoživotní
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
:	:	kIx,	:
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
poradenské	poradenský	k2eAgFnPc4d1	poradenská
<g/>
,	,	kIx,	,
informační	informační	k2eAgFnPc4d1	informační
a	a	k8xC	a
organizační	organizační	k2eAgFnPc4d1	organizační
služby	služba	k1gFnPc4	služba
nabízí	nabízet	k5eAaImIp3nS	nabízet
vzdělávací	vzdělávací	k2eAgFnSc4d1	vzdělávací
a	a	k8xC	a
poradenskou	poradenský	k2eAgFnSc4d1	poradenská
činnost	činnost	k1gFnSc4	činnost
pořádá	pořádat	k5eAaImIp3nS	pořádat
kurzy	kurz	k1gInPc1	kurz
a	a	k8xC	a
semináře	seminář	k1gInPc1	seminář
pro	pro	k7c4	pro
seniory	senior	k1gMnPc4	senior
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Univerzity	univerzita	k1gFnSc2	univerzita
třetího	třetí	k4xOgInSc2	třetí
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumná	výzkumný	k2eAgFnSc1d1	výzkumná
činnost	činnost	k1gFnSc1	činnost
na	na	k7c4	na
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
realizována	realizovat	k5eAaBmNgFnS	realizovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
národních	národní	k2eAgInPc2d1	národní
a	a	k8xC	a
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
projektů	projekt	k1gInPc2	projekt
<g/>
,	,	kIx,	,
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
grantů	grant	k1gInPc2	grant
a	a	k8xC	a
výzkumných	výzkumný	k2eAgNnPc2d1	výzkumné
center	centrum	k1gNnPc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
VUT	VUT	kA	VUT
intenzivně	intenzivně	k6eAd1	intenzivně
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
univerzitami	univerzita	k1gFnPc7	univerzita
a	a	k8xC	a
institucemi	instituce	k1gFnPc7	instituce
<g/>
,	,	kIx,	,
s	s	k7c7	s
Akademií	akademie	k1gFnSc7	akademie
věd	věda	k1gFnPc2	věda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
soukromými	soukromý	k2eAgFnPc7d1	soukromá
firmami	firma	k1gFnPc7	firma
<g/>
.	.	kIx.	.
</s>
<s>
Snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
integraci	integrace	k1gFnSc4	integrace
výuky	výuka	k1gFnSc2	výuka
a	a	k8xC	a
vědeckého	vědecký	k2eAgInSc2d1	vědecký
výzkumu	výzkum	k1gInSc2	výzkum
je	být	k5eAaImIp3nS	být
podporována	podporovat	k5eAaImNgFnS	podporovat
aplikační	aplikační	k2eAgFnSc7d1	aplikační
sférou	sféra	k1gFnSc7	sféra
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
pomocí	pomoc	k1gFnSc7	pomoc
se	se	k3xPyFc4	se
připravují	připravovat	k5eAaImIp3nP	připravovat
nové	nový	k2eAgInPc1d1	nový
studijní	studijní	k2eAgInPc1d1	studijní
programy	program	k1gInPc1	program
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
tak	tak	k8xS	tak
můžou	můžou	k?	můžou
získat	získat	k5eAaPmF	získat
řadu	řada	k1gFnSc4	řada
praktických	praktický	k2eAgFnPc2d1	praktická
zkušeností	zkušenost	k1gFnPc2	zkušenost
již	již	k6eAd1	již
během	během	k7c2	během
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
usnadní	usnadnit	k5eAaPmIp3nS	usnadnit
výběr	výběr	k1gInSc4	výběr
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
a	a	k8xC	a
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
konkurenceschopnost	konkurenceschopnost	k1gFnSc1	konkurenceschopnost
absolventů	absolvent	k1gMnPc2	absolvent
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
cílů	cíl	k1gInPc2	cíl
VUT	VUT	kA	VUT
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
výzkumnou	výzkumný	k2eAgFnSc7d1	výzkumná
univerzitou	univerzita	k1gFnSc7	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Environmentální	environmentální	k2eAgFnPc1d1	environmentální
technologie	technologie	k1gFnPc1	technologie
IT	IT	kA	IT
a	a	k8xC	a
komunikační	komunikační	k2eAgFnSc2d1	komunikační
technologie	technologie	k1gFnSc2	technologie
Letecké	letecký	k2eAgNnSc1d1	letecké
inženýrství	inženýrství	k1gNnSc1	inženýrství
Materiálové	materiálový	k2eAgNnSc1d1	materiálové
inženýrství	inženýrství	k1gNnSc1	inženýrství
Nanotechnologie	nanotechnologie	k1gFnSc2	nanotechnologie
a	a	k8xC	a
mikroelektronika	mikroelektronika	k1gFnSc1	mikroelektronika
Návrhy	návrh	k1gInPc1	návrh
stavebních	stavební	k2eAgFnPc2d1	stavební
a	a	k8xC	a
strojních	strojní	k2eAgFnPc2d1	strojní
konstrukcí	konstrukce	k1gFnPc2	konstrukce
Pokročilé	pokročilý	k2eAgFnSc2d1	pokročilá
polymerní	polymerní	k2eAgFnSc2d1	polymerní
a	a	k8xC	a
keramické	keramický	k2eAgInPc1d1	keramický
materiály	materiál	k1gInPc1	materiál
Procesní	procesní	k2eAgInPc1d1	procesní
a	a	k8xC	a
chemické	chemický	k2eAgInPc1d1	chemický
inženýrství	inženýrství	k1gNnSc6	inženýrství
Robotika	robotika	k1gFnSc1	robotika
a	a	k8xC	a
umělá	umělý	k2eAgFnSc1d1	umělá
inteligence	inteligence	k1gFnSc1	inteligence
Snímání	snímání	k1gNnSc2	snímání
obrazů	obraz	k1gInPc2	obraz
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
matematické	matematický	k2eAgNnSc4d1	matematické
zpracování	zpracování	k1gNnSc4	zpracování
Výrobní	výrobní	k2eAgFnSc2d1	výrobní
technologie	technologie	k1gFnSc2	technologie
<g/>
.	.	kIx.	.
</s>
<s>
AdMaS	AdMaS	k?	AdMaS
–	–	k?	–
pokročilé	pokročilý	k2eAgInPc4d1	pokročilý
stavební	stavební	k2eAgInPc4d1	stavební
materiály	materiál	k1gInPc4	materiál
<g/>
,	,	kIx,	,
konstrukce	konstrukce	k1gFnSc1	konstrukce
a	a	k8xC	a
technologie	technologie	k1gFnSc1	technologie
CEITEC	CEITEC	kA	CEITEC
–	–	k?	–
centrum	centrum	k1gNnSc1	centrum
excelence	excelence	k1gFnSc2	excelence
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
biologických	biologický	k2eAgFnPc2d1	biologická
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
technologií	technologie	k1gFnPc2	technologie
vznikající	vznikající	k2eAgFnSc1d1	vznikající
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
Vysokého	vysoký	k2eAgNnSc2d1	vysoké
učení	učení	k1gNnSc2	učení
technického	technický	k2eAgNnSc2d1	technické
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
s	s	k7c7	s
Masarykovou	Masarykův	k2eAgFnSc7d1	Masarykova
univerzitou	univerzita	k1gFnSc7	univerzita
<g/>
,	,	kIx,	,
Mendelovou	Mendelův	k2eAgFnSc7d1	Mendelova
univerzitou	univerzita	k1gFnSc7	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Veterinární	veterinární	k2eAgFnSc7d1	veterinární
a	a	k8xC	a
farmaceutickou	farmaceutický	k2eAgFnSc7d1	farmaceutická
univerzitou	univerzita	k1gFnSc7	univerzita
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Ústavem	ústav	k1gInSc7	ústav
<g />
.	.	kIx.	.
</s>
<s>
fyziky	fyzika	k1gFnSc2	fyzika
materiálů	materiál	k1gInPc2	materiál
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
a	a	k8xC	a
Výzkumným	výzkumný	k2eAgInSc7d1	výzkumný
ústavem	ústav	k1gInSc7	ústav
veterinárního	veterinární	k2eAgNnSc2d1	veterinární
lékařství	lékařství	k1gNnSc2	lékařství
SIX	SIX	kA	SIX
–	–	k?	–
Centrum	centrum	k1gNnSc1	centrum
senzorických	senzorický	k2eAgInPc2d1	senzorický
<g/>
,	,	kIx,	,
informačních	informační	k2eAgInPc2d1	informační
a	a	k8xC	a
komunikačních	komunikační	k2eAgInPc2d1	komunikační
systémů	systém	k1gInPc2	systém
Centra	centrum	k1gNnSc2	centrum
materiálového	materiálový	k2eAgInSc2d1	materiálový
výzkumu	výzkum	k1gInSc2	výzkum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
chemie	chemie	k1gFnSc2	chemie
CVVOZE	CVVOZE	kA	CVVOZE
–	–	k?	–
Centrum	centrum	k1gNnSc1	centrum
výzkumu	výzkum	k1gInSc2	výzkum
a	a	k8xC	a
využití	využití	k1gNnSc2	využití
obnovitelných	obnovitelný	k2eAgInPc2d1	obnovitelný
zdrojů	zdroj	k1gInPc2	zdroj
energie	energie	k1gFnSc2	energie
NETME	NETME	kA	NETME
Centre	centr	k1gInSc5	centr
–	–	k?	–
nové	nový	k2eAgFnSc2d1	nová
strojírenské	strojírenský	k2eAgFnSc2d1	strojírenská
technologie	technologie	k1gFnSc2	technologie
Spolupráce	spolupráce	k1gFnSc1	spolupráce
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
s	s	k7c7	s
průmyslem	průmysl	k1gInSc7	průmysl
mj.	mj.	kA	mj.
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
inovaci	inovace	k1gFnSc4	inovace
a	a	k8xC	a
přípravu	příprava	k1gFnSc4	příprava
nových	nový	k2eAgInPc2d1	nový
studijních	studijní	k2eAgInPc2d1	studijní
programů	program	k1gInPc2	program
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
průmyslovou	průmyslový	k2eAgFnSc7d1	průmyslová
sférou	sféra	k1gFnSc7	sféra
přímou	přímý	k2eAgFnSc4d1	přímá
spolupráci	spolupráce	k1gFnSc4	spolupráce
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
výzkumu	výzkum	k1gInSc2	výzkum
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
s	s	k7c7	s
podniky	podnik	k1gInPc7	podnik
v	v	k7c6	v
tuzemsku	tuzemsko	k1gNnSc6	tuzemsko
i	i	k8xC	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
osobní	osobní	k2eAgFnSc4d1	osobní
účast	účast	k1gFnSc4	účast
odborníků	odborník	k1gMnPc2	odborník
z	z	k7c2	z
praxe	praxe	k1gFnSc2	praxe
na	na	k7c6	na
výuce	výuka	k1gFnSc6	výuka
odborné	odborný	k2eAgFnSc2d1	odborná
exkurze	exkurze	k1gFnSc2	exkurze
a	a	k8xC	a
stáže	stázat	k5eAaPmIp3nS	stázat
kontaktním	kontaktní	k2eAgInSc7d1	kontaktní
bodem	bod	k1gInSc7	bod
pro	pro	k7c4	pro
spolupráci	spolupráce	k1gFnSc4	spolupráce
podnikatelské	podnikatelský	k2eAgFnSc2d1	podnikatelská
sféry	sféra	k1gFnSc2	sféra
a	a	k8xC	a
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
Útvar	útvar	k1gInSc1	útvar
<g />
.	.	kIx.	.
</s>
<s>
transferu	transfer	k1gInSc3	transfer
technologií	technologie	k1gFnPc2	technologie
–	–	k?	–
http://www.vutbr.cz/utt	[url]	k1gMnSc1	http://www.vutbr.cz/utt
Na	na	k7c6	na
škole	škola	k1gFnSc6	škola
učilo	učit	k5eAaImAgNnS	učit
více	hodně	k6eAd2	hodně
vynikajících	vynikající	k2eAgFnPc2d1	vynikající
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
získaly	získat	k5eAaPmAgFnP	získat
trvalý	trvalý	k2eAgInSc4d1	trvalý
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
respekt	respekt	k1gInSc4	respekt
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Fuchs	Fuchs	k1gMnSc1	Fuchs
Konrád	Konrád	k1gMnSc1	Konrád
Hruban	Hruban	k1gMnSc1	Hruban
Jaromír	Jaromír	k1gMnSc1	Jaromír
Krejcar	krejcar	k1gInSc4	krejcar
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Lederer	Lederer	k1gMnSc1	Lederer
Viktor	Viktor	k1gMnSc1	Viktor
Kaplan	Kaplan	k1gMnSc1	Kaplan
Vladimír	Vladimír	k1gMnSc1	Vladimír
List	lista	k1gFnPc2	lista
Bedřich	Bedřich	k1gMnSc1	Bedřich
Rozehnal	Rozehnal	k1gMnSc1	Rozehnal
František	František	k1gMnSc1	František
Weyr	Weyr	k1gMnSc1	Weyr
Oldřich	Oldřich	k1gMnSc1	Oldřich
Meduna	Meduna	k1gFnSc1	Meduna
Tomáš	Tomáš	k1gMnSc1	Tomáš
Přibyl	Přibyl	k1gMnSc1	Přibyl
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
Stanislav	Stanislav	k1gMnSc1	Stanislav
Bělehrádek	Bělehrádka	k1gFnPc2	Bělehrádka
Luděk	Luděk	k1gMnSc1	Luděk
Navara	Navar	k1gMnSc2	Navar
Pavel	Pavel	k1gMnSc1	Pavel
Novák	Novák	k1gMnSc1	Novák
Rostislav	Rostislav	k1gMnSc1	Rostislav
Slavotínek	Slavotínek	k1gMnSc1	Slavotínek
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Škromach	Škromach	k1gMnSc1	Škromach
Martin	Martin	k1gMnSc1	Martin
<g />
.	.	kIx.	.
</s>
<s>
Říman	Říman	k1gMnSc1	Říman
Martin	Martin	k1gMnSc1	Martin
Tesařík	Tesařík	k1gMnSc1	Tesařík
Milan	Milan	k1gMnSc1	Milan
Šimonovský	Šimonovský	k2eAgMnSc1d1	Šimonovský
Václav	Václav	k1gMnSc1	Václav
Mencl	Mencl	k1gMnSc1	Mencl
Petr	Petr	k1gMnSc1	Petr
Žaluda	Žalud	k1gMnSc2	Žalud
Dan	Dan	k1gMnSc1	Dan
Ťok	Ťok	k1gMnSc1	Ťok
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vysoké	vysoká	k1gFnSc2	vysoká
učení	učení	k1gNnSc2	učení
technické	technický	k2eAgInPc4d1	technický
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
vutbr	vutbra	k1gFnPc2	vutbra
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
domovská	domovský	k2eAgFnSc1d1	domovská
stránka	stránka	k1gFnSc1	stránka
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
Statut	statuta	k1gNnPc2	statuta
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
Studentský	studentský	k2eAgInSc1d1	studentský
portál	portál	k1gInSc1	portál
–	–	k?	–
portál	portál	k1gInSc1	portál
studentských	studentská	k1gFnPc2	studentská
organizací	organizace	k1gFnPc2	organizace
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
VUT	VUT	kA	VUT
Facebook	Facebook	k1gInSc1	Facebook
–	–	k?	–
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
navut	navut	k1gInSc1	navut
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
web	web	k1gInSc1	web
pro	pro	k7c4	pro
uchazeče	uchazeč	k1gMnPc4	uchazeč
o	o	k7c4	o
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
Absolventi	absolvent	k1gMnPc1	absolvent
–	–	k?	–
absolventský	absolventský	k2eAgInSc4d1	absolventský
portál	portál	k1gInSc4	portál
Útvar	útvar	k1gInSc1	útvar
transferu	transfer	k1gInSc2	transfer
technologií	technologie	k1gFnPc2	technologie
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
–	–	k?	–
kontaktní	kontaktní	k2eAgNnSc4d1	kontaktní
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
firmy	firma	k1gFnPc4	firma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
chtějí	chtít	k5eAaImIp3nP	chtít
navázat	navázat	k5eAaPmF	navázat
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
</s>
