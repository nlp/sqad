<p>
<s>
Stanislav	Stanislav	k1gMnSc1	Stanislav
Kaczmarczyk	Kaczmarczyk	k1gMnSc1	Kaczmarczyk
(	(	kIx(	(
<g/>
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1936	[number]	k4	1936
Vělopolí	Vělopolí	k1gNnSc2	Vělopolí
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
evangelický	evangelický	k2eAgMnSc1d1	evangelický
kazatel	kazatel	k1gMnSc1	kazatel
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1955	[number]	k4	1955
až	až	k9	až
1959	[number]	k4	1959
studoval	studovat	k5eAaImAgInS	studovat
teologii	teologie	k1gFnSc4	teologie
na	na	k7c6	na
Evangelické	evangelický	k2eAgFnSc6d1	evangelická
bohoslovecké	bohoslovecký	k2eAgFnSc6d1	bohoslovecká
fakultě	fakulta	k1gFnSc6	fakulta
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
hornické	hornický	k2eAgFnSc6d1	hornická
brigádě	brigáda	k1gFnSc6	brigáda
na	na	k7c6	na
Dole	dol	k1gInSc6	dol
1	[number]	k4	1
<g/>
.	.	kIx.	.
máj	máj	k1gFnSc1	máj
v	v	k7c6	v
Karviné	Karviná	k1gFnSc6	Karviná
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
povinnou	povinný	k2eAgFnSc4d1	povinná
dvouletou	dvouletý	k2eAgFnSc4d1	dvouletá
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
službu	služba	k1gFnSc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
duchovenské	duchovenský	k2eAgFnSc6d1	duchovenská
službě	služba	k1gFnSc6	služba
ve	v	k7c6	v
Slezské	slezský	k2eAgFnSc6d1	Slezská
církvi	církev	k1gFnSc6	církev
evangelické	evangelický	k2eAgFnSc2d1	evangelická
a.	a.	k?	a.
v.	v.	k?	v.
ve	v	k7c6	v
farním	farní	k2eAgInSc6d1	farní
sboru	sbor	k1gInSc6	sbor
v	v	k7c6	v
Komorní	komorní	k2eAgFnSc6d1	komorní
Lhotce	Lhotka	k1gFnSc6	Lhotka
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
vrchností	vrchnost	k1gFnSc7	vrchnost
odňat	odňat	k2eAgInSc1d1	odňat
státní	státní	k2eAgInSc1d1	státní
souhlas	souhlas	k1gInSc1	souhlas
k	k	k7c3	k
vykonávání	vykonávání	k1gNnSc3	vykonávání
duchovenské	duchovenský	k2eAgFnSc2d1	duchovenská
činnosti	činnost	k1gFnSc2	činnost
pro	pro	k7c4	pro
velmi	velmi	k6eAd1	velmi
aktivní	aktivní	k2eAgFnSc4d1	aktivní
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
mladou	mladý	k2eAgFnSc7d1	mladá
generací	generace	k1gFnSc7	generace
ve	v	k7c6	v
sboru	sbor	k1gInSc6	sbor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1975	[number]	k4	1975
až	až	k9	až
1983	[number]	k4	1983
pracoval	pracovat	k5eAaImAgInS	pracovat
v	v	k7c6	v
tiskárně	tiskárna	k1gFnSc6	tiskárna
Tisk	tisk	k1gInSc1	tisk
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Těšíně	Těšín	k1gInSc6	Těšín
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
jako	jako	k9	jako
pomocný	pomocný	k2eAgMnSc1d1	pomocný
dělník	dělník	k1gMnSc1	dělník
a	a	k8xC	a
pak	pak	k6eAd1	pak
jako	jako	k9	jako
řidič	řidič	k1gMnSc1	řidič
nákladního	nákladní	k2eAgNnSc2d1	nákladní
auta	auto	k1gNnSc2	auto
pro	pro	k7c4	pro
odvážení	odvážení	k1gNnSc4	odvážení
smetí	smetí	k1gNnSc2	smetí
na	na	k7c4	na
smetiště	smetiště	k1gNnSc4	smetiště
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
června	červen	k1gInSc2	červen
1983	[number]	k4	1983
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
farář	farář	k1gMnSc1	farář
ČCE	ČCE	kA	ČCE
v	v	k7c6	v
evangelickém	evangelický	k2eAgInSc6d1	evangelický
sboru	sbor	k1gInSc6	sbor
ve	v	k7c6	v
Štramberku	Štramberk	k1gInSc6	Štramberk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
Evangelickém	evangelický	k2eAgInSc6d1	evangelický
sboru	sbor	k1gInSc6	sbor
a.v.	a.v.	k?	a.v.
ČCE	ČCE	kA	ČCE
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Těšíně	Těšín	k1gInSc6	Těšín
Na	na	k7c6	na
Rozvoji	rozvoj	k1gInSc6	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1993	[number]	k4	1993
až	až	k9	až
2006	[number]	k4	2006
přednášel	přednášet	k5eAaImAgMnS	přednášet
na	na	k7c6	na
Katedře	katedra	k1gFnSc6	katedra
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
výchovy	výchova	k1gFnSc2	výchova
na	na	k7c6	na
Pedagogické	pedagogický	k2eAgFnSc6d1	pedagogická
fakultě	fakulta	k1gFnSc6	fakulta
Ostravské	ostravský	k2eAgFnSc2d1	Ostravská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
4	[number]	k4	4
roky	rok	k1gInPc7	rok
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
předseda	předseda	k1gMnSc1	předseda
Rady	rada	k1gFnSc2	rada
TWR-C	TWR-C	k1gFnSc2	TWR-C
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
pak	pak	k6eAd1	pak
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
jako	jako	k8xC	jako
člen	člen	k1gMnSc1	člen
Rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
založení	založení	k1gNnSc2	založení
Dětské	dětský	k2eAgFnSc2d1	dětská
misie	misie	k1gFnSc2	misie
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
byl	být	k5eAaImAgInS	být
dlouholetým	dlouholetý	k2eAgMnSc7d1	dlouholetý
členem	člen	k1gMnSc7	člen
Rady	rada	k1gFnSc2	rada
a	a	k8xC	a
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
byl	být	k5eAaImAgInS	být
také	také	k9	také
jejím	její	k3xOp3gMnSc7	její
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
stále	stále	k6eAd1	stále
jako	jako	k9	jako
hostující	hostující	k2eAgMnSc1d1	hostující
lektor	lektor	k1gMnSc1	lektor
pro	pro	k7c4	pro
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
pracovníků	pracovník	k1gMnPc2	pracovník
Dětské	dětský	k2eAgFnSc2d1	dětská
misie	misie	k1gFnSc2	misie
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
16	[number]	k4	16
let	léto	k1gNnPc2	léto
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
člen	člen	k1gMnSc1	člen
Evangelizačního	evangelizační	k2eAgInSc2d1	evangelizační
odboru	odbor	k1gInSc2	odbor
při	při	k7c6	při
Synodní	synodní	k2eAgFnSc6d1	synodní
radě	rada	k1gFnSc6	rada
ČCE	ČCE	kA	ČCE
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
<g/>
,	,	kIx,	,
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Olgou	Olga	k1gFnSc7	Olga
žije	žít	k5eAaImIp3nS	žít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starší	k1gMnSc1	starší
syn	syn	k1gMnSc1	syn
Vladislav	Vladislav	k1gMnSc1	Vladislav
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
inženýr-programátor	inženýrrogramátor	k1gMnSc1	inženýr-programátor
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgMnSc1d2	mladší
syn	syn	k1gMnSc1	syn
Daniel	Daniel	k1gMnSc1	Daniel
je	být	k5eAaImIp3nS	být
magistrem	magister	k1gMnSc7	magister
teologie	teologie	k1gFnSc2	teologie
a	a	k8xC	a
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
ředitel	ředitel	k1gMnSc1	ředitel
Wycliffovy	Wycliffův	k2eAgFnSc2d1	Wycliffův
misie	misie	k1gFnSc2	misie
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
šest	šest	k4xCc4	šest
vnoučat	vnouče	k1gNnPc2	vnouče
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Publikační	publikační	k2eAgNnSc4d1	publikační
dílo	dílo	k1gNnSc4	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Slova	slovo	k1gNnPc1	slovo
naděje	naděje	k1gFnSc2	naděje
</s>
</p>
<p>
<s>
Probuzenská	probuzenský	k2eAgNnPc1d1	probuzenský
hnutí	hnutí	k1gNnPc1	hnutí
na	na	k7c6	na
Těšínském	těšínský	k2eAgNnSc6d1	Těšínské
Slezsku	Slezsko	k1gNnSc6	Slezsko
</s>
</p>
<p>
<s>
Křesťanské	křesťanský	k2eAgNnSc1d1	křesťanské
společenství	společenství	k1gNnSc1	společenství
na	na	k7c6	na
Těšínsku	Těšínsko	k1gNnSc6	Těšínsko
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Chceš	chtít	k5eAaImIp2nS	chtít
být	být	k5eAaImF	být
vítězem	vítěz	k1gMnSc7	vítěz
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
Život	život	k1gInSc1	život
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
Kristíny	Kristína	k1gFnSc2	Kristína
Royové	Roy	k1gMnPc1	Roy
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cestou	cesta	k1gFnSc7	cesta
pravdy	pravda	k1gFnSc2	pravda
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
300	[number]	k4	300
let	let	k1gInSc1	let
Ježíšova	Ježíšův	k2eAgInSc2d1	Ježíšův
chrámu	chrám	k1gInSc2	chrám
v	v	k7c6	v
Těšíně	Těšín	k1gInSc6	Těšín
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
profesorem	profesor	k1gMnSc7	profesor
Jarem	jar	k1gInSc7	jar
Křivohlavým	Křivohlavý	k2eAgFnPc3d1	Křivohlavá
napsali	napsat	k5eAaBmAgMnP	napsat
a	a	k8xC	a
vydali	vydat	k5eAaPmAgMnP	vydat
knihu	kniha	k1gFnSc4	kniha
Poslední	poslední	k2eAgInSc4d1	poslední
úsek	úsek	k1gInSc4	úsek
cesty	cesta	k1gFnSc2	cesta
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
I	i	k9	i
pro	pro	k7c4	pro
tebe	ty	k3xPp2nSc4	ty
je	být	k5eAaImIp3nS	být
naděje	naděje	k1gFnSc1	naděje
<g/>
;	;	kIx,	;
vydala	vydat	k5eAaPmAgFnS	vydat
Synodní	synodní	k2eAgFnSc1d1	synodní
rada	rada	k1gFnSc1	rada
ČCE	ČCE	kA	ČCE
v	v	k7c6	v
r.	r.	kA	r.
1984	[number]	k4	1984
</s>
</p>
<p>
<s>
I	i	k9	i
dla	dla	k?	dla
ciebie	ciebie	k1gFnPc1	ciebie
istnieje	istniej	k1gMnSc2	istniej
nadzieja	nadziejus	k1gMnSc2	nadziejus
<g/>
;	;	kIx,	;
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
</s>
</p>
<p>
<s>
Slovenské	slovenský	k2eAgNnSc1d1	slovenské
evangelizační	evangelizační	k2eAgNnSc1d1	evangelizační
středisko	středisko	k1gNnSc1	středisko
uveřejnilo	uveřejnit	k5eAaPmAgNnS	uveřejnit
14	[number]	k4	14
jeho	jeho	k3xOp3gFnPc2	jeho
přednášek	přednáška	k1gFnPc2	přednáška
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
několika	několik	k4yIc2	několik
brožur	brožura	k1gFnPc2	brožura
a	a	k8xC	a
letáků	leták	k1gInPc2	leták
Brněnské	brněnský	k2eAgFnSc2d1	brněnská
tiskové	tiskový	k2eAgFnSc2d1	tisková
misie	misie	k1gFnSc2	misie
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Stanislav	Stanislav	k1gMnSc1	Stanislav
Kaczmarczyk	Kaczmarczyk	k1gMnSc1	Kaczmarczyk
</s>
</p>
<p>
<s>
Přednáška	přednáška	k1gFnSc1	přednáška
Pastorace	pastorace	k1gFnSc2	pastorace
umírajících	umírající	k1gFnPc2	umírající
(	(	kIx(	(
<g/>
PDF	PDF	kA	PDF
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zvukový	zvukový	k2eAgInSc1d1	zvukový
záznam	záznam	k1gInSc1	záznam
jeho	on	k3xPp3gNnSc2	on
kázání	kázání	k1gNnSc2	kázání
</s>
</p>
