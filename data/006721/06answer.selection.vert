<s>
Tisícileté	tisíciletý	k2eAgNnSc1d1	tisícileté
období	období	k1gNnSc1	období
čínské	čínský	k2eAgFnSc2d1	čínská
nadvlády	nadvláda	k1gFnSc2	nadvláda
nad	nad	k7c7	nad
územím	území	k1gNnSc7	území
Vietnamu	Vietnam	k1gInSc2	Vietnam
trvalo	trvat	k5eAaImAgNnS	trvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
938	[number]	k4	938
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Ngô	Ngô	k1gMnPc1	Ngô
Quyề	Quyề	k1gFnSc2	Quyề
taktickým	taktický	k2eAgInSc7d1	taktický
bojem	boj	k1gInSc7	boj
porazil	porazit	k5eAaPmAgMnS	porazit
čínskou	čínský	k2eAgFnSc4d1	čínská
armádu	armáda	k1gFnSc4	armáda
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Bach	Bach	k1gMnSc1	Bach
Dang	Dang	k1gMnSc1	Dang
<g/>
.	.	kIx.	.
</s>
