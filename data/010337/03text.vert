<p>
<s>
Rodzinka	Rodzinka	k1gFnSc1	Rodzinka
<g/>
.	.	kIx.	.
<g/>
pl	pl	k?	pl
je	být	k5eAaImIp3nS	být
polský	polský	k2eAgInSc1d1	polský
komediální	komediální	k2eAgInSc1d1	komediální
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
vysílaný	vysílaný	k2eAgInSc1d1	vysílaný
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
TVP	TVP	kA	TVP
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
14	[number]	k4	14
řad	řada	k1gFnPc2	řada
s	s	k7c7	s
celkem	celek	k1gInSc7	celek
265	[number]	k4	265
díly	dílo	k1gNnPc7	dílo
a	a	k8xC	a
1	[number]	k4	1
speciální	speciální	k2eAgInSc4d1	speciální
díl	díl	k1gInSc4	díl
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
pět	pět	k4xCc1	pět
sérií	série	k1gFnPc2	série
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
v	v	k7c6	v
letech	let	k1gInPc6	let
2011	[number]	k4	2011
<g/>
–	–	k?	–
<g/>
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
následovala	následovat	k5eAaImAgFnS	následovat
dvouletá	dvouletý	k2eAgFnSc1d1	dvouletá
přestávka	přestávka	k1gFnSc1	přestávka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
natočen	natočen	k2eAgInSc1d1	natočen
odvozený	odvozený	k2eAgInSc1d1	odvozený
"	"	kIx"	"
<g/>
miniseriál	miniseriál	k1gInSc1	miniseriál
<g/>
"	"	kIx"	"
Boscy	Bosca	k1gFnPc1	Bosca
w	w	k?	w
sieci	siece	k1gMnSc6	siece
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
13	[number]	k4	13
dílů	díl	k1gInPc2	díl
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
kolem	kolem	k7c2	kolem
tří	tři	k4xCgFnPc2	tři
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Seriál	seriál	k1gInSc1	seriál
Rodzinka	Rodzinka	k1gFnSc1	Rodzinka
<g/>
.	.	kIx.	.
<g/>
pl	pl	k?	pl
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
pětičlenné	pětičlenný	k2eAgFnSc6d1	pětičlenná
rodině	rodina	k1gFnSc6	rodina
Boskových	Boskův	k2eAgMnPc2d1	Boskův
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
a	a	k8xC	a
kteří	který	k3yQgMnPc1	který
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
využívají	využívat	k5eAaPmIp3nP	využívat
mobilní	mobilní	k2eAgFnSc4d1	mobilní
síť	síť	k1gFnSc4	síť
Orange	Orang	k1gInSc2	Orang
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Vysílání	vysílání	k1gNnSc2	vysílání
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Rodzinka	Rodzinka	k1gFnSc1	Rodzinka
<g/>
.	.	kIx.	.
<g/>
pl	pl	k?	pl
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rodzinka	Rodzinka	k1gFnSc1	Rodzinka
<g/>
.	.	kIx.	.
<g/>
pl	pl	k?	pl
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Rodzinka	Rodzinka	k1gFnSc1	Rodzinka
<g/>
.	.	kIx.	.
<g/>
pl	pl	k?	pl
na	na	k7c4	na
SerialZone	SerialZon	k1gInSc5	SerialZon
</s>
</p>
