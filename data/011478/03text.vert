<p>
<s>
Moonwalk	Moonwalk	k1gInSc1	Moonwalk
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
měsíční	měsíční	k2eAgFnSc2d1	měsíční
chůze	chůze	k1gFnSc2	chůze
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
taneční	taneční	k2eAgInSc1d1	taneční
pohyb	pohyb	k1gInSc1	pohyb
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c4	v
iluzi	iluze	k1gFnSc4	iluze
pohybu	pohyb	k1gInSc2	pohyb
tanečníka	tanečník	k1gMnSc2	tanečník
dopředu	dopředu	k6eAd1	dopředu
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
dozadu	dozadu	k6eAd1	dozadu
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
tanec	tanec	k1gInSc1	tanec
získal	získat	k5eAaPmAgInS	získat
popularitu	popularita	k1gFnSc4	popularita
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
jej	on	k3xPp3gMnSc4	on
zatančil	zatančit	k5eAaPmAgMnS	zatančit
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
tanečník	tanečník	k1gMnSc1	tanečník
Michael	Michael	k1gMnSc1	Michael
Jackson	Jackson	k1gMnSc1	Jackson
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
písni	píseň	k1gFnSc6	píseň
Billie	Billie	k1gFnSc2	Billie
Jean	Jean	k1gMnSc1	Jean
<g/>
,	,	kIx,	,
prezentované	prezentovaný	k2eAgFnPc1d1	prezentovaná
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
speciálu	speciál	k1gInSc6	speciál
k	k	k7c3	k
oslavě	oslava	k1gFnSc3	oslava
výročí	výročí	k1gNnSc2	výročí
nahrávací	nahrávací	k2eAgFnSc2d1	nahrávací
společnosti	společnost	k1gFnSc2	společnost
Motown	Motowna	k1gFnPc2	Motowna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Technika	technika	k1gFnSc1	technika
==	==	k?	==
</s>
</p>
<p>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
přední	přední	k2eAgFnSc1d1	přední
noha	noha	k1gFnSc1	noha
spočívá	spočívat	k5eAaImIp3nS	spočívat
celým	celý	k2eAgNnSc7d1	celé
chodidlem	chodidlo	k1gNnSc7	chodidlo
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zadní	zadní	k2eAgFnSc1d1	zadní
noha	noha	k1gFnSc1	noha
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
špičce	špička	k1gFnSc6	špička
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgFnSc1d1	přední
noha	noha	k1gFnSc1	noha
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
při	při	k7c6	při
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
lehce	lehko	k6eAd1	lehko
a	a	k8xC	a
hladce	hladko	k6eAd1	hladko
sklouzne	sklouznout	k5eAaPmIp3nS	sklouznout
dozadu	dozadu	k6eAd1	dozadu
kolem	kolem	k7c2	kolem
špičky	špička	k1gFnSc2	špička
zadní	zadní	k2eAgFnSc2d1	zadní
nohy	noha	k1gFnSc2	noha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
pozici	pozice	k1gFnSc6	pozice
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
váha	váha	k1gFnSc1	váha
na	na	k7c4	na
nynější	nynější	k2eAgFnSc4d1	nynější
přední	přední	k2eAgFnSc4d1	přední
nohu	noha	k1gFnSc4	noha
<g/>
,	,	kIx,	,
zadní	zadní	k2eAgNnSc4d1	zadní
se	se	k3xPyFc4	se
staví	stavit	k5eAaBmIp3nS	stavit
na	na	k7c4	na
špičku	špička	k1gFnSc4	špička
a	a	k8xC	a
pohyb	pohyb	k1gInSc1	pohyb
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
s	s	k7c7	s
vystřídanými	vystřídaný	k2eAgNnPc7d1	vystřídané
chodidly	chodidlo	k1gNnPc7	chodidlo
<g/>
.	.	kIx.	.
</s>
<s>
Opakováním	opakování	k1gNnSc7	opakování
těchto	tento	k3xDgInPc2	tento
kroků	krok	k1gInPc2	krok
tanečník	tanečník	k1gMnSc1	tanečník
docílí	docílit	k5eAaPmIp3nS	docílit
Moonwalku	Moonwalka	k1gFnSc4	Moonwalka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Variacemi	variace	k1gFnPc7	variace
tohoto	tento	k3xDgInSc2	tento
pohybu	pohyb	k1gInSc2	pohyb
je	být	k5eAaImIp3nS	být
pohyb	pohyb	k1gInSc4	pohyb
jiným	jiný	k2eAgInSc7d1	jiný
směrem	směr	k1gInSc7	směr
–	–	k?	–
dopředu	dopředu	k6eAd1	dopředu
<g/>
,	,	kIx,	,
do	do	k7c2	do
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
sidewalk	sidewalk	k1gInSc1	sidewalk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
v	v	k7c6	v
kruhu	kruh	k1gInSc6	kruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Moonwalk	Moonwalka	k1gFnPc2	Moonwalka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
zatančit	zatančit	k5eAaPmF	zatančit
moonwalk	moonwalk	k1gInSc4	moonwalk
-	-	kIx~	-
video	video	k1gNnSc4	video
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc1	první
performance	performance	k1gFnPc1	performance
tance	tanec	k1gInSc2	tanec
Moonwalk	Moonwalk	k1gInSc1	Moonwalk
při	pře	k1gFnSc3	pře
"	"	kIx"	"
<g/>
Billie	Billie	k1gFnSc1	Billie
Jean	Jean	k1gMnSc1	Jean
<g/>
"	"	kIx"	"
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
-	-	kIx~	-
video	video	k1gNnSc4	video
</s>
</p>
