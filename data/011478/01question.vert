<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
česky	česky	k6eAd1	česky
nazývá	nazývat	k5eAaImIp3nS	nazývat
taneční	taneční	k2eAgInSc1d1	taneční
pohyb	pohyb	k1gInSc1	pohyb
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgNnSc6	který
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
iluzi	iluze	k1gFnSc3	iluze
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tanečník	tanečník	k1gMnSc1	tanečník
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
dopředu	dopředu	k6eAd1	dopředu
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
dozadu	dozadu	k6eAd1	dozadu
<g/>
?	?	kIx.	?
</s>
