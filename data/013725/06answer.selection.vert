<s>
Termínem	termín	k1gInSc7
exarchát	exarchát	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
pravoslavných	pravoslavný	k2eAgFnPc6d1
církvích	církev	k1gFnPc6
a	a	k8xC
východních	východní	k2eAgFnPc6d1
katolických	katolický	k2eAgFnPc6d1
církvích	církev	k1gFnPc6
označuje	označovat	k5eAaImIp3nS
část	část	k1gFnSc1
božího	boží	k2eAgInSc2d1
lidu	lid	k1gInSc2
na	na	k7c6
území	území	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
nebyla	být	k5eNaImAgFnS
ještě	ještě	k9
zřízena	zřízen	k2eAgFnSc1d1
eparchie	eparchie	k1gFnSc1
<g/>
.	.	kIx.
</s>