<s>
Exarchát	exarchát	k1gInSc1
(	(	kIx(
<g/>
církev	církev	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
části	část	k1gFnSc6
božího	boží	k2eAgInSc2d1
lidu	lid	k1gInSc2
v	v	k7c6
církvi	církev	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Exarchát	exarchát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Termínem	termín	k1gInSc7
exarchát	exarchát	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
pravoslavných	pravoslavný	k2eAgFnPc6d1
církvích	církev	k1gFnPc6
a	a	k8xC
východních	východní	k2eAgFnPc6d1
katolických	katolický	k2eAgFnPc6d1
církvích	církev	k1gFnPc6
označuje	označovat	k5eAaImIp3nS
část	část	k1gFnSc1
božího	boží	k2eAgInSc2d1
lidu	lid	k1gInSc2
na	na	k7c6
území	území	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
nebyla	být	k5eNaImAgFnS
ještě	ještě	k9
zřízena	zřízen	k2eAgFnSc1d1
eparchie	eparchie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
čele	čelo	k1gNnSc6
exarchátu	exarchát	k1gInSc2
stojí	stát	k5eAaImIp3nS
exarcha	exarcha	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Starověký	starověký	k2eAgInSc1d1
význam	význam	k1gInSc1
</s>
<s>
Název	název	k1gInSc1
pro	pro	k7c4
exarchát	exarchát	k1gInSc4
byl	být	k5eAaImAgInS
určen	určit	k5eAaPmNgInS
původně	původně	k6eAd1
pro	pro	k7c4
administrativní	administrativní	k2eAgInSc4d1
celek	celek	k1gInSc4
Východořímské	východořímský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
církev	církev	k1gFnSc4
receptovala	receptovat	k5eAaPmAgFnS,k5eAaImAgFnS,k5eAaBmAgFnS
toto	tento	k3xDgNnSc4
byzantské	byzantský	k2eAgNnSc4d1
politické	politický	k2eAgNnSc4d1
správní	správní	k2eAgNnSc4d1
dělení	dělení	k1gNnSc4
a	a	k8xC
exarcha	exarcha	k1gMnSc1
<g/>
,	,	kIx,
stojící	stojící	k2eAgMnSc1d1
v	v	k7c6
čele	čelo	k1gNnSc6
byzantské	byzantský	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
<g/>
,	,	kIx,
měl	mít	k5eAaImAgMnS
ve	v	k7c6
4	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
5	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
měl	mít	k5eAaImAgMnS
hodnost	hodnost	k1gFnSc4
primase	primas	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaujímal	zaujímat	k5eAaImAgMnS
hodností	hodnost	k1gFnSc7
postavení	postavení	k1gNnSc2
mezi	mezi	k7c7
patriarchou	patriarcha	k1gMnSc7
a	a	k8xC
metropolitním	metropolitní	k2eAgMnSc7d1
biskupem	biskup	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Termín	termín	k1gInSc1
byl	být	k5eAaImAgInS
po	po	k7c6
roce	rok	k1gInSc6
451	#num#	k4
formálně	formálně	k6eAd1
omezen	omezit	k5eAaPmNgInS
<g/>
,	,	kIx,
mimo	mimo	k6eAd1
biskupů	biskup	k1gInPc2
stojících	stojící	k2eAgInPc2d1
v	v	k7c6
čele	čelo	k1gNnSc6
několika	několik	k4yIc2
velkých	velká	k1gFnPc2
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Moderní	moderní	k2eAgInSc1d1
význam	význam	k1gInSc1
</s>
<s>
Pravoslavné	pravoslavný	k2eAgFnPc1d1
církve	církev	k1gFnPc1
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
pravoslavné	pravoslavný	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
je	být	k5eAaImIp3nS
exarchou	exarcha	k1gMnSc7
hodnostář	hodnostář	k1gMnSc1
(	(	kIx(
<g/>
velekněz	velekněz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
je	být	k5eAaImIp3nS
inspektorem	inspektor	k1gMnSc7
klášterů	klášter	k1gInPc2
<g/>
,	,	kIx,
zástupcem	zástupce	k1gMnSc7
patriarchy	patriarcha	k1gMnSc2
<g/>
,	,	kIx,
a	a	k8xC
nebo	nebo	k8xC
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
vede	vést	k5eAaImIp3nS
pravoslavnou	pravoslavný	k2eAgFnSc4d1
církev	církev	k1gFnSc4
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
mimo	mimo	k7c4
území	území	k1gNnSc4
patriarchátu	patriarchát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
v	v	k7c6
USA	USA	kA
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
pravoslavné	pravoslavný	k2eAgMnPc4d1
Srby	Srb	k1gMnPc4
<g/>
,	,	kIx,
Rumuny	Rumun	k1gMnPc4
<g/>
,	,	kIx,
Bulhary	Bulhar	k1gMnPc4
a	a	k8xC
další	další	k2eAgMnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Exarcha	exarcha	k1gMnSc1
Jeruzalémského	jeruzalémský	k2eAgInSc2d1
patriarchátu	patriarchát	k1gInSc2
je	být	k5eAaImIp3nS
nositelem	nositel	k1gMnSc7
titulu	titul	k1gInSc2
„	„	k?
<g/>
Exarcha	exarcha	k1gMnSc1
Božího	boží	k2eAgInSc2d1
hrobu	hrob	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Východní	východní	k2eAgFnSc2d1
katolické	katolický	k2eAgFnSc2d1
církve	církev	k1gFnSc2
</s>
<s>
Název	název	k1gInSc1
exarchát	exarchát	k1gInSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
také	také	k9
v	v	k7c4
katolických	katolický	k2eAgFnPc2d1
církví	církev	k1gFnPc2
východního	východní	k2eAgInSc2d1
obřadu	obřad	k1gInSc2
pro	pro	k7c4
část	část	k1gFnSc4
území	území	k1gNnSc3
obývanou	obývaný	k2eAgFnSc7d1
věřícími	věřící	k1gFnPc7
východní	východní	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
podobný	podobný	k2eAgInSc1d1
eparchii	eparchie	k1gFnSc3
(	(	kIx(
<g/>
diecézi	diecéze	k1gFnSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ze	z	k7c2
zvláštních	zvláštní	k2eAgInPc2d1
důvodů	důvod	k1gInPc2
s	s	k7c7
ní	on	k3xPp3gFnSc7
není	být	k5eNaImIp3nS
na	na	k7c6
stejné	stejný	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
eparchii	eparchie	k1gFnSc4
vede	vést	k5eAaImIp3nS
sídelní	sídelní	k2eAgInSc1d1
biskup	biskup	k1gInSc1
<g/>
,	,	kIx,
exarchát	exarchát	k1gInSc1
je	být	k5eAaImIp3nS
řízen	řídit	k5eAaImNgInS
exarchou	exarcha	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
biskupem	biskup	k1gMnSc7
titulární	titulární	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
ostatní	ostatní	k1gNnSc1
co	co	k9
se	se	k3xPyFc4
však	však	k9
týká	týkat	k5eAaImIp3nS
eparchie	eparchie	k1gFnSc1
<g/>
,	,	kIx,
vztahuje	vztahovat	k5eAaImIp3nS
se	se	k3xPyFc4
také	také	k9
na	na	k7c4
exarchát	exarchát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Exarcháty	exarchát	k1gInPc7
se	se	k3xPyFc4
řídí	řídit	k5eAaImIp3nP
kánony	kánon	k1gInPc1
311-321	311-321	k4
Kodexu	kodex	k1gInSc2
kanonického	kanonický	k2eAgNnSc2d1
práva	právo	k1gNnSc2
východních	východní	k2eAgFnPc2d1
církví	církev	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP
dva	dva	k4xCgInPc1
typy	typ	k1gInPc1
exarchátů	exarchát	k1gInPc2
<g/>
:	:	kIx,
apoštolský	apoštolský	k2eAgInSc1d1
exarchát	exarchát	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
v	v	k7c6
latinské	latinský	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
koresponduje	korespondovat	k5eAaImIp3nS
s	s	k7c7
termínem	termín	k1gInSc7
apoštolský	apoštolský	k2eAgInSc4d1
vikariát	vikariát	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
arcibiskupský	arcibiskupský	k2eAgInSc1d1
exarchát	exarchát	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
s	s	k7c7
apoštolským	apoštolský	k2eAgInSc7d1
exarchátem	exarchát	k1gInSc7
analogický	analogický	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
Exarchatus	Exarchatus	k1gInSc4
na	na	k7c6
latinské	latinský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
,	,	kIx,
Esarcato	Esarcat	k2eAgNnSc1d1
na	na	k7c6
italské	italský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
Esarca	Esarca	k1gFnSc1
na	na	k7c6
italské	italský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Exarchát	exarchát	k1gInSc1
(	(	kIx(
<g/>
byzantská	byzantský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
</s>
