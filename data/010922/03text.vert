<p>
<s>
Joširó	Joširó	k?	Joširó
Jamašita	Jamašit	k2eAgFnSc1d1	Jamašita
(	(	kIx(	(
<g/>
jap.	jap.	k?	jap.
山	山	k?	山
芳	芳	k?	芳
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
japonský	japonský	k2eAgMnSc1d1	japonský
grafický	grafický	k2eAgMnSc1d1	grafický
designér	designér	k1gMnSc1	designér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Masaru	Masar	k1gInSc2	Masar
Kacumiho	Kacumi	k1gMnSc2	Kacumi
(	(	kIx(	(
<g/>
勝	勝	k?	勝
勝	勝	k?	勝
<g/>
)	)	kIx)	)
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Joširó	Joširó	k1gMnSc1	Joširó
Jamašita	Jamašit	k2eAgMnSc4d1	Jamašit
piktogramy	piktogram	k1gInPc1	piktogram
pro	pro	k7c4	pro
Letní	letní	k2eAgFnPc4d1	letní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
1964	[number]	k4	1964
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
.	.	kIx.	.
</s>
<s>
Tokijská	tokijský	k2eAgFnSc1d1	Tokijská
olympiáda	olympiáda	k1gFnSc1	olympiáda
stanovila	stanovit	k5eAaPmAgFnS	stanovit
grafické	grafický	k2eAgFnPc4d1	grafická
normy	norma	k1gFnPc4	norma
pro	pro	k7c4	pro
pozdější	pozdní	k2eAgFnPc4d2	pozdější
velké	velký	k2eAgFnPc4d1	velká
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
akce	akce	k1gFnPc4	akce
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
piktogramy	piktogram	k1gInPc1	piktogram
měly	mít	k5eAaImAgInP	mít
pro	pro	k7c4	pro
dějiny	dějiny	k1gFnPc4	dějiny
zásadní	zásadní	k2eAgInSc1d1	zásadní
význam	význam	k1gInSc1	význam
<g/>
.	.	kIx.	.
</s>
<s>
Potvrdily	potvrdit	k5eAaPmAgInP	potvrdit
aktuálnost	aktuálnost	k1gFnSc4	aktuálnost
"	"	kIx"	"
<g/>
obrázkového	obrázkový	k2eAgNnSc2d1	obrázkové
písma	písmo	k1gNnSc2	písmo
<g/>
"	"	kIx"	"
v	v	k7c6	v
moderním	moderní	k2eAgInSc6d1	moderní
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Cizinci	cizinec	k1gMnPc1	cizinec
neschopní	schopný	k2eNgMnPc1d1	neschopný
dešifrovat	dešifrovat	k5eAaBmF	dešifrovat
japonské	japonský	k2eAgInPc1d1	japonský
nápisy	nápis	k1gInPc1	nápis
se	se	k3xPyFc4	se
spolehlivě	spolehlivě	k6eAd1	spolehlivě
orientovali	orientovat	k5eAaBmAgMnP	orientovat
na	na	k7c6	na
sportovištích	sportoviště	k1gNnPc6	sportoviště
a	a	k8xC	a
akcích	akce	k1gFnPc6	akce
olympiády	olympiáda	k1gFnSc2	olympiáda
právě	právě	k9	právě
díky	díky	k7c3	díky
piktogramům	piktogram	k1gInPc3	piktogram
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
návrh	návrh	k1gInSc4	návrh
20	[number]	k4	20
piktogramů	piktogram	k1gInPc2	piktogram
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
sporty	sport	k1gInPc4	sport
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
39	[number]	k4	39
všeobecných	všeobecný	k2eAgInPc2d1	všeobecný
informačních	informační	k2eAgInPc2d1	informační
piktogramů	piktogram	k1gInPc2	piktogram
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Zdeno	Zdena	k1gFnSc5	Zdena
Kolesár	Kolesár	k1gInSc4	Kolesár
<g/>
:	:	kIx,	:
Kapitoly	kapitola	k1gFnPc4	kapitola
z	z	k7c2	z
dejín	dejína	k1gFnPc2	dejína
grafického	grafický	k2eAgInSc2d1	grafický
designu	design	k1gInSc2	design
<g/>
,	,	kIx,	,
Slovenské	slovenský	k2eAgNnSc1d1	slovenské
centrum	centrum	k1gNnSc1	centrum
dizajnu	dizajn	k1gInSc2	dizajn
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
https://web.archive.org/web/20130403210530/http://olympic-museum.de/pictograms/Picto1964.htm	[url]	k6eAd1	https://web.archive.org/web/20130403210530/http://olympic-museum.de/pictograms/Picto1964.htm
</s>
</p>
