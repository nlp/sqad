<s>
Papežská	papežský	k2eAgFnSc1d1	Papežská
akademie	akademie	k1gFnSc1	akademie
pro	pro	k7c4	pro
život	život	k1gInSc4	život
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Pontificia	Pontificia	k1gFnSc1	Pontificia
Academia	academia	k1gFnSc1	academia
Pro	pro	k7c4	pro
Vita	vit	k2eAgMnSc4d1	vit
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1994	[number]	k4	1994
dekretem	dekret	k1gInSc7	dekret
Jana	Jan	k1gMnSc2	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
