<s>
Euromed	Euromed	k1gMnSc1
</s>
<s>
Jednotka	jednotka	k1gFnSc1
Euromed	Euromed	k1gInSc1
101.003	101.003	k4
v	v	k7c6
stanici	stanice	k1gFnSc6
Valè	Valè	k1gFnPc2
</s>
<s>
Jednotka	jednotka	k1gFnSc1
Euromed	Euromed	k1gInSc1
na	na	k7c6
viaduktu	viadukt	k1gInSc6
vedoucím	vedoucí	k1gMnSc7
přes	přes	k7c4
Barcelonu	Barcelona	k1gFnSc4
</s>
<s>
Jednotka	jednotka	k1gFnSc1
Euromed	Euromed	k1gInSc1
při	při	k7c6
průjezdu	průjezd	k1gInSc6
stanicí	stanice	k1gFnSc7
Tarragona	Tarragon	k1gMnSc4
</s>
<s>
Euromed	Euromed	k1gMnSc1
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc4
pro	pro	k7c4
6	#num#	k4
vysokorychlostních	vysokorychlostní	k2eAgFnPc2d1
elektrických	elektrický	k2eAgFnPc2d1
vlakových	vlakový	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
odvozených	odvozený	k2eAgFnPc2d1
od	od	k7c2
TGV	TGV	kA
Atlantique	Atlantique	k1gFnPc2
<g/>
,	,	kIx,
provozovaných	provozovaný	k2eAgInPc2d1
od	od	k7c2
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1997	#num#	k4
na	na	k7c6
východním	východní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
Španělska	Španělsko	k1gNnSc2
jakožto	jakožto	k8xS
produkt	produkt	k1gInSc1
společnosti	společnost	k1gFnSc2
RENFE	RENFE	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlaky	vlak	k1gInPc7
jsou	být	k5eAaImIp3nP
nasazovány	nasazován	k2eAgInPc1d1
na	na	k7c6
koridoru	koridor	k1gInSc6
spojujícím	spojující	k2eAgInSc6d1
města	město	k1gNnSc2
Barcelonu	Barcelona	k1gFnSc4
<g/>
,	,	kIx,
Valencii	Valencie	k1gFnSc4
a	a	k8xC
Alicante	Alicant	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
je	být	k5eAaImIp3nS
220	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Používají	používat	k5eAaImIp3nP
se	se	k3xPyFc4
jednotky	jednotka	k1gFnSc2
AVE	ave	k1gNnSc7
S-101	S-101	k1gFnPc2
francouzského	francouzský	k2eAgMnSc2d1
výrobce	výrobce	k1gMnSc2
Alstom	Alstom	k1gInSc4
<g/>
,	,	kIx,
upravené	upravený	k2eAgFnPc1d1
na	na	k7c4
široký	široký	k2eAgInSc4d1
rozchod	rozchod	k1gInSc4
1	#num#	k4
668	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s>
Nehoda	nehoda	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2002	#num#	k4
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
stanici	stanice	k1gFnSc6
Torredembarra	Torredembarr	k1gInSc2
k	k	k7c3
nehodě	nehoda	k1gFnSc3
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
odjíždějící	odjíždějící	k2eAgInSc1d1
Euromed	Euromed	k1gInSc1
101.001	101.001	k4
srazil	srazit	k5eAaPmAgInS
s	s	k7c7
rovněž	rovněž	k6eAd1
odjíždějícím	odjíždějící	k2eAgInSc7d1
regionálním	regionální	k2eAgInSc7d1
vlakem	vlak	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
směřovaly	směřovat	k5eAaImAgFnP
současně	současně	k6eAd1
na	na	k7c4
stejnou	stejný	k2eAgFnSc4d1
kolej	kolej	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
úmrtí	úmrtí	k1gNnSc3
2	#num#	k4
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
poškození	poškození	k1gNnSc3
lokomotivy	lokomotiva	k1gFnSc2
a	a	k8xC
několika	několik	k4yIc2
vozů	vůz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poškozená	poškozený	k2eAgFnSc1d1
lokomotiva	lokomotiva	k1gFnSc1
byla	být	k5eAaImAgFnS
nahrazena	nahradit	k5eAaPmNgFnS
novou	nova	k1gFnSc7
-	-	kIx~
identickou	identický	k2eAgFnSc7d1
s	s	k7c7
TGV	TGV	kA
POS	POS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepoškozená	poškozený	k2eNgFnSc1d1
část	část	k1gFnSc1
vlaku	vlak	k1gInSc2
jezdí	jezdit	k5eAaImIp3nS
nadále	nadále	k6eAd1
(	(	kIx(
video	video	k1gNnSc1
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Trať	trať	k1gFnSc1
</s>
<s>
Barcelona-Sants	Barcelona-Sants	k6eAd1
</s>
<s>
Tarragona	Tarragona	k1gFnSc1
</s>
<s>
Castellón	Castellón	k1gInSc1
de	de	k?
la	la	k1gNnSc2
Plana	Plana	k?
</s>
<s>
Valencia-Nord	Valencia-Nord	k6eAd1
</s>
<s>
Alicante	Alicant	k1gMnSc5
<g/>
/	/	kIx~
<g/>
Alacant-Terminal	Alacant-Terminal	k1gMnSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Euromed	Euromed	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Fotografie	fotografia	k1gFnPc1
</s>
<s>
Euromed	Euromed	k1gMnSc1
/	/	kIx~
Arco	Arco	k1gMnSc1
(	(	kIx(
<g/>
SK	Sk	kA
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
Euromed	Euromed	k1gMnSc1
(	(	kIx(
<g/>
ES	ES	kA
<g/>
)	)	kIx)
</s>
<s>
Obsazovací	obsazovací	k2eAgInSc1d1
plán	plán	k1gInSc1
<g/>
(	(	kIx(
<g/>
ES	ES	kA
<g/>
)	)	kIx)
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
(	(	kIx(
<g/>
ES	ES	kA
<g/>
)	)	kIx)
</s>
<s>
Renfe	Renf	k1gMnSc5
Serie	serie	k1gFnSc2
101	#num#	k4
Archivováno	archivován	k2eAgNnSc1d1
14	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
(	(	kIx(
<g/>
ES	es	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Vysokorychlostní	vysokorychlostní	k2eAgInPc1d1
vlaky	vlak	k1gInPc1
>	>	kIx)
<g/>
249	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
</s>
<s>
AGV	AGV	kA
(	(	kIx(
<g/>
F	F	kA
/	/	kIx~
I	I	kA
<g/>
)	)	kIx)
•	•	k?
AVE	ave	k1gNnSc4
(	(	kIx(
<g/>
E	E	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
401	#num#	k4
(	(	kIx(
<g/>
I	I	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
450	#num#	k4
(	(	kIx(
<g/>
I	I	kA
<g/>
)	)	kIx)
•	•	k?
CRH	CRH	kA
(	(	kIx(
<g/>
CN	CN	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
460	#num#	k4
(	(	kIx(
<g/>
F	F	kA
/	/	kIx~
I	I	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
480	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
I	I	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
500	#num#	k4
(	(	kIx(
<g/>
I	I	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
600	#num#	k4
(	(	kIx(
<g/>
I	I	kA
<g/>
,	,	kIx,
CN	CN	kA
<g/>
)	)	kIx)
•	•	k?
ETR	ETR	kA
610	#num#	k4
(	(	kIx(
<g/>
CH	Ch	kA
/	/	kIx~
I	I	kA
<g/>
)	)	kIx)
•	•	k?
Eurostar	Eurostar	k1gMnSc1
(	(	kIx(
<g/>
UK	UK	kA
/	/	kIx~
F	F	kA
/	/	kIx~
B	B	kA
<g/>
)	)	kIx)
•	•	k?
ICE	ICE	kA
(	(	kIx(
<g/>
D	D	kA
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
F	F	kA
/	/	kIx~
B	B	kA
/	/	kIx~
NL	NL	kA
/	/	kIx~
A	A	kA
/	/	kIx~
CH	Ch	kA
/	/	kIx~
E	E	kA
/	/	kIx~
RU	RU	kA
/	/	kIx~
CN	CN	kA
<g/>
)	)	kIx)
•	•	k?
KTX	KTX	kA
(	(	kIx(
<g/>
KR	KR	kA
<g/>
)	)	kIx)
•	•	k?
RENFE	RENFE	kA
S-120	S-120	k1gFnSc2
(	(	kIx(
<g/>
E	E	kA
/	/	kIx~
TR	TR	kA
<g/>
)	)	kIx)
•	•	k?
Šinkansen	Šinkansen	k1gInSc1
(	(	kIx(
<g/>
J	J	kA
/	/	kIx~
TW	TW	kA
<g/>
,	,	kIx,
CN	CN	kA
<g/>
)	)	kIx)
•	•	k?
Talgo	Talgo	k1gMnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
E	E	kA
<g/>
)	)	kIx)
•	•	k?
TGV	TGV	kA
(	(	kIx(
<g/>
F	F	kA
/	/	kIx~
D	D	kA
/	/	kIx~
CH	Ch	kA
/	/	kIx~
I	I	kA
/	/	kIx~
E	E	kA
/	/	kIx~
B	B	kA
/	/	kIx~
LX	LX	kA
<g/>
)	)	kIx)
•	•	k?
Thalys	Thalysa	k1gFnPc2
(	(	kIx(
<g/>
F	F	kA
/	/	kIx~
B	B	kA
/	/	kIx~
NL	NL	kA
/	/	kIx~
D	D	kA
<g/>
)	)	kIx)
•	•	k?
V	v	k7c6
250	#num#	k4
(	(	kIx(
<g/>
B	B	kA
/	/	kIx~
NL	NL	kA
<g/>
)	)	kIx)
•	•	k?
Zefiro	Zefiro	k1gNnSc4
(	(	kIx(
<g/>
CN	CN	kA
/	/	kIx~
I	I	kA
<g/>
)	)	kIx)
</s>
<s>
Transrapid	Transrapid	k1gInSc1
<g/>
*	*	kIx~
(	(	kIx(
<g/>
D	D	kA
/	/	kIx~
CN	CN	kA
<g/>
)	)	kIx)
•	•	k?
JR-Maglev	JR-Maglev	k1gFnSc1
<g/>
*	*	kIx~
(	(	kIx(
<g/>
J	J	kA
<g/>
)	)	kIx)
•	•	k?
Swissmetro	Swissmetro	k1gNnSc1
<g/>
**	**	k?
(	(	kIx(
<g/>
CH	Ch	kA
<g/>
)	)	kIx)
*	*	kIx~
<g/>
technologie	technologie	k1gFnSc1
Maglev	Maglev	k1gMnSc1
**	**	k?
<g/>
Maglev	Maglev	k1gMnSc1
ve	v	k7c6
vakuovém	vakuový	k2eAgInSc6d1
tunelu	tunel	k1gInSc6
(	(	kIx(
<g/>
Vactrain	Vactrain	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
