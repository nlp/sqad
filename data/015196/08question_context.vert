<s>
Euromed	Euromed	k1gMnSc1
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc4
pro	pro	k7c4
6	#num#	k4
vysokorychlostních	vysokorychlostní	k2eAgFnPc2d1
elektrických	elektrický	k2eAgFnPc2d1
vlakových	vlakový	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
odvozených	odvozený	k2eAgFnPc2d1
od	od	k7c2
TGV	TGV	kA
Atlantique	Atlantique	k1gFnPc2
<g/>
,	,	kIx,
provozovaných	provozovaný	k2eAgInPc2d1
od	od	k7c2
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1997	#num#	k4
na	na	k7c6
východním	východní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
Španělska	Španělsko	k1gNnSc2
jakožto	jakožto	k8xS
produkt	produkt	k1gInSc1
společnosti	společnost	k1gFnSc2
RENFE	RENFE	kA
<g/>
.	.	kIx.
</s>