<s>
Standardní	standardní	k2eAgFnPc1d1	standardní
prodloužené	prodloužená	k1gFnPc1	prodloužená
nebo	nebo	k8xC	nebo
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
zápalky	zápalka	k1gFnPc1	zápalka
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
domácnostní	domácnostní	k2eAgInPc1d1	domácnostní
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
delší	dlouhý	k2eAgInPc1d2	delší
(	(	kIx(	(
<g/>
cca	cca	kA	cca
5	[number]	k4	5
cm	cm	kA	cm
<g/>
)	)	kIx)	)
–	–	k?	–
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
usnadnění	usnadnění	k1gNnSc2	usnadnění
zapálení	zapálení	k1gNnSc2	zapálení
hořáku	hořák	k1gInSc2	hořák
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
stojí	stát	k5eAaImIp3nS	stát
např.	např.	kA	např.
hrnec	hrnec	k1gInSc1	hrnec
<g/>
.	.	kIx.	.
</s>
