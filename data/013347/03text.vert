<p>
<s>
1984	[number]	k4	1984
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
vypsáno	vypsat	k5eAaPmNgNnS	vypsat
slovy	slovo	k1gNnPc7	slovo
Nineteen	Nineteen	k2eAgMnSc1d1	Nineteen
Eighty-Four	Eighty-Four	k1gMnSc1	Eighty-Four
<g/>
,	,	kIx,	,
pracovním	pracovní	k2eAgInSc7d1	pracovní
názvem	název	k1gInSc7	název
Poslední	poslední	k2eAgMnSc1d1	poslední
člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
antiutopický	antiutopický	k2eAgMnSc1d1	antiutopický
(	(	kIx(	(
<g/>
dystopický	dystopický	k2eAgInSc1d1	dystopický
<g/>
)	)	kIx)	)
román	román	k1gInSc1	román
anglického	anglický	k2eAgMnSc2d1	anglický
spisovatele	spisovatel	k1gMnSc2	spisovatel
George	Georg	k1gMnSc2	Georg
Orwella	Orwell	k1gMnSc2	Orwell
dokončený	dokončený	k2eAgInSc4d1	dokončený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
publikován	publikován	k2eAgMnSc1d1	publikován
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Secker	Secker	k1gInSc1	Secker
and	and	k?	and
Warburg	Warburg	k1gInSc1	Warburg
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
pochází	pocházet	k5eAaImIp3nS	pocházet
známý	známý	k2eAgInSc1d1	známý
výrok	výrok	k1gInSc1	výrok
Velký	velký	k2eAgMnSc1d1	velký
bratr	bratr	k1gMnSc1	bratr
tě	ty	k3xPp2nSc4	ty
sleduje	sledovat	k5eAaImIp3nS	sledovat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejzásadnějších	zásadní	k2eAgNnPc2d3	nejzásadnější
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
vládne	vládnout	k5eAaImIp3nS	vládnout
absolutní	absolutní	k2eAgFnSc1d1	absolutní
totalita	totalita	k1gFnSc1	totalita
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
živí	živit	k5eAaImIp3nP	živit
permanentní	permanentní	k2eAgMnPc1d1	permanentní
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
udržovací	udržovací	k2eAgFnSc1d1	udržovací
<g/>
"	"	kIx"	"
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
třemi	tři	k4xCgFnPc7	tři
kontinentálními	kontinentální	k2eAgFnPc7d1	kontinentální
mocnostmi	mocnost	k1gFnPc7	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Společnosti	společnost	k1gFnSc3	společnost
vládne	vládnout	k5eAaImIp3nS	vládnout
Strana	strana	k1gFnSc1	strana
a	a	k8xC	a
cokoli	cokoli	k3yInSc4	cokoli
proti	proti	k7c3	proti
kolektivnímu	kolektivní	k2eAgNnSc3d1	kolektivní
myšlení	myšlení	k1gNnSc3	myšlení
Strany	strana	k1gFnSc2	strana
je	být	k5eAaImIp3nS	být
krutě	krutě	k6eAd1	krutě
trestáno	trestán	k2eAgNnSc1d1	trestáno
<g/>
,	,	kIx,	,
lidská	lidský	k2eAgFnSc1d1	lidská
individualita	individualita	k1gFnSc1	individualita
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
zločinem	zločin	k1gInSc7	zločin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
O	o	k7c6	o
knize	kniha	k1gFnSc6	kniha
==	==	k?	==
</s>
</p>
<p>
<s>
Časopis	časopis	k1gInSc1	časopis
TIME	TIME	kA	TIME
zařadil	zařadit	k5eAaPmAgInS	zařadit
1984	[number]	k4	1984
mezi	mezi	k7c4	mezi
stovku	stovka	k1gFnSc4	stovka
nejvlivnějších	vlivný	k2eAgFnPc2d3	nejvlivnější
knih	kniha	k1gFnPc2	kniha
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skandálu	skandál	k1gInSc6	skandál
špehování	špehování	k1gNnSc2	špehování
NSA	NSA	kA	NSA
zveřejněném	zveřejněný	k2eAgInSc6d1	zveřejněný
Edwardem	Edward	k1gMnSc7	Edward
Snowdenem	Snowden	k1gMnSc7	Snowden
prodej	prodej	k1gInSc4	prodej
knihy	kniha	k1gFnSc2	kniha
rekordně	rekordně	k6eAd1	rekordně
stoupl	stoupnout	k5eAaPmAgInS	stoupnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zrcadlení	zrcadlení	k1gNnSc4	zrcadlení
1984	[number]	k4	1984
v	v	k7c6	v
realitě	realita	k1gFnSc6	realita
===	===	k?	===
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
1984	[number]	k4	1984
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
dobou	doba	k1gFnSc7	doba
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
přímo	přímo	k6eAd1	přímo
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
totalitní	totalitní	k2eAgInPc4d1	totalitní
režimy	režim	k1gInPc4	režim
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Zvířecí	zvířecí	k2eAgFnSc2d1	zvířecí
farmy	farma	k1gFnSc2	farma
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jasný	jasný	k2eAgInSc4d1	jasný
komunismus	komunismus	k1gInSc4	komunismus
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zde	zde	k6eAd1	zde
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
zejména	zejména	k9	zejména
na	na	k7c4	na
skrytou	skrytý	k2eAgFnSc4d1	skrytá
totalitu	totalita	k1gFnSc4	totalita
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
si	se	k3xPyFc3	se
uvědomíme	uvědomit	k5eAaPmIp1nP	uvědomit
především	především	k9	především
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Winston	Winston	k1gInSc1	Winston
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
že	že	k8xS	že
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
Oceánie	Oceánie	k1gFnSc2	Oceánie
se	se	k3xPyFc4	se
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
členy	člen	k1gMnPc4	člen
Vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
2	[number]	k4	2
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
cca	cca	kA	cca
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
skutečně	skutečně	k6eAd1	skutečně
vládnou	vládnout	k5eAaImIp3nP	vládnout
<g/>
,	,	kIx,	,
členy	člen	k1gMnPc4	člen
Vnější	vnější	k2eAgFnSc2d1	vnější
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
pracují	pracovat	k5eAaImIp3nP	pracovat
pro	pro	k7c4	pro
stát	stát	k1gInSc4	stát
a	a	k8xC	a
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
jeho	jeho	k3xOp3gFnSc4	jeho
vůli	vůle	k1gFnSc4	vůle
(	(	kIx(	(
<g/>
členem	člen	k1gInSc7	člen
je	být	k5eAaImIp3nS	být
i	i	k9	i
Winston	Winston	k1gInSc1	Winston
<g/>
)	)	kIx)	)
a	a	k8xC	a
proletariát	proletariát	k1gInSc1	proletariát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
85	[number]	k4	85
%	%	kIx~	%
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Proléti	Prolét	k1gMnPc1	Prolét
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
méněcenné	méněcenný	k2eAgMnPc4d1	méněcenný
<g/>
,	,	kIx,	,
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
chudých	chudý	k2eAgInPc6d1	chudý
poměrech	poměr	k1gInPc6	poměr
<g/>
,	,	kIx,	,
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
o	o	k7c4	o
ně	on	k3xPp3gInPc4	on
nestará	starat	k5eNaImIp3nS	starat
a	a	k8xC	a
nechává	nechávat	k5eAaImIp3nS	nechávat
jim	on	k3xPp3gMnPc3	on
relativní	relativní	k2eAgFnPc4d1	relativní
volnost	volnost	k1gFnSc4	volnost
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
je	být	k5eAaImIp3nS	být
ani	ani	k8xC	ani
nesleduje	sledovat	k5eNaImIp3nS	sledovat
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
členy	člen	k1gMnPc4	člen
Vnější	vnější	k2eAgFnSc2d1	vnější
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
ale	ale	k9	ale
také	také	k9	také
vycházel	vycházet	k5eAaImAgMnS	vycházet
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
zkušeností	zkušenost	k1gFnPc2	zkušenost
s	s	k7c7	s
válečnou	válečný	k2eAgFnSc7d1	válečná
realitou	realita	k1gFnSc7	realita
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Orwell	Orwell	k1gMnSc1	Orwell
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
volnou	volný	k2eAgFnSc4d1	volná
extrapolaci	extrapolace	k1gFnSc4	extrapolace
svých	svůj	k3xOyFgFnPc2	svůj
zkušeností	zkušenost	k1gFnPc2	zkušenost
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
režimy	režim	k1gInPc7	režim
do	do	k7c2	do
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
aspektech	aspekt	k1gInPc6	aspekt
blízko	blízko	k6eAd1	blízko
fiktivnímu	fiktivní	k2eAgInSc3d1	fiktivní
systému	systém	k1gInSc3	systém
Oceánie	Oceánie	k1gFnSc2	Oceánie
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
stalinská	stalinský	k2eAgFnSc1d1	stalinská
diktatura	diktatura	k1gFnSc1	diktatura
v	v	k7c6	v
KLDR	KLDR	kA	KLDR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dostupnost	dostupnost	k1gFnSc4	dostupnost
textu	text	k1gInSc2	text
===	===	k?	===
</s>
</p>
<p>
<s>
Původní	původní	k2eAgInSc1d1	původní
text	text	k1gInSc1	text
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
většina	většina	k1gFnSc1	většina
Orwellovy	Orwellův	k2eAgFnSc2d1	Orwellova
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
,	,	kIx,	,
dostupný	dostupný	k2eAgMnSc1d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
kopiích	kopie	k1gFnPc6	kopie
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
public	publicum	k1gNnPc2	publicum
domain	domaina	k1gFnPc2	domaina
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
internetu	internet	k1gInSc6	internet
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
Evy	Eva	k1gFnSc2	Eva
Šimečkové	Šimečková	k1gFnSc2	Šimečková
s	s	k7c7	s
doslovem	doslov	k1gInSc7	doslov
Milana	Milan	k1gMnSc2	Milan
Šimečky	Šimeček	k1gMnPc7	Šimeček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
Československu	Československo	k1gNnSc6	Československo
vznikal	vznikat	k5eAaImAgInS	vznikat
Orwellův	Orwellův	k2eAgInSc1d1	Orwellův
překlad	překlad	k1gInSc1	překlad
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983-4	[number]	k4	1983-4
v	v	k7c6	v
undergroundových	undergroundový	k2eAgFnPc6d1	undergroundová
podmínkách	podmínka	k1gFnPc6	podmínka
samizdatu	samizdat	k1gInSc2	samizdat
(	(	kIx(	(
<g/>
tajné	tajný	k2eAgNnSc1d1	tajné
rozdělování	rozdělování	k1gNnSc1	rozdělování
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
překladu	překlad	k1gInSc6	překlad
<g/>
,	,	kIx,	,
šíření	šíření	k1gNnSc4	šíření
přes	přes	k7c4	přes
známé	známý	k2eAgNnSc4d1	známé
<g/>
,	,	kIx,	,
psaní	psaní	k1gNnSc4	psaní
na	na	k7c6	na
stroji	stroj	k1gInSc6	stroj
s	s	k7c7	s
deseti	deset	k4xCc7	deset
"	"	kIx"	"
<g/>
kopíráky	kopírák	k1gInPc4	kopírák
<g/>
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
překladatelů	překladatel	k1gMnPc2	překladatel
se	se	k3xPyFc4	se
v	v	k7c6	v
doslovu	doslov	k1gInSc6	doslov
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
překladů	překlad	k1gInPc2	překlad
zmínil	zmínit	k5eAaPmAgMnS	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
době	doba	k1gFnSc6	doba
překládání	překládání	k1gNnSc2	překládání
zažíval	zažívat	k5eAaImAgMnS	zažívat
pocity	pocit	k1gInPc1	pocit
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgInPc1d1	podobný
některým	některý	k3yIgInPc3	některý
momentům	moment	k1gInPc3	moment
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
románu	román	k1gInSc2	román
je	být	k5eAaImIp3nS	být
úředník	úředník	k1gMnSc1	úředník
<g/>
,	,	kIx,	,
soudruh	soudruh	k1gMnSc1	soudruh
Winston	Winston	k1gInSc4	Winston
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
žije	žít	k5eAaImIp3nS	žít
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
ve	v	k7c6	v
fiktivní	fiktivní	k2eAgFnSc6d1	fiktivní
zemi	zem	k1gFnSc6	zem
Oceánii	Oceánie	k1gFnSc6	Oceánie
<g/>
.	.	kIx.	.
</s>
<s>
Vládne	vládnout	k5eAaImIp3nS	vládnout
velmi	velmi	k6eAd1	velmi
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
diktatura	diktatura	k1gFnSc1	diktatura
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
tzv.	tzv.	kA	tzv.
Velkým	velký	k2eAgMnSc7d1	velký
bratrem	bratr	k1gMnSc7	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
ideologie	ideologie	k1gFnSc1	ideologie
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Angsoc	Angsoc	k1gFnSc1	Angsoc
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
anglický	anglický	k2eAgInSc1d1	anglický
socialismus	socialismus	k1gInSc1	socialismus
<g/>
,	,	kIx,	,
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
knihy	kniha	k1gFnSc2	kniha
Ingsoc	Ingsoc	k1gFnSc1	Ingsoc
<g/>
)	)	kIx)	)
a	a	k8xC	a
její	její	k3xOp3gNnPc1	její
ústřední	ústřední	k2eAgNnPc1d1	ústřední
hesla	heslo	k1gNnPc1	heslo
zní	znět	k5eAaImIp3nS	znět
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
je	být	k5eAaImIp3nS	být
mír	mír	k1gInSc4	mír
</s>
</p>
<p>
<s>
Svoboda	Svoboda	k1gMnSc1	Svoboda
je	být	k5eAaImIp3nS	být
otroctví	otroctví	k1gNnSc4	otroctví
</s>
</p>
<p>
<s>
Nevědomost	nevědomost	k1gFnSc1	nevědomost
je	být	k5eAaImIp3nS	být
sílaŽivotní	sílaŽivotní	k2eAgFnSc1d1	sílaŽivotní
úroveň	úroveň	k1gFnSc1	úroveň
je	být	k5eAaImIp3nS	být
bídná	bídný	k2eAgFnSc1d1	bídná
a	a	k8xC	a
zhoršuje	zhoršovat	k5eAaImIp3nS	zhoršovat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Winston	Winston	k1gInSc1	Winston
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
Ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
pravdy	pravda	k1gFnSc2	pravda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
manipulováním	manipulování	k1gNnSc7	manipulování
minulosti	minulost	k1gFnSc2	minulost
<g/>
,	,	kIx,	,
upravování	upravování	k1gNnSc2	upravování
dokumentů	dokument	k1gInPc2	dokument
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgInP	být
stále	stále	k6eAd1	stále
platné	platný	k2eAgInPc1d1	platný
<g/>
.	.	kIx.	.
</s>
<s>
Winstonovou	Winstonový	k2eAgFnSc7d1	Winstonová
prací	práce	k1gFnSc7	práce
je	být	k5eAaImIp3nS	být
upravovat	upravovat	k5eAaImF	upravovat
stará	starý	k2eAgNnPc4d1	staré
čísla	číslo	k1gNnPc4	číslo
novin	novina	k1gFnPc2	novina
<g/>
.	.	kIx.	.
</s>
<s>
Oceánie	Oceánie	k1gFnSc1	Oceánie
vede	vést	k5eAaImIp3nS	vést
střídavě	střídavě	k6eAd1	střídavě
válku	válka	k1gFnSc4	válka
se	s	k7c7	s
zeměmi	zem	k1gFnPc7	zem
Eurasie	Eurasie	k1gFnSc2	Eurasie
a	a	k8xC	a
Eastasie	Eastasie	k1gFnSc2	Eastasie
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nS	změnit
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
ze	z	k7c2	z
spojence	spojenec	k1gMnSc2	spojenec
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nepřítel	nepřítel	k1gMnSc1	nepřítel
a	a	k8xC	a
z	z	k7c2	z
nepřítele	nepřítel	k1gMnSc2	nepřítel
spojenec	spojenec	k1gMnSc1	spojenec
<g/>
,	,	kIx,	,
Winston	Winston	k1gInSc1	Winston
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
úředníci	úředník	k1gMnPc1	úředník
přepisují	přepisovat	k5eAaImIp3nP	přepisovat
všechny	všechen	k3xTgInPc4	všechen
články	článek	k1gInPc4	článek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vyšly	vyjít	k5eAaPmAgFnP	vyjít
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgMnSc1d1	velký
bratr	bratr	k1gMnSc1	bratr
totiž	totiž	k9	totiž
nikdy	nikdy	k6eAd1	nikdy
nemění	měnit	k5eNaImIp3nS	měnit
svá	svůj	k3xOyFgNnPc4	svůj
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
absolutní	absolutní	k2eAgFnSc4d1	absolutní
pravdu	pravda	k1gFnSc4	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
přepisují	přepisovat	k5eAaImIp3nP	přepisovat
předpovědi	předpověď	k1gFnPc1	předpověď
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
válečného	válečný	k2eAgInSc2d1	válečný
vývoje	vývoj	k1gInSc2	vývoj
apod.	apod.	kA	apod.
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
nelze	lze	k6eNd1	lze
spolehnout	spolehnout	k5eAaPmF	spolehnout
na	na	k7c4	na
pravdivost	pravdivost	k1gFnSc4	pravdivost
ani	ani	k8xC	ani
u	u	k7c2	u
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Winston	Winston	k1gInSc1	Winston
se	se	k3xPyFc4	se
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
bránit	bránit	k5eAaImF	bránit
myšlenkám	myšlenka	k1gFnPc3	myšlenka
na	na	k7c4	na
minulost	minulost	k1gFnSc4	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
začíná	začínat	k5eAaImIp3nS	začínat
pátrat	pátrat	k5eAaImF	pátrat
<g/>
,	,	kIx,	,
jaký	jaký	k3yIgInSc1	jaký
byl	být	k5eAaImAgInS	být
život	život	k1gInSc4	život
před	před	k7c7	před
nastolením	nastolení	k1gNnSc7	nastolení
diktatury	diktatura	k1gFnSc2	diktatura
Strany	strana	k1gFnSc2	strana
a	a	k8xC	a
domnívá	domnívat	k5eAaImIp3nS	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebyl	být	k5eNaImAgInS	být
takový	takový	k3xDgInSc1	takový
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jej	on	k3xPp3gMnSc4	on
líčí	líčit	k5eAaImIp3nS	líčit
oficiální	oficiální	k2eAgFnSc1d1	oficiální
propaganda	propaganda	k1gFnSc1	propaganda
<g/>
.	.	kIx.	.
</s>
<s>
Přemýšlí	přemýšlet	k5eAaImIp3nS	přemýšlet
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
matce	matka	k1gFnSc6	matka
a	a	k8xC	a
sestře	sestra	k1gFnSc6	sestra
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zmizely	zmizet	k5eAaPmAgInP	zmizet
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
malý	malý	k2eAgMnSc1d1	malý
kluk	kluk	k1gMnSc1	kluk
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
si	se	k3xPyFc3	se
psát	psát	k5eAaImF	psát
deník	deník	k1gInSc4	deník
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
ale	ale	k9	ale
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
kroku	krok	k1gInSc6	krok
i	i	k9	i
v	v	k7c6	v
soukromí	soukromí	k1gNnSc6	soukromí
sleduje	sledovat	k5eAaImIp3nS	sledovat
ideopolicie	ideopolicie	k1gFnSc1	ideopolicie
pomocí	pomocí	k7c2	pomocí
všudypřítomné	všudypřítomný	k2eAgFnSc2d1	všudypřítomná
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
všechny	všechen	k3xTgMnPc4	všechen
ostatní	ostatní	k2eAgMnPc4d1	ostatní
členy	člen	k1gMnPc4	člen
Strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenky	myšlenka	k1gFnPc4	myšlenka
proti	proti	k7c3	proti
Straně	strana	k1gFnSc3	strana
jsou	být	k5eAaImIp3nP	být
ideozločin	ideozločina	k1gFnPc2	ideozločina
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
tvrdě	tvrdě	k6eAd1	tvrdě
trestá	trestat	k5eAaImIp3nS	trestat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
člověka	člověk	k1gMnSc4	člověk
může	moct	k5eAaImIp3nS	moct
prozradit	prozradit	k5eAaPmF	prozradit
i	i	k9	i
pouhý	pouhý	k2eAgInSc4d1	pouhý
výraz	výraz	k1gInSc4	výraz
tváře	tvář	k1gFnSc2	tvář
či	či	k8xC	či
mluvení	mluvení	k1gNnSc1	mluvení
ze	z	k7c2	z
spaní	spaní	k1gNnSc2	spaní
<g/>
.	.	kIx.	.
</s>
<s>
Děti	dítě	k1gFnPc1	dítě
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
stranické	stranický	k2eAgFnSc6d1	stranická
organizaci	organizace	k1gFnSc6	organizace
vychovávány	vychováván	k2eAgFnPc1d1	vychovávána
k	k	k7c3	k
udávání	udávání	k1gNnSc3	udávání
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
děje	děj	k1gInSc2	děj
se	se	k3xPyFc4	se
do	do	k7c2	do
Winstona	Winston	k1gMnSc2	Winston
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
Julie	Julie	k1gFnSc1	Julie
<g/>
.	.	kIx.	.
</s>
<s>
Láska	láska	k1gFnSc1	láska
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
i	i	k9	i
sexuální	sexuální	k2eAgFnSc1d1	sexuální
touha	touha	k1gFnSc1	touha
je	být	k5eAaImIp3nS	být
ideozločin	ideozločin	k2eAgMnSc1d1	ideozločin
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
oba	dva	k4xCgMnPc1	dva
pečlivě	pečlivě	k6eAd1	pečlivě
skrývají	skrývat	k5eAaImIp3nP	skrývat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Julií	Julie	k1gFnSc7	Julie
o	o	k7c6	o
samotě	samota	k1gFnSc6	samota
mimo	mimo	k7c4	mimo
dosah	dosah	k1gInSc4	dosah
techniky	technika	k1gFnSc2	technika
může	moct	k5eAaImIp3nS	moct
Winston	Winston	k1gInSc1	Winston
mluvit	mluvit	k5eAaImF	mluvit
svobodně	svobodně	k6eAd1	svobodně
<g/>
.	.	kIx.	.
</s>
<s>
Julie	Julie	k1gFnSc1	Julie
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
také	také	k9	také
proti	proti	k7c3	proti
straně	strana	k1gFnSc3	strana
a	a	k8xC	a
Winston	Winston	k1gInSc1	Winston
není	být	k5eNaImIp3nS	být
její	její	k3xOp3gNnSc1	její
první	první	k4xOgFnSc1	první
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
první	první	k4xOgMnSc1	první
milenec	milenec	k1gMnSc1	milenec
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
ale	ale	k9	ale
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
vědomi	vědom	k2eAgMnPc1d1	vědom
<g/>
,	,	kIx,	,
že	že	k8xS	že
ideopolicie	ideopolicie	k1gFnSc1	ideopolicie
je	být	k5eAaImIp3nS	být
dříve	dříve	k6eAd2	dříve
či	či	k8xC	či
později	pozdě	k6eAd2	pozdě
odhalí	odhalit	k5eAaPmIp3nS	odhalit
a	a	k8xC	a
zničí	zničit	k5eAaPmIp3nS	zničit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
Oceánie	Oceánie	k1gFnSc2	Oceánie
se	se	k3xPyFc4	se
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
členy	člen	k1gMnPc4	člen
Vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
2	[number]	k4	2
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
cca	cca	kA	cca
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
skutečně	skutečně	k6eAd1	skutečně
vládnou	vládnout	k5eAaImIp3nP	vládnout
<g/>
,	,	kIx,	,
členy	člen	k1gMnPc4	člen
Vnější	vnější	k2eAgFnSc2d1	vnější
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
pracují	pracovat	k5eAaImIp3nP	pracovat
pro	pro	k7c4	pro
stát	stát	k1gInSc4	stát
a	a	k8xC	a
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
jeho	jeho	k3xOp3gFnSc4	jeho
vůli	vůle	k1gFnSc4	vůle
(	(	kIx(	(
<g/>
členem	člen	k1gInSc7	člen
je	být	k5eAaImIp3nS	být
i	i	k9	i
Winston	Winston	k1gInSc1	Winston
<g/>
)	)	kIx)	)
a	a	k8xC	a
proletariát	proletariát	k1gInSc1	proletariát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
85	[number]	k4	85
%	%	kIx~	%
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Proléti	Prolét	k1gMnPc1	Prolét
jsou	být	k5eAaImIp3nP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
méněcenné	méněcenný	k2eAgMnPc4d1	méněcenný
<g/>
,	,	kIx,	,
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
chudých	chudý	k2eAgInPc6d1	chudý
poměrech	poměr	k1gInPc6	poměr
<g/>
,	,	kIx,	,
stát	stát	k5eAaPmF	stát
se	se	k3xPyFc4	se
o	o	k7c4	o
ně	on	k3xPp3gInPc4	on
nestará	starat	k5eNaImIp3nS	starat
a	a	k8xC	a
nechává	nechávat	k5eAaImIp3nS	nechávat
jim	on	k3xPp3gMnPc3	on
relativní	relativní	k2eAgFnPc4d1	relativní
volnost	volnost	k1gFnSc4	volnost
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
neobjeví	objevit	k5eNaPmIp3nS	objevit
někdo	někdo	k3yInSc1	někdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
pro	pro	k7c4	pro
Stranu	strana	k1gFnSc4	strana
stát	stát	k5eAaImF	stát
nebezpečným	bezpečný	k2eNgFnPc3d1	nebezpečná
<g/>
.	.	kIx.	.
</s>
<s>
Winstonovi	Winstonův	k2eAgMnPc1d1	Winstonův
a	a	k8xC	a
Julii	Julie	k1gFnSc4	Julie
se	se	k3xPyFc4	se
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
dostane	dostat	k5eAaPmIp3nS	dostat
kniha	kniha	k1gFnSc1	kniha
známého	známý	k2eAgMnSc2d1	známý
odpůrce	odpůrce	k1gMnSc2	odpůrce
režimu	režim	k1gInSc2	režim
Emanuela	Emanuel	k1gMnSc4	Emanuel
Goldsteina	Goldstein	k1gMnSc4	Goldstein
<g/>
,	,	kIx,	,
Teorie	teorie	k1gFnSc1	teorie
a	a	k8xC	a
praxe	praxe	k1gFnSc1	praxe
oligarchického	oligarchický	k2eAgInSc2d1	oligarchický
kolektivismu	kolektivismus	k1gInSc2	kolektivismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
se	se	k3xPyFc4	se
Winston	Winston	k1gInSc1	Winston
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
jak	jak	k6eAd1	jak
celý	celý	k2eAgInSc1d1	celý
systém	systém	k1gInSc1	systém
funguje	fungovat	k5eAaImIp3nS	fungovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedozví	dozvědět	k5eNaPmIp3nS	dozvědět
se	se	k3xPyFc4	se
proč	proč	k6eAd1	proč
svět	svět	k1gInSc1	svět
vypadá	vypadat	k5eAaImIp3nS	vypadat
právě	právě	k9	právě
tak	tak	k6eAd1	tak
<g/>
.	.	kIx.	.
</s>
<s>
Otázka	otázka	k1gFnSc1	otázka
proč	proč	k6eAd1	proč
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
symbolicky	symbolicky	k6eAd1	symbolicky
nezodpovězena	zodpovědět	k5eNaPmNgFnS	zodpovědět
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
románu	román	k1gInSc6	román
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nakonec	nakonec	k6eAd1	nakonec
jsou	být	k5eAaImIp3nP	být
Winston	Winston	k1gInSc4	Winston
i	i	k8xC	i
Julie	Julie	k1gFnSc1	Julie
zatčeni	zatčen	k2eAgMnPc1d1	zatčen
a	a	k8xC	a
odvedeni	odveden	k2eAgMnPc1d1	odveden
do	do	k7c2	do
cel	cela	k1gFnPc2	cela
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Winston	Winston	k1gInSc1	Winston
při	při	k7c6	při
těžkém	těžký	k2eAgNnSc6d1	těžké
mučení	mučení	k1gNnSc6	mučení
prozradí	prozradit	k5eAaPmIp3nS	prozradit
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
ví	vědět	k5eAaImIp3nS	vědět
a	a	k8xC	a
mnohem	mnohem	k6eAd1	mnohem
víc	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Přeje	přát	k5eAaImIp3nS	přát
si	se	k3xPyFc3	se
zemřít	zemřít	k5eAaPmF	zemřít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemá	mít	k5eNaImIp3nS	mít
možnost	možnost	k1gFnSc4	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Ptá	ptat	k5eAaImIp3nS	ptat
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
i	i	k9	i
svých	svůj	k3xOyFgMnPc2	svůj
mučitelů	mučitel	k1gMnPc2	mučitel
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
Strana	strana	k1gFnSc1	strana
ještě	ještě	k9	ještě
chce	chtít	k5eAaImIp3nS	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
pochopí	pochopit	k5eAaPmIp3nP	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
cílem	cíl	k1gInSc7	cíl
vůbec	vůbec	k9	vůbec
není	být	k5eNaImIp3nS	být
dostat	dostat	k5eAaPmF	dostat
z	z	k7c2	z
vězně	vězeň	k1gMnSc2	vězeň
informace	informace	k1gFnSc2	informace
a	a	k8xC	a
pak	pak	k6eAd1	pak
jej	on	k3xPp3gMnSc4	on
popravit	popravit	k5eAaPmF	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
chce	chtít	k5eAaImIp3nS	chtít
úplné	úplný	k2eAgNnSc4d1	úplné
pokoření	pokoření	k1gNnSc4	pokoření
a	a	k8xC	a
obrat	obrat	k1gInSc4	obrat
myšlení	myšlení	k1gNnSc2	myšlení
o	o	k7c4	o
180	[number]	k4	180
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Chce	chtít	k5eAaImIp3nS	chtít
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
upřímně	upřímně	k6eAd1	upřímně
miloval	milovat	k5eAaImAgMnS	milovat
Velkého	velký	k2eAgMnSc4d1	velký
bratra	bratr	k1gMnSc4	bratr
a	a	k8xC	a
bezmezně	bezmezně	k6eAd1	bezmezně
mu	on	k3xPp3gMnSc3	on
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
zhola	zhola	k6eAd1	zhola
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
surovost	surovost	k1gFnSc1	surovost
promyšleně	promyšleně	k6eAd1	promyšleně
kombinovaná	kombinovaný	k2eAgFnSc1d1	kombinovaná
s	s	k7c7	s
vlídnými	vlídný	k2eAgFnPc7d1	vlídná
domluvami	domluva	k1gFnPc7	domluva
a	a	k8xC	a
obdobími	období	k1gNnPc7	období
klidu	klid	k1gInSc3	klid
nakonec	nakonec	k6eAd1	nakonec
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
cíli	cíl	k1gInSc3	cíl
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
hraje	hrát	k5eAaImIp3nS	hrát
jistě	jistě	k9	jistě
i	i	k9	i
Stockholmský	stockholmský	k2eAgInSc1d1	stockholmský
syndrom	syndrom	k1gInSc1	syndrom
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Winston	Winston	k1gInSc1	Winston
v	v	k7c6	v
okamžicích	okamžik	k1gInPc6	okamžik
vrcholné	vrcholný	k2eAgFnSc2d1	vrcholná
hrůzy	hrůza	k1gFnSc2	hrůza
přestane	přestat	k5eAaPmIp3nS	přestat
dokonce	dokonce	k9	dokonce
i	i	k9	i
milovat	milovat	k5eAaImF	milovat
Julii	Julie	k1gFnSc4	Julie
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
pokořen	pokořen	k2eAgInSc1d1	pokořen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vězeň	vězeň	k1gMnSc1	vězeň
překvapivě	překvapivě	k6eAd1	překvapivě
ocitá	ocitat	k5eAaImIp3nS	ocitat
zpátky	zpátky	k6eAd1	zpátky
na	na	k7c6	na
svobodě	svoboda	k1gFnSc6	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
jiným	jiný	k2eAgMnSc7d1	jiný
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
je	být	k5eAaImIp3nS	být
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
na	na	k7c6	na
Ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
pravdy	pravda	k1gFnSc2	pravda
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
lépe	dobře	k6eAd2	dobře
placen	placen	k2eAgMnSc1d1	placen
<g/>
.	.	kIx.	.
</s>
<s>
Ideopolicie	Ideopolicie	k1gFnSc1	Ideopolicie
už	už	k6eAd1	už
se	se	k3xPyFc4	se
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
nezajímá	zajímat	k5eNaImIp3nS	zajímat
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
by	by	kYmCp3nS	by
ani	ani	k9	ani
nenapadlo	napadnout	k5eNaPmAgNnS	napadnout
proti	proti	k7c3	proti
něčemu	něco	k3yInSc3	něco
protestovat	protestovat	k5eAaBmF	protestovat
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
jen	jen	k9	jen
v	v	k7c6	v
myšlenkách	myšlenka	k1gFnPc6	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Setká	setkat	k5eAaPmIp3nS	setkat
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
Julií	Julie	k1gFnSc7	Julie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ona	onen	k3xDgFnSc1	onen
ho	on	k3xPp3gMnSc4	on
zradila	zradit	k5eAaPmAgFnS	zradit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
podrobená	podrobený	k2eAgFnSc1d1	podrobená
<g/>
.	.	kIx.	.
</s>
<s>
Winston	Winston	k1gInSc1	Winston
si	se	k3xPyFc3	se
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
navzdory	navzdory	k7c3	navzdory
všemu	všecek	k3xTgNnSc3	všecek
teď	teď	k6eAd1	teď
Velkého	velký	k2eAgMnSc2d1	velký
bratra	bratr	k1gMnSc2	bratr
miluje	milovat	k5eAaImIp3nS	milovat
a	a	k8xC	a
ctí	čest	k1gFnSc7	čest
<g/>
.	.	kIx.	.
</s>
<s>
Čtenář	čtenář	k1gMnSc1	čtenář
ale	ale	k9	ale
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
ani	ani	k8xC	ani
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Velký	velký	k2eAgMnSc1d1	velký
bratr	bratr	k1gMnSc1	bratr
je	být	k5eAaImIp3nS	být
žijící	žijící	k2eAgFnSc1d1	žijící
osoba	osoba	k1gFnSc1	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Goldstein	Goldstein	k1gInSc1	Goldstein
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
kniha	kniha	k1gFnSc1	kniha
jsou	být	k5eAaImIp3nP	být
promyšleným	promyšlený	k2eAgInSc7d1	promyšlený
dílem	díl	k1gInSc7	díl
Strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
knize	kniha	k1gFnSc6	kniha
se	se	k3xPyFc4	se
podílelo	podílet	k5eAaImAgNnS	podílet
více	hodně	k6eAd2	hodně
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
např.	např.	kA	např.
i	i	k8xC	i
O	O	kA	O
<g/>
'	'	kIx"	'
<g/>
Brien	Brien	k1gInSc1	Brien
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
Winstonovi	Winston	k1gMnSc3	Winston
knihu	kniha	k1gFnSc4	kniha
dal	dát	k5eAaPmAgMnS	dát
a	a	k8xC	a
na	na	k7c6	na
Ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
lásky	láska	k1gFnSc2	láska
ho	on	k3xPp3gMnSc4	on
poté	poté	k6eAd1	poté
vyslýchal	vyslýchat	k5eAaImAgMnS	vyslýchat
a	a	k8xC	a
mučil	mučit	k5eAaImAgMnS	mučit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Děj	děj	k1gInSc1	děj
knihy	kniha	k1gFnSc2	kniha
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
kde	kde	k6eAd1	kde
neplatí	platit	k5eNaImIp3nP	platit
žádné	žádný	k3yNgInPc4	žádný
psané	psaný	k2eAgInPc4d1	psaný
zákony	zákon	k1gInPc4	zákon
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
kam	kam	k6eAd1	kam
utéct	utéct	k5eAaPmF	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vylíčení	vylíčení	k1gNnSc4	vylíčení
diktatury	diktatura	k1gFnSc2	diktatura
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vládne	vládnout	k5eAaImIp3nS	vládnout
tak	tak	k6eAd1	tak
efektivně	efektivně	k6eAd1	efektivně
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
přestávají	přestávat	k5eAaImIp3nP	přestávat
uvědomovat	uvědomovat	k5eAaImF	uvědomovat
jiné	jiný	k2eAgFnPc4d1	jiná
možnosti	možnost	k1gFnPc4	možnost
<g/>
,	,	kIx,	,
přestávají	přestávat	k5eAaImIp3nP	přestávat
myslet	myslet	k5eAaImF	myslet
<g/>
.	.	kIx.	.
</s>
<s>
Vnímají	vnímat	k5eAaImIp3nP	vnímat
jen	jen	k9	jen
hesla	heslo	k1gNnPc1	heslo
a	a	k8xC	a
strojově	strojově	k6eAd1	strojově
vytvářenou	vytvářený	k2eAgFnSc4d1	vytvářená
zábavu	zábava	k1gFnSc4	zábava
(	(	kIx(	(
<g/>
romány	román	k1gInPc1	román
<g/>
,	,	kIx,	,
poezii	poezie	k1gFnSc4	poezie
i	i	k8xC	i
hudbu	hudba	k1gFnSc4	hudba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
nikdo	nikdo	k3yNnSc1	nikdo
už	už	k6eAd1	už
na	na	k7c6	na
způsobu	způsob	k1gInSc6	způsob
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
lze	lze	k6eAd1	lze
shrnout	shrnout	k5eAaPmF	shrnout
do	do	k7c2	do
několika	několik	k4yIc2	několik
hesel	heslo	k1gNnPc2	heslo
<g/>
,	,	kIx,	,
nevidí	vidět	k5eNaImIp3nS	vidět
nic	nic	k3yNnSc1	nic
špatného	špatný	k2eAgNnSc2d1	špatné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Některé	některý	k3yIgInPc1	některý
principy	princip	k1gInPc1	princip
Orwellova	Orwellův	k2eAgInSc2d1	Orwellův
světa	svět	k1gInSc2	svět
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vaporizace	Vaporizace	k1gFnSc2	Vaporizace
===	===	k?	===
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
tématem	téma	k1gNnSc7	téma
románu	román	k1gInSc2	román
je	být	k5eAaImIp3nS	být
manipulace	manipulace	k1gFnSc1	manipulace
<g/>
.	.	kIx.	.
</s>
<s>
Totalitní	totalitní	k2eAgInSc1d1	totalitní
systém	systém	k1gInSc1	systém
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
efektivně	efektivně	k6eAd1	efektivně
měnit	měnit	k5eAaImF	měnit
minulost	minulost	k1gFnSc4	minulost
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
dvěma	dva	k4xCgFnPc7	dva
věcmi	věc	k1gFnPc7	věc
<g/>
:	:	kIx,	:
hmatatelnými	hmatatelný	k2eAgInPc7d1	hmatatelný
dokumenty	dokument	k1gInPc7	dokument
a	a	k8xC	a
lidskými	lidský	k2eAgFnPc7d1	lidská
vzpomínkami	vzpomínka	k1gFnPc7	vzpomínka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
manipulaci	manipulace	k1gFnSc3	manipulace
veškerých	veškerý	k3xTgInPc2	veškerý
dokumentů	dokument	k1gInPc2	dokument
slouží	sloužit	k5eAaImIp3nS	sloužit
aparát	aparát	k1gInSc1	aparát
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
pravdy	pravda	k1gFnSc2	pravda
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
kdykoli	kdykoli	k6eAd1	kdykoli
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
změnit	změnit	k5eAaPmF	změnit
veškeré	veškerý	k3xTgInPc4	veškerý
záznamy	záznam	k1gInPc4	záznam
o	o	k7c6	o
zvolené	zvolený	k2eAgFnSc3d1	zvolená
události	událost	k1gFnSc3	událost
či	či	k8xC	či
člověku	člověk	k1gMnSc3	člověk
na	na	k7c4	na
něco	něco	k3yInSc4	něco
jiného	jiný	k2eAgMnSc4d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
například	například	k6eAd1	například
nepohodlnou	pohodlný	k2eNgFnSc4d1	nepohodlná
osobu	osoba	k1gFnSc4	osoba
tzv.	tzv.	kA	tzv.
vaporizovat	vaporizovat	k5eAaBmF	vaporizovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
vymazat	vymazat	k5eAaPmF	vymazat
z	z	k7c2	z
existence	existence	k1gFnSc2	existence
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
vapor	vapor	k1gInSc1	vapor
-	-	kIx~	-
pára	pára	k1gFnSc1	pára
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zničit	zničit	k5eAaPmF	zničit
veškeré	veškerý	k3xTgFnPc4	veškerý
zmínky	zmínka	k1gFnPc4	zmínka
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
existovala	existovat	k5eAaImAgFnS	existovat
<g/>
,	,	kIx,	,
i	i	k8xC	i
o	o	k7c6	o
samotné	samotný	k2eAgFnSc6d1	samotná
vaporizaci	vaporizace	k1gFnSc6	vaporizace
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
minulosti	minulost	k1gFnSc2	minulost
však	však	k9	však
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
naprostá	naprostý	k2eAgFnSc1d1	naprostá
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
např.	např.	kA	např.
přepisují	přepisovat	k5eAaImIp3nP	přepisovat
všechny	všechen	k3xTgInPc4	všechen
články	článek	k1gInPc4	článek
v	v	k7c6	v
novinách	novina	k1gFnPc6	novina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
vaporizovaná	vaporizovaný	k2eAgFnSc1d1	vaporizovaná
osoba	osoba	k1gFnSc1	osoba
zmíněna	zmíněn	k2eAgFnSc1d1	zmíněna
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
noviny	novina	k1gFnPc1	novina
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
znovu	znovu	k6eAd1	znovu
tisknou	tisknout	k5eAaImIp3nP	tisknout
a	a	k8xC	a
novými	nový	k2eAgInPc7d1	nový
výtisky	výtisk	k1gInPc7	výtisk
se	se	k3xPyFc4	se
nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
všechny	všechen	k3xTgInPc1	všechen
staré	starý	k2eAgInPc1d1	starý
výtisky	výtisk	k1gInPc1	výtisk
v	v	k7c6	v
knihovnách	knihovna	k1gFnPc6	knihovna
a	a	k8xC	a
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
<g/>
To	ten	k3xDgNnSc4	ten
by	by	kYmCp3nS	by
ale	ale	k9	ale
na	na	k7c4	na
úplnou	úplný	k2eAgFnSc4d1	úplná
změnu	změna	k1gFnSc4	změna
minulosti	minulost	k1gFnSc2	minulost
nestačilo	stačit	k5eNaBmAgNnS	stačit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
vymazat	vymazat	k5eAaPmF	vymazat
i	i	k9	i
nehmotné	hmotný	k2eNgFnPc4d1	nehmotná
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
<g/>
,	,	kIx,	,
zmanipulovat	zmanipulovat	k5eAaPmF	zmanipulovat
i	i	k9	i
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Doublethink	Doublethink	k1gInSc4	Doublethink
===	===	k?	===
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
kontrole	kontrola	k1gFnSc3	kontrola
kolektivního	kolektivní	k2eAgNnSc2d1	kolektivní
myšlení	myšlení	k1gNnSc2	myšlení
existuje	existovat	k5eAaImIp3nS	existovat
v	v	k7c6	v
Oceánii	Oceánie	k1gFnSc6	Oceánie
řada	řada	k1gFnSc1	řada
propracovaných	propracovaný	k2eAgFnPc2d1	propracovaná
technik	technika	k1gFnPc2	technika
<g/>
,	,	kIx,	,
především	především	k9	především
tzv.	tzv.	kA	tzv.
doublethink	doublethink	k1gInSc1	doublethink
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
dvojité	dvojitý	k2eAgNnSc4d1	dvojité
myšlení	myšlení	k1gNnSc4	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Doublethink	Doublethink	k1gInSc1	Doublethink
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc4	schopnost
udržet	udržet	k5eAaPmF	udržet
pohromadě	pohromadě	k6eAd1	pohromadě
dvě	dva	k4xCgFnPc4	dva
zcela	zcela	k6eAd1	zcela
protikladné	protikladný	k2eAgFnPc4d1	protikladná
informace	informace	k1gFnPc4	informace
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
oběma	dva	k4xCgMnPc7	dva
pevně	pevně	k6eAd1	pevně
věřit	věřit	k5eAaImF	věřit
<g/>
.	.	kIx.	.
</s>
<s>
Musí	muset	k5eAaImIp3nS	muset
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
vědomý	vědomý	k2eAgInSc4d1	vědomý
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
inteligenci	inteligence	k1gFnSc4	inteligence
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
nevědomý	vědomý	k2eNgMnSc1d1	nevědomý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
lež	lež	k1gFnSc1	lež
není	být	k5eNaImIp3nS	být
přípustná	přípustný	k2eAgFnSc1d1	přípustná
<g/>
,	,	kIx,	,
Strana	strana	k1gFnSc1	strana
nelže	lhát	k5eNaImIp3nS	lhát
<g/>
.	.	kIx.	.
</s>
<s>
Samo	sám	k3xTgNnSc1	sám
používání	používání	k1gNnSc1	používání
slova	slovo	k1gNnSc2	slovo
doublethink	doublethink	k1gInSc4	doublethink
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
tuto	tento	k3xDgFnSc4	tento
schopnost	schopnost	k1gFnSc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
příkladem	příklad	k1gInSc7	příklad
použití	použití	k1gNnSc2	použití
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
názvy	název	k1gInPc4	název
ministerstev	ministerstvo	k1gNnPc2	ministerstvo
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
lásky	láska	k1gFnSc2	láska
(	(	kIx(	(
<g/>
v	v	k7c6	v
newspeaku	newspeak	k1gInSc6	newspeak
Lamini	lamin	k2eAgMnPc1d1	lamin
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
cely	cela	k1gFnPc1	cela
a	a	k8xC	a
mučírny	mučírna	k1gFnPc1	mučírna
pro	pro	k7c4	pro
politické	politický	k2eAgMnPc4d1	politický
vězně	vězeň	k1gMnPc4	vězeň
<g/>
,	,	kIx,	,
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
pravdy	pravda	k1gFnSc2	pravda
(	(	kIx(	(
<g/>
Pramini	Pramin	k2eAgMnPc1d1	Pramin
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
programově	programově	k6eAd1	programově
zabývá	zabývat	k5eAaImIp3nS	zabývat
lhaním	lhaní	k1gNnSc7	lhaní
<g/>
,	,	kIx,	,
ekonomika	ekonomika	k1gFnSc1	ekonomika
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
hojnosti	hojnost	k1gFnSc2	hojnost
(	(	kIx(	(
<g/>
Hojmini	Hojmin	k2eAgMnPc1d1	Hojmin
<g/>
)	)	kIx)	)
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
hladomoru	hladomor	k1gInSc2	hladomor
<g/>
,	,	kIx,	,
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
míru	mír	k1gInSc2	mír
(	(	kIx(	(
<g/>
Mírmini	Mírmin	k2eAgMnPc1d1	Mírmin
<g/>
)	)	kIx)	)
řídí	řídit	k5eAaImIp3nP	řídit
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
cvičený	cvičený	k2eAgMnSc1d1	cvičený
v	v	k7c6	v
doublethinku	doublethink	k1gInSc6	doublethink
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
ale	ale	k9	ale
nevidí	vidět	k5eNaImIp3nS	vidět
spor	spor	k1gInSc1	spor
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vnitřně	vnitřně	k6eAd1	vnitřně
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vaporizovaná	vaporizovaný	k2eAgFnSc1d1	vaporizovaná
osoba	osoba	k1gFnSc1	osoba
nikdy	nikdy	k6eAd1	nikdy
neexistovala	existovat	k5eNaImAgFnS	existovat
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
by	by	kYmCp3nS	by
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
vlastní	vlastní	k2eAgFnSc4d1	vlastní
matku	matka	k1gFnSc4	matka
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jestliže	jestliže	k8xS	jestliže
Strana	strana	k1gFnSc1	strana
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Eurasie	Eurasie	k1gFnSc1	Eurasie
je	být	k5eAaImIp3nS	být
úhlavní	úhlavní	k2eAgMnSc1d1	úhlavní
nepřítel	nepřítel	k1gMnSc1	nepřítel
Oceánie	Oceánie	k1gFnSc2	Oceánie
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
nepřítelem	nepřítel	k1gMnSc7	nepřítel
odjakživa	odjakživa	k6eAd1	odjakživa
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
klamnou	klamný	k2eAgFnSc4d1	klamná
vzpomínku	vzpomínka	k1gFnSc4	vzpomínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
ještě	ještě	k9	ještě
před	před	k7c7	před
10	[number]	k4	10
minutami	minuta	k1gFnPc7	minuta
byla	být	k5eAaImAgNnP	být
spojencem	spojenec	k1gMnSc7	spojenec
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
===	===	k?	===
Newspeak	Newspeak	k1gInSc4	Newspeak
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
řadového	řadový	k2eAgNnSc2d1	řadové
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Oceánie	Oceánie	k1gFnSc2	Oceánie
se	se	k3xPyFc4	se
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
upřímně	upřímně	k6eAd1	upřímně
jásalo	jásat	k5eAaImAgNnS	jásat
nad	nad	k7c7	nad
prací	práce	k1gFnSc7	práce
Strany	strana	k1gFnSc2	strana
a	a	k8xC	a
ze	z	k7c2	z
srdce	srdce	k1gNnSc2	srdce
nenávidělo	návidět	k5eNaImAgNnS	návidět
její	její	k3xOp3gMnPc4	její
nepřátele	nepřítel	k1gMnPc4	nepřítel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
o	o	k7c6	o
ničem	nic	k3yNnSc6	nic
moc	moc	k6eAd1	moc
nepřemýšlelo	přemýšlet	k5eNaImAgNnS	přemýšlet
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgFnPc1	veškerý
lidské	lidský	k2eAgFnPc1d1	lidská
emoce	emoce	k1gFnPc1	emoce
<g/>
,	,	kIx,	,
city	cit	k1gInPc1	cit
a	a	k8xC	a
potřeby	potřeba	k1gFnPc1	potřeba
jsou	být	k5eAaImIp3nP	být
programově	programově	k6eAd1	programově
přesměrovány	přesměrován	k2eAgInPc1d1	přesměrován
na	na	k7c4	na
jiné	jiný	k2eAgInPc4d1	jiný
cíle	cíl	k1gInPc4	cíl
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
ideologií	ideologie	k1gFnSc7	ideologie
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nástrojů	nástroj	k1gInPc2	nástroj
k	k	k7c3	k
upevnění	upevnění	k1gNnSc3	upevnění
pravověrného	pravověrný	k2eAgNnSc2d1	pravověrné
myšlení	myšlení	k1gNnSc2	myšlení
je	být	k5eAaImIp3nS	být
newspeak	newspeak	k1gInSc1	newspeak
<g/>
,	,	kIx,	,
jazyk	jazyk	k1gInSc1	jazyk
uměle	uměle	k6eAd1	uměle
vytvářený	vytvářený	k2eAgInSc1d1	vytvářený
speciálním	speciální	k2eAgNnSc7d1	speciální
oddělením	oddělení	k1gNnSc7	oddělení
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
pravdy	pravda	k1gFnSc2	pravda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Oceánii	Oceánie	k1gFnSc6	Oceánie
zcela	zcela	k6eAd1	zcela
nahradit	nahradit	k5eAaPmF	nahradit
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
oldspeak	oldspeak	k6eAd1	oldspeak
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
zjednodušení	zjednodušení	k1gNnSc1	zjednodušení
jazyka	jazyk	k1gInSc2	jazyk
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ideologicky	ideologicky	k6eAd1	ideologicky
závadné	závadný	k2eAgFnSc2d1	závadná
myšlenky	myšlenka	k1gFnSc2	myšlenka
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
ani	ani	k8xC	ani
slovně	slovně	k6eAd1	slovně
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
aby	aby	kYmCp3nP	aby
působily	působit	k5eAaImAgFnP	působit
absurdně	absurdně	k6eAd1	absurdně
už	už	k6eAd1	už
na	na	k7c4	na
první	první	k4xOgInSc4	první
dojem	dojem	k1gInSc4	dojem
<g/>
.	.	kIx.	.
</s>
<s>
Newspeak	Newspeak	k1gInSc1	Newspeak
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgInSc7d1	jediný
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
se	se	k3xPyFc4	se
nerozšiřuje	rozšiřovat	k5eNaImIp3nS	rozšiřovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
zužuje	zužovat	k5eAaImIp3nS	zužovat
s	s	k7c7	s
každým	každý	k3xTgNnSc7	každý
vydáním	vydání	k1gNnSc7	vydání
nové	nový	k2eAgFnSc2d1	nová
verze	verze	k1gFnSc2	verze
Slovníku	slovník	k1gInSc2	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc4	všechen
vedlejší	vedlejší	k2eAgInPc4d1	vedlejší
významy	význam	k1gInPc4	význam
slov	slovo	k1gNnPc2	slovo
jsou	být	k5eAaImIp3nP	být
potlačeny	potlačen	k2eAgInPc1d1	potlačen
<g/>
,	,	kIx,	,
slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
zredukována	zredukovat	k5eAaPmNgFnS	zredukovat
na	na	k7c4	na
nezbytné	nezbytný	k2eAgNnSc4d1	nezbytný
minimum	minimum	k1gNnSc4	minimum
<g/>
,	,	kIx,	,
citové	citový	k2eAgNnSc1d1	citové
zabarvení	zabarvení	k1gNnSc1	zabarvení
mají	mít	k5eAaImIp3nP	mít
jen	jen	k9	jen
výrazy	výraz	k1gInPc1	výraz
podporující	podporující	k2eAgInSc1d1	podporující
Angsoc	Angsoc	k1gInSc4	Angsoc
a	a	k8xC	a
odsuzující	odsuzující	k2eAgNnSc4d1	odsuzující
jeho	jeho	k3xOp3gMnPc4	jeho
nepřátele	nepřítel	k1gMnPc4	nepřítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Příkladem	příklad	k1gInSc7	příklad
Newspeaku	Newspeak	k1gInSc2	Newspeak
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
nejefektivnější	efektivní	k2eAgFnSc6d3	nejefektivnější
podobě	podoba	k1gFnSc6	podoba
je	být	k5eAaImIp3nS	být
záhlaví	záhlaví	k1gNnSc1	záhlaví
novin	novina	k1gFnPc2	novina
Times	Timesa	k1gFnPc2	Timesa
z	z	k7c2	z
románu	román	k1gInSc2	román
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Oldthinkers	Oldthinkers	k1gInSc1	Oldthinkers
unbellyfeel	unbellyfeel	k1gInSc1	unbellyfeel
Ingsoc	Ingsoc	k1gFnSc1	Ingsoc
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
do	do	k7c2	do
běžného	běžný	k2eAgInSc2d1	běžný
jazyka	jazyk	k1gInSc2	jazyk
má	mít	k5eAaImIp3nS	mít
znamenat	znamenat	k5eAaImF	znamenat
"	"	kIx"	"
<g/>
Ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
názory	názor	k1gInPc1	názor
byly	být	k5eAaImAgInP	být
utvářeny	utvářit	k5eAaPmNgInP	utvářit
před	před	k7c7	před
Revolucí	revoluce	k1gFnSc7	revoluce
<g/>
,	,	kIx,	,
nemohou	moct	k5eNaImIp3nP	moct
plně	plně	k6eAd1	plně
porozumět	porozumět	k5eAaPmF	porozumět
principům	princip	k1gInPc3	princip
anglického	anglický	k2eAgInSc2d1	anglický
socialismu	socialismus	k1gInSc2	socialismus
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Ideozločin	Ideozločin	k2eAgMnSc1d1	Ideozločin
===	===	k?	===
</s>
</p>
<p>
<s>
Ideozločin	Ideozločin	k2eAgInSc1d1	Ideozločin
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
thoughtcrime	thoughtcrimat	k5eAaPmIp3nS	thoughtcrimat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zločin	zločin	k1gMnSc1	zločin
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
spáchán	spáchat	k5eAaPmNgInS	spáchat
nejenom	nejenom	k6eAd1	nejenom
slovy	slovo	k1gNnPc7	slovo
či	či	k8xC	či
skutky	skutek	k1gInPc7	skutek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
myšlením	myšlení	k1gNnSc7	myšlení
nebo	nebo	k8xC	nebo
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
myšlenky	myšlenka	k1gFnSc2	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
definice	definice	k1gFnSc1	definice
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ideozločin	ideozločin	k2eAgInSc1d1	ideozločin
je	být	k5eAaImIp3nS	být
jakýkoli	jakýkoli	k3yIgInSc1	jakýkoli
zločin	zločin	k1gInSc1	zločin
proti	proti	k7c3	proti
Straně	strana	k1gFnSc3	strana
<g/>
.	.	kIx.	.
</s>
<s>
Ideozločin	Ideozločina	k1gFnPc2	Ideozločina
je	být	k5eAaImIp3nS	být
trestán	trestat	k5eAaImNgMnS	trestat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
zločin	zločin	k1gInSc1	zločin
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
i	i	k8xC	i
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
opatření	opatření	k1gNnSc1	opatření
newspeaku	newspeak	k1gInSc2	newspeak
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pomalu	pomalu	k6eAd1	pomalu
ale	ale	k8xC	ale
jistě	jistě	k6eAd1	jistě
omezuje	omezovat	k5eAaImIp3nS	omezovat
"	"	kIx"	"
<g/>
manévrovací	manévrovací	k2eAgInSc4d1	manévrovací
prostor	prostor	k1gInSc4	prostor
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
vyjádření	vyjádření	k1gNnSc4	vyjádření
závadných	závadný	k2eAgFnPc2d1	závadná
myšlenek	myšlenka	k1gFnPc2	myšlenka
(	(	kIx(	(
<g/>
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k8xC	i
myšlenek	myšlenka	k1gFnPc2	myšlenka
na	na	k7c4	na
samotný	samotný	k2eAgInSc4d1	samotný
zločin	zločin	k1gInSc4	zločin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Orwellově	Orwellův	k2eAgMnSc6d1	Orwellův
1984	[number]	k4	1984
jsou	být	k5eAaImIp3nP	být
uváděny	uvádět	k5eAaImNgInP	uvádět
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
praktiky	praktika	k1gFnPc4	praktika
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
směřují	směřovat	k5eAaImIp3nP	směřovat
k	k	k7c3	k
vymýcení	vymýcení	k1gNnSc2	vymýcení
ideozločinu	ideozločina	k1gFnSc4	ideozločina
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
řekla	říct	k5eAaPmAgFnS	říct
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
postav	postava	k1gFnPc2	postava
<g/>
:	:	kIx,	:
brzy	brzy	k6eAd1	brzy
v	v	k7c6	v
Oceánii	Oceánie	k1gFnSc6	Oceánie
nebude	být	k5eNaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ideozločin	ideozločina	k1gFnPc2	ideozločina
spáchat	spáchat	k5eAaPmF	spáchat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Udržovací	udržovací	k2eAgFnSc1d1	udržovací
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
mocnostmi	mocnost	k1gFnPc7	mocnost
v	v	k7c6	v
Orwellově	Orwellův	k2eAgInSc6d1	Orwellův
světě	svět	k1gInSc6	svět
není	být	k5eNaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
vyhrána	vyhrát	k5eAaPmNgFnS	vyhrát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
udržována	udržovat	k5eAaImNgFnS	udržovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
obyvatelé	obyvatel	k1gMnPc1	obyvatel
nebohatli	bohatnout	k5eNaImAgMnP	bohatnout
a	a	k8xC	a
neměli	mít	k5eNaImAgMnP	mít
čas	čas	k1gInSc4	čas
přemýšlet	přemýšlet	k5eAaImF	přemýšlet
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
udržuje	udržovat	k5eAaImIp3nS	udržovat
obyvatele	obyvatel	k1gMnPc4	obyvatel
ve	v	k7c6	v
strachu	strach	k1gInSc6	strach
a	a	k8xC	a
odrazuje	odrazovat	k5eAaImIp3nS	odrazovat
je	on	k3xPp3gNnSc4	on
od	od	k7c2	od
emocí	emoce	k1gFnPc2	emoce
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgMnPc2	jenž
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
těšit	těšit	k5eAaImF	těšit
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
udržuje	udržovat	k5eAaImIp3nS	udržovat
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
mezi	mezi	k7c7	mezi
mocnostmi	mocnost	k1gFnPc7	mocnost
a	a	k8xC	a
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
motorem	motor	k1gInSc7	motor
nejenom	nejenom	k6eAd1	nejenom
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
míru	mír	k1gInSc2	mír
ale	ale	k8xC	ale
i	i	k9	i
Strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Vlády	vláda	k1gFnPc1	vláda
všech	všecek	k3xTgFnPc2	všecek
mocností	mocnost	k1gFnPc2	mocnost
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
toho	ten	k3xDgNnSc2	ten
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
vědomy	vědom	k2eAgFnPc1d1	vědoma
a	a	k8xC	a
záměrně	záměrně	k6eAd1	záměrně
udržují	udržovat	k5eAaImIp3nP	udržovat
status	status	k1gInSc4	status
quo	quo	k?	quo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Adaptace	adaptace	k1gFnSc1	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Filmové	filmový	k2eAgInPc1d1	filmový
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
byla	být	k5eAaImAgFnS	být
natočena	natočen	k2eAgFnSc1d1	natočena
televizní	televizní	k2eAgFnSc1d1	televizní
verze	verze	k1gFnSc1	verze
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
film	film	k1gInSc1	film
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
velmi	velmi	k6eAd1	velmi
věrná	věrný	k2eAgFnSc1d1	věrná
adaptace	adaptace	k1gFnSc1	adaptace
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
natočil	natočit	k5eAaBmAgInS	natočit
Terry	Terra	k1gFnSc2	Terra
Gilliam	Gilliam	k1gInSc1	Gilliam
film	film	k1gInSc1	film
Brazil	Brazil	k1gFnSc2	Brazil
inspirovaný	inspirovaný	k2eAgInSc1d1	inspirovaný
románem	román	k1gInSc7	román
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Film	film	k1gInSc1	film
Equilibrium	Equilibrium	k1gNnSc1	Equilibrium
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
krom	krom	k7c2	krom
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
My	my	k3xPp1nPc1	my
<g/>
,	,	kIx,	,
Konec	konec	k1gInSc1	konec
civilizace	civilizace	k1gFnSc2	civilizace
nebo	nebo	k8xC	nebo
451	[number]	k4	451
stupňů	stupeň	k1gInPc2	stupeň
Fahrenheita	Fahrenheit	k1gMnSc2	Fahrenheit
<g/>
,	,	kIx,	,
inspiruje	inspirovat	k5eAaBmIp3nS	inspirovat
také	také	k9	také
1984	[number]	k4	1984
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
vize	vize	k1gFnSc2	vize
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
umělecká	umělecký	k2eAgNnPc4d1	umělecké
díla	dílo	k1gNnPc4	dílo
likvidována	likvidován	k2eAgNnPc4d1	likvidováno
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
lidech	lid	k1gInPc6	lid
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
emoce	emoce	k1gFnPc1	emoce
<g/>
,	,	kIx,	,
a	a	k8xC	a
ty	ten	k3xDgInPc1	ten
jsou	být	k5eAaImIp3nP	být
zakázané	zakázaný	k2eAgNnSc4d1	zakázané
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgNnPc4d1	ostatní
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
investovala	investovat	k5eAaBmAgFnS	investovat
firma	firma	k1gFnSc1	firma
Apple	Apple	kA	Apple
přes	přes	k7c4	přes
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
dolarů	dolar	k1gInPc2	dolar
na	na	k7c4	na
reklamu	reklama	k1gFnSc4	reklama
a	a	k8xC	a
prostor	prostor	k1gInSc4	prostor
ve	v	k7c6	v
vysílacím	vysílací	k2eAgInSc6d1	vysílací
čase	čas	k1gInSc6	čas
v	v	k7c6	v
přestávce	přestávka	k1gFnSc6	přestávka
v	v	k7c6	v
Superbowlu	Superbowl	k1gInSc6	Superbowl
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
představila	představit	k5eAaPmAgFnS	představit
svůj	svůj	k3xOyFgInSc4	svůj
nový	nový	k2eAgInSc4d1	nový
počítač	počítač	k1gInSc4	počítač
Macintosh	Macintosh	kA	Macintosh
<g/>
.	.	kIx.	.
</s>
<s>
Reklama	reklama	k1gFnSc1	reklama
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
1984	[number]	k4	1984
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
použity	použit	k2eAgFnPc4d1	použita
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgFnPc4d1	podobná
kulisy	kulisa	k1gFnPc4	kulisa
a	a	k8xC	a
rekvizity	rekvizit	k1gInPc4	rekvizit
jako	jako	k9	jako
ve	v	k7c6	v
filmové	filmový	k2eAgFnSc6d1	filmová
adaptaci	adaptace	k1gFnSc6	adaptace
1984	[number]	k4	1984
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
představitelů	představitel	k1gMnPc2	představitel
Apple	Apple	kA	Apple
reklama	reklama	k1gFnSc1	reklama
zafungovala	zafungovat	k5eAaPmAgFnS	zafungovat
a	a	k8xC	a
firma	firma	k1gFnSc1	firma
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
poptávku	poptávka	k1gFnSc4	poptávka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Bowie	Bowie	k1gFnSc2	Bowie
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
albu	album	k1gNnSc6	album
Diamond	Diamond	k1gInSc4	Diamond
Dogs	Dogsa	k1gFnPc2	Dogsa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
zařadil	zařadit	k5eAaPmAgInS	zařadit
svojí	svojit	k5eAaImIp3nS	svojit
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
1984	[number]	k4	1984
<g/>
"	"	kIx"	"
s	s	k7c7	s
motivem	motiv	k1gInSc7	motiv
děje	děj	k1gInSc2	děj
Orwellova	Orwellův	k2eAgInSc2d1	Orwellův
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Americká	americký	k2eAgFnSc1d1	americká
hardcore-punková	hardcoreunkový	k2eAgFnSc1d1	hardcore-punkový
legenda	legenda	k1gFnSc1	legenda
Dead	Dead	k1gInSc4	Dead
Kennedys	Kennedys	k1gInSc1	Kennedys
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
román	román	k1gInSc4	román
v	v	k7c6	v
textu	text	k1gInSc6	text
ke	k	k7c3	k
skladbě	skladba	k1gFnSc3	skladba
"	"	kIx"	"
<g/>
We	We	k1gFnSc1	We
<g/>
'	'	kIx"	'
<g/>
ve	v	k7c6	v
Got	Got	k1gFnSc6	Got
a	a	k8xC	a
Bigger	Bigger	k1gInSc4	Bigger
Problem	Probl	k1gMnSc7	Probl
Now	Now	k1gMnSc7	Now
<g/>
"	"	kIx"	"
na	na	k7c6	na
desce	deska	k1gFnSc6	deska
In	In	k1gMnSc2	In
God	God	k1gMnSc2	God
We	We	k1gMnSc2	We
Trust	trust	k1gInSc4	trust
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc4	Inc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Britská	britský	k2eAgFnSc1d1	britská
grindcorová	grindcorový	k2eAgFnSc1d1	grindcorová
legenda	legenda	k1gFnSc1	legenda
Carcass	Carcassa	k1gFnPc2	Carcassa
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
román	román	k1gInSc4	román
v	v	k7c6	v
textu	text	k1gInSc6	text
skladby	skladba	k1gFnSc2	skladba
"	"	kIx"	"
<g/>
Room	Room	k1gInSc1	Room
101	[number]	k4	101
<g/>
"	"	kIx"	"
na	na	k7c6	na
desce	deska	k1gFnSc6	deska
Swansong	Swansong	k1gInSc4	Swansong
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Americká	americký	k2eAgFnSc1d1	americká
punk	punk	k1gInSc1	punk
rocková	rockový	k2eAgFnSc1d1	rocková
skupina	skupina	k1gFnSc1	skupina
Anti-Flag	Anti-Flaga	k1gFnPc2	Anti-Flaga
přímo	přímo	k6eAd1	přímo
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
knihu	kniha	k1gFnSc4	kniha
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
"	"	kIx"	"
<g/>
Welcome	Welcom	k1gInSc5	Welcom
to	ten	k3xDgNnSc1	ten
1984	[number]	k4	1984
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Britská	britský	k2eAgFnSc1d1	britská
skupina	skupina	k1gFnSc1	skupina
Muse	Musa	k1gFnSc6	Musa
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
album	album	k1gNnSc4	album
The	The	k1gFnSc2	The
Resistance	Resistanec	k1gInPc1	Resistanec
inspirované	inspirovaný	k2eAgInPc1d1	inspirovaný
právě	právě	k9	právě
dílem	dílo	k1gNnSc7	dílo
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Americká	americký	k2eAgFnSc1d1	americká
skupina	skupina	k1gFnSc1	skupina
Rage	Rage	k1gFnSc1	Rage
Against	Against	k1gFnSc1	Against
the	the	k?	the
Machine	Machin	k1gInSc5	Machin
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
album	album	k1gNnSc4	album
The	The	k1gFnSc2	The
Battle	Battle	k1gFnSc2	Battle
of	of	k?	of
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
cítit	cítit	k5eAaImF	cítit
výrazná	výrazný	k2eAgFnSc1d1	výrazná
inspirace	inspirace	k1gFnSc1	inspirace
novelou	novela	k1gFnSc7	novela
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
písních	píseň	k1gFnPc6	píseň
"	"	kIx"	"
<g/>
Testify	Testif	k1gInPc4	Testif
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Sleep	Sleep	k1gInSc1	Sleep
Now	Now	k1gFnSc2	Now
in	in	k?	in
the	the	k?	the
Fire	Fire	k1gInSc1	Fire
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Voice	Voice	k1gFnSc1	Voice
of	of	k?	of
the	the	k?	the
Voiceless	Voiceless	k1gInSc1	Voiceless
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
doslovné	doslovný	k2eAgFnPc1d1	doslovná
citace	citace	k1gFnPc1	citace
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Britská	britský	k2eAgFnSc1d1	britská
skupina	skupina	k1gFnSc1	skupina
Radiohead	Radiohead	k1gInSc1	Radiohead
se	se	k3xPyFc4	se
dílem	dílem	k6eAd1	dílem
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
u	u	k7c2	u
skladby	skladba	k1gFnSc2	skladba
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
=	=	kIx~	=
<g/>
5	[number]	k4	5
z	z	k7c2	z
alba	album	k1gNnSc2	album
Hail	Haila	k1gFnPc2	Haila
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Thief	Thief	k1gInSc1	Thief
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Britská	britský	k2eAgFnSc1d1	britská
skupina	skupina	k1gFnSc1	skupina
Kitchens	Kitchens	k1gInSc1	Kitchens
of	of	k?	of
Distinction	Distinction	k1gInSc1	Distinction
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
doublethink	doublethink	k1gInSc4	doublethink
v	v	k7c6	v
textu	text	k1gInSc6	text
ke	k	k7c3	k
skladbě	skladba	k1gFnSc3	skladba
I	i	k8xC	i
Wish	Wisha	k1gFnPc2	Wisha
it	it	k?	it
Would	Would	k1gMnSc1	Would
Snow	Snow	k1gMnSc1	Snow
na	na	k7c6	na
desce	deska	k1gFnSc6	deska
Folly	Folla	k1gFnSc2	Folla
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Britský	britský	k2eAgMnSc1d1	britský
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
klávesista	klávesista	k1gMnSc1	klávesista
Rick	Rick	k1gMnSc1	Rick
Wakeman	Wakeman	k1gMnSc1	Wakeman
vydal	vydat	k5eAaPmAgMnS	vydat
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
sólové	sólový	k2eAgNnSc4d1	sólové
album	album	k1gNnSc4	album
"	"	kIx"	"
<g/>
1984	[number]	k4	1984
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
zhudebněním	zhudebnění	k1gNnSc7	zhudebnění
tohoto	tento	k3xDgInSc2	tento
Orwellova	Orwellův	k2eAgInSc2d1	Orwellův
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
skupina	skupina	k1gFnSc1	skupina
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
videoklip	videoklip	k1gInSc4	videoklip
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
Orwellovým	Orwellův	k2eAgInSc7d1	Orwellův
románem	román	k1gInSc7	román
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
volnočasový	volnočasový	k2eAgInSc1d1	volnočasový
spolek	spolek	k1gInSc1	spolek
StopTime	StopTim	k1gInSc5	StopTim
již	již	k6eAd1	již
pátým	pátý	k4xOgInSc7	pátý
rokem	rok	k1gInSc7	rok
pořádá	pořádat	k5eAaImIp3nS	pořádat
městskou	městský	k2eAgFnSc4d1	městská
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
tohoto	tento	k3xDgInSc2	tento
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
informací	informace	k1gFnPc2	informace
na	na	k7c4	na
velky-bratr	velkyratr	k1gInSc4	velky-bratr
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
100	[number]	k4	100
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
knih	kniha	k1gFnPc2	kniha
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
podle	podle	k7c2	podle
Le	Le	k1gFnSc2	Le
Monde	mond	k1gInSc5	mond
</s>
</p>
<p>
<s>
Diktatura	diktatura	k1gFnSc1	diktatura
</s>
</p>
<p>
<s>
Newspeak	Newspeak	k6eAd1	Newspeak
</s>
</p>
<p>
<s>
George	Georg	k1gMnSc2	Georg
Orwell	Orwell	k1gMnSc1	Orwell
</s>
</p>
<p>
<s>
Totalita	totalita	k1gFnSc1	totalita
</s>
</p>
<p>
<s>
Velký	velký	k2eAgMnSc1d1	velký
bratr	bratr	k1gMnSc1	bratr
</s>
</p>
<p>
<s>
Vymývání	vymývání	k1gNnSc1	vymývání
mozku	mozek	k1gInSc2	mozek
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
1984	[number]	k4	1984
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
text	text	k1gInSc1	text
díla	dílo	k1gNnSc2	dílo
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Anglický	anglický	k2eAgInSc1d1	anglický
text	text	k1gInSc1	text
díla	dílo	k1gNnSc2	dílo
</s>
</p>
