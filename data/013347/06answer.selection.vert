<s>
1984	[number]	k4	1984
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
vypsáno	vypsat	k5eAaPmNgNnS	vypsat
slovy	slovo	k1gNnPc7	slovo
Nineteen	Nineteen	k2eAgMnSc1d1	Nineteen
Eighty-Four	Eighty-Four	k1gMnSc1	Eighty-Four
<g/>
,	,	kIx,	,
pracovním	pracovní	k2eAgInSc7d1	pracovní
názvem	název	k1gInSc7	název
Poslední	poslední	k2eAgMnSc1d1	poslední
člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
antiutopický	antiutopický	k2eAgMnSc1d1	antiutopický
(	(	kIx(	(
<g/>
dystopický	dystopický	k2eAgInSc1d1	dystopický
<g/>
)	)	kIx)	)
román	román	k1gInSc1	román
anglického	anglický	k2eAgMnSc2d1	anglický
spisovatele	spisovatel	k1gMnSc2	spisovatel
George	Georg	k1gMnSc2	Georg
Orwella	Orwell	k1gMnSc2	Orwell
dokončený	dokončený	k2eAgInSc4d1	dokončený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
publikován	publikován	k2eAgMnSc1d1	publikován
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Secker	Secker	k1gInSc1	Secker
and	and	k?	and
Warburg	Warburg	k1gInSc1	Warburg
<g/>
.	.	kIx.	.
</s>
