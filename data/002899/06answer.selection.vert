<s>
Back	Back	k1gMnSc1	Back
in	in	k?	in
Black	Black	k1gMnSc1	Black
je	být	k5eAaImIp3nS	být
tudíž	tudíž	k8xC	tudíž
druhým	druhý	k4xOgNnSc7	druhý
celosvětově	celosvětově	k6eAd1	celosvětově
nejprodávanějším	prodávaný	k2eAgNnSc7d3	nejprodávanější
albem	album	k1gNnSc7	album
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
úspěšnější	úspěšný	k2eAgFnSc2d2	úspěšnější
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
album	album	k1gNnSc1	album
Michaela	Michael	k1gMnSc2	Michael
Jacksona	Jackson	k1gMnSc2	Jackson
–	–	k?	–
Thriller	thriller	k1gInSc1	thriller
<g/>
,	,	kIx,	,
kterého	který	k3yRgNnSc2	který
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
kopií	kopie	k1gFnPc2	kopie
Kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
čtvrtá	čtvrtá	k1gFnSc1	čtvrtá
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
100	[number]	k4	100
Greatest	Greatest	k1gInSc1	Greatest
Artists	Artists	k1gInSc4	Artists
of	of	k?	of
Hard	Hard	k1gInSc1	Hard
Rock	rock	k1gInSc1	rock
televize	televize	k1gFnSc2	televize
VH1	VH1	k1gFnSc2	VH1
a	a	k8xC	a
televizí	televize	k1gFnSc7	televize
MTV	MTV	kA	MTV
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
"	"	kIx"	"
<g/>
sedmou	sedmý	k4xOgFnSc4	sedmý
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
heavy	heav	k1gInPc4	heav
metalovou	metalový	k2eAgFnSc7d1	metalová
kapelou	kapela	k1gFnSc7	kapela
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
