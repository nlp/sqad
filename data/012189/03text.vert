<p>
<s>
Gisela	Gisela	k1gFnSc1	Gisela
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Gisè	Gisè	k1gMnSc2	Gisè
de	de	k?	de
France	Franc	k1gMnSc2	Franc
<g/>
;	;	kIx,	;
969	[number]	k4	969
<g/>
?	?	kIx.	?
</s>
<s>
–	–	k?	–
1002	[number]	k4	1002
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
hraběnka	hraběnka	k1gFnSc1	hraběnka
z	z	k7c2	z
Ponthieu	Ponthieus	k1gInSc2	Ponthieus
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Kapetovců	Kapetovec	k1gInPc2	Kapetovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodila	narodit	k5eAaPmAgFnS	narodit
se	se	k3xPyFc4	se
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
969	[number]	k4	969
jako	jako	k8xS	jako
dcera	dcera	k1gFnSc1	dcera
krále	král	k1gMnSc2	král
Huga	Hugo	k1gMnSc2	Hugo
Kapeta	Kapet	k1gMnSc2	Kapet
a	a	k8xC	a
Adély	Adéla	k1gFnSc2	Adéla
<g/>
,	,	kIx,	,
dcery	dcera	k1gFnSc2	dcera
akvitánského	akvitánský	k2eAgMnSc2d1	akvitánský
vévody	vévoda	k1gMnSc2	vévoda
Viléma	Vilém	k1gMnSc2	Vilém
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
provdána	provdat	k5eAaPmNgFnS	provdat
za	za	k7c4	za
hraběte	hrabě	k1gMnSc4	hrabě
Huga	Hugo	k1gMnSc4	Hugo
I.	I.	kA	I.
z	z	k7c2	z
Ponthieu	Ponthieus	k1gInSc2	Ponthieus
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k4xCyI	pár
měl	mít	k5eAaImAgMnS	mít
minimálně	minimálně	k6eAd1	minimálně
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
Enguerrand	Enguerrand	k1gInSc1	Enguerrand
zdědil	zdědit	k5eAaPmAgInS	zdědit
hraběcí	hraběcí	k2eAgInSc1d1	hraběcí
titul	titul	k1gInSc1	titul
a	a	k8xC	a
Vít	Vít	k1gMnSc1	Vít
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
církevní	církevní	k2eAgFnSc3d1	církevní
kariéře	kariéra	k1gFnSc3	kariéra
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
opatem	opat	k1gMnSc7	opat
v	v	k7c4	v
Saint-Riquier	Saint-Riquier	k1gInSc4	Saint-Riquier
<g/>
.	.	kIx.	.
</s>
</p>
