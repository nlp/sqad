<s>
Milán	Milán	k1gInSc1	Milán
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Milano	Milana	k1gFnSc5	Milana
<g/>
,	,	kIx,	,
v	v	k7c6	v
milánském	milánský	k2eAgInSc6d1	milánský
dialektu	dialekt	k1gInSc6	dialekt
Milan	Milan	k1gMnSc1	Milan
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
italské	italský	k2eAgNnSc4d1	italské
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
oblasti	oblast	k1gFnSc2	oblast
Lombardie	Lombardie	k1gFnSc2	Lombardie
a	a	k8xC	a
provincie	provincie	k1gFnSc2	provincie
Milano	Milana	k1gFnSc5	Milana
<g/>
.	.	kIx.	.
</s>
