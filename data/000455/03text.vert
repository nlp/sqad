<s>
Milán	Milán	k1gInSc1	Milán
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Milano	Milana	k1gFnSc5	Milana
<g/>
,	,	kIx,	,
v	v	k7c6	v
milánském	milánský	k2eAgInSc6d1	milánský
dialektu	dialekt	k1gInSc6	dialekt
Milan	Milan	k1gMnSc1	Milan
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
italské	italský	k2eAgNnSc4d1	italské
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
oblasti	oblast	k1gFnSc2	oblast
Lombardie	Lombardie	k1gFnSc2	Lombardie
a	a	k8xC	a
provincie	provincie	k1gFnSc2	provincie
Milano	Milana	k1gFnSc5	Milana
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
slavné	slavný	k2eAgNnSc1d1	slavné
jako	jako	k8xS	jako
město	město	k1gNnSc1	město
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
módy	móda	k1gFnSc2	móda
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
sídlo	sídlo	k1gNnSc1	sídlo
opery	opera	k1gFnSc2	opera
La	la	k1gNnSc2	la
Scala	scát	k5eAaImAgFnS	scát
a	a	k8xC	a
fotbalových	fotbalový	k2eAgInPc2d1	fotbalový
klubů	klub	k1gInPc2	klub
AC	AC	kA	AC
a	a	k8xC	a
Inter	Inter	k1gMnSc1	Inter
Milán	Milán	k1gInSc1	Milán
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
také	také	k9	také
velká	velký	k2eAgFnSc1d1	velká
gotická	gotický	k2eAgFnSc1d1	gotická
katedrála	katedrála	k1gFnSc1	katedrála
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
nížině	nížina	k1gFnSc6	nížina
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Lombardie	Lombardie	k1gFnSc2	Lombardie
a	a	k8xC	a
protékají	protékat	k5eAaImIp3nP	protékat
jím	on	k3xPp3gNnSc7	on
jenom	jenom	k6eAd1	jenom
malé	malý	k2eAgFnPc1d1	malá
řeky	řeka	k1gFnPc1	řeka
(	(	kIx(	(
<g/>
Lambro	Lambro	k1gNnSc1	Lambro
<g/>
,	,	kIx,	,
Olona	Olona	k1gFnSc1	Olona
<g/>
,	,	kIx,	,
Seveso	Sevesa	k1gFnSc5	Sevesa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Samo	sám	k3xTgNnSc1	sám
město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
1,3	[number]	k4	1,3
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
s	s	k7c7	s
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
a	a	k8xC	a
souvislou	souvislý	k2eAgFnSc7d1	souvislá
okolní	okolní	k2eAgFnSc7d1	okolní
aglomerací	aglomerace	k1gFnSc7	aglomerace
až	až	k6eAd1	až
4	[number]	k4	4
miliony	milion	k4xCgInPc4	milion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
velkých	velký	k2eAgFnPc2d1	velká
bank	banka	k1gFnPc2	banka
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
sídla	sídlo	k1gNnSc2	sídlo
velkých	velký	k2eAgFnPc2d1	velká
firem	firma	k1gFnPc2	firma
(	(	kIx(	(
<g/>
Pirelli	Pirelle	k1gFnSc4	Pirelle
<g/>
,	,	kIx,	,
Alfa	alfa	k1gFnSc1	alfa
Romeo	Romeo	k1gMnSc1	Romeo
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
konají	konat	k5eAaImIp3nP	konat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
pravidelné	pravidelný	k2eAgInPc1d1	pravidelný
veletrhy	veletrh	k1gInPc1	veletrh
<g/>
,	,	kIx,	,
např.	např.	kA	např.
FieraMilano	FieraMilana	k1gFnSc5	FieraMilana
<g/>
.	.	kIx.	.
</s>
<s>
Milán	Milán	k1gInSc1	Milán
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc1d1	významný
železniční	železniční	k2eAgInSc1d1	železniční
a	a	k8xC	a
dálniční	dálniční	k2eAgInSc1d1	dálniční
uzel	uzel	k1gInSc1	uzel
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgNnPc4	dva
mezinárodní	mezinárodní	k2eAgNnPc4d1	mezinárodní
letiště	letiště	k1gNnPc4	letiště
a	a	k8xC	a
blízko	blízko	k1gNnSc4	blízko
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
závodní	závodní	k2eAgInSc1d1	závodní
okruh	okruh	k1gInSc1	okruh
Monza	Monza	k1gFnSc1	Monza
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nS	sídlet
12	[number]	k4	12
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
slavné	slavný	k2eAgFnPc1d1	slavná
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
umělecké	umělecký	k2eAgFnPc1d1	umělecká
školy	škola	k1gFnPc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
tepna	tepna	k1gFnSc1	tepna
Itálie	Itálie	k1gFnSc1	Itálie
-	-	kIx~	-
burza	burza	k1gFnSc1	burza
Mibtel	Mibtela	k1gFnPc2	Mibtela
<g/>
.	.	kIx.	.
</s>
<s>
Milán	Milán	k1gInSc1	Milán
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejbohatší	bohatý	k2eAgNnPc4d3	nejbohatší
a	a	k8xC	a
nejrozvinutější	rozvinutý	k2eAgNnPc4d3	nejrozvinutější
města	město	k1gNnPc4	město
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Latinský	latinský	k2eAgInSc1d1	latinský
název	název	k1gInSc1	název
města	město	k1gNnSc2	město
Mediolanum	Mediolanum	k1gInSc1	Mediolanum
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
název	název	k1gInSc1	název
Milano	Milana	k1gFnSc5	Milana
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
keltského	keltský	k2eAgInSc2d1	keltský
původu	původ	k1gInSc2	původ
a	a	k8xC	a
znamenal	znamenat	k5eAaImAgInS	znamenat
"	"	kIx"	"
<g/>
město	město	k1gNnSc1	město
uprostřed	uprostřed	k7c2	uprostřed
roviny	rovina	k1gFnSc2	rovina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pátém	pátý	k4xOgNnSc6	pátý
století	století	k1gNnSc6	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
sídliště	sídliště	k1gNnSc4	sídliště
keltských	keltský	k2eAgInPc2d1	keltský
Insubrů	Insubr	k1gInPc2	Insubr
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
roku	rok	k1gInSc2	rok
222	[number]	k4	222
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Římané	Říman	k1gMnPc1	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
293	[number]	k4	293
za	za	k7c2	za
císaře	císař	k1gMnSc2	císař
Diokleciána	Dioklecián	k1gMnSc2	Dioklecián
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
západní	západní	k2eAgFnSc2d1	západní
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
roku	rok	k1gInSc2	rok
313	[number]	k4	313
zde	zde	k6eAd1	zde
císař	císař	k1gMnSc1	císař
Konstantin	Konstantin	k1gMnSc1	Konstantin
I.	I.	kA	I.
Ediktem	edikt	k1gInSc7	edikt
milánským	milánský	k2eAgFnPc3d1	Milánská
povolil	povolit	k5eAaPmAgInS	povolit
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
říši	říš	k1gFnSc6	říš
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
402	[number]	k4	402
město	město	k1gNnSc4	město
obléhali	obléhat	k5eAaImAgMnP	obléhat
Visigóti	Visigót	k1gMnPc1	Visigót
a	a	k8xC	a
císařská	císařský	k2eAgFnSc1d1	císařská
residence	residence	k1gFnSc1	residence
se	se	k3xPyFc4	se
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Ravenny	Ravenna	k1gFnSc2	Ravenna
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
několikrát	několikrát	k6eAd1	několikrát
dobyto	dobyt	k2eAgNnSc1d1	dobyto
a	a	k8xC	a
poničeno	poničen	k2eAgNnSc1d1	poničeno
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
539	[number]	k4	539
Langobardy	Langobarda	k1gFnSc2	Langobarda
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
název	název	k1gInSc1	název
provincie	provincie	k1gFnSc2	provincie
Lombardsko	Lombardsko	k?	Lombardsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
roku	rok	k1gInSc2	rok
774	[number]	k4	774
je	být	k5eAaImIp3nS	být
dobyl	dobýt	k5eAaPmAgMnS	dobýt
Karel	Karel	k1gMnSc1	Karel
Veliký	veliký	k2eAgMnSc1d1	veliký
a	a	k8xC	a
přičlenil	přičlenit	k5eAaPmAgInS	přičlenit
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1162	[number]	k4	1162
je	být	k5eAaImIp3nS	být
dobyl	dobýt	k5eAaPmAgMnS	dobýt
a	a	k8xC	a
vyplenil	vyplenit	k5eAaPmAgMnS	vyplenit
císař	císař	k1gMnSc1	císař
Fridrich	Fridrich	k1gMnSc1	Fridrich
I.	I.	kA	I.
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
českého	český	k2eAgNnSc2d1	české
vojska	vojsko	k1gNnSc2	vojsko
knížete	kníže	k1gMnSc2	kníže
Vladislava	Vladislav	k1gMnSc2	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
dostal	dostat	k5eAaPmAgInS	dostat
královský	královský	k2eAgInSc1d1	královský
titul	titul	k1gInSc1	titul
<g/>
.	.	kIx.	.
</s>
<s>
Dobývání	dobývání	k1gNnSc1	dobývání
Milána	Milán	k1gInSc2	Milán
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
čeští	český	k2eAgMnPc1d1	český
válečníci	válečník	k1gMnPc1	válečník
vyznamenali	vyznamenat	k5eAaPmAgMnP	vyznamenat
odvahou	odvaha	k1gFnSc7	odvaha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
krutostí	krutost	k1gFnSc7	krutost
<g/>
,	,	kIx,	,
líčí	líčit	k5eAaImIp3nS	líčit
Dalimilova	Dalimilův	k2eAgFnSc1d1	Dalimilova
kronika	kronika	k1gFnSc1	kronika
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
obnovené	obnovený	k2eAgNnSc1d1	obnovené
město	město	k1gNnSc1	město
stálo	stát	k5eAaImAgNnS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Lombardské	lombardský	k2eAgFnSc2d1	Lombardská
ligy	liga	k1gFnSc2	liga
měst	město	k1gNnPc2	město
(	(	kIx(	(
<g/>
1167	[number]	k4	1167
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1184	[number]	k4	1184
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
vévodstvím	vévodství	k1gNnSc7	vévodství
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgNnSc3	jenž
vládly	vládnout	k5eAaImAgFnP	vládnout
rody	rod	k1gInPc4	rod
Visconti	Visconť	k1gFnSc2	Visconť
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
Sforza	Sforza	k1gFnSc1	Sforza
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
Milán	Milán	k1gInSc1	Milán
stal	stát	k5eAaPmAgInS	stát
jedním	jeden	k4xCgNnSc7	jeden
ze	z	k7c2	z
středisek	středisko	k1gNnPc2	středisko
italské	italský	k2eAgFnSc2d1	italská
renesance	renesance	k1gFnSc2	renesance
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
signorií	signoria	k1gFnSc7	signoria
a	a	k8xC	a
nejmocnějším	mocný	k2eAgInSc7d3	nejmocnější
a	a	k8xC	a
nejucelenějším	ucelený	k2eAgInSc7d3	nejucelenější
státem	stát	k1gInSc7	stát
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1515	[number]	k4	1515
dobyl	dobýt	k5eAaPmAgMnS	dobýt
město	město	k1gNnSc4	město
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
František	František	k1gMnSc1	František
I.	I.	kA	I.
<g/>
,	,	kIx,	,
už	už	k9	už
roku	rok	k1gInSc2	rok
1525	[number]	k4	1525
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
dobyl	dobýt	k5eAaPmAgMnS	dobýt
císař	císař	k1gMnSc1	císař
Karel	Karel	k1gMnSc1	Karel
V.	V.	kA	V.
a	a	k8xC	a
město	město	k1gNnSc1	město
přešlo	přejít	k5eAaPmAgNnS	přejít
do	do	k7c2	do
držení	držení	k1gNnSc2	držení
španělských	španělský	k2eAgInPc2d1	španělský
Habsburků	Habsburk	k1gInPc2	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Milán	Milán	k1gInSc1	Milán
stal	stát	k5eAaPmAgInS	stát
střediskem	středisko	k1gNnSc7	středisko
nové	nový	k2eAgFnSc2d1	nová
opery	opera	k1gFnSc2	opera
<g/>
:	:	kIx,	:
tři	tři	k4xCgFnPc1	tři
opery	opera	k1gFnPc1	opera
zde	zde	k6eAd1	zde
napsal	napsat	k5eAaPmAgMnS	napsat
Mozart	Mozart	k1gMnSc1	Mozart
a	a	k8xC	a
divadlo	divadlo	k1gNnSc1	divadlo
La	la	k1gNnSc2	la
Scala	scát	k5eAaImAgFnS	scát
později	pozdě	k6eAd2	pozdě
proslavily	proslavit	k5eAaPmAgFnP	proslavit
premiéry	premiéra	k1gFnPc1	premiéra
děl	dělo	k1gNnPc2	dělo
Belliniho	Bellini	k1gMnSc2	Bellini
<g/>
,	,	kIx,	,
Donizettiho	Donizetti	k1gMnSc2	Donizetti
a	a	k8xC	a
Verdiho	Verdi	k1gMnSc2	Verdi
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1796	[number]	k4	1796
dobyl	dobýt	k5eAaPmAgMnS	dobýt
město	město	k1gNnSc4	město
Napoleon	napoleon	k1gInSc1	napoleon
I.	I.	kA	I.
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
Milán	Milán	k1gInSc1	Milán
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
za	za	k7c4	za
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Italského	italský	k2eAgNnSc2d1	italské
království	království	k1gNnSc2	království
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
se	se	k3xPyFc4	se
ve	v	k7c6	v
zdejším	zdejší	k2eAgInSc6d1	zdejší
dómu	dóm	k1gInSc6	dóm
korunovat	korunovat	k5eAaBmF	korunovat
italským	italský	k2eAgMnSc7d1	italský
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Napoleonově	Napoleonův	k2eAgInSc6d1	Napoleonův
pádu	pád	k1gInSc6	pád
vrátil	vrátit	k5eAaPmAgInS	vrátit
Vídeňský	vídeňský	k2eAgInSc1d1	vídeňský
kongres	kongres	k1gInSc1	kongres
1815	[number]	k4	1815
Lombardsko	Lombardsko	k?	Lombardsko
Rakousku	Rakousko	k1gNnSc6	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Protirakouské	protirakouský	k2eAgNnSc1d1	protirakouské
povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
sice	sice	k8xC	sice
maršál	maršál	k1gMnSc1	maršál
Radecký	Radecký	k1gMnSc1	Radecký
po	po	k7c6	po
čase	čas	k1gInSc6	čas
potlačil	potlačit	k5eAaPmAgMnS	potlačit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
u	u	k7c2	u
Solferina	Solferino	k1gNnSc2	Solferino
1859	[number]	k4	1859
se	se	k3xPyFc4	se
Rakousko	Rakousko	k1gNnSc1	Rakousko
muselo	muset	k5eAaImAgNnS	muset
Lombardie	Lombardie	k1gFnPc4	Lombardie
vzdát	vzdát	k5eAaPmF	vzdát
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
sjednocující	sjednocující	k2eAgFnSc2d1	sjednocující
se	se	k3xPyFc4	se
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
sjednocování	sjednocování	k1gNnSc6	sjednocování
hrál	hrát	k5eAaImAgInS	hrát
Milán	Milán	k1gInSc1	Milán
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
úlohu	úloha	k1gFnSc4	úloha
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
střediskem	středisko	k1gNnSc7	středisko
italského	italský	k2eAgInSc2d1	italský
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
železnic	železnice	k1gFnPc2	železnice
i	i	k8xC	i
financí	finance	k1gFnPc2	finance
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
zde	zde	k6eAd1	zde
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
založil	založit	k5eAaPmAgMnS	založit
své	svůj	k3xOyFgNnSc4	svůj
fašistické	fašistický	k2eAgNnSc4d1	fašistické
hnutí	hnutí	k1gNnSc4	hnutí
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
zahájil	zahájit	k5eAaPmAgInS	zahájit
1922	[number]	k4	1922
svůj	svůj	k3xOyFgInSc4	svůj
pochod	pochod	k1gInSc4	pochod
na	na	k7c4	na
Řím	Řím	k1gInSc4	Řím
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Milána	Milán	k1gInSc2	Milán
byl	být	k5eAaImAgInS	být
také	také	k9	také
1945	[number]	k4	1945
zajat	zajat	k2eAgInSc4d1	zajat
a	a	k8xC	a
popraven	popraven	k2eAgInSc4d1	popraven
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poválečných	poválečný	k2eAgNnPc6d1	poválečné
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
do	do	k7c2	do
Milána	Milán	k1gInSc2	Milán
stěhovaly	stěhovat	k5eAaImAgInP	stěhovat
tisíce	tisíc	k4xCgInPc1	tisíc
lidí	člověk	k1gMnPc2	člověk
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
vyvrcholil	vyvrcholit	k5eAaPmAgInS	vyvrcholit
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
sice	sice	k8xC	sice
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
stěhovali	stěhovat	k5eAaImAgMnP	stěhovat
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
ale	ale	k8xC	ale
opět	opět	k6eAd1	opět
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Dóm	dóm	k1gInSc1	dóm
(	(	kIx(	(
<g/>
Katedrála	katedrála	k1gFnSc1	katedrála
Narození	narození	k1gNnSc2	narození
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
)	)	kIx)	)
postavený	postavený	k2eAgMnSc1d1	postavený
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1386	[number]	k4	1386
<g/>
-	-	kIx~	-
<g/>
1510	[number]	k4	1510
<g/>
,	,	kIx,	,
s	s	k7c7	s
průčelím	průčelí	k1gNnSc7	průčelí
přestavěným	přestavěný	k2eAgNnSc7d1	přestavěné
za	za	k7c4	za
Napoleona	Napoleon	k1gMnSc4	Napoleon
1805	[number]	k4	1805
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
svými	svůj	k3xOyFgInPc7	svůj
rozměry	rozměr	k1gInPc7	rozměr
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
157	[number]	k4	157
m	m	kA	m
<g/>
,	,	kIx,	,
šířka	šířka	k1gFnSc1	šířka
92	[number]	k4	92
m	m	kA	m
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
klenby	klenba	k1gFnSc2	klenba
45	[number]	k4	45
m	m	kA	m
a	a	k8xC	a
výška	výška	k1gFnSc1	výška
centrální	centrální	k2eAgFnSc2d1	centrální
věže	věž	k1gFnSc2	věž
s	s	k7c7	s
pozlacenou	pozlacený	k2eAgFnSc7d1	pozlacená
sochou	socha	k1gFnSc7	socha
Madony	Madona	k1gFnSc2	Madona
106	[number]	k4	106
m	m	kA	m
<g/>
)	)	kIx)	)
třetí	třetí	k4xOgFnSc1	třetí
největší	veliký	k2eAgFnSc1d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
pojmout	pojmout	k5eAaPmF	pojmout
až	až	k9	až
40	[number]	k4	40
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
je	být	k5eAaImIp3nS	být
bohatá	bohatý	k2eAgFnSc1d1	bohatá
výzdoba	výzdoba	k1gFnSc1	výzdoba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
renesanční	renesanční	k2eAgMnSc1d1	renesanční
a	a	k8xC	a
barokní	barokní	k2eAgMnSc1d1	barokní
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
Santa	Sant	k1gMnSc2	Sant
Maria	Mario	k1gMnSc2	Mario
delle	dell	k1gMnSc2	dell
Grazie	Grazie	k1gFnSc1	Grazie
s	s	k7c7	s
Leonardovou	Leonardův	k2eAgFnSc7d1	Leonardova
freskou	freska	k1gFnSc7	freska
Poslední	poslední	k2eAgFnPc1d1	poslední
večeře	večeře	k1gFnPc1	večeře
(	(	kIx(	(
<g/>
1405	[number]	k4	1405
<g/>
-	-	kIx~	-
<g/>
1498	[number]	k4	1498
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Románská	románský	k2eAgFnSc1d1	románská
bazilika	bazilika	k1gFnSc1	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Ambrože	Ambrož	k1gMnSc2	Ambrož
(	(	kIx(	(
<g/>
Sant	Sant	k1gMnSc1	Sant
<g/>
'	'	kIx"	'
<g/>
Ambroggio	Ambroggio	k1gMnSc1	Ambroggio
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1100	[number]	k4	1100
<g/>
.	.	kIx.	.
</s>
<s>
Antická	antický	k2eAgFnSc1d1	antická
bazilika	bazilika	k1gFnSc1	bazilika
svatého	svatý	k2eAgMnSc2d1	svatý
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
(	(	kIx(	(
<g/>
San	San	k1gMnSc2	San
Lorenzo	Lorenza	k1gFnSc5	Lorenza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
500	[number]	k4	500
<g/>
,	,	kIx,	,
přestavěná	přestavěný	k2eAgFnSc1d1	přestavěná
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
San	San	k?	San
Simpliciano	Simpliciana	k1gFnSc5	Simpliciana
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
chrámů	chrám	k1gInPc2	chrám
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vystavěn	vystavět	k5eAaPmNgInS	vystavět
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1140	[number]	k4	1140
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chóru	chór	k1gInSc6	chór
jsou	být	k5eAaImIp3nP	být
fresky	freska	k1gFnPc4	freska
z	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
st.	st.	kA	st.
Renesanční	renesanční	k2eAgInSc1d1	renesanční
hrad	hrad	k1gInSc1	hrad
rodiny	rodina	k1gFnSc2	rodina
Sforzů	Sforz	k1gMnPc2	Sforz
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
st.	st.	kA	st.
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k8xS	jako
rezidence	rezidence	k1gFnSc1	rezidence
Viscontiů	Visconti	k1gMnPc2	Visconti
a	a	k8xC	a
Sforzů	Sforz	k1gMnPc2	Sforz
a	a	k8xC	a
jako	jako	k9	jako
pevnost	pevnost	k1gFnSc1	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
zde	zde	k6eAd1	zde
najdeme	najít	k5eAaPmIp1nP	najít
sochařské	sochařský	k2eAgNnSc4d1	sochařské
muzeum	muzeum	k1gNnSc4	muzeum
<g/>
,	,	kIx,	,
obrazárnu	obrazárna	k1gFnSc4	obrazárna
a	a	k8xC	a
archeologickou	archeologický	k2eAgFnSc4d1	archeologická
sbírku	sbírka	k1gFnSc4	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
La	la	k1gNnSc2	la
Scala	scát	k5eAaImAgFnS	scát
<g/>
,	,	kIx,	,
vystavěné	vystavěný	k2eAgNnSc4d1	vystavěné
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1776	[number]	k4	1776
<g/>
-	-	kIx~	-
<g/>
1778	[number]	k4	1778
za	za	k7c2	za
císařovny	císařovna	k1gFnSc2	císařovna
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
dnes	dnes	k6eAd1	dnes
přes	přes	k7c4	přes
2800	[number]	k4	2800
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejslavnějších	slavný	k2eAgFnPc2d3	nejslavnější
operních	operní	k2eAgFnPc2d1	operní
scén	scéna	k1gFnPc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Galerie	galerie	k1gFnSc1	galerie
Viktora	Viktor	k1gMnSc2	Viktor
Emanuela	Emanuel	k1gMnSc2	Emanuel
II	II	kA	II
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
zastřešená	zastřešený	k2eAgFnSc1d1	zastřešená
pasáž	pasáž	k1gFnSc1	pasáž
složená	složený	k2eAgFnSc1d1	složená
ze	z	k7c2	z
dvou	dva	k4xCgMnPc2	dva
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
kolmých	kolmý	k2eAgFnPc2d1	kolmá
arkád	arkáda	k1gFnPc2	arkáda
se	s	k7c7	s
skleněnými	skleněný	k2eAgInPc7d1	skleněný
stopy	stop	k1gInPc7	stop
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
svého	svůj	k3xOyFgNnSc2	svůj
křížení	křížení	k1gNnSc2	křížení
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
oktagonální	oktagonální	k2eAgNnSc4d1	oktagonální
prostranství	prostranství	k1gNnSc4	prostranství
se	s	k7c7	s
skleněnou	skleněný	k2eAgFnSc7d1	skleněná
kupolí	kupole	k1gFnSc7	kupole
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
vstupem	vstup	k1gInSc7	vstup
je	být	k5eAaImIp3nS	být
monumentální	monumentální	k2eAgInSc1d1	monumentální
oblouk	oblouk	k1gInSc1	oblouk
z	z	k7c2	z
Piazza	Piazz	k1gMnSc2	Piazz
del	del	k?	del
Duomo	Duoma	k1gFnSc5	Duoma
(	(	kIx(	(
<g/>
Náměstí	náměstí	k1gNnSc1	náměstí
Dómu	dóm	k1gInSc2	dóm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
italským	italský	k2eAgInSc7d1	italský
architektem	architekt	k1gMnSc7	architekt
Giuseppem	Giusepp	k1gMnSc7	Giusepp
Mengonim	Mengonima	k1gFnPc2	Mengonima
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
a	a	k8xC	a
postavena	postavit	k5eAaPmNgFnS	postavit
během	během	k7c2	během
let	léto	k1gNnPc2	léto
1865	[number]	k4	1865
až	až	k6eAd1	až
1877	[number]	k4	1877
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
primárně	primárně	k6eAd1	primárně
jako	jako	k8xS	jako
luxusní	luxusní	k2eAgNnSc1d1	luxusní
nákupní	nákupní	k2eAgNnSc1d1	nákupní
centrum	centrum	k1gNnSc1	centrum
<g/>
,	,	kIx,	,
kterým	který	k3yIgFnPc3	který
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
i	i	k9	i
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
Palazzo	Palazza	k1gFnSc5	Palazza
Reale	Real	k1gInSc6	Real
<g/>
,	,	kIx,	,
stojí	stát	k5eAaImIp3nS	stát
napravo	napravo	k6eAd1	napravo
od	od	k7c2	od
dómu	dóm	k1gInSc2	dóm
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1772	[number]	k4	1772
<g/>
,	,	kIx,	,
sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
muzeum	muzeum	k1gNnSc1	muzeum
katedrály	katedrála	k1gFnSc2	katedrála
a	a	k8xC	a
Muzeum	muzeum	k1gNnSc1	muzeum
současného	současný	k2eAgNnSc2d1	současné
umění	umění	k1gNnSc2	umění
Palazzo	Palazza	k1gFnSc5	Palazza
della	dell	k1gMnSc2	dell
Ragione	Ragion	k1gInSc5	Ragion
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Piazza	Piazz	k1gMnSc2	Piazz
Mercanti	Mercant	k1gMnPc1	Mercant
byl	být	k5eAaImAgInS	být
vystavěný	vystavěný	k2eAgMnSc1d1	vystavěný
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1228	[number]	k4	1228
-	-	kIx~	-
33	[number]	k4	33
a	a	k8xC	a
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k8xS	jako
radnice	radnice	k1gFnSc1	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
palácem	palác	k1gInSc7	palác
pak	pak	k6eAd1	pak
stojí	stát	k5eAaImIp3nS	stát
palác	palác	k1gInSc4	palác
Loggia	Loggium	k1gNnSc2	Loggium
degli	degl	k1gMnPc1	degl
Osii	Osi	k1gMnPc1	Osi
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1316	[number]	k4	1316
<g/>
.	.	kIx.	.
</s>
<s>
Palazzo	Palazza	k1gFnSc5	Palazza
Marino	Marina	k1gFnSc5	Marina
<g/>
,	,	kIx,	,
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1558	[number]	k4	1558
<g/>
,	,	kIx,	,
současná	současný	k2eAgFnSc1d1	současná
budova	budova	k1gFnSc1	budova
radnice	radnice	k1gFnSc2	radnice
V	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgFnSc1d3	nejstarší
Pirellone	Pirellon	k1gInSc5	Pirellon
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
měří	měřit	k5eAaImIp3nS	měřit
130	[number]	k4	130
<g/>
m	m	kA	m
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
a	a	k8xC	a
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
budova	budova	k1gFnSc1	budova
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
Unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
budovou	budova	k1gFnSc7	budova
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
Torre	torr	k1gInSc5	torr
Unicredit	Unicredit	k1gMnPc6	Unicredit
<g/>
,	,	kIx,	,
s	s	k7c7	s
231	[number]	k4	231
<g/>
m	m	kA	m
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
budovou	budova	k1gFnSc7	budova
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Pinacoteca	Pinacoteca	k1gMnSc1	Pinacoteca
di	di	k?	di
Brera	Brera	k1gMnSc1	Brera
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
bývalém	bývalý	k2eAgInSc6d1	bývalý
jezuitském	jezuitský	k2eAgInSc6d1	jezuitský
paláci	palác	k1gInSc6	palác
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
st.	st.	kA	st.
Palazzo	Palazza	k1gFnSc5	Palazza
di	di	k?	di
Brera	Brero	k1gNnSc2	Brero
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
po	po	k7c6	po
florenstké	florenstký	k2eAgFnSc6d1	florenstký
galerii	galerie	k1gFnSc6	galerie
Uffizi	Uffize	k1gFnSc4	Uffize
<g/>
,	,	kIx,	,
o	o	k7c4	o
druhou	druhý	k4xOgFnSc4	druhý
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
sbírku	sbírka	k1gFnSc4	sbírka
obrazů	obraz	k1gInPc2	obraz
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
díla	dílo	k1gNnPc1	dílo
Tintoretta	Tintoretto	k1gNnPc1	Tintoretto
<g/>
,	,	kIx,	,
Raffaela	Raffaela	k1gFnSc1	Raffaela
<g/>
,	,	kIx,	,
Bramantova	Bramantův	k2eAgFnSc1d1	Bramantův
<g/>
,	,	kIx,	,
Caravaggia	Caravaggia	k1gFnSc1	Caravaggia
<g/>
.	.	kIx.	.
</s>
<s>
Pinacoteca	Pinacotec	k2eAgFnSc1d1	Pinacoteca
Ambrosiana	Ambrosiana	k1gFnSc1	Ambrosiana
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
Piazza	Piazz	k1gMnSc4	Piazz
Pio	Pio	k1gFnPc2	Pio
XI	XI	kA	XI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
v	v	k7c6	v
Palazzo	Palazza	k1gFnSc5	Palazza
dell	dell	k1gInSc1	dell
<g/>
'	'	kIx"	'
<g/>
Ambrosiana	Ambrosiana	k1gFnSc1	Ambrosiana
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
jsou	být	k5eAaImIp3nP	být
obrazy	obraz	k1gInPc4	obraz
od	od	k7c2	od
Botticelliho	Botticelli	k1gMnSc2	Botticelli
<g/>
,	,	kIx,	,
Tiziana	Tizian	k1gMnSc2	Tizian
<g/>
,	,	kIx,	,
Caravagia	Caravagius	k1gMnSc2	Caravagius
<g/>
,	,	kIx,	,
Raffaela	Raffael	k1gMnSc2	Raffael
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Biblioteca	Bibliotec	k2eAgFnSc1d1	Biblioteca
Ambrosiana	Ambrosiana	k1gFnSc1	Ambrosiana
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
milánským	milánský	k2eAgMnSc7d1	milánský
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Fredericem	Frederic	k1gMnSc7	Frederic
Borromeo	Borromeo	k1gMnSc1	Borromeo
(	(	kIx(	(
<g/>
1564	[number]	k4	1564
<g/>
-	-	kIx~	-
<g/>
1631	[number]	k4	1631
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
první	první	k4xOgFnSc4	první
veřejná	veřejný	k2eAgFnSc1d1	veřejná
knihovna	knihovna	k1gFnSc1	knihovna
na	na	k7c6	na
evropské	evropský	k2eAgFnSc6d1	Evropská
pevnině	pevnina	k1gFnSc6	pevnina
<g/>
,	,	kIx,	,
s	s	k7c7	s
velkými	velký	k2eAgFnPc7d1	velká
sbírkami	sbírka	k1gFnPc7	sbírka
rukopisů	rukopis	k1gInPc2	rukopis
a	a	k8xC	a
kreseb	kresba	k1gFnPc2	kresba
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
jedinečnou	jedinečný	k2eAgFnSc7d1	jedinečná
sbírkou	sbírka	k1gFnSc7	sbírka
kreseb	kresba	k1gFnPc2	kresba
Leonarda	Leonardo	k1gMnSc2	Leonardo
da	da	k?	da
Vinciho	Vinci	k1gMnSc2	Vinci
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc2	jeho
deníků	deník	k1gInPc2	deník
<g/>
.	.	kIx.	.
</s>
<s>
Museo	Museo	k1gNnSc1	Museo
Poldi	Poldi	k1gFnSc2	Poldi
Pezzoli	Pezzole	k1gFnSc3	Pezzole
<g/>
,	,	kIx,	,
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
až	až	k9	až
19	[number]	k4	19
<g/>
.	.	kIx.	.
st.	st.	kA	st.
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Via	via	k7c4	via
A.	A.	kA	A.
Manzoni	Manzoň	k1gFnSc6	Manzoň
<g/>
;	;	kIx,	;
obrazy	obraz	k1gInPc4	obraz
od	od	k7c2	od
Botticeliho	Botticeli	k1gMnSc2	Botticeli
<g/>
,	,	kIx,	,
Canaletta	Canalett	k1gMnSc2	Canalett
<g/>
,	,	kIx,	,
Guardiho	Guardi	k1gMnSc2	Guardi
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
</s>
<s>
Milán	Milán	k1gInSc1	Milán
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
9	[number]	k4	9
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
městské	městský	k2eAgFnPc1d1	městská
části	část	k1gFnPc1	část
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
čtvrtí	čtvrt	k1gFnPc2	čtvrt
(	(	kIx(	(
<g/>
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zóna	zóna	k1gFnSc1	zóna
1	[number]	k4	1
<g/>
:	:	kIx,	:
Centro	Centro	k1gNnSc1	Centro
storico	storico	k1gNnSc1	storico
(	(	kIx(	(
<g/>
Brera	Brera	k1gFnSc1	Brera
<g/>
,	,	kIx,	,
Centro	Centro	k1gNnSc1	Centro
Storico	Storico	k1gMnSc1	Storico
<g/>
,	,	kIx,	,
Conca	Conca	k1gMnSc1	Conca
del	del	k?	del
Naviglio	Naviglio	k1gMnSc1	Naviglio
<g/>
,	,	kIx,	,
Guastalla	Guastalla	k1gMnSc1	Guastalla
<g/>
,	,	kIx,	,
Porta	porta	k1gFnSc1	porta
Sempione	Sempion	k1gInSc5	Sempion
<g/>
,	,	kIx,	,
Porta	porta	k1gFnSc1	porta
Tenaglia	Tenaglia	k1gFnSc1	Tenaglia
<g/>
)	)	kIx)	)
Zóna	zóna	k1gFnSc1	zóna
2	[number]	k4	2
<g/>
:	:	kIx,	:
Stazione	Stazion	k1gInSc5	Stazion
Centrale	Central	k1gMnSc5	Central
<g/>
,	,	kIx,	,
Gorla	Gorlo	k1gNnPc1	Gorlo
<g/>
,	,	kIx,	,
Turro	Turro	k1gNnSc1	Turro
<g/>
,	,	kIx,	,
Precotto	Precotto	k1gNnSc1	Precotto
<g/>
,	,	kIx,	,
Greco	Greco	k1gNnSc1	Greco
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Crescenzago	Crescenzaga	k1gMnSc5	Crescenzaga
(	(	kIx(	(
<g/>
Adriano	Adriana	k1gFnSc5	Adriana
<g/>
,	,	kIx,	,
Crescenzago	Crescenzaga	k1gFnSc5	Crescenzaga
<g/>
,	,	kIx,	,
Gorla	Gorla	k1gMnSc1	Gorla
<g/>
,	,	kIx,	,
Greco	Greco	k1gMnSc1	Greco
<g/>
,	,	kIx,	,
Loreto	Loreto	k1gNnSc1	Loreto
<g/>
,	,	kIx,	,
Maggiolina	Maggiolina	k1gFnSc1	Maggiolina
<g/>
,	,	kIx,	,
Mandello	Mandello	k1gNnSc1	Mandello
<g/>
,	,	kIx,	,
Mirabello	Mirabello	k1gNnSc1	Mirabello
<g/>
,	,	kIx,	,
Ponte	Pont	k1gInSc5	Pont
Seveso	Sevesa	k1gFnSc5	Sevesa
<g/>
,	,	kIx,	,
Porta	porta	k1gFnSc1	porta
Nuova	Nuova	k1gFnSc1	Nuova
<g/>
,	,	kIx,	,
Precotto	Precotto	k1gNnSc1	Precotto
<g/>
,	,	kIx,	,
Stazione	Stazion	k1gInSc5	Stazion
Centrale	Central	k1gMnSc5	Central
<g/>
,	,	kIx,	,
Turro	Turra	k1gMnSc5	Turra
<g/>
,	,	kIx,	,
Villaggio	Villaggio	k6eAd1	Villaggio
dei	dei	k?	dei
Giornalisti	Giornalist	k1gMnPc1	Giornalist
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Zóna	zóna	k1gFnSc1	zóna
3	[number]	k4	3
<g/>
:	:	kIx,	:
Città	Città	k1gMnSc1	Città
Studi	Stud	k1gMnPc1	Stud
<g/>
,	,	kIx,	,
Lambrate	Lambrat	k1gMnSc5	Lambrat
<g/>
,	,	kIx,	,
Venezia	Venezia	k1gFnSc1	Venezia
(	(	kIx(	(
<g/>
Casoretto	Casoretto	k1gNnSc1	Casoretto
<g/>
,	,	kIx,	,
Cimiano	Cimiana	k1gFnSc5	Cimiana
<g/>
,	,	kIx,	,
Città	Città	k1gMnSc1	Città
Studi	Stud	k1gMnPc1	Stud
<g/>
,	,	kIx,	,
Dosso	Dossa	k1gFnSc5	Dossa
<g/>
,	,	kIx,	,
Lambrate	Lambrat	k1gMnSc5	Lambrat
<g/>
,	,	kIx,	,
Ortica	Ortica	k1gFnSc1	Ortica
<g/>
,	,	kIx,	,
Porta	porta	k1gFnSc1	porta
Monforte	Monfort	k1gInSc5	Monfort
<g/>
,	,	kIx,	,
Porta	porta	k1gFnSc1	porta
Venezia	Venezia	k1gFnSc1	Venezia
<g/>
,	,	kIx,	,
Quartiere	Quartier	k1gMnSc5	Quartier
Feltre	Feltr	k1gMnSc5	Feltr
<g/>
,	,	kIx,	,
Rottole	Rottola	k1gFnSc6	Rottola
<g/>
)	)	kIx)	)
Zóna	zóna	k1gFnSc1	zóna
<g />
.	.	kIx.	.
</s>
<s>
4	[number]	k4	4
<g/>
:	:	kIx,	:
Vittoria	Vittorium	k1gNnSc2	Vittorium
<g/>
,	,	kIx,	,
Forlanini	Forlanin	k2eAgMnPc1d1	Forlanin
(	(	kIx(	(
<g/>
Acquabella	Acquabella	k1gMnSc1	Acquabella
<g/>
,	,	kIx,	,
Calvairate	Calvairat	k1gMnSc5	Calvairat
<g/>
,	,	kIx,	,
Castagnedo	Castagneda	k1gFnSc5	Castagneda
<g/>
,	,	kIx,	,
Cavriano	Cavriana	k1gFnSc5	Cavriana
<g/>
,	,	kIx,	,
Forlanini	Forlanin	k2eAgMnPc1d1	Forlanin
<g/>
,	,	kIx,	,
Gamboloita	Gamboloita	k1gFnSc1	Gamboloita
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Trecca	Trecc	k1gInSc2	Trecc
<g/>
,	,	kIx,	,
Monluè	Monluè	k1gFnSc5	Monluè
<g/>
,	,	kIx,	,
Morsenchio	Morsenchia	k1gFnSc5	Morsenchia
<g/>
,	,	kIx,	,
Nosedo	Noseda	k1gFnSc5	Noseda
<g/>
,	,	kIx,	,
Omero	Omera	k1gFnSc5	Omera
<g/>
,	,	kIx,	,
Ponte	Pont	k1gInSc5	Pont
Lambro	Lambro	k1gNnSc1	Lambro
<g/>
,	,	kIx,	,
Porta	porta	k1gFnSc1	porta
Vittoria	Vittorium	k1gNnSc2	Vittorium
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Porta	porta	k1gFnSc1	porta
Romana	Romana	k1gFnSc1	Romana
<g/>
,	,	kIx,	,
Rogoredo	Rogoredo	k1gNnSc1	Rogoredo
<g/>
,	,	kIx,	,
San	San	k1gFnSc1	San
Luigi	Luig	k1gFnSc2	Luig
<g/>
,	,	kIx,	,
Santa	Sant	k1gMnSc2	Sant
Giulia	Giulius	k1gMnSc2	Giulius
<g/>
,	,	kIx,	,
Taliedo	Talieda	k1gFnSc5	Talieda
<g/>
,	,	kIx,	,
Triulzo	Triulza	k1gFnSc5	Triulza
Superiore	superior	k1gMnSc5	superior
<g/>
)	)	kIx)	)
Zóna	zóna	k1gFnSc1	zóna
5	[number]	k4	5
<g/>
:	:	kIx,	:
Vigentino	Vigentin	k2eAgNnSc1d1	Vigentin
<g/>
,	,	kIx,	,
Chiaravalle	Chiaravalle	k1gInSc1	Chiaravalle
<g/>
,	,	kIx,	,
Gratosoglio	Gratosoglio	k6eAd1	Gratosoglio
Zóna	zóna	k1gFnSc1	zóna
6	[number]	k4	6
<g/>
:	:	kIx,	:
Barona	baron	k1gMnSc4	baron
<g/>
,	,	kIx,	,
Lorenteggio	Lorenteggio	k6eAd1	Lorenteggio
Zóna	zóna	k1gFnSc1	zóna
7	[number]	k4	7
<g/>
:	:	kIx,	:
Baggio	Baggio	k1gMnSc1	Baggio
<g/>
,	,	kIx,	,
De	De	k?	De
<g />
.	.	kIx.	.
</s>
<s>
Angeli	angel	k1gMnSc5	angel
<g/>
,	,	kIx,	,
San	San	k1gFnSc3	San
Siro	siro	k6eAd1	siro
Zóna	zóna	k1gFnSc1	zóna
8	[number]	k4	8
<g/>
:	:	kIx,	:
Fiera	Fiera	k1gFnSc1	Fiera
<g/>
,	,	kIx,	,
Gallaratese	Gallaratese	k1gFnSc1	Gallaratese
<g/>
,	,	kIx,	,
Certosa	Certosa	k1gFnSc1	Certosa
<g/>
,	,	kIx,	,
Quarto	Quarta	k1gFnSc5	Quarta
Oggiaro	Oggiara	k1gFnSc5	Oggiara
Zóna	zóna	k1gFnSc1	zóna
9	[number]	k4	9
<g/>
:	:	kIx,	:
Stazione	Stazion	k1gInSc5	Stazion
Garibaldi	Garibald	k1gMnPc1	Garibald
<g/>
,	,	kIx,	,
Niguarda	Niguarda	k1gFnSc1	Niguarda
Milán	Milán	k1gInSc1	Milán
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
klíčových	klíčový	k2eAgNnPc2d1	klíčové
měst	město	k1gNnPc2	město
průmyslového	průmyslový	k2eAgInSc2d1	průmyslový
a	a	k8xC	a
moderního	moderní	k2eAgInSc2d1	moderní
designu	design	k1gInSc2	design
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejvlivnějších	vlivný	k2eAgNnPc2d3	nejvlivnější
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
známé	známý	k2eAgFnPc1d1	známá
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
vysoce	vysoce	k6eAd1	vysoce
kvalitní	kvalitní	k2eAgInSc4d1	kvalitní
starobylý	starobylý	k2eAgInSc4d1	starobylý
i	i	k8xC	i
moderní	moderní	k2eAgInSc4d1	moderní
nábytek	nábytek	k1gInSc4	nábytek
a	a	k8xC	a
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
zboží	zboží	k1gNnSc4	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Milán	Milán	k1gInSc1	Milán
hostí	hostit	k5eAaImIp3nS	hostit
mimo	mimo	k7c4	mimo
již	již	k6eAd1	již
zmíněný	zmíněný	k2eAgInSc4d1	zmíněný
veletrh	veletrh	k1gInSc4	veletrh
FieraMilano	FieraMilana	k1gFnSc5	FieraMilana
také	také	k9	také
nejdůležitější	důležitý	k2eAgFnPc4d3	nejdůležitější
události	událost	k1gFnPc4	událost
související	související	k2eAgFnPc4d1	související
s	s	k7c7	s
designem	design	k1gInSc7	design
a	a	k8xC	a
architekturou	architektura	k1gFnSc7	architektura
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
"	"	kIx"	"
<g/>
Fuori	Fuori	k1gNnSc7	Fuori
Salone	salon	k1gInSc5	salon
<g/>
"	"	kIx"	"
a	a	k8xC	a
Salone	salon	k1gInSc5	salon
del	del	k?	del
Mobile	mobile	k1gNnPc7	mobile
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
začátkem	začátkem	k7c2	začátkem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Milán	Milán	k1gInSc1	Milán
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Turínem	Turín	k1gInSc7	Turín
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
poválečného	poválečný	k2eAgInSc2d1	poválečný
designu	design	k1gInSc2	design
a	a	k8xC	a
architektury	architektura	k1gFnSc2	architektura
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
průmyslovým	průmyslový	k2eAgNnSc7d1	průmyslové
centrem	centrum	k1gNnSc7	centrum
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejprogresivnějších	progresivní	k2eAgNnPc2d3	nejprogresivnější
a	a	k8xC	a
nejdynamičtějších	dynamický	k2eAgNnPc2d3	nejdynamičtější
měst	město	k1gNnPc2	město
pevninské	pevninský	k2eAgFnSc2d1	pevninská
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zde	zde	k6eAd1	zde
postaveno	postavit	k5eAaPmNgNnS	postavit
několik	několik	k4yIc1	několik
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
například	například	k6eAd1	například
Pirelli	Pirell	k1gMnSc3	Pirell
Tower	Towra	k1gFnPc2	Towra
a	a	k8xC	a
Torre	torr	k1gInSc5	torr
Velasca	Velascus	k1gMnSc4	Velascus
<g/>
.	.	kIx.	.
</s>
<s>
Umělci	umělec	k1gMnPc1	umělec
jako	jako	k9	jako
Bruno	Bruno	k1gMnSc1	Bruno
Munari	Munar	k1gFnSc2	Munar
<g/>
,	,	kIx,	,
Lucio	Lucio	k6eAd1	Lucio
Fontana	Fontana	k1gFnSc1	Fontana
<g/>
,	,	kIx,	,
Enrico	Enrico	k6eAd1	Enrico
Castellani	Castellan	k1gMnPc1	Castellan
<g/>
,	,	kIx,	,
Piero	Piero	k1gNnSc1	Piero
Manzoni	Manzoň	k1gFnSc3	Manzoň
a	a	k8xC	a
mnoho	mnoho	k6eAd1	mnoho
dalších	další	k2eAgInPc2d1	další
ve	v	k7c6	v
městě	město	k1gNnSc6	město
buď	buď	k8xC	buď
žili	žít	k5eAaImAgMnP	žít
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
tam	tam	k6eAd1	tam
pracovali	pracovat	k5eAaImAgMnP	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Milán	Milán	k1gInSc1	Milán
je	být	k5eAaImIp3nS	být
také	také	k9	také
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
světové	světový	k2eAgNnSc4d1	světové
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
módy	móda	k1gFnSc2	móda
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
City	City	k1gFnSc1	City
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
nebo	nebo	k8xC	nebo
Londýn	Londýn	k1gInSc1	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Global	globat	k5eAaImAgInS	globat
Language	language	k1gFnSc4	language
Monitor	monitor	k1gInSc1	monitor
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
Milán	Milán	k1gInSc1	Milán
umístil	umístit	k5eAaPmAgInS	umístit
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
žebříčku	žebříček	k1gInSc2	žebříček
ekonomických	ekonomický	k2eAgNnPc2d1	ekonomické
a	a	k8xC	a
mediálních	mediální	k2eAgNnPc2d1	mediální
globálních	globální	k2eAgNnPc2d1	globální
měst	město	k1gNnPc2	město
módy	móda	k1gFnSc2	móda
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
propadl	propadnout	k5eAaPmAgInS	propadnout
na	na	k7c4	na
šesté	šestý	k4xOgNnSc4	šestý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
a	a	k8xC	a
zase	zase	k9	zase
si	se	k3xPyFc3	se
polepšil	polepšit	k5eAaPmAgMnS	polepšit
na	na	k7c4	na
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
příčku	příčka	k1gFnSc4	příčka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
předních	přední	k2eAgFnPc2d1	přední
italských	italský	k2eAgFnPc2d1	italská
módních	módní	k2eAgFnPc2d1	módní
značek	značka	k1gFnPc2	značka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Valentino	Valentina	k1gFnSc5	Valentina
<g/>
,	,	kIx,	,
Gucci	Gucce	k1gMnPc7	Gucce
<g/>
,	,	kIx,	,
Versace	Versace	k1gFnSc1	Versace
<g/>
,	,	kIx,	,
Prada	Prada	k1gFnSc1	Prada
<g/>
,	,	kIx,	,
Armani	Arman	k1gMnPc1	Arman
a	a	k8xC	a
Dolce	dolce	k6eAd1	dolce
&	&	k?	&
Gabbana	Gabbana	k1gFnSc1	Gabbana
<g/>
,	,	kIx,	,
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Četné	četný	k2eAgFnPc1d1	četná
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
módní	módní	k2eAgFnPc1d1	módní
značky	značka	k1gFnPc1	značka
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
své	svůj	k3xOyFgInPc4	svůj
obchody	obchod	k1gInPc4	obchod
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
obchodu	obchod	k1gInSc2	obchod
Abercrombie	Abercrombie	k1gFnSc2	Abercrombie
&	&	k?	&
Fitch	Fitch	k1gMnSc1	Fitch
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hlavní	hlavní	k2eAgFnSc7d1	hlavní
spotřebitelskou	spotřebitelský	k2eAgFnSc7d1	spotřebitelská
atrakcí	atrakce	k1gFnSc7	atrakce
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
navíc	navíc	k6eAd1	navíc
dvakrát	dvakrát	k6eAd1	dvakrát
ročně	ročně	k6eAd1	ročně
hostí	hostit	k5eAaImIp3nS	hostit
Milan	Milan	k1gMnSc1	Milan
Fashion	Fashion	k1gInSc1	Fashion
Week	Weeko	k1gNnPc2	Weeko
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
ostatní	ostatní	k2eAgNnPc1d1	ostatní
mezinárodní	mezinárodní	k2eAgNnPc1d1	mezinárodní
centra	centrum	k1gNnPc1	centrum
módy	móda	k1gFnSc2	móda
<g/>
:	:	kIx,	:
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Tokio	Tokio	k1gNnSc1	Tokio
a	a	k8xC	a
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
milánská	milánský	k2eAgFnSc1d1	Milánská
luxusní	luxusní	k2eAgFnSc1d1	luxusní
módní	módní	k2eAgFnSc1d1	módní
čtvrť	čtvrť	k1gFnSc1	čtvrť
je	být	k5eAaImIp3nS	být
Quadrilatero	Quadrilatero	k1gNnSc4	Quadrilatero
della	dell	k1gMnSc2	dell
moda	modus	k1gMnSc2	modus
(	(	kIx(	(
<g/>
doslovně	doslovně	k6eAd1	doslovně
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
čtyřúhelník	čtyřúhelník	k1gInSc1	čtyřúhelník
módy	móda	k1gFnSc2	móda
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
nejprestižnější	prestižní	k2eAgFnPc1d3	nejprestižnější
nákupní	nákupní	k2eAgFnPc1d1	nákupní
ulice	ulice	k1gFnPc1	ulice
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
Via	via	k7c4	via
Monte	Mont	k1gMnSc5	Mont
Napoleone	Napoleon	k1gMnSc5	Napoleon
<g/>
,	,	kIx,	,
Via	via	k7c4	via
della	dell	k1gMnSc4	dell
Spiga	Spig	k1gMnSc4	Spig
<g/>
,	,	kIx,	,
Via	via	k7c4	via
Sant	Sant	k1gInSc4	Sant
<g/>
'	'	kIx"	'
<g/>
Andrea	Andrea	k1gFnSc1	Andrea
<g/>
,	,	kIx,	,
Via	via	k7c4	via
Manzoni	Manzon	k1gMnPc1	Manzon
a	a	k8xC	a
Corso	Corsa	k1gFnSc5	Corsa
Venezia	Venezium	k1gNnPc4	Venezium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
důležité	důležitý	k2eAgFnPc4d1	důležitá
nákupní	nákupní	k2eAgFnPc4d1	nákupní
ulice	ulice	k1gFnPc4	ulice
a	a	k8xC	a
náměstí	náměstí	k1gNnSc6	náměstí
patří	patřit	k5eAaImIp3nP	patřit
Galleria	Gallerium	k1gNnPc1	Gallerium
Vittorio	Vittorio	k1gMnSc1	Vittorio
Emanuele	Emanuela	k1gFnSc3	Emanuela
II	II	kA	II
<g/>
,	,	kIx,	,
Piazza	Piazza	k1gFnSc1	Piazza
del	del	k?	del
Duomo	Duoma	k1gFnSc5	Duoma
<g/>
,	,	kIx,	,
Via	via	k7c4	via
Dante	Dante	k1gMnSc1	Dante
a	a	k8xC	a
Corso	Corsa	k1gFnSc5	Corsa
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gInSc1	Aires
<g/>
.	.	kIx.	.
</s>
<s>
Mario	Mario	k1gMnSc1	Mario
Prada	Prada	k1gMnSc1	Prada
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
značky	značka	k1gFnSc2	značka
Prada	Prada	k1gMnSc1	Prada
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
narodil	narodit	k5eAaPmAgMnS	narodit
<g/>
,	,	kIx,	,
a	a	k8xC	a
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
udržovat	udržovat	k5eAaImF	udržovat
postavení	postavení	k1gNnSc4	postavení
Milána	Milán	k1gInSc2	Milán
jako	jako	k8xS	jako
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
světové	světový	k2eAgFnSc2d1	světová
módy	móda	k1gFnSc2	móda
<g/>
.	.	kIx.	.
</s>
<s>
Milán	Milán	k1gInSc1	Milán
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
několika	několik	k4yIc2	několik
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
italských	italský	k2eAgFnPc2d1	italská
vzdělávacích	vzdělávací	k2eAgFnPc2d1	vzdělávací
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Milánský	milánský	k2eAgInSc1d1	milánský
systém	systém	k1gInSc1	systém
vysokoškolského	vysokoškolský	k2eAgNnSc2d1	vysokoškolské
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
7	[number]	k4	7
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
48	[number]	k4	48
fakult	fakulta	k1gFnPc2	fakulta
a	a	k8xC	a
142	[number]	k4	142
oddělení	oddělení	k1gNnPc2	oddělení
<g/>
,	,	kIx,	,
se	s	k7c7	s
185.000	[number]	k4	185.000
vysokoškolských	vysokoškolský	k2eAgMnPc2d1	vysokoškolský
studentů	student	k1gMnPc2	student
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
11	[number]	k4	11
<g/>
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
populace	populace	k1gFnSc2	populace
vysokoškolských	vysokoškolský	k2eAgMnPc2d1	vysokoškolský
studentů	student	k1gMnPc2	student
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
)	)	kIx)	)
a	a	k8xC	a
největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
absolventů	absolvent	k1gMnPc2	absolvent
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
doktorandů	doktorand	k1gMnPc2	doktorand
(	(	kIx(	(
<g/>
34000	[number]	k4	34000
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5000	[number]	k4	5000
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
<g/>
)	)	kIx)	)
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Politecnico	Politecnico	k1gMnSc1	Politecnico
di	di	k?	di
Milano	Milana	k1gFnSc5	Milana
<g/>
,	,	kIx,	,
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc7d3	nejstarší
univerzitou	univerzita	k1gFnSc7	univerzita
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
16	[number]	k4	16
oddělení	oddělení	k1gNnSc2	oddělení
a	a	k8xC	a
sítě	síť	k1gFnPc1	síť
9	[number]	k4	9
škol	škola	k1gFnPc2	škola
inženýrství	inženýrství	k1gNnSc2	inženýrství
<g/>
,	,	kIx,	,
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
průmyslového	průmyslový	k2eAgInSc2d1	průmyslový
designu	design	k1gInSc2	design
<g/>
,	,	kIx,	,
na	na	k7c6	na
7	[number]	k4	7
kampusech	kampus	k1gInPc6	kampus
v	v	k7c6	v
lombardském	lombardský	k2eAgInSc6d1	lombardský
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
studentů	student	k1gMnPc2	student
zapsaných	zapsaný	k2eAgMnPc2d1	zapsaný
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
areálech	areál	k1gInPc6	areál
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
38.000	[number]	k4	38.000
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
činí	činit	k5eAaImIp3nS	činit
z	z	k7c2	z
Politecnico	Politecnico	k6eAd1	Politecnico
největší	veliký	k2eAgFnSc4d3	veliký
technickou	technický	k2eAgFnSc4d1	technická
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc1d1	státní
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
<g/>
,	,	kIx,	,
založená	založený	k2eAgNnPc1d1	založené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
veřejnou	veřejný	k2eAgFnSc7d1	veřejná
výzkumnou	výzkumný	k2eAgFnSc7d1	výzkumná
vysokou	vysoký	k2eAgFnSc7d1	vysoká
školou	škola	k1gFnSc7	škola
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
s	s	k7c7	s
9	[number]	k4	9
fakultami	fakulta	k1gFnPc7	fakulta
<g/>
,	,	kIx,	,
58	[number]	k4	58
odděleními	oddělení	k1gNnPc7	oddělení
<g/>
,	,	kIx,	,
48	[number]	k4	48
ústavy	ústav	k1gInPc1	ústav
a	a	k8xC	a
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2500	[number]	k4	2500
profesory	profesor	k1gMnPc7	profesor
<g/>
.	.	kIx.	.
</s>
<s>
Milánská	milánský	k2eAgFnSc1d1	Milánská
univerzita	univerzita	k1gFnSc1	univerzita
je	být	k5eAaImIp3nS	být
vůdčí	vůdčí	k2eAgFnSc7d1	vůdčí
italskou	italský	k2eAgFnSc7d1	italská
i	i	k8xC	i
evropskou	evropský	k2eAgFnSc7d1	Evropská
institucí	instituce	k1gFnSc7	instituce
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
vědecké	vědecký	k2eAgFnSc2d1	vědecká
publikace	publikace	k1gFnSc2	publikace
<g/>
,	,	kIx,	,
a	a	k8xC	a
šestou	šestý	k4xOgFnSc7	šestý
největší	veliký	k2eAgFnSc7d3	veliký
univerzitou	univerzita	k1gFnSc7	univerzita
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
s	s	k7c7	s
přibližně	přibližně	k6eAd1	přibližně
60.000	[number]	k4	60.000
zapsanými	zapsaný	k2eAgMnPc7d1	zapsaný
studenty	student	k1gMnPc7	student
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
prominentním	prominentní	k2eAgFnPc3d1	prominentní
univerzitám	univerzita	k1gFnPc3	univerzita
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
soukromá	soukromý	k2eAgFnSc1d1	soukromá
Univerzita	univerzita	k1gFnSc1	univerzita
Bocconi	Boccoň	k1gFnSc3	Boccoň
<g/>
,	,	kIx,	,
zaměřující	zaměřující	k2eAgMnSc1d1	zaměřující
se	se	k3xPyFc4	se
na	na	k7c4	na
výuku	výuka	k1gFnSc4	výuka
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
managementu	management	k1gInSc2	management
a	a	k8xC	a
financí	finance	k1gFnPc2	finance
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
sedmou	sedmý	k4xOgFnSc7	sedmý
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
podnikatelskou	podnikatelský	k2eAgFnSc7d1	podnikatelská
školou	škola	k1gFnSc7	škola
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
;	;	kIx,	;
Università	Università	k1gFnPc6	Università
Cattolica	Cattolic	k1gInSc2	Cattolic
del	del	k?	del
Sacro	Sacra	k1gMnSc5	Sacra
Cuore	Cuor	k1gMnSc5	Cuor
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
největší	veliký	k2eAgFnSc7d3	veliký
katolickou	katolický	k2eAgFnSc7d1	katolická
<g />
.	.	kIx.	.
</s>
<s>
univerzitou	univerzita	k1gFnSc7	univerzita
na	na	k7c6	na
světě	svět	k1gInSc6	svět
s	s	k7c7	s
42.000	[number]	k4	42.000
přijatých	přijatý	k2eAgMnPc2d1	přijatý
studentů	student	k1gMnPc2	student
<g/>
;	;	kIx,	;
Milánská	milánský	k2eAgFnSc1d1	Milánská
univerzita	univerzita	k1gFnSc1	univerzita
Bicocca	Bicocc	k2eAgFnSc1d1	Bicocc
<g/>
,	,	kIx,	,
multidisciplinární	multidisciplinární	k2eAgFnSc1d1	multidisciplinární
veřejná	veřejný	k2eAgFnSc1d1	veřejná
univerzita	univerzita	k1gFnSc1	univerzita
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30.000	[number]	k4	30.000
studenty	student	k1gMnPc7	student
<g/>
;	;	kIx,	;
IULM	IULM	kA	IULM
<g/>
,	,	kIx,	,
specializující	specializující	k2eAgMnSc1d1	specializující
se	se	k3xPyFc4	se
na	na	k7c4	na
marketingové	marketingový	k2eAgFnPc4d1	marketingová
<g/>
,	,	kIx,	,
informační	informační	k2eAgFnPc4d1	informační
a	a	k8xC	a
komunikační	komunikační	k2eAgFnPc4d1	komunikační
technologie	technologie	k1gFnPc4	technologie
<g/>
,	,	kIx,	,
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
a	a	k8xC	a
módu	móda	k1gFnSc4	móda
<g/>
;	;	kIx,	;
Università	Università	k1gFnSc1	Università
Vita	vit	k2eAgFnSc1d1	Vita
Salute	salut	k1gInSc5	salut
San	San	k1gMnSc3	San
Raffaele	Raffael	k1gInSc2	Raffael
<g/>
,	,	kIx,	,
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
nemocnicí	nemocnice	k1gFnSc7	nemocnice
San	San	k1gFnSc2	San
Raffaele	Raffael	k1gInSc2	Raffael
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
výzkumných	výzkumný	k2eAgFnPc2d1	výzkumná
laboratoří	laboratoř	k1gFnPc2	laboratoř
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
neurologie	neurologie	k1gFnSc2	neurologie
<g/>
,	,	kIx,	,
neurochirurgie	neurochirurgie	k1gFnSc2	neurochirurgie
<g/>
,	,	kIx,	,
diabetologie	diabetologie	k1gFnSc2	diabetologie
<g/>
,	,	kIx,	,
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
<g/>
,	,	kIx,	,
studií	studie	k1gFnSc7	studie
AIDS	AIDS	kA	AIDS
a	a	k8xC	a
kognitivních	kognitivní	k2eAgFnPc2d1	kognitivní
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Milánské	milánský	k2eAgNnSc1d1	Milánské
metro	metro	k1gNnSc1	metro
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
101	[number]	k4	101
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
83	[number]	k4	83
je	být	k5eAaImIp3nS	být
podzemních	podzemní	k2eAgFnPc2d1	podzemní
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dohromady	dohromady	k6eAd1	dohromady
čtyři	čtyři	k4xCgFnPc1	čtyři
linky	linka	k1gFnPc1	linka
označené	označený	k2eAgFnSc2d1	označená
M	M	kA	M
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
M3	M3	k1gFnSc1	M3
a	a	k8xC	a
M5	M5	k1gFnSc1	M5
(	(	kIx(	(
<g/>
zatím	zatím	k6eAd1	zatím
s	s	k7c7	s
vynecháním	vynechání	k1gNnSc7	vynechání
linky	linka	k1gFnSc2	linka
M	M	kA	M
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
tras	trasa	k1gFnPc2	trasa
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
84	[number]	k4	84
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
let	léto	k1gNnPc2	léto
2020	[number]	k4	2020
až	až	k6eAd1	až
2030	[number]	k4	2030
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
metro	metro	k1gNnSc1	metro
mělo	mít	k5eAaImAgNnS	mít
rozšířit	rozšířit	k5eAaPmF	rozšířit
až	až	k9	až
na	na	k7c4	na
100	[number]	k4	100
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Systémy	systém	k1gInPc1	systém
zpoplatnění	zpoplatnění	k1gNnSc2	zpoplatnění
vjezdu	vjezd	k1gInSc2	vjezd
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zlepšit	zlepšit	k5eAaPmF	zlepšit
kvalitu	kvalita	k1gFnSc4	kvalita
ovzduší	ovzduší	k1gNnSc2	ovzduší
a	a	k8xC	a
omezit	omezit	k5eAaPmF	omezit
počet	počet	k1gInSc4	počet
vozidel	vozidlo	k1gNnPc2	vozidlo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Zpoplatněna	zpoplatněn	k2eAgFnSc1d1	zpoplatněna
byla	být	k5eAaImAgFnS	být
zóna	zóna	k1gFnSc1	zóna
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
8	[number]	k4	8
km	km	kA	km
<g/>
2	[number]	k4	2
v	v	k7c6	v
pracovních	pracovní	k2eAgInPc6d1	pracovní
dnech	den	k1gInPc6	den
od	od	k7c2	od
7.30	[number]	k4	7.30
do	do	k7c2	do
19.30	[number]	k4	19.30
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
většina	většina	k1gFnSc1	většina
měst	město	k1gNnPc2	město
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
Milán	Milán	k1gInSc1	Milán
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
okolí	okolí	k1gNnSc1	okolí
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
regionální	regionální	k2eAgFnSc4d1	regionální
kuchyni	kuchyně	k1gFnSc4	kuchyně
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
lombardská	lombardský	k2eAgFnSc1d1	Lombardská
kuchyně	kuchyně	k1gFnSc1	kuchyně
obecně	obecně	k6eAd1	obecně
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
častěji	často	k6eAd2	často
rýži	rýže	k1gFnSc4	rýže
než	než	k8xS	než
těstoviny	těstovina	k1gFnPc4	těstovina
<g/>
,	,	kIx,	,
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
nepoužívá	používat	k5eNaImIp3nS	používat
rajčata	rajče	k1gNnPc4	rajče
<g/>
.	.	kIx.	.
</s>
<s>
Milánská	milánský	k2eAgFnSc1d1	Milánská
kuchyně	kuchyně	k1gFnSc1	kuchyně
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
"	"	kIx"	"
<g/>
Cotoletta	Cotoletta	k1gFnSc1	Cotoletta
alla	alla	k1gFnSc1	alla
Milanese	Milanese	k1gFnSc1	Milanese
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
obalovaný	obalovaný	k2eAgInSc1d1	obalovaný
telecí	telecí	k2eAgInSc1d1	telecí
(	(	kIx(	(
<g/>
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
také	také	k9	také
vepřový	vepřový	k2eAgInSc4d1	vepřový
či	či	k8xC	či
krůtí	krůtí	k2eAgInSc4d1	krůtí
<g/>
)	)	kIx)	)
řízek	řízek	k1gInSc4	řízek
smažený	smažený	k2eAgInSc4d1	smažený
na	na	k7c6	na
másle	máslo	k1gNnSc6	máslo
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
vídeňský	vídeňský	k2eAgMnSc1d1	vídeňský
"	"	kIx"	"
<g/>
Wienerschnitzel	Wienerschnitzel	k1gMnSc1	Wienerschnitzel
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vzešel	vzejít	k5eAaPmAgInS	vzejít
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
milánské	milánský	k2eAgFnSc2d1	Milánská
speciality	specialita	k1gFnSc2	specialita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
typickými	typický	k2eAgInPc7d1	typický
pokrmy	pokrm	k1gInPc7	pokrm
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
cassoeula	cassoeula	k1gFnSc1	cassoeula
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dušená	dušený	k2eAgFnSc1d1	dušená
vepřová	vepřová	k1gFnSc1	vepřová
žebírka	žebírko	k1gNnSc2	žebírko
s	s	k7c7	s
klobásou	klobása	k1gFnSc7	klobása
a	a	k8xC	a
hlávkovou	hlávkový	k2eAgFnSc7d1	hlávková
kapustou	kapusta	k1gFnSc7	kapusta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
ossobuco	ossobuco	k6eAd1	ossobuco
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dušená	dušený	k2eAgFnSc1d1	dušená
telecí	telecí	k2eAgFnSc1d1	telecí
kost	kost	k1gFnSc1	kost
s	s	k7c7	s
morkem	morek	k1gInSc7	morek
s	s	k7c7	s
omáčkou	omáčka	k1gFnSc7	omáčka
zvanou	zvaný	k2eAgFnSc7d1	zvaná
gremolata	gremole	k1gNnPc1	gremole
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rizoto	rizoto	k1gNnSc1	rizoto
"	"	kIx"	"
<g/>
alla	all	k2eAgFnSc1d1	alla
Milanese	Milanese	k1gFnSc1	Milanese
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
se	s	k7c7	s
šafránem	šafrán	k1gInSc7	šafrán
a	a	k8xC	a
hovězím	hovězí	k2eAgInSc7d1	hovězí
morkem	morek	k1gInSc7	morek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Busecca	Busecca	k1gFnSc1	Busecca
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dušené	dušený	k2eAgFnPc1d1	dušená
dršťky	dršťka	k1gFnPc1	dršťka
s	s	k7c7	s
fazolemi	fazole	k1gFnPc7	fazole
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
"	"	kIx"	"
<g/>
Brasato	Brasat	k2eAgNnSc1d1	Brasat
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dušené	dušený	k2eAgNnSc1d1	dušené
hovězí	hovězí	k2eAgNnSc1d1	hovězí
nebo	nebo	k8xC	nebo
vepřové	vepřový	k2eAgNnSc1d1	vepřové
maso	maso	k1gNnSc1	maso
na	na	k7c6	na
víně	víno	k1gNnSc6	víno
s	s	k7c7	s
brambory	brambor	k1gInPc7	brambor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sezónní	sezónní	k2eAgNnSc4d1	sezónní
pečivo	pečivo	k1gNnSc4	pečivo
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
"	"	kIx"	"
<g/>
Chiacchiere	Chiacchier	k1gInSc5	Chiacchier
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
lívance	lívanec	k1gInPc1	lívanec
posypané	posypaný	k2eAgInPc1d1	posypaný
cukrem	cukr	k1gInSc7	cukr
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Tortelli	Tortell	k1gMnPc1	Tortell
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
smažené	smažený	k2eAgFnPc1d1	smažená
koulovité	koulovitý	k2eAgFnPc1d1	koulovitý
sušenky	sušenka	k1gFnPc1	sušenka
<g/>
)	)	kIx)	)
během	během	k7c2	během
období	období	k1gNnSc2	období
karnevalu	karneval	k1gInSc2	karneval
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Colomba	Colomba	k1gFnSc1	Colomba
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dort	dort	k1gInSc1	dort
s	s	k7c7	s
polevou	poleva	k1gFnSc7	poleva
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
holubice	holubice	k1gFnSc2	holubice
<g/>
)	)	kIx)	)
na	na	k7c4	na
Velikonoce	Velikonoce	k1gFnPc4	Velikonoce
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Pane	Pan	k1gMnSc5	Pan
dei	dei	k?	dei
Morti	Morti	k1gNnPc4	Morti
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Chléb	chléb	k1gInSc1	chléb
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
sušenky	sušenka	k1gFnPc4	sušenka
aromatizované	aromatizovaný	k2eAgFnPc4d1	aromatizovaná
skořicí	skořice	k1gFnSc7	skořice
<g/>
)	)	kIx)	)
během	během	k7c2	během
svátku	svátek	k1gInSc2	svátek
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
a	a	k8xC	a
"	"	kIx"	"
<g/>
Panettone	Panetton	k1gInSc5	Panetton
<g/>
"	"	kIx"	"
o	o	k7c6	o
Vánocích	Vánoce	k1gFnPc6	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Salame	Salam	k1gInSc5	Salam
Milano	Milana	k1gFnSc5	Milana
<g/>
,	,	kIx,	,
salám	salám	k1gInSc1	salám
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
jemným	jemný	k2eAgNnSc7d1	jemné
zrnem	zrno	k1gNnSc7	zrno
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgInSc7d3	nejznámější
milánským	milánský	k2eAgInSc7d1	milánský
sýrem	sýr	k1gInSc7	sýr
je	být	k5eAaImIp3nS	být
gorgonzola	gorgonzola	k1gFnSc1	gorgonzola
ze	z	k7c2	z
stejnojmenného	stejnojmenný	k2eAgNnSc2d1	stejnojmenné
nedalekého	daleký	k2eNgNnSc2d1	nedaleké
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
dnes	dnes	k6eAd1	dnes
sídlí	sídlet	k5eAaImIp3nP	sídlet
hlavní	hlavní	k2eAgMnPc1d1	hlavní
výrobci	výrobce	k1gMnPc1	výrobce
gorgonzoly	gorgonzola	k1gFnSc2	gorgonzola
v	v	k7c6	v
Piemontu	Piemont	k1gInSc6	Piemont
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
pocta	pocta	k1gFnSc1	pocta
k	k	k7c3	k
jedinečné	jedinečný	k2eAgFnSc3d1	jedinečná
kuchyni	kuchyně	k1gFnSc3	kuchyně
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
několik	několik	k4yIc1	několik
světově	světově	k6eAd1	světově
proslulých	proslulý	k2eAgFnPc2d1	proslulá
restaurací	restaurace	k1gFnPc2	restaurace
a	a	k8xC	a
kaváren	kavárna	k1gFnPc2	kavárna
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
rafinovanějších	rafinovaný	k2eAgFnPc2d2	rafinovanější
restaurací	restaurace	k1gFnPc2	restaurace
vyšší	vysoký	k2eAgFnSc2d2	vyšší
třídy	třída	k1gFnSc2	třída
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
tradičnější	tradiční	k2eAgMnPc1d2	tradičnější
a	a	k8xC	a
lidovější	lidový	k2eAgMnPc1d2	lidovější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
v	v	k7c6	v
okresech	okres	k1gInPc6	okres
Brera	Brero	k1gNnSc2	Brero
a	a	k8xC	a
Navigli	Navigle	k1gFnSc4	Navigle
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
najdete	najít	k5eAaPmIp2nP	najít
také	také	k9	také
japonskou	japonský	k2eAgFnSc4d1	japonská
restauraci	restaurace	k1gFnSc4	restaurace
Nobu	Nobus	k1gInSc2	Nobus
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
Světě	svět	k1gInSc6	svět
Armani	Arman	k1gMnPc1	Arman
ve	v	k7c6	v
Via	via	k7c4	via
Manzoni	Manzoň	k1gFnSc6	Manzoň
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejmódnějších	módný	k2eAgFnPc2d3	nejmódnější
restaurací	restaurace	k1gFnPc2	restaurace
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvíc	nejvíc	k6eAd1	nejvíc
chic	chic	k6eAd1	chic
kaváren	kavárna	k1gFnPc2	kavárna
a	a	k8xC	a
cukráren	cukrárna	k1gFnPc2	cukrárna
je	být	k5eAaImIp3nS	být
Caffè	Caffè	k1gFnSc1	Caffè
Cova	Cov	k2eAgFnSc1d1	Cov
<g/>
,	,	kIx,	,
starobylá	starobylý	k2eAgFnSc1d1	starobylá
milánská	milánský	k2eAgFnSc1d1	Milánská
kavárna	kavárna	k1gFnSc1	kavárna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1817	[number]	k4	1817
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Teatro	Teatro	k1gNnSc1	Teatro
alla	alla	k6eAd1	alla
Scala	scát	k5eAaImAgFnS	scát
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
franšízy	franšíza	k1gFnPc4	franšíza
také	také	k9	také
v	v	k7c4	v
Hong	Hong	k1gInSc4	Hong
Kongu	Kongo	k1gNnSc3	Kongo
<g/>
.	.	kIx.	.
</s>
<s>
Biffi	Biffi	k1gNnSc1	Biffi
Caffè	Caffè	k1gFnSc2	Caffè
a	a	k8xC	a
Zucca	Zuccum	k1gNnSc2	Zuccum
v	v	k7c4	v
Galleria	Gallerium	k1gNnPc4	Gallerium
jsou	být	k5eAaImIp3nP	být
další	další	k2eAgFnPc4d1	další
známé	známý	k2eAgFnPc4d1	známá
a	a	k8xC	a
historické	historický	k2eAgFnPc4d1	historická
kavárny	kavárna	k1gFnPc4	kavárna
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
restaurace	restaurace	k1gFnPc4	restaurace
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
patří	patřit	k5eAaImIp3nS	patřit
restaurace	restaurace	k1gFnSc1	restaurace
hotelu	hotel	k1gInSc2	hotel
Four	Foura	k1gFnPc2	Foura
Seasons	Seasons	k1gInSc1	Seasons
<g/>
,	,	kIx,	,
La	la	k1gNnPc1	la
Briciola	Briciola	k1gFnSc1	Briciola
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
Marino	Marina	k1gFnSc5	Marina
alla	all	k1gInSc2	all
Scala	scát	k5eAaImAgFnS	scát
a	a	k8xC	a
Chandelier	Chandelier	k1gInSc4	Chandelier
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
najdete	najít	k5eAaPmIp2nP	najít
v	v	k7c4	v
Galleria	Gallerium	k1gNnPc4	Gallerium
Vittorio	Vittorio	k1gMnSc1	Vittorio
Emanuele	Emanuela	k1gFnSc3	Emanuela
II	II	kA	II
také	také	k9	také
McDonald	McDonald	k1gInSc1	McDonald
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
a	a	k8xC	a
několik	několik	k4yIc1	několik
nových	nový	k2eAgFnPc2d1	nová
butikových	butikův	k2eAgFnPc2d1	butikův
kaváren	kavárna	k1gFnPc2	kavárna
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Just	just	k6eAd1	just
Cavalli	Cavalle	k1gFnSc4	Cavalle
Café	café	k1gNnSc2	café
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
vlastníkem	vlastník	k1gMnSc7	vlastník
je	být	k5eAaImIp3nS	být
luxusní	luxusní	k2eAgFnSc1d1	luxusní
módní	módní	k2eAgFnSc1d1	módní
značka	značka	k1gFnSc1	značka
Roberto	Roberta	k1gFnSc5	Roberta
Cavalli	Cavall	k1gMnSc6	Cavall
<g/>
.	.	kIx.	.
</s>
<s>
AC	AC	kA	AC
Milan	Milan	k1gMnSc1	Milan
FC	FC	kA	FC
Internazionale	Internazionale	k1gMnSc1	Internazionale
Milano	Milana	k1gFnSc5	Milana
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
v	v	k7c4	v
tis	tis	k1gInSc4	tis
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Betlém	Betlém	k1gInSc1	Betlém
<g/>
,	,	kIx,	,
Palestina	Palestina	k1gFnSc1	Palestina
Birmingham	Birmingham	k1gInSc1	Birmingham
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
Krakov	Krakov	k1gInSc1	Krakov
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Dakar	Dakar	k1gInSc1	Dakar
<g/>
,	,	kIx,	,
Senegal	Senegal	k1gInSc1	Senegal
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Lyon	Lyon	k1gInSc1	Lyon
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Melbourne	Melbourne	k1gNnSc2	Melbourne
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
Ósaka	Ósaka	k1gFnSc1	Ósaka
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
Sã	Sã	k1gFnSc2	Sã
Paulo	Paula	k1gFnSc5	Paula
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
Šanghaj	Šanghaj	k1gFnSc1	Šanghaj
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
Tel	tel	kA	tel
Aviv	Aviv	k1gInSc4	Aviv
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc4	Izrael
Toronto	Toronto	k1gNnSc4	Toronto
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
</s>
