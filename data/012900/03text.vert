<p>
<s>
Hybridní	hybridní	k2eAgInSc1d1	hybridní
pohon	pohon	k1gInSc1	pohon
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
kombinaci	kombinace	k1gFnSc4	kombinace
několika	několik	k4yIc2	několik
zdrojů	zdroj	k1gInPc2	zdroj
energie	energie	k1gFnSc2	energie
pro	pro	k7c4	pro
pohon	pohon	k1gInSc4	pohon
jednoho	jeden	k4xCgInSc2	jeden
dopravního	dopravní	k2eAgInSc2d1	dopravní
prostředku	prostředek	k1gInSc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
myslí	myslet	k5eAaImIp3nS	myslet
kombinace	kombinace	k1gFnPc4	kombinace
elektrické	elektrický	k2eAgFnPc4d1	elektrická
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
trakce	trakce	k1gFnPc4	trakce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
hybridního	hybridní	k2eAgInSc2d1	hybridní
automobilu	automobil	k1gInSc2	automobil
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
kombinaci	kombinace	k1gFnSc4	kombinace
elektromotoru	elektromotor	k1gInSc2	elektromotor
a	a	k8xC	a
spalovacího	spalovací	k2eAgInSc2d1	spalovací
motoru	motor	k1gInSc2	motor
<g/>
.	.	kIx.	.
</s>
<s>
Hybridní	hybridní	k2eAgInPc1d1	hybridní
pohony	pohon	k1gInPc1	pohon
jsou	být	k5eAaImIp3nP	být
využívány	využívat	k5eAaImNgInP	využívat
především	především	k9	především
v	v	k7c6	v
silniční	silniční	k2eAgFnSc6d1	silniční
a	a	k8xC	a
železniční	železniční	k2eAgFnSc6d1	železniční
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhy	druh	k1gInPc1	druh
hybridních	hybridní	k2eAgInPc2d1	hybridní
pohonů	pohon	k1gInPc2	pohon
==	==	k?	==
</s>
</p>
<p>
<s>
Automobil	automobil	k1gInSc1	automobil
s	s	k7c7	s
hybridním	hybridní	k2eAgInSc7d1	hybridní
pohonem	pohon	k1gInSc7	pohon
představuje	představovat	k5eAaImIp3nS	představovat
vozidlo	vozidlo	k1gNnSc1	vozidlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
při	při	k7c6	při
svém	svůj	k3xOyFgInSc6	svůj
pohonu	pohon	k1gInSc6	pohon
využívá	využívat	k5eAaImIp3nS	využívat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
zdroj	zdroj	k1gInSc4	zdroj
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Hybridní	hybridní	k2eAgInPc1d1	hybridní
pohony	pohon	k1gInPc1	pohon
využívají	využívat	k5eAaImIp3nP	využívat
především	především	k6eAd1	především
výhod	výhoda	k1gFnPc2	výhoda
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
pohonů	pohon	k1gInPc2	pohon
při	při	k7c6	při
různých	různý	k2eAgInPc6d1	různý
pracovních	pracovní	k2eAgInPc6d1	pracovní
stavech	stav	k1gInPc6	stav
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
testují	testovat	k5eAaImIp3nP	testovat
a	a	k8xC	a
vyvíjejí	vyvíjet	k5eAaImIp3nP	vyvíjet
tyto	tento	k3xDgInPc1	tento
druhy	druh	k1gInPc1	druh
hybridních	hybridní	k2eAgInPc2d1	hybridní
pohonů	pohon	k1gInPc2	pohon
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
spalovací	spalovací	k2eAgInSc1d1	spalovací
motor	motor	k1gInSc1	motor
+	+	kIx~	+
elektromotor	elektromotor	k1gInSc1	elektromotor
+	+	kIx~	+
akumulátor	akumulátor	k1gInSc1	akumulátor
</s>
</p>
<p>
<s>
spalovací	spalovací	k2eAgInSc4d1	spalovací
motor	motor	k1gInSc4	motor
+	+	kIx~	+
elektromotor	elektromotor	k1gInSc4	elektromotor
+	+	kIx~	+
externí	externí	k2eAgInSc4d1	externí
přívod	přívod	k1gInSc4	přívod
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
trolej	trolej	k1gInSc1	trolej
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
spalovací	spalovací	k2eAgInSc1d1	spalovací
motor	motor	k1gInSc1	motor
+	+	kIx~	+
setrvačník	setrvačník	k1gInSc1	setrvačník
</s>
</p>
<p>
<s>
plynová	plynový	k2eAgFnSc1d1	plynová
turbína	turbína	k1gFnSc1	turbína
+	+	kIx~	+
generátor	generátor	k1gInSc1	generátor
+	+	kIx~	+
akumulátor	akumulátor	k1gInSc1	akumulátor
+	+	kIx~	+
elektromotor	elektromotor	k1gInSc1	elektromotor
</s>
</p>
<p>
<s>
lidská	lidský	k2eAgFnSc1d1	lidská
síla	síla	k1gFnSc1	síla
+	+	kIx~	+
elektromotor	elektromotor	k1gInSc1	elektromotor
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Twike	Twike	k1gFnSc1	Twike
nebo	nebo	k8xC	nebo
elektrokola	elektrokola	k1gFnSc1	elektrokola
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Funkce	funkce	k1gFnPc4	funkce
sériového	sériový	k2eAgInSc2d1	sériový
hybridního	hybridní	k2eAgInSc2d1	hybridní
pohonu	pohon	k1gInSc2	pohon
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
již	již	k6eAd1	již
ověřené	ověřený	k2eAgInPc4d1	ověřený
hybridní	hybridní	k2eAgInPc4d1	hybridní
pohony	pohon	k1gInPc4	pohon
patří	patřit	k5eAaImIp3nS	patřit
kombinace	kombinace	k1gFnSc1	kombinace
spalovacího	spalovací	k2eAgInSc2d1	spalovací
motoru	motor	k1gInSc2	motor
s	s	k7c7	s
elektromotorem	elektromotor	k1gInSc7	elektromotor
a	a	k8xC	a
akumulátorem	akumulátor	k1gInSc7	akumulátor
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
sériový	sériový	k2eAgInSc1d1	sériový
hybridní	hybridní	k2eAgInSc1d1	hybridní
pohon	pohon	k1gInSc1	pohon
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
sériový	sériový	k2eAgInSc1d1	sériový
pohon	pohon	k1gInSc1	pohon
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
jízdě	jízda	k1gFnSc6	jízda
na	na	k7c6	na
krátké	krátký	k2eAgFnSc6d1	krátká
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
jízda	jízda	k1gFnSc1	jízda
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
při	při	k7c6	při
rovnoměrné	rovnoměrný	k2eAgFnSc6d1	rovnoměrná
jízdě	jízda	k1gFnSc6	jízda
poháněn	pohánět	k5eAaImNgInS	pohánět
stejnosměrným	stejnosměrný	k2eAgInSc7d1	stejnosměrný
točivým	točivý	k2eAgInSc7d1	točivý
strojem	stroj	k1gInSc7	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Stroj	stroj	k1gInSc1	stroj
se	se	k3xPyFc4	se
napájí	napájet	k5eAaImIp3nS	napájet
jako	jako	k9	jako
elektromotor	elektromotor	k1gInSc4	elektromotor
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
energií	energie	k1gFnSc7	energie
z	z	k7c2	z
akumulátoru	akumulátor	k1gInSc2	akumulátor
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vozidle	vozidlo	k1gNnSc6	vozidlo
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc4	dva
spojky	spojka	k1gFnPc4	spojka
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
jen	jen	k9	jen
S	s	k7c7	s
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
spojuje	spojovat	k5eAaImIp3nS	spojovat
spalovací	spalovací	k2eAgInSc1d1	spalovací
motor	motor	k1gInSc1	motor
s	s	k7c7	s
elektromotorem	elektromotor	k1gInSc7	elektromotor
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
(	(	kIx(	(
<g/>
dále	daleko	k6eAd2	daleko
jen	jen	k9	jen
S	s	k7c7	s
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
elektromotor	elektromotor	k1gInSc4	elektromotor
s	s	k7c7	s
převodovkou	převodovka	k1gFnSc7	převodovka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jízdě	jízda	k1gFnSc6	jízda
na	na	k7c4	na
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
energii	energie	k1gFnSc4	energie
je	být	k5eAaImIp3nS	být
spojka	spojka	k1gFnSc1	spojka
S	s	k7c7	s
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
připojuje	připojovat	k5eAaImIp3nS	připojovat
spalovací	spalovací	k2eAgInSc4d1	spalovací
motor	motor	k1gInSc4	motor
rozpojená	rozpojený	k2eAgFnSc1d1	rozpojená
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jízdě	jízda	k1gFnSc6	jízda
na	na	k7c4	na
delší	dlouhý	k2eAgFnSc4d2	delší
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
např.	např.	kA	např.
mimo	mimo	k7c4	mimo
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
při	při	k7c6	při
potřebě	potřeba	k1gFnSc6	potřeba
větší	veliký	k2eAgFnSc2d2	veliký
akcelerace	akcelerace	k1gFnSc2	akcelerace
nebo	nebo	k8xC	nebo
při	při	k7c6	při
plném	plný	k2eAgNnSc6d1	plné
zatížení	zatížení	k1gNnSc6	zatížení
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
pohon	pohon	k1gInSc1	pohon
spalovací	spalovací	k2eAgInSc1d1	spalovací
motor	motor	k1gInSc1	motor
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
vozidlo	vozidlo	k1gNnSc1	vozidlo
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
silou	síla	k1gFnSc7	síla
spalovacího	spalovací	k2eAgInSc2d1	spalovací
motoru	motor	k1gInSc2	motor
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
výkon	výkon	k1gInSc1	výkon
přenáší	přenášet	k5eAaImIp3nS	přenášet
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
spojky	spojka	k1gFnSc2	spojka
S1	S1	k1gFnSc2	S1
a	a	k8xC	a
S2	S2	k1gFnSc2	S2
na	na	k7c4	na
převodovku	převodovka	k1gFnSc4	převodovka
<g/>
.	.	kIx.	.
</s>
<s>
Stejnosměrný	stejnosměrný	k2eAgInSc1d1	stejnosměrný
elektrický	elektrický	k2eAgInSc1d1	elektrický
točivý	točivý	k2eAgInSc1d1	točivý
stroj	stroj	k1gInSc1	stroj
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
chvíli	chvíle	k1gFnSc6	chvíle
mění	měnit	k5eAaImIp3nS	měnit
svojí	svůj	k3xOyFgFnSc3	svůj
funkci	funkce	k1gFnSc3	funkce
a	a	k8xC	a
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
generátor	generátor	k1gInSc1	generátor
stejnosměrného	stejnosměrný	k2eAgInSc2d1	stejnosměrný
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
získaná	získaný	k2eAgFnSc1d1	získaná
elektrická	elektrický	k2eAgFnSc1d1	elektrická
energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
přivedena	přivést	k5eAaPmNgFnS	přivést
do	do	k7c2	do
akumulátoru	akumulátor	k1gInSc2	akumulátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
brždění	bržděný	k2eAgMnPc1d1	bržděný
se	se	k3xPyFc4	se
rozpojí	rozpojit	k5eAaPmIp3nP	rozpojit
spojka	spojka	k1gFnSc1	spojka
S	s	k7c7	s
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
odpojen	odpojit	k5eAaPmNgInS	odpojit
spalovací	spalovací	k2eAgInSc1d1	spalovací
motor	motor	k1gInSc1	motor
a	a	k8xC	a
generátor	generátor	k1gInSc1	generátor
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
energii	energie	k1gFnSc4	energie
pro	pro	k7c4	pro
dobíjení	dobíjení	k1gNnSc4	dobíjení
akumulátoru	akumulátor	k1gInSc2	akumulátor
ze	z	k7c2	z
setrvačné	setrvačný	k2eAgFnSc2d1	setrvačná
energie	energie	k1gFnSc2	energie
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
hybridního	hybridní	k2eAgInSc2d1	hybridní
pohonu	pohon	k1gInSc2	pohon
lze	lze	k6eAd1	lze
využívat	využívat	k5eAaPmF	využívat
výhody	výhoda	k1gFnPc4	výhoda
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
pohonů	pohon	k1gInPc2	pohon
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
elektropohonu	elektropohon	k1gInSc2	elektropohon
nízkou	nízký	k2eAgFnSc4d1	nízká
hlučnost	hlučnost	k1gFnSc4	hlučnost
<g/>
,	,	kIx,	,
žádné	žádný	k3yNgFnPc4	žádný
výfukové	výfukový	k2eAgFnPc4d1	výfuková
zplodiny	zplodina	k1gFnPc4	zplodina
a	a	k8xC	a
vysokou	vysoký	k2eAgFnSc4d1	vysoká
účinnost	účinnost	k1gFnSc4	účinnost
elektromotoru	elektromotor	k1gInSc2	elektromotor
(	(	kIx(	(
<g/>
asi	asi	k9	asi
90	[number]	k4	90
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pohonu	pohon	k1gInSc2	pohon
spalovacím	spalovací	k2eAgInSc7d1	spalovací
motorem	motor	k1gInSc7	motor
velký	velký	k2eAgInSc4d1	velký
dojezd	dojezd	k1gInSc4	dojezd
vozidla	vozidlo	k1gNnSc2	vozidlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc6d2	vyšší
oblasti	oblast	k1gFnSc6	oblast
otáček	otáčka	k1gFnPc2	otáčka
vysoký	vysoký	k2eAgInSc1d1	vysoký
točivý	točivý	k2eAgInSc1d1	točivý
moment	moment	k1gInSc1	moment
a	a	k8xC	a
možnost	možnost	k1gFnSc1	možnost
jízdy	jízda	k1gFnSc2	jízda
vysokou	vysoký	k2eAgFnSc7d1	vysoká
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgFnSc4d3	veliký
výhodu	výhoda	k1gFnSc4	výhoda
tohoto	tento	k3xDgInSc2	tento
kombinovaného	kombinovaný	k2eAgInSc2d1	kombinovaný
pohonu	pohon	k1gInSc2	pohon
patří	patřit	k5eAaImIp3nS	patřit
možnost	možnost	k1gFnSc4	možnost
využití	využití	k1gNnSc2	využití
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
pohonů	pohon	k1gInPc2	pohon
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
účinnosti	účinnost	k1gFnSc2	účinnost
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
snížení	snížení	k1gNnSc1	snížení
spotřeby	spotřeba	k1gFnSc2	spotřeba
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
pohonu	pohon	k1gInSc2	pohon
jsou	být	k5eAaImIp3nP	být
vysoké	vysoký	k2eAgInPc1d1	vysoký
pořizovací	pořizovací	k2eAgInPc1d1	pořizovací
náklady	náklad	k1gInPc1	náklad
<g/>
,	,	kIx,	,
zvýšení	zvýšení	k1gNnSc1	zvýšení
hmotnosti	hmotnost	k1gFnSc2	hmotnost
vozidla	vozidlo	k1gNnSc2	vozidlo
o	o	k7c4	o
hmotnost	hmotnost	k1gFnSc4	hmotnost
akumulátoru	akumulátor	k1gInSc2	akumulátor
a	a	k8xC	a
zmenšení	zmenšení	k1gNnSc2	zmenšení
úložných	úložný	k2eAgFnPc2d1	úložná
prostor	prostora	k1gFnPc2	prostora
v	v	k7c6	v
vozidle	vozidlo	k1gNnSc6	vozidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
vývoje	vývoj	k1gInSc2	vývoj
hybridního	hybridní	k2eAgInSc2d1	hybridní
pohonu	pohon	k1gInSc2	pohon
investuje	investovat	k5eAaBmIp3nS	investovat
například	například	k6eAd1	například
japonská	japonský	k2eAgFnSc1d1	japonská
Toyota	toyota	k1gFnSc1	toyota
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
prodávat	prodávat	k5eAaImF	prodávat
hybridní	hybridní	k2eAgInPc4d1	hybridní
vozy	vůz	k1gInPc4	vůz
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
představila	představit	k5eAaPmAgFnS	představit
tato	tento	k3xDgFnSc1	tento
automobilka	automobilka	k1gFnSc1	automobilka
na	na	k7c6	na
autosalonu	autosalon	k1gInSc6	autosalon
v	v	k7c6	v
Detroitu	Detroit	k1gInSc6	Detroit
již	již	k9	již
třetí	třetí	k4xOgFnSc4	třetí
generaci	generace	k1gFnSc4	generace
hybridního	hybridní	k2eAgInSc2d1	hybridní
vozu	vůz	k1gInSc2	vůz
Toyota	toyota	k1gFnSc1	toyota
Prius	prius	k1gNnSc4	prius
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
je	být	k5eAaImIp3nS	být
výkonnější	výkonný	k2eAgMnSc1d2	výkonnější
<g/>
,	,	kIx,	,
úspornější	úsporný	k2eAgMnSc1d2	úspornější
a	a	k8xC	a
tišší	tichý	k2eAgMnSc1d2	tišší
než	než	k8xS	než
jeho	jeho	k3xOp3gMnPc7	jeho
předchůdci	předchůdce	k1gMnPc7	předchůdce
<g/>
.	.	kIx.	.
</s>
<s>
Kombinovaná	kombinovaný	k2eAgFnSc1d1	kombinovaná
spotřeba	spotřeba	k1gFnSc1	spotřeba
tohoto	tento	k3xDgInSc2	tento
vozu	vůz	k1gInSc2	vůz
je	být	k5eAaImIp3nS	být
3,9	[number]	k4	3,9
l	l	kA	l
na	na	k7c4	na
100	[number]	k4	100
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Elektromotor	elektromotor	k1gInSc1	elektromotor
má	mít	k5eAaImIp3nS	mít
výkon	výkon	k1gInSc4	výkon
60	[number]	k4	60
kW	kW	kA	kW
<g/>
,	,	kIx,	,
spalovací	spalovací	k2eAgInSc1d1	spalovací
motor	motor	k1gInSc1	motor
pak	pak	k6eAd1	pak
73	[number]	k4	73
kW	kW	kA	kW
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
výkon	výkon	k1gInSc1	výkon
hybridního	hybridní	k2eAgInSc2d1	hybridní
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
136	[number]	k4	136
koní	kůň	k1gMnPc2	kůň
(	(	kIx(	(
<g/>
100	[number]	k4	100
kW	kW	kA	kW
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nuly	nula	k1gFnSc2	nula
na	na	k7c4	na
sto	sto	k4xCgNnSc4	sto
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
za	za	k7c4	za
10,4	[number]	k4	10,4
sekund	sekunda	k1gFnPc2	sekunda
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typická	typický	k2eAgNnPc1d1	typické
hybridní	hybridní	k2eAgNnPc1d1	hybridní
vozidla	vozidlo	k1gNnPc1	vozidlo
==	==	k?	==
</s>
</p>
<p>
<s>
elektrická	elektrický	k2eAgFnSc1d1	elektrická
posunovací	posunovací	k2eAgFnSc1d1	posunovací
lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
s	s	k7c7	s
bateriovým	bateriový	k2eAgInSc7d1	bateriový
vozem	vůz	k1gInSc7	vůz
–	–	k?	–
Několik	několik	k4yIc1	několik
v	v	k7c6	v
ČR	ČR	kA	ČR
běžných	běžný	k2eAgFnPc2d1	běžná
elektrických	elektrický	k2eAgFnPc2d1	elektrická
posunovacích	posunovací	k2eAgFnPc2d1	posunovací
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
řady	řada	k1gFnSc2	řada
210	[number]	k4	210
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
depu	depo	k1gNnSc6	depo
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
přestavěno	přestavěn	k2eAgNnSc4d1	přestavěno
pro	pro	k7c4	pro
možnost	možnost	k1gFnSc4	možnost
posunu	posun	k1gInSc2	posun
mimo	mimo	k7c4	mimo
trolejové	trolejový	k2eAgNnSc4d1	trolejové
vedení	vedení	k1gNnSc4	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Lokomotivy	lokomotiva	k1gFnPc1	lokomotiva
tak	tak	k9	tak
mohou	moct	k5eAaImIp3nP	moct
posunovat	posunovat	k5eAaImF	posunovat
i	i	k9	i
na	na	k7c6	na
některých	některý	k3yIgFnPc6	některý
kolejích	kolej	k1gFnPc6	kolej
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
trolejí	trolej	k1gFnSc7	trolej
vybaveny	vybaven	k2eAgInPc1d1	vybaven
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
mohou	moct	k5eAaImIp3nP	moct
zajíždět	zajíždět	k5eAaImF	zajíždět
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
vlečky	vlečka	k1gFnPc4	vlečka
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
do	do	k7c2	do
blízkosti	blízkost	k1gFnSc2	blízkost
oněch	onen	k3xDgFnPc2	onen
vleček	vlečka	k1gFnPc2	vlečka
trolej	trolej	k1gFnSc1	trolej
vede	vést	k5eAaImIp3nS	vést
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
zbytečné	zbytečný	k2eAgNnSc1d1	zbytečné
<g/>
,	,	kIx,	,
posílat	posílat	k5eAaImF	posílat
tam	tam	k6eAd1	tam
motorovou	motorový	k2eAgFnSc4d1	motorová
lokomotivu	lokomotiva	k1gFnSc4	lokomotiva
<g/>
.	.	kIx.	.
</s>
<s>
Upravená	upravený	k2eAgFnSc1d1	upravená
posunovací	posunovací	k2eAgFnSc1d1	posunovací
lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
tedy	tedy	k9	tedy
buďto	buďto	k8xC	buďto
jede	jet	k5eAaImIp3nS	jet
v	v	k7c6	v
režimu	režim	k1gInSc6	režim
"	"	kIx"	"
<g/>
trolej	trolej	k1gFnSc1	trolej
<g/>
"	"	kIx"	"
a	a	k8xC	a
odebírá	odebírat	k5eAaImIp3nS	odebírat
proud	proud	k1gInSc4	proud
sběračem	sběrač	k1gInSc7	sběrač
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
režimu	režim	k1gInSc6	režim
"	"	kIx"	"
<g/>
akumulátory	akumulátor	k1gInPc7	akumulátor
<g/>
"	"	kIx"	"
a	a	k8xC	a
odebírá	odebírat	k5eAaImIp3nS	odebírat
proud	proud	k1gInSc1	proud
z	z	k7c2	z
akumulátorů	akumulátor	k1gInPc2	akumulátor
v	v	k7c6	v
připojeném	připojený	k2eAgInSc6d1	připojený
vagóně	vagón	k1gInSc6	vagón
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
hybridní	hybridní	k2eAgInSc1d1	hybridní
trolejbus	trolejbus	k1gInSc1	trolejbus
(	(	kIx(	(
<g/>
duobus	duobus	k1gInSc1	duobus
<g/>
)	)	kIx)	)
–	–	k?	–
s	s	k7c7	s
dieselelektrickým	dieselelektrický	k2eAgInSc7d1	dieselelektrický
agregátem	agregát	k1gInSc7	agregát
<g/>
,	,	kIx,	,
s	s	k7c7	s
akumulátorem	akumulátor	k1gInSc7	akumulátor
nebo	nebo	k8xC	nebo
s	s	k7c7	s
nezávislým	závislý	k2eNgInSc7d1	nezávislý
dieselovým	dieselový	k2eAgInSc7d1	dieselový
motorem	motor	k1gInSc7	motor
</s>
</p>
<p>
<s>
vodíkový	vodíkový	k2eAgInSc1d1	vodíkový
hybridní	hybridní	k2eAgInSc1d1	hybridní
autobus	autobus	k1gInSc1	autobus
–	–	k?	–
s	s	k7c7	s
palivovým	palivový	k2eAgInSc7d1	palivový
článkem	článek	k1gInSc7	článek
<g/>
,	,	kIx,	,
bateriemi	baterie	k1gFnPc7	baterie
a	a	k8xC	a
případně	případně	k6eAd1	případně
ultrakapacitory	ultrakapacitor	k1gInPc4	ultrakapacitor
pro	pro	k7c4	pro
pokrývání	pokrývání	k1gNnSc4	pokrývání
proudových	proudový	k2eAgFnPc2d1	proudová
špiček	špička	k1gFnPc2	špička
</s>
</p>
<p>
<s>
hybridní	hybridní	k2eAgInSc1d1	hybridní
automobil	automobil	k1gInSc1	automobil
–	–	k?	–
pohon	pohon	k1gInSc1	pohon
obstarává	obstarávat	k5eAaImIp3nS	obstarávat
spalovací	spalovací	k2eAgInSc1d1	spalovací
motor	motor	k1gInSc1	motor
a	a	k8xC	a
elektromotor	elektromotor	k1gInSc1	elektromotor
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Spalovací	spalovací	k2eAgInSc1d1	spalovací
motor	motor	k1gInSc1	motor
</s>
</p>
<p>
<s>
Elektromobil	elektromobil	k1gInSc1	elektromobil
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hybridní	hybridní	k2eAgInSc4d1	hybridní
pohon	pohon	k1gInSc4	pohon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Stránka	stránka	k1gFnSc1	stránka
zabývající	zabývající	k2eAgMnPc4d1	zabývající
se	se	k3xPyFc4	se
hybridními	hybridní	k2eAgFnPc7d1	hybridní
vozidly	vozidlo	k1gNnPc7	vozidlo
</s>
</p>
<p>
<s>
Hybridní	hybridní	k2eAgInPc1d1	hybridní
automobily	automobil	k1gInPc1	automobil
na	na	k7c4	na
Koncepty	koncept	k1gInPc4	koncept
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
Hybridním	hybridní	k2eAgInSc6d1	hybridní
pohonu	pohon	k1gInSc6	pohon
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Hybridní	hybridní	k2eAgInPc1d1	hybridní
pohony	pohon	k1gInPc1	pohon
budou	být	k5eAaImBp3nP	být
automobilovou	automobilový	k2eAgFnSc7d1	automobilová
revolucí	revoluce	k1gFnSc7	revoluce
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
článek	článek	k1gInSc1	článek
o	o	k7c6	o
Toyotě	toyota	k1gFnSc6	toyota
Prius	prius	k1gNnSc2	prius
na	na	k7c4	na
www.nazeleno.cz	www.nazeleno.cz	k1gInSc4	www.nazeleno.cz
</s>
</p>
