<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
byl	být	k5eAaImAgInS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
v	v	k7c6	v
listnatých	listnatý	k2eAgInPc6d1	listnatý
lesích	les	k1gInPc6	les
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
však	však	k9	však
jeho	jeho	k3xOp3gInSc1	jeho
výskyt	výskyt	k1gInSc1	výskyt
lokalizován	lokalizovat	k5eAaBmNgInS	lokalizovat
do	do	k7c2	do
několika	několik	k4yIc2	několik
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
