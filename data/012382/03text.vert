<p>
<s>
Roháč	roháč	k1gInSc1	roháč
obecný	obecný	k2eAgInSc1d1	obecný
(	(	kIx(	(
<g/>
Lucanus	Lucanus	k1gInSc1	Lucanus
cervus	cervus	k1gInSc1	cervus
(	(	kIx(	(
<g/>
Linné	Linné	k1gNnSc1	Linné
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
))	))	k?	))
je	být	k5eAaImIp3nS	být
nejznámější	známý	k2eAgMnSc1d3	nejznámější
představitel	představitel	k1gMnSc1	představitel
brouků	brouk	k1gMnPc2	brouk
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
roháčovitých	roháčovití	k1gMnPc2	roháčovití
a	a	k8xC	a
největší	veliký	k2eAgMnSc1d3	veliký
brouk	brouk	k1gMnSc1	brouk
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
starých	starý	k2eAgInPc2d1	starý
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
v	v	k7c6	v
mrtvých	mrtvý	k2eAgInPc6d1	mrtvý
pařezech	pařez	k1gInPc6	pařez
v	v	k7c6	v
lesích	les	k1gInPc6	les
a	a	k8xC	a
hájích	háj	k1gInPc6	háj
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
odstraňováním	odstraňování	k1gNnSc7	odstraňování
starých	starý	k2eAgInPc2d1	starý
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
pařezů	pařez	k1gInPc2	pařez
mizí	mizet	k5eAaImIp3nS	mizet
i	i	k9	i
přirozené	přirozený	k2eAgNnSc1d1	přirozené
prostředí	prostředí	k1gNnSc1	prostředí
a	a	k8xC	a
zdroj	zdroj	k1gInSc1	zdroj
obživy	obživa	k1gFnSc2	obživa
tohoto	tento	k3xDgMnSc2	tento
brouka	brouk	k1gMnSc2	brouk
<g/>
.	.	kIx.	.
</s>
<s>
Snižuje	snižovat	k5eAaImIp3nS	snižovat
se	se	k3xPyFc4	se
tak	tak	k9	tak
populace	populace	k1gFnSc1	populace
roháče	roháč	k1gMnSc2	roháč
obecného	obecný	k2eAgMnSc2d1	obecný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ostatních	ostatní	k2eAgInPc2d1	ostatní
druhů	druh	k1gInPc2	druh
brouků	brouk	k1gMnPc2	brouk
žijících	žijící	k2eAgInPc2d1	žijící
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
prostředí	prostředí	k1gNnSc6	prostředí
a	a	k8xC	a
roháč	roháč	k1gInSc1	roháč
obecný	obecný	k2eAgInSc1d1	obecný
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostává	dostávat	k5eAaImIp3nS	dostávat
na	na	k7c4	na
světový	světový	k2eAgInSc4d1	světový
seznam	seznam	k1gInSc4	seznam
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
byl	být	k5eAaImAgInS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
v	v	k7c6	v
listnatých	listnatý	k2eAgInPc6d1	listnatý
lesích	les	k1gInPc6	les
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
však	však	k9	však
jeho	jeho	k3xOp3gInSc1	jeho
výskyt	výskyt	k1gInSc1	výskyt
lokalizován	lokalizovat	k5eAaBmNgInS	lokalizovat
do	do	k7c2	do
několika	několik	k4yIc2	několik
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Nejhojněji	hojně	k6eAd3	hojně
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biologie	biologie	k1gFnSc2	biologie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Larva	larva	k1gFnSc1	larva
===	===	k?	===
</s>
</p>
<p>
<s>
Larvy	larva	k1gFnPc1	larva
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
tvar	tvar	k1gInSc4	tvar
podobný	podobný	k2eAgInSc4d1	podobný
písmeni	písmeno	k1gNnSc6	písmeno
"	"	kIx"	"
<g/>
C	C	kA	C
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
slepé	slepý	k2eAgInPc1d1	slepý
a	a	k8xC	a
živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
rozkládajícím	rozkládající	k2eAgMnSc7d1	rozkládající
se	se	k3xPyFc4	se
dřevem	dřevo	k1gNnSc7	dřevo
pařezů	pařez	k1gInPc2	pařez
<g/>
,	,	kIx,	,
starých	starý	k2eAgInPc2d1	starý
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
keřů	keř	k1gInPc2	keř
<g/>
,	,	kIx,	,
hnijících	hnijící	k2eAgInPc2d1	hnijící
kůlů	kůl	k1gInPc2	kůl
plotů	plot	k1gInPc2	plot
v	v	k7c6	v
kompostových	kompostový	k2eAgFnPc6d1	kompostová
hromadách	hromada	k1gFnPc6	hromada
a	a	k8xC	a
listovkách	listovka	k1gFnPc6	listovka
<g/>
.	.	kIx.	.
</s>
<s>
Larvy	larva	k1gFnPc1	larva
mají	mít	k5eAaImIp3nP	mít
měkké	měkký	k2eAgFnPc1d1	měkká
<g/>
,	,	kIx,	,
krémově	krémově	k6eAd1	krémově
zbarvené	zbarvený	k2eAgNnSc4d1	zbarvené
průsvitné	průsvitný	k2eAgNnSc4d1	průsvitné
tělo	tělo	k1gNnSc4	tělo
se	s	k7c7	s
šesti	šest	k4xCc7	šest
oranžově	oranžově	k6eAd1	oranžově
zabarvenýma	zabarvený	k2eAgFnPc7d1	zabarvená
nohama	noha	k1gFnPc7	noha
a	a	k8xC	a
oranžovou	oranžový	k2eAgFnSc7d1	oranžová
hlavou	hlava	k1gFnSc7	hlava
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
vyjímají	vyjímat	k5eAaImIp3nP	vyjímat
ostrá	ostrý	k2eAgNnPc1d1	ostré
<g/>
,	,	kIx,	,
hnědě	hnědě	k6eAd1	hnědě
zbarvená	zbarvený	k2eAgNnPc4d1	zbarvené
kusadla	kusadla	k1gNnPc4	kusadla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
nohách	noha	k1gFnPc6	noha
mají	mít	k5eAaImIp3nP	mít
larvy	larva	k1gFnPc1	larva
kartáčky	kartáček	k1gInPc4	kartáček
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
komunikaci	komunikace	k1gFnSc4	komunikace
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
stridulaci	stridulace	k1gFnSc6	stridulace
<g/>
)	)	kIx)	)
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
larvami	larva	k1gFnPc7	larva
stejného	stejný	k2eAgInSc2d1	stejný
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Larva	larva	k1gFnSc1	larva
prochází	procházet	k5eAaImIp3nS	procházet
několika	několik	k4yIc7	několik
fázemi	fáze	k1gFnPc7	fáze
vývoje	vývoj	k1gInSc2	vývoj
a	a	k8xC	a
po	po	k7c6	po
4	[number]	k4	4
až	až	k8xS	až
6	[number]	k4	6
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
zakukluje	zakuklovat	k5eAaImIp3nS	zakuklovat
<g/>
.	.	kIx.	.
</s>
<s>
Entomolog	entomolog	k1gMnSc1	entomolog
Charlie	Charlie	k1gMnSc1	Charlie
Morgan	morgan	k1gMnSc1	morgan
koncem	koncem	k7c2	koncem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
objevil	objevit	k5eAaPmAgMnS	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kukla	kukla	k1gFnSc1	kukla
roháče	roháč	k1gInSc2	roháč
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
se	se	k3xPyFc4	se
vylíhne	vylíhnout	k5eAaPmIp3nS	vylíhnout
dospělý	dospělý	k1gMnSc1	dospělý
brouk	brouk	k1gMnSc1	brouk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
odlétá	odlétat	k5eAaImIp3nS	odlétat
pářit	pářit	k5eAaImF	pářit
s	s	k7c7	s
jedincem	jedinec	k1gMnSc7	jedinec
opačného	opačný	k2eAgNnSc2d1	opačné
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dospělci	dospělec	k1gMnPc5	dospělec
===	===	k?	===
</s>
</p>
<p>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
jedinci	jedinec	k1gMnPc1	jedinec
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
od	od	k7c2	od
konce	konec	k1gInSc2	konec
května	květen	k1gInSc2	květen
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
nejvíce	hodně	k6eAd3	hodně
aktivní	aktivní	k2eAgInSc4d1	aktivní
ve	v	k7c6	v
večerních	večerní	k2eAgFnPc6d1	večerní
hodinách	hodina	k1gFnPc6	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Samičky	samička	k1gFnPc1	samička
roháče	roháč	k1gInSc2	roháč
kladou	klást	k5eAaImIp3nP	klást
svá	svůj	k3xOyFgNnPc4	svůj
vajíčka	vajíčko	k1gNnPc4	vajíčko
do	do	k7c2	do
rozkládajícího	rozkládající	k2eAgNnSc2d1	rozkládající
se	se	k3xPyFc4	se
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
brouci	brouk	k1gMnPc1	brouk
žijí	žít	k5eAaImIp3nP	žít
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
nektarem	nektar	k1gInSc7	nektar
a	a	k8xC	a
šťávou	šťáva	k1gFnSc7	šťáva
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Létají	létat	k5eAaImIp3nP	létat
za	za	k7c2	za
soumraku	soumrak	k1gInSc2	soumrak
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
pomalý	pomalý	k2eAgInSc1d1	pomalý
let	let	k1gInSc1	let
je	být	k5eAaImIp3nS	být
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
hluboce	hluboko	k6eAd1	hluboko
bzučivým	bzučivý	k2eAgInSc7d1	bzučivý
zvukem	zvuk	k1gInSc7	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
létají	létat	k5eAaImIp3nP	létat
mnohem	mnohem	k6eAd1	mnohem
častěji	často	k6eAd2	často
než	než	k8xS	než
samice	samice	k1gFnSc1	samice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
roháče	roháč	k1gInSc2	roháč
obecného	obecný	k2eAgInSc2d1	obecný
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
dimorfismus	dimorfismus	k1gInSc4	dimorfismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
samci	samec	k1gMnPc1	samec
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgNnPc4d2	veliký
kusadla	kusadla	k1gNnPc4	kusadla
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
celkově	celkově	k6eAd1	celkově
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
samice	samice	k1gFnSc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
samčí	samčí	k2eAgNnPc1d1	samčí
kusadla	kusadla	k1gNnPc1	kusadla
vypadají	vypadat	k5eAaPmIp3nP	vypadat
hrozivěji	hrozivě	k6eAd2	hrozivě
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
příliš	příliš	k6eAd1	příliš
slabá	slabý	k2eAgFnSc1d1	slabá
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ublížila	ublížit	k5eAaPmAgFnS	ublížit
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
menší	malý	k2eAgNnPc1d2	menší
kusadla	kusadla	k1gNnPc1	kusadla
samice	samice	k1gFnSc2	samice
mohou	moct	k5eAaImIp3nP	moct
způsobit	způsobit	k5eAaPmF	způsobit
bolestivé	bolestivý	k2eAgNnSc4d1	bolestivé
kousnutí	kousnutí	k1gNnSc4	kousnutí
<g/>
.	.	kIx.	.
</s>
<s>
Samčí	samčí	k2eAgNnPc1d1	samčí
kusadla	kusadla	k1gNnPc1	kusadla
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
paroží	paroží	k1gNnSc3	paroží
jelena	jelen	k1gMnSc2	jelen
a	a	k8xC	a
brouci	brouk	k1gMnPc1	brouk
je	on	k3xPp3gMnPc4	on
používají	používat	k5eAaImIp3nP	používat
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
jeleni	jelen	k1gMnPc1	jelen
k	k	k7c3	k
zápasu	zápas	k1gInSc3	zápas
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
podoba	podoba	k1gFnSc1	podoba
dala	dát	k5eAaPmAgFnS	dát
tomuto	tento	k3xDgMnSc3	tento
brouku	brouk	k1gMnSc3	brouk
vědecké	vědecký	k2eAgNnSc4d1	vědecké
i	i	k8xC	i
obecné	obecný	k2eAgNnSc4d1	obecné
jméno	jméno	k1gNnSc4	jméno
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nepřátelé	nepřítel	k1gMnPc5	nepřítel
===	===	k?	===
</s>
</p>
<p>
<s>
Přirozenými	přirozený	k2eAgMnPc7d1	přirozený
nepřáteli	nepřítel	k1gMnPc7	nepřítel
roháčů	roháč	k1gMnPc2	roháč
jsou	být	k5eAaImIp3nP	být
straky	straka	k1gFnPc1	straka
<g/>
,	,	kIx,	,
sojky	sojka	k1gFnPc1	sojka
<g/>
,	,	kIx,	,
jezevci	jezevec	k1gMnPc1	jezevec
<g/>
,	,	kIx,	,
lišky	liška	k1gFnPc1	liška
<g/>
,	,	kIx,	,
ježci	ježek	k1gMnPc1	ježek
<g/>
,	,	kIx,	,
kočky	kočka	k1gFnPc1	kočka
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
datlové	datel	k1gMnPc1	datel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ochrana	ochrana	k1gFnSc1	ochrana
==	==	k?	==
</s>
</p>
<p>
<s>
Lucanus	Lucanus	k1gInSc1	Lucanus
cervus	cervus	k1gInSc1	cervus
je	být	k5eAaImIp3nS	být
zapsán	zapsat	k5eAaPmNgInS	zapsat
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
dodatku	dodatek	k1gInSc6	dodatek
směrnic	směrnice	k1gFnPc2	směrnice
lokalit	lokalita	k1gFnPc2	lokalita
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
požaduje	požadovat	k5eAaImIp3nS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
stanovily	stanovit	k5eAaPmAgInP	stanovit
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
území	území	k1gNnSc4	území
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
pro	pro	k7c4	pro
ohrožené	ohrožený	k2eAgInPc4d1	ohrožený
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
též	též	k9	též
zapsán	zapsat	k5eAaPmNgInS	zapsat
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
dodatku	dodatek	k1gInSc6	dodatek
"	"	kIx"	"
<g/>
Dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
evropských	evropský	k2eAgMnPc2d1	evropský
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
přírodních	přírodní	k2eAgNnPc2d1	přírodní
nalezišť	naleziště	k1gNnPc2	naleziště
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Bernská	bernský	k2eAgFnSc1d1	Bernská
dohoda	dohoda	k1gFnSc1	dohoda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc4	seznam
poddruhů	poddruh	k1gInPc2	poddruh
==	==	k?	==
</s>
</p>
<p>
<s>
Nejznámějšími	známý	k2eAgInPc7d3	nejznámější
poddruhy	poddruh	k1gInPc7	poddruh
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lucanus	Lucanus	k1gMnSc1	Lucanus
cervus	cervus	k1gMnSc1	cervus
cervussamci	cervussamec	k1gMnSc3	cervussamec
<g/>
:	:	kIx,	:
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
92	[number]	k4	92
mm	mm	kA	mm
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
<g/>
:	:	kIx,	:
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
45	[number]	k4	45
mm	mm	kA	mm
</s>
</p>
<p>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
EvropaLucanus	EvropaLucanus	k1gMnSc1	EvropaLucanus
cervus	cervus	k1gMnSc1	cervus
akbesianussamci	akbesianussamec	k1gMnSc3	akbesianussamec
<g/>
:	:	kIx,	:
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
mm	mm	kA	mm
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
<g/>
:	:	kIx,	:
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
45	[number]	k4	45
mm	mm	kA	mm
</s>
</p>
<p>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
Sýrie	Sýrie	k1gFnSc1	Sýrie
<g/>
,	,	kIx,	,
TureckoLucanus	TureckoLucanus	k1gMnSc1	TureckoLucanus
cervus	cervus	k1gMnSc1	cervus
judaicussamci	judaicussamec	k1gMnSc3	judaicussamec
<g/>
:	:	kIx,	:
50	[number]	k4	50
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
mm	mm	kA	mm
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
<g/>
:	:	kIx,	:
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
mm	mm	kA	mm
</s>
</p>
<p>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
Sýrie	Sýrie	k1gFnSc1	Sýrie
<g/>
,	,	kIx,	,
TureckoLucanus	TureckoLucanus	k1gMnSc1	TureckoLucanus
cervus	cervus	k1gMnSc1	cervus
turcicussamci	turcicussamec	k1gMnSc3	turcicussamec
<g/>
:	:	kIx,	:
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
75	[number]	k4	75
mm	mm	kA	mm
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
<g/>
:	:	kIx,	:
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
mm	mm	kA	mm
</s>
</p>
<p>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
<g/>
:	:	kIx,	:
Bulharsko	Bulharsko	k1gNnSc1	Bulharsko
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Lucanus	Lucanus	k1gMnSc1	Lucanus
cervus	cervus	k1gMnSc1	cervus
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Bernhard	Bernhard	k1gMnSc1	Bernhard
Klausnitzer	Klausnitzer	k1gMnSc1	Klausnitzer
<g/>
:	:	kIx,	:
Die	Die	k1gMnSc1	Die
Hirschkäfer	Hirschkäfer	k1gMnSc1	Hirschkäfer
(	(	kIx(	(
<g/>
Lucanidae	Lucanidae	k1gFnSc1	Lucanidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Die	Die	k1gMnSc3	Die
Neue	Neu	k1gFnSc2	Neu
Brehm-Bücherei	Brehm-Büchere	k1gMnSc3	Brehm-Büchere
Bd	Bd	k1gMnSc3	Bd
<g/>
.	.	kIx.	.
551	[number]	k4	551
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Westarp	Westarp	k1gInSc1	Westarp
&	&	k?	&
Spektrum	spektrum	k1gNnSc1	spektrum
<g/>
,	,	kIx,	,
Magdeburg	Magdeburg	k1gInSc1	Magdeburg
<g/>
,	,	kIx,	,
Heidelberg	Heidelberg	k1gInSc1	Heidelberg
<g/>
,	,	kIx,	,
Berlin	berlina	k1gFnPc2	berlina
und	und	k?	und
Oxford	Oxford	k1gInSc1	Oxford
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3-89432-451-1	[number]	k4	3-89432-451-1
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
roháč	roháč	k1gMnSc1	roháč
obecný	obecný	k2eAgMnSc1d1	obecný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
BioLib	BioLib	k1gInSc1	BioLib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Lucanus	Lucanus	k1gMnSc1	Lucanus
cervus	cervus	k1gMnSc1	cervus
(	(	kIx(	(
<g/>
roháč	roháč	k1gMnSc1	roháč
obecný	obecný	k2eAgMnSc1d1	obecný
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
BioLib	BioLib	k1gInSc1	BioLib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
