<s>
Indická	indický	k2eAgFnSc1d1	indická
kosmická	kosmický	k2eAgFnSc1d1	kosmická
agentura	agentura	k1gFnSc1	agentura
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Indian	Indiana	k1gFnPc2	Indiana
Space	Spaec	k1gInSc2	Spaec
Research	Research	k1gInSc4	Research
Organisation	Organisation	k1gInSc1	Organisation
známá	známá	k1gFnSc1	známá
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
ISRO	ISRO	kA	ISRO
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgFnSc1d1	státní
indická	indický	k2eAgFnSc1d1	indická
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
agentura	agentura	k1gFnSc1	agentura
zastřešující	zastřešující	k2eAgFnSc1d1	zastřešující
indické	indický	k2eAgFnPc4d1	indická
kosmické	kosmický	k2eAgFnPc4d1	kosmická
ambice	ambice	k1gFnPc4	ambice
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Bengalúru	Bengalúr	k1gInSc2	Bengalúr
<g/>
.	.	kIx.	.
</s>
