<p>
<s>
Indická	indický	k2eAgFnSc1d1	indická
kosmická	kosmický	k2eAgFnSc1d1	kosmická
agentura	agentura	k1gFnSc1	agentura
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Indian	Indiana	k1gFnPc2	Indiana
Space	Spaec	k1gInSc2	Spaec
Research	Research	k1gInSc4	Research
Organisation	Organisation	k1gInSc1	Organisation
známá	známá	k1gFnSc1	známá
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
ISRO	ISRO	kA	ISRO
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgFnSc1d1	státní
indická	indický	k2eAgFnSc1d1	indická
vesmírná	vesmírný	k2eAgFnSc1d1	vesmírná
agentura	agentura	k1gFnSc1	agentura
zastřešující	zastřešující	k2eAgFnSc1d1	zastřešující
indické	indický	k2eAgFnPc4d1	indická
kosmické	kosmický	k2eAgFnPc4d1	kosmická
ambice	ambice	k1gFnPc4	ambice
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Bengalúru	Bengalúr	k1gInSc2	Bengalúr
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
disponuje	disponovat	k5eAaBmIp3nS	disponovat
rozpočtem	rozpočet	k1gInSc7	rozpočet
okolo	okolo	k7c2	okolo
866	[number]	k4	866
miliónů	milión	k4xCgInPc2	milión
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Založena	založen	k2eAgFnSc1d1	založena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
indická	indický	k2eAgFnSc1d1	indická
vláda	vláda	k1gFnSc1	vláda
zřídila	zřídit	k5eAaPmAgFnS	zřídit
Vesmírnou	vesmírný	k2eAgFnSc4d1	vesmírná
komisi	komise	k1gFnSc4	komise
(	(	kIx(	(
<g/>
Space	Spaec	k1gInPc4	Spaec
Commission	Commission	k1gInSc1	Commission
<g/>
)	)	kIx)	)
a	a	k8xC	a
Oddělení	oddělení	k1gNnSc4	oddělení
pro	pro	k7c4	pro
vesmír	vesmír	k1gInSc4	vesmír
(	(	kIx(	(
<g/>
Department	department	k1gInSc1	department
of	of	k?	of
Space	Space	k1gFnSc2	Space
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterými	který	k3yQgMnPc7	který
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
zřízena	zřízen	k2eAgFnSc1d1	zřízena
organizace	organizace	k1gFnSc1	organizace
ISRO	ISRO	kA	ISRO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Kořeny	kořen	k1gInPc1	kořen
indického	indický	k2eAgInSc2d1	indický
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
programu	program	k1gInSc2	program
sahají	sahat	k5eAaImIp3nP	sahat
na	na	k7c4	na
počátek	počátek	k1gInSc4	počátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
založeno	založen	k2eAgNnSc4d1	založeno
středisko	středisko	k1gNnSc4	středisko
pro	pro	k7c4	pro
start	start	k1gInSc4	start
raket	raketa	k1gFnPc2	raketa
v	v	k7c6	v
indické	indický	k2eAgFnSc6d1	indická
Thumbě	Thumba	k1gFnSc6	Thumba
(	(	kIx(	(
<g/>
TERLS	TERLS	kA	TERLS
–	–	k?	–
Thumba	Thumba	k1gMnSc1	Thumba
Equatorial	Equatorial	k1gMnSc1	Equatorial
Rocket	Rocket	k1gMnSc1	Rocket
Launching	Launching	k1gInSc4	Launching
Station	station	k1gInSc1	station
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
Indie	Indie	k1gFnSc1	Indie
sestrojila	sestrojit	k5eAaPmAgFnS	sestrojit
první	první	k4xOgFnSc4	první
funkční	funkční	k2eAgFnSc4d1	funkční
raketu	raketa	k1gFnSc4	raketa
(	(	kIx(	(
<g/>
start	start	k1gInSc1	start
byl	být	k5eAaImAgInS	být
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Thumbě	Thumba	k1gFnSc6	Thumba
založeno	založen	k2eAgNnSc1d1	založeno
Vesmírné	vesmírný	k2eAgNnSc1d1	vesmírné
vědecko-technické	vědeckoechnický	k2eAgNnSc1d1	vědecko-technické
centrum	centrum	k1gNnSc1	centrum
(	(	kIx(	(
<g/>
SSTC	SSTC	kA	SSTC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stalo	stát	k5eAaPmAgNnS	stát
páteří	páteř	k1gFnPc2	páteř
indického	indický	k2eAgInSc2d1	indický
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
projektu	projekt	k1gInSc2	projekt
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
Indii	Indie	k1gFnSc4	Indie
úspěšně	úspěšně	k6eAd1	úspěšně
vyslat	vyslat	k5eAaPmF	vyslat
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
satelit	satelit	k1gInSc4	satelit
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
satelitu	satelit	k1gInSc2	satelit
Aryabatha	Aryabatha	k1gFnSc1	Aryabatha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
se	se	k3xPyFc4	se
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
sovětské	sovětský	k2eAgFnSc2d1	sovětská
lodi	loď	k1gFnSc2	loď
podíval	podívat	k5eAaPmAgMnS	podívat
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
i	i	k8xC	i
první	první	k4xOgMnSc1	první
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
Rakeshe	Rakesh	k1gFnSc2	Rakesh
Sharmy	Sharma	k1gFnSc2	Sharma
<g/>
.	.	kIx.	.
<g/>
Otcem	otec	k1gMnSc7	otec
indického	indický	k2eAgInSc2d1	indický
vesmírného	vesmírný	k2eAgInSc2d1	vesmírný
projektu	projekt	k1gInSc2	projekt
byl	být	k5eAaImAgMnS	být
doktor	doktor	k1gMnSc1	doktor
Vikram	Vikram	k1gInSc4	Vikram
Sarabhai	Sarabha	k1gFnSc2	Sarabha
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
-	-	kIx~	-
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
po	po	k7c4	po
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
vedl	vést	k5eAaImAgMnS	vést
a	a	k8xC	a
udával	udávat	k5eAaImAgMnS	udávat
zaměření	zaměření	k1gNnSc4	zaměření
indického	indický	k2eAgInSc2d1	indický
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
si	se	k3xPyFc3	se
vědom	vědom	k2eAgInSc1d1	vědom
skutečnosti	skutečnost	k1gFnSc3	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Indie	Indie	k1gFnSc1	Indie
nemá	mít	k5eNaImIp3nS	mít
prostředky	prostředek	k1gInPc4	prostředek
na	na	k7c4	na
vyslání	vyslání	k1gNnSc4	vyslání
svého	svůj	k3xOyFgMnSc2	svůj
kosmonauta	kosmonaut	k1gMnSc2	kosmonaut
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
,	,	kIx,	,
či	či	k8xC	či
aby	aby	kYmCp3nS	aby
přistála	přistát	k5eAaPmAgFnS	přistát
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
vedl	vést	k5eAaImAgMnS	vést
Indii	Indie	k1gFnSc4	Indie
k	k	k7c3	k
reálnějším	reální	k2eAgInPc3d2	reální
cílům	cíl	k1gInPc3	cíl
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
byla	být	k5eAaImAgFnS	být
výstavba	výstavba	k1gFnSc1	výstavba
vlastního	vlastní	k2eAgInSc2d1	vlastní
raketového	raketový	k2eAgInSc2d1	raketový
nosiče	nosič	k1gInSc2	nosič
<g/>
,	,	kIx,	,
vysílání	vysílání	k1gNnSc4	vysílání
telekomunikačních	telekomunikační	k2eAgInPc2d1	telekomunikační
satelitů	satelit	k1gInPc2	satelit
<g/>
,	,	kIx,	,
meteorologických	meteorologický	k2eAgFnPc2d1	meteorologická
družic	družice	k1gFnPc2	družice
atd.	atd.	kA	atd.
Věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Indie	Indie	k1gFnSc1	Indie
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
aspoň	aspoň	k9	aspoň
touto	tento	k3xDgFnSc7	tento
cestou	cesta	k1gFnSc7	cesta
pokoušet	pokoušet	k5eAaImF	pokoušet
prorazit	prorazit	k5eAaPmF	prorazit
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
ISRO	ISRO	kA	ISRO
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
převážně	převážně	k6eAd1	převážně
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
<g/>
,	,	kIx,	,
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
vypouštění	vypouštění	k1gNnSc4	vypouštění
satelitů	satelit	k1gMnPc2	satelit
<g/>
,	,	kIx,	,
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
vypouštění	vypouštění	k1gNnSc4	vypouštění
nosných	nosný	k2eAgFnPc2d1	nosná
raket	raketa	k1gFnPc2	raketa
<g/>
,	,	kIx,	,
odpalovacích	odpalovací	k2eAgNnPc2d1	odpalovací
vozidel	vozidlo	k1gNnPc2	vozidlo
a	a	k8xC	a
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
připojeného	připojený	k2eAgNnSc2d1	připojené
pozemního	pozemní	k2eAgNnSc2d1	pozemní
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Experimentuje	experimentovat	k5eAaImIp3nS	experimentovat
se	se	k3xPyFc4	se
satelity	satelit	k1gInPc1	satelit
pro	pro	k7c4	pro
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
,	,	kIx,	,
či	či	k8xC	či
pro	pro	k7c4	pro
telekomunikaci	telekomunikace	k1gFnSc4	telekomunikace
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
zařízení	zařízení	k1gNnSc1	zařízení
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
ke	k	k7c3	k
komerčním	komerční	k2eAgInPc3d1	komerční
záměrům	záměr	k1gInPc3	záměr
<g/>
.	.	kIx.	.
</s>
<s>
ISRO	ISRO	kA	ISRO
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
jinými	jiný	k2eAgFnPc7d1	jiná
zeměmi	zem	k1gFnPc7	zem
(	(	kIx(	(
<g/>
ESA	eso	k1gNnSc2	eso
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
či	či	k8xC	či
s	s	k7c7	s
čínskou	čínský	k2eAgFnSc7d1	čínská
CNSA	CNSA	kA	CNSA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISRO	ISRO	kA	ISRO
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
výrobní	výrobní	k2eAgInPc4d1	výrobní
závody	závod	k1gInPc4	závod
<g/>
,	,	kIx,	,
vědecká	vědecký	k2eAgNnPc4d1	vědecké
pracoviště	pracoviště	k1gNnPc4	pracoviště
a	a	k8xC	a
pobočky	pobočka	k1gFnPc4	pobočka
roztroušené	roztroušený	k2eAgFnPc4d1	roztroušená
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Agentura	agentura	k1gFnSc1	agentura
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
indický	indický	k2eAgInSc4d1	indický
Kosmodrom	kosmodrom	k1gInSc4	kosmodrom
Šríharikota	Šríharikot	k1gMnSc2	Šríharikot
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
zprovoznila	zprovoznit	k5eAaPmAgFnS	zprovoznit
druhý	druhý	k4xOgInSc4	druhý
startovací	startovací	k2eAgInSc4d1	startovací
komplex	komplex	k1gInSc4	komplex
SLP	SLP	kA	SLP
(	(	kIx(	(
<g/>
Second	Second	k1gMnSc1	Second
Lauch	Lauch	k1gMnSc1	Lauch
Pad	padnout	k5eAaPmDgInS	padnout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2019	[number]	k4	2019
ředitel	ředitel	k1gMnSc1	ředitel
ISRO	ISRO	kA	ISRO
zveřejnil	zveřejnit	k5eAaPmAgMnS	zveřejnit
zprávu	zpráva	k1gFnSc4	zpráva
"	"	kIx"	"
<g/>
2018	[number]	k4	2018
:	:	kIx,	:
A	a	k8xC	a
Year	Year	k1gMnSc1	Year
of	of	k?	of
many	mana	k1gFnSc2	mana
'	'	kIx"	'
<g/>
firsts	firsts	k1gInSc1	firsts
<g/>
'	'	kIx"	'
and	and	k?	and
'	'	kIx"	'
<g/>
beginnings	beginnings	k1gInSc1	beginnings
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nosné	nosný	k2eAgFnPc1d1	nosná
rakety	raketa	k1gFnPc1	raketa
==	==	k?	==
</s>
</p>
<p>
<s>
Indie	Indie	k1gFnSc1	Indie
od	od	k7c2	od
konce	konec	k1gInSc2	konec
70	[number]	k4	70
<g/>
.	.	kIx.	.
<g/>
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
své	svůj	k3xOyFgFnPc4	svůj
nosné	nosný	k2eAgFnPc4d1	nosná
rakety	raketa	k1gFnPc4	raketa
pro	pro	k7c4	pro
vynášení	vynášení	k1gNnPc4	vynášení
družic	družice	k1gFnPc2	družice
Země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
takovou	takový	k3xDgFnSc7	takový
raketou	raketa	k1gFnSc7	raketa
pro	pro	k7c4	pro
kosmonautiku	kosmonautika	k1gFnSc4	kosmonautika
byla	být	k5eAaImAgFnS	být
SLV-	SLV-	k1gFnSc1	SLV-
<g/>
3	[number]	k4	3
<g/>
-E	-E	k?	-E
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
při	při	k7c6	při
startu	start	k1gInSc6	start
s	s	k7c7	s
družicí	družice	k1gFnSc7	družice
Rohini-	Rohini-	k1gFnSc7	Rohini-
<g/>
1	[number]	k4	1
<g/>
A	A	kA	A
dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1979	[number]	k4	1979
havarovala	havarovat	k5eAaPmAgFnS	havarovat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
raketa	raketa	k1gFnSc1	raketa
SLV-E2	SLV-E2	k1gFnSc1	SLV-E2
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
start	start	k1gInSc4	start
úspěšně	úspěšně	k6eAd1	úspěšně
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
začaly	začít	k5eAaPmAgInP	začít
startovat	startovat	k5eAaBmF	startovat
se	s	k7c7	s
střídavými	střídavý	k2eAgInPc7d1	střídavý
úspěchy	úspěch	k1gInPc7	úspěch
rakety	raketa	k1gFnSc2	raketa
ASLV	ASLV	kA	ASLV
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
první	první	k4xOgInSc1	první
start	start	k1gInSc1	start
rakety	raketa	k1gFnSc2	raketa
PSLV	PSLV	kA	PSLV
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2001	[number]	k4	2001
poprvé	poprvé	k6eAd1	poprvé
startovala	startovat	k5eAaBmAgFnS	startovat
nová	nový	k2eAgFnSc1d1	nová
raketa	raketa	k1gFnSc1	raketa
GSLV	GSLV	kA	GSLV
Mk	Mk	k1gFnSc1	Mk
<g/>
.1	.1	k4	.1
po	po	k7c6	po
předchozím	předchozí	k2eAgInSc6d1	předchozí
desetiletém	desetiletý	k2eAgInSc6d1	desetiletý
vývoji	vývoj	k1gInSc6	vývoj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stál	stát	k5eAaImAgInS	stát
Indii	Indie	k1gFnSc4	Indie
305	[number]	k4	305
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
jednoho	jeden	k4xCgInSc2	jeden
startu	start	k1gInSc2	start
je	být	k5eAaImIp3nS	být
udávána	udávat	k5eAaImNgFnS	udávat
částkou	částka	k1gFnSc7	částka
60	[number]	k4	60
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
mise	mise	k1gFnSc2	mise
==	==	k?	==
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
z	z	k7c2	z
indického	indický	k2eAgInSc2d1	indický
kosmodromu	kosmodrom	k1gInSc2	kosmodrom
Satiš	Satiš	k1gInSc1	Satiš
Davan	Davany	k1gInPc2	Davany
sonda	sonda	k1gFnSc1	sonda
Mangalján	Mangalján	k1gInSc1	Mangalján
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
upravené	upravený	k2eAgFnSc2d1	upravená
rakety	raketa	k1gFnSc2	raketa
Polar	Polar	k1gMnSc1	Polar
Satellite	Satellit	k1gInSc5	Satellit
Launch	Launch	k1gInSc1	Launch
Vehicle	Vehicl	k1gMnSc4	Vehicl
C-	C-	k1gFnSc2	C-
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
mise	mise	k1gFnSc2	mise
je	být	k5eAaImIp3nS	být
průzkum	průzkum	k1gInSc1	průzkum
atmosféry	atmosféra	k1gFnSc2	atmosféra
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Let	let	k1gInSc1	let
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
startu	start	k1gInSc6	start
doprovázely	doprovázet	k5eAaImAgInP	doprovázet
problémy	problém	k1gInPc1	problém
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2014	[number]	k4	2014
byla	být	k5eAaImAgFnS	být
sonda	sonda	k1gFnSc1	sonda
úspěšně	úspěšně	k6eAd1	úspěšně
navedena	navést	k5eAaPmNgFnS	navést
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
suborbitální	suborbitální	k2eAgFnSc4d1	suborbitální
dráhu	dráha	k1gFnSc4	dráha
úspěšně	úspěšně	k6eAd1	úspěšně
vynesen	vynesen	k2eAgInSc1d1	vynesen
návratový	návratový	k2eAgInSc1d1	návratový
modul	modul	k1gInSc1	modul
CARE	car	k1gMnSc5	car
pomocí	pomoc	k1gFnSc7	pomoc
nové	nový	k2eAgFnSc2d1	nová
nosné	nosný	k2eAgFnSc2d1	nosná
rakety	raketa	k1gFnSc2	raketa
GSLV	GSLV	kA	GSLV
Mk	Mk	k1gFnSc1	Mk
<g/>
.3	.3	k4	.3
(	(	kIx(	(
<g/>
LVM	LVM	kA	LVM
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
<g/>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2016	[number]	k4	2016
Indie	Indie	k1gFnSc1	Indie
vyslala	vyslat	k5eAaPmAgFnS	vyslat
na	na	k7c4	na
geostacionární	geostacionární	k2eAgFnSc4d1	geostacionární
dráhu	dráha	k1gFnSc4	dráha
meteorologickou	meteorologický	k2eAgFnSc4d1	meteorologická
družici	družice	k1gFnSc4	družice
INSAT	INSAT	kA	INSAT
3DR	[number]	k4	3DR
pomocí	pomocí	k7c2	pomocí
svého	svůj	k3xOyFgInSc2	svůj
kosmického	kosmický	k2eAgInSc2d1	kosmický
nosiče	nosič	k1gInSc2	nosič
GSLV	GSLV	kA	GSLV
Mk	Mk	k1gFnSc1	Mk
<g/>
.2	.2	k4	.2
.	.	kIx.	.
<g/>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
na	na	k7c4	na
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2017	[number]	k4	2017
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
kosmodromu	kosmodrom	k1gInSc2	kosmodrom
Šríharikota	Šríharikot	k1gMnSc2	Šríharikot
pomocí	pomocí	k7c2	pomocí
nosiče	nosič	k1gInSc2	nosič
ISRO	ISRO	kA	ISRO
–	–	k?	–
PSLV	PSLV	kA	PSLV
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
vypuštěno	vypuštěn	k2eAgNnSc1d1	vypuštěno
104	[number]	k4	104
družic	družice	k1gFnPc2	družice
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
88	[number]	k4	88
družic	družice	k1gFnPc2	družice
Flock	Flock	k1gInSc4	Flock
3P	[number]	k4	3P
na	na	k7c4	na
polární	polární	k2eAgFnSc4d1	polární
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2017	[number]	k4	2017
byla	být	k5eAaImAgFnS	být
nosičem	nosič	k1gInSc7	nosič
GSLV	GSLV	kA	GSLV
Mk	Mk	k1gFnSc1	Mk
<g/>
.3	.3	k4	.3
na	na	k7c4	na
dráhu	dráha	k1gFnSc4	dráha
přechodovou	přechodový	k2eAgFnSc4d1	přechodová
ke	k	k7c3	k
geostacionární	geostacionární	k2eAgFnSc1d1	geostacionární
vynesena	vynesen	k2eAgFnSc1d1	vynesena
družice	družice	k1gFnSc1	družice
GSAT-19	GSAT-19	k1gFnSc2	GSAT-19
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
3136	[number]	k4	3136
kg	kg	kA	kg
<g/>
.23	.23	k4	.23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2017	[number]	k4	2017
bylo	být	k5eAaImAgNnS	být
nosičem	nosič	k1gInSc7	nosič
PSLV	PSLV	kA	PSLV
<g />
.	.	kIx.	.
</s>
<s>
vypuštěno	vypuštěn	k2eAgNnSc1d1	vypuštěno
asi	asi	k9	asi
30	[number]	k4	30
satelitů	satelit	k1gMnPc2	satelit
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
český	český	k2eAgInSc4d1	český
VZLUSat-	VZLUSat-	k1gFnSc7	VZLUSat-
<g/>
1	[number]	k4	1
a	a	k8xC	a
slovenský	slovenský	k2eAgMnSc1d1	slovenský
skCube	skCubat	k5eAaPmIp3nS	skCubat
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2018	[number]	k4	2018
byla	být	k5eAaImAgFnS	být
nosičem	nosič	k1gInSc7	nosič
PSLV	PSLV	kA	PSLV
na	na	k7c4	na
polární	polární	k2eAgFnSc4d1	polární
dráhu	dráha	k1gFnSc4	dráha
vypuštěno	vypustit	k5eAaPmNgNnS	vypustit
31	[number]	k4	31
družic	družice	k1gFnPc2	družice
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
družice	družice	k1gFnSc2	družice
řady	řada	k1gFnSc2	řada
Cartosat-	Cartosat-	k1gFnSc2	Cartosat-
<g/>
2	[number]	k4	2
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
710	[number]	k4	710
kg	kg	kA	kg
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
byla	být	k5eAaImAgFnS	být
nosičem	nosič	k1gInSc7	nosič
GSLV	GSLV	kA	GSLV
Mk	Mk	k1gFnSc1	Mk
<g/>
.3	.3	k4	.3
na	na	k7c4	na
dráhu	dráha	k1gFnSc4	dráha
přechodovou	přechodový	k2eAgFnSc4d1	přechodová
ke	k	k7c3	k
geostacionární	geostacionární	k2eAgFnSc1d1	geostacionární
vypuštěna	vypuštěn	k2eAgFnSc1d1	vypuštěna
GSAT-29	GSAT-29	k1gFnSc1	GSAT-29
o	o	k7c6	o
hmotnosti	hmotnost	k1gFnSc6	hmotnost
3877	[number]	k4	3877
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
PSLV	PSLV	kA	PSLV
-	-	kIx~	-
lehký	lehký	k2eAgInSc4d1	lehký
indický	indický	k2eAgInSc4d1	indický
kosmický	kosmický	k2eAgInSc4d1	kosmický
nosič	nosič	k1gInSc4	nosič
</s>
</p>
<p>
<s>
GSLV	GSLV	kA	GSLV
–	–	k?	–
dosavadní	dosavadní	k2eAgInSc4d1	dosavadní
indický	indický	k2eAgInSc4d1	indický
kosmický	kosmický	k2eAgInSc4d1	kosmický
nosič	nosič	k1gInSc4	nosič
</s>
</p>
<p>
<s>
GSLV	GSLV	kA	GSLV
Mk	Mk	k1gFnSc1	Mk
<g/>
.3	.3	k4	.3
–	–	k?	–
nový	nový	k2eAgMnSc1d1	nový
indický	indický	k2eAgMnSc1d1	indický
kosmický	kosmický	k2eAgMnSc1d1	kosmický
nosič	nosič	k1gMnSc1	nosič
<g/>
,	,	kIx,	,
nástupce	nástupce	k1gMnSc1	nástupce
GSLV	GSLV	kA	GSLV
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Indická	indický	k2eAgFnSc1d1	indická
kosmická	kosmický	k2eAgFnSc1d1	kosmická
agentura	agentura	k1gFnSc1	agentura
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Neoficiální	oficiální	k2eNgFnPc1d1	neoficiální
stránky	stránka	k1gFnPc1	stránka
o	o	k7c6	o
indickém	indický	k2eAgInSc6d1	indický
programu	program	k1gInSc6	program
</s>
</p>
