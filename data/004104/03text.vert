<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
List	list	k1gInSc1	list
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1877	[number]	k4	1877
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
elektrotechnický	elektrotechnický	k2eAgMnSc1d1	elektrotechnický
inženýr	inženýr	k1gMnSc1	inženýr
a	a	k8xC	a
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
.	.	kIx.	.
</s>
<s>
Zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
se	se	k3xPyFc4	se
o	o	k7c4	o
elektrifikaci	elektrifikace	k1gFnSc4	elektrifikace
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
o	o	k7c4	o
zavedení	zavedení	k1gNnSc4	zavedení
československých	československý	k2eAgFnPc2d1	Československá
technických	technický	k2eAgFnPc2d1	technická
norem	norma	k1gFnPc2	norma
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
List	lista	k1gFnPc2	lista
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
pražského	pražský	k2eAgMnSc2d1	pražský
poštovního	poštovní	k1gMnSc2	poštovní
úředníka	úředník	k1gMnSc2	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
akademické	akademický	k2eAgNnSc4d1	akademické
gymnázium	gymnázium	k1gNnSc4	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Zajímala	zajímat	k5eAaImAgFnS	zajímat
ho	on	k3xPp3gNnSc4	on
sociologie	sociologie	k1gFnSc1	sociologie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
matematika	matematika	k1gFnSc1	matematika
<g/>
,	,	kIx,	,
fyzika	fyzika	k1gFnSc1	fyzika
a	a	k8xC	a
deskriptivní	deskriptivní	k2eAgFnSc1d1	deskriptivní
geometrie	geometrie	k1gFnSc1	geometrie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
nebyl	být	k5eNaImAgMnS	být
spokojen	spokojen	k2eAgMnSc1d1	spokojen
s	s	k7c7	s
úrovní	úroveň	k1gFnSc7	úroveň
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Přešel	přejít	k5eAaPmAgMnS	přejít
proto	proto	k8xC	proto
na	na	k7c4	na
pražskou	pražský	k2eAgFnSc4d1	Pražská
techniku	technika	k1gFnSc4	technika
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c4	na
obor	obor	k1gInSc4	obor
strojírenství	strojírenství	k1gNnSc2	strojírenství
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
na	na	k7c4	na
elektrotechniku	elektrotechnika	k1gFnSc4	elektrotechnika
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc4	studium
úspěšně	úspěšně	k6eAd1	úspěšně
dokončil	dokončit	k5eAaPmAgInS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
1901	[number]	k4	1901
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
belgickém	belgický	k2eAgInSc6d1	belgický
Lutychu	Lutych	k1gInSc6	Lutych
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
šéfkonstruktér	šéfkonstruktér	k1gMnSc1	šéfkonstruktér
v	v	k7c6	v
Křižíkových	Křižíkových	k2eAgInPc6d1	Křižíkových
závodech	závod	k1gInPc6	závod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
hořínské	hořínský	k2eAgFnSc2d1	hořínský
hydroelektrárny	hydroelektrárna	k1gFnSc2	hydroelektrárna
u	u	k7c2	u
Mělníka	Mělník	k1gInSc2	Mělník
<g/>
,	,	kIx,	,
elektrické	elektrický	k2eAgFnSc2d1	elektrická
dráhy	dráha	k1gFnSc2	dráha
Tábor	tábor	k1gMnSc1	tábor
<g/>
–	–	k?	–
<g/>
Bechyně	Bechyně	k1gMnSc1	Bechyně
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
projektech	projekt	k1gInPc6	projekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
se	se	k3xPyFc4	se
Vladimír	Vladimír	k1gMnSc1	Vladimír
List	list	k1gInSc4	list
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Helenou	Helena	k1gFnSc7	Helena
Gebauerovou	Gebauerová	k1gFnSc7	Gebauerová
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
bohemisty	bohemista	k1gMnSc2	bohemista
Jana	Jan	k1gMnSc2	Jan
Gebauera	Gebauer	k1gMnSc2	Gebauer
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
přijal	přijmout	k5eAaPmAgMnS	přijmout
Vladimír	Vladimír	k1gMnSc1	Vladimír
List	lista	k1gFnPc2	lista
nabídku	nabídka	k1gFnSc4	nabídka
stát	stát	k5eAaImF	stát
se	s	k7c7	s
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c6	na
České	český	k2eAgFnSc6d1	Česká
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
technické	technický	k2eAgFnSc2d1	technická
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
nástupu	nástup	k1gInSc6	nástup
začal	začít	k5eAaPmAgInS	začít
kromě	kromě	k7c2	kromě
výuky	výuka	k1gFnSc2	výuka
a	a	k8xC	a
psaní	psaní	k1gNnSc4	psaní
skript	skript	k1gInSc1	skript
také	také	k9	také
budovat	budovat	k5eAaImF	budovat
nové	nový	k2eAgFnPc4d1	nová
laboratoře	laboratoř	k1gFnPc4	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
zde	zde	k6eAd1	zde
založil	založit	k5eAaPmAgInS	založit
Ústav	ústav	k1gInSc1	ústav
konstruktivní	konstruktivní	k2eAgFnSc2d1	konstruktivní
elektrotechniky	elektrotechnika	k1gFnSc2	elektrotechnika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1917	[number]	k4	1917
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
byl	být	k5eAaImAgMnS	být
rektorem	rektor	k1gMnSc7	rektor
České	český	k2eAgFnSc2d1	Česká
vysoké	vysoká	k1gFnSc2	vysoká
škole	škola	k1gFnSc3	škola
technické	technický	k2eAgFnSc2d1	technická
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
několikrát	několikrát	k6eAd1	několikrát
zastával	zastávat	k5eAaImAgMnS	zastávat
funkci	funkce	k1gFnSc4	funkce
děkana	děkan	k1gMnSc2	děkan
odboru	odbor	k1gInSc2	odbor
strojního	strojní	k2eAgNnSc2d1	strojní
a	a	k8xC	a
elektrotechnického	elektrotechnický	k2eAgNnSc2d1	elektrotechnické
inženýrství	inženýrství	k1gNnSc2	inženýrství
(	(	kIx(	(
<g/>
v	v	k7c6	v
obdobích	období	k1gNnPc6	období
1910	[number]	k4	1910
<g/>
–	–	k?	–
<g/>
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
a	a	k8xC	a
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
České	český	k2eAgFnSc6d1	Česká
vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
technické	technický	k2eAgFnSc2d1	technická
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xS	jako
profesor	profesor	k1gMnSc1	profesor
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
pokyn	pokyn	k1gInSc4	pokyn
komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
penzionován	penzionovat	k5eAaBmNgMnS	penzionovat
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
List	list	k1gInSc4	list
aktivné	aktivný	k2eAgFnSc3d1	aktivná
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
elektrotechniky	elektrotechnika	k1gFnSc2	elektrotechnika
i	i	k9	i
mimo	mimo	k7c4	mimo
rámec	rámec	k1gInSc4	rámec
brněnské	brněnský	k2eAgFnSc2d1	brněnská
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
založení	založení	k1gNnSc6	založení
odborného	odborný	k2eAgInSc2d1	odborný
časopisu	časopis	k1gInSc2	časopis
Elektrotechnický	elektrotechnický	k2eAgInSc4d1	elektrotechnický
obzor	obzor	k1gInSc4	obzor
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Františkem	František	k1gMnSc7	František
Weyrem	Weyr	k1gMnSc7	Weyr
a	a	k8xC	a
Karlem	Karel	k1gMnSc7	Karel
Englišem	Engliš	k1gMnSc7	Engliš
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
návrh	návrh	k1gInSc4	návrh
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
elektrifikaci	elektrifikace	k1gFnSc6	elektrifikace
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
předložen	předložit	k5eAaPmNgInS	předložit
ke	k	k7c3	k
schválení	schválení	k1gNnSc3	schválení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
a	a	k8xC	a
osobně	osobně	k6eAd1	osobně
se	se	k3xPyFc4	se
zasazoval	zasazovat	k5eAaImAgMnS	zasazovat
o	o	k7c4	o
vybudování	vybudování	k1gNnSc4	vybudování
páteřní	páteřní	k2eAgFnSc2d1	páteřní
sítě	síť	k1gFnSc2	síť
vysokonapěťových	vysokonapěťový	k2eAgNnPc2d1	vysokonapěťové
elektrických	elektrický	k2eAgNnPc2d1	elektrické
vedení	vedení	k1gNnSc2	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Působnost	působnost	k1gFnSc1	působnost
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
platil	platit	k5eAaImAgInS	platit
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
Československa	Československo	k1gNnSc2	Československo
rozšířena	rozšířen	k2eAgFnSc1d1	rozšířena
na	na	k7c6	na
území	území	k1gNnSc6	území
celého	celý	k2eAgInSc2d1	celý
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
Elektrotechnického	elektrotechnický	k2eAgInSc2d1	elektrotechnický
svazu	svaz	k1gInSc2	svaz
československého	československý	k2eAgInSc2d1	československý
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
zásluhy	zásluha	k1gFnPc4	zásluha
na	na	k7c4	na
sjednocení	sjednocení	k1gNnSc4	sjednocení
elektrického	elektrický	k2eAgNnSc2d1	elektrické
napětí	napětí	k1gNnSc2	napětí
v	v	k7c6	v
nízko-	nízko-	k?	nízko-
i	i	k8xC	i
vysokonapěťové	vysokonapěťový	k2eAgFnSc3d1	vysokonapěťová
elektrorozvodné	elektrorozvodný	k2eAgFnSc3d1	elektrorozvodná
síti	síť	k1gFnSc3	síť
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
spoluautorem	spoluautor	k1gMnSc7	spoluautor
prvního	první	k4xOgInSc2	první
projektu	projekt	k1gInSc2	projekt
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Bohumilem	Bohumil	k1gMnSc7	Bohumil
Beladou	Belada	k1gFnSc7	Belada
vypracoval	vypracovat	k5eAaPmAgInS	vypracovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
Významně	významně	k6eAd1	významně
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
vytváření	vytváření	k1gNnSc6	vytváření
a	a	k8xC	a
prosazování	prosazování	k1gNnSc6	prosazování
technických	technický	k2eAgFnPc2d1	technická
norem	norma	k1gFnPc2	norma
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Elektrotechnického	elektrotechnický	k2eAgInSc2d1	elektrotechnický
svazu	svaz	k1gInSc2	svaz
československého	československý	k2eAgInSc2d1	československý
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
také	také	k6eAd1	také
v	v	k7c6	v
Československé	československý	k2eAgFnSc6d1	Československá
společnosti	společnost	k1gFnSc6	společnost
normalisační	normalisační	k2eAgFnSc6d1	normalisační
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
předsedal	předsedat	k5eAaImAgMnS	předsedat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
prosadil	prosadit	k5eAaPmAgInS	prosadit
označování	označování	k1gNnSc4	označování
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
odpovídaly	odpovídat	k5eAaImAgFnP	odpovídat
normám	norma	k1gFnPc3	norma
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
ověřeny	ověřit	k5eAaPmNgInP	ověřit
zkušebnou	zkušebna	k1gFnSc7	zkušebna
<g/>
,	,	kIx,	,
logem	log	k1gInSc7	log
ESČ	ESČ	kA	ESČ
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
doklad	doklad	k1gInSc1	doklad
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
elektrotechnických	elektrotechnický	k2eAgNnPc2d1	elektrotechnické
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
zastupoval	zastupovat	k5eAaImAgInS	zastupovat
Československo	Československo	k1gNnSc4	Československo
při	při	k7c6	při
podpisu	podpis	k1gInSc6	podpis
zakládací	zakládací	k2eAgFnSc2d1	zakládací
listiny	listina	k1gFnSc2	listina
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
normalizační	normalizační	k2eAgFnSc2d1	normalizační
federace	federace	k1gFnSc2	federace
(	(	kIx(	(
<g/>
ISA	ISA	kA	ISA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
místopředsedou	místopředseda	k1gMnSc7	místopředseda
ISA	ISA	kA	ISA
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1932	[number]	k4	1932
<g/>
–	–	k?	–
<g/>
1934	[number]	k4	1934
byl	být	k5eAaImAgMnS	být
jejím	její	k3xOp3gMnSc7	její
předsedou	předseda	k1gMnSc7	předseda
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
List	lista	k1gFnPc2	lista
publikoval	publikovat	k5eAaBmAgMnS	publikovat
jako	jako	k9	jako
autor	autor	k1gMnSc1	autor
nebo	nebo	k8xC	nebo
spoluautor	spoluautor	k1gMnSc1	spoluautor
více	hodně	k6eAd2	hodně
než	než	k8xS	než
600	[number]	k4	600
odborných	odborný	k2eAgFnPc2d1	odborná
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
skript	skripta	k1gNnPc2	skripta
a	a	k8xC	a
článků	článek	k1gInPc2	článek
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
publikace	publikace	k1gFnSc1	publikace
Normalisace	Normalisace	k1gFnSc1	Normalisace
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
významně	významně	k6eAd1	významně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
standardizaci	standardizace	k1gFnSc4	standardizace
součástek	součástka	k1gFnPc2	součástka
československých	československý	k2eAgInPc2d1	československý
strojírenských	strojírenský	k2eAgInPc2d1	strojírenský
a	a	k8xC	a
elektrotechnických	elektrotechnický	k2eAgInPc2d1	elektrotechnický
výrobků	výrobek	k1gInPc2	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
obdržel	obdržet	k5eAaPmAgInS	obdržet
čestný	čestný	k2eAgInSc1d1	čestný
doktorát	doktorát	k1gInSc1	doktorát
brněnské	brněnský	k2eAgFnSc2d1	brněnská
techniky	technika	k1gFnSc2	technika
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
titul	titul	k1gInSc1	titul
doktor	doktor	k1gMnSc1	doktor
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
DrSc	DrSc	kA	DrSc
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
medaile	medaile	k1gFnSc1	medaile
VUT	VUT	kA	VUT
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
své	svůj	k3xOyFgFnPc4	svůj
zásluhy	zásluha	k1gFnPc4	zásluha
obdržel	obdržet	k5eAaPmAgMnS	obdržet
rovněž	rovněž	k9	rovněž
řadu	řada	k1gFnSc4	řada
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
řádů	řád	k1gInPc2	řád
a	a	k8xC	a
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
LIST	list	k1gInSc1	list
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Mechanika	mechanika	k1gFnSc1	mechanika
venkovních	venkovní	k2eAgNnPc2d1	venkovní
vedení	vedení	k1gNnPc2	vedení
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
matice	matice	k1gFnSc1	matice
technická	technický	k2eAgFnSc1d1	technická
<g/>
,	,	kIx,	,
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
LIST	list	k1gInSc1	list
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Navrhování	navrhování	k1gNnSc1	navrhování
elektrických	elektrický	k2eAgFnPc2d1	elektrická
drah	draha	k1gFnPc2	draha
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
matice	matice	k1gFnSc1	matice
technická	technický	k2eAgFnSc1d1	technická
<g/>
,	,	kIx,	,
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
BELADA	BELADA	kA	BELADA
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
<g/>
;	;	kIx,	;
LIST	list	k1gInSc1	list
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Podzemní	podzemní	k2eAgFnSc1d1	podzemní
rychlá	rychlý	k2eAgFnSc1d1	rychlá
dráha	dráha	k1gFnSc1	dráha
pro	pro	k7c4	pro
Prahu	Praha	k1gFnSc4	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
vl	vl	k?	vl
<g/>
.	.	kIx.	.
nákl	nákl	k1gMnSc1	nákl
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
LIST	list	k1gInSc1	list
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Normalisace	Normalisace	k1gFnSc1	Normalisace
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
matice	matice	k1gFnSc1	matice
technická	technický	k2eAgFnSc1d1	technická
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
LIST	list	k1gInSc1	list
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Elektrické	elektrický	k2eAgFnSc2d1	elektrická
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Elektrotechnický	elektrotechnický	k2eAgInSc1d1	elektrotechnický
svaz	svaz	k1gInSc1	svaz
českomoravský	českomoravský	k2eAgInSc1d1	českomoravský
<g/>
,	,	kIx,	,
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
LIST	list	k1gInSc1	list
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Užité	užitý	k2eAgFnPc4d1	užitá
vědy	věda	k1gFnPc4	věda
technické	technický	k2eAgFnPc4d1	technická
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Václav	Václav	k1gMnSc1	Václav
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
LIST	list	k1gInSc1	list
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
vozba	vozba	k1gFnSc1	vozba
I.	I.	kA	I.
Navrhování	navrhování	k1gNnSc2	navrhování
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
přepracované	přepracovaný	k2eAgNnSc1d1	přepracované
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
matice	matice	k1gFnSc1	matice
technická	technický	k2eAgFnSc1d1	technická
<g/>
,	,	kIx,	,
1949	[number]	k4	1949
<g/>
.	.	kIx.	.
</s>
