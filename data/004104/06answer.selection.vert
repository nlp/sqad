<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Františkem	František	k1gMnSc7	František
Weyrem	Weyr	k1gMnSc7	Weyr
a	a	k8xC	a
Karlem	Karel	k1gMnSc7	Karel
Englišem	Engliš	k1gMnSc7	Engliš
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
návrh	návrh	k1gInSc4	návrh
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
elektrifikaci	elektrifikace	k1gFnSc6	elektrifikace
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
předložen	předložit	k5eAaPmNgInS	předložit
ke	k	k7c3	k
schválení	schválení	k1gNnSc3	schválení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
a	a	k8xC	a
osobně	osobně	k6eAd1	osobně
se	se	k3xPyFc4	se
zasazoval	zasazovat	k5eAaImAgMnS	zasazovat
o	o	k7c4	o
vybudování	vybudování	k1gNnSc4	vybudování
páteřní	páteřní	k2eAgFnSc2d1	páteřní
sítě	síť	k1gFnSc2	síť
vysokonapěťových	vysokonapěťový	k2eAgNnPc2d1	vysokonapěťové
elektrických	elektrický	k2eAgNnPc2d1	elektrické
vedení	vedení	k1gNnSc2	vedení
<g/>
.	.	kIx.	.
</s>
