<s>
Mykola	Mykola	k1gFnSc1
Smaha	Smaha	k?
</s>
<s>
Mykola	Mykola	k1gFnSc1
Smaha	Smaha	k?
Narození	narození	k1gNnSc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1938	#num#	k4
<g/>
Bobrove	Bobrov	k1gInSc5
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1981	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
42	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Kyjev	Kyjev	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
atlet	atlet	k1gMnSc1
Ocenění	ocenění	k1gNnSc2
</s>
<s>
Odznak	odznak	k1gInSc1
ctiZasloužilý	ctiZasloužilý	k2eAgMnSc1d1
mistr	mistr	k1gMnSc1
sportu	sport	k1gInSc2
SSSR	SSSR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Atletika	atletika	k1gFnSc1
na	na	k7c4
LOH	LOH	kA
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
LOH	LOH	kA
1968	#num#	k4
</s>
<s>
chůze	chůze	k1gFnSc1
na	na	k7c4
20	#num#	k4
km	km	kA
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
atletice	atletika	k1gFnSc6
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
ME	ME	kA
1966	#num#	k4
</s>
<s>
chůze	chůze	k1gFnSc1
na	na	k7c4
20	#num#	k4
km	km	kA
</s>
<s>
bronz	bronz	k1gInSc4
</s>
<s>
ME	ME	kA
1969	#num#	k4
</s>
<s>
chůze	chůze	k1gFnSc1
na	na	k7c4
20	#num#	k4
km	km	kA
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
ME	ME	kA
1971	#num#	k4
</s>
<s>
chůze	chůze	k1gFnSc1
na	na	k7c4
20	#num#	k4
km	km	kA
</s>
<s>
Mykola	Mykola	k1gFnSc1
Smaha	Smaha	k?
<g/>
,	,	kIx,
ukrajinsky	ukrajinsky	k6eAd1
М	М	k?
Я	Я	k?
С	С	k?
<g/>
,	,	kIx,
rusky	rusky	k6eAd1
Н	Н	k?
Я	Я	k?
С	С	k?
<g/>
,	,	kIx,
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1938	#num#	k4
–	–	k?
28	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1981	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
sovětský	sovětský	k2eAgMnSc1d1
atlet	atlet	k1gMnSc1
<g/>
,	,	kIx,
chodec	chodec	k1gMnSc1
<g/>
,	,	kIx,
mistr	mistr	k1gMnSc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
chůzi	chůze	k1gFnSc6
na	na	k7c4
20	#num#	k4
kilometrů	kilometr	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
1971	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Sportovní	sportovní	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Prvního	první	k4xOgInSc2
mezinárodního	mezinárodní	k2eAgInSc2d1
úspěchu	úspěch	k1gInSc2
dosáhl	dosáhnout	k5eAaPmAgInS
na	na	k7c6
mistrovství	mistrovství	k1gNnSc6
Evropy	Evropa	k1gFnSc2
v	v	k7c6
Budapešti	Budapešť	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1966	#num#	k4
<g/>
,	,	kIx,
kde	kde	k6eAd1
získal	získat	k5eAaPmAgMnS
bronzovou	bronzový	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
v	v	k7c6
chůzi	chůze	k1gFnSc6
na	na	k7c4
20	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
třetí	třetí	k4xOgNnSc4
místo	místo	k1gNnSc4
v	v	k7c6
této	tento	k3xDgFnSc6
disciplíně	disciplína	k1gFnSc6
obsadil	obsadit	k5eAaPmAgMnS
na	na	k7c6
olympiádě	olympiáda	k1gFnSc6
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
i	i	k8xC
na	na	k7c6
evropském	evropský	k2eAgInSc6d1
šampionátu	šampionát	k1gInSc6
v	v	k7c6
Athénách	Athéna	k1gFnPc6
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Helsinkách	Helsinky	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1971	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
mistrem	mistr	k1gMnSc7
Evropy	Evropa	k1gFnSc2
v	v	k7c6
závodě	závod	k1gInSc6
na	na	k7c4
20	#num#	k4
kilometrů	kilometr	k1gInPc2
chůze	chůze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
olympiádě	olympiáda	k1gFnSc6
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
v	v	k7c6
následující	následující	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
obsadil	obsadit	k5eAaPmAgMnS
v	v	k7c6
této	tento	k3xDgFnSc6
disciplíně	disciplína	k1gFnSc6
páté	pátý	k4xOgNnSc4
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Mykola	Mykola	k1gFnSc1
Smaha	Smaha	k?
v	v	k7c6
databázi	databáze	k1gFnSc6
Olympedia	Olympedium	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Mistři	mistr	k1gMnPc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
chůzi	chůze	k1gFnSc6
na	na	k7c4
20	#num#	k4
km	km	kA
</s>
<s>
1946	#num#	k4
<g/>
:	:	kIx,
John	John	k1gMnSc1
Mikaelsson	Mikaelsson	k1gMnSc1
(	(	kIx(
<g/>
10	#num#	k4
km	km	kA
<g/>
)	)	kIx)
•	•	k?
1950	#num#	k4
<g/>
:	:	kIx,
Fritz	Fritz	k1gMnSc1
Schwab	Schwab	k1gMnSc1
(	(	kIx(
<g/>
10	#num#	k4
km	km	kA
<g/>
)	)	kIx)
•	•	k?
1954	#num#	k4
<g/>
:	:	kIx,
Josef	Josef	k1gMnSc1
Doležal	Doležal	k1gMnSc1
(	(	kIx(
<g/>
10	#num#	k4
km	km	kA
<g/>
)	)	kIx)
•	•	k?
1958	#num#	k4
<g/>
:	:	kIx,
Stan	stan	k1gInSc1
Vickers	Vickers	k1gInSc1
•	•	k?
1962	#num#	k4
<g/>
:	:	kIx,
Ken	Ken	k1gFnSc1
Matthews	Matthews	k1gInSc1
•	•	k?
1966	#num#	k4
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Dieter	Dieter	k1gInSc1
Lindner	Lindner	k1gInSc4
•	•	k?
1969	#num#	k4
<g/>
:	:	kIx,
Paul	Paula	k1gFnPc2
Nihill	Nihill	k1gInSc1
•	•	k?
1971	#num#	k4
<g/>
:	:	kIx,
Mykola	Mykola	k1gFnSc1
Smaha	Smaha	k?
•	•	k?
1974	#num#	k4
<g/>
:	:	kIx,
Vladimir	Vladimir	k1gMnSc1
Golubničij	Golubničij	k1gFnSc2
•	•	k?
1978	#num#	k4
<g/>
:	:	kIx,
Roland	Roland	k1gInSc1
Wieser	Wieser	k1gInSc1
•	•	k?
1982	#num#	k4
<g/>
:	:	kIx,
José	José	k1gNnSc1
Marín	Marína	k1gFnPc2
•	•	k?
1986	#num#	k4
<g/>
:	:	kIx,
Jozef	Jozef	k1gInSc1
Pribilinec	Pribilinec	k1gInSc1
•	•	k?
1990	#num#	k4
<g/>
:	:	kIx,
Pavol	Pavola	k1gFnPc2
Blažek	Blažka	k1gFnPc2
•	•	k?
1994	#num#	k4
<g/>
:	:	kIx,
Michail	Michail	k1gInSc1
Ščennikov	Ščennikov	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1998	#num#	k4
<g/>
:	:	kIx,
Ilja	Ilja	k1gFnSc1
Markov	Markov	k1gInSc1
•	•	k?
2002	#num#	k4
<g/>
:	:	kIx,
Francisco	Francisco	k6eAd1
Javier	Javier	k1gInSc1
Fernández	Fernández	k1gInSc1
•	•	k?
2006	#num#	k4
<g/>
:	:	kIx,
Francisco	Francisco	k6eAd1
Javier	Javier	k1gInSc1
Fernández	Fernández	k1gInSc1
•	•	k?
2010	#num#	k4
<g/>
:	:	kIx,
Alex	Alex	k1gMnSc1
Schwazer	Schwazra	k1gFnPc2
•	•	k?
2012	#num#	k4
<g/>
:	:	kIx,
závod	závod	k1gInSc1
neproběhl	proběhnout	k5eNaPmAgMnS
•	•	k?
2014	#num#	k4
<g/>
:	:	kIx,
Miguel	Miguel	k1gMnSc1
Ángel	Ángel	k1gMnSc1
López	López	k1gMnSc1
•	•	k?
2016	#num#	k4
<g/>
:	:	kIx,
závod	závod	k1gInSc1
neproběhl	proběhnout	k5eNaPmAgMnS
•	•	k?
2018	#num#	k4
<g/>
:	:	kIx,
Álvaro	Álvara	k1gFnSc5
Martín	Martín	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
</s>
