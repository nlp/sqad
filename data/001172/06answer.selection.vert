<s>
Jan	Jan	k1gMnSc1	Jan
Gebauer	Gebauer	k1gMnSc1	Gebauer
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1838	[number]	k4	1838
Úbislavice	Úbislavice	k1gFnSc1	Úbislavice
-	-	kIx~	-
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1907	[number]	k4	1907
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
významný	významný	k2eAgMnSc1d1	významný
český	český	k2eAgMnSc1d1	český
bohemista	bohemista	k1gMnSc1	bohemista
<g/>
,	,	kIx,	,
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
přední	přední	k2eAgMnSc1d1	přední
český	český	k2eAgMnSc1d1	český
vědec	vědec	k1gMnSc1	vědec
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejuznávanějších	uznávaný	k2eAgMnPc2d3	nejuznávanější
českých	český	k2eAgMnPc2d1	český
jazykovědců	jazykovědec	k1gMnPc2	jazykovědec
<g/>
.	.	kIx.	.
</s>
