<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
taktický	taktický	k2eAgInSc1d1
šachový	šachový	k2eAgInSc1d1
prvek	prvek	k1gInSc1
<g/>
,	,	kIx,
při	při	k7c6
němž	jenž	k3xRgNnSc6
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgMnSc1
šachový	šachový	k2eAgInSc1d1
kámen	kámen	k1gInSc1
použit	použít	k5eAaPmNgInS
k	k	k7c3
napadení	napadení	k1gNnSc6
dvou	dva	k4xCgInPc2
nebo	nebo	k8xC
více	hodně	k6eAd2
kamenů	kámen	k1gInPc2
soupeře	soupeř	k1gMnSc2
<g/>
?	?	kIx.
</s>