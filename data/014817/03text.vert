<s>
Vidlička	vidlička	k1gFnSc1
(	(	kIx(
<g/>
šachy	šach	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
používá	používat	k5eAaImIp3nS
k	k	k7c3
popisu	popis	k1gInSc3
tahů	tah	k1gInPc2
šachovou	šachový	k2eAgFnSc4d1
notaci	notace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
abcdefgh	abcdefgh	k1gInSc4
<g/>
8877665544332211	#num#	k4
<g/>
abcdefghBílý	abcdefghBílý	k1gMnSc1
jezdec	jezdec	k1gMnSc1
dává	dávat	k5eAaImIp3nS
vidličku	vidlička	k1gFnSc4
černému	černý	k1gMnSc3
králi	král	k1gMnSc3
a	a	k8xC
věži	věž	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vidlička	vidlička	k1gFnSc1
králi	král	k1gMnSc6
je	být	k5eAaImIp3nS
zvláště	zvláště	k6eAd1
účinná	účinný	k2eAgFnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
pravidla	pravidlo	k1gNnPc1
požadují	požadovat	k5eAaImIp3nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
hrozba	hrozba	k1gFnSc1
byla	být	k5eAaImAgFnS
okamžitě	okamžitě	k6eAd1
odstraněna	odstranit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Černý	Černý	k1gMnSc1
nemůže	moct	k5eNaImIp3nS
ani	ani	k8xC
bránit	bránit	k5eAaImF
druhou	druhý	k4xOgFnSc4
ohroženou	ohrožený	k2eAgFnSc4d1
figuru	figura	k1gFnSc4
ani	ani	k8xC
si	se	k3xPyFc3
v	v	k7c6
komplikované	komplikovaný	k2eAgFnSc6d1
situaci	situace	k1gFnSc6
pomoci	pomoc	k1gFnSc2
nějakým	nějaký	k3yIgInSc7
mezitahem	mezitah	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
situaci	situace	k1gFnSc6
nezbývá	zbývat	k5eNaImIp3nS
než	než	k8xS
táhnout	táhnout	k5eAaImF
králem	král	k1gMnSc7
<g/>
,	,	kIx,
takže	takže	k8xS
bílý	bílý	k1gMnSc1
bere	brát	k5eAaImIp3nS
věž	věž	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Vidlička	vidlička	k1gFnSc1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
též	též	k9
vidličky	vidlička	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
taktický	taktický	k2eAgInSc4d1
šachový	šachový	k2eAgInSc4d1
prvek	prvek	k1gInSc4
<g/>
,	,	kIx,
při	při	k7c6
němž	jenž	k3xRgNnSc6
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgMnSc1
šachový	šachový	k2eAgInSc1d1
kámen	kámen	k1gInSc1
použit	použít	k5eAaPmNgInS
k	k	k7c3
napadení	napadení	k1gNnSc6
dvou	dva	k4xCgInPc2
nebo	nebo	k8xC
více	hodně	k6eAd2
kamenů	kámen	k1gInPc2
soupeře	soupeř	k1gMnSc4
s	s	k7c7
cílem	cíl	k1gInSc7
dosáhnout	dosáhnout	k5eAaPmF
materiálního	materiální	k2eAgInSc2d1
zisku	zisk	k1gInSc2
(	(	kIx(
<g/>
odebráním	odebrání	k1gNnSc7
jednoho	jeden	k4xCgMnSc2
ze	z	k7c2
soupeřových	soupeřův	k2eAgMnPc2d1
kamenů	kámen	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
protože	protože	k8xS
soupeř	soupeř	k1gMnSc1
může	moct	k5eAaImIp3nS
odrazit	odrazit	k5eAaPmF
pouze	pouze	k6eAd1
jednu	jeden	k4xCgFnSc4
z	z	k7c2
těchto	tento	k3xDgFnPc2
hrozeb	hrozba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Dát	dát	k5eAaPmF
vidličku	vidlička	k1gFnSc4
může	moct	k5eAaImIp3nS
kterýkoliv	kterýkoliv	k3yIgInSc1
kámen	kámen	k1gInSc1
<g/>
,	,	kIx,
včetně	včetně	k7c2
krále	král	k1gMnSc2
<g/>
,	,	kIx,
i	i	k9
když	když	k8xS
nejběžnější	běžný	k2eAgFnPc1d3
jsou	být	k5eAaImIp3nP
vidličky	vidlička	k1gFnPc1
jezdcem	jezdec	k1gMnSc7
a	a	k8xC
dámou	dáma	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
však	však	k9
nemůže	moct	k5eNaImIp3nS
dávat	dávat	k5eAaImF
vidličku	vidlička	k1gFnSc4
kamenům	kámen	k1gInPc3
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
by	by	kYmCp3nP
ho	on	k3xPp3gMnSc4
současně	současně	k6eAd1
ohrozily	ohrozit	k5eAaPmAgFnP
<g/>
,	,	kIx,
protože	protože	k8xS
šachová	šachový	k2eAgNnPc1d1
pravidla	pravidlo	k1gNnPc1
nedovolují	dovolovat	k5eNaImIp3nP
vystavit	vystavit	k5eAaPmF
krále	král	k1gMnPc4
šachu	šach	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ohrožen	ohrožen	k2eAgInSc1d1
vidličkou	vidlička	k1gFnSc7
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
rovněž	rovněž	k9
jakýkoliv	jakýkoliv	k3yIgInSc4
typ	typ	k1gInSc4
kamene	kámen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
vidličkám	vidlička	k1gFnPc3
se	se	k3xPyFc4
velmi	velmi	k6eAd1
často	často	k6eAd1
používají	používat	k5eAaImIp3nP
jezdci	jezdec	k1gMnPc1
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
první	první	k4xOgInSc1
diagram	diagram	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
protože	protože	k8xS
mohou	moct	k5eAaImIp3nP
skočit	skočit	k5eAaPmF
na	na	k7c4
pozici	pozice	k1gFnSc4
<g/>
,	,	kIx,
z	z	k7c2
níž	jenž	k3xRgFnSc2
napadnou	napadnout	k5eAaPmIp3nP
dva	dva	k4xCgInPc4
soupeřovy	soupeřův	k2eAgInPc4d1
kameny	kámen	k1gInPc4
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nP
se	se	k3xPyFc4
mohly	moct	k5eAaImAgFnP
stát	stát	k5eAaPmF,k5eAaImF
obětí	oběť	k1gFnSc7
jejich	jejich	k3xOp3gInSc2
protiútoku	protiútok	k1gInSc2
(	(	kIx(
<g/>
pokud	pokud	k8xS
ovšem	ovšem	k9
mezi	mezi	k7c7
napadenými	napadený	k2eAgInPc7d1
kameny	kámen	k1gInPc7
není	být	k5eNaImIp3nS
také	také	k9
jezdec	jezdec	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dáma	dáma	k1gFnSc1
také	také	k9
často	často	k6eAd1
dává	dávat	k5eAaImIp3nS
vidličku	vidlička	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
protože	protože	k8xS
má	mít	k5eAaImIp3nS
vždy	vždy	k6eAd1
větší	veliký	k2eAgFnSc4d2
nebo	nebo	k8xC
stejnou	stejný	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
jako	jako	k8xS,k8xC
napadené	napadený	k2eAgInPc4d1
kameny	kámen	k1gInPc4
(	(	kIx(
<g/>
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
krále	král	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
lze	lze	k6eAd1
tímto	tento	k3xDgInSc7
způsobem	způsob	k1gInSc7
obvykle	obvykle	k6eAd1
získat	získat	k5eAaPmF
materiál	materiál	k1gInSc4
pouze	pouze	k6eAd1
<g/>
,	,	kIx,
pokud	pokud	k8xS
jsou	být	k5eAaImIp3nP
oba	dva	k4xCgInPc1
napadené	napadený	k2eAgInPc1d1
kameny	kámen	k1gInPc1
nechráněné	chráněný	k2eNgFnSc2d1
nebo	nebo	k8xC
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgInSc4
nechráněný	chráněný	k2eNgInSc4d1
a	a	k8xC
druhým	druhý	k4xOgMnPc3
napadeným	napadený	k2eAgMnPc3d1
je	on	k3xPp3gInPc4
král	král	k1gMnSc1
(	(	kIx(
<g/>
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
vždy	vždy	k6eAd1
při	při	k7c6
šachu	šach	k1gInSc6
bráněný	bráněný	k2eAgInSc1d1
přednostně	přednostně	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
velkému	velký	k2eAgInSc3d1
akčnímu	akční	k2eAgInSc3d1
rádiu	rádius	k1gInSc3
dámy	dáma	k1gFnSc2
je	být	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
vidličky	vidlička	k1gFnSc2
touto	tento	k3xDgFnSc7
figurou	figura	k1gFnSc7
zvláště	zvláště	k6eAd1
reálná	reálný	k2eAgFnSc1d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
na	na	k7c6
otevřeném	otevřený	k2eAgNnSc6d1
prostranství	prostranství	k1gNnSc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
často	často	k6eAd1
stává	stávat	k5eAaImIp3nS
v	v	k7c6
koncovkách	koncovka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vidlička	vidlička	k1gFnSc1
chráněnou	chráněný	k2eAgFnSc7d1
dámou	dáma	k1gFnSc7
soupeřově	soupeřův	k2eAgFnSc3d1
dámě	dáma	k1gFnSc3
a	a	k8xC
králi	král	k1gMnSc3
(	(	kIx(
<g/>
nebo	nebo	k8xC
dámě	dáma	k1gFnSc3
a	a	k8xC
nechráněnému	chráněný	k2eNgInSc3d1
kameni	kámen	k1gInSc3
<g/>
)	)	kIx)
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
užitečná	užitečný	k2eAgFnSc1d1
<g/>
,	,	kIx,
pokud	pokud	k8xS
si	se	k3xPyFc3
hráč	hráč	k1gMnSc1
chce	chtít	k5eAaImIp3nS
vynutit	vynutit	k5eAaPmF
výměnu	výměna	k1gFnSc4
dam	dáma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
vidličku	vidlička	k1gFnSc4
dámě	dáma	k1gFnSc3
a	a	k8xC
králi	král	k1gMnSc3
dává	dávat	k5eAaImIp3nS
jiný	jiný	k2eAgInSc1d1
kámen	kámen	k1gInSc1
než	než	k8xS
dáma	dáma	k1gFnSc1
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
z	z	k7c2
pohledu	pohled	k1gInSc2
napadeného	napadený	k2eAgMnSc2d1
o	o	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejhorších	zlý	k2eAgFnPc2d3
možných	možný	k2eAgFnPc2d1
situací	situace	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
téměř	téměř	k6eAd1
vždy	vždy	k6eAd1
vyústí	vyústit	k5eAaPmIp3nS
ve	v	k7c4
ztrátu	ztráta	k1gFnSc4
dámy	dáma	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pěšci	pěšec	k1gMnPc1
také	také	k9
mohou	moct	k5eAaImIp3nP
dávat	dávat	k5eAaImF
vidličky	vidlička	k1gFnPc4
soupeřovým	soupeřův	k2eAgInPc3d1
kamenům	kámen	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohybem	pohyb	k1gInSc7
pěšce	pěšec	k1gMnSc2
dopředu	dopředu	k6eAd1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
napadeny	napaden	k2eAgInPc4d1
dva	dva	k4xCgInPc4
kameny	kámen	k1gInPc4
<g/>
:	:	kIx,
jeden	jeden	k4xCgMnSc1
diagonálně	diagonálně	k6eAd1
vlevo	vlevo	k6eAd1
a	a	k8xC
druhý	druhý	k4xOgMnSc1
diagonálně	diagonálně	k6eAd1
vpravo	vpravo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
prvním	první	k4xOgInSc6
diagramu	diagram	k1gInSc6
je	být	k5eAaImIp3nS
zobrazen	zobrazen	k2eAgMnSc1d1
černý	černý	k2eAgMnSc1d1
pěšec	pěšec	k1gMnSc1
napadající	napadající	k2eAgFnPc1d1
dvě	dva	k4xCgFnPc1
bílé	bílý	k2eAgFnPc1d1
věže	věž	k1gFnPc1
(	(	kIx(
<g/>
šachovnice	šachovnice	k1gFnSc1
je	být	k5eAaImIp3nS
dle	dle	k7c2
konvence	konvence	k1gFnSc2
orientována	orientovat	k5eAaBmNgFnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
černý	černý	k1gMnSc1
měl	mít	k5eAaImAgMnS
svou	svůj	k3xOyFgFnSc4
základní	základní	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
nahoře	nahoře	k6eAd1
<g/>
,	,	kIx,
takže	takže	k8xS
černý	černý	k2eAgMnSc1d1
pěšec	pěšec	k1gMnSc1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
směrem	směr	k1gInSc7
dolů	dol	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Zatímco	zatímco	k8xS
pěšec	pěšec	k1gMnSc1
může	moct	k5eAaImIp3nS
dát	dát	k5eAaPmF
pouze	pouze	k6eAd1
jednoduchou	jednoduchý	k2eAgFnSc4d1
vidličku	vidlička	k1gFnSc4
<g/>
,	,	kIx,
vyšší	vysoký	k2eAgFnPc1d2
figury	figura	k1gFnPc1
mají	mít	k5eAaImIp3nP
možnost	možnost	k1gFnSc4
dávat	dávat	k5eAaImF
vidličky	vidlička	k1gFnPc4
vícenásobné	vícenásobný	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c6
možnosti	možnost	k1gFnSc6
svého	svůj	k3xOyFgInSc2
pohybu	pohyb	k1gInSc2
mohou	moct	k5eAaImIp3nP
věž	věž	k1gFnSc1
a	a	k8xC
střelec	střelec	k1gMnSc1
současně	současně	k6eAd1
ohrožovat	ohrožovat	k5eAaImF
až	až	k9
čtyři	čtyři	k4xCgInPc4
soupeřovy	soupeřův	k2eAgInPc4d1
kameny	kámen	k1gInPc4
a	a	k8xC
jezdec	jezdec	k1gMnSc1
<g/>
,	,	kIx,
dáma	dáma	k1gFnSc1
a	a	k8xC
král	král	k1gMnSc1
teoreticky	teoreticky	k6eAd1
až	až	k9
osm	osm	k4xCc4
kamenů	kámen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zejména	zejména	k6eAd1
u	u	k7c2
krále	král	k1gMnSc2
je	být	k5eAaImIp3nS
však	však	k9
tato	tento	k3xDgFnSc1
možnost	možnost	k1gFnSc1
extrémně	extrémně	k6eAd1
nepravděpodobná	pravděpodobný	k2eNgFnSc1d1
s	s	k7c7
ohledem	ohled	k1gInSc7
na	na	k7c4
omezené	omezený	k2eAgFnPc4d1
možnosti	možnost	k1gFnPc4
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
pohybu	pohyb	k1gInSc2
a	a	k8xC
zákaz	zákaz	k1gInSc1
napadání	napadání	k1gNnSc2
kamenů	kámen	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
by	by	kYmCp3nP
jej	on	k3xPp3gInSc4
samy	sám	k3xTgFnPc1
ohrozily	ohrozit	k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s>
Vidlička	vidlička	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jsou	být	k5eAaImIp3nP
napadeny	napaden	k2eAgFnPc1d1
dvě	dva	k4xCgFnPc1
figury	figura	k1gFnPc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
dvojí	dvojí	k4xRgInSc4
úder	úder	k1gInSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tissir-Drejev	Tissir-Drejet	k5eAaPmDgInS
<g/>
,	,	kIx,
2004	#num#	k4
</s>
<s>
abcdefgh	abcdefgh	k1gInSc1
<g/>
8877665544332211	#num#	k4
<g/>
abcdefghPozice	abcdefghPozice	k1gFnSc2
po	po	k7c4
33	#num#	k4
<g/>
.	.	kIx.
tahu	tah	k1gInSc6
bílého	bílý	k2eAgNnSc2d1
</s>
<s>
Následující	následující	k2eAgInSc1d1
příklad	příklad	k1gInSc1
vidličky	vidlička	k1gFnSc2
(	(	kIx(
<g/>
na	na	k7c6
druhém	druhý	k4xOgInSc6
diagramu	diagram	k1gInSc6
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
z	z	k7c2
prvního	první	k4xOgNnSc2
kola	kolo	k1gNnSc2
Mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
v	v	k7c6
šachu	šach	k1gInSc6
FIDE	FIDE	kA
mezi	mezi	k7c7
Mohamedem	Mohamed	k1gMnSc7
Tissirem	Tissir	k1gMnSc7
a	a	k8xC
Alexejem	Alexej	k1gMnSc7
Drejevem	Drejev	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
tazích	tag	k1gInPc6
33	#num#	k4
<g/>
.	.	kIx.
...	...	k?
Jf	Jf	k1gFnSc1
<g/>
2	#num#	k4
<g/>
+	+	kIx~
34	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kg	kg	kA
<g/>
1	#num#	k4
Jd	Jd	k1gFnSc2
<g/>
3	#num#	k4
bílý	bílý	k1gMnSc1
vzdal	vzdát	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závěrečné	závěrečný	k2eAgFnSc6d1
pozici	pozice	k1gFnSc6
černý	černý	k2eAgMnSc1d1
jezdec	jezdec	k1gMnSc1
dává	dávat	k5eAaImIp3nS
vidličku	vidlička	k1gFnSc4
dámě	dáma	k1gFnSc6
a	a	k8xC
věži	věž	k1gFnSc6
<g/>
,	,	kIx,
takže	takže	k8xS
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
bílý	bílý	k1gMnSc1
odjede	odjet	k5eAaPmIp3nS
s	s	k7c7
dámou	dáma	k1gFnSc7
<g/>
,	,	kIx,
ztratí	ztratit	k5eAaPmIp3nS
kvalitu	kvalita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Vidličky	vidlička	k1gFnPc1
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
součástí	součást	k1gFnSc7
složitějších	složitý	k2eAgFnPc2d2
šachových	šachový	k2eAgFnPc2d1
kombinací	kombinace	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
mohou	moct	k5eAaImIp3nP
zahrnovat	zahrnovat	k5eAaImF
i	i	k9
jiné	jiný	k2eAgInPc4d1
typy	typ	k1gInPc4
taktických	taktický	k2eAgInPc2d1
prvků	prvek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
PLISKA	PLISKA	k?
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Učebnice	učebnice	k1gFnSc1
šachu	šach	k1gInSc2
pro	pro	k7c4
samouky	samouk	k1gMnPc4
<g/>
,	,	kIx,
středně	středně	k6eAd1
pokročilí	pokročilý	k2eAgMnPc1d1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
Frýdek-Místek	Frýdek-Místek	k1gInSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
978-80-85232-61-5	978-80-85232-61-5	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Fork	Fork	k1gInSc1
(	(	kIx(
<g/>
chess	chess	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
GOLOMBEK	GOLOMBEK	kA
<g/>
,	,	kIx,
Harry	Harr	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Golombek	Golombek	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Chess	Chessa	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Crown	Crown	k1gInSc1
Publishing	Publishing	k1gInSc1
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
517	#num#	k4
<g/>
-	-	kIx~
<g/>
53146	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
HOOPER	HOOPER	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
Vincent	Vincent	k1gMnSc1
<g/>
;	;	kIx,
WHYLD	WHYLD	kA
<g/>
,	,	kIx,
Kenneth	Kenneth	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Oxford	Oxford	k1gInSc4
Companion	Companion	k1gInSc1
to	ten	k3xDgNnSc1
Chess	Chessa	k1gFnPc2
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
-	-	kIx~
<g/>
280049	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Chess	Chess	k6eAd1
Tactics	Tactics	k1gInSc1
Repository	Repositor	k1gInPc1
-	-	kIx~
Forks	Forks	k1gInSc1
–	–	k?
Sbírka	sbírka	k1gFnSc1
šachových	šachový	k2eAgInPc2d1
problémů	problém	k1gInPc2
s	s	k7c7
vidličkami	vidlička	k1gFnPc7
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Šachy	šach	k1gInPc1
</s>
