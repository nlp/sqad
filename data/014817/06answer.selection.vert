<s>
Vidlička	vidlička	k1gFnSc1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
též	též	k9
vidličky	vidlička	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
taktický	taktický	k2eAgInSc4d1
šachový	šachový	k2eAgInSc4d1
prvek	prvek	k1gInSc4
<g/>
,	,	kIx,
při	při	k7c6
němž	jenž	k3xRgNnSc6
je	být	k5eAaImIp3nS
jeden	jeden	k4xCgMnSc1
šachový	šachový	k2eAgInSc1d1
kámen	kámen	k1gInSc1
použit	použít	k5eAaPmNgInS
k	k	k7c3
napadení	napadení	k1gNnSc6
dvou	dva	k4xCgInPc2
nebo	nebo	k8xC
více	hodně	k6eAd2
kamenů	kámen	k1gInPc2
soupeře	soupeř	k1gMnSc4
s	s	k7c7
cílem	cíl	k1gInSc7
dosáhnout	dosáhnout	k5eAaPmF
materiálního	materiální	k2eAgInSc2d1
zisku	zisk	k1gInSc2
(	(	kIx(
<g/>
odebráním	odebrání	k1gNnSc7
jednoho	jeden	k4xCgMnSc2
ze	z	k7c2
soupeřových	soupeřův	k2eAgMnPc2d1
kamenů	kámen	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
protože	protože	k8xS
soupeř	soupeř	k1gMnSc1
může	moct	k5eAaImIp3nS
odrazit	odrazit	k5eAaPmF
pouze	pouze	k6eAd1
jednu	jeden	k4xCgFnSc4
z	z	k7c2
těchto	tento	k3xDgFnPc2
hrozeb	hrozba	k1gFnPc2
<g/>
.	.	kIx.
</s>