<p>
<s>
Centrifugace	Centrifugace	k1gFnSc1	Centrifugace
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc1	proces
rozdělení	rozdělení	k1gNnSc2	rozdělení
částic	částice	k1gFnPc2	částice
pomocí	pomocí	k7c2	pomocí
odstředivé	odstředivý	k2eAgFnSc2d1	odstředivá
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
urychlení	urychlení	k1gNnSc4	urychlení
sedimentace	sedimentace	k1gFnSc2	sedimentace
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
při	při	k7c6	při
sedimentaci	sedimentace	k1gFnSc6	sedimentace
se	se	k3xPyFc4	se
částice	částice	k1gFnPc1	částice
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
podle	podle	k7c2	podle
své	svůj	k3xOyFgFnSc2	svůj
hustoty	hustota	k1gFnSc2	hustota
vlivem	vlivem	k7c2	vlivem
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
zrychlení	zrychlení	k1gNnSc2	zrychlení
<g/>
,	,	kIx,	,
při	při	k7c6	při
centrifugaci	centrifugace	k1gFnSc6	centrifugace
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
působí	působit	k5eAaImIp3nS	působit
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgNnSc1d2	veliký
odstředivé	odstředivý	k2eAgNnSc1d1	odstředivé
zrychlení	zrychlení	k1gNnSc1	zrychlení
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
směsi	směs	k1gFnSc2	směs
proto	proto	k8xC	proto
probíhá	probíhat	k5eAaImIp3nS	probíhat
mnohem	mnohem	k6eAd1	mnohem
rychleji	rychle	k6eAd2	rychle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
nejjednodušších	jednoduchý	k2eAgInPc6d3	nejjednodušší
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
centrifugace	centrifugace	k1gFnSc1	centrifugace
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
oddělení	oddělení	k1gNnSc3	oddělení
pevných	pevný	k2eAgFnPc2d1	pevná
částic	částice	k1gFnPc2	částice
ze	z	k7c2	z
suspenze	suspenze	k1gFnSc2	suspenze
<g/>
,	,	kIx,	,
např.	např.	kA	např.
k	k	k7c3	k
odstranění	odstranění	k1gNnSc3	odstranění
sraženin	sraženina	k1gFnPc2	sraženina
či	či	k8xC	či
krvinek	krvinka	k1gFnPc2	krvinka
z	z	k7c2	z
plné	plný	k2eAgFnSc2d1	plná
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
centrifugace	centrifugace	k1gFnSc2	centrifugace
je	být	k5eAaImIp3nS	být
také	také	k9	také
možné	možný	k2eAgNnSc1d1	možné
rozdělit	rozdělit	k5eAaPmF	rozdělit
směsi	směs	k1gFnPc4	směs
nemísitelných	mísitelný	k2eNgFnPc2d1	nemísitelná
kapalin	kapalina	k1gFnPc2	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
využitím	využití	k1gNnSc7	využití
vysokých	vysoký	k2eAgFnPc2d1	vysoká
rychlostí	rychlost	k1gFnPc2	rychlost
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
ve	v	k7c6	v
vysokootáčkových	vysokootáčkový	k2eAgFnPc6d1	vysokootáčková
centrifugách	centrifuga	k1gFnPc6	centrifuga
a	a	k8xC	a
ultracentrifugách	ultracentrifuga	k1gFnPc6	ultracentrifuga
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
také	také	k9	také
separovat	separovat	k5eAaBmF	separovat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
součásti	součást	k1gFnPc4	součást
buněk	buňka	k1gFnPc2	buňka
z	z	k7c2	z
buněčného	buněčný	k2eAgInSc2d1	buněčný
lyzátu	lyzát	k1gInSc2	lyzát
nebo	nebo	k8xC	nebo
rozdělit	rozdělit	k5eAaPmF	rozdělit
směsi	směs	k1gFnPc4	směs
makromolekul	makromolekula	k1gFnPc2	makromolekula
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Centrifugační	Centrifugační	k2eAgNnSc1d1	Centrifugační
zrychlení	zrychlení	k1gNnSc1	zrychlení
==	==	k?	==
</s>
</p>
<p>
<s>
Lze	lze	k6eAd1	lze
nahlédnout	nahlédnout	k5eAaPmF	nahlédnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozdělení	rozdělení	k1gNnSc1	rozdělení
směsi	směs	k1gFnSc2	směs
při	při	k7c6	při
centrifugaci	centrifugace	k1gFnSc6	centrifugace
bude	být	k5eAaImBp3nS	být
probíhat	probíhat	k5eAaImF	probíhat
tím	ten	k3xDgNnSc7	ten
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
čím	co	k3yRnSc7	co
rychleji	rychle	k6eAd2	rychle
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
točit	točit	k5eAaImF	točit
rotor	rotor	k1gInSc4	rotor
centrifugy	centrifuga	k1gFnSc2	centrifuga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
pracuje	pracovat	k5eAaImIp3nS	pracovat
s	s	k7c7	s
veličinou	veličina	k1gFnSc7	veličina
označovanou	označovaný	k2eAgFnSc7d1	označovaná
jako	jako	k8xC	jako
relativní	relativní	k2eAgFnSc1d1	relativní
centrifugační	centrifugační	k2eAgFnSc1d1	centrifugační
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
kolikrát	kolikrát	k6eAd1	kolikrát
je	být	k5eAaImIp3nS	být
odstředivé	odstředivý	k2eAgNnSc1d1	odstředivé
zrychlení	zrychlení	k1gNnSc1	zrychlení
vyšší	vysoký	k2eAgNnSc1d2	vyšší
než	než	k8xS	než
tíhové	tíhový	k2eAgNnSc1d1	tíhové
zrychlení	zrychlení	k1gNnSc1	zrychlení
(	(	kIx(	(
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
v	v	k7c6	v
násobcích	násobek	k1gInPc6	násobek
g.	g.	k?	g.
</s>
</p>
<p>
<s>
Snadno	snadno	k6eAd1	snadno
lze	lze	k6eAd1	lze
odvodit	odvodit	k5eAaPmF	odvodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
relativní	relativní	k2eAgFnSc1d1	relativní
centrifugační	centrifugační	k2eAgFnSc1d1	centrifugační
síla	síla	k1gFnSc1	síla
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
poloměru	poloměr	k1gInSc6	poloměr
rotoru	rotor	k1gInSc2	rotor
centrifugy	centrifuga	k1gFnSc2	centrifuga
a	a	k8xC	a
počtu	počet	k1gInSc2	počet
otáček	otáčka	k1gFnPc2	otáčka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
R	R	kA	R
=	=	kIx~	=
1,12	[number]	k4	1,12
·	·	k?	·
n	n	k0	n
<g/>
2	[number]	k4	2
·	·	k?	·
r	r	kA	r
·	·	k?	·
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
5	[number]	k4	5
<g/>
kde	kde	k6eAd1	kde
R	R	kA	R
je	být	k5eAaImIp3nS	být
relativní	relativní	k2eAgFnSc1d1	relativní
centrifugační	centrifugační	k2eAgFnSc1d1	centrifugační
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
n	n	k0	n
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
otáček	otáčka	k1gFnPc2	otáčka
rotoru	rotor	k1gInSc2	rotor
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
a	a	k8xC	a
r	r	kA	r
je	být	k5eAaImIp3nS	být
poloměr	poloměr	k1gInSc4	poloměr
rotoru	rotor	k1gInSc2	rotor
v	v	k7c6	v
centimetrech	centimetr	k1gInPc6	centimetr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
částice	částice	k1gFnPc1	částice
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
během	během	k7c2	během
centrifugace	centrifugace	k1gFnSc2	centrifugace
pohybovat	pohybovat	k5eAaImF	pohybovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
sedimentační	sedimentační	k2eAgFnSc1d1	sedimentační
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
idealizovaných	idealizovaný	k2eAgFnPc2d1	idealizovaná
podmínek	podmínka	k1gFnPc2	podmínka
je	být	k5eAaImIp3nS	být
úměrná	úměrná	k1gFnSc1	úměrná
odstředivému	odstředivý	k2eAgNnSc3d1	odstředivé
zrychlení	zrychlení	k1gNnSc3	zrychlení
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
relativní	relativní	k2eAgFnSc3d1	relativní
centrifugační	centrifugační	k2eAgFnSc3d1	centrifugační
síle	síla	k1gFnSc3	síla
<g/>
)	)	kIx)	)
a	a	k8xC	a
rozdílu	rozdíl	k1gInSc2	rozdíl
hustoty	hustota	k1gFnSc2	hustota
částice	částice	k1gFnSc2	částice
a	a	k8xC	a
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
dispergována	dispergován	k2eAgFnSc1d1	dispergován
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
velikosti	velikost	k1gFnSc6	velikost
částice	částice	k1gFnSc2	částice
a	a	k8xC	a
viskozitě	viskozita	k1gFnSc6	viskozita
směsi	směs	k1gFnSc2	směs
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Namísto	namísto	k7c2	namísto
sedimentační	sedimentační	k2eAgFnSc2d1	sedimentační
rychlosti	rychlost	k1gFnSc2	rychlost
se	se	k3xPyFc4	se
často	často	k6eAd1	často
pracuje	pracovat	k5eAaImIp3nS	pracovat
se	s	k7c7	s
sedimentačním	sedimentační	k2eAgInSc7d1	sedimentační
koeficientem	koeficient	k1gInSc7	koeficient
–	–	k?	–
veličinou	veličina	k1gFnSc7	veličina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
chování	chování	k1gNnSc4	chování
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
částice	částice	k1gFnSc2	částice
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
podmínky	podmínka	k1gFnPc4	podmínka
centrifugace	centrifugace	k1gFnSc2	centrifugace
<g/>
.	.	kIx.	.
</s>
<s>
Sedimentační	sedimentační	k2eAgInSc1d1	sedimentační
koeficient	koeficient	k1gInSc1	koeficient
vlastně	vlastně	k9	vlastně
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
sedimentační	sedimentační	k2eAgFnPc4d1	sedimentační
rychlosti	rychlost	k1gFnPc4	rychlost
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
o	o	k7c6	o
jednotkovém	jednotkový	k2eAgNnSc6d1	jednotkové
zrychlení	zrychlení	k1gNnSc6	zrychlení
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
asi	asi	k9	asi
10	[number]	k4	10
<g/>
×	×	k?	×
menším	menšit	k5eAaImIp1nS	menšit
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
tíhové	tíhový	k2eAgNnSc1d1	tíhové
zrychlení	zrychlení	k1gNnSc1	zrychlení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
v	v	k7c6	v
sekundách	sekunda	k1gFnPc6	sekunda
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
násobná	násobný	k2eAgFnSc1d1	násobná
jednotka	jednotka	k1gFnSc1	jednotka
Svedberg	Svedberg	k1gMnSc1	Svedberg
(	(	kIx(	(
<g/>
1	[number]	k4	1
Svedberg	Svedberg	k1gInSc1	Svedberg
=	=	kIx~	=
1	[number]	k4	1
S	s	k7c7	s
=	=	kIx~	=
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
13	[number]	k4	13
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Centrifugy	centrifuga	k1gFnSc2	centrifuga
==	==	k?	==
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
pro	pro	k7c4	pro
zpracování	zpracování	k1gNnSc4	zpracování
materiálu	materiál	k1gInSc2	materiál
postačují	postačovat	k5eAaImIp3nP	postačovat
malé	malý	k2eAgFnPc4d1	malá
relativní	relativní	k2eAgFnPc4d1	relativní
centrifugační	centrifugační	k2eAgFnPc4d1	centrifugační
síly	síla	k1gFnPc4	síla
(	(	kIx(	(
<g/>
do	do	k7c2	do
20	[number]	k4	20
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
×	×	k?	×
g	g	kA	g
či	či	k8xC	či
ještě	ještě	k6eAd1	ještě
méně	málo	k6eAd2	málo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
poměrně	poměrně	k6eAd1	poměrně
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
nízkoobrátkové	nízkoobrátkový	k2eAgFnPc4d1	nízkoobrátkový
centrifugy	centrifuga	k1gFnPc4	centrifuga
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
sedimentaci	sedimentace	k1gFnSc4	sedimentace
velmi	velmi	k6eAd1	velmi
malých	malý	k2eAgFnPc2d1	malá
částic	částice	k1gFnPc2	částice
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
ultracentrifugy	ultracentrifuga	k1gFnPc1	ultracentrifuga
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
rotor	rotor	k1gInSc1	rotor
pracuje	pracovat	k5eAaImIp3nS	pracovat
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
zamezí	zamezit	k5eAaPmIp3nP	zamezit
jeho	jeho	k3xOp3gMnPc1	jeho
brždění	bržděný	k2eAgMnPc1d1	bržděný
vzduchem	vzduch	k1gInSc7	vzduch
a	a	k8xC	a
zahřívání	zahřívání	k1gNnSc4	zahřívání
<g/>
.	.	kIx.	.
</s>
<s>
Součásti	součást	k1gFnPc1	součást
ultracentrifug	ultracentrifuga	k1gFnPc2	ultracentrifuga
musejí	muset	k5eAaImIp3nP	muset
odolávat	odolávat	k5eAaImF	odolávat
obrovským	obrovský	k2eAgFnPc3d1	obrovská
silám	síla	k1gFnPc3	síla
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
velmi	velmi	k6eAd1	velmi
náročné	náročný	k2eAgInPc1d1	náročný
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
<g/>
;	;	kIx,	;
cena	cena	k1gFnSc1	cena
ultracentrifug	ultracentrifuga	k1gFnPc2	ultracentrifuga
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
nízkootáčkovými	nízkootáčkův	k2eAgFnPc7d1	nízkootáčkův
centrifugami	centrifuga	k1gFnPc7	centrifuga
a	a	k8xC	a
ultracentrifugami	ultracentrifuga	k1gFnPc7	ultracentrifuga
stojí	stát	k5eAaImIp3nS	stát
tzv.	tzv.	kA	tzv.
vysokootáčkové	vysokootáčkový	k2eAgFnSc2d1	vysokootáčková
centrifugy	centrifuga	k1gFnSc2	centrifuga
<g/>
.	.	kIx.	.
</s>
<s>
Pracují	pracovat	k5eAaImIp3nP	pracovat
bez	bez	k7c2	bez
vakua	vakuum	k1gNnSc2	vakuum
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
však	však	k9	však
pomocí	pomocí	k7c2	pomocí
nich	on	k3xPp3gMnPc2	on
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
zrychlení	zrychlení	k1gNnSc1	zrychlení
až	až	k6eAd1	až
kolem	kolem	k7c2	kolem
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
×	×	k?	×
g.	g.	k?	g.
</s>
</p>
<p>
<s>
Centrifugování	centrifugování	k1gNnSc1	centrifugování
při	při	k7c6	při
vyšších	vysoký	k2eAgFnPc6d2	vyšší
otáčkách	otáčka	k1gFnPc6	otáčka
by	by	kYmCp3nS	by
kvůli	kvůli	k7c3	kvůli
tření	tření	k1gNnSc3	tření
o	o	k7c4	o
vzduch	vzduch	k1gInSc4	vzduch
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
zahřívání	zahřívání	k1gNnSc3	zahřívání
vzorku	vzorek	k1gInSc2	vzorek
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
biologické	biologický	k2eAgInPc1d1	biologický
vzorky	vzorek	k1gInPc1	vzorek
často	často	k6eAd1	často
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
zpracovávány	zpracovávat	k5eAaImNgInP	zpracovávat
při	při	k7c6	při
nízkých	nízký	k2eAgFnPc6d1	nízká
teplotách	teplota	k1gFnPc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Centrifugy	centrifuga	k1gFnPc1	centrifuga
proto	proto	k8xC	proto
bývají	bývat	k5eAaImIp3nP	bývat
často	často	k6eAd1	často
vybaveny	vybavit	k5eAaPmNgInP	vybavit
chlazením	chlazení	k1gNnSc7	chlazení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
je	být	k5eAaImIp3nS	být
použit	použít	k5eAaPmNgInS	použít
text	text	k1gInSc1	text
článku	článek	k1gInSc2	článek
Centrifugace	Centrifugace	k1gFnSc2	Centrifugace
ve	v	k7c6	v
WikiSkriptech	WikiSkript	k1gInPc6	WikiSkript
českých	český	k2eAgFnPc2d1	Česká
a	a	k8xC	a
slovenských	slovenský	k2eAgFnPc2d1	slovenská
lékařských	lékařský	k2eAgFnPc2d1	lékařská
fakult	fakulta	k1gFnPc2	fakulta
zapojených	zapojený	k2eAgFnPc2d1	zapojená
v	v	k7c6	v
MEFANETu	MEFANETus	k1gInSc6	MEFANETus
<g/>
.	.	kIx.	.
</s>
</p>
