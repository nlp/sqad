<p>
<s>
ABAP	ABAP	kA	ABAP
je	být	k5eAaImIp3nS	být
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
používaný	používaný	k2eAgInSc1d1	používaný
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
aplikací	aplikace	k1gFnPc2	aplikace
mySAP	mySAP	k?	mySAP
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
od	od	k7c2	od
firmy	firma	k1gFnSc2	firma
SAP	sapa	k1gFnPc2	sapa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
70	[number]	k4	70
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
:	:	kIx,	:
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
makro	makro	k1gNnSc4	makro
assembler	assembler	k1gInSc4	assembler
pro	pro	k7c4	pro
generování	generování	k1gNnSc4	generování
reportů	report	k1gInPc2	report
v	v	k7c6	v
systémech	systém	k1gInPc6	systém
R	R	kA	R
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
"	"	kIx"	"
<g/>
Allgemeiner	Allgemeinero	k1gNnPc2	Allgemeinero
Berichts-Aufbereitungs	Berichts-Aufbereitungsa	k1gFnPc2	Berichts-Aufbereitungsa
Prozessor	Prozessora	k1gFnPc2	Prozessora
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
80	[number]	k4	80
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
<g/>
:	:	kIx,	:
ABAP	ABAP	kA	ABAP
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
interpretačního	interpretační	k2eAgInSc2d1	interpretační
jazyka	jazyk	k1gInSc2	jazyk
pro	pro	k7c4	pro
aplikace	aplikace	k1gFnPc4	aplikace
v	v	k7c6	v
R	R	kA	R
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Umožňoval	umožňovat	k5eAaImAgInS	umožňovat
programovat	programovat	k5eAaImF	programovat
tzv.	tzv.	kA	tzv.
dialogově	dialogově	k6eAd1	dialogově
řízené	řízený	k2eAgFnSc2d1	řízená
transakce	transakce	k1gFnSc2	transakce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Začátek	začátek	k1gInSc1	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
:	:	kIx,	:
V	v	k7c6	v
systému	systém	k1gInSc6	systém
R	R	kA	R
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
4	[number]	k4	4
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc1	generace
<g/>
"	"	kIx"	"
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
ABAP	ABAP	kA	ABAP
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Advanced	Advanced	k1gInSc1	Advanced
Business	business	k1gInSc1	business
Application	Application	k1gInSc1	Application
Programming	Programming	k1gInSc1	Programming
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
jsou	být	k5eAaImIp3nP	být
aplikace	aplikace	k1gFnPc1	aplikace
pro	pro	k7c4	pro
produkty	produkt	k1gInPc4	produkt
mySAP	mySAP	k?	mySAP
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
psány	psát	k5eAaImNgInP	psát
v	v	k7c4	v
ABAP	ABAP	kA	ABAP
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
systémové	systémový	k2eAgNnSc1d1	systémové
jádro	jádro	k1gNnSc1	jádro
je	být	k5eAaImIp3nS	být
psáno	psát	k5eAaImNgNnS	psát
v	v	k7c6	v
programovacím	programovací	k2eAgInSc6d1	programovací
jazyku	jazyk	k1gInSc6	jazyk
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
:	:	kIx,	:
začala	začít	k5eAaPmAgFnS	začít
nová	nový	k2eAgFnSc1d1	nová
etapa	etapa	k1gFnSc1	etapa
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
objektové	objektový	k2eAgNnSc1d1	objektové
rozšíření	rozšíření	k1gNnSc1	rozšíření
jazyka	jazyk	k1gInSc2	jazyk
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
ABAP	ABAP	kA	ABAP
Objects	Objectsa	k1gFnPc2	Objectsa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
používat	používat	k5eAaImF	používat
i	i	k9	i
jazyk	jazyk	k1gInSc4	jazyk
Java	Jav	k1gInSc2	Jav
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
aplikací	aplikace	k1gFnPc2	aplikace
mySAP	mySAP	k?	mySAP
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
silně	silně	k6eAd1	silně
i	i	k9	i
slabě	slabě	k6eAd1	slabě
typový	typový	k2eAgMnSc1d1	typový
zároveň	zároveň	k6eAd1	zároveň
</s>
</p>
<p>
<s>
má	mít	k5eAaImIp3nS	mít
vestavěnou	vestavěný	k2eAgFnSc4d1	vestavěná
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
SQL	SQL	kA	SQL
(	(	kIx(	(
<g/>
OpenSQL	OpenSQL	k1gMnSc1	OpenSQL
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
syntaxi	syntax	k1gFnSc6	syntax
jazyka	jazyk	k1gInSc2	jazyk
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Ukázka	ukázka	k1gFnSc1	ukázka
==	==	k?	==
</s>
</p>
<p>
<s>
Ukázka	ukázka	k1gFnSc1	ukázka
definice	definice	k1gFnSc1	definice
<g/>
,	,	kIx,	,
implementování	implementování	k1gNnSc1	implementování
a	a	k8xC	a
použití	použití	k1gNnSc1	použití
jednoduché	jednoduchý	k2eAgFnSc2d1	jednoduchá
lokální	lokální	k2eAgFnSc2d1	lokální
třídy	třída	k1gFnSc2	třída
(	(	kIx(	(
<g/>
Local	Local	k1gInSc1	Local
class	class	k1gInSc1	class
<g/>
)	)	kIx)	)
v	v	k7c6	v
reportu	report	k1gInSc6	report
<g/>
/	/	kIx~	/
<g/>
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Modifikovaný	modifikovaný	k2eAgInSc4d1	modifikovaný
kód	kód	k1gInSc4	kód
ze	z	k7c2	z
SAP	sapa	k1gFnPc2	sapa
Help	help	k1gInSc1	help
Classes	Classes	k1gMnSc1	Classes
-	-	kIx~	-
Introductory	Introductor	k1gInPc1	Introductor
Example	Example	k1gFnPc2	Example
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Using	Using	k1gMnSc1	Using
ABAP	ABAP	kA	ABAP
</s>
</p>
<p>
<s>
ABAP	ABAP	kA	ABAP
Training	Training	k1gInSc1	Training
Materials	Materials	k1gInSc1	Materials
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Keller	Keller	k1gMnSc1	Keller
<g/>
,	,	kIx,	,
Horst	Horst	k1gMnSc1	Horst
;	;	kIx,	;
Krüger	Krüger	k1gMnSc1	Krüger
<g/>
,	,	kIx,	,
Sascha	Sascha	k1gMnSc1	Sascha
<g/>
,	,	kIx,	,
ABAP	ABAP	kA	ABAP
Objects	Objects	k1gInSc1	Objects
<g/>
,	,	kIx,	,
Addison-Wesley	Addison-Wesley	k1gInPc1	Addison-Wesley
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
201	[number]	k4	201
<g/>
-	-	kIx~	-
<g/>
75080	[number]	k4	75080
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
