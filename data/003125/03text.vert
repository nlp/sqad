<s>
Silnice	silnice	k1gFnSc1	silnice
je	být	k5eAaImIp3nS	být
dopravní	dopravní	k2eAgFnSc1d1	dopravní
stavba	stavba	k1gFnSc1	stavba
umožňující	umožňující	k2eAgFnSc4d1	umožňující
nekolejovou	kolejový	k2eNgFnSc4d1	nekolejová
pozemní	pozemní	k2eAgFnSc4d1	pozemní
dopravu	doprava	k1gFnSc4	doprava
na	na	k7c6	na
větší	veliký	k2eAgFnSc6d2	veliký
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
silnice	silnice	k1gFnSc2	silnice
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
císař	císař	k1gMnSc1	císař
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
nechal	nechat	k5eAaPmAgMnS	nechat
zesílit	zesílit	k5eAaPmF	zesílit
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
hlavní	hlavní	k2eAgFnSc2d1	hlavní
cesty	cesta	k1gFnSc2	cesta
(	(	kIx(	(
<g/>
silná	silný	k2eAgFnSc1d1	silná
cesta	cesta	k1gFnSc1	cesta
=	=	kIx~	=
silnice	silnice	k1gFnSc1	silnice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
se	se	k3xPyFc4	se
v	v	k7c6	v
obecném	obecný	k2eAgInSc6d1	obecný
jazyce	jazyk	k1gInSc6	jazyk
pro	pro	k7c4	pro
silnici	silnice	k1gFnSc4	silnice
běžně	běžně	k6eAd1	běžně
užívá	užívat	k5eAaImIp3nS	užívat
slova	slovo	k1gNnSc2	slovo
cesta	cesta	k1gFnSc1	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Silnice	silnice	k1gFnSc1	silnice
často	často	k6eAd1	často
využívá	využívat	k5eAaPmIp3nS	využívat
pro	pro	k7c4	pro
překonání	překonání	k1gNnSc4	překonání
terénních	terénní	k2eAgFnPc2d1	terénní
překážek	překážka	k1gFnPc2	překážka
dalších	další	k2eAgFnPc2d1	další
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
mosty	most	k1gInPc1	most
či	či	k8xC	či
tunely	tunel	k1gInPc1	tunel
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
ještě	ještě	k9	ještě
zářezy	zářez	k1gInPc1	zářez
<g/>
,	,	kIx,	,
odřezy	odřez	k1gInPc1	odřez
nebo	nebo	k8xC	nebo
náspy	násep	k1gInPc1	násep
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
české	český	k2eAgFnSc6d1	Česká
terminologii	terminologie	k1gFnSc6	terminologie
je	být	k5eAaImIp3nS	být
silnice	silnice	k1gFnSc1	silnice
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
kategorií	kategorie	k1gFnPc2	kategorie
pozemních	pozemní	k2eAgFnPc2d1	pozemní
komunikací	komunikace	k1gFnPc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc1	termín
silnice	silnice	k1gFnSc2	silnice
protikladem	protiklad	k1gInSc7	protiklad
k	k	k7c3	k
termínům	termín	k1gInPc3	termín
účelová	účelový	k2eAgFnSc1d1	účelová
komunikace	komunikace	k1gFnSc1	komunikace
<g/>
,	,	kIx,	,
místní	místní	k2eAgFnSc1d1	místní
komunikace	komunikace	k1gFnSc1	komunikace
a	a	k8xC	a
dálnice	dálnice	k1gFnSc1	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
ulice	ulice	k1gFnSc2	ulice
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
neoficiální	oficiální	k2eNgNnSc1d1	neoficiální
označení	označení	k1gNnSc1	označení
pozemních	pozemní	k2eAgFnPc2d1	pozemní
komunikací	komunikace	k1gFnPc2	komunikace
ve	v	k7c6	v
městech	město	k1gNnPc6	město
a	a	k8xC	a
významem	význam	k1gInSc7	význam
má	mít	k5eAaImIp3nS	mít
nejblíže	blízce	k6eAd3	blízce
k	k	k7c3	k
termínu	termín	k1gInSc3	termín
místní	místní	k2eAgFnSc2d1	místní
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
silnice	silnice	k1gFnPc1	silnice
stavěly	stavět	k5eAaImAgFnP	stavět
už	už	k6eAd1	už
starověké	starověký	k2eAgFnPc1d1	starověká
civilizace	civilizace	k1gFnPc1	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgInPc1d1	významný
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
silnice	silnice	k1gFnPc4	silnice
římského	římský	k2eAgNnSc2d1	římské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnPc1d3	nejznámější
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
Via	via	k7c4	via
Appia	Appium	k1gNnPc4	Appium
<g/>
,	,	kIx,	,
nejdelší	dlouhý	k2eAgFnPc4d3	nejdelší
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
dláždených	dláždený	k2eAgFnPc2d1	dláždený
silnic	silnice	k1gFnPc2	silnice
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
římské	římský	k2eAgFnSc6d1	římská
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
rozvoje	rozvoj	k1gInSc2	rozvoj
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
revoluce	revoluce	k1gFnSc2	revoluce
platilo	platit	k5eAaImAgNnS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	čí	k3xOyRgNnSc7	čí
je	být	k5eAaImIp3nS	být
silnice	silnice	k1gFnSc1	silnice
horší	zlý	k2eAgFnSc1d2	horší
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
více	hodně	k6eAd2	hodně
zbrzdí	zbrzdit	k5eAaPmIp3nS	zbrzdit
protivníka	protivník	k1gMnSc4	protivník
při	při	k7c6	při
případném	případný	k2eAgInSc6d1	případný
vpádu	vpád	k1gInSc6	vpád
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
stavěly	stavět	k5eAaImAgFnP	stavět
silnice	silnice	k1gFnPc1	silnice
štěrkové	štěrkový	k2eAgFnPc1d1	štěrková
<g/>
,	,	kIx,	,
podložené	podložený	k2eAgFnPc1d1	podložená
několika	několik	k4yIc7	několik
druhy	druh	k1gMnPc7	druh
hrubého	hrubý	k2eAgInSc2d1	hrubý
štěrku	štěrk	k1gInSc2	štěrk
<g/>
,	,	kIx,	,
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
automobilové	automobilový	k2eAgFnSc2d1	automobilová
dopravy	doprava	k1gFnSc2	doprava
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
až	až	k9	až
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
stavěly	stavět	k5eAaImAgFnP	stavět
dlážděné	dlážděný	k2eAgFnPc1d1	dlážděná
silnice	silnice	k1gFnPc1	silnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
začínají	začínat	k5eAaImIp3nP	začínat
objevovat	objevovat	k5eAaImF	objevovat
i	i	k9	i
první	první	k4xOgFnSc1	první
dálnice	dálnice	k1gFnSc1	dálnice
<g/>
,	,	kIx,	,
postavené	postavený	k2eAgFnPc1d1	postavená
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
za	za	k7c2	za
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
ve	v	k7c4	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
z	z	k7c2	z
betonu	beton	k1gInSc2	beton
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
Mezistátní	mezistátní	k2eAgInSc1d1	mezistátní
dálniční	dálniční	k2eAgInSc1d1	dálniční
systém	systém	k1gInSc1	systém
D.	D.	kA	D.
Eisenhowera	Eisenhower	k1gMnSc2	Eisenhower
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
byly	být	k5eAaImAgInP	být
beton	beton	k1gInSc4	beton
i	i	k8xC	i
dlažba	dlažba	k1gFnSc1	dlažba
nahrazeny	nahradit	k5eAaPmNgFnP	nahradit
asfaltem	asfalt	k1gInSc7	asfalt
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
staví	stavit	k5eAaBmIp3nS	stavit
povrch	povrch	k1gInSc1	povrch
silnic	silnice	k1gFnPc2	silnice
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovým	klíčový	k2eAgInSc7d1	klíčový
úkolem	úkol	k1gInSc7	úkol
ve	v	k7c6	v
stavbě	stavba	k1gFnSc6	stavba
dnešních	dnešní	k2eAgFnPc2d1	dnešní
silnic	silnice	k1gFnPc2	silnice
je	být	k5eAaImIp3nS	být
dostavět	dostavět	k5eAaPmF	dostavět
celoevropskou	celoevropský	k2eAgFnSc4d1	celoevropská
dálniční	dálniční	k2eAgFnSc4d1	dálniční
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
hlavně	hlavně	k9	hlavně
nových	nový	k2eAgFnPc2d1	nová
členských	členský	k2eAgFnPc2d1	členská
zemí	zem	k1gFnPc2	zem
EU	EU	kA	EU
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k8xC	i
jejich	jejich	k3xOp3gFnPc2	jejich
kandidátských	kandidátský	k2eAgFnPc2d1	kandidátská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
pozemních	pozemní	k2eAgFnPc6d1	pozemní
komunikacích	komunikace	k1gFnPc6	komunikace
(	(	kIx(	(
<g/>
č.	č.	k?	č.
13	[number]	k4	13
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
tři	tři	k4xCgFnPc1	tři
třídy	třída	k1gFnPc1	třída
silnic	silnice	k1gFnPc2	silnice
<g/>
:	:	kIx,	:
Silnice	silnice	k1gFnSc1	silnice
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
dálkovou	dálkový	k2eAgFnSc4d1	dálková
a	a	k8xC	a
mezistátní	mezistátní	k2eAgFnSc4d1	mezistátní
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
jednomístným	jednomístný	k2eAgMnSc7d1	jednomístný
nebo	nebo	k8xC	nebo
dvojmístným	dvojmístný	k2eAgNnSc7d1	dvojmístné
číslem	číslo	k1gNnSc7	číslo
<g/>
,	,	kIx,	,
před	před	k7c7	před
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
uvádí	uvádět	k5eAaImIp3nS	uvádět
ještě	ještě	k9	ještě
římské	římský	k2eAgNnSc1d1	římské
číslo	číslo	k1gNnSc1	číslo
I	i	k9	i
oddělené	oddělený	k2eAgNnSc1d1	oddělené
lomítkem	lomítko	k1gNnSc7	lomítko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
čísla	číslo	k1gNnPc4	číslo
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
71	[number]	k4	71
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
systému	systém	k1gInSc6	systém
jsou	být	k5eAaImIp3nP	být
číslovány	číslován	k2eAgFnPc1d1	číslována
i	i	k9	i
dálnice	dálnice	k1gFnSc1	dálnice
<g/>
,	,	kIx,	,
před	před	k7c4	před
jejichž	jejichž	k3xOyRp3gNnSc4	jejichž
číslo	číslo	k1gNnSc4	číslo
se	se	k3xPyFc4	se
vkládá	vkládat	k5eAaImIp3nS	vkládat
písmeno	písmeno	k1gNnSc1	písmeno
D.	D.	kA	D.
Některé	některý	k3yIgInPc1	některý
úseky	úsek	k1gInPc1	úsek
silnic	silnice	k1gFnPc2	silnice
I.	I.	kA	I.
třídy	třída	k1gFnPc1	třída
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
obdobné	obdobný	k2eAgNnSc4d1	obdobné
stavebně	stavebně	k6eAd1	stavebně
technické	technický	k2eAgNnSc4d1	technické
vybavení	vybavení	k1gNnSc4	vybavení
jako	jako	k8xC	jako
dálnice	dálnice	k1gFnPc4	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
Silnice	silnice	k1gFnSc1	silnice
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
dopravu	doprava	k1gFnSc4	doprava
mezi	mezi	k7c7	mezi
okresy	okres	k1gInPc7	okres
<g/>
.	.	kIx.	.
</s>
<s>
Označuje	označovat	k5eAaImIp3nS	označovat
se	s	k7c7	s
trojmístným	trojmístný	k2eAgNnSc7d1	trojmístné
číslem	číslo	k1gNnSc7	číslo
<g/>
,	,	kIx,	,
před	před	k7c7	před
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
uvádí	uvádět	k5eAaImIp3nS	uvádět
ještě	ještě	k9	ještě
římské	římský	k2eAgNnSc1d1	římské
číslo	číslo	k1gNnSc1	číslo
II	II	kA	II
oddělené	oddělený	k2eAgFnPc4d1	oddělená
lomítkem	lomítko	k1gNnSc7	lomítko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
cca	cca	kA	cca
450	[number]	k4	450
<g/>
.	.	kIx.	.
</s>
<s>
Silnice	silnice	k1gFnSc1	silnice
III	III	kA	III
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
je	být	k5eAaImIp3nS	být
určena	určen	k2eAgFnSc1d1	určena
k	k	k7c3	k
vzájemnému	vzájemný	k2eAgNnSc3d1	vzájemné
spojení	spojení	k1gNnSc3	spojení
obcí	obec	k1gFnPc2	obec
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gNnSc2	jejich
napojení	napojení	k1gNnSc2	napojení
na	na	k7c4	na
ostatní	ostatní	k2eAgFnPc4d1	ostatní
pozemní	pozemní	k2eAgFnPc4d1	pozemní
komunikace	komunikace	k1gFnPc4	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
terénu	terén	k1gInSc6	terén
ani	ani	k8xC	ani
v	v	k7c6	v
mapách	mapa	k1gFnPc6	mapa
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
číslem	číslo	k1gNnSc7	číslo
neoznačují	označovat	k5eNaImIp3nP	označovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úředních	úřední	k2eAgInPc6d1	úřední
dokumentech	dokument	k1gInPc6	dokument
a	a	k8xC	a
specializovaných	specializovaný	k2eAgFnPc6d1	specializovaná
mapách	mapa	k1gFnPc6	mapa
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
čtyř-	čtyř-	k?	čtyř-
nebo	nebo	k8xC	nebo
pětimístným	pětimístný	k2eAgNnSc7d1	pětimístné
číslem	číslo	k1gNnSc7	číslo
<g/>
,	,	kIx,	,
před	před	k7c7	před
nímž	jenž	k3xRgMnSc7	jenž
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
uvádí	uvádět	k5eAaImIp3nS	uvádět
ještě	ještě	k9	ještě
římské	římský	k2eAgNnSc1d1	římské
číslo	číslo	k1gNnSc1	číslo
III	III	kA	III
oddělené	oddělený	k2eAgFnPc4d1	oddělená
lomítkem	lomítko	k1gNnSc7	lomítko
<g/>
.	.	kIx.	.
</s>
<s>
Číslo	číslo	k1gNnSc1	číslo
obvykle	obvykle	k6eAd1	obvykle
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
některé	některý	k3yIgFnSc2	některý
silnice	silnice	k1gFnSc2	silnice
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yQgFnSc4	který
se	se	k3xPyFc4	se
napojuje	napojovat	k5eAaImIp3nS	napojovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
silnice	silnice	k1gFnSc2	silnice
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
128	[number]	k4	128
je	být	k5eAaImIp3nS	být
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
12801	[number]	k4	12801
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
z	z	k7c2	z
čísla	číslo	k1gNnSc2	číslo
silnice	silnice	k1gFnSc2	silnice
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
(	(	kIx(	(
<g/>
Z	z	k7c2	z
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
19	[number]	k4	19
např.	např.	kA	např.
III	III	kA	III
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
třídy	třída	k1gFnPc1	třída
(	(	kIx(	(
<g/>
I.	I.	kA	I.
až	až	k8xS	až
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
funkční	funkční	k2eAgFnPc1d1	funkční
třídy	třída	k1gFnPc1	třída
A-D	A-D	k1gFnSc4	A-D
i	i	k9	i
u	u	k7c2	u
místních	místní	k2eAgFnPc2d1	místní
komunikací	komunikace	k1gFnPc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Vlastníkem	vlastník	k1gMnSc7	vlastník
dálnic	dálnice	k1gFnPc2	dálnice
a	a	k8xC	a
silnic	silnice	k1gFnPc2	silnice
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
<g/>
.	.	kIx.	.
</s>
<s>
Vlastníkem	vlastník	k1gMnSc7	vlastník
silnic	silnice	k1gFnPc2	silnice
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
III	III	kA	III
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
je	být	k5eAaImIp3nS	být
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
silnice	silnice	k1gFnPc1	silnice
nacházejí	nacházet	k5eAaImIp3nP	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Vlastníkem	vlastník	k1gMnSc7	vlastník
místních	místní	k2eAgFnPc2d1	místní
komunikací	komunikace	k1gFnPc2	komunikace
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc1	obec
<g/>
.	.	kIx.	.
</s>
<s>
Silnice	silnice	k1gFnPc1	silnice
a	a	k8xC	a
místní	místní	k2eAgFnPc1d1	místní
komunikace	komunikace	k1gFnPc1	komunikace
jsou	být	k5eAaImIp3nP	být
za	za	k7c2	za
stanovených	stanovený	k2eAgFnPc2d1	stanovená
podmínek	podmínka	k1gFnPc2	podmínka
veřejně	veřejně	k6eAd1	veřejně
přístupné	přístupný	k2eAgFnPc4d1	přístupná
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákonem	zákon	k1gInSc7	zákon
daného	daný	k2eAgNnSc2d1	dané
práva	právo	k1gNnSc2	právo
obecného	obecný	k2eAgNnSc2d1	obecné
užívání	užívání	k1gNnSc2	užívání
(	(	kIx(	(
<g/>
§	§	k?	§
19	[number]	k4	19
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
13	[number]	k4	13
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dálnic	dálnice	k1gFnPc2	dálnice
je	být	k5eAaImIp3nS	být
užívání	užívání	k1gNnSc1	užívání
podmíněno	podmínit	k5eAaPmNgNnS	podmínit
dálničním	dálniční	k2eAgInSc7d1	dálniční
poplatkem	poplatek	k1gInSc7	poplatek
<g/>
.	.	kIx.	.
</s>
<s>
Silniční	silniční	k2eAgNnSc1d1	silniční
ochranné	ochranný	k2eAgNnSc1d1	ochranné
pásmo	pásmo	k1gNnSc1	pásmo
je	být	k5eAaImIp3nS	být
definováno	definovat	k5eAaBmNgNnS	definovat
mimo	mimo	k7c4	mimo
souvisle	souvisle	k6eAd1	souvisle
zastavěné	zastavěný	k2eAgNnSc4d1	zastavěné
území	území	k1gNnSc4	území
u	u	k7c2	u
rychlostních	rychlostní	k2eAgFnPc2d1	rychlostní
komunikací	komunikace	k1gFnPc2	komunikace
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
100	[number]	k4	100
m	m	kA	m
od	od	k7c2	od
osy	osa	k1gFnSc2	osa
přilehlého	přilehlý	k2eAgInSc2d1	přilehlý
jízdního	jízdní	k2eAgInSc2d1	jízdní
pásu	pás	k1gInSc2	pás
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
reklamy	reklama	k1gFnPc4	reklama
<g/>
,	,	kIx,	,
poutače	poutač	k1gInPc4	poutač
a	a	k8xC	a
světelná	světelný	k2eAgNnPc4d1	světelné
zařízení	zařízení	k1gNnPc4	zařízení
250	[number]	k4	250
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
ostatních	ostatní	k2eAgFnPc2d1	ostatní
komunikací	komunikace	k1gFnPc2	komunikace
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
50	[number]	k4	50
m	m	kA	m
od	od	k7c2	od
osy	osa	k1gFnSc2	osa
přilehlého	přilehlý	k2eAgInSc2d1	přilehlý
jízdního	jízdní	k2eAgInSc2d1	jízdní
pásu	pás	k1gInSc2	pás
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
vozovky	vozovka	k1gFnSc2	vozovka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
silnic	silnice	k1gFnPc2	silnice
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
III	III	kA	III
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
a	a	k8xC	a
místních	místní	k2eAgFnPc2d1	místní
komunikací	komunikace	k1gFnPc2	komunikace
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
15	[number]	k4	15
m	m	kA	m
od	od	k7c2	od
osy	osa	k1gFnSc2	osa
přilehlého	přilehlý	k2eAgInSc2d1	přilehlý
jízdního	jízdní	k2eAgInSc2d1	jízdní
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
silničním	silniční	k2eAgNnSc6d1	silniční
ochranném	ochranný	k2eAgNnSc6d1	ochranné
pásmu	pásmo	k1gNnSc6	pásmo
podléhá	podléhat	k5eAaImIp3nS	podléhat
stavební	stavební	k2eAgFnSc1d1	stavební
<g/>
,	,	kIx,	,
reklamní	reklamní	k2eAgFnSc4d1	reklamní
i	i	k8xC	i
pěstební	pěstební	k2eAgFnSc4d1	pěstební
činnost	činnost	k1gFnSc4	činnost
přísnějším	přísný	k2eAgFnPc3d2	přísnější
podmínkám	podmínka	k1gFnPc3	podmínka
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
souhlasu	souhlas	k1gInSc3	souhlas
silničního	silniční	k2eAgInSc2d1	silniční
správního	správní	k2eAgInSc2d1	správní
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Vlastníci	vlastník	k1gMnPc1	vlastník
pozemků	pozemek	k1gInPc2	pozemek
v	v	k7c6	v
ochranném	ochranný	k2eAgNnSc6d1	ochranné
pásmu	pásmo	k1gNnSc6	pásmo
jsou	být	k5eAaImIp3nP	být
povinni	povinen	k2eAgMnPc1d1	povinen
strpět	strpět	k5eAaPmF	strpět
nezbytné	zbytný	k2eNgFnSc2d1	zbytný
činnosti	činnost	k1gFnSc2	činnost
vlastníka	vlastník	k1gMnSc2	vlastník
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c4	o
zařazení	zařazení	k1gNnSc4	zařazení
pozemní	pozemní	k2eAgFnSc2d1	pozemní
komunikace	komunikace	k1gFnSc2	komunikace
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
silnice	silnice	k1gFnSc2	silnice
nebo	nebo	k8xC	nebo
dálnice	dálnice	k1gFnSc2	dálnice
a	a	k8xC	a
o	o	k7c4	o
zrušení	zrušení	k1gNnSc4	zrušení
silnice	silnice	k1gFnSc2	silnice
nebo	nebo	k8xC	nebo
dálnice	dálnice	k1gFnSc2	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
Vykonává	vykonávat	k5eAaImIp3nS	vykonávat
působnost	působnost	k1gFnSc4	působnost
silničního	silniční	k2eAgInSc2d1	silniční
správního	správní	k2eAgInSc2d1	správní
úřadu	úřad	k1gInSc2	úřad
a	a	k8xC	a
speciálního	speciální	k2eAgInSc2d1	speciální
stavebního	stavební	k2eAgInSc2d1	stavební
úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
dálnice	dálnice	k1gFnPc4	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
Rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c6	o
odvoláních	odvolání	k1gNnPc6	odvolání
proti	proti	k7c3	proti
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
krajských	krajský	k2eAgMnPc2d1	krajský
úřadů	úřad	k1gInPc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Krajské	krajský	k2eAgInPc1d1	krajský
úřady	úřad	k1gInPc1	úřad
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
působnost	působnost	k1gFnSc4	působnost
silničního	silniční	k2eAgInSc2d1	silniční
správního	správní	k2eAgInSc2d1	správní
úřadu	úřad	k1gInSc2	úřad
a	a	k8xC	a
speciálního	speciální	k2eAgInSc2d1	speciální
stavebního	stavební	k2eAgInSc2d1	stavební
úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
silnice	silnice	k1gFnPc4	silnice
I.	I.	kA	I.
třídy	třída	k1gFnPc4	třída
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
kompetencí	kompetence	k1gFnPc2	kompetence
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vyhrazeny	vyhradit	k5eAaPmNgInP	vyhradit
Ministerstvu	ministerstvo	k1gNnSc3	ministerstvo
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
spojů	spoj	k1gInPc2	spoj
<g/>
.	.	kIx.	.
</s>
<s>
Rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c4	o
zařazení	zařazení	k1gNnSc4	zařazení
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
nebo	nebo	k8xC	nebo
(	(	kIx(	(
<g/>
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
ministerstev	ministerstvo	k1gNnPc2	ministerstvo
<g/>
)	)	kIx)	)
o	o	k7c4	o
zrušení	zrušení	k1gNnSc4	zrušení
silnic	silnice	k1gFnPc2	silnice
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
III	III	kA	III
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
o	o	k7c6	o
odvoláních	odvolání	k1gNnPc6	odvolání
proti	proti	k7c3	proti
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
obecních	obecní	k2eAgInPc2d1	obecní
úřadů	úřad	k1gInPc2	úřad
a	a	k8xC	a
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Obecní	obecní	k2eAgInPc1d1	obecní
úřady	úřad	k1gInPc1	úřad
obcí	obec	k1gFnPc2	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
působnost	působnost	k1gFnSc4	působnost
silničního	silniční	k2eAgInSc2d1	silniční
správního	správní	k2eAgInSc2d1	správní
úřadu	úřad	k1gInSc2	úřad
a	a	k8xC	a
speciálního	speciální	k2eAgInSc2d1	speciální
úřadu	úřad	k1gInSc2	úřad
zejména	zejména	k9	zejména
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
silnic	silnice	k1gFnPc2	silnice
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
III	III	kA	III
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Projednávají	projednávat	k5eAaImIp3nP	projednávat
přestupky	přestupek	k1gInPc4	přestupek
na	na	k7c6	na
dálnicích	dálnice	k1gFnPc6	dálnice
i	i	k8xC	i
silnicích	silnice	k1gFnPc6	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Kategorie	kategorie	k1gFnPc1	kategorie
pozemních	pozemní	k2eAgFnPc2d1	pozemní
komunikací	komunikace	k1gFnPc2	komunikace
Silniční	silniční	k2eAgFnSc1d1	silniční
síť	síť	k1gFnSc1	síť
Intenzita	intenzita	k1gFnSc1	intenzita
dopravy	doprava	k1gFnSc2	doprava
Projektová	projektový	k2eAgFnSc1d1	projektová
dokumentace	dokumentace	k1gFnSc1	dokumentace
Evropská	evropský	k2eAgFnSc1d1	Evropská
silnice	silnice	k1gFnSc1	silnice
Pravidla	pravidlo	k1gNnSc2	pravidlo
silničního	silniční	k2eAgInSc2d1	silniční
provozu	provoz	k1gInSc2	provoz
Císařská	císařský	k2eAgFnSc1d1	císařská
silnice	silnice	k1gFnSc1	silnice
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
silnice	silnice	k1gFnSc2	silnice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
silnice	silnice	k1gFnSc2	silnice
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Zemské	zemský	k2eAgFnSc2d1	zemská
silnice	silnice	k1gFnSc2	silnice
<g/>
,	,	kIx,	,
císařské	císařský	k2eAgFnSc2d1	císařská
silnice	silnice	k1gFnSc2	silnice
Dálkové	dálkový	k2eAgFnSc2d1	dálková
trasy	trasa	k1gFnSc2	trasa
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Merta	Merta	k1gMnSc1	Merta
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
</s>
