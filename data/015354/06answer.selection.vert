<s>
Vlajka	vlajka	k1gFnSc1
Francie	Francie	k1gFnSc2
je	být	k5eAaImIp3nS
státní	státní	k2eAgFnSc1d1
francouzská	francouzský	k2eAgFnSc1d1
vlajka	vlajka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
má	mít	k5eAaImIp3nS
podobu	podoba	k1gFnSc4
svislé	svislý	k2eAgFnSc2d1
trikolóry	trikolóra	k1gFnSc2
složené	složený	k2eAgFnSc6d1
z	z	k7c2
modrého	modrý	k2eAgInSc2d1
<g/>
,	,	kIx,
bílého	bílý	k2eAgInSc2d1
a	a	k8xC
červeného	červený	k2eAgInSc2d1
pruhu	pruh	k1gInSc2
<g/>
.	.	kIx.
</s>