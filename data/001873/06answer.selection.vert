<s>
In	In	k?	In
vitro	vitro	k6eAd1	vitro
je	být	k5eAaImIp3nS	být
odborný	odborný	k2eAgInSc4d1	odborný
termín	termín	k1gInSc4	termín
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc4d1	používaný
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
<g/>
,	,	kIx,	,
biologii	biologie	k1gFnSc6	biologie
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
příbuzných	příbuzný	k2eAgInPc6d1	příbuzný
oborech	obor	k1gInPc6	obor
pracujících	pracující	k1gMnPc2	pracující
s	s	k7c7	s
organizmy	organizmus	k1gInPc7	organizmus
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc7	jejich
částmi	část	k1gFnPc7	část
v	v	k7c6	v
umělých	umělý	k2eAgFnPc6d1	umělá
podmínkách	podmínka	k1gFnPc6	podmínka
laboratoře	laboratoř	k1gFnSc2	laboratoř
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
překládá	překládat	k5eAaImIp3nS	překládat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
ve	v	k7c6	v
skle	sklo	k1gNnSc6	sklo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
