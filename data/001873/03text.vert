<s>
In	In	k?	In
vitro	vitro	k6eAd1	vitro
je	být	k5eAaImIp3nS	být
odborný	odborný	k2eAgInSc4d1	odborný
termín	termín	k1gInSc4	termín
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc4d1	používaný
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
<g/>
,	,	kIx,	,
biologii	biologie	k1gFnSc6	biologie
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
příbuzných	příbuzný	k2eAgInPc6d1	příbuzný
oborech	obor	k1gInPc6	obor
pracujících	pracující	k1gMnPc2	pracující
s	s	k7c7	s
organizmy	organizmus	k1gInPc7	organizmus
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc7	jejich
částmi	část	k1gFnPc7	část
v	v	k7c6	v
umělých	umělý	k2eAgFnPc6d1	umělá
podmínkách	podmínka	k1gFnPc6	podmínka
laboratoře	laboratoř	k1gFnSc2	laboratoř
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
překládá	překládat	k5eAaImIp3nS	překládat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
ve	v	k7c6	v
skle	sklo	k1gNnSc6	sklo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
pak	pak	k6eAd1	pak
znamená	znamenat	k5eAaImIp3nS	znamenat
ve	v	k7c6	v
zkumavce	zkumavka	k1gFnSc6	zkumavka
<g/>
,	,	kIx,	,
Erlenmeyerově	Erlenmeyerův	k2eAgFnSc6d1	Erlenmeyerova
baňce	baňka	k1gFnSc6	baňka
<g/>
,	,	kIx,	,
Petriho	Petri	k1gMnSc2	Petri
miskách	miska	k1gFnPc6	miska
a	a	k8xC	a
dalším	další	k2eAgNnSc6d1	další
laboratorním	laboratorní	k2eAgNnSc6d1	laboratorní
skle	sklo	k1gNnSc6	sklo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
lze	lze	k6eAd1	lze
něco	něco	k3yInSc4	něco
pěstovat	pěstovat	k5eAaImF	pěstovat
či	či	k8xC	či
kultivovat	kultivovat	k5eAaImF	kultivovat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
i	i	k9	i
plastové	plastový	k2eAgFnPc4d1	plastová
zkumavky	zkumavka	k1gFnPc4	zkumavka
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
původně	původně	k6eAd1	původně
vytyčený	vytyčený	k2eAgInSc1d1	vytyčený
pro	pro	k7c4	pro
materiál	materiál	k1gInSc4	materiál
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
zahrnovat	zahrnovat	k5eAaImF	zahrnovat
i	i	k9	i
např.	např.	kA	např.
plastové	plastový	k2eAgFnSc2d1	plastová
zkumavky	zkumavka	k1gFnSc2	zkumavka
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
předměty	předmět	k1gInPc4	předmět
pro	pro	k7c4	pro
uchování	uchování	k1gNnSc4	uchování
či	či	k8xC	či
kultivaci	kultivace	k1gFnSc4	kultivace
nacházející	nacházející	k2eAgFnSc4d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
Kultivace	kultivace	k1gFnSc1	kultivace
ve	v	k7c6	v
tkáni	tkáň	k1gFnSc6	tkáň
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
pletivech	pletivo	k1gNnPc6	pletivo
či	či	k8xC	či
hostitelském	hostitelský	k2eAgInSc6d1	hostitelský
organizmu	organizmus	k1gInSc6	organizmus
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
in	in	k?	in
vivo	vivo	k6eAd1	vivo
<g/>
.	.	kIx.	.
</s>
<s>
Opakem	opak	k1gInSc7	opak
výrazu	výraz	k1gInSc2	výraz
in	in	k?	in
vitro	vitro	k6eAd1	vitro
je	být	k5eAaImIp3nS	být
ex	ex	k6eAd1	ex
vitro	vitro	k6eAd1	vitro
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
literatuře	literatura	k1gFnSc6	literatura
je	být	k5eAaImIp3nS	být
zvykem	zvyk	k1gInSc7	zvyk
psát	psát	k5eAaImF	psát
tento	tento	k3xDgInSc4	tento
termín	termín	k1gInSc4	termín
kurzívou	kurzíva	k1gFnSc7	kurzíva
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
odlišení	odlišení	k1gNnSc3	odlišení
a	a	k8xC	a
zvýraznění	zvýraznění	k1gNnSc4	zvýraznění
v	v	k7c6	v
textu	text	k1gInSc6	text
<g/>
.	.	kIx.	.
</s>
<s>
Setkáváme	setkávat	k5eAaImIp1nP	setkávat
se	se	k3xPyFc4	se
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
nejenom	nejenom	k6eAd1	nejenom
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
anglické	anglický	k2eAgFnSc6d1	anglická
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
in-vitro	initro	k6eAd1	in-vitro
kultuře	kultura	k1gFnSc6	kultura
mohou	moct	k5eAaImIp3nP	moct
býti	být	k5eAaImF	být
i	i	k9	i
masožravé	masožravý	k2eAgFnPc4d1	masožravá
rostliny	rostlina	k1gFnPc4	rostlina
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
jiných	jiný	k2eAgFnPc2d1	jiná
rostlin	rostlina	k1gFnPc2	rostlina
Při	při	k7c6	při
přenesení	přenesení	k1gNnSc6	přenesení
z	z	k7c2	z
podmínek	podmínka	k1gFnPc2	podmínka
in	in	k?	in
vitro	vitro	k1gNnSc4	vitro
do	do	k7c2	do
ex	ex	k6eAd1	ex
vitro	vitro	k6eAd1	vitro
se	se	k3xPyFc4	se
rostliny	rostlina	k1gFnPc1	rostlina
musí	muset	k5eAaImIp3nP	muset
potýkat	potýkat	k5eAaImF	potýkat
se	s	k7c7	s
značným	značný	k2eAgInSc7d1	značný
stresem	stres	k1gInSc7	stres
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
i	i	k9	i
fráze	fráze	k1gFnSc1	fráze
"	"	kIx"	"
<g/>
oplodnění	oplodnění	k1gNnSc1	oplodnění
in	in	k?	in
vitro	vitro	k1gNnSc1	vitro
<g/>
"	"	kIx"	"
či	či	k8xC	či
cizím	cizí	k2eAgNnSc7d1	cizí
slovem	slovo	k1gNnSc7	slovo
"	"	kIx"	"
<g/>
in	in	k?	in
vitro	vitro	k1gNnSc1	vitro
fertilizace	fertilizace	k1gFnSc1	fertilizace
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
metoda	metoda	k1gFnSc1	metoda
umělého	umělý	k2eAgNnSc2d1	umělé
oplodnění	oplodnění	k1gNnSc2	oplodnění
<g/>
.	.	kIx.	.
mikropropagace	mikropropagace	k1gFnSc1	mikropropagace
rostlin	rostlina	k1gFnPc2	rostlina
vitrifikace	vitrifikace	k1gFnSc2	vitrifikace
in	in	k?	in
(	(	kIx(	(
<g/>
latina	latina	k1gFnSc1	latina
<g/>
)	)	kIx)	)
</s>
