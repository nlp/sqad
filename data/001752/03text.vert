<s>
Andrzej	Andrzat	k5eAaPmRp2nS	Andrzat
Sapkowski	Sapkowski	k1gNnSc4	Sapkowski
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
Lodž	Lodž	k1gFnSc1	Lodž
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
polský	polský	k2eAgMnSc1d1	polský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
autorů	autor	k1gMnPc2	autor
slovanské	slovanský	k2eAgInPc4d1	slovanský
fantasy	fantas	k1gInPc4	fantas
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
populární	populární	k2eAgInSc1d1	populární
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
a	a	k8xC	a
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
popularitu	popularita	k1gFnSc4	popularita
si	se	k3xPyFc3	se
vydobyl	vydobýt	k5eAaPmAgMnS	vydobýt
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
nejznámějším	známý	k2eAgInSc7d3	nejznámější
dílem	díl	k1gInSc7	díl
jsou	být	k5eAaImIp3nP	být
povídky	povídka	k1gFnPc1	povídka
a	a	k8xC	a
osmidílná	osmidílný	k2eAgFnSc1d1	osmidílná
sága	sága	k1gFnSc1	sága
o	o	k7c6	o
zaklínači	zaklínač	k1gMnSc6	zaklínač
Geraltovi	Geralt	k1gMnSc6	Geralt
z	z	k7c2	z
Rivie	Rivie	k1gFnSc2	Rivie
a	a	k8xC	a
princezně	princezna	k1gFnSc3	princezna
Ciri	Cir	k1gFnSc3	Cir
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
byla	být	k5eAaImAgNnP	být
i	i	k9	i
zfilmována	zfilmován	k2eAgNnPc1d1	zfilmováno
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
filmu	film	k1gInSc2	film
a	a	k8xC	a
seriálu	seriál	k1gInSc2	seriál
se	se	k3xPyFc4	se
však	však	k9	však
sám	sám	k3xTgMnSc1	sám
Sapkowski	Sapkowske	k1gFnSc4	Sapkowske
důrazně	důrazně	k6eAd1	důrazně
distancoval	distancovat	k5eAaBmAgMnS	distancovat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Andrzej	Andrzej	k1gMnSc3	Andrzej
Sapkowski	Sapkowsk	k1gMnSc3	Sapkowsk
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Lodži	Lodž	k1gFnSc6	Lodž
a	a	k8xC	a
jelikož	jelikož	k8xS	jelikož
má	mít	k5eAaImIp3nS	mít
nadání	nadání	k1gNnSc4	nadání
na	na	k7c4	na
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
po	po	k7c6	po
studiu	studio	k1gNnSc6	studio
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
povídku	povídka	k1gFnSc4	povídka
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
a	a	k8xC	a
již	již	k6eAd1	již
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejoblíbenějších	oblíbený	k2eAgMnPc2d3	nejoblíbenější
spisovatelů	spisovatel	k1gMnPc2	spisovatel
fantasy	fantas	k1gInPc7	fantas
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
knih	kniha	k1gFnPc2	kniha
píše	psát	k5eAaImIp3nS	psát
i	i	k9	i
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
recenze	recenze	k1gFnPc1	recenze
<g/>
,	,	kIx,	,
komentáře	komentář	k1gInPc1	komentář
a	a	k8xC	a
fejetony	fejeton	k1gInPc1	fejeton
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
do	do	k7c2	do
polských	polský	k2eAgMnPc2d1	polský
fantasy	fantas	k1gInPc1	fantas
časopisů	časopis	k1gInPc2	časopis
Fantastika	fantastika	k1gFnSc1	fantastika
a	a	k8xC	a
Magie	magie	k1gFnSc1	magie
a	a	k8xC	a
Meč	meč	k1gInSc1	meč
(	(	kIx(	(
<g/>
v	v	k7c4	v
orig	orig	k1gInSc4	orig
<g/>
.	.	kIx.	.
</s>
<s>
Fantastyka	Fantastyka	k1gFnSc1	Fantastyka
a	a	k8xC	a
Magia	Magia	k1gFnSc1	Magia
i	i	k8xC	i
miecz	miecz	k1gInSc1	miecz
<g/>
)	)	kIx)	)
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnSc4	jeho
novější	nový	k2eAgNnPc1d2	novější
díla	dílo	k1gNnPc1	dílo
patří	patřit	k5eAaImIp3nS	patřit
"	"	kIx"	"
<g/>
sága	sága	k1gFnSc1	sága
o	o	k7c6	o
Reinmarovi	Reinmar	k1gMnSc6	Reinmar
z	z	k7c2	z
Bělavy	bělavo	k1gNnPc7	bělavo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
známější	známý	k2eAgFnSc1d2	známější
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Husitská	husitský	k2eAgFnSc1d1	husitská
trilogie	trilogie	k1gFnSc1	trilogie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
historická	historický	k2eAgFnSc1d1	historická
fantasy	fantas	k1gInPc4	fantas
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
v	v	k7c6	v
období	období	k1gNnSc6	období
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
a	a	k8xC	a
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
košatý	košatý	k2eAgInSc4d1	košatý
jazyk	jazyk	k1gInSc4	jazyk
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
latinských	latinský	k2eAgInPc2d1	latinský
citátů	citát	k1gInPc2	citát
a	a	k8xC	a
mistrovskou	mistrovský	k2eAgFnSc4d1	mistrovská
gradaci	gradace	k1gFnSc4	gradace
děje	děj	k1gInSc2	děj
bývá	bývat	k5eAaImIp3nS	bývat
přirovnávána	přirovnáván	k2eAgFnSc1d1	přirovnávána
ke	k	k7c3	k
Křižákům	křižák	k1gInPc3	křižák
Henryka	Henryko	k1gNnSc2	Henryko
Sienkiewicze	Sienkiewicze	k1gFnSc2	Sienkiewicze
<g/>
.	.	kIx.	.
</s>
<s>
Sapkowski	Sapkowski	k6eAd1	Sapkowski
zde	zde	k6eAd1	zde
projevuje	projevovat	k5eAaImIp3nS	projevovat
skvělou	skvělý	k2eAgFnSc4d1	skvělá
znalost	znalost	k1gFnSc4	znalost
historických	historický	k2eAgFnPc2d1	historická
reálií	reálie	k1gFnPc2	reálie
(	(	kIx(	(
<g/>
čerpal	čerpat	k5eAaImAgInS	čerpat
zejména	zejména	k9	zejména
z	z	k7c2	z
Polské	polský	k2eAgFnSc2d1	polská
kroniky	kronika	k1gFnSc2	kronika
Jana	Jan	k1gMnSc2	Jan
Dlugosze	Dlugosze	k1gFnSc2	Dlugosze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
mnoho	mnoho	k4c1	mnoho
historických	historický	k2eAgFnPc2d1	historická
osobností	osobnost	k1gFnPc2	osobnost
z	z	k7c2	z
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
Uher	Uhry	k1gFnPc2	Uhry
a	a	k8xC	a
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
románech	román	k1gInPc6	román
zde	zde	k6eAd1	zde
Sapkowski	Sapkowsk	k1gFnSc2	Sapkowsk
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
svou	svůj	k3xOyFgFnSc4	svůj
pověstnou	pověstný	k2eAgFnSc4d1	pověstná
ironii	ironie	k1gFnSc4	ironie
a	a	k8xC	a
skvělý	skvělý	k2eAgInSc4d1	skvělý
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
detail	detail	k1gInSc4	detail
<g/>
.	.	kIx.	.
</s>
<s>
Povídky	povídka	k1gFnPc1	povídka
o	o	k7c6	o
Zaklínači	zaklínač	k1gMnSc6	zaklínač
Zaklínač	zaklínač	k1gMnSc1	zaklínač
-	-	kIx~	-
Stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
meč	meč	k1gInSc1	meč
ISBN	ISBN	kA	ISBN
80-900912-5-3	[number]	k4	80-900912-5-3
(	(	kIx(	(
<g/>
Winston	Winston	k1gInSc1	Winston
Smith	Smith	k1gInSc1	Smith
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Zaklínač	zaklínač	k1gMnSc1	zaklínač
-	-	kIx~	-
Věčný	věčný	k2eAgInSc1d1	věčný
oheň	oheň	k1gInSc1	oheň
ISBN	ISBN	kA	ISBN
80-85643-22-7	[number]	k4	80-85643-22-7
(	(	kIx(	(
<g/>
Winston	Winston	k1gInSc1	Winston
Smith	Smith	k1gInSc1	Smith
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Zaklínač	zaklínač	k1gMnSc1	zaklínač
-	-	kIx~	-
Meč	meč	k1gInSc1	meč
osudu	osud	k1gInSc2	osud
ISBN	ISBN	kA	ISBN
80-85643-23-5	[number]	k4	80-85643-23-5
(	(	kIx(	(
<g/>
Winston	Winston	k1gInSc1	Winston
Smith	Smith	k1gInSc1	Smith
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Zaklínač	zaklínač	k1gMnSc1	zaklínač
I	i	k8xC	i
-	-	kIx~	-
Poslední	poslední	k2eAgNnPc1d1	poslední
přání	přání	k1gNnPc1	přání
ISBN	ISBN	kA	ISBN
80-85951-14-2	[number]	k4	80-85951-14-2
(	(	kIx(	(
<g/>
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Zaklínač	zaklínač	k1gMnSc1	zaklínač
II	II	kA	II
-	-	kIx~	-
Meč	meč	k1gInSc1	meč
osudu	osud	k1gInSc2	osud
ISBN	ISBN	kA	ISBN
80-85951-23-1	[number]	k4	80-85951-23-1
(	(	kIx(	(
<g/>
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Ostatní	ostatní	k2eAgFnSc2d1	ostatní
povídky	povídka	k1gFnSc2	povídka
Maladie	Maladie	k1gFnSc2	Maladie
-	-	kIx~	-
antologie	antologie	k1gFnSc1	antologie
polských	polský	k2eAgFnPc2d1	polská
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
titulní	titulní	k2eAgFnPc4d1	titulní
pov	pov	k?	pov
<g/>
.	.	kIx.	.
</s>
<s>
Sapkowského	Sapkowského	k2eAgNnSc1d1	Sapkowského
Tandaradei	Tandaradei	k1gNnSc1	Tandaradei
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Magické	magický	k2eAgInPc1d1	magický
střípky	střípek	k1gInPc1	střípek
ISBN	ISBN	kA	ISBN
80-85951-28-2	[number]	k4	80-85951-28-2
(	(	kIx(	(
<g/>
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Krev	krev	k1gFnSc1	krev
elfů	elf	k1gMnPc2	elf
ISBN	ISBN	kA	ISBN
80-85951-00-2	[number]	k4	80-85951-00-2
(	(	kIx(	(
<g/>
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Čas	čas	k1gInSc1	čas
opovržení	opovržení	k1gNnSc2	opovržení
ISBN	ISBN	kA	ISBN
80-85951-01-0	[number]	k4	80-85951-01-0
(	(	kIx(	(
<g/>
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Křest	křest	k1gInSc1	křest
ohněm	oheň	k1gInSc7	oheň
<g />
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-85951-03-7	[number]	k4	80-85951-03-7
(	(	kIx(	(
<g/>
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Věž	věž	k1gFnSc1	věž
vlaštovky	vlaštovka	k1gFnSc2	vlaštovka
ISBN	ISBN	kA	ISBN
80-85951-07-X	[number]	k4	80-85951-07-X
(	(	kIx(	(
<g/>
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Paní	paní	k1gFnSc1	paní
jezera	jezero	k1gNnSc2	jezero
ISBN	ISBN	kA	ISBN
80-85951-19-3	[number]	k4	80-85951-19-3
(	(	kIx(	(
<g/>
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Knihy	kniha	k1gFnPc1	kniha
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
znovu	znovu	k6eAd1	znovu
jako	jako	k8xS	jako
<g/>
:	:	kIx,	:
Zaklínač	zaklínač	k1gMnSc1	zaklínač
<g />
.	.	kIx.	.
</s>
<s>
I	i	k9	i
-	-	kIx~	-
Poslední	poslední	k2eAgNnSc1d1	poslední
přání	přání	k1gNnSc1	přání
ISBN	ISBN	kA	ISBN
978-80-85951-65-3	[number]	k4	978-80-85951-65-3
(	(	kIx(	(
<g/>
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Zaklínač	zaklínač	k1gMnSc1	zaklínač
II	II	kA	II
-	-	kIx~	-
Meč	meč	k1gInSc1	meč
osudu	osud	k1gInSc2	osud
ISBN	ISBN	kA	ISBN
978-80-85951-66-0	[number]	k4	978-80-85951-66-0
(	(	kIx(	(
<g/>
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Zaklínač	zaklínač	k1gMnSc1	zaklínač
III	III	kA	III
-	-	kIx~	-
Krev	krev	k1gFnSc1	krev
elfů	elf	k1gMnPc2	elf
ISBN	ISBN	kA	ISBN
978-80-85951-69-1	[number]	k4	978-80-85951-69-1
(	(	kIx(	(
<g/>
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Zaklínač	zaklínač	k1gMnSc1	zaklínač
IV	IV	kA	IV
-	-	kIx~	-
Čas	čas	k1gInSc1	čas
opovržení	opovržení	k1gNnSc2	opovržení
ISBN	ISBN	kA	ISBN
<g />
.	.	kIx.	.
</s>
<s>
978-80-85951-70-7	[number]	k4	978-80-85951-70-7
(	(	kIx(	(
<g/>
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Zaklínač	zaklínač	k1gMnSc1	zaklínač
V	V	kA	V
-	-	kIx~	-
Křest	křest	k1gInSc1	křest
ohněm	oheň	k1gInSc7	oheň
ISBN	ISBN	kA	ISBN
978-80-85951-71-4	[number]	k4	978-80-85951-71-4
(	(	kIx(	(
<g/>
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Zaklínač	zaklínač	k1gMnSc1	zaklínač
VI	VI	kA	VI
-	-	kIx~	-
Věž	věž	k1gFnSc1	věž
vlaštovky	vlaštovka	k1gFnSc2	vlaštovka
ISBN	ISBN	kA	ISBN
978-80-85951-73-8	[number]	k4	978-80-85951-73-8
(	(	kIx(	(
<g/>
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Zaklínač	zaklínač	k1gMnSc1	zaklínač
VII	VII	kA	VII
-	-	kIx~	-
Paní	paní	k1gFnSc1	paní
jezera	jezero	k1gNnSc2	jezero
ISBN	ISBN	kA	ISBN
978-80-85951-74-5	[number]	k4	978-80-85951-74-5
(	(	kIx(	(
<g/>
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Zmije	zmije	k1gFnSc2	zmije
ISBN	ISBN	kA	ISBN
978-80-85951-67-7	[number]	k4	978-80-85951-67-7
(	(	kIx(	(
<g/>
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Zaklínač	zaklínač	k1gMnSc1	zaklínač
-	-	kIx~	-
Bouřková	bouřkový	k2eAgFnSc1d1	bouřková
sezóna	sezóna	k1gFnSc1	sezóna
ISBN	ISBN	kA	ISBN
978-80-7477-050-0	[number]	k4	978-80-7477-050-0
(	(	kIx(	(
<g/>
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Narrenturm	Narrenturm	k1gInSc1	Narrenturm
<g/>
,	,	kIx,	,
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-85951-36-3	[number]	k4	80-85951-36-3
(	(	kIx(	(
<g/>
Narrenturm	Narrenturm	k1gInSc1	Narrenturm
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Boží	božit	k5eAaImIp3nP	božit
bojovníci	bojovník	k1gMnPc1	bojovník
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-85951-43-6	[number]	k4	80-85951-43-6
(	(	kIx(	(
<g/>
Boży	Boż	k2eAgInPc1d1	Boż
bojownicy	bojownicy	k1gInPc1	bojownicy
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Lux	Lux	k1gMnSc1	Lux
Perpetua	perpetuum	k1gNnSc2	perpetuum
<g/>
,	,	kIx,	,
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-85951-49-3	[number]	k4	978-80-85951-49-3
(	(	kIx(	(
<g/>
Lux	Lux	k1gMnSc1	Lux
perpetua	perpetuum	k1gNnSc2	perpetuum
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Tyto	tento	k3xDgFnPc1	tento
povídky	povídka	k1gFnPc1	povídka
byly	být	k5eAaImAgFnP	být
vydané	vydaný	k2eAgInPc1d1	vydaný
ve	v	k7c6	v
sborníku	sborník	k1gInSc6	sborník
Magické	magický	k2eAgInPc4d1	magický
střípky	střípek	k1gInPc4	střípek
Zlaté	zlatá	k1gFnSc2	zlatá
odpoledne	odpoledne	k1gNnSc2	odpoledne
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Ikarie	Ikarie	k1gFnSc1	Ikarie
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
99	[number]	k4	99
<g/>
,	,	kIx,	,
s.	s.	k?	s.
23	[number]	k4	23
<g/>
-	-	kIx~	-
<g/>
35	[number]	k4	35
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Stanislav	Stanislav	k1gMnSc1	Stanislav
Komárek	Komárek	k1gMnSc1	Komárek
<g/>
)	)	kIx)	)
V	v	k7c6	v
kráteru	kráter	k1gInSc6	kráter
po	po	k7c6	po
bombě	bomba	k1gFnSc6	bomba
Něco	něco	k3yInSc1	něco
končí	končit	k5eAaImIp3nS	končit
<g/>
,	,	kIx,	,
něco	něco	k3yInSc4	něco
začíná	začínat	k5eAaImIp3nS	začínat
(	(	kIx(	(
<g/>
autorova	autorův	k2eAgFnSc1d1	autorova
legrácka	legrácka	k1gFnSc1	legrácka
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
nebyla	být	k5eNaImAgFnS	být
určena	určen	k2eAgFnSc1d1	určena
k	k	k7c3	k
veřejnému	veřejný	k2eAgNnSc3d1	veřejné
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
<g/>
)	)	kIx)	)
Yrrhedesovo	Yrrhedesův	k2eAgNnSc1d1	Yrrhedesovo
oko	oko	k1gNnSc1	oko
(	(	kIx(	(
<g/>
R.	R.	kA	R.
<g/>
S.	S.	kA	S.
<g/>
G	G	kA	G
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Zachraňme	zachránit	k5eAaPmRp1nP	zachránit
elfy	elf	k1gMnPc4	elf
(	(	kIx(	(
<g/>
Ikarie	Ikarie	k1gFnSc1	Ikarie
5	[number]	k4	5
<g/>
/	/	kIx~	/
<g/>
99	[number]	k4	99
<g/>
,	,	kIx,	,
s.	s.	k?	s.
44	[number]	k4	44
<g/>
-	-	kIx~	-
<g/>
47	[number]	k4	47
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Michael	Michael	k1gMnSc1	Michael
Bronec	Bronec	k1gMnSc1	Bronec
<g/>
)	)	kIx)	)
V	v	k7c6	v
Šedých	Šedých	k2eAgFnPc6d1	Šedých
horách	hora	k1gFnPc6	hora
zlato	zlato	k1gNnSc4	zlato
není	být	k5eNaImIp3nS	být
</s>
