<s>
Kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
autorom	autorom	k1gInSc4	autorom
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jediný	jediný	k2eAgInSc1d1	jediný
známý	známý	k2eAgInSc1d1	známý
výskyt	výskyt	k1gInSc1	výskyt
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
Autogyos	Autogyosa	k1gFnPc2	Autogyosa
<g/>
?	?	kIx.	?
</s>
