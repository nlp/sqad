<s>
Autogyos	Autogyos	k1gInSc1	Autogyos
(	(	kIx(	(
<g/>
α	α	k?	α
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc4	slovo
ve	v	k7c6	v
starověké	starověký	k2eAgFnSc6d1	starověká
řečtině	řečtina	k1gFnSc6	řečtina
<g/>
,	,	kIx,	,
označující	označující	k2eAgInSc4d1	označující
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
druh	druh	k1gInSc4	druh
orby	orba	k1gFnSc2	orba
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
jednou	jeden	k4xCgFnSc7	jeden
v	v	k7c6	v
díle	díl	k1gInSc6	díl
Hésiodově	Hésiodův	k2eAgInSc6d1	Hésiodův
a	a	k8xC	a
nenalézáme	nalézat	k5eNaImIp1nP	nalézat
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
jiném	jiný	k2eAgInSc6d1	jiný
antickém	antický	k2eAgInSc6d1	antický
spise	spis	k1gInSc6	spis
Lilith	Lilitha	k1gFnPc2	Lilitha
(	(	kIx(	(
<g/>
ל	ל	k?	ל
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
označující	označující	k2eAgFnSc4d1	označující
patrně	patrně	k6eAd1	patrně
noční	noční	k2eAgFnSc4d1	noční
bytost	bytost	k1gFnSc4	bytost
nebo	nebo	k8xC	nebo
lelka	lelek	k1gMnSc4	lelek
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
zákoně	zákon	k1gInSc6	zákon
jen	jen	k9	jen
jednou	jednou	k6eAd1	jednou
v	v	k7c6	v
Izajášově	Izajášův	k2eAgNnSc6d1	Izajášovo
proroctví	proroctví	k1gNnSc6	proroctví
34	[number]	k4	34
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
</s>
