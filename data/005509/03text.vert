<s>
Long	Long	k1gMnSc1	Long
Beach	Beach	k1gMnSc1	Beach
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc4	město
v	v	k7c6	v
USA	USA	kA	USA
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
aglomeraci	aglomerace	k1gFnSc6	aglomerace
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
<g/>
,	,	kIx,	,
30	[number]	k4	30
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
L.A.	L.A.	k1gFnSc2	L.A.
Má	mít	k5eAaImIp3nS	mít
463	[number]	k4	463
956	[number]	k4	956
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
Long	Long	k1gMnSc1	Long
Beach	Beach	k1gMnSc1	Beach
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
velmi	velmi	k6eAd1	velmi
důležitý	důležitý	k2eAgInSc1d1	důležitý
přístav	přístav	k1gInSc1	přístav
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
Kalifornská	kalifornský	k2eAgFnSc1d1	kalifornská
státní	státní	k2eAgFnSc1d1	státní
Univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Long	Long	k1gMnSc1	Long
Beach	Beach	k1gMnSc1	Beach
je	být	k5eAaImIp3nS	být
34	[number]	k4	34
<g/>
.	.	kIx.	.
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
největší	veliký	k2eAgFnSc2d3	veliký
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
a	a	k8xC	a
druhé	druhý	k4xOgFnSc2	druhý
největší	veliký	k2eAgFnSc2d3	veliký
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Los	los	k1gMnSc1	los
Angeles	Angeles	k1gMnSc1	Angeles
(	(	kIx(	(
<g/>
po	po	k7c6	po
městu	město	k1gNnSc3	město
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc1	Angeles
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Narodili	narodit	k5eAaPmAgMnP	narodit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
slavní	slavný	k2eAgMnPc1d1	slavný
rapeři	raper	k1gMnPc1	raper
Snoop	Snoop	k1gInSc4	Snoop
Dogg	Dogg	k1gMnSc1	Dogg
a	a	k8xC	a
Zack	Zack	k1gMnSc1	Zack
de	de	k?	de
la	la	k1gNnSc2	la
Rocha	Roch	k1gMnSc2	Roch
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstala	vyrůstat	k5eAaImAgFnS	vyrůstat
zde	zde	k6eAd1	zde
i	i	k9	i
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
Cameron	Cameron	k1gMnSc1	Cameron
Diaz	Diaz	k1gMnSc1	Diaz
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
462	[number]	k4	462
257	[number]	k4	257
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
46,1	[number]	k4	46,1
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
13,5	[number]	k4	13,5
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,7	[number]	k4	0,7
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
12,9	[number]	k4	12,9
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
1,1	[number]	k4	1,1
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
20,3	[number]	k4	20,3
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
5,3	[number]	k4	5,3
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
40,8	[number]	k4	40,8
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
