<p>
<s>
Voda	voda	k1gFnSc1	voda
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
je	být	k5eAaImIp3nS	být
souhrnné	souhrnný	k2eAgNnSc1d1	souhrnné
označení	označení	k1gNnSc1	označení
veškeré	veškerý	k3xTgFnSc2	veškerý
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Mars	Mars	k1gInSc1	Mars
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
Zemi	zem	k1gFnSc3	zem
však	však	k9	však
nemá	mít	k5eNaImIp3nS	mít
Mars	Mars	k1gInSc1	Mars
výskyt	výskyt	k1gInSc1	výskyt
vody	voda	k1gFnSc2	voda
ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
třech	tři	k4xCgNnPc6	tři
skupenstvích	skupenství	k1gNnPc6	skupenství
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
obdobném	obdobný	k2eAgInSc6d1	obdobný
pozemskému	pozemský	k2eAgNnSc3d1	pozemské
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
neexistují	existovat	k5eNaImIp3nP	existovat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
oblasti	oblast	k1gFnPc4	oblast
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
hydrosféry	hydrosféra	k1gFnSc2	hydrosféra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
vázána	vázat	k5eAaImNgFnS	vázat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
kryosféře	kryosféra	k1gFnSc6	kryosféra
(	(	kIx(	(
<g/>
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
permafrostu	permafrost	k1gInSc2	permafrost
<g/>
,	,	kIx,	,
polárních	polární	k2eAgFnPc2d1	polární
čepiček	čepička	k1gFnPc2	čepička
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
led	led	k1gInSc1	led
nebo	nebo	k8xC	nebo
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
jako	jako	k8xC	jako
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgFnPc1d1	současná
podmínky	podmínka	k1gFnPc1	podmínka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Marsu	Mars	k1gInSc2	Mars
neumožňují	umožňovat	k5eNaImIp3nP	umožňovat
dlouhodobou	dlouhodobý	k2eAgFnSc4d1	dlouhodobá
existenci	existence	k1gFnSc4	existence
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
hodnoty	hodnota	k1gFnPc1	hodnota
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
teploty	teplota	k1gFnSc2	teplota
jsou	být	k5eAaImIp3nP	být
příliš	příliš	k6eAd1	příliš
nízké	nízký	k2eAgFnPc1d1	nízká
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
voda	voda	k1gFnSc1	voda
začíná	začínat	k5eAaImIp3nS	začínat
okamžitě	okamžitě	k6eAd1	okamžitě
mrznout	mrznout	k5eAaImF	mrznout
a	a	k8xC	a
následně	následně	k6eAd1	následně
sublimovat	sublimovat	k5eAaBmF	sublimovat
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
planety	planeta	k1gFnSc2	planeta
však	však	k9	však
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Marsu	Mars	k1gInSc2	Mars
tekoucí	tekoucí	k2eAgFnSc1d1	tekoucí
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vyskytovala	vyskytovat	k5eAaImAgFnS	vyskytovat
<g/>
,	,	kIx,	,
tvořila	tvořit	k5eAaImAgFnS	tvořit
souvislé	souvislý	k2eAgFnPc4d1	souvislá
vodní	vodní	k2eAgFnPc4d1	vodní
plochy	plocha	k1gFnPc4	plocha
a	a	k8xC	a
dnešní	dnešní	k2eAgFnSc1d1	dnešní
otázka	otázka	k1gFnSc1	otázka
spíše	spíše	k9	spíše
zní	znět	k5eAaImIp3nS	znět
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
voda	voda	k1gFnSc1	voda
poděla	poděla	k?	poděla
<g/>
.	.	kIx.	.
<g/>
O	o	k7c6	o
výskytu	výskyt	k1gInSc6	výskyt
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
či	či	k8xC	či
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gInSc7	jeho
povrchem	povrch	k1gInSc7	povrch
existuje	existovat	k5eAaImIp3nS	existovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
přímých	přímý	k2eAgMnPc2d1	přímý
i	i	k8xC	i
nepřímých	přímý	k2eNgMnPc2d1	nepřímý
důkazů	důkaz	k1gInPc2	důkaz
jako	jako	k8xS	jako
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
říčních	říční	k2eAgNnPc2d1	říční
koryt	koryto	k1gNnPc2	koryto
<g/>
,	,	kIx,	,
polárních	polární	k2eAgFnPc2d1	polární
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
spektrometrických	spektrometrický	k2eAgNnPc2d1	spektrometrické
měření	měření	k1gNnPc2	měření
<g/>
,	,	kIx,	,
erodovaných	erodovaný	k2eAgInPc2d1	erodovaný
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
či	či	k8xC	či
minerálů	minerál	k1gInPc2	minerál
přímo	přímo	k6eAd1	přímo
spojených	spojený	k2eAgFnPc2d1	spojená
s	s	k7c7	s
existencí	existence	k1gFnSc7	existence
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
zásobám	zásoba	k1gFnPc3	zásoba
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
zmenší	zmenšit	k5eAaPmIp3nP	zmenšit
potřebné	potřebný	k2eAgFnPc4d1	potřebná
zásoby	zásoba	k1gFnPc4	zásoba
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgFnPc4d1	budoucí
kosmické	kosmický	k2eAgFnPc4d1	kosmická
mise	mise	k1gFnPc4	mise
k	k	k7c3	k
planetě	planeta	k1gFnSc3	planeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
průzkumů	průzkum	k1gInPc2	průzkum
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Mars	Mars	k1gInSc1	Mars
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
popředí	popředí	k1gNnSc2	popředí
veřejného	veřejný	k2eAgInSc2d1	veřejný
zájmu	zájem	k1gInSc2	zájem
<g/>
,	,	kIx,	,
když	když	k8xS	když
italský	italský	k2eAgMnSc1d1	italský
astronom	astronom	k1gMnSc1	astronom
Giovanni	Giovanň	k1gMnSc3	Giovanň
Schiaparelli	Schiaparell	k1gMnSc3	Schiaparell
ohlásil	ohlásit	k5eAaPmAgMnS	ohlásit
objevení	objevení	k1gNnSc4	objevení
kanálů	kanál	k1gInPc2	kanál
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
canali	canat	k5eAaPmAgMnP	canat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
nejprve	nejprve	k6eAd1	nejprve
pokládal	pokládat	k5eAaImAgMnS	pokládat
za	za	k7c4	za
přírodní	přírodní	k2eAgInSc4d1	přírodní
útvar	útvar	k1gInSc4	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
špatného	špatný	k2eAgInSc2d1	špatný
překladu	překlad	k1gInSc2	překlad
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
záměně	záměna	k1gFnSc3	záměna
přírodních	přírodní	k2eAgInPc2d1	přírodní
kanálů	kanál	k1gInPc2	kanál
za	za	k7c4	za
uměle	uměle	k6eAd1	uměle
vytvořené	vytvořený	k2eAgFnPc4d1	vytvořená
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
kolem	kolem	k7c2	kolem
Marsu	Mars	k1gInSc2	Mars
šířit	šířit	k5eAaImF	šířit
příběhy	příběh	k1gInPc1	příběh
o	o	k7c6	o
umírající	umírající	k2eAgFnSc6d1	umírající
civilizaci	civilizace	k1gFnSc6	civilizace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
přivádět	přivádět	k5eAaImF	přivádět
z	z	k7c2	z
polárních	polární	k2eAgFnPc2d1	polární
čepiček	čepička	k1gFnPc2	čepička
vodu	voda	k1gFnSc4	voda
do	do	k7c2	do
vysychajících	vysychající	k2eAgFnPc2d1	vysychající
oblastí	oblast	k1gFnPc2	oblast
kolem	kolem	k7c2	kolem
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgNnSc1d2	pozdější
pozorování	pozorování	k1gNnSc1	pozorování
vyvrátilo	vyvrátit	k5eAaPmAgNnS	vyvrátit
existenci	existence	k1gFnSc4	existence
kanálů	kanál	k1gInPc2	kanál
a	a	k8xC	a
první	první	k4xOgInPc1	první
fotografické	fotografický	k2eAgInPc1d1	fotografický
snímky	snímek	k1gInPc1	snímek
povrchu	povrch	k1gInSc2	povrch
i	i	k9	i
představu	představa	k1gFnSc4	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
nachází	nacházet	k5eAaImIp3nS	nacházet
tekoucí	tekoucí	k2eAgFnSc1d1	tekoucí
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Následná	následný	k2eAgNnPc1d1	následné
měření	měření	k1gNnPc1	měření
ukázala	ukázat	k5eAaPmAgNnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
současný	současný	k2eAgInSc1d1	současný
stav	stav	k1gInSc1	stav
atmosféry	atmosféra	k1gFnSc2	atmosféra
dlouhodobější	dlouhodobý	k2eAgInSc4d2	dlouhodobější
výskyt	výskyt	k1gInSc4	výskyt
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
<g/>
.	.	kIx.	.
<g/>
Vědci	vědec	k1gMnPc1	vědec
studující	studující	k2eAgFnSc2d1	studující
snímky	snímka	k1gFnSc2	snímka
ze	z	k7c2	z
sondy	sonda	k1gFnSc2	sonda
Viking	Viking	k1gMnSc1	Viking
brzy	brzy	k6eAd1	brzy
začali	začít	k5eAaPmAgMnP	začít
rozeznávat	rozeznávat	k5eAaImF	rozeznávat
struktury	struktura	k1gFnPc4	struktura
velmi	velmi	k6eAd1	velmi
nápadně	nápadně	k6eAd1	nápadně
se	se	k3xPyFc4	se
podobající	podobající	k2eAgMnSc1d1	podobající
pozemským	pozemský	k2eAgFnPc3d1	pozemská
oblastem	oblast	k1gFnPc3	oblast
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
vodní	vodní	k2eAgInPc1d1	vodní
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc1	snímek
ukazovaly	ukazovat	k5eAaImAgInP	ukazovat
rozsáhlá	rozsáhlý	k2eAgNnPc1d1	rozsáhlé
říční	říční	k2eAgNnPc1d1	říční
koryta	koryto	k1gNnPc1	koryto
<g/>
,	,	kIx,	,
hluboké	hluboký	k2eAgInPc1d1	hluboký
kanály	kanál	k1gInPc1	kanál
<g/>
,	,	kIx,	,
kaňony	kaňon	k1gInPc1	kaňon
a	a	k8xC	a
objekty	objekt	k1gInPc1	objekt
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vypadaly	vypadat	k5eAaPmAgFnP	vypadat
jako	jako	k9	jako
starodávné	starodávný	k2eAgInPc4d1	starodávný
pobřežní	pobřežní	k2eAgInPc4d1	pobřežní
útvary	útvar	k1gInPc4	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnPc1d1	následující
sondy	sonda	k1gFnPc1	sonda
Mars	Mars	k1gMnSc1	Mars
Pathfinder	Pathfinder	k1gMnSc1	Pathfinder
<g/>
,	,	kIx,	,
Mars	Mars	k1gMnSc1	Mars
Global	globat	k5eAaImAgMnS	globat
Surveyor	Surveyor	k1gInSc4	Surveyor
<g/>
,	,	kIx,	,
Mars	Mars	k1gInSc1	Mars
Express	express	k1gInSc1	express
či	či	k8xC	či
dvě	dva	k4xCgNnPc4	dva
povrchová	povrchový	k2eAgNnPc4d1	povrchové
vozítka	vozítko	k1gNnPc4	vozítko
Mars	Mars	k1gInSc1	Mars
Exploration	Exploration	k1gInSc1	Exploration
Rover	rover	k1gMnSc1	rover
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
existují	existovat	k5eAaImIp3nP	existovat
důkazy	důkaz	k1gInPc1	důkaz
o	o	k7c6	o
projevech	projev	k1gInPc6	projev
tekoucí	tekoucí	k2eAgFnSc2d1	tekoucí
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
vědci	vědec	k1gMnPc1	vědec
dokonce	dokonce	k9	dokonce
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
objevili	objevit	k5eAaPmAgMnP	objevit
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tekla	téct	k5eAaImAgFnS	téct
řeka	řeka	k1gFnSc1	řeka
10	[number]	k4	10
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
×	×	k?	×
mohutnější	mohutný	k2eAgFnSc2d2	mohutnější
než	než	k8xS	než
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
došlo	dojít	k5eAaPmAgNnS	dojít
díky	díky	k7c3	díky
opakovanému	opakovaný	k2eAgNnSc3d1	opakované
snímkování	snímkování	k1gNnSc3	snímkování
bezejmenného	bezejmenný	k2eAgInSc2d1	bezejmenný
kráteru	kráter	k1gInSc2	kráter
sondou	sonda	k1gFnSc7	sonda
Mars	Mars	k1gInSc1	Mars
Global	globat	k5eAaImAgInS	globat
Surveyor	Surveyor	k1gInSc4	Surveyor
k	k	k7c3	k
náhodnému	náhodný	k2eAgInSc3d1	náhodný
objevu	objev	k1gInSc3	objev
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
naznačil	naznačit	k5eAaPmAgInS	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tekoucí	tekoucí	k2eAgFnSc1d1	tekoucí
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
a	a	k8xC	a
po	po	k7c4	po
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
stále	stále	k6eAd1	stále
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnPc1d2	pozdější
studie	studie	k1gFnPc1	studie
ale	ale	k9	ale
vyvrátily	vyvrátit	k5eAaPmAgFnP	vyvrátit
účast	účast	k1gFnSc4	účast
vody	voda	k1gFnPc1	voda
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
útvaru	útvar	k1gInSc2	útvar
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
výsledek	výsledek	k1gInSc4	výsledek
svahového	svahový	k2eAgInSc2d1	svahový
sesuvu	sesuv	k1gInSc2	sesuv
jemnozrnného	jemnozrnný	k2eAgInSc2d1	jemnozrnný
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Metodika	metodika	k1gFnSc1	metodika
průzkumu	průzkum	k1gInSc2	průzkum
==	==	k?	==
</s>
</p>
<p>
<s>
Vědci	vědec	k1gMnPc1	vědec
využívají	využívat	k5eAaImIp3nP	využívat
k	k	k7c3	k
detekci	detekce	k1gFnSc3	detekce
vody	voda	k1gFnSc2	voda
celou	celá	k1gFnSc4	celá
řadu	řada	k1gFnSc4	řada
vědeckých	vědecký	k2eAgInPc2d1	vědecký
přístrojů	přístroj	k1gInPc2	přístroj
založených	založený	k2eAgInPc2d1	založený
na	na	k7c6	na
různém	různý	k2eAgInSc6d1	různý
fyzikálním	fyzikální	k2eAgInSc6d1	fyzikální
základu	základ	k1gInSc6	základ
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejjednodušší	jednoduchý	k2eAgFnPc4d3	nejjednodušší
a	a	k8xC	a
nejstarší	starý	k2eAgFnPc4d3	nejstarší
metody	metoda	k1gFnPc4	metoda
patří	patřit	k5eAaImIp3nS	patřit
optické	optický	k2eAgNnSc1d1	optické
pozorování	pozorování	k1gNnSc1	pozorování
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
už	už	k6eAd1	už
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
přineslo	přinést	k5eAaPmAgNnS	přinést
informace	informace	k1gFnPc4	informace
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
polárních	polární	k2eAgFnPc2d1	polární
čepiček	čepička	k1gFnPc2	čepička
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
ale	ale	k9	ale
tehdy	tehdy	k6eAd1	tehdy
byli	být	k5eAaImAgMnP	být
odkázáni	odkázat	k5eAaPmNgMnP	odkázat
jen	jen	k9	jen
na	na	k7c4	na
optická	optický	k2eAgNnPc4d1	optické
pozorování	pozorování	k1gNnPc4	pozorování
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
nemohli	moct	k5eNaImAgMnP	moct
jasně	jasně	k6eAd1	jasně
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
z	z	k7c2	z
čeho	co	k3yRnSc2	co
jsou	být	k5eAaImIp3nP	být
čepičky	čepička	k1gFnPc1	čepička
složeny	složit	k5eAaPmNgFnP	složit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1854	[number]	k4	1854
předložil	předložit	k5eAaPmAgInS	předložit
William	William	k1gInSc1	William
Whewell	Whewell	k1gInSc1	Whewell
teorii	teorie	k1gFnSc3	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
nacházejí	nacházet	k5eAaImIp3nP	nacházet
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
země	zem	k1gFnPc4	zem
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
mimozemský	mimozemský	k2eAgInSc4d1	mimozemský
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
pozorování	pozorování	k1gNnSc2	pozorování
domnělých	domnělý	k2eAgNnPc2d1	domnělé
kanálu	kanál	k1gInSc2	kanál
vydal	vydat	k5eAaPmAgInS	vydat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
americký	americký	k2eAgMnSc1d1	americký
astronom	astronom	k1gMnSc1	astronom
Percival	Percival	k1gMnSc1	Percival
Lowell	Lowell	k1gMnSc1	Lowell
knihu	kniha	k1gFnSc4	kniha
"	"	kIx"	"
<g/>
Mars	Mars	k1gInSc1	Mars
<g/>
"	"	kIx"	"
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
"	"	kIx"	"
<g/>
Mars	Mars	k1gInSc1	Mars
and	and	k?	and
its	its	k?	its
Canals	Canals	k1gInSc1	Canals
<g/>
"	"	kIx"	"
či	či	k8xC	či
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
"	"	kIx"	"
<g/>
Mars	Mars	k1gInSc1	Mars
As	as	k1gInSc1	as
the	the	k?	the
Abode	Abod	k1gInSc5	Abod
of	of	k?	of
Life	Life	k1gNnPc4	Life
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
začal	začít	k5eAaPmAgMnS	začít
americký	americký	k2eAgMnSc1d1	americký
astronom	astronom	k1gMnSc1	astronom
William	William	k1gInSc4	William
Wallace	Wallace	k1gFnSc2	Wallace
Campbell	Campbella	k1gFnPc2	Campbella
provádět	provádět	k5eAaImF	provádět
první	první	k4xOgFnPc1	první
spektroskopické	spektroskopický	k2eAgFnPc1d1	spektroskopická
analýzy	analýza	k1gFnPc1	analýza
atmosféry	atmosféra	k1gFnSc2	atmosféra
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
prokázaly	prokázat	k5eAaPmAgInP	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
atmosféra	atmosféra	k1gFnSc1	atmosféra
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádnou	žádný	k3yNgFnSc4	žádný
vodní	vodní	k2eAgFnSc4d1	vodní
páru	pára	k1gFnSc4	pára
a	a	k8xC	a
ani	ani	k8xC	ani
kyslík	kyslík	k1gInSc4	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
se	se	k3xPyFc4	se
Mars	Mars	k1gMnSc1	Mars
nacházel	nacházet	k5eAaImAgMnS	nacházet
v	v	k7c6	v
nejlepší	dobrý	k2eAgFnSc6d3	nejlepší
pozorovací	pozorovací	k2eAgFnSc6d1	pozorovací
pozici	pozice	k1gFnSc6	pozice
vůči	vůči	k7c3	vůči
Zemi	zem	k1gFnSc3	zem
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1877	[number]	k4	1877
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
dokonalejších	dokonalý	k2eAgInPc2d2	dokonalejší
teleskopů	teleskop	k1gInPc2	teleskop
přineslo	přinést	k5eAaPmAgNnS	přinést
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
kanály	kanál	k1gInPc1	kanál
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
neexistují	existovat	k5eNaImIp3nP	existovat
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
optický	optický	k2eAgInSc4d1	optický
klam	klam	k1gInSc4	klam
<g/>
.	.	kIx.	.
</s>
<s>
Definitivní	definitivní	k2eAgInSc1d1	definitivní
pád	pád	k1gInSc1	pád
teorie	teorie	k1gFnSc2	teorie
inteligentních	inteligentní	k2eAgMnPc2d1	inteligentní
Marťanů	Marťan	k1gMnPc2	Marťan
přinesla	přinést	k5eAaPmAgFnS	přinést
až	až	k9	až
první	první	k4xOgFnSc1	první
globální	globální	k2eAgFnSc1d1	globální
mapa	mapa	k1gFnSc1	mapa
planety	planeta	k1gFnSc2	planeta
pořízená	pořízený	k2eAgFnSc1d1	pořízená
sondou	sonda	k1gFnSc7	sonda
Mariner	Mariner	k1gInSc1	Mariner
9	[number]	k4	9
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
<g/>
.	.	kIx.	.
<g/>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
technických	technický	k2eAgFnPc2d1	technická
možností	možnost	k1gFnPc2	možnost
lidstva	lidstvo	k1gNnSc2	lidstvo
rostla	růst	k5eAaImAgFnS	růst
i	i	k9	i
výzkumná	výzkumný	k2eAgFnSc1d1	výzkumná
činnost	činnost	k1gFnSc1	činnost
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
Mars	Mars	k1gInSc1	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
byly	být	k5eAaImAgFnP	být
k	k	k7c3	k
Marsu	Mars	k1gInSc3	Mars
vyslány	vyslat	k5eAaPmNgFnP	vyslat
první	první	k4xOgFnPc1	první
sondy	sonda	k1gFnPc1	sonda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
sonda	sonda	k1gFnSc1	sonda
Mariner	Mariner	k1gInSc1	Mariner
4	[number]	k4	4
z	z	k7c2	z
programu	program	k1gInSc2	program
Mariner	Marinra	k1gFnPc2	Marinra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
pořídily	pořídit	k5eAaPmAgFnP	pořídit
nejprve	nejprve	k6eAd1	nejprve
hrubé	hrubý	k2eAgFnPc1d1	hrubá
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
detailní	detailní	k2eAgInPc4d1	detailní
snímky	snímek	k1gInPc4	snímek
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
začala	začít	k5eAaPmAgFnS	začít
další	další	k2eAgNnSc4d1	další
fáze	fáze	k1gFnSc1	fáze
v	v	k7c6	v
hledání	hledání	k1gNnSc6	hledání
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
pomocí	pomocí	k7c2	pomocí
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
snímky	snímek	k1gInPc1	snímek
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
tekutá	tekutý	k2eAgFnSc1d1	tekutá
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nenachází	nacházet	k5eNaImIp3nS	nacházet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
důkazů	důkaz	k1gInPc2	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
situace	situace	k1gFnSc1	situace
jiná	jiný	k2eAgFnSc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
ukazovaly	ukazovat	k5eAaImAgFnP	ukazovat
říční	říční	k2eAgFnPc1d1	říční
sítě	síť	k1gFnPc1	síť
<g/>
,	,	kIx,	,
koryta	koryto	k1gNnPc1	koryto
<g/>
,	,	kIx,	,
oblasti	oblast	k1gFnPc1	oblast
připomínající	připomínající	k2eAgNnSc1d1	připomínající
pobřeží	pobřeží	k1gNnSc1	pobřeží
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
útvary	útvar	k1gInPc1	útvar
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgFnPc2	který
existovala	existovat	k5eAaImAgFnS	existovat
velká	velký	k2eAgFnSc1d1	velká
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
vlivem	vlivem	k7c2	vlivem
tekoucí	tekoucí	k2eAgFnSc2d1	tekoucí
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
objevení	objevení	k1gNnSc3	objevení
sezónního	sezónní	k2eAgNnSc2d1	sezónní
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
podrobně	podrobně	k6eAd1	podrobně
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
sond	sonda	k1gFnPc2	sonda
Viking	Viking	k1gMnSc1	Viking
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
přistály	přistát	k5eAaPmAgFnP	přistát
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Marsu	Mars	k1gInSc2	Mars
a	a	k8xC	a
po	po	k7c4	po
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
prováděly	provádět	k5eAaImAgFnP	provádět
podrobná	podrobný	k2eAgNnPc1d1	podrobné
meteorologická	meteorologický	k2eAgNnPc1d1	meteorologické
pozorování	pozorování	k1gNnPc1	pozorování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mimo	mimo	k6eAd1	mimo
přímého	přímý	k2eAgNnSc2d1	přímé
pozorování	pozorování	k1gNnSc2	pozorování
povrchových	povrchový	k2eAgInPc2d1	povrchový
projevů	projev	k1gInPc2	projev
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
i	i	k9	i
další	další	k2eAgNnSc4d1	další
fyzikální	fyzikální	k2eAgNnSc4d1	fyzikální
zařízení	zařízení	k1gNnSc4	zařízení
pro	pro	k7c4	pro
detekci	detekce	k1gFnSc4	detekce
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
tak	tak	k9	tak
i	i	k9	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
,	,	kIx,	,
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
obíhajících	obíhající	k2eAgFnPc2d1	obíhající
sond	sonda	k1gFnPc2	sonda
<g/>
.	.	kIx.	.
</s>
<s>
Aktuálně	aktuálně	k6eAd1	aktuálně
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
hledání	hledání	k1gNnSc4	hledání
vody	voda	k1gFnSc2	voda
využívá	využívat	k5eAaPmIp3nS	využívat
několik	několik	k4yIc4	několik
druhů	druh	k1gInPc2	druh
přístrojů	přístroj	k1gInPc2	přístroj
na	na	k7c6	na
sondách	sonda	k1gFnPc6	sonda
Mars	Mars	k1gInSc1	Mars
Global	globat	k5eAaImAgMnS	globat
Surveyor	Surveyor	k1gMnSc1	Surveyor
<g/>
,	,	kIx,	,
Mars	Mars	k1gMnSc1	Mars
Express	express	k1gInSc1	express
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
sondě	sonda	k1gFnSc6	sonda
Mars	Mars	k1gMnSc1	Mars
Odyssey	Odyssea	k1gFnSc2	Odyssea
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
detekci	detekce	k1gFnSc4	detekce
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
určování	určování	k1gNnSc2	určování
chemického	chemický	k2eAgNnSc2d1	chemické
složení	složení	k1gNnSc2	složení
povrchu	povrch	k1gInSc2	povrch
vybavena	vybavit	k5eAaPmNgFnS	vybavit
neutronovým	neutronový	k2eAgInSc7d1	neutronový
spektrometrem	spektrometr	k1gInSc7	spektrometr
<g/>
,	,	kIx,	,
čidlem	čidlo	k1gNnSc7	čidlo
gama	gama	k1gNnSc4	gama
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
detektorem	detektor	k1gInSc7	detektor
vysokoenergetických	vysokoenergetický	k2eAgInPc2d1	vysokoenergetický
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
přístroje	přístroj	k1gInPc1	přístroj
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
detekovat	detekovat	k5eAaImF	detekovat
přítomnost	přítomnost	k1gFnSc4	přítomnost
množství	množství	k1gNnSc2	množství
protonů	proton	k1gInPc2	proton
v	v	k7c6	v
jádrech	jádro	k1gNnPc6	jádro
atomů	atom	k1gInPc2	atom
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
výskyt	výskyt	k1gInSc4	výskyt
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgNnPc2	tento
měření	měření	k1gNnPc2	měření
se	se	k3xPyFc4	se
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
velká	velký	k2eAgFnSc1d1	velká
koncentrace	koncentrace	k1gFnSc1	koncentrace
vodíku	vodík	k1gInSc2	vodík
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pólů	pól	k1gInPc2	pól
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
složení	složení	k1gNnSc4	složení
polárních	polární	k2eAgFnPc2d1	polární
čepiček	čepička	k1gFnPc2	čepička
z	z	k7c2	z
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
těchto	tento	k3xDgNnPc2	tento
zařízení	zařízení	k1gNnPc2	zařízení
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc4	jejich
schopnost	schopnost	k1gFnSc4	schopnost
proniknout	proniknout	k5eAaPmF	proniknout
řádově	řádově	k6eAd1	řádově
jen	jen	k9	jen
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
centimetrů	centimetr	k1gInPc2	centimetr
pod	pod	k7c4	pod
povrch	povrch	k1gInSc4	povrch
planety	planeta	k1gFnSc2	planeta
a	a	k8xC	a
nemožnost	nemožnost	k1gFnSc4	nemožnost
tak	tak	k6eAd1	tak
zjistit	zjistit	k5eAaPmF	zjistit
skutečné	skutečný	k2eAgFnPc4d1	skutečná
zásoby	zásoba	k1gFnPc4	zásoba
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
celé	celý	k2eAgFnSc2d1	celá
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
prostředkem	prostředek	k1gInSc7	prostředek
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
voda	voda	k1gFnSc1	voda
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
hledá	hledat	k5eAaImIp3nS	hledat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
pojízdná	pojízdný	k2eAgNnPc1d1	pojízdné
vozítka	vozítko	k1gNnPc1	vozítko
či	či	k8xC	či
nepohyblivé	pohyblivý	k2eNgFnPc4d1	nepohyblivá
sondy	sonda	k1gFnPc4	sonda
operující	operující	k2eAgFnPc4d1	operující
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
provádět	provádět	k5eAaImF	provádět
přímou	přímý	k2eAgFnSc4d1	přímá
chemickou	chemický	k2eAgFnSc4d1	chemická
analýzu	analýza	k1gFnSc4	analýza
povrchových	povrchový	k2eAgFnPc2d1	povrchová
hornin	hornina	k1gFnPc2	hornina
a	a	k8xC	a
tak	tak	k6eAd1	tak
detekovat	detekovat	k5eAaImF	detekovat
minerály	minerál	k1gInPc4	minerál
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
působením	působení	k1gNnSc7	působení
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Marsu	Mars	k1gInSc2	Mars
úspěšně	úspěšně	k6eAd1	úspěšně
přistály	přistát	k5eAaImAgFnP	přistát
nepohyblivé	pohyblivý	k2eNgFnPc1d1	nepohyblivá
sondy	sonda	k1gFnPc1	sonda
Viking	Viking	k1gMnSc1	Viking
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
pojízdné	pojízdný	k2eAgNnSc4d1	pojízdné
vozítko	vozítko	k1gNnSc4	vozítko
Sojourner	Sojournra	k1gFnPc2	Sojournra
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mise	mise	k1gFnSc2	mise
Mars	Mars	k1gMnSc1	Mars
Pathfinder	Pathfinder	k1gMnSc1	Pathfinder
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
zkoumalo	zkoumat	k5eAaImAgNnS	zkoumat
oblast	oblast	k1gFnSc4	oblast
Ares	Ares	k1gMnSc1	Ares
Vallis	Vallis	k1gFnSc2	Vallis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dvě	dva	k4xCgNnPc1	dva
funkční	funkční	k2eAgNnPc1d1	funkční
vozítka	vozítko	k1gNnPc1	vozítko
Spirit	Spirita	k1gFnPc2	Spirita
(	(	kIx(	(
<g/>
místo	místo	k7c2	místo
přistání	přistání	k1gNnSc2	přistání
kráter	kráter	k1gInSc1	kráter
Gusev	Gusev	k1gFnSc4	Gusev
<g/>
)	)	kIx)	)
a	a	k8xC	a
Opportunity	Opportunita	k1gFnSc2	Opportunita
(	(	kIx(	(
<g/>
místo	místo	k7c2	místo
přistání	přistání	k1gNnSc2	přistání
pláň	pláň	k1gFnSc4	pláň
Meridiani	Meridiaň	k1gFnSc6	Meridiaň
<g/>
)	)	kIx)	)
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mise	mise	k1gFnSc2	mise
Mars	Mars	k1gInSc1	Mars
Exploration	Exploration	k1gInSc4	Exploration
Rover	rover	k1gMnSc1	rover
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
přistála	přistát	k5eAaImAgFnS	přistát
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
polární	polární	k2eAgFnSc6d1	polární
oblasti	oblast	k1gFnSc6	oblast
další	další	k2eAgFnSc1d1	další
americká	americký	k2eAgFnSc1d1	americká
sonda	sonda	k1gFnSc1	sonda
Phoenix	Phoenix	k1gInSc1	Phoenix
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zkoumat	zkoumat	k5eAaImF	zkoumat
složení	složení	k1gNnSc4	složení
regolitu	regolit	k1gInSc2	regolit
<g/>
,	,	kIx,	,
obsah	obsah	k1gInSc1	obsah
ledu	led	k1gInSc2	led
a	a	k8xC	a
historii	historie	k1gFnSc4	historie
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
<g/>
.31	.31	k4	.31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2008	[number]	k4	2008
sonda	sonda	k1gFnSc1	sonda
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
přítomnost	přítomnost	k1gFnSc4	přítomnost
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
našla	najít	k5eAaPmAgFnS	najít
několik	několik	k4yIc4	několik
centimetrů	centimetr	k1gInPc2	centimetr
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Minulost	minulost	k1gFnSc1	minulost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
nepanuje	panovat	k5eNaImIp3nS	panovat
jasná	jasný	k2eAgFnSc1d1	jasná
shoda	shoda	k1gFnSc1	shoda
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
Mars	Mars	k1gInSc1	Mars
v	v	k7c6	v
dávné	dávný	k2eAgFnSc6d1	dávná
minulosti	minulost	k1gFnSc6	minulost
vypadal	vypadat	k5eAaImAgInS	vypadat
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
část	část	k1gFnSc1	část
vědecké	vědecký	k2eAgFnSc2d1	vědecká
obce	obec	k1gFnSc2	obec
zastává	zastávat	k5eAaImIp3nS	zastávat
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mars	Mars	k1gInSc1	Mars
vypadal	vypadat	k5eAaPmAgInS	vypadat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
dnes	dnes	k6eAd1	dnes
–	–	k?	–
studený	studený	k2eAgMnSc1d1	studený
a	a	k8xC	a
s	s	k7c7	s
řídkou	řídký	k2eAgFnSc7d1	řídká
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Tekutá	tekutý	k2eAgFnSc1d1	tekutá
voda	voda	k1gFnSc1	voda
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
povrchu	povrch	k1gInSc6	povrch
se	se	k3xPyFc4	se
objevovala	objevovat	k5eAaImAgFnS	objevovat
jen	jen	k9	jen
dočasně	dočasně	k6eAd1	dočasně
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
uvolnila	uvolnit	k5eAaPmAgFnS	uvolnit
z	z	k7c2	z
napjaté	napjatý	k2eAgFnSc2d1	napjatá
kůry	kůra	k1gFnSc2	kůra
či	či	k8xC	či
byl	být	k5eAaImAgInS	být
rozpuštěn	rozpuštěn	k2eAgInSc4d1	rozpuštěn
půdní	půdní	k2eAgInSc4d1	půdní
led	led	k1gInSc4	led
sopečnou	sopečný	k2eAgFnSc7d1	sopečná
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
zastává	zastávat	k5eAaImIp3nS	zastávat
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mars	Mars	k1gInSc1	Mars
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
teplejší	teplý	k2eAgFnSc1d2	teplejší
s	s	k7c7	s
hustší	hustý	k2eAgFnSc7d2	hustší
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
výskyt	výskyt	k1gInSc4	výskyt
oceánu	oceán	k1gInSc2	oceán
tekuté	tekutý	k2eAgFnSc2d1	tekutá
vody	voda	k1gFnSc2	voda
po	po	k7c4	po
delší	dlouhý	k2eAgNnSc4d2	delší
časové	časový	k2eAgNnSc4d1	časové
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
<g/>
Dle	dle	k7c2	dle
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgInS	být
povrch	povrch	k1gInSc1	povrch
Marsu	Mars	k1gInSc2	Mars
zaplaven	zaplavit	k5eAaPmNgInS	zaplavit
oceánem	oceán	k1gInSc7	oceán
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
rozkládal	rozkládat	k5eAaImAgInS	rozkládat
nejspíše	nejspíše	k9	nejspíše
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
nížin	nížina	k1gFnPc2	nížina
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
planety	planeta	k1gFnSc2	planeta
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
tzv.	tzv.	kA	tzv.
Jižními	jižní	k2eAgFnPc7d1	jižní
vysočinami	vysočina	k1gFnPc7	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
oceán	oceán	k1gInSc1	oceán
existoval	existovat	k5eAaImAgInS	existovat
v	v	k7c6	v
období	období	k1gNnSc6	období
noachianu	noachian	k1gInSc2	noachian
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vliv	k1gInSc7	vliv
ochlazování	ochlazování	k1gNnSc2	ochlazování
planety	planeta	k1gFnSc2	planeta
během	během	k7c2	během
hesperianu	hesperian	k1gInSc2	hesperian
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
zamrznutí	zamrznutí	k1gNnSc3	zamrznutí
<g/>
.	.	kIx.	.
</s>
<s>
Povrchová	povrchový	k2eAgFnSc1d1	povrchová
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
přeměnila	přeměnit	k5eAaPmAgFnS	přeměnit
v	v	k7c4	v
led	led	k1gInSc4	led
a	a	k8xC	a
část	část	k1gFnSc1	část
ji	on	k3xPp3gFnSc4	on
zřejmě	zřejmě	k6eAd1	zřejmě
unikla	uniknout	k5eAaPmAgFnS	uniknout
i	i	k9	i
do	do	k7c2	do
kosmického	kosmický	k2eAgInSc2d1	kosmický
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následné	následný	k2eAgInPc1d1	následný
erozivní	erozivní	k2eAgInPc1d1	erozivní
procesy	proces	k1gInPc1	proces
pohřbily	pohřbít	k5eAaPmAgInP	pohřbít
část	část	k1gFnSc4	část
zmrzlého	zmrzlý	k2eAgInSc2d1	zmrzlý
ledu	led	k1gInSc2	led
pod	pod	k7c4	pod
povrch	povrch	k1gInSc4	povrch
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
fotografickým	fotografický	k2eAgInPc3d1	fotografický
snímkům	snímek	k1gInPc3	snímek
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Marsu	Mars	k1gInSc2	Mars
rozlišeny	rozlišen	k2eAgInPc1d1	rozlišen
morfologické	morfologický	k2eAgInPc1d1	morfologický
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
vodní	vodní	k2eAgFnSc2d1	vodní
činnosti	činnost	k1gFnSc2	činnost
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
říčních	říční	k2eAgNnPc2d1	říční
koryt	koryto	k1gNnPc2	koryto
<g/>
,	,	kIx,	,
sedimentů	sediment	k1gInPc2	sediment
<g/>
,	,	kIx,	,
pozůstatky	pozůstatek	k1gInPc4	pozůstatek
zaplavených	zaplavený	k2eAgFnPc2d1	zaplavená
oblastí	oblast	k1gFnPc2	oblast
či	či	k8xC	či
relikty	relikt	k1gInPc4	relikt
po	po	k7c6	po
rychlém	rychlý	k2eAgInSc6d1	rychlý
úniku	únik	k1gInSc6	únik
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
kryosféry	kryosféra	k1gFnSc2	kryosféra
Marsu	Mars	k1gInSc2	Mars
vlivem	vlivem	k7c2	vlivem
vulkanické	vulkanický	k2eAgFnSc2d1	vulkanická
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeden	jeden	k4xCgInSc1	jeden
podobný	podobný	k2eAgInSc1d1	podobný
obrovský	obrovský	k2eAgInSc1d1	obrovský
únik	únik	k1gInSc1	únik
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
i	i	k9	i
údolí	údolí	k1gNnSc4	údolí
Valles	Vallesa	k1gFnPc2	Vallesa
Marineris	Marineris	k1gFnPc2	Marineris
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
dávné	dávný	k2eAgFnSc6d1	dávná
historii	historie	k1gFnSc6	historie
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
Cerberus	Cerberus	k1gMnSc1	Cerberus
Fossae	Fossa	k1gInSc2	Fossa
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
vznik	vznik	k1gInSc1	vznik
před	před	k7c7	před
5	[number]	k4	5
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Prolomení	prolomení	k1gNnSc1	prolomení
vyvrhlo	vyvrhnout	k5eAaPmAgNnS	vyvrhnout
vodu	voda	k1gFnSc4	voda
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Elysium	elysium	k1gNnSc1	elysium
Planitia	Planitium	k1gNnSc2	Planitium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
ledové	ledový	k2eAgNnSc4d1	ledové
moře	moře	k1gNnSc4	moře
viditelné	viditelný	k2eAgFnSc2d1	viditelná
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
<g/>
Otevřenou	otevřený	k2eAgFnSc7d1	otevřená
otázkou	otázka	k1gFnSc7	otázka
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
,	,	kIx,	,
jaká	jaký	k3yRgFnSc1	jaký
změna	změna	k1gFnSc1	změna
způsobila	způsobit	k5eAaPmAgFnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
klimatické	klimatický	k2eAgFnPc1d1	klimatická
podmínky	podmínka	k1gFnPc1	podmínka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Marsu	Mars	k1gInSc2	Mars
radikálně	radikálně	k6eAd1	radikálně
změnily	změnit	k5eAaPmAgInP	změnit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
tekoucí	tekoucí	k2eAgFnSc1d1	tekoucí
voda	voda	k1gFnSc1	voda
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
povrchu	povrch	k1gInSc6	povrch
přestala	přestat	k5eAaPmAgFnS	přestat
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
některých	některý	k3yIgFnPc2	některý
teorií	teorie	k1gFnPc2	teorie
globální	globální	k2eAgFnSc4d1	globální
změnu	změna	k1gFnSc4	změna
způsobil	způsobit	k5eAaPmAgInS	způsobit
impakt	impakt	k1gInSc1	impakt
obrovského	obrovský	k2eAgNnSc2d1	obrovské
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
změnilo	změnit	k5eAaPmAgNnS	změnit
rotační	rotační	k2eAgFnSc4d1	rotační
dobu	doba	k1gFnSc4	doba
či	či	k8xC	či
orientaci	orientace	k1gFnSc4	orientace
rotační	rotační	k2eAgFnSc2d1	rotační
osy	osa	k1gFnSc2	osa
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
teorie	teorie	k1gFnPc1	teorie
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
proces	proces	k1gInSc1	proces
byl	být	k5eAaImAgInS	být
mnohem	mnohem	k6eAd1	mnohem
pozvolnější	pozvolný	k2eAgInSc1d2	pozvolnější
a	a	k8xC	a
že	že	k8xS	že
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
postupnému	postupný	k2eAgNnSc3d1	postupné
ustávání	ustávání	k1gNnSc3	ustávání
sopečné	sopečný	k2eAgFnSc2d1	sopečná
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
ochlazování	ochlazování	k1gNnSc3	ochlazování
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
atmosféry	atmosféra	k1gFnSc2	atmosféra
současně	současně	k6eAd1	současně
unikala	unikat	k5eAaImAgFnS	unikat
do	do	k7c2	do
okolního	okolní	k2eAgInSc2d1	okolní
kosmického	kosmický	k2eAgInSc2d1	kosmický
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
což	což	k9	což
celý	celý	k2eAgInSc4d1	celý
proces	proces	k1gInSc4	proces
zamrznutí	zamrznutí	k1gNnSc1	zamrznutí
urychlilo	urychlit	k5eAaPmAgNnS	urychlit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
nedá	dát	k5eNaPmIp3nS	dát
jasně	jasně	k6eAd1	jasně
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
přesně	přesně	k6eAd1	přesně
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
stalo	stát	k5eAaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Obecnější	obecní	k2eAgFnSc1d2	obecní
shoda	shoda	k1gFnSc1	shoda
panuje	panovat	k5eAaImIp3nS	panovat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
nacházela	nacházet	k5eAaImAgFnS	nacházet
tekoucí	tekoucí	k2eAgFnSc1d1	tekoucí
voda	voda	k1gFnSc1	voda
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
4	[number]	k4	4
až	až	k9	až
3,5	[number]	k4	3,5
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Řeky	Řek	k1gMnPc4	Řek
===	===	k?	===
</s>
</p>
<p>
<s>
Snad	snad	k9	snad
nejsnadněji	snadno	k6eAd3	snadno
rozpoznatelné	rozpoznatelný	k2eAgInPc1d1	rozpoznatelný
útvary	útvar	k1gInPc1	útvar
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
tekoucí	tekoucí	k2eAgFnSc7d1	tekoucí
vodou	voda	k1gFnSc7	voda
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
jsou	být	k5eAaImIp3nP	být
vyschlé	vyschlý	k2eAgFnPc1d1	vyschlá
říční	říční	k2eAgFnPc1d1	říční
sítě	síť	k1gFnPc1	síť
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgInPc2	který
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
vidět	vidět	k5eAaImF	vidět
spádová	spádový	k2eAgFnSc1d1	spádová
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
vodu	voda	k1gFnSc4	voda
získávaly	získávat	k5eAaImAgFnP	získávat
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
potoky	potok	k1gInPc1	potok
se	se	k3xPyFc4	se
spojují	spojovat	k5eAaImIp3nP	spojovat
do	do	k7c2	do
říček	říčka	k1gFnPc2	říčka
a	a	k8xC	a
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
pak	pak	k6eAd1	pak
sváděly	svádět	k5eAaImAgFnP	svádět
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
Jižních	jižní	k2eAgFnPc2d1	jižní
vysočin	vysočina	k1gFnPc2	vysočina
do	do	k7c2	do
severních	severní	k2eAgFnPc2d1	severní
nížin	nížina	k1gFnPc2	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklá	vzniklý	k2eAgNnPc1d1	vzniklé
koryta	koryto	k1gNnPc1	koryto
mají	mít	k5eAaImIp3nP	mít
shodné	shodný	k2eAgInPc1d1	shodný
znaky	znak	k1gInPc1	znak
s	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
pozemskými	pozemský	k2eAgMnPc7d1	pozemský
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
zařezávání	zařezávání	k1gNnSc4	zařezávání
do	do	k7c2	do
skalního	skalní	k2eAgNnSc2d1	skalní
podloží	podloží	k1gNnSc2	podloží
<g/>
,	,	kIx,	,
vzniklé	vzniklý	k2eAgInPc4d1	vzniklý
sedimenty	sediment	k1gInPc4	sediment
či	či	k8xC	či
meandrující	meandrující	k2eAgNnPc4d1	meandrující
koryta	koryto	k1gNnPc4	koryto
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Říční	říční	k2eAgFnPc1d1	říční
sítě	síť	k1gFnPc1	síť
napovídají	napovídat	k5eAaBmIp3nP	napovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
klima	klima	k1gNnSc1	klima
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Marsu	Mars	k1gInSc2	Mars
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
jiné	jiný	k2eAgNnSc4d1	jiné
než	než	k8xS	než
to	ten	k3xDgNnSc1	ten
dnešní	dnešní	k2eAgNnSc1d1	dnešní
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc1d1	podobná
sítě	síť	k1gFnPc1	síť
vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
napájena	napájet	k5eAaImNgFnS	napájet
tekoucí	tekoucí	k2eAgFnSc7d1	tekoucí
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
dopadá	dopadat	k5eAaImIp3nS	dopadat
z	z	k7c2	z
atmosférických	atmosférický	k2eAgFnPc2d1	atmosférická
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jejich	jejich	k3xOp3gInSc4	jejich
vznik	vznik	k1gInSc4	vznik
tak	tak	k6eAd1	tak
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
Mars	Mars	k1gInSc1	Mars
teplejší	teplý	k2eAgInSc1d2	teplejší
s	s	k7c7	s
proměnlivým	proměnlivý	k2eAgNnSc7d1	proměnlivé
počasím	počasí	k1gNnSc7	počasí
umožňujícím	umožňující	k2eAgNnSc7d1	umožňující
déšť	déšť	k1gInSc1	déšť
<g/>
.	.	kIx.	.
<g/>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
řeky	řeka	k1gFnPc1	řeka
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
planety	planeta	k1gFnPc1	planeta
zmizely	zmizet	k5eAaPmAgFnP	zmizet
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
době	doba	k1gFnSc6	doba
před	před	k7c7	před
3,5	[number]	k4	3,5
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
teorii	teorie	k1gFnSc3	teorie
o	o	k7c6	o
povrchových	povrchový	k2eAgFnPc6d1	povrchová
řekách	řeka	k1gFnPc6	řeka
hovoří	hovořit	k5eAaImIp3nS	hovořit
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
některá	některý	k3yIgNnPc1	některý
potenciální	potenciální	k2eAgNnPc1d1	potenciální
říční	říční	k2eAgNnPc1d1	říční
údolí	údolí	k1gNnPc1	údolí
nemají	mít	k5eNaImIp3nP	mít
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
dně	dno	k1gNnSc6	dno
vyhloubené	vyhloubený	k2eAgNnSc1d1	vyhloubené
říční	říční	k2eAgNnSc1d1	říční
koryto	koryto	k1gNnSc1	koryto
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
voda	voda	k1gFnSc1	voda
proudí	proudit	k5eAaImIp3nS	proudit
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
vědci	vědec	k1gMnPc1	vědec
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
takto	takto	k6eAd1	takto
vzniklá	vzniklý	k2eAgNnPc1d1	vzniklé
údolí	údolí	k1gNnPc1	údolí
mohla	moct	k5eAaImAgNnP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
proudící	proudící	k2eAgFnSc7d1	proudící
vodou	voda	k1gFnSc7	voda
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
zřícením	zřícení	k1gNnSc7	zřícení
stropu	strop	k1gInSc2	strop
do	do	k7c2	do
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
vodního	vodní	k2eAgInSc2d1	vodní
tunelu	tunel	k1gInSc2	tunel
<g/>
.	.	kIx.	.
<g/>
Zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
obrovská	obrovský	k2eAgNnPc1d1	obrovské
řečiště	řečiště	k1gNnPc1	řečiště
vymykající	vymykající	k2eAgNnPc1d1	vymykající
se	se	k3xPyFc4	se
pozemským	pozemský	k2eAgNnSc7d1	pozemské
srovnáním	srovnání	k1gNnSc7	srovnání
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
mohou	moct	k5eAaImIp3nP	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
100	[number]	k4	100
až	až	k9	až
200	[number]	k4	200
km	km	kA	km
na	na	k7c4	na
šířku	šířka	k1gFnSc4	šířka
a	a	k8xC	a
1000	[number]	k4	1000
až	až	k9	až
2000	[number]	k4	2000
km	km	kA	km
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
vyskytující	vyskytující	k2eAgFnSc4d1	vyskytující
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
útvary	útvar	k1gInPc1	útvar
jsou	být	k5eAaImIp3nP	být
výsledkem	výsledek	k1gInSc7	výsledek
obrovských	obrovský	k2eAgFnPc2d1	obrovská
záplav	záplava	k1gFnPc2	záplava
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgFnPc3	který
několikrát	několikrát	k6eAd1	několikrát
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Marsu	Mars	k1gInSc2	Mars
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
nejspíše	nejspíše	k9	nejspíše
jako	jako	k9	jako
projev	projev	k1gInSc4	projev
porušení	porušení	k1gNnSc2	porušení
marsovské	marsovský	k2eAgFnSc2d1	marsovská
kůry	kůra	k1gFnSc2	kůra
vlivem	vliv	k1gInSc7	vliv
impaktů	impakt	k1gInPc2	impakt
nebo	nebo	k8xC	nebo
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
obrovské	obrovský	k2eAgInPc4d1	obrovský
vodní	vodní	k2eAgInPc4d1	vodní
rezervoáry	rezervoár	k1gInPc4	rezervoár
a	a	k8xC	a
následným	následný	k2eAgInSc7d1	následný
únikem	únik	k1gInSc7	únik
této	tento	k3xDgFnSc2	tento
vody	voda	k1gFnSc2	voda
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
pozorování	pozorování	k1gNnSc2	pozorování
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
těmto	tento	k3xDgFnPc3	tento
událostem	událost	k1gFnPc3	událost
došlo	dojít	k5eAaPmAgNnS	dojít
vícekrát	vícekrát	k6eAd1	vícekrát
v	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
2,5	[number]	k4	2,5
až	až	k9	až
1,5	[number]	k4	1,5
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Přesný	přesný	k2eAgInSc1d1	přesný
mechanismus	mechanismus	k1gInSc1	mechanismus
není	být	k5eNaImIp3nS	být
doposud	doposud	k6eAd1	doposud
znám	znám	k2eAgMnSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
ale	ale	k9	ale
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
tak	tak	k6eAd1	tak
obrovské	obrovský	k2eAgNnSc1d1	obrovské
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohlo	moct	k5eAaImAgNnS	moct
umožnit	umožnit	k5eAaPmF	umožnit
vzniknout	vzniknout	k5eAaPmF	vzniknout
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Oceán	oceán	k1gInSc1	oceán
===	===	k?	===
</s>
</p>
<p>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
otázek	otázka	k1gFnPc2	otázka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
skutečně	skutečně	k6eAd1	skutečně
existoval	existovat	k5eAaImAgInS	existovat
komplexní	komplexní	k2eAgInSc1d1	komplexní
oceán	oceán	k1gInSc1	oceán
anebo	anebo	k8xC	anebo
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
jen	jen	k9	jen
o	o	k7c4	o
několik	několik	k4yIc4	několik
lokálních	lokální	k2eAgFnPc2d1	lokální
zaplavených	zaplavený	k2eAgFnPc2d1	zaplavená
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
předpoklady	předpoklad	k1gInPc4	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
oceán	oceán	k1gInSc1	oceán
nejspíše	nejspíše	k9	nejspíše
existoval	existovat	k5eAaImAgInS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
doklady	doklad	k1gInPc4	doklad
jeho	jeho	k3xOp3gFnSc2	jeho
existence	existence	k1gFnSc2	existence
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
počítají	počítat	k5eAaImIp3nP	počítat
geologické	geologický	k2eAgInPc1d1	geologický
útvary	útvar	k1gInPc1	útvar
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zdánlivě	zdánlivě	k6eAd1	zdánlivě
připomínají	připomínat	k5eAaImIp3nP	připomínat
mořské	mořský	k2eAgNnSc4d1	mořské
pobřeží	pobřeží	k1gNnSc4	pobřeží
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgInPc1d1	známý
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
severní	severní	k2eAgFnSc1d1	severní
oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
zcela	zcela	k6eAd1	zcela
hladká	hladkat	k5eAaImIp3nS	hladkat
<g/>
,	,	kIx,	,
zdánlivě	zdánlivě	k6eAd1	zdánlivě
vyhlazená	vyhlazený	k2eAgFnSc1d1	vyhlazená
erozivní	erozivní	k2eAgFnSc7d1	erozivní
silou	síla	k1gFnSc7	síla
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
dříve	dříve	k6eAd2	dříve
tvořila	tvořit	k5eAaImAgFnS	tvořit
oceánské	oceánský	k2eAgNnSc4d1	oceánské
dno	dno	k1gNnSc4	dno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
vyskytoval	vyskytovat	k5eAaImAgInS	vyskytovat
oceán	oceán	k1gInSc1	oceán
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
začala	začít	k5eAaPmAgFnS	začít
část	část	k1gFnSc1	část
vědců	vědec	k1gMnPc2	vědec
podrobněji	podrobně	k6eAd2	podrobně
zaobírat	zaobírat	k5eAaImF	zaobírat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
výzkumu	výzkum	k1gInSc2	výzkum
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
mohl	moct	k5eAaImAgInS	moct
existovat	existovat	k5eAaImF	existovat
oceán	oceán	k1gInSc1	oceán
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
oblastech	oblast	k1gFnPc6	oblast
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Severní	severní	k2eAgInSc1d1	severní
oceán	oceán	k1gInSc1	oceán
(	(	kIx(	(
<g/>
Oceanus	Oceanus	k1gInSc1	Oceanus
Borealis	Borealis	k1gFnSc2	Borealis
<g/>
)	)	kIx)	)
–	–	k?	–
vodní	vodní	k2eAgFnSc1d1	vodní
plocha	plocha	k1gFnSc1	plocha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
rozkládala	rozkládat	k5eAaImAgFnS	rozkládat
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
severních	severní	k2eAgFnPc2d1	severní
planin	planina	k1gFnPc2	planina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
model	model	k1gInSc1	model
oceánu	oceán	k1gInSc2	oceán
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
tohoto	tento	k3xDgInSc2	tento
oceánu	oceán	k1gInSc2	oceán
popisuje	popisovat	k5eAaImIp3nS	popisovat
jako	jako	k9	jako
výsledek	výsledek	k1gInSc1	výsledek
ohromné	ohromný	k2eAgFnSc2d1	ohromná
záplavy	záplava	k1gFnSc2	záplava
o	o	k7c6	o
rychlosti	rychlost	k1gFnSc6	rychlost
108	[number]	k4	108
až	až	k9	až
109	[number]	k4	109
m	m	kA	m
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
s	s	k7c7	s
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
o	o	k7c6	o
celkovém	celkový	k2eAgInSc6d1	celkový
objemu	objem	k1gInSc6	objem
105	[number]	k4	105
až	až	k9	až
107	[number]	k4	107
km	km	kA	km
<g/>
3	[number]	k4	3
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
následek	následek	k1gInSc4	následek
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
sopečné	sopečný	k2eAgFnSc2d1	sopečná
aktivity	aktivita	k1gFnSc2	aktivita
v	v	k7c6	v
celoplanetárním	celoplanetární	k2eAgNnSc6d1	celoplanetární
měřítku	měřítko	k1gNnSc6	měřítko
(	(	kIx(	(
<g/>
žádný	žádný	k3yNgInSc4	žádný
impakt	impakt	k1gInSc4	impakt
vesmírného	vesmírný	k2eAgNnSc2d1	vesmírné
tělesa	těleso	k1gNnSc2	těleso
by	by	kYmCp3nS	by
nejspíše	nejspíše	k9	nejspíše
nemohl	moct	k5eNaImAgInS	moct
zapříčinit	zapříčinit	k5eAaPmF	zapříčinit
takto	takto	k6eAd1	takto
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
oteplení	oteplení	k1gNnSc4	oteplení
projevující	projevující	k2eAgFnSc2d1	projevující
se	se	k3xPyFc4	se
roztáním	roztání	k1gNnSc7	roztání
permafrostu	permafrost	k1gInSc2	permafrost
a	a	k8xC	a
následné	následný	k2eAgFnPc1d1	následná
záplavy	záplava	k1gFnPc1	záplava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vypařování	vypařování	k1gNnSc1	vypařování
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
z	z	k7c2	z
plochy	plocha	k1gFnSc2	plocha
oceánu	oceán	k1gInSc2	oceán
obohatilo	obohatit	k5eAaPmAgNnS	obohatit
skleníkové	skleníkový	k2eAgInPc4d1	skleníkový
plyny	plyn	k1gInPc4	plyn
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
vznik	vznik	k1gInSc4	vznik
teplejší	teplý	k2eAgInSc1d2	teplejší
a	a	k8xC	a
hustší	hustý	k2eAgFnPc1d2	hustší
atmosféry	atmosféra	k1gFnPc1	atmosféra
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
dešťové	dešťový	k2eAgFnPc1d1	dešťová
srážky	srážka	k1gFnPc1	srážka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oceán	oceán	k1gInSc1	oceán
v	v	k7c6	v
severních	severní	k2eAgFnPc6d1	severní
nížinách	nížina	k1gFnPc6	nížina
(	(	kIx(	(
<g/>
Utopia	Utopia	k1gFnSc1	Utopia
Planitia	Planitia	k1gFnSc1	Planitia
<g/>
)	)	kIx)	)
–	–	k?	–
je	být	k5eAaImIp3nS	být
předpokládané	předpokládaný	k2eAgNnSc1d1	předpokládané
menší	malý	k2eAgNnSc1d2	menší
vodní	vodní	k2eAgNnSc1d1	vodní
těleso	těleso	k1gNnSc1	těleso
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyplňovalo	vyplňovat	k5eAaImAgNnS	vyplňovat
oblast	oblast	k1gFnSc4	oblast
Utopia	Utopium	k1gNnSc2	Utopium
Planitia	Planitium	k1gNnSc2	Planitium
a	a	k8xC	a
které	který	k3yRgNnSc1	který
teoreticky	teoreticky	k6eAd1	teoreticky
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pouze	pouze	k6eAd1	pouze
zlomkovou	zlomkový	k2eAgFnSc7d1	zlomková
částí	část	k1gFnSc7	část
většího	veliký	k2eAgInSc2d2	veliký
oceánu	oceán	k1gInSc2	oceán
Oceanus	Oceanus	k1gMnSc1	Oceanus
Borealis	Borealis	k1gFnSc1	Borealis
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
oblasti	oblast	k1gFnSc2	oblast
Utopia	Utopia	k1gFnSc1	Utopia
Planitia	Planitia	k1gFnSc1	Planitia
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
známky	známka	k1gFnPc4	známka
po	po	k7c6	po
přítomnosti	přítomnost	k1gFnSc6	přítomnost
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
vrstvy	vrstva	k1gFnPc1	vrstva
sedimentů	sediment	k1gInPc2	sediment
či	či	k8xC	či
teras	terasa	k1gFnPc2	terasa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průzkumná	průzkumný	k2eAgNnPc1d1	průzkumné
vozítka	vozítko	k1gNnPc1	vozítko
Spirit	Spirita	k1gFnPc2	Spirita
a	a	k8xC	a
Opportunity	Opportunita	k1gFnSc2	Opportunita
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
sírany	síran	k1gInPc4	síran
vznikající	vznikající	k2eAgFnSc4d1	vznikající
během	během	k7c2	během
vypařování	vypařování	k1gNnSc2	vypařování
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
předchozí	předchozí	k2eAgInSc1d1	předchozí
výskyt	výskyt	k1gInSc1	výskyt
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
vědce	vědec	k1gMnPc4	vědec
neznámý	známý	k2eNgInSc4d1	neznámý
a	a	k8xC	a
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
oceánu	oceán	k1gInSc6	oceán
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
má	mít	k5eAaImIp3nS	mít
Mars	Mars	k1gInSc4	Mars
rozdílné	rozdílný	k2eAgNnSc1d1	rozdílné
složení	složení	k1gNnSc1	složení
atmosféry	atmosféra	k1gFnSc2	atmosféra
než	než	k8xS	než
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
chemické	chemický	k2eAgNnSc1d1	chemické
složení	složení	k1gNnSc1	složení
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
rozdílné	rozdílný	k2eAgInPc1d1	rozdílný
<g/>
.	.	kIx.	.
</s>
<s>
Vysoký	vysoký	k2eAgInSc1d1	vysoký
obsah	obsah	k1gInSc1	obsah
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
síry	síra	k1gFnSc2	síra
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
nejspíše	nejspíše	k9	nejspíše
zapříčinil	zapříčinit	k5eAaPmAgMnS	zapříčinit
<g/>
,	,	kIx,	,
že	že	k8xS	že
voda	voda	k1gFnSc1	voda
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
byla	být	k5eAaImAgFnS	být
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
kyselá	kyselý	k2eAgFnSc1d1	kyselá
než	než	k8xS	než
ta	ten	k3xDgFnSc1	ten
pozemská	pozemský	k2eAgFnSc1d1	pozemská
<g/>
.	.	kIx.	.
</s>
<s>
Kyselé	kyselé	k1gNnSc1	kyselé
prostředí	prostředí	k1gNnSc2	prostředí
bránilo	bránit	k5eAaImAgNnS	bránit
srážení	srážení	k1gNnSc4	srážení
karbonátů	karbonát	k1gInPc2	karbonát
z	z	k7c2	z
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Sopečná	sopečný	k2eAgFnSc1d1	sopečná
aktivita	aktivita	k1gFnSc1	aktivita
v	v	k7c6	v
noachianu	noachian	k1gInSc6	noachian
vypouštěla	vypouštět	k5eAaImAgFnS	vypouštět
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
stále	stále	k6eAd1	stále
další	další	k2eAgNnSc4d1	další
množství	množství	k1gNnSc4	množství
sopečných	sopečný	k2eAgInPc2d1	sopečný
plyn	plyn	k1gInSc4	plyn
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zvyšovalo	zvyšovat	k5eAaImAgNnS	zvyšovat
jeho	jeho	k3xOp3gFnSc4	jeho
koncentraci	koncentrace	k1gFnSc4	koncentrace
až	až	k9	až
na	na	k7c4	na
současný	současný	k2eAgInSc4d1	současný
stav	stav	k1gInSc4	stav
(	(	kIx(	(
<g/>
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
tvoří	tvořit	k5eAaImIp3nS	tvořit
95,32	[number]	k4	95,32
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
modelu	model	k1gInSc2	model
kyselého	kyselý	k2eAgInSc2d1	kyselý
oceánu	oceán	k1gInSc2	oceán
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
chybějící	chybějící	k2eAgInPc4d1	chybějící
karbonáty	karbonát	k1gInPc4	karbonát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
by	by	kYmCp3nS	by
s	s	k7c7	s
oceánem	oceán	k1gInSc7	oceán
nejspíše	nejspíše	k9	nejspíše
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Erodované	erodovaný	k2eAgInPc1d1	erodovaný
krátery	kráter	k1gInPc1	kráter
===	===	k?	===
</s>
</p>
<p>
<s>
Krátery	kráter	k1gInPc1	kráter
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
v	v	k7c6	v
rané	raný	k2eAgFnSc6d1	raná
historii	historie	k1gFnSc6	historie
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
jeví	jevit	k5eAaImIp3nS	jevit
silné	silný	k2eAgFnPc4d1	silná
známky	známka	k1gFnPc4	známka
eroze	eroze	k1gFnSc1	eroze
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
větrné	větrný	k2eAgInPc1d1	větrný
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
vodní	vodní	k2eAgFnSc1d1	vodní
<g/>
.	.	kIx.	.
</s>
<s>
Krátery	kráter	k1gInPc1	kráter
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
menší	malý	k2eAgFnSc4d2	menší
než	než	k8xS	než
15	[number]	k4	15
km	km	kA	km
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
zarovnány	zarovnán	k2eAgMnPc4d1	zarovnán
a	a	k8xC	a
jen	jen	k6eAd1	jen
velmi	velmi	k6eAd1	velmi
obtížně	obtížně	k6eAd1	obtížně
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
detekují	detekovat	k5eAaImIp3nP	detekovat
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInPc1d2	veliký
krátery	kráter	k1gInPc1	kráter
nesou	nést	k5eAaImIp3nP	nést
silné	silný	k2eAgFnPc4d1	silná
známky	známka	k1gFnPc4	známka
tekoucí	tekoucí	k2eAgFnSc2d1	tekoucí
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
napovídá	napovídat	k5eAaBmIp3nS	napovídat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgInP	být
vystaveny	vystavit	k5eAaPmNgInP	vystavit
vlhkému	vlhký	k2eAgNnSc3d1	vlhké
klimatu	klima	k1gNnSc3	klima
se	s	k7c7	s
srážkovou	srážkový	k2eAgFnSc7d1	srážková
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Krátery	kráter	k1gInPc1	kráter
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
mladší	mladý	k2eAgInPc1d2	mladší
3,5	[number]	k4	3,5
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
však	však	k9	však
podobné	podobný	k2eAgNnSc4d1	podobné
poškození	poškození	k1gNnSc4	poškození
nenesou	nést	k5eNaImIp3nP	nést
či	či	k8xC	či
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgFnSc1d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
změně	změna	k1gFnSc3	změna
klimatu	klima	k1gNnSc2	klima
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
planeta	planeta	k1gFnSc1	planeta
stala	stát	k5eAaPmAgFnS	stát
opět	opět	k6eAd1	opět
suchou	suchý	k2eAgFnSc4d1	suchá
<g/>
.	.	kIx.	.
<g/>
Rychlost	rychlost	k1gFnSc4	rychlost
eroze	eroze	k1gFnSc2	eroze
kráterů	kráter	k1gInPc2	kráter
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
známý	známý	k2eAgInSc1d1	známý
jev	jev	k1gInSc1	jev
popsaný	popsaný	k2eAgInSc1d1	popsaný
z	z	k7c2	z
Měsíce	měsíc	k1gInSc2	měsíc
s	s	k7c7	s
přesnou	přesný	k2eAgFnSc7d1	přesná
datací	datace	k1gFnSc7	datace
díky	díky	k7c3	díky
dovezeným	dovezený	k2eAgInPc3d1	dovezený
vzorkům	vzorek	k1gInPc3	vzorek
měsíčních	měsíční	k2eAgFnPc2d1	měsíční
hornin	hornina	k1gFnPc2	hornina
uplatněný	uplatněný	k2eAgMnSc1d1	uplatněný
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
srovnání	srovnání	k1gNnSc6	srovnání
jeví	jevit	k5eAaImIp3nP	jevit
krátery	kráter	k1gInPc1	kráter
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
mnohem	mnohem	k6eAd1	mnohem
silnější	silný	k2eAgInSc1d2	silnější
stupeň	stupeň	k1gInSc1	stupeň
eroze	eroze	k1gFnSc1	eroze
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
sice	sice	k8xC	sice
vanou	vanout	k5eAaImIp3nP	vanout
silné	silný	k2eAgInPc1d1	silný
větry	vítr	k1gInPc1	vítr
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc3	který
erozi	eroze	k1gFnSc3	eroze
také	také	k9	také
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ty	ten	k3xDgMnPc4	ten
nemají	mít	k5eNaImIp3nP	mít
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
sílu	síla	k1gFnSc4	síla
pro	pro	k7c4	pro
takto	takto	k6eAd1	takto
silnou	silný	k2eAgFnSc4d1	silná
erozi	eroze	k1gFnSc4	eroze
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
některé	některý	k3yIgInPc1	některý
krátery	kráter	k1gInPc1	kráter
jsou	být	k5eAaImIp3nP	být
přímo	přímo	k6eAd1	přímo
napojeny	napojit	k5eAaPmNgInP	napojit
na	na	k7c4	na
říční	říční	k2eAgFnSc4d1	říční
síť	síť	k1gFnSc4	síť
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
dno	dno	k1gNnSc1	dno
je	být	k5eAaImIp3nS	být
vyplněno	vyplnit	k5eAaPmNgNnS	vyplnit
sedimenty	sediment	k1gInPc1	sediment
či	či	k8xC	či
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
nacházejí	nacházet	k5eAaImIp3nP	nacházet
útvary	útvar	k1gInPc1	útvar
připomínající	připomínající	k2eAgFnSc2d1	připomínající
říční	říční	k2eAgFnSc2d1	říční
delty	delta	k1gFnSc2	delta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sedimentární	sedimentární	k2eAgFnPc1d1	sedimentární
vrstvy	vrstva	k1gFnPc1	vrstva
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Marsu	Mars	k1gInSc2	Mars
jsou	být	k5eAaImIp3nP	být
pozorovány	pozorován	k2eAgFnPc1d1	pozorována
oblasti	oblast	k1gFnPc1	oblast
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgFnPc6	který
jsou	být	k5eAaImIp3nP	být
vidět	vidět	k5eAaImF	vidět
vrstvy	vrstva	k1gFnPc4	vrstva
sedimentu	sediment	k1gInSc2	sediment
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nemohou	moct	k5eNaImIp3nP	moct
vznikat	vznikat	k5eAaImF	vznikat
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
současných	současný	k2eAgFnPc2d1	současná
znalostí	znalost	k1gFnPc2	znalost
<g/>
)	)	kIx)	)
bez	bez	k7c2	bez
přítomnosti	přítomnost	k1gFnSc2	přítomnost
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
spektrometrických	spektrometrický	k2eAgNnPc2d1	spektrometrické
měření	měření	k1gNnPc2	měření
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
nepodařilo	podařit	k5eNaPmAgNnS	podařit
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
lokalizovat	lokalizovat	k5eAaBmF	lokalizovat
jílové	jílový	k2eAgFnPc1d1	jílová
horniny	hornina	k1gFnPc1	hornina
například	například	k6eAd1	například
palagonit	palagonit	k5eAaImF	palagonit
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
hydratací	hydratace	k1gFnSc7	hydratace
vulkanického	vulkanický	k2eAgInSc2d1	vulkanický
materiálu	materiál	k1gInSc2	materiál
při	při	k7c6	při
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
tekutou	tekutý	k2eAgFnSc7d1	tekutá
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
jenž	jenž	k3xRgMnSc1	jenž
by	by	kYmCp3nS	by
na	na	k7c6	na
převážně	převážně	k6eAd1	převážně
bazaltovém	bazaltový	k2eAgInSc6d1	bazaltový
Marsu	Mars	k1gInSc6	Mars
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
poměrně	poměrně	k6eAd1	poměrně
hojný	hojný	k2eAgMnSc1d1	hojný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
měření	měření	k1gNnSc2	měření
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
velké	velký	k2eAgNnSc4d1	velké
zastoupení	zastoupení	k1gNnSc4	zastoupení
sulfátů	sulfát	k1gInPc2	sulfát
<g/>
,	,	kIx,	,
chloridů	chlorid	k1gInPc2	chlorid
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
solích	sůl	k1gFnPc6	sůl
spadající	spadající	k2eAgFnSc2d1	spadající
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
evaporitů	evaporit	k1gInPc2	evaporit
vznikajících	vznikající	k2eAgInPc2d1	vznikající
vodní	vodní	k2eAgFnSc7d1	vodní
alterací	alterace	k1gFnSc7	alterace
<g/>
.	.	kIx.	.
</s>
<s>
Transportované	transportovaný	k2eAgFnPc1d1	transportovaná
části	část	k1gFnPc1	část
jsou	být	k5eAaImIp3nP	být
přenášeny	přenášen	k2eAgMnPc4d1	přenášen
na	na	k7c4	na
nová	nový	k2eAgNnPc4d1	nové
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začínají	začínat	k5eAaImIp3nP	začínat
postupně	postupně	k6eAd1	postupně
sedimentovat	sedimentovat	k5eAaImF	sedimentovat
a	a	k8xC	a
vytvářet	vytvářet	k5eAaImF	vytvářet
nové	nový	k2eAgFnSc2d1	nová
horniny	hornina	k1gFnSc2	hornina
s	s	k7c7	s
typickou	typický	k2eAgFnSc7d1	typická
strukturou	struktura	k1gFnSc7	struktura
a	a	k8xC	a
texturou	textura	k1gFnSc7	textura
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc1	tento
oblasti	oblast	k1gFnSc6	oblast
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
fosílií	fosílie	k1gFnPc2	fosílie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
unášeny	unášen	k2eAgInPc1d1	unášen
a	a	k8xC	a
pak	pak	k6eAd1	pak
uloženy	uložit	k5eAaPmNgInP	uložit
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
někdy	někdy	k6eAd1	někdy
život	život	k1gInSc1	život
existoval	existovat	k5eAaImAgInS	existovat
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc7	jeho
zbytky	zbytek	k1gInPc7	zbytek
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
daly	dát	k5eAaPmAgInP	dát
nalézt	nalézt	k5eAaPmF	nalézt
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
<g/>
Oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
sedimenty	sediment	k1gInPc1	sediment
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
rozesety	rozesít	k5eAaPmNgInP	rozesít
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
planetě	planeta	k1gFnSc6	planeta
na	na	k7c6	na
nejrůznějších	různý	k2eAgNnPc6d3	nejrůznější
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
impaktních	impaktní	k2eAgInPc6d1	impaktní
kráterech	kráter	k1gInPc6	kráter
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
oblasti	oblast	k1gFnSc6	oblast
Arabia	Arabium	k1gNnSc2	Arabium
Terra	Terr	k1gInSc2	Terr
<g/>
,	,	kIx,	,
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
Terra	Terra	k1gMnSc1	Terra
Meridiani	Meridian	k1gMnPc1	Meridian
<g/>
,	,	kIx,	,
v	v	k7c6	v
roklích	rokle	k1gFnPc6	rokle
Valles	Vallesa	k1gFnPc2	Vallesa
Marineris	Marineris	k1gFnSc2	Marineris
a	a	k8xC	a
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
pánve	pánev	k1gFnSc2	pánev
Hellas	Hellas	k1gFnSc1	Hellas
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgFnPc1d1	podobná
pozemským	pozemský	k2eAgInPc3d1	pozemský
útvarům	útvar	k1gInPc3	útvar
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Grand	grand	k1gMnSc1	grand
Canyon	Canyon	k1gMnSc1	Canyon
či	či	k8xC	či
Painted	Painted	k1gMnSc1	Painted
Desert	desert	k1gInSc4	desert
v	v	k7c6	v
Arizoně	Arizona	k1gFnSc6	Arizona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sedimenty	sediment	k1gInPc1	sediment
vznikaly	vznikat	k5eAaImAgInP	vznikat
nejspíše	nejspíše	k9	nejspíše
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
oceánu	oceán	k1gInSc2	oceán
či	či	k8xC	či
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vyplňovaly	vyplňovat	k5eAaImAgInP	vyplňovat
krátery	kráter	k1gInPc4	kráter
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
deprese	deprese	k1gFnPc4	deprese
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
stáří	stáří	k1gNnSc1	stáří
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
výskytem	výskyt	k1gInSc7	výskyt
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
asi	asi	k9	asi
před	před	k7c7	před
3,5	[number]	k4	3,5
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
tvoří	tvořit	k5eAaImIp3nP	tvořit
vrstvy	vrstva	k1gFnPc1	vrstva
podrobnou	podrobný	k2eAgFnSc4d1	podrobná
dataci	datace	k1gFnSc4	datace
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
epoch	epocha	k1gFnPc2	epocha
historie	historie	k1gFnSc2	historie
Marsu	Mars	k1gInSc2	Mars
dle	dle	k7c2	dle
pravidla	pravidlo	k1gNnSc2	pravidlo
superpozice	superpozice	k1gFnSc2	superpozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
Aktuální	aktuální	k2eAgFnPc1d1	aktuální
podmínky	podmínka	k1gFnPc1	podmínka
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Marsu	Mars	k1gInSc2	Mars
neumožňují	umožňovat	k5eNaImIp3nP	umožňovat
existenci	existence	k1gFnSc4	existence
tekuté	tekutý	k2eAgFnSc2d1	tekutá
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
delším	dlouhý	k2eAgInSc6d2	delší
časovém	časový	k2eAgInSc6d1	časový
horizontu	horizont	k1gInSc6	horizont
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
vody	voda	k1gFnSc2	voda
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
v	v	k7c6	v
polárních	polární	k2eAgFnPc6d1	polární
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
permafrostu	permafrost	k1gInSc6	permafrost
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
schována	schován	k2eAgFnSc1d1	schována
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
aquifer	aquifra	k1gFnPc2	aquifra
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
existenci	existence	k1gFnSc6	existence
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
zbyly	zbýt	k5eAaPmAgInP	zbýt
jen	jen	k6eAd1	jen
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
zaoblených	zaoblený	k2eAgInPc2d1	zaoblený
kamenů	kámen	k1gInPc2	kámen
<g/>
,	,	kIx,	,
koryt	koryto	k1gNnPc2	koryto
<g/>
,	,	kIx,	,
řečišť	řečiště	k1gNnPc2	řečiště
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Mars	Mars	k1gInSc1	Mars
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
být	být	k5eAaImF	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
suchým	suchý	k2eAgInSc7d1	suchý
světem	svět	k1gInSc7	svět
bez	bez	k7c2	bez
tekoucí	tekoucí	k2eAgFnSc2d1	tekoucí
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
představa	představa	k1gFnSc1	představa
platila	platit	k5eAaImAgFnS	platit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
americká	americký	k2eAgFnSc1d1	americká
sonda	sonda	k1gFnSc1	sonda
Mars	Mars	k1gInSc1	Mars
Global	globat	k5eAaImAgMnS	globat
Surveyor	Surveyor	k1gMnSc1	Surveyor
přinesla	přinést	k5eAaPmAgFnS	přinést
snímky	snímek	k1gInPc4	snímek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ukázaly	ukázat	k5eAaPmAgInP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
mohla	moct	k5eAaImAgFnS	moct
tekoucí	tekoucí	k2eAgFnSc1d1	tekoucí
voda	voda	k1gFnSc1	voda
nacházet	nacházet	k5eAaImF	nacházet
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pořízených	pořízený	k2eAgFnPc6d1	pořízená
fotografiích	fotografia	k1gFnPc6	fotografia
byla	být	k5eAaImAgFnS	být
stěna	stěna	k1gFnSc1	stěna
kráteru	kráter	k1gInSc2	kráter
poblíž	poblíž	k7c2	poblíž
hory	hora	k1gFnSc2	hora
Centauri	Centauri	k1gNnSc2	Centauri
Montes	Montesa	k1gFnPc2	Montesa
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
nová	nový	k2eAgFnSc1d1	nová
vrstva	vrstva	k1gFnSc1	vrstva
sedimentů	sediment	k1gInPc2	sediment
napovídající	napovídající	k2eAgFnSc1d1	napovídající
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
krátkému	krátký	k2eAgInSc3d1	krátký
výlevu	výlev	k1gInSc3	výlev
tekuté	tekutý	k2eAgFnSc2d1	tekutá
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
jejímu	její	k3xOp3gNnSc3	její
stékání	stékání	k1gNnSc3	stékání
po	po	k7c6	po
stěně	stěna	k1gFnSc6	stěna
kráteru	kráter	k1gInSc2	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Převratný	převratný	k2eAgInSc1d1	převratný
objev	objev	k1gInSc1	objev
oživil	oživit	k5eAaPmAgInS	oživit
spekulace	spekulace	k1gFnPc4	spekulace
o	o	k7c6	o
přeživším	přeživší	k2eAgInSc6d1	přeživší
mimozemském	mimozemský	k2eAgInSc6d1	mimozemský
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pozdějším	pozdní	k2eAgNnSc6d2	pozdější
zkoumání	zkoumání	k1gNnSc6	zkoumání
této	tento	k3xDgFnSc2	tento
nové	nový	k2eAgFnSc2d1	nová
vrstvy	vrstva	k1gFnSc2	vrstva
spektrometrem	spektrometr	k1gInSc7	spektrometr
CRISM	CRISM	kA	CRISM
sondy	sonda	k1gFnPc4	sonda
Mars	Mars	k1gInSc4	Mars
Reconnaissance	Reconnaissanec	k1gInSc2	Reconnaissanec
Orbiter	Orbitrum	k1gNnPc2	Orbitrum
však	však	k9	však
nebyly	být	k5eNaImAgFnP	být
nalezeny	nalézt	k5eAaBmNgFnP	nalézt
stopy	stopa	k1gFnPc1	stopa
ledu	led	k1gInSc2	led
ani	ani	k8xC	ani
minerálů	minerál	k1gInPc2	minerál
obsahujících	obsahující	k2eAgFnPc2d1	obsahující
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Vědci	vědec	k1gMnPc1	vědec
v	v	k7c6	v
NASA	NASA	kA	NASA
tedy	tedy	k8xC	tedy
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nová	nový	k2eAgFnSc1d1	nová
vrstva	vrstva	k1gFnSc1	vrstva
sedimentů	sediment	k1gInPc2	sediment
odlišného	odlišný	k2eAgNnSc2d1	odlišné
zbarvení	zbarvení	k1gNnSc2	zbarvení
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
spíše	spíše	k9	spíše
než	než	k8xS	než
výlevem	výlev	k1gInSc7	výlev
tekuté	tekutý	k2eAgFnSc2d1	tekutá
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
sesutím	sesutí	k1gNnSc7	sesutí
suché	suchý	k2eAgFnSc2d1	suchá
horniny	hornina	k1gFnSc2	hornina
jiného	jiný	k2eAgNnSc2d1	jiné
stáří	stáří	k1gNnSc2	stáří
po	po	k7c6	po
příkrém	příkrý	k2eAgInSc6d1	příkrý
svahu	svah	k1gInSc6	svah
kráteru	kráter	k1gInSc2	kráter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
existenci	existence	k1gFnSc4	existence
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
splněny	splnit	k5eAaPmNgFnP	splnit
některé	některý	k3yIgFnPc1	některý
podmínky	podmínka	k1gFnPc1	podmínka
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
teploty	teplota	k1gFnSc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Atmosférický	atmosférický	k2eAgInSc1d1	atmosférický
tlak	tlak	k1gInSc1	tlak
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
610	[number]	k4	610
Pa	Pa	kA	Pa
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
nad	nad	k7c7	nad
bodem	bod	k1gInSc7	bod
mrazu	mráz	k1gInSc2	mráz
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
nad	nad	k7c7	nad
0,01	[number]	k4	0,01
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
dosahovat	dosahovat	k5eAaImF	dosahovat
hodnot	hodnota	k1gFnPc2	hodnota
tzv.	tzv.	kA	tzv.
trojného	trojný	k2eAgInSc2d1	trojný
bodu	bod	k1gInSc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Marsu	Mars	k1gInSc2	Mars
se	se	k3xPyFc4	se
hodnoty	hodnota	k1gFnSc2	hodnota
tlaku	tlak	k1gInSc2	tlak
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
právě	právě	k9	právě
okolo	okolo	k7c2	okolo
hodnoty	hodnota	k1gFnSc2	hodnota
610	[number]	k4	610
Pa	Pa	kA	Pa
či	či	k8xC	či
pod	pod	k7c7	pod
touto	tento	k3xDgFnSc7	tento
hranicí	hranice	k1gFnSc7	hranice
a	a	k8xC	a
teploty	teplota	k1gFnPc1	teplota
většinou	většinou	k6eAd1	většinou
hluboko	hluboko	k6eAd1	hluboko
pod	pod	k7c7	pod
bodem	bod	k1gInSc7	bod
mrazu	mráz	k1gInSc2	mráz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nepatrná	patrný	k2eNgFnSc1d1	patrný
část	část	k1gFnSc1	část
vody	voda	k1gFnSc2	voda
připadá	připadat	k5eAaPmIp3nS	připadat
i	i	k9	i
na	na	k7c4	na
atmosférickou	atmosférický	k2eAgFnSc4d1	atmosférická
vodní	vodní	k2eAgFnSc4d1	vodní
páru	pára	k1gFnSc4	pára
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemuselo	muset	k5eNaImAgNnS	muset
tomu	ten	k3xDgNnSc3	ten
být	být	k5eAaImF	být
vždy	vždy	k6eAd1	vždy
tak	tak	k6eAd1	tak
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
teorie	teorie	k1gFnPc1	teorie
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
vody	voda	k1gFnSc2	voda
zmizela	zmizet	k5eAaPmAgFnS	zmizet
z	z	k7c2	z
Marsu	Mars	k1gInSc2	Mars
do	do	k7c2	do
okolního	okolní	k2eAgInSc2d1	okolní
kosmického	kosmický	k2eAgInSc2d1	kosmický
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
Mars	Mars	k1gInSc1	Mars
má	mít	k5eAaImIp3nS	mít
mnohem	mnohem	k6eAd1	mnohem
slabší	slabý	k2eAgNnSc4d2	slabší
gravitační	gravitační	k2eAgNnSc4d1	gravitační
pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
částice	částice	k1gFnPc1	částice
snadněji	snadno	k6eAd2	snadno
unikají	unikat	k5eAaImIp3nP	unikat
do	do	k7c2	do
okolního	okolní	k2eAgNnSc2d1	okolní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Atmosféra	atmosféra	k1gFnSc1	atmosféra
===	===	k?	===
</s>
</p>
<p>
<s>
Malé	Malé	k2eAgNnSc4d1	Malé
procento	procento	k1gNnSc4	procento
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
0,03	[number]	k4	0,03
%	%	kIx~	%
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
Marsu	Mars	k1gInSc2	Mars
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
1	[number]	k4	1
mg	mg	kA	mg
vody	voda	k1gFnSc2	voda
na	na	k7c4	na
1	[number]	k4	1
m	m	kA	m
<g/>
3	[number]	k4	3
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
modelů	model	k1gInPc2	model
její	její	k3xOp3gInSc4	její
podíl	podíl	k1gInSc4	podíl
asi	asi	k9	asi
větší	veliký	k2eAgInSc4d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
3,5	[number]	k4	3,5
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
měl	mít	k5eAaImAgMnS	mít
Mars	Mars	k1gInSc4	Mars
teplejší	teplý	k2eAgInSc4d2	teplejší
a	a	k8xC	a
vlhčí	vlhčit	k5eAaImIp3nP	vlhčit
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
existovala	existovat	k5eAaImAgFnS	existovat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
asi	asi	k9	asi
104	[number]	k4	104
až	až	k9	až
105	[number]	k4	105
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Oblačnost	oblačnost	k1gFnSc1	oblačnost
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
většinou	většina	k1gFnSc7	většina
krystalky	krystalka	k1gFnSc2	krystalka
suchého	suchý	k2eAgInSc2d1	suchý
ledu	led	k1gInSc2	led
(	(	kIx(	(
<g/>
zmrzlý	zmrzlý	k2eAgInSc1d1	zmrzlý
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Mars	Mars	k1gInSc1	Mars
Global	globat	k5eAaImAgInS	globat
Sureyor	Sureyor	k1gInSc4	Sureyor
definitivně	definitivně	k6eAd1	definitivně
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
krystalky	krystalka	k1gFnPc1	krystalka
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
i	i	k9	i
vodou	voda	k1gFnSc7	voda
v	v	k7c6	v
pevném	pevný	k2eAgNnSc6d1	pevné
skupenství	skupenství	k1gNnSc6	skupenství
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc4	první
pozorování	pozorování	k1gNnPc4	pozorování
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
již	již	k6eAd1	již
sonda	sonda	k1gFnSc1	sonda
Mariner	Mariner	k1gInSc1	Mariner
9	[number]	k4	9
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gInPc1	její
výsledky	výsledek	k1gInPc1	výsledek
se	se	k3xPyFc4	se
daly	dát	k5eAaPmAgInP	dát
interpretovat	interpretovat	k5eAaBmF	interpretovat
více	hodně	k6eAd2	hodně
způsoby	způsob	k1gInPc4	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
ledových	ledový	k2eAgInPc2d1	ledový
krystalků	krystalek	k1gInPc2	krystalek
je	být	k5eAaImIp3nS	být
spojen	spojit	k5eAaPmNgInS	spojit
převážně	převážně	k6eAd1	převážně
se	s	k7c7	s
severní	severní	k2eAgFnSc7d1	severní
polární	polární	k2eAgFnSc7d1	polární
čepičkou	čepička	k1gFnSc7	čepička
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
tvořena	tvořit	k5eAaImNgFnS	tvořit
vodním	vodní	k2eAgInSc7d1	vodní
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
marsovského	marsovský	k2eAgNnSc2d1	marsovské
jara	jaro	k1gNnSc2	jaro
a	a	k8xC	a
léta	léto	k1gNnSc2	léto
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
evaporaci	evaporace	k1gFnSc3	evaporace
nad	nad	k7c7	nad
oblastí	oblast	k1gFnSc7	oblast
čepičky	čepička	k1gFnSc2	čepička
<g/>
,	,	kIx,	,
vzniku	vznik	k1gInSc3	vznik
oblačnosti	oblačnost	k1gFnSc2	oblačnost
a	a	k8xC	a
jejímu	její	k3xOp3gInSc3	její
přesunu	přesun	k1gInSc3	přesun
do	do	k7c2	do
rovníkových	rovníkový	k2eAgFnPc2d1	Rovníková
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mraky	mrak	k1gInPc1	mrak
zmrznou	zmrznout	k5eAaPmIp3nP	zmrznout
a	a	k8xC	a
dopadnou	dopadnout	k5eAaPmIp3nP	dopadnout
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
ledových	ledový	k2eAgInPc2d1	ledový
krystalků	krystalek	k1gInPc2	krystalek
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
jinovatky	jinovatka	k1gFnSc2	jinovatka
tvořené	tvořený	k2eAgNnSc1d1	tvořené
zmrzlou	zmrzlý	k2eAgFnSc7d1	zmrzlá
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
ledových	ledový	k2eAgInPc2d1	ledový
mraků	mrak	k1gInPc2	mrak
je	být	k5eAaImIp3nS	být
sezónní	sezónní	k2eAgMnSc1d1	sezónní
<g/>
,	,	kIx,	,
největší	veliký	k2eAgMnSc1d3	veliký
bývá	bývat	k5eAaImIp3nS	bývat
mezi	mezi	k7c4	mezi
Ls	Ls	k1gFnSc4	Ls
=	=	kIx~	=
40	[number]	k4	40
až	až	k9	až
150	[number]	k4	150
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Polární	polární	k2eAgFnPc1d1	polární
čepičky	čepička	k1gFnPc1	čepička
===	===	k?	===
</s>
</p>
<p>
<s>
Vedle	vedle	k7c2	vedle
těchto	tento	k3xDgInPc2	tento
vodních	vodní	k2eAgInPc2d1	vodní
zdrojů	zdroj	k1gInPc2	zdroj
se	se	k3xPyFc4	se
na	na	k7c6	na
pólech	pól	k1gInPc6	pól
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dvě	dva	k4xCgFnPc1	dva
polární	polární	k2eAgFnPc1d1	polární
čepičky	čepička	k1gFnPc1	čepička
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
částečně	částečně	k6eAd1	částečně
tvořeny	tvořit	k5eAaImNgInP	tvořit
vodním	vodní	k2eAgInSc7d1	vodní
ledem	led	k1gInSc7	led
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
suchým	suchý	k2eAgInSc7d1	suchý
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
<s>
Polární	polární	k2eAgFnPc1d1	polární
čepičky	čepička	k1gFnPc1	čepička
jsou	být	k5eAaImIp3nP	být
vzájemně	vzájemně	k6eAd1	vzájemně
rozdílné	rozdílný	k2eAgFnPc1d1	rozdílná
v	v	k7c6	v
chemickém	chemický	k2eAgNnSc6d1	chemické
složení	složení	k1gNnSc6	složení
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
čepička	čepička	k1gFnSc1	čepička
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
převážně	převážně	k6eAd1	převážně
ze	z	k7c2	z
suchého	suchý	k2eAgInSc2d1	suchý
ledu	led	k1gInSc2	led
(	(	kIx(	(
<g/>
zmrzlý	zmrzlý	k2eAgInSc1d1	zmrzlý
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
<g/>
)	)	kIx)	)
a	a	k8xC	a
malé	malý	k2eAgFnPc1d1	malá
části	část	k1gFnPc1	část
vody	voda	k1gFnSc2	voda
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
výzkum	výzkum	k1gInSc4	výzkum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
možná	možná	k9	možná
až	až	k9	až
1,6	[number]	k4	1,6
miliónu	milión	k4xCgInSc2	milión
km	km	kA	km
<g/>
3	[number]	k4	3
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
větší	veliký	k2eAgNnSc1d2	veliký
množství	množství	k1gNnSc1	množství
<g/>
,	,	kIx,	,
než	než	k8xS	než
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
severní	severní	k2eAgFnSc1d1	severní
čepička	čepička	k1gFnSc1	čepička
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
severní	severní	k2eAgFnSc1d1	severní
polární	polární	k2eAgFnSc1d1	polární
čepička	čepička	k1gFnSc1	čepička
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
sublimuje	sublimovat	k5eAaBmIp3nS	sublimovat
a	a	k8xC	a
zásobuje	zásobovat	k5eAaImIp3nS	zásobovat
vodní	vodní	k2eAgInPc4d1	vodní
mraky	mrak	k1gInPc4	mrak
<g/>
.	.	kIx.	.
</s>
<s>
Objevení	objevení	k1gNnSc1	objevení
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
polární	polární	k2eAgFnSc6d1	polární
čepičce	čepička	k1gFnSc6	čepička
je	být	k5eAaImIp3nS	být
objev	objev	k1gInSc1	objev
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
předpovědí	předpověď	k1gFnSc7	předpověď
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
po	po	k7c6	po
zjištění	zjištění	k1gNnSc6	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
atmosféry	atmosféra	k1gFnSc2	atmosféra
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgInSc1d1	tehdejší
model	model	k1gInSc1	model
počítal	počítat	k5eAaImAgInS	počítat
s	s	k7c7	s
čepičkou	čepička	k1gFnSc7	čepička
složenou	složený	k2eAgFnSc7d1	složená
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
jižní	jižní	k2eAgFnSc1d1	jižní
čepička	čepička	k1gFnSc1	čepička
převážně	převážně	k6eAd1	převážně
ze	z	k7c2	z
suchého	suchý	k2eAgInSc2d1	suchý
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Sonda	sonda	k1gFnSc1	sonda
Viking	Viking	k1gMnSc1	Viking
přinesla	přinést	k5eAaPmAgFnS	přinést
sice	sice	k8xC	sice
nové	nový	k2eAgInPc4d1	nový
poznatky	poznatek	k1gInPc4	poznatek
o	o	k7c6	o
vodním	vodní	k2eAgInSc6d1	vodní
ledu	led	k1gInSc6	led
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vědci	vědec	k1gMnPc1	vědec
stále	stále	k6eAd1	stále
věřili	věřit	k5eAaImAgMnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
minoritní	minoritní	k2eAgFnSc7d1	minoritní
složkou	složka	k1gFnSc7	složka
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
revizi	revize	k1gFnSc3	revize
tohoto	tento	k3xDgInSc2	tento
názoru	názor	k1gInSc2	názor
na	na	k7c6	na
základě	základ	k1gInSc6	základ
termálních	termální	k2eAgInPc2d1	termální
snímků	snímek	k1gInPc2	snímek
sondy	sonda	k1gFnSc2	sonda
Mars	Mars	k1gMnSc1	Mars
Global	globat	k5eAaImAgMnS	globat
Surveyor	Surveyor	k1gInSc4	Surveyor
a	a	k8xC	a
Mars	Mars	k1gInSc4	Mars
Odyssey	Odyssea	k1gFnSc2	Odyssea
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
snímků	snímek	k1gInPc2	snímek
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
polární	polární	k2eAgFnSc1d1	polární
oblast	oblast	k1gFnSc1	oblast
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
na	na	k7c4	na
vyšší	vysoký	k2eAgFnSc4d2	vyšší
teplotu	teplota	k1gFnSc4	teplota
<g/>
,	,	kIx,	,
než	než	k8xS	než
při	při	k7c6	při
jaké	jaký	k3yRgFnSc6	jaký
může	moct	k5eAaImIp3nS	moct
suchý	suchý	k2eAgInSc4d1	suchý
led	led	k1gInSc4	led
existovat	existovat	k5eAaImF	existovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jeho	jeho	k3xOp3gFnSc3	jeho
výskyt	výskyt	k1gInSc1	výskyt
vyloučil	vyloučit	k5eAaPmAgInS	vyloučit
<g/>
.	.	kIx.	.
<g/>
Severní	severní	k2eAgFnSc1d1	severní
polární	polární	k2eAgFnSc1d1	polární
čepička	čepička	k1gFnSc1	čepička
zabírá	zabírat	k5eAaImIp3nS	zabírat
plochu	plocha	k1gFnSc4	plocha
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
200	[number]	k4	200
km	km	kA	km
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
tloušťkou	tloušťka	k1gFnSc7	tloušťka
ledu	led	k1gInSc2	led
1,03	[number]	k4	1,03
km	km	kA	km
(	(	kIx(	(
<g/>
maximální	maximální	k2eAgFnSc4d1	maximální
3	[number]	k4	3
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c6	na
území	území	k1gNnSc6	území
velkém	velký	k2eAgNnSc6d1	velké
jako	jako	k9	jako
1,5	[number]	k4	1,5
Texasu	Texas	k1gInSc2	Texas
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
po	po	k7c6	po
přepočtu	přepočet	k1gInSc6	přepočet
přibližně	přibližně	k6eAd1	přibližně
13,2	[number]	k4	13,2
násobku	násobek	k1gInSc2	násobek
rozlohy	rozloha	k1gFnSc2	rozloha
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
zbrázděn	zbrázdit	k5eAaPmNgInS	zbrázdit
kaňony	kaňon	k1gInPc7	kaňon
a	a	k8xC	a
trhlinami	trhlina	k1gFnPc7	trhlina
<g/>
.	.	kIx.	.
</s>
<s>
Odhadované	odhadovaný	k2eAgNnSc1d1	odhadované
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
by	by	kYmCp3nS	by
po	po	k7c6	po
roztátí	roztátý	k2eAgMnPc1d1	roztátý
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
1,2	[number]	k4	1,2
miliónů	milión	k4xCgInPc2	milión
km	km	kA	km
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
polovině	polovina	k1gFnSc6	polovina
všeho	všecek	k3xTgInSc2	všecek
ledu	led	k1gInSc2	led
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
či	či	k8xC	či
4	[number]	k4	4
%	%	kIx~	%
ledové	ledový	k2eAgFnSc2d1	ledová
pokrývky	pokrývka	k1gFnSc2	pokrývka
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tyto	tento	k3xDgFnPc4	tento
obrovské	obrovský	k2eAgFnPc4d1	obrovská
zásoby	zásoba	k1gFnPc4	zásoba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemůže	moct	k5eNaImIp3nS	moct
tvořit	tvořit	k5eAaImF	tvořit
veškeré	veškerý	k3xTgNnSc4	veškerý
množství	množství	k1gNnSc4	množství
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
dříve	dříve	k6eAd2	dříve
tvořilo	tvořit	k5eAaImAgNnS	tvořit
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
oceán	oceán	k1gInSc1	oceán
<g/>
.	.	kIx.	.
<g/>
Poblíž	poblíž	k7c2	poblíž
severního	severní	k2eAgInSc2d1	severní
pólu	pól	k1gInSc2	pól
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Vastitas	Vastitasa	k1gFnPc2	Vastitasa
Borealis	Borealis	k1gFnPc2	Borealis
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
objeven	objevit	k5eAaPmNgInS	objevit
kráter	kráter	k1gInSc1	kráter
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
části	část	k1gFnSc2	část
vyplněn	vyplnit	k5eAaPmNgInS	vyplnit
vodním	vodní	k2eAgInSc7d1	vodní
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
<s>
Kráter	kráter	k1gInSc1	kráter
je	být	k5eAaImIp3nS	být
35	[number]	k4	35
kilometrů	kilometr	k1gInPc2	kilometr
široký	široký	k2eAgInSc4d1	široký
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
hloubkou	hloubka	k1gFnSc7	hloubka
2	[number]	k4	2
kilometry	kilometr	k1gInPc7	kilometr
od	od	k7c2	od
báze	báze	k1gFnSc2	báze
po	po	k7c4	po
okraje	okraj	k1gInPc4	okraj
kráteru	kráter	k1gInSc2	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
suchý	suchý	k2eAgInSc4d1	suchý
led	led	k1gInSc4	led
byl	být	k5eAaImAgInS	být
vyvrácen	vyvrácen	k2eAgMnSc1d1	vyvrácen
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
fotografie	fotografia	k1gFnSc2	fotografia
již	již	k9	již
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
vlivem	vlivem	k7c2	vlivem
marsovského	marsovský	k2eAgNnSc2d1	marsovské
léta	léto	k1gNnSc2	léto
sublimoval	sublimovat	k5eAaBmAgMnS	sublimovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Permafrost	Permafrost	k1gFnSc4	Permafrost
===	===	k?	===
</s>
</p>
<p>
<s>
Vyjma	vyjma	k7c2	vyjma
koncentrace	koncentrace	k1gFnSc2	koncentrace
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
polárních	polární	k2eAgFnPc6d1	polární
čepičkách	čepička	k1gFnPc6	čepička
se	se	k3xPyFc4	se
voda	voda	k1gFnSc1	voda
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
věčně	věčně	k6eAd1	věčně
zmrzlé	zmrzlý	k2eAgFnSc2d1	zmrzlá
půdy	půda	k1gFnSc2	půda
tzv.	tzv.	kA	tzv.
permafrostu	permafrost	k1gInSc2	permafrost
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
ve	v	k7c6	v
výrazném	výrazný	k2eAgNnSc6d1	výrazné
zastoupení	zastoupení	k1gNnSc6	zastoupení
do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
kolem	kolem	k7c2	kolem
60	[number]	k4	60
<g/>
°	°	k?	°
rovnoběžek	rovnoběžka	k1gFnPc2	rovnoběžka
<g/>
,	,	kIx,	,
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
v	v	k7c6	v
menším	malý	k2eAgNnSc6d2	menší
množství	množství	k1gNnSc6	množství
rozkládat	rozkládat	k5eAaImF	rozkládat
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
skládat	skládat	k5eAaImF	skládat
až	až	k9	až
z	z	k7c2	z
50	[number]	k4	50
%	%	kIx~	%
z	z	k7c2	z
vodního	vodní	k2eAgInSc2d1	vodní
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgInPc1	první
poznatky	poznatek	k1gInPc1	poznatek
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
existenci	existence	k1gFnSc4	existence
přinesly	přinést	k5eAaPmAgFnP	přinést
fotografie	fotografia	k1gFnPc1	fotografia
pořízené	pořízený	k2eAgFnPc1d1	pořízená
sondou	sonda	k1gFnSc7	sonda
Viking	Viking	k1gMnSc1	Viking
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
základě	základ	k1gInSc6	základ
rozpoznali	rozpoznat	k5eAaPmAgMnP	rozpoznat
vědci	vědec	k1gMnPc1	vědec
objekty	objekt	k1gInPc4	objekt
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
připomínaly	připomínat	k5eAaImAgFnP	připomínat
pozemské	pozemský	k2eAgFnPc1d1	pozemská
oblasti	oblast	k1gFnPc1	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
permafrost	permafrost	k1gFnSc1	permafrost
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
převážně	převážně	k6eAd1	převážně
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
nejspíše	nejspíše	k9	nejspíše
vyskytoval	vyskytovat	k5eAaImAgInS	vyskytovat
severní	severní	k2eAgInSc1d1	severní
ledový	ledový	k2eAgInSc1d1	ledový
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
oblast	oblast	k1gFnSc1	oblast
Hellas	Hellas	k1gFnSc1	Hellas
Planitia	Planitia	k1gFnSc1	Planitia
<g/>
,	,	kIx,	,
Argyre	Argyr	k1gMnSc5	Argyr
<g/>
,	,	kIx,	,
dna	dno	k1gNnPc1	dno
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
koryt	koryto	k1gNnPc2	koryto
(	(	kIx(	(
<g/>
obzvláště	obzvláště	k6eAd1	obzvláště
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Ares	Ares	k1gMnSc1	Ares
Valley	Vallea	k1gFnSc2	Vallea
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přistál	přistát	k5eAaPmAgInS	přistát
povrchový	povrchový	k2eAgInSc1d1	povrchový
modul	modul	k1gInSc1	modul
Sojourner	Sojournra	k1gFnPc2	Sojournra
sondy	sonda	k1gFnSc2	sonda
Mars	Mars	k1gMnSc1	Mars
Pathfinder	Pathfinder	k1gMnSc1	Pathfinder
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
oblasti	oblast	k1gFnPc4	oblast
s	s	k7c7	s
permafrostem	permafrost	k1gFnPc3	permafrost
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
oteplení	oteplení	k1gNnSc3	oteplení
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
nastal	nastat	k5eAaPmAgInS	nastat
by	by	kYmCp3nS	by
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
kolaps	kolaps	k1gInSc1	kolaps
a	a	k8xC	a
sesuvy	sesuv	k1gInPc1	sesuv
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
modely	model	k1gInPc1	model
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
permafrost	permafrost	k1gFnSc4	permafrost
mohl	moct	k5eAaImAgMnS	moct
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
pólů	pól	k1gInPc2	pól
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
až	až	k9	až
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
1	[number]	k4	1
km	km	kA	km
a	a	k8xC	a
v	v	k7c6	v
rovníkových	rovníkový	k2eAgFnPc6d1	Rovníková
oblastech	oblast	k1gFnPc6	oblast
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Průzkum	průzkum	k1gInSc1	průzkum
těchto	tento	k3xDgFnPc2	tento
podzemních	podzemní	k2eAgFnPc2d1	podzemní
zásob	zásoba	k1gFnPc2	zásoba
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
hloubky	hloubka	k1gFnSc2	hloubka
je	být	k5eAaImIp3nS	být
extrémně	extrémně	k6eAd1	extrémně
složitý	složitý	k2eAgInSc1d1	složitý
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jeho	jeho	k3xOp3gInPc4	jeho
odhady	odhad	k1gInPc4	odhad
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
změny	změna	k1gFnPc4	změna
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
nepřesná	přesný	k2eNgFnSc1d1	nepřesná
a	a	k8xC	a
přináší	přinášet	k5eAaImIp3nS	přinášet
jen	jen	k9	jen
hrubé	hrubý	k2eAgFnPc4d1	hrubá
představy	představa	k1gFnPc4	představa
o	o	k7c6	o
skutečném	skutečný	k2eAgInSc6d1	skutečný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Voda	voda	k1gFnSc1	voda
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
===	===	k?	===
</s>
</p>
<p>
<s>
Jádro	jádro	k1gNnSc1	jádro
Marsu	Mars	k1gInSc2	Mars
je	být	k5eAaImIp3nS	být
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
to	ten	k3xDgNnSc4	ten
zemské	zemský	k2eAgNnSc4d1	zemské
polotekuté	polotekutý	k2eAgNnSc4d1	polotekuté
a	a	k8xC	a
generující	generující	k2eAgNnSc4d1	generující
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
vlivem	vliv	k1gInSc7	vliv
tepelných	tepelný	k2eAgInPc2d1	tepelný
proudů	proud	k1gInPc2	proud
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
svrchnějších	svrchní	k2eAgFnPc2d2	svrchní
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Plášť	plášť	k1gInSc1	plášť
a	a	k8xC	a
kůra	kůra	k1gFnSc1	kůra
část	část	k1gFnSc1	část
tohoto	tento	k3xDgNnSc2	tento
tepla	teplo	k1gNnSc2	teplo
získávají	získávat	k5eAaImIp3nP	získávat
a	a	k8xC	a
teoreticky	teoreticky	k6eAd1	teoreticky
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
v	v	k7c6	v
hloubkách	hloubka	k1gFnPc6	hloubka
5	[number]	k4	5
km	km	kA	km
(	(	kIx(	(
<g/>
na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
<g/>
)	)	kIx)	)
až	až	k9	až
10	[number]	k4	10
km	km	kA	km
(	(	kIx(	(
<g/>
na	na	k7c6	na
pólech	pólo	k1gNnPc6	pólo
<g/>
)	)	kIx)	)
existenci	existence	k1gFnSc4	existence
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgNnPc6	který
panují	panovat	k5eAaImIp3nP	panovat
dostatečné	dostatečný	k2eAgFnPc4d1	dostatečná
teploty	teplota	k1gFnPc4	teplota
pro	pro	k7c4	pro
existenci	existence	k1gFnSc4	existence
tekuté	tekutý	k2eAgFnSc2d1	tekutá
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
tato	tento	k3xDgFnSc1	tento
vrstva	vrstva	k1gFnSc1	vrstva
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
50	[number]	k4	50
až	až	k9	až
500	[number]	k4	500
metrů	metr	k1gInPc2	metr
silná	silný	k2eAgFnSc1d1	silná
a	a	k8xC	a
obepínat	obepínat	k5eAaImF	obepínat
celou	celý	k2eAgFnSc4d1	celá
planetu	planeta	k1gFnSc4	planeta
<g/>
.	.	kIx.	.
<g/>
Analogicky	analogicky	k6eAd1	analogicky
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
nacházejí	nacházet	k5eAaImIp3nP	nacházet
místa	místo	k1gNnPc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
geotermální	geotermální	k2eAgFnSc1d1	geotermální
energie	energie	k1gFnSc1	energie
dostává	dostávat	k5eAaImIp3nS	dostávat
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
voda	voda	k1gFnSc1	voda
nachází	nacházet	k5eAaImIp3nS	nacházet
méně	málo	k6eAd2	málo
než	než	k8xS	než
500	[number]	k4	500
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
snímky	snímek	k1gInPc1	snímek
pak	pak	k6eAd1	pak
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výlevu	výlev	k1gInSc3	výlev
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vody	voda	k1gFnSc2	voda
na	na	k7c4	na
marsovský	marsovský	k2eAgInSc4d1	marsovský
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Znamenalo	znamenat	k5eAaImAgNnS	znamenat
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tekutá	tekutý	k2eAgFnSc1d1	tekutá
voda	voda	k1gFnSc1	voda
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
blíže	blízce	k6eAd2	blízce
povrchu	povrch	k1gInSc2	povrch
než	než	k8xS	než
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
soudí	soudit	k5eAaImIp3nS	soudit
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
byl	být	k5eAaImAgInS	být
snímek	snímek	k1gInSc1	snímek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
ukazující	ukazující	k2eAgFnSc1d1	ukazující
oblast	oblast	k1gFnSc1	oblast
Noachis	Noachis	k1gFnSc1	Noachis
Terra	Terra	k1gFnSc1	Terra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
kráter	kráter	k1gInSc1	kráter
s	s	k7c7	s
patrnými	patrný	k2eAgFnPc7d1	patrná
stopami	stopa	k1gFnPc7	stopa
výronu	výron	k1gInSc2	výron
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
jevy	jev	k1gInPc1	jev
vyvracejí	vyvracet	k5eAaImIp3nP	vyvracet
pochyby	pochyba	k1gFnPc4	pochyba
o	o	k7c6	o
přítomnosti	přítomnost	k1gFnSc6	přítomnost
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
kapalném	kapalný	k2eAgNnSc6d1	kapalné
skupenství	skupenství	k1gNnSc6	skupenství
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Teploty	teplota	k1gFnPc1	teplota
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
z	z	k7c2	z
hloubky	hloubka	k1gFnSc2	hloubka
okolo	okolo	k7c2	okolo
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
trvale	trvale	k6eAd1	trvale
zamrzlá	zamrzlý	k2eAgFnSc1d1	zamrzlá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Budoucnost	budoucnost	k1gFnSc1	budoucnost
==	==	k?	==
</s>
</p>
<p>
<s>
Objevení	objevení	k1gNnSc1	objevení
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
významným	významný	k2eAgInSc7d1	významný
faktorem	faktor	k1gInSc7	faktor
i	i	k9	i
při	při	k7c6	při
plánování	plánování	k1gNnSc6	plánování
budoucích	budoucí	k2eAgFnPc2d1	budoucí
kosmických	kosmický	k2eAgFnPc2d1	kosmická
misí	mise	k1gFnPc2	mise
k	k	k7c3	k
Marsu	Mars	k1gInSc3	Mars
s	s	k7c7	s
lidskou	lidský	k2eAgFnSc7d1	lidská
posádkou	posádka	k1gFnSc7	posádka
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
znamenalo	znamenat	k5eAaImAgNnS	znamenat
snížení	snížení	k1gNnSc4	snížení
zásob	zásoba	k1gFnPc2	zásoba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
budou	být	k5eAaImBp3nP	být
nuceni	nucen	k2eAgMnPc1d1	nucen
astronauti	astronaut	k1gMnPc1	astronaut
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přepravovat	přepravovat	k5eAaImF	přepravovat
<g/>
.	.	kIx.	.
</s>
<s>
Naskytuje	naskytovat	k5eAaImIp3nS	naskytovat
se	se	k3xPyFc4	se
reálná	reálný	k2eAgFnSc1d1	reálná
možnost	možnost	k1gFnSc1	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
potřebná	potřebný	k2eAgFnSc1d1	potřebná
voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
těžit	těžit	k5eAaImF	těžit
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
Marsu	Mars	k1gInSc2	Mars
či	či	k8xC	či
z	z	k7c2	z
polárních	polární	k2eAgFnPc2d1	polární
čepiček	čepička	k1gFnPc2	čepička
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
značně	značně	k6eAd1	značně
ušetří	ušetřit	k5eAaPmIp3nS	ušetřit
nákladový	nákladový	k2eAgInSc4d1	nákladový
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
moci	moct	k5eAaImF	moct
získávat	získávat	k5eAaImF	získávat
několika	několik	k4yIc7	několik
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
z	z	k7c2	z
regolitu	regolit	k1gInSc2	regolit
<g/>
,	,	kIx,	,
z	z	k7c2	z
permafrostu	permafrost	k1gInSc2	permafrost
<g/>
,	,	kIx,	,
z	z	k7c2	z
aquiferů	aquifer	k1gInPc2	aquifer
nebo	nebo	k8xC	nebo
z	z	k7c2	z
polárních	polární	k2eAgFnPc2d1	polární
čepiček	čepička	k1gFnPc2	čepička
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
získání	získání	k1gNnSc4	získání
bude	být	k5eAaImBp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
dodávat	dodávat	k5eAaImF	dodávat
teplo	teplo	k1gNnSc4	teplo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozpuštění	rozpuštění	k1gNnSc3	rozpuštění
ledu	led	k1gInSc2	led
na	na	k7c4	na
kapalnou	kapalný	k2eAgFnSc4d1	kapalná
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
smělé	smělý	k2eAgInPc1d1	smělý
plány	plán	k1gInPc1	plán
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
na	na	k7c4	na
přeměnu	přeměna	k1gFnSc4	přeměna
Marsu	Mars	k1gInSc2	Mars
na	na	k7c4	na
člověkem	člověk	k1gMnSc7	člověk
obyvatelnou	obyvatelný	k2eAgFnSc4d1	obyvatelná
planetu	planeta	k1gFnSc4	planeta
–	–	k?	–
terraformace	terraformace	k1gFnSc1	terraformace
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
předpokladů	předpoklad	k1gInPc2	předpoklad
je	být	k5eAaImIp3nS	být
ohřátí	ohřátý	k2eAgMnPc1d1	ohřátý
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
vytvoření	vytvoření	k1gNnSc6	vytvoření
hustší	hustý	k2eAgFnSc2d2	hustší
atmosféry	atmosféra	k1gFnSc2	atmosféra
umožňující	umožňující	k2eAgFnSc4d1	umožňující
existenci	existence	k1gFnSc4	existence
kapalné	kapalný	k2eAgFnSc2d1	kapalná
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc6	vytvoření
biosféry	biosféra	k1gFnSc2	biosféra
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
současnými	současný	k2eAgFnPc7d1	současná
technologiemi	technologie	k1gFnPc7	technologie
nejspíše	nejspíše	k9	nejspíše
neproveditelný	proveditelný	k2eNgMnSc1d1	neproveditelný
či	či	k8xC	či
proveditelný	proveditelný	k2eAgMnSc1d1	proveditelný
za	za	k7c2	za
vynaložení	vynaložení	k1gNnSc2	vynaložení
extrémních	extrémní	k2eAgInPc2d1	extrémní
nákladů	náklad	k1gInPc2	náklad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
dnes	dnes	k6eAd1	dnes
vznikají	vznikat	k5eAaImIp3nP	vznikat
návrhy	návrh	k1gInPc4	návrh
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tento	tento	k3xDgInSc4	tento
úkol	úkol	k1gInSc4	úkol
provést	provést	k5eAaPmF	provést
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c4	mnoho
těchto	tento	k3xDgInPc2	tento
projektů	projekt	k1gInPc2	projekt
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
vědecko	vědecko	k6eAd1	vědecko
fantastické	fantastický	k2eAgFnSc6d1	fantastická
literatuře	literatura	k1gFnSc6	literatura
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Trilogie	trilogie	k1gFnSc2	trilogie
o	o	k7c6	o
Marsu	Mars	k1gInSc6	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nápady	nápad	k1gInPc4	nápad
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
rozpustit	rozpustit	k5eAaPmF	rozpustit
vodní	vodní	k2eAgInSc4d1	vodní
led	led	k1gInSc4	led
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
využití	využití	k1gNnSc1	využití
jaderných	jaderný	k2eAgFnPc2d1	jaderná
náloží	nálož	k1gFnPc2	nálož
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
<g/>
,	,	kIx,	,
ohřátí	ohřátý	k2eAgMnPc1d1	ohřátý
planety	planeta	k1gFnSc2	planeta
pomocí	pomocí	k7c2	pomocí
soustavy	soustava	k1gFnSc2	soustava
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
<g/>
,	,	kIx,	,
vypouštěním	vypouštění	k1gNnSc7	vypouštění
speciální	speciální	k2eAgFnSc2d1	speciální
směsi	směs	k1gFnSc2	směs
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
odvážných	odvážný	k2eAgInPc2d1	odvážný
plánů	plán	k1gInPc2	plán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
==	==	k?	==
</s>
</p>
<p>
<s>
Celkové	celkový	k2eAgNnSc1d1	celkové
množství	množství	k1gNnSc1	množství
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
je	být	k5eAaImIp3nS	být
značné	značný	k2eAgNnSc1d1	značné
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vázána	vázán	k2eAgFnSc1d1	vázána
v	v	k7c6	v
různorodých	různorodý	k2eAgInPc6d1	různorodý
zdrojích	zdroj	k1gInPc6	zdroj
popsaných	popsaný	k2eAgInPc2d1	popsaný
výše	vysoce	k6eAd2	vysoce
<g/>
.	.	kIx.	.
</s>
<s>
Teoretické	teoretický	k2eAgInPc4d1	teoretický
odhady	odhad	k1gInPc4	odhad
jejího	její	k3xOp3gNnSc2	její
celkového	celkový	k2eAgNnSc2d1	celkové
množství	množství	k1gNnSc2	množství
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
nových	nový	k2eAgInPc6d1	nový
poznatcích	poznatek	k1gInPc6	poznatek
sond	sonda	k1gFnPc2	sonda
<g/>
.	.	kIx.	.
</s>
<s>
Přesné	přesný	k2eAgNnSc4d1	přesné
vyčíslení	vyčíslení	k1gNnSc4	vyčíslení
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
zatím	zatím	k6eAd1	zatím
složité	složitý	k2eAgNnSc1d1	složité
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
NASA	NASA	kA	NASA
provedla	provést	k5eAaPmAgFnS	provést
odhad	odhad	k1gInSc4	odhad
množství	množství	k1gNnSc2	množství
vody	voda	k1gFnSc2	voda
zachycené	zachycený	k2eAgFnSc2d1	zachycená
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
polární	polární	k2eAgFnSc6d1	polární
čepičce	čepička	k1gFnSc6	čepička
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
počítačového	počítačový	k2eAgInSc2d1	počítačový
modelu	model	k1gInSc2	model
by	by	kYmCp3nS	by
veškerá	veškerý	k3xTgFnSc1	veškerý
voda	voda	k1gFnSc1	voda
uvolněná	uvolněný	k2eAgFnSc1d1	uvolněná
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
čepičky	čepička	k1gFnSc2	čepička
zaplavila	zaplavit	k5eAaPmAgFnS	zaplavit
celý	celý	k2eAgInSc4d1	celý
Mars	Mars	k1gInSc4	Mars
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
okolo	okolo	k7c2	okolo
11	[number]	k4	11
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
připočíst	připočíst	k5eAaPmF	připočíst
ještě	ještě	k9	ještě
množství	množství	k1gNnSc4	množství
vody	voda	k1gFnSc2	voda
vázané	vázaný	k2eAgFnSc2d1	vázaná
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
polární	polární	k2eAgFnSc6d1	polární
čepičce	čepička	k1gFnSc6	čepička
(	(	kIx(	(
<g/>
až	až	k9	až
1,2	[number]	k4	1,2
miliónu	milión	k4xCgInSc2	milión
km	km	kA	km
<g/>
3	[number]	k4	3
vody	voda	k1gFnPc1	voda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
kilogram	kilogram	k1gInSc4	kilogram
marsovského	marsovský	k2eAgInSc2d1	marsovský
regolitu	regolit	k1gInSc2	regolit
připadá	připadat	k5eAaPmIp3nS	připadat
až	až	k9	až
40	[number]	k4	40
gramů	gram	k1gInPc2	gram
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
proveden	provést	k5eAaPmNgInS	provést
odhad	odhad	k1gInSc1	odhad
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
polárních	polární	k2eAgFnPc6d1	polární
oblastech	oblast	k1gFnPc6	oblast
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
množství	množství	k1gNnSc4	množství
vody	voda	k1gFnSc2	voda
obsažené	obsažený	k2eAgFnSc2d1	obsažená
ve	v	k7c6	v
zmrzlé	zmrzlý	k2eAgFnSc6d1	zmrzlá
formě	forma	k1gFnSc6	forma
na	na	k7c4	na
~	~	kIx~	~
<g/>
5	[number]	k4	5
miliónů	milión	k4xCgInPc2	milión
km	km	kA	km
<g/>
3	[number]	k4	3
v	v	k7c6	v
polárních	polární	k2eAgFnPc6d1	polární
<g />
.	.	kIx.	.
</s>
<s>
čepičkách	čepička	k1gFnPc6	čepička
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
než	než	k8xS	než
6	[number]	k4	6
<g/>
×	×	k?	×
<g/>
104	[number]	k4	104
km	km	kA	km
<g/>
3	[number]	k4	3
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
šířkách	šířka	k1gFnPc6	šířka
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sedimentů	sediment	k1gInPc2	sediment
bohatých	bohatý	k2eAgInPc2d1	bohatý
na	na	k7c4	na
led	led	k1gInSc4	led
a	a	k8xC	a
okolo	okolo	k7c2	okolo
~	~	kIx~	~
<g/>
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
2	[number]	k4	2
km	km	kA	km
<g/>
3	[number]	k4	3
účastnící	účastnící	k2eAgFnSc2d1	účastnící
se	se	k3xPyFc4	se
sublimace	sublimace	k1gFnSc2	sublimace
sezónních	sezónní	k2eAgFnPc2d1	sezónní
polárních	polární	k2eAgFnPc2d1	polární
čepiček	čepička	k1gFnPc2	čepička
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
padající	padající	k2eAgInPc1d1	padající
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
sněhu	sníh	k1gInSc2	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Počítačový	počítačový	k2eAgInSc1d1	počítačový
model	model	k1gInSc1	model
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kdyby	kdyby	kYmCp3nS	kdyby
veškerá	veškerý	k3xTgFnSc1	veškerý
tato	tento	k3xDgFnSc1	tento
voda	voda	k1gFnSc1	voda
roztála	roztát	k5eAaPmAgFnS	roztát
<g/>
,	,	kIx,	,
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
okolo	okolo	k7c2	okolo
celé	celý	k2eAgFnSc2d1	celá
planety	planeta	k1gFnSc2	planeta
souvislá	souvislý	k2eAgFnSc1d1	souvislá
vodní	vodní	k2eAgFnSc1d1	vodní
vrstva	vrstva	k1gFnSc1	vrstva
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
35	[number]	k4	35
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Voda	voda	k1gFnSc1	voda
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
YouTube	YouTubat	k5eAaPmIp3nS	YouTubat
<g/>
:	:	kIx,	:
Nalezeno	nalezen	k2eAgNnSc4d1	Nalezeno
tekuté	tekutý	k2eAgFnPc4d1	tekutá
vody	voda	k1gFnPc4	voda
na	na	k7c6	na
Marsu	Mars	k1gInSc6	Mars
</s>
</p>
<p>
<s>
Snímky	snímek	k1gInPc1	snímek
Marsu	Mars	k1gInSc2	Mars
ve	v	k7c6	v
vysokém	vysoký	k2eAgNnSc6d1	vysoké
rozlišení	rozlišení	k1gNnSc6	rozlišení
–	–	k?	–
detailní	detailní	k2eAgNnSc4d1	detailní
pozorování	pozorování	k1gNnSc4	pozorování
projevů	projev	k1gInPc2	projev
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Case	Case	k1gInSc1	Case
of	of	k?	of
the	the	k?	the
Missing	Missing	k1gInSc1	Missing
Mars	Mars	k1gInSc1	Mars
Water	Water	k1gInSc4	Water
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
