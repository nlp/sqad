<s>
Gustav	Gustav	k1gMnSc1
I.	I.	kA
Vasa	Vasa	k1gMnSc1
</s>
<s>
Gustav	Gustav	k1gMnSc1
I.	I.	kA
</s>
<s>
král	král	k1gMnSc1
švédský	švédský	k2eAgMnSc1d1
</s>
<s>
Král	Král	k1gMnSc1
Gustav	Gustav	k1gMnSc1
I.	I.	kA
Vasa	Vasa	k1gMnSc1
<g/>
,	,	kIx,
portrét	portrét	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1542	#num#	k4
od	od	k7c2
Jakoba	Jakob	k1gMnSc2
Binckse	Bincks	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Doba	doba	k1gFnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1523	#num#	k4
–	–	k?
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1560	#num#	k4
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1496	#num#	k4
(	(	kIx(
<g/>
pravděpodobně	pravděpodobně	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Hrad	hrad	k1gInSc1
Rydboholm	Rydboholm	k1gInSc1
<g/>
,	,	kIx,
Uppland	Uppland	k1gInSc1
nebo	nebo	k8xC
Lindholmen	Lindholmen	k1gInSc1
<g/>
,	,	kIx,
Uppland	Uppland	k1gInSc1
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1560	#num#	k4
(	(	kIx(
<g/>
64	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Stockholmský	stockholmský	k2eAgInSc1d1
palác	palác	k1gInSc1
<g/>
,	,	kIx,
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
Pohřben	pohřben	k2eAgInSc1d1
</s>
<s>
Katedrála	katedrála	k1gFnSc1
v	v	k7c6
Uppsale	Uppsala	k1gFnSc6
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1560	#num#	k4
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Kristián	Kristián	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dánský	dánský	k2eAgInSc1d1
</s>
<s>
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Erik	Erik	k1gMnSc1
XIV	XIV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švédský	švédský	k2eAgInSc1d1
</s>
<s>
Královna	královna	k1gFnSc1
</s>
<s>
Kateřina	Kateřina	k1gFnSc1
Sasko-LauenburskáMargaret	Sasko-LauenburskáMargareta	k1gFnPc2
LeijonhufvudKateřina	LeijonhufvudKateřina	k1gFnSc1
Stenbock	Stenbock	k1gInSc1
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
Erik	Erik	k1gMnSc1
XIV	XIV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
ŠvédskýJan	ŠvédskýJan	k1gInSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
ŠvédskýKateřina	ŠvédskýKateřina	k1gFnSc1
VasaCecílie	VasaCecílie	k1gFnSc1
<g/>
,	,	kIx,
markraběnka	markraběnka	k1gFnSc1
Badensko-RodemachernskáMagnus	Badensko-RodemachernskáMagnus	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
ÖstergötlanduAnna	ÖstergötlanduAnno	k1gNnSc2
Marie	Maria	k1gFnSc2
<g/>
,	,	kIx,
falckraběnka	falckraběnka	k1gFnSc1
z	z	k7c2
VeldenzŽofie	VeldenzŽofie	k1gFnSc2
<g/>
,	,	kIx,
vévodkyně	vévodkyně	k1gFnSc1
Sasko-LauenburskáAlžběta	Sasko-LauenburskáAlžběta	k1gFnSc1
<g/>
,	,	kIx,
vévodkyně	vévodkyně	k1gFnSc1
Meklenbursko-GadebušskáKarel	Meklenbursko-GadebušskáKarel	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švédský	švédský	k2eAgInSc1d1
</s>
<s>
Dynastie	dynastie	k1gFnSc1
</s>
<s>
Vasa	Vasa	k1gFnSc1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Erik	Erik	k1gMnSc1
Johansson	Johansson	k1gMnSc1
Vasa	Vasa	k1gMnSc1
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Cecilia	Cecilia	k1gFnSc1
Må	Må	k1gMnSc1
Eka	Eka	k1gMnSc1
</s>
<s>
Podpis	podpis	k1gInSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Gustav	Gustav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1496	#num#	k4
Rydboholm	Rydboholm	k1gInSc1
–	–	k?
29	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1560	#num#	k4
Stockholm	Stockholm	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
narozený	narozený	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
Gustav	Gustav	k1gMnSc1
Eriksson	Eriksson	k1gMnSc1
a	a	k8xC
později	pozdě	k6eAd2
známý	známý	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
Gustav	Gustav	k1gMnSc1
Vasa	Vasa	k1gMnSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
švédský	švédský	k2eAgMnSc1d1
král	král	k1gMnSc1
od	od	k7c2
roku	rok	k1gInSc2
1523	#num#	k4
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
prvním	první	k4xOgMnSc7
panovníkem	panovník	k1gMnSc7
z	z	k7c2
dynastie	dynastie	k1gFnSc2
Vasa	Vas	k1gInSc2
<g/>
,	,	kIx,
vlivného	vlivný	k2eAgInSc2d1
šlechtického	šlechtický	k2eAgInSc2d1
rodu	rod	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
vládl	vládnout	k5eAaImAgInS
ve	v	k7c6
Švédsku	Švédsko	k1gNnSc6
po	po	k7c4
většinu	většina	k1gFnSc4
16	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Gustav	Gustav	k1gMnSc1
I.	I.	kA
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
regentem	regens	k1gMnSc7
roku	rok	k1gInSc2
1521	#num#	k4
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
vedl	vést	k5eAaImAgMnS
povstání	povstání	k1gNnSc4
proti	proti	k7c3
dánskému	dánský	k2eAgMnSc3d1
králi	král	k1gMnSc3
Kristiánu	Kristián	k1gMnSc3
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
stál	stát	k5eAaImAgInS
v	v	k7c6
čele	čelo	k1gNnSc6
Kalmarské	Kalmarský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
a	a	k8xC
ovládal	ovládat	k5eAaImAgInS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
většinu	většina	k1gFnSc4
území	území	k1gNnSc1
dnešního	dnešní	k2eAgNnSc2d1
Švédska	Švédsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Gustav	Gustav	k1gMnSc1
měl	mít	k5eAaImAgMnS
nevyzpytatelnou	vyzpytatelný	k2eNgFnSc4d1
povahu	povaha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hovoří	hovořit	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c6
něm	on	k3xPp3gNnSc6
jako	jako	k8xS,k8xC
o	o	k7c6
osvoboditeli	osvoboditel	k1gMnSc6
země	zem	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
jako	jako	k9
o	o	k7c6
tyranizujícím	tyranizující	k2eAgMnSc6d1
vládci	vládce	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
je	být	k5eAaImIp3nS
častou	častý	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
vystupující	vystupující	k2eAgFnSc1d1
v	v	k7c6
mnoha	mnoho	k4c6
knihách	kniha	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1523	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
získal	získat	k5eAaPmAgMnS
moc	moc	k1gFnSc4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
téměř	téměř	k6eAd1
neznámou	známý	k2eNgFnSc7d1
osobností	osobnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stal	stát	k5eAaPmAgMnS
se	s	k7c7
vládcem	vládce	k1gMnSc7
rozdělené	rozdělený	k2eAgFnSc2d1
země	zem	k1gFnSc2
bez	bez	k7c2
ústřední	ústřední	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
nebyl	být	k5eNaImAgInS
tak	tak	k6eAd1
známý	známý	k2eAgInSc1d1
jako	jako	k8xS,k8xC
jeho	jeho	k3xOp3gMnPc1
současníci	současník	k1gMnPc1
z	z	k7c2
kontinentální	kontinentální	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
stal	stát	k5eAaPmAgMnS
se	s	k7c7
prvním	první	k4xOgMnSc7
rodilým	rodilý	k2eAgMnSc7d1
suverénním	suverénní	k2eAgMnSc7d1
švédským	švédský	k2eAgMnSc7d1
panovníkem	panovník	k1gMnSc7
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
zdatným	zdatný	k2eAgMnSc7d1
propagandistou	propagandista	k1gMnSc7
a	a	k8xC
byrokratem	byrokrat	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
zřídil	zřídit	k5eAaPmAgInS
silnou	silný	k2eAgFnSc4d1
ústřední	ústřední	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
bylo	být	k5eAaImAgNnS
ve	v	k7c6
Švédsku	Švédsko	k1gNnSc6
zavedeno	zavést	k5eAaPmNgNnS
protestantské	protestantský	k2eAgNnSc1d1
vyznání	vyznání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
tradiční	tradiční	k2eAgFnSc6d1
švédské	švédský	k2eAgFnSc6d1
historiografii	historiografie	k1gFnSc6
je	být	k5eAaImIp3nS
označován	označovat	k5eAaImNgInS
za	za	k7c2
zakladatele	zakladatel	k1gMnSc2
moderního	moderní	k2eAgInSc2d1
švédského	švédský	k2eAgInSc2d1
státu	stát	k1gInSc2
a	a	k8xC
za	za	k7c4
„	„	k?
<g/>
otce	otec	k1gMnSc4
vlasti	vlast	k1gFnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
pater	patro	k1gNnPc2
patriae	patriae	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gustav	Gustav	k1gMnSc1
se	se	k3xPyFc4
rád	rád	k6eAd1
srovnával	srovnávat	k5eAaImAgInS
s	s	k7c7
Mojžíšem	Mojžíš	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
osvobodil	osvobodit	k5eAaPmAgMnS
svůj	svůj	k3xOyFgInSc4
lid	lid	k1gInSc4
a	a	k8xC
založil	založit	k5eAaPmAgMnS
stát	stát	k5eAaPmF,k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
znám	znám	k2eAgMnSc1d1
svými	svůj	k3xOyFgFnPc7
nelítostnými	lítostný	k2eNgFnPc7d1
metodami	metoda	k1gFnPc7
a	a	k8xC
špatnou	špatný	k2eAgFnSc7d1
povahou	povaha	k1gFnSc7
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
však	však	k9
miloval	milovat	k5eAaImAgMnS
hudbu	hudba	k1gFnSc4
a	a	k8xC
vyznačoval	vyznačovat	k5eAaImAgMnS
se	se	k3xPyFc4
bystrým	bystrý	k2eAgInSc7d1
úsudkem	úsudek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Manželství	manželství	k1gNnSc1
a	a	k8xC
potomci	potomek	k1gMnPc1
</s>
<s>
Gustav	Gustav	k1gMnSc1
I.	I.	kA
Vasa	Vasa	k1gMnSc1
byl	být	k5eAaImAgMnS
třikrát	třikrát	k6eAd1
ženatý	ženatý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
roku	rok	k1gInSc2
1531	#num#	k4
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
princeznou	princezna	k1gFnSc7
Kateřinou	Kateřina	k1gFnSc7
von	von	k1gInSc4
Sachsen-Lauenburg-Ratzeburg	Sachsen-Lauenburg-Ratzeburg	k1gInSc1
(	(	kIx(
<g/>
1513	#num#	k4
<g/>
–	–	k?
<g/>
1535	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgNnSc2
manželství	manželství	k1gNnSc2
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
jediný	jediný	k2eAgMnSc1d1
syn	syn	k1gMnSc1
<g/>
;	;	kIx,
po	po	k7c6
potratu	potrat	k1gInSc6
druhého	druhý	k4xOgNnSc2
dítěte	dítě	k1gNnSc2
Kateřina	Kateřina	k1gFnSc1
23	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
roku	rok	k1gInSc2
1535	#num#	k4
zemřela	zemřít	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Erik	Erik	k1gMnSc1
(	(	kIx(
<g/>
1533	#num#	k4
<g/>
–	–	k?
<g/>
1577	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
švédský	švédský	k2eAgMnSc1d1
král	král	k1gMnSc1
v	v	k7c6
letech	let	k1gInPc6
1560	#num#	k4
<g/>
–	–	k?
<g/>
1567	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
roku	rok	k1gInSc2
1536	#num#	k4
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Markétou	Markéta	k1gFnSc7
Leijonhufvud	Leijonhufvud	k1gMnSc1
(	(	kIx(
<g/>
1514	#num#	k4
<g/>
–	–	k?
<g/>
1551	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
manželství	manželství	k1gNnSc2
vzešlo	vzejít	k5eAaPmAgNnS
deset	deset	k4xCc1
potomků	potomek	k1gMnPc2
–	–	k?
pět	pět	k4xCc1
dcer	dcera	k1gFnPc2
a	a	k8xC
pět	pět	k4xCc1
synů	syn	k1gMnPc2
<g/>
,	,	kIx,
nichž	jenž	k3xRgMnPc2
dva	dva	k4xCgMnPc1
zemřeli	zemřít	k5eAaPmAgMnP
jako	jako	k8xC,k8xS
nemluvňata	nemluvně	k1gNnPc1
<g/>
:	:	kIx,
</s>
<s>
Jan	Jan	k1gMnSc1
(	(	kIx(
<g/>
1537	#num#	k4
<g/>
–	–	k?
<g/>
1592	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
švédský	švédský	k2eAgMnSc1d1
král	král	k1gMnSc1
v	v	k7c6
letech	let	k1gInPc6
1567	#num#	k4
<g/>
–	–	k?
<g/>
1592	#num#	k4
<g/>
;	;	kIx,
</s>
<s>
Kateřina	Kateřina	k1gFnSc1
(	(	kIx(
<g/>
1539	#num#	k4
<g/>
–	–	k?
<g/>
1610	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
∞	∞	k?
vévoda	vévoda	k1gMnSc1
Eduard	Eduard	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východofríský	Východofríský	k1gMnSc1
<g/>
;	;	kIx,
</s>
<s>
Cecilie	Cecilie	k1gFnSc1
(	(	kIx(
<g/>
1540	#num#	k4
<g/>
–	–	k?
<g/>
1627	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
∞	∞	k?
vévoda	vévoda	k1gMnSc1
Kryštof	Kryštof	k1gMnSc1
Baden-Rodemachern	Baden-Rodemachern	k1gMnSc1
<g/>
;	;	kIx,
</s>
<s>
Magnus	Magnus	k1gInSc1
(	(	kIx(
<g/>
1542	#num#	k4
<g/>
–	–	k?
<g/>
1595	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
östergötlandský	östergötlandský	k2eAgMnSc1d1
<g/>
;	;	kIx,
</s>
<s>
Karel	Karel	k1gMnSc1
(	(	kIx(
<g/>
1544	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Anna	Anna	k1gFnSc1
(	(	kIx(
<g/>
1545	#num#	k4
<g/>
–	–	k?
<g/>
1610	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
∞	∞	k?
vévoda	vévoda	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Jan	Jan	k1gMnSc1
Falcko-veldenzský	Falcko-veldenzský	k2eAgMnSc1d1
<g/>
;	;	kIx,
</s>
<s>
Sten	sten	k1gInSc1
(	(	kIx(
<g/>
1546	#num#	k4
<g/>
–	–	k?
<g/>
1547	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Žofie	Žofie	k1gFnSc1
(	(	kIx(
<g/>
1547	#num#	k4
<g/>
–	–	k?
<g/>
1611	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
∞	∞	k?
vévoda	vévoda	k1gMnSc1
Magnus	Magnus	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sasko-Lauenburský	Sasko-Lauenburský	k2eAgInSc1d1
<g/>
;	;	kIx,
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
(	(	kIx(
<g/>
1549	#num#	k4
<g/>
–	–	k?
<g/>
1597	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
∞	∞	k?
vévoda	vévoda	k1gMnSc1
Kryštof	Kryštof	k1gMnSc1
Mecklenburský	Mecklenburský	k2eAgMnSc1d1
<g/>
;	;	kIx,
</s>
<s>
Karel	Karel	k1gMnSc1
(	(	kIx(
<g/>
1550	#num#	k4
<g/>
–	–	k?
<g/>
1611	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
švédský	švédský	k2eAgMnSc1d1
král	král	k1gMnSc1
v	v	k7c6
letech	let	k1gInPc6
1599	#num#	k4
<g/>
–	–	k?
<g/>
1611	#num#	k4
<g/>
;	;	kIx,
</s>
<s>
Dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1552	#num#	k4
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Kateřinou	Kateřina	k1gFnSc7
Stenbock	Stenbock	k1gMnSc1
(	(	kIx(
<g/>
1535	#num#	k4
<g/>
–	–	k?
<g/>
1621	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
bylo	být	k5eAaImAgNnS
Gustavovi	Gustavův	k2eAgMnPc1d1
56	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
Kateřině	Kateřina	k1gFnSc6
šestnáct	šestnáct	k4xCc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manželství	manželství	k1gNnSc1
trvalo	trvat	k5eAaImAgNnS
osm	osm	k4xCc4
let	léto	k1gNnPc2
a	a	k8xC
zůstalo	zůstat	k5eAaPmAgNnS
bezdětné	bezdětný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Gustav	Gustav	k1gMnSc1
I	i	k9
of	of	k?
Sweden	Swedna	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
LARSSON	LARSSON	kA
<g/>
,	,	kIx,
Lars	Lars	k1gInSc1
Olof	Olof	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gustaw	Gustaw	k1gMnSc1
Waza	Waza	k1gMnSc1
:	:	kIx,
ojciec	ojciec	k1gMnSc1
państwa	państwa	k1gMnSc1
szwedzkiego	szwedzkiego	k1gMnSc1
czy	czy	k?
tyran	tyran	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Warszawa	Warszaw	k1gInSc2
<g/>
:	:	kIx,
Państwowy	Państwowa	k1gFnPc1
Instytut	Instytut	k1gInSc4
Wydawniczy	Wydawnicza	k1gFnSc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
471	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
83	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3167	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Vasův	Vasův	k2eAgInSc1d1
běh	běh	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Gustav	Gustav	k1gMnSc1
I.	I.	kA
Vasa	Vasum	k1gNnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Kristián	Kristián	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Švédský	švédský	k2eAgMnSc1d1
král	král	k1gMnSc1
Gustav	Gustav	k1gMnSc1
I	i	k9
<g/>
.1523	.1523	k4
<g/>
–	–	k?
<g/>
1560	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Erik	Erik	k1gMnSc1
XIV	XIV	kA
<g/>
.	.	kIx.
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Kristián	Kristián	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dánský	dánský	k2eAgInSc1d1
</s>
<s>
Finský	finský	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
Kustaa	Kustaa	k1gMnSc1
I.	I.	kA
Vaasa	Vaasa	k1gFnSc1
<g/>
1523	#num#	k4
<g/>
–	–	k?
<g/>
1560	#num#	k4
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Erik	Erik	k1gMnSc1
XIV	XIV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švédský	švédský	k2eAgInSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Dynastie	dynastie	k1gFnSc1
vládnoucí	vládnoucí	k2eAgFnSc1d1
ve	v	k7c6
Švédsku	Švédsko	k1gNnSc6
Seznam	seznam	k1gInSc1
dynastií	dynastie	k1gFnSc7
</s>
<s>
Ynglingové	Ynglingový	k2eAgFnPc4d1
•	•	k?
Stenkilové	Stenkilová	k1gFnPc4
•	•	k?
Sverkerové	Sverkerová	k1gFnSc2
a	a	k8xC
Erikovci	Erikovec	k1gInSc3
•	•	k?
Folkungové	Folkungový	k2eAgFnSc2d1
•	•	k?
Kalmarská	Kalmarský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
•	•	k?
Vasovci	Vasovec	k1gInSc6
•	•	k?
Wittelsbachové	Wittelsbachová	k1gFnSc2
•	•	k?
Hesenští	hesenský	k2eAgMnPc1d1
•	•	k?
Holstein-Gottorpští	Holstein-Gottorpský	k2eAgMnPc1d1
•	•	k?
Bernadottové	Bernadott	k1gMnPc1
Dynastie	dynastie	k1gFnSc2
Vasa	Vasa	k1gMnSc1
(	(	kIx(
<g/>
1523	#num#	k4
<g/>
–	–	k?
<g/>
1654	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Gustav	Gustav	k1gMnSc1
I.	I.	kA
•	•	k?
Erik	Erik	k1gMnSc1
XIV	XIV	kA
<g/>
.	.	kIx.
•	•	k?
Jan	Jan	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Zikmund	Zikmund	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
•	•	k?
Karel	Karel	k1gMnSc1
IX	IX	kA
<g/>
.	.	kIx.
•	•	k?
Gustav	Gustav	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Adolf	Adolf	k1gMnSc1
•	•	k?
Kristina	Kristina	k1gFnSc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
118699431	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
6133	#num#	k4
2050	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
82013210	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
59878606	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
82013210	#num#	k4
</s>
