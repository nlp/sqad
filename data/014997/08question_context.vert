<s>
Gustav	Gustav	k1gMnSc1
I.	I.	kA
(	(	kIx(
<g/>
12	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1496	#num#	k4
Rydboholm	Rydboholm	k1gInSc1
–	–	k?
29	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1560	#num#	k4
Stockholm	Stockholm	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
narozený	narozený	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
Gustav	Gustav	k1gMnSc1
Eriksson	Eriksson	k1gMnSc1
a	a	k8xC
později	pozdě	k6eAd2
známý	známý	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
Gustav	Gustav	k1gMnSc1
Vasa	Vasa	k1gMnSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
švédský	švédský	k2eAgMnSc1d1
král	král	k1gMnSc1
od	od	k7c2
roku	rok	k1gInSc2
1523	#num#	k4
až	až	k9
do	do	k7c2
své	svůj	k3xOyFgFnSc2
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
</s>