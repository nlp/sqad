<p>
<s>
List	list	k1gInSc1	list
od	od	k7c2	od
Nimrala	nimral	k1gMnSc2	nimral
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Leaf	Leaf	k1gMnSc1	Leaf
by	by	kYmCp3nS	by
Niggle	Niggle	k1gInSc1	Niggle
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
filozofická	filozofický	k2eAgFnSc1d1	filozofická
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
napsaná	napsaný	k2eAgFnSc1d1	napsaná
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkienem	Tolkien	k1gMnSc7	Tolkien
v	v	k7c6	v
letech	let	k1gInPc6	let
1938	[number]	k4	1938
–	–	k?	–
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Dublin	Dublin	k1gInSc1	Dublin
Review	Review	k1gFnSc2	Review
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Tree	Tre	k1gInSc2	Tre
and	and	k?	and
Leaf	Leaf	k1gMnSc1	Leaf
a	a	k8xC	a
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Poems	Poemsa	k1gFnPc2	Poemsa
and	and	k?	and
Stories	Storiesa	k1gFnPc2	Storiesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
vyšla	vyjít	k5eAaPmAgFnS	vyjít
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Pohádky	pohádka	k1gFnSc2	pohádka
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgNnSc4d1	vydané
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Winston-Smith	Winston-Smitha	k1gFnPc2	Winston-Smitha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
Příběhy	příběh	k1gInPc1	příběh
z	z	k7c2	z
čarovné	čarovný	k2eAgFnSc2d1	čarovná
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgNnSc4d1	vydané
nakladatelstvím	nakladatelství	k1gNnSc7	nakladatelství
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
List	list	k1gInSc1	list
od	od	k7c2	od
Nimrala	nimral	k1gMnSc2	nimral
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
příběhy	příběh	k1gInPc4	příběh
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
pro	pro	k7c4	pro
obě	dva	k4xCgNnPc4	dva
česká	český	k2eAgNnPc4d1	české
vydání	vydání	k1gNnPc4	vydání
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Stanislava	Stanislav	k1gMnSc4	Stanislav
Pošustová	Pošustová	k1gFnSc1	Pošustová
<g/>
.	.	kIx.	.
</s>
<s>
Povídka	povídka	k1gFnSc1	povídka
vypovídá	vypovídat	k5eAaPmIp3nS	vypovídat
o	o	k7c6	o
Tolkienově	Tolkienův	k2eAgInSc6d1	Tolkienův
přístupu	přístup	k1gInSc6	přístup
k	k	k7c3	k
umělecké	umělecký	k2eAgFnSc3d1	umělecká
tvorbě	tvorba	k1gFnSc3	tvorba
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
alegorií	alegorie	k1gFnSc7	alegorie
jeho	on	k3xPp3gNnSc2	on
chápání	chápání	k1gNnSc2	chápání
života	život	k1gInSc2	život
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Povídka	povídka	k1gFnSc1	povídka
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
malíři	malíř	k1gMnSc6	malíř
jménem	jméno	k1gNnSc7	jméno
Nimral	nimral	k1gMnSc1	nimral
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Niggle	Niggle	k1gFnSc2	Niggle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žijícím	žijící	k2eAgMnSc7d1	žijící
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
není	být	k5eNaImIp3nS	být
ceněno	cenit	k5eAaImNgNnS	cenit
<g/>
.	.	kIx.	.
</s>
<s>
Maluje	malovat	k5eAaImIp3nS	malovat
tedy	tedy	k9	tedy
z	z	k7c2	z
nutkavé	nutkavý	k2eAgFnSc2d1	nutkavá
potřeby	potřeba	k1gFnSc2	potřeba
jen	jen	k9	jen
pro	pro	k7c4	pro
vlastní	vlastní	k2eAgNnSc4d1	vlastní
potěšení	potěšení	k1gNnSc4	potěšení
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
životním	životní	k2eAgNnSc7d1	životní
dílem	dílo	k1gNnSc7	dílo
je	být	k5eAaImIp3nS	být
obrovské	obrovský	k2eAgNnSc4d1	obrovské
plátno	plátno	k1gNnSc4	plátno
s	s	k7c7	s
obrazem	obraz	k1gInSc7	obraz
velkého	velký	k2eAgInSc2d1	velký
stromu	strom	k1gInSc2	strom
uprostřed	uprostřed	k7c2	uprostřed
lesa	les	k1gInSc2	les
s	s	k7c7	s
krajinou	krajina	k1gFnSc7	krajina
a	a	k8xC	a
s	s	k7c7	s
horami	hora	k1gFnPc7	hora
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Vymalovává	vymalovávat	k5eAaImIp3nS	vymalovávat
detailně	detailně	k6eAd1	detailně
každý	každý	k3xTgInSc1	každý
list	list	k1gInSc1	list
stromu	strom	k1gInSc2	strom
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc1	každý
jedinečný	jedinečný	k2eAgInSc1d1	jedinečný
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
obrazu	obraz	k1gInSc3	obraz
Nimral	nimrat	k5eAaImAgMnS	nimrat
opomíjí	opomíjet	k5eAaImIp3nS	opomíjet
mnohé	mnohé	k1gNnSc4	mnohé
své	svůj	k3xOyFgFnSc2	svůj
jiné	jiný	k2eAgFnSc2d1	jiná
povinnosti	povinnost	k1gFnSc2	povinnost
<g/>
,	,	kIx,	,
i	i	k8xC	i
tak	tak	k9	tak
ho	on	k3xPp3gMnSc4	on
ale	ale	k8xC	ale
od	od	k7c2	od
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
každou	každý	k3xTgFnSc4	každý
chvíli	chvíle	k1gFnSc6	chvíle
odvádí	odvádět	k5eAaImIp3nP	odvádět
nějaké	nějaký	k3yIgFnPc4	nějaký
všední	všední	k2eAgFnPc4d1	všední
starosti	starost	k1gFnPc4	starost
a	a	k8xC	a
povinnosti	povinnost	k1gFnPc4	povinnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nimral	nimral	k1gMnSc1	nimral
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
"	"	kIx"	"
<g/>
vydat	vydat	k5eAaPmF	vydat
na	na	k7c4	na
dalekou	daleký	k2eAgFnSc4d1	daleká
cestu	cesta	k1gFnSc4	cesta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
nemůže	moct	k5eNaImIp3nS	moct
vyhnout	vyhnout	k5eAaPmF	vyhnout
<g/>
"	"	kIx"	"
a	a	k8xC	a
tuší	tušit	k5eAaImIp3nS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nestihne	stihnout	k5eNaPmIp3nS	stihnout
svůj	svůj	k3xOyFgInSc4	svůj
obraz	obraz	k1gInSc4	obraz
dokončit	dokončit	k5eAaPmF	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
se	se	k3xPyFc4	se
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
cestu	cesta	k1gFnSc4	cesta
musí	muset	k5eAaImIp3nP	muset
připravit	připravit	k5eAaPmF	připravit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přípravy	příprava	k1gFnSc2	příprava
neustále	neustále	k6eAd1	neustále
odkládá	odkládat	k5eAaImIp3nS	odkládat
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k9	jednou
jej	on	k3xPp3gMnSc4	on
od	od	k7c2	od
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
vyruší	vyrušit	k5eAaPmIp3nS	vyrušit
jeho	jeho	k3xOp3gMnSc1	jeho
chromý	chromý	k2eAgMnSc1d1	chromý
soused	soused	k1gMnSc1	soused
Farský	farský	k2eAgMnSc1d1	farský
s	s	k7c7	s
prosbou	prosba	k1gFnSc7	prosba
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
-	-	kIx~	-
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
je	být	k5eAaImIp3nS	být
nemocná	mocný	k2eNgFnSc1d1	mocný
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
zavolat	zavolat	k5eAaPmF	zavolat
lékaře	lékař	k1gMnPc4	lékař
<g/>
.	.	kIx.	.
</s>
<s>
Nimral	nimral	k1gMnSc1	nimral
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
obětavě	obětavě	k6eAd1	obětavě
vydá	vydat	k5eAaPmIp3nS	vydat
v	v	k7c6	v
dešti	dešť	k1gInSc6	dešť
pro	pro	k7c4	pro
lékaře	lékař	k1gMnSc4	lékař
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
tuší	tušit	k5eAaImIp3nS	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvůli	kvůli	k7c3	kvůli
této	tento	k3xDgFnSc3	tento
pochůzce	pochůzka	k1gFnSc3	pochůzka
možná	možná	k9	možná
nestihne	stihnout	k5eNaPmIp3nS	stihnout
domalovat	domalovat	k5eAaPmF	domalovat
svůj	svůj	k3xOyFgInSc4	svůj
milovaný	milovaný	k2eAgInSc4d1	milovaný
obraz	obraz	k1gInSc4	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
domů	domů	k6eAd1	domů
sám	sám	k3xTgMnSc1	sám
onemocní	onemocnět	k5eAaPmIp3nS	onemocnět
a	a	k8xC	a
přijede	přijet	k5eAaPmIp3nS	přijet
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
kdosi	kdosi	k3yInSc1	kdosi
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
představí	představit	k5eAaPmIp3nS	představit
jako	jako	k9	jako
Kočí	Kočí	k2eAgNnSc1d1	Kočí
<g/>
.	.	kIx.	.
</s>
<s>
Přišel	přijít	k5eAaPmAgMnS	přijít
prý	prý	k9	prý
Nimralův	nimralův	k2eAgInSc4d1	nimralův
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
odcestovat	odcestovat	k5eAaPmF	odcestovat
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
ale	ale	k8xC	ale
Nimral	nimral	k1gMnSc1	nimral
nebyl	být	k5eNaImAgMnS	být
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
připraven	připraven	k2eAgMnSc1d1	připraven
<g/>
,	,	kIx,	,
pošlou	poslat	k5eAaPmIp3nP	poslat
jej	on	k3xPp3gNnSc4	on
do	do	k7c2	do
jakéhosi	jakýsi	k3yIgInSc2	jakýsi
nemocničního	nemocniční	k2eAgInSc2d1	nemocniční
ústavu	ústav	k1gInSc2	ústav
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
musí	muset	k5eAaImIp3nS	muset
vykonávat	vykonávat	k5eAaImF	vykonávat
podřadnou	podřadný	k2eAgFnSc4d1	podřadná
a	a	k8xC	a
těžkou	těžký	k2eAgFnSc4d1	těžká
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Pracuje	pracovat	k5eAaImIp3nS	pracovat
nekonečně	konečně	k6eNd1	konečně
dlouho	dlouho	k6eAd1	dlouho
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
tvrdě	tvrdě	k6eAd1	tvrdě
až	až	k9	až
do	do	k7c2	do
úplného	úplný	k2eAgNnSc2d1	úplné
vyčerpání	vyčerpání	k1gNnSc2	vyčerpání
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
vyslechne	vyslechnout	k5eAaPmIp3nS	vyslechnout
jakousi	jakýsi	k3yIgFnSc4	jakýsi
poradu	porada	k1gFnSc4	porada
<g/>
.	.	kIx.	.
</s>
<s>
Lékaři	lékař	k1gMnPc1	lékař
zde	zde	k6eAd1	zde
hodnotí	hodnotit	k5eAaImIp3nP	hodnotit
celý	celý	k2eAgInSc4d1	celý
jeho	jeho	k3xOp3gInSc4	jeho
předešlý	předešlý	k2eAgInSc4d1	předešlý
život	život	k1gInSc4	život
a	a	k8xC	a
posuzují	posuzovat	k5eAaImIp3nP	posuzovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
má	mít	k5eAaImIp3nS	mít
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
ústavu	ústav	k1gInSc6	ústav
zůstat	zůstat	k5eAaPmF	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Nimralovo	nimralův	k2eAgNnSc4d1	nimralův
propuštění	propuštění	k1gNnSc4	propuštění
"	"	kIx"	"
<g/>
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
stupeň	stupeň	k1gInSc4	stupeň
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
rozhodující	rozhodující	k2eAgFnSc1d1	rozhodující
jeho	jeho	k3xOp3gFnSc1	jeho
obětavá	obětavý	k2eAgFnSc1d1	obětavá
cesta	cesta	k1gFnSc1	cesta
deštěm	dešť	k1gInSc7	dešť
pro	pro	k7c4	pro
lékaře	lékař	k1gMnPc4	lékař
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
poslán	poslat	k5eAaPmNgMnS	poslat
do	do	k7c2	do
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
krajina	krajina	k1gFnSc1	krajina
s	s	k7c7	s
lesem	les	k1gInSc7	les
a	a	k8xC	a
velkým	velký	k2eAgInSc7d1	velký
stromem	strom	k1gInSc7	strom
je	být	k5eAaImIp3nS	být
krajinou	krajina	k1gFnSc7	krajina
z	z	k7c2	z
jeho	jeho	k3xOp3gInSc2	jeho
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
připojí	připojit	k5eAaPmIp3nS	připojit
jeho	jeho	k3xOp3gMnSc1	jeho
soused	soused	k1gMnSc1	soused
Farský	farský	k2eAgMnSc1d1	farský
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
pracují	pracovat	k5eAaImIp3nP	pracovat
jako	jako	k8xS	jako
zahradníci	zahradník	k1gMnPc1	zahradník
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
Nimral	nimral	k1gMnSc1	nimral
pocítí	pocítit	k5eAaPmIp3nS	pocítit
touhu	touha	k1gFnSc4	touha
vydat	vydat	k5eAaPmF	vydat
se	se	k3xPyFc4	se
do	do	k7c2	do
nejvzdálenějších	vzdálený	k2eAgNnPc2d3	nejvzdálenější
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
do	do	k7c2	do
Hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
obraze	obraz	k1gInSc6	obraz
zobrazeny	zobrazit	k5eAaPmNgFnP	zobrazit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
obrysech	obrys	k1gInPc6	obrys
<g/>
.	.	kIx.	.
</s>
<s>
Farský	farský	k2eAgMnSc1d1	farský
se	se	k3xPyFc4	se
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
cestu	cesta	k1gFnSc4	cesta
ještě	ještě	k9	ještě
necítí	cítit	k5eNaImIp3nS	cítit
být	být	k5eAaImF	být
připraven	připravit	k5eAaPmNgInS	připravit
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
s	s	k7c7	s
Nimralem	nimral	k1gMnSc7	nimral
na	na	k7c4	na
čas	čas	k1gInSc4	čas
loučí	louč	k1gFnPc2	louč
a	a	k8xC	a
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
v	v	k7c6	v
krajině	krajina	k1gFnSc6	krajina
z	z	k7c2	z
Nimralova	nimralův	k2eAgInSc2d1	nimralův
obrazu	obraz	k1gInSc2	obraz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nimralovi	nimralův	k2eAgMnPc1d1	nimralův
sousedi	soused	k1gMnPc1	soused
a	a	k8xC	a
přátelé	přítel	k1gMnPc1	přítel
mluví	mluvit	k5eAaImIp3nP	mluvit
o	o	k7c6	o
Nimralovi	nimral	k1gMnSc6	nimral
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
jeho	jeho	k3xOp3gNnSc4	jeho
malování	malování	k1gNnSc4	malování
prakticky	prakticky	k6eAd1	prakticky
nikdo	nikdo	k3yNnSc1	nikdo
necenil	cenit	k5eNaImAgMnS	cenit
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc2	jeho
práce	práce	k1gFnSc2	práce
se	se	k3xPyFc4	se
zastal	zastat	k5eAaPmAgMnS	zastat
pouze	pouze	k6eAd1	pouze
pan	pan	k1gMnSc1	pan
učitel	učitel	k1gMnSc1	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Zachránil	zachránit	k5eAaPmAgMnS	zachránit
z	z	k7c2	z
Nimralova	nimralův	k2eAgInSc2d1	nimralův
obrazu	obraz	k1gInSc2	obraz
malý	malý	k2eAgInSc1d1	malý
kousek	kousek	k1gInSc1	kousek
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
byl	být	k5eAaImAgInS	být
namalovaný	namalovaný	k2eAgInSc1d1	namalovaný
jeden	jeden	k4xCgInSc1	jeden
list	list	k1gInSc1	list
stromu	strom	k1gInSc2	strom
-	-	kIx~	-
"	"	kIx"	"
<g/>
List	list	k1gInSc1	list
od	od	k7c2	od
Nimrala	nimral	k1gMnSc2	nimral
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
pak	pak	k6eAd1	pak
dlouho	dlouho	k6eAd1	dlouho
visel	viset	k5eAaImAgInS	viset
zarámovaný	zarámovaný	k2eAgInSc1d1	zarámovaný
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
,	,	kIx,	,
než	než	k8xS	než
i	i	k9	i
s	s	k7c7	s
muzeem	muzeum	k1gNnSc7	muzeum
při	při	k7c6	při
požáru	požár	k1gInSc6	požár
shořel	shořet	k5eAaPmAgInS	shořet
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
o	o	k7c6	o
Nimralovi	nimral	k1gMnSc6	nimral
a	a	k8xC	a
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
krajině	krajina	k1gFnSc6	krajina
z	z	k7c2	z
obrazu	obraz	k1gInSc2	obraz
mluví	mluvit	k5eAaImIp3nP	mluvit
Lékaři	lékař	k1gMnPc1	lékař
z	z	k7c2	z
ústavu	ústav	k1gInSc2	ústav
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
naopak	naopak	k6eAd1	naopak
mluví	mluvit	k5eAaImIp3nP	mluvit
s	s	k7c7	s
uznáním	uznání	k1gNnSc7	uznání
a	a	k8xC	a
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Farský	farský	k2eAgInSc1d1	farský
Nimralov	Nimralov	k1gInSc1	Nimralov
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
krajinu	krajina	k1gFnSc4	krajina
nazvali	nazvat	k5eAaPmAgMnP	nazvat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejlepším	dobrý	k2eAgInSc7d3	nejlepší
předstupněm	předstupeň	k1gInSc7	předstupeň
k	k	k7c3	k
Horám	hora	k1gFnPc3	hora
<g/>
,	,	kIx,	,
krajem	krajem	k6eAd1	krajem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jejich	jejich	k3xOp3gMnPc1	jejich
svěřenci	svěřenec	k1gMnPc1	svěřenec
docházejí	docházet	k5eAaImIp3nP	docházet
zotavení	zotavený	k2eAgMnPc1d1	zotavený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozbor	rozbor	k1gInSc1	rozbor
==	==	k?	==
</s>
</p>
<p>
<s>
Nimralův	nimralův	k2eAgInSc1d1	nimralův
list	list	k1gInSc1	list
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
číst	číst	k5eAaImF	číst
jako	jako	k8xC	jako
alegorii	alegorie	k1gFnSc4	alegorie
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
očistce	očistec	k1gInSc2	očistec
a	a	k8xC	a
ráje	ráj	k1gInSc2	ráj
<g/>
.	.	kIx.	.
</s>
<s>
Nimral	nimral	k1gMnSc1	nimral
není	být	k5eNaImIp3nS	být
připraven	připravit	k5eAaPmNgMnS	připravit
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
nevyhnutelnou	vyhnutelný	k2eNgFnSc4d1	nevyhnutelná
cestu	cesta	k1gFnSc4	cesta
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
lidé	člověk	k1gMnPc1	člověk
nejsou	být	k5eNaImIp3nP	být
připraveni	připravit	k5eAaPmNgMnP	připravit
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
v	v	k7c6	v
ústavu	ústav	k1gInSc6	ústav
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
lese	les	k1gInSc6	les
představuje	představovat	k5eAaImIp3nS	představovat
očistec	očistec	k1gInSc1	očistec
odkud	odkud	k6eAd1	odkud
odchází	odcházet	k5eAaImIp3nS	odcházet
Nimral	nimrat	k5eAaImAgMnS	nimrat
do	do	k7c2	do
Hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
představují	představovat	k5eAaImIp3nP	představovat
ráj	ráj	k1gInSc4	ráj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nimralův	nimralův	k2eAgInSc1d1	nimralův
list	list	k1gInSc1	list
tak	tak	k6eAd1	tak
připomíná	připomínat	k5eAaImIp3nS	připomínat
knihu	kniha	k1gFnSc4	kniha
Poslední	poslední	k2eAgFnSc1d1	poslední
bitva	bitva	k1gFnSc1	bitva
Tolkienova	Tolkienův	k2eAgMnSc2d1	Tolkienův
blízkého	blízký	k2eAgMnSc2d1	blízký
přítele	přítel	k1gMnSc2	přítel
C.	C.	kA	C.
<g/>
S.	S.	kA	S.
Lewise	Lewise	k1gFnSc1	Lewise
<g/>
.	.	kIx.	.
</s>
<s>
Důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
očistu	očista	k1gFnSc4	očista
je	být	k5eAaImIp3nS	být
však	však	k9	však
u	u	k7c2	u
Tolkiena	Tolkieno	k1gNnSc2	Tolkieno
jako	jako	k8xC	jako
u	u	k7c2	u
katolíka	katolík	k1gMnSc2	katolík
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
u	u	k7c2	u
C.	C.	kA	C.
<g/>
S.	S.	kA	S.
Lewise	Lewise	k1gFnSc1	Lewise
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
anglikánem	anglikán	k1gMnSc7	anglikán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nimralův	nimralův	k2eAgInSc1d1	nimralův
list	list	k1gInSc1	list
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
interpretován	interpretován	k2eAgMnSc1d1	interpretován
jako	jako	k8xS	jako
alegorie	alegorie	k1gFnSc1	alegorie
Tolkienova	Tolkienův	k2eAgNnSc2d1	Tolkienovo
chápání	chápání	k1gNnSc2	chápání
světa	svět	k1gInSc2	svět
-	-	kIx~	-
skutečné	skutečný	k2eAgNnSc1d1	skutečné
stvoření	stvoření	k1gNnSc1	stvoření
je	být	k5eAaImIp3nS	být
výsadou	výsada	k1gFnSc7	výsada
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
a	a	k8xC	a
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
tvořit	tvořit	k5eAaImF	tvořit
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
pouze	pouze	k6eAd1	pouze
ozvěny	ozvěna	k1gFnSc2	ozvěna
tohoto	tento	k3xDgNnSc2	tento
stvoření	stvoření	k1gNnSc2	stvoření
(	(	kIx(	(
<g/>
dobro	dobro	k1gNnSc1	dobro
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
karikaturu	karikatura	k1gFnSc4	karikatura
tohoto	tento	k3xDgNnSc2	tento
stvoření	stvoření	k1gNnSc2	stvoření
(	(	kIx(	(
<g/>
zlo	zlo	k1gNnSc1	zlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
Nimralova	nimralův	k2eAgFnSc1d1	nimralův
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
pravdě	pravda	k1gFnSc6	pravda
a	a	k8xC	a
kráse	krása	k1gFnSc6	krása
odráží	odrážet	k5eAaImIp3nS	odrážet
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
obraze	obraz	k1gInSc6	obraz
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
odměněn	odměnit	k5eAaPmNgMnS	odměnit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
obraz	obraz	k1gInSc1	obraz
stane	stanout	k5eAaPmIp3nS	stanout
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
také	také	k9	také
strom	strom	k1gInSc1	strom
skutečně	skutečně	k6eAd1	skutečně
existuje	existovat	k5eAaImIp3nS	existovat
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
obraze	obraz	k1gInSc6	obraz
je	být	k5eAaImIp3nS	být
odrážen	odrážen	k2eAgInSc1d1	odrážen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Postava	postava	k1gFnSc1	postava
Nimrala	nimral	k1gMnSc2	nimral
je	být	k5eAaImIp3nS	být
autobiografická	autobiografický	k2eAgFnSc1d1	autobiografická
–	–	k?	–
všední	všední	k2eAgFnPc1d1	všední
starosti	starost	k1gFnPc1	starost
Tolkiena	Tolkieno	k1gNnSc2	Tolkieno
odváděly	odvádět	k5eAaImAgFnP	odvádět
od	od	k7c2	od
literární	literární	k2eAgFnSc2d1	literární
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Tolkien	Tolkien	k1gInSc1	Tolkien
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
literární	literární	k2eAgFnSc6d1	literární
tvorbě	tvorba	k1gFnSc6	tvorba
perfekcionistický	perfekcionistický	k2eAgInSc1d1	perfekcionistický
<g/>
,	,	kIx,	,
usiloval	usilovat	k5eAaImAgInS	usilovat
o	o	k7c4	o
dokonalou	dokonalý	k2eAgFnSc4d1	dokonalá
reálnost	reálnost	k1gFnSc4	reálnost
svého	svůj	k3xOyFgInSc2	svůj
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
mj.	mj.	kA	mj.
ve	v	k7c6	v
chronologii	chronologie	k1gFnSc6	chronologie
příběhů	příběh	k1gInPc2	příběh
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jazycích	jazyk	k1gInPc6	jazyk
národů	národ	k1gInPc2	národ
jeho	jeho	k3xOp3gFnSc2	jeho
bájné	bájný	k2eAgFnSc2d1	bájná
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Nimral	nimral	k1gMnSc1	nimral
odsouval	odsouvat	k5eAaImAgMnS	odsouvat
ostatní	ostatní	k2eAgFnPc4d1	ostatní
činnosti	činnost	k1gFnPc4	činnost
a	a	k8xC	a
věnoval	věnovat	k5eAaImAgMnS	věnovat
se	se	k3xPyFc4	se
především	především	k9	především
práci	práce	k1gFnSc4	práce
na	na	k7c6	na
knihách	kniha	k1gFnPc6	kniha
o	o	k7c6	o
Středozemi	Středozem	k1gFnSc6	Středozem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Celá	celý	k2eAgFnSc1d1	celá
povídka	povídka	k1gFnSc1	povídka
česky	česky	k6eAd1	česky
v	v	k7c6	v
pdf	pdf	k?	pdf
formátu	formát	k1gInSc6	formát
</s>
</p>
<p>
<s>
K	k	k7c3	k
tematu	temat	k1gInSc3	temat
Tolkienova	Tolkienův	k2eAgNnSc2d1	Tolkienovo
chápání	chápání	k1gNnSc2	chápání
stvoření	stvoření	k1gNnSc2	stvoření
-	-	kIx~	-
článek	článek	k1gInSc1	článek
S.	S.	kA	S.
<g/>
D.	D.	kA	D.
Greydanuse	Greydanuse	k1gFnSc1	Greydanuse
<g/>
:	:	kIx,	:
Tolkien	Tolkien	k1gInSc1	Tolkien
katolícky	katolícka	k1gFnSc2	katolícka
</s>
</p>
