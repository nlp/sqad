<s>
Piešťany	Piešťany	k1gInPc1	Piešťany
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Pistyan	Pistyan	k1gInSc1	Pistyan
<g/>
,	,	kIx,	,
zřídka	zřídka	k6eAd1	zřídka
Bad	Bad	k1gMnSc1	Bad
Püschtin	Püschtin	k1gMnSc1	Püschtin
<g/>
,	,	kIx,	,
maďarsky	maďarsky	k6eAd1	maďarsky
Pöstyén	Pöstyén	k1gInSc1	Pöstyén
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
Pieszczany	Pieszczana	k1gFnSc2	Pieszczana
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
lázeňské	lázeňský	k2eAgNnSc1d1	lázeňské
město	město	k1gNnSc1	město
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Váh	Váh	k1gInSc4	Váh
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Trnavském	trnavský	k2eAgInSc6d1	trnavský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mezinárodním	mezinárodní	k2eAgNnSc7d1	mezinárodní
střediskem	středisko	k1gNnSc7	středisko
léčby	léčba	k1gFnSc2	léčba
revmatických	revmatický	k2eAgFnPc2d1	revmatická
chorob	choroba	k1gFnPc2	choroba
a	a	k8xC	a
významním	významní	k2eAgNnSc7d1	významní
regionálním	regionální	k2eAgNnSc7d1	regionální
centrem	centrum	k1gNnSc7	centrum
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
sportu	sport	k1gInSc2	sport
a	a	k8xC	a
rekreace	rekreace	k1gFnSc2	rekreace
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
spjatá	spjatý	k2eAgFnSc1d1	spjatá
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgFnPc7	svůj
lázněmi	lázeň	k1gFnPc7	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgNnSc1d1	lidské
osídlení	osídlení	k1gNnSc1	osídlení
zde	zde	k6eAd1	zde
existuje	existovat	k5eAaImIp3nS	existovat
již	již	k6eAd1	již
od	od	k7c2	od
prehistorických	prehistorický	k2eAgFnPc2d1	prehistorická
dob	doba	k1gFnPc2	doba
<g/>
;	;	kIx,	;
lidé	člověk	k1gMnPc1	člověk
sem	sem	k6eAd1	sem
přicházeli	přicházet	k5eAaImAgMnP	přicházet
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
minerálním	minerální	k2eAgInPc3d1	minerální
pramenům	pramen	k1gInPc3	pramen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
nezamrzaly	zamrzat	k5eNaImAgFnP	zamrzat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
pochází	pocházet	k5eAaImIp3nS	pocházet
Moravanská	Moravanský	k2eAgFnSc1d1	Moravanská
venuše	venuše	k1gFnSc1	venuše
<g/>
,	,	kIx,	,
malá	malý	k2eAgFnSc1d1	malá
soška	soška	k1gFnSc1	soška
podobná	podobný	k2eAgFnSc1d1	podobná
Věstonické	věstonický	k2eAgFnSc3d1	Věstonická
venuši	venuše	k1gFnSc3	venuše
<g/>
.	.	kIx.	.
</s>
<s>
Nalezena	nalezen	k2eAgFnSc1d1	nalezena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Moravany	Moravan	k1gMnPc4	Moravan
nad	nad	k7c4	nad
Váhom	Váhom	k1gInSc4	Váhom
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
Piešťan	Piešťany	k1gInPc2	Piešťany
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1113	[number]	k4	1113
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
neslo	nést	k5eAaImAgNnS	nést
název	název	k1gInSc4	název
Pescan	Pescany	k1gInPc2	Pescany
a	a	k8xC	a
skládalo	skládat	k5eAaImAgNnS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
několika	několik	k4yIc2	několik
menších	malý	k2eAgFnPc2d2	menší
osad	osada	k1gFnPc2	osada
<g/>
.	.	kIx.	.
</s>
<s>
Nacházel	nacházet	k5eAaImAgInS	nacházet
sa	sa	k?	sa
tady	tady	k6eAd1	tady
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
klášter	klášter	k1gInSc4	klášter
Johanitů	johanita	k1gMnPc2	johanita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
místní	místní	k2eAgInPc4d1	místní
minerální	minerální	k2eAgInPc4d1	minerální
prameny	pramen	k1gInPc4	pramen
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
poukázáno	poukázat	k5eAaPmNgNnS	poukázat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
"	"	kIx"	"
<g/>
De	De	k?	De
admirandis	admirandis	k1gInSc1	admirandis
Hungariae	Hungariae	k1gInSc1	Hungariae
aquis	aquis	k1gInSc1	aquis
hypomnemation	hypomnemation	k1gInSc4	hypomnemation
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
O	o	k7c6	o
zázračných	zázračný	k2eAgFnPc6d1	zázračná
vodách	voda	k1gFnPc6	voda
Uherské	uherský	k2eAgFnSc2d1	uherská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
)	)	kIx)	)
od	od	k7c2	od
autora	autor	k1gMnSc2	autor
Georgia	Georgius	k1gMnSc2	Georgius
Wernhera	Wernher	k1gMnSc2	Wernher
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1549	[number]	k4	1549
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
ohrožováno	ohrožovat	k5eAaImNgNnS	ohrožovat
Tureckými	turecký	k2eAgInPc7d1	turecký
výboji	výboj	k1gInPc7	výboj
a	a	k8xC	a
protihabsburskými	protihabsburský	k2eAgNnPc7d1	protihabsburské
povstáními	povstání	k1gNnPc7	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Piešťany	Piešťany	k1gInPc1	Piešťany
až	až	k9	až
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
rodina	rodina	k1gFnSc1	rodina
Erdődyovců	Erdődyovec	k1gMnPc2	Erdődyovec
<g/>
;	;	kIx,	;
lázně	lázeň	k1gFnSc2	lázeň
jim	on	k3xPp3gMnPc3	on
patřily	patřit	k5eAaImAgInP	patřit
až	až	k9	až
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
rod	rod	k1gInSc4	rod
zde	zde	k6eAd1	zde
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
první	první	k4xOgFnPc4	první
lázně	lázeň	k1gFnPc4	lázeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1778	[number]	k4	1778
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1813	[number]	k4	1813
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
poškozené	poškozený	k2eAgInPc1d1	poškozený
povodní	povodeň	k1gFnSc7	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
přestavěny	přestavět	k5eAaPmNgFnP	přestavět
v	v	k7c6	v
neoklasicistním	neoklasicistní	k2eAgInSc6d1	neoklasicistní
slohu	sloh	k1gInSc6	sloh
a	a	k8xC	a
pojmenovány	pojmenován	k2eAgFnPc4d1	pojmenována
po	po	k7c6	po
Napoleonovi	Napoleon	k1gMnSc6	Napoleon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
si	se	k3xPyFc3	se
je	on	k3xPp3gNnSc4	on
pronajala	pronajmout	k5eAaPmAgFnS	pronajmout
rodina	rodina	k1gFnSc1	rodina
Winterů	Winter	k1gMnPc2	Winter
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
světově	světově	k6eAd1	světově
proslulé	proslulý	k2eAgFnPc4d1	proslulá
lázně	lázeň	k1gFnPc4	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byly	být	k5eAaImAgInP	být
znárodněny	znárodnit	k5eAaPmNgInP	znárodnit
a	a	k8xC	a
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
rozšířeny	rozšířit	k5eAaPmNgInP	rozšířit
<g/>
;	;	kIx,	;
i	i	k8xC	i
město	město	k1gNnSc1	město
samotné	samotný	k2eAgNnSc1d1	samotné
se	se	k3xPyFc4	se
rozrůstalo	rozrůstat	k5eAaImAgNnS	rozrůstat
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
jsou	být	k5eAaImIp3nP	být
Piešťany	Piešťany	k1gInPc1	Piešťany
znovu	znovu	k6eAd1	znovu
okresním	okresní	k2eAgNnSc7d1	okresní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
středověku	středověk	k1gInSc2	středověk
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
jen	jen	k9	jen
zřícenina	zřícenina	k1gFnSc1	zřícenina
kostela	kostel	k1gInSc2	kostel
a	a	k8xC	a
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
světských	světský	k2eAgFnPc2d1	světská
budov	budova	k1gFnPc2	budova
je	být	k5eAaImIp3nS	být
Kurhotel	Kurhotel	k1gInSc1	Kurhotel
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
renesanční	renesanční	k2eAgNnSc1d1	renesanční
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
vícekrát	vícekrát	k6eAd1	vícekrát
přestavované	přestavovaný	k2eAgFnPc1d1	přestavovaná
až	až	k9	až
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
podoby	podoba	k1gFnSc2	podoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
patří	patřit	k5eAaImIp3nS	patřit
Slovenským	slovenský	k2eAgInSc7d1	slovenský
liečebným	liečebný	k2eAgInSc7d1	liečebný
kúpeľom	kúpeľom	k1gInSc4	kúpeľom
Piešťany	Piešťany	k1gInPc1	Piešťany
<g/>
.	.	kIx.	.
</s>
<s>
Kolonádový	kolonádový	k2eAgInSc1d1	kolonádový
most	most	k1gInSc1	most
od	od	k7c2	od
Emila	Emil	k1gMnSc2	Emil
Belluše	Belluše	k1gFnSc2	Belluše
je	být	k5eAaImIp3nS	být
významnou	významný	k2eAgFnSc7d1	významná
funkcionalistickou	funkcionalistický	k2eAgFnSc7d1	funkcionalistická
stavbou	stavba	k1gFnSc7	stavba
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1930	[number]	k4	1930
<g/>
-	-	kIx~	-
<g/>
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
sochou	socha	k1gFnSc7	socha
barlolamača	barlolamačum	k1gNnSc2	barlolamačum
od	od	k7c2	od
R.	R.	kA	R.
Kühmayera	Kühmayera	k1gFnSc1	Kühmayera
a	a	k8xC	a
leptami	lepta	k1gFnPc7	lepta
do	do	k7c2	do
skla	sklo	k1gNnSc2	sklo
od	od	k7c2	od
Martina	Martin	k1gInSc2	Martin
Benky	Benka	k1gFnSc2	Benka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nachází	nacházet	k5eAaImIp3nS	nacházet
Balneologické	balneologický	k2eAgNnSc1d1	balneologické
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
nedaleko	daleko	k6eNd1	daleko
na	na	k7c6	na
Váhu	Váh	k1gInSc6	Váh
pak	pak	k6eAd1	pak
leží	ležet	k5eAaImIp3nS	ležet
přehradní	přehradní	k2eAgFnSc1d1	přehradní
nádrž	nádrž	k1gFnSc1	nádrž
Sĺňava	Sĺňava	k1gFnSc1	Sĺňava
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
také	také	k9	také
umístěno	umístěn	k2eAgNnSc1d1	umístěno
slovenské	slovenský	k2eAgNnSc1d1	slovenské
Vojenské	vojenský	k2eAgNnSc1d1	vojenské
historické	historický	k2eAgNnSc1d1	historické
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svojí	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
na	na	k7c4	na
Váhu	váha	k1gFnSc4	váha
mají	mít	k5eAaImIp3nP	mít
Piešťany	Piešťany	k1gInPc4	Piešťany
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgNnSc4d1	dobré
dopravní	dopravní	k2eAgNnSc4d1	dopravní
spojení	spojení	k1gNnSc4	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Prochází	procházet	k5eAaImIp3nS	procházet
tudy	tudy	k6eAd1	tudy
jak	jak	k8xS	jak
dálnice	dálnice	k1gFnSc1	dálnice
D	D	kA	D
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zároveň	zároveň	k6eAd1	zároveň
hlavní	hlavní	k2eAgInSc1d1	hlavní
železniční	železniční	k2eAgInSc1d1	železniční
tah	tah	k1gInSc1	tah
mezi	mezi	k7c7	mezi
Bratislavou	Bratislava	k1gFnSc7	Bratislava
a	a	k8xC	a
Žilinou	Žilina	k1gFnSc7	Žilina
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
svoje	svůj	k3xOyFgNnSc4	svůj
letiště	letiště	k1gNnSc4	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Krasko	Krasko	k1gNnSc4	Krasko
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
-	-	kIx~	-
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
a	a	k8xC	a
překladatel	překladatel	k1gMnSc1	překladatel
Samuel	Samuel	k1gMnSc1	Samuel
David	David	k1gMnSc1	David
Ungar	Ungar	k1gMnSc1	Ungar
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
-	-	kIx~	-
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
roš	roš	k?	roš
ješiva	ješivo	k1gNnSc2	ješivo
nitranské	nitranský	k2eAgFnSc2d1	Nitranská
ješivy	ješiva	k1gFnSc2	ješiva
<g/>
,	,	kIx,	,
nitranský	nitranský	k2eAgMnSc1d1	nitranský
rabín	rabín	k1gMnSc1	rabín
<g/>
,	,	kIx,	,
pohřben	pohřben	k2eAgMnSc1d1	pohřben
v	v	k7c6	v
Piešťanech	Piešťany	k1gInPc6	Piešťany
Ivan	Ivan	k1gMnSc1	Ivan
Stodola	stodola	k1gFnSc1	stodola
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
-	-	kIx~	-
1977	[number]	k4	1977
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Martin	Martin	k1gMnSc1	Martin
Benka	Benka	k1gMnSc1	Benka
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
-	-	kIx~	-
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
Janko	Janko	k1gMnSc1	Janko
Alexy	Alexa	k1gMnSc2	Alexa
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
-	-	kIx~	-
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
Miloš	Miloš	k1gMnSc1	Miloš
Alexander	Alexandra	k1gFnPc2	Alexandra
Bazovský	Bazovský	k1gMnSc1	Bazovský
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
-	-	kIx~	-
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
zakladatel	zakladatel	k1gMnSc1	zakladatel
slovenské	slovenský	k2eAgFnSc2d1	slovenská
výtvarné	výtvarný	k2eAgFnSc2d1	výtvarná
moderny	moderna	k1gFnSc2	moderna
Ernő	Ernő	k1gMnSc1	Ernő
Rubik	Rubik	k1gMnSc1	Rubik
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
-	-	kIx~	-
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
maďarský	maďarský	k2eAgMnSc1d1	maďarský
letecký	letecký	k2eAgMnSc1d1	letecký
konstruktér	konstruktér	k1gMnSc1	konstruktér
Jozef	Jozef	k1gMnSc1	Jozef
Šošoka	Šošoka	k1gMnSc1	Šošoka
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
-	-	kIx~	-
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
džezový	džezový	k2eAgMnSc1d1	džezový
hudebník	hudebník	k1gMnSc1	hudebník
Irena	Irena	k1gFnSc1	Irena
Belohorská	Belohorský	k2eAgFnSc1d1	Belohorská
(	(	kIx(	(
<g/>
*	*	kIx~	*
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékařka	lékařka	k1gFnSc1	lékařka
<g/>
,	,	kIx,	,
politička	politička	k1gFnSc1	politička
a	a	k8xC	a
poslankyně	poslankyně	k1gFnSc1	poslankyně
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
Marián	Marián	k1gMnSc1	Marián
<g />
.	.	kIx.	.
</s>
<s>
Geišberg	Geišberg	k1gInSc1	Geišberg
(	(	kIx(	(
<g/>
*	*	kIx~	*
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
písničkář	písničkář	k1gMnSc1	písničkář
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
humorista	humorista	k1gMnSc1	humorista
Róbert	Róbert	k1gMnSc1	Róbert
Erban	Erban	k1gMnSc1	Erban
(	(	kIx(	(
<g/>
*	*	kIx~	*
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rychlostní	rychlostní	k2eAgMnSc1d1	rychlostní
kajakář	kajakář	k1gMnSc1	kajakář
Martina	Martina	k1gFnSc1	Martina
Moravcová	Moravcová	k1gFnSc1	Moravcová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plavkyně	plavkyně	k1gFnSc1	plavkyně
<g/>
,	,	kIx,	,
a	a	k8xC	a
držitelka	držitelka	k1gFnSc1	držitelka
dvou	dva	k4xCgFnPc2	dva
stříbrných	stříbrný	k2eAgFnPc2d1	stříbrná
olympijských	olympijský	k2eAgFnPc2d1	olympijská
medailí	medaile	k1gFnPc2	medaile
Rytmus	rytmus	k1gInSc1	rytmus
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
*	*	kIx~	*
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
/	/	kIx~	/
<g/>
rapper	rapper	k1gInSc1	rapper
Ego	ego	k1gNnSc2	ego
(	(	kIx(	(
<g/>
*	*	kIx~	*
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
/	/	kIx~	/
<g/>
rapper	rapper	k1gMnSc1	rapper
Marcela	Marcela	k1gFnSc1	Marcela
Erbanová	Erbanová	k1gFnSc1	Erbanová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rychlostní	rychlostní	k2eAgFnSc1d1	rychlostní
kajakářka	kajakářka	k1gFnSc1	kajakářka
Branko	branka	k1gFnSc5	branka
Radivojevič	Radivojevič	k1gInSc4	Radivojevič
(	(	kIx(	(
<g/>
*	*	kIx~	*
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
<g />
.	.	kIx.	.
</s>
<s>
Filip	Filip	k1gMnSc1	Filip
Hološko	Hološka	k1gFnSc5	Hološka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
Lukáš	Lukáš	k1gMnSc1	Lukáš
Lacko	Lacko	k1gNnSc4	Lacko
(	(	kIx(	(
<g/>
*	*	kIx~	*
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenista	tenista	k1gMnSc1	tenista
Dominika	Dominik	k1gMnSc2	Dominik
Cibulková	Cibulková	k1gFnSc1	Cibulková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tenistka	tenistka	k1gFnSc1	tenistka
Budapešť	Budapešť	k1gFnSc1	Budapešť
(	(	kIx(	(
<g/>
IX	IX	kA	IX
<g/>
.	.	kIx.	.
obvod	obvod	k1gInSc1	obvod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
Eilat	Eilat	k1gInSc1	Eilat
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
Hajdúnánás	Hajdúnánás	k1gInSc1	Hajdúnánás
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
Heinola	Heinola	k1gFnSc1	Heinola
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
Luhačovice	Luhačovice	k1gFnPc1	Luhačovice
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
Poděbrady	Poděbrady	k1gInPc4	Poděbrady
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
Ustroń	Ustroń	k1gFnSc1	Ustroń
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Varaždinske	Varaždinsk	k1gFnSc2	Varaždinsk
Toplice	Toplice	k1gFnSc2	Toplice
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
</s>
