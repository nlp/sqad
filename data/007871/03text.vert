<s>
Kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
je	být	k5eAaImIp3nS	být
moderní	moderní	k2eAgFnSc1d1	moderní
trojlodní	trojlodní	k2eAgFnSc1d1	trojlodní
sakrální	sakrální	k2eAgFnSc1d1	sakrální
stavba	stavba	k1gFnSc1	stavba
stojící	stojící	k2eAgFnSc1d1	stojící
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Svatopluka	Svatopluk	k1gMnSc2	Svatopluk
Čecha	Čech	k1gMnSc2	Čech
v	v	k7c6	v
Praze-Vršovicích	Praze-Vršovice	k1gFnPc6	Praze-Vršovice
<g/>
.	.	kIx.	.
</s>
<s>
Železobetonový	železobetonový	k2eAgInSc4d1	železobetonový
kostel	kostel	k1gInSc4	kostel
postavený	postavený	k2eAgInSc4d1	postavený
v	v	k7c6	v
letech	let	k1gInPc6	let
1929	[number]	k4	1929
<g/>
–	–	k?	–
<g/>
1930	[number]	k4	1930
podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
architekta	architekt	k1gMnSc2	architekt
Josefa	Josef	k1gMnSc2	Josef
Gočára	Gočár	k1gMnSc2	Gočár
je	být	k5eAaImIp3nS	být
odborníky	odborník	k1gMnPc4	odborník
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejzdařilejších	zdařilý	k2eAgFnPc2d3	nejzdařilejší
funkcionalistických	funkcionalistický	k2eAgFnPc2d1	funkcionalistická
církevních	církevní	k2eAgFnPc2d1	církevní
staveb	stavba	k1gFnPc2	stavba
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
měly	mít	k5eAaImAgFnP	mít
Vršovice	Vršovice	k1gFnPc1	Vršovice
13	[number]	k4	13
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
převážně	převážně	k6eAd1	převážně
katolického	katolický	k2eAgNnSc2d1	katolické
náboženství	náboženství	k1gNnSc2	náboženství
a	a	k8xC	a
kapacita	kapacita	k1gFnSc1	kapacita
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Mikuláše	mikuláš	k1gInSc2	mikuláš
ani	ani	k8xC	ani
po	po	k7c6	po
přestavbě	přestavba	k1gFnSc6	přestavba
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc4	rozšíření
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
nestačila	stačit	k5eNaBmAgFnS	stačit
poptávce	poptávka	k1gFnSc3	poptávka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
povýšení	povýšení	k1gNnSc2	povýšení
obce	obec	k1gFnSc2	obec
na	na	k7c4	na
město	město	k1gNnSc4	město
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1902	[number]	k4	1902
se	se	k3xPyFc4	se
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
vršovický	vršovický	k2eAgMnSc1d1	vršovický
farář	farář	k1gMnSc1	farář
František	František	k1gMnSc1	František
Dusil	dusit	k5eAaImAgMnS	dusit
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
založit	založit	k5eAaPmF	založit
Spolek	spolek	k1gInSc1	spolek
pro	pro	k7c4	pro
zbudování	zbudování	k1gNnSc4	zbudování
nového	nový	k2eAgInSc2d1	nový
katolického	katolický	k2eAgInSc2d1	katolický
chrámu	chrám	k1gInSc2	chrám
Páně	páně	k2eAgInSc2d1	páně
ve	v	k7c6	v
Vršovicích	Vršovice	k1gFnPc6	Vršovice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
první	první	k4xOgFnSc6	první
valné	valný	k2eAgFnSc6d1	valná
hromadě	hromada	k1gFnSc6	hromada
spolku	spolek	k1gInSc2	spolek
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1902	[number]	k4	1902
byly	být	k5eAaImAgFnP	být
schváleny	schválit	k5eAaPmNgFnP	schválit
stanovy	stanova	k1gFnPc1	stanova
a	a	k8xC	a
Dusil	dusit	k5eAaImAgMnS	dusit
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
budoucí	budoucí	k2eAgInSc1d1	budoucí
svatostánek	svatostánek	k1gInSc1	svatostánek
byl	být	k5eAaImAgInS	být
ukázkou	ukázka	k1gFnSc7	ukázka
moderní	moderní	k2eAgFnSc2d1	moderní
české	český	k2eAgFnSc2d1	Česká
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Protektorem	protektor	k1gInSc7	protektor
spolku	spolek	k1gInSc2	spolek
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
pražský	pražský	k2eAgMnSc1d1	pražský
kardinál	kardinál	k1gMnSc1	kardinál
Lev	Lev	k1gMnSc1	Lev
Skrbenský	Skrbenský	k2eAgMnSc1d1	Skrbenský
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
členských	členský	k2eAgInPc2d1	členský
příspěvků	příspěvek	k1gInPc2	příspěvek
a	a	k8xC	a
sbírek	sbírka	k1gFnPc2	sbírka
získal	získat	k5eAaPmAgInS	získat
spolek	spolek	k1gInSc1	spolek
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
peněz	peníze	k1gInPc2	peníze
potřebných	potřebný	k2eAgInPc2d1	potřebný
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
díky	díky	k7c3	díky
úspěšné	úspěšný	k2eAgFnSc3d1	úspěšná
investici	investice	k1gFnSc3	investice
do	do	k7c2	do
pozemků	pozemek	k1gInPc2	pozemek
v	v	k7c6	v
sousedních	sousední	k2eAgFnPc6d1	sousední
Záběhlicích	Záběhlice	k1gFnPc6	Záběhlice
<g/>
.	.	kIx.	.
</s>
<s>
Dusil	dusit	k5eAaImAgMnS	dusit
je	být	k5eAaImIp3nS	být
koupil	koupit	k5eAaPmAgMnS	koupit
pro	pro	k7c4	pro
spolek	spolek	k1gInSc4	spolek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
a	a	k8xC	a
v	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jejich	jejich	k3xOp3gFnSc1	jejich
cena	cena	k1gFnSc1	cena
prudce	prudko	k6eAd1	prudko
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
prodal	prodat	k5eAaPmAgMnS	prodat
<g/>
.	.	kIx.	.
</s>
<s>
Budoucí	budoucí	k2eAgInSc1d1	budoucí
kostel	kostel	k1gInSc1	kostel
měl	mít	k5eAaImAgInS	mít
původně	původně	k6eAd1	původně
stát	stát	k5eAaImF	stát
na	na	k7c6	na
pozemku	pozemek	k1gInSc6	pozemek
vedle	vedle	k7c2	vedle
Vršovického	vršovický	k2eAgInSc2d1	vršovický
zámečku	zámeček	k1gInSc2	zámeček
(	(	kIx(	(
<g/>
Rangherky	Rangherka	k1gFnSc2	Rangherka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalé	bývalý	k2eAgFnSc2d1	bývalá
továrny	továrna	k1gFnSc2	továrna
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
hedvábí	hedvábí	k1gNnSc2	hedvábí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
park	park	k1gInSc1	park
a	a	k8xC	a
spolek	spolek	k1gInSc1	spolek
musel	muset	k5eAaImAgInS	muset
najít	najít	k5eAaPmF	najít
jiné	jiný	k2eAgNnSc4d1	jiné
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
záhy	záhy	k6eAd1	záhy
nalezeno	nalézt	k5eAaBmNgNnS	nalézt
na	na	k7c6	na
starém	starý	k2eAgInSc6d1	starý
zádušním	zádušní	k2eAgInSc6d1	zádušní
hřbitově	hřbitov	k1gInSc6	hřbitov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
rozkládal	rozkládat	k5eAaImAgInS	rozkládat
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgMnSc2d1	dnešní
Čechova	Čechov	k1gMnSc2	Čechov
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
přestalo	přestat	k5eAaPmAgNnS	přestat
pohřbívat	pohřbívat	k5eAaImF	pohřbívat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
<g/>
.	.	kIx.	.
</s>
<s>
Plán	plán	k1gInSc1	plán
v	v	k7c6	v
pseudorenesančním	pseudorenesanční	k2eAgInSc6d1	pseudorenesanční
stylu	styl	k1gInSc6	styl
pro	pro	k7c4	pro
původní	původní	k2eAgFnSc4d1	původní
lokalitu	lokalita	k1gFnSc4	lokalita
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
místní	místní	k2eAgMnSc1d1	místní
stavitel	stavitel	k1gMnSc1	stavitel
Bohumil	Bohumil	k1gMnSc1	Bohumil
Hrabě	Hrabě	k1gMnSc1	Hrabě
<g/>
.	.	kIx.	.
</s>
<s>
Plány	plán	k1gInPc1	plán
pro	pro	k7c4	pro
nové	nový	k2eAgNnSc4d1	nové
místo	místo	k1gNnSc4	místo
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
profesor	profesor	k1gMnSc1	profesor
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
inženýr	inženýr	k1gMnSc1	inženýr
Václav	Václav	k1gMnSc1	Václav
Materka	Materka	k1gFnSc1	Materka
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
budovy	budova	k1gFnSc2	budova
v	v	k7c6	v
renesančním	renesanční	k2eAgInSc6d1	renesanční
stylu	styl	k1gInSc6	styl
obsahující	obsahující	k2eAgInSc4d1	obsahující
například	například	k6eAd1	například
kopii	kopie	k1gFnSc4	kopie
kupole	kupole	k1gFnSc2	kupole
chrámu	chrám	k1gInSc2	chrám
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
se	se	k3xPyFc4	se
však	však	k9	však
ukázal	ukázat	k5eAaPmAgInS	ukázat
být	být	k5eAaImF	být
nepoužitelný	použitelný	k2eNgInSc1d1	nepoužitelný
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
velkou	velký	k2eAgFnSc4d1	velká
nákladnost	nákladnost	k1gFnSc4	nákladnost
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
1	[number]	k4	1
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
musel	muset	k5eAaImAgInS	muset
spolek	spolek	k1gInSc1	spolek
svoje	svůj	k3xOyFgFnPc4	svůj
aktivity	aktivita	k1gFnPc4	aktivita
přerušit	přerušit	k5eAaPmF	přerušit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
své	svůj	k3xOyFgInPc4	svůj
plány	plán	k1gInPc4	plán
navázal	navázat	k5eAaPmAgInS	navázat
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
Arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
František	František	k1gMnSc1	František
Kordač	Kordač	k1gMnSc1	Kordač
pověřil	pověřit	k5eAaPmAgMnS	pověřit
nastupujícího	nastupující	k2eAgMnSc4d1	nastupující
faráře	farář	k1gMnSc4	farář
Stanislava	Stanislav	k1gMnSc4	Stanislav
Pilíka	Pilík	k1gMnSc4	Pilík
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
postaral	postarat	k5eAaPmAgMnS	postarat
o	o	k7c4	o
realizaci	realizace	k1gFnSc4	realizace
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Architekt	architekt	k1gMnSc1	architekt
Materka	Materka	k1gFnSc1	Materka
zpracoval	zpracovat	k5eAaPmAgInS	zpracovat
dokumentaci	dokumentace	k1gFnSc3	dokumentace
železobetonové	železobetonový	k2eAgFnSc2d1	železobetonová
stavby	stavba	k1gFnSc2	stavba
a	a	k8xC	a
plány	plán	k1gInPc1	plán
byly	být	k5eAaImAgInP	být
předloženy	předložit	k5eAaPmNgInP	předložit
arcibiskupské	arcibiskupský	k2eAgFnSc3d1	arcibiskupská
konzistoři	konzistoř	k1gFnSc3	konzistoř
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1927	[number]	k4	1927
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
vypsat	vypsat	k5eAaPmF	vypsat
novou	nový	k2eAgFnSc4d1	nová
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
architektonickou	architektonický	k2eAgFnSc4d1	architektonická
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1927	[number]	k4	1927
vybírala	vybírat	k5eAaImAgFnS	vybírat
z	z	k7c2	z
52	[number]	k4	52
návrhů	návrh	k1gInPc2	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Jednoznačný	jednoznačný	k2eAgMnSc1d1	jednoznačný
vítěz	vítěz	k1gMnSc1	vítěz
nebyl	být	k5eNaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
užšího	úzký	k2eAgNnSc2d2	užší
finále	finále	k1gNnSc2	finále
byli	být	k5eAaImAgMnP	být
pozváni	pozván	k2eAgMnPc1d1	pozván
architekti	architekt	k1gMnPc1	architekt
Gočár	Gočár	k1gMnSc1	Gočár
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Kabeš	Kabeš	k1gMnSc1	Kabeš
a	a	k8xC	a
Vladimír	Vladimír	k1gMnSc1	Vladimír
Bolech	bol	k1gInPc6	bol
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
byla	být	k5eAaImAgFnS	být
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1928	[number]	k4	1928
vybrána	vybrán	k2eAgFnSc1d1	vybrána
varianta	varianta	k1gFnSc1	varianta
Josefa	Josef	k1gMnSc2	Josef
Gočára	Gočár	k1gMnSc2	Gočár
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
vítězného	vítězný	k2eAgInSc2d1	vítězný
projektu	projekt	k1gInSc2	projekt
byl	být	k5eAaImAgInS	být
podán	podat	k5eAaPmNgInS	podat
návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
zrušení	zrušení	k1gNnSc4	zrušení
hřbitova	hřbitov	k1gInSc2	hřbitov
a	a	k8xC	a
uložení	uložení	k1gNnSc2	uložení
exhumovaných	exhumovaný	k2eAgInPc2d1	exhumovaný
ostatků	ostatek	k1gInPc2	ostatek
do	do	k7c2	do
pohřební	pohřební	k2eAgFnSc2d1	pohřební
krypty	krypta	k1gFnSc2	krypta
pod	pod	k7c7	pod
pohřební	pohřební	k2eAgFnSc7d1	pohřební
kaplí	kaple	k1gFnSc7	kaple
budoucího	budoucí	k2eAgInSc2d1	budoucí
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Blížící	blížící	k2eAgMnSc1d1	blížící
se	se	k3xPyFc4	se
svatováclavské	svatováclavský	k2eAgNnSc1d1	Svatováclavské
milénium	milénium	k1gNnSc1	milénium
uspíšilo	uspíšit	k5eAaPmAgNnS	uspíšit
realizaci	realizace	k1gFnSc4	realizace
projektu	projekt	k1gInSc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
položen	položit	k5eAaPmNgInS	položit
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1929	[number]	k4	1929
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
strahovského	strahovský	k2eAgMnSc2d1	strahovský
opata	opat	k1gMnSc2	opat
Metoda	Metod	k1gMnSc2	Metod
Jana	Jan	k1gMnSc2	Jan
Zavorala	Zavoral	k1gMnSc2	Zavoral
a	a	k8xC	a
první	první	k4xOgInSc4	první
výkop	výkop	k1gInSc4	výkop
provedl	provést	k5eAaPmAgMnS	provést
světící	světící	k2eAgMnSc1d1	světící
biskup	biskup	k1gMnSc1	biskup
Antonín	Antonín	k1gMnSc1	Antonín
Podlaha	podlaha	k1gFnSc1	podlaha
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc4	chrám
vystavěli	vystavět	k5eAaPmAgMnP	vystavět
pracovníci	pracovník	k1gMnPc1	pracovník
stavební	stavební	k2eAgFnSc2d1	stavební
firmy	firma	k1gFnSc2	firma
inženýra	inženýr	k1gMnSc2	inženýr
Strnada	Strnad	k1gMnSc2	Strnad
za	za	k7c4	za
11	[number]	k4	11
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Kostel	kostel	k1gInSc1	kostel
byl	být	k5eAaImAgInS	být
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
pražským	pražský	k2eAgMnSc7d1	pražský
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
Františkem	František	k1gMnSc7	František
Kordačem	Kordač	k1gMnSc7	Kordač
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
Gočárův	Gočárův	k2eAgInSc1d1	Gočárův
projekt	projekt	k1gInSc1	projekt
důvtipně	důvtipně	k6eAd1	důvtipně
počítal	počítat	k5eAaImAgInS	počítat
se	s	k7c7	s
svažujícím	svažující	k2eAgMnSc7d1	svažující
se	se	k3xPyFc4	se
terénem	terén	k1gInSc7	terén
<g/>
.	.	kIx.	.
</s>
<s>
Vstupní	vstupní	k2eAgNnSc1d1	vstupní
schodiště	schodiště	k1gNnSc1	schodiště
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
šíři	šíř	k1gFnSc6	šíř
kostela	kostel	k1gInSc2	kostel
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
do	do	k7c2	do
otevřené	otevřený	k2eAgFnSc2d1	otevřená
předsíně	předsíň	k1gFnSc2	předsíň
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
vchod	vchod	k1gInSc1	vchod
do	do	k7c2	do
chrámové	chrámový	k2eAgFnSc2d1	chrámová
lodi	loď	k1gFnSc2	loď
je	být	k5eAaImIp3nS	být
trojdílný	trojdílný	k2eAgInSc1d1	trojdílný
<g/>
.	.	kIx.	.
</s>
<s>
Půlkruhové	půlkruhový	k2eAgNnSc1d1	půlkruhové
kněžiště	kněžiště	k1gNnSc1	kněžiště
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
trojlodní	trojlodní	k2eAgFnSc4d1	trojlodní
stavbu	stavba	k1gFnSc4	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Podélný	podélný	k2eAgInSc4d1	podélný
půdorys	půdorys	k1gInSc4	půdorys
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
hlavnímu	hlavní	k2eAgInSc3d1	hlavní
oltáři	oltář	k1gInSc3	oltář
mírně	mírně	k6eAd1	mírně
zužuje	zužovat	k5eAaImIp3nS	zužovat
a	a	k8xC	a
intenzita	intenzita	k1gFnSc1	intenzita
světla	světlo	k1gNnSc2	světlo
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zesiluje	zesilovat	k5eAaImIp3nS	zesilovat
<g/>
.	.	kIx.	.
</s>
<s>
Kněžiště	kněžiště	k1gNnSc1	kněžiště
a	a	k8xC	a
boční	boční	k2eAgFnPc1d1	boční
lodě	loď	k1gFnPc1	loď
osvětlují	osvětlovat	k5eAaImIp3nP	osvětlovat
široká	široký	k2eAgNnPc4d1	široké
okna	okno	k1gNnPc4	okno
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
železobetonové	železobetonový	k2eAgFnSc3d1	železobetonová
konstrukci	konstrukce	k1gFnSc3	konstrukce
nemusely	muset	k5eNaImAgFnP	muset
být	být	k5eAaImF	být
boční	boční	k2eAgFnPc1d1	boční
lodě	loď	k1gFnPc1	loď
odděleny	oddělit	k5eAaPmNgFnP	oddělit
od	od	k7c2	od
hlavní	hlavní	k2eAgFnSc2d1	hlavní
pomocí	pomoc	k1gFnSc7	pomoc
sloupů	sloup	k1gInPc2	sloup
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
kříž	kříž	k1gInSc1	kříž
na	na	k7c6	na
hlavním	hlavní	k2eAgInSc6d1	hlavní
oltáři	oltář	k1gInSc6	oltář
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Čeněk	Čeněk	k1gMnSc1	Čeněk
Vosmík	Vosmík	k1gMnSc1	Vosmík
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Kaplický	Kaplický	k2eAgMnSc1d1	Kaplický
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
okno	okno	k1gNnSc4	okno
s	s	k7c7	s
vitráží	vitráž	k1gFnSc7	vitráž
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
presbytáře	presbytář	k1gInSc2	presbytář
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
znázorněn	znázorněn	k2eAgMnSc1d1	znázorněn
svatý	svatý	k2eAgMnSc1d1	svatý
Václav	Václav	k1gMnSc1	Václav
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
sluha	sluha	k1gMnSc1	sluha
Podiven	podiven	k2eAgMnSc1d1	podiven
a	a	k8xC	a
Radslav	Radslav	k1gMnSc1	Radslav
Zlický	zlický	k2eAgMnSc1d1	zlický
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
levého	levý	k2eAgInSc2d1	levý
postranního	postranní	k2eAgInSc2d1	postranní
oltáře	oltář	k1gInSc2	oltář
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
je	být	k5eAaImIp3nS	být
Karel	Karel	k1gMnSc1	Karel
Pokorný	Pokorný	k1gMnSc1	Pokorný
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
němu	on	k3xPp3gInSc3	on
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
oltář	oltář	k1gInSc1	oltář
Srdce	srdce	k1gNnSc2	srdce
Páně	páně	k2eAgNnSc2d1	páně
od	od	k7c2	od
Josefa	Josef	k1gMnSc4	Josef
Kubíčka	Kubíček	k1gMnSc2	Kubíček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
boční	boční	k2eAgFnSc6d1	boční
lodi	loď	k1gFnSc6	loď
je	být	k5eAaImIp3nS	být
oltář	oltář	k1gInSc1	oltář
sv.	sv.	kA	sv.
Josefa	Josef	k1gMnSc4	Josef
od	od	k7c2	od
stejného	stejný	k2eAgMnSc2d1	stejný
autora	autor	k1gMnSc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Reliéf	reliéf	k1gInSc1	reliéf
na	na	k7c6	na
Oltáři	Oltář	k1gInSc6	Oltář
českých	český	k2eAgMnPc2d1	český
patronů	patron	k1gMnPc2	patron
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
pravé	pravý	k2eAgFnSc6d1	pravá
boční	boční	k2eAgFnSc6d1	boční
lodi	loď	k1gFnSc6	loď
<g/>
,	,	kIx,	,
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Čeněk	Čeněk	k1gMnSc1	Čeněk
Vosmík	Vosmík	k1gMnSc1	Vosmík
<g/>
.	.	kIx.	.
</s>
<s>
Křížová	křížový	k2eAgFnSc1d1	křížová
cesta	cesta	k1gFnSc1	cesta
z	z	k7c2	z
opuky	opuka	k1gFnSc2	opuka
lemující	lemující	k2eAgFnSc2d1	lemující
stěny	stěna	k1gFnSc2	stěna
je	být	k5eAaImIp3nS	být
dílem	dílo	k1gNnSc7	dílo
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Stefana	Stefan	k1gMnSc2	Stefan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
a	a	k8xC	a
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
kostel	kostel	k1gInSc1	kostel
nově	nově	k6eAd1	nově
vymalován	vymalovat	k5eAaPmNgInS	vymalovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
bylo	být	k5eAaImAgNnS	být
presbyterium	presbyterium	k1gNnSc1	presbyterium
(	(	kIx(	(
<g/>
kněžiště	kněžiště	k1gNnSc1	kněžiště
<g/>
)	)	kIx)	)
doplněno	doplnit	k5eAaPmNgNnS	doplnit
o	o	k7c4	o
oltář	oltář	k1gInSc4	oltář
umístěný	umístěný	k2eAgInSc4d1	umístěný
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
lidu	lid	k1gInSc3	lid
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
kněžištěm	kněžiště	k1gNnSc7	kněžiště
je	být	k5eAaImIp3nS	být
kaple	kaple	k1gFnSc1	kaple
<g/>
,	,	kIx,	,
přístupná	přístupný	k2eAgNnPc4d1	přístupné
bočním	boční	k2eAgInSc7d1	boční
vchodem	vchod	k1gInSc7	vchod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
sakristie	sakristie	k1gFnSc1	sakristie
<g/>
,	,	kIx,	,
zpěvárna	zpěvárna	k1gFnSc1	zpěvárna
a	a	k8xC	a
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
předsíní	předsíň	k1gFnSc7	předsíň
se	se	k3xPyFc4	se
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
56	[number]	k4	56
metrů	metr	k1gInPc2	metr
tyčí	tyčit	k5eAaImIp3nS	tyčit
hranolovitá	hranolovitý	k2eAgFnSc1d1	hranolovitá
věž	věž	k1gFnSc1	věž
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
čelní	čelní	k2eAgFnSc1d1	čelní
stěna	stěna	k1gFnSc1	stěna
je	být	k5eAaImIp3nS	být
zasklena	zasklen	k2eAgFnSc1d1	zasklena
luxfery	luxfera	k1gFnPc1	luxfera
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věži	věž	k1gFnSc6	věž
je	být	k5eAaImIp3nS	být
zvonice	zvonice	k1gFnSc1	zvonice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
vybavena	vybavit	k5eAaPmNgFnS	vybavit
pěti	pět	k4xCc7	pět
zvony	zvon	k1gInPc7	zvon
(	(	kIx(	(
<g/>
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
Ludmila	Ludmila	k1gFnSc1	Ludmila
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
a	a	k8xC	a
umíráček	umíráček	k1gInSc1	umíráček
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvony	zvon	k1gInPc1	zvon
ulité	ulitý	k2eAgFnSc2d1	ulitá
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Pernerem	Perner	k1gMnSc7	Perner
však	však	k9	však
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
zrekvírovány	zrekvírován	k2eAgInPc4d1	zrekvírován
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
nové	nový	k2eAgInPc1d1	nový
zvony	zvon	k1gInPc1	zvon
(	(	kIx(	(
<g/>
Václav	Václav	k1gMnSc1	Václav
a	a	k8xC	a
Ludmila	Ludmila	k1gFnSc1	Ludmila
<g/>
)	)	kIx)	)
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgInP	být
instalovány	instalovat	k5eAaBmNgInP	instalovat
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnSc7	jejich
tvůrcem	tvůrce	k1gMnSc7	tvůrce
je	být	k5eAaImIp3nS	být
rodina	rodina	k1gFnSc1	rodina
Dytrichova	Dytrichův	k2eAgFnSc1d1	Dytrichův
z	z	k7c2	z
Brodku	brodek	k1gInSc2	brodek
u	u	k7c2	u
Přerova	Přerov	k1gInSc2	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
věže	věž	k1gFnSc2	věž
je	být	k5eAaImIp3nS	být
osazen	osadit	k5eAaPmNgInS	osadit
velkým	velký	k2eAgInSc7d1	velký
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
osvětleným	osvětlený	k2eAgInSc7d1	osvětlený
křížem	kříž	k1gInSc7	kříž
ze	z	k7c2	z
žlutého	žlutý	k2eAgInSc2d1	žlutý
opaxitu	opaxit	k1gInSc2	opaxit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
věže	věž	k1gFnSc2	věž
je	být	k5eAaImIp3nS	být
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
podle	podle	k7c2	podle
původního	původní	k2eAgInSc2d1	původní
návrhu	návrh	k1gInSc2	návrh
nacházet	nacházet	k5eAaImF	nacházet
socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
věž	věž	k1gFnSc4	věž
instalována	instalovat	k5eAaBmNgNnP	instalovat
až	až	k9	až
28	[number]	k4	28
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
návrh	návrh	k1gInSc1	návrh
sochy	socha	k1gFnSc2	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
Bedřich	Bedřich	k1gMnSc1	Bedřich
Stefan	Stefan	k1gMnSc1	Stefan
<g/>
.	.	kIx.	.
</s>
<s>
Nadživotní	nadživotní	k2eAgFnSc4d1	nadživotní
podobu	podoba	k1gFnSc4	podoba
sochy	socha	k1gFnPc1	socha
podle	podle	k7c2	podle
modelu	model	k1gInSc2	model
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
akademický	akademický	k2eAgMnSc1d1	akademický
sochař	sochař	k1gMnSc1	sochař
Jan	Jan	k1gMnSc1	Jan
Roith	Roith	k1gMnSc1	Roith
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
tak	tak	k9	tak
vlastně	vlastně	k9	vlastně
stal	stát	k5eAaPmAgMnS	stát
de	de	k?	de
facto	facta	k1gFnSc5	facta
jejím	její	k3xOp3gMnSc7	její
právoplatným	právoplatný	k2eAgMnSc7d1	právoplatný
spoluautorem	spoluautor	k1gMnSc7	spoluautor
<g/>
.	.	kIx.	.
</s>
<s>
Výrobu	výroba	k1gFnSc4	výroba
sochy	socha	k1gFnSc2	socha
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
umístění	umístění	k1gNnSc4	umístění
zaplatila	zaplatit	k5eAaPmAgFnS	zaplatit
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
Praha	Praha	k1gFnSc1	Praha
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
mší	mše	k1gFnPc2	mše
se	se	k3xPyFc4	se
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
konají	konat	k5eAaImIp3nP	konat
koncerty	koncert	k1gInPc1	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
koná	konat	k5eAaImIp3nS	konat
slavnostní	slavnostní	k2eAgNnSc4d1	slavnostní
zakončení	zakončení	k1gNnSc4	zakončení
Dnů	den	k1gInPc2	den
evropského	evropský	k2eAgNnSc2d1	Evropské
dědictví	dědictví	k1gNnSc2	dědictví
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
spoluorganizuje	spoluorganizovat	k5eAaImIp3nS	spoluorganizovat
městská	městský	k2eAgFnSc1d1	městská
část	část	k1gFnSc1	část
Praha	Praha	k1gFnSc1	Praha
10	[number]	k4	10
a	a	k8xC	a
Sdružení	sdružení	k1gNnSc1	sdružení
historických	historický	k2eAgNnPc2d1	historické
sídel	sídlo	k1gNnPc2	sídlo
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
této	tento	k3xDgFnSc2	tento
akce	akce	k1gFnSc2	akce
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
Hudba	hudba	k1gFnSc1	hudba
Hradní	hradní	k2eAgFnSc2d1	hradní
stráže	stráž	k1gFnSc2	stráž
a	a	k8xC	a
Policie	policie	k1gFnSc2	policie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2015	[number]	k4	2015
se	s	k7c7	s
před	před	k7c7	před
kostelem	kostel	k1gInSc7	kostel
konala	konat	k5eAaImAgFnS	konat
vystoupení	vystoupení	k1gNnSc4	vystoupení
hudebních	hudební	k2eAgFnPc2d1	hudební
skupin	skupina	k1gFnPc2	skupina
a	a	k8xC	a
program	program	k1gInSc1	program
pro	pro	k7c4	pro
rodiny	rodina	k1gFnPc4	rodina
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
akce	akce	k1gFnSc1	akce
Zažít	zažít	k5eAaPmF	zažít
město	město	k1gNnSc4	město
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
