<s>
Džomolhari	Džomolhari	k1gNnSc1
(	(	kIx(
<g/>
tibetsky	tibetsky	k6eAd1
ཇ	ཇ	k?
<g/>
ོ	ོ	k?
<g/>
་	་	k?
<g/>
མ	མ	k?
<g/>
ོ	ོ	k?
<g/>
་	་	k?
<g/>
ལ	ལ	k?
<g/>
ྷ	ྷ	k?
<g/>
་	་	k?
<g/>
ར	ར	k?
<g/>
ི	ི	k?
<g/>
,	,	kIx,
čínsky	čínsky	k6eAd1
v	v	k7c6
českém	český	k2eAgInSc6d1
přepisu	přepis	k1gInSc6
Čchuo-mo-la-ž	Čchuo-mo-la-ž	k1gFnSc1
<g/>
’	’	k?
Feng	Fenga	k1gFnPc2
<g/>
,	,	kIx,
pchin-jinem	pchin-jin	k1gMnSc7
Chuò	Chuò	k1gFnPc2
Fē	Fē	k1gInSc1
<g/>
,	,	kIx,
znaky	znak	k1gInPc1
绰	绰	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
hora	hora	k1gFnSc1
v	v	k7c6
Himálaji	Himálaj	k1gFnSc6
na	na	k7c6
hranici	hranice	k1gFnSc6
mezi	mezi	k7c7
Tibetskou	tibetský	k2eAgFnSc7d1
autonomní	autonomní	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
Číny	Čína	k1gFnSc2
a	a	k8xC
Bhútánem	Bhútán	k1gInSc7
<g/>
.	.	kIx.
</s>