<s>
Džomolhari	Džomolhari	k6eAd1
</s>
<s>
Džomolhari	Džomolhari	k1gNnSc1
Džomolhari	Džomolhar	k1gFnSc2
od	od	k7c2
jihu	jih	k1gInSc2
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
7326	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Prominence	prominence	k1gFnSc2
</s>
<s>
2086	#num#	k4
m	m	kA
Izolace	izolace	k1gFnSc1
</s>
<s>
44,4	44,4	k4
km	km	kA
→	→	k?
Pauhunri	Pauhunr	k1gFnSc2
Seznamy	seznam	k1gInPc1
</s>
<s>
Sedmitisícovky	Sedmitisícovka	k1gFnPc1
#	#	kIx~
<g/>
98	#num#	k4
<g/>
Ultraprominentní	Ultraprominentní	k2eAgFnSc2d1
hory	hora	k1gFnSc2
Poloha	poloha	k1gFnSc1
Světadíl	světadíl	k1gInSc1
</s>
<s>
Asie	Asie	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Čína	Čína	k1gFnSc1
ČínaBhútán	ČínaBhútán	k1gMnSc1
Bhútán	Bhútán	k1gInSc1
Pohoří	pohoří	k1gNnSc1
</s>
<s>
Himálaj	Himálaj	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
27	#num#	k4
<g/>
°	°	k?
<g/>
49	#num#	k4
<g/>
′	′	k?
<g/>
27	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
89	#num#	k4
<g/>
°	°	k?
<g/>
16	#num#	k4
<g/>
′	′	k?
<g/>
12	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Džomolhari	Džomolhari	k6eAd1
</s>
<s>
Prvovýstup	prvovýstup	k1gInSc1
</s>
<s>
1937	#num#	k4
Freddie	Freddie	k1gFnSc1
Spencer	Spencer	k1gMnSc1
Chapman	Chapman	k1gMnSc1
a	a	k8xC
Pasang	Pasang	k1gMnSc1
Dawa	Dawa	k1gMnSc1
Lama	lama	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Džomolhari	Džomolhari	k1gNnSc1
(	(	kIx(
<g/>
tibetsky	tibetsky	k6eAd1
ཇ	ཇ	k?
<g/>
ོ	ོ	k?
<g/>
་	་	k?
<g/>
མ	མ	k?
<g/>
ོ	ོ	k?
<g/>
་	་	k?
<g/>
ལ	ལ	k?
<g/>
ྷ	ྷ	k?
<g/>
་	་	k?
<g/>
ར	ར	k?
<g/>
ི	ི	k?
<g/>
,	,	kIx,
čínsky	čínsky	k6eAd1
v	v	k7c6
českém	český	k2eAgInSc6d1
přepisu	přepis	k1gInSc6
Čchuo-mo-la-ž	Čchuo-mo-la-ž	k1gFnSc1
<g/>
’	’	k?
Feng	Fenga	k1gFnPc2
<g/>
,	,	kIx,
pchin-jinem	pchin-jin	k1gMnSc7
Chuò	Chuò	k1gFnPc2
Fē	Fē	k1gInSc1
<g/>
,	,	kIx,
znaky	znak	k1gInPc1
绰	绰	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
hora	hora	k1gFnSc1
v	v	k7c6
Himálaji	Himálaj	k1gFnSc6
na	na	k7c6
hranici	hranice	k1gFnSc6
mezi	mezi	k7c7
Tibetskou	tibetský	k2eAgFnSc7d1
autonomní	autonomní	k2eAgFnSc7d1
oblastí	oblast	k1gFnSc7
Číny	Čína	k1gFnSc2
a	a	k8xC
Bhútánem	Bhútán	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výškou	výška	k1gFnSc7
7326	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
mezi	mezi	k7c4
sedmitisícovky	sedmitisícovka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
krajině	krajina	k1gFnSc6
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
výrazná	výrazný	k2eAgFnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
v	v	k7c6
blízkém	blízký	k2eAgNnSc6d1
okolí	okolí	k1gNnSc6
se	se	k3xPyFc4
srovnatelně	srovnatelně	k6eAd1
vysoké	vysoký	k2eAgFnPc1d1
hory	hora	k1gFnPc1
nevyskytují	vyskytovat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severní	severní	k2eAgFnSc1d1
stěna	stěna	k1gFnSc1
ční	čnět	k5eAaImIp3nS
2700	#num#	k4
metrů	metr	k1gInPc2
nad	nad	k7c7
relativní	relativní	k2eAgFnSc7d1
rovinou	rovina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
buddhisty	buddhista	k1gMnPc4
je	být	k5eAaImIp3nS
hora	hora	k1gFnSc1
posvátná	posvátný	k2eAgFnSc1d1
a	a	k8xC
konají	konat	k5eAaImIp3nP
pod	pod	k7c4
ni	on	k3xPp3gFnSc4
poutě	pouť	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Prvovýstup	prvovýstup	k1gInSc4
provedli	provést	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1937	#num#	k4
Freddie	Freddie	k1gFnSc2
Spencer	Spencero	k1gNnPc2
Chapman	Chapman	k1gMnSc1
a	a	k8xC
Pasang	Pasang	k1gMnSc1
Dawa	Dawa	k1gMnSc1
Lama	lama	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Chomolhari	Chomolhar	k1gFnSc2
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Džomolhari	Džomolhar	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
