<s>
Telekomunikační	telekomunikační	k2eAgFnPc1d1
a	a	k8xC
internetové	internetový	k2eAgFnPc1d1
konvergované	konvergovaný	k2eAgFnPc1d1
služby	služba	k1gFnPc1
a	a	k8xC
protokoly	protokol	k1gInPc1
pro	pro	k7c4
pokročilé	pokročilý	k2eAgFnPc4d1
sítě	síť	k1gFnPc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Telecoms	Telecoms	k1gInSc1
&	&	k?
Internet	Internet	k1gInSc1
converged	converged	k1gMnSc1
Services	Servicesa	k1gFnPc2
&	&	k?
Protocols	Protocolsa	k1gFnPc2
for	forum	k1gNnPc2
Advanced	Advanced	k1gMnSc1
Networks	Networks	k1gInSc1
<g/>
,	,	kIx,
TISPAN	TISPAN	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
standardizační	standardizační	k2eAgInSc1d1
orgán	orgán	k1gInSc1
ETSI	ETSI	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
konvergencí	konvergence	k1gFnSc7
pevných	pevný	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
a	a	k8xC
Internetu	Internet	k1gInSc2
(	(	kIx(
<g/>
přechodem	přechod	k1gInSc7
pevných	pevný	k2eAgFnPc2d1
telefonních	telefonní	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
na	na	k7c4
internetové	internetový	k2eAgInPc4d1
protokoly	protokol	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>