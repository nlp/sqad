<s>
TISPAN	TISPAN	kA
</s>
<s>
Telekomunikační	telekomunikační	k2eAgFnPc1d1
a	a	k8xC
internetové	internetový	k2eAgFnPc1d1
konvergované	konvergovaný	k2eAgFnPc1d1
služby	služba	k1gFnPc1
a	a	k8xC
protokoly	protokol	k1gInPc1
pro	pro	k7c4
pokročilé	pokročilý	k2eAgFnPc4d1
sítě	síť	k1gFnPc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Telecoms	Telecoms	k1gInSc1
&	&	k?
Internet	Internet	k1gInSc1
converged	converged	k1gMnSc1
Services	Servicesa	k1gFnPc2
&	&	k?
Protocols	Protocolsa	k1gFnPc2
for	forum	k1gNnPc2
Advanced	Advanced	k1gMnSc1
Networks	Networks	k1gInSc1
<g/>
,	,	kIx,
TISPAN	TISPAN	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
standardizační	standardizační	k2eAgInSc1d1
orgán	orgán	k1gInSc1
ETSI	ETSI	kA
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
konvergencí	konvergence	k1gFnSc7
pevných	pevný	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
a	a	k8xC
Internetu	Internet	k1gInSc2
(	(	kIx(
<g/>
přechodem	přechod	k1gInSc7
pevných	pevný	k2eAgFnPc2d1
telefonních	telefonní	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
na	na	k7c4
internetové	internetový	k2eAgInPc4d1
protokoly	protokol	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
založen	založit	k5eAaPmNgMnS
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
sloučením	sloučení	k1gNnSc7
dvou	dva	k4xCgInPc2
orgánů	orgán	k1gInPc2
ETSI	ETSI	kA
<g/>
:	:	kIx,
</s>
<s>
Harmonizace	harmonizace	k1gFnSc1
telekomunikačních	telekomunikační	k2eAgInPc2d1
a	a	k8xC
internetových	internetový	k2eAgInPc2d1
protokolů	protokol	k1gInPc2
v	v	k7c6
sítích	síť	k1gFnPc6
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Telecommunications	Telecommunications	k1gInSc1
and	and	k?
Internet	Internet	k1gInSc1
Protocol	Protocola	k1gFnPc2
Harmonization	Harmonization	k1gInSc1
Over	Over	k1gMnSc1
Networks	Networks	k1gInSc1
<g/>
,	,	kIx,
TIPHON	TIPHON	kA
<g/>
)	)	kIx)
</s>
<s>
Služby	služba	k1gFnPc1
a	a	k8xC
protokoly	protokol	k1gInPc1
pro	pro	k7c4
pokročilé	pokročilý	k2eAgFnPc4d1
sítě	síť	k1gFnPc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Services	Services	k1gInSc1
and	and	k?
Protocols	Protocolsa	k1gFnPc2
for	forum	k1gNnPc2
Advanced	Advanced	k1gMnSc1
Networks	Networks	k1gInSc1
<g/>
,	,	kIx,
SPAN	SPAN	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
TISPAN	TISPAN	kA
usiluje	usilovat	k5eAaImIp3nS
o	o	k7c4
definování	definování	k1gNnSc4
evropského	evropský	k2eAgInSc2d1
pohledu	pohled	k1gInSc2
na	na	k7c4
sítě	síť	k1gFnPc4
příští	příští	k2eAgFnSc2d1
generace	generace	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Next	Next	k2eAgInSc1d1
Generation	Generation	k1gInSc1
Networks	Networksa	k1gFnPc2
<g/>
,	,	kIx,
NGN	NGN	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
má	mít	k5eAaImIp3nS
i	i	k9
mnoho	mnoho	k4c1
účastníků	účastník	k1gMnPc2
z	z	k7c2
mimoevropských	mimoevropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
TISPAN	TISPAN	kA
NGN	NGN	kA
Release	Releasa	k1gFnSc6
1	#num#	k4
zveřejněné	zveřejněný	k2eAgFnSc2d1
v	v	k7c6
prosinci	prosinec	k1gInSc6
2005	#num#	k4
obsahuje	obsahovat	k5eAaImIp3nS
architektonické	architektonický	k2eAgInPc4d1
základy	základ	k1gInPc4
a	a	k8xC
základní	základní	k2eAgFnPc4d1
specifikace	specifikace	k1gFnPc4
požadované	požadovaný	k2eAgFnPc4d1
pro	pro	k7c4
podporu	podpora	k1gFnSc4
náhrady	náhrada	k1gFnSc2
veřejných	veřejný	k2eAgFnPc2d1
komutovaných	komutovaný	k2eAgFnPc2d1
telefonních	telefonní	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
(	(	kIx(
<g/>
PSTN	PSTN	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Architektura	architektura	k1gFnSc1
TISPAN	TISPAN	kA
NGN	NGN	kA
je	být	k5eAaImIp3nS
založena	založen	k2eAgFnSc1d1
na	na	k7c4
sdílení	sdílení	k1gNnSc4
společných	společný	k2eAgFnPc2d1
komponent	komponenta	k1gFnPc2
mezi	mezi	k7c7
spolupracujícími	spolupracující	k2eAgInPc7d1
subsystémy	subsystém	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Architektura	architektura	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
obecným	obecný	k2eAgInSc7d1
referenčním	referenční	k2eAgInSc7d1
modelem	model	k1gInSc7
pro	pro	k7c4
sítě	síť	k1gFnPc4
příští	příští	k2eAgFnSc2d1
generace	generace	k1gFnSc2
definovaným	definovaný	k2eAgInSc7d1
v	v	k7c6
doporučení	doporučení	k1gNnSc4
ITU-T	ITU-T	k1gFnSc2
Y	Y	kA
<g/>
.2011	.2011	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
je	být	k5eAaImIp3nS
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c4
servisní	servisní	k2eAgFnSc4d1
vrstvu	vrstva	k1gFnSc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
service	service	k1gFnSc2
stratum	stratum	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
transportní	transportní	k2eAgFnSc4d1
vrstvu	vrstva	k1gFnSc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
transport	transport	k1gInSc1
stratum	stratum	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
z	z	k7c2
těchto	tento	k3xDgFnPc2
vrstev	vrstva	k1gFnPc2
je	být	k5eAaImIp3nS
dále	daleko	k6eAd2
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c4
dílčí	dílčí	k2eAgInPc4d1
systémy	systém	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
v	v	k7c6
celkové	celkový	k2eAgFnSc6d1
struktuře	struktura	k1gFnSc6
vykonávají	vykonávat	k5eAaImIp3nP
jednotlivé	jednotlivý	k2eAgInPc4d1
úkoly	úkol	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
umožňuje	umožňovat	k5eAaImIp3nS
postupné	postupný	k2eAgNnSc1d1
přidávání	přidávání	k1gNnSc1
nových	nový	k2eAgInPc2d1
subsystémů	subsystém	k1gInPc2
pro	pro	k7c4
nové	nový	k2eAgInPc4d1
požadavky	požadavek	k1gInPc4
a	a	k8xC
třídy	třída	k1gFnPc4
služeb	služba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
síťové	síťový	k2eAgInPc1d1
prostředky	prostředek	k1gInPc1
<g/>
,	,	kIx,
aplikace	aplikace	k1gFnPc1
a	a	k8xC
uživatelská	uživatelský	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
budou	být	k5eAaImBp3nP
společné	společný	k2eAgFnPc1d1
pro	pro	k7c4
všechny	všechen	k3xTgInPc4
subsystémy	subsystém	k1gInPc4
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
zajištěna	zajistit	k5eAaPmNgFnS
maximální	maximální	k2eAgFnSc1d1
možná	možný	k2eAgFnSc1d1
míra	míra	k1gFnSc1
mobility	mobilita	k1gFnSc2
uživatelských	uživatelský	k2eAgInPc2d1
terminálů	terminál	k1gInPc2
a	a	k8xC
služeb	služba	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
přes	přes	k7c4
správní	správní	k2eAgFnPc4d1
hranice	hranice	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klíčový	klíčový	k2eAgInSc1d1
subsystém	subsystém	k1gInSc1
je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
na	na	k7c6
architektuře	architektura	k1gFnSc6
IP	IP	kA
Multimedia	multimedium	k1gNnSc2
Subsystem	Subsyst	k1gMnSc7
(	(	kIx(
<g/>
IMS	IMS	kA
<g/>
)	)	kIx)
vyvinuté	vyvinutý	k2eAgNnSc1d1
sdružením	sdružení	k1gNnSc7
3	#num#	k4
<g/>
rd	rd	k?
Generation	Generation	k1gInSc1
Partnership	Partnership	k1gMnSc1
Project	Project	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
GPP	GPP	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
TISPAN	TISPAN	kA
spolupracoval	spolupracovat	k5eAaImAgMnS
s	s	k7c7
3GPP	3GPP	k4
na	na	k7c4
rozšíření	rozšíření	k1gNnSc4
architektury	architektura	k1gFnSc2
IMS	IMS	kA
o	o	k7c4
vlastnosti	vlastnost	k1gFnPc4
potřebné	potřebný	k2eAgFnPc4d1
pro	pro	k7c4
podporu	podpora	k1gFnSc4
přístupu	přístup	k1gInSc2
pomocí	pomocí	k7c2
pevných	pevný	k2eAgFnPc2d1
linek	linka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
TISPAN	TISPAN	kA
NGN	NGN	kA
Release	Releasa	k1gFnSc6
2	#num#	k4
dokončené	dokončený	k2eAgFnSc2d1
počátkem	počátkem	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
přidává	přidávat	k5eAaImIp3nS
podporu	podpora	k1gFnSc4
pro	pro	k7c4
služby	služba	k1gFnPc4
IPTV	IPTV	kA
a	a	k8xC
obchodní	obchodní	k2eAgFnSc2d1
komunikace	komunikace	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Business	business	k1gInSc1
Communications	Communicationsa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
pomocí	pomocí	k7c2
IMS	IMS	kA
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
začátku	začátek	k1gInSc2
roku	rok	k1gInSc2
2008	#num#	k4
začal	začít	k5eAaPmAgMnS
TISPAN	TISPAN	kA
pracovat	pracovat	k5eAaImF
na	na	k7c6
třetím	třetí	k4xOgNnSc6
vydání	vydání	k1gNnSc6
své	svůj	k3xOyFgNnSc4
NGN	NGN	kA
specifikace	specifikace	k1gFnPc4
s	s	k7c7
předsedou	předseda	k1gMnSc7
zaměřením	zaměření	k1gNnSc7
na	na	k7c4
vylepšení	vylepšení	k1gNnSc4
IPTV	IPTV	kA
<g/>
,	,	kIx,
sítě	síť	k1gFnPc1
pro	pro	k7c4
doručování	doručování	k1gNnSc4
obsahu	obsah	k1gInSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Content	Content	k1gMnSc1
Delivery	Delivera	k1gFnSc2
Networks	Networks	k1gInSc1
<g/>
,	,	kIx,
CDN	CDN	kA
<g/>
)	)	kIx)
a	a	k8xC
domácí	domácí	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
zveřejnil	zveřejnit	k5eAaPmAgInS
TISPAN	TISPAN	kA
specifikaci	specifikace	k1gFnSc4
funkční	funkční	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
pro	pro	k7c4
sítě	síť	k1gFnPc4
doručování	doručování	k1gNnSc2
obsahu	obsah	k1gInSc2
(	(	kIx(
<g/>
CDN	CDN	kA
<g/>
)	)	kIx)
a	a	k8xC
nyní	nyní	k6eAd1
pracuje	pracovat	k5eAaImIp3nS
na	na	k7c4
specifikaci	specifikace	k1gFnSc4
protokolů	protokol	k1gInPc2
pro	pro	k7c4
referenční	referenční	k2eAgInPc4d1
body	bod	k1gInPc4
definované	definovaný	k2eAgInPc4d1
v	v	k7c6
této	tento	k3xDgFnSc6
architektuře	architektura	k1gFnSc6
(	(	kIx(
<g/>
ETSI	ETSI	kA
TS	ts	k0
182	#num#	k4
0	#num#	k4
<g/>
19	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ITU-T	ITU-T	k1gMnSc1
doporučení	doporučení	k1gNnSc2
Y	Y	kA
<g/>
.2011	.2011	k4
General	General	k1gMnPc1
principles	principlesa	k1gFnPc2
and	and	k?
general	generat	k5eAaImAgMnS,k5eAaPmAgMnS
reference	reference	k1gFnPc4
model	model	k1gInSc1
for	forum	k1gNnPc2
Next	Next	k2eAgInSc1d1
Generation	Generation	k1gInSc1
Networks	Networks	k1gInSc1
<g/>
↑	↑	k?
CHATRAS	CHATRAS	kA
<g/>
,	,	kIx,
B.	B.	kA
Business	business	k1gInSc1
Trunking	Trunking	k1gInSc1
Communications	Communications	k1gInSc4
in	in	k?
ETSI	ETSI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
IEEE	IEEE	kA
Wireless	Wirelessa	k1gFnPc2
Communications	Communicationsa	k1gFnPc2
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
16	#num#	k4
<g/>
,	,	kIx,
Issue	Issue	k1gFnSc1
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
8	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1536	#num#	k4
<g/>
-	-	kIx~
<g/>
1284	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Telecoms	Telecoms	k1gInSc1
&	&	k?
Internet	Internet	k1gInSc1
converged	converged	k1gMnSc1
Services	Servicesa	k1gFnPc2
&	&	k?
Protocols	Protocolsa	k1gFnPc2
for	forum	k1gNnPc2
Advanced	Advanced	k1gMnSc1
Networks	Networks	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
TISPAN	TISPAN	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Úvod	úvod	k1gInSc1
do	do	k7c2
architektury	architektura	k1gFnSc2
TISPAN	TISPAN	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Malé	Malé	k2eAgInPc1d1
krůčky	krůček	k1gInPc1
ke	k	k7c3
konvergenci	konvergence	k1gFnSc3
<g/>
:	:	kIx,
IMS	IMS	kA
</s>
