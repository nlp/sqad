<s>
Harrisburg	Harrisburg	k1gInSc1	Harrisburg
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Pensylvánie	Pensylvánie	k1gFnSc2	Pensylvánie
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
sídelní	sídelní	k2eAgNnSc1d1	sídelní
město	město	k1gNnSc1	město
kraje	kraj	k1gInSc2	kraj
Dauphin	dauphin	k1gMnSc1	dauphin
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výsledku	výsledek	k1gInSc2	výsledek
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
žilo	žít	k5eAaImAgNnS	žít
48	[number]	k4	48
528	[number]	k4	528
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
10	[number]	k4	10
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
největších	veliký	k2eAgNnPc2d3	veliký
měst	město	k1gNnPc2	město
tohoto	tento	k3xDgInSc2	tento
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
aglomeraci	aglomerace	k1gFnSc6	aglomerace
však	však	k9	však
žije	žít	k5eAaImIp3nS	žít
až	až	k9	až
k	k	k7c3	k
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
podle	podle	k7c2	podle
prvního	první	k4xOgMnSc2	první
evropského	evropský	k2eAgMnSc2d1	evropský
přistěhovalce	přistěhovalec	k1gMnSc2	přistěhovalec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
usadil	usadit	k5eAaPmAgInS	usadit
<g/>
.	.	kIx.	.
</s>
<s>
Harrisburg	Harrisburg	k1gInSc1	Harrisburg
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
hranici	hranice	k1gFnSc6	hranice
Dauphin	dauphin	k1gMnSc1	dauphin
County	Counta	k1gFnSc2	Counta
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
situován	situovat	k5eAaBmNgInS	situovat
do	do	k7c2	do
jihovýchodního	jihovýchodní	k2eAgInSc2d1	jihovýchodní
kvadrantu	kvadrant	k1gInSc2	kvadrant
státu	stát	k1gInSc2	stát
Pensylvánie	Pensylvánie	k1gFnSc1	Pensylvánie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vzdálen	vzdálit	k5eAaPmNgInS	vzdálit
asi	asi	k9	asi
169	[number]	k4	169
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
velkoměsta	velkoměsto	k1gNnSc2	velkoměsto
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
Susquehanna	Susquehanna	k1gFnSc1	Susquehanna
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
Harrisburg	Harrisburg	k1gMnSc1	Harrisburg
leží	ležet	k5eAaImIp3nS	ležet
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
obýváno	obývat	k5eAaImNgNnS	obývat
původními	původní	k2eAgInPc7d1	původní
indiánskými	indiánský	k2eAgInPc7d1	indiánský
kmeny	kmen	k1gInPc7	kmen
<g/>
;	;	kIx,	;
až	až	k6eAd1	až
3000	[number]	k4	3000
před	před	k7c7	před
n.	n.	k?	n.
l.	l.	k?	l.
První	první	k4xOgInSc4	první
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
evropských	evropský	k2eAgMnPc2d1	evropský
cestovalů	cestoval	k1gMnPc2	cestoval
provedl	provést	k5eAaPmAgMnS	provést
kapitán	kapitán	k1gMnSc1	kapitán
John	John	k1gMnSc1	John
Smith	Smith	k1gMnSc1	Smith
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1608	[number]	k4	1608
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1719	[number]	k4	1719
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
usadili	usadit	k5eAaPmAgMnP	usadit
první	první	k4xOgMnPc1	první
evropští	evropský	k2eAgMnPc1d1	evropský
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
-	-	kIx~	-
John	John	k1gMnSc1	John
Harris	Harris	k1gFnSc1	Harris
<g/>
,	,	kIx,	,
Sr	Sr	k1gFnSc1	Sr
<g/>
.	.	kIx.	.
-	-	kIx~	-
anglický	anglický	k2eAgMnSc1d1	anglický
obchodník	obchodník	k1gMnSc1	obchodník
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
syn	syn	k1gMnSc1	syn
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1785	[number]	k4	1785
začal	začít	k5eAaPmAgInS	začít
plán	plán	k1gInSc1	plán
učinit	učinit	k5eAaImF	učinit
z	z	k7c2	z
osady	osada	k1gFnSc2	osada
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
čehož	což	k3yQnSc2	což
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1791	[number]	k4	1791
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
se	se	k3xPyFc4	se
Harrisburg	Harrisburg	k1gInSc1	Harrisburg
stal	stát	k5eAaPmAgInS	stát
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
Harrisburg	Harrisburg	k1gInSc4	Harrisburg
významnou	významný	k2eAgFnSc7d1	významná
křižovatkou	křižovatka	k1gFnSc7	křižovatka
pro	pro	k7c4	pro
otroky	otrok	k1gMnPc4	otrok
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
podzemní	podzemní	k2eAgFnSc6d1	podzemní
železnici	železnice	k1gFnSc6	železnice
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
spojoval	spojovat	k5eAaImAgInS	spojovat
východní	východní	k2eAgNnPc4d1	východní
přístavní	přístavní	k2eAgNnPc4d1	přístavní
města	město	k1gNnPc4	město
<g/>
,	,	kIx,	,
cestu	cesta	k1gFnSc4	cesta
podél	podél	k7c2	podél
Susqahanny	Susqahanna	k1gFnSc2	Susqahanna
a	a	k8xC	a
severní	severní	k2eAgFnSc4d1	severní
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
východokanadských	východokanadský	k2eAgFnPc2d1	východokanadský
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
Severu	sever	k1gInSc2	sever
proti	proti	k7c3	proti
Jihu	jih	k1gInSc3	jih
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Harrisburgu	Harrisburg	k1gInSc6	Harrisburg
velké	velký	k2eAgNnSc1d1	velké
výcvikové	výcvikový	k2eAgNnSc1d1	výcvikové
centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
vojáky	voják	k1gMnPc4	voják
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
pol	pola	k1gFnPc2	pola
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Harrisburg	Harrisburg	k1gInSc1	Harrisburg
stal	stát	k5eAaPmAgInS	stát
významnou	významný	k2eAgFnSc7d1	významná
železniční	železniční	k2eAgFnSc7d1	železniční
křižovatkou	křižovatka	k1gFnSc7	křižovatka
<g/>
,	,	kIx,	,
spojující	spojující	k2eAgInSc1d1	spojující
americký	americký	k2eAgInSc4d1	americký
východ	východ	k1gInSc4	východ
a	a	k8xC	a
středozápad	středozápad	k1gInSc4	středozápad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1979	[number]	k4	1979
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
nedaleké	daleký	k2eNgFnSc6d1	nedaleká
jaderné	jaderný	k2eAgFnSc6d1	jaderná
elektrárně	elektrárna	k1gFnSc6	elektrárna
Three	Thre	k1gInSc2	Thre
Mile	mile	k6eAd1	mile
Island	Island	k1gInSc1	Island
k	k	k7c3	k
havárii	havárie	k1gFnSc3	havárie
5	[number]	k4	5
<g/>
.	.	kIx.	.
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
radioaktivita	radioaktivita	k1gFnSc1	radioaktivita
unikla	uniknout	k5eAaPmAgFnS	uniknout
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
zvolen	zvolen	k2eAgMnSc1d1	zvolen
současný	současný	k2eAgMnSc1d1	současný
starosta	starosta	k1gMnSc1	starosta
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Steven	Steven	k2eAgInSc1d1	Steven
Reed	Reed	k1gInSc1	Reed
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
inicioval	iniciovat	k5eAaBmAgInS	iniciovat
významnou	významný	k2eAgFnSc4d1	významná
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
přeměnu	přeměna	k1gFnSc4	přeměna
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
posílit	posílit	k5eAaPmF	posílit
jak	jak	k8xS	jak
ekonomiku	ekonomika	k1gFnSc4	ekonomika
tak	tak	k6eAd1	tak
turistický	turistický	k2eAgInSc1d1	turistický
ruch	ruch	k1gInSc1	ruch
v	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
v	v	k7c6	v
Harrisburgu	Harrisburg	k1gInSc6	Harrisburg
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc1	několik
museí	museum	k1gNnPc2	museum
<g/>
,	,	kIx,	,
hotelů	hotel	k1gInPc2	hotel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
obchodní	obchodní	k2eAgFnSc2d1	obchodní
a	a	k8xC	a
rezidenční	rezidenční	k2eAgFnSc2d1	rezidenční
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Harrisburg	Harrisburg	k1gMnSc1	Harrisburg
má	mít	k5eAaImIp3nS	mít
klima	klima	k1gNnSc4	klima
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
americkému	americký	k2eAgInSc3d1	americký
severovýchodu	severovýchod	k1gInSc3	severovýchod
nížinných	nížinný	k2eAgFnPc2d1	nížinná
poloh	poloha	k1gFnPc2	poloha
(	(	kIx(	(
<g/>
město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
98	[number]	k4	98
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
relativní	relativní	k2eAgFnSc7d1	relativní
blízkostí	blízkost	k1gFnSc7	blízkost
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
tamního	tamní	k2eAgInSc2d1	tamní
Golfského	golfský	k2eAgInSc2d1	golfský
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
kolem	kolem	k7c2	kolem
bodu	bod	k1gInSc2	bod
mrazu	mráz	k1gInSc2	mráz
až	až	k9	až
po	po	k7c4	po
20-30	[number]	k4	20-30
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
mohou	moct	k5eAaImIp3nP	moct
překročit	překročit	k5eAaPmF	překročit
"	"	kIx"	"
<g/>
tropickou	tropický	k2eAgFnSc4d1	tropická
<g/>
"	"	kIx"	"
čtyřicítku	čtyřicítka	k1gFnSc4	čtyřicítka
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc1d1	roční
srážky	srážka	k1gFnPc1	srážka
činí	činit	k5eAaImIp3nP	činit
1052,8	[number]	k4	1052,8
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
49	[number]	k4	49
528	[number]	k4	528
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Harrisburg	Harrisburg	k1gInSc1	Harrisburg
24	[number]	k4	24
316	[number]	k4	316
obytných	obytný	k2eAgFnPc2d1	obytná
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
20	[number]	k4	20
561	[number]	k4	561
domácností	domácnost	k1gFnPc2	domácnost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
žilo	žít	k5eAaImAgNnS	žít
10	[number]	k4	10
917	[number]	k4	917
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
30,7	[number]	k4	30,7
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
52,4	[number]	k4	52,4
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,5	[number]	k4	0,5
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
3,5	[number]	k4	3,5
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
<g />
.	.	kIx.	.
</s>
<s>
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
7,8	[number]	k4	7,8
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
5,2	[number]	k4	5,2
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
18,0	[number]	k4	18,0
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
28,2	[number]	k4	28,2
%	%	kIx~	%
do	do	k7c2	do
18	[number]	k4	18
let	léto	k1gNnPc2	léto
9,2	[number]	k4	9,2
%	%	kIx~	%
18-26	[number]	k4	18-26
let	léto	k1gNnPc2	léto
31	[number]	k4	31
%	%	kIx~	%
26-44	[number]	k4	26-44
let	léto	k1gNnPc2	léto
20,8	[number]	k4	20,8
%	%	kIx~	%
45-64	[number]	k4	45-64
let	léto	k1gNnPc2	léto
10,9	[number]	k4	10,9
%	%	kIx~	%
nad	nad	k7c4	nad
65	[number]	k4	65
let	léto	k1gNnPc2	léto
V	v	k7c6	v
Harrisburgu	Harrisburg	k1gInSc6	Harrisburg
mají	mít	k5eAaImIp3nP	mít
sídlo	sídlo	k1gNnSc4	sídlo
dva	dva	k4xCgInPc1	dva
deníky	deník	k1gInPc1	deník
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Sentinel	sentinel	k1gInSc1	sentinel
a	a	k8xC	a
The	The	k1gFnSc1	The
Press	Pressa	k1gFnPc2	Pressa
and	and	k?	and
Journal	Journal	k1gFnPc2	Journal
<g/>
)	)	kIx)	)
s	s	k7c7	s
nákladem	náklad	k1gInSc7	náklad
přes	přes	k7c4	přes
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
výtisků	výtisk	k1gInPc2	výtisk
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nich	on	k3xPp3gInPc2	on
existuje	existovat	k5eAaImIp3nS	existovat
dalších	další	k2eAgNnPc2d1	další
pět	pět	k4xCc4	pět
periodik	periodikum	k1gNnPc2	periodikum
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkem	celkem	k6eAd1	celkem
devíti	devět	k4xCc2	devět
televizních	televizní	k2eAgFnPc2d1	televizní
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
Harrisburgu	Harrisburg	k1gInSc6	Harrisburg
fungují	fungovat	k5eAaImIp3nP	fungovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
7	[number]	k4	7
komerčních	komerční	k2eAgFnPc2d1	komerční
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
nezávislý	závislý	k2eNgInSc4d1	nezávislý
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
kabelový	kabelový	k2eAgMnSc1d1	kabelový
a	a	k8xC	a
současně	současně	k6eAd1	současně
veřejně	veřejně	k6eAd1	veřejně
přístupný	přístupný	k2eAgMnSc1d1	přístupný
(	(	kIx(	(
<g/>
WHBG	WHBG	kA	WHBG
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
15	[number]	k4	15
AM	AM	kA	AM
a	a	k8xC	a
22	[number]	k4	22
FM	FM	kA	FM
rádiových	rádiový	k2eAgFnPc2d1	rádiová
stanic	stanice	k1gFnPc2	stanice
s	s	k7c7	s
různým	různý	k2eAgInSc7d1	různý
tematickými	tematický	k2eAgNnPc7d1	tematické
pojetími	pojetí	k1gNnPc7	pojetí
a	a	k8xC	a
cílovými	cílový	k2eAgFnPc7d1	cílová
skupinami	skupina	k1gFnPc7	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Harrisburg	Harrisburg	k1gInSc1	Harrisburg
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgNnPc4	dva
místa	místo	k1gNnPc4	místo
vhodná	vhodný	k2eAgNnPc4d1	vhodné
pro	pro	k7c4	pro
velké	velký	k2eAgFnPc4d1	velká
kulturní	kulturní	k2eAgFnPc4d1	kulturní
akce	akce	k1gFnPc4	akce
<g/>
:	:	kIx,	:
Whitaker	Whitaker	k1gInSc1	Whitaker
Center	centrum	k1gNnPc2	centrum
for	forum	k1gNnPc2	forum
Science	Science	k1gFnSc2	Science
and	and	k?	and
the	the	k?	the
Arts	Arts	k1gInSc1	Arts
<g/>
,	,	kIx,	,
a	a	k8xC	a
The	The	k1gMnPc1	The
Forum	forum	k1gNnSc1	forum
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
nacvičuje	nacvičovat	k5eAaImIp3nS	nacvičovat
symfonický	symfonický	k2eAgInSc1d1	symfonický
orchestr	orchestr	k1gInSc1	orchestr
města	město	k1gNnSc2	město
a	a	k8xC	a
do	do	k7c2	do
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
vejde	vejít	k5eAaPmIp3nS	vejít
1763	[number]	k4	1763
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Neméně	málo	k6eNd2	málo
impozantní	impozantní	k2eAgNnPc4d1	impozantní
je	být	k5eAaImIp3nS	být
Pennsylvania	Pennsylvanium	k1gNnPc4	Pennsylvanium
State	status	k1gInSc5	status
Farm	Farmo	k1gNnPc2	Farmo
Show	show	k1gFnSc1	show
Arena	Arena	k1gFnSc1	Arena
-	-	kIx~	-
největší	veliký	k2eAgNnSc1d3	veliký
místo	místo	k1gNnSc1	místo
pro	pro	k7c4	pro
výstavy	výstava	k1gFnPc4	výstava
nebo	nebo	k8xC	nebo
sněmy	sněm	k1gInPc4	sněm
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
a	a	k8xC	a
několik	několik	k4yIc4	několik
menších	malý	k2eAgFnPc2d2	menší
hal	hala	k1gFnPc2	hala
a	a	k8xC	a
sálů	sál	k1gInPc2	sál
a	a	k8xC	a
kolem	kolem	k7c2	kolem
půl	půl	k1xP	půl
tuctu	tucet	k1gInSc2	tucet
muzeí	muzeum	k1gNnPc2	muzeum
-	-	kIx~	-
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
tematických	tematický	k2eAgInPc2d1	tematický
nebo	nebo	k8xC	nebo
reprezentující	reprezentující	k2eAgInSc4d1	reprezentující
celý	celý	k2eAgInSc4d1	celý
stát	stát	k1gInSc4	stát
Pensylvánie	Pensylvánie	k1gFnSc2	Pensylvánie
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
chvíle	chvíle	k1gFnPc4	chvíle
oddechu	oddech	k1gInSc2	oddech
a	a	k8xC	a
rekreace	rekreace	k1gFnSc2	rekreace
město	město	k1gNnSc1	město
nabízí	nabízet	k5eAaImIp3nS	nabízet
šest	šest	k4xCc4	šest
parků	park	k1gInPc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
Lake	Lak	k1gFnPc1	Lak
Parks	Parksa	k1gFnPc2	Parksa
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
parky	park	k1gInPc1	park
s	s	k7c7	s
vybudovanými	vybudovaný	k2eAgInPc7d1	vybudovaný
rybníky	rybník	k1gInPc7	rybník
či	či	k8xC	či
jezírky	jezírko	k1gNnPc7	jezírko
<g/>
.	.	kIx.	.
</s>
<s>
Památníky	památník	k1gInPc1	památník
Holocaust	holocaust	k1gInSc1	holocaust
Memorial	Memorial	k1gMnSc1	Memorial
for	forum	k1gNnPc2	forum
the	the	k?	the
Commonwealth	Commonwealth	k1gMnSc1	Commonwealth
of	of	k?	of
Pennsylvania	Pennsylvanium	k1gNnPc1	Pennsylvanium
Harrisburg	Harrisburg	k1gInSc1	Harrisburg
je	on	k3xPp3gMnPc4	on
sídle	sídlo	k1gNnSc6	sídlo
7	[number]	k4	7
poloprofesionálních	poloprofesionální	k2eAgInPc2d1	poloprofesionální
nebo	nebo	k8xC	nebo
profesionálních	profesionální	k2eAgInPc2d1	profesionální
sportovních	sportovní	k2eAgInPc2d1	sportovní
týmů	tým	k1gInPc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
až	až	k9	až
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
baseballový	baseballový	k2eAgInSc1d1	baseballový
tým	tým	k1gInSc1	tým
Harrisburg	Harrisburg	k1gInSc1	Harrisburg
Senators	Senators	k1gInSc4	Senators
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k1gNnPc1	ostatní
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
fotbal	fotbal	k1gInSc4	fotbal
<g/>
,	,	kIx,	,
americký	americký	k2eAgInSc4d1	americký
fotbal	fotbal	k1gInSc4	fotbal
<g/>
,	,	kIx,	,
halový	halový	k2eAgInSc4d1	halový
fotbal	fotbal	k1gInSc4	fotbal
a	a	k8xC	a
basketbal	basketbal	k1gInSc4	basketbal
<g/>
.	.	kIx.	.
</s>
<s>
Vládní	vládní	k2eAgFnPc1d1	vládní
budovy	budova	k1gFnPc1	budova
v	v	k7c6	v
Harrisburgu	Harrisburg	k1gInSc6	Harrisburg
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
<g/>
:	:	kIx,	:
Pennsylvania	Pennsylvanium	k1gNnPc1	Pennsylvanium
State	status	k1gInSc5	status
Capitol	Capitol	k1gInSc4	Capitol
Complex	Complex	k1gInSc4	Complex
-	-	kIx~	-
státní	státní	k2eAgInSc4d1	státní
kapitol	kapitola	k1gFnPc2	kapitola
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Martin	Martin	k1gMnSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
King	King	k1gMnSc1	King
<g/>
,	,	kIx,	,
Jr	Jr	k1gMnSc1	Jr
<g/>
.	.	kIx.	.
</s>
<s>
City	city	k1gNnSc1	city
Government	Government	k1gInSc1	Government
Center	centrum	k1gNnPc2	centrum
-	-	kIx~	-
radnice	radnice	k1gFnSc1	radnice
Commonwealth	Commonwealth	k1gMnSc1	Commonwealth
Judicial	Judicial	k1gMnSc1	Judicial
Center	centrum	k1gNnPc2	centrum
Ronald	Ronald	k1gMnSc1	Ronald
Reagan	Reagan	k1gMnSc1	Reagan
Federal	Federal	k1gMnSc1	Federal
Building	Building	k1gInSc1	Building
and	and	k?	and
Courthouse	Courthouse	k1gFnSc2	Courthouse
Pro	pro	k7c4	pro
vzdělání	vzdělání	k1gNnSc4	vzdělání
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Harrisburgu	Harrisburg	k1gInSc6	Harrisburg
zřízen	zřídit	k5eAaPmNgInS	zřídit
Harrisburg	Harrisburg	k1gInSc1	Harrisburg
School	Schoola	k1gFnPc2	Schoola
District	District	k1gInSc1	District
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
každoročně	každoročně	k6eAd1	každoročně
musí	muset	k5eAaImIp3nS	muset
zvládnout	zvládnout	k5eAaPmF	zvládnout
zápis	zápis	k1gInSc1	zápis
nebo	nebo	k8xC	nebo
přijímací	přijímací	k2eAgInSc1d1	přijímací
proces	proces	k1gInSc1	proces
několika	několik	k4yIc2	několik
tisíc	tisíc	k4xCgInPc2	tisíc
žáků	žák	k1gMnPc2	žák
a	a	k8xC	a
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
městě	město	k1gNnSc6	město
(	(	kIx(	(
<g/>
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
americké	americký	k2eAgFnSc2d1	americká
školského	školský	k2eAgInSc2d1	školský
systému	systém	k1gInSc2	systém
obecně	obecně	k6eAd1	obecně
<g/>
)	)	kIx)	)
fungují	fungovat	k5eAaImIp3nP	fungovat
státní	státní	k2eAgFnPc1d1	státní
i	i	k8xC	i
soukromé	soukromý	k2eAgFnPc1d1	soukromá
školy	škola	k1gFnPc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Soukromých	soukromý	k2eAgInPc2d1	soukromý
je	být	k5eAaImIp3nS	být
cca	cca	kA	cca
40	[number]	k4	40
a	a	k8xC	a
tematicky	tematicky	k6eAd1	tematicky
jsou	být	k5eAaImIp3nP	být
stratifikované	stratifikovaný	k2eAgInPc1d1	stratifikovaný
od	od	k7c2	od
náboženských	náboženský	k2eAgFnPc2d1	náboženská
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
akademie	akademie	k1gFnPc4	akademie
po	po	k7c4	po
přípravky	přípravek	k1gInPc4	přípravek
na	na	k7c4	na
univerzity	univerzita	k1gFnPc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Univerzit	univerzita	k1gFnPc2	univerzita
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Harrisburgu	Harrisburg	k1gInSc6	Harrisburg
je	být	k5eAaImIp3nS	být
sedm	sedm	k4xCc1	sedm
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Harrisburg	Harrisburg	k1gMnSc1	Harrisburg
Area	Ares	k1gMnSc2	Ares
Community	Communita	k1gFnSc2	Communita
College	Colleg	k1gMnSc2	Colleg
coby	coby	k?	coby
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
jejích	její	k3xOp3gInPc2	její
kampusů	kampus	k1gInPc2	kampus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
nachází	nacházet	k5eAaImIp3nS	nacházet
dalších	další	k2eAgNnPc2d1	další
13	[number]	k4	13
míst	místo	k1gNnPc2	místo
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Newt	Newt	k2eAgInSc1d1	Newt
Gingrich	Gingrich	k1gInSc1	Gingrich
(	(	kIx(	(
<g/>
*	*	kIx~	*
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
Glenn	Glenn	k1gMnSc1	Glenn
Branca	Branca	k?	Branca
(	(	kIx(	(
<g/>
*	*	kIx~	*
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avantgardní	avantgardní	k2eAgMnSc1d1	avantgardní
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
kytarista	kytarista	k1gMnSc1	kytarista
Dan	Dan	k1gMnSc1	Dan
Hartman	Hartman	k1gMnSc1	Hartman
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
-	-	kIx~	-
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
producent	producent	k1gMnSc1	producent
Ma	Ma	k1gMnSc1	Ma
<g/>
'	'	kIx"	'
<g/>
alot-Taršicha	alot-Taršich	k1gMnSc2	alot-Taršich
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
Pachuca	Pachuca	k1gFnSc1	Pachuca
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
</s>
