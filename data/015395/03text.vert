<s>
Mary	Mary	k1gFnSc1
Stewartová	Stewartová	k1gFnSc1
</s>
<s>
Mary	Mary	k1gFnSc1
StewartováOsobní	StewartováOsobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Narození	narození	k1gNnSc2
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1956	#num#	k4
(	(	kIx(
<g/>
65	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Stát	stát	k1gInSc1
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Kariéra	kariéra	k1gFnSc1
Disciplína	disciplína	k1gFnSc1
</s>
<s>
1500	#num#	k4
m	m	kA
Účasti	účast	k1gFnSc2
na	na	k7c4
LOH	LOH	kA
</s>
<s>
1976	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Halové	halový	k2eAgNnSc1d1
mistrovství	mistrovství	k1gNnSc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
atletice	atletika	k1gFnSc6
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
HME	HME	kA
1977	#num#	k4
</s>
<s>
běh	běh	k1gInSc1
na	na	k7c4
1500	#num#	k4
m	m	kA
</s>
<s>
Mary	Mary	k1gFnSc1
Stewartová	Stewartová	k1gFnSc1
(	(	kIx(
<g/>
provdaná	provdaný	k2eAgFnSc1d1
Cottonová	Cottonový	k2eAgFnSc1d1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
*	*	kIx~
25	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1956	#num#	k4
<g/>
,	,	kIx,
Birmingham	Birmingham	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bývalá	bývalý	k2eAgFnSc1d1
britská	britský	k2eAgFnSc1d1
atletka	atletka	k1gFnSc1
<g/>
,	,	kIx,
běžkyně	běžkyně	k1gFnSc1
<g/>
,	,	kIx,
halová	halový	k2eAgFnSc1d1
mistryně	mistryně	k1gFnSc1
Evropy	Evropa	k1gFnSc2
v	v	k7c6
běhu	běh	k1gInSc6
na	na	k7c4
1500	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Sportovní	sportovní	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Startovala	startovat	k5eAaBmAgFnS
na	na	k7c6
olympiádě	olympiáda	k1gFnSc6
v	v	k7c6
Montrealu	Montreal	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
běhu	běh	k1gInSc6
na	na	k7c4
1500	#num#	k4
metrů	metr	k1gInPc2
doběhla	doběhnout	k5eAaPmAgFnS
pátá	pátá	k1gFnSc1
v	v	k7c6
semifinále	semifinále	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následující	následující	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
evropskou	evropský	k2eAgFnSc7d1
halovou	halový	k2eAgFnSc7d1
mistryní	mistryně	k1gFnSc7
v	v	k7c6
běhu	běh	k1gInSc6
na	na	k7c4
1500	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Jejím	její	k3xOp3gMnSc7
starším	starý	k2eAgMnSc7d2
bratrem	bratr	k1gMnSc7
je	být	k5eAaImIp3nS
britský	britský	k2eAgMnSc1d1
běžec	běžec	k1gMnSc1
Ian	Ian	k1gMnSc1
Stewart	Stewart	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
zvítězil	zvítězit	k5eAaPmAgMnS
její	její	k3xOp3gMnSc1
syn	syn	k1gMnSc1
Adam	Adam	k1gMnSc1
Cotton	Cotton	k1gInSc4
v	v	k7c6
běhu	běh	k1gInSc6
na	na	k7c4
1500	#num#	k4
metrů	metr	k1gInPc2
na	na	k7c6
evropském	evropský	k2eAgInSc6d1
juniorském	juniorský	k2eAgInSc6d1
šampionátu	šampionát	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Mary	Mary	k1gFnSc1
Stewartová	Stewartová	k1gFnSc1
v	v	k7c6
databázi	databáze	k1gFnSc6
Olympedia	Olympedium	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normat	k5eAaBmAgInS,k5eAaImAgInS,k5eAaPmAgInS
<g/>
}	}	kIx)
Halové	halový	k2eAgFnPc4d1
mistryně	mistryně	k1gFnPc4
Evropy	Evropa	k1gFnSc2
v	v	k7c6
běhu	běh	k1gInSc6
na	na	k7c4
1	#num#	k4
500	#num#	k4
m	m	kA
</s>
<s>
1971	#num#	k4
<g/>
:	:	kIx,
Margaret	Margareta	k1gFnPc2
Beachamová	Beachamový	k2eAgFnSc1d1
•	•	k?
1972	#num#	k4
<g/>
:	:	kIx,
Tamara	Tamara	k1gFnSc1
Pangelovová	Pangelovová	k1gFnSc1
•	•	k?
1973	#num#	k4
<g/>
:	:	kIx,
Ellen	Ellen	k1gInSc1
Tittelová	Tittelová	k1gFnSc1
•	•	k?
1974	#num#	k4
<g/>
:	:	kIx,
Tonka	Tonka	k1gFnSc1
Petrovová	Petrovová	k1gFnSc1
•	•	k?
1975	#num#	k4
<g/>
:	:	kIx,
Natalia	Natalia	k1gFnSc1
Andreiová	Andreiový	k2eAgFnSc1d1
•	•	k?
1976	#num#	k4
<g/>
:	:	kIx,
Brigitte	Brigitte	k1gFnSc1
Krausová	Krausová	k1gFnSc1
•	•	k?
1977	#num#	k4
<g/>
:	:	kIx,
Mary	Mary	k1gFnSc1
Stewartová	Stewartová	k1gFnSc1
•	•	k?
1978	#num#	k4
<g/>
:	:	kIx,
Ileana	Ileana	k1gFnSc1
Silaiová	Silaiový	k2eAgFnSc1d1
•	•	k?
1979	#num#	k4
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Natalia	Natalia	k1gFnSc1
Marasescuová	Marasescuová	k1gFnSc1
•	•	k?
1980	#num#	k4
<g/>
:	:	kIx,
Tamara	Tamara	k1gFnSc1
Kobová	Kobová	k1gFnSc1
•	•	k?
1981	#num#	k4
<g/>
:	:	kIx,
Agnese	Agnese	k1gFnSc1
Possamaiová	Possamaiový	k2eAgFnSc1d1
•	•	k?
1982	#num#	k4
<g/>
:	:	kIx,
Gabriella	Gabriella	k1gFnSc1
Dorio	Dorio	k1gNnSc1
•	•	k?
1983	#num#	k4
<g/>
:	:	kIx,
Brigitte	Brigitte	k1gFnSc1
Krausová	Krausová	k1gFnSc1
•	•	k?
1984	#num#	k4
<g/>
:	:	kIx,
Fiț	Fiț	k2eAgFnSc1d1
Lovinová	Lovinová	k1gFnSc1
•	•	k?
1985	#num#	k4
<g/>
:	:	kIx,
Doina	Doien	k2eAgFnSc1d1
Melinteová	Melinteová	k1gFnSc1
•	•	k?
1986	#num#	k4
<g/>
:	:	kIx,
Světlana	Světlana	k1gFnSc1
Kitovová	Kitovová	k1gFnSc1
•	•	k?
1987	#num#	k4
<g/>
:	:	kIx,
Sandra	Sandra	k1gFnSc1
Gasserová	Gasserová	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1988	#num#	k4
<g/>
:	:	kIx,
Doina	Doien	k2eAgFnSc1d1
Melinteová	Melinteová	k1gFnSc1
•	•	k?
1989	#num#	k4
<g/>
:	:	kIx,
Paula	Paula	k1gFnSc1
Ivanová	Ivanová	k1gFnSc1
•	•	k?
1990	#num#	k4
<g/>
:	:	kIx,
Doina	Doien	k2eAgFnSc1d1
Melinteová	Melinteová	k1gFnSc1
•	•	k?
1992	#num#	k4
<g/>
:	:	kIx,
Jekatěrina	Jekatěrin	k2eAgFnSc1d1
Podkopajevová	Podkopajevová	k1gFnSc1
•	•	k?
1994	#num#	k4
<g/>
:	:	kIx,
Jekatěrina	Jekatěrin	k2eAgFnSc1d1
Podkopajevová	Podkopajevová	k1gFnSc1
•	•	k?
1996	#num#	k4
<g/>
:	:	kIx,
Carla	Carla	k1gFnSc1
Sacramentová	Sacramentový	k2eAgFnSc1d1
•	•	k?
1998	#num#	k4
<g/>
:	:	kIx,
Theresia	Theresia	k1gFnSc1
Kieslová	Kieslový	k2eAgFnSc1d1
•	•	k?
2000	#num#	k4
<g/>
:	:	kIx,
Violeta	Violeta	k1gFnSc1
Becleaová	Becleaová	k1gFnSc1
•	•	k?
2002	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
Jekatěrina	Jekatěrin	k2eAgFnSc1d1
Puzanovová	Puzanovová	k1gFnSc1
•	•	k?
2005	#num#	k4
<g/>
:	:	kIx,
Elena	Elena	k1gFnSc1
Iagărová	Iagărová	k1gFnSc1
•	•	k?
2007	#num#	k4
<g/>
:	:	kIx,
Lidia	Lidia	k1gFnSc1
Chojecká	Chojecká	k1gFnSc1
•	•	k?
2009	#num#	k4
<g/>
:	:	kIx,
Anna	Anna	k1gFnSc1
Alminovová	Alminovová	k1gFnSc1
•	•	k?
2011	#num#	k4
<g/>
:	:	kIx,
Jelena	Jelena	k1gFnSc1
Aržakovová	Aržakovová	k1gFnSc1
•	•	k?
2013	#num#	k4
<g/>
:	:	kIx,
Abeba	Abeba	k1gFnSc1
Aregawiová	Aregawiový	k2eAgFnSc1d1
•	•	k?
2015	#num#	k4
<g/>
:	:	kIx,
Sifan	Sifan	k1gInSc1
Hassanová	Hassanová	k1gFnSc1
•	•	k?
2017	#num#	k4
<g/>
:	:	kIx,
Laura	Laura	k1gFnSc1
Muirová	Muirová	k1gFnSc1
•	•	k?
2019	#num#	k4
<g/>
:	:	kIx,
Laura	Laura	k1gFnSc1
Muirová	Muirová	k1gFnSc1
•	•	k?
2021	#num#	k4
<g/>
:	:	kIx,
Elise	elise	k1gFnSc1
Vanderelstová	Vanderelstová	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
