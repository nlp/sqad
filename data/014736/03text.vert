<s>
Egyptská	egyptský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
</s>
<s>
Egyptská	egyptský	k2eAgFnSc1d1
libraج	libraج	k?
ا	ا	k?
(	(	kIx(
<g/>
arabsky	arabsky	k6eAd1
<g/>
)	)	kIx)
Mince	mince	k1gFnSc1
10	#num#	k4
piastrůZemě	piastrůZemě	k6eAd1
</s>
<s>
Egypt	Egypt	k1gInSc1
Egypt	Egypt	k1gInSc1
ISO	ISO	kA
4217	#num#	k4
</s>
<s>
EGP	EGP	kA
Inflace	inflace	k1gFnSc1
</s>
<s>
11	#num#	k4
<g/>
%	%	kIx~
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
2015	#num#	k4
odhad	odhad	k1gInSc1
<g/>
)	)	kIx)
Mince	mince	k1gFnSc1
</s>
<s>
25	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
piaster	piaster	k1gInSc1
Bankovky	bankovka	k1gFnSc2
</s>
<s>
1	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
<g/>
,	,	kIx,
200	#num#	k4
liber	libra	k1gFnPc2
</s>
<s>
Egyptská	egyptský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
(	(	kIx(
<g/>
arabsky	arabsky	k6eAd1
ج	ج	k?
ا	ا	k?
)	)	kIx)
je	být	k5eAaImIp3nS
zákonným	zákonný	k2eAgNnSc7d1
platidlem	platidlo	k1gNnSc7
afrického	africký	k2eAgInSc2d1
státu	stát	k1gInSc2
Egypt	Egypt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
setina	setina	k1gFnSc1
libry	libra	k1gFnSc2
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
piastr	piastr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISO	ISO	kA
4217	#num#	k4
kód	kód	k1gInSc1
egyptské	egyptský	k2eAgFnSc2d1
libry	libra	k1gFnSc2
je	být	k5eAaImIp3nS
EGP	EGP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
„	„	k?
<g/>
libra	libra	k1gFnSc1
<g/>
“	“	k?
má	mít	k5eAaImIp3nS
egyptská	egyptský	k2eAgFnSc1d1
měna	měna	k1gFnSc1
společný	společný	k2eAgInSc4d1
s	s	k7c7
měnami	měna	k1gFnPc7
několika	několik	k4yIc2
dalších	další	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
byly	být	k5eAaImAgInP
v	v	k7c6
minulosti	minulost	k1gFnSc6
součástí	součást	k1gFnSc7
britské	britský	k2eAgFnSc2d1
imperiální	imperiální	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Egyptská	egyptský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
praxi	praxe	k1gFnSc6
používá	používat	k5eAaImIp3nS
i	i	k9
na	na	k7c6
území	území	k1gNnSc6
Gazy	Gaza	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
zákonným	zákonný	k2eAgNnSc7d1
platidlem	platidlo	k1gNnSc7
izraelský	izraelský	k2eAgInSc4d1
šekel	šekel	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Mince	mince	k1gFnPc1
a	a	k8xC
bankovky	bankovka	k1gFnPc1
</s>
<s>
Současné	současný	k2eAgFnPc1d1
bankovky	bankovka	k1gFnPc1
v	v	k7c6
oběhu	oběh	k1gInSc6
mají	mít	k5eAaImIp3nP
nominální	nominální	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
25	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
piastrů	piastr	k1gInPc2
a	a	k8xC
1	#num#	k4
<g/>
,	,	kIx,
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
<g/>
,	,	kIx,
100	#num#	k4
a	a	k8xC
200	#num#	k4
liber	libra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bankovky	bankovka	k1gFnSc2
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
25	#num#	k4
piastrů	piastr	k1gInPc2
jsou	být	k5eAaImIp3nP
od	od	k7c2
r.	r.	kA
2009	#num#	k4
postupně	postupně	k6eAd1
stahovány	stahován	k2eAgInPc1d1
z	z	k7c2
oběhu	oběh	k1gInSc2
a	a	k8xC
nahrazovány	nahrazovat	k5eAaImNgInP
mincemi	mince	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mince	mince	k1gFnPc1
mají	mít	k5eAaImIp3nP
hodnoty	hodnota	k1gFnSc2
5	#num#	k4
<g/>
,	,	kIx,
10	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
<g/>
,	,	kIx,
25	#num#	k4
<g/>
,	,	kIx,
50	#num#	k4
piastrů	piastr	k1gInPc2
a	a	k8xC
1	#num#	k4
libra	libra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Aktuální	aktuální	k2eAgInSc1d1
kurz	kurz	k1gInSc1
měny	měna	k1gFnSc2
Egyptská	egyptský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
Podle	podle	k7c2
ČNB	ČNB	kA
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
Podle	podle	k7c2
Google	Google	k1gFnSc2
Finance	finance	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
EUR	euro	k1gNnPc2
USD	USD	kA
Podle	podle	k7c2
Kurzy	kurz	k1gInPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
(	(	kIx(
<g/>
Graf	graf	k1gInSc1
Banky	banka	k1gFnSc2
<g/>
)	)	kIx)
EUR	euro	k1gNnPc2
USD	USD	kA
Podle	podle	k7c2
Yahoo	Yahoo	k6eAd1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Finance	finance	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
CZK	CZK	kA
EUR	euro	k1gNnPc2
USD	USD	kA
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
INFLATION	INFLATION	kA
RATE	RATE	kA
(	(	kIx(
<g/>
CONSUMER	CONSUMER	kA
PRICES	PRICES	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CIA	CIA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1553	#num#	k4
<g/>
-	-	kIx~
<g/>
8133	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Egyptská	egyptský	k2eAgFnSc1d1
libra	libra	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Central	Centrat	k5eAaPmAgInS,k5eAaImAgInS
Bank	bank	k1gInSc1
of	of	k?
Egypt	Egypt	k1gInSc1
</s>
<s>
Bankovky	bankovka	k1gFnPc1
z	z	k7c2
Egypta	Egypt	k1gInSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
