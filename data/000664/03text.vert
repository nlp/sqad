<s>
Peter	Peter	k1gMnSc1	Peter
Zumthor	Zumthor	k1gMnSc1	Zumthor
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
Basilej	Basilej	k1gFnSc1	Basilej
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
architekt	architekt	k1gMnSc1	architekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
vyučil	vyučit	k5eAaPmAgMnS	vyučit
nábytkářem	nábytkář	k1gMnSc7	nábytkář
<g/>
,	,	kIx,	,
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
uměleckoprůmyslovou	uměleckoprůmyslový	k2eAgFnSc4d1	uměleckoprůmyslová
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Basileji	Basilej	k1gFnSc6	Basilej
a	a	k8xC	a
studoval	studovat	k5eAaImAgMnS	studovat
Pratt	Pratt	k1gMnSc1	Pratt
Institute	institut	k1gInSc5	institut
v	v	k7c6	v
New	New	k1gFnPc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
si	se	k3xPyFc3	se
otevřel	otevřít	k5eAaPmAgMnS	otevřít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
architektonickou	architektonický	k2eAgFnSc4d1	architektonická
kancelář	kancelář	k1gFnSc4	kancelář
v	v	k7c6	v
Haldensteinu	Haldensteino	k1gNnSc6	Haldensteino
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
externím	externí	k2eAgMnSc7d1	externí
profesorem	profesor	k1gMnSc7	profesor
na	na	k7c4	na
Southern	Southern	k1gInSc4	Southern
California	Californium	k1gNnSc2	Californium
Institute	institut	k1gInSc5	institut
of	of	k?	of
Architecture	Architectur	k1gMnSc5	Architectur
v	v	k7c6	v
Santa	Sant	k1gMnSc2	Sant
Monice	Monika	k1gFnSc6	Monika
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
Akademie	akademie	k1gFnSc2	akademie
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
je	být	k5eAaImIp3nS	být
čestným	čestný	k2eAgInSc7d1	čestný
členem	člen	k1gInSc7	člen
Spolku	spolek	k1gInSc2	spolek
německých	německý	k2eAgMnPc2d1	německý
architektů	architekt	k1gMnPc2	architekt
(	(	kIx(	(
<g/>
BDA	BDA	kA	BDA
<g/>
)	)	kIx)	)
a	a	k8xC	a
profesorem	profesor	k1gMnSc7	profesor
Academia	academia	k1gFnSc1	academia
di	di	k?	di
architettura	architettura	k1gFnSc1	architettura
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Medrisio	Medrisio	k6eAd1	Medrisio
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
:	:	kIx,	:
Termální	termální	k2eAgFnPc1d1	termální
lázně	lázeň	k1gFnPc1	lázeň
ve	v	k7c6	v
Valsu	Vals	k1gInSc6	Vals
Domov	domov	k1gInSc4	domov
pro	pro	k7c4	pro
seniory	senior	k1gMnPc4	senior
v	v	k7c6	v
Churu	Chur	k1gInSc6	Chur
Kaple	kaple	k1gFnSc2	kaple
sv.	sv.	kA	sv.
Benedikta	Benedikt	k1gMnSc2	Benedikt
v	v	k7c6	v
Sumvitgu	Sumvitg	k1gInSc6	Sumvitg
Německo	Německo	k1gNnSc1	Německo
<g/>
:	:	kIx,	:
Polní	polní	k2eAgFnSc1d1	polní
kaple	kaple	k1gFnSc1	kaple
bratra	bratr	k1gMnSc2	bratr
Klause	Klaus	k1gMnSc2	Klaus
ve	v	k7c6	v
Wachendorfu	Wachendorf	k1gInSc6	Wachendorf
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
:	:	kIx,	:
Umělecké	umělecký	k2eAgNnSc1d1	umělecké
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c4	v
Bregenz	Bregenz	k1gInSc4	Bregenz
Cena	cena	k1gFnSc1	cena
Heinricha	Heinrich	k1gMnSc2	Heinrich
Tessenowa	Tessenowus	k1gMnSc2	Tessenowus
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Prestižní	prestižní	k2eAgFnSc1d1	prestižní
Pritzkerova	Pritzkerův	k2eAgFnSc1d1	Pritzkerova
cena	cena	k1gFnSc1	cena
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Peter	Peter	k1gMnSc1	Peter
Zumthor	Zumthora	k1gFnPc2	Zumthora
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Peter	Peter	k1gMnSc1	Peter
Zumthor	Zumthor	k1gMnSc1	Zumthor
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Peter	Peter	k1gMnSc1	Peter
Zumthor	Zumthor	k1gMnSc1	Zumthor
ArchiWeb	ArchiWba	k1gFnPc2	ArchiWba
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Češi	Čech	k1gMnPc1	Čech
vydali	vydat	k5eAaPmAgMnP	vydat
unikátní	unikátní	k2eAgInSc4d1	unikátní
kalendář	kalendář	k1gInSc4	kalendář
nejslavnějších	slavný	k2eAgFnPc2d3	nejslavnější
staveb	stavba	k1gFnPc2	stavba
architekta	architekt	k1gMnSc2	architekt
Zumthora	Zumthor	k1gMnSc2	Zumthor
-	-	kIx~	-
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
Můžeme	moct	k5eAaImIp1nP	moct
dál	daleko	k6eAd2	daleko
<g/>
...	...	k?	...
Zumthor	Zumthor	k1gMnSc1	Zumthor
<g/>
,	,	kIx,	,
pan	pan	k1gMnSc1	pan
Architekt	architekt	k1gMnSc1	architekt
-	-	kIx~	-
Akad	Akad	k1gMnSc1	Akad
<g/>
.	.	kIx.	.
arch	arch	k1gInSc1	arch
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Vávra	Vávra	k1gMnSc1	Vávra
<g/>
,	,	kIx,	,
ČRo	ČRo	k1gMnSc1	ČRo
Leonardo	Leonardo	k1gMnSc1	Leonardo
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
9.03	[number]	k4	9.03
<g/>
.2009	.2009	k4	.2009
</s>
