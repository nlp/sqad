<s>
Roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
jen	jen	k9	jen
v	v	k7c6	v
nejsevernějších	severní	k2eAgFnPc6d3	nejsevernější
částech	část	k1gFnPc6	část
Skandinávského	skandinávský	k2eAgInSc2d1	skandinávský
poloostrova	poloostrov	k1gInSc2	poloostrov
a	a	k8xC	a
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Uralu	Ural	k1gInSc2	Ural
<g/>
.	.	kIx.	.
</s>
