<s>
Národní	národní	k2eAgFnSc1d1	národní
hokejová	hokejový	k2eAgFnSc1d1	hokejová
liga	liga	k1gFnSc1	liga
(	(	kIx(	(
<g/>
NHL	NHL	kA	NHL
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
Národní	národní	k2eAgFnSc1d1	národní
hokejová	hokejový	k2eAgFnSc1d1	hokejová
liga	liga	k1gFnSc1	liga
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
Ligue	Ligue	k1gInSc1	Ligue
nationale	nationale	k6eAd1	nationale
de	de	k?	de
hockey	hockea	k1gFnSc2	hockea
-	-	kIx~	-
LNH	LNH	kA	LNH
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejprestižnější	prestižní	k2eAgFnSc7d3	nejprestižnější
hokejovou	hokejový	k2eAgFnSc7d1	hokejová
ligou	liga	k1gFnSc7	liga
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Účastní	účastnit	k5eAaImIp3nS	účastnit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
pouze	pouze	k6eAd1	pouze
týmy	tým	k1gInPc4	tým
z	z	k7c2	z
USA	USA	kA	USA
a	a	k8xC	a
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
ji	on	k3xPp3gFnSc4	on
30	[number]	k4	30
mužstev	mužstvo	k1gNnPc2	mužstvo
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
jsou	být	k5eAaImIp3nP	být
rozdělena	rozdělit	k5eAaPmNgNnP	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
konferencí	konference	k1gFnPc2	konference
<g/>
,	,	kIx,	,
východní	východní	k2eAgInPc4d1	východní
a	a	k8xC	a
západní	západní	k2eAgInPc4d1	západní
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
konference	konference	k1gFnPc1	konference
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k6eAd1	ještě
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
celkem	celkem	k6eAd1	celkem
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
divize	divize	k1gFnPc4	divize
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
konferenci	konference	k1gFnSc6	konference
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc1	tento
divize	divize	k1gFnPc1	divize
<g/>
:	:	kIx,	:
atlantická	atlantický	k2eAgFnSc1d1	Atlantická
a	a	k8xC	a
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
konferenci	konference	k1gFnSc6	konference
jsou	být	k5eAaImIp3nP	být
divize	divize	k1gFnPc1	divize
<g/>
:	:	kIx,	:
pacifická	pacifický	k2eAgFnSc1d1	Pacifická
a	a	k8xC	a
centrální	centrální	k2eAgFnSc1d1	centrální
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
divizi	divize	k1gFnSc6	divize
osm	osm	k4xCc4	osm
týmů	tým	k1gInPc2	tým
a	a	k8xC	a
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
sedm	sedm	k4xCc1	sedm
<g/>
.	.	kIx.	.
</s>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
základní	základní	k2eAgFnSc1d1	základní
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
každé	každý	k3xTgNnSc4	každý
družstvo	družstvo	k1gNnSc4	družstvo
odehraje	odehrát	k5eAaPmIp3nS	odehrát
82	[number]	k4	82
utkání	utkání	k1gNnPc2	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
základní	základní	k2eAgFnSc2d1	základní
části	část	k1gFnSc2	část
nastává	nastávat	k5eAaImIp3nS	nastávat
druhé	druhý	k4xOgNnSc1	druhý
kolo	kolo	k1gNnSc1	kolo
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
play	play	k0	play
off	off	k?	off
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
playoff	playoff	k1gMnSc1	playoff
postupuje	postupovat	k5eAaImIp3nS	postupovat
osm	osm	k4xCc1	osm
nejlepších	dobrý	k2eAgNnPc2d3	nejlepší
družstev	družstvo	k1gNnPc2	družstvo
z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
konference	konference	k1gFnSc2	konference
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
hrají	hrát	k5eAaImIp3nP	hrát
série	série	k1gFnSc2	série
na	na	k7c4	na
4	[number]	k4	4
vítězná	vítězný	k2eAgNnPc1d1	vítězné
utkání	utkání	k1gNnPc1	utkání
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
mužstvo	mužstvo	k1gNnSc4	mužstvo
východní	východní	k2eAgFnSc2d1	východní
konference	konference	k1gFnSc2	konference
hraje	hrát	k5eAaImIp3nS	hrát
s	s	k7c7	s
8	[number]	k4	8
<g/>
.	.	kIx.	.
týmem	tým	k1gInSc7	tým
východní	východní	k2eAgFnSc2d1	východní
konference	konference	k1gFnSc2	konference
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
hraje	hrát	k5eAaImIp3nS	hrát
se	s	k7c7	s
7	[number]	k4	7
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
hraje	hrát	k5eAaImIp3nS	hrát
s	s	k7c7	s
6	[number]	k4	6
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
s	s	k7c7	s
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Úplně	úplně	k6eAd1	úplně
stejně	stejně	k6eAd1	stejně
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
play	play	k0	play
off	off	k?	off
i	i	k9	i
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
konferenci	konference	k1gFnSc6	konference
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
zbudou	zbýt	k5eAaPmIp3nP	zbýt
dvě	dva	k4xCgNnPc1	dva
družstva	družstvo	k1gNnPc1	družstvo
(	(	kIx(	(
<g/>
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
a	a	k8xC	a
druhé	druhý	k4xOgMnPc4	druhý
ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
<g/>
)	)	kIx)	)
a	a	k8xC	a
sehrají	sehrát	k5eAaPmIp3nP	sehrát
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
finálovou	finálový	k2eAgFnSc4d1	finálová
sérii	série	k1gFnSc4	série
na	na	k7c4	na
4	[number]	k4	4
vítězné	vítězný	k2eAgInPc4d1	vítězný
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Vítěz	vítěz	k1gMnSc1	vítěz
získává	získávat	k5eAaImIp3nS	získávat
trofej	trofej	k1gFnSc4	trofej
pro	pro	k7c4	pro
vítěze	vítěz	k1gMnSc4	vítěz
-	-	kIx~	-
Stanley	Stanlea	k1gFnSc2	Stanlea
Cup	cup	k1gInSc1	cup
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sezóně	sezóna	k1gFnSc6	sezóna
probíhá	probíhat	k5eAaImIp3nS	probíhat
draft	draft	k1gInSc1	draft
nováčků	nováček	k1gMnPc2	nováček
a	a	k8xC	a
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
hráči	hráč	k1gMnPc1	hráč
uplynulé	uplynulý	k2eAgFnSc2d1	uplynulá
sezóny	sezóna	k1gFnSc2	sezóna
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
odměněni	odměnit	k5eAaPmNgMnP	odměnit
některou	některý	k3yIgFnSc7	některý
z	z	k7c2	z
trofejí	trofej	k1gFnPc2	trofej
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Historie	historie	k1gFnSc2	historie
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
Proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
série	série	k1gFnSc1	série
jednání	jednání	k1gNnSc2	jednání
v	v	k7c6	v
kanadské	kanadský	k2eAgFnSc6d1	kanadská
National	National	k1gFnSc6	National
Hockey	Hockea	k1gFnSc2	Hockea
Association	Association	k1gInSc1	Association
(	(	kIx(	(
<g/>
NHA	NHA	kA	NHA
<g/>
)	)	kIx)	)
mezi	mezi	k7c4	mezi
Eddie	Eddius	k1gMnPc4	Eddius
Livingstonem	Livingston	k1gInSc7	Livingston
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
vlastníkem	vlastník	k1gMnSc7	vlastník
Toronto	Toronto	k1gNnSc1	Toronto
Blueshirts	Blueshirts	k1gInSc4	Blueshirts
<g/>
,	,	kIx,	,
a	a	k8xC	a
ostatními	ostatní	k2eAgMnPc7d1	ostatní
vlastníky	vlastník	k1gMnPc7	vlastník
-	-	kIx~	-
vlastníky	vlastník	k1gMnPc7	vlastník
Montreal	Montreal	k1gInSc1	Montreal
Canadiens	Canadiens	k1gInSc1	Canadiens
<g/>
,	,	kIx,	,
Montreal	Montreal	k1gInSc1	Montreal
Wanderers	Wanderers	k1gInSc1	Wanderers
<g/>
,	,	kIx,	,
Toronto	Toronto	k1gNnSc1	Toronto
Maple	Maple	k1gFnSc2	Maple
Leafs	Leafs	k1gInSc1	Leafs
a	a	k8xC	a
Quebec	Quebec	k1gInSc1	Quebec
Bulldogs	Bulldogsa	k1gFnPc2	Bulldogsa
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
jmenovaní	jmenovaný	k1gMnPc1	jmenovaný
se	se	k3xPyFc4	se
sešli	sejít	k5eAaPmAgMnP	sejít
ve	v	k7c4	v
Windsor	Windsor	k1gInSc4	Windsor
Hotelu	hotel	k1gInSc2	hotel
v	v	k7c6	v
Montrealu	Montreal	k1gInSc6	Montreal
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jednali	jednat	k5eAaImAgMnP	jednat
o	o	k7c6	o
budoucnosti	budoucnost	k1gFnSc6	budoucnost
NHA	NHA	kA	NHA
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
diskuse	diskuse	k1gFnSc1	diskuse
dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
Bývalí	bývalý	k2eAgMnPc1d1	bývalý
členové	člen	k1gMnPc1	člen
NHA	NHA	kA	NHA
-	-	kIx~	-
Montreal	Montreal	k1gInSc1	Montreal
Canadiens	Canadiens	k1gInSc1	Canadiens
<g/>
,	,	kIx,	,
Montreal	Montreal	k1gInSc1	Montreal
Wanderers	Wanderers	k1gInSc1	Wanderers
a	a	k8xC	a
Toronto	Toronto	k1gNnSc1	Toronto
Maple	Maple	k1gFnSc2	Maple
Leafs	Leafsa	k1gFnPc2	Leafsa
byli	být	k5eAaImAgMnP	být
zakládajícími	zakládající	k2eAgInPc7d1	zakládající
členy	člen	k1gInPc7	člen
NHL	NHL	kA	NHL
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
týmem	tým	k1gInSc7	tým
Toronto	Toronto	k1gNnSc4	Toronto
Arenas	Arenasa	k1gFnPc2	Arenasa
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
desetiletí	desetiletí	k1gNnSc6	desetiletí
liga	liga	k1gFnSc1	liga
těžce	těžce	k6eAd1	těžce
bojovala	bojovat	k5eAaImAgFnS	bojovat
o	o	k7c4	o
přežití	přežití	k1gNnSc4	přežití
<g/>
,	,	kIx,	,
týmy	tým	k1gInPc1	tým
NHL	NHL	kA	NHL
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
ledě	led	k1gInSc6	led
velmi	velmi	k6eAd1	velmi
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
NHL	NHL	kA	NHL
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
že	že	k8xS	že
tým	tým	k1gInSc1	tým
z	z	k7c2	z
NHL	NHL	kA	NHL
nevyhrál	vyhrát	k5eNaPmAgInS	vyhrát
Stanley	Stanlea	k1gFnSc2	Stanlea
Cup	cup	k1gInSc1	cup
a	a	k8xC	a
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
ho	on	k3xPp3gInSc4	on
tým	tým	k1gInSc4	tým
z	z	k7c2	z
konkurenčních	konkurenční	k2eAgFnPc2d1	konkurenční
lig	liga	k1gFnPc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
již	již	k6eAd1	již
pak	pak	k6eAd1	pak
po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
konkurenčních	konkurenční	k2eAgFnPc2d1	konkurenční
lig	liga	k1gFnPc2	liga
patřil	patřit	k5eAaImAgInS	patřit
Stanley	Stanlea	k1gFnSc2	Stanlea
Cup	cup	k1gInSc1	cup
pouze	pouze	k6eAd1	pouze
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
započal	započnout	k5eAaPmAgInS	započnout
proces	proces	k1gInSc1	proces
rozšiřování	rozšiřování	k1gNnSc2	rozšiřování
<g/>
:	:	kIx,	:
Boston	Boston	k1gInSc1	Boston
Bruins	Bruins	k1gInSc1	Bruins
(	(	kIx(	(
<g/>
první	první	k4xOgInSc1	první
tým	tým	k1gInSc1	tým
z	z	k7c2	z
USA	USA	kA	USA
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
hrál	hrát	k5eAaImAgMnS	hrát
NHL	NHL	kA	NHL
<g/>
)	)	kIx)	)
a	a	k8xC	a
Montreal	Montreal	k1gInSc1	Montreal
Maroons	Maroonsa	k1gFnPc2	Maroonsa
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
do	do	k7c2	do
ligy	liga	k1gFnSc2	liga
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1924	[number]	k4	1924
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
;	;	kIx,	;
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Americans	Americans	k1gInSc1	Americans
a	a	k8xC	a
Pittsburgh	Pittsburgh	k1gInSc1	Pittsburgh
Pirates	Piratesa	k1gFnPc2	Piratesa
se	se	k3xPyFc4	se
přidali	přidat	k5eAaPmAgMnP	přidat
o	o	k7c4	o
sezónu	sezóna	k1gFnSc4	sezóna
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1926-27	[number]	k4	1926-27
vstoupily	vstoupit	k5eAaPmAgInP	vstoupit
další	další	k2eAgInPc1d1	další
týmy	tým	k1gInPc1	tým
-	-	kIx~	-
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Rangers	Rangers	k1gInSc1	Rangers
<g/>
,	,	kIx,	,
Chicago	Chicago	k1gNnSc1	Chicago
Black	Blacka	k1gFnPc2	Blacka
Hawks	Hawksa	k1gFnPc2	Hawksa
<g/>
,	,	kIx,	,
a	a	k8xC	a
Detroit	Detroit	k1gInSc1	Detroit
Cougars	Cougarsa	k1gFnPc2	Cougarsa
(	(	kIx(	(
<g/>
nynější	nynější	k2eAgInSc1d1	nynější
Detroit	Detroit	k1gInSc1	Detroit
Red	Red	k1gFnSc2	Red
Wings	Wingsa	k1gFnPc2	Wingsa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
NHL	NHL	kA	NHL
tak	tak	k6eAd1	tak
měla	mít	k5eAaImAgFnS	mít
10	[number]	k4	10
účastníků	účastník	k1gMnPc2	účastník
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
následnou	následný	k2eAgFnSc7d1	následná
2	[number]	k4	2
<g/>
.	.	kIx.	.
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
zapříčinila	zapříčinit	k5eAaPmAgFnS	zapříčinit
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
ligy	liga	k1gFnSc2	liga
musely	muset	k5eAaImAgInP	muset
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
času	čas	k1gInSc2	čas
odejít	odejít	k5eAaPmF	odejít
4	[number]	k4	4
týmy	tým	k1gInPc1	tým
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
z	z	k7c2	z
finančních	finanční	k2eAgInPc2d1	finanční
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
tak	tak	k6eAd1	tak
ligu	liga	k1gFnSc4	liga
hrála	hrát	k5eAaImAgFnS	hrát
šestice	šestice	k1gFnSc1	šestice
týmů	tým	k1gInPc2	tým
(	(	kIx(	(
<g/>
Montreal	Montreal	k1gInSc1	Montreal
Canadiens	Canadiens	k1gInSc1	Canadiens
<g/>
,	,	kIx,	,
Toronto	Toronto	k1gNnSc1	Toronto
Maple	Maple	k1gFnSc2	Maple
Leafs	Leafs	k1gInSc1	Leafs
<g/>
,	,	kIx,	,
Detroit	Detroit	k1gInSc1	Detroit
Red	Red	k1gFnPc2	Red
Wings	Wings	k1gInSc1	Wings
<g/>
,	,	kIx,	,
Chicago	Chicago	k1gNnSc1	Chicago
Black	Blacka	k1gFnPc2	Blacka
Hawks	Hawksa	k1gFnPc2	Hawksa
<g/>
,	,	kIx,	,
Boston	Boston	k1gInSc1	Boston
Bruins	Bruins	k1gInSc1	Bruins
a	a	k8xC	a
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Rangers	Rangers	k1gInSc1	Rangers
<g/>
)	)	kIx)	)
zvaná	zvaný	k2eAgNnPc1d1	zvané
jako	jako	k8xC	jako
Original	Original	k1gMnSc1	Original
Six	Six	k1gMnSc1	Six
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
čtvrt	čtvrt	k1gFnSc4	čtvrt
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
osazenstvo	osazenstvo	k1gNnSc1	osazenstvo
ligy	liga	k1gFnSc2	liga
neměnilo	měnit	k5eNaImAgNnS	měnit
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1958	[number]	k4	1958
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
důležitý	důležitý	k2eAgInSc1d1	důležitý
milník	milník	k1gInSc1	milník
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Willie	Willie	k1gFnPc1	Willie
O	o	k7c6	o
<g/>
'	'	kIx"	'
<g/>
Ree	Rea	k1gFnSc6	Rea
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
hráčem	hráč	k1gMnSc7	hráč
černé	černá	k1gFnSc2	černá
pleti	pleť	k1gFnSc2	pleť
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
NHL	NHL	kA	NHL
konkurence	konkurence	k1gFnSc1	konkurence
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
Western	Western	kA	Western
Hockey	Hockea	k1gMnSc2	Hockea
League	Leagu	k1gMnSc2	Leagu
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
plánovala	plánovat	k5eAaImAgFnS	plánovat
prohlásit	prohlásit	k5eAaPmF	prohlásit
za	za	k7c4	za
hlavní	hlavní	k2eAgFnSc4d1	hlavní
ligu	liga	k1gFnSc4	liga
a	a	k8xC	a
každoročně	každoročně	k6eAd1	každoročně
vyzývat	vyzývat	k5eAaImF	vyzývat
NHL	NHL	kA	NHL
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
o	o	k7c4	o
Stanley	Stanle	k1gMnPc4	Stanle
Cup	cup	k1gInSc4	cup
<g/>
.	.	kIx.	.
</s>
<s>
NHL	NHL	kA	NHL
se	se	k3xPyFc4	se
tuto	tento	k3xDgFnSc4	tento
hrozbu	hrozba	k1gFnSc4	hrozba
snažila	snažit	k5eAaImAgFnS	snažit
odvrátit	odvrátit	k5eAaPmF	odvrátit
rozšířením	rozšíření	k1gNnSc7	rozšíření
o	o	k7c4	o
šestici	šestice	k1gFnSc4	šestice
týmů	tým	k1gInPc2	tým
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1967	[number]	k4	1967
<g/>
-	-	kIx~	-
<g/>
68	[number]	k4	68
<g/>
.	.	kIx.	.
</s>
<s>
Novými	nový	k2eAgInPc7d1	nový
týmy	tým	k1gInPc7	tým
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
<g/>
:	:	kIx,	:
Los	los	k1gInSc1	los
Angeles	Angelesa	k1gFnPc2	Angelesa
Kings	Kings	k1gInSc1	Kings
<g/>
,	,	kIx,	,
Oakland	Oakland	k1gInSc1	Oakland
Seals	Seals	k1gInSc1	Seals
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
zrušen	zrušen	k2eAgInSc1d1	zrušen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pittsburgh	Pittsburgh	k1gInSc1	Pittsburgh
Penguins	Penguins	k1gInSc1	Penguins
<g/>
,	,	kIx,	,
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
Flyers	Flyers	k1gInSc1	Flyers
<g/>
,	,	kIx,	,
Minnesota	Minnesota	k1gFnSc1	Minnesota
North	North	k1gMnSc1	North
Stars	Stars	k1gInSc1	Stars
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
přestěhován	přestěhovat	k5eAaPmNgInS	přestěhovat
do	do	k7c2	do
Dallasu	Dallas	k1gInSc2	Dallas
<g/>
,	,	kIx,	,
přejmenován	přejmenován	k2eAgInSc1d1	přejmenován
na	na	k7c4	na
Dallas	Dallas	k1gInSc4	Dallas
Stars	Stars	k1gInSc4	Stars
<g/>
)	)	kIx)	)
a	a	k8xC	a
St.	st.	kA	st.
Louis	louis	k1gInSc4	louis
Blues	blues	k1gNnSc2	blues
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
do	do	k7c2	do
ligy	liga	k1gFnSc2	liga
připojily	připojit	k5eAaPmAgInP	připojit
týmy	tým	k1gInPc1	tým
Vancouver	Vancouvra	k1gFnPc2	Vancouvra
Canucks	Canucksa	k1gFnPc2	Canucksa
a	a	k8xC	a
Buffalo	Buffalo	k1gFnPc2	Buffalo
Sabres	Sabresa	k1gFnPc2	Sabresa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
World	World	k1gMnSc1	World
Hockey	Hockea	k1gFnSc2	Hockea
Association	Association	k1gInSc1	Association
(	(	kIx(	(
<g/>
WHA	WHA	kA	WHA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
NHL	NHL	kA	NHL
vážným	vážný	k1gMnSc7	vážný
konkurentem	konkurent	k1gMnSc7	konkurent
<g/>
.	.	kIx.	.
</s>
<s>
NHL	NHL	kA	NHL
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
pospíšit	pospíšit	k5eAaPmF	pospíšit
si	se	k3xPyFc3	se
s	s	k7c7	s
dalším	další	k2eAgNnSc7d1	další
rozšířením	rozšíření	k1gNnSc7	rozšíření
o	o	k7c4	o
celky	celek	k1gInPc4	celek
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
Islanders	Islanders	k1gInSc1	Islanders
a	a	k8xC	a
Atlanta	Atlanta	k1gFnSc1	Atlanta
Flames	Flamesa	k1gFnPc2	Flamesa
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
Calgary	Calgary	k1gNnSc1	Calgary
Flames	Flamesa	k1gFnPc2	Flamesa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zabránila	zabránit	k5eAaPmAgFnS	zabránit
WHA	WHA	kA	WHA
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zmocnila	zmocnit	k5eAaPmAgFnS	zmocnit
nových	nový	k2eAgInPc2d1	nový
stadionů	stadion	k1gInPc2	stadion
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgInP	být
přidány	přidat	k5eAaPmNgInP	přidat
týmy	tým	k1gInPc1	tým
Kansas	Kansas	k1gInSc1	Kansas
City	city	k1gNnSc1	city
Scouts	Scoutsa	k1gFnPc2	Scoutsa
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
Colorado	Colorado	k1gNnSc1	Colorado
Rockies	Rockiesa	k1gFnPc2	Rockiesa
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
New	New	k1gFnSc2	New
Jersey	Jersea	k1gFnSc2	Jersea
Devils	Devilsa	k1gFnPc2	Devilsa
<g/>
)	)	kIx)	)
a	a	k8xC	a
Washington	Washington	k1gInSc1	Washington
Capitals	Capitalsa	k1gFnPc2	Capitalsa
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
ligy	liga	k1gFnPc1	liga
bojovaly	bojovat	k5eAaImAgFnP	bojovat
o	o	k7c4	o
hráče	hráč	k1gMnPc4	hráč
a	a	k8xC	a
fanoušky	fanoušek	k1gMnPc4	fanoušek
až	až	k9	až
do	do	k7c2	do
zániku	zánik	k1gInSc2	zánik
WHA	WHA	kA	WHA
po	po	k7c6	po
sezóně	sezóna	k1gFnSc6	sezóna
1978	[number]	k4	1978
<g/>
-	-	kIx~	-
<g/>
79	[number]	k4	79
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sezóně	sezóna	k1gFnSc6	sezóna
se	se	k3xPyFc4	se
NHL	NHL	kA	NHL
zmenšila	zmenšit	k5eAaPmAgFnS	zmenšit
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
tým	tým	k1gInSc4	tým
<g/>
,	,	kIx,	,
sloučením	sloučení	k1gNnSc7	sloučení
Cleveland	Clevelanda	k1gFnPc2	Clevelanda
Barons	Barons	k1gInSc4	Barons
a	a	k8xC	a
Minnesota	Minnesota	k1gFnSc1	Minnesota
North	Northa	k1gFnPc2	Northa
Stars	Starsa	k1gFnPc2	Starsa
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgInPc1	čtyři
týmy	tým	k1gInPc1	tým
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
členů	člen	k1gMnPc2	člen
WHA	WHA	kA	WHA
se	se	k3xPyFc4	se
od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
1979-80	[number]	k4	1979-80
přidaly	přidat	k5eAaPmAgInP	přidat
do	do	k7c2	do
NHL	NHL	kA	NHL
<g/>
:	:	kIx,	:
Hartford	Hartford	k1gInSc1	Hartford
Whalers	Whalersa	k1gFnPc2	Whalersa
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
Carolina	Carolina	k1gFnSc1	Carolina
Hurricanes	Hurricanesa	k1gFnPc2	Hurricanesa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Quebec	Quebec	k1gMnSc1	Quebec
Nordiques	Nordiques	k1gMnSc1	Nordiques
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
Colorado	Colorado	k1gNnSc1	Colorado
Avalanche	Avalanche	k1gFnPc2	Avalanche
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Edmonton	Edmonton	k1gInSc1	Edmonton
Oilers	Oilersa	k1gFnPc2	Oilersa
<g/>
,	,	kIx,	,
a	a	k8xC	a
Winnipeg	Winnipeg	k1gInSc1	Winnipeg
Jets	Jetsa	k1gFnPc2	Jetsa
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
Phoenix	Phoenix	k1gInSc1	Phoenix
Coyotes	Coyotes	k1gInSc4	Coyotes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozšíření	rozšíření	k1gNnSc6	rozšíření
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
byla	být	k5eAaImAgFnS	být
NHL	NHL	kA	NHL
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
konferencí	konference	k1gFnPc2	konference
(	(	kIx(	(
<g/>
konference	konference	k1gFnSc1	konference
Clarence	Clarence	k1gFnSc1	Clarence
Campbella	Campbella	k1gFnSc1	Campbella
-	-	kIx~	-
západ	západ	k1gInSc1	západ
a	a	k8xC	a
konference	konference	k1gFnSc1	konference
Prince	princ	k1gMnSc2	princ
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
-	-	kIx~	-
východ	východ	k1gInSc1	východ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
konference	konference	k1gFnPc1	konference
měly	mít	k5eAaImAgFnP	mít
dvě	dva	k4xCgFnPc4	dva
divize	divize	k1gFnPc4	divize
<g/>
.	.	kIx.	.
</s>
<s>
Divize	divize	k1gFnSc1	divize
v	v	k7c6	v
Campbellově	Campbellův	k2eAgFnSc6d1	Campbellova
konferenci	konference	k1gFnSc6	konference
se	se	k3xPyFc4	se
jmenovaly	jmenovat	k5eAaImAgInP	jmenovat
divize	divize	k1gFnSc2	divize
Lestera	Lester	k1gMnSc2	Lester
Patricka	Patricko	k1gNnSc2	Patricko
a	a	k8xC	a
divize	divize	k1gFnSc2	divize
Conna	Conna	k1gFnSc1	Conna
Smytha	Smytha	k1gFnSc1	Smytha
<g/>
;	;	kIx,	;
divize	divize	k1gFnSc1	divize
v	v	k7c6	v
konferenci	konference	k1gFnSc6	konference
Prince	princ	k1gMnSc2	princ
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
se	se	k3xPyFc4	se
jmenovaly	jmenovat	k5eAaBmAgFnP	jmenovat
divize	divize	k1gFnSc1	divize
Jacka	Jacka	k1gFnSc1	Jacka
Adamse	Adamse	k1gFnSc1	Adamse
a	a	k8xC	a
divize	divize	k1gFnSc1	divize
Jamese	Jamese	k1gFnSc1	Jamese
Norrise	Norrise	k1gFnSc1	Norrise
(	(	kIx(	(
<g/>
Norrisova	Norrisův	k2eAgFnSc1d1	Norrisova
a	a	k8xC	a
Patrickova	Patrickův	k2eAgFnSc1d1	Patrickova
divize	divize	k1gFnSc1	divize
byla	být	k5eAaImAgFnS	být
vzájemně	vzájemně	k6eAd1	vzájemně
přehozena	přehodit	k5eAaPmNgFnS	přehodit
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1981	[number]	k4	1981
<g/>
-	-	kIx~	-
<g/>
82	[number]	k4	82
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
změnily	změnit	k5eAaPmAgInP	změnit
názvy	název	k1gInPc1	název
divizí	divize	k1gFnPc2	divize
a	a	k8xC	a
konferencí	konference	k1gFnPc2	konference
podle	podle	k7c2	podle
geografických	geografický	k2eAgInPc2d1	geografický
názvů	název	k1gInPc2	název
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
1998-99	[number]	k4	1998-99
má	mít	k5eAaImIp3nS	mít
každá	každý	k3xTgFnSc1	každý
konference	konference	k1gFnSc2	konference
tři	tři	k4xCgFnPc4	tři
divize	divize	k1gFnPc4	divize
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stabilním	stabilní	k2eAgNnSc6d1	stabilní
období	období	k1gNnSc6	období
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
byla	být	k5eAaImAgFnS	být
NHL	NHL	kA	NHL
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
o	o	k7c4	o
9	[number]	k4	9
týmů	tým	k1gInPc2	tým
během	během	k7c2	během
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
San	San	k1gMnSc2	San
Jose	Jose	k1gFnSc1	Jose
Sharks	Sharks	k1gInSc1	Sharks
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgInS	přidat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
o	o	k7c4	o
sezónu	sezóna	k1gFnSc4	sezóna
později	pozdě	k6eAd2	pozdě
Ottawa	Ottawa	k1gFnSc1	Ottawa
Senators	Senatorsa	k1gFnPc2	Senatorsa
a	a	k8xC	a
Tampa	Tampa	k1gFnSc1	Tampa
Bay	Bay	k1gMnPc2	Bay
Lightning	Lightning	k1gInSc4	Lightning
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
Anaheim	Anaheimo	k1gNnPc2	Anaheimo
Ducks	Ducksa	k1gFnPc2	Ducksa
a	a	k8xC	a
Florida	Florida	k1gFnSc1	Florida
Panthers	Panthersa	k1gFnPc2	Panthersa
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
Nashville	Nashvill	k1gMnSc2	Nashvill
Predators	Predators	k1gInSc4	Predators
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
Atlanta	Atlanta	k1gFnSc1	Atlanta
Thrashers	Thrashersa	k1gFnPc2	Thrashersa
a	a	k8xC	a
celky	celek	k1gInPc1	celek
Minnesota	Minnesota	k1gFnSc1	Minnesota
Wild	Wild	k1gInSc1	Wild
a	a	k8xC	a
Columbus	Columbus	k1gInSc1	Columbus
Blue	Blue	k1gNnSc2	Blue
Jackets	Jacketsa	k1gFnPc2	Jacketsa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
tým	tým	k1gInSc1	tým
Atlanta	Atlant	k1gMnSc4	Atlant
Thrashers	Thrashers	k1gInSc1	Thrashers
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Winnipegu	Winnipeg	k1gInSc2	Winnipeg
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Winnipeg	Winnipega	k1gFnPc2	Winnipega
Jets	Jetsa	k1gFnPc2	Jetsa
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Lední	lední	k2eAgInSc4d1	lední
hokej	hokej	k1gInSc4	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
zápas	zápas	k1gInSc1	zápas
NHL	NHL	kA	NHL
trvá	trvat	k5eAaImIp3nS	trvat
60	[number]	k4	60
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Utkání	utkání	k1gNnSc1	utkání
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
dvacetiminutových	dvacetiminutový	k2eAgFnPc2d1	dvacetiminutová
třetin	třetina	k1gFnPc2	třetina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
každé	každý	k3xTgFnSc6	každý
třetině	třetina	k1gFnSc6	třetina
je	být	k5eAaImIp3nS	být
sedmnáctiminutová	sedmnáctiminutový	k2eAgFnSc1d1	sedmnáctiminutová
přestávka	přestávka	k1gFnSc1	přestávka
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
evropských	evropský	k2eAgFnPc2d1	Evropská
soutěží	soutěž	k1gFnPc2	soutěž
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
od	od	k7c2	od
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
poslední	poslední	k2eAgMnSc1d1	poslední
hráč	hráč	k1gMnSc1	hráč
opustí	opustit	k5eAaPmIp3nS	opustit
hřiště	hřiště	k1gNnSc4	hřiště
-	-	kIx~	-
nikoliv	nikoliv	k9	nikoliv
od	od	k7c2	od
skončení	skončení	k1gNnSc2	skončení
třetiny	třetina	k1gFnSc2	třetina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
60	[number]	k4	60
minutách	minuta	k1gFnPc6	minuta
hracího	hrací	k2eAgInSc2d1	hrací
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
vítězem	vítěz	k1gMnSc7	vítěz
ten	ten	k3xDgInSc4	ten
tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
více	hodně	k6eAd2	hodně
branek	branka	k1gFnPc2	branka
než	než	k8xS	než
soupeř	soupeř	k1gMnSc1	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc1	stav
nerozhodný	rozhodný	k2eNgInSc1d1	nerozhodný
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
prodloužení	prodloužení	k1gNnSc4	prodloužení
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
základní	základní	k2eAgFnSc2d1	základní
části	část	k1gFnSc2	část
trvá	trvat	k5eAaImIp3nS	trvat
prodloužení	prodloužení	k1gNnSc1	prodloužení
pět	pět	k4xCc4	pět
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
týmy	tým	k1gInPc1	tým
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
prodloužení	prodloužení	k1gNnSc6	prodloužení
na	na	k7c6	na
hřišti	hřiště	k1gNnSc6	hřiště
3	[number]	k4	3
hráče	hráč	k1gMnPc4	hráč
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vstřelí	vstřelit	k5eAaPmIp3nS	vstřelit
jako	jako	k9	jako
první	první	k4xOgFnSc4	první
branku	branka	k1gFnSc4	branka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
vítězem	vítěz	k1gMnSc7	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
sezony	sezona	k1gFnSc2	sezona
2005-06	[number]	k4	2005-06
platilo	platit	k5eAaImAgNnS	platit
pravidlo	pravidlo	k1gNnSc1	pravidlo
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nerozhodne	rozhodnout	k5eNaPmIp3nS	rozhodnout
ani	ani	k8xC	ani
v	v	k7c6	v
prodloužení	prodloužení	k1gNnSc6	prodloužení
<g/>
,	,	kIx,	,
utkání	utkání	k1gNnSc1	utkání
končí	končit	k5eAaImIp3nS	končit
remízou	remíza	k1gFnSc7	remíza
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
výhru	výhra	k1gFnSc4	výhra
(	(	kIx(	(
<g/>
v	v	k7c6	v
prodloužení	prodloužení	k1gNnSc6	prodloužení
<g/>
,	,	kIx,	,
na	na	k7c4	na
nájezdy	nájezd	k1gInPc4	nájezd
i	i	k9	i
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
hrací	hrací	k2eAgFnSc6d1	hrací
době	doba	k1gFnSc6	doba
<g/>
)	)	kIx)	)
získává	získávat	k5eAaImIp3nS	získávat
tým	tým	k1gInSc4	tým
2	[number]	k4	2
body	bod	k1gInPc7	bod
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
prohru	prohra	k1gFnSc4	prohra
v	v	k7c6	v
prodloužení	prodloužení	k1gNnSc6	prodloužení
a	a	k8xC	a
na	na	k7c4	na
nájezdy	nájezd	k1gInPc4	nájezd
získává	získávat	k5eAaImIp3nS	získávat
tým	tým	k1gInSc4	tým
1	[number]	k4	1
bod	bod	k1gInSc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sezony	sezona	k1gFnSc2	sezona
2005-06	[number]	k4	2005-06
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
pravidlo	pravidlo	k1gNnSc1	pravidlo
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
zápas	zápas	k1gInSc1	zápas
nerozhodný	rozhodný	k2eNgInSc1d1	nerozhodný
i	i	k9	i
po	po	k7c6	po
prodloužení	prodloužení	k1gNnSc6	prodloužení
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
zavedeny	zaveden	k2eAgInPc1d1	zaveden
samostatné	samostatný	k2eAgInPc1d1	samostatný
nájezdy	nájezd	k1gInPc1	nájezd
(	(	kIx(	(
<g/>
série	série	k1gFnPc1	série
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
následuje	následovat	k5eAaImIp3nS	následovat
série	série	k1gFnSc1	série
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
nájezdu	nájezd	k1gInSc6	nájezd
až	až	k9	až
do	do	k7c2	do
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
v	v	k7c4	v
playoff	playoff	k1gInSc4	playoff
jsou	být	k5eAaImIp3nP	být
pravidla	pravidlo	k1gNnPc1	pravidlo
odlišná	odlišný	k2eAgNnPc1d1	odlišné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
nerozhodného	rozhodný	k2eNgInSc2d1	nerozhodný
stavu	stav	k1gInSc2	stav
po	po	k7c6	po
60	[number]	k4	60
minutách	minuta	k1gFnPc6	minuta
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
prodloužení	prodloužení	k1gNnSc1	prodloužení
(	(	kIx(	(
<g/>
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
řádná	řádný	k2eAgFnSc1d1	řádná
přestávka	přestávka	k1gFnSc1	přestávka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nepadne	padnout	k5eNaPmIp3nS	padnout
branka	branka	k1gFnSc1	branka
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
vstřelí	vstřelit	k5eAaPmIp3nS	vstřelit
gól	gól	k1gInSc1	gól
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
vítězem	vítěz	k1gMnSc7	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc1	zápas
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
teoreticky	teoreticky	k6eAd1	teoreticky
pokračovat	pokračovat	k5eAaImF	pokračovat
do	do	k7c2	do
nekonečna	nekonečno	k1gNnSc2	nekonečno
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
zápasy	zápas	k1gInPc4	zápas
v	v	k7c4	v
historii	historie	k1gFnSc4	historie
NHL	NHL	kA	NHL
dokonce	dokonce	k9	dokonce
skončily	skončit	k5eAaPmAgInP	skončit
až	až	k9	až
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
prodloužení	prodloužení	k1gNnSc6	prodloužení
<g/>
.	.	kIx.	.
</s>
<s>
Zakázané	zakázaný	k2eAgNnSc1d1	zakázané
uvolnění	uvolnění	k1gNnSc1	uvolnění
<g/>
:	:	kIx,	:
Hybridní	hybridní	k2eAgInSc1d1	hybridní
icing	icing	k1gInSc1	icing
jak	jak	k8xS	jak
v	v	k7c6	v
IIHF	IIHF	kA	IIHF
tak	tak	k9	tak
v	v	k7c6	v
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
Vyloučení	vyloučení	k1gNnSc1	vyloučení
<g/>
:	:	kIx,	:
V	v	k7c6	v
NHL	NHL	kA	NHL
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
hráči	hráč	k1gMnPc1	hráč
při	při	k7c6	při
nebezpečném	bezpečný	k2eNgInSc6d1	nebezpečný
faulu	faul	k1gInSc6	faul
udělen	udělen	k2eAgInSc4d1	udělen
pětiminutový	pětiminutový	k2eAgInSc4d1	pětiminutový
trest	trest	k1gInSc4	trest
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
vyloučen	vyloučit	k5eAaPmNgInS	vyloučit
současně	současně	k6eAd1	současně
do	do	k7c2	do
konce	konec	k1gInSc2	konec
utkání	utkání	k1gNnSc2	utkání
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
pravidel	pravidlo	k1gNnPc2	pravidlo
IIHF	IIHF	kA	IIHF
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tento	tento	k3xDgInSc1	tento
trest	trest	k1gInSc1	trest
následuje	následovat	k5eAaImIp3nS	následovat
automaticky	automaticky	k6eAd1	automaticky
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
brankáře	brankář	k1gMnSc2	brankář
<g/>
:	:	kIx,	:
V	v	k7c6	v
NHL	NHL	kA	NHL
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
brankářů	brankář	k1gMnPc2	brankář
s	s	k7c7	s
pukem	puk	k1gInSc7	puk
za	za	k7c7	za
vlastní	vlastní	k2eAgFnSc7d1	vlastní
brankou	branka	k1gFnSc7	branka
omezena	omezit	k5eAaPmNgFnS	omezit
dvěma	dva	k4xCgFnPc7	dva
čarami	čára	k1gFnPc7	čára
(	(	kIx(	(
<g/>
nesmí	smět	k5eNaImIp3nS	smět
hrát	hrát	k5eAaImF	hrát
pukem	puk	k1gInSc7	puk
v	v	k7c6	v
rohu	roh	k1gInSc6	roh
<g/>
,	,	kIx,	,
při	při	k7c6	při
porušení	porušení	k1gNnSc6	porušení
následuje	následovat	k5eAaImIp3nS	následovat
trest	trest	k1gInSc1	trest
-	-	kIx~	-
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
zvýhodnění	zvýhodnění	k1gNnSc4	zvýhodnění
útočícího	útočící	k2eAgInSc2d1	útočící
týmu	tým	k1gInSc2	tým
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
jsou	být	k5eAaImIp3nP	být
zmenšena	zmenšen	k2eAgNnPc4d1	zmenšeno
brankoviště	brankoviště	k1gNnPc4	brankoviště
<g/>
.	.	kIx.	.
</s>
<s>
Hřiště	hřiště	k1gNnSc1	hřiště
<g/>
:	:	kIx,	:
V	v	k7c6	v
NHL	NHL	kA	NHL
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
pravidlům	pravidlo	k1gNnPc3	pravidlo
IIHF	IIHF	kA	IIHF
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
metry	metr	k1gInPc4	metr
užší	úzký	k2eAgNnSc4d2	užší
hřiště	hřiště	k1gNnSc4	hřiště
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
/	/	kIx~	/
<g/>
26	[number]	k4	26
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
kluziště	kluziště	k1gNnSc2	kluziště
je	být	k5eAaImIp3nS	být
totožná	totožný	k2eAgFnSc1d1	totožná
(	(	kIx(	(
<g/>
61	[number]	k4	61
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
bez	bez	k7c2	bez
helmy	helma	k1gFnSc2	helma
<g/>
:	:	kIx,	:
V	v	k7c6	v
NHL	NHL	kA	NHL
je	být	k5eAaImIp3nS	být
dovoleno	dovolit	k5eAaPmNgNnS	dovolit
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
po	po	k7c6	po
ztrátě	ztráta	k1gFnSc6	ztráta
helmy	helma	k1gFnSc2	helma
(	(	kIx(	(
<g/>
nenásleduje	následovat	k5eNaImIp3nS	následovat
menší	malý	k2eAgInSc1d2	menší
trest	trest	k1gInSc1	trest
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Soupiska	soupiska	k1gFnSc1	soupiska
<g/>
:	:	kIx,	:
Podle	podle	k7c2	podle
pravidel	pravidlo	k1gNnPc2	pravidlo
IIHF	IIHF	kA	IIHF
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
soupisce	soupiska	k1gFnSc6	soupiska
22	[number]	k4	22
hráčů	hráč	k1gMnPc2	hráč
(	(	kIx(	(
<g/>
4	[number]	k4	4
útoky	útok	k1gInPc7	útok
po	po	k7c4	po
3	[number]	k4	3
<g/>
,	,	kIx,	,
3	[number]	k4	3
obrany	obrana	k1gFnSc2	obrana
po	po	k7c4	po
2	[number]	k4	2
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
.	.	kIx.	.
útočník	útočník	k1gMnSc1	útočník
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
obránce	obránce	k1gMnSc1	obránce
<g/>
,	,	kIx,	,
2	[number]	k4	2
brankáři	brankář	k1gMnSc3	brankář
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
NHL	NHL	kA	NHL
pouze	pouze	k6eAd1	pouze
20	[number]	k4	20
(	(	kIx(	(
<g/>
4	[number]	k4	4
útoky	útok	k1gInPc7	útok
po	po	k7c4	po
3	[number]	k4	3
<g/>
,	,	kIx,	,
3	[number]	k4	3
obrany	obrana	k1gFnSc2	obrana
po	po	k7c4	po
2	[number]	k4	2
<g/>
,	,	kIx,	,
2	[number]	k4	2
brankáři	brankář	k1gMnPc1	brankář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
NHL	NHL	kA	NHL
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
účastnily	účastnit	k5eAaImAgFnP	účastnit
čtyři	čtyři	k4xCgFnPc1	čtyři
týmy	tým	k1gInPc1	tým
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
mnoha	mnoho	k4c3	mnoho
rozšířením	rozšíření	k1gNnSc7	rozšíření
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
počtu	počet	k1gInSc2	počet
týmů	tým	k1gInPc2	tým
na	na	k7c4	na
30	[number]	k4	30
(	(	kIx(	(
<g/>
23	[number]	k4	23
z	z	k7c2	z
USA	USA	kA	USA
a	a	k8xC	a
7	[number]	k4	7
z	z	k7c2	z
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
klubem	klub	k1gInSc7	klub
historie	historie	k1gFnSc2	historie
je	být	k5eAaImIp3nS	být
Montreal	Montreal	k1gInSc1	Montreal
Canadiens	Canadiens	k1gInSc1	Canadiens
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
získal	získat	k5eAaPmAgInS	získat
Stanley	Stanley	k1gInPc4	Stanley
Cup	cup	k1gInSc1	cup
24	[number]	k4	24
<g/>
krát	krát	k6eAd1	krát
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
úspěšným	úspěšný	k2eAgInSc7d1	úspěšný
celkem	celek	k1gInSc7	celek
je	být	k5eAaImIp3nS	být
Toronto	Toronto	k1gNnSc1	Toronto
Maple	Maple	k1gFnSc2	Maple
Leafs	Leafsa	k1gFnPc2	Leafsa
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
ze	z	k7c2	z
zisku	zisk	k1gInSc2	zisk
Stanley	Stanlea	k1gMnSc2	Stanlea
Cupu	cup	k1gInSc2	cup
radoval	radovat	k5eAaImAgInS	radovat
13	[number]	k4	13
<g/>
krát	krát	k6eAd1	krát
(	(	kIx(	(
<g/>
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
třetím	třetí	k4xOgInSc7	třetí
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
celkem	celek	k1gInSc7	celek
je	být	k5eAaImIp3nS	být
Detroit	Detroit	k1gInSc1	Detroit
Red	Red	k1gFnSc2	Red
Wings	Wingsa	k1gFnPc2	Wingsa
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
celek	celek	k1gInSc1	celek
má	mít	k5eAaImIp3nS	mít
svoje	svůj	k3xOyFgFnPc4	svůj
farmy	farma	k1gFnPc4	farma
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
amerických	americký	k2eAgFnPc6d1	americká
ligách	liga	k1gFnPc6	liga
-	-	kIx~	-
American	American	k1gInSc4	American
Hockey	Hockea	k1gFnSc2	Hockea
League	League	k1gFnSc1	League
a	a	k8xC	a
East	East	k2eAgInSc1d1	East
Coast	Coast	k1gInSc1	Coast
Hockey	Hockea	k1gMnSc2	Hockea
League	Leagu	k1gMnSc2	Leagu
<g/>
.	.	kIx.	.
</s>
<s>
NHL	NHL	kA	NHL
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
konference	konference	k1gFnPc4	konference
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
východní	východní	k2eAgNnSc1d1	východní
a	a	k8xC	a
západní	západní	k2eAgNnSc1d1	západní
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
divize	divize	k1gFnPc4	divize
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
divizi	divize	k1gFnSc6	divize
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
šestnáct	šestnáct	k4xCc4	šestnáct
týmů	tým	k1gInPc2	tým
a	a	k8xC	a
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
jich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
čtrnáct	čtrnáct	k4xCc4	čtrnáct
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nového	nový	k2eAgNnSc2d1	nové
rozdělení	rozdělení	k1gNnSc2	rozdělení
se	se	k3xPyFc4	se
Detroit	Detroit	k1gInSc1	Detroit
Red	Red	k1gFnPc2	Red
Wings	Wings	k1gInSc1	Wings
a	a	k8xC	a
Columbus	Columbus	k1gInSc1	Columbus
Blue	Blue	k1gNnSc2	Blue
Jackets	Jackets	k1gInSc1	Jackets
přesunou	přesunout	k5eAaPmIp3nP	přesunout
ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
konference	konference	k1gFnSc2	konference
do	do	k7c2	do
východní	východní	k2eAgFnSc2d1	východní
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
Winnipeg	Winnipeg	k1gInSc1	Winnipeg
Jets	Jetsa	k1gFnPc2	Jetsa
se	se	k3xPyFc4	se
přesune	přesunout	k5eAaPmIp3nS	přesunout
do	do	k7c2	do
západní	západní	k2eAgFnSc2d1	západní
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
herní	herní	k2eAgInSc1d1	herní
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
v	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
část	část	k1gFnSc1	část
NHL	NHL	kA	NHL
začíná	začínat	k5eAaImIp3nS	začínat
první	první	k4xOgFnSc4	první
středu	středa	k1gFnSc4	středa
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
následuje	následovat	k5eAaImIp3nS	následovat
playoff	playoff	k1gMnSc1	playoff
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
nejpozději	pozdě	k6eAd3	pozdě
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Play	play	k0	play
off	off	k?	off
je	být	k5eAaImIp3nS	být
vyřazovací	vyřazovací	k2eAgFnSc1d1	vyřazovací
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
hrají	hrát	k5eAaImIp3nP	hrát
týmy	tým	k1gInPc1	tým
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
vítězné	vítězný	k2eAgInPc4d1	vítězný
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Vítěz	vítěz	k1gMnSc1	vítěz
finále	finále	k1gNnSc2	finále
je	být	k5eAaImIp3nS	být
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
vítězem	vítěz	k1gMnSc7	vítěz
Stanley	Stanlea	k1gMnSc2	Stanlea
Cupu	cup	k1gInSc2	cup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
hraje	hrát	k5eAaImIp3nS	hrát
každý	každý	k3xTgInSc1	každý
tým	tým	k1gInSc1	tým
82	[number]	k4	82
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
41	[number]	k4	41
zápasů	zápas	k1gInPc2	zápas
doma	doma	k6eAd1	doma
a	a	k8xC	a
41	[number]	k4	41
venku	venku	k6eAd1	venku
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
hraje	hrát	k5eAaImIp3nS	hrát
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
konferenci	konference	k1gFnSc6	konference
každý	každý	k3xTgInSc1	každý
tým	tým	k1gInSc1	tým
30	[number]	k4	30
zápasů	zápas	k1gInPc2	zápas
proti	proti	k7c3	proti
soupeřům	soupeř	k1gMnPc3	soupeř
z	z	k7c2	z
jejich	jejich	k3xOp3gFnSc2	jejich
divize	divize	k1gFnSc2	divize
(	(	kIx(	(
<g/>
4	[number]	k4	4
zápasy	zápas	k1gInPc4	zápas
proti	proti	k7c3	proti
pěti	pět	k4xCc3	pět
týmům	tým	k1gInPc3	tým
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
divize	divize	k1gFnSc2	divize
a	a	k8xC	a
5	[number]	k4	5
zápasů	zápas	k1gInPc2	zápas
proti	proti	k7c3	proti
dvěma	dva	k4xCgInPc3	dva
týmům	tým	k1gInPc3	tým
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
divizi	divize	k1gFnSc6	divize
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
24	[number]	k4	24
zápasů	zápas	k1gInPc2	zápas
proti	proti	k7c3	proti
týmům	tým	k1gInPc3	tým
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
konference	konference	k1gFnSc2	konference
(	(	kIx(	(
<g/>
3	[number]	k4	3
zápasy	zápas	k1gInPc4	zápas
proti	proti	k7c3	proti
každému	každý	k3xTgInSc3	každý
týmu	tým	k1gInSc3	tým
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
divize	divize	k1gFnSc2	divize
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
konferenci	konference	k1gFnSc6	konference
<g/>
)	)	kIx)	)
a	a	k8xC	a
28	[number]	k4	28
utkání	utkání	k1gNnSc6	utkání
se	s	k7c7	s
čtrnácti	čtrnáct	k4xCc7	čtrnáct
zbývajícími	zbývající	k2eAgInPc7d1	zbývající
celky	celek	k1gInPc7	celek
(	(	kIx(	(
<g/>
2	[number]	k4	2
zápasy	zápas	k1gInPc4	zápas
proti	proti	k7c3	proti
každému	každý	k3xTgInSc3	každý
týmu	tým	k1gInSc3	tým
ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
konference	konference	k1gFnSc2	konference
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
konferenci	konference	k1gFnSc6	konference
si	se	k3xPyFc3	se
každé	každý	k3xTgNnSc4	každý
mužstvo	mužstvo	k1gNnSc4	mužstvo
zahraje	zahrát	k5eAaPmIp3nS	zahrát
celkem	celkem	k6eAd1	celkem
29	[number]	k4	29
utkání	utkání	k1gNnPc1	utkání
proti	proti	k7c3	proti
soupeřům	soupeř	k1gMnPc3	soupeř
z	z	k7c2	z
jejich	jejich	k3xOp3gFnSc2	jejich
divize	divize	k1gFnSc2	divize
(	(	kIx(	(
<g/>
5	[number]	k4	5
zápasů	zápas	k1gInPc2	zápas
proti	proti	k7c3	proti
pěti	pět	k4xCc3	pět
soupeřům	soupeř	k1gMnPc3	soupeř
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
divizi	divize	k1gFnSc6	divize
a	a	k8xC	a
4	[number]	k4	4
zápasy	zápas	k1gInPc7	zápas
proti	proti	k7c3	proti
jednomu	jeden	k4xCgInSc3	jeden
týmu	tým	k1gInSc3	tým
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
divizi	divize	k1gFnSc6	divize
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
21	[number]	k4	21
utkání	utkání	k1gNnPc2	utkání
proti	proti	k7c3	proti
celkům	celek	k1gInPc3	celek
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
konference	konference	k1gFnSc2	konference
(	(	kIx(	(
<g/>
3	[number]	k4	3
zápasy	zápas	k1gInPc4	zápas
proti	proti	k7c3	proti
každému	každý	k3xTgInSc3	každý
týmu	tým	k1gInSc3	tým
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
divize	divize	k1gFnSc2	divize
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
konferenci	konference	k1gFnSc6	konference
<g/>
)	)	kIx)	)
a	a	k8xC	a
32	[number]	k4	32
zápasů	zápas	k1gInPc2	zápas
s	s	k7c7	s
šestnácti	šestnáct	k4xCc7	šestnáct
zbývajícími	zbývající	k2eAgMnPc7d1	zbývající
soky	sok	k1gMnPc7	sok
(	(	kIx(	(
<g/>
2	[number]	k4	2
zápasy	zápas	k1gInPc4	zápas
proti	proti	k7c3	proti
každému	každý	k3xTgInSc3	každý
týmu	tým	k1gInSc3	tým
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
konference	konference	k1gFnSc2	konference
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takovýto	takovýto	k3xDgInSc1	takovýto
rozpis	rozpis	k1gInSc1	rozpis
sezony	sezona	k1gFnSc2	sezona
je	být	k5eAaImIp3nS	být
platný	platný	k2eAgMnSc1d1	platný
od	od	k7c2	od
sezony	sezona	k1gFnSc2	sezona
2013-14	[number]	k4	2013-14
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vytvořený	vytvořený	k2eAgMnSc1d1	vytvořený
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
týmy	tým	k1gInPc1	tým
ušetřily	ušetřit	k5eAaPmAgInP	ušetřit
na	na	k7c4	na
cestovaní	cestovaný	k2eAgMnPc1d1	cestovaný
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
fyzicky	fyzicky	k6eAd1	fyzicky
zatěžovalo	zatěžovat	k5eAaImAgNnS	zatěžovat
hráče	hráč	k1gMnSc4	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
play	play	k0	play
off	off	k?	off
postoupí	postoupit	k5eAaPmIp3nP	postoupit
nejlepší	dobrý	k2eAgInPc1d3	nejlepší
tři	tři	k4xCgInPc1	tři
týmy	tým	k1gInPc1	tým
z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
divize	divize	k1gFnSc2	divize
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
tedy	tedy	k9	tedy
12	[number]	k4	12
celků	celek	k1gInPc2	celek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
dvě	dva	k4xCgNnPc1	dva
mužstva	mužstvo	k1gNnPc1	mužstvo
z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
konference	konference	k1gFnSc2	konference
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
počet	počet	k1gInSc4	počet
bodů	bod	k1gInPc2	bod
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
divize	divize	k1gFnPc4	divize
(	(	kIx(	(
<g/>
4	[number]	k4	4
týmy	tým	k1gInPc7	tým
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
počtem	počet	k1gInSc7	počet
bodů	bod	k1gInPc2	bod
po	po	k7c6	po
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
konferencí	konference	k1gFnPc2	konference
obdrží	obdržet	k5eAaPmIp3nS	obdržet
Presidents	Presidents	k1gInSc1	Presidents
<g/>
'	'	kIx"	'
Trophy	Tropha	k1gFnPc1	Tropha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
konferenci	konference	k1gFnSc6	konference
hraje	hrát	k5eAaImIp3nS	hrát
play	play	k0	play
off	off	k?	off
8	[number]	k4	8
týmů	tým	k1gInPc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Vítězové	vítěz	k1gMnPc1	vítěz
divizí	divize	k1gFnPc2	divize
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
play	play	k0	play
off	off	k?	off
nasazeni	nasadit	k5eAaPmNgMnP	nasadit
na	na	k7c6	na
prvních	první	k4xOgNnPc6	první
dvou	dva	k4xCgNnPc6	dva
místech	místo	k1gNnPc6	místo
(	(	kIx(	(
<g/>
i	i	k8xC	i
když	když	k8xS	když
nějaký	nějaký	k3yIgInSc4	nějaký
nevítěz	vítězit	k5eNaImRp2nS	vítězit
divize	divize	k1gFnSc1	divize
měl	mít	k5eAaImAgInS	mít
více	hodně	k6eAd2	hodně
bodů	bod	k1gInPc2	bod
než	než	k8xS	než
vítěz	vítěz	k1gMnSc1	vítěz
divize	divize	k1gFnSc2	divize
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
výše	vysoce	k6eAd2	vysoce
nasazen	nasazen	k2eAgMnSc1d1	nasazen
vítěz	vítěz	k1gMnSc1	vítěz
divize	divize	k1gFnSc2	divize
<g/>
)	)	kIx)	)
a	a	k8xC	a
zbylé	zbylý	k2eAgInPc1d1	zbylý
týmy	tým	k1gInPc1	tým
jsou	být	k5eAaImIp3nP	být
nasazeny	nasadit	k5eAaPmNgInP	nasadit
jako	jako	k9	jako
týmy	tým	k1gInPc4	tým
na	na	k7c6	na
pátém	pátý	k4xOgInSc6	pátý
až	až	k8xS	až
osmém	osmý	k4xOgInSc6	osmý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Play	play	k0	play
off	off	k?	off
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
jako	jako	k9	jako
vyřazovací	vyřazovací	k2eAgInSc1d1	vyřazovací
turnaj	turnaj	k1gInSc1	turnaj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
čtyři	čtyři	k4xCgNnPc4	čtyři
vítězná	vítězný	k2eAgNnPc4d1	vítězné
utkání	utkání	k1gNnPc4	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
konference	konference	k1gFnPc1	konference
mají	mít	k5eAaImIp3nP	mít
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
play	play	k0	play
off	off	k?	off
<g/>
,	,	kIx,	,
týmy	tým	k1gInPc1	tým
z	z	k7c2	z
opačných	opačný	k2eAgFnPc2d1	opačná
konferencí	konference	k1gFnPc2	konference
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
mohou	moct	k5eAaImIp3nP	moct
potkat	potkat	k5eAaPmF	potkat
až	až	k9	až
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Stanley	Stanlea	k1gFnSc2	Stanlea
Cupu	cup	k1gInSc2	cup
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
také	také	k9	také
bude	být	k5eAaImBp3nS	být
zaveden	zaveden	k2eAgInSc1d1	zaveden
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
"	"	kIx"	"
<g/>
systém	systém	k1gInSc1	systém
divokých	divoký	k2eAgFnPc2d1	divoká
karet	kareta	k1gFnPc2	kareta
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
dá	dát	k5eAaPmIp3nS	dát
dohromady	dohromady	k6eAd1	dohromady
dva	dva	k4xCgMnPc4	dva
vítěze	vítěz	k1gMnPc4	vítěz
divizí	divize	k1gFnPc2	divize
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
počtem	počet	k1gInSc7	počet
bodů	bod	k1gInPc2	bod
s	s	k7c7	s
držiteli	držitel	k1gMnPc7	držitel
divoké	divoký	k2eAgFnSc2d1	divoká
karty	karta	k1gFnSc2	karta
s	s	k7c7	s
nejnižším	nízký	k2eAgNnSc7d3	nejnižší
a	a	k8xC	a
druhým	druhý	k4xOgInSc7	druhý
nejnižším	nízký	k2eAgInSc7d3	nejnižší
počtem	počet	k1gInSc7	počet
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
kolo	kolo	k1gNnSc1	kolo
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
divizí	divize	k1gFnPc2	divize
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
tým	tým	k1gInSc1	tým
nastoupí	nastoupit	k5eAaPmIp3nS	nastoupit
proti	proti	k7c3	proti
čtvrtému	čtvrtý	k4xOgInSc3	čtvrtý
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
proti	proti	k7c3	proti
třetímu	třetí	k4xOgMnSc3	třetí
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	k9	kdyby
z	z	k7c2	z
divize	divize	k1gFnSc2	divize
postoupili	postoupit	k5eAaPmAgMnP	postoupit
pouze	pouze	k6eAd1	pouze
tři	tři	k4xCgInPc4	tři
týmy	tým	k1gInPc1	tým
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
nejlepšímu	dobrý	k2eAgInSc3d3	nejlepší
týmu	tým	k1gInSc3	tým
z	z	k7c2	z
konference	konference	k1gFnSc2	konference
postavil	postavit	k5eAaPmAgMnS	postavit
slabší	slabý	k2eAgMnSc1d2	slabší
z	z	k7c2	z
týmů	tým	k1gInPc2	tým
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
postoupili	postoupit	k5eAaPmAgMnP	postoupit
díky	díky	k7c3	díky
divokým	divoký	k2eAgFnPc3d1	divoká
kartám	karta	k1gFnPc3	karta
<g/>
.	.	kIx.	.
</s>
<s>
Vítězové	vítěz	k1gMnPc1	vítěz
1	[number]	k4	1
<g/>
.	.	kIx.	.
kola	kolo	k1gNnSc2	kolo
se	se	k3xPyFc4	se
střetnou	střetnout	k5eAaPmIp3nP	střetnout
v	v	k7c6	v
divizním	divizní	k2eAgNnSc6d1	divizní
finále	finále	k1gNnSc6	finále
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yQgInSc6	který
bude	být	k5eAaImBp3nS	být
následovat	následovat	k5eAaImF	následovat
finále	finále	k1gNnSc4	finále
konferencí	konference	k1gFnPc2	konference
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
vítězové	vítěz	k1gMnPc1	vítěz
play	play	k0	play
off	off	k?	off
konferencí	konference	k1gFnPc2	konference
se	se	k3xPyFc4	se
utkají	utkat	k5eAaPmIp3nP	utkat
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
vítěz	vítěz	k1gMnSc1	vítěz
obdrží	obdržet	k5eAaPmIp3nS	obdržet
Stanley	Stanlea	k1gFnPc4	Stanlea
Cup	cup	k1gInSc1	cup
<g/>
.	.	kIx.	.
</s>
<s>
Výše	vysoce	k6eAd2	vysoce
nasazený	nasazený	k2eAgInSc1d1	nasazený
tým	tým	k1gInSc1	tým
má	mít	k5eAaImIp3nS	mít
výhodu	výhoda	k1gFnSc4	výhoda
domácího	domácí	k2eAgNnSc2d1	domácí
prostředí	prostředí	k1gNnSc2	prostředí
-	-	kIx~	-
čtyři	čtyři	k4xCgInPc1	čtyři
z	z	k7c2	z
případných	případný	k2eAgInPc2d1	případný
sedmi	sedm	k4xCc2	sedm
utkání	utkání	k1gNnPc2	utkání
série	série	k1gFnSc2	série
jsou	být	k5eAaImIp3nP	být
hrány	hrát	k5eAaImNgFnP	hrát
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
hřišti	hřiště	k1gNnSc6	hřiště
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgFnPc1	druhý
<g/>
,	,	kIx,	,
případné	případný	k2eAgFnPc1d1	případná
páté	pátá	k1gFnPc1	pátá
a	a	k8xC	a
sedmé	sedmý	k4xOgFnSc2	sedmý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stanley	Stanle	k2eAgInPc1d1	Stanle
Cup	cup	k1gInSc1	cup
Clarence	Clarence	k1gFnSc2	Clarence
S.	S.	kA	S.
Campbell	Campbell	k1gMnSc1	Campbell
Bowl	Bowl	k1gMnSc1	Bowl
-	-	kIx~	-
Nejlepší	dobrý	k2eAgInSc1d3	nejlepší
tým	tým	k1gInSc1	tým
Západní	západní	k2eAgFnSc2d1	západní
konference	konference	k1gFnSc2	konference
v	v	k7c4	v
Playoff	Playoff	k1gInSc4	Playoff
NHL	NHL	kA	NHL
Prince	princ	k1gMnSc2	princ
of	of	k?	of
Wales	Wales	k1gInSc1	Wales
Trophy	Tropha	k1gFnSc2	Tropha
-	-	kIx~	-
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
tým	tým	k1gInSc4	tým
Východní	východní	k2eAgFnSc2d1	východní
konference	konference	k1gFnSc2	konference
v	v	k7c4	v
Playoff	Playoff	k1gInSc4	Playoff
NHL	NHL	kA	NHL
Presidents	Presidents	k1gInSc1	Presidents
<g/>
'	'	kIx"	'
Trophy	Tropha	k1gFnPc1	Tropha
-	-	kIx~	-
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
klub	klub	k1gInSc4	klub
základní	základní	k2eAgFnSc2d1	základní
soutěže	soutěž	k1gFnSc2	soutěž
NHL	NHL	kA	NHL
Seznam	seznam	k1gInSc1	seznam
hráčů	hráč	k1gMnPc2	hráč
NHL	NHL	kA	NHL
s	s	k7c7	s
1000	[number]	k4	1000
a	a	k8xC	a
více	hodně	k6eAd2	hodně
body	bod	k1gInPc4	bod
Seznam	seznam	k1gInSc4	seznam
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
střelců	střelec	k1gMnPc2	střelec
NHL	NHL	kA	NHL
Seznam	seznam	k1gInSc1	seznam
obránců	obránce	k1gMnPc2	obránce
<g />
.	.	kIx.	.
</s>
<s>
NHL	NHL	kA	NHL
s	s	k7c7	s
nejvíce	hodně	k6eAd3	hodně
body	bod	k1gInPc7	bod
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgMnPc2d1	český
hokejových	hokejový	k2eAgMnPc2d1	hokejový
brankářů	brankář	k1gMnPc2	brankář
v	v	k7c6	v
NHL	NHL	kA	NHL
Hokejisté	hokejista	k1gMnPc1	hokejista
narození	narozený	k2eAgMnPc1d1	narozený
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
-	-	kIx~	-
vítězové	vítěz	k1gMnPc1	vítěz
Stanley	Stanlea	k1gFnSc2	Stanlea
Cupu	cupat	k5eAaImIp1nS	cupat
NHL	NHL	kA	NHL
All-Star	All-Star	k1gInSc1	All-Star
Team	team	k1gInSc1	team
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
National	National	k1gFnSc2	National
Hockey	Hockea	k1gMnSc2	Hockea
League	Leagu	k1gMnSc2	Leagu
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
www.nhlportal.cz/	www.nhlportal.cz/	k?	www.nhlportal.cz/
-	-	kIx~	-
český	český	k2eAgInSc1d1	český
a	a	k8xC	a
slovenský	slovenský	k2eAgInSc1d1	slovenský
portál	portál	k1gInSc1	portál
o	o	k7c6	o
NHL	NHL	kA	NHL
<g/>
,	,	kIx,	,
online	onlinout	k5eAaPmIp3nS	onlinout
statistiky	statistika	k1gFnPc4	statistika
<g/>
,	,	kIx,	,
tipovací	tipovací	k2eAgFnSc4d1	tipovací
soutěž	soutěž	k1gFnSc4	soutěž
a	a	k8xC	a
mnoho	mnoho	k6eAd1	mnoho
dalšího	další	k2eAgMnSc2d1	další
www.hokejportal.cz/severni-amerika/nhl/	www.hokejportal.cz/severnimerika/nhl/	k?	www.hokejportal.cz/severni-amerika/nhl/
-	-	kIx~	-
české	český	k2eAgNnSc4d1	české
zpravodajství	zpravodajství	k1gNnSc4	zpravodajství
z	z	k7c2	z
NHL	NHL	kA	NHL
www.nhl.cz	www.nhl.cza	k1gFnPc2	www.nhl.cza
-	-	kIx~	-
české	český	k2eAgFnSc2d1	Česká
stránky	stránka	k1gFnSc2	stránka
o	o	k7c4	o
NHL	NHL	kA	NHL
Draft	draft	k1gInSc4	draft
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
ročnících	ročník	k1gInPc6	ročník
NHL	NHL	kA	NHL
na	na	k7c4	na
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Arény	aréna	k1gFnSc2	aréna
na	na	k7c6	na
mapách	mapa	k1gFnPc6	mapa
NHL	NHL	kA	NHL
</s>
