<s>
Běžník	běžník	k1gMnSc1
květomilný	květomilný	k2eAgMnSc1d1
</s>
<s>
Běžník	běžník	k1gMnSc1
květomilný	květomilný	k2eAgMnSc1d1
běžník	běžník	k1gMnSc1
květomilný	květomilný	k2eAgMnSc1d1
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc2
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
členovci	členovec	k1gMnPc1
(	(	kIx(
<g/>
Arthropoda	Arthropoda	k1gMnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
klepítkatci	klepítkatec	k1gMnPc1
(	(	kIx(
<g/>
Chelicerata	Chelicerat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
pavoukovci	pavoukovec	k1gMnPc1
(	(	kIx(
<g/>
Arachnida	Arachnida	k1gFnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
pavouci	pavouk	k1gMnPc1
(	(	kIx(
<g/>
Araneae	Araneae	k1gInSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
běžníkovití	běžníkovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Thomisidae	Thomisidae	k1gNnSc7
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
běžník	běžník	k1gMnSc1
(	(	kIx(
<g/>
Synaema	Synaema	k1gFnSc1
<g/>
''	''	k?
<g/>
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
Thomisus	Thomisus	k1gMnSc1
onustusWalckenaer	onustusWalckenaer	k1gMnSc1
<g/>
,	,	kIx,
1805	#num#	k4
Poddruhy	poddruh	k1gInPc1
</s>
<s>
T.	T.	kA
o.	o.	k?
meridionalis	meridionalis	k1gInSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Běžník	běžník	k1gMnSc1
květinový	květinový	k2eAgMnSc1d1
(	(	kIx(
<g/>
Thomisus	Thomisus	k1gMnSc1
onustus	onustus	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
druh	druh	k1gInSc1
pavouka	pavouk	k1gMnSc2
z	z	k7c2
čeledi	čeleď	k1gFnSc2
běžníkovití	běžníkovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Thomisidae	Thomisidae	k1gNnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Samci	Samek	k1gMnPc1
dorůstají	dorůstat	k5eAaImIp3nP
velikosti	velikost	k1gFnSc3
asi	asi	k9
5	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
mm	mm	kA
<g/>
,	,	kIx,
samice	samice	k1gFnSc1
až	až	k9
10	#num#	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mají	mít	k5eAaImIp3nP
vpředu	vpředu	k6eAd1
vystouplou	vystouplý	k2eAgFnSc4d1
hlavohruď	hlavohruď	k1gFnSc4
se	s	k7c7
dvěma	dva	k4xCgInPc7
růžky	růžek	k1gInPc7
<g/>
,	,	kIx,
ve	v	k7c6
středu	střed	k1gInSc6
se	s	k7c7
světlým	světlý	k2eAgInSc7d1
širokým	široký	k2eAgInSc7d1
pruhem	pruh	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zadeček	zadeček	k1gInSc1
má	mít	k5eAaImIp3nS
tvar	tvar	k1gInSc4
lichoběžníku	lichoběžník	k1gInSc2
<g/>
,	,	kIx,
rozšiřuje	rozšiřovat	k5eAaImIp3nS
se	se	k3xPyFc4
vzadu	vzadu	k6eAd1
ve	v	k7c4
dva	dva	k4xCgInPc4
rohy	roh	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nohy	noha	k1gFnPc1
jsou	být	k5eAaImIp3nP
většinou	většinou	k6eAd1
tmavě	tmavě	k6eAd1
pruhované	pruhovaný	k2eAgNnSc1d1
<g/>
,	,	kIx,
u	u	k7c2
samců	samec	k1gMnPc2
celé	celý	k2eAgFnSc2d1
tmavé	tmavý	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnSc1
zbarvení	zbarvení	k1gNnSc1
je	být	k5eAaImIp3nS
variabilní	variabilní	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
u	u	k7c2
běžníka	běžník	k1gMnSc2
kopretinového	kopretinový	k2eAgInSc2d1
mají	mít	k5eAaImIp3nP
zbarvení	zbarvení	k1gNnSc2
přizpůsobené	přizpůsobený	k2eAgFnSc6d1
barvě	barva	k1gFnSc6
květu	květ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mívají	mívat	k5eAaImIp3nP
bílou	bílý	k2eAgFnSc4d1
<g/>
,	,	kIx,
žlutou	žlutý	k2eAgFnSc4d1
nebo	nebo	k8xC
fialovo	fialovo	k1gNnSc1
bílou	bílý	k2eAgFnSc4d1
až	až	k9
růžovou	růžový	k2eAgFnSc4d1
barvu	barva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samci	Samek	k1gMnPc1
bývají	bývat	k5eAaImIp3nP
tmavě	tmavě	k6eAd1
hnědí	hnědý	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Potrava	potrava	k1gFnSc1
</s>
<s>
Loví	lovit	k5eAaImIp3nP
většinou	většinou	k6eAd1
létající	létající	k2eAgInSc4d1
hmyz	hmyz	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nestaví	stavit	k5eNaBmIp3nP,k5eNaImIp3nP
si	se	k3xPyFc3
sítě	síť	k1gFnPc4
a	a	k8xC
kořist	kořist	k1gFnSc4
loví	lovit	k5eAaImIp3nP
předníma	přední	k2eAgFnPc7d1
nohama	noha	k1gFnPc7
s	s	k7c7
jedovými	jedový	k2eAgFnPc7d1
žlázami	žláza	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zakusují	zakusovat	k5eAaImIp3nP
se	se	k3xPyFc4
především	především	k9
do	do	k7c2
míst	místo	k1gNnPc2
za	za	k7c7
hlavou	hlava	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Kvůli	kvůli	k7c3
dobrému	dobré	k1gNnSc3
lovu	lov	k1gInSc2
jsou	být	k5eAaImIp3nP
schopni	schopen	k2eAgMnPc1d1
měnit	měnit	k5eAaImF
barvu	barva	k1gFnSc4
svého	svůj	k3xOyFgNnSc2
těla	tělo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Barvy	barva	k1gFnSc2
můžou	můžou	k?
měnit	měnit	k5eAaImF
od	od	k7c2
bílé	bílý	k2eAgFnSc2d1
po	po	k7c4
fialovou	fialový	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přebarvování	přebarvování	k1gNnPc4
je	být	k5eAaImIp3nS
složitý	složitý	k2eAgInSc1d1
proces	proces	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
trvá	trvat	k5eAaImIp3nS
několik	několik	k4yIc4
dnů	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dochází	docházet	k5eAaImIp3nS
k	k	k7c3
přesouvání	přesouvání	k1gNnSc3
podkožního	podkožní	k2eAgNnSc2d1
barviva	barvivo	k1gNnSc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
dosáhne	dosáhnout	k5eAaPmIp3nS
barvy	barva	k1gFnSc2
podkladu	podklad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potom	potom	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
rostlině	rostlina	k1gFnSc6
určité	určitý	k2eAgFnSc2d1
barvy	barva	k1gFnSc2
drží	držet	k5eAaImIp3nS
i	i	k9
několik	několik	k4yIc4
týdnů	týden	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP
se	se	k3xPyFc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
Asii	Asie	k1gFnSc6
<g/>
,	,	kIx,
severní	severní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
a	a	k8xC
Japonsku	Japonsko	k1gNnSc6
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
ve	v	k7c6
stepích	step	k1gFnPc6
<g/>
,	,	kIx,
lesostepích	lesostep	k1gFnPc6
<g/>
,	,	kIx,
písčitých	písčitý	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
suchých	suchý	k2eAgFnPc6d1
stráních	stráň	k1gFnPc6
<g/>
,	,	kIx,
ve	v	k7c6
starých	starý	k2eAgInPc6d1
lomech	lom	k1gInPc6
atd.	atd.	kA
Nejčastěji	často	k6eAd3
pobývají	pobývat	k5eAaImIp3nP
na	na	k7c6
květech	květ	k1gInPc6
<g/>
,	,	kIx,
na	na	k7c6
kterých	který	k3yQgFnPc6,k3yRgFnPc6,k3yIgFnPc6
jsou	být	k5eAaImIp3nP
svým	svůj	k3xOyFgNnSc7
zbarvením	zbarvení	k1gNnSc7
velmi	velmi	k6eAd1
dobře	dobře	k6eAd1
maskováni	maskovat	k5eAaBmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
není	být	k5eNaImIp3nS
hojný	hojný	k2eAgMnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
lze	lze	k6eAd1
ho	on	k3xPp3gMnSc4
nalézt	nalézt	k5eAaPmF,k5eAaBmF
v	v	k7c6
teplých	teplý	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jsou	být	k5eAaImIp3nP
střední	střední	k2eAgFnPc4d1
Čechy	Čechy	k1gFnPc4
a	a	k8xC
také	také	k9
na	na	k7c6
jihu	jih	k1gInSc6
Moravy	Morava	k1gFnSc2
<g/>
,	,	kIx,
jinde	jinde	k6eAd1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
vzácný	vzácný	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reprodukce	reprodukce	k1gFnSc1
</s>
<s>
K	k	k7c3
páření	páření	k1gNnSc3
dochází	docházet	k5eAaImIp3nS
koncem	koncem	k7c2
května	květen	k1gInSc2
a	a	k8xC
v	v	k7c6
průběhu	průběh	k1gInSc6
června	červen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samice	samice	k1gFnSc1
zapřádá	zapřádat	k5eAaImIp3nS
do	do	k7c2
listů	list	k1gInPc2
nebo	nebo	k8xC
na	na	k7c4
spodní	spodní	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
květu	květ	k1gInSc2
plochý	plochý	k2eAgInSc1d1
kokon	kokon	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
následně	následně	k6eAd1
střeží	střežit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
podzim	podzim	k1gInSc4
vytváří	vytvářit	k5eAaPmIp3nP,k5eAaImIp3nP
padákovitá	padákovitý	k2eAgNnPc1d1
vlákna	vlákno	k1gNnPc1
<g/>
,	,	kIx,
na	na	k7c6
kterých	který	k3yRgInPc6,k3yIgInPc6,k3yQgInPc6
se	se	k3xPyFc4
rozšiřují	rozšiřovat	k5eAaImIp3nP
po	po	k7c6
krajině	krajina	k1gFnSc6
vzduchem	vzduch	k1gInSc7
jejich	jejich	k3xOp3gNnPc4
mláďata	mládě	k1gNnPc4
(	(	kIx(
<g/>
babí	babí	k2eAgNnSc4d1
léto	léto	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
naturabohemica	naturabohemica	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
thomisus	thomisus	k1gMnSc1
onuntus	onuntus	k1gMnSc1
<g/>
↑	↑	k?
fotoblog	fotoblog	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Běžník	běžník	k1gMnSc1
květomilný	květomilný	k2eAgMnSc1d1
(	(	kIx(
<g/>
Thomisus	Thomisus	k1gMnSc1
onustus	onustus	k1gMnSc1
<g/>
,	,	kIx,
Walckenauer	Walckenauer	k1gMnSc1
1806	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
hmyzisvet	hmyzisvet	k1gInSc1
<g/>
.	.	kIx.
<g/>
blog	blog	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
zbarvení	zbarvení	k1gNnSc1
běžníků	běžník	k1gMnPc2
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
aranearium	aranearium	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Thomisus	Thomisus	k1gMnSc1
onustus	onustus	k1gMnSc1
—	—	k?
běžník	běžník	k1gMnSc1
květomilný	květomilný	k2eAgMnSc1d1
<g/>
.	.	kIx.
aranearium	aranearium	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
vysokeskoly	vysokeskola	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
clenovci-arthropoda	clenovci-arthropoda	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
běžník	běžník	k1gMnSc1
květomilný	květomilný	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Thomisus	Thomisus	k1gInSc1
onustus	onustus	k1gInSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
