<p>
<s>
Kenneth	Kenneth	k1gMnSc1	Kenneth
Campbell	Campbell	k1gMnSc1	Campbell
"	"	kIx"	"
<g/>
Ken	Ken	k1gMnSc1	Ken
<g/>
"	"	kIx"	"
Stott	Stott	k1gMnSc1	Stott
(	(	kIx(	(
<g/>
*	*	kIx~	*
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
skotský	skotský	k2eAgMnSc1d1	skotský
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
především	především	k9	především
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gMnPc4	jeho
mnoho	mnoho	k6eAd1	mnoho
rolí	role	k1gFnSc7	role
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
2012	[number]	k4	2012
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
v	v	k7c6	v
trilogii	trilogie	k1gFnSc6	trilogie
Hobit	hobit	k1gMnSc1	hobit
jako	jako	k8xC	jako
trpaslík	trpaslík	k1gMnSc1	trpaslík
Balin	Balin	k1gMnSc1	Balin
<g/>
.	.	kIx.	.
</s>
<s>
Stott	Stott	k1gMnSc1	Stott
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Edinburghu	Edinburgh	k1gInSc6	Edinburgh
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
Antonia	Antonio	k1gMnSc4	Antonio
byla	být	k5eAaImAgFnS	být
sicilská	sicilský	k2eAgFnSc1d1	sicilská
učitelka	učitelka	k1gFnSc1	učitelka
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
škole	škola	k1gFnSc6	škola
George	Georg	k1gInSc2	Georg
Heriot	Heriota	k1gFnPc2	Heriota
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
School	Schoola	k1gFnPc2	Schoola
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
kapely	kapela	k1gFnSc2	kapela
Keyhole	Keyhole	k1gFnSc2	Keyhole
<g/>
.	.	kIx.	.
<g/>
Dokonce	dokonce	k9	dokonce
byl	být	k5eAaImAgInS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
2	[number]	k4	2
Oscary	Oscara	k1gFnSc2	Oscara
</s>
</p>
