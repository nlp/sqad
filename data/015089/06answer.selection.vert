<s>
Integrální	integrální	k2eAgInSc1d1
počet	počet	k1gInSc1
je	být	k5eAaImIp3nS
část	část	k1gFnSc4
matematiky	matematika	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
především	především	k9
integrací	integrace	k1gFnSc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
inverzní	inverzní	k2eAgInSc1d1
proces	proces	k1gInSc1
k	k	k7c3
derivaci	derivace	k1gFnSc3
<g/>
,	,	kIx,
a	a	k8xC
integrály	integrál	k1gInPc1
<g/>
.	.	kIx.
</s>