<s>
Integrální	integrální	k2eAgInSc1d1
počet	počet	k1gInSc1
</s>
<s>
Integrální	integrální	k2eAgInSc1d1
počet	počet	k1gInSc1
je	být	k5eAaImIp3nS
část	část	k1gFnSc4
matematiky	matematika	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
především	především	k9
integrací	integrace	k1gFnSc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
inverzní	inverzní	k2eAgInSc1d1
proces	proces	k1gInSc1
k	k	k7c3
derivaci	derivace	k1gFnSc3
<g/>
,	,	kIx,
a	a	k8xC
integrály	integrál	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Základním	základní	k2eAgInSc7d1
pojmem	pojem	k1gInSc7
integrálního	integrální	k2eAgInSc2d1
počtu	počet	k1gInSc2
je	být	k5eAaImIp3nS
integrál	integrál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Integrály	integrál	k1gInPc7
se	se	k3xPyFc4
využívají	využívat	k5eAaPmIp3nP,k5eAaImIp3nP
pro	pro	k7c4
hledání	hledání	k1gNnSc4
ploch	plocha	k1gFnPc2
<g/>
,	,	kIx,
objemů	objem	k1gInPc2
a	a	k8xC
délek	délka	k1gFnPc2
křivek	křivka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
další	další	k2eAgInPc4d1
důležité	důležitý	k2eAgInPc4d1
pojmy	pojem	k1gInPc4
integrálního	integrální	k2eAgInSc2d1
počtu	počet	k1gInSc2
patří	patřit	k5eAaImIp3nS
např.	např.	kA
limita	limita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Již	již	k6eAd1
Archimédés	Archimédés	k1gInSc1
vytvořil	vytvořit	k5eAaPmAgInS
postup	postup	k1gInSc4
nalezení	nalezení	k1gNnSc4
plochy	plocha	k1gFnSc2
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
rozdělil	rozdělit	k5eAaPmAgInS
na	na	k7c4
mnoho	mnoho	k4c4
segmentů	segment	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
postup	postup	k1gInSc1
pak	pak	k6eAd1
rozšířil	rozšířit	k5eAaPmAgInS
také	také	k9
na	na	k7c4
hledání	hledání	k1gNnSc4
objemů	objem	k1gInPc2
některých	některý	k3yIgNnPc2
těles	těleso	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
také	také	k9
bývá	bývat	k5eAaImIp3nS
označován	označován	k2eAgMnSc1d1
jako	jako	k8xC,k8xS
otec	otec	k1gMnSc1
integrálního	integrální	k2eAgInSc2d1
počtu	počet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Kepler	Kepler	k1gMnSc1
použil	použít	k5eAaPmAgMnS
k	k	k7c3
nalezení	nalezení	k1gNnSc3
objemu	objem	k1gInSc2
těles	těleso	k1gNnPc2
postup	postup	k1gInSc4
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yIgNnSc6,k3yRgNnSc6,k3yQgNnSc6
je	být	k5eAaImIp3nS
dělil	dělit	k5eAaImAgMnS
na	na	k7c4
nekonečný	konečný	k2eNgInSc4d1
počet	počet	k1gInSc4
infinitezimálně	infinitezimálně	k6eAd1
malých	malý	k2eAgInPc2d1
prvků	prvek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
metodu	metoda	k1gFnSc4
zobecnil	zobecnit	k5eAaPmAgMnS
Cavalieri	Cavaliere	k1gFnSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
závěry	závěr	k1gInPc4
dále	daleko	k6eAd2
upravil	upravit	k5eAaPmAgMnS
John	John	k1gMnSc1
Wallis	Wallis	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
období	období	k1gNnSc6
byl	být	k5eAaImAgInS
integrální	integrální	k2eAgInSc1d1
počet	počet	k1gInSc1
používán	používat	k5eAaImNgInS
především	především	k6eAd1
k	k	k7c3
určování	určování	k1gNnSc3
délek	délka	k1gFnPc2
<g/>
,	,	kIx,
ploch	plocha	k1gFnPc2
a	a	k8xC
objemů	objem	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
objevu	objev	k1gInSc2
derivace	derivace	k1gFnSc2
je	být	k5eAaImIp3nS
integrace	integrace	k1gFnSc1
považována	považován	k2eAgFnSc1d1
za	za	k7c4
k	k	k7c3
ní	on	k3xPp3gFnSc7
inverzní	inverzní	k2eAgInSc1d1
postup	postup	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgNnSc2
období	období	k1gNnSc2
pochází	pocházet	k5eAaImIp3nS
také	také	k9
Newtonova	Newtonův	k2eAgFnSc1d1
definice	definice	k1gFnSc1
integrálu	integrál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Cauchy	Caucha	k1gFnPc4
definoval	definovat	k5eAaBmAgInS
základy	základ	k1gInPc4
integrálního	integrální	k2eAgInSc2d1
počtu	počet	k1gInSc2
použitím	použití	k1gNnSc7
limity	limita	k1gFnSc2
jako	jako	k8xS,k8xC
limitu	limit	k1gInSc2
určitého	určitý	k2eAgInSc2d1
typu	typ	k1gInSc2
součtu	součet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
definice	definice	k1gFnPc4
byla	být	k5eAaImAgFnS
později	pozdě	k6eAd2
rozvinuta	rozvinout	k5eAaPmNgFnS
Riemannem	Riemanno	k1gNnSc7
do	do	k7c2
Riemannova	Riemannův	k2eAgInSc2d1
integrálu	integrál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
byla	být	k5eAaImAgFnS
definice	definice	k1gFnSc1
integrálu	integrál	k1gInSc2
dále	daleko	k6eAd2
rozšířena	rozšířit	k5eAaPmNgFnS
především	především	k9
díky	díky	k7c3
rozvoji	rozvoj	k1gInSc3
teorie	teorie	k1gFnSc2
množin	množina	k1gFnPc2
a	a	k8xC
zahrnutím	zahrnutí	k1gNnSc7
obecného	obecný	k2eAgInSc2d1
pojmu	pojem	k1gInSc2
míry	míra	k1gFnSc2
(	(	kIx(
<g/>
a	a	k8xC
také	také	k9
rozvojem	rozvoj	k1gInSc7
teorie	teorie	k1gFnSc2
míry	míra	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
Lebesgueovy	Lebesgueův	k2eAgFnSc2d1
míry	míra	k1gFnSc2
vytvořil	vytvořit	k5eAaPmAgInS
Lebesgue	Lebesgue	k1gInSc1
tzv.	tzv.	kA
Lebesgueův	Lebesgueův	k2eAgInSc1d1
integrál	integrál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobný	podobný	k2eAgInSc4d1
postup	postup	k1gInSc4
použili	použít	k5eAaPmAgMnP
i	i	k9
další	další	k2eAgMnPc1d1
matematici	matematik	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Rektorys	Rektorys	k1gInSc1
<g/>
,	,	kIx,
K.	K.	kA
a	a	k8xC
spol	spol	k1gInSc1
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Přehled	přehled	k1gInSc1
užité	užitý	k2eAgFnSc2d1
matematiky	matematika	k1gFnSc2
I.	I.	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prometheus	Prometheus	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80-7196-179-5	80-7196-179-5	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Integrál	integrál	k1gInSc1
</s>
<s>
Diferenciální	diferenciální	k2eAgInSc1d1
počet	počet	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
integrální	integrální	k2eAgInSc4d1
počet	počet	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Kniha	kniha	k1gFnSc1
Integrování	integrování	k1gNnSc2
ve	v	k7c6
Wikiknihách	Wikiknih	k1gInPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Matematika	matematika	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
121134	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4027232-1	4027232-1	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
7443	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85018804	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85018804	#num#	k4
</s>
