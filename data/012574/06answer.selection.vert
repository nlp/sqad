<s>
Grónsko	Grónsko	k1gNnSc1
je	být	k5eAaImIp3nS
největší	veliký	k2eAgInSc4d3
ostrov	ostrov	k1gInSc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
jen	jen	k9
15	[number]	k4
%	%	kIx~
jeho	jeho	k3xOp3gNnSc2
území	území	k1gNnSc2
(	(	kIx(
<g/>
asi	asi	k9
o	o	k7c6
velikosti	velikost	k1gFnSc6
Britských	britský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
<g/>
)	)	kIx)
není	být	k5eNaImIp3nS
trvale	trvale	k6eAd1
zaledněno	zaledněn	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>