<s>
Linux	linux	k1gInSc1	linux
nebo	nebo	k8xC	nebo
GNU	gnu	k1gMnSc1	gnu
<g/>
/	/	kIx~	/
<g/>
Linux	Linux	kA	Linux
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
GNU	gnu	k1gMnSc1	gnu
<g/>
/	/	kIx~	/
<g/>
Linux	Linux	kA	Linux
kontroverze	kontroverze	k1gFnSc1	kontroverze
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
informatice	informatika	k1gFnSc6	informatika
označení	označení	k1gNnSc2	označení
pro	pro	k7c4	pro
svobodný	svobodný	k2eAgInSc4d1	svobodný
a	a	k8xC	a
otevřený	otevřený	k2eAgInSc4d1	otevřený
počítačový	počítačový	k2eAgInSc4d1	počítačový
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
linuxovém	linuxový	k2eAgNnSc6d1	linuxové
jádru	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Linuxové	linuxový	k2eAgInPc1d1	linuxový
systémy	systém	k1gInPc1	systém
jsou	být	k5eAaImIp3nP	být
šířeny	šířit	k5eAaImNgInP	šířit
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
distribucí	distribuce	k1gFnPc2	distribuce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nainstalovat	nainstalovat	k5eAaPmF	nainstalovat
nebo	nebo	k8xC	nebo
používat	používat	k5eAaImF	používat
bez	bez	k7c2	bez
instalace	instalace	k1gFnSc2	instalace
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
live	live	k1gFnPc2	live
CD	CD	kA	CD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používané	používaný	k2eAgFnPc1d1	používaná
licence	licence	k1gFnPc1	licence
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
systém	systém	k1gInSc4	systém
zdarma	zdarma	k6eAd1	zdarma
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
volně	volně	k6eAd1	volně
používat	používat	k5eAaImF	používat
<g/>
,	,	kIx,	,
distribuovat	distribuovat	k5eAaBmF	distribuovat
(	(	kIx(	(
<g/>
kopírovat	kopírovat	k5eAaImF	kopírovat
<g/>
,	,	kIx,	,
sdílet	sdílet	k5eAaImF	sdílet
<g/>
)	)	kIx)	)
i	i	k9	i
upravovat	upravovat	k5eAaImF	upravovat
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
proprietárních	proprietární	k2eAgInPc2d1	proprietární
systémů	systém	k1gInPc2	systém
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
či	či	k8xC	či
Mac	Mac	kA	Mac
OS	OS	kA	OS
X	X	kA	X
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
které	který	k3yRgFnPc4	který
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
platit	platit	k5eAaImF	platit
a	a	k8xC	a
dodržovat	dodržovat	k5eAaImF	dodržovat
omezující	omezující	k2eAgFnPc4d1	omezující
licence	licence	k1gFnPc4	licence
<g/>
.	.	kIx.	.
</s>
<s>
Operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
Linux	Linux	kA	Linux
používá	používat	k5eAaImIp3nS	používat
unixové	unixový	k2eAgNnSc1d1	unixové
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
myšlenek	myšlenka	k1gFnPc2	myšlenka
Unixu	Unix	k1gInSc2	Unix
a	a	k8xC	a
respektuje	respektovat	k5eAaImIp3nS	respektovat
příslušné	příslušný	k2eAgInPc4d1	příslušný
standardy	standard	k1gInPc4	standard
POSIX	POSIX	kA	POSIX
a	a	k8xC	a
Single	singl	k1gInSc5	singl
UNIX	UNIX	kA	UNIX
Specification	Specification	k1gInSc1	Specification
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
křestního	křestní	k2eAgNnSc2d1	křestní
jména	jméno	k1gNnSc2	jméno
jeho	on	k3xPp3gMnSc4	on
tvůrce	tvůrce	k1gMnSc4	tvůrce
Linuse	Linuse	k1gFnSc2	Linuse
Torvaldse	Torvaldse	k1gFnSc1	Torvaldse
a	a	k8xC	a
koncovka	koncovka	k1gFnSc1	koncovka
písmenem	písmeno	k1gNnSc7	písmeno
"	"	kIx"	"
<g/>
x	x	k?	x
<g/>
"	"	kIx"	"
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
právě	právě	k9	právě
na	na	k7c4	na
Unix	Unix	k1gInSc4	Unix
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
XENIX	XENIX	kA	XENIX
<g/>
,	,	kIx,	,
Ultrix	Ultrix	k1gInSc1	Ultrix
<g/>
,	,	kIx,	,
IRIX	IRIX	kA	IRIX
<g/>
,	,	kIx,	,
AIX	AIX	kA	AIX
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
UN	UN	kA	UN
<g/>
*	*	kIx~	*
<g/>
Xy	Xy	k1gMnSc1	Xy
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
Linuxu	linux	k1gInSc2	linux
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
spouštět	spouštět	k5eAaImF	spouštět
více	hodně	k6eAd2	hodně
programů	program	k1gInPc2	program
(	(	kIx(	(
<g/>
úloh	úloha	k1gFnPc2	úloha
<g/>
)	)	kIx)	)
najednou	najednou	k6eAd1	najednou
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
program	program	k1gInSc1	program
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
skládat	skládat	k5eAaImF	skládat
z	z	k7c2	z
jednoho	jeden	k4xCgMnSc2	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
víceúlohový	víceúlohový	k2eAgInSc4d1	víceúlohový
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
proces	proces	k1gInSc1	proces
potom	potom	k6eAd1	potom
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
jeden	jeden	k4xCgInSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
podprocesů	podproces	k1gInPc2	podproces
<g/>
.	.	kIx.	.
</s>
<s>
Operační	operační	k2eAgInPc1d1	operační
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
běh	běh	k1gInSc4	běh
více	hodně	k6eAd2	hodně
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
podprocesů	podproces	k1gInPc2	podproces
současně	současně	k6eAd1	současně
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
využít	využít	k5eAaPmF	využít
i	i	k9	i
vícejádrové	vícejádrový	k2eAgInPc4d1	vícejádrový
a	a	k8xC	a
víceprocesorové	víceprocesorový	k2eAgInPc4d1	víceprocesorový
počítače	počítač	k1gInPc4	počítač
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
zefektivnit	zefektivnit	k5eAaPmF	zefektivnit
práci	práce	k1gFnSc4	práce
uživatele	uživatel	k1gMnSc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
Linuxu	linux	k1gInSc2	linux
je	být	k5eAaImIp3nS	být
víceuživatelské	víceuživatelský	k2eAgNnSc1d1	víceuživatelské
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
spouštět	spouštět	k5eAaImF	spouštět
programy	program	k1gInPc4	program
různých	různý	k2eAgMnPc2d1	různý
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
jeden	jeden	k4xCgMnSc1	jeden
uživatel	uživatel	k1gMnSc1	uživatel
může	moct	k5eAaImIp3nS	moct
obsluhovat	obsluhovat	k5eAaImF	obsluhovat
počítač	počítač	k1gInSc4	počítač
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
další	další	k2eAgMnPc1d1	další
mohou	moct	k5eAaImIp3nP	moct
obsluhovat	obsluhovat	k5eAaImF	obsluhovat
stejný	stejný	k2eAgInSc4d1	stejný
počítač	počítač	k1gInSc4	počítač
například	například	k6eAd1	například
přes	přes	k7c4	přes
síť	síť	k1gFnSc4	síť
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
Internet	Internet	k1gInSc1	Internet
<g/>
.	.	kIx.	.
</s>
<s>
Příslušné	příslušný	k2eAgInPc1d1	příslušný
uživatelské	uživatelský	k2eAgInPc1d1	uživatelský
účty	účet	k1gInPc1	účet
jsou	být	k5eAaImIp3nP	být
před	před	k7c7	před
neoprávněným	oprávněný	k2eNgInSc7d1	neoprávněný
přístupem	přístup	k1gInSc7	přístup
chráněny	chránit	k5eAaImNgInP	chránit
autentizací	autentizace	k1gFnSc7	autentizace
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
jménem	jméno	k1gNnSc7	jméno
a	a	k8xC	a
heslem	heslo	k1gNnSc7	heslo
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelé	uživatel	k1gMnPc1	uživatel
mají	mít	k5eAaImIp3nP	mít
přidělena	přidělen	k2eAgNnPc4d1	přiděleno
různá	různý	k2eAgNnPc4d1	různé
práva	právo	k1gNnPc4	právo
<g/>
,	,	kIx,	,
od	od	k7c2	od
naprosté	naprostý	k2eAgFnSc2d1	naprostá
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
správce	správce	k1gMnSc1	správce
(	(	kIx(	(
<g/>
root	root	k1gMnSc1	root
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
různé	různý	k2eAgFnSc2d1	různá
míry	míra	k1gFnSc2	míra
omezené	omezený	k2eAgInPc4d1	omezený
účty	účet	k1gInPc4	účet
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
označením	označení	k1gNnSc7	označení
Linux	Linux	kA	Linux
míněno	mínit	k5eAaImNgNnS	mínit
nejen	nejen	k6eAd1	nejen
jádro	jádro	k1gNnSc1	jádro
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
též	též	k9	též
veškeré	veškerý	k3xTgNnSc1	veškerý
programové	programový	k2eAgNnSc1d1	programové
vybavení	vybavení	k1gNnSc1	vybavení
(	(	kIx(	(
<g/>
software	software	k1gInSc1	software
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
uživatelé	uživatel	k1gMnPc1	uživatel
používají	používat	k5eAaImIp3nP	používat
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
,	,	kIx,	,
utility	utilita	k1gFnPc4	utilita
<g/>
,	,	kIx,	,
grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vyvíjeno	vyvíjen	k2eAgNnSc1d1	vyvíjeno
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
samotném	samotný	k2eAgNnSc6d1	samotné
jádře	jádro	k1gNnSc6	jádro
Linuxu	linux	k1gInSc2	linux
<g/>
.	.	kIx.	.
</s>
<s>
Linux	linux	k1gInSc1	linux
je	být	k5eAaImIp3nS	být
šířen	šířit	k5eAaImNgInS	šířit
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
linuxových	linuxový	k2eAgFnPc2d1	linuxová
distribucí	distribuce	k1gFnPc2	distribuce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
jak	jak	k6eAd1	jak
zmíněné	zmíněný	k2eAgNnSc4d1	zmíněné
jádro	jádro	k1gNnSc4	jádro
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
doplňující	doplňující	k2eAgInSc1d1	doplňující
software	software	k1gInSc1	software
v	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
jeho	jeho	k3xOp3gFnSc4	jeho
instalaci	instalace	k1gFnSc4	instalace
a	a	k8xC	a
používání	používání	k1gNnSc4	používání
(	(	kIx(	(
<g/>
instalace	instalace	k1gFnSc1	instalace
někdy	někdy	k6eAd1	někdy
není	být	k5eNaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
Live	Live	k1gInSc4	Live
CD	CD	kA	CD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Linux	linux	k1gInSc1	linux
je	být	k5eAaImIp3nS	být
open	open	k1gNnSc4	open
source	source	k1gMnSc2	source
software	software	k1gInSc4	software
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
jeho	jeho	k3xOp3gInPc4	jeho
zdrojové	zdrojový	k2eAgInPc4d1	zdrojový
kódy	kód	k1gInPc4	kód
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
lze	lze	k6eAd1	lze
za	za	k7c4	za
dodržení	dodržení	k1gNnSc4	dodržení
jistých	jistý	k2eAgFnPc2d1	jistá
podmínek	podmínka	k1gFnPc2	podmínka
upravovat	upravovat	k5eAaImF	upravovat
a	a	k8xC	a
vše	všechen	k3xTgNnSc4	všechen
dále	daleko	k6eAd2	daleko
šířit	šířit	k5eAaImF	šířit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
zneužitím	zneužití	k1gNnSc7	zneužití
zdrojových	zdrojový	k2eAgInPc2d1	zdrojový
kódů	kód	k1gInPc2	kód
používá	používat	k5eAaImIp3nS	používat
open	open	k1gNnSc4	open
source	source	k1gMnSc2	source
software	software	k1gInSc4	software
různé	různý	k2eAgFnSc2d1	různá
licence	licence	k1gFnSc2	licence
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
jádro	jádro	k1gNnSc1	jádro
Linuxu	linux	k1gInSc2	linux
je	být	k5eAaImIp3nS	být
chráněno	chráněn	k2eAgNnSc4d1	chráněno
a	a	k8xC	a
šířeno	šířen	k2eAgNnSc4d1	šířeno
pod	pod	k7c7	pod
licencí	licence	k1gFnSc7	licence
GPLv	GPLvum	k1gNnPc2	GPLvum
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
s	s	k7c7	s
důležitou	důležitý	k2eAgFnSc7d1	důležitá
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Software	software	k1gInSc1	software
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Linuxem	linux	k1gInSc7	linux
šířen	šířen	k2eAgInSc1d1	šířen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
nejrůznějšími	různý	k2eAgFnPc7d3	nejrůznější
licencemi	licence	k1gFnPc7	licence
(	(	kIx(	(
<g/>
GPL	GPL	kA	GPL
<g/>
,	,	kIx,	,
LGPL	LGPL	kA	LGPL
<g/>
,	,	kIx,	,
MPL	MPL	kA	MPL
<g/>
,	,	kIx,	,
Licence	licence	k1gFnSc1	licence
MIT	MIT	kA	MIT
<g/>
,	,	kIx,	,
BSD	BSD	kA	BSD
licence	licence	k1gFnSc2	licence
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
distribucí	distribuce	k1gFnSc7	distribuce
vybírá	vybírat	k5eAaImIp3nS	vybírat
software	software	k1gInSc1	software
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gFnPc2	jeho
licencí	licence	k1gFnPc2	licence
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vyhovoval	vyhovovat	k5eAaImAgInS	vyhovovat
buď	buď	k8xC	buď
volnějšímu	volný	k2eAgInSc3d2	volnější
výkladu	výklad	k1gInSc3	výklad
open	opena	k1gFnPc2	opena
source	sourec	k1gInSc2	sourec
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
přísnějšímu	přísný	k2eAgInSc3d2	přísnější
výkladu	výklad	k1gInSc3	výklad
svobodného	svobodný	k2eAgNnSc2d1	svobodné
software	software	k1gInSc4	software
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
free	freat	k5eAaPmIp3nS	freat
software	software	k1gInSc1	software
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
nemá	mít	k5eNaImIp3nS	mít
na	na	k7c4	na
koncového	koncový	k2eAgMnSc4d1	koncový
uživatele	uživatel	k1gMnSc4	uživatel
přímý	přímý	k2eAgInSc1d1	přímý
vliv	vliv	k1gInSc1	vliv
<g/>
,	,	kIx,	,
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
to	ten	k3xDgNnSc1	ten
však	však	k9	však
zejména	zejména	k9	zejména
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
vývoj	vývoj	k1gInSc4	vývoj
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
součástí	součást	k1gFnPc2	součást
Linuxu	linux	k1gInSc2	linux
<g/>
.	.	kIx.	.
</s>
<s>
Linus	Linus	k1gInSc1	Linus
Torvalds	Torvalds	k1gInSc1	Torvalds
začal	začít	k5eAaPmAgInS	začít
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
jádro	jádro	k1gNnSc4	jádro
Linuxu	linux	k1gInSc2	linux
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
jako	jako	k8xS	jako
svůj	svůj	k3xOyFgInSc4	svůj
koníček	koníček	k1gInSc4	koníček
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
důvody	důvod	k1gInPc7	důvod
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
právě	právě	k6eAd1	právě
unixového	unixový	k2eAgInSc2d1	unixový
systému	systém	k1gInSc2	systém
patřil	patřit	k5eAaImAgInS	patřit
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
Unix	Unix	k1gInSc1	Unix
je	být	k5eAaImIp3nS	být
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
upřednostňuje	upřednostňovat	k5eAaImIp3nS	upřednostňovat
jednoduchost	jednoduchost	k1gFnSc4	jednoduchost
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
přednášen	přednášen	k2eAgInSc1d1	přednášen
na	na	k7c6	na
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
Torvalds	Torvalds	k1gInSc1	Torvalds
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
finské	finský	k2eAgFnSc6d1	finská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
obor	obora	k1gFnPc2	obora
Informatika	informatika	k1gFnSc1	informatika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Torvalds	Torvalds	k1gInSc1	Torvalds
byl	být	k5eAaImAgInS	být
dále	daleko	k6eAd2	daleko
inspirován	inspirován	k2eAgInSc1d1	inspirován
MINIXem	MINIXem	k1gInSc1	MINIXem
od	od	k7c2	od
Andrewa	Andrewus	k1gMnSc2	Andrewus
Tanenbauma	Tanenbaum	k1gMnSc2	Tanenbaum
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
napsal	napsat	k5eAaBmAgMnS	napsat
svoji	svůj	k3xOyFgFnSc4	svůj
verzi	verze	k1gFnSc4	verze
unixového	unixový	k2eAgInSc2d1	unixový
systému	systém	k1gInSc2	systém
jako	jako	k8xS	jako
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
projekt	projekt	k1gInSc1	projekt
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
výuce	výuka	k1gFnSc3	výuka
a	a	k8xC	a
knihám	kniha	k1gFnPc3	kniha
o	o	k7c6	o
operačních	operační	k2eAgInPc6d1	operační
systémech	systém	k1gInPc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
však	však	k9	však
Torvalds	Torvalds	k1gInSc1	Torvalds
nevyužil	využít	k5eNaPmAgInS	využít
svůj	svůj	k3xOyFgInSc4	svůj
projekt	projekt	k1gInSc4	projekt
komerčně	komerčně	k6eAd1	komerčně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
preferoval	preferovat	k5eAaImAgInS	preferovat
otevřený	otevřený	k2eAgInSc4d1	otevřený
vývoj	vývoj	k1gInSc4	vývoj
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
open	open	k1gMnSc1	open
source	source	k1gMnSc1	source
software	software	k1gInSc1	software
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
linuxového	linuxový	k2eAgNnSc2d1	linuxové
jádra	jádro	k1gNnSc2	jádro
(	(	kIx(	(
<g/>
0.01	[number]	k4	0.01
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
Internetu	Internet	k1gInSc6	Internet
zveřejněna	zveřejněn	k2eAgFnSc1d1	zveřejněna
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Linusově	Linusův	k2eAgFnSc3d1	Linusova
překvapení	překvapení	k1gNnSc3	překvapení
byl	být	k5eAaImAgInS	být
o	o	k7c4	o
jeho	jeho	k3xOp3gInSc4	jeho
nedokonalý	dokonalý	k2eNgInSc4d1	nedokonalý
systém	systém	k1gInSc4	systém
velký	velký	k2eAgInSc4d1	velký
zájem	zájem	k1gInSc4	zájem
a	a	k8xC	a
záhy	záhy	k6eAd1	záhy
začal	začít	k5eAaPmAgInS	začít
dostávat	dostávat	k5eAaImF	dostávat
e-mailem	eail	k1gInSc7	e-mail
další	další	k2eAgInPc4d1	další
podněty	podnět	k1gInPc4	podnět
<g/>
,	,	kIx,	,
opravy	oprava	k1gFnPc4	oprava
a	a	k8xC	a
zdrojové	zdrojový	k2eAgInPc4d1	zdrojový
kódy	kód	k1gInPc4	kód
<g/>
.	.	kIx.	.
</s>
<s>
Torvalds	Torvalds	k6eAd1	Torvalds
jádro	jádro	k1gNnSc4	jádro
dále	daleko	k6eAd2	daleko
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
začal	začít	k5eAaPmAgMnS	začít
příspěvky	příspěvek	k1gInPc4	příspěvek
ostatních	ostatní	k2eAgMnPc2d1	ostatní
do	do	k7c2	do
svého	svůj	k3xOyFgNnSc2	svůj
jádra	jádro	k1gNnSc2	jádro
začleňoval	začleňovat	k5eAaImAgMnS	začleňovat
a	a	k8xC	a
upravené	upravený	k2eAgInPc1d1	upravený
zdrojové	zdrojový	k2eAgInPc1d1	zdrojový
kódy	kód	k1gInPc1	kód
obratem	obratem	k6eAd1	obratem
zveřejňovat	zveřejňovat	k5eAaImF	zveřejňovat
(	(	kIx(	(
<g/>
další	další	k2eAgFnSc1d1	další
verze	verze	k1gFnSc1	verze
byla	být	k5eAaImAgFnS	být
zveřejněna	zveřejnit	k5eAaPmNgFnS	zveřejnit
již	již	k6eAd1	již
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
podílely	podílet	k5eAaImAgInP	podílet
tisíce	tisíc	k4xCgInPc1	tisíc
vývojářů	vývojář	k1gMnPc2	vývojář
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Model	model	k1gInSc1	model
vývoje	vývoj	k1gInSc2	vývoj
linuxového	linuxový	k2eAgNnSc2d1	linuxové
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
podobného	podobný	k2eAgInSc2d1	podobný
softwaru	software	k1gInSc2	software
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
výstižně	výstižně	k6eAd1	výstižně
popsán	popsat	k5eAaPmNgInS	popsat
v	v	k7c6	v
eseji	esej	k1gInSc6	esej
Katedrála	katedrála	k1gFnSc1	katedrála
a	a	k8xC	a
tržiště	tržiště	k1gNnSc1	tržiště
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
The	The	k1gMnSc1	The
Cathedral	Cathedral	k1gFnSc2	Cathedral
and	and	k?	and
the	the	k?	the
Bazaar	Bazaara	k1gFnPc2	Bazaara
<g/>
)	)	kIx)	)
od	od	k7c2	od
Erica	Ericus	k1gMnSc2	Ericus
S.	S.	kA	S.
Raymonda	Raymond	k1gMnSc2	Raymond
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
velmi	velmi	k6eAd1	velmi
brzo	brzo	k6eAd1	brzo
předběhl	předběhnout	k5eAaPmAgMnS	předběhnout
Linux	linux	k1gInSc4	linux
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
svůj	svůj	k3xOyFgInSc4	svůj
vzor	vzor	k1gInSc4	vzor
–	–	k?	–
MINIX	MINIX	kA	MINIX
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Zejména	zejména	k9	zejména
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
byl	být	k5eAaImAgMnS	být
při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
využíván	využívat	k5eAaPmNgInS	využívat
Projekt	projekt	k1gInSc1	projekt
GNU	gnu	k1gNnSc2	gnu
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
zabýval	zabývat	k5eAaImAgInS	zabývat
myšlenkou	myšlenka	k1gFnSc7	myšlenka
vývoje	vývoj	k1gInSc2	vývoj
volně	volně	k6eAd1	volně
dostupného	dostupný	k2eAgInSc2d1	dostupný
unixového	unixový	k2eAgInSc2d1	unixový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vlastní	vlastní	k2eAgNnSc1d1	vlastní
jádro	jádro	k1gNnSc1	jádro
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
neměl	mít	k5eNaImAgInS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
projektu	projekt	k1gInSc2	projekt
GNU	gnu	k1gNnSc2	gnu
hned	hned	k9	hned
počátku	počátek	k1gInSc2	počátek
Linux	Linux	kA	Linux
využil	využít	k5eAaPmAgMnS	využít
shell	shell	k1gMnSc1	shell
bash	bash	k1gMnSc1	bash
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
nástroje	nástroj	k1gInPc4	nástroj
(	(	kIx(	(
<g/>
základní	základní	k2eAgInPc4d1	základní
unixové	unixový	k2eAgInPc4d1	unixový
nástroje	nástroj	k1gInPc4	nástroj
používané	používaný	k2eAgInPc4d1	používaný
na	na	k7c6	na
příkazovém	příkazový	k2eAgInSc6d1	příkazový
řádku	řádek	k1gInSc6	řádek
<g/>
,	,	kIx,	,
kompilátor	kompilátor	k1gInSc1	kompilátor
GCC	GCC	kA	GCC
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
též	též	k9	též
GNU	gnu	k1gMnSc1	gnu
C	C	kA	C
Library	Librar	k1gMnPc7	Librar
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgInSc1	sám
Linux	linux	k1gInSc1	linux
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgMnS	být
součástí	součást	k1gFnSc7	součást
GNU	gnu	k1gNnSc2	gnu
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
samotné	samotný	k2eAgNnSc1d1	samotné
jádro	jádro	k1gNnSc1	jádro
používá	používat	k5eAaImIp3nS	používat
licenci	licence	k1gFnSc4	licence
GPLv	GPLv	k1gInSc1	GPLv
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
též	též	k9	též
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
GNU	gnu	k1gNnSc2	gnu
<g/>
.	.	kIx.	.
</s>
<s>
Torvalds	Torvalds	k6eAd1	Torvalds
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
hlavou	hlava	k1gFnSc7	hlava
vývoje	vývoj	k1gInSc2	vývoj
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
zveřejňováno	zveřejňovat	k5eAaImNgNnS	zveřejňovat
na	na	k7c6	na
serveru	server	k1gInSc6	server
kernel	kernel	k1gInSc1	kernel
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
vydává	vydávat	k5eAaPmIp3nS	vydávat
nové	nový	k2eAgFnPc4d1	nová
verze	verze	k1gFnPc4	verze
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
některé	některý	k3yIgFnPc1	některý
starší	starý	k2eAgFnPc1d2	starší
verze	verze	k1gFnPc1	verze
jsou	být	k5eAaImIp3nP	být
udržovány	udržovat	k5eAaImNgFnP	udržovat
jinými	jiný	k2eAgMnPc7d1	jiný
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
něj	on	k3xPp3gInSc2	on
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
tisíce	tisíc	k4xCgInPc1	tisíc
programátorů	programátor	k1gMnPc2	programátor
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Již	jenž	k3xRgFnSc4	jenž
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
vývoj	vývoj	k1gInSc1	vývoj
jádra	jádro	k1gNnSc2	jádro
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
placen	platit	k5eAaImNgMnS	platit
firmami	firma	k1gFnPc7	firma
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Red	Red	k1gFnSc1	Red
Hat	hat	k0	hat
<g/>
,	,	kIx,	,
Intel	Intel	kA	Intel
<g/>
,	,	kIx,	,
IBM	IBM	kA	IBM
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
součásti	součást	k1gFnPc1	součást
Linuxu	linux	k1gInSc2	linux
jsou	být	k5eAaImIp3nP	být
vyvíjeny	vyvíjen	k2eAgFnPc1d1	vyvíjena
samostatně	samostatně	k6eAd1	samostatně
(	(	kIx(	(
<g/>
např.	např.	kA	např.
KDE	kde	k6eAd1	kde
<g/>
,	,	kIx,	,
GNOME	GNOME	kA	GNOME
<g/>
,	,	kIx,	,
X.	X.	kA	X.
<g/>
Org	Org	k1gFnSc2	Org
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Logem	log	k1gInSc7	log
a	a	k8xC	a
maskotem	maskot	k1gInSc7	maskot
Linuxu	linux	k1gInSc2	linux
je	být	k5eAaImIp3nS	být
tučňák	tučňák	k1gMnSc1	tučňák
Tux	Tux	k1gFnSc2	Tux
vycházející	vycházející	k2eAgFnSc2d1	vycházející
z	z	k7c2	z
obrázku	obrázek	k1gInSc2	obrázek
Larryho	Larry	k1gMnSc2	Larry
Ewinga	Ewing	k1gMnSc2	Ewing
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
jiná	jiný	k2eAgNnPc1d1	jiné
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
známá	známý	k2eAgNnPc4d1	známé
zpodobnění	zpodobnění	k1gNnPc4	zpodobnění
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
OS-tan	OSan	k1gInSc4	OS-tan
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
Linux	linux	k1gInSc1	linux
<g/>
"	"	kIx"	"
nevytvořil	vytvořit	k5eNaPmAgInS	vytvořit
sám	sám	k3xTgInSc1	sám
Torvalds	Torvalds	k1gInSc1	Torvalds
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Ari	Ari	k1gMnSc4	Ari
Lemmke	Lemmk	k1gMnSc4	Lemmk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
helsinské	helsinský	k2eAgFnSc6d1	Helsinská
univerzitě	univerzita	k1gFnSc6	univerzita
jako	jako	k9	jako
správce	správce	k1gMnSc1	správce
FTP	FTP	kA	FTP
serveru	server	k1gInSc2	server
ftp	ftp	k?	ftp
<g/>
.	.	kIx.	.
<g/>
funet	funet	k1gMnSc1	funet
<g/>
.	.	kIx.	.
<g/>
fi	fi	k0	fi
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
uveřejněna	uveřejnit	k5eAaPmNgFnS	uveřejnit
první	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
Linuxu	linux	k1gInSc2	linux
<g/>
.	.	kIx.	.
</s>
<s>
Torvalds	Torvalds	k6eAd1	Torvalds
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
jméno	jméno	k1gNnSc4	jméno
"	"	kIx"	"
<g/>
Freax	Freax	k1gInSc1	Freax
<g/>
"	"	kIx"	"
jako	jako	k8xS	jako
free	free	k6eAd1	free
(	(	kIx(	(
<g/>
svobodný	svobodný	k2eAgMnSc1d1	svobodný
<g/>
)	)	kIx)	)
+	+	kIx~	+
freak	freak	k1gMnSc1	freak
(	(	kIx(	(
<g/>
blázen	blázen	k1gMnSc1	blázen
<g/>
)	)	kIx)	)
+	+	kIx~	+
x	x	k?	x
(	(	kIx(	(
<g/>
unixový	unixový	k2eAgInSc1d1	unixový
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
Lemmkemu	Lemmkem	k1gMnSc3	Lemmkem
nelíbilo	líbit	k5eNaImAgNnS	líbit
a	a	k8xC	a
na	na	k7c6	na
FTP	FTP	kA	FTP
serveru	server	k1gInSc6	server
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
adresář	adresář	k1gInSc1	adresář
"	"	kIx"	"
<g/>
Linux	Linux	kA	Linux
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
Linusův	Linusův	k2eAgInSc1d1	Linusův
unixový	unixový	k2eAgInSc1d1	unixový
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Linux	Linux	kA	Linux
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgMnS	stát
ochrannou	ochranný	k2eAgFnSc7d1	ochranná
známkou	známka	k1gFnSc7	známka
(	(	kIx(	(
<g/>
č.	č.	k?	č.
1916230	[number]	k4	1916230
<g/>
)	)	kIx)	)
na	na	k7c4	na
"	"	kIx"	"
<g/>
software	software	k1gInSc4	software
počítačového	počítačový	k2eAgInSc2d1	počítačový
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
počítačem	počítač	k1gInSc7	počítač
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
sám	sám	k3xTgMnSc1	sám
Linus	Linus	k1gMnSc1	Linus
Torvalds	Torvaldsa	k1gFnPc2	Torvaldsa
<g/>
.	.	kIx.	.
</s>
<s>
Licencování	licencování	k1gNnSc1	licencování
této	tento	k3xDgFnSc2	tento
ochranné	ochranný	k2eAgFnSc2d1	ochranná
známky	známka	k1gFnSc2	známka
nyní	nyní	k6eAd1	nyní
obstarává	obstarávat	k5eAaImIp3nS	obstarávat
Linux	linux	k1gInSc1	linux
Mark	Mark	k1gMnSc1	Mark
Institute	institut	k1gInSc5	institut
(	(	kIx(	(
<g/>
LMI	LMI	kA	LMI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
GNU	gnu	k1gNnSc1	gnu
<g/>
/	/	kIx~	/
<g/>
Linux	linux	k1gInSc1	linux
kontroverze	kontroverze	k1gFnSc2	kontroverze
<g/>
.	.	kIx.	.
</s>
<s>
Richard	Richard	k1gMnSc1	Richard
Stallman	Stallman	k1gMnSc1	Stallman
a	a	k8xC	a
Free	Free	k1gFnSc1	Free
Software	software	k1gInSc1	software
Foundation	Foundation	k1gInSc1	Foundation
změnily	změnit	k5eAaPmAgInP	změnit
označení	označení	k1gNnSc4	označení
Linuxu	linux	k1gInSc2	linux
na	na	k7c4	na
GNU	gnu	k1gNnSc4	gnu
<g/>
/	/	kIx~	/
<g/>
Linux	Linux	kA	Linux
a	a	k8xC	a
vidí	vidět	k5eAaImIp3nP	vidět
linuxové	linuxový	k2eAgFnSc2d1	linuxová
distribuce	distribuce	k1gFnSc2	distribuce
používající	používající	k2eAgInSc1d1	používající
software	software	k1gInSc1	software
GNU	gnu	k1gNnSc2	gnu
jako	jako	k8xS	jako
varianty	varianta	k1gFnSc2	varianta
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
GNU	gnu	k1gNnSc2	gnu
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
požadují	požadovat	k5eAaImIp3nP	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
takové	takový	k3xDgInPc1	takový
systémy	systém	k1gInPc1	systém
označovaly	označovat	k5eAaImAgInP	označovat
jako	jako	k9	jako
GNU	gnu	k1gNnSc1	gnu
<g/>
/	/	kIx~	/
<g/>
Linux	linux	k1gInSc1	linux
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
"	"	kIx"	"
<g/>
Na	na	k7c6	na
Linuxu	linux	k1gInSc6	linux
založený	založený	k2eAgMnSc1d1	založený
GNU	gnu	k1gMnSc1	gnu
systém	systém	k1gInSc1	systém
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Média	médium	k1gNnPc1	médium
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
uživatelů	uživatel	k1gMnPc2	uživatel
preferují	preferovat	k5eAaImIp3nP	preferovat
krátké	krátký	k2eAgNnSc4d1	krátké
označení	označení	k1gNnSc4	označení
této	tento	k3xDgFnSc2	tento
rodiny	rodina	k1gFnSc2	rodina
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
jako	jako	k8xC	jako
Linux	linux	k1gInSc4	linux
<g/>
.	.	kIx.	.
</s>
<s>
Distribuce	distribuce	k1gFnSc1	distribuce
Debian	Debiana	k1gFnPc2	Debiana
používá	používat	k5eAaImIp3nS	používat
označení	označení	k1gNnSc1	označení
GNU	gnu	k1gNnSc2	gnu
<g/>
/	/	kIx~	/
<g/>
Linux	linux	k1gInSc1	linux
na	na	k7c4	na
výzvu	výzva	k1gFnSc4	výzva
Richarda	Richard	k1gMnSc2	Richard
Stallmana	Stallman	k1gMnSc2	Stallman
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
samotná	samotný	k2eAgFnSc1d1	samotná
FSF	FSF	kA	FSF
pak	pak	k6eAd1	pak
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
obvyklých	obvyklý	k2eAgFnPc6d1	obvyklá
distribucích	distribuce	k1gFnPc6	distribuce
Linuxu	linux	k1gInSc2	linux
obsaženy	obsažen	k2eAgInPc4d1	obsažen
vlastní	vlastní	k2eAgInSc4d1	vlastní
projekty	projekt	k1gInPc4	projekt
GNU	gnu	k1gNnSc2	gnu
v	v	k7c6	v
menšině	menšina	k1gFnSc6	menšina
(	(	kIx(	(
<g/>
některé	některý	k3yIgInPc1	některý
významné	významný	k2eAgInPc1d1	významný
projekty	projekt	k1gInPc1	projekt
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
od	od	k7c2	od
GNU	gnu	k1gNnSc2	gnu
oprostily	oprostit	k5eAaPmAgFnP	oprostit
–	–	k?	–
např.	např.	kA	např.
GCC	GCC	kA	GCC
<g/>
,	,	kIx,	,
GNU	gnu	k1gNnSc1	gnu
C	C	kA	C
Library	Librara	k1gFnSc2	Librara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
projektů	projekt	k1gInPc2	projekt
(	(	kIx(	(
<g/>
součásti	součást	k1gFnPc1	součást
distribucí	distribuce	k1gFnSc7	distribuce
<g/>
)	)	kIx)	)
sice	sice	k8xC	sice
využívá	využívat	k5eAaPmIp3nS	využívat
licence	licence	k1gFnSc1	licence
od	od	k7c2	od
GNU	gnu	k1gNnSc2	gnu
(	(	kIx(	(
<g/>
GPL	GPL	kA	GPL
<g/>
,	,	kIx,	,
LGPL	LGPL	kA	LGPL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
to	ten	k3xDgNnSc1	ten
projekty	projekt	k1gInPc1	projekt
GNU	gnu	k1gNnSc2	gnu
(	(	kIx(	(
<g/>
nemají	mít	k5eNaImIp3nP	mít
s	s	k7c7	s
GNU	gnu	k1gNnSc7	gnu
nic	nic	k6eAd1	nic
společného	společný	k2eAgInSc2d1	společný
kromě	kromě	k7c2	kromě
použité	použitý	k2eAgFnSc2d1	použitá
licence	licence	k1gFnSc2	licence
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
spíše	spíše	k9	spíše
o	o	k7c4	o
ideovou	ideový	k2eAgFnSc4d1	ideová
záležitost	záležitost	k1gFnSc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Linuxová	linuxový	k2eAgFnSc1d1	linuxová
distribuce	distribuce	k1gFnSc1	distribuce
a	a	k8xC	a
Seznam	seznam	k1gInSc1	seznam
distribucí	distribuce	k1gFnPc2	distribuce
Linuxu	linux	k1gInSc2	linux
<g/>
.	.	kIx.	.
</s>
<s>
Linux	linux	k1gInSc1	linux
jako	jako	k8xS	jako
takový	takový	k3xDgInSc4	takový
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
jádro	jádro	k1gNnSc1	jádro
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
počítač	počítač	k1gInSc1	počítač
s	s	k7c7	s
Linuxem	linux	k1gInSc7	linux
používat	používat	k5eAaImF	používat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
doplnit	doplnit	k5eAaPmF	doplnit
jádro	jádro	k1gNnSc4	jádro
o	o	k7c4	o
další	další	k2eAgInPc4d1	další
programy	program	k1gInPc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
tvoří	tvořit	k5eAaImIp3nS	tvořit
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
utility	utilita	k1gFnPc4	utilita
(	(	kIx(	(
<g/>
malé	malý	k2eAgInPc4d1	malý
programy	program	k1gInPc4	program
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
systémové	systémový	k2eAgInPc1d1	systémový
nástroje	nástroj	k1gInPc1	nástroj
a	a	k8xC	a
které	který	k3yRgFnPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
pro	pro	k7c4	pro
zajištění	zajištění	k1gNnSc4	zajištění
startu	start	k1gInSc2	start
(	(	kIx(	(
<g/>
bootování	bootování	k1gNnSc2	bootování
<g/>
)	)	kIx)	)
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
zajištění	zajištění	k1gNnSc4	zajištění
běhu	běh	k1gInSc2	běh
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
uživatele	uživatel	k1gMnPc4	uživatel
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mu	on	k3xPp3gMnSc3	on
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
provádět	provádět	k5eAaImF	provádět
nějakou	nějaký	k3yIgFnSc4	nějaký
užitečnou	užitečný	k2eAgFnSc4d1	užitečná
činnost	činnost	k1gFnSc4	činnost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
LibreOffice	LibreOffice	k1gFnSc1	LibreOffice
<g/>
,	,	kIx,	,
Mozilla	Mozilla	k1gFnSc1	Mozilla
Firefox	Firefox	k1gInSc1	Firefox
<g/>
,	,	kIx,	,
Pidgin	Pidgin	k1gInSc1	Pidgin
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
běžně	běžně	k6eAd1	běžně
používané	používaný	k2eAgInPc1d1	používaný
nástroje	nástroj	k1gInPc1	nástroj
i	i	k8xC	i
aplikace	aplikace	k1gFnPc1	aplikace
jsou	být	k5eAaImIp3nP	být
volně	volně	k6eAd1	volně
dostupné	dostupný	k2eAgFnPc1d1	dostupná
na	na	k7c6	na
Internetu	Internet	k1gInSc6	Internet
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
výše	vysoce	k6eAd2	vysoce
zmíněné	zmíněný	k2eAgInPc1d1	zmíněný
nástroje	nástroj	k1gInPc1	nástroj
i	i	k8xC	i
aplikace	aplikace	k1gFnPc1	aplikace
na	na	k7c6	na
Internetu	Internet	k1gInSc6	Internet
dostupné	dostupný	k2eAgFnSc2d1	dostupná
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
zdrojových	zdrojový	k2eAgInPc2d1	zdrojový
kódů	kód	k1gInPc2	kód
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
je	být	k5eAaImIp3nS	být
nejprve	nejprve	k6eAd1	nejprve
nutné	nutný	k2eAgNnSc1d1	nutné
přeložit	přeložit	k5eAaPmF	přeložit
do	do	k7c2	do
formy	forma	k1gFnSc2	forma
spustitelných	spustitelný	k2eAgInPc2d1	spustitelný
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nS	by
pro	pro	k7c4	pro
uživatele	uživatel	k1gMnSc4	uživatel
velmi	velmi	k6eAd1	velmi
nepohodlné	pohodlný	k2eNgNnSc1d1	nepohodlné
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
si	se	k3xPyFc3	se
vše	všechen	k3xTgNnSc4	všechen
musel	muset	k5eAaImAgInS	muset
dělat	dělat	k5eAaImF	dělat
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
existují	existovat	k5eAaImIp3nP	existovat
takzvané	takzvaný	k2eAgFnPc1d1	takzvaná
distribuce	distribuce	k1gFnPc1	distribuce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
vše	všechen	k3xTgNnSc4	všechen
potřebné	potřebné	k1gNnSc4	potřebné
v	v	k7c6	v
úhledném	úhledný	k2eAgNnSc6d1	úhledné
balení	balení	k1gNnSc6	balení
–	–	k?	–
přeložené	přeložený	k2eAgInPc4d1	přeložený
binární	binární	k2eAgInPc4d1	binární
soubory	soubor	k1gInPc4	soubor
včetně	včetně	k7c2	včetně
instalačního	instalační	k2eAgInSc2d1	instalační
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
připravit	připravit	k5eAaPmF	připravit
Linux	linux	k1gInSc4	linux
na	na	k7c6	na
uživatelově	uživatelův	k2eAgInSc6d1	uživatelův
počítači	počítač	k1gInSc6	počítač
k	k	k7c3	k
okamžitému	okamžitý	k2eAgNnSc3d1	okamžité
používání	používání	k1gNnSc3	používání
(	(	kIx(	(
<g/>
avšak	avšak	k8xC	avšak
Linux	linux	k1gInSc4	linux
lze	lze	k6eAd1	lze
používat	používat	k5eAaImF	používat
i	i	k9	i
bez	bez	k7c2	bez
instalace	instalace	k1gFnSc2	instalace
pomocí	pomocí	k7c2	pomocí
tzv.	tzv.	kA	tzv.
Live	Liv	k1gFnSc2	Liv
CD	CD	kA	CD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Distribuce	distribuce	k1gFnPc1	distribuce
jsou	být	k5eAaImIp3nP	být
sestavovány	sestavován	k2eAgFnPc1d1	sestavována
jednotlivci	jednotlivec	k1gMnPc1	jednotlivec
<g/>
,	,	kIx,	,
týmy	tým	k1gInPc1	tým
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
komerčními	komerční	k2eAgFnPc7d1	komerční
firmami	firma	k1gFnPc7	firma
<g/>
.	.	kIx.	.
</s>
<s>
Distribuce	distribuce	k1gFnSc1	distribuce
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
jádro	jádro	k1gNnSc4	jádro
<g/>
,	,	kIx,	,
další	další	k2eAgNnSc4d1	další
systémový	systémový	k2eAgInSc4d1	systémový
a	a	k8xC	a
aplikační	aplikační	k2eAgInSc4d1	aplikační
software	software	k1gInSc4	software
<g/>
,	,	kIx,	,
grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
(	(	kIx(	(
<g/>
X.	X.	kA	X.
<g/>
org	org	k?	org
<g/>
,	,	kIx,	,
KDE	kde	k6eAd1	kde
<g/>
,	,	kIx,	,
GNOME	GNOME	kA	GNOME
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Distribuce	distribuce	k1gFnPc1	distribuce
mají	mít	k5eAaImIp3nP	mít
různá	různý	k2eAgNnPc4d1	různé
zaměření	zaměření	k1gNnPc4	zaměření
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
výběr	výběr	k1gInSc1	výběr
obsažených	obsažený	k2eAgInPc2d1	obsažený
programů	program	k1gInPc2	program
<g/>
,	,	kIx,	,
podpora	podpora	k1gFnSc1	podpora
určité	určitý	k2eAgFnSc2d1	určitá
počítačové	počítačový	k2eAgFnSc2d1	počítačová
architektury	architektura	k1gFnSc2	architektura
<g/>
,	,	kIx,	,
použití	použití	k1gNnSc1	použití
ve	v	k7c6	v
vestavěných	vestavěný	k2eAgInPc6d1	vestavěný
systémech	systém	k1gInPc6	systém
atd.	atd.	kA	atd.
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
existuje	existovat	k5eAaImIp3nS	existovat
kolem	kolem	k7c2	kolem
450	[number]	k4	450
různých	různý	k2eAgFnPc2d1	různá
distribucí	distribuce	k1gFnPc2	distribuce
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc1	snímek
obrazovek	obrazovka	k1gFnPc2	obrazovka
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
popisy	popis	k1gInPc1	popis
distribucí	distribuce	k1gFnPc2	distribuce
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
Internetu	Internet	k1gInSc2	Internet
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
na	na	k7c6	na
serveru	server	k1gInSc6	server
DistroWatch	DistroWatch	k1gInSc1	DistroWatch
nebo	nebo	k8xC	nebo
i	i	k9	i
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgFnPc4d3	nejznámější
distribuce	distribuce	k1gFnPc4	distribuce
Linuxu	linux	k1gInSc2	linux
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Arch	arch	k1gInSc1	arch
Linux	linux	k1gInSc1	linux
<g/>
,	,	kIx,	,
Danix	Danix	k1gInSc1	Danix
(	(	kIx(	(
<g/>
česká	český	k2eAgFnSc1d1	Česká
distribuce	distribuce	k1gFnSc1	distribuce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Debian	Debian	k1gInSc1	Debian
<g/>
,	,	kIx,	,
Fedora	Fedora	k1gFnSc1	Fedora
(	(	kIx(	(
<g/>
nástupce	nástupce	k1gMnSc1	nástupce
Red	Red	k1gMnSc1	Red
Hat	hat	k0	hat
Linuxu	linux	k1gInSc6	linux
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Red	Red	k1gMnSc1	Red
Hat	hat	k0	hat
Enterprise	Enterprise	k1gFnPc4	Enterprise
Linux	linux	k1gInSc1	linux
(	(	kIx(	(
<g/>
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
Fedory	Fedora	k1gFnSc2	Fedora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gentoo	Gentoo	k1gMnSc1	Gentoo
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Greenie	Greenie	k1gFnSc1	Greenie
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
slovenská	slovenský	k2eAgFnSc1d1	slovenská
distribuce	distribuce	k1gFnSc1	distribuce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Knoppix	Knoppix	k1gInSc1	Knoppix
<g/>
,	,	kIx,	,
Mandriva	Mandriva	k1gFnSc1	Mandriva
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Mandrake	Mandrak	k1gMnSc2	Mandrak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Linux	linux	k1gInSc1	linux
Mint	Mint	k1gInSc1	Mint
(	(	kIx(	(
<g/>
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
Ubuntu	Ubunt	k1gInSc2	Ubunt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Slackware	Slackwar	k1gMnSc5	Slackwar
<g/>
,	,	kIx,	,
Slax	Slax	k1gInSc1	Slax
(	(	kIx(	(
<g/>
česká	český	k2eAgFnSc1d1	Česká
live	live	k1gFnSc1	live
distribuce	distribuce	k1gFnSc2	distribuce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Source	Source	k1gMnSc1	Source
Mage	Mag	k1gFnSc2	Mag
<g/>
,	,	kIx,	,
SUSE	SUSE	kA	SUSE
<g/>
,	,	kIx,	,
Ubuntu	Ubunta	k1gFnSc4	Ubunta
<g/>
,	,	kIx,	,
Kubuntu	Kubunta	k1gFnSc4	Kubunta
(	(	kIx(	(
<g/>
derivát	derivát	k1gInSc1	derivát
Ubuntu	Ubunt	k1gInSc2	Ubunt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
...	...	k?	...
Distribuce	distribuce	k1gFnSc1	distribuce
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
na	na	k7c6	na
Internetu	Internet	k1gInSc6	Internet
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
i	i	k9	i
volně	volně	k6eAd1	volně
používat	používat	k5eAaImF	používat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
open	openo	k1gNnPc2	openo
source	sourec	k1gInSc2	sourec
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
distribuci	distribuce	k1gFnSc6	distribuce
je	být	k5eAaImIp3nS	být
ke	k	k7c3	k
každému	každý	k3xTgInSc3	každý
programu	program	k1gInSc3	program
standardně	standardně	k6eAd1	standardně
přiložena	přiložen	k2eAgFnSc1d1	přiložena
licence	licence	k1gFnSc1	licence
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
instalaci	instalace	k1gFnSc6	instalace
uložena	uložit	k5eAaPmNgFnS	uložit
společně	společně	k6eAd1	společně
s	s	k7c7	s
programem	program	k1gInSc7	program
na	na	k7c4	na
pevný	pevný	k2eAgInSc4d1	pevný
disk	disk	k1gInSc4	disk
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
si	se	k3xPyFc3	se
vše	všechen	k3xTgNnSc4	všechen
uživatel	uživatel	k1gMnSc1	uživatel
může	moct	k5eAaImIp3nS	moct
ověřit	ověřit	k5eAaPmF	ověřit
(	(	kIx(	(
<g/>
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
open	opena	k1gFnPc2	opena
source	sourec	k1gInSc2	sourec
programy	program	k1gInPc7	program
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
s	s	k7c7	s
nimi	on	k3xPp3gNnPc7	on
licence	licence	k1gFnSc2	licence
byla	být	k5eAaImAgFnS	být
dodávána	dodávat	k5eAaImNgFnS	dodávat
nejen	nejen	k6eAd1	nejen
u	u	k7c2	u
zdrojových	zdrojový	k2eAgInPc2d1	zdrojový
kódů	kód	k1gInPc2	kód
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
u	u	k7c2	u
funkční	funkční	k2eAgFnSc2d1	funkční
podoby	podoba	k1gFnSc2	podoba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
vytvořené	vytvořený	k2eAgFnSc2d1	vytvořená
distribuce	distribuce	k1gFnSc2	distribuce
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
vše	všechen	k3xTgNnSc1	všechen
na	na	k7c6	na
distribučním	distribuční	k2eAgNnSc6d1	distribuční
médiu	médium	k1gNnSc6	médium
organizováno	organizovat	k5eAaBmNgNnS	organizovat
<g/>
)	)	kIx)	)
také	také	k6eAd1	také
podléhá	podléhat	k5eAaImIp3nS	podléhat
licenci	licence	k1gFnSc4	licence
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
typicky	typicky	k6eAd1	typicky
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
open	open	k1gMnSc1	open
source	source	k1gMnSc1	source
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
linuxových	linuxový	k2eAgFnPc2d1	linuxová
distribucí	distribuce	k1gFnPc2	distribuce
je	být	k5eAaImIp3nS	být
sestavována	sestavovat	k5eAaImNgFnS	sestavovat
výhradně	výhradně	k6eAd1	výhradně
ze	z	k7c2	z
svobodného	svobodný	k2eAgNnSc2d1	svobodné
software	software	k1gInSc1	software
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
nejen	nejen	k6eAd1	nejen
volně	volně	k6eAd1	volně
používat	používat	k5eAaImF	používat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
dále	daleko	k6eAd2	daleko
šířit	šířit	k5eAaImF	šířit
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
nesvobodný	svobodný	k2eNgInSc4d1	nesvobodný
software	software	k1gInSc4	software
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
volně	volně	k6eAd1	volně
používat	používat	k5eAaImF	používat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
omezeno	omezen	k2eAgNnSc1d1	omezeno
jejich	jejich	k3xOp3gNnSc4	jejich
šíření	šíření	k1gNnSc4	šíření
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
komerční	komerční	k2eAgInPc1d1	komerční
programy	program	k1gInPc1	program
(	(	kIx(	(
<g/>
ovladače	ovladač	k1gInPc1	ovladač
pro	pro	k7c4	pro
grafickou	grafický	k2eAgFnSc4d1	grafická
kartu	karta	k1gFnSc4	karta
<g/>
,	,	kIx,	,
počítačové	počítačový	k2eAgFnPc4d1	počítačová
hry	hra	k1gFnPc4	hra
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k9	též
profesionální	profesionální	k2eAgFnPc1d1	profesionální
komerční	komerční	k2eAgFnPc1d1	komerční
distribuce	distribuce	k1gFnPc1	distribuce
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
dostupné	dostupný	k2eAgInPc1d1	dostupný
pouze	pouze	k6eAd1	pouze
po	po	k7c6	po
zaplacení	zaplacení	k1gNnSc6	zaplacení
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Red	Red	k1gFnSc1	Red
Hat	hat	k0	hat
Enterprise	Enterprise	k1gFnPc4	Enterprise
Linux	Linux	kA	Linux
<g/>
,	,	kIx,	,
SUSE	SUSE	kA	SUSE
Linux	Linux	kA	Linux
Enterprise	Enterprise	k1gFnSc1	Enterprise
Server	server	k1gInSc1	server
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
díky	díky	k7c3	díky
povaze	povaha	k1gFnSc3	povaha
open	open	k1gMnSc1	open
source	source	k1gMnSc1	source
k	k	k7c3	k
některým	některý	k3yIgInPc3	některý
existují	existovat	k5eAaImIp3nP	existovat
volně	volně	k6eAd1	volně
šiřitelné	šiřitelný	k2eAgInPc1d1	šiřitelný
identické	identický	k2eAgInPc1d1	identický
klony	klon	k1gInPc1	klon
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
CentOS	CentOS	k1gFnSc6	CentOS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
výhodou	výhoda	k1gFnSc7	výhoda
linuxových	linuxový	k2eAgFnPc2d1	linuxová
distribucí	distribuce	k1gFnPc2	distribuce
je	být	k5eAaImIp3nS	být
existence	existence	k1gFnSc1	existence
repozitářů	repozitář	k1gInPc2	repozitář
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgInP	založit
na	na	k7c6	na
balíčkovacích	balíčkovací	k2eAgInPc6d1	balíčkovací
systémech	systém	k1gInPc6	systém
a	a	k8xC	a
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
snadno	snadno	k6eAd1	snadno
instalovatelné	instalovatelný	k2eAgInPc1d1	instalovatelný
balíčky	balíček	k1gInPc1	balíček
s	s	k7c7	s
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
programy	program	k1gInPc7	program
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
lze	lze	k6eAd1	lze
v	v	k7c6	v
linuxových	linuxový	k2eAgFnPc6d1	linuxová
distribucích	distribuce	k1gFnPc6	distribuce
velmi	velmi	k6eAd1	velmi
pohodlně	pohodlně	k6eAd1	pohodlně
instalovat	instalovat	k5eAaBmF	instalovat
a	a	k8xC	a
odebírat	odebírat	k5eAaImF	odebírat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
součásti	součást	k1gFnPc4	součást
systému	systém	k1gInSc2	systém
a	a	k8xC	a
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
repozitářů	repozitář	k1gInPc2	repozitář
jsou	být	k5eAaImIp3nP	být
umisťovány	umisťován	k2eAgFnPc1d1	umisťován
též	též	k9	též
aktualizace	aktualizace	k1gFnPc1	aktualizace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
zajistit	zajistit	k5eAaPmF	zajistit
nejen	nejen	k6eAd1	nejen
automatické	automatický	k2eAgFnPc4d1	automatická
opravy	oprava	k1gFnPc4	oprava
chyb	chyba	k1gFnPc2	chyba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
také	také	k9	také
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
systému	systém	k1gInSc2	systém
odstraňováním	odstraňování	k1gNnSc7	odstraňování
zjištěných	zjištěný	k2eAgFnPc2d1	zjištěná
zranitelností	zranitelnost	k1gFnPc2	zranitelnost
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
samotný	samotný	k2eAgInSc4d1	samotný
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
ostatní	ostatní	k2eAgFnPc4d1	ostatní
součásti	součást	k1gFnPc4	součást
příslušné	příslušný	k2eAgFnSc2d1	příslušná
distribuce	distribuce	k1gFnSc2	distribuce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
působení	působení	k1gNnSc4	působení
Linux	Linux	kA	Linux
získal	získat	k5eAaPmAgInS	získat
mnoho	mnoho	k4c4	mnoho
příznivců	příznivec	k1gMnPc2	příznivec
a	a	k8xC	a
významné	významný	k2eAgNnSc1d1	významné
místo	místo	k1gNnSc1	místo
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
je	být	k5eAaImIp3nS	být
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
zejména	zejména	k9	zejména
na	na	k7c6	na
internetových	internetový	k2eAgInPc6d1	internetový
a	a	k8xC	a
intranetových	intranetový	k2eAgInPc6d1	intranetový
serverech	server	k1gInPc6	server
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vysoce	vysoce	k6eAd1	vysoce
výkonných	výkonný	k2eAgFnPc2d1	výkonná
výpočetních	výpočetní	k2eAgFnPc2d1	výpočetní
stanic	stanice	k1gFnPc2	stanice
(	(	kIx(	(
<g/>
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
500	[number]	k4	500
nejvýkonnějších	výkonný	k2eAgInPc2d3	nejvýkonnější
superpočítačů	superpočítač	k1gInPc2	superpočítač
<g/>
/	/	kIx~	/
<g/>
TOP	topit	k5eAaImRp2nS	topit
<g/>
500	[number]	k4	500
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
podíl	podíl	k1gInSc4	podíl
91,8	[number]	k4	91,8
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Linux	linux	k1gInSc1	linux
je	být	k5eAaImIp3nS	být
nasazen	nasadit	k5eAaPmNgInS	nasadit
i	i	k9	i
na	na	k7c6	na
současném	současný	k2eAgInSc6d1	současný
nejvýkonnějším	výkonný	k2eAgInSc6d3	nejvýkonnější
superpočítači	superpočítač	k1gInSc6	superpočítač
"	"	kIx"	"
<g/>
Tianhe-	Tianhe-	k1gFnSc1	Tianhe-
<g/>
1	[number]	k4	1
<g/>
A	A	kA	A
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Linux	linux	k1gInSc1	linux
pozvolna	pozvolna	k6eAd1	pozvolna
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
i	i	k9	i
do	do	k7c2	do
firemní	firemní	k2eAgFnSc2d1	firemní
sféry	sféra	k1gFnSc2	sféra
a	a	k8xC	a
na	na	k7c4	na
domácí	domácí	k2eAgInPc4d1	domácí
počítače	počítač	k1gInPc4	počítač
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jeho	jeho	k3xOp3gInSc1	jeho
podíl	podíl	k1gInSc1	podíl
na	na	k7c4	na
PC	PC	kA	PC
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
tři	tři	k4xCgNnPc4	tři
procenta	procento	k1gNnPc4	procento
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
zvládá	zvládat	k5eAaImIp3nS	zvládat
prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgFnPc4	všechen
činnosti	činnost	k1gFnPc4	činnost
od	od	k7c2	od
počítače	počítač	k1gInSc2	počítač
očekávané	očekávaný	k2eAgNnSc1d1	očekávané
a	a	k8xC	a
mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gFnPc4	jeho
přednosti	přednost	k1gFnPc4	přednost
patří	patřit	k5eAaImIp3nS	patřit
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
<g/>
,	,	kIx,	,
nízká	nízký	k2eAgFnSc1d1	nízká
cena	cena	k1gFnSc1	cena
a	a	k8xC	a
flexibilita	flexibilita	k1gFnSc1	flexibilita
<g/>
,	,	kIx,	,
rozšíření	rozšíření	k1gNnSc1	rozšíření
stále	stále	k6eAd1	stále
brání	bránit	k5eAaImIp3nS	bránit
zejména	zejména	k9	zejména
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
aplikací	aplikace	k1gFnPc2	aplikace
dostupných	dostupný	k2eAgFnPc2d1	dostupná
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
počítačových	počítačový	k2eAgFnPc2d1	počítačová
her	hra	k1gFnPc2	hra
<g/>
)	)	kIx)	)
a	a	k8xC	a
nejistá	jistý	k2eNgFnSc1d1	nejistá
podpora	podpora	k1gFnSc1	podpora
spuštění	spuštění	k1gNnSc2	spuštění
těchto	tento	k3xDgFnPc2	tento
konkrétních	konkrétní	k2eAgFnPc2d1	konkrétní
aplikací	aplikace	k1gFnPc2	aplikace
pod	pod	k7c7	pod
Linuxem	linux	k1gInSc7	linux
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
také	také	k9	také
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
Linux	linux	k1gInSc1	linux
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
i	i	k9	i
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
chytrých	chytrý	k2eAgInPc2d1	chytrý
telefonů	telefon	k1gInPc2	telefon
a	a	k8xC	a
tabletů	tablet	k1gInPc2	tablet
<g/>
.	.	kIx.	.
</s>
<s>
Instalace	instalace	k1gFnSc1	instalace
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
podle	podle	k7c2	podle
zvolené	zvolený	k2eAgFnSc2d1	zvolená
linuxové	linuxový	k2eAgFnSc2d1	linuxová
distribuce	distribuce	k1gFnSc2	distribuce
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
distribucí	distribuce	k1gFnSc7	distribuce
nabízí	nabízet	k5eAaImIp3nS	nabízet
textovou	textový	k2eAgFnSc4d1	textová
i	i	k8xC	i
grafickou	grafický	k2eAgFnSc4d1	grafická
verzi	verze	k1gFnSc4	verze
instalace	instalace	k1gFnSc2	instalace
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
obvykle	obvykle	k6eAd1	obvykle
zvládne	zvládnout	k5eAaPmIp3nS	zvládnout
i	i	k9	i
začátečník	začátečník	k1gMnSc1	začátečník
─	─	k?	─
mnozí	mnohý	k2eAgMnPc1d1	mnohý
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
instalace	instalace	k1gFnSc1	instalace
některých	některý	k3yIgFnPc2	některý
distribucí	distribuce	k1gFnPc2	distribuce
Linuxu	linux	k1gInSc2	linux
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
snadnější	snadný	k2eAgMnSc1d2	snazší
než	než	k8xS	než
u	u	k7c2	u
konkurenčních	konkurenční	k2eAgInPc2d1	konkurenční
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
instalaci	instalace	k1gFnSc6	instalace
se	se	k3xPyFc4	se
také	také	k9	také
obvykle	obvykle	k6eAd1	obvykle
nainstaluje	nainstalovat	k5eAaPmIp3nS	nainstalovat
nejen	nejen	k6eAd1	nejen
samotný	samotný	k2eAgInSc1d1	samotný
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
veškerý	veškerý	k3xTgInSc4	veškerý
software	software	k1gInSc4	software
potřebný	potřebný	k2eAgInSc4d1	potřebný
k	k	k7c3	k
používání	používání	k1gNnSc3	používání
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Instalovat	instalovat	k5eAaBmF	instalovat
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
instalačního	instalační	k2eAgNnSc2d1	instalační
média	médium	k1gNnSc2	médium
(	(	kIx(	(
<g/>
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
nainstalují	nainstalovat	k5eAaPmIp3nP	nainstalovat
aplikace	aplikace	k1gFnPc1	aplikace
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vydání	vydání	k1gNnSc2	vydání
distribuce	distribuce	k1gFnSc2	distribuce
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
lze	lze	k6eAd1	lze
z	z	k7c2	z
instalačního	instalační	k2eAgNnSc2d1	instalační
média	médium	k1gNnSc2	médium
pouze	pouze	k6eAd1	pouze
nabootovat	nabootovat	k5eAaPmF	nabootovat
a	a	k8xC	a
stáhnout	stáhnout	k5eAaPmF	stáhnout
aktuální	aktuální	k2eAgFnSc4d1	aktuální
verzi	verze	k1gFnSc4	verze
distribuce	distribuce	k1gFnSc2	distribuce
ze	z	k7c2	z
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
distribuce	distribuce	k1gFnSc1	distribuce
lze	lze	k6eAd1	lze
také	také	k9	také
instalovat	instalovat	k5eAaBmF	instalovat
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
běžícího	běžící	k2eAgInSc2d1	běžící
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
jiné	jiný	k2eAgFnSc2d1	jiná
distribuce	distribuce	k1gFnSc2	distribuce
Linuxu	linux	k1gInSc2	linux
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
spíš	spíš	k9	spíš
zajímavost	zajímavost	k1gFnSc4	zajímavost
pro	pro	k7c4	pro
odborníky	odborník	k1gMnPc4	odborník
než	než	k8xS	než
doporučená	doporučený	k2eAgFnSc1d1	Doporučená
metoda	metoda	k1gFnSc1	metoda
pro	pro	k7c4	pro
začátečníka	začátečník	k1gMnSc4	začátečník
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
běh	běh	k1gInSc4	běh
aplikací	aplikace	k1gFnPc2	aplikace
z	z	k7c2	z
MS	MS	kA	MS
Windows	Windows	kA	Windows
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
řada	řada	k1gFnSc1	řada
emulátorů	emulátor	k1gInPc2	emulátor
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
jsou	být	k5eAaImIp3nP	být
založené	založený	k2eAgInPc1d1	založený
na	na	k7c4	na
vytváření	vytváření	k1gNnSc4	vytváření
plnohodnotných	plnohodnotný	k2eAgInPc2d1	plnohodnotný
virtuálních	virtuální	k2eAgInPc2d1	virtuální
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiná	k1gFnPc1	jiná
pouze	pouze	k6eAd1	pouze
překládají	překládat	k5eAaImIp3nP	překládat
systémová	systémový	k2eAgNnPc1d1	systémové
volání	volání	k1gNnPc1	volání
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jednak	jednak	k8xC	jednak
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
mnohem	mnohem	k6eAd1	mnohem
efektivnějšímu	efektivní	k2eAgInSc3d2	efektivnější
běhu	běh	k1gInSc3	běh
spouštěných	spouštěný	k2eAgFnPc2d1	spouštěná
aplikací	aplikace	k1gFnPc2	aplikace
a	a	k8xC	a
jednak	jednak	k8xC	jednak
k	k	k7c3	k
efektivnějšímu	efektivní	k2eAgNnSc3d2	efektivnější
využití	využití	k1gNnSc3	využití
stávajícího	stávající	k2eAgInSc2d1	stávající
hardwaru	hardware	k1gInSc2	hardware
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
podpora	podpora	k1gFnSc1	podpora
3D	[number]	k4	3D
akcelerace	akcelerace	k1gFnSc1	akcelerace
grafických	grafický	k2eAgFnPc2d1	grafická
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
možnost	možnost	k1gFnSc4	možnost
použití	použití	k1gNnSc3	použití
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
architektuře	architektura	k1gFnSc6	architektura
x	x	k?	x
<g/>
86	[number]	k4	86
a	a	k8xC	a
kompatibilních	kompatibilní	k2eAgFnPc6d1	kompatibilní
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
emulátory	emulátor	k1gInPc4	emulátor
zdarma	zdarma	k6eAd1	zdarma
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejznámější	známý	k2eAgInPc4d3	nejznámější
je	on	k3xPp3gInPc4	on
Wine	Win	k1gInPc4	Win
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
pomocí	pomocí	k7c2	pomocí
Wine	Win	k1gInSc2	Win
funguje	fungovat	k5eAaImIp3nS	fungovat
pod	pod	k7c7	pod
Linuxem	linux	k1gInSc7	linux
většina	většina	k1gFnSc1	většina
Windows	Windows	kA	Windows
aplikací	aplikace	k1gFnSc7	aplikace
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
existuje	existovat	k5eAaImIp3nS	existovat
alternativa	alternativa	k1gFnSc1	alternativa
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
i	i	k9	i
komerční	komerční	k2eAgFnSc1d1	komerční
odnož	odnož	k1gFnSc1	odnož
Cedega	Cedega	k1gFnSc1	Cedega
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
možnost	možnost	k1gFnSc4	možnost
hraní	hraň	k1gFnPc2	hraň
her	hra	k1gFnPc2	hra
napsaných	napsaný	k2eAgInPc2d1	napsaný
pro	pro	k7c4	pro
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
CrossOver	CrossOver	k1gInSc1	CrossOver
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
emulátory	emulátor	k1gInPc4	emulátor
virtuálního	virtuální	k2eAgNnSc2d1	virtuální
PC	PC	kA	PC
<g/>
:	:	kIx,	:
Bochs	Bochs	k1gInSc1	Bochs
<g/>
,	,	kIx,	,
QEMU	QEMU	kA	QEMU
<g/>
,	,	kIx,	,
VirtualBox	VirtualBox	k1gInSc1	VirtualBox
(	(	kIx(	(
<g/>
GPL	GPL	kA	GPL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
VMWare	VMWar	k1gMnSc5	VMWar
(	(	kIx(	(
<g/>
proprietární	proprietární	k2eAgFnSc7d1	proprietární
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
programovacím	programovací	k2eAgInSc7d1	programovací
jazykem	jazyk	k1gInSc7	jazyk
v	v	k7c6	v
Linuxu	linux	k1gInSc6	linux
je	být	k5eAaImIp3nS	být
jazyk	jazyk	k1gInSc1	jazyk
C	C	kA	C
a	a	k8xC	a
sada	sada	k1gFnSc1	sada
GCC	GCC	kA	GCC
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
překladače	překladač	k1gMnSc4	překladač
pro	pro	k7c4	pro
několik	několik	k4yIc4	několik
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
C	C	kA	C
a	a	k8xC	a
C	C	kA	C
<g/>
++	++	k?	++
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
programovacích	programovací	k2eAgInPc2d1	programovací
nástrojů	nástroj	k1gInPc2	nástroj
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
GNU	gnu	k1gNnSc4	gnu
binutils	binutilsa	k1gFnPc2	binutilsa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
nástroje	nástroj	k1gInPc4	nástroj
pro	pro	k7c4	pro
překlad	překlad	k1gInSc4	překlad
jazyka	jazyk	k1gInSc2	jazyk
symbolických	symbolický	k2eAgFnPc2d1	symbolická
adres	adresa	k1gFnPc2	adresa
a	a	k8xC	a
linkování	linkování	k1gNnSc2	linkování
binárních	binární	k2eAgInPc2d1	binární
objektových	objektový	k2eAgInPc2d1	objektový
souborů	soubor	k1gInPc2	soubor
do	do	k7c2	do
spustitelné	spustitelný	k2eAgFnSc2d1	spustitelná
podoby	podoba	k1gFnSc2	podoba
<g/>
;	;	kIx,	;
na	na	k7c6	na
systému	systém	k1gInSc6	systém
Linux	Linux	kA	Linux
jsou	být	k5eAaImIp3nP	být
standardně	standardně	k6eAd1	standardně
objektové	objektový	k2eAgInPc1d1	objektový
soubory	soubor	k1gInPc1	soubor
i	i	k8xC	i
spustitelné	spustitelný	k2eAgInPc1d1	spustitelný
programy	program	k1gInPc1	program
uloženy	uložen	k2eAgInPc1d1	uložen
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
ELF	elf	k1gMnSc1	elf
(	(	kIx(	(
<g/>
executable	executable	k6eAd1	executable
and	and	k?	and
linkable	linkable	k6eAd1	linkable
format	format	k5eAaPmF	format
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prostředí	prostředí	k1gNnSc1	prostředí
GNU	gnu	k1gNnSc2	gnu
nabízí	nabízet	k5eAaImIp3nS	nabízet
i	i	k9	i
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgInPc2d1	další
nástrojů	nástroj	k1gInPc2	nástroj
pro	pro	k7c4	pro
usnadnění	usnadnění	k1gNnSc4	usnadnění
vývoje	vývoj	k1gInSc2	vývoj
složitějších	složitý	k2eAgInPc2d2	složitější
programů	program	k1gInPc2	program
(	(	kIx(	(
<g/>
make	make	k1gInSc1	make
<g/>
,	,	kIx,	,
autoconf	autoconf	k1gInSc1	autoconf
<g/>
,	,	kIx,	,
gettext	gettext	k1gInSc1	gettext
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Linux	linux	k1gInSc1	linux
podporuje	podporovat	k5eAaImIp3nS	podporovat
i	i	k9	i
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgInPc2d1	další
programovacích	programovací	k2eAgInPc2d1	programovací
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
jednoduchého	jednoduchý	k2eAgInSc2d1	jednoduchý
jazyka	jazyk	k1gInSc2	jazyk
zabudovaného	zabudovaný	k2eAgInSc2d1	zabudovaný
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
příkazové	příkazový	k2eAgFnSc6d1	příkazová
řádce	řádka	k1gFnSc6	řádka
(	(	kIx(	(
<g/>
shell	shell	k1gInSc1	shell
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
nejpoužívanějšími	používaný	k2eAgMnPc7d3	nejpoužívanější
jazyky	jazyk	k1gMnPc7	jazyk
v	v	k7c6	v
linuxovém	linuxový	k2eAgNnSc6d1	linuxové
prostředí	prostředí	k1gNnSc6	prostředí
Perl	perl	k1gInSc4	perl
a	a	k8xC	a
Python	Python	k1gMnSc1	Python
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
Linux	linux	k1gInSc1	linux
stal	stát	k5eAaPmAgInS	stát
velice	velice	k6eAd1	velice
populární	populární	k2eAgFnSc7d1	populární
platformou	platforma	k1gFnSc7	platforma
pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
WWW	WWW	kA	WWW
serverů	server	k1gInPc2	server
<g/>
,	,	kIx,	,
tak	tak	k9	tak
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
uživatelských	uživatelský	k2eAgFnPc2d1	Uživatelská
aplikací	aplikace	k1gFnPc2	aplikace
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
běžně	běžně	k6eAd1	běžně
provozují	provozovat	k5eAaImIp3nP	provozovat
pod	pod	k7c7	pod
tímto	tento	k3xDgInSc7	tento
systémem	systém	k1gInSc7	systém
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
webové	webový	k2eAgFnPc4d1	webová
aplikace	aplikace	k1gFnPc4	aplikace
napsané	napsaný	k2eAgFnPc4d1	napsaná
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
PHP	PHP	kA	PHP
<g/>
.	.	kIx.	.
</s>
<s>
Programování	programování	k1gNnSc1	programování
v	v	k7c6	v
Linuxu	linux	k1gInSc6	linux
většinou	většinou	k6eAd1	většinou
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
cyklu	cyklus	k1gInSc6	cyklus
<g/>
:	:	kIx,	:
programátor	programátor	k1gInSc1	programátor
napíše	napsat	k5eAaPmIp3nS	napsat
zdrojový	zdrojový	k2eAgInSc1d1	zdrojový
kód	kód	k1gInSc1	kód
v	v	k7c6	v
textovém	textový	k2eAgInSc6d1	textový
editoru	editor	k1gInSc6	editor
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
spustí	spustit	k5eAaPmIp3nS	spustit
v	v	k7c6	v
příkazové	příkazový	k2eAgFnSc6d1	příkazová
řádce	řádka	k1gFnSc6	řádka
kompilátor	kompilátor	k1gInSc1	kompilátor
a	a	k8xC	a
program	program	k1gInSc1	program
otestuje	otestovat	k5eAaPmIp3nS	otestovat
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
programátorské	programátorský	k2eAgInPc1d1	programátorský
editory	editor	k1gInPc1	editor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
za	za	k7c4	za
programátora	programátor	k1gMnSc4	programátor
spustí	spustit	k5eAaPmIp3nS	spustit
kompilátor	kompilátor	k1gInSc1	kompilátor
a	a	k8xC	a
případně	případně	k6eAd1	případně
ve	v	k7c6	v
zdrojovém	zdrojový	k2eAgInSc6d1	zdrojový
textu	text	k1gInSc6	text
označí	označit	k5eAaPmIp3nS	označit
chyby	chyba	k1gFnPc4	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmostí	samozřejmost	k1gFnSc7	samozřejmost
je	být	k5eAaImIp3nS	být
i	i	k9	i
zvýraznění	zvýraznění	k1gNnSc4	zvýraznění
syntaxe	syntax	k1gFnSc2	syntax
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
již	již	k6eAd1	již
i	i	k8xC	i
rozvinutá	rozvinutý	k2eAgNnPc1d1	rozvinuté
plně	plně	k6eAd1	plně
funkční	funkční	k2eAgNnPc1d1	funkční
vývojová	vývojový	k2eAgNnPc1d1	vývojové
prostředí	prostředí	k1gNnPc1	prostředí
označovaná	označovaný	k2eAgNnPc1d1	označované
jako	jako	k8xC	jako
IDE	IDE	kA	IDE
nebo	nebo	k8xC	nebo
v	v	k7c6	v
případě	případ	k1gInSc6	případ
návrhu	návrh	k1gInSc2	návrh
grafického	grafický	k2eAgNnSc2d1	grafické
uživatelského	uživatelský	k2eAgNnSc2d1	Uživatelské
rozhraní	rozhraní	k1gNnSc2	rozhraní
označovaná	označovaný	k2eAgFnSc1d1	označovaná
RAD	rad	k1gInSc1	rad
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
prostředí	prostředí	k1gNnPc1	prostředí
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
grafické	grafický	k2eAgNnSc4d1	grafické
rozhraní	rozhraní	k1gNnSc4	rozhraní
X	X	kA	X
Window	Window	k1gMnSc7	Window
System	Syst	k1gMnSc7	Syst
<g/>
.	.	kIx.	.
</s>
<s>
Vesměs	vesměs	k6eAd1	vesměs
jsou	být	k5eAaImIp3nP	být
zaměřená	zaměřený	k2eAgNnPc1d1	zaměřené
na	na	k7c4	na
kompilované	kompilovaný	k2eAgInPc4d1	kompilovaný
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
ale	ale	k8xC	ale
i	i	k9	i
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
podporují	podporovat	k5eAaImIp3nP	podporovat
ladění	ladění	k1gNnSc4	ladění
skriptovacích	skriptovací	k2eAgInPc2d1	skriptovací
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Pythonu	Python	k1gMnSc6	Python
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
IDEA	idea	k1gFnSc1	idea
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
programování	programování	k1gNnSc3	programování
grafických	grafický	k2eAgFnPc2d1	grafická
aplikací	aplikace	k1gFnPc2	aplikace
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
např.	např.	kA	např.
Anjuta	Anjut	k1gMnSc2	Anjut
<g/>
,	,	kIx,	,
Glade	Glad	k1gInSc5	Glad
či	či	k8xC	či
KDevelop	KDevelop	k1gInSc4	KDevelop
(	(	kIx(	(
<g/>
primárně	primárně	k6eAd1	primárně
pro	pro	k7c4	pro
prostředí	prostředí	k1gNnSc4	prostředí
KDE	kde	k6eAd1	kde
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podpora	podpora	k1gFnSc1	podpora
linuxových	linuxový	k2eAgFnPc2d1	linuxová
distribucí	distribuce	k1gFnPc2	distribuce
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
realizována	realizován	k2eAgFnSc1d1	realizována
komerčními	komerční	k2eAgFnPc7d1	komerční
společnostmi	společnost	k1gFnPc7	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Canonical	Canonical	k1gFnSc4	Canonical
<g/>
,	,	kIx,	,
Novell	Novell	kA	Novell
<g/>
,	,	kIx,	,
Red	Red	k1gFnSc1	Red
Hat	hat	k0	hat
nebo	nebo	k8xC	nebo
Mandriva	Mandrivo	k1gNnPc4	Mandrivo
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
přímo	přímo	k6eAd1	přímo
o	o	k7c6	o
společnosti	společnost	k1gFnSc6	společnost
spravující	spravující	k2eAgFnSc4d1	spravující
určitou	určitý	k2eAgFnSc4d1	určitá
distribuci	distribuce	k1gFnSc4	distribuce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
jsou	být	k5eAaImIp3nP	být
společnosti	společnost	k1gFnPc1	společnost
jako	jako	k8xC	jako
např.	např.	kA	např.
VA	va	k0wR	va
Linux	linux	k1gInSc1	linux
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
specializují	specializovat	k5eAaBmIp3nP	specializovat
na	na	k7c4	na
aplikace	aplikace	k1gFnPc4	aplikace
řešení	řešení	k1gNnPc2	řešení
postavených	postavený	k2eAgMnPc2d1	postavený
na	na	k7c6	na
Linuxu	linux	k1gInSc6	linux
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenější	rozšířený	k2eAgInSc1d3	nejrozšířenější
model	model	k1gInSc1	model
podpory	podpora	k1gFnSc2	podpora
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
koupí	koupě	k1gFnSc7	koupě
distribuce	distribuce	k1gFnSc2	distribuce
dostáváte	dostávat	k5eAaImIp2nP	dostávat
právo	právo	k1gNnSc4	právo
využít	využít	k5eAaPmF	využít
omezenou	omezený	k2eAgFnSc4d1	omezená
podporu	podpora	k1gFnSc4	podpora
po	po	k7c4	po
omezený	omezený	k2eAgInSc4d1	omezený
čas	čas	k1gInSc4	čas
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
můžete	moct	k5eAaImIp2nP	moct
později	pozdě	k6eAd2	pozdě
dokoupit	dokoupit	k5eAaPmF	dokoupit
další	další	k2eAgFnPc4d1	další
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
uživatele	uživatel	k1gMnPc4	uživatel
distribucí	distribuce	k1gFnPc2	distribuce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zdarma	zdarma	k6eAd1	zdarma
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
poměrně	poměrně	k6eAd1	poměrně
dobrá	dobrý	k2eAgFnSc1d1	dobrá
podpora	podpora	k1gFnSc1	podpora
řada	řada	k1gFnSc1	řada
diskusních	diskusní	k2eAgNnPc2d1	diskusní
fór	fórum	k1gNnPc2	fórum
<g/>
,	,	kIx,	,
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
i	i	k8xC	i
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
GNU	gnu	k1gNnSc1	gnu
<g/>
/	/	kIx~	/
<g/>
Linux	linux	k1gInSc1	linux
kontroverze	kontroverze	k1gFnSc2	kontroverze
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Linux	Linux	kA	Linux
původně	původně	k6eAd1	původně
označoval	označovat	k5eAaImAgInS	označovat
jen	jen	k9	jen
samotné	samotný	k2eAgNnSc4d1	samotné
jádro	jádro	k1gNnSc4	jádro
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
byl	být	k5eAaImAgInS	být
název	název	k1gInSc1	název
zevšeobecněn	zevšeobecnit	k5eAaPmNgInS	zevšeobecnit
a	a	k8xC	a
vztažen	vztáhnout	k5eAaPmNgInS	vztáhnout
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
operační	operační	k2eAgInSc4d1	operační
systém	systém	k1gInSc4	systém
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
Linux	Linux	kA	Linux
a	a	k8xC	a
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
GNU	gnu	k1gNnSc2	gnu
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
ale	ale	k8xC	ale
neplatí	platit	k5eNaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Linux	linux	k1gInSc1	linux
a	a	k8xC	a
GNU	gnu	k1gNnSc1	gnu
tvoří	tvořit	k5eAaImIp3nS	tvořit
takřka	takřka	k6eAd1	takřka
celý	celý	k2eAgInSc1d1	celý
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
do	do	k7c2	do
systému	systém	k1gInSc2	systém
byla	být	k5eAaImAgFnS	být
postupně	postupně	k6eAd1	postupně
zahrnuta	zahrnut	k2eAgFnSc1d1	zahrnuta
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
vznikaly	vznikat	k5eAaImAgInP	vznikat
dříve	dříve	k6eAd2	dříve
spory	spor	k1gInPc1	spor
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
správně	správně	k6eAd1	správně
slovo	slovo	k1gNnSc1	slovo
Linux	Linux	kA	Linux
vyslovovat	vyslovovat	k5eAaImF	vyslovovat
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
slovo	slovo	k1gNnSc1	slovo
vyslovováno	vyslovován	k2eAgNnSc1d1	vyslovováno
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
existují	existovat	k5eAaImIp3nP	existovat
různé	různý	k2eAgFnPc4d1	různá
varianty	varianta	k1gFnPc4	varianta
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
sám	sám	k3xTgMnSc1	sám
Linus	Linus	k1gMnSc1	Linus
Torvalds	Torvaldsa	k1gFnPc2	Torvaldsa
<g/>
,	,	kIx,	,
původce	původce	k1gMnSc2	původce
jména	jméno	k1gNnSc2	jméno
Linux	Linux	kA	Linux
<g/>
,	,	kIx,	,
slovo	slovo	k1gNnSc1	slovo
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
–	–	k?	–
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
podle	podle	k7c2	podle
Linuse	Linuse	k1gFnSc2	Linuse
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
SCHRODER	SCHRODER	kA	SCHRODER
<g/>
,	,	kIx,	,
Carla	Carlo	k1gNnSc2	Carlo
<g/>
.	.	kIx.	.
</s>
<s>
Linux	linux	k1gInSc1	linux
–	–	k?	–
Kuchařka	kuchařka	k1gFnSc1	kuchařka
administrátora	administrátor	k1gMnSc2	administrátor
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
:	:	kIx,	:
Computer	computer	k1gInSc1	computer
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
251	[number]	k4	251
<g/>
-	-	kIx~	-
<g/>
2407	[number]	k4	2407
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
608	[number]	k4	608
<g/>
.	.	kIx.	.
</s>
<s>
Bednář	Bednář	k1gMnSc1	Bednář
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
:	:	kIx,	:
Linux	Linux	kA	Linux
na	na	k7c6	na
firemním	firemní	k2eAgInSc6d1	firemní
PC	PC	kA	PC
–	–	k?	–
možnosti	možnost	k1gFnPc4	možnost
<g/>
,	,	kIx,	,
rizika	riziko	k1gNnPc1	riziko
cena	cena	k1gFnSc1	cena
<g/>
,	,	kIx,	,
BEN	Ben	k1gInSc1	Ben
-	-	kIx~	-
technická	technický	k2eAgFnSc1d1	technická
literatura	literatura	k1gFnSc1	literatura
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7300-225-1	[number]	k4	978-80-7300-225-1
Svobodný	svobodný	k2eAgInSc1d1	svobodný
software	software	k1gInSc1	software
Projekt	projekt	k1gInSc1	projekt
GNU	gnu	k1gNnSc2	gnu
Operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
LAMP	lampa	k1gFnPc2	lampa
Unix	Unix	k1gInSc4	Unix
Seznam	seznam	k1gInSc4	seznam
distribucí	distribuce	k1gFnPc2	distribuce
Linuxu	linux	k1gInSc2	linux
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Linux	Linux	kA	Linux
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Galerie	galerie	k1gFnSc2	galerie
Linux	Linux	kA	Linux
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Linux	Linux	kA	Linux
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Kniha	kniha	k1gFnSc1	kniha
Linux	linux	k1gInSc1	linux
ve	v	k7c6	v
Wikiknihách	Wikikniha	k1gFnPc6	Wikikniha
Výukový	výukový	k2eAgInSc4d1	výukový
kurs	kurs	k1gInSc4	kurs
Linux	Linux	kA	Linux
ve	v	k7c6	v
Wikiverzitě	Wikiverzita	k1gFnSc6	Wikiverzita
Téma	téma	k1gNnSc1	téma
Linux	Linux	kA	Linux
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Filosofie	filosofie	k1gFnSc2	filosofie
projektu	projekt	k1gInSc2	projekt
GNU	gnu	k1gNnSc7	gnu
"	"	kIx"	"
<g/>
GNU	gnu	k1gMnSc1	gnu
<g/>
/	/	kIx~	/
<g/>
Linux	Linux	kA	Linux
<g/>
"	"	kIx"	"
Co	co	k3yInSc4	co
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Gnu	gnu	k1gNnSc7	gnu
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
české	český	k2eAgFnSc2d1	Česká
stránky	stránka	k1gFnSc2	stránka
hnutí	hnutí	k1gNnSc2	hnutí
GNU	gnu	k1gNnSc1	gnu
Přehled	přehled	k1gInSc1	přehled
distribucí	distribuce	k1gFnSc7	distribuce
Linux	Linux	kA	Linux
Dokumentační	dokumentační	k2eAgFnSc2d1	dokumentační
Projekt	projekt	k1gInSc4	projekt
–	–	k?	–
česky	česky	k6eAd1	česky
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
MB	MB	kA	MB
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Linux	linux	k1gInSc1	linux
Documentation	Documentation	k1gInSc1	Documentation
Project	Project	k2eAgInSc1d1	Project
ÚSL	ÚSL	kA	ÚSL
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
Systému	systém	k1gInSc2	systém
Linux	linux	k1gInSc1	linux
Učebnice	učebnice	k1gFnSc1	učebnice
GNU	gnu	k1gMnSc1	gnu
<g/>
/	/	kIx~	/
<g/>
Linuxu	linux	k1gInSc2	linux
–	–	k?	–
česky	česky	k6eAd1	česky
Dokumenty	dokument	k1gInPc1	dokument
o	o	k7c6	o
Linuxu	linux	k1gInSc6	linux
Revolution	Revolution	k1gInSc1	Revolution
OS	OS	kA	OS
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc2	režie
J.	J.	kA	J.
T.	T.	kA	T.
S.	S.	kA	S.
Moore	Moor	k1gInSc5	Moor
<g/>
,	,	kIx,	,
85	[number]	k4	85
minut	minuta	k1gFnPc2	minuta
</s>
