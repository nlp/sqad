<p>
<s>
Očkování	očkování	k1gNnSc1	očkování
(	(	kIx(	(
<g/>
též	též	k9	též
vakcinace	vakcinace	k1gFnSc1	vakcinace
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
lékařský	lékařský	k2eAgInSc1d1	lékařský
zákrok	zákrok	k1gInSc1	zákrok
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
zdravý	zdravý	k2eAgInSc1d1	zdravý
organismus	organismus	k1gInSc1	organismus
záměrně	záměrně	k6eAd1	záměrně
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
méně	málo	k6eAd2	málo
nebezpečným	bezpečný	k2eNgInSc7d1	nebezpečný
mikrobem	mikrob	k1gInSc7	mikrob
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gInSc7	jeho
fragmentem	fragment	k1gInSc7	fragment
<g/>
.	.	kIx.	.
</s>
<s>
Imunitní	imunitní	k2eAgInSc1d1	imunitní
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
naučí	naučit	k5eAaPmIp3nS	naučit
rozpoznávat	rozpoznávat	k5eAaImF	rozpoznávat
příslušné	příslušný	k2eAgInPc4d1	příslušný
antigeny	antigen	k1gInPc4	antigen
a	a	k8xC	a
očkovaný	očkovaný	k2eAgInSc1d1	očkovaný
by	by	kYmCp3nS	by
tak	tak	k9	tak
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
chráněn	chránit	k5eAaImNgInS	chránit
před	před	k7c7	před
nákazou	nákaza	k1gFnSc7	nákaza
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
před	před	k7c7	před
vážným	vážný	k2eAgInSc7d1	vážný
průběhem	průběh	k1gInSc7	průběh
onemocnění	onemocnění	k1gNnSc2	onemocnění
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
původcem	původce	k1gMnSc7	původce
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Očkování	očkování	k1gNnSc1	očkování
nevede	vést	k5eNaImIp3nS	vést
vždy	vždy	k6eAd1	vždy
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
imunity	imunita	k1gFnSc2	imunita
<g/>
,	,	kIx,	,
ne	ne	k9	ne
každý	každý	k3xTgMnSc1	každý
očkovaný	očkovaný	k2eAgMnSc1d1	očkovaný
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
chráněn	chráněn	k2eAgMnSc1d1	chráněn
před	před	k7c7	před
infekcí	infekce	k1gFnSc7	infekce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Očkování	očkování	k1gNnSc2	očkování
proti	proti	k7c3	proti
některým	některý	k3yIgFnPc3	některý
nemocem	nemoc	k1gFnPc3	nemoc
má	mít	k5eAaImIp3nS	mít
širokou	široký	k2eAgFnSc4d1	široká
společenskou	společenský	k2eAgFnSc4d1	společenská
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
také	také	k9	také
podporu	podpora	k1gFnSc4	podpora
některých	některý	k3yIgFnPc2	některý
zdravotních	zdravotní	k2eAgFnPc2d1	zdravotní
pojišťoven	pojišťovna	k1gFnPc2	pojišťovna
a	a	k8xC	a
některých	některý	k3yIgInPc2	některý
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Podpora	podpora	k1gFnSc1	podpora
očkování	očkování	k1gNnSc2	očkování
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
podobu	podoba	k1gFnSc4	podoba
cíleného	cílený	k2eAgInSc2d1	cílený
či	či	k8xC	či
plošného	plošný	k2eAgInSc2d1	plošný
finančního	finanční	k2eAgInSc2d1	finanční
příspěvku	příspěvek	k1gInSc2	příspěvek
<g/>
,	,	kIx,	,
povinnost	povinnost	k1gFnSc4	povinnost
očkování	očkování	k1gNnSc2	očkování
jako	jako	k8xS	jako
předpokladu	předpoklad	k1gInSc2	předpoklad
institucionálního	institucionální	k2eAgNnSc2d1	institucionální
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
nebo	nebo	k8xC	nebo
organizovaných	organizovaný	k2eAgInPc2d1	organizovaný
dětských	dětský	k2eAgInPc2d1	dětský
pobytů	pobyt	k1gInPc2	pobyt
a	a	k8xC	a
nebo	nebo	k8xC	nebo
i	i	k9	i
nepodmíněnou	podmíněný	k2eNgFnSc4d1	nepodmíněná
všeobecnou	všeobecný	k2eAgFnSc4d1	všeobecná
povinnost	povinnost	k1gFnSc4	povinnost
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
případů	případ	k1gInPc2	případ
jasných	jasný	k2eAgFnPc2d1	jasná
kontraindikací	kontraindikace	k1gFnPc2	kontraindikace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
u	u	k7c2	u
silné	silný	k2eAgFnSc2d1	silná
státní	státní	k2eAgFnSc2d1	státní
podpory	podpora	k1gFnSc2	podpora
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
zpochybňující	zpochybňující	k2eAgInPc1d1	zpochybňující
názory	názor	k1gInPc1	názor
a	a	k8xC	a
podpora	podpora	k1gFnSc1	podpora
daného	daný	k2eAgNnSc2d1	dané
očkování	očkování	k1gNnSc2	očkování
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stává	stávat	k5eAaImIp3nS	stávat
předmětem	předmět	k1gInSc7	předmět
celospolečenské	celospolečenský	k2eAgFnSc2d1	celospolečenská
debaty	debata	k1gFnSc2	debata
zvyšující	zvyšující	k2eAgFnSc4d1	zvyšující
poptávku	poptávka	k1gFnSc4	poptávka
po	po	k7c6	po
dalším	další	k2eAgInSc6d1	další
výzkumu	výzkum	k1gInSc6	výzkum
ohledně	ohledně	k7c2	ohledně
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
možných	možný	k2eAgInPc2d1	možný
žádoucích	žádoucí	k2eAgInPc2d1	žádoucí
a	a	k8xC	a
nežádoucích	žádoucí	k2eNgInPc2d1	nežádoucí
účinků	účinek	k1gInPc2	účinek
diskutovaného	diskutovaný	k2eAgNnSc2d1	diskutované
očkování	očkování	k1gNnSc2	očkování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
zprávy	zpráva	k1gFnPc1	zpráva
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Východní	východní	k2eAgFnSc2d1	východní
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
ochrana	ochrana	k1gFnSc1	ochrana
proti	proti	k7c3	proti
neštovicím	neštovice	k1gFnPc3	neštovice
používaly	používat	k5eAaImAgInP	používat
stroupky	stroupek	k1gInPc4	stroupek
z	z	k7c2	z
neštovic	neštovice	k1gFnPc2	neštovice
nemocných	nemocný	k1gMnPc2	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
Skutečně	skutečně	k6eAd1	skutečně
doložené	doložený	k2eAgInPc1d1	doložený
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
výkony	výkon	k1gInPc1	výkon
až	až	k9	až
v	v	k7c4	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
pramenů	pramen	k1gInPc2	pramen
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
prováděny	provádět	k5eAaImNgInP	provádět
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
již	již	k6eAd1	již
v	v	k7c6	v
období	období	k1gNnSc6	období
1000	[number]	k4	1000
let	léto	k1gNnPc2	léto
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
představoval	představovat	k5eAaImAgInS	představovat
nákazu	nákaza	k1gFnSc4	nákaza
<g/>
,	,	kIx,	,
při	pře	k1gFnSc4	pře
které	který	k3yRgNnSc1	který
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
onemocnění	onemocnění	k1gNnSc1	onemocnění
mírnějším	mírný	k2eAgInSc7d2	mírnější
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
ochrany	ochrana	k1gFnSc2	ochrana
pokusila	pokusit	k5eAaPmAgFnS	pokusit
zavést	zavést	k5eAaPmF	zavést
Mary	Mary	k1gFnSc1	Mary
Wortley	Wortlea	k1gFnSc2	Wortlea
Montagu	Montag	k1gInSc2	Montag
(	(	kIx(	(
<g/>
1689	[number]	k4	1689
<g/>
–	–	k?	–
<g/>
1762	[number]	k4	1762
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anglická	anglický	k2eAgFnSc1d1	anglická
aristokratka	aristokratka	k1gFnSc1	aristokratka
a	a	k8xC	a
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sama	sám	k3xTgMnSc4	sám
prodělala	prodělat	k5eAaPmAgFnS	prodělat
neštovice	neštovice	k1gFnSc1	neštovice
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
předcházení	předcházení	k1gNnPc2	předcházení
plnému	plný	k2eAgInSc3d1	plný
onemocnění	onemocnění	k1gNnPc2	onemocnění
se	se	k3xPyFc4	se
seznámila	seznámit	k5eAaPmAgFnS	seznámit
v	v	k7c6	v
Osmanské	osmanský	k2eAgFnSc6d1	Osmanská
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
<g/>
,	,	kIx,	,
variolace	variolace	k1gFnSc1	variolace
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
hojně	hojně	k6eAd1	hojně
byl	být	k5eAaImAgInS	být
využíván	využívat	k5eAaPmNgInS	využívat
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
byl	být	k5eAaImAgInS	být
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
efekt	efekt	k1gInSc1	efekt
variolace	variolace	k1gFnSc2	variolace
dobře	dobře	k6eAd1	dobře
prokazatelný	prokazatelný	k2eAgInSc1d1	prokazatelný
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
byla	být	k5eAaImAgFnS	být
variolace	variolace	k1gFnSc1	variolace
např.	např.	kA	např.
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgNnPc4	první
očkování	očkování	k1gNnPc4	očkování
v	v	k7c6	v
moderním	moderní	k2eAgInSc6d1	moderní
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc2	smysl
zavedl	zavést	k5eAaPmAgMnS	zavést
britský	britský	k2eAgMnSc1d1	britský
lékař	lékař	k1gMnSc1	lékař
Edward	Edward	k1gMnSc1	Edward
Jenner	Jenner	k1gMnSc1	Jenner
(	(	kIx(	(
<g/>
1749	[number]	k4	1749
<g/>
–	–	k?	–
<g/>
1823	[number]	k4	1823
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vypozoroval	vypozorovat	k5eAaPmAgMnS	vypozorovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
prodělali	prodělat	k5eAaPmAgMnP	prodělat
kravské	kravský	k2eAgFnPc4d1	kravská
neštovice	neštovice	k1gFnPc4	neštovice
(	(	kIx(	(
<g/>
jiný	jiný	k2eAgInSc4d1	jiný
virus	virus	k1gInSc4	virus
ze	z	k7c2	z
stejné	stejný	k2eAgFnSc2d1	stejná
čeledi	čeleď	k1gFnSc2	čeleď
Poxviridae	Poxvirida	k1gFnSc2	Poxvirida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
podstatně	podstatně	k6eAd1	podstatně
lehčí	lehký	k2eAgNnPc1d2	lehčí
onemocnění	onemocnění	k1gNnPc1	onemocnění
<g/>
,	,	kIx,	,
nemívají	mívat	k5eNaImIp3nP	mívat
pravé	pravý	k2eAgFnPc4d1	pravá
neštovice	neštovice	k1gFnPc4	neštovice
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
vakcinace	vakcinace	k1gFnSc2	vakcinace
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
slova	slovo	k1gNnSc2	slovo
vacca	vacca	k1gFnSc1	vacca
=	=	kIx~	=
kráva	kráva	k1gFnSc1	kráva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následující	následující	k2eAgInSc1d1	následující
přehled	přehled	k1gInSc1	přehled
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
některé	některý	k3yIgFnPc4	některý
první	první	k4xOgFnPc4	první
vakcíny	vakcína	k1gFnPc4	vakcína
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
–	–	k?	–
Variolace	Variolace	k1gFnSc2	Variolace
<g/>
,	,	kIx,	,
očkování	očkování	k1gNnSc2	očkování
proti	proti	k7c3	proti
neštovicím	neštovice	k1gFnPc3	neštovice
oslabeným	oslabený	k2eAgMnSc7d1	oslabený
patogenem	patogen	k1gInSc7	patogen
(	(	kIx(	(
<g/>
Montagu	Montaga	k1gFnSc4	Montaga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1798	[number]	k4	1798
–	–	k?	–
Očkování	očkování	k1gNnSc2	očkování
proti	proti	k7c3	proti
neštovicím	neštovice	k1gFnPc3	neštovice
příbuzným	příbuzný	k2eAgMnSc7d1	příbuzný
virem	vir	k1gInSc7	vir
s	s	k7c7	s
podstatně	podstatně	k6eAd1	podstatně
méně	málo	k6eAd2	málo
závažným	závažný	k2eAgInSc7d1	závažný
průběhem	průběh	k1gInSc7	průběh
infekce	infekce	k1gFnSc2	infekce
(	(	kIx(	(
<g/>
Jenner	Jenner	k1gInSc1	Jenner
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1885	[number]	k4	1885
–	–	k?	–
Očkování	očkování	k1gNnSc4	očkování
proti	proti	k7c3	proti
vzteklině	vzteklina	k1gFnSc6	vzteklina
oslabeným	oslabený	k2eAgInSc7d1	oslabený
kmenem	kmen	k1gInSc7	kmen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1896	[number]	k4	1896
–	–	k?	–
Očkování	očkování	k1gNnSc2	očkování
proti	proti	k7c3	proti
břišnímu	břišní	k2eAgInSc3d1	břišní
tyfu	tyf	k1gInSc2	tyf
usmrceným	usmrcený	k2eAgMnSc7d1	usmrcený
původcem	původce	k1gMnSc7	původce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1896	[number]	k4	1896
–	–	k?	–
Očkování	očkování	k1gNnSc4	očkování
proti	proti	k7c3	proti
choleře	cholera	k1gFnSc3	cholera
usmrceným	usmrcený	k2eAgMnSc7d1	usmrcený
původcem	původce	k1gMnSc7	původce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1897	[number]	k4	1897
–	–	k?	–
Očkování	očkování	k1gNnSc4	očkování
proti	proti	k7c3	proti
moru	mor	k1gInSc2	mor
usmrceným	usmrcený	k2eAgMnSc7d1	usmrcený
původcem	původce	k1gMnSc7	původce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1923	[number]	k4	1923
–	–	k?	–
Očkování	očkování	k1gNnSc4	očkování
proti	proti	k7c3	proti
záškrtu	záškrt	k1gInSc3	záškrt
<g/>
,	,	kIx,	,
očkování	očkování	k1gNnSc1	očkování
toxoidem	toxoid	k1gInSc7	toxoid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1926	[number]	k4	1926
–	–	k?	–
Očkování	očkování	k1gNnSc2	očkování
proti	proti	k7c3	proti
černému	černý	k2eAgInSc3d1	černý
kašli	kašel	k1gInSc3	kašel
(	(	kIx(	(
<g/>
pertusi	pertuse	k1gFnSc4	pertuse
<g/>
)	)	kIx)	)
usmrceným	usmrcený	k2eAgMnSc7d1	usmrcený
původcem	původce	k1gMnSc7	původce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1926	[number]	k4	1926
–	–	k?	–
Očkování	očkování	k1gNnSc4	očkování
proti	proti	k7c3	proti
tetanu	tetan	k1gInSc3	tetan
<g/>
,	,	kIx,	,
očkování	očkování	k1gNnSc1	očkování
toxoidem	toxoid	k1gInSc7	toxoid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1927	[number]	k4	1927
–	–	k?	–
Očkování	očkování	k1gNnSc4	očkování
proti	proti	k7c3	proti
tuberkulóze	tuberkulóza	k1gFnSc6	tuberkulóza
oslabeným	oslabený	k2eAgInSc7d1	oslabený
kmenem	kmen	k1gInSc7	kmen
(	(	kIx(	(
<g/>
Calmettův	Calmettův	k2eAgInSc1d1	Calmettův
Guérinův	Guérinův	k2eAgInSc1d1	Guérinův
bacil	bacil	k1gInSc1	bacil
<g/>
,	,	kIx,	,
BCG	BCG	kA	BCG
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1935	[number]	k4	1935
–	–	k?	–
Očkování	očkování	k1gNnSc4	očkování
proti	proti	k7c3	proti
žluté	žlutý	k2eAgFnSc3d1	žlutá
zimnici	zimnice	k1gFnSc3	zimnice
oslabeným	oslabený	k2eAgInSc7d1	oslabený
kmenem	kmen	k1gInSc7	kmen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1936	[number]	k4	1936
–	–	k?	–
Očkování	očkování	k1gNnSc4	očkování
proti	proti	k7c3	proti
chřipce	chřipka	k1gFnSc3	chřipka
usmrceným	usmrcený	k2eAgMnSc7d1	usmrcený
původcem	původce	k1gMnSc7	původce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1938	[number]	k4	1938
–	–	k?	–
Očkování	očkování	k1gNnSc2	očkování
proti	proti	k7c3	proti
skvrnitému	skvrnitý	k2eAgInSc3d1	skvrnitý
tyfu	tyf	k1gInSc2	tyf
usmrceným	usmrcený	k2eAgMnSc7d1	usmrcený
původcem	původce	k1gMnSc7	původce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1955	[number]	k4	1955
–	–	k?	–
Očkování	očkování	k1gNnSc4	očkování
proti	proti	k7c3	proti
dětské	dětský	k2eAgFnSc3d1	dětská
obrně	obrna	k1gFnSc6	obrna
usmrceným	usmrcený	k2eAgInSc7d1	usmrcený
organismem	organismus	k1gInSc7	organismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1963	[number]	k4	1963
–	–	k?	–
Očkování	očkování	k1gNnSc4	očkování
proti	proti	k7c3	proti
dětské	dětský	k2eAgFnSc3d1	dětská
obrně	obrna	k1gFnSc6	obrna
oslabeným	oslabený	k2eAgInSc7d1	oslabený
kmenem	kmen	k1gInSc7	kmen
(	(	kIx(	(
<g/>
perorální	perorální	k2eAgFnSc1d1	perorální
vakcína	vakcína	k1gFnSc1	vakcína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1963	[number]	k4	1963
–	–	k?	–
Očkování	očkování	k1gNnSc2	očkování
proti	proti	k7c3	proti
spalničkám	spalničky	k1gFnPc3	spalničky
oslabeným	oslabený	k2eAgMnSc7d1	oslabený
kmenem	kmen	k1gInSc7	kmen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1967	[number]	k4	1967
–	–	k?	–
Očkování	očkování	k1gNnSc2	očkování
proti	proti	k7c3	proti
příušnicím	příušnice	k1gFnPc3	příušnice
oslabeným	oslabený	k2eAgMnSc7d1	oslabený
kmenem	kmen	k1gInSc7	kmen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
–	–	k?	–
Očkování	očkování	k1gNnSc2	očkování
proti	proti	k7c3	proti
zarděnkám	zarděnky	k1gFnPc3	zarděnky
oslabeným	oslabený	k2eAgMnSc7d1	oslabený
kmenem	kmen	k1gInSc7	kmen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
–	–	k?	–
Očkování	očkování	k1gNnSc2	očkování
proti	proti	k7c3	proti
proteinům	protein	k1gInPc3	protein
produkovaným	produkovaný	k2eAgInPc3d1	produkovaný
při	při	k7c6	při
anthraxu	anthrax	k1gInSc6	anthrax
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1974	[number]	k4	1974
–	–	k?	–
Očkování	očkování	k1gNnSc4	očkování
proti	proti	k7c3	proti
meningokokovi	meningokok	k1gMnSc3	meningokok
povrchovými	povrchový	k2eAgInPc7d1	povrchový
polysacharidy	polysacharid	k1gInPc7	polysacharid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
–	–	k?	–
Očkování	očkování	k1gNnSc4	očkování
proti	proti	k7c3	proti
pneumokokovi	pneumokok	k1gMnSc3	pneumokok
povrchovými	povrchový	k2eAgInPc7d1	povrchový
polysacharidy	polysacharid	k1gInPc7	polysacharid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
–	–	k?	–
Očkování	očkování	k1gNnSc4	očkování
proti	proti	k7c3	proti
hepatitidě	hepatitida	k1gFnSc3	hepatitida
B	B	kA	B
rekombinantním	rekombinantní	k2eAgInSc7d1	rekombinantní
povrchovým	povrchový	k2eAgInSc7d1	povrchový
antigenem	antigen	k1gInSc7	antigen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Princip	princip	k1gInSc1	princip
účinku	účinek	k1gInSc2	účinek
==	==	k?	==
</s>
</p>
<p>
<s>
Vrozená	vrozený	k2eAgFnSc1d1	vrozená
imunita	imunita	k1gFnSc1	imunita
rozpoznává	rozpoznávat	k5eAaImIp3nS	rozpoznávat
cizí	cizí	k2eAgInPc4d1	cizí
antigeny	antigen	k1gInPc4	antigen
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
setkání	setkání	k1gNnSc6	setkání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vazba	vazba	k1gFnSc1	vazba
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
slabá	slabý	k2eAgFnSc1d1	slabá
a	a	k8xC	a
odpověď	odpověď	k1gFnSc1	odpověď
není	být	k5eNaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
<g/>
.	.	kIx.	.
</s>
<s>
Získaná	získaný	k2eAgFnSc1d1	získaná
imunita	imunita	k1gFnSc1	imunita
má	mít	k5eAaImIp3nS	mít
odpověď	odpověď	k1gFnSc4	odpověď
cílenou	cílený	k2eAgFnSc4d1	cílená
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
strukturu	struktura	k1gFnSc4	struktura
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
razantnější	razantní	k2eAgMnSc1d2	razantnější
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
organismus	organismus	k1gInSc1	organismus
prvně	prvně	k?	prvně
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
patogenem	patogen	k1gInSc7	patogen
<g/>
,	,	kIx,	,
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
pouze	pouze	k6eAd1	pouze
vrozená	vrozený	k2eAgFnSc1d1	vrozená
imunita	imunita	k1gFnSc1	imunita
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
likvidace	likvidace	k1gFnSc2	likvidace
takto	takto	k6eAd1	takto
rozpoznaných	rozpoznaný	k2eAgFnPc2d1	rozpoznaná
struktur	struktura	k1gFnPc2	struktura
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc1	tento
předkládány	předkládán	k2eAgFnPc1d1	předkládána
i	i	k9	i
částem	část	k1gFnPc3	část
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
odpovědným	odpovědný	k2eAgMnSc7d1	odpovědný
za	za	k7c4	za
vznik	vznik	k1gInSc4	vznik
získané	získaný	k2eAgFnSc2d1	získaná
imunity	imunita	k1gFnSc2	imunita
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
organismus	organismus	k1gInSc1	organismus
setká	setkat	k5eAaPmIp3nS	setkat
podruhé	podruhé	k6eAd1	podruhé
s	s	k7c7	s
týmž	týž	k3xTgInSc7	týž
patogenem	patogen	k1gInSc7	patogen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
reakce	reakce	k1gFnSc1	reakce
již	již	k6eAd1	již
rychlejší	rychlý	k2eAgFnSc1d2	rychlejší
a	a	k8xC	a
intenzivnější	intenzivní	k2eAgFnSc1d2	intenzivnější
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
ani	ani	k8xC	ani
neproběhne	proběhnout	k5eNaPmIp3nS	proběhnout
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Očkování	očkování	k1gNnSc1	očkování
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
imunitnímu	imunitní	k2eAgInSc3d1	imunitní
systému	systém	k1gInSc3	systém
předloženy	předložit	k5eAaPmNgInP	předložit
antigeny	antigen	k1gInPc1	antigen
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
původcem	původce	k1gMnSc7	původce
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
zajištěno	zajištěn	k2eAgNnSc1d1	zajištěno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
neproběhla	proběhnout	k5eNaPmAgFnS	proběhnout
skutečná	skutečný	k2eAgFnSc1d1	skutečná
infekce	infekce	k1gFnSc1	infekce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Účinnost	účinnost	k1gFnSc1	účinnost
a	a	k8xC	a
účinkovost	účinkovost	k1gFnSc1	účinkovost
<g/>
[	[	kIx(	[
<g/>
ujasnit	ujasnit	k5eAaPmF	ujasnit
<g/>
]	]	kIx)	]
vakcín	vakcína	k1gFnPc2	vakcína
se	se	k3xPyFc4	se
zaměňují	zaměňovat	k5eAaImIp3nP	zaměňovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bývají	bývat	k5eAaImIp3nP	bývat
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
vakcín	vakcína	k1gFnPc2	vakcína
==	==	k?	==
</s>
</p>
<p>
<s>
Toho	ten	k3xDgMnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
neproběhla	proběhnout	k5eNaPmAgFnS	proběhnout
plná	plný	k2eAgFnSc1d1	plná
infekce	infekce	k1gFnSc1	infekce
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
několika	několik	k4yIc2	několik
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
použitého	použitý	k2eAgInSc2d1	použitý
způsobu	způsob	k1gInSc2	způsob
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
následující	následující	k2eAgInPc1d1	následující
typy	typ	k1gInPc1	typ
vakcín	vakcína	k1gFnPc2	vakcína
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Oslabená	oslabený	k2eAgFnSc1d1	oslabená
vakcína	vakcína	k1gFnSc1	vakcína
(	(	kIx(	(
<g/>
atenuovaná	atenuovaný	k2eAgFnSc1d1	atenuovaná
vakcína	vakcína	k1gFnSc1	vakcína
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
živými	živý	k2eAgInPc7d1	živý
patogeny	patogen	k1gInPc7	patogen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ztratily	ztratit	k5eAaPmAgFnP	ztratit
schopnost	schopnost	k1gFnSc4	schopnost
vyvolat	vyvolat	k5eAaPmF	vyvolat
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
takovéto	takovýto	k3xDgFnSc6	takovýto
vakcíně	vakcína	k1gFnSc6	vakcína
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
obvykle	obvykle	k6eAd1	obvykle
nanejvýš	nanejvýš	k6eAd1	nanejvýš
mírná	mírný	k2eAgFnSc1d1	mírná
infekce	infekce	k1gFnSc1	infekce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zanechává	zanechávat	k5eAaImIp3nS	zanechávat
imunitu	imunita	k1gFnSc4	imunita
jako	jako	k8xS	jako
po	po	k7c6	po
prodělaném	prodělaný	k2eAgNnSc6d1	prodělané
onemocnění	onemocnění	k1gNnSc6	onemocnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Inaktivovaná	Inaktivovaný	k2eAgFnSc1d1	Inaktivovaná
vakcína	vakcína	k1gFnSc1	vakcína
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
usmrcenými	usmrcený	k2eAgInPc7d1	usmrcený
patogeny	patogen	k1gInPc7	patogen
<g/>
.	.	kIx.	.
</s>
<s>
Usmrcení	usmrcení	k1gNnSc1	usmrcení
patogenů	patogen	k1gInPc2	patogen
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
provedeno	provést	k5eAaPmNgNnS	provést
takovým	takový	k3xDgInSc7	takový
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgInP	být
zachována	zachován	k2eAgFnSc1d1	zachována
struktura	struktura	k1gFnSc1	struktura
antigenů	antigen	k1gInPc2	antigen
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
rozpozná	rozpoznat	k5eAaPmIp3nS	rozpoznat
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Toxoidová	Toxoidový	k2eAgFnSc1d1	Toxoidový
vakcína	vakcína	k1gFnSc1	vakcína
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
bakteriálními	bakteriální	k2eAgInPc7d1	bakteriální
toxiny	toxin	k1gInPc7	toxin
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
potlačenou	potlačený	k2eAgFnSc4d1	potlačená
schopnost	schopnost	k1gFnSc4	schopnost
vyvolat	vyvolat	k5eAaPmF	vyvolat
toxickou	toxický	k2eAgFnSc4d1	toxická
reakci	reakce	k1gFnSc4	reakce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stimulují	stimulovat	k5eAaImIp3nP	stimulovat
imunitní	imunitní	k2eAgFnSc4d1	imunitní
odpověď	odpověď	k1gFnSc4	odpověď
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
očkování	očkování	k1gNnSc1	očkování
tetanovým	tetanový	k2eAgInSc7d1	tetanový
toxoidem	toxoid	k1gInSc7	toxoid
nijak	nijak	k6eAd1	nijak
neomezí	omezit	k5eNaPmIp3nS	omezit
množení	množení	k1gNnSc4	množení
původce	původce	k1gMnSc2	původce
tetanu	tetan	k1gInSc2	tetan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zabrání	zabránit	k5eAaPmIp3nS	zabránit
vzniku	vznik	k1gInSc3	vznik
tetanu	tetan	k1gInSc2	tetan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Subjednotková	Subjednotkový	k2eAgFnSc1d1	Subjednotkový
vakcína	vakcína	k1gFnSc1	vakcína
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
pouze	pouze	k6eAd1	pouze
některými	některý	k3yIgFnPc7	některý
fragmenty	fragment	k1gInPc1	fragment
původce	původce	k1gMnSc2	původce
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
kterému	který	k3yIgInSc3	který
se	se	k3xPyFc4	se
očkuje	očkovat	k5eAaImIp3nS	očkovat
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
probíhá	probíhat	k5eAaImIp3nS	probíhat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
patogen	patogen	k1gInSc1	patogen
pomnoží	pomnožit	k5eAaPmIp3nS	pomnožit
<g/>
,	,	kIx,	,
usmrtí	usmrtit	k5eAaPmIp3nS	usmrtit
a	a	k8xC	a
následně	následně	k6eAd1	následně
separuje	separovat	k5eAaBmIp3nS	separovat
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
fragmenty	fragment	k1gInPc4	fragment
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konjugovaná	konjugovaný	k2eAgFnSc1d1	konjugovaná
vakcína	vakcína	k1gFnSc1	vakcína
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
vlastním	vlastní	k2eAgInSc7d1	vlastní
zpravidla	zpravidla	k6eAd1	zpravidla
polysacharidovým	polysacharidový	k2eAgInSc7d1	polysacharidový
antigenem	antigen	k1gInSc7	antigen
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yQgInSc4	který
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
vyvolat	vyvolat	k5eAaPmF	vyvolat
imunitní	imunitní	k2eAgFnSc4d1	imunitní
odpověď	odpověď	k1gFnSc4	odpověď
<g/>
,	,	kIx,	,
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
proteinovým	proteinový	k2eAgInSc7d1	proteinový
nosičem	nosič	k1gInSc7	nosič
(	(	kIx(	(
<g/>
difterickým	difterický	k2eAgInSc7d1	difterický
nebo	nebo	k8xC	nebo
tetanovým	tetanový	k2eAgInSc7d1	tetanový
toxoidem	toxoid	k1gInSc7	toxoid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
prezentaci	prezentace	k1gFnSc4	prezentace
antigenu	antigen	k1gInSc2	antigen
imunitnímu	imunitní	k2eAgInSc3d1	imunitní
systému	systém	k1gInSc3	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rekombinantní	Rekombinantní	k2eAgFnSc1d1	Rekombinantní
vakcína	vakcína	k1gFnSc1	vakcína
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
subjednotkové	subjednotkový	k2eAgFnSc3d1	subjednotková
vakcíně	vakcína	k1gFnSc3	vakcína
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
podjednotky	podjednotka	k1gFnPc1	podjednotka
jsou	být	k5eAaImIp3nP	být
získány	získat	k5eAaPmNgFnP	získat
pomocí	pomocí	k7c2	pomocí
technik	technika	k1gFnPc2	technika
genetického	genetický	k2eAgNnSc2d1	genetické
inženýrství	inženýrství	k1gNnSc2	inženýrství
jako	jako	k8xC	jako
produkty	produkt	k1gInPc4	produkt
činnosti	činnost	k1gFnSc2	činnost
bakterií	bakterie	k1gFnPc2	bakterie
a	a	k8xC	a
kvasinek	kvasinka	k1gFnPc2	kvasinka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DNA	DNA	kA	DNA
vakcína	vakcína	k1gFnSc1	vakcína
je	být	k5eAaImIp3nS	být
výzkumný	výzkumný	k2eAgInSc1d1	výzkumný
koncept	koncept	k1gInSc1	koncept
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
donutit	donutit	k5eAaPmF	donutit
několik	několik	k4yIc4	několik
buněk	buňka	k1gFnPc2	buňka
očkovaného	očkovaný	k2eAgMnSc2d1	očkovaný
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
dočasně	dočasně	k6eAd1	dočasně
produkovaly	produkovat	k5eAaImAgInP	produkovat
fragmenty	fragment	k1gInPc1	fragment
patogenu	patogen	k1gInSc2	patogen
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
kterým	který	k3yRgFnPc3	který
je	být	k5eAaImIp3nS	být
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
očkován	očkován	k2eAgMnSc1d1	očkován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Atenuovaná	Atenuovaný	k2eAgFnSc1d1	Atenuovaný
vakcína	vakcína	k1gFnSc1	vakcína
===	===	k?	===
</s>
</p>
<p>
<s>
Vakcinace	vakcinace	k1gFnSc1	vakcinace
touto	tento	k3xDgFnSc7	tento
formou	forma	k1gFnSc7	forma
vakcíny	vakcína	k1gFnSc2	vakcína
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c4	v
podání	podání	k1gNnSc4	podání
původce	původce	k1gMnSc2	původce
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
živý	živý	k2eAgMnSc1d1	živý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
oslabený	oslabený	k2eAgMnSc1d1	oslabený
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
schopnosti	schopnost	k1gFnSc2	schopnost
vyvolat	vyvolat	k5eAaPmF	vyvolat
infekci	infekce	k1gFnSc4	infekce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
organismus	organismus	k1gInSc4	organismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
výrazně	výrazně	k6eAd1	výrazně
oslabenou	oslabený	k2eAgFnSc4d1	oslabená
schopnost	schopnost	k1gFnSc4	schopnost
přežívat	přežívat	k5eAaImF	přežívat
a	a	k8xC	a
množit	množit	k5eAaImF	množit
se	se	k3xPyFc4	se
například	například	k6eAd1	například
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
cíleného	cílený	k2eAgNnSc2d1	cílené
poškození	poškození	k1gNnSc2	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k6eAd1	vedle
skutečné	skutečný	k2eAgNnSc4d1	skutečné
oslabení	oslabení	k1gNnSc4	oslabení
schopnosti	schopnost	k1gFnSc2	schopnost
mikroorganismu	mikroorganismus	k1gInSc2	mikroorganismus
se	se	k3xPyFc4	se
množit	množit	k5eAaImF	množit
a	a	k8xC	a
přežívat	přežívat	k5eAaImF	přežívat
může	moct	k5eAaImIp3nS	moct
oslabení	oslabení	k1gNnSc4	oslabení
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
i	i	k9	i
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
kmen	kmen	k1gInSc1	kmen
používaný	používaný	k2eAgInSc1d1	používaný
k	k	k7c3	k
vakcinaci	vakcinace	k1gFnSc3	vakcinace
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
ve	v	k7c6	v
tkáňových	tkáňový	k2eAgFnPc6d1	tkáňová
kulturách	kultura	k1gFnPc6	kultura
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
ztratí	ztratit	k5eAaPmIp3nS	ztratit
schopnost	schopnost	k1gFnSc4	schopnost
vyvolat	vyvolat	k5eAaPmF	vyvolat
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
boje	boj	k1gInSc2	boj
s	s	k7c7	s
mikroorganismy	mikroorganismus	k1gInPc7	mikroorganismus
ovšem	ovšem	k9	ovšem
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
jen	jen	k9	jen
pozitivní	pozitivní	k2eAgMnPc4d1	pozitivní
<g/>
.	.	kIx.	.
<g/>
Velkou	velký	k2eAgFnSc7d1	velká
výhodou	výhoda	k1gFnSc7	výhoda
atenuovaných	atenuovaný	k2eAgFnPc2d1	atenuovaná
vakcín	vakcína	k1gFnPc2	vakcína
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
takové	takový	k3xDgNnSc1	takový
očkování	očkování	k1gNnSc1	očkování
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
z	z	k7c2	z
podstaty	podstata	k1gFnSc2	podstata
prakticky	prakticky	k6eAd1	prakticky
stejnou	stejný	k2eAgFnSc4d1	stejná
imunitní	imunitní	k2eAgFnSc4d1	imunitní
odpověď	odpověď	k1gFnSc4	odpověď
jako	jako	k8xC	jako
infekce	infekce	k1gFnSc2	infekce
neoslabeným	oslabený	k2eNgInSc7d1	neoslabený
kmenem	kmen	k1gInSc7	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Účinnost	účinnost	k1gFnSc1	účinnost
takového	takový	k3xDgNnSc2	takový
očkování	očkování	k1gNnSc2	očkování
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
malého	malý	k2eAgInSc2d1	malý
podílu	podíl	k1gInSc2	podíl
částečného	částečný	k2eAgNnSc2d1	částečné
nebo	nebo	k8xC	nebo
úplného	úplný	k2eAgNnSc2d1	úplné
selhání	selhání	k1gNnSc2	selhání
očkování	očkování	k1gNnSc2	očkování
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
velmi	velmi	k6eAd1	velmi
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
při	při	k7c6	při
podání	podání	k1gNnSc6	podání
živého	živý	k2eAgInSc2d1	živý
kmene	kmen	k1gInSc2	kmen
může	moct	k5eAaImIp3nS	moct
teoreticky	teoreticky	k6eAd1	teoreticky
vést	vést	k5eAaImF	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
celé	celý	k2eAgNnSc4d1	celé
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
toto	tento	k3xDgNnSc1	tento
hrozí	hrozit	k5eAaImIp3nS	hrozit
u	u	k7c2	u
imunokompromitovaných	imunokompromitovaný	k2eAgMnPc2d1	imunokompromitovaný
pacientů	pacient	k1gMnPc2	pacient
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
u	u	k7c2	u
nemocných	nemocný	k1gMnPc2	nemocný
s	s	k7c7	s
vrozenou	vrozený	k2eAgFnSc7d1	vrozená
poruchou	porucha	k1gFnSc7	porucha
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
u	u	k7c2	u
nemocných	nemocná	k1gFnPc2	nemocná
léčených	léčený	k2eAgFnPc2d1	léčená
imunosupresivními	imunosupresivní	k2eAgInPc7d1	imunosupresivní
léky	lék	k1gInPc7	lék
např.	např.	kA	např.
po	po	k7c6	po
transplantaci	transplantace	k1gFnSc6	transplantace
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
autoimunitní	autoimunitní	k2eAgNnSc4d1	autoimunitní
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
u	u	k7c2	u
HIV	HIV	kA	HIV
pozitivních	pozitivní	k2eAgFnPc2d1	pozitivní
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
pokročilejším	pokročilý	k2eAgNnSc6d2	pokročilejší
stádiu	stádium	k1gNnSc6	stádium
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Riziko	riziko	k1gNnSc1	riziko
zvratu	zvrat	k1gInSc2	zvrat
oslabeného	oslabený	k2eAgInSc2d1	oslabený
kmene	kmen	k1gInSc2	kmen
zpět	zpět	k6eAd1	zpět
v	v	k7c4	v
patogenní	patogenní	k2eAgFnSc4d1	patogenní
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
jen	jen	k9	jen
teoretické	teoretický	k2eAgFnPc1d1	teoretická
<g/>
,	,	kIx,	,
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
pozorováno	pozorován	k2eAgNnSc1d1	pozorováno
u	u	k7c2	u
perorální	perorální	k2eAgFnSc2d1	perorální
vakcíny	vakcína	k1gFnSc2	vakcína
proti	proti	k7c3	proti
poliomyelitidě	poliomyelitida	k1gFnSc3	poliomyelitida
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
nepoužívaná	používaný	k2eNgFnSc1d1	nepoužívaná
Sabinova	Sabinův	k2eAgFnSc1d1	Sabinova
vakcína	vakcína	k1gFnSc1	vakcína
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Technickou	technický	k2eAgFnSc7d1	technická
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
atenuovaných	atenuovaný	k2eAgFnPc2d1	atenuovaná
vakcín	vakcína	k1gFnPc2	vakcína
je	být	k5eAaImIp3nS	být
především	především	k9	především
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vakcinační	vakcinační	k2eAgFnPc1d1	vakcinační
látky	látka	k1gFnPc1	látka
bývají	bývat	k5eAaImIp3nP	bývat
poměrně	poměrně	k6eAd1	poměrně
choulostivé	choulostivý	k2eAgFnPc1d1	choulostivá
na	na	k7c4	na
podmínky	podmínka	k1gFnPc4	podmínka
při	při	k7c6	při
transportu	transport	k1gInSc6	transport
a	a	k8xC	a
skladování	skladování	k1gNnSc6	skladování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
oslabené	oslabený	k2eAgFnPc4d1	oslabená
vakcíny	vakcína	k1gFnPc4	vakcína
pro	pro	k7c4	pro
následující	následující	k2eAgNnSc4d1	následující
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
spalničky	spalničky	k1gFnPc1	spalničky
</s>
</p>
<p>
<s>
příušnice	příušnice	k1gFnPc1	příušnice
</s>
</p>
<p>
<s>
zarděnky	zarděnky	k1gFnPc1	zarděnky
</s>
</p>
<p>
<s>
kravské	kravský	k2eAgFnPc1d1	kravská
neštovice	neštovice	k1gFnPc1	neštovice
</s>
</p>
<p>
<s>
plané	planý	k2eAgFnPc1d1	planá
neštovice	neštovice	k1gFnPc1	neštovice
</s>
</p>
<p>
<s>
žlutá	žlutý	k2eAgFnSc1d1	žlutá
zimnice	zimnice	k1gFnSc1	zimnice
</s>
</p>
<p>
<s>
rotavirové	rotavirový	k2eAgFnPc1d1	rotavirová
infekce	infekce	k1gFnPc1	infekce
</s>
</p>
<p>
<s>
chřipka	chřipka	k1gFnSc1	chřipka
(	(	kIx(	(
<g/>
intranasální	intranasální	k2eAgFnSc1d1	intranasální
aplikace	aplikace	k1gFnSc1	aplikace
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
poliomyelitida	poliomyelitida	k1gFnSc1	poliomyelitida
(	(	kIx(	(
<g/>
perorální	perorální	k2eAgFnSc1d1	perorální
vakcína	vakcína	k1gFnSc1	vakcína
–	–	k?	–
Sabinova	Sabinův	k2eAgFnSc1d1	Sabinova
vakcína	vakcína	k1gFnSc1	vakcína
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
(	(	kIx(	(
<g/>
BCG	BCG	kA	BCG
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
břišní	břišní	k2eAgInSc1d1	břišní
tyfus	tyfus	k1gInSc1	tyfus
(	(	kIx(	(
<g/>
perorální	perorální	k2eAgFnSc1d1	perorální
vakcína	vakcína	k1gFnSc1	vakcína
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Inaktivovaná	Inaktivovaný	k2eAgFnSc1d1	Inaktivovaná
vakcína	vakcína	k1gFnSc1	vakcína
===	===	k?	===
</s>
</p>
<p>
<s>
Inaktivované	Inaktivovaný	k2eAgFnPc1d1	Inaktivovaná
vakcíny	vakcína	k1gFnPc1	vakcína
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
mrtvé	mrtvý	k2eAgInPc4d1	mrtvý
viry	vir	k1gInPc4	vir
nebo	nebo	k8xC	nebo
bakterie	bakterie	k1gFnPc4	bakterie
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
celobuněčných	celobuněčný	k2eAgFnPc6d1	celobuněčná
vakcínách	vakcína	k1gFnPc6	vakcína
<g/>
.	.	kIx.	.
</s>
<s>
Organismy	organismus	k1gInPc1	organismus
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
ve	v	k7c6	v
vhodných	vhodný	k2eAgFnPc6d1	vhodná
kulturách	kultura	k1gFnPc6	kultura
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jsou	být	k5eAaImIp3nP	být
usmrceny	usmrcen	k2eAgFnPc1d1	usmrcena
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
teplem	teplo	k1gNnSc7	teplo
nebo	nebo	k8xC	nebo
chemikáliemi	chemikálie	k1gFnPc7	chemikálie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
usmrcení	usmrcení	k1gNnSc6	usmrcení
organismu	organismus	k1gInSc2	organismus
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
konformace	konformace	k1gFnSc2	konformace
proteinů	protein	k1gInPc2	protein
<g/>
;	;	kIx,	;
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
zachována	zachován	k2eAgFnSc1d1	zachována
účinnost	účinnost	k1gFnSc1	účinnost
vakcíny	vakcína	k1gFnSc2	vakcína
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
co	co	k3yInSc4	co
nejméně	málo	k6eAd3	málo
změněna	změněn	k2eAgFnSc1d1	změněna
antigenní	antigenní	k2eAgFnSc1d1	antigenní
struktura	struktura	k1gFnSc1	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
poměrně	poměrně	k6eAd1	poměrně
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
formaldehyd	formaldehyd	k1gInSc1	formaldehyd
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
zachování	zachování	k1gNnSc2	zachování
antigenní	antigenní	k2eAgFnSc2d1	antigenní
struktury	struktura	k1gFnSc2	struktura
proteinů	protein	k1gInPc2	protein
dostatečně	dostatečně	k6eAd1	dostatečně
šetrný	šetrný	k2eAgMnSc1d1	šetrný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
výhodou	výhoda	k1gFnSc7	výhoda
inaktivovaných	inaktivovaný	k2eAgFnPc2d1	inaktivovaná
vakcín	vakcína	k1gFnPc2	vakcína
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemohou	moct	k5eNaImIp3nP	moct
vyvolat	vyvolat	k5eAaPmF	vyvolat
infekci	infekce	k1gFnSc3	infekce
ani	ani	k8xC	ani
u	u	k7c2	u
imunokompromitovaných	imunokompromitovaný	k2eAgMnPc2d1	imunokompromitovaný
pacientů	pacient	k1gMnPc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
je	být	k5eAaImIp3nS	být
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
inaktivované	inaktivovaný	k2eAgFnPc4d1	inaktivovaná
vakcíny	vakcína	k1gFnPc4	vakcína
méně	málo	k6eAd2	málo
vydatná	vydatný	k2eAgFnSc1d1	vydatná
než	než	k8xS	než
na	na	k7c4	na
atenuovanou	atenuovaný	k2eAgFnSc4d1	atenuovaná
vakcínu	vakcína	k1gFnSc4	vakcína
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
atenuované	atenuovaný	k2eAgFnSc2d1	atenuovaná
vakcíny	vakcína	k1gFnSc2	vakcína
obvykle	obvykle	k6eAd1	obvykle
stačí	stačit	k5eAaBmIp3nS	stačit
jedna	jeden	k4xCgFnSc1	jeden
dávka	dávka	k1gFnSc1	dávka
k	k	k7c3	k
vyvolání	vyvolání	k1gNnSc3	vyvolání
dobré	dobrý	k2eAgFnSc2d1	dobrá
odpovědi	odpověď	k1gFnSc2	odpověď
<g/>
,	,	kIx,	,
u	u	k7c2	u
inaktivované	inaktivovaný	k2eAgFnSc2d1	inaktivovaná
vakcíny	vakcína	k1gFnSc2	vakcína
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
nutné	nutný	k2eAgNnSc1d1	nutné
druhé	druhý	k4xOgFnSc2	druhý
nebo	nebo	k8xC	nebo
i	i	k9	i
třetí	třetí	k4xOgFnPc4	třetí
dávky	dávka	k1gFnPc4	dávka
a	a	k8xC	a
případně	případně	k6eAd1	případně
i	i	k9	i
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
boost	boost	k1gInSc1	boost
<g/>
"	"	kIx"	"
dávky	dávka	k1gFnPc1	dávka
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
odstupem	odstup	k1gInSc7	odstup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
inaktivované	inaktivovaný	k2eAgFnPc4d1	inaktivovaná
vakcíny	vakcína	k1gFnPc4	vakcína
pro	pro	k7c4	pro
následující	následující	k2eAgNnSc4d1	následující
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
poliomyelitida	poliomyelitida	k1gFnSc1	poliomyelitida
</s>
</p>
<p>
<s>
hepatitida	hepatitida	k1gFnSc1	hepatitida
A	a	k9	a
</s>
</p>
<p>
<s>
vzteklina	vzteklina	k1gFnSc1	vzteklina
</s>
</p>
<p>
<s>
chřipka	chřipka	k1gFnSc1	chřipka
</s>
</p>
<p>
<s>
černý	černý	k2eAgInSc1d1	černý
kašel	kašel	k1gInSc1	kašel
</s>
</p>
<p>
<s>
břišní	břišní	k2eAgInSc1d1	břišní
tyfus	tyfus	k1gInSc1	tyfus
</s>
</p>
<p>
<s>
mor	mor	k1gInSc1	mor
</s>
</p>
<p>
<s>
===	===	k?	===
Toxoidová	Toxoidový	k2eAgFnSc1d1	Toxoidový
vakcína	vakcína	k1gFnSc1	vakcína
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
toxoidové	toxoidový	k2eAgFnPc4d1	toxoidový
vakcíny	vakcína	k1gFnPc4	vakcína
pro	pro	k7c4	pro
následující	následující	k2eAgNnSc4d1	následující
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
záškrt	záškrt	k1gInSc1	záškrt
</s>
</p>
<p>
<s>
tetanus	tetanus	k1gInSc1	tetanus
</s>
</p>
<p>
<s>
===	===	k?	===
Subjednotková	Subjednotkový	k2eAgFnSc1d1	Subjednotkový
vakcína	vakcína	k1gFnSc1	vakcína
===	===	k?	===
</s>
</p>
<p>
<s>
Subjednotková	Subjednotkový	k2eAgFnSc1d1	Subjednotkový
vakcína	vakcína	k1gFnSc1	vakcína
je	být	k5eAaImIp3nS	být
inaktivovaná	inaktivovaný	k2eAgFnSc1d1	inaktivovaná
vakcína	vakcína	k1gFnSc1	vakcína
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
fragmenty	fragment	k1gInPc1	fragment
mikroorganismu	mikroorganismus	k1gInSc2	mikroorganismus
<g/>
.	.	kIx.	.
</s>
<s>
Usmrcený	usmrcený	k2eAgInSc1d1	usmrcený
patogen	patogen	k1gInSc1	patogen
je	být	k5eAaImIp3nS	být
fragmentován	fragmentován	k2eAgInSc1d1	fragmentován
<g/>
,	,	kIx,	,
fragmenty	fragment	k1gInPc1	fragment
jsou	být	k5eAaImIp3nP	být
separovány	separován	k2eAgInPc1d1	separován
a	a	k8xC	a
ve	v	k7c6	v
vakcíně	vakcína	k1gFnSc6	vakcína
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
ty	ten	k3xDgInPc1	ten
fragmenty	fragment	k1gInPc1	fragment
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
významné	významný	k2eAgInPc1d1	významný
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
imunitní	imunitní	k2eAgFnSc2d1	imunitní
odpovědi	odpověď	k1gFnSc2	odpověď
na	na	k7c4	na
příslušný	příslušný	k2eAgInSc4d1	příslušný
mikroorganismus	mikroorganismus	k1gInSc4	mikroorganismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
subjednotkové	subjednotkový	k2eAgFnPc4d1	subjednotková
vakcíny	vakcína	k1gFnPc4	vakcína
pro	pro	k7c4	pro
následující	následující	k2eAgNnSc4d1	následující
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
hepatitida	hepatitida	k1gFnSc1	hepatitida
B	B	kA	B
</s>
</p>
<p>
<s>
chřipka	chřipka	k1gFnSc1	chřipka
</s>
</p>
<p>
<s>
černý	černý	k2eAgInSc1d1	černý
kašel	kašel	k1gInSc1	kašel
(	(	kIx(	(
<g/>
acelulární	acelulární	k2eAgFnSc1d1	acelulární
vakcína	vakcína	k1gFnSc1	vakcína
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
lidský	lidský	k2eAgInSc1d1	lidský
papilomavirus	papilomavirus	k1gInSc1	papilomavirus
(	(	kIx(	(
<g/>
HPV	HPV	kA	HPV
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
antrax	antrax	k1gInSc1	antrax
</s>
</p>
<p>
<s>
Lymská	Lymská	k1gFnSc1	Lymská
boreliózaZvláštním	boreliózaZvláštnět	k5eAaPmIp1nS	boreliózaZvláštnět
typem	typ	k1gInSc7	typ
podjednotkových	podjednotkův	k2eAgFnPc2d1	podjednotkův
vakcín	vakcína	k1gFnPc2	vakcína
jsou	být	k5eAaImIp3nP	být
vakcíny	vakcína	k1gFnPc4	vakcína
polysacharidové	polysacharidový	k2eAgFnPc4d1	polysacharidová
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
dětí	dítě	k1gFnPc2	dítě
mladších	mladý	k2eAgFnPc2d2	mladší
2	[number]	k4	2
let	léto	k1gNnPc2	léto
není	být	k5eNaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
schopen	schopen	k2eAgInSc1d1	schopen
tyto	tento	k3xDgFnPc4	tento
vakcíny	vakcína	k1gFnPc4	vakcína
zpracovat	zpracovat	k5eAaPmF	zpracovat
žádoucím	žádoucí	k2eAgInSc7d1	žádoucí
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
vakcinace	vakcinace	k1gFnSc1	vakcinace
obvykle	obvykle	k6eAd1	obvykle
nevede	vést	k5eNaImIp3nS	vést
k	k	k7c3	k
imunizaci	imunizace	k1gFnSc3	imunizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
polysacharidové	polysacharidový	k2eAgFnPc4d1	polysacharidová
vakcíny	vakcína	k1gFnPc4	vakcína
pro	pro	k7c4	pro
následující	následující	k2eAgNnSc4d1	následující
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
pneumokokové	pneumokokový	k2eAgFnPc1d1	pneumokoková
infekce	infekce	k1gFnPc1	infekce
</s>
</p>
<p>
<s>
meningokokové	meningokokový	k2eAgFnPc1d1	meningokoková
infekce	infekce	k1gFnPc1	infekce
</s>
</p>
<p>
<s>
břišní	břišní	k2eAgInSc1d1	břišní
tyfus	tyfus	k1gInSc1	tyfus
</s>
</p>
<p>
<s>
infekce	infekce	k1gFnPc1	infekce
Haemophilus	Haemophilus	k1gInSc1	Haemophilus
influenzae	influenza	k1gFnSc2	influenza
typu	typ	k1gInSc2	typ
b	b	k?	b
</s>
</p>
<p>
<s>
===	===	k?	===
Konjugovaná	konjugovaný	k2eAgFnSc1d1	konjugovaná
vakcína	vakcína	k1gFnSc1	vakcína
===	===	k?	===
</s>
</p>
<p>
<s>
Konjugovaná	konjugovaný	k2eAgFnSc1d1	konjugovaná
vakcína	vakcína	k1gFnSc1	vakcína
je	být	k5eAaImIp3nS	být
řešením	řešení	k1gNnSc7	řešení
problému	problém	k1gInSc2	problém
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
dětí	dítě	k1gFnPc2	dítě
do	do	k7c2	do
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
nemusí	muset	k5eNaImIp3nP	muset
adekvátním	adekvátní	k2eAgInSc7d1	adekvátní
způsobem	způsob	k1gInSc7	způsob
zpracovat	zpracovat	k5eAaPmF	zpracovat
sacharidové	sacharidový	k2eAgInPc4d1	sacharidový
antigeny	antigen	k1gInPc4	antigen
<g/>
.	.	kIx.	.
</s>
<s>
Řešením	řešení	k1gNnSc7	řešení
problému	problém	k1gInSc2	problém
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
chemická	chemický	k2eAgFnSc1d1	chemická
vazba	vazba	k1gFnSc1	vazba
sacharidu	sacharid	k1gInSc2	sacharid
na	na	k7c4	na
proteinový	proteinový	k2eAgInSc4d1	proteinový
nosič	nosič	k1gInSc4	nosič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
konjugované	konjugovaný	k2eAgFnPc4d1	konjugovaná
vakcíny	vakcína	k1gFnPc4	vakcína
pro	pro	k7c4	pro
následující	následující	k2eAgNnSc4d1	následující
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
meningokokové	meningokokový	k2eAgFnPc1d1	meningokoková
infekce	infekce	k1gFnPc1	infekce
</s>
</p>
<p>
<s>
infekce	infekce	k1gFnPc1	infekce
Haemophilus	Haemophilus	k1gInSc1	Haemophilus
influenzae	influenza	k1gFnSc2	influenza
typu	typ	k1gInSc2	typ
b	b	k?	b
</s>
</p>
<p>
<s>
pneumokokové	pneumokokový	k2eAgFnPc1d1	pneumokoková
infekce	infekce	k1gFnPc1	infekce
</s>
</p>
<p>
<s>
===	===	k?	===
Rekombinantní	Rekombinantní	k2eAgFnSc1d1	Rekombinantní
vakcína	vakcína	k1gFnSc1	vakcína
===	===	k?	===
</s>
</p>
<p>
<s>
Rekombinantní	Rekombinantní	k2eAgFnPc1d1	Rekombinantní
vakcíny	vakcína	k1gFnPc1	vakcína
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
především	především	k9	především
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
technologii	technologie	k1gFnSc6	technologie
výroby	výroba	k1gFnSc2	výroba
se	se	k3xPyFc4	se
neobjevuje	objevovat	k5eNaImIp3nS	objevovat
fáze	fáze	k1gFnSc1	fáze
pomnožování	pomnožování	k1gNnSc2	pomnožování
patogenu	patogen	k1gInSc2	patogen
<g/>
.	.	kIx.	.
</s>
<s>
Fragmenty	fragment	k1gInPc1	fragment
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
by	by	kYmCp3nP	by
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
podjednotkové	podjednotkový	k2eAgFnPc4d1	podjednotkový
vakcíny	vakcína	k1gFnPc4	vakcína
separovány	separován	k2eAgFnPc4d1	separována
z	z	k7c2	z
usmrcených	usmrcený	k2eAgInPc2d1	usmrcený
patogenů	patogen	k1gInPc2	patogen
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
produkovány	produkován	k2eAgFnPc1d1	produkována
metodami	metoda	k1gFnPc7	metoda
genetického	genetický	k2eAgNnSc2d1	genetické
inženýrství	inženýrství	k1gNnSc2	inženýrství
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
struktura	struktura	k1gFnSc1	struktura
fragmentu	fragment	k1gInSc2	fragment
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
získat	získat	k5eAaPmF	získat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
identifikován	identifikován	k2eAgInSc1d1	identifikován
gen	gen	k1gInSc1	gen
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
produkci	produkce	k1gFnSc4	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
gen	gen	k1gInSc1	gen
je	být	k5eAaImIp3nS	být
vnesen	vnést	k5eAaPmNgInS	vnést
do	do	k7c2	do
genomu	genom	k1gInSc2	genom
organismu	organismus	k1gInSc2	organismus
použitelného	použitelný	k2eAgInSc2d1	použitelný
v	v	k7c6	v
bioreaktoru	bioreaktor	k1gInSc6	bioreaktor
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
kvasinky	kvasinka	k1gFnSc2	kvasinka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
i	i	k9	i
o	o	k7c4	o
bakterie	bakterie	k1gFnPc4	bakterie
nebo	nebo	k8xC	nebo
tkáňové	tkáňový	k2eAgFnPc1d1	tkáňová
kultury	kultura	k1gFnPc1	kultura
savčích	savčí	k2eAgFnPc2d1	savčí
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bioreaktorech	bioreaktor	k1gInPc6	bioreaktor
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
produkovány	produkován	k2eAgInPc1d1	produkován
fragmenty	fragment	k1gInPc1	fragment
použitelné	použitelný	k2eAgInPc1d1	použitelný
k	k	k7c3	k
vakcinaci	vakcinace	k1gFnSc3	vakcinace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
rekombinantní	rekombinantní	k2eAgFnPc4d1	rekombinantní
vakcíny	vakcína	k1gFnPc4	vakcína
pro	pro	k7c4	pro
následující	následující	k2eAgNnSc4d1	následující
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
hepatitida	hepatitida	k1gFnSc1	hepatitida
B	B	kA	B
</s>
</p>
<p>
<s>
lidský	lidský	k2eAgMnSc1d1	lidský
papilomavirusGenetické	papilomavirusGenetický	k2eAgNnSc4d1	papilomavirusGenetický
inženýrství	inženýrství	k1gNnSc4	inženýrství
nabízí	nabízet	k5eAaImIp3nS	nabízet
ještě	ještě	k9	ještě
jednu	jeden	k4xCgFnSc4	jeden
možnost	možnost	k1gFnSc4	možnost
při	při	k7c6	při
produkci	produkce	k1gFnSc6	produkce
vakcín	vakcína	k1gFnPc2	vakcína
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
znalosti	znalost	k1gFnSc6	znalost
genomu	genom	k1gInSc2	genom
patogenního	patogenní	k2eAgInSc2d1	patogenní
organismu	organismus	k1gInSc2	organismus
lze	lze	k6eAd1	lze
totiž	totiž	k9	totiž
cílenou	cílený	k2eAgFnSc7d1	cílená
manipulací	manipulace	k1gFnSc7	manipulace
vyřadit	vyřadit	k5eAaPmF	vyřadit
geny	gen	k1gInPc4	gen
odpovědné	odpovědný	k2eAgInPc4d1	odpovědný
za	za	k7c4	za
virulenci	virulence	k1gFnSc4	virulence
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bude	být	k5eAaImBp3nS	být
vlastně	vlastně	k9	vlastně
oslabený	oslabený	k2eAgInSc1d1	oslabený
kmen	kmen	k1gInSc1	kmen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
plně	plně	k6eAd1	plně
životaschopný	životaschopný	k2eAgMnSc1d1	životaschopný
<g/>
,	,	kIx,	,
jen	jen	k9	jen
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
ani	ani	k8xC	ani
teoreticky	teoreticky	k6eAd1	teoreticky
vyvolat	vyvolat	k5eAaPmF	vyvolat
základní	základní	k2eAgNnSc4d1	základní
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
oslabený	oslabený	k2eAgInSc4d1	oslabený
kmen	kmen	k1gInSc4	kmen
Salmonela	salmonela	k1gFnSc1	salmonela
typhi	typhi	k1gNnPc2	typhi
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgInSc1d1	schopen
vyvolat	vyvolat	k5eAaPmF	vyvolat
břišní	břišní	k2eAgInSc4d1	břišní
tyfus	tyfus	k1gInSc4	tyfus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
modifikovaného	modifikovaný	k2eAgInSc2d1	modifikovaný
viru	vir	k1gInSc2	vir
chřipky	chřipka	k1gFnSc2	chřipka
je	být	k5eAaImIp3nS	být
strategie	strategie	k1gFnSc1	strategie
jiná	jiný	k2eAgFnSc1d1	jiná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vir	vir	k1gInSc1	vir
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
pomnožit	pomnožit	k5eAaPmF	pomnožit
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
hostitele	hostitel	k1gMnSc2	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Vir	vir	k1gInSc1	vir
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
modifikován	modifikovat	k5eAaBmNgInS	modifikovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
byl	být	k5eAaImAgInS	být
schopnen	schopnen	k1gInSc1	schopnen
pomnožit	pomnožit	k5eAaPmF	pomnožit
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
sliznici	sliznice	k1gFnSc6	sliznice
nosohltanu	nosohltan	k1gInSc2	nosohltan
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
schopen	schopen	k2eAgMnSc1d1	schopen
napadnout	napadnout	k5eAaPmF	napadnout
plíce	plíce	k1gFnPc4	plíce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pomocné	pomocný	k2eAgFnPc1d1	pomocná
látky	látka	k1gFnPc1	látka
ve	v	k7c6	v
vakcínách	vakcína	k1gFnPc6	vakcína
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
účinek	účinek	k1gInSc4	účinek
vakcíny	vakcína	k1gFnSc2	vakcína
je	být	k5eAaImIp3nS	být
podstatné	podstatný	k2eAgNnSc1d1	podstatné
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgInPc1	jaký
procesy	proces	k1gInPc1	proces
probíhají	probíhat	k5eAaImIp3nP	probíhat
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
podání	podání	k1gNnSc2	podání
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
vakcína	vakcína	k1gFnSc1	vakcína
zpracována	zpracovat	k5eAaPmNgFnS	zpracovat
příliš	příliš	k6eAd1	příliš
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
hrozí	hrozit	k5eAaImIp3nS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nedojde	dojít	k5eNaPmIp3nS	dojít
k	k	k7c3	k
dostatečnému	dostatečný	k2eAgInSc3d1	dostatečný
rozvoji	rozvoj	k1gInSc3	rozvoj
specifické	specifický	k2eAgFnSc2d1	specifická
imunity	imunita	k1gFnSc2	imunita
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
že	že	k8xS	že
očkování	očkování	k1gNnSc1	očkování
nebude	být	k5eNaImBp3nS	být
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
může	moct	k5eAaImIp3nS	moct
očkování	očkování	k1gNnSc1	očkování
selhat	selhat	k5eAaPmF	selhat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
očkování	očkování	k1gNnSc1	očkování
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
takový	takový	k3xDgInSc1	takový
typ	typ	k1gInSc1	typ
imunitní	imunitní	k2eAgFnSc2d1	imunitní
odpovědi	odpověď	k1gFnSc2	odpověď
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
není	být	k5eNaImIp3nS	být
účinný	účinný	k2eAgMnSc1d1	účinný
na	na	k7c4	na
příslušného	příslušný	k2eAgMnSc4d1	příslušný
živého	živý	k1gMnSc4	živý
patogenu	patogen	k1gInSc2	patogen
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
modifikaci	modifikace	k1gFnSc3	modifikace
odpovědi	odpověď	k1gFnSc2	odpověď
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
na	na	k7c4	na
vakcínu	vakcína	k1gFnSc4	vakcína
slouží	sloužit	k5eAaImIp3nS	sloužit
řada	řada	k1gFnSc1	řada
látek	látka	k1gFnPc2	látka
přidávaných	přidávaný	k2eAgFnPc2d1	přidávaná
do	do	k7c2	do
vakcíny	vakcína	k1gFnSc2	vakcína
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
adjuvans	adjuvans	k1gInSc1	adjuvans
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vakcinace	vakcinace	k1gFnPc1	vakcinace
proti	proti	k7c3	proti
neinfekčním	infekční	k2eNgNnPc3d1	neinfekční
onemocněním	onemocnění	k1gNnPc3	onemocnění
==	==	k?	==
</s>
</p>
<p>
<s>
Vakcinace	vakcinace	k1gFnSc1	vakcinace
jako	jako	k8xC	jako
cílené	cílený	k2eAgNnSc1d1	cílené
navození	navození	k1gNnSc1	navození
imunitní	imunitní	k2eAgFnSc2d1	imunitní
odpovědi	odpověď	k1gFnSc2	odpověď
proti	proti	k7c3	proti
určenému	určený	k2eAgInSc3d1	určený
antigenu	antigen	k1gInSc3	antigen
je	být	k5eAaImIp3nS	být
zásadním	zásadní	k2eAgInSc7d1	zásadní
nástrojem	nástroj	k1gInSc7	nástroj
prevence	prevence	k1gFnSc2	prevence
před	před	k7c7	před
infekčními	infekční	k2eAgFnPc7d1	infekční
chorobami	choroba	k1gFnPc7	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
naučit	naučit	k5eAaPmF	naučit
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
rozpoznávat	rozpoznávat	k5eAaImF	rozpoznávat
a	a	k8xC	a
neutralizovat	neutralizovat	k5eAaBmF	neutralizovat
některé	některý	k3yIgFnPc4	některý
látky	látka	k1gFnPc4	látka
ve	v	k7c6	v
vnitřním	vnitřní	k2eAgNnSc6d1	vnitřní
prostředí	prostředí	k1gNnSc6	prostředí
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
lákavá	lákavý	k2eAgFnSc1d1	lákavá
i	i	k9	i
pro	pro	k7c4	pro
lékaře	lékař	k1gMnPc4	lékař
a	a	k8xC	a
vědce	vědec	k1gMnPc4	vědec
pohybující	pohybující	k2eAgFnSc2d1	pohybující
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
oborech	obor	k1gInPc6	obor
medicíny	medicína	k1gFnSc2	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
probíhá	probíhat	k5eAaImIp3nS	probíhat
výzkum	výzkum	k1gInSc4	výzkum
využití	využití	k1gNnSc2	využití
vakcinace	vakcinace	k1gFnSc2	vakcinace
i	i	k9	i
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zdánlivě	zdánlivě	k6eAd1	zdánlivě
nemají	mít	k5eNaImIp3nP	mít
s	s	k7c7	s
imunitním	imunitní	k2eAgInSc7d1	imunitní
systémem	systém	k1gInSc7	systém
moc	moc	k6eAd1	moc
společných	společný	k2eAgInPc2d1	společný
rysů	rys	k1gInPc2	rys
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vakcinace	vakcinace	k1gFnSc1	vakcinace
a	a	k8xC	a
léčba	léčba	k1gFnSc1	léčba
závislostí	závislost	k1gFnSc7	závislost
===	===	k?	===
</s>
</p>
<p>
<s>
Molekuly	molekula	k1gFnPc4	molekula
většiny	většina	k1gFnSc2	většina
látek	látka	k1gFnPc2	látka
zneužívaných	zneužívaný	k2eAgFnPc2d1	zneužívaná
jako	jako	k9	jako
drogy	droga	k1gFnPc1	droga
jsou	být	k5eAaImIp3nP	být
příliš	příliš	k6eAd1	příliš
malé	malý	k2eAgFnPc1d1	malá
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
samy	sám	k3xTgInPc1	sám
o	o	k7c4	o
sobě	se	k3xPyFc3	se
vyvolaly	vyvolat	k5eAaPmAgFnP	vyvolat
imunitní	imunitní	k2eAgFnSc4d1	imunitní
odpověď	odpověď	k1gFnSc4	odpověď
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
možné	možný	k2eAgNnSc1d1	možné
je	on	k3xPp3gInPc4	on
navázat	navázat	k5eAaPmF	navázat
na	na	k7c4	na
proteinový	proteinový	k2eAgInSc4d1	proteinový
nosič	nosič	k1gInSc4	nosič
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
imunogenními	imunogenní	k2eAgNnPc7d1	imunogenní
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
pak	pak	k6eAd1	pak
takový	takový	k3xDgInSc4	takový
sloučenina	sloučenina	k1gFnSc1	sloučenina
indukuje	indukovat	k5eAaBmIp3nS	indukovat
tvorbu	tvorba	k1gFnSc4	tvorba
protilátek	protilátka	k1gFnPc2	protilátka
schopných	schopný	k2eAgMnPc2d1	schopný
vázat	vázat	k5eAaImF	vázat
i	i	k9	i
volné	volný	k2eAgFnPc4d1	volná
molekuly	molekula	k1gFnPc4	molekula
drogy	droga	k1gFnPc4	droga
<g/>
,	,	kIx,	,
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
očkovaného	očkovaný	k2eAgInSc2d1	očkovaný
velmi	velmi	k6eAd1	velmi
rychlou	rychlý	k2eAgFnSc4d1	rychlá
eliminaci	eliminace	k1gFnSc4	eliminace
podané	podaný	k2eAgFnSc2d1	podaná
drogy	droga	k1gFnSc2	droga
z	z	k7c2	z
krevního	krevní	k2eAgInSc2d1	krevní
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
omezen	omezit	k5eAaPmNgInS	omezit
i	i	k8xC	i
vlastní	vlastní	k2eAgInSc1d1	vlastní
účinek	účinek	k1gInSc1	účinek
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc4	vývoj
je	být	k5eAaImIp3nS	být
limitován	limitovat	k5eAaBmNgMnS	limitovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
molekuly	molekula	k1gFnPc1	molekula
jsou	být	k5eAaImIp3nP	být
příliš	příliš	k6eAd1	příliš
malé	malý	k2eAgInPc1d1	malý
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
proti	proti	k7c3	proti
nim	on	k3xPp3gMnPc3	on
existovala	existovat	k5eAaImAgFnS	existovat
protilátka	protilátka	k1gFnSc1	protilátka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
alkohol	alkohol	k1gInSc1	alkohol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
molekuly	molekula	k1gFnPc1	molekula
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
antigenní	antigenní	k2eAgFnSc2d1	antigenní
struktury	struktura	k1gFnSc2	struktura
značně	značně	k6eAd1	značně
nestabilní	stabilní	k2eNgFnSc2d1	nestabilní
(	(	kIx(	(
<g/>
kanabinoidy	kanabinoida	k1gFnSc2	kanabinoida
<g/>
)	)	kIx)	)
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
protilátky	protilátka	k1gFnPc1	protilátka
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
blokovat	blokovat	k5eAaImF	blokovat
i	i	k9	i
některé	některý	k3yIgFnPc4	některý
prospěšné	prospěšný	k2eAgFnPc4d1	prospěšná
látky	látka	k1gFnPc4	látka
(	(	kIx(	(
<g/>
kofein	kofein	k1gInSc1	kofein
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
žádnou	žádný	k3yNgFnSc4	žádný
převratnou	převratný	k2eAgFnSc4d1	převratná
novinku	novinka	k1gFnSc4	novinka
<g/>
,	,	kIx,	,
první	první	k4xOgInPc1	první
úspěšné	úspěšný	k2eAgInPc1d1	úspěšný
pokusy	pokus	k1gInPc1	pokus
s	s	k7c7	s
imunizací	imunizace	k1gFnSc7	imunizace
zvířete	zvíře	k1gNnSc2	zvíře
proti	proti	k7c3	proti
návykové	návykový	k2eAgFnSc3d1	návyková
látce	látka	k1gFnSc3	látka
se	se	k3xPyFc4	se
datují	datovat	k5eAaImIp3nP	datovat
do	do	k7c2	do
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
fázích	fáze	k1gFnPc6	fáze
výzkumu	výzkum	k1gInSc2	výzkum
očkovací	očkovací	k2eAgFnSc2d1	očkovací
látky	látka	k1gFnSc2	látka
navozující	navozující	k2eAgFnSc4d1	navozující
sníženou	snížený	k2eAgFnSc4d1	snížená
vnímavost	vnímavost	k1gFnSc4	vnímavost
k	k	k7c3	k
heroinu	heroin	k1gInSc3	heroin
<g/>
,	,	kIx,	,
morfiu	morfium	k1gNnSc6	morfium
<g/>
,	,	kIx,	,
metamfetaminu	metamfetamin	k1gInSc6	metamfetamin
<g/>
,	,	kIx,	,
nikotinu	nikotin	k1gInSc6	nikotin
a	a	k8xC	a
kokainu	kokain	k1gInSc6	kokain
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vakcinace	vakcinace	k1gFnSc1	vakcinace
a	a	k8xC	a
léčba	léčba	k1gFnSc1	léčba
hypertenze	hypertenze	k1gFnSc2	hypertenze
===	===	k?	===
</s>
</p>
<p>
<s>
Arteriální	arteriální	k2eAgFnSc1d1	arteriální
hypertenze	hypertenze	k1gFnSc1	hypertenze
je	být	k5eAaImIp3nS	být
multifaktoriální	multifaktoriální	k2eAgNnSc4d1	multifaktoriální
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
postupně	postupně	k6eAd1	postupně
k	k	k7c3	k
řadě	řada	k1gFnSc3	řada
závažných	závažný	k2eAgInPc2d1	závažný
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Zásadním	zásadní	k2eAgInSc7d1	zásadní
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
hypertenze	hypertenze	k1gFnSc1	hypertenze
je	být	k5eAaImIp3nS	být
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
subjektivní	subjektivní	k2eAgFnPc4d1	subjektivní
obtíže	obtíž	k1gFnPc4	obtíž
až	až	k9	až
v	v	k7c6	v
pokročilém	pokročilý	k2eAgNnSc6d1	pokročilé
stádiu	stádium	k1gNnSc6	stádium
poškození	poškození	k1gNnSc2	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stádiu	stádium	k1gNnSc6	stádium
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
lze	lze	k6eAd1	lze
poškození	poškození	k1gNnSc4	poškození
bránit	bránit	k5eAaImF	bránit
<g/>
,	,	kIx,	,
nemocný	mocný	k2eNgMnSc1d1	nemocný
obvykle	obvykle	k6eAd1	obvykle
obtíže	obtíž	k1gFnPc4	obtíž
nemá	mít	k5eNaImIp3nS	mít
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
závažným	závažný	k2eAgInSc7d1	závažný
problémem	problém	k1gInSc7	problém
ochota	ochota	k1gFnSc1	ochota
nemocného	nemocný	k1gMnSc2	nemocný
užívat	užívat	k5eAaImF	užívat
medikaci	medikace	k1gFnSc4	medikace
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
medikace	medikace	k1gFnSc1	medikace
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
obvykle	obvykle	k6eAd1	obvykle
do	do	k7c2	do
osy	osa	k1gFnSc2	osa
renin-angiotenzin-aldosteron	reninngiotenzinldosteron	k1gInSc1	renin-angiotenzin-aldosteron
<g/>
,	,	kIx,	,
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
se	se	k3xPyFc4	se
o	o	k7c6	o
očkování	očkování	k1gNnSc6	očkování
proti	proti	k7c3	proti
některým	některý	k3yIgInPc3	některý
klíčovým	klíčový	k2eAgInPc3d1	klíčový
receptorům	receptor	k1gInPc3	receptor
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Vakcíny	vakcína	k1gFnPc1	vakcína
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
časného	časný	k2eAgInSc2d1	časný
výzkumu	výzkum	k1gInSc2	výzkum
na	na	k7c6	na
zvířatech	zvíře	k1gNnPc6	zvíře
a	a	k8xC	a
na	na	k7c6	na
lidských	lidský	k2eAgMnPc6d1	lidský
dobrovolnících	dobrovolník	k1gMnPc6	dobrovolník
<g/>
,	,	kIx,	,
výsledky	výsledek	k1gInPc1	výsledek
jsou	být	k5eAaImIp3nP	být
prozatím	prozatím	k6eAd1	prozatím
spíše	spíše	k9	spíše
povzbudivé	povzbudivý	k2eAgNnSc1d1	povzbudivé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vedlejší	vedlejší	k2eAgInPc1d1	vedlejší
účinky	účinek	k1gInPc1	účinek
očkování	očkování	k1gNnSc2	očkování
==	==	k?	==
</s>
</p>
<p>
<s>
Očkování	očkování	k1gNnSc1	očkování
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
některé	některý	k3yIgInPc4	některý
nezamýšlené	zamýšlený	k2eNgInPc4d1	nezamýšlený
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
zdraví	zdraví	k1gNnSc4	zdraví
člověka	člověk	k1gMnSc2	člověk
prospěšné	prospěšný	k2eAgNnSc4d1	prospěšné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
vznikem	vznik	k1gInSc7	vznik
melanomu	melanom	k1gInSc2	melanom
===	===	k?	===
</s>
</p>
<p>
<s>
Zajímavou	zajímavý	k2eAgFnSc7d1	zajímavá
oblastí	oblast	k1gFnSc7	oblast
je	být	k5eAaImIp3nS	být
vztah	vztah	k1gInSc1	vztah
očkování	očkování	k1gNnSc2	očkování
proti	proti	k7c3	proti
tuberkulóze	tuberkulóza	k1gFnSc3	tuberkulóza
a	a	k8xC	a
proti	proti	k7c3	proti
planým	planý	k2eAgFnPc3d1	planá
neštovicím	neštovice	k1gFnPc3	neštovice
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
tato	tento	k3xDgNnPc1	tento
očkování	očkování	k1gNnPc1	očkování
představují	představovat	k5eAaImIp3nP	představovat
protektivní	protektivní	k2eAgInSc4d1	protektivní
faktor	faktor	k1gInSc4	faktor
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
očkovaných	očkovaný	k2eAgFnPc2d1	očkovaná
se	se	k3xPyFc4	se
melanom	melanom	k1gInSc1	melanom
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
méně	málo	k6eAd2	málo
než	než	k8xS	než
u	u	k7c2	u
neoočkovaných	oočkovaný	k2eNgInPc2d1	oočkovaný
<g/>
.	.	kIx.	.
</s>
<s>
Molekulárním	molekulární	k2eAgInSc7d1	molekulární
podkladem	podklad	k1gInSc7	podklad
této	tento	k3xDgFnSc2	tento
ochrany	ochrana	k1gFnSc2	ochrana
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obě	dva	k4xCgFnPc1	dva
tato	tento	k3xDgNnPc1	tento
očkování	očkování	k1gNnPc4	očkování
navozují	navozovat	k5eAaImIp3nP	navozovat
jako	jako	k9	jako
vedlejší	vedlejší	k2eAgInSc4d1	vedlejší
efekt	efekt	k1gInSc4	efekt
imunitní	imunitní	k2eAgFnSc4d1	imunitní
odpověď	odpověď	k1gFnSc4	odpověď
proti	proti	k7c3	proti
antigenu	antigen	k1gInSc3	antigen
HERV-K-MEL	HERV-K-MEL	k1gFnSc2	HERV-K-MEL
<g/>
.	.	kIx.	.
<g/>
Například	například	k6eAd1	například
Krone	Kron	k1gMnSc5	Kron
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
ve	v	k7c6	v
studii	studie	k1gFnSc6	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
hodnotili	hodnotit	k5eAaImAgMnP	hodnotit
vztah	vztah	k1gInSc4	vztah
očkování	očkování	k1gNnSc2	očkování
proti	proti	k7c3	proti
planým	planý	k2eAgFnPc3d1	planá
neštovicím	neštovice	k1gFnPc3	neštovice
a	a	k8xC	a
tuberkulóze	tuberkulóza	k1gFnSc3	tuberkulóza
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
prodělaného	prodělaný	k2eAgNnSc2d1	prodělané
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
,	,	kIx,	,
a	a	k8xC	a
melanomu	melanom	k1gInSc2	melanom
<g/>
.	.	kIx.	.
</s>
<s>
Svá	svůj	k3xOyFgNnPc4	svůj
zjištění	zjištění	k1gNnPc4	zjištění
uzavírají	uzavírat	k5eAaImIp3nP	uzavírat
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
obě	dva	k4xCgFnPc1	dva
vakcíny	vakcína	k1gFnPc1	vakcína
a	a	k8xC	a
také	také	k9	také
prodělání	prodělání	k1gNnSc1	prodělání
těžkého	těžký	k2eAgNnSc2d1	těžké
infekčního	infekční	k2eAgNnSc2d1	infekční
onemocnění	onemocnění	k1gNnSc2	onemocnění
indukuje	indukovat	k5eAaBmIp3nS	indukovat
ty	ten	k3xDgInPc1	ten
samé	samý	k3xTgInPc1	samý
protektivní	protektivní	k2eAgInPc1d1	protektivní
mechanismy	mechanismus	k1gInPc1	mechanismus
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
výrazně	výrazně	k6eAd1	výrazně
snižují	snižovat	k5eAaImIp3nP	snižovat
riziko	riziko	k1gNnSc4	riziko
vzniku	vznik	k1gInSc2	vznik
nádoru	nádor	k1gInSc2	nádor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
populační	populační	k2eAgFnSc6d1	populační
úrovni	úroveň	k1gFnSc6	úroveň
je	být	k5eAaImIp3nS	být
významnější	významný	k2eAgInSc4d2	významnější
vliv	vliv	k1gInSc4	vliv
očkování	očkování	k1gNnSc2	očkování
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
autoři	autor	k1gMnPc1	autor
doporučují	doporučovat	k5eAaImIp3nP	doporučovat
přehodnotit	přehodnotit	k5eAaPmF	přehodnotit
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
ústupu	ústup	k1gInSc3	ústup
od	od	k7c2	od
plošné	plošný	k2eAgFnSc2d1	plošná
vakcinace	vakcinace	k1gFnSc2	vakcinace
těmito	tento	k3xDgFnPc7	tento
vakcínami	vakcína	k1gFnPc7	vakcína
<g/>
.	.	kIx.	.
<g/>
Mastrangelo	Mastrangela	k1gFnSc5	Mastrangela
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
studii	studie	k1gFnSc6	studie
publikované	publikovaný	k2eAgFnSc6d1	publikovaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
analyzují	analyzovat	k5eAaImIp3nP	analyzovat
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
melanomem	melanom	k1gInSc7	melanom
a	a	k8xC	a
očkováním	očkování	k1gNnSc7	očkování
proti	proti	k7c3	proti
žluté	žlutý	k2eAgFnSc3d1	žlutá
zimnici	zimnice	k1gFnSc3	zimnice
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
strukturní	strukturní	k2eAgFnSc2d1	strukturní
analogie	analogie	k1gFnSc2	analogie
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
i	i	k9	i
toto	tento	k3xDgNnSc1	tento
očkování	očkování	k1gNnSc1	očkování
mohlo	moct	k5eAaImAgNnS	moct
poskytovat	poskytovat	k5eAaImF	poskytovat
ochranu	ochrana	k1gFnSc4	ochrana
proti	proti	k7c3	proti
melanomu	melanom	k1gInSc3	melanom
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInSc2	jejich
závěru	závěr	k1gInSc2	závěr
data	datum	k1gNnSc2	datum
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
očkování	očkování	k1gNnSc1	očkování
proti	proti	k7c3	proti
žluté	žlutý	k2eAgFnSc3d1	žlutá
zimnici	zimnice	k1gFnSc3	zimnice
také	také	k9	také
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
.	.	kIx.	.
<g/>
Hodges-Vazquez	Hodges-Vazquez	k1gMnSc1	Hodges-Vazquez
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
studii	studie	k1gFnSc6	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
studovali	studovat	k5eAaImAgMnP	studovat
vztah	vztah	k1gInSc4	vztah
melanomu	melanom	k1gInSc2	melanom
a	a	k8xC	a
očkování	očkování	k1gNnSc2	očkování
proti	proti	k7c3	proti
žluté	žlutý	k2eAgFnSc3d1	žlutá
zimnici	zimnice	k1gFnSc3	zimnice
u	u	k7c2	u
vojáků	voják	k1gMnPc2	voják
armády	armáda	k1gFnSc2	armáda
USA	USA	kA	USA
dospěli	dochvít	k5eAaPmAgMnP	dochvít
k	k	k7c3	k
opačnému	opačný	k2eAgInSc3d1	opačný
závěru	závěr	k1gInSc3	závěr
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
desetiletého	desetiletý	k2eAgNnSc2d1	desetileté
sledování	sledování	k1gNnSc2	sledování
neprokázali	prokázat	k5eNaPmAgMnP	prokázat
statisticky	statisticky	k6eAd1	statisticky
významnou	významný	k2eAgFnSc4d1	významná
vazbu	vazba	k1gFnSc4	vazba
mezi	mezi	k7c7	mezi
očkováním	očkování	k1gNnSc7	očkování
a	a	k8xC	a
výskytem	výskyt	k1gInSc7	výskyt
melanomu	melanom	k1gInSc2	melanom
a	a	k8xC	a
svá	svůj	k3xOyFgNnPc4	svůj
zjištění	zjištění	k1gNnPc4	zjištění
uzavírají	uzavírat	k5eAaPmIp3nP	uzavírat
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
melanomem	melanom	k1gInSc7	melanom
a	a	k8xC	a
očkováním	očkování	k1gNnSc7	očkování
proti	proti	k7c3	proti
žluté	žlutý	k2eAgFnSc3d1	žlutá
zimnici	zimnice	k1gFnSc3	zimnice
není	být	k5eNaImIp3nS	být
vazba	vazba	k1gFnSc1	vazba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nežádoucí	žádoucí	k2eNgInPc1d1	nežádoucí
účinky	účinek	k1gInPc1	účinek
očkování	očkování	k1gNnSc2	očkování
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
každý	každý	k3xTgInSc1	každý
lék	lék	k1gInSc1	lék
<g/>
,	,	kIx,	,
i	i	k8xC	i
očkování	očkování	k1gNnSc1	očkování
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
na	na	k7c4	na
očkovaného	očkovaný	k2eAgInSc2d1	očkovaný
nežádoucí	žádoucí	k2eNgInPc4d1	nežádoucí
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Očekávané	očekávaný	k2eAgInPc1d1	očekávaný
nežádoucí	žádoucí	k2eNgInPc1d1	nežádoucí
účinky	účinek	k1gInPc1	účinek
vycházejí	vycházet	k5eAaImIp3nP	vycházet
ze	z	k7c2	z
samé	samý	k3xTgFnSc2	samý
podstaty	podstata	k1gFnSc2	podstata
vakcíny	vakcína	k1gFnSc2	vakcína
a	a	k8xC	a
biologie	biologie	k1gFnSc2	biologie
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc4	takový
účinky	účinek	k1gInPc4	účinek
lze	lze	k6eAd1	lze
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
často	často	k6eAd1	často
jen	jen	k9	jen
ve	v	k7c6	v
zcela	zcela	k6eAd1	zcela
nenápadné	nápadný	k2eNgFnSc6d1	nenápadná
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
v	v	k7c6	v
případě	případ	k1gInSc6	případ
podávaní	podávaný	k2eAgMnPc1d1	podávaný
nedostatečně	dostatečně	k6eNd1	dostatečně
oslabených	oslabený	k2eAgFnPc2d1	oslabená
vakcín	vakcína	k1gFnPc2	vakcína
(	(	kIx(	(
<g/>
popsáno	popsat	k5eAaPmNgNnS	popsat
u	u	k7c2	u
Sabinovy	Sabinův	k2eAgFnSc2d1	Sabinova
vakcíny	vakcína	k1gFnSc2	vakcína
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
u	u	k7c2	u
historické	historický	k2eAgFnSc2d1	historická
variolizace	variolizace	k1gFnSc2	variolizace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
takovým	takový	k3xDgInSc7	takový
nežádoucím	žádoucí	k2eNgInSc7d1	nežádoucí
účinkem	účinek	k1gInSc7	účinek
vyvolání	vyvolání	k1gNnSc2	vyvolání
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
očkuje	očkovat	k5eAaImIp3nS	očkovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
spolehlivě	spolehlivě	k6eAd1	spolehlivě
atenuovaných	atenuovaný	k2eAgInPc2d1	atenuovaný
kmenů	kmen	k1gInPc2	kmen
a	a	k8xC	a
u	u	k7c2	u
mrtvých	mrtvý	k2eAgFnPc2d1	mrtvá
vakcín	vakcína	k1gFnPc2	vakcína
přicházejí	přicházet	k5eAaImIp3nP	přicházet
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
nežádoucí	žádoucí	k2eNgInPc4d1	nežádoucí
účinky	účinek	k1gInPc4	účinek
vyvolané	vyvolaný	k2eAgInPc4d1	vyvolaný
samotným	samotný	k2eAgInSc7d1	samotný
aktem	akt	k1gInSc7	akt
očkování	očkování	k1gNnSc2	očkování
(	(	kIx(	(
<g/>
vpich	vpich	k1gInSc1	vpich
injekce	injekce	k1gFnSc2	injekce
<g/>
)	)	kIx)	)
a	a	k8xC	a
stimulace	stimulace	k1gFnSc1	stimulace
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
reakce	reakce	k1gFnPc1	reakce
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
přechodné	přechodný	k2eAgInPc4d1	přechodný
<g/>
,	,	kIx,	,
nevýrazné	výrazný	k2eNgInPc4d1	nevýrazný
a	a	k8xC	a
neškodné	škodný	k2eNgInPc4d1	neškodný
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
sem	sem	k6eAd1	sem
zejména	zejména	k9	zejména
zarudnutí	zarudnutí	k1gNnSc1	zarudnutí
a	a	k8xC	a
bolestivost	bolestivost	k1gFnSc1	bolestivost
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
vpichu	vpich	k1gInSc2	vpich
a	a	k8xC	a
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
u	u	k7c2	u
malých	malý	k2eAgFnPc2d1	malá
dětí	dítě	k1gFnPc2	dítě
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
může	moct	k5eAaImIp3nS	moct
projevit	projevit	k5eAaPmF	projevit
i	i	k9	i
přechodným	přechodný	k2eAgInSc7d1	přechodný
neklidem	neklid	k1gInSc7	neklid
<g/>
,	,	kIx,	,
výraznějším	výrazný	k2eAgInSc7d2	výraznější
pláčem	pláč	k1gInSc7	pláč
nebo	nebo	k8xC	nebo
poruchou	porucha	k1gFnSc7	porucha
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nežádoucí	žádoucí	k2eNgInPc1d1	nežádoucí
účinky	účinek	k1gInPc1	účinek
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
chybné	chybný	k2eAgFnSc2d1	chybná
aplikace	aplikace	k1gFnSc2	aplikace
představují	představovat	k5eAaImIp3nP	představovat
chybu	chyba	k1gFnSc4	chyba
zdravotníka	zdravotník	k1gMnSc2	zdravotník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
aplikuje	aplikovat	k5eAaBmIp3nS	aplikovat
očkovací	očkovací	k2eAgFnSc4d1	očkovací
látku	látka	k1gFnSc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Chybné	chybný	k2eAgNnSc1d1	chybné
podání	podání	k1gNnSc1	podání
látky	látka	k1gFnSc2	látka
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
rychlému	rychlý	k2eAgNnSc3d1	rychlé
rozptýlení	rozptýlení	k1gNnSc3	rozptýlení
dávky	dávka	k1gFnSc2	dávka
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
k	k	k7c3	k
navození	navození	k1gNnSc3	navození
nedostatečné	dostatečný	k2eNgFnSc2d1	nedostatečná
imunitní	imunitní	k2eAgFnSc2d1	imunitní
odpovědi	odpověď	k1gFnSc2	odpověď
<g/>
.	.	kIx.	.
</s>
<s>
Vážné	vážný	k2eAgFnPc1d1	vážná
komplikace	komplikace	k1gFnPc1	komplikace
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
např.	např.	kA	např.
embolizace	embolizace	k1gFnSc2	embolizace
látky	látka	k1gFnSc2	látka
podané	podaný	k2eAgFnPc4d1	podaná
nitrožilně	nitrožilně	k6eAd1	nitrožilně
nebo	nebo	k8xC	nebo
infekce	infekce	k1gFnSc2	infekce
zanesené	zanesený	k2eAgFnSc2d1	zanesená
nesterilním	sterilní	k2eNgNnSc7d1	nesterilní
podáním	podání	k1gNnSc7	podání
jsou	být	k5eAaImIp3nP	být
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
vzácné	vzácný	k2eAgNnSc1d1	vzácné
je	být	k5eAaImIp3nS	být
i	i	k9	i
neúmyslné	úmyslný	k2eNgNnSc1d1	neúmyslné
usmrcení	usmrcení	k1gNnSc1	usmrcení
živé	živý	k2eAgFnSc2d1	živá
vakcíny	vakcína	k1gFnSc2	vakcína
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pak	pak	k6eAd1	pak
nevede	vést	k5eNaImIp3nS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
imunity	imunita	k1gFnSc2	imunita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nežádoucí	žádoucí	k2eNgInPc1d1	nežádoucí
účinky	účinek	k1gInPc1	účinek
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
očkovaného	očkovaný	k2eAgInSc2d1	očkovaný
jsou	být	k5eAaImIp3nP	být
takové	takový	k3xDgInPc4	takový
nežádoucí	žádoucí	k2eNgInPc4d1	nežádoucí
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
specifickým	specifický	k2eAgInSc7d1	specifický
stavem	stav	k1gInSc7	stav
pacienta	pacient	k1gMnSc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Nejzávažnějším	závažný	k2eAgInSc7d3	nejzávažnější
problémem	problém	k1gInSc7	problém
jsou	být	k5eAaImIp3nP	být
odchylné	odchylný	k2eAgInPc1d1	odchylný
stavy	stav	k1gInPc1	stav
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
očkovaného	očkovaný	k2eAgInSc2d1	očkovaný
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
nerozpoznaný	rozpoznaný	k2eNgInSc4d1	nerozpoznaný
vrozený	vrozený	k2eAgInSc4d1	vrozený
či	či	k8xC	či
získaný	získaný	k2eAgInSc4d1	získaný
imunodeficit	imunodeficita	k1gFnPc2	imunodeficita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
může	moct	k5eAaImIp3nS	moct
i	i	k9	i
podání	podání	k1gNnSc4	podání
oslabené	oslabený	k2eAgFnSc2d1	oslabená
vakcíny	vakcína	k1gFnSc2	vakcína
vyvolat	vyvolat	k5eAaPmF	vyvolat
vážné	vážný	k2eAgNnSc4d1	vážné
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Obvyklejší	obvyklý	k2eAgFnSc1d2	obvyklejší
je	být	k5eAaImIp3nS	být
nedostatečná	dostatečný	k2eNgFnSc1d1	nedostatečná
odpověď	odpověď	k1gFnSc1	odpověď
na	na	k7c4	na
vakcínu	vakcína	k1gFnSc4	vakcína
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
aktuálního	aktuální	k2eAgInSc2d1	aktuální
stavu	stav	k1gInSc2	stav
nebo	nebo	k8xC	nebo
vrozené	vrozený	k2eAgFnSc2d1	vrozená
neschopnosti	neschopnost	k1gFnSc2	neschopnost
prezentovat	prezentovat	k5eAaBmF	prezentovat
určité	určitý	k2eAgInPc4d1	určitý
konkrétní	konkrétní	k2eAgInPc4d1	konkrétní
antigeny	antigen	k1gInPc4	antigen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nežádoucí	žádoucí	k2eNgInPc1d1	nežádoucí
účinky	účinek	k1gInPc1	účinek
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
technologické	technologický	k2eAgFnSc2d1	technologická
chyby	chyba	k1gFnSc2	chyba
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
velmi	velmi	k6eAd1	velmi
závažné	závažný	k2eAgInPc1d1	závažný
<g/>
,	,	kIx,	,
naštěstí	naštěstí	k6eAd1	naštěstí
výjimečné	výjimečný	k2eAgFnPc1d1	výjimečná
<g/>
.	.	kIx.	.
</s>
<s>
Podaná	podaný	k2eAgFnSc1d1	podaná
vakcína	vakcína	k1gFnSc1	vakcína
je	být	k5eAaImIp3nS	být
kontaminována	kontaminovat	k5eAaBmNgFnS	kontaminovat
příměsí	příměs	k1gFnSc7	příměs
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
značně	značně	k6eAd1	značně
škodlivá	škodlivý	k2eAgFnSc1d1	škodlivá
buď	buď	k8xC	buď
vlastní	vlastní	k2eAgFnSc1d1	vlastní
toxicitou	toxicita	k1gFnSc7	toxicita
nebo	nebo	k8xC	nebo
schopností	schopnost	k1gFnSc7	schopnost
indukovat	indukovat	k5eAaBmF	indukovat
autoimunitní	autoimunitní	k2eAgFnSc4d1	autoimunitní
odpověď	odpověď	k1gFnSc4	odpověď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nežádoucí	žádoucí	k2eNgInPc1d1	nežádoucí
účinky	účinek	k1gInPc1	účinek
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
zkřížené	zkřížený	k2eAgFnSc2d1	zkřížená
reaktivity	reaktivita	k1gFnSc2	reaktivita
jsou	být	k5eAaImIp3nP	být
chybou	chyba	k1gFnSc7	chyba
nedostatečného	dostatečný	k2eNgNnSc2d1	nedostatečné
testování	testování	k1gNnSc2	testování
<g/>
.	.	kIx.	.
</s>
<s>
Zpracováním	zpracování	k1gNnSc7	zpracování
očkovací	očkovací	k2eAgFnSc2d1	očkovací
látky	látka	k1gFnSc2	látka
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
lehce	lehko	k6eAd1	lehko
změnit	změnit	k5eAaPmF	změnit
antigenní	antigenní	k2eAgFnSc1d1	antigenní
struktura	struktura	k1gFnSc1	struktura
a	a	k8xC	a
podaná	podaný	k2eAgFnSc1d1	podaná
vakcína	vakcína	k1gFnSc1	vakcína
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
produkci	produkce	k1gFnSc3	produkce
protilátek	protilátka	k1gFnPc2	protilátka
proti	proti	k7c3	proti
vlastním	vlastní	k2eAgInPc3d1	vlastní
antigenům	antigen	k1gInPc3	antigen
nemocného	nemocný	k1gMnSc2	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
otázkou	otázka	k1gFnSc7	otázka
<g/>
,	,	kIx,	,
nakolik	nakolik	k6eAd1	nakolik
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nežádoucí	žádoucí	k2eNgInSc4d1	nežádoucí
účinek	účinek	k1gInSc4	účinek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tentýž	týž	k3xTgInSc1	týž
projev	projev	k1gInSc1	projev
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
prodělání	prodělání	k1gNnSc4	prodělání
infekce	infekce	k1gFnSc2	infekce
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
které	který	k3yQgFnSc3	který
se	se	k3xPyFc4	se
očkuje	očkovat	k5eAaImIp3nS	očkovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Očkování	očkování	k1gNnSc1	očkování
jako	jako	k8xC	jako
spouštěcí	spouštěcí	k2eAgInSc1d1	spouštěcí
faktor	faktor	k1gInSc1	faktor
onemocnění	onemocnění	k1gNnSc2	onemocnění
===	===	k?	===
</s>
</p>
<p>
<s>
Někteří	některý	k3yIgMnPc1	některý
odpůrci	odpůrce	k1gMnPc1	odpůrce
očkování	očkování	k1gNnPc2	očkování
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
očkování	očkování	k1gNnSc1	očkování
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
spouštěcím	spouštěcí	k2eAgInSc7d1	spouštěcí
faktorem	faktor	k1gInSc7	faktor
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
pozadí	pozadí	k1gNnSc6	pozadí
stojí	stát	k5eAaImIp3nP	stát
patologicky	patologicky	k6eAd1	patologicky
probíhající	probíhající	k2eAgFnPc1d1	probíhající
reakce	reakce	k1gFnPc1	reakce
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
obavy	obava	k1gFnPc1	obava
mají	mít	k5eAaImIp3nP	mít
racionální	racionální	k2eAgNnSc4d1	racionální
jádro	jádro	k1gNnSc4	jádro
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
objevily	objevit	k5eAaPmAgInP	objevit
případy	případ	k1gInPc1	případ
vakcín	vakcína	k1gFnPc2	vakcína
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vlivem	vlivem	k7c2	vlivem
technologicky	technologicky	k6eAd1	technologicky
nezvládnuté	zvládnutý	k2eNgFnSc2d1	nezvládnutá
výroby	výroba	k1gFnSc2	výroba
nebo	nebo	k8xC	nebo
neočekávané	očekávaný	k2eNgFnSc2d1	neočekávaná
antigenní	antigenní	k2eAgFnSc2d1	antigenní
podobnosti	podobnost	k1gFnSc2	podobnost
inaktivované	inaktivovaný	k2eAgFnSc2d1	inaktivovaná
vakcíny	vakcína	k1gFnSc2	vakcína
s	s	k7c7	s
vlastními	vlastní	k2eAgFnPc7d1	vlastní
lidskými	lidský	k2eAgFnPc7d1	lidská
antigeny	antigen	k1gInPc4	antigen
vedly	vést	k5eAaImAgInP	vést
ke	k	k7c3	k
spuštění	spuštění	k1gNnSc3	spuštění
závažného	závažný	k2eAgNnSc2d1	závažné
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Očkování	očkování	k1gNnSc1	očkování
proti	proti	k7c3	proti
chřipce	chřipka	k1gFnSc3	chřipka
a	a	k8xC	a
Guillian-Barré	Guillian-Barrý	k2eAgInPc1d1	Guillian-Barrý
syndrom	syndrom	k1gInSc1	syndrom
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
USA	USA	kA	USA
použito	použít	k5eAaPmNgNnS	použít
očkování	očkování	k1gNnSc1	očkování
proti	proti	k7c3	proti
viru	vir	k1gInSc3	vir
chřipky	chřipka	k1gFnSc2	chřipka
typu	typ	k1gInSc2	typ
A	a	k9	a
H	H	kA	H
<g/>
14	[number]	k4	14
<g/>
N	N	kA	N
<g/>
1	[number]	k4	1
<g/>
*	*	kIx~	*
<g/>
NJ	NJ	kA	NJ
<g/>
/	/	kIx~	/
<g/>
76	[number]	k4	76
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
představovalo	představovat	k5eAaImAgNnS	představovat
významný	významný	k2eAgInSc4d1	významný
faktor	faktor	k1gInSc4	faktor
<g/>
,	,	kIx,	,
vyvolával	vyvolávat	k5eAaImAgInS	vyvolávat
závažný	závažný	k2eAgInSc1d1	závažný
Guillain-Barré	Guillain-Barrý	k2eAgFnSc2d1	Guillain-Barrý
syndrom	syndrom	k1gInSc4	syndrom
<g/>
.	.	kIx.	.
</s>
<s>
Molekulární	molekulární	k2eAgFnSc7d1	molekulární
podstatou	podstata	k1gFnSc7	podstata
byla	být	k5eAaImAgFnS	být
indukce	indukce	k1gFnSc1	indukce
protilátek	protilátka	k1gFnPc2	protilátka
proti	proti	k7c3	proti
gangliosidům	gangliosid	k1gMnPc3	gangliosid
GM	GM	kA	GM
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
byl	být	k5eAaImAgInS	být
jediný	jediný	k2eAgInSc1d1	jediný
případ	případ	k1gInSc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vakcinace	vakcinace	k1gFnSc1	vakcinace
proti	proti	k7c3	proti
chřipce	chřipka	k1gFnSc3	chřipka
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
toto	tento	k3xDgNnSc4	tento
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vellozzi	Vellozh	k1gMnPc1	Vellozh
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
studii	studie	k1gFnSc6	studie
publikované	publikovaný	k2eAgFnSc6d1	publikovaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
dokonce	dokonce	k9	dokonce
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
se	se	k3xPyFc4	se
srovnají	srovnat	k5eAaPmIp3nP	srovnat
očkovaná	očkovaný	k2eAgFnSc1d1	očkovaná
a	a	k8xC	a
neočkovaná	očkovaný	k2eNgFnSc1d1	neočkovaná
populace	populace	k1gFnSc1	populace
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
očkovaní	očkovaný	k2eAgMnPc1d1	očkovaný
menší	malý	k2eAgFnPc4d2	menší
četnost	četnost	k1gFnSc4	četnost
výskytu	výskyt	k1gInSc2	výskyt
Guillian-Barré	Guillian-Barrý	k2eAgNnSc1d1	Guillian-Barrý
syndromu	syndrom	k1gInSc3	syndrom
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
očkování	očkování	k1gNnSc1	očkování
proti	proti	k7c3	proti
chřipce	chřipka	k1gFnSc3	chřipka
mohlo	moct	k5eAaImAgNnS	moct
naopak	naopak	k6eAd1	naopak
před	před	k7c7	před
tímto	tento	k3xDgNnSc7	tento
onemocněním	onemocnění	k1gNnSc7	onemocnění
chránit	chránit	k5eAaImF	chránit
<g/>
;	;	kIx,	;
ví	vědět	k5eAaImIp3nS	vědět
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
<g/>
,	,	kIx,	,
že	že	k8xS	že
spouštěčem	spouštěč	k1gInSc7	spouštěč
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
infekční	infekční	k2eAgNnSc1d1	infekční
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Očkování	očkování	k1gNnSc1	očkování
proti	proti	k7c3	proti
hepatitidě	hepatitida	k1gFnSc3	hepatitida
B	B	kA	B
a	a	k8xC	a
roztroušená	roztroušený	k2eAgFnSc1d1	roztroušená
skleróza	skleróza	k1gFnSc1	skleróza
====	====	k?	====
</s>
</p>
<p>
<s>
Na	na	k7c6	na
asociaci	asociace	k1gFnSc6	asociace
mezi	mezi	k7c4	mezi
očkování	očkování	k1gNnSc4	očkování
proti	proti	k7c3	proti
hepatitidou	hepatitida	k1gFnSc7	hepatitida
B	B	kA	B
a	a	k8xC	a
vznikem	vznik	k1gInSc7	vznik
roztroušené	roztroušený	k2eAgFnSc2d1	roztroušená
sklerózy	skleróza	k1gFnSc2	skleróza
poukázaly	poukázat	k5eAaPmAgFnP	poukázat
některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
<g/>
,	,	kIx,	,
např.	např.	kA	např.
studie	studie	k1gFnSc2	studie
M.	M.	kA	M.
A.	A.	kA	A.
Hernána	Hernán	k2eAgMnSc4d1	Hernán
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
publikovali	publikovat	k5eAaBmAgMnP	publikovat
A.	A.	kA	A.
Langer-Gould	Langer-Gould	k1gInSc1	Langer-Gould
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
četnost	četnost	k1gFnSc4	četnost
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
očkování	očkování	k1gNnSc6	očkování
proti	proti	k7c3	proti
hepatitidě	hepatitida	k1gFnSc3	hepatitida
B	B	kA	B
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
z	z	k7c2	z
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
hlediska	hledisko	k1gNnSc2	hledisko
se	se	k3xPyFc4	se
četnost	četnost	k1gFnSc1	četnost
onemocnění	onemocnění	k1gNnPc2	onemocnění
mezi	mezi	k7c7	mezi
očkovanými	očkovaný	k2eAgInPc7d1	očkovaný
a	a	k8xC	a
neočkovanými	očkovaný	k2eNgInPc7d1	neočkovaný
prakticky	prakticky	k6eAd1	prakticky
neliší	lišit	k5eNaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
autorů	autor	k1gMnPc2	autor
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
očkování	očkování	k1gNnSc1	očkování
může	moct	k5eAaImIp3nS	moct
poněkud	poněkud	k6eAd1	poněkud
urychlit	urychlit	k5eAaPmF	urychlit
přechod	přechod	k1gInSc4	přechod
již	již	k6eAd1	již
existujícího	existující	k2eAgNnSc2d1	existující
onemocnění	onemocnění	k1gNnSc2	onemocnění
od	od	k7c2	od
latentní	latentní	k2eAgFnSc2d1	latentní
fáze	fáze	k1gFnSc2	fáze
ke	k	k7c3	k
klinické	klinický	k2eAgFnSc3d1	klinická
manifestaci	manifestace	k1gFnSc3	manifestace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
očkování	očkování	k1gNnSc1	očkování
onemocnění	onemocnění	k1gNnSc2	onemocnění
nezpůsobuje	způsobovat	k5eNaImIp3nS	způsobovat
ani	ani	k8xC	ani
nezvyšuje	zvyšovat	k5eNaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc4	riziko
jeho	jeho	k3xOp3gInSc2	jeho
vzniku	vznik	k1gInSc2	vznik
u	u	k7c2	u
zdravého	zdravý	k2eAgMnSc2d1	zdravý
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
<g/>
Studií	studie	k1gFnPc2	studie
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
několik	několik	k4yIc1	několik
metaanalýz	metaanalýza	k1gFnPc2	metaanalýza
a	a	k8xC	a
přehledových	přehledový	k2eAgInPc2d1	přehledový
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
kritických	kritický	k2eAgFnPc2d1	kritická
prací	práce	k1gFnPc2	práce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
analyzovaly	analyzovat	k5eAaImAgFnP	analyzovat
všechny	všechen	k3xTgFnPc4	všechen
dosud	dosud	k6eAd1	dosud
publikované	publikovaný	k2eAgFnPc4d1	publikovaná
studie	studie	k1gFnPc4	studie
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
F.	F.	kA	F.
DeStefana	DeStefana	k1gFnSc1	DeStefana
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
uzavírá	uzavírat	k5eAaImIp3nS	uzavírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
publikované	publikovaný	k2eAgFnPc4d1	publikovaná
epidemiologické	epidemiologický	k2eAgFnPc4d1	epidemiologická
studie	studie	k1gFnPc4	studie
prokazují	prokazovat	k5eAaImIp3nP	prokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vakcinace	vakcinace	k1gFnSc1	vakcinace
proti	proti	k7c3	proti
hepatitidě	hepatitida	k1gFnSc3	hepatitida
B	B	kA	B
nezvyšuje	zvyšovat	k5eNaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc1	riziko
vzniku	vznik	k1gInSc2	vznik
roztroušené	roztroušený	k2eAgFnSc2d1	roztroušená
sklerózy	skleróza	k1gFnSc2	skleróza
ani	ani	k8xC	ani
riziko	riziko	k1gNnSc4	riziko
ataky	ataka	k1gFnSc2	ataka
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
z	z	k7c2	z
autorů	autor	k1gMnPc2	autor
této	tento	k3xDgFnSc2	tento
studie	studie	k1gFnSc2	studie
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
publikace	publikace	k1gFnSc2	publikace
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
CDC	CDC	kA	CDC
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
autorů	autor	k1gMnPc2	autor
studie	studie	k1gFnSc2	studie
(	(	kIx(	(
<g/>
T.	T.	kA	T.
Verstraeten	Verstraeten	k2eAgMnSc1d1	Verstraeten
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
zaměstnancem	zaměstnanec	k1gMnSc7	zaměstnanec
firmy	firma	k1gFnSc2	firma
GlaxoSmithKline	GlaxoSmithKlin	k1gInSc5	GlaxoSmithKlin
Biologicals	Biologicals	k1gInSc1	Biologicals
<g/>
.	.	kIx.	.
<g/>
V.	V.	kA	V.
Martínez-Sernández	Martínez-Sernández	k1gInSc1	Martínez-Sernández
a	a	k8xC	a
A.	A.	kA	A.
Figueiras	Figueiras	k1gInSc4	Figueiras
publikovali	publikovat	k5eAaBmAgMnP	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
metaanalýzu	metaanalýza	k1gFnSc4	metaanalýza
<g/>
,	,	kIx,	,
ve	v	k7c6	v
konstatují	konstatovat	k5eAaBmIp3nP	konstatovat
metodologickou	metodologický	k2eAgFnSc4d1	metodologická
limitaci	limitace	k1gFnSc4	limitace
publikovaných	publikovaný	k2eAgFnPc2d1	publikovaná
studií	studie	k1gFnPc2	studie
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc1	všechen
studie	studie	k1gFnPc1	studie
splňující	splňující	k2eAgFnPc1d1	splňující
inkluzní	inkluznit	k5eAaPmIp3nP	inkluznit
kritéria	kritérion	k1gNnPc1	kritérion
jejich	jejich	k3xOp3gNnSc4	jejich
metaanalýzy	metaanalýza	k1gFnPc1	metaanalýza
prokazovaly	prokazovat	k5eAaImAgFnP	prokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c4	mezi
očkování	očkování	k1gNnSc4	očkování
a	a	k8xC	a
roztroušenou	roztroušený	k2eAgFnSc7d1	roztroušená
sklerózou	skleróza	k1gFnSc7	skleróza
není	být	k5eNaImIp3nS	být
asociace	asociace	k1gFnSc1	asociace
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
publikovaná	publikovaný	k2eAgNnPc4d1	publikované
data	datum	k1gNnPc4	datum
podle	podle	k7c2	podle
autorů	autor	k1gMnPc2	autor
nepodporují	podporovat	k5eNaImIp3nP	podporovat
tvrzení	tvrzení	k1gNnPc4	tvrzení
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
očkování	očkování	k1gNnSc1	očkování
proti	proti	k7c3	proti
hepatitidě	hepatitida	k1gFnSc3	hepatitida
B	B	kA	B
představovalo	představovat	k5eAaImAgNnS	představovat
riziko	riziko	k1gNnSc1	riziko
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgFnPc4	který
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
revidovat	revidovat	k5eAaImF	revidovat
očkovací	očkovací	k2eAgNnSc4d1	očkovací
schéma	schéma	k1gNnSc4	schéma
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
žádoucí	žádoucí	k2eAgMnSc1d1	žádoucí
zlepšit	zlepšit	k5eAaPmF	zlepšit
kvalitu	kvalita	k1gFnSc4	kvalita
observačních	observační	k2eAgFnPc2d1	observační
studií	studie	k1gFnPc2	studie
<g/>
,	,	kIx,	,
protožee	protožee	k6eAd1	protožee
doposud	doposud	k6eAd1	doposud
publikované	publikovaný	k2eAgFnPc4d1	publikovaná
studie	studie	k1gFnPc4	studie
nepředstavují	představovat	k5eNaImIp3nP	představovat
dostatečně	dostatečně	k6eAd1	dostatečně
robustní	robustní	k2eAgNnPc4d1	robustní
data	datum	k1gNnPc4	datum
pro	pro	k7c4	pro
jednoznačný	jednoznačný	k2eAgInSc4d1	jednoznačný
závěr	závěr	k1gInSc4	závěr
o	o	k7c6	o
bezpečnosti	bezpečnost	k1gFnSc6	bezpečnost
očkování	očkování	k1gNnSc2	očkování
proti	proti	k7c3	proti
hepatitidě	hepatitida	k1gFnSc3	hepatitida
B	B	kA	B
u	u	k7c2	u
zdravých	zdravý	k2eAgInPc2d1	zdravý
subjektů	subjekt	k1gInPc2	subjekt
i	i	k8xC	i
u	u	k7c2	u
nemocných	nemocný	k1gMnPc2	nemocný
s	s	k7c7	s
roztroušenou	roztroušený	k2eAgFnSc7d1	roztroušená
sklerózou	skleróza	k1gFnSc7	skleróza
<g/>
.	.	kIx.	.
<g/>
Asociaci	asociace	k1gFnSc4	asociace
autoimunitních	autoimunitní	k2eAgNnPc2d1	autoimunitní
onemocnění	onemocnění	k1gNnPc2	onemocnění
centrální	centrální	k2eAgFnSc2d1	centrální
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
s	s	k7c7	s
očkováním	očkování	k1gNnSc7	očkování
proti	proti	k7c3	proti
hepatitidě	hepatitida	k1gFnSc3	hepatitida
B	B	kA	B
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
v	v	k7c6	v
letech	let	k1gInPc6	let
1994	[number]	k4	1994
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
analyzovali	analyzovat	k5eAaImAgMnP	analyzovat
Y.	Y.	kA	Y.
Mikaeloff	Mikaeloff	k1gMnSc1	Mikaeloff
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
publikovali	publikovat	k5eAaBmAgMnP	publikovat
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgInPc4	který
došli	dojít	k5eAaPmAgMnP	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
očkování	očkování	k1gNnSc1	očkování
proti	proti	k7c3	proti
hepatitidě	hepatitida	k1gFnSc3	hepatitida
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
obecně	obecně	k6eAd1	obecně
nezvyšuje	zvyšovat	k5eNaImIp3nS	zvyšovat
riziko	riziko	k1gNnSc1	riziko
zánětlivých	zánětlivý	k2eAgNnPc2d1	zánětlivé
demyelinizačních	demyelinizační	k2eAgNnPc2d1	demyelinizační
onemocnění	onemocnění	k1gNnPc2	onemocnění
centrální	centrální	k2eAgFnSc2d1	centrální
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
vč.	vč.	k?	vč.
roztroušené	roztroušený	k2eAgFnSc2d1	roztroušená
sklerózy	skleróza	k1gFnSc2	skleróza
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
představovat	představovat	k5eAaImF	představovat
očkování	očkování	k1gNnSc1	očkování
vakcínou	vakcína	k1gFnSc7	vakcína
Enherix	Enherix	k1gInSc4	Enherix
B	B	kA	B
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
riziko	riziko	k1gNnSc1	riziko
skutečně	skutečně	k6eAd1	skutečně
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
očkováním	očkování	k1gNnSc7	očkování
zvýšeno	zvýšen	k2eAgNnSc4d1	zvýšeno
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
analyzoval	analyzovat	k5eAaImAgMnS	analyzovat
i	i	k9	i
pediatr	pediatr	k1gMnSc1	pediatr
D.	D.	kA	D.
Le	Le	k1gMnSc1	Le
Houézec	Houézec	k1gMnSc1	Houézec
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
dospěl	dochvít	k5eAaPmAgMnS	dochvít
k	k	k7c3	k
naprosto	naprosto	k6eAd1	naprosto
odlišnému	odlišný	k2eAgInSc3d1	odlišný
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
že	že	k8xS	že
mezi	mezi	k7c7	mezi
očkováním	očkování	k1gNnSc7	očkování
proti	proti	k7c3	proti
hepatitidě	hepatitida	k1gFnSc3	hepatitida
B	B	kA	B
a	a	k8xC	a
výskytem	výskyt	k1gInSc7	výskyt
roztroušené	roztroušený	k2eAgFnSc2d1	roztroušená
sklerózy	skleróza	k1gFnSc2	skleróza
je	být	k5eAaImIp3nS	být
silná	silný	k2eAgFnSc1d1	silná
korelace	korelace	k1gFnSc1	korelace
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
práci	práce	k1gFnSc4	práce
podrobili	podrobit	k5eAaPmAgMnP	podrobit
silné	silný	k2eAgFnSc3d1	silná
kritice	kritika	k1gFnSc3	kritika
epidemiologové	epidemiolog	k1gMnPc1	epidemiolog
A.	A.	kA	A.
Spira	Spira	k1gMnSc1	Spira
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
poukázali	poukázat	k5eAaPmAgMnP	poukázat
na	na	k7c4	na
zcela	zcela	k6eAd1	zcela
zásadní	zásadní	k2eAgNnPc4d1	zásadní
metodologická	metodologický	k2eAgNnPc4d1	metodologické
pochybení	pochybení	k1gNnPc4	pochybení
při	při	k7c6	při
analýze	analýza	k1gFnSc6	analýza
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podle	podle	k7c2	podle
nich	on	k3xPp3gFnPc2	on
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
úplnému	úplný	k2eAgNnSc3d1	úplné
znehodnocení	znehodnocení	k1gNnSc3	znehodnocení
výsledku	výsledek	k1gInSc2	výsledek
<g/>
.	.	kIx.	.
<g/>
Global	globat	k5eAaImAgMnS	globat
Advisory	Advisor	k1gInPc4	Advisor
Committee	Committe	k1gFnSc2	Committe
on	on	k3xPp3gMnSc1	on
Vaccine	Vaccin	k1gInSc5	Vaccin
Safety	Safet	k5eAaPmNgFnP	Safet
<g/>
,	,	kIx,	,
orgán	orgán	k1gInSc1	orgán
WHO	WHO	kA	WHO
<g/>
,	,	kIx,	,
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
a	a	k8xC	a
2008	[number]	k4	2008
stanovisko	stanovisko	k1gNnSc4	stanovisko
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
očkováním	očkování	k1gNnSc7	očkování
proti	proti	k7c3	proti
hepatitidě	hepatitida	k1gFnSc3	hepatitida
B	B	kA	B
a	a	k8xC	a
roztroušenou	roztroušený	k2eAgFnSc7d1	roztroušená
sklerózou	skleróza	k1gFnSc7	skleróza
není	být	k5eNaImIp3nS	být
asociace	asociace	k1gFnSc1	asociace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Očkování	očkování	k1gNnSc1	očkování
a	a	k8xC	a
autismus	autismus	k1gInSc1	autismus
====	====	k?	====
</s>
</p>
<p>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Očkování	očkování	k1gNnSc2	očkování
a	a	k8xC	a
autismus	autismus	k1gInSc1	autismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autismus	autismus	k1gInSc1	autismus
je	být	k5eAaImIp3nS	být
vývojové	vývojový	k2eAgNnSc4d1	vývojové
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
zejména	zejména	k9	zejména
poruchami	porucha	k1gFnPc7	porucha
sociální	sociální	k2eAgFnSc2d1	sociální
interakce	interakce	k1gFnSc2	interakce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
publikoval	publikovat	k5eAaBmAgMnS	publikovat
Andrew	Andrew	k1gMnSc1	Andrew
Wakefield	Wakefield	k1gMnSc1	Wakefield
studii	studie	k1gFnSc4	studie
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
je	být	k5eAaImIp3nS	být
rizikovým	rizikový	k2eAgInSc7d1	rizikový
faktorem	faktor	k1gInSc7	faktor
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
autismus	autismus	k1gInSc4	autismus
očkování	očkování	k1gNnPc2	očkování
proti	proti	k7c3	proti
spalničkám	spalničky	k1gFnPc3	spalničky
<g/>
,	,	kIx,	,
příušnicím	příušnice	k1gFnPc3	příušnice
a	a	k8xC	a
zarděnkám	zarděnky	k1gFnPc3	zarděnky
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
v	v	k7c6	v
prestižním	prestižní	k2eAgInSc6d1	prestižní
časopise	časopis	k1gInSc6	časopis
The	The	k1gFnSc2	The
Lancet	lanceta	k1gFnPc2	lanceta
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
závažná	závažný	k2eAgNnPc4d1	závažné
etická	etický	k2eAgNnPc4d1	etické
i	i	k8xC	i
věcná	věcný	k2eAgNnPc4d1	věcné
pochybení	pochybení	k1gNnPc4	pochybení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
stažena	stáhnout	k5eAaPmNgFnS	stáhnout
(	(	kIx(	(
<g/>
RETRACTED	RETRACTED	kA	RETRACTED
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
F.	F.	kA	F.
DeStefano	DeStefana	k1gFnSc5	DeStefana
provedl	provést	k5eAaPmAgInS	provést
přehled	přehled	k1gInSc4	přehled
prací	práce	k1gFnPc2	práce
studujících	studující	k1gFnPc2	studující
asociaci	asociace	k1gFnSc4	asociace
mezi	mezi	k7c7	mezi
očkováním	očkování	k1gNnSc7	očkování
a	a	k8xC	a
autismem	autismus	k1gInSc7	autismus
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
vakcínou	vakcína	k1gFnSc7	vakcína
MMR	MMR	kA	MMR
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgFnP	být
publikovány	publikovat	k5eAaBmNgInP	publikovat
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1999	[number]	k4	1999
až	až	k9	až
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgFnPc2	tento
studií	studie	k1gFnPc2	studie
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
očkováním	očkování	k1gNnSc7	očkování
a	a	k8xC	a
autismem	autismus	k1gInSc7	autismus
neexistuje	existovat	k5eNaImIp3nS	existovat
kauzální	kauzální	k2eAgInSc1d1	kauzální
vztah	vztah	k1gInSc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
podobnému	podobný	k2eAgInSc3d1	podobný
závěru	závěr	k1gInSc3	závěr
dospěli	dochvít	k5eAaPmAgMnP	dochvít
i	i	k9	i
další	další	k2eAgMnPc1d1	další
odborníci	odborník	k1gMnPc1	odborník
stojící	stojící	k2eAgMnSc1d1	stojící
za	za	k7c4	za
stanovisky	stanovisko	k1gNnPc7	stanovisko
Institut	institut	k1gInSc1	institut
of	of	k?	of
Medicine	Medicin	k1gInSc5	Medicin
(	(	kIx(	(
<g/>
IOM	IOM	kA	IOM
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Národních	národní	k2eAgFnPc2d1	národní
akademií	akademie	k1gFnPc2	akademie
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
CDC	CDC	kA	CDC
<g/>
,	,	kIx,	,
Americkou	americký	k2eAgFnSc7d1	americká
akademií	akademie	k1gFnSc7	akademie
pediatrů	pediatr	k1gMnPc2	pediatr
(	(	kIx(	(
<g/>
AAP	AAP	kA	AAP
<g/>
)	)	kIx)	)
a	a	k8xC	a
Americkou	americký	k2eAgFnSc7d1	americká
akademií	akademie	k1gFnSc7	akademie
rodinných	rodinný	k2eAgMnPc2d1	rodinný
lékařů	lékař	k1gMnPc2	lékař
(	(	kIx(	(
<g/>
AAFP	AAFP	kA	AAFP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Adjuvans	Adjuvans	k1gInSc4	Adjuvans
===	===	k?	===
</s>
</p>
<p>
<s>
Vedle	vedle	k7c2	vedle
samotné	samotný	k2eAgFnSc2d1	samotná
aktivace	aktivace	k1gFnSc2	aktivace
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
za	za	k7c4	za
případné	případný	k2eAgInPc4d1	případný
nežádoucí	žádoucí	k2eNgInPc4d1	nežádoucí
účinky	účinek	k1gInPc4	účinek
odpovědné	odpovědný	k2eAgInPc4d1	odpovědný
i	i	k9	i
adjuvans	adjuvans	k1gInSc4	adjuvans
<g/>
.	.	kIx.	.
</s>
<s>
Látky	látka	k1gFnPc1	látka
používané	používaný	k2eAgFnPc1d1	používaná
v	v	k7c4	v
adjuvans	adjuvans	k1gInSc4	adjuvans
jsou	být	k5eAaImIp3nP	být
testovány	testovat	k5eAaImNgInP	testovat
na	na	k7c4	na
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
občasné	občasný	k2eAgFnPc1d1	občasná
zprávy	zpráva	k1gFnPc1	zpráva
referující	referující	k2eAgFnPc4d1	referující
o	o	k7c6	o
možné	možný	k2eAgFnSc6d1	možná
toxicitě	toxicita	k1gFnSc6	toxicita
některých	některý	k3yIgFnPc2	některý
adjuvans	adjuvansa	k1gFnPc2	adjuvansa
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
o	o	k7c6	o
nežádoucích	žádoucí	k2eNgInPc6d1	nežádoucí
účincích	účinek	k1gInPc6	účinek
adjuvans	adjuvansa	k1gFnPc2	adjuvansa
jako	jako	k8xC	jako
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Škodlivé	škodlivý	k2eAgNnSc4d1	škodlivé
působení	působení	k1gNnSc4	působení
adjuvans	adjuvansa	k1gFnPc2	adjuvansa
====	====	k?	====
</s>
</p>
<p>
<s>
J	J	kA	J
Hawken	Hawken	k1gInSc1	Hawken
a	a	k8xC	a
S.	S.	kA	S.
B.	B.	kA	B.
Troy	Troa	k1gMnSc2	Troa
ve	v	k7c6	v
přehledové	přehledový	k2eAgFnSc6d1	přehledová
práci	práce	k1gFnSc6	práce
v	v	k7c6	v
roku	rok	k1gInSc6	rok
2012	[number]	k4	2012
o	o	k7c4	o
adjuvans	adjuvans	k1gInSc4	adjuvans
v	v	k7c6	v
inaktivovaných	inaktivovaný	k2eAgFnPc6d1	inaktivovaná
vakcínách	vakcína	k1gFnPc6	vakcína
proti	proti	k7c3	proti
poliomyelitidě	poliomyelitida	k1gFnSc3	poliomyelitida
hodnotí	hodnotit	k5eAaImIp3nP	hodnotit
nevýhody	nevýhoda	k1gFnPc1	nevýhoda
používaných	používaný	k2eAgFnPc2d1	používaná
adjuvans	adjuvansa	k1gFnPc2	adjuvansa
podle	podle	k7c2	podle
doposud	doposud	k6eAd1	doposud
publikovaných	publikovaný	k2eAgFnPc2d1	publikovaná
studií	studie	k1gFnPc2	studie
následujícím	následující	k2eAgInSc7d1	následující
způsobem	způsob	k1gInSc7	způsob
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nerozpustné	rozpustný	k2eNgFnPc1d1	nerozpustná
hlinité	hlinitý	k2eAgFnPc1d1	hlinitá
sloučeniny	sloučenina	k1gFnPc1	sloučenina
jsou	být	k5eAaImIp3nP	být
dobré	dobrý	k2eAgFnPc1d1	dobrá
a	a	k8xC	a
efektivní	efektivní	k2eAgFnPc1d1	efektivní
adjuvanty	adjuvanta	k1gFnPc1	adjuvanta
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přece	přece	k9	přece
jen	jen	k9	jen
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
imunogenní	imunogenní	k2eAgFnPc1d1	imunogenní
než	než	k8xS	než
jiná	jiný	k2eAgFnSc1d1	jiná
adjuvans	adjuvans	k1gInSc1	adjuvans
<g/>
.	.	kIx.	.
</s>
<s>
Můžou	Můžou	k?	Můžou
vyvolat	vyvolat	k5eAaPmF	vyvolat
lokální	lokální	k2eAgFnSc4d1	lokální
reakci	reakce	k1gFnSc4	reakce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kalcium	kalcium	k1gNnSc1	kalcium
fosfát	fosfát	k1gInSc1	fosfát
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
jako	jako	k9	jako
dobré	dobrý	k2eAgNnSc1d1	dobré
adjuvans	adjuvans	k6eAd1	adjuvans
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yQgNnSc2	který
nebyla	být	k5eNaImAgFnS	být
pozorována	pozorován	k2eAgFnSc1d1	pozorována
lokální	lokální	k2eAgFnSc1d1	lokální
ani	ani	k8xC	ani
systémová	systémový	k2eAgFnSc1d1	systémová
reakce	reakce	k1gFnSc1	reakce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Olejové	olejový	k2eAgFnPc1d1	olejová
emulze	emulze	k1gFnPc1	emulze
používané	používaný	k2eAgFnPc4d1	používaná
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgInP	být
silně	silně	k6eAd1	silně
imunogenní	imunogenní	k2eAgInPc1d1	imunogenní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poněkud	poněkud	k6eAd1	poněkud
častěji	často	k6eAd2	často
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
0,5	[number]	k4	0,5
%	%	kIx~	%
očkovaných	očkovaný	k2eAgInPc2d1	očkovaný
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
výraznější	výrazný	k2eAgFnPc1d2	výraznější
lokální	lokální	k2eAgFnPc1d1	lokální
reakce	reakce	k1gFnPc1	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnPc1d1	moderní
olejové	olejový	k2eAgFnPc1d1	olejová
emulze	emulze	k1gFnPc1	emulze
(	(	kIx(	(
<g/>
např.	např.	kA	např.
MF	MF	kA	MF
<g/>
59	[number]	k4	59
<g/>
,	,	kIx,	,
AF	AF	kA	AF
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
AS	as	k9	as
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
vyšší	vysoký	k2eAgFnSc7d2	vyšší
biokompatibilitou	biokompatibilita	k1gFnSc7	biokompatibilita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chitosan	Chitosan	k1gInSc1	Chitosan
má	mít	k5eAaImIp3nS	mít
zřejmě	zřejmě	k6eAd1	zřejmě
velmi	velmi	k6eAd1	velmi
dobrý	dobrý	k2eAgInSc4d1	dobrý
bezpečnostní	bezpečnostní	k2eAgInSc4d1	bezpečnostní
profil	profil	k1gInSc4	profil
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
např.	např.	kA	např.
jeho	on	k3xPp3gInSc4	on
intramuskultární	intramuskultární	k2eAgFnSc1d1	intramuskultární
aplikace	aplikace	k1gFnSc1	aplikace
nebyla	být	k5eNaImAgFnS	být
dosud	dosud	k6eAd1	dosud
výrazněji	výrazně	k6eAd2	výrazně
studována	studovat	k5eAaImNgFnS	studovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vitamín	vitamín	k1gInSc1	vitamín
D	D	kA	D
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
experimentální	experimentální	k2eAgInSc1d1	experimentální
adjuvans	adjuvans	k1gInSc1	adjuvans
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
vyšší	vysoký	k2eAgFnSc7d2	vyšší
bolestivostí	bolestivost	k1gFnSc7	bolestivost
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
vpichu	vpich	k1gInSc2	vpich
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zřejmě	zřejmě	k6eAd1	zřejmě
nemá	mít	k5eNaImIp3nS	mít
jiné	jiný	k2eAgInPc4d1	jiný
lokální	lokální	k2eAgInPc4d1	lokální
nežádoucí	žádoucí	k2eNgInPc4d1	nežádoucí
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CpG	CpG	k?	CpG
oligonukleotidy	oligonukleotida	k1gFnPc1	oligonukleotida
jsou	být	k5eAaImIp3nP	být
experimentální	experimentální	k2eAgFnPc1d1	experimentální
adjuvans	adjuvans	k6eAd1	adjuvans
imitující	imitující	k2eAgNnPc4d1	imitující
bakteriální	bakteriální	k2eAgNnPc4d1	bakteriální
DNA	dno	k1gNnPc4	dno
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
poněkud	poněkud	k6eAd1	poněkud
vyšší	vysoký	k2eAgFnSc1d2	vyšší
frekvence	frekvence	k1gFnSc1	frekvence
lehkých	lehký	k2eAgFnPc2d1	lehká
lokálních	lokální	k2eAgFnPc2d1	lokální
reakcí	reakce	k1gFnPc2	reakce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stearyltyrozin	Stearyltyrozin	k1gInSc1	Stearyltyrozin
a	a	k8xC	a
oktadecyltyrozin	oktadecyltyrozin	k1gInSc1	oktadecyltyrozin
byly	být	k5eAaImAgFnP	být
studovány	studovat	k5eAaImNgInP	studovat
v	v	k7c4	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
;	;	kIx,	;
ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
bezpečnostní	bezpečnostní	k2eAgInSc1d1	bezpečnostní
profil	profil	k1gInSc1	profil
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
dobrý	dobrý	k2eAgInSc1d1	dobrý
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
se	se	k3xPyFc4	se
nepokračovalo	pokračovat	k5eNaImAgNnS	pokračovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Liposomy	liposom	k1gInPc1	liposom
byly	být	k5eAaImAgInP	být
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
shledány	shledán	k2eAgInPc1d1	shledán
jako	jako	k8xS	jako
bezpečné	bezpečný	k2eAgInPc1d1	bezpečný
ale	ale	k8xC	ale
méně	málo	k6eAd2	málo
imunogenní	imunogenní	k2eAgFnSc1d1	imunogenní
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
moderními	moderní	k2eAgInPc7d1	moderní
přístupy	přístup	k1gInPc7	přístup
dojde	dojít	k5eAaPmIp3nS	dojít
ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
imunogenity	imunogenita	k1gFnSc2	imunogenita
<g/>
.	.	kIx.	.
<g/>
W.	W.	kA	W.
E.	E.	kA	E.
Beyer	Beyra	k1gFnPc2	Beyra
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
publikovali	publikovat	k5eAaBmAgMnP	publikovat
přehledový	přehledový	k2eAgInSc4d1	přehledový
článek	článek	k1gInSc4	článek
srovnávající	srovnávající	k2eAgFnSc4d1	srovnávající
účinnost	účinnost	k1gFnSc4	účinnost
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
vakcín	vakcína	k1gFnPc2	vakcína
proti	proti	k7c3	proti
chřipce	chřipka	k1gFnSc3	chřipka
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
vakcína	vakcína	k1gFnSc1	vakcína
s	s	k7c7	s
adjuvans	adjuvansa	k1gFnPc2	adjuvansa
(	(	kIx(	(
<g/>
olejová	olejový	k2eAgFnSc1d1	olejová
emulze	emulze	k1gFnSc1	emulze
MF	MF	kA	MF
<g/>
59	[number]	k4	59
<g/>
)	)	kIx)	)
indukuje	indukovat	k5eAaBmIp3nS	indukovat
poněkud	poněkud	k6eAd1	poněkud
vyšší	vysoký	k2eAgInSc1d2	vyšší
titr	titr	k1gInSc1	titr
protilátek	protilátka	k1gFnPc2	protilátka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
častější	častý	k2eAgFnSc7d2	častější
lokální	lokální	k2eAgFnSc7d1	lokální
reakcí	reakce	k1gFnSc7	reakce
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neliší	lišit	k5eNaImIp3nS	lišit
se	se	k3xPyFc4	se
profilem	profil	k1gInSc7	profil
systémových	systémový	k2eAgFnPc2d1	systémová
reakcí	reakce	k1gFnPc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
popsané	popsaný	k2eAgFnPc1d1	popsaná
systémové	systémový	k2eAgFnPc1d1	systémová
reakce	reakce	k1gFnPc1	reakce
byly	být	k5eAaImAgFnP	být
mírné	mírný	k2eAgInPc1d1	mírný
a	a	k8xC	a
přechodné	přechodný	k2eAgInPc1d1	přechodný
<g/>
.	.	kIx.	.
<g/>
M.	M.	kA	M.
Montana	Montana	k1gFnSc1	Montana
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
publikovali	publikovat	k5eAaBmAgMnP	publikovat
přehledový	přehledový	k2eAgInSc4d1	přehledový
článek	článek	k1gInSc4	článek
analyzující	analyzující	k2eAgInSc4d1	analyzující
potenciální	potenciální	k2eAgFnSc4d1	potenciální
toxicitu	toxicita	k1gFnSc4	toxicita
skvalenu	skvalen	k2eAgFnSc4d1	skvalen
a	a	k8xC	a
thiomersalu	thiomersala	k1gFnSc4	thiomersala
ve	v	k7c6	v
vakcíně	vakcína	k1gFnSc6	vakcína
proti	proti	k7c3	proti
chřipkovému	chřipkový	k2eAgInSc3d1	chřipkový
viru	vir	k1gInSc3	vir
A	A	kA	A
(	(	kIx(	(
<g/>
H	H	kA	H
<g/>
1	[number]	k4	1
<g/>
N	N	kA	N
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
publikované	publikovaný	k2eAgInPc1d1	publikovaný
výsledky	výsledek	k1gInPc1	výsledek
uzavírají	uzavírat	k5eAaImIp3nP	uzavírat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
adjuvans	adjuvans	k6eAd1	adjuvans
nezvyšují	zvyšovat	k5eNaImIp3nP	zvyšovat
incidenci	incidence	k1gFnSc4	incidence
autoimunitních	autoimunitní	k2eAgNnPc2d1	autoimunitní
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Neexistují	existovat	k5eNaImIp3nP	existovat
ani	ani	k8xC	ani
spolehlivé	spolehlivý	k2eAgInPc1d1	spolehlivý
důkazy	důkaz	k1gInPc1	důkaz
pro	pro	k7c4	pro
předpoklad	předpoklad	k1gInSc4	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
expozice	expozice	k1gFnSc1	expozice
malému	malý	k2eAgNnSc3d1	malé
množství	množství	k1gNnSc3	množství
thiomersalu	thiomersal	k1gInSc2	thiomersal
zhoršovala	zhoršovat	k5eAaImAgFnS	zhoršovat
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
<s>
Konstatují	konstatovat	k5eAaBmIp3nP	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
systémové	systémový	k2eAgFnPc1d1	systémová
reakce	reakce	k1gFnPc1	reakce
po	po	k7c6	po
vakcínách	vakcína	k1gFnPc6	vakcína
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
objevit	objevit	k5eAaPmF	objevit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebývají	bývat	k5eNaImIp3nP	bývat
vážné	vážný	k2eAgInPc1d1	vážný
<g/>
.	.	kIx.	.
<g/>
G.	G.	kA	G.
Lippi	Lipp	k1gFnSc2	Lipp
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
v	v	k7c6	v
přehledovém	přehledový	k2eAgInSc6d1	přehledový
článku	článek	k1gInSc6	článek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
analyzovali	analyzovat	k5eAaImAgMnP	analyzovat
potenciální	potenciální	k2eAgMnPc1d1	potenciální
rizika	riziko	k1gNnPc4	riziko
použití	použití	k1gNnSc6	použití
skvalenu	skvalen	k2eAgFnSc4d1	skvalen
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k6eAd1	především
konstatují	konstatovat	k5eAaBmIp3nP	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
skvalen	skvalen	k2eAgMnSc1d1	skvalen
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
vyšších	vysoký	k2eAgInPc6d2	vyšší
organismech	organismus	k1gInPc6	organismus
jako	jako	k8xC	jako
meziprodukt	meziprodukt	k1gInSc4	meziprodukt
syntézy	syntéza	k1gFnSc2	syntéza
cholesterolu	cholesterol	k1gInSc2	cholesterol
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
emulzifikuje	emulzifikovat	k5eAaBmIp3nS	emulzifikovat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
využíván	využívat	k5eAaImNgInS	využívat
kosmetickým	kosmetický	k2eAgInSc7d1	kosmetický
průmyslem	průmysl	k1gInSc7	průmysl
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
jako	jako	k9	jako
adjuvans	adjuvans	k6eAd1	adjuvans
do	do	k7c2	do
vakcín	vakcína	k1gFnPc2	vakcína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
přítomnosti	přítomnost	k1gFnSc6	přítomnost
protilátek	protilátka	k1gFnPc2	protilátka
proti	proti	k7c3	proti
skvalenu	skvalen	k2eAgFnSc4d1	skvalen
u	u	k7c2	u
nemocných	nemocný	k2eAgMnPc2d1	nemocný
se	s	k7c7	s
syndromem	syndrom	k1gInSc7	syndrom
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
očkováni	očkovat	k5eAaImNgMnP	očkovat
proti	proti	k7c3	proti
antraxu	antrax	k1gInSc3	antrax
vakcínou	vakcína	k1gFnSc7	vakcína
obsahující	obsahující	k2eAgFnPc1d1	obsahující
jako	jako	k8xC	jako
adjuvans	adjuvans	k6eAd1	adjuvans
skvalen	skvalen	k2eAgInSc1d1	skvalen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
iniciální	iniciální	k2eAgFnSc6d1	iniciální
studii	studie	k1gFnSc6	studie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ukazovala	ukazovat	k5eAaImAgFnS	ukazovat
na	na	k7c4	na
silnou	silný	k2eAgFnSc4d1	silná
asociaci	asociace	k1gFnSc4	asociace
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
zásadní	zásadní	k2eAgInPc1d1	zásadní
technické	technický	k2eAgInPc1d1	technický
nedostatky	nedostatek	k1gInPc1	nedostatek
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
výsledek	výsledek	k1gInSc1	výsledek
výrazně	výrazně	k6eAd1	výrazně
oslabily	oslabit	k5eAaPmAgFnP	oslabit
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
zcela	zcela	k6eAd1	zcela
nezpochybnily	zpochybnit	k5eNaPmAgFnP	zpochybnit
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgInSc1d2	pozdější
vývoj	vývoj	k1gInSc1	vývoj
nových	nový	k2eAgFnPc2d1	nová
imunochemických	imunochemický	k2eAgFnPc2d1	imunochemická
technik	technika	k1gFnPc2	technika
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
preciznějšímu	precizní	k2eAgNnSc3d2	preciznější
studiu	studio	k1gNnSc3	studio
přítomnosti	přítomnost	k1gFnSc2	přítomnost
specifických	specifický	k2eAgFnPc2d1	specifická
protilátek	protilátka	k1gFnPc2	protilátka
proti	proti	k7c3	proti
skvalenu	skvalen	k2eAgFnSc4d1	skvalen
po	po	k7c6	po
očkování	očkování	k1gNnSc6	očkování
vakcínou	vakcína	k1gFnSc7	vakcína
proti	proti	k7c3	proti
antraxu	antrax	k1gInSc3	antrax
<g/>
.	.	kIx.	.
</s>
<s>
Protilátky	protilátka	k1gFnPc1	protilátka
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
IgM	IgM	k1gFnPc2	IgM
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
asi	asi	k9	asi
u	u	k7c2	u
pětiny	pětina	k1gFnSc2	pětina
očkovaných	očkovaný	k2eAgMnPc2d1	očkovaný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
u	u	k7c2	u
jednoho	jeden	k4xCgInSc2	jeden
se	se	k3xPyFc4	se
neobjevily	objevit	k5eNaPmAgFnP	objevit
protilátky	protilátka	k1gFnPc1	protilátka
proti	proti	k7c3	proti
skvalenu	skvalen	k2eAgFnSc4d1	skvalen
ve	v	k7c6	v
třídě	třída	k1gFnSc6	třída
IgG	IgG	k1gFnSc2	IgG
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
protilátky	protilátka	k1gFnPc1	protilátka
specifické	specifický	k2eAgFnPc1d1	specifická
a	a	k8xC	a
paměťové	paměťový	k2eAgFnPc1d1	paměťová
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
výzkumy	výzkum	k1gInPc1	výzkum
odhalily	odhalit	k5eAaPmAgInP	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
protilátky	protilátka	k1gFnPc1	protilátka
proti	proti	k7c3	proti
skvalenu	skvalen	k2eAgFnSc4d1	skvalen
ve	v	k7c6	v
třidě	třid	k1gInSc6	třid
IgM	IgM	k1gFnSc2	IgM
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
přirozeně	přirozeně	k6eAd1	přirozeně
u	u	k7c2	u
části	část	k1gFnSc2	část
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
podíl	podíl	k1gInSc1	podíl
lidí	člověk	k1gMnPc2	člověk
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
protilátkami	protilátka	k1gFnPc7	protilátka
s	s	k7c7	s
věkem	věk	k1gInSc7	věk
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
recentní	recentní	k2eAgFnPc1d1	recentní
studie	studie	k1gFnPc1	studie
neprokazují	prokazovat	k5eNaImIp3nP	prokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
mezi	mezi	k7c7	mezi
protilátkami	protilátka	k1gFnPc7	protilátka
proti	proti	k7c3	proti
skvalenu	skvalen	k2eAgFnSc4d1	skvalen
a	a	k8xC	a
chronickými	chronický	k2eAgInPc7d1	chronický
syndromy	syndrom	k1gInPc7	syndrom
typu	typ	k1gInSc2	typ
syndrom	syndrom	k1gInSc1	syndrom
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Zálivu	záliv	k1gInSc6	záliv
byla	být	k5eAaImAgFnS	být
statistická	statistický	k2eAgFnSc1d1	statistická
asociace	asociace	k1gFnSc1	asociace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Syndrom	syndrom	k1gInSc1	syndrom
adjuvans	adjuvans	k6eAd1	adjuvans
indukovaného	indukovaný	k2eAgNnSc2d1	indukované
autoimunitního	autoimunitní	k2eAgNnSc2d1	autoimunitní
onemocnění	onemocnění	k1gNnSc2	onemocnění
====	====	k?	====
</s>
</p>
<p>
<s>
Syndrom	syndrom	k1gInSc1	syndrom
adjuvans	adjuvans	k6eAd1	adjuvans
indukovaného	indukovaný	k2eAgNnSc2d1	indukované
autoimunitního	autoimunitní	k2eAgNnSc2d1	autoimunitní
onemocnění	onemocnění	k1gNnSc2	onemocnění
(	(	kIx(	(
<g/>
Autoimmune	Autoimmun	k1gInSc5	Autoimmun
Syndrome	syndrom	k1gInSc5	syndrom
Induced	Induced	k1gInSc1	Induced
by	by	kYmCp3nS	by
Adjuvants	Adjuvantsa	k1gFnPc2	Adjuvantsa
<g/>
,	,	kIx,	,
ASIA	ASIA	kA	ASIA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hypotetické	hypotetický	k2eAgNnSc1d1	hypotetické
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
existenci	existence	k1gFnSc4	existence
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
Y.	Y.	kA	Y.
Schoenfeld	Schoenfeld	k1gInSc1	Schoenfeld
a	a	k8xC	a
N.	N.	kA	N.
Agmon-Levin	Agmon-Levina	k1gFnPc2	Agmon-Levina
jako	jako	k8xC	jako
poměrně	poměrně	k6eAd1	poměrně
širokou	široký	k2eAgFnSc4d1	široká
skupinu	skupina	k1gFnSc4	skupina
autoimunitních	autoimunitní	k2eAgNnPc2d1	autoimunitní
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
vznik	vznik	k1gInSc1	vznik
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
hypotézy	hypotéza	k1gFnSc2	hypotéza
indukován	indukovat	k5eAaBmNgInS	indukovat
působením	působení	k1gNnSc7	působení
adjuvans	adjuvansa	k1gFnPc2	adjuvansa
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
publikovali	publikovat	k5eAaBmAgMnP	publikovat
D.	D.	kA	D.
Hawkens	Hawkensa	k1gFnPc2	Hawkensa
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
přehledový	přehledový	k2eAgInSc1d1	přehledový
článek	článek	k1gInSc1	článek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
analyzovali	analyzovat	k5eAaImAgMnP	analyzovat
dosud	dosud	k6eAd1	dosud
publikované	publikovaný	k2eAgFnPc4d1	publikovaná
práce	práce	k1gFnPc4	práce
zabývající	zabývající	k2eAgFnPc4d1	zabývající
se	se	k3xPyFc4	se
tímto	tento	k3xDgInSc7	tento
syndromem	syndrom	k1gInSc7	syndrom
<g/>
.	.	kIx.	.
</s>
<s>
Analýzu	analýza	k1gFnSc4	analýza
uzavírají	uzavírat	k5eAaImIp3nP	uzavírat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
neexistují	existovat	k5eNaImIp3nP	existovat
důkazy	důkaz	k1gInPc1	důkaz
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
takový	takový	k3xDgInSc1	takový
syndrom	syndrom	k1gInSc1	syndrom
vůbec	vůbec	k9	vůbec
existoval	existovat	k5eAaImAgInS	existovat
jako	jako	k8xC	jako
validní	validní	k2eAgFnSc1d1	validní
diagnóza	diagnóza	k1gFnSc1	diagnóza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přetížení	přetížení	k1gNnSc6	přetížení
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
===	===	k?	===
</s>
</p>
<p>
<s>
Počet	počet	k1gInSc1	počet
vakcín	vakcína	k1gFnPc2	vakcína
může	moct	k5eAaImIp3nS	moct
někdy	někdy	k6eAd1	někdy
vzbuzovat	vzbuzovat	k5eAaImF	vzbuzovat
obavy	obava	k1gFnPc4	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
případě	případ	k1gInSc6	případ
dětského	dětský	k2eAgNnSc2d1	dětské
očkování	očkování	k1gNnSc2	očkování
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
"	"	kIx"	"
<g/>
přetížení	přetížení	k1gNnSc3	přetížení
<g/>
"	"	kIx"	"
vyvíjejícího	vyvíjející	k2eAgMnSc2d1	vyvíjející
se	se	k3xPyFc4	se
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Chatterjee	Chatterjee	k6eAd1	Chatterjee
a	a	k8xC	a
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Keefe	Keef	k1gInSc5	Keef
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
dodávají	dodávat	k5eAaImIp3nP	dodávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
skutečně	skutečně	k6eAd1	skutečně
některé	některý	k3yIgFnPc4	některý
vakcíny	vakcína	k1gFnPc4	vakcína
s	s	k7c7	s
oslabeným	oslabený	k2eAgInSc7d1	oslabený
virem	vir	k1gInSc7	vir
mohly	moct	k5eAaImAgFnP	moct
výjimečně	výjimečně	k6eAd1	výjimečně
vyvolat	vyvolat	k5eAaPmF	vyvolat
imunosupresi	imunosuprese	k1gFnSc4	imunosuprese
(	(	kIx(	(
<g/>
útlum	útlum	k1gInSc1	útlum
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
používané	používaný	k2eAgFnPc4d1	používaná
vakcíny	vakcína	k1gFnPc4	vakcína
však	však	k9	však
dle	dle	k7c2	dle
expertů	expert	k1gMnPc2	expert
u	u	k7c2	u
zdravých	zdravý	k2eAgFnPc2d1	zdravá
dětí	dítě	k1gFnPc2	dítě
imunosupresi	imunosuprese	k1gFnSc4	imunosuprese
zpravidla	zpravidla	k6eAd1	zpravidla
nevyvolávají	vyvolávat	k5eNaImIp3nP	vyvolávat
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
dodávají	dodávat	k5eAaImIp3nP	dodávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
novorozence	novorozenec	k1gMnSc2	novorozenec
a	a	k8xC	a
kojence	kojenec	k1gMnSc2	kojenec
má	mít	k5eAaImIp3nS	mít
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
kapacitu	kapacita	k1gFnSc4	kapacita
vyrovnávat	vyrovnávat	k5eAaImF	vyrovnávat
se	se	k3xPyFc4	se
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
stimulů	stimul	k1gInPc2	stimul
<g/>
,	,	kIx,	,
stačí	stačit	k5eAaBmIp3nS	stačit
si	se	k3xPyFc3	se
jen	jen	k9	jen
uvědomit	uvědomit	k5eAaPmF	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
je	být	k5eAaImIp3nS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
bakteriální	bakteriální	k2eAgFnSc1d1	bakteriální
kolonizace	kolonizace	k1gFnSc1	kolonizace
střeva	střevo	k1gNnSc2	střevo
a	a	k8xC	a
dítě	dítě	k1gNnSc1	dítě
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
plným	plný	k2eAgNnSc7d1	plné
spektrem	spektrum	k1gNnSc7	spektrum
mikrobů	mikrob	k1gInPc2	mikrob
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
očkování	očkování	k1gNnSc6	očkování
je	být	k5eAaImIp3nS	být
podáno	podat	k5eAaPmNgNnS	podat
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
při	při	k7c6	při
podjednotkových	podjednotkův	k2eAgFnPc6d1	podjednotkův
vakcínách	vakcína	k1gFnPc6	vakcína
<g/>
,	,	kIx,	,
řádově	řádově	k6eAd1	řádově
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
typů	typ	k1gInPc2	typ
antigenů	antigen	k1gInPc2	antigen
i	i	k8xC	i
antigenní	antigenní	k2eAgFnSc4d1	antigenní
zátěž	zátěž	k1gFnSc4	zátěž
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
kapacita	kapacita	k1gFnSc1	kapacita
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
se	se	k3xPyFc4	se
s	s	k7c7	s
takovou	takový	k3xDgFnSc7	takový
zátěží	zátěž	k1gFnSc7	zátěž
bez	bez	k7c2	bez
komplikací	komplikace	k1gFnPc2	komplikace
vyrovnat	vyrovnat	k5eAaBmF	vyrovnat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Očkování	očkování	k1gNnSc1	očkování
jako	jako	k8xS	jako
nástroj	nástroj	k1gInSc1	nástroj
veřejného	veřejný	k2eAgNnSc2d1	veřejné
zdraví	zdraví	k1gNnSc2	zdraví
==	==	k?	==
</s>
</p>
<p>
<s>
Očkování	očkování	k1gNnSc1	očkování
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc4d1	významný
nástroj	nástroj	k1gInSc4	nástroj
veřejného	veřejný	k2eAgNnSc2d1	veřejné
zdraví	zdraví	k1gNnSc2	zdraví
<g/>
,	,	kIx,	,
významným	významný	k2eAgInSc7d1	významný
způsobem	způsob	k1gInSc7	způsob
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
vzniku	vznik	k1gInSc2	vznik
infekčních	infekční	k2eAgNnPc2d1	infekční
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kolektivní	kolektivní	k2eAgFnSc1d1	kolektivní
imunita	imunita	k1gFnSc1	imunita
===	===	k?	===
</s>
</p>
<p>
<s>
Kolektivní	kolektivní	k2eAgFnSc1d1	kolektivní
imunita	imunita	k1gFnSc1	imunita
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
herd	herda	k1gFnPc2	herda
immunity	immunit	k2eAgFnPc1d1	immunit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc1	termín
popisující	popisující	k2eAgFnSc1d1	popisující
vyšší	vysoký	k2eAgFnSc1d2	vyšší
odolnost	odolnost	k1gFnSc1	odolnost
převážně	převážně	k6eAd1	převážně
imunní	imunní	k2eAgFnSc1d1	imunní
populace	populace	k1gFnSc1	populace
k	k	k7c3	k
šíření	šíření	k1gNnSc3	šíření
nakažlivé	nakažlivý	k2eAgFnSc2d1	nakažlivá
choroby	choroba	k1gFnSc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
imunity	imunita	k1gFnPc1	imunita
lze	lze	k6eAd1	lze
obvykle	obvykle	k6eAd1	obvykle
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
vysokou	vysoký	k2eAgFnSc7d1	vysoká
proočkovaností	proočkovanost	k1gFnSc7	proočkovanost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
případě	případ	k1gInSc6	případ
některých	některý	k3yIgFnPc2	některý
chorob	choroba	k1gFnPc2	choroba
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
bylo	být	k5eAaImAgNnS	být
takové	takový	k3xDgFnPc4	takový
imunity	imunita	k1gFnPc4	imunita
dosahováno	dosahován	k2eAgNnSc1d1	dosahováno
i	i	k9	i
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
novorozenci	novorozenec	k1gMnPc1	novorozenec
časně	časně	k6eAd1	časně
nakazili	nakazit	k5eAaPmAgMnP	nakazit
a	a	k8xC	a
pokud	pokud	k8xS	pokud
přežili	přežít	k5eAaPmAgMnP	přežít
<g/>
,	,	kIx,	,
zůstala	zůstat	k5eAaPmAgFnS	zůstat
jim	on	k3xPp3gMnPc3	on
celoživotní	celoživotní	k2eAgFnSc1d1	celoživotní
imunita	imunita	k1gFnSc1	imunita
<g/>
.	.	kIx.	.
</s>
<s>
Kolektivní	kolektivní	k2eAgFnSc1d1	kolektivní
imunita	imunita	k1gFnSc1	imunita
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
nástrojem	nástroj	k1gInSc7	nástroj
ochrany	ochrana	k1gFnSc2	ochrana
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
nemohli	moct	k5eNaImAgMnP	moct
být	být	k5eAaImF	být
ze	z	k7c2	z
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
důvodů	důvod	k1gInPc2	důvod
očkováni	očkovat	k5eAaImNgMnP	očkovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgMnPc2	který
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
odpovědi	odpověď	k1gFnSc3	odpověď
na	na	k7c4	na
vakcínu	vakcína	k1gFnSc4	vakcína
<g/>
,	,	kIx,	,
a	a	k8xC	a
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
z	z	k7c2	z
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
důvodu	důvod	k1gInSc2	důvod
oslabený	oslabený	k2eAgInSc4d1	oslabený
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
(	(	kIx(	(
<g/>
imunodeficit	imunodeficita	k1gFnPc2	imunodeficita
<g/>
,	,	kIx,	,
těžké	těžký	k2eAgFnPc4d1	těžká
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
,	,	kIx,	,
stáří	stáří	k1gNnSc4	stáří
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nutnou	nutný	k2eAgFnSc7d1	nutná
podmínkou	podmínka	k1gFnSc7	podmínka
zachování	zachování	k1gNnSc2	zachování
této	tento	k3xDgFnSc2	tento
funkce	funkce	k1gFnSc2	funkce
kolektivní	kolektivní	k2eAgFnSc2d1	kolektivní
imunity	imunita	k1gFnSc2	imunita
je	být	k5eAaImIp3nS	být
vysoká	vysoký	k2eAgFnSc1d1	vysoká
míra	míra	k1gFnSc1	míra
proočkovanosti	proočkovanost	k1gFnSc2	proočkovanost
<g/>
;	;	kIx,	;
konkrétní	konkrétní	k2eAgFnSc1d1	konkrétní
hodnota	hodnota	k1gFnSc1	hodnota
požadované	požadovaný	k2eAgFnSc2d1	požadovaná
proočkovanosti	proočkovanost	k1gFnSc2	proočkovanost
závisí	záviset	k5eAaImIp3nS	záviset
zejména	zejména	k9	zejména
na	na	k7c4	na
nakažlivosti	nakažlivost	k1gFnPc4	nakažlivost
onemocnění	onemocnění	k1gNnPc2	onemocnění
a	a	k8xC	a
na	na	k7c6	na
efektivitě	efektivita	k1gFnSc6	efektivita
podávané	podávaný	k2eAgFnSc2d1	podávaná
vakcíny	vakcína	k1gFnSc2	vakcína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Očkování	očkování	k1gNnSc1	očkování
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Očkovací	očkovací	k2eAgInSc4d1	očkovací
kalendář	kalendář	k1gInSc4	kalendář
===	===	k?	===
</s>
</p>
<p>
<s>
Očkovací	očkovací	k2eAgInSc4d1	očkovací
kalendář	kalendář	k1gInSc4	kalendář
v	v	k7c6	v
ČR	ČR	kA	ČR
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
následující	následující	k2eAgNnSc4d1	následující
očkování	očkování	k1gNnSc4	očkování
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
povinné	povinný	k2eAgNnSc1d1	povinné
očkování	očkování	k1gNnSc1	očkování
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
černý	černý	k2eAgInSc1d1	černý
kašel	kašel	k1gInSc1	kašel
</s>
</p>
<p>
<s>
dětská	dětský	k2eAgFnSc1d1	dětská
obrna	obrna	k1gFnSc1	obrna
</s>
</p>
<p>
<s>
Haemophilus	Haemophilus	k1gInSc1	Haemophilus
influenzae	influenzae	k1gNnSc2	influenzae
typu	typ	k1gInSc2	typ
</s>
</p>
<p>
<s>
příušnice	příušnice	k1gFnPc1	příušnice
</s>
</p>
<p>
<s>
spalničky	spalničky	k1gFnPc1	spalničky
</s>
</p>
<p>
<s>
tetanus	tetanus	k1gInSc1	tetanus
</s>
</p>
<p>
<s>
zarděnky	zarděnky	k1gFnPc1	zarděnky
</s>
</p>
<p>
<s>
záškrt	záškrt	k1gInSc1	záškrt
</s>
</p>
<p>
<s>
žloutenka	žloutenka	k1gFnSc1	žloutenka
typu	typ	k1gInSc2	typ
B	B	kA	B
</s>
</p>
<p>
<s>
povinné	povinný	k2eAgNnSc1d1	povinné
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
s	s	k7c7	s
rizikem	riziko	k1gNnSc7	riziko
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
</s>
</p>
<p>
<s>
nepovinné	povinný	k2eNgNnSc1d1	nepovinné
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
lidské	lidský	k2eAgFnPc1d1	lidská
papilomaviry	papilomavira	k1gFnPc1	papilomavira
</s>
</p>
<p>
<s>
plané	planý	k2eAgFnPc1d1	planá
neštovice	neštovice	k1gFnPc1	neštovice
</s>
</p>
<p>
<s>
pneumokokové	pneumokokový	k2eAgFnPc1d1	pneumokoková
infekce	infekce	k1gFnPc1	infekce
</s>
</p>
<p>
<s>
rotavirové	rotavirový	k2eAgNnSc1d1	rotavirový
infekceOčkování	infekceOčkování	k1gNnSc1	infekceOčkování
je	být	k5eAaImIp3nS	být
stanovené	stanovený	k2eAgNnSc1d1	stanovené
jako	jako	k8xC	jako
povinné	povinný	k2eAgNnSc1d1	povinné
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
povinného	povinný	k2eAgInSc2d1	povinný
očkovacího	očkovací	k2eAgInSc2d1	očkovací
kalendáře	kalendář	k1gInSc2	kalendář
a	a	k8xC	a
předepsanou	předepsaný	k2eAgFnSc7d1	předepsaná
očkovací	očkovací	k2eAgFnSc7d1	očkovací
látkou	látka	k1gFnSc7	látka
<g/>
.	.	kIx.	.
</s>
<s>
Výjimky	výjimka	k1gFnPc1	výjimka
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
volby	volba	k1gFnSc2	volba
jiné	jiný	k2eAgFnSc2d1	jiná
látky	látka	k1gFnSc2	látka
jsou	být	k5eAaImIp3nP	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
látka	látka	k1gFnSc1	látka
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
v	v	k7c6	v
ČR	ČR	kA	ČR
registrovaná	registrovaný	k2eAgFnSc1d1	registrovaná
a	a	k8xC	a
náklady	náklad	k1gInPc4	náklad
hradí	hradit	k5eAaImIp3nS	hradit
rodič	rodič	k1gMnSc1	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Výjimky	výjimka	k1gFnPc1	výjimka
z	z	k7c2	z
očkovacího	očkovací	k2eAgNnSc2d1	očkovací
schématu	schéma	k1gNnSc2	schéma
jsou	být	k5eAaImIp3nP	být
možné	možný	k2eAgInPc1d1	možný
z	z	k7c2	z
lékařské	lékařský	k2eAgFnSc2d1	lékařská
indikace	indikace	k1gFnSc2	indikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Povinné	povinný	k2eAgNnSc1d1	povinné
očkování	očkování	k1gNnSc1	očkování
===	===	k?	===
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
jsou	být	k5eAaImIp3nP	být
některá	některý	k3yIgNnPc1	některý
očkování	očkování	k1gNnPc1	očkování
povinná	povinný	k2eAgFnSc1d1	povinná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
případě	případ	k1gInSc6	případ
povinných	povinný	k2eAgNnPc2d1	povinné
očkování	očkování	k1gNnPc2	očkování
rodiče	rodič	k1gMnPc1	rodič
své	svůj	k3xOyFgFnPc4	svůj
děti	dítě	k1gFnPc4	dítě
nechat	nechat	k5eAaPmF	nechat
očkovat	očkovat	k5eAaImF	očkovat
musí	muset	k5eAaImIp3nP	muset
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
za	za	k7c4	za
odepření	odepření	k1gNnSc4	odepření
očkování	očkování	k1gNnSc2	očkování
hrozí	hrozit	k5eAaImIp3nS	hrozit
pokuta	pokuta	k1gFnSc1	pokuta
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
by	by	kYmCp3nS	by
existovat	existovat	k5eAaImF	existovat
i	i	k9	i
způsob	způsob	k1gInSc4	způsob
odškodnění	odškodnění	k1gNnPc2	odškodnění
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
by	by	kYmCp3nS	by
toto	tento	k3xDgNnSc1	tento
povinné	povinný	k2eAgNnSc1d1	povinné
očkování	očkování	k1gNnSc1	očkování
mohlo	moct	k5eAaImAgNnS	moct
ublížit	ublížit	k5eAaPmF	ublížit
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
očkování	očkování	k1gNnSc6	očkování
novelizován	novelizovat	k5eAaBmNgInS	novelizovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
očkování	očkování	k1gNnSc1	očkování
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
samotného	samotný	k2eAgMnSc2d1	samotný
pacienta	pacient	k1gMnSc2	pacient
nebo	nebo	k8xC	nebo
rodičů	rodič	k1gMnPc2	rodič
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
odpůrců	odpůrce	k1gMnPc2	odpůrce
povinného	povinný	k2eAgNnSc2d1	povinné
očkování	očkování	k1gNnSc2	očkování
mohla	moct	k5eAaImAgFnS	moct
umožnit	umožnit	k5eAaPmF	umožnit
věta	věta	k1gFnSc1	věta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
obsažena	obsáhnout	k5eAaPmNgFnS	obsáhnout
v	v	k7c6	v
návrhu	návrh	k1gInSc6	návrh
novely	novela	k1gFnSc2	novela
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
se	se	k3xPyFc4	se
též	též	k9	též
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
zdravotní	zdravotní	k2eAgFnPc1d1	zdravotní
služby	služba	k1gFnPc1	služba
stanovené	stanovený	k2eAgFnPc1d1	stanovená
k	k	k7c3	k
předcházení	předcházení	k1gNnSc3	předcházení
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
šíření	šíření	k1gNnSc2	šíření
infekčních	infekční	k2eAgNnPc2d1	infekční
onemocnění	onemocnění	k1gNnPc2	onemocnění
zákonem	zákon	k1gInSc7	zákon
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
veřejného	veřejný	k2eAgNnSc2d1	veřejné
zdraví	zdraví	k1gNnSc2	zdraví
nebo	nebo	k8xC	nebo
v	v	k7c6	v
nezbytném	zbytný	k2eNgInSc6d1	zbytný
rozsahu	rozsah	k1gInSc6	rozsah
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
základě	základ	k1gInSc6	základ
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Do	do	k7c2	do
novely	novela	k1gFnSc2	novela
nebyly	být	k5eNaImAgInP	být
přijaty	přijmout	k5eAaPmNgInP	přijmout
návrhy	návrh	k1gInPc1	návrh
na	na	k7c4	na
zavedení	zavedení	k1gNnSc4	zavedení
individuálního	individuální	k2eAgInSc2d1	individuální
očkovacího	očkovací	k2eAgInSc2d1	očkovací
plánu	plán	k1gInSc2	plán
<g/>
,	,	kIx,	,
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
státu	stát	k1gInSc2	stát
za	za	k7c4	za
nežádoucí	žádoucí	k2eNgInPc4d1	nežádoucí
účinky	účinek	k1gInPc4	účinek
vakcín	vakcína	k1gFnPc2	vakcína
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
volby	volba	k1gFnSc2	volba
alternativní	alternativní	k2eAgFnSc2d1	alternativní
očkovací	očkovací	k2eAgFnSc2d1	očkovací
látky	látka	k1gFnSc2	látka
a	a	k8xC	a
povinnost	povinnost	k1gFnSc1	povinnost
lékaře	lékař	k1gMnSc2	lékař
informovat	informovat	k5eAaBmF	informovat
o	o	k7c4	o
možnosti	možnost	k1gFnPc4	možnost
výběru	výběr	k1gInSc2	výběr
očkovací	očkovací	k2eAgFnSc2d1	očkovací
látky	látka	k1gFnSc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Povinné	povinný	k2eAgNnSc1d1	povinné
očkování	očkování	k1gNnSc1	očkování
ale	ale	k8xC	ale
i	i	k9	i
po	po	k7c6	po
novelizaci	novelizace	k1gFnSc6	novelizace
zákona	zákon	k1gInSc2	zákon
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
<g/>
,	,	kIx,	,
u	u	k7c2	u
povinného	povinný	k2eAgNnSc2d1	povinné
očkování	očkování	k1gNnSc2	očkování
tak	tak	k9	tak
zákon	zákon	k1gInSc4	zákon
pod	pod	k7c7	pod
hrozbou	hrozba	k1gFnSc7	hrozba
sankcí	sankce	k1gFnSc7	sankce
udělení	udělení	k1gNnSc2	udělení
souhlasu	souhlas	k1gInSc2	souhlas
rodičů	rodič	k1gMnPc2	rodič
s	s	k7c7	s
očkováním	očkování	k1gNnSc7	očkování
vynucuje	vynucovat	k5eAaImIp3nS	vynucovat
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
novely	novela	k1gFnSc2	novela
zachoval	zachovat	k5eAaPmAgInS	zachovat
sankci	sankce	k1gFnSc4	sankce
až	až	k6eAd1	až
deset	deset	k4xCc4	deset
tisíc	tisíc	k4xCgInPc2	tisíc
Kč	Kč	kA	Kč
rodičům	rodič	k1gMnPc3	rodič
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
by	by	kYmCp3nP	by
svě	svě	k?	svě
dítě	dítě	k1gNnSc4	dítě
očkovat	očkovat	k5eAaImF	očkovat
nenechali	nechat	k5eNaPmAgMnP	nechat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kontroverze	kontroverze	k1gFnSc2	kontroverze
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Odpírání	odpírání	k1gNnSc3	odpírání
očkování	očkování	k1gNnSc3	očkování
===	===	k?	===
</s>
</p>
<p>
<s>
Praktická	praktický	k2eAgFnSc1d1	praktická
lékařka	lékařka	k1gFnSc1	lékařka
Ludmila	Ludmila	k1gFnSc1	Ludmila
Eleková	Eleková	k1gFnSc1	Eleková
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vakcíny	vakcína	k1gFnPc1	vakcína
jsou	být	k5eAaImIp3nP	být
nedostatečně	dostatečně	k6eNd1	dostatečně
otestované	otestovaný	k2eAgFnPc1d1	otestovaná
a	a	k8xC	a
že	že	k8xS	že
lékaři	lékař	k1gMnPc1	lékař
nejsou	být	k5eNaImIp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
proškoleni	proškolen	k2eAgMnPc1d1	proškolen
o	o	k7c6	o
nežádoucích	žádoucí	k2eNgInPc6d1	nežádoucí
účincích	účinek	k1gInPc6	účinek
vakcín	vakcína	k1gFnPc2	vakcína
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
za	za	k7c4	za
každé	každý	k3xTgNnSc4	každý
očkování	očkování	k1gNnSc4	očkování
je	být	k5eAaImIp3nS	být
lékař	lékař	k1gMnSc1	lékař
odměněn	odměněn	k2eAgMnSc1d1	odměněn
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c4	za
"	"	kIx"	"
<g/>
udatné	udatný	k2eAgNnSc4d1	udatné
tažení	tažení	k1gNnSc4	tažení
proti	proti	k7c3	proti
očkování	očkování	k1gNnSc3	očkování
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
Ludmila	Ludmila	k1gFnSc1	Ludmila
Eleková	Eleková	k1gFnSc1	Eleková
oceněna	ocenit	k5eAaPmNgFnS	ocenit
anticenou	anticený	k2eAgFnSc7d1	anticený
Bludný	bludný	k2eAgInSc1d1	bludný
balvan	balvan	k1gInSc1	balvan
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Podle	podle	k7c2	podle
ortopeda	ortoped	k1gMnSc2	ortoped
Jana	Jan	k1gMnSc2	Jan
Vavrečky	Vavrečka	k1gFnSc2	Vavrečka
neexistuje	existovat	k5eNaImIp3nS	existovat
metoda	metoda	k1gFnSc1	metoda
stanovení	stanovení	k1gNnSc2	stanovení
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
od	od	k7c2	od
které	který	k3yRgFnSc2	který
má	mít	k5eAaImIp3nS	mít
očkování	očkování	k1gNnSc1	očkování
smysl	smysl	k1gInSc1	smysl
<g/>
,	,	kIx,	,
od	od	k7c2	od
které	který	k3yRgFnSc2	který
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
plošná	plošný	k2eAgNnPc4d1	plošné
očkování	očkování	k1gNnPc4	očkování
povinná	povinný	k2eAgNnPc4d1	povinné
a	a	k8xC	a
od	od	k7c2	od
které	který	k3yRgFnSc2	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
očkování	očkování	k1gNnSc1	očkování
vynucováno	vynucován	k2eAgNnSc1d1	vynucováno
<g/>
,	,	kIx,	,
a	a	k8xC	a
ani	ani	k8xC	ani
o	o	k7c6	o
těchto	tento	k3xDgFnPc6	tento
hranicích	hranice	k1gFnPc6	hranice
neexistuje	existovat	k5eNaImIp3nS	existovat
diskuse	diskuse	k1gFnSc1	diskuse
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
podle	podle	k7c2	podle
Vavrečky	Vavrečka	k1gFnSc2	Vavrečka
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
pro	pro	k7c4	pro
některé	některý	k3yIgFnPc4	některý
skupiny	skupina	k1gFnPc4	skupina
nežádoucí	žádoucí	k2eNgFnPc4d1	nežádoucí
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Vavrečky	Vavrečka	k1gFnSc2	Vavrečka
také	také	k9	také
nejsou	být	k5eNaImIp3nP	být
dostupné	dostupný	k2eAgFnPc1d1	dostupná
odpovědi	odpověď	k1gFnPc1	odpověď
o	o	k7c6	o
užitečnosti	užitečnost	k1gFnSc6	užitečnost
a	a	k8xC	a
přínosu	přínos	k1gInSc6	přínos
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
očkování	očkování	k1gNnPc2	očkování
jak	jak	k8xS	jak
absolutně	absolutně	k6eAd1	absolutně
tak	tak	k9	tak
relativně	relativně	k6eAd1	relativně
vůči	vůči	k7c3	vůči
jiným	jiný	k2eAgNnPc3d1	jiné
očkováním	očkování	k1gNnPc3	očkování
a	a	k8xC	a
nikdo	nikdo	k3yNnSc1	nikdo
se	se	k3xPyFc4	se
nesnaží	snažit	k5eNaImIp3nS	snažit
význam	význam	k1gInSc4	význam
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
očkování	očkování	k1gNnPc2	očkování
kvantitativně	kvantitativně	k6eAd1	kvantitativně
určit	určit	k5eAaPmF	určit
a	a	k8xC	a
jako	jako	k8xS	jako
argument	argument	k1gInSc1	argument
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
odpověď	odpověď	k1gFnSc1	odpověď
<g/>
,	,	kIx,	,
že	že	k8xS	že
přínos	přínos	k1gInSc1	přínos
očkování	očkování	k1gNnSc2	očkování
stále	stále	k6eAd1	stále
převažuje	převažovat	k5eAaImIp3nS	převažovat
nad	nad	k7c7	nad
riziky	riziko	k1gNnPc7	riziko
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
názoru	názor	k1gInSc2	názor
Davida	David	k1gMnSc2	David
Broniatowského	Broniatowský	k1gMnSc2	Broniatowský
polemika	polemik	k1gMnSc2	polemik
o	o	k7c6	o
očkování	očkování	k1gNnSc6	očkování
je	být	k5eAaImIp3nS	být
údajně	údajně	k6eAd1	údajně
součástí	součást	k1gFnSc7	součást
hybridní	hybridní	k2eAgFnSc2d1	hybridní
války	válka	k1gFnSc2	válka
ruské	ruský	k2eAgFnSc2d1	ruská
vlády	vláda	k1gFnSc2	vláda
proti	proti	k7c3	proti
západním	západní	k2eAgFnPc3d1	západní
demokraciím	demokracie	k1gFnPc3	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
článku	článek	k1gInSc6	článek
zveřejněném	zveřejněný	k2eAgInSc6d1	zveřejněný
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
vědeckém	vědecký	k2eAgInSc6d1	vědecký
časopisu	časopis	k1gInSc6	časopis
American	American	k1gMnSc1	American
Journal	Journal	k1gMnSc1	Journal
of	of	k?	of
Public	publicum	k1gNnPc2	publicum
Health	Health	k1gInSc4	Health
autoři	autor	k1gMnPc1	autor
popsali	popsat	k5eAaPmAgMnP	popsat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
roboty	robot	k1gInPc1	robot
a	a	k8xC	a
šiřitelé	šiřitel	k1gMnPc1	šiřitel
spamu	spam	k1gInSc2	spam
a	a	k8xC	a
malwaru	malwar	k1gInSc2	malwar
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
content	content	k1gInSc1	content
polluters	polluters	k1gInSc1	polluters
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nejprve	nejprve	k6eAd1	nejprve
šíří	šířit	k5eAaImIp3nP	šířit
antivakcinační	antivakcinační	k2eAgFnPc1d1	antivakcinační
pověry	pověra	k1gFnPc1	pověra
a	a	k8xC	a
údajní	údajný	k2eAgMnPc1d1	údajný
ruští	ruský	k2eAgMnPc1d1	ruský
internetoví	internetový	k2eAgMnPc1d1	internetový
trollové	troll	k1gMnPc1	troll
negativně	negativně	k6eAd1	negativně
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
do	do	k7c2	do
veřejné	veřejný	k2eAgFnSc2d1	veřejná
debaty	debata	k1gFnSc2	debata
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
uměle	uměle	k6eAd1	uměle
amplifikují	amplifikovat	k5eAaImIp3nP	amplifikovat
tyto	tento	k3xDgInPc4	tento
rozpory	rozpor	k1gInPc4	rozpor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Reakce	reakce	k1gFnPc1	reakce
na	na	k7c4	na
odpírání	odpírání	k1gNnSc4	odpírání
očkování	očkování	k1gNnSc2	očkování
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
německého	německý	k2eAgMnSc2d1	německý
ministra	ministr	k1gMnSc2	ministr
zdravotnictví	zdravotnictví	k1gNnSc2	zdravotnictví
Hermanna	Hermann	k1gMnSc2	Hermann
Gröhea	Gröheus	k1gMnSc2	Gröheus
odpůrci	odpůrce	k1gMnPc1	odpůrce
očkování	očkování	k1gNnSc4	očkování
nezodpovědně	zodpovědně	k6eNd1	zodpovědně
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
strach	strach	k1gInSc4	strach
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
není	být	k5eNaImIp3nS	být
racionální	racionální	k2eAgInSc1d1	racionální
<g/>
,	,	kIx,	,
a	a	k8xC	a
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
odepřou	odepřít	k5eAaPmIp3nP	odepřít
očkování	očkování	k1gNnSc4	očkování
vlastním	vlastní	k2eAgFnPc3d1	vlastní
dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
vystavují	vystavovat	k5eAaImIp3nP	vystavovat
riziku	riziko	k1gNnSc3	riziko
nejen	nejen	k6eAd1	nejen
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
ostatní	ostatní	k2eAgFnPc1d1	ostatní
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
<g/>
Novinářka	novinářka	k1gFnSc1	novinářka
Ludmila	Ludmila	k1gFnSc1	Ludmila
Hamplová	Hamplová	k1gFnSc1	Hamplová
upozornila	upozornit	k5eAaPmAgFnS	upozornit
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
článku	článek	k1gInSc6	článek
z	z	k7c2	z
června	červen	k1gInSc2	červen
<g />
.	.	kIx.	.
</s>
<s>
2015	[number]	k4	2015
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
odpírači	odpírač	k1gMnPc1	odpírač
očkování	očkování	k1gNnSc2	očkování
zkreslují	zkreslovat	k5eAaImIp3nP	zkreslovat
informace	informace	k1gFnPc4	informace
o	o	k7c4	o
úmrtí	úmrtí	k1gNnSc4	úmrtí
na	na	k7c4	na
spalničky	spalničky	k1gFnPc4	spalničky
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
na	na	k7c4	na
nemoc	nemoc	k1gFnSc4	nemoc
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
které	který	k3yIgFnSc3	který
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
chránit	chránit	k5eAaImF	chránit
očkováním	očkování	k1gNnSc7	očkování
<g/>
.	.	kIx.	.
<g/>
Roman	Roman	k1gMnSc1	Roman
Prymula	Prymul	k1gMnSc2	Prymul
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Anna	Anna	k1gFnSc1	Anna
Strunecká	Strunecký	k2eAgFnSc1d1	Strunecká
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
textu	text	k1gInSc6	text
o	o	k7c6	o
rizicích	rizice	k1gFnPc6	rizice
očkování	očkování	k1gNnSc2	očkování
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2015	[number]	k4	2015
rozeslala	rozeslat	k5eAaPmAgFnS	rozeslat
poslancům	poslanec	k1gMnPc3	poslanec
jako	jako	k8xS	jako
odborné	odborný	k2eAgNnSc4d1	odborné
stanovisko	stanovisko	k1gNnSc4	stanovisko
k	k	k7c3	k
chystané	chystaný	k2eAgFnSc3d1	chystaná
diskuzi	diskuze	k1gFnSc3	diskuze
o	o	k7c6	o
očkování	očkování	k1gNnSc6	očkování
<g/>
,	,	kIx,	,
uvádí	uvádět	k5eAaImIp3nS	uvádět
řadu	řada	k1gFnSc4	řada
tvrzení	tvrzení	k1gNnSc2	tvrzení
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
k	k	k7c3	k
seriózním	seriózní	k2eAgFnPc3d1	seriózní
informacím	informace	k1gFnPc3	informace
velmi	velmi	k6eAd1	velmi
daleko	daleko	k6eAd1	daleko
<g/>
..	..	k?	..
</s>
</p>
<p>
<s>
Novinář	novinář	k1gMnSc1	novinář
Erik	Erik	k1gMnSc1	Erik
Ose	osa	k1gFnSc3	osa
uvedl	uvést	k5eAaPmAgMnS	uvést
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
článku	článek	k1gInSc6	článek
publikovaném	publikovaný	k2eAgInSc6d1	publikovaný
v	v	k7c4	v
Huffingtom	Huffingtom	k1gInSc4	Huffingtom
Post	posta	k1gFnPc2	posta
několik	několik	k4yIc1	několik
jmen	jméno	k1gNnPc2	jméno
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
v	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gMnPc1	jejich
rodiče	rodič	k1gMnPc1	rodič
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
kampani	kampaň	k1gFnSc3	kampaň
odpíračů	odpírač	k1gMnPc2	odpírač
očkování	očkování	k1gNnSc2	očkování
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
argumentaci	argumentace	k1gFnSc4	argumentace
některých	některý	k3yIgMnPc2	některý
mediálně	mediálně	k6eAd1	mediálně
známých	známý	k2eAgMnPc2d1	známý
odpíračů	odpírač	k1gMnPc2	odpírač
očkování	očkování	k1gNnSc2	očkování
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
celebrit	celebrita	k1gFnPc2	celebrita
v	v	k7c6	v
USA	USA	kA	USA
píše	psát	k5eAaImIp3nS	psát
bez	bez	k7c2	bez
okolků	okolek	k1gInPc2	okolek
jako	jako	k9	jako
o	o	k7c4	o
šíření	šíření	k1gNnSc4	šíření
lží	lež	k1gFnPc2	lež
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
očkování	očkování	k1gNnSc2	očkování
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
očkování	očkování	k1gNnSc2	očkování
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Vakciny	Vakcin	k1gInPc1	Vakcin
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
očkování	očkování	k1gNnSc6	očkování
a	a	k8xC	a
ochraně	ochrana	k1gFnSc6	ochrana
proti	proti	k7c3	proti
onemocněním	onemocnění	k1gNnSc7	onemocnění
pro	pro	k7c4	pro
širokou	široký	k2eAgFnSc4d1	široká
veřejnost	veřejnost	k1gFnSc4	veřejnost
v	v	k7c6	v
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
Očkování	očkování	k1gNnSc1	očkování
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
informační	informační	k2eAgInSc1d1	informační
server	server	k1gInSc1	server
<g/>
,	,	kIx,	,
přehled	přehled	k1gInSc1	přehled
povinného	povinný	k2eAgNnSc2d1	povinné
a	a	k8xC	a
dobrovolného	dobrovolný	k2eAgNnSc2d1	dobrovolné
očkování	očkování	k1gNnSc2	očkování
<g/>
,	,	kIx,	,
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
očkování	očkování	k1gNnSc6	očkování
pro	pro	k7c4	pro
cestování	cestování	k1gNnSc4	cestování
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
zemí	zem	k1gFnPc2	zem
</s>
</p>
<p>
<s>
www.szu.cz/tema/vakciny	www.szu.cz/tema/vakcin	k1gInPc1	www.szu.cz/tema/vakcin
–	–	k?	–
Očkování	očkování	k1gNnPc4	očkování
a	a	k8xC	a
vakcíny	vakcína	k1gFnPc4	vakcína
na	na	k7c6	na
webu	web	k1gInSc6	web
Státního	státní	k2eAgInSc2d1	státní
zdravotního	zdravotní	k2eAgInSc2d1	zdravotní
ústavu	ústav	k1gInSc2	ústav
</s>
</p>
<p>
<s>
Michal	Michal	k1gMnSc1	Michal
Křupka	křupka	k1gFnSc1	křupka
<g/>
:	:	kIx,	:
Mýty	mýtus	k1gInPc1	mýtus
a	a	k8xC	a
legendy	legenda	k1gFnPc1	legenda
antivakcinačního	antivakcinační	k2eAgNnSc2d1	antivakcinační
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
.	.	kIx.	.
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
