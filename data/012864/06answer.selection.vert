<s>
Očkování	očkování	k1gNnSc1	očkování
nevede	vést	k5eNaImIp3nS	vést
vždy	vždy	k6eAd1	vždy
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
imunity	imunita	k1gFnSc2	imunita
<g/>
,	,	kIx,	,
ne	ne	k9	ne
každý	každý	k3xTgMnSc1	každý
očkovaný	očkovaný	k2eAgMnSc1d1	očkovaný
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
chráněn	chráněn	k2eAgMnSc1d1	chráněn
před	před	k7c7	před
infekcí	infekce	k1gFnSc7	infekce
<g/>
.	.	kIx.	.
</s>
