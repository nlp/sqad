<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
jediným	jediný	k2eAgMnSc7d1	jediný
zbývajícím	zbývající	k2eAgMnSc7d1	zbývající
nepřítelem	nepřítel	k1gMnSc7	nepřítel
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
osamocená	osamocený	k2eAgFnSc1d1	osamocená
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
nový	nový	k2eAgMnSc1d1	nový
ministerský	ministerský	k2eAgMnSc1d1	ministerský
předseda	předseda	k1gMnSc1	předseda
Winston	Winston	k1gInSc4	Winston
Churchill	Churchill	k1gMnSc1	Churchill
sliboval	slibovat	k5eAaImAgMnS	slibovat
Britům	Brit	k1gMnPc3	Brit
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
burcujících	burcující	k2eAgInPc6d1	burcující
projevech	projev	k1gInPc6	projev
pouze	pouze	k6eAd1	pouze
"	"	kIx"	"
<g/>
krev	krev	k1gFnSc1	krev
<g/>
,	,	kIx,	,
pot	pot	k1gInSc1	pot
a	a	k8xC	a
slzy	slza	k1gFnPc1	slza
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
