<s>
Koala	koala	k1gFnSc1	koala
medvídkovitý	medvídkovitý	k2eAgMnSc1d1	medvídkovitý
(	(	kIx(	(
<g/>
Phascolarctos	Phascolarctos	k1gMnSc1	Phascolarctos
cinereus	cinereus	k1gMnSc1	cinereus
<g/>
,	,	kIx,	,
Goldfuss	Goldfuss	k1gInSc1	Goldfuss
<g/>
,	,	kIx,	,
1817	[number]	k4	1817
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
také	také	k9	také
pod	pod	k7c7	pod
nesprávným	správný	k2eNgInSc7d1	nesprávný
názvem	název	k1gInSc7	název
jako	jako	k8xC	jako
medvídek	medvídek	k1gMnSc1	medvídek
koala	koala	k1gFnSc1	koala
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgNnPc2d3	nejznámější
australských	australský	k2eAgNnPc2d1	Australské
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
vzhledem	vzhled	k1gInSc7	vzhled
opravdu	opravdu	k6eAd1	opravdu
připomíná	připomínat	k5eAaImIp3nS	připomínat
medvěda	medvěd	k1gMnSc4	medvěd
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
vačnatec	vačnatec	k1gMnSc1	vačnatec
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
třeba	třeba	k6eAd1	třeba
klokan	klokan	k1gMnSc1	klokan
nebo	nebo	k8xC	nebo
vakoveverka	vakoveverka	k1gFnSc1	vakoveverka
létavá	létavý	k2eAgFnSc1d1	létavá
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgMnSc7d1	jediný
známým	známý	k2eAgMnSc7d1	známý
zástupcem	zástupce	k1gMnSc7	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
koalovitých	koalovitý	k2eAgInPc2d1	koalovitý
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
koala	koala	k1gFnSc1	koala
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
gula	gula	k1gFnSc1	gula
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
dharuk	dharuk	k6eAd1	dharuk
(	(	kIx(	(
<g/>
téměř	téměř	k6eAd1	téměř
vymizelý	vymizelý	k2eAgInSc1d1	vymizelý
jazyk	jazyk	k1gInSc1	jazyk
domorodých	domorodý	k2eAgMnPc2d1	domorodý
obyvatel	obyvatel	k1gMnPc2	obyvatel
Austrálie	Austrálie	k1gFnSc2	Austrálie
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Sydney	Sydney	k1gNnSc2	Sydney
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
samohláska	samohláska	k1gFnSc1	samohláska
/	/	kIx~	/
<g/>
u	u	k7c2	u
<g/>
/	/	kIx~	/
původně	původně	k6eAd1	původně
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
psala	psát	k5eAaImAgFnS	psát
jako	jako	k9	jako
"	"	kIx"	"
<g/>
oo	oo	k?	oo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ve	v	k7c6	v
slovech	slovo	k1gNnPc6	slovo
jako	jako	k8xC	jako
coola	coola	k1gFnSc1	coola
a	a	k8xC	a
koolah	koolah	k1gInSc1	koolah
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
změnila	změnit	k5eAaPmAgFnS	změnit
se	se	k3xPyFc4	se
na	na	k7c4	na
"	"	kIx"	"
<g/>
oa	oa	k?	oa
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
možná	možná	k9	možná
kvůli	kvůli	k7c3	kvůli
chybě	chyba	k1gFnSc3	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Nesprávně	správně	k6eNd1	správně
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
slovo	slovo	k1gNnSc4	slovo
koala	koala	k1gFnSc1	koala
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
nepije	pít	k5eNaImIp3nS	pít
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Systematické	systematický	k2eAgNnSc1d1	systematické
jméno	jméno	k1gNnSc1	jméno
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgMnSc3	který
koala	koala	k1gFnSc1	koala
náleží	náležet	k5eAaImIp3nS	náležet
<g/>
,	,	kIx,	,
Phascolarctos	Phascolarctos	k1gInSc1	Phascolarctos
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
phascolos	phascolos	k1gInSc1	phascolos
"	"	kIx"	"
<g/>
vak	vak	k1gInSc1	vak
<g/>
"	"	kIx"	"
a	a	k8xC	a
arktos	arktos	k1gMnSc1	arktos
"	"	kIx"	"
<g/>
medvěd	medvěd	k1gMnSc1	medvěd
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Druhové	druhový	k2eAgNnSc1d1	druhové
jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
cinereus	cinereus	k1gInSc1	cinereus
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
latinského	latinský	k2eAgInSc2d1	latinský
původu	původ	k1gInSc2	původ
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
popelavý	popelavý	k2eAgMnSc1d1	popelavý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
medvěda	medvěd	k1gMnSc4	medvěd
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
mluvící	mluvící	k2eAgMnPc1d1	mluvící
osadníci	osadník	k1gMnPc1	osadník
na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
toto	tento	k3xDgNnSc4	tento
zvíře	zvíře	k1gNnSc4	zvíře
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
medvídek	medvídek	k1gMnSc1	medvídek
<g/>
/	/	kIx~	/
<g/>
medvěd	medvěd	k1gMnSc1	medvěd
koala	koala	k1gFnSc1	koala
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gInSc3	jeho
vnější	vnější	k2eAgFnPc4d1	vnější
podobnosti	podobnost	k1gFnPc4	podobnost
s	s	k7c7	s
medvědy	medvěd	k1gMnPc7	medvěd
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
medvídek	medvídek	k1gMnSc1	medvídek
koala	koala	k1gFnSc1	koala
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
Austrálii	Austrálie	k1gFnSc4	Austrálie
stále	stále	k6eAd1	stále
používá	používat	k5eAaImIp3nS	používat
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
taxonomicky	taxonomicky	k6eAd1	taxonomicky
nesprávné	správný	k2eNgNnSc1d1	nesprávné
a	a	k8xC	a
odborníci	odborník	k1gMnPc1	odborník
veřejnost	veřejnost	k1gFnSc4	veřejnost
odrazují	odrazovat	k5eAaImIp3nP	odrazovat
od	od	k7c2	od
jeho	jeho	k3xOp3gNnSc2	jeho
používání	používání	k1gNnSc2	používání
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
pojmenování	pojmenování	k1gNnSc1	pojmenování
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c4	na
podobnosti	podobnost	k1gFnPc4	podobnost
s	s	k7c7	s
medvědy	medvěd	k1gMnPc7	medvěd
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
opičí	opičí	k2eAgMnSc1d1	opičí
medvídek	medvídek	k1gMnSc1	medvídek
<g/>
,	,	kIx,	,
domorodý	domorodý	k2eAgMnSc1d1	domorodý
medvídek	medvídek	k1gMnSc1	medvídek
a	a	k8xC	a
stromový	stromový	k2eAgMnSc1d1	stromový
medvídek	medvídek	k1gMnSc1	medvídek
<g/>
.	.	kIx.	.
</s>
<s>
Koalové	Koal	k1gMnPc1	Koal
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
oblastech	oblast	k1gFnPc6	oblast
východní	východní	k2eAgFnSc2d1	východní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
od	od	k7c2	od
Adelaide	Adelaid	k1gInSc5	Adelaid
až	až	k9	až
k	k	k7c3	k
jižní	jižní	k2eAgFnSc3d1	jižní
části	část	k1gFnSc3	část
poloostrova	poloostrov	k1gInSc2	poloostrov
Cape	capat	k5eAaImIp3nS	capat
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
dostatečnou	dostatečný	k2eAgFnSc7d1	dostatečná
vlhkostí	vlhkost	k1gFnSc7	vlhkost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
podmínkou	podmínka	k1gFnSc7	podmínka
růstu	růst	k1gInSc2	růst
vhodných	vhodný	k2eAgInPc2d1	vhodný
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
populace	populace	k1gFnPc1	populace
koalů	koal	k1gInPc2	koal
také	také	k9	také
výrazně	výrazně	k6eAd1	výrazně
rozšířeny	rozšířit	k5eAaPmNgFnP	rozšířit
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc6	století
byli	být	k5eAaImAgMnP	být
koalové	koal	k1gMnPc1	koal
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Austrálii	Austrálie	k1gFnSc6	Austrálie
prakticky	prakticky	k6eAd1	prakticky
vyhubeni	vyhuben	k2eAgMnPc1d1	vyhuben
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
vymizelé	vymizelý	k2eAgFnPc1d1	vymizelá
populace	populace	k1gFnPc1	populace
nahrazeny	nahrazen	k2eAgFnPc1d1	nahrazena
jedinci	jedinec	k1gMnSc3	jedinec
pocházejícími	pocházející	k2eAgFnPc7d1	pocházející
z	z	k7c2	z
Victorie	Victorie	k1gFnSc2	Victorie
<g/>
.	.	kIx.	.
</s>
<s>
Koalové	Koal	k1gMnPc1	Koal
se	se	k3xPyFc4	se
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
v	v	k7c6	v
Tasmánii	Tasmánie	k1gFnSc6	Tasmánie
a	a	k8xC	a
Západní	západní	k2eAgFnSc3d1	západní
Austrálii	Austrálie	k1gFnSc3	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
objasněn	objasnit	k5eAaPmNgInS	objasnit
<g/>
;	;	kIx,	;
nejpravděpodobněji	pravděpodobně	k6eAd3	pravděpodobně
se	se	k3xPyFc4	se
však	však	k9	však
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
ze	z	k7c2	z
suchozemských	suchozemský	k2eAgNnPc2d1	suchozemské
zvířat	zvíře	k1gNnPc2	zvíře
podobných	podobný	k2eAgNnPc2d1	podobné
vombatům	vombat	k1gMnPc3	vombat
<g/>
.	.	kIx.	.
</s>
<s>
Zkameněliny	zkamenělina	k1gFnPc1	zkamenělina
koalů	koal	k1gMnPc2	koal
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
vzácné	vzácný	k2eAgInPc1d1	vzácný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
několik	několik	k4yIc1	několik
jich	on	k3xPp3gFnPc2	on
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
20	[number]	k4	20
miliony	milion	k4xCgInPc7	milion
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
severní	severní	k2eAgFnSc1d1	severní
polovina	polovina	k1gFnSc1	polovina
Austrálie	Austrálie	k1gFnSc1	Austrálie
porostlá	porostlý	k2eAgFnSc1d1	porostlá
deštnými	deštný	k2eAgInPc7d1	deštný
pralesy	prales	k1gInPc7	prales
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
potravní	potravní	k2eAgFnSc3d1	potravní
specializaci	specializace	k1gFnSc3	specializace
koalů	koal	k1gMnPc2	koal
na	na	k7c4	na
blahovičníky	blahovičník	k1gInPc4	blahovičník
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
po	po	k7c4	po
ochlazení	ochlazení	k1gNnSc4	ochlazení
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
deštné	deštný	k2eAgInPc4d1	deštný
pralesy	prales	k1gInPc4	prales
vystřídány	vystřídán	k2eAgInPc4d1	vystřídán
blahovičníkovými	blahovičníkový	k2eAgInPc7d1	blahovičníkový
lesy	les	k1gInPc7	les
<g/>
.	.	kIx.	.
</s>
<s>
Fosilní	fosilní	k2eAgInPc1d1	fosilní
nálezy	nález	k1gInPc1	nález
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
50	[number]	k4	50
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
také	také	k6eAd1	také
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgFnP	být
jižní	jižní	k2eAgFnPc1d1	jižní
oblasti	oblast	k1gFnPc1	oblast
Austrálie	Austrálie	k1gFnSc2	Austrálie
obývány	obýván	k2eAgInPc1d1	obýván
obřími	obří	k2eAgInPc7d1	obří
koaly	koala	k1gFnPc4	koala
<g/>
.	.	kIx.	.
</s>
<s>
Koala	koala	k1gFnSc1	koala
hraje	hrát	k5eAaImIp3nS	hrát
z	z	k7c2	z
ekologického	ekologický	k2eAgNnSc2d1	ekologické
hlediska	hledisko	k1gNnSc2	hledisko
stejnou	stejný	k2eAgFnSc4d1	stejná
roli	role	k1gFnSc4	role
jako	jako	k8xS	jako
jihoamerický	jihoamerický	k2eAgMnSc1d1	jihoamerický
lenochod	lenochod	k1gMnSc1	lenochod
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
kg	kg	kA	kg
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
70	[number]	k4	70
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
cm	cm	kA	cm
Délka	délka	k1gFnSc1	délka
ocasu	ocas	k1gInSc2	ocas
<g/>
:	:	kIx,	:
6	[number]	k4	6
cm	cm	kA	cm
Koalové	Koal	k1gMnPc1	Koal
jsou	být	k5eAaImIp3nP	být
zavalitá	zavalitý	k2eAgNnPc4d1	zavalité
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
delší	dlouhý	k2eAgNnPc4d2	delší
uši	ucho	k1gNnPc4	ucho
a	a	k8xC	a
končetiny	končetina	k1gFnPc4	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
hustá	hustý	k2eAgFnSc1d1	hustá
a	a	k8xC	a
měkká	měkký	k2eAgFnSc1d1	měkká
<g/>
,	,	kIx,	,
v	v	k7c6	v
odstínech	odstín	k1gInPc6	odstín
od	od	k7c2	od
popelavě	popelavě	k6eAd1	popelavě
šedé	šedý	k2eAgFnSc2d1	šedá
k	k	k7c3	k
hnědavé	hnědavý	k2eAgFnSc3d1	hnědavá
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
břichu	břicho	k1gNnSc6	břicho
a	a	k8xC	a
ušních	ušní	k2eAgInPc6d1	ušní
boltcích	boltec	k1gInPc6	boltec
jsou	být	k5eAaImIp3nP	být
bílé	bílý	k2eAgInPc1d1	bílý
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
chlupy	chlup	k1gInPc1	chlup
<g/>
.	.	kIx.	.
</s>
<s>
Koala	koala	k1gFnSc1	koala
se	s	k7c7	s
vzhledem	vzhled	k1gInSc7	vzhled
velice	velice	k6eAd1	velice
podobá	podobat	k5eAaImIp3nS	podobat
vombatovi	vombat	k1gMnSc3	vombat
(	(	kIx(	(
<g/>
nejbližší	blízký	k2eAgMnSc1d3	nejbližší
nevyhynulý	vyhynulý	k2eNgMnSc1d1	vyhynulý
příbuzný	příbuzný	k1gMnSc1	příbuzný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
hustší	hustý	k2eAgFnSc1d2	hustší
srst	srst	k1gFnSc1	srst
<g/>
,	,	kIx,	,
několikanásobně	několikanásobně	k6eAd1	několikanásobně
větší	veliký	k2eAgNnPc4d2	veliký
uši	ucho	k1gNnPc4	ucho
a	a	k8xC	a
delší	dlouhý	k2eAgFnPc4d2	delší
končetiny	končetina	k1gFnPc4	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c4	mezi
12	[number]	k4	12
kg	kg	kA	kg
u	u	k7c2	u
mohutného	mohutný	k2eAgInSc2d1	mohutný
samce	samec	k1gInSc2	samec
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
5	[number]	k4	5
kg	kg	kA	kg
u	u	k7c2	u
drobné	drobný	k2eAgFnSc2d1	drobná
samice	samice	k1gFnSc2	samice
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
šplhání	šplhání	k1gNnSc6	šplhání
ve	v	k7c6	v
stromech	strom	k1gInPc6	strom
mu	on	k3xPp3gNnSc3	on
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
velké	velká	k1gFnPc4	velká
<g/>
,	,	kIx,	,
ostré	ostrý	k2eAgInPc4d1	ostrý
drápy	dráp	k1gInPc4	dráp
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
končetině	končetina	k1gFnSc6	končetina
najdeme	najít	k5eAaPmIp1nP	najít
pět	pět	k4xCc4	pět
prstů	prst	k1gInPc2	prst
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
končetinách	končetina	k1gFnPc6	končetina
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc4	dva
palce	palec	k1gInPc4	palec
stojící	stojící	k2eAgFnSc2d1	stojící
proti	proti	k7c3	proti
ostatním	ostatní	k2eAgInPc3d1	ostatní
třem	tři	k4xCgInPc3	tři
prstům	prst	k1gInPc3	prst
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
lepší	dobrý	k2eAgNnSc4d2	lepší
uchopení	uchopení	k1gNnSc4	uchopení
větve	větev	k1gFnSc2	větev
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
končetině	končetina	k1gFnSc6	končetina
je	být	k5eAaImIp3nS	být
protistojný	protistojný	k2eAgInSc1d1	protistojný
palec	palec	k1gInSc1	palec
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgMnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Koala	koala	k1gFnSc1	koala
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
savců	savec	k1gMnPc2	savec
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
primátů	primát	k1gMnPc2	primát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
otisky	otisk	k1gInPc4	otisk
prstů	prst	k1gInPc2	prst
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgInPc1d1	podobný
lidským	lidský	k2eAgInSc7d1	lidský
otiskům	otisk	k1gInPc3	otisk
prstů	prst	k1gInPc2	prst
–	–	k?	–
jejich	jejich	k3xOp3gNnSc1	jejich
odlišení	odlišení	k1gNnSc1	odlišení
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
docela	docela	k6eAd1	docela
obtížné	obtížný	k2eAgNnSc1d1	obtížné
i	i	k9	i
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
elektronového	elektronový	k2eAgInSc2d1	elektronový
mikroskopu	mikroskop	k1gInSc2	mikroskop
<g/>
.	.	kIx.	.
</s>
<s>
Zuby	zub	k1gInPc1	zub
koaly	koala	k1gFnSc2	koala
jsou	být	k5eAaImIp3nP	být
přizpůsobeny	přizpůsobit	k5eAaPmNgInP	přizpůsobit
býložravému	býložravý	k2eAgInSc3d1	býložravý
způsobu	způsob	k1gInSc3	způsob
obživy	obživa	k1gFnSc2	obživa
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgFnPc4d1	podobná
zubům	zub	k1gInPc3	zub
dalších	další	k2eAgInPc2d1	další
druhů	druh	k1gInPc2	druh
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
dvojitozubců	dvojitozubec	k1gInPc2	dvojitozubec
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vombat	vombat	k1gMnSc1	vombat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostrý	ostrý	k2eAgMnSc1d1	ostrý
pár	pár	k4xCyI	pár
řezáků	řezák	k1gInPc2	řezák
určených	určený	k2eAgInPc2d1	určený
k	k	k7c3	k
uštipování	uštipování	k1gNnSc3	uštipování
listů	list	k1gInPc2	list
je	být	k5eAaImIp3nS	být
oddělen	oddělit	k5eAaPmNgInS	oddělit
od	od	k7c2	od
stoliček	stolička	k1gFnPc2	stolička
širokou	široký	k2eAgFnSc7d1	široká
mezerou	mezera	k1gFnSc7	mezera
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gInSc1	samec
koaly	koala	k1gFnSc2	koala
má	mít	k5eAaImIp3nS	mít
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
vačnatců	vačnatec	k1gMnPc2	vačnatec
<g/>
,	,	kIx,	,
rozeklaný	rozeklaný	k2eAgInSc1d1	rozeklaný
penis	penis	k1gInSc1	penis
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
oddělené	oddělený	k2eAgFnPc4d1	oddělená
dělohy	děloha	k1gFnPc4	děloha
<g/>
,	,	kIx,	,
každou	každý	k3xTgFnSc4	každý
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
boční	boční	k2eAgFnSc7d1	boční
pochvou	pochva	k1gFnSc7	pochva
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
společný	společný	k2eAgInSc1d1	společný
znak	znak	k1gInSc1	znak
všech	všecek	k3xTgMnPc2	všecek
vačnatců	vačnatec	k1gMnPc2	vačnatec
<g/>
.	.	kIx.	.
</s>
<s>
Mozek	mozek	k1gInSc1	mozek
předchůdců	předchůdce	k1gMnPc2	předchůdce
dnešních	dnešní	k2eAgFnPc2d1	dnešní
koal	koala	k1gFnPc2	koala
vyplňoval	vyplňovat	k5eAaImAgInS	vyplňovat
celou	celý	k2eAgFnSc4d1	celá
dutinu	dutina	k1gFnSc4	dutina
lebeční	lebeční	k2eAgFnSc4d1	lebeční
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
drasticky	drasticky	k6eAd1	drasticky
zmenšil	zmenšit	k5eAaPmAgInS	zmenšit
na	na	k7c4	na
dnešní	dnešní	k2eAgFnSc4d1	dnešní
velikost	velikost	k1gFnSc4	velikost
–	–	k?	–
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejmenších	malý	k2eAgFnPc2d3	nejmenší
u	u	k7c2	u
vačnatců	vačnatec	k1gMnPc2	vačnatec
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
degeneraci	degenerace	k1gFnSc4	degenerace
vědci	vědec	k1gMnPc1	vědec
připisují	připisovat	k5eAaImIp3nP	připisovat
přechodu	přechod	k1gInSc3	přechod
na	na	k7c4	na
energeticky	energeticky	k6eAd1	energeticky
chudou	chudý	k2eAgFnSc4d1	chudá
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Dutina	dutina	k1gFnSc1	dutina
lebeční	lebeční	k2eAgNnSc1d1	lebeční
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
zhruba	zhruba	k6eAd1	zhruba
40	[number]	k4	40
%	%	kIx~	%
naplněna	naplnit	k5eAaPmNgFnS	naplnit
mozkomíšním	mozkomíšní	k2eAgInSc7d1	mozkomíšní
mokem	mok	k1gInSc7	mok
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
dvě	dva	k4xCgFnPc1	dva
mozkové	mozkový	k2eAgFnPc1d1	mozková
hemisféry	hemisféra	k1gFnPc1	hemisféra
vypadají	vypadat	k5eAaImIp3nP	vypadat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
dvě	dva	k4xCgFnPc1	dva
scvrklé	scvrklý	k2eAgFnPc1d1	scvrklá
půlky	půlka	k1gFnPc1	půlka
vlašského	vlašský	k2eAgInSc2d1	vlašský
ořechu	ořech	k1gInSc2	ořech
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
mozkového	mozkový	k2eAgInSc2d1	mozkový
kmenu	kmen	k1gInSc2	kmen
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nedotýkají	dotýkat	k5eNaImIp3nP	dotýkat
ani	ani	k8xC	ani
jedna	jeden	k4xCgFnSc1	jeden
druhé	druhý	k4xOgFnSc2	druhý
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
lebečních	lebeční	k2eAgFnPc2d1	lebeční
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jediné	jediný	k2eAgNnSc4d1	jediné
zvíře	zvíře	k1gNnSc4	zvíře
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
s	s	k7c7	s
takto	takto	k6eAd1	takto
zvláštně	zvláštně	k6eAd1	zvláštně
zmenšeným	zmenšený	k2eAgInSc7d1	zmenšený
mozkem	mozek	k1gInSc7	mozek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Koala	koala	k1gFnSc1	koala
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
tiché	tichý	k2eAgNnSc1d1	tiché
zvíře	zvíře	k1gNnSc1	zvíře
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
samci	samec	k1gMnPc1	samec
v	v	k7c6	v
období	období	k1gNnSc6	období
páření	páření	k1gNnSc2	páření
vydávají	vydávat	k5eAaImIp3nP	vydávat
velmi	velmi	k6eAd1	velmi
hlasité	hlasitý	k2eAgNnSc1d1	hlasité
volání	volání	k1gNnSc1	volání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
slyšet	slyšet	k5eAaImF	slyšet
až	až	k9	až
do	do	k7c2	do
kilometrové	kilometrový	k2eAgFnSc2d1	kilometrová
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Samicím	samice	k1gFnPc3	samice
toto	tento	k3xDgNnSc1	tento
volání	volání	k1gNnSc1	volání
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
odhadnout	odhadnout	k5eAaPmF	odhadnout
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
bude	být	k5eAaImBp3nS	být
samec	samec	k1gInSc1	samec
vhodný	vhodný	k2eAgInSc1d1	vhodný
k	k	k7c3	k
páření	páření	k1gNnSc3	páření
–	–	k?	–
obecně	obecně	k6eAd1	obecně
dávají	dávat	k5eAaImIp3nP	dávat
přednost	přednost	k1gFnSc4	přednost
větším	veliký	k2eAgInPc3d2	veliký
samcům	samec	k1gInPc3	samec
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
koala	koala	k1gFnSc1	koala
ocitne	ocitnout	k5eAaPmIp3nS	ocitnout
ve	v	k7c6	v
stresující	stresující	k2eAgFnSc6d1	stresující
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
vydávat	vydávat	k5eAaImF	vydávat
hlasitý	hlasitý	k2eAgInSc4d1	hlasitý
nářek	nářek	k1gInSc4	nářek
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
pláči	pláč	k1gInSc3	pláč
lidského	lidský	k2eAgNnSc2d1	lidské
dítěte	dítě	k1gNnSc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
jen	jen	k9	jen
málo	málo	k4c1	málo
spolehlivých	spolehlivý	k2eAgFnPc2d1	spolehlivá
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
života	život	k1gInSc2	život
koaly	koala	k1gFnSc2	koala
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
dožívají	dožívat	k5eAaImIp3nP	dožívat
až	až	k9	až
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byly	být	k5eAaImAgInP	být
popsány	popsat	k5eAaPmNgInP	popsat
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgInPc4	tři
poddruhy	poddruh	k1gInPc4	poddruh
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
projevem	projev	k1gInSc7	projev
morfologického	morfologický	k2eAgMnSc2d1	morfologický
klinu	klinout	k5eAaImIp1nS	klinout
–	–	k?	–
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
Bergmannovým	Bergmannův	k2eAgNnSc7d1	Bergmannovo
pravidlem	pravidlo	k1gNnSc7	pravidlo
jsou	být	k5eAaImIp3nP	být
jedinci	jedinec	k1gMnPc1	jedinec
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
chladnějším	chladný	k2eAgInSc6d2	chladnější
klimatu	klima	k1gNnSc2	klima
větší	veliký	k2eAgFnSc4d2	veliký
-	-	kIx~	-
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
tudíž	tudíž	k8xC	tudíž
obecně	obecně	k6eAd1	obecně
platné	platný	k2eAgNnSc1d1	platné
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rozlišování	rozlišování	k1gNnSc4	rozlišování
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
variet	varieta	k1gFnPc2	varieta
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
používá	používat	k5eAaImIp3nS	používat
označení	označení	k1gNnSc4	označení
typ	typ	k1gInSc1	typ
nebo	nebo	k8xC	nebo
ráz	ráz	k1gInSc1	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
koaly	koala	k1gFnPc4	koala
žijící	žijící	k2eAgFnPc4d1	žijící
na	na	k7c6	na
území	území	k1gNnSc6	území
státu	stát	k1gInSc2	stát
Victorie	Victorie	k1gFnSc2	Victorie
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
známé	známý	k2eAgInPc1d1	známý
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
P.	P.	kA	P.
cinereus	cinereus	k1gMnSc1	cinereus
victor	victor	k1gMnSc1	victor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
delší	dlouhý	k2eAgFnSc1d2	delší
<g/>
,	,	kIx,	,
hustší	hustý	k2eAgFnSc1d2	hustší
srst	srst	k1gFnSc1	srst
tmavší	tmavý	k2eAgFnSc2d2	tmavší
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
čokoládově	čokoládově	k6eAd1	čokoládově
hnědými	hnědý	k2eAgInPc7d1	hnědý
odstíny	odstín	k1gInPc7	odstín
na	na	k7c6	na
hřbetě	hřbet	k1gInSc6	hřbet
a	a	k8xC	a
končetinách	končetina	k1gFnPc6	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
zbarvením	zbarvení	k1gNnSc7	zbarvení
hřbetní	hřbetní	k2eAgFnSc2d1	hřbetní
a	a	k8xC	a
břišní	břišní	k2eAgFnSc2d1	břišní
krajiny	krajina	k1gFnSc2	krajina
je	být	k5eAaImIp3nS	být
výraznější	výrazný	k2eAgMnSc1d2	výraznější
–	–	k?	–
břicho	břicho	k1gNnSc1	břicho
je	být	k5eAaImIp3nS	být
nápadně	nápadně	k6eAd1	nápadně
světleji	světle	k6eAd2	světle
zbarvené	zbarvený	k2eAgFnPc1d1	zbarvená
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
v	v	k7c6	v
uších	ucho	k1gNnPc6	ucho
je	být	k5eAaImIp3nS	být
nadýchaná	nadýchaný	k2eAgFnSc1d1	nadýchaná
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnPc1d1	bílá
barvy	barva	k1gFnPc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Jedinci	jedinec	k1gMnPc1	jedinec
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
státu	stát	k1gInSc2	stát
Queensland	Queenslanda	k1gFnPc2	Queenslanda
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
klasifikovaní	klasifikovaný	k2eAgMnPc1d1	klasifikovaný
jako	jako	k9	jako
P.	P.	kA	P.
cinereus	cinereus	k1gMnSc1	cinereus
adustus	adustus	k1gMnSc1	adustus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
v	v	k7c6	v
sub-tropickém	subropický	k2eAgNnSc6d1	sub-tropický
a	a	k8xC	a
tropickém	tropický	k2eAgNnSc6d1	tropické
pásmu	pásmo	k1gNnSc6	pásmo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
řidší	řídký	k2eAgFnSc7d2	řidší
a	a	k8xC	a
kratší	krátký	k2eAgFnSc7d2	kratší
srstí	srst	k1gFnSc7	srst
špinavě	špinavě	k6eAd1	špinavě
šedé	šedý	k2eAgFnSc2d1	šedá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
malou	malý	k2eAgFnSc7d1	malá
tělesnou	tělesný	k2eAgFnSc7d1	tělesná
hmotností	hmotnost	k1gFnSc7	hmotnost
–	–	k?	–
průměrně	průměrně	k6eAd1	průměrně
6,5	[number]	k4	6,5
kg	kg	kA	kg
u	u	k7c2	u
samců	samec	k1gInPc2	samec
a	a	k8xC	a
lehce	lehko	k6eAd1	lehko
nad	nad	k7c7	nad
5	[number]	k4	5
kg	kg	kA	kg
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
jedinců	jedinec	k1gMnPc2	jedinec
žijících	žijící	k2eAgMnPc2d1	žijící
ve	v	k7c6	v
Victorii	Victorie	k1gFnSc6	Victorie
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
P.	P.	kA	P.
cinereus	cinereus	k1gMnSc1	cinereus
victor	victor	k1gMnSc1	victor
)	)	kIx)	)
a	a	k8xC	a
Novém	nový	k2eAgInSc6d1	nový
Jižním	jižní	k2eAgInSc6d1	jižní
Walesu	Wales	k1gInSc6	Wales
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
P.	P.	kA	P.
cinereus	cinereus	k1gMnSc1	cinereus
cinereus	cinereus	k1gMnSc1	cinereus
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
12	[number]	k4	12
kg	kg	kA	kg
u	u	k7c2	u
samců	samec	k1gInPc2	samec
a	a	k8xC	a
8,5	[number]	k4	8,5
kg	kg	kA	kg
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
poddruh	poddruh	k1gInSc4	poddruh
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
ráz	ráz	k1gInSc4	ráz
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
zlatý	zlatý	k1gInSc1	zlatý
koala	koala	k1gFnSc1	koala
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
který	který	k3yRgInSc4	který
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
jemný	jemný	k2eAgInSc1d1	jemný
zlatavý	zlatavý	k2eAgInSc1d1	zlatavý
nádech	nádech	k1gInSc1	nádech
srsti	srst	k1gFnSc2	srst
způsobený	způsobený	k2eAgInSc4d1	způsobený
absencí	absence	k1gFnSc7	absence
pigmentu	pigment	k1gInSc2	pigment
melaninu	melanin	k1gInSc2	melanin
–	–	k?	–
ta	ten	k3xDgFnSc1	ten
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
ostatních	ostatní	k2eAgMnPc2d1	ostatní
savců	savec	k1gMnPc2	savec
albinismus	albinismus	k1gInSc4	albinismus
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
rázy	ráz	k1gInPc7	ráz
je	být	k5eAaImIp3nS	být
plynulý	plynulý	k2eAgInSc1d1	plynulý
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
značné	značný	k2eAgInPc1d1	značný
rozdíly	rozdíl	k1gInPc1	rozdíl
např.	např.	kA	např.
ve	v	k7c6	v
zbarvení	zbarvení	k1gNnSc6	zbarvení
i	i	k8xC	i
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgMnPc7d1	jednotlivý
jedinci	jedinec	k1gMnPc7	jedinec
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jedné	jeden	k4xCgFnSc2	jeden
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Ojediněle	ojediněle	k6eAd1	ojediněle
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
jedinci	jedinec	k1gMnPc1	jedinec
s	s	k7c7	s
bílou	bílý	k2eAgFnSc7d1	bílá
srstí	srst	k1gFnSc7	srst
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
přítomností	přítomnost	k1gFnSc7	přítomnost
recesivního	recesivní	k2eAgInSc2d1	recesivní
genu	gen	k1gInSc2	gen
<g/>
.	.	kIx.	.
</s>
<s>
Koaly	koala	k1gFnPc4	koala
jsou	být	k5eAaImIp3nP	být
přímí	přímý	k2eAgMnPc1d1	přímý
příbuzní	příbuzný	k1gMnPc1	příbuzný
s	s	k7c7	s
prvními	první	k4xOgInPc7	první
vačnatci	vačnatec	k1gMnPc7	vačnatec
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
dospělosti	dospělost	k1gFnPc4	dospělost
mezi	mezi	k7c7	mezi
druhým	druhý	k4xOgInSc7	druhý
a	a	k8xC	a
třetím	třetí	k4xOgInSc7	třetí
rokem	rok	k1gInSc7	rok
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
samci	samec	k1gInSc6	samec
mezi	mezi	k7c7	mezi
třetím	třetí	k4xOgInSc7	třetí
a	a	k8xC	a
čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
<g/>
.	.	kIx.	.
</s>
<s>
Páření	páření	k1gNnSc1	páření
obvykle	obvykle	k6eAd1	obvykle
probíhá	probíhat	k5eAaImIp3nS	probíhat
od	od	k7c2	od
prosince	prosinec	k1gInSc2	prosinec
do	do	k7c2	do
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
léto	léto	k1gNnSc1	léto
<g/>
.	.	kIx.	.
</s>
<s>
Zdravá	zdravý	k2eAgFnSc1d1	zdravá
samice	samice	k1gFnSc1	samice
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
jedno	jeden	k4xCgNnSc4	jeden
mládě	mládě	k1gNnSc4	mládě
ročně	ročně	k6eAd1	ročně
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přibližně	přibližně	k6eAd1	přibližně
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
dvanácti	dvanáct	k4xCc2	dvanáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Březost	březost	k1gFnSc1	březost
trvá	trvat	k5eAaImIp3nS	trvat
35	[number]	k4	35
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Dvojčata	dvojče	k1gNnPc1	dvojče
jsou	být	k5eAaImIp3nP	být
velice	velice	k6eAd1	velice
vzácná	vzácný	k2eAgFnSc1d1	vzácná
–	–	k?	–
první	první	k4xOgMnPc1	první
koalí	koalý	k1gMnPc1	koalý
dvojčata	dvojče	k1gNnPc1	dvojče
na	na	k7c6	na
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1999	[number]	k4	1999
na	na	k7c6	na
Queenslandské	Queenslandský	k2eAgFnSc6d1	Queenslandská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Mládě	mládě	k1gNnSc1	mládě
koaly	koala	k1gFnSc2	koala
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
joey	joey	k1gInPc4	joey
<g/>
"	"	kIx"	"
[	[	kIx(	[
<g/>
džouí	džouí	k6eAd1	džouí
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
holé	holý	k2eAgNnSc1d1	holé
<g/>
,	,	kIx,	,
slepé	slepý	k2eAgNnSc1d1	slepé
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
uší	ucho	k1gNnPc2	ucho
a	a	k8xC	a
v	v	k7c6	v
době	doba	k1gFnSc6	doba
porodu	porod	k1gInSc2	porod
měří	měřit	k5eAaImIp3nS	měřit
pouze	pouze	k6eAd1	pouze
2	[number]	k4	2
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porodu	porod	k1gInSc6	porod
vyšplhá	vyšplhat	k5eAaPmIp3nS	vyšplhat
do	do	k7c2	do
vaku	vak	k1gInSc2	vak
na	na	k7c6	na
matčině	matčin	k2eAgNnSc6d1	matčino
břiše	břicho	k1gNnSc6	břicho
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
přisaje	přisát	k5eAaPmIp3nS	přisát
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
bradavek	bradavka	k1gFnPc2	bradavka
<g/>
.	.	kIx.	.
</s>
<s>
Vak	vak	k1gInSc1	vak
samic	samice	k1gFnPc2	samice
se	se	k3xPyFc4	se
otevírá	otevírat	k5eAaImIp3nS	otevírat
směrem	směr	k1gInSc7	směr
dozadu	dozadu	k6eAd1	dozadu
(	(	kIx(	(
<g/>
k	k	k7c3	k
řitnímu	řitní	k2eAgInSc3d1	řitní
otvoru	otvor	k1gInSc3	otvor
<g/>
)	)	kIx)	)
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
matkou	matka	k1gFnSc7	matka
libovolně	libovolně	k6eAd1	libovolně
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
pomocí	pomocí	k7c2	pomocí
svalu	sval	k1gInSc2	sval
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
stahovací	stahovací	k2eAgFnSc3d1	stahovací
šňůrce	šňůrka	k1gFnSc3	šňůrka
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
šest	šest	k4xCc1	šest
měsíců	měsíc	k1gInPc2	měsíc
setrvává	setrvávat	k5eAaImIp3nS	setrvávat
mládě	mládě	k1gNnSc1	mládě
ve	v	k7c6	v
vaku	vak	k1gInSc6	vak
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
pouze	pouze	k6eAd1	pouze
mateřským	mateřský	k2eAgNnSc7d1	mateřské
mlékem	mléko	k1gNnSc7	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vyvinou	vyvinout	k5eAaPmIp3nP	vyvinout
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
uši	ucho	k1gNnPc4	ucho
<g/>
,	,	kIx,	,
a	a	k8xC	a
naroste	narůst	k5eAaPmIp3nS	narůst
srst	srst	k1gFnSc4	srst
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
začínává	začínávat	k5eAaImIp3nS	začínávat
prozkoumávat	prozkoumávat	k5eAaImF	prozkoumávat
svět	svět	k1gInSc4	svět
mimo	mimo	k7c4	mimo
vak	vak	k1gInSc4	vak
a	a	k8xC	a
konzumovat	konzumovat	k5eAaBmF	konzumovat
malá	malý	k2eAgNnPc4d1	malé
množství	množství	k1gNnPc4	množství
matčiny	matčin	k2eAgFnSc2d1	matčina
"	"	kIx"	"
<g/>
kašičky	kašička	k1gFnSc2	kašička
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
exkrement	exkrement	k1gInSc4	exkrement
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pocházející	pocházející	k2eAgFnSc4d1	pocházející
ze	z	k7c2	z
slepého	slepý	k2eAgNnSc2d1	slepé
střeva	střevo	k1gNnSc2	střevo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
které	který	k3yQgFnSc3	který
se	se	k3xPyFc4	se
do	do	k7c2	do
střev	střevo	k1gNnPc2	střevo
mláděte	mládě	k1gNnSc2	mládě
dostanou	dostat	k5eAaPmIp3nP	dostat
mikroby	mikrob	k1gInPc1	mikrob
nezbytné	nezbytný	k2eAgInPc1d1	nezbytný
k	k	k7c3	k
trávení	trávení	k1gNnSc3	trávení
listů	list	k1gInPc2	list
blahovičníku	blahovičník	k1gInSc2	blahovičník
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgNnPc2d1	další
přibližně	přibližně	k6eAd1	přibližně
šest	šest	k4xCc4	šest
měsíců	měsíc	k1gInPc2	měsíc
mládě	mládě	k1gNnSc1	mládě
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
vak	vak	k1gInSc4	vak
příliš	příliš	k6eAd1	příliš
veliké	veliký	k2eAgFnPc1d1	veliká
<g/>
,	,	kIx,	,
vozí	vozit	k5eAaImIp3nP	vozit
se	se	k3xPyFc4	se
na	na	k7c6	na
matčiných	matčin	k2eAgNnPc6d1	matčino
zádech	záda	k1gNnPc6	záda
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
mlékem	mléko	k1gNnSc7	mléko
i	i	k8xC	i
listy	list	k1gInPc7	list
blahovičníku	blahovičník	k1gInSc2	blahovičník
až	až	k8xS	až
do	do	k7c2	do
úplného	úplný	k2eAgNnSc2d1	úplné
odstavení	odstavení	k1gNnSc2	odstavení
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
dochází	docházet	k5eAaImIp3nS	docházet
kolem	kolem	k7c2	kolem
12	[number]	k4	12
<g/>
.	.	kIx.	.
<g/>
měsíce	měsíc	k1gInSc2	měsíc
života	život	k1gInSc2	život
mláděte	mládě	k1gNnSc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgFnSc2d1	mladá
samičky	samička	k1gFnSc2	samička
se	se	k3xPyFc4	se
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
vydají	vydat	k5eAaPmIp3nP	vydat
pryč	pryč	k6eAd1	pryč
a	a	k8xC	a
usídlí	usídlit	k5eAaPmIp3nS	usídlit
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
samečci	sameček	k1gMnPc1	sameček
často	často	k6eAd1	často
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
v	v	k7c6	v
teritoriu	teritorium	k1gNnSc6	teritorium
matky	matka	k1gFnSc2	matka
až	až	k9	až
do	do	k7c2	do
věku	věk	k1gInSc2	věk
tří	tři	k4xCgNnPc2	tři
či	či	k8xC	či
čtyř	čtyři	k4xCgNnPc2	čtyři
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Koala	koala	k1gFnSc1	koala
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
listy	list	k1gInPc1	list
blahovičníků	blahovičník	k1gInPc2	blahovičník
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
důsledek	důsledek	k1gInSc4	důsledek
adaptace	adaptace	k1gFnSc2	adaptace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
volnou	volný	k2eAgFnSc4d1	volná
ekologickou	ekologický	k2eAgFnSc4d1	ekologická
niku	nika	k1gFnSc4	nika
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
eukalyptových	eukalyptový	k2eAgInPc2d1	eukalyptový
listů	list	k1gInPc2	list
bez	bez	k7c2	bez
konzumentů	konzument	k1gMnPc2	konzument
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
blahovičníků	blahovičník	k1gInPc2	blahovičník
mají	mít	k5eAaImIp3nP	mít
nízký	nízký	k2eAgInSc4d1	nízký
obsah	obsah	k1gInSc4	obsah
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
mají	mít	k5eAaImIp3nP	mít
vysoký	vysoký	k2eAgInSc4d1	vysoký
podíl	podíl	k1gInSc4	podíl
nestravitelných	stravitelný	k2eNgFnPc2d1	nestravitelná
látek	látka	k1gFnPc2	látka
a	a	k8xC	a
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
fenolické	fenolický	k2eAgFnPc1d1	fenolická
a	a	k8xC	a
terpenové	terpenový	k2eAgFnPc1d1	terpenový
složky	složka	k1gFnPc1	složka
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kterým	který	k3yRgMnPc3	který
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
jedovaté	jedovatý	k2eAgNnSc1d1	jedovaté
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
vombati	vombat	k1gMnPc1	vombat
a	a	k8xC	a
lenochodi	lenochod	k1gMnPc1	lenochod
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
koala	koala	k1gFnSc1	koala
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
savci	savec	k1gMnPc7	savec
velmi	velmi	k6eAd1	velmi
pomalý	pomalý	k2eAgInSc1d1	pomalý
metabolismus	metabolismus	k1gInSc1	metabolismus
a	a	k8xC	a
tráví	trávit	k5eAaImIp3nP	trávit
16	[number]	k4	16
až	až	k9	až
18	[number]	k4	18
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
bez	bez	k7c2	bez
pohnutí	pohnutí	k1gNnSc2	pohnutí
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
většinu	většina	k1gFnSc4	většina
tohoto	tento	k3xDgInSc2	tento
času	čas	k1gInSc2	čas
prospí	prospat	k5eAaPmIp3nS	prospat
<g/>
.	.	kIx.	.
</s>
<s>
Koalové	Koal	k1gMnPc1	Koal
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
vůči	vůči	k7c3	vůči
příslušníkům	příslušník	k1gMnPc3	příslušník
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
agresivní	agresivní	k2eAgNnSc1d1	agresivní
(	(	kIx(	(
<g/>
přední	přední	k2eAgFnSc7d1	přední
tlapou	tlapa	k1gFnSc7	tlapa
si	se	k3xPyFc3	se
protivníka	protivník	k1gMnSc4	protivník
přidrží	přidržet	k5eAaPmIp3nP	přidržet
a	a	k8xC	a
koušou	kousat	k5eAaImIp3nP	kousat
jej	on	k3xPp3gMnSc4	on
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
většina	většina	k1gFnSc1	většina
agresivního	agresivní	k2eAgNnSc2d1	agresivní
chování	chování	k1gNnSc2	chování
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
krátké	krátký	k2eAgFnPc1d1	krátká
roztržky	roztržka	k1gFnPc1	roztržka
<g/>
.	.	kIx.	.
</s>
<s>
Manipulace	manipulace	k1gFnSc1	manipulace
s	s	k7c7	s
koaly	koala	k1gFnPc4	koala
jim	on	k3xPp3gMnPc3	on
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
stres	stres	k1gInSc1	stres
–	–	k?	–
otázka	otázka	k1gFnSc1	otázka
agresivity	agresivita	k1gFnSc2	agresivita
a	a	k8xC	a
stresu	stres	k1gInSc2	stres
navozeného	navozený	k2eAgInSc2d1	navozený
manipulací	manipulace	k1gFnSc7	manipulace
s	s	k7c7	s
těmito	tento	k3xDgNnPc7	tento
zvířaty	zvíře	k1gNnPc7	zvíře
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
politickým	politický	k2eAgInSc7d1	politický
problémem	problém	k1gInSc7	problém
<g/>
.	.	kIx.	.
</s>
<s>
Koala	koala	k1gFnSc1	koala
tráví	trávit	k5eAaImIp3nS	trávit
kolem	kolem	k7c2	kolem
tří	tři	k4xCgInPc2	tři
z	z	k7c2	z
pěti	pět	k4xCc2	pět
hodin	hodina	k1gFnPc2	hodina
své	svůj	k3xOyFgFnSc2	svůj
aktivity	aktivita	k1gFnSc2	aktivita
krmením	krmení	k1gNnSc7	krmení
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
v	v	k7c4	v
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
denní	denní	k2eAgFnSc4d1	denní
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
obvykle	obvykle	k6eAd1	obvykle
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Koala	koala	k1gFnSc1	koala
sní	snít	k5eAaImIp3nS	snít
průměrně	průměrně	k6eAd1	průměrně
500	[number]	k4	500
g	g	kA	g
eukalyptových	eukalyptový	k2eAgInPc2d1	eukalyptový
listů	list	k1gInPc2	list
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
spolknutím	spolknutí	k1gNnSc7	spolknutí
je	být	k5eAaImIp3nS	být
důkladně	důkladně	k6eAd1	důkladně
rozmělní	rozmělnit	k5eAaPmIp3nP	rozmělnit
silnými	silný	k2eAgFnPc7d1	silná
čelistmi	čelist	k1gFnPc7	čelist
na	na	k7c6	na
velmi	velmi	k6eAd1	velmi
jemnou	jemný	k2eAgFnSc4d1	jemná
kaši	kaše	k1gFnSc4	kaše
<g/>
.	.	kIx.	.
</s>
<s>
Toxické	toxický	k2eAgFnPc1d1	toxická
složky	složka	k1gFnPc1	složka
jsou	být	k5eAaImIp3nP	být
játry	játra	k1gNnPc7	játra
zneškodněny	zneškodněn	k2eAgFnPc1d1	zneškodněna
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
vyloučeny	vyloučen	k2eAgInPc1d1	vyloučen
<g/>
.	.	kIx.	.
</s>
<s>
Koncové	koncový	k2eAgNnSc1d1	koncové
střevo	střevo	k1gNnSc1	střevo
(	(	kIx(	(
<g/>
obzvláště	obzvláště	k6eAd1	obzvláště
slepé	slepý	k2eAgNnSc1d1	slepé
střevo	střevo	k1gNnSc1	střevo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
zvětšené	zvětšený	k2eAgNnSc1d1	zvětšené
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
schopno	schopen	k2eAgNnSc1d1	schopno
využít	využít	k5eAaPmF	využít
co	co	k9	co
největší	veliký	k2eAgNnSc4d3	veliký
množství	množství	k1gNnSc4	množství
živin	živina	k1gFnPc2	živina
v	v	k7c6	v
chudé	chudý	k2eAgFnSc6d1	chudá
potravě	potrava	k1gFnSc6	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
části	část	k1gFnPc1	část
tohoto	tento	k3xDgInSc2	tento
procesu	proces	k1gInSc2	proces
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
bakteriální	bakteriální	k2eAgFnSc1d1	bakteriální
fermentace	fermentace	k1gFnSc1	fermentace
<g/>
.	.	kIx.	.
</s>
<s>
Koalům	Koal	k1gMnPc3	Koal
slouží	sloužit	k5eAaImIp3nP	sloužit
za	za	k7c4	za
potravu	potrava	k1gFnSc4	potrava
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
druhů	druh	k1gInPc2	druh
blahovičníků	blahovičník	k1gInPc2	blahovičník
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
některé	některý	k3yIgInPc1	některý
jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
stromů	strom	k1gInPc2	strom
(	(	kIx(	(
<g/>
rody	rod	k1gInPc1	rod
Acacia	Acacium	k1gNnSc2	Acacium
<g/>
,	,	kIx,	,
Leptospermum	Leptospermum	k1gNnSc1	Leptospermum
a	a	k8xC	a
Melaleuca	Melaleuca	k1gFnSc1	Melaleuca
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koalové	Koal	k1gMnPc1	Koal
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
silné	silný	k2eAgFnPc4d1	silná
preference	preference	k1gFnPc4	preference
pro	pro	k7c4	pro
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
druh	druh	k1gInSc4	druh
eukalyptů	eukalypt	k1gInPc2	eukalypt
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
preference	preference	k1gFnPc1	preference
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
oblast	oblast	k1gFnSc1	oblast
od	od	k7c2	od
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
množství	množství	k1gNnSc1	množství
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
určují	určovat	k5eAaImIp3nP	určovat
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
z	z	k7c2	z
680	[number]	k4	680
druhů	druh	k1gInPc2	druh
blahovičníků	blahovičník	k1gInPc2	blahovičník
bude	být	k5eAaImBp3nS	být
koala	koala	k1gFnSc1	koala
jíst	jíst	k5eAaImF	jíst
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
hlavním	hlavní	k2eAgInSc7d1	hlavní
faktorem	faktor	k1gInSc7	faktor
v	v	k7c6	v
rozhodování	rozhodování	k1gNnSc6	rozhodování
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
stromy	strom	k1gInPc7	strom
oblíbených	oblíbený	k2eAgInPc2d1	oblíbený
druhů	druh	k1gInPc2	druh
blahovičníku	blahovičník	k1gInSc2	blahovičník
je	být	k5eAaImIp3nS	být
koncentrace	koncentrace	k1gFnSc1	koncentrace
fenolických	fenolický	k2eAgInPc2d1	fenolický
toxinů	toxin	k1gInPc2	toxin
nazývaných	nazývaný	k2eAgFnPc2d1	nazývaná
floroglucinoly	floroglucinola	k1gFnSc2	floroglucinola
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
prováděný	prováděný	k2eAgInSc1d1	prováděný
ošetřovateli	ošetřovatel	k1gMnPc7	ošetřovatel
koalů	koal	k1gInPc2	koal
v	v	k7c4	v
13	[number]	k4	13
záchranných	záchranný	k2eAgFnPc6d1	záchranná
stanicích	stanice	k1gFnPc6	stanice
a	a	k8xC	a
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jižním	jižní	k2eAgInSc6d1	jižní
Walesu	Wales	k1gInSc6	Wales
také	také	k9	také
ukázal	ukázat	k5eAaPmAgMnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejoblíbenější	oblíbený	k2eAgInSc1d3	nejoblíbenější
druh	druh	k1gInSc1	druh
eukalyptového	eukalyptový	k2eAgNnSc2d1	eukalyptové
listí	listí	k1gNnSc2	listí
měl	mít	k5eAaImAgInS	mít
nejmenší	malý	k2eAgInSc1d3	nejmenší
obsah	obsah	k1gInSc1	obsah
taninů	tanin	k1gInPc2	tanin
<g/>
.	.	kIx.	.
</s>
<s>
Koala	koala	k1gFnSc1	koala
nemusí	muset	k5eNaImIp3nS	muset
pít	pít	k5eAaImF	pít
<g/>
,	,	kIx,	,
vystačí	vystačit	k5eAaBmIp3nS	vystačit
si	se	k3xPyFc3	se
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
obsaženou	obsažený	k2eAgFnSc7d1	obsažená
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Koalové	Koal	k1gMnPc1	Koal
byli	být	k5eAaImAgMnP	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
téměř	téměř	k6eAd1	téměř
vyhubeni	vyhubit	k5eAaPmNgMnP	vyhubit
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
lovil	lovit	k5eAaImAgMnS	lovit
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
kožešinám	kožešina	k1gFnPc3	kožešina
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
milióny	milión	k4xCgInPc1	milión
byly	být	k5eAaImAgFnP	být
prodány	prodat	k5eAaPmNgFnP	prodat
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
populace	populace	k1gFnSc1	populace
koalů	koal	k1gMnPc2	koal
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
nezotavila	zotavit	k5eNaPmAgFnS	zotavit
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
drastického	drastický	k2eAgNnSc2d1	drastické
snížení	snížení	k1gNnSc2	snížení
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
milion	milion	k4xCgInSc4	milion
koalů	koal	k1gMnPc2	koal
bylo	být	k5eAaImAgNnS	být
zastřeleno	zastřelit	k5eAaPmNgNnS	zastřelit
<g/>
,	,	kIx,	,
zabito	zabít	k5eAaPmNgNnS	zabít
či	či	k8xC	či
chyceno	chycen	k2eAgNnSc1d1	chyceno
do	do	k7c2	do
ok	oka	k1gFnPc2	oka
v	v	k7c6	v
Queenslandu	Queensland	k1gInSc6	Queensland
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
a	a	k8xC	a
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
<s>
Vlna	vlna	k1gFnSc1	vlna
bouřlivého	bouřlivý	k2eAgInSc2d1	bouřlivý
nesouhlasu	nesouhlas	k1gInSc2	nesouhlas
s	s	k7c7	s
vybíjením	vybíjení	k1gNnSc7	vybíjení
koalů	koal	k1gInPc2	koal
byla	být	k5eAaImAgFnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
prvním	první	k4xOgMnSc7	první
environmentálním	environmentální	k2eAgMnSc7d1	environmentální
tématem	téma	k1gNnSc7	téma
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
sjednotilo	sjednotit	k5eAaPmAgNnS	sjednotit
Australany	Australan	k1gMnPc4	Australan
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
sílící	sílící	k2eAgFnPc4d1	sílící
tendence	tendence	k1gFnPc4	tendence
chránit	chránit	k5eAaImF	chránit
původní	původní	k2eAgMnPc4d1	původní
druhy	druh	k1gMnPc4	druh
<g/>
,	,	kIx,	,
chudoba	chudoba	k1gFnSc1	chudoba
způsobená	způsobený	k2eAgFnSc1d1	způsobená
suchem	sucho	k1gNnSc7	sucho
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1926-28	[number]	k4	1926-28
zapříčinila	zapříčinit	k5eAaPmAgFnS	zapříčinit
smrt	smrt	k1gFnSc1	smrt
dalších	další	k2eAgInPc2d1	další
600	[number]	k4	600
000	[number]	k4	000
koalů	koal	k1gMnPc2	koal
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
zabiti	zabít	k5eAaPmNgMnP	zabít
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
jediné	jediný	k2eAgFnSc2d1	jediná
měsíční	měsíční	k2eAgFnSc2d1	měsíční
doby	doba	k1gFnSc2	doba
lovu	lov	k1gInSc2	lov
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
vedou	vést	k5eAaImIp3nP	vést
žebříček	žebříček	k1gInSc4	žebříček
ohrožujících	ohrožující	k2eAgInPc2d1	ohrožující
faktorů	faktor	k1gInPc2	faktor
ztráta	ztráta	k1gFnSc1	ztráta
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
dopady	dopad	k1gInPc4	dopad
urbanizace	urbanizace	k1gFnSc2	urbanizace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
střety	střet	k1gInPc4	střet
vozidel	vozidlo	k1gNnPc2	vozidlo
s	s	k7c7	s
koaly	koala	k1gFnPc1	koala
a	a	k8xC	a
útoky	útok	k1gInPc1	útok
psů	pes	k1gMnPc2	pes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
jsou	být	k5eAaImIp3nP	být
koalové	koal	k1gMnPc1	koal
velmi	velmi	k6eAd1	velmi
vnímaví	vnímavý	k2eAgMnPc1d1	vnímavý
k	k	k7c3	k
infekcím	infekce	k1gFnPc3	infekce
<g/>
,	,	kIx,	,
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgFnP	být
některé	některý	k3yIgFnPc1	některý
kolonie	kolonie	k1gFnPc1	kolonie
koalů	koal	k1gMnPc2	koal
značně	značně	k6eAd1	značně
postiženy	postihnout	k5eAaPmNgFnP	postihnout
nemocemi	nemoc	k1gFnPc7	nemoc
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
bakteriemi	bakterie	k1gFnPc7	bakterie
rodu	rod	k1gInSc2	rod
chlamydiae	chlamydiae	k1gFnPc2	chlamydiae
<g/>
.	.	kIx.	.
</s>
<s>
Koala	koala	k1gFnSc1	koala
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
rozlehlé	rozlehlý	k2eAgFnSc2d1	rozlehlá
plochy	plocha	k1gFnSc2	plocha
zdravého	zdravý	k2eAgInSc2d1	zdravý
<g/>
,	,	kIx,	,
nepřerušeného	přerušený	k2eNgInSc2d1	nepřerušený
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
nového	nový	k2eAgNnSc2d1	nové
teritoria	teritorium	k1gNnSc2	teritorium
či	či	k8xC	či
partnera	partner	k1gMnSc4	partner
často	často	k6eAd1	často
urazí	urazit	k5eAaPmIp3nP	urazit
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přemisťování	přemisťování	k1gNnSc3	přemisťování
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
biokoridory	biokoridor	k1gInPc4	biokoridor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
ničeny	ničen	k2eAgInPc4d1	ničen
rozpínající	rozpínající	k2eAgInPc4d1	rozpínající
se	se	k3xPyFc4	se
lidskou	lidský	k2eAgFnSc7d1	lidská
činností	činnost	k1gFnSc7	činnost
(	(	kIx(	(
<g/>
zemědělstvím	zemědělství	k1gNnSc7	zemědělství
<g/>
,	,	kIx,	,
lesnictvím	lesnictví	k1gNnSc7	lesnictví
<g/>
,	,	kIx,	,
stavbou	stavba	k1gFnSc7	stavba
silnic	silnice	k1gFnPc2	silnice
a	a	k8xC	a
obytných	obytný	k2eAgFnPc2d1	obytná
zón	zóna	k1gFnPc2	zóna
<g/>
)	)	kIx)	)
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
jsou	být	k5eAaImIp3nP	být
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
populace	populace	k1gFnPc1	populace
koalů	koal	k1gInPc2	koal
izolovány	izolován	k2eAgFnPc1d1	izolována
jedna	jeden	k4xCgFnSc1	jeden
od	od	k7c2	od
druhé	druhý	k4xOgFnSc6	druhý
ve	v	k7c6	v
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
zmenšujících	zmenšující	k2eAgFnPc2d1	zmenšující
zbytcích	zbytek	k1gInPc6	zbytek
zalesněných	zalesněný	k2eAgFnPc2d1	zalesněná
ploch	plocha	k1gFnPc2	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
proto	proto	k8xC	proto
divu	div	k1gInSc2	div
<g/>
,	,	kIx,	,
že	že	k8xS	že
dlouhodobé	dlouhodobý	k2eAgNnSc1d1	dlouhodobé
přežití	přežití	k1gNnSc1	přežití
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
je	být	k5eAaImIp3nS	být
ohroženo	ohrozit	k5eAaPmNgNnS	ohrozit
omezenou	omezený	k2eAgFnSc7d1	omezená
rozmanitostí	rozmanitost	k1gFnSc7	rozmanitost
genofondu	genofond	k1gInSc2	genofond
<g/>
.	.	kIx.	.
</s>
<s>
Stěžejní	stěžejní	k2eAgFnSc7d1	stěžejní
organizací	organizace	k1gFnSc7	organizace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
ochraně	ochrana	k1gFnSc3	ochrana
koalů	koal	k1gMnPc2	koal
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
prostředí	prostředí	k1gNnSc2	prostředí
je	být	k5eAaImIp3nS	být
Australská	australský	k2eAgFnSc1d1	australská
Nadace	nadace	k1gFnSc1	nadace
pro	pro	k7c4	pro
koaly	koala	k1gFnPc4	koala
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
plánuje	plánovat	k5eAaImIp3nS	plánovat
vyhlásit	vyhlásit	k5eAaPmF	vyhlásit
40	[number]	k4	40
000	[number]	k4	000
km	km	kA	km
<g/>
2	[number]	k4	2
přirozeným	přirozený	k2eAgNnSc7d1	přirozené
prostředím	prostředí	k1gNnSc7	prostředí
pro	pro	k7c4	pro
koaly	koala	k1gFnPc4	koala
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
na	na	k7c6	na
základě	základ	k1gInSc6	základ
přesvědčivých	přesvědčivý	k2eAgInPc2d1	přesvědčivý
důkazů	důkaz	k1gInPc2	důkaz
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
stavy	stav	k1gInPc4	stav
populací	populace	k1gFnPc2	populace
divokých	divoký	k2eAgInPc2d1	divoký
koalů	koal	k1gInPc2	koal
vážně	vážně	k6eAd1	vážně
klesají	klesat	k5eAaImIp3nP	klesat
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
oblasti	oblast	k1gFnSc6	oblast
výskytu	výskyt	k1gInSc2	výskyt
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
koala	koala	k1gFnSc1	koala
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
plošně	plošně	k6eAd1	plošně
rozsáhlém	rozsáhlý	k2eAgNnSc6d1	rozsáhlé
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
z	z	k7c2	z
jejich	jejich	k3xOp3gNnSc2	jejich
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
prostředí	prostředí	k1gNnSc2	prostředí
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
pouze	pouze	k6eAd1	pouze
"	"	kIx"	"
<g/>
zbytky	zbytek	k1gInPc7	zbytek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vhodné	vhodný	k2eAgNnSc1d1	vhodné
prostředí	prostředí	k1gNnSc1	prostředí
je	být	k5eAaImIp3nS	být
ničeno	ničit	k5eAaImNgNnS	ničit
invazemi	invaze	k1gFnPc7	invaze
plevelů	plevel	k1gInPc2	plevel
<g/>
,	,	kIx,	,
odlesňováním	odlesňování	k1gNnSc7	odlesňování
ze	z	k7c2	z
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
důvodů	důvod	k1gInPc2	důvod
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
zabíráno	zabírán	k2eAgNnSc1d1	zabíráno
developery	developer	k1gMnPc7	developer
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc4d1	další
hrozby	hrozba	k1gFnPc4	hrozba
představují	představovat	k5eAaImIp3nP	představovat
těžba	těžba	k1gFnSc1	těžba
dřeva	dřevo	k1gNnPc1	dřevo
<g/>
,	,	kIx,	,
chybný	chybný	k2eAgInSc1d1	chybný
management	management	k1gInSc1	management
<g/>
,	,	kIx,	,
útoky	útok	k1gInPc1	útok
zdivočelých	zdivočelý	k2eAgInPc2d1	zdivočelý
a	a	k8xC	a
domácích	domácí	k2eAgNnPc2d1	domácí
zvířat	zvíře	k1gNnPc2	zvíře
a	a	k8xC	a
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Australská	australský	k2eAgFnSc1d1	australská
vláda	vláda	k1gFnSc1	vláda
uvádí	uvádět	k5eAaImIp3nS	uvádět
koalu	koala	k1gFnSc4	koala
na	na	k7c6	na
čelním	čelní	k2eAgNnSc6d1	čelní
místě	místo	k1gNnSc6	místo
seznamu	seznam	k1gInSc2	seznam
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
ochranářský	ochranářský	k2eAgInSc4d1	ochranářský
status	status	k1gInSc4	status
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
posoudit	posoudit	k5eAaPmF	posoudit
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
celkové	celkový	k2eAgNnSc4d1	celkové
množství	množství	k1gNnSc4	množství
jedinců	jedinec	k1gMnPc2	jedinec
v	v	k7c6	v
stovkách	stovka	k1gFnPc6	stovka
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
jiné	jiný	k2eAgFnPc1d1	jiná
studie	studie	k1gFnPc1	studie
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
zbylo	zbýt	k5eAaPmAgNnS	zbýt
již	již	k6eAd1	již
jen	jen	k9	jen
80	[number]	k4	80
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Australská	australský	k2eAgFnSc1d1	australská
Nadace	nadace	k1gFnSc1	nadace
pro	pro	k7c4	pro
koaly	koala	k1gFnPc4	koala
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
000	[number]	k4	000
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Koalové	Koal	k1gMnPc1	Koal
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
většina	většina	k1gFnSc1	většina
původních	původní	k2eAgNnPc2d1	původní
australských	australský	k2eAgNnPc2d1	Australské
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
dle	dle	k7c2	dle
zákona	zákon	k1gInSc2	zákon
chováni	chovat	k5eAaImNgMnP	chovat
jako	jako	k8xS	jako
domácí	domácí	k2eAgMnPc1d1	domácí
mazlíčci	mazlíček	k1gMnPc1	mazlíček
ani	ani	k8xC	ani
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
kdekoli	kdekoli	k6eAd1	kdekoli
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
<s>
Jediní	jediný	k2eAgMnPc1d1	jediný
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
je	být	k5eAaImIp3nS	být
dovoleno	dovolen	k2eAgNnSc1d1	dovoleno
chovat	chovat	k5eAaImF	chovat
koaly	koala	k1gFnPc4	koala
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
pracovníci	pracovník	k1gMnPc1	pracovník
v	v	k7c6	v
útulcích	útulek	k1gInPc6	útulek
pro	pro	k7c4	pro
zraněná	zraněný	k2eAgNnPc4d1	zraněné
divoká	divoký	k2eAgNnPc4d1	divoké
zvířata	zvíře	k1gNnPc4	zvíře
a	a	k8xC	a
občas	občas	k6eAd1	občas
výzkumníci	výzkumník	k1gMnPc1	výzkumník
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
lidé	člověk	k1gMnPc1	člověk
obdrží	obdržet	k5eAaPmIp3nP	obdržet
speciální	speciální	k2eAgNnPc4d1	speciální
povolení	povolení	k1gNnPc4	povolení
k	k	k7c3	k
péči	péče	k1gFnSc3	péče
o	o	k7c4	o
koaly	koala	k1gFnPc4	koala
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
musí	muset	k5eAaImIp3nP	muset
je	on	k3xPp3gFnPc4	on
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
volné	volný	k2eAgFnSc2d1	volná
přírody	příroda	k1gFnSc2	příroda
hned	hned	k6eAd1	hned
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
dostatečně	dostatečně	k6eAd1	dostatečně
dobré	dobrý	k2eAgFnSc6d1	dobrá
kondici	kondice	k1gFnSc6	kondice
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
mláďat	mládě	k1gNnPc2	mládě
hned	hned	k6eAd1	hned
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
jsou	být	k5eAaImIp3nP	být
dostatečně	dostatečně	k6eAd1	dostatečně
stará	starý	k2eAgNnPc1d1	staré
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
druh	druh	k1gInSc1	druh
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
území	území	k1gNnSc6	území
čtyř	čtyři	k4xCgInPc2	čtyři
australských	australský	k2eAgMnPc2d1	australský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c2	za
<g/>
:	:	kIx,	:
Queensland	Queensland	k1gInSc1	Queensland
–	–	k?	–
"	"	kIx"	"
<g/>
málo	málo	k6eAd1	málo
dotčený	dotčený	k2eAgInSc1d1	dotčený
druh	druh	k1gInSc1	druh
<g/>
"	"	kIx"	"
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
státu	stát	k1gInSc2	stát
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
bioregionu	bioregion	k1gInSc2	bioregion
severovýchodního	severovýchodní	k2eAgInSc2d1	severovýchodní
Queenslandu	Queensland	k1gInSc2	Queensland
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
veden	vést	k5eAaImNgInS	vést
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
zranitelný	zranitelný	k2eAgMnSc1d1	zranitelný
<g/>
"	"	kIx"	"
Nový	nový	k2eAgInSc1d1	nový
Jižní	jižní	k2eAgInSc1d1	jižní
Wales	Wales	k1gInSc1	Wales
–	–	k?	–
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
zranitelný	zranitelný	k2eAgInSc1d1	zranitelný
<g/>
"	"	kIx"	"
v	v	k7c6	v
celostátním	celostátní	k2eAgNnSc6d1	celostátní
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
místně	místně	k6eAd1	místně
kolísá	kolísat	k5eAaImIp3nS	kolísat
mezi	mezi	k7c4	mezi
"	"	kIx"	"
<g/>
stabilizovaný	stabilizovaný	k2eAgInSc4d1	stabilizovaný
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
lokálně	lokálně	k6eAd1	lokálně
vymřelý	vymřelý	k2eAgInSc1d1	vymřelý
<g/>
"	"	kIx"	"
Jižní	jižní	k2eAgFnSc1d1	jižní
Austrálie	Austrálie	k1gFnSc1	Austrálie
–	–	k?	–
"	"	kIx"	"
<g/>
vzácný	vzácný	k2eAgMnSc1d1	vzácný
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ačkoli	ačkoli	k8xS	ačkoli
populace	populace	k1gFnSc2	populace
na	na	k7c6	na
Klokaním	klokaní	k2eAgInSc6d1	klokaní
ostrově	ostrov	k1gInSc6	ostrov
prosperuje	prosperovat	k5eAaImIp3nS	prosperovat
<g/>
)	)	kIx)	)
Victoria	Victorium	k1gNnSc2	Victorium
-	-	kIx~	-
početnou	početný	k2eAgFnSc7d1	početná
a	a	k8xC	a
prosperující	prosperující	k2eAgFnSc4d1	prosperující
populaci	populace	k1gFnSc4	populace
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
článku	článek	k1gInSc2	článek
naposledy	naposledy	k6eAd1	naposledy
revidovaném	revidovaný	k2eAgInSc6d1	revidovaný
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Koala	koala	k1gFnSc1	koala
medvídkovitý	medvídkovitý	k2eAgMnSc1d1	medvídkovitý
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgMnSc1d1	jediný
zástupce	zástupce	k1gMnSc1	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
koalovitých	koalovitý	k2eAgMnPc2d1	koalovitý
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
vak	vak	k1gInSc1	vak
koalů	koal	k1gMnPc2	koal
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
vak	vak	k1gInSc1	vak
vombatů	vombat	k1gMnPc2	vombat
<g/>
,	,	kIx,	,
otevírá	otevírat	k5eAaImIp3nS	otevírat
dozadu	dozadu	k6eAd1	dozadu
<g/>
,	,	kIx,	,
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
budou	být	k5eAaImBp3nP	být
vombati	vombat	k1gMnPc1	vombat
nejbližší	blízký	k2eAgMnPc1d3	nejbližší
příbuzní	příbuzný	k1gMnPc1	příbuzný
koalů	koal	k1gMnPc2	koal
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgMnPc2d1	další
vačnatců	vačnatec	k1gMnPc2	vačnatec
je	být	k5eAaImIp3nS	být
penis	penis	k1gInSc1	penis
samců	samec	k1gMnPc2	samec
na	na	k7c6	na
konci	konec	k1gInSc6	konec
rozeklaný	rozeklaný	k2eAgMnSc1d1	rozeklaný
<g/>
;	;	kIx,	;
samice	samice	k1gFnPc1	samice
mají	mít	k5eAaImIp3nP	mít
dvě	dva	k4xCgFnPc4	dva
boční	boční	k2eAgFnPc4d1	boční
vagíny	vagína	k1gFnPc4	vagína
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
oddělené	oddělený	k2eAgFnPc4d1	oddělená
dělohy	děloha	k1gFnPc4	děloha
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
vačnatce	vačnatec	k1gMnPc4	vačnatec
<g/>
.	.	kIx.	.
</s>
<s>
Mozek	mozek	k1gInSc1	mozek
koalů	koal	k1gInPc2	koal
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejmenších	malý	k2eAgMnPc2d3	nejmenší
mezi	mezi	k7c7	mezi
vačnatci	vačnatec	k1gMnPc7	vačnatec
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
2	[number]	k4	2
%	%	kIx~	%
tělesné	tělesný	k2eAgFnSc2d1	tělesná
váhy	váha	k1gFnSc2	váha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mozek	mozek	k1gInSc1	mozek
předků	předek	k1gMnPc2	předek
koaly	koala	k1gFnSc2	koala
původně	původně	k6eAd1	původně
vyplňoval	vyplňovat	k5eAaImAgMnS	vyplňovat
celou	celý	k2eAgFnSc4d1	celá
dutinu	dutina	k1gFnSc4	dutina
lebeční	lebeční	k2eAgFnSc4d1	lebeční
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
zmenšil	zmenšit	k5eAaPmAgInS	zmenšit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
kvůli	kvůli	k7c3	kvůli
specializaci	specializace	k1gFnSc3	specializace
na	na	k7c4	na
energeticky	energeticky	k6eAd1	energeticky
chudou	chudý	k2eAgFnSc4d1	chudá
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Koala	koala	k1gFnSc1	koala
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgNnSc7d1	jediné
zvířetem	zvíře	k1gNnSc7	zvíře
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
s	s	k7c7	s
takto	takto	k6eAd1	takto
drasticky	drasticky	k6eAd1	drasticky
zmenšeným	zmenšený	k2eAgInSc7d1	zmenšený
mozkem	mozek	k1gInSc7	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Linuxová	linuxový	k2eAgFnSc1d1	linuxová
distribuce	distribuce	k1gFnSc1	distribuce
Ubuntu	Ubunt	k1gInSc2	Ubunt
z	z	k7c2	z
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
Karmic	Karmic	k1gMnSc1	Karmic
Koala	koala	k1gFnSc1	koala
(	(	kIx(	(
<g/>
osudná	osudný	k2eAgFnSc1d1	osudná
koala	koala	k1gFnSc1	koala
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
potravní	potravní	k2eAgFnSc3d1	potravní
specializaci	specializace	k1gFnSc3	specializace
je	být	k5eAaImIp3nS	být
chov	chov	k1gInSc1	chov
koal	koala	k1gFnPc2	koala
možný	možný	k2eAgInSc1d1	možný
jen	jen	k6eAd1	jen
ve	v	k7c6	v
vybraných	vybraný	k2eAgNnPc6d1	vybrané
ZOO	zoo	k1gNnPc6	zoo
<g/>
.	.	kIx.	.
</s>
<s>
Expozice	expozice	k1gFnSc1	expozice
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
velké	velký	k2eAgNnSc1d1	velké
<g/>
,	,	kIx,	,
prostorné	prostorný	k2eAgNnSc1d1	prostorné
<g/>
,	,	kIx,	,
s	s	k7c7	s
vysokými	vysoký	k2eAgInPc7d1	vysoký
parkosy	parkos	k1gInPc7	parkos
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
vhodné	vhodný	k2eAgNnSc4d1	vhodné
mikroklima	mikroklima	k1gNnSc4	mikroklima
<g/>
.	.	kIx.	.
</s>
<s>
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
mimo	mimo	k7c4	mimo
australské	australský	k2eAgNnSc4d1	Australské
území	území	k1gNnSc4	území
je	být	k5eAaImIp3nS	být
podmíněno	podmínit	k5eAaPmNgNnS	podmínit
pravidelným	pravidelný	k2eAgNnSc7d1	pravidelné
zajišťováním	zajišťování	k1gNnSc7	zajišťování
dostatečného	dostatečný	k2eAgNnSc2d1	dostatečné
množství	množství	k1gNnSc2	množství
čerstvých	čerstvý	k2eAgFnPc2d1	čerstvá
blahovičníkových	blahovičníkový	k2eAgFnPc2d1	blahovičníkový
větví	větev	k1gFnPc2	větev
a	a	k8xC	a
dodržování	dodržování	k1gNnSc4	dodržování
podmínek	podmínka	k1gFnPc2	podmínka
stanovených	stanovený	k2eAgFnPc2d1	stanovená
koordinátorem	koordinátor	k1gMnSc7	koordinátor
chovu	chov	k1gInSc2	chov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
chovají	chovat	k5eAaImIp3nP	chovat
koaly	koala	k1gFnSc2	koala
ZOO	zoo	k1gFnPc2	zoo
Madrid	Madrid	k1gInSc1	Madrid
<g/>
,	,	kIx,	,
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
,	,	kIx,	,
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
a	a	k8xC	a
Wild	Wild	k1gInSc1	Wild
Animal	animal	k1gMnSc1	animal
Park	park	k1gInSc1	park
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
,	,	kIx,	,
v	v	k7c6	v
USA	USA	kA	USA
v	v	k7c6	v
San	San	k1gFnSc6	San
Diegu	Dieg	k1gInSc2	Dieg
a	a	k8xC	a
v	v	k7c6	v
Jihoafrické	jihoafrický	k2eAgFnSc6d1	Jihoafrická
republice	republika	k1gFnSc6	republika
v	v	k7c6	v
Pretorii	Pretorie	k1gFnSc6	Pretorie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
také	také	k9	také
ZOO	zoo	k1gNnSc1	zoo
Lipsko	Lipsko	k1gNnSc1	Lipsko
a	a	k8xC	a
ZOO	zoo	k1gFnSc1	zoo
Drážďany	Drážďany	k1gInPc1	Drážďany
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
koala	koala	k1gFnSc1	koala
medvídkovitý	medvídkovitý	k2eAgMnSc1d1	medvídkovitý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Taxon	taxon	k1gInSc1	taxon
Phascolarctos	Phascolarctos	k1gMnSc1	Phascolarctos
cinereus	cinereus	k1gMnSc1	cinereus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
