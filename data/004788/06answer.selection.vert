<s>
Hepatitida	hepatitida	k1gFnSc1	hepatitida
E	E	kA	E
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
RNA	RNA	kA	RNA
virem	vir	k1gInSc7	vir
hepatitidy	hepatitida	k1gFnSc2	hepatitida
E	E	kA	E
(	(	kIx(	(
<g/>
HEV	HEV	kA	HEV
<g/>
)	)	kIx)	)
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
Hepeviridae	Hepevirida	k1gFnSc2	Hepevirida
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
podobné	podobný	k2eAgInPc4d1	podobný
příznaky	příznak	k1gInPc4	příznak
jako	jako	k8xC	jako
hepatitida	hepatitida	k1gFnSc1	hepatitida
A	A	kA	A
<g/>
.	.	kIx.	.
</s>
