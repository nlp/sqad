<s>
Během	během	k7c2	během
holokaustu	holokaust	k1gInSc2	holokaust
přišel	přijít	k5eAaPmAgInS	přijít
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
celou	celá	k1gFnSc4	celá
svoji	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jej	on	k3xPp3gNnSc2	on
hluboce	hluboko	k6eAd1	hluboko
poznamenalo	poznamenat	k5eAaPmAgNnS	poznamenat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
už	už	k9	už
od	od	k7c2	od
prvních	první	k4xOgInPc2	první
povídkových	povídkový	k2eAgInPc2d1	povídkový
souborů	soubor	k1gInPc2	soubor
zabývají	zabývat	k5eAaImIp3nP	zabývat
právě	právě	k9	právě
tematikou	tematika	k1gFnSc7	tematika
Židů	Žid	k1gMnPc2	Žid
a	a	k8xC	a
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
