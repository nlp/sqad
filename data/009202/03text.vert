<p>
<s>
Arnošt	Arnošt	k1gMnSc1	Arnošt
Lustig	Lustig	k1gMnSc1	Lustig
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1926	[number]	k4	1926
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
židovský	židovský	k2eAgMnSc1d1	židovský
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
světového	světový	k2eAgInSc2d1	světový
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
děl	dělo	k1gNnPc2	dělo
s	s	k7c7	s
tématem	téma	k1gNnSc7	téma
holokaustu	holokaust	k1gInSc2	holokaust
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
malého	malý	k2eAgMnSc2d1	malý
obchodníka	obchodník	k1gMnSc2	obchodník
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze-Libni	Praze-Libni	k1gFnSc6	Praze-Libni
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
a	a	k8xC	a
sestrou	sestra	k1gFnSc7	sestra
Hanou	Hana	k1gFnSc7	Hana
bydlel	bydlet	k5eAaImAgMnS	bydlet
na	na	k7c6	na
Královské	královský	k2eAgFnSc6d1	královská
třídě	třída	k1gFnSc6	třída
čp.	čp.	k?	čp.
428	[number]	k4	428
(	(	kIx(	(
<g/>
Sokolovská	sokolovský	k2eAgNnPc4d1	Sokolovské
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
čase	čas	k1gInSc6	čas
na	na	k7c6	na
Královské	královský	k2eAgFnSc6d1	královská
čp.	čp.	k?	čp.
137	[number]	k4	137
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
také	také	k9	také
vychodil	vychodit	k5eAaImAgInS	vychodit
obecnou	obecný	k2eAgFnSc4d1	obecná
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
reálce	reálka	k1gFnSc6	reálka
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
však	však	k9	však
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
z	z	k7c2	z
rasových	rasový	k2eAgInPc2d1	rasový
důvodů	důvod	k1gInPc2	důvod
vyloučen	vyloučen	k2eAgMnSc1d1	vyloučen
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
Žid	Žid	k1gMnSc1	Žid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vyučil	vyučit	k5eAaPmAgMnS	vyučit
krejčím	krejčí	k1gMnSc7	krejčí
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1942	[number]	k4	1942
byl	být	k5eAaImAgInS	být
poslán	poslat	k5eAaPmNgMnS	poslat
do	do	k7c2	do
Terezína	Terezín	k1gInSc2	Terezín
<g/>
;	;	kIx,	;
později	pozdě	k6eAd2	pozdě
poznal	poznat	k5eAaPmAgInS	poznat
i	i	k9	i
další	další	k2eAgInPc4d1	další
koncentrační	koncentrační	k2eAgInPc4d1	koncentrační
tábory	tábor	k1gInPc4	tábor
<g/>
,	,	kIx,	,
Osvětim	Osvětim	k1gFnSc4	Osvětim
a	a	k8xC	a
Buchenwald	Buchenwald	k1gInSc4	Buchenwald
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1945	[number]	k4	1945
jako	jako	k8xS	jako
zázrakem	zázrak	k1gInSc7	zázrak
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
z	z	k7c2	z
transportu	transport	k1gInSc2	transport
smrti	smrt	k1gFnSc2	smrt
(	(	kIx(	(
<g/>
z	z	k7c2	z
Buchenwaldu	Buchenwald	k1gInSc2	Buchenwald
do	do	k7c2	do
Dachau	Dachaus	k1gInSc2	Dachaus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
ukrýval	ukrývat	k5eAaImAgMnS	ukrývat
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
holokaustu	holokaust	k1gInSc2	holokaust
přišel	přijít	k5eAaPmAgInS	přijít
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
celou	celá	k1gFnSc4	celá
svoji	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jej	on	k3xPp3gNnSc2	on
hluboce	hluboko	k6eAd1	hluboko
poznamenalo	poznamenat	k5eAaPmAgNnS	poznamenat
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
už	už	k9	už
od	od	k7c2	od
prvních	první	k4xOgInPc2	první
povídkových	povídkový	k2eAgInPc2d1	povídkový
souborů	soubor	k1gInPc2	soubor
zabývají	zabývat	k5eAaImIp3nP	zabývat
právě	právě	k9	právě
tematikou	tematika	k1gFnSc7	tematika
Židů	Žid	k1gMnPc2	Žid
a	a	k8xC	a
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
politických	politický	k2eAgFnPc2d1	politická
a	a	k8xC	a
sociálních	sociální	k2eAgFnPc2d1	sociální
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
také	také	k9	také
přispívat	přispívat	k5eAaImF	přispívat
do	do	k7c2	do
novin	novina	k1gFnPc2	novina
a	a	k8xC	a
časopisů	časopis	k1gInPc2	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Izraele	Izrael	k1gInSc2	Izrael
jako	jako	k8xC	jako
zpravodaj	zpravodaj	k1gInSc1	zpravodaj
Lidových	lidový	k2eAgFnPc2d1	lidová
novin	novina	k1gFnPc2	novina
v	v	k7c6	v
izraelsko-arabské	izraelskorabský	k2eAgFnSc6d1	izraelsko-arabská
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
redaktor	redaktor	k1gMnSc1	redaktor
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
spřátelil	spřátelit	k5eAaPmAgMnS	spřátelit
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
žákem	žák	k1gMnSc7	žák
F.	F.	kA	F.
R.	R.	kA	R.
Krause	Kraus	k1gMnSc2	Kraus
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgMnS	být
vedoucím	vedoucí	k1gMnSc7	vedoucí
kulturní	kulturní	k2eAgFnSc2d1	kulturní
rubriky	rubrika	k1gFnSc2	rubrika
týdeníku	týdeník	k1gInSc2	týdeník
Mladý	mladý	k2eAgInSc4d1	mladý
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
i	i	k9	i
scenáristou	scenárista	k1gMnSc7	scenárista
Čs	čs	kA	čs
<g/>
.	.	kIx.	.
filmu	film	k1gInSc2	film
(	(	kIx(	(
<g/>
Barrandov	Barrandov	k1gInSc1	Barrandov
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
napsal	napsat	k5eAaBmAgMnS	napsat
mj.	mj.	kA	mj.
scénář	scénář	k1gInSc1	scénář
podle	podle	k7c2	podle
své	svůj	k3xOyFgFnSc2	svůj
novely	novela	k1gFnSc2	novela
Modlitba	modlitba	k1gFnSc1	modlitba
pro	pro	k7c4	pro
Kateřinu	Kateřina	k1gFnSc4	Kateřina
Horovitzovou	Horovitzový	k2eAgFnSc4d1	Horovitzový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
padesátých	padesátý	k4xOgNnPc2	padesátý
a	a	k8xC	a
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
několik	několik	k4yIc1	několik
zaměstnání	zaměstnání	k1gNnPc2	zaměstnání
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
Komunistické	komunistický	k2eAgFnSc2d1	komunistická
strany	strana	k1gFnSc2	strana
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1968	[number]	k4	1968
trávil	trávit	k5eAaImAgMnS	trávit
Arnošt	Arnošt	k1gMnSc1	Arnošt
Lustig	Lustig	k1gMnSc1	Lustig
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
a	a	k8xC	a
dalšími	další	k2eAgMnPc7d1	další
novináři	novinář	k1gMnPc7	novinář
a	a	k8xC	a
umělci	umělec	k1gMnPc1	umělec
dovolenou	dovolená	k1gFnSc4	dovolená
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
ráno	ráno	k6eAd1	ráno
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
o	o	k7c6	o
invazi	invaze	k1gFnSc6	invaze
vojsk	vojsko	k1gNnPc2	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
se	se	k3xPyFc4	se
už	už	k6eAd1	už
se	s	k7c7	s
ženou	žena	k1gFnSc7	žena
a	a	k8xC	a
dětmi	dítě	k1gFnPc7	dítě
nevrátili	vrátit	k5eNaPmAgMnP	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
odjeli	odjet	k5eAaPmAgMnP	odjet
do	do	k7c2	do
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
záhřebském	záhřebský	k2eAgNnSc6d1	Záhřebské
filmovém	filmový	k2eAgNnSc6d1	filmové
studiu	studio	k1gNnSc6	studio
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
usadili	usadit	k5eAaPmAgMnP	usadit
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Lustig	Lustig	k1gMnSc1	Lustig
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
přednášel	přednášet	k5eAaImAgMnS	přednášet
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
literaturu	literatura	k1gFnSc4	literatura
a	a	k8xC	a
scenáristiku	scenáristika	k1gFnSc4	scenáristika
na	na	k7c6	na
Americké	americký	k2eAgFnSc6d1	americká
univerzitě	univerzita	k1gFnSc6	univerzita
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
washingtonské	washingtonský	k2eAgInPc4d1	washingtonský
American	American	k1gInSc4	American
University	universita	k1gFnSc2	universita
jmenován	jmenován	k2eAgInSc4d1	jmenován
profesorem	profesor	k1gMnSc7	profesor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
české	český	k2eAgFnSc2d1	Česká
verze	verze	k1gFnSc2	verze
časopisu	časopis	k1gInSc2	časopis
Playboy	playboy	k1gMnSc1	playboy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2011	[number]	k4	2011
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkaz	odkaz	k1gInSc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
výročí	výročí	k1gNnSc3	výročí
spisovatelova	spisovatelův	k2eAgNnSc2d1	spisovatelovo
úmrtí	úmrtí	k1gNnSc2	úmrtí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byla	být	k5eAaImAgFnS	být
Česko-izraelskou	českozraelský	k2eAgFnSc7d1	česko-izraelská
smíšenou	smíšený	k2eAgFnSc7d1	smíšená
obchodní	obchodní	k2eAgFnSc7d1	obchodní
komorou	komora	k1gFnSc7	komora
zřízena	zřízen	k2eAgFnSc1d1	zřízena
Cena	cena	k1gFnSc1	cena
Arnošta	Arnošt	k1gMnSc2	Arnošt
Lustiga	Lustig	k1gMnSc2	Lustig
<g/>
.	.	kIx.	.
</s>
<s>
Oceněnými	oceněný	k2eAgInPc7d1	oceněný
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
spojují	spojovat	k5eAaImIp3nP	spojovat
vlastnosti	vlastnost	k1gFnSc3	vlastnost
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
odvaha	odvaha	k1gFnSc1	odvaha
a	a	k8xC	a
statečnost	statečnost	k1gFnSc1	statečnost
<g/>
,	,	kIx,	,
lidskost	lidskost	k1gFnSc1	lidskost
a	a	k8xC	a
spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
tvorba	tvorba	k1gFnSc1	tvorba
málokdy	málokdy	k6eAd1	málokdy
vybočila	vybočit	k5eAaPmAgFnS	vybočit
z	z	k7c2	z
tématu	téma	k1gNnSc2	téma
holokaustu	holokaust	k1gInSc2	holokaust
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
knihy	kniha	k1gFnPc1	kniha
náleží	náležet	k5eAaImIp3nP	náležet
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
druhé	druhý	k4xOgFnSc3	druhý
vlně	vlna	k1gFnSc3	vlna
válečné	válečný	k2eAgFnSc2d1	válečná
prózy	próza	k1gFnSc2	próza
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
válečná	válečná	k1gFnSc1	válečná
próza	próza	k1gFnSc1	próza
z	z	k7c2	z
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
druhé	druhý	k4xOgFnSc2	druhý
vlny	vlna	k1gFnSc2	vlna
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
soustředili	soustředit	k5eAaPmAgMnP	soustředit
na	na	k7c4	na
psychiku	psychika	k1gFnSc4	psychika
a	a	k8xC	a
vztah	vztah	k1gInSc4	vztah
jedince	jedinec	k1gMnSc2	jedinec
k	k	k7c3	k
době	doba	k1gFnSc3	doba
než	než	k8xS	než
na	na	k7c4	na
události	událost	k1gFnPc4	událost
v	v	k7c6	v
širších	široký	k2eAgFnPc6d2	širší
souvislostech	souvislost	k1gFnPc6	souvislost
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
faktograficky	faktograficky	k6eAd1	faktograficky
zaměřených	zaměřený	k2eAgMnPc2d1	zaměřený
autorů	autor	k1gMnPc2	autor
vlny	vlna	k1gFnSc2	vlna
první	první	k4xOgFnSc6	první
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rysy	rys	k1gInPc1	rys
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
:	:	kIx,	:
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
drastických	drastický	k2eAgFnPc2d1	drastická
zkušeností	zkušenost	k1gFnPc2	zkušenost
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
(	(	kIx(	(
<g/>
vydává	vydávat	k5eAaImIp3nS	vydávat
osobní	osobní	k2eAgFnSc4d1	osobní
zpověď	zpověď	k1gFnSc4	zpověď
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chtěl	chtít	k5eAaImAgMnS	chtít
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
své	svůj	k3xOyFgFnSc3	svůj
dceři	dcera	k1gFnSc3	dcera
vylíčit	vylíčit	k5eAaPmF	vylíčit
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
osudy	osud	k1gInPc4	osud
mladých	mladý	k2eAgFnPc2d1	mladá
dívek	dívka	k1gFnPc2	dívka
<g/>
,	,	kIx,	,
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
starých	starý	k2eAgMnPc2d1	starý
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
nejvíce	nejvíce	k6eAd1	nejvíce
zranitelní	zranitelný	k2eAgMnPc1d1	zranitelný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
bezbranní	bezbranný	k2eAgMnPc1d1	bezbranný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc4	všechen
události	událost	k1gFnPc4	událost
představuje	představovat	k5eAaImIp3nS	představovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
každodenní	každodenní	k2eAgFnPc1d1	každodenní
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
povídky	povídka	k1gFnSc2	povídka
nebo	nebo	k8xC	nebo
novely	novela	k1gFnSc2	novela
a	a	k8xC	a
první	první	k4xOgInSc4	první
vznikaly	vznikat	k5eAaImAgInP	vznikat
už	už	k6eAd1	už
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
pol.	pol.	k?	pol.
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
příběhy	příběh	k1gInPc4	příběh
neustále	neustále	k6eAd1	neustále
přepracovává	přepracovávat	k5eAaImIp3nS	přepracovávat
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
svých	svůj	k3xOyFgFnPc2	svůj
prvních	první	k4xOgFnPc2	první
próz	próza	k1gFnPc2	próza
<g/>
,	,	kIx,	,
soustředěných	soustředěný	k2eAgFnPc2d1	soustředěná
v	v	k7c6	v
povídkových	povídkový	k2eAgFnPc6d1	povídková
sbírkách	sbírka	k1gFnPc6	sbírka
Noc	noc	k1gFnSc1	noc
a	a	k8xC	a
naděje	naděje	k1gFnPc1	naděje
a	a	k8xC	a
Démanty	démant	k1gInPc1	démant
noci	noc	k1gFnSc2	noc
píše	psát	k5eAaImIp3nS	psát
Lustig	Lustig	k1gMnSc1	Lustig
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
centru	centrum	k1gNnSc6	centrum
stojí	stát	k5eAaImIp3nS	stát
obyčejné	obyčejný	k2eAgNnSc1d1	obyčejné
<g/>
,	,	kIx,	,
zdánlivě	zdánlivě	k6eAd1	zdánlivě
nehrdinské	hrdinský	k2eNgInPc1d1	nehrdinský
lidské	lidský	k2eAgInPc1d1	lidský
charaktery	charakter	k1gInPc1	charakter
konfrontované	konfrontovaný	k2eAgInPc1d1	konfrontovaný
s	s	k7c7	s
mezní	mezní	k2eAgFnSc7d1	mezní
životní	životní	k2eAgFnSc7d1	životní
situací	situace	k1gFnSc7	situace
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Válečnou	válečný	k2eAgFnSc4d1	válečná
tematiku	tematika	k1gFnSc4	tematika
neopouští	opouštět	k5eNaImIp3nS	opouštět
ani	ani	k8xC	ani
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
rozsáhlejších	rozsáhlý	k2eAgInPc6d2	rozsáhlejší
<g/>
,	,	kIx,	,
psychologicky	psychologicky	k6eAd1	psychologicky
laděných	laděný	k2eAgInPc6d1	laděný
textech	text	k1gInPc6	text
Můj	můj	k3xOp1gMnSc1	můj
známý	známý	k1gMnSc1	známý
Vili	vít	k5eAaImAgMnP	vít
Feld	Feld	k1gMnSc1	Feld
a	a	k8xC	a
Dita	Dita	k1gFnSc1	Dita
Saxová	Saxová	k1gFnSc1	Saxová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
prózách	próza	k1gFnPc6	próza
i	i	k8xC	i
s	s	k7c7	s
odstupem	odstup	k1gInSc7	odstup
padesáti	padesát	k4xCc2	padesát
let	léto	k1gNnPc2	léto
hledá	hledat	k5eAaImIp3nS	hledat
Lustig	Lustig	k1gMnSc1	Lustig
smysl	smysl	k1gInSc4	smysl
lidství	lidství	k1gNnSc2	lidství
a	a	k8xC	a
podstatu	podstata	k1gFnSc4	podstata
lidského	lidský	k2eAgNnSc2d1	lidské
bytí	bytí	k1gNnSc2	bytí
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnPc4	jeho
prózy	próza	k1gFnPc4	próza
<g/>
,	,	kIx,	,
poznamenané	poznamenaný	k2eAgFnPc4d1	poznamenaná
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
s	s	k7c7	s
nacistickou	nacistický	k2eAgFnSc7d1	nacistická
genocidou	genocida	k1gFnSc7	genocida
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
věcnost	věcnost	k1gFnSc1	věcnost
podání	podání	k1gNnSc1	podání
a	a	k8xC	a
střízlivá	střízlivý	k2eAgFnSc1d1	střízlivá
dramatičnost	dramatičnost	k1gFnSc1	dramatičnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Noc	noc	k1gFnSc1	noc
a	a	k8xC	a
naděje	naděje	k1gFnSc1	naděje
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
–	–	k?	–
sb	sb	kA	sb
<g/>
.	.	kIx.	.
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
7	[number]	k4	7
povídek	povídka	k1gFnPc2	povídka
inspirovaných	inspirovaný	k2eAgFnPc2d1	inspirovaná
vlastními	vlastní	k2eAgInPc7d1	vlastní
zážitky	zážitek	k1gInPc7	zážitek
autora	autor	k1gMnSc2	autor
v	v	k7c6	v
terezínském	terezínský	k2eAgNnSc6d1	Terezínské
ghettu	ghetto	k1gNnSc6	ghetto
</s>
</p>
<p>
<s>
Démanty	démant	k1gInPc1	démant
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
–	–	k?	–
sb	sb	kA	sb
<g/>
.	.	kIx.	.
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
povídku	povídka	k1gFnSc4	povídka
Tma	tma	k6eAd1	tma
nemá	mít	k5eNaImIp3nS	mít
stín	stín	k1gInSc1	stín
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
scénáře	scénář	k1gInSc2	scénář
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgNnSc6	který
se	se	k3xPyFc4	se
A.	A.	kA	A.
L.	L.	kA	L.
rovněž	rovněž	k9	rovněž
podílel	podílet	k5eAaImAgInS	podílet
<g/>
,	,	kIx,	,
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Jana	Jan	k1gMnSc2	Jan
Němce	Němec	k1gMnSc2	Němec
roku	rok	k1gInSc2	rok
1964	[number]	k4	1964
zfilmována	zfilmován	k2eAgFnSc1d1	zfilmována
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
klenotům	klenot	k1gInPc3	klenot
české	český	k2eAgFnSc2d1	Česká
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
,	,	kIx,	,
Kniha	kniha	k1gFnSc1	kniha
povídek	povídka	k1gFnPc2	povídka
a	a	k8xC	a
novel	novela	k1gFnPc2	novela
Démanty	démant	k1gInPc1	démant
noci	noc	k1gFnSc2	noc
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
a	a	k8xC	a
četných	četný	k2eAgInPc6d1	četný
překladech	překlad	k1gInPc6	překlad
pak	pak	k6eAd1	pak
vyšla	vyjít	k5eAaPmAgFnS	vyjít
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
ještě	ještě	k6eAd1	ještě
několikrát	několikrát	k6eAd1	několikrát
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
románem	román	k1gInSc7	román
Dita	Dita	k1gFnSc1	Dita
Saxová	Saxová	k1gFnSc1	Saxová
patří	patřit	k5eAaImIp3nS	patřit
nejen	nejen	k6eAd1	nejen
k	k	k7c3	k
vrcholům	vrchol	k1gInPc3	vrchol
Lustigovy	Lustigův	k2eAgFnSc2d1	Lustigova
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
do	do	k7c2	do
zlatého	zlatý	k2eAgInSc2d1	zlatý
fondu	fond	k1gInSc2	fond
poválečné	poválečný	k2eAgFnSc2d1	poválečná
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ulice	ulice	k1gFnSc1	ulice
ztracených	ztracený	k2eAgMnPc2d1	ztracený
bratří	bratr	k1gMnPc2	bratr
<g/>
,	,	kIx,	,
1959	[number]	k4	1959
–	–	k?	–
sb	sb	kA	sb
<g/>
.	.	kIx.	.
povídek	povídka	k1gFnPc2	povídka
</s>
</p>
<p>
<s>
Můj	můj	k3xOp1gMnSc1	můj
známý	známý	k1gMnSc1	známý
Vili	vít	k5eAaImAgMnP	vít
Feld	Feld	k1gMnSc1	Feld
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
–	–	k?	–
novela	novela	k1gFnSc1	novela
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc1	příběh
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
obratně	obratně	k6eAd1	obratně
získává	získávat	k5eAaImIp3nS	získávat
prospěch	prospěch	k1gInSc4	prospěch
v	v	k7c6	v
Němci	Němec	k1gMnSc6	Němec
obsazené	obsazený	k2eAgFnSc2d1	obsazená
Praze	Praha	k1gFnSc3	Praha
i	i	k8xC	i
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
(	(	kIx(	(
<g/>
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
pak	pak	k6eAd1	pak
deziluze	deziluze	k1gFnSc2	deziluze
a	a	k8xC	a
pocit	pocit	k1gInSc4	pocit
zbytečnosti	zbytečnost	k1gFnSc2	zbytečnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
stanice	stanice	k1gFnSc1	stanice
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
,	,	kIx,	,
1961	[number]	k4	1961
–	–	k?	–
povídky	povídka	k1gFnSc2	povídka
s	s	k7c7	s
poněkud	poněkud	k6eAd1	poněkud
socialistickým	socialistický	k2eAgInSc7d1	socialistický
nádechem	nádech	k1gInSc7	nádech
</s>
</p>
<p>
<s>
Dita	Dita	k1gFnSc1	Dita
Saxová	Saxová	k1gFnSc1	Saxová
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
–	–	k?	–
novela	novela	k1gFnSc1	novela
<g/>
,	,	kIx,	,
o	o	k7c6	o
dívce	dívka	k1gFnSc6	dívka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
z	z	k7c2	z
koncentráku	koncentrák	k1gInSc2	koncentrák
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
otřesné	otřesný	k2eAgInPc4d1	otřesný
zážitky	zážitek	k1gInPc4	zážitek
schopná	schopný	k2eAgFnSc1d1	schopná
navázat	navázat	k5eAaPmF	navázat
normální	normální	k2eAgInSc4d1	normální
život	život	k1gInSc4	život
<g/>
;	;	kIx,	;
kvůli	kvůli	k7c3	kvůli
nejistotě	nejistota	k1gFnSc3	nejistota
v	v	k7c6	v
poválečném	poválečný	k2eAgInSc6d1	poválečný
světě	svět	k1gInSc6	svět
nakonec	nakonec	k6eAd1	nakonec
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
do	do	k7c2	do
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
a	a	k8xC	a
páchá	páchat	k5eAaImIp3nS	páchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
</s>
</p>
<p>
<s>
Noc	noc	k1gFnSc1	noc
a	a	k8xC	a
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
–	–	k?	–
přepracovaný	přepracovaný	k2eAgInSc4d1	přepracovaný
soubor	soubor	k1gInSc4	soubor
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
vydaných	vydaný	k2eAgFnPc2d1	vydaná
povídek	povídka	k1gFnPc2	povídka
</s>
</p>
<p>
<s>
Transport	transport	k1gInSc1	transport
z	z	k7c2	z
ráje	ráj	k1gInSc2	ráj
<g/>
,	,	kIx,	,
1962	[number]	k4	1962
</s>
</p>
<p>
<s>
Nikoho	nikdo	k3yNnSc4	nikdo
neponížíš	ponížit	k5eNaPmIp2nS	ponížit
<g/>
,	,	kIx,	,
1963	[number]	k4	1963
–	–	k?	–
sb	sb	kA	sb
<g/>
.	.	kIx.	.
4	[number]	k4	4
povídek	povídka	k1gFnPc2	povídka
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
protektorátu	protektorát	k1gInSc2	protektorát
a	a	k8xC	a
Květnové	květnový	k2eAgFnSc2d1	květnová
revoluce	revoluce	k1gFnSc2	revoluce
</s>
</p>
<p>
<s>
Modlitba	modlitba	k1gFnSc1	modlitba
pro	pro	k7c4	pro
Kateřinu	Kateřina	k1gFnSc4	Kateřina
Horovitzovou	Horovitzová	k1gFnSc4	Horovitzová
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
–	–	k?	–
novela	novela	k1gFnSc1	novela
podle	podle	k7c2	podle
skutečné	skutečný	k2eAgFnSc2d1	skutečná
události	událost	k1gFnSc2	událost
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
(	(	kIx(	(
<g/>
Tehdy	tehdy	k6eAd1	tehdy
Němci	Němec	k1gMnPc1	Němec
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
zajali	zajmout	k5eAaPmAgMnP	zajmout
bohaté	bohatý	k2eAgMnPc4d1	bohatý
židovské	židovský	k2eAgMnPc4d1	židovský
podnikatele	podnikatel	k1gMnPc4	podnikatel
s	s	k7c7	s
americkými	americký	k2eAgInPc7d1	americký
pasy	pas	k1gInPc7	pas
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
záminkami	záminka	k1gFnPc7	záminka
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
vymámí	vymámit	k5eAaPmIp3nS	vymámit
jejich	jejich	k3xOp3gInPc4	jejich
peníze	peníz	k1gInPc4	peníz
a	a	k8xC	a
ožebračí	ožebračit	k5eAaPmIp3nP	ožebračit
je	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	on	k3xPp3gInPc4	on
pošlou	poslat	k5eAaPmIp3nP	poslat
s	s	k7c7	s
mladou	mladý	k2eAgFnSc7d1	mladá
tanečnicí	tanečnice	k1gFnSc7	tanečnice
Kateřinou	Kateřina	k1gFnSc7	Kateřina
do	do	k7c2	do
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
jediná	jediný	k2eAgFnSc1d1	jediná
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
<g/>
,	,	kIx,	,
vytrhne	vytrhnout	k5eAaPmIp3nS	vytrhnout
dozorci	dozorce	k1gMnSc3	dozorce
pistoli	pistole	k1gFnSc4	pistole
a	a	k8xC	a
zastřelí	zastřelit	k5eAaPmIp3nS	zastřelit
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzpoura	vzpoura	k1gFnSc1	vzpoura
je	být	k5eAaImIp3nS	být
marná	marný	k2eAgFnSc1d1	marná
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vlny	vlna	k1gFnPc1	vlna
v	v	k7c6	v
řece	řeka	k1gFnSc6	řeka
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
–	–	k?	–
souborné	souborný	k2eAgNnSc4d1	souborné
vydání	vydání	k1gNnSc4	vydání
několika	několik	k4yIc2	několik
jeho	jeho	k3xOp3gFnPc2	jeho
předchozích	předchozí	k2eAgFnPc2d1	předchozí
prací	práce	k1gFnPc2	práce
</s>
</p>
<p>
<s>
Bílé	bílý	k2eAgFnPc1d1	bílá
břízy	bříza	k1gFnPc1	bříza
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
;	;	kIx,	;
přepracováno	přepracovat	k5eAaPmNgNnS	přepracovat
jako	jako	k9	jako
Bílé	bílý	k2eAgFnSc2d1	bílá
břízy	bříza	k1gFnSc2	bříza
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
Propast	propast	k1gFnSc1	propast
<g/>
:	:	kIx,	:
Román	Román	k1gMnSc1	Román
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
–	–	k?	–
vojín	vojín	k1gMnSc1	vojín
spadne	spadnout	k5eAaPmIp3nS	spadnout
do	do	k7c2	do
propasti	propast	k1gFnSc2	propast
a	a	k8xC	a
smrtelně	smrtelně	k6eAd1	smrtelně
se	se	k3xPyFc4	se
zraní	zranit	k5eAaPmIp3nS	zranit
<g/>
,	,	kIx,	,
bilancuje	bilancovat	k5eAaImIp3nS	bilancovat
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
</s>
</p>
<p>
<s>
Hořká	hořký	k2eAgFnSc1d1	hořká
vůně	vůně	k1gFnSc1	vůně
mandlí	mandle	k1gFnPc2	mandle
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
–	–	k?	–
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
osudy	osud	k1gInPc1	osud
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
židovských	židovský	k2eAgFnPc2d1	židovská
rodin	rodina	k1gFnPc2	rodina
zasažených	zasažený	k2eAgFnPc2d1	zasažená
holokaustem	holokaust	k1gInSc7	holokaust
<g/>
,	,	kIx,	,
tematikou	tematika	k1gFnSc7	tematika
jsou	být	k5eAaImIp3nP	být
koncentrační	koncentrační	k2eAgInPc1d1	koncentrační
tábory	tábor	k1gInPc1	tábor
<g/>
,	,	kIx,	,
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
následky	následek	k1gInPc1	následek
</s>
</p>
<p>
<s>
Miláček	miláček	k1gMnSc1	miláček
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
–	–	k?	–
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
zúročil	zúročit	k5eAaPmAgMnS	zúročit
zkušenost	zkušenost	k1gFnSc4	zkušenost
z	z	k7c2	z
izraelsko-arabského	izraelskorabský	k2eAgInSc2d1	izraelsko-arabský
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
,	,	kIx,	,
milostný	milostný	k2eAgInSc1d1	milostný
příběh	příběh	k1gInSc4	příběh
odehrávající	odehrávající	k2eAgInSc4d1	odehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
obleženém	obležený	k2eAgInSc6d1	obležený
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
,	,	kIx,	,
očima	oko	k1gNnPc7	oko
novináře	novinář	k1gMnSc2	novinář
Danyho	Danyho	k?	Danyho
Polnauera	Polnauer	k1gMnSc2	Polnauer
je	být	k5eAaImIp3nS	být
sledován	sledovat	k5eAaImNgInS	sledovat
milostný	milostný	k2eAgInSc1d1	milostný
příběh	příběh	k1gInSc1	příběh
z	z	k7c2	z
první	první	k4xOgFnSc2	první
izraelsko-arabské	izraelskorabský	k2eAgFnSc2d1	izraelsko-arabská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vydaných	vydaný	k2eAgInPc2d1	vydaný
nákladů	náklad	k1gInPc2	náklad
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nemilovaná	milovaný	k2eNgFnSc1d1	nemilovaná
<g/>
:	:	kIx,	:
Z	z	k7c2	z
deníku	deník	k1gInSc2	deník
sedmnáctileté	sedmnáctiletý	k2eAgFnSc2d1	sedmnáctiletá
Perly	perla	k1gFnSc2	perla
Sch	Sch	k1gFnSc2	Sch
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1979	[number]	k4	1979
–	–	k?	–
novela	novela	k1gFnSc1	novela
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc1	příběh
prostitutky	prostitutka	k1gFnSc2	prostitutka
v	v	k7c6	v
terezínském	terezínský	k2eAgNnSc6d1	Terezínské
ghettu	ghetto	k1gNnSc6	ghetto
stylizovaný	stylizovaný	k2eAgMnSc1d1	stylizovaný
jako	jako	k8xC	jako
její	její	k3xOp3gInSc1	její
deník	deník	k1gInSc1	deník
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
Král	Král	k1gMnSc1	Král
promluvil	promluvit	k5eAaPmAgMnS	promluvit
<g/>
,	,	kIx,	,
neřekl	říct	k5eNaPmAgMnS	říct
nic	nic	k3yNnSc4	nic
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
–	–	k?	–
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
návrat	návrat	k1gInSc1	návrat
k	k	k7c3	k
postavě	postava	k1gFnSc3	postava
Viliho	Vili	k1gMnSc2	Vili
Felda	Feld	k1gMnSc2	Feld
z	z	k7c2	z
románu	román	k1gInSc2	román
Můj	můj	k3xOp1gMnSc1	můj
známý	známý	k1gMnSc1	známý
Vili	vít	k5eAaImAgMnP	vít
Feld	Feld	k1gInSc1	Feld
</s>
</p>
<p>
<s>
Tma	tma	k1gFnSc1	tma
nemá	mít	k5eNaImIp3nS	mít
stín	stín	k1gInSc4	stín
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
</s>
</p>
<p>
<s>
Velká	velký	k2eAgFnSc1d1	velká
trojka	trojka	k1gFnSc1	trojka
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
</s>
</p>
<p>
<s>
Trilogie	trilogie	k1gFnSc1	trilogie
o	o	k7c6	o
osudech	osud	k1gInPc6	osud
tří	tři	k4xCgFnPc2	tři
židovských	židovský	k2eAgFnPc2d1	židovská
žen	žena	k1gFnPc2	žena
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Colette	Colette	k5eAaPmIp2nP	Colette
<g/>
:	:	kIx,	:
Dívka	dívka	k1gFnSc1	dívka
z	z	k7c2	z
Antverp	Antverpy	k1gFnPc2	Antverpy
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
</s>
</p>
<p>
<s>
Tanga	tango	k1gNnPc1	tango
<g/>
:	:	kIx,	:
Dívka	dívka	k1gFnSc1	dívka
z	z	k7c2	z
Hamburku	Hamburk	k1gInSc2	Hamburk
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
</s>
</p>
<p>
<s>
Lea	Lea	k1gFnSc1	Lea
<g/>
:	:	kIx,	:
Dívka	dívka	k1gFnSc1	dívka
z	z	k7c2	z
Leeuwardenu	Leeuwarden	k1gInSc2	Leeuwarden
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
vrácené	vrácený	k2eAgFnSc2d1	vrácená
ozvěny	ozvěna	k1gFnSc2	ozvěna
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
</s>
</p>
<p>
<s>
Dívka	dívka	k1gFnSc1	dívka
s	s	k7c7	s
jizvou	jizva	k1gFnSc7	jizva
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
</s>
</p>
<p>
<s>
Kamarádi	kamarád	k1gMnPc1	kamarád
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
–	–	k?	–
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
život	život	k1gInSc1	život
party	parta	k1gFnSc2	parta
židovských	židovský	k2eAgMnPc2d1	židovský
mladíků	mladík	k1gMnPc2	mladík
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Modrý	modrý	k2eAgInSc1d1	modrý
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
</s>
</p>
<p>
<s>
Porgess	Porgess	k6eAd1	Porgess
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
</s>
</p>
<p>
<s>
Neslušné	slušný	k2eNgInPc1d1	neslušný
sny	sen	k1gInPc1	sen
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
</s>
</p>
<p>
<s>
Oheň	oheň	k1gInSc1	oheň
na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
<g/>
:	:	kIx,	:
Povídky	povídka	k1gFnSc2	povídka
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
</s>
</p>
<p>
<s>
Dobrý	dobrý	k2eAgInSc1d1	dobrý
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
pane	pan	k1gMnSc5	pan
Lustig	Lustig	k1gInSc1	Lustig
<g/>
:	:	kIx,	:
Myšlenky	myšlenka	k1gFnPc1	myšlenka
o	o	k7c6	o
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
Krásné	krásný	k2eAgNnSc4d1	krásné
zelené	zelený	k2eAgNnSc4d1	zelené
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
</s>
</p>
<p>
<s>
Odpovědi	odpověď	k1gFnPc1	odpověď
<g/>
:	:	kIx,	:
Rozhovory	rozhovor	k1gInPc1	rozhovor
s	s	k7c7	s
Harry	Harr	k1gMnPc7	Harr
Jamesem	James	k1gMnSc7	James
Cargassem	Cargass	k1gMnSc7	Cargass
a	a	k8xC	a
Michalem	Michal	k1gMnSc7	Michal
Bauerem	Bauer	k1gMnSc7	Bauer
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
</s>
</p>
<p>
<s>
Eseje	esej	k1gFnPc1	esej
<g/>
:	:	kIx,	:
Vybrané	vybraný	k2eAgInPc1d1	vybraný
texty	text	k1gInPc1	text
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1965	[number]	k4	1965
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
x	x	k?	x
<g/>
18	[number]	k4	18
(	(	kIx(	(
<g/>
portréty	portrét	k1gInPc1	portrét
a	a	k8xC	a
postřehy	postřeh	k1gInPc1	postřeh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
–	–	k?	–
autobiografie	autobiografie	k1gFnSc1	autobiografie
(	(	kIx(	(
<g/>
spoluautor	spoluautor	k1gMnSc1	spoluautor
František	František	k1gMnSc1	František
Cinger	Cinger	k1gMnSc1	Cinger
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zasvěcení	zasvěcení	k1gNnSc1	zasvěcení
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
</s>
</p>
<p>
<s>
Esence	esence	k1gFnSc1	esence
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
–	–	k?	–
sbírka	sbírka	k1gFnSc1	sbírka
citátů	citát	k1gInPc2	citát
</s>
</p>
<p>
<s>
O	o	k7c6	o
literatuře	literatura	k1gFnSc6	literatura
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
–	–	k?	–
sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Láska	láska	k1gFnSc1	láska
a	a	k8xC	a
tělo	tělo	k1gNnSc1	tělo
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
(	(	kIx(	(
<g/>
novela	novela	k1gFnSc1	novela
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Okamžiky	okamžik	k1gInPc1	okamžik
s	s	k7c7	s
Otou	Ota	k1gMnSc7	Ota
Pavlem	Pavel	k1gMnSc7	Pavel
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
</s>
</p>
<p>
<s>
Případ	případ	k1gInSc1	případ
Marie	Maria	k1gFnSc2	Maria
Navarové	Navarová	k1gFnSc2	Navarová
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
z	z	k7c2	z
Lustigových	Lustigův	k2eAgFnPc2d1	Lustigova
prací	práce	k1gFnPc2	práce
se	se	k3xPyFc4	se
dočkaly	dočkat	k5eAaPmAgFnP	dočkat
televizních	televizní	k2eAgInPc2d1	televizní
a	a	k8xC	a
filmových	filmový	k2eAgFnPc2d1	filmová
adaptací	adaptace	k1gFnPc2	adaptace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
životě	život	k1gInSc6	život
a	a	k8xC	a
díle	dílo	k1gNnSc6	dílo
hovořil	hovořit	k5eAaImAgInS	hovořit
v	v	k7c6	v
knižním	knižní	k2eAgInSc6d1	knižní
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
Karlem	Karel	k1gMnSc7	Karel
Hvížďalou	Hvížďala	k1gFnSc7	Hvížďala
Tachles	Tachles	k1gMnSc1	Tachles
<g/>
,	,	kIx,	,
Lustig	Lustig	k1gMnSc1	Lustig
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vedle	vedle	k7c2	vedle
své	svůj	k3xOyFgFnSc2	svůj
literární	literární	k2eAgFnSc2d1	literární
tvorby	tvorba	k1gFnSc2	tvorba
také	také	k9	také
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
českým	český	k2eAgMnSc7d1	český
sklářem	sklář	k1gMnSc7	sklář
Janem	Jan	k1gMnSc7	Jan
Huňátem	Huňát	k1gMnSc7	Huňát
a	a	k8xC	a
podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
designu	design	k1gInSc2	design
otisku	otisk	k1gInSc2	otisk
své	svůj	k3xOyFgFnSc2	svůj
dlaně	dlaň	k1gFnSc2	dlaň
v	v	k7c6	v
křišťálovém	křišťálový	k2eAgNnSc6d1	křišťálové
skle	sklo	k1gNnSc6	sklo
-	-	kIx~	-
Křišťálovém	křišťálový	k2eAgInSc6d1	křišťálový
doteku	dotek	k1gInSc6	dotek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
1967	[number]	k4	1967
<g/>
:	:	kIx,	:
Státní	státní	k2eAgFnSc1d1	státní
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
-	-	kIx~	-
za	za	k7c4	za
televizní	televizní	k2eAgFnSc4d1	televizní
inscenaci	inscenace	k1gFnSc4	inscenace
románu	román	k1gInSc2	román
Modlitba	modlitba	k1gFnSc1	modlitba
pro	pro	k7c4	pro
Kateřinu	Kateřina	k1gFnSc4	Kateřina
Horovitzovou	Horovitzový	k2eAgFnSc4d1	Horovitzový
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
<g/>
:	:	kIx,	:
Cena	cena	k1gFnSc1	cena
Karla	Karel	k1gMnSc2	Karel
Čapka	Čapek	k1gMnSc2	Čapek
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
Čestné	čestný	k2eAgNnSc1d1	čestné
občanství	občanství	k1gNnSc1	občanství
MČ	MČ	kA	MČ
Prahy	Praha	k1gFnSc2	Praha
2	[number]	k4	2
uděleno	udělen	k2eAgNnSc1d1	uděleno
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Nominace	nominace	k1gFnSc1	nominace
na	na	k7c4	na
Pulitzerovu	Pulitzerův	k2eAgFnSc4d1	Pulitzerova
cenu	cena	k1gFnSc4	cena
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Udělena	udělen	k2eAgFnSc1d1	udělena
Cena	cena	k1gFnSc1	cena
Franze	Franze	k1gFnSc2	Franze
Kafky	Kafka	k1gMnSc2	Kafka
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Nominace	nominace	k1gFnSc1	nominace
na	na	k7c4	na
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
Man	Man	k1gMnSc1	Man
Bookerovu	Bookerův	k2eAgFnSc4d1	Bookerova
cenu	cena	k1gFnSc4	cena
-	-	kIx~	-
za	za	k7c4	za
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
přínos	přínos	k1gInSc4	přínos
světové	světový	k2eAgFnSc6d1	světová
literatuře	literatura	k1gFnSc6	literatura
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Arnošt	Arnošt	k1gMnSc1	Arnošt
Lustig	Lustiga	k1gFnPc2	Lustiga
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Arnošt	Arnošt	k1gMnSc1	Arnošt
Lustig	Lustig	k1gMnSc1	Lustig
</s>
</p>
<p>
<s>
Arnošt	Arnošt	k1gMnSc1	Arnošt
Lustig	Lustig	k1gMnSc1	Lustig
ve	v	k7c6	v
Slovníku	slovník	k1gInSc6	slovník
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
(	(	kIx(	(
<g/>
projekt	projekt	k1gInSc1	projekt
ÚČL	ÚČL	kA	ÚČL
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Festivalu	festival	k1gInSc2	festival
spisovatelů	spisovatel	k1gMnPc2	spisovatel
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
-	-	kIx~	-
Třinácta	Třinácta	k1gMnSc1	Třinácta
komnata	komnata	k1gFnSc1	komnata
Arnošta	Arnošt	k1gMnSc2	Arnošt
Lustiga	Lustig	k1gMnSc2	Lustig
</s>
</p>
<p>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
Arnošt	Arnošt	k1gMnSc1	Arnošt
Lustig	Lustig	k1gMnSc1	Lustig
</s>
</p>
<p>
<s>
Arnošt	Arnošt	k1gMnSc1	Arnošt
Lustig	Lustig	k1gMnSc1	Lustig
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Arnošt	Arnošt	k1gMnSc1	Arnošt
Lustig	Lustig	k1gMnSc1	Lustig
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
