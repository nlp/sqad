<s>
Současný	současný	k2eAgMnSc1d1	současný
papež	papež	k1gMnSc1	papež
František	František	k1gMnSc1	František
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
na	na	k7c6	na
konkláve	konkláve	k1gNnPc6	konkláve
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
k	k	k7c3	k
28	[number]	k4	28
<g/>
.	.	kIx.	.
únoru	únor	k1gInSc6	únor
2013	[number]	k4	2013
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
jeho	jeho	k3xOp3gMnSc1	jeho
předchůdce	předchůdce	k1gMnSc1	předchůdce
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
