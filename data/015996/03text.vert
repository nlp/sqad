<s>
Šest	šest	k4xCc4
stupňů	stupeň	k1gInPc2
odloučení	odloučení	k1gNnSc2
</s>
<s>
Mapa	mapa	k1gFnSc1
větví	větev	k1gFnPc2
a	a	k8xC
stupňů	stupeň	k1gInPc2
sociální	sociální	k2eAgFnSc4d1
síťe	síťe	k1gFnSc4
osobnosti	osobnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Šest	šest	k4xCc1
stupňů	stupeň	k1gInPc2
odloučení	odloučení	k1gNnPc2
</s>
<s>
Šest	šest	k4xCc4
stupňů	stupeň	k1gInPc2
odloučení	odloučení	k1gNnSc2
je	být	k5eAaImIp3nS
myšlenka	myšlenka	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
všichni	všechen	k3xTgMnPc1
lidé	člověk	k1gMnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
průměru	průměr	k1gInSc6
šest	šest	k4xCc1
nebo	nebo	k8xC
méně	málo	k6eAd2
sociálních	sociální	k2eAgInPc2d1
kontaktů	kontakt	k1gInPc2
od	od	k7c2
sebe	sebe	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
výsledku	výsledek	k1gInSc6
lze	lze	k6eAd1
vytvořit	vytvořit	k5eAaPmF
řetězec	řetězec	k1gInSc1
„	„	k?
<g/>
přítel	přítel	k1gMnSc1
přítele	přítel	k1gMnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
spojí	spoj	k1gFnSc7
libovolné	libovolný	k2eAgMnPc4d1
dva	dva	k4xCgMnPc4
lidi	člověk	k1gMnPc4
v	v	k7c4
maximálně	maximálně	k6eAd1
šesti	šest	k4xCc2
krocích	krok	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
původně	původně	k6eAd1
stanoveno	stanovit	k5eAaPmNgNnS
Frigyesem	Frigyes	k1gInSc7
Karinthym	Karinthym	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
a	a	k8xC
popularizováno	popularizovat	k5eAaImNgNnS
ve	v	k7c6
stejnojmenné	stejnojmenný	k2eAgFnSc6d1
hře	hra	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
napsané	napsaný	k2eAgFnSc2d1
Johnem	John	k1gMnSc7
Guareem	Guareus	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
se	se	k3xPyFc4
zobecňuje	zobecňovat	k5eAaImIp3nS
na	na	k7c4
průměrnou	průměrný	k2eAgFnSc4d1
sociální	sociální	k2eAgFnSc4d1
vzdálenost	vzdálenost	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
logaritmická	logaritmický	k2eAgFnSc1d1
ve	v	k7c6
velikosti	velikost	k1gFnSc6
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Raná	raný	k2eAgFnSc1d1
koncepce	koncepce	k1gFnSc1
</s>
<s>
Zmenšující	zmenšující	k2eAgMnSc1d1
se	se	k3xPyFc4
svět	svět	k1gInSc1
</s>
<s>
Teorie	teorie	k1gFnSc1
optimálního	optimální	k2eAgInSc2d1
designu	design	k1gInSc2
městských	městský	k2eAgFnPc2d1
čtvrtí	čtvrt	k1gFnPc2
a	a	k8xC
demografie	demografie	k1gFnPc1
byly	být	k5eAaImAgFnP
v	v	k7c6
módě	móda	k1gFnSc6
po	po	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
domněnky	domněnka	k1gFnPc1
byly	být	k5eAaImAgFnP
rozšířeny	rozšířit	k5eAaPmNgFnP
v	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
maďarským	maďarský	k2eAgMnSc7d1
autorem	autor	k1gMnSc7
Frigyesem	Frigyes	k1gMnSc7
Karinthym	Karinthym	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
vydal	vydat	k5eAaPmAgInS
svazek	svazek	k1gInSc4
povídek	povídka	k1gFnPc2
s	s	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
Všechno	všechen	k3xTgNnSc1
je	být	k5eAaImIp3nS
jiné	jiný	k2eAgNnSc1d1
(	(	kIx(
<g/>
Everything	Everything	k1gInSc1
is	is	k?
Different	Different	k1gInSc1
<g/>
)	)	kIx)
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
z	z	k7c2
těchto	tento	k3xDgInPc2
kousků	kousek	k1gInPc2
měl	mít	k5eAaImAgInS
název	název	k1gInSc1
„	„	k?
<g/>
Řetěz	řetěz	k1gInSc1
(	(	kIx(
<g/>
Chains	Chains	k1gInSc1
<g/>
)	)	kIx)
<g/>
“	“	k?
nebo	nebo	k8xC
„	„	k?
<g/>
Řetězové	řetězový	k2eAgNnSc1d1
spojení	spojení	k1gNnSc1
(	(	kIx(
<g/>
Chain-Links	Chain-Links	k1gInSc1
<g/>
)	)	kIx)
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příběh	příběh	k1gInSc1
zkoumal	zkoumat	k5eAaImAgInS
<g/>
,	,	kIx,
abstraktně	abstraktně	k6eAd1
<g/>
,	,	kIx,
koncepčně	koncepčně	k6eAd1
a	a	k8xC
fiktivně	fiktivně	k6eAd1
<g/>
,	,	kIx,
mnoho	mnoho	k4c4
problémů	problém	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
by	by	kYmCp3nP
zaujaly	zaujmout	k5eAaPmAgFnP
budoucí	budoucí	k2eAgFnPc4d1
generace	generace	k1gFnPc4
matematiků	matematik	k1gMnPc2
<g/>
,	,	kIx,
sociologů	sociolog	k1gMnPc2
a	a	k8xC
fyziků	fyzik	k1gMnPc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
teorie	teorie	k1gFnPc1
sítí	síť	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Díky	díky	k7c3
technologickému	technologický	k2eAgInSc3d1
pokroku	pokrok	k1gInSc3
v	v	k7c4
komunikaci	komunikace	k1gFnSc4
a	a	k8xC
cestování	cestování	k1gNnSc4
se	se	k3xPyFc4
mohly	moct	k5eAaImAgFnP
sítě	síť	k1gFnPc1
přátelství	přátelství	k1gNnSc4
zvětšit	zvětšit	k5eAaPmF
a	a	k8xC
překlenout	překlenout	k5eAaPmF
větší	veliký	k2eAgFnPc4d2
vzdálenosti	vzdálenost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karinthy	Karinth	k1gInPc7
věřil	věřit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
moderní	moderní	k2eAgInSc1d1
svět	svět	k1gInSc1
„	„	k?
<g/>
zmenšuje	zmenšovat	k5eAaImIp3nS
<g/>
“	“	k?
<g/>
,	,	kIx,
díky	dík	k1gInPc4
stále	stále	k6eAd1
se	se	k3xPyFc4
zvyšující	zvyšující	k2eAgFnPc1d1
propojenosti	propojenost	k1gFnPc1
lidských	lidský	k2eAgFnPc2d1
bytostí	bytost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládal	předpokládat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
přes	přes	k7c4
velké	velký	k2eAgFnPc4d1
fyzické	fyzický	k2eAgFnPc4d1
vzdálenosti	vzdálenost	k1gFnPc4
mezi	mezi	k7c7
jednotlivci	jednotlivec	k1gMnPc7
na	na	k7c6
celé	celý	k2eAgFnSc6d1
planetě	planeta	k1gFnSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
skutečná	skutečný	k2eAgFnSc1d1
sociální	sociální	k2eAgFnSc1d1
vzdálenost	vzdálenost	k1gFnSc1
zmenšila	zmenšit	k5eAaPmAgFnS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výsledkem	výsledek	k1gInSc7
této	tento	k3xDgFnSc2
hypotézy	hypotéza	k1gFnSc2
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
jakékoli	jakýkoli	k3yIgInPc4
dva	dva	k4xCgInPc4
jednotlivce	jednotlivec	k1gMnPc4
lze	lze	k6eAd1
spojit	spojit	k5eAaPmF
prostřednictvím	prostřednictvím	k7c2
nejvýše	nejvýše	k6eAd1,k6eAd3
pěti	pět	k4xCc2
známých	známý	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
myšlenka	myšlenka	k1gFnSc1
přímo	přímo	k6eAd1
i	i	k9
nepřímo	přímo	k6eNd1
ovlivnila	ovlivnit	k5eAaPmAgFnS
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
raných	raný	k2eAgFnPc2d1
myšlenek	myšlenka	k1gFnPc2
na	na	k7c6
sociálních	sociální	k2eAgFnPc6d1
sítích	síť	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karinthy	Karinth	k1gInPc7
byl	být	k5eAaImAgMnS
považován	považován	k2eAgMnSc1d1
za	za	k7c4
původce	původce	k1gMnSc4
pojmu	pojmout	k5eAaPmIp1nS
šest	šest	k4xCc4
stupňů	stupeň	k1gInPc2
odloučení	odloučení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Malý	malý	k2eAgInSc1d1
svět	svět	k1gInSc1
</s>
<s>
Michael	Michael	k1gMnSc1
Gurevich	Gurevich	k1gMnSc1
provedl	provést	k5eAaPmAgMnS
klíčovou	klíčový	k2eAgFnSc4d1
práci	práce	k1gFnSc4
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
empirickém	empirický	k2eAgNnSc6d1
studiu	studio	k1gNnSc6
struktury	struktura	k1gFnSc2
sociálních	sociální	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
disertační	disertační	k2eAgFnSc6d1
práci	práce	k1gFnSc6
na	na	k7c6
Massachusettském	massachusettský	k2eAgInSc6d1
technologickém	technologický	k2eAgInSc6d1
institutu	institut	k1gInSc6
(	(	kIx(
<g/>
Massachusetts	Massachusetts	k1gNnSc1
Institute	institut	k1gInSc5
of	of	k?
Technology	technolog	k1gMnPc7
<g/>
)	)	kIx)
z	z	k7c2
roku	rok	k1gInSc2
1961	#num#	k4
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Ithiel	Ithiel	k1gMnSc1
de	de	k?
Sola	Sola	k1gMnSc1
Pool	Pool	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Matematik	matematik	k1gMnSc1
Manfred	Manfred	k1gMnSc1
Kochen	Kochen	k2eAgMnSc1d1
<g/>
,	,	kIx,
Rakušan	Rakušan	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
zabýval	zabývat	k5eAaImAgInS
urbanistickým	urbanistický	k2eAgInSc7d1
designem	design	k1gInSc7
<g/>
,	,	kIx,
tyto	tento	k3xDgInPc4
empirické	empirický	k2eAgInPc4d1
výsledky	výsledek	k1gInPc4
extrapoloval	extrapolovat	k5eAaBmAgMnS
do	do	k7c2
matematického	matematický	k2eAgInSc2d1
rukopisu	rukopis	k1gInSc2
Kontakty	kontakt	k1gInPc1
a	a	k8xC
vlivy	vliv	k1gInPc1
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
dospěl	dochvít	k5eAaPmAgMnS
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
u	u	k7c2
populace	populace	k1gFnSc2
o	o	k7c6
velikosti	velikost	k1gFnSc6
USA	USA	kA
bez	bez	k7c2
sociální	sociální	k2eAgFnSc2d1
struktury	struktura	k1gFnSc2
„	„	k?
<g/>
je	být	k5eAaImIp3nS
prakticky	prakticky	k6eAd1
jisté	jistý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
dvě	dva	k4xCgFnPc1
jednotlivci	jednotlivec	k1gMnPc1
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
navzájem	navzájem	k6eAd1
kontaktovat	kontaktovat	k5eAaImF
prostřednictvím	prostřednictvím	k7c2
nejvýše	nejvýše	k6eAd1,k6eAd3
dvou	dva	k4xCgInPc2
zprostředkovatelů	zprostředkovatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
sociálně	sociálně	k6eAd1
strukturované	strukturovaný	k2eAgFnSc2d1
populace	populace	k1gFnSc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
méně	málo	k6eAd2
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
možné	možný	k2eAgNnSc4d1
<g/>
“	“	k?
Následně	následně	k6eAd1
vytvořili	vytvořit	k5eAaPmAgMnP
simulace	simulace	k1gFnPc4
Monte	Mont	k1gInSc5
Carlo	Carlo	k1gNnSc4
založené	založený	k2eAgNnSc4d1
na	na	k7c6
Gurevichových	Gurevichův	k2eAgNnPc6d1
datech	datum	k1gNnPc6
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
uznaly	uznat	k5eAaPmAgInP
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
modelování	modelování	k1gNnSc4
sociální	sociální	k2eAgFnSc2d1
struktury	struktura	k1gFnSc2
jsou	být	k5eAaImIp3nP
zapotřebí	zapotřebí	k6eAd1
slabé	slabý	k2eAgFnPc1d1
i	i	k8xC
silné	silný	k2eAgFnPc1d1
vazby	vazba	k1gFnPc1
na	na	k7c4
známé	známý	k2eAgMnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Simulace	simulace	k1gFnSc1
prováděné	prováděný	k2eAgNnSc1d1
na	na	k7c6
relativně	relativně	k6eAd1
omezených	omezený	k2eAgInPc6d1
počítačích	počítač	k1gInPc6
z	z	k7c2
roku	rok	k1gInSc2
1973	#num#	k4
však	však	k9
dokázaly	dokázat	k5eAaPmAgInP
předpovědět	předpovědět	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
celé	celý	k2eAgFnSc6d1
populaci	populace	k1gFnSc6
USA	USA	kA
existují	existovat	k5eAaImIp3nP
realističtější	realistický	k2eAgInPc1d2
tři	tři	k4xCgInPc1
stupně	stupeň	k1gInPc1
oddělení	oddělení	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
předznamenává	předznamenávat	k5eAaImIp3nS
zjištění	zjištění	k1gNnSc4
amerického	americký	k2eAgMnSc2d1
psychologa	psycholog	k1gMnSc2
Stanleyho	Stanley	k1gMnSc2
Milgrama	Milgram	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Milgram	Milgram	k1gInSc1
pokračoval	pokračovat	k5eAaImAgInS
v	v	k7c6
Gurevichových	Gurevichův	k2eAgInPc6d1
experimentech	experiment	k1gInPc6
skrze	skrze	k?
známosti	známost	k1gFnPc1
na	na	k7c6
Univerzitě	univerzita	k1gFnSc6
Harvard	Harvard	k1gInSc1
v	v	k7c4
Cambridge	Cambridge	k1gFnPc4
ve	v	k7c6
státě	stát	k1gInSc6
Massachusetts	Massachusetts	k1gNnSc1
<g/>
,	,	kIx,
U.	U.	kA
S.	S.	kA
Kochen	Kochen	k1gInSc1
a	a	k8xC
de	de	k?
Sola	Solum	k1gNnSc2
Pool	Poola	k1gFnPc2
pokračovali	pokračovat	k5eAaImAgMnP
v	v	k7c6
rukopisu	rukopis	k1gInSc6
Kontakty	kontakt	k1gInPc1
a	a	k8xC
vlivy	vliv	k1gInPc1
(	(	kIx(
<g/>
Contacts	Contacts	k1gInSc1
and	and	k?
Influences	Influences	k1gInSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
oba	dva	k4xCgMnPc1
pracovali	pracovat	k5eAaImAgMnP
na	na	k7c6
pařížské	pařížský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátkem	počátkem	k7c2
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
je	být	k5eAaImIp3nS
Milgram	Milgram	k1gInSc4
navštívil	navštívit	k5eAaPmAgMnS
a	a	k8xC
spolupracoval	spolupracovat	k5eAaImAgMnS
s	s	k7c7
nimi	on	k3xPp3gMnPc7
na	na	k7c6
jejich	jejich	k3xOp3gInSc6
výzkumu	výzkum	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc4
nepublikovaný	publikovaný	k2eNgInSc1d1
rukopis	rukopis	k1gInSc1
obíhal	obíhat	k5eAaImAgInS
mezi	mezi	k7c7
akademiky	akademik	k1gMnPc7
více	hodně	k6eAd2
než	než	k8xS
20	#num#	k4
let	léto	k1gNnPc2
před	před	k7c7
vydáním	vydání	k1gNnSc7
v	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Formuloval	formulovat	k5eAaImAgMnS
mechaniku	mechanika	k1gFnSc4
sociálních	sociální	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
a	a	k8xC
prozkoumal	prozkoumat	k5eAaPmAgMnS
jejich	jejich	k3xOp3gInPc4
matematické	matematický	k2eAgInPc4d1
důsledky	důsledek	k1gInPc4
(	(	kIx(
<g/>
včetně	včetně	k7c2
míry	míra	k1gFnSc2
propojenosti	propojenost	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rukopis	rukopis	k1gInSc1
ponechal	ponechat	k5eAaPmAgInS
mnoho	mnoho	k4c4
významných	významný	k2eAgFnPc2d1
otázek	otázka	k1gFnPc2
týkajících	týkající	k2eAgFnPc2d1
se	s	k7c7
sítí	síť	k1gFnSc7
nevyřešených	vyřešený	k2eNgInPc2d1
a	a	k8xC
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nich	on	k3xPp3gFnPc2
byl	být	k5eAaImAgInS
počet	počet	k1gInSc1
stupňů	stupeň	k1gInPc2
oddělení	oddělení	k1gNnSc2
ve	v	k7c6
skutečných	skutečný	k2eAgFnPc6d1
sociálních	sociální	k2eAgFnPc6d1
sítích	síť	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rukopis	rukopis	k1gInSc1
ponechal	ponechat	k5eAaPmAgInS
mnoho	mnoho	k4c4
významných	významný	k2eAgFnPc2d1
otázek	otázka	k1gFnPc2
týkajících	týkající	k2eAgFnPc2d1
se	s	k7c7
sítí	síť	k1gFnSc7
nevyřešených	vyřešený	k2eNgInPc2d1
a	a	k8xC
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nich	on	k3xPp3gFnPc2
byl	být	k5eAaImAgInS
počet	počet	k1gInSc1
stupňů	stupeň	k1gInPc2
oddělení	oddělení	k1gNnSc2
ve	v	k7c6
skutečných	skutečný	k2eAgFnPc6d1
sociálních	sociální	k2eAgFnPc6d1
sítích	síť	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Významným	významný	k2eAgInSc7d1
milníkem	milník	k1gInSc7
je	být	k5eAaImIp3nS
rok	rok	k1gInSc4
1967	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Milgram	Milgram	k1gInSc1
provedl	provést	k5eAaPmAgInS
důležitý	důležitý	k2eAgInSc1d1
experiment	experiment	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náhodně	náhodně	k6eAd1
vybraným	vybraný	k2eAgMnPc3d1
adresátům	adresát	k1gMnPc3
odeslal	odeslat	k5eAaPmAgInS
z	z	k7c2
amerického	americký	k2eAgInSc2d1
středozápadu	středozápad	k1gInSc2
vždy	vždy	k6eAd1
jeden	jeden	k4xCgInSc1
balíček	balíček	k1gInSc1
se	s	k7c7
vzkazem	vzkaz	k1gInSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jej	on	k3xPp3gInSc4
co	co	k9
nejrychleji	rychle	k6eAd3
doručili	doručit	k5eAaPmAgMnP
adresátovi	adresátův	k2eAgMnPc1d1
do	do	k7c2
Bostonu	Boston	k1gInSc2
přes	přes	k7c4
své	svůj	k3xOyFgMnPc4
známé	známý	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Balíček	balíček	k1gMnSc1
však	však	k9
směl	smět	k5eAaImAgMnS
putovat	putovat	k5eAaImF
pouze	pouze	k6eAd1
přes	přes	k7c4
osoby	osoba	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
vzájemně	vzájemně	k6eAd1
osobně	osobně	k6eAd1
znaly	znát	k5eAaImAgFnP
a	a	k8xC
zároveň	zároveň	k6eAd1
o	o	k7c6
nichž	jenž	k3xRgFnPc6
se	se	k3xPyFc4
odesílající	odesílající	k2eAgMnSc1d1
domníval	domnívat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
znají	znát	k5eAaImIp3nP
adresáta	adresát	k1gMnSc4
v	v	k7c6
Bostonu	Boston	k1gInSc6
nebo	nebo	k8xC
někoho	někdo	k3yInSc4
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
mu	on	k3xPp3gMnSc3
balíček	balíček	k1gInSc1
doručí	doručit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vyhodnocení	vyhodnocení	k1gNnSc6
výsledků	výsledek	k1gInPc2
Milgram	Milgram	k1gInSc1
překvapeně	překvapeně	k6eAd1
zjistil	zjistit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
doručení	doručení	k1gNnSc3
balíčku	balíček	k1gInSc2
mezi	mezi	k7c7
oněmi	onen	k3xDgMnPc7
náhodně	náhodně	k6eAd1
vybranými	vybraný	k2eAgMnPc7d1
lidmi	člověk	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
vůbec	vůbec	k9
neznají	neznat	k5eAaImIp3nP,k5eNaImIp3nP
<g/>
,	,	kIx,
stačí	stačit	k5eAaBmIp3nS
průměrně	průměrně	k6eAd1
5,5	5,5	k4
mezistupňů	mezistupeň	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Milgramův	Milgramův	k2eAgInSc1d1
článek	článek	k1gInSc1
vydaný	vydaný	k2eAgInSc1d1
v	v	k7c6
populárně-vědeckém	populárně-vědecký	k2eAgInSc6d1
časopise	časopis	k1gInSc6
Psychology	psycholog	k1gMnPc7
Today	Todaa	k1gFnSc2
proslavil	proslavit	k5eAaPmAgMnS
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
soubor	soubor	k1gInSc4
experimentů	experiment	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
1967	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
zkoumal	zkoumat	k5eAaImAgMnS
de	de	k?
Sola	Sola	k1gMnSc1
Poolův	Poolův	k2eAgMnSc1d1
a	a	k8xC
Kochenův	Kochenův	k2eAgMnSc1d1
„	„	k?
<g/>
Problém	problém	k1gInSc1
malého	malý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Matematik	matematik	k1gMnSc1
Benoit	Benoit	k1gMnSc1
Mandelbrot	Mandelbrot	k1gMnSc1
<g/>
,	,	kIx,
narozený	narozený	k2eAgMnSc1d1
ve	v	k7c6
Varšavě	Varšava	k1gFnSc6
<g/>
,	,	kIx,
vyrůstal	vyrůstat	k5eAaImAgMnS
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
věděl	vědět	k5eAaImAgMnS
o	o	k7c6
statistickém	statistický	k2eAgNnSc6d1
pravidle	pravidlo	k1gNnSc6
a	a	k8xC
byl	být	k5eAaImAgMnS
také	také	k9
kolegou	kolega	k1gMnSc7
de	de	k?
Sola	Sola	k1gMnSc1
Pool	Pool	k1gMnSc1
<g/>
,	,	kIx,
Kochena	Kochena	k1gFnSc1
a	a	k8xC
Milgrama	Milgrama	k1gFnSc1
na	na	k7c6
pařížské	pařížský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
na	na	k7c6
počátku	počátek	k1gInSc6
padesátých	padesátý	k4xOgNnPc2
let	léto	k1gNnPc2
(	(	kIx(
<g/>
Kochen	Kochen	k1gInSc1
přivedl	přivést	k5eAaPmAgMnS
Mandelbrota	Mandelbrot	k1gMnSc4
k	k	k7c3
práci	práce	k1gFnSc3
na	na	k7c6
Institute	institut	k1gInSc5
for	forum	k1gNnPc2
Advanced	Advanced	k1gInSc4
Study	stud	k1gInPc4
a	a	k8xC
později	pozdě	k6eAd2
IBM	IBM	kA
v	v	k7c6
USA	USA	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc4
okruh	okruh	k1gInSc4
vědců	vědec	k1gMnPc2
byl	být	k5eAaImAgMnS
fascinován	fascinovat	k5eAaBmNgMnS
vzájemnou	vzájemný	k2eAgFnSc7d1
propojeností	propojenost	k1gFnSc7
a	a	k8xC
„	„	k?
<g/>
sociálním	sociální	k2eAgInSc7d1
kapitálem	kapitál	k1gInSc7
<g/>
“	“	k?
lidských	lidský	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledky	výsledek	k1gInPc7
Milgramovy	Milgramův	k2eAgFnSc2d1
studie	studie	k1gFnSc2
ukázaly	ukázat	k5eAaPmAgFnP
<g/>
,	,	kIx,
že	že	k8xS
lidem	člověk	k1gMnPc3
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
se	se	k3xPyFc4
zdálo	zdát	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
v	v	k7c6
průměru	průměr	k1gInSc6
spojeni	spojit	k5eAaPmNgMnP
přibližně	přibližně	k6eAd1
třemi	tři	k4xCgFnPc7
přátelskými	přátelský	k2eAgFnPc7d1
vazbami	vazba	k1gFnPc7
<g/>
,	,	kIx,
aniž	aniž	k8xC,k8xS
by	by	kYmCp3nP
spekulovali	spekulovat	k5eAaImAgMnP
o	o	k7c6
globálních	globální	k2eAgFnPc6d1
vazbách	vazba	k1gFnPc6
<g/>
;	;	kIx,
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
nikdy	nikdy	k6eAd1
nepoužil	použít	k5eNaPmAgInS
výraz	výraz	k1gInSc1
„	„	k?
<g/>
šest	šest	k4xCc4
stupňů	stupeň	k1gInPc2
odloučení	odloučení	k1gNnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
článek	článek	k1gInSc4
Psychology	psycholog	k1gMnPc7
Today	Todaa	k1gFnSc2
poskytl	poskytnout	k5eAaPmAgMnS
experimentům	experiment	k1gInPc3
širokou	široký	k2eAgFnSc4d1
publicitu	publicita	k1gFnSc4
<g/>
,	,	kIx,
ani	ani	k8xC
jeden	jeden	k4xCgMnSc1
z	z	k7c2
trojice	trojice	k1gFnSc2
Milgram	Milgram	k1gInSc1
<g/>
,	,	kIx,
Kochen	Kochen	k1gInSc1
a	a	k8xC
Karinthy	Karinth	k1gMnPc7
nebyl	být	k5eNaImAgMnS
původcem	původce	k1gMnSc7
pojmu	pojem	k1gInSc2
šest	šest	k4xCc4
stupňů	stupeň	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největším	veliký	k2eAgMnSc7d3
popularizátorem	popularizátor	k1gMnSc7
výrazu	výraz	k1gInSc2
„	„	k?
<g/>
šest	šest	k4xCc4
stupňů	stupeň	k1gInPc2
odloučení	odloučení	k1gNnSc2
<g/>
“	“	k?
byl	být	k5eAaImAgMnS
John	John	k1gMnSc1
Guare	Guar	k1gInSc5
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
jako	jako	k9
autora	autor	k1gMnSc4
pojmu	pojmout	k5eAaPmIp1nS
označil	označit	k5eAaPmAgMnS
Marconiho	Marconi	k1gMnSc4
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pokračující	pokračující	k2eAgInSc1d1
výzkum	výzkum	k1gInSc1
<g/>
:	:	kIx,
Projekt	projekt	k1gInSc1
malého	malý	k2eAgInSc2d1
světa	svět	k1gInSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
provedla	provést	k5eAaPmAgFnS
Columbia	Columbia	k1gFnSc1
University	universita	k1gFnSc2
analogický	analogický	k2eAgInSc1d1
experiment	experiment	k1gInSc1
o	o	k7c6
sociální	sociální	k2eAgFnSc6d1
propojenosti	propojenost	k1gFnSc6
uživatelů	uživatel	k1gMnPc2
internetových	internetový	k2eAgInPc2d1
e-mailů	e-mail	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
pokus	pokus	k1gInSc1
byl	být	k5eAaImAgInS
nazván	nazván	k2eAgInSc1d1
Projekt	projekt	k1gInSc1
malého	malý	k2eAgInSc2d1
světa	svět	k1gInSc2
(	(	kIx(
<g/>
Columbia	Columbia	k1gFnSc1
Small	Small	k1gMnSc1
World	World	k1gMnSc1
Project	Project	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
zahrnovalo	zahrnovat	k5eAaImAgNnS
24	#num#	k4
163	#num#	k4
e-mailových	e-mailův	k2eAgInPc2d1
řetězců	řetězec	k1gInPc2
zaměřených	zaměřený	k2eAgInPc2d1
na	na	k7c6
18	#num#	k4
cílů	cíl	k1gInPc2
ze	z	k7c2
13	#num#	k4
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Zaregistrovalo	zaregistrovat	k5eAaPmAgNnS
se	se	k3xPyFc4
téměř	téměř	k6eAd1
100	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
konečného	konečný	k2eAgInSc2d1
cíle	cíl	k1gInSc2
dosáhlo	dosáhnout	k5eAaPmAgNnS
pouze	pouze	k6eAd1
384	#num#	k4
(	(	kIx(
<g/>
0,4	0,4	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc1
řetězce	řetězec	k1gInPc1
dosáhly	dosáhnout	k5eAaPmAgInP
svého	svůj	k3xOyFgInSc2
cíle	cíl	k1gInSc2
po	po	k7c4
pouhých	pouhý	k2eAgInPc2d1
7	#num#	k4
<g/>
,	,	kIx,
8	#num#	k4
<g/>
,	,	kIx,
9	#num#	k4
nebo	nebo	k8xC
10	#num#	k4
krocích	krok	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodds	Dodds	k1gInSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
poznamenali	poznamenat	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
účastníci	účastník	k1gMnPc1
(	(	kIx(
<g/>
všichni	všechen	k3xTgMnPc1
dobrovolníci	dobrovolník	k1gMnPc1
<g/>
)	)	kIx)
byli	být	k5eAaImAgMnP
silně	silně	k6eAd1
zaujatí	zaujatý	k2eAgMnPc1d1
vůči	vůči	k7c3
stávajícím	stávající	k2eAgMnPc3d1
modelům	model	k1gInPc3
uživatelů	uživatel	k1gMnPc2
internetu	internet	k1gInSc2
a	a	k8xC
že	že	k8xS
propojenost	propojenost	k1gFnSc1
založená	založený	k2eAgFnSc1d1
na	na	k7c6
profesionálních	profesionální	k2eAgFnPc6d1
vazbách	vazba	k1gFnPc6
byla	být	k5eAaImAgFnS
mnohem	mnohem	k6eAd1
silnější	silný	k2eAgFnSc1d2
než	než	k8xS
v	v	k7c6
rámci	rámec	k1gInSc6
rodin	rodina	k1gFnPc2
nebo	nebo	k8xC
přátelství	přátelství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Autoři	autor	k1gMnPc1
uvádějí	uvádět	k5eAaImIp3nP
„	„	k?
<g/>
nezájem	nezájem	k1gInSc4
<g/>
“	“	k?
jako	jako	k8xC,k8xS
převládající	převládající	k2eAgInSc1d1
faktor	faktor	k1gInSc1
ve	v	k7c6
vysoké	vysoký	k2eAgFnSc6d1
míře	míra	k1gFnSc6
úbytku	úbytek	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
nález	nález	k1gInSc1
konzistentní	konzistentní	k2eAgInSc1d1
s	s	k7c7
předchozími	předchozí	k2eAgFnPc7d1
studiemi	studie	k1gFnPc7
<g/>
.	.	kIx.
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výzkum	výzkum	k1gInSc1
</s>
<s>
Několik	několik	k4yIc1
studií	studio	k1gNnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
experiment	experiment	k1gInSc1
Stanleyho	Stanley	k1gMnSc2
Milgrama	Milgram	k1gMnSc2
"	"	kIx"
<g/>
Malý	malý	k2eAgInSc1d1
svět	svět	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
provedeno	provést	k5eAaPmNgNnS
za	za	k7c7
účelem	účel	k1gInSc7
empirického	empirický	k2eAgNnSc2d1
změření	změření	k1gNnSc2
této	tento	k3xDgFnSc2
propojenosti	propojenost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojení	spojení	k1gNnSc1
slov	slovo	k1gNnPc2
„	„	k?
<g/>
šest	šest	k4xCc4
stupňů	stupeň	k1gInPc2
odloučení	odloučení	k1gNnSc2
<g/>
“	“	k?
je	být	k5eAaImIp3nS
často	často	k6eAd1
používáno	používán	k2eAgNnSc1d1
jako	jako	k8xC,k8xS
synonymum	synonymum	k1gNnSc1
pro	pro	k7c4
fenomén	fenomén	k1gInSc4
„	„	k?
<g/>
malého	malý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kritici	kritik	k1gMnPc1
argumentují	argumentovat	k5eAaImIp3nP
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
Milgramův	Milgramův	k2eAgInSc1d1
experiment	experiment	k1gInSc1
neprokazuje	prokazovat	k5eNaImIp3nS
takovéto	takovýto	k3xDgNnSc4
propojení	propojení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
teorii	teorie	k1gFnSc4
odsoudili	odsoudit	k5eAaPmAgMnP
jako	jako	k8xS,k8xC
akademický	akademický	k2eAgInSc1d1
městský	městský	k2eAgInSc1d1
mýtus	mýtus	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Taktéž	Taktéž	k?
existence	existence	k1gFnSc2
izolovaných	izolovaný	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
brazilských	brazilský	k2eAgMnPc2d1
kmenů	kmen	k1gInPc2
a	a	k8xC
jiných	jiný	k2eAgInPc2d1
domorodých	domorodý	k2eAgInPc2d1
národů	národ	k1gInPc2
<g/>
,	,	kIx,
odporuje	odporovat	k5eAaImIp3nS
striktnímu	striktní	k2eAgInSc3d1
výkladu	výklad	k1gInSc3
této	tento	k3xDgFnSc2
teorie	teorie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Počítačové	počítačový	k2eAgFnPc1d1
sítě	síť	k1gFnPc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
se	se	k3xPyFc4
profesor	profesor	k1gMnSc1
Duncan	Duncana	k1gFnPc2
Watts	Watts	k1gInSc4
z	z	k7c2
Kolumbijské	kolumbijský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
pokusil	pokusit	k5eAaPmAgMnS
znovu	znovu	k6eAd1
provést	provést	k5eAaPmF
Milgramův	Milgramův	k2eAgInSc4d1
experiment	experiment	k1gInSc4
rozesláním	rozeslání	k1gNnSc7
e-mailové	e-mailové	k2eAgFnSc2d1
zprávy	zpráva	k1gFnSc2
internetem	internet	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpráva	zpráva	k1gFnSc1
sloužila	sloužit	k5eAaImAgFnS
jako	jako	k9
"	"	kIx"
<g/>
balíček	balíček	k1gInSc1
<g/>
"	"	kIx"
adresovaný	adresovaný	k2eAgInSc1d1
48	#num#	k4
tisícům	tisíc	k4xCgInPc3
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
doručen	doručen	k2eAgInSc4d1
19	#num#	k4
lidem	lid	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Watts	Watts	k1gInSc1
zjistil	zjistit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
průměrný	průměrný	k2eAgInSc1d1
počet	počet	k1gInSc1
zprostředkovatelů	zprostředkovatel	k1gMnPc2
byl	být	k5eAaImAgInS
kolem	kolem	k6eAd1
šesti	šest	k4xCc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
zkoumali	zkoumat	k5eAaImAgMnP
dva	dva	k4xCgMnPc4
počítačoví	počítačový	k2eAgMnPc1d1
vědci	vědec	k1gMnPc1
–	–	k?
Jure	Jur	k1gInSc2
Leskovec	Leskovec	k1gInSc1
a	a	k8xC
Eric	Eric	k1gInSc1
Horvitz	Horvitza	k1gFnPc2
data	datum	k1gNnSc2
z	z	k7c2
30	#num#	k4
miliard	miliarda	k4xCgFnPc2
konverzací	konverzace	k1gFnPc2
mezi	mezi	k7c4
240	#num#	k4
miliony	milion	k4xCgInPc1
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zjistili	zjistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
průměrná	průměrný	k2eAgFnSc1d1
vzdálenost	vzdálenost	k1gFnSc1
mezi	mezi	k7c7
uživateli	uživatel	k1gMnPc7
Microsoft	Microsoft	kA
Messengeru	Messengero	k1gNnSc6
je	být	k5eAaImIp3nS
6	#num#	k4
sociálních	sociální	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Optimální	optimální	k2eAgInSc1d1
algoritmus	algoritmus	k1gInSc1
k	k	k7c3
vypočítání	vypočítání	k1gNnSc3
stupňů	stupeň	k1gInPc2
odloučení	odloučení	k1gNnSc2
na	na	k7c6
sociálních	sociální	k2eAgFnPc6d1
sítích	síť	k1gFnPc6
</s>
<s>
Bakhshandeh	Bakhshandeh	k1gInSc1
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
uvádějí	uvádět	k5eAaImIp3nP
výzkum	výzkum	k1gInSc4
zjišťující	zjišťující	k2eAgInSc4d1
<g/>
,	,	kIx,
jaký	jaký	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
je	být	k5eAaImIp3nS
počet	počet	k1gInSc1
sociálních	sociální	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
mezi	mezi	k7c7
uživateli	uživatel	k1gMnPc7
sociální	sociální	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
Twitter	Twittra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Optimální	optimální	k2eAgInSc1d1
algoritmus	algoritmus	k1gInSc1
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
průměrná	průměrný	k2eAgFnSc1d1
vzdálenost	vzdálenost	k1gFnSc1
mezi	mezi	k7c7
dvěma	dva	k4xCgMnPc7
náhodnými	náhodný	k2eAgMnPc7d1
uživateli	uživatel	k1gMnPc7
Twitteru	Twitter	k1gInSc2
je	být	k5eAaImIp3nS
3,43	3,43	k4
sociálních	sociální	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
překvapivě	překvapivě	k6eAd1
nízké	nízký	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvádějí	uvádět	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
době	doba	k1gFnSc6
internetu	internet	k1gInSc2
a	a	k8xC
sociálních	sociální	k2eAgFnPc2d1
sítí	síť	k1gFnPc2
se	se	k3xPyFc4
svět	svět	k1gInSc1
zmenšuje	zmenšovat	k5eAaImIp3nS
každým	každý	k3xTgInSc7
dnem	den	k1gInSc7
a	a	k8xC
lidé	člověk	k1gMnPc1
jsou	být	k5eAaImIp3nP
více	hodně	k6eAd2
a	a	k8xC
více	hodně	k6eAd2
propojeni	propojit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s>
Tři	tři	k4xCgInPc1
a	a	k8xC
půl	půl	k1xP
stupně	stupeň	k1gInSc2
odloučení	odloučení	k1gNnSc2
</s>
<s>
Výzkum	výzkum	k1gInSc1
probíhal	probíhat	k5eAaImAgInS
i	i	k9
v	v	k7c6
rámci	rámec	k1gInSc6
sociální	sociální	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
Facebook	Facebook	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
době	doba	k1gFnSc6
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
přes	přes	k7c4
2,7	2,7	k4
miliardy	miliarda	k4xCgFnSc2
aktivních	aktivní	k2eAgMnPc2d1
uživatelů	uživatel	k1gMnPc2
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výzkum	výzkum	k1gInSc1
probíhal	probíhat	k5eAaImAgInS
v	v	k7c6
letech	léto	k1gNnPc6
2011	#num#	k4
a	a	k8xC
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
čítal	čítat	k5eAaImAgInS
průměrný	průměrný	k2eAgInSc1d1
počet	počet	k1gInSc1
prostředníků	prostředník	k1gInPc2
3,74	3,74	k4
(	(	kIx(
<g/>
=	=	kIx~
<g/>
4,74	4,74	k4
sociální	sociální	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
byl	být	k5eAaImAgInS
počet	počet	k1gInSc1
aktivních	aktivní	k2eAgMnPc2d1
uživatelů	uživatel	k1gMnPc2
Facebooku	Facebook	k1gInSc2
721	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
se	se	k3xPyFc4
počet	počet	k1gInSc1
uživatelů	uživatel	k1gMnPc2
více	hodně	k6eAd2
než	než	k8xS
zdvojnásobil	zdvojnásobit	k5eAaPmAgInS
(	(	kIx(
<g/>
1,59	1,59	k4
miliard	miliarda	k4xCgFnPc2
uživ	uživit	k5eAaPmRp2nS
<g/>
.	.	kIx.
<g/>
)	)	kIx)
a	a	k8xC
průměrný	průměrný	k2eAgInSc1d1
počet	počet	k1gInSc1
prostředníků	prostředník	k1gMnPc2
mezi	mezi	k7c7
uživateli	uživatel	k1gMnPc7
klesl	klesnout	k5eAaPmAgMnS
na	na	k7c4
3,57	3,57	k4
(	(	kIx(
<g/>
=	=	kIx~
<g/>
4,57	4,57	k4
sociální	sociální	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
USA	USA	kA
je	být	k5eAaImIp3nS
toto	tento	k3xDgNnSc1
číslo	číslo	k1gNnSc1
ještě	ještě	k6eAd1
nižší	nízký	k2eAgMnSc1d2
a	a	k8xC
čítá	čítat	k5eAaImIp3nS
průměrně	průměrně	k6eAd1
3,46	3,46	k4
prostředníků	prostředník	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
výzkum	výzkum	k1gInSc1
poukazuje	poukazovat	k5eAaImIp3nS
na	na	k7c4
pozitivní	pozitivní	k2eAgFnSc4d1
korelaci	korelace	k1gFnSc4
mezi	mezi	k7c7
počtem	počet	k1gInSc7
uživatelů	uživatel	k1gMnPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnSc7
vyšší	vysoký	k2eAgFnSc7d2
propojeností	propojenost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Data	datum	k1gNnPc1
byla	být	k5eAaImAgNnP
v	v	k7c6
rámci	rámec	k1gInSc6
výzkumu	výzkum	k1gInSc2
získávána	získáván	k2eAgFnSc1d1
statistickými	statistický	k2eAgFnPc7d1
metodami	metoda	k1gFnPc7
a	a	k8xC
algoritmy	algoritmus	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studie	studie	k1gFnSc1
ukazuje	ukazovat	k5eAaImIp3nS
ke	k	k7c3
stále	stále	k6eAd1
většímu	veliký	k2eAgInSc3d2
vývoji	vývoj	k1gInSc3
fenoménu	fenomén	k1gInSc2
„	„	k?
<g/>
malého	malý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
“	“	k?
a	a	k8xC
mnohem	mnohem	k6eAd1
větší	veliký	k2eAgFnSc7d2
propojeností	propojenost	k1gFnSc7
mezi	mezi	k7c7
lidmi	člověk	k1gMnPc7
<g/>
,	,	kIx,
než	než	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
dalo	dát	k5eAaPmAgNnS
očekávat	očekávat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
<g/>
Šest	šest	k4xCc4
stupňů	stupeň	k1gInPc2
kopulace	kopulace	k1gFnSc2
<g/>
“	“	k?
</s>
<s>
Podobný	podobný	k2eAgInSc1d1
princip	princip	k1gInSc1
propojenosti	propojenost	k1gFnSc2
mezi	mezi	k7c7
lidmi	člověk	k1gMnPc7
se	se	k3xPyFc4
aplikoval	aplikovat	k5eAaBmAgInS
i	i	k9
ve	v	k7c6
Švédsku	Švédsko	k1gNnSc6
při	při	k7c6
výzkumu	výzkum	k1gInSc6
promiskuity	promiskuita	k1gFnSc2
ve	v	k7c6
společnosti	společnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studie	studie	k1gFnSc1
byla	být	k5eAaImAgFnS
zaměřena	zaměřit	k5eAaPmNgFnS
na	na	k7c4
cesty	cesta	k1gFnPc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
mohou	moct	k5eAaImIp3nP
mezi	mezi	k7c7
lidmi	člověk	k1gMnPc7
v	v	k7c6
jedné	jeden	k4xCgFnSc6
komunitě	komunita	k1gFnSc6
šířit	šířit	k5eAaImF
pohlavní	pohlavní	k2eAgFnPc4d1
choroby	choroba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studie	studie	k1gFnSc1
se	se	k3xPyFc4
zúčastnilo	zúčastnit	k5eAaPmAgNnS
2810	#num#	k4
respondentů	respondent	k1gMnPc2
(	(	kIx(
<g/>
18	#num#	k4
<g/>
-	-	kIx~
<g/>
74	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
byl	být	k5eAaImAgInS
nejméně	málo	k6eAd3
zastoupen	zastoupen	k2eAgInSc1d1
vzorek	vzorek	k1gInSc1
starších	starý	k2eAgFnPc2d2
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
této	tento	k3xDgFnSc2
skutečnosti	skutečnost	k1gFnSc2
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
reprezentativní	reprezentativní	k2eAgInSc4d1
vzorek	vzorek	k1gInSc4
napříč	napříč	k7c7
demografickým	demografický	k2eAgNnSc7d1
spektrem	spektrum	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studie	studie	k1gFnSc1
zkoumala	zkoumat	k5eAaImAgFnS
<g/>
,	,	kIx,
kolik	kolik	k9
měl	mít	k5eAaImAgMnS
respondent	respondent	k1gMnSc1
partnerů	partner	k1gMnPc2
v	v	k7c6
průběhu	průběh	k1gInSc6
jednoho	jeden	k4xCgInSc2
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výzkum	výzkum	k1gInSc1
poukazuje	poukazovat	k5eAaImIp3nS
na	na	k7c4
mnohem	mnohem	k6eAd1
větší	veliký	k2eAgFnSc4d2
propojenost	propojenost	k1gFnSc4
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
kolem	kolem	k7c2
2-3	2-3	k4
sociálních	sociální	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledky	výsledek	k1gInPc1
byly	být	k5eAaImAgInP
silně	silně	k6eAd1
ovlivněny	ovlivnit	k5eAaPmNgInP
jednotlivci	jednotlivec	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
měli	mít	k5eAaImAgMnP
velký	velký	k2eAgInSc4d1
počet	počet	k1gInSc4
partnerů	partner	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studie	studie	k1gFnSc1
poskytla	poskytnout	k5eAaPmAgFnS
data	datum	k1gNnPc4
pro	pro	k7c4
epidemiology	epidemiolog	k1gMnPc4
a	a	k8xC
lékaře	lékař	k1gMnPc4
k	k	k7c3
dalšímu	další	k2eAgNnSc3d1
zkoumání	zkoumání	k1gNnSc3
a	a	k8xC
prevenci	prevence	k1gFnSc3
šíření	šíření	k1gNnSc2
pohlavně	pohlavně	k6eAd1
přenosných	přenosný	k2eAgFnPc2d1
chorob	choroba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popularizace	popularizace	k1gFnSc1
</s>
<s>
Šest	šest	k4xCc1
stupňů	stupeň	k1gInPc2
odloučení	odloučení	k1gNnPc2
se	se	k3xPyFc4
už	už	k6eAd1
nelimituje	limitovat	k5eNaBmIp3nS
pouze	pouze	k6eAd1
na	na	k7c4
akademické	akademický	k2eAgNnSc4d1
a	a	k8xC
filozofické	filozofický	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
,	,	kIx,
stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
vlivným	vlivný	k2eAgNnSc7d1
tématem	téma	k1gNnSc7
v	v	k7c6
současné	současný	k2eAgFnSc6d1
popkultuře	popkultuře	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgInPc4d1
technologické	technologický	k2eAgInPc4d1
objevy	objev	k1gInPc4
<g/>
,	,	kIx,
především	především	k6eAd1
internet	internet	k1gInSc4
inspirovali	inspirovat	k5eAaBmAgMnP
lidstvo	lidstvo	k1gNnSc4
věnovat	věnovat	k5eAaPmF,k5eAaImF
větší	veliký	k2eAgFnSc4d2
pozornost	pozornost	k1gFnSc4
sociálním	sociální	k2eAgFnPc3d1
sítím	síť	k1gFnPc3
a	a	k8xC
lidským	lidský	k2eAgMnPc3d1
vzájemným	vzájemný	k2eAgMnPc3d1
kontaktům	kontakt	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téma	téma	k1gNnSc1
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
ve	v	k7c6
většině	většina	k1gFnSc6
populárních	populární	k2eAgNnPc2d1
médií	médium	k1gNnPc2
včetně	včetně	k7c2
kinematografie	kinematografie	k1gFnSc2
<g/>
,	,	kIx,
hudbě	hudba	k1gFnSc3
<g/>
,	,	kIx,
filmu	film	k1gInSc3
a	a	k8xC
internetu	internet	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Drama	drama	k1gNnSc1
a	a	k8xC
společenská	společenský	k2eAgFnSc1d1
hra	hra	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
americký	americký	k2eAgMnSc1d1
dramatik	dramatik	k1gMnSc1
John	John	k1gMnSc1
Guare	Guar	k1gInSc5
divadelní	divadelní	k2eAgFnSc7d1
hru	hra	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
zpopularizoval	zpopularizovat	k5eAaPmAgInS
film	film	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Hra	hra	k1gFnSc1
byla	být	k5eAaImAgFnS
nominována	nominovat	k5eAaBmNgFnS
na	na	k7c4
Pulitzerovu	Pulitzerův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
za	za	k7c4
drama	drama	k1gNnSc4
a	a	k8xC
na	na	k7c4
Tonyho	Tony	k1gMnSc4
cenu	cena	k1gFnSc4
za	za	k7c4
nejlepší	dobrý	k2eAgFnSc4d3
hru	hra	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Guare	Guar	k1gInSc5
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c4
propojenost	propojenost	k1gFnSc4
americké	americký	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
prostřednictvím	prostřednictvím	k7c2
lásky	láska	k1gFnSc2
k	k	k7c3
hmotným	hmotný	k2eAgInPc3d1
statkům	statek	k1gInPc3
<g/>
,	,	kIx,
krásy	krása	k1gFnSc2
a	a	k8xC
slávy	sláva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podporuje	podporovat	k5eAaImIp3nS
předpoklad	předpoklad	k1gInSc1
propojení	propojení	k1gNnSc2
světa	svět	k1gInSc2
právě	právě	k6eAd1
šesti	šest	k4xCc2
stupni	stupeň	k1gInPc7
odloučení	odloučení	k1gNnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Jak	jak	k6eAd1
uvádí	uvádět	k5eAaImIp3nS
hrdinka	hrdinka	k1gFnSc1
hry	hra	k1gFnSc2
Quisa	Quisa	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Někde	někde	k6eAd1
jsem	být	k5eAaImIp1nS
četla	číst	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
každého	každý	k3xTgMnSc4
na	na	k7c6
této	tento	k3xDgFnSc6
planetě	planeta	k1gFnSc6
odděluje	oddělovat	k5eAaImIp3nS
šest	šest	k4xCc1
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šest	šest	k4xCc1
stupňů	stupeň	k1gInPc2
odloučení	odloučení	k1gNnPc2
mezi	mezi	k7c7
námi	my	k3xPp1nPc7
a	a	k8xC
každým	každý	k3xTgMnSc7
dalším	další	k2eAgMnSc7d1
člověkem	člověk	k1gMnSc7
na	na	k7c6
této	tento	k3xDgFnSc6
planetě	planeta	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezident	prezident	k1gMnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
,	,	kIx,
gondoliér	gondoliér	k1gMnSc1
v	v	k7c6
Benátkách	Benátky	k1gFnPc6
<g/>
,	,	kIx,
jen	jen	k9
doplň	doplnit	k5eAaPmRp2nS
jména	jméno	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Považuji	považovat	k5eAaImIp1nS
za	za	k7c4
extrémně	extrémně	k6eAd1
uklidňující	uklidňující	k2eAgNnSc4d1
<g/>
,	,	kIx,
že	že	k8xS
jsme	být	k5eAaImIp1nP
tak	tak	k6eAd1
blízko	blízko	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připomíná	připomínat	k5eAaImIp3nS
mi	já	k3xPp1nSc3
to	ten	k3xDgNnSc1
čínské	čínský	k2eAgNnSc1d1
vodní	vodní	k2eAgNnSc1d1
mučení	mučení	k1gNnSc1
<g/>
,	,	kIx,
že	že	k8xS
jsme	být	k5eAaImIp1nP
si	se	k3xPyFc3
tak	tak	k6eAd1
nablízku	nablízku	k6eAd1
<g/>
,	,	kIx,
protože	protože	k8xS
musíme	muset	k5eAaImIp1nP
najít	najít	k5eAaPmF
správných	správný	k2eAgInPc2d1
šest	šest	k4xCc4
lidí	člověk	k1gMnPc2
k	k	k7c3
navázání	navázání	k1gNnSc3
správného	správný	k2eAgNnSc2d1
spojení	spojení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Já	já	k3xPp1nSc1
i	i	k9
ty	ty	k3xPp2nSc1
jsme	být	k5eAaImIp1nP
ke	k	k7c3
každému	každý	k3xTgMnSc3
na	na	k7c6
této	tento	k3xDgFnSc6
planetě	planeta	k1gFnSc6
napojeni	napojen	k2eAgMnPc1d1
stopou	stopa	k1gFnSc7
šesti	šest	k4xCc2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dále	daleko	k6eAd2
existuje	existovat	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1994	#num#	k4
koncepční	koncepční	k2eAgFnSc1d1
společenská	společenský	k2eAgFnSc1d1
hra	hra	k1gFnSc1
Šest	šest	k4xCc1
stupňů	stupeň	k1gInPc2
herce	herec	k1gMnSc2
Kevina	Kevin	k1gMnSc2
Bacona	Bacon	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hráči	hráč	k1gMnPc1
soupeří	soupeřit	k5eAaImIp3nP
o	o	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
najde	najít	k5eAaPmIp3nS
nejrychleji	rychle	k6eAd3
nejkratší	krátký	k2eAgFnSc4d3
cestu	cesta	k1gFnSc4
od	od	k7c2
náhodného	náhodný	k2eAgInSc2d1
herce	herc	k1gInSc2
ke	k	k7c3
Kevinu	Kevin	k1gMnSc3
Baconovi	Bacon	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc4
podle	podle	k7c2
filmů	film	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
hráli	hrát	k5eAaImAgMnP
herci	herec	k1gMnPc7
společně	společně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Usuzuje	usuzovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
jakýkoliv	jakýkoliv	k3yIgMnSc1
hollywoodský	hollywoodský	k2eAgMnSc1d1
herec	herec	k1gMnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
spojen	spojit	k5eAaPmNgInS
v	v	k7c6
šesti	šest	k4xCc6
krocích	krok	k1gInPc6
s	s	k7c7
Baconem	Bacon	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Hra	hra	k1gFnSc1
byla	být	k5eAaImAgFnS
vytvořena	vytvořit	k5eAaPmNgFnS
třemi	tři	k4xCgMnPc7
studenty	student	k1gMnPc7
z	z	k7c2
Albright	Albrighta	k1gFnPc2
College	Colleg	k1gInSc2
v	v	k7c6
Pensylvánii	Pensylvánie	k1gFnSc6
<g/>
,	,	kIx,
koncept	koncept	k1gInSc4
vytvořili	vytvořit	k5eAaPmAgMnP
při	při	k7c6
sledování	sledování	k1gNnSc6
Footloose	Footloosa	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Populární	populární	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
</s>
<s>
Film	film	k1gInSc1
</s>
<s>
Téma	téma	k1gNnSc4
nejvíce	hodně	k6eAd3,k6eAd1
zpopularizoval	zpopularizovat	k5eAaPmAgInS
film	film	k1gInSc1
Šest	šest	k4xCc1
stupňů	stupeň	k1gInPc2
odloučení	odloučení	k1gNnPc2
z	z	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dva	dva	k4xCgInPc1
prodejci	prodejce	k1gMnPc7
umění	umění	k1gNnSc2
najdou	najít	k5eAaPmIp3nP
pobodaného	pobodaný	k2eAgMnSc4d1
muže	muž	k1gMnSc4
v	v	k7c6
Central	Central	k1gFnSc6
Parku	park	k1gInSc2
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
a	a	k8xC
od	od	k7c2
této	tento	k3xDgFnSc2
chvíle	chvíle	k1gFnSc2
se	se	k3xPyFc4
jejich	jejich	k3xOp3gInPc1
životy	život	k1gInPc1
obrátí	obrátit	k5eAaPmIp3nP
naruby	naruby	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Režie	režie	k1gFnSc1
v	v	k7c6
podání	podání	k1gNnSc6
Australana	Australan	k1gMnSc2
Freda	Fred	k1gMnSc2
Schepisi	Schepise	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
hlavních	hlavní	k2eAgFnPc6d1
rolích	role	k1gFnPc6
Will	Will	k1gMnSc1
Smith	Smith	k1gMnSc1
<g/>
,	,	kIx,
Donald	Donald	k1gMnSc1
Sutherland	Sutherland	k1gInSc1
a	a	k8xC
Stockard	Stockard	k1gInSc1
Channing	Channing	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oskarový	Oskarový	k2eAgInSc4d1
film	film	k1gInSc4
Babel	Babel	k1gMnSc1
z	z	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
na	na	k7c6
konceptu	koncept	k1gInSc6
šesti	šest	k4xCc2
stupňů	stupeň	k1gInPc2
odloučení	odloučení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrdinové	Hrdinová	k1gFnSc6
se	se	k3xPyFc4
neznají	neznat	k5eAaImIp3nP,k5eNaImIp3nP
a	a	k8xC
žijí	žít	k5eAaImIp3nP
tisíce	tisíc	k4xCgInPc4
mil	míle	k1gFnPc2
od	od	k7c2
sebe	sebe	k3xPyFc4
<g/>
,	,	kIx,
ale	ale	k8xC
jejich	jejich	k3xOp3gInPc1
životy	život	k1gInPc1
jsou	být	k5eAaImIp3nP
stejně	stejně	k6eAd1
úzce	úzko	k6eAd1
propleteny	propleten	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
V	v	k7c6
literatuře	literatura	k1gFnSc6
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
vědomostní	vědomostní	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
Joined-Up	Joined-Up	k1gMnSc1
Thinking	Thinking	k1gInSc1
a	a	k8xC
Connectoscope	Connectoscop	k1gInSc5
od	od	k7c2
britského	britský	k2eAgMnSc2d1
spisovatele	spisovatel	k1gMnSc2
Stevyna	Stevyn	k1gMnSc2
Colgana	Colgan	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zakládají	zakládat	k5eAaImIp3nP
na	na	k7c6
šesti	šest	k4xCc6
stupních	stupeň	k1gInPc6
informací	informace	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
všechny	všechen	k3xTgFnPc1
propojeny	propojen	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Dále	daleko	k6eAd2
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
podobná	podobný	k2eAgFnSc1d1
hra	hra	k1gFnSc1
té	ten	k3xDgFnSc3
Kevina	Kevin	k2eAgFnSc1d1
Bacona	Bacona	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
rámci	rámec	k1gInSc6
literatury	literatura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinou	většinou	k6eAd1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
v	v	k7c6
originále	originál	k1gInSc6
6	#num#	k4
degrees	degrees	k1gMnSc1
of	of	k?
separation	separation	k1gInSc1
meme	memat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cílem	cíl	k1gInSc7
není	být	k5eNaImIp3nS
dojít	dojít	k5eAaPmF
k	k	k7c3
určité	určitý	k2eAgFnSc3d1
knize	kniha	k1gFnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
od	od	k7c2
vybrané	vybraný	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
vytvořit	vytvořit	k5eAaPmF
hezky	hezky	k6eAd1
pospojovaný	pospojovaný	k2eAgInSc1d1
související	související	k2eAgInSc1d1
řetěz	řetěz	k1gInSc1
6	#num#	k4
titulů	titul	k1gInPc2
podle	podle	k7c2
žánrů	žánr	k1gInPc2
<g/>
,	,	kIx,
autorů	autor	k1gMnPc2
nebo	nebo	k8xC
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hudba	hudba	k1gFnSc1
</s>
<s>
Ústřední	ústřední	k2eAgNnSc4d1
téma	téma	k1gNnSc4
šesti	šest	k4xCc2
stupňů	stupeň	k1gInPc2
odloučení	odloučení	k1gNnSc2
lze	lze	k6eAd1
sledovat	sledovat	k5eAaImF
ve	v	k7c6
skladbě	skladba	k1gFnSc6
"	"	kIx"
<g/>
Full	Full	k1gInSc1
Circle	Circle	k1gFnSc2
<g/>
"	"	kIx"
od	od	k7c2
skupiny	skupina	k1gFnSc2
No	no	k9
Doubt	Doubt	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
"	"	kIx"
<g/>
Šest	šest	k4xCc4
stupňů	stupeň	k1gInPc2
odloučení	odloučení	k1gNnSc2
<g/>
"	"	kIx"
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
česká	český	k2eAgFnSc1d1
metalová	metalový	k2eAgFnSc1d1
kapela	kapela	k1gFnSc1
z	z	k7c2
Velkého	velký	k2eAgNnSc2d1
Ořechova	Ořechův	k2eAgNnSc2d1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
aktivní	aktivní	k2eAgFnSc1d1
od	od	k7c2
roku	rok	k1gInSc2
1996	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
"	"	kIx"
<g/>
Šest	šest	k4xCc4
stupňů	stupeň	k1gInPc2
odloučení	odloučení	k1gNnSc2
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
10	#num#	k4
<g/>
.	.	kIx.
nahrávka	nahrávka	k1gFnSc1
na	na	k7c4
2	#num#	k4
<g/>
.	.	kIx.
disku	disk	k1gInSc2
z	z	k7c2
alba	album	k1gNnSc2
The	The	k1gFnSc2
Weight	Weight	k1gMnSc1
of	of	k?
These	these	k1gFnSc2
Wings	Wingsa	k1gFnPc2
z	z	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
od	od	k7c2
americké	americký	k2eAgFnSc2d1
zpěvačky	zpěvačka	k1gFnSc2
Mirandy	Miranda	k1gFnSc2
Lambert	Lambert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
"	"	kIx"
<g/>
Šest	šest	k4xCc4
stupňů	stupeň	k1gInPc2
odloučení	odloučení	k1gNnSc2
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
2	#num#	k4
<g/>
.	.	kIx.
nahrávka	nahrávka	k1gFnSc1
3	#num#	k4
<g/>
.	.	kIx.
alba	album	k1gNnSc2
#	#	kIx~
<g/>
3	#num#	k4
od	od	k7c2
irské	irský	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
The	The	k1gMnSc1
Script	Script	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
"	"	kIx"
<g/>
Šest	šest	k4xCc1
stupňů	stupeň	k1gInPc2
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
6	#num#	k4
<g/>
.	.	kIx.
nahrávka	nahrávka	k1gFnSc1
z	z	k7c2
3	#num#	k4
<g/>
.	.	kIx.
alba	album	k1gNnSc2
The	The	k1gFnSc2
Light	Light	k2eAgInSc4d1
Between	Between	k1gInSc4
Us	Us	k1gFnPc2
od	od	k7c2
britské	britský	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Scouting	Scouting	k1gInSc1
for	forum	k1gNnPc2
Girls	girl	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
"	"	kIx"
<g/>
Šest	šest	k4xCc4
stupňů	stupeň	k1gInPc2
vnitřní	vnitřní	k2eAgFnSc2d1
turbulence	turbulence	k1gFnSc2
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
album	album	k1gNnSc4
z	z	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
od	od	k7c2
progresivní	progresivní	k2eAgFnSc2d1
metalové	metalový	k2eAgFnSc2d1
kapely	kapela	k1gFnSc2
Dream	Dream	k1gInSc1
Theater	Theater	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
"	"	kIx"
<g/>
The	The	k1gMnPc2
Seventh	Seventh	k1gMnSc1
Degree	Degre	k1gFnSc2
of	of	k?
Separation	Separation	k1gInSc1
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
album	album	k1gNnSc4
z	z	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
od	od	k7c2
progresivní	progresivní	k2eAgFnSc2d1
rockové	rockový	k2eAgFnSc2d1
kapely	kapela	k1gFnSc2
Arena	Aren	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Píseň	píseň	k1gFnSc1
"	"	kIx"
<g/>
No	no	k9
Degree	Degree	k1gNnSc6
of	of	k?
Separation	Separation	k1gInSc1
<g/>
"	"	kIx"
od	od	k7c2
italského	italský	k2eAgMnSc2d1
zpěváka	zpěvák	k1gMnSc2
Francesca	Francesca	k1gFnSc1
Michielina	Michielina	k1gFnSc1
reprezentovala	reprezentovat	k5eAaImAgFnS
Itálii	Itálie	k1gFnSc4
na	na	k7c6
soutěži	soutěž	k1gFnSc6
Eurovize	Eurovize	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Webové	webový	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
a	a	k8xC
aplikace	aplikace	k1gFnPc4
</s>
<s>
Internet	Internet	k1gInSc1
</s>
<s>
Maďarský	maďarský	k2eAgMnSc1d1
fyzik	fyzik	k1gMnSc1
Albert-László	Albert-László	k1gMnSc1
Barabási	Barabáse	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
objevil	objevit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
v	v	k7c6
průměru	průměr	k1gInSc6
19	#num#	k4
bodů	bod	k1gInPc2
odloučení	odloučení	k1gNnSc2
mezi	mezi	k7c7
dvěma	dva	k4xCgFnPc7
libovolnými	libovolný	k2eAgFnPc7d1
webovými	webový	k2eAgFnPc7d1
stránkami	stránka	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Wikipedie	Wikipedie	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
publikoval	publikovat	k5eAaBmAgMnS
Jacob	Jacoba	k1gFnPc2
Wenger	Wenger	k1gMnSc1
webovou	webový	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
www.SixDegreesOfWikipedia.com	www.SixDegreesOfWikipedia.com	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
stránka	stránka	k1gFnSc1
ukáže	ukázat	k5eAaPmIp3nS
počet	počet	k1gInSc4
cest	cesta	k1gFnPc2
a	a	k8xC
stupně	stupeň	k1gInSc2
odloučení	odloučení	k1gNnSc2
od	od	k7c2
jednoho	jeden	k4xCgInSc2
zvoleného	zvolený	k2eAgInSc2d1
pojmu	pojem	k1gInSc2
z	z	k7c2
Wikipedie	Wikipedie	k1gFnSc2
k	k	k7c3
druhému	druhý	k4xOgMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
ukáže	ukázat	k5eAaPmIp3nS
grafickou	grafický	k2eAgFnSc4d1
mapu	mapa	k1gFnSc4
spojení	spojení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrný	průměrný	k2eAgInSc1d1
stupeň	stupeň	k1gInSc1
při	při	k7c6
hledání	hledání	k1gNnSc6
u	u	k7c2
většiny	většina	k1gFnSc2
uživatelů	uživatel	k1gMnPc2
je	být	k5eAaImIp3nS
3	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Facebook	Facebook	k1gInSc1
</s>
<s>
Datový	datový	k2eAgInSc1d1
tým	tým	k1gInSc1
Facebooku	Facebook	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
publikoval	publikovat	k5eAaBmAgMnS
2	#num#	k4
studie	studie	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
dokumentují	dokumentovat	k5eAaBmIp3nP
<g/>
,	,	kIx,
že	že	k8xS
všichni	všechen	k3xTgMnPc1
uživatelé	uživatel	k1gMnPc1
Facebooku	Facebook	k1gInSc2
(	(	kIx(
<g/>
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
727	#num#	k4
milionů	milion	k4xCgInPc2
uživatelů	uživatel	k1gMnPc2
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
průměrně	průměrně	k6eAd1
od	od	k7c2
sebe	sebe	k3xPyFc4
vzdáleni	vzdálen	k2eAgMnPc1d1
4.74	4.74	k4
kliknutí	kliknutí	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
Facebook	Facebook	k1gInSc1
hlásil	hlásit	k5eAaImAgInS
<g/>
,	,	kIx,
že	že	k8xS
vzdálenost	vzdálenost	k1gFnSc1
klesla	klesnout	k5eAaPmAgFnS
na	na	k7c4
4.57	4.57	k4
kliknutí	kliknutí	k1gNnPc2
při	při	k7c6
1,6	1,6	k4
miliardě	miliarda	k4xCgFnSc3
uživatelů	uživatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Six	Six	k1gMnSc1
degrees	degrees	k1gMnSc1
of	of	k?
separation	separation	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Newman	Newman	k1gMnSc1
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
<g/>
,	,	kIx,
Albert-László	Albert-László	k1gMnSc5
Barabási	Barabás	k1gMnSc5
<g/>
,	,	kIx,
and	and	k?
Duncan	Duncan	k1gInSc1
J.	J.	kA
Watts	Watts	k1gInSc1
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Structure	Structur	k1gMnSc5
and	and	k?
Dynamics	Dynamics	k1gInSc1
of	of	k?
Networks	Networks	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Princeton	Princeton	k1gInSc1
<g/>
,	,	kIx,
NJ	NJ	kA
<g/>
:	:	kIx,
Princeton	Princeton	k1gInSc4
University	universita	k1gFnSc2
Press	Press	k1gInSc4
<g/>
.1	.1	k4
2	#num#	k4
Barabási	Barabás	k1gMnPc7
<g/>
,	,	kIx,
Albert-László	Albert-László	k1gFnSc1
Archived	Archived	k1gInSc1
2005-03-04	2005-03-04	k4
at	at	k?
the	the	k?
Wayback	Wayback	k1gInSc1
Machine	Machin	k1gInSc5
<g/>
.	.	kIx.
2003	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Linked	Linked	k1gInSc1
<g/>
:	:	kIx,
How	How	k1gFnSc1
Everything	Everything	k1gInSc1
is	is	k?
Connected	Connected	k1gInSc1
to	ten	k3xDgNnSc1
Everything	Everything	k1gInSc1
Else	Elsa	k1gFnSc3
and	and	k?
What	What	k2eAgMnSc1d1
It	It	k1gMnSc1
Means	Meansa	k1gFnPc2
for	forum	k1gNnPc2
Business	business	k1gInSc1
<g/>
,	,	kIx,
Science	Science	k1gFnSc1
<g/>
,	,	kIx,
and	and	k?
Everyday	Everydaa	k1gFnSc2
↑	↑	k?
Karinthy	Karintha	k1gFnSc2
<g/>
,	,	kIx,
Frigyes	Frigyes	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1929	#num#	k4
<g/>
)	)	kIx)
"	"	kIx"
<g/>
Chain	Chain	k2eAgInSc1d1
Links	Links	k1gInSc1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
↑	↑	k?
Gurevich	Gurevich	k1gInSc1
<g/>
,	,	kIx,
M	M	kA
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1961	#num#	k4
<g/>
)	)	kIx)
The	The	k1gMnSc1
Social	Social	k1gMnSc1
Structure	Structur	k1gMnSc5
of	of	k?
Acquaintanceship	Acquaintanceship	k1gInSc1
Networks	Networks	k1gInSc1
<g/>
,	,	kIx,
Cambridge	Cambridge	k1gFnSc1
<g/>
,	,	kIx,
MA	MA	kA
<g/>
:	:	kIx,
MIT	MIT	kA
Press	Press	k1gInSc1
<g/>
↑	↑	k?
de	de	k?
Sola	Sola	k1gMnSc1
Pool	Pool	k1gMnSc1
<g/>
,	,	kIx,
Ithiel	Ithiel	k1gMnSc1
<g/>
,	,	kIx,
Kochen	Kochen	k2eAgMnSc1d1
<g/>
,	,	kIx,
Manfred	Manfred	k1gMnSc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
–	–	k?
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
Contacts	Contacts	k1gInSc1
and	and	k?
<g />
.	.	kIx.
</s>
<s hack="1">
influence	influence	k1gFnSc1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
Social	Social	k1gInSc1
Networks	Networks	k1gInSc1
1	#num#	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
42	#num#	k4
<g/>
↑	↑	k?
de	de	k?
Sola	Sola	k1gMnSc1
Pool	Pool	k1gMnSc1
<g/>
,	,	kIx,
Ithiel	Ithiel	k1gMnSc1
<g/>
,	,	kIx,
Kochen	Kochen	k2eAgMnSc1d1
<g/>
,	,	kIx,
Manfred	Manfred	k1gMnSc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
–	–	k?
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
Contacts	Contacts	k1gInSc1
and	and	k?
Influence	influence	k1gFnSc1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
Social	Social	k1gInSc1
Networks	Networks	k1gInSc1
1	#num#	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
5	#num#	k4
<g/>
–	–	k?
<g/>
51	#num#	k4
<g/>
↑	↑	k?
Milgram	Milgram	k1gInSc1
<g/>
,	,	kIx,
Stanley	Stanley	k1gInPc1
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
The	The	k1gMnSc1
Small	Small	k1gMnSc1
World	World	k1gMnSc1
Problem	Probl	k1gMnSc7
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Psychology	psycholog	k1gMnPc7
Today	Todaa	k1gFnSc2
<g/>
.	.	kIx.
2	#num#	k4
<g/>
:	:	kIx,
60	#num#	k4
<g/>
–	–	k?
<g/>
67	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
The	The	k1gMnSc1
concept	concept	k1gMnSc1
of	of	k?
Six	Six	k1gMnSc1
degrees	degrees	k1gMnSc1
of	of	k?
separation	separation	k1gInSc1
stretches	stretchesa	k1gFnPc2
back	back	k6eAd1
to	ten	k3xDgNnSc4
Italian	Italian	k1gMnSc1
inventor	inventor	k1gMnSc1
Guglielmo	Guglielma	k1gFnSc5
Marconi	Marcoň	k1gFnSc3
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retrieved	Retrieved	k1gInSc1
16	#num#	k4
July	Jula	k1gFnSc2
2012	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dodds	Dodds	k1gInSc1
<g/>
,	,	kIx,
Muhamad	Muhamad	k1gInSc1
<g/>
,	,	kIx,
Watts	Watts	k1gInSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
"	"	kIx"
<g/>
Small	Small	k1gMnSc1
World	World	k1gMnSc1
Project	Project	k1gMnSc1
<g/>
,	,	kIx,
<g/>
"	"	kIx"
Science	Science	k1gFnSc1
Magazine	Magazin	k1gInSc5
<g/>
.	.	kIx.
pp	pp	k?
<g/>
.827	.827	k4
<g/>
-	-	kIx~
<g/>
829	#num#	k4
<g/>
,	,	kIx,
8	#num#	k4
August	August	k1gMnSc1
2003	#num#	k4
https://www.sciencemag.org/content/301/5634/827	https://www.sciencemag.org/content/301/5634/827	k4
<g/>
↑	↑	k?
Judith	Judith	k1gMnSc1
S.	S.	kA
Kleinfeld	Kleinfeld	k1gMnSc1
<g/>
,	,	kIx,
University	universita	k1gFnPc1
of	of	k?
Alaska	Alask	k1gInSc2
Fairbanks	Fairbanksa	k1gFnPc2
(	(	kIx(
<g/>
January	Januara	k1gFnPc1
<g/>
–	–	k?
<g/>
February	Februara	k1gFnSc2
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
The	The	k1gMnSc1
Small	Small	k1gMnSc1
World	World	k1gMnSc1
Problem	Probl	k1gMnSc7
<g/>
"	"	kIx"
(	(	kIx(
<g/>
PDF	PDF	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Society	societa	k1gFnPc1
(	(	kIx(
<g/>
Springer	Springer	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Social	Social	k1gInSc1
Science	Science	k1gFnSc1
and	and	k?
Public	publicum	k1gNnPc2
Policy	Polic	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Six	Six	k1gMnSc1
Degrees	Degrees	k1gMnSc1
of	of	k?
Separation	Separation	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Connecting	Connecting	k1gInSc1
with	witha	k1gFnPc2
people	people	k6eAd1
in	in	k?
six	six	k?
steps	steps	k1gInSc1
<g/>
.	.	kIx.
news	news	k1gInSc1
<g/>
.	.	kIx.
<g/>
bbc	bbc	k?
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
.	.	kIx.
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
KLEINFELD	KLEINFELD	kA
<g/>
,	,	kIx,
Judith	Judith	k1gMnSc1
S.	S.	kA
The	The	k1gMnSc1
small	small	k1gMnSc1
world	world	k6eAd1
problem	probl	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Society	societa	k1gFnPc1
<g/>
.	.	kIx.
2002	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
39	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
2	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
61	#num#	k4
<g/>
–	–	k?
<g/>
66	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1936	#num#	k4
<g/>
-	-	kIx~
<g/>
4725	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
BF	BF	kA
<g/>
0	#num#	k4
<g/>
2717530	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Six	Six	k1gMnSc1
Degrees	Degrees	k1gMnSc1
<g/>
:	:	kIx,
Urban	Urban	k1gMnSc1
Myth	Myth	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
|	|	kIx~
Psychology	psycholog	k1gMnPc7
Today	Todaa	k1gFnSc2
<g/>
.	.	kIx.
www.psychologytoday.com	www.psychologytoday.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WATTS	WATTS	kA
<g/>
,	,	kIx,
Duncan	Duncan	k1gMnSc1
J.	J.	kA
<g/>
;	;	kIx,
STROGATZ	STROGATZ	kA
<g/>
,	,	kIx,
Steven	Steven	k2eAgMnSc1d1
H.	H.	kA
Collective	Collectiv	k1gInSc5
dynamics	dynamicsa	k1gFnPc2
of	of	k?
‘	‘	k?
<g/>
small-world	small-world	k1gInSc1
<g/>
’	’	k?
networks	networks	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
<g/>
.	.	kIx.
1998	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
393	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6684	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
440	#num#	k4
<g/>
–	–	k?
<g/>
442	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1476	#num#	k4
<g/>
-	-	kIx~
<g/>
4687	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
30918	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LESKOVEC	LESKOVEC	kA
<g/>
,	,	kIx,
Jure	Jure	k1gFnSc1
<g/>
;	;	kIx,
HORVITZ	HORVITZ	kA
<g/>
,	,	kIx,
Eric	Eric	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Planetary-Scale	Planetary-Scal	k1gMnSc5
Views	Views	k1gInSc1
on	on	k3xPp3gMnSc1
an	an	k?
Instant-Messaging	Instant-Messaging	k1gInSc1
Network	network	k1gInSc1
<g/>
.	.	kIx.
arXiv	arXiv	k6eAd1
e-prints	e-prints	k6eAd1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
arXiv	arXiva	k1gFnPc2
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
803.093	803.093	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Degrees	Degrees	k1gInSc1
of	of	k?
Separation	Separation	k1gInSc1
in	in	k?
Social	Social	k1gInSc1
Networks	Networks	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Facebook	Facebook	k1gInSc1
MAU	MAU	kA
worldwide	worldwid	k1gInSc5
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Statista	statista	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Facebook	Facebook	k1gInSc1
Research	Research	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-02-04	2016-02-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SAMPLE	SAMPLE	kA
<g/>
,	,	kIx,
Ian	Ian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Six	Six	k1gMnSc1
degrees	degrees	k1gMnSc1
of	of	k?
copulation	copulation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
Scientist	Scientist	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LILJEROS	LILJEROS	kA
<g/>
,	,	kIx,
Fredrik	Fredrik	k1gMnSc1
<g/>
;	;	kIx,
EDLING	EDLING	kA
<g/>
,	,	kIx,
Christofer	Christofer	k1gMnSc1
R.	R.	kA
<g/>
;	;	kIx,
AMARAL	AMARAL	kA
<g/>
,	,	kIx,
Luís	Luís	k1gInSc1
A.	A.	kA
Nunes	Nunes	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
web	web	k1gInSc4
of	of	k?
human	humany	k1gInPc2
sexual	sexuat	k5eAaPmAgInS,k5eAaImAgInS,k5eAaBmAgInS
contacts	contacts	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
<g/>
.	.	kIx.
2001	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
411	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6840	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
907	#num#	k4
<g/>
–	–	k?
<g/>
908	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1476	#num#	k4
<g/>
-	-	kIx~
<g/>
4687	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
35082140	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Turina	Turina	k1gMnSc1
<g/>
,	,	kIx,
Ana	Ana	k1gMnSc1
Fernández-Caparrós	Fernández-Caparrós	k1gInSc1
<g/>
.	.	kIx.
"	"	kIx"
<g/>
The	The	k1gFnSc1
Geography	Geographa	k1gFnSc2
of	of	k?
Imagination	Imagination	k1gInSc1
<g/>
:	:	kIx,
Urban	Urban	k1gMnSc1
Mapping	Mapping	k1gInSc1
of	of	k?
Social	Social	k1gInSc1
Networks	Networks	k1gInSc1
in	in	k?
John	John	k1gMnSc1
Guare	Guar	k1gInSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
"	"	kIx"
<g/>
Six	Six	k1gMnSc1
Degrees	Degrees	k1gMnSc1
of	of	k?
Separation	Separation	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
"	"	kIx"
South	South	k1gInSc1
Atlantic	Atlantice	k1gFnPc2
Review	Review	k1gMnPc2
76	#num#	k4
<g/>
,	,	kIx,
no	no	k9
<g/>
.	.	kIx.
4	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
57	#num#	k4
<g/>
-	-	kIx~
<g/>
74	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Accessed	Accessed	k1gMnSc1
November	November	k1gMnSc1
12	#num#	k4
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
.	.	kIx.
http://www.jstor.org/stable/43738917.	http://www.jstor.org/stable/43738917.	k4
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Pulitzer	Pulitzer	k1gMnSc1
Prize	Prize	k1gFnSc2
for	forum	k1gNnPc2
Drama	drama	k1gFnSc1
<g/>
"	"	kIx"
pulitzer	pulitzer	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
,	,	kIx,
accessed	accessed	k1gInSc1
November	November	k1gInSc1
12	#num#	k4
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
↑	↑	k?
Memorable	Memorable	k1gMnSc1
quotes	quotes	k1gMnSc1
from	from	k1gMnSc1
Six	Six	k1gMnSc1
Degrees	Degrees	k1gMnSc1
of	of	k?
Separation	Separation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Accessed	Accessed	k1gInSc1
Nov	nov	k1gInSc4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
,	,	kIx,
2020	#num#	k4
from	from	k1gInSc1
IMDB	IMDB	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Harris	Harris	k1gFnPc2
<g/>
,	,	kIx,
Lauren	Laurna	k1gFnPc2
McArthur	McArthura	k1gFnPc2
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Six	Six	k1gMnSc1
Degrees	Degrees	k1gMnSc1
of	of	k?
the	the	k?
Mongol	mongol	k1gInSc1
Empire	empir	k1gInSc5
<g/>
:	:	kIx,
Using	Using	k1gInSc4
Thought	Thought	k2eAgInSc4d1
Experiments	Experiments	k1gInSc4
to	ten	k3xDgNnSc1
Prepare	Prepar	k1gMnSc5
World	World	k1gInSc1
History	Histor	k1gInPc4
Teachers	Teachers	k1gInSc1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
The	The	k1gFnSc2
History	Histor	k1gInPc1
Teacher	Teachra	k1gFnPc2
50	#num#	k4
<g/>
,	,	kIx,
no	no	k9
<g/>
.	.	kIx.
3	#num#	k4
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
381	#num#	k4
<g/>
-	-	kIx~
<g/>
401	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Accessed	Accessed	k1gMnSc1
November	November	k1gMnSc1
12	#num#	k4
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
.	.	kIx.
http://www.jstor.org/stable/44507256.	http://www.jstor.org/stable/44507256.	k4
<g/>
↑	↑	k?
Actor	Actor	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Hollywood	Hollywood	k1gInSc1
career	career	k1gMnSc1
spawned	spawned	k1gMnSc1
'	'	kIx"
<g/>
Six	Six	k1gMnSc1
Degrees	Degrees	k1gMnSc1
of	of	k?
Kevin	Kevin	k1gMnSc1
Bacon	Bacon	k1gMnSc1
<g/>
'	'	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Telegraph	Telegraph	k1gInSc1
<g/>
.	.	kIx.
6	#num#	k4
June	jun	k1gMnSc5
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
12	#num#	k4
November	November	k1gInSc1
2020	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Gillan	Gillan	k1gMnSc1
<g/>
,	,	kIx,
Jennifer	Jennifer	k1gMnSc1
<g/>
.	.	kIx.
""	""	k?
<g/>
No	no	k9
One	One	k1gMnSc1
Knows	Knowsa	k1gFnPc2
You	You	k1gMnSc1
<g/>
'	'	kIx"
<g/>
re	re	k9
Black	Black	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
"	"	kIx"
<g/>
:	:	kIx,
"	"	kIx"
<g/>
Six	Six	k1gMnSc1
Degrees	Degrees	k1gMnSc1
of	of	k?
Separation	Separation	k1gInSc1
<g/>
"	"	kIx"
and	and	k?
the	the	k?
Buddy	Budda	k1gMnSc2
Formula	Formul	k1gMnSc2
<g/>
.	.	kIx.
<g/>
"	"	kIx"
Cinema	Cinema	k1gFnSc1
Journal	Journal	k1gFnSc1
40	#num#	k4
<g/>
,	,	kIx,
no	no	k9
<g/>
.	.	kIx.
3	#num#	k4
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
47	#num#	k4
<g/>
-	-	kIx~
<g/>
68	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Accessed	Accessed	k1gMnSc1
November	November	k1gMnSc1
12	#num#	k4
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
.	.	kIx.
http://www.jstor.org/stable/1350194.	http://www.jstor.org/stable/1350194.	k4
<g/>
↑	↑	k?
Felperin	Felperin	k1gInSc1
<g/>
,	,	kIx,
Leslie	Leslie	k1gFnSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
.	.	kIx.
“	“	k?
<g/>
Babel	Babel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
”	”	k?
Sight	Sight	k1gInSc1
&	&	k?
Sound	Sound	k1gInSc1
17	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
41	#num#	k4
<g/>
–	–	k?
<g/>
43	#num#	k4
<g/>
.	.	kIx.
http://search.ebscohost.com/login.aspx?direct=true&	http://search.ebscohost.com/login.aspx?direct=true&	k?
<g/>
↑	↑	k?
https://booksaremyfavouriteandbest.com/6-degrees-of-separation-meme/	https://booksaremyfavouriteandbest.com/6-degrees-of-separation-meme/	k4
<g/>
↑	↑	k?
https://www.metal-archives.com/bands/Six_Degrees_of_Separation/11886#:~:text=Country%20of%20origin%3A%20Czechia%20Location,MetalGate%20Years%20active%3A%201996%2Dpresent	https://www.metal-archives.com/bands/Six_Degrees_of_Separation/11886#:~:text=Country%20of%20origin%3A%20Czechia%20Location,MetalGate%20Years%20active%3A%201996%2Dpresent	k1gMnSc1
<g/>
↑	↑	k?
Barabási	Barabáse	k1gFnSc4
Albert-László	Albert-László	k1gFnSc2
2013	#num#	k4
<g/>
Network	network	k1gInPc2
sciencePhil	sciencePhila	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trans.	Trans.	k1gFnPc2
R.	R.	kA
Soc	soc	kA
<g/>
.	.	kIx.
A	A	kA
<g/>
.37120120375	.37120120375	k4
</s>
<s>
http://doi.org/10.1098/rsta.2012.0375	http://doi.org/10.1098/rsta.2012.0375	k4
<g/>
↑	↑	k?
https://www.sixdegreesofwikipedia.com/	https://www.sixdegreesofwikipedia.com/	k?
<g/>
↑	↑	k?
https://research.fb.com/three-and-a-half-degrees-of-separation/	https://research.fb.com/three-and-a-half-degrees-of-separation/	k?
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Bezškálová	Bezškálový	k2eAgFnSc1d1
síť	síť	k1gFnSc1
</s>
<s>
Erdősovo	Erdősův	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
Dunbarovo	Dunbarův	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Sítě	síť	k1gFnPc1
„	„	k?
<g/>
malého	malý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
“	“	k?
</s>
<s>
Svět	svět	k1gInSc1
na	na	k7c4
pouhých	pouhý	k2eAgInPc2d1
šest	šest	k4xCc4
kroků	krok	k1gInPc2
</s>
<s>
Šest	šest	k4xCc1
osob	osoba	k1gFnPc2
stačí	stačit	k5eAaBmIp3nS
k	k	k7c3
propojení	propojení	k1gNnSc3
celé	celý	k2eAgFnSc2d1
planety	planeta	k1gFnSc2
</s>
