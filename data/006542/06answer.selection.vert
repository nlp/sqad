<s>
U	u	k7c2	u
eukaryotických	eukaryotický	k2eAgInPc2d1	eukaryotický
organizmů	organizmus	k1gInPc2	organizmus
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
např.	např.	kA	např.
rostliny	rostlina	k1gFnSc2	rostlina
a	a	k8xC	a
živočichové	živočich	k1gMnPc1	živočich
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
DNA	dna	k1gFnSc1	dna
hlavní	hlavní	k2eAgFnSc1d1	hlavní
složkou	složka	k1gFnSc7	složka
chromatinu	chromatin	k1gInSc2	chromatin
<g/>
,	,	kIx,	,
směsi	směs	k1gFnSc2	směs
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
a	a	k8xC	a
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
uložena	uložit	k5eAaPmNgFnS	uložit
zejména	zejména	k9	zejména
uvnitř	uvnitř	k7c2	uvnitř
buněčného	buněčný	k2eAgNnSc2d1	buněčné
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
prokaryot	prokaryota	k1gFnPc2	prokaryota
(	(	kIx(	(
<g/>
např.	např.	kA	např.
bakterie	bakterie	k1gFnSc2	bakterie
a	a	k8xC	a
archea	archeum	k1gNnSc2	archeum
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
DNA	dna	k1gFnSc1	dna
nachází	nacházet	k5eAaImIp3nS	nacházet
volně	volně	k6eAd1	volně
v	v	k7c6	v
cytoplazmě	cytoplazma	k1gFnSc6	cytoplazma
<g/>
.	.	kIx.	.
</s>
