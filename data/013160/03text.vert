<p>
<s>
Lípa	lípa	k1gFnSc1	lípa
v	v	k7c6	v
Lipce	lipka	k1gFnSc6	lipka
je	být	k5eAaImIp3nS	být
památný	památný	k2eAgInSc4d1	památný
strom	strom	k1gInSc4	strom
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Lipka	lipka	k1gFnSc1	lipka
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
města	město	k1gNnSc2	město
Chotěboř	Chotěboř	k1gFnSc1	Chotěboř
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
CHKO	CHKO	kA	CHKO
Železné	železný	k2eAgFnSc2d1	železná
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Rozměry	rozměr	k1gInPc1	rozměr
i	i	k8xC	i
stáří	stáří	k1gNnSc4	stáří
stromu	strom	k1gInSc2	strom
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
rod	rod	k1gInSc4	rod
nezvykle	zvykle	k6eNd1	zvykle
vysoké	vysoký	k2eAgFnPc1d1	vysoká
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
obvodem	obvod	k1gInSc7	obvod
kmene	kmen	k1gInSc2	kmen
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
věkem	věk	k1gInSc7	věk
řadí	řadit	k5eAaImIp3nP	řadit
mezi	mezi	k7c4	mezi
pět	pět	k4xCc4	pět
nejvzrostlejších	vzrostlý	k2eAgFnPc2d3	vzrostlý
lip	lípa	k1gFnPc2	lípa
malolistých	malolistý	k2eAgFnPc2d1	malolistá
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgFnPc3d3	nejstarší
lipám	lípa	k1gFnPc3	lípa
Chrudimska	Chrudimsk	k1gInSc2	Chrudimsk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgInPc1d1	základní
údaje	údaj	k1gInPc1	údaj
==	==	k?	==
</s>
</p>
<p>
<s>
název	název	k1gInSc1	název
<g/>
:	:	kIx,	:
Lípa	lípa	k1gFnSc1	lípa
v	v	k7c6	v
Lipce	lipka	k1gFnSc6	lipka
<g/>
,	,	kIx,	,
Lípa	lípa	k1gFnSc1	lípa
na	na	k7c6	na
Lipce	lipka	k1gFnSc6	lipka
</s>
</p>
<p>
<s>
výška	výška	k1gFnSc1	výška
<g/>
:	:	kIx,	:
27	[number]	k4	27
m	m	kA	m
<g/>
,	,	kIx,	,
27	[number]	k4	27
m	m	kA	m
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
32	[number]	k4	32
m	m	kA	m
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
32	[number]	k4	32
m	m	kA	m
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
obvod	obvod	k1gInSc1	obvod
<g/>
:	:	kIx,	:
802	[number]	k4	802
cm	cm	kA	cm
<g/>
,	,	kIx,	,
835	[number]	k4	835
cm	cm	kA	cm
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
840	[number]	k4	840
cm	cm	kA	cm
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
870	[number]	k4	870
cm	cm	kA	cm
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
věk	věk	k1gInSc1	věk
<g/>
:	:	kIx,	:
600	[number]	k4	600
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
<g/>
:	:	kIx,	:
2	[number]	k4	2
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
3	[number]	k4	3
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
sanace	sanace	k1gFnSc1	sanace
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
umístění	umístění	k1gNnSc1	umístění
<g/>
:	:	kIx,	:
kraj	kraj	k1gInSc1	kraj
Pardubický	pardubický	k2eAgInSc1d1	pardubický
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Chrudim	Chrudim	k1gFnSc1	Chrudim
<g/>
,	,	kIx,	,
obec	obec	k1gFnSc1	obec
Horní	horní	k2eAgNnSc1d1	horní
Bradlo	bradlo	k1gNnSc1	bradlo
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
obce	obec	k1gFnSc2	obec
Lipka	lipka	k1gFnSc1	lipka
</s>
</p>
<p>
<s>
souřadnice	souřadnice	k1gFnSc1	souřadnice
<g/>
:	:	kIx,	:
49	[number]	k4	49
<g/>
°	°	k?	°
<g/>
47	[number]	k4	47
<g/>
'	'	kIx"	'
<g/>
45.23	[number]	k4	45.23
<g/>
"	"	kIx"	"
<g/>
N	N	kA	N
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
°	°	k?	°
<g/>
43	[number]	k4	43
<g/>
'	'	kIx"	'
<g/>
58.81	[number]	k4	58.81
<g/>
"	"	kIx"	"
<g/>
ELípa	ELípa	k1gFnSc1	ELípa
roste	růst	k5eAaImIp3nS	růst
u	u	k7c2	u
žluté	žlutý	k2eAgFnSc2d1	žlutá
turistické	turistický	k2eAgFnSc2d1	turistická
trasy	trasa	k1gFnSc2	trasa
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
původně	původně	k6eAd1	původně
gotické	gotický	k2eAgFnSc2d1	gotická
tvrze	tvrz	k1gFnSc2	tvrz
(	(	kIx(	(
<g/>
jižně	jižně	k6eAd1	jižně
<g/>
)	)	kIx)	)
v	v	k7c6	v
Lipce	lipka	k1gFnSc6	lipka
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
od	od	k7c2	od
lípy	lípa	k1gFnSc2	lípa
dále	daleko	k6eAd2	daleko
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
ke	k	k7c3	k
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
vzdálené	vzdálený	k2eAgInPc4d1	vzdálený
hrobce	hrobec	k1gInPc4	hrobec
bývalých	bývalý	k2eAgMnPc2d1	bývalý
majitelů	majitel	k1gMnPc2	majitel
tvrze	tvrz	k1gFnSc2	tvrz
Kustošů	Kustoš	k1gMnPc2	Kustoš
ze	z	k7c2	z
Zubří	zubří	k2eAgFnSc2d1	zubří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stav	stav	k1gInSc1	stav
stromu	strom	k1gInSc2	strom
a	a	k8xC	a
údržba	údržba	k1gFnSc1	údržba
==	==	k?	==
</s>
</p>
<p>
<s>
Původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
lípa	lípa	k1gFnSc1	lípa
troják	troják	k1gInSc1	troják
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
kmenů	kmen	k1gInPc2	kmen
se	se	k3xPyFc4	se
vylomil	vylomit	k5eAaPmAgInS	vylomit
a	a	k8xC	a
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
zpevněna	zpevněn	k2eAgFnSc1d1	zpevněna
a	a	k8xC	a
během	během	k7c2	během
let	léto	k1gNnPc2	léto
lípa	lípa	k1gFnSc1	lípa
ránu	rána	k1gFnSc4	rána
po	po	k7c6	po
vylomené	vylomený	k2eAgFnSc6d1	vylomená
části	část	k1gFnSc6	část
zacelila	zacelit	k5eAaPmAgFnS	zacelit
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
dorostla	dorůst	k5eAaPmAgFnS	dorůst
i	i	k9	i
chybějící	chybějící	k2eAgFnSc1d1	chybějící
část	část	k1gFnSc1	část
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
lípa	lípa	k1gFnSc1	lípa
dvoják	dvoják	k1gInSc1	dvoják
<g/>
,	,	kIx,	,
kmen	kmen	k1gInSc1	kmen
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
poměrně	poměrně	k6eAd1	poměrně
nízko	nízko	k6eAd1	nízko
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dutý	dutý	k2eAgInSc4d1	dutý
až	až	k9	až
do	do	k7c2	do
koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
průměr	průměr	k1gInSc1	průměr
činí	činit	k5eAaImIp3nS	činit
26	[number]	k4	26
m.	m.	k?	m.
Strom	strom	k1gInSc1	strom
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
věk	věk	k1gInSc4	věk
nezvykle	zvykle	k6eNd1	zvykle
zachovalý	zachovalý	k2eAgMnSc1d1	zachovalý
a	a	k8xC	a
v	v	k7c6	v
dobrém	dobrý	k2eAgInSc6d1	dobrý
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
a	a	k8xC	a
pověsti	pověst	k1gFnPc1	pověst
==	==	k?	==
</s>
</p>
<p>
<s>
Lípa	lípa	k1gFnSc1	lípa
stojí	stát	k5eAaImIp3nS	stát
prakticky	prakticky	k6eAd1	prakticky
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
cesty	cesta	k1gFnSc2	cesta
mezi	mezi	k7c7	mezi
hrobkou	hrobka	k1gFnSc7	hrobka
pánů	pan	k1gMnPc2	pan
Kustošů	Kustoš	k1gMnPc2	Kustoš
ze	z	k7c2	z
Zubří	zubří	k2eAgFnSc2d1	zubří
a	a	k8xC	a
Lipky	lipka	k1gFnSc2	lipka
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc7	jejich
zámkem	zámek	k1gInSc7	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
gotickou	gotický	k2eAgFnSc7d1	gotická
tvrzí	tvrz	k1gFnSc7	tvrz
založenou	založený	k2eAgFnSc7d1	založená
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
věk	věk	k1gInSc1	věk
lípy	lípa	k1gFnSc2	lípa
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
odpovídat	odpovídat	k5eAaImF	odpovídat
vysazení	vysazení	k1gNnSc4	vysazení
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
stavby	stavba	k1gFnSc2	stavba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1561	[number]	k4	1561
-	-	kIx~	-
1588	[number]	k4	1588
tvrz	tvrz	k1gFnSc1	tvrz
přestavěl	přestavět	k5eAaPmAgMnS	přestavět
Jan	Jan	k1gMnSc1	Jan
Kustoš	Kustoš	k1gMnSc1	Kustoš
na	na	k7c4	na
renesanční	renesanční	k2eAgInSc4d1	renesanční
zámek	zámek	k1gInSc4	zámek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
dále	daleko	k6eAd2	daleko
upravován	upravovat	k5eAaImNgInS	upravovat
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
zámek	zámek	k1gInSc1	zámek
neobydlený	obydlený	k2eNgInSc1d1	neobydlený
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
řadu	řada	k1gFnSc4	řada
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
nedokončené	dokončený	k2eNgFnSc2d1	nedokončená
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
na	na	k7c6	na
nedaleké	daleký	k2eNgFnSc6d1	nedaleká
hrobce	hrobka	k1gFnSc6	hrobka
prozrazují	prozrazovat	k5eAaImIp3nP	prozrazovat
šlechtický	šlechtický	k2eAgInSc4d1	šlechtický
původ	původ	k1gInSc4	původ
rodu	rod	k1gInSc2	rod
Kustošů	Kustoš	k1gMnPc2	Kustoš
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
také	také	k9	také
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
nechala	nechat	k5eAaPmAgFnS	nechat
vystavět	vystavět	k5eAaPmF	vystavět
kněžna	kněžna	k1gFnSc1	kněžna
Vilemína	Vilemína	k1gFnSc1	Vilemína
z	z	k7c2	z
Augspergu	Augsperg	k1gInSc2	Augsperg
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
(	(	kIx(	(
<g/>
t.j.	t.j.	k?	t.j.
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
1876	[number]	k4	1876
údajně	údajně	k6eAd1	údajně
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zničení	zničení	k1gNnSc3	zničení
bleskem	blesk	k1gInSc7	blesk
(	(	kIx(	(
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
u	u	k7c2	u
hrobky	hrobka	k1gFnSc2	hrobka
stál	stát	k5eAaImAgInS	stát
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
po	po	k7c6	po
zásahu	zásah	k1gInSc6	zásah
padl	padnout	k5eAaImAgMnS	padnout
a	a	k8xC	a
stavbu	stavba	k1gFnSc4	stavba
poškodil	poškodit	k5eAaPmAgInS	poškodit
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
a	a	k8xC	a
kněžna	kněžna	k1gFnSc1	kněžna
nechala	nechat	k5eAaPmAgFnS	nechat
vše	všechen	k3xTgNnSc4	všechen
znovu	znovu	k6eAd1	znovu
vystavět	vystavět	k5eAaPmF	vystavět
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
zřejmě	zřejmě	k6eAd1	zřejmě
pocházejí	pocházet	k5eAaImIp3nP	pocházet
blízké	blízký	k2eAgInPc4d1	blízký
javory	javor	k1gInPc4	javor
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
-	-	kIx~	-
ač	ač	k8xS	ač
mladší	mladý	k2eAgFnSc1d2	mladší
než	než	k8xS	než
lípa	lípa	k1gFnSc1	lípa
-	-	kIx~	-
prozradí	prozradit	k5eAaPmIp3nS	prozradit
svůj	svůj	k3xOyFgInSc4	svůj
věk	věk	k1gInSc4	věk
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
oslav	oslava	k1gFnPc2	oslava
Dne	den	k1gInSc2	den
stromů	strom	k1gInPc2	strom
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2016	[number]	k4	2016
byla	být	k5eAaImAgFnS	být
lípa	lípa	k1gFnSc1	lípa
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
vítězem	vítěz	k1gMnSc7	vítěz
jubilejního	jubilejní	k2eAgMnSc2d1	jubilejní
patnáctého	patnáctý	k4xOgInSc2	patnáctý
ročníku	ročník	k1gInSc2	ročník
celostátní	celostátní	k2eAgFnSc2d1	celostátní
ankety	anketa	k1gFnSc2	anketa
Strom	strom	k1gInSc4	strom
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Lípa	lípa	k1gFnSc1	lípa
zastupovala	zastupovat	k5eAaImAgFnS	zastupovat
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
i	i	k9	i
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
anketě	anketa	k1gFnSc6	anketa
Evropský	evropský	k2eAgInSc1d1	evropský
strom	strom	k1gInSc1	strom
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
zajímavosti	zajímavost	k1gFnPc1	zajímavost
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
==	==	k?	==
</s>
</p>
<p>
<s>
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
Spálava	Spálava	k1gFnSc1	Spálava
</s>
</p>
<p>
<s>
300	[number]	k4	300
<g/>
leté	letý	k2eAgInPc1d1	letý
stromy	strom	k1gInPc1	strom
v	v	k7c6	v
pralesní	pralesní	k2eAgFnSc6d1	pralesní
jedlobučině	jedlobučina	k1gFnSc6	jedlobučina
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Polom	polom	k1gInSc1	polom
(	(	kIx(	(
<g/>
1,5	[number]	k4	1,5
km	km	kA	km
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Památné	památný	k2eAgInPc4d1	památný
a	a	k8xC	a
významné	významný	k2eAgInPc4d1	významný
stromy	strom	k1gInPc4	strom
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
===	===	k?	===
</s>
</p>
<p>
<s>
blízké	blízký	k2eAgNnSc1d1	blízké
okolí	okolí	k1gNnSc1	okolí
</s>
</p>
<p>
<s>
Spálavská	Spálavský	k2eAgFnSc1d1	Spálavský
lípa	lípa	k1gFnSc1	lípa
(	(	kIx(	(
<g/>
500	[number]	k4	500
<g/>
-	-	kIx~	-
<g/>
550	[number]	k4	550
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
5	[number]	k4	5
km	km	kA	km
<g/>
,	,	kIx,	,
pěšky	pěšky	k6eAd1	pěšky
3	[number]	k4	3
km	km	kA	km
<g/>
,	,	kIx,	,
J	J	kA	J
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Stará	starý	k2eAgFnSc1d1	stará
královna	královna	k1gFnSc1	královna
z	z	k7c2	z
Polomi	Polo	k1gFnPc7	Polo
(	(	kIx(	(
<g/>
jedle	jedle	k6eAd1	jedle
s	s	k7c7	s
obvodem	obvod	k1gInSc7	obvod
580	[number]	k4	580
cm	cm	kA	cm
<g/>
,	,	kIx,	,
padla	padnout	k5eAaPmAgFnS	padnout
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
<g/>
J	J	kA	J
směr	směr	k1gInSc1	směr
</s>
</p>
<p>
<s>
Lánská	lánský	k2eAgFnSc1d1	lánská
lípa	lípa	k1gFnSc1	lípa
(	(	kIx(	(
<g/>
700	[number]	k4	700
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
10	[number]	k4	10
km	km	kA	km
<g/>
,	,	kIx,	,
pěšky	pěšky	k6eAd1	pěšky
5	[number]	k4	5
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
JV	JV	kA	JV
směr	směr	k1gInSc1	směr
</s>
</p>
<p>
<s>
Jírovce	jírovec	k1gInPc1	jírovec
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Hluboká	Hluboká	k1gFnSc1	Hluboká
(	(	kIx(	(
<g/>
významné	významný	k2eAgInPc1d1	významný
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
8,5	[number]	k4	8,5
km	km	kA	km
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Štikovská	Štikovský	k2eAgFnSc1d1	Štikovská
lípa	lípa	k1gFnSc1	lípa
(	(	kIx(	(
<g/>
600	[number]	k4	600
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
14	[number]	k4	14
km	km	kA	km
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vestecký	vestecký	k2eAgInSc1d1	vestecký
kaštanovník	kaštanovník	k1gInSc1	kaštanovník
(	(	kIx(	(
<g/>
11	[number]	k4	11
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
JZ	JZ	kA	JZ
směr	směr	k1gInSc1	směr
</s>
</p>
<p>
<s>
Žižkův	Žižkův	k2eAgInSc1d1	Žižkův
dub	dub	k1gInSc1	dub
(	(	kIx(	(
<g/>
Chotěboř	Chotěboř	k1gFnSc1	Chotěboř
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
zaniklý	zaniklý	k2eAgInSc1d1	zaniklý
strom	strom	k1gInSc1	strom
<g/>
,	,	kIx,	,
13	[number]	k4	13
km	km	kA	km
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Žižkovy	Žižkův	k2eAgInPc1d1	Žižkův
duby	dub	k1gInPc1	dub
(	(	kIx(	(
<g/>
Chotěboř	Chotěboř	k1gFnSc1	Chotěboř
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
13	[number]	k4	13
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
V	v	k7c4	v
směr	směr	k1gInSc4	směr
</s>
</p>
<p>
<s>
Lípa	lípa	k1gFnSc1	lípa
v	v	k7c6	v
Kameničkách	Kameničky	k1gFnPc6	Kameničky
(	(	kIx(	(
<g/>
450	[number]	k4	450
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
5	[number]	k4	5
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
Z	z	k7c2	z
směr	směr	k1gInSc4	směr
</s>
</p>
<p>
<s>
Přemilovský	Přemilovský	k2eAgInSc1d1	Přemilovský
jilm	jilm	k1gInSc1	jilm
(	(	kIx(	(
<g/>
5	[number]	k4	5
km	km	kA	km
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Klokočovská	Klokočovský	k2eAgFnSc1d1	Klokočovská
lípa	lípa	k1gFnSc1	lípa
(	(	kIx(	(
<g/>
1000	[number]	k4	1000
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
6	[number]	k4	6
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
SV	sv	kA	sv
směr	směr	k1gInSc1	směr
</s>
</p>
<p>
<s>
Kaštanka	kaštanka	k1gFnSc1	kaštanka
(	(	kIx(	(
<g/>
Nasavrky	Nasavrek	k1gInPc1	Nasavrek
<g/>
,	,	kIx,	,
10	[number]	k4	10
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
S	s	k7c7	s
směr	směr	k1gInSc1	směr
</s>
</p>
<p>
<s>
Lípy	lípa	k1gFnPc1	lípa
v	v	k7c6	v
Krásném	krásný	k2eAgInSc6d1	krásný
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
2	[number]	k4	2
lípy	lípa	k1gFnSc2	lípa
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
<g/>
,	,	kIx,	,
zůstala	zůstat	k5eAaPmAgFnS	zůstat
1	[number]	k4	1
<g/>
,	,	kIx,	,
6	[number]	k4	6
km	km	kA	km
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Lípa	lípa	k1gFnSc1	lípa
malolistá	malolistý	k2eAgFnSc1d1	malolistá
</s>
</p>
<p>
<s>
Lipka	lipka	k1gFnSc1	lipka
</s>
</p>
<p>
<s>
Památný	památný	k2eAgInSc1d1	památný
strom	strom	k1gInSc1	strom
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lípa	lípa	k1gFnSc1	lípa
v	v	k7c6	v
Lipce	lipka	k1gFnSc6	lipka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
