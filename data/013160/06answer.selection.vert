<s>
Lípa	lípa	k1gFnSc1	lípa
v	v	k7c6	v
Lipce	lipka	k1gFnSc6	lipka
je	být	k5eAaImIp3nS	být
památný	památný	k2eAgInSc4d1	památný
strom	strom	k1gInSc4	strom
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Lipka	lipka	k1gFnSc1	lipka
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
města	město	k1gNnSc2	město
Chotěboř	Chotěboř	k1gFnSc1	Chotěboř
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
CHKO	CHKO	kA	CHKO
Železné	železný	k2eAgFnSc2d1	železná
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
