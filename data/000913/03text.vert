<s>
Dominik	Dominik	k1gMnSc1	Dominik
Hašek	Hašek	k1gMnSc1	Hašek
<g/>
,	,	kIx,	,
rodným	rodný	k2eAgNnSc7d1	rodné
příjmením	příjmení	k1gNnSc7	příjmení
Kaštánek	kaštánek	k1gMnSc1	kaštánek
(	(	kIx(	(
<g/>
*	*	kIx~	*
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1965	[number]	k4	1965
Pardubice	Pardubice	k1gInPc4	Pardubice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
také	také	k9	také
pod	pod	k7c7	pod
přezdívkou	přezdívka	k1gFnSc7	přezdívka
Dominátor	Dominátor	k1gMnSc1	Dominátor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
český	český	k2eAgMnSc1d1	český
hokejový	hokejový	k2eAgMnSc1d1	hokejový
brankář	brankář	k1gMnSc1	brankář
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
polorodým	polorodý	k2eAgMnSc7d1	polorodý
bratrem	bratr	k1gMnSc7	bratr
(	(	kIx(	(
<g/>
mají	mít	k5eAaImIp3nP	mít
společnou	společný	k2eAgFnSc4d1	společná
matku	matka	k1gFnSc4	matka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
fotbalista	fotbalista	k1gMnSc1	fotbalista
Martin	Martin	k1gMnSc1	Martin
Hašek	Hašek	k1gMnSc1	Hašek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
jej	on	k3xPp3gMnSc4	on
časopis	časopis	k1gInSc1	časopis
The	The	k1gMnPc2	The
Hockey	Hockea	k1gFnSc2	Hockea
News	News	k1gInSc1	News
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
evropským	evropský	k2eAgMnSc7d1	evropský
hokejistou	hokejista	k1gMnSc7	hokejista
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
Hokej	hokej	k1gInSc1	hokej
začal	začít	k5eAaPmAgInS	začít
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
šesti	šest	k4xCc6	šest
letech	let	k1gInPc6	let
v	v	k7c6	v
rodných	rodný	k2eAgInPc6d1	rodný
Pardubicích	Pardubice	k1gInPc6	Pardubice
<g/>
,	,	kIx,	,
v	v	k7c6	v
16	[number]	k4	16
letech	léto	k1gNnPc6	léto
si	se	k3xPyFc3	se
za	za	k7c4	za
tým	tým	k1gInSc4	tým
Tesla	Tesla	k1gFnSc1	Tesla
Pardubice	Pardubice	k1gInPc4	Pardubice
poprvé	poprvé	k6eAd1	poprvé
zachytal	zachytat	k5eAaPmAgInS	zachytat
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
československé	československý	k2eAgFnSc6d1	Československá
lize	liga	k1gFnSc6	liga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1982	[number]	k4	1982
<g/>
/	/	kIx~	/
<g/>
1983	[number]	k4	1983
se	se	k3xPyFc4	se
Hašek	Hašek	k1gMnSc1	Hašek
v	v	k7c6	v
pardubickém	pardubický	k2eAgInSc6d1	pardubický
týmu	tým	k1gInSc6	tým
propracoval	propracovat	k5eAaPmAgInS	propracovat
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
brankářské	brankářský	k2eAgFnSc2d1	brankářská
jedničky	jednička	k1gFnSc2	jednička
a	a	k8xC	a
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
18	[number]	k4	18
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
reprezentačním	reprezentační	k2eAgMnSc7d1	reprezentační
brankářem	brankář	k1gMnSc7	brankář
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
draftu	draft	k1gInSc6	draft
NHL	NHL	kA	NHL
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
byl	být	k5eAaImAgMnS	být
vybrán	vybrat	k5eAaPmNgMnS	vybrat
na	na	k7c4	na
celkově	celkově	k6eAd1	celkově
199	[number]	k4	199
<g/>
.	.	kIx.	.
pozici	pozice	k1gFnSc6	pozice
týmem	tým	k1gInSc7	tým
Chicago	Chicago	k1gNnSc4	Chicago
Blackhawks	Blackhawksa	k1gFnPc2	Blackhawksa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1987	[number]	k4	1987
a	a	k8xC	a
1989	[number]	k4	1989
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
s	s	k7c7	s
Pardubicemi	Pardubice	k1gInPc7	Pardubice
mistrem	mistr	k1gMnSc7	mistr
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
1989	[number]	k4	1989
<g/>
/	/	kIx~	/
<g/>
1990	[number]	k4	1990
odehrál	odehrát	k5eAaPmAgMnS	odehrát
kvůli	kvůli	k7c3	kvůli
vojenské	vojenský	k2eAgFnSc3d1	vojenská
službě	služba	k1gFnSc3	služba
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
HC	HC	kA	HC
Dukla	Dukla	k1gFnSc1	Dukla
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
sezóně	sezóna	k1gFnSc6	sezóna
poprvé	poprvé	k6eAd1	poprvé
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
zámořské	zámořský	k2eAgFnSc6d1	zámořská
International	International	k1gFnSc6	International
Hockey	Hockea	k1gFnSc2	Hockea
League	Leagu	k1gFnSc2	Leagu
za	za	k7c4	za
tým	tým	k1gInSc4	tým
Indianapolis	Indianapolis	k1gFnSc2	Indianapolis
Ice	Ice	k1gFnSc2	Ice
a	a	k8xC	a
v	v	k7c6	v
National	National	k1gFnSc6	National
Hockey	Hockea	k1gFnSc2	Hockea
League	Leagu	k1gFnSc2	Leagu
za	za	k7c4	za
tým	tým	k1gInSc4	tým
Chicago	Chicago	k1gNnSc4	Chicago
Blackhawks	Blackhawks	k1gInSc4	Blackhawks
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
letech	let	k1gInPc6	let
byl	být	k5eAaImAgInS	být
vyměněn	vyměnit	k5eAaPmNgInS	vyměnit
do	do	k7c2	do
týmu	tým	k1gInSc2	tým
Buffalo	Buffalo	k1gMnSc1	Buffalo
Sabres	Sabres	k1gMnSc1	Sabres
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stal	stát	k5eAaPmAgInS	stát
jedničkou	jednička	k1gFnSc7	jednička
a	a	k8xC	a
kde	kde	k6eAd1	kde
strávil	strávit	k5eAaPmAgMnS	strávit
celých	celý	k2eAgFnPc2d1	celá
9	[number]	k4	9
sezón	sezóna	k1gFnPc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
s	s	k7c7	s
českým	český	k2eAgInSc7d1	český
reprezentačním	reprezentační	k2eAgInSc7d1	reprezentační
výběrem	výběr	k1gInSc7	výběr
stal	stát	k5eAaPmAgInS	stát
olympijským	olympijský	k2eAgMnSc7d1	olympijský
vítězem	vítěz	k1gMnSc7	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
2001	[number]	k4	2001
<g/>
/	/	kIx~	/
<g/>
2002	[number]	k4	2002
odehrál	odehrát	k5eAaPmAgMnS	odehrát
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
Detroit	Detroit	k1gInSc1	Detroit
Red	Red	k1gFnSc2	Red
Wings	Wingsa	k1gFnPc2	Wingsa
a	a	k8xC	a
poprvé	poprvé	k6eAd1	poprvé
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
Stanley	Stanlea	k1gFnSc2	Stanlea
Cup	cup	k1gInSc1	cup
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
následující	následující	k2eAgFnSc6d1	následující
roční	roční	k2eAgFnSc6d1	roční
pauze	pauza	k1gFnSc6	pauza
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
NHL	NHL	kA	NHL
a	a	k8xC	a
odehrál	odehrát	k5eAaPmAgInS	odehrát
další	další	k2eAgFnSc4d1	další
sezónu	sezóna	k1gFnSc4	sezóna
za	za	k7c4	za
Detroit	Detroit	k1gInSc4	Detroit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
pak	pak	k6eAd1	pak
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
týmu	tým	k1gInSc2	tým
Ottawa	Ottawa	k1gFnSc1	Ottawa
Senators	Senators	k1gInSc4	Senators
<g/>
,	,	kIx,	,
odehrál	odehrát	k5eAaPmAgMnS	odehrát
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
jednu	jeden	k4xCgFnSc4	jeden
sezónu	sezóna	k1gFnSc4	sezóna
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Detroitu	Detroit	k1gInSc2	Detroit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Red	Red	k1gFnSc4	Red
Wings	Wingsa	k1gFnPc2	Wingsa
tentokrát	tentokrát	k6eAd1	tentokrát
odehrál	odehrát	k5eAaPmAgMnS	odehrát
dvě	dva	k4xCgFnPc4	dva
sezóny	sezóna	k1gFnPc4	sezóna
a	a	k8xC	a
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
druhý	druhý	k4xOgInSc1	druhý
Stanley	Stanle	k1gMnPc4	Stanle
Cup	cup	k1gInSc4	cup
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
další	další	k2eAgFnSc1d1	další
roční	roční	k2eAgFnSc1d1	roční
pauza	pauza	k1gFnSc1	pauza
a	a	k8xC	a
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
roční	roční	k2eAgInPc1d1	roční
působení	působení	k1gNnSc4	působení
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
extralize	extraliga	k1gFnSc6	extraliga
<g/>
,	,	kIx,	,
završené	završený	k2eAgFnSc6d1	završená
ziskem	zisk	k1gInSc7	zisk
mistrovského	mistrovský	k2eAgInSc2d1	mistrovský
titulu	titul	k1gInSc2	titul
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
HC	HC	kA	HC
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Sezónu	sezóna	k1gFnSc4	sezóna
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
pak	pak	k8xC	pak
Hašek	Hašek	k1gMnSc1	Hašek
odehrál	odehrát	k5eAaPmAgMnS	odehrát
v	v	k7c6	v
Kontinentální	kontinentální	k2eAgFnSc6d1	kontinentální
hokejové	hokejový	k2eAgFnSc6d1	hokejová
lize	liga	k1gFnSc6	liga
za	za	k7c4	za
tým	tým	k1gInSc4	tým
HC	HC	kA	HC
Spartak	Spartak	k1gInSc1	Spartak
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Hašek	Hašek	k1gMnSc1	Hašek
je	být	k5eAaImIp3nS	být
šestinásobný	šestinásobný	k2eAgMnSc1d1	šestinásobný
nositel	nositel	k1gMnSc1	nositel
Vezina	Vezin	k2eAgMnSc2d1	Vezin
Trophy	Tropha	k1gMnSc2	Tropha
<g/>
,	,	kIx,	,
dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
vítěz	vítěz	k1gMnSc1	vítěz
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
-	-	kIx~	-
<g/>
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
ceny	cena	k1gFnSc2	cena
pro	pro	k7c4	pro
nejužitečnějšího	užitečný	k2eAgMnSc4d3	nejužitečnější
hráče	hráč	k1gMnSc4	hráč
NHL	NHL	kA	NHL
-	-	kIx~	-
Hart	Hart	k2eAgInSc1d1	Hart
Memorial	Memorial	k1gInSc1	Memorial
Trophy	Tropha	k1gFnSc2	Tropha
<g/>
,	,	kIx,	,
dvakrát	dvakrát	k6eAd1	dvakrát
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
také	také	k9	také
Lester	Lester	k1gMnSc1	Lester
B.	B.	kA	B.
Pearson	Pearson	k1gMnSc1	Pearson
Award	Award	k1gMnSc1	Award
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třikrát	třikrát	k6eAd1	třikrát
pak	pak	k6eAd1	pak
William	William	k1gInSc1	William
M.	M.	kA	M.
Jennings	Jennings	k1gInSc1	Jennings
Trophy	Tropha	k1gMnSc2	Tropha
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
pětkrát	pětkrát	k6eAd1	pětkrát
vítězem	vítěz	k1gMnSc7	vítěz
ankety	anketa	k1gFnSc2	anketa
Zlaté	zlatá	k1gFnSc2	zlatá
hokejky	hokejka	k1gFnSc2	hokejka
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
vítězem	vítěz	k1gMnSc7	vítěz
ankety	anketa	k1gFnSc2	anketa
Hokejista	hokejista	k1gMnSc1	hokejista
století	století	k1gNnSc2	století
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1994	[number]	k4	1994
a	a	k8xC	a
1998	[number]	k4	1998
vítězem	vítěz	k1gMnSc7	vítěz
ankety	anketa	k1gFnSc2	anketa
Sportovec	sportovec	k1gMnSc1	sportovec
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
českého	český	k2eAgInSc2d1	český
hokeje	hokej	k1gInSc2	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2013	[number]	k4	2013
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
pardubické	pardubický	k2eAgFnSc6d1	pardubická
ČEZ	ČEZ	kA	ČEZ
Aréně	aréna	k1gFnSc6	aréna
vyvěšen	vyvěšen	k2eAgInSc4d1	vyvěšen
Haškův	Haškův	k2eAgInSc4d1	Haškův
dres	dres	k1gInSc4	dres
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byl	být	k5eAaImAgMnS	být
uveden	uvést	k5eAaPmNgMnS	uvést
do	do	k7c2	do
Hokejové	hokejový	k2eAgFnSc2d1	hokejová
síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2015	[number]	k4	2015
Buffalo	Buffalo	k1gMnPc1	Buffalo
Sabres	Sabresa	k1gFnPc2	Sabresa
slavnostně	slavnostně	k6eAd1	slavnostně
vyřadilo	vyřadit	k5eAaPmAgNnS	vyřadit
jeho	jeho	k3xOp3gNnSc1	jeho
číslo	číslo	k1gNnSc1	číslo
39	[number]	k4	39
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
banner	banner	k1gInSc1	banner
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
dne	den	k1gInSc2	den
visí	viset	k5eAaImIp3nS	viset
u	u	k7c2	u
stropu	strop	k1gInSc2	strop
haly	hala	k1gFnSc2	hala
First	First	k1gFnSc1	First
Niagara	Niagara	k1gFnSc1	Niagara
Center	centrum	k1gNnPc2	centrum
v	v	k7c6	v
Buffalu	Buffal	k1gInSc6	Buffal
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
Síně	síň	k1gFnSc2	síň
slávy	sláva	k1gFnSc2	sláva
IIHF	IIHF	kA	IIHF
<g/>
.	.	kIx.	.
</s>
<s>
Dominik	Dominik	k1gMnSc1	Dominik
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
Aloisi	Alois	k1gMnSc3	Alois
a	a	k8xC	a
Marii	Maria	k1gFnSc3	Maria
Kaštánkovým	kaštánkový	k2eAgFnPc3d1	Kaštánková
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
rodiče	rodič	k1gMnPc1	rodič
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
podruhé	podruhé	k6eAd1	podruhé
za	za	k7c4	za
Jana	Jan	k1gMnSc4	Jan
Haška	Hašek	k1gMnSc4	Hašek
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Dominikovi	Dominikův	k2eAgMnPc1d1	Dominikův
do	do	k7c2	do
jeho	jeho	k3xOp3gInPc2	jeho
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeho	jeho	k3xOp3gMnSc1	jeho
biologický	biologický	k2eAgMnSc1d1	biologický
otec	otec	k1gMnSc1	otec
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
s	s	k7c7	s
přejmenováním	přejmenování	k1gNnSc7	přejmenování
<g/>
,	,	kIx,	,
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
příjmení	příjmení	k1gNnSc1	příjmení
Kaštánek	kaštánek	k1gInSc1	kaštánek
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
jeho	jeho	k3xOp3gMnSc1	jeho
nevlastní	vlastní	k2eNgMnSc1d1	nevlastní
otec	otec	k1gMnSc1	otec
jej	on	k3xPp3gMnSc4	on
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
vzal	vzít	k5eAaPmAgInS	vzít
na	na	k7c4	na
zamrzlé	zamrzlý	k2eAgNnSc4d1	zamrzlé
jezero	jezero	k1gNnSc4	jezero
za	za	k7c7	za
řekou	řeka	k1gFnSc7	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
si	se	k3xPyFc3	se
doma	doma	k6eAd1	doma
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
brankáře	brankář	k1gMnSc4	brankář
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgFnP	být
dveře	dveře	k1gFnPc1	dveře
brankou	branka	k1gFnSc7	branka
a	a	k8xC	a
děda	děda	k1gMnSc1	děda
útočníkem	útočník	k1gMnSc7	útočník
střílející	střílející	k2eAgInSc4d1	střílející
tenisový	tenisový	k2eAgInSc4d1	tenisový
míč	míč	k1gInSc4	míč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
začal	začít	k5eAaPmAgInS	začít
chodit	chodit	k5eAaImF	chodit
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
dědy	děda	k1gMnSc2	děda
na	na	k7c4	na
zimní	zimní	k2eAgInSc4d1	zimní
stadión	stadión	k1gInSc4	stadión
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Tesly	Tesla	k1gFnSc2	Tesla
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
stali	stát	k5eAaPmAgMnP	stát
vzorem	vzor	k1gInSc7	vzor
brankáři	brankář	k1gMnPc1	brankář
jako	jako	k9	jako
Miroslav	Miroslav	k1gMnSc1	Miroslav
Lacký	Lacký	k2eAgMnSc1d1	Lacký
či	či	k8xC	či
Jiří	Jiří	k1gMnSc1	Jiří
Crha	Crha	k1gMnSc1	Crha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
jej	on	k3xPp3gMnSc4	on
rodiče	rodič	k1gMnPc1	rodič
přihlásili	přihlásit	k5eAaPmAgMnP	přihlásit
do	do	k7c2	do
pardubické	pardubický	k2eAgFnSc2d1	pardubická
přípravky	přípravka	k1gFnSc2	přípravka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
"	"	kIx"	"
<g/>
profesionálním	profesionální	k2eAgInSc7d1	profesionální
<g/>
"	"	kIx"	"
hokejem	hokej	k1gInSc7	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
jsou	být	k5eAaImIp3nP	být
pardubičtí	pardubický	k2eAgMnPc1d1	pardubický
mladší	mladý	k2eAgMnPc1d2	mladší
žáci	žák	k1gMnPc1	žák
v	v	k7c6	v
brance	branka	k1gFnSc6	branka
s	s	k7c7	s
Dominikem	Dominik	k1gMnSc7	Dominik
k	k	k7c3	k
neporažení	neporažení	k1gNnSc3	neporažení
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výběr	výběr	k1gInSc1	výběr
vydrží	vydržet	k5eAaPmIp3nS	vydržet
pohromadě	pohromadě	k6eAd1	pohromadě
až	až	k9	až
do	do	k7c2	do
dorostu	dorost	k1gInSc2	dorost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
vyhraje	vyhrát	k5eAaPmIp3nS	vyhrát
československý	československý	k2eAgInSc1d1	československý
přebor	přebor	k1gInSc1	přebor
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
třináctiletý	třináctiletý	k2eAgMnSc1d1	třináctiletý
Dominik	Dominik	k1gMnSc1	Dominik
vybrán	vybrat	k5eAaPmNgMnS	vybrat
do	do	k7c2	do
reprezentačního	reprezentační	k2eAgInSc2d1	reprezentační
týmu	tým	k1gInSc2	tým
"	"	kIx"	"
<g/>
šestnáctiletých	šestnáctiletý	k2eAgNnPc2d1	šestnáctileté
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
odehrál	odehrát	k5eAaPmAgMnS	odehrát
první	první	k4xOgInSc4	první
zápas	zápas	k1gInSc4	zápas
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
reprezentaci	reprezentace	k1gFnSc6	reprezentace
-	-	kIx~	-
přátelské	přátelský	k2eAgNnSc1d1	přátelské
utkání	utkání	k1gNnSc1	utkání
v	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
a	a	k8xC	a
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
turnaje	turnaj	k1gInSc2	turnaj
v	v	k7c6	v
sovětském	sovětský	k2eAgNnSc6d1	sovětské
Togliatti	Togliatti	k1gNnSc6	Togliatti
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
si	se	k3xPyFc3	se
odváží	odvážet	k5eAaImIp3nP	odvážet
trofej	trofej	k1gFnSc4	trofej
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
gólmana	gólman	k1gMnSc4	gólman
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
získává	získávat	k5eAaImIp3nS	získávat
zkušenosti	zkušenost	k1gFnPc4	zkušenost
i	i	k9	i
s	s	k7c7	s
výběrem	výběr	k1gInSc7	výběr
"	"	kIx"	"
<g/>
sedmnáctiletých	sedmnáctiletý	k2eAgInPc2d1	sedmnáctiletý
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
osmnáctiletých	osmnáctiletý	k2eAgFnPc2d1	osmnáctiletá
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
se	se	k3xPyFc4	se
v	v	k7c6	v
seniorském	seniorský	k2eAgInSc6d1	seniorský
týmu	tým	k1gInSc6	tým
Pardubic	Pardubice	k1gInPc2	Pardubice
zranila	zranit	k5eAaPmAgFnS	zranit
brankářská	brankářský	k2eAgFnSc1d1	brankářská
jednička	jednička	k1gFnSc1	jednička
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Radvanovský	Radvanovský	k2eAgMnSc1d1	Radvanovský
<g/>
,	,	kIx,	,
do	do	k7c2	do
zápasů	zápas	k1gInPc2	zápas
musel	muset	k5eAaImAgMnS	muset
nastoupit	nastoupit	k5eAaPmF	nastoupit
druhý	druhý	k4xOgMnSc1	druhý
brankář	brankář	k1gMnSc1	brankář
Milan	Milan	k1gMnSc1	Milan
Kečkeš	Kečkeš	k1gMnSc1	Kečkeš
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
Zdeňkem	Zdeněk	k1gMnSc7	Zdeněk
Uhrem	Uher	k1gMnSc7	Uher
povolán	povolat	k5eAaPmNgMnS	povolat
šestnáctiletý	šestnáctiletý	k2eAgMnSc1d1	šestnáctiletý
Dominik	Dominik	k1gMnSc1	Dominik
Hašek	Hašek	k1gMnSc1	Hašek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
posadil	posadit	k5eAaPmAgInS	posadit
na	na	k7c4	na
lavici	lavice	k1gFnSc4	lavice
střídačky	střídačka	k1gFnSc2	střídačka
<g/>
.	.	kIx.	.
</s>
<s>
Svoji	svůj	k3xOyFgFnSc4	svůj
šanci	šance	k1gFnSc4	šance
dostal	dostat	k5eAaPmAgMnS	dostat
ihned	ihned	k6eAd1	ihned
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
zápase	zápas	k1gInSc6	zápas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
hrála	hrát	k5eAaImAgFnS	hrát
Tesla	Tesla	k1gFnSc1	Tesla
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	s	k7c7	s
Spartou	Sparta	k1gFnSc7	Sparta
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
třetinách	třetina	k1gFnPc6	třetina
hosté	host	k1gMnPc1	host
prohrávali	prohrávat	k5eAaImAgMnP	prohrávat
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
Dominik	Dominik	k1gMnSc1	Dominik
poslán	poslán	k2eAgInSc4d1	poslán
mezi	mezi	k7c4	mezi
tyče	tyč	k1gFnPc4	tyč
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
jako	jako	k9	jako
šestnáctiletý	šestnáctiletý	k2eAgMnSc1d1	šestnáctiletý
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
ligové	ligový	k2eAgFnSc6d1	ligová
premiéře	premiéra	k1gFnSc6	premiéra
nezabránil	zabránit	k5eNaPmAgInS	zabránit
dalším	další	k2eAgNnSc7d1	další
dvěma	dva	k4xCgFnPc7	dva
gólům	gól	k1gInPc3	gól
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
Pardubice	Pardubice	k1gInPc1	Pardubice
z	z	k7c2	z
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
vracely	vracet	k5eAaImAgInP	vracet
s	s	k7c7	s
prohrou	prohra	k1gFnSc7	prohra
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
zápasem	zápas	k1gInSc7	zápas
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
Dominik	Dominik	k1gMnSc1	Dominik
dostal	dostat	k5eAaPmAgMnS	dostat
šanci	šance	k1gFnSc4	šance
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
střetnutí	střetnutí	k1gNnSc1	střetnutí
v	v	k7c6	v
Gottwaldově	Gottwaldov	k1gInSc6	Gottwaldov
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přestože	přestože	k8xS	přestože
nastoupení	nastoupení	k1gNnSc1	nastoupení
tak	tak	k8xC	tak
mladého	mladý	k2eAgMnSc4d1	mladý
hráče	hráč	k1gMnSc4	hráč
v	v	k7c6	v
důležité	důležitý	k2eAgFnSc6d1	důležitá
pozici	pozice	k1gFnSc6	pozice
by	by	kYmCp3nS	by
pro	pro	k7c4	pro
Pardubice	Pardubice	k1gInPc4	Pardubice
mohlo	moct	k5eAaImAgNnS	moct
znamenat	znamenat	k5eAaImF	znamenat
velké	velký	k2eAgFnPc4d1	velká
potíže	potíž	k1gFnPc4	potíž
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
vyplatilo	vyplatit	k5eAaPmAgNnS	vyplatit
a	a	k8xC	a
Tesla	Tesla	k1gFnSc1	Tesla
tentokrát	tentokrát	k6eAd1	tentokrát
slavila	slavit	k5eAaImAgFnS	slavit
vítězství	vítězství	k1gNnSc4	vítězství
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vánoční	vánoční	k2eAgFnSc6d1	vánoční
přestávce	přestávka	k1gFnSc6	přestávka
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
z	z	k7c2	z
marodky	marodka	k1gFnSc2	marodka
Radvanovský	Radvanovský	k2eAgMnSc1d1	Radvanovský
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Hašek	Hašek	k1gMnSc1	Hašek
plnil	plnit	k5eAaImAgMnS	plnit
spíše	spíše	k9	spíše
funkci	funkce	k1gFnSc4	funkce
třetího	třetí	k4xOgMnSc2	třetí
brankáře	brankář	k1gMnSc2	brankář
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
i	i	k9	i
přesto	přesto	k8xC	přesto
celkově	celkově	k6eAd1	celkově
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
ve	v	k7c6	v
dvanácti	dvanáct	k4xCc6	dvanáct
zápasech	zápas	k1gInPc6	zápas
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
obdržených	obdržený	k2eAgFnPc2d1	obdržená
branek	branka	k1gFnPc2	branka
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
3,09	[number]	k4	3,09
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1982	[number]	k4	1982
<g/>
/	/	kIx~	/
<g/>
1983	[number]	k4	1983
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
z	z	k7c2	z
Haška	Hašek	k1gMnSc2	Hašek
brankářská	brankářský	k2eAgFnSc1d1	brankářská
dvojka	dvojka	k1gFnSc1	dvojka
za	za	k7c7	za
Radvanovským	Radvanovský	k2eAgInSc7d1	Radvanovský
<g/>
.	.	kIx.	.
</s>
<s>
Kečkeš	Kečkat	k5eAaBmIp2nS	Kečkat
byl	být	k5eAaImAgInS	být
uvolněn	uvolnit	k5eAaPmNgMnS	uvolnit
do	do	k7c2	do
Bratislavy	Bratislava	k1gFnSc2	Bratislava
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
Hašek	Hašek	k1gMnSc1	Hašek
propracovává	propracovávat	k5eAaImIp3nS	propracovávat
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
brankářské	brankářský	k2eAgFnSc2d1	brankářská
jedničky	jednička	k1gFnSc2	jednička
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
šestnáctého	šestnáctý	k4xOgNnSc2	šestnáctý
kola	kolo	k1gNnSc2	kolo
vyhrály	vyhrát	k5eAaPmAgFnP	vyhrát
Pardubice	Pardubice	k1gInPc4	Pardubice
díky	díky	k7c3	díky
Haškovi	Hašek	k1gMnSc3	Hašek
jedenáct	jedenáct	k4xCc1	jedenáct
zápasů	zápas	k1gInPc2	zápas
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
a	a	k8xC	a
umístily	umístit	k5eAaPmAgFnP	umístit
se	se	k3xPyFc4	se
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
tabulky	tabulka	k1gFnSc2	tabulka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
přítelem	přítel	k1gMnSc7	přítel
František	František	k1gMnSc1	František
Musil	Musil	k1gMnSc1	Musil
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgMnSc7	který
se	se	k3xPyFc4	se
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
setkává	setkávat	k5eAaImIp3nS	setkávat
již	již	k6eAd1	již
od	od	k7c2	od
dorostu	dorost	k1gInSc2	dorost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
dostává	dostávat	k5eAaImIp3nS	dostávat
Hašek	Hašek	k1gMnSc1	Hašek
první	první	k4xOgFnSc4	první
pozvánku	pozvánka	k1gFnSc4	pozvánka
na	na	k7c4	na
seniorské	seniorský	k2eAgNnSc4d1	seniorské
mezistátní	mezistátní	k2eAgNnSc4d1	mezistátní
utkání	utkání	k1gNnSc4	utkání
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
vystoupení	vystoupení	k1gNnSc1	vystoupení
za	za	k7c4	za
národní	národní	k2eAgInSc4d1	národní
tým	tým	k1gInSc4	tým
si	se	k3xPyFc3	se
odbyl	odbýt	k5eAaPmAgInS	odbýt
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1983	[number]	k4	1983
v	v	k7c6	v
přátelském	přátelský	k2eAgNnSc6d1	přátelské
utkání	utkání	k1gNnSc6	utkání
se	s	k7c7	s
Švédskem	Švédsko	k1gNnSc7	Švédsko
v	v	k7c6	v
Göteborgu	Göteborg	k1gInSc6	Göteborg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
této	tento	k3xDgFnSc2	tento
premiéry	premiéra	k1gFnSc2	premiéra
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
osmnáct	osmnáct	k4xCc4	osmnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejmladším	mladý	k2eAgMnSc7d3	nejmladší
brankářem	brankář	k1gMnSc7	brankář
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
reprezentace	reprezentace	k1gFnSc2	reprezentace
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zápas	zápas	k1gInSc1	zápas
však	však	k9	však
prohraje	prohrát	k5eAaPmIp3nS	prohrát
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
i	i	k9	i
přesto	přesto	k8xC	přesto
obdrží	obdržet	k5eAaPmIp3nP	obdržet
cenu	cena	k1gFnSc4	cena
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
hráče	hráč	k1gMnSc4	hráč
zápasu	zápas	k1gInSc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
zápasech	zápas	k1gInPc6	zápas
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
také	také	k9	také
nasazen	nasadit	k5eAaPmNgInS	nasadit
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
oslavoval	oslavovat	k5eAaImAgMnS	oslavovat
výhry	výhra	k1gFnPc4	výhra
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
opětovný	opětovný	k2eAgInSc4d1	opětovný
střet	střet	k1gInSc4	střet
se	s	k7c7	s
Švédskem	Švédsko	k1gNnSc7	Švédsko
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Kanadou	Kanada	k1gFnSc7	Kanada
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
Finskem	Finsko	k1gNnSc7	Finsko
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
těchto	tento	k3xDgFnPc6	tento
zkušenostech	zkušenost	k1gFnPc6	zkušenost
byl	být	k5eAaImAgInS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
světový	světový	k2eAgInSc4d1	světový
šampionát	šampionát	k1gInSc4	šampionát
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
plánu	plán	k1gInSc6	plán
<g/>
,	,	kIx,	,
Hašek	Hašek	k1gMnSc1	Hašek
plní	plnit	k5eAaImIp3nS	plnit
funkci	funkce	k1gFnSc4	funkce
druhého	druhý	k4xOgMnSc2	druhý
brankáře	brankář	k1gMnSc2	brankář
po	po	k7c6	po
Jiřím	Jiří	k1gMnSc6	Jiří
Králíkovi	Králík	k1gMnSc6	Králík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
šampionátu	šampionát	k1gInSc6	šampionát
odehrál	odehrát	k5eAaPmAgInS	odehrát
dva	dva	k4xCgInPc4	dva
zápasy	zápas	k1gInPc4	zápas
<g/>
:	:	kIx,	:
se	s	k7c7	s
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
sbornou	sborný	k2eAgFnSc7d1	sborná
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
odnesl	odnést	k5eAaPmAgMnS	odnést
pětibrankové	pětibrankový	k2eAgNnSc4d1	pětibrankové
manko	manko	k1gNnSc4	manko
a	a	k8xC	a
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
zápase	zápas	k1gInSc6	zápas
s	s	k7c7	s
italským	italský	k2eAgInSc7d1	italský
výběrem	výběr	k1gInSc7	výběr
si	se	k3xPyFc3	se
naopak	naopak	k6eAd1	naopak
připsal	připsat	k5eAaPmAgMnS	připsat
první	první	k4xOgMnSc1	první
shutout	shutout	k1gMnSc1	shutout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ostatních	ostatní	k2eAgInPc6d1	ostatní
zápasech	zápas	k1gInPc6	zápas
již	již	k6eAd1	již
nastupoval	nastupovat	k5eAaImAgMnS	nastupovat
Králík	Králík	k1gMnSc1	Králík
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
tento	tento	k3xDgInSc1	tento
šampionát	šampionát	k1gInSc1	šampionát
znamenal	znamenat	k5eAaImAgInS	znamenat
pro	pro	k7c4	pro
Dominika	Dominik	k1gMnSc4	Dominik
velkou	velký	k2eAgFnSc4d1	velká
zkušenost	zkušenost	k1gFnSc4	zkušenost
a	a	k8xC	a
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
nastávající	nastávající	k2eAgFnSc6d1	nastávající
sezóně	sezóna	k1gFnSc6	sezóna
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
Hašek	Hašek	k1gMnSc1	Hašek
již	již	k6eAd1	již
jako	jako	k8xS	jako
jednička	jednička	k1gFnSc1	jednička
Tesly	Tesla	k1gFnSc2	Tesla
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
draftu	draft	k1gInSc6	draft
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1983	[number]	k4	1983
byl	být	k5eAaImAgMnS	být
Dominik	Dominik	k1gMnSc1	Dominik
vybrán	vybrat	k5eAaPmNgMnS	vybrat
v	v	k7c4	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
kole	kolo	k1gNnSc6	kolo
na	na	k7c6	na
celkově	celkově	k6eAd1	celkově
199	[number]	k4	199
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
týmem	tým	k1gInSc7	tým
Chicago	Chicago	k1gNnSc4	Chicago
Blackhawks	Blackhawksa	k1gFnPc2	Blackhawksa
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
zranil	zranit	k5eAaPmAgMnS	zranit
Králík	Králík	k1gMnSc1	Králík
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Hašek	Hašek	k1gMnSc1	Hašek
Bukačem	bukač	k1gMnSc7	bukač
vybrán	vybrat	k5eAaPmNgMnS	vybrat
na	na	k7c4	na
prosincovou	prosincový	k2eAgFnSc4d1	prosincová
Cenu	cena	k1gFnSc4	cena
Izvestijí	Izvestijí	k1gFnSc2	Izvestijí
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
na	na	k7c4	na
lednovou	lednový	k2eAgFnSc4d1	lednová
předolympijskou	předolympijský	k2eAgFnSc4d1	předolympijská
přípravu	příprava	k1gFnSc4	příprava
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
měl	mít	k5eAaImAgMnS	mít
Hašek	Hašek	k1gMnSc1	Hašek
sen	sena	k1gFnPc2	sena
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
překazil	překazit	k5eAaPmAgMnS	překazit
mu	on	k3xPp3gMnSc3	on
je	on	k3xPp3gInPc4	on
zotavený	zotavený	k2eAgMnSc1d1	zotavený
Králík	Králík	k1gMnSc1	Králík
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jej	on	k3xPp3gMnSc4	on
rozladilo	rozladit	k5eAaPmAgNnS	rozladit
a	a	k8xC	a
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
sezóny	sezóna	k1gFnSc2	sezóna
musel	muset	k5eAaImAgMnS	muset
třikrát	třikrát	k6eAd1	třikrát
nastoupit	nastoupit	k5eAaPmF	nastoupit
jen	jen	k9	jen
jako	jako	k9	jako
náhradník	náhradník	k1gMnSc1	náhradník
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Hašek	Hašek	k1gMnSc1	Hašek
pozvánku	pozvánka	k1gFnSc4	pozvánka
na	na	k7c4	na
Kanadský	kanadský	k2eAgInSc4d1	kanadský
pohár	pohár	k1gInSc4	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
pro	pro	k7c4	pro
československý	československý	k2eAgInSc4d1	československý
tým	tým	k1gInSc4	tým
znamenaly	znamenat	k5eAaImAgFnP	znamenat
4	[number]	k4	4
utkání	utkání	k1gNnSc2	utkání
prohru	prohra	k1gFnSc4	prohra
<g/>
,	,	kIx,	,
jen	jen	k9	jen
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
remíza	remíza	k1gFnSc1	remíza
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
končí	končit	k5eAaImIp3nS	končit
celkově	celkově	k6eAd1	celkově
na	na	k7c6	na
šestém	šestý	k4xOgMnSc6	šestý
<g/>
,	,	kIx,	,
posledním	poslední	k2eAgNnSc6d1	poslední
<g/>
,	,	kIx,	,
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
Kanady	Kanada	k1gFnSc2	Kanada
na	na	k7c4	na
Haška	Hašek	k1gMnSc4	Hašek
čeká	čekat	k5eAaImIp3nS	čekat
nový	nový	k2eAgMnSc1d1	nový
trenér	trenér	k1gMnSc1	trenér
-	-	kIx~	-
Karel	Karel	k1gMnSc1	Karel
Franěk	Franěk	k1gMnSc1	Franěk
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
asistent	asistent	k1gMnSc1	asistent
Bukače	bukač	k1gMnSc4	bukač
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přestože	přestože	k8xS	přestože
Pardubice	Pardubice	k1gInPc1	Pardubice
končí	končit	k5eAaImIp3nP	končit
celkově	celkově	k6eAd1	celkově
na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
Dominik	Dominik	k1gMnSc1	Dominik
měl	mít	k5eAaImAgMnS	mít
tuto	tento	k3xDgFnSc4	tento
sezónu	sezóna	k1gFnSc4	sezóna
nejhorší	zlý	k2eAgInSc4d3	Nejhorší
průměr	průměr	k1gInSc4	průměr
branek	branka	k1gFnPc2	branka
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
v	v	k7c6	v
československé	československý	k2eAgFnSc6d1	Československá
lize	liga	k1gFnSc6	liga
-	-	kIx~	-
3,25	[number]	k4	3,25
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
velkou	velký	k2eAgFnSc4d1	velká
snahu	snaha	k1gFnSc4	snaha
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
šampionát	šampionát	k1gInSc4	šampionát
konající	konající	k2eAgInSc4d1	konající
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
měli	mít	k5eAaImAgMnP	mít
zkušení	zkušený	k2eAgMnPc1d1	zkušený
gólmani	gólman	k1gMnPc1	gólman
Jiří	Jiří	k1gMnSc1	Jiří
Králík	Králík	k1gMnSc1	Králík
a	a	k8xC	a
Jaromír	Jaromír	k1gMnSc1	Jaromír
Šindel	šindel	k1gInSc4	šindel
přednost	přednost	k1gFnSc4	přednost
před	před	k7c7	před
mladíkem	mladík	k1gInSc7	mladík
z	z	k7c2	z
Pardubic	Pardubice	k1gInPc2	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1985	[number]	k4	1985
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
vybrán	vybrat	k5eAaPmNgMnS	vybrat
do	do	k7c2	do
reprezentačního	reprezentační	k2eAgInSc2d1	reprezentační
týmu	tým	k1gInSc2	tým
pro	pro	k7c4	pro
Cenu	cena	k1gFnSc4	cena
Izvestijí	Izvestijí	k1gFnSc2	Izvestijí
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
si	se	k3xPyFc3	se
odvezl	odvézt	k5eAaPmAgMnS	odvézt
hlavní	hlavní	k2eAgFnSc4d1	hlavní
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
však	však	k9	však
českému	český	k2eAgMnSc3d1	český
týmu	tým	k1gInSc2	tým
vůbec	vůbec	k9	vůbec
nevedlo	vést	k5eNaImAgNnS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
výhra	výhra	k1gFnSc1	výhra
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
remíza	remíza	k1gFnSc1	remíza
stačily	stačit	k5eAaBmAgFnP	stačit
k	k	k7c3	k
celkovému	celkový	k2eAgNnSc3d1	celkové
pátému	pátý	k4xOgNnSc3	pátý
místu	místo	k1gNnSc3	místo
<g/>
.	.	kIx.	.
</s>
<s>
Pardubice	Pardubice	k1gInPc4	Pardubice
směřující	směřující	k2eAgInPc4d1	směřující
k	k	k7c3	k
titulu	titul	k1gInSc3	titul
mistrů	mistr	k1gMnPc2	mistr
však	však	k9	však
vypadly	vypadnout	k5eAaPmAgInP	vypadnout
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
playoff	playoff	k1gInSc4	playoff
s	s	k7c7	s
Košicemi	Košice	k1gInPc7	Košice
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgInS	být
alespoň	alespoň	k9	alespoň
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
brankářem	brankář	k1gMnSc7	brankář
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Pardubice	Pardubice	k1gInPc4	Pardubice
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
sezóna	sezóna	k1gFnSc1	sezóna
mistrovská	mistrovský	k2eAgFnSc1d1	mistrovská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domácích	domácí	k2eAgInPc6d1	domácí
zápasech	zápas	k1gInPc6	zápas
ztratili	ztratit	k5eAaPmAgMnP	ztratit
hokejisté	hokejista	k1gMnPc1	hokejista
Tesly	Tesla	k1gFnSc2	Tesla
pouhé	pouhý	k2eAgInPc4d1	pouhý
tři	tři	k4xCgInPc4	tři
body	bod	k1gInPc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zápasech	zápas	k1gInPc6	zápas
odehrávajících	odehrávající	k2eAgFnPc2d1	odehrávající
se	se	k3xPyFc4	se
na	na	k7c6	na
cizí	cizí	k2eAgFnSc6d1	cizí
půdě	půda	k1gFnSc6	půda
byli	být	k5eAaImAgMnP	být
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
horší	zlý	k2eAgNnSc4d2	horší
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
Pardubice	Pardubice	k1gInPc1	Pardubice
zakončily	zakončit	k5eAaPmAgInP	zakončit
vítězně	vítězně	k6eAd1	vítězně
základní	základní	k2eAgFnSc4d1	základní
část	část	k1gFnSc4	část
s	s	k7c7	s
pětibodovým	pětibodový	k2eAgInSc7d1	pětibodový
náskokem	náskok	k1gInSc7	náskok
nad	nad	k7c7	nad
Spartou	Sparta	k1gFnSc7	Sparta
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc7	čtvrtfinále
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
pardubičtí	pardubický	k2eAgMnPc1d1	pardubický
proti	proti	k7c3	proti
Gottwaldovu	Gottwaldov	k1gInSc3	Gottwaldov
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
vyřazen	vyřazen	k2eAgMnSc1d1	vyřazen
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
zápasech	zápas	k1gInPc6	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Semifinále	semifinále	k1gNnSc1	semifinále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
střetli	střetnout	k5eAaPmAgMnP	střetnout
se	s	k7c7	s
slovenskými	slovenský	k2eAgInPc7d1	slovenský
Košicemi	Košice	k1gInPc7	Košice
<g/>
,	,	kIx,	,
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
titul	titul	k1gInSc4	titul
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
střetli	střetnout	k5eAaPmAgMnP	střetnout
Pardubice	Pardubice	k1gInPc4	Pardubice
a	a	k8xC	a
Dukla	Dukla	k1gFnSc1	Dukla
Jihlava	Jihlava	k1gFnSc1	Jihlava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
zápase	zápas	k1gInSc6	zápas
excelovali	excelovat	k5eAaImAgMnP	excelovat
pardubičtí	pardubický	k2eAgMnPc1d1	pardubický
hráči	hráč	k1gMnPc1	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
výsledek	výsledek	k1gInSc1	výsledek
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
gardu	gard	k1gInSc6	gard
<g/>
,	,	kIx,	,
přinesl	přinést	k5eAaPmAgInS	přinést
i	i	k9	i
druhý	druhý	k4xOgInSc1	druhý
zápas	zápas	k1gInSc1	zápas
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
pátém	pátý	k4xOgInSc6	pátý
gólu	gól	k1gInSc6	gól
vystřídán	vystřídán	k2eAgInSc4d1	vystřídán
náhradníkem	náhradník	k1gMnSc7	náhradník
Michalem	Michal	k1gMnSc7	Michal
Kopačkou	kopačka	k1gFnSc7	kopačka
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
zápas	zápas	k1gInSc1	zápas
znamenal	znamenat	k5eAaImAgInS	znamenat
pro	pro	k7c4	pro
Pardubice	Pardubice	k1gInPc4	Pardubice
těsné	těsný	k2eAgNnSc4d1	těsné
vítězství	vítězství	k1gNnSc4	vítězství
a	a	k8xC	a
pro	pro	k7c4	pro
Dominika	Dominik	k1gMnSc4	Dominik
Haška	Hašek	k1gMnSc4	Hašek
přímo	přímo	k6eAd1	přímo
čisté	čistý	k2eAgNnSc1d1	čisté
konto	konto	k1gNnSc1	konto
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	k9	aby
střídání	střídání	k1gNnSc1	střídání
výsledků	výsledek	k1gInPc2	výsledek
bylo	být	k5eAaImAgNnS	být
celkové	celkový	k2eAgNnSc1d1	celkové
<g/>
,	,	kIx,	,
Pardubice	Pardubice	k1gInPc1	Pardubice
ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
finálovém	finálový	k2eAgInSc6d1	finálový
zápase	zápas	k1gInSc6	zápas
prohrály	prohrát	k5eAaPmAgFnP	prohrát
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
muselo	muset	k5eAaImAgNnS	muset
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
v	v	k7c6	v
pátém	pátý	k4xOgInSc6	pátý
posledním	poslední	k2eAgInSc6d1	poslední
zápase	zápas	k1gInSc6	zápas
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
pardubičtí	pardubický	k2eAgMnPc1d1	pardubický
hráči	hráč	k1gMnPc1	hráč
vyhrávali	vyhrávat	k5eAaImAgMnP	vyhrávat
<g/>
,	,	kIx,	,
po	po	k7c6	po
první	první	k4xOgFnSc6	první
třetině	třetina	k1gFnSc6	třetina
odešli	odejít	k5eAaPmAgMnP	odejít
do	do	k7c2	do
kabin	kabina	k1gFnPc2	kabina
s	s	k7c7	s
jednogólovou	jednogólový	k2eAgFnSc7d1	jednogólová
ztrátou	ztráta	k1gFnSc7	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
druhé	druhý	k4xOgFnSc2	druhý
třetiny	třetina	k1gFnSc2	třetina
zápasu	zápas	k1gInSc2	zápas
pardubičtí	pardubický	k2eAgMnPc1d1	pardubický
srovnali	srovnat	k5eAaPmAgMnP	srovnat
a	a	k8xC	a
do	do	k7c2	do
konce	konec	k1gInSc2	konec
celého	celý	k2eAgInSc2d1	celý
zápasu	zápas	k1gInSc2	zápas
se	se	k3xPyFc4	se
skóre	skóre	k1gNnSc1	skóre
již	již	k6eAd1	již
nezměnilo	změnit	k5eNaPmAgNnS	změnit
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
sehráno	sehrát	k5eAaPmNgNnS	sehrát
prodloužení	prodloužení	k1gNnSc1	prodloužení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
skončilo	skončit	k5eAaPmAgNnS	skončit
po	po	k7c6	po
jedenácti	jedenáct	k4xCc6	jedenáct
minutách	minuta	k1gFnPc6	minuta
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
pardubických	pardubický	k2eAgMnPc2d1	pardubický
trestal	trestat	k5eAaImAgMnS	trestat
chyby	chyba	k1gFnPc4	chyba
jihlavské	jihlavský	k2eAgFnSc2d1	Jihlavská
obrany	obrana	k1gFnSc2	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
dvaadvaceti	dvaadvacet	k4xCc6	dvaadvacet
letech	léto	k1gNnPc6	léto
si	se	k3xPyFc3	se
připsal	připsat	k5eAaPmAgInS	připsat
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
brankářem	brankář	k1gMnSc7	brankář
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
ligovém	ligový	k2eAgInSc6d1	ligový
triumfu	triumf	k1gInSc6	triumf
se	se	k3xPyFc4	se
přemístil	přemístit	k5eAaPmAgInS	přemístit
do	do	k7c2	do
sousedního	sousední	k2eAgNnSc2d1	sousední
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výhrou	výhra	k1gFnSc7	výhra
nad	nad	k7c7	nad
Finy	Fin	k1gMnPc7	Fin
<g/>
,	,	kIx,	,
remízou	remíza	k1gFnSc7	remíza
s	s	k7c7	s
Kanaďany	Kanaďan	k1gMnPc7	Kanaďan
a	a	k8xC	a
prohrou	prohra	k1gFnSc7	prohra
se	se	k3xPyFc4	se
Sověty	Sovět	k1gMnPc4	Sovět
si	se	k3xPyFc3	se
zajistili	zajistit	k5eAaPmAgMnP	zajistit
postup	postup	k1gInSc4	postup
mezi	mezi	k7c4	mezi
finálovou	finálový	k2eAgFnSc4d1	finálová
čtyřku	čtyřka	k1gFnSc4	čtyřka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
zápas	zápas	k1gInSc1	zápas
se	se	k3xPyFc4	se
Švédy	švéda	k1gFnSc2	švéda
skončil	skončit	k5eAaPmAgInS	skončit
remízou	remíza	k1gFnSc7	remíza
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Následný	následný	k2eAgInSc1d1	následný
zápas	zápas	k1gInSc1	zápas
se	s	k7c7	s
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
sbornou	sborný	k2eAgFnSc7d1	sborná
znamenal	znamenat	k5eAaImAgMnS	znamenat
pro	pro	k7c4	pro
naše	náš	k3xOp1gMnPc4	náš
hokejisty	hokejista	k1gMnPc4	hokejista
mnoho	mnoho	k6eAd1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Výhra	výhra	k1gFnSc1	výhra
by	by	kYmCp3nS	by
znamenala	znamenat	k5eAaImAgFnS	znamenat
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
šampionátu	šampionát	k1gInSc6	šampionát
<g/>
,	,	kIx,	,
remíza	remíza	k1gFnSc1	remíza
či	či	k8xC	či
prohra	prohra	k1gFnSc1	prohra
by	by	kYmCp3nS	by
nás	my	k3xPp1nPc4	my
posunula	posunout	k5eAaPmAgFnS	posunout
na	na	k7c4	na
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
<g/>
,	,	kIx,	,
či	či	k8xC	či
bronzové	bronzový	k2eAgNnSc1d1	bronzové
umístění	umístění	k1gNnSc1	umístění
<g/>
.	.	kIx.	.
</s>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Stavjaňa	Stavjaňa	k1gMnSc1	Stavjaňa
otevřel	otevřít	k5eAaPmAgMnS	otevřít
skóre	skóre	k1gNnSc4	skóre
zápasu	zápas	k1gInSc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
53	[number]	k4	53
<g/>
.	.	kIx.	.
minuty	minuta	k1gFnPc1	minuta
se	se	k3xPyFc4	se
skóre	skóre	k1gNnSc2	skóre
nezměnilo	změnit	k5eNaPmAgNnS	změnit
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
však	však	k9	však
nestačil	stačit	k5eNaBmAgInS	stačit
na	na	k7c4	na
střelu	střela	k1gFnSc4	střela
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Krutova	Krutův	k2eAgMnSc2d1	Krutův
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
ani	ani	k8xC	ani
na	na	k7c4	na
Igora	Igor	k1gMnSc4	Igor
Stělnova	Stělnov	k1gInSc2	Stělnov
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
prohra	prohra	k1gFnSc1	prohra
československý	československý	k2eAgInSc4d1	československý
výběr	výběr	k1gInSc4	výběr
odsoudila	odsoudit	k5eAaPmAgFnS	odsoudit
na	na	k7c4	na
konečné	konečný	k2eAgNnSc4d1	konečné
bronzové	bronzový	k2eAgNnSc4d1	bronzové
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
byl	být	k5eAaImAgMnS	být
Dominik	Dominik	k1gMnSc1	Dominik
Hašek	Hašek	k1gMnSc1	Hašek
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
brankářem	brankář	k1gMnSc7	brankář
turnaje	turnaj	k1gInSc2	turnaj
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
do	do	k7c2	do
All-Star	All-Stara	k1gFnPc2	All-Stara
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
turnaje	turnaj	k1gInSc2	turnaj
jej	on	k3xPp3gMnSc4	on
kontaktovali	kontaktovat	k5eAaImAgMnP	kontaktovat
zástupci	zástupce	k1gMnPc1	zástupce
draftujícího	draftující	k2eAgNnSc2d1	draftující
Chicaga	Chicago	k1gNnSc2	Chicago
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mu	on	k3xPp3gMnSc3	on
nabídli	nabídnout	k5eAaPmAgMnP	nabídnout
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
však	však	k9	však
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rok	rok	k1gInSc1	rok
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
vítězem	vítěz	k1gMnSc7	vítěz
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
hokejky	hokejka	k1gFnSc2	hokejka
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k3yQnSc1	co
obdržel	obdržet	k5eAaPmAgInS	obdržet
970	[number]	k4	970
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
o	o	k7c4	o
208	[number]	k4	208
hlasů	hlas	k1gInPc2	hlas
více	hodně	k6eAd2	hodně
než	než	k8xS	než
druhý	druhý	k4xOgMnSc1	druhý
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
Jiří	Jiří	k1gMnSc1	Jiří
Hrdina	Hrdina	k1gMnSc1	Hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
1987	[number]	k4	1987
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
srpen	srpen	k1gInSc4	srpen
naplánován	naplánovat	k5eAaBmNgInS	naplánovat
Kanadský	kanadský	k2eAgInSc1d1	kanadský
pohár	pohár	k1gInSc1	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Hašek	Hašek	k1gMnSc1	Hašek
byl	být	k5eAaImAgMnS	být
také	také	k9	také
do	do	k7c2	do
reprezentace	reprezentace	k1gFnSc2	reprezentace
pozván	pozvat	k5eAaPmNgMnS	pozvat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
zápase	zápas	k1gInSc6	zápas
turnaje	turnaj	k1gInSc2	turnaj
se	se	k3xPyFc4	se
československý	československý	k2eAgInSc1d1	československý
výběr	výběr	k1gInSc1	výběr
střetl	střetnout	k5eAaPmAgInS	střetnout
s	s	k7c7	s
Kanadou	Kanada	k1gFnSc7	Kanada
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zápas	zápas	k1gInSc1	zápas
po	po	k7c6	po
velkém	velký	k2eAgInSc6d1	velký
boji	boj	k1gInSc6	boj
zápas	zápas	k1gInSc1	zápas
skončil	skončit	k5eAaPmAgInS	skončit
remízou	remíza	k1gFnSc7	remíza
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
čeští	český	k2eAgMnPc1d1	český
hokejisté	hokejista	k1gMnPc1	hokejista
proti	proti	k7c3	proti
Sovětům	Sovět	k1gMnPc3	Sovět
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zápas	zápas	k1gInSc4	zápas
proměnili	proměnit	k5eAaPmAgMnP	proměnit
spíše	spíše	k9	spíše
v	v	k7c6	v
exhibici	exhibice	k1gFnSc6	exhibice
<g/>
,	,	kIx,	,
po	po	k7c6	po
prohře	prohra	k1gFnSc6	prohra
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
Čechoslováky	Čechoslovák	k1gMnPc7	Čechoslovák
čekal	čekat	k5eAaImAgInS	čekat
stejný	stejný	k2eAgInSc1d1	stejný
výsledek	výsledek	k1gInSc1	výsledek
i	i	k9	i
se	s	k7c7	s
Švédy	Švéd	k1gMnPc7	Švéd
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInPc1d1	následující
zápasy	zápas	k1gInPc1	zápas
s	s	k7c7	s
Finy	Fin	k1gMnPc7	Fin
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
a	a	k8xC	a
Američany	Američan	k1gMnPc4	Američan
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
zachránil	zachránit	k5eAaPmAgInS	zachránit
český	český	k2eAgInSc1d1	český
výběr	výběr	k1gInSc1	výběr
před	před	k7c7	před
ukončením	ukončení	k1gNnSc7	ukončení
šampionátu	šampionát	k1gInSc2	šampionát
bez	bez	k7c2	bez
postupu	postup	k1gInSc2	postup
<g/>
.	.	kIx.	.
</s>
<s>
Češi	Čech	k1gMnPc1	Čech
postoupili	postoupit	k5eAaPmAgMnP	postoupit
ze	z	k7c2	z
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
místa	místo	k1gNnSc2	místo
a	a	k8xC	a
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
se	se	k3xPyFc4	se
střetli	střetnout	k5eAaPmAgMnP	střetnout
s	s	k7c7	s
Kanadou	Kanada	k1gFnSc7	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
třetině	třetina	k1gFnSc6	třetina
vedli	vést	k5eAaImAgMnP	vést
hosté	host	k1gMnPc1	host
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
asi	asi	k9	asi
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
zápasu	zápas	k1gInSc2	zápas
jej	on	k3xPp3gInSc2	on
překonal	překonat	k5eAaPmAgInS	překonat
Dale	Dale	k1gInSc1	Dale
Hawerchuk	Hawerchuko	k1gNnPc2	Hawerchuko
a	a	k8xC	a
po	po	k7c6	po
několika	několik	k4yIc6	několik
sekundách	sekunda	k1gFnPc6	sekunda
dvěma	dva	k4xCgFnPc7	dva
brankami	branka	k1gFnPc7	branka
Mario	Mario	k1gMnSc1	Mario
Lemieux	Lemieux	k1gInSc1	Lemieux
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
třetina	třetina	k1gFnSc1	třetina
skončila	skončit	k5eAaPmAgFnS	skončit
šťastněji	šťastně	k6eAd2	šťastně
pro	pro	k7c4	pro
kanadské	kanadský	k2eAgMnPc4d1	kanadský
hráče	hráč	k1gMnPc4	hráč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
třetí	třetí	k4xOgFnSc2	třetí
třetiny	třetina	k1gFnSc2	třetina
ještě	ještě	k9	ještě
inkasoval	inkasovat	k5eAaBmAgMnS	inkasovat
jeden	jeden	k4xCgInSc4	jeden
gól	gól	k1gInSc4	gól
<g/>
,	,	kIx,	,
pět	pět	k4xCc4	pět
minut	minuta	k1gFnPc2	minuta
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
zápasu	zápas	k1gInSc2	zápas
snížil	snížit	k5eAaPmAgMnS	snížit
David	David	k1gMnSc1	David
Volek	Volek	k1gMnSc1	Volek
na	na	k7c4	na
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
utkání	utkání	k1gNnSc2	utkání
trefou	trefa	k1gFnSc7	trefa
do	do	k7c2	do
prázdné	prázdný	k2eAgFnSc2d1	prázdná
brány	brána	k1gFnSc2	brána
Kanaďan	Kanaďan	k1gMnSc1	Kanaďan
Brian	Brian	k1gMnSc1	Brian
Propp	Propp	k1gMnSc1	Propp
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
výhru	výhra	k1gFnSc4	výhra
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Kanadský	kanadský	k2eAgInSc1d1	kanadský
pohár	pohár	k1gInSc1	pohár
skončil	skončit	k5eAaPmAgInS	skončit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozjela	rozjet	k5eAaPmAgFnS	rozjet
nová	nový	k2eAgFnSc1d1	nová
sezóna	sezóna	k1gFnSc1	sezóna
československé	československý	k2eAgFnSc2d1	Československá
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
potvrdily	potvrdit	k5eAaPmAgInP	potvrdit
pověst	pověst	k1gFnSc4	pověst
mistra	mistr	k1gMnSc2	mistr
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
října	říjen	k1gInSc2	říjen
byla	být	k5eAaImAgNnP	být
naplánována	naplánován	k2eAgNnPc1d1	naplánováno
přátelská	přátelský	k2eAgNnPc1d1	přátelské
utkání	utkání	k1gNnPc1	utkání
s	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
druhém	druhý	k4xOgInSc6	druhý
zápase	zápas	k1gInSc6	zápas
<g/>
,	,	kIx,	,
hraném	hraný	k2eAgNnSc6d1	hrané
v	v	k7c6	v
Gottwaldově	Gottwaldův	k2eAgFnSc6d1	Gottwaldova
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
tisící	tisící	k4xOgInSc4	tisící
zápas	zápas	k1gInSc4	zápas
československé	československý	k2eAgFnSc2d1	Československá
reprezentace	reprezentace	k1gFnSc2	reprezentace
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kariéře	kariéra	k1gFnSc6	kariéra
zranil	zranit	k5eAaPmAgMnS	zranit
-	-	kIx~	-
natrhnutý	natrhnutý	k2eAgInSc4d1	natrhnutý
břišní	břišní	k2eAgInSc4d1	břišní
sval	sval	k1gInSc4	sval
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
to	ten	k3xDgNnSc4	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
jedno	jeden	k4xCgNnSc1	jeden
-	-	kIx~	-
stop	stopa	k1gFnPc2	stopa
do	do	k7c2	do
povánočních	povánoční	k2eAgFnPc2d1	povánoční
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Pardubice	Pardubice	k1gInPc1	Pardubice
bojovaly	bojovat	k5eAaImAgInP	bojovat
o	o	k7c4	o
Spenglerův	Spenglerův	k2eAgInSc4d1	Spenglerův
pohár	pohár	k1gInSc4	pohár
v	v	k7c6	v
Davosu	Davos	k1gInSc6	Davos
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Pardubice	Pardubice	k1gInPc1	Pardubice
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
turnaji	turnaj	k1gInSc6	turnaj
skončily	skončit	k5eAaPmAgInP	skončit
poslední	poslední	k2eAgInPc4d1	poslední
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgMnS	být
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
brankářem	brankář	k1gMnSc7	brankář
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
přípravy	příprava	k1gFnSc2	příprava
na	na	k7c4	na
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
v	v	k7c6	v
Calgary	Calgary	k1gNnSc6	Calgary
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
zápase	zápas	k1gInSc6	zápas
olympijského	olympijský	k2eAgInSc2d1	olympijský
turnaje	turnaj	k1gInSc2	turnaj
proti	proti	k7c3	proti
NSR	NSR	kA	NSR
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
brankářská	brankářský	k2eAgFnSc1d1	brankářská
dvojka	dvojka	k1gFnSc1	dvojka
Jaromír	Jaromír	k1gMnSc1	Jaromír
Šindel	šindel	k1gInSc1	šindel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
již	již	k9	již
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zápas	zápas	k1gInSc1	zápas
patřil	patřit	k5eAaImAgInS	patřit
mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gInPc4	jeho
nejhorší	zlý	k2eAgInPc4d3	Nejhorší
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
reprezentační	reprezentační	k2eAgFnSc6d1	reprezentační
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
:	:	kIx,	:
během	během	k7c2	během
tří	tři	k4xCgFnPc2	tři
minut	minuta	k1gFnPc2	minuta
třikrát	třikrát	k6eAd1	třikrát
inkasoval	inkasovat	k5eAaBmAgInS	inkasovat
a	a	k8xC	a
v	v	k7c6	v
sedmé	sedmý	k4xOgFnSc6	sedmý
minutě	minuta	k1gFnSc6	minuta
by	by	kYmCp3nP	by
vystřídán	vystřídán	k2eAgMnSc1d1	vystřídán
Jaromírem	Jaromír	k1gMnSc7	Jaromír
Šindelem	šindel	k1gInSc7	šindel
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
šanci	šance	k1gFnSc4	šance
dostal	dostat	k5eAaPmAgMnS	dostat
až	až	k9	až
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
se	s	k7c7	s
Sověty	Sovět	k1gMnPc7	Sovět
<g/>
.	.	kIx.	.
</s>
<s>
Šindel	šindel	k1gInSc1	šindel
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
inkasoval	inkasovat	k5eAaBmAgMnS	inkasovat
čtyři	čtyři	k4xCgFnPc4	čtyři
branky	branka	k1gFnPc4	branka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vystřídán	vystřídán	k2eAgInSc1d1	vystřídán
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
inkasoval	inkasovat	k5eAaBmAgInS	inkasovat
ještě	ještě	k6eAd1	ještě
dvakrát	dvakrát	k6eAd1	dvakrát
a	a	k8xC	a
československý	československý	k2eAgInSc1d1	československý
výběr	výběr	k1gInSc1	výběr
prohrál	prohrát	k5eAaPmAgInS	prohrát
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
tým	tým	k1gInSc1	tým
udržel	udržet	k5eAaPmAgInS	udržet
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
postupové	postupový	k2eAgFnSc6d1	postupová
pozici	pozice	k1gFnSc6	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
první	první	k4xOgInSc4	první
zápas	zápas	k1gInSc4	zápas
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
československý	československý	k2eAgInSc4d1	československý
tým	tým	k1gInSc4	tým
nevyvíjel	vyvíjet	k5eNaImAgInS	vyvíjet
dobře	dobře	k6eAd1	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Švédský	švédský	k2eAgInSc1d1	švédský
výběr	výběr	k1gInSc1	výběr
nad	nad	k7c7	nad
Čechoslováky	Čechoslovák	k1gMnPc7	Čechoslovák
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
střetli	střetnout	k5eAaPmAgMnP	střetnout
s	s	k7c7	s
Finskem	Finsko	k1gNnSc7	Finsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zvítězili	zvítězit	k5eAaPmAgMnP	zvítězit
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jedinou	jediný	k2eAgFnSc4d1	jediná
výhru	výhra	k1gFnSc4	výhra
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
o	o	k7c4	o
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc4	třetí
a	a	k8xC	a
poslední	poslední	k2eAgInSc4d1	poslední
zápas	zápas	k1gInSc4	zápas
s	s	k7c7	s
Kanadou	Kanada	k1gFnSc7	Kanada
o	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
skončil	skončit	k5eAaPmAgMnS	skončit
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
šesti	šest	k4xCc7	šest
brankami	branka	k1gFnPc7	branka
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Pardubic	Pardubice	k1gInPc2	Pardubice
<g/>
,	,	kIx,	,
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
všechno	všechen	k3xTgNnSc4	všechen
zapomenuto	zapomenut	k2eAgNnSc4d1	zapomenuto
<g/>
.	.	kIx.	.
</s>
<s>
Tesla	Tesla	k1gFnSc1	Tesla
skončila	skončit	k5eAaPmAgFnS	skončit
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
na	na	k7c6	na
šestém	šestý	k4xOgNnSc6	šestý
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
se	se	k3xPyFc4	se
střetla	střetnout	k5eAaPmAgFnS	střetnout
s	s	k7c7	s
pražskou	pražský	k2eAgFnSc7d1	Pražská
Spartou	Sparta	k1gFnSc7	Sparta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
brance	branka	k1gFnSc6	branka
Sparty	Sparta	k1gFnSc2	Sparta
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
jeho	jeho	k3xOp3gMnSc1	jeho
reprezentační	reprezentační	k2eAgMnSc1d1	reprezentační
kolega	kolega	k1gMnSc1	kolega
Jaromír	Jaromír	k1gMnSc1	Jaromír
Šindel	šindel	k1gInSc4	šindel
<g/>
.	.	kIx.	.
</s>
<s>
Sparta	Sparta	k1gFnSc1	Sparta
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
třech	tři	k4xCgInPc6	tři
zápasech	zápas	k1gInPc6	zápas
úspěšnější	úspěšný	k2eAgInSc1d2	úspěšnější
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
radovala	radovat	k5eAaImAgFnS	radovat
z	z	k7c2	z
postupu	postup	k1gInSc2	postup
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
výkony	výkon	k1gInPc4	výkon
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
lize	liga	k1gFnSc6	liga
obdržel	obdržet	k5eAaPmAgMnS	obdržet
již	již	k6eAd1	již
potřetí	potřetí	k4xO	potřetí
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
cenu	cena	k1gFnSc4	cena
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
brankáře	brankář	k1gMnSc4	brankář
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
novou	nový	k2eAgFnSc4d1	nová
sezónu	sezóna	k1gFnSc4	sezóna
se	se	k3xPyFc4	se
v	v	k7c6	v
pardubické	pardubický	k2eAgFnSc6d1	pardubická
Tesle	Tesla	k1gFnSc6	Tesla
vystřídali	vystřídat	k5eAaPmAgMnP	vystřídat
trenéři	trenér	k1gMnPc1	trenér
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgMnSc7d1	nový
trenérem	trenér	k1gMnSc7	trenér
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
bývalý	bývalý	k2eAgMnSc1d1	bývalý
asistent	asistent	k1gMnSc1	asistent
Vladimír	Vladimír	k1gMnSc1	Vladimír
Martinec	Martinec	k1gMnSc1	Martinec
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
tým	tým	k1gInSc1	tým
neprohrál	prohrát	k5eNaPmAgInS	prohrát
devatenáct	devatenáct	k4xCc4	devatenáct
zápasů	zápas	k1gInPc2	zápas
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
domácím	domácí	k2eAgNnSc6d1	domácí
kluzišti	kluziště	k1gNnSc6	kluziště
je	on	k3xPp3gNnPc4	on
dokázali	dokázat	k5eAaPmAgMnP	dokázat
porazit	porazit	k5eAaPmF	porazit
pouze	pouze	k6eAd1	pouze
Sparta	Sparta	k1gFnSc1	Sparta
a	a	k8xC	a
Litvínov	Litvínov	k1gInSc1	Litvínov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
sezóny	sezóna	k1gFnSc2	sezóna
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
tabulky	tabulka	k1gFnSc2	tabulka
s	s	k7c7	s
náskokem	náskok	k1gInSc7	náskok
deseti	deset	k4xCc2	deset
bodů	bod	k1gInPc2	bod
nad	nad	k7c7	nad
druhým	druhý	k4xOgInSc7	druhý
Trenčínem	Trenčín	k1gInSc7	Trenčín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
hodnocení	hodnocení	k1gNnSc6	hodnocení
měli	mít	k5eAaImAgMnP	mít
taktéž	taktéž	k?	taktéž
nejlepší	dobrý	k2eAgFnSc2d3	nejlepší
republikové	republikový	k2eAgFnSc2d1	republiková
statistiky	statistika	k1gFnSc2	statistika
ve	v	k7c6	v
střelených	střelený	k2eAgFnPc6d1	střelená
a	a	k8xC	a
obdržených	obdržený	k2eAgFnPc6d1	obdržená
brankách	branka	k1gFnPc6	branka
<g/>
:	:	kIx,	:
158	[number]	k4	158
<g/>
:	:	kIx,	:
<g/>
95	[number]	k4	95
<g/>
.	.	kIx.	.
</s>
<s>
Tesla	Tesla	k1gFnSc1	Tesla
se	se	k3xPyFc4	se
také	také	k9	také
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
turnaji	turnaj	k1gInSc6	turnaj
Pohárů	pohár	k1gInPc2	pohár
mistrů	mistr	k1gMnPc2	mistr
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
sehrála	sehrát	k5eAaPmAgFnS	sehrát
3	[number]	k4	3
zápasy	zápas	k1gInPc4	zápas
<g/>
:	:	kIx,	:
první	první	k4xOgNnSc4	první
utkání	utkání	k1gNnSc4	utkání
se	s	k7c7	s
švédským	švédský	k2eAgInSc7d1	švédský
Björklövenem	Björklöven	k1gInSc7	Björklöven
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
patem	pat	k1gInSc7	pat
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
po	po	k7c6	po
následném	následný	k2eAgInSc6d1	následný
zápase	zápas	k1gInSc6	zápas
s	s	k7c7	s
finským	finský	k2eAgInSc7d1	finský
týmem	tým	k1gInSc7	tým
Tappara	Tappara	k1gFnSc1	Tappara
čeští	český	k2eAgMnPc1d1	český
hráči	hráč	k1gMnPc1	hráč
slavili	slavit	k5eAaImAgMnP	slavit
výhru	výhra	k1gFnSc4	výhra
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
utkání	utkání	k1gNnSc1	utkání
turnaje	turnaj	k1gInSc2	turnaj
sehráli	sehrát	k5eAaPmAgMnP	sehrát
pardubičtí	pardubický	k2eAgMnPc1d1	pardubický
se	s	k7c7	s
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
CSKA	CSKA	kA	CSKA
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Hašek	Hašek	k1gMnSc1	Hašek
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
zápase	zápas	k1gInSc6	zápas
odchytal	odchytat	k5eAaPmAgMnS	odchytat
61	[number]	k4	61
střel	střela	k1gFnPc2	střela
<g/>
,	,	kIx,	,
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
jen	jen	k9	jen
na	na	k7c4	na
střely	střela	k1gFnPc4	střela
Vladimira	Vladimir	k1gInSc2	Vladimir
Krutova	Krutův	k2eAgInSc2d1	Krutův
a	a	k8xC	a
Igora	Igor	k1gMnSc2	Igor
Larionova	Larionův	k2eAgMnSc2d1	Larionův
<g/>
.	.	kIx.	.
</s>
<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
prohrály	prohrát	k5eAaPmAgInP	prohrát
těsně	těsně	k6eAd1	těsně
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
republiky	republika	k1gFnSc2	republika
započalo	započnout	k5eAaPmAgNnS	započnout
playoff	playoff	k1gInSc4	playoff
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
Tesla	Tesla	k1gFnSc1	Tesla
deklasovala	deklasovat	k5eAaBmAgFnS	deklasovat
třemi	tři	k4xCgInPc7	tři
zápasy	zápas	k1gInPc7	zápas
Litvínov	Litvínov	k1gInSc1	Litvínov
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
snadném	snadný	k2eAgInSc6d1	snadný
postupu	postup	k1gInSc6	postup
do	do	k7c2	do
semifinále	semifinále	k1gNnSc2	semifinále
narazila	narazit	k5eAaPmAgFnS	narazit
na	na	k7c4	na
Spartu	Sparta	k1gFnSc4	Sparta
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
narazili	narazit	k5eAaPmAgMnP	narazit
pardubičtí	pardubický	k2eAgMnPc1d1	pardubický
na	na	k7c4	na
slovenský	slovenský	k2eAgInSc4d1	slovenský
Trenčín	Trenčín	k1gInSc4	Trenčín
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgInPc4	čtyři
zápasy	zápas	k1gInPc4	zápas
stačili	stačit	k5eAaBmAgMnP	stačit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Tesla	Tesla	k1gFnSc1	Tesla
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Dominik	Dominik	k1gMnSc1	Dominik
Hašek	Hašek	k1gMnSc1	Hašek
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
nastávající	nastávající	k1gFnSc4	nastávající
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
začátkem	začátek	k1gInSc7	začátek
turnaje	turnaj	k1gInSc2	turnaj
přišla	přijít	k5eAaPmAgFnS	přijít
smolná	smolný	k2eAgFnSc1d1	smolná
remíza	remíza	k1gFnSc1	remíza
se	s	k7c7	s
SRN	SRN	kA	SRN
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Následovali	následovat	k5eAaImAgMnP	následovat
tři	tři	k4xCgFnPc4	tři
výhry	výhra	k1gFnPc4	výhra
nad	nad	k7c4	nad
výběry	výběr	k1gInPc4	výběr
z	z	k7c2	z
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
střet	střet	k1gInSc1	střet
se	s	k7c7	s
Sověty	Sovět	k1gMnPc7	Sovět
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
porazili	porazit	k5eAaPmAgMnP	porazit
Čechoslováky	Čechoslovák	k1gMnPc4	Čechoslovák
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
remíza	remíza	k1gFnSc1	remíza
se	s	k7c7	s
Švédy	Švéd	k1gMnPc7	Švéd
a	a	k8xC	a
prohra	prohra	k1gFnSc1	prohra
s	s	k7c7	s
Kanadou	Kanada	k1gFnSc7	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
střetnutí	střetnutí	k1gNnSc6	střetnutí
s	s	k7c7	s
sovětskou	sovětský	k2eAgFnSc7d1	sovětská
sbornou	sborný	k2eAgFnSc7d1	sborná
padla	padnout	k5eAaPmAgFnS	padnout
jediná	jediný	k2eAgFnSc1d1	jediná
branka	branka	k1gFnSc1	branka
v	v	k7c4	v
neprospěch	neprospěch	k1gInSc4	neprospěch
Čechoslováků	Čechoslovák	k1gMnPc2	Čechoslovák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předposledním	předposlední	k2eAgInSc6d1	předposlední
zápase	zápas	k1gInSc6	zápas
padli	padnout	k5eAaPmAgMnP	padnout
Švédové	Švédové	k2eAgInPc2d1	Švédové
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
v	v	k7c6	v
rozhodujícím	rozhodující	k2eAgMnSc6d1	rozhodující
třetím	třetí	k4xOgInSc6	třetí
zápase	zápas	k1gInSc6	zápas
s	s	k7c7	s
Kanadou	Kanada	k1gFnSc7	Kanada
hráči	hráč	k1gMnPc7	hráč
z	z	k7c2	z
centrální	centrální	k2eAgFnSc2d1	centrální
Evropy	Evropa	k1gFnSc2	Evropa
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
si	se	k3xPyFc3	se
nakonec	nakonec	k6eAd1	nakonec
přivezli	přivézt	k5eAaPmAgMnP	přivézt
domů	domů	k6eAd1	domů
"	"	kIx"	"
<g/>
jen	jen	k9	jen
<g/>
"	"	kIx"	"
bronzové	bronzový	k2eAgFnPc4d1	bronzová
medaile	medaile	k1gFnPc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mistrovství	mistrovství	k1gNnSc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
Československo	Československo	k1gNnSc1	Československo
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c6	na
druhém	druhý	k4xOgMnSc6	druhý
<g/>
,	,	kIx,	,
stříbrném	stříbrný	k2eAgNnSc6d1	stříbrné
<g/>
,	,	kIx,	,
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
složil	složit	k5eAaPmAgMnS	složit
státní	státní	k2eAgFnSc2d1	státní
závěrečné	závěrečný	k2eAgFnSc2d1	závěrečná
zkoušky	zkouška	k1gFnSc2	zkouška
na	na	k7c6	na
Pedagogické	pedagogický	k2eAgFnSc6d1	pedagogická
fakultě	fakulta	k1gFnSc6	fakulta
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
<g/>
.	.	kIx.	.
</s>
<s>
Diplomovou	diplomový	k2eAgFnSc4d1	Diplomová
práci	práce	k1gFnSc4	práce
napsal	napsat	k5eAaPmAgMnS	napsat
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
"	"	kIx"	"
<g/>
Psychologická	psychologický	k2eAgFnSc1d1	psychologická
osobnost	osobnost	k1gFnSc1	osobnost
brankáře	brankář	k1gMnSc2	brankář
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
psaní	psaní	k1gNnSc3	psaní
používal	používat	k5eAaImAgMnS	používat
dotazníky	dotazník	k1gInPc4	dotazník
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc4	který
nechával	nechávat	k5eAaImAgMnS	nechávat
vyplňovat	vyplňovat	k5eAaImF	vyplňovat
brankářům	brankář	k1gMnPc3	brankář
jako	jako	k8xS	jako
například	například	k6eAd1	například
Švédovi	Švéd	k1gMnSc3	Švéd
Peteru	Peter	k1gMnSc3	Peter
Lindmarkovi	Lindmarek	k1gMnSc3	Lindmarek
<g/>
,	,	kIx,	,
Němci	Němec	k1gMnSc3	Němec
Karlu	Karel	k1gMnSc3	Karel
Friesenovi	Friesen	k1gMnSc3	Friesen
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Jiřímu	Jiří	k1gMnSc3	Jiří
Králíkovi	Králík	k1gMnSc3	Králík
a	a	k8xC	a
Jirkovi	Jirka	k1gMnSc3	Jirka
Holečkovi	Holeček	k1gMnSc3	Holeček
<g/>
.	.	kIx.	.
</s>
<s>
Týden	týden	k1gInSc1	týden
po	po	k7c4	po
složení	složení	k1gNnSc4	složení
zkoušek	zkouška	k1gFnPc2	zkouška
jej	on	k3xPp3gMnSc4	on
čekal	čekat	k5eAaImAgInS	čekat
další	další	k2eAgInSc1d1	další
velký	velký	k2eAgInSc1d1	velký
krok	krok	k1gInSc1	krok
v	v	k7c6	v
osobním	osobní	k2eAgInSc6d1	osobní
životě	život	k1gInSc6	život
-	-	kIx~	-
svatba	svatba	k1gFnSc1	svatba
s	s	k7c7	s
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
Alenou	Alena	k1gFnSc7	Alena
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
odstěhovali	odstěhovat	k5eAaPmAgMnP	odstěhovat
k	k	k7c3	k
jejím	její	k3xOp3gMnPc3	její
rodičům	rodič	k1gMnPc3	rodič
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bydleli	bydlet	k5eAaImAgMnP	bydlet
až	až	k9	až
do	do	k7c2	do
odchodu	odchod	k1gInSc2	odchod
do	do	k7c2	do
zámoří	zámoří	k1gNnSc2	zámoří
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
dostal	dostat	k5eAaPmAgInS	dostat
povolávací	povolávací	k2eAgInSc1d1	povolávací
rozkaz	rozkaz	k1gInSc1	rozkaz
na	na	k7c4	na
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
základní	základní	k2eAgFnSc4d1	základní
službu	služba	k1gFnSc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
působiště	působiště	k1gNnSc4	působiště
si	se	k3xPyFc3	se
vybral	vybrat	k5eAaPmAgMnS	vybrat
Jihlavu	Jihlava	k1gFnSc4	Jihlava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začal	začít	k5eAaPmAgInS	začít
nastupovat	nastupovat	k5eAaImF	nastupovat
za	za	k7c4	za
tamní	tamní	k2eAgInSc4d1	tamní
tým	tým	k1gInSc4	tým
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
Dukle	Dukla	k1gFnSc6	Dukla
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
stala	stát	k5eAaPmAgFnS	stát
brankářská	brankářský	k2eAgFnSc1d1	brankářská
jednička	jednička	k1gFnSc1	jednička
<g/>
,	,	kIx,	,
záda	záda	k1gNnPc4	záda
mu	on	k3xPp3gMnSc3	on
kryje	krýt	k5eAaImIp3nS	krýt
Oldřich	Oldřich	k1gMnSc1	Oldřich
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Dominikem	Dominik	k1gMnSc7	Dominik
v	v	k7c6	v
bráně	brána	k1gFnSc6	brána
Jihlava	Jihlava	k1gFnSc1	Jihlava
neprohrála	prohrát	k5eNaPmAgFnS	prohrát
jednadvacet	jednadvacet	k1gNnSc4	jednadvacet
zápasů	zápas	k1gInPc2	zápas
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
zkomplikovalo	zkomplikovat	k5eAaPmAgNnS	zkomplikovat
v	v	k7c6	v
období	období	k1gNnSc6	období
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
základní	základní	k2eAgFnSc2d1	základní
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Jihlava	Jihlava	k1gFnSc1	Jihlava
měla	mít	k5eAaImAgFnS	mít
střetnou	střetnout	k5eAaPmIp3nP	střetnout
s	s	k7c7	s
Pardubicemi	Pardubice	k1gInPc7	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Hašek	Hašek	k1gMnSc1	Hašek
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
několik	několik	k4yIc1	několik
kmenových	kmenový	k2eAgInPc2d1	kmenový
"	"	kIx"	"
<g/>
Pardubičáků	Pardubičák	k1gInPc2	Pardubičák
<g/>
"	"	kIx"	"
požádali	požádat	k5eAaPmAgMnP	požádat
trenéra	trenér	k1gMnSc4	trenér
Josefa	Josef	k1gMnSc4	Josef
Augustu	Augusta	k1gMnSc4	Augusta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nemuseli	muset	k5eNaImAgMnP	muset
nastoupit	nastoupit	k5eAaPmF	nastoupit
proti	proti	k7c3	proti
svému	svůj	k3xOyFgInSc3	svůj
týmu	tým	k1gInSc3	tým
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
potácel	potácet	k5eAaImAgInS	potácet
na	na	k7c6	na
hraně	hrana	k1gFnSc6	hrana
sestupových	sestupový	k2eAgFnPc2d1	sestupová
příček	příčka	k1gFnPc2	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Trenér	trenér	k1gMnSc1	trenér
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
později	pozdě	k6eAd2	pozdě
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
změnil	změnit	k5eAaPmAgMnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Dominik	Dominik	k1gMnSc1	Dominik
do	do	k7c2	do
zápasu	zápas	k1gInSc2	zápas
nastoupit	nastoupit	k5eAaPmF	nastoupit
musel	muset	k5eAaImAgMnS	muset
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
po	po	k7c6	po
prvním	první	k4xOgInSc6	první
kontaktu	kontakt	k1gInSc6	kontakt
s	s	k7c7	s
pukem	puk	k1gInSc7	puk
odjel	odjet	k5eAaPmAgMnS	odjet
na	na	k7c4	na
střídačku	střídačka	k1gFnSc4	střídačka
se	s	k7c7	s
"	"	kIx"	"
<g/>
zraněním	zranění	k1gNnSc7	zranění
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Trenér	trenér	k1gMnSc1	trenér
Josef	Josef	k1gMnSc1	Josef
Augusta	Augusta	k1gMnSc1	Augusta
však	však	k9	však
jej	on	k3xPp3gInSc4	on
nenechal	nechat	k5eNaPmAgInS	nechat
beze	beze	k7c2	beze
slova	slovo	k1gNnSc2	slovo
odejít	odejít	k5eAaPmF	odejít
<g/>
,	,	kIx,	,
Hašek	Hašek	k1gMnSc1	Hašek
reagoval	reagovat	k5eAaBmAgMnS	reagovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
svůj	svůj	k3xOyFgInSc4	svůj
dres	dres	k1gInSc4	dres
vyhodil	vyhodit	k5eAaPmAgMnS	vyhodit
do	do	k7c2	do
nejbližšího	blízký	k2eAgInSc2d3	Nejbližší
odpadkového	odpadkový	k2eAgInSc2d1	odpadkový
koše	koš	k1gInSc2	koš
<g/>
.	.	kIx.	.
</s>
<s>
Trenéři	trenér	k1gMnPc1	trenér
naštvaného	naštvaný	k2eAgMnSc2d1	naštvaný
gólmana	gólman	k1gMnSc2	gólman
okamžitě	okamžitě	k6eAd1	okamžitě
vyhodili	vyhodit	k5eAaPmAgMnP	vyhodit
z	z	k7c2	z
kabin	kabina	k1gFnPc2	kabina
a	a	k8xC	a
poslali	poslat	k5eAaPmAgMnP	poslat
jej	on	k3xPp3gMnSc4	on
do	do	k7c2	do
kasáren	kasárny	k1gFnPc2	kasárny
v	v	k7c6	v
Liberci	Liberec	k1gInSc6	Liberec
<g/>
,	,	kIx,	,
taktéž	taktéž	k?	taktéž
se	se	k3xPyFc4	se
přičinili	přičinit	k5eAaPmAgMnP	přičinit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
od	od	k7c2	od
svazu	svaz	k1gInSc2	svaz
obdržel	obdržet	k5eAaPmAgMnS	obdržet
osmizápasový	osmizápasový	k2eAgMnSc1d1	osmizápasový
distanc	distanc	k?	distanc
<g/>
.	.	kIx.	.
</s>
<s>
Jihlava	Jihlava	k1gFnSc1	Jihlava
dokončila	dokončit	k5eAaPmAgFnS	dokončit
základní	základní	k2eAgFnSc4d1	základní
část	část	k1gFnSc4	část
a	a	k8xC	a
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
se	se	k3xPyFc4	se
střetla	střetnout	k5eAaPmAgFnS	střetnout
s	s	k7c7	s
Litvínovem	Litvínov	k1gInSc7	Litvínov
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
zápasech	zápas	k1gInPc6	zápas
bylo	být	k5eAaImAgNnS	být
vyrovnáno	vyrovnán	k2eAgNnSc1d1	vyrovnáno
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	on	k3xPp3gInSc4	on
trest	trest	k1gInSc4	trest
mu	on	k3xPp3gMnSc3	on
vypršel	vypršet	k5eAaPmAgMnS	vypršet
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
stěžoval	stěžovat	k5eAaImAgMnS	stěžovat
si	se	k3xPyFc3	se
na	na	k7c4	na
zranění	zranění	k1gNnSc4	zranění
třísel	tříslo	k1gNnPc2	tříslo
<g/>
,	,	kIx,	,
i	i	k8xC	i
přesto	přesto	k8xC	přesto
do	do	k7c2	do
zápasu	zápas	k1gInSc2	zápas
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
první	první	k4xOgFnSc6	první
třetině	třetina	k1gFnSc6	třetina
pustil	pustit	k5eAaPmAgMnS	pustit
tři	tři	k4xCgInPc4	tři
góly	gól	k1gInPc4	gól
a	a	k8xC	a
hokejisté	hokejista	k1gMnPc1	hokejista
Jihlavy	Jihlava	k1gFnSc2	Jihlava
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
bojích	boj	k1gInPc6	boj
o	o	k7c4	o
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
titul	titul	k1gInSc4	titul
skončili	skončit	k5eAaPmAgMnP	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
z	z	k7c2	z
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
radovat	radovat	k5eAaImF	radovat
z	z	k7c2	z
narození	narození	k1gNnSc2	narození
prvního	první	k4xOgNnSc2	první
dítěte	dítě	k1gNnSc2	dítě
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
Michala	Michal	k1gMnSc2	Michal
<g/>
,	,	kIx,	,
přišla	přijít	k5eAaPmAgFnS	přijít
pozvánka	pozvánka	k1gFnSc1	pozvánka
na	na	k7c4	na
reprezentační	reprezentační	k2eAgNnSc4d1	reprezentační
utkání	utkání	k1gNnSc4	utkání
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgInSc6d1	švýcarský
Bernu	Bern	k1gInSc6	Bern
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
základní	základní	k2eAgFnPc4d1	základní
části	část	k1gFnPc4	část
Češi	Čech	k1gMnPc1	Čech
oslavovali	oslavovat	k5eAaImAgMnP	oslavovat
vítězství	vítězství	k1gNnSc4	vítězství
proti	proti	k7c3	proti
Norsku	Norsko	k1gNnSc3	Norsko
a	a	k8xC	a
SRN	SRN	kA	SRN
avšak	avšak	k8xC	avšak
s	s	k7c7	s
Kanadou	Kanada	k1gFnSc7	Kanada
<g/>
,	,	kIx,	,
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
a	a	k8xC	a
Švédskem	Švédsko	k1gNnSc7	Švédsko
prohráli	prohrát	k5eAaPmAgMnP	prohrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
zápase	zápas	k1gInSc6	zápas
o	o	k7c4	o
medaile	medaile	k1gFnPc4	medaile
s	s	k7c7	s
Kanadou	Kanada	k1gFnSc7	Kanada
si	se	k3xPyFc3	se
však	však	k8xC	však
poranil	poranit	k5eAaPmAgMnS	poranit
koleno	koleno	k1gNnSc4	koleno
a	a	k8xC	a
následující	následující	k2eAgInSc4d1	následující
zápas	zápas	k1gInSc4	zápas
musel	muset	k5eAaImAgMnS	muset
odehrát	odehrát	k5eAaPmF	odehrát
Petr	Petr	k1gMnSc1	Petr
Bříza	Bříza	k1gMnSc1	Bříza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
rozhodujícím	rozhodující	k2eAgInSc6d1	rozhodující
zápase	zápas	k1gInSc6	zápas
opět	opět	k6eAd1	opět
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
tentokrát	tentokrát	k6eAd1	tentokrát
byli	být	k5eAaImAgMnP	být
úspěšnější	úspěšný	k2eAgMnPc1d2	úspěšnější
Sověti	Sovět	k1gMnPc1	Sovět
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k9	tak
v	v	k7c6	v
konečném	konečný	k2eAgNnSc6d1	konečné
pořadí	pořadí	k1gNnSc6	pořadí
se	se	k3xPyFc4	se
Československá	československý	k2eAgFnSc1d1	Československá
federativní	federativní	k2eAgFnSc1d1	federativní
republika	republika	k1gFnSc1	republika
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgInS	být
zařazen	zařadit	k5eAaPmNgMnS	zařadit
do	do	k7c2	do
All-Stars	All-Starsa	k1gFnPc2	All-Starsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
republice	republika	k1gFnSc6	republika
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
již	již	k6eAd1	již
potřetí	potřetí	k4xO	potřetí
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
hokejku	hokejka	k1gFnSc4	hokejka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
nastaly	nastat	k5eAaPmAgFnP	nastat
velké	velký	k2eAgFnPc1d1	velká
změny	změna	k1gFnPc1	změna
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byla	být	k5eAaImAgFnS	být
také	také	k9	také
otevření	otevření	k1gNnSc4	otevření
hranic	hranice	k1gFnPc2	hranice
na	na	k7c4	na
Západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
scouti	scouť	k1gFnSc2	scouť
z	z	k7c2	z
Chicaga	Chicago	k1gNnSc2	Chicago
mu	on	k3xPp3gNnSc3	on
nabídli	nabídnout	k5eAaPmAgMnP	nabídnout
již	již	k9	již
několikátou	několikátý	k4xOyIgFnSc4	několikátý
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
ji	on	k3xPp3gFnSc4	on
podepsal	podepsat	k5eAaPmAgMnS	podepsat
bez	bez	k7c2	bez
strachu	strach	k1gInSc2	strach
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
musel	muset	k5eAaImAgMnS	muset
emigrovat	emigrovat	k5eAaBmF	emigrovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Edmontonu	Edmonton	k1gInSc6	Edmonton
se	se	k3xPyFc4	se
sešel	sejít	k5eAaPmAgMnS	sejít
s	s	k7c7	s
generálním	generální	k2eAgMnSc7d1	generální
manažerem	manažer	k1gMnSc7	manažer
Černých	Černých	k2eAgMnPc2d1	Černých
jestřábů	jestřáb	k1gMnPc2	jestřáb
Bobem	Bob	k1gMnSc7	Bob
Pulfordem	Pulford	k1gInSc7	Pulford
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
svého	svůj	k3xOyFgMnSc2	svůj
agenta	agent	k1gMnSc2	agent
Richa	Rich	k1gMnSc2	Rich
Wintera	Winter	k1gMnSc2	Winter
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jednání	jednání	k1gNnSc2	jednání
vzešla	vzejít	k5eAaPmAgFnS	vzejít
dvojcestná	dvojcestný	k2eAgFnSc1d1	dvojcestná
smlouva	smlouva	k1gFnSc1	smlouva
-	-	kIx~	-
pakliže	pakliže	k8xS	pakliže
se	se	k3xPyFc4	se
prosadí	prosadit	k5eAaPmIp3nS	prosadit
u	u	k7c2	u
Černých	Černých	k2eAgMnPc2d1	Černých
jestřábů	jestřáb	k1gMnPc2	jestřáb
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
šestý	šestý	k4xOgMnSc1	šestý
nejlépe	dobře	k6eAd3	dobře
placený	placený	k2eAgMnSc1d1	placený
brankář	brankář	k1gMnSc1	brankář
v	v	k7c6	v
NHL	NHL	kA	NHL
<g/>
,	,	kIx,	,
plat	plat	k1gInSc1	plat
200	[number]	k4	200
000	[number]	k4	000
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
by	by	kYmCp3nS	by
zůstal	zůstat	k5eAaPmAgMnS	zůstat
na	na	k7c6	na
farmě	farma	k1gFnSc6	farma
v	v	k7c6	v
Indianapolisu	Indianapolis	k1gInSc6	Indianapolis
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
plat	plat	k1gInSc1	plat
by	by	kYmCp3nP	by
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
k	k	k7c3	k
60	[number]	k4	60
tisícům	tisíc	k4xCgInPc3	tisíc
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
týmu	tým	k1gInSc6	tým
se	se	k3xPyFc4	se
střídali	střídat	k5eAaImAgMnP	střídat
brankáři	brankář	k1gMnPc1	brankář
Jacques	Jacques	k1gMnSc1	Jacques
Cloutier	Cloutier	k1gMnSc1	Cloutier
<g/>
,	,	kIx,	,
Greg	Greg	k1gMnSc1	Greg
Mille	Mille	k1gFnSc2	Mille
<g/>
,	,	kIx,	,
Jimmy	Jimma	k1gFnSc2	Jimma
Waite	Wait	k1gInSc5	Wait
a	a	k8xC	a
Ed	Ed	k1gMnSc1	Ed
Belfour	Belfour	k1gMnSc1	Belfour
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnPc1d1	hlavní
trenéři	trenér	k1gMnPc1	trenér
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
pro	pro	k7c4	pro
Belfoura	Belfour	k1gMnSc4	Belfour
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
více	hodně	k6eAd2	hodně
zkušeností	zkušenost	k1gFnPc2	zkušenost
a	a	k8xC	a
vyšla	vyjít	k5eAaPmAgFnS	vyjít
mu	on	k3xPp3gMnSc3	on
lépe	dobře	k6eAd2	dobře
příprava	příprava	k1gFnSc1	příprava
<g/>
.	.	kIx.	.
</s>
<s>
Musel	muset	k5eAaImAgMnS	muset
tedy	tedy	k9	tedy
odcestovat	odcestovat	k5eAaPmF	odcestovat
do	do	k7c2	do
Indianapolisu	Indianapolis	k1gInSc2	Indianapolis
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
spíše	spíše	k9	spíše
počítalo	počítat	k5eAaImAgNnS	počítat
jako	jako	k9	jako
náhradníkem	náhradník	k1gMnSc7	náhradník
Waita	Wait	k1gInSc2	Wait
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
dostal	dostat	k5eAaPmAgMnS	dostat
šanci	šance	k1gFnSc4	šance
hned	hned	k6eAd1	hned
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
zápase	zápas	k1gInSc6	zápas
a	a	k8xC	a
zápas	zápas	k1gInSc4	zápas
mu	on	k3xPp3gMnSc3	on
vyšel	vyjít	k5eAaPmAgMnS	vyjít
-	-	kIx~	-
zápas	zápas	k1gInSc1	zápas
odehrál	odehrát	k5eAaPmAgInS	odehrát
bez	bez	k7c2	bez
obdržené	obdržený	k2eAgFnSc2d1	obdržená
branky	branka	k1gFnSc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
je	on	k3xPp3gFnPc4	on
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
kouč	kouč	k1gMnSc1	kouč
Indianapolis	Indianapolis	k1gFnSc2	Indianapolis
Dave	Dav	k1gInSc5	Dav
McDowall	McDowall	k1gInSc4	McDowall
pravidelně	pravidelně	k6eAd1	pravidelně
střídat	střídat	k5eAaImF	střídat
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
na	na	k7c6	na
farmě	farma	k1gFnSc6	farma
ukázal	ukázat	k5eAaPmAgInS	ukázat
v	v	k7c6	v
dobrém	dobrý	k2eAgNnSc6d1	dobré
světle	světlo	k1gNnSc6	světlo
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1990	[number]	k4	1990
dostal	dostat	k5eAaPmAgMnS	dostat
šanci	šance	k1gFnSc4	šance
i	i	k9	i
v	v	k7c6	v
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
soupeřem	soupeř	k1gMnSc7	soupeř
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Hartford	Hartford	k1gMnSc1	Hartford
Whalers	Whalersa	k1gFnPc2	Whalersa
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc1	zápas
po	po	k7c6	po
základní	základní	k2eAgFnSc6d1	základní
hrací	hrací	k2eAgFnSc6d1	hrací
době	doba	k1gFnSc6	doba
skončil	skončit	k5eAaPmAgInS	skončit
nerozhodně	rozhodně	k6eNd1	rozhodně
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
však	však	k9	však
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sezóně	sezóna	k1gFnSc6	sezóna
většinu	většina	k1gFnSc4	většina
zápasů	zápas	k1gInPc2	zápas
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
IHL	IHL	kA	IHL
<g/>
,	,	kIx,	,
v	v	k7c6	v
NHL	NHL	kA	NHL
poté	poté	k6eAd1	poté
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
již	již	k6eAd1	již
jen	jen	k9	jen
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
<g/>
.	.	kIx.	.
</s>
<s>
Lepší	dobrý	k2eAgFnPc1d2	lepší
statistiky	statistika	k1gFnPc1	statistika
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
v	v	k7c6	v
zápasech	zápas	k1gInPc6	zápas
za	za	k7c4	za
Indianapolis	Indianapolis	k1gFnSc4	Indianapolis
-	-	kIx~	-
v	v	k7c6	v
33	[number]	k4	33
zápasech	zápas	k1gInPc6	zápas
vychytal	vychytat	k5eAaPmAgMnS	vychytat
5	[number]	k4	5
shutoutů	shutout	k1gMnPc2	shutout
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
2,52	[number]	k4	2,52
gólu	gól	k1gInSc2	gól
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
<g/>
,	,	kIx,	,
čím	co	k3yInSc7	co
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
brankářem	brankář	k1gMnSc7	brankář
IHL	IHL	kA	IHL
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k3yQnSc4	co
Chicago	Chicago	k1gNnSc1	Chicago
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
Stanley	Stanlea	k1gFnPc4	Stanlea
Cup	cup	k1gInSc4	cup
<g/>
,	,	kIx,	,
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
proti	proti	k7c3	proti
Minnesotě	Minnesota	k1gFnSc3	Minnesota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
bojích	boj	k1gInPc6	boj
třikrát	třikrát	k6eAd1	třikrát
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vždy	vždy	k6eAd1	vždy
střídal	střídat	k5eAaImAgMnS	střídat
Belfoura	Belfour	k1gMnSc4	Belfour
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
sezóna	sezóna	k1gFnSc1	sezóna
započala	započnout	k5eAaPmAgFnS	započnout
novým	nový	k2eAgInSc7d1	nový
ročníkem	ročník	k1gInSc7	ročník
Kanadského	kanadský	k2eAgInSc2d1	kanadský
poháru	pohár	k1gInSc2	pohár
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byl	být	k5eAaImAgMnS	být
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
nominován	nominován	k2eAgMnSc1d1	nominován
<g/>
.	.	kIx.	.
</s>
<s>
Československý	československý	k2eAgInSc1d1	československý
výběr	výběr	k1gInSc1	výběr
začal	začít	k5eAaPmAgInS	začít
turnaj	turnaj	k1gInSc4	turnaj
vítězstvím	vítězství	k1gNnSc7	vítězství
nad	nad	k7c7	nad
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
další	další	k2eAgInPc1d1	další
zápasy	zápas	k1gInPc1	zápas
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nezdařily	zdařit	k5eNaPmAgFnP	zdařit
<g/>
,	,	kIx,	,
nejdřív	dříve	k6eAd3	dříve
přišla	přijít	k5eAaPmAgFnS	přijít
první	první	k4xOgFnSc1	první
smolná	smolný	k2eAgFnSc1d1	smolná
prohra	prohra	k1gFnSc1	prohra
s	s	k7c7	s
Finskem	Finsko	k1gNnSc7	Finsko
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
padl	padnout	k5eAaImAgInS	padnout
jediný	jediný	k2eAgInSc1d1	jediný
gól	gól	k1gInSc1	gól
několik	několik	k4yIc1	několik
sekund	sekunda	k1gFnPc2	sekunda
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
do	do	k7c2	do
jím	on	k3xPp3gMnSc7	on
střežené	střežený	k2eAgFnSc2d1	střežená
brány	brána	k1gFnPc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
pak	pak	k6eAd1	pak
s	s	k7c7	s
Američany	Američan	k1gMnPc7	Američan
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
Kanadou	Kanada	k1gFnSc7	Kanada
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
a	a	k8xC	a
poslední	poslední	k2eAgInSc1d1	poslední
zápas	zápas	k1gInSc1	zápas
se	s	k7c7	s
Švédy	Švéd	k1gMnPc7	Švéd
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
za	za	k7c7	za
mořem	moře	k1gNnSc7	moře
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
Dominika	Dominik	k1gMnSc4	Dominik
začala	začít	k5eAaPmAgFnS	začít
víc	hodně	k6eAd2	hodně
vyjasňovat	vyjasňovat	k5eAaImF	vyjasňovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
týmu	tým	k1gInSc6	tým
již	již	k6eAd1	již
zůstal	zůstat	k5eAaPmAgMnS	zůstat
jen	jen	k9	jen
Belfour	Belfour	k1gMnSc1	Belfour
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
dlouho	dlouho	k6eAd1	dlouho
nemohl	moct	k5eNaImAgMnS	moct
domluvit	domluvit	k5eAaPmF	domluvit
na	na	k7c6	na
nové	nový	k2eAgFnSc6d1	nová
smlouvě	smlouva	k1gFnSc6	smlouva
<g/>
,	,	kIx,	,
Hašek	Hašek	k1gMnSc1	Hašek
a	a	k8xC	a
Waite	Wait	k1gInSc5	Wait
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sezóně	sezóna	k1gFnSc6	sezóna
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
za	za	k7c4	za
Chicago	Chicago	k1gNnSc4	Chicago
na	na	k7c4	na
20	[number]	k4	20
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
Torontu	Toronto	k1gNnSc3	Toronto
vychytal	vychytat	k5eAaPmAgInS	vychytat
dokonce	dokonce	k9	dokonce
svoje	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
čisté	čistý	k2eAgNnSc4d1	čisté
konto	konto	k1gNnSc4	konto
v	v	k7c6	v
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
Stejný	stejný	k2eAgInSc1d1	stejný
počet	počet	k1gInSc1	počet
zápasů	zápas	k1gInPc2	zápas
odehrál	odehrát	k5eAaPmAgInS	odehrát
za	za	k7c4	za
Indianapolis	Indianapolis	k1gFnSc4	Indianapolis
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
gólů	gól	k1gInPc2	gól
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
se	se	k3xPyFc4	se
zastavil	zastavit	k5eAaPmAgMnS	zastavit
na	na	k7c6	na
čísle	číslo	k1gNnSc6	číslo
2,60	[number]	k4	2,60
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
Blackhowk	Blackhowk	k1gMnSc1	Blackhowk
postoupili	postoupit	k5eAaPmAgMnP	postoupit
do	do	k7c2	do
playoff	playoff	k1gInSc4	playoff
<g/>
,	,	kIx,	,
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
dokonce	dokonce	k9	dokonce
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
Stanley	Stanlea	k1gFnSc2	Stanlea
cupu	cup	k1gInSc2	cup
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystřídal	vystřídat	k5eAaPmAgInS	vystřídat
Belfoura	Belfour	k1gMnSc4	Belfour
při	při	k7c6	při
stavu	stav	k1gInSc6	stav
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
zabránil	zabránit	k5eAaPmAgInS	zabránit
však	však	k9	však
ještě	ještě	k6eAd1	ještě
čtyřem	čtyři	k4xCgInPc3	čtyři
gólům	gól	k1gInPc3	gól
a	a	k8xC	a
finále	finále	k1gNnSc6	finále
skončilo	skončit	k5eAaPmAgNnS	skončit
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
a	a	k8xC	a
Pittsburgh	Pittsburgh	k1gInSc4	Pittsburgh
oslavilo	oslavit	k5eAaPmAgNnS	oslavit
svůj	svůj	k3xOyFgInSc4	svůj
druhý	druhý	k4xOgInSc4	druhý
pohár	pohár	k1gInSc4	pohár
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
sezónách	sezóna	k1gFnPc6	sezóna
u	u	k7c2	u
Chicaga	Chicago	k1gNnSc2	Chicago
skončilo	skončit	k5eAaPmAgNnS	skončit
jeho	jeho	k3xOp3gNnSc4	jeho
zdejší	zdejší	k2eAgNnSc4d1	zdejší
angažmá	angažmá	k1gNnSc4	angažmá
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
poslán	poslat	k5eAaPmNgInS	poslat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
trojvýměny	trojvýměn	k2eAgInPc4d1	trojvýměn
do	do	k7c2	do
Buffala	Buffala	k1gFnSc2	Buffala
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
využíván	využívat	k5eAaPmNgInS	využívat
více	hodně	k6eAd2	hodně
jak	jak	k6eAd1	jak
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
časem	časem	k6eAd1	časem
přišlo	přijít	k5eAaPmAgNnS	přijít
menší	malý	k2eAgNnSc1d2	menší
zranění	zranění	k1gNnSc1	zranění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
marodce	marodka	k1gFnSc6	marodka
Buffalo	Buffalo	k1gFnSc2	Buffalo
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
vícenásobným	vícenásobný	k2eAgMnSc7d1	vícenásobný
držitelem	držitel	k1gMnSc7	držitel
Stanley	Stanlea	k1gFnSc2	Stanlea
Cupu	cupat	k5eAaImIp1nS	cupat
Grantem	grant	k1gInSc7	grant
Fuhrem	Fuhrma	k1gFnPc2	Fuhrma
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
odsunut	odsunout	k5eAaPmNgInS	odsunout
do	do	k7c2	do
pozice	pozice	k1gFnSc2	pozice
druhého	druhý	k4xOgMnSc2	druhý
náhradního	náhradní	k2eAgMnSc2d1	náhradní
brankáře	brankář	k1gMnSc2	brankář
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
tedy	tedy	k9	tedy
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
ve	v	k7c6	v
28	[number]	k4	28
zápasech	zápas	k1gInPc6	zápas
v	v	k7c4	v
základní	základní	k2eAgFnPc4d1	základní
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
playoff	playoff	k1gInSc4	playoff
však	však	k9	však
nastoupí	nastoupit	k5eAaPmIp3nP	nastoupit
jen	jen	k9	jen
v	v	k7c6	v
jediném	jediný	k2eAgInSc6d1	jediný
zápase	zápas	k1gInSc6	zápas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
zraněného	zraněný	k2eAgMnSc4d1	zraněný
Fuhra	Fuhr	k1gMnSc4	Fuhr
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
hry	hra	k1gFnSc2	hra
obdržel	obdržet	k5eAaPmAgMnS	obdržet
jeden	jeden	k4xCgInSc4	jeden
gól	gól	k1gInSc4	gól
a	a	k8xC	a
dovedl	dovést	k5eAaPmAgMnS	dovést
Buffalo	Buffalo	k1gFnSc4	Buffalo
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
sezóna	sezóna	k1gFnSc1	sezóna
přinesla	přinést	k5eAaPmAgFnS	přinést
týmu	tým	k1gInSc2	tým
spíše	spíše	k9	spíše
smůlu	smůla	k1gFnSc4	smůla
-	-	kIx~	-
týmu	tým	k1gInSc2	tým
se	se	k3xPyFc4	se
nedařilo	dařit	k5eNaImAgNnS	dařit
a	a	k8xC	a
jednička	jednička	k1gFnSc1	jednička
v	v	k7c6	v
brance	branka	k1gFnSc6	branka
<g/>
,	,	kIx,	,
Fuhr	Fuhr	k1gMnSc1	Fuhr
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zranil	zranit	k5eAaPmAgMnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Musel	muset	k5eAaImAgMnS	muset
tedy	tedy	k9	tedy
nastoupit	nastoupit	k5eAaPmF	nastoupit
Dominik	Dominik	k1gMnSc1	Dominik
a	a	k8xC	a
měl	mít	k5eAaImAgMnS	mít
jedinečnou	jedinečný	k2eAgFnSc4d1	jedinečná
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
předvést	předvést	k5eAaPmF	předvést
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
povedlo	povést	k5eAaPmAgNnS	povést
<g/>
.	.	kIx.	.
</s>
<s>
Čtyři	čtyři	k4xCgNnPc4	čtyři
čistá	čistý	k2eAgNnPc4d1	čisté
konta	konto	k1gNnPc4	konto
za	za	k7c4	za
měsíc	měsíc	k1gInSc4	měsíc
mu	on	k3xPp3gMnSc3	on
vyneslo	vynést	k5eAaPmAgNnS	vynést
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
hráčem	hráč	k1gMnSc7	hráč
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k9	co
se	se	k3xPyFc4	se
Fuhr	Fuhr	k1gMnSc1	Fuhr
vrátil	vrátit	k5eAaPmAgMnS	vrátit
z	z	k7c2	z
marodky	marodka	k1gFnSc2	marodka
<g/>
,	,	kIx,	,
trenér	trenér	k1gMnSc1	trenér
John	John	k1gMnSc1	John
Muckler	Muckler	k1gMnSc1	Muckler
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
oba	dva	k4xCgMnPc4	dva
brankáře	brankář	k1gMnPc4	brankář
vystřídat	vystřídat	k5eAaPmF	vystřídat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
základní	základní	k2eAgFnSc2d1	základní
části	část	k1gFnSc2	část
měl	mít	k5eAaImAgInS	mít
odehráno	odehrát	k5eAaPmNgNnS	odehrát
58	[number]	k4	58
zápasů	zápas	k1gInPc2	zápas
a	a	k8xC	a
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
obdržených	obdržený	k2eAgFnPc2d1	obdržená
branek	branka	k1gFnPc2	branka
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
1,95	[number]	k4	1,95
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jeho	jeho	k3xOp3gFnSc1	jeho
nová	nový	k2eAgFnSc1d1	nová
přezdívka	přezdívka	k1gFnSc1	přezdívka
-	-	kIx~	-
Dominátor	Dominátor	k1gInSc1	Dominátor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
playoff	playoff	k1gInSc4	playoff
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
první	první	k4xOgInSc1	první
zápas	zápas	k1gInSc1	zápas
odehrán	odehrát	k5eAaPmNgInS	odehrát
proti	proti	k7c3	proti
New	New	k1gFnSc3	New
Jersey	Jersea	k1gFnSc2	Jersea
Devils	Devilsa	k1gFnPc2	Devilsa
<g/>
.	.	kIx.	.
</s>
<s>
Přednost	přednost	k1gFnSc4	přednost
dostal	dostat	k5eAaPmAgMnS	dostat
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
Fuhr	Fuhr	k1gMnSc1	Fuhr
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
spokojit	spokojit	k5eAaPmF	spokojit
s	s	k7c7	s
rolí	role	k1gFnSc7	role
náhradníka	náhradník	k1gMnSc2	náhradník
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
však	však	k9	však
Jersey	Jersey	k1gInPc1	Jersey
zvítězí	zvítězit	k5eAaPmIp3nP	zvítězit
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
sezóny	sezóna	k1gFnSc2	sezóna
byl	být	k5eAaImAgInS	být
odměněn	odměnit	k5eAaPmNgInS	odměnit
první	první	k4xOgFnSc7	první
individuální	individuální	k2eAgFnSc7d1	individuální
trofejí	trofej	k1gFnSc7	trofej
v	v	k7c6	v
zámoří	zámoří	k1gNnSc6	zámoří
-	-	kIx~	-
Vezina	Vezina	k1gFnSc1	Vezina
Trophy	Tropha	k1gFnSc2	Tropha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
udělována	udělován	k2eAgFnSc1d1	udělována
nejlepšímu	dobrý	k2eAgMnSc3d3	nejlepší
brankáři	brankář	k1gMnSc3	brankář
v	v	k7c4	v
základní	základní	k2eAgFnPc4d1	základní
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Fuhrem	Fuhr	k1gInSc7	Fuhr
obdrží	obdržet	k5eAaPmIp3nS	obdržet
William	William	k1gInSc1	William
M.	M.	kA	M.
Jennings	Jennings	k1gInSc4	Jennings
Trophy	Tropha	k1gFnSc2	Tropha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
dvojici	dvojice	k1gFnSc4	dvojice
brankářů	brankář	k1gMnPc2	brankář
s	s	k7c7	s
nejnižším	nízký	k2eAgInSc7d3	nejnižší
počtem	počet	k1gInSc7	počet
inkasovaných	inkasovaný	k2eAgFnPc2d1	inkasovaná
branek	branka	k1gFnPc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
Podepsal	podepsat	k5eAaPmAgMnS	podepsat
novou	nový	k2eAgFnSc4d1	nová
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
vynesla	vynést	k5eAaPmAgFnS	vynést
7	[number]	k4	7
milionů	milion	k4xCgInPc2	milion
$	$	kIx~	$
za	za	k7c4	za
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
nastávající	nastávající	k2eAgFnSc1d1	nastávající
sezóna	sezóna	k1gFnSc1	sezóna
přináší	přinášet	k5eAaImIp3nS	přinášet
spory	spor	k1gInPc4	spor
mezi	mezi	k7c7	mezi
majiteli	majitel	k1gMnPc7	majitel
klubů	klub	k1gInPc2	klub
a	a	k8xC	a
hráčskou	hráčský	k2eAgFnSc7d1	hráčská
asociací	asociace	k1gFnSc7	asociace
NHLPA	NHLPA	kA	NHLPA
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
se	se	k3xPyFc4	se
pozdržuje	pozdržovat	k5eAaImIp3nS	pozdržovat
začátek	začátek	k1gInSc1	začátek
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
někteří	některý	k3yIgMnPc1	některý
hráči	hráč	k1gMnPc1	hráč
ze	z	k7c2	z
starého	starý	k2eAgInSc2d1	starý
kontinentu	kontinent	k1gInSc2	kontinent
hledají	hledat	k5eAaImIp3nP	hledat
uplatnění	uplatnění	k1gNnSc4	uplatnění
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
byl	být	k5eAaImAgMnS	být
i	i	k9	i
Hašek	Hašek	k1gMnSc1	Hašek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
na	na	k7c6	na
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
za	za	k7c7	za
HC	HC	kA	HC
Pardubice	Pardubice	k1gInPc4	Pardubice
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
utkání	utkání	k1gNnPc4	utkání
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
klubu	klub	k1gInSc2	klub
se	se	k3xPyFc4	se
rozhodovalo	rozhodovat	k5eAaImAgNnS	rozhodovat
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
z	z	k7c2	z
brankářů	brankář	k1gMnPc2	brankář
dá	dát	k5eAaPmIp3nS	dát
přednost	přednost	k1gFnSc4	přednost
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
pro	pro	k7c4	pro
středoevropana	středoevropan	k1gMnSc4	středoevropan
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
základní	základní	k2eAgFnSc2d1	základní
části	část	k1gFnSc2	část
tým	tým	k1gInSc1	tým
nasbíral	nasbírat	k5eAaPmAgInS	nasbírat
51	[number]	k4	51
bodů	bod	k1gInPc2	bod
a	a	k8xC	a
do	do	k7c2	do
playoff	playoff	k1gInSc1	playoff
postupovaly	postupovat	k5eAaImAgFnP	postupovat
ze	z	k7c2	z
sedmého	sedmý	k4xOgNnSc2	sedmý
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
sérii	série	k1gFnSc4	série
započali	započnout	k5eAaPmAgMnP	započnout
s	s	k7c7	s
hráči	hráč	k1gMnPc7	hráč
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
Flyers	Flyers	k1gInSc1	Flyers
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zakončila	zakončit	k5eAaPmAgFnS	zakončit
celou	celý	k2eAgFnSc4d1	celá
sérii	série	k1gFnSc4	série
jednoznačně	jednoznačně	k6eAd1	jednoznačně
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
této	tento	k3xDgFnSc2	tento
sezóny	sezóna	k1gFnSc2	sezóna
podruhé	podruhé	k6eAd1	podruhé
obdržel	obdržet	k5eAaPmAgMnS	obdržet
Vezinu	Vezina	k1gFnSc4	Vezina
Trophy	Tropha	k1gFnSc2	Tropha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osobním	osobní	k2eAgInSc6d1	osobní
životě	život	k1gInSc6	život
Haškovi	Haškův	k2eAgMnPc1d1	Haškův
slaví	slavit	k5eAaImIp3nS	slavit
<g/>
,	,	kIx,	,
narodilo	narodit	k5eAaPmAgNnS	narodit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
totiž	totiž	k9	totiž
druhé	druhý	k4xOgNnSc1	druhý
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Dominika	Dominik	k1gMnSc2	Dominik
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
sezóna	sezóna	k1gFnSc1	sezóna
znamenala	znamenat	k5eAaImAgFnS	znamenat
pro	pro	k7c4	pro
Buffalo	Buffalo	k1gFnSc4	Buffalo
Sabres	Sabresa	k1gFnPc2	Sabresa
značné	značný	k2eAgFnSc2d1	značná
změny	změna	k1gFnSc2	změna
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgMnSc1d1	hlavní
trenér	trenér	k1gMnSc1	trenér
John	John	k1gMnSc1	John
Muckler	Muckler	k1gMnSc1	Muckler
povýšil	povýšit	k5eAaPmAgMnS	povýšit
na	na	k7c4	na
generálního	generální	k2eAgMnSc4d1	generální
manažera	manažer	k1gMnSc4	manažer
a	a	k8xC	a
místo	místo	k1gNnSc4	místo
sebe	sebe	k3xPyFc4	sebe
našel	najít	k5eAaPmAgMnS	najít
náhradu	náhrada	k1gFnSc4	náhrada
v	v	k7c6	v
Tedu	Ted	k1gMnSc6	Ted
Nolanovi	Nolan	k1gMnSc6	Nolan
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
hra	hra	k1gFnSc1	hra
týmu	tým	k1gInSc2	tým
se	se	k3xPyFc4	se
podstatně	podstatně	k6eAd1	podstatně
zhoršila	zhoršit	k5eAaPmAgFnS	zhoršit
<g/>
,	,	kIx,	,
z	z	k7c2	z
82	[number]	k4	82
zápasů	zápas	k1gInPc2	zápas
bylo	být	k5eAaImAgNnS	být
vítězných	vítězný	k2eAgMnPc2d1	vítězný
pouze	pouze	k6eAd1	pouze
33	[number]	k4	33
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
snášelo	snášet	k5eAaImAgNnS	snášet
více	hodně	k6eAd2	hodně
střel	střela	k1gFnPc2	střela
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgMnS	moct
vylepšovat	vylepšovat	k5eAaImF	vylepšovat
statistiky	statistika	k1gFnSc2	statistika
<g/>
.	.	kIx.	.
</s>
<s>
Obdržených	obdržený	k2eAgFnPc2d1	obdržená
branek	branka	k1gFnPc2	branka
měl	mít	k5eAaImAgInS	mít
avšak	avšak	k8xC	avšak
nejvíce	nejvíce	k6eAd1	nejvíce
za	za	k7c4	za
celou	celá	k1gFnSc4	celá
svoji	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
-	-	kIx~	-
161	[number]	k4	161
branek	branka	k1gFnPc2	branka
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
branek	branka	k1gFnPc2	branka
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
2,83	[number]	k4	2,83
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
toho	ten	k3xDgNnSc2	ten
všeho	všecek	k3xTgNnSc2	všecek
jej	on	k3xPp3gMnSc4	on
trápily	trápit	k5eAaImAgInP	trápit
zdravotní	zdravotní	k2eAgInSc4d1	zdravotní
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
všechno	všechen	k3xTgNnSc4	všechen
byl	být	k5eAaImAgMnS	být
poprvé	poprvé	k6eAd1	poprvé
nominován	nominovat	k5eAaBmNgMnS	nominovat
do	do	k7c2	do
All-Star	All-Stara	k1gFnPc2	All-Stara
Game	game	k1gInSc1	game
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
"	"	kIx"	"
<g/>
prázdninách	prázdniny	k1gFnPc6	prázdniny
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
Buffalu	Buffal	k1gInSc6	Buffal
uklidnila	uklidnit	k5eAaPmAgFnS	uklidnit
<g/>
,	,	kIx,	,
hra	hra	k1gFnSc1	hra
se	se	k3xPyFc4	se
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
základní	základní	k2eAgFnSc2d1	základní
části	část	k1gFnSc2	část
s	s	k7c7	s
přehledem	přehled	k1gInSc7	přehled
Sabres	Sabresa	k1gFnPc2	Sabresa
zvítězilo	zvítězit	k5eAaPmAgNnS	zvítězit
Severovýchodní	severovýchodní	k2eAgFnSc4d1	severovýchodní
divizi	divize	k1gFnSc4	divize
s	s	k7c7	s
náskokem	náskok	k1gInSc7	náskok
osmi	osm	k4xCc2	osm
bodů	bod	k1gInPc2	bod
nad	nad	k7c7	nad
druhým	druhý	k4xOgInSc7	druhý
Pittsburghem	Pittsburgh	k1gInSc7	Pittsburgh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgNnSc6	první
utkání	utkání	k1gNnSc6	utkání
playoff	playoff	k1gInSc4	playoff
se	se	k3xPyFc4	se
Buffalo	Buffalo	k1gMnPc3	Buffalo
střetlo	střetnout	k5eAaPmAgNnS	střetnout
s	s	k7c7	s
Ottawou	Ottawa	k1gFnSc7	Ottawa
Senators	Senatorsa	k1gFnPc2	Senatorsa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nakonec	nakonec	k6eAd1	nakonec
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
Dominik	Dominik	k1gMnSc1	Dominik
si	se	k3xPyFc3	se
v	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
zápase	zápas	k1gInSc6	zápas
poranil	poranit	k5eAaPmAgMnS	poranit
koleno	koleno	k1gNnSc4	koleno
<g/>
.	.	kIx.	.
</s>
<s>
Buffalský	buffalský	k2eAgMnSc1d1	buffalský
novinář	novinář	k1gMnSc1	novinář
Jim	on	k3xPp3gMnPc3	on
Kelley	Kellea	k1gFnPc1	Kellea
napsal	napsat	k5eAaBmAgInS	napsat
v	v	k7c6	v
Buffalo	Buffalo	k1gFnSc6	Buffalo
News	News	k1gInSc4	News
o	o	k7c6	o
Haškovi	Hašek	k1gMnSc6	Hašek
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
Hašek	Hašek	k1gMnSc1	Hašek
simulant	simulant	k1gMnSc1	simulant
a	a	k8xC	a
zrádce	zrádce	k1gMnSc1	zrádce
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
po	po	k7c6	po
zápase	zápas	k1gInSc6	zápas
byl	být	k5eAaImAgInS	být
totiž	totiž	k9	totiž
viděn	vidět	k5eAaImNgMnS	vidět
odcházet	odcházet	k5eAaImF	odcházet
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Františka	František	k1gMnSc2	František
Musila	Musil	k1gMnSc2	Musil
<g/>
,	,	kIx,	,
hráče	hráč	k1gMnSc2	hráč
Ottawy	Ottawa	k1gFnSc2	Ottawa
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
Hašek	Hašek	k1gMnSc1	Hašek
potkal	potkat	k5eAaPmAgMnS	potkat
osobně	osobně	k6eAd1	osobně
Kelleyho	Kelley	k1gMnSc4	Kelley
<g/>
,	,	kIx,	,
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
a	a	k8xC	a
odtrhal	odtrhat	k5eAaPmAgMnS	odtrhat
mu	on	k3xPp3gMnSc3	on
několik	několik	k4yIc4	několik
knoflíků	knoflík	k1gMnPc2	knoflík
u	u	k7c2	u
košile	košile	k1gFnSc2	košile
<g/>
.	.	kIx.	.
</s>
<s>
Hašek	Hašek	k1gMnSc1	Hašek
byl	být	k5eAaImAgMnS	být
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
výbuch	výbuch	k1gInSc4	výbuch
vzteku	vztek	k1gInSc2	vztek
potrestán	potrestán	k2eAgInSc1d1	potrestán
distancem	distanec	k1gInSc7	distanec
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
zápasy	zápas	k1gInPc4	zápas
a	a	k8xC	a
pokutou	pokuta	k1gFnSc7	pokuta
10	[number]	k4	10
000	[number]	k4	000
$	$	kIx~	$
<g/>
.	.	kIx.	.
</s>
<s>
Hašek	Hašek	k1gMnSc1	Hašek
již	již	k6eAd1	již
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
o	o	k7c4	o
Stanley	Stanlea	k1gFnPc4	Stanlea
Cup	cup	k1gInSc1	cup
nenastoupil	nastoupit	k5eNaPmAgInS	nastoupit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mezitím	mezitím	k6eAd1	mezitím
Buffalo	Buffalo	k1gMnPc2	Buffalo
podlehlo	podlehnout	k5eAaPmAgNnS	podlehnout
Philadelphii	Philadelphia	k1gFnSc4	Philadelphia
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
ročníku	ročník	k1gInSc6	ročník
byl	být	k5eAaImAgMnS	být
Hašek	Hašek	k1gMnSc1	Hašek
za	za	k7c4	za
výsledky	výsledek	k1gInPc4	výsledek
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
soutěže	soutěž	k1gFnSc2	soutěž
odměněn	odměněn	k2eAgInSc4d1	odměněn
již	již	k6eAd1	již
potřetí	potřetí	k4xO	potřetí
Vezinou	Vezina	k1gFnSc7	Vezina
Trophy	Tropha	k1gFnSc2	Tropha
a	a	k8xC	a
nově	nově	k6eAd1	nově
Hart	Hart	k2eAgInSc4d1	Hart
Memorial	Memorial	k1gInSc4	Memorial
Trophy	Tropha	k1gFnSc2	Tropha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
uděluje	udělovat	k5eAaImIp3nS	udělovat
nejužitečnějšímu	užitečný	k2eAgMnSc3d3	nejužitečnější
hráči	hráč	k1gMnSc3	hráč
ligy	liga	k1gFnSc2	liga
a	a	k8xC	a
obdobnou	obdobný	k2eAgFnSc7d1	obdobná
Lester	Lester	k1gMnSc1	Lester
B.	B.	kA	B.
Pearson	Pearson	k1gMnSc1	Pearson
Award	Award	k1gMnSc1	Award
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
udělují	udělovat	k5eAaImIp3nP	udělovat
samotní	samotný	k2eAgMnPc1d1	samotný
hráči	hráč	k1gMnPc1	hráč
někomu	někdo	k3yInSc3	někdo
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
středu	střed	k1gInSc2	střed
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
ještě	ještě	k6eAd1	ještě
k	k	k7c3	k
ocenění	ocenění	k1gNnSc3	ocenění
přidá	přidat	k5eAaPmIp3nS	přidat
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
hokejku	hokejka	k1gFnSc4	hokejka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
nastávající	nastávající	k2eAgFnSc4d1	nastávající
sezónu	sezóna	k1gFnSc4	sezóna
nastaly	nastat	k5eAaPmAgInP	nastat
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
Buffalo	Buffalo	k1gFnSc2	Buffalo
Sabres	Sabresa	k1gFnPc2	Sabresa
velké	velký	k2eAgFnSc2d1	velká
změny	změna	k1gFnSc2	změna
-	-	kIx~	-
generálního	generální	k2eAgMnSc2d1	generální
manažera	manažer	k1gMnSc2	manažer
Johna	John	k1gMnSc2	John
Mucklera	Muckler	k1gMnSc2	Muckler
vystřídá	vystřídat	k5eAaPmIp3nS	vystřídat
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
Darcy	Darca	k1gMnSc2	Darca
Regier	Regier	k1gInSc4	Regier
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
novopečeného	novopečený	k2eAgMnSc4d1	novopečený
držitele	držitel	k1gMnSc4	držitel
Jack	Jack	k1gInSc4	Jack
Adams	Adams	k1gInSc4	Adams
Award	Awardo	k1gNnPc2	Awardo
Teda	Ted	k1gMnSc2	Ted
Nolana	Nolan	k1gMnSc2	Nolan
nahradí	nahradit	k5eAaPmIp3nP	nahradit
Lindy	Linda	k1gFnPc1	Linda
Ruff	Ruff	k1gInSc4	Ruff
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
se	se	k3xPyFc4	se
týmu	tým	k1gInSc3	tým
nedařilo	dařit	k5eNaImAgNnS	dařit
<g/>
,	,	kIx,	,
Hašek	Hašek	k1gMnSc1	Hašek
inkasoval	inkasovat	k5eAaBmAgMnS	inkasovat
zbytečné	zbytečný	k2eAgInPc4d1	zbytečný
góly	gól	k1gInPc4	gól
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
dokonce	dokonce	k9	dokonce
odsunut	odsunout	k5eAaPmNgInS	odsunout
do	do	k7c2	do
role	role	k1gFnSc2	role
druhého	druhý	k4xOgMnSc2	druhý
brankáře	brankář	k1gMnSc2	brankář
<g/>
,	,	kIx,	,
v	v	k7c6	v
brance	branka	k1gFnSc6	branka
jej	on	k3xPp3gMnSc4	on
zastoupil	zastoupit	k5eAaPmAgMnS	zastoupit
Steve	Steve	k1gMnSc1	Steve
Shields	Shieldsa	k1gFnPc2	Shieldsa
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
přelomu	přelom	k1gInSc2	přelom
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
forma	forma	k1gFnSc1	forma
vrátila	vrátit	k5eAaPmAgFnS	vrátit
a	a	k8xC	a
opět	opět	k6eAd1	opět
obsadil	obsadit	k5eAaPmAgMnS	obsadit
post	post	k1gInSc4	post
hlavního	hlavní	k2eAgMnSc2d1	hlavní
brankáře	brankář	k1gMnSc2	brankář
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
započal	započnout	k5eAaPmAgInS	započnout
Turnaj	turnaj	k1gInSc1	turnaj
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byl	být	k5eAaImAgInS	být
označován	označovat	k5eAaImNgInS	označovat
olympijský	olympijský	k2eAgInSc1d1	olympijský
hokejový	hokejový	k2eAgInSc1d1	hokejový
turnaj	turnaj	k1gInSc1	turnaj
konaný	konaný	k2eAgInSc1d1	konaný
v	v	k7c6	v
Naganu	Nagano	k1gNnSc6	Nagano
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
ročník	ročník	k1gInSc1	ročník
byl	být	k5eAaImAgInS	být
přelomový	přelomový	k2eAgMnSc1d1	přelomový
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
olympijský	olympijský	k2eAgInSc1d1	olympijský
výbor	výbor	k1gInSc1	výbor
se	se	k3xPyFc4	se
dohodl	dohodnout	k5eAaPmAgInS	dohodnout
s	s	k7c7	s
IIHF	IIHF	kA	IIHF
a	a	k8xC	a
NHL	NHL	kA	NHL
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejslavnější	slavný	k2eAgFnSc1d3	nejslavnější
ligová	ligový	k2eAgFnSc1d1	ligová
soutěž	soutěž	k1gFnSc1	soutěž
bude	být	k5eAaImBp3nS	být
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
přerušena	přerušit	k5eAaPmNgFnS	přerušit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c6	na
olympijském	olympijský	k2eAgInSc6d1	olympijský
turnaji	turnaj	k1gInSc6	turnaj
mohli	moct	k5eAaImAgMnP	moct
startovat	startovat	k5eAaBmF	startovat
i	i	k9	i
hráči	hráč	k1gMnPc1	hráč
z	z	k7c2	z
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jedničkou	jednička	k1gFnSc7	jednička
brankářů	brankář	k1gMnPc2	brankář
českého	český	k2eAgInSc2d1	český
výběru	výběr	k1gInSc2	výběr
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
reprezentanti	reprezentant	k1gMnPc1	reprezentant
byli	být	k5eAaImAgMnP	být
nasazeni	nasadit	k5eAaPmNgMnP	nasadit
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
C	C	kA	C
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
střetu	střet	k1gInSc6	střet
s	s	k7c7	s
Finskem	Finsko	k1gNnSc7	Finsko
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
Kazachstánem	Kazachstán	k1gInSc7	Kazachstán
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
a	a	k8xC	a
Ruskem	Rusko	k1gNnSc7	Rusko
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
skončili	skončit	k5eAaPmAgMnP	skončit
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinálovém	čtvrtfinálový	k2eAgInSc6d1	čtvrtfinálový
zápase	zápas	k1gInSc6	zápas
se	se	k3xPyFc4	se
střetli	střetnout	k5eAaPmAgMnP	střetnout
s	s	k7c7	s
hokejisty	hokejista	k1gMnPc7	hokejista
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
porazili	porazit	k5eAaPmAgMnP	porazit
nečekaně	nečekaně	k6eAd1	nečekaně
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
zápase	zápas	k1gInSc6	zápas
se	se	k3xPyFc4	se
představil	představit	k5eAaPmAgInS	představit
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
nejlepšími	dobrý	k2eAgInPc7d3	nejlepší
výkony	výkon	k1gInPc7	výkon
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
semifinále	semifinále	k1gNnSc6	semifinále
se	se	k3xPyFc4	se
střetli	střetnout	k5eAaPmAgMnP	střetnout
výběrem	výběr	k1gInSc7	výběr
z	z	k7c2	z
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
po	po	k7c6	po
nerozhodných	rozhodný	k2eNgInPc6d1	nerozhodný
60	[number]	k4	60
minutách	minuta	k1gFnPc6	minuta
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
následném	následný	k2eAgNnSc6d1	následné
bezbrankovém	bezbrankový	k2eAgNnSc6d1	bezbrankové
prodloužení	prodloužení	k1gNnSc6	prodloužení
musel	muset	k5eAaImAgMnS	muset
být	být	k5eAaImF	být
rozhodnut	rozhodnut	k2eAgInSc4d1	rozhodnut
nájezdy	nájezd	k1gInPc4	nájezd
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
obstál	obstát	k5eAaPmAgMnS	obstát
s	s	k7c7	s
čistým	čistý	k2eAgInSc7d1	čistý
štítem	štít	k1gInSc7	štít
a	a	k8xC	a
díky	díky	k7c3	díky
Robertu	Robert	k1gMnSc3	Robert
Reichlovi	Reichel	k1gMnSc3	Reichel
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
jako	jako	k8xC	jako
jediný	jediný	k2eAgMnSc1d1	jediný
proměnil	proměnit	k5eAaPmAgMnS	proměnit
<g/>
,	,	kIx,	,
čeští	český	k2eAgMnPc1d1	český
sportovci	sportovec	k1gMnPc1	sportovec
slavili	slavit	k5eAaImAgMnP	slavit
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Finálový	finálový	k2eAgInSc4d1	finálový
zápas	zápas	k1gInSc4	zápas
nastoupili	nastoupit	k5eAaPmAgMnP	nastoupit
Češi	Čech	k1gMnPc1	Čech
proti	proti	k7c3	proti
ruským	ruský	k2eAgMnPc3d1	ruský
hokejistům	hokejista	k1gMnPc3	hokejista
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgMnPc7	který
již	již	k9	již
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
jednou	jednou	k6eAd1	jednou
hráli	hrát	k5eAaImAgMnP	hrát
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
ve	v	k7c6	v
výtečné	výtečný	k2eAgFnSc6d1	výtečná
formě	forma	k1gFnSc6	forma
a	a	k8xC	a
vychytal	vychytat	k5eAaPmAgMnS	vychytat
druhý	druhý	k4xOgInSc4	druhý
zápas	zápas	k1gInSc4	zápas
bez	bez	k7c2	bez
obdržené	obdržený	k2eAgFnSc2d1	obdržená
branky	branka	k1gFnSc2	branka
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc4d1	jediný
"	"	kIx"	"
<g/>
zlatý	zlatý	k2eAgInSc4d1	zlatý
<g/>
"	"	kIx"	"
olympijský	olympijský	k2eAgInSc4d1	olympijský
gól	gól	k1gInSc4	gól
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
padl	padnout	k5eAaImAgMnS	padnout
<g/>
,	,	kIx,	,
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
hokejky	hokejka	k1gFnSc2	hokejka
Petra	Petr	k1gMnSc2	Petr
Svobody	Svoboda	k1gMnSc2	Svoboda
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
historicky	historicky	k6eAd1	historicky
první	první	k4xOgInSc4	první
olympijský	olympijský	k2eAgInSc4d1	olympijský
titul	titul	k1gInSc4	titul
v	v	k7c6	v
ledním	lední	k2eAgInSc6d1	lední
hokeji	hokej	k1gInSc6	hokej
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
olympijském	olympijský	k2eAgInSc6d1	olympijský
turnaji	turnaj	k1gInSc6	turnaj
objevili	objevit	k5eAaPmAgMnP	objevit
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
světoví	světový	k2eAgMnPc1d1	světový
hráči	hráč	k1gMnPc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
týmu	tým	k1gInSc3	tým
musí	muset	k5eAaImIp3nS	muset
dokončit	dokončit	k5eAaPmF	dokončit
základní	základní	k2eAgFnSc4d1	základní
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
celkově	celkově	k6eAd1	celkově
odehrál	odehrát	k5eAaPmAgInS	odehrát
72	[number]	k4	72
zápasů	zápas	k1gInPc2	zápas
s	s	k7c7	s
průměrným	průměrný	k2eAgInSc7d1	průměrný
gólem	gól	k1gInSc7	gól
obdrženým	obdržený	k2eAgInSc7d1	obdržený
za	za	k7c4	za
zápas	zápas	k1gInSc4	zápas
2,09	[number]	k4	2,09
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
kariéře	kariéra	k1gFnSc6	kariéra
rekordním	rekordní	k2eAgInSc7d1	rekordní
počtem	počet	k1gInSc7	počet
13	[number]	k4	13
shutoutů	shutout	k1gMnPc2	shutout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgNnSc6	první
kole	kolo	k1gNnSc6	kolo
playoff	playoff	k1gInSc4	playoff
Buffalo	Buffalo	k1gMnPc2	Buffalo
narazilo	narazit	k5eAaPmAgNnS	narazit
na	na	k7c4	na
soupeře	soupeř	k1gMnSc4	soupeř
z	z	k7c2	z
Philadelphie	Philadelphia	k1gFnSc2	Philadelphia
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mu	on	k3xPp3gMnSc3	on
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
konference	konference	k1gFnSc2	konference
porazí	porazit	k5eAaPmIp3nS	porazit
bez	bez	k7c2	bez
velkých	velký	k2eAgInPc2d1	velký
problémů	problém	k1gInPc2	problém
Montrealské	montrealský	k2eAgFnSc2d1	Montrealská
hokejisty	hokejista	k1gMnSc2	hokejista
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
postoupilo	postoupit	k5eAaPmAgNnS	postoupit
po	po	k7c6	po
osmnácti	osmnáct	k4xCc6	osmnáct
letech	léto	k1gNnPc6	léto
do	do	k7c2	do
finálového	finálový	k2eAgNnSc2d1	finálové
střetnutí	střetnutí	k1gNnSc2	střetnutí
východní	východní	k2eAgFnSc2d1	východní
konference	konference	k1gFnSc2	konference
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
však	však	k9	však
Buffalo	Buffalo	k1gFnSc1	Buffalo
naráží	narážet	k5eAaPmIp3nS	narážet
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc4	první
utkání	utkání	k1gNnSc4	utkání
ještě	ještě	k6eAd1	ještě
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
poté	poté	k6eAd1	poté
zkušenější	zkušený	k2eAgInSc4d2	zkušenější
Capitals	Capitals	k1gInSc4	Capitals
zaváhali	zaváhat	k5eAaPmAgMnP	zaváhat
jen	jen	k9	jen
jednou	jeden	k4xCgFnSc7	jeden
a	a	k8xC	a
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
sérii	série	k1gFnSc4	série
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
sezóny	sezóna	k1gFnSc2	sezóna
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
vyznamenán	vyznamenat	k5eAaPmNgInS	vyznamenat
počtvrté	počtvrté	k4xO	počtvrté
Vezina	Vezino	k1gNnSc2	Vezino
Trophy	Tropha	k1gFnSc2	Tropha
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jediný	jediný	k2eAgMnSc1d1	jediný
brankář	brankář	k1gMnSc1	brankář
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
podruhé	podruhé	k6eAd1	podruhé
Hart	Hart	k1gInSc4	Hart
Trophy	Tropha	k1gFnSc2	Tropha
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
Lester	Lester	k1gMnSc1	Lester
B.	B.	kA	B.
Pearson	Pearson	k1gMnSc1	Pearson
Award	Award	k1gMnSc1	Award
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
byl	být	k5eAaImAgMnS	být
vyhlášen	vyhlášen	k2eAgMnSc1d1	vyhlášen
taktéž	taktéž	k?	taktéž
popáté	popáté	k4xO	popáté
vítězem	vítěz	k1gMnSc7	vítěz
ankety	anketa	k1gFnSc2	anketa
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
hokejka	hokejka	k1gFnSc1	hokejka
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
sportovcem	sportovec	k1gMnSc7	sportovec
roku	rok	k1gInSc2	rok
a	a	k8xC	a
hokejistou	hokejista	k1gMnSc7	hokejista
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
nové	nový	k2eAgFnSc2d1	nová
sezóny	sezóna	k1gFnSc2	sezóna
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Jaromírem	Jaromír	k1gMnSc7	Jaromír
Jágrem	Jágr	k1gMnSc7	Jágr
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Utkání	utkání	k1gNnSc1	utkání
snů	sen	k1gInPc2	sen
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
českému	český	k2eAgInSc3d1	český
výběru	výběr	k1gInSc3	výběr
z	z	k7c2	z
Nagana	Nagano	k1gNnSc2	Nagano
postavily	postavit	k5eAaPmAgFnP	postavit
největší	veliký	k2eAgFnPc1d3	veliký
světové	světový	k2eAgFnPc1d1	světová
hokejové	hokejový	k2eAgFnPc1d1	hokejová
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
výběr	výběr	k1gInSc1	výběr
porazil	porazit	k5eAaPmAgInS	porazit
světový	světový	k2eAgInSc1d1	světový
výběr	výběr	k1gInSc1	výběr
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
část	část	k1gFnSc1	část
vychází	vycházet	k5eAaImIp3nS	vycházet
Buffalu	Buffala	k1gFnSc4	Buffala
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
předchozí	předchozí	k2eAgInSc4d1	předchozí
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
91	[number]	k4	91
body	bod	k1gInPc4	bod
skončilo	skončit	k5eAaPmAgNnS	skončit
na	na	k7c6	na
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
divizi	divize	k1gFnSc6	divize
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
playoff	playoff	k1gInSc4	playoff
roznese	roznést	k5eAaPmIp3nS	roznést
Buffalo	Buffalo	k1gFnSc1	Buffalo
na	na	k7c6	na
kopytech	kopyto	k1gNnPc6	kopyto
Ottawu	Ottawa	k1gFnSc4	Ottawa
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
podlehne	podlehnout	k5eAaPmIp3nS	podlehnout
i	i	k9	i
Boston	Boston	k1gInSc1	Boston
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
a	a	k8xC	a
Toronto	Toronto	k1gNnSc1	Toronto
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finálovém	finálový	k2eAgNnSc6d1	finálové
klání	klání	k1gNnSc6	klání
Stanley	Stanlea	k1gFnSc2	Stanlea
Cupu	cup	k1gInSc2	cup
<g/>
,	,	kIx,	,
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
poprvé	poprvé	k6eAd1	poprvé
<g/>
,	,	kIx,	,
Buffalo	Buffalo	k1gFnSc3	Buffalo
Sabres	Sabresa	k1gFnPc2	Sabresa
nastoupilo	nastoupit	k5eAaPmAgNnS	nastoupit
proti	proti	k7c3	proti
favorizovanému	favorizovaný	k2eAgInSc3d1	favorizovaný
Dallasu	Dallas	k1gInSc3	Dallas
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
zápas	zápas	k1gInSc4	zápas
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
po	po	k7c6	po
prodloužení	prodloužení	k1gNnSc6	prodloužení
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
poté	poté	k6eAd1	poté
následovaly	následovat	k5eAaImAgInP	následovat
dvě	dva	k4xCgFnPc4	dva
prohry	prohra	k1gFnPc4	prohra
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
a	a	k8xC	a
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Stav	stav	k1gInSc4	stav
série	série	k1gFnSc2	série
srovnali	srovnat	k5eAaPmAgMnP	srovnat
po	po	k7c6	po
posledním	poslední	k2eAgInSc6d1	poslední
vyhraném	vyhraný	k2eAgInSc6d1	vyhraný
zápase	zápas	k1gInSc6	zápas
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
již	již	k9	již
po	po	k7c6	po
zápasech	zápas	k1gInPc6	zápas
slavil	slavit	k5eAaImAgInS	slavit
pouze	pouze	k6eAd1	pouze
Dallas	Dallas	k1gInSc1	Dallas
<g/>
,	,	kIx,	,
s	s	k7c7	s
výhrami	výhra	k1gFnPc7	výhra
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
a	a	k8xC	a
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1998	[number]	k4	1998
<g/>
/	/	kIx~	/
<g/>
1999	[number]	k4	1999
držitelem	držitel	k1gMnSc7	držitel
Stanley	Stanlea	k1gFnSc2	Stanlea
Cupu	cup	k1gInSc2	cup
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sezóně	sezóna	k1gFnSc6	sezóna
si	se	k3xPyFc3	se
Dominik	Dominik	k1gMnSc1	Dominik
připsal	připsat	k5eAaPmAgMnS	připsat
na	na	k7c4	na
konto	konto	k1gNnSc4	konto
již	již	k6eAd1	již
popáté	popáté	k4xO	popáté
trofej	trofej	k1gFnSc1	trofej
pro	pro	k7c4	pro
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
gólmana	gólman	k1gMnSc4	gólman
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
sezónou	sezóna	k1gFnSc7	sezóna
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sezóna	sezóna	k1gFnSc1	sezóna
1999	[number]	k4	1999
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
z	z	k7c2	z
rodinných	rodinný	k2eAgInPc2d1	rodinný
důvodů	důvod	k1gInPc2	důvod
jeho	jeho	k3xOp3gFnSc1	jeho
poslední	poslední	k2eAgFnSc1d1	poslední
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
obnovily	obnovit	k5eAaPmAgInP	obnovit
jeho	jeho	k3xOp3gInPc1	jeho
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
třísly	tříslo	k1gNnPc7	tříslo
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
Vánoc	Vánoce	k1gFnPc2	Vánoce
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
jeho	jeho	k3xOp3gNnSc4	jeho
zranění	zranění	k1gNnSc4	zranění
byla	být	k5eAaImAgFnS	být
dlouhodobější	dlouhodobý	k2eAgFnSc1d2	dlouhodobější
<g/>
,	,	kIx,	,
od	od	k7c2	od
profesionálního	profesionální	k2eAgInSc2d1	profesionální
hokeje	hokej	k1gInSc2	hokej
by	by	kYmCp3nS	by
prozatím	prozatím	k6eAd1	prozatím
neodstoupil	odstoupit	k5eNaPmAgMnS	odstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
led	led	k1gInSc4	led
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
sezóny	sezóna	k1gFnSc2	sezóna
měl	mít	k5eAaImAgInS	mít
odehráno	odehrát	k5eAaPmNgNnS	odehrát
35	[number]	k4	35
zápasů	zápas	k1gInPc2	zápas
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
gólu	gól	k1gInSc2	gól
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
2.21	[number]	k4	2.21
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
playoff	playoff	k1gInSc4	playoff
se	se	k3xPyFc4	se
Buffalo	Buffalo	k1gFnSc3	Buffalo
Sabres	Sabresa	k1gFnPc2	Sabresa
střetlo	střetnout	k5eAaPmAgNnS	střetnout
s	s	k7c7	s
Philadelphií	Philadelphia	k1gFnSc7	Philadelphia
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pěti	pět	k4xCc6	pět
zápasech	zápas	k1gInPc6	zápas
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
ještě	ještě	k6eAd1	ještě
s	s	k7c7	s
hokejem	hokej	k1gInSc7	hokej
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
sezóny	sezóna	k1gFnSc2	sezóna
nasbíralo	nasbírat	k5eAaPmAgNnS	nasbírat
Buffalo	Buffalo	k1gFnSc4	Buffalo
v	v	k7c6	v
82	[number]	k4	82
zápasech	zápas	k1gInPc6	zápas
98	[number]	k4	98
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
vyřazovací	vyřazovací	k2eAgFnSc2d1	vyřazovací
části	část	k1gFnSc2	část
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
Sabres	Sabres	k1gInSc4	Sabres
ze	z	k7c2	z
šesté	šestý	k4xOgFnSc2	šestý
příčky	příčka	k1gFnSc2	příčka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
kolo	kolo	k1gNnSc1	kolo
bylo	být	k5eAaImAgNnS	být
opět	opět	k6eAd1	opět
souzeno	soudit	k5eAaImNgNnS	soudit
pro	pro	k7c4	pro
střet	střet	k1gInSc4	střet
s	s	k7c7	s
Philadelphií	Philadelphia	k1gFnSc7	Philadelphia
Flyers	Flyersa	k1gFnPc2	Flyersa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
Roman	Roman	k1gMnSc1	Roman
Čechmánek	Čechmánek	k1gMnSc1	Čechmánek
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
ze	z	k7c2	z
střetu	střet	k1gInSc2	střet
vítězněji	vítězně	k6eAd2	vítězně
Buffalo	Buffalo	k1gFnSc4	Buffalo
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
Letce	letka	k1gFnSc3	letka
porazilo	porazit	k5eAaPmAgNnS	porazit
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
na	na	k7c4	na
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
se	se	k3xPyFc4	se
utkalo	utkat	k5eAaPmAgNnS	utkat
s	s	k7c7	s
Pittsburghem	Pittsburgh	k1gInSc7	Pittsburgh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
slavil	slavit	k5eAaImAgInS	slavit
svůj	svůj	k3xOyFgInSc4	svůj
návrat	návrat	k1gInSc4	návrat
Mario	Mario	k1gMnSc1	Mario
Lemieux	Lemieux	k1gInSc1	Lemieux
<g/>
.	.	kIx.	.
</s>
<s>
Sérii	série	k1gFnSc4	série
po	po	k7c6	po
šesti	šest	k4xCc6	šest
zápasech	zápas	k1gInPc6	zápas
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
výhra	výhra	k1gFnSc1	výhra
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
po	po	k7c6	po
prodloužení	prodloužení	k1gNnSc6	prodloužení
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Tučňáků	tučňák	k1gMnPc2	tučňák
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k6eAd1	tak
jeho	jeho	k3xOp3gFnSc1	jeho
cesta	cesta	k1gFnSc1	cesta
za	za	k7c2	za
Stanley	Stanlea	k1gFnSc2	Stanlea
Cupem	cup	k1gInSc7	cup
opět	opět	k6eAd1	opět
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
již	již	k6eAd1	již
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
kole	kolo	k1gNnSc6	kolo
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
trofej	trofej	k1gFnSc4	trofej
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sezóně	sezóna	k1gFnSc6	sezóna
byl	být	k5eAaImAgInS	být
pošesté	pošesté	k4xO	pošesté
oceněn	oceněn	k2eAgMnSc1d1	oceněn
Vezinou	Vezina	k1gFnSc7	Vezina
Trophy	Tropha	k1gFnSc2	Tropha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dohodě	dohoda	k1gFnSc6	dohoda
se	se	k3xPyFc4	se
Sabres	Sabres	k1gMnSc1	Sabres
je	být	k5eAaImIp3nS	být
Hašek	Hašek	k1gMnSc1	Hašek
uvolněn	uvolněn	k2eAgMnSc1d1	uvolněn
jako	jako	k8xS	jako
volný	volný	k2eAgMnSc1d1	volný
hráč	hráč	k1gMnSc1	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přestup	přestup	k1gInSc4	přestup
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
týmy	tým	k1gInPc7	tým
-	-	kIx~	-
Detroit	Detroit	k1gInSc1	Detroit
Red	Red	k1gFnSc2	Red
Wings	Wingsa	k1gFnPc2	Wingsa
a	a	k8xC	a
St.	st.	kA	st.
Louis	louis	k1gInSc4	louis
Blues	blues	k1gNnSc2	blues
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
je	být	k5eAaImIp3nS	být
vybrán	vybrán	k2eAgInSc1d1	vybrán
Detroit	Detroit	k1gInSc1	Detroit
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
do	do	k7c2	do
Buffala	Buffala	k1gFnSc2	Buffala
poslal	poslat	k5eAaPmAgMnS	poslat
Vjačeslava	Vjačeslav	k1gMnSc4	Vjačeslav
Kozlova	Kozlův	k2eAgMnSc4d1	Kozlův
a	a	k8xC	a
volbu	volba	k1gFnSc4	volba
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
draftu	draft	k1gInSc2	draft
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
jeho	jeho	k3xOp3gFnSc4	jeho
začaly	začít	k5eAaPmAgFnP	začít
opět	opět	k6eAd1	opět
trápit	trápit	k5eAaImF	trápit
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
po	po	k7c6	po
čase	čas	k1gInSc6	čas
naštěstí	naštěstí	k6eAd1	naštěstí
odezněly	odeznět	k5eAaPmAgInP	odeznět
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
jeho	jeho	k3xOp3gInSc7	jeho
příchodem	příchod	k1gInSc7	příchod
do	do	k7c2	do
klubu	klub	k1gInSc2	klub
byl	být	k5eAaImAgInS	být
jedničkou	jednička	k1gFnSc7	jednička
v	v	k7c6	v
brance	branka	k1gFnSc6	branka
Chris	Chris	k1gFnSc2	Chris
Osgood	Osgood	k1gInSc1	Osgood
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
nové	nový	k2eAgFnSc2d1	nová
posily	posila	k1gFnSc2	posila
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Islanders	Islandersa	k1gFnPc2	Islandersa
<g/>
.	.	kIx.	.
</s>
<s>
Začátek	začátek	k1gInSc1	začátek
nové	nový	k2eAgFnSc2d1	nová
sezóny	sezóna	k1gFnSc2	sezóna
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařil	podařit	k5eAaPmAgInS	podařit
a	a	k8xC	a
do	do	k7c2	do
olympijské	olympijský	k2eAgFnSc2d1	olympijská
přestávky	přestávka	k1gFnSc2	přestávka
vychytal	vychytat	k5eAaPmAgMnS	vychytat
třikrát	třikrát	k6eAd1	třikrát
shutout	shutout	k5eAaImF	shutout
<g/>
.	.	kIx.	.
</s>
<s>
Turnaj	turnaj	k1gInSc1	turnaj
v	v	k7c6	v
Salt	salto	k1gNnPc2	salto
Lake	Lak	k1gMnSc2	Lak
City	City	k1gFnSc2	City
byl	být	k5eAaImAgInS	být
teprve	teprve	k6eAd1	teprve
druhý	druhý	k4xOgMnSc1	druhý
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
i	i	k9	i
pro	pro	k7c4	pro
hráče	hráč	k1gMnPc4	hráč
z	z	k7c2	z
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
výběr	výběr	k1gInSc1	výběr
byl	být	k5eAaImAgInS	být
nasazen	nasadit	k5eAaPmNgInS	nasadit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
C	C	kA	C
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
střetl	střetnout	k5eAaPmAgMnS	střetnout
s	s	k7c7	s
týmy	tým	k1gInPc7	tým
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Švédska	Švédsko	k1gNnSc2	Švédsko
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kanady	Kanada	k1gFnSc2	Kanada
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
Česko	Česko	k1gNnSc1	Česko
postupuje	postupovat	k5eAaImIp3nS	postupovat
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
body	bod	k1gInPc7	bod
ze	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
pozice	pozice	k1gFnSc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
se	se	k3xPyFc4	se
střetl	střetnout	k5eAaPmAgMnS	střetnout
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
zde	zde	k6eAd1	zde
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
třetině	třetina	k1gFnSc6	třetina
po	po	k7c6	po
úniku	únik	k1gInSc6	únik
a	a	k8xC	a
střele	střela	k1gFnSc6	střela
Maxima	Maxim	k1gMnSc2	Maxim
Afinogenova	Afinogenův	k2eAgMnSc2d1	Afinogenův
český	český	k2eAgInSc4d1	český
tým	tým	k1gInSc4	tým
prohrál	prohrát	k5eAaPmAgMnS	prohrát
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
utkání	utkání	k1gNnSc2	utkání
již	již	k9	již
skóre	skóre	k1gNnPc4	skóre
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
nezměněno	změněn	k2eNgNnSc1d1	nezměněno
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
tak	tak	k6eAd1	tak
česká	český	k2eAgFnSc1d1	Česká
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
základní	základní	k2eAgFnSc2d1	základní
části	část	k1gFnSc2	část
NHL	NHL	kA	NHL
odehrál	odehrát	k5eAaPmAgInS	odehrát
65	[number]	k4	65
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
41	[number]	k4	41
vítězných	vítězný	k2eAgFnPc2d1	vítězná
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
obdržených	obdržený	k2eAgFnPc2d1	obdržená
branek	branka	k1gFnPc2	branka
2,17	[number]	k4	2,17
s	s	k7c7	s
celkově	celkově	k6eAd1	celkově
pěti	pět	k4xCc7	pět
shutouty	shutout	k1gMnPc7	shutout
<g/>
.	.	kIx.	.
</s>
<s>
Detroit	Detroit	k1gInSc1	Detroit
se	s	k7c7	s
116	[number]	k4	116
body	bod	k1gInPc7	bod
postoupil	postoupit	k5eAaPmAgInS	postoupit
do	do	k7c2	do
playoff	playoff	k1gInSc1	playoff
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
místa	místo	k1gNnSc2	místo
s	s	k7c7	s
President	president	k1gMnSc1	president
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Trophy	Troph	k1gMnPc7	Troph
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
konference	konference	k1gFnSc2	konference
se	se	k3xPyFc4	se
tým	tým	k1gInSc1	tým
střetl	střetnout	k5eAaPmAgInS	střetnout
s	s	k7c7	s
Vancouver	Vancouver	k1gInSc1	Vancouver
Canucks	Canucks	k1gInSc4	Canucks
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
první	první	k4xOgInSc4	první
společný	společný	k2eAgInSc4d1	společný
zápas	zápas	k1gInSc4	zápas
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
nakonec	nakonec	k6eAd1	nakonec
skončila	skončit	k5eAaPmAgFnS	skončit
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
pro	pro	k7c4	pro
Rudá	rudý	k2eAgNnPc4d1	Rudé
křídla	křídlo	k1gNnPc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Semifinále	semifinále	k1gNnSc7	semifinále
se	se	k3xPyFc4	se
Detroit	Detroit	k1gInSc1	Detroit
setkal	setkat	k5eAaPmAgInS	setkat
se	s	k7c7	s
St.	st.	kA	st.
Louis	louis	k1gInPc7	louis
Blues	blues	k1gNnSc2	blues
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
si	se	k3xPyFc3	se
Rudá	rudý	k2eAgFnSc1d1	rudá
Křídla	křídlo	k1gNnPc1	křídlo
dovolila	dovolit	k5eAaPmAgNnP	dovolit
jen	jen	k9	jen
jedno	jeden	k4xCgNnSc4	jeden
zaváhání	zaváhání	k1gNnSc4	zaváhání
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
prohrála	prohrát	k5eAaPmAgFnS	prohrát
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
vychytal	vychytat	k5eAaPmAgMnS	vychytat
dva	dva	k4xCgMnPc4	dva
shutouty	shutout	k1gMnPc4	shutout
a	a	k8xC	a
pro	pro	k7c4	pro
Detroit	Detroit	k1gInSc4	Detroit
to	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
postup	postup	k1gInSc4	postup
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
konference	konference	k1gFnSc2	konference
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
střetli	střetnout	k5eAaPmAgMnP	střetnout
Coloradem	Colorado	k1gNnSc7	Colorado
Avalanche	Avalanche	k1gNnSc2	Avalanche
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bráně	brána	k1gFnSc6	brána
protivníků	protivník	k1gMnPc2	protivník
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
jeho	jeho	k3xOp3gMnSc1	jeho
rival	rival	k1gMnSc1	rival
Patrick	Patrick	k1gMnSc1	Patrick
Roy	Roy	k1gMnSc1	Roy
<g/>
.	.	kIx.	.
</s>
<s>
Konečný	konečný	k2eAgInSc1d1	konečný
výsledek	výsledek	k1gInSc1	výsledek
střetnutí	střetnutí	k1gNnSc2	střetnutí
byl	být	k5eAaImAgInS	být
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
pro	pro	k7c4	pro
Rudá	rudý	k2eAgNnPc4d1	Rudé
Křídla	křídlo	k1gNnPc4	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
si	se	k3xPyFc3	se
zajistili	zajistit	k5eAaPmAgMnP	zajistit
postup	postup	k1gInSc4	postup
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
playoff	playoff	k1gInSc1	playoff
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
změřili	změřit	k5eAaPmAgMnP	změřit
síly	síla	k1gFnPc4	síla
s	s	k7c7	s
Carolinou	Carolina	k1gFnSc7	Carolina
Hurricanes	Hurricanesa	k1gFnPc2	Hurricanesa
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
zápas	zápas	k1gInSc1	zápas
znamenal	znamenat	k5eAaImAgInS	znamenat
více	hodně	k6eAd2	hodně
štěstí	štěstí	k1gNnSc4	štěstí
pro	pro	k7c4	pro
Carolinu	Carolina	k1gFnSc4	Carolina
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k3yRnSc1	co
slavili	slavit	k5eAaImAgMnP	slavit
vítězství	vítězství	k1gNnSc4	vítězství
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInPc1d1	následující
zápasy	zápas	k1gInPc1	zápas
byly	být	k5eAaImAgInP	být
již	již	k6eAd1	již
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
Detroitu	Detroit	k1gInSc2	Detroit
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
dočkal	dočkat	k5eAaPmAgMnS	dočkat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
Carolina	Carolina	k1gFnSc1	Carolina
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
v	v	k7c6	v
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
pátém	pátý	k4xOgInSc6	pátý
zápase	zápas	k1gInSc6	zápas
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
splnil	splnit	k5eAaPmAgInS	splnit
dlouholetý	dlouholetý	k2eAgInSc4d1	dlouholetý
sen	sen	k1gInSc4	sen
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgFnP	následovat
oslavy	oslava	k1gFnPc1	oslava
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterých	který	k3yRgFnPc6	který
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
i	i	k8xC	i
přes	přes	k7c4	přes
neustálé	neustálý	k2eAgNnSc4d1	neustálé
přemlouvání	přemlouvání	k1gNnSc4	přemlouvání
vedením	vedení	k1gNnSc7	vedení
Detroitu	Detroit	k1gInSc2	Detroit
<g/>
,	,	kIx,	,
definitivní	definitivní	k2eAgInSc1d1	definitivní
konec	konec	k1gInSc1	konec
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
i	i	k9	i
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Minulá	minulý	k2eAgFnSc1d1	minulá
sezóna	sezóna	k1gFnSc1	sezóna
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
znamenala	znamenat	k5eAaImAgFnS	znamenat
konec	konec	k1gInSc4	konec
s	s	k7c7	s
hokejem	hokej	k1gInSc7	hokej
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
hraní	hraní	k1gNnSc2	hraní
in-line	inin	k1gInSc5	in-lin
hokeje	hokej	k1gInSc2	hokej
za	za	k7c4	za
extraligový	extraligový	k2eAgInSc4d1	extraligový
tým	tým	k1gInSc4	tým
IHC	IHC	kA	IHC
Bonfire	Bonfir	k1gInSc5	Bonfir
Střída	střída	k1gFnSc1	střída
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2003	[number]	k4	2003
byl	být	k5eAaImAgMnS	být
však	však	k9	však
Hašek	Hašek	k1gMnSc1	Hašek
obviněn	obviněn	k2eAgMnSc1d1	obviněn
z	z	k7c2	z
napadení	napadení	k1gNnPc2	napadení
soupeře	soupeř	k1gMnSc2	soupeř
Martina	Martin	k1gInSc2	Martin
Šíly	šíl	k1gMnPc4	šíl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
hospitalizován	hospitalizovat	k5eAaBmNgInS	hospitalizovat
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
tří	tři	k4xCgInPc2	tři
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
incident	incident	k1gInSc1	incident
byl	být	k5eAaImAgInS	být
vyhodnocen	vyhodnotit	k5eAaPmNgInS	vyhodnotit
jako	jako	k9	jako
přestupek	přestupek	k1gInSc4	přestupek
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
Haškovi	Hašek	k1gMnSc3	Hašek
uložena	uložit	k5eAaPmNgFnS	uložit
pokuta	pokuta	k1gFnSc1	pokuta
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
2	[number]	k4	2
tisíc	tisíc	k4xCgInPc2	tisíc
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
loučil	loučit	k5eAaImAgInS	loučit
s	s	k7c7	s
hokejem	hokej	k1gInSc7	hokej
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
po	po	k7c6	po
roce	rok	k1gInSc6	rok
opět	opět	k6eAd1	opět
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
profesionální	profesionální	k2eAgFnSc2d1	profesionální
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
podepsal	podepsat	k5eAaPmAgMnS	podepsat
opět	opět	k6eAd1	opět
Detroit	Detroit	k1gInSc4	Detroit
<g/>
.	.	kIx.	.
</s>
<s>
Návrat	návrat	k1gInSc1	návrat
to	ten	k3xDgNnSc4	ten
ale	ale	k9	ale
nebyl	být	k5eNaImAgInS	být
moc	moc	k6eAd1	moc
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
vrátily	vrátit	k5eAaPmAgInP	vrátit
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
třísly	tříslo	k1gNnPc7	tříslo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jej	on	k3xPp3gMnSc4	on
vyřadily	vyřadit	k5eAaPmAgInP	vyřadit
po	po	k7c4	po
14	[number]	k4	14
odehraných	odehraný	k2eAgInPc6d1	odehraný
zápasech	zápas	k1gInPc6	zápas
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
zbytek	zbytek	k1gInSc4	zbytek
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2004	[number]	k4	2004
podstoupil	podstoupit	k5eAaPmAgMnS	podstoupit
operaci	operace	k1gFnSc3	operace
třísel	tříslo	k1gNnPc2	tříslo
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgInS	vrátit
se	se	k3xPyFc4	se
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Pardubic	Pardubice	k1gInPc2	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Jednoroční	jednoroční	k2eAgFnSc1d1	jednoroční
smlouva	smlouva	k1gFnSc1	smlouva
vypršela	vypršet	k5eAaPmAgFnS	vypršet
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
angažmá	angažmá	k1gNnSc6	angažmá
pod	pod	k7c7	pod
novým	nový	k2eAgInSc7d1	nový
týmem	tým	k1gInSc7	tým
z	z	k7c2	z
Ottawy	Ottawa	k1gFnSc2	Ottawa
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
následující	následující	k2eAgFnSc1d1	následující
sezóna	sezóna	k1gFnSc1	sezóna
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
stávky	stávka	k1gFnSc2	stávka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stávce	stávka	k1gFnSc6	stávka
v	v	k7c6	v
NHL	NHL	kA	NHL
se	se	k3xPyFc4	se
Hašek	Hašek	k1gMnSc1	Hašek
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
zápasů	zápas	k1gInPc2	zápas
s	s	k7c7	s
výběrem	výběr	k1gInSc7	výběr
Primus	primus	k1gInSc1	primus
Worldstars	Worldstarsa	k1gFnPc2	Worldstarsa
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
cestoval	cestovat	k5eAaImAgMnS	cestovat
Evropou	Evropa	k1gFnSc7	Evropa
a	a	k8xC	a
sehrál	sehrát	k5eAaPmAgInS	sehrát
zápasy	zápas	k1gInPc4	zápas
s	s	k7c7	s
tamními	tamní	k2eAgInPc7d1	tamní
týmy	tým	k1gInPc7	tým
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
deseti	deset	k4xCc6	deset
zápasech	zápas	k1gInPc6	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
započalo	započnout	k5eAaPmAgNnS	započnout
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
a	a	k8xC	a
trvalo	trvat	k5eAaImAgNnS	trvat
do	do	k7c2	do
23	[number]	k4	23
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
Primus	primus	k1gInSc1	primus
Worldstars	Worldstars	k1gInSc1	Worldstars
navštívil	navštívit	k5eAaPmAgInS	navštívit
Lotyšsko	Lotyšsko	k1gNnSc4	Lotyšsko
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc1	Norsko
a	a	k8xC	a
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nastávající	nastávající	k2eAgFnSc6d1	nastávající
sezóně	sezóna	k1gFnSc6	sezóna
do	do	k7c2	do
olympijské	olympijský	k2eAgFnSc2d1	olympijská
přestávky	přestávka	k1gFnSc2	přestávka
odehrál	odehrát	k5eAaPmAgInS	odehrát
43	[number]	k4	43
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
bylo	být	k5eAaImAgNnS	být
28	[number]	k4	28
výher	výhra	k1gFnPc2	výhra
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
2,09	[number]	k4	2,09
obdržených	obdržený	k2eAgFnPc2d1	obdržená
branek	branka	k1gFnPc2	branka
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nastávajícím	nastávající	k2eAgInSc6d1	nastávající
olympijském	olympijský	k2eAgInSc6d1	olympijský
turnaji	turnaj	k1gInSc6	turnaj
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
počítalo	počítat	k5eAaImAgNnS	počítat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
zápase	zápas	k1gInSc6	zápas
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
česká	český	k2eAgFnSc1d1	Česká
reprezentace	reprezentace	k1gFnSc1	reprezentace
střetla	střetnout	k5eAaPmAgFnS	střetnout
s	s	k7c7	s
celkem	celek	k1gInSc7	celek
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
zranění	zranění	k1gNnSc3	zranění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
znamenal	znamenat	k5eAaImAgInS	znamenat
konec	konec	k1gInSc1	konec
turnaje	turnaj	k1gInSc2	turnaj
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
celé	celý	k2eAgFnSc2d1	celá
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Ottawa	Ottawa	k1gFnSc1	Ottawa
však	však	k9	však
s	s	k7c7	s
Haškem	Hašek	k1gMnSc7	Hašek
již	již	k9	již
přestala	přestat	k5eAaPmAgFnS	přestat
počítat	počítat	k5eAaImF	počítat
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc4d1	nová
smlouvu	smlouva	k1gFnSc4	smlouva
podepsal	podepsat	k5eAaPmAgMnS	podepsat
opět	opět	k6eAd1	opět
s	s	k7c7	s
Detroitem	Detroit	k1gInSc7	Detroit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
gólmanem	gólman	k1gMnSc7	gólman
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Dvojkou	dvojka	k1gFnSc7	dvojka
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Chris	Chris	k1gFnSc2	Chris
Osgood	Osgood	k1gInSc1	Osgood
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
základní	základní	k2eAgFnSc2d1	základní
části	část	k1gFnSc2	část
odehrál	odehrát	k5eAaPmAgInS	odehrát
56	[number]	k4	56
zápasů	zápas	k1gInPc2	zápas
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
obdržených	obdržený	k2eAgFnPc2d1	obdržená
branek	branka	k1gFnPc2	branka
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
2,05	[number]	k4	2,05
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
playoff	playoff	k1gInSc4	playoff
odehrál	odehrát	k5eAaPmAgMnS	odehrát
18	[number]	k4	18
zápasů	zápas	k1gInPc2	zápas
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
cesta	cesta	k1gFnSc1	cesta
za	za	k7c7	za
pohárem	pohár	k1gInSc7	pohár
skončila	skončit	k5eAaPmAgFnS	skončit
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
západní	západní	k2eAgFnSc2d1	západní
konference	konference	k1gFnSc2	konference
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
Detroit	Detroit	k1gInSc1	Detroit
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
Anaheimu	Anaheim	k1gMnSc3	Anaheim
<g/>
,	,	kIx,	,
budoucímu	budoucí	k2eAgMnSc3d1	budoucí
výherci	výherce	k1gMnSc3	výherce
Stanley	Stanlea	k1gFnSc2	Stanlea
Cupu	cupat	k5eAaImIp1nS	cupat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nastávající	nastávající	k2eAgFnSc6d1	nastávající
sezóně	sezóna	k1gFnSc6	sezóna
odehrál	odehrát	k5eAaPmAgInS	odehrát
41	[number]	k4	41
zápasů	zápas	k1gInPc2	zápas
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
obdržených	obdržený	k2eAgFnPc2d1	obdržená
branek	branka	k1gFnPc2	branka
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
2,14	[number]	k4	2,14
<g/>
.	.	kIx.	.
</s>
<s>
Detroit	Detroit	k1gInSc1	Detroit
postoupil	postoupit	k5eAaPmAgInS	postoupit
do	do	k7c2	do
playoff	playoff	k1gInSc1	playoff
se	s	k7c7	s
115	[number]	k4	115
body	bod	k1gInPc7	bod
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
v	v	k7c4	v
playoff	playoff	k1gInSc4	playoff
odehrál	odehrát	k5eAaPmAgMnS	odehrát
pouze	pouze	k6eAd1	pouze
čtyři	čtyři	k4xCgInPc4	čtyři
zápasy	zápas	k1gInPc4	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
posledním	poslední	k2eAgInSc7d1	poslední
zápasem	zápas	k1gInSc7	zápas
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
čtvrté	čtvrtý	k4xOgNnSc1	čtvrtý
utkání	utkání	k1gNnSc1	utkání
Detroitu	Detroit	k1gInSc2	Detroit
proti	proti	k7c3	proti
Nashvillu	Nashvillo	k1gNnSc3	Nashvillo
Predators	Predators	k1gInSc1	Predators
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
po	po	k7c6	po
obdržení	obdržení	k1gNnSc3	obdržení
třetího	třetí	k4xOgInSc2	třetí
gólu	gól	k1gInSc2	gól
opustil	opustit	k5eAaPmAgMnS	opustit
branku	branka	k1gFnSc4	branka
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
zápasu	zápas	k1gInSc2	zápas
i	i	k8xC	i
playoff	playoff	k1gMnSc1	playoff
odchytal	odchytat	k5eAaPmAgMnS	odchytat
již	již	k6eAd1	již
Chris	Chris	k1gFnSc4	Chris
Osgood	Osgooda	k1gFnPc2	Osgooda
<g/>
.	.	kIx.	.
</s>
<s>
Detroit	Detroit	k1gInSc1	Detroit
nakonec	nakonec	k6eAd1	nakonec
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
porazil	porazit	k5eAaPmAgInS	porazit
výběr	výběr	k1gInSc1	výběr
Pittsburgh	Pittsburgh	k1gInSc1	Pittsburgh
Penguins	Penguinsa	k1gFnPc2	Penguinsa
a	a	k8xC	a
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
Stanley	Stanlea	k1gFnSc2	Stanlea
Cup	cup	k1gInSc1	cup
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
oslavách	oslava	k1gFnPc6	oslava
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
oznámil	oznámit	k5eAaPmAgInS	oznámit
"	"	kIx"	"
<g/>
definitivní	definitivní	k2eAgNnSc4d1	definitivní
<g/>
"	"	kIx"	"
konec	konec	k1gInSc4	konec
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
na	na	k7c6	na
tiskové	tiskový	k2eAgFnSc6d1	tisková
konferenci	konference	k1gFnSc6	konference
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
příští	příští	k2eAgFnSc2d1	příští
sezóny	sezóna	k1gFnSc2	sezóna
se	se	k3xPyFc4	se
po	po	k7c6	po
roční	roční	k2eAgFnSc6d1	roční
pauze	pauza	k1gFnSc6	pauza
vrací	vracet	k5eAaImIp3nS	vracet
k	k	k7c3	k
profesionálnímu	profesionální	k2eAgInSc3d1	profesionální
hokeji	hokej	k1gInSc3	hokej
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
chytat	chytat	k5eAaImF	chytat
za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
mateřský	mateřský	k2eAgInSc4d1	mateřský
klub	klub	k1gInSc4	klub
HC	HC	kA	HC
Eaton	Eaton	k1gInSc1	Eaton
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
.	.	kIx.	.
</s>
<s>
Pardubice	Pardubice	k1gInPc4	Pardubice
po	po	k7c6	po
52	[number]	k4	52
zápasech	zápas	k1gInPc6	zápas
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
postoupilo	postoupit	k5eAaPmAgNnS	postoupit
do	do	k7c2	do
play-off	playff	k1gInSc1	play-off
ze	z	k7c2	z
třetího	třetí	k4xOgNnSc2	třetí
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Hašek	Hašek	k1gMnSc1	Hašek
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
chvíli	chvíle	k1gFnSc6	chvíle
odehraných	odehraný	k2eAgInPc2d1	odehraný
36	[number]	k4	36
zápasů	zápas	k1gInPc2	zápas
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
obdržených	obdržený	k2eAgFnPc2d1	obdržená
branek	branka	k1gFnPc2	branka
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
2,24	[number]	k4	2,24
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
play-off	playff	k1gInSc4	play-off
odehrál	odehrát	k5eAaPmAgMnS	odehrát
13	[number]	k4	13
zápasů	zápas	k1gInPc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
prohraným	prohraný	k2eAgInSc7d1	prohraný
zápasem	zápas	k1gInSc7	zápas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
pardubičtí	pardubický	k2eAgMnPc1d1	pardubický
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
první	první	k4xOgInSc1	první
zápas	zápas	k1gInSc1	zápas
v	v	k7c4	v
play-off	playff	k1gInSc4	play-off
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
odehráli	odehrát	k5eAaPmAgMnP	odehrát
s	s	k7c7	s
HC	HC	kA	HC
Oceláři	ocelář	k1gMnPc1	ocelář
Třinec	Třinec	k1gInSc4	Třinec
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgInP	následovat
již	již	k6eAd1	již
jen	jen	k9	jen
výhry	výhra	k1gFnPc1	výhra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jim	on	k3xPp3gMnPc3	on
vynesly	vynést	k5eAaPmAgInP	vynést
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Haška	Hašek	k1gMnSc2	Hašek
se	se	k3xPyFc4	se
tak	tak	k9	tak
stal	stát	k5eAaPmAgMnS	stát
historicky	historicky	k6eAd1	historicky
nejstarší	starý	k2eAgMnSc1d3	nejstarší
český	český	k2eAgMnSc1d1	český
hokejista	hokejista	k1gMnSc1	hokejista
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
mistrem	mistr	k1gMnSc7	mistr
hokejové	hokejový	k2eAgFnSc2d1	hokejová
extraligy	extraliga	k1gFnSc2	extraliga
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
podepsal	podepsat	k5eAaPmAgMnS	podepsat
roční	roční	k2eAgInSc4d1	roční
kontrakt	kontrakt	k1gInSc4	kontrakt
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
KHL	KHL	kA	KHL
HC	HC	kA	HC
Spartak	Spartak	k1gInSc1	Spartak
Moskva	Moskva	k1gFnSc1	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
části	část	k1gFnSc6	část
KHL	KHL	kA	KHL
odchytal	odchytat	k5eAaPmAgMnS	odchytat
Hašek	Hašek	k1gMnSc1	Hašek
44	[number]	k4	44
zápasů	zápas	k1gInPc2	zápas
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
obdržených	obdržený	k2eAgFnPc2d1	obdržená
branek	branka	k1gFnPc2	branka
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
2,45	[number]	k4	2,45
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
Gagarinův	Gagarinův	k2eAgInSc4d1	Gagarinův
pohár	pohár	k1gInSc4	pohár
Spartak	Spartak	k1gInSc1	Spartak
Moskva	Moskva	k1gFnSc1	Moskva
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
proti	proti	k7c3	proti
SKA	SKA	kA	SKA
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
po	po	k7c6	po
čtyřech	čtyři	k4xCgInPc6	čtyři
vyhraných	vyhraný	k2eAgInPc6d1	vyhraný
zápasech	zápas	k1gInPc6	zápas
postoupil	postoupit	k5eAaPmAgMnS	postoupit
<g/>
.	.	kIx.	.
</s>
<s>
Dominik	Dominik	k1gMnSc1	Dominik
Hašek	Hašek	k1gMnSc1	Hašek
odchytal	odchytat	k5eAaPmAgMnS	odchytat
všechny	všechen	k3xTgInPc4	všechen
čtyři	čtyři	k4xCgInPc4	čtyři
zápasy	zápas	k1gInPc4	zápas
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
obdržených	obdržený	k2eAgFnPc2d1	obdržená
branek	branka	k1gFnPc2	branka
4,12	[number]	k4	4,12
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sezóně	sezóna	k1gFnSc6	sezóna
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
rád	rád	k6eAd1	rád
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
KHL	KHL	kA	KHL
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
týmu	tým	k1gInSc6	tým
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
šanci	šance	k1gFnSc4	šance
vyhrát	vyhrát	k5eAaPmF	vyhrát
Gagarinův	Gagarinův	k2eAgInSc4d1	Gagarinův
pohár	pohár	k1gInSc4	pohár
<g/>
.	.	kIx.	.
</s>
<s>
Nabídku	nabídka	k1gFnSc4	nabídka
tomu	ten	k3xDgMnSc3	ten
odpovídající	odpovídající	k2eAgFnSc2d1	odpovídající
však	však	k9	však
nedostal	dostat	k5eNaPmAgMnS	dostat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
sezóně	sezóna	k1gFnSc6	sezóna
věnovat	věnovat	k5eAaImF	věnovat
jiným	jiný	k2eAgInPc3d1	jiný
sportům	sport	k1gInPc3	sport
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
investovat	investovat	k5eAaBmF	investovat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
neúspěšně	úspěšně	k6eNd1	úspěšně
prodával	prodávat	k5eAaImAgMnS	prodávat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
značku	značka	k1gFnSc4	značka
sportovního	sportovní	k2eAgNnSc2d1	sportovní
oblečení	oblečení	k1gNnSc2	oblečení
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgInS	založit
společnost	společnost	k1gFnSc4	společnost
DM	dm	kA	dm
Life	Life	k1gFnSc4	Life
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nabízí	nabízet	k5eAaImIp3nS	nabízet
energetický	energetický	k2eAgInSc4d1	energetický
drink	drink	k1gInSc4	drink
Smarty	Smarta	k1gFnSc2	Smarta
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
počínání	počínání	k1gNnSc1	počínání
s	s	k7c7	s
nápojem	nápoj	k1gInSc7	nápoj
však	však	k9	však
budí	budit	k5eAaImIp3nS	budit
posměch	posměch	k1gInSc4	posměch
díky	díky	k7c3	díky
bizarní	bizarní	k2eAgFnSc3d1	bizarní
marketingové	marketingový	k2eAgFnSc3d1	marketingová
propagaci	propagace	k1gFnSc3	propagace
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
je	být	k5eAaImIp3nS	být
tváří	tvář	k1gFnSc7	tvář
<g/>
.	.	kIx.	.
</s>
<s>
Hašek	Hašek	k1gMnSc1	Hašek
byl	být	k5eAaImAgMnS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
s	s	k7c7	s
Alenou	Alena	k1gFnSc7	Alena
<g/>
,	,	kIx,	,
rozenou	rozený	k2eAgFnSc7d1	rozená
Dvořákovou	Dvořáková	k1gFnSc7	Dvořáková
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
znal	znát	k5eAaImAgMnS	znát
již	již	k6eAd1	již
od	od	k7c2	od
gymnaziálních	gymnaziální	k2eAgFnPc2d1	gymnaziální
studií	studie	k1gFnPc2	studie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
manželství	manželství	k1gNnSc2	manželství
vzešly	vzejít	k5eAaPmAgFnP	vzejít
dvě	dva	k4xCgFnPc1	dva
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Dominika	Dominik	k1gMnSc2	Dominik
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
však	však	k9	však
partneři	partner	k1gMnPc1	partner
po	po	k7c6	po
třiadvacetiletém	třiadvacetiletý	k2eAgNnSc6d1	třiadvacetileté
manželství	manželství	k1gNnSc6	manželství
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
léta	léto	k1gNnSc2	léto
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgMnS	být
Hašek	Hašek	k1gMnSc1	Hašek
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
s	s	k7c7	s
Libuší	Libuše	k1gFnSc7	Libuše
Šmuclerovou	Šmuclerův	k2eAgFnSc7d1	Šmuclerova
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prohlašoval	prohlašovat	k5eAaImAgMnS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Šmuclerová	Šmuclerová	k1gFnSc1	Šmuclerová
za	za	k7c7	za
rozpadem	rozpad	k1gInSc7	rozpad
jeho	on	k3xPp3gNnSc2	on
manželství	manželství	k1gNnSc2	manželství
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
léta	léto	k1gNnSc2	léto
2015	[number]	k4	2015
se	se	k3xPyFc4	se
však	však	k9	však
rozešli	rozejít	k5eAaPmAgMnP	rozejít
<g/>
.	.	kIx.	.
</s>
