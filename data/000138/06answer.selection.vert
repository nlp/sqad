<s>
Kostní	kostní	k2eAgFnSc1d1	kostní
dřeň	dřeň	k1gFnSc1	dřeň
(	(	kIx(	(
<g/>
medulla	medulla	k1gMnSc1	medulla
ossea	ossea	k1gMnSc1	ossea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
morek	morek	k1gInSc1	morek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
měkká	měkký	k2eAgFnSc1d1	měkká
tkáň	tkáň	k1gFnSc1	tkáň
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
vnitřky	vnitřek	k1gInPc4	vnitřek
kostí	kost	k1gFnPc2	kost
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
