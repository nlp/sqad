<s>
Kostní	kostní	k2eAgFnSc1d1	kostní
dřeň	dřeň	k1gFnSc1	dřeň
(	(	kIx(	(
<g/>
medulla	medulla	k1gMnSc1	medulla
ossea	ossea	k1gMnSc1	ossea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
morek	morek	k1gInSc1	morek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
měkká	měkký	k2eAgFnSc1d1	měkká
tkáň	tkáň	k1gFnSc1	tkáň
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
vnitřky	vnitřek	k1gInPc4	vnitřek
kostí	kost	k1gFnPc2	kost
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
dřeňovou	dřeňový	k2eAgFnSc4d1	dřeňová
dutinu	dutina	k1gFnSc4	dutina
(	(	kIx(	(
<g/>
cavitas	cavitas	k1gInSc1	cavitas
medullaris	medullaris	k1gFnPc2	medullaris
<g/>
)	)	kIx)	)
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
prostory	prostor	k1gInPc4	prostor
mezi	mezi	k7c7	mezi
trámci	trámec	k1gInPc7	trámec
houbovité	houbovitý	k2eAgFnSc2d1	houbovitá
kostní	kostní	k2eAgFnSc2d1	kostní
tkáně	tkáň	k1gFnSc2	tkáň
konců	konec	k1gInPc2	konec
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nP	tvořit
nové	nový	k2eAgFnPc4d1	nová
krvinky	krvinka	k1gFnPc4	krvinka
a	a	k8xC	a
krevní	krevní	k2eAgFnPc1d1	krevní
destičky	destička	k1gFnPc1	destička
(	(	kIx(	(
<g/>
krvetvorba	krvetvorba	k1gFnSc1	krvetvorba
<g/>
)	)	kIx)	)
Je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
tzv.	tzv.	kA	tzv.
hemopoetickou	hemopoetický	k2eAgFnSc7d1	hemopoetický
tkání	tkáň	k1gFnSc7	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Krvetvorba	krvetvorba	k1gFnSc1	krvetvorba
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
kostech	kost	k1gFnPc6	kost
do	do	k7c2	do
4	[number]	k4	4
až	až	k9	až
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
asi	asi	k9	asi
2,6	[number]	k4	2,6
kg	kg	kA	kg
kostní	kostní	k2eAgFnSc2d1	kostní
dřeně	dřeň	k1gFnSc2	dřeň
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
kostní	kostní	k2eAgFnSc2d1	kostní
dřeně	dřeň	k1gFnSc2	dřeň
<g/>
:	:	kIx,	:
červená	červený	k2eAgFnSc1d1	červená
kostní	kostní	k2eAgFnSc1d1	kostní
dřeň	dřeň	k1gFnSc1	dřeň
(	(	kIx(	(
<g/>
medulla	medulla	k6eAd1	medulla
ossium	ossium	k1gNnSc1	ossium
rubra	rubrum	k1gNnSc2	rubrum
<g/>
)	)	kIx)	)
a	a	k8xC	a
žlutá	žlutý	k2eAgFnSc1d1	žlutá
kostní	kostní	k2eAgFnSc1d1	kostní
dřeň	dřeň	k1gFnSc1	dřeň
(	(	kIx(	(
<g/>
medulla	medulla	k6eAd1	medulla
ossium	ossium	k1gNnSc1	ossium
flava	flavo	k1gNnSc2	flavo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
krevních	krevní	k2eAgFnPc2d1	krevní
cév	céva	k1gFnPc2	céva
a	a	k8xC	a
vlásečnic	vlásečnice	k1gFnPc2	vlásečnice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
vzniká	vznikat	k5eAaImIp3nS	vznikat
ještě	ještě	k9	ještě
šedá	šedý	k2eAgFnSc1d1	šedá
kostní	kostní	k2eAgFnSc1d1	kostní
dřeň	dřeň	k1gFnSc1	dřeň
(	(	kIx(	(
<g/>
medulla	medulla	k6eAd1	medulla
ossium	ossium	k1gNnSc1	ossium
grisea	griseum	k1gNnSc2	griseum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
želatinové	želatinový	k2eAgFnSc2d1	želatinová
konzistence	konzistence	k1gFnSc2	konzistence
<g/>
.	.	kIx.	.
</s>
<s>
Červené	Červené	k2eAgFnPc1d1	Červené
krvinky	krvinka	k1gFnPc1	krvinka
<g/>
,	,	kIx,	,
krevní	krevní	k2eAgFnPc1d1	krevní
destičky	destička	k1gFnPc1	destička
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
vzniká	vznikat	k5eAaImIp3nS	vznikat
právě	právě	k9	právě
v	v	k7c6	v
červené	červený	k2eAgFnSc6d1	červená
kostní	kostní	k2eAgFnSc6d1	kostní
dřeni	dřeň	k1gFnSc6	dřeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
narození	narození	k1gNnSc2	narození
je	být	k5eAaImIp3nS	být
veškerá	veškerý	k3xTgFnSc1	veškerý
kostní	kostní	k2eAgFnSc1d1	kostní
dřeň	dřeň	k1gFnSc1	dřeň
červená	červený	k2eAgFnSc1d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
ze	z	k7c2	z
sítě	síť	k1gFnSc2	síť
retikulárního	retikulární	k2eAgNnSc2d1	retikulární
vaziva	vazivo	k1gNnSc2	vazivo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
hustě	hustě	k6eAd1	hustě
prostoupeno	prostoupit	k5eAaPmNgNnS	prostoupit
krevními	krevní	k2eAgFnPc7d1	krevní
vlásečnicemi	vlásečnice	k1gFnPc7	vlásečnice
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
přibývajícím	přibývající	k2eAgInSc7d1	přibývající
věkem	věk	k1gInSc7	věk
se	se	k3xPyFc4	se
v	v	k7c6	v
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
kostech	kost	k1gFnPc6	kost
mění	měnit	k5eAaImIp3nS	měnit
ve	v	k7c4	v
žlutý	žlutý	k2eAgInSc4d1	žlutý
typ	typ	k1gInSc4	typ
<g/>
.	.	kIx.	.
</s>
<s>
Zůstává	zůstávat	k5eAaImIp3nS	zůstávat
však	však	k9	však
v	v	k7c6	v
plochých	plochý	k2eAgFnPc6d1	plochá
kostech	kost	k1gFnPc6	kost
(	(	kIx(	(
<g/>
lebka	lebka	k1gFnSc1	lebka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obratlích	obratel	k1gInPc6	obratel
<g/>
,	,	kIx,	,
pánvi	pánev	k1gFnSc6	pánev
<g/>
,	,	kIx,	,
žebrech	žebr	k1gInPc6	žebr
<g/>
,	,	kIx,	,
zápěstních	zápěstní	k2eAgFnPc6d1	zápěstní
a	a	k8xC	a
zánártních	zánártní	k2eAgFnPc6d1	zánártní
kostech	kost	k1gFnPc6	kost
<g/>
,	,	kIx,	,
kostech	kost	k1gFnPc6	kost
pánevních	pánevní	k2eAgFnPc6d1	pánevní
a	a	k8xC	a
v	v	k7c6	v
hrudní	hrudní	k2eAgFnSc6d1	hrudní
kosti	kost	k1gFnSc6	kost
<g/>
,	,	kIx,	,
v	v	k7c6	v
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
kostech	kost	k1gFnPc6	kost
se	se	k3xPyFc4	se
udržuje	udržovat	k5eAaImIp3nS	udržovat
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
kloubních	kloubní	k2eAgInPc6d1	kloubní
koncích	konec	k1gInPc6	konec
v	v	k7c6	v
houbovité	houbovitý	k2eAgFnSc6d1	houbovitá
kostní	kostní	k2eAgFnSc6d1	kostní
tkáni	tkáň	k1gFnSc6	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
orgán	orgán	k1gInSc1	orgán
krvetvorby	krvetvorba	k1gFnSc2	krvetvorba
se	se	k3xPyFc4	se
vyšetřuje	vyšetřovat	k5eAaImIp3nS	vyšetřovat
nabodnutím	nabodnutí	k1gNnSc7	nabodnutí
hrudní	hrudní	k2eAgFnSc2d1	hrudní
kosti	kost	k1gFnSc2	kost
(	(	kIx(	(
<g/>
sternální	sternální	k2eAgFnSc2d1	sternální
punkce	punkce	k1gFnSc2	punkce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dárci	dárce	k1gMnPc1	dárce
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
kostní	kostní	k2eAgFnSc4d1	kostní
dřeň	dřeň	k1gFnSc4	dřeň
z	z	k7c2	z
kosti	kost	k1gFnSc2	kost
pánevní	pánevní	k2eAgFnSc2d1	pánevní
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
kostní	kostní	k2eAgFnSc2d1	kostní
dřeně	dřeň	k1gFnSc2	dřeň
červené	červený	k2eAgFnPc1d1	červená
nahrazováním	nahrazování	k1gNnSc7	nahrazování
původního	původní	k2eAgNnSc2d1	původní
retikulárního	retikulární	k2eAgNnSc2d1	retikulární
vaziva	vazivo	k1gNnSc2	vazivo
vazivem	vazivo	k1gNnSc7	vazivo
tukovým	tukový	k2eAgNnSc7d1	tukové
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
žlutou	žlutý	k2eAgFnSc7d1	žlutá
barvou	barva	k1gFnSc7	barva
<g/>
;	;	kIx,	;
na	na	k7c6	na
krvetvorbě	krvetvorba	k1gFnSc6	krvetvorba
se	se	k3xPyFc4	se
nepodílí	podílet	k5eNaImIp3nS	podílet
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
věku	věk	k1gInSc2	věk
člověka	člověk	k1gMnSc2	člověk
začíná	začínat	k5eAaImIp3nS	začínat
být	být	k5eAaImF	být
podíl	podíl	k1gInSc1	podíl
červené	červený	k2eAgFnSc2d1	červená
a	a	k8xC	a
žluté	žlutý	k2eAgFnSc2d1	žlutá
kostní	kostní	k2eAgFnSc2d1	kostní
dřeně	dřeň	k1gFnSc2	dřeň
vyrovnaný	vyrovnaný	k2eAgInSc1d1	vyrovnaný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
prostředních	prostřední	k2eAgFnPc6d1	prostřední
částech	část	k1gFnPc6	část
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
stehenní	stehenní	k2eAgFnSc1d1	stehenní
<g/>
,	,	kIx,	,
záprstní	záprstní	k2eAgFnSc1d1	záprstní
<g/>
,	,	kIx,	,
nártní	nártní	k2eAgFnSc1d1	nártní
<g/>
,	,	kIx,	,
lýtková	lýtkový	k2eAgFnSc1d1	lýtková
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
okolností	okolnost	k1gFnPc2	okolnost
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
změnit	změnit	k5eAaPmF	změnit
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
dřeň	dřeň	k1gFnSc4	dřeň
červenou	červený	k2eAgFnSc4d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
ze	z	k7c2	z
žluté	žlutý	k2eAgFnSc2d1	žlutá
dřeně	dřeň	k1gFnSc2	dřeň
ztrátou	ztráta	k1gFnSc7	ztráta
tuku	tuk	k1gInSc2	tuk
v	v	k7c6	v
pozdním	pozdní	k2eAgInSc6d1	pozdní
věku	věk	k1gInSc6	věk
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
průsvitný	průsvitný	k2eAgInSc4d1	průsvitný
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
želatinový	želatinový	k2eAgInSc4d1	želatinový
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Krvetvorba	krvetvorba	k1gFnSc1	krvetvorba
zde	zde	k6eAd1	zde
není	být	k5eNaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
na	na	k7c4	na
šedou	šedý	k2eAgFnSc4d1	šedá
dřeň	dřeň	k1gFnSc4	dřeň
je	být	k5eAaImIp3nS	být
ireverzibilní	ireverzibilní	k2eAgNnSc1d1	ireverzibilní
<g/>
.	.	kIx.	.
</s>
