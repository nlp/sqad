<s>
Ovládací	ovládací	k2eAgInSc1d1
prvek	prvek	k1gInSc1
</s>
<s>
Technicky	technicky	k6eAd1
vzato	vzat	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
ovládací	ovládací	k2eAgInSc4d1
prvek	prvek	k1gInSc4
zařízení	zařízení	k1gNnSc2
<g/>
,	,	kIx,
pomocí	pomocí	k7c2
kterého	který	k3yQgNnSc2,k3yRgNnSc2,k3yIgNnSc2
může	moct	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
(	(	kIx(
<g/>
uživatel	uživatel	k1gMnSc1
nebo	nebo	k8xC
operátor	operátor	k1gMnSc1
<g/>
)	)	kIx)
ovládat	ovládat	k5eAaImF
stroj	stroj	k1gInSc4
nebo	nebo	k8xC
zařízení	zařízení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Dnes	dnes	k6eAd1
jsou	být	k5eAaImIp3nP
ovládací	ovládací	k2eAgInPc4d1
prvky	prvek	k1gInPc4
většinou	většinou	k6eAd1
elektrické	elektrický	k2eAgInPc4d1
nebo	nebo	k8xC
elektronické	elektronický	k2eAgInPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
pro	pro	k7c4
ovládání	ovládání	k1gNnSc4
strojů	stroj	k1gInPc2
a	a	k8xC
zařízení	zařízení	k1gNnSc2
i	i	k8xC
jiná	jiný	k2eAgFnSc1d1
energie	energie	k1gFnSc1
(	(	kIx(
<g/>
např.	např.	kA
pneumatické	pneumatický	k2eAgFnPc1d1
<g/>
,	,	kIx,
hydraulické	hydraulický	k2eAgNnSc1d1
nebo	nebo	k8xC
mechanické	mechanický	k2eAgNnSc1d1
ovládání	ovládání	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ovládací	ovládací	k2eAgInPc1d1
prvky	prvek	k1gInPc1
jsou	být	k5eAaImIp3nP
konstruovány	konstruovat	k5eAaImNgInP
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
mohly	moct	k5eAaImAgInP
být	být	k5eAaImF
ovládány	ovládat	k5eAaImNgInP
prstem	prst	k1gInSc7
<g/>
,	,	kIx,
rukou	ruka	k1gFnSc7
<g/>
,	,	kIx,
nohou	noha	k1gFnSc7
nebo	nebo	k8xC
jinou	jiný	k2eAgFnSc7d1
částí	část	k1gFnSc7
těla	tělo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Tlačítko	tlačítko	k1gNnSc1
</s>
<s>
Pedál	pedál	k1gInSc1
</s>
<s>
Páka	páka	k1gFnSc1
(	(	kIx(
<g/>
jednoduchý	jednoduchý	k2eAgInSc1d1
stroj	stroj	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Klávesnice	klávesnice	k1gFnSc1
</s>
<s>
Volant	volant	k1gInSc1
</s>
<s>
Ruční	ruční	k2eAgNnSc1d1
ovládání	ovládání	k1gNnSc1
</s>
<s>
Ovládací	ovládací	k2eAgInSc1d1
prvek	prvek	k1gInSc1
(	(	kIx(
<g/>
GUI	GUI	kA
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ovládací	ovládací	k2eAgInSc4d1
prvek	prvek	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
