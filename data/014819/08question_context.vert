<s>
Technicky	technicky	k6eAd1
vzato	vzat	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
ovládací	ovládací	k2eAgInSc4d1
prvek	prvek	k1gInSc4
zařízení	zařízení	k1gNnSc2
<g/>
,	,	kIx,
pomocí	pomocí	k7c2
kterého	který	k3yQgNnSc2,k3yRgNnSc2,k3yIgNnSc2
může	moct	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
(	(	kIx(
<g/>
uživatel	uživatel	k1gMnSc1
nebo	nebo	k8xC
operátor	operátor	k1gMnSc1
<g/>
)	)	kIx)
ovládat	ovládat	k5eAaImF
stroj	stroj	k1gInSc4
nebo	nebo	k8xC
zařízení	zařízení	k1gNnSc4
<g/>
.	.	kIx.
</s>