<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Slapy	slap	k1gInPc1	slap
je	být	k5eAaImIp3nS	být
přehradní	přehradní	k2eAgFnSc1d1	přehradní
nádrž	nádrž	k1gFnSc1	nádrž
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Vltavě	Vltava	k1gFnSc6	Vltava
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Slapy	slap	k1gInPc1	slap
ve	v	k7c6	v
Středočeském	středočeský	k2eAgInSc6d1	středočeský
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
rozlohy	rozloha	k1gFnSc2	rozloha
(	(	kIx(	(
<g/>
11,626	[number]	k4	11,626
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
šestou	šestý	k4xOgFnSc7	šestý
největší	veliký	k2eAgFnSc7d3	veliký
přehradou	přehrada	k1gFnSc7	přehrada
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
hloubka	hloubka	k1gFnSc1	hloubka
je	být	k5eAaImIp3nS	být
58	[number]	k4	58
m	m	kA	m
a	a	k8xC	a
hladina	hladina	k1gFnSc1	hladina
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
271	[number]	k4	271
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Přehrada	přehrada	k1gFnSc1	přehrada
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
tzv.	tzv.	kA	tzv.
Vltavské	vltavský	k2eAgFnSc2d1	Vltavská
kaskády	kaskáda	k1gFnSc2	kaskáda
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1949	[number]	k4	1949
<g/>
-	-	kIx~	-
<g/>
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
hráz	hráz	k1gFnSc1	hráz
přehrady	přehrada	k1gFnSc2	přehrada
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
260	[number]	k4	260
m	m	kA	m
a	a	k8xC	a
vysoká	vysoká	k1gFnSc1	vysoká
60	[number]	k4	60
m	m	kA	m
je	být	k5eAaImIp3nS	být
založena	založen	k2eAgFnSc1d1	založena
na	na	k7c6	na
skalním	skalní	k2eAgNnSc6d1	skalní
podloží	podloží	k1gNnSc6	podloží
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Slapy	slap	k1gInPc1	slap
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1955	[number]	k4	1955
<g/>
-	-	kIx~	-
<g/>
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
elektrárně	elektrárna	k1gFnSc6	elektrárna
jsou	být	k5eAaImIp3nP	být
nainstalovány	nainstalován	k2eAgFnPc1d1	nainstalována
3	[number]	k4	3
Kaplanovy	Kaplanův	k2eAgFnPc1d1	Kaplanova
turbíny	turbína	k1gFnPc1	turbína
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
3	[number]	k4	3
<g/>
×	×	k?	×
48	[number]	k4	48
MW	MW	kA	MW
<g/>
.	.	kIx.	.
</s>
<s>
Plného	plný	k2eAgInSc2d1	plný
výkonu	výkon	k1gInSc2	výkon
je	být	k5eAaImIp3nS	být
elektrárna	elektrárna	k1gFnSc1	elektrárna
schopna	schopen	k2eAgFnSc1d1	schopna
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
za	za	k7c4	za
136	[number]	k4	136
s.	s.	k?	s.
Ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
povodněmi	povodeň	k1gFnPc7	povodeň
Energetika	energetika	k1gFnSc1	energetika
Nalepšení	Nalepšení	k1gNnPc1	Nalepšení
průtoků	průtok	k1gInPc2	průtok
Průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
odběr	odběr	k1gInSc1	odběr
Rekreace	rekreace	k1gFnSc2	rekreace
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Slapy	slap	k1gInPc4	slap
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Slapy	slap	k1gInPc1	slap
-	-	kIx~	-
Skupina	skupina	k1gFnSc1	skupina
ČEZ	ČEZ	kA	ČEZ
Zatopené	zatopený	k2eAgInPc4d1	zatopený
osudy	osud	k1gInPc4	osud
-	-	kIx~	-
Slapy	slap	k1gInPc4	slap
<g/>
,	,	kIx,	,
dokument	dokument	k1gInSc4	dokument
ČT	ČT	kA	ČT
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc1	možnost
online	onlinout	k5eAaPmIp3nS	onlinout
přehrání	přehrání	k1gNnSc4	přehrání
BERAN	Beran	k1gMnSc1	Beran
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
<g/>
.	.	kIx.	.
</s>
<s>
Malacologica	Malacologic	k2eAgFnSc1d1	Malacologic
Bohemoslovaca	Bohemoslovaca	k1gFnSc1	Bohemoslovaca
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Vodní	vodní	k2eAgMnPc1d1	vodní
měkkýši	měkkýš	k1gMnPc1	měkkýš
přehradní	přehradní	k2eAgFnSc2d1	přehradní
nádrže	nádrž	k1gFnSc2	nádrž
Slapy	slap	k1gInPc1	slap
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
