<p>
<s>
Marco	Marco	k6eAd1	Marco
Polo	polo	k6eAd1	polo
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1254	[number]	k4	1254
<g/>
,	,	kIx,	,
ostrov	ostrov	k1gInSc4	ostrov
Korčula	Korčul	k1gMnSc2	Korčul
nebo	nebo	k8xC	nebo
Benátky	Benátky	k1gFnPc4	Benátky
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1324	[number]	k4	1324
<g/>
,	,	kIx,	,
Benátky	Benátky	k1gFnPc1	Benátky
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
benátský	benátský	k2eAgMnSc1d1	benátský
kupec	kupec	k1gMnSc1	kupec
a	a	k8xC	a
cestovatel	cestovatel	k1gMnSc1	cestovatel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
svými	svůj	k3xOyFgFnPc7	svůj
cestami	cesta	k1gFnPc7	cesta
po	po	k7c6	po
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
zaznamenanými	zaznamenaný	k2eAgInPc7d1	zaznamenaný
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Il	Il	k1gFnSc2	Il
Milione	milion	k4xCgInSc5	milion
(	(	kIx(	(
<g/>
Milion	milion	k4xCgInSc4	milion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
Evropanem	Evropan	k1gMnSc7	Evropan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
podrobněji	podrobně	k6eAd2	podrobně
popsal	popsat	k5eAaPmAgMnS	popsat
východní	východní	k2eAgFnSc4d1	východní
Asii	Asie	k1gFnSc4	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
kráter	kráter	k1gInSc1	kráter
Marco	Marco	k6eAd1	Marco
Polo	polo	k6eAd1	polo
na	na	k7c6	na
přivrácené	přivrácený	k2eAgFnSc6d1	přivrácená
straně	strana	k1gFnSc6	strana
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Bratři	bratřit	k5eAaImRp2nS	bratřit
Polové	Polová	k1gFnSc2	Polová
==	==	k?	==
</s>
</p>
<p>
<s>
Marco	Marco	k1gMnSc1	Marco
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
benátské	benátský	k2eAgFnSc6d1	Benátská
kupecké	kupecký	k2eAgFnSc6d1	kupecká
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Niccolo	Niccola	k1gFnSc5	Niccola
a	a	k8xC	a
strýc	strýc	k1gMnSc1	strýc
Matteo	Matteo	k1gMnSc1	Matteo
podnikli	podniknout	k5eAaPmAgMnP	podniknout
už	už	k9	už
v	v	k7c6	v
letech	let	k1gInPc6	let
1255	[number]	k4	1255
<g/>
–	–	k?	–
<g/>
1269	[number]	k4	1269
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
nitra	nitro	k1gNnSc2	nitro
Asie	Asie	k1gFnSc2	Asie
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
Konstantinopol	Konstantinopol	k1gInSc4	Konstantinopol
<g/>
,	,	kIx,	,
krymskou	krymský	k2eAgFnSc4d1	Krymská
Soldaiu	Soldaium	k1gNnSc3	Soldaium
<g/>
,	,	kIx,	,
území	území	k1gNnSc3	území
Zlaté	zlatá	k1gFnSc2	zlatá
hordy	horda	k1gFnSc2	horda
na	na	k7c6	na
dolní	dolní	k2eAgFnSc6d1	dolní
Volze	Volha	k1gFnSc6	Volha
a	a	k8xC	a
Bucharu	buchar	k1gInSc6	buchar
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yQgFnSc6	který
dospěli	dochvít	k5eAaPmAgMnP	dochvít
až	až	k9	až
k	k	k7c3	k
mongolskému	mongolský	k2eAgMnSc3d1	mongolský
chánovi	chán	k1gMnSc3	chán
Kublajovi	Kublaj	k1gMnSc3	Kublaj
do	do	k7c2	do
Pekingu	Peking	k1gInSc2	Peking
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
přes	přes	k7c4	přes
přístav	přístav	k1gInSc4	přístav
Akkon	Akkona	k1gFnPc2	Akkona
do	do	k7c2	do
Benátek	Benátky	k1gFnPc2	Benátky
s	s	k7c7	s
Kublajovým	Kublajový	k2eAgNnSc7d1	Kublajový
poselstvím	poselství	k1gNnSc7	poselství
pro	pro	k7c4	pro
papeže	papež	k1gMnSc4	papež
Řehoře	Řehoř	k1gMnSc4	Řehoř
X.	X.	kA	X.
Velký	velký	k2eAgMnSc1d1	velký
chán	chán	k1gMnSc1	chán
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
matka	matka	k1gFnSc1	matka
byla	být	k5eAaImAgFnS	být
nestoriánkou	nestoriánka	k1gFnSc7	nestoriánka
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
papež	papež	k1gMnSc1	papež
vyslal	vyslat	k5eAaPmAgMnS	vyslat
do	do	k7c2	do
jeho	jeho	k3xOp3gFnSc2	jeho
říše	říš	k1gFnSc2	říš
stovky	stovka	k1gFnSc2	stovka
křesťanských	křesťanský	k2eAgMnPc2d1	křesťanský
kněží	kněz	k1gMnPc2	kněz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
zpátky	zpátky	k6eAd1	zpátky
==	==	k?	==
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1271	[number]	k4	1271
se	se	k3xPyFc4	se
bratři	bratr	k1gMnPc1	bratr
vypravili	vypravit	k5eAaPmAgMnP	vypravit
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
je	on	k3xPp3gMnPc4	on
doprovázeli	doprovázet	k5eAaImAgMnP	doprovázet
dva	dva	k4xCgMnPc4	dva
křesťanští	křesťanský	k2eAgMnPc1d1	křesťanský
duchovní	duchovní	k1gMnPc1	duchovní
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
však	však	k9	však
zalekli	zaleknout	k5eAaPmAgMnP	zaleknout
obtížného	obtížný	k2eAgNnSc2d1	obtížné
putování	putování	k1gNnSc2	putování
a	a	k8xC	a
z	z	k7c2	z
Arménie	Arménie	k1gFnSc2	Arménie
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Arménie	Arménie	k1gFnSc2	Arménie
putovali	putovat	k5eAaImAgMnP	putovat
Polové	Polus	k1gMnPc1	Polus
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
západní	západní	k2eAgInSc4d1	západní
Ílchanát	Ílchanát	k1gInSc4	Ílchanát
do	do	k7c2	do
Bagdádu	Bagdád	k1gInSc2	Bagdád
<g/>
,	,	kIx,	,
Basry	Basra	k1gFnSc2	Basra
a	a	k8xC	a
Hormuzu	Hormuz	k1gInSc2	Hormuz
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
obchodním	obchodní	k2eAgInSc7d1	obchodní
přístavem	přístav	k1gInSc7	přístav
v	v	k7c6	v
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
cípu	cíp	k1gInSc6	cíp
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Sem	sem	k6eAd1	sem
přijížděli	přijíždět	k5eAaImAgMnP	přijíždět
kupci	kupec	k1gMnPc1	kupec
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
s	s	k7c7	s
kořením	koření	k1gNnSc7	koření
<g/>
,	,	kIx,	,
drahými	drahý	k2eAgInPc7d1	drahý
kameny	kámen	k1gInPc7	kámen
a	a	k8xC	a
perlami	perla	k1gFnPc7	perla
<g/>
,	,	kIx,	,
hedvábnými	hedvábný	k2eAgFnPc7d1	hedvábná
látkami	látka	k1gFnPc7	látka
a	a	k8xC	a
slonovinou	slonovina	k1gFnSc7	slonovina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
místní	místní	k2eAgMnPc1d1	místní
obchodníci	obchodník	k1gMnPc1	obchodník
prodávali	prodávat	k5eAaImAgMnP	prodávat
do	do	k7c2	do
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Polové	Polus	k1gMnPc1	Polus
chtěli	chtít	k5eAaImAgMnP	chtít
odsud	odsud	k6eAd1	odsud
plout	plout	k5eAaImF	plout
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
svůj	svůj	k3xOyFgInSc4	svůj
úmysl	úmysl	k1gInSc4	úmysl
neznámo	neznámo	k6eAd1	neznámo
proč	proč	k6eAd1	proč
změnili	změnit	k5eAaPmAgMnP	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Vydali	vydat	k5eAaPmAgMnP	vydat
se	se	k3xPyFc4	se
po	po	k7c6	po
souši	souš	k1gFnSc6	souš
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
vyprahlými	vyprahlý	k2eAgFnPc7d1	vyprahlá
oblastmi	oblast	k1gFnPc7	oblast
východní	východní	k2eAgFnSc2d1	východní
Persie	Persie	k1gFnSc2	Persie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
dny	den	k1gInPc4	den
neviděli	vidět	k5eNaImAgMnP	vidět
živého	živý	k2eAgMnSc4d1	živý
tvora	tvor	k1gMnSc4	tvor
a	a	k8xC	a
jejich	jejich	k3xOp3gMnSc4	jejich
cesta	cesta	k1gFnSc1	cesta
byla	být	k5eAaImAgFnS	být
lemována	lemovat	k5eAaImNgFnS	lemovat
kostrami	kostra	k1gFnPc7	kostra
zvířat	zvíře	k1gNnPc2	zvíře
i	i	k8xC	i
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Severním	severní	k2eAgInSc7d1	severní
Afghánistánem	Afghánistán	k1gInSc7	Afghánistán
přes	přes	k7c4	přes
pohoří	pohoří	k1gNnSc4	pohoří
Pamír	Pamír	k1gInSc1	Pamír
a	a	k8xC	a
Turkestán	Turkestán	k1gInSc4	Turkestán
pak	pak	k6eAd1	pak
dorazili	dorazit	k5eAaPmAgMnP	dorazit
na	na	k7c4	na
okraj	okraj	k1gInSc4	okraj
největší	veliký	k2eAgFnSc2d3	veliký
asijské	asijský	k2eAgFnSc2d1	asijská
pouště	poušť	k1gFnSc2	poušť
Gobi	Gobi	k1gFnSc2	Gobi
a	a	k8xC	a
s	s	k7c7	s
karavanou	karavana	k1gFnSc7	karavana
se	se	k3xPyFc4	se
pustili	pustit	k5eAaPmAgMnP	pustit
přes	přes	k7c4	přes
ni	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cílem	cíl	k1gInSc7	cíl
jejich	jejich	k3xOp3gFnSc2	jejich
cesty	cesta	k1gFnSc2	cesta
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
Peking	Peking	k1gInSc1	Peking
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1264	[number]	k4	1264
sídlo	sídlo	k1gNnSc4	sídlo
velkého	velký	k2eAgMnSc2d1	velký
mongolského	mongolský	k2eAgMnSc2d1	mongolský
chána	chán	k1gMnSc2	chán
<g/>
.	.	kIx.	.
</s>
<s>
Kublaj	Kublaj	k1gFnSc1	Kublaj
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
přijal	přijmout	k5eAaPmAgMnS	přijmout
s	s	k7c7	s
velkými	velký	k2eAgFnPc7d1	velká
poctami	pocta	k1gFnPc7	pocta
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
si	se	k3xPyFc3	se
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
mladého	mladý	k2eAgMnSc4d1	mladý
Marka	Marek	k1gMnSc4	Marek
a	a	k8xC	a
během	během	k7c2	během
sedmnácti	sedmnáct	k4xCc2	sedmnáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
tu	tu	k6eAd1	tu
pobýval	pobývat	k5eAaImAgMnS	pobývat
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
svěřoval	svěřovat	k5eAaImAgInS	svěřovat
různé	různý	k2eAgInPc4d1	různý
úřady	úřad	k1gInPc4	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
ho	on	k3xPp3gMnSc4	on
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
správou	správa	k1gFnSc7	správa
města	město	k1gNnSc2	město
a	a	k8xC	a
okresu	okres	k1gInSc2	okres
Jang-čou	Jang-ča	k1gFnSc7	Jang-ča
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
z	z	k7c2	z
titulu	titul	k1gInSc2	titul
své	svůj	k3xOyFgFnSc2	svůj
funkce	funkce	k1gFnSc2	funkce
Marco	Marco	k6eAd1	Marco
Polo	polo	k6eAd1	polo
navštívil	navštívit	k5eAaPmAgMnS	navštívit
provincie	provincie	k1gFnPc4	provincie
Šan-si	Šane	k1gFnSc4	Šan-se
<g/>
,	,	kIx,	,
Šen-si	Šene	k1gFnSc4	Šen-se
<g/>
,	,	kIx,	,
S	s	k7c7	s
<g/>
'	'	kIx"	'
<g/>
-čchuan	-čchuan	k1gMnSc1	-čchuan
a	a	k8xC	a
Jün-nan	Jünan	k1gMnSc1	Jün-nan
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
i	i	k9	i
severní	severní	k2eAgFnSc4d1	severní
Barmu	Barma	k1gFnSc4	Barma
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
z	z	k7c2	z
Pekingu	Peking	k1gInSc2	Peking
cestoval	cestovat	k5eAaImAgMnS	cestovat
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
až	až	k9	až
po	po	k7c4	po
přístav	přístav	k1gInSc4	přístav
Cuan-čou	Cuan-čá	k1gFnSc4	Cuan-čá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1292	[number]	k4	1292
velký	velký	k2eAgInSc1d1	velký
chán	chán	k1gMnSc1	chán
Marka	marka	k1gFnSc1	marka
Pola	pola	k1gFnSc1	pola
s	s	k7c7	s
otcem	otec	k1gMnSc7	otec
a	a	k8xC	a
strýcem	strýc	k1gMnSc7	strýc
bohatě	bohatě	k6eAd1	bohatě
odměnil	odměnit	k5eAaPmAgMnS	odměnit
a	a	k8xC	a
propustil	propustit	k5eAaPmAgMnS	propustit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Benátek	Benátky	k1gFnPc2	Benátky
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
po	po	k7c6	po
tříleté	tříletý	k2eAgFnSc6d1	tříletá
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
se	se	k3xPyFc4	se
plavili	plavit	k5eAaImAgMnP	plavit
jihočínským	jihočínský	k2eAgNnSc7d1	Jihočínské
mořem	moře	k1gNnSc7	moře
do	do	k7c2	do
Končinčíny	Končinčína	k1gFnSc2	Končinčína
a	a	k8xC	a
na	na	k7c4	na
Sumatru	Sumatra	k1gFnSc4	Sumatra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
čekali	čekat	k5eAaImAgMnP	čekat
půl	půl	k6eAd1	půl
roku	rok	k1gInSc2	rok
na	na	k7c4	na
příznivý	příznivý	k2eAgInSc4d1	příznivý
vítr	vítr	k1gInSc4	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
přes	přes	k7c4	přes
Cejlon	Cejlon	k1gInSc4	Cejlon
na	na	k7c4	na
Malabarské	Malabarský	k2eAgNnSc4d1	Malabarský
pobřeží	pobřeží	k1gNnSc4	pobřeží
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
podél	podél	k7c2	podél
perských	perský	k2eAgInPc2d1	perský
břehů	břeh	k1gInPc2	břeh
do	do	k7c2	do
Basry	Basra	k1gFnSc2	Basra
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
Černé	Černé	k2eAgNnSc4d1	Černé
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
Trabzon	Trabzon	k1gInSc4	Trabzon
a	a	k8xC	a
Konstantinopol	Konstantinopol	k1gInSc4	Konstantinopol
dostali	dostat	k5eAaPmAgMnP	dostat
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Cestopis	cestopis	k1gInSc4	cestopis
==	==	k?	==
</s>
</p>
<p>
<s>
Léta	léto	k1gNnSc2	léto
1298	[number]	k4	1298
<g/>
–	–	k?	–
<g/>
1299	[number]	k4	1299
strávil	strávit	k5eAaPmAgInS	strávit
Marco	Marco	k6eAd1	Marco
Polo	polo	k6eAd1	polo
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
v	v	k7c6	v
Janově	Janov	k1gInSc6	Janov
jako	jako	k8xS	jako
zajatec	zajatec	k1gMnSc1	zajatec
Janovanů	Janovan	k1gMnPc2	Janovan
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
benátského	benátský	k2eAgNnSc2d1	benátské
a	a	k8xC	a
janovského	janovský	k2eAgNnSc2d1	janovské
loďstva	loďstvo	k1gNnSc2	loďstvo
u	u	k7c2	u
Korčuly	Korčula	k1gFnSc2	Korčula
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
trávil	trávit	k5eAaImAgMnS	trávit
volný	volný	k2eAgInSc4d1	volný
čas	čas	k1gInSc4	čas
diktováním	diktování	k1gNnSc7	diktování
svých	svůj	k3xOyFgFnPc2	svůj
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
benátském	benátský	k2eAgNnSc6d1	benátské
nářečí	nářečí	k1gNnSc6	nářečí
s	s	k7c7	s
francouzskými	francouzský	k2eAgInPc7d1	francouzský
prvky	prvek	k1gInPc7	prvek
<g/>
)	)	kIx)	)
Rustichellovi	Rustichell	k1gMnSc3	Rustichell
da	da	k?	da
Pisa	Pisa	k1gFnSc1	Pisa
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
nesprávně	správně	k6eNd1	správně
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Rusticiano	Rusticiana	k1gFnSc5	Rusticiana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
sepsal	sepsat	k5eAaPmAgMnS	sepsat
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Livres	Livresa	k1gFnPc2	Livresa
des	des	k1gNnSc2	des
merveilles	merveilles	k1gMnSc1	merveilles
du	du	k?	du
monde	mond	k1gInSc5	mond
(	(	kIx(	(
<g/>
Kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
zázracích	zázrak	k1gInPc6	zázrak
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
je	být	k5eAaImIp3nS	být
cestopis	cestopis	k1gInSc1	cestopis
známý	známý	k2eAgInSc1d1	známý
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Milione	milion	k4xCgInSc5	milion
(	(	kIx(	(
<g/>
asi	asi	k9	asi
podle	podle	k7c2	podle
autorova	autorův	k2eAgNnSc2d1	autorovo
jména	jméno	k1gNnSc2	jméno
Marco-Emilione	Marco-Emilion	k1gInSc5	Marco-Emilion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původní	původní	k2eAgInSc1d1	původní
text	text	k1gInSc1	text
se	se	k3xPyFc4	se
bohužel	bohužel	k9	bohužel
ztratil	ztratit	k5eAaPmAgMnS	ztratit
<g/>
,	,	kIx,	,
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
ale	ale	k9	ale
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
opisů	opis	k1gInPc2	opis
a	a	k8xC	a
překladů	překlad	k1gInPc2	překlad
(	(	kIx(	(
<g/>
např.	např.	kA	např.
český	český	k2eAgInSc4d1	český
cca	cca	kA	cca
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1400	[number]	k4	1400
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgInSc1d3	nejstarší
italský	italský	k2eAgInSc1d1	italský
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1599	[number]	k4	1599
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
vydal	vydat	k5eAaPmAgMnS	vydat
Luigi	Luige	k1gFnSc4	Luige
Foscolo	Foscola	k1gFnSc5	Foscola
Benedetto	Benedetta	k1gFnSc5	Benedetta
Il	Il	k1gMnSc1	Il
milione	milion	k4xCgInSc5	milion
di	di	k?	di
Marco	Marco	k6eAd1	Marco
Polo	polo	k6eAd1	polo
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
Il	Il	k1gMnSc1	Il
Milione	milion	k4xCgInSc5	milion
<g/>
,	,	kIx,	,
prima	prima	k6eAd1	prima
edizione	edizion	k1gInSc5	edizion
integrale	integrale	k6eAd1	integrale
a	a	k8xC	a
cura	cura	k1gFnSc1	cura
di	di	k?	di
Luigi	Luig	k1gFnSc2	Luig
Foscolo	Foscola	k1gFnSc5	Foscola
Benedetto	Benedetta	k1gMnSc5	Benedetta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
původního	původní	k2eAgInSc2d1	původní
textu	text	k1gInSc2	text
na	na	k7c6	na
základě	základ	k1gInSc6	základ
srovnání	srovnání	k1gNnSc2	srovnání
více	hodně	k6eAd2	hodně
zachovaných	zachovaný	k2eAgFnPc2d1	zachovaná
verzí	verze	k1gFnPc2	verze
rukopisu	rukopis	k1gInSc2	rukopis
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
části	část	k1gFnPc4	část
nechal	nechat	k5eAaPmAgMnS	nechat
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
jazyce	jazyk	k1gInSc6	jazyk
(	(	kIx(	(
<g/>
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
,	,	kIx,	,
toskánštině	toskánština	k1gFnSc6	toskánština
<g/>
,	,	kIx,	,
benátštině	benátština	k1gFnSc6	benátština
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
přepsal	přepsat	k5eAaPmAgInS	přepsat
i	i	k9	i
do	do	k7c2	do
prózy	próza	k1gFnSc2	próza
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
italštině	italština	k1gFnSc6	italština
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Il	Il	k1gFnSc2	Il
libro	libra	k1gFnSc5	libra
di	di	k?	di
Messer	Messer	k1gMnSc1	Messer
Marco	Marco	k1gMnSc1	Marco
Polo	polo	k6eAd1	polo
Cittadino	Cittadin	k2eAgNnSc4d1	Cittadin
di	di	k?	di
Venezia	Venezia	k1gFnSc1	Venezia
detto	detto	k1gNnSc1	detto
il	il	k?	il
Milione	milion	k4xCgInSc5	milion
<g/>
,	,	kIx,	,
dove	dove	k5eAaImRp2nP	dove
si	se	k3xPyFc3	se
raccontano	raccontana	k1gFnSc5	raccontana
le	le	k?	le
Meraviglie	Meraviglie	k1gFnPc4	Meraviglie
del	del	k?	del
Mondo	Mondo	k1gNnSc1	Mondo
(	(	kIx(	(
<g/>
Kniha	kniha	k1gFnSc1	kniha
pana	pan	k1gMnSc2	pan
Marca	Marca	k1gFnSc1	Marca
Pola	pola	k1gFnSc1	pola
<g/>
,	,	kIx,	,
benátského	benátský	k2eAgMnSc2d1	benátský
měšťana	měšťan	k1gMnSc2	měšťan
<g/>
,	,	kIx,	,
zvaného	zvaný	k2eAgInSc2d1	zvaný
Milión	milión	k4xCgInSc4	milión
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
popisují	popisovat	k5eAaImIp3nP	popisovat
divy	div	k1gInPc1	div
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
významných	významný	k2eAgNnPc2d1	významné
zjištění	zjištění	k1gNnPc2	zjištění
Benedetteho	Benedette	k1gMnSc4	Benedette
zjistili	zjistit	k5eAaPmAgMnP	zjistit
moderní	moderní	k2eAgMnPc1d1	moderní
vědci	vědec	k1gMnPc1	vědec
<g/>
,	,	kIx,	,
že	že	k8xS	že
existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgInPc4	tři
rukopisy	rukopis	k1gInPc4	rukopis
nejbližší	blízký	k2eAgInSc1d3	Nejbližší
původnímu	původní	k2eAgInSc3d1	původní
textu	text	k1gInSc3	text
<g/>
:	:	kIx,	:
jeden	jeden	k4xCgInSc1	jeden
latinský	latinský	k2eAgInSc1d1	latinský
překlad	překlad	k1gInSc1	překlad
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgInSc1d1	francouzský
kodex	kodex	k1gInSc1	kodex
(	(	kIx(	(
<g/>
Národní	národní	k2eAgFnSc1d1	národní
knihovna	knihovna	k1gFnSc1	knihovna
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
)	)	kIx)	)
–	–	k?	–
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
textové	textový	k2eAgFnSc2d1	textová
kritiky	kritika	k1gFnSc2	kritika
nejlepší	dobrý	k2eAgFnSc2d3	nejlepší
–	–	k?	–
a	a	k8xC	a
toskánské	toskánský	k2eAgNnSc4d1	toskánské
vydání	vydání	k1gNnSc4	vydání
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cestopis	cestopis	k1gInSc1	cestopis
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgInSc7	první
zeměpisným	zeměpisný	k2eAgInSc7d1	zeměpisný
dílem	díl	k1gInSc7	díl
o	o	k7c6	o
Asii	Asie	k1gFnSc6	Asie
a	a	k8xC	a
nejcennějším	cenný	k2eAgInSc7d3	nejcennější
zdrojem	zdroj	k1gInSc7	zdroj
historických	historický	k2eAgFnPc2d1	historická
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
obzvlášť	obzvlášť	k6eAd1	obzvlášť
cenný	cenný	k2eAgInSc4d1	cenný
kvůli	kvůli	k7c3	kvůli
fantastickým	fantastický	k2eAgFnPc3d1	fantastická
legendám	legenda	k1gFnPc3	legenda
z	z	k7c2	z
raného	raný	k2eAgInSc2d1	raný
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Podává	podávat	k5eAaImIp3nS	podávat
informace	informace	k1gFnPc4	informace
o	o	k7c4	o
způsobu	způsoba	k1gFnSc4	způsoba
života	život	k1gInSc2	život
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
počasí	počasí	k1gNnSc6	počasí
<g/>
,	,	kIx,	,
pouštích	poušť	k1gFnPc6	poušť
<g/>
,	,	kIx,	,
řekách	řeka	k1gFnPc6	řeka
<g/>
,	,	kIx,	,
monzunech	monzun	k1gInPc6	monzun
<g/>
,	,	kIx,	,
o	o	k7c6	o
Tichém	tichý	k2eAgInSc6d1	tichý
a	a	k8xC	a
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc1d1	východní
Asie	Asie	k1gFnSc1	Asie
je	být	k5eAaImIp3nS	být
vykreslená	vykreslený	k2eAgFnSc1d1	vykreslená
jako	jako	k8xC	jako
velmi	velmi	k6eAd1	velmi
bohatá	bohatý	k2eAgFnSc1d1	bohatá
<g/>
,	,	kIx,	,
oplývající	oplývající	k2eAgFnSc1d1	oplývající
zlatem	zlato	k1gNnSc7	zlato
<g/>
,	,	kIx,	,
drahým	drahý	k2eAgNnSc7d1	drahé
kořením	koření	k1gNnSc7	koření
<g/>
,	,	kIx,	,
přístavy	přístav	k1gInPc7	přístav
s	s	k7c7	s
čilým	čilý	k2eAgInSc7d1	čilý
obchodním	obchodní	k2eAgInSc7d1	obchodní
ruchem	ruch	k1gInSc7	ruch
a	a	k8xC	a
velkými	velký	k2eAgNnPc7d1	velké
městy	město	k1gNnPc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
území	území	k1gNnSc4	území
od	od	k7c2	od
Persie	Persie	k1gFnSc2	Persie
po	po	k7c4	po
Čínu	Čína	k1gFnSc4	Čína
<g/>
,	,	kIx,	,
Indii	Indie	k1gFnSc4	Indie
a	a	k8xC	a
východoindické	východoindický	k2eAgNnSc4d1	východoindické
souostroví	souostroví	k1gNnSc4	souostroví
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
území	území	k1gNnPc1	území
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
Marco	Marco	k6eAd1	Marco
Polo	polo	k6eAd1	polo
sám	sám	k3xTgMnSc1	sám
nenavštívil	navštívit	k5eNaPmAgMnS	navštívit
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Sibiř	Sibiř	k1gFnSc1	Sibiř
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
více	hodně	k6eAd2	hodně
zveličených	zveličený	k2eAgFnPc2d1	zveličená
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
připisovány	připisovat	k5eAaImNgInP	připisovat
hlavně	hlavně	k6eAd1	hlavně
Rustichellovi	Rustichellův	k2eAgMnPc1d1	Rustichellův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
ojedinělých	ojedinělý	k2eAgInPc2d1	ojedinělý
názorů	názor	k1gInPc2	názor
(	(	kIx(	(
<g/>
na	na	k7c4	na
všechny	všechen	k3xTgInPc4	všechen
existují	existovat	k5eAaImIp3nP	existovat
protiargumenty	protiargument	k1gInPc1	protiargument
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
chyby	chyba	k1gFnPc4	chyba
v	v	k7c6	v
textu	text	k1gInSc6	text
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Marco	Marco	k6eAd1	Marco
Polo	polo	k6eAd1	polo
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
nikdy	nikdy	k6eAd1	nikdy
nebyl	být	k5eNaImAgMnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
největší	veliký	k2eAgFnSc7d3	veliký
záhadou	záhada	k1gFnSc7	záhada
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
o	o	k7c6	o
přítomnosti	přítomnost	k1gFnSc6	přítomnost
Marca	Marc	k2eAgFnSc1d1	Marca
Pola	pola	k1gFnSc1	pola
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
neexistují	existovat	k5eNaImIp3nP	existovat
záznamy	záznam	k1gInPc1	záznam
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
čínských	čínský	k2eAgInPc6d1	čínský
dokumentech	dokument	k1gInPc6	dokument
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
často	často	k6eAd1	často
čtené	čtený	k2eAgInPc1d1	čtený
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
impulzem	impulz	k1gInSc7	impulz
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
objevitelské	objevitelský	k2eAgFnPc4d1	objevitelská
cesty	cesta	k1gFnPc4	cesta
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pro	pro	k7c4	pro
Kryštofa	Kryštof	k1gMnSc4	Kryštof
Kolumba	Kolumbus	k1gMnSc4	Kolumbus
<g/>
.	.	kIx.	.
</s>
<s>
Údaje	údaj	k1gInPc1	údaj
z	z	k7c2	z
něj	on	k3xPp3gMnSc4	on
čerpali	čerpat	k5eAaImAgMnP	čerpat
i	i	k9	i
kartografové	kartograf	k1gMnPc1	kartograf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Marco	Marco	k6eAd1	Marco
Polo	polo	k6eAd1	polo
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
FÜLLE	FÜLLE	kA	FÜLLE
<g/>
,	,	kIx,	,
Sankja	Sankjum	k1gNnSc2	Sankjum
<g/>
.	.	kIx.	.
</s>
<s>
Bol	bol	k1gInSc1	bol
Marco	Marco	k6eAd1	Marco
Polo	polo	k6eAd1	polo
v	v	k7c6	v
Číne	Číne	k1gFnSc6	Číne
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
8	[number]	k4	8
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
/	/	kIx~	/
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s.	s.	k?	s.
248	[number]	k4	248
<g/>
–	–	k?	–
<g/>
257	[number]	k4	257
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
6097	[number]	k4	6097
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ELPL	ELPL	kA	ELPL
<g/>
,	,	kIx,	,
Mirek	Mirek	k1gMnSc1	Mirek
<g/>
.	.	kIx.	.
</s>
<s>
Marco	Marco	k6eAd1	Marco
Polo	polo	k6eAd1	polo
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
a	a	k8xC	a
doba	doba	k1gFnSc1	doba
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Družstvo	družstvo	k1gNnSc1	družstvo
Moravského	moravský	k2eAgNnSc2d1	Moravské
kola	kolo	k1gNnSc2	kolo
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
230	[number]	k4	230
s.	s.	k?	s.
</s>
</p>
<p>
<s>
POLO	pola	k1gFnSc5	pola
<g/>
,	,	kIx,	,
Marco	Marca	k1gFnSc5	Marca
<g/>
.	.	kIx.	.
</s>
<s>
Marka	marka	k1gFnSc1	marka
Pavlova	Pavlův	k2eAgFnSc1d1	Pavlova
z	z	k7c2	z
Benátek	Benátky	k1gFnPc2	Benátky
Milion	milion	k4xCgInSc1	milion
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Justin	Justina	k1gFnPc2	Justina
Václav	Václav	k1gMnSc1	Václav
Prášek	Prášek	k1gMnSc1	Prášek
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
<g/>
,	,	kIx,	,
1902	[number]	k4	1902
<g/>
.	.	kIx.	.
305	[number]	k4	305
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Marco	Marco	k6eAd1	Marco
Polo	polo	k6eAd1	polo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Marco	Marco	k1gMnSc1	Marco
Polo	polo	k6eAd1	polo
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Marco	Marco	k6eAd1	Marco
Polo	polo	k6eAd1	polo
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Marco	Marco	k6eAd1	Marco
Polo	polo	k6eAd1	polo
</s>
</p>
<p>
<s>
Marco	Marco	k6eAd1	Marco
Polo	polo	k6eAd1	polo
–	–	k?	–
Milion	milion	k4xCgInSc1	milion
</s>
</p>
