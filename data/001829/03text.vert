<s>
Funkcionalismus	funkcionalismus	k1gInSc1	funkcionalismus
je	být	k5eAaImIp3nS	být
architektonický	architektonický	k2eAgInSc4d1	architektonický
sloh	sloh	k1gInSc4	sloh
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
lze	lze	k6eAd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
do	do	k7c2	do
obecného	obecný	k2eAgInSc2d1	obecný
pojmu	pojem	k1gInSc2	pojem
moderní	moderní	k2eAgFnSc1d1	moderní
architektura	architektura	k1gFnSc1	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
začala	začít	k5eAaPmAgFnS	začít
krystalizovat	krystalizovat	k5eAaImF	krystalizovat
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
předcházející	předcházející	k2eAgFnPc1d1	předcházející
historické	historický	k2eAgFnPc1d1	historická
slohy	sloha	k1gFnPc1	sloha
<g/>
,	,	kIx,	,
hledala	hledat	k5eAaImAgFnS	hledat
svůj	svůj	k3xOyFgInSc4	svůj
výraz	výraz	k1gInSc4	výraz
ve	v	k7c6	v
formách	forma	k1gFnPc6	forma
vyjadřujících	vyjadřující	k2eAgFnPc2d1	vyjadřující
její	její	k3xOp3gFnSc4	její
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
,	,	kIx,	,
ovlivněnou	ovlivněný	k2eAgFnSc4d1	ovlivněná
celospolečenskými	celospolečenský	k2eAgFnPc7d1	celospolečenská
změnami	změna	k1gFnPc7	změna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
stavebnictví	stavebnictví	k1gNnSc2	stavebnictví
to	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
stavět	stavět	k5eAaImF	stavět
domy	dům	k1gInPc4	dům
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gFnSc2	jejich
funkce	funkce	k1gFnSc2	funkce
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
požadavky	požadavek	k1gInPc4	požadavek
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
a	a	k8xC	a
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
funkcionalismus	funkcionalismus	k1gInSc1	funkcionalismus
vedoucím	vedoucí	k2eAgMnSc7d1	vedoucí
architektonickým	architektonický	k2eAgInSc7d1	architektonický
slohem	sloh	k1gInSc7	sloh
i	i	k9	i
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
a	a	k8xC	a
svými	svůj	k3xOyFgFnPc7	svůj
realizacemi	realizace	k1gFnPc7	realizace
v	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
přiřadil	přiřadit	k5eAaPmAgMnS	přiřadit
československou	československý	k2eAgFnSc4d1	Československá
architekturu	architektura	k1gFnSc4	architektura
k	k	k7c3	k
evropskému	evropský	k2eAgInSc3d1	evropský
vrcholu	vrchol	k1gInSc3	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Koncept	koncept	k1gInSc1	koncept
funckionalismu	funckionalismus	k1gInSc2	funckionalismus
dle	dle	k7c2	dle
první	první	k4xOgFnSc2	první
definice	definice	k1gFnSc2	definice
od	od	k7c2	od
Louise	Louis	k1gMnSc2	Louis
Sullivana	Sullivan	k1gMnSc2	Sullivan
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
lze	lze	k6eAd1	lze
shrnout	shrnout	k5eAaPmF	shrnout
jednou	jeden	k4xCgFnSc7	jeden
větou	věta	k1gFnSc7	věta
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Forma	forma	k1gFnSc1	forma
následuje	následovat	k5eAaImIp3nS	následovat
funkci	funkce	k1gFnSc4	funkce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
funkcionalistickou	funkcionalistický	k2eAgFnSc4d1	funkcionalistická
architekturu	architektura	k1gFnSc4	architektura
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgFnPc1d1	typická
účelové	účelový	k2eAgFnPc1d1	účelová
budovy	budova	k1gFnPc1	budova
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
tvarů	tvar	k1gInPc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
převážně	převážně	k6eAd1	převážně
nové	nový	k2eAgInPc1d1	nový
materiály	materiál	k1gInPc1	materiál
(	(	kIx(	(
<g/>
např.	např.	kA	např.
šamotové	šamotový	k2eAgFnPc4d1	šamotová
cihly	cihla	k1gFnPc4	cihla
<g/>
,	,	kIx,	,
železo	železo	k1gNnSc4	železo
nebo	nebo	k8xC	nebo
beton	beton	k1gInSc1	beton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Architektura	architektura	k1gFnSc1	architektura
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
Adolf	Adolf	k1gMnSc1	Adolf
Loos	Loosa	k1gFnPc2	Loosa
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
předních	přední	k2eAgMnPc2d1	přední
představitelů	představitel	k1gMnPc2	představitel
funkcionalismu	funkcionalismus	k1gInSc2	funkcionalismus
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
nepatří	patřit	k5eNaImIp3nS	patřit
mezi	mezi	k7c4	mezi
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ale	ale	k9	ale
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemá	mít	k5eNaImIp3nS	mít
splňovat	splňovat	k5eAaImF	splňovat
estetické	estetický	k2eAgInPc4d1	estetický
ideály	ideál	k1gInPc4	ideál
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
Le	Le	k1gMnSc1	Le
Corbusier	Corbusier	k1gMnSc1	Corbusier
představil	představit	k5eAaPmAgMnS	představit
funkcionalismus	funkcionalismus	k1gInSc4	funkcionalismus
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
jako	jako	k9	jako
novou	nový	k2eAgFnSc4d1	nová
formu	forma	k1gFnSc4	forma
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
dokázala	dokázat	k5eAaPmAgFnS	dokázat
oprostit	oprostit	k5eAaPmF	oprostit
od	od	k7c2	od
okázalosti	okázalost	k1gFnSc2	okázalost
a	a	k8xC	a
kultu	kult	k1gInSc2	kult
strojené	strojený	k2eAgFnSc2d1	strojená
estetiky	estetika	k1gFnSc2	estetika
přetrvávající	přetrvávající	k2eAgFnPc1d1	přetrvávající
ještě	ještě	k6eAd1	ještě
počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Formy	forma	k1gFnSc2	forma
považované	považovaný	k2eAgFnSc2d1	považovaná
za	za	k7c4	za
přeplácanost	přeplácanost	k1gFnSc4	přeplácanost
a	a	k8xC	a
zbytečnou	zbytečný	k2eAgFnSc4d1	zbytečná
komplikovanost	komplikovanost	k1gFnSc4	komplikovanost
byly	být	k5eAaImAgFnP	být
nahrazeny	nahradit	k5eAaPmNgFnP	nahradit
geometrickou	geometrický	k2eAgFnSc7d1	geometrická
čistotou	čistota	k1gFnSc7	čistota
tvarů	tvar	k1gInPc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
Adolf	Adolf	k1gMnSc1	Adolf
Loos	Loosa	k1gFnPc2	Loosa
označil	označit	k5eAaPmAgMnS	označit
ornament	ornament	k1gInSc4	ornament
za	za	k7c4	za
zločin	zločin	k1gInSc4	zločin
<g/>
,	,	kIx,	,
Le	Le	k1gMnSc1	Le
Corbusier	Corbusier	k1gMnSc1	Corbusier
říká	říkat	k5eAaImIp3nS	říkat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dekorace	dekorace	k1gFnSc1	dekorace
je	být	k5eAaImIp3nS	být
smyslné	smyslný	k2eAgFnSc3d1	smyslná
a	a	k8xC	a
primitivní	primitivní	k2eAgFnSc2d1	primitivní
povahy	povaha	k1gFnSc2	povaha
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
a	a	k8xC	a
hodí	hodit	k5eAaImIp3nS	hodit
se	se	k3xPyFc4	se
toliko	toliko	k6eAd1	toliko
pro	pro	k7c4	pro
nižší	nízký	k2eAgFnPc4d2	nižší
třídy	třída	k1gFnPc4	třída
<g/>
,	,	kIx,	,
sedláky	sedlák	k1gMnPc4	sedlák
a	a	k8xC	a
divochy	divoch	k1gMnPc4	divoch
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Le	Le	k1gFnSc1	Le
Cobrusier	Cobrusier	k1gInSc1	Cobrusier
také	také	k6eAd1	také
stanovil	stanovit	k5eAaPmAgInS	stanovit
proslulých	proslulý	k2eAgInPc2d1	proslulý
"	"	kIx"	"
<g/>
Pět	pět	k4xCc4	pět
bodů	bod	k1gInPc2	bod
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
(	(	kIx(	(
<g/>
funkcionalismu	funkcionalismus	k1gInSc2	funkcionalismus
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgFnPc2	tento
pět	pět	k4xCc1	pět
tezí	teze	k1gFnPc2	teze
prakticky	prakticky	k6eAd1	prakticky
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
hlavní	hlavní	k2eAgFnPc4d1	hlavní
vymoženosti	vymoženost	k1gFnPc4	vymoženost
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
techniky	technika	k1gFnSc2	technika
<g/>
:	:	kIx,	:
Sloupy	sloup	k1gInPc7	sloup
<g/>
:	:	kIx,	:
stavět	stavět	k5eAaImF	stavět
domy	dům	k1gInPc4	dům
na	na	k7c6	na
sloupech	sloup	k1gInPc6	sloup
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
přízemí	přízemí	k1gNnSc1	přízemí
pro	pro	k7c4	pro
zeleň	zeleň	k1gFnSc4	zeleň
a	a	k8xC	a
volný	volný	k2eAgInSc4d1	volný
pohyb	pohyb	k1gInSc4	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Střešní	střešní	k2eAgFnPc1d1	střešní
zahrady	zahrada	k1gFnPc1	zahrada
<g/>
:	:	kIx,	:
technika	technika	k1gFnSc1	technika
plochých	plochý	k2eAgFnPc2d1	plochá
střech	střecha	k1gFnPc2	střecha
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
budovat	budovat	k5eAaImF	budovat
na	na	k7c6	na
střechách	střecha	k1gFnPc6	střecha
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Nahrazují	nahrazovat	k5eAaImIp3nP	nahrazovat
zeleň	zeleň	k1gFnSc4	zeleň
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
dům	dům	k1gInSc1	dům
místu	místo	k1gNnSc3	místo
odebral	odebrat	k5eAaPmAgInS	odebrat
<g/>
.	.	kIx.	.
</s>
<s>
Volný	volný	k2eAgInSc1d1	volný
půdorys	půdorys	k1gInSc1	půdorys
<g/>
:	:	kIx,	:
sloupy	sloup	k1gInPc1	sloup
nesou	nést	k5eAaImIp3nP	nést
síly	síla	k1gFnPc4	síla
všech	všecek	k3xTgNnPc2	všecek
podlaží	podlaží	k1gNnPc2	podlaží
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
volné	volný	k2eAgNnSc4d1	volné
členění	členění	k1gNnSc4	členění
prostoru	prostor	k1gInSc2	prostor
nenosnými	nosný	k2eNgFnPc7d1	nenosná
příčkami	příčka	k1gFnPc7	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Pásová	pásový	k2eAgNnPc1d1	pásové
okna	okno	k1gNnPc1	okno
<g/>
:	:	kIx,	:
systém	systém	k1gInSc1	systém
sloupů	sloup	k1gInPc2	sloup
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vést	vést	k5eAaImF	vést
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
okna	okno	k1gNnPc4	okno
mezi	mezi	k7c4	mezi
sloupy	sloup	k1gInPc4	sloup
<g/>
.	.	kIx.	.
</s>
<s>
Volné	volný	k2eAgNnSc1d1	volné
průčelí	průčelí	k1gNnSc1	průčelí
<g/>
:	:	kIx,	:
konzolovitě	konzolovitě	k6eAd1	konzolovitě
vyvedené	vyvedený	k2eAgInPc1d1	vyvedený
stropy	strop	k1gInPc1	strop
uvolňují	uvolňovat	k5eAaImIp3nP	uvolňovat
průčelí	průčelí	k1gNnSc4	průčelí
pro	pro	k7c4	pro
řešení	řešení	k1gNnSc4	řešení
oken	okno	k1gNnPc2	okno
bez	bez	k7c2	bez
přímé	přímý	k2eAgFnSc2d1	přímá
návaznosti	návaznost	k1gFnSc2	návaznost
na	na	k7c4	na
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
dělení	dělení	k1gNnSc4	dělení
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
rysem	rys	k1gInSc7	rys
funkcionalistických	funkcionalistický	k2eAgFnPc2d1	funkcionalistická
staveb	stavba	k1gFnPc2	stavba
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
železobetonová	železobetonový	k2eAgFnSc1d1	železobetonová
skeletová	skeletový	k2eAgFnSc1d1	skeletová
konstrukce	konstrukce	k1gFnSc1	konstrukce
a	a	k8xC	a
volný	volný	k2eAgInSc1d1	volný
půdorys	půdorys	k1gInSc1	půdorys
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
moderně	moderně	k6eAd1	moderně
řešených	řešený	k2eAgFnPc2d1	řešená
veřejných	veřejný	k2eAgFnPc2d1	veřejná
a	a	k8xC	a
obytných	obytný	k2eAgFnPc2d1	obytná
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1932	[number]	k4	1932
<g/>
-	-	kIx~	-
<g/>
1936	[number]	k4	1936
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
6	[number]	k4	6
-	-	kIx~	-
Dejvicích	Dejvice	k1gFnPc6	Dejvice
Osada	osada	k1gFnSc1	osada
Baba	baba	k1gFnSc1	baba
(	(	kIx(	(
<g/>
osada	osada	k1gFnSc1	osada
Svazu	svaz	k1gInSc2	svaz
československého	československý	k2eAgNnSc2d1	Československé
díla	dílo	k1gNnSc2	dílo
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řešená	řešený	k2eAgFnSc1d1	řešená
předními	přední	k2eAgMnPc7d1	přední
českými	český	k2eAgMnPc7d1	český
architekty	architekt	k1gMnPc7	architekt
s	s	k7c7	s
přispěním	přispění	k1gNnSc7	přispění
holandského	holandský	k2eAgMnSc2d1	holandský
architekta	architekt	k1gMnSc2	architekt
Marta	Marta	k1gFnSc1	Marta
Stama	Stama	k1gFnSc1	Stama
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
kolonie	kolonie	k1gFnSc1	kolonie
Nový	Nový	k1gMnSc1	Nový
dům	dům	k1gInSc1	dům
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
nebo	nebo	k8xC	nebo
město	město	k1gNnSc4	město
Zlín	Zlín	k1gInSc1	Zlín
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
významu	význam	k1gInSc6	význam
meziválečné	meziválečný	k2eAgFnSc2d1	meziválečná
funkcionalistické	funkcionalistický	k2eAgFnSc2d1	funkcionalistická
architektury	architektura	k1gFnSc2	architektura
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
svědčí	svědčit	k5eAaImIp3nS	svědčit
účast	účast	k1gFnSc1	účast
proslulých	proslulý	k2eAgMnPc2d1	proslulý
architektů	architekt	k1gMnPc2	architekt
<g/>
:	:	kIx,	:
Adolfa	Adolf	k1gMnSc2	Adolf
Loose	Loos	k1gMnSc2	Loos
(	(	kIx(	(
<g/>
Müllerova	Müllerův	k2eAgFnSc1d1	Müllerova
vila	vila	k1gFnSc1	vila
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ludwiga	Ludwiga	k1gFnSc1	Ludwiga
Mies	Miesa	k1gFnPc2	Miesa
van	vana	k1gFnPc2	vana
der	drát	k5eAaImRp2nS	drát
Roheho	Roheha	k1gMnSc5	Roheha
(	(	kIx(	(
<g/>
Vila	vít	k5eAaImAgFnS	vít
Tugendhat	Tugendhat	k1gFnSc1	Tugendhat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Le	Le	k1gFnSc1	Le
Corbusiera	Corbusiera	k1gFnSc1	Corbusiera
(	(	kIx(	(
<g/>
Zlín	Zlín	k1gInSc1	Zlín
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Marta	Marta	k1gFnSc1	Marta
Stama	Stama	k1gFnSc1	Stama
(	(	kIx(	(
<g/>
Paličkova	Paličkův	k2eAgFnSc1d1	Paličkova
vila	vila	k1gFnSc1	vila
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Československa	Československo	k1gNnSc2	Československo
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
pozoruhodné	pozoruhodný	k2eAgFnPc1d1	pozoruhodná
funkcionalistické	funkcionalistický	k2eAgFnPc1d1	funkcionalistická
stavby	stavba	k1gFnPc1	stavba
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Holandsko	Holandsko	k1gNnSc1	Holandsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
širšímu	široký	k2eAgNnSc3d2	širší
<g/>
,	,	kIx,	,
mezinárodnímu	mezinárodní	k2eAgNnSc3d1	mezinárodní
<g/>
,	,	kIx,	,
uplatnění	uplatnění	k1gNnSc4	uplatnění
funkcionalismu	funkcionalismus	k1gInSc2	funkcionalismus
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc4	jehož
zásady	zásada	k1gFnPc4	zásada
formuloval	formulovat	k5eAaImAgMnS	formulovat
Congrè	Congrè	k1gMnSc1	Congrè
International	International	k1gFnSc2	International
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Architecture	Architectur	k1gMnSc5	Architectur
Moderne	Modern	k1gInSc5	Modern
-	-	kIx~	-
CIAM	CIAM	kA	CIAM
(	(	kIx(	(
<g/>
Kongres	kongres	k1gInSc1	kongres
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ustavený	ustavený	k2eAgInSc1d1	ustavený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
La	la	k1gNnSc2	la
Sarraz	Sarraz	k1gInSc1	Sarraz
u	u	k7c2	u
Lausanne	Lausanne	k1gNnSc2	Lausanne
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
styl	styl	k1gInSc1	styl
(	(	kIx(	(
<g/>
International	International	k1gFnSc1	International
Style	styl	k1gInSc5	styl
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ujímají	ujímat	k5eAaImIp3nP	ujímat
zásady	zásada	k1gFnPc1	zásada
funkcionalismu	funkcionalismus	k1gInSc2	funkcionalismus
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
Skandinávie	Skandinávie	k1gFnSc1	Skandinávie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
2	[number]	k4	2
<g/>
.	.	kIx.	.
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
znamenala	znamenat	k5eAaImAgFnS	znamenat
pro	pro	k7c4	pro
evropskou	evropský	k2eAgFnSc4d1	Evropská
avantgardu	avantgarda	k1gFnSc4	avantgarda
stagnaci	stagnace	k1gFnSc4	stagnace
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
úplné	úplný	k2eAgNnSc1d1	úplné
opuštění	opuštění	k1gNnSc4	opuštění
programových	programový	k2eAgFnPc2d1	programová
zásad	zásada	k1gFnPc2	zásada
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
stylu	styl	k1gInSc2	styl
mimo	mimo	k7c4	mimo
Evropu	Evropa	k1gFnSc4	Evropa
na	na	k7c4	na
zámořské	zámořský	k2eAgInPc4d1	zámořský
kontinenty	kontinent	k1gInPc4	kontinent
<g/>
,	,	kIx,	,
do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Japonska	Japonsko	k1gNnSc2	Japonsko
a	a	k8xC	a
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
propojení	propojení	k1gNnSc3	propojení
zásad	zásada	k1gFnPc2	zásada
funkcionalismu	funkcionalismus	k1gInSc2	funkcionalismus
s	s	k7c7	s
tradicí	tradice	k1gFnSc7	tradice
a	a	k8xC	a
zvyklostmi	zvyklost	k1gFnPc7	zvyklost
místních	místní	k2eAgFnPc2d1	místní
stavebních	stavební	k2eAgFnPc2d1	stavební
kultur	kultura	k1gFnPc2	kultura
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
stanovení	stanovení	k1gNnSc4	stanovení
nového	nový	k2eAgInSc2d1	nový
konceptu	koncept	k1gInSc2	koncept
soudobé	soudobý	k2eAgFnSc2d1	soudobá
architektury	architektura	k1gFnSc2	architektura
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
ideou	idea	k1gFnSc7	idea
byla	být	k5eAaImAgFnS	být
tzv.	tzv.	kA	tzv.
organická	organický	k2eAgFnSc1d1	organická
architektura	architektura	k1gFnSc1	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
Benš	Benš	k1gMnSc1	Benš
Otto	Otto	k1gMnSc1	Otto
Eisler	Eisler	k1gMnSc1	Eisler
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Fragner	Fragner	k1gMnSc1	Fragner
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Fuchs	Fuchs	k1gMnSc1	Fuchs
Victor	Victor	k1gMnSc1	Victor
Fürth	Fürth	k1gMnSc1	Fürth
František	František	k1gMnSc1	František
Lydie	Lydie	k1gFnSc2	Lydie
Gahura	Gahura	k1gFnSc1	Gahura
Jan	Jan	k1gMnSc1	Jan
Gillar	Gillar	k1gMnSc1	Gillar
Karel	Karel	k1gMnSc1	Karel
Hannauer	Hannauer	k1gMnSc1	Hannauer
ml.	ml.	kA	ml.
Josef	Josef	k1gMnSc1	Josef
Havlíček	Havlíček	k1gMnSc1	Havlíček
Karel	Karel	k1gMnSc1	Karel
Honzík	Honzík	k1gMnSc1	Honzík
Pavel	Pavel	k1gMnSc1	Pavel
Janák	Janák	k1gMnSc1	Janák
Vladimír	Vladimír	k1gMnSc1	Vladimír
Karfík	Karfík	k1gMnSc1	Karfík
Karel	Karel	k1gMnSc1	Karel
Kohn	Kohn	k1gMnSc1	Kohn
mj.	mj.	kA	mj.
spoluautor	spoluautor	k1gMnSc1	spoluautor
letenského	letenský	k2eAgInSc2d1	letenský
Molochova	Molochův	k2eAgInSc2d1	Molochův
Otto	Otto	k1gMnSc1	Otto
Kohn	Kohn	k1gMnSc1	Kohn
mj.	mj.	kA	mj.
spoluautor	spoluautor	k1gMnSc1	spoluautor
letenského	letenský	k2eAgInSc2d1	letenský
Molochova	Molochův	k2eAgInSc2d1	Molochův
Josef	Josef	k1gMnSc1	Josef
Kranz	Kranza	k1gFnPc2	Kranza
Jaromír	Jaromíra	k1gFnPc2	Jaromíra
Krejcar	krejcar	k1gInSc1	krejcar
Jiří	Jiří	k1gMnSc1	Jiří
Kroha	Kroha	k1gMnSc1	Kroha
Evžen	Evžen	k1gMnSc1	Evžen
Linhart	Linhart	k1gMnSc1	Linhart
Ladislav	Ladislav	k1gMnSc1	Ladislav
Machoň	Machoň	k1gMnSc1	Machoň
Otakar	Otakar	k1gMnSc1	Otakar
Novotný	Novotný	k1gMnSc1	Novotný
<g />
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Pilc	Pilc	k1gInSc4	Pilc
Karel	Karel	k1gMnSc1	Karel
Řepa	Řepa	k1gMnSc1	Řepa
Jan	Jan	k1gMnSc1	Jan
Víšek	Víšek	k1gMnSc1	Víšek
Ladislav	Ladislav	k1gMnSc1	Ladislav
Žák	Žák	k1gMnSc1	Žák
Le	Le	k1gMnSc1	Le
Corbusier	Corbusier	k1gMnSc1	Corbusier
Ludwig	Ludwig	k1gMnSc1	Ludwig
Mies	Miesa	k1gFnPc2	Miesa
van	van	k1gInSc4	van
der	drát	k5eAaImRp2nS	drát
Rohe	Roh	k1gMnSc2	Roh
Mart	Marta	k1gFnPc2	Marta
Stam	Stam	k1gMnSc1	Stam
Walter	Walter	k1gMnSc1	Walter
Gropius	Gropius	k1gMnSc1	Gropius
Hannes	Hannes	k1gMnSc1	Hannes
Meyer	Meyer	k1gMnSc1	Meyer
Arne	Arne	k1gMnSc1	Arne
Jacobsen	Jacobsen	k1gInSc4	Jacobsen
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Seznam	seznam	k1gInSc4	seznam
funkcionalistických	funkcionalistický	k2eAgFnPc2d1	funkcionalistická
staveb	stavba	k1gFnPc2	stavba
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
Seznam	seznam	k1gInSc4	seznam
funkcionalistických	funkcionalistický	k2eAgFnPc2d1	funkcionalistická
staveb	stavba	k1gFnPc2	stavba
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
a	a	k8xC	a
Seznam	seznam	k1gInSc4	seznam
funkcionalistických	funkcionalistický	k2eAgFnPc2d1	funkcionalistická
staveb	stavba	k1gFnPc2	stavba
ve	v	k7c6	v
Zlíně	Zlín	k1gInSc6	Zlín
<g/>
.	.	kIx.	.
</s>
<s>
Terasy	terasa	k1gFnPc1	terasa
Barrandov	Barrandov	k1gInSc1	Barrandov
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
Vila	vila	k1gFnSc1	vila
Tugendhat	Tugendhat	k1gFnSc1	Tugendhat
(	(	kIx(	(
<g/>
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
Kostel	kostel	k1gInSc1	kostel
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
Zemanova	Zemanův	k2eAgFnSc1d1	Zemanova
kavárna	kavárna	k1gFnSc1	kavárna
(	(	kIx(	(
<g/>
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
město	město	k1gNnSc1	město
Zlín	Zlín	k1gInSc1	Zlín
Památník	památník	k1gInSc1	památník
Tomáše	Tomáš	k1gMnSc2	Tomáš
Bati	Baťa	k1gMnSc2	Baťa
(	(	kIx(	(
<g/>
Zlín	Zlín	k1gInSc1	Zlín
<g/>
)	)	kIx)	)
Baťův	Baťův	k2eAgInSc1d1	Baťův
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
(	(	kIx(	(
<g/>
Zlín	Zlín	k1gInSc1	Zlín
<g/>
)	)	kIx)	)
Osada	osada	k1gFnSc1	osada
Baba	baba	k1gFnSc1	baba
Müllerova	Müllerův	k2eAgFnSc1d1	Müllerova
vila	vila	k1gFnSc1	vila
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Loosova	Loosův	k2eAgFnSc1d1	Loosova
vila	vila	k1gFnSc1	vila
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
Nakládalova	Nakládalův	k2eAgFnSc1d1	Nakládalova
vila	vila	k1gFnSc1	vila
(	(	kIx(	(
<g/>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
Veletržní	veletržní	k2eAgInSc1d1	veletržní
palác	palác	k1gInSc1	palác
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
Státní	státní	k2eAgNnSc1d1	státní
reformní	reformní	k2eAgNnSc1d1	reformní
reálné	reálný	k2eAgNnSc1d1	reálné
gymnasium	gymnasium	k1gNnSc1	gymnasium
(	(	kIx(	(
<g/>
Český	český	k2eAgInSc1d1	český
Těšín	Těšín	k1gInSc1	Těšín
<g/>
)	)	kIx)	)
Volmanova	Volmanův	k2eAgFnSc1d1	Volmanova
vila	vila	k1gFnSc1	vila
(	(	kIx(	(
<g/>
Čelákovice	Čelákovice	k1gFnPc1	Čelákovice
<g/>
)	)	kIx)	)
Vila	vila	k1gFnSc1	vila
Franze	Franze	k1gFnSc2	Franze
Nožičky	nožička	k1gFnSc2	nožička
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
Kamenice	Kamenice	k1gFnSc1	Kamenice
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Kavárna	kavárna	k1gFnSc1	kavárna
Era	Era	k1gFnSc1	Era
(	(	kIx(	(
<g/>
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
Hotel	hotel	k1gInSc1	hotel
AXA	AXA	kA	AXA
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
Hotel	hotel	k1gInSc1	hotel
Juliš	Juliš	k1gInSc1	Juliš
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
Budova	budova	k1gFnSc1	budova
Spolku	spolek	k1gInSc2	spolek
výtvarných	výtvarný	k2eAgMnPc2d1	výtvarný
umělců	umělec	k1gMnPc2	umělec
Mánes	Mánes	k1gMnSc1	Mánes
Obchodní	obchodní	k2eAgInSc4d1	obchodní
dům	dům	k1gInSc4	dům
Brouk	brouk	k1gMnSc1	brouk
a	a	k8xC	a
Babka	Babka	k1gMnSc1	Babka
(	(	kIx(	(
<g/>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
)	)	kIx)	)
Národní	národní	k2eAgNnSc1d1	národní
technické	technický	k2eAgNnSc1d1	technické
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
pozdně	pozdně	k6eAd1	pozdně
funkcionalistická	funkcionalistický	k2eAgFnSc1d1	funkcionalistická
budova	budova	k1gFnSc1	budova
v	v	k7c6	v
silně	silně	k6eAd1	silně
klasicizujícím	klasicizující	k2eAgInSc6d1	klasicizující
<g />
.	.	kIx.	.
</s>
<s>
pojetí	pojetí	k1gNnSc1	pojetí
<g/>
)	)	kIx)	)
Obchodní	obchodní	k2eAgInSc1d1	obchodní
dům	dům	k1gInSc1	dům
Bílá	bílý	k2eAgFnSc1d1	bílá
Labuť	labuť	k1gFnSc1	labuť
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
Palác	palác	k1gInSc1	palác
Elektrických	elektrický	k2eAgFnPc2d1	elektrická
drah	draha	k1gFnPc2	draha
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
Holešovice	Holešovice	k1gFnPc1	Holešovice
<g/>
)	)	kIx)	)
Villa	Villa	k1gFnSc1	Villa
Savoye	Savoy	k1gFnSc2	Savoy
v	v	k7c4	v
Poisy	Poisa	k1gFnPc4	Poisa
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Palác	palác	k1gInSc1	palác
Centrosojuzu	Centrosojuz	k1gInSc2	Centrosojuz
<g/>
,	,	kIx,	,
Moskva	Moskva	k1gFnSc1	Moskva
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Brazílie	Brazílie	k1gFnSc2	Brazílie
Brasília	Brasílium	k1gNnSc2	Brasílium
Helsinský	helsinský	k2eAgInSc1d1	helsinský
olympijský	olympijský	k2eAgInSc1d1	olympijský
stadion	stadion	k1gInSc1	stadion
<g/>
,	,	kIx,	,
Helsinky	Helsinky	k1gFnPc1	Helsinky
<g/>
,	,	kIx,	,
Finsko	Finsko	k1gNnSc1	Finsko
Továrna	továrna	k1gFnSc1	továrna
Van	van	k1gInSc1	van
Nelle	Nelle	k1gFnSc1	Nelle
<g/>
,	,	kIx,	,
Rotterdam	Rotterdam	k1gInSc1	Rotterdam
</s>
