<p>
<s>
Pavlína	Pavlína	k1gFnSc1	Pavlína
Danková	Danková	k1gFnSc1	Danková
(	(	kIx(	(
<g/>
*	*	kIx~	*
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
Těšín	Těšín	k1gInSc1	Těšín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
moderátorka	moderátorka	k1gFnSc1	moderátorka
<g/>
,	,	kIx,	,
dramaturgyně	dramaturgyně	k1gFnSc1	dramaturgyně
<g/>
,	,	kIx,	,
tisková	tiskový	k2eAgFnSc1d1	tisková
mluvčí	mluvčí	k1gFnSc1	mluvčí
Motolské	motolský	k2eAgFnSc2d1	motolská
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobní	osobní	k2eAgInSc4d1	osobní
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
Pavlína	Pavlína	k1gFnSc1	Pavlína
Danková	Danková	k1gFnSc1	Danková
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Havířova	Havířov	k1gInSc2	Havířov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
studovala	studovat	k5eAaImAgFnS	studovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Konzervatoři	konzervatoř	k1gFnSc6	konzervatoř
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Ježka	Ježek	k1gMnSc2	Ježek
vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
herectví	herectví	k1gNnSc3	herectví
a	a	k8xC	a
moderování	moderování	k1gNnSc3	moderování
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
se	se	k3xPyFc4	se
hlásila	hlásit	k5eAaImAgFnS	hlásit
na	na	k7c4	na
DAMU	DAMU	kA	DAMU
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
jí	on	k3xPp3gFnSc3	on
však	však	k9	však
nepřijali	přijmout	k5eNaPmAgMnP	přijmout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
na	na	k7c4	na
Univerzitu	univerzita	k1gFnSc4	univerzita
Jana	Jan	k1gMnSc2	Jan
Amose	Amos	k1gMnSc2	Amos
Komenského	Komenský	k1gMnSc2	Komenský
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studovala	studovat	k5eAaImAgFnS	studovat
bakalářský	bakalářský	k2eAgInSc4d1	bakalářský
obor	obor	k1gInSc4	obor
Sociální	sociální	k2eAgFnSc1d1	sociální
a	a	k8xC	a
masová	masový	k2eAgFnSc1d1	masová
komunikace	komunikace	k1gFnSc1	komunikace
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
ještě	ještě	k6eAd1	ještě
studovala	studovat	k5eAaImAgFnS	studovat
navazující	navazující	k2eAgInSc4d1	navazující
magisterký	magisterký	k2eAgInSc4d1	magisterký
obor	obor	k1gInSc4	obor
Sociální	sociální	k2eAgFnSc1d1	sociální
a	a	k8xC	a
masová	masový	k2eAgFnSc1d1	masová
komunikace	komunikace	k1gFnSc1	komunikace
na	na	k7c6	na
téže	týž	k3xTgFnSc6	týž
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
titul	titul	k1gInSc4	titul
Mgr.	Mgr.	kA	Mgr.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
rozvedená	rozvedená	k1gFnSc1	rozvedená
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
syna	syn	k1gMnSc4	syn
Tomáše	Tomáš	k1gMnSc4	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
partnerem	partner	k1gMnSc7	partner
je	být	k5eAaImIp3nS	být
manažer	manažer	k1gMnSc1	manažer
pojišťovny	pojišťovna	k1gFnSc2	pojišťovna
Milan	Milan	k1gMnSc1	Milan
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
jí	on	k3xPp3gFnSc3	on
byla	být	k5eAaImAgFnS	být
diagnostikována	diagnostikovat	k5eAaBmNgFnS	diagnostikovat
rakovina	rakovina	k1gFnSc1	rakovina
prsu	prs	k1gInSc2	prs
<g/>
.	.	kIx.	.
</s>
<s>
Pořád	pořád	k6eAd1	pořád
chodí	chodit	k5eAaImIp3nP	chodit
na	na	k7c4	na
kontroly	kontrola	k1gFnPc4	kontrola
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
hlídaná	hlídaný	k2eAgFnSc1d1	hlídaná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Profesní	profesní	k2eAgFnSc1d1	profesní
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
pracovala	pracovat	k5eAaImAgFnS	pracovat
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Černé	Černé	k2eAgFnSc2d1	Černé
ovce	ovce	k1gFnSc2	ovce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
pracovala	pracovat	k5eAaImAgFnS	pracovat
na	na	k7c4	na
televizní	televizní	k2eAgFnSc4d1	televizní
stanici	stanice	k1gFnSc4	stanice
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
moderovala	moderovat	k5eAaBmAgFnS	moderovat
a	a	k8xC	a
dělala	dělat	k5eAaImAgFnS	dělat
dramaturgyni	dramaturgyně	k1gFnSc4	dramaturgyně
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Občanské	občanský	k2eAgNnSc1d1	občanské
judo	judo	k1gNnSc1	judo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
pořad	pořad	k1gInSc1	pořad
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
vedoucí	vedoucí	k1gMnSc1	vedoucí
Odboru	odbor	k1gInSc2	odbor
komunikace	komunikace	k1gFnSc2	komunikace
Motolské	motolský	k2eAgFnSc2d1	motolská
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
nahradila	nahradit	k5eAaPmAgFnS	nahradit
Evu	Eva	k1gFnSc4	Eva
Jurinovou	Jurinová	k1gFnSc4	Jurinová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
odvolaná	odvolaný	k2eAgFnSc1d1	odvolaná
kvůli	kvůli	k7c3	kvůli
sporu	spor	k1gInSc3	spor
s	s	k7c7	s
ředitelem	ředitel	k1gMnSc7	ředitel
Miloslavem	Miloslav	k1gMnSc7	Miloslav
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
také	také	k9	také
patronkou	patronka	k1gFnSc7	patronka
projektu	projekt	k1gInSc2	projekt
Poplatkyzpet	Poplatkyzpet	k1gInSc1	Poplatkyzpet
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
každému	každý	k3xTgMnSc3	každý
klientovi	klient	k1gMnSc3	klient
zajistí	zajistit	k5eAaPmIp3nS	zajistit
navrácení	navrácení	k1gNnSc4	navrácení
poplatků	poplatek	k1gInPc2	poplatek
za	za	k7c4	za
vedení	vedení	k1gNnSc4	vedení
úvěrových	úvěrový	k2eAgInPc2d1	úvěrový
účtů	účet	k1gInPc2	účet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc1	filmografie
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc1	výběr
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
Redakce	redakce	k1gFnSc1	redakce
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Pavlína	Pavlína	k1gFnSc1	Pavlína
Danková	Dankový	k2eAgFnSc1d1	Danková
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pavlína	Pavlína	k1gFnSc1	Pavlína
Danková	Dankový	k2eAgFnSc1d1	Danková
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Pavlína	Pavlína	k1gFnSc1	Pavlína
Danková	Dankový	k2eAgFnSc1d1	Danková
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Pavlína	Pavlína	k1gFnSc1	Pavlína
Danková	Dankový	k2eAgFnSc1d1	Danková
na	na	k7c6	na
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
</s>
</p>
<p>
<s>
Pavlína	Pavlína	k1gFnSc1	Pavlína
Danková	Dankový	k2eAgFnSc1d1	Danková
na	na	k7c6	na
Super	super	k1gInSc6	super
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Pavlína	Pavlína	k1gFnSc1	Pavlína
Danková	Dankový	k2eAgFnSc1d1	Danková
na	na	k7c4	na
Showbiz	Showbiz	k1gInSc4	Showbiz
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Pavlína	Pavlína	k1gFnSc1	Pavlína
Danková	Dankový	k2eAgFnSc1d1	Danková
na	na	k7c4	na
Sweb	Sweb	k1gInSc4	Sweb
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
(	(	kIx(	(
<g/>
Projevy	projev	k1gInPc7	projev
na	na	k7c6	na
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pavlína	Pavlína	k1gFnSc1	Pavlína
Danková	Dankový	k2eAgFnSc1d1	Danková
na	na	k7c6	na
YouTube	YouTub	k1gInSc5	YouTub
http://www.youtube.com/channel/UCkcIP8kBhznyxOqBOSRxwBQ	[url]	k4	http://www.youtube.com/channel/UCkcIP8kBhznyxOqBOSRxwBQ
</s>
</p>
