<s>
ČEZ	ČEZ	kA	ČEZ
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgMnSc1d3	veliký
výrobce	výrobce	k1gMnSc1	výrobce
elektřiny	elektřina	k1gFnSc2	elektřina
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
mateřská	mateřský	k2eAgFnSc1d1	mateřská
společnost	společnost	k1gFnSc1	společnost
Skupiny	skupina	k1gFnSc2	skupina
ČEZ	ČEZ	kA	ČEZ
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
další	další	k2eAgFnPc4d1	další
desítky	desítka	k1gFnPc4	desítka
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Značka	značka	k1gFnSc1	značka
ČEZ	ČEZ	kA	ČEZ
představuje	představovat	k5eAaImIp3nS	představovat
výrobce	výrobce	k1gMnSc1	výrobce
či	či	k8xC	či
dodavatele	dodavatel	k1gMnSc2	dodavatel
energií	energie	k1gFnSc7	energie
celkem	celkem	k6eAd1	celkem
v	v	k7c6	v
7	[number]	k4	7
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
tak	tak	k6eAd1	tak
silnou	silný	k2eAgFnSc4d1	silná
pozici	pozice	k1gFnSc4	pozice
mezi	mezi	k7c7	mezi
energetikami	energetika	k1gFnPc7	energetika
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
ČEZ	ČEZ	kA	ČEZ
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
především	především	k9	především
těžbou	těžba	k1gFnSc7	těžba
uhlí	uhlí	k1gNnSc2	uhlí
a	a	k8xC	a
výrobou	výroba	k1gFnSc7	výroba
<g/>
,	,	kIx,	,
distribucí	distribuce	k1gFnSc7	distribuce
a	a	k8xC	a
prodejem	prodej	k1gInSc7	prodej
energií	energie	k1gFnPc2	energie
koncovým	koncový	k2eAgMnSc7d1	koncový
zákazníkům	zákazník	k1gMnPc3	zákazník
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
energií	energie	k1gFnPc2	energie
nabízí	nabízet	k5eAaImIp3nS	nabízet
ale	ale	k9	ale
i	i	k9	i
další	další	k2eAgFnPc4d1	další
služby	služba	k1gFnPc4	služba
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
mobilní	mobilní	k2eAgNnSc4d1	mobilní
volání	volání	k1gNnSc4	volání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
ČEZ	ČEZ	kA	ČEZ
první	první	k4xOgInSc4	první
nejziskovější	ziskový	k2eAgInSc4d3	nejziskovější
a	a	k8xC	a
druhou	druhý	k4xOgFnSc7	druhý
největší	veliký	k2eAgFnSc7d3	veliký
českou	český	k2eAgFnSc7d1	Česká
firmou	firma	k1gFnSc7	firma
podle	podle	k7c2	podle
tržeb	tržba	k1gFnPc2	tržba
a	a	k8xC	a
se	s	k7c7	s
zhruba	zhruba	k6eAd1	zhruba
26	[number]	k4	26
tisíci	tisíc	k4xCgInPc7	tisíc
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgMnSc7	druhý
největším	veliký	k2eAgMnSc7d3	veliký
zaměstnavatelem	zaměstnavatel	k1gMnSc7	zaměstnavatel
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
ČEZ	ČEZ	kA	ČEZ
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1992	[number]	k4	1992
přeměnou	přeměna	k1gFnSc7	přeměna
státního	státní	k2eAgInSc2d1	státní
podniku	podnik	k1gInSc2	podnik
České	český	k2eAgInPc1d1	český
energetické	energetický	k2eAgInPc1d1	energetický
závody	závod	k1gInPc1	závod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
1998	[number]	k4	1998
společnost	společnost	k1gFnSc1	společnost
ČEZ	ČEZ	kA	ČEZ
realizovala	realizovat	k5eAaBmAgFnS	realizovat
ekologický	ekologický	k2eAgInSc4d1	ekologický
a	a	k8xC	a
rozvojový	rozvojový	k2eAgInSc4d1	rozvojový
projekt	projekt	k1gInSc4	projekt
"	"	kIx"	"
<g/>
vyčištění	vyčištění	k1gNnSc4	vyčištění
<g/>
"	"	kIx"	"
uhelných	uhelný	k2eAgFnPc2d1	uhelná
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
odsíření	odsíření	k1gNnSc6	odsíření
a	a	k8xC	a
modernizaci	modernizace	k1gFnSc3	modernizace
šlo	jít	k5eAaImAgNnS	jít
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
letech	léto	k1gNnPc6	léto
46	[number]	k4	46
miliard	miliarda	k4xCgFnPc2	miliarda
Kč	Kč	kA	Kč
přímých	přímý	k2eAgFnPc2d1	přímá
investic	investice	k1gFnPc2	investice
a	a	k8xC	a
cca	cca	kA	cca
65	[number]	k4	65
miliard	miliarda	k4xCgFnPc2	miliarda
Kč	Kč	kA	Kč
souvisejících	související	k2eAgFnPc2d1	související
investic	investice	k1gFnPc2	investice
<g/>
.	.	kIx.	.
</s>
<s>
Povedlo	povést	k5eAaPmAgNnS	povést
se	se	k3xPyFc4	se
tak	tak	k9	tak
snížit	snížit	k5eAaPmF	snížit
emise	emise	k1gFnPc4	emise
SO2	SO2	k1gMnPc3	SO2
o	o	k7c4	o
92	[number]	k4	92
%	%	kIx~	%
<g/>
,	,	kIx,	,
pevných	pevný	k2eAgFnPc2d1	pevná
částic	částice	k1gFnPc2	částice
popílku	popílek	k1gInSc2	popílek
o	o	k7c4	o
95	[number]	k4	95
%	%	kIx~	%
<g/>
,	,	kIx,	,
emise	emise	k1gFnSc1	emise
oxidů	oxid	k1gInPc2	oxid
dusíku	dusík	k1gInSc2	dusík
o	o	k7c4	o
50	[number]	k4	50
%	%	kIx~	%
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgInSc2d1	uhelnatý
o	o	k7c4	o
77	[number]	k4	77
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
základ	základ	k1gInSc1	základ
dnešní	dnešní	k2eAgFnSc2d1	dnešní
podoby	podoba	k1gFnSc2	podoba
Skupiny	skupina	k1gFnSc2	skupina
ČEZ	ČEZ	kA	ČEZ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
energetická	energetický	k2eAgFnSc1d1	energetická
společnost	společnost	k1gFnSc1	společnost
ČEZ	ČEZ	kA	ČEZ
spojila	spojit	k5eAaPmAgFnS	spojit
s	s	k7c7	s
distribučními	distribuční	k2eAgFnPc7d1	distribuční
společnostmi	společnost	k1gFnPc7	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
ČEZ	ČEZ	kA	ČEZ
začleněny	začleněn	k2eAgFnPc1d1	začleněna
SD	SD	kA	SD
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
(	(	kIx(	(
<g/>
Severočeské	severočeský	k2eAgInPc1d1	severočeský
doly	dol	k1gInPc1	dol
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
probíhá	probíhat	k5eAaImIp3nS	probíhat
se	s	k7c7	s
svolením	svolení	k1gNnSc7	svolení
majoritního	majoritní	k2eAgMnSc2d1	majoritní
akcionáře	akcionář	k1gMnSc2	akcionář
(	(	kIx(	(
<g/>
stát	stát	k1gInSc1	stát
ČR	ČR	kA	ČR
<g/>
)	)	kIx)	)
akviziční	akviziční	k2eAgFnSc1d1	akviziční
politika	politika	k1gFnSc1	politika
Skupiny	skupina	k1gFnSc2	skupina
ČEZ	ČEZ	kA	ČEZ
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
zahraniční	zahraniční	k2eAgFnSc7d1	zahraniční
akvizicí	akvizice	k1gFnSc7	akvizice
byl	být	k5eAaImAgInS	být
nákup	nákup	k1gInSc1	nákup
67	[number]	k4	67
<g/>
%	%	kIx~	%
podílu	podíl	k1gInSc2	podíl
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
distribučních	distribuční	k2eAgFnPc6d1	distribuční
společnostech	společnost	k1gFnPc6	společnost
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
rozšíření	rozšíření	k1gNnSc1	rozšíření
působení	působení	k1gNnSc2	působení
do	do	k7c2	do
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
do	do	k7c2	do
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
zahraničních	zahraniční	k2eAgNnPc2d1	zahraniční
aktiv	aktivum	k1gNnPc2	aktivum
na	na	k7c6	na
celkových	celkový	k2eAgInPc6d1	celkový
aktivech	aktiv	k1gInPc6	aktiv
Skupiny	skupina	k1gFnSc2	skupina
ČEZ	ČEZ	kA	ČEZ
dnes	dnes	k6eAd1	dnes
představuje	představovat	k5eAaImIp3nS	představovat
19	[number]	k4	19
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
ČEZ	ČEZ	kA	ČEZ
se	se	k3xPyFc4	se
průběžně	průběžně	k6eAd1	průběžně
přizpůsobuje	přizpůsobovat	k5eAaImIp3nS	přizpůsobovat
úpravám	úprava	k1gFnPc3	úprava
energetické	energetický	k2eAgFnSc2d1	energetická
legislativy	legislativa	k1gFnSc2	legislativa
EU	EU	kA	EU
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
trhu	trh	k1gInSc2	trh
s	s	k7c7	s
elektřinou	elektřina	k1gFnSc7	elektřina
a	a	k8xC	a
environmentálních	environmentální	k2eAgFnPc2d1	environmentální
norem	norma	k1gFnPc2	norma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
projekt	projekt	k1gInSc1	projekt
integrace	integrace	k1gFnSc2	integrace
Skupiny	skupina	k1gFnSc2	skupina
ČEZ	ČEZ	kA	ČEZ
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
zaveden	zaveden	k2eAgInSc1d1	zaveden
jednotný	jednotný	k2eAgInSc1d1	jednotný
systém	systém	k1gInSc1	systém
řízení	řízení	k1gNnSc2	řízení
a	a	k8xC	a
začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
maximálně	maximálně	k6eAd1	maximálně
využívat	využívat	k5eAaImF	využívat
synergické	synergický	k2eAgInPc4d1	synergický
efekty	efekt	k1gInPc4	efekt
nového	nový	k2eAgNnSc2d1	nové
uspořádání	uspořádání	k1gNnSc2	uspořádání
akciové	akciový	k2eAgFnSc2d1	akciová
společnosti	společnost	k1gFnSc2	společnost
ČEZ	ČEZ	kA	ČEZ
i	i	k8xC	i
celé	celý	k2eAgFnSc2d1	celá
Skupiny	skupina	k1gFnSc2	skupina
ČEZ	ČEZ	kA	ČEZ
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
začala	začít	k5eAaPmAgFnS	začít
energetická	energetický	k2eAgFnSc1d1	energetická
skupina	skupina	k1gFnSc1	skupina
ČEZ	ČEZ	kA	ČEZ
nabízet	nabízet	k5eAaImF	nabízet
koncovým	koncový	k2eAgMnPc3d1	koncový
zákazníkům	zákazník	k1gMnPc3	zákazník
také	také	k6eAd1	také
dodávky	dodávka	k1gFnSc2	dodávka
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
potom	potom	k8xC	potom
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
své	svůj	k3xOyFgNnSc4	svůj
portfolio	portfolio	k1gNnSc4	portfolio
služeb	služba	k1gFnPc2	služba
také	také	k9	také
o	o	k7c4	o
mobilní	mobilní	k2eAgFnSc4d1	mobilní
telefonii	telefonie	k1gFnSc4	telefonie
<g/>
.	.	kIx.	.
</s>
<s>
Generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
ČEZ	ČEZ	kA	ČEZ
Daniel	Daniel	k1gMnSc1	Daniel
Beneš	Beneš	k1gMnSc1	Beneš
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2014	[number]	k4	2014
představil	představit	k5eAaPmAgInS	představit
aktualizovanou	aktualizovaný	k2eAgFnSc4d1	aktualizovaná
firemní	firemní	k2eAgFnSc4d1	firemní
strategii	strategie	k1gFnSc4	strategie
ČEZ	ČEZ	kA	ČEZ
jako	jako	k8xS	jako
na	na	k7c4	na
krizi	krize	k1gFnSc4	krize
a	a	k8xC	a
změnu	změna	k1gFnSc4	změna
podmínek	podmínka	k1gFnPc2	podmínka
v	v	k7c6	v
energetického	energetický	k2eAgInSc2d1	energetický
sektoru	sektor	k1gInSc2	sektor
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
strategie	strategie	k1gFnSc1	strategie
ČEZ	ČEZ	kA	ČEZ
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
třech	tři	k4xCgInPc6	tři
pilířích	pilíř	k1gInPc6	pilíř
<g/>
:	:	kIx,	:
Patřit	patřit	k5eAaImF	patřit
k	k	k7c3	k
nejlepším	dobrý	k2eAgInPc3d3	nejlepší
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
tradiční	tradiční	k2eAgFnSc2d1	tradiční
energetiky	energetika	k1gFnSc2	energetika
a	a	k8xC	a
aktivně	aktivně	k6eAd1	aktivně
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
výzvy	výzva	k1gFnPc4	výzva
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
Provozování	provozování	k1gNnSc2	provozování
klasických	klasický	k2eAgInPc2d1	klasický
energetických	energetický	k2eAgInPc2d1	energetický
zdrojů	zdroj	k1gInPc2	zdroj
-	-	kIx~	-
tedy	tedy	k8xC	tedy
uhelných	uhelný	k2eAgFnPc2d1	uhelná
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
,	,	kIx,	,
hydroelektráren	hydroelektrárna	k1gFnPc2	hydroelektrárna
<g/>
,	,	kIx,	,
elektráren	elektrárna	k1gFnPc2	elektrárna
na	na	k7c4	na
obnovitelné	obnovitelný	k2eAgInPc4d1	obnovitelný
zdroje	zdroj	k1gInPc4	zdroj
a	a	k8xC	a
jaderných	jaderný	k2eAgFnPc2d1	jaderná
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
ČEZ	ČEZ	kA	ČEZ
je	být	k5eAaImIp3nS	být
provozovat	provozovat	k5eAaImF	provozovat
elektrárny	elektrárna	k1gFnPc4	elektrárna
za	za	k7c4	za
podmínky	podmínka	k1gFnPc4	podmínka
maximálního	maximální	k2eAgNnSc2d1	maximální
využití	využití	k1gNnSc2	využití
s	s	k7c7	s
co	co	k9	co
nejnižšími	nízký	k2eAgInPc7d3	nejnižší
náklady	náklad	k1gInPc7	náklad
a	a	k8xC	a
proaktivně	proaktivně	k6eAd1	proaktivně
reagovat	reagovat	k5eAaBmF	reagovat
na	na	k7c4	na
budoucí	budoucí	k2eAgFnSc4d1	budoucí
podobu	podoba	k1gFnSc4	podoba
energetiky	energetika	k1gFnSc2	energetika
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
podílem	podíl	k1gInSc7	podíl
decentralizované	decentralizovaný	k2eAgFnSc2d1	decentralizovaná
a	a	k8xC	a
bezemisní	bezemisní	k2eAgFnSc2d1	bezemisní
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
stírajícím	stírající	k2eAgInPc3d1	stírající
se	se	k3xPyFc4	se
rozdílům	rozdíl	k1gInPc3	rozdíl
mezi	mezi	k7c7	mezi
výrobci	výrobce	k1gMnPc7	výrobce
a	a	k8xC	a
spotřebiteli	spotřebitel	k1gMnPc7	spotřebitel
<g/>
.	.	kIx.	.
</s>
<s>
Nabízet	nabízet	k5eAaImF	nabízet
zákazníkům	zákazník	k1gMnPc3	zákazník
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
produktů	produkt	k1gInPc2	produkt
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
zaměřených	zaměřený	k2eAgFnPc2d1	zaměřená
na	na	k7c4	na
jejich	jejich	k3xOp3gFnPc4	jejich
energetické	energetický	k2eAgFnPc4d1	energetická
potřeby	potřeba	k1gFnPc4	potřeba
Nabídnout	nabídnout	k5eAaPmF	nabídnout
zákazníkům	zákazník	k1gMnPc3	zákazník
partnerství	partnerství	k1gNnSc4	partnerství
<g/>
,	,	kIx,	,
know-how	knowow	k?	know-how
<g/>
,	,	kIx,	,
nástroje	nástroj	k1gInPc4	nástroj
i	i	k8xC	i
financování	financování	k1gNnPc4	financování
při	při	k7c6	při
řešení	řešení	k1gNnSc6	řešení
jejich	jejich	k3xOp3gFnPc2	jejich
energetických	energetický	k2eAgFnPc2d1	energetická
potřeb	potřeba	k1gFnPc2	potřeba
a	a	k8xC	a
také	také	k9	také
další	další	k2eAgInPc4d1	další
produkty	produkt	k1gInPc4	produkt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
synergii	synergie	k1gFnSc4	synergie
s	s	k7c7	s
prodejem	prodej	k1gInSc7	prodej
elektřiny	elektřina	k1gFnSc2	elektřina
a	a	k8xC	a
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Posílit	posílit	k5eAaPmF	posílit
a	a	k8xC	a
konsolidovat	konsolidovat	k5eAaBmF	konsolidovat
pozici	pozice	k1gFnSc4	pozice
v	v	k7c6	v
regionu	region	k1gInSc6	region
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
ČEZ	ČEZ	kA	ČEZ
se	se	k3xPyFc4	se
hodlá	hodlat	k5eAaImIp3nS	hodlat
aktivně	aktivně	k6eAd1	aktivně
udržet	udržet	k5eAaPmF	udržet
mezi	mezi	k7c7	mezi
10	[number]	k4	10
největšími	veliký	k2eAgFnPc7d3	veliký
energetikami	energetika	k1gFnPc7	energetika
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
díky	díky	k7c3	díky
využití	využití	k1gNnSc3	využití
významných	významný	k2eAgFnPc2d1	významná
synergií	synergie	k1gFnPc2	synergie
při	při	k7c6	při
provozování	provozování	k1gNnSc6	provozování
aktiv	aktivum	k1gNnPc2	aktivum
a	a	k8xC	a
při	při	k7c6	při
nabízení	nabízení	k1gNnSc6	nabízení
nových	nový	k2eAgInPc2d1	nový
produktů	produkt	k1gInPc2	produkt
a	a	k8xC	a
obsluze	obsluha	k1gFnSc3	obsluha
zákazníků	zákazník	k1gMnPc2	zákazník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svých	svůj	k3xOyFgNnPc2	svůj
zahraničních	zahraniční	k2eAgNnPc2d1	zahraniční
aktiv	aktivum	k1gNnPc2	aktivum
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
zaměřit	zaměřit	k5eAaPmF	zaměřit
na	na	k7c4	na
regiony	region	k1gInPc4	region
a	a	k8xC	a
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ČEZ	ČEZ	kA	ČEZ
i	i	k8xC	i
ČR	ČR	kA	ČR
blízké	blízký	k2eAgFnSc2d1	blízká
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
energetických	energetický	k2eAgInPc2d1	energetický
trhů	trh	k1gInPc2	trh
<g/>
,	,	kIx,	,
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
klíčová	klíčový	k2eAgFnSc1d1	klíčová
přitom	přitom	k6eAd1	přitom
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
jednoznačná	jednoznačný	k2eAgFnSc1d1	jednoznačná
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
výhodnost	výhodnost	k1gFnSc1	výhodnost
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
ČEZ	ČEZ	kA	ČEZ
plánuje	plánovat	k5eAaImIp3nS	plánovat
postupně	postupně	k6eAd1	postupně
uzavírat	uzavírat	k5eAaPmF	uzavírat
uhelné	uhelný	k2eAgFnPc4d1	uhelná
elektrárny	elektrárna	k1gFnPc4	elektrárna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
provozuje	provozovat	k5eAaImIp3nS	provozovat
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
a	a	k8xC	a
ponechat	ponechat	k5eAaPmF	ponechat
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
jen	jen	k6eAd1	jen
ty	ten	k3xDgFnPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
prošly	projít	k5eAaPmAgFnP	projít
komplexní	komplexní	k2eAgInPc1d1	komplexní
modernizací	modernizace	k1gFnSc7	modernizace
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
jsou	být	k5eAaImIp3nP	být
tenčící	tenčící	k2eAgFnPc1d1	tenčící
se	se	k3xPyFc4	se
zásoby	zásoba	k1gFnSc2	zásoba
hnědého	hnědý	k2eAgNnSc2d1	hnědé
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
končící	končící	k2eAgFnSc1d1	končící
životnost	životnost	k1gFnSc1	životnost
starších	starý	k2eAgFnPc2d2	starší
uhelných	uhelný	k2eAgFnPc2d1	uhelná
elektráren	elektrárna	k1gFnPc2	elektrárna
a	a	k8xC	a
cíl	cíl	k1gInSc4	cíl
dál	daleko	k6eAd2	daleko
snižovat	snižovat	k5eAaImF	snižovat
emise	emise	k1gFnPc4	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
energetickém	energetický	k2eAgInSc6d1	energetický
summitu	summit	k1gInSc6	summit
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
(	(	kIx(	(
<g/>
Business	business	k1gInSc1	business
&	&	k?	&
Climate	Climat	k1gInSc5	Climat
Summit	summit	k1gInSc1	summit
<g/>
,	,	kIx,	,
květen	květen	k1gInSc1	květen
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
Daniel	Daniel	k1gMnSc1	Daniel
Beneš	Beneš	k1gMnSc1	Beneš
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ČEZ	ČEZ	kA	ČEZ
plánuje	plánovat	k5eAaImIp3nS	plánovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2050	[number]	k4	2050
zredukovat	zredukovat	k5eAaPmF	zredukovat
své	svůj	k3xOyFgFnPc4	svůj
emise	emise	k1gFnPc4	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
na	na	k7c4	na
nulu	nula	k1gFnSc4	nula
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Uhlí	uhlí	k1gNnSc1	uhlí
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
elektřiny	elektřina	k1gFnSc2	elektřina
i	i	k9	i
v	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
,	,	kIx,	,
z	z	k7c2	z
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
hlediska	hledisko	k1gNnSc2	hledisko
by	by	kYmCp3nS	by
ale	ale	k9	ale
jeho	jeho	k3xOp3gNnSc1	jeho
využívání	využívání	k1gNnSc1	využívání
mělo	mít	k5eAaImAgNnS	mít
skončit	skončit	k5eAaPmF	skončit
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Beneš	Beneš	k1gMnSc1	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
financí	finance	k1gFnPc2	finance
ČR	ČR	kA	ČR
-	-	kIx~	-
69,78	[number]	k4	69,78
%	%	kIx~	%
ČEZ	ČEZ	kA	ČEZ
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
-	-	kIx~	-
0,72	[number]	k4	0,72
%	%	kIx~	%
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
právnické	právnický	k2eAgFnPc1d1	právnická
osoby	osoba	k1gFnPc1	osoba
-	-	kIx~	-
22,81	[number]	k4	22,81
%	%	kIx~	%
Fyzické	fyzický	k2eAgFnSc2d1	fyzická
osoby	osoba	k1gFnSc2	osoba
-	-	kIx~	-
6,69	[number]	k4	6,69
%	%	kIx~	%
Celková	celkový	k2eAgFnSc1d1	celková
výše	výše	k1gFnSc1	výše
základního	základní	k2eAgInSc2d1	základní
kapitálu	kapitál	k1gInSc2	kapitál
ČEZ	ČEZ	kA	ČEZ
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
zapsaná	zapsaný	k2eAgFnSc1d1	zapsaná
v	v	k7c6	v
obchodním	obchodní	k2eAgInSc6d1	obchodní
rejstříku	rejstřík	k1gInSc6	rejstřík
činila	činit	k5eAaImAgFnS	činit
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
celkem	celkem	k6eAd1	celkem
53	[number]	k4	53
798	[number]	k4	798
975	[number]	k4	975
900	[number]	k4	900
Kč	Kč	kA	Kč
rozděleného	rozdělený	k2eAgNnSc2d1	rozdělené
do	do	k7c2	do
537	[number]	k4	537
989	[number]	k4	989
759	[number]	k4	759
ks	ks	kA	ks
akcií	akcie	k1gFnPc2	akcie
ČEZ	ČEZ	kA	ČEZ
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
ISIN	ISIN	kA	ISIN
CZ0005112300	CZ0005112300	k1gFnSc4	CZ0005112300
o	o	k7c6	o
nominální	nominální	k2eAgFnSc6d1	nominální
hodnotě	hodnota	k1gFnSc6	hodnota
100	[number]	k4	100
Kč	Kč	kA	Kč
zaknihovaných	zaknihovaný	k2eAgInPc2d1	zaknihovaný
v	v	k7c6	v
Centrálním	centrální	k2eAgInSc6d1	centrální
depozitáři	depozitář	k1gInSc6	depozitář
cenných	cenný	k2eAgInPc2d1	cenný
papírů	papír	k1gInPc2	papír
<g/>
.	.	kIx.	.
</s>
<s>
Akcie	akcie	k1gFnSc1	akcie
ČEZ	ČEZ	kA	ČEZ
se	se	k3xPyFc4	se
od	od	k7c2	od
15.6	[number]	k4	15.6
<g/>
.1993	.1993	k4	.1993
obchodují	obchodovat	k5eAaImIp3nP	obchodovat
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
burze	burza	k1gFnSc6	burza
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
pilířů	pilíř	k1gInPc2	pilíř
Indexu	index	k1gInSc2	index
PX	PX	kA	PX
<g/>
.	.	kIx.	.
</s>
<s>
ČEZ	ČEZ	kA	ČEZ
je	být	k5eAaImIp3nS	být
nejvíce	nejvíce	k6eAd1	nejvíce
obchodovanou	obchodovaný	k2eAgFnSc7d1	obchodovaná
akcií	akcie	k1gFnSc7	akcie
na	na	k7c6	na
BCPP	BCPP	kA	BCPP
<g/>
,	,	kIx,	,
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
celkem	celkem	k6eAd1	celkem
zobchodováno	zobchodovat	k5eAaImNgNnS	zobchodovat
4	[number]	k4	4
766	[number]	k4	766
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
ks	ks	kA	ks
v	v	k7c6	v
objemu	objem	k1gInSc6	objem
2	[number]	k4	2
345	[number]	k4	345
mld.	mld.	k?	mld.
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Objem	objem	k1gInSc1	objem
obchodů	obchod	k1gInPc2	obchod
akcie	akcie	k1gFnSc2	akcie
ČEZ	ČEZ	kA	ČEZ
tvoří	tvořit	k5eAaImIp3nS	tvořit
zhruba	zhruba	k6eAd1	zhruba
15	[number]	k4	15
%	%	kIx~	%
objemů	objem	k1gInPc2	objem
<g/>
.	.	kIx.	.
</s>
<s>
Akcie	akcie	k1gFnPc1	akcie
společnosti	společnost	k1gFnSc2	společnost
jsou	být	k5eAaImIp3nP	být
přijaty	přijat	k2eAgFnPc1d1	přijata
k	k	k7c3	k
obchodování	obchodování	k1gNnSc3	obchodování
také	také	k6eAd1	také
na	na	k7c6	na
Burze	burza	k1gFnSc6	burza
cenných	cenný	k2eAgInPc2d1	cenný
papírů	papír	k1gInPc2	papír
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
v	v	k7c6	v
Polské	polský	k2eAgFnSc6d1	polská
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
neomezeně	omezeně	k6eNd1	omezeně
převoditelné	převoditelný	k2eAgFnPc1d1	převoditelná
<g/>
.	.	kIx.	.
</s>
<s>
ČEZ	ČEZ	kA	ČEZ
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
naplňuje	naplňovat	k5eAaImIp3nS	naplňovat
energetickou	energetický	k2eAgFnSc4d1	energetická
koncepci	koncepce	k1gFnSc4	koncepce
stanovenou	stanovený	k2eAgFnSc7d1	stanovená
vládou	vláda	k1gFnSc7	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
energetickými	energetický	k2eAgFnPc7d1	energetická
společnostmi	společnost	k1gFnPc7	společnost
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
provádí	provádět	k5eAaImIp3nS	provádět
Energetický	energetický	k2eAgInSc1d1	energetický
regulační	regulační	k2eAgInSc1d1	regulační
úřad	úřad	k1gInSc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
provozu	provoz	k1gInSc2	provoz
jaderných	jaderný	k2eAgFnPc2d1	jaderná
elektráren	elektrárna	k1gFnPc2	elektrárna
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
Státní	státní	k2eAgInSc1d1	státní
úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
jadernou	jaderný	k2eAgFnSc4d1	jaderná
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
ČEZ	ČEZ	kA	ČEZ
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
Ústav	ústav	k1gInSc1	ústav
jaderného	jaderný	k2eAgInSc2d1	jaderný
výzkumu	výzkum	k1gInSc2	výzkum
Řež	řež	k1gFnSc1	řež
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Husinec-Řež	Husinec-Řež	k1gFnSc4	Husinec-Řež
a	a	k8xC	a
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
provádí	provádět	k5eAaImIp3nS	provádět
aplikovaný	aplikovaný	k2eAgInSc4d1	aplikovaný
výzkum	výzkum	k1gInSc4	výzkum
a	a	k8xC	a
inženýrskou	inženýrský	k2eAgFnSc4d1	inženýrská
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c6	o
převedení	převedení	k1gNnSc6	převedení
výzkumných	výzkumný	k2eAgInPc2d1	výzkumný
a	a	k8xC	a
vývojových	vývojový	k2eAgInPc2d1	vývojový
projektů	projekt	k1gInPc2	projekt
do	do	k7c2	do
praxe	praxe	k1gFnSc2	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Elektrárny	elektrárna	k1gFnPc1	elektrárna
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
provozuje	provozovat	k5eAaImIp3nS	provozovat
ČEZ	ČEZ	kA	ČEZ
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Dukovany	Dukovany	k1gInPc1	Dukovany
Temelín	Temelín	k1gInSc1	Temelín
Dětmarovice	Dětmarovice	k1gFnPc1	Dětmarovice
Hodonín	Hodonín	k1gInSc4	Hodonín
Ledvice	Ledvice	k1gFnSc2	Ledvice
Mělník	Mělník	k1gInSc1	Mělník
Počerady	Počerada	k1gFnSc2	Počerada
Poříčí	Poříčí	k1gNnSc2	Poříčí
Prunéřov	Prunéřov	k1gInSc1	Prunéřov
I	i	k8xC	i
a	a	k9	a
II	II	kA	II
Tisová	tisový	k2eAgFnSc1d1	Tisová
Tušimice	Tušimika	k1gFnSc6	Tušimika
II	II	kA	II
Přečerpávací	přečerpávací	k2eAgFnSc1d1	přečerpávací
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Dalešice	Dalešice	k1gFnSc2	Dalešice
mezi	mezi	k7c7	mezi
vodní	vodní	k2eAgFnSc7d1	vodní
nádrží	nádrž	k1gFnSc7	nádrž
Dalešice	Dalešice	k1gFnSc2	Dalešice
a	a	k8xC	a
vodní	vodní	k2eAgFnSc7d1	vodní
nádrží	nádrž	k1gFnSc7	nádrž
Mohelno	Mohelna	k1gFnSc5	Mohelna
Přečerpávací	přečerpávací	k2eAgFnSc1d1	přečerpávací
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Dlouhé	Dlouhé	k2eAgFnSc2d1	Dlouhé
Stráně	stráň	k1gFnSc2	stráň
přečerpávací	přečerpávací	k2eAgFnSc1d1	přečerpávací
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Dlouhé	Dlouhé	k2eAgFnSc2d1	Dlouhé
Stráně	stráň	k1gFnSc2	stráň
I	i	k9	i
malá	malý	k2eAgFnSc1d1	malá
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Dlouhé	Dlouhé	k2eAgFnSc2d1	Dlouhé
Stráně	stráň	k1gFnSc2	stráň
II	II	kA	II
Malá	malý	k2eAgFnSc1d1	malá
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Hněvkovice	Hněvkovice	k1gFnSc1	Hněvkovice
u	u	k7c2	u
vodní	vodní	k2eAgFnSc2d1	vodní
<g />
.	.	kIx.	.
</s>
<s>
nádrže	nádrž	k1gFnPc1	nádrž
Hněvkovice	Hněvkovice	k1gFnSc2	Hněvkovice
Vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Kamýk	Kamýk	k1gInSc4	Kamýk
u	u	k7c2	u
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
Kamýk	Kamýk	k1gInSc4	Kamýk
Malá	malý	k2eAgFnSc1d1	malá
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Kořensko	Kořensko	k1gNnSc1	Kořensko
u	u	k7c2	u
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
Kořensko	Kořensko	k1gNnSc4	Kořensko
malá	malý	k2eAgFnSc1d1	malá
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Kořensko	Kořensko	k1gNnSc1	Kořensko
I	i	k8xC	i
malá	malý	k2eAgFnSc1d1	malá
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Kořensko	Kořensko	k1gNnSc1	Kořensko
II	II	kA	II
Vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Lipno	Lipno	k1gNnSc1	Lipno
u	u	k7c2	u
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
Lipno	Lipno	k1gNnSc4	Lipno
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Lipno	Lipno	k1gNnSc1	Lipno
I	i	k8xC	i
malá	malý	k2eAgFnSc1d1	malá
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Lipno	Lipno	k1gNnSc1	Lipno
II	II	kA	II
Vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Mohelno	Mohelna	k1gFnSc5	Mohelna
u	u	k7c2	u
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
Mohelno	Mohelna	k1gFnSc5	Mohelna
<g />
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Orlík	Orlík	k1gInSc4	Orlík
u	u	k7c2	u
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
Orlík	Orlík	k1gInSc4	Orlík
Vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Slapy	slap	k1gInPc1	slap
u	u	k7c2	u
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
Slapy	slap	k1gInPc4	slap
Vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Štěchovice	Štěchovice	k1gFnPc1	Štěchovice
u	u	k7c2	u
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
Štěchovice	Štěchovice	k1gFnPc4	Štěchovice
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Štěchovice	Štěchovice	k1gFnPc1	Štěchovice
I	i	k8xC	i
přečerpávací	přečerpávací	k2eAgFnSc1d1	přečerpávací
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Štěchovice	Štěchovice	k1gFnPc1	Štěchovice
II	II	kA	II
Vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Vrané	vraný	k2eAgFnSc2d1	Vraná
u	u	k7c2	u
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
Vrané	vraný	k2eAgFnSc2d1	Vraná
Malá	malý	k2eAgFnSc1d1	malá
vodní	vodní	k2eAgFnSc1d1	vodní
elektrárna	elektrárna	k1gFnSc1	elektrárna
Želina	Želina	k1gFnSc1	Želina
u	u	k7c2	u
Kadaně	Kadaň	k1gFnSc2	Kadaň
<g/>
,	,	kIx,	,
poblíž	poblíž	k7c2	poblíž
elektrárny	elektrárna	k1gFnSc2	elektrárna
Tušimice	Tušimika	k1gFnSc3	Tušimika
Větrné	větrný	k2eAgFnSc2d1	větrná
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g />
.	.	kIx.	.
</s>
<s>
Věžnice	Věžnice	k1gFnSc1	Věžnice
Větrné	větrný	k2eAgFnSc2d1	větrná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Janov	Janov	k1gInSc4	Janov
Fotovoltaická	fotovoltaický	k2eAgFnSc1d1	fotovoltaická
elektrárna	elektrárna	k1gFnSc1	elektrárna
Bežerovice	Bežerovice	k1gFnSc2	Bežerovice
Fotovoltaická	fotovoltaický	k2eAgFnSc1d1	fotovoltaická
elektrárna	elektrárna	k1gFnSc1	elektrárna
Buštěhrad	Buštěhrad	k1gInSc1	Buštěhrad
Fotovoltaická	fotovoltaický	k2eAgFnSc1d1	fotovoltaická
elektrárna	elektrárna	k1gFnSc1	elektrárna
Čekanice	Čekanice	k1gFnSc2	Čekanice
Fotovoltaická	fotovoltaický	k2eAgFnSc1d1	fotovoltaická
elektrárna	elektrárna	k1gFnSc1	elektrárna
Dukovany	Dukovany	k1gInPc4	Dukovany
Fotovoltaická	fotovoltaický	k2eAgFnSc1d1	fotovoltaická
elektrárna	elektrárna	k1gFnSc1	elektrárna
Hrušovany	Hrušovany	k1gInPc4	Hrušovany
Fotovoltaická	fotovoltaický	k2eAgFnSc1d1	fotovoltaická
elektrárna	elektrárna	k1gFnSc1	elektrárna
Chýnov	Chýnov	k1gInSc1	Chýnov
Fotovoltaická	fotovoltaický	k2eAgFnSc1d1	fotovoltaická
elektrárna	elektrárna	k1gFnSc1	elektrárna
Přelouč	Přelouč	k1gFnSc1	Přelouč
Fotovoltaická	fotovoltaický	k2eAgFnSc1d1	fotovoltaická
elektrárna	elektrárna	k1gFnSc1	elektrárna
Žabčice	Žabčice	k1gFnSc2	Žabčice
Fotovoltaická	fotovoltaický	k2eAgFnSc1d1	fotovoltaická
elektrárna	elektrárna	k1gFnSc1	elektrárna
Mimoň	Mimoň	k1gFnSc1	Mimoň
Fotovoltaická	fotovoltaický	k2eAgFnSc1d1	fotovoltaická
elektrárna	elektrárna	k1gFnSc1	elektrárna
Pánov	Pánov	k1gInSc1	Pánov
Fotovoltaická	fotovoltaický	k2eAgFnSc1d1	fotovoltaická
elektrárna	elektrárna	k1gFnSc1	elektrárna
Ralsko	Ralsko	k1gNnSc1	Ralsko
Fotovoltaická	fotovoltaický	k2eAgFnSc1d1	fotovoltaická
elektrárna	elektrárna	k1gFnSc1	elektrárna
Ševětín	Ševětína	k1gFnPc2	Ševětína
Fotovoltaická	fotovoltaický	k2eAgFnSc1d1	fotovoltaická
elektrárna	elektrárna	k1gFnSc1	elektrárna
Vranovská	vranovský	k2eAgFnSc1d1	Vranovská
Ves	ves	k1gFnSc1	ves
Zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
expanzi	expanze	k1gFnSc4	expanze
zahájila	zahájit	k5eAaPmAgFnS	zahájit
Skupina	skupina	k1gFnSc1	skupina
ČEZ	ČEZ	kA	ČEZ
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
Skupina	skupina	k1gFnSc1	skupina
ČEZ	ČEZ	kA	ČEZ
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
zejména	zejména	k9	zejména
na	na	k7c4	na
obnovitelné	obnovitelný	k2eAgInPc4d1	obnovitelný
zdroje	zdroj	k1gInPc4	zdroj
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
větrných	větrný	k2eAgFnPc2d1	větrná
a	a	k8xC	a
vodních	vodní	k2eAgFnPc2d1	vodní
elektráren	elektrárna	k1gFnPc2	elektrárna
<g/>
..	..	k?	..
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2005	[number]	k4	2005
Skupina	skupina	k1gFnSc1	skupina
ČEZ	ČEZ	kA	ČEZ
po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
v	v	k7c6	v
privatizačním	privatizační	k2eAgInSc6d1	privatizační
tendru	tendr	k1gInSc6	tendr
převzala	převzít	k5eAaPmAgFnS	převzít
většinové	většinový	k2eAgInPc4d1	většinový
podíly	podíl	k1gInPc4	podíl
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
bulharských	bulharský	k2eAgFnPc6d1	bulharská
distribučních	distribuční	k2eAgFnPc6d1	distribuční
společnostech	společnost	k1gFnPc6	společnost
(	(	kIx(	(
<g/>
Elektrorazpredelenie	Elektrorazpredelenie	k1gFnSc1	Elektrorazpredelenie
Sofia	Sofia	k1gFnSc1	Sofia
Oblast	oblast	k1gFnSc1	oblast
AD	ad	k7c4	ad
<g/>
,	,	kIx,	,
Elektrorazpredelenie	Elektrorazpredelenie	k1gFnPc1	Elektrorazpredelenie
Pleven	Plevno	k1gNnPc2	Plevno
AD	ad	k7c4	ad
a	a	k8xC	a
Elektrorazpredelenie	Elektrorazpredelenie	k1gFnPc4	Elektrorazpredelenie
Stolichno	Stolichno	k6eAd1	Stolichno
AD	ad	k7c4	ad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
koupil	koupit	k5eAaPmAgInS	koupit
ČEZ	ČEZ	kA	ČEZ
od	od	k7c2	od
rumunské	rumunský	k2eAgFnSc2d1	rumunská
vlády	vláda	k1gFnSc2	vláda
většinový	většinový	k2eAgInSc1d1	většinový
podíl	podíl	k1gInSc1	podíl
v	v	k7c6	v
distribuční	distribuční	k2eAgFnSc6d1	distribuční
společnosti	společnost	k1gFnSc6	společnost
Electrica	Electricum	k1gNnSc2	Electricum
Oltenia	Oltenium	k1gNnSc2	Oltenium
S.	S.	kA	S.
<g/>
A.	A.	kA	A.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
získal	získat	k5eAaPmAgMnS	získat
ČEZ	ČEZ	kA	ČEZ
většinové	většinový	k2eAgInPc4d1	většinový
podíly	podíl	k1gInPc4	podíl
v	v	k7c6	v
černouhelných	černouhelný	k2eAgFnPc6d1	černouhelná
elektrárnách	elektrárna	k1gFnPc6	elektrárna
Elektrownia	Elektrownium	k1gNnSc2	Elektrownium
Skawina	Skawino	k1gNnSc2	Skawino
a	a	k8xC	a
Elektrocieplownia	Elektrocieplownium	k1gNnSc2	Elektrocieplownium
Elcho	Elc	k1gMnSc2	Elc
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
zdroje	zdroj	k1gInPc1	zdroj
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
českých	český	k2eAgFnPc2d1	Česká
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
podepsal	podepsat	k5eAaPmAgMnS	podepsat
ČEZ	ČEZ	kA	ČEZ
dohodu	dohoda	k1gFnSc4	dohoda
s	s	k7c7	s
energetickou	energetický	k2eAgFnSc7d1	energetická
společností	společnost	k1gFnSc7	společnost
Republiky	republika	k1gFnSc2	republika
srbské	srbský	k2eAgFnSc2d1	Srbská
(	(	kIx(	(
<g/>
ERS	ERS	kA	ERS
<g/>
)	)	kIx)	)
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
společného	společný	k2eAgInSc2d1	společný
podniku	podnik	k1gInSc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgMnS	být
ČEZ	ČEZ	kA	ČEZ
vybrán	vybrán	k2eAgMnSc1d1	vybrán
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
partnerů	partner	k1gMnPc2	partner
v	v	k7c6	v
projektu	projekt	k1gInSc6	projekt
výstavby	výstavba	k1gFnSc2	výstavba
a	a	k8xC	a
provozu	provoz	k1gInSc2	provoz
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
bloku	blok	k1gInSc2	blok
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Černavoda	Černavoda	k1gFnSc1	Černavoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
ČEZ	ČEZ	kA	ČEZ
a	a	k8xC	a
energetické	energetický	k2eAgNnSc1d1	energetické
uskupení	uskupení	k1gNnSc1	uskupení
RAO	RAO	kA	RAO
JES	JES	kA	JES
podepsali	podepsat	k5eAaPmAgMnP	podepsat
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
společného	společný	k2eAgInSc2d1	společný
podniku	podnik	k1gInSc2	podnik
pro	pro	k7c4	pro
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
elektrárny	elektrárna	k1gFnSc2	elektrárna
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Ščekino	Ščekino	k1gNnSc1	Ščekino
<g/>
,	,	kIx,	,
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
ČEZ	ČEZ	kA	ČEZ
podepsal	podepsat	k5eAaPmAgMnS	podepsat
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c4	o
založení	založení	k1gNnSc4	založení
společného	společný	k2eAgInSc2d1	společný
podniku	podnik	k1gInSc2	podnik
s	s	k7c7	s
maďarskou	maďarský	k2eAgFnSc7d1	maďarská
společností	společnost	k1gFnSc7	společnost
MOL	mol	k1gMnSc1	mol
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
podnik	podnik	k1gInSc1	podnik
se	se	k3xPyFc4	se
zaměří	zaměřit	k5eAaPmIp3nS	zaměřit
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
elektřiny	elektřina	k1gFnSc2	elektřina
v	v	k7c6	v
plynových	plynový	k2eAgFnPc6d1	plynová
elektrárnách	elektrárna	k1gFnPc6	elektrárna
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
,	,	kIx,	,
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
Slovinsku	Slovinsko	k1gNnSc6	Slovinsko
a	a	k8xC	a
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
Severočeské	severočeský	k2eAgInPc1d1	severočeský
doly	dol	k1gInPc1	dol
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
patří	patřit	k5eAaImIp3nP	patřit
energetické	energetický	k2eAgFnSc3d1	energetická
Skupině	skupina	k1gFnSc3	skupina
ČEZ	ČEZ	kA	ČEZ
a	a	k8xC	a
finanční	finanční	k2eAgFnSc1d1	finanční
skupina	skupina	k1gFnSc1	skupina
J	J	kA	J
<g/>
&	&	k?	&
<g/>
T	T	kA	T
koupily	koupit	k5eAaPmAgFnP	koupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
za	za	k7c4	za
404	[number]	k4	404
milionů	milion	k4xCgInPc2	milion
eur	euro	k1gNnPc2	euro
saskou	saský	k2eAgFnSc4d1	saská
hnědouhelnou	hnědouhelný	k2eAgFnSc4d1	hnědouhelná
těžební	těžební	k2eAgFnSc4d1	těžební
firmu	firma	k1gFnSc4	firma
Mibrag	Mibraga	k1gFnPc2	Mibraga
<g/>
.	.	kIx.	.
</s>
<s>
ČEZ	ČEZ	kA	ČEZ
investoval	investovat	k5eAaBmAgInS	investovat
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jednu	jeden	k4xCgFnSc4	jeden
miliardu	miliarda	k4xCgFnSc4	miliarda
eur	euro	k1gNnPc2	euro
do	do	k7c2	do
výstavby	výstavba	k1gFnSc2	výstavba
dvou	dva	k4xCgFnPc2	dva
větrných	větrný	k2eAgFnPc2d1	větrná
farem	farma	k1gFnPc2	farma
o	o	k7c6	o
výkonu	výkon	k1gInSc6	výkon
600	[number]	k4	600
MW	MW	kA	MW
<g/>
.	.	kIx.	.
</s>
<s>
Půjde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
největší	veliký	k2eAgInSc4d3	veliký
projekt	projekt	k1gInSc4	projekt
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
ČEZ	ČEZ	kA	ČEZ
dokončil	dokončit	k5eAaPmAgMnS	dokončit
nákup	nákup	k1gInSc4	nákup
poloviny	polovina	k1gFnSc2	polovina
majoritního	majoritní	k2eAgInSc2d1	majoritní
podílu	podíl	k1gInSc2	podíl
v	v	k7c6	v
turecké	turecký	k2eAgFnSc6d1	turecká
firmě	firma	k1gFnSc6	firma
Akenerji	Akenerje	k1gFnSc4	Akenerje
<g/>
.	.	kIx.	.
</s>
<s>
Partneři	partner	k1gMnPc1	partner
Akkök	Akkök	k1gMnSc1	Akkök
Group	Group	k1gMnSc1	Group
a	a	k8xC	a
ČEZ	ČEZ	kA	ČEZ
tak	tak	k6eAd1	tak
společně	společně	k6eAd1	společně
ve	v	k7c6	v
firmě	firma	k1gFnSc6	firma
drží	držet	k5eAaImIp3nS	držet
téměř	téměř	k6eAd1	téměř
75	[number]	k4	75
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc1	podíl
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
firmy	firma	k1gFnPc1	firma
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
podnikají	podnikat	k5eAaImIp3nP	podnikat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
společného	společný	k2eAgInSc2d1	společný
podniku	podnik	k1gInSc2	podnik
Akcez	Akceza	k1gFnPc2	Akceza
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
května	květen	k1gInSc2	květen
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
ČEZ	ČEZ	kA	ČEZ
stal	stát	k5eAaPmAgInS	stát
oficiálním	oficiální	k2eAgMnSc7d1	oficiální
partnerem	partner	k1gMnSc7	partner
výstavby	výstavba	k1gFnSc2	výstavba
nového	nový	k2eAgInSc2d1	nový
bloku	blok	k1gInSc2	blok
slovenské	slovenský	k2eAgFnSc2d1	slovenská
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
Jaslovské	Jaslovský	k2eAgFnPc4d1	Jaslovský
Bohunice	Bohunice	k1gFnPc4	Bohunice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
společném	společný	k2eAgInSc6d1	společný
podniku	podnik	k1gInSc6	podnik
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
JVS	JVS	kA	JVS
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
ČEZ	ČEZ	kA	ČEZ
49	[number]	k4	49
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
ČEZ	ČEZ	kA	ČEZ
majitelem	majitel	k1gMnSc7	majitel
76	[number]	k4	76
<g/>
%	%	kIx~	%
podílu	podíl	k1gInSc2	podíl
v	v	k7c6	v
jediné	jediný	k2eAgFnSc6d1	jediná
albánské	albánský	k2eAgFnSc6d1	albánská
distribuční	distribuční	k2eAgFnSc6d1	distribuční
společnosti	společnost	k1gFnSc6	společnost
OSSH	OSSH	kA	OSSH
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
přejmenoval	přejmenovat	k5eAaPmAgInS	přejmenovat
na	na	k7c6	na
CEZ	CEZ	kA	CEZ
Shpërndarje	Shpërndarj	k1gFnSc2	Shpërndarj
a	a	k8xC	a
začlenil	začlenit	k5eAaPmAgMnS	začlenit
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
nadnárodní	nadnárodní	k2eAgFnSc2d1	nadnárodní
skupiny	skupina	k1gFnSc2	skupina
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
ČEZ	ČEZ	kA	ČEZ
koupil	koupit	k5eAaPmAgInS	koupit
67	[number]	k4	67
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc4	podíl
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Eco-Wind	Eco-Wind	k1gInSc1	Eco-Wind
Construction	Construction	k1gInSc4	Construction
S.	S.	kA	S.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
developer	developer	k1gMnSc1	developer
větrných	větrný	k2eAgInPc2d1	větrný
parků	park	k1gInPc2	park
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
akvizicí	akvizice	k1gFnSc7	akvizice
Skupiny	skupina	k1gFnSc2	skupina
ČEZ	ČEZ	kA	ČEZ
byl	být	k5eAaImAgMnS	být
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
německého	německý	k2eAgMnSc2d1	německý
výrobce	výrobce	k1gMnSc2	výrobce
baterií	baterie	k1gFnPc2	baterie
Sonnenbatterie	Sonnenbatterie	k1gFnSc2	Sonnenbatterie
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
ČEZ	ČEZ	kA	ČEZ
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
německé	německý	k2eAgFnSc2d1	německá
společnosti	společnost	k1gFnSc2	společnost
investovala	investovat	k5eAaBmAgFnS	investovat
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2015	[number]	k4	2015
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
dceřiné	dceřiný	k2eAgFnSc2d1	dceřiná
společnosti	společnost	k1gFnSc2	společnost
Inven	Inven	k2eAgMnSc1d1	Inven
Capital	Capital	k1gMnSc1	Capital
a	a	k8xC	a
získala	získat	k5eAaPmAgFnS	získat
kromě	kromě	k7c2	kromě
vlastnického	vlastnický	k2eAgInSc2d1	vlastnický
podílu	podíl	k1gInSc2	podíl
také	také	k9	také
možnost	možnost	k1gFnSc4	možnost
podílet	podílet	k5eAaImF	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
strategických	strategický	k2eAgNnPc6d1	strategické
rozhodnutích	rozhodnutí	k1gNnPc6	rozhodnutí
firmy	firma	k1gFnSc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Tržby	tržba	k1gFnPc1	tržba
společnosti	společnost	k1gFnSc2	společnost
Sonnenbatterie	Sonnenbatterie	k1gFnSc2	Sonnenbatterie
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
více	hodně	k6eAd2	hodně
než	než	k8xS	než
zdvojnásobují	zdvojnásobovat	k5eAaImIp3nP	zdvojnásobovat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
přímou	přímý	k2eAgFnSc7d1	přímá
konkurencí	konkurence	k1gFnSc7	konkurence
bateriových	bateriový	k2eAgInPc2d1	bateriový
systémů	systém	k1gInPc2	systém
společnosti	společnost	k1gFnSc2	společnost
Tesla	Tesla	k1gFnSc1	Tesla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dárcovství	dárcovství	k1gNnSc2	dárcovství
hraje	hrát	k5eAaImIp3nS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
Nadace	nadace	k1gFnSc2	nadace
ČEZ	ČEZ	kA	ČEZ
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
podpořila	podpořit	k5eAaPmAgFnS	podpořit
přes	přes	k7c4	přes
4	[number]	k4	4
900	[number]	k4	900
veřejně	veřejně	k6eAd1	veřejně
prospěšných	prospěšný	k2eAgInPc2d1	prospěšný
projektů	projekt	k1gInPc2	projekt
částkou	částka	k1gFnSc7	částka
1,63	[number]	k4	1,63
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gNnSc7	její
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
159,2	[number]	k4	159,2
milionu	milion	k4xCgInSc2	milion
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
regionech	region	k1gInPc6	region
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Skupina	skupina	k1gFnSc1	skupina
ČEZ	ČEZ	kA	ČEZ
působí	působit	k5eAaImIp3nS	působit
<g/>
,	,	kIx,	,
podporuje	podporovat	k5eAaImIp3nS	podporovat
projekty	projekt	k1gInPc4	projekt
zaměřené	zaměřený	k2eAgInPc4d1	zaměřený
na	na	k7c4	na
zvyšování	zvyšování	k1gNnSc4	zvyšování
kvality	kvalita	k1gFnSc2	kvalita
života	život	k1gInSc2	život
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
aktivity	aktivita	k1gFnPc1	aktivita
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
článků	článek	k1gInPc2	článek
firemní	firemní	k2eAgFnSc2d1	firemní
filozofie	filozofie	k1gFnSc2	filozofie
"	"	kIx"	"
<g/>
pomáhat	pomáhat	k5eAaImF	pomáhat
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působíme	působit	k5eAaImIp1nP	působit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
Skupina	skupina	k1gFnSc1	skupina
ČEZ	ČEZ	kA	ČEZ
podporuje	podporovat	k5eAaImIp3nS	podporovat
jako	jako	k9	jako
generální	generální	k2eAgMnSc1d1	generální
partner	partner	k1gMnSc1	partner
Český	český	k2eAgInSc4d1	český
olympijský	olympijský	k2eAgInSc4d1	olympijský
tým	tým	k1gInSc4	tým
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
celkové	celkový	k2eAgFnSc2d1	celková
kvality	kvalita	k1gFnSc2	kvalita
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
zákazníky	zákazník	k1gMnPc4	zákazník
zřídila	zřídit	k5eAaPmAgFnS	zřídit
Skupina	skupina	k1gFnSc1	skupina
ČEZ	ČEZ	kA	ČEZ
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
funkci	funkce	k1gFnSc4	funkce
ombudsmana	ombudsman	k1gMnSc2	ombudsman
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
podáním	podání	k1gNnSc7	podání
se	se	k3xPyFc4	se
na	na	k7c4	na
ombudsmana	ombudsman	k1gMnSc4	ombudsman
může	moct	k5eAaImIp3nS	moct
obrátit	obrátit	k5eAaPmF	obrátit
zákazník	zákazník	k1gMnSc1	zákazník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
není	být	k5eNaImIp3nS	být
spokojen	spokojen	k2eAgMnSc1d1	spokojen
s	s	k7c7	s
vyřízením	vyřízení	k1gNnSc7	vyřízení
své	svůj	k3xOyFgFnSc2	svůj
stížnosti	stížnost	k1gFnSc2	stížnost
či	či	k8xC	či
reklamace	reklamace	k1gFnSc2	reklamace
dříve	dříve	k6eAd2	dříve
podané	podaný	k2eAgFnPc1d1	podaná
u	u	k7c2	u
příslušné	příslušný	k2eAgFnSc2d1	příslušná
společnosti	společnost	k1gFnSc2	společnost
Skupiny	skupina	k1gFnSc2	skupina
ČEZ	ČEZ	kA	ČEZ
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
je	být	k5eAaImIp3nS	být
ČEZ	ČEZ	kA	ČEZ
každoročně	každoročně	k6eAd1	každoročně
nejziskovější	ziskový	k2eAgFnSc7d3	nejziskovější
českou	český	k2eAgFnSc7d1	Česká
společností	společnost	k1gFnSc7	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
kritici	kritik	k1gMnPc1	kritik
upozorňovali	upozorňovat	k5eAaImAgMnP	upozorňovat
na	na	k7c4	na
monopolní	monopolní	k2eAgNnSc4d1	monopolní
postavení	postavení	k1gNnSc4	postavení
ČEZ	ČEZ	kA	ČEZ
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
cena	cena	k1gFnSc1	cena
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
se	s	k7c7	s
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
dividend	dividenda	k1gFnPc2	dividenda
stala	stát	k5eAaPmAgFnS	stát
příjmem	příjem	k1gInSc7	příjem
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
a	a	k8xC	a
tedy	tedy	k9	tedy
vlastně	vlastně	k9	vlastně
jakousi	jakýsi	k3yIgFnSc7	jakýsi
formou	forma	k1gFnSc7	forma
skryté	skrytý	k2eAgFnPc1d1	skrytá
daně	daň	k1gFnPc1	daň
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
už	už	k6eAd1	už
se	se	k3xPyFc4	se
ale	ale	k9	ale
v	v	k7c6	v
ČR	ČR	kA	ČR
obchoduje	obchodovat	k5eAaImIp3nS	obchodovat
silová	silový	k2eAgFnSc1d1	silová
elektřina	elektřina	k1gFnSc1	elektřina
na	na	k7c6	na
burze	burza	k1gFnSc6	burza
PXE	PXE	kA	PXE
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc4	její
cenu	cena	k1gFnSc4	cena
tak	tak	k6eAd1	tak
určuje	určovat	k5eAaImIp3nS	určovat
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
kritiky	kritik	k1gMnPc7	kritik
společnosti	společnost	k1gFnSc2	společnost
ČEZ	ČEZ	kA	ČEZ
patří	patřit	k5eAaImIp3nP	patřit
zejména	zejména	k9	zejména
ekologické	ekologický	k2eAgFnPc1d1	ekologická
organizace	organizace	k1gFnPc1	organizace
a	a	k8xC	a
také	také	k9	také
Strana	strana	k1gFnSc1	strana
zelených	zelený	k2eAgMnPc2d1	zelený
<g/>
.	.	kIx.	.
</s>
<s>
Nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
stát	stát	k1gInSc1	stát
firmě	firma	k1gFnSc3	firma
prodal	prodat	k5eAaPmAgInS	prodat
většinu	většina	k1gFnSc4	většina
distribučních	distribuční	k2eAgFnPc2d1	distribuční
společností	společnost	k1gFnPc2	společnost
a	a	k8xC	a
některé	některý	k3yIgFnSc2	některý
severočeské	severočeský	k2eAgFnSc2d1	Severočeská
firmy	firma	k1gFnSc2	firma
těžící	těžící	k2eAgNnSc4d1	těžící
hnědé	hnědý	k2eAgNnSc4d1	hnědé
uhlí	uhlí	k1gNnSc4	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Argumentují	argumentovat	k5eAaImIp3nP	argumentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tak	tak	k6eAd1	tak
byla	být	k5eAaImAgFnS	být
potlačena	potlačen	k2eAgFnSc1d1	potlačena
svobodná	svobodný	k2eAgFnSc1d1	svobodná
soutěž	soutěž	k1gFnSc1	soutěž
na	na	k7c6	na
energetickém	energetický	k2eAgInSc6d1	energetický
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Kritika	kritika	k1gFnSc1	kritika
rovněž	rovněž	k9	rovněž
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
malé	malý	k2eAgFnSc3d1	malá
podpoře	podpora	k1gFnSc3	podpora
údajně	údajně	k6eAd1	údajně
ekologičtějších	ekologický	k2eAgInPc2d2	ekologičtější
zdrojů	zdroj	k1gInPc2	zdroj
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
upřednostňování	upřednostňování	k1gNnSc4	upřednostňování
výroby	výroba	k1gFnSc2	výroba
z	z	k7c2	z
neobnovitelných	obnovitelný	k2eNgInPc2d1	neobnovitelný
zdrojů	zdroj	k1gInPc2	zdroj
-	-	kIx~	-
jádra	jádro	k1gNnSc2	jádro
a	a	k8xC	a
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
kritici	kritik	k1gMnPc1	kritik
poukazovali	poukazovat	k5eAaImAgMnP	poukazovat
na	na	k7c4	na
nízkou	nízký	k2eAgFnSc4d1	nízká
účinnost	účinnost	k1gFnSc4	účinnost
tepelných	tepelný	k2eAgFnPc2d1	tepelná
elektráren	elektrárna	k1gFnPc2	elektrárna
provozovaných	provozovaný	k2eAgFnPc2d1	provozovaná
společností	společnost	k1gFnPc2	společnost
ČEZ	ČEZ	kA	ČEZ
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
výrazně	výrazně	k6eAd1	výrazně
zvyšována	zvyšován	k2eAgFnSc1d1	zvyšována
díky	díky	k7c3	díky
nákladnému	nákladný	k2eAgInSc3d1	nákladný
Programu	program	k1gInSc3	program
obnovy	obnova	k1gFnSc2	obnova
uhelných	uhelný	k2eAgInPc2d1	uhelný
zdrojů	zdroj	k1gInPc2	zdroj
Skupiny	skupina	k1gFnSc2	skupina
ČEZ	ČEZ	kA	ČEZ
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
realizační	realizační	k2eAgFnSc1d1	realizační
fáze	fáze	k1gFnSc1	fáze
se	se	k3xPyFc4	se
chýlí	chýlit	k5eAaImIp3nS	chýlit
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
něj	on	k3xPp3gMnSc4	on
došlo	dojít	k5eAaPmAgNnS	dojít
jednak	jednak	k8xC	jednak
k	k	k7c3	k
odstavení	odstavení	k1gNnSc3	odstavení
či	či	k8xC	či
komplexní	komplexní	k2eAgFnSc3d1	komplexní
obnově	obnova	k1gFnSc3	obnova
starých	starý	k2eAgInPc2d1	starý
bloků	blok	k1gInPc2	blok
a	a	k8xC	a
výstavbě	výstavba	k1gFnSc3	výstavba
nového	nový	k2eAgInSc2d1	nový
moderního	moderní	k2eAgInSc2d1	moderní
nadkritického	nadkritický	k2eAgInSc2d1	nadkritický
660MW	[number]	k4	660MW
bloku	blok	k1gInSc2	blok
v	v	k7c6	v
Ledvicích	Ledvice	k1gFnPc6	Ledvice
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
47	[number]	k4	47
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Aktivisté	aktivista	k1gMnPc1	aktivista
z	z	k7c2	z
Greenpeace	Greenpeace	k1gFnSc2	Greenpeace
přesto	přesto	k8xC	přesto
kritizují	kritizovat	k5eAaImIp3nP	kritizovat
modernizaci	modernizace	k1gFnSc4	modernizace
jako	jako	k8xS	jako
nedostatečnou	dostatečný	k2eNgFnSc4d1	nedostatečná
<g/>
.	.	kIx.	.
</s>
<s>
Odpůrci	odpůrce	k1gMnPc1	odpůrce
společnosti	společnost	k1gFnSc2	společnost
také	také	k9	také
kritizují	kritizovat	k5eAaImIp3nP	kritizovat
na	na	k7c4	na
evropské	evropský	k2eAgInPc4d1	evropský
poměry	poměr	k1gInPc4	poměr
mírné	mírný	k2eAgFnSc2d1	mírná
limity	limita	k1gFnSc2	limita
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
společnosti	společnost	k1gFnPc4	společnost
ČEZ	ČEZ	kA	ČEZ
stanovila	stanovit	k5eAaPmAgFnS	stanovit
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
parlament	parlament	k1gInSc1	parlament
v	v	k7c6	v
případě	případ	k1gInSc6	případ
havárie	havárie	k1gFnSc2	havárie
jaderného	jaderný	k2eAgNnSc2d1	jaderné
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
havárii	havárie	k1gFnSc3	havárie
jaderné	jaderný	k2eAgFnSc2d1	jaderná
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
,	,	kIx,	,
ČEZ	ČEZ	kA	ČEZ
podle	podle	k7c2	podle
tzv.	tzv.	kA	tzv.
atomového	atomový	k2eAgInSc2d1	atomový
zákona	zákon	k1gInSc2	zákon
ručí	ručit	k5eAaImIp3nS	ručit
za	za	k7c4	za
škody	škoda	k1gFnPc4	škoda
jen	jen	k9	jen
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
6	[number]	k4	6
miliard	miliarda	k4xCgFnPc2	miliarda
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
společnost	společnost	k1gFnSc1	společnost
by	by	kYmCp3nS	by
ze	z	k7c2	z
smluvního	smluvní	k2eAgNnSc2d1	smluvní
pojištění	pojištění	k1gNnSc2	pojištění
získala	získat	k5eAaPmAgFnS	získat
okolo	okolo	k7c2	okolo
35	[number]	k4	35
miliard	miliarda	k4xCgFnPc2	miliarda
odškodného	odškodné	k1gNnSc2	odškodné
<g/>
.	.	kIx.	.
</s>
<s>
Policejní	policejní	k2eAgInSc1d1	policejní
Útvar	útvar	k1gInSc1	útvar
pro	pro	k7c4	pro
odhalování	odhalování	k1gNnSc4	odhalování
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
zločinu	zločin	k1gInSc2	zločin
(	(	kIx(	(
<g/>
ÚOOZ	ÚOOZ	kA	ÚOOZ
<g/>
)	)	kIx)	)
počátkem	počátkem	k7c2	počátkem
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
obvinil	obvinit	k5eAaPmAgInS	obvinit
32	[number]	k4	32
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
ČEZ	ČEZ	kA	ČEZ
Měření	měření	k1gNnSc1	měření
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
z	z	k7c2	z
vydírání	vydírání	k1gNnSc2	vydírání
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
policie	policie	k1gFnSc2	policie
vstupovali	vstupovat	k5eAaImAgMnP	vstupovat
obvinění	obviněný	k1gMnPc1	obviněný
na	na	k7c4	na
pozemky	pozemek	k1gInPc4	pozemek
odběratelů	odběratel	k1gMnPc2	odběratel
elektřiny	elektřina	k1gFnSc2	elektřina
bez	bez	k7c2	bez
povolení	povolení	k1gNnSc2	povolení
a	a	k8xC	a
nutili	nutit	k5eAaImAgMnP	nutit
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
zaplacení	zaplacení	k1gNnSc3	zaplacení
vysokých	vysoký	k2eAgFnPc2d1	vysoká
částek	částka	k1gFnPc2	částka
za	za	k7c4	za
tzv.	tzv.	kA	tzv.
černý	černý	k2eAgInSc4d1	černý
odběr	odběr	k1gInSc4	odběr
<g/>
.	.	kIx.	.
</s>
<s>
Mluvčí	mluvčí	k1gMnSc1	mluvčí
firmy	firma	k1gFnSc2	firma
ČEZ	ČEZ	kA	ČEZ
pro	pro	k7c4	pro
ČTK	ČTK	kA	ČTK
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
společnosti	společnost	k1gFnPc1	společnost
za	za	k7c7	za
svými	svůj	k3xOyFgMnPc7	svůj
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
stojí	stát	k5eAaImIp3nS	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2010	[number]	k4	2010
ÚOOZ	ÚOOZ	kA	ÚOOZ
podalo	podat	k5eAaPmAgNnS	podat
návrh	návrh	k1gInSc4	návrh
obžaloby	obžaloba	k1gFnSc2	obžaloba
26	[number]	k4	26
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
firmy	firma	k1gFnSc2	firma
ČEZ	ČEZ	kA	ČEZ
Měření	měření	k1gNnSc1	měření
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
policie	policie	k1gFnSc1	policie
obvinila	obvinit	k5eAaPmAgFnS	obvinit
z	z	k7c2	z
vydírání	vydírání	k1gNnPc2	vydírání
odběratelů	odběratel	k1gMnPc2	odběratel
elektřiny	elektřina	k1gFnSc2	elektřina
ve	v	k7c6	v
48	[number]	k4	48
případech	případ	k1gInPc6	případ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2010	[number]	k4	2010
státní	státní	k2eAgFnSc6d1	státní
zástupce	zástupka	k1gFnSc6	zástupka
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
obžalovat	obžalovat	k5eAaPmF	obžalovat
13	[number]	k4	13
kontrolorů	kontrolor	k1gMnPc2	kontrolor
společnosti	společnost	k1gFnSc2	společnost
ČEZ	ČEZ	kA	ČEZ
z	z	k7c2	z
útisku	útisk	k1gInSc2	útisk
<g/>
,	,	kIx,	,
u	u	k7c2	u
ostatních	ostatní	k2eAgNnPc2d1	ostatní
stíhání	stíhání	k1gNnSc2	stíhání
zastavil	zastavit	k5eAaPmAgInS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
obžalobě	obžaloba	k1gFnSc6	obžaloba
rozhodovalo	rozhodovat	k5eAaImAgNnS	rozhodovat
Okresní	okresní	k2eAgNnSc1d1	okresní
státní	státní	k2eAgNnSc1d1	státní
zastupitelství	zastupitelství	k1gNnSc1	zastupitelství
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
stíhání	stíhání	k1gNnSc2	stíhání
zastavil	zastavit	k5eAaPmAgMnS	zastavit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
jeho	on	k3xPp3gInSc2	on
názoru	názor	k1gInSc2	názor
trestný	trestný	k2eAgInSc4d1	trestný
čin	čin	k1gInSc4	čin
nestal	stát	k5eNaPmAgInS	stát
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
poté	poté	k6eAd1	poté
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
i	i	k9	i
Krajský	krajský	k2eAgInSc4d1	krajský
soud	soud	k1gInSc4	soud
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
státní	státní	k2eAgMnSc1d1	státní
zástupce	zástupce	k1gMnSc1	zástupce
Pavel	Pavel	k1gMnSc1	Pavel
Zeman	Zeman	k1gMnSc1	Zeman
následně	následně	k6eAd1	následně
podal	podat	k5eAaPmAgMnS	podat
dovolání	dovolání	k1gNnSc4	dovolání
k	k	k7c3	k
Nejvyššímu	vysoký	k2eAgInSc3d3	Nejvyšší
soudu	soud	k1gInSc3	soud
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
však	však	k9	však
soud	soud	k1gInSc1	soud
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2012	[number]	k4	2012
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
a	a	k8xC	a
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
nižších	nízký	k2eAgInPc2d2	nižší
soudů	soud	k1gInPc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ČEZ	ČEZ	kA	ČEZ
příznivě	příznivě	k6eAd1	příznivě
ale	ale	k8xC	ale
vyznívá	vyznívat	k5eAaImIp3nS	vyznívat
stanovisko	stanovisko	k1gNnSc4	stanovisko
Státní	státní	k2eAgFnSc2d1	státní
energetické	energetický	k2eAgFnSc2d1	energetická
inspekce	inspekce	k1gFnSc2	inspekce
<g/>
,	,	kIx,	,
<g/>
která	který	k3yIgFnSc1	který
na	na	k7c4	na
podnět	podnět	k1gInSc4	podnět
Energetického	energetický	k2eAgInSc2d1	energetický
regulačního	regulační	k2eAgInSc2d1	regulační
úřadu	úřad	k1gInSc2	úřad
prověřila	prověřit	k5eAaPmAgFnS	prověřit
kontroly	kontrola	k1gFnPc4	kontrola
společnosti	společnost	k1gFnSc2	společnost
ČEZ	ČEZ	kA	ČEZ
Distribuce	distribuce	k1gFnSc1	distribuce
a	a	k8xC	a
postup	postup	k1gInSc1	postup
ČEZ	ČEZ	kA	ČEZ
v	v	k7c6	v
případě	případ	k1gInSc6	případ
94	[number]	k4	94
odběratelů	odběratel	k1gMnPc2	odběratel
<g/>
.	.	kIx.	.
</s>
<s>
Dospěla	dochvít	k5eAaPmAgFnS	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
firma	firma	k1gFnSc1	firma
nepřekročila	překročit	k5eNaPmAgFnS	překročit
oprávnění	oprávnění	k1gNnSc4	oprávnění
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
energetického	energetický	k2eAgInSc2d1	energetický
zákona	zákon	k1gInSc2	zákon
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
ČEZ	ČEZ	kA	ČEZ
při	při	k7c6	při
odhalování	odhalování	k1gNnSc6	odhalování
neoprávněných	oprávněný	k2eNgInPc2d1	neoprávněný
odběrů	odběr	k1gInPc2	odběr
nepochybil	pochybit	k5eNaPmAgInS	pochybit
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
uvedl	uvést	k5eAaPmAgMnS	uvést
Petr	Petr	k1gMnSc1	Petr
Holoubek	Holoubek	k1gMnSc1	Holoubek
pověřený	pověřený	k2eAgMnSc1d1	pověřený
řízením	řízení	k1gNnSc7	řízení
SEI	SEI	kA	SEI
ve	v	k7c6	v
stanovisku	stanovisko	k1gNnSc6	stanovisko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
má	mít	k5eAaImIp3nS	mít
ČTK	ČTK	kA	ČTK
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
Sdružení	sdružení	k1gNnSc2	sdružení
proti	proti	k7c3	proti
nemravným	mravný	k2eNgFnPc3d1	nemravná
cenám	cena	k1gFnPc3	cena
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
Sponel	Sponel	k1gInSc1	Sponel
<g/>
)	)	kIx)	)
podalo	podat	k5eAaPmAgNnS	podat
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
první	první	k4xOgFnSc4	první
spotřebitelskou	spotřebitelský	k2eAgFnSc4d1	spotřebitelská
žalobu	žaloba	k1gFnSc4	žaloba
na	na	k7c4	na
firmu	firma	k1gFnSc4	firma
ČEZ	ČEZ	kA	ČEZ
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
podle	podle	k7c2	podle
žalobců	žalobce	k1gMnPc2	žalobce
"	"	kIx"	"
<g/>
zamezit	zamezit	k5eAaPmF	zamezit
protizákonnému	protizákonný	k2eAgNnSc3d1	protizákonné
jednání	jednání	k1gNnSc3	jednání
ČEZu	ČEZus	k1gInSc2	ČEZus
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
odpojování	odpojování	k1gNnSc2	odpojování
elektroměrů	elektroměr	k1gInPc2	elektroměr
domácností	domácnost	k1gFnPc2	domácnost
a	a	k8xC	a
malých	malý	k2eAgInPc2d1	malý
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
následnému	následný	k2eAgNnSc3d1	následné
stanovování	stanovování	k1gNnSc3	stanovování
škody	škoda	k1gFnSc2	škoda
a	a	k8xC	a
připojování	připojování	k1gNnSc2	připojování
k	k	k7c3	k
elektřině	elektřina	k1gFnSc3	elektřina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sdružení	sdružení	k1gNnSc1	sdružení
předložilo	předložit	k5eAaPmAgNnS	předložit
soudu	soud	k1gInSc2	soud
zhruba	zhruba	k6eAd1	zhruba
50	[number]	k4	50
případů	případ	k1gInPc2	případ
údajného	údajný	k2eAgNnSc2d1	údajné
nezákonného	zákonný	k2eNgNnSc2d1	nezákonné
jednání	jednání	k1gNnSc2	jednání
ČEZ	ČEZ	kA	ČEZ
a	a	k8xC	a
během	během	k7c2	během
projednání	projednání	k1gNnSc2	projednání
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
předvoláno	předvolat	k5eAaPmNgNnS	předvolat
asi	asi	k9	asi
300	[number]	k4	300
svědků	svědek	k1gMnPc2	svědek
<g/>
.	.	kIx.	.
</s>
<s>
Poškozeným	poškozený	k2eAgInSc7d1	poškozený
měla	mít	k5eAaImAgFnS	mít
vzniknout	vzniknout	k5eAaPmF	vzniknout
škoda	škoda	k1gFnSc1	škoda
až	až	k9	až
20	[number]	k4	20
miliónů	milión	k4xCgInPc2	milión
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2010	[number]	k4	2010
ale	ale	k8xC	ale
městský	městský	k2eAgInSc1d1	městský
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
žalobu	žaloba	k1gFnSc4	žaloba
proti	proti	k7c3	proti
Skupině	skupina	k1gFnSc3	skupina
ČEZ	ČEZ	kA	ČEZ
zamítl	zamítnout	k5eAaPmAgMnS	zamítnout
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
bodech	bod	k1gInPc6	bod
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgNnSc1d1	současné
vedení	vedení	k1gNnSc1	vedení
společnosti	společnost	k1gFnSc2	společnost
ČEZ	ČEZ	kA	ČEZ
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
předcházet	předcházet	k5eAaImF	předcházet
dalším	další	k2eAgFnPc3d1	další
žalobám	žaloba	k1gFnPc3	žaloba
vydalo	vydat	k5eAaPmAgNnS	vydat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2014	[number]	k4	2014
Zákaznický	zákaznický	k2eAgInSc1d1	zákaznický
kodex	kodex	k1gInSc1	kodex
ČEZ	ČEZ	kA	ČEZ
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
rozsah	rozsah	k1gInSc4	rozsah
i	i	k8xC	i
úroveň	úroveň	k1gFnSc4	úroveň
služeb	služba	k1gFnPc2	služba
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
mohou	moct	k5eAaImIp3nP	moct
domácnosti	domácnost	k1gFnPc1	domácnost
a	a	k8xC	a
malí	malý	k2eAgMnPc1d1	malý
podnikatelé	podnikatel	k1gMnPc1	podnikatel
při	při	k7c6	při
využívání	využívání	k1gNnSc6	využívání
produktů	produkt	k1gInPc2	produkt
Skupiny	skupina	k1gFnSc2	skupina
ČEZ	ČEZ	kA	ČEZ
očekávat	očekávat	k5eAaImF	očekávat
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Toskánská	toskánský	k2eAgFnSc1d1	Toskánská
aféra	aféra	k1gFnSc1	aféra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2011	[number]	k4	2011
předseda	předseda	k1gMnSc1	předseda
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
TOP09	TOP09	k1gMnSc1	TOP09
Karel	Karel	k1gMnSc1	Karel
Schwarzenberg	Schwarzenberg	k1gMnSc1	Schwarzenberg
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ČEZ	ČEZ	kA	ČEZ
skrytě	skrytě	k6eAd1	skrytě
financoval	financovat	k5eAaBmAgMnS	financovat
politické	politický	k2eAgFnPc4d1	politická
strany	strana	k1gFnPc4	strana
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
tvrzení	tvrzení	k1gNnSc4	tvrzení
dále	daleko	k6eAd2	daleko
podpořil	podpořit	k5eAaPmAgMnS	podpořit
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ředitel	ředitel	k1gMnSc1	ředitel
české	český	k2eAgFnSc2d1	Česká
rozvědky	rozvědka	k1gFnSc2	rozvědka
a	a	k8xC	a
exministr	exministr	k1gMnSc1	exministr
vnitra	vnitro	k1gNnSc2	vnitro
František	František	k1gMnSc1	František
Bublan	Bublan	k1gMnSc1	Bublan
<g/>
.	.	kIx.	.
</s>
<s>
Martin	Martin	k1gMnSc1	Martin
Roman	Roman	k1gMnSc1	Roman
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
společnost	společnost	k1gFnSc1	společnost
ČEZ	ČEZ	kA	ČEZ
vedlo	vést	k5eAaImAgNnS	vést
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
ke	k	k7c3	k
spekulacím	spekulace	k1gFnPc3	spekulace
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
vlivu	vliv	k1gInSc6	vliv
na	na	k7c4	na
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
skrytém	skrytý	k2eAgNnSc6d1	skryté
financování	financování	k1gNnSc6	financování
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
pro	pro	k7c4	pro
časopis	časopis	k1gInSc4	časopis
Týden	týden	k1gInSc1	týden
uvedl	uvést	k5eAaPmAgInS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jestli	jestli	k8xS	jestli
mám	mít	k5eAaImIp1nS	mít
nějaký	nějaký	k3yIgInSc4	nějaký
vliv	vliv	k1gInSc4	vliv
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jen	jen	k9	jen
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
příležitostně	příležitostně	k6eAd1	příležitostně
zajímají	zajímat	k5eAaImIp3nP	zajímat
o	o	k7c4	o
mé	můj	k3xOp1gInPc4	můj
názory	názor	k1gInPc4	názor
<g/>
.	.	kIx.	.
</s>
<s>
Nevím	vědět	k5eNaImIp1nS	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
a	a	k8xC	a
z	z	k7c2	z
jakých	jaký	k3yRgInPc2	jaký
peněz	peníze	k1gInPc2	peníze
bych	by	kYmCp1nS	by
mohl	moct	k5eAaImAgMnS	moct
někoho	někdo	k3yInSc4	někdo
financovat	financovat	k5eAaBmF	financovat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Současné	současný	k2eAgNnSc4d1	současné
vedení	vedení	k1gNnSc4	vedení
společnosti	společnost	k1gFnSc2	společnost
si	se	k3xPyFc3	se
naopak	naopak	k6eAd1	naopak
zakládá	zakládat	k5eAaImIp3nS	zakládat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
prodlouženou	prodloužený	k2eAgFnSc7d1	prodloužená
rukou	ruka	k1gFnSc7	ruka
plnící	plnící	k2eAgFnSc4d1	plnící
energetickou	energetický	k2eAgFnSc4d1	energetická
koncepci	koncepce	k1gFnSc4	koncepce
jeho	jeho	k3xOp3gMnSc2	jeho
akcionáře	akcionář	k1gMnSc2	akcionář
-	-	kIx~	-
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ČEZ	ČEZ	kA	ČEZ
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
www.cez.cz	www.cez.cz	k1gMnSc1	www.cez.cz
www.dukovany.cz	www.dukovany.cz	k1gMnSc1	www.dukovany.cz
-	-	kIx~	-
Sdružení	sdružení	k1gNnSc1	sdružení
operativního	operativní	k2eAgInSc2d1	operativní
řídícího	řídící	k2eAgInSc2d1	řídící
personálu	personál	k1gInSc2	personál
jaderných	jaderný	k2eAgFnPc2d1	jaderná
elektráren	elektrárna	k1gFnPc2	elektrárna
ČEZ	ČEZ	kA	ČEZ
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
-	-	kIx~	-
vizuální	vizuální	k2eAgInSc1d1	vizuální
obchodní	obchodní	k2eAgInSc1d1	obchodní
rejstřík	rejstřík	k1gInSc1	rejstřík
vizualizace	vizualizace	k1gFnSc2	vizualizace
vztahů	vztah	k1gInPc2	vztah
a	a	k8xC	a
historie	historie	k1gFnSc2	historie
firmy	firma	k1gFnSc2	firma
ČEZ	ČEZ	kA	ČEZ
v	v	k7c6	v
rejstříku	rejstřík	k1gInSc6	rejstřík
firem	firma	k1gFnPc2	firma
</s>
