<p>
<s>
Virgilijus	Virgilijus	k1gInSc1	Virgilijus
Alekna	Alekn	k1gInSc2	Alekn
(	(	kIx(	(
<g/>
*	*	kIx~	*
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
Terpeikiai	Terpeikia	k1gFnPc1	Terpeikia
blízko	blízko	k7c2	blízko
Kupiškisu	Kupiškis	k1gInSc2	Kupiškis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
litevský	litevský	k2eAgMnSc1d1	litevský
atlet	atlet	k1gMnSc1	atlet
<g/>
,	,	kIx,	,
dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
<g/>
,	,	kIx,	,
dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
a	a	k8xC	a
mistr	mistr	k1gMnSc1	mistr
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
hodu	hod	k1gInSc6	hod
diskem	disk	k1gInSc7	disk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2000	[number]	k4	2000
v	v	k7c6	v
Kaunasu	Kaunas	k1gInSc6	Kaunas
poslal	poslat	k5eAaPmAgMnS	poslat
disk	disk	k1gInSc4	disk
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
73,88	[number]	k4	73,88
metru	metr	k1gInSc2	metr
a	a	k8xC	a
za	za	k7c7	za
světovým	světový	k2eAgInSc7d1	světový
rekordem	rekord	k1gInSc7	rekord
Němce	Němec	k1gMnSc2	Němec
Jürgena	Jürgen	k1gMnSc2	Jürgen
Schulta	Schult	k1gMnSc2	Schult
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
zaostal	zaostat	k5eAaPmAgInS	zaostat
o	o	k7c4	o
20	[number]	k4	20
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vítězem	vítěz	k1gMnSc7	vítěz
ankety	anketa	k1gFnSc2	anketa
Atlet	atlet	k1gMnSc1	atlet
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úspěchy	úspěch	k1gInPc1	úspěch
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Virgilijus	Virgilijus	k1gInSc1	Virgilijus
Alekna	Alekno	k1gNnSc2	Alekno
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Virgilijus	Virgilijus	k1gInSc1	Virgilijus
Alekna	Alekn	k1gInSc2	Alekn
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
IAAF	IAAF	kA	IAAF
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Profil	profil	k1gInSc1	profil
na	na	k7c6	na
sports-reference	sportseferenka	k1gFnSc6	sports-referenka
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
