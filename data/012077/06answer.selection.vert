<s>
Státní	státní	k2eAgFnSc1d1	státní
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
druhem	druh	k1gInSc7	druh
veřejné	veřejný	k2eAgFnSc2d1	veřejná
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
schopnost	schopnost	k1gFnSc4	schopnost
státu	stát	k1gInSc2	stát
vnutit	vnutit	k5eAaPmF	vnutit
svou	svůj	k3xOyFgFnSc4	svůj
vůli	vůle	k1gFnSc4	vůle
subjektům	subjekt	k1gInPc3	subjekt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
dosahu	dosah	k1gInSc6	dosah
jeho	jeho	k3xOp3gInSc2	jeho
právního	právní	k2eAgInSc2d1	právní
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
