<p>
<s>
Státní	státní	k2eAgFnSc1d1	státní
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
druhem	druh	k1gInSc7	druh
veřejné	veřejný	k2eAgFnSc2d1	veřejná
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
schopnost	schopnost	k1gFnSc4	schopnost
státu	stát	k1gInSc2	stát
vnutit	vnutit	k5eAaPmF	vnutit
svou	svůj	k3xOyFgFnSc4	svůj
vůli	vůle	k1gFnSc4	vůle
subjektům	subjekt	k1gInPc3	subjekt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
dosahu	dosah	k1gInSc6	dosah
jeho	on	k3xPp3gInSc2	on
právního	právní	k2eAgInSc2d1	právní
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
prostředek	prostředek	k1gInSc1	prostředek
organizace	organizace	k1gFnSc2	organizace
a	a	k8xC	a
řízení	řízení	k1gNnSc2	řízení
společenských	společenský	k2eAgInPc2d1	společenský
vztahů	vztah	k1gInPc2	vztah
uvnitř	uvnitř	k7c2	uvnitř
určité	určitý	k2eAgFnSc2d1	určitá
státní	státní	k2eAgFnSc2d1	státní
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stát	stát	k1gInSc1	stát
jako	jako	k8xC	jako
subjekt	subjekt	k1gInSc1	subjekt
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
od	od	k7c2	od
monokracie	monokracie	k1gFnSc2	monokracie
a	a	k8xC	a
aristokracie	aristokracie	k1gFnSc2	aristokracie
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
různé	různý	k2eAgFnPc4d1	různá
modifikace	modifikace	k1gFnPc4	modifikace
zastupitelské	zastupitelský	k2eAgFnSc2d1	zastupitelská
demokracie	demokracie	k1gFnSc2	demokracie
<g/>
,	,	kIx,	,
až	až	k9	až
ke	k	k7c3	k
stávající	stávající	k2eAgFnSc3d1	stávající
podobě	podoba	k1gFnSc3	podoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
tradičním	tradiční	k2eAgMnSc6d1	tradiční
eurocentrickém	eurocentrický	k2eAgMnSc6d1	eurocentrický
a	a	k8xC	a
angloamerickém	angloamerický	k2eAgNnSc6d1	angloamerické
pojetí	pojetí	k1gNnSc6	pojetí
je	být	k5eAaImIp3nS	být
mocensky	mocensky	k6eAd1	mocensky
vyvážená	vyvážený	k2eAgFnSc1d1	vyvážená
a	a	k8xC	a
kontrolovaná	kontrolovaný	k2eAgFnSc1d1	kontrolovaná
instituce	instituce	k1gFnSc1	instituce
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
věc	věc	k1gFnSc1	věc
veřejná	veřejný	k2eAgFnSc1d1	veřejná
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
res	res	k?	res
publica	publicum	k1gNnSc2	publicum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novějším	nový	k2eAgNnSc6d2	novější
pojetí	pojetí	k1gNnSc6	pojetí
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgFnSc1d1	státní
moc	moc	k1gFnSc1	moc
zastupitelské	zastupitelský	k2eAgFnSc2d1	zastupitelská
demokracie	demokracie	k1gFnSc2	demokracie
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
o	o	k7c4	o
přímou	přímý	k2eAgFnSc4d1	přímá
spoluúčast	spoluúčast	k1gFnSc4	spoluúčast
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
přímou	přímý	k2eAgFnSc4d1	přímá
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výkon	výkon	k1gInSc1	výkon
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
==	==	k?	==
</s>
</p>
<p>
<s>
Výkon	výkon	k1gInSc1	výkon
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
acta	acta	k6eAd1	acta
iure	iurat	k5eAaPmIp3nS	iurat
imperii	imperie	k1gFnSc4	imperie
–	–	k?	–
akty	akt	k1gInPc7	akt
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
motivovány	motivovat	k5eAaBmNgInP	motivovat
výsostně	výsostně	k6eAd1	výsostně
politicky	politicky	k6eAd1	politicky
<g/>
;	;	kIx,	;
stát	stát	k1gInSc1	stát
zde	zde	k6eAd1	zde
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jako	jako	k8xS	jako
nositel	nositel	k1gMnSc1	nositel
své	svůj	k3xOyFgFnSc2	svůj
vlastní	vlastní	k2eAgFnSc2d1	vlastní
suverenity	suverenita	k1gFnSc2	suverenita
</s>
</p>
<p>
<s>
acta	acta	k1gFnSc1	acta
iure	iurat	k5eAaPmIp3nS	iurat
gestionis	gestionis	k1gInSc4	gestionis
–	–	k?	–
akty	akt	k1gInPc4	akt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nemají	mít	k5eNaImIp3nP	mít
politický	politický	k2eAgInSc4d1	politický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
účel	účel	k1gInSc4	účel
<g/>
;	;	kIx,	;
stát	stát	k1gInSc1	stát
zde	zde	k6eAd1	zde
nevystupuje	vystupovat	k5eNaImIp3nS	vystupovat
jako	jako	k9	jako
suverén	suverén	k1gMnSc1	suverén
a	a	k8xC	a
neoplývá	oplývat	k5eNaImIp3nS	oplývat
tudíž	tudíž	k8xC	tudíž
mezinárodněprávní	mezinárodněprávní	k2eAgNnSc1d1	mezinárodněprávní
imunitouV	imunitouV	k?	imunitouV
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
čl	čl	kA	čl
<g/>
.	.	kIx.	.
2	[number]	k4	2
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
3	[number]	k4	3
Ústavou	ústava	k1gFnSc7	ústava
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
státní	státní	k2eAgFnSc1d1	státní
moc	moc	k1gFnSc1	moc
slouží	sloužit	k5eAaImIp3nS	sloužit
všem	všecek	k3xTgMnPc3	všecek
občanům	občan	k1gMnPc3	občan
a	a	k8xC	a
lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
jen	jen	k9	jen
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
v	v	k7c6	v
mezích	mez	k1gFnPc6	mez
a	a	k8xC	a
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
stanoví	stanovit	k5eAaPmIp3nS	stanovit
zákon	zákon	k1gInSc1	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
nebylo	být	k5eNaImAgNnS	být
a	a	k8xC	a
činnost	činnost	k1gFnSc4	činnost
ústavních	ústavní	k2eAgInPc2d1	ústavní
orgánů	orgán	k1gInPc2	orgán
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
ochromena	ochromit	k5eAaPmNgFnS	ochromit
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělba	dělba	k1gFnSc1	dělba
moci	moc	k1gFnSc2	moc
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
demokratickém	demokratický	k2eAgInSc6d1	demokratický
právním	právní	k2eAgInSc6d1	právní
státě	stát	k1gInSc6	stát
existuje	existovat	k5eAaImIp3nS	existovat
vyvážený	vyvážený	k2eAgInSc1d1	vyvážený
systém	systém	k1gInSc1	systém
tří	tři	k4xCgFnPc2	tři
mocí	moc	k1gFnPc2	moc
<g/>
:	:	kIx,	:
zákonodárné	zákonodárný	k2eAgFnSc2d1	zákonodárná
<g/>
,	,	kIx,	,
výkonné	výkonný	k2eAgFnSc2d1	výkonná
a	a	k8xC	a
soudní	soudní	k2eAgFnSc2d1	soudní
<g/>
.	.	kIx.	.
</s>
<s>
Demokratický	demokratický	k2eAgInSc1d1	demokratický
<g/>
,	,	kIx,	,
svobodný	svobodný	k2eAgInSc1d1	svobodný
systém	systém	k1gInSc1	systém
věnuje	věnovat	k5eAaImIp3nS	věnovat
velkou	velký	k2eAgFnSc4d1	velká
pozornost	pozornost	k1gFnSc4	pozornost
ústavním	ústavní	k2eAgFnPc3d1	ústavní
pojistkám	pojistka	k1gFnPc3	pojistka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
omezují	omezovat	k5eAaImIp3nP	omezovat
moc	moc	k6eAd1	moc
státu	stát	k1gInSc2	stát
a	a	k8xC	a
zabraňují	zabraňovat	k5eAaImIp3nP	zabraňovat
zneužití	zneužití	k1gNnSc4	zneužití
moci	moct	k5eAaImF	moct
ve	v	k7c6	v
sféře	sféra	k1gFnSc6	sféra
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k6eAd1	moc
zákonodárná	zákonodárný	k2eAgFnSc1d1	zákonodárná
<g/>
,	,	kIx,	,
moc	moc	k6eAd1	moc
výkonná	výkonný	k2eAgFnSc1d1	výkonná
a	a	k8xC	a
moc	moc	k6eAd1	moc
soudní	soudní	k2eAgNnSc4d1	soudní
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
nezávislé	závislý	k2eNgInPc4d1	nezávislý
<g/>
,	,	kIx,	,
demokraticky	demokraticky	k6eAd1	demokraticky
kontrolovatelné	kontrolovatelný	k2eAgFnPc1d1	kontrolovatelná
a	a	k8xC	a
vzájemně	vzájemně	k6eAd1	vzájemně
vyvažované	vyvažovaný	k2eAgInPc1d1	vyvažovaný
do	do	k7c2	do
rovnováhy	rovnováha	k1gFnSc2	rovnováha
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
totalitních	totalitní	k2eAgInPc6d1	totalitní
režimech	režim	k1gInPc6	režim
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
jednoty	jednota	k1gFnPc4	jednota
moci	moc	k1gFnSc2	moc
-	-	kIx~	-
neexistuje	existovat	k5eNaImIp3nS	existovat
dělba	dělba	k1gFnSc1	dělba
na	na	k7c4	na
moc	moc	k1gFnSc4	moc
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
<g/>
,	,	kIx,	,
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
a	a	k8xC	a
soudní	soudní	k2eAgFnSc4d1	soudní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zákonodárná	zákonodárný	k2eAgFnSc1d1	zákonodárná
moc	moc	k1gFnSc1	moc
===	===	k?	===
</s>
</p>
<p>
<s>
Zákonodárná	zákonodárný	k2eAgFnSc1d1	zákonodárná
moc	moc	k1gFnSc1	moc
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
potestas	potestas	k1gInSc1	potestas
legislativa	legislativa	k1gFnSc1	legislativa
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
oprávnění	oprávnění	k1gNnSc4	oprávnění
vydávat	vydávat	k5eAaImF	vydávat
zákony	zákon	k1gInPc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
teorii	teorie	k1gFnSc6	teorie
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
vedle	vedle	k7c2	vedle
moci	moc	k1gFnSc2	moc
výkonné	výkonný	k2eAgFnSc2d1	výkonná
a	a	k8xC	a
soudní	soudní	k2eAgFnSc2d1	soudní
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nezávislých	závislý	k2eNgFnPc2d1	nezávislá
větví	větev	k1gFnPc2	větev
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
svěřená	svěřený	k2eAgFnSc1d1	svěřená
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
možných	možný	k2eAgFnPc2d1	možná
oblastí	oblast	k1gFnPc2	oblast
právní	právní	k2eAgFnSc2d1	právní
úpravy	úprava	k1gFnSc2	úprava
není	být	k5eNaImIp3nS	být
zákonodárce	zákonodárce	k1gMnSc1	zákonodárce
ničím	ničit	k5eAaImIp1nS	ničit
omezen	omezit	k5eAaPmNgMnS	omezit
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
tedy	tedy	k9	tedy
normovat	normovat	k5eAaBmF	normovat
kteroukoli	kterýkoli	k3yIgFnSc4	kterýkoli
oblast	oblast	k1gFnSc4	oblast
společenských	společenský	k2eAgInPc2d1	společenský
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
však	však	k9	však
přijatý	přijatý	k2eAgInSc1d1	přijatý
zákon	zákon	k1gInSc1	zákon
nebo	nebo	k8xC	nebo
některé	některý	k3yIgNnSc1	některý
jeho	jeho	k3xOp3gNnSc1	jeho
ustanovení	ustanovení	k1gNnSc1	ustanovení
odporuje	odporovat	k5eAaImIp3nS	odporovat
ústavnímu	ústavní	k2eAgInSc3d1	ústavní
řádu	řád	k1gInSc3	řád
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
jej	on	k3xPp3gNnSc4	on
zrušit	zrušit	k5eAaPmF	zrušit
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
moc	moc	k1gFnSc1	moc
===	===	k?	===
</s>
</p>
<p>
<s>
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
moc	moc	k1gFnSc1	moc
nebo	nebo	k8xC	nebo
též	též	k9	též
exekutiva	exekutiva	k1gFnSc1	exekutiva
je	být	k5eAaImIp3nS	být
složkou	složka	k1gFnSc7	složka
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Orgány	orgán	k1gInPc1	orgán
výkonné	výkonný	k2eAgFnSc2d1	výkonná
moci	moc	k1gFnSc2	moc
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
prezident	prezident	k1gMnSc1	prezident
či	či	k8xC	či
panovník	panovník	k1gMnSc1	panovník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
formy	forma	k1gFnSc2	forma
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
detailech	detail	k1gInPc6	detail
pravomoc	pravomoc	k1gFnSc1	pravomoc
a	a	k8xC	a
složení	složení	k1gNnSc1	složení
exekutivy	exekutiva	k1gFnSc2	exekutiva
měnit	měnit	k5eAaImF	měnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Soudní	soudní	k2eAgFnSc4d1	soudní
moc	moc	k1gFnSc4	moc
===	===	k?	===
</s>
</p>
<p>
<s>
Soudní	soudní	k2eAgFnSc1d1	soudní
moc	moc	k1gFnSc1	moc
nebo	nebo	k8xC	nebo
též	též	k9	též
judikativa	judikativum	k1gNnSc2	judikativum
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
složek	složka	k1gFnPc2	složka
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
ji	on	k3xPp3gFnSc4	on
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
soudy	soud	k1gInPc4	soud
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
zákonem	zákon	k1gInSc7	zákon
stanoveným	stanovený	k2eAgInSc7d1	stanovený
způsobem	způsob	k1gInSc7	způsob
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
v	v	k7c6	v
občanskoprávním	občanskoprávní	k2eAgNnSc6d1	občanskoprávní
řízení	řízení	k1gNnSc6	řízení
ochranu	ochrana	k1gFnSc4	ochrana
subjektivních	subjektivní	k2eAgNnPc2d1	subjektivní
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
v	v	k7c6	v
trestním	trestní	k2eAgNnSc6d1	trestní
řízení	řízení	k1gNnSc6	řízení
rozhodují	rozhodovat	k5eAaImIp3nP	rozhodovat
o	o	k7c6	o
vině	vina	k1gFnSc6	vina
a	a	k8xC	a
trestu	trest	k1gInSc6	trest
za	za	k7c4	za
trestné	trestný	k2eAgInPc4d1	trestný
činy	čin	k1gInPc4	čin
<g/>
,	,	kIx,	,
ve	v	k7c6	v
správním	správní	k2eAgNnSc6d1	správní
soudnictví	soudnictví	k1gNnSc6	soudnictví
přezkoumávají	přezkoumávat	k5eAaImIp3nP	přezkoumávat
akty	akt	k1gInPc4	akt
orgánů	orgán	k1gInPc2	orgán
veřejné	veřejný	k2eAgFnSc2d1	veřejná
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
v	v	k7c6	v
ústavním	ústavní	k2eAgNnSc6d1	ústavní
soudnictví	soudnictví	k1gNnSc6	soudnictví
rozhodují	rozhodovat	k5eAaImIp3nP	rozhodovat
o	o	k7c6	o
souladu	soulad	k1gInSc6	soulad
právních	právní	k2eAgInPc2d1	právní
předpisů	předpis	k1gInPc2	předpis
i	i	k8xC	i
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
s	s	k7c7	s
ústavou	ústava	k1gFnSc7	ústava
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
rozhodují	rozhodovat	k5eAaImIp3nP	rozhodovat
i	i	k9	i
o	o	k7c6	o
dalších	další	k2eAgFnPc6d1	další
věcech	věc	k1gFnPc6	věc
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
jim	on	k3xPp3gMnPc3	on
zákonem	zákon	k1gInSc7	zákon
svěřeny	svěřit	k5eAaPmNgFnP	svěřit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
rozhodování	rozhodování	k1gNnSc6	rozhodování
je	být	k5eAaImIp3nS	být
soudce	soudce	k1gMnSc1	soudce
vázán	vázat	k5eAaImNgMnS	vázat
jen	jen	k9	jen
zákony	zákon	k1gInPc1	zákon
a	a	k8xC	a
prameny	pramen	k1gInPc1	pramen
práva	právo	k1gNnSc2	právo
vyšší	vysoký	k2eAgFnSc2d2	vyšší
právní	právní	k2eAgFnSc2d1	právní
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Soudce	soudce	k1gMnSc1	soudce
nesmí	smět	k5eNaImIp3nS	smět
mít	mít	k5eAaImF	mít
žádný	žádný	k3yNgInSc4	žádný
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
věci	věc	k1gFnSc3	věc
<g/>
,	,	kIx,	,
účastníkům	účastník	k1gMnPc3	účastník
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc3	jejich
zástupcům	zástupce	k1gMnPc3	zástupce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Stát	stát	k1gInSc1	stát
</s>
</p>
<p>
<s>
Svrchovanost	svrchovanost	k1gFnSc1	svrchovanost
</s>
</p>
