<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
druh	druh	k1gInSc1	druh
veřejné	veřejný	k2eAgFnSc2d1	veřejná
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
znamenající	znamenající	k2eAgInSc1d1	znamenající
schopnost	schopnost	k1gFnSc4	schopnost
státu	stát	k1gInSc2	stát
vnutit	vnutit	k5eAaPmF	vnutit
svou	svůj	k3xOyFgFnSc4	svůj
vůli	vůle	k1gFnSc4	vůle
subjektům	subjekt	k1gInPc3	subjekt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
dosahu	dosah	k1gInSc6	dosah
právního	právní	k2eAgInSc2d1	právní
řádu	řád	k1gInSc2	řád
<g/>
?	?	kIx.	?
</s>
