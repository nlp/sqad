<p>
<s>
Faerština	Faerština	k1gFnSc1	Faerština
(	(	kIx(	(
<g/>
Fø	Fø	k1gFnSc1	Fø
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
severogermánský	severogermánský	k2eAgInSc1d1	severogermánský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
mluví	mluvit	k5eAaImIp3nS	mluvit
asi	asi	k9	asi
45	[number]	k4	45
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
Faerských	Faerský	k2eAgInPc2d1	Faerský
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
asi	asi	k9	asi
21	[number]	k4	21
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
oblastech	oblast	k1gFnPc6	oblast
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
poměrně	poměrně	k6eAd1	poměrně
blízko	blízko	k6eAd1	blízko
k	k	k7c3	k
islandštině	islandština	k1gFnSc3	islandština
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ona	onen	k3xDgFnSc1	onen
přežila	přežít	k5eAaPmAgFnS	přežít
na	na	k7c6	na
izolovaných	izolovaný	k2eAgInPc6d1	izolovaný
ostrovech	ostrov	k1gInPc6	ostrov
bez	bez	k7c2	bez
podstatných	podstatný	k2eAgFnPc2d1	podstatná
změn	změna	k1gFnPc2	změna
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
až	až	k6eAd1	až
do	do	k7c2	do
dneška	dnešek	k1gInSc2	dnešek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Mimo	mimo	k7c4	mimo
Faery	Faer	k1gMnPc4	Faer
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
minimálně	minimálně	k6eAd1	minimálně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Faerách	Faera	k1gFnPc6	Faera
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Dánska	Dánsko	k1gNnSc2	Dánsko
širokou	široký	k2eAgFnSc4d1	široká
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgInSc7	první
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
<g/>
;	;	kIx,	;
druhým	druhý	k4xOgNnSc7	druhý
je	on	k3xPp3gMnPc4	on
dánština	dánština	k1gFnSc1	dánština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
ryze	ryze	k6eAd1	ryze
faerské	faerský	k2eAgInPc4d1	faerský
texty	text	k1gInPc4	text
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgFnPc1	tři
balady	balada	k1gFnPc1	balada
zaznamenané	zaznamenaný	k2eAgFnPc1d1	zaznamenaná
roku	rok	k1gInSc2	rok
1773	[number]	k4	1773
<g/>
.	.	kIx.	.
</s>
<s>
Spisovnou	spisovný	k2eAgFnSc4d1	spisovná
normu	norma	k1gFnSc4	norma
sestavil	sestavit	k5eAaPmAgMnS	sestavit
roku	rok	k1gInSc2	rok
1846	[number]	k4	1846
Venceslaus	Venceslaus	k1gMnSc1	Venceslaus
Ulricus	Ulricus	k1gMnSc1	Ulricus
Hammershaimb	Hammershaimb	k1gMnSc1	Hammershaimb
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
také	také	k9	také
začaly	začít	k5eAaPmAgFnP	začít
snahy	snaha	k1gFnPc1	snaha
o	o	k7c4	o
zrovnoprávnění	zrovnoprávnění	k1gNnSc4	zrovnoprávnění
faerštiny	faerština	k1gFnSc2	faerština
s	s	k7c7	s
dánštinou	dánština	k1gFnSc7	dánština
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
školství	školství	k1gNnSc2	školství
začala	začít	k5eAaPmAgFnS	začít
faerština	faerština	k1gFnSc1	faerština
pronikat	pronikat	k5eAaImF	pronikat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
školách	škola	k1gFnPc6	škola
prvním	první	k4xOgInSc7	první
vyučovacím	vyučovací	k2eAgInSc7d1	vyučovací
jazykem	jazyk	k1gInSc7	jazyk
a	a	k8xC	a
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
literární	literární	k2eAgInSc4d1	literární
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Abeceda	abeceda	k1gFnSc1	abeceda
a	a	k8xC	a
výslovnost	výslovnost	k1gFnSc1	výslovnost
==	==	k?	==
</s>
</p>
<p>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
latinka	latinka	k1gFnSc1	latinka
obohacená	obohacený	k2eAgFnSc1d1	obohacená
o	o	k7c4	o
několik	několik	k4yIc4	několik
speciálních	speciální	k2eAgInPc2d1	speciální
znaků	znak	k1gInPc2	znak
podle	podle	k7c2	podle
norsko-islandského	norskoslandský	k2eAgInSc2d1	norsko-islandský
vzoru	vzor	k1gInSc2	vzor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hláskosloví	hláskosloví	k1gNnSc1	hláskosloví
stojí	stát	k5eAaImIp3nS	stát
mezi	mezi	k7c7	mezi
islandštinou	islandština	k1gFnSc7	islandština
a	a	k8xC	a
západonorskými	západonorský	k2eAgInPc7d1	západonorský
dialekty	dialekt	k1gInPc7	dialekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Æ	Æ	k?	Æ
-	-	kIx~	-
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
norštiny	norština	k1gFnSc2	norština
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
krátké	krátká	k1gFnSc2	krátká
<g/>
,	,	kIx,	,
čte	číst	k5eAaImIp3nS	číst
se	se	k3xPyFc4	se
jako	jako	k9	jako
A	a	k9	a
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
<g/>
,	,	kIx,	,
čte	číst	k5eAaImIp3nS	číst
se	se	k3xPyFc4	se
jako	jako	k9	jako
EA	EA	kA	EA
<g/>
.	.	kIx.	.
</s>
<s>
Unicode	Unicod	k1gMnSc5	Unicod
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
C	C	kA	C
<g/>
6	[number]	k4	6
velké	velký	k2eAgInPc1d1	velký
<g/>
,	,	kIx,	,
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
E	E	kA	E
<g/>
6	[number]	k4	6
malé	malý	k2eAgInPc1d1	malý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ø	Ø	k?	Ø
-	-	kIx~	-
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
norštiny	norština	k1gFnSc2	norština
<g/>
,	,	kIx,	,
čte	číst	k5eAaImIp3nS	číst
se	se	k3xPyFc4	se
jako	jako	k9	jako
německé	německý	k2eAgFnSc3d1	německá
přehlasované	přehlasovaný	k2eAgFnSc3d1	přehlasovaná
Ö.	Ö.	k1gFnSc3	Ö.
Unicode	Unicod	k1gInSc5	Unicod
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
D	D	kA	D
<g/>
8	[number]	k4	8
velké	velký	k2eAgInPc1d1	velký
<g/>
,	,	kIx,	,
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
F	F	kA	F
<g/>
8	[number]	k4	8
malé	malý	k2eAgInPc1d1	malý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ð	Ð	k?	Ð
-	-	kIx~	-
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
islandštiny	islandština	k1gFnSc2	islandština
a	a	k8xC	a
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
nevyslovuje	vyslovovat	k5eNaImIp3nS	vyslovovat
<g/>
.	.	kIx.	.
</s>
<s>
Ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
ale	ale	k9	ale
předchozí	předchozí	k2eAgFnSc2d1	předchozí
a	a	k8xC	a
následné	následný	k2eAgFnSc2d1	následná
samohlásky	samohláska	k1gFnSc2	samohláska
a	a	k8xC	a
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
slyšet	slyšet	k5eAaImF	slyšet
buď	buď	k8xC	buď
J	J	kA	J
nebo	nebo	k8xC	nebo
V.	V.	kA	V.
Unicode	Unicod	k1gInSc5	Unicod
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
D	D	kA	D
<g/>
0	[number]	k4	0
velké	velký	k2eAgInPc1d1	velký
<g/>
,	,	kIx,	,
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
F	F	kA	F
<g/>
0	[number]	k4	0
malé	malá	k1gFnPc1	malá
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Neplést	plést	k5eNaImF	plést
s	s	k7c7	s
chorvatským	chorvatský	k2eAgMnSc7d1	chorvatský
Đđ	Đđ	k1gMnSc7	Đđ
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
velké	velká	k1gFnPc1	velká
se	se	k3xPyFc4	se
opticky	opticky	k6eAd1	opticky
shoduje	shodovat	k5eAaImIp3nS	shodovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
malé	malý	k2eAgInPc1d1	malý
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
odlišné	odlišný	k2eAgInPc1d1	odlišný
kódy	kód	k1gInPc1	kód
<g/>
:	:	kIx,	:
velké	velká	k1gFnPc1	velká
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
110	[number]	k4	110
<g/>
,	,	kIx,	,
malé	malý	k2eAgNnSc1d1	malé
U	u	k7c2	u
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
111	[number]	k4	111
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
Dlouhé	Dlouhé	k2eAgFnPc1d1	Dlouhé
samohlásky	samohláska	k1gFnPc1	samohláska
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
přízvučné	přízvučný	k2eAgFnSc6d1	přízvučná
pozici	pozice	k1gFnSc6	pozice
<g/>
,	,	kIx,	,
když	když	k8xS	když
<g />
.	.	kIx.	.
</s>
<s>
za	za	k7c7	za
nimi	on	k3xPp3gInPc7	on
následuje	následovat	k5eAaImIp3nS	následovat
max	max	kA	max
jedna	jeden	k4xCgFnSc1	jeden
souhláska	souhláska	k1gFnSc1	souhláska
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
skupiny	skupina	k1gFnPc1	skupina
hlásek	hláska	k1gFnPc2	hláska
PL	PL	kA	PL
<g/>
,	,	kIx,	,
KL	kl	kA	kl
<g/>
,	,	kIx,	,
PR	pr	k0	pr
<g/>
,	,	kIx,	,
TR	TR	kA	TR
<g/>
,	,	kIx,	,
KR	KR	kA	KR
<g/>
,	,	kIx,	,
TJ	tj	kA	tj
<g/>
,	,	kIx,	,
KJ	KJ	kA	KJ
<g/>
,	,	kIx,	,
SJ	SJ	kA	SJ
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Faerských	Faerský	k2eAgInPc2d1	Faerský
ostrovů	ostrov	k1gInPc2	ostrov
se	se	k3xPyFc4	se
před	před	k7c7	před
TJ	tj	kA	tj
a	a	k8xC	a
SJ	SJ	kA	SJ
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
samohlásky	samohláska	k1gFnSc2	samohláska
krátce	krátce	k6eAd1	krátce
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
G	G	kA	G
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
jako	jako	k9	jako
dž	dž	k?	dž
před	před	k7c7	před
E	E	kA	E
<g/>
,	,	kIx,	,
I	I	kA	I
<g/>
,	,	kIx,	,
J	J	kA	J
<g/>
,	,	kIx,	,
Y	Y	kA	Y
a	a	k8xC	a
EY	EY	kA	EY
(	(	kIx(	(
<g/>
platí	platit	k5eAaImIp3nS	platit
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
po	po	k7c6	po
samohlásce	samohláska	k1gFnSc6	samohláska
se	se	k3xPyFc4	se
semostatné	semostatný	k2eAgFnPc4d1	semostatný
G	G	kA	G
většinou	většina	k1gFnSc7	většina
nevyslovuje	vyslovovat	k5eNaImIp3nS	vyslovovat
vůbec	vůbec	k9	vůbec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
K	K	kA	K
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
jako	jako	k9	jako
č	č	k0	č
před	před	k7c7	před
'	'	kIx"	'
<g/>
E	E	kA	E
<g/>
,	,	kIx,	,
I	I	kA	I
<g/>
,	,	kIx,	,
J	J	kA	J
<g/>
,	,	kIx,	,
Y	Y	kA	Y
a	a	k8xC	a
EY	EY	kA	EY
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
SKJ	SKJ	kA	SKJ
a	a	k8xC	a
SK	Sk	kA	Sk
se	se	k3xPyFc4	se
čtou	číst	k5eAaImIp3nP	číst
jako	jako	k8xS	jako
š	š	k?	š
před	před	k7c7	před
'	'	kIx"	'
<g/>
E	E	kA	E
<g/>
,	,	kIx,	,
I	I	kA	I
<g/>
,	,	kIx,	,
Y	Y	kA	Y
a	a	k8xC	a
EY	EY	kA	EY
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
DJ	DJ	kA	DJ
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
jako	jako	k9	jako
dž	dž	k?	dž
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Faerských	Faerský	k2eAgInPc2d1	Faerský
ostrovů	ostrov	k1gInPc2	ostrov
dj	dj	k?	dj
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
STJ	STJ	kA	STJ
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
čte	číst	k5eAaImIp3nS	číst
jako	jako	k9	jako
š	š	k?	š
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
(	(	kIx(	(
<g/>
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Faerských	Faerský	k2eAgInPc2d1	Faerský
ostrovů	ostrov	k1gInPc2	ostrov
vždy	vždy	k6eAd1	vždy
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
stj	stj	k?	stj
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
TJ	tj	kA	tj
a	a	k8xC	a
HJ	HJ	kA	HJ
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
čtou	číst	k5eAaImIp3nP	číst
jako	jako	k8xS	jako
č	č	k0	č
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
TJ	tj	kA	tj
<g/>
'	'	kIx"	'
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Faerských	Faerský	k2eAgInPc2d1	Faerský
ostrovů	ostrov	k1gInPc2	ostrov
vždy	vždy	k6eAd1	vždy
tj	tj	kA	tj
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
EI	EI	kA	EI
se	se	k3xPyFc4	se
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Faerských	Faerský	k2eAgInPc2d1	Faerský
ostrovů	ostrov	k1gInPc2	ostrov
čte	číst	k5eAaImIp3nS	číst
jako	jako	k9	jako
aj	aj	kA	aj
<g/>
/	/	kIx~	/
<g/>
aj	aj	kA	aj
<g/>
,	,	kIx,	,
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
jako	jako	k8xS	jako
oj	oj	k1gFnSc1	oj
<g/>
/	/	kIx~	/
<g/>
oj	oj	k1gFnSc1	oj
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
'	'	kIx"	'
<g/>
OY	OY	kA	OY
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
-	-	kIx~	-
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
samotného	samotný	k2eAgNnSc2d1	samotné
'	'	kIx"	'
<g/>
E	E	kA	E
<g/>
'	'	kIx"	'
nebo	nebo	k8xC	nebo
digrafu	digraf	k1gMnSc3	digraf
'	'	kIx"	'
<g/>
EY	EY	kA	EY
<g/>
'	'	kIx"	'
neměkčí	měkčit	k5eNaImIp3nS	měkčit
předcházející	předcházející	k2eAgFnSc1d1	předcházející
'	'	kIx"	'
<g/>
K	K	kA	K
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
,	,	kIx,	,
SK	Sk	kA	Sk
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
EY	EY	kA	EY
se	se	k3xPyFc4	se
v	v	k7c6	v
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
výslovnosti	výslovnost	k1gFnSc6	výslovnost
čte	číst	k5eAaImIp3nS	číst
ej	ej	k0	ej
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
jako	jako	k8xC	jako
e	e	k0	e
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Æ	Æ	k?	Æ
se	se	k3xPyFc4	se
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Faerských	Faerský	k2eAgInPc2d1	Faerský
ostrovů	ostrov	k1gInPc2	ostrov
čte	číst	k5eAaImIp3nS	číst
shodně	shodně	k6eAd1	shodně
jako	jako	k9	jako
'	'	kIx"	'
<g/>
E	E	kA	E
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Ó	Ó	kA	Ó
se	se	k3xPyFc4	se
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Faerských	Faerský	k2eAgInPc2d1	Faerský
ostrovů	ostrov	k1gInPc2	ostrov
čte	číst	k5eAaImIp3nS	číst
jako	jako	k9	jako
ou	ou	k0	ou
<g/>
/	/	kIx~	/
<g/>
o	o	k7c6	o
<g/>
,	,	kIx,	,
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Tórshavnu	Tórshavna	k1gFnSc4	Tórshavna
jako	jako	k8xS	jako
ou	ou	k0	ou
<g/>
/	/	kIx~	/
<g/>
ö	ö	k?	ö
a	a	k8xC	a
na	na	k7c6	na
severu	sever	k1gInSc6	sever
většinou	většina	k1gFnSc7	většina
jako	jako	k8xC	jako
öu	öu	k?	öu
(	(	kIx(	(
<g/>
eu	eu	k?	eu
<g/>
)	)	kIx)	)
<g/>
/	/	kIx~	/
<g/>
ö	ö	k?	ö
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Ð	Ð	k?	Ð
se	se	k3xPyFc4	se
nevyslovuje	vyslovovat	k5eNaImIp3nS	vyslovovat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
i	i	k8xC	i
G	G	kA	G
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
po	po	k7c6	po
samohlásce	samohláska	k1gFnSc6	samohláska
-	-	kIx~	-
místo	místo	k7c2	místo
nich	on	k3xPp3gInPc2	on
se	se	k3xPyFc4	se
vkládá	vkládat	k5eAaImIp3nS	vkládat
hiátové	hiátový	k2eAgNnSc1d1	hiátový
j	j	k?	j
nebo	nebo	k8xC	nebo
v	v	k7c6	v
(	(	kIx(	(
<g/>
w	w	k?	w
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
tato	tento	k3xDgNnPc1	tento
písmena	písmeno	k1gNnPc1	písmeno
nevyslovují	vyslovovat	k5eNaImIp3nP	vyslovovat
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
LL	LL	kA	LL
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
dl	dl	k?	dl
(	(	kIx(	(
<g/>
výjimkou	výjimka	k1gFnSc7	výjimka
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
málo	málo	k6eAd1	málo
zejména	zejména	k9	zejména
přejatých	přejatý	k2eAgNnPc2d1	přejaté
slov	slovo	k1gNnPc2	slovo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
RN	RN	kA	RN
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
čte	číst	k5eAaImIp3nS	číst
jako	jako	k9	jako
dn	dn	k?	dn
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
i	i	k8xC	i
NN	NN	kA	NN
po	po	k7c6	po
EI	EI	kA	EI
a	a	k8xC	a
OY	OY	kA	OY
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
RS	RS	kA	RS
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
jako	jako	k9	jako
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
cerebrální	cerebrální	k2eAgNnSc4d1	cerebrální
š	š	k?	š
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
S	s	k7c7	s
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
Í	Í	kA	Í
<g/>
,	,	kIx,	,
Ý	Ý	kA	Ý
<g/>
,	,	kIx,	,
EI	EI	kA	EI
a	a	k8xC	a
OY	OY	kA	OY
a	a	k8xC	a
další	další	k2eAgFnSc7d1	další
souhláskou	souhláska	k1gFnSc7	souhláska
čte	číst	k5eAaImIp3nS	číst
jako	jako	k9	jako
š	š	k?	š
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
j	j	k?	j
ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
zaniká	zanikat	k5eAaImIp3nS	zanikat
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Í	Í	kA	Í
<g/>
,	,	kIx,	,
Ý	Ý	kA	Ý
<g/>
,	,	kIx,	,
EI	EI	kA	EI
<g/>
,	,	kIx,	,
OY	OY	kA	OY
před	před	k7c7	před
měkkým	měkký	k2eAgInSc7d1	měkký
NK	NK	kA	NK
<g/>
(	(	kIx(	(
<g/>
J	J	kA	J
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
NG	NG	kA	NG
<g/>
(	(	kIx(	(
<g/>
J	J	kA	J
<g/>
)	)	kIx)	)
a	a	k8xC	a
GG	GG	kA	GG
<g/>
(	(	kIx(	(
<g/>
J	J	kA	J
<g/>
)	)	kIx)	)
ztrácejí	ztrácet	k5eAaImIp3nP	ztrácet
druhý	druhý	k4xOgInSc4	druhý
komponent	komponent	k1gInSc4	komponent
dvojhlásky	dvojhláska	k1gFnSc2	dvojhláska
-	-	kIx~	-
u	u	k7c2	u
<g/>
,	,	kIx,	,
u	u	k7c2	u
<g/>
,	,	kIx,	,
a	a	k8xC	a
<g/>
,	,	kIx,	,
o	o	k0	o
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Ó	Ó	kA	Ó
a	a	k8xC	a
Ú	Ú	kA	Ú
se	se	k3xPyFc4	se
před	před	k7c4	před
GV	GV	kA	GV
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
jako	jako	k8xS	jako
e	e	k0	e
<g/>
,	,	kIx,	,
i	i	k9	i
(	(	kIx(	(
<g/>
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Faerských	Faerský	k2eAgInPc2d1	Faerský
ostrovů	ostrov	k1gInPc2	ostrov
ÓGV	ÓGV	kA	ÓGV
=	=	kIx~	=
ogv	ogv	k?	ogv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
A	a	k9	a
se	se	k3xPyFc4	se
před	před	k7c4	před
NK	NK	kA	NK
<g/>
,	,	kIx,	,
NG	NG	kA	NG
čte	číst	k5eAaImIp3nS	číst
e	e	k0	e
(	(	kIx(	(
<g/>
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Faerských	Faerský	k2eAgInPc2d1	Faerský
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
vyslovované	vyslovovaný	k2eAgNnSc1d1	vyslovované
e	e	k0	e
neměkčí	měkčit	k5eNaImIp3nP	měkčit
předchozí	předchozí	k2eAgMnSc1d1	předchozí
'	'	kIx"	'
<g/>
K	K	kA	K
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
,	,	kIx,	,
SK	Sk	kA	Sk
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Gramatika	gramatika	k1gFnSc1	gramatika
==	==	k?	==
</s>
</p>
<p>
<s>
Gramatická	gramatický	k2eAgFnSc1d1	gramatická
stavba	stavba	k1gFnSc1	stavba
a	a	k8xC	a
slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
ovlivněná	ovlivněný	k2eAgFnSc1d1	ovlivněná
islandštinou	islandština	k1gFnSc7	islandština
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
i	i	k9	i
řada	řada	k1gFnSc1	řada
rysů	rys	k1gMnPc2	rys
dialektů	dialekt	k1gInPc2	dialekt
jihozápadního	jihozápadní	k2eAgNnSc2d1	jihozápadní
Norska	Norsko	k1gNnSc2	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Faerština	Faerština	k1gFnSc1	Faerština
je	být	k5eAaImIp3nS	být
flexivní	flexivní	k2eAgInSc4d1	flexivní
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
3	[number]	k4	3
gramatické	gramatický	k2eAgInPc4d1	gramatický
rody	rod	k1gInPc4	rod
a	a	k8xC	a
4	[number]	k4	4
pády	pád	k1gInPc7	pád
-	-	kIx~	-
nominativ	nominativ	k1gInSc1	nominativ
<g/>
,	,	kIx,	,
akuzativ	akuzativ	k1gInSc1	akuzativ
<g/>
,	,	kIx,	,
dativ	dativ	k1gInSc1	dativ
a	a	k8xC	a
genitiv	genitiv	k1gInSc1	genitiv
<g/>
.	.	kIx.	.
</s>
<s>
Genitiv	genitiv	k1gInSc1	genitiv
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
mluvené	mluvený	k2eAgFnSc6d1	mluvená
řeči	řeč	k1gFnSc6	řeč
nepoužívá	používat	k5eNaImIp3nS	používat
a	a	k8xC	a
opisuje	opisovat	k5eAaImIp3nS	opisovat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Gramatika	gramatika	k1gFnSc1	gramatika
jazyků	jazyk	k1gInPc2	jazyk
ostrovní	ostrovní	k2eAgFnSc2d1	ostrovní
větve	větev	k1gFnSc2	větev
severogermánských	severogermánský	k2eAgInPc2d1	severogermánský
jazyků	jazyk	k1gInPc2	jazyk
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
islandština	islandština	k1gFnSc1	islandština
a	a	k8xC	a
právě	právě	k9	právě
faerština	faerština	k1gFnSc1	faerština
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejsložitějších	složitý	k2eAgFnPc2d3	nejsložitější
gramatik	gramatika	k1gFnPc2	gramatika
mezi	mezi	k7c7	mezi
germánskými	germánský	k2eAgInPc7d1	germánský
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
germánských	germánský	k2eAgMnPc2d1	germánský
jazyků	jazyk	k1gInPc2	jazyk
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
analytických	analytický	k2eAgInPc2d1	analytický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
slovníku	slovník	k1gInSc6	slovník
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
dánských	dánský	k2eAgFnPc2d1	dánská
výpůjček	výpůjčka	k1gFnPc2	výpůjčka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
příslušností	příslušnost	k1gFnSc7	příslušnost
Faerských	Faerský	k2eAgInPc2d1	Faerský
ostrovů	ostrov	k1gInPc2	ostrov
k	k	k7c3	k
Dánsku	Dánsko	k1gNnSc3	Dánsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Osobní	osobní	k2eAgNnPc1d1	osobní
zájmena	zájmeno	k1gNnPc1	zájmeno
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Faerské	Faerský	k2eAgFnSc2d1	Faerská
číslovky	číslovka	k1gFnSc2	číslovka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Sloveso	sloveso	k1gNnSc4	sloveso
VERA	VERA	kA	VERA
-	-	kIx~	-
být	být	k5eAaImF	být
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Sloveso	sloveso	k1gNnSc4	sloveso
HAVA	HAVA	kA	HAVA
-	-	kIx~	-
mít	mít	k5eAaImF	mít
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Příbuznost	příbuznost	k1gFnSc4	příbuznost
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
jazyky	jazyk	k1gInPc7	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
Faerština	Faerština	k1gFnSc1	Faerština
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
germánský	germánský	k2eAgInSc4d1	germánský
jazyk	jazyk	k1gInSc4	jazyk
součástí	součást	k1gFnPc2	součást
velké	velký	k2eAgFnSc2d1	velká
skupiny	skupina	k1gFnSc2	skupina
indoevropských	indoevropský	k2eAgInPc2d1	indoevropský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Faerštině	Faerština	k1gFnSc6	Faerština
nejpodobnějším	podobný	k2eAgInSc7d3	nejpodobnější
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
islandština	islandština	k1gFnSc1	islandština
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
sdílí	sdílet	k5eAaImIp3nS	sdílet
starobylý	starobylý	k2eAgInSc1d1	starobylý
charakter	charakter	k1gInSc1	charakter
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
nijak	nijak	k6eAd1	nijak
výrazně	výrazně	k6eAd1	výrazně
nezměnila	změnit	k5eNaPmAgFnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Islanďané	Islanďan	k1gMnPc1	Islanďan
a	a	k8xC	a
Faeřané	Faeřan	k1gMnPc1	Faeřan
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
domluvit	domluvit	k5eAaPmF	domluvit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dáno	dát	k5eAaPmNgNnS	dát
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
i	i	k8xC	i
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
výslovnost	výslovnost	k1gFnSc1	výslovnost
obou	dva	k4xCgInPc2	dva
jazyků	jazyk	k1gInPc2	jazyk
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
příklad	příklad	k1gInSc4	příklad
lze	lze	k6eAd1	lze
uvést	uvést	k5eAaPmF	uvést
předložku	předložka	k1gFnSc4	předložka
hjá	hjá	k?	hjá
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
islandštině	islandština	k1gFnSc6	islandština
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
přibližně	přibližně	k6eAd1	přibližně
jako	jako	k9	jako
[	[	kIx(	[
<g/>
hjau	hja	k1gMnSc6	hja
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ve	v	k7c6	v
faerštině	faerština	k1gFnSc6	faerština
jako	jako	k9	jako
[	[	kIx(	[
<g/>
čoa	čoa	k?	čoa
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzorový	vzorový	k2eAgInSc1d1	vzorový
text	text	k1gInSc1	text
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
deklarace	deklarace	k1gFnSc1	deklarace
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
faerštiny	faerština	k1gFnSc2	faerština
s	s	k7c7	s
islandštinou	islandština	k1gFnSc7	islandština
je	být	k5eAaImIp3nS	být
text	text	k1gInSc1	text
uveden	uveden	k2eAgInSc1d1	uveden
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
jazyků	jazyk	k1gInPc2	jazyk
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
písem	písmo	k1gNnPc2	písmo
</s>
</p>
<p>
<s>
Faerský	Faerský	k2eAgInSc1d1	Faerský
jazykový	jazykový	k2eAgInSc1d1	jazykový
výbor	výbor	k1gInSc1	výbor
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
faerština	faerština	k1gFnSc1	faerština
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
faerština	faerština	k1gFnSc1	faerština
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Faerština	Faerština	k1gFnSc1	Faerština
na	na	k7c4	na
Omniglotu	Omniglota	k1gFnSc4	Omniglota
</s>
</p>
<p>
<s>
Faersko-anglický	Faerskonglický	k2eAgInSc1d1	Faersko-anglický
slovník	slovník	k1gInSc1	slovník
z	z	k7c2	z
Webster	Webstra	k1gFnPc2	Webstra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Online	Onlin	k1gInSc5	Onlin
Dictionary	Dictionara	k1gFnSc2	Dictionara
–	–	k?	–
the	the	k?	the
Rosetta	Rosetto	k1gNnPc1	Rosetto
Edition	Edition	k1gInSc1	Edition
</s>
</p>
<p>
<s>
Fonologie	fonologie	k1gFnSc1	fonologie
faerštiny	faerština	k1gFnSc2	faerština
</s>
</p>
