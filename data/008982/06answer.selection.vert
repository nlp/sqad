<s>
Faerština	Faerština	k1gFnSc1	Faerština
(	(	kIx(	(
<g/>
Fø	Fø	k1gFnSc1	Fø
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
severogermánský	severogermánský	k2eAgInSc1d1	severogermánský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
mluví	mluvit	k5eAaImIp3nS	mluvit
asi	asi	k9	asi
45	[number]	k4	45
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
Faerských	Faerský	k2eAgInPc2d1	Faerský
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
asi	asi	k9	asi
21	[number]	k4	21
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
oblastech	oblast	k1gFnPc6	oblast
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
