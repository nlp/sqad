<s>
Clona	clona	k1gFnSc1	clona
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
optice	optika	k1gFnSc6	optika
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
reguluje	regulovat	k5eAaImIp3nS	regulovat
nebo	nebo	k8xC	nebo
omezuje	omezovat	k5eAaImIp3nS	omezovat
množství	množství	k1gNnSc1	množství
světla	světlo	k1gNnSc2	světlo
procházejícího	procházející	k2eAgInSc2d1	procházející
objektivem	objektiv	k1gInSc7	objektiv
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
(	(	kIx(	(
<g/>
dalekohledu	dalekohled	k1gInSc2	dalekohled
<g/>
,	,	kIx,	,
mikroskopu	mikroskop	k1gInSc2	mikroskop
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
Je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
mezi	mezi	k7c7	mezi
čočkami	čočka	k1gFnPc7	čočka
tak	tak	k9	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
neomezovala	omezovat	k5eNaImAgFnS	omezovat
zorné	zorný	k2eAgNnSc4d1	zorné
pole	pole	k1gNnSc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pevná	pevný	k2eAgFnSc1d1	pevná
nebo	nebo	k8xC	nebo
proměnná	proměnná	k1gFnSc1	proměnná
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Proměnná	proměnný	k2eAgFnSc1d1	proměnná
clona	clona	k1gFnSc1	clona
funguje	fungovat	k5eAaImIp3nS	fungovat
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
lidská	lidský	k2eAgFnSc1d1	lidská
oční	oční	k2eAgFnSc1d1	oční
zornice	zornice	k1gFnSc1	zornice
a	a	k8xC	a
kontroluje	kontrolovat	k5eAaImIp3nS	kontrolovat
množství	množství	k1gNnSc1	množství
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dopadá	dopadat	k5eAaImIp3nS	dopadat
na	na	k7c4	na
fotocitlivý	fotocitlivý	k2eAgInSc4d1	fotocitlivý
materiál	materiál	k1gInSc4	materiál
nebo	nebo	k8xC	nebo
obrazový	obrazový	k2eAgInSc4d1	obrazový
snímač	snímač	k1gInSc4	snímač
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
clonu	clona	k1gFnSc4	clona
Noël	Noël	k1gMnSc1	Noël
Marie	Maria	k1gFnSc2	Maria
Paymal	Paymal	k1gMnSc1	Paymal
Lerebours	Lerebours	k1gInSc1	Lerebours
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1855	[number]	k4	1855
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
otočnou	otočný	k2eAgFnSc4d1	otočná
clonu	clona	k1gFnSc4	clona
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
různými	různý	k2eAgInPc7d1	různý
otvory	otvor	k1gInPc7	otvor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
nastavovaly	nastavovat	k5eAaImAgInP	nastavovat
na	na	k7c6	na
přední	přední	k2eAgFnSc6d1	přední
straně	strana	k1gFnSc6	strana
objektivu	objektiv	k1gInSc2	objektiv
<g/>
.	.	kIx.	.
</s>
<s>
Lamelovou	lamelový	k2eAgFnSc4d1	lamelová
clonu	clona	k1gFnSc4	clona
snad	snad	k9	snad
vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
francouzský	francouzský	k2eAgMnSc1d1	francouzský
vynálezce	vynálezce	k1gMnSc1	vynálezce
a	a	k8xC	a
průkopník	průkopník	k1gMnSc1	průkopník
fotografie	fotografia	k1gFnSc2	fotografia
Nicéphore	Nicéphor	k1gInSc5	Nicéphor
Niépce	Niépce	k1gFnSc5	Niépce
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
clony	clona	k1gFnSc2	clona
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
pomocí	pomocí	k7c2	pomocí
clonového	clonový	k2eAgNnSc2d1	clonové
čísla	číslo	k1gNnSc2	číslo
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
F	F	kA	F
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
F	F	kA	F
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
clonové	clonový	k2eAgNnSc1d1	clonové
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
f	f	k?	f
<g/>
}	}	kIx)	}
ohnisková	ohniskový	k2eAgFnSc1d1	ohnisková
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
objektivu	objektiv	k1gInSc2	objektiv
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
d	d	k?	d
<g/>
}	}	kIx)	}
průměr	průměr	k1gInSc1	průměr
otvoru	otvor	k1gInSc2	otvor
clony	clona	k1gFnSc2	clona
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	co	k3yRnSc7	co
větší	veliký	k2eAgInSc1d2	veliký
otvor	otvor	k1gInSc1	otvor
(	(	kIx(	(
<g/>
apertura	apertura	k1gFnSc1	apertura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
menší	malý	k2eAgNnSc1d2	menší
clonové	clonový	k2eAgNnSc1d1	clonové
číslo	číslo	k1gNnSc1	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
dopadne	dopadnout	k5eAaPmIp3nS	dopadnout
za	za	k7c4	za
jednotku	jednotka	k1gFnSc4	jednotka
času	čas	k1gInSc2	čas
na	na	k7c4	na
film	film	k1gInSc4	film
nebo	nebo	k8xC	nebo
senzor	senzor	k1gInSc1	senzor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nepřímo	přímo	k6eNd1	přímo
úměrné	úměrný	k2eAgFnSc6d1	úměrná
druhé	druhý	k4xOgFnSc6	druhý
mocnině	mocnina	k1gFnSc6	mocnina
clonového	clonový	k2eAgNnSc2d1	clonové
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Dvojnásobné	dvojnásobný	k2eAgNnSc1d1	dvojnásobné
clonové	clonový	k2eAgNnSc1d1	clonové
číslo	číslo	k1gNnSc1	číslo
znamená	znamenat	k5eAaImIp3nS	znamenat
čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
clonová	clonový	k2eAgNnPc1d1	clonové
čísla	číslo	k1gNnPc1	číslo
na	na	k7c6	na
objektivech	objektiv	k1gInPc6	objektiv
uvádějí	uvádět	k5eAaImIp3nP	uvádět
jako	jako	k9	jako
násobky	násobek	k1gInPc1	násobek
odmocniny	odmocnina	k1gFnSc2	odmocnina
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
(	(	kIx(	(
<g/>
1,4	[number]	k4	1,4
-	-	kIx~	-
2	[number]	k4	2
-	-	kIx~	-
2,8	[number]	k4	2,8
-	-	kIx~	-
4	[number]	k4	4
-	-	kIx~	-
5,6	[number]	k4	5,6
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
pokaždé	pokaždé	k6eAd1	pokaždé
polovičnímu	poloviční	k2eAgNnSc3d1	poloviční
množství	množství	k1gNnSc3	množství
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Clona	clona	k1gFnSc1	clona
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
uvádí	uvádět	k5eAaImIp3nS	uvádět
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
<	<	kIx(	<
<g/>
clonové	clonový	k2eAgNnSc1d1	clonové
číslo	číslo	k1gNnSc1	číslo
<g/>
>	>	kIx)	>
<g/>
,	,	kIx,	,
eventuálně	eventuálně	k6eAd1	eventuálně
jako	jako	k8xC	jako
poměr	poměr	k1gInSc1	poměr
či	či	k8xC	či
zlomek	zlomek	k1gInSc1	zlomek
se	s	k7c7	s
clonovým	clonový	k2eAgNnSc7d1	clonové
číslem	číslo	k1gNnSc7	číslo
ve	v	k7c6	v
jmenovateli	jmenovatel	k1gInSc6	jmenovatel
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
tvar	tvar	k1gInSc1	tvar
se	se	k3xPyFc4	se
vžil	vžít	k5eAaPmAgInS	vžít
právě	právě	k9	právě
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
nezměněném	změněný	k2eNgNnSc6d1	nezměněné
clonovém	clonový	k2eAgNnSc6d1	clonové
čísle	číslo	k1gNnSc6	číslo
a	a	k8xC	a
měnící	měnící	k2eAgFnPc4d1	měnící
se	se	k3xPyFc4	se
ohniskové	ohniskový	k2eAgFnSc2d1	ohnisková
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
mění	měnit	k5eAaImIp3nS	měnit
i	i	k9	i
velikost	velikost	k1gFnSc1	velikost
otvoru	otvor	k1gInSc2	otvor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
množství	množství	k1gNnSc1	množství
světla	světlo	k1gNnSc2	světlo
dopadajícího	dopadající	k2eAgNnSc2d1	dopadající
na	na	k7c4	na
film	film	k1gInSc4	film
nebo	nebo	k8xC	nebo
senzor	senzor	k1gInSc1	senzor
za	za	k7c4	za
jednotku	jednotka	k1gFnSc4	jednotka
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Clonové	clonový	k2eAgNnSc1d1	clonové
číslo	číslo	k1gNnSc1	číslo
odpovídající	odpovídající	k2eAgFnSc2d1	odpovídající
maximálnímu	maximální	k2eAgNnSc3d1	maximální
možnému	možný	k2eAgNnSc3d1	možné
otevření	otevření	k1gNnSc3	otevření
clony	clona	k1gFnSc2	clona
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
světelnost	světelnost	k1gFnSc1	světelnost
objektivu	objektiv	k1gInSc2	objektiv
<g/>
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
<g/>
-li	i	k?	-li
tudíž	tudíž	k8xC	tudíž
objektiv	objektiv	k1gInSc4	objektiv
s	s	k7c7	s
ohniskovou	ohniskový	k2eAgFnSc7d1	ohnisková
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
40	[number]	k4	40
mm	mm	kA	mm
a	a	k8xC	a
světelností	světelnost	k1gFnSc7	světelnost
značenou	značený	k2eAgFnSc7d1	značená
jako	jako	k8xC	jako
f	f	k?	f
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
či	či	k8xC	či
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
maximální	maximální	k2eAgInSc4d1	maximální
průměr	průměr	k1gInSc4	průměr
otevřené	otevřený	k2eAgFnSc2d1	otevřená
clony	clona	k1gFnSc2	clona
10	[number]	k4	10
mm	mm	kA	mm
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
clonovému	clonový	k2eAgNnSc3d1	clonové
číslu	číslo	k1gNnSc3	číslo
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
nastavením	nastavení	k1gNnSc7	nastavení
rychlosti	rychlost	k1gFnSc2	rychlost
závěrky	závěrka	k1gFnSc2	závěrka
(	(	kIx(	(
<g/>
čili	čili	k8xC	čili
expoziční	expoziční	k2eAgFnSc2d1	expoziční
doby	doba	k1gFnSc2	doba
<g/>
)	)	kIx)	)
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přesně	přesně	k6eAd1	přesně
nastavit	nastavit	k5eAaPmF	nastavit
množství	množství	k1gNnSc4	množství
světla	světlo	k1gNnSc2	světlo
pro	pro	k7c4	pro
správnou	správný	k2eAgFnSc4d1	správná
expozici	expozice	k1gFnSc4	expozice
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
hloubku	hloubka	k1gFnSc4	hloubka
ostrosti	ostrost	k1gFnSc2	ostrost
výsledné	výsledný	k2eAgFnSc2d1	výsledná
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
:	:	kIx,	:
čím	co	k3yQnSc7	co
vyšší	vysoký	k2eAgFnSc1d2	vyšší
clonové	clonový	k2eAgNnSc4d1	clonové
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
větší	veliký	k2eAgFnSc1d2	veliký
hloubka	hloubka	k1gFnSc1	hloubka
ostrosti	ostrost	k1gFnSc2	ostrost
<g/>
.	.	kIx.	.
</s>
<s>
Nastavení	nastavení	k1gNnSc1	nastavení
clony	clona	k1gFnSc2	clona
(	(	kIx(	(
<g/>
clonového	clonový	k2eAgNnSc2d1	clonové
čísla	číslo	k1gNnSc2	číslo
<g/>
)	)	kIx)	)
na	na	k7c6	na
objektivu	objektiv	k1gInSc6	objektiv
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
základních	základní	k2eAgInPc2d1	základní
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
kterými	který	k3yQgNnPc7	který
může	moct	k5eAaImIp3nS	moct
fotograf	fotograf	k1gMnSc1	fotograf
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
expozici	expozice	k1gFnSc4	expozice
snímku	snímek	k1gInSc2	snímek
(	(	kIx(	(
<g/>
vedle	vedle	k7c2	vedle
rychlosti	rychlost	k1gFnSc2	rychlost
závěrky	závěrka	k1gFnSc2	závěrka
a	a	k8xC	a
citlivosti	citlivost	k1gFnSc2	citlivost
filmu	film	k1gInSc2	film
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Otevření	otevření	k1gNnSc1	otevření
clony	clona	k1gFnSc2	clona
(	(	kIx(	(
<g/>
snížení	snížení	k1gNnSc1	snížení
clonového	clonový	k2eAgNnSc2d1	clonové
čísla	číslo	k1gNnSc2	číslo
<g/>
)	)	kIx)	)
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vytvořit	vytvořit	k5eAaPmF	vytvořit
dobře	dobře	k6eAd1	dobře
exponovaný	exponovaný	k2eAgInSc4d1	exponovaný
snímek	snímek	k1gInSc4	snímek
za	za	k7c2	za
horších	zlý	k2eAgFnPc2d2	horší
světelných	světelný	k2eAgFnPc2d1	světelná
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Otevření	otevření	k1gNnSc1	otevření
clony	clona	k1gFnSc2	clona
za	za	k7c2	za
dobrých	dobrý	k2eAgFnPc2d1	dobrá
světelných	světelný	k2eAgFnPc2d1	světelná
podmínek	podmínka	k1gFnPc2	podmínka
a	a	k8xC	a
korekce	korekce	k1gFnSc2	korekce
snížením	snížení	k1gNnSc7	snížení
expozičního	expoziční	k2eAgInSc2d1	expoziční
času	čas	k1gInSc2	čas
sníží	snížit	k5eAaPmIp3nS	snížit
hloubku	hloubka	k1gFnSc4	hloubka
ostrosti	ostrost	k1gFnSc2	ostrost
(	(	kIx(	(
<g/>
může	moct	k5eAaImIp3nS	moct
tak	tak	k6eAd1	tak
vizuálně	vizuálně	k6eAd1	vizuálně
oddělit	oddělit	k5eAaPmF	oddělit
snímaný	snímaný	k2eAgInSc4d1	snímaný
objekt	objekt	k1gInSc4	objekt
od	od	k7c2	od
pozadí	pozadí	k1gNnSc2	pozadí
-	-	kIx~	-
bokeh	bokeh	k1gInSc1	bokeh
efekt	efekt	k1gInSc1	efekt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přivření	přivření	k1gNnSc1	přivření
clony	clona	k1gFnSc2	clona
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
světelné	světelný	k2eAgFnPc1d1	světelná
podmínky	podmínka	k1gFnPc1	podmínka
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
<g/>
)	)	kIx)	)
naopak	naopak	k6eAd1	naopak
hloubku	hloubka	k1gFnSc4	hloubka
ostrosti	ostrost	k1gFnSc2	ostrost
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
(	(	kIx(	(
<g/>
způsobí	způsobit	k5eAaPmIp3nS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
výsledném	výsledný	k2eAgInSc6d1	výsledný
snímku	snímek	k1gInSc6	snímek
budou	být	k5eAaImBp3nP	být
blízké	blízký	k2eAgFnPc1d1	blízká
i	i	k8xC	i
vzdálené	vzdálený	k2eAgInPc1d1	vzdálený
předměty	předmět	k1gInPc1	předmět
ostřejší	ostrý	k2eAgInPc1d2	ostřejší
nebo	nebo	k8xC	nebo
zcela	zcela	k6eAd1	zcela
ostré	ostrý	k2eAgInPc1d1	ostrý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
třeba	třeba	k6eAd1	třeba
fotografovat	fotografovat	k5eAaImF	fotografovat
s	s	k7c7	s
otevřenou	otevřený	k2eAgFnSc7d1	otevřená
clonou	clona	k1gFnSc7	clona
a	a	k8xC	a
vyšším	vysoký	k2eAgInSc7d2	vyšší
expozičním	expoziční	k2eAgInSc7d1	expoziční
časem	čas	k1gInSc7	čas
za	za	k7c2	za
světelných	světelný	k2eAgFnPc2d1	světelná
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
při	při	k7c6	při
tomto	tento	k3xDgNnSc6	tento
nastavení	nastavení	k1gNnSc6	nastavení
způsobily	způsobit	k5eAaPmAgInP	způsobit
přeexpozici	přeexpozice	k1gFnSc3	přeexpozice
snímku	snímek	k1gInSc2	snímek
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
si	se	k3xPyFc3	se
pomoci	pomoct	k5eAaPmF	pomoct
neutrálním	neutrální	k2eAgInSc7d1	neutrální
filtrem	filtr	k1gInSc7	filtr
(	(	kIx(	(
<g/>
též	též	k9	též
značený	značený	k2eAgMnSc1d1	značený
jako	jako	k8xS	jako
ND	ND	kA	ND
<g/>
,	,	kIx,	,
z	z	k7c2	z
angl.	angl.	k?	angl.
neutral	utrat	k5eNaPmAgMnS	utrat
density	densit	k1gInPc4	densit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pevná	pevný	k2eAgFnSc1d1	pevná
clona	clona	k1gFnSc1	clona
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
mezikruží	mezikruží	k1gNnSc1	mezikruží
z	z	k7c2	z
tenkého	tenký	k2eAgInSc2d1	tenký
černěného	černěný	k2eAgInSc2d1	černěný
plechu	plech	k1gInSc2	plech
<g/>
.	.	kIx.	.
</s>
<s>
Užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
dalekohledech	dalekohled	k1gInPc6	dalekohled
pro	pro	k7c4	pro
vyloučení	vyloučení	k1gNnSc4	vyloučení
paprsků	paprsek	k1gInPc2	paprsek
<g/>
,	,	kIx,	,
odražených	odražený	k2eAgInPc2d1	odražený
od	od	k7c2	od
stěn	stěna	k1gFnPc2	stěna
tubusu	tubus	k1gInSc2	tubus
a	a	k8xC	a
pod	pod	k7c7	pod
<g/>
.	.	kIx.	.
</s>
<s>
Nejjednodušší	jednoduchý	k2eAgFnSc1d3	nejjednodušší
proměnlivá	proměnlivý	k2eAgFnSc1d1	proměnlivá
clona	clona	k1gFnSc1	clona
je	být	k5eAaImIp3nS	být
otočný	otočný	k2eAgInSc4d1	otočný
segment	segment	k1gInSc4	segment
s	s	k7c7	s
otvory	otvor	k1gInPc7	otvor
různé	různý	k2eAgFnSc2d1	různá
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
přesunout	přesunout	k5eAaPmF	přesunout
do	do	k7c2	do
osy	osa	k1gFnSc2	osa
optiky	optika	k1gFnSc2	optika
<g/>
.	.	kIx.	.
</s>
<s>
Objektiv	objektiv	k1gInSc1	objektiv
tedy	tedy	k9	tedy
nabízí	nabízet	k5eAaImIp3nS	nabízet
jen	jen	k9	jen
několik	několik	k4yIc4	několik
clonových	clonový	k2eAgNnPc2d1	clonové
čísel	číslo	k1gNnPc2	číslo
a	a	k8xC	a
změna	změna	k1gFnSc1	změna
clony	clona	k1gFnSc2	clona
je	být	k5eAaImIp3nS	být
skoková	skokový	k2eAgFnSc1d1	skoková
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInPc1d1	moderní
fotoaparáty	fotoaparát	k1gInPc1	fotoaparát
používají	používat	k5eAaImIp3nP	používat
lamelovou	lamelový	k2eAgFnSc4d1	lamelová
(	(	kIx(	(
<g/>
také	také	k6eAd1	také
"	"	kIx"	"
<g/>
irisovou	irisový	k2eAgFnSc4d1	irisová
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
clonu	clona	k1gFnSc4	clona
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
lze	lze	k6eAd1	lze
regulovat	regulovat	k5eAaImF	regulovat
spojitě	spojitě	k6eAd1	spojitě
v	v	k7c6	v
jistém	jistý	k2eAgNnSc6d1	jisté
rozmezí	rozmezí	k1gNnSc6	rozmezí
<g/>
.	.	kIx.	.
</s>
<s>
Lamelová	lamelový	k2eAgFnSc1d1	lamelová
clona	clona	k1gFnSc1	clona
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgInSc4d2	veliký
počet	počet	k1gInSc4	počet
lamel	lamela	k1gFnPc2	lamela
srpkovitého	srpkovitý	k2eAgInSc2d1	srpkovitý
tvaru	tvar	k1gInSc2	tvar
z	z	k7c2	z
tenkého	tenký	k2eAgInSc2d1	tenký
černěného	černěný	k2eAgInSc2d1	černěný
plechu	plech	k1gInSc2	plech
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
koncem	konec	k1gInSc7	konec
jsou	být	k5eAaImIp3nP	být
uchyceny	uchytit	k5eAaPmNgInP	uchytit
otočně	otočně	k6eAd1	otočně
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
objektivu	objektiv	k1gInSc2	objektiv
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
konci	konec	k1gInSc6	konec
mají	mít	k5eAaImIp3nP	mít
krátký	krátký	k2eAgInSc4d1	krátký
kolíček	kolíček	k1gInSc4	kolíček
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
drážce	drážka	k1gFnSc6	drážka
otočného	otočný	k2eAgInSc2d1	otočný
prstence	prstenec	k1gInSc2	prstenec
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
se	se	k3xPyFc4	se
clona	clona	k1gFnSc1	clona
nastavuje	nastavovat	k5eAaImIp3nS	nastavovat
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgNnSc3	tento
uspořádání	uspořádání	k1gNnSc3	uspořádání
se	se	k3xPyFc4	se
otvor	otvor	k1gInSc1	otvor
clony	clona	k1gFnSc2	clona
v	v	k7c6	v
širokých	široký	k2eAgFnPc6d1	široká
mezích	mez	k1gFnPc6	mez
mění	měnit	k5eAaImIp3nS	měnit
a	a	k8xC	a
přesto	přesto	k8xC	přesto
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
zhruba	zhruba	k6eAd1	zhruba
kruhový	kruhový	k2eAgInSc1d1	kruhový
<g/>
.	.	kIx.	.
</s>
