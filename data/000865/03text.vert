<s>
Lipa	Lipa	k1gFnSc1	Lipa
je	být	k5eAaImIp3nS	být
zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
československá	československý	k2eAgFnSc1d1	Československá
značka	značka	k1gFnSc1	značka
cigaret	cigareta	k1gFnPc2	cigareta
bez	bez	k7c2	bez
filtru	filtr	k1gInSc2	filtr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
trh	trh	k1gInSc4	trh
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
svátku	svátek	k1gInSc2	svátek
1	[number]	k4	1
<g/>
.	.	kIx.	.
máje	máj	k1gInSc2	máj
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
(	(	kIx(	(
<g/>
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
původně	původně	k6eAd1	původně
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Cigareta	cigareta	k1gFnSc1	cigareta
IX	IX	kA	IX
<g/>
.	.	kIx.	.
sjezdu	sjezd	k1gInSc6	sjezd
KSS	KSS	kA	KSS
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prodávaly	prodávat	k5eAaImAgFnP	prodávat
se	se	k3xPyFc4	se
po	po	k7c4	po
10	[number]	k4	10
ks	ks	kA	ks
v	v	k7c6	v
papírové	papírový	k2eAgFnSc6d1	papírová
krabičce	krabička	k1gFnSc6	krabička
oranžovo-žlutých	oranžovo-žlutý	k2eAgFnPc2d1	oranžovo-žlutá
barev	barva	k1gFnPc2	barva
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
2	[number]	k4	2
Kčs	Kčs	kA	Kčs
<g/>
.	.	kIx.	.
</s>
<s>
Vyráběl	vyrábět	k5eAaImAgInS	vyrábět
je	on	k3xPp3gMnPc4	on
Československý	československý	k2eAgInSc1d1	československý
tabákový	tabákový	k2eAgInSc1d1	tabákový
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
n.	n.	k?	n.
p.	p.	k?	p.
podle	podle	k7c2	podle
ČSN	ČSN	kA	ČSN
56	[number]	k4	56
9560	[number]	k4	9560
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
grafického	grafický	k2eAgInSc2d1	grafický
návrhu	návrh	k1gInSc2	návrh
krabičky	krabička	k1gFnSc2	krabička
byl	být	k5eAaImAgMnS	být
akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
Richard	Richard	k1gMnSc1	Richard
Lander	Lander	k1gMnSc1	Lander
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
levnější	levný	k2eAgFnSc7d2	levnější
značkou	značka	k1gFnSc7	značka
Partyzánka	partyzánka	k1gFnSc1	partyzánka
o	o	k7c4	o
pilíř	pilíř	k1gInSc4	pilíř
cigaretového	cigaretový	k2eAgInSc2d1	cigaretový
trhu	trh	k1gInSc2	trh
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
je	být	k5eAaImIp3nS	být
postupně	postupně	k6eAd1	postupně
vytlačila	vytlačit	k5eAaPmAgFnS	vytlačit
nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
značek	značka	k1gFnPc2	značka
(	(	kIx(	(
<g/>
Mars	Mars	k1gInSc1	Mars
<g/>
,	,	kIx,	,
Sparta	Sparta	k1gFnSc1	Sparta
<g/>
,	,	kIx,	,
Start	start	k1gInSc1	start
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
cigaret	cigareta	k1gFnPc2	cigareta
Lipa	Lipa	k1gFnSc1	Lipa
byla	být	k5eAaImAgFnS	být
ukončena	ukončen	k2eAgFnSc1d1	ukončena
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
