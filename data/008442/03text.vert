<p>
<s>
Karelská	karelský	k2eAgFnSc1d1	Karelská
šíje	šíje	k1gFnSc1	šíje
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
К	К	k?	К
п	п	k?	п
<g/>
,	,	kIx,	,
finsky	finsky	k6eAd1	finsky
Karjalankannas	Karjalankannas	k1gInSc1	Karjalankannas
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pruh	pruh	k1gInSc1	pruh
země	zem	k1gFnSc2	zem
rozkládající	rozkládající	k2eAgFnSc2d1	rozkládající
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
Ladožským	ladožský	k2eAgNnSc7d1	Ladožské
jezerem	jezero	k1gNnSc7	jezero
a	a	k8xC	a
Finským	finský	k2eAgInSc7d1	finský
zálivem	záliv	k1gInSc7	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
částí	část	k1gFnPc2	část
tzv.	tzv.	kA	tzv.
Karelie	Karelie	k1gFnSc2	Karelie
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
městem	město	k1gNnSc7	město
tohoto	tento	k3xDgNnSc2	tento
území	území	k1gNnSc2	území
je	být	k5eAaImIp3nS	být
přístav	přístav	k1gInSc1	přístav
Vyborg	Vyborg	k1gInSc1	Vyborg
(	(	kIx(	(
<g/>
finský	finský	k2eAgInSc1d1	finský
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
je	být	k5eAaImIp3nS	být
Viipuri	Viipuri	k1gNnPc7	Viipuri
<g/>
)	)	kIx)	)
ležící	ležící	k2eAgInPc1d1	ležící
u	u	k7c2	u
západních	západní	k2eAgFnPc2d1	západní
hranic	hranice	k1gFnPc2	hranice
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
byla	být	k5eAaImAgFnS	být
K.	K.	kA	K.
šíje	šíje	k1gFnSc1	šíje
nárazníkovým	nárazníkový	k2eAgNnSc7d1	nárazníkové
pásmem	pásmo	k1gNnSc7	pásmo
mezi	mezi	k7c7	mezi
Švédskem	Švédsko	k1gNnSc7	Švédsko
a	a	k8xC	a
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
autonomního	autonomní	k2eAgNnSc2d1	autonomní
Finského	finský	k2eAgNnSc2d1	finské
velkovévodství	velkovévodství	k1gNnSc2	velkovévodství
začleněného	začleněný	k2eAgNnSc2d1	začleněné
do	do	k7c2	do
Impéria	impérium	k1gNnSc2	impérium
ruských	ruský	k2eAgMnPc2d1	ruský
carů	car	k1gMnPc2	car
a	a	k8xC	a
po	po	k7c4	po
jeho	jeho	k3xOp3gNnSc4	jeho
osamostatnění	osamostatnění	k1gNnSc4	osamostatnění
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karelská	karelský	k2eAgFnSc1d1	Karelská
šíje	šíje	k1gFnSc1	šíje
vždy	vždy	k6eAd1	vždy
představovala	představovat	k5eAaImAgFnS	představovat
hlavní	hlavní	k2eAgFnSc4d1	hlavní
přístupovou	přístupový	k2eAgFnSc4d1	přístupová
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
nitra	nitro	k1gNnSc2	nitro
Finska	Finsko	k1gNnSc2	Finsko
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
pak	pak	k9	pak
tzv.	tzv.	kA	tzv.
Viipurská	Viipurský	k2eAgFnSc1d1	Viipurský
brána	brána	k1gFnSc1	brána
<g/>
)	)	kIx)	)
a	a	k8xC	a
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
bojištěm	bojiště	k1gNnSc7	bojiště
během	během	k7c2	během
rusko-švédských	rusko-švédský	k2eAgFnPc2d1	rusko-švédská
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ještě	ještě	k6eAd1	ještě
Finsko	Finsko	k1gNnSc1	Finsko
náleželo	náležet	k5eAaImAgNnS	náležet
ke	k	k7c3	k
Švédsku	Švédsko	k1gNnSc3	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stala	stát	k5eAaPmAgFnS	stát
bojištěm	bojiště	k1gNnSc7	bojiště
v	v	k7c6	v
rusko-finských	ruskoinský	k2eAgFnPc6d1	rusko-finský
válkách	válka	k1gFnPc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1920	[number]	k4	1920
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
střetávaly	střetávat	k5eAaImAgFnP	střetávat
síly	síla	k1gFnPc4	síla
bílých	bílý	k2eAgMnPc2d1	bílý
Finů	Fin	k1gMnPc2	Fin
s	s	k7c7	s
intervenčními	intervenční	k2eAgFnPc7d1	intervenční
silami	síla	k1gFnPc7	síla
ruských	ruský	k2eAgMnPc2d1	ruský
bolševiků	bolševik	k1gMnPc2	bolševik
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
nejprve	nejprve	k6eAd1	nejprve
hodlaly	hodlat	k5eAaImAgInP	hodlat
podpořit	podpořit	k5eAaPmF	podpořit
své	svůj	k3xOyFgMnPc4	svůj
soudruhy	soudruh	k1gMnPc4	soudruh
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
Finsku	Finsko	k1gNnSc6	Finsko
a	a	k8xC	a
po	po	k7c6	po
krachu	krach	k1gInSc6	krach
této	tento	k3xDgFnSc2	tento
ideje	idea	k1gFnSc2	idea
se	se	k3xPyFc4	se
alespoň	alespoň	k9	alespoň
pokoušely	pokoušet	k5eAaImAgFnP	pokoušet
(	(	kIx(	(
<g/>
neúspěšně	úspěšně	k6eNd1	úspěšně
<g/>
)	)	kIx)	)
ovládnout	ovládnout	k5eAaPmF	ovládnout
Viipuri	Viipure	k1gFnSc4	Viipure
a	a	k8xC	a
odtrhnout	odtrhnout	k5eAaPmF	odtrhnout
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Karelská	karelský	k2eAgFnSc1d1	Karelská
šíje	šíje	k1gFnSc1	šíje
zůstala	zůstat	k5eAaPmAgFnS	zůstat
Finsku	Finsko	k1gNnSc3	Finsko
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
a	a	k8xC	a
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	let	k1gInPc6	let
vybudovalo	vybudovat	k5eAaPmAgNnS	vybudovat
systém	systém	k1gInSc4	systém
opevnění	opevnění	k1gNnSc2	opevnění
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
Mannerheimova	Mannerheimův	k2eAgFnSc1d1	Mannerheimova
linie	linie	k1gFnSc1	linie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Karelské	karelský	k2eAgFnSc6d1	Karelská
šíji	šíj	k1gFnSc6	šíj
se	se	k3xPyFc4	se
odehrály	odehrát	k5eAaPmAgInP	odehrát
nejtěžší	těžký	k2eAgInPc1d3	nejtěžší
a	a	k8xC	a
nejzuřivější	zuřivý	k2eAgInPc1d3	Nejzuřivější
boje	boj	k1gInPc1	boj
Zimní	zimní	k2eAgFnSc2d1	zimní
války	válka	k1gFnSc2	válka
v	v	k7c6	v
letech	let	k1gInPc6	let
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yQgFnSc6	který
SSSR	SSSR	kA	SSSR
Karelskou	karelský	k2eAgFnSc4d1	Karelská
šíji	šíje	k1gFnSc4	šíje
zabral	zabrat	k5eAaPmAgMnS	zabrat
jako	jako	k9	jako
válečnou	válečný	k2eAgFnSc4d1	válečná
kořist	kořist	k1gFnSc4	kořist
<g/>
,	,	kIx,	,
i	i	k8xC	i
Pokračovací	pokračovací	k2eAgFnPc1d1	pokračovací
války	válka	k1gFnPc1	válka
v	v	k7c6	v
letech	let	k1gInPc6	let
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
nejprve	nejprve	k6eAd1	nejprve
osvobozena	osvobozen	k2eAgFnSc1d1	osvobozena
Finy	Fina	k1gFnPc1	Fina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
během	během	k7c2	během
letní	letní	k2eAgFnSc2d1	letní
ofenzívy	ofenzíva	k1gFnSc2	ofenzíva
dobyl	dobýt	k5eAaPmAgInS	dobýt
její	její	k3xOp3gFnSc4	její
střední	střední	k2eAgFnSc4d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
ji	on	k3xPp3gFnSc4	on
na	na	k7c6	na
základě	základ	k1gInSc6	základ
podmínek	podmínka	k1gFnPc2	podmínka
příměří	příměří	k1gNnSc4	příměří
zabral	zabrat	k5eAaPmAgInS	zabrat
celou	celá	k1gFnSc4	celá
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
je	být	k5eAaImIp3nS	být
Karelská	karelský	k2eAgFnSc1d1	Karelská
šíje	šíje	k1gFnSc1	šíje
součástí	součást	k1gFnSc7	součást
Ruska	Ruska	k1gFnSc1	Ruska
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
Leningradské	leningradský	k2eAgFnPc4d1	Leningradská
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
finských	finský	k2eAgNnPc2d1	finské
jmen	jméno	k1gNnPc2	jméno
měst	město	k1gNnPc2	město
a	a	k8xC	a
vesnic	vesnice	k1gFnPc2	vesnice
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
ruskými	ruský	k2eAgFnPc7d1	ruská
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgNnSc1d1	současné
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
či	či	k8xC	či
jejich	jejich	k3xOp3gMnPc2	jejich
potomků	potomek	k1gMnPc2	potomek
(	(	kIx(	(
<g/>
především	především	k9	především
ruské	ruský	k2eAgFnSc2d1	ruská
národnosti	národnost	k1gFnSc2	národnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1939	[number]	k4	1939
a	a	k8xC	a
1944	[number]	k4	1944
prakticky	prakticky	k6eAd1	prakticky
veškeré	veškerý	k3xTgNnSc4	veškerý
finské	finský	k2eAgNnSc4d1	finské
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Karelskou	karelský	k2eAgFnSc4d1	Karelská
šíji	šíje	k1gFnSc4	šíje
opustilo	opustit	k5eAaPmAgNnS	opustit
a	a	k8xC	a
přestěhovalo	přestěhovat	k5eAaPmAgNnS	přestěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Karelská	karelský	k2eAgFnSc1d1	Karelská
šíje	šíje	k1gFnSc1	šíje
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
