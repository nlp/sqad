<s>
Karelská	karelský	k2eAgFnSc1d1	Karelská
šíje	šíje	k1gFnSc1	šíje
vždy	vždy	k6eAd1	vždy
představovala	představovat	k5eAaImAgFnS	představovat
hlavní	hlavní	k2eAgFnSc4d1	hlavní
přístupovou	přístupový	k2eAgFnSc4d1	přístupová
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
nitra	nitro	k1gNnSc2	nitro
Finska	Finsko	k1gNnSc2	Finsko
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
pak	pak	k9	pak
tzv.	tzv.	kA	tzv.
Viipurská	Viipurský	k2eAgFnSc1d1	Viipurský
brána	brána	k1gFnSc1	brána
<g/>
)	)	kIx)	)
a	a	k8xC	a
proto	proto	k8xC	proto
byla	být	k5eAaImAgFnS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
bojištěm	bojiště	k1gNnSc7	bojiště
během	během	k7c2	během
rusko-švédských	rusko-švédský	k2eAgFnPc2d1	rusko-švédská
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ještě	ještě	k6eAd1	ještě
Finsko	Finsko	k1gNnSc1	Finsko
náleželo	náležet	k5eAaImAgNnS	náležet
ke	k	k7c3	k
Švédsku	Švédsko	k1gNnSc3	Švédsko
<g/>
.	.	kIx.	.
</s>
