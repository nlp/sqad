<s>
Sala	Sal	k2eAgFnSc1d1	Sala
terrena	terrena	k1gFnSc1	terrena
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
přízemní	přízemní	k2eAgInSc1d1	přízemní
sál	sál	k1gInSc1	sál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
rovněž	rovněž	k9	rovněž
sala	sal	k2eAgFnSc1d1	sala
terrana	terrana	k1gFnSc1	terrana
nebo	nebo	k8xC	nebo
zahradní	zahradní	k2eAgInSc1d1	zahradní
sál	sál	k1gInSc1	sál
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
otevřená	otevřený	k2eAgFnSc1d1	otevřená
architektonická	architektonický	k2eAgFnSc1d1	architektonická
stavba	stavba	k1gFnSc1	stavba
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
pro	pro	k7c4	pro
barokní	barokní	k2eAgInPc4d1	barokní
palácové	palácový	k2eAgInPc4d1	palácový
a	a	k8xC	a
zámecké	zámecký	k2eAgFnPc4d1	zámecká
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
.	.	kIx.	.
</s>
