<p>
<s>
Sala	Sal	k2eAgFnSc1d1	Sala
terrena	terrena	k1gFnSc1	terrena
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
přízemní	přízemní	k2eAgInSc1d1	přízemní
sál	sál	k1gInSc1	sál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
rovněž	rovněž	k9	rovněž
sala	sal	k2eAgFnSc1d1	sala
terrana	terrana	k1gFnSc1	terrana
nebo	nebo	k8xC	nebo
zahradní	zahradní	k2eAgInSc1d1	zahradní
sál	sál	k1gInSc1	sál
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
otevřená	otevřený	k2eAgFnSc1d1	otevřená
architektonická	architektonický	k2eAgFnSc1d1	architektonická
stavba	stavba	k1gFnSc1	stavba
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
pro	pro	k7c4	pro
barokní	barokní	k2eAgInPc4d1	barokní
palácové	palácový	k2eAgInPc4d1	palácový
a	a	k8xC	a
zámecké	zámecký	k2eAgFnPc4d1	zámecká
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sala	Sal	k2eAgFnSc1d1	Sala
terrena	terrena	k1gFnSc1	terrena
je	být	k5eAaImIp3nS	být
sál	sál	k1gInSc1	sál
situovaný	situovaný	k2eAgInSc1d1	situovaný
v	v	k7c6	v
přízemním	přízemní	k2eAgNnSc6d1	přízemní
podlaží	podlaží	k1gNnSc6	podlaží
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
ose	osa	k1gFnSc6	osa
zámku	zámek	k1gInSc2	zámek
nebo	nebo	k8xC	nebo
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
tvoří	tvořit	k5eAaImIp3nP	tvořit
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
stavbu	stavba	k1gFnSc4	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
<g/>
,	,	kIx,	,
klasicky	klasicky	k6eAd1	klasicky
arkádou	arkáda	k1gFnSc7	arkáda
o	o	k7c6	o
třech	tři	k4xCgInPc6	tři
obloucích	oblouk	k1gInPc6	oblouk
<g/>
,	,	kIx,	,
do	do	k7c2	do
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
úrovni	úroveň	k1gFnSc6	úroveň
nebo	nebo	k8xC	nebo
přístupná	přístupný	k2eAgFnSc1d1	přístupná
několika	několik	k4yIc7	několik
stupni	stupeň	k1gInSc3	stupeň
schodiště	schodiště	k1gNnSc1	schodiště
<g/>
.	.	kIx.	.
</s>
<s>
Sala	Sala	k1gFnSc1	Sala
sloužila	sloužit	k5eAaImAgFnS	sloužit
pro	pro	k7c4	pro
relaxaci	relaxace	k1gFnSc4	relaxace
<g/>
,	,	kIx,	,
svačiny	svačina	k1gFnPc4	svačina
<g/>
,	,	kIx,	,
koncerty	koncert	k1gInPc4	koncert
apod.	apod.	kA	apod.
Bývá	bývat	k5eAaImIp3nS	bývat
reprezentativně	reprezentativně	k6eAd1	reprezentativně
vyzdobena	vyzdoben	k2eAgFnSc1d1	vyzdobena
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
zařízena	zařízen	k2eAgFnSc1d1	zařízena
jako	jako	k8xS	jako
jeskyně	jeskyně	k1gFnSc1	jeskyně
a	a	k8xC	a
vyzdobena	vyzdobit	k5eAaPmNgFnS	vyzdobit
kašnami	kašna	k1gFnPc7	kašna
a	a	k8xC	a
fontánami	fontána	k1gFnPc7	fontána
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
moravské	moravský	k2eAgFnSc2d1	Moravská
saly	sala	k1gFnSc2	sala
terreny	terrena	k1gFnSc2	terrena
==	==	k?	==
</s>
</p>
<p>
<s>
Některé	některý	k3yIgInPc4	některý
z	z	k7c2	z
nejznámějších	známý	k2eAgInPc2d3	nejznámější
příkladů	příklad	k1gInPc2	příklad
saly	sal	k2eAgInPc4d1	sal
terreny	terren	k1gInPc4	terren
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
těchto	tento	k3xDgFnPc2	tento
památek	památka	k1gFnPc2	památka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ledeburská	Ledeburský	k2eAgFnSc1d1	Ledeburská
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
Valdštejnská	valdštejnský	k2eAgFnSc1d1	Valdštejnská
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
Vrtbovská	Vrtbovský	k2eAgFnSc1d1	Vrtbovská
zahrada	zahrada	k1gFnSc1	zahrada
</s>
</p>
<p>
<s>
Libosad	libosad	k1gInSc1	libosad
u	u	k7c2	u
Jičína	Jičín	k1gInSc2	Jičín
</s>
</p>
<p>
<s>
Arcibiskupský	arcibiskupský	k2eAgInSc1d1	arcibiskupský
zámek	zámek	k1gInSc1	zámek
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
</s>
</p>
<p>
<s>
zámek	zámek	k1gInSc1	zámek
Libochovice	Libochovice	k1gFnPc4	Libochovice
</s>
</p>
<p>
<s>
Vyhlídka	vyhlídka	k1gFnSc1	vyhlídka
Belvedér	belvedér	k1gInSc1	belvedér
u	u	k7c2	u
zámku	zámek	k1gInSc2	zámek
v	v	k7c6	v
Bynovci	Bynovec	k1gInSc6	Bynovec
</s>
</p>
<p>
<s>
zámek	zámek	k1gInSc1	zámek
v	v	k7c6	v
Budišově	Budišův	k2eAgFnSc6d1	Budišova
</s>
</p>
<p>
<s>
Klášterec	Klášterec	k1gInSc1	Klášterec
nad	nad	k7c7	nad
Ohří	Ohře	k1gFnSc7	Ohře
</s>
</p>
<p>
<s>
zámek	zámek	k1gInSc1	zámek
v	v	k7c6	v
Holešově	Holešov	k1gInSc6	Holešov
</s>
</p>
<p>
<s>
zámek	zámek	k1gInSc1	zámek
Jaroměřice	Jaroměřice	k1gFnPc4	Jaroměřice
nad	nad	k7c7	nad
Rokytnou	Rokytný	k2eAgFnSc7d1	Rokytná
</s>
</p>
<p>
<s>
zámek	zámek	k1gInSc1	zámek
Buchlovice	Buchlovice	k1gFnPc4	Buchlovice
</s>
</p>
<p>
<s>
zámek	zámek	k1gInSc4	zámek
Mnichovo	mnichův	k2eAgNnSc1d1	Mnichovo
Hradiště	Hradiště	k1gNnSc1	Hradiště
</s>
</p>
<p>
<s>
zámek	zámek	k1gInSc1	zámek
Boskovice	Boskovice	k1gInPc1	Boskovice
</s>
</p>
<p>
<s>
zámek	zámek	k1gInSc1	zámek
Liteň	Liteň	k1gFnSc1	Liteň
</s>
</p>
<p>
<s>
zámek	zámek	k1gInSc1	zámek
Milotice	Milotice	k1gFnSc2	Milotice
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
HEROUT	HEROUT	kA	HEROUT
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Staletí	staletý	k2eAgMnPc1d1	staletý
kolem	kolem	k7c2	kolem
nás	my	k3xPp1nPc2	my
<g/>
.	.	kIx.	.
</s>
<s>
Přehled	přehled	k1gInSc1	přehled
stavebních	stavební	k2eAgInPc2d1	stavební
slohů	sloh	k1gInPc2	sloh
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Panorama	panorama	k1gNnSc1	panorama
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
396	[number]	k4	396
<g/>
+	+	kIx~	+
<g/>
32	[number]	k4	32
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
563	[number]	k4	563
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
168	[number]	k4	168
<g/>
,	,	kIx,	,
V	V	kA	V
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sala	Salus	k1gMnSc2	Salus
terrena	terren	k1gMnSc2	terren
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
