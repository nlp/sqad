<s>
Majurakši	Majuraksat	k5eAaPmIp1nSwK	Majuraksat
nebo	nebo	k8xC	nebo
Mor	mor	k1gInSc1	mor
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Mayurakshi	Mayurakshi	k1gNnSc1	Mayurakshi
River	Rivra	k1gFnPc2	Rivra
nebo	nebo	k8xC	nebo
Mor	mora	k1gFnPc2	mora
River	Rivra	k1gFnPc2	Rivra
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
řeka	řeka	k1gFnSc1	řeka
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Západní	západní	k2eAgNnSc1d1	západní
Bengálsko	Bengálsko	k1gNnSc1	Bengálsko
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
250	[number]	k4	250
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Pramení	pramenit	k5eAaImIp3nS	pramenit
na	na	k7c6	na
vysočině	vysočina	k1gFnSc6	vysočina
Radžmahal	Radžmahal	k1gFnSc2	Radžmahal
a	a	k8xC	a
ústí	ústit	k5eAaImIp3nS	ústit
zprava	zprava	k6eAd1	zprava
do	do	k7c2	do
ramene	rameno	k1gNnSc2	rameno
delty	delta	k1gFnSc2	delta
Gangy	Ganga	k1gFnSc2	Ganga
zvaného	zvaný	k2eAgInSc2d1	zvaný
Hugli	Hugl	k1gMnPc7	Hugl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
velký	velký	k2eAgInSc1d1	velký
hydrouzel	hydrouzel	k1gInSc1	hydrouzel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
hráz	hráz	k1gFnSc4	hráz
<g/>
,	,	kIx,	,
přehradní	přehradní	k2eAgFnSc4d1	přehradní
nádrž	nádrž	k1gFnSc4	nádrž
a	a	k8xC	a
systém	systém	k1gInSc4	systém
zavlažovacích	zavlažovací	k2eAgInPc2d1	zavlažovací
kanálů	kanál	k1gInPc2	kanál
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
délce	délka	k1gFnSc6	délka
1	[number]	k4	1
500	[number]	k4	500
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgFnP	být
použity	použit	k2eAgFnPc1d1	použita
informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Velké	velký	k2eAgFnSc2d1	velká
sovětské	sovětský	k2eAgFnSc2d1	sovětská
encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
<g/>
,	,	kIx,	,
heslo	heslo	k1gNnSc1	heslo
"	"	kIx"	"
<g/>
Mop	mop	k1gInSc1	mop
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
