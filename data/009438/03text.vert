<p>
<s>
Uhrovské	Uhrovské	k2eAgFnSc1d1	Uhrovské
Podhradie	Podhradie	k1gFnSc1	Podhradie
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc1	obec
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Bánovce	Bánovec	k1gInSc2	Bánovec
nad	nad	k7c7	nad
Bebravou	Bebrava	k1gFnSc7	Bebrava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poloha	poloha	k1gFnSc1	poloha
==	==	k?	==
</s>
</p>
<p>
<s>
Malá	malý	k2eAgFnSc1d1	malá
podhorská	podhorský	k2eAgFnSc1d1	podhorská
obec	obec	k1gFnSc1	obec
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Nitrických	Nitrický	k2eAgInPc2d1	Nitrický
vrchů	vrch	k1gInPc2	vrch
<g/>
,	,	kIx,	,
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Podhradského	podhradský	k2eAgInSc2d1	podhradský
potoka	potok	k1gInSc2	potok
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
asi	asi	k9	asi
10	[number]	k4	10
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Bánovců	Bánovec	k1gInPc2	Bánovec
nad	nad	k7c7	nad
Bebravou	Bebrava	k1gFnSc7	Bebrava
<g/>
,	,	kIx,	,
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
se	se	k3xPyFc4	se
vypíná	vypínat	k5eAaImIp3nS	vypínat
poměrně	poměrně	k6eAd1	poměrně
zachovalá	zachovalý	k2eAgFnSc1d1	zachovalá
ruina	ruina	k1gFnSc1	ruina
Uhroveckého	Uhrovecký	k2eAgInSc2d1	Uhrovecký
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Turistika	turistika	k1gFnSc1	turistika
==	==	k?	==
</s>
</p>
<p>
<s>
Poloha	poloha	k1gFnSc1	poloha
obce	obec	k1gFnSc2	obec
v	v	k7c6	v
lůně	lůno	k1gNnSc6	lůno
přírody	příroda	k1gFnSc2	příroda
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
vhodné	vhodný	k2eAgFnPc4d1	vhodná
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
turistiku	turistika	k1gFnSc4	turistika
<g/>
.	.	kIx.	.
</s>
<s>
Blízký	blízký	k2eAgInSc1d1	blízký
Uhrovecký	Uhrovecký	k2eAgInSc1d1	Uhrovecký
hrad	hrad	k1gInSc1	hrad
láká	lákat	k5eAaImIp3nS	lákat
nadšence	nadšenka	k1gFnSc3	nadšenka
historie	historie	k1gFnSc2	historie
<g/>
;	;	kIx,	;
milovníci	milovník	k1gMnPc1	milovník
zachovalé	zachovalý	k2eAgFnSc2d1	zachovalá
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
turistiky	turistika	k1gFnSc2	turistika
rádi	rád	k2eAgMnPc1d1	rád
využijí	využít	k5eAaPmIp3nP	využít
turistické	turistický	k2eAgFnPc1d1	turistická
stezky	stezka	k1gFnPc1	stezka
do	do	k7c2	do
Nitrických	Nitrický	k2eAgInPc2d1	Nitrický
vrchů	vrch	k1gInPc2	vrch
<g/>
.	.	kIx.	.
</s>
<s>
Masiv	masiv	k1gInSc1	masiv
Rokoš	Rokoš	k1gMnSc1	Rokoš
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
chráněná	chráněný	k2eAgNnPc4d1	chráněné
území	území	k1gNnPc4	území
NPR	NPR	kA	NPR
Rokoš	Rokoš	k1gMnSc1	Rokoš
a	a	k8xC	a
přírodní	přírodní	k2eAgFnSc4d1	přírodní
rezervaci	rezervace	k1gFnSc4	rezervace
Jedlie	Jedlie	k1gFnSc2	Jedlie
<g/>
,	,	kIx,	,
relativně	relativně	k6eAd1	relativně
dostupná	dostupný	k2eAgFnSc1d1	dostupná
je	být	k5eAaImIp3nS	být
i	i	k9	i
přírodní	přírodní	k2eAgFnSc1d1	přírodní
rezervace	rezervace	k1gFnSc1	rezervace
Jankov	Jankov	k1gInSc4	Jankov
vŕšok	vŕšok	k1gInSc1	vŕšok
a	a	k8xC	a
Čerešňová	Čerešňový	k2eAgFnSc1d1	Čerešňový
jaskyňa	jaskyňa	k1gFnSc1	jaskyňa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Značené	značený	k2eAgFnPc1d1	značená
stezky	stezka	k1gFnPc1	stezka
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
značce	značka	k1gFnSc6	značka
na	na	k7c4	na
Uhrovecký	Uhrovecký	k2eAgInSc4d1	Uhrovecký
hrad	hrad	k1gInSc4	hrad
</s>
</p>
<p>
<s>
Odtud	odtud	k6eAd1	odtud
po	po	k7c6	po
značce	značka	k1gFnSc6	značka
na	na	k7c4	na
Holý	holý	k2eAgInSc4d1	holý
vrch	vrch	k1gInSc4	vrch
688	[number]	k4	688
m	m	kA	m
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
po	po	k7c6	po
značce	značka	k1gFnSc6	značka
na	na	k7c4	na
Jankov	Jankov	k1gInSc4	Jankov
vŕšok	vŕšok	k1gInSc4	vŕšok
</s>
</p>
<p>
<s>
Po	po	k7c6	po
značce	značka	k1gFnSc6	značka
na	na	k7c4	na
Rokoš	Rokoš	k1gMnSc1	Rokoš
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Uhrovské	Uhrovský	k2eAgFnSc2d1	Uhrovský
Podhradie	Podhradie	k1gFnSc2	Podhradie
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
