<p>
<s>
Pojmem	pojem	k1gInSc7	pojem
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
migrace	migrace	k1gFnSc2	migrace
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
barbarských	barbarský	k2eAgInPc2d1	barbarský
kmenů	kmen	k1gInPc2	kmen
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
skupin	skupina	k1gFnPc2	skupina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
probíhaly	probíhat	k5eAaImAgFnP	probíhat
koncem	koncem	k7c2	koncem
starověku	starověk	k1gInSc2	starověk
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
příčinou	příčina	k1gFnSc7	příčina
byly	být	k5eAaImAgFnP	být
demografické	demografický	k2eAgFnPc1d1	demografická
změny	změna	k1gFnPc1	změna
<g/>
,	,	kIx,	,
především	především	k9	především
růst	růst	k1gInSc1	růst
počtu	počet	k1gInSc2	počet
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
způsobený	způsobený	k2eAgInSc1d1	způsobený
přechodem	přechod	k1gInSc7	přechod
od	od	k7c2	od
pastevectví	pastevectví	k1gNnSc2	pastevectví
k	k	k7c3	k
zemědělství	zemědělství	k1gNnSc3	zemědělství
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
k	k	k7c3	k
usedlému	usedlý	k2eAgInSc3d1	usedlý
způsobu	způsob	k1gInSc3	způsob
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
i	i	k9	i
sociální	sociální	k2eAgInPc4d1	sociální
důvody	důvod	k1gInPc4	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
primitivní	primitivní	k2eAgInSc4d1	primitivní
způsob	způsob	k1gInSc4	způsob
obdělávání	obdělávání	k1gNnSc2	obdělávání
půdy	půda	k1gFnSc2	půda
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
rychlému	rychlý	k2eAgNnSc3d1	rychlé
vyčerpání	vyčerpání	k1gNnSc3	vyčerpání
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
dalším	další	k2eAgInSc7d1	další
důvodem	důvod	k1gInSc7	důvod
ke	k	k7c3	k
stěhování	stěhování	k1gNnSc3	stěhování
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
jak	jak	k6eAd1	jak
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
archeologické	archeologický	k2eAgInPc4d1	archeologický
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
nemuselo	muset	k5eNaImAgNnS	muset
jít	jít	k5eAaImF	jít
o	o	k7c4	o
stěhování	stěhování	k1gNnSc4	stěhování
celých	celý	k2eAgInPc2d1	celý
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
vládnoucích	vládnoucí	k2eAgFnPc2d1	vládnoucí
elit	elita	k1gFnPc2	elita
<g/>
.	.	kIx.	.
<g/>
Kmeny	kmen	k1gInPc1	kmen
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
počet	počet	k1gInSc1	počet
členů	člen	k1gInPc2	člen
rostl	růst	k5eAaImAgInS	růst
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
hledat	hledat	k5eAaImF	hledat
novou	nový	k2eAgFnSc4d1	nová
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
ji	on	k3xPp3gFnSc4	on
nacházely	nacházet	k5eAaImAgFnP	nacházet
na	na	k7c6	na
území	území	k1gNnSc6	území
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
nacházela	nacházet	k5eAaImAgFnS	nacházet
v	v	k7c6	v
hluboké	hluboký	k2eAgFnSc6d1	hluboká
krizi	krize	k1gFnSc6	krize
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
hranice	hranice	k1gFnPc1	hranice
nebyly	být	k5eNaImAgFnP	být
dostatečně	dostatečně	k6eAd1	dostatečně
chráněné	chráněný	k2eAgFnPc1d1	chráněná
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
území	území	k1gNnSc6	území
říše	říše	k1gFnSc1	říše
možné	možný	k2eAgNnSc1d1	možné
získat	získat	k5eAaPmF	získat
bohatou	bohatý	k2eAgFnSc4d1	bohatá
kořist	kořist	k1gFnSc4	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
impérium	impérium	k1gNnSc1	impérium
se	se	k3xPyFc4	se
v	v	k7c6	v
pozdně	pozdně	k6eAd1	pozdně
antickém	antický	k2eAgNnSc6d1	antické
období	období	k1gNnSc6	období
stalo	stát	k5eAaPmAgNnS	stát
lákavým	lákavý	k2eAgInSc7d1	lákavý
cílem	cíl	k1gInSc7	cíl
nájezdů	nájezd	k1gInPc2	nájezd
barbarských	barbarský	k2eAgInPc2d1	barbarský
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
na	na	k7c6	na
takové	takový	k3xDgFnSc6	takový
výpravě	výprava	k1gFnSc6	výprava
uspěli	uspět	k5eAaPmAgMnP	uspět
<g/>
,	,	kIx,	,
získali	získat	k5eAaPmAgMnP	získat
nejen	nejen	k6eAd1	nejen
kořist	kořist	k1gFnSc4	kořist
a	a	k8xC	a
novou	nový	k2eAgFnSc4d1	nová
půdu	půda	k1gFnSc4	půda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
vážnost	vážnost	k1gFnSc4	vážnost
a	a	k8xC	a
prestiž	prestiž	k1gFnSc4	prestiž
u	u	k7c2	u
svého	svůj	k3xOyFgInSc2	svůj
kmene	kmen	k1gInSc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
patřilo	patřit	k5eAaImAgNnS	patřit
mezi	mezi	k7c4	mezi
sociální	sociální	k2eAgFnPc4d1	sociální
příčiny	příčina	k1gFnPc4	příčina
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostřední	bezprostřední	k2eAgFnSc7d1	bezprostřední
hybnou	hybný	k2eAgFnSc7d1	hybná
silou	síla	k1gFnSc7	síla
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
často	často	k6eAd1	často
také	také	k9	také
tlak	tlak	k1gInSc1	tlak
jiných	jiný	k2eAgInPc2d1	jiný
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Všeobecně	všeobecně	k6eAd1	všeobecně
==	==	k?	==
</s>
</p>
<p>
<s>
Vlastním	vlastní	k2eAgNnSc7d1	vlastní
stěhováním	stěhování	k1gNnSc7	stěhování
národů	národ	k1gInPc2	národ
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
rozumí	rozumět	k5eAaImIp3nS	rozumět
pohyb	pohyb	k1gInSc1	pohyb
germánských	germánský	k2eAgInPc2d1	germánský
a	a	k8xC	a
slovanských	slovanský	k2eAgInPc2d1	slovanský
kmenů	kmen	k1gInPc2	kmen
začínající	začínající	k2eAgFnSc1d1	začínající
ve	v	k7c4	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
končící	končící	k2eAgMnPc1d1	končící
koncem	koncem	k7c2	koncem
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
germánské	germánský	k2eAgFnSc2d1	germánská
etapy	etapa	k1gFnSc2	etapa
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
znamená	znamenat	k5eAaImIp3nS	znamenat
na	na	k7c6	na
západě	západ	k1gInSc6	západ
současně	současně	k6eAd1	současně
konec	konec	k1gInSc4	konec
pozdní	pozdní	k2eAgFnSc2d1	pozdní
antiky	antika	k1gFnSc2	antika
resp.	resp.	kA	resp.
římského	římský	k2eAgNnSc2d1	římské
císařství	císařství	k1gNnSc2	císařství
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
raný	raný	k2eAgInSc4d1	raný
středověk	středověk	k1gInSc4	středověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
zahrnovány	zahrnovat	k5eAaImNgFnP	zahrnovat
také	také	k9	také
vikingské	vikingský	k2eAgFnPc1d1	vikingská
výpravy	výprava	k1gFnPc1	výprava
a	a	k8xC	a
příchod	příchod	k1gInSc1	příchod
Maďarů	Maďar	k1gMnPc2	Maďar
do	do	k7c2	do
Karpatské	karpatský	k2eAgFnSc2d1	Karpatská
kotliny	kotlina	k1gFnSc2	kotlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Geograficky	geograficky	k6eAd1	geograficky
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
představit	představit	k5eAaPmF	představit
přímku	přímka	k1gFnSc4	přímka
protínající	protínající	k2eAgFnSc4d1	protínající
Evropu	Evropa	k1gFnSc4	Evropa
od	od	k7c2	od
ústí	ústí	k1gNnSc2	ústí
Rýna	Rýn	k1gInSc2	Rýn
až	až	k9	až
k	k	k7c3	k
ústí	ústí	k1gNnSc3	ústí
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
linie	linie	k1gFnSc2	linie
sídlily	sídlit	k5eAaImAgFnP	sídlit
ve	v	k7c4	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
germánské	germánský	k2eAgInPc1d1	germánský
kmeny	kmen	k1gInPc1	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Jmenovitě	jmenovitě	k6eAd1	jmenovitě
(	(	kIx(	(
<g/>
řazeny	řadit	k5eAaImNgInP	řadit
od	od	k7c2	od
západu	západ	k1gInSc2	západ
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
)	)	kIx)	)
to	ten	k3xDgNnSc1	ten
byli	být	k5eAaImAgMnP	být
Sasové	Sas	k1gMnPc1	Sas
<g/>
,	,	kIx,	,
Frankové	Frank	k1gMnPc1	Frank
<g/>
,	,	kIx,	,
Burgundi	Burgund	k1gMnPc1	Burgund
<g/>
,	,	kIx,	,
Langobardi	Langobard	k1gMnPc1	Langobard
<g/>
,	,	kIx,	,
Vandalové	Vandal	k1gMnPc1	Vandal
<g/>
,	,	kIx,	,
Vizigóti	Vizigót	k1gMnPc1	Vizigót
a	a	k8xC	a
Ostrogóti	Ostrogót	k1gMnPc1	Ostrogót
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
linii	linie	k1gFnSc4	linie
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
rovněž	rovněž	k9	rovněž
představit	představit	k5eAaPmF	představit
jako	jako	k8xC	jako
vrata	vrata	k1gNnPc4	vrata
od	od	k7c2	od
stodoly	stodola	k1gFnSc2	stodola
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
stěžejním	stěžejní	k2eAgInSc7d1	stěžejní
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
ústí	ústí	k1gNnSc1	ústí
Rýna	Rýn	k1gInSc2	Rýn
<g/>
.	.	kIx.	.
</s>
<s>
Vpád	vpád	k1gInSc1	vpád
nomádských	nomádský	k2eAgMnPc2d1	nomádský
Hunů	Hun	k1gMnPc2	Hun
<g/>
,	,	kIx,	,
příchozích	příchozí	k1gMnPc2	příchozí
ze	z	k7c2	z
středoasijských	středoasijský	k2eAgFnPc2d1	středoasijská
stepí	step	k1gFnPc2	step
v	v	k7c6	v
roce	rok	k1gInSc6	rok
375	[number]	k4	375
<g/>
,	,	kIx,	,
tato	tento	k3xDgNnPc4	tento
vrata	vrata	k1gNnPc4	vrata
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
a	a	k8xC	a
vyhnal	vyhnat	k5eAaPmAgMnS	vyhnat
zde	zde	k6eAd1	zde
usídlené	usídlený	k2eAgInPc4d1	usídlený
germánské	germánský	k2eAgInPc4d1	germánský
kmeny	kmen	k1gInPc4	kmen
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
sídlily	sídlit	k5eAaImAgFnP	sídlit
nejdále	daleko	k6eAd3	daleko
na	na	k7c6	na
východě	východ	k1gInSc6	východ
(	(	kIx(	(
<g/>
Vandalové	Vandal	k1gMnPc1	Vandal
<g/>
,	,	kIx,	,
Vizigóti	Vizigót	k1gMnPc1	Vizigót
a	a	k8xC	a
Ostrogóti	Ostrogót	k1gMnPc1	Ostrogót
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
účastnily	účastnit	k5eAaImAgFnP	účastnit
nejdelšího	dlouhý	k2eAgNnSc2d3	nejdelší
přesunu	přesunout	k5eAaPmIp1nS	přesunout
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
Frankové	Frank	k1gMnPc1	Frank
zcela	zcela	k6eAd1	zcela
na	na	k7c6	na
západě	západ	k1gInSc6	západ
se	se	k3xPyFc4	se
přemístili	přemístit	k5eAaPmAgMnP	přemístit
z	z	k7c2	z
Dolního	dolní	k2eAgNnSc2d1	dolní
Porýní	Porýní	k1gNnSc2	Porýní
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
blízké	blízký	k2eAgFnSc2d1	blízká
severní	severní	k2eAgFnSc2d1	severní
Galie	Galie	k1gFnSc2	Galie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Motivy	motiv	k1gInPc1	motiv
stěhování	stěhování	k1gNnSc2	stěhování
nejsou	být	k5eNaImIp3nP	být
úplně	úplně	k6eAd1	úplně
zřejmé	zřejmý	k2eAgFnPc1d1	zřejmá
<g/>
.	.	kIx.	.
</s>
<s>
Počáteční	počáteční	k2eAgInSc1d1	počáteční
pohyb	pohyb	k1gInSc1	pohyb
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
kmenů	kmen	k1gInPc2	kmen
(	(	kIx(	(
<g/>
především	především	k9	především
Hunů	Hun	k1gMnPc2	Hun
<g/>
)	)	kIx)	)
a	a	k8xC	a
následný	následný	k2eAgInSc1d1	následný
tlak	tlak	k1gInSc1	tlak
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
novému	nový	k2eAgNnSc3d1	nové
rozmístění	rozmístění	k1gNnSc3	rozmístění
kmenů	kmen	k1gInPc2	kmen
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
si	se	k3xPyFc3	se
všimnout	všimnout	k5eAaPmF	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
kmeny	kmen	k1gInPc1	kmen
nebo	nebo	k8xC	nebo
národy	národ	k1gInPc1	národ
postrádaly	postrádat	k5eAaImAgInP	postrádat
trvalou	trvalý	k2eAgFnSc4d1	trvalá
jednotu	jednota	k1gFnSc4	jednota
a	a	k8xC	a
společný	společný	k2eAgInSc4d1	společný
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
Kmeny	kmen	k1gInPc1	kmen
nebyly	být	k5eNaImAgInP	být
homogenním	homogenní	k2eAgInSc7d1	homogenní
útvarem	útvar	k1gInSc7	útvar
tvořeným	tvořený	k2eAgInSc7d1	tvořený
ryze	ryze	k6eAd1	ryze
jedním	jeden	k4xCgNnSc7	jeden
společenstvím	společenství	k1gNnSc7	společenství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
zahrnovaly	zahrnovat	k5eAaImAgInP	zahrnovat
více	hodně	k6eAd2	hodně
takových	takový	k3xDgNnPc2	takový
společenství	společenství	k1gNnSc2	společenství
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jedno	jeden	k4xCgNnSc1	jeden
bylo	být	k5eAaImAgNnS	být
dominantní	dominantní	k2eAgMnSc1d1	dominantní
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
velikost	velikost	k1gFnSc1	velikost
a	a	k8xC	a
složení	složení	k1gNnSc1	složení
kmene	kmen	k1gInSc2	kmen
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
silně	silně	k6eAd1	silně
proměnlivé	proměnlivý	k2eAgNnSc1d1	proměnlivé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pomalý	pomalý	k2eAgInSc1d1	pomalý
úpadek	úpadek	k1gInSc1	úpadek
západořímské	západořímský	k2eAgFnSc2d1	Západořímská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
související	související	k2eAgFnSc1d1	související
ztráta	ztráta	k1gFnSc1	ztráta
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
správních	správní	k2eAgFnPc2d1	správní
struktur	struktura	k1gFnPc2	struktura
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
rostoucí	rostoucí	k2eAgNnSc1d1	rostoucí
začleňování	začleňování	k1gNnSc1	začleňování
Germánů	Germán	k1gMnPc2	Germán
jako	jako	k8xC	jako
spojenců	spojenec	k1gMnPc2	spojenec
(	(	kIx(	(
<g/>
federátů	federát	k1gMnPc2	federát
<g/>
)	)	kIx)	)
a	a	k8xC	a
jako	jako	k9	jako
vojáků	voják	k1gMnPc2	voják
do	do	k7c2	do
římského	římský	k2eAgNnSc2d1	římské
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
vytvářelo	vytvářet	k5eAaImAgNnS	vytvářet
další	další	k2eAgInSc4d1	další
předpoklad	předpoklad	k1gInSc4	předpoklad
pro	pro	k7c4	pro
stěhování	stěhování	k1gNnSc4	stěhování
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Přičemž	přičemž	k6eAd1	přičemž
větší	veliký	k2eAgInSc1d2	veliký
význam	význam	k1gInSc1	význam
než	než	k8xS	než
samotná	samotný	k2eAgFnSc1d1	samotná
barbarizace	barbarizace	k1gFnSc1	barbarizace
římského	římský	k2eAgNnSc2d1	římské
vojska	vojsko	k1gNnSc2	vojsko
měla	mít	k5eAaImAgFnS	mít
přítomnost	přítomnost	k1gFnSc1	přítomnost
germánských	germánský	k2eAgMnPc2d1	germánský
federátů	federát	k1gMnPc2	federát
na	na	k7c6	na
území	území	k1gNnSc6	území
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
nechali	nechat	k5eAaPmAgMnP	nechat
císaři	císař	k1gMnPc1	císař
stále	stále	k6eAd1	stále
méně	málo	k6eAd2	málo
ovládat	ovládat	k5eAaImF	ovládat
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
vytvářeli	vytvářet	k5eAaImAgMnP	vytvářet
fakticky	fakticky	k6eAd1	fakticky
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
říše	říš	k1gFnSc2	říš
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
spíše	spíše	k9	spíše
o	o	k7c6	o
transformaci	transformace	k1gFnSc6	transformace
-	-	kIx~	-
přeměně	přeměna	k1gFnSc6	přeměna
-	-	kIx~	-
římského	římský	k2eAgInSc2d1	římský
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
než	než	k8xS	než
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
regulérním	regulérní	k2eAgNnSc6d1	regulérní
dobytí	dobytí	k1gNnSc6	dobytí
Germány	Germán	k1gMnPc7	Germán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stěhování	stěhování	k1gNnSc1	stěhování
národů	národ	k1gInPc2	národ
od	od	k7c2	od
markomanských	markomanský	k2eAgFnPc2d1	markomanská
válek	válka	k1gFnPc2	válka
až	až	k9	až
do	do	k7c2	do
vpádu	vpád	k1gInSc2	vpád
Hunů	Hun	k1gMnPc2	Hun
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Gótové	Gót	k1gMnPc5	Gót
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
historických	historický	k2eAgFnPc2d1	historická
tradic	tradice	k1gFnPc2	tradice
pocházejí	pocházet	k5eAaImIp3nP	pocházet
Gótové	Gót	k1gMnPc1	Gót
původně	původně	k6eAd1	původně
ze	z	k7c2	z
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
však	však	k9	však
podle	podle	k7c2	podle
moderních	moderní	k2eAgInPc2d1	moderní
výzkumů	výzkum	k1gInPc2	výzkum
přinejmenším	přinejmenším	k6eAd1	přinejmenším
sporné	sporný	k2eAgNnSc1d1	sporné
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
letopočtu	letopočet	k1gInSc2	letopočet
sídlili	sídlit	k5eAaImAgMnP	sídlit
Gótové	Gót	k1gMnPc1	Gót
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Visly	Visla	k1gFnSc2	Visla
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
odešli	odejít	k5eAaPmAgMnP	odejít
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Černému	černý	k2eAgNnSc3d1	černé
moři	moře	k1gNnSc3	moře
<g/>
,	,	kIx,	,
a	a	k8xC	a
zapříčinili	zapříčinit	k5eAaPmAgMnP	zapříčinit
tak	tak	k6eAd1	tak
první	první	k4xOgNnSc4	první
větší	veliký	k2eAgNnSc4d2	veliký
stěhování	stěhování	k1gNnSc4	stěhování
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vytlačili	vytlačit	k5eAaPmAgMnP	vytlačit
Vandaly	Vandal	k1gMnPc4	Vandal
a	a	k8xC	a
Markomany	Markoman	k1gMnPc4	Markoman
na	na	k7c4	na
jih	jih	k1gInSc4	jih
a	a	k8xC	a
Burgundy	Burgundy	k1gInPc4	Burgundy
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
posun	posun	k1gInSc1	posun
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
příčinou	příčina	k1gFnSc7	příčina
markomanských	markomanský	k2eAgFnPc2d1	markomanská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
drobným	drobný	k2eAgInPc3d1	drobný
vpádům	vpád	k1gInPc3	vpád
na	na	k7c4	na
římské	římský	k2eAgNnSc4d1	římské
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
buď	buď	k8xC	buď
odraženy	odražen	k2eAgFnPc1d1	odražena
anebo	anebo	k8xC	anebo
skončily	skončit	k5eAaPmAgFnP	skončit
drobnými	drobný	k2eAgFnPc7d1	drobná
korekturami	korektura	k1gFnPc7	korektura
hranic	hranice	k1gFnPc2	hranice
(	(	kIx(	(
<g/>
Agri	Agri	k1gNnPc1	Agri
Decumates	Decumatesa	k1gFnPc2	Decumatesa
<g/>
,	,	kIx,	,
Dácie	Dácie	k1gFnSc1	Dácie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
kmenů	kmen	k1gInPc2	kmen
bylo	být	k5eAaImAgNnS	být
usazeno	usazen	k2eAgNnSc1d1	usazeno
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
jako	jako	k8xC	jako
spojenci	spojenec	k1gMnPc1	spojenec
(	(	kIx(	(
<g/>
foederati	foederat	k1gMnPc1	foederat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tvořilo	tvořit	k5eAaImAgNnS	tvořit
tak	tak	k9	tak
nárazník	nárazník	k1gInSc4	nárazník
vůči	vůči	k7c3	vůči
Římu	Řím	k1gInSc3	Řím
nepřátelsky	přátelsky	k6eNd1	přátelsky
smýšlejícím	smýšlející	k2eAgInPc3d1	smýšlející
kmenům	kmen	k1gInPc3	kmen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gótové	Gót	k1gMnPc1	Gót
se	se	k3xPyFc4	se
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
3	[number]	k4	3
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Jednak	jednak	k8xC	jednak
to	ten	k3xDgNnSc4	ten
byli	být	k5eAaImAgMnP	být
Tervingové	Tervingový	k2eAgFnPc4d1	Tervingový
<g/>
/	/	kIx~	/
<g/>
západní	západní	k2eAgMnPc1d1	západní
Gótové	Gót	k1gMnPc1	Gót
(	(	kIx(	(
<g/>
kteří	který	k3yRgMnPc1	který
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
zcela	zcela	k6eAd1	zcela
identičtí	identický	k2eAgMnPc1d1	identický
s	s	k7c7	s
pozdějšími	pozdní	k2eAgMnPc7d2	pozdější
Vizigóty	Vizigót	k1gMnPc7	Vizigót
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
ovlivněni	ovlivnit	k5eAaPmNgMnP	ovlivnit
také	také	k6eAd1	také
druhou	druhý	k4xOgFnSc7	druhý
skupinou	skupina	k1gFnSc7	skupina
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednak	jednak	k8xC	jednak
to	ten	k3xDgNnSc4	ten
byli	být	k5eAaImAgMnP	být
Greutungové	Greutung	k1gMnPc1	Greutung
<g/>
/	/	kIx~	/
<g/>
východní	východní	k2eAgMnPc1d1	východní
Gótové	Gót	k1gMnPc1	Gót
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
Ostrogóty	Ostrogót	k1gMnPc4	Ostrogót
to	ten	k3xDgNnSc1	ten
samé	samý	k3xTgFnSc6	samý
<g/>
,	,	kIx,	,
co	co	k9	co
pro	pro	k7c4	pro
Vizigóty	Vizigót	k1gMnPc4	Vizigót
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgMnPc1d1	východní
Gótové	Gót	k1gMnPc1	Gót
sídlili	sídlit	k5eAaImAgMnP	sídlit
u	u	k7c2	u
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgMnPc1d1	západní
pobývali	pobývat	k5eAaImAgMnP	pobývat
zatím	zatím	k6eAd1	zatím
na	na	k7c6	na
Balkánském	balkánský	k2eAgInSc6d1	balkánský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
,	,	kIx,	,
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
severně	severně	k6eAd1	severně
od	od	k7c2	od
Dunaje	Dunaj	k1gInSc2	Dunaj
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Od	od	k7c2	od
vpádu	vpád	k1gInSc2	vpád
Hunů	Hun	k1gMnPc2	Hun
až	až	k9	až
k	k	k7c3	k
dobytí	dobytí	k1gNnSc3	dobytí
Říma	Řím	k1gInSc2	Řím
v	v	k7c6	v
roce	rok	k1gInSc6	rok
410	[number]	k4	410
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
tlačili	tlačit	k5eAaImAgMnP	tlačit
Hunové	Hun	k1gMnPc1	Hun
ze	z	k7c2	z
stepí	step	k1gFnPc2	step
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
do	do	k7c2	do
jižního	jižní	k2eAgNnSc2d1	jižní
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
způsobili	způsobit	k5eAaPmAgMnP	způsobit
pohyb	pohyb	k1gInSc4	pohyb
četných	četný	k2eAgInPc2d1	četný
germánských	germánský	k2eAgInPc2d1	germánský
a	a	k8xC	a
sarmatských	sarmatský	k2eAgInPc2d1	sarmatský
kmenů	kmen	k1gInPc2	kmen
do	do	k7c2	do
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
375	[number]	k4	375
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
Hunům	Hun	k1gMnPc3	Hun
nejprve	nejprve	k6eAd1	nejprve
východní	východní	k2eAgMnPc1d1	východní
Gótové	Gót	k1gMnPc1	Gót
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
rovněž	rovněž	k9	rovněž
Tervingové	Tervingový	k2eAgFnSc3d1	Tervingový
(	(	kIx(	(
<g/>
západní	západní	k2eAgMnPc1d1	západní
Gótové	Gót	k1gMnPc1	Gót
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
vytlačeni	vytlačit	k5eAaPmNgMnP	vytlačit
z	z	k7c2	z
území	území	k1gNnSc2	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xS	jako
začátek	začátek	k1gInSc1	začátek
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následného	následný	k2eAgInSc2d1	následný
chaosu	chaos	k1gInSc2	chaos
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
fragmentaci	fragmentace	k1gFnSc4	fragmentace
obou	dva	k4xCgInPc2	dva
kmenů	kmen	k1gInPc2	kmen
(	(	kIx(	(
<g/>
Peter	Peter	k1gMnSc1	Peter
Heather	Heathra	k1gFnPc2	Heathra
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
až	až	k9	až
dvanáct	dvanáct	k4xCc4	dvanáct
různých	různý	k2eAgNnPc2d1	různé
gótských	gótský	k2eAgNnPc2d1	gótské
uskupení	uskupení	k1gNnPc2	uskupení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vizigóti	Vizigót	k1gMnPc5	Vizigót
===	===	k?	===
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
poražených	poražený	k2eAgMnPc2d1	poražený
východních	východní	k2eAgMnPc2d1	východní
Gótů	Gót	k1gMnPc2	Gót
(	(	kIx(	(
<g/>
pozdější	pozdní	k2eAgMnPc1d2	pozdější
Ostrogóti	Ostrogót	k1gMnPc1	Ostrogót
<g/>
)	)	kIx)	)
upadla	upadnout	k5eAaPmAgFnS	upadnout
pod	pod	k7c4	pod
nadvládu	nadvláda	k1gFnSc4	nadvláda
Hunů	Hun	k1gMnPc2	Hun
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Tervingové	Tervingový	k2eAgNnSc1d1	Tervingový
(	(	kIx(	(
<g/>
pozdější	pozdní	k2eAgMnPc1d2	pozdější
Vizigóti	Vizigót	k1gMnPc1	Vizigót
<g/>
)	)	kIx)	)
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
přes	přes	k7c4	přes
Dunaj	Dunaj	k1gInSc4	Dunaj
do	do	k7c2	do
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
Gótové	Gót	k1gMnPc1	Gót
byli	být	k5eAaImAgMnP	být
usazeni	usadit	k5eAaPmNgMnP	usadit
císařem	císař	k1gMnSc7	císař
Valentem	Valentem	k?	Valentem
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Thrákii	Thrákie	k1gFnSc4	Thrákie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vinou	vina	k1gFnSc7	vina
nedostatečného	dostatečný	k2eNgNnSc2d1	nedostatečné
zásobování	zásobování	k1gNnSc2	zásobování
došlo	dojít	k5eAaPmAgNnS	dojít
brzy	brzy	k6eAd1	brzy
k	k	k7c3	k
revoltě	revolta	k1gFnSc3	revolta
novousedlíků	novousedlík	k1gMnPc2	novousedlík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
378	[number]	k4	378
porazili	porazit	k5eAaPmAgMnP	porazit
Gótové	Gót	k1gMnPc1	Gót
Římany	Říman	k1gMnPc7	Říman
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
císařem	císař	k1gMnSc7	císař
Valentem	Valentem	k?	Valentem
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Adrianopole	Adrianopole	k1gFnSc2	Adrianopole
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
382	[number]	k4	382
sice	sice	k8xC	sice
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
mír	mír	k1gInSc4	mír
s	s	k7c7	s
císařem	císař	k1gMnSc7	císař
Theodosiem	Theodosius	k1gMnSc7	Theodosius
I.	I.	kA	I.
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
napětí	napětí	k1gNnSc1	napětí
stále	stále	k6eAd1	stále
přetrvávalo	přetrvávat	k5eAaImAgNnS	přetrvávat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
Gótové	Gót	k1gMnPc1	Gót
vedení	vedení	k1gNnSc2	vedení
svým	svůj	k1gMnSc7	svůj
náčelníkem	náčelník	k1gMnSc7	náčelník
Alarichem	Alarich	k1gMnSc7	Alarich
protáhli	protáhnout	k5eAaPmAgMnP	protáhnout
celým	celý	k2eAgInSc7d1	celý
Balkánem	Balkán	k1gInSc7	Balkán
a	a	k8xC	a
Peloponésem	Peloponés	k1gInSc7	Peloponés
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
410	[number]	k4	410
pak	pak	k6eAd1	pak
dobyli	dobýt	k5eAaPmAgMnP	dobýt
a	a	k8xC	a
vyplenili	vyplenit	k5eAaPmAgMnP	vyplenit
Řím	Řím	k1gInSc4	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
táhli	táhnout	k5eAaImAgMnP	táhnout
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
s	s	k7c7	s
konečnou	konečný	k2eAgFnSc7d1	konečná
platností	platnost	k1gFnSc7	platnost
zformovaní	zformovaný	k2eAgMnPc1d1	zformovaný
Vizigóti	Vizigót	k1gMnPc1	Vizigót
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
Galie	Galie	k1gFnSc2	Galie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
418	[number]	k4	418
usazeni	usazen	k2eAgMnPc1d1	usazen
Římany	Říman	k1gMnPc4	Říman
<g/>
,	,	kIx,	,
a	a	k8xC	a
poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
Toulouse	Toulouse	k1gInSc2	Toulouse
založili	založit	k5eAaPmAgMnP	založit
tzv.	tzv.	kA	tzv.
Tolosánskou	Tolosánský	k2eAgFnSc4d1	Tolosánský
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
507	[number]	k4	507
byli	být	k5eAaImAgMnP	být
poraženi	poražen	k2eAgMnPc1d1	poražen
Franky	Frank	k1gMnPc4	Frank
vedenými	vedený	k2eAgFnPc7d1	vedená
Chlodvíkem	Chlodvík	k1gMnSc7	Chlodvík
<g/>
.	.	kIx.	.
</s>
<s>
Vizigóti	Vizigót	k1gMnPc1	Vizigót
byli	být	k5eAaImAgMnP	být
pak	pak	k6eAd1	pak
zatlačeni	zatlačit	k5eAaPmNgMnP	zatlačit
na	na	k7c4	na
Pyrenejský	pyrenejský	k2eAgInSc4d1	pyrenejský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vládli	vládnout	k5eAaImAgMnP	vládnout
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
711	[number]	k4	711
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
vizigótská	vizigótský	k2eAgFnSc1d1	Vizigótská
říše	říše	k1gFnSc1	říše
zničena	zničit	k5eAaPmNgFnS	zničit
Araby	Arab	k1gMnPc7	Arab
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
vpádu	vpád	k1gInSc2	vpád
Hunů	Hun	k1gMnPc2	Hun
nedocházelo	docházet	k5eNaImAgNnS	docházet
zhruba	zhruba	k6eAd1	zhruba
nějakých	nějaký	k3yIgInPc2	nějaký
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
k	k	k7c3	k
žádným	žádný	k3yNgInPc3	žádný
větším	veliký	k2eAgInPc3d2	veliký
pohybům	pohyb	k1gInPc3	pohyb
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
Římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
pomalu	pomalu	k6eAd1	pomalu
slábla	slábnout	k5eAaImAgFnS	slábnout
a	a	k8xC	a
musela	muset	k5eAaImAgFnS	muset
bojovat	bojovat	k5eAaImF	bojovat
s	s	k7c7	s
rostoucími	rostoucí	k2eAgInPc7d1	rostoucí
vnitropolitickými	vnitropolitický	k2eAgInPc7d1	vnitropolitický
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
395	[number]	k4	395
(	(	kIx(	(
<g/>
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
císaře	císař	k1gMnSc2	císař
Theodosia	Theodosium	k1gNnSc2	Theodosium
I.	I.	kA	I.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozdělení	rozdělení	k1gNnSc1	rozdělení
existovalo	existovat	k5eAaImAgNnS	existovat
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
správně-technické	správněechnický	k2eAgNnSc4d1	správně-technický
rozdělení	rozdělení	k1gNnSc4	rozdělení
a	a	k8xC	a
současníci	současník	k1gMnPc1	současník
je	on	k3xPp3gNnSc4	on
proto	proto	k8xC	proto
nepovažovali	považovat	k5eNaImAgMnP	považovat
za	za	k7c4	za
skutečné	skutečný	k2eAgNnSc4d1	skutečné
rozdělení	rozdělení	k1gNnSc4	rozdělení
<g/>
.	.	kIx.	.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
z	z	k7c2	z
roku	rok	k1gInSc2	rok
395	[number]	k4	395
se	se	k3xPyFc4	se
však	však	k9	však
mělo	mít	k5eAaImAgNnS	mít
stát	stát	k5eAaPmF	stát
faktickým	faktický	k2eAgMnSc7d1	faktický
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
od	od	k7c2	od
nynějška	nynějšek	k1gInSc2	nynějšek
vyvíjely	vyvíjet	k5eAaImAgInP	vyvíjet
zcela	zcela	k6eAd1	zcela
nezávisle	závisle	k6eNd1	závisle
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
klíčových	klíčový	k2eAgInPc2d1	klíčový
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
byly	být	k5eAaImAgInP	být
barbarské	barbarský	k2eAgInPc1d1	barbarský
vpády	vpád	k1gInPc1	vpád
na	na	k7c4	na
římské	římský	k2eAgNnSc4d1	římské
území	území	k1gNnSc4	území
tak	tak	k9	tak
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samotní	samotný	k2eAgMnPc1d1	samotný
Germáni	Germán	k1gMnPc1	Germán
ale	ale	k8xC	ale
neměli	mít	k5eNaImAgMnP	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
Římskou	římský	k2eAgFnSc4d1	římská
říši	říše	k1gFnSc4	říše
zničit	zničit	k5eAaPmF	zničit
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
se	se	k3xPyFc4	se
získat	získat	k5eAaPmF	získat
na	na	k7c6	na
římské	římský	k2eAgFnSc6d1	římská
vládě	vláda	k1gFnSc6	vláda
jisté	jistý	k2eAgFnSc2d1	jistá
sídelní	sídelní	k2eAgFnSc2d1	sídelní
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
zajistili	zajistit	k5eAaPmAgMnP	zajistit
určitou	určitý	k2eAgFnSc4d1	určitá
životní	životní	k2eAgFnSc4d1	životní
základnu	základna	k1gFnSc4	základna
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc7	jejich
cílem	cíl	k1gInSc7	cíl
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
zničení	zničení	k1gNnSc2	zničení
stávající	stávající	k2eAgFnSc2d1	stávající
římské	římský	k2eAgFnSc2d1	římská
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
významné	významný	k2eAgNnSc1d1	významné
pro	pro	k7c4	pro
spolupráci	spolupráce	k1gFnSc4	spolupráce
nově	nova	k1gFnSc3	nova
příchozích	příchozí	k1gFnPc2	příchozí
Germánů	Germán	k1gMnPc2	Germán
s	s	k7c7	s
místní	místní	k2eAgFnSc7d1	místní
římskou	římský	k2eAgFnSc7d1	římská
vládnoucí	vládnoucí	k2eAgFnSc7d1	vládnoucí
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
této	tento	k3xDgFnSc2	tento
spolupráce	spolupráce	k1gFnSc2	spolupráce
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
nemohly	moct	k5eNaImAgFnP	moct
germánské	germánský	k2eAgFnPc1d1	germánská
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
co	co	k3yQnSc4	co
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
Římanům	Říman	k1gMnPc3	Říman
podléhaly	podléhat	k5eAaImAgFnP	podléhat
<g/>
,	,	kIx,	,
prosadit	prosadit	k5eAaPmF	prosadit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Útok	útok	k1gInSc1	útok
Hunů	Hun	k1gMnPc2	Hun
<g/>
,	,	kIx,	,
Attila	Attila	k1gMnSc1	Attila
a	a	k8xC	a
konec	konec	k1gInSc1	konec
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vandalové	Vandal	k1gMnPc5	Vandal
===	===	k?	===
</s>
</p>
<p>
<s>
Vandalové	Vandal	k1gMnPc1	Vandal
byli	být	k5eAaImAgMnP	být
původně	původně	k6eAd1	původně
vytlačeni	vytlačen	k2eAgMnPc1d1	vytlačen
Góty	Gót	k1gMnPc7	Gót
na	na	k7c6	na
území	území	k1gNnSc6	území
mezi	mezi	k7c7	mezi
Vislou	Visla	k1gFnSc7	Visla
a	a	k8xC	a
Odrou	Odra	k1gFnSc7	Odra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Silvestra	Silvestr	k1gMnSc4	Silvestr
roku	rok	k1gInSc2	rok
406	[number]	k4	406
překročili	překročit	k5eAaPmAgMnP	překročit
společně	společně	k6eAd1	společně
se	s	k7c7	s
Svévy	Svév	k1gMnPc7	Svév
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Suebi	Suebi	k1gNnSc1	Suebi
<g/>
)	)	kIx)	)
a	a	k8xC	a
Alany	alan	k1gInPc1	alan
zamrzlý	zamrzlý	k2eAgInSc1d1	zamrzlý
Rýn	Rýn	k1gInSc4	Rýn
a	a	k8xC	a
vpadli	vpadnout	k5eAaPmAgMnP	vpadnout
do	do	k7c2	do
Galie	Galie	k1gFnSc2	Galie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
409	[number]	k4	409
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
je	být	k5eAaImIp3nS	být
však	však	k9	však
po	po	k7c6	po
několika	několik	k4yIc6	několik
letech	léto	k1gNnPc6	léto
vytlačili	vytlačit	k5eAaPmAgMnP	vytlačit
Vizigóti	Vizigót	k1gMnPc1	Vizigót
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
západořímské	západořímský	k2eAgFnSc2d1	Západořímská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
náčelníka	náčelník	k1gMnSc2	náčelník
Geisericha	Geiserich	k1gMnSc2	Geiserich
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
porážce	porážka	k1gFnSc6	porážka
přesunuli	přesunout	k5eAaPmAgMnP	přesunout
přes	přes	k7c4	přes
Středozemní	středozemní	k2eAgNnSc4d1	středozemní
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
stanuli	stanout	k5eAaPmAgMnP	stanout
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
válčili	válčit	k5eAaImAgMnP	válčit
dalších	další	k2eAgInPc2d1	další
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
439	[number]	k4	439
dobyl	dobýt	k5eAaPmAgInS	dobýt
Geiserich	Geiserich	k1gInSc1	Geiserich
Kartágo	Kartágo	k1gNnSc1	Kartágo
(	(	kIx(	(
<g/>
poblíž	poblíž	k7c2	poblíž
dnešního	dnešní	k2eAgInSc2d1	dnešní
Tunisu	Tunis	k1gInSc2	Tunis
<g/>
)	)	kIx)	)
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
říši	říše	k1gFnSc4	říše
Vandalů	Vandal	k1gMnPc2	Vandal
<g/>
.	.	kIx.	.
</s>
<s>
Vandalové	Vandal	k1gMnPc1	Vandal
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jediný	jediný	k2eAgInSc4d1	jediný
germánský	germánský	k2eAgInSc4d1	germánský
kmen	kmen	k1gInSc4	kmen
<g/>
,	,	kIx,	,
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
ohromnou	ohromný	k2eAgFnSc4d1	ohromná
námořní	námořní	k2eAgFnSc4d1	námořní
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pak	pak	k6eAd1	pak
ohrožovala	ohrožovat	k5eAaImAgFnS	ohrožovat
celé	celý	k2eAgNnSc4d1	celé
západní	západní	k2eAgNnSc4d1	západní
Středomoří	středomoří	k1gNnSc4	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
vedli	vést	k5eAaImAgMnP	vést
tažení	tažení	k1gNnSc4	tažení
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
455	[number]	k4	455
dobyli	dobýt	k5eAaPmAgMnP	dobýt
a	a	k8xC	a
vydrancovali	vydrancovat	k5eAaPmAgMnP	vydrancovat
Řím	Řím	k1gInSc4	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
také	také	k9	také
Sicílii	Sicílie	k1gFnSc4	Sicílie
<g/>
,	,	kIx,	,
Sardínii	Sardínie	k1gFnSc4	Sardínie
a	a	k8xC	a
Korsiku	Korsika	k1gFnSc4	Korsika
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
534	[number]	k4	534
zničil	zničit	k5eAaPmAgMnS	zničit
říši	říše	k1gFnSc4	říše
Vandalů	Vandal	k1gMnPc2	Vandal
východořímský	východořímský	k2eAgMnSc1d1	východořímský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
Belisar	Belisar	k1gMnSc1	Belisar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hunové	Hun	k1gMnPc5	Hun
===	===	k?	===
</s>
</p>
<p>
<s>
Hunská	hunský	k2eAgFnSc1d1	hunská
říše	říše	k1gFnSc1	říše
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
více	hodně	k6eAd2	hodně
zvratů	zvrat	k1gInPc2	zvrat
<g/>
.	.	kIx.	.
</s>
<s>
Velitel	velitel	k1gMnSc1	velitel
římského	římský	k2eAgNnSc2d1	římské
vojska	vojsko	k1gNnSc2	vojsko
Flavius	Flavius	k1gMnSc1	Flavius
Aetius	Aetius	k1gMnSc1	Aetius
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nejprve	nejprve	k6eAd1	nejprve
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Huny	Hun	k1gMnPc7	Hun
<g/>
,	,	kIx,	,
odrazil	odrazit	k5eAaPmAgInS	odrazit
společně	společně	k6eAd1	společně
s	s	k7c7	s
Vizigóty	Vizigót	k1gMnPc7	Vizigót
v	v	k7c6	v
roce	rok	k1gInSc6	rok
451	[number]	k4	451
hunská	hunský	k2eAgNnPc1d1	hunské
vojska	vojsko	k1gNnPc1	vojsko
vedená	vedený	k2eAgNnPc1d1	vedené
Attilou	Attila	k1gMnSc7	Attila
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Katalaunských	Katalaunský	k2eAgNnPc6d1	Katalaunský
polích	pole	k1gNnPc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Attila	Attila	k1gMnSc1	Attila
sice	sice	k8xC	sice
ještě	ještě	k6eAd1	ještě
stihl	stihnout	k5eAaPmAgMnS	stihnout
vpadnout	vpadnout	k5eAaPmF	vpadnout
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zanedlouho	zanedlouho	k6eAd1	zanedlouho
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
hunská	hunský	k2eAgFnSc1d1	hunská
říše	říše	k1gFnSc1	říše
rychle	rychle	k6eAd1	rychle
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ostrogóti	Ostrogót	k1gMnPc5	Ostrogót
===	===	k?	===
</s>
</p>
<p>
<s>
Ostrogóti	Ostrogót	k1gMnPc1	Ostrogót
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
vznikli	vzniknout	k5eAaPmAgMnP	vzniknout
z	z	k7c2	z
Greutungů	Greutung	k1gInPc2	Greutung
(	(	kIx(	(
<g/>
východních	východní	k2eAgMnPc2d1	východní
Gótů	Gót	k1gMnPc2	Gót
<g/>
)	)	kIx)	)
a	a	k8xC	a
ještě	ještě	k9	ještě
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
kmenů	kmen	k1gInPc2	kmen
a	a	k8xC	a
kteří	který	k3yQgMnPc1	který
setrvávali	setrvávat	k5eAaImAgMnP	setrvávat
až	až	k9	až
do	do	k7c2	do
Attilovy	Attilův	k2eAgFnSc2d1	Attilova
smrti	smrt	k1gFnSc2	smrt
pod	pod	k7c7	pod
hunskou	hunský	k2eAgFnSc7d1	hunská
nadvládou	nadvláda	k1gFnSc7	nadvláda
<g/>
,	,	kIx,	,
vytáhli	vytáhnout	k5eAaPmAgMnP	vytáhnout
poté	poté	k6eAd1	poté
na	na	k7c4	na
Balkán	Balkán	k1gInSc4	Balkán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
východními	východní	k2eAgMnPc7d1	východní
Římany	Říman	k1gMnPc7	Říman
usazeni	usadit	k5eAaPmNgMnP	usadit
jako	jako	k9	jako
federáti	federát	k1gMnPc1	federát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
489	[number]	k4	489
Theodorich	Theodorich	k1gInSc1	Theodorich
Veliký	veliký	k2eAgInSc1d1	veliký
vedl	vést	k5eAaImAgInS	vést
Ostrogóty	Ostrogót	k1gMnPc4	Ostrogót
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
východořímského	východořímský	k2eAgMnSc2d1	východořímský
císaře	císař	k1gMnSc2	císař
Zenona	Zenon	k1gMnSc2	Zenon
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
pak	pak	k6eAd1	pak
porazil	porazit	k5eAaPmAgMnS	porazit
a	a	k8xC	a
zabil	zabít	k5eAaPmAgMnS	zabít
Odoakera	Odoaker	k1gMnSc4	Odoaker
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
tu	tu	k6eAd1	tu
vládl	vládnout	k5eAaImAgMnS	vládnout
po	po	k7c6	po
abdikaci	abdikace	k1gFnSc6	abdikace
Romula	Romulus	k1gMnSc2	Romulus
Augusta	August	k1gMnSc2	August
<g/>
,	,	kIx,	,
posledního	poslední	k2eAgMnSc2d1	poslední
západořímského	západořímský	k2eAgMnSc2d1	západořímský
císaře	císař	k1gMnSc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Theodorich	Theodorich	k1gInSc1	Theodorich
byl	být	k5eAaImAgInS	být
formálně	formálně	k6eAd1	formálně
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c6	na
císaři	císař	k1gMnSc6	císař
v	v	k7c6	v
Konstantinopoli	Konstantinopol	k1gInSc6	Konstantinopol
<g/>
,	,	kIx,	,
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
však	však	k9	však
vládl	vládnout	k5eAaImAgInS	vládnout
zcela	zcela	k6eAd1	zcela
neomezeně	omezeně	k6eNd1	omezeně
<g/>
.	.	kIx.	.
</s>
<s>
Nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
ale	ale	k9	ale
neodvážil	odvážit	k5eNaPmAgMnS	odvážit
přivlastnit	přivlastnit	k5eAaPmF	přivlastnit
si	se	k3xPyFc3	se
titul	titul	k1gInSc4	titul
císaře	císař	k1gMnSc2	císař
západu	západ	k1gInSc2	západ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
536	[number]	k4	536
napadl	napadnout	k5eAaPmAgMnS	napadnout
východořímský	východořímský	k2eAgMnSc1d1	východořímský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
Belisar	Belisar	k1gMnSc1	Belisar
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
císaře	císař	k1gMnSc2	císař
Justiniána	Justinián	k1gMnSc2	Justinián
říši	říše	k1gFnSc4	říše
Ostrogótů	Ostrogót	k1gMnPc2	Ostrogót
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
po	po	k7c6	po
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
boji	boj	k1gInSc6	boj
nakonec	nakonec	k6eAd1	nakonec
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
(	(	kIx(	(
<g/>
rozhodující	rozhodující	k2eAgFnSc4d1	rozhodující
bylo	být	k5eAaImAgNnS	být
poslední	poslední	k2eAgNnSc4d1	poslední
vítězství	vítězství	k1gNnSc4	vítězství
východořímského	východořímský	k2eAgMnSc2d1	východořímský
vojevůdce	vojevůdce	k1gMnSc2	vojevůdce
Narsa	Nars	k1gMnSc2	Nars
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Busta	busta	k1gFnSc1	busta
Gallorum	Gallorum	k1gNnSc4	Gallorum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
552	[number]	k4	552
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Frankové	Frankové	k2eAgNnPc6d1	Frankové
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
486	[number]	k4	486
odstranili	odstranit	k5eAaPmAgMnP	odstranit
Frankové	Frank	k1gMnPc1	Frank
vedení	vedení	k1gNnSc2	vedení
králem	král	k1gMnSc7	král
Chlodvíkem	Chlodvík	k1gMnSc7	Chlodvík
zbytky	zbytek	k1gInPc7	zbytek
římské	římský	k2eAgFnSc2d1	římská
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Galii	Galie	k1gFnSc6	Galie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
ještě	ještě	k6eAd1	ještě
udržovala	udržovat	k5eAaImAgFnS	udržovat
po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
západořímské	západořímský	k2eAgFnSc2d1	Západořímská
říše	říš	k1gFnSc2	říš
v	v	k7c6	v
roce	rok	k1gInSc6	rok
476	[number]	k4	476
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
534	[number]	k4	534
pak	pak	k6eAd1	pak
porazili	porazit	k5eAaPmAgMnP	porazit
Alamany	Alaman	k1gMnPc7	Alaman
<g/>
,	,	kIx,	,
Vizigóty	Vizigót	k1gMnPc7	Vizigót
(	(	kIx(	(
<g/>
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
po	po	k7c6	po
roce	rok	k1gInSc6	rok
507	[number]	k4	507
zatlačeni	zatlačit	k5eAaPmNgMnP	zatlačit
na	na	k7c4	na
Pyrenejský	pyrenejský	k2eAgInSc4d1	pyrenejský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
)	)	kIx)	)
a	a	k8xC	a
Burgundy	Burgundy	k1gInPc4	Burgundy
a	a	k8xC	a
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
tak	tak	k6eAd1	tak
velký	velký	k2eAgInSc4d1	velký
státní	státní	k2eAgInSc4d1	státní
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
stát	stát	k5eAaPmF	stát
nejvíce	nejvíce	k6eAd1	nejvíce
životaschopnou	životaschopný	k2eAgFnSc7d1	životaschopná
říší	říš	k1gFnSc7	říš
vzniklou	vzniklý	k2eAgFnSc7d1	vzniklá
v	v	k7c4	v
období	období	k1gNnSc4	období
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Invaze	invaze	k1gFnPc1	invaze
Anglů	Angl	k1gMnPc2	Angl
a	a	k8xC	a
Sasů	Sas	k1gMnPc2	Sas
do	do	k7c2	do
Británie	Británie	k1gFnSc2	Británie
===	===	k?	===
</s>
</p>
<p>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
již	již	k6eAd1	již
od	od	k7c2	od
konce	konec	k1gInSc2	konec
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pronikali	pronikat	k5eAaImAgMnP	pronikat
–	–	k?	–
ačkoli	ačkoli	k8xS	ačkoli
zpočátku	zpočátku	k6eAd1	zpočátku
jen	jen	k9	jen
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
skupinách	skupina	k1gFnPc6	skupina
–	–	k?	–
Frísové	Frís	k1gMnPc1	Frís
<g/>
,	,	kIx,	,
Anglové	Angl	k1gMnPc1	Angl
a	a	k8xC	a
Sasové	Sas	k1gMnPc1	Sas
ze	z	k7c2	z
severního	severní	k2eAgNnSc2d1	severní
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
z	z	k7c2	z
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Dánska	Dánsko	k1gNnSc2	Dánsko
do	do	k7c2	do
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
obsazovali	obsazovat	k5eAaImAgMnP	obsazovat
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
části	část	k1gFnPc4	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
Británie	Británie	k1gFnSc2	Británie
nebylo	být	k5eNaImAgNnS	být
bojeschopné	bojeschopný	k2eAgNnSc1d1	bojeschopné
po	po	k7c6	po
staletích	staletí	k1gNnPc6	staletí
"	"	kIx"	"
<g/>
římského	římský	k2eAgInSc2d1	římský
míru	mír	k1gInSc2	mír
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
vlnách	vlna	k1gFnPc6	vlna
odešly	odejít	k5eAaPmAgFnP	odejít
vojenské	vojenský	k2eAgFnPc1d1	vojenská
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
zemi	zem	k1gFnSc4	zem
chránit	chránit	k5eAaImF	chránit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
384	[number]	k4	384
odešly	odejít	k5eAaPmAgFnP	odejít
legie	legie	k1gFnPc1	legie
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
vojevůdce	vojevůdce	k1gMnSc2	vojevůdce
Maxima	Maxim	k1gMnSc2	Maxim
do	do	k7c2	do
Galie	Galie	k1gFnSc2	Galie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Maximus	Maximus	k1gMnSc1	Maximus
porazil	porazit	k5eAaPmAgMnS	porazit
císaře	císař	k1gMnSc4	císař
Graciána	Gracián	k1gMnSc4	Gracián
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
sám	sám	k3xTgMnSc1	sám
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
poražen	porazit	k5eAaPmNgMnS	porazit
císařem	císař	k1gMnSc7	císař
Východu	východ	k1gInSc2	východ
Theodosiem	Theodosius	k1gMnSc7	Theodosius
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
legie	legie	k1gFnSc1	legie
se	se	k3xPyFc4	se
už	už	k6eAd1	už
nikdy	nikdy	k6eAd1	nikdy
nevrátily	vrátit	k5eNaPmAgFnP	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Zůstala	zůstat	k5eAaPmAgFnS	zůstat
po	po	k7c6	po
nich	on	k3xPp3gFnPc6	on
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejkrásnějších	krásný	k2eAgFnPc2d3	nejkrásnější
keltských	keltský	k2eAgFnPc2d1	keltská
bájí	báj	k1gFnPc2	báj
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
oplakává	oplakávat	k5eAaImIp3nS	oplakávat
mrtvé	mrtvý	k2eAgMnPc4d1	mrtvý
rytíře	rytíř	k1gMnPc4	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
vojáci	voják	k1gMnPc1	voják
odcházejí	odcházet	k5eAaImIp3nP	odcházet
počátkem	počátkem	k7c2	počátkem
pátého	pátý	k4xOgNnSc2	pátý
století	století	k1gNnSc2	století
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
generála	generál	k1gMnSc2	generál
Stilichona	Stilichon	k1gMnSc2	Stilichon
bránit	bránit	k5eAaImF	bránit
Římskou	římský	k2eAgFnSc4d1	římská
říši	říše	k1gFnSc4	říše
proti	proti	k7c3	proti
Vandalům	Vandal	k1gMnPc3	Vandal
a	a	k8xC	a
Burgundům	Burgund	k1gMnPc3	Burgund
<g/>
.	.	kIx.	.
</s>
<s>
Římská	římský	k2eAgFnSc1d1	římská
správa	správa	k1gFnSc1	správa
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
se	se	k3xPyFc4	se
po	po	k7c6	po
odsunu	odsun	k1gInSc6	odsun
posledních	poslední	k2eAgFnPc2d1	poslední
vojenských	vojenský	k2eAgFnPc2d1	vojenská
posádek	posádka	k1gFnPc2	posádka
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zhroutila	zhroutit	k5eAaPmAgFnS	zhroutit
<g/>
.	.	kIx.	.
</s>
<s>
Brzo	brzo	k6eAd1	brzo
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
římská	římský	k2eAgFnSc1d1	římská
kultura	kultura	k1gFnSc1	kultura
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
zcela	zcela	k6eAd1	zcela
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Germáni	Germán	k1gMnPc1	Germán
pak	pak	k6eAd1	pak
zabírali	zabírat	k5eAaImAgMnP	zabírat
další	další	k2eAgFnPc4d1	další
části	část	k1gFnPc4	část
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
tak	tak	k9	tak
k	k	k7c3	k
bojům	boj	k1gInPc3	boj
mezi	mezi	k7c7	mezi
nově	nově	k6eAd1	nově
příchozími	příchozí	k1gMnPc7	příchozí
Germány	Germán	k1gMnPc7	Germán
a	a	k8xC	a
domorodými	domorodý	k2eAgMnPc7d1	domorodý
Kelty	Kelt	k1gMnPc7	Kelt
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
také	také	k9	také
zrodila	zrodit	k5eAaPmAgFnS	zrodit
legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
králi	král	k1gMnSc6	král
Artušovi	Artuš	k1gMnSc6	Artuš
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
prý	prý	k9	prý
byl	být	k5eAaImAgMnS	být
vůdcem	vůdce	k1gMnSc7	vůdce
domácích	domácí	k2eAgMnPc2d1	domácí
Keltů	Kelt	k1gMnPc2	Kelt
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
proti	proti	k7c3	proti
saským	saský	k2eAgMnPc3d1	saský
útočníkům	útočník	k1gMnPc3	útočník
<g/>
.	.	kIx.	.
</s>
<s>
Poražení	poražený	k2eAgMnPc1d1	poražený
keltští	keltský	k2eAgMnPc1d1	keltský
Britanové	Britanový	k2eAgFnSc2d1	Britanový
se	se	k3xPyFc4	se
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
do	do	k7c2	do
okrajových	okrajový	k2eAgFnPc2d1	okrajová
oblastí	oblast	k1gFnPc2	oblast
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
přemístila	přemístit	k5eAaPmAgFnS	přemístit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
450	[number]	k4	450
až	až	k9	až
do	do	k7c2	do
galské	galský	k2eAgFnSc2d1	galská
Armoriky	Armorika	k1gFnSc2	Armorika
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
název	název	k1gInSc1	název
Bretagne	Bretagne	k1gFnSc2	Bretagne
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Langobardi	Langobard	k1gMnPc5	Langobard
===	===	k?	===
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
5	[number]	k4	5
<g/>
.	.	kIx.	.
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Langobardi	Langobard	k1gMnPc1	Langobard
přemístili	přemístit	k5eAaPmAgMnP	přemístit
od	od	k7c2	od
dolního	dolní	k2eAgNnSc2d1	dolní
Labe	Labe	k1gNnSc2	Labe
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
a	a	k8xC	a
následně	následně	k6eAd1	následně
do	do	k7c2	do
Panonie	Panonie	k1gFnSc2	Panonie
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
Avarů	Avar	k1gMnPc2	Avar
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jim	on	k3xPp3gMnPc3	on
nejprve	nejprve	k6eAd1	nejprve
pomohli	pomoct	k5eAaPmAgMnP	pomoct
zničit	zničit	k5eAaPmF	zničit
Gepidy	Gepida	k1gFnPc4	Gepida
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
568	[number]	k4	568
vydali	vydat	k5eAaPmAgMnP	vydat
Langobardi	Langobard	k1gMnPc1	Langobard
vedení	vedení	k1gNnSc2	vedení
králem	král	k1gMnSc7	král
Alboinem	Alboin	k1gMnSc7	Alboin
z	z	k7c2	z
Panonie	Panonie	k1gFnSc2	Panonie
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
založili	založit	k5eAaPmAgMnP	založit
zde	zde	k6eAd1	zde
langobardskou	langobardský	k2eAgFnSc4d1	langobardská
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
existence	existence	k1gFnSc1	existence
trvala	trvat	k5eAaImAgFnS	trvat
pak	pak	k6eAd1	pak
až	až	k9	až
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
než	než	k8xS	než
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
podrobil	podrobit	k5eAaPmAgMnS	podrobit
Karel	Karel	k1gMnSc1	Karel
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
tažení	tažení	k1gNnSc1	tažení
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
předcházel	předcházet	k5eAaImAgInS	předcházet
moudrý	moudrý	k2eAgInSc4d1	moudrý
pokus	pokus	k1gInSc4	pokus
východních	východní	k2eAgMnPc2d1	východní
Římanů	Říman	k1gMnPc2	Říman
usadit	usadit	k5eAaPmF	usadit
Langobardy	Langobard	k1gInPc4	Langobard
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
jako	jako	k9	jako
federáty	federát	k1gInPc4	federát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
konec	konec	k1gInSc4	konec
období	období	k1gNnSc4	období
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Císařským	císařský	k2eAgNnPc3d1	císařské
vojskům	vojsko	k1gNnPc3	vojsko
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
udržet	udržet	k5eAaPmF	udržet
jen	jen	k9	jen
asi	asi	k9	asi
polovinu	polovina	k1gFnSc4	polovina
poloostrova	poloostrov	k1gInSc2	poloostrov
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Ravenny	Ravenna	k1gFnSc2	Ravenna
a	a	k8xC	a
Říma	Řím	k1gInSc2	Řím
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
v	v	k7c6	v
téže	týž	k3xTgFnSc6	týž
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevují	objevovat	k5eAaImIp3nP	objevovat
Bavoři	Bavor	k1gMnPc1	Bavor
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
obsazují	obsazovat	k5eAaImIp3nP	obsazovat
Slované	Slovan	k1gMnPc1	Slovan
kdysi	kdysi	k6eAd1	kdysi
germánská	germánský	k2eAgNnPc1d1	germánské
území	území	k1gNnPc1	území
a	a	k8xC	a
usazují	usazovat	k5eAaImIp3nP	usazovat
se	se	k3xPyFc4	se
na	na	k7c6	na
dosud	dosud	k6eAd1	dosud
římském	římský	k2eAgInSc6d1	římský
Balkáně	Balkán	k1gInSc6	Balkán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Avaři	Avar	k1gMnPc5	Avar
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
objevují	objevovat	k5eAaImIp3nP	objevovat
kočovní	kočovní	k2eAgMnPc1d1	kočovní
Avaři	Avar	k1gMnPc1	Avar
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
obratně	obratně	k6eAd1	obratně
využívali	využívat	k5eAaImAgMnP	využívat
politických	politický	k2eAgFnPc2d1	politická
krizí	krize	k1gFnPc2	krize
v	v	k7c6	v
Byzantské	byzantský	k2eAgFnSc6d1	byzantská
říši	říš	k1gFnSc6	říš
a	a	k8xC	a
jejích	její	k3xOp3gInPc2	její
konfliktů	konflikt	k1gInPc2	konflikt
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
sousedy	soused	k1gMnPc7	soused
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Franky	Frank	k1gMnPc4	Frank
<g/>
,	,	kIx,	,
Slovany	Slovan	k1gMnPc4	Slovan
a	a	k8xC	a
Peršany	Peršan	k1gMnPc4	Peršan
<g/>
.	.	kIx.	.
</s>
<s>
Ovládali	ovládat	k5eAaImAgMnP	ovládat
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
Balkánu	Balkán	k1gInSc2	Balkán
a	a	k8xC	a
Panonie	Panonie	k1gFnSc2	Panonie
<g/>
.	.	kIx.	.
</s>
<s>
Prosluli	proslout	k5eAaPmAgMnP	proslout
svou	svůj	k3xOyFgFnSc7	svůj
bojovností	bojovnost	k1gFnSc7	bojovnost
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
zcela	zcela	k6eAd1	zcela
zničeni	zničen	k2eAgMnPc1d1	zničen
Karlem	Karel	k1gMnSc7	Karel
Velikým	veliký	k2eAgFnPc3d1	veliká
roku	rok	k1gInSc2	rok
810	[number]	k4	810
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhá	druhý	k4xOgFnSc1	druhý
fáze	fáze	k1gFnSc1	fáze
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
==	==	k?	==
</s>
</p>
<p>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
fází	fáze	k1gFnSc7	fáze
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
národnostní	národnostní	k2eAgInPc1d1	národnostní
a	a	k8xC	a
etnické	etnický	k2eAgInPc1d1	etnický
pohyby	pohyb	k1gInPc1	pohyb
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
v	v	k7c6	v
období	období	k1gNnSc6	období
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
do	do	k7c2	do
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
plynule	plynule	k6eAd1	plynule
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
předchozí	předchozí	k2eAgNnSc4d1	předchozí
stěhování	stěhování	k1gNnSc4	stěhování
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
zejména	zejména	k9	zejména
objevení	objevení	k1gNnSc1	objevení
se	se	k3xPyFc4	se
Slovanů	Slovan	k1gInPc2	Slovan
na	na	k7c6	na
historické	historický	k2eAgFnSc6d1	historická
scéně	scéna	k1gFnSc6	scéna
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
příchod	příchod	k1gInSc1	příchod
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
na	na	k7c4	na
Balkán	Balkán	k1gInSc4	Balkán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
velkou	velký	k2eAgFnSc7d1	velká
národnostní	národnostní	k2eAgFnSc7d1	národnostní
změnou	změna	k1gFnSc7	změna
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
byl	být	k5eAaImAgInS	být
příchod	příchod	k1gInSc1	příchod
Maďarů	Maďar	k1gMnPc2	Maďar
do	do	k7c2	do
Panonie	Panonie	k1gFnSc2	Panonie
v	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnSc4d1	další
významné	významný	k2eAgNnSc4d1	významné
stěhování	stěhování	k1gNnSc4	stěhování
patří	patřit	k5eAaImIp3nS	patřit
pronikání	pronikání	k1gNnSc4	pronikání
Kumánů	Kumán	k1gMnPc2	Kumán
(	(	kIx(	(
<g/>
Polovců	Polovec	k1gMnPc2	Polovec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pečeněhů	Pečeněh	k1gMnPc2	Pečeněh
<g/>
,	,	kIx,	,
Sikulů	Sikul	k1gMnPc2	Sikul
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
útok	útok	k1gInSc1	útok
Tatarů	Tatar	k1gMnPc2	Tatar
(	(	kIx(	(
<g/>
Mongolů	Mongol	k1gMnPc2	Mongol
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
příchod	příchod	k1gInSc1	příchod
Cikánů	cikán	k1gMnPc2	cikán
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgNnSc7d1	poslední
"	"	kIx"	"
<g/>
stěhováním	stěhování	k1gNnSc7	stěhování
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
příchod	příchod	k1gInSc1	příchod
Osmanských	osmanský	k2eAgInPc2d1	osmanský
Turků	turek	k1gInPc2	turek
(	(	kIx(	(
<g/>
kmenový	kmenový	k2eAgInSc1d1	kmenový
svaz	svaz	k1gInSc1	svaz
Oguzů	Oguz	k1gMnPc2	Oguz
<g/>
)	)	kIx)	)
do	do	k7c2	do
Anatolie	Anatolie	k1gFnSc2	Anatolie
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významná	významný	k2eAgNnPc1d1	významné
data	datum	k1gNnPc1	datum
==	==	k?	==
</s>
</p>
<p>
<s>
166	[number]	k4	166
–	–	k?	–
174	[number]	k4	174
a	a	k8xC	a
177	[number]	k4	177
–	–	k?	–
180	[number]	k4	180
Markomanské	markomanský	k2eAgFnSc2d1	markomanská
války	válka	k1gFnSc2	válka
na	na	k7c6	na
středním	střední	k2eAgInSc6d1	střední
Dunaji	Dunaj	k1gInSc6	Dunaj
</s>
</p>
<p>
<s>
250	[number]	k4	250
–	–	k?	–
270	[number]	k4	270
Gótové	Gót	k1gMnPc1	Gót
využívají	využívat	k5eAaImIp3nP	využívat
anarchie	anarchie	k1gFnPc4	anarchie
v	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
říši	říš	k1gFnSc6	říš
a	a	k8xC	a
vpadají	vpadat	k5eAaPmIp3nP	vpadat
za	za	k7c4	za
Dunaj	Dunaj	k1gInSc4	Dunaj
<g/>
,	,	kIx,	,
plení	plenit	k5eAaImIp3nS	plenit
Balkán	Balkán	k1gInSc4	Balkán
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc4	Řecko
a	a	k8xC	a
Malou	malý	k2eAgFnSc4d1	malá
Asii	Asie	k1gFnSc4	Asie
</s>
</p>
<p>
<s>
260	[number]	k4	260
Římané	Říman	k1gMnPc1	Říman
přenechávají	přenechávat	k5eAaImIp3nP	přenechávat
Agri	Agre	k1gFnSc4	Agre
Decumates	Decumatesa	k1gFnPc2	Decumatesa
Alamanům	Alaman	k1gMnPc3	Alaman
</s>
</p>
<p>
<s>
271	[number]	k4	271
Římané	Říman	k1gMnPc1	Říman
přenechávají	přenechávat	k5eAaImIp3nP	přenechávat
Dácii	Dácie	k1gFnSc4	Dácie
Gótům	Gót	k1gMnPc3	Gót
</s>
</p>
<p>
<s>
375	[number]	k4	375
Nomádští	nomádský	k2eAgMnPc1d1	nomádský
Hunové	Hun	k1gMnPc1	Hun
porážejí	porážet	k5eAaImIp3nP	porážet
Alany	alan	k1gInPc4	alan
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
tlačí	tlačit	k5eAaImIp3nS	tlačit
na	na	k7c4	na
gótské	gótský	k2eAgInPc4d1	gótský
Greutungy	Greutung	k1gInPc4	Greutung
</s>
</p>
<p>
<s>
376	[number]	k4	376
Tervingové	Tervingový	k2eAgInPc1d1	Tervingový
a	a	k8xC	a
Greutungové	Greutungový	k2eAgInPc1d1	Greutungový
žádají	žádat	k5eAaImIp3nP	žádat
císaře	císař	k1gMnSc2	císař
Valenta	Valenta	k1gMnSc1	Valenta
o	o	k7c4	o
azyl	azyl	k1gInSc4	azyl
v	v	k7c6	v
impériu	impérium	k1gNnSc6	impérium
<g/>
,	,	kIx,	,
záhy	záhy	k6eAd1	záhy
vypuká	vypukat	k5eAaImIp3nS	vypukat
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
vzpoura	vzpoura	k1gFnSc1	vzpoura
Gótů	Gót	k1gMnPc2	Gót
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
</s>
</p>
<p>
<s>
378	[number]	k4	378
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Adrianopole	Adrianopole	k1gFnSc2	Adrianopole
<g/>
,	,	kIx,	,
naprosté	naprostý	k2eAgNnSc1d1	naprosté
gótské	gótský	k2eAgNnSc1d1	gótské
vítězství	vítězství	k1gNnSc1	vítězství
<g/>
,	,	kIx,	,
císař	císař	k1gMnSc1	císař
Valens	Valensa	k1gFnPc2	Valensa
umírá	umírat	k5eAaImIp3nS	umírat
</s>
</p>
<p>
<s>
382	[number]	k4	382
Theodosius	Theodosius	k1gInSc1	Theodosius
uzavírá	uzavírat	k5eAaImIp3nS	uzavírat
s	s	k7c7	s
Góty	Gót	k1gMnPc7	Gót
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jim	on	k3xPp3gMnPc3	on
přidělena	přidělen	k2eAgFnSc1d1	přidělena
půda	půda	k1gFnSc1	půda
a	a	k8xC	a
povoleno	povolen	k2eAgNnSc1d1	povoleno
vlastní	vlastní	k2eAgNnSc1d1	vlastní
zákonodárství	zákonodárství	k1gNnSc1	zákonodárství
</s>
</p>
<p>
<s>
395	[number]	k4	395
Smrt	smrt	k1gFnSc1	smrt
císaře	císař	k1gMnSc2	císař
Theodosia	Theodosius	k1gMnSc2	Theodosius
<g/>
,	,	kIx,	,
římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
</s>
</p>
<p>
<s>
395	[number]	k4	395
Počátek	počátek	k1gInSc1	počátek
gótské	gótský	k2eAgFnSc2d1	gótská
revolty	revolta	k1gFnSc2	revolta
<g/>
,	,	kIx,	,
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
Alarich	Alarich	k1gInSc1	Alarich
</s>
</p>
<p>
<s>
395	[number]	k4	395
–	–	k?	–
420	[number]	k4	420
Přesun	přesun	k1gInSc1	přesun
Hunů	Hun	k1gMnPc2	Hun
ze	z	k7c2	z
severního	severní	k2eAgNnSc2d1	severní
Černomoří	Černomoří	k1gNnSc2	Černomoří
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
uherské	uherský	k2eAgFnSc2d1	uherská
nížiny	nížina	k1gFnSc2	nížina
</s>
</p>
<p>
<s>
401	[number]	k4	401
–	–	k?	–
402	[number]	k4	402
První	první	k4xOgNnSc4	první
Alarichovo	Alarichův	k2eAgNnSc4d1	Alarichův
neúspěšné	úspěšný	k2eNgNnSc4d1	neúspěšné
tažení	tažení	k1gNnSc4	tažení
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
</s>
</p>
<p>
<s>
406	[number]	k4	406
–	–	k?	–
407	[number]	k4	407
Vpád	vpád	k1gInSc4	vpád
Vandalů	Vandal	k1gMnPc2	Vandal
<g/>
,	,	kIx,	,
Alanů	Alan	k1gMnPc2	Alan
a	a	k8xC	a
Svébů	Svéb	k1gMnPc2	Svéb
do	do	k7c2	do
Galie	Galie	k1gFnSc2	Galie
</s>
</p>
<p>
<s>
407	[number]	k4	407
Římané	Říman	k1gMnPc1	Říman
se	se	k3xPyFc4	se
stahují	stahovat	k5eAaImIp3nP	stahovat
z	z	k7c2	z
Británie	Británie	k1gFnSc2	Británie
</s>
</p>
<p>
<s>
408	[number]	k4	408
–	–	k?	–
409	[number]	k4	409
Alarich	Alarich	k1gInSc1	Alarich
využívá	využívat	k5eAaPmIp3nS	využívat
zmatku	zmatek	k1gInSc3	zmatek
v	v	k7c6	v
západořímské	západořímský	k2eAgFnSc6d1	Západořímská
říši	říš	k1gFnSc6	říš
po	po	k7c4	po
zavraždění	zavraždění	k1gNnSc4	zavraždění
Stilichona	Stilichona	k1gFnSc1	Stilichona
k	k	k7c3	k
novému	nový	k2eAgNnSc3d1	nové
tažení	tažení	k1gNnSc3	tažení
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
</s>
</p>
<p>
<s>
409	[number]	k4	409
Vpád	vpád	k1gInSc4	vpád
Vandalů	Vandal	k1gMnPc2	Vandal
<g/>
,	,	kIx,	,
Alanů	Alan	k1gMnPc2	Alan
a	a	k8xC	a
Svébů	Svéb	k1gMnPc2	Svéb
do	do	k7c2	do
Hispánie	Hispánie	k1gFnSc2	Hispánie
</s>
</p>
<p>
<s>
410	[number]	k4	410
Alarich	Alaricha	k1gFnPc2	Alaricha
dobývá	dobývat	k5eAaImIp3nS	dobývat
Řím	Řím	k1gInSc1	Řím
</s>
</p>
<p>
<s>
418	[number]	k4	418
Smlouva	smlouva	k1gFnSc1	smlouva
s	s	k7c7	s
Římany	Říman	k1gMnPc7	Říman
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
Vizigótům	Vizigót	k1gMnPc3	Vizigót
sídla	sídlo	k1gNnSc2	sídlo
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Galii	Galie	k1gFnSc6	Galie
</s>
</p>
<p>
<s>
429	[number]	k4	429
Vpád	vpád	k1gInSc4	vpád
Vandalů	Vandal	k1gMnPc2	Vandal
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
</s>
</p>
<p>
<s>
439	[number]	k4	439
Náčelník	náčelník	k1gInSc4	náčelník
Vandalů	Vandal	k1gMnPc2	Vandal
Geiserich	Geiserich	k1gInSc1	Geiserich
dobývá	dobývat	k5eAaImIp3nS	dobývat
Kartágo	Kartágo	k1gNnSc4	Kartágo
<g/>
,	,	kIx,	,
završuje	završovat	k5eAaImIp3nS	završovat
tím	ten	k3xDgNnSc7	ten
tak	tak	k9	tak
dobytí	dobytí	k1gNnSc1	dobytí
Afriky	Afrika	k1gFnSc2	Afrika
Vandaly	Vandal	k1gMnPc4	Vandal
</s>
</p>
<p>
<s>
440	[number]	k4	440
Attila	Attila	k1gMnSc1	Attila
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
hunským	hunský	k2eAgMnSc7d1	hunský
králem	král	k1gMnSc7	král
</s>
</p>
<p>
<s>
451	[number]	k4	451
Bitva	bitva	k1gFnSc1	bitva
na	na	k7c6	na
Katalaunských	Katalaunský	k2eAgNnPc6d1	Katalaunský
polích	pole	k1gNnPc6	pole
<g/>
,	,	kIx,	,
porážka	porážka	k1gFnSc1	porážka
Hunů	Hun	k1gMnPc2	Hun
</s>
</p>
<p>
<s>
po	po	k7c4	po
453	[number]	k4	453
Dezintegrace	dezintegrace	k1gFnPc4	dezintegrace
hunské	hunský	k2eAgFnSc2d1	hunská
říše	říš	k1gFnSc2	říš
po	po	k7c6	po
Attilově	Attilův	k2eAgFnSc6d1	Attilova
smrti	smrt	k1gFnSc6	smrt
</s>
</p>
<p>
<s>
455	[number]	k4	455
Geiserich	Geiserich	k1gInSc1	Geiserich
dobývá	dobývat	k5eAaImIp3nS	dobývat
a	a	k8xC	a
plení	plenit	k5eAaImIp3nS	plenit
Řím	Řím	k1gInSc1	Řím
</s>
</p>
<p>
<s>
474	[number]	k4	474
Theodorich	Theodorich	k1gMnSc1	Theodorich
Veliký	veliký	k2eAgMnSc1d1	veliký
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
králem	král	k1gMnSc7	král
Ostrogótů	Ostrogót	k1gMnPc2	Ostrogót
</s>
</p>
<p>
<s>
476	[number]	k4	476
Germánský	germánský	k2eAgMnSc1d1	germánský
velitel	velitel	k1gMnSc1	velitel
Odoaker	Odoaker	k1gMnSc1	Odoaker
sesazuje	sesazovat	k5eAaImIp3nS	sesazovat
posledního	poslední	k2eAgMnSc4d1	poslední
západořímského	západořímský	k2eAgMnSc4d1	západořímský
císaře	císař	k1gMnSc4	císař
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
pánem	pán	k1gMnSc7	pán
Itálie	Itálie	k1gFnSc2	Itálie
</s>
</p>
<p>
<s>
po	po	k7c4	po
480	[number]	k4	480
Vizigóti	Vizigót	k1gMnPc1	Vizigót
se	se	k3xPyFc4	se
usazují	usazovat	k5eAaImIp3nP	usazovat
na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
</s>
</p>
<p>
<s>
488	[number]	k4	488
Theodorich	Theodorich	k1gInSc1	Theodorich
Veliký	veliký	k2eAgInSc1d1	veliký
táhne	táhnout	k5eAaImIp3nS	táhnout
s	s	k7c7	s
Ostrogóty	Ostrogót	k1gMnPc7	Ostrogót
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
</s>
</p>
<p>
<s>
493	[number]	k4	493
Theodorich	Theodorich	k1gInSc1	Theodorich
obsazuje	obsazovat	k5eAaImIp3nS	obsazovat
Ravennu	Ravenna	k1gFnSc4	Ravenna
<g/>
,	,	kIx,	,
Odoakerova	Odoakerův	k2eAgFnSc1d1	Odoakerův
smrt	smrt	k1gFnSc1	smrt
</s>
</p>
<p>
<s>
507	[number]	k4	507
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Vouillé	Vouillý	k2eAgFnSc2d1	Vouillý
:	:	kIx,	:
Frankové	Franková	k1gFnSc2	Franková
poráží	porážet	k5eAaImIp3nS	porážet
Vizigóty	Vizigót	k1gMnPc4	Vizigót
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
odcházejí	odcházet	k5eAaImIp3nP	odcházet
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
Gallie	Gallie	k1gFnSc2	Gallie
na	na	k7c4	na
Pyrenejský	pyrenejský	k2eAgInSc4d1	pyrenejský
poloostrov	poloostrov	k1gInSc4	poloostrov
</s>
</p>
<p>
<s>
534	[number]	k4	534
Belisar	Belisar	k1gInSc1	Belisar
vyvrací	vyvracet	k5eAaImIp3nS	vyvracet
říši	říše	k1gFnSc4	říše
Vandalů	Vandal	k1gMnPc2	Vandal
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
</s>
</p>
<p>
<s>
536	[number]	k4	536
Belisar	Belisar	k1gInSc1	Belisar
se	se	k3xPyFc4	se
vyloďuje	vyloďovat	k5eAaImIp3nS	vyloďovat
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
válka	válka	k1gFnSc1	válka
Římanů	Říman	k1gMnPc2	Říman
s	s	k7c7	s
Ostrogóty	Ostrogót	k1gMnPc7	Ostrogót
</s>
</p>
<p>
<s>
552	[number]	k4	552
Římský	římský	k2eAgMnSc1d1	římský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
Narses	Narses	k1gMnSc1	Narses
poráží	porážet	k5eAaImIp3nS	porážet
Ostrogóty	Ostrogót	k1gMnPc4	Ostrogót
u	u	k7c2	u
Busta	busta	k1gFnSc1	busta
Gallorum	Gallorum	k1gInSc1	Gallorum
<g/>
,	,	kIx,	,
konec	konec	k1gInSc1	konec
říše	říš	k1gFnSc2	říš
Ostrogótů	Ostrogót	k1gMnPc2	Ostrogót
</s>
</p>
<p>
<s>
558	[number]	k4	558
Příchod	příchod	k1gInSc4	příchod
Avarů	Avar	k1gMnPc2	Avar
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
</s>
</p>
<p>
<s>
568	[number]	k4	568
Langobardi	Langobard	k1gMnPc1	Langobard
napadají	napadat	k5eAaImIp3nP	napadat
severní	severní	k2eAgFnSc3d1	severní
Itálii	Itálie	k1gFnSc3	Itálie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Východořímské	východořímský	k2eAgFnSc2d1	Východořímská
říše	říš	k1gFnSc2	říš
</s>
</p>
<p>
<s>
582	[number]	k4	582
Počátek	počátek	k1gInSc1	počátek
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
expanze	expanze	k1gFnSc2	expanze
na	na	k7c4	na
Balkán	Balkán	k1gInSc4	Balkán
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Sirmia	Sirmium	k1gNnSc2	Sirmium
a	a	k8xC	a
zhroucení	zhroucení	k1gNnSc2	zhroucení
byzantské	byzantský	k2eAgFnSc2d1	byzantská
hranice	hranice	k1gFnSc2	hranice
na	na	k7c6	na
Dunaji	Dunaj	k1gInSc6	Dunaj
</s>
</p>
<p>
<s>
626	[number]	k4	626
Společný	společný	k2eAgInSc1d1	společný
neúspěšný	úspěšný	k2eNgInSc1d1	neúspěšný
útok	útok	k1gInSc1	útok
Avarů	Avar	k1gMnPc2	Avar
<g/>
,	,	kIx,	,
Slovanů	Slovan	k1gMnPc2	Slovan
a	a	k8xC	a
Peršanů	Peršan	k1gMnPc2	Peršan
na	na	k7c4	na
Konstantinopol	Konstantinopol	k1gInSc4	Konstantinopol
</s>
</p>
<p>
<s>
681	[number]	k4	681
Vznik	vznik	k1gInSc4	vznik
První	první	k4xOgFnSc2	první
bulharské	bulharský	k2eAgFnSc2d1	bulharská
říše	říš	k1gFnSc2	říš
</s>
</p>
<p>
<s>
711-713	[number]	k4	711-713
Arabové	Arab	k1gMnPc1	Arab
porážejí	porážet	k5eAaImIp3nP	porážet
a	a	k8xC	a
likvidují	likvidovat	k5eAaBmIp3nP	likvidovat
vizigótskou	vizigótský	k2eAgFnSc4d1	Vizigótská
říši	říše	k1gFnSc4	říše
na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
</s>
</p>
<p>
<s>
732	[number]	k4	732
Frankové	Frank	k1gMnPc1	Frank
porážejí	porážet	k5eAaImIp3nP	porážet
Araby	Arab	k1gMnPc4	Arab
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Tours	Toursa	k1gFnPc2	Toursa
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
</s>
</p>
<p>
<s>
774	[number]	k4	774
Franský	franský	k2eAgMnSc1d1	franský
panovník	panovník	k1gMnSc1	panovník
Karel	Karel	k1gMnSc1	Karel
Veliký	veliký	k2eAgMnSc1d1	veliký
vyvrací	vyvracet	k5eAaImIp3nS	vyvracet
říši	říše	k1gFnSc4	říše
Langobardů	Langobard	k1gInPc2	Langobard
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
</s>
</p>
<p>
<s>
793	[number]	k4	793
Počátek	počátek	k1gInSc1	počátek
éry	éra	k1gFnSc2	éra
Vikingů	Viking	k1gMnPc2	Viking
</s>
</p>
<p>
<s>
805	[number]	k4	805
Byzantinci	Byzantinec	k1gMnPc1	Byzantinec
porážejí	porážet	k5eAaImIp3nP	porážet
Slovany	Slovan	k1gInPc4	Slovan
u	u	k7c2	u
Patrasu	Patras	k1gInSc2	Patras
a	a	k8xC	a
vytlačují	vytlačovat	k5eAaImIp3nP	vytlačovat
je	on	k3xPp3gFnPc4	on
z	z	k7c2	z
Peloponésu	Peloponés	k1gInSc2	Peloponés
</s>
</p>
<p>
<s>
810	[number]	k4	810
Likvidace	likvidace	k1gFnSc1	likvidace
Avarů	Avar	k1gMnPc2	Avar
Karlem	Karel	k1gMnSc7	Karel
Velikým	veliký	k2eAgInPc3d1	veliký
</s>
</p>
<p>
<s>
895	[number]	k4	895
<g/>
/	/	kIx~	/
<g/>
896	[number]	k4	896
První	první	k4xOgMnPc4	první
Maďaři	Maďar	k1gMnPc1	Maďar
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
v	v	k7c6	v
Karpatské	karpatský	k2eAgFnSc6d1	Karpatská
kotlině	kotlina	k1gFnSc6	kotlina
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Völkerwanderung	Völkerwanderunga	k1gFnPc2	Völkerwanderunga
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BEDNAŘÍKOVÁ	Bednaříková	k1gFnSc1	Bednaříková
<g/>
,	,	kIx,	,
Jarmila	Jarmila	k1gFnSc1	Jarmila
<g/>
.	.	kIx.	.
</s>
<s>
Frankové	Franková	k1gFnPc1	Franková
a	a	k8xC	a
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
335	[number]	k4	335
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
942	[number]	k4	942
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BEDNAŘÍKOVÁ	Bednaříková	k1gFnSc1	Bednaříková
<g/>
,	,	kIx,	,
Jarmila	Jarmila	k1gFnSc1	Jarmila
<g/>
.	.	kIx.	.
</s>
<s>
Stěhování	stěhování	k1gNnSc1	stěhování
národů	národ	k1gInPc2	národ
a	a	k8xC	a
sever	sever	k1gInSc4	sever
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Vikingové	Viking	k1gMnPc1	Viking
na	na	k7c6	na
mořích	moře	k1gNnPc6	moře
i	i	k8xC	i
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
280	[number]	k4	280
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7429	[number]	k4	7429
<g/>
-	-	kIx~	-
<g/>
744	[number]	k4	744
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BEDNAŘÍKOVÁ	Bednaříková	k1gFnSc1	Bednaříková
<g/>
,	,	kIx,	,
Jarmila	Jarmila	k1gFnSc1	Jarmila
<g/>
;	;	kIx,	;
MĚŘÍNSKÝ	MĚŘÍNSKÝ	kA	MĚŘÍNSKÝ
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
HOMOLA	Homola	k1gMnSc1	Homola
<g/>
,	,	kIx,	,
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
.	.	kIx.	.
</s>
<s>
Stěhování	stěhování	k1gNnSc1	stěhování
národů	národ	k1gInPc2	národ
a	a	k8xC	a
Východ	východ	k1gInSc4	východ
Evropy	Evropa	k1gFnSc2	Evropa
:	:	kIx,	:
Byzanc	Byzanc	k1gFnSc1	Byzanc
<g/>
,	,	kIx,	,
Slované	Slovan	k1gMnPc1	Slovan
<g/>
,	,	kIx,	,
Arabové	Arab	k1gMnPc1	Arab
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
557	[number]	k4	557
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
787	[number]	k4	787
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
COLLINS	COLLINS	kA	COLLINS
<g/>
,	,	kIx,	,
Roger	Roger	k1gInSc1	Roger
<g/>
:	:	kIx,	:
Evropa	Evropa	k1gFnSc1	Evropa
raného	raný	k2eAgInSc2d1	raný
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
300	[number]	k4	300
<g/>
-	-	kIx~	-
<g/>
1000	[number]	k4	1000
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
479	[number]	k4	479
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
660	[number]	k4	660
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Češka	Češka	k1gFnSc1	Češka
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Zánik	zánik	k1gInSc1	zánik
antického	antický	k2eAgInSc2d1	antický
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doležal	Doležal	k1gMnSc1	Doležal
<g/>
,	,	kIx,	,
S.	S.	kA	S.
<g/>
:	:	kIx,	:
Interakce	interakce	k1gFnSc1	interakce
Gótů	Gót	k1gMnPc2	Gót
a	a	k8xC	a
římského	římský	k2eAgNnSc2d1	římské
impéria	impérium	k1gNnSc2	impérium
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
DROBERJAR	DROBERJAR	kA	DROBERJAR
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
<g/>
.	.	kIx.	.
</s>
<s>
Věk	věk	k1gInSc1	věk
barbarů	barbar	k1gMnPc2	barbar
<g/>
:	:	kIx,	:
české	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
a	a	k8xC	a
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
archeologie	archeologie	k1gFnSc2	archeologie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7185-656-8	[number]	k4	80-7185-656-8
</s>
</p>
<p>
<s>
HEATHER	HEATHER	kA	HEATHER
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
<g/>
.	.	kIx.	.
</s>
<s>
Gótové	Gót	k1gMnPc1	Gót
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Lidové	lidový	k2eAgFnPc4d1	lidová
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7106-199-9	[number]	k4	80-7106-199-9
</s>
</p>
<p>
<s>
Todd	Todd	k1gMnSc1	Todd
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Germáni	Germán	k1gMnPc1	Germán
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
NLN	NLN	kA	NLN
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
</s>
</p>
<p>
<s>
Germáni	Germán	k1gMnPc1	Germán
</s>
</p>
<p>
<s>
Hunové	Hun	k1gMnPc1	Hun
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Stěhování	stěhování	k1gNnPc2	stěhování
národů	národ	k1gInPc2	národ
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Nástup	nástup	k1gInSc1	nástup
Barbarů	barbar	k1gMnPc2	barbar
</s>
</p>
