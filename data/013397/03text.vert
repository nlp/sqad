<p>
<s>
Regulární	regulární	k2eAgFnSc1d1	regulární
gramatika	gramatika	k1gFnSc1	gramatika
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc4	typ
formální	formální	k2eAgFnSc2d1	formální
gramatiky	gramatika	k1gFnSc2	gramatika
<g/>
.	.	kIx.	.
</s>
<s>
Přesněji	přesně	k6eAd2	přesně
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
gramatika	gramatika	k1gFnSc1	gramatika
typu	typ	k1gInSc2	typ
3	[number]	k4	3
podle	podle	k7c2	podle
Chomského	Chomský	k2eAgInSc2d1	Chomský
hierarchie	hierarchie	k1gFnSc1	hierarchie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gramatika	gramatika	k1gFnSc1	gramatika
typu	typ	k1gInSc2	typ
3	[number]	k4	3
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pravidla	pravidlo	k1gNnSc2	pravidlo
tvaru	tvar	k1gInSc3	tvar
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
X	X	kA	X
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
Y	Y	kA	Y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
wY	wY	k?	wY
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
X	X	kA	X
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
w	w	k?	w
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
kde	kde	k6eAd1	kde
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
jsou	být	k5eAaImIp3nP	být
neterminály	neterminál	k1gInPc1	neterminál
a	a	k8xC	a
w	w	k?	w
je	být	k5eAaImIp3nS	být
řetězcem	řetězec	k1gInSc7	řetězec
terminálů	terminál	k1gInPc2	terminál
<g/>
.	.	kIx.	.
</s>
<s>
Gramatika	gramatika	k1gFnSc1	gramatika
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
pravidlo	pravidlo	k1gNnSc1	pravidlo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
ε	ε	k?	ε
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
\	\	kIx~	\
<g/>
epsilon	epsilon	k1gNnSc1	epsilon
}	}	kIx)	}
</s>
</p>
<p>
<s>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
žádného	žádný	k3yNgNnSc2	žádný
pravidla	pravidlo	k1gNnSc2	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1	rozšíření
regulární	regulární	k2eAgFnSc2d1	regulární
gramatiky	gramatika	k1gFnSc2	gramatika
o	o	k7c4	o
řetězce	řetězec	k1gInPc4	řetězec
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
pravá	pravý	k2eAgFnSc1d1	pravá
regulární	regulární	k2eAgFnSc1d1	regulární
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obdobně	obdobně	k6eAd1	obdobně
se	se	k3xPyFc4	se
definují	definovat	k5eAaBmIp3nP	definovat
i	i	k9	i
levé	levý	k2eAgFnPc1d1	levá
regulární	regulární	k2eAgFnPc1d1	regulární
gramatiky	gramatika	k1gFnPc1	gramatika
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
pravidla	pravidlo	k1gNnPc4	pravidlo
tvaru	tvar	k1gInSc2	tvar
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
X	X	kA	X
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
Y	Y	kA	Y
</s>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
Yw	Yw	k1gFnSc1	Yw
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
X	X	kA	X
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
w	w	k?	w
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
kde	kde	k6eAd1	kde
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
jsou	být	k5eAaImIp3nP	být
neterminály	neterminál	k1gInPc1	neterminál
a	a	k8xC	a
w	w	k?	w
je	být	k5eAaImIp3nS	být
řetězcem	řetězec	k1gInSc7	řetězec
terminálů	terminál	k1gInPc2	terminál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lze	lze	k6eAd1	lze
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravé	pravý	k2eAgFnSc2d1	pravá
a	a	k8xC	a
levé	levý	k2eAgFnSc2d1	levá
lineární	lineární	k2eAgFnSc2d1	lineární
gramatiky	gramatika	k1gFnSc2	gramatika
jsou	být	k5eAaImIp3nP	být
ekvivalentní	ekvivalentní	k2eAgFnPc1d1	ekvivalentní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gramatika	gramatika	k1gFnSc1	gramatika
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
standardní	standardní	k2eAgFnSc6d1	standardní
formě	forma	k1gFnSc6	forma
(	(	kIx(	(
<g/>
regulární	regulární	k2eAgFnSc6d1	regulární
formě	forma	k1gFnSc6	forma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pouze	pouze	k6eAd1	pouze
pravidla	pravidlo	k1gNnSc2	pravidlo
tvaru	tvar	k1gInSc2	tvar
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
X	X	kA	X
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
Y	Y	kA	Y
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
aY	aY	k?	aY
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
X	X	kA	X
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
kde	kde	k6eAd1	kde
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
Y	Y	kA	Y
jsou	být	k5eAaImIp3nP	být
neterminály	neterminál	k1gInPc1	neterminál
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
právě	právě	k6eAd1	právě
jeden	jeden	k4xCgInSc4	jeden
terminál	terminál	k1gInSc4	terminál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jazyky	jazyk	k1gInPc1	jazyk
generované	generovaný	k2eAgInPc1d1	generovaný
regulárními	regulární	k2eAgFnPc7d1	regulární
gramatikami	gramatika	k1gFnPc7	gramatika
jsou	být	k5eAaImIp3nP	být
právě	právě	k6eAd1	právě
jazyky	jazyk	k1gInPc4	jazyk
rozpoznatelné	rozpoznatelný	k2eAgInPc4d1	rozpoznatelný
konečným	konečný	k2eAgInSc7d1	konečný
automatem	automat	k1gInSc7	automat
<g/>
.	.	kIx.	.
</s>
</p>
