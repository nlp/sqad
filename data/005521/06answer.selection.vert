<s>
Největším	veliký	k2eAgMnSc7d3	veliký
celosvětovým	celosvětový	k2eAgMnSc7d1	celosvětový
producentem	producent	k1gMnSc7	producent
piva	pivo	k1gNnSc2	pivo
je	být	k5eAaImIp3nS	být
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
470	[number]	k4	470
milionu	milion	k4xCgInSc2	milion
hektolitrů	hektolitr	k1gInPc2	hektolitr
piva	pivo	k1gNnSc2	pivo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přibližně	přibližně	k6eAd1	přibližně
21,1	[number]	k4	21,1
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
