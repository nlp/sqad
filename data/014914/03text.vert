<s>
Chronograf	chronograf	k1gMnSc1
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Chronograf	chronograf	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mechanický	mechanický	k2eAgInSc1d1
náramkový	náramkový	k2eAgInSc1d1
chronograf	chronograf	k1gInSc1
</s>
<s>
Slovem	slovem	k6eAd1
chronograf	chronograf	k1gInSc4
se	se	k3xPyFc4
většinou	většina	k1gFnSc7
označují	označovat	k5eAaImIp3nP
hodinky	hodinka	k1gFnPc1
vybavené	vybavený	k2eAgFnPc1d1
funkcemi	funkce	k1gFnPc7
pro	pro	k7c4
přesné	přesný	k2eAgNnSc4d1
odměřování	odměřování	k1gNnSc4
kratších	krátký	k2eAgInPc2d2
časových	časový	k2eAgInPc2d1
intervalů	interval	k1gInPc2
<g/>
,	,	kIx,
tj.	tj.	kA
funkcemi	funkce	k1gFnPc7
stopek	stopky	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chronograf	chronograf	k1gInSc1
se	se	k3xPyFc4
pozná	poznat	k5eAaPmIp3nS
podle	podle	k7c2
několika	několik	k4yIc2
subčíselníků	subčíselník	k1gInPc2
s	s	k7c7
ručkami	ručka	k1gFnPc7
a	a	k8xC
běžně	běžně	k6eAd1
dvou	dva	k4xCgNnPc2
tlačítek	tlačítko	k1gNnPc2
nad	nad	k7c7
a	a	k8xC
pod	pod	k7c7
korunkou	korunka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
elektronických	elektronický	k2eAgInPc2d1
chronografů	chronograf	k1gInPc2
se	se	k3xPyFc4
odměřovaný	odměřovaný	k2eAgInSc1d1
čas	čas	k1gInSc1
zobrazuje	zobrazovat	k5eAaImIp3nS
digitálně	digitálně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Centrální	centrální	k2eAgFnSc1d1
sekundová	sekundový	k2eAgFnSc1d1
ručička	ručička	k1gFnSc1
měří	měřit	k5eAaImIp3nS
u	u	k7c2
chronografu	chronograf	k1gInSc2
odměřovaný	odměřovaný	k2eAgInSc4d1
interval	interval	k1gInSc4
v	v	k7c6
sekundách	sekunda	k1gFnPc6
<g/>
,	,	kIx,
zvláštní	zvláštní	k2eAgFnSc1d1
ručka	ručka	k1gFnSc1
podle	podle	k7c2
provedení	provedení	k1gNnSc2
chronografu	chronograf	k1gInSc2
ukazuje	ukazovat	k5eAaImIp3nS
setiny	setina	k1gFnPc4
<g/>
,	,	kIx,
desetiny	desetina	k1gFnPc4
<g/>
,	,	kIx,
minuty	minuta	k1gFnPc4
a	a	k8xC
případně	případně	k6eAd1
hodiny	hodina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odměřování	odměřování	k1gNnSc1
se	se	k3xPyFc4
většinou	většinou	k6eAd1
spouští	spouštět	k5eAaImIp3nS
a	a	k8xC
zastavuje	zastavovat	k5eAaImIp3nS
tlačítkem	tlačítko	k1gNnSc7
nad	nad	k7c7
korunkou	korunka	k1gFnSc7
<g/>
,	,	kIx,
vynulování	vynulování	k1gNnSc1
se	se	k3xPyFc4
provádí	provádět	k5eAaImIp3nS
tlačítkem	tlačítko	k1gNnSc7
pod	pod	k7c7
korunkou	korunka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mechanismus	mechanismus	k1gInSc1
chronografu	chronograf	k1gInSc2
(	(	kIx(
<g/>
Seagull	Seagull	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Na	na	k7c6
okraji	okraj	k1gInSc6
ciferníku	ciferník	k1gInSc2
bývají	bývat	k5eAaImIp3nP
i	i	k9
další	další	k2eAgFnPc1d1
stupnice	stupnice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nelineární	lineární	k2eNgFnSc1d1
tachymetrická	tachymetrický	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
měření	měření	k1gNnSc3
rychlosti	rychlost	k1gFnSc2
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
na	na	k7c6
kilometrovém	kilometrový	k2eAgInSc6d1
úseku	úsek	k1gInSc6
dráhy	dráha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jednom	jeden	k4xCgInSc6
kilometrovníku	kilometrovník	k1gInSc6
se	se	k3xPyFc4
měření	měření	k1gNnSc1
spustí	spustit	k5eAaPmIp3nS
a	a	k8xC
na	na	k7c6
následujícím	následující	k2eAgInSc6d1
zastaví	zastavit	k5eAaPmIp3nS
<g/>
;	;	kIx,
ručka	ručka	k1gFnSc1
pak	pak	k6eAd1
přímo	přímo	k6eAd1
ukazuje	ukazovat	k5eAaImIp3nS
rychlost	rychlost	k1gFnSc1
v	v	k7c6
kilometrech	kilometr	k1gInPc6
za	za	k7c4
hodinu	hodina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lineární	lineární	k2eAgFnSc1d1
telemetrická	telemetrický	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
měří	měřit	k5eAaImIp3nS
vzdálenost	vzdálenost	k1gFnSc4
například	například	k6eAd1
blesku	blesk	k1gInSc2
nebo	nebo	k8xC
výstřelu	výstřel	k1gInSc2
<g/>
:	:	kIx,
měření	měření	k1gNnSc1
se	se	k3xPyFc4
spustí	spustit	k5eAaPmIp3nS
<g/>
,	,	kIx,
když	když	k8xS
je	být	k5eAaImIp3nS
vidět	vidět	k5eAaImF
záblesk	záblesk	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
zastaví	zastavit	k5eAaPmIp3nS
<g/>
,	,	kIx,
když	když	k8xS
je	být	k5eAaImIp3nS
ho	on	k3xPp3gNnSc4
slyšet	slyšet	k5eAaImF
<g/>
;	;	kIx,
ručka	ručka	k1gFnSc1
pak	pak	k6eAd1
ukazuje	ukazovat	k5eAaImIp3nS
vzdálenost	vzdálenost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Mechanický	mechanický	k2eAgInSc1d1
chronograf	chronograf	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
kvalitní	kvalitní	k2eAgFnPc4d1
hodinky	hodinka	k1gFnPc4
<g/>
,	,	kIx,
při	při	k7c6
začátku	začátek	k1gInSc6
měření	měření	k1gNnSc2
se	se	k3xPyFc4
kolo	kolo	k1gNnSc4
na	na	k7c6
hřídeli	hřídel	k1gInSc6
sekundové	sekundový	k2eAgFnSc2d1
ručky	ručka	k1gFnSc2
uvede	uvést	k5eAaPmIp3nS
do	do	k7c2
záběru	záběr	k1gInSc2
se	s	k7c7
sekundovým	sekundový	k2eAgNnSc7d1
kolem	kolo	k1gNnSc7
hodinek	hodinka	k1gFnPc2
<g/>
,	,	kIx,
při	při	k7c6
skončení	skončení	k1gNnSc6
měření	měření	k1gNnSc2
se	se	k3xPyFc4
ze	z	k7c2
záběru	záběr	k1gInSc2
vyřadí	vyřadit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záběr	záběr	k1gInSc1
obstarávají	obstarávat	k5eAaImIp3nP
kola	kolo	k1gNnPc1
s	s	k7c7
velmi	velmi	k6eAd1
jemným	jemný	k2eAgNnSc7d1
ozubením	ozubení	k1gNnSc7
s	s	k7c7
trojúhelníkovitými	trojúhelníkovitý	k2eAgInPc7d1
zuby	zub	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
hřídeli	hřídel	k1gInSc6
ručky	ručka	k1gFnSc2
je	být	k5eAaImIp3nS
nasazeno	nasadit	k5eAaPmNgNnS
takzvané	takzvaný	k2eAgNnSc1d1
srdíčko	srdíčko	k1gNnSc1
<g/>
,	,	kIx,
kulisa	kulisa	k1gFnSc1
ve	v	k7c6
tvaru	tvar	k1gInSc6
srdce	srdce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
vynulování	vynulování	k1gNnSc6
se	se	k3xPyFc4
na	na	k7c4
kulisu	kulisa	k1gFnSc4
přitlačí	přitlačit	k5eAaPmIp3nS
malá	malý	k2eAgFnSc1d1
kladka	kladka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
ručku	ručka	k1gFnSc4
vrátí	vrátit	k5eAaPmIp3nS
do	do	k7c2
nulové	nulový	k2eAgFnSc2d1
polohy	poloha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celý	celý	k2eAgInSc1d1
mechanismus	mechanismus	k1gInSc1
je	být	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
složitý	složitý	k2eAgInSc1d1
a	a	k8xC
vyžaduje	vyžadovat	k5eAaImIp3nS
velmi	velmi	k6eAd1
přesné	přesný	k2eAgNnSc4d1
zpracování	zpracování	k1gNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
spolehlivě	spolehlivě	k6eAd1
fungoval	fungovat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Stopky	stopka	k1gFnPc1
</s>
<s>
Telemetrická	telemetrický	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
</s>
<s>
Tachymetrická	tachymetrický	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
</s>
<s>
Chronometr	chronometr	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
chronograf	chronograf	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Krátká	krátký	k2eAgFnSc1d1
historie	historie	k1gFnSc1
chronografů	chronograf	k1gMnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Podrobný	podrobný	k2eAgInSc1d1
popis	popis	k1gInSc1
Piguetova	Piguetův	k2eAgInSc2d1
chronografu	chronograf	k1gInSc2
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Podrobný	podrobný	k2eAgInSc1d1
popis	popis	k1gInSc1
mechanismu	mechanismus	k1gInSc2
chronografu	chronograf	k1gInSc2
Valjoux	Valjoux	k1gInSc4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
M.	M.	kA
Hajn	Hajn	k1gMnSc1
<g/>
,	,	kIx,
Základy	základ	k1gInPc1
jemné	jemný	k2eAgFnSc2d1
mechaniky	mechanika	k1gFnSc2
a	a	k8xC
hodinářství	hodinářství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1953	#num#	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4289500-5	4289500-5	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85025405	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85025405	#num#	k4
</s>
