<s>
Oxford	Oxford	k1gInSc1	Oxford
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
a	a	k8xC	a
nemetropolitní	metropolitní	k2eNgInSc1d1	nemetropolitní
distrikt	distrikt	k1gInSc1	distrikt
v	v	k7c6	v
hrabství	hrabství	k1gNnSc6	hrabství
Oxfordshire	Oxfordshir	k1gInSc5	Oxfordshir
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnSc1	obyvatel
149	[number]	k4	149
800	[number]	k4	800
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nejstarší	starý	k2eAgFnSc1d3	nejstarší
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
používajících	používající	k2eAgMnPc2d1	používající
angličtinu	angličtina	k1gFnSc4	angličtina
-	-	kIx~	-
University	universita	k1gFnSc2	universita
of	of	k?	of
Oxford	Oxford	k1gInSc1	Oxford
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
město	město	k1gNnSc1	město
snové	snový	k2eAgFnSc2d1	snová
nálady	nálada	k1gFnSc2	nálada
-	-	kIx~	-
termín	termín	k1gInSc1	termín
použitý	použitý	k2eAgInSc1d1	použitý
Matthewem	Matthew	k1gMnSc7	Matthew
Arnoldem	Arnold	k1gMnSc7	Arnold
s	s	k7c7	s
odkazem	odkaz	k1gInSc7	odkaz
na	na	k7c4	na
harmonickou	harmonický	k2eAgFnSc4d1	harmonická
architekturu	architektura	k1gFnSc4	architektura
univerzitních	univerzitní	k2eAgFnPc2d1	univerzitní
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
Temže	Temže	k1gFnSc1	Temže
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
úseku	úsek	k1gInSc6	úsek
asi	asi	k9	asi
15	[number]	k4	15
km	km	kA	km
známa	známo	k1gNnSc2	známo
jako	jako	k8xS	jako
Isis	Isis	k1gFnSc1	Isis
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
byl	být	k5eAaImAgInS	být
osídlen	osídlen	k2eAgInSc1d1	osídlen
Sasy	Sas	k1gMnPc7	Sas
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xS	jako
Oxenaforda	Oxenaforda	k1gMnSc1	Oxenaforda
V	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
klášter	klášter	k1gInSc1	klášter
svaté	svatý	k2eAgFnSc2d1	svatá
Frideswidy	Frideswida	k1gFnSc2	Frideswida
a	a	k8xC	a
první	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
Anglosaské	anglosaský	k2eAgFnSc6d1	anglosaská
kronice	kronika	k1gFnSc6	kronika
u	u	k7c2	u
roku	rok	k1gInSc2	rok
912	[number]	k4	912
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Oxford	Oxford	k1gInSc1	Oxford
stal	stát	k5eAaPmAgInS	stát
důležitým	důležitý	k2eAgNnSc7d1	důležité
vojenským	vojenský	k2eAgNnSc7d1	vojenské
hraničním	hraniční	k2eAgNnSc7d1	hraniční
městem	město	k1gNnSc7	město
mezi	mezi	k7c7	mezi
Mercií	Mercie	k1gFnSc7	Mercie
a	a	k8xC	a
Wessexem	Wessex	k1gInSc7	Wessex
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
několikrát	několikrát	k6eAd1	několikrát
napaden	napadnout	k5eAaPmNgMnS	napadnout
Dány	Dán	k1gMnPc7	Dán
<g/>
.	.	kIx.	.
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1	svatá
Frideswida	Frideswida	k1gFnSc1	Frideswida
je	být	k5eAaImIp3nS	být
patronem	patron	k1gMnSc7	patron
města	město	k1gNnSc2	město
i	i	k8xC	i
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
Oxfordu	Oxford	k1gInSc2	Oxford
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
i	i	k9	i
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
Jindřicha	Jindřich	k1gMnSc2	Jindřich
II	II	kA	II
<g/>
.	.	kIx.	.
obdržel	obdržet	k5eAaPmAgMnS	obdržet
privilegia	privilegium	k1gNnPc4	privilegium
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
obyvatelům	obyvatel	k1gMnPc3	obyvatel
města	město	k1gNnSc2	město
zajišťovala	zajišťovat	k5eAaImAgNnP	zajišťovat
stejná	stejný	k2eAgNnPc1d1	stejné
práva	právo	k1gNnPc1	právo
jako	jako	k8xS	jako
měli	mít	k5eAaImAgMnP	mít
obyvatelé	obyvatel	k1gMnPc1	obyvatel
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
mnoho	mnoho	k4c1	mnoho
církevních	církevní	k2eAgFnPc2d1	církevní
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
Rewleyské	Rewleyský	k2eAgNnSc1d1	Rewleyský
opatství	opatství	k1gNnSc1	opatství
Cisterciánského	Cisterciánský	k2eAgInSc2d1	Cisterciánský
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
kláštery	klášter	k1gInPc4	klášter
řádů	řád	k1gInPc2	řád
Dominikánů	dominikán	k1gMnPc2	dominikán
<g/>
,	,	kIx,	,
Františkánů	františkán	k1gMnPc2	františkán
Karmelitek	karmelitka	k1gFnPc2	karmelitka
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
probíhalo	probíhat	k5eAaImAgNnS	probíhat
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
často	často	k6eAd1	často
i	i	k9	i
jednání	jednání	k1gNnSc4	jednání
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
ale	ale	k9	ale
také	také	k9	také
začalo	začít	k5eAaPmAgNnS	začít
vzrůstat	vzrůstat	k5eAaImF	vzrůstat
napětí	napětí	k1gNnSc4	napětí
mezi	mezi	k7c7	mezi
městem	město	k1gNnSc7	město
a	a	k8xC	a
univerzitou	univerzita	k1gFnSc7	univerzita
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
skončilo	skončit	k5eAaPmAgNnS	skončit
porobením	porobení	k1gNnSc7	porobení
města	město	k1gNnSc2	město
a	a	k8xC	a
snížením	snížení	k1gNnSc7	snížení
jeho	on	k3xPp3gInSc2	on
občanského	občanský	k2eAgInSc2d1	občanský
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
univerzitě	univerzita	k1gFnSc6	univerzita
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nejstaršími	starý	k2eAgFnPc7d3	nejstarší
fakultami	fakulta	k1gFnPc7	fakulta
byly	být	k5eAaImAgFnP	být
University	universita	k1gFnPc4	universita
College	College	k1gNnSc2	College
(	(	kIx(	(
<g/>
1249	[number]	k4	1249
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Balliol	Balliol	k1gInSc1	Balliol
(	(	kIx(	(
<g/>
1263	[number]	k4	1263
<g/>
)	)	kIx)	)
a	a	k8xC	a
Merton	Merton	k1gInSc1	Merton
(	(	kIx(	(
<g/>
1264	[number]	k4	1264
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
fakulty	fakulta	k1gFnPc1	fakulta
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
v	v	k7c6	v
době	doba	k1gFnSc6	doba
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
začaly	začít	k5eAaPmAgFnP	začít
překládat	překládat	k5eAaImF	překládat
díla	dílo	k1gNnPc4	dílo
řeckých	řecký	k2eAgMnPc2d1	řecký
filozofů	filozof	k1gMnPc2	filozof
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
díla	dílo	k1gNnPc1	dílo
měnila	měnit	k5eAaImAgFnS	měnit
evropskou	evropský	k2eAgFnSc4d1	Evropská
ideologii	ideologie	k1gFnSc4	ideologie
<g/>
,	,	kIx,	,
podporovala	podporovat	k5eAaImAgFnS	podporovat
vědecké	vědecký	k2eAgInPc4d1	vědecký
výzkumy	výzkum	k1gInPc4	výzkum
a	a	k8xC	a
pokrok	pokrok	k1gInSc4	pokrok
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
a	a	k8xC	a
měnila	měnit	k5eAaImAgFnS	měnit
názor	názor	k1gInSc4	názor
společnosti	společnost	k1gFnSc2	společnost
na	na	k7c4	na
samu	sám	k3xTgFnSc4	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
těchto	tento	k3xDgFnPc2	tento
fakult	fakulta	k1gFnPc2	fakulta
byl	být	k5eAaImAgInS	být
podporován	podporovat	k5eAaImNgInS	podporovat
církví	církev	k1gFnSc7	církev
aby	aby	kYmCp3nS	aby
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
smíru	smír	k1gInSc3	smír
mezi	mezi	k7c7	mezi
řeckou	řecký	k2eAgFnSc7d1	řecká
filozofií	filozofie	k1gFnSc7	filozofie
a	a	k8xC	a
křesťanskou	křesťanský	k2eAgFnSc7d1	křesťanská
teologií	teologie	k1gFnSc7	teologie
<g/>
.	.	kIx.	.
</s>
<s>
Christ	Christ	k1gMnSc1	Christ
Church	Church	k1gMnSc1	Church
Cathedral	Cathedral	k1gMnSc1	Cathedral
je	být	k5eAaImIp3nS	být
unikátní	unikátní	k2eAgMnSc1d1	unikátní
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
jako	jako	k8xS	jako
univerzitní	univerzitní	k2eAgFnSc1d1	univerzitní
kaple	kaple	k1gFnSc1	kaple
i	i	k8xC	i
katedrála	katedrála	k1gFnSc1	katedrála
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
chrám	chrám	k1gInSc4	chrám
svaté	svatý	k2eAgFnSc2d1	svatá
Frideswidy	Frideswida	k1gFnSc2	Frideswida
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
než	než	k8xS	než
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
roku	rok	k1gInSc2	rok
1546	[number]	k4	1546
katedrálou	katedrála	k1gFnSc7	katedrála
oxfordské	oxfordský	k2eAgFnSc2d1	Oxfordská
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
,	,	kIx,	,
rozšířen	rozšířit	k5eAaPmNgMnS	rozšířit
a	a	k8xC	a
začleněn	začlenit	k5eAaPmNgMnS	začlenit
do	do	k7c2	do
struktury	struktura	k1gFnSc2	struktura
Cardinal	Cardinal	k1gFnSc2	Cardinal
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
College	College	k1gFnSc7	College
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
občany	občan	k1gMnPc7	občan
města	město	k1gNnSc2	město
a	a	k8xC	a
univerzitní	univerzitní	k2eAgFnSc7d1	univerzitní
komunitou	komunita	k1gFnSc7	komunita
nebyly	být	k5eNaImAgFnP	být
nikdy	nikdy	k6eAd1	nikdy
příliš	příliš	k6eAd1	příliš
vřelé	vřelý	k2eAgFnPc4d1	vřelá
a	a	k8xC	a
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
o	o	k7c6	o
čemž	což	k3yQnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
roku	rok	k1gInSc2	rok
1355	[number]	k4	1355
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
St	St	kA	St
Scholastica	Scholastica	k1gFnSc1	Scholastica
Day	Day	k1gFnSc2	Day
zabito	zabít	k5eAaPmNgNnS	zabít
několik	několik	k4yIc1	několik
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Anglické	anglický	k2eAgFnSc2d1	anglická
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
roku	rok	k1gInSc2	rok
1642	[number]	k4	1642
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
sídlil	sídlit	k5eAaImAgInS	sídlit
dvůr	dvůr	k1gInSc1	dvůr
Karla	Karel	k1gMnSc2	Karel
I.	I.	kA	I.
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k3yInSc1	co
byl	být	k5eAaImAgMnS	být
král	král	k1gMnSc1	král
vyhnán	vyhnat	k5eAaPmNgMnS	vyhnat
z	z	k7c2	z
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
parlamentaristé	parlamentarista	k1gMnPc1	parlamentarista
měli	mít	k5eAaImAgMnP	mít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
silnou	silný	k2eAgFnSc4d1	silná
podporu	podpora	k1gFnSc4	podpora
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
vzdalo	vzdát	k5eAaPmAgNnS	vzdát
parlamentaristům	parlamentarista	k1gMnPc3	parlamentarista
roku	rok	k1gInSc2	rok
1642	[number]	k4	1642
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
Velkého	velký	k2eAgInSc2d1	velký
moru	mor	k1gInSc2	mor
se	se	k3xPyFc4	se
do	do	k7c2	do
města	město	k1gNnSc2	město
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
dvůr	dvůr	k1gInSc1	dvůr
Karla	Karel	k1gMnSc2	Karel
II	II	kA	II
<g/>
..	..	k?	..
</s>
<s>
Roku	rok	k1gInSc2	rok
1790	[number]	k4	1790
byl	být	k5eAaImAgInS	být
Oxford	Oxford	k1gInSc1	Oxford
spojen	spojit	k5eAaPmNgInS	spojit
Oxfordským	oxfordský	k2eAgInSc7d1	oxfordský
kanálem	kanál	k1gInSc7	kanál
s	s	k7c7	s
Coventry	Coventr	k1gMnPc7	Coventr
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1796	[number]	k4	1796
vybudovala	vybudovat	k5eAaPmAgFnS	vybudovat
společnost	společnost	k1gFnSc1	společnost
vlastnící	vlastnící	k2eAgInSc4d1	vlastnící
tento	tento	k3xDgInSc4	tento
kanál	kanál	k1gInSc4	kanál
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
řekou	řeka	k1gFnSc7	řeka
Temží	Temže	k1gFnPc2	Temže
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
spojnice	spojnice	k1gFnSc1	spojnice
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
Isi	Isi	k1gFnSc1	Isi
Lock	Locka	k1gFnPc2	Locka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1840	[number]	k4	1840
společnosti	společnost	k1gFnSc2	společnost
Great	Great	k2eAgInSc4d1	Great
Western	western	k1gInSc4	western
Railway	Railwaa	k1gFnSc2	Railwaa
a	a	k8xC	a
London	London	k1gMnSc1	London
and	and	k?	and
North	North	k1gMnSc1	North
Western	Western	kA	Western
Railway	Railwaa	k1gFnPc1	Railwaa
spojily	spojit	k5eAaPmAgFnP	spojit
Oxford	Oxford	k1gInSc4	Oxford
s	s	k7c7	s
Londýnem	Londýn	k1gInSc7	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Stavitelem	stavitel	k1gMnSc7	stavitel
Oxfordské	oxfordský	k2eAgFnSc2d1	Oxfordská
radnice	radnice	k1gFnSc2	radnice
byl	být	k5eAaImAgInS	být
Henry	henry	k1gInSc1	henry
T.	T.	kA	T.
Hare	Har	k1gInPc1	Har
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1893	[number]	k4	1893
a	a	k8xC	a
otevřena	otevřít	k5eAaPmNgFnS	otevřít
byla	být	k5eAaImAgFnS	být
Eduardem	Eduard	k1gMnSc7	Eduard
VII	VII	kA	VII
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1897	[number]	k4	1897
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
sídlo	sídlo	k1gNnSc1	sídlo
místní	místní	k2eAgFnSc2d1	místní
správy	správa	k1gFnSc2	správa
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1292	[number]	k4	1292
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
Oxford	Oxford	k1gInSc1	Oxford
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
výrazný	výrazný	k2eAgInSc4d1	výrazný
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
hnacím	hnací	k2eAgInSc7d1	hnací
motorem	motor	k1gInSc7	motor
byl	být	k5eAaImAgInS	být
tiskařský	tiskařský	k2eAgInSc1d1	tiskařský
a	a	k8xC	a
nakladatelský	nakladatelský	k2eAgInSc1d1	nakladatelský
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
založením	založení	k1gNnSc7	založení
Morris	Morris	k1gFnSc2	Morris
Motor	motor	k1gInSc1	motor
Company	Compana	k1gFnSc2	Compana
k	k	k7c3	k
významné	významný	k2eAgFnSc3d1	významná
změně	změna	k1gFnSc3	změna
struktury	struktura	k1gFnSc2	struktura
ekonomiky	ekonomika	k1gFnSc2	ekonomika
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
v	v	k7c4	v
Cowley	Cowley	k1gInPc4	Cowley
na	na	k7c6	na
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hromadně	hromadně	k6eAd1	hromadně
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
automobily	automobil	k1gInPc1	automobil
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
asi	asi	k9	asi
20	[number]	k4	20
000	[number]	k4	000
pracovníků	pracovník	k1gMnPc2	pracovník
i	i	k8xC	i
když	když	k8xS	když
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
znatelnému	znatelný	k2eAgInSc3d1	znatelný
poklesu	pokles	k1gInSc3	pokles
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
tak	tak	k9	tak
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
tváře	tvář	k1gFnPc4	tvář
-	-	kIx~	-
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
Magdalen	Magdalena	k1gFnPc2	Magdalena
Bridge	Bridg	k1gFnSc2	Bridg
je	být	k5eAaImIp3nS	být
univerzitní	univerzitní	k2eAgFnSc4d1	univerzitní
část	část	k1gFnSc4	část
a	a	k8xC	a
na	na	k7c4	na
východ	východ	k1gInSc4	východ
je	být	k5eAaImIp3nS	být
automobilová	automobilový	k2eAgFnSc1d1	automobilová
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
hledajících	hledající	k2eAgMnPc2d1	hledající
práci	práce	k1gFnSc4	práce
v	v	k7c6	v
automobilových	automobilový	k2eAgInPc6d1	automobilový
závodech	závod	k1gInPc6	závod
<g/>
,	,	kIx,	,
nedávná	dávný	k2eNgFnSc1d1	nedávná
vlna	vlna	k1gFnSc1	vlna
imigrace	imigrace	k1gFnSc2	imigrace
z	z	k7c2	z
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
studentská	studentský	k2eAgFnSc1d1	studentská
komunita	komunita	k1gFnSc1	komunita
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
z	z	k7c2	z
Oxfordu	Oxford	k1gInSc2	Oxford
kosmopolitní	kosmopolitní	k2eAgNnSc1d1	kosmopolitní
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
především	především	k9	především
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Headingtonu	Headington	k1gInSc2	Headington
a	a	k8xC	a
Cowley	Cowlea	k1gFnSc2	Cowlea
Road	Road	k1gMnSc1	Road
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
bary	bar	k1gInPc7	bar
<g/>
,	,	kIx,	,
kavárnami	kavárna	k1gFnPc7	kavárna
<g/>
,	,	kIx,	,
restauracemi	restaurace	k1gFnPc7	restaurace
<g/>
,	,	kIx,	,
kluby	klub	k1gInPc7	klub
<g/>
,	,	kIx,	,
exotickými	exotický	k2eAgInPc7d1	exotický
obchody	obchod	k1gInPc7	obchod
a	a	k8xC	a
rychlým	rychlý	k2eAgNnSc7d1	rychlé
občerstvením	občerstvení	k1gNnSc7	občerstvení
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
s	s	k7c7	s
19,3	[number]	k4	19,3
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
narozených	narozený	k2eAgFnPc2d1	narozená
mimo	mimo	k7c4	mimo
Velkou	velký	k2eAgFnSc4d1	velká
Británii	Británie	k1gFnSc4	Británie
paří	pařit	k5eAaImIp3nS	pařit
mezi	mezi	k7c4	mezi
města	město	k1gNnPc4	město
s	s	k7c7	s
nejvíce	hodně	k6eAd3	hodně
různorodou	různorodý	k2eAgFnSc4d1	různorodá
populaci	populace	k1gFnSc4	populace
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yQnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
23,2	[number]	k4	23,2
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
tvoří	tvořit	k5eAaImIp3nS	tvořit
etnické	etnický	k2eAgFnPc4d1	etnická
menšiny	menšina	k1gFnPc4	menšina
a	a	k8xC	a
12,9	[number]	k4	12,9
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
nejsou	být	k5eNaImIp3nP	být
běloši	běloch	k1gMnPc1	běloch
<g/>
.	.	kIx.	.
</s>
<s>
Etnický	etnický	k2eAgInSc1d1	etnický
původ	původ	k1gInSc1	původ
(	(	kIx(	(
<g/>
sčítání	sčítání	k1gNnSc1	sčítání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
77,7	[number]	k4	77,7
<g/>
%	%	kIx~	%
-	-	kIx~	-
běloši	běloch	k1gMnPc1	běloch
(	(	kIx(	(
<g/>
63,6	[number]	k4	63,6
<g/>
%	%	kIx~	%
bílí	bílý	k2eAgMnPc1d1	bílý
Britové	Brit	k1gMnPc1	Brit
<g/>
)	)	kIx)	)
12,4	[number]	k4	12,4
<g/>
%	%	kIx~	%
-	-	kIx~	-
Asiaté	Asiat	k1gMnPc1	Asiat
4,6	[number]	k4	4,6
<g/>
%	%	kIx~	%
-	-	kIx~	-
černoši	černoch	k1gMnPc1	černoch
4,0	[number]	k4	4,0
<g/>
%	%	kIx~	%
-	-	kIx~	-
míšenci	míšenec	k1gMnSc3	míšenec
0,6	[number]	k4	0,6
<g/>
%	%	kIx~	%
-	-	kIx~	-
Arabové	Arab	k1gMnPc1	Arab
0,7	[number]	k4	0,7
<g/>
%	%	kIx~	%
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
ostatní	ostatní	k2eAgNnSc1d1	ostatní
Náboženství	náboženství	k1gNnSc1	náboženství
(	(	kIx(	(
<g/>
sčítání	sčítání	k1gNnSc1	sčítání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
48,0	[number]	k4	48,0
<g/>
%	%	kIx~	%
-	-	kIx~	-
křesťanství	křesťanství	k1gNnSc4	křesťanství
6,8	[number]	k4	6,8
<g/>
%	%	kIx~	%
-	-	kIx~	-
islám	islám	k1gInSc1	islám
1,3	[number]	k4	1,3
<g/>
%	%	kIx~	%
-	-	kIx~	-
hinduismus	hinduismus	k1gInSc1	hinduismus
0,3	[number]	k4	0,3
<g/>
%	%	kIx~	%
-	-	kIx~	-
sikhismus	sikhismus	k1gInSc1	sikhismus
0,9	[number]	k4	0,9
<g/>
%	%	kIx~	%
-	-	kIx~	-
buddhismus	buddhismus	k1gInSc1	buddhismus
0,7	[number]	k4	0,7
<g/>
%	%	kIx~	%
-	-	kIx~	-
judaismus	judaismus	k1gInSc1	judaismus
0,5	[number]	k4	0,5
<g/>
%	%	kIx~	%
-	-	kIx~	-
ostatní	ostatní	k2eAgNnSc1d1	ostatní
náboženství	náboženství	k1gNnSc1	náboženství
33,1	[number]	k4	33,1
<g/>
%	%	kIx~	%
-	-	kIx~	-
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnPc2	vyznání
6,3	[number]	k4	6,3
<g/>
%	%	kIx~	%
-	-	kIx~	-
neuvedeno	uveden	k2eNgNnSc1d1	neuvedeno
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
rady	rada	k1gFnSc2	rada
města	město	k1gNnSc2	město
každý	každý	k3xTgInSc1	každý
sudý	sudý	k2eAgInSc1d1	sudý
rok	rok	k1gInSc1	rok
a	a	k8xC	a
délka	délka	k1gFnSc1	délka
mandátu	mandát	k1gInSc2	mandát
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
radních	radní	k1gMnPc2	radní
trvá	trvat	k5eAaImIp3nS	trvat
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
volební	volební	k2eAgInSc1d1	volební
obvod	obvod	k1gInSc1	obvod
je	být	k5eAaImIp3nS	být
zastoupen	zastoupit	k5eAaPmNgInS	zastoupit
dvěma	dva	k4xCgFnPc7	dva
radními	radní	k1gFnPc7	radní
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
každých	každý	k3xTgFnPc6	každý
volbách	volba	k1gFnPc6	volba
je	být	k5eAaImIp3nS	být
volen	volen	k2eAgMnSc1d1	volen
jeden	jeden	k4xCgMnSc1	jeden
radní	radní	k1gMnSc1	radní
z	z	k7c2	z
každého	každý	k3xTgInSc2	každý
obvodu	obvod	k1gInSc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byla	být	k5eAaImAgFnS	být
frekvence	frekvence	k1gFnSc1	frekvence
voleb	volba	k1gFnPc2	volba
tříletá	tříletý	k2eAgFnSc1d1	tříletá
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
Oxford	Oxford	k1gInSc1	Oxford
tvoří	tvořit	k5eAaImIp3nS	tvořit
kompaktní	kompaktní	k2eAgFnSc4d1	kompaktní
obydlenou	obydlený	k2eAgFnSc4d1	obydlená
oblast	oblast	k1gFnSc4	oblast
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
rámci	rámec	k1gInSc6	rámec
čtyři	čtyři	k4xCgFnPc1	čtyři
obce	obec	k1gFnPc1	obec
s	s	k7c7	s
obecními	obecní	k2eAgFnPc7d1	obecní
radami	rada	k1gFnPc7	rada
-	-	kIx~	-
Blackbird	Blackbird	k1gInSc1	Blackbird
Leys	Leysa	k1gFnPc2	Leysa
<g/>
,	,	kIx,	,
Littlemore	Littlemor	k1gMnSc5	Littlemor
<g/>
,	,	kIx,	,
Old	Olda	k1gFnPc2	Olda
Marston	Marston	k1gInSc1	Marston
a	a	k8xC	a
Risinghurst	Risinghurst	k1gInSc1	Risinghurst
and	and	k?	and
Sandhills	Sandhills	k1gInSc1	Sandhills
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
volební	volební	k2eAgInPc4d1	volební
obvody	obvod	k1gInPc4	obvod
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
-	-	kIx~	-
Oxford	Oxford	k1gInSc1	Oxford
East	East	k1gInSc1	East
a	a	k8xC	a
Oxford	Oxford	k1gInSc1	Oxford
West	Westa	k1gFnPc2	Westa
a	a	k8xC	a
Abingdon	Abingdona	k1gFnPc2	Abingdona
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
asi	asi	k9	asi
90	[number]	k4	90
km	km	kA	km
na	na	k7c4	na
severozápad	severozápad	k1gInSc4	severozápad
od	od	k7c2	od
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
asi	asi	k9	asi
100	[number]	k4	100
km	km	kA	km
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Birminghamu	Birmingham	k1gInSc2	Birmingham
<g/>
.	.	kIx.	.
</s>
<s>
Dálnice	dálnice	k1gFnSc1	dálnice
M40	M40	k1gFnSc2	M40
prochází	procházet	k5eAaImIp3nS	procházet
asi	asi	k9	asi
12	[number]	k4	12
km	km	kA	km
od	od	k7c2	od
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Silnice	silnice	k1gFnSc1	silnice
A34	A34	k1gFnSc2	A34
spojující	spojující	k2eAgFnSc1d1	spojující
Hampshire	Hampshir	k1gInSc5	Hampshir
a	a	k8xC	a
Midlands	Midlands	k1gInSc1	Midlands
prochází	procházet	k5eAaImIp3nS	procházet
Oxfordem	Oxford	k1gInSc7	Oxford
a	a	k8xC	a
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
část	část	k1gFnSc4	část
jeho	jeho	k3xOp3gInSc2	jeho
západního	západní	k2eAgInSc2d1	západní
obchvatu	obchvat	k1gInSc2	obchvat
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
důležitými	důležitý	k2eAgFnPc7d1	důležitá
silnicemi	silnice	k1gFnPc7	silnice
jsou	být	k5eAaImIp3nP	být
A40	A40	k1gMnPc1	A40
-	-	kIx~	-
spojující	spojující	k2eAgNnSc1d1	spojující
město	město	k1gNnSc1	město
s	s	k7c7	s
Londýnem	Londýn	k1gInSc7	Londýn
a	a	k8xC	a
A420	A420	k1gFnSc7	A420
směřující	směřující	k2eAgFnSc7d1	směřující
k	k	k7c3	k
Bristolu	Bristol	k1gInSc3	Bristol
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnSc1	železnice
spojuje	spojovat	k5eAaImIp3nS	spojovat
město	město	k1gNnSc4	město
s	s	k7c7	s
Paddingtonem	Paddington	k1gInSc7	Paddington
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
Bournemouthem	Bournemouth	k1gInSc7	Bournemouth
<g/>
,	,	kIx,	,
Worcesterem	Worcester	k1gInSc7	Worcester
<g/>
,	,	kIx,	,
Birminghamem	Birmingham	k1gInSc7	Birmingham
<g/>
,	,	kIx,	,
Coventry	Coventr	k1gMnPc7	Coventr
<g/>
,	,	kIx,	,
Banbury	Banbur	k1gMnPc7	Banbur
a	a	k8xC	a
Bicesterem	Bicester	k1gInSc7	Bicester
<g/>
.	.	kIx.	.
</s>
<s>
Linka	linka	k1gFnSc1	linka
do	do	k7c2	do
Bicesteru	Bicester	k1gInSc2	Bicester
je	být	k5eAaImIp3nS	být
částí	část	k1gFnSc7	část
původní	původní	k2eAgFnSc2d1	původní
trasy	trasa	k1gFnSc2	trasa
spojující	spojující	k2eAgInSc1d1	spojující
Oxford	Oxford	k1gInSc1	Oxford
s	s	k7c7	s
Cambridge	Cambridge	k1gFnSc2	Cambridge
známé	známý	k1gMnPc4	známý
jako	jako	k8xS	jako
Varsity	Varsit	k1gInPc4	Varsit
Line	linout	k5eAaImIp3nS	linout
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
této	tento	k3xDgFnSc2	tento
linky	linka	k1gFnSc2	linka
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Oxfordský	oxfordský	k2eAgInSc4d1	oxfordský
kanál	kanál	k1gInSc4	kanál
spojuje	spojovat	k5eAaImIp3nS	spojovat
město	město	k1gNnSc1	město
s	s	k7c7	s
oblastí	oblast	k1gFnSc7	oblast
Midlands	Midlandsa	k1gFnPc2	Midlandsa
a	a	k8xC	a
poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
i	i	k9	i
s	s	k7c7	s
řekou	řeka	k1gFnSc7	řeka
Temží	Temže	k1gFnPc2	Temže
<g/>
.	.	kIx.	.
</s>
<s>
Temže	Temže	k1gFnSc1	Temže
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
ale	ale	k9	ale
používá	používat	k5eAaImIp3nS	používat
spíše	spíše	k9	spíše
pro	pro	k7c4	pro
vyhlídkové	vyhlídkový	k2eAgFnPc4d1	vyhlídková
plavby	plavba	k1gFnPc4	plavba
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgNnSc4d1	Malé
soukromé	soukromý	k2eAgNnSc4d1	soukromé
Oxfordské	oxfordský	k2eAgNnSc4d1	Oxfordské
letiště	letiště	k1gNnSc4	letiště
v	v	k7c6	v
Kidlingtonu	Kidlington	k1gInSc6	Kidlington
odbavuje	odbavovat	k5eAaImIp3nS	odbavovat
obchodní	obchodní	k2eAgInPc4d1	obchodní
a	a	k8xC	a
soukromé	soukromý	k2eAgInPc4d1	soukromý
lety	let	k1gInPc4	let
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnSc1d1	místní
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
zabezpečována	zabezpečován	k2eAgFnSc1d1	zabezpečována
dvěma	dva	k4xCgFnPc7	dva
společnostmi	společnost	k1gFnPc7	společnost
-	-	kIx~	-
Oxford	Oxford	k1gInSc1	Oxford
Bus	bus	k1gInSc1	bus
Company	Compana	k1gFnSc2	Compana
a	a	k8xC	a
Stagecoach	Stagecoach	k1gInSc1	Stagecoach
South	Southa	k1gFnPc2	Southa
Midlands	Midlandsa	k1gFnPc2	Midlandsa
<g/>
.	.	kIx.	.
</s>
<s>
Autobusy	autobus	k1gInPc1	autobus
obou	dva	k4xCgFnPc2	dva
společností	společnost	k1gFnPc2	společnost
využívají	využívat	k5eAaImIp3nP	využívat
autobusové	autobusový	k2eAgNnSc4d1	autobusové
stanoviště	stanoviště	k1gNnSc4	stanoviště
Gloucester	Gloucester	k1gInSc4	Gloucester
Green	Green	k2eAgInSc4d1	Green
Bus	bus	k1gInSc4	bus
Station	station	k1gInSc4	station
umístěné	umístěný	k2eAgFnSc2d1	umístěná
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
je	být	k5eAaImIp3nS	být
poskytováno	poskytovat	k5eAaImNgNnS	poskytovat
především	především	k9	především
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
na	na	k7c4	na
Londýn	Londýn	k1gInSc4	Londýn
<g/>
,	,	kIx,	,
Cambridge	Cambridge	k1gFnSc1	Cambridge
a	a	k8xC	a
Northampton	Northampton	k1gInSc1	Northampton
<g/>
.	.	kIx.	.
</s>
<s>
Oxfordem	Oxford	k1gInSc7	Oxford
projíždějí	projíždět	k5eAaImIp3nP	projíždět
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
linky	linka	k1gFnPc1	linka
společnosti	společnost	k1gFnSc2	společnost
National	National	k1gFnSc2	National
Express	express	k1gInSc1	express
vedoucí	vedoucí	k1gFnSc2	vedoucí
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Midlands	Midlandsa	k1gFnPc2	Midlandsa
a	a	k8xC	a
severem	sever	k1gInSc7	sever
země	zem	k1gFnSc2	zem
do	do	k7c2	do
na	na	k7c4	na
jih	jih	k1gInSc4	jih
a	a	k8xC	a
jihozápad	jihozápad	k1gInSc4	jihozápad
<g/>
.	.	kIx.	.
</s>
<s>
Příměstské	příměstský	k2eAgInPc4d1	příměstský
spoje	spoj	k1gInPc4	spoj
do	do	k7c2	do
okolních	okolní	k2eAgNnPc2d1	okolní
měst	město	k1gNnPc2	město
a	a	k8xC	a
vesnic	vesnice	k1gFnPc2	vesnice
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
společnost	společnost	k1gFnSc1	společnost
Thames	Thamesa	k1gFnPc2	Thamesa
Travel	Travel	k1gInSc4	Travel
se	s	k7c7	s
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
Wallingfordu	Wallingford	k1gInSc6	Wallingford
<g/>
.	.	kIx.	.
</s>
<s>
University	universita	k1gFnPc1	universita
of	of	k?	of
Oxford	Oxford	k1gInSc1	Oxford
Ashmolean	Ashmolean	k1gInSc1	Ashmolean
Museum	museum	k1gNnSc1	museum
-	-	kIx~	-
nejstarší	starý	k2eAgNnSc4d3	nejstarší
britské	britský	k2eAgNnSc4d1	Britské
muzeum	muzeum	k1gNnSc4	muzeum
Pitt	Pitt	k2eAgInSc1d1	Pitt
Rivers	Rivers	k1gInSc1	Rivers
Museum	museum	k1gNnSc1	museum
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Natural	Natural	k?	Natural
History	Histor	k1gMnPc7	Histor
Museum	museum	k1gNnSc1	museum
of	of	k?	of
the	the	k?	the
History	Histor	k1gInPc4	Histor
of	of	k?	of
Science	Science	k1gFnSc1	Science
-	-	kIx~	-
nejstarší	starý	k2eAgNnSc1d3	nejstarší
účelově	účelově	k6eAd1	účelově
postavené	postavený	k2eAgNnSc1d1	postavené
muzeum	muzeum	k1gNnSc1	muzeum
Bate	Bat	k1gInSc2	Bat
Collection	Collection	k1gInSc1	Collection
of	of	k?	of
Musical	musical	k1gInSc1	musical
Instruments	Instruments	kA	Instruments
Museum	museum	k1gNnSc4	museum
of	of	k?	of
Oxford	Oxford	k1gInSc1	Oxford
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Modern	Modern	k1gNnSc1	Modern
Art	Art	k1gMnSc2	Art
Science	Science	k1gFnSc1	Science
Oxford	Oxford	k1gInSc1	Oxford
OVADA	OVADA	kA	OVADA
Oxford	Oxford	k1gInSc4	Oxford
Playhouse	Playhouse	k1gFnSc2	Playhouse
na	na	k7c4	na
Beaumont	Beaumont	k1gInSc4	Beaumont
Street	Street	k1gMnSc1	Street
New	New	k1gMnSc1	New
Theatre	Theatr	k1gInSc5	Theatr
na	na	k7c4	na
<g />
.	.	kIx.	.
</s>
<s>
George	George	k1gInSc1	George
Street	Streeta	k1gFnPc2	Streeta
Burton	Burton	k1gInSc1	Burton
Taylor	Taylor	k1gMnSc1	Taylor
Theatre	Theatr	k1gInSc5	Theatr
na	na	k7c4	na
Worcester	Worcester	k1gInSc4	Worcester
Street	Street	k1gInSc4	Street
Old	Olda	k1gFnPc2	Olda
Fire	Fir	k1gInSc2	Fir
Station	station	k1gInSc4	station
Theatre	Theatr	k1gInSc5	Theatr
na	na	k7c4	na
George	Georg	k1gMnSc4	Georg
Street	Street	k1gMnSc1	Street
Pegasus	Pegasus	k1gMnSc1	Pegasus
Theatre	Theatr	k1gInSc5	Theatr
na	na	k7c4	na
Magdalen	Magdalena	k1gFnPc2	Magdalena
Road	Road	k1gInSc1	Road
Ultimate	Ultimat	k1gInSc5	Ultimat
Picture	Pictur	k1gMnSc5	Pictur
Palace	Palace	k1gFnPc1	Palace
na	na	k7c4	na
Cowley	Cowley	k1gInPc4	Cowley
Road	Road	k1gMnSc1	Road
Phoenix	Phoenix	k1gInSc4	Phoenix
Picturehouse	Picturehouse	k1gFnSc2	Picturehouse
na	na	k7c4	na
Walton	Walton	k1gInSc4	Walton
Street	Street	k1gInSc1	Street
Odeon	odeon	k1gInSc1	odeon
Cinema	Cinem	k1gMnSc2	Cinem
na	na	k7c4	na
George	George	k1gInSc4	George
Street	Street	k1gInSc1	Street
Odeon	odeon	k1gInSc1	odeon
Cinema	Cinem	k1gMnSc2	Cinem
na	na	k7c4	na
Magdalen	Magdalena	k1gFnPc2	Magdalena
Street	Street	k1gInSc4	Street
Vue	Vue	k1gFnSc4	Vue
Cinema	Cinem	k1gMnSc2	Cinem
na	na	k7c4	na
Grenoble	Grenoble	k1gInSc4	Grenoble
Road	Roada	k1gFnPc2	Roada
Mezi	mezi	k7c4	mezi
populární	populární	k2eAgFnPc4d1	populární
noviny	novina	k1gFnPc4	novina
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zařadit	zařadit	k5eAaPmF	zařadit
týdeník	týdeník	k1gInSc4	týdeník
Oxford	Oxford	k1gInSc1	Oxford
Times	Times	k1gInSc1	Times
<g/>
,	,	kIx,	,
bulvární	bulvární	k2eAgInSc1d1	bulvární
Oxford	Oxford	k1gInSc1	Oxford
Mail	mail	k1gInSc1	mail
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
Star	Star	kA	Star
a	a	k8xC	a
Oxford	Oxford	k1gInSc4	Oxford
Journal	Journal	k1gFnSc2	Journal
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
vysílá	vysílat	k5eAaImIp3nS	vysílat
několik	několik	k4yIc1	několik
lokálních	lokální	k2eAgFnPc2d1	lokální
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
stanic	stanice	k1gFnPc2	stanice
-	-	kIx~	-
BBC	BBC	kA	BBC
Radio	radio	k1gNnSc1	radio
Oxford	Oxford	k1gInSc1	Oxford
<g/>
,	,	kIx,	,
Fox	fox	k1gInSc1	fox
FM	FM	kA	FM
<g/>
,	,	kIx,	,
FM	FM	kA	FM
<g/>
107.9	[number]	k4	107.9
a	a	k8xC	a
Oxide	oxid	k1gInSc5	oxid
-	-	kIx~	-
Oxfordská	oxfordský	k2eAgFnSc1d1	Oxfordská
studentská	studentský	k2eAgFnSc1d1	studentská
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Televizní	televizní	k2eAgNnSc1d1	televizní
vysílání	vysílání	k1gNnSc1	vysílání
je	být	k5eAaImIp3nS	být
zastoupeno	zastoupit	k5eAaPmNgNnS	zastoupit
Six	Six	k1gFnSc7	Six
TV	TV	kA	TV
a	a	k8xC	a
místní	místní	k2eAgFnSc7d1	místní
redakcí	redakce	k1gFnSc7	redakce
BBC	BBC	kA	BBC
TV	TV	kA	TV
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
podklady	podklad	k1gInPc4	podklad
pro	pro	k7c4	pro
lokální	lokální	k2eAgNnSc4d1	lokální
vysílání	vysílání	k1gNnSc4	vysílání
ze	z	k7c2	z
Southamptonu	Southampton	k1gInSc2	Southampton
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
sídlí	sídlet	k5eAaImIp3nS	sídlet
neobvykle	obvykle	k6eNd1	obvykle
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
jsou	být	k5eAaImIp3nP	být
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
studenti	student	k1gMnPc1	student
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
zde	zde	k6eAd1	zde
tři	tři	k4xCgFnPc1	tři
církevní	církevní	k2eAgFnPc1d1	církevní
školy	škola	k1gFnPc1	škola
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
udržují	udržovat	k5eAaImIp3nP	udržovat
tradici	tradice	k1gFnSc4	tradice
chlapeckých	chlapecký	k2eAgFnPc2d1	chlapecká
škol	škola	k1gFnPc2	škola
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
výsledků	výsledek	k1gInPc2	výsledek
zkoušek	zkouška	k1gFnPc2	zkouška
pod	pod	k7c7	pod
celostátním	celostátní	k2eAgInSc7d1	celostátní
průměrem	průměr	k1gInSc7	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Oxfordské	oxfordský	k2eAgFnPc1d1	Oxfordská
střední	střední	k2eAgFnPc1d1	střední
školy	škola	k1gFnPc1	škola
<g/>
:	:	kIx,	:
Cheney	Cheney	k1gInPc1	Cheney
School	Schoola	k1gFnPc2	Schoola
Cherwell	Cherwella	k1gFnPc2	Cherwella
School	School	k1gInSc1	School
Christ	Christ	k1gMnSc1	Christ
Church	Church	k1gMnSc1	Church
Cathedral	Cathedral	k1gMnSc1	Cathedral
School	School	k1gInSc1	School
(	(	kIx(	(
<g/>
církevní	církevní	k2eAgFnSc1d1	církevní
škola	škola	k1gFnSc1	škola
<g/>
)	)	kIx)	)
Dragon	Dragon	k1gMnSc1	Dragon
School	Schoola	k1gFnPc2	Schoola
Headington	Headington	k1gInSc4	Headington
School	School	k1gInSc4	School
Magdalen	Magdalena	k1gFnPc2	Magdalena
College	Colleg	k1gInSc2	Colleg
School	Schoola	k1gFnPc2	Schoola
(	(	kIx(	(
<g/>
církevní	církevní	k2eAgFnSc1d1	církevní
škola	škola	k1gFnSc1	škola
<g/>
)	)	kIx)	)
New	New	k1gFnSc1	New
College	Colleg	k1gFnSc2	Colleg
School	Schoola	k1gFnPc2	Schoola
(	(	kIx(	(
<g/>
církevní	církevní	k2eAgFnSc1d1	církevní
škola	škola	k1gFnSc1	škola
<g/>
)	)	kIx)	)
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Overbroeck	Overbroeck	k1gMnSc1	Overbroeck
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
College	College	k6eAd1	College
Oxford	Oxford	k1gInSc1	Oxford
High	High	k1gInSc1	High
School	School	k1gInSc1	School
Oxford	Oxford	k1gInSc4	Oxford
Community	Communita	k1gFnSc2	Communita
School	Schoola	k1gFnPc2	Schoola
Peers	Peersa	k1gFnPc2	Peersa
School	School	k1gInSc1	School
Rye	Rye	k1gMnSc1	Rye
St	St	kA	St
Antony	anton	k1gInPc1	anton
School	Schoola	k1gFnPc2	Schoola
St	St	kA	St
Clare	Clar	k1gInSc5	Clar
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
College	College	k1gFnSc7	College
St	St	kA	St
Edward	Edward	k1gMnSc1	Edward
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
School	Schoola	k1gFnPc2	Schoola
St	St	kA	St
Gregory	Gregor	k1gMnPc4	Gregor
the	the	k?	the
Great	Great	k2eAgInSc1d1	Great
School	School	k1gInSc1	School
Summer	Summer	k1gInSc1	Summer
Fields	Fieldsa	k1gFnPc2	Fieldsa
School	Schoola	k1gFnPc2	Schoola
Wheatley	Wheatlea	k1gFnSc2	Wheatlea
Park	park	k1gInSc1	park
School	School	k1gInSc1	School
Druhá	druhý	k4xOgFnSc1	druhý
Oxfordská	oxfordský	k2eAgFnSc1d1	Oxfordská
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
Brookes	Brookes	k1gInSc1	Brookes
University	universita	k1gFnSc2	universita
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
Oxford	Oxford	k1gInSc1	Oxford
School	School	k1gInSc1	School
of	of	k?	of
Art	Art	k1gFnSc2	Art
<g/>
,	,	kIx,	,
sídlí	sídlet	k5eAaImIp3nS	sídlet
na	na	k7c4	na
Headington	Headington	k1gInSc4	Headington
Hill	Hill	k1gInSc4	Hill
obdržela	obdržet	k5eAaPmAgFnS	obdržet
status	status	k1gInSc4	status
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
a	a	k8xC	a
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
hodnocena	hodnotit	k5eAaImNgFnS	hodnotit
jako	jako	k8xC	jako
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
z	z	k7c2	z
nových	nový	k2eAgFnPc2d1	nová
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
turistických	turistický	k2eAgFnPc2d1	turistická
atrakcí	atrakce	k1gFnPc2	atrakce
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
patří	patřit	k5eAaImIp3nS	patřit
univerzitě	univerzita	k1gFnSc3	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
mimo	mimo	k6eAd1	mimo
jiných	jiný	k2eAgFnPc2d1	jiná
zajímavostí	zajímavost	k1gFnPc2	zajímavost
nachází	nacházet	k5eAaImIp3nS	nacházet
Carfax	Carfax	k1gInSc1	Carfax
Tower	Tower	k1gInSc1	Tower
-	-	kIx~	-
jediná	jediný	k2eAgFnSc1d1	jediná
dochovaná	dochovaný	k2eAgFnSc1d1	dochovaná
část	část	k1gFnSc1	část
kostela	kostel	k1gInSc2	kostel
Svatého	svatý	k2eAgMnSc4d1	svatý
Martina	Martin	k1gMnSc4	Martin
pocházejícího	pocházející	k2eAgMnSc4d1	pocházející
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
obchodů	obchod	k1gInPc2	obchod
se	s	k7c7	s
suvenýry	suvenýr	k1gInPc7	suvenýr
a	a	k8xC	a
jiným	jiný	k2eAgNnSc7d1	jiné
zajímavým	zajímavý	k2eAgNnSc7d1	zajímavé
zbožím	zboží	k1gNnSc7	zboží
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
je	být	k5eAaImIp3nS	být
rozmístěno	rozmístěn	k2eAgNnSc1d1	rozmístěno
na	na	k7c4	na
Covered	Covered	k1gInSc4	Covered
Market	market	k1gInSc1	market
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letním	letní	k2eAgNnSc6d1	letní
období	období	k1gNnSc6	období
jsou	být	k5eAaImIp3nP	být
populární	populární	k2eAgFnPc1d1	populární
vyjížďky	vyjížďka	k1gFnPc1	vyjížďka
na	na	k7c6	na
pramicích	pramice	k1gFnPc6	pramice
po	po	k7c6	po
řekách	řeka	k1gFnPc6	řeka
Cherwell	Cherwella	k1gFnPc2	Cherwella
a	a	k8xC	a
Temži	Temže	k1gFnSc4	Temže
<g/>
.	.	kIx.	.
</s>
<s>
Oxfordská	oxfordský	k2eAgFnSc1d1	Oxfordská
katedrála	katedrála	k1gFnSc1	katedrála
Kostel	kostel	k1gInSc1	kostel
panny	panna	k1gFnSc2	panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
univerzitní	univerzitní	k2eAgFnSc2d1	univerzitní
kaple	kaple	k1gFnSc2	kaple
<g/>
)	)	kIx)	)
Martyrs	Martyrs	k1gInSc1	Martyrs
<g/>
'	'	kIx"	'
Memorial	Memorial	k1gInSc1	Memorial
St	St	kA	St
Bartholomew	Bartholomew	k1gMnPc2	Bartholomew
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Chapel	Chapel	k1gInSc1	Chapel
katedrála	katedrála	k1gFnSc1	katedrála
Christ	Christ	k1gMnSc1	Christ
Church	Church	k1gMnSc1	Church
University	universita	k1gFnSc2	universita
Church	Church	k1gMnSc1	Church
of	of	k?	of
St	St	kA	St
Mary	Mary	k1gFnSc2	Mary
the	the	k?	the
Virgin	Virgin	k2eAgMnSc1d1	Virgin
Bodleian	Bodleian	k1gMnSc1	Bodleian
Library	Librara	k1gFnSc2	Librara
Clarendon	Clarendon	k1gMnSc1	Clarendon
Building	Building	k1gInSc1	Building
Radcliffe	Radcliff	k1gInSc5	Radcliff
Camera	Camer	k1gMnSc2	Camer
Sheldonian	Sheldonian	k1gMnSc1	Sheldonian
Theatre	Theatr	k1gInSc5	Theatr
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
University	universita	k1gFnSc2	universita
Parks	Parks	k1gInSc1	Parks
University	universita	k1gFnSc2	universita
Botanic	Botanice	k1gFnPc2	Botanice
Garden	Gardna	k1gFnPc2	Gardna
Christ	Christ	k1gInSc1	Christ
Church	Church	k1gMnSc1	Church
Meadow	Meadow	k1gFnPc2	Meadow
Port	porta	k1gFnPc2	porta
Meadow	Meadow	k1gMnSc2	Meadow
Mesopotamia	Mesopotamium	k1gNnSc2	Mesopotamium
Angel	angel	k1gMnSc1	angel
&	&	k?	&
Greyhound	Greyhound	k1gMnSc1	Greyhound
Meadow	Meadow	k1gFnSc4	Meadow
Cutteslowe	Cutteslow	k1gFnSc2	Cutteslow
Park	park	k1gInSc4	park
Florence	Florenc	k1gFnSc2	Florenc
Park	park	k1gInSc1	park
South	South	k1gInSc1	South
Park	park	k1gInSc1	park
Warneford	Warneforda	k1gFnPc2	Warneforda
Meadow	Meadow	k1gMnSc1	Meadow
Harold	Harold	k1gMnSc1	Harold
I.	I.	kA	I.
(	(	kIx(	(
<g/>
*	*	kIx~	*
1015	[number]	k4	1015
<g/>
?	?	kIx.	?
</s>
<s>
-	-	kIx~	-
†	†	k?	†
1040	[number]	k4	1040
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
George	Georg	k1gMnSc2	Georg
Berkeley	Berkelea	k1gFnSc2	Berkelea
(	(	kIx(	(
<g/>
*	*	kIx~	*
1685	[number]	k4	1685
-	-	kIx~	-
†	†	k?	†
1753	[number]	k4	1753
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
irský	irský	k2eAgMnSc1d1	irský
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
teolog	teolog	k1gMnSc1	teolog
Otto	Otto	k1gMnSc1	Otto
Neurath	Neurath	k1gMnSc1	Neurath
(	(	kIx(	(
<g/>
*	*	kIx~	*
1882	[number]	k4	1882
-	-	kIx~	-
†	†	k?	†
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rakouský	rakouský	k2eAgMnSc1d1	rakouský
filosof	filosof	k1gMnSc1	filosof
Dorothy	Dorotha	k1gFnSc2	Dorotha
L.	L.	kA	L.
Sayersová	Sayersová	k1gFnSc1	Sayersová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1893	[number]	k4	1893
-	-	kIx~	-
†	†	k?	†
<g />
.	.	kIx.	.
</s>
<s>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anglická	anglický	k2eAgFnSc1d1	anglická
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
a	a	k8xC	a
překladatelka	překladatelka	k1gFnSc1	překladatelka
známá	známý	k2eAgFnSc1d1	známá
svými	svůj	k3xOyFgInPc7	svůj
detektivními	detektivní	k2eAgInPc7d1	detektivní
příběhy	příběh	k1gInPc7	příběh
Clive	Cliev	k1gFnSc2	Cliev
Staples	Staples	k1gInSc4	Staples
Lewis	Lewis	k1gFnSc2	Lewis
(	(	kIx(	(
<g/>
*	*	kIx~	*
1898	[number]	k4	1898
-	-	kIx~	-
†	†	k?	†
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
irský	irský	k2eAgMnSc1d1	irský
spisovatel	spisovatel	k1gMnSc1	spisovatel
fantasy	fantas	k1gInPc4	fantas
Edward	Edward	k1gMnSc1	Edward
Evan	Evan	k1gMnSc1	Evan
Evans-Pritchard	Evans-Pritchard	k1gMnSc1	Evans-Pritchard
(	(	kIx(	(
<g/>
*	*	kIx~	*
1902	[number]	k4	1902
-	-	kIx~	-
†	†	k?	†
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
sociální	sociální	k2eAgMnSc1d1	sociální
antropolog	antropolog	k1gMnSc1	antropolog
a	a	k8xC	a
religionista	religionista	k1gMnSc1	religionista
Nikolaas	Nikolaas	k1gMnSc1	Nikolaas
<g />
.	.	kIx.	.
</s>
<s>
Tinbergen	Tinbergen	k1gInSc1	Tinbergen
(	(	kIx(	(
<g/>
*	*	kIx~	*
1907	[number]	k4	1907
-	-	kIx~	-
†	†	k?	†
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
a	a	k8xC	a
britský	britský	k2eAgMnSc1d1	britský
biolog	biolog	k1gMnSc1	biolog
<g/>
,	,	kIx,	,
etolog	etolog	k1gMnSc1	etolog
a	a	k8xC	a
ornitolog	ornitolog	k1gMnSc1	ornitolog
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
John	John	k1gMnSc1	John
Langshaw	Langshaw	k1gMnSc1	Langshaw
Austin	Austin	k1gMnSc1	Austin
(	(	kIx(	(
<g/>
*	*	kIx~	*
1911	[number]	k4	1911
-	-	kIx~	-
†	†	k?	†
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
filosof	filosof	k1gMnSc1	filosof
Peter	Peter	k1gMnSc1	Peter
Frederick	Frederick	k1gMnSc1	Frederick
Strawson	Strawson	k1gMnSc1	Strawson
(	(	kIx(	(
<g/>
*	*	kIx~	*
1919	[number]	k4	1919
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
†	†	k?	†
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
analytický	analytický	k2eAgMnSc1d1	analytický
filosof	filosof	k1gMnSc1	filosof
Abdus	Abdus	k1gMnSc1	Abdus
Salam	Salam	k1gInSc1	Salam
(	(	kIx(	(
<g/>
*	*	kIx~	*
1926	[number]	k4	1926
-	-	kIx~	-
†	†	k?	†
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pákistánský	pákistánský	k2eAgMnSc1d1	pákistánský
teoretický	teoretický	k2eAgMnSc1d1	teoretický
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
astrofyzik	astrofyzik	k1gMnSc1	astrofyzik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
P.	P.	kA	P.
D.	D.	kA	D.
Jamesová	Jamesový	k2eAgFnSc1d1	Jamesová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
anglická	anglický	k2eAgFnSc1d1	anglická
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
hlavně	hlavně	k6eAd1	hlavně
detektivních	detektivní	k2eAgInPc2d1	detektivní
příběhů	příběh	k1gInPc2	příběh
Leszek	Leszka	k1gFnPc2	Leszka
Kołakowski	Kołakowsk	k1gFnSc2	Kołakowsk
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
*	*	kIx~	*
1927	[number]	k4	1927
-	-	kIx~	-
†	†	k?	†
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polsko-britský	polskoritský	k2eAgMnSc1d1	polsko-britský
filosof	filosof	k1gMnSc1	filosof
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
Laura	Laura	k1gFnSc1	Laura
Mulvey	Mulvea	k1gFnSc2	Mulvea
(	(	kIx(	(
<g/>
*	*	kIx~	*
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
britská	britský	k2eAgFnSc1d1	britská
režisérka	režisérka	k1gFnSc1	režisérka
<g/>
,	,	kIx,	,
scenáristka	scenáristka	k1gFnSc1	scenáristka
<g/>
,	,	kIx,	,
producentka	producentka	k1gFnSc1	producentka
<g/>
,	,	kIx,	,
filmová	filmový	k2eAgFnSc1d1	filmová
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc1d1	kulturní
teoretička	teoretička	k1gFnSc1	teoretička
Stephen	Stephen	k1gInSc4	Stephen
Hawking	Hawking	k1gInSc1	Hawking
(	(	kIx(	(
<g/>
*	*	kIx~	*
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
teoretický	teoretický	k2eAgMnSc1d1	teoretický
fyzik	fyzik	k1gMnSc1	fyzik
<g />
.	.	kIx.	.
</s>
<s>
Jacqueline	Jacquelin	k1gInSc5	Jacquelin
du	du	k?	du
Pré	pré	k1gNnPc4	pré
(	(	kIx(	(
<g/>
*	*	kIx~	*
1945	[number]	k4	1945
-	-	kIx~	-
†	†	k?	†
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
britská	britský	k2eAgFnSc1d1	britská
violončelistka	violončelistka	k1gFnSc1	violončelistka
Humphrey	Humphrea	k1gFnSc2	Humphrea
Carpenter	Carpentra	k1gFnPc2	Carpentra
(	(	kIx(	(
<g/>
*	*	kIx~	*
1946	[number]	k4	1946
-	-	kIx~	-
†	†	k?	†
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
životopisec	životopisec	k1gMnSc1	životopisec
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
rozhlasový	rozhlasový	k2eAgMnSc1d1	rozhlasový
hlasatel	hlasatel	k1gMnSc1	hlasatel
Hugh	Hugh	k1gMnSc1	Hugh
Laurie	Laurie	k1gFnSc1	Laurie
(	(	kIx(	(
<g/>
*	*	kIx~	*
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
britský	britský	k2eAgMnSc1d1	britský
herec	herec	k1gMnSc1	herec
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Jayne	Jayn	k1gInSc5	Jayn
Soliman	Soliman	k1gMnSc1	Soliman
(	(	kIx(	(
<g/>
*	*	kIx~	*
1968	[number]	k4	1968
-	-	kIx~	-
†	†	k?	†
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
britská	britský	k2eAgFnSc1d1	britská
krasobruslařka	krasobruslařka	k1gFnSc1	krasobruslařka
a	a	k8xC	a
trenérka	trenérka	k1gFnSc1	trenérka
Tim	Tim	k?	Tim
Henman	Henman	k1gMnSc1	Henman
(	(	kIx(	(
<g/>
*	*	kIx~	*
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
britský	britský	k2eAgMnSc1d1	britský
tenista	tenista	k1gMnSc1	tenista
Emma	Emma	k1gFnSc1	Emma
Watsonová	Watsonová	k1gFnSc1	Watsonová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
britská	britský	k2eAgFnSc1d1	britská
herečka	herečka	k1gFnSc1	herečka
Madeleine	Madeleine	k1gFnSc1	Madeleine
Wickham	Wickham	k1gInSc1	Wickham
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
*	*	kIx~	*
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
britská	britský	k2eAgFnSc1d1	britská
novinářka	novinářka	k1gFnSc1	novinářka
a	a	k8xC	a
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Bonn	Bonn	k1gInSc1	Bonn
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Grenoble	Grenoble	k1gInSc1	Grenoble
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
Leiden	Leidna	k1gFnPc2	Leidna
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
León	Leóna	k1gFnPc2	Leóna
<g/>
,	,	kIx,	,
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
Perm	perm	k1gInSc1	perm
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
Španělsko	Španělsko	k1gNnSc1	Španělsko
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Oxford	Oxford	k1gInSc1	Oxford
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Dílo	dílo	k1gNnSc1	dílo
Anglické	anglický	k2eAgInPc1d1	anglický
listy	list	k1gInPc1	list
<g/>
/	/	kIx~	/
<g/>
Cambridge	Cambridge	k1gFnSc1	Cambridge
a	a	k8xC	a
Oxford	Oxford	k1gInSc1	Oxford
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
Slovníkové	slovníkový	k2eAgFnSc2d1	slovníková
heslo	heslo	k1gNnSc4	heslo
Oxford	Oxford	k1gInSc1	Oxford
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Stránky	stránka	k1gFnSc2	stránka
Rady	rada	k1gFnSc2	rada
města	město	k1gNnSc2	město
Oxfordu	Oxford	k1gInSc2	Oxford
Průvodce	průvodce	k1gMnSc1	průvodce
městem	město	k1gNnSc7	město
Vitruální	Vitruální	k2eAgFnSc2d1	Vitruální
průvodce	průvodce	k1gMnSc4	průvodce
Oxfordem	Oxford	k1gInSc7	Oxford
Oxfordská	oxfordský	k2eAgFnSc1d1	Oxfordská
univerzita	univerzita	k1gFnSc1	univerzita
Oxford	Oxford	k1gInSc4	Oxford
Brookes	Brookes	k1gInSc1	Brookes
University	universita	k1gFnSc2	universita
Podrobná	podrobný	k2eAgFnSc1d1	podrobná
mapa	mapa	k1gFnSc1	mapa
centra	centrum	k1gNnSc2	centrum
Oxfordu	Oxford	k1gInSc2	Oxford
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
</s>
