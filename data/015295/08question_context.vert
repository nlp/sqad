<s>
Izraelská	izraelský	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
:	:	kIx,
ש	ש	k?
י	י	k?
<g/>
,	,	kIx,
Švil	Švil	k1gMnSc1
Jisra	Jisra	k1gMnSc1
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
turistická	turistický	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
<g/>
,	,	kIx,
vedoucí	vedoucí	k1gFnSc1
po	po	k7c6
celém	celý	k2eAgInSc6d1
Izraeli	Izrael	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
začíná	začínat	k5eAaImIp3nS
u	u	k7c2
města	město	k1gNnSc2
Dan	Dana	k1gFnPc2
<g/>
,	,	kIx,
při	při	k7c6
libanonských	libanonský	k2eAgFnPc6d1
hranicích	hranice	k1gFnPc6
a	a	k8xC
končí	končit	k5eAaImIp3nS
u	u	k7c2
města	město	k1gNnSc2
Ejlat	Ejlat	k1gInSc1
u	u	k7c2
Rudého	rudý	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>