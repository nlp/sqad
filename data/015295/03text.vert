<s>
Izraelská	izraelský	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
</s>
<s>
Izraelská	izraelský	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
</s>
<s>
Typická	typický	k2eAgFnSc1d1
turistická	turistický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Izraelské	izraelský	k2eAgFnSc2d1
stezky	stezka	k1gFnSc2
</s>
<s>
Izraelská	izraelský	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
:	:	kIx,
ש	ש	k?
י	י	k?
<g/>
,	,	kIx,
Švil	Švil	k1gMnSc1
Jisra	Jisra	k1gMnSc1
<g/>
'	'	kIx"
<g/>
el	ela	k1gFnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
turistická	turistický	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
<g/>
,	,	kIx,
vedoucí	vedoucí	k1gFnSc1
po	po	k7c6
celém	celý	k2eAgInSc6d1
Izraeli	Izrael	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
začíná	začínat	k5eAaImIp3nS
u	u	k7c2
města	město	k1gNnSc2
Dan	Dana	k1gFnPc2
<g/>
,	,	kIx,
při	při	k7c6
libanonských	libanonský	k2eAgFnPc6d1
hranicích	hranice	k1gFnPc6
a	a	k8xC
končí	končit	k5eAaImIp3nS
u	u	k7c2
města	město	k1gNnSc2
Ejlat	Ejlat	k1gInSc1
u	u	k7c2
Rudého	rudý	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celková	celkový	k2eAgFnSc1d1
délka	délka	k1gFnSc1
je	být	k5eAaImIp3nS
940	#num#	k4
km	km	kA
a	a	k8xC
ujít	ujít	k5eAaPmF
tuto	tento	k3xDgFnSc4
trasu	trasa	k1gFnSc4
souvisle	souvisle	k6eAd1
trvá	trvat	k5eAaImIp3nS
30	#num#	k4
<g/>
–	–	k?
<g/>
70	#num#	k4
dní	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Izraelská	izraelský	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
je	být	k5eAaImIp3nS
označována	označovat	k5eAaImNgFnS
třemi	tři	k4xCgInPc7
pruhy	pruh	k1gInPc7
(	(	kIx(
<g/>
bílým	bílý	k2eAgInSc7d1
<g/>
,	,	kIx,
modrým	modrý	k2eAgInSc7d1
a	a	k8xC
oranžovým	oranžový	k2eAgInSc7d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
malovány	malovat	k5eAaImNgFnP
na	na	k7c4
kameny	kámen	k1gInPc4
či	či	k8xC
jiné	jiný	k2eAgInPc4d1
objekty	objekt	k1gInPc4
podél	podél	k7c2
cesty	cesta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
neexistuje	existovat	k5eNaImIp3nS
v	v	k7c6
angličtině	angličtina	k1gFnSc6
mnoho	mnoho	k6eAd1
průvodců	průvodce	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
tuto	tento	k3xDgFnSc4
trasu	trasa	k1gFnSc4
zmiňují	zmiňovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
však	však	k9
snaží	snažit	k5eAaImIp3nS
napravit	napravit	k5eAaPmF
oficiální	oficiální	k2eAgFnPc4d1
internetové	internetový	k2eAgFnPc4d1
stránky	stránka	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
poskytují	poskytovat	k5eAaImIp3nP
užitečné	užitečný	k2eAgFnPc4d1
informace	informace	k1gFnPc4
v	v	k7c6
angličtině	angličtina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Části	část	k1gFnPc1
stezky	stezka	k1gFnPc1
</s>
<s>
Pro	pro	k7c4
ty	ten	k3xDgMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
by	by	kYmCp3nP
si	se	k3xPyFc3
chtěli	chtít	k5eAaImAgMnP
projít	projít	k5eAaPmF
jen	jen	k9
část	část	k1gFnSc4
stezky	stezka	k1gFnSc2
připravily	připravit	k5eAaPmAgFnP
internetové	internetový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Izraelské	izraelský	k2eAgFnPc1d1
stezky	stezka	k1gFnSc2
rozdělení	rozdělení	k1gNnSc2
celé	celý	k2eAgNnSc1d1
trasy	trasa	k1gFnPc4
do	do	k7c2
dvanácti	dvanáct	k4xCc2
menších	malý	k2eAgFnPc2d2
částí	část	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
hory	hora	k1gFnPc1
Naftali	Naftali	k1gFnSc2
a	a	k8xC
útesy	útes	k1gInPc4
Reches	Rechesa	k1gFnPc2
Ramim	Ramima	k1gFnPc2
(	(	kIx(
<g/>
Horní	horní	k2eAgFnSc1d1
Galilea	Galilea	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
potok	potok	k1gInSc1
Nachal	Nachal	k1gMnSc1
Kedeš	Kedeš	k1gMnSc1
a	a	k8xC
pevnost	pevnost	k1gFnSc1
Ješa	Ješa	k1gFnSc1
(	(	kIx(
<g/>
neboli	neboli	k8xC
Mecudat	Mecudat	k1gMnSc1
Koach	Koach	k1gMnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
Horní	horní	k2eAgFnSc1d1
Galilea	Galilea	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
potok	potok	k1gInSc1
Merom	Merom	k1gInSc1
<g/>
,	,	kIx,
Ein	Ein	k1gMnSc1
Ceved	Ceved	k1gMnSc1
a	a	k8xC
ruiny	ruina	k1gFnPc1
Šema	Šem	k1gInSc2
(	(	kIx(
<g/>
Horní	horní	k2eAgFnSc1d1
Galilea	Galilea	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
hora	hora	k1gFnSc1
Tábor	Tábor	k1gInSc1
(	(	kIx(
<g/>
Dolní	dolní	k2eAgFnSc1d1
Galilea	Galilea	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Nachal	Nachal	k1gMnSc1
Cipori	Cipor	k1gFnSc2
(	(	kIx(
<g/>
Dolní	dolní	k2eAgFnSc1d1
Galilea	Galilea	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Nachal	Nachal	k1gMnSc1
Ma	Ma	k1gMnSc1
<g/>
'	'	kIx"
<g/>
apilim	apilim	k1gMnSc1
/	/	kIx~
potok	potok	k1gInSc1
Nachaš	Nachaš	k1gInSc1
(	(	kIx(
<g/>
Karmel	Karmel	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Šajarotské	Šajarotský	k2eAgNnSc1d1
pohoří	pohoří	k1gNnSc1
(	(	kIx(
<g/>
Judské	judský	k2eAgFnPc1d1
hory	hora	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Jatirské	Jatirský	k2eAgFnPc1d1
ruiny	ruina	k1gFnPc1
a	a	k8xC
lom	lom	k1gInSc1
Dragot	Dragota	k1gFnPc2
</s>
<s>
Mamšit	Mamšit	k5eAaPmF,k5eAaImF
a	a	k8xC
Mamšitský	Mamšitský	k2eAgInSc1d1
potok	potok	k1gInSc1
(	(	kIx(
<g/>
Negevská	Negevský	k2eAgFnSc1d1
poušť	poušť	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Micpe	Micpat	k5eAaPmIp3nS
Ramon	Ramona	k1gFnPc2
a	a	k8xC
Machteš	Machteš	k1gFnPc2
Ramon	Ramona	k1gFnPc2
(	(	kIx(
<g/>
Negevská	Negevský	k2eAgFnSc1d1
poušť	poušť	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kisujský	Kisujský	k2eAgInSc1d1
potok	potok	k1gInSc1
a	a	k8xC
údolí	údolí	k1gNnSc2
Ovda	Ovd	k1gInSc2
(	(	kIx(
<g/>
Negevská	Negevský	k2eAgFnSc1d1
poušť	poušť	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Šchoretský	Šchoretský	k2eAgInSc1d1
potok	potok	k1gInSc1
(	(	kIx(
<g/>
Ejlatské	Ejlatský	k2eAgFnPc1d1
hory	hora	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Israel	Israel	k1gMnSc1
National	National	k1gMnSc1
Trail	Trail	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Ježíšova	Ježíšův	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Izraelská	izraelský	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
internetové	internetový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Izraelska	Izraelska	k1gFnSc1
stezka	stezka	k1gFnSc1
Archivováno	archivován	k2eAgNnSc4d1
20	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
-	-	kIx~
Trasa	trasa	k1gFnSc1
<g/>
,	,	kIx,
mapy	mapa	k1gFnPc1
a	a	k8xC
příběhy	příběh	k1gInPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Izrael	Izrael	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
7842878-6	7842878-6	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
233965370	#num#	k4
</s>
