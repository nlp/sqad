<s>
Torbjø	Torbjø	k1gNnSc1
Rø	Rø	k1gFnPc2
</s>
<s>
Torbjø	Torbjø	k1gNnSc1
Rø	Rø	k1gFnPc2
Narození	narození	k1gNnSc2
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1970	#num#	k4
(	(	kIx(
<g/>
51	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Povolání	povolání	k1gNnPc2
</s>
<s>
fotograf	fotograf	k1gMnSc1
Webová	webový	k2eAgNnPc5d1
stránka	stránka	k1gFnSc1
</s>
<s>
www.rodland.net	www.rodland.net	k5eAaImF,k5eAaPmF,k5eAaBmF
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Torbjø	Torbjø	k1gInSc1
Rø	Rø	k1gInSc1
(	(	kIx(
<g/>
*	*	kIx~
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1970	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
norský	norský	k2eAgMnSc1d1
fotografický	fotografický	k2eAgMnSc1d1
umělec	umělec	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInPc1
obrazy	obraz	k1gInPc1
jsou	být	k5eAaImIp3nP
nasyceny	nasytit	k5eAaPmNgInP
symbolismem	symbolismus	k1gInSc7
<g/>
,	,	kIx,
lyrikou	lyrika	k1gFnSc7
a	a	k8xC
erotikou	erotika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
samostatná	samostatný	k2eAgFnSc1d1
výstava	výstava	k1gFnSc1
v	v	k7c6
Serpentine	serpentin	k1gInSc5
Gallery	Galler	k1gMnPc7
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
byla	být	k5eAaImAgFnS
nazvána	nazván	k2eAgFnSc1d1
The	The	k1gFnSc1
Touch	Toucha	k1gFnPc2
That	Thata	k1gFnPc2
Made	Mad	k1gMnSc2
You	You	k1gMnSc2
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
institucí	instituce	k1gFnSc7
věnovanou	věnovaný	k2eAgFnSc7d1
současnému	současný	k2eAgNnSc3d1
umění	umění	k1gNnSc3
a	a	k8xC
kultuře	kultura	k1gFnSc3
Fondazione	Fondazion	k1gInSc5
Prada	Prad	k1gMnSc4
odcestoval	odcestovat	k5eAaPmAgMnS
do	do	k7c2
Milána	Milán	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gFnPc1
práce	práce	k1gFnPc1
byly	být	k5eAaImAgFnP
uvedeny	uvést	k5eAaPmNgFnP
na	na	k7c6
benátském	benátský	k2eAgNnSc6d1
bienále	bienále	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Raná	raný	k2eAgFnSc1d1
retrospektiva	retrospektiva	k1gFnSc1
se	se	k3xPyFc4
uskutečnila	uskutečnit	k5eAaPmAgFnS
v	v	k7c6
Muzeu	muzeum	k1gNnSc6
moderního	moderní	k2eAgNnSc2d1
umění	umění	k1gNnSc2
Astrupa	Astrup	k1gMnSc2
Fearnleyho	Fearnley	k1gMnSc2
v	v	k7c6
Oslu	Oslo	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezi	mezi	k7c7
veřejnými	veřejný	k2eAgFnPc7d1
sbírkami	sbírka	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
vlastní	vlastnit	k5eAaImIp3nP
některé	některý	k3yIgFnPc1
jeho	jeho	k3xOp3gFnPc1
práce	práce	k1gFnPc1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
Muzeum	muzeum	k1gNnSc4
amerického	americký	k2eAgNnSc2d1
umění	umění	k1gNnSc2
Whitneyové	Whitneyová	k1gFnSc2
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
Muzeum	muzeum	k1gNnSc1
moderního	moderní	k2eAgNnSc2d1
umění	umění	k1gNnSc2
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
Národní	národní	k2eAgInSc1d1
fond	fond	k1gInSc1
současného	současný	k2eAgNnSc2d1
umění	umění	k1gNnSc2
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
,	,	kIx,
Stedelijk	Stedelijk	k1gInSc4
Museum	museum	k1gNnSc4
v	v	k7c6
Amsterdamu	Amsterdam	k1gInSc6
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Muzeum	muzeum	k1gNnSc1
současného	současný	k2eAgNnSc2d1
umění	umění	k1gNnSc2
v	v	k7c6
Chicagu	Chicago	k1gNnSc6
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Moderna	Moderna	k1gFnSc1
Museet	Museeta	k1gFnPc2
ve	v	k7c6
Stockholmu	Stockholm	k1gInSc6
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pozadí	pozadí	k1gNnSc1
a	a	k8xC
vzdělání	vzdělání	k1gNnSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1970	#num#	k4
ve	v	k7c6
Stavangeru	Stavanger	k1gInSc6
(	(	kIx(
<g/>
Norsko	Norsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
teenager	teenager	k1gMnSc1
pracoval	pracovat	k5eAaImAgMnS
na	na	k7c6
volné	volný	k2eAgFnSc6d1
noze	noha	k1gFnSc6
pro	pro	k7c4
několik	několik	k4yIc4
norských	norský	k2eAgFnPc2d1
novin	novina	k1gFnPc2
jako	jako	k8xC,k8xS
redakční	redakční	k2eAgMnSc1d1
karikaturista	karikaturista	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Vystudoval	vystudovat	k5eAaPmAgMnS
fotografii	fotografia	k1gFnSc4
na	na	k7c4
National	National	k1gFnSc4
College	Colleg	k1gInSc2
of	of	k?
Art	Art	k1gMnSc1
and	and	k?
Design	design	k1gInSc1
v	v	k7c6
Bergenu	Bergen	k1gInSc6
(	(	kIx(
<g/>
Norsko	Norsko	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
kulturní	kulturní	k2eAgNnPc1d1
studia	studio	k1gNnPc1
na	na	k7c6
Univerzitě	univerzita	k1gFnSc6
ve	v	k7c6
Stavangeru	Stavanger	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Teprve	teprve	k6eAd1
v	v	k7c6
devadesátých	devadesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
vznikla	vzniknout	k5eAaPmAgFnS
první	první	k4xOgFnPc4
instituce	instituce	k1gFnPc4
pro	pro	k7c4
vyšší	vysoký	k2eAgNnSc4d2
fotografické	fotografický	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
–	–	k?
Národní	národní	k2eAgFnSc1d1
umělecká	umělecký	k2eAgFnSc1d1
akademie	akademie	k1gFnSc1
v	v	k7c6
Bergenu	Bergen	k1gInSc6
(	(	kIx(
<g/>
KHiB	KHiB	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umělci	umělec	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
zásadně	zásadně	k6eAd1
formují	formovat	k5eAaImIp3nP
dnešní	dnešní	k2eAgFnSc4d1
norskou	norský	k2eAgFnSc4d1
fotografickou	fotografický	k2eAgFnSc4d1
scénu	scéna	k1gFnSc4
jako	jako	k8xS,k8xC
Mikkel	Mikkel	k1gInSc4
McAlinden	McAlindna	k1gFnPc2
<g/>
,	,	kIx,
Vibeke	Vibek	k1gInPc1
Tandberg	Tandberg	k1gInSc1
nebo	nebo	k8xC
Torbjø	Torbjø	k1gInSc1
Rø	Rø	k1gFnPc2
zde	zde	k6eAd1
absolvovali	absolvovat	k5eAaPmAgMnP
v	v	k7c6
polovině	polovina	k1gFnSc6
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Rø	Rø	k1gInSc1
žije	žít	k5eAaImIp3nS
a	a	k8xC
pracuje	pracovat	k5eAaImIp3nS
v	v	k7c4
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Rø	Rø	k1gInSc1
pořizuje	pořizovat	k5eAaImIp3nS
své	svůj	k3xOyFgFnPc4
fotografie	fotografia	k1gFnPc4
na	na	k7c4
klasický	klasický	k2eAgInSc4d1
fotografický	fotografický	k2eAgInSc4d1
film	film	k1gInSc4
a	a	k8xC
zvětšeniny	zvětšenina	k1gFnSc2
zpracovává	zpracovávat	k5eAaImIp3nS
chemickou	chemický	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historička	historička	k1gFnSc1
umění	umění	k1gNnSc2
a	a	k8xC
kritička	kritička	k1gFnSc1
Ina	Ina	k1gFnSc2
Blom	Bloma	k1gFnPc2
v	v	k7c6
Artforu	Artfor	k1gInSc6
filozofovala	filozofovat	k5eAaImAgFnS
o	o	k7c6
kulturních	kulturní	k2eAgInPc6d1
důsledcích	důsledek	k1gInPc6
jeho	jeho	k3xOp3gNnSc4
děl	dít	k5eAaImAgInS,k5eAaBmAgInS
<g/>
:	:	kIx,
„	„	k?
<g/>
Zvláštnost	zvláštnost	k1gFnSc1
těchto	tento	k3xDgInPc2
obrazů	obraz	k1gInPc2
mohla	moct	k5eAaImAgFnS
vyvolat	vyvolat	k5eAaPmF
další	další	k2eAgFnSc1d1
intuice	intuice	k1gFnSc1
–	–	k?
možná	možná	k9
související	související	k2eAgFnSc1d1
s	s	k7c7
otázkou	otázka	k1gFnSc7
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
přesně	přesně	k6eAd1
znamená	znamenat	k5eAaImIp3nS
být	být	k5eAaImF
fotografickým	fotografický	k2eAgInSc7d1
obrazem	obraz	k1gInSc7
uprostřed	uprostřed	k7c2
skutečného	skutečný	k2eAgInSc2d1
souhrnu	souhrn	k1gInSc2
nových	nový	k2eAgFnPc2d1
mediálních	mediální	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
<g/>
,	,	kIx,
prostředí	prostředí	k1gNnSc2
a	a	k8xC
objektů	objekt	k1gInPc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Rø	Rø	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
chtěl	chtít	k5eAaImAgMnS
posouvat	posouvat	k5eAaImF
umělecké	umělecký	k2eAgFnSc2d1
hranice	hranice	k1gFnSc2
svého	svůj	k3xOyFgNnSc2
média	médium	k1gNnSc2
<g/>
,	,	kIx,
rekonceptualizoval	rekonceptualizovat	k5eAaPmAgMnS,k5eAaImAgMnS,k5eAaBmAgMnS
a	a	k8xC
integroval	integrovat	k5eAaBmAgMnS
estetické	estetický	k2eAgFnPc4d1
kvality	kvalita	k1gFnPc4
odmítnuté	odmítnutý	k2eAgFnPc4d1
v	v	k7c6
postmoderním	postmoderní	k2eAgNnSc6d1
umění	umění	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rø	Rø	k2eAgFnSc1d1
fotografie	fotografie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
práce	práce	k1gFnSc2
The	The	k1gMnSc1
Pictures	Picturesa	k1gFnPc2
Generation	Generation	k1gInSc4
a	a	k8xC
Jeffa	Jeff	k1gMnSc2
Walla	Wall	k1gMnSc2
<g/>
,	,	kIx,
představuje	představovat	k5eAaImIp3nS
překvapivé	překvapivý	k2eAgNnSc1d1
přecenění	přecenění	k1gNnSc1
lyricismu	lyricismus	k1gInSc2
a	a	k8xC
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
nazývá	nazývat	k5eAaImIp3nS
smyslností	smyslnost	k1gFnSc7
fotografického	fotografický	k2eAgInSc2d1
okamžiku	okamžik	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Rø	Rø	k1gInSc1
<g/>
,	,	kIx,
původně	původně	k6eAd1
známý	známý	k2eAgMnSc1d1
svými	svůj	k3xOyFgInPc7
portréty	portrét	k1gInPc7
v	v	k7c6
severských	severský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
,	,	kIx,
překonal	překonat	k5eAaPmAgMnS
<g />
.	.	kIx.
</s>
<s hack="1">
tuto	tento	k3xDgFnSc4
potenciální	potenciální	k2eAgFnSc4d1
trofej	trofej	k1gFnSc4
důsledným	důsledný	k2eAgNnSc7d1
nalézáním	nalézání	k1gNnSc7
nových	nový	k2eAgFnPc2d1
nástrah	nástraha	k1gFnPc2
pro	pro	k7c4
diváky	divák	k1gMnPc4
svých	svůj	k3xOyFgFnPc2
fotografií	fotografia	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Příkladem	příklad	k1gInSc7
těchto	tento	k3xDgFnPc2
návnad	návnada	k1gFnPc2
je	být	k5eAaImIp3nS
subtilní	subtilní	k2eAgNnSc4d1
soužití	soužití	k1gNnSc4
přehánění	přehánění	k1gNnSc1
s	s	k7c7
normálností	normálnost	k1gFnSc7
jeho	jeho	k3xOp3gFnPc2
postav	postava	k1gFnPc2
<g/>
;	;	kIx,
jak	jak	k8xC,k8xS
viděl	vidět	k5eAaImAgMnS
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
fotografii	fotografia	k1gFnSc4
ženské	ženský	k2eAgFnSc2d1
ruky	ruka	k1gFnSc2
s	s	k7c7
chobotnicí	chobotnice	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vylézá	vylézat	k5eAaImIp3nS
z	z	k7c2
rukávu	rukáv	k1gInSc2
a	a	k8xC
ovinuje	ovinovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g />
.	.	kIx.
</s>
<s hack="1">
kolem	kolem	k7c2
prstů	prst	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Charakteristický	charakteristický	k2eAgMnSc1d1
v	v	k7c6
Rø	Rø	k2eAgFnSc6d1
práci	práce	k1gFnSc6
je	být	k5eAaImIp3nS
také	také	k9
subtilní	subtilní	k2eAgInSc1d1
symbol	symbol	k1gInSc1
nonduality	nondualita	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gFnSc1
věcnost	věcnost	k1gFnSc1
<g/>
,	,	kIx,
dokonce	dokonce	k9
i	i	k9
ve	v	k7c6
stylizovaných	stylizovaný	k2eAgInPc6d1
obrazech	obraz	k1gInPc6
a	a	k8xC
vícenásobných	vícenásobný	k2eAgFnPc6d1
expozicích	expozice	k1gFnPc6
<g/>
,	,	kIx,
umožňuje	umožňovat	k5eAaImIp3nS
Rø	Rø	k1gMnSc3
překročit	překročit	k5eAaPmF
jak	jak	k6eAd1
běžnou	běžný	k2eAgFnSc4d1
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
světskou	světský	k2eAgFnSc4d1
rovinu	rovina	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
Rø	Rø	k1gNnSc2
„	„	k?
<g/>
bojem	boj	k1gInSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgInS
obraz	obraz	k1gInSc4
aktivní	aktivní	k2eAgInSc4d1
a	a	k8xC
relativní	relativní	k2eAgInSc4d1
<g/>
,	,	kIx,
jasný	jasný	k2eAgInSc4d1
<g/>
,	,	kIx,
ale	ale	k8xC
složitý	složitý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
naše	náš	k3xOp1gFnSc1
nová	nový	k2eAgFnSc1d1
realita	realita	k1gFnSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
vrstvená	vrstvený	k2eAgFnSc1d1
a	a	k8xC
otevřená	otevřený	k2eAgFnSc1d1
paranoidní	paranoidní	k2eAgFnSc3d1
interpretaci	interpretace	k1gFnSc3
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Nebo	nebo	k8xC
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
to	ten	k3xDgNnSc4
kurátor	kurátor	k1gMnSc1
Bennett	Bennett	k1gMnSc1
Simpson	Simpson	k1gMnSc1
uvedl	uvést	k5eAaPmAgMnS
v	v	k7c6
eseji	esej	k1gFnSc6
o	o	k7c6
Rø	Rø	k1gMnSc6
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yIgNnPc1,k3yQgNnPc1
byla	být	k5eAaImAgNnP
zveřejněna	zveřejnit	k5eAaPmNgNnP
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
<g/>
:	:	kIx,
„	„	k?
<g/>
Jeho	jeho	k3xOp3gInPc4
obrazy	obraz	k1gInPc4
jsou	být	k5eAaImIp3nP
spojité	spojitý	k2eAgInPc1d1
<g/>
;	;	kIx,
fungují	fungovat	k5eAaImIp3nP
pod	pod	k7c7
zábleskem	záblesk	k1gInSc7
pochybností	pochybnost	k1gFnPc2
<g/>
,	,	kIx,
ovlivněné	ovlivněný	k2eAgFnPc4d1
touhy	touha	k1gFnPc4
<g/>
,	,	kIx,
možnosti	možnost	k1gFnPc4
nemožného	možný	k2eNgInSc2d1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
2004	#num#	k4
až	až	k9
2007	#num#	k4
produkoval	produkovat	k5eAaImAgInS
Rø	Rø	k1gInSc1
šest	šest	k4xCc1
videosnímků	videosnímek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgMnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
<g/>
,	,	kIx,
nazvaný	nazvaný	k2eAgInSc4d1
132	#num#	k4
BPM	BPM	kA
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
vystaven	vystavit	k5eAaPmNgInS
samostatně	samostatně	k6eAd1
v	v	k7c6
MoMA	MoMA	k1gFnSc6
PS1	PS1	k1gMnSc1
(	(	kIx(
<g/>
Long	Long	k1gMnSc1
Island	Island	k1gInSc4
City	City	k1gFnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
v	v	k7c6
Muzeu	muzeum	k1gNnSc6
současného	současný	k2eAgNnSc2d1
umění	umění	k1gNnSc2
v	v	k7c6
Hirošimě	Hirošima	k1gFnSc6
(	(	kIx(
<g/>
Hirošima	Hirošima	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
Rø	Rø	k1gInSc1
vytvořil	vytvořit	k5eAaPmAgInS
nové	nový	k2eAgNnSc4d1
video	video	k1gNnSc4
s	s	k7c7
názvem	název	k1gInSc7
Between	Between	k2eAgMnSc1d1
Fork	Fork	k1gMnSc1
and	and	k?
Ladder	Ladder	k1gMnSc1
(	(	kIx(
<g/>
Mezi	mezi	k7c7
vidličkou	vidlička	k1gFnSc7
a	a	k8xC
žebříkem	žebřík	k1gInSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
objevilo	objevit	k5eAaPmAgNnS
na	na	k7c6
jeho	jeho	k3xOp3gFnSc6
samostatné	samostatný	k2eAgFnSc6d1
výstavě	výstava	k1gFnSc6
Fifth	Fiftha	k1gFnPc2
Honeymoon	Honeymoona	k1gFnPc2
(	(	kIx(
<g/>
Páté	pátý	k4xOgFnPc1
líbánky	líbánky	k1gFnPc1
<g/>
)	)	kIx)
v	v	k7c6
Bergenské	Bergenský	k2eAgFnSc6d1
Kunsthalle	Kunsthalla	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Samostatná	samostatný	k2eAgFnSc1d1
výstava	výstava	k1gFnSc1
Páté	pátá	k1gFnSc2
líbánky	líbánky	k1gFnPc4
byla	být	k5eAaImAgNnP
znovu	znovu	k6eAd1
představena	představit	k5eAaPmNgNnP
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
v	v	k7c4
Bonniers	Bonniers	k1gInSc4
Konsthall	Konsthall	k1gInSc1
(	(	kIx(
<g/>
Švédsko	Švédsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
i	i	k8xC
v	v	k7c6
Muzeu	muzeum	k1gNnSc6
současného	současný	k2eAgNnSc2d1
umění	umění	k1gNnSc2
v	v	k7c6
Kiasmě	Kiasma	k1gFnSc6
(	(	kIx(
<g/>
Helsinky	Helsinky	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Knihy	kniha	k1gFnPc1
</s>
<s>
Fifth	Fifth	k1gMnSc1
Honeymoon	Honeymoon	k1gMnSc1
<g/>
,	,	kIx,
Sternberg	Sternberg	k1gMnSc1
Press	Press	k1gInSc1
/	/	kIx~
Bergen	Bergen	k1gMnSc1
Kunsthall	Kunsthall	k1gMnSc1
/	/	kIx~
Bonniers	Bonniers	k1gInSc1
Konsthall	Konsthall	k1gInSc1
/	/	kIx~
Kiasma	Kiasma	k1gFnSc1
Museum	museum	k1gNnSc1
of	of	k?
Contemporary	Contemporara	k1gFnSc2
Art	Art	k1gFnPc2
<g/>
:	:	kIx,
Berlin	berlina	k1gFnPc2
/	/	kIx~
Bergen	Bergen	k1gInSc1
/	/	kIx~
Stockholm	Stockholm	k1gInSc1
/	/	kIx~
Helsinki	Helsinki	k1gNnSc1
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9783956794124	#num#	k4
</s>
<s>
The	The	k?
Touch	Touch	k1gInSc1
That	That	k1gInSc1
Made	Mad	k1gMnSc2
You	You	k1gMnSc2
<g/>
,	,	kIx,
Serpentine	serpentin	k1gInSc5
Galleries	Galleries	k1gInSc1
/	/	kIx~
Koenig	Koenig	k1gInSc1
Books	Books	k1gInSc1
<g/>
:	:	kIx,
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781908617484ISBN	9781908617484ISBN	k4
9783960982371	#num#	k4
</s>
<s>
The	The	k?
Model	model	k1gInSc1
<g/>
,	,	kIx,
MACK	MACK	kA
<g/>
:	:	kIx,
London	London	k1gMnSc1
<g/>
,	,	kIx,
říjen	říjen	k1gInSc1
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781910164945	#num#	k4
</s>
<s>
Confabulations	Confabulations	k1gInSc1
<g/>
,	,	kIx,
MACK	MACK	kA
<g/>
:	:	kIx,
London	London	k1gMnSc1
<g/>
,	,	kIx,
červen	červen	k1gInSc1
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781910164631	#num#	k4
</s>
<s>
Sasquatch	Sasquatch	k1gInSc1
Century	Centura	k1gFnSc2
<g/>
,	,	kIx,
Mousse	Mousse	k1gFnSc2
Publisher	Publishra	k1gFnPc2
/	/	kIx~
Henie	Henie	k1gFnSc1
Onstad	Onstad	k1gInSc1
Art	Art	k1gFnSc4
Center	centrum	k1gNnPc2
<g/>
:	:	kIx,
Milano	Milana	k1gFnSc5
/	/	kIx~
Oslo	Oslo	k1gNnSc1
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9788867491308	#num#	k4
</s>
<s>
Vanilla	Vanilla	k1gMnSc1
Partner	partner	k1gMnSc1
<g/>
,	,	kIx,
MACK	MACK	kA
<g/>
:	:	kIx,
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
říjen	říjen	k1gInSc1
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781907946318	#num#	k4
</s>
<s>
Andy	Anda	k1gFnPc1
Capp	Capp	k1gInSc1
Variations	Variations	k1gInSc1
<g/>
,	,	kIx,
Hassla	Hassla	k1gFnSc1
<g/>
:	:	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780982547168	#num#	k4
</s>
<s>
A	a	k9
day	day	k?
in	in	k?
the	the	k?
life	life	k1gInSc1
of	of	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Libraryman	Libraryman	k1gMnSc1
<g/>
:	:	kIx,
Stockholm	Stockholm	k1gInSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9789186269029	#num#	k4
</s>
<s>
I	i	k8xC
Want	Want	k1gMnSc1
to	ten	k3xDgNnSc4
Live	Live	k1gNnSc4
Innocent	Innocent	k1gMnSc1
<g/>
,	,	kIx,
SteidlMACK	SteidlMACK	k1gMnSc1
<g/>
:	:	kIx,
Göttingen	Göttingen	k1gInSc1
<g/>
,	,	kIx,
únor	únor	k1gInSc1
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9783865216175	#num#	k4
</s>
<s>
White	White	k5eAaPmIp2nP
Planet	planeta	k1gFnPc2
<g/>
,	,	kIx,
Black	Blacka	k1gFnPc2
Heart	Hearta	k1gFnPc2
<g/>
,	,	kIx,
SteidlMACK	SteidlMACK	k1gFnSc1
<g/>
:	:	kIx,
Göttingen	Göttingen	k1gInSc1
<g/>
,	,	kIx,
červen	červen	k1gInSc1
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
3865212220	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Torbjø	Torbjø	k1gFnPc2
Rø	Rø	k1gNnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Torbjorn	Torbjorn	k1gInSc1
Rodland	Rodland	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Puzzling	Puzzling	k1gInSc1
Photos	Photosa	k1gFnPc2
Are	ar	k1gInSc5
Unsettling	Unsettling	k1gInSc4
and	and	k?
Arousing	Arousing	k1gInSc1
<g/>
.	.	kIx.
www.nytimes.com	www.nytimes.com	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Torbjø	Torbjø	k1gInSc1
Rø	Rø	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Touch	Touch	k1gMnSc1
That	Thatum	k1gNnPc2
Made	Made	k1gNnSc7
You	You	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Serpentine	serpentin	k1gInSc5
Galleries	Galleriesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TORBJØ	TORBJØ	k1gMnSc1
RØ	RØ	k1gMnSc1
<g/>
:	:	kIx,
THE	THE	kA
TOUCH	TOUCH	kA
THAT	THAT	kA
MADE	MADE	kA
YOU	YOU	kA
–	–	k?
Fondazione	Fondazion	k1gInSc5
Prada	Prada	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Astrup	Astrup	k1gMnSc1
Fearnley	Fearnlea	k1gFnSc2
Museet	Museet	k1gMnSc1
<g/>
:	:	kIx,
Torbjø	Torbjø	k1gInSc1
Rø	Rø	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Whitney	Whitnea	k1gFnPc1
Museum	museum	k1gNnSc1
of	of	k?
American	American	k1gMnSc1
Art	Art	k1gMnSc1
<g/>
:	:	kIx,
Torbjø	Torbjø	k1gInSc1
Rø	Rø	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MoMA	MoMA	k1gFnSc1
<g/>
:	:	kIx,
Torbjø	Torbjø	k1gInSc1
Rø	Rø	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Stedelijk	Stedelijk	k1gInSc1
Museum	museum	k1gNnSc1
Amsterdam	Amsterdam	k1gInSc1
<g/>
:	:	kIx,
Torbjø	Torbjø	k1gInSc1
Rø	Rø	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gMnSc1
Museum	museum	k1gNnSc1
of	of	k?
Contemporary	Contemporar	k1gInPc1
Art	Art	k1gFnSc7
Chicago	Chicago	k1gNnSc4
<g/>
:	:	kIx,
Torbjø	Torbjø	k1gInSc1
Rø	Rø	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Moderna	Moderna	k1gFnSc1
Museet	Museet	k1gMnSc1
<g/>
:	:	kIx,
Torbjø	Torbjø	k1gInSc1
Rø	Rø	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Torbjø	Torbjø	k1gInSc1
Rø	Rø	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Puzzling	Puzzling	k1gInSc1
Photos	Photosa	k1gFnPc2
Are	ar	k1gInSc5
Unsettling	Unsettling	k1gInSc4
and	and	k?
Arousing	Arousing	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Torbjø	Torbjø	k1gInSc1
Rø	Rø	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Blom	Blom	k1gMnSc1
<g/>
,	,	kIx,
Ina	Ina	k1gMnSc1
<g/>
,	,	kIx,
„	„	k?
<g/>
Dream	Dream	k1gInSc1
Work	Work	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Art	Art	k1gMnSc1
of	of	k?
Torbjø	Torbjø	k1gNnPc2
Rø	Rø	k1gNnPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
Artforum	Artforum	k1gInSc1
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
September	September	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
338	#num#	k4
<g/>
–	–	k?
<g/>
349	#num#	k4
<g/>
↑	↑	k?
Blalock	Blalock	k1gMnSc1
<g/>
,	,	kIx,
Lucas	Lucas	k1gMnSc1
with	with	k1gMnSc1
Rø	Rø	k1gFnPc2
and	and	k?
an	an	k?
introduction	introduction	k1gInSc4
by	by	kYmCp3nS
Hoffman	Hoffman	k1gMnSc1
<g/>
,	,	kIx,
Jens	Jens	k1gInSc1
<g/>
,	,	kIx,
Folding	Folding	k1gInSc1
the	the	k?
Periphery	Periphera	k1gFnSc2
<g/>
,	,	kIx,
Mousse	Mousse	k1gFnSc2
#	#	kIx~
<g/>
42	#num#	k4
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
2014	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
<g/>
118	#num#	k4
<g/>
↑	↑	k?
HERBERT	HERBERT	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Profile	profil	k1gInSc5
<g/>
:	:	kIx,
Torbjø	Torbjø	k1gFnPc2
Rø	Rø	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Contemporary	Contemporar	k1gMnPc4
Magazine	Magazin	k1gInSc5
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://scs-assets-cdn.vice.com/int/v16n7/htdocs/torbjorn-rodland-perverted-photo-947/2.jpg	http://scs-assets-cdn.vice.com/int/v16n7/htdocs/torbjorn-rodland-perverted-photo-947/2.jpg	k1gMnSc1
<g/>
↑	↑	k?
Blalock	Blalock	k1gMnSc1
<g/>
,	,	kIx,
Lucas	Lucas	k1gMnSc1
with	with	k1gMnSc1
Rø	Rø	k1gFnPc2
and	and	k?
an	an	k?
introduction	introduction	k1gInSc4
by	by	kYmCp3nS
Hoffman	Hoffman	k1gMnSc1
<g/>
,	,	kIx,
Jens	Jens	k1gInSc1
<g/>
,	,	kIx,
Folding	Folding	k1gInSc1
the	the	k?
Periphery	Periphera	k1gFnSc2
<g/>
,	,	kIx,
Mousse	Mousse	k1gFnSc2
#	#	kIx~
<g/>
42	#num#	k4
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
2014	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
<g/>
119	#num#	k4
<g/>
↑	↑	k?
NICKAS	NICKAS	kA
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Perverted	Perverted	k1gMnSc1
Photography	Photographa	k1gFnSc2
of	of	k?
Torbjø	Torbjø	k1gInSc1
Rø	Rø	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vice	vika	k1gFnSc6
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MARTINEZ	MARTINEZ	kA
<g/>
,	,	kIx,
Christina	Christina	k1gFnSc1
Catherine	Catherin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
perverse	perverse	k1gFnSc2
photography	photographa	k1gFnSc2
of	of	k?
Torbjø	Torbjø	k1gInSc1
Rø	Rø	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Simpson	Simpson	k1gMnSc1
<g/>
,	,	kIx,
Bennett	Bennett	k1gMnSc1
<g/>
,	,	kIx,
„	„	k?
<g/>
Torbjø	Torbjø	k1gInSc1
Rø	Rø	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Sentimental	Sentimental	k1gMnSc1
Education	Education	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
Nu	nu	k9
<g/>
:	:	kIx,
The	The	k1gMnSc1
Nordic	Nordic	k1gMnSc1
Art	Art	k1gMnSc1
Review	Review	k1gMnSc1
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
No	no	k9
<g/>
.3	.3	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
Stockholm	Stockholm	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
52	#num#	k4
<g/>
–	–	k?
<g/>
59	#num#	k4
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
momaps	momaps	k1gInSc1
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Torbjø	Torbjø	k1gInSc1
Rø	Rø	k1gInSc1
<g/>
:	:	kIx,
Fifth	Fifth	k1gMnSc1
Honeymoon	Honeymoon	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Fifth	Fifth	k1gMnSc1
Honeymoon	Honeymoon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bonniers	Bonniers	k1gInSc1
Konsthall	Konsthall	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Fifth	Fifth	k1gMnSc1
Honeymoon	Honeymoon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Museum	museum	k1gNnSc4
of	of	k?
Contemporary	Contemporara	k1gFnSc2
Art	Art	k1gMnSc4
Kiasma	Kiasm	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Fotografie	fotografia	k1gFnPc1
v	v	k7c6
Norsku	Norsko	k1gNnSc6
</s>
<s>
Seznam	seznam	k1gInSc1
norských	norský	k2eAgMnPc2d1
fotografů	fotograf	k1gMnPc2
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Andenæ	Andenæ	k1gInSc1
<g/>
,	,	kIx,
Morten	Morten	k2eAgInSc1d1
<g/>
,	,	kIx,
„	„	k?
<g/>
Torbjø	Torbjø	k1gInSc1
Rø	Rø	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objectiv	Objectiv	k1gInSc1
June	jun	k1gMnSc5
2018	#num#	k4
</s>
<s>
Sholis	Sholis	k1gFnSc1
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
<g/>
,	,	kIx,
„	„	k?
<g/>
Torbjø	Torbjø	k1gInSc1
Rø	Rø	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aperture	Apertur	k1gMnSc5
221	#num#	k4
<g/>
,	,	kIx,
listopad	listopad	k1gInSc1
2015	#num#	k4
</s>
<s>
Sholis	Sholis	k1gFnSc1
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
<g/>
,	,	kIx,
„	„	k?
<g/>
Torbjø	Torbjø	k1gInSc1
Rø	Rø	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frieze	Frieze	k1gFnSc1
<g/>
,	,	kIx,
Issue	Issue	k1gFnSc1
173	#num#	k4
<g/>
,	,	kIx,
srpen	srpen	k1gInSc4
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Johnson	Johnson	k1gMnSc1
<g/>
,	,	kIx,
Ken	Ken	k1gMnSc1
„	„	k?
<g/>
Review	Review	k1gMnSc1
<g/>
:	:	kIx,
Torbjorn	Torbjorn	k1gMnSc1
Rodland	Rodlanda	k1gFnPc2
Mixes	Mixes	k1gMnSc1
the	the	k?
Religious	Religious	k1gMnSc1
With	With	k1gMnSc1
the	the	k?
Evocative	Evocativ	k1gInSc5
<g/>
“	“	k?
<g/>
,	,	kIx,
The	The	k1gMnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
,	,	kIx,
12	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2015	#num#	k4
(	(	kIx(
<g/>
nutná	nutný	k2eAgFnSc1d1
registrace	registrace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Gavin	Gavin	k1gMnSc1
<g/>
,	,	kIx,
Francesca	Francesca	k1gMnSc1
<g/>
,	,	kIx,
„	„	k?
<g/>
Torbjø	Torbjø	k1gInSc1
Rø	Rø	k1gInSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
Dazed	Dazed	k1gInSc1
Digital	Digital	kA
<g/>
,	,	kIx,
leden	leden	k1gInSc4
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
The	The	k?
Photo	Photo	k1gNnSc1
Department	department	k1gInSc1
<g/>
,	,	kIx,
„	„	k?
<g/>
Our	Our	k1gFnSc1
Top	topit	k5eAaImRp2nS
10	#num#	k4
Photo	Photo	k1gNnSc1
Books	Booksa	k1gFnPc2
of	of	k?
2012	#num#	k4
<g/>
.	.	kIx.
<g/>
“	“	k?
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gMnSc1
Magazine	Magazin	k1gInSc5
<g/>
,	,	kIx,
prosinec	prosinec	k1gInSc4
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rø	Rø	k1gInSc1
<g/>
,	,	kIx,
Torbjø	Torbjø	k1gInSc1
<g/>
,	,	kIx,
„	„	k?
<g/>
Sentences	Sentences	k1gInSc1
on	on	k3xPp3gMnSc1
Photography	Photograph	k1gInPc7
<g/>
.	.	kIx.
<g/>
“	“	k?
Triple	tripl	k1gInSc5
Canopy	Canopa	k1gFnPc4
<g/>
,	,	kIx,
květen	květen	k1gInSc4
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Taft	taft	k1gInSc1
<g/>
,	,	kIx,
Catherine	Catherin	k1gInSc5
<g/>
,	,	kIx,
„	„	k?
<g/>
Torbjø	Torbjø	k1gInSc1
Rø	Rø	k1gInSc1
at	at	k?
Michael	Michael	k1gMnSc1
Benevento	Benevento	k1gNnSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
Artform	Artform	k1gInSc1
<g/>
,	,	kIx,
říjen	říjen	k1gInSc1
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Lavalette	Lavalett	k1gMnSc5
<g/>
,	,	kIx,
Shane	Shan	k1gMnSc5
<g/>
,	,	kIx,
„	„	k?
<g/>
Torbjø	Torbjø	k1gInSc1
Rø	Rø	k1gInSc1
in	in	k?
Conversation	Conversation	k1gInSc1
<g/>
.	.	kIx.
<g/>
“	“	k?
Photo-Eye	Photo-Eyus	k1gMnSc5
Magazine	Magazin	k1gMnSc5
<g/>
,	,	kIx,
říjen	říjen	k1gInSc4
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rø	Rø	k1gMnSc1
<g/>
,	,	kIx,
Kjetil	Kjetil	k1gMnSc1
<g/>
,	,	kIx,
“	“	k?
<g/>
Books	Books	k1gInSc1
<g/>
:	:	kIx,
I	i	k8xC
Want	Want	k1gMnSc1
To	ten	k3xDgNnSc4
Live	Live	k1gNnSc4
Innocent	Innocent	k1gInSc4
<g/>
.	.	kIx.
<g/>
”	”	k?
Artreview	Artreview	k1gFnSc1
<g/>
,	,	kIx,
červen	červen	k1gInSc1
2008	#num#	k4
<g/>
:	:	kIx,
vyd	vyd	k?
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kessler	Kessler	k1gInSc1
<g/>
,	,	kIx,
Sarah	Sarah	k1gFnSc1
<g/>
,	,	kIx,
“	“	k?
<g/>
Torbjø	Torbjø	k1gInSc1
Rø	Rø	k1gInSc1
<g/>
:	:	kIx,
Opposites	Opposites	k1gMnSc1
Must	Must	k1gMnSc1
Never	Never	k1gMnSc1
Cease	Cease	k1gFnSc2
to	ten	k3xDgNnSc4
Come	Come	k1gNnSc4
Together	Togethra	k1gFnPc2
<g/>
.	.	kIx.
<g/>
”	”	k?
Whitewall	Whitewall	k1gInSc1
<g/>
,	,	kIx,
duben	duben	k1gInSc1
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Blank	Blank	k1gMnSc1
<g/>
,	,	kIx,
Gil	Gil	k1gMnSc1
<g/>
,	,	kIx,
„	„	k?
<g/>
Interview	interview	k1gInSc1
<g/>
:	:	kIx,
Torbjø	Torbjø	k1gNnSc1
Rø	Rø	k1gFnPc2
<g/>
.	.	kIx.
<g/>
“	“	k?
Uovo	Uova	k1gMnSc5
Magazine	Magazin	k1gMnSc5
<g/>
,	,	kIx,
březen	březen	k1gInSc4
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Fox	fox	k1gInSc1
<g/>
,	,	kIx,
Dan	Dan	k1gMnSc1
<g/>
,	,	kIx,
„	„	k?
<g/>
Another	Anothra	k1gFnPc2
Green	Green	k2eAgInSc4d1
World	World	k1gInSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
Frieze	Frieze	k1gFnSc2
<g/>
,	,	kIx,
červen-srpen	červen-srpen	k2eAgMnSc1d1
2001	#num#	k4
<g/>
:	:	kIx,
Issue	Issu	k1gInSc2
60	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Fotografie	fotografia	k1gFnPc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
8159	#num#	k4
690X	690X	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nr	nr	k?
<g/>
2005027572	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500389398	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
79459601	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nr	lccn-nr	k1gInSc1
<g/>
2005027572	#num#	k4
</s>
