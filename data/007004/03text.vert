<s>
Irina	Irina	k1gFnSc1	Irina
Andrejevna	Andrejevna	k1gFnSc1	Andrejevna
Gordějevová	Gordějevová	k1gFnSc1	Gordějevová
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
И	И	k?	И
А	А	k?	А
Г	Г	k?	Г
<g/>
;	;	kIx,	;
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
Leningrad	Leningrad	k1gInSc1	Leningrad
<g/>
,	,	kIx,	,
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ruská	ruský	k2eAgFnSc1d1	ruská
atletka	atletka	k1gFnSc1	atletka
<g/>
,	,	kIx,	,
výškařka	výškařka	k1gFnSc1	výškařka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
do	do	k7c2	do
17	[number]	k4	17
let	léto	k1gNnPc2	léto
v	v	k7c6	v
kanadském	kanadský	k2eAgNnSc6d1	kanadské
Sherbrooke	Sherbrooke	k1gNnSc6	Sherbrooke
sedmá	sedmý	k4xOgFnSc1	sedmý
(	(	kIx(	(
<g/>
175	[number]	k4	175
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c6	na
juniorském	juniorský	k2eAgNnSc6d1	juniorské
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
Grossetu	Grosset	k1gInSc6	Grosset
na	na	k7c4	na
9	[number]	k4	9
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
(	(	kIx(	(
<g/>
180	[number]	k4	180
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
málo	málo	k6eAd1	málo
ji	on	k3xPp3gFnSc4	on
od	od	k7c2	od
stupně	stupeň	k1gInSc2	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
dělilo	dělit	k5eAaImAgNnS	dělit
na	na	k7c6	na
ME	ME	kA	ME
juniorů	junior	k1gMnPc2	junior
v	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
2005	[number]	k4	2005
v	v	k7c6	v
litevském	litevský	k2eAgInSc6d1	litevský
Kaunasu	Kaunas	k1gInSc6	Kaunas
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
obsadila	obsadit	k5eAaPmAgFnS	obsadit
výkonem	výkon	k1gInSc7	výkon
182	[number]	k4	182
cm	cm	kA	cm
4	[number]	k4	4
<g/>
.	.	kIx.	.
pozici	pozice	k1gFnSc6	pozice
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
překonala	překonat	k5eAaPmAgFnS	překonat
dvoumetrovou	dvoumetrový	k2eAgFnSc4d1	dvoumetrová
hranici	hranice	k1gFnSc4	hranice
a	a	k8xC	a
výkonem	výkon	k1gInSc7	výkon
201	[number]	k4	201
cm	cm	kA	cm
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Skokanský	skokanský	k2eAgInSc4d1	skokanský
mítink	mítink	k1gInSc4	mítink
v	v	k7c6	v
Chotěbuzi	Chotěbuz	k1gFnSc6	Chotěbuz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
halovém	halový	k2eAgNnSc6d1	halové
ME	ME	kA	ME
v	v	k7c6	v
Turíně	Turín	k1gInSc6	Turín
však	však	k9	však
skočila	skočit	k5eAaPmAgFnS	skočit
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
192	[number]	k4	192
cm	cm	kA	cm
a	a	k8xC	a
obsadila	obsadit	k5eAaPmAgFnS	obsadit
společně	společně	k6eAd1	společně
s	s	k7c7	s
Blankou	Blanka	k1gFnSc7	Blanka
Vlašičovou	Vlašičův	k2eAgFnSc7d1	Vlašičův
páté	pátý	k4xOgNnSc4	pátý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
neprošla	projít	k5eNaPmAgFnS	projít
na	na	k7c6	na
halovém	halový	k2eAgNnSc6d1	halové
MS	MS	kA	MS
v	v	k7c6	v
katarském	katarský	k2eAgInSc6d1	katarský
Dauhá	Dauhý	k2eAgNnPc1d1	Dauhý
a	a	k8xC	a
na	na	k7c4	na
ME	ME	kA	ME
v	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
sítem	síto	k1gNnSc7	síto
kvalifikace	kvalifikace	k1gFnSc2	kvalifikace
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
pod	pod	k7c7	pod
stupni	stupeň	k1gInPc7	stupeň
vítězů	vítěz	k1gMnPc2	vítěz
<g/>
,	,	kIx,	,
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
letní	letní	k2eAgFnSc6d1	letní
univerziádě	univerziáda	k1gFnSc6	univerziáda
v	v	k7c6	v
čínském	čínský	k2eAgInSc6d1	čínský
Šen-čenu	Šen-čen	k2eAgFnSc4d1	Šen-čen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
překonala	překonat	k5eAaPmAgFnS	překonat
186	[number]	k4	186
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
Anna	Anna	k1gFnSc1	Anna
Iljuštšenková	Iljuštšenkový	k2eAgFnSc1d1	Iljuštšenkový
z	z	k7c2	z
Estonska	Estonsko	k1gNnSc2	Estonsko
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
překonala	překonat	k5eAaPmAgFnS	překonat
napoprvé	napoprvé	k6eAd1	napoprvé
194	[number]	k4	194
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
halovém	halový	k2eAgInSc6d1	halový
MS	MS	kA	MS
2012	[number]	k4	2012
v	v	k7c6	v
tureckém	turecký	k2eAgInSc6d1	turecký
Istanbulu	Istanbul	k1gInSc6	Istanbul
nepřekonala	překonat	k5eNaPmAgFnS	překonat
v	v	k7c6	v
kvalifikaci	kvalifikace	k1gFnSc6	kvalifikace
195	[number]	k4	195
cm	cm	kA	cm
a	a	k8xC	a
obsadila	obsadit	k5eAaPmAgFnS	obsadit
dělené	dělený	k2eAgInPc4d1	dělený
9	[number]	k4	9
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
se	s	k7c7	s
Švédkou	Švédka	k1gFnSc7	Švédka
Emmou	Emma	k1gFnSc7	Emma
Greenovou	Greenová	k1gFnSc7	Greenová
a	a	k8xC	a
Litevkou	Litevka	k1gFnSc7	Litevka
Airinė	Airinė	k1gFnSc7	Airinė
Palšytė	Palšytė	k1gFnSc7	Palšytė
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
šampionátu	šampionát	k1gInSc6	šampionát
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
prozatím	prozatím	k6eAd1	prozatím
největší	veliký	k2eAgInSc4d3	veliký
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
když	když	k8xS	když
výkonem	výkon	k1gInSc7	výkon
192	[number]	k4	192
cm	cm	kA	cm
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
bronz	bronz	k1gInSc4	bronz
se	se	k3xPyFc4	se
podělila	podělit	k5eAaPmAgFnS	podělit
společně	společně	k6eAd1	společně
se	s	k7c7	s
Švédkou	Švédka	k1gFnSc7	Švédka
Emmou	Emma	k1gFnSc7	Emma
Greenovou	Greenová	k1gFnSc7	Greenová
a	a	k8xC	a
Ukrajinkou	Ukrajinka	k1gFnSc7	Ukrajinka
Olenou	Olena	k1gFnSc7	Olena
Hološovou	Hološův	k2eAgFnSc7d1	Hološův
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2012	[number]	k4	2012
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
postoupila	postoupit	k5eAaPmAgFnS	postoupit
do	do	k7c2	do
dvanáctičlenného	dvanáctičlenný	k2eAgNnSc2d1	dvanáctičlenné
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
obsadila	obsadit	k5eAaPmAgFnS	obsadit
10	[number]	k4	10
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
193	[number]	k4	193
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
výkonem	výkon	k1gInSc7	výkon
204	[number]	k4	204
cm	cm	kA	cm
na	na	k7c6	na
prestižním	prestižní	k2eAgInSc6d1	prestižní
výškařském	výškařský	k2eAgInSc6d1	výškařský
mítinku	mítink	k1gInSc6	mítink
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
Eberstadtu	Eberstadt	k1gInSc6	Eberstadt
<g/>
.	.	kIx.	.
</s>
<s>
Vítězkou	vítězka	k1gFnSc7	vítězka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
výkonem	výkon	k1gInSc7	výkon
202	[number]	k4	202
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Dvoumetrovou	dvoumetrový	k2eAgFnSc4d1	dvoumetrová
hranici	hranice	k1gFnSc4	hranice
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc1d2	vyšší
zdolala	zdolat	k5eAaPmAgFnS	zdolat
celkové	celkový	k2eAgNnSc4d1	celkové
třikrát	třikrát	k6eAd1	třikrát
<g/>
.	.	kIx.	.
hala	hala	k1gFnSc1	hala
–	–	k?	–
201	[number]	k4	201
cm	cm	kA	cm
–	–	k?	–
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
Chotěbuz	Chotěbuz	k1gFnSc1	Chotěbuz
venku	venek	k1gInSc2	venek
–	–	k?	–
204	[number]	k4	204
cm	cm	kA	cm
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
Eberstadt	Eberstadt	k1gInSc1	Eberstadt
</s>
