<s>
Astronomie	astronomie	k1gFnSc1	astronomie
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
α	α	k?	α
z	z	k7c2	z
ά	ά	k?	ά
(	(	kIx(	(
<g/>
astron	astron	k1gMnSc1	astron
<g/>
)	)	kIx)	)
hvězda	hvězda	k1gFnSc1	hvězda
a	a	k8xC	a
ν	ν	k?	ν
(	(	kIx(	(
<g/>
nomos	nomos	k1gMnSc1	nomos
<g/>
)	)	kIx)	)
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
hvězdářství	hvězdářství	k1gNnSc4	hvězdářství
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
jevy	jev	k1gInPc4	jev
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
tedy	tedy	k8xC	tedy
výzkumem	výzkum	k1gInSc7	výzkum
vesmírných	vesmírný	k2eAgNnPc2d1	vesmírné
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc2	jejich
soustav	soustava	k1gFnPc2	soustava
<g/>
,	,	kIx,	,
různých	různý	k2eAgInPc2d1	různý
dějů	děj	k1gInPc2	děj
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
i	i	k8xC	i
vesmírem	vesmír	k1gInSc7	vesmír
jako	jako	k8xC	jako
celkem	celek	k1gInSc7	celek
<g/>
.	.	kIx.	.
</s>
<s>
Astronomie	astronomie	k1gFnSc1	astronomie
se	se	k3xPyFc4	se
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
další	další	k2eAgFnSc2d1	další
vědy	věda	k1gFnSc2	věda
začala	začít	k5eAaPmAgFnS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Babylonie	Babylonie	k1gFnSc2	Babylonie
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
používáno	používán	k2eAgNnSc1d1	používáno
již	již	k9	již
vynalezené	vynalezený	k2eAgFnPc4d1	vynalezená
geometrie	geometrie	k1gFnPc4	geometrie
(	(	kIx(	(
<g/>
grafy	graf	k1gInPc4	graf
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
se	se	k3xPyFc4	se
z	z	k7c2	z
astronomie	astronomie	k1gFnSc2	astronomie
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
astrometrie	astrometrie	k1gFnSc1	astrometrie
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
měřením	měření	k1gNnSc7	měření
poloh	poloha	k1gFnPc2	poloha
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
planet	planeta	k1gFnPc2	planeta
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
astronomie	astronomie	k1gFnSc2	astronomie
měla	mít	k5eAaImAgFnS	mít
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
navigaci	navigace	k1gFnSc4	navigace
<g/>
.	.	kIx.	.
</s>
<s>
Podstatnou	podstatný	k2eAgFnSc7d1	podstatná
částí	část	k1gFnSc7	část
astrometrie	astrometrie	k1gFnSc2	astrometrie
je	být	k5eAaImIp3nS	být
sférická	sférický	k2eAgFnSc1d1	sférická
astronomie	astronomie	k1gFnSc1	astronomie
sloužící	sloužící	k1gFnSc1	sloužící
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
poloh	poloha	k1gFnPc2	poloha
objektů	objekt	k1gInPc2	objekt
na	na	k7c6	na
nebeské	nebeský	k2eAgFnSc6d1	nebeská
sféře	sféra	k1gFnSc6	sféra
<g/>
,	,	kIx,	,
zavádí	zavádět	k5eAaImIp3nS	zavádět
souřadnice	souřadnice	k1gFnSc1	souřadnice
a	a	k8xC	a
popisuje	popisovat	k5eAaImIp3nS	popisovat
významné	významný	k2eAgFnPc4d1	významná
křivky	křivka	k1gFnPc4	křivka
a	a	k8xC	a
body	bod	k1gInPc4	bod
na	na	k7c6	na
nebeské	nebeský	k2eAgFnSc6d1	nebeská
sféře	sféra	k1gFnSc6	sféra
<g/>
.	.	kIx.	.
</s>
<s>
Pojmy	pojem	k1gInPc1	pojem
ze	z	k7c2	z
sférické	sférický	k2eAgFnSc2d1	sférická
astronomie	astronomie	k1gFnSc2	astronomie
se	se	k3xPyFc4	se
také	také	k9	také
používají	používat	k5eAaImIp3nP	používat
při	při	k7c6	při
měření	měření	k1gNnSc6	měření
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
oblastí	oblast	k1gFnSc7	oblast
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
nebeská	nebeský	k2eAgFnSc1d1	nebeská
mechanika	mechanika	k1gFnSc1	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
pohybem	pohyb	k1gInSc7	pohyb
těles	těleso	k1gNnPc2	těleso
v	v	k7c6	v
gravitačním	gravitační	k2eAgNnSc6d1	gravitační
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
planet	planeta	k1gFnPc2	planeta
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
nebeské	nebeský	k2eAgFnSc2d1	nebeská
mechaniky	mechanika	k1gFnSc2	mechanika
jsou	být	k5eAaImIp3nP	být
práce	práce	k1gFnPc1	práce
Keplera	Kepler	k1gMnSc2	Kepler
a	a	k8xC	a
Newtona	Newton	k1gMnSc2	Newton
<g/>
.	.	kIx.	.
</s>
<s>
Aristotelés	Aristotelés	k6eAd1	Aristotelés
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
O	o	k7c6	o
nebi	nebe	k1gNnSc6	nebe
z	z	k7c2	z
roku	rok	k1gInSc2	rok
340	[number]	k4	340
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tvar	tvar	k1gInSc1	tvar
Země	zem	k1gFnSc2	zem
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
kulatý	kulatý	k2eAgMnSc1d1	kulatý
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
stín	stín	k1gInSc1	stín
Země	zem	k1gFnSc2	zem
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
zatmění	zatmění	k1gNnSc6	zatmění
vždy	vždy	k6eAd1	vždy
kulatý	kulatý	k2eAgMnSc1d1	kulatý
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nP	by
při	při	k7c6	při
plochém	plochý	k2eAgInSc6d1	plochý
tvaru	tvar	k1gInSc6	tvar
Země	zem	k1gFnSc2	zem
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
také	také	k9	také
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
sledujeme	sledovat	k5eAaImIp1nP	sledovat
Polárku	Polárka	k1gFnSc4	Polárka
z	z	k7c2	z
jižnějšího	jižní	k2eAgNnSc2d2	jižnější
místa	místo	k1gNnSc2	místo
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
jeví	jevit	k5eAaImIp3nS	jevit
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
níže	nízce	k6eAd2	nízce
nad	nad	k7c7	nad
obzorem	obzor	k1gInSc7	obzor
než	než	k8xS	než
pro	pro	k7c4	pro
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
<g/>
,	,	kIx,	,
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
její	její	k3xOp3gNnSc1	její
poloha	poloha	k1gFnSc1	poloha
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
jevit	jevit	k5eAaImF	jevit
výše	vysoce	k6eAd2	vysoce
<g/>
.	.	kIx.	.
</s>
<s>
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
dále	daleko	k6eAd2	daleko
určil	určit	k5eAaPmAgInS	určit
poloměr	poloměr	k1gInSc1	poloměr
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ale	ale	k9	ale
odhadl	odhadnout	k5eAaPmAgInS	odhadnout
na	na	k7c4	na
dvojnásobek	dvojnásobek	k1gInSc4	dvojnásobek
skutečného	skutečný	k2eAgInSc2d1	skutečný
poloměru	poloměr	k1gInSc2	poloměr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
aristotelovském	aristotelovský	k2eAgInSc6d1	aristotelovský
modelu	model	k1gInSc6	model
Země	zem	k1gFnSc2	zem
stojí	stát	k5eAaImIp3nS	stát
a	a	k8xC	a
Měsíc	měsíc	k1gInSc1	měsíc
se	s	k7c7	s
Sluncem	slunce	k1gNnSc7	slunce
a	a	k8xC	a
hvězdami	hvězda	k1gFnPc7	hvězda
krouží	kroužit	k5eAaImIp3nS	kroužit
kolem	kolem	k7c2	kolem
ní	on	k3xPp3gFnSc2	on
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
kruhových	kruhový	k2eAgFnPc6d1	kruhová
drahách	draha	k1gFnPc6	draha
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenky	myšlenka	k1gFnPc1	myšlenka
Aristotelovy	Aristotelův	k2eAgFnPc1d1	Aristotelova
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
Klaudios	Klaudios	k1gMnSc1	Klaudios
Ptolemaios	Ptolemaios	k1gMnSc1	Ptolemaios
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
také	také	k9	také
stavěl	stavět	k5eAaImAgMnS	stavět
Zemi	zem	k1gFnSc4	zem
do	do	k7c2	do
středu	střed	k1gInSc2	střed
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
objekty	objekt	k1gInPc4	objekt
nechal	nechat	k5eAaPmAgMnS	nechat
obíhat	obíhat	k5eAaImF	obíhat
kolem	kolem	k7c2	kolem
ní	on	k3xPp3gFnSc2	on
ve	v	k7c6	v
sférách	sféra	k1gFnPc6	sféra
<g/>
:	:	kIx,	:
první	první	k4xOgFnSc1	první
byla	být	k5eAaImAgFnS	být
sféra	sféra	k1gFnSc1	sféra
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
sféry	sféra	k1gFnSc2	sféra
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
-	-	kIx~	-
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
planet	planeta	k1gFnPc2	planeta
:	:	kIx,	:
Merkuru	Merkur	k1gInSc2	Merkur
<g/>
,	,	kIx,	,
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
Marsu	Mars	k1gInSc2	Mars
<g/>
,	,	kIx,	,
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
,	,	kIx,	,
Saturna	Saturn	k1gMnSc2	Saturn
a	a	k8xC	a
sféra	sféra	k1gFnSc1	sféra
stálic	stálice	k1gFnPc2	stálice
-	-	kIx~	-
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
byly	být	k5eAaImAgFnP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
nehybné	hybný	k2eNgNnSc4d1	nehybné
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
plyne	plynout	k5eAaImIp3nS	plynout
z	z	k7c2	z
názvu	název	k1gInSc2	název
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgFnP	mít
se	se	k3xPyFc4	se
pohybovat	pohybovat	k5eAaImF	pohybovat
jen	jen	k9	jen
společně	společně	k6eAd1	společně
s	s	k7c7	s
oblohou	obloha	k1gFnSc7	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
model	model	k1gInSc1	model
dostatečně	dostatečně	k6eAd1	dostatečně
vyhovoval	vyhovovat	k5eAaImAgInS	vyhovovat
polohám	poloha	k1gFnPc3	poloha
těles	těleso	k1gNnPc2	těleso
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1514	[number]	k4	1514
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Koperník	Koperník	k1gMnSc1	Koperník
nový	nový	k2eAgInSc4d1	nový
model	model	k1gInSc4	model
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
soustavy	soustava	k1gFnSc2	soustava
Slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
planety	planeta	k1gFnPc1	planeta
obíhaly	obíhat	k5eAaImAgFnP	obíhat
kolem	kolem	k7c2	kolem
něj	on	k3xPp3gNnSc2	on
po	po	k7c6	po
kruhových	kruhový	k2eAgFnPc6d1	kruhová
drahách	draha	k1gFnPc6	draha
<g/>
,	,	kIx,	,
setkal	setkat	k5eAaPmAgMnS	setkat
se	se	k3xPyFc4	se
ale	ale	k9	ale
s	s	k7c7	s
problémy	problém	k1gInPc7	problém
při	při	k7c6	při
pozorováních	pozorování	k1gNnPc6	pozorování
<g/>
,	,	kIx,	,
objekty	objekt	k1gInPc1	objekt
se	se	k3xPyFc4	se
nenacházely	nacházet	k5eNaImAgInP	nacházet
na	na	k7c6	na
správných	správný	k2eAgFnPc6d1	správná
souřadnicích	souřadnice	k1gFnPc6	souřadnice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1609	[number]	k4	1609
zkonstruoval	zkonstruovat	k5eAaPmAgInS	zkonstruovat
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galile	k1gMnPc7	Galile
dalekohled	dalekohled	k1gInSc4	dalekohled
<g/>
,	,	kIx,	,
s	s	k7c7	s
jehož	jehož	k3xOyRp3gFnSc7	jehož
pomocí	pomoc	k1gFnSc7	pomoc
objevil	objevit	k5eAaPmAgMnS	objevit
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
obíhající	obíhající	k2eAgFnSc4d1	obíhající
kolem	kolem	k7c2	kolem
planety	planeta	k1gFnSc2	planeta
Jupiter	Jupiter	k1gInSc1	Jupiter
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
dokázal	dokázat	k5eAaPmAgMnS	dokázat
Koperníkovu	Koperníkův	k2eAgFnSc4d1	Koperníkova
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
Slunci	slunce	k1gNnSc6	slunce
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
a	a	k8xC	a
planetách	planeta	k1gFnPc6	planeta
kroužících	kroužící	k2eAgFnPc6d1	kroužící
kolem	kolem	k6eAd1	kolem
<g/>
.	.	kIx.	.
</s>
<s>
Johannes	Johannes	k1gMnSc1	Johannes
Kepler	Kepler	k1gMnSc1	Kepler
zaměnil	zaměnit	k5eAaPmAgMnS	zaměnit
kruhové	kruhový	k2eAgFnSc2d1	kruhová
dráhy	dráha	k1gFnSc2	dráha
planet	planeta	k1gFnPc2	planeta
za	za	k7c4	za
eliptické	eliptický	k2eAgNnSc4d1	eliptické
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
pozorovanými	pozorovaný	k2eAgFnPc7d1	pozorovaná
polohami	poloha	k1gFnPc7	poloha
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1687	[number]	k4	1687
vydal	vydat	k5eAaPmAgMnS	vydat
sir	sir	k1gMnSc1	sir
Isaac	Isaac	k1gFnSc4	Isaac
Newton	Newton	k1gMnSc1	Newton
knihu	kniha	k1gFnSc4	kniha
Philosophiae	Philosophiae	k1gFnSc4	Philosophiae
Naturalis	Naturalis	k1gFnSc7	Naturalis
Principia	principium	k1gNnSc2	principium
Mathematica	Mathematic	k1gInSc2	Mathematic
o	o	k7c6	o
poloze	poloha	k1gFnSc6	poloha
těles	těleso	k1gNnPc2	těleso
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
a	a	k8xC	a
čase	čas	k1gInSc6	čas
a	a	k8xC	a
zákon	zákon	k1gInSc1	zákon
obecné	obecný	k2eAgFnSc2d1	obecná
přitažlivosti	přitažlivost	k1gFnSc2	přitažlivost
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgInSc2	jenž
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
sobě	se	k3xPyFc3	se
tělesa	těleso	k1gNnPc1	těleso
vázána	vázat	k5eAaImNgFnS	vázat
gravitací	gravitace	k1gFnSc7	gravitace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
hmotnosti	hmotnost	k1gFnSc6	hmotnost
těles	těleso	k1gNnPc2	těleso
a	a	k8xC	a
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
gravitačního	gravitační	k2eAgInSc2d1	gravitační
zákona	zákon	k1gInSc2	zákon
vychází	vycházet	k5eAaImIp3nS	vycházet
eliptický	eliptický	k2eAgInSc4d1	eliptický
pohyb	pohyb	k1gInSc4	pohyb
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
studoval	studovat	k5eAaImAgMnS	studovat
Edwin	Edwin	k2eAgMnSc1d1	Edwin
Hubble	Hubble	k1gMnSc1	Hubble
daleké	daleký	k2eAgFnSc2d1	daleká
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
,	,	kIx,	,
zjistil	zjistit	k5eAaPmAgInS	zjistit
rudý	rudý	k2eAgInSc1d1	rudý
posuv	posuv	k1gInSc1	posuv
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
se	s	k7c7	s
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
důkaz	důkaz	k1gInSc1	důkaz
o	o	k7c4	o
rozpínání	rozpínání	k1gNnSc4	rozpínání
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
objekty	objekt	k1gInPc1	objekt
vzdalují	vzdalovat	k5eAaImIp3nP	vzdalovat
<g/>
,	,	kIx,	,
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgInP	být
objekty	objekt	k1gInPc1	objekt
velmi	velmi	k6eAd1	velmi
blízko	blízko	k6eAd1	blízko
od	od	k7c2	od
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
zrodily	zrodit	k5eAaPmAgFnP	zrodit
myšlenky	myšlenka	k1gFnPc1	myšlenka
o	o	k7c6	o
velkém	velký	k2eAgInSc6d1	velký
třesku	třesk	k1gInSc6	třesk
<g/>
,	,	kIx,	,
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
čase	čas	k1gInSc6	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
vesmír	vesmír	k1gInSc1	vesmír
nekonečně	konečně	k6eNd1	konečně
malý	malý	k2eAgInSc1d1	malý
a	a	k8xC	a
hustý	hustý	k2eAgInSc1d1	hustý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1905	[number]	k4	1905
<g/>
–	–	k?	–
<g/>
1915	[number]	k4	1915
napsal	napsat	k5eAaPmAgMnS	napsat
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
teorii	teorie	k1gFnSc4	teorie
relativity	relativita	k1gFnSc2	relativita
–	–	k?	–
speciální	speciální	k2eAgInSc1d1	speciální
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
zavedl	zavést	k5eAaPmAgInS	zavést
konečnou	konečný	k2eAgFnSc4d1	konečná
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
obecnou	obecný	k2eAgFnSc4d1	obecná
relativitu	relativita	k1gFnSc4	relativita
o	o	k7c6	o
gravitaci	gravitace	k1gFnSc6	gravitace
<g/>
,	,	kIx,	,
čase	čas	k1gInSc6	čas
a	a	k8xC	a
prostoru	prostor	k1gInSc6	prostor
ve	v	k7c6	v
velkých	velký	k2eAgInPc6d1	velký
rozměrech	rozměr	k1gInPc6	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
kvantová	kvantový	k2eAgFnSc1d1	kvantová
teorie	teorie	k1gFnSc1	teorie
o	o	k7c4	o
chování	chování	k1gNnSc4	chování
elementárních	elementární	k2eAgFnPc2d1	elementární
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
astronomie	astronomie	k1gFnSc1	astronomie
má	mít	k5eAaImIp3nS	mít
velice	velice	k6eAd1	velice
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
historii	historie	k1gFnSc4	historie
a	a	k8xC	a
dějepisci	dějepisec	k1gMnPc1	dějepisec
považují	považovat	k5eAaImIp3nP	považovat
Číňany	Číňan	k1gMnPc7	Číňan
za	za	k7c4	za
"	"	kIx"	"
<g/>
nejdůslednější	důsledný	k2eAgInPc4d3	nejdůslednější
a	a	k8xC	a
nejpřesnější	přesný	k2eAgMnPc4d3	nejpřesnější
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
nebeských	nebeský	k2eAgInPc2d1	nebeský
jevů	jev	k1gInPc2	jev
na	na	k7c6	na
světě	svět	k1gInSc6	svět
před	před	k7c7	před
Araby	Arab	k1gMnPc7	Arab
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Jména	jméno	k1gNnPc1	jméno
hvězd	hvězda	k1gFnPc2	hvězda
později	pozdě	k6eAd2	pozdě
rozdělili	rozdělit	k5eAaPmAgMnP	rozdělit
do	do	k7c2	do
28	[number]	k4	28
kategorií	kategorie	k1gFnPc2	kategorie
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
panství	panství	k1gNnSc1	panství
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
říší	říš	k1gFnSc7	říš
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
dynastie	dynastie	k1gFnSc2	dynastie
Šang	Šang	k1gInSc1	Šang
(	(	kIx(	(
<g/>
Shang	Shang	k1gInSc1	Shang
<g/>
)	)	kIx)	)
v	v	k7c6	v
čínské	čínský	k2eAgFnSc6d1	čínská
době	doba	k1gFnSc6	doba
bronzové	bronzový	k2eAgFnSc6d1	bronzová
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
se	se	k3xPyFc4	se
zformovaly	zformovat	k5eAaPmAgFnP	zformovat
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Wu-Tinga	Wu-Ting	k1gMnSc2	Wu-Ting
(	(	kIx(	(
<g/>
Wu	Wu	k1gMnSc1	Wu
Ding	Ding	k1gMnSc1	Ding
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1339	[number]	k4	1339
<g/>
-	-	kIx~	-
<g/>
1281	[number]	k4	1281
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobné	podrobný	k2eAgInPc1d1	podrobný
záznamy	záznam	k1gInPc1	záznam
astrologických	astrologický	k2eAgNnPc2d1	astrologické
pozorování	pozorování	k1gNnPc2	pozorování
započaly	započnout	k5eAaPmAgFnP	započnout
v	v	k7c6	v
éře	éra	k1gFnSc6	éra
válek	válka	k1gFnPc2	válka
kolem	kolem	k7c2	kolem
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
vzkvétaly	vzkvétat	k5eAaImAgInP	vzkvétat
dále	daleko	k6eAd2	daleko
od	od	k7c2	od
období	období	k1gNnSc2	období
dynastie	dynastie	k1gFnSc2	dynastie
Chan	Chana	k1gFnPc2	Chana
<g/>
.	.	kIx.	.
</s>
<s>
Čínská	čínský	k2eAgFnSc1d1	čínská
astronomie	astronomie	k1gFnSc1	astronomie
byla	být	k5eAaImAgFnS	být
rovníková	rovníkový	k2eAgFnSc1d1	Rovníková
soustředěná	soustředěný	k2eAgFnSc1d1	soustředěná
na	na	k7c4	na
podrobná	podrobný	k2eAgNnPc4d1	podrobné
pozorování	pozorování	k1gNnPc4	pozorování
hvězd	hvězda	k1gFnPc2	hvězda
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
pólu	pól	k1gInSc2	pól
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c6	na
jiných	jiný	k2eAgInPc6d1	jiný
principech	princip	k1gInPc6	princip
než	než	k8xS	než
převládaly	převládat	k5eAaImAgFnP	převládat
v	v	k7c4	v
západoevropské	západoevropský	k2eAgFnPc4d1	západoevropská
astronomie	astronomie	k1gFnPc4	astronomie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
východ	východ	k1gInSc1	východ
a	a	k8xC	a
západ	západ	k1gInSc1	západ
slunce	slunce	k1gNnSc2	slunce
zodiakálních	zodiakální	k2eAgNnPc2d1	zodiakální
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
tvořil	tvořit	k5eAaImAgInS	tvořit
základní	základní	k2eAgInSc1d1	základní
ekliptický	ekliptický	k2eAgInSc1d1	ekliptický
rámec	rámec	k1gInSc1	rámec
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
prvky	prvek	k1gInPc1	prvek
indické	indický	k2eAgFnSc2d1	indická
astronomie	astronomie	k1gFnSc2	astronomie
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
při	při	k7c6	při
expanzi	expanze	k1gFnSc6	expanze
buddhismu	buddhismus	k1gInSc2	buddhismus
po	po	k7c6	po
dynastii	dynastie	k1gFnSc6	dynastie
Chan	Chana	k1gFnPc2	Chana
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
220	[number]	k4	220
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejpodrobnější	podrobný	k2eAgNnSc1d3	nejpodrobnější
vtělení	vtělení	k1gNnSc1	vtělení
indické	indický	k2eAgFnSc2d1	indická
astronomie	astronomie	k1gFnSc2	astronomie
Nastalo	nastat	k5eAaPmAgNnS	nastat
za	za	k7c2	za
dynastie	dynastie	k1gFnSc2	dynastie
Tchang	Tchang	k1gInSc1	Tchang
(	(	kIx(	(
<g/>
Tang	tango	k1gNnPc2	tango
<g/>
,	,	kIx,	,
618	[number]	k4	618
<g/>
-	-	kIx~	-
<g/>
907	[number]	k4	907
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mnozí	mnohý	k2eAgMnPc1d1	mnohý
indičtí	indický	k2eAgMnPc1d1	indický
astronomové	astronom	k1gMnPc1	astronom
přesídlili	přesídlit	k5eAaPmAgMnP	přesídlit
do	do	k7c2	do
čínského	čínský	k2eAgNnSc2d1	čínské
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
a	a	k8xC	a
čínští	čínský	k2eAgMnPc1d1	čínský
učenci	učenec	k1gMnPc1	učenec
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
velký	velký	k2eAgMnSc1d1	velký
tantrický	tantrický	k2eAgMnSc1d1	tantrický
buddhistický	buddhistický	k2eAgMnSc1d1	buddhistický
mnich	mnich	k1gMnSc1	mnich
a	a	k8xC	a
matematik	matematik	k1gMnSc1	matematik
Ji-Šing	Ji-Šing	k1gInSc1	Ji-Šing
(	(	kIx(	(
<g/>
Yi	Yi	k1gMnSc1	Yi
Xing	Xing	k1gMnSc1	Xing
<g/>
)	)	kIx)	)
propracoval	propracovat	k5eAaPmAgInS	propracovat
její	její	k3xOp3gInSc1	její
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Astronomie	astronomie	k1gFnSc1	astronomie
islámského	islámský	k2eAgInSc2d1	islámský
středověku	středověk	k1gInSc2	středověk
úzce	úzko	k6eAd1	úzko
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
čínskými	čínský	k2eAgMnPc7d1	čínský
kolegy	kolega	k1gMnPc7	kolega
během	během	k7c2	během
dynastie	dynastie	k1gFnSc2	dynastie
Juan	Juan	k1gMnSc1	Juan
(	(	kIx(	(
<g/>
Yuan	Yuan	k1gMnSc1	Yuan
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
období	období	k1gNnSc6	období
poměrného	poměrný	k2eAgInSc2d1	poměrný
ústupu	ústup	k1gInSc2	ústup
za	za	k7c2	za
dynastie	dynastie	k1gFnSc2	dynastie
Ming	Minga	k1gFnPc2	Minga
astronomie	astronomie	k1gFnSc1	astronomie
ožila	ožít	k5eAaPmAgFnS	ožít
podněty	podnět	k1gInPc4	podnět
západní	západní	k2eAgFnSc2d1	západní
kosmologie	kosmologie	k1gFnSc2	kosmologie
a	a	k8xC	a
techniky	technika	k1gFnSc2	technika
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
jezuitských	jezuitský	k2eAgFnPc2d1	jezuitská
misií	misie	k1gFnPc2	misie
<g/>
.	.	kIx.	.
</s>
<s>
Dalekohled	dalekohled	k1gInSc1	dalekohled
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1669	[number]	k4	1669
byla	být	k5eAaImAgFnS	být
pekingská	pekingský	k2eAgFnSc1d1	Pekingská
observatoř	observatoř	k1gFnSc1	observatoř
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Verbiesta	Verbiest	k1gMnSc2	Verbiest
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
Čína	Čína	k1gFnSc1	Čína
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
astronomických	astronomický	k2eAgFnPc6d1	astronomická
aktivitách	aktivita	k1gFnPc6	aktivita
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
hvězdárnami	hvězdárna	k1gFnPc7	hvězdárna
a	a	k8xC	a
vlastním	vlastní	k2eAgInSc7d1	vlastní
vesmírným	vesmírný	k2eAgInSc7d1	vesmírný
programem	program	k1gInSc7	program
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Chinese	Chinese	k1gFnSc2	Chinese
astronomy	astronom	k1gMnPc4	astronom
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
novověku	novověk	k1gInSc2	novověk
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
se	se	k3xPyFc4	se
astronomie	astronomie	k1gFnSc1	astronomie
nesmírně	smírně	k6eNd1	smírně
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
nových	nový	k2eAgFnPc2d1	nová
oblastí	oblast	k1gFnPc2	oblast
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
lze	lze	k6eAd1	lze
velmi	velmi	k6eAd1	velmi
zhruba	zhruba	k6eAd1	zhruba
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
pozorování	pozorování	k1gNnSc4	pozorování
a	a	k8xC	a
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
objektu	objekt	k1gInSc2	objekt
zájmu	zájem	k1gInSc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Astronom	astronom	k1gMnSc1	astronom
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
hvězdář	hvězdář	k1gMnSc1	hvězdář
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
zkoumáním	zkoumání	k1gNnSc7	zkoumání
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
astronomů	astronom	k1gMnPc2	astronom
se	se	k3xPyFc4	se
astronomii	astronomie	k1gFnSc3	astronomie
věnuje	věnovat	k5eAaPmIp3nS	věnovat
i	i	k9	i
řada	řada	k1gFnSc1	řada
astronomů	astronom	k1gMnPc2	astronom
amatérských	amatérský	k2eAgMnPc2d1	amatérský
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
zdrojem	zdroj	k1gInSc7	zdroj
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
vesmíru	vesmír	k1gInSc6	vesmír
je	být	k5eAaImIp3nS	být
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gFnPc2	jeho
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
<g/>
,	,	kIx,	,
vnímatelná	vnímatelný	k2eAgFnSc1d1	vnímatelná
očima	oko	k1gNnPc7	oko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Obory	obor	k1gInPc1	obor
astronomického	astronomický	k2eAgNnSc2d1	astronomické
pozorování	pozorování	k1gNnSc2	pozorování
podle	podle	k7c2	podle
využívaných	využívaný	k2eAgFnPc2d1	využívaná
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
jsou	být	k5eAaImIp3nP	být
gama-astronomie	gamastronomie	k1gFnPc1	gama-astronomie
rentgenová	rentgenový	k2eAgFnSc1d1	rentgenová
astronomie	astronomie	k1gFnSc2	astronomie
ultrafialová	ultrafialový	k2eAgFnSc1d1	ultrafialová
astronomie	astronomie	k1gFnSc1	astronomie
optická	optický	k2eAgFnSc1d1	optická
astronomie	astronomie	k1gFnSc2	astronomie
infračervená	infračervený	k2eAgFnSc1d1	infračervená
astronomie	astronomie	k1gFnSc1	astronomie
mikrovlnná	mikrovlnný	k2eAgFnSc1d1	mikrovlnná
astronomie	astronomie	k1gFnSc1	astronomie
radioastronomie	radioastronomie	k1gFnSc2	radioastronomie
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
a	a	k8xC	a
nejdůležitější	důležitý	k2eAgFnSc1d3	nejdůležitější
je	být	k5eAaImIp3nS	být
optická	optický	k2eAgFnSc1d1	optická
astronomie	astronomie	k1gFnSc1	astronomie
<g/>
,	,	kIx,	,
využívající	využívající	k2eAgNnSc1d1	využívající
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Rozvoj	rozvoj	k1gInSc1	rozvoj
dalších	další	k2eAgInPc2d1	další
oborů	obor	k1gInPc2	obor
souvisel	souviset	k5eAaImAgInS	souviset
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
techniky	technika	k1gFnSc2	technika
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
radioastronomie	radioastronomie	k1gFnSc1	radioastronomie
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Karl	Karl	k1gMnSc1	Karl
Guthe	Guth	k1gFnSc2	Guth
Jansky	Jansky	k?	Jansky
při	při	k7c6	při
zkoumání	zkoumání	k1gNnSc6	zkoumání
zdrojů	zdroj	k1gInPc2	zdroj
šumu	šum	k1gInSc2	šum
rušících	rušící	k2eAgInPc2d1	rušící
rádiové	rádiový	k2eAgInPc4d1	rádiový
hovory	hovor	k1gInPc4	hovor
objevil	objevit	k5eAaPmAgMnS	objevit
rádiové	rádiový	k2eAgFnSc2d1	rádiová
emise	emise	k1gFnSc2	emise
centra	centrum	k1gNnSc2	centrum
naší	náš	k3xOp1gFnSc2	náš
Galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Země	zem	k1gFnSc2	zem
mnoho	mnoho	k6eAd1	mnoho
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
účinně	účinně	k6eAd1	účinně
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
gama	gama	k1gNnSc1	gama
a	a	k8xC	a
rentgenové	rentgenový	k2eAgNnSc1d1	rentgenové
pozorování	pozorování	k1gNnSc1	pozorování
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
konat	konat	k5eAaImF	konat
jen	jen	k9	jen
pomocí	pomocí	k7c2	pomocí
stratosférických	stratosférický	k2eAgInPc2d1	stratosférický
balónů	balón	k1gInPc2	balón
a	a	k8xC	a
výrazný	výrazný	k2eAgInSc1d1	výrazný
rozvoj	rozvoj	k1gInSc1	rozvoj
se	se	k3xPyFc4	se
dostavil	dostavit	k5eAaPmAgInS	dostavit
teprve	teprve	k6eAd1	teprve
s	s	k7c7	s
pokrokem	pokrok	k1gInSc7	pokrok
kosmonautiky	kosmonautika	k1gFnSc2	kosmonautika
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
exotičtější	exotický	k2eAgNnPc4d2	exotičtější
je	být	k5eAaImIp3nS	být
pozorování	pozorování	k1gNnPc4	pozorování
jiných	jiný	k2eAgFnPc2d1	jiná
částic	částice	k1gFnPc2	částice
než	než	k8xS	než
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
neutrinová	neutrinová	k1gFnSc1	neutrinová
astronomie	astronomie	k1gFnSc2	astronomie
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
neutrina	neutrino	k1gNnPc1	neutrino
<g/>
,	,	kIx,	,
teleskopy	teleskop	k1gInPc1	teleskop
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
velké	velký	k2eAgFnSc2d1	velká
prostory	prostora	k1gFnSc2	prostora
hluboko	hluboko	k6eAd1	hluboko
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
zaplněné	zaplněný	k2eAgFnPc1d1	zaplněná
vodou	voda	k1gFnSc7	voda
nebo	nebo	k8xC	nebo
jiným	jiný	k2eAgNnSc7d1	jiné
pozorovacím	pozorovací	k2eAgNnSc7d1	pozorovací
médiem	médium	k1gNnSc7	médium
studium	studium	k1gNnSc1	studium
kosmického	kosmický	k2eAgNnSc2d1	kosmické
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
vysokoenergetických	vysokoenergetický	k2eAgFnPc2d1	vysokoenergetická
částic	částice	k1gFnPc2	částice
mimozemského	mimozemský	k2eAgInSc2d1	mimozemský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaPmIp3nS	využívat
metod	metoda	k1gFnPc2	metoda
jaderné	jaderný	k2eAgFnSc2d1	jaderná
fyziky	fyzika	k1gFnSc2	fyzika
(	(	kIx(	(
<g/>
v	v	k7c6	v
kosmickém	kosmický	k2eAgNnSc6d1	kosmické
záření	záření	k1gNnSc6	záření
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
částice	částice	k1gFnPc1	částice
s	s	k7c7	s
o	o	k7c6	o
mnoho	mnoho	k4c4	mnoho
řádů	řád	k1gInPc2	řád
větší	veliký	k2eAgFnSc4d2	veliký
energií	energie	k1gFnSc7	energie
než	než	k8xS	než
jaká	jaký	k3yQgFnSc1	jaký
je	být	k5eAaImIp3nS	být
dosažitelná	dosažitelný	k2eAgFnSc1d1	dosažitelná
na	na	k7c6	na
urychlovačích	urychlovač	k1gInPc6	urychlovač
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hypotetická	hypotetický	k2eAgFnSc1d1	hypotetická
gravitační	gravitační	k2eAgFnSc1d1	gravitační
astronomie	astronomie	k1gFnSc1	astronomie
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
pozorovat	pozorovat	k5eAaImF	pozorovat
gravitační	gravitační	k2eAgFnPc4d1	gravitační
vlny	vlna	k1gFnPc4	vlna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
převažujícím	převažující	k2eAgInSc7d1	převažující
způsobem	způsob	k1gInSc7	způsob
detekce	detekce	k1gFnSc2	detekce
velké	velký	k2eAgInPc1d1	velký
interferometry	interferometr	k1gInPc1	interferometr
<g/>
,	,	kIx,	,
nadějný	nadějný	k2eAgInSc1d1	nadějný
projekt	projekt	k1gInSc1	projekt
LIGO	liga	k1gFnSc5	liga
je	on	k3xPp3gMnPc4	on
ve	v	k7c6	v
stádiu	stádium	k1gNnSc6	stádium
ověřování	ověřování	k1gNnSc2	ověřování
<g/>
.	.	kIx.	.
</s>
<s>
Obecným	obecný	k2eAgInSc7d1	obecný
teoretickým	teoretický	k2eAgInSc7d1	teoretický
oborem	obor	k1gInSc7	obor
je	být	k5eAaImIp3nS	být
astrofyzika	astrofyzika	k1gFnSc1	astrofyzika
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
fyzikou	fyzika	k1gFnSc7	fyzika
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
mezihvězdné	mezihvězdný	k2eAgFnSc2d1	mezihvězdná
hmoty	hmota	k1gFnSc2	hmota
(	(	kIx(	(
<g/>
hustotou	hustota	k1gFnSc7	hustota
<g/>
,	,	kIx,	,
teplotou	teplota	k1gFnSc7	teplota
<g/>
,	,	kIx,	,
chemickým	chemický	k2eAgNnSc7d1	chemické
složením	složení	k1gNnSc7	složení
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kosmologie	kosmologie	k1gFnSc1	kosmologie
studuje	studovat	k5eAaImIp3nS	studovat
vesmír	vesmír	k1gInSc4	vesmír
jako	jako	k8xS	jako
celek	celek	k1gInSc4	celek
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
jeho	on	k3xPp3gInSc4	on
vznik	vznik	k1gInSc4	vznik
<g/>
,	,	kIx,	,
současný	současný	k2eAgInSc4d1	současný
a	a	k8xC	a
budoucí	budoucí	k2eAgInSc4d1	budoucí
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Astrobiologie	Astrobiologie	k1gFnSc1	Astrobiologie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
možnostmi	možnost	k1gFnPc7	možnost
existence	existence	k1gFnSc2	existence
života	život	k1gInSc2	život
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
astronomie	astronomie	k1gFnSc1	astronomie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Slunce	slunce	k1gNnSc2	slunce
<g/>
;	;	kIx,	;
výzkumem	výzkum	k1gInSc7	výzkum
prostorového	prostorový	k2eAgNnSc2d1	prostorové
rozložení	rozložení	k1gNnSc2	rozložení
a	a	k8xC	a
zákonitostmi	zákonitost	k1gFnPc7	zákonitost
pohybů	pohyb	k1gInPc2	pohyb
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
hvězdných	hvězdný	k2eAgFnPc2d1	hvězdná
soustav	soustava	k1gFnPc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
metody	metoda	k1gFnSc2	metoda
výzkumu	výzkum	k1gInSc2	výzkum
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c6	na
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
stelární	stelární	k2eAgFnSc4d1	stelární
statistiku	statistika	k1gFnSc4	statistika
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
stelární	stelární	k2eAgFnSc4d1	stelární
kinematiku	kinematika	k1gFnSc4	kinematika
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
stelární	stelární	k2eAgFnSc4d1	stelární
dynamiku	dynamika	k1gFnSc4	dynamika
Galaktická	galaktický	k2eAgFnSc1d1	Galaktická
astronomie	astronomie	k1gFnSc1	astronomie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
zkoumáním	zkoumání	k1gNnSc7	zkoumání
struktury	struktura	k1gFnSc2	struktura
<g/>
,	,	kIx,	,
součástí	součást	k1gFnPc2	součást
a	a	k8xC	a
vývoje	vývoj	k1gInSc2	vývoj
galaxií	galaxie	k1gFnPc2	galaxie
–	–	k?	–
v	v	k7c6	v
prvé	prvý	k4xOgFnSc6	prvý
řadě	řada	k1gFnSc6	řada
naší	náš	k3xOp1gFnSc2	náš
Galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Extragalaktická	extragalaktický	k2eAgFnSc1d1	extragalaktická
astronomie	astronomie	k1gFnSc1	astronomie
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
objekty	objekt	k1gInPc4	objekt
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
naší	náš	k3xOp1gFnSc2	náš
Galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Planetární	planetární	k2eAgFnPc1d1	planetární
vědy	věda	k1gFnPc1	věda
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
planety	planeta	k1gFnPc4	planeta
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Řadí	řadit	k5eAaImIp3nS	řadit
se	se	k3xPyFc4	se
do	do	k7c2	do
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnPc1	jejich
části	část	k1gFnPc1	část
mají	mít	k5eAaImIp3nP	mít
často	často	k6eAd1	často
užší	úzký	k2eAgFnSc4d2	užší
spojitost	spojitost	k1gFnSc4	spojitost
s	s	k7c7	s
odpovídajícími	odpovídající	k2eAgFnPc7d1	odpovídající
vědami	věda	k1gFnPc7	věda
o	o	k7c6	o
planetě	planeta	k1gFnSc6	planeta
Zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
geologie	geologie	k1gFnPc4	geologie
Marsu	Mars	k1gInSc2	Mars
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Meteorická	meteorický	k2eAgFnSc1d1	meteorická
astronomie	astronomie	k1gFnSc1	astronomie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
meteorů	meteor	k1gInPc2	meteor
a	a	k8xC	a
meteoritů	meteorit	k1gInPc2	meteorit
<g/>
.	.	kIx.	.
</s>
<s>
Astronomie	astronomie	k1gFnSc1	astronomie
má	mít	k5eAaImIp3nS	mít
nejužší	úzký	k2eAgInSc4d3	nejužší
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
fyzikou	fyzika	k1gFnSc7	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Astronomická	astronomický	k2eAgFnSc1d1	astronomická
teorie	teorie	k1gFnSc1	teorie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
fyzika	fyzik	k1gMnSc2	fyzik
astronomických	astronomický	k2eAgInPc2d1	astronomický
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
astronomické	astronomický	k2eAgInPc1d1	astronomický
systémy	systém	k1gInPc1	systém
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
teorie	teorie	k1gFnSc2	teorie
nejdůležitější	důležitý	k2eAgFnSc2d3	nejdůležitější
"	"	kIx"	"
<g/>
laboratoří	laboratoř	k1gFnPc2	laboratoř
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přirozeně	přirozeně	k6eAd1	přirozeně
především	především	k9	především
ve	v	k7c6	v
velkých	velký	k2eAgInPc6d1	velký
prostorových	prostorový	k2eAgInPc6d1	prostorový
a	a	k8xC	a
časových	časový	k2eAgNnPc6d1	časové
měřítkách	měřítko	k1gNnPc6	měřítko
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
gravitace	gravitace	k1gFnSc1	gravitace
a	a	k8xC	a
testuje	testovat	k5eAaImIp3nS	testovat
obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc1	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
extrémní	extrémní	k2eAgFnPc1d1	extrémní
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
zatím	zatím	k6eAd1	zatím
dosažitelné	dosažitelný	k2eAgFnPc1d1	dosažitelná
v	v	k7c6	v
laboratořích	laboratoř	k1gFnPc6	laboratoř
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
hustota	hustota	k1gFnSc1	hustota
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
vztah	vztah	k1gInSc1	vztah
má	mít	k5eAaImIp3nS	mít
astronomie	astronomie	k1gFnSc2	astronomie
i	i	k9	i
k	k	k7c3	k
religionistice	religionistika	k1gFnSc3	religionistika
<g/>
.	.	kIx.	.
</s>
