<s>
Astronomie	astronomie	k1gFnSc1	astronomie
<g/>
,	,	kIx,	,
řecky	řecky	k6eAd1	řecky
α	α	k?	α
z	z	k7c2	z
ά	ά	k?	ά
(	(	kIx(	(
<g/>
astron	astron	k1gMnSc1	astron
<g/>
)	)	kIx)	)
hvězda	hvězda	k1gFnSc1	hvězda
a	a	k8xC	a
ν	ν	k?	ν
(	(	kIx(	(
<g/>
nomos	nomos	k1gMnSc1	nomos
<g/>
)	)	kIx)	)
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
hvězdářství	hvězdářství	k1gNnSc4	hvězdářství
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
jevy	jev	k1gInPc4	jev
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
