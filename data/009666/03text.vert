<p>
<s>
Nachal	Nachal	k1gMnSc1	Nachal
Brit	Brit	k1gMnSc1	Brit
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
נ	נ	k?	נ
ב	ב	k?	ב
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vádí	vádí	k1gNnPc7	vádí
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
,	,	kIx,	,
v	v	k7c6	v
Dolní	dolní	k2eAgFnSc6d1	dolní
Galileji	Galilea	k1gFnSc6	Galilea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
přes	přes	k7c4	přes
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
cca	cca	kA	cca
1	[number]	k4	1
kilometr	kilometr	k1gInSc1	kilometr
západně	západně	k6eAd1	západně
od	od	k7c2	od
vesnice	vesnice	k1gFnSc2	vesnice
Tajbe	Tajb	k1gInSc5	Tajb
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
náhorní	náhorní	k2eAgFnSc6d1	náhorní
planině	planina	k1gFnSc6	planina
Ramot	Ramot	k1gMnSc1	Ramot
Jisachar	Jisachar	k1gMnSc1	Jisachar
<g/>
.	.	kIx.	.
</s>
<s>
Vádí	vádí	k1gNnSc1	vádí
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
jihovýchodu	jihovýchod	k1gInSc3	jihovýchod
plochou	plocha	k1gFnSc7	plocha
<g/>
,	,	kIx,	,
zemědělsky	zemědělsky	k6eAd1	zemědělsky
využívanou	využívaný	k2eAgFnSc7d1	využívaná
krajinou	krajina	k1gFnSc7	krajina
a	a	k8xC	a
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
okraji	okraj	k1gInSc6	okraj
vesnice	vesnice	k1gFnSc2	vesnice
Moledet	Moledeta	k1gFnPc2	Moledeta
ústí	ústit	k5eAaImIp3nS	ústit
zprava	zprava	k6eAd1	zprava
do	do	k7c2	do
vádí	vádí	k1gNnSc2	vádí
Nachal	Nachal	k1gMnSc1	Nachal
Jisachar	Jisachar	k1gMnSc1	Jisachar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Ramot	Ramot	k1gMnSc1	Ramot
Jisachar	Jisachar	k1gMnSc1	Jisachar
</s>
</p>
