<s>
Jan	Jan	k1gMnSc1	Jan
Kraus	Kraus	k1gMnSc1	Kraus
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1953	[number]	k4	1953
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
moderátor	moderátor	k1gMnSc1	moderátor
<g/>
,	,	kIx,	,
publicista	publicista	k1gMnSc1	publicista
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
<g/>
.	.	kIx.	.
</s>
<s>
Moderoval	moderovat	k5eAaBmAgInS	moderovat
populární	populární	k2eAgInSc1d1	populární
pořad	pořad	k1gInSc1	pořad
Uvolněte	uvolnit	k5eAaPmRp2nP	uvolnit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
prosím	prosit	k5eAaImIp1nS	prosit
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
obdobu	obdoba	k1gFnSc4	obdoba
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Show	show	k1gFnSc2	show
Jana	Jan	k1gMnSc2	Jan
Krause	Kraus	k1gMnSc2	Kraus
uvádí	uvádět	k5eAaImIp3nS	uvádět
na	na	k7c4	na
Prima	prima	k2eAgFnSc4d1	prima
TV	TV	kA	TV
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2009	[number]	k4	2009
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
předsedy	předseda	k1gMnSc2	předseda
Českého	český	k2eAgInSc2d1	český
filmového	filmový	k2eAgInSc2d1	filmový
a	a	k8xC	a
televizního	televizní	k2eAgInSc2d1	televizní
svazu	svaz	k1gInSc2	svaz
(	(	kIx(	(
<g/>
FITES	FITES	kA	FITES
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
byla	být	k5eAaImAgFnS	být
obava	obava	k1gFnSc1	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInPc1	jeho
osobní	osobní	k2eAgInPc1d1	osobní
spory	spor	k1gInPc1	spor
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
televizí	televize	k1gFnSc7	televize
negativně	negativně	k6eAd1	negativně
přenesou	přenést	k5eAaPmIp3nP	přenést
i	i	k9	i
na	na	k7c4	na
filmový	filmový	k2eAgInSc4d1	filmový
svaz	svaz	k1gInSc4	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
ke	k	k7c3	k
spoluzřizovatelům	spoluzřizovatel	k1gMnPc3	spoluzřizovatel
Nadačního	nadační	k2eAgInSc2d1	nadační
fondu	fond	k1gInSc2	fond
proti	proti	k7c3	proti
korupci	korupce	k1gFnSc3	korupce
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
v	v	k7c6	v
česko-židovské	česko-židovský	k2eAgFnSc6d1	česko-židovská
rodině	rodina	k1gFnSc6	rodina
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Ota	Ota	k1gMnSc1	Ota
Kraus	Kraus	k1gMnSc1	Kraus
přežil	přežít	k5eAaPmAgMnS	přežít
holocaust	holocaust	k1gInSc4	holocaust
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
herečka	herečka	k1gFnSc1	herečka
Jana	Jana	k1gFnSc1	Jana
Krausová	Krausová	k1gFnSc1	Krausová
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozchodu	rozchod	k1gInSc6	rozchod
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
v	v	k7c4	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
žije	žít	k5eAaImIp3nS	žít
s	s	k7c7	s
herečkou	herečka	k1gFnSc7	herečka
Ivanou	Ivana	k1gFnSc7	Ivana
Chýlkovou	Chýlková	k1gFnSc7	Chýlková
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
otcem	otec	k1gMnSc7	otec
čtyř	čtyři	k4xCgFnPc2	čtyři
dětí	dítě	k1gFnPc2	dítě
-	-	kIx~	-
Marka	Marek	k1gMnSc2	Marek
Blažka	Blažek	k1gMnSc2	Blažek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěváka	zpěvák	k1gMnSc2	zpěvák
Davida	David	k1gMnSc2	David
Krause	Kraus	k1gMnSc2	Kraus
(	(	kIx(	(
<g/>
*	*	kIx~	*
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Adama	Adam	k1gMnSc4	Adam
Krause	Kraus	k1gMnSc4	Kraus
(	(	kIx(	(
<g/>
*	*	kIx~	*
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jáchyma	Jáchym	k1gMnSc2	Jáchym
Krause	Kraus	k1gMnSc2	Kraus
(	(	kIx(	(
<g/>
*	*	kIx~	*
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
s	s	k7c7	s
I.	I.	kA	I.
Chýlkovou	Chýlková	k1gFnSc7	Chýlková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
bratrem	bratr	k1gMnSc7	bratr
spisovatele	spisovatel	k1gMnSc2	spisovatel
a	a	k8xC	a
herce	herec	k1gMnSc2	herec
Ivana	Ivan	k1gMnSc2	Ivan
Krause	Kraus	k1gMnSc2	Kraus
(	(	kIx(	(
<g/>
*	*	kIx~	*
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
-	-	kIx~	-
žijícího	žijící	k2eAgInSc2d1	žijící
střídavě	střídavě	k6eAd1	střídavě
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
dalšími	další	k2eAgMnPc7d1	další
sourozenci	sourozenec	k1gMnPc1	sourozenec
jsou	být	k5eAaImIp3nP	být
PhDr.	PhDr.	kA	PhDr.
Eliška	Eliška	k1gFnSc1	Eliška
Krausová-Chavez	Krausová-Chavez	k1gMnSc1	Krausová-Chavez
(	(	kIx(	(
<g/>
*	*	kIx~	*
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
-	-	kIx~	-
působí	působit	k5eAaImIp3nS	působit
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Bogotě	Bogota	k1gFnSc6	Bogota
<g/>
,	,	kIx,	,
PhDr.	PhDr.	kA	PhDr.
Michael	Michael	k1gMnSc1	Michael
Kraus	Kraus	k1gMnSc1	Kraus
(	(	kIx(	(
<g/>
*	*	kIx~	*
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
-	-	kIx~	-
profesor	profesor	k1gMnSc1	profesor
politologie	politologie	k1gFnSc2	politologie
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c4	v
Middlebury	Middlebur	k1gInPc4	Middlebur
ve	v	k7c6	v
Vermontu	Vermont	k1gInSc6	Vermont
<g/>
,	,	kIx,	,
Kateřina	Kateřina	k1gFnSc1	Kateřina
Krausová	Krausová	k1gFnSc1	Krausová
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
-	-	kIx~	-
pracovala	pracovat	k5eAaImAgFnS	pracovat
v	v	k7c6	v
knihovně	knihovna	k1gFnSc6	knihovna
Princetonské	Princetonský	k2eAgFnSc2d1	Princetonská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Kraus	Kraus	k1gMnSc1	Kraus
Jana	Jana	k1gFnSc1	Jana
Krausová	Krausová	k1gFnSc1	Krausová
(	(	kIx(	(
<g/>
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Pehrová	Pehrová	k1gFnSc1	Pehrová
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
-	-	kIx~	-
první	první	k4xOgFnSc1	první
manželka	manželka	k1gFnSc1	manželka
David	David	k1gMnSc1	David
Kraus	Kraus	k1gMnSc1	Kraus
(	(	kIx(	(
<g/>
*	*	kIx~	*
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
Adam	Adam	k1gMnSc1	Adam
Kraus	Kraus	k1gMnSc1	Kraus
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
Ivana	Ivana	k1gFnSc1	Ivana
Chýlková	Chýlková	k1gFnSc1	Chýlková
(	(	kIx(	(
<g/>
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
-	-	kIx~	-
současná	současný	k2eAgFnSc1d1	současná
partnerka	partnerka	k1gFnSc1	partnerka
Jáchym	Jáchym	k1gMnSc1	Jáchym
Kraus	Kraus	k1gMnSc1	Kraus
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
hrál	hrát	k5eAaImAgInS	hrát
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
filmech	film	k1gInPc6	film
<g/>
.	.	kIx.	.
</s>
<s>
Debutoval	debutovat	k5eAaBmAgMnS	debutovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
v	v	k7c6	v
málo	málo	k6eAd1	málo
známém	známý	k2eAgInSc6d1	známý
filmu	film	k1gInSc6	film
Dva	dva	k4xCgMnPc1	dva
tygři	tygr	k1gMnPc1	tygr
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
hrál	hrát	k5eAaImAgInS	hrát
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
60	[number]	k4	60
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
nejznámější	známý	k2eAgMnSc1d3	nejznámější
jsou	být	k5eAaImIp3nP	být
Lišáci	lišák	k1gMnPc1	lišák
<g/>
,	,	kIx,	,
Myšáci	myšák	k1gMnPc1	myšák
a	a	k8xC	a
Šibeničák	Šibeničák	k1gMnSc1	Šibeničák
<g/>
,	,	kIx,	,
Spalovač	spalovač	k1gMnSc1	spalovač
mrtvol	mrtvola	k1gFnPc2	mrtvola
<g/>
,	,	kIx,	,
Dívka	dívka	k1gFnSc1	dívka
na	na	k7c6	na
koštěti	koště	k1gNnSc6	koště
<g/>
,	,	kIx,	,
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
budí	budit	k5eAaImIp3nP	budit
princezny	princezna	k1gFnPc1	princezna
<g/>
,	,	kIx,	,
Černí	černý	k2eAgMnPc1d1	černý
baroni	baron	k1gMnPc1	baron
<g/>
,	,	kIx,	,
Gympl	gympl	k1gInSc1	gympl
<g/>
,	,	kIx,	,
Drahé	drahý	k2eAgFnSc2d1	drahá
tety	teta	k1gFnSc2	teta
a	a	k8xC	a
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
Copak	copak	k3yQnSc1	copak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
vojáka	voják	k1gMnSc4	voják
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Pravidelně	pravidelně	k6eAd1	pravidelně
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Kraus	Kraus	k1gMnSc1	Kraus
a	a	k8xC	a
blondýna	blondýna	k1gFnSc1	blondýna
na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
Frekvence	frekvence	k1gFnSc2	frekvence
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Satirická	satirický	k2eAgNnPc4d1	satirické
prohlášení	prohlášení	k1gNnPc4	prohlášení
komentující	komentující	k2eAgFnSc2d1	komentující
moravské	moravský	k2eAgFnSc2d1	Moravská
záležitosti	záležitost	k1gFnSc2	záležitost
bude	být	k5eAaImBp3nS	být
řešit	řešit	k5eAaImF	řešit
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
1966	[number]	k4	1966
-	-	kIx~	-
Dva	dva	k4xCgMnPc1	dva
tygři	tygr	k1gMnPc1	tygr
1967	[number]	k4	1967
-	-	kIx~	-
Martin	Martin	k1gMnSc1	Martin
a	a	k8xC	a
devět	devět	k4xCc4	devět
bláznů	blázen	k1gMnPc2	blázen
<g/>
,	,	kIx,	,
Noc	noc	k1gFnSc1	noc
nevěsty	nevěsta	k1gFnSc2	nevěsta
1968	[number]	k4	1968
-	-	kIx~	-
Na	na	k7c6	na
Žižkově	Žižkův	k2eAgInSc6d1	Žižkův
válečném	válečný	k2eAgInSc6d1	válečný
voze	vůz	k1gInSc6	vůz
<g/>
,	,	kIx,	,
Spalovač	spalovač	k1gMnSc1	spalovač
mrtvol	mrtvola	k1gFnPc2	mrtvola
1969	[number]	k4	1969
-	-	kIx~	-
Záhada	záhada	k1gFnSc1	záhada
hlavolamu	hlavolam	k1gInSc2	hlavolam
1970	[number]	k4	1970
-	-	kIx~	-
Lišáci	lišák	k1gMnPc1	lišák
<g/>
,	,	kIx,	,
Myšáci	myšák	k1gMnPc1	myšák
a	a	k8xC	a
Šibeničák	Šibeničák	k1gInSc1	Šibeničák
1971	[number]	k4	1971
-	-	kIx~	-
Slaměný	slaměný	k2eAgInSc1d1	slaměný
klobouk	klobouk	k1gInSc1	klobouk
<g />
.	.	kIx.	.
</s>
<s>
1972	[number]	k4	1972
-	-	kIx~	-
Dívka	dívka	k1gFnSc1	dívka
na	na	k7c6	na
koštěti	koště	k1gNnSc6	koště
1973	[number]	k4	1973
-	-	kIx~	-
Láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
30	[number]	k4	30
panen	panna	k1gFnPc2	panna
a	a	k8xC	a
Pythagoras	Pythagoras	k1gMnSc1	Pythagoras
<g/>
,	,	kIx,	,
Tajemství	tajemství	k1gNnSc1	tajemství
zlatého	zlatý	k2eAgMnSc2d1	zlatý
Buddhy	Buddha	k1gMnSc2	Buddha
1974	[number]	k4	1974
-	-	kIx~	-
Kvočny	kvočna	k1gFnSc2	kvočna
a	a	k8xC	a
král	král	k1gMnSc1	král
<g/>
,	,	kIx,	,
Poslední	poslední	k2eAgInSc1d1	poslední
ples	ples	k1gInSc1	ples
na	na	k7c6	na
rožnovské	rožnovský	k2eAgFnSc6d1	Rožnovská
plovárně	plovárna	k1gFnSc6	plovárna
<g/>
,	,	kIx,	,
Drahé	drahá	k1gFnSc6	drahá
tety	teta	k1gFnSc2	teta
a	a	k8xC	a
já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
Na	na	k7c6	na
startu	start	k1gInSc6	start
je	být	k5eAaImIp3nS	být
delfín	delfín	k1gMnSc1	delfín
1975	[number]	k4	1975
-	-	kIx~	-
Dva	dva	k4xCgMnPc1	dva
muži	muž	k1gMnPc1	muž
hlásí	hlásit	k5eAaImIp3nP	hlásit
příchod	příchod	k1gInSc4	příchod
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Pomerančový	pomerančový	k2eAgMnSc1d1	pomerančový
kluk	kluk	k1gMnSc1	kluk
<g/>
,	,	kIx,	,
Tam	tam	k6eAd1	tam
kde	kde	k6eAd1	kde
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
čápi	čáp	k1gMnPc1	čáp
1976	[number]	k4	1976
-	-	kIx~	-
Bouřlivé	bouřlivý	k2eAgNnSc1d1	bouřlivé
víno	víno	k1gNnSc1	víno
<g/>
,	,	kIx,	,
Odysseus	Odysseus	k1gInSc1	Odysseus
a	a	k8xC	a
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
Boty	bota	k1gFnPc1	bota
plné	plný	k2eAgFnSc2d1	plná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
Osvobození	osvobození	k1gNnSc2	osvobození
Prahy	Praha	k1gFnSc2	Praha
1977	[number]	k4	1977
-	-	kIx~	-
Což	což	k3yRnSc1	což
takhle	takhle	k6eAd1	takhle
dát	dát	k5eAaPmF	dát
si	se	k3xPyFc3	se
špenát	špenát	k1gInSc4	špenát
<g/>
,	,	kIx,	,
Šestapadesát	šestapadesát	k4xCc1	šestapadesát
neomluvených	omluvený	k2eNgFnPc2d1	neomluvená
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
Jak	jak	k8xS	jak
se	se	k3xPyFc4	se
budí	budit	k5eAaImIp3nS	budit
princezny	princezna	k1gFnSc2	princezna
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
točí	točit	k5eAaImIp3nS	točit
Rozmarýny	rozmarýna	k1gFnSc2	rozmarýna
1978	[number]	k4	1978
-	-	kIx~	-
Muž	muž	k1gMnSc1	muž
s	s	k7c7	s
orlem	orel	k1gMnSc7	orel
a	a	k8xC	a
slepicí	slepice	k1gFnSc7	slepice
1979	[number]	k4	1979
-	-	kIx~	-
Arabela	Arabela	k1gFnSc1	Arabela
<g/>
,	,	kIx,	,
Poprask	poprask	k1gInSc1	poprask
na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
E	E	kA	E
4	[number]	k4	4
<g/>
,	,	kIx,	,
Drsná	drsný	k2eAgFnSc1d1	drsná
Planina	planina	k1gFnSc1	planina
1980	[number]	k4	1980
-	-	kIx~	-
Co	co	k3yRnSc1	co
je	být	k5eAaImIp3nS	být
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
<g/>
,	,	kIx,	,
pánové	pán	k1gMnPc1	pán
<g/>
,	,	kIx,	,
Půl	půl	k6eAd1	půl
domu	dům	k1gInSc2	dům
bez	bez	k7c2	bez
ženicha	ženich	k1gMnSc2	ženich
<g/>
,	,	kIx,	,
Evžen	Evžen	k1gMnSc1	Evžen
mezi	mezi	k7c7	mezi
<g />
.	.	kIx.	.
</s>
<s>
námi	my	k3xPp1nPc7	my
<g/>
,	,	kIx,	,
Kaňka	Kaňka	k1gMnSc1	Kaňka
do	do	k7c2	do
pohádky	pohádka	k1gFnSc2	pohádka
1981	[number]	k4	1981
-	-	kIx~	-
Krakonoš	Krakonoš	k1gMnSc1	Krakonoš
a	a	k8xC	a
lyžníci	lyžník	k1gMnPc1	lyžník
<g/>
,	,	kIx,	,
Hodina	hodina	k1gFnSc1	hodina
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
Zralé	zralý	k2eAgNnSc1d1	zralé
víno	víno	k1gNnSc1	víno
<g/>
,	,	kIx,	,
Křtiny	křtiny	k1gFnPc1	křtiny
1984	[number]	k4	1984
-	-	kIx~	-
Všechno	všechen	k3xTgNnSc1	všechen
nebo	nebo	k8xC	nebo
nic	nic	k6eAd1	nic
1985	[number]	k4	1985
-	-	kIx~	-
Čarovné	čarovný	k2eAgNnSc1d1	čarovné
dědictví	dědictví	k1gNnSc1	dědictví
<g/>
,	,	kIx,	,
Tvá	tvůj	k3xOp2gFnSc1	tvůj
sestra	sestra	k1gFnSc1	sestra
je	být	k5eAaImIp3nS	být
vlkodlak	vlkodlak	k1gMnSc1	vlkodlak
(	(	kIx(	(
<g/>
Howling	Howling	k1gInSc1	Howling
II	II	kA	II
<g/>
:	:	kIx,	:
Your	Your	k1gMnSc1	Your
Sister	Sister	k1gMnSc1	Sister
Is	Is	k1gMnSc1	Is
a	a	k8xC	a
Werewolf	Werewolf	k1gMnSc1	Werewolf
<g/>
)	)	kIx)	)
1986	[number]	k4	1986
-	-	kIx~	-
Cena	cena	k1gFnSc1	cena
medu	med	k1gInSc2	med
<g/>
,	,	kIx,	,
Copak	copak	k3yQnSc1	copak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
za	za	k7c2	za
vojáka	voják	k1gMnSc2	voják
1987	[number]	k4	1987
-	-	kIx~	-
Mág	mág	k1gMnSc1	mág
<g/>
,	,	kIx,	,
Proč	proč	k6eAd1	proč
<g/>
?	?	kIx.	?
</s>
<s>
1988	[number]	k4	1988
-	-	kIx~	-
Pan	Pan	k1gMnSc1	Pan
Tau	tau	k1gNnSc4	tau
1989	[number]	k4	1989
-	-	kIx~	-
Příběh	příběh	k1gInSc1	příběh
88	[number]	k4	88
<g/>
,	,	kIx,	,
Cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
Jihozápad	jihozápad	k1gInSc4	jihozápad
1992	[number]	k4	1992
-	-	kIx~	-
Černí	černý	k2eAgMnPc1d1	černý
baroni	baron	k1gMnPc1	baron
<g/>
,	,	kIx,	,
Jídlo	jídlo	k1gNnSc1	jídlo
1993	[number]	k4	1993
-	-	kIx~	-
Mistr	mistr	k1gMnSc1	mistr
Kampanus	Kampanus	k1gMnSc1	Kampanus
<g/>
,	,	kIx,	,
Lekce	lekce	k1gFnSc1	lekce
Faust	Faust	k1gFnSc1	Faust
1994	[number]	k4	1994
-	-	kIx~	-
Douwe	Douw	k1gInSc2	Douw
Egberts	Egberts	k1gInSc1	Egberts
<g/>
,	,	kIx,	,
Ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgMnSc1d2	veliký
blbec	blbec	k1gMnSc1	blbec
<g/>
,	,	kIx,	,
než	než	k8xS	než
jsme	být	k5eAaImIp1nP	být
doufali	doufat	k5eAaImAgMnP	doufat
1995	[number]	k4	1995
-	-	kIx~	-
Trio	trio	k1gNnSc1	trio
1998	[number]	k4	1998
-	-	kIx~	-
Hanele	Hanel	k1gInSc2	Hanel
1999	[number]	k4	1999
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
očima	oko	k1gNnPc7	oko
<g/>
,	,	kIx,	,
Absolutní	absolutní	k2eAgFnSc1d1	absolutní
láska	láska	k1gFnSc1	láska
-	-	kIx~	-
muž	muž	k1gMnSc1	muž
z	z	k7c2	z
dvojice	dvojice	k1gFnSc2	dvojice
v	v	k7c6	v
metru	metro	k1gNnSc6	metro
2001	[number]	k4	2001
-	-	kIx~	-
Zdivočelá	zdivočelý	k2eAgFnSc1d1	Zdivočelá
země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
Královský	královský	k2eAgInSc1d1	královský
slib	slib	k1gInSc1	slib
<g/>
,	,	kIx,	,
Mach	Mach	k1gMnSc1	Mach
<g/>
,	,	kIx,	,
Šebestová	Šebestová	k1gFnSc1	Šebestová
a	a	k8xC	a
kouzelné	kouzelný	k2eAgNnSc1d1	kouzelné
sluchátko	sluchátko	k1gNnSc1	sluchátko
2002	[number]	k4	2002
-	-	kIx~	-
Musím	muset	k5eAaImIp1nS	muset
tě	ty	k3xPp2nSc4	ty
svést	svést	k5eAaPmF	svést
2003	[number]	k4	2003
-	-	kIx~	-
Mazaný	mazaný	k2eAgMnSc1d1	mazaný
Filip	Filip	k1gMnSc1	Filip
<g/>
,	,	kIx,	,
Městečko	městečko	k1gNnSc1	městečko
2005	[number]	k4	2005
-	-	kIx~	-
On	on	k3xPp3gMnSc1	on
je	být	k5eAaImIp3nS	být
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
Skřítek	skřítek	k1gMnSc1	skřítek
2007	[number]	k4	2007
-	-	kIx~	-
Gympl	gympl	k1gInSc1	gympl
2009	[number]	k4	2009
-	-	kIx~	-
3	[number]	k4	3
sezóny	sezóna	k1gFnSc2	sezóna
v	v	k7c6	v
pekle	peklo	k1gNnSc6	peklo
2014	[number]	k4	2014
-	-	kIx~	-
Vejška	Vejška	k?	Vejška
2015	[number]	k4	2015
-	-	kIx~	-
Wilsonov	Wilsonov	k1gInSc1	Wilsonov
</s>
