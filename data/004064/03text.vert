<s>
Kiwi	kiwi	k1gNnSc1	kiwi
je	být	k5eAaImIp3nS	být
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
,	,	kIx,	,
plod	plod	k1gInSc4	plod
popínavých	popínavý	k2eAgFnPc2d1	popínavá
dřevin	dřevina	k1gFnPc2	dřevina
rodu	rod	k1gInSc2	rod
aktinidie	aktinidie	k1gFnSc2	aktinidie
<g/>
,	,	kIx,	,
pocházejících	pocházející	k2eAgFnPc2d1	pocházející
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Plod	plod	k1gInSc1	plod
druhu	druh	k1gInSc2	druh
aktinidie	aktinidie	k1gFnSc2	aktinidie
lahodná	lahodný	k2eAgFnSc1d1	lahodná
(	(	kIx(	(
<g/>
Actinidia	Actinidium	k1gNnSc2	Actinidium
deliciosa	deliciosa	k1gFnSc1	deliciosa
<g/>
)	)	kIx)	)
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
vitamínu	vitamín	k1gInSc2	vitamín
C	C	kA	C
<g/>
.	.	kIx.	.
</s>
<s>
Velikostí	velikost	k1gFnSc7	velikost
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
slepičímu	slepičí	k2eAgNnSc3d1	slepičí
vejci	vejce	k1gNnSc3	vejce
<g/>
,	,	kIx,	,
slupka	slupka	k1gFnSc1	slupka
je	být	k5eAaImIp3nS	být
hnědá	hnědý	k2eAgFnSc1d1	hnědá
a	a	k8xC	a
chlupatá	chlupatý	k2eAgFnSc1d1	chlupatá
<g/>
,	,	kIx,	,
dužina	dužina	k1gFnSc1	dužina
obvykle	obvykle	k6eAd1	obvykle
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
stromku	stromek	k1gInSc2	stromek
se	se	k3xPyFc4	se
sklidí	sklidit	k5eAaPmIp3nS	sklidit
až	až	k9	až
200	[number]	k4	200
kg	kg	kA	kg
plodů	plod	k1gInPc2	plod
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Předkem	předek	k1gMnSc7	předek
dnešního	dnešní	k2eAgNnSc2d1	dnešní
kiwi	kiwi	k1gNnSc2	kiwi
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
je	on	k3xPp3gMnPc4	on
Actinidia	Actinidium	k1gNnPc1	Actinidium
deliciosa	deliciosa	k1gFnSc1	deliciosa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
původně	původně	k6eAd1	původně
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Jiný	jiný	k2eAgMnSc1d1	jiný
předek	předek	k1gMnSc1	předek
kiwi	kiwi	k1gNnSc2	kiwi
<g/>
,	,	kIx,	,
také	také	k9	také
Actinidia	Actinidium	k1gNnPc1	Actinidium
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
nalezen	nalézt	k5eAaBmNgInS	nalézt
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Kiwi	kiwi	k1gNnSc4	kiwi
zde	zde	k6eAd1	zde
měla	mít	k5eAaImAgFnS	mít
různé	různý	k2eAgInPc4d1	různý
názvy	název	k1gInPc4	název
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
"	"	kIx"	"
<g/>
jang-tao	jangao	k6eAd1	jang-tao
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
znaky	znak	k1gInPc4	znak
羊	羊	k?	羊
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
začala	začít	k5eAaPmAgNnP	začít
cestovat	cestovat	k5eAaImF	cestovat
světem	svět	k1gInSc7	svět
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
a	a	k8xC	a
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
kiwi	kiwi	k1gNnPc4	kiwi
"	"	kIx"	"
<g/>
představena	představit	k5eAaPmNgFnS	představit
<g/>
"	"	kIx"	"
Novému	nový	k2eAgInSc3d1	nový
Zélandu	Zéland	k1gInSc3	Zéland
a	a	k8xC	a
také	také	k9	také
zasazena	zasazen	k2eAgFnSc1d1	zasazena
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
kiwi	kiwi	k1gNnPc4	kiwi
ochutnali	ochutnat	k5eAaPmAgMnP	ochutnat
<g/>
,	,	kIx,	,
mysleli	myslet	k5eAaImAgMnP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
chutná	chutnat	k5eAaImIp3nS	chutnat
po	po	k7c6	po
angreštu	angrešt	k1gInSc6	angrešt
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jej	on	k3xPp3gMnSc4	on
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
také	také	k9	také
čínský	čínský	k2eAgInSc4d1	čínský
angrešt	angrešt	k1gInSc4	angrešt
či	či	k8xC	či
angrešt	angrešt	k1gInSc4	angrešt
Číňana	Číňan	k1gMnSc2	Číňan
<g/>
.	.	kIx.	.
</s>
<s>
Kiwi	kiwi	k1gNnSc1	kiwi
však	však	k9	však
s	s	k7c7	s
angreštem	angrešt	k1gInSc7	angrešt
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
rodinou	rodina	k1gFnSc7	rodina
nemá	mít	k5eNaImIp3nS	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
rozmohlo	rozmoct	k5eAaPmAgNnS	rozmoct
domácí	domácí	k2eAgNnSc1d1	domácí
pěstování	pěstování	k1gNnSc1	pěstování
kiwi	kiwi	k1gNnSc2	kiwi
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
již	již	k6eAd1	již
přešlo	přejít	k5eAaPmAgNnS	přejít
na	na	k7c4	na
komerční	komerční	k2eAgNnSc4d1	komerční
pěstování	pěstování	k1gNnSc4	pěstování
a	a	k8xC	a
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
držel	držet	k5eAaImAgInS	držet
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
prvenství	prvenství	k1gNnSc2	prvenství
ve	v	k7c6	v
vývozu	vývoz	k1gInSc6	vývoz
kiwi	kiwi	k1gNnSc2	kiwi
(	(	kIx(	(
<g/>
12	[number]	k4	12
t	t	k?	t
<g/>
/	/	kIx~	/
<g/>
ha	ha	kA	ha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
vedoucím	vedoucí	k2eAgMnSc7d1	vedoucí
producentem	producent	k1gMnSc7	producent
kiwi	kiwi	k1gNnSc2	kiwi
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
drží	držet	k5eAaImIp3nS	držet
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
,	,	kIx,	,
Chile	Chile	k1gNnSc1	Chile
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Vyšlechtěné	vyšlechtěný	k2eAgFnPc1d1	vyšlechtěná
velkoplodé	velkoplodý	k2eAgFnPc1d1	velkoplodá
odrůdy	odrůda	k1gFnPc1	odrůda
kiwi	kiwi	k1gNnSc2	kiwi
ale	ale	k8xC	ale
mají	mít	k5eAaImIp3nP	mít
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
pochází	pocházet	k5eAaImIp3nS	pocházet
jejich	jejich	k3xOp3gInSc1	jejich
obchodní	obchodní	k2eAgInSc1d1	obchodní
název	název	k1gInSc1	název
<g/>
.	.	kIx.	.
</s>
<s>
Kiwi	kiwi	k1gNnSc1	kiwi
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pěstováno	pěstovat	k5eAaImNgNnS	pěstovat
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
mírných	mírný	k2eAgNnPc6d1	mírné
klimatech	klima	k1gNnPc6	klima
s	s	k7c7	s
adekvátním	adekvátní	k2eAgNnSc7d1	adekvátní
letním	letní	k2eAgNnSc7d1	letní
teplem	teplo	k1gNnSc7	teplo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
se	se	k3xPyFc4	se
dováží	dovázat	k5eAaPmIp3nP	dovázat
více	hodně	k6eAd2	hodně
kiwi	kiwi	k1gNnSc4	kiwi
italského	italský	k2eAgInSc2d1	italský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
levnější	levný	k2eAgFnPc1d2	levnější
<g/>
,	,	kIx,	,
dostupnější	dostupný	k2eAgFnPc1d2	dostupnější
a	a	k8xC	a
dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
lépe	dobře	k6eAd2	dobře
skladovat	skladovat	k5eAaImF	skladovat
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
dovoz	dovoz	k1gInSc4	dovoz
z	z	k7c2	z
nepříliš	příliš	k6eNd1	příliš
vzdálené	vzdálený	k2eAgFnSc2d1	vzdálená
Itálie	Itálie	k1gFnSc2	Itálie
ekologicky	ekologicky	k6eAd1	ekologicky
účelnější	účelný	k2eAgMnSc1d2	účelnější
než	než	k8xS	než
ze	z	k7c2	z
zámoří	zámoří	k1gNnSc2	zámoří
<g/>
.	.	kIx.	.
</s>
<s>
Kiwi	kiwi	k1gNnSc1	kiwi
dostalo	dostat	k5eAaPmAgNnS	dostat
svůj	svůj	k3xOyFgInSc4	svůj
nový	nový	k2eAgInSc4d1	nový
název	název	k1gInSc4	název
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
čínský	čínský	k2eAgInSc4d1	čínský
angrešt	angrešt	k1gInSc4	angrešt
<g/>
,	,	kIx,	,
angrešt	angrešt	k1gInSc4	angrešt
Číňana	Číňan	k1gMnSc4	Číňan
<g/>
)	)	kIx)	)
hlavně	hlavně	k9	hlavně
kvůli	kvůli	k7c3	kvůli
studené	studený	k2eAgFnSc3d1	studená
válce	válka	k1gFnSc3	válka
a	a	k8xC	a
také	také	k9	také
z	z	k7c2	z
nové	nový	k2eAgFnSc2d1	nová
marketingové	marketingový	k2eAgFnSc2d1	marketingová
strategie	strategie	k1gFnSc2	strategie
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
dostalo	dostat	k5eAaPmAgNnS	dostat
ovoce	ovoce	k1gNnSc1	ovoce
podle	podle	k7c2	podle
národního	národní	k2eAgMnSc2d1	národní
ptáka	pták	k1gMnSc2	pták
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
-	-	kIx~	-
kivi	kivi	k1gMnSc1	kivi
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
vzhledově	vzhledově	k6eAd1	vzhledově
poněkud	poněkud	k6eAd1	poněkud
připomíná	připomínat	k5eAaImIp3nS	připomínat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
se	se	k3xPyFc4	se
jméno	jméno	k1gNnSc1	jméno
stalo	stát	k5eAaPmAgNnS	stát
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
obchodním	obchodní	k2eAgNnSc7d1	obchodní
jménem	jméno	k1gNnSc7	jméno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
kvůli	kvůli	k7c3	kvůli
zmatkům	zmatek	k1gInPc3	zmatek
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
při	při	k7c6	při
vyslovení	vyslovení	k1gNnSc6	vyslovení
"	"	kIx"	"
<g/>
kiwi	kiwi	k1gNnSc2	kiwi
<g/>
"	"	kIx"	"
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
ovoce	ovoce	k1gNnSc4	ovoce
nebo	nebo	k8xC	nebo
o	o	k7c4	o
ptáka	pták	k1gMnSc4	pták
<g/>
,	,	kIx,	,
prodává	prodávat	k5eAaImIp3nS	prodávat
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
kiwi	kiwi	k1gNnSc2	kiwi
pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
Zespri	Zespr	k1gFnSc2	Zespr
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
také	také	k9	také
ochranná	ochranný	k2eAgFnSc1d1	ochranná
známka	známka	k1gFnSc1	známka
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
značkování	značkování	k1gNnSc2	značkování
také	také	k9	také
poslouží	posloužit	k5eAaPmIp3nS	posloužit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
kiwi	kiwi	k1gNnSc7	kiwi
z	z	k7c2	z
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
od	od	k7c2	od
ovoce	ovoce	k1gNnSc2	ovoce
plozeného	plozený	k2eAgInSc2d1	plozený
jinými	jiný	k2eAgFnPc7d1	jiná
zeměmi	zem	k1gFnPc7	zem
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mnohdy	mnohdy	k6eAd1	mnohdy
profitují	profitovat	k5eAaBmIp3nP	profitovat
ze	z	k7c2	z
jména	jméno	k1gNnSc2	jméno
"	"	kIx"	"
<g/>
Kiwi	kiwi	k1gNnSc2	kiwi
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgNnPc1d1	tradiční
zelená	zelený	k2eAgNnPc1d1	zelené
kiwi	kiwi	k1gNnPc1	kiwi
jsou	být	k5eAaImIp3nP	být
oválná	oválný	k2eAgNnPc1d1	oválné
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
je	on	k3xPp3gNnSc4	on
přiblížit	přiblížit	k5eAaPmF	přiblížit
zhruba	zhruba	k6eAd1	zhruba
slepičímu	slepičí	k2eAgNnSc3d1	slepičí
vejci	vejce	k1gNnSc3	vejce
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
kiwi	kiwi	k1gNnPc1	kiwi
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
špičatá	špičatý	k2eAgNnPc1d1	špičaté
<g/>
.	.	kIx.	.
</s>
<s>
Kiwi	kiwi	k1gNnSc1	kiwi
má	mít	k5eAaImIp3nS	mít
hnědou	hnědý	k2eAgFnSc4d1	hnědá
a	a	k8xC	a
chlupatou	chlupatý	k2eAgFnSc4d1	chlupatá
slupku	slupka	k1gFnSc4	slupka
<g/>
.	.	kIx.	.
</s>
<s>
Slupka	slupka	k1gFnSc1	slupka
není	být	k5eNaImIp3nS	být
zdravotně	zdravotně	k6eAd1	zdravotně
závadná	závadný	k2eAgFnSc1d1	závadná
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
jíst	jíst	k5eAaImF	jíst
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
však	však	k9	však
před	před	k7c7	před
spotřebou	spotřeba	k1gFnSc7	spotřeba
kiwi	kiwi	k1gNnSc2	kiwi
oloupe	oloupat	k5eAaPmIp3nS	oloupat
<g/>
.	.	kIx.	.
</s>
<s>
Kiwi	kiwi	k1gNnSc1	kiwi
je	být	k5eAaImIp3nS	být
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
a	a	k8xC	a
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
měkne	měknout	k5eAaImIp3nS	měknout
a	a	k8xC	a
zraje	zrát	k5eAaImIp3nS	zrát
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
citlivé	citlivý	k2eAgNnSc1d1	citlivé
na	na	k7c4	na
tlak	tlak	k1gInSc4	tlak
a	a	k8xC	a
mačkání	mačkání	k1gNnSc4	mačkání
<g/>
.	.	kIx.	.
</s>
<s>
Mačkáním	mačkání	k1gNnSc7	mačkání
měknutí	měknutí	k1gNnSc4	měknutí
kiwi	kiwi	k1gNnSc2	kiwi
neuspíšíme	uspíšit	k5eNaPmIp1nP	uspíšit
<g/>
.	.	kIx.	.
</s>
<s>
Chuť	chuť	k1gFnSc1	chuť
kiwi	kiwi	k1gNnPc4	kiwi
popisuje	popisovat	k5eAaImIp3nS	popisovat
každý	každý	k3xTgMnSc1	každý
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
je	být	k5eAaImIp3nS	být
však	však	k9	však
přirovnává	přirovnávat	k5eAaImIp3nS	přirovnávat
ke	k	k7c3	k
kombinaci	kombinace	k1gFnSc3	kombinace
jahod	jahoda	k1gFnPc2	jahoda
<g/>
,	,	kIx,	,
ananasu	ananas	k1gInSc2	ananas
a	a	k8xC	a
banánu	banán	k1gInSc2	banán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
sehnat	sehnat	k5eAaPmF	sehnat
i	i	k9	i
zlatá	zlatý	k2eAgNnPc4d1	Zlaté
kiwi	kiwi	k1gNnPc4	kiwi
<g/>
.	.	kIx.	.
</s>
<s>
Zlaté	zlatý	k2eAgNnSc1d1	Zlaté
kiwi	kiwi	k1gNnSc1	kiwi
se	se	k3xPyFc4	se
od	od	k7c2	od
tradičního	tradiční	k2eAgNnSc2d1	tradiční
zeleného	zelené	k1gNnSc2	zelené
neliší	lišit	k5eNaImIp3nS	lišit
jen	jen	k9	jen
barvou	barva	k1gFnSc7	barva
dužiny	dužina	k1gFnSc2	dužina
ale	ale	k8xC	ale
také	také	k9	také
tvarem	tvar	k1gInSc7	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Špička	špička	k1gFnSc1	špička
naproti	naproti	k7c3	naproti
stopce	stopka	k1gFnSc3	stopka
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
kulatější	kulatý	k2eAgMnSc1d2	kulatější
a	a	k8xC	a
tvar	tvar	k1gInSc1	tvar
tak	tak	k6eAd1	tak
více	hodně	k6eAd2	hodně
připomíná	připomínat	k5eAaImIp3nS	připomínat
vejce	vejce	k1gNnSc4	vejce
nebo	nebo	k8xC	nebo
hrušku	hruška	k1gFnSc4	hruška
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
rozdíl	rozdíl	k1gInSc1	rozdíl
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ochlupení	ochlupení	k1gNnSc6	ochlupení
<g/>
.	.	kIx.	.
</s>
<s>
Chloupky	chloupek	k1gInPc1	chloupek
na	na	k7c6	na
slupce	slupka	k1gFnSc6	slupka
jsou	být	k5eAaImIp3nP	být
totiž	totiž	k9	totiž
miniaturní	miniaturní	k2eAgInPc1d1	miniaturní
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zastoupeny	zastoupit	k5eAaPmNgFnP	zastoupit
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
menším	malý	k2eAgNnSc6d2	menší
množství	množství	k1gNnSc6	množství
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
zlaté	zlatý	k2eAgNnSc1d1	Zlaté
kiwi	kiwi	k1gNnSc1	kiwi
mnohem	mnohem	k6eAd1	mnohem
citlivější	citlivý	k2eAgNnSc1d2	citlivější
na	na	k7c4	na
poškození	poškození	k1gNnSc4	poškození
a	a	k8xC	a
dozrává	dozrávat	k5eAaImIp3nS	dozrávat
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
farem	farma	k1gFnPc2	farma
produkujících	produkující	k2eAgInPc2d1	produkující
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
kiwi	kiwi	k1gNnSc4	kiwi
proto	proto	k8xC	proto
preferuje	preferovat	k5eAaImIp3nS	preferovat
ruční	ruční	k2eAgNnSc4d1	ruční
sbírání	sbírání	k1gNnSc4	sbírání
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
keřů	keř	k1gInPc2	keř
s	s	k7c7	s
ručním	ruční	k2eAgNnSc7d1	ruční
překládáním	překládání	k1gNnSc7	překládání
do	do	k7c2	do
košů	koš	k1gInPc2	koš
a	a	k8xC	a
krabic	krabice	k1gFnPc2	krabice
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
svoji	svůj	k3xOyFgFnSc4	svůj
náročnost	náročnost	k1gFnSc4	náročnost
na	na	k7c4	na
šetrné	šetrný	k2eAgNnSc4d1	šetrné
zacházení	zacházení	k1gNnSc4	zacházení
se	se	k3xPyFc4	se
zlaté	zlatý	k2eAgNnSc1d1	Zlaté
kiwi	kiwi	k1gNnSc1	kiwi
vyváží	vyvážet	k5eAaImIp3nS	vyvážet
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
zelenému	zelené	k1gNnSc3	zelené
kiwi	kiwi	k1gNnSc2	kiwi
je	být	k5eAaImIp3nS	být
zlaté	zlatý	k2eAgFnPc4d1	zlatá
sladší	sladký	k2eAgFnPc4d2	sladší
<g/>
,	,	kIx,	,
jemnější	jemný	k2eAgFnPc4d2	jemnější
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
kyselé	kyselý	k2eAgNnSc1d1	kyselé
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
vyšším	vysoký	k2eAgInPc3d2	vyšší
nákladům	náklad	k1gInPc3	náklad
na	na	k7c4	na
pěstování	pěstování	k1gNnSc4	pěstování
<g/>
,	,	kIx,	,
sklizeň	sklizeň	k1gFnSc4	sklizeň
i	i	k8xC	i
přepravu	přeprava	k1gFnSc4	přeprava
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
také	také	k9	také
dražší	drahý	k2eAgInPc1d2	dražší
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
málokteré	málokterý	k3yIgNnSc1	málokterý
ovoce	ovoce	k1gNnSc1	ovoce
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tolik	tolik	k6eAd1	tolik
vitamínu	vitamín	k1gInSc2	vitamín
C	C	kA	C
jako	jako	k8xS	jako
kiwi	kiwi	k1gNnSc2	kiwi
<g/>
.	.	kIx.	.
</s>
<s>
Obsahem	obsah	k1gInSc7	obsah
až	až	k9	až
1	[number]	k4	1
miligram	miligram	k1gInSc1	miligram
vitamínu	vitamín	k1gInSc2	vitamín
C	C	kA	C
v	v	k7c6	v
1	[number]	k4	1
gramu	gram	k1gInSc6	gram
kiwi	kiwi	k1gNnSc2	kiwi
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
konzumace	konzumace	k1gFnSc1	konzumace
jednoho	jeden	k4xCgInSc2	jeden
kusu	kus	k1gInSc2	kus
kiwi	kiwi	k1gNnPc4	kiwi
příjem	příjem	k1gInSc1	příjem
denní	denní	k2eAgFnSc2d1	denní
dávky	dávka	k1gFnSc2	dávka
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
vitamínu	vitamín	k1gInSc3	vitamín
C	C	kA	C
<g/>
,	,	kIx,	,
i	i	k8xC	i
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
hořčíkem	hořčík	k1gInSc7	hořčík
<g/>
,	,	kIx,	,
podporuje	podporovat	k5eAaImIp3nS	podporovat
kiwi	kiwi	k1gNnSc1	kiwi
látkovou	látkový	k2eAgFnSc4d1	látková
výměnu	výměna	k1gFnSc4	výměna
<g/>
,	,	kIx,	,
zpevňuje	zpevňovat	k5eAaImIp3nS	zpevňovat
cévy	céva	k1gFnPc4	céva
a	a	k8xC	a
žíly	žíla	k1gFnPc4	žíla
<g/>
,	,	kIx,	,
posiluje	posilovat	k5eAaImIp3nS	posilovat
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
a	a	k8xC	a
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
vidění	vidění	k1gNnSc3	vidění
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
vitamín	vitamín	k1gInSc1	vitamín
C	C	kA	C
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc4d1	důležitý
antioxidant	antioxidant	k1gInSc4	antioxidant
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
zvládat	zvládat	k5eAaImF	zvládat
stresy	stres	k1gInPc4	stres
<g/>
,	,	kIx,	,
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
schopnost	schopnost	k1gFnSc4	schopnost
soustředit	soustředit	k5eAaPmF	soustředit
se	se	k3xPyFc4	se
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
tvorbu	tvorba	k1gFnSc4	tvorba
hormonů	hormon	k1gInPc2	hormon
pro	pro	k7c4	pro
dobrou	dobrý	k2eAgFnSc4d1	dobrá
náladu	nálada	k1gFnSc4	nálada
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
konzumace	konzumace	k1gFnSc1	konzumace
kiwi	kiwi	k1gNnSc2	kiwi
přispívá	přispívat	k5eAaImIp3nS	přispívat
ke	k	k7c3	k
zdravé	zdravý	k2eAgFnSc3d1	zdravá
funkci	funkce	k1gFnSc3	funkce
srdce	srdce	k1gNnSc2	srdce
a	a	k8xC	a
svalů	sval	k1gInPc2	sval
<g/>
,	,	kIx,	,
k	k	k7c3	k
růstu	růst	k1gInSc3	růst
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
při	při	k7c6	při
krvácení	krvácení	k1gNnSc6	krvácení
z	z	k7c2	z
dásní	dáseň	k1gFnPc2	dáseň
<g/>
,	,	kIx,	,
parodontóze	parodontóza	k1gFnSc3	parodontóza
<g/>
.	.	kIx.	.
</s>
<s>
Hlavně	hlavně	k9	hlavně
v	v	k7c6	v
zimních	zimní	k2eAgInPc6d1	zimní
měsících	měsíc	k1gInPc6	měsíc
je	být	k5eAaImIp3nS	být
kiwi	kiwi	k1gNnSc2	kiwi
výbornou	výborný	k2eAgFnSc7d1	výborná
prevencí	prevence	k1gFnSc7	prevence
chřipek	chřipka	k1gFnPc2	chřipka
<g/>
,	,	kIx,	,
nachlazení	nachlazení	k1gNnSc2	nachlazení
a	a	k8xC	a
rýmy	rýma	k1gFnSc2	rýma
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
i	i	k9	i
významný	významný	k2eAgInSc1d1	významný
zdroj	zdroj	k1gInSc1	zdroj
energie	energie	k1gFnSc2	energie
pro	pro	k7c4	pro
sportovce	sportovec	k1gMnPc4	sportovec
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
také	také	k9	také
pomůže	pomoct	k5eAaPmIp3nS	pomoct
například	například	k6eAd1	například
při	při	k7c6	při
namožení	namožení	k1gNnSc6	namožení
vaziv	vazivo	k1gNnPc2	vazivo
nebo	nebo	k8xC	nebo
podvrtnutém	podvrtnutý	k2eAgInSc6d1	podvrtnutý
kotníku	kotník	k1gInSc6	kotník
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
díky	díky	k7c3	díky
vysokému	vysoký	k2eAgInSc3d1	vysoký
obsahu	obsah	k1gInSc3	obsah
vitaminu	vitamin	k1gInSc2	vitamin
C	C	kA	C
zpevňuje	zpevňovat	k5eAaImIp3nS	zpevňovat
vaziva	vazivo	k1gNnPc4	vazivo
<g/>
.	.	kIx.	.
</s>
<s>
Obsah	obsah	k1gInSc1	obsah
draslíku	draslík	k1gInSc2	draslík
je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
u	u	k7c2	u
banánu	banán	k1gInSc2	banán
<g/>
.	.	kIx.	.
</s>
<s>
Kiwi	kiwi	k1gNnSc1	kiwi
také	také	k9	také
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vitamín	vitamín	k1gInSc4	vitamín
E	E	kA	E
s	s	k7c7	s
rovněž	rovněž	k9	rovněž
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgInPc4d1	jiný
antioxidačními	antioxidační	k2eAgInPc7d1	antioxidační
účinky	účinek	k1gInPc7	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
bohaté	bohatý	k2eAgNnSc1d1	bohaté
na	na	k7c4	na
některé	některý	k3yIgFnPc4	některý
bílkoviny	bílkovina	k1gFnPc4	bílkovina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
ke	k	k7c3	k
změkčování	změkčování	k1gNnSc3	změkčování
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
pro	pro	k7c4	pro
některé	některý	k3yIgMnPc4	některý
lidi	člověk	k1gMnPc4	člověk
alergenem	alergen	k1gInSc7	alergen
<g/>
.	.	kIx.	.
</s>
<s>
Kiwi	kiwi	k1gNnPc1	kiwi
můžeme	moct	k5eAaImIp1nP	moct
jíst	jíst	k5eAaImF	jíst
syrové	syrový	k2eAgNnSc4d1	syrové
nebo	nebo	k8xC	nebo
tepelně	tepelně	k6eAd1	tepelně
upravené	upravený	k2eAgFnPc1d1	upravená
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
si	se	k3xPyFc3	se
kiwi	kiwi	k1gNnPc4	kiwi
před	před	k7c7	před
konzumací	konzumace	k1gFnSc7	konzumace
loupe	loupat	k5eAaImIp3nS	loupat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
.	.	kIx.	.
</s>
<s>
Kiwi	kiwi	k1gNnSc1	kiwi
lze	lze	k6eAd1	lze
jíst	jíst	k5eAaImF	jíst
i	i	k9	i
jako	jako	k9	jako
jablko	jablko	k1gNnSc4	jablko
<g/>
,	,	kIx,	,
stačí	stačit	k5eAaBmIp3nS	stačit
jej	on	k3xPp3gMnSc4	on
před	před	k7c7	před
konzumací	konzumace	k1gFnSc7	konzumace
jen	jen	k6eAd1	jen
umýt	umýt	k5eAaPmF	umýt
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jíst	jíst	k5eAaImF	jíst
kiwi	kiwi	k1gNnPc4	kiwi
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	on	k3xPp3gInPc4	on
nechceme	chtít	k5eNaImIp1nP	chtít
jíst	jíst	k5eAaImF	jíst
se	s	k7c7	s
slupkou	slupka	k1gFnSc7	slupka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rozpůlit	rozpůlit	k5eAaPmF	rozpůlit
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
lžičkou	lžička	k1gFnSc7	lžička
vydlabat	vydlabat	k5eAaPmF	vydlabat
měkkou	měkký	k2eAgFnSc4d1	měkká
dužinu	dužina	k1gFnSc4	dužina
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
můžeme	moct	k5eAaImIp1nP	moct
kiwi	kiwi	k1gNnPc4	kiwi
omýt	omýt	k5eAaPmF	omýt
<g/>
,	,	kIx,	,
oloupat	oloupat	k5eAaPmF	oloupat
a	a	k8xC	a
sníst	sníst	k5eAaPmF	sníst
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
použít	použít	k5eAaPmF	použít
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
nerezová	rezový	k2eNgFnSc1d1	nerezová
škrabka	škrabka	k1gFnSc1	škrabka
na	na	k7c4	na
brambory	brambor	k1gInPc4	brambor
nebo	nebo	k8xC	nebo
obyčejný	obyčejný	k2eAgInSc4d1	obyčejný
loupací	loupací	k2eAgInSc4d1	loupací
nůž	nůž	k1gInSc4	nůž
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
jiného	jiný	k2eAgNnSc2d1	jiné
ovoce	ovoce	k1gNnSc2	ovoce
se	se	k3xPyFc4	se
ale	ale	k9	ale
i	i	k9	i
zde	zde	k6eAd1	zde
loupáním	loupání	k1gNnSc7	loupání
ztrácí	ztrácet	k5eAaImIp3nP	ztrácet
mnoho	mnoho	k6eAd1	mnoho
ve	v	k7c6	v
výživě	výživa	k1gFnSc6	výživa
významných	významný	k2eAgFnPc2d1	významná
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
změkčovacímu	změkčovací	k2eAgInSc3d1	změkčovací
enzymu	enzym	k1gInSc3	enzym
není	být	k5eNaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
kiwi	kiwi	k1gNnSc1	kiwi
používat	používat	k5eAaImF	používat
v	v	k7c6	v
dezertech	dezert	k1gInPc6	dezert
a	a	k8xC	a
moučnících	moučník	k1gInPc6	moučník
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
mléko	mléko	k1gNnSc4	mléko
nebo	nebo	k8xC	nebo
mléčné	mléčný	k2eAgInPc4d1	mléčný
výrobky	výrobek	k1gInPc4	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	on	k3xPp3gNnPc4	on
kiwi	kiwi	k1gNnPc4	kiwi
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
se	s	k7c7	s
šlehačkou	šlehačka	k1gFnSc7	šlehačka
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblíbených	oblíbený	k2eAgInPc6d1	oblíbený
dezertech	dezert	k1gInPc6	dezert
<g/>
.	.	kIx.	.
</s>
<s>
Kiwi	kiwi	k1gNnSc1	kiwi
chutná	chutnat	k5eAaImIp3nS	chutnat
nejlépe	dobře	k6eAd3	dobře
v	v	k7c6	v
čerstvém	čerstvý	k2eAgInSc6d1	čerstvý
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
můžeme	moct	k5eAaImIp1nP	moct
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
dělat	dělat	k5eAaImF	dělat
i	i	k9	i
kompoty	kompot	k1gInPc1	kompot
<g/>
,	,	kIx,	,
koláče	koláč	k1gInPc1	koláč
<g/>
,	,	kIx,	,
ovocné	ovocný	k2eAgInPc1d1	ovocný
dezerty	dezert	k1gInPc1	dezert
<g/>
,	,	kIx,	,
marmelády	marmeláda	k1gFnPc1	marmeláda
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
ke	k	k7c3	k
zdobení	zdobení	k1gNnSc3	zdobení
dezertů	dezert	k1gInPc2	dezert
a	a	k8xC	a
výrobků	výrobek	k1gInPc2	výrobek
studené	studený	k2eAgFnSc2d1	studená
kuchyně	kuchyně	k1gFnSc2	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
kiwi	kiwi	k1gNnSc1	kiwi
také	také	k9	také
užívá	užívat	k5eAaImIp3nS	užívat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
džusů	džus	k1gInPc2	džus
nebo	nebo	k8xC	nebo
míchání	míchání	k1gNnSc2	míchání
koktejlů	koktejl	k1gInPc2	koktejl
<g/>
;	;	kIx,	;
do	do	k7c2	do
nápojů	nápoj	k1gInPc2	nápoj
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
vhodné	vhodný	k2eAgNnSc4d1	vhodné
kiwi	kiwi	k1gNnSc4	kiwi
oloupat	oloupat	k5eAaPmF	oloupat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
do	do	k7c2	do
nápoje	nápoj	k1gInSc2	nápoj
ze	z	k7c2	z
slupky	slupka	k1gFnSc2	slupka
rozmixováním	rozmixování	k1gNnSc7	rozmixování
rozptýlí	rozptýlit	k5eAaPmIp3nS	rozptýlit
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k4c4	mnoho
hořkých	hořký	k2eAgFnPc2d1	hořká
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
sice	sice	k8xC	sice
nejsou	být	k5eNaImIp3nP	být
nezdravé	zdravý	k2eNgFnPc1d1	nezdravá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
nepříjemnou	příjemný	k2eNgFnSc4d1	nepříjemná
chuť	chuť	k1gFnSc4	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Kiwi	kiwi	k1gNnSc1	kiwi
lze	lze	k6eAd1	lze
i	i	k9	i
sušit	sušit	k5eAaImF	sušit
<g/>
.	.	kIx.	.
</s>
<s>
Kiwi	kiwi	k1gNnSc1	kiwi
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
použít	použít	k5eAaPmF	použít
také	také	k9	také
jako	jako	k9	jako
náplň	náplň	k1gFnSc4	náplň
do	do	k7c2	do
palačinek	palačinka	k1gFnPc2	palačinka
<g/>
,	,	kIx,	,
k	k	k7c3	k
ochucení	ochucení	k1gNnSc3	ochucení
jogurtů	jogurt	k1gInPc2	jogurt
nebo	nebo	k8xC	nebo
zmrzliny	zmrzlina	k1gFnSc2	zmrzlina
<g/>
.	.	kIx.	.
</s>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
udává	udávat	k5eAaImIp3nS	udávat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
živin	živina	k1gFnPc2	živina
<g/>
,	,	kIx,	,
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
vitamínů	vitamín	k1gInPc2	vitamín
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
nutričních	nutriční	k2eAgInPc2d1	nutriční
parametrů	parametr	k1gInPc2	parametr
zjištěných	zjištěný	k2eAgInPc2d1	zjištěný
v	v	k7c6	v
plodech	plod	k1gInPc6	plod
kiwi	kiwi	k1gNnPc2	kiwi
<g/>
.	.	kIx.	.
</s>
<s>
Plody	plod	k1gInPc1	plod
bychom	by	kYmCp1nP	by
při	při	k7c6	při
výběru	výběr	k1gInSc6	výběr
měli	mít	k5eAaImAgMnP	mít
jen	jen	k9	jen
lehce	lehko	k6eAd1	lehko
ohmatat	ohmatat	k5eAaPmF	ohmatat
<g/>
.	.	kIx.	.
</s>
<s>
Příliš	příliš	k6eAd1	příliš
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
kiwi	kiwi	k1gNnSc1	kiwi
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
nezralé	zralý	k2eNgNnSc1d1	nezralé
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
méně	málo	k6eAd2	málo
šťávy	šťáva	k1gFnSc2	šťáva
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kyselejší	kyselý	k2eAgNnSc1d2	kyselejší
a	a	k8xC	a
zejména	zejména	k9	zejména
slupka	slupka	k1gFnSc1	slupka
je	být	k5eAaImIp3nS	být
tuhá	tuhý	k2eAgNnPc4d1	tuhé
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jíst	jíst	k5eAaImF	jíst
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgNnSc4	takový
kiwi	kiwi	k1gNnSc4	kiwi
můžeme	moct	k5eAaImIp1nP	moct
koupit	koupit	k5eAaPmF	koupit
a	a	k8xC	a
nechat	nechat	k5eAaPmF	nechat
doma	doma	k6eAd1	doma
v	v	k7c6	v
teple	teplo	k1gNnSc6	teplo
dozrát	dozrát	k5eAaPmF	dozrát
<g/>
.	.	kIx.	.
</s>
<s>
Příliš	příliš	k6eAd1	příliš
měkké	měkký	k2eAgNnSc1d1	měkké
kiwi	kiwi	k1gNnPc1	kiwi
nekupujeme	kupovat	k5eNaImIp1nP	kupovat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
nechceme	chtít	k5eNaImIp1nP	chtít
připravovat	připravovat	k5eAaImF	připravovat
marmeládu	marmeláda	k1gFnSc4	marmeláda
<g/>
.	.	kIx.	.
</s>
<s>
Kiwi	kiwi	k1gNnSc1	kiwi
není	být	k5eNaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
chemicky	chemicky	k6eAd1	chemicky
ošetřeno	ošetřen	k2eAgNnSc1d1	ošetřeno
jako	jako	k8xS	jako
citrusy	citrus	k1gInPc4	citrus
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
jej	on	k3xPp3gMnSc4	on
můžeme	moct	k5eAaImIp1nP	moct
skladovat	skladovat	k5eAaImF	skladovat
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
kolem	kolem	k7c2	kolem
1	[number]	k4	1
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
asi	asi	k9	asi
80	[number]	k4	80
-	-	kIx~	-
90	[number]	k4	90
%	%	kIx~	%
vlhkosti	vlhkost	k1gFnSc2	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
vydrží	vydržet	k5eAaPmIp3nS	vydržet
kiwi	kiwi	k1gNnPc4	kiwi
až	až	k6eAd1	až
5	[number]	k4	5
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kiwi	kiwi	k1gNnSc2	kiwi
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kiwi	kiwi	k1gNnSc2	kiwi
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
