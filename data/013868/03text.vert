<s>
Operace	operace	k1gFnSc1
Tungsten	Tungsten	k2eAgMnSc1d1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
výsadku	výsadek	k1gInSc6
z	z	k7c2
Anglie	Anglie	k1gFnSc2
na	na	k7c6
území	území	k1gNnSc6
Protektorátu	protektorát	k1gInSc6
Čechy	Čechy	k1gFnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možná	možná	k6eAd1
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
Operace	operace	k1gFnSc1
Tungsten	Tungsten	k2eAgInSc4d1
–	–	k?
pokus	pokus	k1gInSc4
RAF	raf	k0
o	o	k7c4
potopení	potopení	k1gNnSc4
německé	německý	k2eAgFnSc2d1
bitevní	bitevní	k2eAgFnSc2d1
lodě	loď	k1gFnSc2
Tirpitz	Tirpitza	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Operace	operace	k1gFnSc1
Tungsten	Tungsten	k1gInSc1
byl	být	k5eAaImAgInS
krycí	krycí	k2eAgInSc1d1
název	název	k1gInSc1
pro	pro	k7c4
paradesantní	paradesantní	k2eAgInSc4d1
výsadek	výsadek	k1gInSc4
vyslaný	vyslaný	k2eAgInSc4d1
během	během	k7c2
II	II	kA
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
z	z	k7c2
Anglie	Anglie	k1gFnSc2
na	na	k7c6
území	území	k1gNnSc4
Protektorátu	protektorát	k1gInSc2
Čechy	Čechy	k1gFnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsadek	výsadek	k1gInSc1
byl	být	k5eAaImAgInS
organizován	organizovat	k5eAaBmNgInS
zpravodajským	zpravodajský	k2eAgInSc7d1
odborem	odbor	k1gInSc7
exilového	exilový	k2eAgNnSc2d1
Ministerstva	ministerstvo	k1gNnSc2
národní	národní	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
a	a	k8xC
byl	být	k5eAaImAgInS
řazen	řadit	k5eAaImNgInS
do	do	k7c2
třetí	třetí	k4xOgFnSc2
vlny	vlna	k1gFnSc2
výsadků	výsadek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Složení	složení	k1gNnSc1
a	a	k8xC
úkol	úkol	k1gInSc1
</s>
<s>
Desant	desant	k1gInSc4
tvořili	tvořit	k5eAaImAgMnP
velitel	velitel	k1gMnSc1
kapitán	kapitán	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
Pernický	Pernický	k2eAgMnSc1d1
a	a	k8xC
rotmistr	rotmistr	k1gMnSc1
Leopold	Leopold	k1gMnSc1
Musil	Musil	k1gMnSc1
jako	jako	k8xS,k8xC
obsluha	obsluha	k1gFnSc1
radiomajáku	radiomaják	k1gInSc2
Eureka	Eureek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInSc7
úkolem	úkol	k1gInSc7
bylo	být	k5eAaImAgNnS
zajišťovat	zajišťovat	k5eAaImF
příjem	příjem	k1gInSc4
dalších	další	k2eAgInPc2d1
výsadků	výsadek	k1gInPc2
a	a	k8xC
materiálu	materiál	k1gInSc2
na	na	k7c6
území	území	k1gNnSc6
protektorátu	protektorát	k1gInSc2
<g/>
,	,	kIx,
dopravit	dopravit	k5eAaPmF
náhradní	náhradní	k2eAgInPc4d1
krystaly	krystal	k1gInPc4
pro	pro	k7c4
radiostanici	radiostanice	k1gFnSc4
Calcia	Calcium	k1gNnSc2
a	a	k8xC
připravit	připravit	k5eAaPmF
odlet	odlet	k1gInSc4
představitelů	představitel	k1gMnPc2
domácího	domácí	k2eAgInSc2d1
odboje	odboj	k1gInSc2
do	do	k7c2
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
měl	mít	k5eAaImAgInS
desant	desant	k1gInSc1
urovnat	urovnat	k5eAaPmF
kompetenční	kompetenční	k2eAgInPc4d1
spory	spor	k1gInPc4
mezi	mezi	k7c7
příslušníky	příslušník	k1gMnPc7
Calcia	Calcium	k1gNnSc2
a	a	k8xC
Radou	rada	k1gFnSc7
tří	tři	k4xCgFnPc2
<g/>
.	.	kIx.
</s>
<s>
Činnost	činnost	k1gFnSc1
</s>
<s>
Nad	nad	k7c7
územím	území	k1gNnSc7
protektorátu	protektorát	k1gInSc2
byl	být	k5eAaImAgMnS
desant	desant	k1gInSc4
vysazen	vysazen	k2eAgInSc4d1
společně	společně	k6eAd1
s	s	k7c7
desantem	desant	k1gInSc7
Embassy	Embassa	k1gFnSc2
v	v	k7c6
noci	noc	k1gFnSc6
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1944	#num#	k4
před	před	k7c7
půlnocí	půlnoc	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vinou	vinou	k7c2
chyby	chyba	k1gFnSc2
navigátora	navigátor	k1gMnSc2
byli	být	k5eAaImAgMnP
vysazeni	vysadit	k5eAaPmNgMnP
u	u	k7c2
Libenic	Libenice	k1gFnPc2
nedaleko	nedaleko	k7c2
Kutné	kutný	k2eAgFnSc2d1
Hory	hora	k1gFnSc2
na	na	k7c4
místo	místo	k1gNnSc4
u	u	k7c2
Vortové	Vortová	k1gFnSc2
u	u	k7c2
Hlinska	Hlinsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
usilovném	usilovný	k2eAgInSc6d1
<g/>
,	,	kIx,
více	hodně	k6eAd2
jak	jak	k6eAd1
stokilometrovém	stokilometrový	k2eAgInSc6d1
pochodu	pochod	k1gInSc6
dorazili	dorazit	k5eAaPmAgMnP
do	do	k7c2
místa	místo	k1gNnSc2
určení	určení	k1gNnSc2
nad	nad	k7c7
ránem	ráno	k1gNnSc7
29	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
omrzlí	omrzlý	k2eAgMnPc1d1
a	a	k8xC
vyčerpaní	vyčerpaný	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukryli	ukrýt	k5eAaPmAgMnP
se	se	k3xPyFc4
na	na	k7c6
záchytné	záchytný	k2eAgFnSc6d1
adrese	adresa	k1gFnSc6
ve	v	k7c6
Studnici	studnice	k1gFnSc6
u	u	k7c2
Nového	Nového	k2eAgNnSc2d1
Města	město	k1gNnSc2
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	on	k3xPp3gMnPc4
kontaktovali	kontaktovat	k5eAaImAgMnP
příslušníci	příslušník	k1gMnPc1
odbojové	odbojový	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
Rada	rada	k1gFnSc1
tří	tři	k4xCgFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
rekonvalescenci	rekonvalescence	k1gFnSc6
zahájili	zahájit	k5eAaPmAgMnP
Pernický	Pernický	k2eAgInSc4d1
a	a	k8xC
Musil	musit	k5eAaImAgMnS
svou	svůj	k3xOyFgFnSc4
činnost	činnost	k1gFnSc4
<g/>
:	:	kIx,
Pernický	Pernický	k2eAgMnSc1d1
vyslechl	vyslechnout	k5eAaPmAgMnS
obě	dva	k4xCgFnPc4
znesvářené	znesvářený	k2eAgFnPc4d1
strany	strana	k1gFnPc4
a	a	k8xC
o	o	k7c6
výsledku	výsledek	k1gInSc6
jednání	jednání	k1gNnSc2
informoval	informovat	k5eAaBmAgInS
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
potom	potom	k6eAd1
vyhledávali	vyhledávat	k5eAaImAgMnP
vhodné	vhodný	k2eAgFnPc4d1
plochy	plocha	k1gFnPc4
pro	pro	k7c4
shoz	shoz	k1gInSc4
materiálu	materiál	k1gInSc2
a	a	k8xC
pomocí	pomocí	k7c2
radiomajáku	radiomaják	k1gInSc2
Eureka	Eureek	k1gInSc2
naváděli	navádět	k5eAaImAgMnP
letadla	letadlo	k1gNnSc2
přivážející	přivážející	k2eAgFnPc4d1
další	další	k2eAgFnPc4d1
výsadky	výsadka	k1gFnPc4
s	s	k7c7
materiálem	materiál	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
činnosti	činnost	k1gFnSc6
pokračovali	pokračovat	k5eAaImAgMnP
až	až	k9
do	do	k7c2
osvobození	osvobození	k1gNnSc2
<g/>
,	,	kIx,
kterého	který	k3yIgNnSc2,k3yRgNnSc2,k3yQgNnSc2
se	se	k3xPyFc4
oba	dva	k4xCgMnPc1
členové	člen	k1gMnPc1
desantu	desant	k1gInSc2
dožili	dožít	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
PACNER	PACNER	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Československo	Československo	k1gNnSc1
ve	v	k7c6
zvláštních	zvláštní	k2eAgFnPc6d1
službách	služba	k1gFnPc6
(	(	kIx(
<g/>
1939	#num#	k4
<g/>
-	-	kIx~
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
díl	díl	k1gInSc1
II	II	kA
<g/>
..	..	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Themis	Themis	k1gFnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7312	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HANÁK	Hanák	k1gMnSc1
<g/>
,	,	kIx,
Vítězslav	Vítězslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muži	muž	k1gMnPc7
a	a	k8xC
radiostanice	radiostanice	k1gFnSc1
tajné	tajný	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvůr	Dvůr	k1gInSc1
Králové	Králová	k1gFnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
:	:	kIx,
ELLI	ELLI	kA
print	print	k1gMnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
239	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
322	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Podrobnosti	podrobnost	k1gFnPc1
na	na	k7c4
rozhlas	rozhlas	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Vzpomínky	vzpomínka	k1gFnPc1
genmjr.	genmjr.	k?
Pernického	Pernický	k2eAgMnSc4d1
na	na	k7c4
army	army	k1gInPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
(	(	kIx(
<g/>
formát	formát	k1gInSc1
pdf	pdf	k?
<g/>
,	,	kIx,
page	page	k1gFnSc1
26	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Podrobnosti	podrobnost	k1gFnPc1
a	a	k8xC
fotografie	fotografia	k1gFnPc1
na	na	k7c4
stripky	stripek	k1gInPc4
<g/>
.	.	kIx.
<g/>
org	org	k?
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
