<p>
<s>
Lamberk	Lamberk	k1gInSc1	Lamberk
je	být	k5eAaImIp3nS	být
zřícenina	zřícenina	k1gFnSc1	zřícenina
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
záhybu	záhyb	k1gInSc6	záhyb
řeky	řeka	k1gFnSc2	řeka
Oslavy	oslava	k1gFnSc2	oslava
asi	asi	k9	asi
2	[number]	k4	2
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Březníka	Březník	k1gMnSc2	Březník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
opačné	opačný	k2eAgFnSc6d1	opačná
straně	strana	k1gFnSc6	strana
řeky	řeka	k1gFnSc2	řeka
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
220	[number]	k4	220
metrů	metr	k1gInPc2	metr
leží	ležet	k5eAaImIp3nS	ležet
Sedlecký	sedlecký	k2eAgInSc4d1	sedlecký
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
první	první	k4xOgMnSc1	první
majitel	majitel	k1gMnSc1	majitel
byl	být	k5eAaImAgMnS	být
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
z	z	k7c2	z
Knínic	Knínice	k1gFnPc2	Knínice
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
působil	působit	k5eAaImAgInS	působit
v	v	k7c6	v
zemských	zemský	k2eAgInPc6d1	zemský
úřadech	úřad	k1gInPc6	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
prodal	prodat	k5eAaPmAgInS	prodat
roku	rok	k1gInSc2	rok
1364	[number]	k4	1364
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
si	se	k3xPyFc3	se
postavit	postavit	k5eAaPmF	postavit
hrad	hrad	k1gInSc4	hrad
Lamberk	Lamberk	k1gInSc1	Lamberk
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
psal	psát	k5eAaImAgMnS	psát
poprvé	poprvé	k6eAd1	poprvé
roku	rok	k1gInSc2	rok
1376	[number]	k4	1376
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámějším	známý	k2eAgMnSc7d3	nejznámější
majitelem	majitel	k1gMnSc7	majitel
hradu	hrad	k1gInSc2	hrad
byl	být	k5eAaImAgMnS	být
však	však	k9	však
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Jan	Jan	k1gMnSc1	Jan
Sokol	Sokol	k1gMnSc1	Sokol
z	z	k7c2	z
Lamberka	Lamberka	k1gFnSc1	Lamberka
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
proslul	proslout	k5eAaPmAgMnS	proslout
jako	jako	k8xC	jako
zdatný	zdatný	k2eAgMnSc1d1	zdatný
válečník	válečník	k1gMnSc1	válečník
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
husitských	husitský	k2eAgFnPc6d1	husitská
válkách	válka	k1gFnPc6	válka
patřil	patřit	k5eAaImAgInS	patřit
hrad	hrad	k1gInSc1	hrad
Janu	Jana	k1gFnSc4	Jana
Komorovskému	Komorovský	k2eAgMnSc3d1	Komorovský
<g/>
.	.	kIx.	.
</s>
<s>
Zanikl	zaniknout	k5eAaPmAgMnS	zaniknout
patrně	patrně	k6eAd1	patrně
po	po	k7c6	po
roku	rok	k1gInSc6	rok
1440	[number]	k4	1440
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgFnP	být
podnikány	podnikán	k2eAgFnPc4d1	podnikána
trestné	trestný	k2eAgFnPc4d1	trestná
výpravy	výprava	k1gFnPc4	výprava
proti	proti	k7c3	proti
hradům	hrad	k1gInPc3	hrad
rozbrojníků	rozbrojník	k1gMnPc2	rozbrojník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1459	[number]	k4	1459
se	se	k3xPyFc4	se
hrad	hrad	k1gInSc1	hrad
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
pustý	pustý	k2eAgInSc4d1	pustý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současmý	Současmý	k2eAgInSc1d1	Současmý
stav	stav	k1gInSc1	stav
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hradu	hrad	k1gInSc2	hrad
se	se	k3xPyFc4	se
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
dochovaly	dochovat	k5eAaPmAgInP	dochovat
pouze	pouze	k6eAd1	pouze
fragmenty	fragment	k1gInPc1	fragment
zdiva	zdivo	k1gNnSc2	zdivo
a	a	k8xC	a
zbytky	zbytek	k1gInPc4	zbytek
příkopů	příkop	k1gInPc2	příkop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Lamberk	Lamberk	k1gInSc1	Lamberk
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Lamberk	Lamberk	k1gInSc1	Lamberk
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Hrady	hrad	k1gInPc4	hrad
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
