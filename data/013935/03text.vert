<s>
Indium-	Indium-	k?
<g/>
111	#num#	k4
</s>
<s>
Indium-	Indium-	k1gNnSc1
<g/>
111	#num#	k4
(	(	kIx(
<g/>
111	#num#	k4
<g/>
In	In	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
izotop	izotop	k1gInSc1
india	indium	k1gNnSc2
s	s	k7c7
poločasem	poločas	k1gInSc7
přeměny	přeměna	k1gFnSc2
2,8	2,8	k4
dne	den	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1
z	z	k7c2
něj	on	k3xPp3gMnSc2
dělá	dělat	k5eAaImIp3nS
vhodný	vhodný	k2eAgInSc4d1
radioaktivní	radioaktivní	k2eAgInSc4d1
značkovač	značkovač	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
izotop	izotop	k1gInSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
medicíně	medicína	k1gFnSc6
mimo	mimo	k7c4
jiné	jiný	k2eAgFnPc4d1
následující	následující	k2eAgFnPc4d1
diagnostická	diagnostický	k2eAgNnPc4d1
využití	využití	k1gNnPc4
<g/>
:	:	kIx,
</s>
<s>
protilátky	protilátka	k1gFnSc2
značkované	značkovaný	k2eAgFnSc2d1
111	#num#	k4
<g/>
In	In	k1gMnPc2
</s>
<s>
komplex	komplex	k1gInSc1
111	#num#	k4
<g/>
In	In	k1gFnPc2
s	s	k7c7
8	#num#	k4
<g/>
-hydroxychinolinem	-hydroxychinolin	k1gInSc7
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
ke	k	k7c3
značkování	značkování	k1gNnSc3
částí	část	k1gFnPc2
krevních	krevní	k2eAgFnPc2d1
buněk	buňka	k1gFnPc2
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
ke	k	k7c3
značkování	značkování	k1gNnSc3
krevních	krevní	k2eAgFnPc2d1
destiček	destička	k1gFnPc2
sloužícímu	sloužící	k1gMnSc3
k	k	k7c3
detekci	detekce	k1gFnSc3
krevní	krevní	k2eAgFnSc2d1
sraženiny	sraženina	k1gFnSc2
a	a	k8xC
rovněž	rovněž	k9
ke	k	k7c3
značkování	značkování	k1gNnSc3
bílých	bílý	k2eAgFnPc2d1
krvinek	krvinka	k1gFnPc2
za	za	k7c7
účelem	účel	k1gInSc7
lokalizace	lokalizace	k1gFnSc2
zánětů	zánět	k1gInPc2
a	a	k8xC
abscesů	absces	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
ke	k	k7c3
zjišťování	zjišťování	k1gNnSc3
kinetiky	kinetika	k1gFnSc2
bílých	bílý	k2eAgFnPc2d1
krvinek	krvinka	k1gFnPc2
</s>
<s>
Indium-	Indium-	k?
<g/>
111	#num#	k4
se	se	k3xPyFc4
přeměňuje	přeměňovat	k5eAaImIp3nS
záchytem	záchyt	k1gInSc7
elektronu	elektron	k1gInSc2
s	s	k7c7
poločasem	poločas	k1gInSc7
2,804	2,804	k4
<g/>
7	#num#	k4
dne	den	k1gInSc2
na	na	k7c4
stabilní	stabilní	k2eAgFnSc4d1
kadmium-	kadmium-	k?
<g/>
111	#num#	k4
<g/>
,	,	kIx,
vzniká	vznikat	k5eAaImIp3nS
z	z	k7c2
cínu-	cínu-	k?
<g/>
111	#num#	k4
beta	beta	k1gNnSc2
plus	plus	k6eAd1
přeměnou	přeměna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Izotopy	izotop	k1gInPc1
india	indium	k1gNnSc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Indium-	Indium-	k1gFnSc2
<g/>
111	#num#	k4
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Chemie	chemie	k1gFnSc1
|	|	kIx~
Fyzika	fyzika	k1gFnSc1
|	|	kIx~
Medicína	medicína	k1gFnSc1
</s>
