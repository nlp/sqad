<s>
Bjarne	Bjarnout	k5eAaPmIp3nS	Bjarnout
Stroustrup	Stroustrup	k1gInSc1	Stroustrup
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1950	[number]	k4	1950
<g/>
,	,	kIx,	,
Aarhus	Aarhus	k1gInSc1	Aarhus
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dánský	dánský	k2eAgMnSc1d1	dánský
programátor	programátor	k1gMnSc1	programátor
a	a	k8xC	a
informatik	informatik	k1gMnSc1	informatik
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
na	na	k7c4	na
Texas	Texas	kA	Texas
A	A	kA	A
<g/>
&	&	k?	&
<g/>
M	M	kA	M
University	universita	k1gFnSc2	universita
a	a	k8xC	a
tvůrce	tvůrce	k1gMnSc2	tvůrce
programovacího	programovací	k2eAgInSc2d1	programovací
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
<g/>
++	++	k?	++
<g/>
.	.	kIx.	.
</s>
<s>
Bjarne	Bjarnout	k5eAaPmIp3nS	Bjarnout
Stroustrup	Stroustrup	k1gMnSc1	Stroustrup
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
C	C	kA	C
<g/>
++	++	k?	++
Programming	Programming	k1gInSc1	Programming
Language	language	k1gFnSc2	language
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-901507-2-1	[number]	k4	80-901507-2-1
Bjarne	Bjarn	k1gInSc5	Bjarn
Stroustrup	Stroustrup	k1gInSc1	Stroustrup
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Design	design	k1gInSc1	design
and	and	k?	and
Evolution	Evolution	k1gInSc1	Evolution
of	of	k?	of
C	C	kA	C
<g/>
++	++	k?	++
Margaret	Margareta	k1gFnPc2	Margareta
A.	A.	kA	A.
Ellis	Ellis	k1gFnSc2	Ellis
<g/>
,	,	kIx,	,
Bjarne	Bjarn	k1gInSc5	Bjarn
Stroustrup	Stroustrup	k1gMnSc1	Stroustrup
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Annotated	Annotated	k1gMnSc1	Annotated
C	C	kA	C
<g/>
++	++	k?	++
Reference	reference	k1gFnSc1	reference
Manual	Manual	k1gMnSc1	Manual
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bjarne	Bjarn	k1gInSc5	Bjarn
Stroustrup	Stroustrup	k1gInSc4	Stroustrup
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Bjarne	Bjarn	k1gInSc5	Bjarn
Stroustrup	Stroustrup	k1gInSc4	Stroustrup
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Domovská	domovský	k2eAgFnSc1d1	domovská
stránka	stránka	k1gFnSc1	stránka
</s>
