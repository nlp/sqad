<p>
<s>
Konchologie	Konchologie	k1gFnSc1	Konchologie
je	být	k5eAaImIp3nS	být
sběr	sběr	k1gInSc4	sběr
a	a	k8xC	a
studium	studium	k1gNnSc4	studium
schránek	schránka	k1gFnPc2	schránka
měkkýšů	měkkýš	k1gMnPc2	měkkýš
jako	jako	k8xS	jako
součást	součást	k1gFnSc4	součást
malakologie	malakologie	k1gFnSc2	malakologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konchologové	Koncholog	k1gMnPc1	Koncholog
(	(	kIx(	(
<g/>
lidé	člověk	k1gMnPc1	člověk
zabývající	zabývající	k2eAgMnPc1d1	zabývající
se	se	k3xPyFc4	se
konchologií	konchologie	k1gFnSc7	konchologie
<g/>
)	)	kIx)	)
studují	studovat	k5eAaImIp3nP	studovat
schránky	schránka	k1gFnPc1	schránka
měkkýšů	měkkýš	k1gMnPc2	měkkýš
aby	aby	kYmCp3nP	aby
porozuměli	porozumět	k5eAaPmAgMnP	porozumět
biodiverzitě	biodiverzita	k1gFnSc3	biodiverzita
a	a	k8xC	a
taxonomii	taxonomie	k1gFnSc4	taxonomie
měkkýšů	měkkýš	k1gMnPc2	měkkýš
nebo	nebo	k8xC	nebo
jednoduše	jednoduše	k6eAd1	jednoduše
oceňují	oceňovat	k5eAaImIp3nP	oceňovat
jejich	jejich	k3xOp3gFnSc4	jejich
estetickou	estetický	k2eAgFnSc4d1	estetická
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
seznam	seznam	k1gInSc1	seznam
malakologů	malakolog	k1gMnPc2	malakolog
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
seznam	seznam	k1gInSc1	seznam
konchologů	koncholog	k1gMnPc2	koncholog
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
Wikipedie	Wikipedie	k1gFnSc1	Wikipedie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
konchologie	konchologie	k1gFnSc2	konchologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Conchology	Concholog	k1gMnPc4	Concholog
<g/>
,	,	kIx,	,
Inc	Inc	k1gMnPc4	Inc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Worldwide	Worldwid	k1gMnSc5	Worldwid
Conchology	Concholog	k1gMnPc7	Concholog
</s>
</p>
