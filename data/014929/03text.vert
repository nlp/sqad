<s>
Antiguanská	Antiguanský	k2eAgFnSc1d1
a	a	k8xC
barbudská	barbudský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
</s>
<s>
Callaloo	Callaloo	k6eAd1
</s>
<s>
Pepperpot	Pepperpot	k1gMnSc1
</s>
<s>
Kuchyně	kuchyně	k1gFnSc1
Antiguy	Antigua	k1gFnSc2
a	a	k8xC
Barbudy	Barbuda	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
:	:	kIx,
Antigua	Antigu	k2eAgFnSc1d1
and	and	k?
Barbuda	Barbuda	k1gFnSc1
cuisine	cuisinout	k5eAaPmIp3nS
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
podobná	podobný	k2eAgFnSc1d1
ostatním	ostatní	k2eAgFnPc3d1
karibským	karibský	k2eAgFnPc3d1
kuchyním	kuchyně	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
typické	typický	k2eAgFnPc4d1
suroviny	surovina	k1gFnPc4
patří	patřit	k5eAaImIp3nS
zelenina	zelenina	k1gFnSc1
<g/>
,	,	kIx,
brambory	brambora	k1gFnPc1
<g/>
,	,	kIx,
rýže	rýže	k1gFnPc1
<g/>
,	,	kIx,
tropické	tropický	k2eAgNnSc1d1
ovoce	ovoce	k1gNnSc1
<g/>
,	,	kIx,
maso	maso	k1gNnSc1
<g/>
,	,	kIx,
ryby	ryba	k1gFnPc1
a	a	k8xC
mořské	mořský	k2eAgInPc1d1
plody	plod	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Příklady	příklad	k1gInPc1
pokrmů	pokrm	k1gInPc2
a	a	k8xC
nápojů	nápoj	k1gInPc2
z	z	k7c2
Antiguy	Antigua	k1gFnSc2
a	a	k8xC
Barbudy	Barbuda	k1gFnSc2
</s>
<s>
Příklady	příklad	k1gInPc1
pokrmů	pokrm	k1gInPc2
a	a	k8xC
nápojů	nápoj	k1gInPc2
z	z	k7c2
Antiguy	Antigua	k1gFnSc2
a	a	k8xC
Barbudy	Barbuda	k1gFnSc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fungie	Fungie	k1gFnSc1
<g/>
,	,	kIx,
směs	směs	k1gFnSc1
kukuřičné	kukuřičný	k2eAgFnSc2d1
mouky	mouka	k1gFnSc2
a	a	k8xC
okry	okr	k1gInPc4
<g/>
,	,	kIx,
podobná	podobný	k2eAgNnPc4d1
polentě	polenta	k1gFnSc6
</s>
<s>
Callaloo	Callaloo	k1gNnSc1
<g/>
,	,	kIx,
pokrm	pokrm	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
základem	základ	k1gInSc7
jsou	být	k5eAaImIp3nP
dušené	dušený	k2eAgInPc1d1
listy	list	k1gInPc1
taro	taro	k1gNnSc1
(	(	kIx(
<g/>
kolokázie	kolokázie	k1gFnSc1
jedlá	jedlý	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Pepperpot	Pepperpot	k1gInSc1
<g/>
,	,	kIx,
dušené	dušený	k2eAgNnSc1d1
maso	maso	k1gNnSc1
se	s	k7c7
zeleninou	zelenina	k1gFnSc7
</s>
<s>
Roti	Roti	k1gNnSc1
<g/>
,	,	kIx,
taštička	taštička	k1gFnSc1
plněná	plněný	k2eAgFnSc1d1
kari	kari	k1gNnSc7
</s>
<s>
Ducana	Ducana	k1gFnSc1
<g/>
,	,	kIx,
směs	směs	k1gFnSc1
z	z	k7c2
batátů	batáty	k1gInPc2
(	(	kIx(
<g/>
sladkých	sladký	k2eAgFnPc2d1
brambor	brambora	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
kokosu	kokos	k1gInSc2
podávaná	podávaný	k2eAgFnSc1d1
v	v	k7c6
banánovém	banánový	k2eAgInSc6d1
listě	list	k1gInSc6
</s>
<s>
Tamarind	tamarind	k1gInSc1
balls	balls	k1gInSc1
<g/>
,	,	kIx,
sladkost	sladkost	k1gFnSc1
z	z	k7c2
tamarindu	tamarind	k1gInSc2
</s>
<s>
Johnnycake	Johnnycake	k1gFnSc1
<g/>
,	,	kIx,
chléb	chléb	k1gInSc1
z	z	k7c2
kukuřičné	kukuřičný	k2eAgFnSc2d1
mouky	mouka	k1gFnSc2
</s>
<s>
Rum	rum	k1gInSc1
</s>
<s>
Ovocné	ovocný	k2eAgFnPc1d1
šťávy	šťáva	k1gFnPc1
</s>
<s>
Pivo	pivo	k1gNnSc1
</s>
<s>
Ting	Ting	k1gMnSc1
<g/>
,	,	kIx,
grepová	grepový	k2eAgFnSc1d1
limonáda	limonáda	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Antigua	Antigu	k1gInSc2
and	and	k?
Barbuda	Barbuda	k1gMnSc1
cuisine	cuisinout	k5eAaPmIp3nS
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
World	World	k1gMnSc1
Travel	Travel	k1gMnSc1
Guide	Guid	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Kuchyně	kuchyně	k1gFnSc1
severní	severní	k2eAgFnSc2d1
a	a	k8xC
střední	střední	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
•	•	k?
Mexiko	Mexiko	k1gNnSc1
•	•	k?
USA	USA	kA
•	•	k?
(	(	kIx(
<g/>
Grónsko	Grónsko	k1gNnSc1
<g/>
)	)	kIx)
Střední	střední	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Belize	Belize	k1gFnSc1
•	•	k?
Guatemala	Guatemala	k1gFnSc1
•	•	k?
Honduras	Honduras	k1gInSc1
•	•	k?
Kostarika	Kostarika	k1gFnSc1
•	•	k?
Nikaragua	Nikaragua	k1gFnSc1
•	•	k?
Panama	Panama	k1gFnSc1
•	•	k?
Salvador	Salvador	k1gInSc1
Karibské	karibský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
</s>
<s>
Antigua	Antigua	k1gFnSc1
a	a	k8xC
Barbuda	Barbuda	k1gFnSc1
•	•	k?
Bahamy	Bahamy	k1gFnPc4
•	•	k?
Barbados	Barbados	k1gInSc1
•	•	k?
Dominika	Dominik	k1gMnSc2
•	•	k?
Dominikánská	dominikánský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Grenada	Grenada	k1gFnSc1
•	•	k?
Haiti	Haiti	k1gNnSc2
•	•	k?
Jamajka	Jamajka	k1gFnSc1
•	•	k?
Kuba	Kuba	k1gFnSc1
•	•	k?
Svatá	svatý	k2eAgFnSc1d1
Lucie	Lucie	k1gFnSc1
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Kryštof	Kryštof	k1gMnSc1
a	a	k8xC
Nevis	viset	k5eNaImRp2nS
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Vincenc	Vincenc	k1gMnSc1
a	a	k8xC
Grenadiny	grenadina	k1gFnPc1
•	•	k?
Trinidad	Trinidad	k1gInSc1
a	a	k8xC
Tobago	Tobago	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Gastronomie	gastronomie	k1gFnSc1
|	|	kIx~
Střední	střední	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
a	a	k8xC
Karibik	Karibik	k1gInSc1
</s>
