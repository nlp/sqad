<s>
Katoda	katoda	k1gFnSc1	katoda
je	být	k5eAaImIp3nS	být
elektroda	elektroda	k1gFnSc1	elektroda
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
redukce	redukce	k1gFnSc1	redukce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
galvanického	galvanický	k2eAgInSc2d1	galvanický
článku	článek	k1gInSc2	článek
je	být	k5eAaImIp3nS	být
kladným	kladný	k2eAgNnSc7d1	kladné
pólem	pólo	k1gNnSc7	pólo
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
elektrochemického	elektrochemický	k2eAgInSc2d1	elektrochemický
článku	článek	k1gInSc2	článek
(	(	kIx(	(
<g/>
elektrolyzéru	elektrolyzér	k1gInSc2	elektrolyzér
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
záporným	záporný	k2eAgInSc7d1	záporný
pólem	pól	k1gInSc7	pól
<g/>
.	.	kIx.	.
</s>
<s>
Katoda	katoda	k1gFnSc1	katoda
tedy	tedy	k9	tedy
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jak	jak	k6eAd1	jak
kladná	kladný	k2eAgFnSc1d1	kladná
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
záporná	záporný	k2eAgFnSc1d1	záporná
<g/>
.	.	kIx.	.
</s>
<s>
Závisí	záviset	k5eAaImIp3nS	záviset
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
napětí	napětí	k1gNnSc1	napětí
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
emise	emise	k1gFnSc2	emise
elektronů	elektron	k1gInPc2	elektron
(	(	kIx(	(
<g/>
záporná	záporný	k2eAgFnSc1d1	záporná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
následkem	následkem	k7c2	následkem
emise	emise	k1gFnSc2	emise
elektronů	elektron	k1gInPc2	elektron
(	(	kIx(	(
<g/>
kladná	kladný	k2eAgFnSc1d1	kladná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protějškem	protějšek	k1gInSc7	protějšek
katody	katoda	k1gFnSc2	katoda
je	být	k5eAaImIp3nS	být
anoda	anoda	k1gFnSc1	anoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
elektrochemii	elektrochemie	k1gFnSc6	elektrochemie
se	se	k3xPyFc4	se
jako	jako	k9	jako
katoda	katoda	k1gFnSc1	katoda
označuje	označovat	k5eAaImIp3nS	označovat
elektroda	elektroda	k1gFnSc1	elektroda
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgFnPc4	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
redukce	redukce	k1gFnSc1	redukce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
vložení	vložení	k1gNnSc2	vložení
vnějšího	vnější	k2eAgNnSc2d1	vnější
napětí	napětí	k1gNnSc2	napětí
na	na	k7c4	na
elektrody	elektroda	k1gFnPc4	elektroda
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
při	při	k7c6	při
elektrolýze	elektrolýza	k1gFnSc6	elektrolýza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
katoda	katoda	k1gFnSc1	katoda
záporný	záporný	k2eAgInSc1d1	záporný
náboj	náboj	k1gInSc4	náboj
<g/>
,	,	kIx,	,
u	u	k7c2	u
galvanického	galvanický	k2eAgInSc2d1	galvanický
článku	článek	k1gInSc2	článek
kladný	kladný	k2eAgInSc4d1	kladný
náboj	náboj	k1gInSc4	náboj
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
akumulátoru	akumulátor	k1gInSc2	akumulátor
se	se	k3xPyFc4	se
funkce	funkce	k1gFnSc1	funkce
elektrod	elektroda	k1gFnPc2	elektroda
mění	měnit	k5eAaImIp3nS	měnit
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
akumulátor	akumulátor	k1gInSc1	akumulátor
nabíjí	nabíjet	k5eAaImIp3nS	nabíjet
nebo	nebo	k8xC	nebo
vybíjí	vybíjet	k5eAaImIp3nS	vybíjet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
elektronice	elektronika	k1gFnSc6	elektronika
je	být	k5eAaImIp3nS	být
katoda	katoda	k1gFnSc1	katoda
jádrem	jádro	k1gNnSc7	jádro
nelineárních	lineární	k2eNgInPc2d1	nelineární
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
součástek	součástka	k1gFnPc2	součástka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
dioda	dioda	k1gFnSc1	dioda
<g/>
,	,	kIx,	,
elektronka	elektronka	k1gFnSc1	elektronka
nebo	nebo	k8xC	nebo
tyristor	tyristor	k1gInSc1	tyristor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
katoda	katoda	k1gFnSc1	katoda
obvykle	obvykle	k6eAd1	obvykle
představuje	představovat	k5eAaImIp3nS	představovat
elektrodu	elektroda	k1gFnSc4	elektroda
se	s	k7c7	s
záporným	záporný	k2eAgNnSc7d1	záporné
napětím	napětí	k1gNnSc7	napětí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
polovodičové	polovodičový	k2eAgFnSc2d1	polovodičová
diody	dioda	k1gFnSc2	dioda
je	být	k5eAaImIp3nS	být
katoda	katoda	k1gFnSc1	katoda
polovodič	polovodič	k1gInSc1	polovodič
typu	typ	k1gInSc2	typ
N	N	kA	N
a	a	k8xC	a
v	v	k7c6	v
propustném	propustný	k2eAgInSc6d1	propustný
směru	směr	k1gInSc6	směr
je	být	k5eAaImIp3nS	být
připojena	připojen	k2eAgFnSc1d1	připojena
k	k	k7c3	k
zápornému	záporný	k2eAgNnSc3d1	záporné
napětí	napětí	k1gNnSc3	napětí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
emisi	emise	k1gFnSc3	emise
elektronů	elektron	k1gInPc2	elektron
ze	z	k7c2	z
studené	studený	k2eAgFnSc2d1	studená
katody	katoda	k1gFnSc2	katoda
dochází	docházet	k5eAaImIp3nS	docházet
až	až	k9	až
při	při	k7c6	při
přiložení	přiložení	k1gNnSc6	přiložení
velmi	velmi	k6eAd1	velmi
vysokého	vysoký	k2eAgNnSc2d1	vysoké
napětí	napětí	k1gNnSc2	napětí
<g/>
,	,	kIx,	,
emisi	emise	k1gFnSc4	emise
lze	lze	k6eAd1	lze
však	však	k9	však
výrazně	výrazně	k6eAd1	výrazně
posílit	posílit	k5eAaPmF	posílit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
katoda	katoda	k1gFnSc1	katoda
rozžhaví	rozžhavit	k5eAaPmIp3nS	rozžhavit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
u	u	k7c2	u
elektronek	elektronka	k1gFnPc2	elektronka
<g/>
,	,	kIx,	,
magnetronů	magnetron	k1gInPc2	magnetron
<g/>
,	,	kIx,	,
vakuových	vakuový	k2eAgFnPc2d1	vakuová
obrazovek	obrazovka	k1gFnPc2	obrazovka
atd.	atd.	kA	atd.
používají	používat	k5eAaImIp3nP	používat
vesměs	vesměs	k6eAd1	vesměs
žhavené	žhavený	k2eAgFnPc1d1	žhavená
katody	katoda	k1gFnPc1	katoda
<g/>
.	.	kIx.	.
</s>
<s>
Přímo	přímo	k6eAd1	přímo
žhavenou	žhavený	k2eAgFnSc4d1	žhavená
katodu	katoda	k1gFnSc4	katoda
tvoří	tvořit	k5eAaImIp3nS	tvořit
sama	sám	k3xTgFnSc1	sám
žhavicí	žhavicí	k2eAgFnSc1d1	žhavicí
spirála	spirála	k1gFnSc1	spirála
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
wolframového	wolframový	k2eAgInSc2d1	wolframový
drátku	drátek	k1gInSc2	drátek
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
řešení	řešení	k1gNnSc1	řešení
se	se	k3xPyFc4	se
dlouho	dlouho	k6eAd1	dlouho
používalo	používat	k5eAaImAgNnS	používat
u	u	k7c2	u
elektronek	elektronka	k1gFnPc2	elektronka
pro	pro	k7c4	pro
bateriové	bateriový	k2eAgNnSc4d1	bateriové
napájení	napájení	k1gNnSc4	napájení
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
pro	pro	k7c4	pro
provoz	provoz	k1gInSc4	provoz
z	z	k7c2	z
elektrické	elektrický	k2eAgFnSc2d1	elektrická
sítě	síť	k1gFnSc2	síť
je	on	k3xPp3gMnPc4	on
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
nepřímé	přímý	k2eNgNnSc4d1	nepřímé
žhavení	žhavení	k1gNnSc4	žhavení
<g/>
.	.	kIx.	.
</s>
<s>
Nepřímo	přímo	k6eNd1	přímo
žhavená	žhavený	k2eAgFnSc1d1	žhavená
katoda	katoda	k1gFnSc1	katoda
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
kovová	kovový	k2eAgFnSc1d1	kovová
trubička	trubička	k1gFnSc1	trubička
<g/>
,	,	kIx,	,
povlečená	povlečený	k2eAgFnSc1d1	povlečená
emisní	emisní	k2eAgFnSc7d1	emisní
hmotou	hmota	k1gFnSc7	hmota
s	s	k7c7	s
nízkým	nízký	k2eAgInSc7d1	nízký
emisním	emisní	k2eAgInSc7d1	emisní
potenciálem	potenciál	k1gInSc7	potenciál
<g/>
.	.	kIx.	.
</s>
<s>
Trubičkou	trubička	k1gFnSc7	trubička
prochází	procházet	k5eAaImIp3nS	procházet
izolovaná	izolovaný	k2eAgFnSc1d1	izolovaná
žhavicí	žhavicí	k2eAgFnSc1d1	žhavicí
spirála	spirála	k1gFnSc1	spirála
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
napájí	napájet	k5eAaImIp3nS	napájet
střídavým	střídavý	k2eAgInSc7d1	střídavý
proudem	proud	k1gInSc7	proud
o	o	k7c6	o
nízkém	nízký	k2eAgNnSc6d1	nízké
napětí	napětí	k1gNnSc6	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Elektronka	elektronka	k1gFnSc1	elektronka
Mnemotechnická	mnemotechnický	k2eAgFnSc1d1	mnemotechnická
pomůcka	pomůcka	k1gFnSc1	pomůcka
-	-	kIx~	-
jak	jak	k6eAd1	jak
si	se	k3xPyFc3	se
zapamatovat	zapamatovat	k5eAaPmF	zapamatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
katoda	katoda	k1gFnSc1	katoda
je	být	k5eAaImIp3nS	být
záporná	záporný	k2eAgFnSc1d1	záporná
elektroda	elektroda	k1gFnSc1	elektroda
Zářivka	zářivka	k1gFnSc1	zářivka
</s>
