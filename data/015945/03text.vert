<s>
Kirjat	Kirjat	k2eAgInSc1d1
Atidim	Atidim	k1gInSc1
</s>
<s>
Zástavba	zástavba	k1gFnSc1
v	v	k7c4
Kirjat	Kirjat	k2eAgInSc4d1
Atidim	Atidim	k1gInSc4
</s>
<s>
Rozestavěný	rozestavěný	k2eAgInSc1d1
mrakodrap	mrakodrap	k1gInSc1
Migdal	Migdal	k1gMnSc1
ha-Chazon	ha-Chazon	k1gMnSc1
v	v	k7c4
Kirjat	Kirjat	k2eAgInSc4d1
Atidim	Atidim	k1gInSc4
</s>
<s>
Kirjat	Kirjat	k1gInSc1
Atidim	Atidim	k1gInSc1
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
:	:	kIx,
ק	ק	k?
ע	ע	k?
<g/>
,	,	kIx,
doslova	doslova	k6eAd1
Město	město	k1gNnSc1
budoucnosti	budoucnost	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
čtvrť	čtvrť	k1gFnSc1
a	a	k8xC
průmyslová	průmyslový	k2eAgFnSc1d1
zóna	zóna	k1gFnSc1
v	v	k7c6
severovýchodní	severovýchodní	k2eAgFnSc6d1
části	část	k1gFnSc6
Tel	tel	kA
Avivu	Aviva	k1gFnSc4
v	v	k7c6
Izraeli	Izrael	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
správního	správní	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
Rova	Rov	k1gInSc2
2	#num#	k4
a	a	k8xC
samosprávné	samosprávný	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
Rova	Rova	k1gMnSc1
Cafon	Cafon	k1gMnSc1
Mizrach	Mizrach	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Leží	ležet	k5eAaImIp3nS
na	na	k7c6
severovýchodním	severovýchodní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
Tel	tel	kA
Avivu	Aviva	k1gFnSc4
<g/>
,	,	kIx,
cca	cca	kA
5,5	5,5	k4
kilometru	kilometr	k1gInSc2
od	od	k7c2
pobřeží	pobřeží	k1gNnSc2
Středozemního	středozemní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
na	na	k7c6
severním	severní	k2eAgInSc6d1
břehu	břeh	k1gInSc6
řeky	řeka	k1gFnSc2
Jarkon	Jarkona	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
cca	cca	kA
20	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
východ	východ	k1gInSc4
od	od	k7c2
čtvrti	čtvrt	k1gFnSc2
leží	ležet	k5eAaImIp3nS
fragment	fragment	k1gInSc4
zemědělské	zemědělský	k2eAgFnSc2d1
krajiny	krajina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
sever	sever	k1gInSc4
odtud	odtud	k6eAd1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
čtvrť	čtvrť	k1gFnSc1
Neve	Nev	k1gFnSc2
Šaret	Šareta	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
západě	západ	k1gInSc6
Ramat	Ramat	k2eAgInSc4d1
ha-Chajal	ha-Chajal	k1gInSc4
a	a	k8xC
Jisgav	Jisgav	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c6
jihozápadě	jihozápad	k1gInSc6
komerční	komerční	k2eAgFnSc1d1
zóna	zóna	k1gFnSc1
a	a	k8xC
v	v	k7c6
ní	on	k3xPp3gFnSc6
nový	nový	k2eAgInSc1d1
areál	areál	k1gInSc1
Nemocnice	nemocnice	k1gFnSc1
Asuta	Asuta	k1gMnSc1
Tel	tel	kA
Aviv	Aviv	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
čtvrti	čtvrt	k1gFnSc2
</s>
<s>
Zóna	zóna	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
roku	rok	k1gInSc2
1972	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gMnSc1
vznik	vznik	k1gInSc4
inicioval	iniciovat	k5eAaBmAgMnS
tehdejší	tehdejší	k2eAgMnSc1d1
starosta	starosta	k1gMnSc1
Tel	tel	kA
Avivu	Avivo	k1gNnSc3
Jehošua	Jehošuum	k1gNnSc2
Rabinovic	Rabinovice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Provoz	provoz	k1gInSc1
zóny	zóna	k1gFnSc2
řídí	řídit	k5eAaImIp3nS
firma	firma	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
poloviny	polovina	k1gFnSc2
vlastněna	vlastnit	k5eAaImNgFnS
městem	město	k1gNnSc7
Tel	tel	kA
Aviv	Aviv	k1gInSc1
z	z	k7c2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
Telavivskou	telavivský	k2eAgFnSc7d1
univerzitou	univerzita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
firma	firma	k1gFnSc1
vlastní	vlastnit	k5eAaImIp3nS
90	#num#	k4
dunamů	dunam	k1gInPc2
(	(	kIx(
<g/>
9	#num#	k4
hektarů	hektar	k1gInPc2
<g/>
)	)	kIx)
zdejších	zdejší	k2eAgInPc2d1
pozemků	pozemek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Jde	jít	k5eAaImIp3nS
o	o	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
nejmodernějších	moderní	k2eAgFnPc2d3
průmyslových	průmyslový	k2eAgFnPc2d1
zón	zóna	k1gFnPc2
zaměřených	zaměřený	k2eAgFnPc2d1
na	na	k7c4
moderní	moderní	k2eAgFnPc4d1
technologie	technologie	k1gFnPc4
v	v	k7c6
Izraeli	Izrael	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
tu	tu	k6eAd1
11	#num#	k4
budov	budova	k1gFnPc2
se	s	k7c7
125	#num#	k4
000	#num#	k4
čtverečními	čtvereční	k2eAgInPc7d1
metry	metr	k1gInPc7
vnitřních	vnitřní	k2eAgFnPc2d1
ploch	plocha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Probíhá	probíhat	k5eAaImIp3nS
další	další	k2eAgNnSc4d1
rozšiřování	rozšiřování	k1gNnSc4
areálu	areál	k1gInSc2
o	o	k7c4
věžové	věžový	k2eAgInPc4d1
objekty	objekt	k1gInPc4
(	(	kIx(
<g/>
takzvané	takzvaný	k2eAgFnPc4d1
Migdalej	Migdalej	k1gFnPc4
Atidim	Atidim	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
výstavbě	výstavba	k1gFnSc6
byl	být	k5eAaImAgInS
nejnověji	nově	k6eAd3
mrakodrap	mrakodrap	k1gInSc1
Migdal	Migdal	k1gFnPc2
ha-Chazon	ha-Chazon	k1gInSc1
(	(	kIx(
<g/>
Vision	vision	k1gInSc1
Tower	Tower	k1gInSc1
<g/>
)	)	kIx)
o	o	k7c6
výšce	výška	k1gFnSc6
téměř	téměř	k6eAd1
150	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
s	s	k7c7
110	#num#	k4
000	#num#	k4
čtverečními	čtvereční	k2eAgInPc7d1
metry	metr	k1gInPc7
plochy	plocha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
areálu	areál	k1gInSc6
Kirjat	Kirjat	k2eAgInSc1d1
Atidim	Atidim	k1gInSc1
sídlí	sídlet	k5eAaImIp3nS
100	#num#	k4
firem	firma	k1gFnPc2
a	a	k8xC
pracují	pracovat	k5eAaImIp3nP
tu	tu	k6eAd1
4000	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnPc2
komplexu	komplex	k1gInSc2
jsou	být	k5eAaImIp3nP
i	i	k9
restaurace	restaurace	k1gFnPc4
<g/>
,	,	kIx,
banka	banka	k1gFnSc1
<g/>
,	,	kIx,
poštovní	poštovní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
zubní	zubní	k2eAgFnSc1d1
klinika	klinika	k1gFnSc1
<g/>
,	,	kIx,
mateřská	mateřský	k2eAgFnSc1d1
škola	škola	k1gFnSc1
a	a	k8xC
další	další	k2eAgFnPc1d1
služby	služba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Kiryat	Kiryat	k2eAgMnSc1d1
Atidim	Atidim	k1gMnSc1
<g/>
,	,	kIx,
Tel	tel	kA
Aviv	Aviv	k1gInSc1
<g/>
,	,	kIx,
Israel	Israel	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gNnSc1
Maps	Mapsa	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
,	,	kIx,
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
amudanan	amudanan	k1gInSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
il	il	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
amudanan	amudanan	k1gInSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
il	il	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Quarters	Quarters	k1gInSc1
and	and	k?
Subquarters	Subquarters	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
tel-aviv	tel-avit	k5eAaPmDgInS
<g/>
.	.	kIx.
<g/>
gov	gov	k?
<g/>
.	.	kIx.
<g/>
il	il	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
kiryat-atidim	kiryat-atidim	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
kiryat-atidim	kiryat-atidim	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
čtvrtí	čtvrtit	k5eAaImIp3nS
v	v	k7c6
Tel	tel	kA
Avivu	Aviv	k1gInSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kirjat	Kirjat	k2eAgInSc4d1
Atidim	Atidim	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Izrael	Izrael	k1gInSc1
</s>
