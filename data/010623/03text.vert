<p>
<s>
Queen	Queen	k1gInSc1	Queen
je	být	k5eAaImIp3nS	být
britská	britský	k2eAgFnSc1d1	britská
rocková	rockový	k2eAgFnSc1d1	rocková
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
její	její	k3xOp3gFnSc6	její
původní	původní	k2eAgFnSc6d1	původní
sestavě	sestava	k1gFnSc6	sestava
byli	být	k5eAaImAgMnP	být
Freddie	Freddie	k1gFnPc4	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
(	(	kIx(	(
<g/>
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
klavír	klavír	k1gInSc1	klavír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Brian	Brian	k1gMnSc1	Brian
May	May	k1gMnSc1	May
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Deacon	Deacon	k1gMnSc1	Deacon
(	(	kIx(	(
<g/>
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
klávesy	kláves	k1gInPc1	kláves
<g/>
)	)	kIx)	)
a	a	k8xC	a
Roger	Roger	k1gMnSc1	Roger
Taylor	Taylor	k1gMnSc1	Taylor
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgFnPc1d1	bicí
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvotní	prvotní	k2eAgFnSc4d1	prvotní
tvorbu	tvorba	k1gFnSc4	tvorba
skupiny	skupina	k1gFnSc2	skupina
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
hlavně	hlavně	k6eAd1	hlavně
Progresivní	progresivní	k2eAgInSc4d1	progresivní
rock	rock	k1gInSc4	rock
<g/>
,	,	kIx,	,
Hard	Hard	k1gInSc1	Hard
rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
kapela	kapela	k1gFnSc1	kapela
pokoušela	pokoušet	k5eAaImAgFnS	pokoušet
o	o	k7c4	o
konvenčnější	konvenční	k2eAgFnSc4d2	konvenčnější
a	a	k8xC	a
komerčnější	komerční	k2eAgFnSc4d2	komerčnější
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
rozmanitějších	rozmanitý	k2eAgInPc2d2	rozmanitější
a	a	k8xC	a
inovatičnějších	inovatičný	k2eAgInPc2d2	inovatičný
žánrů	žánr	k1gInPc2	žánr
(	(	kIx(	(
<g/>
arena	arena	k6eAd1	arena
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
pop	pop	k1gInSc1	pop
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
funk	funk	k1gInSc1	funk
<g/>
,	,	kIx,	,
či	či	k8xC	či
R	R	kA	R
<g/>
&	&	k?	&
<g/>
B	B	kA	B
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
založením	založení	k1gNnSc7	založení
skupiny	skupina	k1gFnSc2	skupina
Queen	Queen	k2eAgMnSc1d1	Queen
<g/>
,	,	kIx,	,
Brian	Brian	k1gMnSc1	Brian
May	May	k1gMnSc1	May
a	a	k8xC	a
Roger	Roger	k1gMnSc1	Roger
Taylor	Taylor	k1gMnSc1	Taylor
hráli	hrát	k5eAaImAgMnP	hrát
společně	společně	k6eAd1	společně
s	s	k7c7	s
baskytaristou	baskytarista	k1gMnSc7	baskytarista
a	a	k8xC	a
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Timem	Tim	k1gMnSc7	Tim
Staffellem	Staffell	k1gMnSc7	Staffell
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Smile	smil	k1gInSc5	smil
<g/>
.	.	kIx.	.
</s>
<s>
Freddie	Freddie	k1gFnSc1	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
známý	známý	k2eAgMnSc1d1	známý
jeho	jeho	k3xOp3gNnSc7	jeho
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Farrokh	Farrokha	k1gFnPc2	Farrokha
Bulsara	Bulsar	k1gMnSc2	Bulsar
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
fanouškem	fanoušek	k1gMnSc7	fanoušek
této	tento	k3xDgFnSc2	tento
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Staffellově	Staffellův	k2eAgInSc6d1	Staffellův
odchodu	odchod	k1gInSc6	odchod
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
Freddie	Freddie	k1gFnSc1	Freddie
připojil	připojit	k5eAaPmAgMnS	připojit
<g/>
,	,	kIx,	,
a	a	k8xC	a
rok	rok	k1gInSc1	rok
nato	nato	k6eAd1	nato
si	se	k3xPyFc3	se
skupina	skupina	k1gFnSc1	skupina
změnila	změnit	k5eAaPmAgFnS	změnit
název	název	k1gInSc4	název
na	na	k7c4	na
''	''	k?	''
<g/>
Queen	Queen	k1gInSc4	Queen
<g/>
''	''	k?	''
<g/>
.	.	kIx.	.
</s>
<s>
Johna	John	k1gMnSc4	John
Deacona	Deacon	k1gMnSc4	Deacon
přijali	přijmout	k5eAaPmAgMnP	přijmout
do	do	k7c2	do
kapely	kapela	k1gFnSc2	kapela
před	před	k7c7	před
nahráváním	nahrávání	k1gNnSc7	nahrávání
jejich	jejich	k3xOp3gNnSc2	jejich
debutového	debutový	k2eAgNnSc2d1	debutové
alba	album	k1gNnSc2	album
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osmnáctkrát	osmnáctkrát	k6eAd1	osmnáctkrát
obsadily	obsadit	k5eAaPmAgFnP	obsadit
jejich	jejich	k3xOp3gFnPc1	jejich
nahrávky	nahrávka	k1gFnPc1	nahrávka
první	první	k4xOgFnSc4	první
příčku	příčka	k1gFnSc4	příčka
albových	albový	k2eAgFnPc2d1	albová
hitparád	hitparáda	k1gFnPc2	hitparáda
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Bohemian	bohemian	k1gInSc4	bohemian
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
historie	historie	k1gFnSc2	historie
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
množství	množství	k1gNnSc4	množství
ocenění	ocenění	k1gNnSc2	ocenění
za	za	k7c4	za
živé	živý	k2eAgNnSc4d1	živé
pojetí	pojetí	k1gNnSc4	pojetí
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
prodeje	prodej	k1gInSc2	prodej
alb	album	k1gNnPc2	album
i	i	k8xC	i
hudební	hudební	k2eAgInSc4d1	hudební
přínos	přínos	k1gInSc4	přínos
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
vystoupení	vystoupení	k1gNnPc1	vystoupení
na	na	k7c6	na
Live	Live	k1gFnPc6	Live
Aid	Aida	k1gFnPc2	Aida
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
bylo	být	k5eAaImAgNnS	být
zvoleno	zvolit	k5eAaPmNgNnS	zvolit
nejlepším	dobrý	k2eAgInSc7d3	nejlepší
koncertem	koncert	k1gInSc7	koncert
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
byli	být	k5eAaImAgMnP	být
uvedeni	uvést	k5eAaPmNgMnP	uvést
do	do	k7c2	do
Rock	rock	k1gInSc1	rock
and	and	k?	and
Roll	Roll	k1gInSc1	Roll
Hall	Hall	k1gInSc1	Hall
of	of	k?	of
Fame	Fam	k1gFnSc2	Fam
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
Hollywoodský	hollywoodský	k2eAgInSc4d1	hollywoodský
chodník	chodník	k1gInSc4	chodník
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
<g/>
Kapela	kapela	k1gFnSc1	kapela
se	se	k3xPyFc4	se
nerozpadla	rozpadnout	k5eNaPmAgFnS	rozpadnout
ani	ani	k9	ani
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Freddieho	Freddie	k1gMnSc2	Freddie
Mercuryho	Mercury	k1gMnSc2	Mercury
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
Johna	John	k1gMnSc2	John
Deacona	Deacon	k1gMnSc2	Deacon
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
novým	nový	k2eAgMnSc7d1	nový
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Paulem	Paul	k1gMnSc7	Paul
Rodgersem	Rodgers	k1gMnSc7	Rodgers
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
r.	r.	kA	r.
2005	[number]	k4	2005
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Queen	Quena	k1gFnPc2	Quena
+	+	kIx~	+
Paul	Paula	k1gFnPc2	Paula
Rodgers	Rodgersa	k1gFnPc2	Rodgersa
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
severoamerického	severoamerický	k2eAgNnSc2d1	severoamerické
turné	turné	k1gNnSc2	turné
<g/>
,	,	kIx,	,
Queen	Queen	k1gInSc4	Queen
navíc	navíc	k6eAd1	navíc
ohlásili	ohlásit	k5eAaPmAgMnP	ohlásit
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
–	–	k?	–
poprvé	poprvé	k6eAd1	poprvé
po	po	k7c6	po
třinácti	třináct	k4xCc6	třináct
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
nové	nový	k2eAgNnSc1d1	nové
album	album	k1gNnSc1	album
The	The	k1gMnPc2	The
Cosmos	Cosmos	k1gInSc4	Cosmos
Rocks	Rocksa	k1gFnPc2	Rocksa
<g/>
,	,	kIx,	,
turné	turné	k1gNnSc2	turné
k	k	k7c3	k
novému	nový	k2eAgInSc3d1	nový
počinu	počin	k1gInSc3	počin
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenán	k2eAgNnSc1d1	zaznamenáno
na	na	k7c6	na
DVD	DVD	kA	DVD
Live	Live	k1gFnSc1	Live
in	in	k?	in
Ukraine	Ukrain	k1gMnSc5	Ukrain
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
rovněž	rovněž	k9	rovněž
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
výběrové	výběrový	k2eAgNnSc1d1	výběrové
album	album	k1gNnSc1	album
Absolute	Absolut	k1gInSc5	Absolut
Greatest	Greatest	k1gInSc1	Greatest
,	,	kIx,	,
obsahující	obsahující	k2eAgInPc1d1	obsahující
největší	veliký	k2eAgInPc1d3	veliký
hity	hit	k1gInPc1	hit
Queenů	Queen	k1gInPc2	Queen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Queen	Queen	k1gInSc4	Queen
vynikali	vynikat	k5eAaImAgMnP	vynikat
nad	nad	k7c7	nad
ostatními	ostatní	k2eAgFnPc7d1	ostatní
skupinami	skupina	k1gFnPc7	skupina
svými	svůj	k3xOyFgFnPc7	svůj
originálními	originální	k2eAgFnPc7d1	originální
skladbami	skladba	k1gFnPc7	skladba
<g/>
,	,	kIx,	,
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
bylo	být	k5eAaImAgNnS	být
spojení	spojení	k1gNnSc1	spojení
hard	harda	k1gFnPc2	harda
rocku	rock	k1gInSc2	rock
s	s	k7c7	s
operou	opera	k1gFnSc7	opera
v	v	k7c4	v
Bohemian	bohemian	k1gInSc4	bohemian
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Freddieho	Freddie	k1gMnSc4	Freddie
hlasovým	hlasový	k2eAgInSc7d1	hlasový
rozsahem	rozsah	k1gInSc7	rozsah
(	(	kIx(	(
<g/>
až	až	k6eAd1	až
4	[number]	k4	4
oktávy	oktáv	k1gInPc4	oktáv
<g/>
)	)	kIx)	)
a	a	k8xC	a
showmanstvím	showmanství	k1gNnSc7	showmanství
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
zvuk	zvuk	k1gInSc4	zvuk
skupiny	skupina	k1gFnSc2	skupina
udávala	udávat	k5eAaImAgFnS	udávat
elektrická	elektrický	k2eAgFnSc1d1	elektrická
kytara	kytara	k1gFnSc1	kytara
Red	Red	k1gFnSc2	Red
Special	Special	k1gMnSc1	Special
Briana	Brian	k1gMnSc2	Brian
Maye	May	k1gMnSc2	May
<g/>
,	,	kIx,	,
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
ze	z	k7c2	z
starého	starý	k2eAgInSc2d1	starý
krbu	krb	k1gInSc2	krb
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
počest	počest	k1gFnSc4	počest
skupiny	skupina	k1gFnSc2	skupina
Queen	Queen	k1gInSc1	Queen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
natočen	natočit	k5eAaBmNgInS	natočit
film	film	k1gInSc1	film
Bohemian	bohemian	k1gInSc1	bohemian
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
obrovský	obrovský	k2eAgInSc4d1	obrovský
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
natáčení	natáčení	k1gNnSc6	natáčení
se	se	k3xPyFc4	se
podíleli	podílet	k5eAaImAgMnP	podílet
i	i	k9	i
2	[number]	k4	2
zbývající	zbývající	k2eAgMnPc1d1	zbývající
původní	původní	k2eAgMnPc1d1	původní
členové	člen	k1gMnPc1	člen
kapely	kapela	k1gFnSc2	kapela
<g/>
:	:	kIx,	:
Brian	Brian	k1gMnSc1	Brian
May	May	k1gMnSc1	May
a	a	k8xC	a
Roger	Roger	k1gMnSc1	Roger
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
1968	[number]	k4	1968
<g/>
–	–	k?	–
<g/>
1973	[number]	k4	1973
<g/>
:	:	kIx,	:
Začátky	začátek	k1gInPc1	začátek
===	===	k?	===
</s>
</p>
<p>
<s>
Kariéra	kariéra	k1gFnSc1	kariéra
skupiny	skupina	k1gFnSc2	skupina
Smile	smil	k1gInSc5	smil
odstartovala	odstartovat	k5eAaPmAgFnS	odstartovat
na	na	k7c6	na
chodbách	chodba	k1gFnPc6	chodba
londýnské	londýnský	k2eAgInPc4d1	londýnský
Imperial	Imperial	k1gInSc4	Imperial
College	Colleg	k1gFnSc2	Colleg
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
student	student	k1gMnSc1	student
astrofyziky	astrofyzika	k1gFnSc2	astrofyzika
Brian	Brian	k1gMnSc1	Brian
May	May	k1gMnSc1	May
pokusil	pokusit	k5eAaPmAgMnS	pokusit
sestavit	sestavit	k5eAaPmF	sestavit
soubor	soubor	k1gInSc4	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
adept	adept	k1gMnSc1	adept
zubního	zubní	k2eAgNnSc2d1	zubní
lékařství	lékařství	k1gNnSc2	lékařství
Roger	Roger	k1gMnSc1	Roger
Taylor	Taylor	k1gMnSc1	Taylor
a	a	k8xC	a
student	student	k1gMnSc1	student
Tim	Tim	k?	Tim
Staffell	Staffell	k1gMnSc1	Staffell
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
rychle	rychle	k6eAd1	rychle
přidali	přidat	k5eAaPmAgMnP	přidat
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
si	se	k3xPyFc3	se
říkat	říkat	k5eAaImF	říkat
Smile	smil	k1gInSc5	smil
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
1969	[number]	k4	1969
podepsali	podepsat	k5eAaPmAgMnP	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Mercury	Mercur	k1gMnPc7	Mercur
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
,	,	kIx,	,
vydali	vydat	k5eAaPmAgMnP	vydat
však	však	k9	však
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
singl	singl	k1gInSc4	singl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
naprostý	naprostý	k2eAgInSc4d1	naprostý
propadák	propadák	k1gInSc4	propadák
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
odchází	odcházet	k5eAaImIp3nS	odcházet
ze	z	k7c2	z
sestavy	sestava	k1gFnSc2	sestava
Tim	Tim	k?	Tim
Staffell	Staffell	k1gInSc1	Staffell
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zkusil	zkusit	k5eAaPmAgMnS	zkusit
své	svůj	k3xOyFgNnSc4	svůj
štěstí	štěstí	k1gNnSc4	štěstí
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
u	u	k7c2	u
Humpy	Humpa	k1gFnSc2	Humpa
Bong	bongo	k1gNnPc2	bongo
<g/>
.	.	kIx.	.
</s>
<s>
Tim	Tim	k?	Tim
přemluvil	přemluvit	k5eAaPmAgMnS	přemluvit
svého	svůj	k3xOyFgMnSc2	svůj
bývalého	bývalý	k2eAgMnSc2d1	bývalý
spolužáka	spolužák	k1gMnSc2	spolužák
z	z	k7c2	z
umělecké	umělecký	k2eAgFnSc2d1	umělecká
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
Farrokha	Farrokha	k1gMnSc1	Farrokha
Bulsaru	Bulsar	k1gInSc2	Bulsar
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
známého	známý	k2eAgMnSc4d1	známý
jako	jako	k9	jako
Freddie	Freddie	k1gFnSc1	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
ke	k	k7c3	k
Smile	smil	k1gInSc5	smil
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
zpíval	zpívat	k5eAaImAgInS	zpívat
s	s	k7c7	s
méně	málo	k6eAd2	málo
známými	známý	k2eAgFnPc7d1	známá
skupinami	skupina	k1gFnPc7	skupina
Wreckage	Wreckag	k1gFnSc2	Wreckag
a	a	k8xC	a
Sour	Sour	k1gMnSc1	Sour
Milk	Milk	k1gMnSc1	Milk
Sea	Sea	k1gMnSc1	Sea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rodák	rodák	k1gMnSc1	rodák
ze	z	k7c2	z
Zanzibaru	Zanzibar	k1gInSc2	Zanzibar
měl	mít	k5eAaImAgInS	mít
velice	velice	k6eAd1	velice
přesnou	přesný	k2eAgFnSc4d1	přesná
představu	představa	k1gFnSc4	představa
o	o	k7c6	o
podobě	podoba	k1gFnSc6	podoba
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Musíte	muset	k5eAaImIp2nP	muset
dělat	dělat	k5eAaImF	dělat
mnohem	mnohem	k6eAd1	mnohem
originálnější	originální	k2eAgFnPc4d2	originálnější
písničky	písnička	k1gFnPc4	písnička
a	a	k8xC	a
mnohem	mnohem	k6eAd1	mnohem
jasněji	jasně	k6eAd2	jasně
definovat	definovat	k5eAaBmF	definovat
způsob	způsob	k1gInSc4	způsob
vašeho	váš	k3xOp2gNnSc2	váš
hudebního	hudební	k2eAgNnSc2d1	hudební
vyjadřování	vyjadřování	k1gNnSc2	vyjadřování
<g/>
.	.	kIx.	.
</s>
<s>
Kdybych	kdyby	kYmCp1nS	kdyby
byl	být	k5eAaImAgMnS	být
vaším	váš	k3xOp2gMnSc7	váš
zpěvákem	zpěvák	k1gMnSc7	zpěvák
<g/>
,	,	kIx,	,
věděl	vědět	k5eAaImAgMnS	vědět
bych	by	kYmCp1nS	by
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
tehdy	tehdy	k6eAd1	tehdy
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
okamžitě	okamžitě	k6eAd1	okamžitě
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
souboru	soubor	k1gInSc2	soubor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
zkoušeli	zkoušet	k5eAaImAgMnP	zkoušet
<g/>
,	,	kIx,	,
skládali	skládat	k5eAaImAgMnP	skládat
(	(	kIx(	(
<g/>
skládali	skládat	k5eAaImAgMnP	skládat
všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgMnPc1	tři
<g/>
)	)	kIx)	)
a	a	k8xC	a
zdokonalovali	zdokonalovat	k5eAaImAgMnP	zdokonalovat
repertoár	repertoár	k1gInSc4	repertoár
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
předvedli	předvést	k5eAaPmAgMnP	předvést
jen	jen	k6eAd1	jen
párkrát	párkrát	k6eAd1	párkrát
na	na	k7c6	na
večírcích	večírek	k1gInPc6	večírek
kamarádům	kamarád	k1gMnPc3	kamarád
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
vyzkoušela	vyzkoušet	k5eAaPmAgFnS	vyzkoušet
šest	šest	k4xCc4	šest
basových	basový	k2eAgMnPc2d1	basový
kytaristů	kytarista	k1gMnPc2	kytarista
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Barryho	Barry	k1gMnSc2	Barry
Mitchella	Mitchell	k1gMnSc2	Mitchell
<g/>
,	,	kIx,	,
Mikea	Mikeus	k1gMnSc2	Mikeus
Grose	gros	k1gInSc5	gros
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
měli	mít	k5eAaImAgMnP	mít
jasno	jasno	k1gNnSc4	jasno
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
členem	člen	k1gInSc7	člen
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
John	John	k1gMnSc1	John
Deacon	Deacon	k1gMnSc1	Deacon
<g/>
,	,	kIx,	,
nejmladší	mladý	k2eAgMnSc1d3	nejmladší
člen	člen	k1gMnSc1	člen
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
přijali	přijmout	k5eAaPmAgMnP	přijmout
název	název	k1gInSc4	název
Queen	Quena	k1gFnPc2	Quena
<g/>
.	.	kIx.	.
</s>
<s>
Freddie	Freddie	k1gFnSc2	Freddie
to	ten	k3xDgNnSc4	ten
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jenom	jenom	k9	jenom
jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
královské	královský	k2eAgNnSc1d1	královské
a	a	k8xC	a
zní	znět	k5eAaImIp3nS	znět
nádherně	nádherně	k6eAd1	nádherně
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
silné	silný	k2eAgNnSc1d1	silné
slovo	slovo	k1gNnSc1	slovo
<g/>
,	,	kIx,	,
naprosto	naprosto	k6eAd1	naprosto
univerzální	univerzální	k2eAgInSc4d1	univerzální
a	a	k8xC	a
bezprostřední	bezprostřední	k2eAgInSc4d1	bezprostřední
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
průlom	průlom	k1gInSc1	průlom
přišel	přijít	k5eAaPmAgInS	přijít
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
začínají	začínat	k5eAaImIp3nP	začínat
objevovat	objevovat	k5eAaImF	objevovat
v	v	k7c6	v
nejrůznějších	různý	k2eAgInPc6d3	nejrůznější
londýnských	londýnský	k2eAgInPc6d1	londýnský
klubech	klub	k1gInPc6	klub
a	a	k8xC	a
barech	bar	k1gInPc6	bar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
si	se	k3xPyFc3	se
jich	on	k3xPp3gInPc2	on
všimli	všimnout	k5eAaPmAgMnP	všimnout
pracovníci	pracovník	k1gMnPc1	pracovník
Trident	Trident	k1gMnSc1	Trident
Audio	audio	k2eAgFnPc2d1	audio
Productions	Productionsa	k1gFnPc2	Productionsa
<g/>
.	.	kIx.	.
</s>
<s>
Podepisují	podepisovat	k5eAaImIp3nP	podepisovat
vydavatelskou	vydavatelský	k2eAgFnSc4d1	vydavatelská
<g/>
,	,	kIx,	,
producentskou	producentský	k2eAgFnSc4d1	producentská
a	a	k8xC	a
manažerskou	manažerský	k2eAgFnSc4d1	manažerská
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
příslušnost	příslušnost	k1gFnSc1	příslušnost
k	k	k7c3	k
mamutí	mamutí	k2eAgFnSc3d1	mamutí
společnosti	společnost	k1gFnSc3	společnost
EMI	EMI	kA	EMI
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
první	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
<g/>
,	,	kIx,	,
Keep	Keep	k1gInSc1	Keep
Yourself	Yourself	k1gInSc1	Yourself
Alive	Aliev	k1gFnSc2	Aliev
byl	být	k5eAaImAgInS	být
propadák	propadák	k1gInSc1	propadák
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc4	album
nahrávají	nahrávat	k5eAaImIp3nP	nahrávat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
studio	studio	k1gNnSc1	studio
volné	volný	k2eAgNnSc1d1	volné
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
většinou	většinou	k6eAd1	většinou
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Tisk	tisk	k1gInSc1	tisk
je	být	k5eAaImIp3nS	být
ztrhal	ztrhat	k5eAaPmAgInS	ztrhat
a	a	k8xC	a
Radio	radio	k1gNnSc1	radio
One	One	k1gFnSc2	One
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
hrát	hrát	k5eAaImF	hrát
tuto	tento	k3xDgFnSc4	tento
píseň	píseň	k1gFnSc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
první	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
Queen	Quena	k1gFnPc2	Quena
propadlo	propadnout	k5eAaPmAgNnS	propadnout
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
singl	singl	k1gInSc4	singl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
vychází	vycházet	k5eAaImIp3nS	vycházet
album	album	k1gNnSc4	album
Queen	Quena	k1gFnPc2	Quena
II	II	kA	II
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
i	i	k9	i
první	první	k4xOgInSc4	první
hit	hit	k1gInSc4	hit
Queenu	Queen	k1gInSc2	Queen
Seven	Seven	k2eAgInSc4d1	Seven
Seas	Seas	k1gInSc4	Seas
of	of	k?	of
Rhye	Rhye	k1gInSc1	Rhye
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skladba	skladba	k1gFnSc1	skladba
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
pomalá	pomalý	k2eAgFnSc1d1	pomalá
instrumentální	instrumentální	k2eAgFnSc1d1	instrumentální
skladba	skladba	k1gFnSc1	skladba
na	na	k7c6	na
albu	album	k1gNnSc6	album
Queen	Quena	k1gFnPc2	Quena
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Mercury	Mercur	k1gInPc4	Mercur
jí	jíst	k5eAaImIp3nS	jíst
přepracoval	přepracovat	k5eAaPmAgMnS	přepracovat
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
první	první	k4xOgMnSc1	první
hitový	hitový	k2eAgInSc1d1	hitový
singl	singl	k1gInSc1	singl
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
poprvé	poprvé	k6eAd1	poprvé
zažila	zažít	k5eAaPmAgFnS	zažít
masový	masový	k2eAgInSc4d1	masový
ohlas	ohlas	k1gInSc4	ohlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
Freddie	Freddie	k1gFnSc2	Freddie
vydává	vydávat	k5eAaImIp3nS	vydávat
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Larry	Larra	k1gFnSc2	Larra
Lurex	Lurex	k1gInSc1	Lurex
singl	singl	k1gInSc1	singl
I	i	k8xC	i
Can	Can	k1gMnSc1	Can
Hear	Hear	k1gMnSc1	Hear
the	the	k?	the
Music	Music	k1gMnSc1	Music
<g/>
,	,	kIx,	,
píseň	píseň	k1gFnSc1	píseň
amerických	americký	k2eAgFnPc2d1	americká
The	The	k1gFnPc2	The
Beach	Beacha	k1gFnPc2	Beacha
Boys	boy	k1gMnPc2	boy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1974	[number]	k4	1974
<g/>
–	–	k?	–
<g/>
1975	[number]	k4	1975
<g/>
:	:	kIx,	:
Sheer	Sheer	k1gInSc1	Sheer
Heart	Hearta	k1gFnPc2	Hearta
Attack	Attacko	k1gNnPc2	Attacko
až	až	k9	až
A	a	k9	a
Night	Night	k1gMnSc1	Night
at	at	k?	at
the	the	k?	the
Opera	opera	k1gFnSc1	opera
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
vydala	vydat	k5eAaPmAgFnS	vydat
skupina	skupina	k1gFnSc1	skupina
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
konečně	konečně	k6eAd1	konečně
prokázalo	prokázat	k5eAaPmAgNnS	prokázat
jejich	jejich	k3xOp3gFnPc4	jejich
možnosti	možnost	k1gFnPc4	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Sheer	Sheer	k1gInSc1	Sheer
Heart	Hearta	k1gFnPc2	Hearta
Attack	Attack	k1gInSc1	Attack
<g/>
,	,	kIx,	,
album	album	k1gNnSc1	album
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
převyšující	převyšující	k2eAgInPc1d1	převyšující
obě	dva	k4xCgFnPc1	dva
předcházející	předcházející	k2eAgFnPc1d1	předcházející
<g/>
,	,	kIx,	,
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
i	i	k9	i
klasickou	klasický	k2eAgFnSc4d1	klasická
Killer	Killer	k1gInSc1	Killer
Queen	Queno	k1gNnPc2	Queno
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
dokonalou	dokonalý	k2eAgFnSc4d1	dokonalá
souhru	souhra	k1gFnSc4	souhra
všech	všecek	k3xTgMnPc2	všecek
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
obsazuje	obsazovat	k5eAaImIp3nS	obsazovat
druhou	druhý	k4xOgFnSc4	druhý
příčku	příčka	k1gFnSc4	příčka
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
za	za	k7c4	za
píseň	píseň	k1gFnSc4	píseň
získává	získávat	k5eAaImIp3nS	získávat
cenu	cena	k1gFnSc4	cena
Zlatý	zlatý	k2eAgInSc4d1	zlatý
lev	lev	k1gInSc4	lev
<g/>
.	.	kIx.	.
</s>
<s>
Zaslouženě	zaslouženě	k6eAd1	zaslouženě
následovala	následovat	k5eAaImAgFnS	následovat
Seven	Seven	k2eAgInSc4d1	Seven
Seas	Seas	k1gInSc4	Seas
or	or	k?	or
Rhye	Rhy	k1gInSc2	Rhy
do	do	k7c2	do
první	první	k4xOgFnSc2	první
desítky	desítka	k1gFnSc2	desítka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ustálila	ustálit	k5eAaPmAgFnS	ustálit
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
připravila	připravit	k5eAaPmAgFnS	připravit
tak	tak	k6eAd1	tak
skupinu	skupina	k1gFnSc4	skupina
o	o	k7c4	o
první	první	k4xOgInSc4	první
vrchol	vrchol	k1gInSc4	vrchol
hitparád	hitparáda	k1gFnPc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
další	další	k2eAgFnSc7d1	další
písní	píseň	k1gFnSc7	píseň
<g/>
,	,	kIx,	,
Now	Now	k1gFnSc7	Now
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Here	Her	k1gMnSc2	Her
Briana	Brian	k1gMnSc2	Brian
Maye	May	k1gMnSc2	May
(	(	kIx(	(
<g/>
jedenáctá	jedenáctý	k4xOgFnSc1	jedenáctý
příčka	příčka	k1gFnSc1	příčka
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
)	)	kIx)	)
album	album	k1gNnSc1	album
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Queen	Queen	k1gInSc1	Queen
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgInSc1d1	schopen
tvořit	tvořit	k5eAaImF	tvořit
propracovanou	propracovaný	k2eAgFnSc4d1	propracovaná
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rok	rok	k1gInSc1	rok
1975	[number]	k4	1975
znamená	znamenat	k5eAaImIp3nS	znamenat
pro	pro	k7c4	pro
Queen	Queen	k2eAgInSc4d1	Queen
historický	historický	k2eAgInSc4d1	historický
milník	milník	k1gInSc4	milník
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
přelomové	přelomový	k2eAgNnSc1d1	přelomové
album	album	k1gNnSc1	album
A	a	k8xC	a
Night	Night	k1gInSc1	Night
at	at	k?	at
the	the	k?	the
Opera	opera	k1gFnSc1	opera
a	a	k8xC	a
svět	svět	k1gInSc1	svět
už	už	k6eAd1	už
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
rock	rock	k1gInSc1	rock
má	mít	k5eAaImIp3nS	mít
svého	svůj	k3xOyFgMnSc2	svůj
panovníka	panovník	k1gMnSc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Tažným	tažný	k2eAgMnSc7d1	tažný
koněm	kůň	k1gMnSc7	kůň
byl	být	k5eAaImAgMnS	být
Freddieho	Freddie	k1gMnSc4	Freddie
magnum	magnum	k1gInSc1	magnum
opus	opus	k1gInSc1	opus
Bohemian	bohemian	k1gInSc4	bohemian
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvynalézavějších	vynalézavý	k2eAgFnPc2d3	nejvynalézavější
a	a	k8xC	a
nejoslnivějších	oslnivý	k2eAgFnPc2d3	nejoslnivější
písní	píseň	k1gFnPc2	píseň
rockové	rockový	k2eAgFnSc2d1	rocková
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
je	být	k5eAaImIp3nS	být
ihned	ihned	k6eAd1	ihned
katapultována	katapultovat	k5eAaBmNgFnS	katapultovat
na	na	k7c4	na
první	první	k4xOgFnSc4	první
místo	místo	k7c2	místo
hitparád	hitparáda	k1gFnPc2	hitparáda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
udrží	udržet	k5eAaPmIp3nS	udržet
neskutečných	skutečný	k2eNgInPc2d1	neskutečný
9	[number]	k4	9
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
z	z	k7c2	z
Queenu	Queen	k1gInSc2	Queen
učinilo	učinit	k5eAaImAgNnS	učinit
rockovou	rockový	k2eAgFnSc4d1	rocková
megastar	megastara	k1gFnPc2	megastara
<g/>
.	.	kIx.	.
</s>
<s>
Freddie	Freddie	k1gFnSc1	Freddie
za	za	k7c4	za
Bohemian	bohemian	k1gInSc4	bohemian
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
získává	získávat	k5eAaImIp3nS	získávat
cenu	cena	k1gFnSc4	cena
Ivora	Ivor	k1gMnSc2	Ivor
Novella	Novell	k1gMnSc2	Novell
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
vzniká	vznikat	k5eAaImIp3nS	vznikat
videoklip	videoklip	k1gInSc1	videoklip
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c2	za
"	"	kIx"	"
<g/>
zakladatele	zakladatel	k1gMnSc2	zakladatel
hudebního	hudební	k2eAgNnSc2d1	hudební
videa	video	k1gNnSc2	video
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
skladby	skladba	k1gFnPc1	skladba
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
You	You	k1gFnSc2	You
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
My	my	k3xPp1nPc1	my
Best	Best	k1gInSc1	Best
Friend	Friend	k1gInSc1	Friend
Johna	John	k1gMnSc2	John
Deacona	Deacon	k1gMnSc2	Deacon
a	a	k8xC	a
Love	lov	k1gInSc5	lov
of	of	k?	of
my	my	k3xPp1nPc1	my
Life	Life	k1gNnSc7	Life
stávají	stávat	k5eAaImIp3nP	stávat
hity	hit	k1gInPc1	hit
<g/>
,	,	kIx,	,
mnohé	mnohý	k2eAgInPc1d1	mnohý
také	také	k9	také
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
osmiminutová	osmiminutový	k2eAgFnSc1d1	osmiminutová
vokální	vokální	k2eAgFnSc1d1	vokální
skladba	skladba	k1gFnSc1	skladba
Prophet	Propheta	k1gFnPc2	Propheta
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Song	song	k1gInSc1	song
nebo	nebo	k8xC	nebo
rocková	rockový	k2eAgFnSc1d1	rocková
verze	verze	k1gFnSc1	verze
britské	britský	k2eAgFnSc2d1	britská
hymny	hymna	k1gFnSc2	hymna
God	God	k1gFnSc2	God
Save	Sav	k1gFnSc2	Sav
the	the	k?	the
Queen	Queen	k1gInSc1	Queen
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Night	Night	k1gMnSc1	Night
at	at	k?	at
The	The	k1gFnSc1	The
Opera	opera	k1gFnSc1	opera
nepřinesla	přinést	k5eNaPmAgFnS	přinést
jen	jen	k9	jen
hity	hit	k1gInPc7	hit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poprvé	poprvé	k6eAd1	poprvé
i	i	k9	i
dnes	dnes	k6eAd1	dnes
legendární	legendární	k2eAgNnSc4d1	legendární
logo	logo	k1gNnSc4	logo
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
horoskopických	horoskopický	k2eAgNnPc2d1	horoskopický
znamení	znamení	k1gNnPc2	znamení
jejích	její	k3xOp3gMnPc2	její
členů	člen	k1gMnPc2	člen
(	(	kIx(	(
<g/>
rak	rak	k1gMnSc1	rak
<g/>
,	,	kIx,	,
panna	panna	k1gFnSc1	panna
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc1	dva
lvi	lev	k1gMnPc1	lev
<g/>
)	)	kIx)	)
a	a	k8xC	a
ptáka	pták	k1gMnSc4	pták
Fénixe	fénix	k1gMnSc4	fénix
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ochraňuje	ochraňovat	k5eAaImIp3nS	ochraňovat
písmeno	písmeno	k1gNnSc4	písmeno
Q.	Q.	kA	Q.
Obrázek	obrázek	k1gInSc1	obrázek
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Freddie	Freddie	k1gFnPc4	Freddie
Mercury	Mercura	k1gFnPc4	Mercura
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Night	Night	k1gMnSc1	Night
at	at	k?	at
the	the	k?	the
Opera	opera	k1gFnSc1	opera
je	být	k5eAaImIp3nS	být
jméno	jméno	k1gNnSc4	jméno
filmové	filmový	k2eAgFnSc2d1	filmová
grotesky	groteska	k1gFnSc2	groteska
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
bratří	bratr	k1gMnPc2	bratr
Marxů	Marx	k1gMnPc2	Marx
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Queen	Queen	k1gInSc1	Queen
si	se	k3xPyFc3	se
vždy	vždy	k6eAd1	vždy
zakládal	zakládat	k5eAaImAgMnS	zakládat
na	na	k7c6	na
podobě	podoba	k1gFnSc6	podoba
obalů	obal	k1gInPc2	obal
svých	svůj	k3xOyFgFnPc2	svůj
alb	alba	k1gFnPc2	alba
a	a	k8xC	a
singlů	singl	k1gInPc2	singl
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
některými	některý	k3yIgFnPc7	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byly	být	k5eAaImAgFnP	být
problémy	problém	k1gInPc4	problém
zvláště	zvláště	k9	zvláště
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
musela	muset	k5eAaImAgFnS	muset
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
tamní	tamní	k2eAgFnSc1d1	tamní
cenzura	cenzura	k1gFnSc1	cenzura
<g/>
,	,	kIx,	,
nelíbilo	líbit	k5eNaImAgNnS	líbit
se	se	k3xPyFc4	se
např.	např.	kA	např.
obnažené	obnažený	k2eAgFnSc2d1	obnažená
ženské	ženská	k1gFnSc2	ženská
pozadí	pozadí	k1gNnSc2	pozadí
na	na	k7c6	na
obálce	obálka	k1gFnSc6	obálka
singlu	singl	k1gInSc2	singl
Fat	fatum	k1gNnPc2	fatum
Bottomed	Bottomed	k1gInSc4	Bottomed
Girls	girl	k1gFnPc4	girl
nebo	nebo	k8xC	nebo
dvě	dva	k4xCgFnPc4	dva
svíjející	svíjející	k2eAgFnPc4d1	svíjející
se	se	k3xPyFc4	se
lidská	lidský	k2eAgNnPc1d1	lidské
těla	tělo	k1gNnPc1	tělo
na	na	k7c4	na
Body	bod	k1gInPc4	bod
Language	language	k1gFnSc2	language
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1976	[number]	k4	1976
<g/>
–	–	k?	–
<g/>
1979	[number]	k4	1979
<g/>
:	:	kIx,	:
A	a	k8xC	a
Day	Day	k1gFnSc1	Day
at	at	k?	at
the	the	k?	the
Races	Races	k1gInSc1	Races
až	až	k8xS	až
Live	Live	k1gFnSc1	Live
Killers	Killersa	k1gFnPc2	Killersa
===	===	k?	===
</s>
</p>
<p>
<s>
Queen	Queen	k1gInSc1	Queen
si	se	k3xPyFc3	se
získal	získat	k5eAaPmAgInS	získat
svět	svět	k1gInSc1	svět
svou	svůj	k3xOyFgFnSc7	svůj
obratností	obratnost	k1gFnSc7	obratnost
a	a	k8xC	a
vydal	vydat	k5eAaPmAgInS	vydat
další	další	k2eAgNnSc4d1	další
album	album	k1gNnSc4	album
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
vypůjčeným	vypůjčený	k2eAgInSc7d1	vypůjčený
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
filmu	film	k1gInSc2	film
bratři	bratr	k1gMnPc1	bratr
Marxů	Marx	k1gMnPc2	Marx
<g/>
,	,	kIx,	,
A	a	k8xC	a
Day	Day	k1gFnSc1	Day
at	at	k?	at
the	the	k?	the
Races	Races	k1gInSc1	Races
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
přesně	přesně	k6eAd1	přesně
rok	rok	k1gInSc4	rok
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
veleúspěšného	veleúspěšný	k2eAgNnSc2d1	veleúspěšné
alba	album	k1gNnSc2	album
A	a	k9	a
night	night	k1gMnSc1	night
at	at	k?	at
the	the	k?	the
opera	opera	k1gFnSc1	opera
přináší	přinášet	k5eAaImIp3nS	přinášet
album	album	k1gNnSc4	album
další	další	k2eAgFnSc4d1	další
smršť	smršť	k1gFnSc4	smršť
skvělých	skvělý	k2eAgFnPc2d1	skvělá
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
šokující	šokující	k2eAgFnSc2d1	šokující
Tie	Tie	k1gFnSc2	Tie
Your	Your	k1gMnSc1	Your
Mother	Mothra	k1gFnPc2	Mothra
Down	Down	k1gMnSc1	Down
<g/>
,	,	kIx,	,
Good	Good	k1gMnSc1	Good
Old-Fashioned	Old-Fashioned	k1gMnSc1	Old-Fashioned
Loverboy	Loverbo	k2eAgInPc4d1	Loverbo
a	a	k8xC	a
nejúspěšnější	úspěšný	k2eAgInPc4d3	nejúspěšnější
Somebody	Somebod	k1gInPc4	Somebod
to	ten	k3xDgNnSc1	ten
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
britské	britský	k2eAgFnSc2d1	britská
hitparády	hitparáda	k1gFnSc2	hitparáda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
září	září	k1gNnSc4	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
open	open	k1gInSc1	open
air	air	k?	air
koncert	koncert	k1gInSc1	koncert
v	v	k7c6	v
Hyde	Hyde	k1gFnSc6	Hyde
Parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
zhlédlo	zhlédnout	k5eAaPmAgNnS	zhlédnout
150	[number]	k4	150
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
šestého	šestý	k4xOgNnSc2	šestý
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
News	News	k1gInSc1	News
of	of	k?	of
the	the	k?	the
World	Worldo	k1gNnPc2	Worldo
<g/>
,	,	kIx,	,
vydaného	vydaný	k2eAgNnSc2d1	vydané
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
pocházejí	pocházet	k5eAaImIp3nP	pocházet
dva	dva	k4xCgInPc4	dva
neuvěřitelné	uvěřitelný	k2eNgInPc4d1	neuvěřitelný
a	a	k8xC	a
vžité	vžitý	k2eAgInPc4d1	vžitý
stadionové	stadionový	k2eAgInPc4d1	stadionový
hity	hit	k1gInPc4	hit
<g/>
:	:	kIx,	:
We	We	k1gFnSc1	We
Will	Will	k1gInSc1	Will
Rock	rock	k1gInSc1	rock
You	You	k1gMnSc2	You
a	a	k8xC	a
We	We	k1gMnSc2	We
Are	ar	k1gInSc5	ar
the	the	k?	the
Champions	Championsa	k1gFnPc2	Championsa
<g/>
.	.	kIx.	.
</s>
<s>
Skladby	skladba	k1gFnPc1	skladba
otevírající	otevírající	k2eAgFnSc2d1	otevírající
celé	celá	k1gFnSc2	celá
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
silnou	silný	k2eAgFnSc4d1	silná
sborovou	sborový	k2eAgFnSc4d1	sborová
linii	linie	k1gFnSc4	linie
a	a	k8xC	a
publikum	publikum	k1gNnSc1	publikum
se	se	k3xPyFc4	se
jimi	on	k3xPp3gNnPc7	on
rádo	rád	k2eAgNnSc1d1	rádo
nechává	nechávat	k5eAaImIp3nS	nechávat
doslova	doslova	k6eAd1	doslova
strhnout	strhnout	k5eAaPmF	strhnout
<g/>
.	.	kIx.	.
</s>
<s>
Queen	Queen	k1gInSc4	Queen
vyjíždějí	vyjíždět	k5eAaImIp3nP	vyjíždět
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roger	Roger	k1gMnSc1	Roger
Taylor	Taylor	k1gMnSc1	Taylor
hledal	hledat	k5eAaImAgMnS	hledat
větší	veliký	k2eAgNnSc4d2	veliký
uplatnění	uplatnění	k1gNnSc4	uplatnění
jako	jako	k8xS	jako
sólový	sólový	k2eAgMnSc1d1	sólový
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
jako	jako	k8xC	jako
první	první	k4xOgMnSc1	první
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
členů	člen	k1gMnPc2	člen
započal	započnout	k5eAaPmAgInS	započnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
svou	svůj	k3xOyFgFnSc4	svůj
sólovou	sólový	k2eAgFnSc4d1	sólová
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
Queen	Quena	k1gFnPc2	Quena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Album	album	k1gNnSc1	album
Jazz	jazz	k1gInSc1	jazz
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
sice	sice	k8xC	sice
neznamená	znamenat	k5eNaImIp3nS	znamenat
ovládnutí	ovládnutí	k1gNnSc1	ovládnutí
teritoria	teritorium	k1gNnSc2	teritorium
Milese	Milese	k1gFnSc2	Milese
Davise	Davise	k1gFnSc2	Davise
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
dostatek	dostatek	k1gInSc4	dostatek
materiálu	materiál	k1gInSc2	materiál
opravňujícího	opravňující	k2eAgInSc2d1	opravňující
takovýto	takovýto	k3xDgInSc4	takovýto
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
směs	směs	k1gFnSc1	směs
hitů	hit	k1gInPc2	hit
<g/>
,	,	kIx,	,
Fat	fatum	k1gNnPc2	fatum
Bottomed	Bottomed	k1gMnSc1	Bottomed
Girls	girl	k1gFnPc4	girl
<g/>
,	,	kIx,	,
mezitím	mezitím	k6eAd1	mezitím
úprk	úprk	k1gInSc1	úprk
k	k	k7c3	k
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Stop	stop	k1gInSc1	stop
Me	Me	k1gMnSc1	Me
Now	Now	k1gMnSc1	Now
nebo	nebo	k8xC	nebo
technická	technický	k2eAgFnSc1d1	technická
Bicycle	Bicycle	k1gFnSc1	Bicycle
Race	Rac	k1gFnSc2	Rac
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
skladbě	skladba	k1gFnSc6	skladba
Mustapha	Mustapha	k1gFnSc1	Mustapha
skupina	skupina	k1gFnSc1	skupina
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jde	jít	k5eAaImIp3nS	jít
píseň	píseň	k1gFnSc4	píseň
složit	složit	k5eAaPmF	složit
zejména	zejména	k9	zejména
ze	z	k7c2	z
tří	tři	k4xCgNnPc2	tři
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
Mustapha	Mustapha	k1gMnSc1	Mustapha
<g/>
,	,	kIx,	,
Ibrahim	Ibrahim	k1gMnSc1	Ibrahim
<g/>
,	,	kIx,	,
Allah	Allah	k1gMnSc1	Allah
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Freddie	Freddie	k1gFnSc2	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
dělil	dělit	k5eAaImAgInS	dělit
na	na	k7c6	na
řadových	řadový	k2eAgFnPc6d1	řadová
deskách	deska	k1gFnPc6	deska
o	o	k7c4	o
mikrofon	mikrofon	k1gInSc4	mikrofon
s	s	k7c7	s
Brianem	Brian	k1gMnSc7	Brian
Mayem	May	k1gMnSc7	May
a	a	k8xC	a
Rogerem	Roger	k1gMnSc7	Roger
Taylorem	Taylor	k1gMnSc7	Taylor
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pravidelně	pravidelně	k6eAd1	pravidelně
přispívali	přispívat	k5eAaImAgMnP	přispívat
na	na	k7c4	na
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
jednou	jednou	k6eAd1	jednou
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k8xC	i
dvěma	dva	k4xCgFnPc7	dva
písničkami	písnička	k1gFnPc7	písnička
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
měli	mít	k5eAaImAgMnP	mít
hlavní	hlavní	k2eAgInSc4d1	hlavní
vokál	vokál	k1gInSc4	vokál
(	(	kIx(	(
<g/>
John	John	k1gMnSc1	John
Deacon	Deacon	k1gMnSc1	Deacon
nikdy	nikdy	k6eAd1	nikdy
sólo	sólo	k2eAgMnSc1d1	sólo
nezpíval	zpívat	k5eNaImAgMnS	zpívat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
dostávali	dostávat	k5eAaImAgMnP	dostávat
prostor	prostor	k1gInSc4	prostor
jen	jen	k9	jen
na	na	k7c6	na
B-stranách	Btrana	k1gFnPc6	B-strana
singlů	singl	k1gInPc2	singl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Singl	singl	k1gInSc1	singl
Spread	Spread	k1gInSc1	Spread
Your	Your	k1gInSc1	Your
Wings	Wings	k1gInSc1	Wings
obsazuje	obsazovat	k5eAaImIp3nS	obsazovat
třicátou	třicátý	k4xOgFnSc4	třicátý
čtvrtou	čtvrtá	k1gFnSc4	čtvrtá
příčku	příčka	k1gFnSc4	příčka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1979	[number]	k4	1979
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
skupina	skupina	k1gFnSc1	skupina
na	na	k7c4	na
velké	velký	k2eAgNnSc4d1	velké
turné	turné	k1gNnSc4	turné
do	do	k7c2	do
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnPc1d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
Skladby	skladba	k1gFnPc1	skladba
z	z	k7c2	z
evropských	evropský	k2eAgInPc2d1	evropský
koncertů	koncert	k1gInPc2	koncert
jsou	být	k5eAaImIp3nP	být
nahrávány	nahráván	k2eAgFnPc1d1	nahrávána
a	a	k8xC	a
vydány	vydán	k2eAgFnPc1d1	vydána
jako	jako	k8xC	jako
živé	živý	k2eAgNnSc1d1	živé
album	album	k1gNnSc1	album
Live	Liv	k1gFnSc2	Liv
Killers	Killersa	k1gFnPc2	Killersa
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
obsazuje	obsazovat	k5eAaImIp3nS	obsazovat
třetí	třetí	k4xOgFnSc4	třetí
příčku	příčka	k1gFnSc4	příčka
hitparád	hitparáda	k1gFnPc2	hitparáda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc1	jeho
zvuk	zvuk	k1gInSc1	zvuk
je	být	k5eAaImIp3nS	být
nekvalitní	kvalitní	k2eNgMnSc1d1	nekvalitní
a	a	k8xC	a
Roger	Roger	k1gMnSc1	Roger
Taylor	Taylor	k1gMnSc1	Taylor
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
deska	deska	k1gFnSc1	deska
do	do	k7c2	do
diskografie	diskografie	k1gFnSc2	diskografie
Queen	Queen	k1gInSc1	Queen
oficiálně	oficiálně	k6eAd1	oficiálně
nepatří	patřit	k5eNaImIp3nS	patřit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1980	[number]	k4	1980
<g/>
–	–	k?	–
<g/>
1984	[number]	k4	1984
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Game	game	k1gInSc1	game
až	až	k9	až
The	The	k1gMnSc1	The
Works	Works	kA	Works
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Jazzu	jazz	k1gInSc6	jazz
přišla	přijít	k5eAaPmAgFnS	přijít
The	The	k1gFnSc1	The
Game	game	k1gInSc1	game
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znamenající	znamenající	k2eAgInSc1d1	znamenající
návrat	návrat	k1gInSc1	návrat
ke	k	k7c3	k
studiové	studiový	k2eAgFnSc3d1	studiová
práci	práce	k1gFnSc3	práce
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
ukazovalo	ukazovat	k5eAaImAgNnS	ukazovat
další	další	k2eAgFnPc4d1	další
možnosti	možnost	k1gFnPc4	možnost
kapely	kapela	k1gFnSc2	kapela
v	v	k7c6	v
písních	píseň	k1gFnPc6	píseň
Save	Sav	k1gFnSc2	Sav
Me	Me	k1gFnSc2	Me
<g/>
,	,	kIx,	,
silné	silný	k2eAgFnSc6d1	silná
baladě	balada	k1gFnSc6	balada
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
je	být	k5eAaImIp3nS	být
Freddieho	Freddie	k1gMnSc2	Freddie
vokál	vokál	k1gInSc1	vokál
rozvinut	rozvinut	k2eAgInSc1d1	rozvinut
naplno	naplno	k6eAd1	naplno
<g/>
,	,	kIx,	,
a	a	k8xC	a
Crazy	Craza	k1gFnPc1	Craza
Little	Little	k1gFnSc2	Little
Thing	Thing	k1gMnSc1	Thing
Called	Called	k1gMnSc1	Called
Love	lov	k1gInSc5	lov
<g/>
.	.	kIx.	.
</s>
<s>
Elvisovská	Elvisovský	k2eAgFnSc1d1	Elvisovský
skladba	skladba	k1gFnSc1	skladba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Freddie	Freddie	k1gFnSc1	Freddie
poprvé	poprvé	k6eAd1	poprvé
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c4	na
elektrickou	elektrický	k2eAgFnSc4d1	elektrická
kytaru	kytara	k1gFnSc4	kytara
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
bombou	bomba	k1gFnSc7	bomba
je	být	k5eAaImIp3nS	být
Another	Anothra	k1gFnPc2	Anothra
One	One	k1gMnSc1	One
Bites	Bites	k1gMnSc1	Bites
the	the	k?	the
Dust	Dust	k1gMnSc1	Dust
<g/>
,	,	kIx,	,
Johnova	Johnův	k2eAgFnSc1d1	Johnova
skladba	skladba	k1gFnSc1	skladba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
rytmem	rytmus	k1gInSc7	rytmus
funky	funk	k1gInPc1	funk
a	a	k8xC	a
úžasnou	úžasný	k2eAgFnSc7d1	úžasná
basovou	basový	k2eAgFnSc7d1	basová
linkou	linka	k1gFnSc7	linka
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
okupovali	okupovat	k5eAaBmAgMnP	okupovat
černí	černý	k1gMnPc1	černý
disco	disco	k1gNnSc6	disco
zpěváci	zpěvák	k1gMnPc1	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Publikum	publikum	k1gNnSc1	publikum
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
albu	album	k1gNnSc6	album
také	také	k9	také
ocenilo	ocenit	k5eAaPmAgNnS	ocenit
píseň	píseň	k1gFnSc4	píseň
Play	play	k0	play
The	The	k1gMnSc4	The
Game	game	k1gInSc1	game
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pak	pak	k6eAd1	pak
přišel	přijít	k5eAaPmAgInS	přijít
další	další	k2eAgInSc1d1	další
převrat	převrat	k1gInSc1	převrat
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jim	on	k3xPp3gMnPc3	on
zajistil	zajistit	k5eAaPmAgInS	zajistit
zcela	zcela	k6eAd1	zcela
odlišný	odlišný	k2eAgInSc1d1	odlišný
druh	druh	k1gInSc1	druh
publika	publikum	k1gNnSc2	publikum
–	–	k?	–
návštěvníky	návštěvník	k1gMnPc4	návštěvník
kin	kino	k1gNnPc2	kino
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
přivítali	přivítat	k5eAaPmAgMnP	přivítat
novou	nový	k2eAgFnSc4d1	nová
dekádu	dekáda	k1gFnSc4	dekáda
zkomponováním	zkomponování	k1gNnSc7	zkomponování
hudby	hudba	k1gFnSc2	hudba
k	k	k7c3	k
celovečernímu	celovečerní	k2eAgInSc3d1	celovečerní
filmu	film	k1gInSc3	film
<g/>
,	,	kIx,	,
přivádějícího	přivádějící	k2eAgMnSc4d1	přivádějící
na	na	k7c4	na
plátno	plátno	k1gNnSc1	plátno
nezničitelného	zničitelný	k2eNgMnSc4d1	nezničitelný
komiksového	komiksový	k2eAgMnSc4d1	komiksový
hrdinu	hrdina	k1gMnSc4	hrdina
Flashe	Flash	k1gMnSc2	Flash
Gordona	Gordon	k1gMnSc2	Gordon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
nebyla	být	k5eNaImAgFnS	být
spolupráce	spolupráce	k1gFnSc1	spolupráce
rocku	rock	k1gInSc2	rock
s	s	k7c7	s
filmem	film	k1gInSc7	film
obvyklá	obvyklý	k2eAgNnPc4d1	obvyklé
–	–	k?	–
film	film	k1gInSc4	film
ještě	ještě	k6eAd1	ještě
neobjevil	objevit	k5eNaPmAgMnS	objevit
výhody	výhoda	k1gFnPc4	výhoda
speciálně	speciálně	k6eAd1	speciálně
upravené	upravený	k2eAgFnSc2d1	upravená
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Mercury	Mercura	k1gFnPc1	Mercura
<g/>
,	,	kIx,	,
Deacon	Deacon	k1gMnSc1	Deacon
<g/>
,	,	kIx,	,
May	May	k1gMnSc1	May
a	a	k8xC	a
Taylor	Taylor	k1gMnSc1	Taylor
byli	být	k5eAaImAgMnP	být
prvními	první	k4xOgMnPc7	první
střihy	střih	k1gInPc1	střih
filmu	film	k1gInSc2	film
tak	tak	k6eAd1	tak
nadšeni	nadchnout	k5eAaPmNgMnP	nadchnout
<g/>
,	,	kIx,	,
až	až	k8xS	až
začnou	začít	k5eAaPmIp3nP	začít
tuto	tento	k3xDgFnSc4	tento
novou	nový	k2eAgFnSc4d1	nová
výzvu	výzva	k1gFnSc4	výzva
zdolávat	zdolávat	k5eAaImF	zdolávat
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
naprostý	naprostý	k2eAgInSc1d1	naprostý
úspěch	úspěch	k1gInSc1	úspěch
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dokazoval	dokazovat	k5eAaImAgInS	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vytvářet	vytvářet	k5eAaImF	vytvářet
novou	nový	k2eAgFnSc4d1	nová
hudbu	hudba	k1gFnSc4	hudba
k	k	k7c3	k
filmu	film	k1gInSc3	film
a	a	k8xC	a
mimochodem	mimochodem	k9	mimochodem
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
udělat	udělat	k5eAaPmF	udělat
i	i	k8xC	i
hit	hit	k1gInSc4	hit
<g/>
.	.	kIx.	.
</s>
<s>
Flash	Flash	k1gInSc1	Flash
<g/>
,	,	kIx,	,
vtipné	vtipný	k2eAgNnSc1d1	vtipné
spojení	spojení	k1gNnSc1	spojení
původního	původní	k2eAgInSc2d1	původní
zvuku	zvuk	k1gInSc2	zvuk
se	s	k7c7	s
hřmícím	hřmící	k2eAgInSc7d1	hřmící
sborem	sbor	k1gInSc7	sbor
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
prolétl	prolétnout	k5eAaPmAgInS	prolétnout
žebříčkem	žebříček	k1gInSc7	žebříček
singlů	singl	k1gInPc2	singl
a	a	k8xC	a
album	album	k1gNnSc4	album
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
desáté	desátý	k4xOgFnPc1	desátý
příčky	příčka	k1gFnPc1	příčka
hitparád	hitparáda	k1gFnPc2	hitparáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
dobrodružné	dobrodružný	k2eAgFnSc6d1	dobrodružná
práci	práce	k1gFnSc6	práce
vzniká	vznikat	k5eAaImIp3nS	vznikat
další	další	k2eAgNnSc4d1	další
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
Greatest	Greatest	k1gInSc4	Greatest
Hits	Hitsa	k1gFnPc2	Hitsa
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
další	další	k2eAgInSc1d1	další
skutečný	skutečný	k2eAgInSc1d1	skutečný
mezník	mezník	k1gInSc1	mezník
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejlépe	dobře	k6eAd3	dobře
prodávaná	prodávaný	k2eAgFnSc1d1	prodávaná
kompilace	kompilace	k1gFnSc1	kompilace
úspěšných	úspěšný	k2eAgFnPc2d1	úspěšná
písní	píseň	k1gFnPc2	píseň
nějaké	nějaký	k3yIgFnSc2	nějaký
skupiny	skupina	k1gFnSc2	skupina
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Přinášelo	přinášet	k5eAaImAgNnS	přinášet
10	[number]	k4	10
hitů	hit	k1gInPc2	hit
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
první	první	k4xOgFnSc2	první
desítky	desítka	k1gFnSc2	desítka
anglického	anglický	k2eAgInSc2d1	anglický
žebříčku	žebříček	k1gInSc2	žebříček
<g/>
,	,	kIx,	,
s	s	k7c7	s
vedoucí	vedoucí	k1gFnSc7	vedoucí
Bohemian	bohemian	k1gInSc4	bohemian
Rhapsody	Rhapsod	k1gMnPc4	Rhapsod
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
přehlídka	přehlídka	k1gFnSc1	přehlídka
těch	ten	k3xDgFnPc2	ten
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
písní	píseň	k1gFnPc2	píseň
Queen	Quena	k1gFnPc2	Quena
<g/>
,	,	kIx,	,
předvádějící	předvádějící	k2eAgFnSc1d1	předvádějící
jejich	jejich	k3xOp3gNnSc4	jejich
neuvěřitelné	uvěřitelný	k2eNgNnSc4d1	neuvěřitelné
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
vokální	vokální	k2eAgFnSc2d1	vokální
a	a	k8xC	a
instrumentální	instrumentální	k2eAgFnSc2d1	instrumentální
stránky	stránka	k1gFnSc2	stránka
projevu	projev	k1gInSc2	projev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uneseni	unést	k5eAaPmNgMnP	unést
úspěchem	úspěch	k1gInSc7	úspěch
sbírky	sbírka	k1gFnPc4	sbírka
svých	svůj	k3xOyFgInPc2	svůj
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
nahrát	nahrát	k5eAaPmF	nahrát
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
objevit	objevit	k5eAaPmF	objevit
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejúžasnějších	úžasný	k2eAgNnPc2d3	nejúžasnější
spojení	spojení	k1gNnPc2	spojení
hudebních	hudební	k2eAgFnPc2d1	hudební
megastar	megastara	k1gFnPc2	megastara
–	–	k?	–
Queen	Quena	k1gFnPc2	Quena
a	a	k8xC	a
David	David	k1gMnSc1	David
Bowie	Bowie	k1gFnSc2	Bowie
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
netypický	typický	k2eNgInSc1d1	netypický
duet	duet	k1gInSc1	duet
Under	Undra	k1gFnPc2	Undra
Pressure	Pressur	k1gMnSc5	Pressur
vyletěl	vyletět	k5eAaPmAgMnS	vyletět
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
britské	britský	k2eAgFnSc2d1	britská
hitparády	hitparáda	k1gFnSc2	hitparáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tuto	tento	k3xDgFnSc4	tento
fantastickou	fantastický	k2eAgFnSc4d1	fantastická
skladbu	skladba	k1gFnSc4	skladba
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
na	na	k7c6	na
albu	album	k1gNnSc6	album
Hot	hot	k0	hot
Space	Space	k1gFnSc2	Space
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
naznačovalo	naznačovat	k5eAaImAgNnS	naznačovat
ještě	ještě	k6eAd1	ještě
další	další	k2eAgFnSc4d1	další
tvář	tvář	k1gFnSc4	tvář
Queen	Quena	k1gFnPc2	Quena
–	–	k?	–
dobývání	dobývání	k1gNnSc4	dobývání
taneční	taneční	k2eAgFnSc2d1	taneční
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
již	již	k6eAd1	již
sklízela	sklízet	k5eAaImAgFnS	sklízet
úspěch	úspěch	k1gInSc4	úspěch
s	s	k7c7	s
disko	disko	k2eAgFnSc7d1	disko
skladbou	skladba	k1gFnSc7	skladba
z	z	k7c2	z
pera	pero	k1gNnSc2	pero
Johna	John	k1gMnSc2	John
Deacona	Deacon	k1gMnSc2	Deacon
Another	Anothra	k1gFnPc2	Anothra
One	One	k1gMnSc1	One
Bites	Bites	k1gMnSc1	Bites
the	the	k?	the
Dust	Dust	k1gMnSc1	Dust
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
zaútočit	zaútočit	k5eAaPmF	zaútočit
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
ovládly	ovládnout	k5eAaPmAgFnP	ovládnout
taneční	taneční	k2eAgInSc4d1	taneční
parket	parket	k1gInSc4	parket
<g/>
:	:	kIx,	:
Back	Back	k1gInSc4	Back
Chat	chata	k1gFnPc2	chata
<g/>
,	,	kIx,	,
Body	bod	k1gInPc4	bod
Language	language	k1gFnPc2	language
a	a	k8xC	a
Las	laso	k1gNnPc2	laso
Palabras	Palabrasa	k1gFnPc2	Palabrasa
de	de	k?	de
Amor	Amor	k1gMnSc1	Amor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jediný	jediný	k2eAgInSc1d1	jediný
hitparádový	hitparádový	k2eAgInSc1d1	hitparádový
úspěch	úspěch	k1gInSc1	úspěch
ji	on	k3xPp3gFnSc4	on
vyneslo	vynést	k5eAaPmAgNnS	vynést
právě	právě	k9	právě
prvenství	prvenství	k1gNnSc1	prvenství
se	se	k3xPyFc4	se
Bowiem	Bowius	k1gMnSc7	Bowius
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Neměnila	měnit	k5eNaImAgFnS	měnit
se	se	k3xPyFc4	se
jen	jen	k9	jen
hudba	hudba	k1gFnSc1	hudba
Queen	Quena	k1gFnPc2	Quena
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
Freddie	Freddie	k1gFnSc2	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
samotný	samotný	k2eAgMnSc1d1	samotný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
si	se	k3xPyFc3	se
zakládal	zakládat	k5eAaImAgInS	zakládat
většinou	většinou	k6eAd1	většinou
na	na	k7c6	na
dámských	dámský	k2eAgInPc6d1	dámský
koncertních	koncertní	k2eAgInPc6d1	koncertní
převlecích	převlek	k1gInPc6	převlek
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgFnP	být
šité	šitý	k2eAgFnPc1d1	šitá
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
míru	míra	k1gFnSc4	míra
<g/>
,	,	kIx,	,
na	na	k7c6	na
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
vlasech	vlas	k1gInPc6	vlas
a	a	k8xC	a
černém	černý	k2eAgInSc6d1	černý
laku	lak	k1gInSc6	lak
na	na	k7c4	na
nehty	nehet	k1gInPc4	nehet
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vše	všechen	k3xTgNnSc4	všechen
vyměnil	vyměnit	k5eAaPmAgMnS	vyměnit
za	za	k7c4	za
kožené	kožený	k2eAgInPc4d1	kožený
oblečky	obleček	k1gInPc4	obleček
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc4d1	krátký
sestřih	sestřih	k1gInSc4	sestřih
a	a	k8xC	a
hustý	hustý	k2eAgInSc4d1	hustý
knír	knír	k1gInSc4	knír
(	(	kIx(	(
<g/>
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nejcharakterističtější	charakteristický	k2eAgFnSc4d3	nejcharakterističtější
podobu	podoba	k1gFnSc4	podoba
Freddieho	Freddie	k1gMnSc2	Freddie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Hot	hot	k0	hot
Space	Space	k1gMnSc1	Space
v	v	k7c6	v
roce	rok	k1gInSc6	rok
nastala	nastat	k5eAaPmAgFnS	nastat
dvouletá	dvouletý	k2eAgFnSc1d1	dvouletá
přestávka	přestávka	k1gFnSc1	přestávka
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yIgFnSc2	který
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
členové	člen	k1gMnPc1	člen
věnovali	věnovat	k5eAaImAgMnP	věnovat
sólové	sólový	k2eAgFnSc3d1	sólová
práci	práce	k1gFnSc3	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
John	John	k1gMnSc1	John
Deacon	Deacon	k1gMnSc1	Deacon
natáčí	natáčet	k5eAaImIp3nS	natáčet
singl	singl	k1gInSc4	singl
s	s	k7c7	s
Man	Man	k1gMnSc1	Man
Friday	Fridaa	k1gFnSc2	Fridaa
and	and	k?	and
Jive	jive	k1gInSc1	jive
Junior	junior	k1gMnSc1	junior
<g/>
,	,	kIx,	,
Picking	Picking	k1gInSc1	Picking
Up	Up	k1gFnSc2	Up
Sounds	Soundsa	k1gFnPc2	Soundsa
<g/>
.	.	kIx.	.
</s>
<s>
Brian	Brian	k1gMnSc1	Brian
May	May	k1gMnSc1	May
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
Def	Def	k1gMnSc1	Def
Leppard	Leppard	k1gMnSc1	Leppard
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
produkuje	produkovat	k5eAaImIp3nS	produkovat
album	album	k1gNnSc1	album
skupiny	skupina	k1gFnSc2	skupina
Heavy	Heava	k1gFnSc2	Heava
Pettin	Pettina	k1gFnPc2	Pettina
a	a	k8xC	a
vydává	vydávat	k5eAaImIp3nS	vydávat
sólové	sólový	k2eAgNnSc1d1	sólové
minialbum	minialbum	k1gNnSc1	minialbum
Star	star	k1gInSc1	star
Fleet	Fleet	k1gMnSc1	Fleet
Project	Project	k1gMnSc1	Project
<g/>
.	.	kIx.	.
</s>
<s>
Freddie	Freddie	k1gFnSc1	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
natáčí	natáčet	k5eAaImIp3nS	natáčet
album	album	k1gNnSc1	album
Mr	Mr	k1gMnSc2	Mr
Bad	Bad	k1gMnSc2	Bad
Guy	Guy	k1gMnSc2	Guy
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
užitečná	užitečný	k2eAgFnSc1d1	užitečná
pauza	pauza	k1gFnSc1	pauza
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
sešli	sejít	k5eAaPmAgMnP	sejít
<g/>
,	,	kIx,	,
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
jedno	jeden	k4xCgNnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgNnPc2	svůj
vůbec	vůbec	k9	vůbec
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
alb	alba	k1gFnPc2	alba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
The	The	k?	The
Works	Works	kA	Works
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
nejen	nejen	k6eAd1	nejen
přitáhly	přitáhnout	k5eAaPmAgInP	přitáhnout
nové	nový	k2eAgMnPc4d1	nový
posluchače	posluchač	k1gMnPc4	posluchač
<g/>
,	,	kIx,	,
také	také	k9	také
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jejích	její	k3xOp3gFnPc2	její
nejúspěšnější	úspěšný	k2eAgInSc4d3	nejúspěšnější
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Nabízelo	nabízet	k5eAaImAgNnS	nabízet
velmi	velmi	k6eAd1	velmi
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
písně	píseň	k1gFnSc2	píseň
<g/>
,	,	kIx,	,
od	od	k7c2	od
sci-fi	scii	k1gNnPc2	sci-fi
vize	vize	k1gFnSc2	vize
Radio	radio	k1gNnSc1	radio
Ga	Ga	k1gMnPc2	Ga
Ga	Ga	k1gFnSc2	Ga
až	až	k9	až
k	k	k7c3	k
rockovému	rockový	k2eAgInSc3d1	rockový
stompu	stomp	k1gInSc3	stomp
Hammer	Hammer	k1gInSc4	Hammer
to	ten	k3xDgNnSc1	ten
Fall	Fall	k1gMnSc1	Fall
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
notě	nota	k1gFnSc6	nota
nesl	nést	k5eAaImAgMnS	nést
pečeť	pečeť	k1gFnSc4	pečeť
Briana	Brian	k1gMnSc2	Brian
Maye	May	k1gMnSc2	May
<g/>
.	.	kIx.	.
</s>
<s>
Dál	daleko	k6eAd2	daleko
tu	tu	k6eAd1	tu
byly	být	k5eAaImAgInP	být
hity	hit	k1gInPc1	hit
It	It	k1gFnSc2	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
a	a	k8xC	a
Hard	Harda	k1gFnPc2	Harda
Life	Life	k1gFnPc3	Life
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
I	i	k9	i
Want	Want	k1gMnSc1	Want
to	ten	k3xDgNnSc1	ten
Break	break	k1gInSc4	break
Free	Fre	k1gMnSc2	Fre
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
Mercury	Mercura	k1gFnSc2	Mercura
podává	podávat	k5eAaImIp3nS	podávat
skvělý	skvělý	k2eAgInSc4d1	skvělý
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
exceluje	excelovat	k5eAaImIp3nS	excelovat
nad	nad	k7c7	nad
nástroji	nástroj	k1gInPc7	nástroj
ve	v	k7c6	v
skladbě	skladba	k1gFnSc6	skladba
o	o	k7c6	o
emancipaci	emancipace	k1gFnSc6	emancipace
a	a	k8xC	a
volnosti	volnost	k1gFnSc6	volnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
hymnou	hymna	k1gFnSc7	hymna
jejich	jejich	k3xOp3gMnPc2	jejich
fanoušků	fanoušek	k1gMnPc2	fanoušek
v	v	k7c6	v
represivních	represivní	k2eAgInPc6d1	represivní
režimech	režim	k1gInPc6	režim
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc3d1	jižní
Africe	Afrika	k1gFnSc3	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Přehršel	přehršel	k1gFnSc1	přehršel
hitovek	hitovka	k1gFnPc2	hitovka
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
píseň	píseň	k1gFnSc1	píseň
Keep	Keep	k1gInSc4	Keep
Passing	Passing	k1gInSc1	Passing
the	the	k?	the
Open	Opena	k1gFnPc2	Opena
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
alba	album	k1gNnSc2	album
byla	být	k5eAaImAgFnS	být
také	také	k9	také
zařazena	zařazen	k2eAgFnSc1d1	zařazena
skladba	skladba	k1gFnSc1	skladba
Is	Is	k1gFnSc2	Is
This	Thisa	k1gFnPc2	Thisa
the	the	k?	the
World	World	k1gMnSc1	World
We	We	k1gMnSc1	We
Created	Created	k1gMnSc1	Created
<g/>
...	...	k?	...
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Mercury	Mercur	k1gInPc1	Mercur
s	s	k7c7	s
Mayem	May	k1gMnSc7	May
předvedli	předvést	k5eAaPmAgMnP	předvést
na	na	k7c6	na
gigantickém	gigantický	k2eAgInSc6d1	gigantický
koncertu	koncert	k1gInSc6	koncert
Live	Liv	k1gFnSc2	Liv
Aid	Aida	k1gFnPc2	Aida
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
provádí	provádět	k5eAaImIp3nS	provádět
skupina	skupina	k1gFnSc1	skupina
zřejmě	zřejmě	k6eAd1	zřejmě
svou	svůj	k3xOyFgFnSc4	svůj
nejkontroverznější	kontroverzní	k2eAgFnSc4d3	nejkontroverznější
akci	akce	k1gFnSc4	akce
–	–	k?	–
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
před	před	k7c7	před
mnohatisícovými	mnohatisícový	k2eAgInPc7d1	mnohatisícový
davy	dav	k1gInPc7	dav
v	v	k7c6	v
jihoafrickém	jihoafrický	k2eAgNnSc6d1	jihoafrické
Sun	Sun	kA	Sun
City	city	k1gNnSc1	city
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
se	se	k3xPyFc4	se
setkalo	setkat	k5eAaPmAgNnS	setkat
s	s	k7c7	s
mohutnou	mohutný	k2eAgFnSc7d1	mohutná
dávkou	dávka	k1gFnSc7	dávka
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Brian	Brian	k1gMnSc1	Brian
May	May	k1gMnSc1	May
vše	všechen	k3xTgNnSc4	všechen
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsme	být	k5eAaImIp1nP	být
rozhodně	rozhodně	k6eAd1	rozhodně
proti	proti	k7c3	proti
apartheidu	apartheid	k1gInSc3	apartheid
a	a	k8xC	a
všemu	všecek	k3xTgNnSc3	všecek
podobnému	podobný	k2eAgNnSc3d1	podobné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsme	být	k5eAaImIp1nP	být
tak	tak	k6eAd1	tak
vystavěli	vystavět	k5eAaPmAgMnP	vystavět
velký	velký	k2eAgInSc4d1	velký
kus	kus	k1gInSc4	kus
mostu	most	k1gInSc2	most
mezi	mezi	k7c7	mezi
národy	národ	k1gInPc7	národ
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
vychází	vycházet	k5eAaImIp3nS	vycházet
vánoční	vánoční	k2eAgInSc4d1	vánoční
singl	singl	k1gInSc4	singl
Thank	Thank	k1gMnSc1	Thank
God	God	k1gMnSc1	God
It	It	k1gMnSc1	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Christmas	Christmas	k1gInSc1	Christmas
<g/>
,	,	kIx,	,
příčka	příčka	k1gFnSc1	příčka
dvacet	dvacet	k4xCc4	dvacet
jedna	jeden	k4xCgFnSc1	jeden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
1988	[number]	k4	1988
<g/>
:	:	kIx,	:
Live	Liv	k1gInSc2	Liv
Aid	Aida	k1gFnPc2	Aida
a	a	k8xC	a
pozdější	pozdní	k2eAgMnSc1d2	pozdější
leta	leta	k1gMnSc1	leta
===	===	k?	===
</s>
</p>
<p>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1985	[number]	k4	1985
–	–	k?	–
největší	veliký	k2eAgNnSc4d3	veliký
vystoupení	vystoupení	k1gNnSc4	vystoupení
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
!	!	kIx.	!
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
Live	Live	k1gNnSc2	Live
Aid	Aida	k1gFnPc2	Aida
představil	představit	k5eAaPmAgMnS	představit
sbírku	sbírka	k1gFnSc4	sbírka
toho	ten	k3xDgNnSc2	ten
nejlepšího	dobrý	k2eAgNnSc2d3	nejlepší
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
svět	svět	k1gInSc1	svět
nabízí	nabízet	k5eAaImIp3nS	nabízet
–	–	k?	–
Queen	Queen	k1gInSc1	Queen
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gFnSc2	McCartnea
<g/>
,	,	kIx,	,
Elton	Elton	k1gMnSc1	Elton
John	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Bowie	Bowie	k1gFnSc1	Bowie
<g/>
,	,	kIx,	,
Led	led	k1gInSc1	led
Zeppelin	Zeppelina	k1gFnPc2	Zeppelina
<g/>
,	,	kIx,	,
U	u	k7c2	u
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Who	Who	k1gMnSc1	Who
<g/>
,	,	kIx,	,
Mick	Mick	k1gMnSc1	Mick
Jagger	Jagger	k1gMnSc1	Jagger
<g/>
,	,	kIx,	,
Status	status	k1gInSc1	status
Quo	Quo	k1gFnSc2	Quo
<g/>
,	,	kIx,	,
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
<g/>
,	,	kIx,	,
Sting	Sting	k1gMnSc1	Sting
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
pro	pro	k7c4	pro
pomoc	pomoc	k1gFnSc4	pomoc
hladem	hlad	k1gInSc7	hlad
trpící	trpící	k2eAgFnSc3d1	trpící
Etiopii	Etiopie	k1gFnSc3	Etiopie
sledovalo	sledovat	k5eAaImAgNnS	sledovat
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
ve	v	k7c4	v
Wembley	Wemblea	k1gFnPc4	Wemblea
přes	přes	k7c4	přes
72	[number]	k4	72
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
Johna	John	k1gMnSc2	John
F.	F.	kA	F.
Kennedyho	Kennedy	k1gMnSc2	Kennedy
ve	v	k7c6	v
Philadelphii	Philadelphia	k1gFnSc6	Philadelphia
99	[number]	k4	99
000	[number]	k4	000
a	a	k8xC	a
televizní	televizní	k2eAgInSc1d1	televizní
přenos	přenos	k1gInSc1	přenos
sledovaly	sledovat	k5eAaImAgInP	sledovat
další	další	k2eAgFnPc4d1	další
2	[number]	k4	2
miliardy	miliarda	k4xCgFnPc4	miliarda
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
šedesáti	šedesát	k4xCc6	šedesát
zemích	zem	k1gFnPc6	zem
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
když	když	k8xS	když
každá	každý	k3xTgFnSc1	každý
skupina	skupina	k1gFnSc1	skupina
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
vystoupení	vystoupení	k1gNnSc6	vystoupení
jen	jen	k9	jen
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
Queen	Quena	k1gFnPc2	Quena
vlétli	vlétnout	k5eAaPmAgMnP	vlétnout
na	na	k7c4	na
pódium	pódium	k1gNnSc4	pódium
s	s	k7c7	s
největšími	veliký	k2eAgInPc7d3	veliký
hity	hit	k1gInPc7	hit
<g/>
:	:	kIx,	:
Bohemian	bohemian	k1gInSc1	bohemian
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
<g/>
,	,	kIx,	,
Hammer	Hammer	k1gInSc1	Hammer
to	ten	k3xDgNnSc1	ten
Fall	Fall	k1gInSc4	Fall
<g/>
,	,	kIx,	,
Radio	radio	k1gNnSc4	radio
GaGa	GaG	k1gInSc2	GaG
<g/>
,	,	kIx,	,
Crazy	Craza	k1gFnSc2	Craza
Little	Little	k1gFnSc2	Little
Thing	Thing	k1gMnSc1	Thing
Called	Called	k1gMnSc1	Called
Love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
We	We	k1gMnSc7	We
Will	Willa	k1gFnPc2	Willa
Rock	rock	k1gInSc1	rock
You	You	k1gMnSc1	You
a	a	k8xC	a
We	We	k1gMnSc1	We
Are	ar	k1gInSc5	ar
the	the	k?	the
Champions	Championsa	k1gFnPc2	Championsa
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
každým	každý	k3xTgInSc7	každý
svým	svůj	k3xOyFgInSc7	svůj
tónem	tón	k1gInSc7	tón
si	se	k3xPyFc3	se
Mercury	Mercur	k1gInPc4	Mercur
omotával	omotávat	k5eAaImAgInS	omotávat
dav	dav	k1gInSc1	dav
víc	hodně	k6eAd2	hodně
a	a	k8xC	a
víc	hodně	k6eAd2	hodně
kolem	kolem	k7c2	kolem
prstu	prst	k1gInSc2	prst
do	do	k7c2	do
ovládnutí	ovládnutí	k1gNnSc2	ovládnutí
celého	celý	k2eAgInSc2d1	celý
davu	dav	k1gInSc2	dav
–	–	k?	–
celý	celý	k2eAgInSc1d1	celý
stadion	stadion	k1gInSc1	stadion
zpívá	zpívat	k5eAaImIp3nS	zpívat
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Pouhých	pouhý	k2eAgFnPc2d1	pouhá
20	[number]	k4	20
minut	minuta	k1gFnPc2	minuta
stačilo	stačit	k5eAaBmAgNnS	stačit
Queen	Queen	k2eAgInSc1d1	Queen
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
hvězdami	hvězda	k1gFnPc7	hvězda
večera	večer	k1gInSc2	večer
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
den	den	k1gInSc1	den
snad	snad	k9	snad
všechny	všechen	k3xTgFnPc1	všechen
noviny	novina	k1gFnPc1	novina
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
měly	mít	k5eAaImAgInP	mít
na	na	k7c6	na
titulní	titulní	k2eAgFnSc6d1	titulní
straně	strana	k1gFnSc6	strana
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Queen	Queen	k2eAgMnSc1d1	Queen
is	is	k?	is
King	King	k1gMnSc1	King
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
vystoupení	vystoupení	k1gNnSc1	vystoupení
Queen	Quena	k1gFnPc2	Quena
během	během	k7c2	během
Live	Liv	k1gInSc2	Liv
Aid	Aida	k1gFnPc2	Aida
označeno	označit	k5eAaPmNgNnS	označit
v	v	k7c6	v
průzkumu	průzkum	k1gInSc6	průzkum
televize	televize	k1gFnSc1	televize
Channel	Channel	k1gInSc1	Channel
4	[number]	k4	4
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
živé	živý	k2eAgNnSc4d1	živé
koncertní	koncertní	k2eAgNnSc4d1	koncertní
vystoupení	vystoupení	k1gNnSc4	vystoupení
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
se	se	k3xPyFc4	se
v	v	k7c6	v
brazilském	brazilský	k2eAgMnSc6d1	brazilský
Rio	Rio	k1gMnSc6	Rio
de	de	k?	de
Janeiru	Janeira	k1gMnSc4	Janeira
konal	konat	k5eAaImAgInS	konat
na	na	k7c6	na
speciálně	speciálně	k6eAd1	speciálně
postaveném	postavený	k2eAgInSc6d1	postavený
stadionu	stadion	k1gInSc6	stadion
v	v	k7c6	v
Barra	Barra	k1gFnSc1	Barra
da	da	k?	da
Tajuca	Tajuca	k1gFnSc1	Tajuca
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
250	[number]	k4	250
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
festival	festival	k1gInSc4	festival
Rock	rock	k1gInSc1	rock
in	in	k?	in
Rio	Rio	k1gFnSc2	Rio
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
deseti	deset	k4xCc2	deset
večerů	večer	k1gInPc2	večer
zde	zde	k6eAd1	zde
vystoupily	vystoupit	k5eAaPmAgFnP	vystoupit
nejslavnější	slavný	k2eAgFnPc1d3	nejslavnější
skupiny	skupina	k1gFnPc1	skupina
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
místními	místní	k2eAgFnPc7d1	místní
kapelami	kapela	k1gFnPc7	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
pozvanými	pozvaný	k2eAgMnPc7d1	pozvaný
byli	být	k5eAaImAgMnP	být
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
<g/>
,	,	kIx,	,
Iron	iron	k1gInSc4	iron
Maiden	Maidna	k1gFnPc2	Maidna
<g/>
,	,	kIx,	,
Yes	Yes	k1gFnPc2	Yes
<g/>
,	,	kIx,	,
Rod	rod	k1gInSc4	rod
Stewart	Stewart	k1gMnSc1	Stewart
<g/>
,	,	kIx,	,
Whitesnake	Whitesnak	k1gInSc2	Whitesnak
a	a	k8xC	a
Ozzy	Ozza	k1gFnSc2	Ozza
Osbourne	Osbourn	k1gInSc5	Osbourn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
největší	veliký	k2eAgFnSc7d3	veliký
hvězdou	hvězda	k1gFnSc7	hvězda
byli	být	k5eAaImAgMnP	být
Queen	Queen	k1gInSc4	Queen
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
vévodili	vévodit	k5eAaImAgMnP	vévodit
úvodnímu	úvodní	k2eAgMnSc3d1	úvodní
i	i	k8xC	i
závěrečnému	závěrečný	k2eAgInSc3d1	závěrečný
koncertu	koncert	k1gInSc3	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Queen	Queen	k1gInSc1	Queen
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
koncert	koncert	k1gInSc4	koncert
změnili	změnit	k5eAaPmAgMnP	změnit
program	program	k1gInSc4	program
z	z	k7c2	z
turné	turné	k1gNnSc2	turné
The	The	k1gFnSc2	The
Works	Works	kA	Works
<g/>
.	.	kIx.	.
</s>
<s>
Vstupní	vstupní	k2eAgNnSc1d1	vstupní
intro	intro	k1gNnSc1	intro
Machines	Machinesa	k1gFnPc2	Machinesa
se	se	k3xPyFc4	se
plynule	plynule	k6eAd1	plynule
přelilo	přelít	k5eAaPmAgNnS	přelít
do	do	k7c2	do
Tear	Tear	k1gMnSc1	Tear
It	It	k1gMnSc2	It
Up	Up	k1gMnSc1	Up
a	a	k8xC	a
Tie	Tie	k1gMnSc1	Tie
Your	Your	k1gMnSc1	Your
Mother	Mothra	k1gFnPc2	Mothra
Down	Down	k1gMnSc1	Down
<g/>
.	.	kIx.	.
</s>
<s>
Vybrali	vybrat	k5eAaPmAgMnP	vybrat
to	ten	k3xDgNnSc1	ten
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
repertoáru	repertoár	k1gInSc2	repertoár
<g/>
:	:	kIx,	:
Seven	Seven	k2eAgInSc1d1	Seven
Seas	Seas	k1gInSc1	Seas
Or	Or	k1gMnSc2	Or
Rhye	Rhy	k1gMnSc2	Rhy
<g/>
,	,	kIx,	,
Now	Now	k1gMnSc2	Now
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Here	Here	k1gNnSc1	Here
<g/>
,	,	kIx,	,
Under	Under	k1gInSc1	Under
Pressure	Pressur	k1gMnSc5	Pressur
<g/>
,	,	kIx,	,
Another	Anothra	k1gFnPc2	Anothra
One	One	k1gFnSc2	One
Bites	Bites	k1gMnSc1	Bites
the	the	k?	the
Dust	Dust	k1gMnSc1	Dust
a	a	k8xC	a
samozřejmě	samozřejmě	k6eAd1	samozřejmě
Bohemian	bohemian	k1gInSc4	bohemian
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
Love	lov	k1gInSc5	lov
Of	Of	k1gMnSc1	Of
my	my	k3xPp1nPc1	my
Life	Life	k1gInSc1	Life
a	a	k8xC	a
přídavek	přídavek	k1gInSc1	přídavek
I	i	k8xC	i
Want	Want	k1gInSc1	Want
To	to	k9	to
Break	break	k1gInSc1	break
Free	Free	k1gFnPc2	Free
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
Freddie	Freddie	k1gFnPc4	Freddie
přišel	přijít	k5eAaPmAgMnS	přijít
na	na	k7c4	na
pódium	pódium	k1gNnSc4	pódium
v	v	k7c6	v
černé	černý	k2eAgFnSc6d1	černá
paruce	paruka	k1gFnSc6	paruka
a	a	k8xC	a
s	s	k7c7	s
falešným	falešný	k2eAgNnSc7d1	falešné
poprsím	poprsí	k1gNnSc7	poprsí
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
klipu	klip	k1gInSc6	klip
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
reakci	reakce	k1gFnSc4	reakce
davu	dav	k1gInSc2	dav
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
připraven	připravit	k5eAaPmNgInS	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
lidí	člověk	k1gMnPc2	člověk
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
byla	být	k5eAaImAgNnP	být
píseň	píseň	k1gFnSc4	píseň
Johna	John	k1gMnSc2	John
Deacona	Deacon	k1gMnSc2	Deacon
výkřikem	výkřik	k1gInSc7	výkřik
touhy	touha	k1gFnSc2	touha
po	po	k7c6	po
svobodě	svoboda	k1gFnSc6	svoboda
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
zpěváka	zpěvák	k1gMnSc4	zpěvák
převlečeného	převlečený	k2eAgMnSc4d1	převlečený
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
představoval	představovat	k5eAaImAgMnS	představovat
svatokrádež	svatokrádež	k1gFnSc4	svatokrádež
<g/>
.	.	kIx.	.
</s>
<s>
Mercury	Mercura	k1gFnPc4	Mercura
později	pozdě	k6eAd2	pozdě
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Myslel	myslet	k5eAaImAgMnS	myslet
jsem	být	k5eAaImIp1nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
mě	já	k3xPp1nSc4	já
ukamenují	ukamenovat	k5eAaPmIp3nP	ukamenovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dav	Dav	k1gInSc1	Dav
jim	on	k3xPp3gMnPc3	on
toto	tento	k3xDgNnSc4	tento
faux	faux	k1gInSc1	faux
pas	pas	k6eAd1	pas
prominul	prominout	k5eAaPmAgInS	prominout
<g/>
,	,	kIx,	,
když	když	k8xS	když
při	při	k7c6	při
písni	píseň	k1gFnSc6	píseň
We	We	k1gMnSc1	We
Will	Willa	k1gFnPc2	Willa
Rock	rock	k1gInSc4	rock
You	You	k1gFnSc4	You
Freddie	Freddie	k1gFnSc2	Freddie
poskakoval	poskakovat	k5eAaImAgMnS	poskakovat
po	po	k7c6	po
pódiu	pódium	k1gNnSc6	pódium
s	s	k7c7	s
dvoustrannou	dvoustranný	k2eAgFnSc7d1	dvoustranná
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
:	:	kIx,	:
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
strany	strana	k1gFnSc2	strana
byla	být	k5eAaImAgFnS	být
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
a	a	k8xC	a
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
brazilská	brazilský	k2eAgFnSc1d1	brazilská
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc4	koncert
ukončovali	ukončovat	k5eAaImAgMnP	ukončovat
We	We	k1gMnPc1	We
Are	ar	k1gInSc5	ar
the	the	k?	the
Champions	Championsa	k1gFnPc2	Championsa
a	a	k8xC	a
God	God	k1gFnPc2	God
Save	Sav	k1gFnSc2	Sav
the	the	k?	the
Queen	Queen	k1gInSc1	Queen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
vychází	vycházet	k5eAaImIp3nS	vycházet
Mercuryho	Mercury	k1gMnSc4	Mercury
druhý	druhý	k4xOgInSc4	druhý
singl	singl	k1gInSc4	singl
<g/>
,	,	kIx,	,
I	i	k8xC	i
Was	Was	k1gMnSc1	Was
Born	Born	k1gMnSc1	Born
to	ten	k3xDgNnSc4	ten
Love	lov	k1gInSc5	lov
You	You	k1gFnSc4	You
<g/>
,	,	kIx,	,
a	a	k8xC	a
dostává	dostávat	k5eAaImIp3nS	dostávat
se	se	k3xPyFc4	se
na	na	k7c4	na
11	[number]	k4	11
<g/>
.	.	kIx.	.
místo	místo	k7c2	místo
žebříčků	žebříček	k1gInPc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
Freddieho	Freddie	k1gMnSc2	Freddie
sólové	sólový	k2eAgNnSc4d1	sólové
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
Mr	Mr	k1gMnSc1	Mr
Bad	Bad	k1gMnSc1	Bad
Guy	Guy	k1gMnSc1	Guy
<g/>
,	,	kIx,	,
napsané	napsaný	k2eAgInPc1d1	napsaný
i	i	k8xC	i
produkované	produkovaný	k2eAgInPc1d1	produkovaný
Mercurym	Mercurym	k1gInSc1	Mercurym
<g/>
,	,	kIx,	,
obsazuje	obsazovat	k5eAaImIp3nS	obsazovat
šestou	šestý	k4xOgFnSc4	šestý
příčku	příčka	k1gFnSc4	příčka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
skupina	skupina	k1gFnSc1	skupina
obohatila	obohatit	k5eAaPmAgFnS	obohatit
sbírku	sbírka	k1gFnSc4	sbírka
alb	alba	k1gFnPc2	alba
další	další	k2eAgFnSc7d1	další
deskou	deska	k1gFnSc7	deska
A	a	k9	a
Kind	Kind	k1gMnSc1	Kind
of	of	k?	of
Magic	Magic	k1gMnSc1	Magic
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
písní	píseň	k1gFnPc2	píseň
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
dojímavou	dojímavý	k2eAgFnSc7d1	dojímavá
baladou	balada	k1gFnSc7	balada
Who	Who	k1gFnSc2	Who
Wants	Wantsa	k1gFnPc2	Wantsa
To	ten	k3xDgNnSc1	ten
Live	Live	k1gNnSc7	Live
Forever	Forevra	k1gFnPc2	Forevra
byla	být	k5eAaImAgFnS	být
zkomponována	zkomponovat	k5eAaPmNgFnS	zkomponovat
pro	pro	k7c4	pro
pohádku	pohádka	k1gFnSc4	pohádka
pro	pro	k7c4	pro
dospělé	dospělí	k1gMnPc4	dospělí
Highlander	Highlander	k1gInSc1	Highlander
s	s	k7c7	s
Christopherem	Christopher	k1gMnSc7	Christopher
Lambertem	Lambert	k1gMnSc7	Lambert
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
vyletělo	vyletět	k5eAaPmAgNnS	vyletět
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
popularity	popularita	k1gFnSc2	popularita
a	a	k8xC	a
singly	singl	k1gInPc1	singl
se	se	k3xPyFc4	se
prodávaly	prodávat	k5eAaImAgInP	prodávat
úspěšně	úspěšně	k6eAd1	úspěšně
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
One	One	k?	One
Vision	vision	k1gInSc1	vision
<g/>
,	,	kIx,	,
hold	hold	k1gInSc1	hold
Queen	Queen	k2eAgInSc1d1	Queen
iniciátorovi	iniciátor	k1gMnSc3	iniciátor
koncertů	koncert	k1gInPc2	koncert
Live	Live	k1gNnSc2	Live
Aid	Aida	k1gFnPc2	Aida
Bobu	bob	k1gInSc2	bob
Geldofovi	Geldof	k1gMnSc3	Geldof
<g/>
,	,	kIx,	,
obsadil	obsadit	k5eAaPmAgMnS	obsadit
sedmou	sedmý	k4xOgFnSc4	sedmý
příčku	příčka	k1gFnSc4	příčka
hitparád	hitparáda	k1gFnPc2	hitparáda
<g/>
,	,	kIx,	,
A	a	k9	a
Kind	Kind	k1gMnSc1	Kind
of	of	k?	of
Magic	Magic	k1gMnSc1	Magic
se	se	k3xPyFc4	se
během	během	k7c2	během
jedenáctitýdenního	jedenáctitýdenní	k2eAgNnSc2d1	jedenáctitýdenní
putování	putování	k1gNnSc2	putování
žebříčkem	žebříček	k1gInSc7	žebříček
objevilo	objevit	k5eAaPmAgNnS	objevit
na	na	k7c6	na
příčce	příčka	k1gFnSc6	příčka
třetí	třetí	k4xOgFnSc6	třetí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obě	dva	k4xCgFnPc1	dva
písně	píseň	k1gFnPc1	píseň
ukazovaly	ukazovat	k5eAaImAgFnP	ukazovat
šíři	šíře	k1gFnSc4	šíře
záběru	záběr	k1gInSc2	záběr
skupiny	skupina	k1gFnSc2	skupina
<g/>
:	:	kIx,	:
One	One	k1gFnSc2	One
Vision	vision	k1gInSc1	vision
byl	být	k5eAaImAgInS	být
klasický	klasický	k2eAgInSc1d1	klasický
rock	rock	k1gInSc1	rock
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
skladba	skladba	k1gFnSc1	skladba
líbivý	líbivý	k2eAgInSc1d1	líbivý
a	a	k8xC	a
hravý	hravý	k2eAgInSc1d1	hravý
popěvek	popěvek	k1gInSc1	popěvek
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
byly	být	k5eAaImAgInP	být
Friends	Friends	k1gInSc4	Friends
Will	Willa	k1gFnPc2	Willa
Be	Be	k1gFnPc2	Be
Friends	Friends	k1gInSc1	Friends
<g/>
,	,	kIx,	,
Princes	princes	k1gInSc1	princes
of	of	k?	of
the	the	k?	the
Universe	Universe	k1gFnSc2	Universe
nebo	nebo	k8xC	nebo
Don	dona	k1gFnPc2	dona
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Lose	los	k1gInSc6	los
Your	Youra	k1gFnPc2	Youra
Head	Head	k1gInSc4	Head
za	za	k7c2	za
asistence	asistence	k1gFnSc2	asistence
talentované	talentovaný	k2eAgNnSc1d1	talentované
Joan	Joan	k1gNnSc1	Joan
Armatradingové	Armatradingový	k2eAgNnSc1d1	Armatradingový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
vychází	vycházet	k5eAaImIp3nS	vycházet
titulní	titulní	k2eAgFnSc4d1	titulní
píseň	píseň	k1gFnSc4	píseň
z	z	k7c2	z
muzikálu	muzikál	k1gInSc2	muzikál
Daeva	Daev	k1gMnSc2	Daev
Clarka	Clarek	k1gMnSc2	Clarek
Time	Tim	k1gMnSc2	Tim
v	v	k7c4	v
Mercuryho	Mercury	k1gMnSc4	Mercury
podání	podání	k1gNnSc2	podání
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
pro	pro	k7c4	pro
muzikál	muzikál	k1gInSc4	muzikál
nahrál	nahrát	k5eAaPmAgMnS	nahrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
Queen	Quena	k1gFnPc2	Quena
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
Magic	Magic	k1gMnSc1	Magic
Tour	Tour	k1gInSc4	Tour
viděl	vidět	k5eAaImAgMnS	vidět
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
milión	milión	k4xCgInSc1	milión
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
začalo	začít	k5eAaPmAgNnS	začít
ve	v	k7c6	v
Stockolmu	Stockolm	k1gInSc6	Stockolm
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
a	a	k8xC	a
končilo	končit	k5eAaImAgNnS	končit
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
v	v	k7c4	v
Knebworth	Knebworth	k1gInSc4	Knebworth
Park	park	k1gInSc1	park
–	–	k?	–
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
jejich	jejich	k3xOp3gInSc1	jejich
největší	veliký	k2eAgInSc1d3	veliký
koncert	koncert	k1gInSc1	koncert
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
poslední	poslední	k2eAgInSc1d1	poslední
koncert	koncert	k1gInSc1	koncert
vůbec	vůbec	k9	vůbec
<g/>
!	!	kIx.	!
</s>
<s>
Na	na	k7c6	na
turné	turné	k1gNnSc6	turné
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
zapsala	zapsat	k5eAaPmAgFnS	zapsat
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
když	když	k8xS	když
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
hrála	hrát	k5eAaImAgFnS	hrát
za	za	k7c7	za
železnou	železný	k2eAgFnSc7d1	železná
oponou	opona	k1gFnSc7	opona
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
na	na	k7c6	na
Nepstadionu	Nepstadion	k1gInSc6	Nepstadion
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
živé	živý	k2eAgNnSc1d1	živé
album	album	k1gNnSc1	album
Live	Liv	k1gFnSc2	Liv
Magic	Magice	k1gFnPc2	Magice
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInSc1d1	obsahující
výběr	výběr	k1gInSc1	výběr
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
skupiny	skupina	k1gFnSc2	skupina
nahraných	nahraný	k2eAgInPc2d1	nahraný
během	během	k7c2	během
turné	turné	k1gNnSc2	turné
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Obsazuje	obsazovat	k5eAaImIp3nS	obsazovat
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
LP	LP	kA	LP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
si	se	k3xPyFc3	se
Queen	Queen	k2eAgMnSc1d1	Queen
chtějí	chtít	k5eAaImIp3nP	chtít
odpočinout	odpočinout	k5eAaPmF	odpočinout
od	od	k7c2	od
nahrávání	nahrávání	k1gNnSc2	nahrávání
i	i	k8xC	i
koncertování	koncertování	k1gNnSc2	koncertování
<g/>
.	.	kIx.	.
</s>
<s>
Mercuryho	Mercuryze	k6eAd1	Mercuryze
sólová	sólový	k2eAgFnSc1d1	sólová
kariéra	kariéra	k1gFnSc1	kariéra
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Rogera	Rogera	k1gFnSc1	Rogera
Taylora	Taylora	k1gFnSc1	Taylora
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozjíždějí	rozjíždět	k5eAaImIp3nP	rozjíždět
naplno	naplno	k6eAd1	naplno
<g/>
.	.	kIx.	.
</s>
<s>
Freddie	Freddie	k1gFnSc1	Freddie
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
vydává	vydávat	k5eAaImIp3nS	vydávat
singl	singl	k1gInSc1	singl
The	The	k1gFnSc2	The
Great	Great	k2eAgInSc1d1	Great
Pretender	Pretender	k1gInSc1	Pretender
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
jeho	jeho	k3xOp3gInSc1	jeho
největší	veliký	k2eAgInSc1d3	veliký
hit	hit	k1gInSc1	hit
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
příčka	příčka	k1gFnSc1	příčka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mercury	Mercura	k1gFnPc1	Mercura
natáčí	natáčet	k5eAaImIp3nP	natáčet
se	s	k7c7	s
španělskou	španělský	k2eAgFnSc7d1	španělská
operní	operní	k2eAgFnSc7d1	operní
pěvkyní	pěvkyně	k1gFnSc7	pěvkyně
Montserrat	Montserrat	k1gMnSc1	Montserrat
Caballé	Caballý	k2eAgNnSc1d1	Caballé
album	album	k1gNnSc1	album
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
titulní	titulní	k2eAgFnSc1d1	titulní
píseň	píseň	k1gFnSc1	píseň
Barcelona	Barcelona	k1gFnSc1	Barcelona
vybere	vybrat	k5eAaPmIp3nS	vybrat
olympijský	olympijský	k2eAgInSc4d1	olympijský
výbor	výbor	k1gInSc4	výbor
jako	jako	k8xS	jako
hudební	hudební	k2eAgNnSc4d1	hudební
téma	téma	k1gNnSc4	téma
barcelonských	barcelonský	k2eAgFnPc2d1	barcelonská
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Taylor	Taylor	k1gMnSc1	Taylor
zakládá	zakládat	k5eAaImIp3nS	zakládat
skupinu	skupina	k1gFnSc4	skupina
The	The	k1gFnSc2	The
Cross	Cross	k1gInSc1	Cross
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mu	on	k3xPp3gMnSc3	on
chybí	chybit	k5eAaPmIp3nS	chybit
koncertní	koncertní	k2eAgFnSc4d1	koncertní
činnost	činnost	k1gFnSc4	činnost
během	během	k7c2	během
doby	doba	k1gFnSc2	doba
odpočinku	odpočinek	k1gInSc2	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
Queen	Queen	k1gInSc1	Queen
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
vydávat	vydávat	k5eAaImF	vydávat
na	na	k7c6	na
turné	turné	k1gNnSc6	turné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
vychází	vycházet	k5eAaImIp3nS	vycházet
jejich	jejich	k3xOp3gNnSc1	jejich
první	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
Cowboys	Cowboys	k1gInSc1	Cowboys
And	Anda	k1gFnPc2	Anda
Indians	Indiansa	k1gFnPc2	Indiansa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mercuryho	Mercuryze	k6eAd1	Mercuryze
sólová	sólový	k2eAgFnSc1d1	sólová
kariéra	kariéra	k1gFnSc1	kariéra
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
s	s	k7c7	s
Montserrat	Montserrat	k1gInSc4	Montserrat
Caballé	Caballý	k2eAgMnPc4d1	Caballý
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
zhoršujícímu	zhoršující	k2eAgMnSc3d1	zhoršující
se	se	k3xPyFc4	se
zdravotnímu	zdravotní	k2eAgInSc3d1	zdravotní
stavu	stav	k1gInSc3	stav
musel	muset	k5eAaImAgMnS	muset
však	však	k9	však
svůj	svůj	k3xOyFgInSc4	svůj
duet	duet	k1gInSc4	duet
zpěvák	zpěvák	k1gMnSc1	zpěvák
odzpívat	odzpívat	k5eAaPmF	odzpívat
z	z	k7c2	z
playbacku	playback	k1gInSc2	playback
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
mnohé	mnohý	k2eAgInPc4d1	mnohý
dohady	dohad	k1gInPc4	dohad
<g/>
.	.	kIx.	.
</s>
<s>
Týž	týž	k3xTgInSc4	týž
měsíc	měsíc	k1gInSc4	měsíc
vydává	vydávat	k5eAaImIp3nS	vydávat
operní	operní	k2eAgNnSc1d1	operní
duo	duo	k1gNnSc1	duo
album	album	k1gNnSc1	album
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
obsazuje	obsazovat	k5eAaImIp3nS	obsazovat
dvacáté	dvacátý	k4xOgFnSc6	dvacátý
páté	pátý	k4xOgNnSc4	pátý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
i	i	k9	i
singl	singl	k1gInSc1	singl
z	z	k7c2	z
alba	album	k1gNnSc2	album
The	The	k1gFnSc2	The
Golden	Goldna	k1gFnPc2	Goldna
Boy	boy	k1gMnSc1	boy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
rok	rok	k1gInSc4	rok
vychází	vycházet	k5eAaImIp3nS	vycházet
první	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
Taylorova	Taylorův	k2eAgInSc2d1	Taylorův
souboru	soubor	k1gInSc2	soubor
The	The	k1gFnSc2	The
Cross	Crossa	k1gFnPc2	Crossa
<g/>
,	,	kIx,	,
na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
k	k	k7c3	k
Taylorovi	Taylor	k1gMnSc3	Taylor
přidávají	přidávat	k5eAaImIp3nP	přidávat
i	i	k9	i
Deacon	Deacon	k1gMnSc1	Deacon
a	a	k8xC	a
May	May	k1gMnSc1	May
a	a	k8xC	a
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
v	v	k7c4	v
Hammersmith	Hammersmith	k1gInSc4	Hammersmith
Palais	Palais	k1gFnSc2	Palais
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1988	[number]	k4	1988
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
:	:	kIx,	:
Mercury	Mercura	k1gFnSc2	Mercura
<g/>
:	:	kIx,	:
nemoc	nemoc	k1gFnSc1	nemoc
<g/>
,	,	kIx,	,
smrt	smrt	k1gFnSc1	smrt
a	a	k8xC	a
hold	hold	k1gInSc1	hold
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
A	A	kA	A
Kind	Kinda	k1gFnPc2	Kinda
of	of	k?	of
Magic	Magice	k1gFnPc2	Magice
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
opět	opět	k6eAd1	opět
odmlčela	odmlčet	k5eAaPmAgFnS	odmlčet
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
The	The	k1gMnSc1	The
Miracle	Miracle	k1gFnSc2	Miracle
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
kritiky	kritika	k1gFnSc2	kritika
zatracovaly	zatracovat	k5eAaImAgFnP	zatracovat
<g/>
,	,	kIx,	,
album	album	k1gNnSc1	album
vedlo	vést	k5eAaImAgNnS	vést
hitparády	hitparáda	k1gFnSc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Vydáno	vydat	k5eAaPmNgNnS	vydat
bylo	být	k5eAaImAgNnS	být
29	[number]	k4	29
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
písně	píseň	k1gFnSc2	píseň
The	The	k1gFnPc2	The
Invisible	Invisible	k1gFnSc7	Invisible
Man	mana	k1gFnPc2	mana
byly	být	k5eAaImAgInP	být
všechny	všechen	k3xTgFnPc4	všechen
skladby	skladba	k1gFnPc4	skladba
klasické	klasický	k2eAgFnPc4d1	klasická
bez	bez	k7c2	bez
zvláštního	zvláštní	k2eAgNnSc2d1	zvláštní
novátorství	novátorství	k1gNnSc2	novátorství
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
singly	singl	k1gInPc1	singl
<g/>
,	,	kIx,	,
I	i	k9	i
Want	Want	k1gInSc1	Want
It	It	k1gMnSc2	It
All	All	k1gMnSc2	All
a	a	k8xC	a
Breakthru	Breakthra	k1gFnSc4	Breakthra
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
první	první	k4xOgFnSc2	první
desítky	desítka	k1gFnSc2	desítka
<g/>
,	,	kIx,	,
bodovaly	bodovat	k5eAaImAgFnP	bodovat
i	i	k9	i
písně	píseň	k1gFnPc1	píseň
Scandal	Scandal	k1gMnSc2	Scandal
a	a	k8xC	a
The	The	k1gMnSc2	The
Miracle	Miracl	k1gMnSc2	Miracl
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
stalo	stát	k5eAaPmAgNnS	stát
úspěšně	úspěšně	k6eAd1	úspěšně
prodávaným	prodávaný	k2eAgInSc7d1	prodávaný
a	a	k8xC	a
dokazovalo	dokazovat	k5eAaImAgNnS	dokazovat
tak	tak	k6eAd1	tak
nezměrnou	nezměrný	k2eAgFnSc4d1	nezměrná
loajálnost	loajálnost	k1gFnSc4	loajálnost
fanoušků	fanoušek	k1gMnPc2	fanoušek
Queen	Queen	k1gInSc4	Queen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
zalovilo	zalovit	k5eAaPmAgNnS	zalovit
BBC	BBC	kA	BBC
hluboko	hluboko	k6eAd1	hluboko
v	v	k7c6	v
archívech	archív	k1gInPc6	archív
a	a	k8xC	a
vydolovalo	vydolovat	k5eAaPmAgNnS	vydolovat
osm	osm	k4xCc1	osm
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
skupina	skupina	k1gFnSc1	skupina
nahrála	nahrát	k5eAaPmAgFnS	nahrát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
At	At	k1gMnSc1	At
the	the	k?	the
Beeb	Beeb	k1gMnSc1	Beeb
<g/>
.	.	kIx.	.
</s>
<s>
Deska	deska	k1gFnSc1	deska
sice	sice	k8xC	sice
byla	být	k5eAaImAgFnS	být
zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
komerční	komerční	k2eAgInSc4d1	komerční
úspěch	úspěch	k1gInSc4	úspěch
nezaznamenala	zaznamenat	k5eNaPmAgFnS	zaznamenat
<g/>
.	.	kIx.	.
</s>
<s>
May	May	k1gMnSc1	May
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nahrál	nahrát	k5eAaPmAgMnS	nahrát
kytarové	kytarový	k2eAgNnSc4d1	kytarové
sólo	sólo	k1gNnSc4	sólo
pro	pro	k7c4	pro
píseň	píseň	k1gFnSc4	píseň
When	When	k1gInSc1	When
Death	Death	k1gMnSc1	Death
Calls	Callsa	k1gFnPc2	Callsa
pro	pro	k7c4	pro
skupinu	skupina	k1gFnSc4	skupina
Black	Black	k1gMnSc1	Black
Sabbath	Sabbath	k1gMnSc1	Sabbath
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
prudce	prudko	k6eAd1	prudko
vzrostly	vzrůst	k5eAaPmAgInP	vzrůst
dohady	dohad	k1gInPc1	dohad
v	v	k7c6	v
tisku	tisk	k1gInSc6	tisk
o	o	k7c4	o
Mercuryho	Mercury	k1gMnSc4	Mercury
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
skupina	skupina	k1gFnSc1	skupina
vytrvale	vytrvale	k6eAd1	vytrvale
popírala	popírat	k5eAaImAgFnS	popírat
zprávy	zpráva	k1gFnPc4	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
zpěvák	zpěvák	k1gMnSc1	zpěvák
měl	mít	k5eAaImAgMnS	mít
mít	mít	k5eAaImF	mít
AIDS	AIDS	kA	AIDS
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
předávání	předávání	k1gNnSc4	předávání
cen	cena	k1gFnPc2	cena
BRIT	Brit	k1gMnSc1	Brit
Awards	Awards	k1gInSc1	Awards
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
se	se	k3xPyFc4	se
Freddie	Freddie	k1gFnSc1	Freddie
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
pokročilejším	pokročilý	k2eAgNnSc6d2	pokročilejší
stádiu	stádium	k1gNnSc6	stádium
nemoci	nemoc	k1gFnSc2	nemoc
AIDS	aids	k1gInSc1	aids
strhaný	strhaný	k2eAgInSc1d1	strhaný
<g/>
,	,	kIx,	,
unavený	unavený	k2eAgMnSc1d1	unavený
<g/>
,	,	kIx,	,
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
vybledlý	vybledlý	k2eAgMnSc1d1	vybledlý
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
dohady	dohad	k1gInPc1	dohad
opět	opět	k6eAd1	opět
rozbíhají	rozbíhat	k5eAaImIp3nP	rozbíhat
<g/>
.	.	kIx.	.
</s>
<s>
Queen	Queen	k1gInSc1	Queen
získává	získávat	k5eAaImIp3nS	získávat
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
výjimečný	výjimečný	k2eAgInSc4d1	výjimečný
přínos	přínos	k1gInSc4	přínos
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
studiovým	studiový	k2eAgNnSc7d1	studiové
albem	album	k1gNnSc7	album
Queen	Quena	k1gFnPc2	Quena
před	před	k7c4	před
Freddieho	Freddie	k1gMnSc4	Freddie
smrtí	smrt	k1gFnSc7	smrt
bylo	být	k5eAaImAgNnS	být
Innuendo	Innuendo	k1gNnSc1	Innuendo
<g/>
,	,	kIx,	,
z	z	k7c2	z
února	únor	k1gInSc2	únor
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
devět	devět	k4xCc4	devět
měsíců	měsíc	k1gInPc2	měsíc
před	před	k7c7	před
Mercuryho	Mercury	k1gMnSc2	Mercury
úmrtím	úmrtí	k1gNnSc7	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
Hudebně	hudebně	k6eAd1	hudebně
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
osvědčeným	osvědčený	k2eAgFnPc3d1	osvědčená
formám	forma	k1gFnPc3	forma
<g/>
,	,	kIx,	,
přinášelo	přinášet	k5eAaImAgNnS	přinášet
některé	některý	k3yIgFnPc4	některý
skvělé	skvělý	k2eAgFnPc4d1	skvělá
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
později	pozdě	k6eAd2	pozdě
ještě	ještě	k6eAd1	ještě
získaly	získat	k5eAaPmAgFnP	získat
na	na	k7c6	na
údernosti	údernost	k1gFnSc6	údernost
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
svět	svět	k1gInSc1	svět
uvědomil	uvědomit	k5eAaPmAgInS	uvědomit
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgNnSc4	jaký
utrpení	utrpení	k1gNnSc4	utrpení
příprava	příprava	k1gFnSc1	příprava
Freddiemu	Freddiem	k1gInSc2	Freddiem
přinášela	přinášet	k5eAaImAgFnS	přinášet
<g/>
.	.	kIx.	.
</s>
<s>
Titulní	titulní	k2eAgFnSc1d1	titulní
melodie	melodie	k1gFnSc1	melodie
Innuendo	Innuendo	k6eAd1	Innuendo
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc7	první
písní	píseň	k1gFnSc7	píseň
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
rovnou	rovnou	k6eAd1	rovnou
usadila	usadit	k5eAaPmAgFnS	usadit
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
žebříčků	žebříček	k1gInPc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Skládala	skládat	k5eAaImAgFnS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
3	[number]	k4	3
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgInSc1d1	připomínající
tak	tak	k6eAd1	tak
klasickou	klasický	k2eAgFnSc7d1	klasická
Bohemian	bohemian	k1gInSc4	bohemian
Rhapsody	Rhapsod	k1gInPc1	Rhapsod
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
singl	singl	k1gInSc1	singl
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Going	Going	k1gMnSc1	Going
Slightly	Slightly	k1gMnSc1	Slightly
Mad	Mad	k1gMnSc1	Mad
ukazoval	ukazovat	k5eAaImAgMnS	ukazovat
další	další	k2eAgInSc4d1	další
radikální	radikální	k2eAgInSc4d1	radikální
odklon	odklon	k1gInSc4	odklon
od	od	k7c2	od
klasického	klasický	k2eAgInSc2d1	klasický
zvuku	zvuk	k1gInSc2	zvuk
Queen	Quena	k1gFnPc2	Quena
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
fantasknímu	fantaskní	k2eAgMnSc3d1	fantaskní
a	a	k8xC	a
zábavnému	zábavný	k2eAgInSc3d1	zábavný
textu	text	k1gInSc3	text
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
Mercury	Mercura	k1gFnPc1	Mercura
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
"	"	kIx"	"
<g/>
chybí	chybit	k5eAaPmIp3nS	chybit
jedna	jeden	k4xCgFnSc1	jeden
karta	karta	k1gFnSc1	karta
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
balíčku	balíček	k1gInSc2	balíček
karet	kareta	k1gFnPc2	kareta
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
myslím	myslet	k5eAaImIp1nS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
banánovník	banánovník	k1gInSc1	banánovník
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nalezneme	nalézt	k5eAaBmIp1nP	nalézt
zde	zde	k6eAd1	zde
spoustu	spousta	k1gFnSc4	spousta
povedených	povedený	k2eAgFnPc2d1	povedená
skladeb	skladba	k1gFnPc2	skladba
jako	jako	k8xS	jako
Headlong	Headlonga	k1gFnPc2	Headlonga
nebo	nebo	k8xC	nebo
tvrdší	tvrdý	k2eAgFnSc2d2	tvrdší
The	The	k1gFnSc2	The
Hitman	Hitman	k1gMnSc1	Hitman
a	a	k8xC	a
I	i	k9	i
Can	Can	k1gFnSc1	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Live	Live	k1gInSc1	Live
with	with	k1gMnSc1	with
You	You	k1gMnSc1	You
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
kráse	krása	k1gFnSc6	krása
<g/>
.	.	kIx.	.
</s>
<s>
Drahokamy	drahokam	k1gInPc1	drahokam
v	v	k7c6	v
koruně	koruna	k1gFnSc6	koruna
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
byly	být	k5eAaImAgFnP	být
Bijou	bijou	k1gNnPc4	bijou
se	s	k7c7	s
skvělou	skvělý	k2eAgFnSc7d1	skvělá
kytarou	kytara	k1gFnSc7	kytara
a	a	k8xC	a
These	these	k1gFnSc1	these
Are	ar	k1gInSc5	ar
the	the	k?	the
Days	Daysa	k1gFnPc2	Daysa
of	of	k?	of
Our	Our	k1gMnSc1	Our
Lives	Lives	k1gMnSc1	Lives
<g/>
,	,	kIx,	,
svíravá	svíravý	k2eAgFnSc1d1	svíravá
skladba	skladba	k1gFnSc1	skladba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
Mercury	Mercur	k1gInPc1	Mercur
ohlíží	ohlížet	k5eAaImIp3nP	ohlížet
za	za	k7c7	za
svým	svůj	k3xOyFgInSc7	svůj
životem	život	k1gInSc7	život
a	a	k8xC	a
trápí	trápit	k5eAaImIp3nS	trápit
se	se	k3xPyFc4	se
nemožností	nemožnost	k1gFnSc7	nemožnost
vrátit	vrátit	k5eAaPmF	vrátit
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	album	k1gNnSc2	album
Innuendo	Innuendo	k6eAd1	Innuendo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
právě	právě	k6eAd1	právě
díky	díky	k7c3	díky
písni	píseň	k1gFnSc3	píseň
These	these	k1gFnSc2	these
are	ar	k1gInSc5	ar
the	the	k?	the
days	days	k1gInSc1	days
of	of	k?	of
our	our	k?	our
lives	lives	k1gInSc4	lives
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
spekulace	spekulace	k1gFnPc1	spekulace
o	o	k7c4	o
Mercuryho	Mercury	k1gMnSc4	Mercury
AIDS	aids	k1gInSc4	aids
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
videoklipech	videoklip	k1gInPc6	videoklip
k	k	k7c3	k
desce	deska	k1gFnSc3	deska
Innuendo	Innuendo	k6eAd1	Innuendo
je	být	k5eAaImIp3nS	být
Freddie	Freddie	k1gFnSc2	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
pohublý	pohublý	k2eAgMnSc1d1	pohublý
<g/>
,	,	kIx,	,
do	do	k7c2	do
kostýmu	kostým	k1gInSc2	kostým
jsou	být	k5eAaImIp3nP	být
mu	on	k3xPp3gInSc3	on
dávány	dáván	k2eAgFnPc4d1	dávána
vycpávky	vycpávka	k1gFnPc4	vycpávka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
šaty	šat	k1gInPc4	šat
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
visí	viset	k5eAaImIp3nS	viset
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
nalíčen	nalíčen	k2eAgMnSc1d1	nalíčen
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
někomu	někdo	k3yInSc3	někdo
jeho	jeho	k3xOp3gFnSc4	jeho
tvář	tvář	k1gFnSc4	tvář
připomíná	připomínat	k5eAaImIp3nS	připomínat
masku	maska	k1gFnSc4	maska
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
použito	použít	k5eAaPmNgNnS	použít
černobílého	černobílý	k2eAgInSc2d1	černobílý
záznamu	záznam	k1gInSc2	záznam
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
veřejnost	veřejnost	k1gFnSc1	veřejnost
nepoznala	poznat	k5eNaPmAgFnS	poznat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
špatně	špatně	k6eAd1	špatně
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
zpěvákovo	zpěvákův	k2eAgNnSc1d1	zpěvákovo
zdraví	zdraví	k1gNnSc1	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
slova	slovo	k1gNnPc1	slovo
"	"	kIx"	"
<g/>
I	i	k9	i
Still	Still	k1gInSc1	Still
Love	lov	k1gInSc5	lov
You	You	k1gMnSc7	You
<g/>
"	"	kIx"	"
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
natočeném	natočený	k2eAgNnSc6d1	natočené
videu	video	k1gNnSc6	video
These	these	k1gFnSc2	these
Are	ar	k1gInSc5	ar
the	the	k?	the
Days	Days	k1gInSc1	Days
of	of	k?	of
Our	Our	k1gFnSc1	Our
Lives	Lives	k1gInSc1	Lives
byla	být	k5eAaImAgFnS	být
nanejvýš	nanejvýš	k6eAd1	nanejvýš
symbolická	symbolický	k2eAgFnSc1d1	symbolická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
singlem	singl	k1gInSc7	singl
vydaným	vydaný	k2eAgInSc7d1	vydaný
za	za	k7c2	za
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
se	se	k3xPyFc4	se
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
stala	stát	k5eAaPmAgFnS	stát
megahitová	megahitový	k2eAgFnSc1d1	megahitový
skladba	skladba	k1gFnSc1	skladba
The	The	k1gFnSc2	The
Show	show	k1gFnPc2	show
Must	Must	k1gMnSc1	Must
Go	Go	k1gMnSc1	Go
On	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
dostala	dostat	k5eAaPmAgFnS	dostat
v	v	k7c6	v
hitparádě	hitparáda	k1gFnSc6	hitparáda
vysoko	vysoko	k6eAd1	vysoko
a	a	k8xC	a
například	například	k6eAd1	například
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
držela	držet	k5eAaImAgFnS	držet
26	[number]	k4	26
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
Freddiemu	Freddiem	k1gInSc2	Freddiem
neumožňoval	umožňovat	k5eNaImAgInS	umožňovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
k	k	k7c3	k
ní	on	k3xPp3gFnSc2	on
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
video	video	k1gNnSc1	video
<g/>
.	.	kIx.	.
</s>
<s>
Queen	Queen	k1gInSc1	Queen
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
rozhodnou	rozhodnout	k5eAaPmIp3nP	rozhodnout
videoklip	videoklip	k1gInSc4	videoklip
pojmout	pojmout	k5eAaPmF	pojmout
jako	jako	k8xC	jako
sestřih	sestřih	k1gInSc4	sestřih
jejich	jejich	k3xOp3gInPc2	jejich
nejúspěšnějších	úspěšný	k2eAgInPc2d3	nejúspěšnější
okamžiků	okamžik	k1gInPc2	okamžik
z	z	k7c2	z
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
využít	využít	k5eAaPmF	využít
ho	on	k3xPp3gMnSc4	on
k	k	k7c3	k
propagaci	propagace	k1gFnSc3	propagace
chystaného	chystaný	k2eAgNnSc2d1	chystané
videa	video	k1gNnSc2	video
Greatest	Greatest	k1gInSc1	Greatest
Hits	Hits	k1gInSc4	Hits
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
vychází	vycházet	k5eAaImIp3nS	vycházet
druhá	druhý	k4xOgFnSc1	druhý
kompilace	kompilace	k1gFnSc1	kompilace
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
Queen	Queen	k1gInSc1	Queen
<g/>
,	,	kIx,	,
Greatest	Greatest	k1gInSc1	Greatest
Hits	Hits	k1gInSc1	Hits
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
řadu	řada	k1gFnSc4	řada
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
hitparádě	hitparáda	k1gFnSc6	hitparáda
do	do	k7c2	do
Top	topit	k5eAaImRp2nS	topit
Ten	ten	k3xDgMnSc1	ten
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
umístily	umístit	k5eAaPmAgFnP	umístit
na	na	k7c6	na
vyšších	vysoký	k2eAgFnPc6d2	vyšší
pozicích	pozice	k1gFnPc6	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
A	a	k8xC	a
Kind	Kinda	k1gFnPc2	Kinda
of	of	k?	of
Magic	Magice	k1gFnPc2	Magice
<g/>
,	,	kIx,	,
Radio	radio	k1gNnSc4	radio
Ga	Ga	k1gMnSc2	Ga
Ga	Ga	k1gMnSc2	Ga
<g/>
,	,	kIx,	,
I	i	k8xC	i
Want	Want	k2eAgMnSc1d1	Want
to	ten	k3xDgNnSc1	ten
Break	break	k1gInSc4	break
Free	Fre	k1gMnSc2	Fre
nebo	nebo	k8xC	nebo
The	The	k1gMnSc2	The
Show	show	k1gFnSc1	show
Must	Must	k1gInSc1	Must
Go	Go	k1gFnSc4	Go
On	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
vychází	vycházet	k5eAaImIp3nS	vycházet
pouhý	pouhý	k2eAgInSc4d1	pouhý
měsíc	měsíc	k1gInSc4	měsíc
před	před	k7c4	před
Mercuryho	Mercury	k1gMnSc4	Mercury
smrtí	smrt	k1gFnPc2	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1991	[number]	k4	1991
kolem	kolem	k7c2	kolem
20	[number]	k4	20
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
Freddie	Freddie	k1gFnSc1	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
nechá	nechat	k5eAaPmIp3nS	nechat
poprvé	poprvé	k6eAd1	poprvé
veřejně	veřejně	k6eAd1	veřejně
oznámit	oznámit	k5eAaPmF	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
trpí	trpět	k5eAaImIp3nS	trpět
AIDS	aids	k1gInSc4	aids
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
chorobě	choroba	k1gFnSc6	choroba
věděli	vědět	k5eAaImAgMnP	vědět
pouze	pouze	k6eAd1	pouze
Freddieho	Freddieha	k1gFnSc5	Freddieha
nejbližší	blízký	k2eAgMnSc1d3	nejbližší
přátelé	přítel	k1gMnPc1	přítel
a	a	k8xC	a
členové	člen	k1gMnPc1	člen
Queen	Quena	k1gFnPc2	Quena
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
v	v	k7c6	v
Londýnském	londýnský	k2eAgInSc6d1	londýnský
Kensingtonu	Kensington	k1gInSc6	Kensington
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
až	až	k9	až
do	do	k7c2	do
posledních	poslední	k2eAgInPc2d1	poslední
okamžiků	okamžik	k1gInPc2	okamžik
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
poslední	poslední	k2eAgFnPc1d1	poslední
písně	píseň	k1gFnPc1	píseň
si	se	k3xPyFc3	se
zbytek	zbytek	k1gInSc4	zbytek
Queen	Queen	k1gInSc1	Queen
šetří	šetřit	k5eAaImIp3nS	šetřit
do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
studiové	studiový	k2eAgFnSc2d1	studiová
desky	deska	k1gFnSc2	deska
Made	Made	k1gNnSc2	Made
in	in	k?	in
Heaven	Heavna	k1gFnPc2	Heavna
vydané	vydaný	k2eAgNnSc4d1	vydané
až	až	k9	až
4	[number]	k4	4
roky	rok	k1gInPc4	rok
po	po	k7c4	po
Mercuryho	Mercury	k1gMnSc4	Mercury
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgFnPc1d1	mnohá
písně	píseň	k1gFnPc1	píseň
z	z	k7c2	z
právě	právě	k6eAd1	právě
zmiňovaného	zmiňovaný	k2eAgNnSc2d1	zmiňované
alba	album	k1gNnSc2	album
museli	muset	k5eAaImAgMnP	muset
zbývající	zbývající	k2eAgMnPc1d1	zbývající
členové	člen	k1gMnPc1	člen
Queen	Queen	k1gInSc4	Queen
dokončit	dokončit	k5eAaPmF	dokončit
sami	sám	k3xTgMnPc1	sám
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1991	[number]	k4	1991
jsou	být	k5eAaImIp3nP	být
Queen	Queno	k1gNnPc2	Queno
nominováni	nominovat	k5eAaBmNgMnP	nominovat
na	na	k7c4	na
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
britskou	britský	k2eAgFnSc4d1	britská
skupinu	skupina	k1gFnSc4	skupina
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
také	také	k9	také
Bohemian	bohemian	k1gInSc1	bohemian
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
<g/>
,	,	kIx,	,
jako	jako	k9	jako
hold	hold	k1gInSc1	hold
zemřelému	zemřelý	k1gMnSc3	zemřelý
příteli	přítel	k1gMnSc3	přítel
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vzlétla	vzlétnout	k5eAaPmAgFnS	vzlétnout
na	na	k7c4	na
první	první	k4xOgFnSc4	první
příčku	příčka	k1gFnSc4	příčka
hitparády	hitparáda	k1gFnSc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Výtěžek	výtěžek	k1gInSc1	výtěžek
prodeje	prodej	k1gInSc2	prodej
je	být	k5eAaImIp3nS	být
darován	darovat	k5eAaPmNgInS	darovat
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
a	a	k8xC	a
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
AIDS	AIDS	kA	AIDS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
na	na	k7c6	na
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
Wembley	Wemble	k1gMnPc7	Wemble
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
velký	velký	k2eAgInSc1d1	velký
vzpomínkový	vzpomínkový	k2eAgInSc1d1	vzpomínkový
koncert	koncert	k1gInSc1	koncert
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Freddiemu	Freddiem	k1gInSc2	Freddiem
–	–	k?	–
The	The	k1gFnSc2	The
Freddie	Freddie	k1gFnSc2	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
Tribute	tribut	k1gInSc5	tribut
Concert	Concert	k1gInSc4	Concert
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
David	David	k1gMnSc1	David
Bowie	Bowie	k1gFnSc1	Bowie
<g/>
,	,	kIx,	,
Axl	Axl	k1gMnSc1	Axl
Rose	Rose	k1gMnSc1	Rose
<g/>
,	,	kIx,	,
Elton	Elton	k1gMnSc1	Elton
John	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Plant	planta	k1gFnPc2	planta
nebo	nebo	k8xC	nebo
George	George	k1gFnPc2	George
Michael	Michael	k1gMnSc1	Michael
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
záznam	záznam	k1gInSc1	záznam
koncertu	koncert	k1gInSc2	koncert
Queen	Queen	k2eAgInSc1d1	Queen
-	-	kIx~	-
Live	Live	k1gInSc1	Live
at	at	k?	at
Wembley	Wemblea	k1gFnSc2	Wemblea
Stadium	stadium	k1gNnSc1	stadium
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
odehráli	odehrát	k5eAaPmAgMnP	odehrát
během	během	k7c2	během
turné	turné	k1gNnSc2	turné
The	The	k1gFnSc2	The
Magic	Magic	k1gMnSc1	Magic
Tour	Tour	k1gMnSc1	Tour
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
</s>
</p>
<p>
<s>
===	===	k?	===
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
:	:	kIx,	:
Made	Mad	k1gInSc2	Mad
in	in	k?	in
Heaven	Heavna	k1gFnPc2	Heavna
až	až	k9	až
46664	[number]	k4	46664
Concert	Concert	k1gInSc1	Concert
===	===	k?	===
</s>
</p>
<p>
<s>
Rok	rok	k1gInSc1	rok
1995	[number]	k4	1995
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
poslední	poslední	k2eAgFnSc2d1	poslední
studiové	studiový	k2eAgFnSc2d1	studiová
desky	deska	k1gFnSc2	deska
Made	Mad	k1gFnSc2	Mad
in	in	k?	in
haven	haven	k1gInSc1	haven
s	s	k7c7	s
písněmi	píseň	k1gFnPc7	píseň
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc4	některý
Freddie	Freddie	k1gFnPc4	Freddie
nahrál	nahrát	k5eAaPmAgMnS	nahrát
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
a	a	k8xC	a
doposud	doposud	k6eAd1	doposud
nebyly	být	k5eNaImAgInP	být
vydány	vydat	k5eAaPmNgInP	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
komerčně	komerčně	k6eAd1	komerčně
velmi	velmi	k6eAd1	velmi
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
<g/>
.	.	kIx.	.
</s>
<s>
Skladby	skladba	k1gFnPc1	skladba
Heaven	Heavna	k1gFnPc2	Heavna
for	forum	k1gNnPc2	forum
Everyone	Everyon	k1gMnSc5	Everyon
a	a	k8xC	a
Too	Too	k1gMnSc5	Too
Much	moucha	k1gFnPc2	moucha
Love	lov	k1gInSc5	lov
Will	Will	k1gMnSc1	Will
Kill	Killa	k1gFnPc2	Killa
You	You	k1gMnSc1	You
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
hity	hit	k1gInPc1	hit
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
mrazivá	mrazivý	k2eAgFnSc1d1	mrazivá
je	být	k5eAaImIp3nS	být
i	i	k9	i
poslední	poslední	k2eAgFnSc4d1	poslední
píseň	píseň	k1gFnSc4	píseň
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Freddie	Freddie	k1gFnSc1	Freddie
nazpíval	nazpívat	k5eAaBmAgMnS	nazpívat
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
jen	jen	k9	jen
polovinu	polovina	k1gFnSc4	polovina
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
část	část	k1gFnSc4	část
mu	on	k3xPp3gMnSc3	on
nezbyly	zbýt	k5eNaPmAgFnP	zbýt
síly	síla	k1gFnPc1	síla
<g/>
)	)	kIx)	)
Mother	Mothra	k1gFnPc2	Mothra
Love	lov	k1gInSc5	lov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
je	být	k5eAaImIp3nS	být
slavnostně	slavnostně	k6eAd1	slavnostně
odhalena	odhalit	k5eAaPmNgFnS	odhalit
Mercuryho	Mercury	k1gMnSc2	Mercury
socha	socha	k1gFnSc1	socha
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgNnSc6d1	švýcarské
Montreaux	Montreaux	k1gInSc4	Montreaux
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
Queen	Queen	k1gInSc4	Queen
natáčeli	natáčet	k5eAaImAgMnP	natáčet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
album	album	k1gNnSc4	album
Jazz	jazz	k1gInSc1	jazz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
vychází	vycházet	k5eAaImIp3nS	vycházet
jediný	jediný	k2eAgInSc1d1	jediný
a	a	k8xC	a
zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgInSc4d1	poslední
nový	nový	k2eAgInSc4d1	nový
singl	singl	k1gInSc4	singl
Queen	Queen	k2eAgInSc4d1	Queen
bez	bez	k7c2	bez
Freddieho	Freddie	k1gMnSc2	Freddie
Mercuryho	Mercury	k1gMnSc2	Mercury
a	a	k8xC	a
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
všech	všecek	k3xTgInPc2	všecek
zbylých	zbylý	k2eAgInPc2d1	zbylý
členů	člen	k1gInPc2	člen
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
No-One	No-On	k1gInSc5	No-On
but	but	k?	but
You	You	k1gFnPc1	You
(	(	kIx(	(
<g/>
Only	Onla	k1gFnPc1	Onla
the	the	k?	the
Good	Good	k1gMnSc1	Good
Die	Die	k1gMnSc1	Die
Young	Young	k1gMnSc1	Young
<g/>
)	)	kIx)	)
na	na	k7c6	na
albu	album	k1gNnSc6	album
Queen	Queen	k2eAgInSc4d1	Queen
Rocks	Rocks	k1gInSc4	Rocks
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
baskytaristu	baskytarista	k1gMnSc4	baskytarista
Johna	John	k1gMnSc4	John
Deacona	Deacon	k1gMnSc4	Deacon
skončila	skončit	k5eAaPmAgFnS	skončit
činnost	činnost	k1gFnSc1	činnost
Queen	Quena	k1gFnPc2	Quena
rokem	rok	k1gInSc7	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
přáteli	přítel	k1gMnPc7	přítel
na	na	k7c6	na
vzpomínkovém	vzpomínkový	k2eAgInSc6d1	vzpomínkový
koncertě	koncert	k1gInSc6	koncert
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
dokončuje	dokončovat	k5eAaImIp3nS	dokončovat
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
desku	deska	k1gFnSc4	deska
Made	Made	k1gNnSc1	Made
in	in	k?	in
Heaven	Heavno	k1gNnPc2	Heavno
<g/>
,	,	kIx,	,
natáčí	natáčet	k5eAaImIp3nS	natáčet
i	i	k9	i
singl	singl	k1gInSc1	singl
No-One	No-On	k1gInSc5	No-On
but	but	k?	but
You	You	k1gFnPc1	You
(	(	kIx(	(
<g/>
Only	Onla	k1gFnPc1	Onla
the	the	k?	the
Good	Good	k1gMnSc1	Good
Die	Die	k1gMnSc1	Die
Young	Young	k1gMnSc1	Young
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c6	o
obnovení	obnovení	k1gNnSc6	obnovení
skupiny	skupina	k1gFnSc2	skupina
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Paulem	Paul	k1gMnSc7	Paul
Rodgersem	Rodgers	k1gMnSc7	Rodgers
již	již	k9	již
nemá	mít	k5eNaImIp3nS	mít
zájem	zájem	k1gInSc1	zájem
nemá	mít	k5eNaImIp3nS	mít
<g/>
,	,	kIx,	,
stahuje	stahovat	k5eAaImIp3nS	stahovat
se	se	k3xPyFc4	se
z	z	k7c2	z
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
a	a	k8xC	a
Queen	Queen	k1gInSc1	Queen
opouští	opouštět	k5eAaImIp3nS	opouštět
Vychází	vycházet	k5eAaImIp3nS	vycházet
kompilace	kompilace	k1gFnSc1	kompilace
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
Greatest	Greatest	k1gFnSc1	Greatest
Hits	Hits	k1gInSc1	Hits
III	III	kA	III
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
předchozí	předchozí	k2eAgInPc4d1	předchozí
dva	dva	k4xCgInPc4	dva
výběry	výběr	k1gInPc4	výběr
<g/>
.	.	kIx.	.
</s>
<s>
Najdeme	najít	k5eAaPmIp1nP	najít
zde	zde	k6eAd1	zde
i	i	k9	i
skladby	skladba	k1gFnPc4	skladba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
Queen	Queen	k1gInSc4	Queen
nahráli	nahrát	k5eAaPmAgMnP	nahrát
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Show	show	k1gNnSc1	show
Must	Must	k1gMnSc1	Must
Go	Go	k1gMnSc1	Go
On	on	k3xPp3gMnSc1	on
s	s	k7c7	s
Eltonem	Elton	k1gMnSc7	Elton
Johnem	John	k1gMnSc7	John
<g/>
,	,	kIx,	,
Under	Under	k1gMnSc1	Under
Pressure	Pressur	k1gMnSc5	Pressur
s	s	k7c7	s
Davidem	David	k1gMnSc7	David
Bowiem	Bowius	k1gMnSc7	Bowius
<g/>
,	,	kIx,	,
Barcelona	Barcelona	k1gFnSc1	Barcelona
–	–	k?	–
Freddie	Freddie	k1gFnSc1	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
+	+	kIx~	+
Montserrat	Montserrat	k1gInSc1	Montserrat
Caballé	Caball	k1gMnPc1	Caball
<g/>
,	,	kIx,	,
Somebody	Someboda	k1gFnPc1	Someboda
to	ten	k3xDgNnSc1	ten
Love	lov	k1gInSc5	lov
s	s	k7c7	s
Georgem	Georg	k1gMnSc7	Georg
Michaelem	Michael	k1gMnSc7	Michael
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jediné	jediný	k2eAgNnSc1d1	jediné
album	album	k1gNnSc1	album
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
jak	jak	k6eAd1	jak
skladby	skladba	k1gFnPc4	skladba
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
sólové	sólový	k2eAgInPc4d1	sólový
projekty	projekt	k1gInPc4	projekt
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Queen	Quena	k1gFnPc2	Quena
+	+	kIx~	+
Paul	Paula	k1gFnPc2	Paula
Rodgers	Rodgers	k1gInSc1	Rodgers
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
milovníky	milovník	k1gMnPc4	milovník
živých	živý	k2eAgNnPc2d1	živé
alb	album	k1gNnPc2	album
vydávají	vydávat	k5eAaPmIp3nP	vydávat
Queen	Queen	k1gInSc4	Queen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
dvoudiskové	dvoudiskový	k2eAgInPc4d1	dvoudiskový
album	album	k1gNnSc4	album
Queen	Queen	k2eAgInSc1d1	Queen
on	on	k3xPp3gInSc1	on
Fire	Fire	k1gInSc1	Fire
–	–	k?	–
Live	Live	k1gInSc1	Live
at	at	k?	at
the	the	k?	the
Bowl	Bowl	k1gInSc1	Bowl
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc1	píseň
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Queen	Quena	k1gFnPc2	Quena
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
Milton	Milton	k1gInSc1	Milton
Keynes	Keynes	k1gMnSc1	Keynes
Bowl	Bowl	k1gMnSc1	Bowl
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1982	[number]	k4	1982
natáčel	natáčet	k5eAaImAgMnS	natáčet
Channel	Channel	k1gMnSc1	Channel
Four	Four	k1gMnSc1	Four
a	a	k8xC	a
díky	díky	k7c3	díky
němu	on	k3xPp3gMnSc3	on
mohl	moct	k5eAaImAgInS	moct
koncert	koncert	k1gInSc1	koncert
vyjít	vyjít	k5eAaPmF	vyjít
jako	jako	k9	jako
dvoudiskové	dvoudiskový	k2eAgNnSc4d1	dvoudiskové
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Queen	Queen	k2eAgInSc1d1	Queen
Rock	rock	k1gInSc1	rock
Montreal	Montreal	k1gInSc1	Montreal
<g/>
.	.	kIx.	.
</s>
<s>
Queen	Queen	k1gInSc4	Queen
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
živě	živě	k6eAd1	živě
v	v	k7c6	v
Montrealu	Montreal	k1gInSc6	Montreal
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
síle	síla	k1gFnSc6	síla
a	a	k8xC	a
playlist	playlist	k1gInSc1	playlist
koncertu	koncert	k1gInSc2	koncert
více	hodně	k6eAd2	hodně
než	než	k8xS	než
skvělý	skvělý	k2eAgInSc1d1	skvělý
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
najdeme	najít	k5eAaPmIp1nP	najít
největší	veliký	k2eAgInPc4d3	veliký
hity	hit	k1gInPc4	hit
Queen	Quena	k1gFnPc2	Quena
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Fandové	Fanda	k1gMnPc1	Fanda
při	při	k7c6	při
Somebody	Someboda	k1gFnPc1	Someboda
to	ten	k3xDgNnSc1	ten
Love	lov	k1gInSc5	lov
<g/>
,	,	kIx,	,
Tie	Tie	k1gFnPc3	Tie
Your	Youra	k1gFnPc2	Youra
Mother	Mothra	k1gFnPc2	Mothra
Down	Down	k1gInSc1	Down
či	či	k8xC	či
Bohemian	bohemian	k1gInSc1	bohemian
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
div	div	k6eAd1	div
nezbourají	zbourat	k5eNaPmIp3nP	zbourat
montrealské	montrealský	k2eAgNnSc4d1	montrealské
Forum	forum	k1gNnSc4	forum
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
vychází	vycházet	k5eAaImIp3nS	vycházet
jako	jako	k9	jako
dvoudiskové	dvoudiskový	k2eAgInPc1d1	dvoudiskový
CD	CD	kA	CD
<g/>
,	,	kIx,	,
Dvoudiskové	dvoudiskový	k2eAgFnPc4d1	dvoudisková
DVD	DVD	kA	DVD
a	a	k8xC	a
také	také	k9	také
jako	jako	k9	jako
speciální	speciální	k2eAgFnSc1d1	speciální
dvoudisková	dvoudiskový	k2eAgFnSc1d1	dvoudisková
edice	edice	k1gFnSc1	edice
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
najdeme	najít	k5eAaPmIp1nP	najít
kromě	kromě	k7c2	kromě
koncertu	koncert	k1gInSc2	koncert
také	také	k9	také
DVD	DVD	kA	DVD
s	s	k7c7	s
vystoupením	vystoupení	k1gNnSc7	vystoupení
Queen	Quena	k1gFnPc2	Quena
na	na	k7c6	na
legendárním	legendární	k2eAgInSc6d1	legendární
koncertě	koncert	k1gInSc6	koncert
Live	Liv	k1gFnSc2	Liv
Aid	Aida	k1gFnPc2	Aida
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Queen	Queen	k1gInSc4	Queen
vydávají	vydávat	k5eAaImIp3nP	vydávat
zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgNnSc4d1	poslední
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
The	The	k1gFnSc2	The
Cosmos	Cosmosa	k1gFnPc2	Cosmosa
Rocks	Rocksa	k1gFnPc2	Rocksa
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Paulem	Paul	k1gMnSc7	Paul
Rodgersem	Rodgers	k1gInSc7	Rodgers
(	(	kIx(	(
<g/>
Queen	Queen	k1gInSc1	Queen
<g/>
+	+	kIx~	+
<g/>
Paul	Paul	k1gMnSc1	Paul
Rodgers	Rodgersa	k1gFnPc2	Rodgersa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
je	být	k5eAaImIp3nS	být
jediné	jediný	k2eAgNnSc1d1	jediné
album	album	k1gNnSc1	album
Queen	Quena	k1gFnPc2	Quena
bez	bez	k7c2	bez
hlasu	hlas	k1gInSc2	hlas
Freddieho	Freddie	k1gMnSc2	Freddie
Mercuryho	Mercury	k1gMnSc2	Mercury
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
hity	hit	k1gInPc7	hit
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
písně	píseň	k1gFnPc1	píseň
Say	Say	k1gMnSc2	Say
It	It	k1gMnSc2	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Not	nota	k1gFnPc2	nota
True	True	k1gNnSc1	True
a	a	k8xC	a
C-lebrity	Cebrita	k1gFnPc1	C-lebrita
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zároveň	zároveň	k6eAd1	zároveň
i	i	k8xC	i
sigly	sigla	k1gFnPc1	sigla
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2009	[number]	k4	2009
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
Odchod	odchod	k1gInSc1	odchod
od	od	k7c2	od
EMI	EMI	kA	EMI
<g/>
,	,	kIx,	,
40	[number]	k4	40
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
===	===	k?	===
</s>
</p>
<p>
<s>
Queen	Queen	k1gInSc4	Queen
oslavují	oslavovat	k5eAaImIp3nP	oslavovat
40	[number]	k4	40
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pořádána	pořádán	k2eAgFnSc1d1	pořádána
výstava	výstava	k1gFnSc1	výstava
o	o	k7c6	o
jejich	jejich	k3xOp3gInSc6	jejich
díle	díl	k1gInSc6	díl
s	s	k7c7	s
názvem	název	k1gInSc7	název
Stormtrooper	Stormtrooper	k1gMnSc1	Stormtrooper
In	In	k1gMnSc1	In
Stilletos	Stilletos	k1gMnSc1	Stilletos
a	a	k8xC	a
do	do	k7c2	do
obchodů	obchod	k1gInPc2	obchod
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
remasterovaná	remasterovaný	k2eAgFnSc1d1	remasterovaná
kolekce	kolekce	k1gFnSc1	kolekce
všech	všecek	k3xTgNnPc2	všecek
řadových	řadový	k2eAgNnPc2d1	řadové
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc4d1	nová
smlouvu	smlouva	k1gFnSc4	smlouva
podepisují	podepisovat	k5eAaImIp3nP	podepisovat
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
Universal	Universal	k1gMnSc1	Universal
Music	Music	k1gMnSc1	Music
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2011	[number]	k4	2011
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
:	:	kIx,	:
Queen	Queen	k2eAgMnSc1d1	Queen
+	+	kIx~	+
Adam	Adam	k1gMnSc1	Adam
Lambert	Lambert	k1gMnSc1	Lambert
<g/>
;	;	kIx,	;
Queen	Queen	k2eAgInSc1d1	Queen
Forever	Forever	k1gInSc1	Forever
===	===	k?	===
</s>
</p>
<p>
<s>
Queen	Queen	k1gInSc4	Queen
odehráli	odehrát	k5eAaPmAgMnP	odehrát
mini	mini	k2eAgNnSc4d1	mini
turné	turné	k1gNnSc4	turné
s	s	k7c7	s
americkým	americký	k2eAgMnSc7d1	americký
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Adamem	Adam	k1gMnSc7	Adam
Lambertem	Lambert	k1gMnSc7	Lambert
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Lambert	Lambert	k1gMnSc1	Lambert
měl	mít	k5eAaImAgMnS	mít
vystoupit	vystoupit	k5eAaPmF	vystoupit
s	s	k7c7	s
Queen	Quena	k1gFnPc2	Quena
jako	jako	k8xS	jako
headline	headlin	k1gInSc5	headlin
na	na	k7c4	na
festivalu	festival	k1gInSc2	festival
Sonisphere	Sonispher	k1gInSc5	Sonispher
v	v	k7c6	v
UK	UK	kA	UK
(	(	kIx(	(
<g/>
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vystoupení	vystoupení	k1gNnSc1	vystoupení
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
signifikantní	signifikantní	k2eAgNnSc1d1	signifikantní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Knebworth	Knebworth	k1gMnSc1	Knebworth
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
festival	festival	k1gInSc1	festival
konat	konat	k5eAaImF	konat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
posledním	poslední	k2eAgNnSc7d1	poslední
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
Freddie	Freddie	k1gFnPc4	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
diskusi	diskuse	k1gFnSc6	diskuse
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
spolupráci	spolupráce	k1gFnSc3	spolupráce
Brian	Brian	k1gMnSc1	Brian
May	May	k1gMnSc1	May
podotknul	podotknout	k5eAaPmAgMnS	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
očekává	očekávat	k5eAaImIp3nS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lambert	Lambert	k1gMnSc1	Lambert
bude	být	k5eAaImBp3nS	být
"	"	kIx"	"
<g/>
skvělým	skvělý	k2eAgMnSc7d1	skvělý
interpretem	interpret	k1gMnSc7	interpret
<g/>
"	"	kIx"	"
Mercuryho	Mercury	k1gMnSc2	Mercury
písní	píseň	k1gFnSc7	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Festival	festival	k1gInSc1	festival
Sonisphere	Sonispher	k1gInSc5	Sonispher
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
logistikou	logistika	k1gFnSc7	logistika
a	a	k8xC	a
vlivem	vliv	k1gInSc7	vliv
dalších	další	k2eAgFnPc2d1	další
okolností	okolnost	k1gFnPc2	okolnost
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
Roger	Roger	k1gMnSc1	Roger
Taylor	Taylor	k1gMnSc1	Taylor
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Queen	Queen	k1gInSc4	Queen
a	a	k8xC	a
Lambert	Lambert	k1gInSc4	Lambert
budou	být	k5eAaImBp3nP	být
spolu	spolu	k6eAd1	spolu
vystupovat	vystupovat	k5eAaImF	vystupovat
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
a	a	k8xC	a
pak	pak	k6eAd1	pak
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přidají	přidat	k5eAaPmIp3nP	přidat
2	[number]	k4	2
další	další	k2eAgFnPc1d1	další
show	show	k1gFnPc1	show
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
a	a	k8xC	a
jednu	jeden	k4xCgFnSc4	jeden
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgInPc4	který
bude	být	k5eAaImBp3nS	být
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgFnSc1d1	hlavní
hvězda	hvězda	k1gFnSc1	hvězda
vystupovat	vystupovat	k5eAaImF	vystupovat
Elton	Elton	k1gInSc4	Elton
John	John	k1gMnSc1	John
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
dubna	duben	k1gInSc2	duben
byl	být	k5eAaImAgInS	být
přidán	přidat	k5eAaPmNgInS	přidat
pátý	pátý	k4xOgInSc1	pátý
koncert	koncert	k1gInSc1	koncert
ve	v	k7c6	v
Wroclawi	Wroclaw	k1gInSc6	Wroclaw
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
headline	headlin	k1gInSc5	headlin
tamějšího	tamější	k2eAgInSc2d1	tamější
rockového	rockový	k2eAgInSc2d1	rockový
festivalu	festival	k1gInSc2	festival
<g/>
.	.	kIx.	.
<g/>
Finální	finální	k2eAgFnSc1d1	finální
verze	verze	k1gFnSc1	verze
spolupráce	spolupráce	k1gFnSc2	spolupráce
Queen	Quena	k1gFnPc2	Quena
+	+	kIx~	+
Adama	Adam	k1gMnSc2	Adam
Lamberta	Lambert	k1gMnSc2	Lambert
začala	začít	k5eAaPmAgFnS	začít
dvouhodinovou	dvouhodinový	k2eAgFnSc4d1	dvouhodinová
velkolepou	velkolepý	k2eAgFnSc4d1	velkolepá
podívanou	podívaná	k1gFnSc4	podívaná
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
UEFA	UEFA	kA	UEFA
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
v	v	k7c4	v
předvečer	předvečer	k1gInSc4	předvečer
vyvrcholení	vyvrcholení	k1gNnSc2	vyvrcholení
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
šampionátu	šampionát	k1gInSc2	šampionát
EURO	euro	k1gNnSc1	euro
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Stovky	stovka	k1gFnPc1	stovka
tisíc	tisíc	k4xCgInPc2	tisíc
fanoušků	fanoušek	k1gMnPc2	fanoušek
na	na	k7c6	na
ukrajinském	ukrajinský	k2eAgNnSc6d1	ukrajinské
hlavním	hlavní	k2eAgNnSc6d1	hlavní
náměstí	náměstí	k1gNnSc6	náměstí
ocenilo	ocenit	k5eAaPmAgNnS	ocenit
toto	tento	k3xDgNnSc1	tento
vystoupení	vystoupení	k1gNnSc1	vystoupení
pro	pro	k7c4	pro
Lambertův	Lambertův	k2eAgInSc4d1	Lambertův
umělecký	umělecký	k2eAgInSc4d1	umělecký
talent	talent	k1gInSc4	talent
<g/>
,	,	kIx,	,
hlasovou	hlasový	k2eAgFnSc4d1	hlasová
výjimečnost	výjimečnost	k1gFnSc4	výjimečnost
a	a	k8xC	a
schopnost	schopnost	k1gFnSc4	schopnost
interpretace	interpretace	k1gFnSc2	interpretace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	s	k7c7	s
sebejistotou	sebejistota	k1gFnSc7	sebejistota
vyvážila	vyvážit	k5eAaPmAgFnS	vyvážit
staré	starý	k2eAgInPc4d1	starý
a	a	k8xC	a
nové	nový	k2eAgInPc4d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Koncert	koncert	k1gInSc1	koncert
na	na	k7c6	na
Olympijském	olympijský	k2eAgInSc6d1	olympijský
stadionu	stadion	k1gInSc6	stadion
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
3	[number]	k4	3
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
ten	ten	k3xDgInSc1	ten
následný	následný	k2eAgInSc1d1	následný
na	na	k7c6	na
městském	městský	k2eAgInSc6d1	městský
stadionu	stadion	k1gInSc6	stadion
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
byl	být	k5eAaImAgInS	být
naplánován	naplánovat	k5eAaBmNgInS	naplánovat
na	na	k7c6	na
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
významnému	významný	k2eAgNnSc3d1	významné
postavení	postavení	k1gNnSc3	postavení
Queen	Quena	k1gFnPc2	Quena
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
nově	nově	k6eAd1	nově
příchozím	příchozí	k1gMnSc7	příchozí
Lambertem	Lambert	k1gMnSc7	Lambert
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgInP	mít
Londýnské	londýnský	k2eAgInPc1d1	londýnský
tři	tři	k4xCgInPc1	tři
koncerty	koncert	k1gInPc1	koncert
v	v	k7c6	v
HMV	HMV	kA	HMV
Hammersmidth	Hammersmidth	k1gMnSc1	Hammersmidth
Apollo	Apollo	k1gMnSc1	Apollo
spoustu	spousta	k1gFnSc4	spousta
recenzí	recenze	k1gFnPc2	recenze
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
jednotně	jednotně	k6eAd1	jednotně
byly	být	k5eAaImAgInP	být
tyto	tento	k3xDgInPc1	tento
koncerty	koncert	k1gInPc1	koncert
známé	známý	k2eAgInPc1d1	známý
pro	pro	k7c4	pro
Lambertovy	Lambertův	k2eAgFnPc4d1	Lambertova
vokálové	vokálový	k2eAgNnSc1d1	vokálové
vedení	vedení	k1gNnSc3	vedení
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
'	'	kIx"	'
<g/>
sexy	sex	k1gInPc1	sex
<g/>
'	'	kIx"	'
a	a	k8xC	a
'	'	kIx"	'
<g/>
vzrušující	vzrušující	k2eAgNnPc4d1	vzrušující
<g/>
'	'	kIx"	'
vystupování	vystupování	k1gNnPc4	vystupování
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
se	s	k7c7	s
vzájemnou	vzájemný	k2eAgFnSc7d1	vzájemná
spoluprací	spolupráce	k1gFnSc7	spolupráce
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Brian	Brian	k1gMnSc1	Brian
May	May	k1gMnSc1	May
nazval	nazvat	k5eAaBmAgMnS	nazvat
"	"	kIx"	"
<g/>
organickou	organický	k2eAgFnSc7d1	organická
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Skupina	skupina	k1gFnSc1	skupina
Queen	Quena	k1gFnPc2	Quena
vystoupí	vystoupit	k5eAaPmIp3nS	vystoupit
s	s	k7c7	s
Adamem	Adam	k1gMnSc7	Adam
Lambertem	Lambert	k1gMnSc7	Lambert
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
září	září	k1gNnSc6	září
2013	[number]	k4	2013
na	na	k7c4	na
iHeartRadio	iHeartRadio	k6eAd1	iHeartRadio
Music	Musice	k1gInPc2	Musice
Festival	festival	k1gInSc4	festival
v	v	k7c6	v
MGM	MGM	kA	MGM
Grand	grand	k1gMnSc1	grand
Aréně	aréna	k1gFnSc6	aréna
v	v	k7c6	v
Las	laso	k1gNnPc2	laso
Vegas	Vegasa	k1gFnPc2	Vegasa
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgInSc2	tento
festivalu	festival	k1gInSc2	festival
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
největšího	veliký	k2eAgInSc2d3	veliký
živého	živý	k2eAgInSc2d1	živý
koncertu	koncert	k1gInSc2	koncert
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
rádia	rádio	k1gNnSc2	rádio
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zúčastní	zúčastnit	k5eAaPmIp3nS	zúčastnit
spousta	spousta	k6eAd1	spousta
hvězd	hvězda	k1gFnPc2	hvězda
zvučných	zvučný	k2eAgNnPc2d1	zvučné
jmen	jméno	k1gNnPc2	jméno
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
se	se	k3xPyFc4	se
konat	konat	k5eAaImF	konat
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
přenášený	přenášený	k2eAgMnSc1d1	přenášený
živě	živě	k6eAd1	živě
na	na	k7c4	na
rádio	rádio	k1gNnSc4	rádio
stanicích	stanice	k1gFnPc6	stanice
Clear	Clear	k1gMnSc1	Clear
Chanel	Chanel	k1gMnSc1	Chanel
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
také	také	k9	také
vysílaný	vysílaný	k2eAgMnSc1d1	vysílaný
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
dílech	díl	k1gInPc6	díl
jako	jako	k8xC	jako
speciální	speciální	k2eAgInSc1d1	speciální
televizní	televizní	k2eAgInSc1d1	televizní
program	program	k1gInSc1	program
<g/>
.12	.12	k4	.12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
kapela	kapela	k1gFnSc1	kapela
na	na	k7c6	na
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
ceremoniálu	ceremoniál	k1gInSc6	ceremoniál
Letních	letní	k2eAgFnPc2d1	letní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
2012	[number]	k4	2012
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Představení	představení	k1gNnSc1	představení
otevřel	otevřít	k5eAaPmAgInS	otevřít
videozáznam	videozáznam	k1gInSc4	videozáznam
hlasové	hlasový	k2eAgFnSc2d1	hlasová
improvizace	improvizace	k1gFnSc2	improvizace
Freddieho	Freddie	k1gMnSc2	Freddie
Mercuryho	Mercury	k1gMnSc2	Mercury
z	z	k7c2	z
koncertu	koncert	k1gInSc2	koncert
Queen	Quena	k1gFnPc2	Quena
ve	v	k7c4	v
Wembley	Wemblea	k1gFnPc4	Wemblea
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
promítán	promítán	k2eAgInSc4d1	promítán
na	na	k7c6	na
velkoplošných	velkoplošný	k2eAgFnPc6d1	velkoplošná
obrazovkách	obrazovka	k1gFnPc6	obrazovka
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
Brian	Brian	k1gMnSc1	Brian
May	May	k1gMnSc1	May
předvedl	předvést	k5eAaPmAgMnS	předvést
kytarové	kytarový	k2eAgNnSc4d1	kytarové
sólo	sólo	k1gNnSc4	sólo
ze	z	k7c2	z
skladby	skladba	k1gFnSc2	skladba
Brighton	Brighton	k1gInSc1	Brighton
Rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
se	se	k3xPyFc4	se
ke	k	k7c3	k
dvojici	dvojice	k1gFnSc3	dvojice
Brian	Brian	k1gMnSc1	Brian
May	May	k1gMnSc1	May
<g/>
,	,	kIx,	,
Roger	Roger	k1gInSc4	Roger
Taylor	Taylora	k1gFnPc2	Taylora
připojila	připojit	k5eAaPmAgFnS	připojit
britská	britský	k2eAgFnSc1d1	britská
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Jessie	Jessie	k1gFnSc2	Jessie
J.	J.	kA	J.
Společně	společně	k6eAd1	společně
zahráli	zahrát	k5eAaPmAgMnP	zahrát
píseň	píseň	k1gFnSc4	píseň
We	We	k1gFnSc2	We
Will	Will	k1gInSc1	Will
Rock	rock	k1gInSc1	rock
You	You	k1gFnSc1	You
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
Queen	Quena	k1gFnPc2	Quena
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Adamem	Adam	k1gMnSc7	Adam
Lambertem	Lambert	k1gMnSc7	Lambert
jako	jako	k8xC	jako
Queen	Queen	k1gInSc4	Queen
<g/>
+	+	kIx~	+
<g/>
Adam	Adam	k1gMnSc1	Adam
Lambert	Lambert	k1gMnSc1	Lambert
<g/>
.	.	kIx.	.
</s>
<s>
Adam	Adam	k1gMnSc1	Adam
Lambert	Lambert	k1gMnSc1	Lambert
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Paula	Paul	k1gMnSc4	Paul
Rodgerse	Rodgerse	k1gFnSc1	Rodgerse
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
s	s	k7c7	s
Queen	Queen	k1gInSc1	Queen
ukončil	ukončit	k5eAaPmAgInS	ukončit
spolupráci	spolupráce	k1gFnSc4	spolupráce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
tour	tour	k1gInSc1	tour
s	s	k7c7	s
Adamem	Adam	k1gMnSc7	Adam
Lambertem	Lambert	k1gMnSc7	Lambert
byla	být	k5eAaImAgFnS	být
krátká	krátká	k1gFnSc1	krátká
Evropská	evropský	k2eAgFnSc1d1	Evropská
tour	tour	k1gInSc4	tour
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
Queen	Queen	k1gInSc4	Queen
+	+	kIx~	+
Adam	Adam	k1gMnSc1	Adam
Lambert	Lambert	k1gMnSc1	Lambert
tour	tour	k1gMnSc1	tour
2014	[number]	k4	2014
<g/>
–	–	k?	–
<g/>
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
Queen	Queen	k1gInSc4	Queen
zavítali	zavítat	k5eAaPmAgMnP	zavítat
do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Austrálie	Austrálie	k1gFnSc2	Austrálie
včetně	včetně	k7c2	včetně
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Příští	příští	k2eAgInSc4d1	příští
rok	rok	k1gInSc4	rok
skupina	skupina	k1gFnSc1	skupina
ohlásila	ohlásit	k5eAaPmAgFnS	ohlásit
další	další	k2eAgFnSc1d1	další
<g/>
,	,	kIx,	,
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
již	již	k6eAd1	již
druhou	druhý	k4xOgFnSc7	druhý
<g/>
,	,	kIx,	,
světovou	světový	k2eAgFnSc7d1	světová
tour	tour	k1gInSc4	tour
Queen	Queen	k1gInSc1	Queen
+	+	kIx~	+
Adam	Adam	k1gMnSc1	Adam
Lambert	Lambert	k1gMnSc1	Lambert
tour	tour	k1gMnSc1	tour
2017	[number]	k4	2017
<g/>
–	–	k?	–
<g/>
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
dostává	dostávat	k5eAaImIp3nS	dostávat
skupina	skupina	k1gFnSc1	skupina
Cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Queen	Queen	k1gInSc1	Queen
+	+	kIx~	+
Adam	Adam	k1gMnSc1	Adam
Lambert	Lambert	k1gMnSc1	Lambert
chystají	chystat	k5eAaImIp3nP	chystat
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2019	[number]	k4	2019
další	další	k2eAgFnSc4d1	další
světovou	světový	k2eAgFnSc4d1	světová
tour	tour	k1gMnSc1	tour
"	"	kIx"	"
<g/>
The	The	k1gMnSc1	The
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
Tour	Tour	k1gMnSc1	Tour
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Největší	veliký	k2eAgInPc4d3	veliký
hity	hit	k1gInPc4	hit
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
70	[number]	k4	70
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
===	===	k?	===
</s>
</p>
<p>
<s>
Killer	Killer	k1gInSc1	Killer
Queen	Queen	k1gInSc1	Queen
(	(	kIx(	(
<g/>
Freddie	Freddie	k1gFnSc1	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bohemian	bohemian	k1gInSc1	bohemian
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
(	(	kIx(	(
<g/>
Mercury	Mercura	k1gFnSc2	Mercura
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Love	lov	k1gInSc5	lov
of	of	k?	of
My	my	k3xPp1nPc1	my
Life	Life	k1gNnPc7	Life
(	(	kIx(	(
<g/>
Mercury	Mercur	k1gInPc7	Mercur
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Somebody	Somebod	k1gMnPc4	Somebod
to	ten	k3xDgNnSc1	ten
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
Mercury	Mercur	k1gInPc4	Mercur
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
We	We	k?	We
Will	Will	k1gInSc1	Will
Rock	rock	k1gInSc1	rock
You	You	k1gFnSc1	You
(	(	kIx(	(
<g/>
Brian	Brian	k1gMnSc1	Brian
May	May	k1gMnSc1	May
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
We	We	k?	We
Are	ar	k1gInSc5	ar
the	the	k?	the
Champions	Champions	k1gInSc1	Champions
(	(	kIx(	(
<g/>
Mercury	Mercura	k1gFnPc1	Mercura
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Don	Don	k1gMnSc1	Don
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Stop	stop	k1gInSc1	stop
Me	Me	k1gMnSc2	Me
Now	Now	k1gMnSc2	Now
(	(	kIx(	(
<g/>
Mercury	Mercura	k1gFnPc1	Mercura
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Crazy	Craza	k1gFnPc1	Craza
Little	Little	k1gFnSc2	Little
Thing	Thing	k1gMnSc1	Thing
Called	Called	k1gMnSc1	Called
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
Mercury	Mercur	k1gInPc4	Mercur
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
80	[number]	k4	80
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
===	===	k?	===
</s>
</p>
<p>
<s>
Another	Anothra	k1gFnPc2	Anothra
One	One	k1gMnSc1	One
Bites	Bites	k1gMnSc1	Bites
the	the	k?	the
Dust	Dust	k1gMnSc1	Dust
(	(	kIx(	(
<g/>
John	John	k1gMnSc1	John
Deacon	Deacon	k1gMnSc1	Deacon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Under	Undrat	k5eAaPmRp2nS	Undrat
Pressure	Pressur	k1gMnSc5	Pressur
(	(	kIx(	(
<g/>
Queen	Queen	k2eAgMnSc1d1	Queen
+	+	kIx~	+
David	David	k1gMnSc1	David
Bowie	Bowie	k1gFnSc2	Bowie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Radio	radio	k1gNnSc1	radio
Ga	Ga	k1gMnSc1	Ga
Ga	Ga	k1gMnSc1	Ga
(	(	kIx(	(
<g/>
Roger	Roger	k1gMnSc1	Roger
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
I	i	k8xC	i
Want	Want	k2eAgMnSc1d1	Want
to	ten	k3xDgNnSc1	ten
Break	break	k1gInSc4	break
Free	Fre	k1gMnSc2	Fre
(	(	kIx(	(
<g/>
Deacon	Deacon	k1gMnSc1	Deacon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
One	One	k?	One
Vision	vision	k1gInSc1	vision
(	(	kIx(	(
<g/>
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
A	a	k9	a
Kind	Kind	k1gMnSc1	Kind
of	of	k?	of
Magic	Magic	k1gMnSc1	Magic
(	(	kIx(	(
<g/>
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Who	Who	k?	Who
Wants	Wants	k1gInSc1	Wants
to	ten	k3xDgNnSc4	ten
Live	Live	k1gNnSc4	Live
Forever	Forever	k1gMnSc1	Forever
(	(	kIx(	(
<g/>
May	May	k1gMnSc1	May
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
I	i	k9	i
Want	Want	k1gMnSc1	Want
It	It	k1gMnSc1	It
All	All	k1gMnSc1	All
(	(	kIx(	(
<g/>
May	May	k1gMnSc1	May
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Invisible	Invisible	k1gMnSc1	Invisible
Man	Man	k1gMnSc1	Man
(	(	kIx(	(
<g/>
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
90	[number]	k4	90
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
===	===	k?	===
</s>
</p>
<p>
<s>
Innuendo	Innuendo	k1gNnSc1	Innuendo
(	(	kIx(	(
<g/>
Mercury	Mercura	k1gFnPc1	Mercura
<g/>
,	,	kIx,	,
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
The	The	k?	The
Show	show	k1gNnSc1	show
Must	Must	k1gMnSc1	Must
Go	Go	k1gMnSc1	Go
On	on	k3xPp3gMnSc1	on
(	(	kIx(	(
<g/>
May	May	k1gMnSc1	May
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Heaven	Heavna	k1gFnPc2	Heavna
for	forum	k1gNnPc2	forum
Everyone	Everyon	k1gInSc5	Everyon
(	(	kIx(	(
<g/>
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Too	Too	k?	Too
Much	moucha	k1gFnPc2	moucha
Love	lov	k1gInSc5	lov
Will	Willa	k1gFnPc2	Willa
Kill	Kill	k1gMnSc1	Kill
You	You	k1gMnSc1	You
(	(	kIx(	(
<g/>
May	May	k1gMnSc1	May
<g/>
,	,	kIx,	,
Frank	Frank	k1gMnSc1	Frank
Musker	Musker	k1gMnSc1	Musker
<g/>
,	,	kIx,	,
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
Lamers	Lamersa	k1gFnPc2	Lamersa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
These	these	k1gFnSc1	these
Are	ar	k1gInSc5	ar
the	the	k?	the
Days	Daysa	k1gFnPc2	Daysa
of	of	k?	of
Our	Our	k1gMnSc1	Our
Lives	Lives	k1gMnSc1	Lives
(	(	kIx(	(
<g/>
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Too	Too	k?	Too
much	moucha	k1gFnPc2	moucha
love	lov	k1gInSc5	lov
will	willum	k1gNnPc2	willum
kill	killum	k1gNnPc2	killum
you	you	k?	you
(	(	kIx(	(
<g/>
May	May	k1gMnSc1	May
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
I	i	k9	i
<g/>
́	́	k?	́
<g/>
m	m	kA	m
going	goinga	k1gFnPc2	goinga
slightly	slightnout	k5eAaPmAgFnP	slightnout
mad	mad	k?	mad
(	(	kIx(	(
<g/>
Merury	Merura	k1gFnSc2	Merura
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
==	==	k?	==
</s>
</p>
<p>
<s>
Současní	současný	k2eAgMnPc1d1	současný
členovéBrian	členovéBrian	k1gMnSc1	členovéBrian
May	May	k1gMnSc1	May
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
klávesy	kláves	k1gInPc1	kláves
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Roger	Roger	k1gMnSc1	Roger
Taylor	Taylor	k1gMnSc1	Taylor
–	–	k?	–
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
perkuse	perkuse	k1gFnPc1	perkuse
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
tamburína	tamburína	k1gFnSc1	tamburína
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Adam	Adam	k1gMnSc1	Adam
Lambert	Lambert	k1gMnSc1	Lambert
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
<g/>
Bývalí	bývalý	k2eAgMnPc1d1	bývalý
členovéFreddie	členovéFreddie	k1gFnSc1	členovéFreddie
Mercury	Mercur	k1gInPc1	Mercur
–	–	k?	–
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
piáno	piáno	k?	piáno
<g/>
,	,	kIx,	,
doprovodná	doprovodný	k2eAgFnSc1d1	doprovodná
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
tamburína	tamburína	k1gFnSc1	tamburína
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Deacon	Deacon	k1gMnSc1	Deacon
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
rytmická	rytmický	k2eAgFnSc1d1	rytmická
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
klávesy	kláves	k1gInPc1	kláves
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
triangl	triangl	k1gInSc1	triangl
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Paul	Paul	k1gMnSc1	Paul
Rodgers	Rodgers	k1gInSc1	Rodgers
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
Členové	člen	k1gMnPc1	člen
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
činnostiMike	činnostiMik	k1gFnSc2	činnostiMik
Grose	gros	k1gInSc5	gros
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Barry	Barra	k1gFnPc1	Barra
Mitchell	Mitchell	k1gInSc1	Mitchell
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
–	–	k?	–
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Doug	Doug	k1gMnSc1	Doug
Bogie	Bogie	k1gFnSc2	Bogie
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
Členové	člen	k1gMnPc1	člen
na	na	k7c6	na
turné	turné	k1gNnSc6	turné
</s>
</p>
<p>
<s>
Spike	Spike	k1gFnSc1	Spike
Edney	Ednea	k1gFnSc2	Ednea
–	–	k?	–
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Neil	Neil	k1gMnSc1	Neil
Fairclough	Fairclough	k1gMnSc1	Fairclough
–	–	k?	–
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tyler	Tyler	k1gMnSc1	Tyler
Warren	Warrna	k1gFnPc2	Warrna
–	–	k?	–
perkuse	perkuse	k1gFnPc1	perkuse
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
–	–	k?	–
<g/>
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
<g/>
Časový	časový	k2eAgInSc1d1	časový
přehled	přehled	k1gInSc1	přehled
</s>
</p>
<p>
<s>
==	==	k?	==
Queen	Quena	k1gFnPc2	Quena
během	během	k7c2	během
let	léto	k1gNnPc2	léto
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Smile	smil	k1gInSc5	smil
===	===	k?	===
</s>
</p>
<p>
<s>
Začínající	začínající	k2eAgFnSc1d1	začínající
kapela	kapela	k1gFnSc1	kapela
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
po	po	k7c6	po
menší	malý	k2eAgFnSc3d2	menší
výměně	výměna	k1gFnSc3	výměna
členů	člen	k1gInPc2	člen
stala	stát	k5eAaPmAgFnS	stát
Queen	Queen	k1gInSc4	Queen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Aktivní	aktivní	k2eAgInPc4d1	aktivní
roky	rok	k1gInPc4	rok
====	====	k?	====
</s>
</p>
<p>
<s>
Kapela	kapela	k1gFnSc1	kapela
začala	začít	k5eAaPmAgFnS	začít
jsou	být	k5eAaImIp3nP	být
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
:	:	kIx,	:
1968	[number]	k4	1968
</s>
</p>
<p>
<s>
Kapela	kapela	k1gFnSc1	kapela
ukončila	ukončit	k5eAaPmAgFnS	ukončit
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
:	:	kIx,	:
1970	[number]	k4	1970
</s>
</p>
<p>
<s>
====	====	k?	====
Členové	člen	k1gMnPc1	člen
====	====	k?	====
</s>
</p>
<p>
<s>
Tim	Tim	k?	Tim
Staffel	Staffel	k1gInSc1	Staffel
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgInPc1d1	hlavní
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
<g/>
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Brian	Brian	k1gMnSc1	Brian
May	May	k1gMnSc1	May
(	(	kIx(	(
<g/>
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Roger	Roger	k1gMnSc1	Roger
Taylor	Taylor	k1gMnSc1	Taylor
(	(	kIx(	(
<g/>
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
bicí	bicí	k2eAgInPc1d1	bicí
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Queen	Queno	k1gNnPc2	Queno
===	===	k?	===
</s>
</p>
<p>
<s>
Legendární	legendární	k2eAgNnSc4d1	legendární
a	a	k8xC	a
originální	originální	k2eAgNnSc4d1	originální
složení	složení	k1gNnSc4	složení
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
frontmanem	frontman	k1gMnSc7	frontman
Freddiem	Freddium	k1gNnSc7	Freddium
Mercurym	Mercurym	k1gInSc4	Mercurym
</s>
</p>
<p>
<s>
====	====	k?	====
Aktivní	aktivní	k2eAgInPc4d1	aktivní
roky	rok	k1gInPc4	rok
====	====	k?	====
</s>
</p>
<p>
<s>
Kapela	kapela	k1gFnSc1	kapela
začala	začít	k5eAaPmAgFnS	začít
jsou	být	k5eAaImIp3nP	být
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
:	:	kIx,	:
1971	[number]	k4	1971
</s>
</p>
<p>
<s>
Kapela	kapela	k1gFnSc1	kapela
ukončila	ukončit	k5eAaPmAgFnS	ukončit
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
:	:	kIx,	:
1991	[number]	k4	1991
</s>
</p>
<p>
<s>
====	====	k?	====
Členové	člen	k1gMnPc1	člen
====	====	k?	====
</s>
</p>
<p>
<s>
Freddie	Freddie	k1gFnSc1	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgInPc1d1	hlavní
vokály	vokál	k1gInPc1	vokál
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Brian	Brian	k1gMnSc1	Brian
May	May	k1gMnSc1	May
(	(	kIx(	(
<g/>
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Roger	Roger	k1gMnSc1	Roger
Taylor	Taylor	k1gMnSc1	Taylor
(	(	kIx(	(
<g/>
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
bicí	bicí	k2eAgInPc1d1	bicí
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Deacon	Deacon	k1gMnSc1	Deacon
(	(	kIx(	(
<g/>
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Queen	Queen	k2eAgMnSc1d1	Queen
+	+	kIx~	+
Paul	Paul	k1gMnSc1	Paul
Rodgers	Rodgersa	k1gFnPc2	Rodgersa
===	===	k?	===
</s>
</p>
<p>
<s>
Úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
skupiny	skupina	k1gFnSc2	skupina
s	s	k7c7	s
novým	nový	k2eAgMnSc7d1	nový
zpěvákem	zpěvák	k1gMnSc7	zpěvák
a	a	k8xC	a
baskytarystou	baskytarysta	k1gMnSc7	baskytarysta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Aktivní	aktivní	k2eAgInPc4d1	aktivní
roky	rok	k1gInPc4	rok
====	====	k?	====
</s>
</p>
<p>
<s>
Kapela	kapela	k1gFnSc1	kapela
začala	začít	k5eAaPmAgFnS	začít
jsou	být	k5eAaImIp3nP	být
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
:	:	kIx,	:
2004	[number]	k4	2004
</s>
</p>
<p>
<s>
Kapela	kapela	k1gFnSc1	kapela
ukončila	ukončit	k5eAaPmAgFnS	ukončit
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
:	:	kIx,	:
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
====	====	k?	====
Členové	člen	k1gMnPc1	člen
====	====	k?	====
</s>
</p>
<p>
<s>
Paul	Paul	k1gMnSc1	Paul
Rodgers	Rodgersa	k1gFnPc2	Rodgersa
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgInPc1d1	hlavní
vokály	vokál	k1gInPc1	vokál
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Brian	Brian	k1gMnSc1	Brian
May	May	k1gMnSc1	May
(	(	kIx(	(
<g/>
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
'	'	kIx"	'
</s>
</p>
<p>
<s>
Roger	Roger	k1gMnSc1	Roger
Taylor	Taylor	k1gMnSc1	Taylor
(	(	kIx(	(
<g/>
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
bicí	bicí	k2eAgInPc1d1	bicí
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Spike	Spike	k1gFnSc1	Spike
Edney	Ednea	k1gFnSc2	Ednea
(	(	kIx(	(
<g/>
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Queen	Queen	k2eAgMnSc1d1	Queen
+	+	kIx~	+
Adam	Adam	k1gMnSc1	Adam
Lambert	Lambert	k1gMnSc1	Lambert
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
Paulem	Paul	k1gMnSc7	Paul
Rodgersem	Rodgers	k1gMnSc7	Rodgers
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
Adamem	Adam	k1gMnSc7	Adam
Lambertem	Lambert	k1gMnSc7	Lambert
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Aktivní	aktivní	k2eAgInPc4d1	aktivní
roky	rok	k1gInPc4	rok
====	====	k?	====
</s>
</p>
<p>
<s>
Kapela	kapela	k1gFnSc1	kapela
začala	začít	k5eAaPmAgFnS	začít
jsou	být	k5eAaImIp3nP	být
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
:	:	kIx,	:
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
Kapela	kapela	k1gFnSc1	kapela
zatím	zatím	k6eAd1	zatím
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
NEUKONČILA	ukončit	k5eNaPmAgFnS	ukončit
</s>
</p>
<p>
<s>
====	====	k?	====
Členové	člen	k1gMnPc1	člen
====	====	k?	====
</s>
</p>
<p>
<s>
Adam	Adam	k1gMnSc1	Adam
Lambert	Lambert	k1gMnSc1	Lambert
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgInPc1d1	hlavní
vokály	vokál	k1gInPc1	vokál
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Brian	Brian	k1gMnSc1	Brian
May	May	k1gMnSc1	May
(	(	kIx(	(
<g/>
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Roger	Roger	k1gMnSc1	Roger
Taylor	Taylor	k1gMnSc1	Taylor
(	(	kIx(	(
<g/>
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
bicí	bicí	k2eAgInPc1d1	bicí
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Spike	Spike	k1gFnSc1	Spike
Edney	Ednea	k1gFnSc2	Ednea
(	(	kIx(	(
<g/>
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Diskografie	diskografie	k1gFnSc2	diskografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Studiová	studiový	k2eAgNnPc4d1	studiové
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
1973	[number]	k4	1973
<g/>
:	:	kIx,	:
Queen	Queen	k1gInSc1	Queen
</s>
</p>
<p>
<s>
1974	[number]	k4	1974
<g/>
:	:	kIx,	:
Queen	Queen	k1gInSc1	Queen
II	II	kA	II
</s>
</p>
<p>
<s>
1974	[number]	k4	1974
<g/>
:	:	kIx,	:
Sheer	Shera	k1gFnPc2	Shera
Heart	Heart	k1gInSc1	Heart
Attack	Attack	k1gMnSc1	Attack
</s>
</p>
<p>
<s>
1975	[number]	k4	1975
<g/>
:	:	kIx,	:
A	a	k8xC	a
Night	Night	k1gInSc1	Night
at	at	k?	at
the	the	k?	the
Opera	opera	k1gFnSc1	opera
</s>
</p>
<p>
<s>
1976	[number]	k4	1976
<g/>
:	:	kIx,	:
A	a	k8xC	a
Day	Day	k1gFnSc1	Day
at	at	k?	at
the	the	k?	the
Races	Races	k1gInSc1	Races
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
<g/>
:	:	kIx,	:
News	News	k1gInSc1	News
of	of	k?	of
the	the	k?	the
World	World	k1gInSc1	World
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
<g/>
:	:	kIx,	:
Jazz	jazz	k1gInSc1	jazz
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Game	game	k1gInSc1	game
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
<g/>
:	:	kIx,	:
Flash	Flash	k1gInSc1	Flash
Gordon	Gordon	k1gMnSc1	Gordon
soundtrack	soundtrack	k1gInSc1	soundtrack
ke	k	k7c3	k
stejnojmennému	stejnojmenný	k2eAgNnSc3d1	stejnojmenné
sci-fi	scii	k1gNnSc3	sci-fi
filmu	film	k1gInSc2	film
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
<g/>
:	:	kIx,	:
Hot	hot	k0	hot
Space	Space	k1gMnSc5	Space
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
<g/>
:	:	kIx,	:
The	The	k1gFnSc2	The
Works	Works	kA	Works
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
<g/>
:	:	kIx,	:
A	a	k8xC	a
Kind	Kind	k1gMnSc1	Kind
of	of	k?	of
Magic	Magic	k1gMnSc1	Magic
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
6	[number]	k4	6
písní	píseň	k1gFnPc2	píseň
pro	pro	k7c4	pro
film	film	k1gInSc4	film
Highlander	Highlandra	k1gFnPc2	Highlandra
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
<g/>
:	:	kIx,	:
The	The	k1gFnSc2	The
Miracle	Miracle	k1gFnSc2	Miracle
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
<g/>
:	:	kIx,	:
Innuendo	Innuendo	k6eAd1	Innuendo
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
<g/>
:	:	kIx,	:
Made	Mad	k1gInSc2	Mad
in	in	k?	in
Heaven	Heaven	k2eAgInSc1d1	Heaven
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Cosmos	Cosmos	k1gMnSc1	Cosmos
Rocks	Rocks	k1gInSc1	Rocks
(	(	kIx(	(
<g/>
Queen	Queen	k1gInSc1	Queen
<g/>
+	+	kIx~	+
<g/>
Paul	Paul	k1gMnSc1	Paul
Rodgers	Rodgersa	k1gFnPc2	Rodgersa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Živá	živý	k2eAgNnPc4d1	živé
alba	album	k1gNnPc4	album
===	===	k?	===
</s>
</p>
<p>
<s>
1979	[number]	k4	1979
<g/>
:	:	kIx,	:
Live	Liv	k1gInSc2	Liv
Killers	Killers	k1gInSc1	Killers
útržky	útržka	k1gFnSc2	útržka
z	z	k7c2	z
turné	turné	k1gNnSc2	turné
z	z	k7c2	z
r.	r.	kA	r.
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
reedice	reedice	k1gFnSc1	reedice
2003	[number]	k4	2003
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
<g/>
:	:	kIx,	:
Live	Liv	k1gInSc2	Liv
Magic	Magice	k1gInPc2	Magice
útržky	útržka	k1gFnSc2	útržka
z	z	k7c2	z
turné	turné	k1gNnSc2	turné
Magic	Magic	k1gMnSc1	Magic
Tour	Tour	k1gMnSc1	Tour
1986	[number]	k4	1986
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
<g/>
:	:	kIx,	:
At	At	k1gMnSc1	At
the	the	k?	the
Beeb	Beeb	k1gMnSc1	Beeb
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
<g/>
:	:	kIx,	:
Live	Liv	k1gInSc2	Liv
at	at	k?	at
Wembley	Wemblea	k1gMnSc2	Wemblea
'	'	kIx"	'
<g/>
86	[number]	k4	86
koncert	koncert	k1gInSc1	koncert
z	z	k7c2	z
Wembley	Wemblea	k1gFnSc2	Wemblea
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
reedice	reedice	k1gFnSc1	reedice
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
opětovná	opětovný	k2eAgFnSc1d1	opětovná
reedice	reedice	k1gFnSc1	reedice
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
Queen	Queen	k2eAgInSc1d1	Queen
On	on	k3xPp3gInSc1	on
Fire	Fire	k1gInSc1	Fire
-	-	kIx~	-
Live	Live	k1gInSc1	Live
at	at	k?	at
the	the	k?	the
Bowl	Bowlum	k1gNnPc2	Bowlum
koncert	koncert	k1gInSc1	koncert
z	z	k7c2	z
Bowlu	Bowl	k1gInSc2	Bowl
1982	[number]	k4	1982
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
Queen	Queen	k2eAgInSc1d1	Queen
Rock	rock	k1gInSc1	rock
Montreal	Montreal	k1gInSc1	Montreal
koncert	koncert	k1gInSc1	koncert
z	z	k7c2	z
Montrealu	Montreal	k1gInSc2	Montreal
1981	[number]	k4	1981
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
<g/>
:	:	kIx,	:
Hungarian	Hungarian	k1gInSc1	Hungarian
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
<g/>
:	:	kIx,	:
Queen	Queen	k2eAgInSc1d1	Queen
Live	Live	k1gInSc1	Live
in	in	k?	in
Budapest	Budapest	k1gFnSc1	Budapest
koncert	koncert	k1gInSc1	koncert
z	z	k7c2	z
Budapešti	Budapešť	k1gFnSc2	Budapešť
1986	[number]	k4	1986
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Live	Liv	k1gInSc2	Liv
at	at	k?	at
the	the	k?	the
Rainbow	Rainbow	k1gFnSc2	Rainbow
'	'	kIx"	'
<g/>
74	[number]	k4	74
koncert	koncert	k1gInSc1	koncert
z	z	k7c2	z
Rainbow	Rainbow	k1gFnSc2	Rainbow
Theatre	Theatr	k1gMnSc5	Theatr
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
Queen	Quena	k1gFnPc2	Quena
<g/>
:	:	kIx,	:
A	a	k9	a
Night	Night	k2eAgMnSc1d1	Night
At	At	k1gMnSc1	At
The	The	k1gFnSc2	The
Odeon	odeon	k1gInSc1	odeon
-	-	kIx~	-
Hammersmith	Hammersmith	k1gInSc1	Hammersmith
1975	[number]	k4	1975
vánoční	vánoční	k2eAgInSc4d1	vánoční
koncert	koncert	k1gInSc4	koncert
z	z	k7c2	z
Hammersmith	Hammersmitha	k1gFnPc2	Hammersmitha
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
</s>
</p>
<p>
<s>
2016	[number]	k4	2016
<g/>
:	:	kIx,	:
On	on	k3xPp3gMnSc1	on
Air	Air	k1gMnSc1	Air
</s>
</p>
<p>
<s>
===	===	k?	===
Kompilace	kompilace	k1gFnSc2	kompilace
===	===	k?	===
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
<g/>
:	:	kIx,	:
Greatest	Greatest	k1gFnSc1	Greatest
Hits	Hits	k1gInSc1	Hits
výběr	výběr	k1gInSc1	výběr
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
1980	[number]	k4	1980
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
<g/>
:	:	kIx,	:
At	At	k1gFnSc1	At
the	the	k?	the
Beeb	Beeba	k1gFnPc2	Beeba
BBC	BBC	kA	BBC
nahrávky	nahrávka	k1gFnPc1	nahrávka
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
<g/>
:	:	kIx,	:
Greatest	Greatest	k1gFnSc1	Greatest
Hits	Hits	k1gInSc1	Hits
II	II	kA	II
výběr	výběr	k1gInSc1	výběr
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
<g/>
:	:	kIx,	:
Classic	Classice	k1gFnPc2	Classice
Queen	Quena	k1gFnPc2	Quena
americká	americký	k2eAgFnSc1d1	americká
obdoba	obdoba	k1gFnSc1	obdoba
Greatest	Greatest	k1gInSc4	Greatest
Hits	Hits	k1gInSc1	Hits
II	II	kA	II
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
12	[number]	k4	12
<g/>
''	''	k?	''
Collection	Collection	k1gInSc1	Collection
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
<g/>
:	:	kIx,	:
Queen	Queen	k1gInSc1	Queen
Rocks	Rocksa	k1gFnPc2	Rocksa
výběr	výběr	k1gInSc4	výběr
z	z	k7c2	z
rockových	rockový	k2eAgFnPc2d1	rocková
nahrávek	nahrávka	k1gFnPc2	nahrávka
Queen	Quena	k1gFnPc2	Quena
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc4d1	obsahující
jednu	jeden	k4xCgFnSc4	jeden
nahrávku	nahrávka	k1gFnSc4	nahrávka
novou	nový	k2eAgFnSc4d1	nová
bez	bez	k7c2	bez
Freddieho	Freddie	k1gMnSc4	Freddie
Mercuryho	Mercury	k1gMnSc2	Mercury
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
<g/>
:	:	kIx,	:
Greatest	Greatest	k1gFnSc1	Greatest
Hits	Hits	k1gInSc1	Hits
III	III	kA	III
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
sólové	sólový	k2eAgFnPc4d1	sólová
nahrávky	nahrávka	k1gFnPc4	nahrávka
členů	člen	k1gMnPc2	člen
skupiny	skupina	k1gFnSc2	skupina
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
Stone	ston	k1gInSc5	ston
Cold	Colda	k1gFnPc2	Colda
Classics	Classicsa	k1gFnPc2	Classicsa
americká	americký	k2eAgFnSc1d1	americká
obdoba	obdoba	k1gFnSc1	obdoba
Queen	Quena	k1gFnPc2	Quena
Rocks	Rocksa	k1gFnPc2	Rocksa
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
The	The	k1gFnPc2	The
A-Z	A-Z	k1gMnPc2	A-Z
of	of	k?	of
Queen	Queno	k1gNnPc2	Queno
<g/>
,	,	kIx,	,
Volume	volum	k1gInSc5	volum
1	[number]	k4	1
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Absolute	Absolut	k1gMnSc5	Absolut
Greatest	Greatest	k1gMnSc1	Greatest
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
Deep	Deep	k1gInSc1	Deep
Cuts	Cutsa	k1gFnPc2	Cutsa
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
1	[number]	k4	1
výběr	výběr	k1gInSc4	výběr
z	z	k7c2	z
méně	málo	k6eAd2	málo
slavných	slavný	k2eAgFnPc2d1	slavná
nahrávek	nahrávka	k1gFnPc2	nahrávka
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
Deep	Deep	k1gInSc1	Deep
Cuts	Cutsa	k1gFnPc2	Cutsa
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
2	[number]	k4	2
výběr	výběr	k1gInSc4	výběr
z	z	k7c2	z
méně	málo	k6eAd2	málo
slavných	slavný	k2eAgFnPc2d1	slavná
nahrávek	nahrávka	k1gFnPc2	nahrávka
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1977	[number]	k4	1977
<g/>
–	–	k?	–
<g/>
1982	[number]	k4	1982
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
Deep	Deep	k1gInSc1	Deep
Cuts	Cutsa	k1gFnPc2	Cutsa
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
3	[number]	k4	3
výběr	výběr	k1gInSc4	výběr
z	z	k7c2	z
méně	málo	k6eAd2	málo
slavných	slavný	k2eAgFnPc2d1	slavná
nahrávek	nahrávka	k1gFnPc2	nahrávka
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1984	[number]	k4	1984
<g/>
–	–	k?	–
<g/>
1995	[number]	k4	1995
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
Icon	Icon	k1gInSc1	Icon
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
<g/>
:	:	kIx,	:
Queen	Queen	k2eAgInSc1d1	Queen
Forever	Forever	k1gInSc1	Forever
</s>
</p>
<p>
<s>
===	===	k?	===
VHS	VHS	kA	VHS
/	/	kIx~	/
DVD	DVD	kA	DVD
===	===	k?	===
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
<g/>
:	:	kIx,	:
Greatest	Greatest	k1gFnSc1	Greatest
Flix	Flix	k1gInSc1	Flix
výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
videoklipů	videoklip	k1gInPc2	videoklip
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1975	[number]	k4	1975
<g/>
–	–	k?	–
<g/>
1980	[number]	k4	1980
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
<g/>
:	:	kIx,	:
We	We	k1gFnSc6	We
Will	Willa	k1gFnPc2	Willa
Rock	rock	k1gInSc1	rock
You	You	k1gMnSc1	You
koncert	koncert	k1gInSc1	koncert
z	z	k7c2	z
Montrealu	Montreal	k1gInSc2	Montreal
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
reedice	reedice	k1gFnSc1	reedice
na	na	k7c4	na
DVD	DVD	kA	DVD
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Queen	Queen	k2eAgInSc1d1	Queen
Rock	rock	k1gInSc1	rock
Montreal	Montreal	k1gInSc1	Montreal
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
<g/>
:	:	kIx,	:
The	The	k1gFnSc2	The
Works	Works	kA	Works
EP	EP	kA	EP
videoklipy	videoklip	k1gInPc1	videoklip
z	z	k7c2	z
alba	album	k1gNnSc2	album
The	The	k1gFnSc2	The
Works	Works	kA	Works
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
<g/>
:	:	kIx,	:
Live	Liv	k1gInSc2	Liv
In	In	k1gFnSc1	In
Rio	Rio	k1gFnSc1	Rio
koncert	koncert	k1gInSc4	koncert
z	z	k7c2	z
Ria	Ria	k1gFnSc2	Ria
1985	[number]	k4	1985
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
<g/>
:	:	kIx,	:
Live	Liv	k1gInSc2	Liv
In	In	k1gFnSc1	In
Budapest	Budapest	k1gFnSc1	Budapest
koncert	koncert	k1gInSc4	koncert
z	z	k7c2	z
Budapešti	Budapešť	k1gFnSc2	Budapešť
1986	[number]	k4	1986
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
<g/>
:	:	kIx,	:
Magic	Magice	k1gFnPc2	Magice
Years	Yearsa	k1gFnPc2	Yearsa
trojdílný	trojdílný	k2eAgInSc4d1	trojdílný
dokument	dokument	k1gInSc4	dokument
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
<g/>
:	:	kIx,	:
Rare	Rar	k1gInSc2	Rar
Live	Liv	k1gFnSc2	Liv
ukázky	ukázka	k1gFnPc1	ukázka
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
live	live	k1gFnPc2	live
záznamů	záznam	k1gInPc2	záznam
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1974	[number]	k4	1974
<g/>
–	–	k?	–
<g/>
1986	[number]	k4	1986
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Miracle	Miracle	k1gInSc1	Miracle
EP	EP	kA	EP
videoklipy	videoklip	k1gInPc1	videoklip
z	z	k7c2	z
alba	album	k1gNnSc2	album
The	The	k1gMnSc2	The
Miracle	Miracl	k1gMnSc2	Miracl
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
<g/>
:	:	kIx,	:
Live	Liv	k1gInSc2	Liv
at	at	k?	at
Wembley	Wemblea	k1gMnSc2	Wemblea
'	'	kIx"	'
<g/>
86	[number]	k4	86
koncert	koncert	k1gInSc1	koncert
z	z	k7c2	z
Wembley	Wemblea	k1gFnSc2	Wemblea
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
v	v	k7c6	v
r.	r.	kA	r.
2003	[number]	k4	2003
reedice	reedice	k1gFnSc2	reedice
na	na	k7c4	na
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
v	v	k7c6	v
r.	r.	kA	r.
2011	[number]	k4	2011
opětovná	opětovný	k2eAgFnSc1d1	opětovná
reedice	reedice	k1gFnSc1	reedice
s	s	k7c7	s
bonusy	bonus	k1gInPc7	bonus
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
<g/>
:	:	kIx,	:
Greatest	Greatest	k1gFnSc1	Greatest
Flix	Flix	k1gInSc1	Flix
II	II	kA	II
výběr	výběr	k1gInSc1	výběr
z	z	k7c2	z
videoklipů	videoklip	k1gInPc2	videoklip
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
<g/>
:	:	kIx,	:
Box	box	k1gInSc4	box
Of	Of	k1gFnSc2	Of
Flix	Flix	k1gInSc1	Flix
Box	box	k1gInSc1	box
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Greatest	Greatest	k1gFnSc4	Greatest
Flix	Flix	k1gInSc1	Flix
I	I	kA	I
+	+	kIx~	+
II	II	kA	II
+	+	kIx~	+
4	[number]	k4	4
klipy	klip	k1gInPc7	klip
navíc	navíc	k6eAd1	navíc
+	+	kIx~	+
koncert	koncert	k1gInSc1	koncert
z	z	k7c2	z
Rainbow	Rainbow	k1gFnSc2	Rainbow
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
<g/>
:	:	kIx,	:
Champions	Champions	k1gInSc1	Champions
Of	Of	k1gMnSc1	Of
The	The	k1gMnSc1	The
World	World	k1gMnSc1	World
dokument	dokument	k1gInSc4	dokument
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
<g/>
:	:	kIx,	:
Made	Mad	k1gInSc2	Mad
In	In	k1gFnSc2	In
Heaven	Heavna	k1gFnPc2	Heavna
-	-	kIx~	-
The	The	k1gMnPc1	The
Films	Filmsa	k1gFnPc2	Filmsa
videoklipy	videoklip	k1gInPc4	videoklip
k	k	k7c3	k
albu	album	k1gNnSc3	album
Made	Mad	k1gInSc2	Mad
in	in	k?	in
Heaven	Heavna	k1gFnPc2	Heavna
<g/>
,	,	kIx,	,
v	v	k7c6	v
r.	r.	kA	r.
2003	[number]	k4	2003
reedice	reedice	k1gFnSc2	reedice
na	na	k7c4	na
DVD	DVD	kA	DVD
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
<g/>
:	:	kIx,	:
Queen	Queen	k2eAgInSc1d1	Queen
Rocks	Rocks	k1gInSc1	Rocks
videoklipy	videoklip	k1gInPc1	videoklip
ke	k	k7c3	k
stejnojmenné	stejnojmenný	k2eAgFnSc3d1	stejnojmenná
CD	CD	kA	CD
kompilaci	kompilace	k1gFnSc3	kompilace
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
<g/>
:	:	kIx,	:
Greatest	Greatest	k1gFnSc1	Greatest
Flix	Flix	k1gInSc1	Flix
III	III	kA	III
videoklipy	videoklip	k1gInPc4	videoklip
ke	k	k7c3	k
stejnojmenné	stejnojmenný	k2eAgFnSc3d1	stejnojmenná
CD	CD	kA	CD
kompilaci	kompilace	k1gFnSc3	kompilace
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
Greatest	Greatest	k1gInSc1	Greatest
Video	video	k1gNnSc1	video
Hits	Hits	k1gInSc1	Hits
videoklipy	videoklip	k1gInPc1	videoklip
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1973	[number]	k4	1973
<g/>
–	–	k?	–
<g/>
1980	[number]	k4	1980
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
<g/>
:	:	kIx,	:
Greatest	Greatest	k1gInSc1	Greatest
Video	video	k1gNnSc1	video
Hits	Hitsa	k1gFnPc2	Hitsa
II	II	kA	II
videoklipy	videoklip	k1gInPc1	videoklip
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
Queen	Queen	k1gInSc1	Queen
on	on	k3xPp3gMnSc1	on
Fire	Fire	k1gFnPc3	Fire
koncert	koncert	k1gInSc4	koncert
z	z	k7c2	z
Bowlu	Bowl	k1gInSc2	Bowl
1982	[number]	k4	1982
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
Return	Return	k1gInSc1	Return
of	of	k?	of
the	the	k?	the
Champions	Championsa	k1gFnPc2	Championsa
záznam	záznam	k1gInSc1	záznam
koncertu	koncert	k1gInSc2	koncert
Queen	Queen	k2eAgMnSc1d1	Queen
s	s	k7c7	s
Paulem	Paul	k1gMnSc7	Paul
Rodgersem	Rodgers	k1gMnSc7	Rodgers
v	v	k7c6	v
Sheffieldu	Sheffield	k1gInSc6	Sheffield
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
<g/>
:	:	kIx,	:
Queen	Queen	k2eAgInSc1d1	Queen
Rock	rock	k1gInSc1	rock
Montreal	Montreal	k1gInSc1	Montreal
koncert	koncert	k1gInSc1	koncert
z	z	k7c2	z
Montrealu	Montreal	k1gInSc2	Montreal
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
DVD	DVD	kA	DVD
vydání	vydání	k1gNnSc4	vydání
záznamu	záznam	k1gInSc2	záznam
We	We	k1gFnSc2	We
Will	Will	k1gInSc1	Will
Rock	rock	k1gInSc1	rock
You	You	k1gFnSc1	You
<g/>
,	,	kIx,	,
limitovaná	limitovaný	k2eAgFnSc1d1	limitovaná
edice	edice	k1gFnSc1	edice
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
vystoupení	vystoupení	k1gNnSc4	vystoupení
z	z	k7c2	z
Live	Liv	k1gInSc2	Liv
Aid	Aida	k1gFnPc2	Aida
1985	[number]	k4	1985
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Live	Livus	k1gMnSc5	Livus
In	In	k1gMnSc5	In
Ukraine	Ukrain	k1gMnSc5	Ukrain
záznam	záznam	k1gInSc1	záznam
koncertu	koncert	k1gInSc2	koncert
Queen	Queen	k2eAgMnSc1d1	Queen
s	s	k7c7	s
Paulem	Paul	k1gMnSc7	Paul
Rodgersem	Rodgers	k1gMnSc7	Rodgers
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
<g/>
:	:	kIx,	:
These	these	k1gFnSc1	these
Are	ar	k1gInSc5	ar
The	The	k1gMnSc1	The
Days	Daysa	k1gFnPc2	Daysa
Of	Of	k1gMnSc1	Of
Our	Our	k1gMnSc1	Our
Lives	Lives	k1gMnSc1	Lives
dokument	dokument	k1gInSc4	dokument
BBC	BBC	kA	BBC
ke	k	k7c3	k
40	[number]	k4	40
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
skupiny	skupina	k1gFnSc2	skupina
</s>
</p>
<p>
<s>
2017	[number]	k4	2017
<g/>
:	:	kIx,	:
Live	Live	k1gFnSc1	Live
In	In	k1gFnSc1	In
Japan	japan	k1gInSc4	japan
záznam	záznam	k1gInSc1	záznam
koncertu	koncert	k1gInSc2	koncert
Queen	Queen	k2eAgMnSc1d1	Queen
s	s	k7c7	s
Adamem	Adam	k1gMnSc7	Adam
Lambertem	Lambert	k1gMnSc7	Lambert
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
</s>
</p>
<p>
<s>
===	===	k?	===
Queen	Queen	k2eAgMnSc1d1	Queen
+	+	kIx~	+
Paul	Paul	k1gMnSc1	Paul
Rodgers	Rodgersa	k1gFnPc2	Rodgersa
===	===	k?	===
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
<g/>
:	:	kIx,	:
Return	Return	k1gInSc1	Return
of	of	k?	of
the	the	k?	the
Champions	Championsa	k1gFnPc2	Championsa
záznam	záznam	k1gInSc1	záznam
koncertu	koncert	k1gInSc2	koncert
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
<g/>
:	:	kIx,	:
Super	super	k1gInSc2	super
Live	Live	k1gFnSc1	Live
In	In	k1gFnSc1	In
Japan	japan	k1gInSc4	japan
záznam	záznam	k1gInSc1	záznam
koncertu	koncert	k1gInSc2	koncert
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
i	i	k9	i
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
CD	CD	kA	CD
The	The	k1gFnSc1	The
Cosmos	Cosmos	k1gMnSc1	Cosmos
Rocks	Rocks	k1gInSc1	Rocks
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Cosmos	Cosmos	k1gMnSc1	Cosmos
Rocks	Rocks	k1gInSc1	Rocks
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Live	Livus	k1gMnSc5	Livus
In	In	k1gMnSc5	In
Ukraine	Ukrain	k1gMnSc5	Ukrain
záznam	záznam	k1gInSc1	záznam
koncertu	koncert	k1gInSc2	koncert
(	(	kIx(	(
<g/>
CD	CD	kA	CD
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
DVD	DVD	kA	DVD
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Queen	Queen	k2eAgInSc4d1	Queen
(	(	kIx(	(
<g/>
band	band	k1gInSc4	band
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
DOHETHY	DOHETHY	kA	DOHETHY
<g/>
,	,	kIx,	,
Harry	Harr	k1gInPc7	Harr
<g/>
.	.	kIx.	.
</s>
<s>
Legenda	legenda	k1gFnSc1	legenda
Queen	Quena	k1gFnPc2	Quena
<g/>
,	,	kIx,	,
40	[number]	k4	40
let	léto	k1gNnPc2	léto
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
předmluva	předmluva	k1gFnSc1	předmluva
Brian	Brian	k1gMnSc1	Brian
May	May	k1gMnSc1	May
a	a	k8xC	a
Roger	Roger	k1gMnSc1	Roger
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
,	,	kIx,	,
Computer	computer	k1gInSc1	computer
Press	Press	k1gInSc1	Press
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
</s>
</p>
<p>
<s>
SKY	SKY	kA	SKY
<g/>
,	,	kIx,	,
Rick	Rick	k1gInSc1	Rick
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
Freddie	Freddie	k1gFnSc2	Freddie
Mercuryho	Mercury	k1gMnSc2	Mercury
<g/>
.	.	kIx.	.
</s>
<s>
Premiéra	premiéra	k1gFnSc1	premiéra
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
,	,	kIx,	,
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
</s>
</p>
<p>
<s>
WICH	WICH	kA	WICH
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Rock	rock	k1gInSc1	rock
&	&	k?	&
Pop	pop	k1gInSc1	pop
<g/>
:	:	kIx,	:
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
II	II	kA	II
(	(	kIx(	(
<g/>
M-Z	M-Z	k1gMnSc1	M-Z
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
207	[number]	k4	207
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DEAN	DEAN	kA	DEAN
<g/>
,	,	kIx,	,
Ken	Ken	k1gMnSc1	Ken
<g/>
.	.	kIx.	.
</s>
<s>
Queen	Queen	k1gInSc1	Queen
<g/>
:	:	kIx,	:
Nový	nový	k2eAgInSc1d1	nový
obrazový	obrazový	k2eAgInSc1d1	obrazový
dokument	dokument	k1gInSc1	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
Champagne	Champagn	k1gMnSc5	Champagn
Avantgarde	Avantgard	k1gMnSc5	Avantgard
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Leyles-Ann	Leyles-Ann	k1gMnSc1	Leyles-Ann
Jones	Jones	k1gMnSc1	Jones
<g/>
:	:	kIx,	:
Freddie	Freddie	k1gFnSc1	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
</s>
</p>
<p>
<s>
Peter	Peter	k1gMnSc1	Peter
Freeston	Freeston	k1gInSc1	Freeston
<g/>
:	:	kIx,	:
Freddie	Freddie	k1gFnSc1	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
</s>
</p>
<p>
<s>
Greg	Greg	k1gInSc1	Greg
Brooks	Brooks	k1gInSc1	Brooks
<g/>
:	:	kIx,	:
Queen	Queen	k2eAgInSc1d1	Queen
Live	Live	k1gInSc1	Live
</s>
</p>
<p>
<s>
Mick	Mick	k1gMnSc1	Mick
St.	st.	kA	st.
Michael	Michael	k1gMnSc1	Michael
<g/>
:	:	kIx,	:
Queen	Queen	k1gInSc1	Queen
ich	ich	k?	ich
vlastnými	vlastný	k2eAgFnPc7d1	vlastná
slovami	slova	k1gFnPc7	slova
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Queen	Quena	k1gFnPc2	Quena
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgFnSc1d1	lokální
šablona	šablona	k1gFnSc1	šablona
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
kategorii	kategorie	k1gFnSc4	kategorie
Commons	Commonsa	k1gFnPc2	Commonsa
než	než	k8xS	než
přiřazená	přiřazený	k2eAgFnSc1d1	přiřazená
položka	položka	k1gFnSc1	položka
Wikidat	Wikidat	k1gFnSc2	Wikidat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgInSc1d1	lokální
odkaz	odkaz	k1gInSc1	odkaz
<g/>
:	:	kIx,	:
Queen	Queen	k1gInSc1	Queen
</s>
</p>
<p>
<s>
Wikidata	Wikidata	k1gFnSc1	Wikidata
<g/>
:	:	kIx,	:
Queen	Queen	k1gInSc1	Queen
(	(	kIx(	(
<g/>
musical	musical	k1gInSc1	musical
group	group	k1gInSc1	group
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Queen	Queen	k1gInSc1	Queen
–	–	k?	–
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
</s>
</p>
<p>
<s>
Queen	Queen	k1gInSc1	Queen
<g/>
+	+	kIx~	+
<g/>
Paul	Paul	k1gMnSc1	Paul
Rodgers	Rodgersa	k1gFnPc2	Rodgersa
–	–	k?	–
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
</s>
</p>
<p>
<s>
Queen	Queen	k1gInSc1	Queen
Diskografie	diskografie	k1gFnSc2	diskografie
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
web	web	k1gInSc1	web
a	a	k8xC	a
fan	fana	k1gFnPc2	fana
club	club	k1gInSc4	club
Queen	Queen	k1gInSc1	Queen
</s>
</p>
<p>
<s>
Queen	Queen	k1gInSc1	Queen
<g/>
.	.	kIx.	.
<g/>
musichall	musichall	k1gInSc1	musichall
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
