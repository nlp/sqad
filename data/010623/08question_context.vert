<s>
Queen	Queen	k1gInSc1	Queen
je	být	k5eAaImIp3nS	být
britská	britský	k2eAgFnSc1d1	britská
rocková	rockový	k2eAgFnSc1d1	rocková
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
její	její	k3xOp3gFnSc6	její
původní	původní	k2eAgFnSc6d1	původní
sestavě	sestava	k1gFnSc6	sestava
byli	být	k5eAaImAgMnP	být
Freddie	Freddie	k1gFnPc4	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
(	(	kIx(	(
<g/>
vokály	vokál	k1gInPc1	vokál
<g/>
,	,	kIx,	,
klavír	klavír	k1gInSc1	klavír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Brian	Brian	k1gMnSc1	Brian
May	May	k1gMnSc1	May
(	(	kIx(	(
<g/>
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Deacon	Deacon	k1gMnSc1	Deacon
(	(	kIx(	(
<g/>
basová	basový	k2eAgFnSc1d1	basová
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
klávesy	kláves	k1gInPc1	kláves
<g/>
)	)	kIx)	)
a	a	k8xC	a
Roger	Roger	k1gMnSc1	Roger
Taylor	Taylor	k1gMnSc1	Taylor
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgFnPc1d1	bicí
<g/>
,	,	kIx,	,
vokály	vokál	k1gInPc1	vokál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>

