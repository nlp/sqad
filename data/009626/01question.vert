<s>
Je	být	k5eAaImIp3nS	být
Westminsterské	Westminsterský	k2eAgNnSc4d1	Westminsterské
opatství	opatství	k1gNnSc4	opatství
chrám	chrám	k1gInSc1	chrám
Anglické	anglický	k2eAgFnSc2d1	anglická
církve	církev	k1gFnSc2	církev
ve	v	k7c6	v
stavebním	stavební	k2eAgInSc6d1	stavební
tvaru	tvar	k1gInSc6	tvar
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgMnSc1d1	postavený
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
gotickém	gotický	k2eAgInSc6d1	gotický
slohu	sloh	k1gInSc6	sloh
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
Londýnském	londýnský	k2eAgInSc6d1	londýnský
obvodu	obvod	k1gInSc6	obvod
Westminster	Westminstra	k1gFnPc2	Westminstra
<g/>
,	,	kIx,	,
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
Westminsterského	Westminsterský	k2eAgInSc2d1	Westminsterský
paláce	palác	k1gInSc2	palác
<g/>
?	?	kIx.	?
</s>
