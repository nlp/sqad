<p>
<s>
Westminsterské	Westminsterský	k2eAgNnSc1d1	Westminsterské
opatství	opatství	k1gNnSc1	opatství
(	(	kIx(	(
<g/>
Westminster	Westminster	k1gInSc1	Westminster
Abbey	Abbea	k1gFnSc2	Abbea
<g/>
)	)	kIx)	)
–	–	k?	–
oficiální	oficiální	k2eAgNnSc4d1	oficiální
označení	označení	k1gNnSc4	označení
Kolegiátní	kolegiátní	k2eAgInSc1d1	kolegiátní
chrám	chrám	k1gInSc1	chrám
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
ve	v	k7c6	v
Westminsteru	Westminster	k1gInSc6	Westminster
(	(	kIx(	(
<g/>
The	The	k1gMnSc5	The
Collegiate	Collegiat	k1gMnSc5	Collegiat
Church	Church	k1gMnSc1	Church
of	of	k?	of
St	St	kA	St
Peter	Peter	k1gMnSc1	Peter
<g/>
,	,	kIx,	,
Westminster	Westminster	k1gMnSc1	Westminster
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
chrám	chrám	k1gInSc1	chrám
Anglické	anglický	k2eAgFnSc2d1	anglická
církve	církev	k1gFnSc2	církev
ve	v	k7c6	v
stavebním	stavební	k2eAgInSc6d1	stavební
tvaru	tvar	k1gInSc6	tvar
katedrály	katedrála	k1gFnSc2	katedrála
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgMnSc1d1	postavený
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
gotickém	gotický	k2eAgInSc6d1	gotický
slohu	sloh	k1gInSc6	sloh
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
Londýnském	londýnský	k2eAgInSc6d1	londýnský
obvodu	obvod	k1gInSc6	obvod
Westminster	Westminstra	k1gFnPc2	Westminstra
<g/>
,	,	kIx,	,
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
Westminsterského	Westminsterský	k2eAgInSc2d1	Westminsterský
paláce	palác	k1gInSc2	palác
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tradičním	tradiční	k2eAgNnSc7d1	tradiční
místem	místo	k1gNnSc7	místo
korunovací	korunovace	k1gFnPc2	korunovace
<g/>
,	,	kIx,	,
svateb	svatba	k1gFnPc2	svatba
a	a	k8xC	a
místem	místo	k1gNnSc7	místo
posledního	poslední	k2eAgInSc2d1	poslední
odpočinku	odpočinek	k1gInSc2	odpočinek
anglických	anglický	k2eAgMnPc2d1	anglický
panovníků	panovník	k1gMnPc2	panovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stojí	stát	k5eAaImIp3nS	stát
současné	současný	k2eAgNnSc4d1	současné
opatství	opatství	k1gNnSc4	opatství
a	a	k8xC	a
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
známé	známý	k2eAgFnSc2d1	známá
jako	jako	k8xS	jako
Thorney	Thorne	k1gMnPc4	Thorne
Island	Island	k1gInSc4	Island
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
616	[number]	k4	616
postavena	postaven	k2eAgFnSc1d1	postavena
kaple	kaple	k1gFnSc1	kaple
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
měl	mít	k5eAaImAgMnS	mít
rybář	rybář	k1gMnSc1	rybář
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Temži	Temže	k1gFnSc6	Temže
vidění	vidění	k1gNnSc4	vidění
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Mnichové	mnich	k1gMnPc1	mnich
opatství	opatství	k1gNnSc4	opatství
dostávali	dostávat	k5eAaImAgMnP	dostávat
po	po	k7c4	po
staletí	staletí	k1gNnPc4	staletí
od	od	k7c2	od
rybářů	rybář	k1gMnPc2	rybář
jako	jako	k9	jako
dar	dar	k1gInSc4	dar
lososy	losos	k1gMnPc4	losos
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
existence	existence	k1gFnSc1	existence
této	tento	k3xDgFnSc2	tento
kaple	kaple	k1gFnSc2	kaple
není	být	k5eNaImIp3nS	být
historicky	historicky	k6eAd1	historicky
doložena	doložit	k5eAaPmNgFnS	doložit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
existovala	existovat	k5eAaImAgFnS	existovat
komunita	komunita	k1gFnSc1	komunita
benediktinských	benediktinský	k2eAgMnPc2d1	benediktinský
mnichů	mnich	k1gMnPc2	mnich
ještě	ještě	k6eAd1	ještě
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
zde	zde	k6eAd1	zde
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1045	[number]	k4	1045
až	až	k9	až
1050	[number]	k4	1050
nechal	nechat	k5eAaPmAgMnS	nechat
král	král	k1gMnSc1	král
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Vyznavač	vyznavač	k1gMnSc1	vyznavač
postavit	postavit	k5eAaPmF	postavit
opatství	opatství	k1gNnSc4	opatství
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
stavba	stavba	k1gFnSc1	stavba
započala	započnout	k5eAaPmAgFnS	započnout
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
král	král	k1gMnSc1	král
nedodržel	dodržet	k5eNaPmAgMnS	dodržet
slib	slib	k1gInSc4	slib
vykonat	vykonat	k5eAaPmF	vykonat
pouť	pouť	k1gFnSc4	pouť
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
byl	být	k5eAaImAgInS	být
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
28	[number]	k4	28
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1065	[number]	k4	1065
<g/>
,	,	kIx,	,
týden	týden	k1gInSc4	týden
před	před	k7c7	před
královou	králův	k2eAgFnSc7d1	králova
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jediné	jediný	k2eAgNnSc1d1	jediné
zobrazení	zobrazení	k1gNnSc1	zobrazení
původního	původní	k2eAgInSc2d1	původní
románského	románský	k2eAgInSc2d1	románský
chrámu	chrám	k1gInSc2	chrám
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sousedním	sousední	k2eAgInSc7d1	sousední
Westminsterským	Westminsterský	k2eAgInSc7d1	Westminsterský
palácem	palác	k1gInSc7	palác
existuje	existovat	k5eAaImIp3nS	existovat
na	na	k7c6	na
Bayeuxské	Bayeuxský	k2eAgFnSc6d1	Bayeuxský
tapisérii	tapisérie	k1gFnSc6	tapisérie
<g/>
.	.	kIx.	.
</s>
<s>
Eduardova	Eduardův	k2eAgFnSc1d1	Eduardova
fundace	fundace	k1gFnSc1	fundace
hrála	hrát	k5eAaImAgFnS	hrát
velkou	velký	k2eAgFnSc4d1	velká
úlohu	úloha	k1gFnSc4	úloha
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
kanonizaci	kanonizace	k1gFnSc6	kanonizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Opat	opat	k1gMnSc1	opat
a	a	k8xC	a
učení	učený	k2eAgMnPc1d1	učený
mnichové	mnich	k1gMnPc1	mnich
z	z	k7c2	z
opatství	opatství	k1gNnSc2	opatství
i	i	k8xC	i
vlivem	vliv	k1gInSc7	vliv
blízkosti	blízkost	k1gFnSc2	blízkost
Westminsterského	Westminsterský	k2eAgInSc2d1	Westminsterský
paláce	palác	k1gInSc2	palác
hráli	hrát	k5eAaImAgMnP	hrát
významnou	významný	k2eAgFnSc4d1	významná
úlohu	úloha	k1gFnSc4	úloha
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Opat	opat	k1gMnSc1	opat
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
v	v	k7c6	v
královských	královský	k2eAgFnPc6d1	královská
službách	služba	k1gFnPc6	služba
a	a	k8xC	a
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
pozice	pozice	k1gFnSc2	pozice
zasedal	zasedat	k5eAaImAgInS	zasedat
ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
lordů	lord	k1gMnPc2	lord
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Plantagenet	Plantagenet	k1gMnSc1	Plantagenet
nechal	nechat	k5eAaPmAgMnS	nechat
opatství	opatství	k1gNnSc4	opatství
přestavět	přestavět	k5eAaPmF	přestavět
v	v	k7c6	v
gotickém	gotický	k2eAgInSc6d1	gotický
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
stavební	stavební	k2eAgFnPc1d1	stavební
úpravy	úprava	k1gFnPc1	úprava
probíhaly	probíhat	k5eAaImAgFnP	probíhat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1245	[number]	k4	1245
až	až	k9	až
1517	[number]	k4	1517
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Tudor	tudor	k1gInSc1	tudor
nechal	nechat	k5eAaPmAgInS	nechat
roku	rok	k1gInSc2	rok
1503	[number]	k4	1503
postavit	postavit	k5eAaPmF	postavit
kapli	kaple	k1gFnSc4	kaple
věnovanou	věnovaný	k2eAgFnSc4d1	věnovaná
Panně	Panna	k1gFnSc6	Panna
Marii	Maria	k1gFnSc6	Maria
v	v	k7c6	v
pravoúhlém	pravoúhlý	k2eAgMnSc6d1	pravoúhlý
(	(	kIx(	(
<g/>
perpendikulárním	perpendikulární	k2eAgInSc6d1	perpendikulární
<g/>
)	)	kIx)	)
stylu	styl	k1gInSc6	styl
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
označována	označován	k2eAgFnSc1d1	označována
jako	jako	k8xC	jako
kaple	kaple	k1gFnPc1	kaple
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VII	VII	kA	VII
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
bylo	být	k5eAaImAgNnS	být
opatství	opatství	k1gNnSc4	opatství
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
zkonfiskováno	zkonfiskován	k2eAgNnSc1d1	zkonfiskováno
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1540	[number]	k4	1540
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1550	[number]	k4	1550
byla	být	k5eAaImAgFnS	být
přístupná	přístupný	k2eAgFnSc1d1	přístupná
pouze	pouze	k6eAd1	pouze
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
,	,	kIx,	,
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
královským	královský	k2eAgInSc7d1	královský
dvorem	dvůr	k1gInSc7	dvůr
jej	on	k3xPp3gInSc4	on
ochránily	ochránit	k5eAaPmAgFnP	ochránit
před	před	k7c7	před
zničením	zničení	k1gNnSc7	zničení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
opatství	opatství	k1gNnSc1	opatství
poškozeno	poškozen	k2eAgNnSc1d1	poškozeno
puritány	puritán	k1gMnPc7	puritán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Opatství	opatství	k1gNnSc1	opatství
bylo	být	k5eAaImAgNnS	být
navráceno	navrátit	k5eAaPmNgNnS	navrátit
řádu	řád	k1gInSc2	řád
benediktinů	benediktin	k1gMnPc2	benediktin
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
katolické	katolický	k2eAgFnSc2d1	katolická
královny	královna	k1gFnSc2	královna
Marie	Maria	k1gFnSc2	Maria
I.	I.	kA	I.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roku	rok	k1gInSc2	rok
1559	[number]	k4	1559
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
I.	I.	kA	I.
znovu	znovu	k6eAd1	znovu
opatství	opatství	k1gNnSc2	opatství
zabavila	zabavit	k5eAaPmAgFnS	zabavit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1579	[number]	k4	1579
Alžběta	Alžběta	k1gFnSc1	Alžběta
Westminster	Westminstra	k1gFnPc2	Westminstra
určila	určit	k5eAaPmAgFnS	určit
za	za	k7c4	za
královský	královský	k2eAgInSc4d1	královský
majetek	majetek	k1gInSc4	majetek
přímo	přímo	k6eAd1	přímo
podléhající	podléhající	k2eAgFnSc1d1	podléhající
panovníkovi	panovník	k1gMnSc3	panovník
a	a	k8xC	a
ustanovila	ustanovit	k5eAaPmAgFnS	ustanovit
kolegiátní	kolegiátní	k2eAgInSc4d1	kolegiátní
chrám	chrám	k1gInSc4	chrám
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
(	(	kIx(	(
<g/>
chrám	chrám	k1gInSc1	chrám
s	s	k7c7	s
připojenou	připojený	k2eAgFnSc7d1	připojená
kapitulou	kapitula	k1gFnSc7	kapitula
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
děkan	děkan	k1gMnSc1	děkan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
opat	opat	k1gMnSc1	opat
byl	být	k5eAaImAgMnS	být
ustanoven	ustanovit	k5eAaPmNgMnS	ustanovit
prvním	první	k4xOgMnSc7	první
děkanem	děkan	k1gMnSc7	děkan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dvě	dva	k4xCgFnPc1	dva
západní	západní	k2eAgFnPc1d1	západní
věže	věž	k1gFnPc1	věž
opatství	opatství	k1gNnSc2	opatství
byly	být	k5eAaImAgFnP	být
postaveny	postaven	k2eAgInPc1d1	postaven
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1722	[number]	k4	1722
až	až	k6eAd1	až
1745	[number]	k4	1745
Nicholasem	Nicholas	k1gMnSc7	Nicholas
Hawksmoorem	Hawksmoor	k1gMnSc7	Hawksmoor
z	z	k7c2	z
portlandského	portlandský	k2eAgInSc2d1	portlandský
vápence	vápenec	k1gInSc2	vápenec
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
rané	raný	k2eAgFnSc2d1	raná
novogotiky	novogotika	k1gFnSc2	novogotika
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
sira	sir	k1gMnSc2	sir
George	Georg	k1gMnSc2	Georg
Gilberta	Gilbert	k1gMnSc2	Gilbert
Scotta	Scott	k1gMnSc2	Scott
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
opatství	opatství	k1gNnSc4	opatství
třetím	třetí	k4xOgNnSc7	třetí
centrem	centrum	k1gNnSc7	centrum
vzdělanosti	vzdělanost	k1gFnSc2	vzdělanost
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
po	po	k7c6	po
Oxfordu	Oxford	k1gInSc6	Oxford
a	a	k8xC	a
Cambridgi	Cambridge	k1gFnSc6	Cambridge
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
přeložena	přeložit	k5eAaPmNgFnS	přeložit
první	první	k4xOgFnSc1	první
třetina	třetina	k1gFnSc1	třetina
Starého	Starého	k2eAgInSc2d1	Starého
zákona	zákon	k1gInSc2	zákon
(	(	kIx(	(
<g/>
Genesis	Genesis	k1gFnSc1	Genesis
<g/>
–	–	k?	–
<g/>
Druhá	druhý	k4xOgFnSc1	druhý
Královská	královský	k2eAgFnSc1d1	královská
<g/>
)	)	kIx)	)
a	a	k8xC	a
všechny	všechen	k3xTgFnPc4	všechen
epištoly	epištola	k1gFnPc4	epištola
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
tzv.	tzv.	kA	tzv.
bible	bible	k1gFnSc2	bible
Krále	Král	k1gMnSc2	Král
Jakuba	Jakub	k1gMnSc2	Jakub
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
anglická	anglický	k2eAgFnSc1d1	anglická
bible	bible	k1gFnSc1	bible
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
kompletována	kompletovat	k5eAaBmNgFnS	kompletovat
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
opatství	opatství	k1gNnSc6	opatství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
katedrály	katedrála	k1gFnSc2	katedrála
===	===	k?	===
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
opatství	opatství	k1gNnSc2	opatství
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
legenda	legenda	k1gFnSc1	legenda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zde	zde	k6eAd1	zde
měl	mít	k5eAaImAgMnS	mít
anglosaský	anglosaský	k2eAgMnSc1d1	anglosaský
král	král	k1gMnSc1	král
Sebert	Sebert	k1gMnSc1	Sebert
založit	založit	k5eAaPmF	založit
klášter	klášter	k1gInSc4	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
ve	v	k7c6	v
Westminsteru	Westminster	k1gInSc6	Westminster
nacházel	nacházet	k5eAaImAgMnS	nacházet
benediktinský	benediktinský	k2eAgInSc4d1	benediktinský
klášter	klášter	k1gInSc4	klášter
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgMnSc3	který
později	pozdě	k6eAd2	pozdě
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Vyznavač	vyznavač	k1gMnSc1	vyznavač
připojil	připojit	k5eAaPmAgMnS	připojit
kostel	kostel	k1gInSc4	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
Westminster	Westminstra	k1gFnPc2	Westminstra
znamená	znamenat	k5eAaImIp3nS	znamenat
Západní	západní	k2eAgInSc1d1	západní
kostel	kostel	k1gInSc1	kostel
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
umístění	umístění	k1gNnSc1	umístění
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
prosince	prosinec	k1gInSc2	prosinec
1065	[number]	k4	1065
byl	být	k5eAaImAgInS	být
kostel	kostel	k1gInSc1	kostel
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
<g/>
.	.	kIx.	.
</s>
<s>
Osm	osm	k4xCc1	osm
dní	den	k1gInPc2	den
nato	nato	k6eAd1	nato
Eduard	Eduard	k1gMnSc1	Eduard
Vyznavač	vyznavač	k1gMnSc1	vyznavač
zemřel	zemřít	k5eAaPmAgMnS	zemřít
a	a	k8xC	a
jako	jako	k8xC	jako
poslední	poslední	k2eAgMnSc1d1	poslední
anglosaský	anglosaský	k2eAgMnSc1d1	anglosaský
král	král	k1gMnSc1	král
nalezl	nalézt	k5eAaBmAgMnS	nalézt
poslední	poslední	k2eAgInSc4d1	poslední
odpočinek	odpočinek	k1gInSc4	odpočinek
právě	právě	k9	právě
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
kostele	kostel	k1gInSc6	kostel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Korunovace	korunovace	k1gFnSc2	korunovace
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
korunovace	korunovace	k1gFnSc2	korunovace
králů	král	k1gMnPc2	král
Harolda	Haroldo	k1gNnSc2	Haroldo
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Viléma	Vilém	k1gMnSc4	Vilém
I.	I.	kA	I.
Dobyvatele	dobyvatel	k1gMnSc4	dobyvatel
roku	rok	k1gInSc2	rok
1066	[number]	k4	1066
byli	být	k5eAaImAgMnP	být
všichni	všechen	k3xTgMnPc1	všechen
angličtí	anglický	k2eAgMnPc1d1	anglický
králové	král	k1gMnPc1	král
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Jany	Jana	k1gFnSc2	Jana
Greyové	Greyová	k1gFnSc2	Greyová
<g/>
,	,	kIx,	,
Eduarda	Eduard	k1gMnSc2	Eduard
V.	V.	kA	V.
a	a	k8xC	a
Eduarda	Eduard	k1gMnSc2	Eduard
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
korunováni	korunován	k2eAgMnPc1d1	korunován
ve	v	k7c6	v
Westminsterském	Westminsterský	k2eAgNnSc6d1	Westminsterské
opatství	opatství	k1gNnSc6	opatství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Korunovaci	korunovace	k1gFnSc4	korunovace
tradičně	tradičně	k6eAd1	tradičně
provádí	provádět	k5eAaImIp3nS	provádět
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
canterburský	canterburský	k2eAgMnSc1d1	canterburský
<g/>
.	.	kIx.	.
</s>
<s>
Korunovační	korunovační	k2eAgNnSc1d1	korunovační
křeslo	křeslo	k1gNnSc1	křeslo
krále	král	k1gMnSc2	král
Eduarda	Eduard	k1gMnSc2	Eduard
(	(	kIx(	(
<g/>
křeslo	křeslo	k1gNnSc1	křeslo
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
sedí	sedit	k5eAaImIp3nS	sedit
britský	britský	k2eAgMnSc1d1	britský
panovník	panovník	k1gMnSc1	panovník
při	při	k7c6	při
korunovaci	korunovace	k1gFnSc6	korunovace
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
opatrováno	opatrovat	k5eAaImNgNnS	opatrovat
v	v	k7c6	v
opatství	opatství	k1gNnSc6	opatství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Královské	královský	k2eAgFnSc2d1	královská
svatby	svatba	k1gFnSc2	svatba
==	==	k?	==
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1100	[number]	k4	1100
-	-	kIx~	-
král	král	k1gMnSc1	král
Jindřich	Jindřich	k1gMnSc1	Jindřich
I.	I.	kA	I.
Anglický	anglický	k2eAgMnSc1d1	anglický
a	a	k8xC	a
princezna	princezna	k1gFnSc1	princezna
Matylda	Matylda	k1gFnSc1	Matylda
Skotská	skotská	k1gFnSc1	skotská
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1243	[number]	k4	1243
-	-	kIx~	-
hrabě	hrabě	k1gMnSc1	hrabě
Richard	Richard	k1gMnSc1	Richard
z	z	k7c2	z
Cornwallu	Cornwall	k1gInSc2	Cornwall
a	a	k8xC	a
Sanchie	Sanchie	k1gFnSc1	Sanchie
Provensálská	provensálský	k2eAgFnSc1d1	provensálská
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
nebo	nebo	k8xC	nebo
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1269	[number]	k4	1269
-	-	kIx~	-
princ	princ	k1gMnSc1	princ
Edmund	Edmund	k1gMnSc1	Edmund
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
Jindřich	Jindřich	k1gMnSc1	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Plantageneta	Plantageneta	k1gFnSc1	Plantageneta
a	a	k8xC	a
Avelin	Avelin	k1gInSc1	Avelin
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
hraběte	hrabě	k1gMnSc2	hrabě
Williama	William	k1gMnSc2	William
de	de	k?	de
Forz	Forz	k1gMnSc1	Forz
</s>
</p>
<p>
<s>
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1290	[number]	k4	1290
-	-	kIx~	-
Johana	Johana	k1gFnSc1	Johana
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Eduarda	Eduard	k1gMnSc2	Eduard
I.	I.	kA	I.
a	a	k8xC	a
Gilbert	Gilbert	k1gMnSc1	Gilbert
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
z	z	k7c2	z
Gloucesteru	Gloucester	k1gInSc2	Gloucester
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1290	[number]	k4	1290
-	-	kIx~	-
Markéta	Markéta	k1gFnSc1	Markéta
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc1d1	další
dcera	dcera	k1gFnSc1	dcera
Eduarda	Eduard	k1gMnSc2	Eduard
I.	I.	kA	I.
a	a	k8xC	a
John	John	k1gMnSc1	John
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
vévody	vévoda	k1gMnSc2	vévoda
brabantského	brabantský	k2eAgInSc2d1	brabantský
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1382	[number]	k4	1382
-	-	kIx~	-
Richard	Richard	k1gMnSc1	Richard
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Anna	Anna	k1gFnSc1	Anna
Lucemburská	lucemburský	k2eAgFnSc1d1	Lucemburská
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
27	[number]	k4	27
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1919	[number]	k4	1919
-	-	kIx~	-
princezna	princezna	k1gFnSc1	princezna
Patricie	Patricie	k1gFnSc1	Patricie
Connaughtská	Connaughtský	k2eAgFnSc1d1	Connaughtský
a	a	k8xC	a
Alexander	Alexandra	k1gFnPc2	Alexandra
Ramsay	Ramsaa	k1gFnSc2	Ramsaa
</s>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1922	[number]	k4	1922
-	-	kIx~	-
princezna	princezna	k1gFnSc1	princezna
Marie	Marie	k1gFnSc1	Marie
<g/>
,	,	kIx,	,
jediná	jediný	k2eAgFnSc1d1	jediná
dcera	dcera	k1gFnSc1	dcera
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
V.	V.	kA	V.
a	a	k8xC	a
vikomt	vikomt	k1gMnSc1	vikomt
Lascellese	Lascellese	k1gFnSc2	Lascellese
</s>
</p>
<p>
<s>
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1923	[number]	k4	1923
-	-	kIx~	-
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnSc1d2	pozdější
král	král	k1gMnSc1	král
Jiří	Jiří	k1gMnSc1	Jiří
VI	VI	kA	VI
<g/>
.	.	kIx.	.
a	a	k8xC	a
lady	lady	k1gFnSc1	lady
Alžběta	Alžběta	k1gFnSc1	Alžběta
Bowes-Lyon	Bowes-Lyon	k1gInSc1	Bowes-Lyon
(	(	kIx(	(
<g/>
pozdější	pozdní	k2eAgFnSc1d2	pozdější
královna	královna	k1gFnSc1	královna
matka	matka	k1gFnSc1	matka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1934	[number]	k4	1934
-	-	kIx~	-
princ	princ	k1gMnSc1	princ
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Kentu	Kent	k1gInSc2	Kent
<g/>
,	,	kIx,	,
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
syn	syn	k1gMnSc1	syn
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
V.	V.	kA	V.
a	a	k8xC	a
řecká	řecký	k2eAgFnSc1d1	řecká
a	a	k8xC	a
dánská	dánský	k2eAgFnSc1d1	dánská
princezna	princezna	k1gFnSc1	princezna
Marina	Marina	k1gFnSc1	Marina
</s>
</p>
<p>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1947	[number]	k4	1947
-	-	kIx~	-
princezna	princezna	k1gFnSc1	princezna
Alžběta	Alžběta	k1gFnSc1	Alžběta
(	(	kIx(	(
<g/>
pozdější	pozdní	k2eAgFnSc1d2	pozdější
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
poručík	poručík	k1gMnSc1	poručík
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
Philip	Philip	k1gMnSc1	Philip
Mountbattena	Mountbatten	k2eAgFnSc1d1	Mountbattena
(	(	kIx(	(
<g/>
pozdější	pozdní	k2eAgMnSc1d2	pozdější
Philip	Philip	k1gMnSc1	Philip
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Edinburghu	Edinburgh	k1gInSc2	Edinburgh
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1963	[number]	k4	1963
-	-	kIx~	-
princezna	princezna	k1gFnSc1	princezna
Alexandra	Alexandr	k1gMnSc2	Alexandr
z	z	k7c2	z
Kentu	Kent	k1gInSc2	Kent
a	a	k8xC	a
Angus	Angus	k1gInSc4	Angus
Ogilvy	Ogilva	k1gFnSc2	Ogilva
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
hraběte	hrabě	k1gMnSc2	hrabě
z	z	k7c2	z
Airlie	Airlie	k1gFnSc2	Airlie
</s>
</p>
<p>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1973	[number]	k4	1973
-	-	kIx~	-
princezna	princezna	k1gFnSc1	princezna
Anna	Anna	k1gFnSc1	Anna
a	a	k8xC	a
kapitán	kapitán	k1gMnSc1	kapitán
Mark	Mark	k1gMnSc1	Mark
Phillips	Phillips	k1gInSc4	Phillips
</s>
</p>
<p>
<s>
23	[number]	k4	23
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1986	[number]	k4	1986
-	-	kIx~	-
Andrew	Andrew	k1gMnSc1	Andrew
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Yorku	York	k1gInSc2	York
a	a	k8xC	a
Sarah	Sarah	k1gFnSc1	Sarah
Margaret	Margareta	k1gFnPc2	Margareta
Fergusonová	Fergusonová	k1gFnSc1	Fergusonová
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
-	-	kIx~	-
Princ	princa	k1gFnPc2	princa
William	William	k1gInSc1	William
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Cambridge	Cambridge	k1gFnSc2	Cambridge
a	a	k8xC	a
Catherine	Catherin	k1gInSc5	Catherin
Elizabeth	Elizabeth	k1gFnSc7	Elizabeth
Middleton	Middleton	k1gInSc4	Middleton
</s>
</p>
<p>
<s>
==	==	k?	==
Hrobky	hrobek	k1gInPc1	hrobek
a	a	k8xC	a
památníky	památník	k1gInPc1	památník
==	==	k?	==
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
přestavěl	přestavět	k5eAaPmAgMnS	přestavět
opatství	opatství	k1gNnSc4	opatství
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
Eduarda	Eduard	k1gMnSc2	Eduard
Vyznavače	vyznavač	k1gMnSc2	vyznavač
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
hrobka	hrobka	k1gFnSc1	hrobka
a	a	k8xC	a
památník	památník	k1gInSc1	památník
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgInP	umístit
v	v	k7c6	v
sakristii	sakristie	k1gFnSc6	sakristie
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
III	III	kA	III
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgInS	být
pohřben	pohřbít	k5eAaPmNgInS	pohřbít
poblíž	poblíž	k7c2	poblíž
dalších	další	k2eAgMnPc2d1	další
panovníků	panovník	k1gMnPc2	panovník
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
rodinných	rodinný	k2eAgMnPc2d1	rodinný
příslušníků	příslušník	k1gMnPc2	příslušník
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Plantagenetů	Plantagenet	k1gInPc2	Plantagenet
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
pohřbena	pohřben	k2eAgFnSc1d1	pohřbena
většina	většina	k1gFnSc1	většina
anglických	anglický	k2eAgMnPc2d1	anglický
panovníků	panovník	k1gMnPc2	panovník
kromě	kromě	k7c2	kromě
Jindřicha	Jindřich	k1gMnSc2	Jindřich
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
Karla	Karel	k1gMnSc4	Karel
I.	I.	kA	I.
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
pochováni	pochovat	k5eAaPmNgMnP	pochovat
v	v	k7c6	v
kapli	kaple	k1gFnSc6	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Jiří	Jiří	k1gMnSc2	Jiří
ve	v	k7c6	v
Windsorském	windsorský	k2eAgInSc6d1	windsorský
zámku	zámek	k1gInSc6	zámek
(	(	kIx(	(
<g/>
od	od	k7c2	od
Jiřího	Jiří	k1gMnSc2	Jiří
II	II	kA	II
<g/>
.	.	kIx.	.
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
Windsoru	Windsor	k1gInSc6	Windsor
pohřbeni	pohřbít	k5eAaPmNgMnP	pohřbít
všichni	všechen	k3xTgMnPc1	všechen
britští	britský	k2eAgMnPc1d1	britský
panovníci	panovník	k1gMnPc1	panovník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
pod	pod	k7c7	pod
mozaikou	mozaika	k1gFnSc7	mozaika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1268	[number]	k4	1268
objevena	objeven	k2eAgFnSc1d1	objevena
původní	původní	k2eAgFnSc1d1	původní
hrobka	hrobka	k1gFnSc1	hrobka
Eduarda	Eduard	k1gMnSc2	Eduard
Vyznavače	vyznavač	k1gMnSc2	vyznavač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aristokraté	aristokrat	k1gMnPc1	aristokrat
byli	být	k5eAaImAgMnP	být
pohřbívání	pohřbívání	k1gNnSc4	pohřbívání
v	v	k7c6	v
postranních	postranní	k2eAgFnPc6d1	postranní
kaplích	kaple	k1gFnPc6	kaple
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
mnichové	mnich	k1gMnPc1	mnich
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
spojení	spojení	k1gNnPc2	spojení
s	s	k7c7	s
opatstvím	opatství	k1gNnSc7	opatství
v	v	k7c6	v
křížových	křížový	k2eAgFnPc6d1	křížová
chodbách	chodba	k1gFnPc6	chodba
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
lidí	člověk	k1gMnPc2	člověk
je	být	k5eAaImIp3nS	být
i	i	k9	i
Geoffrey	Geoffrey	k1gInPc4	Geoffrey
Chaucer	Chaucra	k1gFnPc2	Chaucra
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
opatství	opatství	k1gNnSc6	opatství
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
panovníka	panovník	k1gMnSc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
umělci	umělec	k1gMnPc1	umělec
byli	být	k5eAaImAgMnP	být
pohřbeni	pohřbít	k5eAaPmNgMnP	pohřbít
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
hrobu	hrob	k1gInSc2	hrob
Chaucera	Chaucero	k1gNnSc2	Chaucero
a	a	k8xC	a
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
místo	místo	k1gNnSc1	místo
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
jako	jako	k8xS	jako
Poets	Poets	k1gInSc1	Poets
<g/>
'	'	kIx"	'
Corner	Corner	k1gInSc1	Corner
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
pohřbeno	pohřbít	k5eAaPmNgNnS	pohřbít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
osobností	osobnost	k1gFnPc2	osobnost
spojených	spojený	k2eAgFnPc2d1	spojená
s	s	k7c7	s
historií	historie	k1gFnSc7	historie
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pohřbení	pohřbení	k1gNnSc2	pohřbení
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
chrámová	chrámový	k2eAgFnSc1d1	chrámová
loď	loď	k1gFnSc1	loď
====	====	k?	====
</s>
</p>
<p>
<s>
Clement	Clement	k1gInSc1	Clement
Attlee	Attle	k1gFnSc2	Attle
</s>
</p>
<p>
<s>
Angela	Angela	k1gFnSc1	Angela
Georgina	Georgina	k1gFnSc1	Georgina
Burdett-Coutts	Burdett-Coutts	k1gInSc4	Burdett-Coutts
</s>
</p>
<p>
<s>
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
</s>
</p>
<p>
<s>
James	James	k1gInSc1	James
Clerk	Clerk	k1gInSc1	Clerk
Maxwell	maxwell	k1gInSc4	maxwell
</s>
</p>
<p>
<s>
J.J.	J.J.	k?	J.J.
Thomson	Thomson	k1gInSc1	Thomson
</s>
</p>
<p>
<s>
svatý	svatý	k2eAgMnSc1d1	svatý
Eduard	Eduard	k1gMnSc1	Eduard
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Vyznavač	vyznavač	k1gMnSc1	vyznavač
</s>
</p>
<p>
<s>
Ben	Ben	k1gInSc1	Ben
Jonson	Jonsona	k1gFnPc2	Jonsona
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Livingstone	Livingston	k1gInSc5	Livingston
</s>
</p>
<p>
<s>
Sir	sir	k1gMnSc1	sir
Isaac	Isaac	k1gInSc4	Isaac
Newton	Newton	k1gMnSc1	Newton
</s>
</p>
<p>
<s>
Ernest	Ernest	k1gMnSc1	Ernest
Rutherford	Rutherford	k1gMnSc1	Rutherford
</s>
</p>
<p>
<s>
William	William	k6eAd1	William
Thomson	Thomson	k1gInSc1	Thomson
</s>
</p>
<p>
<s>
neznámý	známý	k2eNgMnSc1d1	neznámý
vojín	vojín	k1gMnSc1	vojín
</s>
</p>
<p>
<s>
George	George	k1gFnSc1	George
Villiers	Villiersa	k1gFnPc2	Villiersa
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Buckinghamu	Buckingham	k1gInSc2	Buckingham
</s>
</p>
<p>
<s>
Ludovic	Ludovice	k1gFnPc2	Ludovice
Stewart	Stewart	k1gMnSc1	Stewart
</s>
</p>
<p>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Tompion	Tompion	k1gInSc4	Tompion
</s>
</p>
<p>
<s>
George	Georg	k1gMnSc2	Georg
Graham	Graham	k1gMnSc1	Graham
</s>
</p>
<p>
<s>
Stephen	Stephen	k2eAgInSc1d1	Stephen
Hawking	Hawking	k1gInSc1	Hawking
</s>
</p>
<p>
<s>
====	====	k?	====
Severní	severní	k2eAgFnSc1d1	severní
příčná	příčný	k2eAgFnSc1d1	příčná
chrámová	chrámový	k2eAgFnSc1d1	chrámová
loď	loď	k1gFnSc1	loď
====	====	k?	====
</s>
</p>
<p>
<s>
William	William	k6eAd1	William
Ewart	Ewart	k1gInSc1	Ewart
Gladstone	Gladston	k1gInSc5	Gladston
</s>
</p>
<p>
<s>
William	William	k6eAd1	William
Pitt	Pitt	k2eAgInSc1d1	Pitt
</s>
</p>
<p>
<s>
William	William	k6eAd1	William
Pitt	Pitt	k2eAgMnSc1d1	Pitt
(	(	kIx(	(
<g/>
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Jižní	jižní	k2eAgFnSc1d1	jižní
příčná	příčný	k2eAgFnSc1d1	příčná
chrámová	chrámový	k2eAgFnSc1d1	chrámová
loď	loď	k1gFnSc1	loď
====	====	k?	====
</s>
</p>
<p>
<s>
Poets	Poets	k1gInSc1	Poets
<g/>
'	'	kIx"	'
Corner	Corner	k1gInSc1	Corner
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Adam	Adam	k1gMnSc1	Adam
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Browning	browning	k1gInSc4	browning
</s>
</p>
<p>
<s>
William	William	k1gInSc1	William
Camden	Camdna	k1gFnPc2	Camdna
</s>
</p>
<p>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Campbell	Campbell	k1gMnSc1	Campbell
</s>
</p>
<p>
<s>
Geoffrey	Geoffrea	k1gFnPc1	Geoffrea
Chaucer	Chaucra	k1gFnPc2	Chaucra
</s>
</p>
<p>
<s>
William	William	k1gInSc1	William
Congreve	Congreev	k1gFnSc2	Congreev
</s>
</p>
<p>
<s>
Abraham	Abraham	k1gMnSc1	Abraham
Cowley	Cowlea	k1gFnSc2	Cowlea
</s>
</p>
<p>
<s>
William	William	k6eAd1	William
Davenant	Davenant	k1gInSc1	Davenant
</s>
</p>
<p>
<s>
Charles	Charles	k1gMnSc1	Charles
Dickens	Dickensa	k1gFnPc2	Dickensa
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Dryden	Drydna	k1gFnPc2	Drydna
</s>
</p>
<p>
<s>
Adam	Adam	k1gMnSc1	Adam
Fox	fox	k1gInSc4	fox
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Garrick	Garrick	k1gMnSc1	Garrick
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Gay	gay	k1gMnSc1	gay
</s>
</p>
<p>
<s>
George	George	k1gFnSc1	George
Frederick	Fredericko	k1gNnPc2	Fredericko
Handel	Handlo	k1gNnPc2	Handlo
</s>
</p>
<p>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Hardy	Harda	k1gFnSc2	Harda
</s>
</p>
<p>
<s>
Samuel	Samuel	k1gMnSc1	Samuel
Johnson	Johnson	k1gMnSc1	Johnson
</s>
</p>
<p>
<s>
Rudyard	Rudyard	k1gInSc1	Rudyard
Kipling	Kipling	k1gInSc1	Kipling
</s>
</p>
<p>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Macaulay	Macaulaa	k1gFnSc2	Macaulaa
</s>
</p>
<p>
<s>
John	John	k1gMnSc1	John
Masefield	Masefield	k1gMnSc1	Masefield
</s>
</p>
<p>
<s>
Laurence	Laurence	k1gFnSc1	Laurence
Olivier	Olivira	k1gFnPc2	Olivira
</s>
</p>
<p>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Parr	Parr	k1gMnSc1	Parr
</s>
</p>
<p>
<s>
Richard	Richard	k1gMnSc1	Richard
Brinsley	Brinslea	k1gFnSc2	Brinslea
Sheridan	Sheridan	k1gMnSc1	Sheridan
</s>
</p>
<p>
<s>
Edmund	Edmund	k1gMnSc1	Edmund
Spenser	Spenser	k1gMnSc1	Spenser
</s>
</p>
<p>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
Tennyson	Tennyson	k1gMnSc1	Tennyson
</s>
</p>
<p>
<s>
====	====	k?	====
Křížová	křížový	k2eAgFnSc1d1	křížová
chodba	chodba	k1gFnSc1	chodba
====	====	k?	====
</s>
</p>
<p>
<s>
Aphra	Aphra	k1gMnSc1	Aphra
Behn	Behn	k1gMnSc1	Behn
</s>
</p>
<p>
<s>
====	====	k?	====
Severní	severní	k2eAgFnSc1d1	severní
chrámová	chrámový	k2eAgFnSc1d1	chrámová
loď	loď	k1gFnSc1	loď
====	====	k?	====
</s>
</p>
<p>
<s>
Henry	Henry	k1gMnSc1	Henry
Purcell	Purcell	k1gMnSc1	Purcell
</s>
</p>
<p>
<s>
Ralph	Ralph	k1gInSc1	Ralph
Vaughan	Vaughany	k1gInPc2	Vaughany
Williams	Williamsa	k1gFnPc2	Williamsa
</s>
</p>
<p>
<s>
====	====	k?	====
Odstranění	odstranění	k1gNnSc1	odstranění
====	====	k?	====
</s>
</p>
<p>
<s>
Následující	následující	k2eAgFnPc1d1	následující
osobnosti	osobnost	k1gFnPc1	osobnost
byly	být	k5eAaImAgFnP	být
pohřbeny	pohřbít	k5eAaPmNgFnP	pohřbít
v	v	k7c4	v
opatství	opatství	k1gNnSc4	opatství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgInP	být
jejich	jejich	k3xOp3gInPc1	jejich
ostatky	ostatek	k1gInPc1	ostatek
odstraněny	odstranit	k5eAaPmNgInP	odstranit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oliver	Oliver	k1gMnSc1	Oliver
Cromwell	Cromwell	k1gMnSc1	Cromwell
</s>
</p>
<p>
<s>
admirál	admirál	k1gMnSc1	admirál
Robert	Robert	k1gMnSc1	Robert
Blake	Blake	k1gInSc4	Blake
</s>
</p>
<p>
<s>
===	===	k?	===
Památníky	památník	k1gInPc4	památník
===	===	k?	===
</s>
</p>
<p>
<s>
William	William	k6eAd1	William
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
</s>
</p>
<p>
<s>
sir	sir	k1gMnSc1	sir
Winston	Winston	k1gInSc4	Winston
Churchill	Churchill	k1gMnSc1	Churchill
</s>
</p>
<p>
<s>
Benjamin	Benjamin	k1gMnSc1	Benjamin
Disraeli	Disrael	k1gInPc7	Disrael
</s>
</p>
<p>
<s>
Adam	Adam	k1gMnSc1	Adam
Lindsay	Lindsaa	k1gFnSc2	Lindsaa
Gordon	Gordon	k1gMnSc1	Gordon
</s>
</p>
<p>
<s>
Paul	Paul	k1gMnSc1	Paul
Dirac	Dirac	k1gInSc4	Dirac
</s>
</p>
<p>
<s>
Oscar	Oscar	k1gInSc1	Oscar
Wilde	Wild	k1gInSc5	Wild
</s>
</p>
<p>
<s>
====	====	k?	====
Mučedníci	mučedník	k1gMnPc1	mučedník
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
====	====	k?	====
</s>
</p>
<p>
<s>
Nad	nad	k7c7	nad
velkými	velký	k2eAgFnPc7d1	velká
dveřmi	dveře	k1gFnPc7	dveře
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
průčelí	průčelí	k1gNnSc6	průčelí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
galerie	galerie	k1gFnSc1	galerie
sousoší	sousoší	k1gNnSc1	sousoší
připomínající	připomínající	k2eAgFnSc2d1	připomínající
mučedníky	mučedník	k1gMnPc4	mučedník
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
sv.	sv.	kA	sv.
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Kolbe	Kolb	k1gInSc5	Kolb
</s>
</p>
<p>
<s>
Manche	Manche	k1gFnSc1	Manche
Masemola	Masemola	k1gFnSc1	Masemola
</s>
</p>
<p>
<s>
Janani	Janaň	k1gFnSc3	Janaň
Luwum	Luwum	k1gNnSc1	Luwum
</s>
</p>
<p>
<s>
sv.	sv.	kA	sv.
Alžběta	Alžběta	k1gFnSc1	Alžběta
Ruská	ruský	k2eAgFnSc1d1	ruská
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Luther	Luthra	k1gFnPc2	Luthra
King	King	k1gMnSc1	King
</s>
</p>
<p>
<s>
Óscar	Óscar	k1gInSc1	Óscar
Romero	Romero	k1gNnSc1	Romero
</s>
</p>
<p>
<s>
Dietrich	Dietrich	k1gMnSc1	Dietrich
Bonhoeffer	Bonhoeffer	k1gMnSc1	Bonhoeffer
</s>
</p>
<p>
<s>
Esther	Esthra	k1gFnPc2	Esthra
John	John	k1gMnSc1	John
</s>
</p>
<p>
<s>
Lucian	Lucian	k1gInSc4	Lucian
Tapiedi	Tapied	k1gMnPc1	Tapied
</s>
</p>
<p>
<s>
Wang	Wang	k1gInSc1	Wang
Zhiming	Zhiming	k1gInSc1	Zhiming
</s>
</p>
<p>
<s>
==	==	k?	==
Školy	škola	k1gFnPc1	škola
==	==	k?	==
</s>
</p>
<p>
<s>
Westminsterská	Westminsterský	k2eAgFnSc1d1	Westminsterská
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
Škola	škola	k1gFnSc1	škola
sboru	sbor	k1gInSc2	sbor
Westminsterského	Westminsterský	k2eAgNnSc2d1	Westminsterské
opatství	opatství	k1gNnSc2	opatství
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
ve	v	k7c6	v
Westminsterském	Westminsterský	k2eAgNnSc6d1	Westminsterské
opatství	opatství	k1gNnSc6	opatství
<g/>
.	.	kIx.	.
</s>
<s>
Westminsterská	Westminsterský	k2eAgFnSc1d1	Westminsterská
škola	škola	k1gFnSc1	škola
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
založena	založen	k2eAgFnSc1d1	založena
benediktinskými	benediktinský	k2eAgMnPc7d1	benediktinský
mnichy	mnich	k1gMnPc7	mnich
roku	rok	k1gInSc2	rok
1179	[number]	k4	1179
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dopravní	dopravní	k2eAgNnSc1d1	dopravní
spojení	spojení	k1gNnSc1	spojení
–	–	k?	–
metro	metro	k1gNnSc1	metro
<g/>
:	:	kIx,	:
St.	st.	kA	st.
James	James	k1gMnSc1	James
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Park	park	k1gInSc1	park
a	a	k8xC	a
Westminster	Westminster	k1gInSc1	Westminster
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Památník	památník	k1gInSc1	památník
československých	československý	k2eAgMnPc2d1	československý
vojáků	voják	k1gMnPc2	voják
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
instalován	instalovat	k5eAaBmNgInS	instalovat
památník	památník	k1gInSc1	památník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vzdává	vzdávat	k5eAaImIp3nS	vzdávat
hold	hold	k1gInSc1	hold
československým	československý	k2eAgMnPc3d1	československý
vojákům	voják	k1gMnPc3	voják
a	a	k8xC	a
letcům	letec	k1gMnPc3	letec
<g/>
,	,	kIx,	,
bojujícím	bojující	k2eAgMnSc6d1	bojující
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Westminsterské	Westminsterský	k2eAgFnSc2d1	Westminsterská
opatství	opatství	k1gNnSc4	opatství
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Westminsterské	Westminsterský	k2eAgNnSc1d1	Westminsterské
opatství	opatství	k1gNnSc1	opatství
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Westminsterské	Westminsterský	k2eAgNnSc4d1	Westminsterské
opatství	opatství	k1gNnSc4	opatství
Galerie	galerie	k1gFnSc1	galerie
Westminsterské	Westminsterský	k2eAgNnSc4d1	Westminsterské
opatství	opatství	k1gNnSc4	opatství
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
