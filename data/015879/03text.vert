<s>
Velké	velký	k2eAgNnSc1d1
sedlo	sedlo	k1gNnSc1
(	(	kIx(
<g/>
seriál	seriál	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Velké	velký	k2eAgNnSc1d1
sedlo	sedlo	k1gNnSc1
Žánr	žánr	k1gInSc1
</s>
<s>
drama	drama	k1gNnSc1
Námět	námět	k1gInSc1
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
DietlFrantišek	DietlFrantišek	k1gMnSc1
Mudra	Mudra	k1gMnSc1
Dramaturgie	dramaturgie	k1gFnSc1
</s>
<s>
Lubomír	Lubomír	k1gMnSc1
Zaorálek	Zaorálek	k1gMnSc1
Scénář	scénář	k1gInSc4
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Dietl	Dietl	k1gFnSc2
Režie	režie	k1gFnSc1
</s>
<s>
František	František	k1gMnSc1
Mudra	Mudra	k1gMnSc1
Výprava	výprava	k1gMnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Hapala	Hapal	k1gMnSc2
Hrají	hrát	k5eAaImIp3nP
</s>
<s>
Pavel	Pavel	k1gMnSc1
Nový	Nový	k1gMnSc1
Jana	Jana	k1gFnSc1
Janěková	Janěková	k1gFnSc1
Jana	Jana	k1gFnSc1
Krausová	Krausová	k1gFnSc1
Lucie	Lucie	k1gFnSc1
Bulavová	Bulavový	k2eAgFnSc1d1
Ivan	Ivan	k1gMnSc1
Vyskočil	Vyskočil	k1gMnSc1
Vítězslav	Vítězslav	k1gMnSc1
Jandák	Jandák	k1gMnSc1
Jitka	Jitka	k1gFnSc1
Smutná	Smutná	k1gFnSc1
Země	zem	k1gFnSc2
původu	původ	k1gInSc2
</s>
<s>
Československo	Československo	k1gNnSc1
Československo	Československo	k1gNnSc1
Jazyk	jazyk	k1gInSc1
</s>
<s>
čeština	čeština	k1gFnSc1
Počet	počet	k1gInSc1
řad	řad	k1gInSc1
</s>
<s>
1	#num#	k4
Počet	počet	k1gInSc4
dílů	díl	k1gInPc2
</s>
<s>
9	#num#	k4
(	(	kIx(
<g/>
seznam	seznam	k1gInSc4
dílů	díl	k1gInPc2
<g/>
)	)	kIx)
Obvyklá	obvyklý	k2eAgFnSc1d1
délka	délka	k1gFnSc1
</s>
<s>
47	#num#	k4
minut	minuta	k1gFnPc2
Produkce	produkce	k1gFnSc1
a	a	k8xC
štáb	štáb	k1gInSc1
Kamera	kamera	k1gFnSc1
</s>
<s>
Jaromír	Jaromír	k1gMnSc1
Zaoral	Zaoral	k1gMnSc1
Robert	Robert	k1gMnSc1
Huber	Huber	k1gMnSc1
Hudba	hudba	k1gFnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Cron	Cron	k1gMnSc1
Produkčníspolečnost	Produkčníspolečnost	k1gFnSc4
</s>
<s>
Československá	československý	k2eAgFnSc1d1
televize	televize	k1gFnSc1
Premiérové	premiérový	k2eAgNnSc1d1
vysílání	vysílání	k1gNnSc1
Stanice	stanice	k1gFnSc1
</s>
<s>
Československá	československý	k2eAgFnSc1d1
televize	televize	k1gFnSc1
Vysíláno	vysílán	k2eAgNnSc1d1
</s>
<s>
19870322	#num#	k4
<g/>
a	a	k8xC
<g/>
22	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1987	#num#	k4
–	–	k?
19870419	#num#	k4
<g/>
a	a	k8xC
<g/>
19	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1987	#num#	k4
Velké	velká	k1gFnSc2
sedlo	sednout	k5eAaPmAgNnS
na	na	k7c4
ČSFDNěkterá	ČSFDNěkterý	k2eAgNnPc4d1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Velké	velký	k2eAgNnSc1d1
sedlo	sedlo	k1gNnSc1
je	být	k5eAaImIp3nS
devítidílný	devítidílný	k2eAgInSc1d1
československý	československý	k2eAgInSc1d1
televizní	televizní	k2eAgInSc1d1
seriál	seriál	k1gInSc1
režiséra	režisér	k1gMnSc2
Františka	František	k1gMnSc2
Mudry	Mudra	k1gMnSc2
podle	podle	k7c2
scénáře	scénář	k1gInSc2
Jaroslava	Jaroslav	k1gMnSc2
Dietla	Dietla	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seriál	seriál	k1gInSc4
natočilo	natočit	k5eAaBmAgNnS
ostravské	ostravský	k2eAgNnSc1d1
studio	studio	k1gNnSc1
Československé	československý	k2eAgFnSc2d1
televize	televize	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1986	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
seriálu	seriál	k1gInSc6
účinkovaly	účinkovat	k5eAaImAgFnP
herecké	herecký	k2eAgFnPc1d1
hvězdy	hvězda	k1gFnPc1
jako	jako	k8xC,k8xS
Pavel	Pavel	k1gMnSc1
Nový	Nový	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
Vyskočil	Vyskočil	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Kraus	Kraus	k1gMnSc1
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
Krausová	Krausová	k1gFnSc1
<g/>
,	,	kIx,
Vítězslav	Vítězslav	k1gMnSc1
Jandák	Jandák	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Holý	Holý	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Děj	děj	k1gInSc1
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1
hrdinou	hrdina	k1gMnSc7
seriálu	seriál	k1gInSc2
je	být	k5eAaImIp3nS
Jan	Jan	k1gMnSc1
Palyza	Palyz	k1gMnSc2
(	(	kIx(
<g/>
Pavel	Pavel	k1gMnSc1
Nový	Nový	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hrázný	hrázný	k1gMnSc1
na	na	k7c6
přehradě	přehrada	k1gFnSc6
Velké	velký	k2eAgNnSc4d1
sedlo	sedlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
povolání	povolání	k1gNnSc6
musí	muset	k5eAaImIp3nS
čelit	čelit	k5eAaImF
přírodním	přírodní	k2eAgInPc3d1
živlům	živel	k1gInPc3
v	v	k7c6
období	období	k1gNnSc6
sucha	sucho	k1gNnSc2
i	i	k8xC
velké	velký	k2eAgFnSc2d1
vody	voda	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
důsledkům	důsledek	k1gInPc3
macešského	macešský	k2eAgNnSc2d1
chování	chování	k1gNnSc2
lidí	člověk	k1gMnPc2
k	k	k7c3
vodě	voda	k1gFnSc3
a	a	k8xC
přírodě	příroda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
osobním	osobní	k2eAgInSc6d1
životě	život	k1gInSc6
prožívá	prožívat	k5eAaImIp3nS
velkou	velký	k2eAgFnSc4d1
lásku	láska	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
zklamání	zklamání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nehasnoucí	hasnoucí	k2eNgFnSc7d1
stálicí	stálice	k1gFnSc7
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
života	život	k1gInSc2
tak	tak	k6eAd1
zůstává	zůstávat	k5eAaImIp3nS
přehrada	přehrada	k1gFnSc1
Velké	velká	k1gFnSc2
sedlo	sedlo	k1gNnSc1
a	a	k8xC
malebná	malebný	k2eAgFnSc1d1
okolní	okolní	k2eAgFnSc1d1
příroda	příroda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Seriálu	seriál	k1gInSc2
dominují	dominovat	k5eAaImIp3nP
záběry	záběr	k1gInPc1
přehradní	přehradní	k2eAgFnSc2d1
nádrže	nádrž	k1gFnSc2
<g/>
,	,	kIx,
vodní	vodní	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
a	a	k8xC
okolních	okolní	k2eAgFnPc2d1
přírodních	přírodní	k2eAgFnPc2d1
scenérií	scenérie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Divácky	divácky	k6eAd1
atraktivní	atraktivní	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
zejména	zejména	k9
letecké	letecký	k2eAgInPc4d1
filmové	filmový	k2eAgInPc4d1
záběry	záběr	k1gInPc4
<g/>
,	,	kIx,
pořízené	pořízený	k2eAgInPc4d1
na	na	k7c6
Kružberské	Kružberský	k2eAgFnSc6d1
přehradě	přehrada	k1gFnSc6
a	a	k8xC
v	v	k7c6
jejím	její	k3xOp3gNnSc6
okolí	okolí	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
většina	většina	k1gFnSc1
seriálu	seriál	k1gInSc2
natočena	natočen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
seriálu	seriál	k1gInSc6
jsou	být	k5eAaImIp3nP
také	také	k9
zaznamenány	zaznamenat	k5eAaPmNgFnP
zatopené	zatopený	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
na	na	k7c6
nynějším	nynější	k2eAgNnSc6d1
dně	dno	k1gNnSc6
vodní	vodní	k2eAgFnSc2d1
nádrže	nádrž	k1gFnSc2
Slezská	slezský	k2eAgFnSc1d1
Harta	Harta	k1gFnSc1
a	a	k8xC
detailní	detailní	k2eAgInPc1d1
záběry	záběr	k1gInPc1
příprav	příprava	k1gFnPc2
pro	pro	k7c4
její	její	k3xOp3gFnSc4
stavbu	stavba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seriál	seriál	k1gInSc1
byl	být	k5eAaImAgInS
natočen	natočit	k5eAaBmNgInS
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
Povodím	povodí	k1gNnSc7
Odry	Odra	k1gFnSc2
<g/>
,	,	kIx,
podnikem	podnik	k1gInSc7
pro	pro	k7c4
provoz	provoz	k1gInSc4
a	a	k8xC
využití	využití	k1gNnSc4
vodních	vodní	k2eAgInPc2d1
toků	tok	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
seriálu	seriál	k1gInSc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
detailní	detailní	k2eAgInSc1d1
pohled	pohled	k1gInSc1
nejen	nejen	k6eAd1
do	do	k7c2
soukromeho	soukrome	k1gMnSc2
života	život	k1gInSc2
lidí	člověk	k1gMnPc2
obsluhující	obsluhující	k2eAgFnSc4d1
hráz	hráz	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
pracovního	pracovní	k2eAgInSc2d1
života	život	k1gInSc2
a	a	k8xC
plnění	plnění	k1gNnSc2
různých	různý	k2eAgFnPc2d1
pracovních	pracovní	k2eAgFnPc2d1
povinností	povinnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Obsazení	obsazení	k1gNnSc1
</s>
<s>
Pavel	Pavel	k1gMnSc1
Nový	Nový	k1gMnSc1
</s>
<s>
hrázný	hrázný	k1gMnSc1
Jan	Jan	k1gMnSc1
Palyza	Palyza	k1gFnSc1
</s>
<s>
Jana	Jana	k1gFnSc1
Janěková	Janěková	k1gFnSc1
</s>
<s>
Palyzová	Palyzová	k1gFnSc1
</s>
<s>
Jana	Jana	k1gFnSc1
Krausová	Krausová	k1gFnSc1
</s>
<s>
Ivana	Ivana	k1gFnSc1
Opatová	Opatová	k1gFnSc1
</s>
<s>
Lucie	Lucie	k1gFnSc1
Bulavová	Bulavový	k2eAgFnSc1d1
</s>
<s>
Lucinka	Lucinka	k1gFnSc1
Opatová	Opatová	k1gFnSc1
</s>
<s>
Ivan	Ivan	k1gMnSc1
Vyskočil	Vyskočil	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Sikyta	Sikyta	k1gMnSc1
</s>
<s>
Vítězslav	Vítězslav	k1gMnSc1
Jandák	Jandák	k1gMnSc1
</s>
<s>
Jožin	Jožin	k2eAgInSc1d1
Uherčík	Uherčík	k1gInSc1
</s>
<s>
Jitka	Jitka	k1gFnSc1
Smutná	Smutná	k1gFnSc1
</s>
<s>
Květa	Květa	k1gFnSc1
Uherčíková	Uherčíková	k1gFnSc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Jirman	Jirman	k1gMnSc1
</s>
<s>
Michal	Michal	k1gMnSc1
Stanko	Stanko	k1gMnSc1
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Horák	Horák	k1gMnSc1
</s>
<s>
otec	otec	k1gMnSc1
Stanko	Stanko	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Holý	Holý	k1gMnSc1
</s>
<s>
starý	starý	k2eAgMnSc1d1
Šperl	Šperl	k1gMnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Logojdová	Logojdová	k1gFnSc1
</s>
<s>
Šperlova	Šperlův	k2eAgFnSc1d1
snacha	snacha	k1gFnSc1
Olga	Olga	k1gFnSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Vaněk	Vaněk	k1gMnSc1
</s>
<s>
Šperlův	Šperlův	k2eAgMnSc1d1
syn	syn	k1gMnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Vochoč	Vochoč	k1gMnSc1
</s>
<s>
náměstek	náměstek	k1gMnSc1
Nocárek	Nocárek	k1gMnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Fišar	Fišar	k1gMnSc1
</s>
<s>
Radim	Radim	k1gMnSc1
</s>
<s>
Svatopluk	Svatopluk	k1gMnSc1
Matyáš	Matyáš	k1gMnSc1
</s>
<s>
Rudyšar	Rudyšar	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Čapka	Čapka	k1gMnSc1
</s>
<s>
Ing.	ing.	kA
Remza	Remza	k1gFnSc1
</s>
<s>
Jana	Jana	k1gFnSc1
Postlerová	Postlerová	k1gFnSc1
</s>
<s>
matka	matka	k1gFnSc1
Pakostová	Pakostová	k1gFnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Kraus	Kraus	k1gMnSc1
</s>
<s>
Vilda	Vilda	k1gMnSc1
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Šmikmátor	Šmikmátor	k1gMnSc1
</s>
<s>
hajný	hajný	k1gMnSc1
Olexa	Olexa	k1gMnSc1
</s>
<s>
Miloslav	Miloslav	k1gMnSc1
Holub	Holub	k1gMnSc1
</s>
<s>
ředitel	ředitel	k1gMnSc1
mlékárny	mlékárna	k1gFnSc2
</s>
<s>
Milan	Milan	k1gMnSc1
Šulc	Šulc	k1gMnSc1
</s>
<s>
inspektor	inspektor	k1gMnSc1
Vorlíček	Vorlíček	k1gMnSc1
</s>
<s>
Stanislav	Stanislav	k1gMnSc1
Tříska	Tříska	k1gMnSc1
</s>
<s>
kuchař	kuchař	k1gMnSc1
potápěčů	potápěč	k1gMnPc2
Otíček	Otíček	k1gMnSc1
Ryšánek	Ryšánek	k1gMnSc1
</s>
<s>
Ivan	Ivan	k1gMnSc1
Dědeček	dědeček	k1gMnSc1
</s>
<s>
předseda	předseda	k1gMnSc1
MNV	MNV	kA
</s>
<s>
Seznam	seznam	k1gInSc1
dílů	díl	k1gInPc2
</s>
<s>
Bylo	být	k5eAaImAgNnS
natočeno	natočit	k5eAaBmNgNnS
a	a	k8xC
odvysíláno	odvysílat	k5eAaPmNgNnS
celkem	celkem	k6eAd1
9	#num#	k4
dílů	díl	k1gInPc2
seriálu	seriál	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
Útěk	útěk	k1gInSc1
</s>
<s>
Lov	lov	k1gInSc1
</s>
<s>
Štola	štola	k1gFnSc1
</s>
<s>
Kroužkování	kroužkování	k1gNnSc1
</s>
<s>
Kalná	kalný	k2eAgFnSc1d1
voda	voda	k1gFnSc1
</s>
<s>
Prsteny	prsten	k1gInPc1
</s>
<s>
Ponor	ponor	k1gInSc1
</s>
<s>
Nebezpečí	nebezpečí	k1gNnSc1
</s>
<s>
Povodeň	povodeň	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Velké	velký	k2eAgNnSc1d1
sedlo	sedlo	k1gNnSc1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Velké	velký	k2eAgNnSc1d1
sedlo	sedlo	k1gNnSc1
(	(	kIx(
<g/>
seriál	seriál	k1gInSc1
<g/>
)	)	kIx)
ve	v	k7c6
Filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Profil	profil	k1gInSc1
seriálu	seriál	k1gInSc2
včetně	včetně	k7c2
fotografií	fotografia	k1gFnPc2
z	z	k7c2
míst	místo	k1gNnPc2
natáčení	natáčení	k1gNnSc2
v	v	k7c6
databázi	databáze	k1gFnSc6
Filmová	filmový	k2eAgNnPc1d1
místa	místo	k1gNnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Televize	televize	k1gFnSc1
</s>
