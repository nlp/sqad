<s>
Triton	triton	k1gMnSc1	triton
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
Neptun	Neptun	k1gInSc1	Neptun
I	i	k9	i
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
měsíců	měsíc	k1gInPc2	měsíc
planety	planeta	k1gFnSc2	planeta
Neptun	Neptun	k1gMnSc1	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1846	[number]	k4	1846
britským	britský	k2eAgMnSc7d1	britský
astronomem	astronom	k1gMnSc7	astronom
Williamem	William	k1gInSc7	William
Lassellem	Lassell	k1gMnSc7	Lassell
<g/>
.	.	kIx.	.
</s>
