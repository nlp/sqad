<s>
Donald	Donald	k1gMnSc1	Donald
Arthur	Arthur	k1gMnSc1	Arthur
Glaser	Glaser	k1gMnSc1	Glaser
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
Cleveland	Cleveland	k1gInSc1	Cleveland
<g/>
,	,	kIx,	,
Ohio	Ohio	k1gNnSc1	Ohio
-	-	kIx~	-
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
Berkeley	Berkelea	k1gFnPc1	Berkelea
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
neurobiolog	neurobiolog	k1gMnSc1	neurobiolog
<g/>
.	.	kIx.	.
</s>
<s>
Proslavil	proslavit	k5eAaPmAgMnS	proslavit
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
vynálezem	vynález	k1gInSc7	vynález
bublinkové	bublinkový	k2eAgFnSc2d1	bublinková
komory	komora	k1gFnSc2	komora
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
obdržel	obdržet	k5eAaPmAgMnS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
