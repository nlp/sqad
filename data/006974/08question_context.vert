<s>
Polonium	polonium	k1gNnSc1	polonium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Po	Po	kA	Po
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
Polonium	polonium	k1gNnSc1	polonium
je	být	k5eAaImIp3nS	být
nestabilní	stabilní	k2eNgInSc4d1	nestabilní
radioaktivní	radioaktivní	k2eAgInSc4d1	radioaktivní
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
nejtěžší	těžký	k2eAgFnSc4d3	nejtěžší
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
chalkogenů	chalkogen	k1gInPc2	chalkogen
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1898	[number]	k4	1898
Marií	Maria	k1gFnSc7	Maria
Curie-Skłodowskou	Curie-Skłodowský	k2eAgFnSc7d1	Curie-Skłodowský
a	a	k8xC	a
pojmenován	pojmenovat	k5eAaPmNgMnS	pojmenovat
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
její	její	k3xOp3gFnSc2	její
vlasti	vlast	k1gFnSc2	vlast
-	-	kIx~	-
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
