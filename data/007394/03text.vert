<s>
Becherovka	becherovka	k1gFnSc1	becherovka
(	(	kIx(	(
<g/>
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Becherovka	becherovka	k1gFnSc1	becherovka
Original	Original	k1gFnPc7	Original
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgInSc1d1	tradiční
český	český	k2eAgInSc1d1	český
bylinný	bylinný	k2eAgInSc1d1	bylinný
likér	likér	k1gInSc1	likér
<g/>
,	,	kIx,	,
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
společností	společnost	k1gFnPc2	společnost
Jan	Jan	k1gMnSc1	Jan
Becher	Bechra	k1gFnPc2	Bechra
–	–	k?	–
Karlovarská	karlovarský	k2eAgFnSc1d1	Karlovarská
Becherovka	becherovka	k1gFnSc1	becherovka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
koncern	koncern	k1gInSc1	koncern
Pernod	Pernoda	k1gFnPc2	Pernoda
Ricard	Ricarda	k1gFnPc2	Ricarda
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvaceti	dvacet	k4xCc2	dvacet
druhů	druh	k1gInPc2	druh
bylin	bylina	k1gFnPc2	bylina
a	a	k8xC	a
koření	kořenit	k5eAaImIp3nS	kořenit
a	a	k8xC	a
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádné	žádný	k3yNgFnPc4	žádný
chemické	chemický	k2eAgFnPc4d1	chemická
přísady	přísada	k1gFnPc4	přísada
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
přezdívá	přezdívat	k5eAaImIp3nS	přezdívat
"	"	kIx"	"
<g/>
třináctý	třináctý	k4xOgInSc4	třináctý
karlovarský	karlovarský	k2eAgInSc4d1	karlovarský
pramen	pramen	k1gInSc4	pramen
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1805	[number]	k4	1805
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
do	do	k7c2	do
Karlových	Karlův	k2eAgInPc2d1	Karlův
Varů	Vary	k1gInPc2	Vary
doktor	doktor	k1gMnSc1	doktor
Christian	Christian	k1gMnSc1	Christian
Frobrig	Frobrig	k1gMnSc1	Frobrig
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
při	při	k7c6	při
odjezdu	odjezd	k1gInSc6	odjezd
Becherovi	Becher	k1gMnSc3	Becher
daruje	darovat	k5eAaPmIp3nS	darovat
recept	recept	k1gInSc4	recept
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1807	[number]	k4	1807
Josef	Josef	k1gMnSc1	Josef
Becher	Bechra	k1gFnPc2	Bechra
začíná	začínat	k5eAaImIp3nS	začínat
prodávat	prodávat	k5eAaImF	prodávat
likér	likér	k1gInSc4	likér
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
English	English	k1gMnSc1	English
Bitter	Bitter	k1gMnSc1	Bitter
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
je	být	k5eAaImIp3nS	být
vyrobeno	vyroben	k2eAgNnSc4d1	vyrobeno
cca	cca	kA	cca
8	[number]	k4	8
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
litrů	litr	k1gInPc2	litr
<g/>
.	.	kIx.	.
</s>
<s>
Becherovka	becherovka	k1gFnSc1	becherovka
je	být	k5eAaImIp3nS	být
momentálně	momentálně	k6eAd1	momentálně
nejprodávanějším	prodávaný	k2eAgInSc7d3	nejprodávanější
likérem	likér	k1gInSc7	likér
v	v	k7c6	v
segmentu	segment	k1gInSc6	segment
hořkých	hořký	k2eAgInPc2d1	hořký
bylinných	bylinný	k2eAgInPc2d1	bylinný
likérů	likér	k1gInPc2	likér
<g/>
.	.	kIx.	.
</s>
<s>
Becherovka	becherovka	k1gFnSc1	becherovka
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
podle	podle	k7c2	podle
tajné	tajný	k2eAgFnSc2d1	tajná
receptury	receptura	k1gFnSc2	receptura
<g/>
,	,	kIx,	,
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
bylin	bylina	k1gFnPc2	bylina
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Česka	Česko	k1gNnSc2	Česko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
se	se	k3xPyFc4	se
najdou	najít	k5eAaPmIp3nP	najít
také	také	k9	také
suroviny	surovina	k1gFnPc1	surovina
ze	z	k7c2	z
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
exotických	exotický	k2eAgFnPc2d1	exotická
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Zanzibaru	Zanzibar	k1gInSc2	Zanzibar
<g/>
,	,	kIx,	,
Srí	Srí	k1gFnSc2	Srí
Lanky	lanko	k1gNnPc7	lanko
či	či	k8xC	či
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
ale	ale	k8xC	ale
také	také	k9	také
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
zhruba	zhruba	k6eAd1	zhruba
tři	tři	k4xCgFnPc1	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
bylin	bylina	k1gFnPc2	bylina
a	a	k8xC	a
koření	koření	k1gNnSc2	koření
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
prý	prý	k9	prý
rostou	růst	k5eAaImIp3nP	růst
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Karlových	Karlův	k2eAgInPc2d1	Karlův
Varů	Vary	k1gInPc2	Vary
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
limitované	limitovaný	k2eAgFnSc2d1	limitovaná
edice	edice	k1gFnSc2	edice
2017	[number]	k4	2017
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Achillea	Achilleus	k1gMnSc2	Achilleus
millefolium	millefolium	k1gNnSc4	millefolium
<g/>
,	,	kIx,	,
Eugenia	Eugenium	k1gNnPc1	Eugenium
aromatica	aromatica	k1gFnSc1	aromatica
<g/>
,	,	kIx,	,
Cinnamomum	Cinnamomum	k1gInSc1	Cinnamomum
Aromaticum	Aromaticum	k1gInSc1	Aromaticum
a	a	k8xC	a
Hyssopus	Hyssopus	k1gInSc1	Hyssopus
Officinalis	Officinalis	k1gFnSc2	Officinalis
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
místnosti	místnost	k1gFnSc6	místnost
zvané	zvaný	k2eAgFnPc1d1	zvaná
"	"	kIx"	"
<g/>
Drogikamr	Drogikamr	k1gInSc1	Drogikamr
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
název	název	k1gInSc1	název
užívaný	užívaný	k2eAgInSc1d1	užívaný
ještě	ještě	k6eAd1	ještě
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
Becherů	Becher	k1gMnPc2	Becher
<g/>
)	)	kIx)	)
a	a	k8xC	a
trvá	trvat	k5eAaImIp3nS	trvat
několik	několik	k4yIc1	několik
týdnů	týden	k1gInPc2	týden
až	až	k8xS	až
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Hotová	hotový	k2eAgFnSc1d1	hotová
směs	směs	k1gFnSc1	směs
je	být	k5eAaImIp3nS	být
přesypána	přesypat	k5eAaPmNgFnS	přesypat
do	do	k7c2	do
pytlů	pytel	k1gInPc2	pytel
z	z	k7c2	z
přírodního	přírodní	k2eAgInSc2d1	přírodní
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
týden	týden	k1gInSc4	týden
ponořena	ponořen	k2eAgFnSc1d1	ponořena
do	do	k7c2	do
alkoholu	alkohol	k1gInSc2	alkohol
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dokonale	dokonale	k6eAd1	dokonale
prosákla	prosáknout	k5eAaPmAgFnS	prosáknout
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
extrakt	extrakt	k1gInSc1	extrakt
se	se	k3xPyFc4	se
smíchá	smíchat	k5eAaPmIp3nS	smíchat
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
přírodním	přírodní	k2eAgInSc7d1	přírodní
cukrem	cukr	k1gInSc7	cukr
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vkládá	vkládat	k5eAaImIp3nS	vkládat
do	do	k7c2	do
digeračních	digerační	k2eAgInPc2d1	digerační
tanků	tank	k1gInPc2	tank
<g/>
.	.	kIx.	.
</s>
<s>
Becherovka	becherovka	k1gFnSc1	becherovka
zraje	zrát	k5eAaImIp3nS	zrát
přibližně	přibližně	k6eAd1	přibližně
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
a	a	k8xC	a
poté	poté	k6eAd1	poté
je	být	k5eAaImIp3nS	být
stáčena	stáčen	k2eAgFnSc1d1	stáčena
do	do	k7c2	do
charakteristických	charakteristický	k2eAgFnPc2d1	charakteristická
zelených	zelený	k2eAgFnPc2d1	zelená
láhví	láhev	k1gFnPc2	láhev
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stočení	stočení	k1gNnSc6	stočení
ji	on	k3xPp3gFnSc4	on
degustuje	degustovat	k5eAaBmIp3nS	degustovat
hlavní	hlavní	k2eAgMnSc1d1	hlavní
sládek	sládek	k1gMnSc1	sládek
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
dodržuje	dodržovat	k5eAaImIp3nS	dodržovat
různá	různý	k2eAgNnPc4d1	různé
bezpečnostní	bezpečnostní	k2eAgNnPc4d1	bezpečnostní
opatření	opatření	k1gNnPc4	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Výrobní	výrobní	k2eAgFnSc1d1	výrobní
receptura	receptura	k1gFnSc1	receptura
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
pouze	pouze	k6eAd1	pouze
dvěma	dva	k4xCgFnPc7	dva
lidem	člověk	k1gMnPc3	člověk
ze	z	k7c2	z
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
výrobnímu	výrobní	k2eAgMnSc3d1	výrobní
manažerovi	manažer	k1gMnSc3	manažer
a	a	k8xC	a
výrobnímu	výrobní	k2eAgMnSc3d1	výrobní
řediteli	ředitel	k1gMnSc3	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
tajemstvím	tajemství	k1gNnSc7	tajemství
jsou	být	k5eAaImIp3nP	být
doživotně	doživotně	k6eAd1	doživotně
smluvně	smluvně	k6eAd1	smluvně
vázáni	vázat	k5eAaImNgMnP	vázat
a	a	k8xC	a
nemohou	moct	k5eNaImIp3nP	moct
například	například	k6eAd1	například
společně	společně	k6eAd1	společně
cestovat	cestovat	k5eAaImF	cestovat
<g/>
.	.	kIx.	.
</s>
<s>
Traduje	tradovat	k5eAaImIp3nS	tradovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
neobjednávají	objednávat	k5eNaImIp3nP	objednávat
všechny	všechen	k3xTgFnPc1	všechen
suroviny	surovina	k1gFnPc1	surovina
naráz	naráz	k6eAd1	naráz
a	a	k8xC	a
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
odebírány	odebírán	k2eAgInPc1d1	odebírán
i	i	k8xC	i
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
bylin	bylina	k1gFnPc2	bylina
a	a	k8xC	a
koření	koření	k1gNnSc2	koření
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
do	do	k7c2	do
likéru	likér	k1gInSc2	likér
vůbec	vůbec	k9	vůbec
nepatří	patřit	k5eNaImIp3nS	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Becherovka	becherovka	k1gFnSc1	becherovka
se	se	k3xPyFc4	se
momentálně	momentálně	k6eAd1	momentálně
vyváží	vyvážit	k5eAaPmIp3nS	vyvážit
do	do	k7c2	do
téměř	téměř	k6eAd1	téměř
40	[number]	k4	40
zemí	zem	k1gFnPc2	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejsilnější	silný	k2eAgInPc4d3	nejsilnější
exportní	exportní	k2eAgInPc4d1	exportní
trhy	trh	k1gInPc4	trh
patří	patřit	k5eAaImIp3nS	patřit
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
vyváží	vyvážit	k5eAaPmIp3nS	vyvážit
také	také	k9	také
do	do	k7c2	do
vzdálenějších	vzdálený	k2eAgFnPc2d2	vzdálenější
zemí	zem	k1gFnPc2	zem
–	–	k?	–
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc2	Brazílie
či	či	k8xC	či
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Servíruje	servírovat	k5eAaBmIp3nS	servírovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
aperitiv	aperitiv	k1gInSc1	aperitiv
nebo	nebo	k8xC	nebo
digestiv	digestit	k5eAaPmDgInS	digestit
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
koktejlu	koktejl	k1gInSc2	koktejl
či	či	k8xC	či
horkého	horký	k2eAgInSc2d1	horký
nápoje	nápoj	k1gInSc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
chuť	chuť	k1gFnSc1	chuť
a	a	k8xC	a
vůně	vůně	k1gFnPc1	vůně
nejlépe	dobře	k6eAd3	dobře
vyniknou	vyniknout	k5eAaPmIp3nP	vyniknout
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
servírována	servírovat	k5eAaBmNgFnS	servírovat
ledově	ledově	k6eAd1	ledově
namražená	namražený	k2eAgFnSc1d1	namražená
(	(	kIx(	(
<g/>
o	o	k7c6	o
teplotě	teplota	k1gFnSc6	teplota
−	−	k?	−
až	až	k8xS	až
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
doporučeno	doporučit	k5eAaPmNgNnS	doporučit
uchovávat	uchovávat	k5eAaImF	uchovávat
jí	on	k3xPp3gFnSc7	on
v	v	k7c6	v
lednici	lednice	k1gFnSc6	lednice
či	či	k8xC	či
mrazničce	mraznička	k1gFnSc6	mraznička
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
míchat	míchat	k5eAaImF	míchat
například	například	k6eAd1	například
s	s	k7c7	s
kolou	kola	k1gFnSc7	kola
nebo	nebo	k8xC	nebo
top-topicem	topopic	k1gMnSc7	top-topic
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
sovětského	sovětský	k2eAgInSc2d1	sovětský
konzulátu	konzulát	k1gInSc2	konzulát
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
svého	své	k1gNnSc2	své
času	čas	k1gInSc2	čas
pili	pít	k5eAaImAgMnP	pít
směs	směs	k1gFnSc4	směs
becherovky	becherovka	k1gFnSc2	becherovka
a	a	k8xC	a
tradiční	tradiční	k2eAgFnSc2d1	tradiční
ruské	ruský	k2eAgFnSc2d1	ruská
vodky	vodka	k1gFnSc2	vodka
Stolichnaya	Stolichnay	k1gInSc2	Stolichnay
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Becherovky	becherovka	k1gFnSc2	becherovka
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
připravit	připravit	k5eAaPmF	připravit
několik	několik	k4yIc4	několik
známých	známý	k2eAgInPc2d1	známý
koktejlů	koktejl	k1gInPc2	koktejl
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgInPc3d3	nejznámější
patří	patřit	k5eAaImIp3nP	patřit
Beton	beton	k1gInSc1	beton
<g/>
,	,	kIx,	,
Red	Red	k1gMnSc1	Red
Moon	Moon	k1gMnSc1	Moon
<g/>
,	,	kIx,	,
Beton	beton	k1gInSc1	beton
Bitter	Bitter	k1gInSc1	Bitter
<g/>
,	,	kIx,	,
Beton	beton	k1gInSc1	beton
s	s	k7c7	s
okurkou	okurka	k1gFnSc7	okurka
či	či	k8xC	či
retrostylový	retrostylový	k2eAgInSc1d1	retrostylový
Koospol	Koospol	k1gInSc1	Koospol
koktejl	koktejl	k1gInSc1	koktejl
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
známým	známý	k2eAgInSc7d1	známý
koktejlem	koktejl	k1gInSc7	koktejl
je	být	k5eAaImIp3nS	být
B-Celebration	B-Celebration	k1gInSc1	B-Celebration
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
připraven	připravit	k5eAaPmNgInS	připravit
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
oslav	oslava	k1gFnPc2	oslava
200	[number]	k4	200
let	léto	k1gNnPc2	léto
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
jehož	jehož	k3xOyRp3gMnSc7	jehož
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Aleš	Aleš	k1gMnSc1	Aleš
Půta	Půta	k1gMnSc1	Půta
<g/>
.	.	kIx.	.
</s>
<s>
Becherovka	becherovka	k1gFnSc1	becherovka
se	se	k3xPyFc4	se
také	také	k9	také
využívá	využívat	k5eAaImIp3nS	využívat
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
horkých	horký	k2eAgInPc2d1	horký
nápojů	nápoj	k1gInPc2	nápoj
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
je	být	k5eAaImIp3nS	být
Horká	horký	k2eAgFnSc1d1	horká
Becherovka	becherovka	k1gFnSc1	becherovka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
hojně	hojně	k6eAd1	hojně
využívána	využívat	k5eAaPmNgFnS	využívat
v	v	k7c6	v
barmanské	barmanský	k2eAgFnSc6d1	barmanská
mixologii	mixologie	k1gFnSc6	mixologie
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Barmani	barman	k1gMnPc1	barman
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
připravuji	připravovat	k5eAaImIp1nS	připravovat
tradiční	tradiční	k2eAgInPc4d1	tradiční
koktejly	koktejl	k1gInPc4	koktejl
a	a	k8xC	a
horké	horký	k2eAgInPc4d1	horký
nápoje	nápoj	k1gInPc4	nápoj
a	a	k8xC	a
využívají	využívat	k5eAaImIp3nP	využívat
ji	on	k3xPp3gFnSc4	on
k	k	k7c3	k
dalším	další	k2eAgMnPc3d1	další
experimentům	experiment	k1gInPc3	experiment
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mixologie	mixologie	k1gFnSc2	mixologie
<g/>
.	.	kIx.	.
</s>
<s>
Becherovka	becherovka	k1gFnSc1	becherovka
získala	získat	k5eAaPmAgFnS	získat
desítky	desítka	k1gFnPc4	desítka
českých	český	k2eAgFnPc2d1	Česká
i	i	k8xC	i
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
1871	[number]	k4	1871
<g/>
:	:	kIx,	:
stříbrná	stříbrnat	k5eAaImIp3nS	stříbrnat
medaile	medaile	k1gFnSc1	medaile
<g/>
,	,	kIx,	,
Výstava	výstava	k1gFnSc1	výstava
řemesel	řemeslo	k1gNnPc2	řemeslo
<g/>
,	,	kIx,	,
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
lesnictví	lesnictví	k1gNnSc2	lesnictví
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
(	(	kIx(	(
<g/>
úplně	úplně	k6eAd1	úplně
první	první	k4xOgNnSc1	první
ocenění	ocenění	k1gNnSc1	ocenění
<g/>
)	)	kIx)	)
1904	[number]	k4	1904
<g/>
:	:	kIx,	:
jmenování	jmenování	k1gNnSc4	jmenování
Becherovky	becherovka	k1gFnSc2	becherovka
dvorním	dvorní	k2eAgMnSc7d1	dvorní
dodavatelem	dodavatel	k1gMnSc7	dodavatel
císařského	císařský	k2eAgInSc2d1	císařský
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
putovalo	putovat	k5eAaImAgNnS	putovat
pravidelně	pravidelně	k6eAd1	pravidelně
na	na	k7c4	na
císařský	císařský	k2eAgInSc4d1	císařský
dvůr	dvůr	k1gInSc4	dvůr
až	až	k9	až
50	[number]	k4	50
litrů	litr	k1gInPc2	litr
Becherovky	becherovka	k1gFnSc2	becherovka
měsíčně	měsíčně	k6eAd1	měsíčně
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
<g/>
:	:	kIx,	:
Rodinné	rodinný	k2eAgNnSc1d1	rodinné
stříbro	stříbro	k1gNnSc1	stříbro
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
stříbrná	stříbrnat	k5eAaImIp3nS	stříbrnat
medaile	medaile	k1gFnSc1	medaile
<g/>
,	,	kIx,	,
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
World	World	k1gMnSc1	World
Spirits	Spiritsa	k1gFnPc2	Spiritsa
Competition	Competition	k1gInSc1	Competition
(	(	kIx(	(
<g/>
jediný	jediný	k2eAgMnSc1d1	jediný
český	český	k2eAgMnSc1d1	český
zástupce	zástupce	k1gMnSc1	zástupce
<g/>
)	)	kIx)	)
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
zlatá	zlatý	k2eAgFnSc1d1	zlatá
medaile	medaile	k1gFnSc1	medaile
<g/>
,	,	kIx,	,
Internationaler	Internationaler	k1gInSc1	Internationaler
Spirituosen	Spirituosen	k2eAgInSc1d1	Spirituosen
Wettbewerb	Wettbewerb	k1gInSc1	Wettbewerb
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
Známým	známá	k1gFnPc3	známá
fandou	fandou	k?	fandou
karlovarské	karlovarský	k2eAgFnSc2d1	Karlovarská
Becherovky	becherovka	k1gFnSc2	becherovka
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
prezident	prezident	k1gMnSc1	prezident
Miloš	Miloš	k1gMnSc1	Miloš
Zeman	Zeman	k1gMnSc1	Zeman
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
své	svůj	k3xOyFgFnSc2	svůj
návštěvy	návštěva	k1gFnSc2	návštěva
karlovarské	karlovarský	k2eAgFnSc2d1	Karlovarská
likérky	likérka	k1gFnSc2	likérka
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
přiznal	přiznat	k5eAaPmAgMnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
jako	jako	k8xC	jako
premiér	premiér	k1gMnSc1	premiér
jsem	být	k5eAaImIp1nS	být
byl	být	k5eAaImAgMnS	být
neplaceným	placený	k2eNgMnSc7d1	neplacený
agentem	agent	k1gMnSc7	agent
becherovky	becherovka	k1gFnSc2	becherovka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vždycky	vždycky	k6eAd1	vždycky
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
mě	já	k3xPp1nSc4	já
ptali	ptat	k5eAaImAgMnP	ptat
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
nejraději	rád	k6eAd3	rád
piju	pít	k5eAaImIp1nS	pít
<g/>
,	,	kIx,	,
vždycky	vždycky	k6eAd1	vždycky
jsem	být	k5eAaImIp1nS	být
řekl	říct	k5eAaPmAgMnS	říct
becherovku	becherovka	k1gFnSc4	becherovka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
zjištěna	zjištěn	k2eAgFnSc1d1	zjištěna
cukrovka	cukrovka	k1gFnSc1	cukrovka
<g/>
,	,	kIx,	,
přešel	přejít	k5eAaPmAgMnS	přejít
na	na	k7c4	na
slivovici	slivovice	k1gFnSc4	slivovice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dle	dle	k7c2	dle
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
zůstal	zůstat	k5eAaPmAgInS	zůstat
becherovce	becherovka	k1gFnSc6	becherovka
věrný	věrný	k2eAgMnSc1d1	věrný
<g/>
.	.	kIx.	.
</s>
