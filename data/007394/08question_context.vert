<s>
Becherovka	becherovka	k1gFnSc1
(	(	kIx(
<g/>
celým	celý	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Becherovka	becherovka	k1gFnSc1
Original	Original	k1gFnPc7
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
tradiční	tradiční	k2eAgInSc1d1
český	český	k2eAgInSc1d1
bylinný	bylinný	k2eAgInSc1d1
likér	likér	k1gInSc1
<g/>
,	,	kIx,
vyráběný	vyráběný	k2eAgInSc1d1
v	v	k7c6
Karlových	Karlův	k2eAgInPc6d1
Varech	Vary	k1gInPc6
společností	společnost	k1gFnPc2
Jan	Jan	k1gMnSc1
Becher	Bechra	k1gFnPc2
–	–	k?
Karlovarská	karlovarský	k2eAgFnSc1d1
Becherovka	becherovka	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4
vlastní	vlastnit	k5eAaImIp3nS
koncern	koncern	k1gInSc1
Pernod	Pernoda	k1gFnPc2
Ricard	Ricarda	k1gFnPc2
<g/>
.	.	kIx.
</s>