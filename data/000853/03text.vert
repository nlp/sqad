<s>
Wolfram	wolfram	k1gInSc1	wolfram
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
W	W	kA	W
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Wolframium	Wolframium	k1gNnSc1	Wolframium
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Tungsten	Tungsten	k2eAgMnSc1d1	Tungsten
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
šedý	šedý	k2eAgInSc1d1	šedý
až	až	k9	až
stříbřitě	stříbřitě	k6eAd1	stříbřitě
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
těžký	těžký	k2eAgInSc1d1	těžký
a	a	k8xC	a
mimořádně	mimořádně	k6eAd1	mimořádně
obtížně	obtížně	k6eAd1	obtížně
tavitelný	tavitelný	k2eAgInSc1d1	tavitelný
kov	kov	k1gInSc1	kov
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc1	jeho
teplota	teplota	k1gFnSc1	teplota
tání	tání	k1gNnSc2	tání
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
po	po	k7c6	po
uhlíku	uhlík	k1gInSc6	uhlík
druhá	druhý	k4xOgFnSc1	druhý
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
z	z	k7c2	z
prvků	prvek	k1gInPc2	prvek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
uplatnění	uplatnění	k1gNnSc1	uplatnění
nalézá	nalézat	k5eAaImIp3nS	nalézat
jako	jako	k9	jako
složka	složka	k1gFnSc1	složka
různých	různý	k2eAgFnPc2d1	různá
slitin	slitina	k1gFnPc2	slitina
<g/>
,	,	kIx,	,
v	v	k7c6	v
čisté	čistý	k2eAgFnSc6d1	čistá
formě	forma	k1gFnSc6	forma
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
běžně	běžně	k6eAd1	běžně
setkáváme	setkávat	k5eAaImIp1nP	setkávat
jako	jako	k9	jako
s	s	k7c7	s
materiálem	materiál	k1gInSc7	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
žárovkových	žárovkový	k2eAgNnPc2d1	žárovkové
vláken	vlákno	k1gNnPc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Wolfram	wolfram	k1gInSc1	wolfram
byl	být	k5eAaImAgInS	být
objeven	objeven	k2eAgInSc1d1	objeven
roku	rok	k1gInSc2	rok
1781	[number]	k4	1781
švédským	švédský	k2eAgMnSc7d1	švédský
chemikem	chemik	k1gMnSc7	chemik
Carlem	Carl	k1gMnSc7	Carl
Wilhelmem	Wilhelm	k1gMnSc7	Wilhelm
Scheelem	Scheel	k1gMnSc7	Scheel
<g/>
.	.	kIx.	.
</s>
<s>
Izolován	izolovat	k5eAaBmNgInS	izolovat
byl	být	k5eAaImAgInS	být
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1783	[number]	k4	1783
<g/>
.	.	kIx.	.
</s>
<s>
Izolovali	izolovat	k5eAaBmAgMnP	izolovat
ho	on	k3xPp3gMnSc4	on
Juan	Juan	k1gMnSc1	Juan
Jose	Jose	k1gNnSc2	Jose
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Elhuayar	Elhuayar	k1gMnSc1	Elhuayar
a	a	k8xC	a
Fausto	Fausta	k1gMnSc5	Fausta
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Elhuayar	Elhuayar	k1gMnSc1	Elhuayar
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1555	[number]	k4	1555
používá	používat	k5eAaImIp3nS	používat
rektor	rektor	k1gMnSc1	rektor
latinské	latinský	k2eAgFnSc2d1	Latinská
školy	škola	k1gFnSc2	škola
Johannes	Johannes	k1gMnSc1	Johannes
Mathesius	Mathesius	k1gMnSc1	Mathesius
v	v	k7c6	v
Jáchymově	Jáchymov	k1gInSc6	Jáchymov
pro	pro	k7c4	pro
šedý	šedý	k2eAgInSc4d1	šedý
<g/>
,	,	kIx,	,
obtížně	obtížně	k6eAd1	obtížně
tavitelný	tavitelný	k2eAgInSc1d1	tavitelný
kov	kov	k1gInSc1	kov
název	název	k1gInSc1	název
wolforma	wolforma	k1gFnSc1	wolforma
nebo	nebo	k8xC	nebo
wolfshaar	wolfshaar	k1gInSc1	wolfshaar
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
vlčí	vlčí	k2eAgInSc4d1	vlčí
vlas	vlas	k1gInSc4	vlas
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Wolfram	wolfram	k1gInSc1	wolfram
je	být	k5eAaImIp3nS	být
šedý	šedý	k2eAgInSc1d1	šedý
až	až	k9	až
stříbřitě	stříbřitě	k6eAd1	stříbřitě
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
,	,	kIx,	,
mimořádně	mimořádně	k6eAd1	mimořádně
obtížně	obtížně	k6eAd1	obtížně
tavitelný	tavitelný	k2eAgInSc1d1	tavitelný
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
bod	bod	k1gInSc1	bod
tavení	tavení	k1gNnSc2	tavení
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
kovových	kovový	k2eAgInPc2d1	kovový
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hustota	hustota	k1gFnSc1	hustota
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
některé	některý	k3yIgInPc4	některý
drahé	drahý	k2eAgInPc4d1	drahý
kovy	kov	k1gInPc4	kov
jako	jako	k8xC	jako
např.	např.	kA	např.
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
platina	platina	k1gFnSc1	platina
<g/>
,	,	kIx,	,
iridium	iridium	k1gNnSc1	iridium
a	a	k8xC	a
osmium	osmium	k1gNnSc1	osmium
jsou	být	k5eAaImIp3nP	být
těžší	těžký	k2eAgInPc1d2	těžší
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc2	tento
vlastnosti	vlastnost	k1gFnSc2	vlastnost
je	být	k5eAaImIp3nS	být
využíváno	využívat	k5eAaImNgNnS	využívat
při	při	k7c6	při
falšování	falšování	k1gNnSc6	falšování
zlatých	zlatý	k2eAgFnPc2d1	zlatá
cihel	cihla	k1gFnPc2	cihla
(	(	kIx(	(
<g/>
slitků	slitek	k1gInPc2	slitek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
slitku	slitek	k1gInSc2	slitek
jsou	být	k5eAaImIp3nP	být
vyvrtány	vyvrtán	k2eAgInPc1d1	vyvrtán
otvory	otvor	k1gInPc1	otvor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
zaplněny	zaplnit	k5eAaPmNgInP	zaplnit
wolframem	wolfram	k1gInSc7	wolfram
a	a	k8xC	a
následně	následně	k6eAd1	následně
zality	zalít	k5eAaPmNgInP	zalít
zlatem	zlato	k1gNnSc7	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
fyzického	fyzický	k2eAgNnSc2d1	fyzické
poškození	poškození	k1gNnSc2	poškození
pak	pak	k6eAd1	pak
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
falzifikát	falzifikát	k1gInSc4	falzifikát
odhalit	odhalit	k5eAaPmF	odhalit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
supernízkých	supernízký	k2eAgFnPc2d1	supernízká
teplot	teplota	k1gFnPc2	teplota
pod	pod	k7c4	pod
0,0012	[number]	k4	0,0012
K	k	k7c3	k
je	být	k5eAaImIp3nS	být
supravodičem	supravodič	k1gInSc7	supravodič
I	i	k8xC	i
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Chemicky	chemicky	k6eAd1	chemicky
je	být	k5eAaImIp3nS	být
kovový	kovový	k2eAgInSc1d1	kovový
wolfram	wolfram	k1gInSc1	wolfram
velmi	velmi	k6eAd1	velmi
stálý	stálý	k2eAgInSc1d1	stálý
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
netečný	netečný	k2eAgMnSc1d1	netečný
k	k	k7c3	k
působení	působení	k1gNnSc3	působení
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
atmosférických	atmosférický	k2eAgInPc2d1	atmosférický
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
odolává	odolávat	k5eAaImIp3nS	odolávat
působení	působení	k1gNnSc1	působení
většiny	většina	k1gFnSc2	většina
běžných	běžný	k2eAgFnPc2d1	běžná
minerálních	minerální	k2eAgFnPc2d1	minerální
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
a	a	k8xC	a
halogeny	halogen	k1gInPc7	halogen
reaguje	reagovat	k5eAaBmIp3nS	reagovat
až	až	k9	až
za	za	k7c2	za
značně	značně	k6eAd1	značně
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
rozpouštění	rozpouštění	k1gNnSc4	rozpouštění
je	být	k5eAaImIp3nS	být
nejúčinnější	účinný	k2eAgFnSc1d3	nejúčinnější
směs	směs	k1gFnSc1	směs
kyseliny	kyselina	k1gFnSc2	kyselina
dusičné	dusičný	k2eAgFnSc2d1	dusičná
a	a	k8xC	a
kyseliny	kyselina	k1gFnSc2	kyselina
fluorovodíkové	fluorovodíkový	k2eAgFnSc2d1	fluorovodíková
<g/>
.	.	kIx.	.
</s>
<s>
Nejsnáze	snadno	k6eAd3	snadno
se	se	k3xPyFc4	se
kovový	kovový	k2eAgInSc1d1	kovový
wolfram	wolfram	k1gInSc1	wolfram
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
alkalickým	alkalický	k2eAgNnSc7d1	alkalické
tavením	tavení	k1gNnSc7	tavení
například	například	k6eAd1	například
se	se	k3xPyFc4	se
směsí	směs	k1gFnSc7	směs
dusičnanu	dusičnan	k1gInSc2	dusičnan
draselného	draselný	k2eAgInSc2d1	draselný
a	a	k8xC	a
hydroxidu	hydroxid	k1gInSc2	hydroxid
sodného	sodný	k2eAgInSc2d1	sodný
(	(	kIx(	(
<g/>
KNO	KNO	kA	KNO
<g/>
3	[number]	k4	3
+	+	kIx~	+
NaOH	NaOH	k1gFnSc2	NaOH
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
různých	různý	k2eAgFnPc2d1	různá
mocenství	mocenství	k1gNnSc4	mocenství
od	od	k7c2	od
WII	WII	kA	WII
<g/>
+	+	kIx~	+
a	a	k8xC	a
po	po	k7c6	po
WVI	WVI	kA	WVI
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
sloučeniny	sloučenina	k1gFnPc1	sloučenina
WVI	WVI	kA	WVI
<g/>
+	+	kIx~	+
jsou	být	k5eAaImIp3nP	být
nejstálejší	stálý	k2eAgFnPc1d3	nejstálejší
a	a	k8xC	a
nejvíce	nejvíce	k6eAd1	nejvíce
prakticky	prakticky	k6eAd1	prakticky
využívané	využívaný	k2eAgFnPc1d1	využívaná
<g/>
.	.	kIx.	.
</s>
<s>
Wolfram	wolfram	k1gInSc1	wolfram
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
poměrně	poměrně	k6eAd1	poměrně
vzácný	vzácný	k2eAgInSc4d1	vzácný
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
obsah	obsah	k1gInSc1	obsah
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
1,5	[number]	k4	1,5
<g/>
-	-	kIx~	-
<g/>
34	[number]	k4	34
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
wolfram	wolfram	k1gInSc1	wolfram
nachází	nacházet	k5eAaImIp3nS	nacházet
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
koncentraci	koncentrace	k1gFnSc6	koncentrace
0,0001	[number]	k4	0,0001
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaImIp3nS	připadat
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
wolframu	wolfram	k1gInSc2	wolfram
na	na	k7c4	na
300	[number]	k4	300
miliard	miliarda	k4xCgFnPc2	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
minerály	minerál	k1gInPc7	minerál
wolframu	wolfram	k1gInSc2	wolfram
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
jsou	být	k5eAaImIp3nP	být
wolframit	wolframit	k1gInSc4	wolframit
-	-	kIx~	-
wolframan	wolframan	k1gInSc4	wolframan
železnato-manganatý	železnatoanganatý	k2eAgInSc4d1	železnato-manganatý
(	(	kIx(	(
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
,	,	kIx,	,
<g/>
Mn	Mn	k1gFnSc1	Mn
<g/>
)	)	kIx)	)
<g/>
WO	WO	kA	WO
<g/>
4	[number]	k4	4
(	(	kIx(	(
<g/>
přechodný	přechodný	k2eAgMnSc1d1	přechodný
člen	člen	k1gMnSc1	člen
řady	řada	k1gFnSc2	řada
ferberit	ferberit	k1gInSc1	ferberit
FeWO	FeWO	k1gFnSc4	FeWO
<g/>
4	[number]	k4	4
hübneritové	hübneritový	k2eAgFnSc6d1	hübneritový
MnWO	MnWO	k1gFnSc6	MnWO
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
wolframan	wolframan	k1gInSc1	wolframan
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
<g/>
,	,	kIx,	,
scheelit	scheelit	k1gInSc1	scheelit
CaWO	CaWO	k1gFnSc1	CaWO
<g/>
4	[number]	k4	4
a	a	k8xC	a
stolzit	stolzit	k1gInSc1	stolzit
<g/>
,	,	kIx,	,
wolframan	wolframan	k1gInSc1	wolframan
olovnatý	olovnatý	k2eAgInSc1d1	olovnatý
<g/>
,	,	kIx,	,
PbWO	PbWO	k1gFnSc1	PbWO
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
metalurgické	metalurgický	k2eAgFnSc6d1	metalurgická
výrobě	výroba	k1gFnSc6	výroba
wolframu	wolfram	k1gInSc2	wolfram
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nejprve	nejprve	k6eAd1	nejprve
mechanicky	mechanicky	k6eAd1	mechanicky
separují	separovat	k5eAaBmIp3nP	separovat
těžké	těžký	k2eAgFnPc4d1	těžká
frakce	frakce	k1gFnPc4	frakce
rudy	ruda	k1gFnSc2	ruda
a	a	k8xC	a
výsledný	výsledný	k2eAgInSc1d1	výsledný
koncentrát	koncentrát	k1gInSc1	koncentrát
se	se	k3xPyFc4	se
taví	tavit	k5eAaImIp3nS	tavit
s	s	k7c7	s
hydroxidem	hydroxid	k1gInSc7	hydroxid
sodným	sodný	k2eAgInSc7d1	sodný
(	(	kIx(	(
<g/>
NaOH	NaOH	k1gMnSc6	NaOH
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tavenina	tavenina	k1gFnSc1	tavenina
se	se	k3xPyFc4	se
louží	loužit	k5eAaImIp3nS	loužit
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
přechází	přecházet	k5eAaImIp3nS	přecházet
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
wolframan	wolframan	k1gInSc1	wolframan
sodný	sodný	k2eAgInSc1d1	sodný
<g/>
,	,	kIx,	,
Na	na	k7c4	na
<g/>
2	[number]	k4	2
<g/>
WO	WO	kA	WO
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Okyselením	okyselení	k1gNnSc7	okyselení
tohoto	tento	k3xDgInSc2	tento
roztoku	roztok	k1gInSc2	roztok
vypadává	vypadávat	k5eAaImIp3nS	vypadávat
sraženina	sraženina	k1gFnSc1	sraženina
hydratovaného	hydratovaný	k2eAgInSc2d1	hydratovaný
oxidu	oxid	k1gInSc2	oxid
wolframového	wolframový	k2eAgInSc2d1	wolframový
WO	WO	kA	WO
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Čistý	čistý	k2eAgInSc1d1	čistý
wolfram	wolfram	k1gInSc1	wolfram
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
molybden	molybden	k1gInSc1	molybden
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
získá	získat	k5eAaPmIp3nS	získat
redukcí	redukce	k1gFnSc7	redukce
oxidu	oxid	k1gInSc2	oxid
wolframového	wolframový	k2eAgInSc2d1	wolframový
vodíkem	vodík	k1gInSc7	vodík
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
850	[number]	k4	850
°	°	k?	°
<g/>
C	C	kA	C
<g/>
:	:	kIx,	:
WO3	WO3	k1gMnSc1	WO3
+	+	kIx~	+
3	[number]	k4	3
H2	H2	k1gFnSc2	H2
→	→	k?	→
W	W	kA	W
+	+	kIx~	+
3	[number]	k4	3
H2O	H2O	k1gFnPc2	H2O
Praktické	praktický	k2eAgNnSc1d1	praktické
použití	použití	k1gNnSc1	použití
wolframu	wolfram	k1gInSc2	wolfram
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
od	od	k7c2	od
jeho	jeho	k3xOp3gFnSc2	jeho
vysoké	vysoký	k2eAgFnSc2d1	vysoká
hustoty	hustota	k1gFnSc2	hustota
a	a	k8xC	a
obtížné	obtížný	k2eAgFnSc2d1	obtížná
tavitelnosti	tavitelnost	k1gFnSc2	tavitelnost
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
setkáme	setkat	k5eAaPmIp1nP	setkat
jako	jako	k9	jako
s	s	k7c7	s
materiálem	materiál	k1gInSc7	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
žárovkových	žárovkový	k2eAgNnPc2d1	žárovkové
vláken	vlákno	k1gNnPc2	vlákno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgInSc1d1	schopen
po	po	k7c6	po
tisíce	tisíc	k4xCgInPc1	tisíc
pracovních	pracovní	k2eAgFnPc2d1	pracovní
hodin	hodina	k1gFnPc2	hodina
snášet	snášet	k5eAaImF	snášet
teploty	teplota	k1gFnPc4	teplota
značně	značně	k6eAd1	značně
přes	přes	k7c4	přes
1000	[number]	k4	1000
°	°	k?	°
<g/>
C.	C.	kA	C.
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
teploty	teplota	k1gFnSc2	teplota
vlákno	vlákno	k1gNnSc1	vlákno
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
průchodem	průchod	k1gInSc7	průchod
elektrického	elektrický	k2eAgInSc2d1	elektrický
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
prostor	prostor	k1gInSc1	prostor
žárovky	žárovka	k1gFnSc2	žárovka
je	být	k5eAaImIp3nS	být
naplněn	naplnit	k5eAaPmNgInS	naplnit
inertním	inertní	k2eAgInSc7d1	inertní
plynem	plyn	k1gInSc7	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
wolfram	wolfram	k1gInSc1	wolfram
totiž	totiž	k9	totiž
není	být	k5eNaImIp3nS	být
natolik	natolik	k6eAd1	natolik
inertní	inertní	k2eAgNnSc1d1	inertní
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
za	za	k7c2	za
těchto	tento	k3xDgFnPc2	tento
podmínek	podmínka	k1gFnPc2	podmínka
nedocházelo	docházet	k5eNaImAgNnS	docházet
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
oxidaci	oxidace	k1gFnSc6	oxidace
vzdušným	vzdušný	k2eAgInSc7d1	vzdušný
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
elektrotechnice	elektrotechnika	k1gFnSc6	elektrotechnika
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
materiál	materiál	k1gInSc4	materiál
pro	pro	k7c4	pro
anodu	anoda	k1gFnSc4	anoda
(	(	kIx(	(
<g/>
terčík	terčík	k1gInSc4	terčík
<g/>
)	)	kIx)	)
rentgenky	rentgenka	k1gFnSc2	rentgenka
<g/>
.	.	kIx.	.
</s>
<s>
Wolfram	wolfram	k1gInSc1	wolfram
má	mít	k5eAaImIp3nS	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
elektronovou	elektronový	k2eAgFnSc4d1	elektronová
hustotu	hustota	k1gFnSc4	hustota
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
dopadající	dopadající	k2eAgInPc1d1	dopadající
elektrony	elektron	k1gInPc1	elektron
jsou	být	k5eAaImIp3nP	být
velkou	velký	k2eAgFnSc7d1	velká
odpudivou	odpudivý	k2eAgFnSc7d1	odpudivá
silou	síla	k1gFnSc7	síla
prudce	prudko	k6eAd1	prudko
brzděny	brzdit	k5eAaImNgInP	brzdit
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
zákonitostí	zákonitost	k1gFnPc2	zákonitost
elektrodynamiky	elektrodynamika	k1gFnSc2	elektrodynamika
část	část	k1gFnSc4	část
jejich	jejich	k3xOp3gFnSc2	jejich
kinetické	kinetický	k2eAgFnSc2d1	kinetická
energie	energie	k1gFnSc2	energie
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c4	v
brzdné	brzdný	k2eAgNnSc4d1	brzdné
elektromagnetické	elektromagnetický	k2eAgNnSc4d1	elektromagnetické
záření	záření	k1gNnSc4	záření
-	-	kIx~	-
fotony	foton	k1gInPc1	foton
X-záření	Xáření	k1gNnSc2	X-záření
Při	při	k7c6	při
svařování	svařování	k1gNnSc6	svařování
kovů	kov	k1gInPc2	kov
elektrickým	elektrický	k2eAgInSc7d1	elektrický
obloukem	oblouk	k1gInSc7	oblouk
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
wolframových	wolframový	k2eAgFnPc2d1	wolframová
elektrod	elektroda	k1gFnPc2	elektroda
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
metoda	metoda	k1gFnSc1	metoda
TIG	TIG	kA	TIG
<g/>
,	,	kIx,	,
tungsten	tungsten	k2eAgInSc1d1	tungsten
inert	inert	k1gInSc1	inert
gas	gas	k?	gas
<g/>
)	)	kIx)	)
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
elektrický	elektrický	k2eAgInSc4d1	elektrický
proud	proud	k1gInSc4	proud
procházející	procházející	k2eAgInSc4d1	procházející
mezi	mezi	k7c7	mezi
elektrodami	elektroda	k1gFnPc7	elektroda
v	v	k7c6	v
inertní	inertní	k2eAgFnSc6d1	inertní
atmosféře	atmosféra	k1gFnSc6	atmosféra
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
argon	argon	k1gInSc1	argon
<g/>
)	)	kIx)	)
roztavení	roztavení	k1gNnSc1	roztavení
zpracovávaných	zpracovávaný	k2eAgInPc2d1	zpracovávaný
kovů	kov	k1gInPc2	kov
bez	bez	k7c2	bez
úbytku	úbytek	k1gInSc2	úbytek
materiálu	materiál	k1gInSc2	materiál
elektrod	elektroda	k1gFnPc2	elektroda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
slitinách	slitina	k1gFnPc6	slitina
se	se	k3xPyFc4	se
přísada	přísada	k1gFnSc1	přísada
wolframu	wolfram	k1gInSc2	wolfram
projeví	projevit	k5eAaPmIp3nS	projevit
především	především	k9	především
zvýšením	zvýšení	k1gNnSc7	zvýšení
tvrdosti	tvrdost	k1gFnSc2	tvrdost
a	a	k8xC	a
mechanické	mechanický	k2eAgFnSc2d1	mechanická
i	i	k8xC	i
tepelné	tepelný	k2eAgFnSc2d1	tepelná
odolnosti	odolnost	k1gFnSc2	odolnost
<g/>
.	.	kIx.	.
</s>
<s>
Rychlořezné	rychlořezný	k2eAgFnPc4d1	rychlořezná
oceli	ocel	k1gFnPc4	ocel
nabízené	nabízený	k2eAgFnPc4d1	nabízená
pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
Stellite	Stellit	k1gInSc5	Stellit
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
až	až	k9	až
18	[number]	k4	18
%	%	kIx~	%
wolframu	wolfram	k1gInSc2	wolfram
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gInPc6	on
kovoobráběcí	kovoobráběcí	k2eAgInPc1d1	kovoobráběcí
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
vrtné	vrtný	k2eAgFnPc1d1	vrtná
hlavice	hlavice	k1gFnPc1	hlavice
geologických	geologický	k2eAgInPc2d1	geologický
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
lopatky	lopatka	k1gFnSc2	lopatka
parních	parní	k2eAgFnPc2d1	parní
turbín	turbína	k1gFnPc2	turbína
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
vysoce	vysoce	k6eAd1	vysoce
teplotně	teplotně	k6eAd1	teplotně
a	a	k8xC	a
mechanicky	mechanicky	k6eAd1	mechanicky
namáhané	namáhaný	k2eAgFnPc1d1	namáhaná
součástky	součástka	k1gFnPc1	součástka
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
vysoké	vysoký	k2eAgFnSc3d1	vysoká
hustotě	hustota	k1gFnSc3	hustota
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
materiál	materiál	k1gInSc1	materiál
penetračních	penetrační	k2eAgInPc2d1	penetrační
projektilů	projektil	k1gInPc2	projektil
(	(	kIx(	(
<g/>
penetrátorů	penetrátor	k1gInPc2	penetrátor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgFnP	používat
již	již	k6eAd1	již
od	od	k7c2	od
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
pro	pro	k7c4	pro
prorážení	prorážení	k1gNnSc4	prorážení
pancíře	pancíř	k1gInSc2	pancíř
tanků	tank	k1gInPc2	tank
<g/>
,	,	kIx,	,
stěn	stěna	k1gFnPc2	stěna
bunkrů	bunkr	k1gInPc2	bunkr
a	a	k8xC	a
opevnění	opevnění	k1gNnSc2	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Pseudoslitiny	Pseudoslitin	k1gInPc1	Pseudoslitin
wolframu	wolfram	k1gInSc2	wolfram
(	(	kIx(	(
<g/>
s	s	k7c7	s
niklem	nikl	k1gInSc7	nikl
<g/>
,	,	kIx,	,
železem	železo	k1gNnSc7	železo
a	a	k8xC	a
kobaltem	kobalt	k1gInSc7	kobalt
<g/>
,	,	kIx,	,
obsah	obsah	k1gInSc4	obsah
wolframu	wolfram	k1gInSc2	wolfram
91	[number]	k4	91
<g/>
-	-	kIx~	-
<g/>
96	[number]	k4	96
hm	hm	k?	hm
<g/>
.	.	kIx.	.
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
vyrobené	vyrobený	k2eAgNnSc1d1	vyrobené
práškovou	práškový	k2eAgFnSc7d1	prášková
metalurgií	metalurgie	k1gFnSc7	metalurgie
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
dobré	dobrý	k2eAgFnSc3d1	dobrá
schopnosti	schopnost	k1gFnSc3	schopnost
odstínit	odstínit	k5eAaPmF	odstínit
rentgenové	rentgenový	k2eAgNnSc4d1	rentgenové
záření	záření	k1gNnSc4	záření
a	a	k8xC	a
záření	záření	k1gNnSc1	záření
gama	gama	k1gNnSc1	gama
jako	jako	k8xS	jako
materiál	materiál	k1gInSc1	materiál
pro	pro	k7c4	pro
radiační	radiační	k2eAgNnSc4d1	radiační
stínění	stínění	k1gNnSc4	stínění
např.	např.	kA	např.
v	v	k7c6	v
kobaltových	kobaltový	k2eAgInPc6d1	kobaltový
ozařovačích	ozařovač	k1gInPc6	ozařovač
<g/>
,	,	kIx,	,
používaných	používaný	k2eAgInPc2d1	používaný
k	k	k7c3	k
ozařování	ozařování	k1gNnSc3	ozařování
zhoubných	zhoubný	k2eAgInPc2d1	zhoubný
nádorů	nádor	k1gInPc2	nádor
<g/>
.	.	kIx.	.
</s>
<s>
Wolfram	wolfram	k1gInSc1	wolfram
tvoří	tvořit	k5eAaImIp3nS	tvořit
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejstálejší	stálý	k2eAgMnPc1d3	nejstálejší
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
oxidační	oxidační	k2eAgNnSc4d1	oxidační
číslo	číslo	k1gNnSc4	číslo
VI	VI	kA	VI
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
dále	daleko	k6eAd2	daleko
oxidační	oxidační	k2eAgNnSc4d1	oxidační
číslo	číslo	k1gNnSc4	číslo
II	II	kA	II
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
III	III	kA	III
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
IV	IV	kA	IV
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
V	V	kA	V
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Praktický	praktický	k2eAgInSc4d1	praktický
význam	význam	k1gInSc4	význam
nalézají	nalézat	k5eAaImIp3nP	nalézat
jeho	jeho	k3xOp3gFnPc4	jeho
sloučeniny	sloučenina	k1gFnPc1	sloučenina
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
katalyzátorů	katalyzátor	k1gInPc2	katalyzátor
pro	pro	k7c4	pro
petrochemický	petrochemický	k2eAgInSc4d1	petrochemický
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
různých	různý	k2eAgInPc2d1	různý
barevných	barevný	k2eAgInPc2d1	barevný
pigmentů	pigment	k1gInPc2	pigment
a	a	k8xC	a
teplotně	teplotně	k6eAd1	teplotně
odolných	odolný	k2eAgInPc2d1	odolný
lubrikantů	lubrikant	k1gInPc2	lubrikant
a	a	k8xC	a
maziv	mazivo	k1gNnPc2	mazivo
(	(	kIx(	(
<g/>
sulfidy	sulfid	k1gInPc1	sulfid
wolframu	wolfram	k1gInSc2	wolfram
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
oxidů	oxid	k1gInPc2	oxid
wolframu	wolfram	k1gInSc2	wolfram
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
oxid	oxid	k1gInSc1	oxid
wolframový	wolframový	k2eAgMnSc1d1	wolframový
WO3	WO3	k1gMnSc1	WO3
a	a	k8xC	a
oxid	oxid	k1gInSc1	oxid
wolframičitý	wolframičitý	k2eAgInSc1d1	wolframičitý
<g/>
,	,	kIx,	,
WO	WO	kA	WO
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
sloučeninou	sloučenina	k1gFnSc7	sloučenina
wolframu	wolfram	k1gInSc2	wolfram
je	být	k5eAaImIp3nS	být
kyselina	kyselina	k1gFnSc1	kyselina
wolframová	wolframový	k2eAgFnSc1d1	wolframová
<g/>
,	,	kIx,	,
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
WO	WO	kA	WO
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
buď	buď	k8xC	buď
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
soli	sůl	k1gFnPc1	sůl
<g/>
,	,	kIx,	,
wolframany	wolframan	k1gInPc1	wolframan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
značně	značně	k6eAd1	značně
složitých	složitý	k2eAgFnPc2d1	složitá
komplexních	komplexní	k2eAgFnPc2d1	komplexní
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Technicky	technicky	k6eAd1	technicky
důležitými	důležitý	k2eAgFnPc7d1	důležitá
sloučeninami	sloučenina	k1gFnPc7	sloučenina
wolframu	wolfram	k1gInSc2	wolfram
jsou	být	k5eAaImIp3nP	být
karbidy	karbid	k1gInPc1	karbid
o	o	k7c4	o
složení	složení	k1gNnSc4	složení
WC	WC	kA	WC
a	a	k8xC	a
W	W	kA	W
<g/>
2	[number]	k4	2
<g/>
C.	C.	kA	C.
Vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
se	se	k3xPyFc4	se
mimořádnou	mimořádný	k2eAgFnSc7d1	mimořádná
tvrdostí	tvrdost	k1gFnSc7	tvrdost
a	a	k8xC	a
využívají	využívat	k5eAaImIp3nP	využívat
se	se	k3xPyFc4	se
jako	jako	k9	jako
součásti	součást	k1gFnPc1	součást
brusiv	brusivo	k1gNnPc2	brusivo
pro	pro	k7c4	pro
kovoobrábění	kovoobrábění	k1gNnPc4	kovoobrábění
a	a	k8xC	a
geologické	geologický	k2eAgFnPc4d1	geologická
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
připravit	připravit	k5eAaPmF	připravit
například	například	k6eAd1	například
redukcí	redukce	k1gFnSc7	redukce
oxidu	oxid	k1gInSc2	oxid
wolframového	wolframový	k2eAgInSc2d1	wolframový
uhlíkem	uhlík	k1gInSc7	uhlík
<g/>
:	:	kIx,	:
WO3	WO3	k1gFnSc1	WO3
+	+	kIx~	+
4	[number]	k4	4
C	C	kA	C
→	→	k?	→
WC	WC	kA	WC
+	+	kIx~	+
3	[number]	k4	3
CO	co	k8xS	co
Díky	díky	k7c3	díky
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnSc3d1	nízká
rozpustnosti	rozpustnost	k1gFnSc3	rozpustnost
wolframu	wolfram	k1gInSc2	wolfram
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
obsah	obsah	k1gInSc1	obsah
v	v	k7c6	v
živých	živý	k2eAgInPc6d1	živý
organizmech	organizmus	k1gInPc6	organizmus
velmi	velmi	k6eAd1	velmi
nízký	nízký	k2eAgMnSc1d1	nízký
a	a	k8xC	a
wolfram	wolfram	k1gInSc1	wolfram
rozhodně	rozhodně	k6eAd1	rozhodně
nepatří	patřit	k5eNaImIp3nS	patřit
mezi	mezi	k7c4	mezi
biogenní	biogenní	k2eAgInPc4d1	biogenní
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
nedostatek	nedostatek	k1gInSc1	nedostatek
ve	v	k7c6	v
stravě	strava	k1gFnSc6	strava
výrazně	výrazně	k6eAd1	výrazně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
fyziologický	fyziologický	k2eAgInSc4d1	fyziologický
stav	stav	k1gInSc4	stav
organizmu	organizmus	k1gInSc2	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
wolfram	wolfram	k1gInSc4	wolfram
obsažený	obsažený	k2eAgInSc4d1	obsažený
v	v	k7c6	v
tkáních	tkáň	k1gFnPc6	tkáň
živých	živý	k2eAgInPc2d1	živý
organizmů	organizmus	k1gInPc2	organizmus
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
molybden	molybden	k1gInSc4	molybden
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
jeho	jeho	k3xOp3gFnSc1	jeho
role	role	k1gFnSc1	role
v	v	k7c6	v
enzymatickém	enzymatický	k2eAgInSc6d1	enzymatický
systému	systém	k1gInSc6	systém
oxidoreduktázy	oxidoreduktáza	k1gFnSc2	oxidoreduktáza
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
nejsou	být	k5eNaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
by	by	kYmCp3nS	by
přebytek	přebytek	k1gInSc1	přebytek
wolframu	wolfram	k1gInSc2	wolfram
v	v	k7c6	v
životním	životní	k2eAgNnSc6d1	životní
prostředí	prostředí	k1gNnSc6	prostředí
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
negativně	negativně	k6eAd1	negativně
ovlivňoval	ovlivňovat	k5eAaImAgMnS	ovlivňovat
lidské	lidský	k2eAgNnSc4d1	lidské
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
enzymů	enzym	k1gInPc2	enzym
hypertermofilních	hypertermofilní	k2eAgFnPc2d1	hypertermofilní
archeí	arche	k1gFnPc2	arche
je	být	k5eAaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
wolfram	wolfram	k1gInSc1	wolfram
využívat	využívat	k5eAaImF	využívat
místo	místo	k1gNnSc4	místo
molybdenu	molybden	k1gInSc2	molybden
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
aktivních	aktivní	k2eAgNnPc6d1	aktivní
centrech	centrum	k1gNnPc6	centrum
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
enzymy	enzym	k1gInPc1	enzym
ovšem	ovšem	k9	ovšem
dokáží	dokázat	k5eAaPmIp3nP	dokázat
využívat	využívat	k5eAaImF	využívat
výhradně	výhradně	k6eAd1	výhradně
wolfram	wolfram	k1gInSc4	wolfram
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
je	on	k3xPp3gInPc4	on
nahradit	nahradit	k5eAaPmF	nahradit
molybdenem	molybden	k1gInSc7	molybden
nebo	nebo	k8xC	nebo
vanadem	vanad	k1gInSc7	vanad
<g/>
.	.	kIx.	.
</s>
