<s>
Obština	Obština	k1gFnSc1
Madan	Madana	k1gFnPc2
</s>
<s>
Obština	Obština	k1gFnSc1
Madan	Madana	k1gFnPc2
О	О	k?
М	М	k?
Obština	Obštin	k2eAgMnSc2d1
na	na	k7c6
mapě	mapa	k1gFnSc6
BulharskaPoloha	BulharskaPoloh	k1gMnSc2
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
41	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
24	#num#	k4
<g/>
°	°	k?
<g/>
57	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Stát	stát	k1gInSc1
</s>
<s>
Bulharsko	Bulharsko	k1gNnSc1
Bulharsko	Bulharsko	k1gNnSc1
oblast	oblast	k1gFnSc4
</s>
<s>
Smoljanská	Smoljanský	k2eAgFnSc1d1
Administrativní	administrativní	k2eAgFnSc1d1
dělení	dělení	k1gNnSc1
</s>
<s>
1	#num#	k4
město	město	k1gNnSc1
a	a	k8xC
43	#num#	k4
vesnice	vesnice	k1gFnSc2
</s>
<s>
Obštiny	Obština	k1gFnPc1
ve	v	k7c6
Smoljanské	Smoljanský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
174,95	174,95	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
12	#num#	k4
276	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
70,2	70,2	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Fachri	Fachri	k6eAd1
Molajsenov	Molajsenov	k1gInSc1
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.madan.bg	www.madan.bg	k1gMnSc1
PSČ	PSČ	kA
</s>
<s>
4900	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Obština	Obština	k1gFnSc1
Madan	Madan	k1gFnSc1
(	(	kIx(
<g/>
bulharsky	bulharsky	k6eAd1
О	О	k?
М	М	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bulharská	bulharský	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
územní	územní	k2eAgFnSc2d1
samosprávy	samospráva	k1gFnSc2
ve	v	k7c6
Smoljanské	Smoljanský	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
v	v	k7c6
jižním	jižní	k2eAgNnSc6d1
Bulharsku	Bulharsko	k1gNnSc6
v	v	k7c6
jihozápadní	jihozápadní	k2eAgFnSc6d1
části	část	k1gFnSc6
Východních	východní	k2eAgInPc2d1
Rodopů	Rodop	k1gInPc2
<g/>
,	,	kIx,
jen	jen	k9
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
Západních	západní	k2eAgInPc6d1
Rodopech	Rodop	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správním	správní	k2eAgNnSc7d1
střediskem	středisko	k1gNnSc7
je	být	k5eAaImIp3nS
město	město	k1gNnSc1
Madan	Madan	k1gFnSc1
<g/>
,	,	kIx,
kromě	kromě	k7c2
něj	on	k3xPp3gMnSc2
zahrnuje	zahrnovat	k5eAaImIp3nS
obština	obština	k1gFnSc1
43	#num#	k4
vesnice	vesnice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
přes	přes	k7c4
11	#num#	k4
tisíc	tisíc	k4xCgInPc2
stálých	stálý	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Sídla	sídlo	k1gNnPc1
</s>
<s>
Arpadžik	Arpadžik	k1gMnSc1
</s>
<s>
Borika	Borika	k1gFnSc1
</s>
<s>
Borinovo	Borinův	k2eAgNnSc1d1
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Borovina	borovina	k1gFnSc1
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bukova	Bukův	k2eAgFnSc1d1
Poljana	Poljana	k1gFnSc1
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bukovo	Bukovo	k1gNnSc1
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Cirka	Cirka	k1gFnSc1
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čurka	čurka	k1gFnSc1
</s>
<s>
Diralo	Dirat	k5eAaImAgNnS,k5eAaBmAgNnS,k5eAaPmAgNnS
</s>
<s>
Dolie	Dolie	k1gFnSc1
</s>
<s>
Gabrina	Gabrina	k1gFnSc1
</s>
<s>
Galište	Gališit	k5eAaPmRp2nP,k5eAaImRp2nP
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kasapsko	Kasapsko	k6eAd1
</s>
<s>
Koriite	Koriit	k1gMnSc5
</s>
<s>
Krajna	Krajna	k6eAd1
</s>
<s>
Krušev	Krušet	k5eAaPmDgMnS
Dol	dol	k1gInSc1
</s>
<s>
Kupen	kupen	k2eAgInSc1d1
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Leska	Leska	k1gFnSc1
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Leštak	Leštak	k1gInSc1
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Livade	Livást	k5eAaPmIp3nS
</s>
<s>
Lovci	lovec	k1gMnPc1
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Madan	Madan	k1gInSc1
<g/>
[	[	kIx(
<g/>
p	p	k?
2	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
sídlo	sídlo	k1gNnSc1
správy	správa	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Măglišta	Măglišta	k1gFnSc1
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mile	mile	k6eAd1
</s>
<s>
Mitovska	Mitovsko	k1gNnPc4
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pečinska	Pečinsko	k1gNnPc1
</s>
<s>
Petrov	Petrov	k1gInSc1
Dol	dol	k1gInSc1
</s>
<s>
Planinci	Planinec	k1gMnPc1
</s>
<s>
Ravnil	Ravnit	k5eAaPmAgInS,k5eAaImAgInS
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ravništa	Ravništa	k1gFnSc1
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ravno	Ravno	k1gNnSc1
Nivište	Nivište	k1gFnSc2
</s>
<s>
Rustan	Rustan	k1gInSc1
</s>
<s>
Srednogorci	Srednogorek	k1gMnPc1
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Stajčin	Stajčin	k2eAgInSc1d1
Dol	dol	k1gInSc1
</s>
<s>
Studena	studeno	k1gNnPc4
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Šarenska	Šarensko	k1gNnPc4
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tănkoto	Tănkota	k1gFnSc5
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Uručevci	Uručevek	k1gMnPc1
</s>
<s>
Vărba	Vărba	k1gFnSc1
</s>
<s>
Vărbina	Vărbina	k1gFnSc1
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vărgov	Vărgov	k1gInSc1
Dol	dol	k1gInSc1
</s>
<s>
Vechtino	Vechtin	k2eAgNnSc1d1
<g/>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Visokite	Visokit	k1gMnSc5
</s>
<s>
Vraninci	Vraninec	k1gMnPc1
</s>
<s>
Sousední	sousední	k2eAgInPc1d1
obštiny	obštin	k1gInPc1
</s>
<s>
Banite	Banit	k1gMnSc5
</s>
<s>
Ardino	Ardino	k1gNnSc1
</s>
<s>
Smoljan	Smoljan	k1gMnSc1
</s>
<s>
Nedelino	Nedelin	k2eAgNnSc1d1
</s>
<s>
Obština	Obština	k1gFnSc1
Madan	Madana	k1gFnPc2
</s>
<s>
Rudozem	Rudoz	k1gInSc7
</s>
<s>
Zlatograd	Zlatograd	k1gInSc1
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
V	v	k7c6
obštině	obština	k1gFnSc6
žije	žít	k5eAaImIp3nS
11	#num#	k4
176	#num#	k4
stálých	stálý	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
včetně	včetně	k7c2
přechodně	přechodně	k6eAd1
hlášených	hlášený	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
13	#num#	k4
0	#num#	k4
<g/>
32	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
sčítáni	sčítat	k5eAaImNgMnP
1	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2011	#num#	k4
bylo	být	k5eAaImAgNnS
národnostní	národnostní	k2eAgNnSc1d1
složení	složení	k1gNnSc1
následující	následující	k2eAgMnSc1d1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
p	p	k?
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bulhaři	Bulhar	k1gMnPc1
<g/>
:	:	kIx,
7	#num#	k4
230	#num#	k4
(	(	kIx(
<g/>
84.5	84.5	k4
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
Turci	Turek	k1gMnPc1
<g/>
:	:	kIx,
661	#num#	k4
(	(	kIx(
<g/>
7.7	7.7	k4
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
ostatní	ostatní	k2eAgMnPc1d1
<g/>
:	:	kIx,
663	#num#	k4
(	(	kIx(
<g/>
7.8	7.8	k4
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
13	#num#	k4
14	#num#	k4
15	#num#	k4
16	#num#	k4
17	#num#	k4
18	#num#	k4
19	#num#	k4
20	#num#	k4
Samosprávné	samosprávný	k2eAgNnSc4d1
sídlo	sídlo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Město	město	k1gNnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jsou	být	k5eAaImIp3nP
uvedeni	uvést	k5eAaPmNgMnP
pouze	pouze	k6eAd1
ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
národnost	národnost	k1gFnSc4
deklarovali	deklarovat	k5eAaBmAgMnP
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Н	Н	k?
м	м	k?
<g/>
,	,	kIx,
п	п	k?
и	и	k?
в	в	k?
<g/>
;	;	kIx,
О	О	k?
<g/>
:	:	kIx,
С	С	k?
О	О	k?
<g/>
:	:	kIx,
М	М	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sofie	Sofie	k1gFnSc1
<g/>
:	:	kIx,
Н	Н	k?
с	с	k?
и	и	k?
<g/>
,	,	kIx,
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
bulharsky	bulharsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
М	М	k?
т	т	k?
н	н	k?
н	н	k?
п	п	k?
п	п	k?
и	и	k?
н	н	k?
а	а	k?
(	(	kIx(
<g/>
п	п	k?
о	о	k?
и	и	k?
о	о	k?
<g/>
)	)	kIx)
з	з	k?
т	т	k?
г	г	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sofie	Sofie	k1gFnSc1
<g/>
:	:	kIx,
Г	Г	k?
Д	Д	k?
<g/>
,	,	kIx,
Г	Г	k?
Р	Р	k?
и	и	k?
А	А	k?
О	О	k?
<g/>
,	,	kIx,
2015-12-15	2015-12-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
bulharsky	bulharsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Н	Н	k?
П	П	k?
О	О	k?
<g/>
,	,	kIx,
О	О	k?
<g/>
,	,	kIx,
Н	Н	k?
М	М	k?
И	И	k?
С	С	k?
П	П	k?
Е	Е	k?
П	П	k?
К	К	k?
1.02	1.02	k4
<g/>
.2011	.2011	k4
Г	Г	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sofie	Sofie	k1gFnSc1
<g/>
:	:	kIx,
Н	Н	k?
с	с	k?
и	и	k?
<g/>
,	,	kIx,
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
bulharsky	bulharsky	k6eAd1
<g/>
)	)	kIx)
</s>
