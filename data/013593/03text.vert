<s>
Robert	Robert	k1gMnSc1
Oppenheimer	Oppenheimer	k1gMnSc1
</s>
<s>
Robert	Robert	k1gMnSc1
Oppenheimer	Oppenheimer	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1904	#num#	k4
<g/>
New	New	k1gMnSc2
York	York	k1gInSc4
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1967	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
62	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Princeton	Princeton	k1gInSc1
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
karcinom	karcinom	k1gInSc1
plic	plíce	k1gFnPc2
Bydliště	bydliště	k1gNnSc2
</s>
<s>
PrincetonBerkeleyLos	PrincetonBerkeleyLos	k1gMnSc1
AlamosNew	AlamosNew	k1gFnSc2
York	York	k1gInSc1
Národnost	národnost	k1gFnSc4
</s>
<s>
Židé	Žid	k1gMnPc1
a	a	k8xC
Aškenázové	Aškenázové	k2eAgFnSc1d1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc2
</s>
<s>
Kristova	Kristův	k2eAgFnSc1d1
kolejEthical	kolejEthicat	k5eAaPmAgInS
Culture	Cultur	k1gMnSc5
Fieldston	Fieldston	k1gInSc1
SchoolJesus	SchoolJesus	k1gMnSc1
CollegeUniverzita	CollegeUniverzita	k1gFnSc1
v	v	k7c6
GöttingenuHarvardova	GöttingenuHarvardův	k2eAgFnSc1d1
univerzitaCavendishova	univerzitaCavendishova	k1gFnSc1
laboratořHarvard	laboratořHarvarda	k1gFnPc2
College	Colleg	k1gMnSc2
Povolání	povolání	k1gNnSc2
</s>
<s>
teoretický	teoretický	k2eAgMnSc1d1
fyzik	fyzik	k1gMnSc1
<g/>
,	,	kIx,
inženýr	inženýr	k1gMnSc1
<g/>
,	,	kIx,
jaderný	jaderný	k2eAgMnSc1d1
fyzik	fyzik	k1gMnSc1
<g/>
,	,	kIx,
sběratel	sběratel	k1gMnSc1
umění	umění	k1gNnSc2
a	a	k8xC
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
Zaměstnavatelé	zaměstnavatel	k1gMnPc1
</s>
<s>
Kalifornská	kalifornský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
v	v	k7c6
BerkeleyUniverzita	BerkeleyUniverzita	k1gFnSc1
v	v	k7c4
CambridgiKalifornský	CambridgiKalifornský	k2eAgInSc4d1
technologický	technologický	k2eAgInSc4d1
institut	institut	k1gInSc4
Ocenění	ocenění	k1gNnSc1
</s>
<s>
medaile	medaile	k1gFnPc4
Za	za	k7c4
zásluhy	zásluha	k1gFnPc4
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
)	)	kIx)
<g/>
Richtmyer	Richtmyer	k1gMnSc1
Memorial	Memorial	k1gMnSc1
Award	Award	k1gMnSc1
(	(	kIx(
<g/>
1947	#num#	k4
<g/>
)	)	kIx)
<g/>
Prix	Prix	k1gInSc1
des	des	k1gNnSc1
trois	trois	k1gFnPc2
physiciens	physiciensa	k1gFnPc2
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
)	)	kIx)
<g/>
rytíř	rytíř	k1gMnSc1
Čestné	čestný	k2eAgFnSc2d1
legie	legie	k1gFnSc2
(	(	kIx(
<g/>
1958	#num#	k4
<g/>
)	)	kIx)
<g/>
Nessim-Habif	Nessim-Habif	k1gMnSc1
Award	Award	k1gMnSc1
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Katherine	Katherin	k1gInSc5
Oppenheimer	Oppenheimer	k1gInSc4
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Frank	Frank	k1gMnSc1
Oppenheimer	Oppenheimer	k1gMnSc1
(	(	kIx(
<g/>
sourozenec	sourozenec	k1gMnSc1
<g/>
)	)	kIx)
Podpis	podpis	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
J.	J.	kA
Robert	Robert	k1gMnSc1
Oppenheimer	Oppenheimer	k1gMnSc1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
Julius	Julius	k1gMnSc1
Robert	Robert	k1gMnSc1
Oppenheimer	Oppenheimero	k1gNnPc2
nebo	nebo	k8xC
Jacob	Jacob	k1gMnSc1
Robert	Robert	k1gMnSc1
Oppenheimer	Oppenheimer	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1904	#num#	k4
–	–	k?
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1967	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
americký	americký	k2eAgMnSc1d1
teoretický	teoretický	k2eAgMnSc1d1
fyzik	fyzik	k1gMnSc1
<g/>
,	,	kIx,
nejznámější	známý	k2eAgMnSc1d3
svou	svůj	k3xOyFgFnSc7
účastí	účast	k1gFnSc7
v	v	k7c6
projektu	projekt	k1gInSc6
Manhattan	Manhattan	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
řídil	řídit	k5eAaImAgMnS
vývoj	vývoj	k1gInSc4
první	první	k4xOgFnSc2
jaderné	jaderný	k2eAgFnSc2d1
zbraně	zbraň	k1gFnSc2
v	v	k7c6
tajné	tajný	k2eAgFnSc6d1
laboratoři	laboratoř	k1gFnSc6
v	v	k7c4
Los	los	k1gInSc4
Alamos	Alamos	k1gFnPc6
v	v	k7c6
Novém	nový	k2eAgNnSc6d1
Mexiku	Mexiko	k1gNnSc6
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývá	bývat	k5eAaImIp3nS
označován	označovat	k5eAaImNgInS
jako	jako	k9
„	„	k?
<g/>
otec	otec	k1gMnSc1
atomové	atomový	k2eAgFnSc2d1
bomby	bomba	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
starším	starý	k2eAgMnSc7d2
bratrem	bratr	k1gMnSc7
dalšího	další	k1gNnSc2
fyzika	fyzik	k1gMnSc2
Franka	Frank	k1gMnSc2
Oppenheimera	Oppenheimer	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Studia	studio	k1gNnPc1
a	a	k8xC
začátek	začátek	k1gInSc1
kariéry	kariéra	k1gFnSc2
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1
rodiče	rodič	k1gMnPc1
pocházeli	pocházet	k5eAaImAgMnP
z	z	k7c2
německého	německý	k2eAgNnSc2d1
města	město	k1gNnSc2
Hanau	Hanaus	k1gInSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1888	#num#	k4
emigrovali	emigrovat	k5eAaBmAgMnP
do	do	k7c2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otec	otec	k1gMnSc1
byl	být	k5eAaImAgMnS
bohatý	bohatý	k2eAgMnSc1d1
židovský	židovský	k2eAgMnSc1d1
importér	importér	k1gMnSc1
textilu	textil	k1gInSc2
<g/>
,	,	kIx,
matka	matka	k1gFnSc1
absolvovala	absolvovat	k5eAaPmAgFnS
malířské	malířský	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
v	v	k7c6
Paříži	Paříž	k1gFnSc6
a	a	k8xC
později	pozdě	k6eAd2
vlastnila	vlastnit	k5eAaImAgFnS
ateliér	ateliér	k1gInSc4
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Robert	Robert	k1gMnSc1
Oppenheimer	Oppenheimer	k1gMnSc1
studoval	studovat	k5eAaImAgMnS
na	na	k7c6
Harvardově	Harvardův	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
fyziku	fyzika	k1gFnSc4
a	a	k8xC
chemii	chemie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovládal	ovládat	k5eAaImAgMnS
několik	několik	k4yIc4
jazyků	jazyk	k1gInPc2
včetně	včetně	k7c2
latiny	latina	k1gFnSc2
a	a	k8xC
řečtiny	řečtina	k1gFnSc2
<g/>
,	,	kIx,
psal	psát	k5eAaImAgMnS
poezii	poezie	k1gFnSc4
a	a	k8xC
zabýval	zabývat	k5eAaImAgInS
se	se	k3xPyFc4
filosofií	filosofie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
studiu	studio	k1gNnSc6
pokračoval	pokračovat	k5eAaImAgInS
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Cambridge	Cambridge	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
ho	on	k3xPp3gNnSc4
zaujala	zaujmout	k5eAaPmAgFnS
především	především	k6eAd1
teorie	teorie	k1gFnSc1
molekul	molekula	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doktorskou	doktorský	k2eAgFnSc4d1
práci	práce	k1gFnSc4
psal	psát	k5eAaImAgMnS
na	na	k7c6
univerzitě	univerzita	k1gFnSc6
v	v	k7c6
Göttingenu	Göttingen	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
působilo	působit	k5eAaImAgNnS
mnoho	mnoho	k4c1
známých	známý	k2eAgMnPc2d1
vědců	vědec	k1gMnPc2
<g/>
:	:	kIx,
Werner	Werner	k1gMnSc1
Heisenberg	Heisenberg	k1gMnSc1
<g/>
,	,	kIx,
Max	Max	k1gMnSc1
Born	Born	k1gMnSc1
<g/>
,	,	kIx,
Wolfgang	Wolfgang	k1gMnSc1
Pauli	Paule	k1gFnSc4
<g/>
,	,	kIx,
Enrico	Enrico	k6eAd1
Fermi	Fer	k1gFnPc7
<g/>
,	,	kIx,
Eduard	Eduard	k1gMnSc1
Teller	Teller	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
Pod	pod	k7c7
vedením	vedení	k1gNnSc7
Maxe	Max	k1gMnSc2
Borna	Born	k1gMnSc2
vytvořil	vytvořit	k5eAaPmAgInS
několik	několik	k4yIc4
vědeckých	vědecký	k2eAgFnPc2d1
prací	práce	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
vyniká	vynikat	k5eAaImIp3nS
zejména	zejména	k9
studie	studie	k1gFnSc1
věnovaná	věnovaný	k2eAgFnSc1d1
kvantové	kvantový	k2eAgFnSc3d1
teorii	teorie	k1gFnSc3
molekul	molekula	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
měl	mít	k5eAaImAgMnS
již	již	k9
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
kontě	konto	k1gNnSc6
devět	devět	k4xCc1
vědeckých	vědecký	k2eAgFnPc2d1
prací	práce	k1gFnPc2
<g/>
,	,	kIx,
publikovaných	publikovaný	k2eAgInPc2d1
během	během	k7c2
necelých	celý	k2eNgNnPc2d1
dvou	dva	k4xCgNnPc2
let	léto	k1gNnPc2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
evropského	evropský	k2eAgInSc2d1
pobytu	pobyt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgNnSc2
období	období	k1gNnSc2
je	být	k5eAaImIp3nS
i	i	k9
jeho	jeho	k3xOp3gFnSc1
slavná	slavný	k2eAgFnSc1d1
práce	práce	k1gFnSc1
o	o	k7c6
kvantové	kvantový	k2eAgFnSc6d1
teorii	teorie	k1gFnSc6
molekul	molekula	k1gFnPc2
<g/>
,	,	kIx,
napsaná	napsaný	k2eAgFnSc1d1
společně	společně	k6eAd1
s	s	k7c7
Bornem	Born	k1gInSc7
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
je	být	k5eAaImIp3nS
navržena	navrhnout	k5eAaPmNgFnS
dnes	dnes	k6eAd1
známá	známý	k2eAgFnSc1d1
Bornova	Bornův	k2eAgFnSc1d1
<g/>
–	–	k?
<g/>
Oppenheimerova	Oppenheimerův	k2eAgFnSc1d1
aproximace	aproximace	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
r.	r.	kA
1928	#num#	k4
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
USA	USA	kA
a	a	k8xC
na	na	k7c6
Kalifornské	kalifornský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
v	v	k7c4
Berkeley	Berkele	k1gMnPc4
a	a	k8xC
Kalifornském	kalifornský	k2eAgInSc6d1
technologickém	technologický	k2eAgInSc6d1
institutu	institut	k1gInSc6
v	v	k7c6
Pasadeně	Pasadena	k1gFnSc6
se	se	k3xPyFc4
věnoval	věnovat	k5eAaImAgInS,k5eAaPmAgInS
otázkám	otázka	k1gFnPc3
kvantové	kvantový	k2eAgFnSc2d1
elektrodynamiky	elektrodynamika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
Miltonem	Milton	k1gInSc7
Plessetem	Plesset	k1gMnSc7
podal	podat	k5eAaPmAgMnS
roku	rok	k1gInSc2
1933	#num#	k4
jako	jako	k9
první	první	k4xOgInSc4
správný	správný	k2eAgInSc4d1
popis	popis	k1gInSc4
mechanismu	mechanismus	k1gInSc2
tvorby	tvorba	k1gFnSc2
párů	pár	k1gInPc2
elektron	elektron	k1gInSc1
<g/>
–	–	k?
<g/>
pozitron	pozitron	k1gInSc1
vlivem	vlivem	k7c2
gama	gama	k1gNnSc2
paprsků	paprsek	k1gInPc2
a	a	k8xC
kvantitativně	kvantitativně	k6eAd1
vysvětlil	vysvětlit	k5eAaPmAgMnS
absorpci	absorpce	k1gFnSc4
záření	záření	k1gNnSc2
γ	γ	k?
v	v	k7c6
těžkých	těžký	k2eAgInPc6d1
prvcích	prvek	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
vlivem	vliv	k1gInSc7
školy	škola	k1gFnSc2
Ernesta	Ernest	k1gMnSc2
Lawrence	Lawrenec	k1gMnSc2
v	v	k7c4
Berkeley	Berkele	k1gMnPc4
se	se	k3xPyFc4
věnoval	věnovat	k5eAaPmAgMnS,k5eAaImAgMnS
i	i	k9
jaderné	jaderný	k2eAgFnSc3d1
fyzice	fyzika	k1gFnSc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
v	v	k7c6
letech	léto	k1gNnPc6
1933-34	1933-34	k4
studoval	studovat	k5eAaImAgMnS
deuteronové	deuteronový	k2eAgFnPc4d1
reakce	reakce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
se	se	k3xPyFc4
jeho	jeho	k3xOp3gInSc1
zájem	zájem	k1gInSc1
obrátil	obrátit	k5eAaPmAgInS
na	na	k7c4
kosmické	kosmický	k2eAgNnSc4d1
záření	záření	k1gNnSc4
a	a	k8xC
teorii	teorie	k1gFnSc4
mezonových	mezonový	k2eAgFnPc2d1
částic	částice	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vykonával	vykonávat	k5eAaImAgMnS
také	také	k9
pedagogickou	pedagogický	k2eAgFnSc4d1
praxi	praxe	k1gFnSc4
a	a	k8xC
na	na	k7c4
jeho	jeho	k3xOp3gFnPc4
přednášky	přednáška	k1gFnPc4
se	se	k3xPyFc4
sjížděli	sjíždět	k5eAaImAgMnP
studenti	student	k1gMnPc1
z	z	k7c2
celých	celý	k2eAgInPc2d1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studenty	student	k1gMnPc4
díky	díky	k7c3
svému	svůj	k3xOyFgInSc3
enormnímu	enormní	k2eAgInSc3d1
rozsahu	rozsah	k1gInSc3
vědomostí	vědomost	k1gFnPc2
a	a	k8xC
schopnosti	schopnost	k1gFnSc2
vědecké	vědecký	k2eAgFnSc2d1
syntézy	syntéza	k1gFnSc2
vedl	vést	k5eAaImAgInS
k	k	k7c3
univerzálnímu	univerzální	k2eAgNnSc3d1
pojetí	pojetí	k1gNnSc3
fyziky	fyzika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1938	#num#	k4
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
řádným	řádný	k2eAgMnSc7d1
profesorem	profesor	k1gMnSc7
Kalifornské	kalifornský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
a	a	k8xC
a	a	k8xC
o	o	k7c4
pět	pět	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
jako	jako	k9
přední	přední	k2eAgMnSc1d1
atomový	atomový	k2eAgMnSc1d1
fyzik	fyzik	k1gMnSc1
zvolen	zvolit	k5eAaPmNgMnS
za	za	k7c2
člena	člen	k1gMnSc2
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1936	#num#	k4
se	se	k3xPyFc4
seznámil	seznámit	k5eAaPmAgMnS
s	s	k7c7
levicovou	levicový	k2eAgFnSc7d1
aktivistkou	aktivistka	k1gFnSc7
<g/>
,	,	kIx,
členkou	členka	k1gFnSc7
komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Jean	Jean	k1gMnSc1
Tatlockovou	Tatlocková	k1gFnSc4
a	a	k8xC
stali	stát	k5eAaPmAgMnP
se	se	k3xPyFc4
z	z	k7c2
nich	on	k3xPp3gMnPc2
blízcí	blízký	k2eAgMnPc1d1
přátelé	přítel	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
jejím	její	k3xOp3gInSc7
vlivem	vliv	k1gInSc7
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
zajímat	zajímat	k5eAaImF
o	o	k7c4
politické	politický	k2eAgInPc4d1
a	a	k8xC
hospodářské	hospodářský	k2eAgInPc4d1
problémy	problém	k1gInPc4
i	i	k8xC
mezinárodní	mezinárodní	k2eAgFnSc4d1
situaci	situace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
po	po	k7c6
smrti	smrt	k1gFnSc6
otce	otec	k1gMnSc2
v	v	k7c6
roce	rok	k1gInSc6
1937	#num#	k4
zdědil	zdědit	k5eAaPmAgInS
značný	značný	k2eAgInSc1d1
majetek	majetek	k1gInSc1
<g/>
,	,	kIx,
podporoval	podporovat	k5eAaImAgInS
finančně	finančně	k6eAd1
protifašistické	protifašistický	k2eAgFnPc4d1
organizace	organizace	k1gFnPc4
a	a	k8xC
levicový	levicový	k2eAgInSc4d1
tisk	tisk	k1gInSc4
<g/>
,	,	kIx,
do	do	k7c2
kterého	který	k3yQgInSc2,k3yRgInSc2,k3yIgInSc2
přispíval	přispívat	k5eAaImAgInS
vlastními	vlastní	k2eAgInPc7d1
články	článek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozešli	rozejít	k5eAaPmAgMnP
se	se	k3xPyFc4
roku	rok	k1gInSc2
1940	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
Oppenheimer	Oppenheimer	k1gMnSc1
zamiloval	zamilovat	k5eAaPmAgMnS
do	do	k7c2
bioložky	bioložka	k1gFnSc2
Katherine	Katherin	k1gInSc5
Pueningové	Pueningový	k2eAgFnPc4d1
a	a	k8xC
oženil	oženit	k5eAaPmAgMnS
se	se	k3xPyFc4
s	s	k7c7
ní	on	k3xPp3gFnSc7
<g/>
.	.	kIx.
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Narodily	narodit	k5eAaPmAgInP
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
dvě	dva	k4xCgFnPc4
děti	dítě	k1gFnPc4
–	–	k?
syn	syn	k1gMnSc1
Peter	Peter	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1941	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
dcera	dcera	k1gFnSc1
Katherine	Katherin	k1gInSc5
(	(	kIx(
<g/>
*	*	kIx~
1944	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
atomové	atomový	k2eAgFnSc2d1
bomby	bomba	k1gFnSc2
</s>
<s>
Robert	Robert	k1gMnSc1
Oppenheimer	Oppenheimer	k1gMnSc1
a	a	k8xC
Leslie	Leslie	k1gFnSc1
Groves	Grovesa	k1gFnPc2
na	na	k7c6
místě	místo	k1gNnSc6
jaderného	jaderný	k2eAgInSc2d1
testu	test	k1gInSc2
Trinity	Trinita	k1gFnSc2
<g/>
,	,	kIx,
září	zářit	k5eAaImIp3nS
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1941	#num#	k4
byl	být	k5eAaImAgMnS
Robert	Robert	k1gMnSc1
Oppenheimer	Oppenheimer	k1gMnSc1
jako	jako	k8xC,k8xS
přední	přední	k2eAgMnSc1d1
jaderný	jaderný	k2eAgMnSc1d1
fyzik	fyzik	k1gMnSc1
zapojen	zapojit	k5eAaPmNgMnS
do	do	k7c2
komise	komise	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
posuzovala	posuzovat	k5eAaImAgFnS
možnosti	možnost	k1gFnPc4
použití	použití	k1gNnSc2
jaderné	jaderný	k2eAgFnSc2d1
energie	energie	k1gFnSc2
pro	pro	k7c4
vojenské	vojenský	k2eAgInPc4d1
účely	účel	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
vlastní	vlastní	k2eAgFnSc2d1
iniciativy	iniciativa	k1gFnSc2
přikročil	přikročit	k5eAaPmAgMnS
k	k	k7c3
pokusům	pokus	k1gInPc3
se	se	k3xPyFc4
štěpením	štěpení	k1gNnSc7
uranu	uran	k1gInSc2
a	a	k8xC
dosažené	dosažený	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
vedly	vést	k5eAaImAgInP
k	k	k7c3
rozhodnutí	rozhodnutí	k1gNnSc3
vytvořit	vytvořit	k5eAaPmF
výzkumné	výzkumný	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
pod	pod	k7c7
jeho	jeho	k3xOp3gNnSc7
vedením	vedení	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1942	#num#	k4
byl	být	k5eAaImAgInS
zahájen	zahájit	k5eAaPmNgInS
tajný	tajný	k2eAgInSc1d1
vojenský	vojenský	k2eAgInSc1d1
projekt	projekt	k1gInSc1
Manhattan	Manhattan	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
vyvinout	vyvinout	k5eAaPmF
atomovou	atomový	k2eAgFnSc4d1
bombu	bomba	k1gFnSc4
dříve	dříve	k6eAd2
než	než	k8xS
Němci	Němec	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oppenheimer	Oppenheimer	k1gMnSc1
byl	být	k5eAaImAgMnS
pověřen	pověřit	k5eAaPmNgMnS
vedením	vedení	k1gNnSc7
výzkumných	výzkumný	k2eAgFnPc2d1
prací	práce	k1gFnPc2
<g/>
,	,	kIx,
kterých	který	k3yIgFnPc2,k3yQgFnPc2,k3yRgFnPc2
se	se	k3xPyFc4
zúčastnilo	zúčastnit	k5eAaPmAgNnS
mnoho	mnoho	k4c1
předních	přední	k2eAgMnPc2d1
amerických	americký	k2eAgMnPc2d1
i	i	k8xC
evropských	evropský	k2eAgMnPc2d1
vědců	vědec	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
uprchli	uprchnout	k5eAaPmAgMnP
před	před	k7c7
nacismem	nacismus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projevil	projevit	k5eAaPmAgMnS
se	se	k3xPyFc4
jako	jako	k8xS,k8xC
vynikající	vynikající	k2eAgMnSc1d1
organizátor	organizátor	k1gMnSc1
<g/>
,	,	kIx,
pro	pro	k7c4
úspěch	úspěch	k1gInSc4
projektu	projekt	k1gInSc2
nepostradatelný	postradatelný	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
mělo	mít	k5eAaImAgNnS
význam	význam	k1gInSc4
v	v	k7c6
létě	léto	k1gNnSc6
1943	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jeho	jeho	k3xOp3gFnSc1
schůzka	schůzka	k1gFnSc1
s	s	k7c7
Jean	Jean	k1gMnSc1
Tatlockovou	Tatlocková	k1gFnSc7
vzbudila	vzbudit	k5eAaPmAgFnS
podezření	podezření	k1gNnSc4
bezpečnostních	bezpečnostní	k2eAgInPc2d1
orgánů	orgán	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svou	svůj	k3xOyFgFnSc4
loajalitu	loajalita	k1gFnSc4
prokázal	prokázat	k5eAaPmAgMnS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
zveřejnil	zveřejnit	k5eAaPmAgInS
obsah	obsah	k1gInSc4
rozhovoru	rozhovor	k1gInSc2
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
přítelem	přítel	k1gMnSc7
H.	H.	kA
Chevalierem	chevalier	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
ho	on	k3xPp3gInSc4
ptal	ptat	k5eAaImAgMnS
na	na	k7c4
možnost	možnost	k1gFnSc4
výměny	výměna	k1gFnSc2
vědeckých	vědecký	k2eAgFnPc2d1
informací	informace	k1gFnPc2
mezi	mezi	k7c7
USA	USA	kA
a	a	k8xC
Sovětským	sovětský	k2eAgInSc7d1
svazem	svaz	k1gInSc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
odmítl	odmítnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
Chevaliera	chevalier	k1gMnSc4
to	ten	k3xDgNnSc1
znamenalo	znamenat	k5eAaImAgNnS
okamžitý	okamžitý	k2eAgInSc4d1
zákaz	zákaz	k1gInSc4
činnosti	činnost	k1gFnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
svržení	svržení	k1gNnSc6
atomových	atomový	k2eAgFnPc2d1
bomb	bomba	k1gFnPc2
na	na	k7c4
Hirošimu	Hirošima	k1gFnSc4
a	a	k8xC
Nagasaki	Nagasaki	k1gNnSc4
se	se	k3xPyFc4
Oppenheimer	Oppenheimer	k1gMnSc1
jako	jako	k9
jeden	jeden	k4xCgMnSc1
z	z	k7c2
hlavních	hlavní	k2eAgMnPc2d1
představitelů	představitel	k1gMnPc2
projektu	projekt	k1gInSc2
Manhattan	Manhattan	k1gInSc1
stal	stát	k5eAaPmAgMnS
známým	známý	k2eAgMnSc7d1
po	po	k7c6
celých	celá	k1gFnPc6
USA	USA	kA
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInSc1
portrét	portrét	k1gInSc1
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgInS
na	na	k7c6
titulních	titulní	k2eAgFnPc6d1
stranách	strana	k1gFnPc6
časopisů	časopis	k1gInPc2
Life	Life	k1gNnSc7
a	a	k8xC
Time	Time	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1947	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
působil	působit	k5eAaImAgMnS
ve	v	k7c6
funkci	funkce	k1gFnSc6
prezidenta	prezident	k1gMnSc2
vědecké	vědecký	k2eAgFnSc2d1
rady	rada	k1gFnSc2
Komise	komise	k1gFnSc2
pro	pro	k7c4
atomovou	atomový	k2eAgFnSc4d1
energii	energie	k1gFnSc4
(	(	kIx(
<g/>
AEC	AEC	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
tak	tak	k6eAd1
nejvlivnějším	vlivný	k2eAgMnSc7d3
poradcem	poradce	k1gMnSc7
vlády	vláda	k1gFnSc2
a	a	k8xC
armády	armáda	k1gFnSc2
v	v	k7c6
jaderných	jaderný	k2eAgFnPc6d1
otázkách	otázka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1946	#num#	k4
byl	být	k5eAaImAgInS
v	v	k7c6
USA	USA	kA
vyznamenán	vyznamenat	k5eAaPmNgInS
Medailí	medaile	k1gFnPc2
za	za	k7c4
zásluhy	zásluha	k1gFnPc4
(	(	kIx(
<g/>
Legion	legion	k1gInSc1
of	of	k?
merit	meritum	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejvyšším	vysoký	k2eAgNnSc7d3
civilním	civilní	k2eAgNnSc7d1
vyznamenáním	vyznamenání	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
prezidentem	prezident	k1gMnSc7
Americké	americký	k2eAgFnSc2d1
fyzikální	fyzikální	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
a	a	k8xC
byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
řady	řada	k1gFnSc2
dalších	další	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Ničivé	ničivý	k2eAgInPc1d1
následky	následek	k1gInPc1
výbuchů	výbuch	k1gInPc2
jaderných	jaderný	k2eAgFnPc2d1
bomb	bomba	k1gFnPc2
přiměly	přimět	k5eAaPmAgFnP
Oppenheimera	Oppenheimero	k1gNnPc4
k	k	k7c3
odchodu	odchod	k1gInSc3
z	z	k7c2
funkce	funkce	k1gFnSc2
ředitele	ředitel	k1gMnSc2
výzkumného	výzkumný	k2eAgNnSc2d1
střediska	středisko	k1gNnSc2
v	v	k7c4
Los	los	k1gInSc4
Alamos	Alamos	k1gInSc1
(	(	kIx(
<g/>
říjen	říjen	k1gInSc1
1945	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poválečné	poválečný	k2eAgNnSc1d1
období	období	k1gNnSc1
</s>
<s>
Lawrence	Lawrence	k1gFnSc1
<g/>
,	,	kIx,
Seaborg	Seaborg	k1gInSc1
a	a	k8xC
Oppenheimer	Oppenheimer	k1gInSc1
v	v	k7c4
Berkeley	Berkelea	k1gFnPc4
<g/>
,	,	kIx,
asi	asi	k9
1946	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
skončení	skončení	k1gNnSc6
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
Oppenheimer	Oppenheimer	k1gMnSc1
nesouhlasil	souhlasit	k5eNaImAgMnS
s	s	k7c7
dalším	další	k2eAgInSc7d1
vývojem	vývoj	k1gInSc7
bomb	bomba	k1gFnPc2
a	a	k8xC
nechtěl	chtít	k5eNaImAgMnS
pokračovat	pokračovat	k5eAaImF
ve	v	k7c6
vojenském	vojenský	k2eAgInSc6d1
jaderném	jaderný	k2eAgInSc6d1
výzkumu	výzkum	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odmítl	odmítnout	k5eAaPmAgMnS
se	se	k3xPyFc4
podílet	podílet	k5eAaImF
na	na	k7c6
výzkumu	výzkum	k1gInSc6
vodíkové	vodíkový	k2eAgFnSc2d1
bomby	bomba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
přesvědčen	přesvědčit	k5eAaPmNgMnS
o	o	k7c6
nutnosti	nutnost	k1gFnSc6
mezinárodní	mezinárodní	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
a	a	k8xC
kontroly	kontrola	k1gFnSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
využití	využití	k1gNnSc2
jaderné	jaderný	k2eAgFnSc2d1
energie	energie	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
v	v	k7c6
období	období	k1gNnSc6
studené	studený	k2eAgFnSc2d1
války	válka	k1gFnSc2
neodpovídalo	odpovídat	k5eNaImAgNnS
vládní	vládní	k2eAgFnSc3d1
politice	politika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1949	#num#	k4
až	až	k9
1954	#num#	k4
byl	být	k5eAaImAgInS
v	v	k7c6
éře	éra	k1gFnSc6
mccarthismu	mccarthismus	k1gInSc2
obviněn	obvinit	k5eAaPmNgInS
z	z	k7c2
prokomunistického	prokomunistický	k2eAgNnSc2d1
smýšlení	smýšlení	k1gNnSc2
<g/>
,	,	kIx,
špionáže	špionáž	k1gFnSc2
pro	pro	k7c4
Sovětský	sovětský	k2eAgInSc4d1
svaz	svaz	k1gInSc4
a	a	k8xC
několikrát	několikrát	k6eAd1
vyslýchán	vyslýchat	k5eAaImNgMnS
vyšetřovací	vyšetřovací	k2eAgFnSc7d1
komisí	komise	k1gFnSc7
vládního	vládní	k2eAgInSc2d1
Výboru	výbor	k1gInSc2
pro	pro	k7c4
atomovou	atomový	k2eAgFnSc4d1
energii	energie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
mu	on	k3xPp3gMnSc3
odebráno	odebrat	k5eAaPmNgNnS
bezpečnostní	bezpečnostní	k2eAgNnSc1d1
prověření	prověření	k1gNnSc1
a	a	k8xC
nemohl	moct	k5eNaImAgMnS
dále	daleko	k6eAd2
pracovat	pracovat	k5eAaImF
na	na	k7c6
projektech	projekt	k1gInPc6
jaderného	jaderný	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Působil	působit	k5eAaImAgMnS
jako	jako	k9
pedagog	pedagog	k1gMnSc1
v	v	k7c6
Ústavu	ústav	k1gInSc6
pro	pro	k7c4
pokročilá	pokročilý	k2eAgNnPc4d1
studia	studio	k1gNnPc4
v	v	k7c6
Princetonu	Princeton	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
pod	pod	k7c7
jeho	jeho	k3xOp3gNnSc7
vedením	vedení	k1gNnSc7
stal	stát	k5eAaPmAgInS
významným	významný	k2eAgInSc7d1
centrem	centr	k1gInSc7
teoretické	teoretický	k2eAgFnSc2d1
a	a	k8xC
jaderné	jaderný	k2eAgFnSc2d1
fyziky	fyzika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Procestoval	procestovat	k5eAaPmAgMnS
Evropu	Evropa	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
dostalo	dostat	k5eAaPmAgNnS
mnoha	mnoho	k4c2
ocenění	ocenění	k1gNnPc2
(	(	kIx(
<g/>
mj.	mj.	kA
<g/>
Čestná	čestný	k2eAgFnSc1d1
legie	legie	k1gFnSc1
ve	v	k7c6
Francii	Francie	k1gFnSc6
roku	rok	k1gInSc2
1958	#num#	k4
<g/>
,	,	kIx,
zvolení	zvolení	k1gNnSc4
zahraničním	zahraniční	k2eAgMnSc7d1
členem	člen	k1gMnSc7
Královské	královský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
v	v	k7c6
Anglii	Anglie	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
USA	USA	kA
se	se	k3xPyFc4
Robert	Robert	k1gMnSc1
Oppenheimer	Oppenheimer	k1gMnSc1
dočkal	dočkat	k5eAaPmAgMnS
politické	politický	k2eAgFnPc4d1
rehabilitace	rehabilitace	k1gFnPc4
až	až	k9
od	od	k7c2
prezidenta	prezident	k1gMnSc2
J.	J.	kA
F.	F.	kA
Kennedyho	Kennedy	k1gMnSc2
<g/>
,	,	kIx,
když	když	k8xS
mu	on	k3xPp3gMnSc3
byla	být	k5eAaImAgFnS
v	v	k7c6
roku	rok	k1gInSc6
1963	#num#	k4
udělena	udělen	k2eAgFnSc1d1
cena	cena	k1gFnSc1
Enrica	Enricus	k1gMnSc2
Fermiho	Fermi	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Životní	životní	k2eAgFnPc1d1
zkušenosti	zkušenost	k1gFnPc1
získané	získaný	k2eAgFnPc1d1
v	v	k7c6
období	období	k1gNnSc6
práce	práce	k1gFnSc2
na	na	k7c6
atomové	atomový	k2eAgFnSc6d1
bombě	bomba	k1gFnSc6
přiměly	přimět	k5eAaPmAgInP
Oppenheimera	Oppenheimero	k1gNnSc2
k	k	k7c3
úvahám	úvaha	k1gFnPc3
o	o	k7c6
úloze	úloha	k1gFnSc6
vědce	vědec	k1gMnSc2
v	v	k7c6
moderní	moderní	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
a	a	k8xC
etických	etický	k2eAgInPc6d1
důsledcích	důsledek	k1gInPc6
vědeckého	vědecký	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zabýval	zabývat	k5eAaImAgMnS
se	se	k3xPyFc4
morálními	morální	k2eAgInPc7d1
problémy	problém	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
před	před	k7c4
vědce	vědec	k1gMnSc4
staví	stavit	k5eAaPmIp3nS,k5eAaImIp3nS,k5eAaBmIp3nS
jejich	jejich	k3xOp3gInPc4
objevy	objev	k1gInPc4
a	a	k8xC
mírou	míra	k1gFnSc7
odpovědnosti	odpovědnost	k1gFnSc2
za	za	k7c4
jejich	jejich	k3xOp3gFnSc4
možné	možný	k2eAgNnSc1d1
zneužití	zneužití	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS
roku	rok	k1gInSc2
1967	#num#	k4
ve	v	k7c6
věku	věk	k1gInSc6
62	#num#	k4
let	léto	k1gNnPc2
na	na	k7c4
rakovinu	rakovina	k1gFnSc4
hrtanu	hrtan	k1gInSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
náruživý	náruživý	k2eAgInSc1d1
kuřák	kuřák	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
kremaci	kremace	k1gFnSc6
rozptýlila	rozptýlit	k5eAaPmAgFnS
jeho	jeho	k3xOp3gFnSc1
žena	žena	k1gFnSc1
popel	popel	k1gInSc4
po	po	k7c6
mořské	mořský	k2eAgFnSc6d1
hladině	hladina	k1gFnSc6
u	u	k7c2
Saint	Sainta	k1gFnPc2
John	John	k1gMnSc1
na	na	k7c6
Panenských	panenský	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
trávíval	trávívat	k5eAaImAgMnS
chvíle	chvíle	k1gFnPc4
odpočinku	odpočinek	k1gInSc2
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
rodinou	rodina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
J.	J.	kA
Robert	Robert	k1gMnSc1
Oppenheimer	Oppenheimer	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dodnes	dodnes	k6eAd1
není	být	k5eNaImIp3nS
zcela	zcela	k6eAd1
jasné	jasný	k2eAgNnSc1d1
co	co	k9
přesně	přesně	k6eAd1
znamená	znamenat	k5eAaImIp3nS
iniciála	iniciála	k1gFnSc1
J.	J.	kA
v	v	k7c6
jeho	jeho	k3xOp3gNnSc6
jméně	jméno	k1gNnSc6
en	en	k?
<g/>
:	:	kIx,
<g/>
Robert	Robert	k1gMnSc1
Oppenheimer	Oppenheimer	k1gMnSc1
<g/>
#	#	kIx~
<g/>
On	on	k3xPp3gMnSc1
Oppenheimer	Oppenheimer	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
first	first	k1gMnSc1
initial	initial	k1gMnSc1
<g/>
↑	↑	k?
Robert	Robert	k1gMnSc1
Jungk	Jungk	k1gMnSc1
<g/>
:	:	kIx,
Jasnější	jasný	k2eAgInSc1d2
než	než	k8xS
tisíc	tisíc	k4xCgInSc1
sluncí	slunce	k1gNnPc2
<g/>
,	,	kIx,
Mladá	mladý	k2eAgFnSc1d1
Fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
19651	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
VOKÁČ	Vokáč	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hitlerova	Hitlerův	k2eAgFnSc1d1
bomba	bomba	k1gFnSc1
nad	nad	k7c7
Hirošimou	Hirošima	k1gFnSc7
<g/>
.	.	kIx.
1	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgInPc1d1
Přílepy	přílep	k1gInPc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Olympia	Olympia	k1gFnSc1
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7376	#num#	k4
<g/>
-	-	kIx~
<g/>
462	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
44	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
CODR	CODR	kA
<g/>
,	,	kIx,
Milan	Milan	k1gMnSc1
<g/>
;	;	kIx,
RŮŽEK	Růžek	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přemožitelé	přemožitel	k1gMnPc1
času	čas	k1gInSc2
sv.	sv.	kA
5	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
novinářů	novinář	k1gMnPc2
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Jacob	Jacoba	k1gFnPc2
Robert	Robert	k1gMnSc1
Oppenheimer	Oppenheimer	k1gMnSc1
<g/>
,	,	kIx,
s.	s.	k?
69	#num#	k4
<g/>
-	-	kIx~
<g/>
73	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Robert	Robert	k1gMnSc1
Oppenheimer	Oppenheimer	k1gMnSc1
|	|	kIx~
Eduportál	Eduportál	k1gInSc1
Techmania	Techmanium	k1gNnSc2
<g/>
.	.	kIx.
edu	edu	k?
<g/>
.	.	kIx.
<g/>
techmania	techmanium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
VYKOUPIL	vykoupit	k5eAaPmAgMnS
<g/>
,	,	kIx,
Libor	Libor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ecce	Ecc	k1gInSc2
homo	homo	k6eAd1
:	:	kIx,
z	z	k7c2
rozhlasových	rozhlasový	k2eAgInPc2d1
fejetonů	fejeton	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Julius	Julius	k1gMnSc1
Zirkus	Zirkus	k1gMnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
312	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
903377	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Robert	Roberta	k1gFnPc2
Oppenheimer	Oppenheimero	k1gNnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Robert	Roberta	k1gFnPc2
Oppenheimer	Oppenheimra	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Osoba	osoba	k1gFnSc1
Robert	Robert	k1gMnSc1
Oppenheimer	Oppenheimer	k1gMnSc1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Jacob	Jacoba	k1gFnPc2
Robert	Robert	k1gMnSc1
Oppenheimer	Oppenheimer	k1gMnSc1
<g/>
:	:	kIx,
Úvahy	úvaha	k1gFnPc1
o	o	k7c6
vědě	věda	k1gFnSc6
a	a	k8xC
kultuře	kultura	k1gFnSc6
1	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
,	,	kIx,
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
24	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2007	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Druhá	druhý	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
|	|	kIx~
Fyzika	fyzika	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kup	kup	k1gInSc1
<g/>
20010000072667	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118590146	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0857	#num#	k4
0639	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
50005793	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
44376228	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
50005793	#num#	k4
</s>
