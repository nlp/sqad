<s>
J.	J.	kA
Robert	Robert	k1gMnSc1
Oppenheimer	Oppenheimer	k1gMnSc1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
Julius	Julius	k1gMnSc1
Robert	Robert	k1gMnSc1
Oppenheimer	Oppenheimero	k1gNnPc2
nebo	nebo	k8xC
Jacob	Jacob	k1gMnSc1
Robert	Robert	k1gMnSc1
Oppenheimer	Oppenheimer	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1904	#num#	k4
–	–	k?
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1967	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
americký	americký	k2eAgMnSc1d1
teoretický	teoretický	k2eAgMnSc1d1
fyzik	fyzik	k1gMnSc1
<g/>
,	,	kIx,
nejznámější	známý	k2eAgMnSc1d3
svou	svůj	k3xOyFgFnSc7
účastí	účast	k1gFnSc7
v	v	k7c6
projektu	projekt	k1gInSc6
Manhattan	Manhattan	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
řídil	řídit	k5eAaImAgMnS
vývoj	vývoj	k1gInSc4
první	první	k4xOgFnSc2
jaderné	jaderný	k2eAgFnSc2d1
zbraně	zbraň	k1gFnSc2
v	v	k7c6
tajné	tajný	k2eAgFnSc6d1
laboratoři	laboratoř	k1gFnSc6
v	v	k7c4
Los	los	k1gInSc4
Alamos	Alamos	k1gFnPc6
v	v	k7c6
Novém	nový	k2eAgNnSc6d1
Mexiku	Mexiko	k1gNnSc6
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>