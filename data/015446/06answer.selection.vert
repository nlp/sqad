<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1
svaz	svaz	k1gInSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
International	International	k1gFnSc1
Union	union	k1gInSc1
for	forum	k1gNnPc2
Conservation	Conservation	k1gInSc1
of	of	k?
Nature	Natur	k1gMnSc5
<g/>
,	,	kIx,
IUCN	IUCN	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
zaměřená	zaměřený	k2eAgFnSc1d1
na	na	k7c4
uchování	uchování	k1gNnSc4
přírodních	přírodní	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
.	.	kIx.
</s>