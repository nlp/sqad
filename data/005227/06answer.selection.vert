<s>
Tenochtitlán	Tenochtitlán	k2eAgInSc1d1	Tenochtitlán
bylo	být	k5eAaImAgNnS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Aztécké	aztécký	k2eAgFnSc2d1	aztécká
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
založené	založený	k2eAgFnSc2d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1325	[number]	k4	1325
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1521	[number]	k4	1521
sloužící	sloužící	k2eAgMnSc1d1	sloužící
jako	jako	k8xS	jako
aztécké	aztécký	k2eAgNnSc1d1	aztécké
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
