<s>
Tenochtitlán	Tenochtitlán	k2eAgInSc1d1	Tenochtitlán
bylo	být	k5eAaImAgNnS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Aztécké	aztécký	k2eAgFnSc2d1	aztécká
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
založené	založený	k2eAgFnSc2d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1325	[number]	k4	1325
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1521	[number]	k4	1521
sloužící	sloužící	k2eAgMnSc1d1	sloužící
jako	jako	k8xS	jako
aztécké	aztécký	k2eAgNnSc1d1	aztécké
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
dobyto	dobyt	k2eAgNnSc1d1	dobyto
a	a	k8xC	a
zničeno	zničen	k2eAgNnSc1d1	zničeno
španělskými	španělský	k2eAgMnPc7d1	španělský
conquistadory	conquistador	k1gMnPc7	conquistador
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
objevení	objevení	k1gNnSc2	objevení
města	město	k1gNnSc2	město
Evropany	Evropan	k1gMnPc4	Evropan
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
žilo	žít	k5eAaImAgNnS	žít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
řádově	řádově	k6eAd1	řádově
asi	asi	k9	asi
stotisíc	stotisit	k5eAaImSgFnS	stotisit
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
amerického	americký	k2eAgInSc2d1	americký
kontinentu	kontinent	k1gInSc2	kontinent
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
měst	město	k1gNnPc2	město
tehdejšího	tehdejší	k2eAgInSc2d1	tehdejší
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládalo	rozkládat	k5eAaImAgNnS	rozkládat
se	se	k3xPyFc4	se
na	na	k7c6	na
několika	několik	k4yIc6	několik
ostrovech	ostrov	k1gInPc6	ostrov
jezera	jezero	k1gNnSc2	jezero
Texcoco	Texcoco	k1gMnSc1	Texcoco
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
rozloha	rozloha	k1gFnSc1	rozloha
činila	činit	k5eAaImAgFnS	činit
přibližně	přibližně	k6eAd1	přibližně
13	[number]	k4	13
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
zbytky	zbytek	k1gInPc1	zbytek
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
překryty	překrýt	k5eAaPmNgFnP	překrýt
dnešním	dnešní	k2eAgNnSc7d1	dnešní
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Mexika	Mexiko	k1gNnSc2	Mexiko
Mexico	Mexico	k6eAd1	Mexico
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
málo	málo	k4c1	málo
zbytků	zbytek	k1gInPc2	zbytek
původního	původní	k2eAgNnSc2d1	původní
města	město	k1gNnSc2	město
patří	patřit	k5eAaImIp3nS	patřit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
ke	k	k7c3	k
Světovému	světový	k2eAgNnSc3d1	světové
dědictví	dědictví	k1gNnSc3	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Tenochtitlán	Tenochtitlán	k2eAgMnSc1d1	Tenochtitlán
ležel	ležet	k5eAaImAgMnS	ležet
na	na	k7c6	na
několika	několik	k4yIc6	několik
ostrovech	ostrov	k1gInPc6	ostrov
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
jezera	jezero	k1gNnSc2	jezero
Texcoco	Texcoco	k6eAd1	Texcoco
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Údolí	údolí	k1gNnSc6	údolí
Mexika	Mexiko	k1gNnSc2	Mexiko
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
přibližně	přibližně	k6eAd1	přibližně
2240	[number]	k4	2240
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
východě	východ	k1gInSc6	východ
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
je	být	k5eAaImIp3nS	být
údolí	údolí	k1gNnSc1	údolí
obklopeno	obklopen	k2eAgNnSc1d1	obklopeno
horským	horský	k2eAgInSc7d1	horský
hřebenem	hřeben	k1gInSc7	hřeben
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgInPc4	který
vévodí	vévodit	k5eAaImIp3nP	vévodit
sopky	sopka	k1gFnPc1	sopka
Popocatépetl	Popocatépetl	k1gMnPc2	Popocatépetl
a	a	k8xC	a
Iztaccíhuatl	Iztaccíhuatl	k1gMnPc2	Iztaccíhuatl
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
pevninou	pevnina	k1gFnSc7	pevnina
pěti	pět	k4xCc2	pět
cestami	cesta	k1gFnPc7	cesta
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
západě	západ	k1gInSc6	západ
a	a	k8xC	a
jihu	jih	k1gInSc6	jih
po	po	k7c6	po
hrázích	hráz	k1gFnPc6	hráz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sousedním	sousední	k2eAgInSc6d1	sousední
ostrově	ostrov	k1gInSc6	ostrov
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
od	od	k7c2	od
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
odděleného	oddělený	k2eAgInSc2d1	oddělený
pouze	pouze	k6eAd1	pouze
kanálem	kanál	k1gInSc7	kanál
<g/>
,	,	kIx,	,
leželo	ležet	k5eAaImAgNnS	ležet
zprvu	zprvu	k6eAd1	zprvu
ještě	ještě	k6eAd1	ještě
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
město	město	k1gNnSc1	město
Tlatelolco	Tlatelolco	k6eAd1	Tlatelolco
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
Texcoco	Texcoco	k6eAd1	Texcoco
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
dřívější	dřívější	k2eAgFnSc6d1	dřívější
rozloze	rozloha	k1gFnSc6	rozloha
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Španěly	Španěly	k1gInPc4	Španěly
po	po	k7c6	po
částech	část	k1gFnPc6	část
vysušeno	vysušen	k2eAgNnSc1d1	vysušeno
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
v	v	k7c4	v
Xochimilco	Xochimilco	k1gNnSc4	Xochimilco
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
dnešního	dnešní	k2eAgNnSc2d1	dnešní
města	město	k1gNnSc2	město
Mexico	Mexico	k6eAd1	Mexico
City	City	k1gFnPc1	City
existují	existovat	k5eAaImIp3nP	existovat
malé	malý	k2eAgFnPc1d1	malá
vodní	vodní	k2eAgFnPc1d1	vodní
plochy	plocha	k1gFnPc1	plocha
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
posledními	poslední	k2eAgInPc7d1	poslední
zbytky	zbytek	k1gInPc7	zbytek
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
Tenochtitlán	Tenochtitlán	k2eAgMnSc1d1	Tenochtitlán
a	a	k8xC	a
Tlatelolco	Tlatelolco	k1gMnSc1	Tlatelolco
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgInP	mít
rozlohu	rozloha	k1gFnSc4	rozloha
přibližně	přibližně	k6eAd1	přibližně
13	[number]	k4	13
čtverečných	čtverečný	k2eAgInPc2d1	čtverečný
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
s	s	k7c7	s
pevninou	pevnina	k1gFnSc7	pevnina
spojeno	spojit	k5eAaPmNgNnS	spojit
hrázemi	hráz	k1gFnPc7	hráz
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
a	a	k8xC	a
zeminy	zemina	k1gFnSc2	zemina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
jsou	být	k5eAaImIp3nP	být
hráze	hráz	k1gFnPc1	hráz
přerušené	přerušený	k2eAgFnPc1d1	přerušená
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
se	se	k3xPyFc4	se
na	na	k7c6	na
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
daly	dát	k5eAaPmAgFnP	dát
uzavírat	uzavírat	k5eAaImF	uzavírat
dřevěnými	dřevěný	k2eAgInPc7d1	dřevěný
pohyblivými	pohyblivý	k2eAgInPc7d1	pohyblivý
mosty	most	k1gInPc7	most
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc2	tento
výhody	výhoda	k1gFnSc2	výhoda
využili	využít	k5eAaPmAgMnP	využít
Aztékové	Azték	k1gMnPc1	Azték
především	především	k6eAd1	především
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1521	[number]	k4	1521
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
město	město	k1gNnSc4	město
bránili	bránit	k5eAaImAgMnP	bránit
před	před	k7c4	před
Španěly	Španěl	k1gMnPc4	Španěl
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Tenochtitlán	Tenochtitlán	k2eAgInSc1d1	Tenochtitlán
byl	být	k5eAaImAgInS	být
přístupný	přístupný	k2eAgInSc1d1	přístupný
pouze	pouze	k6eAd1	pouze
po	po	k7c6	po
silnicích	silnice	k1gFnPc6	silnice
vedoucích	vedoucí	k2eAgFnPc6d1	vedoucí
přes	přes	k7c4	přes
hráze	hráz	k1gFnPc4	hráz
<g/>
,	,	kIx,	,
neměl	mít	k5eNaImAgMnS	mít
žádné	žádný	k3yNgFnPc4	žádný
městské	městský	k2eAgFnPc4d1	městská
hradby	hradba	k1gFnPc4	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Cesty	cesta	k1gFnPc4	cesta
po	po	k7c6	po
hrázích	hráz	k1gFnPc6	hráz
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
<g/>
,	,	kIx,	,
západu	západ	k1gInSc2	západ
a	a	k8xC	a
jihu	jih	k1gInSc2	jih
končily	končit	k5eAaImAgInP	končit
u	u	k7c2	u
chrámu	chrám	k1gInSc2	chrám
Templo	Templo	k1gMnSc1	Templo
Mayor	Mayor	k1gMnSc1	Mayor
na	na	k7c6	na
velkém	velký	k2eAgNnSc6d1	velké
náměstí	náměstí	k1gNnSc6	náměstí
Tlatelolco	Tlatelolco	k6eAd1	Tlatelolco
a	a	k8xC	a
u	u	k7c2	u
náměstí	náměstí	k1gNnSc2	náměstí
Templo	Templo	k1gMnSc1	Templo
Mayor	Mayor	k1gMnSc1	Mayor
v	v	k7c4	v
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
tvořilo	tvořit	k5eAaImAgNnS	tvořit
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
náměstí	náměstí	k1gNnSc2	náměstí
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dějin	dějiny	k1gFnPc2	dějiny
Aztéků	Azték	k1gMnPc2	Azték
postaveny	postaven	k2eAgInPc1d1	postaven
paláce	palác	k1gInPc1	palác
vládců	vládce	k1gMnPc2	vládce
Moctezumy	Moctezum	k1gInPc4	Moctezum
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Axayacatla	Axayacatla	k1gFnPc1	Axayacatla
a	a	k8xC	a
Moctezumy	Moctezum	k1gInPc1	Moctezum
II	II	kA	II
<g/>
..	..	k?	..
Pravoúhlé	pravoúhlý	k2eAgFnSc2d1	pravoúhlá
<g/>
,	,	kIx,	,
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
kolmé	kolmý	k2eAgFnPc1d1	kolmá
hlavní	hlavní	k2eAgFnPc1d1	hlavní
silnice	silnice	k1gFnPc1	silnice
dělily	dělit	k5eAaImAgFnP	dělit
Tenochtitlán	Tenochtitlán	k2eAgInSc4d1	Tenochtitlán
na	na	k7c6	na
městské	městský	k2eAgFnSc6d1	městská
čtvrti	čtvrt	k1gFnSc6	čtvrt
Moyotlan	Moyotlan	k1gMnSc1	Moyotlan
<g/>
,	,	kIx,	,
Zoquiapan	Zoquiapan	k1gMnSc1	Zoquiapan
<g/>
,	,	kIx,	,
Atzacualco	Atzacualco	k1gMnSc1	Atzacualco
a	a	k8xC	a
Cuepopan	Cuepopana	k1gFnPc2	Cuepopana
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělily	dělit	k5eAaImAgInP	dělit
na	na	k7c4	na
menší	malý	k2eAgFnPc4d2	menší
městské	městský	k2eAgFnPc4d1	městská
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Doprava	doprava	k1gFnSc1	doprava
vně	vně	k6eAd1	vně
města	město	k1gNnPc1	město
probíhala	probíhat	k5eAaImAgNnP	probíhat
labyrintem	labyrint	k1gInSc7	labyrint
ulic	ulice	k1gFnPc2	ulice
a	a	k8xC	a
vodních	vodní	k2eAgInPc2d1	vodní
kanálů	kanál	k1gInPc2	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
obchod	obchod	k1gInSc4	obchod
se	s	k7c7	s
sídlišti	sídliště	k1gNnPc7	sídliště
mimo	mimo	k7c4	mimo
město	město	k1gNnSc4	město
byl	být	k5eAaImAgInS	být
kromě	kromě	k7c2	kromě
silnic	silnice	k1gFnPc2	silnice
po	po	k7c6	po
hrázích	hráz	k1gFnPc6	hráz
používán	používat	k5eAaImNgInS	používat
i	i	k9	i
přístav	přístav	k1gInSc1	přístav
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
<g/>
.	.	kIx.	.
</s>
<s>
Natěsnané	natěsnaný	k2eAgInPc1d1	natěsnaný
blízko	blízko	k6eAd1	blízko
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
stály	stát	k5eAaImAgInP	stát
mnohaposchoďové	mnohaposchoďový	k2eAgInPc1d1	mnohaposchoďový
obytné	obytný	k2eAgInPc1d1	obytný
domy	dům	k1gInPc1	dům
nižších	nízký	k2eAgFnPc2d2	nižší
společenských	společenský	k2eAgFnPc2d1	společenská
tříd	třída	k1gFnPc2	třída
<g/>
;	;	kIx,	;
část	část	k1gFnSc1	část
jich	on	k3xPp3gInPc2	on
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
na	na	k7c6	na
pilotech	pilot	k1gInPc6	pilot
<g/>
.	.	kIx.	.
</s>
<s>
Pozemky	pozemka	k1gFnPc1	pozemka
domů	dům	k1gInPc2	dům
ve	v	k7c6	v
vnějším	vnější	k2eAgNnSc6d1	vnější
okrscích	okrsek	k1gInPc6	okrsek
města	město	k1gNnSc2	město
měly	mít	k5eAaImAgInP	mít
většinou	většinou	k6eAd1	většinou
oplocené	oplocený	k2eAgFnSc2d1	oplocená
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
nimi	on	k3xPp3gMnPc7	on
byly	být	k5eAaImAgFnP	být
takzvané	takzvaný	k2eAgFnPc1d1	takzvaná
chinampas	chinampas	k1gInSc1	chinampas
<g/>
,	,	kIx,	,
pole	pole	k1gNnPc1	pole
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
byla	být	k5eAaImAgNnP	být
většinou	většinou	k6eAd1	většinou
jen	jen	k6eAd1	jen
několik	několik	k4yIc4	několik
metrů	metr	k1gInPc2	metr
široká	široký	k2eAgFnSc1d1	široká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
poměru	poměr	k1gInSc3	poměr
k	k	k7c3	k
šířce	šířka	k1gFnSc3	šířka
byla	být	k5eAaImAgFnS	být
velice	velice	k6eAd1	velice
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Chinampas	Chinampas	k1gInSc4	Chinampas
byly	být	k5eAaImAgFnP	být
zemědělské	zemědělský	k2eAgFnPc1d1	zemědělská
pozemky	pozemka	k1gFnPc1	pozemka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
získány	získat	k5eAaPmNgFnP	získat
z	z	k7c2	z
bažinaté	bažinatý	k2eAgFnSc2d1	bažinatá
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
své	svůj	k3xOyFgFnSc2	svůj
vysoké	vysoký	k2eAgFnSc2d1	vysoká
vlhkosti	vlhkost	k1gFnSc2	vlhkost
umožňovaly	umožňovat	k5eAaImAgFnP	umožňovat
více	hodně	k6eAd2	hodně
úrod	úroda	k1gFnPc2	úroda
do	do	k7c2	do
roka	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
chinampas	chinampas	k1gMnSc1	chinampas
samy	sám	k3xTgInPc1	sám
nestačily	stačit	k5eNaBmAgInP	stačit
na	na	k7c4	na
obživu	obživa	k1gFnSc4	obživa
stále	stále	k6eAd1	stále
rostoucí	rostoucí	k2eAgFnSc2d1	rostoucí
populace	populace	k1gFnSc2	populace
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
půdy	půda	k1gFnSc2	půda
se	s	k7c7	s
zvětšováním	zvětšování	k1gNnSc7	zvětšování
rozlohy	rozloha	k1gFnSc2	rozloha
města	město	k1gNnSc2	město
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
stávalo	stávat	k5eAaImAgNnS	stávat
výsadou	výsada	k1gFnSc7	výsada
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
vyšších	vysoký	k2eAgFnPc2d2	vyšší
společenských	společenský	k2eAgFnPc2d1	společenská
vrstev	vrstva	k1gFnPc2	vrstva
ovšem	ovšem	k9	ovšem
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
obyvatelstvu	obyvatelstvo	k1gNnSc6	obyvatelstvo
poměrně	poměrně	k6eAd1	poměrně
vysoký	vysoký	k2eAgMnSc1d1	vysoký
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
měla	mít	k5eAaImAgFnS	mít
chrámová	chrámový	k2eAgFnSc1d1	chrámová
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
Templo	Templo	k1gFnSc2	Templo
Mayor	Mayora	k1gFnPc2	Mayora
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgInP	nacházet
větší	veliký	k2eAgInPc1d2	veliký
a	a	k8xC	a
menší	malý	k2eAgInPc1d2	menší
chrámy	chrám	k1gInPc1	chrám
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
věnovány	věnovat	k5eAaImNgInP	věnovat
různým	různý	k2eAgNnPc3d1	různé
božstvům	božstvo	k1gNnPc3	božstvo
a	a	k8xC	a
rozličným	rozličný	k2eAgInPc3d1	rozličný
kultům	kult	k1gInPc3	kult
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
společenských	společenský	k2eAgFnPc2d1	společenská
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
v	v	k7c6	v
chrámovém	chrámový	k2eAgInSc6d1	chrámový
okrsku	okrsek	k1gInSc6	okrsek
nacházel	nacházet	k5eAaImAgMnS	nacházet
prostor	prostor	k1gInSc4	prostor
pro	pro	k7c4	pro
rituální	rituální	k2eAgFnPc4d1	rituální
míčové	míčový	k2eAgFnPc4d1	Míčová
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc4d1	severní
část	část	k1gFnSc4	část
města	město	k1gNnSc2	město
tvořilo	tvořit	k5eAaImAgNnS	tvořit
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1473	[number]	k4	1473
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
město	město	k1gNnSc4	město
Tlatelolco	Tlatelolco	k6eAd1	Tlatelolco
<g/>
.	.	kIx.	.
</s>
<s>
Sídlo	sídlo	k1gNnSc1	sídlo
bylo	být	k5eAaImAgNnS	být
před	před	k7c7	před
svým	svůj	k3xOyFgNnSc7	svůj
dobytím	dobytí	k1gNnSc7	dobytí
úzce	úzko	k6eAd1	úzko
hospodářsky	hospodářsky	k6eAd1	hospodářsky
propojeno	propojen	k2eAgNnSc1d1	propojeno
s	s	k7c7	s
Tenochtitlánen	Tenochtitlánna	k1gFnPc2	Tenochtitlánna
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
centrem	centrum	k1gNnSc7	centrum
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
podmanění	podmanění	k1gNnSc6	podmanění
obyvateli	obyvatel	k1gMnPc7	obyvatel
Tenochtitlanu	Tenochtitlana	k1gFnSc4	Tenochtitlana
si	se	k3xPyFc3	se
Tlatelolco	Tlatelolco	k1gMnSc1	Tlatelolco
tento	tento	k3xDgInSc4	tento
charakter	charakter	k1gInSc4	charakter
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
ponechalo	ponechat	k5eAaPmAgNnS	ponechat
<g/>
;	;	kIx,	;
sami	sám	k3xTgMnPc1	sám
Španělé	Španěl	k1gMnPc1	Španěl
byly	být	k5eAaImAgFnP	být
překvapeni	překvapit	k5eAaPmNgMnP	překvapit
pestrostí	pestrost	k1gFnPc2	pestrost
zboží	zboží	k1gNnSc4	zboží
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
Tlatelolca	Tlatelolc	k1gInSc2	Tlatelolc
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
době	doba	k1gFnSc6	doba
příchodu	příchod	k1gInSc2	příchod
Španělů	Španěl	k1gMnPc2	Španěl
ve	v	k7c6	v
spojených	spojený	k2eAgNnPc6d1	spojené
městech	město	k1gNnPc6	město
mnoho	mnoho	k4c1	mnoho
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
více	hodně	k6eAd2	hodně
než	než	k8xS	než
150	[number]	k4	150
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Tenochtitlán	Tenochtitlán	k2eAgMnSc1d1	Tenochtitlán
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
měst	město	k1gNnPc2	město
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
města	město	k1gNnSc2	město
v	v	k7c6	v
nahuatlu	nahuatlo	k1gNnSc6	nahuatlo
zní	znět	k5eAaImIp3nS	znět
Tenóčtitlan	Tenóčtitlan	k1gInSc1	Tenóčtitlan
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
te	te	k?	te
<g/>
.	.	kIx.	.
<g/>
noː	noː	k?	noː
<g/>
.	.	kIx.	.
<g/>
'	'	kIx"	'
<g/>
ti	ten	k3xDgMnPc1	ten
<g/>
.	.	kIx.	.
<g/>
tɬ	tɬ	k?	tɬ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Město	město	k1gNnSc1	město
Ténocha	Ténoch	k1gMnSc2	Ténoch
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
ze	z	k7c2	z
slov	slovo	k1gNnPc2	slovo
tetl	tetla	k1gFnPc2	tetla
<g/>
,	,	kIx,	,
kámen	kámen	k1gInSc1	kámen
<g/>
,	,	kIx,	,
a	a	k8xC	a
nochtli	nochtle	k1gFnSc4	nochtle
<g/>
,	,	kIx,	,
fík	fík	k1gInSc4	fík
kaktusu	kaktus	k1gInSc2	kaktus
Opuntia	Opuntium	k1gNnSc2	Opuntium
vulgaris	vulgaris	k1gFnSc2	vulgaris
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
objevující	objevující	k2eAgInSc1d1	objevující
překlad	překlad	k1gInSc1	překlad
"	"	kIx"	"
<g/>
Město	město	k1gNnSc1	město
kamenného	kamenný	k2eAgInSc2d1	kamenný
kaktusu	kaktus	k1gInSc2	kaktus
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
těžko	těžko	k6eAd1	těžko
proto	proto	k8xC	proto
obhajitelný	obhajitelný	k2eAgInSc1d1	obhajitelný
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
legendy	legenda	k1gFnSc2	legenda
leží	ležet	k5eAaImIp3nS	ležet
původ	původ	k1gInSc1	původ
Aztéků	Azték	k1gMnPc2	Azték
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
zvaném	zvaný	k2eAgNnSc6d1	zvané
Aztlán	Aztlán	k1gInSc1	Aztlán
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
přesná	přesný	k2eAgFnSc1d1	přesná
poloha	poloha	k1gFnSc1	poloha
dodnes	dodnes	k6eAd1	dodnes
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
Odtamtud	odtamtud	k6eAd1	odtamtud
putovali	putovat	k5eAaImAgMnP	putovat
na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
svého	svůj	k3xOyFgMnSc2	svůj
boha	bůh	k1gMnSc2	bůh
Huitzilopochtli	Huitzilopochtle	k1gFnSc3	Huitzilopochtle
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jihozápad	jihozápad	k1gInSc4	jihozápad
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c6	po
několika	několik	k4yIc6	několik
zastaveních	zastavení	k1gNnPc6	zastavení
dorazili	dorazit	k5eAaPmAgMnP	dorazit
do	do	k7c2	do
místa	místo	k1gNnSc2	místo
dnešního	dnešní	k2eAgNnSc2d1	dnešní
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
kněží	kněz	k1gMnPc1	kněz
vyslovili	vyslovit	k5eAaPmAgMnP	vyslovit
proroctví	proroctví	k1gNnSc4	proroctví
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mají	mít	k5eAaImIp3nP	mít
usadit	usadit	k5eAaPmF	usadit
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
uvidí	uvidět	k5eAaPmIp3nS	uvidět
orla	orel	k1gMnSc4	orel
sedět	sedět	k5eAaImF	sedět
na	na	k7c4	na
opuncii	opuncie	k1gFnSc4	opuncie
a	a	k8xC	a
zápasit	zápasit	k5eAaImF	zápasit
s	s	k7c7	s
hadem	had	k1gMnSc7	had
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
místo	místo	k1gNnSc1	místo
bylo	být	k5eAaImAgNnS	být
nakonec	nakonec	k6eAd1	nakonec
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
jezera	jezero	k1gNnSc2	jezero
Texcoco	Texcoco	k6eAd1	Texcoco
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Aztékové	Azték	k1gMnPc1	Azték
začali	začít	k5eAaPmAgMnP	začít
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
mýtického	mýtický	k2eAgMnSc2d1	mýtický
vůdce	vůdce	k1gMnSc2	vůdce
Tenocha	Tenoch	k1gMnSc2	Tenoch
stavět	stavět	k5eAaImF	stavět
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Tenochitlánu	Tenochitlán	k2eAgFnSc4d1	Tenochitlán
je	být	k5eAaImIp3nS	být
odvozené	odvozený	k2eAgNnSc1d1	odvozené
z	z	k7c2	z
pojmenování	pojmenování	k1gNnSc2	pojmenování
postavy	postava	k1gFnSc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
aztécké	aztécký	k2eAgNnSc1d1	aztécké
osídlení	osídlení	k1gNnSc1	osídlení
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
přibližně	přibližně	k6eAd1	přibližně
datovat	datovat	k5eAaImF	datovat
mezi	mezi	k7c7	mezi
roky	rok	k1gInPc7	rok
1320	[number]	k4	1320
až	až	k9	až
1350	[number]	k4	1350
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
předtím	předtím	k6eAd1	předtím
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
budoucího	budoucí	k2eAgNnSc2d1	budoucí
města	město	k1gNnSc2	město
jiné	jiný	k2eAgNnSc4d1	jiné
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
po	po	k7c6	po
založení	založení	k1gNnSc6	založení
města	město	k1gNnSc2	město
bylo	být	k5eAaImAgNnS	být
severně	severně	k6eAd1	severně
od	od	k7c2	od
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
založeno	založen	k2eAgNnSc4d1	založeno
další	další	k2eAgNnSc4d1	další
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
,	,	kIx,	,
Tlatelolco	Tlatelolco	k1gNnSc4	Tlatelolco
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
města	město	k1gNnPc4	město
spolu	spolu	k6eAd1	spolu
politicky	politicky	k6eAd1	politicky
jednala	jednat	k5eAaImAgFnS	jednat
a	a	k8xC	a
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
i	i	k9	i
otevřené	otevřený	k2eAgNnSc1d1	otevřené
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1473	[number]	k4	1473
vládce	vládce	k1gMnSc1	vládce
Tlatelolca	Tlatelolca	k1gMnSc1	Tlatelolca
<g/>
,	,	kIx,	,
Moquixhuix	Moquixhuix	k1gInSc1	Moquixhuix
<g/>
,	,	kIx,	,
zabit	zabit	k2eAgMnSc1d1	zabit
a	a	k8xC	a
Tlatelolcu	Tlatelolc	k1gMnSc3	Tlatelolc
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
vládlo	vládnout	k5eAaImAgNnS	vládnout
z	z	k7c2	z
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
historicky	historicky	k6eAd1	historicky
doloženým	doložený	k2eAgMnSc7d1	doložený
vládcem	vládce	k1gMnSc7	vládce
je	být	k5eAaImIp3nS	být
král	král	k1gMnSc1	král
Acamapichtli	Acamapichtle	k1gFnSc4	Acamapichtle
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vládl	vládnout	k5eAaImAgInS	vládnout
koncem	koncem	k7c2	koncem
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
nástupce	nástupce	k1gMnSc1	nástupce
Huitzilíhuitl	Huitzilíhuitl	k1gMnSc1	Huitzilíhuitl
začal	začít	k5eAaPmAgMnS	začít
s	s	k7c7	s
integrací	integrace	k1gFnSc7	integrace
mladšího	mladý	k2eAgNnSc2d2	mladší
města	město	k1gNnSc2	město
do	do	k7c2	do
místních	místní	k2eAgFnPc2d1	místní
mocenských	mocenský	k2eAgFnPc2d1	mocenská
struktur	struktura	k1gFnPc2	struktura
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
se	s	k7c7	s
dcerou	dcera	k1gFnSc7	dcera
vládce	vládce	k1gMnSc2	vládce
Tlatelolca	Tlatelolcus	k1gMnSc2	Tlatelolcus
<g/>
,	,	kIx,	,
čím	čí	k3xOyQgFnPc3	čí
nastalo	nastat	k5eAaPmAgNnS	nastat
i	i	k9	i
odpuštění	odpuštění	k1gNnSc4	odpuštění
daňových	daňový	k2eAgInPc2d1	daňový
poplatků	poplatek	k1gInPc2	poplatek
a	a	k8xC	a
určitá	určitý	k2eAgFnSc1d1	určitá
rovnováha	rovnováha	k1gFnSc1	rovnováha
s	s	k7c7	s
jinými	jiný	k2eAgNnPc7d1	jiné
městy	město	k1gNnPc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Chimalpopoca	Chimalpopoca	k1gMnSc1	Chimalpopoca
<g/>
,	,	kIx,	,
Huitzilíhuitlův	Huitzilíhuitlův	k2eAgMnSc1d1	Huitzilíhuitlův
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
jako	jako	k9	jako
mladistvý	mladistvý	k2eAgMnSc1d1	mladistvý
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
Maxtlou	Maxtlý	k2eAgFnSc7d1	Maxtlý
<g/>
,	,	kIx,	,
vládcem	vládce	k1gMnSc7	vládce
Azcapotzalca	Azcapotzalcus	k1gMnSc2	Azcapotzalcus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jeho	jeho	k3xOp3gMnSc1	jeho
strýc	strýc	k1gMnSc1	strýc
Itzcóatl	Itzcóatl	k1gMnSc1	Itzcóatl
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
Maxtlu	Maxtla	k1gMnSc4	Maxtla
podrobil	podrobit	k5eAaPmAgMnS	podrobit
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
jiná	jiný	k2eAgNnPc1d1	jiné
města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterých	který	k3yRgFnPc2	který
dosadil	dosadit	k5eAaPmAgMnS	dosadit
své	svůj	k3xOyFgMnPc4	svůj
panovníky	panovník	k1gMnPc4	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
až	až	k9	až
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
průběhu	průběh	k1gInSc6	průběh
dobyl	dobýt	k5eAaPmAgInS	dobýt
Texcoco	Texcoco	k1gNnSc4	Texcoco
a	a	k8xC	a
tepanenské	tepanenský	k2eAgNnSc4d1	tepanenský
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Azcapotzalco	Azcapotzalco	k6eAd1	Azcapotzalco
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
si	se	k3xPyFc3	se
slíbila	slíbit	k5eAaPmAgFnS	slíbit
spojenecká	spojenecký	k2eAgNnPc1d1	spojenecké
města	město	k1gNnPc4	město
Tenochtitlán	Tenochtitlán	k2eAgInSc4d1	Tenochtitlán
<g/>
,	,	kIx,	,
Tlacopan	Tlacopan	k1gInSc4	Tlacopan
a	a	k8xC	a
Texcoco	Texcoco	k6eAd1	Texcoco
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
věrnost	věrnost	k1gFnSc4	věrnost
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
aztécký	aztécký	k2eAgInSc4d1	aztécký
trojspolek	trojspolek	k1gInSc4	trojspolek
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
porážkou	porážka	k1gFnSc7	porážka
Tepaneků	Tepanek	k1gInPc2	Tepanek
byla	být	k5eAaImAgFnS	být
aztécká	aztécký	k2eAgFnSc1d1	aztécká
nadvláda	nadvláda	k1gFnSc1	nadvláda
nad	nad	k7c7	nad
údolím	údolí	k1gNnSc7	údolí
Mexika	Mexiko	k1gNnSc2	Mexiko
definitivně	definitivně	k6eAd1	definitivně
zajištěna	zajistit	k5eAaPmNgFnS	zajistit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vládnutí	vládnutí	k1gNnSc2	vládnutí
Itzcoátla	Itzcoátla	k1gFnSc1	Itzcoátla
došlo	dojít	k5eAaPmAgNnS	dojít
i	i	k9	i
v	v	k7c4	v
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
k	k	k7c3	k
rozsáhlým	rozsáhlý	k2eAgFnPc3d1	rozsáhlá
změnám	změna	k1gFnPc3	změna
<g/>
.	.	kIx.	.
</s>
<s>
Stavbou	stavba	k1gFnSc7	stavba
viaduktu	viadukt	k1gInSc2	viadukt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dodával	dodávat	k5eAaImAgInS	dodávat
do	do	k7c2	do
města	město	k1gNnSc2	město
pitnou	pitný	k2eAgFnSc4d1	pitná
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
a	a	k8xC	a
tří	tři	k4xCgFnPc2	tři
nových	nový	k2eAgFnPc2d1	nová
cest	cesta	k1gFnPc2	cesta
po	po	k7c6	po
hrázích	hráz	k1gFnPc6	hráz
bylo	být	k5eAaImAgNnS	být
posíleno	posílen	k2eAgNnSc1d1	posíleno
spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
pevninou	pevnina	k1gFnSc7	pevnina
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
zajištěno	zajištěn	k2eAgNnSc1d1	zajištěno
zásobování	zásobování	k1gNnSc1	zásobování
rostoucího	rostoucí	k2eAgNnSc2d1	rostoucí
města	město	k1gNnSc2	město
pitnou	pitný	k2eAgFnSc7d1	pitná
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
půdy	půda	k1gFnSc2	půda
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
privilegiem	privilegium	k1gNnSc7	privilegium
šlechty	šlechta	k1gFnSc2	šlechta
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
symbolem	symbol	k1gInSc7	symbol
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
příkaz	příkaz	k1gInSc4	příkaz
Itzcoátla	Itzcoátla	k1gMnPc2	Itzcoátla
bylo	být	k5eAaImAgNnS	být
zničeno	zničit	k5eAaPmNgNnS	zničit
mnoho	mnoho	k4c1	mnoho
starých	starý	k2eAgInPc2d1	starý
obrazových	obrazový	k2eAgInPc2d1	obrazový
rukopisů	rukopis	k1gInPc2	rukopis
<g/>
;	;	kIx,	;
důvod	důvod	k1gInSc4	důvod
moderním	moderní	k2eAgInSc7d1	moderní
výzkumem	výzkum	k1gInSc7	výzkum
nebyl	být	k5eNaImAgInS	být
zjištěn	zjistit	k5eAaPmNgInS	zjistit
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
sporu	spor	k1gInSc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobný	pravděpodobný	k2eAgInSc1d1	pravděpodobný
důvod	důvod	k1gInSc1	důvod
spálení	spálení	k1gNnSc2	spálení
starých	starý	k2eAgInPc2d1	starý
rukopisů	rukopis	k1gInPc2	rukopis
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgInSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Itzcoátl	Itzcoátl	k1gMnSc1	Itzcoátl
chtěl	chtít	k5eAaImAgMnS	chtít
posílit	posílit	k5eAaPmF	posílit
stávající	stávající	k2eAgInPc4d1	stávající
mocenské	mocenský	k2eAgInPc4d1	mocenský
poměry	poměr	k1gInPc4	poměr
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
legitimovat	legitimovat	k5eAaBmF	legitimovat
existenci	existence	k1gFnSc4	existence
své	svůj	k3xOyFgFnSc2	svůj
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Itzcóatla	Itzcóatla	k1gFnSc2	Itzcóatla
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1440	[number]	k4	1440
se	se	k3xPyFc4	se
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
dostal	dostat	k5eAaPmAgMnS	dostat
Moctezuma	Moctezum	k1gMnSc4	Moctezum
I.	I.	kA	I.
V	v	k7c6	v
době	doba	k1gFnSc6	doba
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
nastalo	nastat	k5eAaPmAgNnS	nastat
více	hodně	k6eAd2	hodně
přírodních	přírodní	k2eAgFnPc2d1	přírodní
katastrof	katastrofa	k1gFnPc2	katastrofa
<g/>
:	:	kIx,	:
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1446	[number]	k4	1446
až	až	k9	až
1450	[number]	k4	1450
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přemnožení	přemnožení	k1gNnSc3	přemnožení
kobylek	kobylka	k1gFnPc2	kobylka
<g/>
,	,	kIx,	,
povodni	povodeň	k1gFnSc6	povodeň
a	a	k8xC	a
hladomoru	hladomor	k1gInSc6	hladomor
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
poslední	poslední	k2eAgFnSc1d1	poslední
událost	událost	k1gFnSc1	událost
donutila	donutit	k5eAaPmAgFnS	donutit
mnoho	mnoho	k4c4	mnoho
obyvatel	obyvatel	k1gMnPc2	obyvatel
k	k	k7c3	k
opuštění	opuštění	k1gNnSc3	opuštění
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Sice	sice	k8xC	sice
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
zabránění	zabránění	k1gNnSc3	zabránění
další	další	k2eAgFnSc2d1	další
povodně	povodeň	k1gFnSc2	povodeň
východně	východně	k6eAd1	východně
od	od	k7c2	od
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
postavena	postaven	k2eAgFnSc1d1	postavena
16	[number]	k4	16
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
hráz	hráz	k1gFnSc1	hráz
přes	přes	k7c4	přes
jezero	jezero	k1gNnSc4	jezero
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
zjevné	zjevný	k2eAgNnSc1d1	zjevné
<g/>
,	,	kIx,	,
že	že	k8xS	že
město	město	k1gNnSc1	město
není	být	k5eNaImIp3nS	být
schopno	schopen	k2eAgNnSc1d1	schopno
rostoucí	rostoucí	k2eAgInSc4d1	rostoucí
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
uživit	uživit	k5eAaPmF	uživit
vlastními	vlastní	k2eAgFnPc7d1	vlastní
silami	síla	k1gFnPc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
zahájil	zahájit	k5eAaPmAgMnS	zahájit
Moctezuma	Moctezuma	k1gFnSc1	Moctezuma
vojenská	vojenský	k2eAgFnSc1d1	vojenská
tažení	tažení	k1gNnSc6	tažení
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
podmanil	podmanit	k5eAaPmAgMnS	podmanit
tamější	tamější	k2eAgInPc4d1	tamější
městské	městský	k2eAgInPc4d1	městský
státy	stát	k1gInPc4	stát
a	a	k8xC	a
mohl	moct	k5eAaImAgMnS	moct
jim	on	k3xPp3gMnPc3	on
uložit	uložit	k5eAaPmF	uložit
daně	daň	k1gFnSc2	daň
<g/>
.	.	kIx.	.
</s>
<s>
Kdo	kdo	k3yQnSc1	kdo
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1469	[number]	k4	1469
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Moctezumy	Moctezum	k1gInPc1	Moctezum
I.	I.	kA	I.
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
ale	ale	k9	ale
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1471	[number]	k4	1471
po	po	k7c6	po
dlouhých	dlouhý	k2eAgNnPc6d1	dlouhé
jednáních	jednání	k1gNnPc6	jednání
nastala	nastat	k5eAaPmAgFnS	nastat
shoda	shoda	k1gFnSc1	shoda
na	na	k7c6	na
Axayacatlovi	Axayacatl	k1gMnSc6	Axayacatl
<g/>
,	,	kIx,	,
jednom	jeden	k4xCgMnSc6	jeden
z	z	k7c2	z
vnuků	vnuk	k1gMnPc2	vnuk
Itzcóatla	Itzcóatla	k1gFnSc2	Itzcóatla
<g/>
.	.	kIx.	.
</s>
<s>
Definitivně	definitivně	k6eAd1	definitivně
podmanil	podmanit	k5eAaPmAgMnS	podmanit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1473	[number]	k4	1473
sousední	sousední	k2eAgInSc4d1	sousední
město	město	k1gNnSc1	město
Tlatelolco	Tlatelolco	k1gNnSc1	Tlatelolco
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
stálo	stát	k5eAaImAgNnS	stát
více	hodně	k6eAd2	hodně
stávalo	stávat	k5eAaImAgNnS	stávat
součástí	součást	k1gFnPc2	součást
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
<g/>
,	,	kIx,	,
a	a	k8xC	a
při	při	k7c6	při
španělském	španělský	k2eAgNnSc6d1	španělské
dobytí	dobytí	k1gNnSc6	dobytí
Mexika	Mexiko	k1gNnSc2	Mexiko
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
plně	plně	k6eAd1	plně
součástí	součást	k1gFnSc7	součást
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dobytím	dobytí	k1gNnSc7	dobytí
města	město	k1gNnSc2	město
získali	získat	k5eAaPmAgMnP	získat
Aztékové	Azték	k1gMnPc1	Azték
také	také	k9	také
kontrolu	kontrola	k1gFnSc4	kontrola
na	na	k7c4	na
důležitým	důležitý	k2eAgNnSc7d1	důležité
obchodním	obchodní	k2eAgNnSc7d1	obchodní
střediskem	středisko	k1gNnSc7	středisko
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Axayacatlova	Axayacatlův	k2eAgMnSc2d1	Axayacatlův
bratra	bratr	k1gMnSc2	bratr
Tízoce	Tízoce	k1gMnSc2	Tízoce
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
s	s	k7c7	s
rozsáhlým	rozsáhlý	k2eAgNnSc7d1	rozsáhlé
rozšířením	rozšíření	k1gNnSc7	rozšíření
chrámu	chrám	k1gInSc2	chrám
Templo	Templo	k1gFnPc2	Templo
Mayor	Mayor	k1gInSc1	Mayor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
centrálním	centrální	k2eAgInSc7d1	centrální
chrámem	chrám	k1gInSc7	chrám
města	město	k1gNnSc2	město
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zasvěcen	zasvětit	k5eAaPmNgInS	zasvětit
bohům	bůh	k1gMnPc3	bůh
Tlalocovi	Tlaloec	k1gMnSc3	Tlaloec
a	a	k8xC	a
Huitzilopochtliovi	Huitzilopochtlius	k1gMnSc3	Huitzilopochtlius
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
zasvěcení	zasvěcení	k1gNnSc6	zasvěcení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
uskutečněno	uskutečněn	k2eAgNnSc1d1	uskutečněno
za	za	k7c2	za
příštího	příští	k2eAgMnSc2d1	příští
krále	král	k1gMnSc2	král
Auítzotla	Auítzotla	k1gMnSc2	Auítzotla
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
čtyř	čtyři	k4xCgInPc2	čtyři
dnů	den	k1gInPc2	den
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
obětováno	obětován	k2eAgNnSc1d1	obětováno
bohům	bůh	k1gMnPc3	bůh
až	až	k9	až
dvacet	dvacet	k4xCc1	dvacet
tisíc	tisíc	k4xCgInSc1	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Auítzotlovi	Auítzotl	k1gMnSc3	Auítzotl
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
podmanění	podmanění	k1gNnSc4	podmanění
vzdálené	vzdálený	k2eAgFnSc2d1	vzdálená
provincie	provincie	k1gFnSc2	provincie
Xoconochco	Xoconochco	k6eAd1	Xoconochco
poblíž	poblíž	k7c2	poblíž
dnešních	dnešní	k2eAgFnPc2d1	dnešní
hranic	hranice	k1gFnPc2	hranice
Mexika	Mexiko	k1gNnSc2	Mexiko
a	a	k8xC	a
Guatemaly	Guatemala	k1gFnSc2	Guatemala
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
vybudovat	vybudovat	k5eAaPmF	vybudovat
další	další	k2eAgInSc4d1	další
akvadukt	akvadukt	k1gInSc4	akvadukt
do	do	k7c2	do
města	město	k1gNnSc2	město
z	z	k7c2	z
jezera	jezero	k1gNnSc2	jezero
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
bylo	být	k5eAaImAgNnS	být
aztécké	aztécký	k2eAgNnSc4d1	aztécké
hlavní	hlavní	k2eAgNnSc4d1	hlavní
mělo	mít	k5eAaImAgNnS	mít
zaplaveno	zaplaven	k2eAgNnSc1d1	zaplaveno
a	a	k8xC	a
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
zničeno	zničen	k2eAgNnSc1d1	zničeno
<g/>
.	.	kIx.	.
</s>
<s>
Vybudování	vybudování	k1gNnSc1	vybudování
vodovodu	vodovod	k1gInSc2	vodovod
museli	muset	k5eAaImAgMnP	muset
obyvatelé	obyvatel	k1gMnPc1	obyvatel
vzdát	vzdát	k5eAaPmF	vzdát
a	a	k8xC	a
samotný	samotný	k2eAgMnSc1d1	samotný
Tenochtitlán	Tenochtitlán	k2eAgMnSc1d1	Tenochtitlán
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
prakticky	prakticky	k6eAd1	prakticky
postavit	postavit	k5eAaPmF	postavit
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1502	[number]	k4	1502
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
králem	král	k1gMnSc7	král
Aztéků	Azték	k1gMnPc2	Azték
Montezuma	Montezumum	k1gNnSc2	Montezumum
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Podnikl	podniknout	k5eAaPmAgInS	podniknout
vojenská	vojenský	k2eAgNnPc4d1	vojenské
tažení	tažení	k1gNnSc4	tažení
jak	jak	k8xC	jak
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
tak	tak	k9	tak
i	i	k9	i
na	na	k7c4	na
jih	jih	k1gInSc4	jih
do	do	k7c2	do
provincie	provincie	k1gFnSc2	provincie
Oaxaca	Oaxac	k1gInSc2	Oaxac
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
posílil	posílit	k5eAaPmAgInS	posílit
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
pozici	pozice	k1gFnSc4	pozice
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
vládce	vládce	k1gMnSc1	vládce
také	také	k9	také
vědomě	vědomě	k6eAd1	vědomě
zvětšil	zvětšit	k5eAaPmAgInS	zvětšit
propast	propast	k1gFnSc4	propast
mezi	mezi	k7c7	mezi
šlechtou	šlechta	k1gFnSc7	šlechta
a	a	k8xC	a
lidem	lid	k1gInSc7	lid
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
propustil	propustit	k5eAaPmAgInS	propustit
mnoho	mnoho	k4c4	mnoho
nešlechticů	nešlechtiec	k1gInPc2	nešlechtiec
ze	z	k7c2	z
státních	státní	k2eAgFnPc2d1	státní
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgInSc1d1	známý
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
Moctezuma	Moctezuma	k1gFnSc1	Moctezuma
II	II	kA	II
<g/>
.	.	kIx.	.
především	především	k9	především
svou	svůj	k3xOyFgFnSc7	svůj
rolí	role	k1gFnSc7	role
při	při	k7c6	při
dobytí	dobytí	k1gNnSc6	dobytí
Mexika	Mexiko	k1gNnSc2	Mexiko
Španěly	Španěly	k1gInPc1	Španěly
<g/>
.	.	kIx.	.
</s>
<s>
Dopustil	dopustit	k5eAaPmAgMnS	dopustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1519	[number]	k4	1519
mohli	moct	k5eAaImAgMnP	moct
Španělé	Španěl	k1gMnPc1	Španěl
v	v	k7c4	v
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
usadit	usadit	k5eAaPmF	usadit
<g/>
,	,	kIx,	,
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
si	se	k3xPyFc3	se
jimi	on	k3xPp3gMnPc7	on
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
postavení	postavení	k1gNnSc2	postavení
loutkového	loutkový	k2eAgMnSc2d1	loutkový
vládce	vládce	k1gMnSc2	vládce
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rebelii	rebelie	k1gFnSc6	rebelie
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yRgFnSc2	který
byl	být	k5eAaImAgMnS	být
Moctezuma	Moctezum	k1gMnSc4	Moctezum
II	II	kA	II
<g/>
.	.	kIx.	.
zabit	zabit	k2eAgMnSc1d1	zabit
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Španělé	Španěl	k1gMnPc1	Španěl
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
tlaxcaltekskými	tlaxcaltekský	k2eAgMnPc7d1	tlaxcaltekský
spojenci	spojenec	k1gMnPc7	spojenec
vyhnáni	vyhnat	k5eAaPmNgMnP	vyhnat
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
velkých	velký	k2eAgFnPc2d1	velká
ztrát	ztráta	k1gFnPc2	ztráta
z	z	k7c2	z
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c4	za
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
s	s	k7c7	s
posilami	posila	k1gFnPc7	posila
z	z	k7c2	z
vlasti	vlast	k1gFnSc2	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
během	během	k7c2	během
obléhání	obléhání	k1gNnSc2	obléhání
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
hlady	hlady	k6eAd1	hlady
a	a	k8xC	a
na	na	k7c4	na
zavlečené	zavlečený	k2eAgFnPc4d1	zavlečená
choroby	choroba	k1gFnPc4	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
lodí	loď	k1gFnPc2	loď
a	a	k8xC	a
děl	dělo	k1gNnPc2	dělo
přerušili	přerušit	k5eAaPmAgMnP	přerušit
španělští	španělský	k2eAgMnPc1d1	španělský
dobyvatelé	dobyvatel	k1gMnPc1	dobyvatel
zásobování	zásobování	k1gNnSc2	zásobování
města	město	k1gNnSc2	město
kanoemi	kanoe	k1gFnPc7	kanoe
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
frontálním	frontální	k2eAgInSc6d1	frontální
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
hráze	hráz	k1gFnPc4	hráz
vtáhli	vtáhnout	k5eAaPmAgMnP	vtáhnout
obránci	obránce	k1gMnPc1	obránce
města	město	k1gNnSc2	město
Španěly	Španěl	k1gMnPc4	Španěl
do	do	k7c2	do
trpkých	trpký	k2eAgInPc2d1	trpký
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
každý	každý	k3xTgInSc4	každý
dům	dům	k1gInSc4	dům
<g/>
.	.	kIx.	.
</s>
<s>
Dobyvatelé	dobyvatel	k1gMnPc1	dobyvatel
poté	poté	k6eAd1	poté
zbořili	zbořit	k5eAaPmAgMnP	zbořit
všechny	všechen	k3xTgInPc4	všechen
domy	dům	k1gInPc4	dům
v	v	k7c4	v
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
a	a	k8xC	a
dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1521	[number]	k4	1521
zlomili	zlomit	k5eAaPmAgMnP	zlomit
s	s	k7c7	s
konečnou	konečný	k2eAgFnSc7d1	konečná
platností	platnost	k1gFnSc7	platnost
odpor	odpor	k1gInSc1	odpor
Tlatelolca	Tlatelolc	k1gInSc2	Tlatelolc
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
král	král	k1gMnSc1	král
Cuauhtémoc	Cuauhtémoc	k1gInSc4	Cuauhtémoc
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
útěku	útěk	k1gInSc6	útěk
chycen	chytit	k5eAaPmNgMnS	chytit
v	v	k7c4	v
kanoi	kanoe	k1gFnSc4	kanoe
a	a	k8xC	a
vzat	vzít	k5eAaPmNgMnS	vzít
do	do	k7c2	do
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
vítězství	vítězství	k1gNnSc6	vítězství
dovolil	dovolit	k5eAaPmAgInS	dovolit
Hernán	Hernán	k2eAgInSc1d1	Hernán
Cortés	Cortés	k1gInSc1	Cortés
obyvatelům	obyvatel	k1gMnPc3	obyvatel
města	město	k1gNnSc2	město
volný	volný	k2eAgInSc4d1	volný
odchod	odchod	k1gInSc4	odchod
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
s	s	k7c7	s
opravou	oprava	k1gFnSc7	oprava
ulic	ulice	k1gFnPc2	ulice
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
obnoveno	obnoven	k2eAgNnSc1d1	obnoveno
zásobování	zásobování	k1gNnSc1	zásobování
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Kanály	kanál	k1gInPc1	kanál
byly	být	k5eAaImAgInP	být
zasypávány	zasypávat	k5eAaImNgInP	zasypávat
ruinami	ruina	k1gFnPc7	ruina
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Španělé	Španěl	k1gMnPc1	Španěl
dovolili	dovolit	k5eAaPmAgMnP	dovolit
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
měsících	měsíc	k1gInPc6	měsíc
návrat	návrat	k1gInSc4	návrat
obyvatel	obyvatel	k1gMnPc2	obyvatel
do	do	k7c2	do
zničeného	zničený	k2eAgNnSc2d1	zničené
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
sami	sám	k3xTgMnPc1	sám
usadili	usadit	k5eAaPmAgMnP	usadit
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
bývalého	bývalý	k2eAgNnSc2d1	bývalé
hlavního	hlavní	k2eAgNnSc2d1	hlavní
aztéckého	aztécký	k2eAgNnSc2d1	aztécké
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
obnovy	obnova	k1gFnSc2	obnova
původních	původní	k2eAgFnPc2d1	původní
budov	budova	k1gFnPc2	budova
se	se	k3xPyFc4	se
postavily	postavit	k5eAaPmAgFnP	postavit
nové	nový	k2eAgFnPc1d1	nová
budovy	budova	k1gFnPc1	budova
z	z	k7c2	z
trosek	troska	k1gFnPc2	troska
ruin	ruina	k1gFnPc2	ruina
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
palác	palác	k1gInSc1	palác
španělských	španělský	k2eAgMnPc2d1	španělský
vicekrálů	vicekrál	k1gMnPc2	vicekrál
ze	z	k7c2	z
zbytků	zbytek	k1gInPc2	zbytek
paláce	palác	k1gInSc2	palác
Moctezumy	Moctezum	k1gInPc7	Moctezum
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
katedrála	katedrála	k1gFnSc1	katedrála
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
před	před	k7c7	před
bývalým	bývalý	k2eAgMnSc7d1	bývalý
Templo	Templo	k1gMnSc7	Templo
Mayor	Mayora	k1gFnPc2	Mayora
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ustanovením	ustanovení	k1gNnSc7	ustanovení
vicekrálovstí	vicekrálovstit	k5eAaImIp3nS	vicekrálovstit
Nové	Nové	k2eAgNnSc1d1	Nové
Španělsko	Španělsko	k1gNnSc1	Španělsko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Tenochtitlán	Tenochtitlán	k2eAgInSc1d1	Tenochtitlán
nakonec	nakonec	k6eAd1	nakonec
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
nového	nový	k2eAgNnSc2d1	nové
vicekrálovství	vicekrálovství	k1gNnSc2	vicekrálovství
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c4	na
Ciudad	Ciudad	k1gInSc4	Ciudad
de	de	k?	de
México	México	k1gNnSc1	México
(	(	kIx(	(
<g/>
Mexiko	Mexiko	k1gNnSc1	Mexiko
City	city	k1gNnSc1	city
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
znalostí	znalost	k1gFnPc2	znalost
o	o	k7c4	o
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
zpráv	zpráva	k1gFnPc2	zpráva
od	od	k7c2	od
conquistadorů	conquistador	k1gMnPc2	conquistador
<g/>
,	,	kIx,	,
mnichů	mnich	k1gMnPc2	mnich
a	a	k8xC	a
od	od	k7c2	od
indiánských	indiánský	k2eAgMnPc2d1	indiánský
autorů	autor	k1gMnPc2	autor
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
hned	hned	k6eAd1	hned
po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
postaveno	postaven	k2eAgNnSc4d1	postaveno
na	na	k7c6	na
ruinách	ruina	k1gFnPc6	ruina
starého	staré	k1gNnSc2	staré
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
materiální	materiální	k2eAgInPc1d1	materiální
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
zcela	zcela	k6eAd1	zcela
překryty	překrýt	k5eAaPmNgFnP	překrýt
moderním	moderní	k2eAgNnSc7d1	moderní
městem	město	k1gNnSc7	město
a	a	k8xC	a
archeologické	archeologický	k2eAgFnSc2d1	archeologická
vykopávky	vykopávka	k1gFnSc2	vykopávka
jsou	být	k5eAaImIp3nP	být
možné	možný	k2eAgInPc1d1	možný
jen	jen	k9	jen
na	na	k7c6	na
několika	několik	k4yIc6	několik
volných	volný	k2eAgInPc6d1	volný
místech	místo	k1gNnPc6	místo
a	a	k8xC	a
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
novostavbami	novostavba	k1gFnPc7	novostavba
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
aztéckých	aztécký	k2eAgFnPc2d1	aztécká
budov	budova	k1gFnPc2	budova
zakomponované	zakomponovaný	k2eAgInPc1d1	zakomponovaný
do	do	k7c2	do
nových	nový	k2eAgFnPc2d1	nová
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
starého	starý	k2eAgNnSc2d1	staré
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
nenávratně	návratně	k6eNd1	návratně
ztracena	ztratit	k5eAaPmNgFnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Silniční	silniční	k2eAgFnSc4d1	silniční
a	a	k8xC	a
kanalizační	kanalizační	k2eAgFnSc4d1	kanalizační
síť	síť	k1gFnSc4	síť
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
nicméně	nicméně	k8xC	nicméně
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
moderních	moderní	k2eAgFnPc6d1	moderní
ulicích	ulice	k1gFnPc6	ulice
města	město	k1gNnSc2	město
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
zachována	zachovat	k5eAaPmNgFnS	zachovat
<g/>
.	.	kIx.	.
</s>
<s>
Dobrým	dobrý	k2eAgInSc7d1	dobrý
příkladem	příklad	k1gInSc7	příklad
stálost	stálost	k1gFnSc1	stálost
uliční	uliční	k2eAgFnSc2d1	uliční
sítě	síť	k1gFnSc2	síť
je	být	k5eAaImIp3nS	být
poloha	poloha	k1gFnSc1	poloha
čtyř	čtyři	k4xCgInPc2	čtyři
chrámů	chrám	k1gInPc2	chrám
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
čtvrtí	čtvrt	k1gFnPc2	čtvrt
Tenochtitlánu	Tenochtitlán	k2eAgFnSc4d1	Tenochtitlán
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
místa	místo	k1gNnPc1	místo
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
moderním	moderní	k2eAgNnSc6d1	moderní
městě	město	k1gNnSc6	město
stále	stále	k6eAd1	stále
zřetelná	zřetelný	k2eAgFnSc1d1	zřetelná
včetně	včetně	k7c2	včetně
přístupových	přístupový	k2eAgFnPc2d1	přístupová
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
malých	malý	k2eAgNnPc2d1	malé
náměstí	náměstí	k1gNnPc2	náměstí
<g/>
:	:	kIx,	:
Zoquiapan	Zoquiapan	k1gInSc1	Zoquiapan
–	–	k?	–
Plaza	plaz	k1gMnSc2	plaz
San	San	k1gMnSc2	San
Lucas	Lucas	k1gMnSc1	Lucas
Moyotla	Moyotla	k1gMnSc1	Moyotla
–	–	k?	–
Calle	Calle	k1gFnSc2	Calle
Victoria	Victorium	k1gNnSc2	Victorium
Cuepopan	Cuepopan	k1gMnSc1	Cuepopan
–	–	k?	–
Calle	Calle	k1gInSc1	Calle
Obispo	Obispa	k1gFnSc5	Obispa
Atzacualco	Atzacualco	k1gNnSc1	Atzacualco
–	–	k?	–
Plaza	plaz	k1gMnSc2	plaz
del	del	k?	del
Estudiante	Estudiant	k1gMnSc5	Estudiant
V	v	k7c6	v
průběhu	průběh	k1gInSc2	průběh
staletí	staletí	k1gNnPc2	staletí
byly	být	k5eAaImAgInP	být
nacházeny	nacházen	k2eAgInPc1d1	nacházen
relikty	relikt	k1gInPc1	relikt
aztécké	aztécký	k2eAgFnSc2d1	aztécká
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
kamenné	kamenný	k2eAgInPc1d1	kamenný
monumenty	monument	k1gInPc1	monument
a	a	k8xC	a
keramika	keramika	k1gFnSc1	keramika
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nim	on	k3xPp3gInPc3	on
patří	patřit	k5eAaImIp3nS	patřit
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
části	část	k1gFnSc6	část
Palacio	Palacio	k6eAd1	Palacio
Nacional	Nacional	k1gFnSc7	Nacional
nalezená	nalezený	k2eAgFnSc1d1	nalezená
monumentální	monumentální	k2eAgFnSc1d1	monumentální
socha	socha	k1gFnSc1	socha
bohyně	bohyně	k1gFnSc1	bohyně
Coatlicue	Coatlicue	k1gFnSc1	Coatlicue
a	a	k8xC	a
Teocalli	Teocall	k1gMnPc1	Teocall
de	de	k?	de
la	la	k1gNnSc2	la
Guerra	Guerra	k1gFnSc1	Guerra
Sagrada	Sagrada	k1gFnSc1	Sagrada
<g/>
,	,	kIx,	,
kamenný	kamenný	k2eAgInSc1d1	kamenný
monument	monument	k1gInSc1	monument
mající	mající	k2eAgFnSc4d1	mající
podobu	podoba	k1gFnSc4	podoba
chrámové	chrámový	k2eAgFnSc2d1	chrámová
pyramidy	pyramida	k1gFnSc2	pyramida
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gFnSc6	jehož
zadní	zadní	k2eAgFnSc6d1	zadní
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
zobrazená	zobrazený	k2eAgFnSc1d1	zobrazená
scéna	scéna	k1gFnSc1	scéna
orla	orel	k1gMnSc2	orel
sedícího	sedící	k2eAgMnSc2d1	sedící
na	na	k7c6	na
kaktusu	kaktus	k1gInSc6	kaktus
a	a	k8xC	a
sluneční	sluneční	k2eAgInSc1d1	sluneční
kámen	kámen	k1gInSc1	kámen
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
prvními	první	k4xOgFnPc7	první
systematickými	systematický	k2eAgFnPc7d1	systematická
archeologickými	archeologický	k2eAgFnPc7d1	archeologická
vykopávkami	vykopávka	k1gFnPc7	vykopávka
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
až	až	k9	až
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
archeologické	archeologický	k2eAgFnPc1d1	archeologická
vykopávky	vykopávka	k1gFnPc1	vykopávka
byly	být	k5eAaImAgFnP	být
prováděny	provádět	k5eAaImNgFnP	provádět
při	při	k7c6	při
výkopových	výkopový	k2eAgFnPc6d1	výkopová
pracích	práce	k1gFnPc6	práce
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
inženýrských	inženýrský	k2eAgFnPc2d1	inženýrská
sítí	síť	k1gFnPc2	síť
(	(	kIx(	(
<g/>
elektřina	elektřina	k1gFnSc1	elektřina
<g/>
,	,	kIx,	,
kanalizace	kanalizace	k1gFnSc1	kanalizace
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
linek	linka	k1gFnPc2	linka
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
centrem	centrum	k1gNnSc7	centrum
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
především	především	k9	především
linka	linka	k1gFnSc1	linka
číslo	číslo	k1gNnSc1	číslo
2	[number]	k4	2
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obchází	obcházet	k5eAaImIp3nS	obcházet
Zócalo	Zócala	k1gFnSc5	Zócala
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
severu	sever	k1gInSc6	sever
a	a	k8xC	a
Templo	Templo	k1gMnSc1	Templo
Mayor	Mayor	k1gMnSc1	Mayor
těsně	těsně	k6eAd1	těsně
míjí	míjet	k5eAaImIp3nS	míjet
<g/>
,	,	kIx,	,
a	a	k8xC	a
linka	linka	k1gFnSc1	linka
číslo	číslo	k1gNnSc1	číslo
8	[number]	k4	8
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
divadla	divadlo	k1gNnSc2	divadlo
Palacio	Palacio	k1gMnSc1	Palacio
de	de	k?	de
Bellas	Bellas	k1gMnSc1	Bellas
Artes	Artes	k1gMnSc1	Artes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
stanice	stanice	k1gFnSc2	stanice
Pino	Pino	k6eAd1	Pino
Suárez	Suárez	k1gInSc4	Suárez
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	s	k7c7	s
kříži	kříž	k1gInPc7	kříž
linky	linka	k1gFnSc2	linka
číslo	číslo	k1gNnSc1	číslo
1	[number]	k4	1
a	a	k8xC	a
2	[number]	k4	2
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
obnažen	obnažen	k2eAgInSc1d1	obnažen
malý	malý	k2eAgInSc1d1	malý
kruhový	kruhový	k2eAgInSc1d1	kruhový
chrám	chrám	k1gInSc1	chrám
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zasvěcen	zasvětit	k5eAaPmNgInS	zasvětit
bohu	bůh	k1gMnSc3	bůh
větru	vítr	k1gInSc2	vítr
Ehecatlovi	Ehecatl	k1gMnSc3	Ehecatl
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
rekonstruován	rekonstruován	k2eAgInSc1d1	rekonstruován
a	a	k8xC	a
díky	díky	k7c3	díky
změně	změna	k1gFnSc3	změna
planu	planout	k5eAaImIp1nS	planout
začleněn	začleněn	k2eAgInSc1d1	začleněn
do	do	k7c2	do
stanice	stanice	k1gFnSc2	stanice
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
nálezy	nález	k1gInPc1	nález
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
Zócala	Zócal	k1gMnSc2	Zócal
byly	být	k5eAaImAgFnP	být
zachráněny	zachráněn	k2eAgFnPc1d1	zachráněna
jako	jako	k8xC	jako
kamenné	kamenný	k2eAgInPc1d1	kamenný
bloky	blok	k1gInPc1	blok
a	a	k8xC	a
transportovány	transportován	k2eAgInPc1d1	transportován
do	do	k7c2	do
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Napjatý	napjatý	k2eAgInSc1d1	napjatý
časový	časový	k2eAgInSc1d1	časový
rozvrh	rozvrh	k1gInSc1	rozvrh
stavebních	stavební	k2eAgFnPc2d1	stavební
prací	práce	k1gFnPc2	práce
velice	velice	k6eAd1	velice
stěžuje	stěžovat	k5eAaImIp3nS	stěžovat
archeologické	archeologický	k2eAgFnPc4d1	archeologická
vykopávky	vykopávka	k1gFnPc4	vykopávka
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
celoplošné	celoplošný	k2eAgNnSc4d1	celoplošné
zastavění	zastavění	k1gNnSc4	zastavění
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
archeologické	archeologický	k2eAgFnPc1d1	archeologická
vykopávky	vykopávka	k1gFnPc1	vykopávka
ztíženy	ztížen	k2eAgFnPc1d1	ztížena
také	také	k9	také
geologickými	geologický	k2eAgInPc7d1	geologický
poměry	poměr	k1gInPc7	poměr
<g/>
:	:	kIx,	:
Celé	celý	k2eAgNnSc1d1	celé
centrum	centrum	k1gNnSc1	centrum
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
jsou	být	k5eAaImIp3nP	být
postaveny	postavit	k5eAaPmNgInP	postavit
na	na	k7c6	na
dně	dna	k1gFnSc6	dna
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
vysušeného	vysušený	k2eAgNnSc2d1	vysušené
jezera	jezero	k1gNnSc2	jezero
Texcoco	Texcoco	k6eAd1	Texcoco
<g/>
.	.	kIx.	.
</s>
<s>
Hluboké	hluboký	k2eAgInPc1d1	hluboký
sedimenty	sediment	k1gInPc1	sediment
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
jezera	jezero	k1gNnSc2	jezero
jsou	být	k5eAaImIp3nP	být
příčinou	příčina	k1gFnSc7	příčina
<g/>
,	,	kIx,	,
že	že	k8xS	že
starý	starý	k2eAgInSc1d1	starý
povrch	povrch	k1gInSc1	povrch
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
země	zem	k1gFnSc2	zem
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
spodní	spodní	k2eAgFnSc2d1	spodní
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vykopávky	vykopávka	k1gFnPc4	vykopávka
by	by	kYmCp3nP	by
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
umělé	umělý	k2eAgNnSc1d1	umělé
snížení	snížení	k1gNnSc1	snížení
hladiny	hladina	k1gFnSc2	hladina
spodní	spodní	k2eAgFnSc2d1	spodní
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
by	by	kYmCp3nS	by
ohrozilo	ohrozit	k5eAaPmAgNnS	ohrozit
stabilitu	stabilita	k1gFnSc4	stabilita
okolních	okolní	k2eAgFnPc2d1	okolní
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Účinky	účinek	k1gInPc1	účinek
jsou	být	k5eAaImIp3nP	být
zřetelně	zřetelně	k6eAd1	zřetelně
viditelné	viditelný	k2eAgFnPc1d1	viditelná
například	například	k6eAd1	například
u	u	k7c2	u
katedrály	katedrála	k1gFnSc2	katedrála
v	v	k7c6	v
Zócalu	Zócal	k1gInSc6	Zócal
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vychyluje	vychylovat	k5eAaImIp3nS	vychylovat
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
bezprostředním	bezprostřední	k2eAgNnSc6d1	bezprostřední
sousedství	sousedství	k1gNnSc6	sousedství
stojící	stojící	k2eAgMnSc1d1	stojící
Bastisterio	Bastisterio	k1gMnSc1	Bastisterio
se	se	k3xPyFc4	se
propadá	propadat	k5eAaImIp3nS	propadat
východním	východní	k2eAgInSc7d1	východní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
kostelů	kostel	k1gInPc2	kostel
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nachází	nacházet	k5eAaImIp3nS	nacházet
dva	dva	k4xCgInPc4	dva
metry	metr	k1gInPc4	metr
pod	pod	k7c7	pod
úrovní	úroveň	k1gFnSc7	úroveň
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnPc1d2	veliký
předšpanělské	předšpanělský	k2eAgFnPc1d1	předšpanělský
stavby	stavba	k1gFnPc1	stavba
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
viditelné	viditelný	k2eAgFnPc1d1	viditelná
jako	jako	k8xC	jako
menší	malý	k2eAgInPc1d2	menší
pahorky	pahorek	k1gInPc1	pahorek
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
ulic	ulice	k1gFnPc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Poloha	poloha	k1gFnSc1	poloha
hlavního	hlavní	k2eAgInSc2d1	hlavní
chrámu	chrám	k1gInSc2	chrám
byla	být	k5eAaImAgFnS	být
přibližně	přibližně	k6eAd1	přibližně
určena	určit	k5eAaPmNgFnS	určit
po	po	k7c6	po
menších	malý	k2eAgFnPc6d2	menší
vykopávkách	vykopávka	k1gFnPc6	vykopávka
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c6	na
rohu	roh	k1gInSc6	roh
ulic	ulice	k1gFnPc2	ulice
República	Repúblic	k2eAgFnSc1d1	República
de	de	k?	de
Argentina	Argentina	k1gFnSc1	Argentina
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
otevřená	otevřený	k2eAgFnSc1d1	otevřená
část	část	k1gFnSc1	část
ulice	ulice	k1gFnSc2	ulice
<g/>
)	)	kIx)	)
a	a	k8xC	a
República	Repúblic	k2eAgFnSc1d1	República
de	de	k?	de
Guatemala	Guatemala	k1gFnSc1	Guatemala
<g/>
.	.	kIx.	.
</s>
<s>
Hustá	hustý	k2eAgFnSc1d1	hustá
zástavba	zástavba	k1gFnSc1	zástavba
zde	zde	k6eAd1	zde
však	však	k9	však
znemožnila	znemožnit	k5eAaPmAgFnS	znemožnit
další	další	k2eAgFnPc4d1	další
vykopávky	vykopávka	k1gFnPc4	vykopávka
<g/>
.	.	kIx.	.
</s>
<s>
Náhodný	náhodný	k2eAgInSc4d1	náhodný
nález	nález	k1gInSc4	nález
reliéfu	reliéf	k1gInSc2	reliéf
zobrazujícího	zobrazující	k2eAgInSc2d1	zobrazující
mrtvou	mrtvý	k2eAgFnSc4d1	mrtvá
bohyni	bohyně	k1gFnSc4	bohyně
Coyolxauhqui	Coyolxauhqu	k1gFnSc2	Coyolxauhqu
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
využit	využít	k5eAaPmNgInS	využít
jako	jako	k8xS	jako
podnět	podnět	k1gInSc1	podnět
k	k	k7c3	k
projektu	projekt	k1gInSc3	projekt
Proyecto	Proyecto	k1gNnSc4	Proyecto
Templo	Templo	k1gMnSc2	Templo
Mayor	Mayor	k1gInSc4	Mayor
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
úsilí	úsilí	k1gNnSc2	úsilí
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
mexického	mexický	k2eAgMnSc2d1	mexický
archeologa	archeolog	k1gMnSc2	archeolog
Eduarda	Eduard	k1gMnSc2	Eduard
Matosa	Matosa	k1gFnSc1	Matosa
Moctezumy	Moctezum	k1gInPc4	Moctezum
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kterému	který	k3yIgInSc3	který
muselo	muset	k5eAaImAgNnS	muset
být	být	k5eAaImF	být
zbořeno	zbořit	k5eAaPmNgNnS	zbořit
mnoho	mnoho	k4c1	mnoho
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
komplexní	komplexní	k2eAgInSc4d1	komplexní
archeologický	archeologický	k2eAgInSc4d1	archeologický
průzkum	průzkum	k1gInSc4	průzkum
zbytků	zbytek	k1gInPc2	zbytek
celého	celý	k2eAgInSc2d1	celý
chrámu	chrám	k1gInSc2	chrám
Templo	Templo	k1gMnSc1	Templo
Mayor	Mayor	k1gMnSc1	Mayor
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vykopávkách	vykopávka	k1gFnPc6	vykopávka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1978	[number]	k4	1978
až	až	k9	až
1982	[number]	k4	1982
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
sedm	sedm	k4xCc1	sedm
stavebních	stavební	k2eAgFnPc2d1	stavební
fází	fáze	k1gFnPc2	fáze
Templo	Templo	k1gMnSc1	Templo
Mayor	Mayor	k1gMnSc1	Mayor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
stále	stále	k6eAd1	stále
rozšiřován	rozšiřovat	k5eAaImNgInS	rozšiřovat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
novostaveb	novostavba	k1gFnPc2	novostavba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nejmladšího	mladý	k2eAgNnSc2d3	nejmladší
stadia	stadion	k1gNnSc2	stadion
výstavby	výstavba	k1gFnSc2	výstavba
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
zastihli	zastihnout	k5eAaPmAgMnP	zastihnout
španělští	španělský	k2eAgMnPc1d1	španělský
dobyvatelé	dobyvatel	k1gMnPc1	dobyvatel
a	a	k8xC	a
zničili	zničit	k5eAaPmAgMnP	zničit
jej	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zachovaly	zachovat	k5eAaPmAgInP	zachovat
jen	jen	k9	jen
malé	malý	k2eAgInPc1d1	malý
zbytky	zbytek	k1gInPc1	zbytek
<g/>
.	.	kIx.	.
</s>
<s>
Domnělé	domnělý	k2eAgFnPc1d1	domnělá
dřívější	dřívější	k2eAgFnPc1d1	dřívější
fáze	fáze	k1gFnPc1	fáze
výstavby	výstavba	k1gFnSc2	výstavba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
legend	legenda	k1gFnPc2	legenda
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
chrámu	chrám	k1gInSc2	chrám
byl	být	k5eAaImAgInS	být
vybudován	vybudovat	k5eAaPmNgInS	vybudovat
oltář	oltář	k1gInSc1	oltář
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
nebyly	být	k5eNaImAgInP	být
hledány	hledán	k2eAgInPc1d1	hledán
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
těžko	těžko	k6eAd1	těžko
k	k	k7c3	k
nalezení	nalezení	k1gNnSc3	nalezení
a	a	k8xC	a
již	již	k6eAd1	již
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
spodní	spodní	k2eAgFnSc2d1	spodní
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Přiřazení	přiřazení	k1gNnSc1	přiřazení
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
etap	etapa	k1gFnPc2	etapa
výstavby	výstavba	k1gFnSc2	výstavba
různým	různý	k2eAgMnPc3d1	různý
vládcům	vládce	k1gMnPc3	vládce
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc3	jejich
datování	datování	k1gNnSc4	datování
je	být	k5eAaImIp3nS	být
hypotetické	hypotetický	k2eAgNnSc1d1	hypotetické
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
nálezů	nález	k1gInPc2	nález
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
dvě	dva	k4xCgNnPc1	dva
století	století	k1gNnPc2	století
před	před	k7c7	před
španělským	španělský	k2eAgNnSc7d1	španělské
dobytím	dobytí	k1gNnSc7	dobytí
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
archeologických	archeologický	k2eAgInPc2d1	archeologický
nálezů	nález	k1gInPc2	nález
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
možno	možno	k6eAd1	možno
spatřit	spatřit	k5eAaPmF	spatřit
Národním	národní	k2eAgNnSc6d1	národní
muzeu	muzeum	k1gNnSc6	muzeum
pro	pro	k7c4	pro
antropologii	antropologie	k1gFnSc4	antropologie
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
Museo	Museo	k1gMnSc1	Museo
Nacional	Nacional	k1gMnSc1	Nacional
de	de	k?	de
Antropología	Antropología	k1gMnSc1	Antropología
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
muzeu	muzeum	k1gNnSc6	muzeum
Museo	Museo	k1gMnSc1	Museo
del	del	k?	del
Templo	Templo	k1gMnSc1	Templo
Mayor	Mayor	k1gMnSc1	Mayor
nacházejícím	nacházející	k2eAgInSc7d1	nacházející
se	se	k3xPyFc4	se
poblíž	poblíž	k7c2	poblíž
místa	místo	k1gNnSc2	místo
vykopávek	vykopávka	k1gFnPc2	vykopávka
<g/>
.	.	kIx.	.
</s>
<s>
Severně	severně	k6eAd1	severně
od	od	k7c2	od
hlavního	hlavní	k2eAgInSc2d1	hlavní
chrámu	chrám	k1gInSc2	chrám
byl	být	k5eAaImAgInS	být
odkryt	odkryt	k2eAgInSc1d1	odkryt
stavební	stavební	k2eAgInSc1d1	stavební
komplex	komplex	k1gInSc1	komplex
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
očividně	očividně	k6eAd1	očividně
sloužil	sloužit	k5eAaImAgInS	sloužit
jako	jako	k8xC	jako
shromaždiště	shromaždiště	k1gNnSc4	shromaždiště
hodnostářů	hodnostář	k1gMnPc2	hodnostář
a	a	k8xC	a
válečníků	válečník	k1gMnPc2	válečník
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
zdí	zeď	k1gFnPc2	zeď
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
zabudované	zabudovaný	k2eAgFnSc2d1	zabudovaná
lavice	lavice	k1gFnSc2	lavice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
formě	forma	k1gFnSc6	forma
dekorace	dekorace	k1gFnSc2	dekorace
velice	velice	k6eAd1	velice
zřetelně	zřetelně	k6eAd1	zřetelně
připomínají	připomínat	k5eAaImIp3nP	připomínat
podobné	podobný	k2eAgFnPc4d1	podobná
konstrukce	konstrukce	k1gFnPc4	konstrukce
v	v	k7c6	v
sloupových	sloupový	k2eAgFnPc6d1	sloupová
halách	hala	k1gFnPc6	hala
z	z	k7c2	z
Tuly	Tula	k1gFnSc2	Tula
a	a	k8xC	a
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
jimi	on	k3xPp3gInPc7	on
inspirovány	inspirován	k2eAgInPc1d1	inspirován
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
pod	pod	k7c4	pod
Španěly	Španěly	k1gInPc4	Španěly
vybudovanou	vybudovaný	k2eAgFnSc7d1	vybudovaná
katedrálou	katedrála	k1gFnSc7	katedrála
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
při	při	k7c6	při
stavebních	stavební	k2eAgFnPc6d1	stavební
pracích	práce	k1gFnPc6	práce
k	k	k7c3	k
zachování	zachování	k1gNnSc3	zachování
její	její	k3xOp3gFnSc2	její
stability	stabilita	k1gFnSc2	stabilita
v	v	k7c6	v
měkké	měkký	k2eAgFnSc6d1	měkká
půdě	půda	k1gFnSc6	půda
nalezeny	nalezen	k2eAgFnPc1d1	nalezena
stavby	stavba	k1gFnPc1	stavba
v	v	k7c6	v
předpolí	předpolí	k1gNnSc6	předpolí
aztéckého	aztécký	k2eAgInSc2d1	aztécký
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ovšem	ovšem	k9	ovšem
nemohly	moct	k5eNaImAgFnP	moct
být	být	k5eAaImF	být
přesně	přesně	k6eAd1	přesně
určeny	určit	k5eAaPmNgFnP	určit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
místech	místo	k1gNnPc6	místo
vystavený	vystavený	k2eAgInSc1d1	vystavený
model	model	k1gInSc4	model
chrámového	chrámový	k2eAgInSc2d1	chrámový
okrsku	okrsek	k1gInSc2	okrsek
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
studií	studio	k1gNnPc2	studio
mexického	mexický	k2eAgMnSc2d1	mexický
archeologa	archeolog	k1gMnSc2	archeolog
Ignacia	Ignacius	k1gMnSc2	Ignacius
Marquiny	Marquina	k1gMnSc2	Marquina
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
především	především	k6eAd1	především
zaměřoval	zaměřovat	k5eAaImAgMnS	zaměřovat
na	na	k7c4	na
písemné	písemný	k2eAgInPc4d1	písemný
záznamy	záznam	k1gInPc4	záznam
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
archeologickými	archeologický	k2eAgFnPc7d1	archeologická
vykopávkami	vykopávka	k1gFnPc7	vykopávka
překonán	překonat	k5eAaPmNgInS	překonat
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
domovní	domovní	k2eAgInSc1d1	domovní
blok	blok	k1gInSc1	blok
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
katedrály	katedrála	k1gFnSc2	katedrála
(	(	kIx(	(
<g/>
Calle	Calle	k1gInSc1	Calle
Donceles	Donceles	k1gInSc1	Donceles
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
při	při	k7c6	při
záchranných	záchranný	k2eAgFnPc6d1	záchranná
archeologických	archeologický	k2eAgFnPc6d1	archeologická
pracích	práce	k1gFnPc6	práce
při	při	k7c6	při
stavbě	stavba	k1gFnSc6	stavba
nového	nový	k2eAgInSc2d1	nový
domu	dům	k1gInSc2	dům
nalezeny	naleznout	k5eAaPmNgInP	naleznout
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
pěti	pět	k4xCc2	pět
metrů	metr	k1gInPc2	metr
základy	základ	k1gInPc1	základ
dvou	dva	k4xCgFnPc2	dva
staveb	stavba	k1gFnPc2	stavba
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jedna	jeden	k4xCgFnSc1	jeden
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
calmecac	calmecac	k1gFnSc4	calmecac
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
o	o	k7c4	o
zděné	zděný	k2eAgInPc4d1	zděný
pilíře	pilíř	k1gInPc4	pilíř
a	a	k8xC	a
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
vedoucí	vedoucí	k1gFnSc2	vedoucí
schodiště	schodiště	k1gNnSc2	schodiště
do	do	k7c2	do
sloupové	sloupový	k2eAgFnSc2d1	sloupová
síně	síň	k1gFnSc2	síň
<g/>
,	,	kIx,	,
podél	podél	k7c2	podél
jejíchž	jejíž	k3xOyRp3gFnPc2	jejíž
zdí	zeď	k1gFnPc2	zeď
jsou	být	k5eAaImIp3nP	být
lavice	lavice	k1gFnPc1	lavice
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
budovy	budova	k1gFnSc2	budova
byla	být	k5eAaImAgFnS	být
nalezena	nalezen	k2eAgFnSc1d1	nalezena
"	"	kIx"	"
<g/>
pohřbená	pohřbený	k2eAgFnSc1d1	pohřbená
<g/>
"	"	kIx"	"
střešní	střešní	k2eAgNnSc1d1	střešní
cimbuří	cimbuří	k1gNnSc1	cimbuří
z	z	k7c2	z
pálené	pálený	k2eAgFnSc2d1	pálená
hlíny	hlína	k1gFnSc2	hlína
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
rozříznutého	rozříznutý	k2eAgInSc2d1	rozříznutý
šneku	šnek	k1gInSc2	šnek
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
velká	velký	k2eAgFnSc1d1	velká
skulptura	skulptura	k1gFnSc1	skulptura
cuauhxicalli	cuauhxicalle	k1gFnSc4	cuauhxicalle
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
orla	orel	k1gMnSc2	orel
v	v	k7c6	v
nadživotní	nadživotní	k2eAgFnSc6d1	nadživotní
velikosti	velikost	k1gFnSc6	velikost
<g/>
.	.	kIx.	.
</s>
