<p>
<s>
Nápoj	nápoj	k1gInSc1	nápoj
zvaný	zvaný	k2eAgInSc1d1	zvaný
zelený	zelený	k2eAgInSc1d1	zelený
čaj	čaj	k1gInSc1	čaj
(	(	kIx(	(
<g/>
čínsky	čínsky	k6eAd1	čínsky
<g/>
:	:	kIx,	:
绿	绿	k?	绿
<g/>
;	;	kIx,	;
pinyin	pinyin	k1gMnSc1	pinyin
<g/>
:	:	kIx,	:
lǜ	lǜ	k?	lǜ
<g/>
;	;	kIx,	;
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
緑	緑	k?	緑
<g/>
,	,	kIx,	,
rjokuča	rjokuča	k1gFnSc1	rjokuča
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
pravý	pravý	k2eAgInSc4d1	pravý
<g/>
"	"	kIx"	"
čaj	čaj	k1gInSc4	čaj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
zpracování	zpracování	k1gNnSc6	zpracování
prošel	projít	k5eAaPmAgMnS	projít
minimální	minimální	k2eAgFnSc7d1	minimální
oxidací	oxidace	k1gFnSc7	oxidace
<g/>
.	.	kIx.	.
</s>
<s>
Zelený	zelený	k2eAgInSc1d1	zelený
čaj	čaj	k1gInSc1	čaj
je	být	k5eAaImIp3nS	být
populární	populární	k2eAgInSc1d1	populární
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Tchaj-wanu	Tchajan	k1gInSc6	Tchaj-wan
<g/>
,	,	kIx,	,
Hong	Hong	k1gMnSc1	Hong
Kongu	Kongo	k1gNnSc6	Kongo
<g/>
,	,	kIx,	,
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
,	,	kIx,	,
Koreji	Korea	k1gFnSc6	Korea
a	a	k8xC	a
na	na	k7c6	na
Středním	střední	k2eAgInSc6d1	střední
Východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Relativně	relativně	k6eAd1	relativně
nedávno	nedávno	k6eAd1	nedávno
se	se	k3xPyFc4	se
také	také	k9	také
více	hodně	k6eAd2	hodně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
tradičněji	tradičně	k6eAd2	tradičně
konzumován	konzumován	k2eAgInSc1d1	konzumován
čaj	čaj	k1gInSc1	čaj
černý	černý	k2eAgInSc1d1	černý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příprava	příprava	k1gFnSc1	příprava
==	==	k?	==
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc4	každý
listový	listový	k2eAgInSc4d1	listový
čaj	čaj	k1gInSc4	čaj
lze	lze	k6eAd1	lze
připravit	připravit	k5eAaPmF	připravit
více	hodně	k6eAd2	hodně
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecným	všeobecný	k2eAgNnSc7d1	všeobecné
pravidlem	pravidlo	k1gNnSc7	pravidlo
u	u	k7c2	u
zelených	zelený	k2eAgInPc2d1	zelený
čajů	čaj	k1gInPc2	čaj
je	být	k5eAaImIp3nS	být
zalévání	zalévání	k1gNnSc1	zalévání
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
prošla	projít	k5eAaPmAgFnS	projít
varem	var	k1gInSc7	var
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zchladla	zchladnout	k5eAaPmAgFnS	zchladnout
na	na	k7c4	na
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
80	[number]	k4	80
°	°	k?	°
<g/>
C.	C.	kA	C.
Příliš	příliš	k6eAd1	příliš
teplá	teplý	k2eAgFnSc1d1	teplá
voda	voda	k1gFnSc1	voda
způsobí	způsobit	k5eAaPmIp3nS	způsobit
vyluhování	vyluhování	k1gNnSc4	vyluhování
tříslovin	tříslovina	k1gFnPc2	tříslovina
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
substancí	substance	k1gFnPc2	substance
a	a	k8xC	a
nálev	nálev	k1gInSc1	nálev
má	mít	k5eAaImIp3nS	mít
hořkou	hořký	k2eAgFnSc4d1	hořká
chuť	chuť	k1gFnSc4	chuť
až	až	k8xS	až
pachuť	pachuť	k1gFnSc4	pachuť
<g/>
.	.	kIx.	.
</s>
<s>
Čaj	čaj	k1gInSc1	čaj
zalitý	zalitý	k2eAgInSc1d1	zalitý
příliš	příliš	k6eAd1	příliš
chladnou	chladný	k2eAgFnSc7d1	chladná
vodou	voda	k1gFnSc7	voda
má	mít	k5eAaImIp3nS	mít
mdlou	mdlý	k2eAgFnSc4d1	mdlá
až	až	k8xS	až
vodovou	vodový	k2eAgFnSc4d1	vodová
chuť	chuť	k1gFnSc4	chuť
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
vonné	vonný	k2eAgFnSc2d1	vonná
a	a	k8xC	a
chuťové	chuťový	k2eAgFnSc2d1	chuťová
složky	složka	k1gFnSc2	složka
uvolnily	uvolnit	k5eAaPmAgFnP	uvolnit
z	z	k7c2	z
listů	list	k1gInPc2	list
jen	jen	k9	jen
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvalitní	kvalitní	k2eAgNnSc1d1	kvalitní
listové	listový	k2eAgNnSc1d1	listové
zelené	zelené	k1gNnSc1	zelené
čaje	čaj	k1gInSc2	čaj
lze	lze	k6eAd1	lze
zalévat	zalévat	k5eAaImF	zalévat
vícekrát	vícekrát	k6eAd1	vícekrát
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
další	další	k2eAgInPc4d1	další
nálevy	nálev	k1gInPc4	nálev
lze	lze	k6eAd1	lze
zvýšit	zvýšit	k5eAaPmF	zvýšit
teplotu	teplota	k1gFnSc4	teplota
vody	voda	k1gFnSc2	voda
nebo	nebo	k8xC	nebo
prodloužit	prodloužit	k5eAaPmF	prodloužit
dobu	doba	k1gFnSc4	doba
louhování	louhování	k1gNnSc2	louhování
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
louhování	louhování	k1gNnSc2	louhování
pro	pro	k7c4	pro
první	první	k4xOgInSc4	první
nálev	nálev	k1gInSc4	nálev
může	moct	k5eAaImIp3nS	moct
u	u	k7c2	u
zelených	zelený	k2eAgInPc2d1	zelený
čajů	čaj	k1gInPc2	čaj
být	být	k5eAaImF	být
od	od	k7c2	od
30	[number]	k4	30
sekund	sekunda	k1gFnPc2	sekunda
do	do	k7c2	do
několika	několik	k4yIc2	několik
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
konkrétním	konkrétní	k2eAgInSc6d1	konkrétní
čaji	čaj	k1gInSc6	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
kratších	krátký	k2eAgFnPc6d2	kratší
dobách	doba	k1gFnPc6	doba
louhování	louhování	k1gNnSc2	louhování
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
používá	používat	k5eAaImIp3nS	používat
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
čaje	čaj	k1gInSc2	čaj
než	než	k8xS	než
u	u	k7c2	u
louhování	louhování	k1gNnSc2	louhování
delšího	dlouhý	k2eAgNnSc2d2	delší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Množství	množství	k1gNnSc1	množství
čaje	čaj	k1gInSc2	čaj
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
odměřovalo	odměřovat	k5eAaImAgNnS	odměřovat
na	na	k7c4	na
malé	malý	k2eAgFnPc4d1	malá
lžičky	lžička	k1gFnPc4	lžička
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
zažití	zažití	k1gNnSc3	zažití
pojmu	pojem	k1gInSc2	pojem
"	"	kIx"	"
<g/>
čajová	čajový	k2eAgFnSc1d1	čajová
lžička	lžička	k1gFnSc1	lžička
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kvalitních	kvalitní	k2eAgInPc2d1	kvalitní
sypaných	sypaný	k2eAgInPc2d1	sypaný
čajů	čaj	k1gInPc2	čaj
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
sypnou	sypný	k2eAgFnSc7d1	sypná
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
,	,	kIx,	,
velikostí	velikost	k1gFnSc7	velikost
listu	list	k1gInSc2	list
však	však	k9	však
tento	tento	k3xDgInSc4	tento
způsob	způsob	k1gInSc4	způsob
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
spolehlivý	spolehlivý	k2eAgMnSc1d1	spolehlivý
<g/>
.	.	kIx.	.
</s>
<s>
Vhodnější	vhodný	k2eAgFnSc1d2	vhodnější
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
odvážení	odvážení	k1gNnSc4	odvážení
čaje	čaj	k1gInSc2	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
1,5	[number]	k4	1,5
dl	dl	k?	dl
nálevu	nálev	k1gInSc2	nálev
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
g	g	kA	g
čaje	čaj	k1gInSc2	čaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nelze	lze	k6eNd1	lze
stanovit	stanovit	k5eAaPmF	stanovit
"	"	kIx"	"
<g/>
správný	správný	k2eAgInSc4d1	správný
<g/>
"	"	kIx"	"
způsob	způsob	k1gInSc4	způsob
přípravy	příprava	k1gFnSc2	příprava
čaje	čaj	k1gInSc2	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
sklizeň	sklizeň	k1gFnSc1	sklizeň
i	i	k9	i
stejného	stejný	k2eAgInSc2d1	stejný
druhu	druh	k1gInSc2	druh
čaje	čaj	k1gInSc2	čaj
je	být	k5eAaImIp3nS	být
vždycky	vždycky	k6eAd1	vždycky
odlišná	odlišný	k2eAgFnSc1d1	odlišná
a	a	k8xC	a
prožitek	prožitek	k1gInSc1	prožitek
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
čajováním	čajování	k1gNnSc7	čajování
spočívá	spočívat	k5eAaImIp3nS	spočívat
také	také	k9	také
v	v	k7c6	v
experimentu	experiment	k1gInSc6	experiment
a	a	k8xC	a
odhalování	odhalování	k1gNnSc1	odhalování
kvalit	kvalita	k1gFnPc2	kvalita
čaje	čaj	k1gInSc2	čaj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
přípravou	příprava	k1gFnSc7	příprava
zeleného	zelený	k2eAgInSc2d1	zelený
čaje	čaj	k1gInSc2	čaj
se	se	k3xPyFc4	se
ve	v	k7c6	v
východních	východní	k2eAgFnPc6d1	východní
kulturách	kultura	k1gFnPc6	kultura
spojuje	spojovat	k5eAaImIp3nS	spojovat
také	také	k9	také
řada	řada	k1gFnSc1	řada
obřadů	obřad	k1gInPc2	obřad
a	a	k8xC	a
ceremonií	ceremonie	k1gFnPc2	ceremonie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zelený	zelený	k2eAgInSc4d1	zelený
čaj	čaj	k1gInSc4	čaj
a	a	k8xC	a
zdraví	zdraví	k1gNnSc4	zdraví
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Historie	historie	k1gFnSc1	historie
===	===	k?	===
</s>
</p>
<p>
<s>
Archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
čaj	čaj	k1gInSc1	čaj
se	se	k3xPyFc4	se
pije	pít	k5eAaImIp3nS	pít
už	už	k6eAd1	už
téměř	téměř	k6eAd1	téměř
5	[number]	k4	5
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
Indie	Indie	k1gFnSc1	Indie
a	a	k8xC	a
Čína	Čína	k1gFnSc1	Čína
byly	být	k5eAaImAgFnP	být
první	první	k4xOgFnPc4	první
dvě	dva	k4xCgFnPc4	dva
krajiny	krajina	k1gFnPc4	krajina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
pěstovat	pěstovat	k5eAaImF	pěstovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
je	být	k5eAaImIp3nS	být
objevení	objevení	k1gNnSc1	objevení
čaje	čaj	k1gInSc2	čaj
připisováno	připisovat	k5eAaImNgNnS	připisovat
čínskému	čínský	k2eAgMnSc3d1	čínský
císaři	císař	k1gMnSc3	císař
Šen-nungovi	Šenung	k1gMnSc3	Šen-nung
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
při	při	k7c6	při
ohřívání	ohřívání	k1gNnSc6	ohřívání
vody	voda	k1gFnSc2	voda
spadly	spadnout	k5eAaPmAgFnP	spadnout
do	do	k7c2	do
kotlíku	kotlík	k1gInSc2	kotlík
náhodně	náhodně	k6eAd1	náhodně
i	i	k9	i
lístky	lístek	k1gInPc1	lístek
čajovníku	čajovník	k1gInSc2	čajovník
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
lahodně	lahodně	k6eAd1	lahodně
vonící	vonící	k2eAgInSc4d1	vonící
nápoj	nápoj	k1gInSc4	nápoj
ochutnal	ochutnat	k5eAaPmAgMnS	ochutnat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
překvapen	překvapen	k2eAgMnSc1d1	překvapen
jeho	jeho	k3xOp3gFnSc7	jeho
vynikající	vynikající	k2eAgFnSc7d1	vynikající
chutí	chuť	k1gFnSc7	chuť
a	a	k8xC	a
energií	energie	k1gFnSc7	energie
nápoje	nápoj	k1gInSc2	nápoj
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
odstranila	odstranit	k5eAaPmAgFnS	odstranit
únavu	únava	k1gFnSc4	únava
<g/>
.	.	kIx.	.
</s>
<s>
Zelený	zelený	k2eAgInSc1d1	zelený
čaj	čaj	k1gInSc1	čaj
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
v	v	k7c6	v
tradiční	tradiční	k2eAgFnSc6d1	tradiční
medicíně	medicína	k1gFnSc6	medicína
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
Thajsku	Thajsko	k1gNnSc6	Thajsko
v	v	k7c6	v
nejrůznějších	různý	k2eAgInPc6d3	nejrůznější
případech	případ	k1gInPc6	případ
od	od	k7c2	od
zastavování	zastavování	k1gNnSc2	zastavování
krvácení	krvácení	k1gNnSc2	krvácení
a	a	k8xC	a
zlepšování	zlepšování	k1gNnSc4	zlepšování
hojení	hojení	k1gNnPc2	hojení
ran	rána	k1gFnPc2	rána
přes	přes	k7c4	přes
snižování	snižování	k1gNnSc4	snižování
horečky	horečka	k1gFnSc2	horečka
<g/>
,	,	kIx,	,
snižování	snižování	k1gNnSc1	snižování
krevního	krevní	k2eAgInSc2d1	krevní
cukru	cukr	k1gInSc2	cukr
až	až	k9	až
po	po	k7c4	po
zlepšování	zlepšování	k1gNnSc4	zlepšování
trávení	trávení	k1gNnSc2	trávení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Kniha	kniha	k1gFnSc1	kniha
čaje	čaj	k1gInSc2	čaj
====	====	k?	====
</s>
</p>
<p>
<s>
Kissa	Kissa	k1gFnSc1	Kissa
Yojoki	Yojok	k1gFnSc2	Yojok
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
Kniha	kniha	k1gFnSc1	kniha
čaje	čaj	k1gInSc2	čaj
<g/>
,	,	kIx,	,
sepsaná	sepsaný	k2eAgNnPc4d1	sepsané
zenovým	zenový	k2eAgInSc7d1	zenový
mnichem	mnich	k1gInSc7	mnich
jménem	jméno	k1gNnSc7	jméno
Eisai	Eisae	k1gFnSc4	Eisae
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1191	[number]	k4	1191
<g/>
,	,	kIx,	,
popisuje	popisovat	k5eAaImIp3nS	popisovat
pozitivní	pozitivní	k2eAgInPc1d1	pozitivní
účinky	účinek	k1gInPc1	účinek
pití	pití	k1gNnSc2	pití
zeleného	zelený	k2eAgInSc2d1	zelený
čaje	čaj	k1gInSc2	čaj
na	na	k7c4	na
pět	pět	k4xCc4	pět
životně	životně	k6eAd1	životně
důležitých	důležitý	k2eAgInPc2d1	důležitý
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
pak	pak	k6eAd1	pak
na	na	k7c4	na
srdce	srdce	k1gNnSc4	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
se	se	k3xPyFc4	se
zaobírá	zaobírat	k5eAaImIp3nS	zaobírat
léčebnými	léčebný	k2eAgInPc7d1	léčebný
účinky	účinek	k1gInPc7	účinek
čaje	čaj	k1gInSc2	čaj
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
léčby	léčba	k1gFnSc2	léčba
kocoviny	kocovina	k1gFnSc2	kocovina
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc2	jeho
role	role	k1gFnSc2	role
jako	jako	k8xS	jako
stimulantu	stimulant	k1gInSc2	stimulant
<g/>
,	,	kIx,	,
léčení	léčení	k1gNnSc4	léčení
oparů	opar	k1gInPc2	opar
a	a	k8xC	a
uhrů	uhr	k1gInPc2	uhr
<g/>
,	,	kIx,	,
tišení	tišení	k1gNnSc1	tišení
žízně	žízeň	k1gFnSc2	žízeň
<g/>
,	,	kIx,	,
nápravy	náprava	k1gFnSc2	náprava
poruch	porucha	k1gFnPc2	porucha
trávení	trávení	k1gNnSc2	trávení
<g/>
,	,	kIx,	,
léčení	léčení	k1gNnSc2	léčení
onemocnění	onemocnění	k1gNnSc2	onemocnění
nervového	nervový	k2eAgInSc2d1	nervový
systému	systém	k1gInSc2	systém
"	"	kIx"	"
<g/>
beriberi	beriberit	k5eAaImRp2nS	beriberit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
prevenci	prevence	k1gFnSc4	prevence
únavy	únava	k1gFnSc2	únava
a	a	k8xC	a
podporu	podpora	k1gFnSc4	podpora
funkcí	funkce	k1gFnPc2	funkce
močové	močový	k2eAgFnSc2d1	močová
soustavy	soustava	k1gFnSc2	soustava
a	a	k8xC	a
mozkové	mozkový	k2eAgFnSc2d1	mozková
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
knihy	kniha	k1gFnSc2	kniha
také	také	k9	také
popisuje	popisovat	k5eAaImIp3nS	popisovat
druhy	druh	k1gInPc4	druh
čajovníků	čajovník	k1gInPc2	čajovník
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc2	jejich
květů	květ	k1gInPc2	květ
a	a	k8xC	a
listů	list	k1gInPc2	list
a	a	k8xC	a
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
jak	jak	k6eAd1	jak
čaj	čaj	k1gInSc4	čaj
pěstovat	pěstovat	k5eAaImF	pěstovat
a	a	k8xC	a
jak	jak	k6eAd1	jak
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
jeho	jeho	k3xOp3gInPc4	jeho
listy	list	k1gInPc4	list
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
druhá	druhý	k4xOgFnSc1	druhý
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
dávkování	dávkování	k1gNnSc4	dávkování
a	a	k8xC	a
způsoby	způsoba	k1gFnPc4	způsoba
podávání	podávání	k1gNnSc2	podávání
čaje	čaj	k1gInSc2	čaj
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
chorob	choroba	k1gFnPc2	choroba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nepodložená	podložený	k2eNgNnPc4d1	nepodložené
tvrzení	tvrzení	k1gNnPc4	tvrzení
===	===	k?	===
</s>
</p>
<p>
<s>
Zelenému	zelený	k2eAgInSc3d1	zelený
čaji	čaj	k1gInSc3	čaj
se	se	k3xPyFc4	se
připisuje	připisovat	k5eAaImIp3nS	připisovat
řada	řada	k1gFnSc1	řada
zdravotních	zdravotní	k2eAgInPc2d1	zdravotní
účinků	účinek	k1gInPc2	účinek
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
mnohé	mnohý	k2eAgFnPc1d1	mnohá
dosud	dosud	k6eAd1	dosud
nebyly	být	k5eNaImAgFnP	být
vědecky	vědecky	k6eAd1	vědecky
potvrzeny	potvrdit	k5eAaPmNgInP	potvrdit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
tvrzení	tvrzení	k1gNnPc1	tvrzení
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
která	který	k3yRgNnPc4	který
chybí	chybět	k5eAaImIp3nS	chybět
vědecké	vědecký	k2eAgInPc4d1	vědecký
důkazy	důkaz	k1gInPc4	důkaz
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vypsána	vypsat	k5eAaPmNgNnP	vypsat
zde	zde	k6eAd1	zde
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
zastavení	zastavení	k1gNnSc3	zastavení
některých	některý	k3yIgNnPc2	některý
neurodegenerativních	urodegenerativní	k2eNgNnPc2d1	neurodegenerativní
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Alzheimerova	Alzheimerův	k2eAgFnSc1d1	Alzheimerova
nebo	nebo	k8xC	nebo
Parkinsonova	Parkinsonův	k2eAgFnSc1d1	Parkinsonova
choroba	choroba	k1gFnSc1	choroba
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
prevence	prevence	k1gFnSc1	prevence
a	a	k8xC	a
léčba	léčba	k1gFnSc1	léčba
rakoviny	rakovina	k1gFnSc2	rakovina
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
léčba	léčba	k1gFnSc1	léčba
sklerózy	skleróza	k1gFnSc2	skleróza
multiplex	multiplex	k2eAgFnSc1d1	multiplex
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
prevence	prevence	k1gFnSc1	prevence
degradace	degradace	k1gFnSc2	degradace
buněčných	buněčný	k2eAgFnPc2d1	buněčná
membrán	membrána	k1gFnPc2	membrána
neutralizací	neutralizace	k1gFnPc2	neutralizace
šíření	šíření	k1gNnSc2	šíření
volných	volný	k2eAgInPc2d1	volný
radikálů	radikál	k1gInPc2	radikál
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
nastává	nastávat	k5eAaImIp3nS	nastávat
při	při	k7c6	při
oxidačních	oxidační	k2eAgInPc6d1	oxidační
procesech	proces	k1gInPc6	proces
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
snížení	snížení	k1gNnSc1	snížení
negativních	negativní	k2eAgInPc2d1	negativní
účinků	účinek	k1gInPc2	účinek
cholesterolu	cholesterol	k1gInSc2	cholesterol
LDL	LDL	kA	LDL
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
špatný	špatný	k2eAgInSc4d1	špatný
cholesterol	cholesterol	k1gInSc4	cholesterol
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
snížením	snížení	k1gNnSc7	snížení
úrovní	úroveň	k1gFnPc2	úroveň
triglycerinů	triglycerin	k1gInPc2	triglycerin
a	a	k8xC	a
zvýšením	zvýšení	k1gNnSc7	zvýšení
produkce	produkce	k1gFnSc2	produkce
cholesterolu	cholesterol	k1gInSc2	cholesterol
HDL	HDL	kA	HDL
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
dobrý	dobrý	k2eAgInSc1d1	dobrý
cholesterol	cholesterol	k1gInSc1	cholesterol
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
zvýšení	zvýšení	k1gNnSc1	zvýšení
oxidace	oxidace	k1gFnSc2	oxidace
tuků	tuk	k1gInPc2	tuk
(	(	kIx(	(
<g/>
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
tělu	tělo	k1gNnSc3	tělo
využít	využít	k5eAaPmF	využít
tuk	tuk	k1gInSc1	tuk
jako	jako	k8xC	jako
zdroj	zdroj	k1gInSc1	zdroj
energie	energie	k1gFnSc2	energie
<g/>
)	)	kIx)	)
a	a	k8xC	a
podpora	podpora	k1gFnSc1	podpora
látkové	látkový	k2eAgFnSc2d1	látková
výměny	výměna	k1gFnSc2	výměna
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
snížení	snížení	k1gNnSc1	snížení
nechuti	nechuť	k1gFnSc2	nechuť
k	k	k7c3	k
jídlu	jídlo	k1gNnSc3	jídlo
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
podpora	podpora	k1gFnSc1	podpora
močení	močení	k1gNnSc2	močení
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k8xC	i
5	[number]	k4	5
<g/>
x	x	k?	x
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
<g/>
Japonští	japonský	k2eAgMnPc1d1	japonský
výzkumníci	výzkumník	k1gMnPc1	výzkumník
také	také	k9	také
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
vypito	vypít	k5eAaPmNgNnS	vypít
denně	denně	k6eAd1	denně
pět	pět	k4xCc1	pět
šálků	šálek	k1gInPc2	šálek
zeleného	zelený	k2eAgInSc2d1	zelený
čaje	čaj	k1gInSc2	čaj
<g/>
,	,	kIx,	,
spálí	spálit	k5eAaPmIp3nS	spálit
tělo	tělo	k1gNnSc4	tělo
navíc	navíc	k6eAd1	navíc
70	[number]	k4	70
až	až	k9	až
80	[number]	k4	80
kalorií	kalorie	k1gFnPc2	kalorie
<g/>
.	.	kIx.	.
</s>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Nicholas	Nicholas	k1gMnSc1	Nicholas
Perricone	Perricon	k1gInSc5	Perricon
<g/>
,	,	kIx,	,
specialista	specialista	k1gMnSc1	specialista
na	na	k7c4	na
boj	boj	k1gInSc4	boj
se	s	k7c7	s
stárnutím	stárnutí	k1gNnSc7	stárnutí
<g/>
,	,	kIx,	,
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
v	v	k7c6	v
televizní	televizní	k2eAgFnSc6d1	televizní
show	show	k1gFnSc6	show
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
člověk	člověk	k1gMnSc1	člověk
pije	pít	k5eAaImIp3nS	pít
čaj	čaj	k1gInSc4	čaj
namísto	namísto	k7c2	namísto
kávy	káva	k1gFnSc2	káva
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
během	během	k7c2	během
10	[number]	k4	10
týdnů	týden	k1gInPc2	týden
zhubne	zhubnout	k5eAaPmIp3nS	zhubnout
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
ale	ale	k8xC	ale
také	také	k9	také
negativní	negativní	k2eAgInPc1d1	negativní
účinky	účinek	k1gInPc1	účinek
při	při	k7c6	při
nadměrném	nadměrný	k2eAgNnSc6d1	nadměrné
pití	pití	k1gNnSc6	pití
čaje	čaj	k1gInSc2	čaj
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
velkému	velký	k2eAgNnSc3d1	velké
množství	množství	k1gNnSc3	množství
kofeinu	kofein	k1gInSc2	kofein
v	v	k7c6	v
zeleném	zelený	k2eAgInSc6d1	zelený
čaji	čaj	k1gInSc6	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Vedlejší	vedlejší	k2eAgInPc1d1	vedlejší
účinky	účinek	k1gInPc1	účinek
z	z	k7c2	z
množství	množství	k1gNnSc2	množství
kofeinu	kofein	k1gInSc2	kofein
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
nevolnost	nevolnost	k1gFnSc4	nevolnost
<g/>
,	,	kIx,	,
nespavost	nespavost	k1gFnSc1	nespavost
a	a	k8xC	a
časté	častý	k2eAgNnSc1d1	časté
močení	močení	k1gNnSc1	močení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vědecké	vědecký	k2eAgFnPc1d1	vědecká
studie	studie	k1gFnPc1	studie
===	===	k?	===
</s>
</p>
<p>
<s>
Studie	studie	k1gFnSc1	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
v	v	k7c6	v
magazínu	magazín	k1gInSc6	magazín
Journal	Journal	k1gFnSc2	Journal
of	of	k?	of
the	the	k?	the
American	American	k1gInSc1	American
Medial	Medial	k1gMnSc1	Medial
Association	Association	k1gInSc1	Association
<g/>
[	[	kIx(	[
<g/>
kdo	kdo	k3yInSc1	kdo
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
uvádí	uvádět	k5eAaImIp3nS	uvádět
"	"	kIx"	"
<g/>
Konzumace	konzumace	k1gFnSc1	konzumace
zeleného	zelený	k2eAgInSc2d1	zelený
čaje	čaj	k1gInSc2	čaj
je	být	k5eAaImIp3nS	být
spojená	spojený	k2eAgFnSc1d1	spojená
se	s	k7c7	s
snížením	snížení	k1gNnSc7	snížení
úmrtnosti	úmrtnost	k1gFnSc2	úmrtnost
zapříčiněným	zapříčiněný	k2eAgFnPc3d1	zapříčiněná
rakovinou	rakovina	k1gFnSc7	rakovina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
<g/>
,	,	kIx,	,
vedená	vedený	k2eAgFnSc1d1	vedená
japonskou	japonský	k2eAgFnSc7d1	japonská
univerzitou	univerzita	k1gFnSc7	univerzita
Tohoku	Tohok	k1gInSc2	Tohok
<g/>
,	,	kIx,	,
sledovala	sledovat	k5eAaImAgFnS	sledovat
40	[number]	k4	40
520	[number]	k4	520
dospělých	dospělý	k2eAgMnPc2d1	dospělý
japonců	japonec	k1gMnPc2	japonec
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
mezi	mezi	k7c7	mezi
40	[number]	k4	40
a	a	k8xC	a
79	[number]	k4	79
lety	let	k1gInPc7	let
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
neměli	mít	k5eNaImAgMnP	mít
mrtvici	mrtvice	k1gFnSc4	mrtvice
<g/>
,	,	kIx,	,
srdeční	srdeční	k2eAgInSc4d1	srdeční
záchvat	záchvat	k1gInSc4	záchvat
nebo	nebo	k8xC	nebo
rakovinu	rakovina	k1gFnSc4	rakovina
při	při	k7c6	při
zahájení	zahájení	k1gNnSc6	zahájení
výzkumu	výzkum	k1gInSc2	výzkum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
sledovala	sledovat	k5eAaImAgFnS	sledovat
všechny	všechen	k3xTgMnPc4	všechen
účastníky	účastník	k1gMnPc4	účastník
po	po	k7c4	po
celých	celý	k2eAgNnPc2d1	celé
11	[number]	k4	11
let	léto	k1gNnPc2	léto
a	a	k8xC	a
monitorovala	monitorovat	k5eAaImAgFnS	monitorovat
všechny	všechen	k3xTgFnPc4	všechen
příčiny	příčina	k1gFnPc4	příčina
úmrtí	úmrtí	k1gNnSc2	úmrtí
a	a	k8xC	a
po	po	k7c4	po
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
monitorovala	monitorovat	k5eAaImAgNnP	monitorovat
specifická	specifický	k2eAgNnPc1d1	specifické
úmrtí	úmrtí	k1gNnPc1	úmrtí
<g/>
.	.	kIx.	.
</s>
<s>
Účastníci	účastník	k1gMnPc1	účastník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
pili	pít	k5eAaImAgMnP	pít
pět	pět	k4xCc4	pět
a	a	k8xC	a
více	hodně	k6eAd2	hodně
šálků	šálek	k1gInPc2	šálek
čaje	čaj	k1gInSc2	čaj
denně	denně	k6eAd1	denně
měli	mít	k5eAaImAgMnP	mít
o	o	k7c4	o
16	[number]	k4	16
procent	procento	k1gNnPc2	procento
nižší	nízký	k2eAgNnSc4d2	nižší
riziko	riziko	k1gNnSc4	riziko
předčasného	předčasný	k2eAgNnSc2d1	předčasné
úmrtí	úmrtí	k1gNnSc2	úmrtí
a	a	k8xC	a
o	o	k7c4	o
26	[number]	k4	26
procent	procento	k1gNnPc2	procento
nižší	nízký	k2eAgNnSc4d2	nižší
riziko	riziko	k1gNnSc4	riziko
výskytu	výskyt	k1gInSc2	výskyt
kardiovaskulárních	kardiovaskulární	k2eAgFnPc2d1	kardiovaskulární
chorob	choroba	k1gFnPc2	choroba
<g/>
,	,	kIx,	,
než	než	k8xS	než
účastníci	účastník	k1gMnPc1	účastník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
pili	pít	k5eAaImAgMnP	pít
méně	málo	k6eAd2	málo
než	než	k8xS	než
jeden	jeden	k4xCgInSc1	jeden
šálek	šálek	k1gInSc1	šálek
čaje	čaj	k1gInSc2	čaj
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
dále	daleko	k6eAd2	daleko
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
pokud	pokud	k8xS	pokud
zelený	zelený	k2eAgInSc1d1	zelený
čaj	čaj	k1gInSc1	čaj
chrání	chránit	k5eAaImIp3nS	chránit
člověka	člověk	k1gMnSc4	člověk
proti	proti	k7c3	proti
kardiovaskulárním	kardiovaskulární	k2eAgFnPc3d1	kardiovaskulární
chorobám	choroba	k1gFnPc3	choroba
nebo	nebo	k8xC	nebo
rakovině	rakovina	k1gFnSc3	rakovina
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
očekávat	očekávat	k5eAaImF	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pití	pití	k1gNnSc1	pití
tohoto	tento	k3xDgInSc2	tento
nápoje	nápoj	k1gInSc2	nápoj
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
částečně	částečně	k6eAd1	částečně
podílet	podílet	k5eAaImF	podílet
na	na	k7c4	na
prodloužení	prodloužení	k1gNnSc4	prodloužení
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
právě	právě	k9	právě
kardiovaskulární	kardiovaskulární	k2eAgFnPc1d1	kardiovaskulární
choroby	choroba	k1gFnPc1	choroba
a	a	k8xC	a
rakovina	rakovina	k1gFnSc1	rakovina
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc4	dva
celosvětově	celosvětově	k6eAd1	celosvětově
nejčastější	častý	k2eAgFnPc4d3	nejčastější
příčiny	příčina	k1gFnPc4	příčina
předčasného	předčasný	k2eAgNnSc2d1	předčasné
úmrtí	úmrtí	k1gNnSc2	úmrtí
<g />
.	.	kIx.	.
</s>
<s>
<g/>
<g/>
"	"	kIx"	"
<g/>
Studie	studie	k1gFnSc1	studie
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
v	v	k7c6	v
magazínu	magazín	k1gInSc6	magazín
American	American	k1gMnSc1	American
Journal	Journal	k1gMnSc1	Journal
of	of	k?	of
Clinical	Clinical	k1gFnSc1	Clinical
Nutrition	Nutrition	k1gInSc1	Nutrition
uvádí	uvádět	k5eAaImIp3nS	uvádět
ve	v	k7c6	v
vydání	vydání	k1gNnSc6	vydání
z	z	k7c2	z
února	únor	k1gInSc2	únor
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
větší	veliký	k2eAgFnSc1d2	veliký
konzumace	konzumace	k1gFnSc1	konzumace
zeleného	zelený	k2eAgInSc2d1	zelený
čaje	čaj	k1gInSc2	čaj
je	být	k5eAaImIp3nS	být
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
menším	malý	k2eAgInSc7d2	menší
výskytem	výskyt	k1gInSc7	výskyt
poruch	porucha	k1gFnPc2	porucha
kognitivních	kognitivní	k2eAgFnPc2d1	kognitivní
funkcí	funkce	k1gFnPc2	funkce
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2006	[number]	k4	2006
uveřejnili	uveřejnit	k5eAaPmAgMnP	uveřejnit
výzkumníci	výzkumník	k1gMnPc1	výzkumník
Yaleovy	Yaleův	k2eAgFnSc2d1	Yaleova
univerzity	univerzita	k1gFnSc2	univerzita
článek	článek	k1gInSc1	článek
porovnávající	porovnávající	k2eAgInSc1d1	porovnávající
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
různých	různý	k2eAgFnPc2d1	různá
studií	studie	k1gFnPc2	studie
zaměřených	zaměřený	k2eAgFnPc2d1	zaměřená
na	na	k7c4	na
kladné	kladný	k2eAgInPc4d1	kladný
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
účinky	účinek	k1gInPc4	účinek
zeleného	zelený	k2eAgInSc2d1	zelený
čaje	čaj	k1gInSc2	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Upozornili	upozornit	k5eAaPmAgMnP	upozornit
na	na	k7c4	na
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
nazvali	nazvat	k5eAaBmAgMnP	nazvat
"	"	kIx"	"
<g/>
asijským	asijský	k2eAgInSc7d1	asijský
paradoxem	paradox	k1gInSc7	paradox
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
navzdory	navzdory	k7c3	navzdory
velmi	velmi	k6eAd1	velmi
rozšířenému	rozšířený	k2eAgNnSc3d1	rozšířené
kouření	kouření	k1gNnSc3	kouření
cigaret	cigareta	k1gFnPc2	cigareta
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
malý	malý	k2eAgInSc1d1	malý
výskyt	výskyt	k1gInSc1	výskyt
srdečních	srdeční	k2eAgFnPc2d1	srdeční
chorob	choroba	k1gFnPc2	choroba
a	a	k8xC	a
rakoviny	rakovina	k1gFnSc2	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Teoretizují	teoretizovat	k5eAaImIp3nP	teoretizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
1,2	[number]	k4	1,2
litru	litr	k1gInSc2	litr
čaje	čaj	k1gInSc2	čaj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
mnozí	mnohý	k2eAgMnPc1d1	mnohý
Asiaté	Asiat	k1gMnPc1	Asiat
vypijí	vypít	k5eAaPmIp3nP	vypít
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
přináší	přinášet	k5eAaImIp3nS	přinášet
vysoké	vysoký	k2eAgFnPc4d1	vysoká
hodnoty	hodnota	k1gFnPc4	hodnota
polyfenolů	polyfenol	k1gInPc2	polyfenol
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
antioxidantů	antioxidant	k1gInPc2	antioxidant
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
složky	složka	k1gFnPc1	složka
pak	pak	k6eAd1	pak
mohou	moct	k5eAaImIp3nP	moct
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
zlepšit	zlepšit	k5eAaPmF	zlepšit
stav	stav	k1gInSc1	stav
kardiovaskulárního	kardiovaskulární	k2eAgInSc2d1	kardiovaskulární
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
prevence	prevence	k1gFnSc2	prevence
spojování	spojování	k1gNnSc2	spojování
krevních	krevní	k2eAgFnPc2d1	krevní
destiček	destička	k1gFnPc2	destička
a	a	k8xC	a
prevence	prevence	k1gFnSc2	prevence
zvyšování	zvyšování	k1gNnSc2	zvyšování
hodnoty	hodnota	k1gFnSc2	hodnota
cholesterolu	cholesterol	k1gInSc2	cholesterol
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
uvádějí	uvádět	k5eAaImIp3nP	uvádět
výzkumníci	výzkumník	k1gMnPc1	výzkumník
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
studie	studie	k1gFnSc1	studie
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
květnovém	květnový	k2eAgNnSc6d1	květnové
vydání	vydání	k1gNnSc6	vydání
magazínu	magazín	k1gInSc2	magazín
Journal	Journal	k1gFnSc2	Journal
of	of	k?	of
the	the	k?	the
American	American	k1gInSc1	American
College	College	k1gInSc1	College
of	of	k?	of
Surgeons	Surgeons	k1gInSc1	Surgeons
<g/>
.	.	kIx.	.
</s>
<s>
Výslovně	výslovně	k6eAd1	výslovně
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
zelený	zelený	k2eAgInSc1d1	zelený
čaj	čaj	k1gInSc1	čaj
může	moct	k5eAaImIp3nS	moct
pomoci	pomoct	k5eAaPmF	pomoct
v	v	k7c6	v
prevenci	prevence	k1gFnSc6	prevence
oxidace	oxidace	k1gFnSc2	oxidace
cholesterolu	cholesterol	k1gInSc2	cholesterol
LDL	LDL	kA	LDL
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
špatný	špatný	k2eAgInSc4d1	špatný
<g/>
"	"	kIx"	"
cholesterol	cholesterol	k1gInSc4	cholesterol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
následně	následně	k6eAd1	následně
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
zanášení	zanášení	k1gNnSc2	zanášení
cév	céva	k1gFnPc2	céva
<g/>
.	.	kIx.	.
<g/>
Studie	studie	k1gFnSc1	studie
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
v	v	k7c6	v
magazínu	magazín	k1gInSc6	magazín
Biological	Biological	k1gFnSc2	Biological
Psychology	psycholog	k1gMnPc4	psycholog
<g/>
,	,	kIx,	,
vydání	vydání	k1gNnSc4	vydání
z	z	k7c2	z
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
změnu	změna	k1gFnSc4	změna
stresové	stresový	k2eAgFnSc2d1	stresová
reakce	reakce	k1gFnSc2	reakce
pomocí	pomoc	k1gFnPc2	pomoc
L-Theaninu	L-Theanin	k2eAgFnSc4d1	L-Theanin
<g/>
,	,	kIx,	,
chemikálie	chemikálie	k1gFnPc1	chemikálie
vyskytující	vyskytující	k2eAgFnPc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
zeleném	zelený	k2eAgInSc6d1	zelený
čaji	čaj	k1gInSc6	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
orální	orální	k2eAgInSc1d1	orální
příjem	příjem	k1gInSc1	příjem
L-Theaninu	L-Theanina	k1gFnSc4	L-Theanina
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
anti-stresový	antitresový	k2eAgInSc4d1	anti-stresový
efekt	efekt	k1gInSc4	efekt
díky	díky	k7c3	díky
potlačení	potlačení	k1gNnSc3	potlačení
podráždění	podráždění	k1gNnSc2	podráždění
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
oboustranně	oboustranně	k6eAd1	oboustranně
anonymním	anonymní	k2eAgInSc6d1	anonymní
pokusu	pokus	k1gInSc6	pokus
<g/>
,	,	kIx,	,
eliminujícím	eliminující	k2eAgInSc6d1	eliminující
i	i	k8xC	i
placebo	placebo	k1gNnSc4	placebo
efekt	efekt	k1gInSc1	efekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
divize	divize	k1gFnSc1	divize
kardiovaskulární	kardiovaskulární	k2eAgFnSc2d1	kardiovaskulární
medicíny	medicína	k1gFnSc2	medicína
při	při	k7c6	při
Univerzitním	univerzitní	k2eAgNnSc6d1	univerzitní
zdravotním	zdravotní	k2eAgNnSc6d1	zdravotní
centru	centrum	k1gNnSc6	centrum
ve	v	k7c6	v
Vanderbiltu	Vanderbilt	k1gInSc6	Vanderbilt
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
240	[number]	k4	240
dospělým	dospělý	k2eAgMnSc7d1	dospělý
osobám	osoba	k1gFnPc3	osoba
podáván	podávat	k5eAaImNgInS	podávat
buď	buď	k8xC	buď
extrakt	extrakt	k1gInSc1	extrakt
ze	z	k7c2	z
zeleného	zelený	k2eAgInSc2d1	zelený
čaje	čaj	k1gInSc2	čaj
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
375	[number]	k4	375
<g/>
mg	mg	kA	mg
kapsulí	kapsulí	k?	kapsulí
nebo	nebo	k8xC	nebo
placebo	placebo	k1gNnSc1	placebo
(	(	kIx(	(
<g/>
neškodná	škodný	k2eNgFnSc1d1	neškodná
náhražka	náhražka	k1gFnSc1	náhražka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
12	[number]	k4	12
týdnech	týden	k1gInPc6	týden
pacienti	pacient	k1gMnPc1	pacient
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
byl	být	k5eAaImAgInS	být
podáván	podávat	k5eAaImNgInS	podávat
čajový	čajový	k2eAgInSc1d1	čajový
extrakt	extrakt	k1gInSc1	extrakt
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
významně	významně	k6eAd1	významně
nižší	nízký	k2eAgFnSc2d2	nižší
úrovně	úroveň	k1gFnSc2	úroveň
LDL	LDL	kA	LDL
cholesterolu	cholesterol	k1gInSc2	cholesterol
a	a	k8xC	a
též	též	k9	též
celkovou	celkový	k2eAgFnSc4d1	celková
hladinu	hladina	k1gFnSc4	hladina
cholesterolů	cholesterol	k1gInPc2	cholesterol
(	(	kIx(	(
<g/>
o	o	k7c4	o
16,4	[number]	k4	16,4
%	%	kIx~	%
a	a	k8xC	a
11,3	[number]	k4	11,3
%	%	kIx~	%
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
<g/>
)	)	kIx)	)
než	než	k8xS	než
druhá	druhý	k4xOgFnSc1	druhý
skupina	skupina	k1gFnSc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Závěrem	závěrem	k6eAd1	závěrem
doporučili	doporučit	k5eAaPmAgMnP	doporučit
podávání	podávání	k1gNnSc4	podávání
obohaceného	obohacený	k2eAgInSc2d1	obohacený
theaflavinového	theaflavinový	k2eAgInSc2d1	theaflavinový
extraktu	extrakt	k1gInSc2	extrakt
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
další	další	k2eAgFnSc7d1	další
dietou	dieta	k1gFnSc7	dieta
jako	jako	k9	jako
prostředek	prostředek	k1gInSc1	prostředek
ke	k	k7c3	k
snižování	snižování	k1gNnSc3	snižování
"	"	kIx"	"
<g/>
špatného	špatný	k2eAgInSc2d1	špatný
<g/>
"	"	kIx"	"
cholesterolu	cholesterol	k1gInSc2	cholesterol
<g/>
.	.	kIx.	.
<g/>
Studie	studie	k1gFnSc1	studie
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2005	[number]	k4	2005
v	v	k7c6	v
magazínu	magazín	k1gInSc6	magazín
American	American	k1gMnSc1	American
Journal	Journal	k1gMnSc1	Journal
of	of	k?	of
Clinical	Clinical	k1gFnSc1	Clinical
Nutrition	Nutrition	k1gInSc1	Nutrition
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
denní	denní	k2eAgFnSc1d1	denní
konzumace	konzumace	k1gFnSc1	konzumace
čaje	čaj	k1gInSc2	čaj
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
690	[number]	k4	690
<g/>
mg	mg	kA	mg
katechinů	katechin	k1gInPc2	katechin
<g />
.	.	kIx.	.
</s>
<s>
po	po	k7c4	po
12	[number]	k4	12
týdnů	týden	k1gInPc2	týden
snížila	snížit	k5eAaPmAgFnS	snížit
množství	množství	k1gNnSc4	množství
tělesného	tělesný	k2eAgInSc2d1	tělesný
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
příjem	příjem	k1gInSc1	příjem
katechinů	katechin	k1gInPc2	katechin
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
užitečný	užitečný	k2eAgInSc1d1	užitečný
při	při	k7c6	při
prevenci	prevence	k1gFnSc6	prevence
a	a	k8xC	a
zlepšování	zlepšování	k1gNnSc4	zlepšování
chorob	choroba	k1gFnPc2	choroba
špatného	špatný	k2eAgInSc2d1	špatný
životního	životní	k2eAgInSc2d1	životní
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
obezity	obezita	k1gFnSc2	obezita
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Antioxidanty	antioxidant	k1gInPc1	antioxidant
v	v	k7c6	v
zeleném	zelený	k2eAgInSc6d1	zelený
čaji	čaj	k1gInSc6	čaj
mohou	moct	k5eAaImIp3nP	moct
pomoci	pomoct	k5eAaPmF	pomoct
prevenci	prevence	k1gFnSc3	prevence
a	a	k8xC	a
snížení	snížení	k1gNnSc3	snížení
obtíží	obtíž	k1gFnPc2	obtíž
při	při	k7c6	při
revmatické	revmatický	k2eAgFnSc6d1	revmatická
artritidě	artritida	k1gFnSc6	artritida
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
alespoň	alespoň	k9	alespoň
uvádí	uvádět	k5eAaImIp3nS	uvádět
studie	studie	k1gFnSc1	studie
CWRU	CWRU	kA	CWRU
School	School	k1gInSc1	School
of	of	k?	of
Medicine	Medicin	k1gInSc5	Medicin
uveřejněná	uveřejněný	k2eAgFnSc1d1	uveřejněná
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2005	[number]	k4	2005
ve	v	k7c6	v
vydání	vydání	k1gNnSc6	vydání
magazínu	magazín	k1gInSc2	magazín
Proceedings	Proceedings	k1gInSc1	Proceedings
of	of	k?	of
the	the	k?	the
National	National	k1gMnSc1	National
Academy	Academa	k1gFnSc2	Academa
of	of	k?	of
Sciences	Sciences	k1gMnSc1	Sciences
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
vliv	vliv	k1gInSc4	vliv
polyfenolů	polyfenol	k1gInPc2	polyfenol
zeleného	zelené	k1gNnSc2	zelené
čaje	čaj	k1gInSc2	čaj
na	na	k7c6	na
kolagenem	kolagen	k1gInSc7	kolagen
vyvolanou	vyvolaný	k2eAgFnSc4d1	vyvolaná
artritidu	artritida	k1gFnSc4	artritida
u	u	k7c2	u
myší	myš	k1gFnPc2	myš
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
revmatické	revmatický	k2eAgFnSc3d1	revmatická
artritidě	artritida	k1gFnSc3	artritida
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
testovaných	testovaný	k2eAgFnPc2d1	testovaná
skupin	skupina	k1gFnPc2	skupina
se	se	k3xPyFc4	se
u	u	k7c2	u
myší	myš	k1gFnPc2	myš
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
byly	být	k5eAaImAgInP	být
podávány	podáván	k2eAgInPc1d1	podáván
polyfenoly	polyfenol	k1gInPc1	polyfenol
zeleného	zelený	k2eAgInSc2d1	zelený
čaje	čaj	k1gInSc2	čaj
<g/>
,	,	kIx,	,
rozvinula	rozvinout	k5eAaPmAgFnS	rozvinout
artritida	artritida	k1gFnSc1	artritida
významně	významně	k6eAd1	významně
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
18	[number]	k4	18
myší	myš	k1gFnPc2	myš
<g/>
,	,	kIx,	,
kterým	který	k3yIgNnSc7	který
byl	být	k5eAaImAgInS	být
zelený	zelený	k2eAgInSc4d1	zelený
čaj	čaj	k1gInSc4	čaj
podáván	podávat	k5eAaImNgInS	podávat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
artritida	artritida	k1gFnSc1	artritida
objevila	objevit	k5eAaPmAgFnS	objevit
jen	jen	k9	jen
u	u	k7c2	u
8	[number]	k4	8
(	(	kIx(	(
<g/>
44	[number]	k4	44
procent	procento	k1gNnPc2	procento
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
18	[number]	k4	18
myší	myš	k1gFnPc2	myš
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zelený	zelený	k2eAgInSc4d1	zelený
čaj	čaj	k1gInSc4	čaj
nedostávaly	dostávat	k5eNaImAgFnP	dostávat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
artritida	artritida	k1gFnSc1	artritida
objevila	objevit	k5eAaPmAgFnS	objevit
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
jediné	jediný	k2eAgFnSc2d1	jediná
(	(	kIx(	(
<g/>
94	[number]	k4	94
procent	procento	k1gNnPc2	procento
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
výzkumníci	výzkumník	k1gMnPc1	výzkumník
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
8	[number]	k4	8
myší	myš	k1gFnPc2	myš
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dostávaly	dostávat	k5eAaImAgFnP	dostávat
zelený	zelený	k2eAgInSc4d1	zelený
čaj	čaj	k1gInSc4	čaj
a	a	k8xC	a
onemocněly	onemocnět	k5eAaPmAgFnP	onemocnět
artritidou	artritida	k1gFnSc7	artritida
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
artritidy	artritida	k1gFnSc2	artritida
<g/>
.	.	kIx.	.
<g/>
Německá	německý	k2eAgFnSc1d1	německá
studie	studie	k1gFnSc1	studie
odhalila	odhalit	k5eAaPmAgFnS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přikládání	přikládání	k1gNnSc1	přikládání
filtrovaného	filtrovaný	k2eAgInSc2d1	filtrovaný
odvaru	odvar	k1gInSc2	odvar
z	z	k7c2	z
extraktu	extrakt	k1gInSc2	extrakt
zeleného	zelený	k2eAgInSc2d1	zelený
čaje	čaj	k1gInSc2	čaj
a	a	k8xC	a
horké	horký	k2eAgFnSc2d1	horká
vody	voda	k1gFnSc2	voda
na	na	k7c4	na
kůži	kůže	k1gFnSc4	kůže
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
třikrát	třikrát	k6eAd1	třikrát
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
pomoci	pomoct	k5eAaPmF	pomoct
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc4	jejichž
kůže	kůže	k1gFnSc1	kůže
byla	být	k5eAaImAgFnS	být
poškozena	poškodit	k5eAaPmNgFnS	poškodit
radiační	radiační	k2eAgFnSc7d1	radiační
terapií	terapie	k1gFnSc7	terapie
(	(	kIx(	(
<g/>
po	po	k7c6	po
16-22	[number]	k4	16-22
dnech	den	k1gInPc6	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Studie	studie	k1gFnSc1	studie
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1999	[number]	k4	1999
v	v	k7c6	v
magazínu	magazín	k1gInSc6	magazín
American	American	k1gMnSc1	American
Journal	Journal	k1gMnSc1	Journal
of	of	k?	of
Clinical	Clinical	k1gFnSc1	Clinical
Nutrition	Nutrition	k1gInSc1	Nutrition
odhaluje	odhalovat	k5eAaImIp3nS	odhalovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
zelený	zelený	k2eAgInSc1d1	zelený
čaj	čaj	k1gInSc1	čaj
má	mít	k5eAaImIp3nS	mít
termogenické	termogenický	k2eAgFnPc4d1	termogenický
složky	složka	k1gFnPc4	složka
a	a	k8xC	a
podporuje	podporovat	k5eAaImIp3nS	podporovat
oxidaci	oxidace	k1gFnSc4	oxidace
tuku	tuk	k1gInSc2	tuk
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nS	aby
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
pouze	pouze	k6eAd1	pouze
samotným	samotný	k2eAgInSc7d1	samotný
obsahem	obsah	k1gInSc7	obsah
kofeinu	kofein	k1gInSc2	kofein
<g/>
.	.	kIx.	.
</s>
<s>
Extrakt	extrakt	k1gInSc1	extrakt
ze	z	k7c2	z
zeleného	zelený	k2eAgInSc2d1	zelený
čaje	čaj	k1gInSc2	čaj
může	moct	k5eAaImIp3nS	moct
sehrát	sehrát	k5eAaPmF	sehrát
úlohu	úloha	k1gFnSc4	úloha
v	v	k7c6	v
řízení	řízení	k1gNnSc6	řízení
tělesných	tělesný	k2eAgInPc2d1	tělesný
procesů	proces	k1gInPc2	proces
sympatickou	sympatický	k2eAgFnSc7d1	sympatická
aktivací	aktivace	k1gFnSc7	aktivace
termogeneze	termogeneze	k1gFnSc2	termogeneze
<g/>
,	,	kIx,	,
oxidace	oxidace	k1gFnSc2	oxidace
tuku	tuk	k1gInSc2	tuk
nebo	nebo	k8xC	nebo
obou	dva	k4xCgFnPc2	dva
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
V	v	k7c6	v
laboratorních	laboratorní	k2eAgInPc6d1	laboratorní
testech	test	k1gInPc6	test
bylo	být	k5eAaImAgNnS	být
zjištěno	zjištěn	k2eAgNnSc1d1	zjištěno
<g/>
,	,	kIx,	,
že	že	k8xS	že
EGCG	EGCG	kA	EGCG
(	(	kIx(	(
<g/>
katechiny	katechin	k2eAgFnPc4d1	katechin
<g/>
)	)	kIx)	)
obsažené	obsažený	k2eAgFnPc4d1	obsažená
v	v	k7c6	v
zeleném	zelený	k2eAgInSc6d1	zelený
čaji	čaj	k1gInSc6	čaj
brání	bránit	k5eAaImIp3nS	bránit
HIV	HIV	kA	HIV
v	v	k7c6	v
napadání	napadání	k1gNnSc6	napadání
T-buněk	Tuňka	k1gFnPc2	T-buňka
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
nejsou	být	k5eNaImIp3nP	být
dosud	dosud	k6eAd1	dosud
známy	znám	k2eAgInPc1d1	znám
stejné	stejný	k2eAgInPc1d1	stejný
účinky	účinek	k1gInPc4	účinek
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
<g/>
Studie	studie	k1gFnSc1	studie
uveřejněná	uveřejněný	k2eAgFnSc1d1	uveřejněná
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2003	[number]	k4	2003
v	v	k7c6	v
magazínu	magazín	k1gInSc6	magazín
Cellular	Cellular	k1gMnSc1	Cellular
and	and	k?	and
Molecular	Molecular	k1gMnSc1	Molecular
Life	Lif	k1gInSc2	Lif
Sciences	Sciences	k1gInSc1	Sciences
věnujícího	věnující	k2eAgMnSc2d1	věnující
se	se	k3xPyFc4	se
novým	nový	k2eAgFnPc3d1	nová
potenciálním	potenciální	k2eAgFnPc3d1	potenciální
aplikacím	aplikace	k1gFnPc3	aplikace
<g/>
,	,	kIx,	,
odhalila	odhalit	k5eAaPmAgFnS	odhalit
"	"	kIx"	"
<g/>
a	a	k8xC	a
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
novou	nový	k2eAgFnSc4d1	nová
potenciální	potenciální	k2eAgFnSc4d1	potenciální
aplikaci	aplikace	k1gFnSc4	aplikace
(	(	kIx(	(
<g/>
−	−	k?	−
<g/>
)	)	kIx)	)
<g/>
-epigalokatechinu-	pigalokatechinu-	k?	-epigalokatechinu-
<g/>
3	[number]	k4	3
<g/>
-galátu	alát	k1gInSc2	-galát
(	(	kIx(	(
<g/>
EGCG	EGCG	kA	EGCG
<g/>
)	)	kIx)	)
v	v	k7c6	v
prevenci	prevence	k1gFnSc6	prevence
nebo	nebo	k8xC	nebo
léčbě	léčba	k1gFnSc6	léčba
zánětlivých	zánětlivý	k2eAgInPc2d1	zánětlivý
procesů	proces	k1gInPc2	proces
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Japonské	japonský	k2eAgInPc4d1	japonský
zelené	zelený	k2eAgInPc4d1	zelený
čaje	čaj	k1gInPc4	čaj
==	==	k?	==
</s>
</p>
<p>
<s>
Zelený	zelený	k2eAgInSc1d1	zelený
čaj	čaj	k1gInSc1	čaj
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
緑	緑	k?	緑
Rjokuča	Rjokuča	k1gFnSc1	Rjokuča
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
všudypřítomný	všudypřítomný	k2eAgMnSc1d1	všudypřítomný
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
nazýván	nazývat	k5eAaImNgInS	nazývat
"	"	kIx"	"
<g/>
Japonský	japonský	k2eAgInSc1d1	japonský
čaj	čaj	k1gInSc1	čaj
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
日	日	k?	日
Nihonča	Nihonča	k1gFnSc1	Nihonča
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
jen	jen	k9	jen
"	"	kIx"	"
<g/>
čaj	čaj	k1gInSc1	čaj
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
お	お	k?	お
Oča	Oča	k1gFnSc1	Oča
<g/>
)	)	kIx)	)
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
jediný	jediný	k2eAgInSc1d1	jediný
druh	druh	k1gInSc1	druh
čaje	čaj	k1gInSc2	čaj
zde	zde	k6eAd1	zde
pěstovaný	pěstovaný	k2eAgInSc1d1	pěstovaný
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejlepší	dobrý	k2eAgInPc1d3	nejlepší
japonské	japonský	k2eAgInPc1d1	japonský
čaje	čaj	k1gInPc1	čaj
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Udži	Udž	k1gFnSc2	Udž
v	v	k7c6	v
Kjótu	Kjóto	k1gNnSc6	Kjóto
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
první	první	k4xOgFnPc1	první
plantáže	plantáž	k1gFnPc1	plantáž
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
prefektura	prefektura	k1gFnSc1	prefektura
Šizuoka	Šizuoko	k1gNnSc2	Šizuoko
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
svým	svůj	k3xOyFgInSc7	svůj
zeleným	zelený	k2eAgInSc7d1	zelený
čajem	čaj	k1gInSc7	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Kvalitní	kvalitní	k2eAgInSc1d1	kvalitní
čaj	čaj	k1gInSc1	čaj
se	se	k3xPyFc4	se
však	však	k9	však
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
takřka	takřka	k6eAd1	takřka
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
čínských	čínský	k2eAgInPc2d1	čínský
zelených	zelený	k2eAgInPc2d1	zelený
čajů	čaj	k1gInPc2	čaj
se	se	k3xPyFc4	se
k	k	k7c3	k
přerušení	přerušení	k1gNnSc3	přerušení
oxidace	oxidace	k1gFnSc2	oxidace
většinou	většina	k1gFnSc7	většina
používá	používat	k5eAaImIp3nS	používat
horká	horký	k2eAgFnSc1d1	horká
pára	pára	k1gFnSc1	pára
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
především	především	k9	především
dle	dle	k7c2	dle
způsobu	způsob	k1gInSc2	způsob
zpracování	zpracování	k1gNnSc2	zpracování
<g/>
,	,	kIx,	,
od	od	k7c2	od
každého	každý	k3xTgInSc2	každý
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c4	mnoho
variant	varianta	k1gFnPc2	varianta
jak	jak	k6eAd1	jak
cenových	cenový	k2eAgFnPc2d1	cenová
<g/>
,	,	kIx,	,
tak	tak	k9	tak
kvalitativních	kvalitativní	k2eAgFnPc2d1	kvalitativní
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
řada	řada	k1gFnSc1	řada
zvláštních	zvláštní	k2eAgInPc2d1	zvláštní
čajů	čaj	k1gInPc2	čaj
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
mimo	mimo	k7c4	mimo
toto	tento	k3xDgNnSc4	tento
spektrum	spektrum	k1gNnSc4	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
faktorem	faktor	k1gInSc7	faktor
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
také	také	k9	také
použitý	použitý	k2eAgInSc4d1	použitý
kultivar	kultivar	k1gInSc4	kultivar
(	(	kIx(	(
<g/>
odrůda	odrůda	k1gFnSc1	odrůda
<g/>
)	)	kIx)	)
čajovníku	čajovník	k1gInSc2	čajovník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
čajům	čaj	k1gInPc3	čaj
specifika	specifikon	k1gNnSc2	specifikon
své	svůj	k3xOyFgFnSc2	svůj
chuti	chuť	k1gFnSc2	chuť
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zjednodušení	zjednodušení	k1gNnSc4	zjednodušení
se	se	k3xPyFc4	se
však	však	k9	však
dále	daleko	k6eAd2	daleko
uvádí	uvádět	k5eAaImIp3nS	uvádět
pouze	pouze	k6eAd1	pouze
dělení	dělení	k1gNnSc1	dělení
podle	podle	k7c2	podle
zpracování	zpracování	k1gNnSc2	zpracování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
番	番	k?	番
Banča	Banča	k1gFnSc1	Banča
(	(	kIx(	(
<g/>
Bancha	Bancha	k1gFnSc1	Bancha
<g/>
,	,	kIx,	,
běžný	běžný	k2eAgInSc1d1	běžný
čaj	čaj	k1gInSc1	čaj
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc1d1	základní
třída	třída	k1gFnSc1	třída
japonského	japonský	k2eAgInSc2d1	japonský
zeleného	zelený	k2eAgInSc2d1	zelený
čaje	čaj	k1gInSc2	čaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
běžně	běžně	k6eAd1	běžně
podávaný	podávaný	k2eAgMnSc1d1	podávaný
k	k	k7c3	k
jídlu	jídlo	k1gNnSc3	jídlo
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
nízkému	nízký	k2eAgInSc3d1	nízký
obsahu	obsah	k1gInSc3	obsah
kofeinu	kofein	k1gInSc2	kofein
je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
i	i	k9	i
k	k	k7c3	k
večernímu	večerní	k2eAgNnSc3d1	večerní
popíjení	popíjení	k1gNnSc3	popíjení
<g/>
.	.	kIx.	.
</s>
<s>
Nálev	nálev	k1gInSc1	nálev
bývá	bývat	k5eAaImIp3nS	bývat
světle	světle	k6eAd1	světle
zelenožluté	zelenožlutý	k2eAgFnPc4d1	zelenožlutá
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
chuť	chuť	k1gFnSc1	chuť
bývá	bývat	k5eAaImIp3nS	bývat
výrazná	výrazný	k2eAgFnSc1d1	výrazná
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
tóny	tón	k1gInPc7	tón
mořských	mořský	k2eAgFnPc2d1	mořská
řas	řasa	k1gFnPc2	řasa
<g/>
.	.	kIx.	.
<g/>
煎	煎	k?	煎
Senča	Senča	k1gFnSc1	Senča
(	(	kIx(	(
<g/>
Sencha	Sencha	k1gFnSc1	Sencha
<g/>
,	,	kIx,	,
sekaný	sekaný	k2eAgInSc1d1	sekaný
čaj	čaj	k1gInSc1	čaj
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejrozšířenější	rozšířený	k2eAgInSc1d3	nejrozšířenější
typ	typ	k1gInSc1	typ
japonského	japonský	k2eAgInSc2d1	japonský
čaje	čaj	k1gInSc2	čaj
<g/>
.	.	kIx.	.
</s>
<s>
List	list	k1gInSc1	list
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc4	jehož
oxidace	oxidace	k1gFnSc1	oxidace
byla	být	k5eAaImAgFnS	být
zastavena	zastavit	k5eAaPmNgFnS	zastavit
napařením	napaření	k1gNnSc7	napaření
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
svinut	svinut	k2eAgMnSc1d1	svinut
do	do	k7c2	do
tenkých	tenký	k2eAgFnPc2d1	tenká
tmavě	tmavě	k6eAd1	tmavě
zelených	zelený	k2eAgFnPc2d1	zelená
jehliček	jehlička	k1gFnPc2	jehlička
<g/>
.	.	kIx.	.
</s>
<s>
Sencha	Sencha	k1gFnSc1	Sencha
označuje	označovat	k5eAaImIp3nS	označovat
způsob	způsob	k1gInSc4	způsob
zpracování	zpracování	k1gNnSc4	zpracování
čaje	čaj	k1gInSc2	čaj
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
čaji	čaj	k1gInPc7	čaj
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnSc2d1	nízká
kvality	kvalita	k1gFnSc2	kvalita
až	až	k9	až
po	po	k7c6	po
zcela	zcela	k6eAd1	zcela
ručně	ručně	k6eAd1	ručně
zpracované	zpracovaný	k2eAgInPc4d1	zpracovaný
exkluzivní	exkluzivní	k2eAgInPc4d1	exkluzivní
čaje	čaj	k1gInPc4	čaj
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
cena	cena	k1gFnSc1	cena
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
takřka	takřka	k6eAd1	takřka
astronomických	astronomický	k2eAgFnPc2d1	astronomická
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
<g/>
冠	冠	k?	冠
Kabusé	Kabusý	k2eAgFnPc4d1	Kabusý
(	(	kIx(	(
<g/>
Kabuse	Kabuse	k1gFnPc4	Kabuse
<g/>
,	,	kIx,	,
zakrytý	zakrytý	k2eAgInSc4d1	zakrytý
čaj	čaj	k1gInSc4	čaj
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kabusé	Kabusý	k2eAgNnSc1d1	Kabusý
je	být	k5eAaImIp3nS	být
čaj	čaj	k1gInSc4	čaj
zpracovaný	zpracovaný	k2eAgInSc4d1	zpracovaný
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
jako	jako	k9	jako
Sencha	Sencha	k1gMnSc1	Sencha
<g/>
,	,	kIx,	,
liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
však	však	k9	však
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
čajovníky	čajovník	k1gInPc1	čajovník
byly	být	k5eAaImAgInP	být
před	před	k7c7	před
sklizní	sklizeň	k1gFnSc7	sklizeň
na	na	k7c4	na
10	[number]	k4	10
dní	den	k1gInPc2	den
zastíněny	zastíněn	k2eAgInPc1d1	zastíněn
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc1	takový
čaje	čaj	k1gInPc1	čaj
pak	pak	k6eAd1	pak
mají	mít	k5eAaImIp3nP	mít
chuť	chuť	k1gFnSc4	chuť
jemnější	jemný	k2eAgFnSc4d2	jemnější
a	a	k8xC	a
méně	málo	k6eAd2	málo
trpkou	trpký	k2eAgFnSc4d1	trpká
nežli	nežli	k8xS	nežli
klasická	klasický	k2eAgFnSc1d1	klasická
Sencha	Sencha	k1gFnSc1	Sencha
<g/>
.	.	kIx.	.
<g/>
玉	玉	k?	玉
Gjokuro	Gjokura	k1gFnSc5	Gjokura
(	(	kIx(	(
<g/>
Gyokuro	Gyokura	k1gFnSc5	Gyokura
<g/>
,	,	kIx,	,
Nefritová	fritový	k2eNgFnSc1d1	Nefritová
rosa	rosa	k1gFnSc1	rosa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Čaj	čaj	k1gInSc1	čaj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
špatně	špatně	k6eAd1	špatně
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
nejkvalitnější	kvalitní	k2eAgInSc4d3	nejkvalitnější
japonský	japonský	k2eAgInSc4d1	japonský
čaj	čaj	k1gInSc4	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
čaj	čaj	k1gInSc4	čaj
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
který	který	k3yQgInSc4	který
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
především	především	k9	především
způsob	způsob	k1gInSc4	způsob
zpracování	zpracování	k1gNnSc2	zpracování
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
produkován	produkovat	k5eAaImNgInS	produkovat
z	z	k7c2	z
čajovníků	čajovník	k1gInPc2	čajovník
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
před	před	k7c7	před
sklizní	sklizeň	k1gFnSc7	sklizeň
30	[number]	k4	30
dní	den	k1gInPc2	den
stíněny	stíněn	k2eAgFnPc1d1	stíněna
a	a	k8xC	a
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
výrobu	výroba	k1gFnSc4	výroba
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgInPc1d1	vhodný
jen	jen	k9	jen
některé	některý	k3yIgInPc1	některý
kultivary	kultivar	k1gInPc1	kultivar
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Gokoh	Gokoh	k1gInSc1	Gokoh
<g/>
,	,	kIx,	,
Saemidori	Saemidori	k1gNnSc1	Saemidori
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nálev	nálev	k1gInSc1	nálev
těchto	tento	k3xDgInPc2	tento
čajů	čaj	k1gInPc2	čaj
bývá	bývat	k5eAaImIp3nS	bývat
velmi	velmi	k6eAd1	velmi
plný	plný	k2eAgInSc1d1	plný
<g/>
,	,	kIx,	,
kulatý	kulatý	k2eAgInSc1d1	kulatý
<g/>
,	,	kIx,	,
zelenobílé	zelenobílý	k2eAgInPc1d1	zelenobílý
"	"	kIx"	"
<g/>
mléčné	mléčný	k2eAgFnPc1d1	mléčná
<g/>
"	"	kIx"	"
barvy	barva	k1gFnPc1	barva
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
syté	sytý	k2eAgFnSc2d1	sytá
<g/>
,	,	kIx,	,
nasládlé	nasládlý	k2eAgFnSc2d1	nasládlá
chuti	chuť	k1gFnSc2	chuť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
čajích	čaj	k1gInPc6	čaj
bývá	bývat	k5eAaImIp3nS	bývat
nejzřetelnější	zřetelný	k2eAgFnSc1d3	nejzřetelnější
typická	typický	k2eAgFnSc1d1	typická
chuť	chuť	k1gFnSc1	chuť
umami	uma	k1gFnPc7	uma
<g/>
.	.	kIx.	.
<g/>
抹	抹	k?	抹
Mačča	Mačča	k1gFnSc1	Mačča
(	(	kIx(	(
<g/>
Matcha	Matcha	k1gFnSc1	Matcha
<g/>
,	,	kIx,	,
drcený	drcený	k2eAgInSc1d1	drcený
čaj	čaj	k1gInSc1	čaj
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Velmi	velmi	k6eAd1	velmi
kvalitní	kvalitní	k2eAgInSc1d1	kvalitní
práškový	práškový	k2eAgInSc1d1	práškový
zelený	zelený	k2eAgInSc1d1	zelený
čaj	čaj	k1gInSc1	čaj
používaný	používaný	k2eAgInSc1d1	používaný
zejména	zejména	k9	zejména
při	při	k7c6	při
čajových	čajový	k2eAgInPc6d1	čajový
obřadech	obřad	k1gInPc6	obřad
<g/>
.	.	kIx.	.
</s>
<s>
Nižší	nízký	k2eAgFnPc1d2	nižší
sorty	sorta	k1gFnPc1	sorta
čaje	čaj	k1gInSc2	čaj
Matcha	Match	k1gMnSc2	Match
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
v	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
–	–	k?	–
jako	jako	k8xC	jako
příchuť	příchuť	k1gFnSc4	příchuť
do	do	k7c2	do
zmrzlin	zmrzlina	k1gFnPc2	zmrzlina
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
japonských	japonský	k2eAgFnPc2d1	japonská
sladkostí	sladkost	k1gFnPc2	sladkost
<g/>
.	.	kIx.	.
<g/>
玄	玄	k?	玄
Genmaiča	Genmaiča	k1gFnSc1	Genmaiča
(	(	kIx(	(
<g/>
čaj	čaj	k1gInSc1	čaj
s	s	k7c7	s
hnědou	hnědý	k2eAgFnSc7d1	hnědá
rýží	rýže	k1gFnSc7	rýže
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
banča	banča	k6eAd1	banča
s	s	k7c7	s
přídavkem	přídavek	k1gInSc7	přídavek
pražené	pražený	k2eAgFnSc2d1	pražená
hnědé	hnědý	k2eAgFnSc2d1	hnědá
rýže	rýže	k1gFnSc2	rýže
(	(	kIx(	(
<g/>
genmai	genmai	k6eAd1	genmai
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
茎	茎	k?	茎
Kukiča	Kukičus	k1gMnSc2	Kukičus
(	(	kIx(	(
<g/>
Kukicha	Kukich	k1gMnSc2	Kukich
<g/>
,	,	kIx,	,
stonkový	stonkový	k2eAgInSc1d1	stonkový
čaj	čaj	k1gInSc1	čaj
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Čaj	čaj	k1gInSc1	čaj
produkovaný	produkovaný	k2eAgInSc1d1	produkovaný
z	z	k7c2	z
"	"	kIx"	"
<g/>
odpadu	odpad	k1gInSc2	odpad
<g/>
"	"	kIx"	"
z	z	k7c2	z
výroby	výroba	k1gFnSc2	výroba
čajů	čaj	k1gInPc2	čaj
Sencha	Sencha	k1gFnSc1	Sencha
či	či	k8xC	či
Gyokuro	Gyokura	k1gFnSc5	Gyokura
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
sorty	sorta	k1gFnSc2	sorta
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
čaji	čaj	k1gInSc6	čaj
různý	různý	k2eAgInSc4d1	různý
podíl	podíl	k1gInSc4	podíl
řapíků	řapík	k1gInPc2	řapík
<g/>
,	,	kIx,	,
čajové	čajový	k2eAgFnSc2d1	čajová
prachu	prach	k1gInSc2	prach
a	a	k8xC	a
zlomkového	zlomkový	k2eAgInSc2d1	zlomkový
listu	list	k1gInSc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Chuť	chuť	k1gFnSc1	chuť
většinou	většina	k1gFnSc7	většina
bývá	bývat	k5eAaImIp3nS	bývat
lehčí	lehký	k2eAgMnSc1d2	lehčí
<g/>
,	,	kIx,	,
s	s	k7c7	s
výraznými	výrazný	k2eAgInPc7d1	výrazný
trávovými	trávový	k2eAgInPc7d1	trávový
tóny	tón	k1gInPc7	tón
<g/>
.	.	kIx.	.
<g/>
焙	焙	k?	焙
Hódžiča	Hódžiča	k1gFnSc1	Hódžiča
(	(	kIx(	(
<g/>
Hō	Hō	k1gFnSc1	Hō
<g/>
,	,	kIx,	,
na	na	k7c6	na
pánvi	pánev	k1gFnSc6	pánev
sušený	sušený	k2eAgInSc4d1	sušený
čaj	čaj	k1gInSc4	čaj
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pražená	pražený	k2eAgFnSc1d1	pražená
Kukicha	Kukich	k1gMnSc2	Kukich
<g/>
.	.	kIx.	.
<g/>
玉	玉	k?	玉
Tamarjokuča	Tamarjokuča	k1gFnSc1	Tamarjokuča
(	(	kIx(	(
<g/>
Tamaryokucha	Tamaryokucha	k1gFnSc1	Tamaryokucha
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zelený	zelený	k2eAgInSc1d1	zelený
čaj	čaj	k1gInSc1	čaj
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
označovaný	označovaný	k2eAgInSc1d1	označovaný
též	též	k9	též
jako	jako	k9	jako
Guricha	Gurich	k1gMnSc4	Gurich
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
svinut	svinout	k5eAaPmNgMnS	svinout
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
čínských	čínský	k2eAgInPc2d1	čínský
čajů	čaj	k1gInPc2	čaj
do	do	k7c2	do
zakroucených	zakroucený	k2eAgInPc2d1	zakroucený
"	"	kIx"	"
<g/>
drátků	drátek	k1gInPc2	drátek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
všech	všecek	k3xTgInPc2	všecek
ostatních	ostatní	k2eAgInPc2d1	ostatní
japonských	japonský	k2eAgInPc2d1	japonský
čajů	čaj	k1gInPc2	čaj
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
čajů	čaj	k1gInPc2	čaj
Tamaryokucha	Tamaryokucha	k1gFnSc1	Tamaryokucha
byla	být	k5eAaImAgFnS	být
oxidace	oxidace	k1gFnSc2	oxidace
zastavena	zastavit	k5eAaPmNgFnS	zastavit
lehkým	lehký	k2eAgNnSc7d1	lehké
pražením	pražení	k1gNnSc7	pražení
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
napaření	napaření	k1gNnSc2	napaření
lístků	lístek	k1gInPc2	lístek
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
čaj	čaj	k1gInSc1	čaj
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
Kamairi	Kamaire	k1gFnSc4	Kamaire
Tamaryokucha	Tamaryokuch	k1gMnSc2	Tamaryokuch
<g/>
.	.	kIx.	.
</s>
<s>
Chuť	chuť	k1gFnSc1	chuť
těchto	tento	k3xDgInPc2	tento
čajů	čaj	k1gInPc2	čaj
bývá	bývat	k5eAaImIp3nS	bývat
plná	plný	k2eAgFnSc1d1	plná
<g/>
,	,	kIx,	,
lehce	lehko	k6eAd1	lehko
pečená	pečený	k2eAgFnSc1d1	pečená
<g/>
,	,	kIx,	,
nasládlá	nasládlý	k2eAgFnSc1d1	nasládlá
<g/>
,	,	kIx,	,
u	u	k7c2	u
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
sort	sorta	k1gFnPc2	sorta
pak	pak	k6eAd1	pak
částečně	částečně	k6eAd1	částečně
připomíná	připomínat	k5eAaImIp3nS	připomínat
čaje	čaj	k1gInPc4	čaj
typu	typ	k1gInSc2	typ
Gyokuro	Gyokura	k1gFnSc5	Gyokura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Čínské	čínský	k2eAgInPc1d1	čínský
zelené	zelený	k2eAgInPc1d1	zelený
čaje	čaj	k1gInPc1	čaj
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Provincie	provincie	k1gFnSc1	provincie
Zhejiang	Zhejiang	k1gInSc1	Zhejiang
浙	浙	k?	浙
===	===	k?	===
</s>
</p>
<p>
<s>
Zhejiang	Zhejiang	k1gInSc1	Zhejiang
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
nejznámějšího	známý	k2eAgMnSc2d3	nejznámější
z	z	k7c2	z
čajů	čaj	k1gInPc2	čaj
<g/>
,	,	kIx,	,
Xi	Xi	k1gFnPc2	Xi
Hu	hu	k0	hu
Longjing	Longjing	k1gInSc4	Longjing
(	(	kIx(	(
<g/>
西	西	k?	西
<g/>
,	,	kIx,	,
<g/>
Loong	Loong	k1gMnSc1	Loong
Tseng	Tseng	k1gMnSc1	Tseng
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgInPc2d1	další
vysoce	vysoce	k6eAd1	vysoce
kvalitních	kvalitní	k2eAgInPc2d1	kvalitní
čajů	čaj	k1gInPc2	čaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Longjing龙	Longjing龙	k?	Longjing龙
</s>
</p>
<p>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
z	z	k7c2	z
vynikajících	vynikající	k2eAgInPc2d1	vynikající
čínských	čínský	k2eAgInPc2d1	čínský
čajů	čaj	k1gInPc2	čaj
z	z	k7c2	z
Hangzhou	Hangzha	k1gMnSc7	Hangzha
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
v	v	k7c6	v
čínštině	čínština	k1gFnSc6	čínština
znamená	znamenat	k5eAaImIp3nS	znamenat
dračí	dračí	k2eAgFnSc1d1	dračí
studna	studna	k1gFnSc1	studna
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
sušený	sušený	k2eAgMnSc1d1	sušený
na	na	k7c6	na
pánvích	pánev	k1gFnPc6	pánev
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
specifický	specifický	k2eAgInSc1d1	specifický
plochý	plochý	k2eAgInSc1d1	plochý
vzhled	vzhled	k1gInSc1	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
napodobování	napodobování	k1gNnSc1	napodobování
je	být	k5eAaImIp3nS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
,	,	kIx,	,
a	a	k8xC	a
vlastně	vlastně	k9	vlastně
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
množství	množství	k1gNnSc2	množství
dostupného	dostupný	k2eAgNnSc2d1	dostupné
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
je	být	k5eAaImIp3nS	být
produkována	produkován	k2eAgFnSc1d1	produkována
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Sichuan	Sichuana	k1gFnPc2	Sichuana
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
tedy	tedy	k9	tedy
originálním	originální	k2eAgInSc7d1	originální
čajem	čaj	k1gInSc7	čaj
Longjing	Longjing	k1gInSc1	Longjing
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hui	Hui	k?	Hui
Ming	Ming	k1gInSc1	Ming
</s>
</p>
<p>
<s>
Pojmenován	pojmenován	k2eAgInSc1d1	pojmenován
podle	podle	k7c2	podle
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
Zhejiangu	Zhejiang	k1gInSc6	Zhejiang
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Long	Long	k1gMnSc1	Long
Ding	Ding	k1gMnSc1	Ding
开	开	k?	开
</s>
</p>
<p>
<s>
Čaj	čaj	k1gInSc1	čaj
z	z	k7c2	z
kraje	kraj	k1gInSc2	kraj
Kaihua	Kaihu	k1gInSc2	Kaihu
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xS	jako
dračí	dračí	k2eAgFnSc1d1	dračí
hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hua	Hua	k?	Hua
Ding	Ding	k1gInSc1	Ding
</s>
</p>
<p>
<s>
Čaj	čaj	k1gInSc1	čaj
z	z	k7c2	z
kraje	kraj	k1gInSc2	kraj
Tiantai	Tianta	k1gFnSc2	Tianta
<g/>
,	,	kIx,	,
pojmenovaný	pojmenovaný	k2eAgInSc1d1	pojmenovaný
podle	podle	k7c2	podle
vrcholku	vrcholek	k1gInSc2	vrcholek
v	v	k7c6	v
Tiantajském	Tiantajský	k2eAgNnSc6d1	Tiantajský
pohoří	pohoří	k1gNnSc6	pohoří
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Qing	Qing	k1gMnSc1	Qing
Ding	Ding	k1gMnSc1	Ding
天	天	k?	天
</s>
</p>
<p>
<s>
Čaj	čaj	k1gInSc1	čaj
z	z	k7c2	z
Tian	Tian	k1gInSc4	Tian
Mu	on	k3xPp3gNnSc3	on
<g/>
,	,	kIx,	,
také	také	k9	také
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xC	jako
zelený	zelený	k2eAgInSc1d1	zelený
vrcholek	vrcholek	k1gInSc1	vrcholek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gunpowder	Gunpowder	k1gInSc1	Gunpowder
珠	珠	k?	珠
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Střelný	střelný	k2eAgInSc1d1	střelný
prach	prach	k1gInSc1	prach
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
populární	populární	k2eAgInSc1d1	populární
čaj	čaj	k1gInSc1	čaj
známý	známý	k2eAgInSc1d1	známý
také	také	k9	také
jako	jako	k9	jako
zhuchá	zhuchat	k5eAaPmIp3nS	zhuchat
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
Zhejiangu	Zhejiang	k1gInSc2	Zhejiang
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Provincie	provincie	k1gFnSc1	provincie
Hubei	Hube	k1gFnSc2	Hube
湖	湖	k?	湖
===	===	k?	===
</s>
</p>
<p>
<s>
Yu	Yu	k?	Yu
Lu	Lu	k1gFnSc1	Lu
玉	玉	k?	玉
</s>
</p>
<p>
<s>
Napařovaný	napařovaný	k2eAgInSc1d1	napařovaný
čaj	čaj	k1gInSc1	čaj
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xS	jako
Gyokuro	Gyokura	k1gFnSc5	Gyokura
(	(	kIx(	(
<g/>
Nefritová	nefritový	k2eAgFnSc1d1	nefritový
rosa	rosa	k1gFnSc1	rosa
<g/>
)	)	kIx)	)
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
v	v	k7c6	v
japonském	japonský	k2eAgInSc6d1	japonský
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Provincie	provincie	k1gFnSc1	provincie
Henan	Henan	k1gInSc1	Henan
河	河	k?	河
===	===	k?	===
</s>
</p>
<p>
<s>
Xin	Xin	k?	Xin
Yang	Yang	k1gMnSc1	Yang
Mao	Mao	k1gMnSc1	Mao
Jian	Jian	k1gMnSc1	Jian
信	信	k?	信
</s>
</p>
<p>
<s>
Slavný	slavný	k2eAgInSc1d1	slavný
čínský	čínský	k2eAgInSc1d1	čínský
čaj	čaj	k1gInSc1	čaj
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
také	také	k9	také
zelený	zelený	k2eAgInSc1d1	zelený
tips	tips	k1gInSc1	tips
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Provincie	provincie	k1gFnSc1	provincie
Jiangsu	Jiangs	k1gInSc2	Jiangs
江	江	k?	江
===	===	k?	===
</s>
</p>
<p>
<s>
Bi	Bi	k?	Bi
Luo	Luo	k1gMnSc1	Luo
Chun	Chun	k1gMnSc1	Chun
碧	碧	k?	碧
</s>
</p>
<p>
<s>
Slavný	slavný	k2eAgInSc1d1	slavný
čínský	čínský	k2eAgInSc1d1	čínský
čaj	čaj	k1gInSc1	čaj
z	z	k7c2	z
Dong	dong	k1gInSc4	dong
Tingu	Tinga	k1gFnSc4	Tinga
známý	známý	k2eAgInSc1d1	známý
také	také	k6eAd1	také
jako	jako	k8xC	jako
zelené	zelený	k2eAgFnPc1d1	zelená
hlemýždí	hlemýždí	k2eAgFnPc1d1	hlemýždí
spirálky	spirálka	k1gFnPc1	spirálka
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
čaje	čaj	k1gInSc2	čaj
Longjing	Longjing	k1gInSc4	Longjing
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
jeho	jeho	k3xOp3gNnSc1	jeho
napodobování	napodobování	k1gNnSc1	napodobování
čajovými	čajový	k2eAgMnPc7d1	čajový
obchodníky	obchodník	k1gMnPc7	obchodník
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nabízený	nabízený	k2eAgInSc1d1	nabízený
čaj	čaj	k1gInSc1	čaj
může	moct	k5eAaImIp3nS	moct
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
pocházet	pocházet	k5eAaImF	pocházet
z	z	k7c2	z
provincie	provincie	k1gFnSc2	provincie
Sichuan	Sichuany	k1gInPc2	Sichuany
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rain	Rain	k1gMnSc1	Rain
Flower	Flower	k1gMnSc1	Flower
</s>
</p>
<p>
<s>
Dešťová	dešťový	k2eAgFnSc1d1	dešťová
růže	růže	k1gFnSc1	růže
<g/>
,	,	kIx,	,
čaj	čaj	k1gInSc1	čaj
z	z	k7c2	z
Nanjingu	Nanjing	k1gInSc2	Nanjing
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Shui	Shui	k6eAd1	Shui
Xi	Xi	k1gMnSc1	Xi
Cui	Cui	k1gMnSc1	Cui
Bo水	Bo水	k1gMnSc1	Bo水
</s>
</p>
<p>
<s>
===	===	k?	===
Provincie	provincie	k1gFnSc2	provincie
Jiangxi	Jiangxe	k1gFnSc3	Jiangxe
江	江	k?	江
===	===	k?	===
</s>
</p>
<p>
<s>
Chun	Chun	k1gMnSc1	Chun
Mee	Mee	k1gMnSc1	Mee
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
vzácné	vzácný	k2eAgNnSc4d1	vzácné
obočí	obočí	k1gNnSc4	obočí
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Jiangxi	Jiangxe	k1gFnSc4	Jiangxe
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
i	i	k9	i
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Gou	Goa	k1gFnSc4	Goa
Gu	Gu	k1gMnSc2	Gu
Nao	Nao	k1gMnSc2	Nao
</s>
</p>
<p>
<s>
Hodně	hodně	k6eAd1	hodně
známý	známý	k2eAgInSc1d1	známý
čaj	čaj	k1gInSc1	čaj
a	a	k8xC	a
držitel	držitel	k1gMnSc1	držitel
mnoha	mnoho	k4c2	mnoho
národních	národní	k2eAgFnPc2d1	národní
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Yun	Yun	k?	Yun
Wu	Wu	k1gFnSc1	Wu
</s>
</p>
<p>
<s>
Čaj	čaj	k1gInSc1	čaj
známý	známý	k2eAgInSc1d1	známý
také	také	k9	také
jako	jako	k8xC	jako
čaj	čaj	k1gInSc4	čaj
mraků	mrak	k1gInPc2	mrak
a	a	k8xC	a
mlhy	mlha	k1gFnSc2	mlha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Provincie	provincie	k1gFnSc1	provincie
Anhui	Anhu	k1gFnSc2	Anhu
安	安	k?	安
===	===	k?	===
</s>
</p>
<p>
<s>
Anhui	Anhui	k1gNnSc1	Anhui
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
tří	tři	k4xCgInPc2	tři
z	z	k7c2	z
nejslavnějších	slavný	k2eAgInPc2d3	nejslavnější
čínských	čínský	k2eAgInPc2d1	čínský
čajů	čaj	k1gInPc2	čaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Da	Da	k?	Da
Fang	Fang	k1gInSc1	Fang
</s>
</p>
<p>
<s>
Čaj	čaj	k1gInSc1	čaj
z	z	k7c2	z
vrchu	vrch	k1gInSc2	vrch
Huangshan	Huangshany	k1gInPc2	Huangshany
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Huangshan	Huangshan	k1gMnSc1	Huangshan
Mao	Mao	k1gMnSc1	Mao
Feng	Feng	k1gMnSc1	Feng
黄	黄	k?	黄
</s>
</p>
<p>
<s>
Další	další	k2eAgInSc1d1	další
slavný	slavný	k2eAgInSc1d1	slavný
čínský	čínský	k2eAgInSc1d1	čínský
čaj	čaj	k1gInSc1	čaj
z	z	k7c2	z
vrchu	vrch	k1gInSc2	vrch
Huangshan	Huangshany	k1gInPc2	Huangshany
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lu	Lu	k?	Lu
An	An	k1gMnSc1	An
Guapian	Guapian	k1gMnSc1	Guapian
六	六	k?	六
</s>
</p>
<p>
<s>
Slavný	slavný	k2eAgInSc1d1	slavný
čínský	čínský	k2eAgInSc1d1	čínský
čaj	čaj	k1gInSc1	čaj
<g/>
,	,	kIx,	,
také	také	k9	také
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
melounové	melounový	k2eAgNnSc1d1	melounové
semínko	semínko	k1gNnSc1	semínko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hou	hou	k0	hou
Kui	Kui	k1gMnSc5	Kui
太	太	k?	太
</s>
</p>
<p>
<s>
Slavný	slavný	k2eAgInSc1d1	slavný
čínský	čínský	k2eAgInSc1d1	čínský
čaj	čaj	k1gInSc1	čaj
<g/>
,	,	kIx,	,
také	také	k9	také
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xS	jako
opičí	opičí	k2eAgInSc1d1	opičí
čaj	čaj	k1gInSc1	čaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tun	tuna	k1gFnPc2	tuna
Lu	Lu	k1gFnPc2	Lu
</s>
</p>
<p>
<s>
Čaj	čaj	k1gInSc1	čaj
z	z	k7c2	z
okresu	okres	k1gInSc2	okres
Tunxi	Tunxe	k1gFnSc4	Tunxe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Huo	Huo	k?	Huo
Qing	Qing	k1gInSc1	Qing
</s>
</p>
<p>
<s>
Čaj	čaj	k1gInSc1	čaj
z	z	k7c2	z
kraje	kraj	k1gInSc2	kraj
Jing	Jing	k1gInSc1	Jing
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
také	také	k9	také
jako	jako	k9	jako
hořící	hořící	k2eAgFnSc4d1	hořící
zeleň	zeleň	k1gFnSc4	zeleň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hyson	Hyson	k1gMnSc1	Hyson
</s>
</p>
<p>
<s>
Čaj	čaj	k1gInSc1	čaj
střední	střední	k2eAgFnSc2d1	střední
kvality	kvalita	k1gFnSc2	kvalita
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
provincií	provincie	k1gFnPc2	provincie
<g/>
,	,	kIx,	,
raný	raný	k2eAgInSc4d1	raný
sběr	sběr	k1gInSc4	sběr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgInPc1d1	další
zelené	zelený	k2eAgInPc1d1	zelený
čaje	čaj	k1gInPc1	čaj
==	==	k?	==
</s>
</p>
<p>
<s>
Cejlonské	cejlonský	k2eAgInPc1d1	cejlonský
zelené	zelený	k2eAgInPc1d1	zelený
čaje	čaj	k1gInPc1	čaj
</s>
</p>
<p>
<s>
Zelené	zelené	k1gNnSc1	zelené
čaje	čaj	k1gInSc2	čaj
z	z	k7c2	z
Darjeelingu	Darjeeling	k1gInSc2	Darjeeling
</s>
</p>
<p>
<s>
Zelené	Zelené	k2eAgInPc4d1	Zelené
vietnamské	vietnamský	k2eAgInPc4d1	vietnamský
čaje	čaj	k1gInPc4	čaj
</s>
</p>
<p>
<s>
Zelené	zelené	k1gNnSc1	zelené
čaje	čaj	k1gInSc2	čaj
z	z	k7c2	z
Assamu	Assam	k1gInSc2	Assam
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Green	Green	k1gInSc4	Green
tea	tea	k1gFnSc1	tea
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Čaj	čaj	k1gInSc1	čaj
</s>
</p>
<p>
<s>
Čajovna	čajovna	k1gFnSc1	čajovna
</s>
</p>
<p>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
čaj	čaj	k1gInSc1	čaj
</s>
</p>
<p>
<s>
Černý	černý	k2eAgInSc1d1	černý
čaj	čaj	k1gInSc1	čaj
</s>
</p>
<p>
<s>
Oolong	Oolong	k1gMnSc1	Oolong
</s>
</p>
<p>
<s>
Darjeeling	Darjeeling	k1gInSc1	Darjeeling
(	(	kIx(	(
<g/>
čaj	čaj	k1gInSc1	čaj
<g/>
)	)	kIx)	)
</s>
</p>
