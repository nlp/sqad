<s>
Themis	Themis	k1gFnSc1	Themis
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Θ	Θ	k?	Θ
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Themis	Themis	k1gFnSc1	Themis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
dcera	dcera	k1gFnSc1	dcera
boha	bůh	k1gMnSc2	bůh
nebe	nebe	k1gNnSc4	nebe
Úrana	Úran	k1gInSc2	Úran
a	a	k8xC	a
bohyně	bohyně	k1gFnSc2	bohyně
země	zem	k1gFnSc2	zem
Gaie	Gai	k1gFnSc2	Gai
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
bohyní	bohyně	k1gFnSc7	bohyně
zákonného	zákonný	k2eAgInSc2d1	zákonný
pořádku	pořádek	k1gInSc2	pořádek
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
