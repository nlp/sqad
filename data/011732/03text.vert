<p>
<s>
Themis	Themis	k1gFnSc1	Themis
(	(	kIx(	(
<g/>
řecky	řecky	k6eAd1	řecky
Θ	Θ	k?	Θ
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Themis	Themis	k1gFnSc1	Themis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
řecké	řecký	k2eAgFnSc6d1	řecká
mytologii	mytologie	k1gFnSc6	mytologie
dcera	dcera	k1gFnSc1	dcera
boha	bůh	k1gMnSc2	bůh
nebe	nebe	k1gNnSc4	nebe
Úrana	Úran	k1gInSc2	Úran
a	a	k8xC	a
bohyně	bohyně	k1gFnSc2	bohyně
země	zem	k1gFnSc2	zem
Gaie	Gai	k1gFnSc2	Gai
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
bohyní	bohyně	k1gFnSc7	bohyně
zákonného	zákonný	k2eAgInSc2d1	zákonný
pořádku	pořádek	k1gInSc2	pořádek
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významy	význam	k1gInPc4	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
Pelasgického	pelasgický	k2eAgInSc2d1	pelasgický
mýtu	mýtus	k1gInSc2	mýtus
o	o	k7c6	o
stvoření	stvoření	k1gNnSc6	stvoření
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
dcerou	dcera	k1gFnSc7	dcera
Eurynomé	Eurynomý	k2eAgFnSc2d1	Eurynomý
a	a	k8xC	a
hada	had	k1gMnSc2	had
Ofióna	Ofión	k1gMnSc2	Ofión
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Eurymedontem	Eurymedont	k1gInSc7	Eurymedont
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
Titánů	Titán	k1gMnPc2	Titán
přidělena	přidělen	k2eAgFnSc1d1	přidělena
jedné	jeden	k4xCgFnSc3	jeden
z	z	k7c2	z
hvězdných	hvězdný	k2eAgFnPc2d1	hvězdná
mocností	mocnost	k1gFnPc2	mocnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
případě	případ	k1gInSc6	případ
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
Jupiter	Jupiter	k1gMnSc1	Jupiter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
nazývána	nazývat	k5eAaImNgFnS	nazývat
"	"	kIx"	"
<g/>
druhou	druhý	k4xOgFnSc7	druhý
manželkou	manželka	k1gFnSc7	manželka
Diovou	Diův	k2eAgFnSc7d1	Diova
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
porodila	porodit	k5eAaPmAgFnS	porodit
mu	on	k3xPp3gMnSc3	on
Moiry	Moira	k1gFnPc4	Moira
–	–	k?	–
bohyně	bohyně	k1gFnPc4	bohyně
osudu	osud	k1gInSc2	osud
a	a	k8xC	a
Hóry	Hóra	k1gFnSc2	Hóra
–	–	k?	–
bohyně	bohyně	k1gFnSc2	bohyně
ročních	roční	k2eAgNnPc2d1	roční
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Stává	stávat	k5eAaImIp3nS	stávat
při	pře	k1gFnSc4	pře
Diovi	Diův	k2eAgMnPc1d1	Diův
<g/>
,	,	kIx,	,
když	když	k8xS	když
k	k	k7c3	k
výkonu	výkon	k1gInSc3	výkon
soudcovské	soudcovský	k2eAgFnSc2d1	soudcovská
funkce	funkce	k1gFnSc2	funkce
zasedá	zasedat	k5eAaImIp3nS	zasedat
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
i	i	k9	i
bohyně	bohyně	k1gFnSc2	bohyně
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
Diké	Diká	k1gFnSc2	Diká
(	(	kIx(	(
<g/>
bývá	bývat	k5eAaImIp3nS	bývat
uváděna	uvádět	k5eAaImNgFnS	uvádět
jako	jako	k9	jako
dcera	dcera	k1gFnSc1	dcera
Themidina	Themidin	k2eAgFnSc1d1	Themidin
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
vzájemně	vzájemně	k6eAd1	vzájemně
zaměňovány	zaměňován	k2eAgInPc1d1	zaměňován
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
uváděna	uvádět	k5eAaImNgFnS	uvádět
i	i	k9	i
jako	jako	k8xS	jako
matka	matka	k1gFnSc1	matka
matka	matka	k1gFnSc1	matka
Epimétheova	Epimétheův	k2eAgFnSc1d1	Epimétheův
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
zdroje	zdroj	k1gInPc1	zdroj
označují	označovat	k5eAaImIp3nP	označovat
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
matku	matka	k1gFnSc4	matka
Klymené	Klymený	k2eAgNnSc1d1	Klymený
<g/>
.	.	kIx.	.
</s>
<s>
Otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
Titán	Titán	k1gMnSc1	Titán
Íapetos	Íapetos	k1gMnSc1	Íapetos
<g/>
.	.	kIx.	.
</s>
<s>
Epimétheus	Epimétheus	k1gMnSc1	Epimétheus
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgMnS	stát
manželem	manžel	k1gMnSc7	manžel
Pandóry	Pandóra	k1gFnSc2	Pandóra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přinesla	přinést	k5eAaPmAgFnS	přinést
lidem	člověk	k1gMnPc3	člověk
dary	dar	k1gInPc4	dar
od	od	k7c2	od
bohů	bůh	k1gMnPc2	bůh
v	v	k7c6	v
Pandořině	Pandořin	k2eAgFnSc6d1	Pandořina
skříňce	skříňka	k1gFnSc6	skříňka
<g/>
,	,	kIx,	,
plné	plný	k2eAgNnSc4d1	plné
trápení	trápení	k1gNnSc4	trápení
<g/>
,	,	kIx,	,
běd	běda	k1gFnPc2	běda
a	a	k8xC	a
neštěstí	neštěstí	k1gNnSc2	neštěstí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Themis	Themis	k1gFnSc1	Themis
je	být	k5eAaImIp3nS	být
ztělesněním	ztělesnění	k1gNnSc7	ztělesnění
božského	božský	k2eAgInSc2d1	božský
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
zákona	zákon	k1gInSc2	zákon
a	a	k8xC	a
zvyku	zvyk	k1gInSc2	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
Themis	Themis	k1gFnSc1	Themis
přehlížena	přehlížen	k2eAgFnSc1d1	přehlížena
<g/>
,	,	kIx,	,
Nemesis	Nemesis	k1gFnSc1	Nemesis
přináší	přinášet	k5eAaImIp3nS	přinášet
jen	jen	k9	jen
spravedlivou	spravedlivý	k2eAgFnSc4d1	spravedlivá
a	a	k8xC	a
hněvivou	hněvivý	k2eAgFnSc4d1	hněvivá
odplatu	odplata	k1gFnSc4	odplata
<g/>
.	.	kIx.	.
</s>
<s>
Themis	Themis	k1gFnSc1	Themis
není	být	k5eNaImIp3nS	být
zlostná	zlostný	k2eAgFnSc1d1	zlostná
<g/>
:	:	kIx,	:
Byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
Héře	Héra	k1gFnSc3	Héra
<g/>
,	,	kIx,	,
rozrušené	rozrušený	k2eAgInPc1d1	rozrušený
Diovými	Diův	k2eAgFnPc7d1	Diova
hrozbami	hrozba	k1gFnPc7	hrozba
<g/>
,	,	kIx,	,
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
na	na	k7c4	na
Olymp	Olymp	k1gInSc4	Olymp
pohár	pohár	k1gInSc4	pohár
(	(	kIx(	(
<g/>
Illias	Illias	k1gInSc4	Illias
XV	XV	kA	XV
<g/>
.88	.88	k4	.88
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Themis	Themis	k1gFnSc1	Themis
předsedá	předsedat	k5eAaImIp3nS	předsedat
řádnému	řádný	k2eAgInSc3d1	řádný
vztahu	vztah	k1gInSc3	vztah
mezi	mezi	k7c7	mezi
mužem	muž	k1gMnSc7	muž
a	a	k8xC	a
ženou	žena	k1gFnSc7	žena
<g/>
,	,	kIx,	,
základu	základ	k1gInSc2	základ
správně	správně	k6eAd1	správně
zřízené	zřízený	k2eAgFnPc1d1	zřízená
rodiny	rodina	k1gFnPc1	rodina
(	(	kIx(	(
<g/>
rodina	rodina	k1gFnSc1	rodina
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
pilíř	pilíř	k1gInSc4	pilíř
výchovy	výchova	k1gFnSc2	výchova
<g/>
)	)	kIx)	)
a	a	k8xC	a
soudcové	soudce	k1gMnPc1	soudce
byli	být	k5eAaImAgMnP	být
často	často	k6eAd1	často
nazýváni	nazýván	k2eAgMnPc1d1	nazýván
"	"	kIx"	"
<g/>
themistopoloi	themistopolo	k1gMnPc1	themistopolo
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Themidini	Themidin	k2eAgMnPc1d1	Themidin
služebníci	služebník	k1gMnPc1	služebník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hérou	Héra	k1gFnSc7	Héra
byla	být	k5eAaImAgFnS	být
oslovována	oslovovat	k5eAaImNgFnS	oslovovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
paní	paní	k1gFnSc1	paní
Themis	Themis	k1gFnSc1	Themis
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
Themis	Themis	k1gFnSc1	Themis
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
také	také	k9	také
náhradou	náhrada	k1gFnSc7	náhrada
za	za	k7c4	za
Adrasteiu	Adrasteius	k1gMnSc3	Adrasteius
ve	v	k7c4	v
vyprávění	vyprávění	k1gNnSc4	vyprávění
o	o	k7c6	o
narození	narození	k1gNnSc6	narození
Dia	Dia	k1gFnSc2	Dia
na	na	k7c6	na
Krétě	Kréta	k1gFnSc6	Kréta
<g/>
.	.	kIx.	.
</s>
<s>
Zřídila	zřídit	k5eAaPmAgFnS	zřídit
věštírnu	věštírna	k1gFnSc4	věštírna
v	v	k7c6	v
Delfách	Delfy	k1gFnPc6	Delfy
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zde	zde	k6eAd1	zde
orákulem	orákul	k1gInSc7	orákul
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiné	jiný	k2eAgFnSc2d1	jiná
z	z	k7c2	z
legend	legenda	k1gFnPc2	legenda
přijala	přijmout	k5eAaPmAgFnS	přijmout
Themis	Themis	k1gFnSc1	Themis
věštírnu	věštírna	k1gFnSc4	věštírna
v	v	k7c6	v
Delfách	Delfy	k1gFnPc6	Delfy
od	od	k7c2	od
Gaii	Gai	k1gFnSc2	Gai
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
ji	on	k3xPp3gFnSc4	on
předala	předat	k5eAaPmAgFnS	předat
Foibé	Foibý	k2eAgFnPc1d1	Foibý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Themis	Themis	k1gFnSc1	Themis
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
ztotožňována	ztotožňován	k2eAgFnSc1d1	ztotožňována
s	s	k7c7	s
Physis	Physis	k1gFnSc7	Physis
(	(	kIx(	(
<g/>
personifikace	personifikace	k1gFnSc1	personifikace
přírody	příroda	k1gFnSc2	příroda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Římským	římský	k2eAgInSc7d1	římský
ekvivalentem	ekvivalent	k1gInSc7	ekvivalent
jednoho	jeden	k4xCgInSc2	jeden
aspektu	aspekt	k1gInSc2	aspekt
řecké	řecký	k2eAgFnSc2d1	řecká
Themis	Themis	k1gFnSc1	Themis
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
zosobnění	zosobnění	k1gNnSc1	zosobnění
božského	božský	k2eAgNnSc2d1	božské
zákonného	zákonný	k2eAgNnSc2d1	zákonné
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Iustitia	Iustitia	k1gFnSc1	Iustitia
(	(	kIx(	(
<g/>
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
Justitia	Justitia	k1gFnSc1	Justitia
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
zobrazována	zobrazován	k2eAgFnSc1d1	zobrazována
jako	jako	k8xC	jako
žena	žena	k1gFnSc1	žena
bez	bez	k7c2	bez
citu	cit	k1gInSc2	cit
<g/>
,	,	kIx,	,
se	s	k7c7	s
zavázanýma	zavázaný	k2eAgNnPc7d1	zavázané
očima	oko	k1gNnPc7	oko
a	a	k8xC	a
držící	držící	k2eAgFnSc4d1	držící
váhy	váha	k1gFnPc4	váha
a	a	k8xC	a
roh	roh	k1gInSc4	roh
hojnosti	hojnost	k1gFnSc2	hojnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Manželé	manžel	k1gMnPc1	manžel
<g/>
/	/	kIx~	/
<g/>
Děti	dítě	k1gFnPc1	dítě
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
Diem	Die	k1gNnSc7	Die
</s>
</p>
<p>
<s>
Hóry	Hóra	k1gFnPc1	Hóra
<g/>
:	:	kIx,	:
bohyně	bohyně	k1gFnSc1	bohyně
přírodního	přírodní	k2eAgInSc2d1	přírodní
a	a	k8xC	a
společenského	společenský	k2eAgInSc2d1	společenský
řádu	řád	k1gInSc2	řád
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
generace	generace	k1gFnSc1	generace
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
známá	známý	k2eAgNnPc1d1	známé
i	i	k8xC	i
jiná	jiný	k2eAgNnPc1d1	jiné
jména	jméno	k1gNnPc1	jméno
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Auxó	Auxó	k?	Auxó
(	(	kIx(	(
<g/>
bohyně	bohyně	k1gFnSc1	bohyně
růstu	růst	k1gInSc2	růst
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Karpó	Karpó	k?	Karpó
(	(	kIx(	(
<g/>
bohyně	bohyně	k1gFnSc1	bohyně
plodů	plod	k1gInPc2	plod
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Thalló	Thalló	k?	Thalló
(	(	kIx(	(
<g/>
bohyně	bohyně	k1gFnSc1	bohyně
květu	květ	k1gInSc2	květ
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
generace	generace	k1gFnSc1	generace
</s>
</p>
<p>
<s>
Diké	Diké	k1gNnSc1	Diké
(	(	kIx(	(
<g/>
bohyně	bohyně	k1gFnSc1	bohyně
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
Astraea	Astraea	k1gFnSc1	Astraea
v	v	k7c6	v
římském	římský	k2eAgNnSc6d1	římské
náboženství	náboženství	k1gNnSc6	náboženství
<g/>
,	,	kIx,	,
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Panny	Panna	k1gFnSc2	Panna
</s>
</p>
<p>
<s>
Éiréné	Éiréné	k1gNnSc1	Éiréné
(	(	kIx(	(
<g/>
bohyně	bohyně	k1gFnSc1	bohyně
míru	mír	k1gInSc2	mír
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Eunomia	Eunomia	k1gFnSc1	Eunomia
(	(	kIx(	(
<g/>
bohyně	bohyně	k1gFnSc1	bohyně
zákonnosti	zákonnost	k1gFnSc2	zákonnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Moiry	Moir	k1gInPc1	Moir
<g/>
:	:	kIx,	:
bohyně	bohyně	k1gFnSc1	bohyně
osudu	osud	k1gInSc2	osud
</s>
</p>
<p>
<s>
Atropos	Atropos	k1gInSc1	Atropos
(	(	kIx(	(
<g/>
přestřihává	přestřihávat	k5eAaImIp3nS	přestřihávat
nit	nit	k1gFnSc4	nit
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Klóthó	Klóthó	k?	Klóthó
(	(	kIx(	(
<g/>
rozpřádá	rozpřádat	k5eAaImIp3nS	rozpřádat
nit	nit	k1gFnSc4	nit
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lachesis	Lachesis	k1gFnSc1	Lachesis
(	(	kIx(	(
<g/>
rozvíjení	rozvíjení	k1gNnSc1	rozvíjení
nitě	nit	k1gFnSc2	nit
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Slovník	slovník	k1gInSc1	slovník
antické	antický	k2eAgFnSc2d1	antická
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
nakl	naknout	k5eAaPmAgMnS	naknout
<g/>
.	.	kIx.	.
</s>
<s>
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Gerhard	Gerhard	k1gMnSc1	Gerhard
Löwe	Löw	k1gInSc2	Löw
<g/>
,	,	kIx,	,
Heindrich	Heindrich	k1gMnSc1	Heindrich
Alexander	Alexandra	k1gFnPc2	Alexandra
Stoll	Stoll	k1gMnSc1	Stoll
<g/>
,	,	kIx,	,
ABC	ABC	kA	ABC
Antiky	antika	k1gFnPc1	antika
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Graves	Graves	k1gMnSc1	Graves
<g/>
.	.	kIx.	.
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
–	–	k?	–
Řecké	řecký	k2eAgInPc1d1	řecký
mýty	mýtus	k1gInPc1	mýtus
–	–	k?	–
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
Publius	Publius	k1gMnSc1	Publius
Ovidius	Ovidius	k1gMnSc1	Ovidius
Naso	Naso	k1gMnSc1	Naso
<g/>
,	,	kIx,	,
Proměny	proměna	k1gFnPc1	proměna
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Mertlík	Mertlík	k1gInSc1	Mertlík
<g/>
,	,	kIx,	,
Starověké	starověký	k2eAgFnPc1d1	starověká
báje	báj	k1gFnPc1	báj
a	a	k8xC	a
pověsti	pověst	k1gFnPc1	pověst
</s>
</p>
<p>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Zamarovský	Zamarovský	k2eAgMnSc1d1	Zamarovský
<g/>
.	.	kIx.	.
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
–	–	k?	–
Bohové	bůh	k1gMnPc1	bůh
a	a	k8xC	a
hrdinové	hrdina	k1gMnPc1	hrdina
antických	antický	k2eAgFnPc2d1	antická
bájí	báj	k1gFnPc2	báj
–	–	k?	–
BRÁNA	brán	k2eAgFnSc1d1	brána
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7243-266-4	[number]	k4	80-7243-266-4
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Themis	Themis	k1gFnSc1	Themis
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Výtahy	výtah	k1gInPc1	výtah
z	z	k7c2	z
originálních	originální	k2eAgInPc2d1	originální
řeckých	řecký	k2eAgInPc2d1	řecký
pramenů	pramen	k1gInPc2	pramen
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
