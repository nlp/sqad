<s>
Kozoroh	Kozoroh	k1gMnSc1	Kozoroh
je	být	k5eAaImIp3nS	být
desáté	desátý	k4xOgNnSc1	desátý
astrologické	astrologický	k2eAgNnSc1d1	astrologické
znamení	znamení	k1gNnSc1	znamení
zvěrokruhu	zvěrokruh	k1gInSc2	zvěrokruh
mající	mající	k2eAgInSc4d1	mající
původ	původ	k1gInSc4	původ
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Kozoroha	Kozoroh	k1gMnSc2	Kozoroh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
astrologii	astrologie	k1gFnSc6	astrologie
je	být	k5eAaImIp3nS	být
Kozoroh	Kozoroh	k1gMnSc1	Kozoroh
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
negativní	negativní	k2eAgNnSc4d1	negativní
(	(	kIx(	(
<g/>
introvertní	introvertní	k2eAgNnSc4d1	introvertní
<g/>
)	)	kIx)	)
znamení	znamení	k1gNnSc4	znamení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
zemské	zemský	k2eAgNnSc4d1	zemské
znamení	znamení	k1gNnSc4	znamení
<g/>
.	.	kIx.	.
</s>
<s>
Kozoroh	Kozoroh	k1gMnSc1	Kozoroh
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
ovládán	ovládat	k5eAaImNgInS	ovládat
planetou	planeta	k1gFnSc7	planeta
Saturn	Saturn	k1gInSc1	Saturn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
astrologii	astrologie	k1gFnSc6	astrologie
je	být	k5eAaImIp3nS	být
Slunce	slunce	k1gNnSc1	slunce
v	v	k7c6	v
konstelaci	konstelace	k1gFnSc6	konstelace
Kozoroha	Kozoroh	k1gMnSc2	Kozoroh
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sideralistické	sideralistický	k2eAgFnSc6d1	sideralistický
astrologii	astrologie	k1gFnSc6	astrologie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Capricorn	Capricorna	k1gFnPc2	Capricorna
(	(	kIx(	(
<g/>
astrology	astrolog	k1gMnPc7	astrolog
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
