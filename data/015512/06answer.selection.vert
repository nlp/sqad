<s>
Ředitelství	ředitelství	k1gNnSc1
opevňovacích	opevňovací	k2eAgFnPc2d1
prací	práce	k1gFnPc2
(	(	kIx(
<g/>
ŘOP	ŘOP	kA
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
zřízeno	zřídit	k5eAaPmNgNnS
jako	jako	k8xS,k8xC
řídicí	řídicí	k2eAgInSc1d1
výkonný	výkonný	k2eAgInSc1d1
orgán	orgán	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
měl	mít	k5eAaImAgInS
na	na	k7c6
starosti	starost	k1gFnSc6
vlastní	vlastní	k2eAgFnSc4d1
realizaci	realizace	k1gFnSc4
československého	československý	k2eAgNnSc2d1
opevnění	opevnění	k1gNnSc2
v	v	k7c6
terénu	terén	k1gInSc6
<g/>
.	.	kIx.
</s>