<s>
Ředitelství	ředitelství	k1gNnSc1
opevňovacích	opevňovací	k2eAgFnPc2d1
prací	práce	k1gFnPc2
</s>
<s>
Ředitelství	ředitelství	k1gNnSc1
opevňovacích	opevňovací	k2eAgFnPc2d1
prací	práce	k1gFnPc2
(	(	kIx(
<g/>
ŘOP	ŘOP	kA
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
zřízeno	zřídit	k5eAaPmNgNnS
jako	jako	k8xS,k8xC
řídicí	řídicí	k2eAgInSc1d1
výkonný	výkonný	k2eAgInSc1d1
orgán	orgán	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
měl	mít	k5eAaImAgInS
na	na	k7c6
starosti	starost	k1gFnSc6
vlastní	vlastní	k2eAgFnSc4d1
realizaci	realizace	k1gFnSc4
československého	československý	k2eAgNnSc2d1
opevnění	opevnění	k1gNnSc2
v	v	k7c6
terénu	terén	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
čele	čelo	k1gNnSc6
tohoto	tento	k3xDgInSc2
orgánu	orgán	k1gInSc2
stanul	stanout	k5eAaPmAgInS
jako	jako	k9
I.	I.	kA
zástupce	zástupce	k1gMnSc1
náčelníka	náčelník	k1gMnSc2
hlavního	hlavní	k2eAgInSc2d1
štábu	štáb	k1gInSc2
tehdejší	tehdejší	k2eAgMnPc1d1
brig	briga	k1gFnPc2
<g/>
.	.	kIx.
gen.	gen.	kA
Karel	Karel	k1gMnSc1
Husárek	husárek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
ŘOP	ŘOP	kA
se	se	k3xPyFc4
také	také	k9
zřídila	zřídit	k5eAaPmAgFnS
rada	rada	k1gFnSc1
pro	pro	k7c4
opevňování	opevňování	k1gNnSc4
(	(	kIx(
<g/>
RO	RO	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
orgány	orgán	k1gInPc1
byly	být	k5eAaImAgInP
založeny	založit	k5eAaPmNgInP
současně	současně	k6eAd1
20	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1935	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rozpuštění	rozpuštění	k1gNnSc1
ŘOP	ŘOP	kA
1938	#num#	k4
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
Ředitelství	ředitelství	k1gNnSc2
opevňovacích	opevňovací	k2eAgFnPc2d1
prací	práce	k1gFnPc2
v	v	k7c6
září	září	k1gNnSc6
1938	#num#	k4
</s>
<s>
ředitel	ředitel	k1gMnSc1
opevňovacích	opevňovací	k2eAgFnPc2d1
prací	práce	k1gFnPc2
–	–	k?
div	div	k1gMnSc1
<g/>
.	.	kIx.
gen.	gen.	kA
Karel	Karel	k1gMnSc1
Husárek	husárek	k1gMnSc1
</s>
<s>
I.	I.	kA
oddělení	oddělení	k1gNnPc2
–	–	k?
taktické	taktický	k2eAgFnSc2d1
</s>
<s>
přednosta	přednosta	k1gMnSc1
–	–	k?
plk.	plk.	kA
<g/>
gšt	gšt	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ladislav	Ladislav	k1gMnSc1
Tomsa	Tomsa	k1gFnSc1
<g/>
,	,	kIx,
plk.	plk.	kA
gšt	gšt	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hynek	Hynek	k1gMnSc1
Štěpánský	štěpánský	k2eAgInSc4d1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
:	:	kIx,
takticko-studijní	takticko-studijní	k2eAgFnSc1d1
–	–	k?
mjr.	mjr.	kA
gšt	gšt	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karel	Karel	k1gMnSc1
Lukas	Lukas	k1gMnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
:	:	kIx,
organizační	organizační	k2eAgFnSc1d1
–	–	k?
mjr.	mjr.	kA
gšt	gšt	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	František	k1gMnSc1
Podroužek	Podroužek	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
:	:	kIx,
pěchotní	pěchotní	k2eAgFnSc1d1
–	–	k?
plk.	plk.	kA
pěch	pěch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaroslav	Jaroslav	k1gMnSc1
Malec	Malec	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
:	:	kIx,
dělostřelecká	dělostřelecký	k2eAgFnSc1d1
–	–	k?
plk.	plk.	kA
gšt	gšt	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bruno	Bruno	k1gMnSc1
Sklenovský	Sklenovský	k2eAgMnSc1d1
<g/>
,	,	kIx,
plk.	plk.	kA
<g/>
gšt	gšt	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Churavý	churavý	k2eAgInSc4d1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
:	:	kIx,
zpravodajská	zpravodajský	k2eAgFnSc1d1
–	–	k?
pplk.	pplk.	kA
pěch	pěch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	František	k1gMnSc1
Hieke	Hiek	k1gInSc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
:	:	kIx,
právní	právní	k2eAgFnSc1d1
–	–	k?
mjr.	mjr.	kA
just	just	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
JUDr.	JUDr.	kA
Jan	Jan	k1gMnSc1
Hrdlička	Hrdlička	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
:	:	kIx,
intendanční	intendanční	k2eAgFnSc1d1
–	–	k?
škpt.	škpt.	k?
int	int	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Václav	Václav	k1gMnSc1
Tichý	Tichý	k1gMnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
<g/>
:	:	kIx,
osobní	osobní	k2eAgFnSc1d1
–	–	k?
škpt.	škpt.	k?
konc	konc	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
Kárl	Kárl	k1gMnSc1
</s>
<s>
II	II	kA
<g/>
.	.	kIx.
oddělení	oddělení	k1gNnSc1
–	–	k?
technické	technický	k2eAgFnSc2d1
</s>
<s>
přednosta	přednosta	k1gMnSc1
–	–	k?
brig	briga	k1gFnPc2
<g/>
.	.	kIx.
gen.	gen.	kA
Ing.	ing.	kA
Karel	Karel	k1gMnSc1
Štěpánek	Štěpánek	k1gMnSc1
</s>
<s>
pobočník	pobočník	k1gMnSc1
–	–	k?
mjr.	mjr.	kA
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hugo	Hugo	k1gMnSc1
Svoboda	Svoboda	k1gMnSc1
</s>
<s>
II	II	kA
<g/>
.	.	kIx.
<g/>
a	a	k8xC
skupina	skupina	k1gFnSc1
–	–	k?
studijní	studijní	k2eAgNnSc4d1
a	a	k8xC
konstruktivní	konstruktivní	k2eAgNnSc4d1
</s>
<s>
přednosta	přednosta	k1gMnSc1
–	–	k?
brig	briga	k1gFnPc2
<g/>
.	.	kIx.
gen.	gen.	kA
Ing.	ing.	kA
Jan	Jan	k1gMnSc1
Čermák	Čermák	k1gMnSc1
</s>
<s>
pobočník	pobočník	k1gMnSc1
–	–	k?
škpt.	škpt.	k?
konc	konc	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eduard	Eduard	k1gMnSc1
Březina	Březina	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
podskupina	podskupina	k1gFnSc1
<g/>
:	:	kIx,
konstrukční	konstrukční	k2eAgFnSc1d1
–	–	k?
pplk.	pplk.	kA
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Kučera	Kučera	k1gMnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
podskupina	podskupina	k1gFnSc1
<g/>
:	:	kIx,
pancéřová	pancéřový	k2eAgFnSc1d1
–	–	k?
škpt.	škpt.	k?
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ing.	ing.	kA
Rudolf	Rudolf	k1gMnSc1
Tomiška	Tomišek	k1gMnSc2
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
podskupina	podskupina	k1gFnSc1
<g/>
:	:	kIx,
ventilační	ventilační	k2eAgFnSc1d1
–	–	k?
mjr.	mjr.	kA
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Václav	Václav	k1gMnSc1
Donát	Donát	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
podskupina	podskupina	k1gFnSc1
<g/>
:	:	kIx,
strojní	strojní	k2eAgFnSc1d1
–	–	k?
pplk.	pplk.	kA
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ing.	ing.	kA
Karel	Karel	k1gMnSc1
Boháč	Boháč	k1gMnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
podskupina	podskupina	k1gFnSc1
<g/>
:	:	kIx,
dopravní	dopravní	k2eAgFnSc1d1
–	–	k?
mjr.	mjr.	kA
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Miloslav	Miloslav	k1gMnSc1
Kašpar	Kašpar	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
podskupina	podskupina	k1gFnSc1
<g/>
:	:	kIx,
spojovací	spojovací	k2eAgFnSc1d1
–	–	k?
pplk.	pplk.	kA
tel	tel	kA
<g/>
.	.	kIx.
Alois	Alois	k1gMnSc1
Weyszer	Weyszer	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
podskupina	podskupina	k1gFnSc1
<g/>
:	:	kIx,
tvrze	tvrz	k1gFnSc2
–	–	k?
kpt.	kpt.	k?
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ing.	ing.	kA
Alois	Alois	k1gMnSc1
Staňa	Staňa	k?
</s>
<s>
II	II	kA
<g/>
.	.	kIx.
<g/>
b	b	k?
skupina	skupina	k1gFnSc1
–	–	k?
stavebně-administrativní	stavebně-administrativní	k2eAgFnSc7d1
</s>
<s>
přednosta	přednosta	k1gMnSc1
–	–	k?
plk.	plk.	kA
stav	stav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ing.	ing.	kA
Josef	Josef	k1gMnSc1
Hubálek	Hubálek	k1gMnSc1
</s>
<s>
pobočník	pobočník	k1gMnSc1
–	–	k?
škpt.	škpt.	k?
konc	konc	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaroslav	Jaroslav	k1gMnSc1
Beneš	Beneš	k1gMnSc1
(	(	kIx(
<g/>
voják	voják	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
podskupina	podskupina	k1gFnSc1
<g/>
:	:	kIx,
stavební	stavební	k2eAgFnSc1d1
–	–	k?
pplk.	pplk.	kA
stav	stav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ing.	ing.	kA
Alexander	Alexandra	k1gFnPc2
Stolle	Stolle	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
podskupina	podskupina	k1gFnSc1
<g/>
:	:	kIx,
rozpočtová	rozpočtový	k2eAgFnSc1d1
–	–	k?
škpt.	škpt.	k?
stav	stav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karel	Karel	k1gMnSc1
Šimonek	Šimonek	k1gInSc4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
podskupina	podskupina	k1gFnSc1
<g/>
:	:	kIx,
materiální	materiální	k2eAgFnSc1d1
–	–	k?
pplk.	pplk.	kA
stav	stav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ing.	ing.	kA
Vladimír	Vladimír	k1gMnSc1
Rozmara	rozmara	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
podskupina	podskupina	k1gFnSc1
<g/>
:	:	kIx,
vnitřního	vnitřní	k2eAgNnSc2d1
zařízení	zařízení	k1gNnSc2
–	–	k?
mjr.	mjr.	kA
stav	stav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
František	František	k1gMnSc1
Kašík	Kašík	k1gMnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
podskupina	podskupina	k1gFnSc1
<g/>
:	:	kIx,
strojní	strojní	k2eAgNnSc1d1
–	–	k?
kpt.	kpt.	k?
stav	stav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ing.	ing.	kA
Robert	Robert	k1gMnSc1
Křivan	Křivan	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
podskupina	podskupina	k1gFnSc1
<g/>
:	:	kIx,
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
–	–	k?
kpt.	kpt.	k?
stav	stav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ing.	ing.	kA
Bohumil	Bohumila	k1gFnPc2
Bohun	Bohuna	k1gFnPc2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
podskupina	podskupina	k1gFnSc1
<g/>
:	:	kIx,
zeměměřická	zeměměřický	k2eAgFnSc1d1
–	–	k?
škpt.	škpt.	k?
stav	stav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ing.	ing.	kA
Jaroslav	Jaroslav	k1gMnSc1
Payer	Payer	k1gMnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
podskupina	podskupina	k1gFnSc1
<g/>
:	:	kIx,
rýsovny	rýsovna	k1gFnSc2
–	–	k?
mjr.	mjr.	kA
stav	stav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ing.	ing.	kA
Josef	Josef	k1gMnSc1
Němec	Němec	k1gMnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
podskupina	podskupina	k1gFnSc1
<g/>
:	:	kIx,
technicky-administrativní	technicky-administrativní	k2eAgFnSc1d1
–	–	k?
škpt.	škpt.	k?
stav	stav	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karel	Karel	k1gMnSc1
Jeřábek	Jeřábek	k1gMnSc1
</s>
<s>
Pomocná	pomocný	k2eAgFnSc1d1
kancelář	kancelář	k1gFnSc1
</s>
<s>
vedoucí	vedoucí	k1gMnSc1
–	–	k?
npor.	npor.	kA
kanc	kanc	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Jaška	Jaška	k1gMnSc1
</s>
<s>
protokol	protokol	k1gInSc1
–	–	k?
šrtm	šrtm	k1gInSc1
<g/>
.	.	kIx.
kanc	kanc	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Peterka	Peterka	k1gMnSc1
</s>
<s>
podatelna	podatelna	k1gFnSc1
a	a	k8xC
výpravna	výpravna	k1gFnSc1
–	–	k?
por.	por.	k?
kanc	kanc	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojtěch	Vojtěch	k1gMnSc1
Stránský	Stránský	k1gMnSc1
</s>
<s>
spisovna	spisovna	k1gFnSc1
–	–	k?
por.	por.	k?
kanc	kanc	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Josef	Josef	k1gMnSc1
Janoušek	Janoušek	k1gMnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Eduard	Eduard	k1gMnSc1
Stehlík	Stehlík	k1gMnSc1
<g/>
:	:	kIx,
Lexikon	lexikon	k1gInSc1
těžkých	těžký	k2eAgInPc2d1
objektů	objekt	k1gInPc2
č.	č.	k?
<g/>
s.	s.	k?
opevnění	opevnění	k1gNnSc2
z	z	k7c2
let	léto	k1gNnPc2
1935	#num#	k4
-	-	kIx~
1939	#num#	k4
<g/>
,	,	kIx,
FORTprint	FORTprint	k1gInSc1
<g/>
,	,	kIx,
2001	#num#	k4
1	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-86011-13-5	80-86011-13-5	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Československé	československý	k2eAgNnSc1d1
opevnění	opevnění	k1gNnSc1
</s>
