<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
významný	významný	k2eAgMnSc1d1	významný
český	český	k2eAgMnSc1d1	český
náboženský	náboženský	k2eAgMnSc1d1	náboženský
myslitel	myslitel	k1gMnSc1	myslitel
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
upálen	upálit	k5eAaPmNgMnS	upálit
roku	rok	k1gInSc2	rok
1415	[number]	k4	1415
v	v	k7c6	v
Kostnici	Kostnice	k1gFnSc6	Kostnice
<g/>
?	?	kIx.	?
</s>
