<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
jsou	být	k5eAaImIp3nP
přesměrována	přesměrován	k2eAgNnPc1d1
hesla	heslo	k1gNnPc1
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
a	a	k8xC
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránkách	stránka	k1gFnPc6
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Usa	Usa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
United	United	k1gInSc4
States	States	k1gMnSc1
of	of	k?
America	America	k1gMnSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
HymnaThe	HymnaThe	k6eAd1
Star-Spangled	Star-Spangled	k1gInSc1
Banner	banner	k1gInSc1
</s>
<s>
MottoIn	MottoIn	k1gMnSc1
God	God	k1gMnSc1
We	We	k1gMnSc1
Trust	trust	k1gInSc4
Geografie	geografie	k1gFnSc2
</s>
<s>
Poloha	poloha	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
D.C.	D.C.	k1gFnSc1
Rozloha	rozloha	k1gFnSc1
</s>
<s>
9	#num#	k4
833	#num#	k4
517	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
km²	km²	k?
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
)	)	kIx)
z	z	k7c2
toho	ten	k3xDgNnSc2
7	#num#	k4
%	%	kIx~
vodní	vodní	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
Nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
</s>
<s>
Denali	Denat	k5eAaPmAgMnP,k5eAaBmAgMnP,k5eAaImAgMnP
(	(	kIx(
<g/>
6	#num#	k4
194	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
od	od	k7c2
UTC-5	UTC-5	k1gFnSc2
do	do	k7c2
UTC-10	UTC-10	k1gFnSc2
Poloha	poloha	k1gFnSc1
</s>
<s>
39	#num#	k4
<g/>
°	°	k?
<g/>
49	#num#	k4
<g/>
′	′	k?
<g/>
41	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
98	#num#	k4
<g/>
°	°	k?
<g/>
34	#num#	k4
<g/>
′	′	k?
<g/>
46	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
Obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
327	#num#	k4
652	#num#	k4
694	#num#	k4
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
listopad	listopad	k1gInSc1
2018	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
35	#num#	k4
ob.	ob.	k?
/	/	kIx~
km²	km²	k?
(	(	kIx(
<g/>
180	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
)	)	kIx)
HDI	HDI	kA
</s>
<s>
▲	▲	k?
0,915	0,915	k4
(	(	kIx(
<g/>
velmi	velmi	k6eAd1
vysoký	vysoký	k2eAgInSc1d1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
8	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
)	)	kIx)
Jazyk	jazyk	k1gInSc1
</s>
<s>
formálně	formálně	k6eAd1
není	být	k5eNaImIp3nS
<g/>
,	,	kIx,
de	de	k?
facto	facto	k1gNnSc4
angličtinadále	angličtinadále	k6eAd1
především	především	k6eAd1
<g/>
:	:	kIx,
<g/>
španělština	španělština	k1gFnSc1
<g/>
,	,	kIx,
u	u	k7c2
původního	původní	k2eAgNnSc2d1
obyvatelstva	obyvatelstvo	k1gNnSc2
indiánské	indiánský	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
<g/>
,	,	kIx,
u	u	k7c2
větších	veliký	k2eAgFnPc2d2
menšin	menšina	k1gFnPc2
místně	místně	k6eAd1
i	i	k9
jiné	jiný	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náboženství	náboženství	k1gNnSc1
</s>
<s>
křesťanské	křesťanský	k2eAgFnPc1d1
protestantské	protestantský	k2eAgFnPc1d1
a	a	k8xC
evangelikální	evangelikální	k2eAgInPc1d1
směry	směr	k1gInPc1
(	(	kIx(
<g/>
celkem	celkem	k6eAd1
54	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
katolická	katolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
(	(	kIx(
<g/>
24	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mormoni	mormon	k1gMnPc1
(	(	kIx(
<g/>
5	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
judaismus	judaismus	k1gInSc1
(	(	kIx(
<g/>
1,6	1,6	k4
%	%	kIx~
<g/>
)	)	kIx)
Státní	státní	k2eAgInSc1d1
útvar	útvar	k1gInSc1
Státní	státní	k2eAgInSc1d1
zřízení	zřízení	k1gNnSc4
</s>
<s>
federativní	federativní	k2eAgFnSc1d1
prezidentská	prezidentský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Vznik	vznik	k1gInSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1776	#num#	k4
(	(	kIx(
<g/>
vyhlášení	vyhlášení	k1gNnSc4
nezávislosti	nezávislost	k1gFnSc2
na	na	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
)	)	kIx)
Prezident	prezident	k1gMnSc1
</s>
<s>
Joe	Joe	k?
Biden	Biden	k1gInSc1
Viceprezidentka	viceprezidentka	k1gFnSc1
</s>
<s>
Kamala	Kamala	k1gFnSc1
Harrisová	Harrisový	k2eAgFnSc1d1
Měna	měna	k1gFnSc1
</s>
<s>
americký	americký	k2eAgInSc1d1
dolar	dolar	k1gInSc1
(	(	kIx(
<g/>
USD	USD	kA
<g/>
)	)	kIx)
HDP	HDP	kA
<g/>
/	/	kIx~
<g/>
obyv	obyv	k1gMnSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
PPP	PPP	kA
<g/>
)	)	kIx)
</s>
<s>
59	#num#	k4
178	#num#	k4
USD	USD	kA
(	(	kIx(
<g/>
13	#num#	k4
<g/>
.	.	kIx.
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
listopad	listopad	k1gInSc1
2018	#num#	k4
<g/>
)	)	kIx)
Mezinárodní	mezinárodní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
ISO	ISO	kA
3166-1	3166-1	k4
</s>
<s>
840	#num#	k4
USA	USA	kA
US	US	kA
MPZ	MPZ	kA
</s>
<s>
USA	USA	kA
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
+1	+1	k4
Národní	národní	k2eAgInSc1d1
TLD	TLD	kA
</s>
<s>
<g/>
us	us	k?
<g/>
,	,	kIx,
.	.	kIx.
<g/>
gov	gov	k?
<g/>
,	,	kIx,
.	.	kIx.
<g/>
edu	edu	k?
<g/>
,	,	kIx,
.	.	kIx.
<g/>
mil	míle	k1gFnPc2
<g/>
,	,	kIx,
.	.	kIx.
<g/>
um	um	k1gInSc4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Appalačské	Appalačský	k2eAgNnSc4d1
pohoří	pohoří	k1gNnSc4
ve	v	k7c6
státě	stát	k1gInSc6
Tennessee	Tennesse	k1gFnSc2
</s>
<s>
Sonorská	Sonorský	k2eAgFnSc1d1
poušť	poušť	k1gFnSc1
ve	v	k7c6
státě	stát	k1gInSc6
Arizona	Arizona	k1gFnSc1
</s>
<s>
Colorado	Colorado	k1gNnSc1
Springs	Springsa	k1gFnPc2
ve	v	k7c6
státě	stát	k1gInSc6
Colorado	Colorado	k1gNnSc1
</s>
<s>
Miami	Miami	k1gNnSc1
na	na	k7c6
Floridě	Florida	k1gFnSc6
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
United	United	k1gMnSc1
States	States	k1gMnSc1
of	of	k?
America	America	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zkráceným	zkrácený	k2eAgInSc7d1
názvem	název	k1gInSc7
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
United	United	k1gMnSc1
States	States	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zkratkou	zkratka	k1gFnSc7
USA	USA	kA
(	(	kIx(
<g/>
nebo	nebo	k8xC
také	také	k9
US	US	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
demokratická	demokratický	k2eAgFnSc1d1
federativní	federativní	k2eAgFnSc1d1
prezidentská	prezidentský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
,	,	kIx,
rozkládající	rozkládající	k2eAgMnSc1d1
se	se	k3xPyFc4
mezi	mezi	k7c7
Atlantským	atlantský	k2eAgInSc7d1
oceánem	oceán	k1gInSc7
na	na	k7c6
východě	východ	k1gInSc6
a	a	k8xC
Tichým	tichý	k2eAgInSc7d1
oceánem	oceán	k1gInSc7
na	na	k7c6
západě	západ	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severu	sever	k1gInSc6
sousedí	sousedit	k5eAaImIp3nS
s	s	k7c7
Kanadou	Kanada	k1gFnSc7
a	a	k8xC
na	na	k7c6
jihu	jih	k1gInSc6
s	s	k7c7
Mexikem	Mexiko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
státu	stát	k1gInSc3
Aljaška	Aljaška	k1gFnSc1
sahá	sahat	k5eAaImIp3nS
území	území	k1gNnSc4
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
i	i	k8xC
k	k	k7c3
břehům	břeh	k1gInPc3
Severního	severní	k2eAgInSc2d1
ledového	ledový	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
(	(	kIx(
<g/>
Beringova	Beringův	k2eAgFnSc1d1
úžina	úžina	k1gFnSc1
je	on	k3xPp3gInPc4
dělí	dělit	k5eAaImIp3nS
od	od	k7c2
asijského	asijský	k2eAgNnSc2d1
území	území	k1gNnSc2
Ruska	Rusko	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zahrnuje	zahrnovat	k5eAaImIp3nS
také	také	k9
některé	některý	k3yIgInPc1
tichomořské	tichomořský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
zejména	zejména	k9
Havaj	Havaj	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
se	se	k3xPyFc4
skládají	skládat	k5eAaImIp3nP
z	z	k7c2
50	#num#	k4
států	stát	k1gInPc2
<g/>
,	,	kIx,
jednoho	jeden	k4xCgNnSc2
federálního	federální	k2eAgNnSc2d1
území	území	k1gNnSc2
s	s	k7c7
hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
a	a	k8xC
sídlem	sídlo	k1gNnSc7
prezidenta	prezident	k1gMnSc2
<g/>
,	,	kIx,
Kongresu	kongres	k1gInSc2
a	a	k8xC
Nejvyššího	vysoký	k2eAgInSc2d3
soudu	soud	k1gInSc2
(	(	kIx(
<g/>
District	District	k1gInSc1
of	of	k?
Columbia	Columbia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
šesti	šest	k4xCc2
závislých	závislý	k2eAgNnPc2d1
území	území	k1gNnPc2
(	(	kIx(
<g/>
Portoriko	Portoriko	k1gNnSc1
<g/>
,	,	kIx,
Severní	severní	k2eAgFnPc1d1
Mariany	Mariana	k1gFnPc1
<g/>
,	,	kIx,
Guam	Guam	k1gInSc1
<g/>
,	,	kIx,
Americké	americký	k2eAgInPc1d1
Panenské	panenský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
<g/>
,	,	kIx,
Americká	americký	k2eAgFnSc1d1
Samoa	Samoa	k1gFnSc1
a	a	k8xC
atol	atol	k1gInSc1
Palmyra	Palmyra	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
deseti	deset	k4xCc2
malých	malý	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
či	či	k8xC
útesů	útes	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
S	s	k7c7
rozlohou	rozloha	k1gFnSc7
9,8	9,8	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
milionu	milion	k4xCgInSc2
kilometrů	kilometr	k1gInPc2
čtverečních	čtvereční	k2eAgInPc2d1
jsou	být	k5eAaImIp3nP
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
třetí	třetí	k4xOgFnSc7
největší	veliký	k2eAgFnSc7d3
zemí	zem	k1gFnSc7
na	na	k7c6
světě	svět	k1gInSc6
(	(	kIx(
<g/>
po	po	k7c6
Rusku	Rusko	k1gNnSc6
a	a	k8xC
Kanadě	Kanada	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
S	s	k7c7
více	hodně	k6eAd2
než	než	k8xS
327	#num#	k4
miliony	milion	k4xCgInPc1
obyvatel	obyvatel	k1gMnPc2
jsou	být	k5eAaImIp3nP
třetí	třetí	k4xOgFnSc7
nejlidnatější	lidnatý	k2eAgFnSc7d3
zemí	zem	k1gFnSc7
planety	planeta	k1gFnSc2
(	(	kIx(
<g/>
po	po	k7c6
Číně	Čína	k1gFnSc6
a	a	k8xC
Indii	Indie	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Populace	populace	k1gFnSc1
je	být	k5eAaImIp3nS
tradičně	tradičně	k6eAd1
koncentrována	koncentrován	k2eAgFnSc1d1
na	na	k7c6
obou	dva	k4xCgNnPc6
pobřežích	pobřeží	k1gNnPc6
<g/>
,	,	kIx,
na	na	k7c6
východním	východní	k2eAgNnSc6d1
leží	ležet	k5eAaImIp3nS
největší	veliký	k2eAgNnSc1d3
město	město	k1gNnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
západním	západní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
druhé	druhý	k4xOgNnSc4
největší	veliký	k2eAgNnSc4d3
město	město	k1gNnSc4
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
je	být	k5eAaImIp3nS
Washington	Washington	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východní	východní	k2eAgFnSc1d1
část	část	k1gFnSc1
země	zem	k1gFnSc2
je	být	k5eAaImIp3nS
hustěji	husto	k6eAd2
osídlena	osídlen	k2eAgFnSc1d1
než	než	k8xS
západní	západní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
nominálního	nominální	k2eAgMnSc2d1
HDP	HDP	kA
jsou	být	k5eAaImIp3nP
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
největší	veliký	k2eAgInPc1d3
světovou	světový	k2eAgFnSc7d1
ekonomikou	ekonomika	k1gFnSc7
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
podle	podle	k7c2
HDP	HDP	kA
v	v	k7c6
paritě	parita	k1gFnSc6
kupní	kupní	k2eAgFnSc2d1
síly	síla	k1gFnSc2
druhou	druhý	k4xOgFnSc7
největší	veliký	k2eAgFnSc1d3
(	(	kIx(
<g/>
po	po	k7c6
Číně	Čína	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Jejich	jejich	k3xOp3gInSc1
ekonomický	ekonomický	k2eAgInSc1d1
výkon	výkon	k1gInSc1
představuje	představovat	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
čtvrtinu	čtvrtina	k1gFnSc4
celosvětového	celosvětový	k2eAgInSc2d1
HDP	HDP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
největším	veliký	k2eAgMnSc7d3
světovým	světový	k2eAgMnSc7d1
dovozcem	dovozce	k1gMnSc7
a	a	k8xC
druhým	druhý	k4xOgMnPc3
největším	veliký	k2eAgMnPc3d3
vývozcem	vývozce	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Staly	stát	k5eAaPmAgInP
se	s	k7c7
symbolem	symbol	k1gInSc7
a	a	k8xC
průkopníkem	průkopník	k1gMnSc7
ekonomiky	ekonomika	k1gFnSc2
založené	založený	k2eAgInPc4d1
na	na	k7c6
volném	volný	k2eAgInSc6d1
trhu	trh	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
mnohém	mnohé	k1gNnSc6
v	v	k7c6
tom	ten	k3xDgNnSc6
inspirovaly	inspirovat	k5eAaBmAgInP
Evropu	Evropa	k1gFnSc4
nebo	nebo	k8xC
Čínu	Čína	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
Evropě	Evropa	k1gFnSc3
ovšem	ovšem	k9
vykazují	vykazovat	k5eAaImIp3nP
menší	malý	k2eAgFnSc4d2
míru	míra	k1gFnSc4
přerozdělování	přerozdělování	k1gNnSc2
bohatství	bohatství	k1gNnSc2
a	a	k8xC
větší	veliký	k2eAgInPc4d2
ekonomické	ekonomický	k2eAgInPc4d1
rozdíly	rozdíl	k1gInPc4
mezi	mezi	k7c7
lidmi	člověk	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
nominálním	nominální	k2eAgNnSc6d1
HDP	HDP	kA
na	na	k7c4
obyvatele	obyvatel	k1gMnSc4
jsou	být	k5eAaImIp3nP
na	na	k7c6
osmém	osmý	k4xOgInSc6
místě	místo	k1gNnSc6
na	na	k7c6
světě	svět	k1gInSc6
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
v	v	k7c6
HDP	HDP	kA
v	v	k7c6
paritě	parita	k1gFnSc6
kupní	kupní	k2eAgFnSc2d1
síly	síla	k1gFnSc2
na	na	k7c4
hlavu	hlava	k1gFnSc4
na	na	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
dvanáctém	dvanáctý	k4xOgNnSc6
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
v	v	k7c6
indexu	index	k1gInSc6
lidského	lidský	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
na	na	k7c6
třináctém	třináctý	k4xOgInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Americká	americký	k2eAgFnSc1d1
měna	měna	k1gFnSc1
<g/>
,	,	kIx,
dolar	dolar	k1gInSc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgFnSc7d1
rezervní	rezervní	k2eAgFnSc7d1
měnou	měna	k1gFnSc7
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Americký	americký	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
a	a	k8xC
obchod	obchod	k1gInSc1
dosáhl	dosáhnout	k5eAaPmAgInS
vynikajících	vynikající	k2eAgInPc2d1
výsledků	výsledek	k1gInPc2
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
dobyl	dobýt	k5eAaPmAgMnS
trhy	trh	k1gInPc4
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
(	(	kIx(
<g/>
Ford	ford	k1gInSc1
<g/>
,	,	kIx,
Coca-Cola	coca-cola	k1gFnSc1
<g/>
,	,	kIx,
Boeing	boeing	k1gInSc1
<g/>
,	,	kIx,
JP	JP	kA
Morgan	morgan	k1gMnSc1
<g/>
,	,	kIx,
Exxon	Exxon	k1gMnSc1
<g/>
,	,	kIx,
Johnson	Johnson	k1gMnSc1
&	&	k?
Johnson	Johnson	k1gMnSc1
<g/>
,	,	kIx,
Walmart	Walmart	k1gInSc1
<g/>
,	,	kIx,
Amazon	amazona	k1gFnPc2
<g/>
,	,	kIx,
Apple	Apple	kA
<g/>
,	,	kIx,
Microsoft	Microsoft	kA
<g/>
,	,	kIx,
Google	Google	k1gFnSc1
<g/>
,	,	kIx,
Facebook	Facebook	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
většiny	většina	k1gFnSc2
kritérií	kritérion	k1gNnPc2
jsou	být	k5eAaImIp3nP
vojensky	vojensky	k6eAd1
nejmocnější	mocný	k2eAgFnSc7d3
zemí	zem	k1gFnSc7
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtyřicet	čtyřicet	k4xCc1
procent	procento	k1gNnPc2
vojenských	vojenský	k2eAgInPc2d1
výdajů	výdaj	k1gInPc2
na	na	k7c6
světě	svět	k1gInSc6
připadá	připadat	k5eAaImIp3nS,k5eAaPmIp3nS
právě	právě	k9
na	na	k7c6
USA	USA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Jsou	být	k5eAaImIp3nP
jadernou	jaderný	k2eAgFnSc7d1
velmocí	velmoc	k1gFnSc7
a	a	k8xC
jedinou	jediný	k2eAgFnSc7d1
zemí	zem	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
jadernou	jaderný	k2eAgFnSc4d1
zbraň	zbraň	k1gFnSc4
použila	použít	k5eAaPmAgFnS
ve	v	k7c6
vojenské	vojenský	k2eAgFnSc6d1
operaci	operace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
jedním	jeden	k4xCgMnSc7
z	z	k7c2
pěti	pět	k4xCc2
stálých	stálý	k2eAgMnPc2d1
členů	člen	k1gMnPc2
Rady	rada	k1gFnSc2
bezpečnosti	bezpečnost	k1gFnSc2
OSN	OSN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geopoliticky	geopoliticky	k6eAd1
jsou	být	k5eAaImIp3nP
hodnoceny	hodnotit	k5eAaImNgFnP
jako	jako	k8xC,k8xS
supervelmoc	supervelmoc	k1gFnSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
tento	tento	k3xDgInSc4
statut	statut	k1gInSc4
získaly	získat	k5eAaPmAgInP
především	především	k9
po	po	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
<g/>
,	,	kIx,
po	po	k7c6
svém	svůj	k3xOyFgNnSc6
vítězství	vítězství	k1gNnSc6
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
konci	konec	k1gInSc6
studené	studený	k2eAgFnSc2d1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
dokonce	dokonce	k9
hovořilo	hovořit	k5eAaImAgNnS
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
supervelmocí	supervelmoc	k1gFnSc7
jedinou	jediný	k2eAgFnSc7d1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
dnes	dnes	k6eAd1
snaží	snažit	k5eAaImIp3nP
změnit	změnit	k5eAaPmF
zejména	zejména	k9
Čína	Čína	k1gFnSc1
a	a	k8xC
Rusko	Rusko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Specifické	specifický	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
vztahy	vztah	k1gInPc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
s	s	k7c7
Evropou	Evropa	k1gFnSc7
a	a	k8xC
zejména	zejména	k9
Spojeným	spojený	k2eAgNnSc7d1
královstvím	království	k1gNnSc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
to	ten	k3xDgNnSc1
byly	být	k5eAaImAgFnP
evropské	evropský	k2eAgFnPc1d1
velmoci	velmoc	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
severní	severní	k2eAgFnPc4d1
Ameriku	Amerika	k1gFnSc4
kolonizovaly	kolonizovat	k5eAaBmAgFnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
pak	pak	k6eAd1
čelily	čelit	k5eAaImAgInP
vzpouře	vzpoura	k1gFnSc3
místních	místní	k2eAgMnPc2d1
kolonistů	kolonista	k1gMnPc2
a	a	k8xC
hnutí	hnutí	k1gNnSc2
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
střet	střet	k1gInSc1
s	s	k7c7
Brity	Brit	k1gMnPc7
byl	být	k5eAaImAgMnS
rozhodující	rozhodující	k2eAgMnSc1d1
(	(	kIx(
<g/>
nezávislost	nezávislost	k1gFnSc4
na	na	k7c4
Británii	Británie	k1gFnSc4
Američané	Američan	k1gMnPc1
vyhlásili	vyhlásit	k5eAaPmAgMnP
roku	rok	k1gInSc2
1776	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
prosazení	prosazení	k1gNnSc3
se	se	k3xPyFc4
angličtiny	angličtina	k1gFnSc2
jako	jako	k8xC,k8xS
hlavního	hlavní	k2eAgInSc2d1
a	a	k8xC
úředního	úřední	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
USA	USA	kA
však	však	k8xC
specifická	specifický	k2eAgFnSc1d1
vazba	vazba	k1gFnSc1
mezi	mezi	k7c7
Spojenými	spojený	k2eAgInPc7d1
státy	stát	k1gInPc7
a	a	k8xC
Británií	Británie	k1gFnSc7
(	(	kIx(
<g/>
a	a	k8xC
stejně	stejně	k6eAd1
tak	tak	k6eAd1
dalšími	další	k2eAgFnPc7d1
anglicky	anglicky	k6eAd1
mluvícími	mluvící	k2eAgFnPc7d1
zeměmi	zem	k1gFnPc7
Commonwealthu	Commonwealth	k1gInSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
Kanada	Kanada	k1gFnSc1
či	či	k8xC
Austrálie	Austrálie	k1gFnSc1
<g/>
)	)	kIx)
přetrvává	přetrvávat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vazba	vazba	k1gFnSc1
se	s	k7c7
západní	západní	k2eAgFnSc7d1
kontinentální	kontinentální	k2eAgFnSc7d1
Evropou	Evropa	k1gFnSc7
byla	být	k5eAaImAgFnS
dána	dát	k5eAaPmNgFnS
imigračními	imigrační	k2eAgFnPc7d1
vlnami	vlna	k1gFnPc7
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
(	(	kIx(
<g/>
zejm.	zejm.	k?
z	z	k7c2
Německa	Německo	k1gNnSc2
a	a	k8xC
Itálie	Itálie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
posílena	posílen	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
po	po	k7c6
americkém	americký	k2eAgNnSc6d1
osvobození	osvobození	k1gNnSc6
této	tento	k3xDgFnSc2
části	část	k1gFnSc2
Evropy	Evropa	k1gFnSc2
od	od	k7c2
nacismu	nacismus	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
se	se	k3xPyFc4
pak	pak	k6eAd1
<g/>
,	,	kIx,
mj.	mj.	kA
prostřednictvím	prostřednictvím	k7c2
vojenské	vojenský	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
NATO	NATO	kA
<g/>
,	,	kIx,
staly	stát	k5eAaPmAgFnP
garantem	garant	k1gMnSc7
bezpečnosti	bezpečnost	k1gFnSc2
této	tento	k3xDgFnSc2
části	část	k1gFnSc2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
pádu	pád	k1gInSc6
komunismu	komunismus	k1gInSc2
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
a	a	k8xC
východní	východní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
se	se	k3xPyFc4
vytvořily	vytvořit	k5eAaPmAgFnP
podobně	podobně	k6eAd1
úzké	úzký	k2eAgFnPc1d1
spojenecké	spojenecký	k2eAgFnPc1d1
vazby	vazba	k1gFnPc1
i	i	k9
zde	zde	k6eAd1
<g/>
,	,	kIx,
klíčovým	klíčový	k2eAgMnSc7d1
spojencem	spojenec	k1gMnSc7
je	být	k5eAaImIp3nS
pro	pro	k7c4
Američany	Američan	k1gMnPc4
zejména	zejména	k9
Polsko	Polsko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
supervelmoc	supervelmoc	k1gFnSc1
se	se	k3xPyFc4
však	však	k9
USA	USA	kA
snaží	snažit	k5eAaImIp3nS
budovat	budovat	k5eAaImF
spojenecké	spojenecký	k2eAgFnSc2d1
vazby	vazba	k1gFnSc2
ve	v	k7c6
všech	všecek	k3xTgInPc6
regionech	region	k1gInPc6
světa	svět	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
Blízkém	blízký	k2eAgInSc6d1
východě	východ	k1gInSc6
patří	patřit	k5eAaImIp3nS
ke	k	k7c3
klíčovým	klíčový	k2eAgMnPc3d1
spojencům	spojenec	k1gMnPc3
Izrael	Izrael	k1gInSc4
a	a	k8xC
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
Asii	Asie	k1gFnSc6
Japonsko	Japonsko	k1gNnSc1
a	a	k8xC
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
Kolumbie	Kolumbie	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
takovým	takový	k3xDgMnSc7
spojencem	spojenec	k1gMnSc7
stává	stávat	k5eAaImIp3nS
i	i	k9
Brazílie	Brazílie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Americká	americký	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
<g/>
,	,	kIx,
zejména	zejména	k9
pak	pak	k6eAd1
pop-kultura	pop-kultura	k1gFnSc1
<g/>
,	,	kIx,
získala	získat	k5eAaPmAgFnS
globální	globální	k2eAgInSc4d1
vliv	vliv	k1gInSc4
(	(	kIx(
<g/>
Hollywood	Hollywood	k1gInSc1
<g/>
,	,	kIx,
Disney	Disney	k1gInPc1
<g/>
,	,	kIx,
jazz	jazz	k1gInSc1
<g/>
,	,	kIx,
rock	rock	k1gInSc1
n	n	k0
roll	roll	k1gMnSc1
<g/>
,	,	kIx,
rap	rap	k1gMnSc1
<g/>
,	,	kIx,
televizní	televizní	k2eAgInPc4d1
seriály	seriál	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
americký	americký	k2eAgInSc1d1
životní	životní	k2eAgInSc1d1
styl	styl	k1gInSc1
(	(	kIx(
<g/>
automobilismus	automobilismus	k1gInSc1
<g/>
,	,	kIx,
fast	fast	k2eAgInSc1d1
food	food	k1gInSc1
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
až	až	k6eAd1
takový	takový	k3xDgInSc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
mimo	mimo	k7c4
USA	USA	kA
začalo	začít	k5eAaPmAgNnS
hovořit	hovořit	k5eAaImF
o	o	k7c6
americkém	americký	k2eAgInSc6d1
kulturním	kulturní	k2eAgInSc6d1
imperialismu	imperialismus	k1gInSc6
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
či	či	k8xC
amerikanizaci	amerikanizace	k1gFnSc4
života	život	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Výborných	výborný	k2eAgInPc2d1
výsledků	výsledek	k1gInPc2
dosahuje	dosahovat	k5eAaImIp3nS
americká	americký	k2eAgFnSc1d1
věda	věda	k1gFnSc1
a	a	k8xC
školství	školství	k1gNnSc1
(	(	kIx(
<g/>
systém	systém	k1gInSc1
univerzit	univerzita	k1gFnPc2
s	s	k7c7
Harvardem	Harvard	k1gInSc7
v	v	k7c6
čele	čelo	k1gNnSc6
<g/>
,	,	kIx,
vynález	vynález	k1gInSc4
atomové	atomový	k2eAgFnSc2d1
bomby	bomba	k1gFnSc2
<g/>
,	,	kIx,
internetu	internet	k1gInSc2
<g/>
,	,	kIx,
přistání	přistání	k1gNnSc4
na	na	k7c6
Měsíci	měsíc	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Etymologie	etymologie	k1gFnSc1
</s>
<s>
Původ	původ	k1gMnSc1
pojmu	pojem	k1gInSc2
Amerika	Amerika	k1gFnSc1
je	být	k5eAaImIp3nS
znám	znám	k2eAgInSc1d1
<g/>
:	:	kIx,
V	v	k7c6
roce	rok	k1gInSc6
1507	#num#	k4
vyrobil	vyrobit	k5eAaPmAgMnS
německý	německý	k2eAgMnSc1d1
kartograf	kartograf	k1gMnSc1
Martin	Martin	k1gMnSc1
Waldseemüller	Waldseemüller	k1gMnSc1
mapu	mapa	k1gFnSc4
světa	svět	k1gInSc2
<g/>
,	,	kIx,
na	na	k7c6
níž	jenž	k3xRgFnSc6
na	na	k7c6
počest	počest	k1gFnSc6
italského	italský	k2eAgMnSc2d1
objevitele	objevitel	k1gMnSc2
a	a	k8xC
kartografa	kartograf	k1gMnSc2
Ameriga	Amerig	k1gMnSc2
Vespucciho	Vespucci	k1gMnSc2
(	(	kIx(
<g/>
latinsky	latinsky	k6eAd1
Americus	Americus	k1gMnSc1
Vespucius	Vespucius	k1gMnSc1
<g/>
)	)	kIx)
označil	označit	k5eAaPmAgMnS
země	zem	k1gFnPc4
západní	západní	k2eAgFnSc2d1
polokoule	polokoule	k1gFnSc2
"	"	kIx"
<g/>
Amerika	Amerika	k1gFnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvolil	zvolit	k5eAaPmAgMnS
Vespucciho	Vespucciha	k1gMnSc5
a	a	k8xC
nikoli	nikoli	k9
Kryštofa	Kryštof	k1gMnSc4
Kolumba	Kolumbus	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
doplul	doplout	k5eAaPmAgInS
k	k	k7c3
americkým	americký	k2eAgInPc3d1
břehům	břeh	k1gInPc3
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
neboť	neboť	k8xC
Vespucci	Vespucce	k1gMnPc1
si	se	k3xPyFc3
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
Kolumba	Kolumbus	k1gMnSc2
uvědomil	uvědomit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
na	na	k7c6
novém	nový	k2eAgInSc6d1
kontinentě	kontinent	k1gInSc6
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
Kolumbus	Kolumbus	k1gMnSc1
se	se	k3xPyFc4
domníval	domnívat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
dorazil	dorazit	k5eAaPmAgMnS
do	do	k7c2
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
Vespucci	Vespucce	k1gFnSc4
nazýval	nazývat	k5eAaImAgInS
nový	nový	k2eAgInSc1d1
kontinent	kontinent	k1gInSc1
"	"	kIx"
<g/>
Nový	nový	k2eAgInSc1d1
svět	svět	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgNnSc1
užití	užití	k1gNnSc1
pojmu	pojem	k1gInSc2
“	“	k?
<g/>
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgNnSc4d1
<g/>
”	”	k?
je	být	k5eAaImIp3nS
doloženo	doložit	k5eAaPmNgNnS
v	v	k7c6
dopise	dopis	k1gInSc6
z	z	k7c2
2	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1776	#num#	k4
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
irský	irský	k2eAgMnSc1d1
generál	generál	k1gMnSc1
americké	americký	k2eAgFnSc2d1
revoluční	revoluční	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
Stephen	Stephen	k2eAgMnSc1d1
Moylan	Moylan	k1gMnSc1
asistentovi	asistent	k1gMnSc3
George	Georg	k1gMnPc4
Washingtona	Washingtona	k1gFnSc1
Josephu	Joseph	k1gMnSc3
Reedovi	Reeda	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
veřejném	veřejný	k2eAgInSc6d1
textu	text	k1gInSc6
se	se	k3xPyFc4
pojem	pojem	k1gInSc1
prvně	prvně	k?
objevil	objevit	k5eAaPmAgInS
v	v	k7c6
anonymní	anonymní	k2eAgFnSc6d1
eseji	esej	k1gFnSc6
v	v	k7c6
novinách	novina	k1gFnPc6
The	The	k1gFnSc2
Virginia	Virginium	k1gNnSc2
Gazette	Gazett	k1gMnSc5
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
vycházely	vycházet	k5eAaImAgFnP
ve	v	k7c6
Williamsburgu	Williamsburg	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Článek	článek	k1gInSc1
vyšel	vyjít	k5eAaPmAgInS
6	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1776	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
právně	právně	k6eAd1
závazném	závazný	k2eAgInSc6d1
textu	text	k1gInSc6
se	se	k3xPyFc4
název	název	k1gInSc1
objevil	objevit	k5eAaPmAgInS
v	v	k7c6
druhém	druhý	k4xOgInSc6
návrhu	návrh	k1gInSc6
Článků	článek	k1gInPc2
konfederace	konfederace	k1gFnSc2
a	a	k8xC
trvalé	trvalý	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
připravil	připravit	k5eAaPmAgMnS
John	John	k1gMnSc1
Dickinson	Dickinson	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návrh	návrh	k1gInSc1
vyhotovil	vyhotovit	k5eAaPmAgInS
k	k	k7c3
17	#num#	k4
<g/>
.	.	kIx.
červnu	červen	k1gInSc3
1776	#num#	k4
a	a	k8xC
uvádělo	uvádět	k5eAaImAgNnS
se	se	k3xPyFc4
v	v	k7c6
něm	on	k3xPp3gInSc6
<g/>
:	:	kIx,
“	“	k?
<g/>
Jméno	jméno	k1gNnSc1
této	tento	k3xDgFnSc2
konfederace	konfederace	k1gFnSc2
je	být	k5eAaImIp3nS
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgInPc4d1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červnu	červen	k1gInSc6
1776	#num#	k4
také	také	k9
Thomas	Thomas	k1gMnSc1
Jefferson	Jefferson	k1gMnSc1
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
pojem	pojem	k1gInSc4
“	“	k?
<g/>
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgFnSc2d1
<g/>
”	”	k?
velkými	velký	k2eAgNnPc7d1
písmeny	písmeno	k1gNnPc7
do	do	k7c2
nadpisu	nadpis	k1gInSc2
originálního	originální	k2eAgInSc2d1
hrubého	hrubý	k2eAgInSc2d1
návrhu	návrh	k1gInSc2
Deklarace	deklarace	k1gFnSc1
nezávislosti	nezávislost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Běžně	běžně	k6eAd1
se	se	k3xPyFc4
užívá	užívat	k5eAaImIp3nS
též	též	k9
krátká	krátký	k2eAgFnSc1d1
verze	verze	k1gFnSc1
názvu	název	k1gInSc2
"	"	kIx"
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
anglická	anglický	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
"	"	kIx"
<g/>
USA	USA	kA
<g/>
"	"	kIx"
v	v	k7c6
anglické	anglický	k2eAgFnSc6d1
i	i	k8xC
české	český	k2eAgFnSc3d1
výslovnosti	výslovnost	k1gFnSc3
(	(	kIx(
<g/>
logický	logický	k2eAgInSc4d1
český	český	k2eAgInSc4d1
ekvivalent	ekvivalent	k1gInSc4
SSA	SSA	kA
proti	proti	k7c3
tomu	ten	k3xDgNnSc3
zavededen	zavededen	k2eAgMnSc1d1
není	být	k5eNaImIp3nS
<g/>
)	)	kIx)
či	či	k8xC
pojem	pojem	k1gInSc1
"	"	kIx"
<g/>
Amerika	Amerika	k1gFnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
užívalo	užívat	k5eAaImAgNnS
též	též	k9
pojmu	pojem	k1gInSc3
"	"	kIx"
<g/>
Columbia	Columbia	k1gFnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
odkazoval	odkazovat	k5eAaImAgMnS
k	k	k7c3
objeviteli	objevitel	k1gMnSc3
kontinentu	kontinent	k1gInSc2
Kryštofu	Kryštof	k1gMnSc3
Kolumbovi	Kolumbus	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
pojem	pojem	k1gInSc1
<g/>
,	,	kIx,
spíše	spíše	k9
literární	literární	k2eAgMnSc1d1
<g/>
,	,	kIx,
časem	čas	k1gInSc7
vymizel	vymizet	k5eAaPmAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
zachován	zachovat	k5eAaPmNgInS
v	v	k7c6
označení	označení	k1gNnSc6
federálního	federální	k2eAgNnSc2d1
území	území	k1gNnSc2
(	(	kIx(
<g/>
de	de	k?
facto	facto	k1gNnSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
)	)	kIx)
"	"	kIx"
<g/>
District	District	k1gInSc1
of	of	k?
Columbia	Columbia	k1gFnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
angličtině	angličtina	k1gFnSc6
se	se	k3xPyFc4
pojem	pojem	k1gInSc1
United	United	k1gMnSc1
States	States	k1gMnSc1
nejprve	nejprve	k6eAd1
užíval	užívat	k5eAaImAgMnS
v	v	k7c6
množném	množný	k2eAgNnSc6d1
čísle	číslo	k1gNnSc6
(	(	kIx(
<g/>
"	"	kIx"
<g/>
United	United	k1gMnSc1
States	States	k1gMnSc1
are	ar	k1gInSc5
<g/>
...	...	k?
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
občanské	občanský	k2eAgFnSc6d1
válce	válka	k1gFnSc6
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
používat	používat	k5eAaImF
v	v	k7c6
čísle	číslo	k1gNnSc6
jednotném	jednotný	k2eAgNnSc6d1
(	(	kIx(
<g/>
"	"	kIx"
<g/>
United	United	k1gMnSc1
States	States	k1gMnSc1
is	is	k?
<g/>
...	...	k?
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
zdůrazněno	zdůraznit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
nerozbornou	rozborný	k2eNgFnSc4d1
jednotku	jednotka	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
češtině	čeština	k1gFnSc6
se	se	k3xPyFc4
ale	ale	k9
tato	tento	k3xDgFnSc1
verze	verze	k1gFnSc1
neprosadila	prosadit	k5eNaPmAgFnS
a	a	k8xC
užívá	užívat	k5eAaImIp3nS
se	se	k3xPyFc4
nadále	nadále	k6eAd1
plurál	plurál	k1gInSc1
(	(	kIx(
<g/>
"	"	kIx"
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
jsou	být	k5eAaImIp3nP
<g/>
...	...	k?
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
přídavného	přídavný	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
"	"	kIx"
<g/>
americký	americký	k2eAgInSc1d1
<g/>
"	"	kIx"
převážil	převážit	k5eAaPmAgInS
v	v	k7c6
češtině	čeština	k1gFnSc6
význam	význam	k1gInSc1
"	"	kIx"
<g/>
vztahující	vztahující	k2eAgMnSc1d1
se	se	k3xPyFc4
ke	k	k7c3
Spojeným	spojený	k2eAgInPc3d1
státům	stát	k1gInPc3
<g/>
"	"	kIx"
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
pojem	pojem	k1gInSc1
může	moct	k5eAaImIp3nS
značit	značit	k5eAaImF
i	i	k8xC
"	"	kIx"
<g/>
vztahující	vztahující	k2eAgMnSc1d1
se	se	k3xPyFc4
k	k	k7c3
celému	celý	k2eAgInSc3d1
americkému	americký	k2eAgInSc3d1
kontinentu	kontinent	k1gInSc3
<g/>
"	"	kIx"
(	(	kIx(
<g/>
tedy	tedy	k8xC
severní	severní	k2eAgFnSc6d1
i	i	k8xC
jižní	jižní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhý	druhý	k4xOgInSc1
význam	význam	k1gInSc1
je	být	k5eAaImIp3nS
však	však	k9
typický	typický	k2eAgInSc1d1
jen	jen	k9
pro	pro	k7c4
odbornější	odborný	k2eAgFnPc4d2
geografické	geografický	k2eAgFnPc4d1
<g/>
,	,	kIx,
geologické	geologický	k2eAgFnPc4d1
či	či	k8xC
jiné	jiný	k2eAgFnPc4d1
přírodovědné	přírodovědný	k2eAgFnPc4d1
publikace	publikace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
běžné	běžný	k2eAgFnSc6d1
řeči	řeč	k1gFnSc6
a	a	k8xC
publicistice	publicistika	k1gFnSc6
převažuje	převažovat	k5eAaImIp3nS
význam	význam	k1gInSc1
prvý	prvý	k4xOgInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
více	hodně	k6eAd2
pak	pak	k6eAd1
u	u	k7c2
pojmu	pojem	k1gInSc2
Američan	Američan	k1gMnSc1
<g/>
,	,	kIx,
kterým	který	k3yIgMnSc7,k3yQgMnSc7,k3yRgMnSc7
je	být	k5eAaImIp3nS
prakticky	prakticky	k6eAd1
vždy	vždy	k6eAd1
míněn	míněn	k2eAgMnSc1d1
"	"	kIx"
<g/>
občan	občan	k1gMnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejné	stejné	k1gNnSc1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
v	v	k7c6
angličtině	angličtina	k1gFnSc6
u	u	k7c2
pojmu	pojem	k1gInSc2
"	"	kIx"
<g/>
American	American	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Američané	Američan	k1gMnPc1
jím	on	k3xPp3gMnSc7
označují	označovat	k5eAaImIp3nP
jak	jak	k9
"	"	kIx"
<g/>
občana	občan	k1gMnSc4
USA	USA	kA
<g/>
"	"	kIx"
<g/>
,	,	kIx,
tak	tak	k9
"	"	kIx"
<g/>
příslušníka	příslušník	k1gMnSc4
amerického	americký	k2eAgInSc2d1
národa	národ	k1gInSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
neboť	neboť	k8xC
v	v	k7c6
tom	ten	k3xDgNnSc6
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
prakticky	prakticky	k6eAd1
není	být	k5eNaImIp3nS
rozdíl	rozdíl	k1gInSc4
<g/>
,	,	kIx,
vzhledem	vzhledem	k7c3
k	k	k7c3
americké	americký	k2eAgFnSc3d1
politické	politický	k2eAgFnSc3d1
(	(	kIx(
<g/>
státní	státní	k2eAgFnSc3d1
<g/>
)	)	kIx)
koncepci	koncepce	k1gFnSc3
národa	národ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Evropě	Evropa	k1gFnSc6
převažuje	převažovat	k5eAaImIp3nS
pojetí	pojetí	k1gNnSc1
národa	národ	k1gInSc2
kulturní	kulturní	k2eAgInSc1d1
(	(	kIx(
<g/>
jazyk	jazyk	k1gInSc1
<g/>
,	,	kIx,
kultura	kultura	k1gFnSc1
<g/>
)	)	kIx)
či	či	k8xC
etnické	etnický	k2eAgFnPc4d1
(	(	kIx(
<g/>
společný	společný	k2eAgInSc4d1
původ	původ	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
USA	USA	kA
také	také	k9
pojem	pojem	k1gInSc1
"	"	kIx"
<g/>
nation	nation	k1gInSc1
<g/>
"	"	kIx"
prakticky	prakticky	k6eAd1
splývá	splývat	k5eAaImIp3nS
s	s	k7c7
pojmem	pojem	k1gInSc7
"	"	kIx"
<g/>
stát	stát	k1gInSc1
<g/>
"	"	kIx"
či	či	k8xC
"	"	kIx"
<g/>
lid	lid	k1gInSc1
státu	stát	k1gInSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
pojem	pojem	k1gInSc4
národ	národ	k1gInSc4
v	v	k7c6
evropském	evropský	k2eAgInSc6d1
smyslu	smysl	k1gInSc6
se	se	k3xPyFc4
užívá	užívat	k5eAaImIp3nS
spíše	spíše	k9
pojem	pojem	k1gInSc1
"	"	kIx"
<g/>
ethnicity	ethnicita	k1gFnPc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s>
Dějiny	dějiny	k1gFnPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Dějiny	dějiny	k1gFnPc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
států	stát	k1gInPc2
USA	USA	kA
podle	podle	k7c2
vstupu	vstup	k1gInSc2
do	do	k7c2
Unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Původní	původní	k2eAgNnSc1d1
osídlení	osídlení	k1gNnSc1
</s>
<s>
Setkání	setkání	k1gNnSc1
Evropanů	Evropan	k1gMnPc2
s	s	k7c7
původními	původní	k2eAgMnPc7d1
Američany	Američan	k1gMnPc7
<g/>
,	,	kIx,
1764	#num#	k4
</s>
<s>
Původní	původní	k2eAgNnSc1d1
obyvatelstvo	obyvatelstvo	k1gNnSc1
<g/>
,	,	kIx,
tj.	tj.	kA
Indiáni	Indián	k1gMnPc1
a	a	k8xC
Eskymáci	Eskymák	k1gMnPc1
<g/>
,	,	kIx,
osídlilo	osídlit	k5eAaPmAgNnS
americký	americký	k2eAgInSc4d1
kontinent	kontinent	k1gInSc4
pravděpodobně	pravděpodobně	k6eAd1
z	z	k7c2
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tradiční	tradiční	k2eAgInSc1d1
odhad	odhad	k1gInSc1
příchodu	příchod	k1gInSc2
tzv.	tzv.	kA
paleoindiánů	paleoindián	k1gInPc2
přes	přes	k7c4
zamrzlou	zamrzlý	k2eAgFnSc4d1
Beringovu	Beringův	k2eAgFnSc4d1
úžinu	úžina	k1gFnSc4
je	být	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
12	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
11	#num#	k4
000	#num#	k4
let	léto	k1gNnPc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
některé	některý	k3yIgInPc1
novější	nový	k2eAgInPc1d2
výzkumy	výzkum	k1gInPc1
počítají	počítat	k5eAaImIp3nP
i	i	k9
s	s	k7c7
časnějším	časný	k2eAgNnSc7d2
osídlením	osídlení	k1gNnSc7
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
po	po	k7c6
moři	moře	k1gNnSc6
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
též	též	k9
solutréenská	solutréenský	k2eAgFnSc1d1
hypotéza	hypotéza	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolonisté	kolonista	k1gMnPc1
ze	z	k7c2
Sibiře	Sibiř	k1gFnSc2
postupovali	postupovat	k5eAaImAgMnP
ze	z	k7c2
severu	sever	k1gInSc2
na	na	k7c4
jih	jih	k1gInSc4
podél	podél	k7c2
pobřeží	pobřeží	k1gNnSc2
Tichého	Tichého	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
a	a	k8xC
patrně	patrně	k6eAd1
takto	takto	k6eAd1
osídlili	osídlit	k5eAaPmAgMnP
nakonec	nakonec	k6eAd1
i	i	k9
jižní	jižní	k2eAgFnSc4d1
Ameriku	Amerika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
kultura	kultura	k1gFnSc1
je	být	k5eAaImIp3nS
nazývána	nazývat	k5eAaImNgFnS
kultura	kultura	k1gFnSc1
Clovis	Clovis	k1gFnSc1
<g/>
,	,	kIx,
pozůstatky	pozůstatek	k1gInPc1
po	po	k7c6
ní	on	k3xPp3gFnSc6
byly	být	k5eAaImAgFnP
nalezeny	nalezen	k2eAgFnPc1d1
zejména	zejména	k9
v	v	k7c6
dnešním	dnešní	k2eAgNnSc6d1
Novém	nový	k2eAgNnSc6d1
Mexiku	Mexiko	k1gNnSc6
<g/>
,	,	kIx,
Texasu	Texas	k1gInSc6
<g/>
,	,	kIx,
Virginii	Virginie	k1gFnSc6
či	či	k8xC
Pensylvánii	Pensylvánie	k1gFnSc6
(	(	kIx(
<g/>
ale	ale	k8xC
také	také	k9
v	v	k7c6
Chile	Chile	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Clovis	Clovis	k1gFnSc1
byla	být	k5eAaImAgFnS
dlouho	dlouho	k6eAd1
považována	považován	k2eAgFnSc1d1
za	za	k7c4
nejstarší	starý	k2eAgFnSc4d3
domorodou	domorodý	k2eAgFnSc4d1
kulturu	kultura	k1gFnSc4
obou	dva	k4xCgFnPc2
Amerik	Amerika	k1gFnPc2
a	a	k8xC
prapředka	prapředek	k1gMnSc2
všech	všecek	k3xTgFnPc2
kultur	kultura	k1gFnPc2
pozdějších	pozdní	k2eAgMnPc2d2
<g/>
,	,	kIx,
množí	množit	k5eAaImIp3nP
se	se	k3xPyFc4
ale	ale	k9
důkazy	důkaz	k1gInPc1
i	i	k8xC
o	o	k7c6
některých	některý	k3yIgFnPc6
starších	starý	k2eAgFnPc6d2
kulturách	kultura	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
pozdějších	pozdní	k2eAgFnPc2d2
předkolumbovských	předkolumbovský	k2eAgFnPc2d1
komunit	komunita	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
byla	být	k5eAaImAgFnS
nejvýznamnější	významný	k2eAgFnSc1d3
Mississippská	mississippský	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
kulminovala	kulminovat	k5eAaImAgFnS
v	v	k7c4
období	období	k1gNnSc4
mezi	mezi	k7c7
léty	léto	k1gNnPc7
800	#num#	k4
<g/>
–	–	k?
<g/>
1600	#num#	k4
<g/>
,	,	kIx,
s	s	k7c7
centrem	centrum	k1gNnSc7
v	v	k7c6
Cahokii	Cahokie	k1gFnSc6
<g/>
,	,	kIx,
nedaleko	nedaleko	k7c2
dnešního	dnešní	k2eAgNnSc2d1
St.	st.	kA
Louis	Louis	k1gMnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
bylo	být	k5eAaImAgNnS
nalezeno	nalézt	k5eAaBmNgNnS,k5eAaPmNgNnS
osmdesát	osmdesát	k4xCc1
mohyl	mohyla	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
největší	veliký	k2eAgInSc1d3
je	být	k5eAaImIp3nS
Monks	Monks	k1gInSc1
Mound	Mounda	k1gFnPc2
<g/>
,	,	kIx,
s	s	k7c7
rozlohou	rozloha	k1gFnSc7
pět	pět	k4xCc4
hektarů	hektar	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
30	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doloženo	doložen	k2eAgNnSc4d1
bylo	být	k5eAaImAgNnS
vyspělé	vyspělý	k2eAgNnSc1d1
zemědělství	zemědělství	k1gNnSc1
se	s	k7c7
zásadní	zásadní	k2eAgFnSc7d1
rolí	role	k1gFnSc7
kukuřice	kukuřice	k1gFnSc2
<g/>
,	,	kIx,
ptačí	ptačí	k2eAgInSc4d1
kult	kult	k1gInSc4
a	a	k8xC
lidské	lidský	k2eAgFnPc4d1
oběti	oběť	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třetí	třetí	k4xOgFnSc4
nejvýznamnější	významný	k2eAgFnSc4d3
předkolumbovskou	předkolumbovský	k2eAgFnSc4d1
kulturu	kultura	k1gFnSc4
vytvořili	vytvořit	k5eAaPmAgMnP
Anasaziové	Anasazius	k1gMnPc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
dnešního	dnešní	k2eAgNnSc2d1
Nového	Nového	k2eAgNnSc2d1
Mexika	Mexiko	k1gNnSc2
<g/>
,	,	kIx,
Arizony	Arizona	k1gFnSc2
<g/>
,	,	kIx,
Colorada	Colorado	k1gNnSc2
a	a	k8xC
Utahu	Utah	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavěli	stavět	k5eAaImAgMnP
sídla	sídlo	k1gNnSc2
zvaná	zvaný	k2eAgNnPc1d1
pueblo	puebnout	k5eAaPmAgNnS
<g/>
,	,	kIx,
nejslavnější	slavný	k2eAgFnPc1d3
z	z	k7c2
nich	on	k3xPp3gInPc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c4
Mesa	Mesus	k1gMnSc4
Verde	Verd	k1gInSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Pozoruhodná	pozoruhodný	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
se	se	k3xPyFc4
nacházela	nacházet	k5eAaImAgFnS
rovněž	rovněž	k9
v	v	k7c6
Louisianě	Louisiana	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
archeologické	archeologický	k2eAgFnSc6d1
lokalitě	lokalita	k1gFnSc6
Poverty	Povert	k1gMnPc4
Point	pointa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
severu	sever	k1gInSc6
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
byla	být	k5eAaImAgFnS
nejvýznamnějším	významný	k2eAgMnSc7d3
<g />
.	.	kIx.
</s>
<s hack="1">
útvarem	útvar	k1gInSc7
Irokézká	Irokézký	k2eAgFnSc1d1
liga	liga	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
však	však	k9
zažívala	zažívat	k5eAaImAgFnS
svůj	svůj	k3xOyFgInSc4
vrchol	vrchol	k1gInSc4
až	až	k9
po	po	k7c6
příchodu	příchod	k1gInSc6
Evropanů	Evropan	k1gMnPc2
a	a	k8xC
sehrávala	sehrávat	k5eAaImAgFnS
svou	svůj	k3xOyFgFnSc4
roli	role	k1gFnSc4
ještě	ještě	k9
v	v	k7c6
britsko-amerických	britsko-americký	k2eAgInPc6d1
konfliktech	konflikt	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
O	o	k7c6
velikosti	velikost	k1gFnSc6
americké	americký	k2eAgFnSc2d1
populace	populace	k1gFnSc2
před	před	k7c7
příchodem	příchod	k1gInSc7
Evropanů	Evropan	k1gMnPc2
se	se	k3xPyFc4
vedou	vést	k5eAaImIp3nP
spory	spor	k1gInPc1
a	a	k8xC
odhady	odhad	k1gInPc1
se	se	k3xPyFc4
značně	značně	k6eAd1
rozcházejí	rozcházet	k5eAaImIp3nP
<g/>
,	,	kIx,
od	od	k7c2
500	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
až	až	k6eAd1
k	k	k7c3
9	#num#	k4
milionům	milion	k4xCgInPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pro	pro	k7c4
současnou	současný	k2eAgFnSc4d1
evropskou	evropský	k2eAgFnSc4d1
civilizaci	civilizace	k1gFnSc4
byl	být	k5eAaImAgInS
tzv.	tzv.	kA
Nový	nový	k2eAgInSc1d1
svět	svět	k1gInSc1
objeven	objevit	k5eAaPmNgInS
výpravou	výprava	k1gFnSc7
Kryštofa	Kryštof	k1gMnSc2
Kolumba	Kolumbus	k1gMnSc2
roku	rok	k1gInSc2
1492	#num#	k4
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
již	již	k6eAd1
předtím	předtím	k6eAd1
<g/>
,	,	kIx,
okolo	okolo	k7c2
roku	rok	k1gInSc2
1000	#num#	k4
<g/>
,	,	kIx,
přistáli	přistát	k5eAaImAgMnP,k5eAaPmAgMnP
u	u	k7c2
břehů	břeh	k1gInPc2
Ameriky	Amerika	k1gFnSc2
Vikingové	Viking	k1gMnPc1
vedeni	vést	k5eAaImNgMnP
Leifem	Leif	k1gMnSc7
Erikssonem	Eriksson	k1gInSc7
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
též	též	k9
Vinland	Vinland	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Koloniální	koloniální	k2eAgNnSc1d1
období	období	k1gNnSc1
</s>
<s>
Evropské	evropský	k2eAgFnSc2d1
kolonie	kolonie	k1gFnSc2
před	před	k7c7
francouzsko-indiánskou	francouzsko-indiánský	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
v	v	k7c6
polovině	polovina	k1gFnSc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
V	v	k7c6
dalších	další	k2eAgNnPc6d1
stoletích	století	k1gNnPc6
se	se	k3xPyFc4
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
stala	stát	k5eAaPmAgFnS
cílem	cíl	k1gInSc7
kolonizačních	kolonizační	k2eAgFnPc2d1
snah	snaha	k1gFnPc2
Španělska	Španělsko	k1gNnSc2
(	(	kIx(
<g/>
Mexiko	Mexiko	k1gNnSc1
<g/>
,	,	kIx,
Florida	Florida	k1gFnSc1
<g/>
,	,	kIx,
území	území	k1gNnSc1
západně	západně	k6eAd1
od	od	k7c2
Mississippi	Mississippi	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nizozemska	Nizozemsko	k1gNnSc2
(	(	kIx(
<g/>
část	část	k1gFnSc1
východního	východní	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Francie	Francie	k1gFnSc1
(	(	kIx(
<g/>
Kanada	Kanada	k1gFnSc1
<g/>
,	,	kIx,
povodí	povodí	k1gNnSc1
Mississippi	Mississippi	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
malé	malý	k2eAgFnSc6d1
míře	míra	k1gFnSc6
i	i	k8xC
Švédska	Švédsko	k1gNnSc2
(	(	kIx(
<g/>
Nové	Nové	k2eAgNnSc1d1
Švédsko	Švédsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
historii	historie	k1gFnSc4
budoucích	budoucí	k2eAgInPc2d1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
měla	mít	k5eAaImAgFnS
největší	veliký	k2eAgInSc4d3
význam	význam	k1gInSc4
anglická	anglický	k2eAgFnSc1d1
kolonizace	kolonizace	k1gFnSc1
atlantského	atlantský	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1664	#num#	k4
se	se	k3xPyFc4
Anglie	Anglie	k1gFnSc1
a	a	k8xC
později	pozdě	k6eAd2
Království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
postupně	postupně	k6eAd1
zmocnily	zmocnit	k5eAaPmAgFnP
nizozemských	nizozemský	k2eAgFnPc2d1
a	a	k8xC
části	část	k1gFnSc6
francouzských	francouzský	k2eAgFnPc2d1
osad	osada	k1gFnPc2
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
a	a	k8xC
do	do	k7c2
roku	rok	k1gInSc2
1773	#num#	k4
vytvořily	vytvořit	k5eAaPmAgFnP
na	na	k7c6
pobřeží	pobřeží	k1gNnSc6
13	#num#	k4
kolonií	kolonie	k1gFnPc2
(	(	kIx(
<g/>
Massachusetts	Massachusetts	k1gNnSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
Jersey	Jersea	k1gFnPc1
<g/>
,	,	kIx,
New	New	k1gFnPc1
York	York	k1gInSc1
<g/>
,	,	kIx,
Rhode	Rhodos	k1gInSc5
Island	Island	k1gInSc1
<g/>
,	,	kIx,
Connecticut	Connecticut	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gMnSc5
Hampshire	Hampshir	k1gMnSc5
<g/>
,	,	kIx,
Pennsylvania	Pennsylvanium	k1gNnPc1
<g/>
,	,	kIx,
Delaware	Delawar	k1gMnSc5
<g/>
,	,	kIx,
Virginia	Virginium	k1gNnPc1
<g/>
,	,	kIx,
Maryland	Maryland	k1gInSc1
<g/>
,	,	kIx,
North	North	k1gInSc1
Carolina	Carolina	k1gFnSc1
<g/>
,	,	kIx,
South	South	k1gInSc1
Carolina	Carolina	k1gFnSc1
<g/>
,	,	kIx,
Georgia	Georgia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
základem	základ	k1gInSc7
budoucích	budoucí	k2eAgMnPc2d1
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Přestože	přestože	k8xS
evropské	evropský	k2eAgFnPc1d1
mocnosti	mocnost	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
kolonizovaly	kolonizovat	k5eAaBmAgFnP
sever	sever	k1gInSc4
amerického	americký	k2eAgInSc2d1
kontinentu	kontinent	k1gInSc2
<g/>
,	,	kIx,
zdůrazňovaly	zdůrazňovat	k5eAaImAgFnP
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gInSc1
postup	postup	k1gInSc1
není	být	k5eNaImIp3nS
kořistnický	kořistnický	k2eAgInSc1d1
jako	jako	k8xS,k8xC
postup	postup	k1gInSc1
Španělů	Španěl	k1gMnPc2
a	a	k8xC
Portugalců	Portugalec	k1gMnPc2
v	v	k7c6
jižní	jižní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
,	,	kIx,
evropská	evropský	k2eAgFnSc1d1
kolonizace	kolonizace	k1gFnSc1
znamenala	znamenat	k5eAaImAgFnS
i	i	k9
zde	zde	k6eAd1
pro	pro	k7c4
původní	původní	k2eAgMnPc4d1
obyvatele	obyvatel	k1gMnPc4
katastrofu	katastrofa	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Nové	Nové	k2eAgFnSc2d1
zavlečené	zavlečený	k2eAgFnSc2d1
nemoci	nemoc	k1gFnSc2
jako	jako	k8xS,k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
neštovice	neštovice	k1gFnSc1
<g/>
,	,	kIx,
chřipka	chřipka	k1gFnSc1
a	a	k8xC
spalničky	spalničky	k1gFnPc1
zredukovaly	zredukovat	k5eAaPmAgFnP
jejich	jejich	k3xOp3gFnSc4
populaci	populace	k1gFnSc4
místy	místy	k6eAd1
až	až	k9
o	o	k7c4
90	#num#	k4
%	%	kIx~
a	a	k8xC
zcela	zcela	k6eAd1
zničily	zničit	k5eAaPmAgFnP
jejich	jejich	k3xOp3gFnPc1
sociální	sociální	k2eAgFnPc1d1
struktury	struktura	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
Ale	ale	k8xC
již	již	k6eAd1
při	při	k7c6
této	tento	k3xDgFnSc6
kolonizaci	kolonizace	k1gFnSc6
se	se	k3xPyFc4
rodil	rodit	k5eAaImAgMnS
i	i	k9
nový	nový	k2eAgMnSc1d1
svobodný	svobodný	k2eAgMnSc1d1
duch	duch	k1gMnSc1
budoucího	budoucí	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
řada	řada	k1gFnSc1
z	z	k7c2
prvních	první	k4xOgMnPc2
osadníků	osadník	k1gMnPc2
byli	být	k5eAaImAgMnP
Evropané	Evropan	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
odcházeli	odcházet	k5eAaImAgMnP
ze	z	k7c2
starého	starý	k2eAgInSc2d1
kontinentu	kontinent	k1gInSc2
kvůli	kvůli	k7c3
náboženskému	náboženský	k2eAgInSc3d1
útlaku	útlak	k1gInSc3
a	a	k8xC
věřili	věřit	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
novém	nový	k2eAgInSc6d1
světě	svět	k1gInSc6
naleznou	naleznout	k5eAaPmIp3nP,k5eAaBmIp3nP
útočiště	útočiště	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náboženská	náboženský	k2eAgFnSc1d1
svoboda	svoboda	k1gFnSc1
a	a	k8xC
tolerance	tolerance	k1gFnSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
stala	stát	k5eAaPmAgFnS
jedním	jeden	k4xCgNnSc7
z	z	k7c2
klíčových	klíčový	k2eAgNnPc2d1
témat	téma	k1gNnPc2
při	při	k7c6
formování	formování	k1gNnSc6
novodobé	novodobý	k2eAgFnSc2d1
severoamerické	severoamerický	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amerika	Amerika	k1gFnSc1
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
též	též	k9
nekopírovala	kopírovat	k5eNaImAgFnS
evropské	evropský	k2eAgInPc4d1
sekularizační	sekularizační	k2eAgInPc4d1
trendy	trend	k1gInPc4
a	a	k8xC
modernismus	modernismus	k1gInSc1
zde	zde	k6eAd1
byl	být	k5eAaImAgInS
prosazován	prosazovat	k5eAaImNgInS
naopak	naopak	k6eAd1
v	v	k7c6
součinnosti	součinnost	k1gFnSc6
s	s	k7c7
vírou	víra	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
získala	získat	k5eAaPmAgFnS
specifický	specifický	k2eAgInSc4d1
<g/>
,	,	kIx,
naléhavý	naléhavý	k2eAgInSc4d1
<g/>
,	,	kIx,
osobní	osobní	k2eAgInSc4d1
charakter	charakter	k1gInSc4
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
První	první	k4xOgNnSc1
velké	velký	k2eAgNnSc1d1
probuzení	probuzení	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zejména	zejména	k9
mezi	mezi	k7c7
protestanty	protestant	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
nakonec	nakonec	k6eAd1
mezi	mezi	k7c7
osadníky	osadník	k1gMnPc7
převážili	převážit	k5eAaPmAgMnP
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Specifický	specifický	k2eAgInSc1d1
americký	americký	k2eAgInSc1d1
přístup	přístup	k1gInSc1
k	k	k7c3
náboženskému	náboženský	k2eAgInSc3d1
prožitku	prožitek	k1gInSc3
se	se	k3xPyFc4
v	v	k7c6
kultuře	kultura	k1gFnSc6
a	a	k8xC
politice	politika	k1gFnSc6
USA	USA	kA
projevuje	projevovat	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
americký	americký	k2eAgInSc1d1
evangelikalismus	evangelikalismus	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Typickým	typický	k2eAgInSc7d1
rysem	rys	k1gInSc7
prvních	první	k4xOgFnPc2
osadnických	osadnický	k2eAgFnPc2d1
kultur	kultura	k1gFnPc2
byl	být	k5eAaImAgInS
rovněž	rovněž	k9
důraz	důraz	k1gInSc1
na	na	k7c4
samosprávu	samospráva	k1gFnSc4
a	a	k8xC
konstitucionalismus	konstitucionalismus	k1gInSc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
práva	právo	k1gNnPc1
zaručená	zaručený	k2eAgNnPc1d1
základními	základní	k2eAgInPc7d1
zákony	zákon	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Kolem	kolem	k7c2
roku	rok	k1gInSc2
1770	#num#	k4
dosáhla	dosáhnout	k5eAaPmAgFnS
populace	populace	k1gFnSc1
ve	v	k7c6
třinácti	třináct	k4xCc6
britských	britský	k2eAgFnPc6d1
koloniích	kolonie	k1gFnPc6
2,1	2,1	k4
milionu	milion	k4xCgInSc2
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
představovalo	představovat	k5eAaImAgNnS
třetinu	třetina	k1gFnSc4
populace	populace	k1gFnSc2
tehdejší	tehdejší	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tradiční	tradiční	k2eAgInSc1d1
důraz	důraz	k1gInSc1
na	na	k7c4
samosprávnost	samosprávnost	k1gFnSc4
<g/>
,	,	kIx,
rostoucí	rostoucí	k2eAgFnSc1d1
ekonomická	ekonomický	k2eAgFnSc1d1
síla	síla	k1gFnSc1
i	i	k8xC
populace	populace	k1gFnSc1
a	a	k8xC
koroze	koroze	k1gFnSc1
idejí	idea	k1gFnPc2
monarchismu	monarchismus	k1gInSc2
u	u	k7c2
klíčových	klíčový	k2eAgMnPc2d1
myslitelů	myslitel	k1gMnPc2
(	(	kIx(
<g/>
John	John	k1gMnSc1
Adams	Adamsa	k1gFnPc2
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
Paine	Pain	k1gInSc5
<g/>
,	,	kIx,
James	James	k1gMnSc1
Madison	Madison	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
předurčily	předurčit	k5eAaPmAgInP
střet	střet	k1gInSc4
mezi	mezi	k7c7
kolonií	kolonie	k1gFnSc7
a	a	k8xC
kolonizačním	kolonizační	k2eAgNnSc7d1
centrem	centrum	k1gNnSc7
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Vznik	vznik	k1gInSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgFnPc2d1
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
expanze	expanze	k1gFnSc1
</s>
<s>
Deklarace	deklarace	k1gFnSc1
americké	americký	k2eAgFnSc2d1
nezávislosti	nezávislost	k1gFnSc2
</s>
<s>
Mapa	mapa	k1gFnSc1
expanze	expanze	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Mapa	mapa	k1gFnSc1
ukazující	ukazující	k2eAgNnSc1d1
přistoupení	přistoupení	k1gNnSc1
jednotlivých	jednotlivý	k2eAgInPc2d1
států	stát	k1gInPc2
k	k	k7c3
unii	unie	k1gFnSc3
od	od	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Bezohledné	bezohledný	k2eAgInPc1d1
zásahy	zásah	k1gInPc1
mateřské	mateřský	k2eAgFnSc2d1
země	zem	k1gFnSc2
do	do	k7c2
poměrů	poměr	k1gInPc2
v	v	k7c6
koloniích	kolonie	k1gFnPc6
vyvolaly	vyvolat	k5eAaPmAgFnP
protibritskou	protibritský	k2eAgFnSc4d1
opozici	opozice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pověstným	pověstný	k2eAgInSc7d1
incidentem	incident	k1gInSc7
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
zvláště	zvláště	k9
tzv.	tzv.	kA
Bostonské	bostonský	k2eAgNnSc1d1
pití	pití	k1gNnSc1
čaje	čaj	k1gInSc2
roku	rok	k1gInSc2
1773	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napětí	napětí	k1gNnSc1
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1775	#num#	k4
vypuknutím	vypuknutí	k1gNnSc7
otevřené	otevřený	k2eAgFnSc2d1
války	válka	k1gFnSc2
mezi	mezi	k7c7
koloniemi	kolonie	k1gFnPc7
a	a	k8xC
Velkou	velký	k2eAgFnSc7d1
Británií	Británie	k1gFnSc7
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1776	#num#	k4
vydal	vydat	k5eAaPmAgInS
druhý	druhý	k4xOgInSc4
Kontinentální	kontinentální	k2eAgInSc4d1
kongres	kongres	k1gInSc4
Deklaraci	deklarace	k1gFnSc4
nezávislosti	nezávislost	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vyhlašovala	vyhlašovat	k5eAaImAgFnS
vznik	vznik	k1gInSc4
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Článků	článek	k1gInPc2
Konfederace	konfederace	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1781	#num#	k4
si	se	k3xPyFc3
každý	každý	k3xTgInSc1
ze	z	k7c2
států	stát	k1gInPc2
Unie	unie	k1gFnSc2
zachoval	zachovat	k5eAaPmAgInS
samostatnou	samostatný	k2eAgFnSc4d1
vnitřní	vnitřní	k2eAgFnSc4d1
a	a	k8xC
ekonomickou	ekonomický	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válka	válka	k1gFnSc1
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
skončila	skončit	k5eAaPmAgFnS
roku	rok	k1gInSc2
1783	#num#	k4
britským	britský	k2eAgNnSc7d1
uznáním	uznání	k1gNnSc7
nového	nový	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1787	#num#	k4
byl	být	k5eAaImAgInS
konfederativní	konfederativní	k2eAgInSc1d1
charakter	charakter	k1gInSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
nahrazen	nahradit	k5eAaPmNgInS
systémem	systém	k1gInSc7
federativním	federativní	k2eAgInSc7d1
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1789	#num#	k4
byl	být	k5eAaImAgInS
schválen	schválit	k5eAaPmNgInS
tzv.	tzv.	kA
Bill	Bill	k1gMnSc1
of	of	k?
Rights	Rights	k1gInSc1
–	–	k?
listina	listina	k1gFnSc1
práv	právo	k1gNnPc2
<g/>
,	,	kIx,
prvních	první	k4xOgInPc2
10	#num#	k4
dodatků	dodatek	k1gInPc2
ústavy	ústava	k1gFnSc2
-	-	kIx~
jehož	jehož	k3xOyRp3gFnSc1
ratifikace	ratifikace	k1gFnSc1
byla	být	k5eAaImAgFnS
ukončena	ukončit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1791	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1789	#num#	k4
byl	být	k5eAaImAgMnS
prvním	první	k4xOgMnSc7
prezidentem	prezident	k1gMnSc7
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
zvolen	zvolit	k5eAaPmNgInS
George	George	k1gInSc1
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
dovedl	dovést	k5eAaPmAgInS
povstaleckou	povstalecký	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
k	k	k7c3
vítězství	vítězství	k1gNnSc3
nad	nad	k7c4
Brity	Brit	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
konce	konec	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
pak	pak	k6eAd1
začala	začít	k5eAaPmAgFnS
územní	územní	k2eAgFnSc1d1
expanze	expanze	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
směrem	směr	k1gInSc7
na	na	k7c4
západ	západ	k1gInSc4
a	a	k8xC
na	na	k7c4
jih	jih	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupně	postupně	k6eAd1
byly	být	k5eAaImAgInP
do	do	k7c2
Unie	unie	k1gFnSc2
přijaty	přijmout	k5eAaPmNgInP
další	další	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
:	:	kIx,
Vermont	Vermont	k1gInSc1
(	(	kIx(
<g/>
1791	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Kentucky	Kentuck	k1gInPc1
(	(	kIx(
<g/>
1792	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Tennessee	Tennessee	k1gFnSc1
(	(	kIx(
<g/>
1796	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Ohio	Ohio	k1gNnSc1
(	(	kIx(
<g/>
1803	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1803	#num#	k4
byla	být	k5eAaImAgFnS
od	od	k7c2
Francie	Francie	k1gFnSc2
odkoupena	odkoupen	k2eAgMnSc4d1
Louisiana	Louisian	k1gMnSc4
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
Koupě	koupě	k1gFnSc1
Louisiany	Louisian	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
malá	malý	k2eAgFnSc1d1
část	část	k1gFnSc1
byla	být	k5eAaImAgFnS
přijata	přijmout	k5eAaPmNgFnS
do	do	k7c2
Unie	unie	k1gFnSc2
jako	jako	k8xS,k8xC
stejnojmenný	stejnojmenný	k2eAgInSc1d1
stát	stát	k1gInSc1
roku	rok	k1gInSc2
1812	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokračující	pokračující	k2eAgInPc1d1
spory	spor	k1gInPc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
námořního	námořní	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
a	a	k8xC
rozdělení	rozdělení	k1gNnSc2
sfér	sféra	k1gFnPc2
vlivu	vliv	k1gInSc2
na	na	k7c6
severoamerickém	severoamerický	k2eAgInSc6d1
kontinentu	kontinent	k1gInSc6
mezi	mezi	k7c7
Spojenými	spojený	k2eAgInPc7d1
státy	stát	k1gInPc7
a	a	k8xC
Spojeným	spojený	k2eAgNnSc7d1
královstvím	království	k1gNnSc7
vedly	vést	k5eAaImAgFnP
k	k	k7c3
britsko-americké	britsko-americký	k2eAgFnSc3d1
válce	válka	k1gFnSc3
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
je	být	k5eAaImIp3nS
někdy	někdy	k6eAd1
nazývána	nazývat	k5eAaImNgFnS
druhou	druhý	k4xOgFnSc7
válkou	válka	k1gFnSc7
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
a	a	k8xC
trvala	trvat	k5eAaImAgFnS
mezi	mezi	k7c7
lety	léto	k1gNnPc7
1812	#num#	k4
a	a	k8xC
1814	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
válku	válka	k1gFnSc4
vyhlásily	vyhlásit	k5eAaPmAgInP
v	v	k7c6
přesvědčení	přesvědčení	k1gNnSc6
<g/>
,	,	kIx,
že	že	k8xS
budou	být	k5eAaImBp3nP
moci	moct	k5eAaImF
využít	využít	k5eAaPmF
zaneprázdněnosti	zaneprázdněnost	k1gFnPc4
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
zaměstnávala	zaměstnávat	k5eAaImAgFnS
válka	válka	k1gFnSc1
s	s	k7c7
Napoleonem	napoleon	k1gInSc7
<g/>
,	,	kIx,
a	a	k8xC
zabrat	zabrat	k5eAaPmF
zbytek	zbytek	k1gInSc4
britského	britský	k2eAgNnSc2d1
panství	panství	k1gNnSc2
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neuspěly	uspět	k5eNaPmAgInP
však	však	k9
<g/>
;	;	kIx,
jejich	jejich	k3xOp3gInSc1
vpád	vpád	k1gInSc1
do	do	k7c2
Kanady	Kanada	k1gFnSc2
skončil	skončit	k5eAaPmAgMnS
debaklem	debakl	k1gInSc7
a	a	k8xC
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
postupně	postupně	k6eAd1
začalo	začít	k5eAaPmAgNnS
na	na	k7c4
americký	americký	k2eAgInSc4d1
kontinent	kontinent	k1gInSc4
přesouvat	přesouvat	k5eAaImF
další	další	k2eAgFnPc4d1
síly	síla	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britové	Brit	k1gMnPc1
ovšem	ovšem	k9
o	o	k7c4
tuto	tento	k3xDgFnSc4
válku	válka	k1gFnSc4
nestáli	stát	k5eNaImAgMnP
a	a	k8xC
nehodlali	hodlat	k5eNaImAgMnP
investovat	investovat	k5eAaBmF
prostředky	prostředek	k1gInPc4
do	do	k7c2
tak	tak	k6eAd1
nejistého	jistý	k2eNgInSc2d1
podniku	podnik	k1gInSc2
<g/>
,	,	kIx,
jakým	jaký	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
by	by	kYmCp3nS
byl	být	k5eAaImAgInS
pokus	pokus	k1gInSc1
o	o	k7c4
znovudobytí	znovudobytí	k1gNnSc4
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
obě	dva	k4xCgFnPc1
strany	strana	k1gFnPc1
se	se	k3xPyFc4
tedy	tedy	k9
dohodly	dohodnout	k5eAaPmAgInP
na	na	k7c6
návratu	návrat	k1gInSc6
ke	k	k7c3
statu	status	k1gInSc3
quo	quo	k?
ante	ante	k6eAd1
doplněném	doplněný	k2eAgInSc6d1
dohodami	dohoda	k1gFnPc7
řešícími	řešící	k2eAgFnPc7d1
největší	veliký	k2eAgFnSc4d3
kontroverze	kontroverze	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Obnovení	obnovení	k1gNnSc1
předválečného	předválečný	k2eAgInSc2d1
stavu	stav	k1gInSc2
fakticky	fakticky	k6eAd1
posílilo	posílit	k5eAaPmAgNnS
postaveni	postavit	k5eAaPmNgMnP
USA	USA	kA
a	a	k8xC
nakrátko	nakrátko	k6eAd1
zapříčinilo	zapříčinit	k5eAaPmAgNnS
faktickou	faktický	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
jedné	jeden	k4xCgFnSc2
strany	strana	k1gFnSc2
(	(	kIx(
<g/>
dnes	dnes	k6eAd1
označována	označovat	k5eAaImNgFnS
jako	jako	k8xS,k8xC
demokraté-republikáni	demokraté-republikán	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
období	období	k1gNnSc1
je	být	k5eAaImIp3nS
někdy	někdy	k6eAd1
nazýváno	nazýván	k2eAgNnSc1d1
era	era	k?
of	of	k?
good	good	k6eAd1
feelings	feelings	k6eAd1
neboli	neboli	k8xC
érou	éra	k1gFnSc7
dobré	dobrý	k2eAgFnSc2d1
shody	shoda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
jednotlivých	jednotlivý	k2eAgNnPc2d1
teritorií	teritorium	k1gNnPc2
byly	být	k5eAaImAgInP
postupně	postupně	k6eAd1
vytvářeny	vytvářet	k5eAaImNgInP,k5eAaPmNgInP
další	další	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
:	:	kIx,
Indiana	Indiana	k1gFnSc1
(	(	kIx(
<g/>
1816	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Mississippi	Mississippi	k1gFnSc1
(	(	kIx(
<g/>
1817	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Illinois	Illinois	k1gFnSc1
(	(	kIx(
<g/>
1818	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Alabama	Alabama	k1gFnSc1
(	(	kIx(
<g/>
1819	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Maine	Main	k1gMnSc5
(	(	kIx(
<g/>
1820	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Missouri	Missouri	k1gFnSc1
(	(	kIx(
<g/>
1821	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1819	#num#	k4
získaly	získat	k5eAaPmAgFnP
USA	USA	kA
od	od	k7c2
Španělska	Španělsko	k1gNnSc2
Floridu	Florida	k1gFnSc4
(	(	kIx(
<g/>
stát	stát	k1gInSc1
od	od	k7c2
1845	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1845	#num#	k4
byl	být	k5eAaImAgInS
anektován	anektován	k2eAgInSc1d1
Texas	Texas	k1gInSc1
a	a	k8xC
po	po	k7c6
americko-mexické	americko-mexický	k2eAgFnSc6d1
válce	válka	k1gFnSc6
z	z	k7c2
let	léto	k1gNnPc2
1846	#num#	k4
<g/>
–	–	k?
<g/>
1848	#num#	k4
Alta	Altum	k1gNnSc2
California	Californium	k1gNnSc2
a	a	k8xC
Santa	Santo	k1gNnSc2
Fe	Fe	k1gFnPc2
de	de	k?
Nuevo	Nuevo	k1gNnSc1
México	México	k1gMnSc1
(	(	kIx(
<g/>
Kalifornie	Kalifornie	k1gFnSc1
jako	jako	k8xC,k8xS
stát	stát	k1gInSc1
od	od	k7c2
1850	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
počátku	počátek	k1gInSc2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
byly	být	k5eAaImAgFnP
do	do	k7c2
Unie	unie	k1gFnSc2
přijaty	přijmout	k5eAaPmNgInP
další	další	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
:	:	kIx,
Arkansas	Arkansas	k1gInSc1
(	(	kIx(
<g/>
1836	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Michigan	Michigan	k1gInSc1
(	(	kIx(
<g/>
1837	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Iowa	Iowa	k1gMnSc1
(	(	kIx(
<g/>
1846	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Wisconsin	Wisconsin	k1gMnSc1
(	(	kIx(
<g/>
1848	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Minnesota	Minnesota	k1gFnSc1
(	(	kIx(
<g/>
1858	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Oregon	Oregon	k1gMnSc1
(	(	kIx(
<g/>
1859	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Americká	americký	k2eAgFnSc1d1
občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
V	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
začaly	začít	k5eAaPmAgInP
narůstat	narůstat	k5eAaImF
rozpory	rozpor	k1gInPc4
mezi	mezi	k7c7
americkým	americký	k2eAgInSc7d1
severem	sever	k1gInSc7
a	a	k8xC
jihem	jih	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sever	sever	k1gInSc1
byl	být	k5eAaImAgInS
lidnatý	lidnatý	k2eAgMnSc1d1
(	(	kIx(
<g/>
19	#num#	k4
milionů	milion	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
industriální	industriální	k2eAgMnSc1d1
a	a	k8xC
odmítal	odmítat	k5eAaImAgMnS
otroctví	otroctví	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jih	jih	k1gInSc1
byl	být	k5eAaImAgInS
méně	málo	k6eAd2
osídlený	osídlený	k2eAgMnSc1d1
(	(	kIx(
<g/>
8	#num#	k4
milionů	milion	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
z	z	k7c2
toho	ten	k3xDgNnSc2
polovina	polovina	k1gFnSc1
byli	být	k5eAaImAgMnP
černí	černit	k5eAaImIp3nP
otroci	otrok	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zemědělský	zemědělský	k2eAgInSc1d1
(	(	kIx(
<g/>
zejm.	zejm.	k?
plantáže	plantáž	k1gFnSc2
bavlny	bavlna	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
žil	žít	k5eAaImAgMnS
z	z	k7c2
otroctví	otroctví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hnutí	hnutí	k1gNnSc1
<g/>
,	,	kIx,
založené	založený	k2eAgNnSc1d1
na	na	k7c6
protestantské	protestantský	k2eAgFnSc6d1
teologii	teologie	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
otroctví	otroctví	k1gNnSc1
odmítalo	odmítat	k5eAaImAgNnS
<g/>
,	,	kIx,
se	se	k3xPyFc4
nazývalo	nazývat	k5eAaImAgNnS
abolicionismus	abolicionismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozvolna	pozvolna	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
severu	sever	k1gInSc6
otázka	otázka	k1gFnSc1
otroctví	otroctví	k1gNnPc4
ovšem	ovšem	k9
začala	začít	k5eAaPmAgFnS
politizovat	politizovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpůrci	odpůrce	k1gMnPc1
otroctví	otroctví	k1gNnSc4
založili	založit	k5eAaPmAgMnP
i	i	k9
novou	nový	k2eAgFnSc4d1
Republikánskou	republikánský	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
ústřední	ústřední	k2eAgFnSc7d1
osobností	osobnost	k1gFnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Abraham	Abraham	k1gMnSc1
Lincoln	Lincoln	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
1860	#num#	k4
zvolen	zvolit	k5eAaPmNgInS
prezidentem	prezident	k1gMnSc7
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
secesi	secese	k1gFnSc3
(	(	kIx(
<g/>
odtržení	odtržení	k1gNnSc1
<g/>
)	)	kIx)
jedenácti	jedenáct	k4xCc2
jižních	jižní	k2eAgInPc2d1
států	stát	k1gInPc2
(	(	kIx(
<g/>
Jižní	jižní	k2eAgFnSc1d1
Karolíny	Karolína	k1gFnPc1
<g/>
,	,	kIx,
Mississippi	Mississippi	k1gFnPc1
<g/>
,	,	kIx,
Floridy	Florida	k1gFnPc1
<g/>
,	,	kIx,
Alabamy	Alabam	k1gInPc1
<g/>
,	,	kIx,
Georgie	Georgie	k1gFnPc1
<g/>
,	,	kIx,
Louisiany	Louisiana	k1gFnPc1
<g/>
,	,	kIx,
Texasu	Texas	k1gInSc6
<g/>
,	,	kIx,
Virginie	Virginie	k1gFnSc5
<g/>
,	,	kIx,
Arkansasu	Arkansas	k1gInSc2
<g/>
,	,	kIx,
Severní	severní	k2eAgFnSc2d1
Karolíny	Karolína	k1gFnSc2
a	a	k8xC
Tennessee	Tennesse	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
vyhlásily	vyhlásit	k5eAaPmAgInP
Konfederované	konfederovaný	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
nakonec	nakonec	k6eAd1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
občanské	občanský	k2eAgFnSc3d1
válce	válka	k1gFnSc3
(	(	kIx(
<g/>
známé	známá	k1gFnSc2
též	též	k9
jako	jako	k8xS,k8xC
válka	válka	k1gFnSc1
Severu	sever	k1gInSc2
proti	proti	k7c3
Jihu	jih	k1gInSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
začala	začít	k5eAaPmAgFnS
12	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1861	#num#	k4
útokem	útok	k1gInSc7
konfederačních	konfederační	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
na	na	k7c4
americkou	americký	k2eAgFnSc4d1
pevnost	pevnost	k1gFnSc4
Fort	Fort	k?
Sumter	Sumtra	k1gFnPc2
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Karolíně	Karolína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpočátku	zpočátku	k6eAd1
dosáhly	dosáhnout	k5eAaPmAgFnP
jižní	jižní	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
solidních	solidní	k2eAgInPc2d1
úspěchů	úspěch	k1gInPc2
díky	díky	k7c3
zkušenosti	zkušenost	k1gFnSc3
svého	svůj	k3xOyFgMnSc2
velitele	velitel	k1gMnSc2
Roberta	Robert	k1gMnSc2
Edwarda	Edward	k1gMnSc2
Leea	Leeus	k1gMnSc2
<g/>
,	,	kIx,
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
vojska	vojsko	k1gNnSc2
však	však	k9
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
1863	#num#	k4
poražena	poražen	k2eAgFnSc1d1
v	v	k7c6
klíčové	klíčový	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
u	u	k7c2
Gettysburgu	Gettysburg	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armádu	armáda	k1gFnSc4
Unie	unie	k1gFnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
Severu	sever	k1gInSc2
<g/>
,	,	kIx,
nakonec	nakonec	k6eAd1
přivedl	přivést	k5eAaPmAgMnS
k	k	k7c3
vítězství	vítězství	k1gNnSc3
Ulysses	Ulyssesa	k1gFnPc2
S.	S.	kA
Grant	grant	k1gInSc1
(	(	kIx(
<g/>
budoucí	budoucí	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
1869	#num#	k4
<g/>
–	–	k?
<g/>
1877	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válka	válka	k1gFnSc1
trvala	trvat	k5eAaImAgFnS
do	do	k7c2
roku	rok	k1gInSc2
1865	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vítězství	vítězství	k1gNnSc6
Severu	sever	k1gInSc2
musely	muset	k5eAaImAgInP
všechny	všechen	k3xTgInPc1
jižní	jižní	k2eAgInPc1d1
státy	stát	k1gInPc1
ratifikovat	ratifikovat	k5eAaBmF
třináctý	třináctý	k4xOgInSc1
dodatek	dodatek	k1gInSc1
ústavy	ústava	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
otroctví	otroctví	k1gNnSc4
zakazoval	zakazovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězství	vítězství	k1gNnSc1
to	ten	k3xDgNnSc1
však	však	k9
nebylo	být	k5eNaImAgNnS
zdaleka	zdaleka	k6eAd1
definitivní	definitivní	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejenže	nejenže	k6eAd1
byl	být	k5eAaImAgMnS
Lincoln	Lincoln	k1gMnSc1
zavražděn	zavraždit	k5eAaPmNgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
k	k	k7c3
moci	moc	k1gFnSc3
se	se	k3xPyFc4
na	na	k7c6
jihu	jih	k1gInSc6
dostala	dostat	k5eAaPmAgFnS
frakce	frakce	k1gFnSc1
Demokratické	demokratický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
zvaná	zvaný	k2eAgFnSc1d1
Redeemers	Redeemers	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
hlásala	hlásat	k5eAaImAgFnS
nadřazenost	nadřazenost	k1gFnSc4
bělochů	běloch	k1gMnPc2
a	a	k8xC
zavedla	zavést	k5eAaPmAgFnS
rasovou	rasový	k2eAgFnSc4d1
segregaci	segregace	k1gFnSc4
(	(	kIx(
<g/>
tzv.	tzv.	kA
Zákony	zákon	k1gInPc1
Jima	Jimus	k1gMnSc2
Crowa	Crowus	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
O	o	k7c6
odstranění	odstranění	k1gNnSc6
jejích	její	k3xOp3gInPc2
posledních	poslední	k2eAgInPc2d1
zbytků	zbytek	k1gInPc2
se	se	k3xPyFc4
vedl	vést	k5eAaImAgInS
tuhý	tuhý	k2eAgInSc1d1
politický	politický	k2eAgInSc1d1
boj	boj	k1gInSc1
až	až	k9
do	do	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válka	válka	k1gFnSc1
nicméně	nicméně	k8xC
znatelně	znatelně	k6eAd1
posílila	posílit	k5eAaPmAgFnS
federální	federální	k2eAgFnSc1d1
moc	moc	k1gFnSc1
a	a	k8xC
umožnila	umožnit	k5eAaPmAgFnS
další	další	k2eAgFnSc4d1
expanzi	expanze	k1gFnSc4
<g/>
..	..	k?
</s>
<s>
Posilování	posilování	k1gNnSc1
velmocenského	velmocenský	k2eAgNnSc2d1
postavení	postavení	k1gNnSc2
</s>
<s>
Bitva	bitva	k1gFnSc1
v	v	k7c6
Manilské	manilský	k2eAgFnSc6d1
zátoce	zátoka	k1gFnSc6
během	během	k7c2
Španělsko-americké	španělsko-americký	k2eAgFnSc2d1
války	válka	k1gFnSc2
roku	rok	k1gInSc2
1898	#num#	k4
</s>
<s>
Mohutný	mohutný	k2eAgInSc1d1
hospodářský	hospodářský	k2eAgInSc1d1
rozvoj	rozvoj	k1gInSc1
po	po	k7c6
skončení	skončení	k1gNnSc6
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
způsobil	způsobit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
USA	USA	kA
již	již	k6eAd1
roku	rok	k1gInSc2
1872	#num#	k4
staly	stát	k5eAaPmAgFnP
hospodářsky	hospodářsky	k6eAd1
nejsilnější	silný	k2eAgFnSc7d3
zemí	zem	k1gFnSc7
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c7
tímto	tento	k3xDgInSc7
rozmachem	rozmach	k1gInSc7
na	na	k7c6
konci	konec	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
začátku	začátek	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
stáli	stát	k5eAaImAgMnP
průmyslníci	průmyslník	k1gMnPc1
jako	jako	k8xC,k8xS
Cornelius	Cornelius	k1gMnSc1
Vanderbilt	Vanderbilt	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
D.	D.	kA
Rockefeller	Rockefeller	k1gMnSc1
<g/>
,	,	kIx,
Henry	Henry	k1gMnSc1
Ford	ford	k1gInSc1
a	a	k8xC
Andrew	Andrew	k1gFnSc1
Carnegie	Carnegie	k1gFnSc1
<g/>
,	,	kIx,
či	či	k8xC
bankéř	bankéř	k1gMnSc1
John	John	k1gMnSc1
Pierpont	Pierpont	k1gMnSc1
Morgan	morgan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomický	ekonomický	k2eAgInSc1d1
boom	boom	k1gInSc1
byl	být	k5eAaImAgInS
doprovázen	doprovázet	k5eAaImNgInS
další	další	k2eAgFnSc7d1
expanzí	expanze	k1gFnSc7
na	na	k7c4
západ	západ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
před	před	k7c7
občanskou	občanský	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
a	a	k8xC
poté	poté	k6eAd1
až	až	k9
do	do	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
zde	zde	k6eAd1
vzniklo	vzniknout	k5eAaPmAgNnS
15	#num#	k4
dalších	další	k2eAgInPc2d1
unijních	unijní	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
:	:	kIx,
Kansas	Kansas	k1gInSc1
(	(	kIx(
<g/>
1861	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Západní	západní	k2eAgFnPc1d1
Virginie	Virginie	k1gFnPc1
(	(	kIx(
<g/>
1863	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nevada	Nevada	k1gFnSc1
(	(	kIx(
<g/>
1864	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nebraska	Nebraska	k1gFnSc1
(	(	kIx(
<g/>
1867	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Colorado	Colorado	k1gNnSc1
(	(	kIx(
<g/>
1876	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Severní	severní	k2eAgFnSc1d1
Dakota	Dakota	k1gFnSc1
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc1d1
Dakota	Dakota	k1gFnSc1
<g/>
,	,	kIx,
Montana	Montana	k1gFnSc1
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
(	(	kIx(
<g/>
1889	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Idaho	Ida	k1gMnSc4
<g/>
,	,	kIx,
Wyoming	Wyoming	k1gInSc1
(	(	kIx(
<g/>
1890	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Utah	Utah	k1gInSc1
(	(	kIx(
<g/>
1896	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Oklahoma	Oklahoma	k1gFnSc1
(	(	kIx(
<g/>
1907	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Arizona	Arizona	k1gFnSc1
a	a	k8xC
Nové	Nové	k2eAgNnSc1d1
Mexiko	Mexiko	k1gNnSc1
(	(	kIx(
<g/>
1912	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1867	#num#	k4
odkoupila	odkoupit	k5eAaPmAgFnS
americká	americký	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
od	od	k7c2
Ruského	ruský	k2eAgNnSc2d1
impéria	impérium	k1gNnSc2
rozsáhlé	rozsáhlý	k2eAgNnSc4d1
území	území	k1gNnSc4
Aljašky	Aljaška	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
pak	pak	k6eAd1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
expandovaly	expandovat	k5eAaImAgInP
i	i	k9
mimo	mimo	k7c4
vlastní	vlastní	k2eAgFnSc4d1
americkou	americký	k2eAgFnSc4d1
pevninu	pevnina	k1gFnSc4
<g/>
,	,	kIx,
především	především	k6eAd1
do	do	k7c2
Karibiku	Karibik	k1gInSc2
a	a	k8xC
do	do	k7c2
Tichomoří	Tichomoří	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězství	vítězství	k1gNnSc1
ve	v	k7c6
španělsko-americké	španělsko-americký	k2eAgFnSc6d1
válce	válka	k1gFnSc6
a	a	k8xC
další	další	k2eAgFnSc2d1
intervence	intervence	k1gFnSc2
měly	mít	k5eAaImAgFnP
několik	několik	k4yIc4
důsledků	důsledek	k1gInPc2
<g/>
:	:	kIx,
protektorát	protektorát	k1gInSc1
nad	nad	k7c7
Portorikem	Portorico	k1gNnSc7
<g/>
,	,	kIx,
Kubou	Kuba	k1gFnSc7
a	a	k8xC
ostrovem	ostrov	k1gInSc7
Guam	Guama	k1gFnPc2
<g/>
,	,	kIx,
obsazení	obsazení	k1gNnSc1
Filipín	Filipíny	k1gFnPc2
<g/>
,	,	kIx,
anexe	anexe	k1gFnSc1
Havajských	havajský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
roku	rok	k1gInSc2
1898	#num#	k4
<g/>
,	,	kIx,
rozdělení	rozdělení	k1gNnSc1
ostrovů	ostrov	k1gInPc2
Samoa	Samoa	k1gFnSc1
s	s	k7c7
Německou	německý	k2eAgFnSc7d1
říší	říš	k1gFnSc7
roku	rok	k1gInSc2
1899	#num#	k4
a	a	k8xC
kontrola	kontrola	k1gFnSc1
nad	nad	k7c7
průplavovým	průplavový	k2eAgNnSc7d1
pásmem	pásmo	k1gNnSc7
v	v	k7c6
Panamě	Panama	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
staly	stát	k5eAaPmAgInP
jednou	jednou	k6eAd1
ze	z	k7c2
světových	světový	k2eAgFnPc2d1
velmocí	velmoc	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
svého	svůj	k3xOyFgInSc2
již	již	k6eAd1
velkého	velký	k2eAgInSc2d1
vlivu	vliv	k1gInSc2
na	na	k7c6
západní	západní	k2eAgFnSc6d1
polokouli	polokoule	k1gFnSc6
<g/>
,	,	kIx,
resp.	resp.	kA
na	na	k7c6
obou	dva	k4xCgInPc6
kontinentech	kontinent	k1gInPc6
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
však	však	k9
USA	USA	kA
ve	v	k7c6
světové	světový	k2eAgFnSc6d1
politice	politika	k1gFnSc6
až	až	k9
do	do	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
angažovaly	angažovat	k5eAaBmAgFnP
jen	jen	k9
poměrně	poměrně	k6eAd1
málo	málo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
politika	politika	k1gFnSc1
byla	být	k5eAaImAgFnS
do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
izolacionistická	izolacionistický	k2eAgFnSc1d1
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
Monroeova	Monroeův	k2eAgFnSc1d1
doktrína	doktrína	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zásadní	zásadní	k2eAgFnSc4d1
roli	role	k1gFnSc4
při	při	k7c6
překonávání	překonávání	k1gNnSc6
amerického	americký	k2eAgInSc2d1
izolacionismu	izolacionismus	k1gInSc2
sehrál	sehrát	k5eAaPmAgMnS
prezident	prezident	k1gMnSc1
Woodrow	Woodrow	k1gMnSc1
Wilson	Wilson	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Američané	Američan	k1gMnPc1
se	se	k3xPyFc4
díky	díky	k7c3
němu	on	k3xPp3gNnSc3
v	v	k7c6
roce	rok	k1gInSc6
1917	#num#	k4
zapojili	zapojit	k5eAaPmAgMnP
na	na	k7c6
straně	strana	k1gFnSc6
Dohody	dohoda	k1gFnSc2
do	do	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wilson	Wilson	k1gMnSc1
svou	svůj	k3xOyFgFnSc7
koncepcí	koncepce	k1gFnSc7
práva	právo	k1gNnSc2
na	na	k7c4
sebeurčení	sebeurčení	k1gNnSc4
národů	národ	k1gInPc2
též	též	k9
významně	významně	k6eAd1
ovlivnil	ovlivnit	k5eAaPmAgInS
podobu	podoba	k1gFnSc4
Evropy	Evropa	k1gFnSc2
po	po	k7c6
konci	konec	k1gInSc6
války	válka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
přinesla	přinést	k5eAaPmAgFnS
další	další	k2eAgInSc4d1
mohutný	mohutný	k2eAgInSc4d1
rozmach	rozmach	k1gInSc4
hospodářství	hospodářství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Americkým	americký	k2eAgNnSc7d1
sebevědomím	sebevědomí	k1gNnSc7
zacloumal	zacloumat	k5eAaPmAgInS
až	až	k6eAd1
krach	krach	k1gInSc1
na	na	k7c6
newyorské	newyorský	k2eAgFnSc6d1
burze	burza	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Ten	ten	k3xDgInSc1
způsobil	způsobit	k5eAaPmAgInS
celosvětovou	celosvětový	k2eAgFnSc4d1
ekonomickou	ekonomický	k2eAgFnSc4d1
krizi	krize	k1gFnSc4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
katalyzovala	katalyzovat	k5eAaImAgFnS,k5eAaPmAgFnS,k5eAaBmAgFnS
zejména	zejména	k9
politický	politický	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
v	v	k7c6
Evropě	Evropa	k1gFnSc6
(	(	kIx(
<g/>
pomohla	pomoct	k5eAaPmAgFnS
zvláště	zvláště	k6eAd1
vzestupu	vzestup	k1gInSc2
fašismu	fašismus	k1gInSc2
a	a	k8xC
nacismu	nacismus	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
samotných	samotný	k2eAgInPc6d1
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
vedla	vést	k5eAaImAgFnS
k	k	k7c3
opuštění	opuštění	k1gNnSc3
čisté	čistá	k1gFnSc2
„	„	k?
<g/>
laissez	laissez	k1gMnSc1
faire	fair	k1gInSc5
<g/>
“	“	k?
ekonomické	ekonomický	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
a	a	k8xC
větším	veliký	k2eAgInPc3d2
státním	státní	k2eAgInPc3d1
zásahům	zásah	k1gInPc3
do	do	k7c2
ekonomiky	ekonomika	k1gFnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
plánu	plán	k1gInSc2
zvaného	zvaný	k2eAgInSc2d1
New	New	k1gFnSc7
Deal	Dealum	k1gNnPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
připravil	připravit	k5eAaPmAgMnS
Demokrat	demokrat	k1gMnSc1
Franklin	Franklina	k1gFnPc2
Delano	Delana	k1gFnSc5
Roosevelt	Roosevelt	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
Plán	plán	k1gInSc1
měl	mít	k5eAaImAgInS
své	svůj	k3xOyFgFnPc4
kritiky	kritika	k1gFnPc4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
ale	ale	k8xC
obzvláště	obzvláště	k6eAd1
dobře	dobře	k6eAd1
se	se	k3xPyFc4
hodil	hodit	k5eAaPmAgInS,k5eAaImAgInS
pro	pro	k7c4
válečný	válečný	k2eAgInSc4d1
stav	stav	k1gInSc4
<g/>
,	,	kIx,
do	do	k7c2
něhož	jenž	k3xRgInSc2
se	se	k3xPyFc4
USA	USA	kA
dostaly	dostat	k5eAaPmAgInP
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
Japonsko	Japonsko	k1gNnSc1
napadlo	napadnout	k5eAaPmAgNnS
jejich	jejich	k3xOp3gFnSc4
základnu	základna	k1gFnSc4
Pearl	Pearl	k1gMnSc1
Harbor	Harbor	k1gMnSc1
v	v	k7c6
Pacifiku	Pacifik	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
Američané	Američan	k1gMnPc1
následně	následně	k6eAd1
vyhlásili	vyhlásit	k5eAaPmAgMnP
válku	válka	k1gFnSc4
nejen	nejen	k6eAd1
Japonsku	Japonsko	k1gNnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
jeho	jeho	k3xOp3gMnPc1
spojenci	spojenec	k1gMnPc1
<g/>
,	,	kIx,
nacistickému	nacistický	k2eAgNnSc3d1
Německu	Německo	k1gNnSc3
<g/>
,	,	kIx,
a	a	k8xC
vstoupili	vstoupit	k5eAaPmAgMnP
tak	tak	k9
do	do	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
(	(	kIx(
<g/>
již	již	k6eAd1
předtím	předtím	k6eAd1
ovšem	ovšem	k9
hospodářsky	hospodářsky	k6eAd1
podporovaly	podporovat	k5eAaImAgInP
Spojence	spojenka	k1gFnSc3
podle	podle	k7c2
Zákona	zákon	k1gInSc2
o	o	k7c6
půjčce	půjčka	k1gFnSc6
a	a	k8xC
pronájmu	pronájem	k1gInSc6
z	z	k7c2
roku	rok	k1gInSc2
1941	#num#	k4
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
USA	USA	kA
se	se	k3xPyFc4
podílely	podílet	k5eAaImAgInP
přímo	přímo	k6eAd1
na	na	k7c6
poražení	poražení	k1gNnSc3
Německa	Německo	k1gNnSc2
na	na	k7c6
evropském	evropský	k2eAgInSc6d1
kontinentě	kontinent	k1gInSc6
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
Den	den	k1gInSc4
D	D	kA
a	a	k8xC
bitva	bitva	k1gFnSc1
o	o	k7c6
Normandii	Normandie	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Japonce	Japonka	k1gFnSc3
pak	pak	k6eAd1
porazily	porazit	k5eAaPmAgFnP
i	i	k9
za	za	k7c2
pomoci	pomoc	k1gFnSc2
zcela	zcela	k6eAd1
nové	nový	k2eAgFnSc2d1
zbraně	zbraň	k1gFnSc2
–	–	k?
atomové	atomový	k2eAgFnSc2d1
bomby	bomba	k1gFnSc2
–	–	k?
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
svrhly	svrhnout	k5eAaPmAgFnP
na	na	k7c4
japonská	japonský	k2eAgNnPc4d1
města	město	k1gNnPc4
Hirošima	Hirošima	k1gFnSc1
a	a	k8xC
Nagasaki	Nagasaki	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězné	vítězný	k2eAgFnPc4d1
mocnosti	mocnost	k1gFnPc4
<g/>
,	,	kIx,
tedy	tedy	k9
USA	USA	kA
<g/>
,	,	kIx,
Británie	Británie	k1gFnSc1
a	a	k8xC
Sovětský	sovětský	k2eAgInSc1d1
svaz	svaz	k1gInSc1
<g/>
,	,	kIx,
pak	pak	k6eAd1
dohodami	dohoda	k1gFnPc7
ekonomickými	ekonomický	k2eAgFnPc7d1
(	(	kIx(
<g/>
konference	konference	k1gFnSc2
v	v	k7c4
Bretton	Bretton	k1gInSc4
Woods	Woods	k1gInSc4
<g/>
)	)	kIx)
i	i	k9
politickými	politický	k2eAgFnPc7d1
(	(	kIx(
<g/>
Jaltská	jaltský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
<g/>
)	)	kIx)
narýsovaly	narýsovat	k5eAaPmAgFnP
podobu	podoba	k1gFnSc4
poválečného	poválečný	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Studená	studený	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
Martin	Martin	k1gMnSc1
Luther	Luthra	k1gFnPc2
King	King	k1gMnSc1
na	na	k7c6
manifestaci	manifestace	k1gFnSc6
proti	proti	k7c3
válce	válka	k1gFnSc3
ve	v	k7c6
Vietnamu	Vietnam	k1gInSc6
roku	rok	k1gInSc2
1967	#num#	k4
</s>
<s>
Ronald	Ronald	k1gMnSc1
Reagan	Reagan	k1gMnSc1
a	a	k8xC
Michail	Michail	k1gMnSc1
Gorbačov	Gorbačov	k1gInSc4
v	v	k7c6
Ženevě	Ženeva	k1gFnSc6
roku	rok	k1gInSc2
1985	#num#	k4
</s>
<s>
Za	za	k7c2
války	válka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
přímo	přímo	k6eAd1
nezasáhla	zasáhnout	k5eNaPmAgFnS
území	území	k1gNnSc2
kontinentálních	kontinentální	k2eAgInPc2d1
USA	USA	kA
<g/>
,	,	kIx,
se	se	k3xPyFc4
americká	americký	k2eAgFnSc1d1
průmyslová	průmyslový	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Evropa	Evropa	k1gFnSc1
byla	být	k5eAaImAgFnS
hospodářsky	hospodářsky	k6eAd1
na	na	k7c6
kolenou	koleno	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
zřejmé	zřejmý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
klíčovou	klíčový	k2eAgFnSc7d1
mocností	mocnost	k1gFnSc7
Západu	západ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
pomohly	pomoct	k5eAaPmAgInP
těžce	těžce	k6eAd1
postižené	postižený	k2eAgFnSc6d1
Západní	západní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
včetně	včetně	k7c2
Západního	západní	k2eAgNnSc2d1
Německa	Německo	k1gNnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
Marshallova	Marshallův	k2eAgInSc2d1
plánu	plán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západoevropské	západoevropský	k2eAgInPc1d1
státy	stát	k1gInPc1
díky	díky	k7c3
němu	on	k3xPp3gNnSc3
nejen	nejen	k6eAd1
poměrně	poměrně	k6eAd1
rychle	rychle	k6eAd1
překonaly	překonat	k5eAaPmAgFnP
nejhorší	zlý	k2eAgInPc4d3
problémy	problém	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
navíc	navíc	k6eAd1
nastoupily	nastoupit	k5eAaPmAgFnP
cestu	cesta	k1gFnSc4
k	k	k7c3
výraznému	výrazný	k2eAgInSc3d1
hospodářskému	hospodářský	k2eAgInSc3d1
růstu	růst	k1gInSc3
a	a	k8xC
k	k	k7c3
doposud	doposud	k6eAd1
nebývalému	nebývalý	k2eAgInSc3d1,k2eNgInSc3d1
blahobytu	blahobyt	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
válce	válka	k1gFnSc6
dostaly	dostat	k5eAaPmAgFnP
USA	USA	kA
též	též	k9
do	do	k7c2
správy	správa	k1gFnSc2
poručenská	poručenský	k2eAgNnPc1d1
území	území	k1gNnPc1
OSN	OSN	kA
v	v	k7c6
Tichém	tichý	k2eAgInSc6d1
oceáně	oceán	k1gInSc6
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
předtím	předtím	k6eAd1
pod	pod	k7c7
správou	správa	k1gFnSc7
Japonska	Japonsko	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
většina	většina	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
se	se	k3xPyFc4
později	pozdě	k6eAd2
stala	stát	k5eAaPmAgFnS
volně	volně	k6eAd1
přidruženými	přidružený	k2eAgInPc7d1
územími	území	k1gNnPc7
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
(	(	kIx(
<g/>
Marshallovy	Marshallův	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
<g/>
,	,	kIx,
Severní	severní	k2eAgFnPc1d1
Mariany	Mariana	k1gFnPc1
<g/>
,	,	kIx,
Mikronésie	Mikronésie	k1gFnPc1
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1959	#num#	k4
byly	být	k5eAaImAgInP
vytvořeny	vytvořen	k2eAgInPc1d1
dva	dva	k4xCgInPc1
–	–	k?
dosud	dosud	k6eAd1
poslední	poslední	k2eAgInPc1d1
–	–	k?
státy	stát	k1gInPc1
Unie	unie	k1gFnSc1
<g/>
:	:	kIx,
Aljaška	Aljaška	k1gFnSc1
a	a	k8xC
Havaj	Havaj	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
sfér	sféra	k1gFnPc2
vlivu	vliv	k1gInSc2
mezi	mezi	k7c7
vítěznými	vítězný	k2eAgFnPc7d1
mocnostmi	mocnost	k1gFnPc7
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
proběhlo	proběhnout	k5eAaPmAgNnS
poklidně	poklidně	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
klid	klid	k1gInSc4
nevydržel	vydržet	k5eNaPmAgMnS
dlouho	dlouho	k6eAd1
–	–	k?
po	po	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
vzniklo	vzniknout	k5eAaPmAgNnS
mezi	mezi	k7c7
Spojenými	spojený	k2eAgInPc7d1
státy	stát	k1gInPc7
(	(	kIx(
<g/>
a	a	k8xC
jeho	jeho	k3xOp3gMnPc7
západními	západní	k2eAgMnPc7d1
spojenci	spojenec	k1gMnPc7
<g/>
)	)	kIx)
a	a	k8xC
Sovětským	sovětský	k2eAgInSc7d1
svazem	svaz	k1gInSc7
napětí	napětí	k1gNnSc2
<g/>
,	,	kIx,
kterému	který	k3yQgInSc3,k3yIgInSc3,k3yRgInSc3
se	se	k3xPyFc4
pak	pak	k8xC
říkalo	říkat	k5eAaImAgNnS
studená	studený	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
]	]	kIx)
Toto	tento	k3xDgNnSc1
napětí	napětí	k1gNnSc1
se	se	k3xPyFc4
projevovalo	projevovat	k5eAaImAgNnS
jak	jak	k6eAd1
pozitivně	pozitivně	k6eAd1
<g/>
,	,	kIx,
tedy	tedy	k9
snahou	snaha	k1gFnSc7
o	o	k7c4
ekonomické	ekonomický	k2eAgNnSc4d1
předběhnutí	předběhnutí	k1gNnSc4
soupeřícího	soupeřící	k2eAgInSc2d1
tábora	tábor	k1gInSc2
či	či	k8xC
proslulým	proslulý	k2eAgNnSc7d1
„	„	k?
<g/>
vesmírným	vesmírný	k2eAgInSc7d1
závodem	závod	k1gInSc7
<g/>
“	“	k?
<g/>
,	,	kIx,
v	v	k7c6
jehož	jehož	k3xOyRp3gInSc6
rámci	rámec	k1gInSc6
Sověti	Sovět	k1gMnPc1
vyslali	vyslat	k5eAaPmAgMnP
prvního	první	k4xOgMnSc4
člověka	člověk	k1gMnSc4
do	do	k7c2
kosmu	kosmos	k1gInSc2
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Američané	Američan	k1gMnPc1
přistáli	přistát	k5eAaPmAgMnP,k5eAaImAgMnP
na	na	k7c6
Měsíci	měsíc	k1gInSc6
(	(	kIx(
<g/>
1969	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
občas	občas	k6eAd1
i	i	k9
různými	různý	k2eAgInPc7d1
<g/>
,	,	kIx,
často	často	k6eAd1
velmi	velmi	k6eAd1
krvavými	krvavý	k2eAgFnPc7d1
<g/>
,	,	kIx,
proxy	prox	k1gInPc1
wars	warsa	k1gFnPc2
(	(	kIx(
<g/>
zástupnými	zástupný	k2eAgInPc7d1
konflikty	konflikt	k1gInPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
některých	některý	k3yIgFnPc6
z	z	k7c2
nich	on	k3xPp3gFnPc2
se	se	k3xPyFc4
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
tak	tak	k6eAd1
či	či	k8xC
onak	onak	k6eAd1
přímo	přímo	k6eAd1
podílely	podílet	k5eAaImAgInP
–	–	k?
korejská	korejský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
1950	#num#	k4
<g/>
–	–	k?
<g/>
53	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
karibská	karibský	k2eAgFnSc1d1
krize	krize	k1gFnSc1
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vietnamská	vietnamský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
1959	#num#	k4
<g/>
-	-	kIx~
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zejména	zejména	k6eAd1
vietnamský	vietnamský	k2eAgInSc1d1
konflikt	konflikt	k1gInSc1
vyvolal	vyvolat	k5eAaPmAgInS
pnutí	pnutí	k1gNnSc3
i	i	k9
uvnitř	uvnitř	k7c2
americké	americký	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
odpor	odpor	k1gInSc1
proti	proti	k7c3
této	tento	k3xDgFnSc3
válce	válka	k1gFnSc3
probíhal	probíhat	k5eAaImAgInS
v	v	k7c6
součinnosti	součinnost	k1gFnSc6
s	s	k7c7
hnutím	hnutí	k1gNnSc7
za	za	k7c2
práva	právo	k1gNnSc2
Afroameričanů	Afroameričan	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yRgMnPc4,k3yIgMnPc4,k3yQgMnPc4
vedl	vést	k5eAaImAgMnS
především	především	k6eAd1
reverend	reverend	k1gMnSc1
Martin	Martin	k1gMnSc1
Luther	Luthra	k1gFnPc2
King	King	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
boj	boj	k1gInSc1
<g/>
,	,	kIx,
podpořený	podpořený	k2eAgInSc1d1
zejména	zejména	k9
demokratickým	demokratický	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
Johnem	John	k1gMnSc7
Fitzgeraldem	Fitzgerald	k1gMnSc7
<g />
.	.	kIx.
</s>
<s hack="1">
Kennedym	Kennedym	k1gInSc1
(	(	kIx(
<g/>
zavražděn	zavražděn	k2eAgInSc1d1
1963	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
úspěšný	úspěšný	k2eAgMnSc1d1
<g/>
:	:	kIx,
Zákony	zákon	k1gInPc1
na	na	k7c4
obranu	obrana	k1gFnSc4
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
z	z	k7c2
let	léto	k1gNnPc2
1964	#num#	k4
(	(	kIx(
<g/>
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
zaměstnání	zaměstnání	k1gNnSc2
a	a	k8xC
ubytování	ubytování	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1965	#num#	k4
(	(	kIx(
<g/>
volby	volba	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
1968	#num#	k4
(	(	kIx(
<g/>
trh	trh	k1gInSc1
s	s	k7c7
bydlením	bydlení	k1gNnSc7
<g/>
)	)	kIx)
zakázaly	zakázat	k5eAaPmAgInP
diskriminaci	diskriminace	k1gFnSc3
na	na	k7c6
základě	základ	k1gInSc6
rasy	rasa	k1gFnSc2
nebo	nebo	k8xC
barvy	barva	k1gFnSc2
pleti	pleť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hnutí	hnutí	k1gNnSc1
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
se	se	k3xPyFc4
hovoří	hovořit	k5eAaImIp3nS
o	o	k7c6
kontrakultuře	kontrakultura	k1gFnSc6
<g/>
)	)	kIx)
přinesla	přinést	k5eAaPmAgFnS
však	však	k9
i	i	k9
některé	některý	k3yIgInPc4
sporné	sporný	k2eAgInPc4d1
jevy	jev	k1gInPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
adorace	adorace	k1gFnSc1
drog	droga	k1gFnPc2
a	a	k8xC
promiskuity	promiskuita	k1gFnSc2
<g/>
,	,	kIx,
černošský	černošský	k2eAgInSc1d1
radikalismus	radikalismus	k1gInSc1
(	(	kIx(
<g/>
Černí	černý	k2eAgMnPc1d1
panteři	panter	k1gMnPc1
<g/>
,	,	kIx,
Malcolm	Malcolm	k1gMnSc1
X	X	kA
aj.	aj.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
levicový	levicový	k2eAgInSc1d1
extremismus	extremismus	k1gInSc1
(	(	kIx(
<g/>
obdiv	obdiv	k1gInSc1
k	k	k7c3
násilnické	násilnický	k2eAgFnSc3d1
čínské	čínský	k2eAgFnSc3d1
kulturní	kulturní	k2eAgFnSc3d1
revoluci	revoluce	k1gFnSc3
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
v	v	k7c4
70	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
přineslo	přinést	k5eAaPmAgNnS
protipohyb	protipohyb	k1gInSc4
–	–	k?
neokonzervatismus	neokonzervatismus	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
USA	USA	kA
reprezentovaný	reprezentovaný	k2eAgInSc1d1
především	především	k9
republikánským	republikánský	k2eAgMnSc7d1
prezidentem	prezident	k1gMnSc7
Ronaldem	Ronald	k1gMnSc7
Reaganem	Reagan	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reagan	Reagan	k1gMnSc1
zvolil	zvolit	k5eAaPmAgMnS
tvrdý	tvrdý	k2eAgInSc4d1
postup	postup	k1gInSc4
i	i	k9
vůči	vůči	k7c3
Sovětům	Sověty	k1gInPc3
<g/>
,	,	kIx,
avšak	avšak	k8xC
byl	být	k5eAaImAgInS
to	ten	k3xDgNnSc4
právě	právě	k6eAd1
Reagan	Reagan	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
nakonec	nakonec	k6eAd1
reagoval	reagovat	k5eAaBmAgMnS
vstřícně	vstřícně	k6eAd1
na	na	k7c4
reformátorského	reformátorský	k2eAgMnSc4d1
vůdce	vůdce	k1gMnSc4
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
Michaila	Michail	k1gMnSc2
Gorbačova	Gorbačův	k2eAgMnSc2d1
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
-	-	kIx~
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
sérií	série	k1gFnSc7
dohod	dohoda	k1gFnPc2
o	o	k7c6
odzbrojení	odzbrojení	k1gNnSc6
(	(	kIx(
<g/>
např.	např.	kA
smlouva	smlouva	k1gFnSc1
o	o	k7c6
likvidaci	likvidace	k1gFnSc6
raket	raketa	k1gFnPc2
středního	střední	k2eAgInSc2d1
a	a	k8xC
krátkého	krátký	k2eAgInSc2d1
doletu	dolet	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1987	#num#	k4
<g/>
)	)	kIx)
de	de	k?
facto	facto	k1gNnSc1
studenou	studený	k2eAgFnSc4d1
válku	válka	k1gFnSc4
ukončil	ukončit	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
definitivně	definitivně	k6eAd1
skončila	skončit	k5eAaPmAgFnS
pádem	pád	k1gInSc7
komunismu	komunismus	k1gInSc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
rozpadem	rozpad	k1gInSc7
Sovětského	sovětský	k2eAgInSc2d1
svazu	svaz	k1gInSc2
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
se	se	k3xPyFc4
začalo	začít	k5eAaPmAgNnS
hovořit	hovořit	k5eAaImF
jako	jako	k9
o	o	k7c6
jediné	jediný	k2eAgFnSc6d1
světové	světový	k2eAgFnSc6d1
supervelmoci	supervelmoc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
</s>
<s>
Euforie	euforie	k1gFnSc1
z	z	k7c2
konce	konec	k1gInSc2
studené	studený	k2eAgFnSc2d1
války	válka	k1gFnSc2
a	a	k8xC
triumfu	triumf	k1gInSc2
západního	západní	k2eAgInSc2d1
modelu	model	k1gInSc2
společnosti	společnost	k1gFnSc2
trvala	trvat	k5eAaImAgFnS
takřka	takřka	k6eAd1
po	po	k7c6
celá	celý	k2eAgFnSc1d1
90	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Americké	americký	k2eAgFnSc6d1
ekonomice	ekonomika	k1gFnSc6
<g/>
,	,	kIx,
opojené	opojený	k2eAgFnPc4d1
průnikem	průnik	k1gInSc7
na	na	k7c4
nové	nový	k2eAgInPc4d1
trhy	trh	k1gInPc4
a	a	k8xC
možnostmi	možnost	k1gFnPc7
internetu	internet	k1gInSc2
<g/>
,	,	kIx,
digitalizace	digitalizace	k1gFnSc2
a	a	k8xC
nových	nový	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
dařilo	dařit	k5eAaImAgNnS
(	(	kIx(
<g/>
90	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnSc2
jsou	být	k5eAaImIp3nP
nejdelším	dlouhý	k2eAgNnPc3d3
obdobím	období	k1gNnPc3
setrvalého	setrvalý	k2eAgInSc2d1
ekonomického	ekonomický	k2eAgInSc2d1
růstu	růst	k1gInSc2
v	v	k7c6
americké	americký	k2eAgFnSc6d1
historii	historie	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
Svět	svět	k1gInSc1
do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
akceptoval	akceptovat	k5eAaBmAgMnS
i	i	k9
roli	role	k1gFnSc4
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
jako	jako	k8xS,k8xC
„	„	k?
<g/>
světového	světový	k2eAgMnSc2d1
četníka	četník	k1gMnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
zejm.	zejm.	k?
válka	válka	k1gFnSc1
v	v	k7c6
zálivu	záliv	k1gInSc6
roku	rok	k1gInSc2
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
2001	#num#	k4
přišel	přijít	k5eAaPmAgInS
však	však	k9
zlom	zlom	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednak	jednak	k8xC
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
prasknutí	prasknutí	k1gNnSc3
„	„	k?
<g/>
dot-com	dot-com	k1gInSc1
bubliny	bublina	k1gFnSc2
<g/>
“	“	k?
na	na	k7c6
trzích	trh	k1gInPc6
a	a	k8xC
poté	poté	k6eAd1
éru	éra	k1gFnSc4
poklidného	poklidný	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
narušil	narušit	k5eAaPmAgInS
teroristický	teroristický	k2eAgInSc1d1
útok	útok	k1gInSc1
z	z	k7c2
11	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2001	#num#	k4
na	na	k7c4
newyorská	newyorský	k2eAgNnPc4d1
Dvojčata	dvojče	k1gNnPc4
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yQgInSc4,k3yIgInSc4
provedli	provést	k5eAaPmAgMnP
islámští	islámský	k2eAgMnPc1d1
teroristé	terorista	k1gMnPc1
ze	z	k7c2
skupiny	skupina	k1gFnSc2
Al-Káida	Al-Káida	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
Ačkoli	ačkoli	k8xS
se	se	k3xPyFc4
demokratický	demokratický	k2eAgInSc1d1
svět	svět	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
s	s	k7c7
Američany	Američan	k1gMnPc7
a	a	k8xC
jejich	jejich	k3xOp3gMnSc7
prezidentem	prezident	k1gMnSc7
George	George	k1gNnSc2
W.	W.	kA
Bushem	Bush	k1gMnSc7
v	v	k7c6
té	ten	k3xDgFnSc6
chvíli	chvíle	k1gFnSc6
solidarizoval	solidarizovat	k5eAaImAgMnS
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnSc2
reakce	reakce	k1gFnSc2
<g/>
,	,	kIx,
obecně	obecně	k6eAd1
nazvaná	nazvaný	k2eAgFnSc1d1
válka	válka	k1gFnSc1
proti	proti	k7c3
terorismu	terorismus	k1gInSc3
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
především	především	k6eAd1
spočívala	spočívat	k5eAaImAgFnS
v	v	k7c6
invazi	invaze	k1gFnSc6
do	do	k7c2
Iráku	Irák	k1gInSc2
roku	rok	k1gInSc2
2003	#num#	k4
<g/>
,	,	kIx,
již	již	k6eAd1
tolik	tolik	k6eAd1
respektována	respektovat	k5eAaImNgFnS
nebyla	být	k5eNaImAgFnS
a	a	k8xC
vzrostlo	vzrůst	k5eAaPmAgNnS
i	i	k9
napětí	napětí	k1gNnSc4
uvnitř	uvnitř	k7c2
americké	americký	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jistotami	jistota	k1gFnPc7
zacloumala	zacloumat	k5eAaPmAgFnS
i	i	k9
finanční	finanční	k2eAgFnSc1d1
krize	krize	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
začala	začít	k5eAaPmAgFnS
jako	jako	k9
americká	americký	k2eAgFnSc1d1
hypoteční	hypoteční	k2eAgFnSc1d1
krize	krize	k1gFnSc1
<g/>
,	,	kIx,
avšak	avšak	k8xC
přerostla	přerůst	k5eAaPmAgFnS
v	v	k7c4
krizi	krize	k1gFnSc4
celosvětovou	celosvětový	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
vzrostlo	vzrůst	k5eAaPmAgNnS
rovněž	rovněž	k9
napětí	napětí	k1gNnSc1
mezi	mezi	k7c7
Spojenými	spojený	k2eAgInPc7d1
státy	stát	k1gInPc7
a	a	k8xC
dalšími	další	k2eAgFnPc7d1
velmocemi	velmoc	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
s	s	k7c7
nelibostí	nelibost	k1gFnSc7
nesou	nést	k5eAaImIp3nP
americkou	americký	k2eAgFnSc4d1
hegemonii	hegemonie	k1gFnSc4
-	-	kIx~
především	především	k9
Ruskem	Rusko	k1gNnSc7
a	a	k8xC
Čínou	Čína	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
se	se	k3xPyFc4
dokonce	dokonce	k9
hovoří	hovořit	k5eAaImIp3nS
o	o	k7c6
druhé	druhý	k4xOgFnSc6
studené	studený	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
Oslabeny	oslaben	k2eAgInPc1d1
byly	být	k5eAaImAgInP
vztahy	vztah	k1gInPc1
i	i	k9
se	s	k7c7
západní	západní	k2eAgFnSc7d1
Evropou	Evropa	k1gFnSc7
(	(	kIx(
<g/>
krom	krom	k7c2
Británie	Británie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgInPc1
trendy	trend	k1gInPc1
vyvrcholily	vyvrcholit	k5eAaPmAgInP
za	za	k7c2
vlády	vláda	k1gFnSc2
Donalda	Donald	k1gMnSc2
Trumpa	Trump	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
nastoupil	nastoupit	k5eAaPmAgMnS
do	do	k7c2
prezidentského	prezidentský	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
roku	rok	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Geografie	geografie	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
a	a	k8xC
Fyzická	fyzický	k2eAgFnSc1d1
geografie	geografie	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Poloha	poloha	k1gFnSc1
</s>
<s>
Topografická	topografický	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
leží	ležet	k5eAaImIp3nP
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
břehy	břeh	k1gInPc4
omývá	omývat	k5eAaImIp3nS
z	z	k7c2
východu	východ	k1gInSc2
Atlantský	atlantský	k2eAgInSc1d1
oceán	oceán	k1gInSc1
a	a	k8xC
ze	z	k7c2
západu	západ	k1gInSc2
Tichý	tichý	k2eAgInSc1d1
oceán	oceán	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stát	stát	k1gInSc1
Aljaška	Aljaška	k1gFnSc1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
kontinentu	kontinent	k1gInSc2
a	a	k8xC
je	být	k5eAaImIp3nS
ze	z	k7c2
severu	sever	k1gInSc2
omýván	omývat	k5eAaImNgInS
Severním	severní	k2eAgInSc7d1
ledovým	ledový	k2eAgInSc7d1
oceánem	oceán	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Aljašku	Aljaška	k1gFnSc4
odděluje	oddělovat	k5eAaImIp3nS
od	od	k7c2
Euroasijského	euroasijský	k2eAgInSc2d1
kontinentu	kontinent	k1gInSc2
Beringův	Beringův	k2eAgInSc4d1
průliv	průliv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejznámější	známý	k2eAgInPc4d3
poloostrovy	poloostrov	k1gInPc4
patří	patřit	k5eAaImIp3nS
Florida	Florida	k1gFnSc1
na	na	k7c6
jihu	jih	k1gInSc6
USA	USA	kA
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
odděluje	oddělovat	k5eAaImIp3nS
Mexický	mexický	k2eAgInSc4d1
záliv	záliv	k1gInSc4
od	od	k7c2
Atlantského	atlantský	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
severu	sever	k1gInSc2
mají	mít	k5eAaImIp3nP
USA	USA	kA
společnou	společný	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
s	s	k7c7
Kanadou	Kanada	k1gFnSc7
<g/>
,	,	kIx,
na	na	k7c6
jihu	jih	k1gInSc6
pak	pak	k6eAd1
s	s	k7c7
Mexikem	Mexiko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Floridský	floridský	k2eAgInSc1d1
průliv	průliv	k1gInSc1
odděluje	oddělovat	k5eAaImIp3nS
USA	USA	kA
od	od	k7c2
souostroví	souostroví	k1gNnSc2
Bahamy	Bahamy	k1gFnPc4
a	a	k8xC
od	od	k7c2
Kuby	Kuba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rozloha	rozloha	k1gFnSc1
</s>
<s>
Americké	americký	k2eAgInPc1d1
úřady	úřad	k1gInPc1
posupně	posupně	k6eAd1
mění	měnit	k5eAaImIp3nP
definici	definice	k1gFnSc4
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
počítají	počítat	k5eAaImIp3nP
do	do	k7c2
rozlohy	rozloha	k1gFnSc2
<g/>
,	,	kIx,
CIA	CIA	kA
World	World	k1gInSc4
Factbook	Factbook	k1gInSc4
udávala	udávat	k5eAaImAgFnS
<g/>
:	:	kIx,
</s>
<s>
9	#num#	k4
372	#num#	k4
610	#num#	k4
km²	km²	k?
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
–	–	k?
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
pouze	pouze	k6eAd1
pevnina	pevnina	k1gFnSc1
a	a	k8xC
vnitřní	vnitřní	k2eAgFnPc1d1
vody	voda	k1gFnPc1
</s>
<s>
9	#num#	k4
629	#num#	k4
091	#num#	k4
km²	km²	k?
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
-	-	kIx~
přidána	přidán	k2eAgFnSc1d1
Velká	velká	k1gFnSc1
jezera	jezero	k1gNnSc2
a	a	k8xC
pobřežní	pobřežní	k2eAgFnSc2d1
vody	voda	k1gFnSc2
(	(	kIx(
<g/>
coastal	coastat	k5eAaImAgMnS,k5eAaPmAgMnS
waters	waters	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
9	#num#	k4
631	#num#	k4
418	#num#	k4
km²	km²	k?
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
9	#num#	k4
631	#num#	k4
420	#num#	k4
km²	km²	k?
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
9	#num#	k4
826	#num#	k4
630	#num#	k4
km²	km²	k?
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
přidána	přidán	k2eAgFnSc1d1
pobřežní	pobřežní	k2eAgNnSc4d1
moře	moře	k1gNnSc4
(	(	kIx(
<g/>
territorial	territorial	k1gInSc1
waters	waters	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Zahrnování	zahrnování	k1gNnSc1
plochy	plocha	k1gFnSc2
pobřežního	pobřežní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
do	do	k7c2
rozlohy	rozloha	k1gFnSc2
státu	stát	k1gInSc2
je	být	k5eAaImIp3nS
sice	sice	k8xC
věcně	věcně	k6eAd1
naprosto	naprosto	k6eAd1
správné	správný	k2eAgNnSc1d1
<g/>
,	,	kIx,
přesto	přesto	k8xC
ale	ale	k9
dosti	dosti	k6eAd1
neobvyklé	obvyklý	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
z	z	k7c2
motivů	motiv	k1gInPc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
i	i	k9
snaha	snaha	k1gFnSc1
„	„	k?
<g/>
předběhnout	předběhnout	k5eAaPmF
<g/>
“	“	k?
v	v	k7c6
rozloze	rozloha	k1gFnSc6
Čínu	Čína	k1gFnSc4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Ta	ten	k3xDgFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
ročence	ročenka	k1gFnSc6
CIA	CIA	kA
měřena	měřen	k2eAgNnPc1d1
bez	bez	k7c2
sporných	sporný	k2eAgNnPc2d1
území	území	k1gNnPc2
i	i	k9
bez	bez	k7c2
pobřežního	pobřežní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Povrch	povrch	k1gInSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Nejvyšší	vysoký	k2eAgFnSc2d3
hory	hora	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Pohoří	pohoří	k1gNnSc1
Sierra	Sierra	k1gFnSc1
Nevada	Nevada	k1gFnSc1
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
</s>
<s>
Geografická	geografický	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
USA	USA	kA
</s>
<s>
USA	USA	kA
leží	ležet	k5eAaImIp3nS
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základem	základ	k1gInSc7
tohoto	tento	k3xDgInSc2
kontinentu	kontinent	k1gInSc2
je	být	k5eAaImIp3nS
starý	starý	k2eAgInSc1d1
kanadský	kanadský	k2eAgInSc1d1
štít	štít	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
rozprostírá	rozprostírat	k5eAaImIp3nS
v	v	k7c6
severovýchodní	severovýchodní	k2eAgFnSc6d1
části	část	k1gFnSc6
kontinentu	kontinent	k1gInSc2
a	a	k8xC
jehož	jehož	k3xOyRp3gFnSc1
střední	střední	k2eAgFnSc1d1
část	část	k1gFnSc1
je	být	k5eAaImIp3nS
protlačena	protlačen	k2eAgFnSc1d1
a	a	k8xC
tvoří	tvořit	k5eAaImIp3nS
dno	dno	k1gNnSc4
Hudsonova	Hudsonův	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
velmi	velmi	k6eAd1
starými	stará	k1gFnPc7
přeměněnými	přeměněný	k2eAgFnPc7d1
a	a	k8xC
vyvřelými	vyvřelý	k2eAgFnPc7d1
horninami	hornina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
pod	pod	k7c4
západní	západní	k2eAgInSc4d1
okraj	okraj	k1gInSc4
desek	deska	k1gFnPc2
amerických	americký	k2eAgInPc2d1
kontinentů	kontinent	k1gInPc2
podsouvá	podsouvat	k5eAaImIp3nS
tichooceánská	tichooceánský	k2eAgFnSc1d1
litosférická	litosférický	k2eAgFnSc1d1
deska	deska	k1gFnSc1
a	a	k8xC
zároveň	zároveň	k6eAd1
se	se	k3xPyFc4
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
pohybuje	pohybovat	k5eAaImIp3nS
směrem	směr	k1gInSc7
k	k	k7c3
Severní	severní	k2eAgFnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
důsledku	důsledek	k1gInSc6
pohybů	pohyb	k1gInPc2
litosférických	litosférický	k2eAgFnPc2d1
desek	deska	k1gFnPc2
vznikla	vzniknout	k5eAaPmAgFnS
vysoká	vysoký	k2eAgFnSc1d1
horská	horský	k2eAgFnSc1d1
hradba	hradba	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
táhne	táhnout	k5eAaImIp3nS
od	od	k7c2
Aleutských	aleutský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
přes	přes	k7c4
Aljašský	aljašský	k2eAgInSc4d1
poloostrov	poloostrov	k1gInSc4
až	až	k9
po	po	k7c4
Ohňovou	ohňový	k2eAgFnSc4d1
zemi	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Kordillery	Kordillery	k1gFnPc1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc7
nejvyšším	vysoký	k2eAgInSc7d3
bodem	bod	k1gInSc7
v	v	k7c6
USA	USA	kA
je	být	k5eAaImIp3nS
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
výškou	výška	k1gFnSc7
6190	#num#	k4
metrů	metr	k1gInPc2
Denali	Denali	k1gFnPc2
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
Mount	Mount	k1gMnSc1
McKinley	McKinlea	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
začínají	začínat	k5eAaImIp3nP
Aljašským	aljašský	k2eAgInSc7d1
hřbetem	hřbet	k1gInSc7
a	a	k8xC
táhnou	táhnout	k5eAaImIp3nP
se	se	k3xPyFc4
na	na	k7c4
jih	jih	k1gInSc4
ve	v	k7c6
dvou	dva	k4xCgNnPc6
výrazných	výrazný	k2eAgNnPc6d1
pásmech	pásmo	k1gNnPc6
<g/>
:	:	kIx,
při	při	k7c6
pobřeží	pobřeží	k1gNnSc6
Tichého	Tichého	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
leží	ležet	k5eAaImIp3nS
Pacifické	pacifický	k2eAgNnSc1d1
pobřežní	pobřežní	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
a	a	k8xC
ve	v	k7c6
vnitrozemí	vnitrozemí	k1gNnSc6
Skalnaté	skalnatý	k2eAgFnSc2d1
hory	hora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
jednotlivými	jednotlivý	k2eAgInPc7d1
pásy	pás	k1gInPc7
Kordiller	Kordillery	k1gFnPc2
se	se	k3xPyFc4
rozprostírají	rozprostírat	k5eAaImIp3nP
sníženiny	sníženina	k1gFnPc1
a	a	k8xC
plošiny	plošina	k1gFnPc1
<g/>
,	,	kIx,
např.	např.	kA
Koloradská	Koloradský	k2eAgFnSc1d1
plošina	plošina	k1gFnSc1
či	či	k8xC
Velká	velký	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgInSc4d3
bod	bod	k1gInSc4
tzv.	tzv.	kA
sousedících	sousedící	k2eAgInPc2d1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
(	(	kIx(
<g/>
tj.	tj.	kA
bez	bez	k7c2
Aljašky	Aljaška	k1gFnSc2
a	a	k8xC
Havaje	Havaj	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vrchol	vrchol	k1gInSc4
Mount	Mounto	k1gNnPc2
Whitney	Whitnea	k1gMnSc2
(	(	kIx(
<g/>
4417	#num#	k4
m	m	kA
<g/>
)	)	kIx)
v	v	k7c6
kalifornském	kalifornský	k2eAgNnSc6d1
pohoří	pohoří	k1gNnSc6
Sierra	Sierra	k1gFnSc1
Nevada	Nevada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Jen	jen	k9
pár	pár	k4xCyI
desítek	desítka	k1gFnPc2
kilometrů	kilometr	k1gInPc2
od	od	k7c2
Sierry	Sierra	k1gFnSc2
Nevady	Nevada	k1gFnSc2
leží	ležet	k5eAaImIp3nS
nejníže	nízce	k6eAd3
položené	položený	k2eAgNnSc4d1
místo	místo	k1gNnSc4
USA	USA	kA
i	i	k8xC
Ameriky	Amerika	k1gFnSc2
v	v	k7c6
Údolí	údolí	k1gNnSc6
smrti	smrt	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Death	Death	k1gMnSc1
Valley	Vallea	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
proláklina	proláklina	k1gFnSc1
je	být	k5eAaImIp3nS
86	#num#	k4
metrů	metr	k1gInPc2
pod	pod	k7c7
úrovní	úroveň	k1gFnSc7
hladiny	hladina	k1gFnSc2
moře	moře	k1gNnSc2
a	a	k8xC
své	svůj	k3xOyFgNnSc4
jméno	jméno	k1gNnSc4
dostala	dostat	k5eAaPmAgFnS
podle	podle	k7c2
nehostinných	hostinný	k2eNgFnPc2d1
podmínek	podmínka	k1gFnPc2
pro	pro	k7c4
život	život	k1gInSc4
<g/>
:	:	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
poušť	poušť	k1gFnSc4
bez	bez	k7c2
vodních	vodní	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
naměřena	naměřit	k5eAaBmNgFnS
dosud	dosud	k6eAd1
nejvyšší	vysoký	k2eAgFnSc1d3
teplota	teplota	k1gFnSc1
na	na	k7c6
Zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
56,7	56,7	k4
°	°	k?
<g/>
C	C	kA
ve	v	k7c6
stínu	stín	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Poblíž	poblíž	k7c2
východního	východní	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
se	se	k3xPyFc4
táhne	táhnout	k5eAaImIp3nS
staré	starý	k2eAgNnSc4d1
pohoří	pohoří	k1gNnSc4
Apalačské	Apalačský	k2eAgFnSc2d1
hory	hora	k1gFnSc2
a	a	k8xC
mezi	mezi	k7c7
Apalačskými	Apalačský	k2eAgFnPc7d1
horami	hora	k1gFnPc7
a	a	k8xC
Atlantským	atlantský	k2eAgInSc7d1
oceánem	oceán	k1gInSc7
se	se	k3xPyFc4
rozprostírá	rozprostírat	k5eAaImIp3nS
pak	pak	k6eAd1
Pobřežní	pobřežní	k2eAgFnSc1d1
nížina	nížina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mexický	mexický	k2eAgInSc1d1
záliv	záliv	k1gInSc1
lemuje	lemovat	k5eAaImIp3nS
Mississippská	mississippský	k2eAgFnSc1d1
nížina	nížina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
Apalačskými	Apalačský	k2eAgFnPc7d1
horami	hora	k1gFnPc7
a	a	k8xC
Kordillerami	Kordillery	k1gFnPc7
se	se	k3xPyFc4
postupně	postupně	k6eAd1
od	od	k7c2
východu	východ	k1gInSc2
rozprostírají	rozprostírat	k5eAaImIp3nP
Vnitřní	vnitřní	k2eAgFnPc1d1
roviny	rovina	k1gFnPc1
a	a	k8xC
Velké	velký	k2eAgFnPc1d1
planiny	planina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Vodstvo	vodstvo	k1gNnSc1
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1
Michiganského	michiganský	k2eAgNnSc2d1
jezera	jezero	k1gNnSc2
ve	v	k7c6
státě	stát	k1gInSc6
Michigan	Michigan	k1gInSc1
</s>
<s>
Větší	veliký	k2eAgFnSc1d2
část	část	k1gFnSc1
území	území	k1gNnSc2
USA	USA	kA
patří	patřit	k5eAaImIp3nS
do	do	k7c2
úmoří	úmoří	k1gNnSc2
Atlantského	atlantský	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlévají	vlévat	k5eAaImIp3nP
se	se	k3xPyFc4
do	do	k7c2
něj	on	k3xPp3gInSc2
veletoky	veletok	k1gInPc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
Mississippi	Mississippi	k1gNnSc1
s	s	k7c7
přítokem	přítok	k1gInSc7
Missouri	Missouri	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
nejdelší	dlouhý	k2eAgFnSc1d3
říční	říční	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
USA	USA	kA
<g/>
,	,	kIx,
dlouhá	dlouhý	k2eAgFnSc1d1
6212	#num#	k4
km	km	kA
<g/>
,	,	kIx,
a	a	k8xC
dále	daleko	k6eAd2
také	také	k9
Řeka	řeka	k1gFnSc1
Svatého	svatý	k2eAgMnSc2d1
Vavřince	Vavřinec	k1gMnSc2
či	či	k8xC
Rio	Rio	k1gMnSc2
Grande	grand	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tichý	tichý	k2eAgInSc1d1
oceán	oceán	k1gInSc1
zásobují	zásobovat	k5eAaImIp3nP
vesměs	vesměs	k6eAd1
kratší	krátký	k2eAgInPc1d2
toky	tok	k1gInPc1
tekoucí	tekoucí	k2eAgInPc1d1
z	z	k7c2
Kordiller	Kordillery	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
řeky	řeka	k1gFnPc1
Columbia	Columbia	k1gFnSc1
a	a	k8xC
Colorado	Colorado	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
USA	USA	kA
najdeme	najít	k5eAaPmIp1nP
i	i	k9
mnoho	mnoho	k4c4
jezer	jezero	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ledovcového	ledovcový	k2eAgInSc2d1
původu	původ	k1gInSc2
jsou	být	k5eAaImIp3nP
Velká	velký	k2eAgNnPc1d1
jezera	jezero	k1gNnPc1
(	(	kIx(
<g/>
Hořejší	Hořejší	k1gFnSc1
jezero	jezero	k1gNnSc1
<g/>
,	,	kIx,
Michiganské	michiganský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
<g/>
,	,	kIx,
Huronské	Huronský	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
a	a	k8xC
jezero	jezero	k1gNnSc1
Ontario	Ontario	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
tvoří	tvořit	k5eAaImIp3nP
největší	veliký	k2eAgFnSc4d3
zásobárnu	zásobárna	k1gFnSc4
sladké	sladký	k2eAgFnSc2d1
vody	voda	k1gFnSc2
na	na	k7c6
světě	svět	k1gInSc6
a	a	k8xC
přírodní	přírodní	k2eAgFnSc6d1
hranici	hranice	k1gFnSc6
mezi	mezi	k7c7
USA	USA	kA
a	a	k8xC
Kanadou	Kanada	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgNnSc1d1
Solné	solný	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
se	se	k3xPyFc4
rozprostírá	rozprostírat	k5eAaImIp3nS
v	v	k7c6
bezodtoké	bezodtoký	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
mezi	mezi	k7c7
Skalnatými	skalnatý	k2eAgFnPc7d1
horami	hora	k1gFnPc7
a	a	k8xC
Coastal	Coastal	k1gFnPc7
Range	Rang	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
USA	USA	kA
je	být	k5eAaImIp3nS
množství	množství	k1gNnSc1
vodopádů	vodopád	k1gInPc2
<g/>
;	;	kIx,
nesporně	sporně	k6eNd1
nejznámější	známý	k2eAgInPc1d3
jsou	být	k5eAaImIp3nP
Niagarské	niagarský	k2eAgInPc1d1
vodopády	vodopád	k1gInPc1
mezi	mezi	k7c7
Erijským	Erijský	k2eAgNnSc7d1
a	a	k8xC
Ontarijským	ontarijský	k2eAgNnSc7d1
jezerem	jezero	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Podnebí	podnebí	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Podnebí	podnebí	k1gNnPc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Typická	typický	k2eAgFnSc1d1
krajina	krajina	k1gFnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
od	od	k7c2
Velkých	velký	k2eAgNnPc2d1
Jezer	jezero	k1gNnPc2
po	po	k7c4
severovýchod	severovýchod	k1gInSc4
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
</s>
<s>
Díky	dík	k1gInPc1
své	svůj	k3xOyFgFnSc2
velikosti	velikost	k1gFnSc2
leží	ležet	k5eAaImIp3nS
USA	USA	kA
v	v	k7c6
několika	několik	k4yIc6
významných	významný	k2eAgInPc6d1
podnebných	podnebný	k2eAgInPc6d1
pásech	pás	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počasí	počasí	k1gNnSc1
v	v	k7c6
Americe	Amerika	k1gFnSc6
je	být	k5eAaImIp3nS
značně	značně	k6eAd1
nestálé	stálý	k2eNgInPc4d1
a	a	k8xC
teplotní	teplotní	k2eAgInPc4d1
výkyvy	výkyv	k1gInPc4
mohou	moct	k5eAaImIp3nP
nastávat	nastávat	k5eAaImF
dokonce	dokonce	k9
i	i	k9
několikrát	několikrát	k6eAd1
denně	denně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severní	severní	k2eAgFnSc1d1
část	část	k1gFnSc1
území	území	k1gNnSc2
leží	ležet	k5eAaImIp3nS
v	v	k7c6
mírném	mírný	k2eAgInSc6d1
pásu	pás	k1gInSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
ale	ale	k9
chladnější	chladný	k2eAgInSc1d2
a	a	k8xC
vlhčí	vlhký	k2eAgInSc1d2
než	než	k8xS
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Způsobuje	způsobovat	k5eAaImIp3nS
to	ten	k3xDgNnSc1
studený	studený	k2eAgInSc1d1
Labradorský	labradorský	k2eAgInSc1d1
proud	proud	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
omývá	omývat	k5eAaImIp3nS
severovýchodní	severovýchodní	k2eAgNnSc4d1
a	a	k8xC
východní	východní	k2eAgNnSc4d1
pobřeží	pobřeží	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Vnitřních	vnitřní	k2eAgFnPc6d1
rovinách	rovina	k1gFnPc6
a	a	k8xC
Velkých	velký	k2eAgFnPc6d1
planinách	planina	k1gFnPc6
padá	padat	k5eAaImIp3nS
úměrně	úměrně	k6eAd1
vzdálenosti	vzdálenost	k1gFnPc4
od	od	k7c2
Atlantského	atlantský	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
čím	co	k3yQnSc7,k3yRnSc7,k3yInSc7
dál	daleko	k6eAd2
tím	ten	k3xDgNnSc7
méně	málo	k6eAd2
srážek	srážka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímavým	zajímavý	k2eAgInSc7d1
jevem	jev	k1gInSc7
je	být	k5eAaImIp3nS
sněhový	sněhový	k2eAgInSc4d1
efekt	efekt	k1gInSc4
Velkých	velký	k2eAgNnPc2d1
jezer	jezero	k1gNnPc2
<g/>
,	,	kIx,
způsobující	způsobující	k2eAgFnPc1d1
sněhové	sněhový	k2eAgFnPc1d1
kalamity	kalamita	k1gFnPc1
v	v	k7c6
jejich	jejich	k3xOp3gFnSc6
blízkosti	blízkost	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
zejména	zejména	k9
na	na	k7c6
jejich	jejich	k3xOp3gNnSc6
jihozápadním	jihozápadní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
převážně	převážně	k6eAd1
na	na	k7c6
území	území	k1gNnSc6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chladnější	chladný	k2eAgInSc4d2
podnebí	podnebí	k1gNnSc1
panuje	panovat	k5eAaImIp3nS
v	v	k7c6
oblasti	oblast	k1gFnSc6
západního	západní	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
pod	pod	k7c7
vlivem	vliv	k1gInSc7
proudů	proud	k1gInPc2
Tichého	Tichého	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
udržuje	udržovat	k5eAaImIp3nS
teplota	teplota	k1gFnSc1
vesměs	vesměs	k6eAd1
konstantní	konstantní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejchladnější	chladný	k2eAgFnPc1d3
zimy	zima	k1gFnPc1
najdeme	najít	k5eAaPmIp1nP
ve	v	k7c6
Skalistých	skalistý	k2eAgFnPc6d1
horách	hora	k1gFnPc6
a	a	k8xC
Sierra	Sierra	k1gFnSc1
Nevadě	Nevada	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
můžeme	moct	k5eAaImIp1nP
naměřit	naměřit	k5eAaBmF
jedny	jeden	k4xCgInPc4
z	z	k7c2
nejsilnějších	silný	k2eAgInPc2d3
mrazů	mráz	k1gInPc2
v	v	k7c6
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
oblastech	oblast	k1gFnPc6
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
Velkých	velký	k2eAgFnPc2d1
plání	pláň	k1gFnPc2
<g/>
,	,	kIx,
jižních	jižní	k2eAgFnPc2d1
částí	část	k1gFnPc2
atlantického	atlantický	k2eAgNnSc2d1
pobřeží	pobřeží	k1gNnSc2
<g/>
,	,	kIx,
Floridy	Florida	k1gFnSc2
a	a	k8xC
ve	v	k7c6
státech	stát	k1gInPc6
u	u	k7c2
Mexického	mexický	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
panují	panovat	k5eAaImIp3nP
v	v	k7c6
letním	letní	k2eAgNnSc6d1
období	období	k1gNnSc6
až	až	k9
tropická	tropický	k2eAgNnPc1d1
vedra	vedro	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
může	moct	k5eAaImIp3nS
doprovázet	doprovázet	k5eAaImF
i	i	k9
nesnesitelná	snesitelný	k2eNgFnSc1d1
vlhkost	vlhkost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižní	jižní	k2eAgFnSc1d1
část	část	k1gFnSc1
USA	USA	kA
leží	ležet	k5eAaImIp3nS
v	v	k7c6
příjemném	příjemný	k2eAgInSc6d1
subtropickém	subtropický	k2eAgInSc6d1
podnebném	podnebný	k2eAgInSc6d1
pásu	pás	k1gInSc6
a	a	k8xC
Mexický	mexický	k2eAgInSc1d1
záliv	záliv	k1gInSc1
s	s	k7c7
poloostrovem	poloostrov	k1gInSc7
Florida	Florida	k1gFnSc1
v	v	k7c6
tropickém	tropický	k2eAgInSc6d1
pásu	pás	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výjimku	výjimek	k1gInSc2
tvoří	tvořit	k5eAaImIp3nP
vysoce	vysoce	k6eAd1
položené	položený	k2eAgNnSc4d1
pohoří	pohoří	k1gNnSc4
a	a	k8xC
plošiny	plošina	k1gFnPc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
podstatně	podstatně	k6eAd1
chladněji	chladně	k6eAd2
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
se	se	k3xPyFc4
zde	zde	k6eAd1
projevuje	projevovat	k5eAaImIp3nS
výšková	výškový	k2eAgFnSc1d1
stupňovitost	stupňovitost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aljaška	Aljaška	k1gFnSc1
leží	ležet	k5eAaImIp3nS
v	v	k7c6
subarktickém	subarktický	k2eAgInSc6d1
pásu	pás	k1gInSc6
a	a	k8xC
Havajské	havajský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
v	v	k7c6
tropickém	tropický	k2eAgInSc6d1
pásu	pás	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvar	tvar	k1gInSc1
povrchu	povrch	k1gInSc2
Ameriky	Amerika	k1gFnSc2
umožňuje	umožňovat	k5eAaImIp3nS
takřka	takřka	k6eAd1
bezproblémový	bezproblémový	k2eAgInSc4d1
přesun	přesun	k1gInSc4
vzdušných	vzdušný	k2eAgFnPc2d1
mas	masa	k1gFnPc2
ze	z	k7c2
severu	sever	k1gInSc2
na	na	k7c4
jih	jih	k1gInSc4
a	a	k8xC
naopak	naopak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
jsou	být	k5eAaImIp3nP
časté	častý	k2eAgMnPc4d1
hlavně	hlavně	k6eAd1
vpády	vpád	k1gInPc4
arktického	arktický	k2eAgInSc2d1
vzduchu	vzduch	k1gInSc2
z	z	k7c2
vyšších	vysoký	k2eAgFnPc2d2
zeměpisných	zeměpisný	k2eAgFnPc2d1
šířek	šířka	k1gFnPc2
až	až	k9
do	do	k7c2
oblasti	oblast	k1gFnSc2
Mexického	mexický	k2eAgInSc2d1
zálivu	záliv	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proudění	proudění	k1gNnPc1
v	v	k7c6
rovnoběžkovém	rovnoběžkový	k2eAgInSc6d1
směru	směr	k1gInSc6
stojí	stát	k5eAaImIp3nS
v	v	k7c6
cestě	cesta	k1gFnSc6
horské	horský	k2eAgFnPc4d1
bariéry	bariéra	k1gFnPc4
Kordiller	Kordillery	k1gFnPc2
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
vláha	vláha	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
s	s	k7c7
proudícím	proudící	k2eAgInSc7d1
vzduchem	vzduch	k1gInSc7
dostávala	dostávat	k5eAaImAgFnS
do	do	k7c2
vnitrozemí	vnitrozemí	k1gNnSc2
Ameriky	Amerika	k1gFnSc2
z	z	k7c2
Tichého	Tichého	k2eAgInSc2d1
oceánu	oceán	k1gInSc2
<g/>
,	,	kIx,
vyprší	vypršet	k5eAaPmIp3nS
nad	nad	k7c7
oceánem	oceán	k1gInSc7
<g/>
,	,	kIx,
úzkou	úzký	k2eAgFnSc7d1
pobřežní	pobřežní	k2eAgFnSc7d1
nížinou	nížina	k1gFnSc7
a	a	k8xC
návětrnými	návětrný	k2eAgInPc7d1
svahy	svah	k1gInPc7
tichooceánského	tichooceánský	k2eAgNnSc2d1
pásma	pásmo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Texas	Texas	k1gInSc1
je	být	k5eAaImIp3nS
jinak	jinak	k6eAd1
známý	známý	k2eAgMnSc1d1
pro	pro	k7c4
svá	svůj	k3xOyFgNnPc4
tornáda	tornádo	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yIgNnPc1,k3yQgNnPc1
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
hlavně	hlavně	k9
na	na	k7c6
otevřených	otevřený	k2eAgFnPc6d1
plochách	plocha	k1gFnPc6
v	v	k7c6
období	období	k1gNnSc6
května	květen	k1gInSc2
a	a	k8xC
září	září	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1
teploty	teplota	k1gFnPc1
v	v	k7c6
amerických	americký	k2eAgNnPc6d1
městech	město	k1gNnPc6
(	(	kIx(
<g/>
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
</s>
<s>
Období	období	k1gNnSc1
</s>
<s>
Anchorage	Anchorage	k6eAd1
</s>
<s>
Atlanta	Atlanta	k1gFnSc1
</s>
<s>
Boston	Boston	k1gInSc1
</s>
<s>
Chicago	Chicago	k1gNnSc1
</s>
<s>
Houston	Houston	k1gInSc1
</s>
<s>
Las	laso	k1gNnPc2
Vegas	Vegas	k1gInSc4
</s>
<s>
Miami	Miami	k1gNnSc1
</s>
<s>
New	New	k?
Orleans	Orleans	k1gInSc1
</s>
<s>
New	New	k?
York	York	k1gInSc1
</s>
<s>
Salt	salto	k1gNnPc2
Lake	Lake	k1gFnPc7
City	City	k1gFnSc2
</s>
<s>
San	San	k?
Diego	Diego	k6eAd1
</s>
<s>
San	San	k?
Francisco	Francisco	k6eAd1
</s>
<s>
Seattle	Seattle	k6eAd1
</s>
<s>
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
D.C.	D.C.	k1gFnSc1
</s>
<s>
Leden	leden	k1gInSc1
</s>
<s>
-11	-11	k4
</s>
<s>
5	#num#	k4
</s>
<s>
-1	-1	k4
</s>
<s>
-6	-6	k4
</s>
<s>
11	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
-2	-2	k4
</s>
<s>
14	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
Červenec	červenec	k1gInSc1
</s>
<s>
15	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
32	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
Říjen	říjen	k1gInSc1
</s>
<s>
1	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
Vláda	vláda	k1gFnSc1
a	a	k8xC
politika	politika	k1gFnSc1
</s>
<s>
Socha	socha	k1gFnSc1
svobody	svoboda	k1gFnSc2
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
je	být	k5eAaImIp3nS
symbolem	symbol	k1gInSc7
jak	jak	k8xS,k8xC
USA	USA	kA
<g/>
,	,	kIx,
tak	tak	k6eAd1
ideálů	ideál	k1gInPc2
svobody	svoboda	k1gFnSc2
<g/>
,	,	kIx,
demokracie	demokracie	k1gFnSc2
a	a	k8xC
příležitosti	příležitost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Severní	severní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Bílého	bílý	k2eAgInSc2d1
domu	dům	k1gInSc2
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
jsou	být	k5eAaImIp3nP
nejstarší	starý	k2eAgFnSc7d3
federací	federace	k1gFnSc7
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
prezidentskou	prezidentský	k2eAgFnSc4d1
republiku	republika	k1gFnSc4
a	a	k8xC
zastupitelskou	zastupitelský	k2eAgFnSc4d1
demokracii	demokracie	k1gFnSc4
<g/>
,	,	kIx,
fungující	fungující	k2eAgInSc4d1
prostřednictvím	prostřednictvím	k7c2
kongresového	kongresový	k2eAgInSc2d1
systému	systém	k1gInSc2
definovaného	definovaný	k2eAgInSc2d1
ústavou	ústava	k1gFnSc7
nahrazující	nahrazující	k2eAgFnSc4d1
původní	původní	k2eAgFnSc4d1
ústavu	ústava	k1gFnSc4
<g/>
,	,	kIx,
tzv.	tzv.	kA
Články	článek	k1gInPc1
Konfederace	konfederace	k1gFnSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
Articles	Articles	k1gInSc1
of	of	k?
Confederation	Confederation	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
platné	platný	k2eAgInPc4d1
mezi	mezi	k7c4
roky	rok	k1gInPc4
1781	#num#	k4
a	a	k8xC
1788	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Území	území	k1gNnSc1
tak	tak	k6eAd1
spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
pod	pod	k7c4
troje	troje	k4xRgFnPc4
orgány	orgány	k1gFnPc4
–	–	k?
na	na	k7c6
federální	federální	k2eAgFnSc6d1
<g/>
,	,	kIx,
státní	státní	k2eAgFnSc6d1
a	a	k8xC
místní	místní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
mnohé	mnohý	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
jsou	být	k5eAaImIp3nP
spravovány	spravován	k2eAgInPc1d1
více	hodně	k6eAd2
místními	místní	k2eAgFnPc7d1
správami	správa	k1gFnPc7
<g/>
,	,	kIx,
například	například	k6eAd1
i	i	k9
okresními	okresní	k2eAgInPc7d1
nebo	nebo	k8xC
metropolitními	metropolitní	k2eAgInPc7d1
orgány	orgán	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
tyto	tento	k3xDgInPc4
správní	správní	k2eAgInPc4d1
orgány	orgán	k1gInPc4
jsou	být	k5eAaImIp3nP
voliči	volič	k1gMnPc1
voleny	volen	k2eAgInPc4d1
v	v	k7c6
tajných	tajný	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
nebo	nebo	k8xC
jmenovány	jmenovat	k5eAaImNgFnP,k5eAaBmNgFnP
voliči	volič	k1gMnPc7
volenými	volený	k2eAgMnPc7d1
zástupci	zástupce	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
Státní	státní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
se	se	k3xPyFc4
na	na	k7c6
federální	federální	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
tří	tři	k4xCgFnPc2
složek	složka	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
zákonodárné	zákonodárný	k2eAgNnSc1d1
<g/>
:	:	kIx,
Je	být	k5eAaImIp3nS
jí	on	k3xPp3gFnSc3
Kongres	kongres	k1gInSc1
skládající	skládající	k2eAgFnSc2d1
se	se	k3xPyFc4
ze	z	k7c2
Senátu	senát	k1gInSc2
a	a	k8xC
Sněmovny	sněmovna	k1gFnSc2
reprezentantů	reprezentant	k1gMnPc2
(	(	kIx(
<g/>
„	„	k?
<g/>
House	house	k1gNnSc1
of	of	k?
Representatives	Representatives	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
určující	určující	k2eAgInPc1d1
federální	federální	k2eAgInPc1d1
zákony	zákon	k1gInPc1
<g/>
,	,	kIx,
vyhlašující	vyhlašující	k2eAgFnPc1d1
války	válka	k1gFnPc1
<g/>
,	,	kIx,
schvalující	schvalující	k2eAgFnPc1d1
mezinárodní	mezinárodní	k2eAgFnPc1d1
dohody	dohoda	k1gFnPc1
a	a	k8xC
federální	federální	k2eAgInSc1d1
rozpočet	rozpočet	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
výkonné	výkonný	k2eAgFnPc4d1
<g/>
:	:	kIx,
Je	být	k5eAaImIp3nS
jí	on	k3xPp3gFnSc3
prezident	prezident	k1gMnSc1
se	s	k7c7
schválením	schválení	k1gNnSc7
senátu	senát	k1gInSc2
jmenující	jmenující	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
a	a	k8xC
další	další	k2eAgMnPc4d1
úředníky	úředník	k1gMnPc4
<g/>
,	,	kIx,
spravující	spravující	k2eAgNnSc4d1
federální	federální	k2eAgNnSc4d1
právo	právo	k1gNnSc4
<g/>
,	,	kIx,
vetující	vetující	k2eAgInPc4d1
návrhy	návrh	k1gInPc4
zákonů	zákon	k1gInPc2
a	a	k8xC
velící	velící	k2eAgFnSc6d1
armádě	armáda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
soudní	soudní	k2eAgInSc1d1
<g/>
:	:	kIx,
Je	být	k5eAaImIp3nS
jí	on	k3xPp3gFnSc3
nejvyšší	vysoký	k2eAgNnSc1d3
soud	soud	k1gInSc4
a	a	k8xC
níže	nízce	k6eAd2
postavené	postavený	k2eAgInPc1d1
federální	federální	k2eAgInPc1d1
soudy	soud	k1gInPc1
jmenované	jmenovaný	k2eAgInPc1d1
prezidentem	prezident	k1gMnSc7
se	s	k7c7
svolením	svolení	k1gNnSc7
senátu	senát	k1gInSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgNnSc4
právo	právo	k1gNnSc4
vykládají	vykládat	k5eAaImIp3nP
a	a	k8xC
určují	určovat	k5eAaImIp3nP
platnost	platnost	k1gFnSc4
zákonů	zákon	k1gInPc2
podle	podle	k7c2
ústavy	ústava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kongres	kongres	k1gInSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
je	být	k5eAaImIp3nS
parlamentem	parlament	k1gInSc7
dvoukomorovým	dvoukomorový	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sněmovna	sněmovna	k1gFnSc1
reprezentantů	reprezentant	k1gInPc2
má	mít	k5eAaImIp3nS
435	#num#	k4
členů	člen	k1gMnPc2
po	po	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
reprezentujících	reprezentující	k2eAgInPc2d1
takzvané	takzvaný	k2eAgMnPc4d1
„	„	k?
<g/>
kongresové	kongresový	k2eAgInPc1d1
okresy	okres	k1gInPc1
<g/>
“	“	k?
(	(	kIx(
<g/>
český	český	k2eAgInSc1d1
překlad	překlad	k1gInSc1
je	být	k5eAaImIp3nS
problematický	problematický	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poměr	poměr	k1gInSc1
počtu	počet	k1gInSc2
zástupců	zástupce	k1gMnPc2
z	z	k7c2
každého	každý	k3xTgInSc2
státu	stát	k1gInSc2
se	se	k3xPyFc4
každý	každý	k3xTgInSc4
desátý	desátý	k4xOgInSc4
rok	rok	k1gInSc4
mění	měnit	k5eAaImIp3nS
podle	podle	k7c2
vývoje	vývoj	k1gInSc2
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každému	každý	k3xTgInSc3
státu	stát	k1gInSc3
je	být	k5eAaImIp3nS
však	však	k9
zajištěn	zajistit	k5eAaPmNgInS
nejméně	málo	k6eAd3
jeden	jeden	k4xCgMnSc1
zástupce	zástupce	k1gMnSc1
<g/>
,	,	kIx,
jehož	jenž	k3xRgMnSc4,k3xOyRp3gMnSc4
v	v	k7c6
současnosti	současnost	k1gFnSc6
má	mít	k5eAaImIp3nS
sedm	sedm	k4xCc1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nelidnatější	lidnatý	k2eNgInSc1d2
stát	stát	k1gInSc1
<g/>
,	,	kIx,
Kalifornie	Kalifornie	k1gFnSc1
<g/>
,	,	kIx,
jich	on	k3xPp3gMnPc2
má	mít	k5eAaImIp3nS
53	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc1
stát	stát	k1gInSc1
má	mít	k5eAaImIp3nS
dva	dva	k4xCgMnPc4
senátory	senátor	k1gMnPc4
volené	volený	k2eAgMnPc4d1
na	na	k7c4
šestileté	šestiletý	k2eAgNnSc4d1
volební	volební	k2eAgNnSc4d1
období	období	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc4
druhý	druhý	k4xOgInSc4
rok	rok	k1gInSc4
je	být	k5eAaImIp3nS
volena	volit	k5eAaImNgFnS
třetina	třetina	k1gFnSc1
senátu	senát	k1gInSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
sněmovna	sněmovna	k1gFnSc1
reprezentantů	reprezentant	k1gMnPc2
je	být	k5eAaImIp3nS
každý	každý	k3xTgInSc4
druhý	druhý	k4xOgInSc4
rok	rok	k1gInSc4
volena	volen	k2eAgFnSc1d1
celá	celá	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Senátoři	senátor	k1gMnPc1
tedy	tedy	k9
mají	mít	k5eAaImIp3nP
šestileté	šestiletý	k2eAgNnSc4d1
volební	volební	k2eAgNnSc4d1
období	období	k1gNnSc4
a	a	k8xC
kongresmani	kongresman	k1gMnPc1
dvouleté	dvouletý	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volba	volba	k1gFnSc1
kongresmanů	kongresman	k1gMnPc2
i	i	k8xC
senátorů	senátor	k1gMnPc2
není	být	k5eNaImIp3nS
ústavou	ústava	k1gFnSc7
specifikována	specifikován	k2eAgFnSc1d1
a	a	k8xC
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
ponechána	ponechat	k5eAaPmNgFnS
na	na	k7c6
státech	stát	k1gInPc6
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
většinou	většinou	k6eAd1
se	se	k3xPyFc4
do	do	k7c2
obou	dva	k4xCgFnPc2
komor	komora	k1gFnPc2
volí	volit	k5eAaImIp3nS
pomocí	pomocí	k7c2
Systému	systém	k1gInSc2
prvního	první	k4xOgNnSc2
v	v	k7c6
cíli	cíl	k1gInSc6
(	(	kIx(
<g/>
First-past-the-post	First-past-the-post	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ústava	ústava	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
je	být	k5eAaImIp3nS
nejvyšším	vysoký	k2eAgInSc7d3
zákonným	zákonný	k2eAgInSc7d1
dokumentem	dokument	k1gInSc7
amerického	americký	k2eAgInSc2d1
právního	právní	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
společenskou	společenský	k2eAgFnSc7d1
smlouvou	smlouva	k1gFnSc7
upravující	upravující	k2eAgNnSc1d1
fungování	fungování	k1gNnSc1
společnosti	společnost	k1gFnSc2
pomocí	pomocí	k7c2
zvolené	zvolený	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veškeré	veškerý	k3xTgInPc4
zákony	zákon	k1gInPc4
i	i	k8xC
postupy	postup	k1gInPc4
států	stát	k1gInPc2
a	a	k8xC
federální	federální	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
předmětem	předmět	k1gInSc7
zkoumání	zkoumání	k1gNnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
shledáno	shledat	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
nejsou	být	k5eNaImIp3nP
ve	v	k7c6
shodě	shoda	k1gFnSc6
s	s	k7c7
ústavou	ústava	k1gFnSc7
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
zneplatněny	zneplatněn	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústava	ústava	k1gFnSc1
je	být	k5eAaImIp3nS
živým	živý	k2eAgInSc7d1
dokumentem	dokument	k1gInSc7
jež	jenž	k3xRgNnSc4
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
doplněn	doplnit	k5eAaPmNgInS
několika	několik	k4yIc7
různými	různý	k2eAgInPc7d1
způsoby	způsob	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgInPc4
z	z	k7c2
nich	on	k3xPp3gFnPc2
však	však	k9
vyžadují	vyžadovat	k5eAaImIp3nP
svolení	svolení	k1gNnSc4
alespoň	alespoň	k9
převážné	převážný	k2eAgFnSc2d1
většiny	většina	k1gFnSc2
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústava	ústava	k1gFnSc1
tak	tak	k6eAd1
byla	být	k5eAaImAgFnS
od	od	k7c2
doby	doba	k1gFnSc2
svého	svůj	k3xOyFgInSc2
vzniku	vznik	k1gInSc2
změněna	změněn	k2eAgFnSc1d1
<g/>
,	,	kIx,
resp.	resp.	kA
doplněna	doplněn	k2eAgFnSc1d1
<g/>
,	,	kIx,
27	#num#	k4
krát	krát	k6eAd1
<g/>
,	,	kIx,
naposledy	naposledy	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Ústava	ústava	k1gFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
ustanovení	ustanovení	k1gNnSc4
„	„	k?
<g/>
zachovat	zachovat	k5eAaPmF
svobodu	svoboda	k1gFnSc4
<g/>
“	“	k?
a	a	k8xC
základní	základní	k2eAgNnPc4d1
práva	právo	k1gNnPc4
podle	podle	k7c2
Listiny	listina	k1gFnSc2
práv	právo	k1gNnPc2
(	(	kIx(
<g/>
„	„	k?
<g/>
Bill	Bill	k1gMnSc1
of	of	k?
Rights	Rights	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
a	a	k8xC
dodatků	dodatek	k1gInPc2
ústavy	ústava	k1gFnSc2
jež	jenž	k3xRgInPc1
zahrnují	zahrnovat	k5eAaImIp3nP
svobodu	svoboda	k1gFnSc4
slova	slovo	k1gNnSc2
<g/>
,	,	kIx,
svobodu	svoboda	k1gFnSc4
vyznání	vyznání	k1gNnSc2
a	a	k8xC
svobodu	svoboda	k1gFnSc4
tisku	tisk	k1gInSc2
<g/>
,	,	kIx,
právo	právo	k1gNnSc4
na	na	k7c4
spravedlivý	spravedlivý	k2eAgInSc4d1
proces	proces	k1gInSc4
<g/>
,	,	kIx,
právo	právo	k1gNnSc4
držet	držet	k5eAaImF
a	a	k8xC
nosit	nosit	k5eAaImF
zbraň	zbraň	k1gFnSc4
<g/>
,	,	kIx,
všeobecné	všeobecný	k2eAgNnSc4d1
volební	volební	k2eAgNnSc4d1
právo	právo	k1gNnSc4
<g/>
,	,	kIx,
a	a	k8xC
právo	právo	k1gNnSc1
vlastnické	vlastnický	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
všem	všecek	k3xTgInPc3
státům	stát	k1gInPc3
zaručuje	zaručovat	k5eAaImIp3nS
republikánské	republikánský	k2eAgNnSc4d1
zřízení	zřízení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Politický	politický	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Politický	politický	k2eAgInSc1d1
systém	systém	k1gInSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Budova	budova	k1gFnSc1
Kapitolu	Kapitol	k1gInSc2
</s>
<s>
Prezident	prezident	k1gMnSc1
Barack	Barack	k1gMnSc1
Obama	Obama	k?
při	při	k7c6
setkání	setkání	k1gNnSc6
s	s	k7c7
představiteli	představitel	k1gMnPc7
kongresu	kongres	k1gInSc2
<g/>
,	,	kIx,
2011	#num#	k4
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
zastupitelskou	zastupitelský	k2eAgFnSc7d1
demokracií	demokracie	k1gFnSc7
a	a	k8xC
prezidentskou	prezidentský	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politický	politický	k2eAgInSc1d1
systém	systém	k1gInSc1
je	být	k5eAaImIp3nS
většinový	většinový	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
americké	americký	k2eAgFnSc3d1
politické	politický	k2eAgFnSc3d1
kultuře	kultura	k1gFnSc3
patří	patřit	k5eAaImIp3nS
velká	velký	k2eAgFnSc1d1
soupeřivost	soupeřivost	k1gFnSc1
mezi	mezi	k7c7
dvěma	dva	k4xCgFnPc7
největšími	veliký	k2eAgFnPc7d3
stranami	strana	k1gFnPc7
a	a	k8xC
vysoký	vysoký	k2eAgInSc1d1
podíl	podíl	k1gInSc1
veřejnosti	veřejnost	k1gFnSc2
v	v	k7c6
politice	politika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Americké	americký	k2eAgFnSc3d1
politice	politika	k1gFnSc3
dominují	dominovat	k5eAaImIp3nP
dvě	dva	k4xCgFnPc4
největší	veliký	k2eAgFnPc4d3
strany	strana	k1gFnPc4
–	–	k?
Republikánská	republikánský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
a	a	k8xC
Demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členové	člen	k1gMnPc1
těchto	tento	k3xDgFnPc2
dvou	dva	k4xCgFnPc2
stran	strana	k1gFnPc2
drží	držet	k5eAaImIp3nS
převážnou	převážný	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
volených	volený	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
v	v	k7c6
celé	celý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
na	na	k7c6
federální	federální	k2eAgFnSc6d1
<g/>
,	,	kIx,
státní	státní	k2eAgFnSc6d1
i	i	k8xC
místní	místní	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nezávislí	závislý	k2eNgMnPc1d1
nebo	nebo	k8xC
kandidáti	kandidát	k1gMnPc1
menších	malý	k2eAgFnPc2d2
stran	strana	k1gFnPc2
bývají	bývat	k5eAaImIp3nP
nejúspěšnější	úspěšný	k2eAgMnPc1d3
převážně	převážně	k6eAd1
pouze	pouze	k6eAd1
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
místních	místní	k2eAgNnPc2d1
zastupitelstev	zastupitelstvo	k1gNnPc2
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
několik	několik	k4yIc1
nezávislých	závislý	k2eNgMnPc2d1
senátorů	senátor	k1gMnPc2
má	mít	k5eAaImIp3nS
svá	svůj	k3xOyFgNnPc4
křesla	křeslo	k1gNnPc4
i	i	k8xC
v	v	k7c6
senátu	senát	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
americké	americký	k2eAgFnSc6d1
politické	politický	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
je	být	k5eAaImIp3nS
Republikánská	republikánský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
vnímána	vnímat	k5eAaImNgFnS
jako	jako	k9
„	„	k?
<g/>
pravostředová	pravostředový	k2eAgFnSc1d1
<g/>
“	“	k?
nebo	nebo	k8xC
konzervativní	konzervativní	k2eAgFnSc1d1
<g/>
,	,	kIx,
Demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
pak	pak	k6eAd1
jako	jako	k9
„	„	k?
<g/>
levostředová	levostředový	k2eAgFnSc1d1
<g/>
“	“	k?
nebo	nebo	k8xC
liberální	liberální	k2eAgMnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velikost	velikost	k1gFnSc1
obou	dva	k4xCgFnPc2
stran	strana	k1gFnPc2
ovšem	ovšem	k9
způsobuje	způsobovat	k5eAaImIp3nS
značné	značný	k2eAgInPc4d1
rozdíly	rozdíl	k1gInPc4
i	i	k9
uvnitř	uvnitř	k7c2
jich	on	k3xPp3gMnPc2
samotných	samotný	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2021	#num#	k4
je	být	k5eAaImIp3nS
prezidentem	prezident	k1gMnSc7
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
demokrat	demokrat	k1gMnSc1
Joe	Joe	k1gMnSc1
Biden	Bidno	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Přední	přední	k2eAgFnPc1d1
politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Stranický	stranický	k2eAgInSc4d1
systém	systém	k1gInSc4
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
a	a	k8xC
Seznam	seznam	k1gInSc1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgInPc6d1
<g/>
.	.	kIx.
</s>
<s>
Republikánská	republikánský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
–	–	k?
strana	strana	k1gFnSc1
se	se	k3xPyFc4
hlásí	hlásit	k5eAaImIp3nS
ke	k	k7c3
konzervatismu	konzervatismus	k1gInSc3
a	a	k8xC
snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
např.	např.	kA
o	o	k7c4
rozvoj	rozvoj	k1gInSc4
podnikání	podnikání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
ze	z	k7c2
dvou	dva	k4xCgFnPc2
hlavních	hlavní	k2eAgFnPc2d1
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
status	status	k1gInSc4
pravice	pravice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
–	–	k?
oficiálně	oficiálně	k6eAd1
liberální	liberální	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
soustředí	soustředit	k5eAaPmIp3nS
se	se	k3xPyFc4
na	na	k7c4
rozvoj	rozvoj	k1gInSc4
sociální	sociální	k2eAgFnSc2d1
a	a	k8xC
státní	státní	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Americe	Amerika	k1gFnSc6
má	mít	k5eAaImIp3nS
status	status	k1gInSc4
levice	levice	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
Evropě	Evropa	k1gFnSc6
je	být	k5eAaImIp3nS
vnímána	vnímán	k2eAgFnSc1d1
spíše	spíše	k9
jako	jako	k8xS,k8xC
středová	středový	k2eAgFnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
je	být	k5eAaImIp3nS
ekonomicky	ekonomicky	k6eAd1
konzervativnější	konzervativní	k2eAgFnPc4d2
než	než	k8xS
evropské	evropský	k2eAgFnPc4d1
levicové	levicový	k2eAgFnPc4d1
strany	strana	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Zahraniční	zahraniční	k2eAgFnSc1d1
politika	politika	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Zahraniční	zahraniční	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
,	,	kIx,
George	George	k1gFnSc7
W.	W.	kA
Bush	Bush	k1gMnSc1
(	(	kIx(
<g/>
napravo	napravo	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c4
Camp	camp	k1gInSc4
Davidu	David	k1gMnSc3
v	v	k7c6
březnu	březen	k1gInSc6
2003	#num#	k4
při	při	k7c6
setkání	setkání	k1gNnSc6
s	s	k7c7
předsedou	předseda	k1gMnSc7
britské	britský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
Tonym	Tony	k1gMnSc7
Blairem	Blair	k1gMnSc7
</s>
<s>
Vývojové	vývojový	k2eAgFnPc1d1
tendence	tendence	k1gFnPc1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
ovlivňují	ovlivňovat	k5eAaImIp3nP
v	v	k7c6
širokém	široký	k2eAgNnSc6d1
měřítku	měřítko	k1gNnSc6
celosvětový	celosvětový	k2eAgInSc1d1
ekonomický	ekonomický	k2eAgInSc1d1
<g/>
,	,	kIx,
politický	politický	k2eAgInSc1d1
a	a	k8xC
vojenský	vojenský	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
vliv	vliv	k1gInSc1
vyvolává	vyvolávat	k5eAaImIp3nS
četné	četný	k2eAgFnSc2d1
diskuze	diskuze	k1gFnSc2
o	o	k7c6
jejich	jejich	k3xOp3gFnSc6
zahraniční	zahraniční	k2eAgFnSc6d1
politice	politika	k1gFnSc6
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téměř	téměř	k6eAd1
všechny	všechen	k3xTgInPc1
státy	stát	k1gInPc1
mají	mít	k5eAaImIp3nP
ve	v	k7c6
Washingtonu	Washington	k1gInSc6
svá	svůj	k3xOyFgNnPc4
velvyslanectví	velvyslanectví	k1gNnSc1
a	a	k8xC
mnoho	mnoho	k4c1
z	z	k7c2
nich	on	k3xPp3gFnPc2
konzuláty	konzulát	k1gInPc1
po	po	k7c6
celé	celý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
Írán	Írán	k1gInSc1
<g/>
,	,	kIx,
Severní	severní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
<g/>
,	,	kIx,
Bhútán	Bhútán	k1gInSc1
a	a	k8xC
Sýrie	Sýrie	k1gFnSc2
diplomatické	diplomatický	k2eAgInPc1d1
styky	styk	k1gInPc1
se	s	k7c7
Spojenými	spojený	k2eAgInPc7d1
státy	stát	k1gInPc7
neudržují	udržovat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
jsou	být	k5eAaImIp3nP
zakládajícím	zakládající	k2eAgInSc7d1
členem	člen	k1gInSc7
Organizace	organizace	k1gFnSc2
spojených	spojený	k2eAgInPc2d1
národů	národ	k1gInPc2
se	s	k7c7
stálým	stálý	k2eAgNnSc7d1
zastoupením	zastoupení	k1gNnSc7
v	v	k7c6
Radě	rada	k1gFnSc6
bezpečnosti	bezpečnost	k1gFnSc2
OSN	OSN	kA
a	a	k8xC
právem	právo	k1gNnSc7
veta	veto	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
členem	člen	k1gInSc7
mnoha	mnoho	k4c2
dalších	další	k2eAgFnPc2d1
mezinárodních	mezinárodní	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současnými	současný	k2eAgMnPc7d1
spojenci	spojenec	k1gMnPc7
jsou	být	k5eAaImIp3nP
Austrálie	Austrálie	k1gFnSc1
<g/>
,	,	kIx,
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
<g/>
,	,	kIx,
Izrael	Izrael	k1gInSc1
a	a	k8xC
státy	stát	k1gInPc1
NATO	NATO	kA
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Velké	velký	k2eAgFnSc2d1
Británie	Británie	k1gFnSc2
a	a	k8xC
Severního	severní	k2eAgNnSc2d1
Irska	Irsko	k1gNnSc2
má	mít	k5eAaImIp3nS
s	s	k7c7
USA	USA	kA
další	další	k2eAgInPc4d1
nadstandardní	nadstandardní	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
mají	mít	k5eAaImIp3nP
USA	USA	kA
úzké	úzký	k2eAgInPc4d1
diplomatické	diplomatický	k2eAgInPc4d1
<g/>
,	,	kIx,
ekonomické	ekonomický	k2eAgInPc4d1
a	a	k8xC
kulturní	kulturní	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
se	s	k7c7
státy	stát	k1gInPc7
<g/>
,	,	kIx,
s	s	k7c7
nimiž	jenž	k3xRgInPc7
sousedí	sousedit	k5eAaImIp3nS
<g/>
,	,	kIx,
tedy	tedy	k8xC
s	s	k7c7
Kanadou	Kanada	k1gFnSc7
a	a	k8xC
Mexikem	Mexiko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k8xC,k8xS
celek	celek	k1gInSc4
prošla	projít	k5eAaPmAgFnS
zahraniční	zahraniční	k2eAgFnSc1d1
politika	politika	k1gFnSc1
USA	USA	kA
především	především	k9
ve	v	k7c4
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
rozmanitým	rozmanitý	k2eAgInSc7d1
vývojem	vývoj	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Monroeova	Monroeův	k2eAgFnSc1d1
doktrína	doktrína	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1823	#num#	k4
definovala	definovat	k5eAaBmAgFnS
celou	celý	k2eAgFnSc4d1
západní	západní	k2eAgFnSc4d1
polokouli	polokoule	k1gFnSc4
za	za	k7c4
sféru	sféra	k1gFnSc4
vlivu	vliv	k1gInSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vítězné	vítězný	k2eAgFnSc6d1
španělsko-americké	španělsko-americký	k2eAgFnSc6d1
válce	válka	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1898	#num#	k4
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
anektovaly	anektovat	k5eAaBmAgInP
Havaj	Havaj	k1gFnSc4
<g/>
,	,	kIx,
Portoriko	Portoriko	k1gNnSc4
a	a	k8xC
Filipíny	Filipíny	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
tzv.	tzv.	kA
banánových	banánový	k2eAgFnPc6d1
válkách	válka	k1gFnPc6
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
intervenovaly	intervenovat	k5eAaImAgFnP
v	v	k7c6
několika	několik	k4yIc6
zemích	zem	k1gFnPc6
Karibiku	Karibik	k1gInSc2
a	a	k8xC
Střední	střední	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
století	století	k1gNnSc2
byla	být	k5eAaImAgFnS
snaha	snaha	k1gFnSc1
o	o	k7c4
izolacionismus	izolacionismus	k1gInSc4
a	a	k8xC
neangažování	neangažování	k1gNnSc1
se	se	k3xPyFc4
mimo	mimo	k7c4
západní	západní	k2eAgFnSc4d1
polokouli	polokoule	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
trend	trend	k1gInSc1
byl	být	k5eAaImAgInS
na	na	k7c4
čas	čas	k1gInSc4
přerušen	přerušen	k2eAgInSc4d1
během	během	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
do	do	k7c2
které	který	k3yIgFnSc2,k3yQgFnSc2,k3yRgFnSc2
se	se	k3xPyFc4
USA	USA	kA
zapojily	zapojit	k5eAaPmAgInP
v	v	k7c6
roce	rok	k1gInSc6
1917	#num#	k4
po	po	k7c6
potopení	potopení	k1gNnSc6
britské	britský	k2eAgFnSc2d1
lodě	loď	k1gFnSc2
Lusitania	Lusitanium	k1gNnSc2
s	s	k7c7
Američany	Američan	k1gMnPc7
na	na	k7c6
palubě	paluba	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
konci	konec	k1gInSc6
války	válka	k1gFnSc2
se	se	k3xPyFc4
odmítly	odmítnout	k5eAaPmAgInP
zapojit	zapojit	k5eAaPmF
do	do	k7c2
Společnosti	společnost	k1gFnSc2
národů	národ	k1gInPc2
(	(	kIx(
<g/>
předchůdkyně	předchůdkyně	k1gFnSc2
OSN	OSN	kA
<g/>
)	)	kIx)
a	a	k8xC
pokračovaly	pokračovat	k5eAaImAgInP
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
politice	politika	k1gFnSc6
z	z	k7c2
předválečného	předválečný	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgNnPc1
byla	být	k5eAaImAgNnP
definitivně	definitivně	k6eAd1
opuštěna	opustit	k5eAaPmNgNnP
po	po	k7c6
japonském	japonský	k2eAgInSc6d1
útoku	útok	k1gInSc6
na	na	k7c4
Pearl	Pearl	k1gInSc4
Harbor	Harbora	k1gFnPc2
a	a	k8xC
vstupem	vstup	k1gInSc7
USA	USA	kA
do	do	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
konci	konec	k1gInSc6
největšího	veliký	k2eAgInSc2d3
konfliktu	konflikt	k1gInSc2
v	v	k7c6
dějinách	dějiny	k1gFnPc6
pomáhaly	pomáhat	k5eAaImAgFnP
USA	USA	kA
obnovit	obnovit	k5eAaPmF
západní	západní	k2eAgFnSc4d1
Evropu	Evropa	k1gFnSc4
(	(	kIx(
<g/>
Marshallův	Marshallův	k2eAgInSc1d1
plán	plán	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
snažily	snažit	k5eAaImAgFnP
se	se	k3xPyFc4
zabránit	zabránit	k5eAaPmF
celosvětovému	celosvětový	k2eAgInSc3d1
rozmachu	rozmach	k1gInSc3
komunismu	komunismus	k1gInSc2
(	(	kIx(
<g/>
NATO	NATO	kA
<g/>
,	,	kIx,
korejská	korejský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
vietnamská	vietnamský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
tohoto	tento	k3xDgInSc2
boje	boj	k1gInSc2
podporovaly	podporovat	k5eAaImAgFnP
USA	USA	kA
také	také	k9
různé	různý	k2eAgFnPc1d1
nedemokratické	demokratický	k2eNgFnPc1d1
vlády	vláda	k1gFnPc1
(	(	kIx(
<g/>
Pinochet	Pinochet	k1gInSc1
<g/>
,	,	kIx,
Suharto	Suharta	k1gFnSc5
<g/>
,	,	kIx,
Čon	Čon	k1gMnSc1
Tu-hwan	Tu-hwan	k1gMnSc1
<g/>
,	,	kIx,
Perón	perón	k1gInSc1
<g/>
,	,	kIx,
Husajn	Husajn	k1gMnSc1
<g/>
,	,	kIx,
Duvalier	Duvalier	k1gMnSc1
<g/>
,	,	kIx,
podpora	podpora	k1gFnSc1
operace	operace	k1gFnSc2
Kondor	kondor	k1gMnSc1
<g/>
)	)	kIx)
či	či	k8xC
se	se	k3xPyFc4
nepřímo	přímo	k6eNd1
podílely	podílet	k5eAaImAgInP
na	na	k7c4
svržení	svržení	k1gNnSc4
vlád	vláda	k1gFnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
odporovaly	odporovat	k5eAaImAgInP
jejich	jejich	k3xOp3gNnPc3
zájmům	zájem	k1gInPc3
(	(	kIx(
<g/>
Mosaddek	Mosaddek	k1gMnSc1
v	v	k7c6
Íránu	Írán	k1gInSc6
<g/>
,	,	kIx,
Árbenz	Árbenz	k1gInSc1
v	v	k7c6
Guatemale	Guatemala	k1gFnSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
se	se	k3xPyFc4
o	o	k7c6
jejich	jejich	k3xOp3gNnSc6
svržení	svržení	k1gNnSc6
snažily	snažit	k5eAaImAgFnP
(	(	kIx(
<g/>
podpora	podpora	k1gFnSc1
contras	contrasa	k1gFnPc2
v	v	k7c6
Nikaragui	Nikaragua	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
vítězství	vítězství	k1gNnSc6
ve	v	k7c6
studené	studený	k2eAgFnSc6d1
válce	válka	k1gFnSc6
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
USA	USA	kA
v	v	k7c6
podstatě	podstata	k1gFnSc6
jedinou	jediný	k2eAgFnSc7d1
supervelmocí	supervelmoc	k1gFnSc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
jejich	jejich	k3xOp3gFnSc1
pozice	pozice	k1gFnSc1
však	však	k9
doznává	doznávat	k5eAaImIp3nS
v	v	k7c6
posledních	poslední	k2eAgInPc6d1
letech	let	k1gInPc6
určitou	určitý	k2eAgFnSc4d1
proměnu	proměna	k1gFnSc4
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
komplikovanou	komplikovaný	k2eAgFnSc7d1
situací	situace	k1gFnSc7
na	na	k7c6
Středním	střední	k2eAgInSc6d1
Východě	východ	k1gInSc6
a	a	k8xC
v	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
s	s	k7c7
mocenským	mocenský	k2eAgInSc7d1
vzestupem	vzestup	k1gInSc7
Číny	Čína	k1gFnSc2
<g/>
,	,	kIx,
podporovaným	podporovaný	k2eAgInSc7d1
jejím	její	k3xOp3gInSc7
rostoucím	rostoucí	k2eAgInSc7d1
ekonomickým	ekonomický	k2eAgInSc7d1
potenciálem	potenciál	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Některé	některý	k3yIgInPc1
problémy	problém	k1gInPc1
na	na	k7c6
začátku	začátek	k1gInSc6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Antiamerikanismus	antiamerikanismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Američtí	americký	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
hlídají	hlídat	k5eAaImIp3nP
ropné	ropný	k2eAgFnPc4d1
pole	pole	k1gFnPc4
v	v	k7c6
Rumajlá	Rumajlý	k2eAgFnSc1d1
během	během	k7c2
invaze	invaze	k1gFnSc2
do	do	k7c2
Iráku	Irák	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
</s>
<s>
Vlády	vláda	k1gFnPc1
USA	USA	kA
čelily	čelit	k5eAaImAgFnP
po	po	k7c6
světě	svět	k1gInSc6
antipatiím	antipatie	k1gFnPc3
mnoha	mnoho	k4c2
občanů	občan	k1gMnPc2
i	i	k8xC
států	stát	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
protestům	protest	k1gInPc3
v	v	k7c4
samotných	samotný	k2eAgInPc2d1
USA	USA	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
kvůli	kvůli	k7c3
metodám	metoda	k1gFnPc3
boje	boj	k1gInSc2
proti	proti	k7c3
teroristům	terorista	k1gMnPc3
<g/>
,	,	kIx,
mezi	mezi	k7c4
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
patří	patřit	k5eAaImIp3nS
zacházení	zacházení	k1gNnSc1
s	s	k7c7
vězni	vězeň	k1gMnPc7
ve	v	k7c6
věznicích	věznice	k1gFnPc6
Guantánamo	Guantánama	k1gFnSc5
a	a	k8xC
Abu-Ghrajb	Abu-Ghrajb	k1gInSc4
<g/>
,	,	kIx,
provozování	provozování	k1gNnSc4
tajných	tajný	k2eAgFnPc2d1
věznic	věznice	k1gFnPc2
CIA	CIA	kA
mimo	mimo	k7c4
<g />
.	.	kIx.
</s>
<s hack="1">
americké	americký	k2eAgInPc1d1
území	území	k1gNnSc6
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yQgInPc6,k3yRgInPc6,k3yIgInPc6
bylo	být	k5eAaImAgNnS
prováděno	provádět	k5eAaImNgNnS
mučení	mučení	k1gNnSc1
vězňů	vězeň	k1gMnPc2
(	(	kIx(
<g/>
Egypt	Egypt	k1gInSc1
<g/>
,	,	kIx,
Pákistán	Pákistán	k1gInSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
<g/>
,	,	kIx,
Litva	Litva	k1gFnSc1
nebo	nebo	k8xC
Rumunsko	Rumunsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
masové	masový	k2eAgNnSc1d1
odposlouchávání	odposlouchávání	k1gNnSc1
svých	svůj	k3xOyFgMnPc2
občanů	občan	k1gMnPc2
i	i	k8xC
spojenců	spojenec	k1gMnPc2
(	(	kIx(
<g/>
PATRIOT	patriot	k1gMnSc1
Act	Act	k1gMnSc1
<g/>
,	,	kIx,
Echelon	Echelon	k1gInSc1
a	a	k8xC
odhalení	odhalení	k1gNnSc1
Edwarda	Edward	k1gMnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
Snowdena	Snowdeno	k1gNnPc1
o	o	k7c6
činnosti	činnost	k1gFnSc6
NSA	NSA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
kvůli	kvůli	k7c3
válce	válka	k1gFnSc3
v	v	k7c6
Iráku	Irák	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
destabilizovala	destabilizovat	k5eAaBmAgFnS
region	region	k1gInSc4
a	a	k8xC
vyžádala	vyžádat	k5eAaPmAgFnS
si	se	k3xPyFc3
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
obětí	oběť	k1gFnPc2
(	(	kIx(
<g/>
odhady	odhad	k1gInPc1
se	se	k3xPyFc4
pohybují	pohybovat	k5eAaImIp3nP
zhruba	zhruba	k6eAd1
od	od	k7c2
115	#num#	k4
000	#num#	k4
do	do	k7c2
až	až	k6eAd1
461	#num#	k4
000	#num#	k4
mrtvých	mrtvý	k1gMnPc2
od	od	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
z	z	k7c2
důvodu	důvod	k1gInSc2
vojenské	vojenský	k2eAgFnSc2d1
intervence	intervence	k1gFnSc2
v	v	k7c6
Libyi	Libye	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
po	po	k7c6
svržení	svržení	k1gNnSc6
libyjského	libyjský	k2eAgInSc2d1
režimu	režim	k1gInSc2
propadla	propadnout	k5eAaPmAgFnS
do	do	k7c2
chaosu	chaos	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
Kritizována	kritizován	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
také	také	k9
úzká	úzký	k2eAgFnSc1d1
spolupráce	spolupráce	k1gFnSc1
s	s	k7c7
nedemokratickými	demokratický	k2eNgInPc7d1
režimy	režim	k1gInPc7
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
podpora	podpora	k1gFnSc1
vojenské	vojenský	k2eAgFnSc2d1
intervence	intervence	k1gFnSc2
v	v	k7c6
Jemenu	Jemen	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
Vztahy	vztah	k1gInPc1
mezi	mezi	k7c7
Spojenými	spojený	k2eAgInPc7d1
státy	stát	k1gInPc7
a	a	k8xC
Íránem	Írán	k1gInSc7
se	se	k3xPyFc4
prudce	prudko	k6eAd1
zhoršily	zhoršit	k5eAaPmAgInP
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k8xS
USA	USA	kA
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
odstoupily	odstoupit	k5eAaPmAgFnP
od	od	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
jaderné	jaderný	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
s	s	k7c7
Íránem	Írán	k1gInSc7
a	a	k8xC
zpřísnily	zpřísnit	k5eAaPmAgFnP
protiíránské	protiíránský	k2eAgFnPc4d1
sankce	sankce	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Americko-čínské	americko-čínský	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
</s>
<s>
Stav	stav	k1gInSc1
vztahů	vztah	k1gInPc2
mezi	mezi	k7c7
USA	USA	kA
a	a	k8xC
Čínou	Čína	k1gFnSc7
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgInSc7
ze	z	k7c2
základních	základní	k2eAgInPc2d1
parametrů	parametr	k1gInPc2
mezinárodní	mezinárodní	k2eAgFnSc2d1
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
vztahy	vztah	k1gInPc1
se	se	k3xPyFc4
zlepšily	zlepšit	k5eAaPmAgInP
v	v	k7c6
době	doba	k1gFnSc6
prezidentství	prezidentství	k1gNnPc2
Richarda	Richard	k1gMnSc2
Nixona	Nixon	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
provedl	provést	k5eAaPmAgMnS
zásadní	zásadní	k2eAgInSc4d1
obrat	obrat	k1gInSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
uznal	uznat	k5eAaPmAgMnS
vládu	vláda	k1gFnSc4
v	v	k7c6
Pekingu	Peking	k1gInSc6
jako	jako	k8xS,k8xC
legitimní	legitimní	k2eAgFnSc4d1
vládu	vláda	k1gFnSc4
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
uznání	uznání	k1gNnSc3
tzv.	tzv.	kA
politiky	politika	k1gFnSc2
jedné	jeden	k4xCgFnSc2
Číny	Čína	k1gFnSc2
a	a	k8xC
Tchaj-wan	Tchaj-wan	k1gInSc1
byl	být	k5eAaImAgInS
nucen	nutit	k5eAaImNgMnS
postoupit	postoupit	k5eAaPmF
své	svůj	k3xOyFgNnSc4
místo	místo	k1gNnSc4
v	v	k7c6
OSN	OSN	kA
Pekingu	Peking	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
se	se	k3xPyFc4
Čína	Čína	k1gFnSc1
pokoušela	pokoušet	k5eAaImAgFnS
ovlivnit	ovlivnit	k5eAaPmF
americké	americký	k2eAgFnPc4d1
prezidentské	prezidentský	k2eAgFnPc4d1
volby	volba	k1gFnPc4
zasíláním	zasílání	k1gNnSc7
finančních	finanční	k2eAgInPc2d1
příspěvků	příspěvek	k1gInPc2
na	na	k7c4
podporu	podpora	k1gFnSc4
kampaně	kampaň	k1gFnSc2
Billa	Bill	k1gMnSc2
Clintona	Clinton	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Americký	americký	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Barack	Baracka	k1gFnPc2
Obama	Obama	k?
a	a	k8xC
čínský	čínský	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Si	se	k3xPyFc3
Ťin-pching	Ťin-pching	k1gInSc4
na	na	k7c6
banketu	banket	k1gInSc6
v	v	k7c6
Pekingu	Peking	k1gInSc6
12	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2014	#num#	k4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
byla	být	k5eAaImAgFnS
Čína	Čína	k1gFnSc1
jako	jako	k8xS,k8xC
vlastník	vlastník	k1gMnSc1
amerických	americký	k2eAgInPc2d1
státních	státní	k2eAgInPc2d1
dluhopisů	dluhopis	k1gInPc2
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
1,244	1,244	k4
bilionu	bilion	k4xCgInSc2
USD	USD	kA
největším	veliký	k2eAgMnSc7d3
zahraničním	zahraniční	k2eAgMnSc7d1
věřitelem	věřitel	k1gMnSc7
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
době	doba	k1gFnSc6
vlády	vláda	k1gFnSc2
prezidentů	prezident	k1gMnPc2
George	Georg	k1gFnSc2
W.	W.	kA
Bushe	Bush	k1gMnSc4
a	a	k8xC
Baracka	Baracka	k1gFnSc1
Obamy	Obama	k1gFnSc2
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgInP
projevovat	projevovat	k5eAaImF
rozpory	rozpor	k1gInPc4
zapříčiněné	zapříčiněný	k2eAgNnSc1d1
mocenským	mocenský	k2eAgInSc7d1
vzestupem	vzestup	k1gInSc7
Číny	Čína	k1gFnSc2
jako	jako	k8xC,k8xS
vážného	vážný	k1gMnSc2
hráče	hráč	k1gMnSc2
v	v	k7c6
celosvětovém	celosvětový	k2eAgNnSc6d1
měřítku	měřítko	k1gNnSc6
<g/>
,	,	kIx,
hlavně	hlavně	k9
však	však	k9
v	v	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napětí	napětí	k1gNnSc1
mezi	mezi	k7c7
Čínou	Čína	k1gFnSc7
a	a	k8xC
Spojenými	spojený	k2eAgInPc7d1
státy	stát	k1gInPc7
vystupňovala	vystupňovat	k5eAaPmAgFnS
řada	řada	k1gFnSc1
incidentů	incident	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
hackerské	hackerský	k2eAgInPc4d1
útoky	útok	k1gInPc4
<g/>
,	,	kIx,
včetně	včetně	k7c2
krádeže	krádež	k1gFnSc2
osobních	osobní	k2eAgInPc2d1
údajů	údaj	k1gInPc2
21,5	21,5	k4
milionu	milion	k4xCgInSc2
zaměstnanců	zaměstnanec	k1gMnPc2
amerických	americký	k2eAgInPc2d1
federálních	federální	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
budování	budování	k1gNnSc1
umělých	umělý	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
v	v	k7c6
Jihočínském	jihočínský	k2eAgNnSc6d1
moři	moře	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
Ambice	ambice	k1gFnPc1
Číny	Čína	k1gFnSc2
jsou	být	k5eAaImIp3nP
umocněny	umocněn	k2eAgInPc1d1
její	její	k3xOp3gFnSc7
rostoucí	rostoucí	k2eAgFnSc7d1
ekonomickou	ekonomický	k2eAgFnSc7d1
sílou	síla	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
projevuje	projevovat	k5eAaImIp3nS
mj.	mj.	kA
v	v	k7c6
obrovských	obrovský	k2eAgInPc6d1
každoročních	každoroční	k2eAgInPc6d1
přebytcích	přebytek	k1gInPc6
Číny	Čína	k1gFnSc2
v	v	k7c6
jejím	její	k3xOp3gInSc6
obchodu	obchod	k1gInSc6
s	s	k7c7
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
názoru	názor	k1gInSc2
amerických	americký	k2eAgMnPc2d1
analytiků	analytik	k1gMnPc2
provádí	provádět	k5eAaImIp3nS
Čína	Čína	k1gFnSc1
protekcionistickou	protekcionistický	k2eAgFnSc4d1
obchodní	obchodní	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
pomocí	pomocí	k7c2
uměle	uměle	k6eAd1
udržovaného	udržovaný	k2eAgInSc2d1
nízkého	nízký	k2eAgInSc2d1
směnného	směnný	k2eAgInSc2d1
kurzu	kurz	k1gInSc2
čínské	čínský	k2eAgFnSc2d1
měny	měna	k1gFnSc2
a	a	k8xC
zároveň	zároveň	k6eAd1
mnohem	mnohem	k6eAd1
nižších	nízký	k2eAgFnPc2d2
mezd	mzda	k1gFnPc2
pracovníků	pracovník	k1gMnPc2
v	v	k7c6
čínském	čínský	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
může	moct	k5eAaImIp3nS
nabízet	nabízet	k5eAaImF
své	svůj	k3xOyFgNnSc4
zboží	zboží	k1gNnSc4
v	v	k7c6
USA	USA	kA
za	za	k7c4
velmi	velmi	k6eAd1
nízké	nízký	k2eAgFnPc4d1
ceny	cena	k1gFnPc4
a	a	k8xC
vytlačovat	vytlačovat	k5eAaImF
tak	tak	k9
domácí	domácí	k2eAgMnPc4d1
výrobce	výrobce	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
dosáhl	dosáhnout	k5eAaPmAgInS
čínský	čínský	k2eAgInSc1d1
přebytek	přebytek	k1gInSc1
v	v	k7c6
obchodu	obchod	k1gInSc6
s	s	k7c7
USA	USA	kA
375	#num#	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Setkání	setkání	k1gNnSc1
Donalda	Donald	k1gMnSc2
Trumpa	Trump	k1gMnSc2
a	a	k8xC
Kim	Kim	k1gMnSc2
Čong-una	Čong-una	k1gFnSc1
v	v	k7c6
létě	léto	k1gNnSc6
2018	#num#	k4
</s>
<s>
Po	po	k7c6
nástupu	nástup	k1gInSc6
Donalda	Donald	k1gMnSc2
Trumpa	Trump	k1gMnSc2
do	do	k7c2
funkce	funkce	k1gFnSc2
amerického	americký	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
v	v	k7c6
lednu	leden	k1gInSc6
2017	#num#	k4
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
situace	situace	k1gFnSc1
přeměnila	přeměnit	k5eAaPmAgFnS
v	v	k7c4
konflikt	konflikt	k1gInSc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
USA	USA	kA
již	již	k9
zavedly	zavést	k5eAaPmAgFnP
zvýšené	zvýšený	k2eAgFnPc1d1
celní	celní	k2eAgFnPc1d1
sazby	sazba	k1gFnPc1
na	na	k7c4
dovoz	dovoz	k1gInSc4
oceli	ocel	k1gFnSc2
a	a	k8xC
hliníku	hliník	k1gInSc2
z	z	k7c2
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
nedojde	dojít	k5eNaPmIp3nS
ke	k	k7c3
kompromisu	kompromis	k1gInSc3
<g/>
,	,	kIx,
hrozí	hrozit	k5eAaImIp3nS
nebezpečí	nebezpečí	k1gNnSc4
stupňující	stupňující	k2eAgFnSc2d1
se	se	k3xPyFc4
obchodní	obchodní	k2eAgFnSc2d1
války	válka	k1gFnSc2
mezi	mezi	k7c7
oběma	dva	k4xCgFnPc7
velmocemi	velmoc	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
Ohlášené	ohlášený	k2eAgInPc4d1
eventuální	eventuální	k2eAgInPc4d1
odvetné	odvetný	k2eAgInPc4d1
kroky	krok	k1gInPc4
Číny	Čína	k1gFnSc2
<g/>
,	,	kIx,
kterými	který	k3yQgInPc7,k3yIgInPc7,k3yRgInPc7
by	by	kYmCp3nS
zavedla	zavést	k5eAaPmAgFnS
vysoká	vysoká	k1gFnSc1
cla	clo	k1gNnSc2
na	na	k7c4
128	#num#	k4
druhů	druh	k1gInPc2
zboží	zboží	k1gNnSc2
dováženého	dovážený	k2eAgInSc2d1
z	z	k7c2
USA	USA	kA
<g/>
,	,	kIx,
neudělaly	udělat	k5eNaPmAgInP
ve	v	k7c6
Washingtonu	Washington	k1gInSc6
velký	velký	k2eAgInSc4d1
dojem	dojem	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
pozorovatelů	pozorovatel	k1gMnPc2
nejde	jít	k5eNaImIp3nS
prezidentu	prezident	k1gMnSc3
Trumpovi	Trump	k1gMnSc3
primárně	primárně	k6eAd1
o	o	k7c4
přístup	přístup	k1gInSc4
na	na	k7c4
čínské	čínský	k2eAgInPc4d1
trhy	trh	k1gInPc4
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
o	o	k7c4
zlepšení	zlepšení	k1gNnSc4
platební	platební	k2eAgFnSc2d1
bilance	bilance	k1gFnSc2
USA	USA	kA
a	a	k8xC
o	o	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byl	být	k5eAaImAgInS
celkově	celkově	k6eAd1
zbrzděn	zbrzdit	k5eAaPmNgInS
mocenský	mocenský	k2eAgInSc1d1
vzestup	vzestup	k1gInSc1
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
Amerika	Amerika	k1gFnSc1
hrozí	hrozit	k5eAaImIp3nS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
zavedla	zavést	k5eAaPmAgNnP
trestná	trestný	k2eAgNnPc1d1
cla	clo	k1gNnPc1
na	na	k7c4
další	další	k2eAgInPc4d1
čínské	čínský	k2eAgInPc4d1
dovozy	dovoz	k1gInPc4
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
150	#num#	k4
miliard	miliarda	k4xCgFnPc2
USD	USD	kA
ročně	ročně	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
Čína	Čína	k1gFnSc1
neví	vědět	k5eNaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
by	by	kYmCp3nS
takové	takový	k3xDgInPc4
obchodní	obchodní	k2eAgInPc4d1
válce	válec	k1gInPc4
zabránila	zabránit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
by	by	kYmCp3nS
podle	podle	k7c2
čínských	čínský	k2eAgMnPc2d1
odborníků	odborník	k1gMnPc2
zasáhla	zasáhnout	k5eAaPmAgFnS
jejich	jejich	k3xOp3gFnSc3
zemi	zem	k1gFnSc3
silněji	silně	k6eAd2
než	než	k8xS
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
vedou	vést	k5eAaImIp3nP
intenzivní	intenzivní	k2eAgNnPc1d1
jednání	jednání	k1gNnPc1
mezi	mezi	k7c7
oběma	dva	k4xCgInPc7
státy	stát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Pekingu	Peking	k1gInSc2
přiletěla	přiletět	k5eAaPmAgFnS
2	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2018	#num#	k4
americká	americký	k2eAgFnSc1d1
delegace	delegace	k1gFnSc1
<g/>
,	,	kIx,
ke	k	k7c3
které	který	k3yRgFnSc3,k3yQgFnSc3,k3yIgFnSc3
patřili	patřit	k5eAaImAgMnP
ministr	ministr	k1gMnSc1
financí	finance	k1gFnPc2
Steven	Stevna	k1gFnPc2
Mnuchin	Mnuchin	k1gInSc1
<g/>
,	,	kIx,
ministr	ministr	k1gMnSc1
obchodu	obchod	k1gInSc2
Wilbur	Wilbura	k1gFnPc2
Ross	Ross	k1gInSc1
a	a	k8xC
pověřenec	pověřenec	k1gMnSc1
pro	pro	k7c4
obchodní	obchodní	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
Robert	Robert	k1gMnSc1
Lighthizer	Lighthizer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Informované	informovaný	k2eAgInPc1d1
kruhy	kruh	k1gInPc1
ve	v	k7c6
Washingtonu	Washington	k1gInSc6
byly	být	k5eAaImAgInP
toho	ten	k3xDgInSc2
názoru	názor	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
cílem	cíl	k1gInSc7
této	tento	k3xDgFnSc2
delegace	delegace	k1gFnSc2
nebylo	být	k5eNaImAgNnS
ihned	ihned	k6eAd1
ukončit	ukončit	k5eAaPmF
spor	spor	k1gInSc4
s	s	k7c7
Čínou	Čína	k1gFnSc7
o	o	k7c4
obchodní	obchodní	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejznámější	známý	k2eAgFnSc1d3
organizace	organizace	k1gFnSc1
</s>
<s>
Vládní	vládní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
</s>
<s>
CIA	CIA	kA
–	–	k?
Central	Central	k1gMnSc1
Intelligence	Intelligenec	k1gMnSc2
Agency	Agenca	k1gFnSc2
–	–	k?
rozvědka	rozvědka	k1gFnSc1
pro	pro	k7c4
zahraničí	zahraničí	k1gNnSc4
</s>
<s>
FBI	FBI	kA
–	–	k?
Federal	Federal	k1gMnSc1
Bureau	Bureaus	k1gInSc2
of	of	k?
Investigation	Investigation	k1gInSc1
–	–	k?
kontrarozvědka	kontrarozvědka	k1gFnSc1
a	a	k8xC
federální	federální	k2eAgInSc1d1
vyšetřovací	vyšetřovací	k2eAgInSc1d1
úřad	úřad	k1gInSc1
</s>
<s>
NASA	NASA	kA
–	–	k?
National	National	k1gFnSc2
Aeronautic	Aeronautice	k1gFnPc2
and	and	k?
Space	Space	k1gMnSc1
Agency	Agenca	k1gFnSc2
–	–	k?
vládní	vládní	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
pro	pro	k7c4
kosmonautiku	kosmonautika	k1gFnSc4
</s>
<s>
NSA	NSA	kA
–	–	k?
National	National	k1gFnSc2
Security	Securita	k1gFnSc2
Agency	Agenca	k1gFnSc2
–	–	k?
Národní	národní	k2eAgFnSc1d1
bezpečnostní	bezpečnostní	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
</s>
<s>
NCIS	NCIS	kA
–	–	k?
Naval	navalit	k5eAaPmRp2nS
Criminal	Criminal	k1gMnSc1
Investigative	Investigativ	k1gInSc5
Service	Service	k1gFnSc1
–	–	k?
Námořní	námořní	k2eAgFnSc1d1
kriminální	kriminální	k2eAgFnSc1d1
vyšetřovací	vyšetřovací	k2eAgFnSc1d1
služba	služba	k1gFnSc1
</s>
<s>
Veřejné	veřejný	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
</s>
<s>
Národní	národní	k2eAgFnSc1d1
střelecká	střelecký	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
–	–	k?
National	National	k1gFnSc1
Rifle	rifle	k1gFnPc4
Association	Association	k1gInSc1
(	(	kIx(
<g/>
NRA	NRA	kA
<g/>
)	)	kIx)
</s>
<s>
Ozbrojené	ozbrojený	k2eAgFnPc1d1
síly	síla	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Ozbrojené	ozbrojený	k2eAgFnSc2d1
síly	síla	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Úderná	úderný	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
Kitty	Kitta	k1gFnSc2
Hawk	Hawk	k1gMnSc1
<g/>
,	,	kIx,
Ronald	Ronald	k1gMnSc1
Reagan	Reagan	k1gMnSc1
a	a	k8xC
Abraham	Abraham	k1gMnSc1
Lincoln	Lincoln	k1gMnSc1
s	s	k7c7
letadly	letadlo	k1gNnPc7
námořní	námořní	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
<g/>
,	,	kIx,
námořnictva	námořnictvo	k1gNnSc2
a	a	k8xC
letectva	letectvo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Ozbrojené	ozbrojený	k2eAgFnPc1d1
síly	síla	k1gFnPc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
se	se	k3xPyFc4
skládají	skládat	k5eAaImIp3nP
ze	z	k7c2
šesti	šest	k4xCc2
složek	složka	k1gFnPc2
<g/>
:	:	kIx,
armády	armáda	k1gFnSc2
<g/>
,	,	kIx,
letectva	letectvo	k1gNnSc2
<g/>
,	,	kIx,
námořnictva	námořnictvo	k1gNnSc2
<g/>
,	,	kIx,
námořní	námořní	k2eAgFnSc2d1
pěchoty	pěchota	k1gFnSc2
<g/>
,	,	kIx,
vesmírných	vesmírný	k2eAgFnPc2d1
sil	síla	k1gFnPc2
a	a	k8xC
pobřežní	pobřežní	k2eAgFnSc2d1
stráže	stráž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
aktivní	aktivní	k2eAgFnSc6d1
službě	služba	k1gFnSc6
je	být	k5eAaImIp3nS
1	#num#	k4
426	#num#	k4
026	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Mezi	mezi	k7c4
výzbroj	výzbroj	k1gFnSc4
americké	americký	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
patří	patřit	k5eAaImIp3nS
např.	např.	kA
letadlové	letadlový	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
<g/>
,	,	kIx,
jaderné	jaderný	k2eAgFnPc4d1
ponorky	ponorka	k1gFnPc4
<g/>
,	,	kIx,
„	„	k?
<g/>
neviditelná	viditelný	k2eNgFnSc1d1
<g/>
“	“	k?
letadla	letadlo	k1gNnPc4
či	či	k8xC
tanky	tank	k1gInPc4
Abrams	Abramsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokročilými	pokročilý	k2eAgFnPc7d1
technologiemi	technologie	k1gFnPc7
disponují	disponovat	k5eAaBmIp3nP
i	i	k8xC
výstroje	výstroj	k1gFnSc2
a	a	k8xC
vybavení	vybavení	k1gNnSc2
pěších	pěší	k2eAgFnPc2d1
a	a	k8xC
speciálních	speciální	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
jsou	být	k5eAaImIp3nP
jaderná	jaderný	k2eAgFnSc1d1
velmoc	velmoc	k1gFnSc1
vlastnící	vlastnící	k2eAgFnSc1d1
více	hodně	k6eAd2
než	než	k8xS
6000	#num#	k4
jaderných	jaderný	k2eAgFnPc2d1
hlavic	hlavice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Americká	americký	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
se	se	k3xPyFc4
zapojila	zapojit	k5eAaPmAgFnS
do	do	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
hlavních	hlavní	k2eAgInPc2d1
konfliktů	konflikt	k1gInPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomohla	pomoct	k5eAaPmAgFnS
porazit	porazit	k5eAaPmF
císařské	císařský	k2eAgInPc4d1
a	a	k8xC
později	pozdě	k6eAd2
i	i	k9
nacistické	nacistický	k2eAgNnSc4d1
Německo	Německo	k1gNnSc4
v	v	k7c6
obou	dva	k4xCgFnPc6
světových	světový	k2eAgFnPc6d1
válkách	válka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
mají	mít	k5eAaImIp3nP
USA	USA	kA
nejlépe	dobře	k6eAd3
vybavenou	vybavený	k2eAgFnSc4d1
a	a	k8xC
nejsilnější	silný	k2eAgFnSc4d3
armádu	armáda	k1gFnSc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
fakt	fakt	k1gInSc1
je	být	k5eAaImIp3nS
způsoben	způsobit	k5eAaPmNgInS
vysokými	vysoký	k2eAgInPc7d1
výdaji	výdaj	k1gInPc7
na	na	k7c4
obranu	obrana	k1gFnSc4
<g/>
,	,	kIx,
zhruba	zhruba	k6eAd1
3	#num#	k4
<g/>
/	/	kIx~
<g/>
4	#num#	k4
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k8xS
zbytek	zbytek	k1gInSc1
světa	svět	k1gInSc2
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
(	(	kIx(
<g/>
pro	pro	k7c4
rok	rok	k1gInSc4
2011	#num#	k4
–	–	k?
698	#num#	k4
mld.	mld.	k?
dolarů	dolar	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
rozvojem	rozvoj	k1gInSc7
moderních	moderní	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
<g/>
,	,	kIx,
jakými	jaký	k3yRgFnPc7,k3yIgFnPc7,k3yQgFnPc7
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
technologie	technologie	k1gFnPc1
stealth	stealtha	k1gFnPc2
<g/>
,	,	kIx,
mikrovlnné	mikrovlnný	k2eAgFnSc2d1
<g/>
,	,	kIx,
sonické	sonický	k2eAgFnSc2d1
a	a	k8xC
ULF	ULF	kA
(	(	kIx(
<g/>
Ultra-Low	Ultra-Low	k1gMnSc2
Frequency	Frequenca	k1gMnSc2
<g/>
)	)	kIx)
zbraně	zbraň	k1gFnPc1
nebo	nebo	k8xC
munice	munice	k1gFnPc1
z	z	k7c2
ochuzeného	ochuzený	k2eAgInSc2d1
uranu	uran	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Americká	americký	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
má	mít	k5eAaImIp3nS
vysokou	vysoký	k2eAgFnSc4d1
bojeschopnost	bojeschopnost	k1gFnSc4
díky	díky	k7c3
dlouholetým	dlouholetý	k2eAgFnPc3d1
zkušenostem	zkušenost	k1gFnPc3
<g/>
;	;	kIx,
od	od	k7c2
počátku	počátek	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
účastnila	účastnit	k5eAaImAgFnS
přibližně	přibližně	k6eAd1
stovky	stovka	k1gFnPc4
ozbrojených	ozbrojený	k2eAgMnPc2d1
konfliktů	konflikt	k1gInPc2
nebo	nebo	k8xC
intervencí	intervence	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
poslední	poslední	k2eAgFnSc6d1
době	doba	k1gFnSc6
operuje	operovat	k5eAaImIp3nS
například	například	k6eAd1
v	v	k7c6
Afghánistánu	Afghánistán	k1gInSc6
v	v	k7c6
rámci	rámec	k1gInSc6
operace	operace	k1gFnSc2
Operace	operace	k1gFnSc1
Trvalá	trvalá	k1gFnSc1
svoboda	svoboda	k1gFnSc1
a	a	k8xC
obecnějšího	obecní	k2eAgInSc2d2
konceptu	koncept	k1gInSc2
tzv.	tzv.	kA
války	válka	k1gFnSc2
proti	proti	k7c3
terorismu	terorismus	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
mají	mít	k5eAaImIp3nP
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
823	#num#	k4
vojenských	vojenský	k2eAgFnPc2d1
základen	základna	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
rozmístěných	rozmístěný	k2eAgInPc2d1
ve	v	k7c6
více	hodně	k6eAd2
než	než	k8xS
130	#num#	k4
ze	z	k7c2
195	#num#	k4
zemí	zem	k1gFnPc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
2	#num#	k4
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
Německu	Německo	k1gNnSc6
a	a	k8xC
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
<g/>
,	,	kIx,
od	od	k7c2
korejské	korejský	k2eAgFnSc2d1
války	válka	k1gFnSc2
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Koreji	Korea	k1gFnSc6
a	a	k8xC
v	v	k7c6
dalších	další	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
mezinárodních	mezinárodní	k2eAgFnPc2d1
vojenských	vojenský	k2eAgFnPc2d1
aliancí	aliance	k1gFnPc2
patří	patřit	k5eAaImIp3nS
USA	USA	kA
k	k	k7c3
NATO	NATO	kA
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
jsou	být	k5eAaImIp3nP
nejsilnějším	silný	k2eAgInSc7d3
členem	člen	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ozbrojené	ozbrojený	k2eAgFnPc1d1
síly	síla	k1gFnPc1
USA	USA	kA
jsou	být	k5eAaImIp3nP
subjektem	subjekt	k1gInSc7
s	s	k7c7
největším	veliký	k2eAgInSc7d3
dopadem	dopad	k1gInSc7
na	na	k7c4
znečištění	znečištění	k1gNnSc4
ovzduší	ovzduší	k1gNnSc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
též	též	k9
s	s	k7c7
největší	veliký	k2eAgFnSc7d3
spotřebou	spotřeba	k1gFnSc7
ropy	ropa	k1gFnSc2
(	(	kIx(
<g/>
podle	podle	k7c2
CIA	CIA	kA
World	World	k1gInSc1
Factbook	Factbook	k1gInSc1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
320	#num#	k4
000	#num#	k4
barelů	barel	k1gInPc2
denně	denně	k6eAd1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
15	#num#	k4
<g/>
×	×	k?
více	hodně	k6eAd2
než	než	k8xS
v	v	k7c6
době	doba	k1gFnSc6
II	II	kA
<g/>
.	.	kIx.
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
Její	její	k3xOp3gFnSc1
emise	emise	k1gFnSc1
navíc	navíc	k6eAd1
nejsou	být	k5eNaImIp3nP
připočítávány	připočítáván	k2eAgFnPc1d1
k	k	k7c3
emisím	emise	k1gFnPc3
USA	USA	kA
ani	ani	k8xC
země	zem	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
jejímž	jejíž	k3xOyRp3gNnSc6
území	území	k1gNnSc6
ozbrojené	ozbrojený	k2eAgFnPc1d1
síly	síla	k1gFnPc4
operují	operovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
operují	operovat	k5eAaImIp3nP
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
vojensky	vojensky	k6eAd1
podle	podle	k7c2
některých	některý	k3yIgInPc2
zdrojů	zdroj	k1gInPc2
ve	v	k7c6
více	hodně	k6eAd2
než	než	k8xS
stovce	stovka	k1gFnSc6
zemí	zem	k1gFnPc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Administrativní	administrativní	k2eAgNnSc1d1
členění	členění	k1gNnSc1
</s>
<s>
Státy	stát	k1gInPc1
a	a	k8xC
další	další	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Administrativní	administrativní	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
se	se	k3xPyFc4
skládají	skládat	k5eAaImIp3nP
z	z	k7c2
50	#num#	k4
států	stát	k1gInPc2
(	(	kIx(
<g/>
state	status	k1gInSc5
/	/	kIx~
mn	mn	k?
<g/>
.	.	kIx.
č.	č.	k?
states	states	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jednoho	jeden	k4xCgInSc2
federálního	federální	k2eAgInSc2d1
distriktu	distrikt	k1gInSc2
–	–	k?
District	District	k1gMnSc1
of	of	k?
Columbia	Columbia	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
leží	ležet	k5eAaImIp3nS
federální	federální	k2eAgNnSc1d1
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
D.C.	D.C.	k1gFnSc1
(	(	kIx(
<g/>
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
přímo	přímo	k6eAd1
pod	pod	k7c4
jurisdikci	jurisdikce	k1gFnSc4
Kongresu	kongres	k1gInSc2
<g/>
,	,	kIx,
nespadá	spadat	k5eNaImIp3nS,k5eNaPmIp3nS
pod	pod	k7c4
žádný	žádný	k3yNgInSc4
stát	stát	k1gInSc4
a	a	k8xC
oficiálně	oficiálně	k6eAd1
není	být	k5eNaImIp3nS
státem	stát	k1gInSc7
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
s	s	k7c7
nimi	on	k3xPp3gMnPc7
bývá	bývat	k5eAaImIp3nS
často	často	k6eAd1
uváděn	uvádět	k5eAaImNgInS
<g/>
)	)	kIx)
a	a	k8xC
dalších	další	k2eAgNnPc2d1
nezačleněných	začleněný	k2eNgNnPc2d1
území	území	k1gNnPc2
<g/>
,	,	kIx,
např.	např.	kA
ostrovních	ostrovní	k2eAgFnPc2d1
teritorií	teritorium	k1gNnPc2
Portoriko	Portoriko	k1gNnSc4
a	a	k8xC
Severní	severní	k2eAgFnSc2d1
Mariany	Mariana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Při	při	k7c6
podepsání	podepsání	k1gNnSc6
Deklarace	deklarace	k1gFnSc2
nezávislosti	nezávislost	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
se	se	k3xPyFc4
Unie	unie	k1gFnSc1
skládala	skládat	k5eAaImAgFnS
ze	z	k7c2
13	#num#	k4
zakládajících	zakládající	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
britskými	britský	k2eAgFnPc7d1
koloniemi	kolonie	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
států	stát	k1gInPc2
se	se	k3xPyFc4
posléze	posléze	k6eAd1
rozrostl	rozrůst	k5eAaPmAgInS
při	při	k7c6
expanzi	expanze	k1gFnSc6
na	na	k7c4
západ	západ	k1gInSc4
<g/>
,	,	kIx,
dobytím	dobytí	k1gNnSc7
či	či	k8xC
nákupem	nákup	k1gInSc7
nových	nový	k2eAgNnPc2d1
území	území	k1gNnPc2
americkou	americký	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
a	a	k8xC
dělením	dělení	k1gNnSc7
existujících	existující	k2eAgInPc2d1
států	stát	k1gInPc2
(	(	kIx(
<g/>
Západní	západní	k2eAgFnPc1d1
Virginie	Virginie	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Alabama	Alabama	k1gFnSc1
</s>
<s>
Aljaška	Aljaška	k1gFnSc1
(	(	kIx(
<g/>
Alaska	Alaska	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Arizona	Arizona	k1gFnSc1
</s>
<s>
Arkansas	Arkansas	k1gInSc1
</s>
<s>
Colorado	Colorado	k1gNnSc1
</s>
<s>
Connecticut	Connecticut	k1gMnSc1
</s>
<s>
Delaware	Delawar	k1gMnSc5
</s>
<s>
Florida	Florida	k1gFnSc1
</s>
<s>
Georgie	Georgie	k1gFnSc1
(	(	kIx(
<g/>
Georgia	Georgia	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Havaj	Havaj	k1gFnSc1
(	(	kIx(
<g/>
Hawaii	Hawaie	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Idaho	Idaze	k6eAd1
</s>
<s>
Illinois	Illinois	k1gFnSc1
</s>
<s>
Indiana	Indiana	k1gFnSc1
</s>
<s>
Iowa	Iowa	k6eAd1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Dakota	Dakota	k1gFnSc1
(	(	kIx(
<g/>
South	South	k1gInSc1
Dakota	Dakota	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Karolína	Karolína	k1gFnSc1
(	(	kIx(
<g/>
South	South	k1gInSc1
Carolina	Carolina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kalifornie	Kalifornie	k1gFnSc2
(	(	kIx(
<g/>
California	Californium	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Kansas	Kansas	k1gInSc1
</s>
<s>
Kentucky	Kentucky	k6eAd1
</s>
<s>
Louisiana	Louisiana	k1gFnSc1
</s>
<s>
Maine	Mainout	k5eAaImIp3nS,k5eAaPmIp3nS
</s>
<s>
Maryland	Maryland	k1gInSc1
</s>
<s>
Massachusetts	Massachusetts	k1gNnSc1
</s>
<s>
Michigan	Michigan	k1gInSc1
</s>
<s>
Minnesota	Minnesota	k1gFnSc1
</s>
<s>
Mississippi	Mississippi	k1gFnSc1
</s>
<s>
Missouri	Missouri	k1gFnSc1
</s>
<s>
Montana	Montana	k1gFnSc1
</s>
<s>
Nebraska	Nebraska	k1gFnSc1
</s>
<s>
Nevada	Nevada	k1gFnSc1
</s>
<s>
New	New	k?
Hampshire	Hampshir	k1gMnSc5
</s>
<s>
New	New	k?
Jersey	Jersey	k1gInPc1
</s>
<s>
New	New	k?
York	York	k1gInSc1
</s>
<s>
Nové	Nové	k2eAgNnSc1d1
Mexiko	Mexiko	k1gNnSc1
(	(	kIx(
<g/>
New	New	k1gMnSc1
Mexico	Mexico	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Ohio	Ohio	k1gNnSc1
</s>
<s>
Oklahoma	Oklahoma	k1gFnSc1
</s>
<s>
Oregon	Oregon	k1gMnSc1
</s>
<s>
Pensylvánie	Pensylvánie	k1gFnSc1
(	(	kIx(
<g/>
Pennsylvania	Pennsylvanium	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Rhode	Rhodos	k1gInSc5
Island	Island	k1gInSc1
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Dakota	Dakota	k1gFnSc1
(	(	kIx(
<g/>
North	North	k1gInSc1
Dakota	Dakota	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Karolína	Karolína	k1gFnSc1
(	(	kIx(
<g/>
North	North	k1gInSc1
Carolina	Carolina	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Tennessee	Tennessee	k6eAd1
</s>
<s>
Texas	Texas	k1gInSc1
</s>
<s>
Utah	Utah	k1gInSc1
</s>
<s>
Vermont	Vermont	k1gMnSc1
</s>
<s>
Virginie	Virginie	k1gFnPc1
(	(	kIx(
<g/>
Virginia	Virginium	k1gNnPc1
<g/>
)	)	kIx)
</s>
<s>
Washington	Washington	k1gInSc1
</s>
<s>
Wisconsin	Wisconsin	k1gMnSc1
</s>
<s>
Wyoming	Wyoming	k1gInSc1
</s>
<s>
Západní	západní	k2eAgFnSc1d1
Virginie	Virginie	k1gFnSc1
(	(	kIx(
<g/>
West	West	k1gInSc1
Virginia	Virginium	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Města	město	k1gNnSc2
a	a	k8xC
metropolitní	metropolitní	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Metropolitní	metropolitní	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgInPc6d1
<g/>
.	.	kIx.
</s>
<s>
New	New	k?
York	York	k1gInSc1
</s>
<s>
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
</s>
<s>
Více	hodně	k6eAd2
než	než	k8xS
83	#num#	k4
%	%	kIx~
Američanů	Američan	k1gMnPc2
žije	žít	k5eAaImIp3nS
v	v	k7c6
jedné	jeden	k4xCgFnSc6
z	z	k7c2
361	#num#	k4
metropolitních	metropolitní	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
bylo	být	k5eAaImAgNnS
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
254	#num#	k4
měst	město	k1gNnPc2
s	s	k7c7
populací	populace	k1gFnSc7
větší	veliký	k2eAgFnSc7d2
než	než	k8xS
100	#num#	k4
tisíc	tisíc	k4xCgInPc2
<g/>
,	,	kIx,
25	#num#	k4
měst	město	k1gNnPc2
mělo	mít	k5eAaImAgNnS
více	hodně	k6eAd2
než	než	k8xS
1	#num#	k4
milion	milion	k4xCgInSc1
obyvatel	obyvatel	k1gMnPc2
a	a	k8xC
4	#num#	k4
města	město	k1gNnSc2
(	(	kIx(
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
Los	los	k1gInSc1
Angeles	Angeles	k1gInSc1
<g/>
,	,	kIx,
Chicago	Chicago	k1gNnSc1
a	a	k8xC
Houston	Houston	k1gInSc1
<g/>
)	)	kIx)
více	hodně	k6eAd2
než	než	k8xS
2	#num#	k4
miliony	milion	k4xCgInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
je	být	k5eAaImIp3nS
50	#num#	k4
metropolitních	metropolitní	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
mají	mít	k5eAaImIp3nP
počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
přes	přes	k7c4
1	#num#	k4
milion	milion	k4xCgInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejrychleji	rychle	k6eAd3
rostoucí	rostoucí	k2eAgFnPc4d1
aglomerace	aglomerace	k1gFnPc4
patří	patřit	k5eAaImIp3nS
Dallas	Dallas	k1gInSc1
<g/>
,	,	kIx,
Houston	Houston	k1gInSc1
<g/>
,	,	kIx,
Atlanta	Atlanta	k1gFnSc1
a	a	k8xC
Phoenix	Phoenix	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Šest	šest	k4xCc1
nejlidnatějších	lidnatý	k2eAgNnPc2d3
měst	město	k1gNnPc2
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pořadí	pořadí	k1gNnSc1
</s>
<s>
Město	město	k1gNnSc1
</s>
<s>
Populace	populace	k1gFnSc1
samotnéhoměsta	samotnéhoměsta	k1gFnSc1
</s>
<s>
Metropolitní	metropolitní	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
</s>
<s>
Region	region	k1gInSc1
</s>
<s>
populace	populace	k1gFnPc1
</s>
<s>
pořadí	pořadí	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
8	#num#	k4
214	#num#	k4
42618	#num#	k4
818	#num#	k4
5361	#num#	k4
<g/>
Severovýchod	severovýchod	k1gInSc1
</s>
<s>
2	#num#	k4
<g/>
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
<g/>
3	#num#	k4
849	#num#	k4
37812	#num#	k4
950	#num#	k4
1292	#num#	k4
<g/>
Západ	západ	k1gInSc1
</s>
<s>
3	#num#	k4
<g/>
Chicago	Chicago	k1gNnSc1
<g/>
2	#num#	k4
833	#num#	k4
3219	#num#	k4
505	#num#	k4
7483	#num#	k4
<g/>
Středozápad	středozápad	k1gInSc1
</s>
<s>
4	#num#	k4
<g/>
Houston	Houston	k1gInSc1
<g/>
2	#num#	k4
144	#num#	k4
4915	#num#	k4
539	#num#	k4
9496	#num#	k4
<g/>
Jih	jih	k1gInSc1
</s>
<s>
5	#num#	k4
<g/>
Phoenix	Phoenix	k1gInSc1
<g/>
1	#num#	k4
512	#num#	k4
9864	#num#	k4
039	#num#	k4
18213	#num#	k4
<g/>
Západ	západ	k1gInSc1
</s>
<s>
6	#num#	k4
<g/>
Filadelfie	Filadelfie	k1gFnSc1
<g/>
1	#num#	k4
448	#num#	k4
3945	#num#	k4
826	#num#	k4
7425	#num#	k4
<g/>
Severovýchod	severovýchod	k1gInSc1
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Obyvatelstvo	obyvatelstvo	k1gNnSc4
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
v	v	k7c6
USA	USA	kA
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
</s>
<s>
Populace	populace	k1gFnSc1
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Populace	populace	k1gFnSc1
<g/>
310	#num#	k4
232	#num#	k4
863	#num#	k4
<g/>
(	(	kIx(
<g/>
červenec	červenec	k1gInSc1
2010	#num#	k4
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
Populační	populační	k2eAgInSc1d1
růst	růst	k1gInSc1
<g/>
0,97	0,97	k4
%	%	kIx~
</s>
<s>
Nezaregistrovaní	zaregistrovaný	k2eNgMnPc1d1
imigrantikolem	imigrantikol	k1gInSc7
12	#num#	k4
milionů	milion	k4xCgInPc2
</s>
<s>
Občané	občan	k1gMnPc1
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
3	#num#	k4
až	až	k9
7	#num#	k4
milionů	milion	k4xCgInPc2
</s>
<s>
Etnika	etnikum	k1gNnPc1
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Etnické	etnický	k2eAgNnSc1d1
skup	skoupit	k5eAaImRp2nS,k5eAaPmRp2nS
<g/>
.	.	kIx.
s	s	k7c7
více	hodně	k6eAd2
než	než	k8xS
milionem	milion	k4xCgInSc7
členů	člen	k1gMnPc2
<g/>
31	#num#	k4
</s>
<s>
z	z	k7c2
toho	ten	k3xDgNnSc2
</s>
<s>
němečtí	německý	k2eAgMnPc1d1
Američané	Američan	k1gMnPc1
<g/>
15,6	15,6	k4
%	%	kIx~
</s>
<s>
Afroameričané	Afroameričan	k1gMnPc1
<g/>
12,9	12,9	k4
%	%	kIx~
</s>
<s>
Iroameričané	Iroameričan	k1gMnPc1
<g/>
10,8	10,8	k4
%	%	kIx~
</s>
<s>
Angloameričané	Angloameričan	k1gMnPc1
<g/>
8,7	8,7	k4
%	%	kIx~
</s>
<s>
mexičtí	mexický	k2eAgMnPc1d1
Američané	Američan	k1gMnPc1
<g/>
6,5	6,5	k4
%	%	kIx~
</s>
<s>
italští	italský	k2eAgMnPc1d1
Američané	Američan	k1gMnPc1
<g/>
5,6	5,6	k4
%	%	kIx~
</s>
<s>
skandinávští	skandinávský	k2eAgMnPc1d1
Američané	Američan	k1gMnPc1
<g/>
3,7	3,7	k4
%	%	kIx~
</s>
<s>
polští	polský	k2eAgMnPc1d1
Američané	Američan	k1gMnPc1
<g/>
3,2	3,2	k4
%	%	kIx~
</s>
<s>
Frankoameričané	Frankoameričan	k1gMnPc1
<g/>
3,0	3,0	k4
%	%	kIx~
</s>
<s>
domorodí	domorodý	k2eAgMnPc1d1
Američané	Američan	k1gMnPc1
(	(	kIx(
<g/>
Indiáni	Indián	k1gMnPc1
<g/>
)	)	kIx)
<g/>
2,8	2,8	k4
%	%	kIx~
</s>
<s>
Rasy	rasa	k1gFnPc1
a	a	k8xC
etnika	etnikum	k1gNnPc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
běloši	běloch	k1gMnPc1
<g/>
80,0	80,0	k4
%	%	kIx~
</s>
<s>
černoši	černoch	k1gMnPc1
(	(	kIx(
<g/>
Afroameričané	Afroameričan	k1gMnPc1
<g/>
)	)	kIx)
<g/>
12,8	12,8	k4
%	%	kIx~
</s>
<s>
Asiaté	Asiat	k1gMnPc1
<g/>
4,4	4,4	k4
%	%	kIx~
</s>
<s>
domorodí	domorodý	k2eAgMnPc1d1
Američané	Američan	k1gMnPc1
(	(	kIx(
<g/>
Indiáni	Indián	k1gMnPc1
<g/>
)	)	kIx)
a	a	k8xC
Inuité	Inuita	k1gMnPc1
(	(	kIx(
<g/>
Eskymáci	Eskymák	k1gMnPc1
<g/>
)	)	kIx)
<g/>
1,0	1,0	k4
%	%	kIx~
</s>
<s>
Pacifičtí	pacifický	k2eAgMnPc1d1
ostrované	ostrovan	k1gMnPc1
<g/>
0,2	0,2	k4
%	%	kIx~
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1
<g/>
/	/	kIx~
<g/>
míšenci	míšenec	k1gMnSc3
<g/>
1,6	1,6	k4
%	%	kIx~
</s>
<s>
Hispánci	Hispánek	k1gMnPc1
(	(	kIx(
<g/>
zároveň	zároveň	k6eAd1
členy	člen	k1gInPc4
jakékoliv	jakýkoliv	k3yIgFnSc2
rasy	rasa	k1gFnSc2
<g/>
)	)	kIx)
<g/>
15,1	15,1	k4
%	%	kIx~
</s>
<s>
Jazyky	jazyk	k1gInPc1
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
angličtina	angličtina	k1gFnSc1
(	(	kIx(
<g/>
pouze	pouze	k6eAd1
<g/>
)	)	kIx)
<g/>
214,8	214,8	k4
milionu	milion	k4xCgInSc2
</s>
<s>
španělština	španělština	k1gFnSc1
<g/>
29,7	29,7	k4
milionu	milion	k4xCgInSc2
</s>
<s>
čínština	čínština	k1gFnSc1
<g/>
2,2	2,2	k4
milionu	milion	k4xCgInSc2
</s>
<s>
francouzština	francouzština	k1gFnSc1
včetně	včetně	k7c2
kreolské	kreolský	k2eAgFnSc2d1
francouzštiny	francouzština	k1gFnSc2
<g/>
1,4	1,4	k4
milionu	milion	k4xCgInSc2
</s>
<s>
Tagalogština	Tagalogština	k1gFnSc1
<g/>
1,3	1,3	k4
milionu	milion	k4xCgInSc2
</s>
<s>
němčina	němčina	k1gFnSc1
<g/>
1,1	1,1	k4
milionu	milion	k4xCgInSc2
</s>
<s>
vietnamština	vietnamština	k1gFnSc1
<g/>
1,1	1,1	k4
milionu	milion	k4xCgInSc2
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
třetí	třetí	k4xOgFnSc7
nejlidnatější	lidnatý	k2eAgFnSc7d3
zemí	zem	k1gFnSc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2006	#num#	k4
překročily	překročit	k5eAaPmAgFnP
hranici	hranice	k1gFnSc4
300	#num#	k4
milionů	milion	k4xCgInPc2
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
ilegálních	ilegální	k2eAgMnPc2d1
imigrantů	imigrant	k1gMnPc2
se	se	k3xPyFc4
odhaduje	odhadovat	k5eAaImIp3nS
na	na	k7c4
12	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Populační	populační	k2eAgInSc1d1
růst	růst	k1gInSc1
je	být	k5eAaImIp3nS
0,97	0,97	k4
%	%	kIx~
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
celkem	celkem	k6eAd1
vysoké	vysoký	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
např.	např.	kA
v	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
Evropskou	evropský	k2eAgFnSc7d1
unií	unie	k1gFnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
přírůstek	přírůstek	k1gInSc1
pouze	pouze	k6eAd1
0,16	0,16	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
USA	USA	kA
zemí	zem	k1gFnPc2
prvního	první	k4xOgInSc2
světa	svět	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
relativně	relativně	k6eAd1
vysoká	vysoký	k2eAgFnSc1d1
i	i	k8xC
dětská	dětský	k2eAgFnSc1d1
úmrtnost	úmrtnost	k1gFnSc1
(	(	kIx(
<g/>
6,14	6,14	k4
ppm	ppm	k?
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
i	i	k8xC
počet	počet	k1gInSc1
lidí	člověk	k1gMnPc2
pod	pod	k7c7
hranicí	hranice	k1gFnSc7
chudoby	chudoba	k1gFnSc2
(	(	kIx(
<g/>
47	#num#	k4
milionů	milion	k4xCgInPc2
<g/>
,	,	kIx,
tj.	tj.	kA
15,15	15,15	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
ve	v	k7c6
vězení	vězení	k1gNnSc6
(	(	kIx(
<g/>
2,245	2,245	k4
milionu	milion	k4xCgInSc2
neboli	neboli	k8xC
751	#num#	k4
na	na	k7c4
100	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
;	;	kIx,
tj.	tj.	kA
asi	asi	k9
7,5	7,5	k4
<g/>
×	×	k?
více	hodně	k6eAd2
než	než	k8xS
průměr	průměr	k1gInSc1
EU	EU	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
USA	USA	kA
mají	mít	k5eAaImIp3nP
velmi	velmi	k6eAd1
rozmanitou	rozmanitý	k2eAgFnSc4d1
populaci	populace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zemi	zem	k1gFnSc6
je	být	k5eAaImIp3nS
31	#num#	k4
etnických	etnický	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
s	s	k7c7
počtem	počet	k1gInSc7
obyvatel	obyvatel	k1gMnPc2
větším	většit	k5eAaImIp1nS
než	než	k8xS
jeden	jeden	k4xCgMnSc1
milion	milion	k4xCgInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
způsobeno	způsobit	k5eAaPmNgNnS
imigrací	imigrace	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
provázela	provázet	k5eAaImAgFnS
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
po	po	k7c4
celou	celá	k1gFnSc4
jejich	jejich	k3xOp3gFnSc4
historii	historie	k1gFnSc4
–	–	k?
a	a	k8xC
provází	provázet	k5eAaImIp3nS
je	on	k3xPp3gInPc4
i	i	k9
nadále	nadále	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
do	do	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
stěhovali	stěhovat	k5eAaImAgMnP
hlavně	hlavně	k9
Evropané	Evropan	k1gMnPc1
<g/>
,	,	kIx,
v	v	k7c6
posledních	poslední	k2eAgNnPc6d1
několika	několik	k4yIc6
dekádách	dekáda	k1gFnPc6
imigranty	imigrant	k1gMnPc7
představují	představovat	k5eAaImIp3nP
přistěhovalci	přistěhovalec	k1gMnPc1
z	z	k7c2
Latinské	latinský	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
hlavně	hlavně	k9
z	z	k7c2
Mexika	Mexiko	k1gNnSc2
<g/>
,	,	kIx,
Kuby	Kuba	k1gFnSc2
a	a	k8xC
Portorika	Portorico	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
z	z	k7c2
Asie	Asie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každoročně	každoročně	k6eAd1
se	se	k3xPyFc4
do	do	k7c2
USA	USA	kA
přistěhuje	přistěhovat	k5eAaPmIp3nS
okolo	okolo	k7c2
jednoho	jeden	k4xCgInSc2
milionu	milion	k4xCgInSc2
legálních	legální	k2eAgMnPc2d1
imigrantů	imigrant	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Etnické	etnický	k2eAgNnSc1d1
složení	složení	k1gNnSc1
</s>
<s>
Mapa	mapa	k1gFnSc1
zobrazující	zobrazující	k2eAgFnSc2d1
nejčastější	častý	k2eAgFnSc2d3
etnický	etnický	k2eAgInSc4d1
původ	původ	k1gInSc4
v	v	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
okresech	okres	k1gInPc6
</s>
<s>
Podle	podle	k7c2
odhadu	odhad	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
tvoří	tvořit	k5eAaImIp3nS
74,7	74,7	k4
%	%	kIx~
obyvatelstva	obyvatelstvo	k1gNnSc2
běloši	běloch	k1gMnPc1
<g/>
,	,	kIx,
12,1	12,1	k4
%	%	kIx~
černoši	černoch	k1gMnPc1
<g/>
/	/	kIx~
<g/>
Afroameričané	Afroameričan	k1gMnPc1
<g/>
,	,	kIx,
4,3	4,3	k4
%	%	kIx~
Asiaté	Asiat	k1gMnPc1
<g/>
,	,	kIx,
Indiáni	Indián	k1gMnPc1
a	a	k8xC
Eskymáci	Eskymák	k1gMnPc1
pak	pak	k6eAd1
0,8	0,8	k4
%	%	kIx~
a	a	k8xC
jiné	jiný	k2eAgFnSc2d1
rasy	rasa	k1gFnSc2
celkem	celkem	k6eAd1
7,9	7,9	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
těchto	tento	k3xDgNnPc6
číslech	číslo	k1gNnPc6
je	být	k5eAaImIp3nS
zahrnuto	zahrnut	k2eAgNnSc1d1
14,5	14,5	k4
%	%	kIx~
těch	ten	k3xDgMnPc2
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
považují	považovat	k5eAaImIp3nP
za	za	k7c4
jednu	jeden	k4xCgFnSc4
z	z	k7c2
těchto	tento	k3xDgFnPc2
etnických	etnický	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
a	a	k8xC
zároveň	zároveň	k6eAd1
za	za	k7c4
Hispánce	Hispánec	k1gMnPc4
nebo	nebo	k8xC
Latinoameričany	Latinoameričan	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
sčítání	sčítání	k1gNnSc6
obyvatelstva	obyvatelstvo	k1gNnSc2
v	v	k7c6
USA	USA	kA
je	být	k5eAaImIp3nS
totiž	totiž	k9
otázka	otázka	k1gFnSc1
rasy	rasa	k1gFnSc2
jedna	jeden	k4xCgFnSc1
věc	věc	k1gFnSc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
otázka	otázka	k1gFnSc1
hispánského	hispánský	k2eAgInSc2d1
původu	původ	k1gInSc2
věc	věc	k1gFnSc1
druhá	druhý	k4xOgFnSc1
<g/>
.	.	kIx.
</s>
<s>
Nejrychleji	rychle	k6eAd3
rostoucí	rostoucí	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
jsou	být	k5eAaImIp3nP
obyvatelé	obyvatel	k1gMnPc1
hispánského	hispánský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
tvořili	tvořit	k5eAaImAgMnP
10	#num#	k4
%	%	kIx~
obyvatelstva	obyvatelstvo	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
12,5	12,5	k4
%	%	kIx~
a	a	k8xC
podle	podle	k7c2
odhadu	odhad	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
14,5	14,5	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysoký	vysoký	k2eAgInSc1d1
růst	růst	k1gInSc1
je	být	k5eAaImIp3nS
daný	daný	k2eAgInSc1d1
jednak	jednak	k8xC
imigrací	imigrace	k1gFnPc2
<g/>
,	,	kIx,
jednak	jednak	k8xC
vysokou	vysoký	k2eAgFnSc7d1
porodností	porodnost	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
<g/>
,	,	kIx,
Arizoně	Arizona	k1gFnSc6
<g/>
,	,	kIx,
Novém	nový	k2eAgNnSc6d1
Mexiku	Mexiko	k1gNnSc6
a	a	k8xC
v	v	k7c6
Texasu	Texas	k1gInSc6
se	se	k3xPyFc4
pravděpodobně	pravděpodobně	k6eAd1
brzy	brzy	k6eAd1
stanou	stanout	k5eAaPmIp3nP
většinou	většina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInSc4d3
podíl	podíl	k1gInSc4
Hispánců	Hispánec	k1gMnPc2
je	být	k5eAaImIp3nS
v	v	k7c6
Novém	nový	k2eAgNnSc6d1
Mexiku	Mexiko	k1gNnSc6
–	–	k?
43,6	43,6	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mexičané	Mexičan	k1gMnPc1
obývají	obývat	k5eAaImIp3nP
především	především	k9
jihozápad	jihozápad	k1gInSc4
a	a	k8xC
západ	západ	k1gInSc4
USA	USA	kA
<g/>
,	,	kIx,
Portoričané	Portoričan	k1gMnPc1
severovýchod	severovýchod	k1gInSc4
a	a	k8xC
Kubánci	Kubánec	k1gMnPc1
Floridu	Florida	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nehispánští	hispánský	k2eNgMnPc1d1
běloši	běloch	k1gMnPc1
(	(	kIx(
<g/>
většinou	většinou	k6eAd1
evropského	evropský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
)	)	kIx)
tvořili	tvořit	k5eAaImAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
85	#num#	k4
%	%	kIx~
populace	populace	k1gFnSc2
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
představují	představovat	k5eAaImIp3nP
asi	asi	k9
65	#num#	k4
%	%	kIx~
a	a	k8xC
podle	podle	k7c2
prognóz	prognóza	k1gFnPc2
klesne	klesnout	k5eAaPmIp3nS
jejich	jejich	k3xOp3gInSc1
podíl	podíl	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2042	#num#	k4
pod	pod	k7c4
50	#num#	k4
%	%	kIx~
<g/>
;	;	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2050	#num#	k4
mají	mít	k5eAaImIp3nP
tvořit	tvořit	k5eAaImF
46,3	46,3	k4
%	%	kIx~
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
93	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
Nehispánští	hispánský	k2eNgMnPc1d1
běloši	běloch	k1gMnPc1
tvořili	tvořit	k5eAaImAgMnP
v	v	k7c6
letech	let	k1gInPc6
2008	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
51,4	51,4	k4
%	%	kIx~
všech	všecek	k3xTgMnPc2
novorozenců	novorozenec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
jejich	jejich	k3xOp3gFnSc4
podíl	podíl	k1gInSc1
poklesl	poklesnout	k5eAaPmAgInS
pod	pod	k7c4
50	#num#	k4
%	%	kIx~
a	a	k8xC
menšiny	menšina	k1gFnPc1
tak	tak	k9
již	již	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
většinu	většina	k1gFnSc4
novorozenců	novorozenec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Muži	muž	k1gMnPc1
tvoří	tvořit	k5eAaImIp3nP
48,5	48,5	k4
%	%	kIx~
populace	populace	k1gFnPc1
<g/>
,	,	kIx,
ženy	žena	k1gFnPc1
51,5	51,5	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mužů	muž	k1gMnPc2
je	být	k5eAaImIp3nS
přitom	přitom	k6eAd1
více	hodně	k6eAd2
ve	v	k7c6
věkových	věkový	k2eAgFnPc6d1
skupinách	skupina	k1gFnPc6
do	do	k7c2
15	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
naopak	naopak	k6eAd1
nad	nad	k7c4
65	#num#	k4
let	léto	k1gNnPc2
již	již	k6eAd1
převažují	převažovat	k5eAaImIp3nP
ženy	žena	k1gFnPc1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
o	o	k7c4
celých	celý	k2eAgNnPc2d1
12,5	12,5	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Věkové	věkový	k2eAgNnSc1d1
složení	složení	k1gNnSc1
obyvatelstva	obyvatelstvo	k1gNnSc2
je	být	k5eAaImIp3nS
následující	následující	k2eAgFnSc1d1
<g/>
:	:	kIx,
</s>
<s>
VěkPodíl	VěkPodíl	k1gMnSc1
</s>
<s>
0	#num#	k4
<g/>
–	–	k?
<g/>
14	#num#	k4
let	léto	k1gNnPc2
<g/>
20,1	20,1	k4
%	%	kIx~
</s>
<s>
15	#num#	k4
<g/>
–	–	k?
<g/>
64	#num#	k4
let	léto	k1gNnPc2
<g/>
66,9	66,9	k4
%	%	kIx~
</s>
<s>
65	#num#	k4
let	let	k1gInSc4
a	a	k8xC
více	hodně	k6eAd2
<g/>
13	#num#	k4
%	%	kIx~
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rasové	rasový	k2eAgNnSc1d1
složení	složení	k1gNnSc1
<g/>
2013	#num#	k4
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
<g/>
1990	#num#	k4
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
<g/>
1970	#num#	k4
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
<g/>
1940	#num#	k4
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Běloši	běloch	k1gMnPc1
<g/>
77,7	77,7	k4
%	%	kIx~
<g/>
80,3	80,3	k4
%	%	kIx~
<g/>
87,5	87,5	k4
%	%	kIx~
<g/>
89,8	89,8	k4
%	%	kIx~
</s>
<s>
—	—	k?
<g/>
Nehispánští	hispánský	k2eNgMnPc1d1
běloši	běloch	k1gMnPc1
<g/>
62,6	62,6	k4
%	%	kIx~
<g/>
75,6	75,6	k4
%	%	kIx~
<g/>
83,2	83,2	k4
%	%	kIx~
<g/>
88,4	88,4	k4
%	%	kIx~
</s>
<s>
Černoši	černoch	k1gMnPc1
<g/>
13,2	13,2	k4
%	%	kIx~
<g/>
12,1	12,1	k4
%	%	kIx~
<g/>
11,1	11,1	k4
%	%	kIx~
<g/>
9,8	9,8	k4
%	%	kIx~
</s>
<s>
Hispánci	Hispánek	k1gMnPc1
(	(	kIx(
<g/>
jakékoliv	jakýkoliv	k3yIgFnSc2
rasy	rasa	k1gFnSc2
<g/>
)	)	kIx)
<g/>
17,1	17,1	k4
%	%	kIx~
<g/>
9,0	9,0	k4
%	%	kIx~
<g/>
4,7	4,7	k4
%	%	kIx~
<g/>
1,4	1,4	k4
%	%	kIx~
</s>
<s>
Asiaté	Asiat	k1gMnPc1
<g/>
5,3	5,3	k4
%	%	kIx~
<g/>
3,9	3,9	k4
%	%	kIx~
<g/>
0,3	0,3	k4
%	%	kIx~
<g/>
(	(	kIx(
<g/>
X	X	kA
<g/>
)	)	kIx)
</s>
<s>
Imigrace	imigrace	k1gFnSc1
</s>
<s>
Slib	slib	k1gInSc1
věrnosti	věrnost	k1gFnSc2
nových	nový	k2eAgMnPc2d1
amerických	americký	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
v	v	k7c6
Salemu	Salem	k1gInSc6
<g/>
,	,	kIx,
Massachusetts	Massachusetts	k1gNnSc1
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
</s>
<s>
Od	od	k7c2
počátku	počátek	k1gInSc2
kolonizace	kolonizace	k1gFnSc1
v	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
až	až	k9
do	do	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
mezi	mezi	k7c7
imigranty	imigrant	k1gMnPc7
převládali	převládat	k5eAaImAgMnP
Evropané	Evropan	k1gMnPc1
<g/>
,	,	kIx,
především	především	k9
Angličané	Angličan	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
Irové	Ir	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
prchali	prchat	k5eAaImAgMnP
před	před	k7c7
hladomorem	hladomor	k1gInSc7
v	v	k7c6
Irsku	Irsko	k1gNnSc6
<g/>
,	,	kIx,
a	a	k8xC
Němci	Němec	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
začali	začít	k5eAaPmAgMnP
ve	v	k7c6
větších	veliký	k2eAgInPc6d2
počtech	počet	k1gInPc6
stěhovat	stěhovat	k5eAaImF
po	po	k7c6
potlačení	potlačení	k1gNnSc6
revoluce	revoluce	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1848	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
imigrantů	imigrant	k1gMnPc2
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
prošla	projít	k5eAaPmAgFnS
imigračním	imigrační	k2eAgNnSc7d1
střediskem	středisko	k1gNnSc7
na	na	k7c6
Ellis	Ellis	k1gFnSc6
Islandu	Island	k1gInSc2
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
do	do	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
stěhovalo	stěhovat	k5eAaImAgNnS
mnoho	mnoho	k4c1
Čechů	Čech	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
usazovali	usazovat	k5eAaImAgMnP
především	především	k9
v	v	k7c6
Texasu	Texas	k1gInSc6
a	a	k8xC
ve	v	k7c6
státech	stát	k1gInPc6
středozápadu	středozápad	k1gInSc2
(	(	kIx(
<g/>
např.	např.	kA
v	v	k7c6
Nebrasce	Nebraska	k1gFnSc6
tvoří	tvořit	k5eAaImIp3nP
Čechoameričané	Čechoameričan	k1gMnPc1
okolo	okolo	k7c2
5	#num#	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
ve	v	k7c6
velkých	velký	k2eAgNnPc6d1
městech	město	k1gNnPc6
jako	jako	k8xS,k8xC
Chicago	Chicago	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
starostou	starosta	k1gMnSc7
český	český	k2eAgMnSc1d1
rodák	rodák	k1gMnSc1
Antonín	Antonín	k1gMnSc1
Čermák	Čermák	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Imigrační	imigrační	k2eAgInSc1d1
zákon	zákon	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
1921	#num#	k4
(	(	kIx(
<g/>
Emergency	Emergenca	k1gFnSc2
Quota	Quota	k1gMnSc1
Act	Act	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
ještě	ještě	k6eAd1
zpřísněn	zpřísnit	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1924	#num#	k4
<g/>
,	,	kIx,
omezil	omezit	k5eAaPmAgInS
přistěhovalectví	přistěhovalectví	k1gNnSc4
do	do	k7c2
USA	USA	kA
kvótami	kvóta	k1gFnPc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
nejvíce	nejvíce	k6eAd1,k6eAd3
postihlo	postihnout	k5eAaPmAgNnS
emigranty	emigrant	k1gMnPc4
z	z	k7c2
jižní	jižní	k2eAgFnSc2d1
a	a	k8xC
východní	východní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
tvořili	tvořit	k5eAaImAgMnP
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
přes	přes	k7c4
polovinu	polovina	k1gFnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
nově	nově	k6eAd1
příchozích	příchozí	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
tento	tento	k3xDgInSc1
zákon	zákon	k1gInSc1
bránil	bránit	k5eAaImAgInS
příchodu	příchod	k1gInSc3
Židů	Žid	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
prchali	prchat	k5eAaImAgMnP
před	před	k7c7
nacistickým	nacistický	k2eAgNnSc7d1
Německem	Německo	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
Zákon	zákon	k1gInSc1
byl	být	k5eAaImAgInS
změněn	změnit	k5eAaPmNgInS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1952	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
se	se	k3xPyFc4
do	do	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
stěhovalo	stěhovat	k5eAaImAgNnS
ročně	ročně	k6eAd1
200	#num#	k4
<g/>
–	–	k?
<g/>
300	#num#	k4
tisíc	tisíc	k4xCgInPc2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prezident	prezident	k1gMnSc1
Lyndon	Lyndon	k1gMnSc1
B.	B.	kA
Johnson	Johnson	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1965	#num#	k4
podepsal	podepsat	k5eAaPmAgInS
zákon	zákon	k1gInSc1
Immigration	Immigration	k1gInSc1
and	and	k?
Nationality	Nationalita	k1gFnSc2
Act	Act	k1gFnSc2
of	of	k?
1965	#num#	k4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
zrušil	zrušit	k5eAaPmAgMnS
jakákoliv	jakýkoliv	k3yIgNnPc4
rasová	rasový	k2eAgNnPc4d1
omezení	omezení	k1gNnPc4
pro	pro	k7c4
imigraci	imigrace	k1gFnSc4
a	a	k8xC
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
otevřel	otevřít	k5eAaPmAgMnS
imigrantům	imigrant	k1gMnPc3
ze	z	k7c2
zemí	zem	k1gFnPc2
třetího	třetí	k4xOgInSc2
světa	svět	k1gInSc2
<g/>
,	,	kIx,
především	především	k9
z	z	k7c2
Latinské	latinský	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
Karibiku	Karibik	k1gInSc2
a	a	k8xC
Asie	Asie	k1gFnSc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
postupně	postupně	k6eAd1
mezi	mezi	k7c7
imigranty	imigrant	k1gMnPc7
převládli	převládnout	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
počátku	počátek	k1gInSc6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
přijímaly	přijímat	k5eAaImAgInP
okolo	okolo	k7c2
1	#num#	k4
milionu	milion	k4xCgInSc2
imigrantů	imigrant	k1gMnPc2
ročně	ročně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
se	se	k3xPyFc4
do	do	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
přistěhovalo	přistěhovat	k5eAaPmAgNnS
legálně	legálně	k6eAd1
více	hodně	k6eAd2
než	než	k8xS
milion	milion	k4xCgInSc1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
jenom	jenom	k9
85	#num#	k4
tisíc	tisíc	k4xCgInSc4
z	z	k7c2
nich	on	k3xPp3gInPc2
přišlo	přijít	k5eAaPmAgNnS
z	z	k7c2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Takzvaná	takzvaný	k2eAgFnSc1d1
„	„	k?
<g/>
sanctuary	sanctuar	k1gInPc1
cities	cities	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
tedy	tedy	k9
ochranitelská	ochranitelský	k2eAgNnPc4d1
či	či	k8xC
azylová	azylový	k2eAgNnPc4d1
města	město	k1gNnPc4
<g/>
,	,	kIx,
mezi	mezi	k7c4
než	než	k8xS
patří	patřit	k5eAaImIp3nS
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
Los	los	k1gInSc1
Angeles	Angeles	k1gInSc1
<g/>
,	,	kIx,
Denver	Denver	k1gInSc1
<g/>
,	,	kIx,
Chicago	Chicago	k1gNnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
Seattle	Seattle	k1gFnPc4
a	a	k8xC
stovky	stovka	k1gFnPc4
dalších	další	k2eAgNnPc2d1
amerických	americký	k2eAgNnPc2d1
měst	město	k1gNnPc2
a	a	k8xC
okrsků	okrsek	k1gInPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
jsou	být	k5eAaImIp3nP
většinou	většinou	k6eAd1
ovládaná	ovládaný	k2eAgFnSc1d1
demokraty	demokrat	k1gMnPc7
<g/>
,	,	kIx,
nepomáhají	pomáhat	k5eNaImIp3nP
federálním	federální	k2eAgInPc3d1
úřadům	úřad	k1gInPc3
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
území	území	k1gNnSc6
s	s	k7c7
prosazováním	prosazování	k1gNnSc7
federálních	federální	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
týkajících	týkající	k2eAgFnPc2d1
se	se	k3xPyFc4
imigrace	imigrace	k1gFnSc2
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
to	ten	k3xDgNnSc1
není	být	k5eNaImIp3nS
jejich	jejich	k3xOp3gFnSc4
povinnost	povinnost	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
chrání	chránit	k5eAaImIp3nS
nelegální	legální	k2eNgMnPc4d1
imigranty	imigrant	k1gMnPc4
před	před	k7c7
federální	federální	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
109	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jazyky	jazyk	k1gInPc1
</s>
<s>
Angličtina	angličtina	k1gFnSc1
je	být	k5eAaImIp3nS
výlučným	výlučný	k2eAgInSc7d1
mateřským	mateřský	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
pro	pro	k7c4
asi	asi	k9
82	#num#	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
španělština	španělština	k1gFnSc1
pro	pro	k7c4
10	#num#	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
USA	USA	kA
nemají	mít	k5eNaImIp3nP
na	na	k7c6
federální	federální	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
zákonem	zákon	k1gInSc7
kodifikovaný	kodifikovaný	k2eAgInSc4d1
úřední	úřední	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
angličtina	angličtina	k1gFnSc1
jím	on	k3xPp3gNnSc7
fakticky	fakticky	k6eAd1
je	být	k5eAaImIp3nS
a	a	k8xC
některé	některý	k3yIgInPc1
zákony	zákon	k1gInPc1
a	a	k8xC
předpisy	předpis	k1gInPc1
vyžadují	vyžadovat	k5eAaImIp3nP
v	v	k7c6
určitých	určitý	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
její	její	k3xOp3gNnSc1
znalost	znalost	k1gFnSc1
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
např.	např.	kA
požadavky	požadavek	k1gInPc4
pro	pro	k7c4
naturalizaci	naturalizace	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc1
jednotlivé	jednotlivý	k2eAgInPc1d1
státy	stát	k1gInPc1
Unie	unie	k1gFnSc2
úřední	úřední	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
kodifikovány	kodifikován	k2eAgInPc1d1
mají	mít	k5eAaImIp3nP
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
buďto	buďto	k8xC
angličtinu	angličtina	k1gFnSc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
vícero	vícero	k1gNnSc1
jazyků	jazyk	k1gInPc2
včetně	včetně	k7c2
angličtiny	angličtina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vícejazyčné	vícejazyčný	k2eAgInPc1d1
státy	stát	k1gInPc1
jsou	být	k5eAaImIp3nP
Havaj	Havaj	k1gFnSc4
(	(	kIx(
<g/>
angličtina	angličtina	k1gFnSc1
a	a	k8xC
havajština	havajština	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Nové	Nové	k2eAgNnSc1d1
Mexiko	Mexiko	k1gNnSc1
(	(	kIx(
<g/>
angličtina	angličtina	k1gFnSc1
a	a	k8xC
španělština	španělština	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
Louisiana	Louisiana	k1gFnSc1
(	(	kIx(
<g/>
angličtina	angličtina	k1gFnSc1
a	a	k8xC
francouzština	francouzština	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
různých	různý	k2eAgNnPc6d1
dalších	další	k2eAgNnPc6d1
závislých	závislý	k2eAgNnPc6d1
územích	území	k1gNnPc6
je	být	k5eAaImIp3nS
situace	situace	k1gFnSc1
ještě	ještě	k6eAd1
pestřejší	pestrý	k2eAgFnSc1d2
<g/>
,	,	kIx,
často	často	k6eAd1
je	být	k5eAaImIp3nS
zvláštními	zvláštní	k2eAgInPc7d1
předpisy	předpis	k1gInPc7
chráněn	chránit	k5eAaImNgInS
jazyk	jazyk	k1gInSc1
původních	původní	k2eAgMnPc2d1
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Náboženství	náboženství	k1gNnSc1
</s>
<s>
Biblický	biblický	k2eAgInSc1d1
pás	pás	k1gInSc1
–	–	k?
přibližné	přibližný	k2eAgNnSc1d1
vymezení	vymezení	k1gNnSc1
převážně	převážně	k6eAd1
protestantského	protestantský	k2eAgInSc2d1
a	a	k8xC
konzervativního	konzervativní	k2eAgInSc2d1
Jihu	jih	k1gInSc2
</s>
<s>
Náboženství	náboženství	k1gNnSc1
v	v	k7c6
USA	USA	kA
</s>
<s>
<	<	kIx(
<g/>
30	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
<	<	kIx(
<g/>
40	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
<	<	kIx(
<g/>
50	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
>	>	kIx)
<g/>
50	#num#	k4
<g/>
%	%	kIx~
</s>
<s>
katolíci	katolík	k1gMnPc1
</s>
<s>
baptisté	baptista	k1gMnPc1
</s>
<s>
metodisté	metodista	k1gMnPc1
</s>
<s>
luteráni	luterán	k1gMnPc1
</s>
<s>
mormoni	mormon	k1gMnPc1
</s>
<s>
ateisté	ateista	k1gMnPc1
</s>
<s>
Protestatnský	Protestatnský	k2eAgInSc1d1
kostel	kostel	k1gInSc1
na	na	k7c6
americkém	americký	k2eAgInSc6d1
Jihu	jih	k1gInSc6
</s>
<s>
Ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgMnPc2d1
hrají	hrát	k5eAaImIp3nP
náboženství	náboženství	k1gNnSc4
mnohem	mnohem	k6eAd1
významnější	významný	k2eAgFnSc4d2
roli	role	k1gFnSc4
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
tomu	ten	k3xDgNnSc3
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
k	k	k7c3
rozsáhlé	rozsáhlý	k2eAgFnSc3d1
sekularizaci	sekularizace	k1gFnSc3
společnosti	společnost	k1gFnSc2
v	v	k7c6
řadě	řada	k1gFnSc6
zemí	zem	k1gFnPc2
(	(	kIx(
<g/>
kupř	kupř	kA
<g/>
.	.	kIx.
Švédsko	Švédsko	k1gNnSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
<g/>
,	,	kIx,
Estonsko	Estonsko	k1gNnSc1
a	a	k8xC
další	další	k2eAgNnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
obtížné	obtížný	k2eAgNnSc1d1
<g/>
,	,	kIx,
až	až	k8xS
nemožné	možný	k2eNgNnSc1d1
<g/>
,	,	kIx,
dodat	dodat	k5eAaPmF
pro	pro	k7c4
USA	USA	kA
přesná	přesný	k2eAgNnPc4d1
čísla	číslo	k1gNnPc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
neexistují	existovat	k5eNaImIp3nP
žádné	žádný	k3yNgFnPc1
ucelené	ucelený	k2eAgFnPc1d1
statistiky	statistika	k1gFnPc1
typu	typ	k1gInSc2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
(	(	kIx(
<g/>
americké	americký	k2eAgNnSc1d1
pojetí	pojetí	k1gNnSc1
odluky	odluka	k1gFnSc2
církve	církev	k1gFnSc2
od	od	k7c2
státu	stát	k1gInSc2
má	mít	k5eAaImIp3nS
za	za	k7c4
následek	následek	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
státní	státní	k2eAgInPc1d1
úřady	úřad	k1gInPc1
na	na	k7c4
věci	věc	k1gFnPc4
týkající	týkající	k2eAgFnPc4d1
se	se	k3xPyFc4
víry	víra	k1gFnSc2
neptají	ptat	k5eNaImIp3nP
<g/>
)	)	kIx)
<g/>
,	,	kIx,
takže	takže	k8xS
počty	počet	k1gInPc1
věřících	věřící	k1gMnPc2
se	se	k3xPyFc4
odvozují	odvozovat	k5eAaImIp3nP
buďto	buďto	k8xC
ze	z	k7c2
soukromých	soukromý	k2eAgInPc2d1
průzkumů	průzkum	k1gInPc2
či	či	k8xC
údajů	údaj	k1gInPc2
samotných	samotný	k2eAgFnPc2d1
církví	církev	k1gFnPc2
a	a	k8xC
představují	představovat	k5eAaImIp3nP
spíše	spíše	k9
hrubé	hrubý	k2eAgInPc1d1
odhady	odhad	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obyvatel	obyvatel	k1gMnSc1
bez	bez	k7c2
vyznání	vyznání	k1gNnSc2
<g/>
,	,	kIx,
ateistů	ateista	k1gMnPc2
a	a	k8xC
agnostiků	agnostik	k1gMnPc2
je	být	k5eAaImIp3nS
jen	jen	k9
asi	asi	k9
14	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
Nejrozšířenějším	rozšířený	k2eAgNnSc7d3
náboženstvím	náboženství	k1gNnSc7
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
je	být	k5eAaImIp3nS
křesťanství	křesťanství	k1gNnSc1
<g/>
,	,	kIx,
k	k	k7c3
němuž	jenž	k3xRgInSc3
se	se	k3xPyFc4
hlásí	hlásit	k5eAaImIp3nS
asi	asi	k9
76	#num#	k4
%	%	kIx~
až	až	k9
81	#num#	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zhruba	zhruba	k6eAd1
2	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
z	z	k7c2
nich	on	k3xPp3gMnPc2
jsou	být	k5eAaImIp3nP
protestanti	protestant	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
jsou	být	k5eAaImIp3nP
ovšem	ovšem	k9
roztříštěni	roztříštěn	k2eAgMnPc1d1
do	do	k7c2
mnoha	mnoho	k4c2
dílčích	dílčí	k2eAgNnPc2d1
seskupení	seskupení	k1gNnPc2
a	a	k8xC
denominací	denominace	k1gFnPc2
<g/>
,	,	kIx,
takže	takže	k8xS
nejsilnější	silný	k2eAgFnSc7d3
církví	církev	k1gFnSc7
je	být	k5eAaImIp3nS
církev	církev	k1gFnSc1
římskokatolická	římskokatolický	k2eAgFnSc1d1
<g/>
,	,	kIx,
k	k	k7c3
níž	jenž	k3xRgFnSc3
náleží	náležet	k5eAaImIp3nS
zbylá	zbylý	k2eAgFnSc1d1
třetina	třetina	k1gFnSc1
křesťanů	křesťan	k1gMnPc2
(	(	kIx(
<g/>
asi	asi	k9
25	#num#	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
tam	tam	k6eAd1
i	i	k9
početné	početný	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
východních	východní	k2eAgMnPc2d1
katolíků	katolík	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
celkem	celkem	k6eAd1
10	#num#	k4
církví	církev	k1gFnPc2
<g/>
:	:	kIx,
arménští	arménský	k2eAgMnPc1d1
katolíci	katolík	k1gMnPc1
<g/>
,	,	kIx,
syrští	syrský	k2eAgMnPc1d1
katolíci	katolík	k1gMnPc1
<g/>
,	,	kIx,
syromalankarští	syromalankarský	k2eAgMnPc1d1
katolíci	katolík	k1gMnPc1
z	z	k7c2
Indie	Indie	k1gFnSc2
<g/>
,	,	kIx,
syromalabarští	syromalabarský	k2eAgMnPc1d1
katolíci	katolík	k1gMnPc1
z	z	k7c2
Indie	Indie	k1gFnSc2
<g/>
,	,	kIx,
chaldejští	chaldejský	k2eAgMnPc1d1
katolíci	katolík	k1gMnPc1
z	z	k7c2
Iráku	Irák	k1gInSc2
<g/>
,	,	kIx,
maronité	maronitý	k2eAgFnPc1d1
<g/>
,	,	kIx,
řeckokatolíci-melchité	řeckokatolíci-melchita	k1gMnPc1
<g/>
,	,	kIx,
ukrajinští	ukrajinský	k2eAgMnPc1d1
řeckokatolíci	řeckokatolík	k1gMnPc1
<g/>
,	,	kIx,
rusínští	rusínský	k2eAgMnPc1d1
řeckokatolíci	řeckokatolík	k1gMnPc1
a	a	k8xC
rumunští	rumunský	k2eAgMnPc1d1
řeckokatolíci	řeckokatolík	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
mají	mít	k5eAaImIp3nP
tyto	tento	k3xDgFnPc4
církve	církev	k1gFnPc4
468	#num#	k4
330	#num#	k4
věřících	věřící	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
k	k	k7c3
nim	on	k3xPp3gMnPc3
také	také	k9
část	část	k1gFnSc4
z	z	k7c2
36	#num#	k4
000	#num#	k4
arménských	arménský	k2eAgMnPc2d1
katolíků	katolík	k1gMnPc2
a	a	k8xC
část	část	k1gFnSc4
z	z	k7c2
6	#num#	k4
200	#num#	k4
rumunských	rumunský	k2eAgMnPc2d1
řeckokatolíků	řeckokatolík	k1gMnPc2
z	z	k7c2
diecézí	diecéze	k1gFnPc2
pro	pro	k7c4
USA	USA	kA
a	a	k8xC
Kanadu	Kanada	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tam	tam	k6eAd1
též	též	k9
personální	personální	k2eAgInSc1d1
Ordinariát	ordinariát	k1gInSc1
katedry	katedra	k1gFnSc2
sv.	sv.	kA
Petra	Petra	k1gFnSc1
pro	pro	k7c4
římskokatolíky	římskokatolík	k1gMnPc4
anglikánského	anglikánský	k2eAgInSc2d1
ritu	rit	k1gInSc2
(	(	kIx(
<g/>
bývalé	bývalý	k2eAgFnSc2d1
episkopály	episkopála	k1gFnSc2
z	z	k7c2
ECUSA-Episkopální	ECUSA-Episkopální	k2eAgFnSc2d1
církve	církev	k1gFnSc2
v	v	k7c6
USA	USA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
vznikl	vzniknout	k5eAaPmAgInS
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2012	#num#	k4
<g/>
,	,	kIx,
roku	rok	k1gInSc2
2012	#num#	k4
měl	mít	k5eAaImAgInS
2	#num#	k4
550	#num#	k4
věřících	věřící	k1gMnPc2
a	a	k8xC
23	#num#	k4
kněží	kněz	k1gMnPc2
ve	v	k7c6
12	#num#	k4
farnostech	farnost	k1gFnPc6
<g/>
;	;	kIx,
počet	počet	k1gInSc4
věřících	věřící	k1gMnPc2
narůstá	narůstat	k5eAaImIp3nS
-	-	kIx~
2013	#num#	k4
již	již	k9
4	#num#	k4
550	#num#	k4
<g/>
,	,	kIx,
23	#num#	k4
kněží	kněz	k1gMnPc2
ve	v	k7c6
12	#num#	k4
farnostech	farnost	k1gFnPc6
<g/>
,	,	kIx,
2014	#num#	k4
-	-	kIx~
už	už	k6eAd1
6	#num#	k4
000	#num#	k4
věřících	věřící	k1gMnPc2
a	a	k8xC
40	#num#	k4
kněží	kněz	k1gMnPc2
ve	v	k7c6
25	#num#	k4
farnostech	farnost	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ordinářem	ordinář	k1gMnSc7
je	být	k5eAaImIp3nS
Jeffrey	Jeffre	k1gMnPc4
Neil	Neil	k1gMnSc1
Steenson	Steenson	k1gMnSc1
<g/>
,	,	kIx,
do	do	k7c2
září	září	k1gNnSc2
2007	#num#	k4
jeden	jeden	k4xCgMnSc1
z	z	k7c2
biskupů	biskup	k1gMnPc2
ECUSA	ECUSA	kA
<g/>
,	,	kIx,
oficiálně	oficiálně	k6eAd1
konvertoval	konvertovat	k5eAaBmAgMnS
(	(	kIx(
<g/>
i	i	k8xC
s	s	k7c7
rodinou	rodina	k1gFnSc7
<g/>
)	)	kIx)
1	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2007	#num#	k4
<g/>
,	,	kIx,
ordinářem	ordinář	k1gMnSc7
je	být	k5eAaImIp3nS
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2012	#num#	k4
<g/>
,	,	kIx,
uveden	uveden	k2eAgInSc1d1
do	do	k7c2
úřadu	úřad	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k9
nejpočetnější	početní	k2eAgFnSc1d3
a	a	k8xC
nejvýznamnější	významný	k2eAgFnSc1d3
skupina	skupina	k1gFnSc1
obyvatel	obyvatel	k1gMnPc2
(	(	kIx(
<g/>
z	z	k7c2
náboženského	náboženský	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
bráni	brán	k2eAgMnPc1d1
evangelikálové	evangelikál	k1gMnPc1
<g/>
,	,	kIx,
těsně	těsně	k6eAd1
následovaní	následovaný	k2eAgMnPc1d1
katolíky	katolík	k1gMnPc7
(	(	kIx(
<g/>
obojí	oboj	k1gFnSc7
zhruba	zhruba	k6eAd1
25	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
pořadí	pořadí	k1gNnSc2
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
v	v	k7c6
různých	různý	k2eAgFnPc6d1
statistikách	statistika	k1gFnPc6
měnit	měnit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysoká	vysoký	k2eAgFnSc1d1
koncentrace	koncentrace	k1gFnSc1
konzervativních	konzervativní	k2eAgMnPc2d1
protestantů	protestant	k1gMnPc2
v	v	k7c6
jihovýchodní	jihovýchodní	k2eAgFnSc6d1
části	část	k1gFnSc6
USA	USA	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
představují	představovat	k5eAaImIp3nP
dominantní	dominantní	k2eAgFnSc4d1
kulturní	kulturní	k2eAgFnSc4d1
a	a	k8xC
politickou	politický	k2eAgFnSc4d1
sílu	síla	k1gFnSc4
<g/>
,	,	kIx,
dala	dát	k5eAaPmAgFnS
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
přezdívku	přezdívka	k1gFnSc4
„	„	k?
<g/>
biblický	biblický	k2eAgInSc1d1
pás	pás	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Bible	bible	k1gFnSc1
Belt	Belt	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1830	#num#	k4
vznikla	vzniknout	k5eAaPmAgFnS
Církev	církev	k1gFnSc1
Ježíše	Ježíš	k1gMnSc2
Krista	Kristus	k1gMnSc2
Svatých	svatý	k1gMnPc2
posledních	poslední	k2eAgInPc2d1
dnů	den	k1gInPc2
(	(	kIx(
<g/>
tj.	tj.	kA
mormoni	mormon	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gMnPc1
stoupenci	stoupenec	k1gMnPc1
v	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
osídli	osídlit	k5eAaPmRp2nS
území	území	k1gNnSc6
budoucího	budoucí	k2eAgInSc2d1
státu	stát	k1gInSc2
Utah	Utah	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
i	i	k9
dnes	dnes	k6eAd1
většinovou	většinový	k2eAgFnSc7d1
církví	církev	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Nekřesťanská	křesťanský	k2eNgNnPc1d1
náboženství	náboženství	k1gNnPc1
představují	představovat	k5eAaImIp3nP
asi	asi	k9
3,5	3,5	k4
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
USA	USA	kA
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
nejvýznamnější	významný	k2eAgFnPc1d3
z	z	k7c2
nich	on	k3xPp3gFnPc2
jsou	být	k5eAaImIp3nP
judaismus	judaismus	k1gInSc4
(	(	kIx(
<g/>
židé	žid	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
islám	islám	k1gInSc1
<g/>
,	,	kIx,
buddhismus	buddhismus	k1gInSc1
a	a	k8xC
hinduismus	hinduismus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zejména	zejména	k9
počet	počet	k1gInSc1
vyznavačů	vyznavač	k1gMnPc2
islámu	islám	k1gInSc2
a	a	k8xC
buddhismu	buddhismus	k1gInSc2
rychle	rychle	k6eAd1
roste	růst	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
křesťané	křesťan	k1gMnPc1
76,7	76,7	k4
%	%	kIx~
</s>
<s>
protestanti	protestant	k1gMnPc1
52	#num#	k4
%	%	kIx~
</s>
<s>
baptisté	baptista	k1gMnPc1
16,3	16,3	k4
%	%	kIx~
</s>
<s>
metodisté	metodista	k1gMnPc1
6,8	6,8	k4
%	%	kIx~
</s>
<s>
luteráni	luterán	k1gMnPc1
4,6	4,6	k4
%	%	kIx~
</s>
<s>
římští	římský	k2eAgMnPc1d1
katolíci	katolík	k1gMnPc1
24,5	24,5	k4
%	%	kIx~
<g/>
,	,	kIx,
plus	plus	k6eAd1
východní	východní	k2eAgMnPc1d1
katolíci	katolík	k1gMnPc1
</s>
<s>
agnostici	agnostik	k1gMnPc1
<g/>
,	,	kIx,
ateisté	ateista	k1gMnPc1
<g/>
,	,	kIx,
bez	bez	k7c2
vyznání	vyznání	k1gNnSc2
celkem	celkem	k6eAd1
14,2	14,2	k4
%	%	kIx~
</s>
<s>
židé	žid	k1gMnPc1
1,6	1,6	k4
%	%	kIx~
</s>
<s>
muslimové	muslim	k1gMnPc1
0,6	0,6	k4
%	%	kIx~
</s>
<s>
buddhisté	buddhista	k1gMnPc1
0,5	0,5	k4
%	%	kIx~
</s>
<s>
hinduisté	hinduista	k1gMnPc1
0,4	0,4	k4
%	%	kIx~
</s>
<s>
Systém	systém	k1gInSc1
vzdělávaní	vzdělávaný	k2eAgMnPc1d1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Školství	školství	k1gNnSc2
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgInPc6d1
<g/>
.	.	kIx.
</s>
<s>
University	universita	k1gFnPc1
of	of	k?
Virginia	Virginium	k1gNnSc2
<g/>
,	,	kIx,
založená	založený	k2eAgFnSc1d1
Thomasem	Thomas	k1gMnSc7
Jeffersonem	Jefferson	k1gMnSc7
v	v	k7c6
roce	rok	k1gInSc6
1819	#num#	k4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
mnoha	mnoho	k4c2
veřejných	veřejný	k2eAgFnPc2d1
univerzit	univerzita	k1gFnPc2
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Vzdělávací	vzdělávací	k2eAgInSc1d1
systém	systém	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
USA	USA	kA
podobný	podobný	k2eAgInSc1d1
v	v	k7c6
každém	každý	k3xTgInSc6
z	z	k7c2
50	#num#	k4
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Školní	školní	k2eAgInSc1d1
systém	systém	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
kompetenci	kompetence	k1gFnSc6
jednotlivých	jednotlivý	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrazné	výrazný	k2eAgInPc1d1
rozdíly	rozdíl	k1gInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
hlavně	hlavně	k9
ve	v	k7c6
financování	financování	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veřejné	veřejný	k2eAgNnSc1d1
školství	školství	k1gNnSc1
je	být	k5eAaImIp3nS
dále	daleko	k6eAd2
decentralizované	decentralizovaný	k2eAgNnSc1d1
v	v	k7c6
rámci	rámec	k1gInSc6
každého	každý	k3xTgInSc2
státu	stát	k1gInSc2
<g/>
,	,	kIx,
např.	např.	kA
od	od	k7c2
státního	státní	k2eAgInSc2d1
výboru	výbor	k1gInSc2
vzdělávání	vzdělávání	k1gNnSc2
až	až	k9
po	po	k7c4
místní	místní	k2eAgInPc4d1
školské	školský	k2eAgInPc4d1
obvody	obvod	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obecné	obecný	k2eAgInPc1d1
požadavky	požadavek	k1gInPc1
jednotlivých	jednotlivý	k2eAgInPc2d1
států	stát	k1gInPc2
se	se	k3xPyFc4
realizují	realizovat	k5eAaBmIp3nP
prostřednictvím	prostřednictvím	k7c2
těchto	tento	k3xDgInPc2
místních	místní	k2eAgInPc2d1
školních	školní	k2eAgInPc2d1
obvodů	obvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ty	k3xPp2nSc1
jsou	být	k5eAaImIp3nP
spravovány	spravován	k2eAgInPc1d1
školní	školní	k2eAgFnSc7d1
radou	rada	k1gFnSc7
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
si	se	k3xPyFc3
volí	volit	k5eAaImIp3nP
občané	občan	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
formě	forma	k1gFnSc6
výuky	výuka	k1gFnSc2
<g/>
,	,	kIx,
osnov	osnova	k1gFnPc2
<g/>
,	,	kIx,
použití	použití	k1gNnSc1
učebnic	učebnice	k1gFnPc2
rozhoduje	rozhodovat	k5eAaImIp3nS
školní	školní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
spolu	spolu	k6eAd1
s	s	k7c7
kompetentním	kompetentní	k2eAgMnSc7d1
pedagogickým	pedagogický	k2eAgMnSc7d1
pracovníkem	pracovník	k1gMnSc7
školy	škola	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Financování	financování	k1gNnSc1
škol	škola	k1gFnPc2
bylo	být	k5eAaImAgNnS
založeno	založit	k5eAaPmNgNnS
na	na	k7c6
peněžních	peněžní	k2eAgInPc6d1
prostředcích	prostředek	k1gInPc6
plynoucích	plynoucí	k2eAgFnPc2d1
z	z	k7c2
místních	místní	k2eAgFnPc2d1
daní	daň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
se	se	k3xPyFc4
školy	škola	k1gFnPc1
financují	financovat	k5eAaBmIp3nP
hlavně	hlavně	k9
ze	z	k7c2
státního	státní	k2eAgInSc2d1
rozpočtu	rozpočet	k1gInSc2
a	a	k8xC
o	o	k7c4
použití	použití	k1gNnSc4
prostředků	prostředek	k1gInPc2
rozhoduje	rozhodovat	k5eAaImIp3nS
školní	školní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
110	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Veřejné	veřejný	k2eAgNnSc1d1
školství	školství	k1gNnSc1
je	být	k5eAaImIp3nS
všeobecně	všeobecně	k6eAd1
dostupné	dostupný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věk	věk	k1gInSc1
vzdělávací	vzdělávací	k2eAgFnSc2d1
povinnosti	povinnost	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
státech	stát	k1gInPc6
různý	různý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začíná	začínat	k5eAaImIp3nS
v	v	k7c6
pěti	pět	k4xCc6
až	až	k8xS
osmi	osm	k4xCc6
letech	léto	k1gNnPc6
a	a	k8xC
končí	končit	k5eAaImIp3nS
ve	v	k7c6
čtrnácti	čtrnáct	k4xCc6
až	až	k8xS
osmnácti	osmnáct	k4xCc6
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdělávací	vzdělávací	k2eAgFnSc1d1
povinnost	povinnost	k1gFnSc1
se	se	k3xPyFc4
zpravidla	zpravidla	k6eAd1
plní	plnit	k5eAaImIp3nS
školní	školní	k2eAgFnSc7d1
docházkou	docházka	k1gFnSc7
do	do	k7c2
veřejných	veřejný	k2eAgFnPc2d1
škol	škola	k1gFnPc2
<g/>
,	,	kIx,
do	do	k7c2
státem	stát	k1gInSc7
akreditovaných	akreditovaný	k2eAgFnPc2d1
soukromých	soukromý	k2eAgFnPc2d1
škol	škola	k1gFnPc2
nebo	nebo	k8xC
schválenými	schválený	k2eAgInPc7d1
programy	program	k1gInPc7
domácího	domácí	k2eAgNnSc2d1
vzdělávání	vzdělávání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
většině	většina	k1gFnSc6
veřejných	veřejný	k2eAgFnPc2d1
a	a	k8xC
soukromých	soukromý	k2eAgFnPc2d1
škol	škola	k1gFnPc2
je	být	k5eAaImIp3nS
výuka	výuka	k1gFnSc1
rozdělena	rozdělit	k5eAaPmNgFnS
do	do	k7c2
tří	tři	k4xCgInPc2
stupňů	stupeň	k1gInPc2
(	(	kIx(
<g/>
úrovní	úroveň	k1gFnPc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
"	"	kIx"
<g/>
základní	základní	k2eAgFnSc1d1
<g/>
"	"	kIx"
škola	škola	k1gFnSc1
(	(	kIx(
<g/>
elementary	elementar	k1gInPc1
school	schoola	k1gFnPc2
<g/>
;	;	kIx,
v	v	k7c6
Česku	Česko	k1gNnSc6
první	první	k4xOgInSc4
stupeň	stupeň	k1gInSc4
základní	základní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
,	,	kIx,
spíše	spíše	k9
"	"	kIx"
<g/>
obecná	obecná	k1gFnSc1
<g/>
"	"	kIx"
škola	škola	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
"	"	kIx"
<g/>
střední	střední	k2eAgFnSc2d1
<g/>
"	"	kIx"
odpovídá	odpovídat	k5eAaImIp3nS
druhému	druhý	k4xOgInSc3
stupni	stupeň	k1gInSc3
české	český	k2eAgFnSc2d1
základní	základní	k2eAgFnSc2d1
školy	škola	k1gFnSc2
nebo	nebo	k8xC
nižším	nízký	k2eAgInPc3d2
ročníkům	ročník	k1gInPc3
víceletých	víceletý	k2eAgNnPc2d1
gymnázií	gymnázium	k1gNnPc2
(	(	kIx(
<g/>
middle	middle	k6eAd1
school	school	k1gInSc1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
junior	junior	k1gMnSc1
high	higha	k1gFnPc2
school	schoola	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
"	"	kIx"
<g/>
vysoká	vysoký	k2eAgFnSc1d1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
high	high	k1gInSc1
school	schoola	k1gFnPc2
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
secondary	secondara	k1gFnPc1
education	education	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
odpovídající	odpovídající	k2eAgMnSc1d1
českým	český	k2eAgFnPc3d1
středním	střední	k2eAgFnPc3d1
školám	škola	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
převážné	převážný	k2eAgFnSc6d1
většině	většina	k1gFnSc6
škol	škola	k1gFnPc2
těchto	tento	k3xDgInPc2
stupňů	stupeň	k1gInPc2
jsou	být	k5eAaImIp3nP
děti	dítě	k1gFnPc1
rozděleny	rozdělit	k5eAaPmNgFnP
do	do	k7c2
věkových	věkový	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
na	na	k7c4
ročníky	ročník	k1gInPc4
<g/>
,	,	kIx,
resp.	resp.	kA
třídy	třída	k1gFnSc2
(	(	kIx(
<g/>
grade	grad	k1gInSc5
<g/>
)	)	kIx)
od	od	k7c2
školky	školka	k1gFnSc2
(	(	kIx(
<g/>
Kindergarden	Kindergardna	k1gFnPc2
<g/>
)	)	kIx)
pro	pro	k7c4
nejmladší	mladý	k2eAgMnPc4d3
žáky	žák	k1gMnPc4
přes	přes	k7c4
první	první	k4xOgFnSc4
třídu	třída	k1gFnSc4
až	až	k9
po	po	k7c4
dvanáctý	dvanáctý	k4xOgInSc4
ročník	ročník	k1gInSc4
(	(	kIx(
<g/>
třídu	třída	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
poslední	poslední	k2eAgInSc4d1
rok	rok	k1gInSc4
"	"	kIx"
<g/>
vysoké	vysoký	k2eAgFnPc4d1
<g/>
"	"	kIx"
školy	škola	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesné	přesný	k2eAgNnSc4d1
věkové	věkový	k2eAgNnSc4d1
přiřazení	přiřazení	k1gNnSc4
žáků	žák	k1gMnPc2
a	a	k8xC
studentů	student	k1gMnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
jen	jen	k9
student	student	k1gMnSc1
<g/>
)	)	kIx)
v	v	k7c6
ročnících	ročník	k1gInPc6
se	se	k3xPyFc4
trochu	trochu	k6eAd1
liší	lišit	k5eAaImIp3nP
podle	podle	k7c2
místa	místo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2
<g/>
,	,	kIx,
terciární	terciární	k2eAgNnSc1d1
vzdělávání	vzdělávání	k1gNnSc1
<g/>
,	,	kIx,
známější	známý	k2eAgFnSc1d2
jako	jako	k9
college	college	k1gFnPc7
univerzitní	univerzitní	k2eAgFnSc2d1
úrovně	úroveň	k1gFnSc2
je	být	k5eAaImIp3nS
obecně	obecně	k6eAd1
vedeno	vést	k5eAaImNgNnS
nezávisle	závisle	k6eNd1
na	na	k7c6
základním	základní	k2eAgMnSc6d1
a	a	k8xC
středním	střední	k2eAgNnSc6d1
školství	školství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1954	#num#	k4
Nejvyšší	vysoký	k2eAgInSc1d3
soud	soud	k1gInSc1
rozhodl	rozhodnout	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
rasová	rasový	k2eAgFnSc1d1
segregace	segregace	k1gFnSc1
na	na	k7c6
amerických	americký	k2eAgFnPc6d1
veřejných	veřejný	k2eAgFnPc6d1
školách	škola	k1gFnPc6
je	být	k5eAaImIp3nS
protiústavní	protiústavní	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
boji	boj	k1gInSc6
za	za	k7c4
desegregaci	desegregace	k1gFnSc4
začaly	začít	k5eAaPmAgInP
americké	americký	k2eAgInPc1d1
soudy	soud	k1gInPc1
v	v	k7c4
60	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
nařizovat	nařizovat	k5eAaImF
povinné	povinný	k2eAgNnSc4d1
rozvážení	rozvážení	k1gNnSc4
dětí	dítě	k1gFnPc2
odlišných	odlišný	k2eAgFnPc2d1
ras	rasa	k1gFnPc2
do	do	k7c2
škol	škola	k1gFnPc2
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
čtvrtích	čtvrt	k1gFnPc6
(	(	kIx(
<g/>
tzv.	tzv.	kA
desegregation	desegregation	k1gInSc1
busing	busing	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
byla	být	k5eAaImAgFnS
zajištěna	zajištěn	k2eAgFnSc1d1
rasová	rasový	k2eAgFnSc1d1
rovnováha	rovnováha	k1gFnSc1
na	na	k7c6
amerických	americký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
ovšem	ovšem	k9
setkalo	setkat	k5eAaPmAgNnS
s	s	k7c7
odporem	odpor	k1gInSc7
mnoha	mnoho	k4c2
bílých	bílý	k2eAgMnPc2d1
rodičů	rodič	k1gMnPc2
a	a	k8xC
nastal	nastat	k5eAaPmAgInS
tzv.	tzv.	kA
bílý	bílý	k2eAgInSc4d1
útěk	útěk	k1gInSc4
z	z	k7c2
velkých	velký	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
112	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
3	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2018	#num#	k4
vydala	vydat	k5eAaPmAgFnS
Trumpova	Trumpův	k2eAgFnSc1d1
administrativa	administrativa	k1gFnSc1
nařízení	nařízení	k1gNnSc2
<g/>
,	,	kIx,
kterým	který	k3yIgInSc7,k3yRgInSc7,k3yQgInSc7
zrušila	zrušit	k5eAaPmAgFnS
dosavadní	dosavadní	k2eAgFnSc4d1
praxi	praxe	k1gFnSc4
pozitivní	pozitivní	k2eAgFnSc2d1
diskriminace	diskriminace	k1gFnSc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
affirmative	affirmativ	k1gInSc5
action	action	k1gInSc4
<g/>
)	)	kIx)
na	na	k7c6
amerických	americký	k2eAgFnPc6d1
středních	střední	k2eAgFnPc6d1
a	a	k8xC
vysokých	vysoký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
mohly	moct	k5eAaImAgFnP
při	při	k7c6
přijímání	přijímání	k1gNnSc6
studentů	student	k1gMnPc2
rozhodovat	rozhodovat	k5eAaImF
podle	podle	k7c2
rasového	rasový	k2eAgInSc2d1
klíče	klíč	k1gInSc2
a	a	k8xC
směly	smět	k5eAaImAgInP
zvýhodňovat	zvýhodňovat	k5eAaImF
afroamerické	afroamerický	k2eAgMnPc4d1
a	a	k8xC
hispánské	hispánský	k2eAgMnPc4d1
uchazeče	uchazeč	k1gMnPc4
o	o	k7c4
přijetí	přijetí	k1gNnSc4
na	na	k7c4
školu	škola	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Harvardova	Harvardův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
a	a	k8xC
některé	některý	k3yIgFnPc1
další	další	k2eAgFnPc1d1
americké	americký	k2eAgFnPc1d1
univerzity	univerzita	k1gFnPc1
se	se	k3xPyFc4
rozhodly	rozhodnout	k5eAaPmAgFnP
nařízení	nařízení	k1gNnSc4
vlády	vláda	k1gFnSc2
nerespektovat	respektovat	k5eNaImF
a	a	k8xC
chtějí	chtít	k5eAaImIp3nP
dál	daleko	k6eAd2
uplatňovat	uplatňovat	k5eAaImF
rasové	rasový	k2eAgNnSc4d1
hledisko	hledisko	k1gNnSc4
při	při	k7c6
výběru	výběr	k1gInSc6
budoucích	budoucí	k2eAgMnPc2d1
studentů	student	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zdravotní	zdravotní	k2eAgFnSc1d1
péče	péče	k1gFnSc1
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
v	v	k7c6
důsledku	důsledek	k1gInSc6
předávkování	předávkování	k1gNnPc2
drogami	droga	k1gFnPc7
v	v	k7c6
letech	let	k1gInPc6
1999	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
patří	patřit	k5eAaImIp3nP
mezi	mezi	k7c4
země	zem	k1gFnPc4
s	s	k7c7
nejvyššími	vysoký	k2eAgInPc7d3
výdaji	výdaj	k1gInPc7
na	na	k7c6
zdravotnictví	zdravotnictví	k1gNnSc6
v	v	k7c6
poměru	poměr	k1gInSc6
k	k	k7c3
HDP	HDP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
mezinárodních	mezinárodní	k2eAgInPc6d1
žebříčcích	žebříček	k1gInPc6
kvality	kvalita	k1gFnSc2
zdravotnických	zdravotnický	k2eAgInPc2d1
systémů	systém	k1gInPc2
(	(	kIx(
<g/>
WHO	WHO	kA
<g/>
,	,	kIx,
The	The	k1gMnSc1
BMJ	BMJ	kA
<g/>
)	)	kIx)
však	však	k9
nepatří	patřit	k5eNaImIp3nS
ke	k	k7c3
špičce	špička	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
se	se	k3xPyFc4
USA	USA	kA
podle	podle	k7c2
OECD	OECD	kA
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
délky	délka	k1gFnSc2
života	život	k1gInSc2
<g/>
,	,	kIx,
kojenecké	kojenecký	k2eAgFnSc2d1
úmrtnosti	úmrtnost	k1gFnSc2
nebo	nebo	k8xC
potenciálně	potenciálně	k6eAd1
ztracených	ztracený	k2eAgNnPc2d1
let	léto	k1gNnPc2
života	život	k1gInSc2
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
se	s	k7c7
zdravotním	zdravotní	k2eAgInSc7d1
stavem	stav	k1gInSc7
umístili	umístit	k5eAaPmAgMnP
v	v	k7c6
dolní	dolní	k2eAgFnSc6d1
třetině	třetina	k1gFnSc6
států	stát	k1gInPc2
OECD	OECD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
jsou	být	k5eAaImIp3nP
USA	USA	kA
zemí	zem	k1gFnPc2
s	s	k7c7
nejrozšířenějším	rozšířený	k2eAgNnSc7d3
využíváním	využívání	k1gNnSc7
moderních	moderní	k2eAgFnPc2d1
medicínských	medicínský	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
USA	USA	kA
neexistuje	existovat	k5eNaImIp3nS
univerzální	univerzální	k2eAgInSc4d1
zdravotní	zdravotní	k2eAgInSc4d1
systém	systém	k1gInSc4
zajišťující	zajišťující	k2eAgInSc4d1
péči	péče	k1gFnSc4
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
obyvatele	obyvatel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc1
státy	stát	k1gInPc4
však	však	k9
v	v	k7c6
minulosti	minulost	k1gFnSc6
vyvíjely	vyvíjet	k5eAaImAgInP
iniciativu	iniciativa	k1gFnSc4
směrem	směr	k1gInSc7
k	k	k7c3
univerzálnímu	univerzální	k2eAgInSc3d1
systému	systém	k1gInSc3
<g/>
,	,	kIx,
například	například	k6eAd1
zavedením	zavedení	k1gNnSc7
povinného	povinný	k2eAgNnSc2d1
zdravotního	zdravotní	k2eAgNnSc2d1
pojištění	pojištění	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
chudým	chudý	k2eAgInSc7d1
dotuje	dotovat	k5eAaBmIp3nS
stát	stát	k5eAaPmF,k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
hlavně	hlavně	k9
o	o	k7c4
státy	stát	k1gInPc4
Minnesota	Minnesota	k1gFnSc1
a	a	k8xC
Massachusetts	Massachusetts	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vláda	vláda	k1gFnSc1
USA	USA	kA
financuje	financovat	k5eAaBmIp3nS
z	z	k7c2
daní	daň	k1gFnPc2
zdravotní	zdravotní	k2eAgFnSc4d1
péči	péče	k1gFnSc4
pro	pro	k7c4
přibližně	přibližně	k6eAd1
28	#num#	k4
<g/>
%	%	kIx~
obyvatel	obyvatel	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
zejména	zejména	k9
pro	pro	k7c4
lidi	člověk	k1gMnPc4
nad	nad	k7c4
65	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
zdravotně	zdravotně	k6eAd1
postižené	postižený	k2eAgMnPc4d1
(	(	kIx(
<g/>
Medicare	Medicar	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
chudé	chudý	k2eAgInPc1d1
(	(	kIx(
<g/>
Medicaid	Medicaid	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pro	pro	k7c4
děti	dítě	k1gFnPc4
z	z	k7c2
rodin	rodina	k1gFnPc2
s	s	k7c7
nízkým	nízký	k2eAgInSc7d1
příjmem	příjem	k1gInSc7
(	(	kIx(
<g/>
Children	Childrna	k1gFnPc2
<g/>
'	'	kIx"
s	s	k7c7
Health	Healtha	k1gFnPc2
Insurance	Insurance	k1gFnSc1
Program	program	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
pro	pro	k7c4
vojenské	vojenský	k2eAgMnPc4d1
veterány	veterán	k1gMnPc4
(	(	kIx(
<g/>
Veterans	Veterans	k1gInSc1
Health	Health	k1gMnSc1
Administration	Administration	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
lidí	člověk	k1gMnPc2
v	v	k7c6
aktivní	aktivní	k2eAgFnSc6d1
vojenské	vojenský	k2eAgFnSc6d1
službě	služba	k1gFnSc6
(	(	kIx(
<g/>
Tricare	Tricar	k1gMnSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vláda	vláda	k1gFnSc1
také	také	k9
financuje	financovat	k5eAaBmIp3nS
kliniky	klinika	k1gFnPc4
poskytující	poskytující	k2eAgFnSc3d1
předporodní	předporodní	k2eAgFnSc3d1
péči	péče	k1gFnSc3
a	a	k8xC
spolu	spolu	k6eAd1
s	s	k7c7
charitativními	charitativní	k2eAgFnPc7d1
organizacemi	organizace	k1gFnPc7
i	i	k8xC
většinu	většina	k1gFnSc4
hospiců	hospic	k1gInPc2
pečujících	pečující	k2eAgInPc2d1
o	o	k7c4
lidi	člověk	k1gMnPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
očekávaná	očekávaný	k2eAgFnSc1d1
délka	délka	k1gFnSc1
dožití	dožití	k1gNnSc4
je	být	k5eAaImIp3nS
do	do	k7c2
šesti	šest	k4xCc2
měsíců	měsíc	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podíl	podíl	k1gInSc4
veřejných	veřejný	k2eAgInPc2d1
výdajů	výdaj	k1gInPc2
na	na	k7c6
celkových	celkový	k2eAgInPc6d1
výdajích	výdaj	k1gInPc6
na	na	k7c6
zdravotnictví	zdravotnictví	k1gNnSc6
dlouhodobě	dlouhodobě	k6eAd1
stoupá	stoupat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
většinou	většina	k1gFnSc7
jiných	jiný	k2eAgInPc2d1
států	stát	k1gInPc2
OECD	OECD	kA
vláda	vláda	k1gFnSc1
USA	USA	kA
nereguluje	regulovat	k5eNaImIp3nS
ceny	cena	k1gFnPc4
léků	lék	k1gInPc2
<g/>
,	,	kIx,
důsledkem	důsledek	k1gInSc7
čehož	což	k3yRnSc2,k3yQnSc2
jsou	být	k5eAaImIp3nP
zejména	zejména	k9
ceny	cena	k1gFnPc1
originálních	originální	k2eAgInPc2d1
léků	lék	k1gInPc2
podstatně	podstatně	k6eAd1
vyšší	vysoký	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikají	vznikat	k5eAaImIp3nP
tak	tak	k6eAd1
příznivé	příznivý	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
pro	pro	k7c4
vývoj	vývoj	k1gInSc4
nových	nový	k2eAgInPc2d1
léků	lék	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
čehož	což	k3yQnSc2,k3yRnSc2
profitují	profitovat	k5eAaBmIp3nP
i	i	k9
jiné	jiný	k2eAgInPc1d1
státy	stát	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Značná	značný	k2eAgFnSc1d1
část	část	k1gFnSc1
výdajů	výdaj	k1gInPc2
na	na	k7c4
zdravotnictví	zdravotnictví	k1gNnSc4
v	v	k7c6
USA	USA	kA
směřuje	směřovat	k5eAaImIp3nS
do	do	k7c2
výzkumu	výzkum	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většinu	většina	k1gFnSc4
(	(	kIx(
<g/>
hlavně	hlavně	k6eAd1
aplikovaného	aplikovaný	k2eAgInSc2d1
<g/>
)	)	kIx)
výzkumu	výzkum	k1gInSc2
financují	financovat	k5eAaBmIp3nP
soukromé	soukromý	k2eAgFnPc1d1
ziskové	ziskový	k2eAgFnPc1d1
firmy	firma	k1gFnPc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
vláda	vláda	k1gFnSc1
se	se	k3xPyFc4
orientuje	orientovat	k5eAaBmIp3nS
zejména	zejména	k9
na	na	k7c4
základní	základní	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1
diskuse	diskuse	k1gFnSc1
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
hlavně	hlavně	k6eAd1
problému	problém	k1gInSc3
rychle	rychle	k6eAd1
rostoucích	rostoucí	k2eAgInPc2d1
nákladů	náklad	k1gInPc2
<g/>
,	,	kIx,
nerovného	rovný	k2eNgInSc2d1
přístupu	přístup	k1gInSc2
ke	k	k7c3
zdravotní	zdravotní	k2eAgFnSc3d1
péči	péče	k1gFnSc3
a	a	k8xC
nárůstu	nárůst	k1gInSc3
počtu	počet	k1gInSc2
nepojištěných	pojištěný	k2eNgFnPc2d1
<g/>
,	,	kIx,
resp.	resp.	kA
nedostatečně	dostatečně	k6eNd1
pojištěných	pojištěný	k2eAgMnPc2d1
Američanů	Američan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
bylo	být	k5eAaImAgNnS
bez	bez	k7c2
zdravotního	zdravotní	k2eAgNnSc2d1
pojištění	pojištění	k1gNnSc2
28,6	28,6	k4
milionu	milion	k4xCgInSc2
Američanů	Američan	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
nejvíce	hodně	k6eAd3,k6eAd1
diskutovaným	diskutovaný	k2eAgNnPc3d1
řešením	řešení	k1gNnPc3
patří	patřit	k5eAaImIp3nP
různé	různý	k2eAgFnPc1d1
možnosti	možnost	k1gFnPc1
zavedení	zavedení	k1gNnSc2
univerzálního	univerzální	k2eAgInSc2d1
zdravotního	zdravotní	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Společnost	společnost	k1gFnSc1
</s>
<s>
Americký	americký	k2eAgInSc1d1
způsob	způsob	k1gInSc1
života	život	k1gInSc2
</s>
<s>
Američané	Američan	k1gMnPc1
sledují	sledovat	k5eAaImIp3nP
baseballové	baseballový	k2eAgNnSc4d1
utkání	utkání	k1gNnSc4
na	na	k7c6
stadionu	stadion	k1gInSc6
v	v	k7c6
Omaze	Omaza	k1gFnSc6
</s>
<s>
Společnost	společnost	k1gFnSc1
je	být	k5eAaImIp3nS
často	často	k6eAd1
charakterizována	charakterizovat	k5eAaBmNgFnS
"	"	kIx"
<g/>
americkým	americký	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
života	život	k1gInSc2
<g/>
"	"	kIx"
(	(	kIx(
<g/>
American	American	k1gInSc1
way	way	k?
of	of	k?
life	life	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
termín	termín	k1gInSc1
označuje	označovat	k5eAaImIp3nS
specifický	specifický	k2eAgInSc1d1
nacionalistický	nacionalistický	k2eAgInSc1d1
étos	étos	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednoznačná	jednoznačný	k2eAgFnSc1d1
definice	definice	k1gFnSc1
neexistuje	existovat	k5eNaImIp3nS
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
pojem	pojem	k1gInSc1
je	být	k5eAaImIp3nS
často	často	k6eAd1
spojován	spojovat	k5eAaImNgMnS
s	s	k7c7
citací	citace	k1gFnSc7
z	z	k7c2
americké	americký	k2eAgFnSc2d1
Deklarace	deklarace	k1gFnSc2
nezávislosti	nezávislost	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
nezcizitelným	zcizitelný	k2eNgNnSc7d1
lidským	lidský	k2eAgNnSc7d1
právem	právo	k1gNnSc7
je	být	k5eAaImIp3nS
právo	právo	k1gNnSc4
na	na	k7c4
"	"	kIx"
<g/>
život	život	k1gInSc4
<g/>
,	,	kIx,
svobodu	svoboda	k1gFnSc4
a	a	k8xC
usilování	usilování	k1gNnSc4
o	o	k7c4
štěstí	štěstí	k1gNnSc4
<g/>
"	"	kIx"
(	(	kIx(
<g/>
Life	Lif	k1gInPc1
<g/>
,	,	kIx,
Liberty	Libert	k1gInPc1
and	and	k?
the	the	k?
<g />
.	.	kIx.
</s>
<s hack="1">
pursuit	pursuit	k2eAgInSc1d1
of	of	k?
Happiness	Happiness	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
Pojem	pojem	k1gInSc1
úzce	úzko	k6eAd1
souvisí	souviset	k5eAaImIp3nS
rovněž	rovněž	k9
s	s	k7c7
termínem	termín	k1gInSc7
americký	americký	k2eAgInSc1d1
sen	sen	k1gInSc1
<g/>
,	,	kIx,
kterým	který	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
víra	víra	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
společnost	společnost	k1gFnSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
organizována	organizovat	k5eAaBmNgFnS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
každý	každý	k3xTgMnSc1
Američan	Američan	k1gMnSc1
mohl	moct	k5eAaImAgMnS
dosáhnout	dosáhnout	k5eAaPmF
sociálního	sociální	k2eAgInSc2d1
vzestupu	vzestup	k1gInSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
je	být	k5eAaImIp3nS
ochoten	ochoten	k2eAgMnSc1d1
tvrdě	tvrdě	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
pracovat	pracovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
116	#num#	k4
<g/>
]	]	kIx)
Termín	termín	k1gInSc1
tak	tak	k6eAd1
odkazuje	odkazovat	k5eAaImIp3nS
k	k	k7c3
tradici	tradice	k1gFnSc3
vertikální	vertikální	k2eAgFnSc2d1
sociální	sociální	k2eAgFnSc2d1
mobility	mobilita	k1gFnSc2
(	(	kIx(
<g/>
"	"	kIx"
<g/>
ze	z	k7c2
dna	dno	k1gNnSc2
až	až	k9
na	na	k7c4
vrchol	vrchol	k1gInSc4
<g/>
"	"	kIx"
<g/>
)	)	kIx)
a	a	k8xC
rovnostářství	rovnostářství	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
podmíněné	podmíněný	k2eAgFnPc4d1
odbouráním	odbourání	k1gNnSc7
tradičních	tradiční	k2eAgNnPc2d1
privilegií	privilegium	k1gNnPc2
(	(	kIx(
<g/>
např.	např.	kA
šlechtických	šlechtický	k2eAgFnPc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
odvrhly	odvrhnout	k5eAaPmAgInP
při	při	k7c6
své	svůj	k3xOyFgFnSc6
emancipaci	emancipace	k1gFnSc6
od	od	k7c2
britského	britský	k2eAgNnSc2d1
koloniálního	koloniální	k2eAgNnSc2d1
centra	centrum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zprvu	zprvu	k6eAd1
komunistický	komunistický	k2eAgMnSc1d1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
konzervativní	konzervativní	k2eAgMnSc1d1
myslitel	myslitel	k1gMnSc1
William	William	k1gInSc1
Herberg	Herberg	k1gInSc4
definoval	definovat	k5eAaBmAgInS
americký	americký	k2eAgInSc1d1
způsob	způsob	k1gInSc1
života	život	k1gInSc2
individualismem	individualismus	k1gInSc7
<g/>
,	,	kIx,
pohybem	pohyb	k1gInSc7
a	a	k8xC
pragmatismem	pragmatismus	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
Individualismus	individualismus	k1gInSc1
stojí	stát	k5eAaImIp3nS
na	na	k7c6
víře	víra	k1gFnSc6
<g/>
,	,	kIx,
že	že	k8xS
každý	každý	k3xTgMnSc1
jedinec	jedinec	k1gMnSc1
je	být	k5eAaImIp3nS
důležitý	důležitý	k2eAgInSc4d1
a	a	k8xC
boj	boj	k1gInSc4
za	za	k7c4
svobodu	svoboda	k1gFnSc4
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
vždy	vždy	k6eAd1
bojem	boj	k1gInSc7
za	za	k7c4
svobodu	svoboda	k1gFnSc4
jedince	jedinec	k1gMnSc2
<g/>
,	,	kIx,
a	a	k8xC
že	že	k8xS
každý	každý	k3xTgInSc4
sociální	sociální	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
musí	muset	k5eAaImIp3nS
umožňovat	umožňovat	k5eAaImF
rozvoj	rozvoj	k1gInSc4
jedince	jedinec	k1gMnSc2
a	a	k8xC
zmnožování	zmnožování	k1gNnSc1
jeho	jeho	k3xOp3gFnPc2
možností	možnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Individualismus	individualismus	k1gInSc4
také	také	k9
přináší	přinášet	k5eAaImIp3nS
závazek	závazek	k1gInSc1
práce	práce	k1gFnSc2
na	na	k7c6
sobě	sebe	k3xPyFc6
samém	samý	k3xTgMnSc6
a	a	k8xC
sebezdokonalování	sebezdokonalování	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohybem	pohyb	k1gInSc7
je	být	k5eAaImIp3nS
míněna	míněn	k2eAgFnSc1d1
jak	jak	k8xS,k8xC
výše	vysoce	k6eAd2
uvedená	uvedený	k2eAgFnSc1d1
sociální	sociální	k2eAgFnSc1d1
mobilita	mobilita	k1gFnSc1
(	(	kIx(
<g/>
pohyb	pohyb	k1gInSc1
po	po	k7c6
sociálním	sociální	k2eAgInSc6d1
žebříku	žebřík	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k6eAd1
ochota	ochota	k1gFnSc1
nesetrvávat	setrvávat	k5eNaImF
na	na	k7c6
starých	starý	k2eAgFnPc6d1
tradicích	tradice	k1gFnPc6
a	a	k8xC
otevřenost	otevřenost	k1gFnSc4
vůči	vůči	k7c3
novinkám	novinka	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
přináší	přinášet	k5eAaImIp3nP
pokrok	pokrok	k1gInSc4
<g/>
,	,	kIx,
s	s	k7c7
čímž	což	k3yQnSc7,k3yRnSc7
je	být	k5eAaImIp3nS
spojen	spojit	k5eAaPmNgInS
i	i	k9
"	"	kIx"
<g/>
americký	americký	k2eAgInSc1d1
optimismus	optimismus	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
nadějeplné	nadějeplný	k2eAgNnSc4d1
hledění	hledění	k1gNnSc4
k	k	k7c3
horizontu	horizont	k1gInSc3
a	a	k8xC
budoucnu	budoucno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pragmatismem	pragmatismus	k1gInSc7
je	být	k5eAaImIp3nS
míněna	míněn	k2eAgFnSc1d1
jak	jak	k8xS,k8xC
konkrétní	konkrétní	k2eAgFnSc1d1
pragmatická	pragmatický	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
ryze	ryze	k6eAd1
americká	americký	k2eAgFnSc1d1
filozofická	filozofický	k2eAgFnSc1d1
škola	škola	k1gFnSc1
vůbec	vůbec	k9
(	(	kIx(
<g/>
John	John	k1gMnSc1
Dewey	Dewea	k1gFnSc2
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
Sanders	Sandersa	k1gFnPc2
Peirce	Peirce	k1gMnSc1
<g/>
,	,	kIx,
William	William	k1gInSc1
James	James	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k6eAd1
obecnější	obecní	k2eAgFnSc1d2
ochota	ochota	k1gFnSc1
k	k	k7c3
hledání	hledání	k1gNnSc3
reálných	reálný	k2eAgInPc2d1
<g/>
,	,	kIx,
kompromisních	kompromisní	k2eAgNnPc2d1
řešení	řešení	k1gNnPc2
problémů	problém	k1gInPc2
v	v	k7c6
rámci	rámec	k1gInSc6
demokratické	demokratický	k2eAgFnSc2d1
diskuse	diskuse	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
je	být	k5eAaImIp3nS
pojem	pojem	k1gInSc1
"	"	kIx"
<g/>
americký	americký	k2eAgInSc1d1
způsob	způsob	k1gInSc1
života	život	k1gInSc2
<g/>
"	"	kIx"
spojován	spojován	k2eAgMnSc1d1
též	též	k9
s	s	k7c7
aktivitou	aktivita	k1gFnSc7
a	a	k8xC
vírou	víra	k1gFnSc7
v	v	k7c6
práci	práce	k1gFnSc6
a	a	k8xC
její	její	k3xOp3gFnSc7
duchovní	duchovní	k2eAgFnSc7d1
hodnotou	hodnota	k1gFnSc7
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
též	též	k9
Weberova	Weberův	k2eAgFnSc1d1
"	"	kIx"
<g/>
protestantská	protestantský	k2eAgFnSc1d1
etika	etika	k1gFnSc1
kapitalismu	kapitalismus	k1gInSc2
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
USA	USA	kA
je	být	k5eAaImIp3nS
také	také	k9
často	často	k6eAd1
dáván	dávat	k5eAaImNgInS
do	do	k7c2
souvislosti	souvislost	k1gFnSc2
s	s	k7c7
velmi	velmi	k6eAd1
obecným	obecný	k2eAgInSc7d1
termínem	termín	k1gInSc7
"	"	kIx"
<g/>
svoboda	svoboda	k1gFnSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
freedom	freedom	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
projevuje	projevovat	k5eAaImIp3nS
v	v	k7c6
různorodých	různorodý	k2eAgInPc6d1
fenoménech	fenomén	k1gInPc6
jako	jako	k8xC,k8xS
americká	americký	k2eAgFnSc1d1
ochota	ochota	k1gFnSc1
se	se	k3xPyFc4
stěhovat	stěhovat	k5eAaImF
a	a	k8xC
nedržet	držet	k5eNaImF
se	se	k3xPyFc4
jednoho	jeden	k4xCgNnSc2
místa	místo	k1gNnSc2
<g/>
,	,	kIx,
odpor	odpor	k1gInSc1
k	k	k7c3
přílišné	přílišný	k2eAgFnSc3d1
byrokratizaci	byrokratizace	k1gFnSc3
společnosti	společnost	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
k	k	k7c3
občanským	občanský	k2eAgInPc3d1
průkazům	průkaz	k1gInPc3
evropského	evropský	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
s	s	k7c7
fenomény	fenomén	k1gNnPc7
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
moderní	moderní	k2eAgNnSc1d1
nomádství	nomádství	k1gNnSc1
za	za	k7c2
pomoci	pomoc	k1gFnSc2
karavanů	karavan	k1gInPc2
<g/>
,	,	kIx,
život	život	k1gInSc4
subkultur	subkultura	k1gFnPc2
typu	typ	k1gInSc2
uživatelů	uživatel	k1gMnPc2
motorek	motorka	k1gFnPc2
Harley	harley	k1gInSc1
Davidson	Davidson	k1gMnSc1
apod.	apod.	kA
Velkou	velký	k2eAgFnSc4d1
roli	role	k1gFnSc4
ve	v	k7c6
vnímání	vnímání	k1gNnSc6
pojmu	pojem	k1gInSc2
svoboda	svoboda	k1gFnSc1
hraje	hrát	k5eAaImIp3nS
v	v	k7c6
ústavě	ústava	k1gFnSc6
USA	USA	kA
zakotvené	zakotvený	k2eAgNnSc1d1
právo	právo	k1gNnSc1
na	na	k7c4
vlastnění	vlastnění	k1gNnSc4
ručních	ruční	k2eAgFnPc2d1
palných	palný	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgInPc6
státech	stát	k1gInPc6
<g/>
,	,	kIx,
například	například	k6eAd1
v	v	k7c6
Texasu	Texas	k1gInSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
dovoleno	dovolen	k2eAgNnSc1d1
nosit	nosit	k5eAaImF
ruční	ruční	k2eAgFnPc4d1
zbraně	zbraň	k1gFnPc4
viditelně	viditelně	k6eAd1
u	u	k7c2
sebe	se	k3xPyFc2
<g/>
,	,	kIx,
v	v	k7c6
městě	město	k1gNnSc6
New	New	k1gFnSc2
Yorku	York	k1gInSc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
naopak	naopak	k6eAd1
zakázáno	zakázat	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kontrakultura	Kontrakultura	k1gFnSc1
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
často	často	k6eAd1
spojovala	spojovat	k5eAaImAgFnS
s	s	k7c7
americkým	americký	k2eAgNnSc7d1
pojetím	pojetí	k1gNnSc7
svobody	svoboda	k1gFnSc2
a	a	k8xC
tedy	tedy	k9
i	i	k9
americkým	americký	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
života	život	k1gInSc2
drogy	droga	k1gFnSc2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
se	se	k3xPyFc4
společenské	společenský	k2eAgNnSc1d1
vědomí	vědomí	k1gNnSc1
změnilo	změnit	k5eAaPmAgNnS
a	a	k8xC
toto	tento	k3xDgNnSc1
spojení	spojení	k1gNnSc1
bylo	být	k5eAaImAgNnS
rozbito	rozbít	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
tím	ten	k3xDgNnSc7
souvisela	souviset	k5eAaImAgFnS
i	i	k8xC
kampaň	kampaň	k1gFnSc1
proti	proti	k7c3
jiným	jiný	k2eAgFnPc3d1
závislostem	závislost	k1gFnPc3
<g/>
,	,	kIx,
zejména	zejména	k9
proti	proti	k7c3
kouření	kouření	k1gNnSc3
cigaret	cigareta	k1gFnPc2
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Americká	americký	k2eAgFnSc1d1
svoboda	svoboda	k1gFnSc1
<g/>
"	"	kIx"
je	být	k5eAaImIp3nS
často	často	k6eAd1
dávána	dávat	k5eAaImNgFnS
do	do	k7c2
souvislosti	souvislost	k1gFnSc2
s	s	k7c7
absolutní	absolutní	k2eAgFnSc7d1
svobodou	svoboda	k1gFnSc7
slova	slovo	k1gNnSc2
(	(	kIx(
<g/>
americké	americký	k2eAgNnSc1d1
zákonodárství	zákonodárství	k1gNnSc1
nezná	znát	k5eNaImIp3nS,k5eAaImIp3nS
kategorii	kategorie	k1gFnSc4
verbálního	verbální	k2eAgInSc2d1
trestného	trestný	k2eAgInSc2d1
činu	čin	k1gInSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
tedy	tedy	k9
například	například	k6eAd1
i	i	k9
s	s	k7c7
dostupností	dostupnost	k1gFnSc7
<g />
.	.	kIx.
</s>
<s hack="1">
pornografie	pornografie	k1gFnSc1
<g/>
,	,	kIx,
ovšem	ovšem	k9
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
je	být	k5eAaImIp3nS
společenská	společenský	k2eAgFnSc1d1
shoda	shoda	k1gFnSc1
mnohem	mnohem	k6eAd1
menší	malý	k2eAgFnSc1d2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
široké	široký	k2eAgFnPc1d1
vrstvy	vrstva	k1gFnPc1
obyvatelstva	obyvatelstvo	k1gNnSc2
v	v	k7c6
USA	USA	kA
jsou	být	k5eAaImIp3nP
nábožensky	nábožensky	k6eAd1
založené	založený	k2eAgInPc1d1
a	a	k8xC
většina	většina	k1gFnSc1
mravních	mravní	k2eAgFnPc2d1
zásad	zásada	k1gFnPc2
je	být	k5eAaImIp3nS
odvozena	odvodit	k5eAaPmNgFnS
z	z	k7c2
puritánství	puritánství	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
118	#num#	k4
<g/>
]	]	kIx)
To	ten	k3xDgNnSc1
ukazuje	ukazovat	k5eAaImIp3nS
například	například	k6eAd1
prudérní	prudérní	k2eAgInSc1d1
filmový	filmový	k2eAgInSc1d1
rating	rating	k1gInSc1
MPAA	MPAA	kA
či	či	k8xC
citlivost	citlivost	k1gFnSc1
na	na	k7c4
sexismus	sexismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
je	být	k5eAaImIp3nS
pojem	pojem	k1gInSc1
"	"	kIx"
<g/>
americký	americký	k2eAgInSc1d1
způsob	způsob	k1gInSc1
života	život	k1gInSc2
<g/>
"	"	kIx"
často	často	k6eAd1
chápán	chápat	k5eAaImNgInS
jako	jako	k8xS,k8xC
víra	víra	k1gFnSc1
v	v	k7c4
kapitalismus	kapitalismus	k1gInSc4
<g/>
,	,	kIx,
obchod	obchod	k1gInSc4
a	a	k8xC
konzumerismus	konzumerismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někdy	někdy	k6eAd1
je	být	k5eAaImIp3nS
též	též	k9
dáván	dávat	k5eAaImNgInS
do	do	k7c2
souvislosti	souvislost	k1gFnSc2
s	s	k7c7
vírou	víra	k1gFnSc7
v	v	k7c4
americkou	americký	k2eAgFnSc4d1
výjimečnost	výjimečnost	k1gFnSc4
a	a	k8xC
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
pak	pak	k6eAd1
hanlivý	hanlivý	k2eAgInSc4d1
přídech	přídech	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Chudoba	Chudoba	k1gMnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Ekonomika	ekonomika	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
#	#	kIx~
<g/>
Ekonomická	ekonomický	k2eAgFnSc1d1
situace	situace	k1gFnSc1
Američanů	Američan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Rostoucí	rostoucí	k2eAgFnSc1d1
ekonomická	ekonomický	k2eAgFnSc1d1
nerovnost	nerovnost	k1gFnSc1
v	v	k7c6
USA	USA	kA
</s>
<s>
Studie	studie	k1gFnSc1
OSN	OSN	kA
z	z	k7c2
května	květen	k1gInSc2
2018	#num#	k4
konstatovala	konstatovat	k5eAaBmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
40	#num#	k4
milionů	milion	k4xCgInPc2
Američanů	Američan	k1gMnPc2
žije	žít	k5eAaImIp3nS
v	v	k7c6
chudobě	chudoba	k1gFnSc6
a	a	k8xC
5	#num#	k4
milionů	milion	k4xCgInPc2
žije	žít	k5eAaImIp3nS
v	v	k7c6
podmínkách	podmínka	k1gFnPc6
třetího	třetí	k4xOgMnSc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
119	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
120	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
údajů	údaj	k1gInPc2
americké	americký	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
koalice	koalice	k1gFnSc2
proti	proti	k7c3
bezdomovectví	bezdomovectví	k1gNnSc3
z	z	k7c2
roku	rok	k1gInSc2
2009	#num#	k4
má	mít	k5eAaImIp3nS
44	#num#	k4
%	%	kIx~
bezdomovců	bezdomovec	k1gMnPc2
práci	práce	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
přesto	přesto	k8xC
nedokáží	dokázat	k5eNaPmIp3nP
bezdomovectví	bezdomovectví	k1gNnSc4
uniknout	uniknout	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
121	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kriminalita	kriminalita	k1gFnSc1
</s>
<s>
Vývoj	vývoj	k1gInSc1
vězeňské	vězeňský	k2eAgFnSc2d1
populace	populace	k1gFnSc2
v	v	k7c6
USA	USA	kA
</s>
<s>
V	v	k7c6
USA	USA	kA
je	být	k5eAaImIp3nS
kriminalita	kriminalita	k1gFnSc1
relativně	relativně	k6eAd1
vyšší	vysoký	k2eAgFnSc1d2
než	než	k8xS
v	v	k7c6
Evropě	Evropa	k1gFnSc6
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
bylo	být	k5eAaImAgNnS
4,3	4,3	k4
vraždy	vražda	k1gFnSc2
na	na	k7c4
100	#num#	k4
000	#num#	k4
obyvatel	obyvatel	k1gMnPc2
za	za	k7c4
rok	rok	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
se	se	k3xPyFc4
ale	ale	k9
liší	lišit	k5eAaImIp3nP
dle	dle	k7c2
státu	stát	k1gInSc2
a	a	k8xC
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průřezová	průřezový	k2eAgFnSc1d1
analýza	analýza	k1gFnSc1
Světové	světový	k2eAgFnSc2d1
zdravotnické	zdravotnický	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
Mortality	mortalita	k1gFnSc2
Database	Databasa	k1gFnSc3
z	z	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
ukázala	ukázat	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
míra	míra	k1gFnSc1
vražd	vražda	k1gFnPc2
je	být	k5eAaImIp3nS
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
sedmkrát	sedmkrát	k6eAd1
vyšší	vysoký	k2eAgFnPc1d2
než	než	k8xS
v	v	k7c6
jiných	jiný	k2eAgFnPc6d1
rozvinutých	rozvinutý	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
(	(	kIx(
<g/>
nicméně	nicméně	k8xC
zhruba	zhruba	k6eAd1
na	na	k7c6
celosvětovém	celosvětový	k2eAgInSc6d1
průměru	průměr	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Míra	Míra	k1gFnSc1
vražd	vražda	k1gFnPc2
s	s	k7c7
použitím	použití	k1gNnSc7
střelné	střelný	k2eAgFnSc2d1
zbraně	zbraň	k1gFnSc2
pak	pak	k6eAd1
25,2	25,2	k4
<g/>
krát	krát	k6eAd1
vyšší	vysoký	k2eAgInPc1d2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
122	#num#	k4
<g/>
]	]	kIx)
Nebezpečná	bezpečný	k2eNgNnPc1d1
jsou	být	k5eAaImIp3nP
zejména	zejména	k9
ghetta	ghetto	k1gNnPc1
obývaná	obývaný	k2eAgFnSc1d1
městskou	městský	k2eAgFnSc7d1
chudinou	chudina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
s	s	k7c7
nejvyšší	vysoký	k2eAgFnSc7d3
mírou	míra	k1gFnSc7
kriminality	kriminalita	k1gFnSc2
je	být	k5eAaImIp3nS
Detroit	Detroit	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
ukazatel	ukazatel	k1gInSc4
vražd	vražda	k1gFnPc2
na	na	k7c4
zhruba	zhruba	k6eAd1
desetinásobku	desetinásobek	k1gInSc3
amerického	americký	k2eAgInSc2d1
průměru	průměr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
států	stát	k1gInPc2
měla	mít	k5eAaImAgFnS
k	k	k7c3
roku	rok	k1gInSc3
2012	#num#	k4
největší	veliký	k2eAgFnSc1d3
vražednost	vražednost	k1gFnSc1
Louisiana	Louisiana	k1gFnSc1
<g/>
,	,	kIx,
nejnižší	nízký	k2eAgFnSc1d3
pak	pak	k6eAd1
New	New	k1gMnSc5
Hampshire	Hampshir	k1gMnSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
123	#num#	k4
<g/>
]	]	kIx)
Právo	právo	k1gNnSc4
nosit	nosit	k5eAaImF
zbraň	zbraň	k1gFnSc4
zaručuje	zaručovat	k5eAaImIp3nS
Američanům	Američan	k1gMnPc3
jeden	jeden	k4xCgInSc1
z	z	k7c2
dodatků	dodatek	k1gInPc2
ústavy	ústava	k1gFnSc2
<g/>
,	,	kIx,
byť	byť	k8xS
se	se	k3xPyFc4
o	o	k7c6
věci	věc	k1gFnSc6
vede	vést	k5eAaImIp3nS
široká	široký	k2eAgFnSc1d1
společenská	společenský	k2eAgFnSc1d1
diskuse	diskuse	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
údajů	údaj	k1gInPc2
FBI	FBI	kA
z	z	k7c2
let	léto	k1gNnPc2
1980	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
spáchali	spáchat	k5eAaPmAgMnP
90	#num#	k4
%	%	kIx~
vražd	vražda	k1gFnPc2
muži	muž	k1gMnPc1
a	a	k8xC
52,5	52,5	k4
%	%	kIx~
černoši	černoch	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Černoši	černoch	k1gMnPc1
tak	tak	k9
v	v	k7c6
USA	USA	kA
páchají	páchat	k5eAaImIp3nP
vraždy	vražda	k1gFnSc2
vzhledem	vzhled	k1gInSc7
k	k	k7c3
nižšímu	nízký	k2eAgNnSc3d2
zastoupení	zastoupení	k1gNnSc3
v	v	k7c6
populaci	populace	k1gFnSc6
osmkrát	osmkrát	k6eAd1
častěji	často	k6eAd2
než	než	k8xS
bílí	bílý	k2eAgMnPc1d1
Američané	Američan	k1gMnPc1
(	(	kIx(
<g/>
počítáno	počítat	k5eAaImNgNnS
v	v	k7c4
to	ten	k3xDgNnSc4
i	i	k9
Hispánce	Hispánec	k1gMnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
vražd	vražda	k1gFnPc2
je	být	k5eAaImIp3nS
nicméně	nicméně	k8xC
„	„	k?
<g/>
uvnitř	uvnitř	k7c2
jedné	jeden	k4xCgFnSc2
rasy	rasa	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
93	#num#	k4
%	%	kIx~
zavražděných	zavražděný	k2eAgMnPc2d1
černochů	černoch	k1gMnPc2
připadá	připadat	k5eAaImIp3nS,k5eAaPmIp3nS
na	na	k7c4
černošské	černošský	k2eAgMnPc4d1
vrahy	vrah	k1gMnPc4
<g/>
,	,	kIx,
84	#num#	k4
%	%	kIx~
bílých	bílý	k2eAgFnPc2d1
obětí	oběť	k1gFnPc2
bylo	být	k5eAaImAgNnS
zabito	zabít	k5eAaPmNgNnS
bílými	bílý	k2eAgFnPc7d1
vrahy	vrah	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
124	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Třicet	třicet	k4xCc1
z	z	k7c2
padesáti	padesát	k4xCc2
států	stát	k1gInPc2
federace	federace	k1gFnSc2
má	mít	k5eAaImIp3nS
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
zákonodárství	zákonodárství	k1gNnSc1
trest	trest	k1gInSc4
smrti	smrt	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
125	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
1967	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1977	#num#	k4
se	se	k3xPyFc4
kvůli	kvůli	k7c3
rozhodnutí	rozhodnutí	k1gNnSc3
Nejvyššího	vysoký	k2eAgInSc2d3
soudu	soud	k1gInSc2
USA	USA	kA
popravy	poprava	k1gFnPc1
nevykonávaly	vykonávat	k5eNaImAgFnP
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
po	po	k7c6
průlomovém	průlomový	k2eAgNnSc6d1
rozhodnutí	rozhodnutí	k1gNnSc6
Nejvyšší	vysoký	k2eAgInSc1d3
soudu	soud	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1976	#num#	k4
byla	být	k5eAaImAgFnS
praxe	praxe	k1gFnSc1
obnovena	obnoven	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
bylo	být	k5eAaImAgNnS
vykonáno	vykonat	k5eAaPmNgNnS
zhruba	zhruba	k6eAd1
1300	#num#	k4
proprav	proprava	k1gFnPc2
<g/>
,	,	kIx,
drtivá	drtivý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
ve	v	k7c6
třech	tři	k4xCgInPc6
státech	stát	k1gInPc6
<g/>
:	:	kIx,
Texas	Texas	k1gInSc1
<g/>
,	,	kIx,
Virginie	Virginie	k1gFnSc1
a	a	k8xC
Oklahoma	Oklahoma	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
126	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
měly	mít	k5eAaImAgInP
díky	díky	k7c3
tomu	ten	k3xDgMnSc3
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
pátý	pátý	k4xOgInSc1
nejvyšší	vysoký	k2eAgInSc1d3
počet	počet	k1gInSc1
poprav	poprava	k1gFnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
po	po	k7c6
Číně	Čína	k1gFnSc6
<g/>
,	,	kIx,
Íránu	Írán	k1gInSc6
<g/>
,	,	kIx,
Pákistánu	Pákistán	k1gInSc6
a	a	k8xC
Saúdské	saúdský	k2eAgFnSc6d1
Arábii	Arábie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
127	#num#	k4
<g/>
]	]	kIx)
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
mají	mít	k5eAaImIp3nP
rovněž	rovněž	k9
nejvyšší	vysoký	k2eAgInSc4d3
počet	počet	k1gInSc4
uvězněných	uvězněný	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc4
vězňů	vězeň	k1gMnPc2
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
1980	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
zečtyřnásobil	zečtyřnásobit	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
128	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
kritiků	kritik	k1gMnPc2
měla	mít	k5eAaImAgFnS
na	na	k7c4
tento	tento	k3xDgInSc4
trend	trend	k1gInSc4
vliv	vliv	k1gInSc1
privatizace	privatizace	k1gFnSc2
vězeňství	vězeňství	k1gNnSc2
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
rozhodující	rozhodující	k2eAgFnSc1d1
však	však	k9
byla	být	k5eAaImAgFnS
patrně	patrně	k6eAd1
změna	změna	k1gFnSc1
postoje	postoj	k1gInSc2
státu	stát	k1gInSc2
k	k	k7c3
drogám	droga	k1gFnPc3
od	od	k7c2
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
–	–	k?
podle	podle	k7c2
Federal	Federal	k1gFnSc2
Bureau	Bureaus	k1gInSc2
of	of	k?
Prisons	Prisonsa	k1gFnPc2
je	být	k5eAaImIp3nS
většina	většina	k1gFnSc1
vězňů	vězeň	k1gMnPc2
ve	v	k7c6
federálních	federální	k2eAgFnPc6d1
věznicích	věznice	k1gFnPc6
odsouzena	odsoudit	k5eAaPmNgFnS,k5eAaImNgFnS
za	za	k7c2
trestné	trestný	k2eAgFnSc2d1
činy	čina	k1gFnSc2
spjaté	spjatý	k2eAgFnPc4d1
s	s	k7c7
drogami	droga	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgFnSc1d3
míra	míra	k1gFnSc1
uvězněnosti	uvězněnost	k1gFnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
Oklahomě	Oklahomě	k1gFnSc6
<g/>
,	,	kIx,
nejnižší	nízký	k2eAgFnSc1d3
v	v	k7c6
Massachusetts	Massachusetts	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
129	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
se	se	k3xPyFc4
dlouhodobě	dlouhodobě	k6eAd1
potýkají	potýkat	k5eAaImIp3nP
s	s	k7c7
drogovou	drogový	k2eAgFnSc7d1
epidemií	epidemie	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
130	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
zemřelo	zemřít	k5eAaPmAgNnS
na	na	k7c6
předávkování	předávkování	k1gNnSc6
drogami	droga	k1gFnPc7
rekordních	rekordní	k2eAgInPc2d1
72	#num#	k4
000	#num#	k4
Američanů	Američan	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
131	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článcích	článek	k1gInPc6
Ekonomika	ekonomika	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
,	,	kIx,
Ekonomická	ekonomický	k2eAgFnSc1d1
krize	krize	k1gFnSc1
od	od	k7c2
2007	#num#	k4
a	a	k8xC
Státní	státní	k2eAgInSc1d1
dluh	dluh	k1gInSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Stav	stav	k1gInSc1
národního	národní	k2eAgNnSc2d1
hospodářství	hospodářství	k1gNnSc2
</s>
<s>
Obchodní	obchodní	k2eAgFnSc1d1
bilance	bilance	k1gFnSc1
od	od	k7c2
roku	rok	k1gInSc2
1960	#num#	k4
</s>
<s>
Walmart	Walmart	k1gInSc1
je	být	k5eAaImIp3nS
podle	podle	k7c2
objemu	objem	k1gInSc2
tržeb	tržba	k1gFnPc2
největším	veliký	k2eAgMnSc7d3
maloobchodním	maloobchodní	k2eAgInSc7d1
řetězcem	řetězec	k1gInSc7
na	na	k7c6
světě	svět	k1gInSc6
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
je	být	k5eAaImIp3nS
nejsilnější	silný	k2eAgFnSc1d3
národní	národní	k2eAgFnSc7d1
ekonomikou	ekonomika	k1gFnSc7
<g/>
,	,	kIx,
druhým	druhý	k4xOgInSc7
největším	veliký	k2eAgInSc7d3
ekonomickým	ekonomický	k2eAgInSc7d1
celkem	celek	k1gInSc7
na	na	k7c6
světě	svět	k1gInSc6
za	za	k7c7
ekonomikou	ekonomika	k1gFnSc7
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
132	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
země	země	k1gFnSc1
s	s	k7c7
druhým	druhý	k4xOgInSc7
největším	veliký	k2eAgInSc7d3
objemem	objem	k1gInSc7
zahraničního	zahraniční	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
(	(	kIx(
<g/>
za	za	k7c7
Čínou	Čína	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
133	#num#	k4
<g/>
]	]	kIx)
Její	její	k3xOp3gInSc1
hrubý	hrubý	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
produkt	produkt	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
byl	být	k5eAaImAgInS
14	#num#	k4
991	#num#	k4
miliard	miliarda	k4xCgFnPc2
dolarů	dolar	k1gInPc2
<g/>
,	,	kIx,
s	s	k7c7
48	#num#	k4
350	#num#	k4
dolary	dolar	k1gInPc7
na	na	k7c4
obyvatele	obyvatel	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekonomiku	ekonomika	k1gFnSc4
USA	USA	kA
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nS
služby	služba	k1gFnPc4
a	a	k8xC
výzkum	výzkum	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významný	významný	k2eAgInSc1d1
je	být	k5eAaImIp3nS
i	i	k9
těžební	těžební	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemědělství	zemědělství	k1gNnSc1
tvoří	tvořit	k5eAaImIp3nP
jen	jen	k9
1	#num#	k4
%	%	kIx~
hrubého	hrubý	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
produktu	produkt	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c6
vývozech	vývoz	k1gInPc6
se	se	k3xPyFc4
jeho	jeho	k3xOp3gInPc7
výrobky	výrobek	k1gInPc7
podílí	podílet	k5eAaImIp3nP
10,5	10,5	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
oblasti	oblast	k1gFnSc2
nazvané	nazvaný	k2eAgFnSc2d1
Silicon	Silicon	kA
Valley	Vallea	k1gFnSc2
<g/>
,	,	kIx,
sídlí	sídlet	k5eAaImIp3nS
také	také	k9
řada	řada	k1gFnSc1
technologických	technologický	k2eAgFnPc2d1
firem	firma	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
známými	známý	k2eAgFnPc7d1
firmami	firma	k1gFnPc7
najdeme	najít	k5eAaPmIp1nP
společnosti	společnost	k1gFnSc2
Google	Google	k1gFnSc2
<g/>
,	,	kIx,
Amazon	amazona	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
Microsoft	Microsoft	kA
<g/>
,	,	kIx,
Facebook	Facebook	k1gInSc4
<g/>
,	,	kIx,
Apple	Apple	kA
a	a	k8xC
další	další	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnPc1d1
významné	významný	k2eAgFnPc1d1
americké	americký	k2eAgFnPc1d1
firmy	firma	k1gFnPc1
vznikly	vzniknout	k5eAaPmAgFnP
v	v	k7c6
oblastech	oblast	k1gFnPc6
telekomunikací	telekomunikace	k1gFnPc2
(	(	kIx(
<g/>
AT	AT	kA
<g/>
&	&	k?
<g/>
T	T	kA
<g/>
,	,	kIx,
Verizon	Verizon	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
finančnictví	finančnictví	k1gNnSc1
(	(	kIx(
<g/>
Berkshire	Berkshir	k1gMnSc5
Hathaway	Hathawa	k2eAgInPc1d1
<g/>
,	,	kIx,
JPMorgan	JPMorgan	k1gInSc1
Chase	chasa	k1gFnSc3
<g/>
,	,	kIx,
Visa	viso	k1gNnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
chemickém	chemický	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
(	(	kIx(
<g/>
Johnson	Johnson	k1gMnSc1
&	&	k?
Johnson	Johnson	k1gMnSc1
<g/>
,	,	kIx,
Procter	Procter	k1gMnSc1
&	&	k?
Gamble	Gamble	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
<g/>
,	,	kIx,
petrochemii	petrochemie	k1gFnSc4
(	(	kIx(
<g/>
ExxonMobil	ExxonMobil	k1gFnSc4
<g/>
)	)	kIx)
či	či	k8xC
maloobchodu	maloobchod	k1gInSc2
(	(	kIx(
<g/>
Walmart	Walmart	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
134	#num#	k4
<g/>
]	]	kIx)
Také	také	k9
potravinářští	potravinářský	k2eAgMnPc1d1
giganti	gigant	k1gMnPc1
Coca-Cola	coca-cola	k1gFnSc1
a	a	k8xC
McDonald	McDonald	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
nebo	nebo	k8xC
mediální	mediální	k2eAgInSc4d1
a	a	k8xC
zábavní	zábavní	k2eAgInSc4d1
konglomerát	konglomerát	k1gInSc4
Disney	Disnea	k1gFnSc2
mají	mít	k5eAaImIp3nP
vybudovány	vybudován	k2eAgFnPc1d1
značky	značka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
patří	patřit	k5eAaImIp3nP
k	k	k7c3
nejcennějším	cenný	k2eAgInPc3d3
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
135	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
dějinách	dějiny	k1gFnPc6
amerického	americký	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
sehrály	sehrát	k5eAaPmAgFnP
velkou	velký	k2eAgFnSc4d1
roli	role	k1gFnSc4
i	i	k8xC
firmy	firma	k1gFnPc4
jako	jako	k8xS,k8xC
automobilka	automobilka	k1gFnSc1
Ford	ford	k1gInSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
výrobce	výrobce	k1gMnSc1
letadel	letadlo	k1gNnPc2
Boeing	boeing	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c7
problémy	problém	k1gInPc7
americké	americký	k2eAgFnSc2d1
ekonomiky	ekonomika	k1gFnSc2
patří	patřit	k5eAaImIp3nP
státní	státní	k2eAgInSc4d1
a	a	k8xC
veřejný	veřejný	k2eAgInSc4d1
dluh	dluh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
měly	mít	k5eAaImAgInP
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
státní	státní	k2eAgInSc4d1
dluh	dluh	k1gInSc4
zhruba	zhruba	k6eAd1
18	#num#	k4
151	#num#	k4
miliard	miliarda	k4xCgFnPc2
$	$	kIx~
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
asi	asi	k9
102	#num#	k4
%	%	kIx~
jeho	jeho	k3xOp3gNnSc6
HDP	HDP	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
136	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
největší	veliký	k2eAgInSc1d3
zahraniční	zahraniční	k2eAgInSc1d1
dluh	dluh	k1gInSc1
na	na	k7c6
světě	svět	k1gInSc6
a	a	k8xC
11	#num#	k4
<g/>
.	.	kIx.
největší	veliký	k2eAgInSc4d3
vládní	vládní	k2eAgInSc4d1
dluh	dluh	k1gInSc4
na	na	k7c4
%	%	kIx~
HDP	HDP	kA
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
další	další	k2eAgInPc4d1
problémy	problém	k1gInPc4
patří	patřit	k5eAaImIp3nP
korporátní	korporátní	k2eAgInPc1d1
dluhopisy	dluhopis	k1gInPc1
<g/>
,	,	kIx,
hypoteční	hypoteční	k2eAgFnSc1d1
krize	krize	k1gFnSc1
vyvolaná	vyvolaný	k2eAgFnSc1d1
pádem	pád	k1gInSc7
cen	cena	k1gFnPc2
nemovitostí	nemovitost	k1gFnPc2
<g/>
,	,	kIx,
nízké	nízký	k2eAgInPc4d1
úroky	úrok	k1gInPc4
<g/>
,	,	kIx,
výrazně	výrazně	k6eAd1
negativní	negativní	k2eAgNnSc4d1
saldo	saldo	k1gNnSc4
zahraničního	zahraniční	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
<g/>
,	,	kIx,
vzrůstající	vzrůstající	k2eAgFnSc2d1
inflace	inflace	k1gFnSc2
a	a	k8xC
uspokojení	uspokojení	k1gNnSc2
nároků	nárok	k1gInPc2
tzv.	tzv.	kA
baby	baby	k1gNnSc1
boom	boom	k1gInSc1
generace	generace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
v	v	k7c6
současnosti	současnost	k1gFnSc6
začíná	začínat	k5eAaImIp3nS
čerpat	čerpat	k5eAaImF
sociální	sociální	k2eAgInPc4d1
důchody	důchod	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
největší	veliký	k2eAgNnSc4d3
obchodní	obchodní	k2eAgNnSc4d1
odvětví	odvětví	k1gNnSc4
patří	patřit	k5eAaImIp3nS
maloobchodní	maloobchodní	k2eAgInSc1d1
a	a	k8xC
velkoobchodní	velkoobchodní	k2eAgInSc1d1
prodej	prodej	k1gInSc1
<g/>
,	,	kIx,
sektor	sektor	k1gInSc1
(	(	kIx(
<g/>
finančních	finanční	k2eAgFnPc2d1
<g/>
,	,	kIx,
obchodních	obchodní	k2eAgFnPc2d1
<g/>
,	,	kIx,
zdravotnických	zdravotnický	k2eAgFnPc2d1
<g/>
,	,	kIx,
sociálních	sociální	k2eAgFnPc2d1
<g/>
)	)	kIx)
služeb	služba	k1gFnPc2
<g/>
,	,	kIx,
proporcionálně	proporcionálně	k6eAd1
vysoký	vysoký	k2eAgInSc1d1
podíl	podíl	k1gInSc1
má	mít	k5eAaImIp3nS
i	i	k9
věda	věda	k1gFnSc1
a	a	k8xC
výzkum	výzkum	k1gInSc1
nebo	nebo	k8xC
třeba	třeba	k6eAd1
zábavní	zábavní	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
mají	mít	k5eAaImIp3nP
nejvyšší	vysoký	k2eAgFnSc4d3
spotřebu	spotřeba	k1gFnSc4
na	na	k7c6
světě	svět	k1gInSc6
co	co	k8xS
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
ropy	ropa	k1gFnSc2
<g/>
,	,	kIx,
zemního	zemní	k2eAgInSc2d1
plynu	plyn	k1gInSc2
<g/>
,	,	kIx,
elektřiny	elektřina	k1gFnSc2
a	a	k8xC
řady	řada	k1gFnSc2
dalších	další	k2eAgFnPc2d1
komodit	komodita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1
obchodními	obchodní	k2eAgMnPc7d1
partnery	partner	k1gMnPc7
jsou	být	k5eAaImIp3nP
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
včetně	včetně	k7c2
Spojeného	spojený	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
<g/>
,	,	kIx,
Mexiko	Mexiko	k1gNnSc1
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
a	a	k8xC
Čína	Čína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahraniční	zahraniční	k2eAgInSc1d1
obchod	obchod	k1gInSc1
USA	USA	kA
je	být	k5eAaImIp3nS
již	již	k6eAd1
desítky	desítka	k1gFnPc4
let	léto	k1gNnPc2
ve	v	k7c6
schodku	schodek	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
od	od	k7c2
roku	rok	k1gInSc2
1998	#num#	k4
začal	začít	k5eAaPmAgMnS
prohlubovat	prohlubovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
přesahuje	přesahovat	k5eAaImIp3nS
negativní	negativní	k2eAgFnSc1d1
bilance	bilance	k1gFnSc1
(	(	kIx(
<g/>
s	s	k7c7
jedinou	jediný	k2eAgFnSc7d1
výjimkou	výjimka	k1gFnSc7
<g/>
)	)	kIx)
hranici	hranice	k1gFnSc6
400	#num#	k4
miliard	miliarda	k4xCgFnPc2
$	$	kIx~
ročně	ročně	k6eAd1
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
informační	informační	k2eAgFnSc1d1
grafika	grafika	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkový	celkový	k2eAgInSc1d1
zahraniční	zahraniční	k2eAgInSc1d1
dluh	dluh	k1gInSc1
USA	USA	kA
dosáhl	dosáhnout	k5eAaPmAgInS
koncem	koncem	k7c2
října	říjen	k1gInSc2
2018	#num#	k4
částku	částka	k1gFnSc4
6	#num#	k4
200	#num#	k4
miliard	miliarda	k4xCgFnPc2
$	$	kIx~
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
největšími	veliký	k2eAgMnPc7d3
zahraničními	zahraniční	k2eAgMnPc7d1
věřiteli	věřitel	k1gMnPc7
jsou	být	k5eAaImIp3nP
Čína	Čína	k1gFnSc1
(	(	kIx(
<g/>
1139	#num#	k4
miliard	miliarda	k4xCgFnPc2
$	$	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Japonsko	Japonsko	k1gNnSc1
(	(	kIx(
<g/>
1019	#num#	k4
miliard	miliarda	k4xCgFnPc2
$	$	kIx~
<g/>
)	)	kIx)
a	a	k8xC
Brazílie	Brazílie	k1gFnSc1
(	(	kIx(
<g/>
314	#num#	k4
miliard	miliarda	k4xCgFnPc2
$	$	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
137	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sociální	sociální	k2eAgFnPc1d1
reformy	reforma	k1gFnPc1
</s>
<s>
Demonstrace	demonstrace	k1gFnSc1
imigrantů	imigrant	k1gMnPc2
v	v	k7c4
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
na	na	k7c4
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2006	#num#	k4
</s>
<s>
Během	během	k7c2
prezidentských	prezidentský	k2eAgFnPc2d1
období	období	k1gNnPc4
Baracka	Baracko	k1gNnSc2
Obamy	Obama	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
posunu	posun	k1gInSc3
v	v	k7c6
schvalování	schvalování	k1gNnSc6
dvou	dva	k4xCgFnPc2
zásadních	zásadní	k2eAgFnPc2d1
reforem	reforma	k1gFnPc2
–	–	k?
v	v	k7c6
oblasti	oblast	k1gFnSc6
zdravotní	zdravotní	k2eAgFnSc2d1
péče	péče	k1gFnSc2
a	a	k8xC
v	v	k7c6
oblasti	oblast	k1gFnSc6
přistěhovalectví	přistěhovalectví	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Reforma	reforma	k1gFnSc1
zdravotního	zdravotní	k2eAgNnSc2d1
pojištění	pojištění	k1gNnSc2
</s>
<s>
Zdravotní	zdravotní	k2eAgNnSc1d1
pojištění	pojištění	k1gNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
USA	USA	kA
záležitostí	záležitost	k1gFnSc7
každého	každý	k3xTgMnSc2
jedince	jedinec	k1gMnSc2
<g/>
,	,	kIx,
zatím	zatím	k6eAd1
není	být	k5eNaImIp3nS
povinné	povinný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
jsou	být	k5eAaImIp3nP
pojišťovny	pojišťovna	k1gFnPc1
soukromými	soukromý	k2eAgFnPc7d1
firmami	firma	k1gFnPc7
<g/>
,	,	kIx,
výše	vysoce	k6eAd2
pojištění	pojištění	k1gNnSc2
se	se	k3xPyFc4
neodvíjí	odvíjet	k5eNaImIp3nS
od	od	k7c2
výše	výše	k1gFnSc2,k1gFnSc2wB
příjmu	příjem	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
rozdělena	rozdělit	k5eAaPmNgFnS
dle	dle	k7c2
věku	věk	k1gInSc2
<g/>
,	,	kIx,
prodělaných	prodělaný	k2eAgFnPc2d1
nemocí	nemoc	k1gFnPc2
atd.	atd.	kA
To	to	k9
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
mnoho	mnoho	k4c1
lidí	člověk	k1gMnPc2
z	z	k7c2
rodin	rodina	k1gFnPc2
s	s	k7c7
nízkými	nízký	k2eAgInPc7d1
příjmy	příjem	k1gInPc7
si	se	k3xPyFc3
nemůže	moct	k5eNaImIp3nS
dovolit	dovolit	k5eAaPmF
pojištění	pojištění	k1gNnSc4
platit	platit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
statistiky	statistika	k1gFnSc2
National	National	k1gMnSc2
Center	centrum	k1gNnPc2
for	forum	k1gNnPc2
Health	Healtha	k1gFnPc2
Statistics	Statisticsa	k1gFnPc2
z	z	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
45,5	45,5	k4
milionů	milion	k4xCgInPc2
(	(	kIx(
<g/>
tedy	tedy	k9
14,7	14,7	k4
%	%	kIx~
<g/>
)	)	kIx)
osob	osoba	k1gFnPc2
bylo	být	k5eAaImAgNnS
nepojištěno	pojistit	k5eNaPmNgNnS
v	v	k7c6
době	doba	k1gFnSc6
rozhovoru	rozhovor	k1gInSc2
<g/>
,	,	kIx,
pročež	pročež	k6eAd1
57,7	57,7	k4
milionů	milion	k4xCgInPc2
(	(	kIx(
<g/>
18.6	18.6	k4
%	%	kIx~
<g/>
)	)	kIx)
nebylo	být	k5eNaImAgNnS
pojištěno	pojistit	k5eAaPmNgNnS
alespoň	alespoň	k9
část	část	k1gFnSc1
roku	rok	k1gInSc2
předcházejícímu	předcházející	k2eAgNnSc3d1
interview	interview	k1gNnSc3
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
34,1	34,1	k4
milionů	milion	k4xCgInPc2
nebylo	být	k5eNaImAgNnS
pojištěno	pojistit	k5eAaPmNgNnS
více	hodně	k6eAd2
než	než	k8xS
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
celkového	celkový	k2eAgInSc2d1
počtu	počet	k1gInSc2
bylo	být	k5eAaImAgNnS
nepojištěno	pojistit	k5eNaPmNgNnS
4	#num#	k4
900	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
6.6	6.6	k4
%	%	kIx~
<g/>
)	)	kIx)
dětí	dítě	k1gFnPc2
do	do	k7c2
18	#num#	k4
<g/>
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
statistiky	statistika	k1gFnSc2
také	také	k9
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
počet	počet	k1gInSc1
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
si	se	k3xPyFc3
neplatí	platit	k5eNaImIp3nP
zdravotní	zdravotní	k2eAgNnSc4d1
pojištění	pojištění	k1gNnSc4
<g/>
,	,	kIx,
mírně	mírně	k6eAd1
klesá	klesat	k5eAaImIp3nS
a	a	k8xC
přibližně	přibližně	k6eAd1
10	#num#	k4
%	%	kIx~
neplatičů	neplatič	k1gMnPc2
nepochází	pocházet	k5eNaImIp3nS
z	z	k7c2
rodin	rodina	k1gFnPc2
s	s	k7c7
nízkými	nízký	k2eAgInPc7d1
příjmy	příjem	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reforma	reforma	k1gFnSc1
zvaná	zvaný	k2eAgFnSc1d1
„	„	k?
<g/>
Obamacare	Obamacar	k1gMnSc5
<g/>
“	“	k?
(	(	kIx(
<g/>
Patient	Patient	k1gInSc1
Protection	Protection	k1gInSc1
and	and	k?
Affordable	Affordable	k1gMnSc5
Care	car	k1gMnSc5
Act	Act	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
má	mít	k5eAaImIp3nS
začít	začít	k5eAaPmF
platit	platit	k5eAaImF
od	od	k7c2
ledna	leden	k1gInSc2
2014	#num#	k4
<g/>
,	,	kIx,
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
pro	pro	k7c4
všechny	všechen	k3xTgMnPc4
občany	občan	k1gMnPc4
USA	USA	kA
zajistit	zajistit	k5eAaPmF
podstatně	podstatně	k6eAd1
větší	veliký	k2eAgFnSc4d2
dostupnost	dostupnost	k1gFnSc4
zdravotního	zdravotní	k2eAgNnSc2d1
pojištění	pojištění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
například	například	k6eAd1
zakazuje	zakazovat	k5eAaImIp3nS
firmám	firma	k1gFnPc3
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
zajišťují	zajišťovat	k5eAaImIp3nP
zdravotní	zdravotní	k2eAgNnSc4d1
pojištění	pojištění	k1gNnSc4
<g/>
,	,	kIx,
jakoukoliv	jakýkoliv	k3yIgFnSc4
diskriminaci	diskriminace	k1gFnSc4
–	–	k?
tedy	tedy	k8xC
posuzovat	posuzovat	k5eAaImF
výši	výše	k1gFnSc4
zdravotní	zdravotní	k2eAgFnSc2d1
pojistky	pojistka	k1gFnSc2
klienta	klient	k1gMnSc2
podle	podle	k7c2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
předchozího	předchozí	k2eAgInSc2d1
zdravotního	zdravotní	k2eAgInSc2d1
stavu	stav	k1gInSc2
či	či	k8xC
na	na	k7c6
základě	základ	k1gInSc6
pohlaví	pohlaví	k1gNnSc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
bylo	být	k5eAaImAgNnS
doposud	doposud	k6eAd1
běžné	běžný	k2eAgNnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
bylo	být	k5eAaImAgNnS
zdravotní	zdravotní	k2eAgNnSc4d1
pojištění	pojištění	k1gNnSc4
v	v	k7c6
podstatě	podstata	k1gFnSc6
nedostupné	dostupný	k2eNgFnPc1d1
pro	pro	k7c4
vážně	vážně	k6eAd1
nemocné	mocný	k2eNgMnPc4d1,k2eAgMnPc4d1
a	a	k8xC
sociálně	sociálně	k6eAd1
slabé	slabý	k2eAgMnPc4d1
občany	občan	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s>
Imigrační	imigrační	k2eAgFnSc1d1
reforma	reforma	k1gFnSc1
</s>
<s>
V	v	k7c6
USA	USA	kA
žije	žít	k5eAaImIp3nS
asi	asi	k9
11	#num#	k4
milionů	milion	k4xCgInPc2
nelegálních	legální	k2eNgMnPc2d1
přistěhovalců	přistěhovalec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Názory	názor	k1gInPc4
na	na	k7c4
jejich	jejich	k3xOp3gInSc4
dopad	dopad	k1gInSc4
na	na	k7c4
ekonomiku	ekonomika	k1gFnSc4
se	se	k3xPyFc4
různí	různit	k5eAaImIp3nP
<g/>
,	,	kIx,
od	od	k7c2
pozitivního	pozitivní	k2eAgInSc2d1
přínosu	přínos	k1gInSc2
vzhledem	vzhledem	k7c3
k	k	k7c3
poměru	poměr	k1gInSc3
zjednodušeně	zjednodušeně	k6eAd1
vyjádřeno	vyjádřit	k5eAaPmNgNnS
práce	práce	k1gFnSc1
<g/>
/	/	kIx~
<g/>
mzda	mzda	k1gFnSc1
<g/>
/	/	kIx~
<g/>
útrata	útrata	k1gFnSc1
–	–	k?
nekvalifikovaná	kvalifikovaný	k2eNgFnSc1d1
práce	práce	k1gFnSc1
za	za	k7c4
co	co	k9
nejnižší	nízký	k2eAgFnSc4d3
mzdu	mzda	k1gFnSc4
(	(	kIx(
<g/>
neakceptovatelnými	akceptovatelný	k2eNgInPc7d1
pro	pro	k7c4
Američany	Američan	k1gMnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
utracena	utracen	k2eAgFnSc1d1
<g/>
,	,	kIx,
k	k	k7c3
negativní	negativní	k2eAgFnSc3d1
dopadům	dopad	k1gInPc3
souvisejícím	související	k2eAgInPc3d1
s	s	k7c7
šedou	šedý	k2eAgFnSc7d1
ekonomikou	ekonomika	k1gFnSc7
<g/>
,	,	kIx,
vzděláváním	vzdělávání	k1gNnSc7
<g/>
,	,	kIx,
zdravotní	zdravotní	k2eAgFnSc7d1
péčí	péče	k1gFnSc7
<g/>
,	,	kIx,
občanskými	občanský	k2eAgNnPc7d1
právy	právo	k1gNnPc7
<g/>
,	,	kIx,
atd.	atd.	kA
Dne	den	k1gInSc2
27	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2013	#num#	k4
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
Senátu	senát	k1gInSc6
USA	USA	kA
k	k	k7c3
většinovému	většinový	k2eAgNnSc3d1
schválení	schválení	k1gNnSc3
„	„	k?
<g/>
Border	Border	k1gInSc1
Security	Securita	k1gFnSc2
<g/>
,	,	kIx,
Economic	Economic	k1gMnSc1
Opportunity	Opportunita	k1gFnSc2
<g/>
</s>
<s>
,	,	kIx,
and	and	k?
Immigration	Immigration	k1gInSc1
Modernization	Modernization	k1gInSc1
Act	Act	k1gFnSc1
of	of	k?
2013	#num#	k4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
reforma	reforma	k1gFnSc1
by	by	kYmCp3nS
umožnila	umožnit	k5eAaPmAgFnS
zlegalizování	zlegalizování	k1gNnSc3
pobytu	pobyt	k1gInSc2
imigrantů	imigrant	k1gMnPc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
získání	získání	k1gNnSc1
občanství	občanství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
by	by	kYmCp3nS
se	se	k3xPyFc4
změnil	změnit	k5eAaPmAgInS
systém	systém	k1gInSc1
vydávání	vydávání	k1gNnSc2
legálních	legální	k2eAgNnPc2d1
víz	vízo	k1gNnPc2
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
ekonomických	ekonomický	k2eAgFnPc6d1
potřebách	potřeba	k1gFnPc6
státu	stát	k1gInSc2
<g/>
,	,	kIx,
zvýšily	zvýšit	k5eAaPmAgFnP
by	by	kYmCp3nP
se	se	k3xPyFc4
kontroly	kontrola	k1gFnPc1
na	na	k7c6
hranicích	hranice	k1gFnPc6
s	s	k7c7
Mexikem	Mexiko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
reforma	reforma	k1gFnSc1
musí	muset	k5eAaImIp3nS
ještě	ještě	k6eAd1
projít	projít	k5eAaPmF
schválením	schválení	k1gNnSc7
v	v	k7c6
Kongresu	kongres	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Prezident	prezident	k1gMnSc1
Obama	Obama	k?
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
snažil	snažit	k5eAaImAgMnS
imigrační	imigrační	k2eAgFnSc4d1
reformu	reforma	k1gFnSc4
prosadit	prosadit	k5eAaPmF
pomocí	pomocí	k7c2
exekutivních	exekutivní	k2eAgNnPc2d1
nařízení	nařízení	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yRgNnPc1,k3yIgNnPc1
obcházejí	obcházet	k5eAaImIp3nP
Kongres	kongres	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
138	#num#	k4
<g/>
]	]	kIx)
čímž	což	k3yQnSc7,k3yRnSc7
podle	podle	k7c2
kritiků	kritik	k1gMnPc2
porušil	porušit	k5eAaPmAgMnS
americkou	americký	k2eAgFnSc4d1
Ústavu	ústava	k1gFnSc4
a	a	k8xC
proto	proto	k8xC
celkem	celkem	k6eAd1
26	#num#	k4
amerických	americký	k2eAgInPc2d1
států	stát	k1gInPc2
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Texasem	Texas	k1gInSc7
podalo	podat	k5eAaPmAgNnS
na	na	k7c4
Obamovu	Obamův	k2eAgFnSc4d1
administrativu	administrativa	k1gFnSc4
žalobu	žaloba	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
139	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Turismus	turismus	k1gInSc1
</s>
<s>
Miami	Miami	k1gNnSc1
Beach	Beacha	k1gFnPc2
</s>
<s>
Golden	Goldna	k1gFnPc2
Gate	Gate	k1gFnSc1
Bridge	Bridge	k1gFnSc1
</s>
<s>
Mezi	mezi	k7c4
nejvýznamnější	významný	k2eAgNnPc4d3
centra	centrum	k1gNnPc4
cestovního	cestovní	k2eAgInSc2d1
ruchu	ruch	k1gInSc2
patří	patřit	k5eAaImIp3nP
velkoměsta	velkoměsto	k1gNnPc4
(	(	kIx(
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
Los	los	k1gInSc1
Angeles	Angeles	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Množství	množství	k1gNnSc1
turistů	turist	k1gMnPc2
navštěvuje	navštěvovat	k5eAaImIp3nS
národní	národní	k2eAgInPc4d1
parky	park	k1gInPc4
(	(	kIx(
<g/>
Yellowstonský	Yellowstonský	k2eAgInSc4d1
<g/>
,	,	kIx,
Yosemitský	Yosemitský	k2eAgInSc4d1
<g/>
,	,	kIx,
Sequoia	Sequoia	k1gFnSc1
<g/>
,	,	kIx,
Grand	grand	k1gMnSc1
Canyon	Canyon	k1gMnSc1
<g/>
,	,	kIx,
Everglades	Everglades	k1gMnSc1
<g/>
,	,	kIx,
Redwood	Redwood	k1gInSc1
<g/>
,	,	kIx,
Mamutí	mamutí	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
<g/>
,	,	kIx,
Olympic	Olympic	k1gMnSc1
<g/>
,	,	kIx,
Great	Great	k2eAgInSc1d1
Smoky	smok	k1gMnPc7
Mountains	Mountainsa	k1gFnPc2
<g/>
,	,	kIx,
Glacier	Glacira	k1gFnPc2
<g/>
,	,	kIx,
Carlsbadské	Carlsbadský	k2eAgFnPc1d1
jeskyně	jeskyně	k1gFnPc1
<g/>
,	,	kIx,
Skalnaté	skalnatý	k2eAgFnPc1d1
hory	hora	k1gFnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejnavštěvovanější	navštěvovaný	k2eAgFnSc7d3
přírodní	přírodní	k2eAgFnSc7d1
památkou	památka	k1gFnSc7
jsou	být	k5eAaImIp3nP
ovšem	ovšem	k9
Niagarské	niagarský	k2eAgInPc1d1
vodopády	vodopád	k1gInPc1
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
leží	ležet	k5eAaImIp3nP
na	na	k7c6
hranicích	hranice	k1gFnPc6
s	s	k7c7
Kanadou	Kanada	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
140	#num#	k4
<g/>
]	]	kIx)
Poblíž	poblíž	k7c2
amerického	americký	k2eAgNnSc2d1
města	město	k1gNnSc2
Keyston	Keyston	k1gInSc1
v	v	k7c6
Jižní	jižní	k2eAgFnSc6d1
Dakotě	Dakota	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
národní	národní	k2eAgInSc1d1
památník	památník	k1gInSc1
Mount	Mounta	k1gFnPc2
Rushmore	Rushmor	k1gInSc5
<g/>
,	,	kIx,
sousoší	sousoší	k1gNnSc4
čtyř	čtyři	k4xCgMnPc2
amerických	americký	k2eAgMnPc2d1
prezidentů	prezident	k1gMnPc2
vytesané	vytesaný	k2eAgFnSc2d1
do	do	k7c2
skály	skála	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každoročně	každoročně	k6eAd1
ho	on	k3xPp3gMnSc4
navštíví	navštívit	k5eAaPmIp3nS
okolo	okolo	k7c2
dvou	dva	k4xCgInPc2
milionů	milion	k4xCgInPc2
turistů	turist	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
141	#num#	k4
<g/>
]	]	kIx)
Mnohé	mnohé	k1gNnSc1
atrakce	atrakce	k1gFnSc2
nabízí	nabízet	k5eAaImIp3nS
proslulý	proslulý	k2eAgInSc1d1
zábavní	zábavní	k2eAgInSc1d1
park	park	k1gInSc1
Disneyland	Disneyland	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
turistů	turist	k1gMnPc2
chce	chtít	k5eAaImIp3nS
vidět	vidět	k5eAaImF
i	i	k9
centrum	centrum	k1gNnSc4
filmového	filmový	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
Hollywood	Hollywood	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nejcennějším	cenný	k2eAgFnPc3d3
památkám	památka	k1gFnPc3
z	z	k7c2
předkolumbovské	předkolumbovský	k2eAgFnSc2d1
historie	historie	k1gFnSc2
patří	patřit	k5eAaImIp3nS
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Mesa	Mes	k1gInSc2
Verde	Verd	k1gInSc5
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
zapsán	zapsat	k5eAaPmNgInS
i	i	k9
na	na	k7c4
seznam	seznam	k1gInSc4
světového	světový	k2eAgNnSc2d1
kulturního	kulturní	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
nepřírodních	přírodní	k2eNgFnPc2d1
památek	památka	k1gFnPc2
se	se	k3xPyFc4
na	na	k7c4
seznam	seznam	k1gInSc4
dostala	dostat	k5eAaPmAgFnS
i	i	k9
budova	budova	k1gFnSc1
Independence	Independence	k1gFnSc2
Hall	Halla	k1gFnPc2
ve	v	k7c6
Filadelfii	Filadelfie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
přijata	přijat	k2eAgFnSc1d1
Deklarace	deklarace	k1gFnSc1
nezávislosti	nezávislost	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
Socha	socha	k1gFnSc1
Svobody	svoboda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
nejen	nejen	k6eAd1
symbolem	symbol	k1gInSc7
New	New	k1gFnSc2
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
celých	celá	k1gFnPc2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dalším	další	k2eAgMnPc3d1
známým	známý	k1gMnPc3
<g/>
,	,	kIx,
ba	ba	k9
ikonickým	ikonický	k2eAgMnSc7d1
<g/>
,	,	kIx,
stavbám	stavba	k1gFnPc3
patří	patřit	k5eAaImIp3nP
Gateway	Gatewaa	k1gFnPc1
Arch	archa	k1gFnPc2
v	v	k7c6
St.	st.	kA
Louis	louis	k1gInSc2
<g/>
,	,	kIx,
Golden	Goldna	k1gFnPc2
Gate	Gat	k1gInSc2
Bridge	Bridg	k1gInSc2
v	v	k7c6
San	San	k1gFnSc6
Franciscu	Franciscus	k1gInSc2
<g/>
,	,	kIx,
Empire	empir	k1gInSc5
State	status	k1gInSc5
Building	Building	k1gInSc1
a	a	k8xC
nádraží	nádraží	k1gNnSc1
Grand	grand	k1gMnSc1
Central	Central	k1gMnSc1
Terminal	Terminal	k1gMnSc1
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
Lincolnův	Lincolnův	k2eAgInSc1d1
památník	památník	k1gInSc1
<g/>
,	,	kIx,
Bílý	bílý	k2eAgInSc1d1
dům	dům	k1gInSc1
a	a	k8xC
Kapitol	Kapitol	k1gInSc1
ve	v	k7c6
Washingtonu	Washington	k1gInSc6
<g/>
,	,	kIx,
Millennium	millennium	k1gNnSc1
Park	park	k1gInSc1
v	v	k7c6
Chicagu	Chicago	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metropolitní	metropolitní	k2eAgInSc4d1
muzeum	muzeum	k1gNnSc1
umění	umění	k1gNnSc1
či	či	k8xC
Carnegie	Carnegie	k1gFnSc1
Hall	Halla	k1gFnPc2
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejnavštěvovanějším	navštěvovaný	k2eAgFnPc3d3
kulturním	kulturní	k2eAgFnPc3d1
institucím	instituce	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řadu	řad	k1gInSc2
turistů	turist	k1gMnPc2
láká	lákat	k5eAaImIp3nS
i	i	k9
město	město	k1gNnSc4
hazardu	hazard	k1gInSc2
Las	laso	k1gNnPc2
Vegas	Vegas	k1gInSc1
(	(	kIx(
<g/>
zejm.	zejm.	k?
slavná	slavný	k2eAgFnSc1d1
Fontána	fontána	k1gFnSc1
Bellagio	Bellagio	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přímořská	přímořský	k2eAgNnPc1d1
letoviska	letovisko	k1gNnPc1
na	na	k7c6
Floridě	Florida	k1gFnSc6
(	(	kIx(
<g/>
např.	např.	kA
Miami	Miami	k1gNnSc1
Beach	Beach	k1gInSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
(	(	kIx(
<g/>
např.	např.	kA
Santa	Santa	k1gFnSc1
Monica	Monica	k1gFnSc1
se	s	k7c7
slavným	slavný	k2eAgInSc7d1
molem	mol	k1gInSc7
<g/>
,	,	kIx,
v	v	k7c6
minulosti	minulost	k1gFnSc6
centrem	centr	k1gMnSc7
americké	americký	k2eAgFnSc2d1
taneční	taneční	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vízum	vízum	k1gNnSc1
a	a	k8xC
doklady	doklad	k1gInPc1
totožnosti	totožnost	k1gFnSc2
</s>
<s>
Pro	pro	k7c4
cestování	cestování	k1gNnSc4
do	do	k7c2
USA	USA	kA
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
si	se	k3xPyFc3
pořídit	pořídit	k5eAaPmF
víza	vízo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Občan	občan	k1gMnSc1
cizího	cizí	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
chce	chtít	k5eAaImIp3nS
vstoupit	vstoupit	k5eAaPmF
na	na	k7c6
území	území	k1gNnSc6
USA	USA	kA
musí	muset	k5eAaImIp3nS
zpravidla	zpravidla	k6eAd1
nejprve	nejprve	k6eAd1
získat	získat	k5eAaPmF
americké	americký	k2eAgNnSc4d1
vízum	vízum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Víza	vízo	k1gNnPc1
jsou	být	k5eAaImIp3nP
umístěny	umístit	k5eAaPmNgFnP
v	v	k7c6
pase	pas	k1gInSc6
žadatele	žadatel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pas	pas	k1gInSc1
je	být	k5eAaImIp3nS
cestovní	cestovní	k2eAgInSc4d1
doklad	doklad	k1gInSc4
vydaný	vydaný	k2eAgInSc4d1
krajinou	krajina	k1gFnSc7
jejímž	jejíž	k3xOyRp3gNnSc6
je	být	k5eAaImIp3nS
žadatel	žadatel	k1gMnSc1
občanem	občan	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc4
druhů	druh	k1gInPc2
víz	vízo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
<g/>
,	,	kIx,
jaký	jaký	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
typ	typ	k1gInSc4
víz	vízo	k1gNnPc2
bude	být	k5eAaImBp3nS
potřebný	potřebný	k2eAgInSc1d1
na	na	k7c4
cestu	cesta	k1gFnSc4
do	do	k7c2
USA	USA	kA
určuje	určovat	k5eAaImIp3nS
cíl	cíl	k1gInSc4
návštěvy	návštěva	k1gFnSc2
a	a	k8xC
ostatní	ostatní	k2eAgFnPc1d1
skutečnosti	skutečnost	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žadatel	žadatel	k1gMnSc1
o	o	k7c4
víza	vízo	k1gNnSc2
bude	být	k5eAaImBp3nS
muset	muset	k5eAaImF
dokázat	dokázat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
splňuje	splňovat	k5eAaImIp3nS
všechny	všechen	k3xTgInPc4
stanovené	stanovený	k2eAgInPc4d1
kritéria	kritérion	k1gNnPc4
pro	pro	k7c4
vybraný	vybraný	k2eAgInSc4d1
typ	typ	k1gInSc4
víz	vízo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgInPc1d1
a	a	k8xC
nejběžnější	běžný	k2eAgInPc4d3
úřední	úřední	k2eAgInPc4d1
doklady	doklad	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
potvrzují	potvrzovat	k5eAaImIp3nP
totožnost	totožnost	k1gFnSc4
v	v	k7c6
USA	USA	kA
jsou	být	k5eAaImIp3nP
cestovní	cestovní	k2eAgInSc4d1
pas	pas	k1gInSc4
a	a	k8xC
řidičský	řidičský	k2eAgInSc4d1
průkaz	průkaz	k1gInSc4
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
USA	USA	kA
neexistuje	existovat	k5eNaImIp3nS
občanský	občanský	k2eAgInSc4d1
průkaz	průkaz	k1gInSc4
jako	jako	k8xS,k8xC
v	v	k7c6
zemích	zem	k1gFnPc6
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
řidičského	řidičský	k2eAgInSc2d1
průkazu	průkaz	k1gInSc2
a	a	k8xC
pasu	pas	k1gInSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
vaše	váš	k3xOp2gFnSc1
totožnost	totožnost	k1gFnSc1
potvrzena	potvrdit	k5eAaPmNgFnS
i	i	k9
rodným	rodný	k2eAgInSc7d1
listem	list	k1gInSc7
<g/>
,	,	kIx,
povolením	povolení	k1gNnSc7
k	k	k7c3
pobytu	pobyt	k1gInSc3
<g/>
,	,	kIx,
vízem	vízo	k1gNnSc7
nebo	nebo	k8xC
zbrojním	zbrojní	k2eAgInSc7d1
průkazem	průkaz	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
142	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Infrastruktura	infrastruktura	k1gFnSc1
</s>
<s>
Doprava	doprava	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Doprava	doprava	k1gFnSc1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgInPc6d1
<g/>
.	.	kIx.
</s>
<s>
Dálnice	dálnice	k1gFnSc1
v	v	k7c4
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
</s>
<s>
Automobilismus	automobilismus	k1gInSc1
hraje	hrát	k5eAaImIp3nS
klíčovou	klíčový	k2eAgFnSc4d1
roli	role	k1gFnSc4
nejen	nejen	k6eAd1
v	v	k7c6
americké	americký	k2eAgFnSc6d1
osobní	osobní	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
celé	celý	k2eAgFnSc6d1
americké	americký	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
mají	mít	k5eAaImIp3nP
nejvyšší	vysoký	k2eAgFnSc4d3
míru	míra	k1gFnSc4
vlastnictví	vlastnictví	k1gNnSc2
automobilů	automobil	k1gInPc2
na	na	k7c4
obyvatele	obyvatel	k1gMnPc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
bylo	být	k5eAaImAgNnS
v	v	k7c6
Americe	Amerika	k1gFnSc6
255	#num#	k4
009	#num#	k4
283	#num#	k4
motorových	motorový	k2eAgNnPc2d1
vozidel	vozidlo	k1gNnPc2
–	–	k?
to	ten	k3xDgNnSc1
značí	značit	k5eAaImIp3nS
poměr	poměr	k1gInSc1
910	#num#	k4
vozidel	vozidlo	k1gNnPc2
na	na	k7c4
1000	#num#	k4
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
143	#num#	k4
<g/>
]	]	kIx)
Průměrný	průměrný	k2eAgMnSc1d1
dospělý	dospělý	k2eAgMnSc1d1
Američan	Američan	k1gMnSc1
stráví	strávit	k5eAaPmIp3nS
každý	každý	k3xTgInSc4
den	den	k1gInSc4
v	v	k7c6
autě	auto	k1gNnSc6
55	#num#	k4
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
144	#num#	k4
<g/>
]	]	kIx)
Centrem	centr	k1gInSc7
automobilového	automobilový	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
byl	být	k5eAaImAgInS
tradičně	tradičně	k6eAd1
Detroit	Detroit	k1gInSc1
a	a	k8xC
celý	celý	k2eAgInSc1d1
tzv.	tzv.	kA
rezavý	rezavý	k2eAgInSc1d1
pás	pás	k1gInSc1
kolem	kolem	k7c2
Velkých	velký	k2eAgNnPc2d1
jezer	jezero	k1gNnPc2
<g/>
,	,	kIx,
alespoň	alespoň	k9
do	do	k7c2
krize	krize	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tradičními	tradiční	k2eAgFnPc7d1
automobilovými	automobilový	k2eAgFnPc7d1
značkami	značka	k1gFnPc7
byli	být	k5eAaImAgMnP
Ford	ford	k1gInSc4
<g/>
,	,	kIx,
Lincoln	Lincoln	k1gMnSc1
<g/>
,	,	kIx,
Chrysler	Chrysler	k1gMnSc1
<g/>
,	,	kIx,
Chevrolet	chevrolet	k1gInSc1
<g/>
,	,	kIx,
Jeep	jeep	k1gInSc1
<g/>
,	,	kIx,
Cadillac	cadillac	k1gInSc1
<g/>
,	,	kIx,
Buick	Buick	k1gInSc1
nebo	nebo	k8xC
Dodge	Dodge	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
značky	značka	k1gFnSc2
už	už	k6eAd1
ovšem	ovšem	k9
zanikly	zaniknout	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
motorkami	motorka	k1gFnPc7
měly	mít	k5eAaImAgFnP
velkou	velký	k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
značky	značka	k1gFnSc2
Indian	Indiana	k1gFnPc2
a	a	k8xC
Harley-Davidson	Harley-Davidsona	k1gFnPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
si	se	k3xPyFc3
vytvořila	vytvořit	k5eAaPmAgFnS
specifickou	specifický	k2eAgFnSc4d1
uživatelskou	uživatelský	k2eAgFnSc4d1
kulturu	kultura	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Automobilisté	automobilista	k1gMnPc1
i	i	k8xC
motocyklisté	motocyklista	k1gMnPc1
mohou	moct	k5eAaImIp3nP
využívat	využívat	k5eAaPmF,k5eAaImF
jednu	jeden	k4xCgFnSc4
z	z	k7c2
největších	veliký	k2eAgFnPc2d3
sítí	síť	k1gFnPc2
veřejných	veřejný	k2eAgFnPc2d1
silnic	silnice	k1gFnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
v	v	k7c6
délce	délka	k1gFnSc6
6,4	6,4	k4
miliónu	milión	k4xCgInSc2
kilometrů	kilometr	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
91	#num#	k4
700	#num#	k4
kilometrů	kilometr	k1gInPc2
jsou	být	k5eAaImIp3nP
dálnice	dálnice	k1gFnPc4
<g/>
,	,	kIx,
často	často	k6eAd1
mnohaproudové	mnohaproudový	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnPc1
silnice	silnice	k1gFnPc1
mají	mít	k5eAaImIp3nP
přímo	přímo	k6eAd1
ikonický	ikonický	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
,	,	kIx,
například	například	k6eAd1
slavná	slavný	k2eAgFnSc1d1
U.	U.	kA
<g/>
S.	S.	kA
Route	Rout	k1gInSc5
66	#num#	k4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
sice	sice	k8xC
již	již	k6eAd1
v	v	k7c6
dálničním	dálniční	k2eAgInSc6d1
systému	systém	k1gInSc6
není	být	k5eNaImIp3nS
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1985	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
její	její	k3xOp3gFnPc1
části	část	k1gFnPc1
fungují	fungovat	k5eAaImIp3nP
pro	pro	k7c4
turisty	turist	k1gMnPc4
jako	jako	k8xC,k8xS
Historic	Historic	k1gMnSc1
Route	Rout	k1gInSc5
66	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
145	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Slavná	slavný	k2eAgFnSc1d1
Route	Rout	k1gInSc5
66	#num#	k4
</s>
<s>
Velkou	velký	k2eAgFnSc4d1
tradici	tradice	k1gFnSc4
má	mít	k5eAaImIp3nS
i	i	k9
americká	americký	k2eAgFnSc1d1
železnice	železnice	k1gFnSc1
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
počet	počet	k1gInSc1
přepravených	přepravený	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
je	být	k5eAaImIp3nS
oproti	oproti	k7c3
Evropě	Evropa	k1gFnSc3
nízký	nízký	k2eAgInSc1d1
(	(	kIx(
<g/>
přibližně	přibližně	k6eAd1
31	#num#	k4
miliónů	milión	k4xCgInPc2
osob	osoba	k1gFnPc2
ročně	ročně	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
146	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
se	se	k3xPyFc4
ale	ale	k9
zájem	zájem	k1gInSc1
zvyšuje	zvyšovat	k5eAaImIp3nS
–	–	k?
mezi	mezi	k7c4
roky	rok	k1gInPc4
2000	#num#	k4
a	a	k8xC
2010	#num#	k4
vzrostl	vzrůst	k5eAaPmAgInS
počet	počet	k1gInSc1
uživatelů	uživatel	k1gMnPc2
vnitrostátní	vnitrostátní	k2eAgFnSc2d1
meziměstské	meziměstská	k1gFnSc2
osobní	osobní	k2eAgFnSc2d1
železnice	železnice	k1gFnSc2
o	o	k7c4
37	#num#	k4
<g/>
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
147	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
jiných	jiný	k2eAgInPc2d1
druhů	druh	k1gInPc2
dopravy	doprava	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
železnici	železnice	k1gFnSc6
hraje	hrát	k5eAaImIp3nS
velkou	velký	k2eAgFnSc4d1
úlohu	úloha	k1gFnSc4
státní	státní	k2eAgInSc4d1
sektor	sektor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnSc7d3
společností	společnost	k1gFnSc7
je	být	k5eAaImIp3nS
státní	státní	k2eAgMnSc1d1
dopravce	dopravce	k1gMnSc1
Amtrak	Amtrak	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
vznikl	vzniknout	k5eAaPmAgInS
roku	rok	k1gInSc2
1971	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
převzal	převzít	k5eAaPmAgMnS
26	#num#	k4
menších	malý	k2eAgFnPc2d2
společností	společnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejrušnější	rušný	k2eAgFnSc7d3
železniční	železniční	k2eAgFnSc7d1
trasou	trasa	k1gFnSc7
je	být	k5eAaImIp3nS
plně	plně	k6eAd1
elektrifikovaný	elektrifikovaný	k2eAgInSc1d1
Severovýchodní	severovýchodní	k2eAgInSc1d1
koridor	koridor	k1gInSc1
<g/>
,	,	kIx,
spojující	spojující	k2eAgInSc1d1
Washington	Washington	k1gInSc1
a	a	k8xC
Boston	Boston	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
je	být	k5eAaImIp3nS
přepravováno	přepravovat	k5eAaImNgNnS
asi	asi	k9
10	#num#	k4
milionu	milion	k4xCgInSc2
cestujících	cestující	k1gMnPc2
(	(	kIx(
<g/>
tedy	tedy	k9
třetina	třetina	k1gFnSc1
výkonů	výkon	k1gInPc2
celého	celý	k2eAgInSc2d1
Amtraku	Amtrak	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
jezdí	jezdit	k5eAaImIp3nP
zde	zde	k6eAd1
i	i	k9
rychlovlak	rychlovlak	k1gInSc1
Acela	Acelo	k1gNnSc2
vyráběný	vyráběný	k2eAgInSc1d1
kanadskou	kanadský	k2eAgFnSc7d1
společností	společnost	k1gFnSc7
Bombardier	Bombardira	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Civilní	civilní	k2eAgFnSc1d1
letecká	letecký	k2eAgFnSc1d1
přeprava	přeprava	k1gFnSc1
hraje	hrát	k5eAaImIp3nS
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
velkou	velký	k2eAgFnSc4d1
roli	role	k1gFnSc4
i	i	k9
ve	v	k7c6
vnitrostátní	vnitrostátní	k2eAgFnSc6d1
dopravě	doprava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letecké	letecký	k2eAgFnPc1d1
společnosti	společnost	k1gFnPc1
jsou	být	k5eAaImIp3nP
výhradně	výhradně	k6eAd1
soukromé	soukromý	k2eAgFnPc1d1
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
letiště	letiště	k1gNnPc1
jsou	být	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
vlastněna	vlastnit	k5eAaImNgFnS
veřejnými	veřejný	k2eAgFnPc7d1
korporacemi	korporace	k1gFnPc7
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
městy	město	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tři	tři	k4xCgInPc1
největší	veliký	k2eAgFnSc3d3
letecké	letecký	k2eAgFnSc3d1
společnosti	společnost	k1gFnSc3
na	na	k7c6
světě	svět	k1gInSc6
jsou	být	k5eAaImIp3nP
americké	americký	k2eAgFnPc1d1
<g/>
,	,	kIx,
mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
dominuje	dominovat	k5eAaImIp3nS
American	American	k1gMnSc1
Airlines	Airlines	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hartsfield-Jackson	Hartsfield-Jackson	k1gMnSc1
International	International	k1gMnSc1
Airport	Airport	k1gInSc4
u	u	k7c2
Atlanty	Atlanta	k1gFnSc2
je	být	k5eAaImIp3nS
nejvytíženějším	vytížený	k2eAgNnSc7d3
letištěm	letiště	k1gNnSc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
International	International	k1gMnSc1
Airport	Airport	k1gInSc4
čtvrtým	čtvrtý	k4xOgNnSc7
a	a	k8xC
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Hare	Hare	k1gFnSc4
International	International	k1gFnPc2
Airport	Airport	k1gInSc4
u	u	k7c2
Chicaga	Chicago	k1gNnSc2
šestým	šestý	k4xOgMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
148	#num#	k4
<g/>
]	]	kIx)
Americká	americký	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Boeing	boeing	k1gInSc4
je	být	k5eAaImIp3nS
spolu	spolu	k6eAd1
s	s	k7c7
evropským	evropský	k2eAgInSc7d1
Airbusem	airbus	k1gInSc7
největším	veliký	k2eAgInSc7d3
výrobcem	výrobce	k1gMnSc7
letadel	letadlo	k1gNnPc2
na	na	k7c6
světě	svět	k1gInSc6
a	a	k8xC
také	také	k6eAd1
největším	veliký	k2eAgMnSc7d3
americkým	americký	k2eAgMnSc7d1
exportérem	exportér	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
149	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kultura	kultura	k1gFnSc1
</s>
<s>
Sport	sport	k1gInSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Sport	sport	k1gInSc1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgInPc6d1
<g/>
.	.	kIx.
</s>
<s>
Michael	Michael	k1gMnSc1
Jordan	Jordan	k1gMnSc1
</s>
<s>
National	Nationat	k5eAaPmAgMnS,k5eAaImAgMnS
Football	Football	k1gMnSc1
League	Leagu	k1gFnSc2
(	(	kIx(
<g/>
NFL	NFL	kA
<g/>
)	)	kIx)
</s>
<s>
Nejoblíbenější	oblíbený	k2eAgInPc1d3
sporty	sport	k1gInPc1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
jsou	být	k5eAaImIp3nP
(	(	kIx(
<g/>
v	v	k7c6
tomto	tento	k3xDgInSc6
pořadí	pořadí	k1gNnSc6
<g/>
)	)	kIx)
<g/>
:	:	kIx,
americký	americký	k2eAgInSc4d1
fotbal	fotbal	k1gInSc4
<g/>
,	,	kIx,
baseball	baseball	k1gInSc4
<g/>
,	,	kIx,
basketbal	basketbal	k1gInSc4
<g/>
,	,	kIx,
lední	lední	k2eAgInSc4d1
hokej	hokej	k1gInSc4
<g/>
,	,	kIx,
fotbal	fotbal	k1gInSc4
<g/>
,	,	kIx,
tenis	tenis	k1gInSc4
<g/>
,	,	kIx,
golf	golf	k1gInSc4
a	a	k8xC
wrestling	wrestling	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
150	#num#	k4
<g/>
]	]	kIx)
Čtyři	čtyři	k4xCgFnPc1
hlavní	hlavní	k2eAgFnPc1d1
profesionální	profesionální	k2eAgFnPc1d1
sportovní	sportovní	k2eAgFnPc1d1
ligy	liga	k1gFnPc1
<g />
.	.	kIx.
</s>
<s hack="1">
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
jsou	být	k5eAaImIp3nP
Major	major	k1gMnSc1
League	Leagu	k1gFnSc2
Baseball	baseball	k1gInSc1
(	(	kIx(
<g/>
MLB	MLB	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
National	National	k1gFnSc1
Basketball	Basketball	k1gMnSc1
Association	Association	k1gInSc1
(	(	kIx(
<g/>
NBA	NBA	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
National	National	k1gFnSc1
Football	Football	k1gMnSc1
League	League	k1gInSc1
(	(	kIx(
<g/>
NFL	NFL	kA
<g/>
)	)	kIx)
a	a	k8xC
National	National	k1gMnSc2
Hockey	Hockea	k1gMnSc2
League	Leagu	k1gMnSc2
(	(	kIx(
<g/>
NHL	NHL	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
všechny	všechen	k3xTgFnPc1
čtyři	čtyři	k4xCgFnPc1
patří	patřit	k5eAaImIp3nP
mezi	mezi	k7c4
nejlukrativnější	lukrativní	k2eAgFnPc4d3
sportovní	sportovní	k2eAgFnPc4d1
ligy	liga	k1gFnPc4
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Profesionální	profesionální	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
Major	major	k1gMnSc1
League	Leagu	k1gInSc2
Soccer	Soccer	k1gMnSc1
(	(	kIx(
<g/>
MLS	mls	k1gInSc1
<g/>
)	)	kIx)
zatím	zatím	k6eAd1
nedosahuje	dosahovat	k5eNaImIp3nS
úrovně	úroveň	k1gFnPc4
popularity	popularita	k1gFnSc2
čtyř	čtyři	k4xCgFnPc2
nejvyšších	vysoký	k2eAgFnPc2d3
soutěží	soutěž	k1gFnPc2
nebo	nebo	k8xC
svých	svůj	k3xOyFgMnPc2
evropských	evropský	k2eAgMnPc2d1
a	a	k8xC
jihoamerických	jihoamerický	k2eAgMnPc2d1
protějšků	protějšek	k1gInPc2
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
její	její	k3xOp3gFnSc1
průměrná	průměrný	k2eAgFnSc1d1
návštěvnost	návštěvnost	k1gFnSc1
roste	růst	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Americký	americký	k2eAgInSc1d1
původ	původ	k1gInSc1
má	mít	k5eAaImIp3nS
basketbal	basketbal	k1gInSc4
<g/>
,	,	kIx,
volejbal	volejbal	k1gInSc4
<g/>
,	,	kIx,
skateboarding	skateboarding	k1gInSc4
a	a	k8xC
snowboarding	snowboarding	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lakros	lakros	k1gInSc1
vymysleli	vymyslet	k5eAaPmAgMnP
ve	v	k7c4
12	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
severoameričtí	severoamerický	k2eAgMnPc1d1
indiáni	indián	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
americkým	americký	k2eAgFnPc3d1
sportovním	sportovní	k2eAgFnPc3d1
legendám	legenda	k1gFnPc3
patří	patřit	k5eAaImIp3nP
boxeři	boxer	k1gMnPc1
Muhammad	Muhammad	k1gInSc4
Ali	Ali	k1gMnSc1
<g/>
,	,	kIx,
Joe	Joe	k1gMnSc1
Frazier	Frazier	k1gMnSc1
a	a	k8xC
Mike	Mike	k1gFnSc1
Tyson	Tysona	k1gFnPc2
<g/>
,	,	kIx,
basketbalisté	basketbalista	k1gMnPc1
Michael	Michael	k1gMnSc1
Jordan	Jordan	k1gMnSc1
<g/>
,	,	kIx,
Kobe	Kobe	k1gFnSc1
Bryant	Bryant	k1gMnSc1
<g/>
,	,	kIx,
LeBron	LeBron	k1gMnSc1
James	James	k1gMnSc1
<g/>
,	,	kIx,
Magic	Magic	k1gMnSc1
Johnson	Johnson	k1gMnSc1
<g/>
,	,	kIx,
Shaquille	Shaquille	k1gFnSc1
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Neal	Neal	k1gInSc4
<g/>
,	,	kIx,
Kareem	Kareum	k1gNnSc7
Abdul-Jabbar	Abdul-Jabbara	k1gFnPc2
<g/>
,	,	kIx,
Wilt	Wilt	k1gMnSc1
Chamberlain	Chamberlain	k1gMnSc1
<g/>
,	,	kIx,
Karl	Karl	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Malone	Malon	k1gMnSc5
<g/>
,	,	kIx,
Tim	Tim	k?
Duncan	Duncan	k1gInSc1
a	a	k8xC
Larry	Larra	k1gFnPc1
Bird	Birdo	k1gNnPc2
<g/>
,	,	kIx,
baseballisté	baseballista	k1gMnPc1
Babe	babit	k5eAaImSgInS
Ruth	Ruth	k1gFnSc1
<g/>
,	,	kIx,
Jackie	Jackie	k1gFnSc1
Robinson	Robinson	k1gMnSc1
<g/>
,	,	kIx,
Joe	Joe	k1gMnSc1
DiMaggio	DiMaggio	k1gMnSc1
<g/>
,	,	kIx,
Barry	Barr	k1gInPc1
Bonds	Bonds	k1gInSc1
a	a	k8xC
Hank	Hank	k1gInSc1
Aaron	Aarona	k1gFnPc2
<g/>
,	,	kIx,
tenistky	tenistka	k1gFnSc2
Serena	Seren	k2eAgFnSc1d1
Williamsová	Williamsová	k1gFnSc1
<g/>
,	,	kIx,
Chris	Chris	k1gFnSc1
Evertová	Evertová	k1gFnSc1
a	a	k8xC
Billie	Billie	k1gFnSc1
Jean	Jean	k1gMnSc1
Kingová	Kingový	k2eAgFnSc1d1
<g/>
,	,	kIx,
tenisté	tenista	k1gMnPc1
Andre	Andr	k1gMnSc5
Agassi	Agass	k1gMnSc5
<g/>
,	,	kIx,
John	John	k1gMnSc1
McEnroe	McEnro	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Jimmy	Jimm	k1gInPc1
Connors	Connors	k1gInSc1
a	a	k8xC
Pete	Pete	k1gNnSc1
Sampras	Sampras	k1gMnSc1
<g/>
,	,	kIx,
golfisté	golfista	k1gMnPc1
Tiger	Tigero	k1gNnPc2
Woods	Woodsa	k1gFnPc2
a	a	k8xC
Jack	Jack	k1gMnSc1
Nicklaus	Nicklaus	k1gMnSc1
<g/>
,	,	kIx,
cyklista	cyklista	k1gMnSc1
Lance	lance	k1gNnSc2
Armstrong	Armstrong	k1gMnSc1
<g/>
,	,	kIx,
plavci	plavec	k1gMnPc1
Michael	Michael	k1gMnSc1
Phelps	Phelps	k1gInSc1
(	(	kIx(
<g/>
23	#num#	k4
zlatých	zlatý	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
medailí	medaile	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
Spitz	Spitz	k1gMnSc1
(	(	kIx(
<g/>
9	#num#	k4
zlatých	zlatý	k2eAgFnPc2d1
olympijských	olympijský	k2eAgFnPc2d1
medailí	medaile	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Matt	Matt	k2eAgMnSc1d1
Biondi	Biond	k1gMnPc1
(	(	kIx(
<g/>
8	#num#	k4
zlatých	zlatá	k1gFnPc2
<g/>
)	)	kIx)
a	a	k8xC
Jenny	Jenna	k1gFnSc2
Thompsonová	Thompsonová	k1gFnSc1
(	(	kIx(
<g/>
8	#num#	k4
zlatých	zlatá	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
atleti	atlet	k1gMnPc1
Jesse	Jesse	k1gFnSc2
Owens	Owens	k1gInSc1
<g/>
,	,	kIx,
Carl	Carl	k1gMnSc1
Lewis	Lewis	k1gFnSc2
<g/>
,	,	kIx,
Florence	Florenc	k1gFnSc2
Griffith-Joynerová	Griffith-Joynerový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
Johnson	Johnson	k1gMnSc1
a	a	k8xC
Ray	Ray	k1gMnSc1
Ewry	Ewra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jediným	jediný	k2eAgMnSc7d1
americkým	americký	k2eAgMnSc7d1
mistrem	mistr	k1gMnSc7
světa	svět	k1gInSc2
v	v	k7c6
šachu	šach	k1gInSc6
byl	být	k5eAaImAgMnS
Bobby	Bobba	k1gFnPc4
Fischer	Fischer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
největším	veliký	k2eAgFnPc3d3
hvězdám	hvězda	k1gFnPc3
amerického	americký	k2eAgInSc2d1
fotbalu	fotbal	k1gInSc2
patří	patřit	k5eAaImIp3nS
Jerry	Jerra	k1gFnPc4
Rice	Ric	k1gFnSc2
nebo	nebo	k8xC
Adam	Adam	k1gMnSc1
Vinatieri	Vinatier	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
USA	USA	kA
jsou	být	k5eAaImIp3nP
na	na	k7c6
letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
medailově	medailově	k6eAd1
nejúspěšnější	úspěšný	k2eAgFnSc1d3
zemí	zem	k1gFnSc7
<g/>
,	,	kIx,
na	na	k7c6
zimních	zimní	k2eAgInPc6d1
jsou	být	k5eAaImIp3nP
druhé	druhý	k4xOgFnSc6
za	za	k7c7
Norskem	Norsko	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
největším	veliký	k2eAgMnPc3d3
"	"	kIx"
<g/>
zimním	zimní	k2eAgFnPc3d1
hvězdám	hvězda	k1gFnPc3
<g/>
"	"	kIx"
amerického	americký	k2eAgInSc2d1
sportu	sport	k1gInSc2
patří	patřit	k5eAaImIp3nS
sjezdařka	sjezdařka	k1gFnSc1
Lindsey	Lindsea	k1gFnSc2
Vonnová	Vonnová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejúspěšnějším	úspěšný	k2eAgMnSc7d3
americkým	americký	k2eAgMnSc7d1
hokejistou	hokejista	k1gMnSc7
v	v	k7c6
NHL	NHL	kA
byl	být	k5eAaImAgInS
Mike	Mike	k1gInSc1
Modano	Modana	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
osmkrát	osmkrát	k6eAd1
olympijské	olympijský	k2eAgFnSc2d1
hry	hra	k1gFnSc2
pořádaly	pořádat	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čtyřikrát	čtyřikrát	k6eAd1
šlo	jít	k5eAaImAgNnS
o	o	k7c4
hry	hra	k1gFnPc4
letní	letní	k2eAgFnPc4d1
(	(	kIx(
<g/>
v	v	k7c6
St.	st.	kA
Louis	Louis	k1gMnSc1
roku	rok	k1gInSc2
1904	#num#	k4
<g/>
,	,	kIx,
v	v	k7c4
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
v	v	k7c6
letech	léto	k1gNnPc6
1932	#num#	k4
a	a	k8xC
1984	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
Atlantě	Atlanta	k1gFnSc6
1996	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
čtyřikrát	čtyřikrát	k6eAd1
o	o	k7c4
hry	hra	k1gFnPc4
zimní	zimní	k2eAgFnPc4d1
(	(	kIx(
<g/>
v	v	k7c6
Lake	Lake	k1gNnSc6
Placid	Placid	k1gInSc1
1932	#num#	k4
a	a	k8xC
1980	#num#	k4
<g/>
,	,	kIx,
ve	v	k7c6
Squaw	squaw	k1gFnPc6
Valley	Vallea	k1gFnSc2
1960	#num#	k4
a	a	k8xC
v	v	k7c6
Salt	salto	k1gNnPc2
Lake	Lak	k1gFnSc2
City	city	k1gNnSc1
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2028	#num#	k4
se	se	k3xPyFc4
budou	být	k5eAaImBp3nP
konat	konat	k5eAaImF
letní	letní	k2eAgFnPc1d1
hry	hra	k1gFnPc1
již	již	k6eAd1
potřetí	potřetí	k4xO
v	v	k7c4
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
pořádaly	pořádat	k5eAaImAgInP
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Ernest	Ernest	k1gMnSc1
Hemingway	Hemingwaa	k1gFnSc2
</s>
<s>
Americkými	americký	k2eAgMnPc7d1
nositeli	nositel	k1gMnPc7
Nobelovy	Nobelův	k2eAgFnSc2d1
ceny	cena	k1gFnSc2
za	za	k7c4
literaturu	literatura	k1gFnSc4
jsou	být	k5eAaImIp3nP
Ernest	Ernest	k1gMnSc1
Hemingway	Hemingwaa	k1gFnSc2
<g/>
,	,	kIx,
John	John	k1gMnSc1
Steinbeck	Steinbeck	k1gMnSc1
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
Stearns	Stearnsa	k1gFnPc2
Eliot	Eliot	k1gMnSc1
<g/>
,	,	kIx,
William	William	k1gInSc1
Faulkner	Faulkner	k1gInSc1
<g/>
,	,	kIx,
Pearl	Pearl	k1gInSc1
S.	S.	kA
Bucková	Bucková	k1gFnSc1
<g/>
,	,	kIx,
Toni	Toni	k1gFnSc1
Morrisonová	Morrisonová	k1gFnSc1
<g/>
,	,	kIx,
Saul	Saul	k1gMnSc1
Bellow	Bellow	k1gMnSc1
<g/>
,	,	kIx,
Isaac	Isaac	k1gFnSc1
Bashevis	Bashevis	k1gFnPc2
Singer	Singer	k1gMnSc1
<g/>
,	,	kIx,
Sinclair	Sinclair	k1gMnSc1
Lewis	Lewis	k1gFnSc2
a	a	k8xC
Eugene	Eugen	k1gInSc5
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Neill	Neill	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Ke	k	k7c3
klasikům	klasik	k1gMnPc3
americké	americký	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
patří	patřit	k5eAaImIp3nS
též	též	k9
Mark	Mark	k1gMnSc1
Twain	Twain	k1gMnSc1
<g/>
,	,	kIx,
Arthur	Arthur	k1gMnSc1
Miller	Miller	k1gMnSc1
<g/>
,	,	kIx,
Jerome	Jerom	k1gInSc5
David	David	k1gMnSc1
Salinger	Salinger	k1gMnSc1
<g/>
,	,	kIx,
Vladimir	Vladimir	k1gMnSc1
Nabokov	Nabokov	k1gInSc1
<g/>
,	,	kIx,
Francis	Francis	k1gFnSc1
Scott	Scott	k1gMnSc1
Fitzgerald	Fitzgerald	k1gMnSc1
<g/>
,	,	kIx,
Ayn	Ayn	k1gMnSc1
Randová	Randová	k1gFnSc1
<g/>
,	,	kIx,
Truman	Truman	k1gMnSc1
Capote	capot	k1gInSc5
<g/>
,	,	kIx,
Henry	Henry	k1gMnSc1
James	James	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Updike	Updik	k1gFnSc2
<g/>
,	,	kIx,
Norman	Norman	k1gMnSc1
Mailer	Mailer	k1gMnSc1
<g/>
,	,	kIx,
Philip	Philip	k1gMnSc1
Roth	Roth	k1gMnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
O.	O.	kA
Henry	henry	k1gInSc1
<g/>
,	,	kIx,
Gore	Gore	k1gFnSc1
Vidal	Vidal	k1gMnSc1
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
Bukowski	Bukowsk	k1gFnSc2
<g/>
,	,	kIx,
Henry	Henry	k1gMnSc1
Miller	Miller	k1gMnSc1
či	či	k8xC
Thomas	Thomas	k1gMnSc1
Pynchon	Pynchon	k1gMnSc1
v	v	k7c6
próze	próza	k1gFnSc6
<g/>
,	,	kIx,
Walt	Walt	k1gMnSc1
Whitman	Whitman	k1gMnSc1
<g/>
,	,	kIx,
Emily	Emil	k1gMnPc4
Dickinsonová	Dickinsonový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Ralph	Ralph	k1gInSc1
Waldo	Waldo	k1gNnSc1
Emerson	Emerson	k1gInSc1
<g/>
,	,	kIx,
Jack	Jack	k1gMnSc1
Kerouac	Kerouac	k1gFnSc1
<g/>
,	,	kIx,
Allen	Allen	k1gMnSc1
Ginsberg	Ginsberg	k1gMnSc1
<g/>
,	,	kIx,
Ezra	Ezra	k1gMnSc1
Pound	pound	k1gInSc1
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
Frost	Frost	k1gMnSc1
<g/>
,	,	kIx,
Sylvia	Sylvia	k1gFnSc1
Plathová	Plathová	k1gFnSc1
<g/>
,	,	kIx,
Langston	Langston	k1gInSc1
Hughes	Hughes	k1gMnSc1
či	či	k8xC
Henry	Henry	k1gMnSc1
Wadsworth	Wadsworth	k1gMnSc1
Longfellow	Longfellow	k1gMnSc1
v	v	k7c4
poezii	poezie	k1gFnSc4
a	a	k8xC
Tennessee	Tennessee	k1gFnSc4
Williams	Williamsa	k1gFnPc2
v	v	k7c6
dramatu	drama	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Velké	velký	k2eAgFnPc1d1
popularity	popularita	k1gFnPc1
lze	lze	k6eAd1
v	v	k7c6
USA	USA	kA
dosáhnout	dosáhnout	k5eAaPmF
i	i	k9
za	za	k7c2
pomoci	pomoc	k1gFnSc2
jediné	jediný	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
stalo	stát	k5eAaPmAgNnS
Harper	Harper	k1gInSc4
Leeové	Leeová	k1gFnSc2
(	(	kIx(
<g/>
Jako	jako	k8xC,k8xS
zabít	zabít	k5eAaPmF
ptáčka	ptáček	k1gMnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Margaret	Margareta	k1gFnPc2
Mitchellové	Mitchellová	k1gFnSc2
(	(	kIx(
<g/>
Jih	jih	k1gInSc1
proti	proti	k7c3
severu	sever	k1gInSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Harriet	Harriet	k1gInSc1
Beecher	Beechra	k1gFnPc2
Stoweové	Stoweová	k1gFnSc2
(	(	kIx(
<g/>
Chaloupka	chaloupka	k1gFnSc1
strýčka	strýček	k1gMnSc2
Toma	Tom	k1gMnSc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
Maye	May	k1gMnSc2
Angelou	Angela	k1gFnSc7
(	(	kIx(
<g/>
I	i	k9
Know	Know	k1gMnSc1
Why	Why	k1gMnSc1
the	the	k?
Caged	Caged	k1gInSc1
Bird	Bird	k1gMnSc1
Sings	Sings	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
byly	být	k5eAaImAgInP
vždy	vždy	k6eAd1
především	především	k6eAd1
velmocí	velmoc	k1gFnPc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
žánrové	žánrový	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Králem	Král	k1gMnSc7
žánrů	žánr	k1gInPc2
byl	být	k5eAaImAgMnS
již	již	k6eAd1
Edgar	Edgar	k1gMnSc1
Allan	Allan	k1gMnSc1
Poe	Poe	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobrodružný	dobrodružný	k2eAgInSc1d1
román	román	k1gInSc1
přivedl	přivést	k5eAaPmAgInS
k	k	k7c3
dokonalosti	dokonalost	k1gFnSc3
Jack	Jack	k1gMnSc1
London	London	k1gMnSc1
<g/>
,	,	kIx,
Herman	Herman	k1gMnSc1
Melville	Melville	k1gFnSc2
do	do	k7c2
něj	on	k3xPp3gMnSc2
vnesl	vnést	k5eAaPmAgMnS
slavnou	slavný	k2eAgFnSc4d1
postavu	postava	k1gFnSc4
a	a	k8xC
symbol	symbol	k1gInSc4
touhy	touha	k1gFnSc2
Mobydicka	Mobydicko	k1gNnSc2
<g/>
,	,	kIx,
Edgar	Edgar	k1gMnSc1
Rice	Ric	k1gFnSc2
Burroughs	Burroughs	k1gInSc1
Tarzana	Tarzan	k1gMnSc4
<g/>
,	,	kIx,
James	James	k1gMnSc1
Fenimore	Fenimor	k1gInSc5
Cooper	Coopra	k1gFnPc2
svého	svůj	k3xOyFgMnSc2
Posledního	poslední	k2eAgMnSc2d1
mohykána	mohykán	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Isaac	Isaac	k1gInSc1
Asimov	Asimov	k1gInSc4
<g/>
,	,	kIx,
Ray	Ray	k1gFnSc4
Bradbury	Bradbura	k1gFnSc2
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
A.	A.	kA
Heinlein	Heinlein	k1gMnSc1
a	a	k8xC
Philip	Philip	k1gMnSc1
K.	K.	kA
Dick	Dick	k1gMnSc1
patří	patřit	k5eAaImIp3nS
ke	k	k7c3
klasikům	klasik	k1gMnPc3
sci-fi	sci-fi	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jejím	její	k3xOp3gNnSc6
pomezí	pomezí	k1gNnSc6
se	se	k3xPyFc4
vždy	vždy	k6eAd1
rafinovaně	rafinovaně	k6eAd1
pohyboval	pohybovat	k5eAaImAgMnS
i	i	k9
Kurt	Kurt	k1gMnSc1
Vonnegut	Vonnegut	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stephen	Stephna	k1gFnPc2
King	King	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
králem	král	k1gMnSc7
literárního	literární	k2eAgInSc2d1
hororu	horor	k1gInSc2
a	a	k8xC
thrilleru	thriller	k1gInSc2
<g/>
,	,	kIx,
když	když	k8xS
tak	tak	k6eAd1
navázal	navázat	k5eAaPmAgMnS
na	na	k7c4
svého	svůj	k3xOyFgMnSc4
velkého	velký	k2eAgMnSc4d1
předchůdce	předchůdce	k1gMnSc4
Howarda	Howard	k1gMnSc2
Phillipse	Phillips	k1gMnSc2
Lovecrafta	Lovecraft	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známým	známý	k1gMnSc7
autorem	autor	k1gMnSc7
thrillerů	thriller	k1gInPc2
je	být	k5eAaImIp3nS
také	také	k9
John	John	k1gMnSc1
Grisham	Grisham	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mario	Mario	k1gMnSc1
Puzo	Puzo	k1gMnSc1
se	se	k3xPyFc4
proslavil	proslavit	k5eAaPmAgMnS
svým	svůj	k3xOyFgInSc7
mafiánským	mafiánský	k2eAgInSc7d1
eposem	epos	k1gInSc7
Kmotr	kmotr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Drsnou	drsný	k2eAgFnSc4d1
detektivní	detektivní	k2eAgFnSc4d1
školu	škola	k1gFnSc4
zakládal	zakládat	k5eAaImAgInS
Dashiell	Dashiell	k1gMnSc1
Hammett	Hammett	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Revoluci	revoluce	k1gFnSc4
v	v	k7c6
žánru	žánr	k1gInSc6
fantasy	fantas	k1gInPc4
svou	svůj	k3xOyFgFnSc7
Hrou	hra	k1gFnSc7
o	o	k7c4
trůny	trůn	k1gInPc4
způsobil	způsobit	k5eAaPmAgInS
George	George	k1gInSc1
R.	R.	kA
R.	R.	kA
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skulinu	skulina	k1gFnSc4
na	na	k7c6
trhu	trh	k1gInSc6
objevila	objevit	k5eAaPmAgFnS
i	i	k9
Stephenie	Stephenie	k1gFnSc1
Meyerová	Meyerový	k2eAgFnSc1d1
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
ságou	sága	k1gFnSc7
Stmívání	stmívání	k1gNnSc1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
Ursula	Ursula	k1gFnSc1
K.	K.	kA
Le	Le	k1gFnSc1
Guinová	Guinová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
průkopníkům	průkopník	k1gMnPc3
americké	americký	k2eAgFnSc2d1
fantasy	fantas	k1gInPc4
patřil	patřit	k5eAaImAgInS
již	již	k9
„	„	k?
<g/>
temný	temný	k2eAgMnSc1d1
romantik	romantik	k1gMnSc1
<g/>
“	“	k?
Nathaniel	Nathaniel	k1gMnSc1
Hawthorne	Hawthorn	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Špiónské	špiónský	k2eAgInPc1d1
romány	román	k1gInPc1
proslavily	proslavit	k5eAaPmAgInP
Toma	Tom	k1gMnSc4
Clancyho	Clancy	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kouzlo	kouzlo	k1gNnSc1
konspiračních	konspirační	k2eAgFnPc2d1
teorií	teorie	k1gFnPc2
zužitkoval	zužitkovat	k5eAaPmAgInS
beze	beze	k7c2
zbytku	zbytek	k1gInSc2
Dan	Dan	k1gMnSc1
Brown	Brown	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
humoristickém	humoristický	k2eAgInSc6d1
žánru	žánr	k1gInSc6
se	se	k3xPyFc4
prosadil	prosadit	k5eAaPmAgInS
Washington	Washington	k1gInSc1
Irving	Irving	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biografie	biografie	k1gFnSc1
proslavily	proslavit	k5eAaPmAgFnP
Carla	Carl	k1gMnSc4
Sandburga	Sandburga	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řadu	řad	k1gInSc2
klasických	klasický	k2eAgFnPc2d1
komiksových	komiksový	k2eAgFnPc2d1
postav	postava	k1gFnPc2
vytvořil	vytvořit	k5eAaPmAgMnS
Stan	stan	k1gInSc4
Lee	Lea	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
dějinách	dějiny	k1gFnPc6
amerického	americký	k2eAgNnSc2d1
divadla	divadlo	k1gNnSc2
sehrály	sehrát	k5eAaPmAgFnP
velkou	velký	k2eAgFnSc4d1
roli	role	k1gFnSc4
„	„	k?
<g/>
lehčí	lehčit	k5eAaImIp3nS
<g/>
“	“	k?
žánry	žánr	k1gInPc4
jako	jako	k9
burleska	burleska	k1gFnSc1
<g/>
,	,	kIx,
vaudeville	vaudeville	k1gInSc1
či	či	k8xC
minstrel	minstrel	k1gMnSc1
show	show	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
předpokladem	předpoklad	k1gInSc7
rozvoje	rozvoj	k1gInSc2
muzikálu	muzikál	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
Američané	Američan	k1gMnPc1
stali	stát	k5eAaPmAgMnP
velmocí	velmoc	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
151	#num#	k4
<g/>
]	]	kIx)
Proslulou	proslulý	k2eAgFnSc4d1
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
zejména	zejména	k9
čtyřicítka	čtyřicítka	k1gFnSc1
scén	scéna	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
na	na	k7c6
newyorské	newyorský	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
Broadway	Broadwaa	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
měly	mít	k5eAaImAgFnP
premiéru	premiéra	k1gFnSc4
jedny	jeden	k4xCgInPc1
z	z	k7c2
nejslavnějších	slavný	k2eAgInPc2d3
muzikálů	muzikál	k1gInPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
z	z	k7c2
pera	pero	k1gNnSc2
Brita	Brit	k1gMnSc2
Andrew	Andrew	k1gMnSc2
Lloyd	Lloyd	k1gMnSc1
Webbera	Webber	k1gMnSc2
(	(	kIx(
<g/>
Fantom	fantom	k1gInSc1
opery	opera	k1gFnSc2
<g/>
,	,	kIx,
Cats	Cats	k1gInSc1
<g/>
,	,	kIx,
Evita	Evita	k1gMnSc1
<g/>
,	,	kIx,
Jesus	Jesus	k1gMnSc1
Christ	Christ	k1gMnSc1
Superstar	superstar	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
řady	řada	k1gFnSc2
dalších	další	k2eAgMnPc2d1
autorů	autor	k1gMnPc2
(	(	kIx(
<g/>
West	West	k2eAgInSc1d1
Side	Side	k1gInSc1
Story	story	k1gFnSc1
<g/>
,	,	kIx,
Oklahoma	Oklahoma	k1gFnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
,	,	kIx,
Chicago	Chicago	k1gNnSc1
<g/>
,	,	kIx,
My	my	k3xPp1nPc1
Fair	fair	k6eAd1
Lady	lado	k1gNnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
152	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
1947	#num#	k4
je	být	k5eAaImIp3nS
udílena	udílen	k2eAgFnSc1d1
prestižní	prestižní	k2eAgFnSc1d1
divadelní	divadelní	k2eAgFnSc1d1
cena	cena	k1gFnSc1
Tony	Tony	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Výtvarné	výtvarný	k2eAgNnSc1d1
umění	umění	k1gNnSc1
</s>
<s>
Andy	Anda	k1gFnPc1
Warhol	Warhol	k1gInSc1
</s>
<s>
Americké	americký	k2eAgNnSc1d1
výtvarné	výtvarný	k2eAgNnSc1d1
umění	umění	k1gNnSc1
bylo	být	k5eAaImAgNnS
dlouho	dlouho	k6eAd1
ve	v	k7c6
stínu	stín	k1gInSc6
evropského	evropský	k2eAgInSc2d1
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
emancipovalo	emancipovat	k5eAaBmAgNnS
a	a	k8xC
stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
svébytným	svébytný	k2eAgMnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mary	Mary	k1gFnSc1
Cassattová	Cassattová	k1gFnSc1
byla	být	k5eAaImAgFnS
nejvýznamnější	významný	k2eAgFnSc7d3
reprezentantkou	reprezentantka	k1gFnSc7
impresionismu	impresionismus	k1gInSc2
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blízko	blízko	k6eAd1
k	k	k7c3
němu	on	k3xPp3gMnSc3
měl	mít	k5eAaImAgInS
i	i	k9
James	James	k1gMnSc1
Abbott	Abbott	k1gMnSc1
McNeill	McNeill	k1gMnSc1
Whistler	Whistler	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
realistickou	realistický	k2eAgFnSc4d1
malbu	malba	k1gFnSc4
vsadil	vsadit	k5eAaPmAgMnS
Edward	Edward	k1gMnSc1
Hopper	Hopper	k1gMnSc1
<g/>
,	,	kIx,
regionalismus	regionalismus	k1gInSc1
zakládal	zakládat	k5eAaImAgInS
Grant	grant	k1gInSc4
Wood	Wooda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průkopnicí	průkopnice	k1gFnSc7
moderního	moderní	k2eAgNnSc2d1
umění	umění	k1gNnSc2
v	v	k7c6
USA	USA	kA
byla	být	k5eAaImAgFnS
Georgia	Georgia	k1gFnSc1
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Keeffe	Keeff	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modernismus	modernismus	k1gInSc1
došel	dojít	k5eAaPmAgInS
ke	k	k7c3
svému	svůj	k3xOyFgInSc3
vrcholu	vrchol	k1gInSc3
v	v	k7c6
díle	díl	k1gInSc6
Jacksona	Jackson	k1gMnSc2
Pollocka	Pollocko	k1gNnSc2
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
hlavních	hlavní	k2eAgMnPc2d1
světových	světový	k2eAgMnPc2d1
představitelů	představitel	k1gMnPc2
abstraktního	abstraktní	k2eAgInSc2d1
expresionismu	expresionismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
USA	USA	kA
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
vznikl	vzniknout	k5eAaPmAgInS
i	i	k9
zcela	zcela	k6eAd1
osobitý	osobitý	k2eAgInSc4d1
výtvarný	výtvarný	k2eAgInSc4d1
směr	směr	k1gInSc4
zvaný	zvaný	k2eAgInSc1d1
pop-art	pop-art	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc7
hlavním	hlavní	k2eAgMnSc7d1
představitelem	představitel	k1gMnSc7
byl	být	k5eAaImAgMnS
Andy	Anda	k1gFnSc2
Warhol	Warhol	k1gInSc4
<g/>
,	,	kIx,
k	k	k7c3
dalším	další	k2eAgMnPc3d1
reprezentantům	reprezentant	k1gMnPc3
patřil	patřit	k5eAaImAgInS
Roy	Roy	k1gMnSc1
Lichtenstein	Lichtenstein	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jean-Michel	Jean-Michel	k1gMnSc1
Basquiat	Basquiat	k1gInSc4
založil	založit	k5eAaPmAgMnS
pouliční	pouliční	k2eAgNnSc4d1
graffiti	graffiti	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Uměleckou	umělecký	k2eAgFnSc4d1
fotografii	fotografia	k1gFnSc4
formoval	formovat	k5eAaImAgMnS
zvláště	zvláště	k6eAd1
Man	Man	k1gMnSc1
Ray	Ray	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Guggenheimovo	Guggenheimův	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
</s>
<s>
K	k	k7c3
nejvýznamnějším	významný	k2eAgFnPc3d3
galeriím	galerie	k1gFnPc3
a	a	k8xC
muzeím	muzeum	k1gNnPc3
specializovaným	specializovaný	k2eAgNnSc7d1
na	na	k7c4
sbírání	sbírání	k1gNnSc4
umění	umění	k1gNnSc2
patří	patřit	k5eAaImIp3nS
Metropolitní	metropolitní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
umění	umění	k1gNnSc2
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
,	,	kIx,
nejstarší	starý	k2eAgNnSc4d3
a	a	k8xC
největší	veliký	k2eAgNnSc4d3
muzeum	muzeum	k1gNnSc4
umění	umění	k1gNnSc2
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
nacházející	nacházející	k2eAgMnSc1d1
se	se	k3xPyFc4
na	na	k7c6
Páté	pátá	k1gFnSc6
avenue	avenue	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
významnou	významný	k2eAgFnSc7d1
sbírkou	sbírka	k1gFnSc7
starého	starý	k2eAgNnSc2d1
evropského	evropský	k2eAgNnSc2d1
umění	umění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
umění	umění	k1gNnSc4
moderní	moderní	k2eAgFnSc2d1
se	se	k3xPyFc4
naopak	naopak	k6eAd1
specializuje	specializovat	k5eAaBmIp3nS
newyorské	newyorský	k2eAgNnSc1d1
Muzeum	muzeum	k1gNnSc1
moderního	moderní	k2eAgNnSc2d1
umění	umění	k1gNnSc2
(	(	kIx(
<g/>
zvané	zvaný	k2eAgFnSc2d1
též	též	k9
MoMA	MoMA	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
k	k	k7c3
jehož	jehož	k3xOyRp3gInPc3
pokladům	poklad	k1gInPc3
patří	patřit	k5eAaImIp3nS
díla	dílo	k1gNnSc2
jako	jako	k8xC,k8xS
Spící	spící	k2eAgFnSc1d1
cikánka	cikánka	k1gFnSc1
Henri	Henri	k1gNnSc4
Rousseaua	Rousseau	k1gMnSc2
<g/>
,	,	kIx,
Hvězdná	hvězdný	k2eAgFnSc1d1
noc	noc	k1gFnSc1
Vincenta	Vincent	k1gMnSc2
van	vana	k1gFnPc2
Gogha	Gogha	k1gMnSc1
<g/>
,	,	kIx,
Avignonské	avignonský	k2eAgFnPc1d1
slečny	slečna	k1gFnPc1
Pabla	Pabl	k1gMnSc2
Picassa	Picass	k1gMnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
slavná	slavný	k2eAgFnSc1d1
Campbellova	Campbellův	k2eAgFnSc1d1
polévka	polévka	k1gFnSc1
Andy	Anda	k1gFnSc2
Warhola	Warhola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
také	také	k9
Guggenheimovo	Guggenheimův	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
známé	známý	k2eAgNnSc1d1
průkopnickou	průkopnický	k2eAgFnSc7d1
architekturou	architektura	k1gFnSc7
svého	svůj	k3xOyFgNnSc2
sídla	sídlo	k1gNnSc2
z	z	k7c2
dílny	dílna	k1gFnSc2
Franka	Frank	k1gMnSc2
Llloyda	Llloyd	k1gMnSc2
Wrighta	Wright	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unikátní	unikátní	k2eAgFnSc7d1
architekturou	architektura	k1gFnSc7
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
chlubit	chlubit	k5eAaImF
i	i	k9
Denverské	denverský	k2eAgNnSc4d1
umělecké	umělecký	k2eAgNnSc4d1
muzeum	muzeum	k1gNnSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc4
budovu	budova	k1gFnSc4
navrhl	navrhnout	k5eAaPmAgMnS
Daniel	Daniel	k1gMnSc1
Libeskind	Libeskind	k1gMnSc1
<g/>
,	,	kIx,
Milwaukeeské	Milwaukeeský	k2eAgNnSc1d1
umělecké	umělecký	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
,	,	kIx,
losangeleské	losangeleský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
The	The	k1gFnSc2
Broad	Broad	k1gInSc1
nebo	nebo	k8xC
Sanfranciské	sanfranciský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
moderního	moderní	k2eAgNnSc2d1
umění	umění	k1gNnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
nějž	jenž	k3xRgMnSc4
sídlo	sídlo	k1gNnSc1
vytvořil	vytvořit	k5eAaPmAgMnS
švýcarský	švýcarský	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
Mario	Mario	k1gMnSc1
Botta	Botta	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významné	významný	k2eAgFnPc4d1
sbírky	sbírka	k1gFnPc4
lze	lze	k6eAd1
nalézt	nalézt	k5eAaPmF,k5eAaBmF
i	i	k9
v	v	k7c4
Muzeum	muzeum	k1gNnSc4
J.	J.	kA
Paula	Paul	k1gMnSc2
Gettyho	Getty	k1gMnSc2
v	v	k7c4
Los	los	k1gInSc4
Angeles	Angelesa	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
Muzeu	muzeum	k1gNnSc6
výtvarného	výtvarný	k2eAgNnSc2d1
umění	umění	k1gNnSc2
v	v	k7c6
Bostonu	Boston	k1gInSc6
<g/>
,	,	kIx,
Muzeu	muzeum	k1gNnSc3
umění	umění	k1gNnPc2
ve	v	k7c6
Filadelfii	Filadelfie	k1gFnSc6
<g/>
,	,	kIx,
Institutu	institut	k1gInSc6
umění	umění	k1gNnSc2
v	v	k7c6
Chicagu	Chicago	k1gNnSc6
nebo	nebo	k8xC
Losangeleském	losangeleský	k2eAgNnSc6d1
muzeum	muzeum	k1gNnSc4
umění	umění	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důležitou	důležitý	k2eAgFnSc7d1
organizací	organizace	k1gFnSc7
sbírající	sbírající	k2eAgNnSc1d1
umění	umění	k1gNnSc1
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
Smithsonův	Smithsonův	k2eAgInSc1d1
institut	institut	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Washingtonu	Washington	k1gInSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
Národní	národní	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
<g/>
,	,	kIx,
jedna	jeden	k4xCgFnSc1
z	z	k7c2
mála	málo	k4c2
významných	významný	k2eAgFnPc2d1
státních	státní	k2eAgFnPc2d1
uměleckých	umělecký	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Silná	silný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
americká	americký	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frank	Frank	k1gMnSc1
Lloyd	Lloyd	k1gMnSc1
Wright	Wright	k1gMnSc1
zavedl	zavést	k5eAaPmAgMnS
koncept	koncept	k1gInSc4
„	„	k?
<g/>
organické	organický	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobným	podobný	k2eAgMnSc7d1
průkopníkem	průkopník	k1gMnSc7
byl	být	k5eAaImAgMnS
Buckminster	Buckminster	k1gMnSc1
Fuller	Fuller	k1gMnSc1
<g/>
,	,	kIx,
tvůrce	tvůrce	k1gMnSc1
geodetických	geodetický	k2eAgFnPc2d1
kopulí	kopule	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Louis	Louis	k1gMnSc1
Sullivan	Sullivan	k1gMnSc1
je	být	k5eAaImIp3nS
označován	označovat	k5eAaImNgInS
za	za	k7c4
„	„	k?
<g/>
otce	otec	k1gMnSc4
mrakodrapů	mrakodrap	k1gInPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
americkou	americký	k2eAgFnSc4d1
kulturu	kultura	k1gFnSc4
přímo	přímo	k6eAd1
ikonické	ikonický	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typickými	typický	k2eAgInPc7d1
bílými	bílý	k2eAgInPc7d1
domy	dům	k1gInPc7
proslul	proslout	k5eAaPmAgMnS
modernista	modernista	k1gMnSc1
Richard	Richard	k1gMnSc1
Meier	Meier	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
modernismem	modernismus	k1gInSc7
a	a	k8xC
postmodernismem	postmodernismus	k1gInSc7
stál	stát	k5eAaImAgMnS
Philip	Philip	k1gMnSc1
Johnson	Johnson	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klasikem	klasik	k1gMnSc7
postmodernismu	postmodernismus	k1gInSc2
je	být	k5eAaImIp3nS
Robert	Robert	k1gMnSc1
Venturi	Ventur	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Číně	Čína	k1gFnSc6
narozený	narozený	k2eAgMnSc1d1
I.	I.	kA
M.	M.	kA
Pei	Pei	k1gMnSc1
se	se	k3xPyFc4
proslavil	proslavit	k5eAaPmAgMnS
svými	svůj	k3xOyFgInPc7
moderními	moderní	k2eAgInPc7d1
zásahy	zásah	k1gInPc7
do	do	k7c2
galerie	galerie	k1gFnSc2
Louvre	Louvre	k1gInSc1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Hudba	hudba	k1gFnSc1
</s>
<s>
Elvis	Elvis	k1gMnSc1
Presley	Preslea	k1gFnSc2
</s>
<s>
K	k	k7c3
nejslavnějším	slavný	k2eAgMnPc3d3
americkým	americký	k2eAgMnPc3d1
skladatelům	skladatel	k1gMnPc3
vážné	vážný	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
patří	patřit	k5eAaImIp3nP
George	George	k1gNnPc1
Gershwin	Gershwin	k1gMnSc1
<g/>
,	,	kIx,
Leonard	Leonard	k1gMnSc1
Bernstein	Bernstein	k1gMnSc1
<g/>
,	,	kIx,
Aaron	Aaron	k1gMnSc1
Copland	Copland	k1gInSc1
a	a	k8xC
Philip	Philip	k1gInSc1
Glass	Glassa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
čele	čelo	k1gNnSc6
avantgardních	avantgardní	k2eAgInPc2d1
pokusů	pokus	k1gInPc2
stál	stát	k5eAaImAgMnS
John	John	k1gMnSc1
Cage	Cag	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Legendou	legenda	k1gFnSc7
filmové	filmový	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
je	být	k5eAaImIp3nS
John	John	k1gMnSc1
Williams	Williams	k1gInSc1
<g/>
,	,	kIx,
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
žánru	žánr	k1gInSc6
prosluli	proslout	k5eAaPmAgMnP
též	též	k9
James	James	k1gMnSc1
Horner	Horner	k1gMnSc1
<g/>
,	,	kIx,
Henry	Henry	k1gMnSc1
Mancini	Mancin	k2eAgMnPc1d1
či	či	k8xC
Quincy	Quinca	k1gFnPc1
Jones	Jonesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Králem	Král	k1gMnSc7
muzikálů	muzikál	k1gInPc2
byl	být	k5eAaImAgInS
Cole	cola	k1gFnSc3
Porter	porter	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scott	Scott	k2eAgInSc1d1
Joplin	Joplin	k1gInSc1
založil	založit	k5eAaPmAgInS
hudební	hudební	k2eAgInSc1d1
styl	styl	k1gInSc1
ragtime	ragtime	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jazzovými	jazzový	k2eAgFnPc7d1
legendami	legenda	k1gFnPc7
jsou	být	k5eAaImIp3nP
Louis	Louis	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
<g/>
,	,	kIx,
Ella	Ella	k1gMnSc1
Fitzgerald	Fitzgerald	k1gMnSc1
<g/>
,	,	kIx,
Miles	Miles	k1gMnSc1
Davis	Davis	k1gFnSc2
<g/>
,	,	kIx,
Glenn	Glenn	k1gMnSc1
Miller	Miller	k1gMnSc1
<g/>
,	,	kIx,
Nat	Nat	k1gMnSc1
King	King	k1gMnSc1
Cole	cola	k1gFnSc6
<g/>
,	,	kIx,
Charlie	Charlie	k1gMnSc1
Parker	Parker	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Coltrane	Coltran	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Nejslavnější	slavný	k2eAgMnPc1d3
sóloví	sólový	k2eAgMnPc1d1
umělci	umělec	k1gMnPc1
populární	populární	k2eAgFnSc2d1
a	a	k8xC
rockové	rockový	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
pocházejí	pocházet	k5eAaImIp3nP
často	často	k6eAd1
právě	právě	k9
z	z	k7c2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klasiky	klasika	k1gFnSc2
40	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
,	,	kIx,
otci	otec	k1gMnSc3
zakladateli	zakladatel	k1gMnSc3
žánru	žánr	k1gInSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
Elvis	Elvis	k1gMnSc1
Presley	Preslea	k1gFnSc2
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
Dylan	Dylan	k1gMnSc1
<g/>
,	,	kIx,
Johnny	Johnen	k2eAgFnPc1d1
Cash	cash	k1gFnPc1
<g/>
,	,	kIx,
Jimi	on	k3xPp3gInPc7
Hendrix	Hendrix	k1gInSc4
<g/>
,	,	kIx,
Frank	Frank	k1gMnSc1
Sinatra	Sinatra	k1gFnSc1
<g/>
,	,	kIx,
Pete	Pete	k1gFnSc1
Seeger	Seeger	k1gMnSc1
<g/>
,	,	kIx,
B.B.	B.B.	k1gMnSc1
King	King	k1gMnSc1
<g/>
,	,	kIx,
Ray	Ray	k1gMnSc1
Charles	Charles	k1gMnSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Bruce	Bruce	k1gFnSc1
Springsteen	Springsteen	k1gInSc1
<g/>
,	,	kIx,
James	James	k1gMnSc1
Brown	Brown	k1gMnSc1
<g/>
,	,	kIx,
Janis	Janis	k1gFnSc1
Joplin	Joplin	k1gInSc1
<g/>
,	,	kIx,
Tina	Tina	k1gFnSc1
Turner	turner	k1gMnSc1
<g/>
,	,	kIx,
Aretha	Aretha	k1gMnSc1
Franklin	Franklin	k1gInSc1
<g/>
,	,	kIx,
Frank	Frank	k1gMnSc1
Zappa	Zappa	k1gFnSc1
<g/>
,	,	kIx,
Stevie	Stevie	k1gFnSc1
Wonder	Wondra	k1gFnPc2
<g/>
,	,	kIx,
Bing	bingo	k1gNnPc2
Crosby	Crosb	k1gInPc7
<g/>
,	,	kIx,
Chuck	Chuck	k1gMnSc1
Berry	Berra	k1gFnSc2
<g/>
,	,	kIx,
Eartha	Eartha	k1gFnSc1
Kittová	Kittová	k1gFnSc1
<g/>
,	,	kIx,
Liza	Liza	k1gFnSc1
Minnelli	Minnelle	k1gFnSc4
<g/>
,	,	kIx,
Duke	Duke	k1gFnSc1
Ellington	Ellington	k1gInSc1
<g/>
,	,	kIx,
Joan	Joan	k1gInSc1
Baezová	Baezová	k1gFnSc1
<g/>
,	,	kIx,
Buddy	Budda	k1gFnPc1
Holly	Holla	k1gFnSc2
<g/>
,	,	kIx,
Lou	Lou	k1gMnSc1
Reed	Reed	k1gMnSc1
<g/>
,	,	kIx,
Billie	Billie	k1gFnSc1
Holiday	Holidaa	k1gFnSc2
<g/>
,	,	kIx,
Diana	Diana	k1gFnSc1
Rossová	Rossová	k1gFnSc1
<g/>
,	,	kIx,
Donna	donna	k1gFnSc1
Summer	Summer	k1gInSc1
<g/>
,	,	kIx,
Nina	Nina	k1gFnSc1
Simone	Simon	k1gMnSc5
<g/>
,	,	kIx,
Gene	gen	k1gInSc5
Kelly	Kell	k1gInPc7
<g/>
,	,	kIx,
Etta	Etta	k1gMnSc1
James	James	k1gMnSc1
<g/>
,	,	kIx,
Alice	Alice	k1gFnSc1
Cooper	Coopra	k1gFnPc2
či	či	k8xC
Iggy	Igga	k1gFnSc2
Pop	pop	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc1d1
silná	silný	k2eAgFnSc1d1
vlna	vlna	k1gFnSc1
přišla	přijít	k5eAaPmAgFnS
v	v	k7c6
80	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
:	:	kIx,
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
,	,	kIx,
Madonna	Madonna	k1gFnSc1
<g/>
,	,	kIx,
Whitney	Whitneum	k1gNnPc7
Houston	Houston	k1gInSc1
<g/>
,	,	kIx,
Cher	Cher	k1gInSc1
<g/>
,	,	kIx,
Prince	princ	k1gMnPc4
<g/>
,	,	kIx,
Mariah	Mariah	k1gInSc4
Carey	Carea	k1gFnSc2
<g/>
,	,	kIx,
Janet	Janet	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
nastal	nastat	k5eAaPmAgInS
mohutný	mohutný	k2eAgInSc1d1
nástup	nástup	k1gInSc1
rapu	rapa	k1gFnSc4
a	a	k8xC
hip-hopu	hip-hopa	k1gFnSc4
<g/>
,	,	kIx,
nejslavnějšími	slavný	k2eAgInPc7d3
rappery	rapper	k1gInPc7
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
Eminem	Emino	k1gNnSc7
<g/>
,	,	kIx,
Tupac	Tupac	k1gInSc1
Shakur	Shakura	k1gFnPc2
<g/>
,	,	kIx,
50	#num#	k4
Cent	cent	k1gInSc1
<g/>
,	,	kIx,
Kanye	Kanye	k1gFnSc1
West	West	k1gMnSc1
<g/>
,	,	kIx,
Akon	Akon	k1gMnSc1
<g/>
,	,	kIx,
Jay	Jay	k1gMnSc1
Z	Z	kA
<g/>
,	,	kIx,
Snoop	Snoop	k1gInSc4
Dogg	Dogga	k1gFnPc2
<g/>
,	,	kIx,
Dr	dr	kA
<g/>
.	.	kIx.
Dre	Dre	k1gMnSc1
<g/>
,	,	kIx,
Lil	lít	k5eAaImAgMnS
Wayne	Wayn	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Králem	Král	k1gMnSc7
latino-popu	latino-pop	k1gInSc2
je	být	k5eAaImIp3nS
Ricky	Ricek	k1gMnPc4
Martin	Martin	k1gInSc1
<g/>
,	,	kIx,
americké	americký	k2eAgFnSc2d1
country	country	k2eAgFnSc2d1
pak	pak	k8xC
Dolly	Doll	k1gMnPc4
Parton	Parton	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
kralují	kralovat	k5eAaImIp3nP
hitparádám	hitparáda	k1gFnPc3
Lady	Lada	k1gFnSc2
Gaga	Gaga	k1gMnSc1
<g/>
,	,	kIx,
Beyoncé	Beyoncá	k1gFnPc1
<g/>
,	,	kIx,
Katy	Kata	k1gFnPc1
Perry	Perra	k1gFnPc1
<g/>
,	,	kIx,
Miley	Milea	k1gFnPc1
Cyrus	Cyrus	k1gInSc1
<g/>
,	,	kIx,
Britney	Britney	k1gInPc1
Spears	Spearsa	k1gFnPc2
<g/>
,	,	kIx,
Justin	Justina	k1gFnPc2
Timberlake	Timberlak	k1gInSc2
<g/>
,	,	kIx,
Bruno	Bruno	k1gMnSc1
Mars	Mars	k1gMnSc1
<g/>
,	,	kIx,
Anastacia	Anastacia	k1gFnSc1
<g/>
,	,	kIx,
Taylor	Taylor	k1gMnSc1
Swift	Swift	k1gMnSc1
<g/>
,	,	kIx,
Christina	Christin	k1gMnSc2
Aguilera	Aguiler	k1gMnSc2
<g/>
,	,	kIx,
Pink	pink	k0
<g/>
,	,	kIx,
Gwen	Gwen	k1gMnSc1
Stefani	Stefan	k1gMnPc1
<g/>
,	,	kIx,
Usher	Usher	k1gMnSc1
<g/>
,	,	kIx,
Lana	lano	k1gNnSc2
Del	Del	k1gFnSc2
Rey	Rea	k1gFnSc2
<g/>
,	,	kIx,
Norah	Norah	k1gMnSc1
Jones	Jones	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Ze	z	k7c2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
pochází	pocházet	k5eAaImIp3nS
mnoho	mnoho	k4c1
z	z	k7c2
nejslavnějších	slavný	k2eAgFnPc2d3
rockových	rockový	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povětšinou	povětšinou	k6eAd1
z	z	k7c2
Kalifornie	Kalifornie	k1gFnSc2
<g/>
:	:	kIx,
The	The	k1gFnSc1
Doors	Doorsa	k1gFnPc2
(	(	kIx(
<g/>
Jim	on	k3xPp3gMnPc3
Morrison	Morrison	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Metallica	Metallica	k1gMnSc1
(	(	kIx(
<g/>
James	James	k1gMnSc1
Hetfield	Hetfield	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Linkin	Linkin	k2eAgInSc1d1
Park	park	k1gInSc1
(	(	kIx(
<g/>
Chester	Chester	k1gInSc1
Bennington	Bennington	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Red	Red	k1gMnSc1
Hot	hot	k0
Chili	Chile	k1gFnSc3
Peppers	Peppers	k1gInSc1
(	(	kIx(
<g/>
Anthony	Anthon	k1gInPc1
Kiedis	Kiedis	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Eagles	Eagles	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Beach	Beach	k1gInSc4
Boys	boy	k1gMnPc2
<g/>
,	,	kIx,
Thirty	Thirta	k1gFnPc4
Seconds	Secondsa	k1gFnPc2
to	ten	k3xDgNnSc4
Mars	Mars	k1gMnSc1
(	(	kIx(
<g/>
Jared	Jared	k1gMnSc1
Leto	Leto	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Green	Green	k2eAgMnSc1d1
Day	Day	k1gMnSc1
(	(	kIx(
<g/>
Billie	Billie	k1gFnSc1
Joe	Joe	k1gMnSc1
Armstrong	Armstrong	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Guns	Guns	k1gInSc1
N	N	kA
<g/>
'	'	kIx"
Roses	Roses	k1gMnSc1
(	(	kIx(
<g/>
Axl	Axl	k1gMnSc1
Rose	Rose	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Megadeth	Megadeth	k1gMnSc1
(	(	kIx(
<g/>
Dave	Dav	k1gInSc5
Mustaine	Mustain	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Korn	Korn	k1gMnSc1
<g/>
,	,	kIx,
Blink-	Blink-	k1gFnSc1
<g/>
182	#num#	k4
<g/>
,	,	kIx,
Slayer	Slayer	k1gMnSc1
<g/>
,	,	kIx,
The	The	k1gMnSc1
Black	Black	k1gMnSc1
Eyed	Eyed	k1gMnSc1
Peas	Peas	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největším	veliký	k2eAgNnSc7d3
narušením	narušení	k1gNnSc7
kalifornské	kalifornský	k2eAgFnSc2d1
kulturní	kulturní	k2eAgFnSc2d1
hegemonie	hegemonie	k1gFnSc2
byla	být	k5eAaImAgFnS
vlna	vlna	k1gFnSc1
grunge	grunge	k1gFnSc1
z	z	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
zdvihla	zdvihnout	k5eAaPmAgFnS
v	v	k7c4
Seattlu	Seattla	k1gMnSc4
<g/>
:	:	kIx,
Nirvana	Nirvan	k1gMnSc4
(	(	kIx(
<g/>
Kurt	Kurt	k1gMnSc1
Cobain	Cobain	k1gMnSc1
<g/>
,	,	kIx,
Dave	Dav	k1gInSc5
Grohl	Grohl	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Alice	Alice	k1gFnSc1
in	in	k?
Chains	Chains	k1gInSc4
<g/>
,	,	kIx,
Pearl	Pearl	k1gInSc4
Jam	jáma	k1gFnPc2
či	či	k8xC
Soundgarden	Soundgardna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silná	silný	k2eAgFnSc1d1
hudební	hudební	k2eAgFnSc1d1
scéna	scéna	k1gFnSc1
je	být	k5eAaImIp3nS
ale	ale	k8xC
také	také	k9
na	na	k7c6
východním	východní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
<g/>
:	:	kIx,
Bon	bon	k1gInSc1
Jovi	Jovi	k1gNnSc1
(	(	kIx(
<g/>
Jon	Jon	k1gFnPc1
Bon	bon	k1gInSc4
Jovi	Jov	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Kiss	Kiss	k1gInSc1
<g/>
,	,	kIx,
Aerosmith	Aerosmith	k1gInSc1
(	(	kIx(
<g/>
Steven	Steven	k2eAgInSc1d1
Tyler	Tyler	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
R.	R.	kA
<g/>
E.	E.	kA
<g/>
M.	M.	kA
<g/>
,	,	kIx,
Ramones	Ramones	k1gMnSc1
<g/>
,	,	kIx,
My	my	k3xPp1nPc1
Chemical	Chemical	k1gMnPc1
Romance	romance	k1gFnSc2
<g/>
,	,	kIx,
The	The	k1gFnSc2
Velvet	Velveta	k1gFnPc2
Underground	underground	k1gInSc1
<g/>
,	,	kIx,
Simon	Simon	k1gMnSc1
&	&	k?
Garfunkel	Garfunkel	k1gMnSc1
(	(	kIx(
<g/>
Paul	Paul	k1gMnSc1
Simon	Simon	k1gMnSc1
<g/>
,	,	kIx,
Art	Art	k1gMnSc1
Garfunkel	Garfunkel	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Pixies	Pixies	k1gInSc1
<g/>
,	,	kIx,
Backstreet	Backstreet	k1gInSc1
Boys	boy	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
amerického	americký	k2eAgNnSc2d1
vnitrozemí	vnitrozemí	k1gNnSc2
je	být	k5eAaImIp3nS
cesta	cesta	k1gFnSc1
ke	k	k7c3
světové	světový	k2eAgFnSc3d1
slávě	sláva	k1gFnSc3
o	o	k7c4
dost	dost	k6eAd1
těžší	těžký	k2eAgNnSc4d2
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
našly	najít	k5eAaPmAgFnP
se	se	k3xPyFc4
i	i	k9
takové	takový	k3xDgInPc1
případy	případ	k1gInPc1
<g/>
:	:	kIx,
skupina	skupina	k1gFnSc1
Evanescence	Evanescence	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
Arkansasu	Arkansas	k1gInSc6
<g/>
,	,	kIx,
metaloví	metalový	k2eAgMnPc1d1
Nine	Nine	k1gNnSc7
Inch	Inch	k1gInSc1
Nails	Nails	k1gInSc1
v	v	k7c6
Ohiu	Ohio	k1gNnSc6
<g/>
,	,	kIx,
Slipknot	Slipknota	k1gFnPc2
v	v	k7c6
Iowě	Iowa	k1gFnSc6
či	či	k8xC
Pantera	panter	k1gMnSc2
v	v	k7c6
Texasu	Texas	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Nejslavnější	slavný	k2eAgFnSc7d3
americkou	americký	k2eAgFnSc7d1
tanečnicí	tanečnice	k1gFnSc7
byla	být	k5eAaImAgFnS
Isadora	Isadora	k1gFnSc1
Duncanová	Duncanová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Kinematografie	kinematografie	k1gFnSc1
</s>
<s>
Marilyn	Marilyn	k1gFnSc1
Monroe	Monroe	k1gFnSc1
</s>
<s>
Americký	americký	k2eAgInSc1d1
filmový	filmový	k2eAgInSc1d1
průmysl	průmysl	k1gInSc1
je	být	k5eAaImIp3nS
nejrozvinutější	rozvinutý	k2eAgInSc1d3
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc7
synonymem	synonymum	k1gNnSc7
je	být	k5eAaImIp3nS
Hollywood	Hollywood	k1gInSc1
<g/>
,	,	kIx,
komplex	komplex	k1gInSc1
filmových	filmový	k2eAgInPc2d1
koncernů	koncern	k1gInPc2
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
–	–	k?
byť	byť	k8xS
existují	existovat	k5eAaImIp3nP
studia	studio	k1gNnPc1
i	i	k8xC
jinde	jinde	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Zcela	zcela	k6eAd1
zvláštní	zvláštní	k2eAgFnSc4d1
pozici	pozice	k1gFnSc4
v	v	k7c6
americké	americký	k2eAgFnSc6d1
i	i	k8xC
globální	globální	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
si	se	k3xPyFc3
vydobyl	vydobýt	k5eAaPmAgMnS
Walt	Walt	k1gInSc4
Disney	Disnea	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
způsobil	způsobit	k5eAaPmAgInS
revoluci	revoluce	k1gFnSc4
v	v	k7c6
dětském	dětský	k2eAgInSc6d1
a	a	k8xC
kresleném	kreslený	k2eAgInSc6d1
filmu	film	k1gInSc6
a	a	k8xC
vytvořil	vytvořit	k5eAaPmAgInS
i	i	k9
vlastní	vlastní	k2eAgNnSc4d1
obchodní	obchodní	k2eAgNnSc4d1
impérium	impérium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Režiséry	režisér	k1gMnPc4
nejslavnějších	slavný	k2eAgInPc2d3
hollywoodských	hollywoodský	k2eAgInPc2d1
blockbusterů	blockbuster	k1gInPc2
byli	být	k5eAaImAgMnP
Steven	Steven	k2eAgMnSc1d1
Spielberg	Spielberg	k1gMnSc1
(	(	kIx(
<g/>
E.T.	E.T.	k1gMnSc1
<g/>
,	,	kIx,
Čelisti	čelist	k1gFnPc1
<g/>
)	)	kIx)
a	a	k8xC
George	Georg	k1gMnSc2
Lucas	Lucasa	k1gFnPc2
(	(	kIx(
<g/>
Hvězdné	hvězdný	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ron	Ron	k1gMnSc1
Howard	Howard	k1gMnSc1
proslul	proslout	k5eAaPmAgMnS
blockbustery	blockbuster	k1gMnPc4
podle	podle	k7c2
knih	kniha	k1gFnPc2
Dana	Dana	k1gFnSc1
Browna	Browna	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
ceněným	ceněný	k2eAgInSc7d1
životopisným	životopisný	k2eAgInSc7d1
snímkem	snímek	k1gInSc7
Čistá	čistý	k2eAgFnSc1d1
duše	duše	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Westerny	western	k1gInPc7
proslul	proslout	k5eAaPmAgMnS
John	John	k1gMnSc1
Ford	ford	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Specialistou	specialista	k1gMnSc7
na	na	k7c4
sci-fi	sci-fi	k1gFnSc4
byl	být	k5eAaImAgInS
Robert	Robert	k1gMnSc1
Zemeckis	Zemeckis	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
kontě	konto	k1gNnSc6
má	mít	k5eAaImIp3nS
ale	ale	k9
i	i	k9
ceněnou	ceněný	k2eAgFnSc4d1
"	"	kIx"
<g/>
kroniku	kronika	k1gFnSc4
Ameriky	Amerika	k1gFnSc2
2	#num#	k4
<g/>
.	.	kIx.
poloviny	polovina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
snímek	snímek	k1gInSc1
Forrest	Forrest	k1gMnSc1
Gump	Gump	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komedie	komedie	k1gFnSc2
byli	být	k5eAaImAgMnP
doménou	doména	k1gFnSc7
George	Georg	k1gMnSc2
Cukora	Cukor	k1gMnSc2
(	(	kIx(
<g/>
Adamovo	Adamův	k2eAgNnSc1d1
žebro	žebro	k1gNnSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
Billyho	Billy	k1gMnSc2
Wildera	Wilder	k1gMnSc2
(	(	kIx(
<g/>
Někdo	někdo	k3yInSc1
to	ten	k3xDgNnSc4
rád	rád	k6eAd1
horké	horký	k2eAgNnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
komediální	komediální	k2eAgFnPc1d1
klasiky	klasika	k1gFnPc1
mají	mít	k5eAaImIp3nP
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
kontě	konto	k1gNnSc6
i	i	k8xC
Woody	Wood	k1gInPc4
Allen	allen	k1gInSc1
(	(	kIx(
<g/>
Annie	Annie	k1gFnSc1
Hallová	Hallová	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Sydney	Sydney	k1gNnSc4
Pollack	Pollacka	k1gFnPc2
(	(	kIx(
<g/>
Tootsie	Tootsie	k1gFnSc1
<g/>
)	)	kIx)
či	či	k8xC
William	William	k1gInSc1
Wyler	Wylra	k1gFnPc2
(	(	kIx(
<g/>
Prázdniny	prázdniny	k1gFnPc1
v	v	k7c6
Římě	Řím	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velké	velký	k2eAgFnSc2d1
mafiánské	mafiánský	k2eAgFnSc2d1
podívané	podívaná	k1gFnSc2
proslavili	proslavit	k5eAaPmAgMnP
Martina	Martin	k2eAgMnSc4d1
Scorseseho	Scorsese	k1gMnSc4
(	(	kIx(
<g/>
Gangy	Ganga	k1gFnSc2
New	New	k1gFnSc2
Yorku	York	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
Francise	Francise	k1gFnSc1
Forda	ford	k1gMnSc2
Coppolu	Coppol	k1gInSc2
(	(	kIx(
<g/>
Kmotr	kmotr	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Akční	akční	k2eAgInSc1d1
film	film	k1gInSc1
se	se	k3xPyFc4
pokusil	pokusit	k5eAaPmAgInS
na	na	k7c4
umění	umění	k1gNnSc4
povýšit	povýšit	k5eAaPmF
Quentin	Quentin	k1gInSc4
Tarantino	Tarantin	k2eAgNnSc1d1
(	(	kIx(
<g/>
Pulp	pulpa	k1gFnPc2
Fiction	Fiction	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Surrealistické	surrealistický	k2eAgFnPc4d1
inspirace	inspirace	k1gFnPc4
ani	ani	k8xC
v	v	k7c6
komerční	komerční	k2eAgFnSc6d1
produkci	produkce	k1gFnSc6
nezapřel	zapřít	k5eNaPmAgMnS
originální	originální	k2eAgNnSc4d1
Tim	Tim	k?
Burton	Burton	k1gInSc4
(	(	kIx(
<g/>
Batman	Batman	k1gMnSc1
<g/>
,	,	kIx,
Karlík	Karlík	k1gMnSc1
a	a	k8xC
továrna	továrna	k1gFnSc1
na	na	k7c4
čokoládu	čokoláda	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Symbolem	symbol	k1gInSc7
uměleckého	umělecký	k2eAgInSc2d1
filmu	film	k1gInSc2
jsou	být	k5eAaImIp3nP
naopak	naopak	k6eAd1
tvůrci	tvůrce	k1gMnPc1
jako	jako	k9
Orson	Orson	k1gMnSc1
Welles	Welles	k1gMnSc1
(	(	kIx(
<g/>
Občan	občan	k1gMnSc1
Kane	kanout	k5eAaImIp3nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Stanley	Stanley	k1gInPc1
Kubrick	Kubrick	k1gInSc1
(	(	kIx(
<g/>
Mechanický	mechanický	k2eAgInSc1d1
pomeranč	pomeranč	k1gInSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
David	David	k1gMnSc1
Lynch	Lynch	k1gMnSc1
(	(	kIx(
<g/>
Modrý	modrý	k2eAgInSc1d1
samet	samet	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umělecké	umělecký	k2eAgFnPc4d1
ambice	ambice	k1gFnPc4
měl	mít	k5eAaImAgMnS
i	i	k9
Elia	Elia	k1gMnSc1
Kazan	Kazan	k1gMnSc1
(	(	kIx(
<g/>
Na	na	k7c4
východ	východ	k1gInSc4
od	od	k7c2
ráje	ráj	k1gInSc2
<g/>
)	)	kIx)
či	či	k8xC
Sidney	Sidnea	k1gMnSc2
Lumet	Lumeta	k1gFnPc2
(	(	kIx(
<g/>
Dvanáct	dvanáct	k4xCc1
rozhněvaných	rozhněvaný	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politický	politický	k2eAgInSc1d1
aktivismus	aktivismus	k1gInSc1
je	být	k5eAaImIp3nS
hlavním	hlavní	k2eAgInSc7d1
motivem	motiv	k1gInSc7
Olivera	Oliver	k1gMnSc2
Stonea	Stoneus	k1gMnSc2
(	(	kIx(
<g/>
Četa	četa	k1gFnSc1
<g/>
,	,	kIx,
JFK	JFK	kA
<g/>
)	)	kIx)
nebo	nebo	k8xC
dokumentaristy	dokumentarista	k1gMnSc2
Michaela	Michael	k1gMnSc2
Moorea	Mooreus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silný	silný	k2eAgInSc4d1
politický	politický	k2eAgInSc4d1
akcent	akcent	k1gInSc4
nesly	nést	k5eAaImAgInP
již	již	k9
snímky	snímek	k1gInPc1
průkopníka	průkopník	k1gMnSc2
amerického	americký	k2eAgInSc2d1
filmu	film	k1gInSc2
D.	D.	kA
W.	W.	kA
Griffitha	Griffitha	k1gFnSc1
(	(	kIx(
<g/>
Zrození	zrození	k1gNnSc1
národa	národ	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Nejznámější	známý	k2eAgInSc1d3
symbol	symbol	k1gInSc1
Hollywoodu	Hollywood	k1gInSc2
</s>
<s>
Hollywood	Hollywood	k1gInSc1
produkuje	produkovat	k5eAaImIp3nS
i	i	k9
nesčetně	sčetně	k6eNd1
hereckých	herecký	k2eAgFnPc2d1
filmových	filmový	k2eAgFnPc2d1
hvězd	hvězda	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
stávají	stávat	k5eAaImIp3nP
globálními	globální	k2eAgFnPc7d1
celebritami	celebrita	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
éře	éra	k1gFnSc6
klasického	klasický	k2eAgInSc2d1
hollywoodu	hollywood	k1gInSc2
(	(	kIx(
<g/>
20	#num#	k4
<g/>
.	.	kIx.
<g/>
-	-	kIx~
<g/>
50	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
)	)	kIx)
to	ten	k3xDgNnSc1
byly	být	k5eAaImAgFnP
mezi	mezi	k7c7
ženami	žena	k1gFnPc7
Mary	Mary	k1gFnSc1
Pickfordová	Pickfordová	k1gFnSc1
<g/>
,	,	kIx,
Lillian	Lillian	k1gInSc1
Gishová	Gishová	k1gFnSc1
<g/>
,	,	kIx,
Němka	Němka	k1gFnSc1
Marlene	Marlen	k1gInSc5
Dietrichová	Dietrichová	k1gFnSc5
<g/>
,	,	kIx,
Joan	Joan	k1gMnSc1
Crawfordová	Crawfordová	k1gFnSc1
<g/>
,	,	kIx,
Grace	Grace	k1gFnSc1
Kellyová	Kellyová	k1gFnSc1
<g/>
,	,	kIx,
Judy	judo	k1gNnPc7
Garlandová	Garlandový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Marilyn	Marilyn	k1gFnSc1
Monroe	Monroe	k1gFnSc1
<g/>
,	,	kIx,
Elizabeth	Elizabeth	k1gFnSc1
Taylorová	Taylorová	k1gFnSc1
<g/>
,	,	kIx,
Katharine	Katharin	k1gInSc5
Hepburnová	Hepburnová	k1gFnSc1
<g/>
,	,	kIx,
Rita	Rita	k1gFnSc1
Hayworthová	Hayworthová	k1gFnSc1
<g/>
,	,	kIx,
Lauren	Laurna	k1gFnPc2
Bacallová	Bacallový	k2eAgFnSc1d1
<g/>
,	,	kIx,
belgicko-britská	belgicko-britský	k2eAgFnSc1d1
herečka	herečka	k1gFnSc1
Audrey	Audrea	k1gFnSc2
Hepburnová	Hepburnový	k2eAgFnSc1d1
či	či	k8xC
dětská	dětský	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
Shirley	Shirlea	k1gFnSc2
Temple	templ	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
mužů	muž	k1gMnPc2
pak	pak	k6eAd1
Buster	Buster	k1gMnSc1
Keaton	Keaton	k1gInSc1
<g/>
,	,	kIx,
Angličan	Angličan	k1gMnSc1
Charlie	Charlie	k1gMnSc1
Chaplin	Chaplin	k2eAgMnSc1d1
<g/>
,	,	kIx,
Harold	Harold	k1gMnSc1
Lloyd	Lloyd	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Wayne	Wayn	k1gInSc5
<g/>
,	,	kIx,
Henry	Henry	k1gMnPc4
Fonda	Fond	k1gMnSc4
<g/>
,	,	kIx,
Gregory	Gregor	k1gMnPc4
Peck	Pecka	k1gFnPc2
<g/>
,	,	kIx,
Fred	Fred	k1gMnSc1
Astaire	Astair	k1gInSc5
<g/>
,	,	kIx,
Cary	car	k1gMnPc4
Grant	grant	k1gInSc1
<g/>
,	,	kIx,
Humphrey	Humphrea	k1gFnPc1
Bogart	Bogart	k1gInSc1
<g/>
,	,	kIx,
James	James	k1gMnSc1
Dean	Dean	k1gMnSc1
<g/>
,	,	kIx,
Marlon	Marlon	k1gInSc4
Brando	Brando	k1gNnSc4
<g/>
,	,	kIx,
Clark	Clark	k1gInSc4
Gable	Gable	k1gFnSc2
<g/>
,	,	kIx,
Bratři	bratr	k1gMnPc1
Marxové	Marxová	k1gFnSc2
<g/>
,	,	kIx,
Gary	Gara	k1gFnPc1
Cooper	Cooper	k1gMnSc1
<g/>
,	,	kIx,
Spencer	Spencer	k1gMnSc1
Tracy	Traca	k1gFnSc2
<g/>
,	,	kIx,
James	James	k1gMnSc1
Stewart	Stewart	k1gMnSc1
nebo	nebo	k8xC
Charlton	Charlton	k1gInSc1
Heston	Heston	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Později	pozdě	k6eAd2
to	ten	k3xDgNnSc1
byly	být	k5eAaImAgFnP
hvězdy	hvězda	k1gFnPc1
jako	jako	k8xS,k8xC
Julia	Julius	k1gMnSc2
Robertsová	Robertsová	k1gFnSc1
<g/>
,	,	kIx,
Sandra	Sandra	k1gFnSc1
Bullocková	Bullocková	k1gFnSc1
<g/>
,	,	kIx,
Meryl	Meryl	k1gInSc1
Streepová	Streepová	k1gFnSc1
<g/>
,	,	kIx,
Barbra	Barbra	k1gFnSc1
Streisandová	Streisandový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Sigourney	Sigournea	k1gFnPc1
Weaver	Weaver	k1gMnSc1
<g/>
,	,	kIx,
Gwyneth	Gwyneth	k1gMnSc1
Paltrowová	Paltrowová	k1gFnSc1
<g/>
,	,	kIx,
Julianne	Juliann	k1gMnSc5
Moore	Moor	k1gMnSc5
<g/>
,	,	kIx,
Whoopi	Whoop	k1gFnSc2
Goldbergová	Goldbergový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Cameron	Cameron	k1gInSc1
Diazová	Diazová	k1gFnSc1
<g/>
,	,	kIx,
Jennifer	Jennifra	k1gFnPc2
Anistonová	Anistonový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Jodie	Jodie	k1gFnSc1
Fosterová	Fosterová	k1gFnSc1
<g/>
,	,	kIx,
Angelina	Angelin	k2eAgFnSc1d1
Jolie	Jolie	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Jennifer	Jennifer	k1gInSc1
Lopezová	Lopezová	k1gFnSc1
<g/>
,	,	kIx,
Robin	robin	k2eAgInSc1d1
Williams	Williams	k1gInSc1
<g/>
,	,	kIx,
Brad	brada	k1gFnPc2
Pitt	Pitta	k1gFnPc2
<g/>
,	,	kIx,
Leonardo	Leonardo	k1gMnSc1
DiCaprio	DiCaprio	k1gMnSc1
<g/>
,	,	kIx,
Arnold	Arnold	k1gMnSc1
Schwarzenegger	Schwarzenegger	k1gMnSc1
<g/>
,	,	kIx,
Bruce	Bruce	k1gFnPc1
Lee	Lea	k1gFnSc3
<g/>
,	,	kIx,
Johnny	Johnna	k1gFnPc1
Depp	Depp	k1gMnSc1
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
Redford	Redford	k1gMnSc1
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
De	De	k?
Niro	Niro	k1gMnSc1
<g/>
,	,	kIx,
Jim	on	k3xPp3gMnPc3
Carrey	Carrea	k1gFnPc1
<g/>
,	,	kIx,
Tom	Tom	k1gMnSc1
Cruise	Cruise	k1gFnSc1
<g/>
,	,	kIx,
Sylvester	Sylvester	k1gInSc1
Stallone	Stallon	k1gInSc5
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
Bruce	Bruce	k1gFnSc1
Willis	Willis	k1gFnSc1
<g/>
,	,	kIx,
Kevin	Kevin	k1gMnSc1
Costner	Costner	k1gMnSc1
<g/>
,	,	kIx,
Tom	Tom	k1gMnSc1
Hanks	Hanks	k1gInSc1
<g/>
,	,	kIx,
Jack	Jack	k1gMnSc1
Nicholson	Nicholson	k1gMnSc1
<g/>
,	,	kIx,
Clint	Clint	k1gMnSc1
Eastwood	Eastwood	k1gInSc1
<g/>
,	,	kIx,
Morgan	morgan	k1gMnSc1
Freeman	Freeman	k1gMnSc1
<g/>
,	,	kIx,
Will	Will	k1gMnSc1
Smith	Smith	k1gMnSc1
<g/>
,	,	kIx,
George	Georg	k1gMnSc2
Clooney	Cloonea	k1gMnSc2
<g/>
,	,	kIx,
Eddie	Eddie	k1gFnSc2
Murphy	Murpha	k1gFnSc2
<g/>
,	,	kIx,
Mel	mlít	k5eAaImRp2nS
Gibson	Gibson	k1gMnSc1
<g/>
,	,	kIx,
Nicolas	Nicolas	k1gMnSc1
Cage	Cag	k1gFnSc2
<g/>
,	,	kIx,
Harrison	Harrison	k1gMnSc1
Ford	ford	k1gInSc1
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
Newman	Newman	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Travolta	Travolta	k1gMnSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
Douglas	Douglas	k1gMnSc1
<g/>
,	,	kIx,
Patrick	Patrick	k1gMnSc1
Swayze	Swayze	k1gFnSc2
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
Gere	Ger	k1gFnSc2
a	a	k8xC
mnozí	mnohý	k2eAgMnPc1d1
jiní	jiný	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Americká	americký	k2eAgFnSc1d1
Akademie	akademie	k1gFnSc1
filmového	filmový	k2eAgNnSc2d1
umění	umění	k1gNnSc2
a	a	k8xC
věd	věda	k1gFnPc2
každoročně	každoročně	k6eAd1
uděluje	udělovat	k5eAaImIp3nS
cenu	cena	k1gFnSc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
všeobecně	všeobecně	k6eAd1
říká	říkat	k5eAaImIp3nS
Oscar	Oscar	k1gInSc1
(	(	kIx(
<g/>
oficiálně	oficiálně	k6eAd1
Cena	cena	k1gFnSc1
Akademie	akademie	k1gFnSc2
–	–	k?
Academy	Academa	k1gFnSc2
Award	Awarda	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velkou	velký	k2eAgFnSc4d1
prestiž	prestiž	k1gFnSc4
má	mít	k5eAaImIp3nS
také	také	k9
Zlatý	zlatý	k2eAgInSc4d1
glóbus	glóbus	k1gInSc4
(	(	kIx(
<g/>
Golden	Goldna	k1gFnPc2
Globe	globus	k1gInSc5
Award	Award	k1gInSc1
<g/>
)	)	kIx)
udělovaný	udělovaný	k2eAgInSc1d1
každý	každý	k3xTgInSc1
rok	rok	k1gInSc1
Asociací	asociace	k1gFnSc7
zahraničních	zahraniční	k2eAgMnPc2d1
novinářů	novinář	k1gMnPc2
v	v	k7c6
Hollywoodu	Hollywood	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k8xS,k8xC
iluzionista	iluzionista	k1gMnSc1
celosvětově	celosvětově	k6eAd1
proslul	proslout	k5eAaPmAgMnS
David	David	k1gMnSc1
Copperfield	Copperfield	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Média	médium	k1gNnPc1
</s>
<s>
Sídlo	sídlo	k1gNnSc1
ABC	ABC	kA
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
</s>
<s>
Mezi	mezi	k7c4
nejvýznamnější	významný	k2eAgNnPc4d3
americká	americký	k2eAgNnPc4d1
média	médium	k1gNnPc4
patří	patřit	k5eAaImIp3nS
tzv.	tzv.	kA
velká	velký	k2eAgFnSc1d1
trojka	trojka	k1gFnSc1
televizních	televizní	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
–	–	k?
NBC	NBC	kA
<g/>
,	,	kIx,
ABC	ABC	kA
a	a	k8xC
CBS	CBS	kA
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
doplňuje	doplňovat	k5eAaImIp3nS
televizní	televizní	k2eAgFnSc1d1
síť	síť	k1gFnSc1
Fox	fox	k1gInSc1
a	a	k8xC
televize	televize	k1gFnSc1
The	The	k1gMnPc2
CW	CW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
z	z	k7c2
televizních	televizní	k2eAgInPc2d1
seriálů	seriál	k1gInPc2
z	z	k7c2
dílny	dílna	k1gFnSc2
těchto	tento	k3xDgFnPc2
stanic	stanice	k1gFnPc2
dosáhlo	dosáhnout	k5eAaPmAgNnS
celosvětové	celosvětový	k2eAgFnPc4d1
proslulosti	proslulost	k1gFnPc4
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
v	v	k7c6
žánru	žánr	k1gInSc6
sitcomu	sitcom	k1gInSc2
(	(	kIx(
<g/>
Simpsonovi	Simpsonův	k2eAgMnPc1d1
<g/>
,	,	kIx,
Přátelé	přítel	k1gMnPc1
<g/>
,	,	kIx,
Teorie	teorie	k1gFnPc1
velkého	velký	k2eAgInSc2d1
třesku	třesk	k1gInSc2
<g/>
,	,	kIx,
Jak	jak	k8xC,k8xS
jsem	být	k5eAaImIp1nS
poznal	poznat	k5eAaPmAgInS
vaši	váš	k3xOp2gFnSc4
matku	matka	k1gFnSc4
<g/>
,	,	kIx,
Dva	dva	k4xCgInPc1
a	a	k8xC
půl	půl	k1xP
chlapa	chlap	k1gMnSc2
<g/>
,	,	kIx,
M	M	kA
<g/>
*	*	kIx~
<g/>
A	A	kA
<g/>
*	*	kIx~
<g/>
S	s	k7c7
<g/>
*	*	kIx~
<g/>
H	H	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stále	stále	k6eAd1
větší	veliký	k2eAgFnSc4d2
úlohu	úloha	k1gFnSc4
hrají	hrát	k5eAaImIp3nP
zpoplatněné	zpoplatněný	k2eAgInPc4d1
televizní	televizní	k2eAgInPc4d1
a	a	k8xC
internetové	internetový	k2eAgInPc4d1
kanály	kanál	k1gInPc4
jako	jako	k8xC,k8xS
HBO	HBO	kA
a	a	k8xC
Netflix	Netflix	k1gInSc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
po	po	k7c6
skromných	skromný	k2eAgInPc6d1
začátcích	začátek	k1gInPc6
vsadily	vsadit	k5eAaPmAgFnP
na	na	k7c4
vlastní	vlastní	k2eAgFnSc4d1
výpravnou	výpravný	k2eAgFnSc4d1
produkci	produkce	k1gFnSc4
(	(	kIx(
<g/>
Hra	hra	k1gFnSc1
o	o	k7c4
trůny	trůn	k1gInPc4
<g/>
,	,	kIx,
Dům	dům	k1gInSc1
z	z	k7c2
karet	kareta	k1gFnPc2
aj.	aj.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významné	významný	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
také	také	k9
zpravodajská	zpravodajský	k2eAgFnSc1d1
kabelová	kabelový	k2eAgFnSc1d1
televizní	televizní	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
CNN	CNN	kA
a	a	k8xC
na	na	k7c4
ekonomické	ekonomický	k2eAgNnSc4d1
zpravodajství	zpravodajství	k1gNnSc4
zaměřená	zaměřený	k2eAgFnSc1d1
televizní	televizní	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
Bloomberg	Bloomberg	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
nejvýznamnějším	významný	k2eAgFnPc3d3
novinám	novina	k1gFnPc3
–	–	k?
i	i	k8xC
v	v	k7c6
celosvětovém	celosvětový	k2eAgNnSc6d1
měřítku	měřítko	k1gNnSc6
–	–	k?
patří	patřit	k5eAaImIp3nS
deník	deník	k1gInSc1
The	The	k1gFnSc2
New	New	k1gMnPc2
York	York	k1gInSc1
Times	Timesa	k1gFnPc2
mediálního	mediální	k2eAgInSc2d1
konglomerátu	konglomerát	k1gInSc2
Time	Tim	k1gFnSc2
Warner	Warnra	k1gFnPc2
a	a	k8xC
také	také	k9
The	The	k1gMnSc1
Wall	Wall	k1gMnSc1
Street	Street	k1gMnSc1
Journal	Journal	k1gMnSc1
<g/>
,	,	kIx,
rovněž	rovněž	k9
vydávaný	vydávaný	k2eAgInSc1d1
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
lze	lze	k6eAd1
jmenovat	jmenovat	k5eAaBmF,k5eAaImF
mj.	mj.	kA
The	The	k1gFnSc1
Washington	Washington	k1gInSc1
Post	post	k1gInSc1
<g/>
,	,	kIx,
Chicago	Chicago	k1gNnSc1
Tribune	tribun	k1gMnSc5
a	a	k8xC
Los	los	k1gMnSc1
Angeles	Angeles	k1gMnSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Značný	značný	k2eAgInSc4d1
význam	význam	k1gInSc4
je	být	k5eAaImIp3nS
přisuzován	přisuzován	k2eAgInSc1d1
také	také	k9
internetovým	internetový	k2eAgFnPc3d1
novinám	novina	k1gFnPc3
The	The	k1gMnPc2
Huffington	Huffington	k1gInSc4
Post	posta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c7
periodickými	periodický	k2eAgInPc7d1
časopisy	časopis	k1gInPc7
vynikají	vynikat	k5eAaImIp3nP
týdeníky	týdeník	k1gInPc7
Time	Time	k1gNnSc2
a	a	k8xC
Newsweek	Newsweky	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
Evropu	Evropa	k1gFnSc4
<g/>
,	,	kIx,
Latinskou	latinský	k2eAgFnSc4d1
Ameriku	Amerika	k1gFnSc4
<g/>
,	,	kIx,
Afriku	Afrika	k1gFnSc4
a	a	k8xC
Blízký	blízký	k2eAgInSc4d1
východ	východ	k1gInSc4
vychází	vycházet	k5eAaImIp3nS
Time	Timus	k1gMnSc5
Europe	Europ	k1gMnSc5
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
153	#num#	k4
<g/>
]	]	kIx)
Time	Tim	k1gInSc2
Asia	Asium	k1gNnSc2
má	mít	k5eAaImIp3nS
sídlo	sídlo	k1gNnSc4
v	v	k7c6
Hongkongu	Hongkong	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Časopis	časopis	k1gInSc1
Newsweek	Newsweek	k6eAd1
vychází	vycházet	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
pouze	pouze	k6eAd1
na	na	k7c6
internetu	internet	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
vědeckých	vědecký	k2eAgInPc2d1
a	a	k8xC
populárně-vědeckých	populárně-vědecký	k2eAgInPc2d1
časopisů	časopis	k1gInPc2
<g/>
,	,	kIx,
například	například	k6eAd1
National	National	k1gMnSc1
Geographic	Geographic	k1gMnSc1
Magazine	Magazin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Kuchyně	kuchyně	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Americká	americký	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Coca-Cola	coca-cola	k1gFnSc1
je	být	k5eAaImIp3nS
americký	americký	k2eAgInSc4d1
symbol	symbol	k1gInSc4
</s>
<s>
Hamburger	hamburger	k1gInSc1
</s>
<s>
Na	na	k7c4
Den	den	k1gInSc4
díkůvzdání	díkůvzdání	k1gNnSc2
většina	většina	k1gFnSc1
Američanů	Američan	k1gMnPc2
jí	jíst	k5eAaImIp3nS
tradiční	tradiční	k2eAgNnPc4d1
jídla	jídlo	k1gNnPc4
z	z	k7c2
dob	doba	k1gFnPc2
osidlování	osidlování	k1gNnSc2
<g/>
,	,	kIx,
k	k	k7c3
nimž	jenž	k3xRgFnPc3
patří	patřit	k5eAaImIp3nS
zejména	zejména	k9
pečený	pečený	k2eAgMnSc1d1
krocan	krocan	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
sladké	sladký	k2eAgFnPc1d1
brambory	brambora	k1gFnPc1
či	či	k8xC
javorový	javorový	k2eAgInSc1d1
sirup	sirup	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velkou	velký	k2eAgFnSc4d1
roli	role	k1gFnSc4
v	v	k7c6
americké	americký	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
také	také	k9
hraje	hrát	k5eAaImIp3nS
jablečný	jablečný	k2eAgInSc1d1
koláč	koláč	k1gInSc1
(	(	kIx(
<g/>
zvaný	zvaný	k2eAgInSc1d1
často	často	k6eAd1
americký	americký	k2eAgInSc1d1
koláč	koláč	k1gInSc1
–	–	k?
american	american	k1gInSc1
pie	pie	k?
<g/>
)	)	kIx)
či	či	k8xC
hovězí	hovězí	k2eAgInSc4d1
steak	steak	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
si	se	k3xPyFc3
Američané	Američan	k1gMnPc1
oblíbili	oblíbit	k5eAaPmAgMnP
hamburgery	hamburger	k1gInPc4
a	a	k8xC
hot	hot	k0
dogy	doga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
nedílnou	dílný	k2eNgFnSc7d1
součástí	součást	k1gFnSc7
americké	americký	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
staly	stát	k5eAaPmAgFnP
fast	fast	k5eAaPmF
food	food	k1gInSc4
restaurace	restaurace	k1gFnSc2
jako	jako	k8xS,k8xC
McDonald	McDonald	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
<g/>
,	,	kIx,
KFC	KFC	kA
či	či	k8xC
Burger	Burger	k1gMnSc1
King	King	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obliba	obliba	k1gFnSc1
rychlého	rychlý	k2eAgNnSc2d1
občerstvení	občerstvení	k1gNnSc2
je	být	k5eAaImIp3nS
však	však	k9
i	i	k9
předmětem	předmět	k1gInSc7
veřejné	veřejný	k2eAgFnSc2d1
kritiky	kritika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podílela	podílet	k5eAaImAgFnS
se	se	k3xPyFc4
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
během	během	k7c2
osmdesátých	osmdesátý	k4xOgNnPc2
a	a	k8xC
devadesátých	devadesátý	k4xOgNnPc2
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
příjem	příjem	k1gInSc1
kalorií	kalorie	k1gFnPc2
Američanů	Američan	k1gMnPc2
zvýšil	zvýšit	k5eAaPmAgMnS
o	o	k7c4
24	#num#	k4
<g/>
%	%	kIx~
<g/>
[	[	kIx(
<g/>
154	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
bývá	bývat	k5eAaImIp3nS
spojována	spojovat	k5eAaImNgFnS
s	s	k7c7
epidemií	epidemie	k1gFnSc7
obezity	obezita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významnou	významný	k2eAgFnSc7d1
je	být	k5eAaImIp3nS
v	v	k7c6
USA	USA	kA
spotřeba	spotřeba	k1gFnSc1
kávy	káva	k1gFnSc2
<g/>
,	,	kIx,
kavárenský	kavárenský	k2eAgInSc1d1
řetězec	řetězec	k1gInSc1
Starbucks	Starbucksa	k1gFnPc2
je	být	k5eAaImIp3nS
největší	veliký	k2eAgFnSc1d3
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
slavnější	slavný	k2eAgFnSc1d2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
kofeinová	kofeinový	k2eAgFnSc1d1
limonáda	limonáda	k1gFnSc1
Coca-Cola	coca-cola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
sladkostí	sladkost	k1gFnPc2
jsou	být	k5eAaImIp3nP
oblíbené	oblíbený	k2eAgInPc1d1
lívance	lívanec	k1gInPc1
polévané	polévaný	k2eAgInPc1d1
sirupem	sirup	k1gInSc7
<g/>
,	,	kIx,
donut	donut	k1gInSc1
(	(	kIx(
<g/>
americká	americký	k2eAgFnSc1d1
verze	verze	k1gFnSc1
koblihy	kobliha	k1gFnSc2
s	s	k7c7
dírou	díra	k1gFnSc7
uprostřed	uprostřed	k7c2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
sušenky	sušenka	k1gFnPc1
cookies	cookies	k1gInSc1
<g/>
,	,	kIx,
dortíky	dortík	k1gInPc1
muffiny	muffin	k2eAgInPc1d1
<g/>
,	,	kIx,
moučník	moučník	k1gInSc1
brownie	brownie	k1gFnSc2
či	či	k8xC
tvarohový	tvarohový	k2eAgInSc4d1
dort	dort	k1gInSc4
cheesecake	cheesecak	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velký	velký	k2eAgInSc1d1
vliv	vliv	k1gInSc1
na	na	k7c4
americkou	americký	k2eAgFnSc4d1
gastronomii	gastronomie	k1gFnSc4
měly	mít	k5eAaImAgFnP
pochopitelně	pochopitelně	k6eAd1
i	i	k8xC
vlny	vlna	k1gFnSc2
přistěhovalců	přistěhovalec	k1gMnPc2
<g/>
,	,	kIx,
Italové	Ital	k1gMnPc1
sebou	se	k3xPyFc7
přivezli	přivézt	k5eAaPmAgMnP
oblibu	obliba	k1gFnSc4
pizzy	pizza	k1gFnSc2
<g/>
,	,	kIx,
přistěhovalci	přistěhovalec	k1gMnPc1
z	z	k7c2
jihu	jih	k1gInSc2
pak	pak	k6eAd1
jídel	jídlo	k1gNnPc2
jako	jako	k8xS,k8xC
burrito	burrita	k1gFnSc5
či	či	k8xC
tacos	tacos	k1gInSc4
a	a	k8xC
velkou	velký	k2eAgFnSc4d1
oblibu	obliba	k1gFnSc4
si	se	k3xPyFc3
získalo	získat	k5eAaPmAgNnS
i	i	k9
jídlo	jídlo	k1gNnSc1
čínské	čínský	k2eAgFnSc2d1
či	či	k8xC
thajské	thajský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nejznámějším	známý	k2eAgFnPc3d3
regionálním	regionální	k2eAgFnPc3d1
variantám	varianta	k1gFnPc3
americké	americký	k2eAgFnSc2d1
kuchyně	kuchyně	k1gFnSc2
patří	patřit	k5eAaImIp3nS
jižanský	jižanský	k2eAgInSc1d1
Tex-Mex	Tex-Mex	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
nejtypičtějším	typický	k2eAgInSc7d3
pokrmem	pokrm	k1gInSc7
je	být	k5eAaImIp3nS
chili	chile	k1gFnSc3
con	con	k?
carne	carnout	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klasickými	klasický	k2eAgFnPc7d1
surovinami	surovina	k1gFnPc7
jsou	být	k5eAaImIp3nP
fazole	fazole	k1gFnSc1
<g/>
,	,	kIx,
čedar	čedar	k1gInSc1
<g/>
,	,	kIx,
hovězí	hovězí	k2eAgFnSc1d1
a	a	k8xC
kukuřičná	kukuřičný	k2eAgFnSc1d1
mouka	mouka	k1gFnSc1
na	na	k7c4
tortilly	tortill	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
155	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Specifickou	specifický	k2eAgFnSc7d1
<g/>
,	,	kIx,
byť	byť	k8xS
mladou	mladá	k1gFnSc7
<g/>
,	,	kIx,
tradicí	tradice	k1gFnSc7
amerického	americký	k2eAgNnSc2d1
stravování	stravování	k1gNnSc2
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
brunch	brunch	k1gInSc1
<g/>
,	,	kIx,
dopolední	dopolední	k2eAgNnSc1d1
jídlo	jídlo	k1gNnSc1
mezi	mezi	k7c7
časem	čas	k1gInSc7
snídaně	snídaně	k1gFnSc2
a	a	k8xC
oběda	oběd	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
brunch	brun	k1gFnPc6
chodí	chodit	k5eAaImIp3nP
Američané	Američan	k1gMnPc1
zejména	zejména	k9
o	o	k7c6
víkendech	víkend	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
většině	většina	k1gFnSc6
restaurací	restaurace	k1gFnPc2
není	být	k5eNaImIp3nS
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
slušné	slušný	k2eAgMnPc4d1
sednout	sednout	k5eAaPmF
si	se	k3xPyFc3
rovnou	rovnou	k6eAd1
ke	k	k7c3
stolu	stol	k1gInSc3
<g/>
,	,	kIx,
na	na	k7c6
uvedení	uvedení	k1gNnSc6
ke	k	k7c3
stolu	stol	k1gInSc3
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
počkat	počkat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Typické	typický	k2eAgInPc1d1
pro	pro	k7c4
americké	americký	k2eAgFnPc4d1
restaurace	restaurace	k1gFnPc4
rovněž	rovněž	k9
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
obsluha	obsluha	k1gFnSc1
je	být	k5eAaImIp3nS
placena	platit	k5eAaImNgFnS
výhradně	výhradně	k6eAd1
ze	z	k7c2
spropitného	spropitné	k1gNnSc2
<g/>
,	,	kIx,
proto	proto	k8xC
je	být	k5eAaImIp3nS
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
obzvláště	obzvláště	k6eAd1
neslušné	slušný	k2eNgNnSc4d1
dýško	dýško	k1gNnSc4
nedat	dat	k5eNaImF,k5eNaPmF,k5eNaBmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Očekávanou	očekávaný	k2eAgFnSc7d1
jeho	jeho	k3xOp3gNnSc1
výší	výše	k1gFnSc7,k1gFnSc7wB
je	být	k5eAaImIp3nS
15-20	15-20	k4
procent	procento	k1gNnPc2
ceny	cena	k1gFnSc2
jídla	jídlo	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
156	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Věda	věda	k1gFnSc1
</s>
<s>
Američané	Američan	k1gMnPc1
na	na	k7c6
Měsíci	měsíc	k1gInSc6
</s>
<s>
Thomas	Thomas	k1gMnSc1
Alva	Alva	k1gMnSc1
Edison	Edison	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
fonograf	fonograf	k1gInSc1
</s>
<s>
Jakýmsi	jakýsi	k3yIgMnSc7
praotcem	praotec	k1gMnSc7
americké	americký	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
byl	být	k5eAaImAgMnS
všestranný	všestranný	k2eAgMnSc1d1
učenec	učenec	k1gMnSc1
Benjamin	Benjamin	k1gMnSc1
Franklin	Franklin	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Americká	americký	k2eAgFnSc1d1
věda	věda	k1gFnSc1
byla	být	k5eAaImAgFnS
dlouho	dlouho	k6eAd1
ve	v	k7c6
stínu	stín	k1gInSc6
evropské	evropský	k2eAgNnSc1d1
<g/>
,	,	kIx,
od	od	k7c2
konce	konec	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
však	však	k9
začala	začít	k5eAaPmAgFnS
prudce	prudko	k6eAd1
rozvíjet	rozvíjet	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Symbolem	symbol	k1gInSc7
této	tento	k3xDgFnSc2
éry	éra	k1gFnSc2
byli	být	k5eAaImAgMnP
vynálezci	vynálezce	k1gMnPc1
jako	jako	k9
Samuel	Samuel	k1gMnSc1
F.	F.	kA
B.	B.	kA
Morse	Morse	k1gMnSc1
(	(	kIx(
<g/>
elektrický	elektrický	k2eAgInSc1d1
telegraf	telegraf	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Thomas	Thomas	k1gMnSc1
Alva	Alva	k1gMnSc1
Edison	Edison	k1gMnSc1
(	(	kIx(
<g/>
fonograf	fonograf	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
Bratři	bratr	k1gMnPc1
Wrightové	Wrightová	k1gFnSc2
<g/>
,	,	kIx,
tvůrci	tvůrce	k1gMnPc1
prvního	první	k4xOgNnSc2
letadla	letadlo	k1gNnSc2
těžšího	těžký	k2eAgNnSc2d2
než	než	k8xS
vzduch	vzduch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obrovskou	obrovský	k2eAgFnSc7d1
posilou	posila	k1gFnSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
vlna	vlna	k1gFnSc1
vědeckých	vědecký	k2eAgMnPc2d1
emigrantů	emigrant	k1gMnPc2
ve	v	k7c6
30	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
z	z	k7c2
Evropy	Evropa	k1gFnSc2
prchali	prchat	k5eAaImAgMnP
před	před	k7c7
nacismem	nacismus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patřili	patřit	k5eAaImAgMnP
k	k	k7c3
nim	on	k3xPp3gInPc3
i	i	k9
lidé	člověk	k1gMnPc1
jako	jako	k8xS,k8xC
Albert	Albert	k1gMnSc1
Einstein	Einstein	k1gMnSc1
<g/>
,	,	kIx,
Albert	Albert	k1gMnSc1
Abraham	Abraham	k1gMnSc1
Michelson	Michelson	k1gMnSc1
<g/>
,	,	kIx,
Enrico	Enrico	k6eAd1
Fermi	Fer	k1gFnPc7
nebo	nebo	k8xC
John	John	k1gMnSc1
von	von	k1gInSc4
Neumann	Neumann	k1gMnSc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
americkou	americký	k2eAgFnSc4d1
vědu	věda	k1gFnSc4
rychle	rychle	k6eAd1
vytáhli	vytáhnout	k5eAaPmAgMnP
na	na	k7c4
špičkovou	špičkový	k2eAgFnSc4d1
úroveň	úroveň	k1gFnSc4
<g/>
,	,	kIx,
podporováni	podporovat	k5eAaImNgMnP
ekonomickou	ekonomický	k2eAgFnSc7d1
silou	síla	k1gFnSc7
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Symbolem	symbol	k1gInSc7
této	tento	k3xDgFnSc2
nové	nový	k2eAgFnSc2d1
síly	síla	k1gFnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
Projekt	projekt	k1gInSc1
Manhattan	Manhattan	k1gInSc1
(	(	kIx(
<g/>
řízený	řízený	k2eAgInSc1d1
Robertem	Robert	k1gMnSc7
Oppenheimerem	Oppenheimer	k1gMnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
dal	dát	k5eAaPmAgMnS
Spojeným	spojený	k2eAgInPc3d1
státům	stát	k1gInPc3
jako	jako	k8xC,k8xS
první	první	k4xOgFnSc3
zemi	zem	k1gFnSc3
na	na	k7c6
světě	svět	k1gInSc6
atomovou	atomový	k2eAgFnSc4d1
bombu	bomba	k1gFnSc4
<g/>
,	,	kIx,
Program	program	k1gInSc4
Apollo	Apollo	k1gNnSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
Američany	Američan	k1gMnPc7
roku	rok	k1gInSc2
1969	#num#	k4
přivedl	přivést	k5eAaPmAgInS
jako	jako	k9
první	první	k4xOgFnSc7
na	na	k7c4
Měsíc	měsíc	k1gInSc4
(	(	kIx(
<g/>
v	v	k7c6
osobě	osoba	k1gFnSc6
Neila	Neil	k1gMnSc2
Armstronga	Armstrong	k1gMnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
či	či	k8xC
masivní	masivní	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
elektroniky	elektronika	k1gFnSc2
<g/>
,	,	kIx,
počítačů	počítač	k1gInPc2
a	a	k8xC
digitálních	digitální	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
symbolem	symbol	k1gInSc7
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
Silicon	Silicon	kA
Valley	Vallea	k1gMnSc2
<g/>
,	,	kIx,
proslulé	proslulý	k2eAgNnSc4d1
kalifornské	kalifornský	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
inovací	inovace	k1gFnPc2
<g/>
,	,	kIx,
či	či	k8xC
Arpanet	Arpanet	k1gMnSc1
<g/>
,	,	kIx,
předchůdce	předchůdce	k1gMnSc1
internetu	internet	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
původně	původně	k6eAd1
vyvinut	vyvinout	k5eAaPmNgInS
pro	pro	k7c4
americkou	americký	k2eAgFnSc4d1
armádu	armáda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
pilířů	pilíř	k1gInPc2
internetu	internet	k1gInSc2
–	–	k?
vyhledávač	vyhledávač	k1gMnSc1
Google	Google	k1gNnSc1
<g/>
,	,	kIx,
největší	veliký	k2eAgFnSc1d3
sociální	sociální	k2eAgFnSc1d1
síť	síť	k1gFnSc1
Facebook	Facebook	k1gInSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
internetová	internetový	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
Wikipedie	Wikipedie	k1gFnSc1
–	–	k?
vznikla	vzniknout	k5eAaPmAgFnS
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pýchou	pýcha	k1gFnSc7
americké	americký	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
je	být	k5eAaImIp3nS
rovněž	rovněž	k9
síť	síť	k1gFnSc4
univerzit	univerzita	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
některé	některý	k3yIgFnPc1
patří	patřit	k5eAaImIp3nS
k	k	k7c3
nejprestižnějším	prestižní	k2eAgNnPc3d3
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
podle	podle	k7c2
žebříčků	žebříček	k1gInPc2
kvality	kvalita	k1gFnSc2
univerzit	univerzita	k1gFnPc2
QS	QS	kA
World	World	k1gMnSc1
University	universita	k1gFnSc2
Ranking	Ranking	k1gInSc1
2019	#num#	k4
je	být	k5eAaImIp3nS
mezi	mezi	k7c7
prvními	první	k4xOgInPc7
dvaceti	dvacet	k4xCc7
nejlepšími	dobrý	k2eAgFnPc7d3
univerzitami	univerzita	k1gFnPc7
světa	svět	k1gInSc2
jedenáct	jedenáct	k4xCc4
amerických	americký	k2eAgInPc2d1
<g/>
:	:	kIx,
Massachusettský	massachusettský	k2eAgInSc1d1
technologický	technologický	k2eAgInSc1d1
institut	institut	k1gInSc1
<g/>
,	,	kIx,
Stanfordova	Stanfordův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
Harvardova	Harvardův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
Kalifornský	kalifornský	k2eAgInSc1d1
technologický	technologický	k2eAgInSc1d1
institut	institut	k1gInSc1
<g/>
,	,	kIx,
Chicagská	chicagský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
Princetonská	Princetonský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
Cornellova	Cornellův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
Yaleova	Yaleův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
Kolumbijská	kolumbijský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
Pensylvánská	pensylvánský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
a	a	k8xC
Michiganská	michiganský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
157	#num#	k4
<g/>
]	]	kIx)
Harvard	Harvard	k1gInSc1
drží	držet	k5eAaImIp3nS
rekord	rekord	k1gInSc4
v	v	k7c6
počtu	počet	k1gInSc6
laureátů	laureát	k1gMnPc2
Nobelových	Nobelových	k2eAgFnPc2d1
cen	cena	k1gFnPc2
<g/>
,	,	kIx,
s	s	k7c7
velkým	velký	k2eAgInSc7d1
náskokem	náskok	k1gInSc7
před	před	k7c4
britskou	britský	k2eAgFnSc4d1
Cambridge	Cambridge	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
rodilým	rodilý	k2eAgMnSc7d1
Američanem	Američan	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
získal	získat	k5eAaPmAgMnS
Nobelovu	Nobelův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
za	za	k7c4
vědu	věda	k1gFnSc4
byl	být	k5eAaImAgMnS
chemik	chemik	k1gMnSc1
Theodore	Theodor	k1gMnSc5
William	William	k1gInSc4
Richards	Richards	k1gInSc1
(	(	kIx(
<g/>
1914	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Linus	Linus	k1gMnSc1
Pauling	Pauling	k1gInSc4
obdržel	obdržet	k5eAaPmAgMnS
Nobelovu	Nobelův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
za	za	k7c4
chemii	chemie	k1gFnSc4
i	i	k8xC
Nobelovu	Nobelův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
míru	mír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
John	John	k1gMnSc1
Bardeen	Bardeen	k1gInSc4
je	být	k5eAaImIp3nS
jediným	jediný	k2eAgMnSc7d1
vědcem	vědec	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
získal	získat	k5eAaPmAgMnS
Nobelovu	Nobelův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
oboru	obor	k1gInSc6
dvakrát	dvakrát	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
byla	být	k5eAaImAgFnS
za	za	k7c4
průlomový	průlomový	k2eAgInSc4d1
objev	objev	k1gInSc4
transistoru	transistor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
z	z	k7c2
klíčových	klíčový	k2eAgInPc2d1
objevů	objev	k1gInPc2
fyziky	fyzika	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
že	že	k8xS
elektromagnetické	elektromagnetický	k2eAgNnSc1d1
záření	záření	k1gNnSc1
má	mít	k5eAaImIp3nS
jak	jak	k6eAd1
vlnovou	vlnový	k2eAgFnSc7d1
<g/>
,	,	kIx,
tak	tak	k6eAd1
částicovou	částicový	k2eAgFnSc4d1
povahu	povaha	k1gFnSc4
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
na	na	k7c6
svém	svůj	k3xOyFgNnSc6
kontě	konto	k1gNnSc6
Arthur	Arthura	k1gFnPc2
Holly	Holla	k1gFnSc2
Compton	Compton	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
ikonám	ikona	k1gFnPc3
americké	americký	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
patří	patřit	k5eAaImIp3nS
též	též	k9
vynálezce	vynálezce	k1gMnSc1
cyklotronu	cyklotron	k1gInSc2
Ernest	Ernest	k1gMnSc1
Lawrence	Lawrence	k1gFnSc1
<g/>
,	,	kIx,
objevitel	objevitel	k1gMnSc1
vakcíny	vakcína	k1gFnSc2
proti	proti	k7c3
dětské	dětský	k2eAgFnSc3d1
obrně	obrna	k1gFnSc3
Jonas	Jonas	k1gMnSc1
Salk	Salk	k1gMnSc1
<g/>
,	,	kIx,
známý	známý	k2eAgMnSc1d1
popularizátor	popularizátor	k1gMnSc1
vědy	věda	k1gFnSc2
Richard	Richard	k1gMnSc1
Feynman	Feynman	k1gMnSc1
<g/>
,	,	kIx,
spoluobjevitel	spoluobjevitel	k1gMnSc1
DNA	dno	k1gNnSc2
James	James	k1gMnSc1
Dewey	Dewea	k1gFnSc2
Watson	Watson	k1gMnSc1
<g/>
,	,	kIx,
vynálezce	vynálezce	k1gMnSc1
počítačové	počítačový	k2eAgFnSc2d1
myši	myš	k1gFnSc2
Douglas	Douglas	k1gMnSc1
Engelbart	Engelbart	k1gInSc1
či	či	k8xC
astronomové	astronom	k1gMnPc1
Edwin	Edwin	k1gMnSc1
Hubble	Hubble	k1gMnSc1
a	a	k8xC
Carl	Carl	k1gMnSc1
Sagan	Sagan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matematik	matematik	k1gMnSc1
Norbert	Norbert	k1gMnSc1
Wiener	Wiener	k1gMnSc1
založil	založit	k5eAaPmAgMnS
kybernetiku	kybernetika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
nejslavnějším	slavný	k2eAgMnPc3d3
americkým	americký	k2eAgMnPc3d1
informatikům	informatik	k1gMnPc3
patří	patřit	k5eAaImIp3nP
Donald	Donald	k1gMnSc1
Knuth	Knuth	k1gMnSc1
<g/>
,	,	kIx,
tvůrci	tvůrce	k1gMnPc1
programovacího	programovací	k2eAgInSc2d1
jazyka	jazyk	k1gInSc2
C	C	kA
Ken	Ken	k1gMnSc1
Thompson	Thompson	k1gMnSc1
a	a	k8xC
Dennis	Dennis	k1gFnSc1
Ritchie	Ritchie	k1gFnSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
„	„	k?
<g/>
otec	otec	k1gMnSc1
internetu	internet	k1gInSc2
<g/>
“	“	k?
Vint	vint	k1gInSc1
Cerf	Cerf	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
vytvořil	vytvořit	k5eAaPmAgInS
komunikační	komunikační	k2eAgInSc4d1
protokol	protokol	k1gInSc4
TCP	TCP	kA
<g/>
/	/	kIx~
<g/>
IP	IP	kA
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
je	být	k5eAaImIp3nS
celosvětová	celosvětový	k2eAgFnSc1d1
síť	síť	k1gFnSc1
vystavěna	vystavěn	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Pilíře	pilíř	k1gInPc1
americké	americký	k2eAgFnSc2d1
filozofie	filozofie	k1gFnSc2
a	a	k8xC
sociálních	sociální	k2eAgFnPc2d1
věd	věda	k1gFnPc2
stavěli	stavět	k5eAaImAgMnP
Henry	Henry	k1gMnSc1
David	David	k1gMnSc1
Thoreau	Thoreaus	k1gInSc2
či	či	k8xC
William	William	k1gInSc4
James	James	k1gMnSc1
<g/>
,	,	kIx,
Charles	Charles	k1gMnSc1
Sanders	Sandersa	k1gFnPc2
Peirce	Peirce	k1gMnSc1
a	a	k8xC
John	John	k1gMnSc1
Dewey	Dewea	k1gFnSc2
<g/>
,	,	kIx,
nejvýznamnější	významný	k2eAgMnPc1d3
představitelé	představitel	k1gMnPc1
amerického	americký	k2eAgInSc2d1
pragmatismu	pragmatismus	k1gInSc2
<g/>
,	,	kIx,
prvního	první	k4xOgMnSc2
ryze	ryze	k6eAd1
amerického	americký	k2eAgInSc2d1
filozofického	filozofický	k2eAgInSc2d1
směru	směr	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
hluboce	hluboko	k6eAd1
ovlivnil	ovlivnit	k5eAaPmAgInS
celou	celý	k2eAgFnSc4d1
americkou	americký	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
vlivnými	vlivný	k2eAgMnPc7d1
autory	autor	k1gMnPc7
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
psychologové	psycholog	k1gMnPc1
Erich	Erich	k1gMnSc1
Fromm	Fromm	k1gMnSc1
<g/>
,	,	kIx,
Abraham	Abraham	k1gMnSc1
Maslow	Maslow	k1gMnSc1
<g/>
,	,	kIx,
Carl	Carl	k1gMnSc1
Rogers	Rogers	k1gInSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Watson	Watson	k1gMnSc1
<g/>
,	,	kIx,
Edward	Edward	k1gMnSc1
Thorndike	Thorndik	k1gFnSc2
či	či	k8xC
nejcitovanější	citovaný	k2eAgMnSc1d3
psycholog	psycholog	k1gMnSc1
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Burrhus	Burrhus	k1gMnSc1
Frederic	Frederic	k1gMnSc1
Skinner	Skinner	k1gMnSc1
<g/>
,	,	kIx,
sociologové	sociolog	k1gMnPc1
Erving	Erving	k1gInSc4
Goffman	Goffman	k1gMnSc1
a	a	k8xC
Talcott	Talcott	k2eAgInSc1d1
Parsons	Parsons	k1gInSc1
<g/>
,	,	kIx,
filozof	filozof	k1gMnSc1
vědy	věda	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
Thomas	Thomas	k1gMnSc1
Kuhn	Kuhn	k1gMnSc1
<g/>
,	,	kIx,
genderová	genderový	k2eAgFnSc1d1
teoretička	teoretička	k1gFnSc1
Judith	Judith	k1gInSc1
Butlerová	Butlerová	k1gFnSc1
<g/>
,	,	kIx,
političtí	politický	k2eAgMnPc1d1
filozofové	filozof	k1gMnPc1
John	John	k1gMnSc1
Rawls	Rawls	k1gInSc1
<g/>
,	,	kIx,
Samuel	Samuel	k1gMnSc1
Huntington	Huntington	k1gInSc1
(	(	kIx(
<g/>
autor	autor	k1gMnSc1
slavného	slavný	k2eAgInSc2d1
Střetu	střet	k1gInSc2
civilizací	civilizace	k1gFnSc7
<g/>
)	)	kIx)
a	a	k8xC
Francis	Francis	k1gFnSc1
Fukuyama	Fukuyama	k1gFnSc1
(	(	kIx(
<g/>
autor	autor	k1gMnSc1
neméně	málo	k6eNd2
slavného	slavný	k2eAgInSc2d1
eseje	esej	k1gInSc2
Konec	konec	k1gInSc1
dějin	dějiny	k1gFnPc2
a	a	k8xC
poslední	poslední	k2eAgMnSc1d1
člověk	člověk	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
filozof	filozof	k1gMnSc1
jazyka	jazyk	k1gInSc2
Hilary	Hilara	k1gFnSc2
Putnam	Putnam	k1gInSc1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
sexuologie	sexuologie	k1gFnSc2
Alfred	Alfred	k1gMnSc1
Kinsey	Kinsea	k1gFnSc2
<g/>
,	,	kIx,
antropologové	antropolog	k1gMnPc1
Margaret	Margareta	k1gFnPc2
Meadová	Meadová	k1gFnSc1
a	a	k8xC
Clifford	Clifford	k1gMnSc1
Geertz	Geertz	k1gMnSc1
či	či	k8xC
lingvista	lingvista	k1gMnSc1
Noam	Noam	k1gMnSc1
Chomsky	Chomsky	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zcela	zcela	k6eAd1
mimořádné	mimořádný	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
mají	mít	k5eAaImIp3nP
Američané	Američan	k1gMnPc1
v	v	k7c6
ekonomii	ekonomie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
začátku	začátek	k1gInSc2
udílení	udílení	k1gNnSc2
Nobelových	Nobelových	k2eAgFnPc2d1
cen	cena	k1gFnPc2
za	za	k7c4
ekonomii	ekonomie	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
bylo	být	k5eAaImAgNnS
jen	jen	k6eAd1
třináct	třináct	k4xCc1
ročníků	ročník	k1gInPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
cenu	cena	k1gFnSc4
nepřebíral	přebírat	k5eNaImAgMnS
některý	některý	k3yIgMnSc1
z	z	k7c2
Američanů	Američan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgInSc4d3
vliv	vliv	k1gInSc4
na	na	k7c4
společnost	společnost	k1gFnSc4
měli	mít	k5eAaImAgMnP
z	z	k7c2
mnoha	mnoho	k4c2
laureátů	laureát	k1gMnPc2
otec	otec	k1gMnSc1
neoliberalismu	neoliberalismus	k1gInSc2
Milton	Milton	k1gInSc1
Friedman	Friedman	k1gMnSc1
<g/>
,	,	kIx,
klíčoví	klíčový	k2eAgMnPc1d1
představitelé	představitel	k1gMnPc1
neokeynesyánského	okeynesyánský	k2eNgInSc2d1
směru	směr	k1gInSc2
Joseph	Joseph	k1gMnSc1
Stiglitz	Stiglitz	k1gMnSc1
a	a	k8xC
Paul	Paul	k1gMnSc1
Krugman	Krugman	k1gMnSc1
<g/>
,	,	kIx,
představitel	představitel	k1gMnSc1
teorie	teorie	k1gFnSc2
her	hra	k1gFnPc2
John	John	k1gMnSc1
Forbes	forbes	k1gInSc4
Nash	Nash	k1gMnSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc1
proslavil	proslavit	k5eAaPmAgMnS
film	film	k1gInSc4
Čistá	čistý	k2eAgFnSc1d1
duše	duše	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
nesl	nést	k5eAaImAgMnS
při	při	k7c6
bádání	bádání	k1gNnSc6
břímě	břímě	k1gNnSc4
paranoidní	paranoidní	k2eAgFnSc2d1
psychózy	psychóza	k1gFnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
Paul	Paul	k1gMnSc1
Samuelson	Samuelson	k1gMnSc1
<g/>
,	,	kIx,
z	z	k7c2
jehož	jehož	k3xOyRp3gFnSc2
učebnice	učebnice	k1gFnSc2
se	se	k3xPyFc4
již	již	k6eAd1
mnoho	mnoho	k4c4
let	léto	k1gNnPc2
učí	učit	k5eAaImIp3nP
tisíce	tisíc	k4xCgInPc1
studentů	student	k1gMnPc2
ekonomie	ekonomie	k1gFnSc2
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
obohacení	obohacení	k1gNnSc6
ekonomie	ekonomie	k1gFnSc2
analýzou	analýza	k1gFnSc7
netržního	tržní	k2eNgNnSc2d1
chování	chování	k1gNnSc2
usiloval	usilovat	k5eAaImAgInS
Gary	Gar	k2eAgFnPc4d1
Stanley	Stanlea	k1gFnPc4
Becker	Becker	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
psychologií	psychologie	k1gFnSc7
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgInS
ekonomii	ekonomie	k1gFnSc3
propojit	propojit	k5eAaPmF
Herbert	Herbert	k1gMnSc1
A.	A.	kA
Simon	Simon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
jediná	jediný	k2eAgFnSc1d1
žena	žena	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
Nobelovu	Nobelův	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
za	za	k7c4
ekonomii	ekonomie	k1gFnSc4
získala	získat	k5eAaPmAgFnS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
Američanka	Američanka	k1gFnSc1
<g/>
:	:	kIx,
Elinor	Elinor	k1gInSc1
Ostromová	Ostromový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Státní	státní	k2eAgInPc1d1
svátky	svátek	k1gInPc1
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
</s>
<s>
Oslavy	oslava	k1gFnPc4
Dne	den	k1gInSc2
nezávislosti	nezávislost	k1gFnSc2
ve	v	k7c6
Washingtonu	Washington	k1gInSc6
</s>
<s>
Datum	datum	k1gNnSc1
2020	#num#	k4
</s>
<s>
Český	český	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Anglický	anglický	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Poznámka	poznámka	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
</s>
<s>
Nový	nový	k2eAgInSc1d1
rok	rok	k1gInSc1
</s>
<s>
New	New	k?
Year	Year	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Day	Day	k1gFnSc7
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
</s>
<s>
Den	den	k1gInSc1
Martina	Martin	k1gMnSc2
Luthera	Luther	k1gMnSc2
Kinga	King	k1gMnSc2
</s>
<s>
Martin	Martin	k1gMnSc1
Luther	Luthra	k1gFnPc2
King	King	k1gMnSc1
<g/>
,	,	kIx,
Jr	Jr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Day	Day	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
pondělí	pondělí	k1gNnSc4
v	v	k7c6
lednu	leden	k1gInSc6
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
</s>
<s>
Den	den	k1gInSc1
prezidentů	prezident	k1gMnPc2
</s>
<s>
Presidents	Presidents	k1gInSc1
<g/>
'	'	kIx"
Day	Day	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
pondělí	pondělí	k1gNnSc4
v	v	k7c6
únoru	únor	k1gInSc6
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
</s>
<s>
Den	den	k1gInSc1
obětí	oběť	k1gFnPc2
války	válka	k1gFnSc2
</s>
<s>
Memorial	Memorial	k1gMnSc1
Day	Day	k1gMnSc1
</s>
<s>
poslední	poslední	k2eAgNnSc4d1
pondělí	pondělí	k1gNnSc4
v	v	k7c6
květnu	květen	k1gInSc6
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
</s>
<s>
Den	den	k1gInSc1
nezávislosti	nezávislost	k1gFnSc2
</s>
<s>
Independence	Independence	k1gFnSc1
Day	Day	k1gFnSc2
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
</s>
<s>
Den	den	k1gInSc1
práce	práce	k1gFnSc2
</s>
<s>
Labor	Labor	k1gMnSc1
Day	Day	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
pondělí	pondělí	k1gNnSc4
v	v	k7c6
září	září	k1gNnSc6
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
</s>
<s>
Den	den	k1gInSc1
Kryštofa	Kryštof	k1gMnSc2
Kolumba	Kolumbus	k1gMnSc2
</s>
<s>
Columbus	Columbus	k1gMnSc1
Day	Day	k1gMnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
pondělí	pondělí	k1gNnSc4
v	v	k7c6
říjnu	říjen	k1gInSc6
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
</s>
<s>
Den	den	k1gInSc1
válečných	válečný	k2eAgMnPc2d1
veteránů	veterán	k1gMnPc2
</s>
<s>
Veterans	Veterans	k1gInSc1
Day	Day	k1gFnSc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
</s>
<s>
Den	den	k1gInSc1
díkůvzdání	díkůvzdání	k1gNnSc2
</s>
<s>
Thanksgiving	Thanksgiving	k1gInSc1
Day	Day	k1gFnSc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
čtvrtek	čtvrtek	k1gInSc4
v	v	k7c6
listopadu	listopad	k1gInSc6
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
</s>
<s>
Vánoce	Vánoce	k1gFnPc1
</s>
<s>
Christmas	Christmas	k1gMnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
protestanti	protestant	k1gMnPc1
kladou	klást	k5eAaImIp3nP
na	na	k7c4
Bibli	bible	k1gFnSc4
a	a	k8xC
její	její	k3xOp3gInSc1
text	text	k1gInSc1
enormní	enormní	k2eAgInSc4d1
důraz	důraz	k1gInSc4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
North	North	k1gMnSc1
America	America	k1gMnSc1
::	::	k?
United	United	k1gMnSc1
States	States	k1gMnSc1
—	—	k?
The	The	k1gMnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
-	-	kIx~
Central	Central	k1gFnSc1
Intelligence	Intelligenec	k1gMnSc2
Agency	Agenca	k1gMnSc2
<g/>
.	.	kIx.
www.cia.gov	www.cia.gov	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-09-20	2020-09-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Universum	universum	k1gNnSc1
<g/>
,	,	kIx,
všeobecná	všeobecný	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
Euromedia	Euromedium	k1gNnPc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
646	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
207	#num#	k4
<g/>
-	-	kIx~
<g/>
1071	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
31	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
LIŠČÁK	LIŠČÁK	kA
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
BOHÁČ	Boháč	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jména	jméno	k1gNnPc4
států	stát	k1gInPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnPc2
územních	územní	k2eAgFnPc2d1
částí	část	k1gFnPc2
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
rozšířené	rozšířený	k2eAgNnSc1d1
a	a	k8xC
přepracované	přepracovaný	k2eAgNnSc1d1
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
úřad	úřad	k1gInSc1
zeměměřický	zeměměřický	k2eAgInSc1d1
a	a	k8xC
katastrální	katastrální	k2eAgMnSc1d1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
.	.	kIx.
111	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86918	#num#	k4
<g/>
-	-	kIx~
<g/>
57	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
70	#num#	k4
-	-	kIx~
75	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Státy	stát	k1gInPc1
podle	podle	k7c2
rozlohy	rozloha	k1gFnSc2
<g/>
.	.	kIx.
www.zemepis.com	www.zemepis.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Státy	stát	k1gInPc1
podle	podle	k7c2
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
www.zemepis.com	www.zemepis.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
World	World	k1gInSc1
GDP	GDP	kA
Ranking	Ranking	k1gInSc4
2019	#num#	k4
-	-	kIx~
StatisticsTimes	StatisticsTimesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
statisticstimes	statisticstimes	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Countries	Countries	k1gInSc1
by	by	kYmCp3nS
GDP	GDP	kA
(	(	kIx(
<g/>
PPP	PPP	kA
<g/>
)	)	kIx)
2018	#num#	k4
-	-	kIx~
StatisticsTimes	StatisticsTimesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
statisticstimes	statisticstimes	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
The	The	k1gFnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
—	—	k?
Central	Central	k1gMnSc2
Intelligence	Intelligenec	k1gMnSc2
Agency	Agenca	k1gMnSc2
<g/>
.	.	kIx.
www.cia.gov	www.cia.gov	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Countries	Countries	k1gInSc1
by	by	kYmCp3nS
GDP	GDP	kA
(	(	kIx(
<g/>
nominal	nominat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
)	)	kIx)
per	prát	k5eAaImRp2nS
capita	capitum	k1gNnPc4
2018	#num#	k4
-	-	kIx~
StatisticsTimes	StatisticsTimesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
statisticstimes	statisticstimes	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Countries	Countries	k1gInSc1
by	by	kYmCp3nS
GDP	GDP	kA
(	(	kIx(
<g/>
PPP	PPP	kA
<g/>
)	)	kIx)
per	pero	k1gNnPc2
capita	capita	k1gFnSc1
2018	#num#	k4
-	-	kIx~
StatisticsTimes	StatisticsTimesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
statisticstimes	statisticstimes	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Human	Human	k1gInSc1
Development	Development	k1gMnSc1
Reports	Reports	k1gInSc1
<g/>
.	.	kIx.
hdr	hdr	k?
<g/>
.	.	kIx.
<g/>
undp	undp	k1gMnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Podíl	podíl	k1gInSc1
dolaru	dolar	k1gInSc2
v	v	k7c6
globálních	globální	k2eAgFnPc6d1
devizových	devizový	k2eAgFnPc6d1
rezervách	rezerva	k1gFnPc6
klesá	klesat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdůležitější	důležitý	k2eAgFnSc1d3
rezervní	rezervní	k2eAgFnSc1d1
měna	měna	k1gFnSc1
na	na	k7c6
světě	svět	k1gInSc6
oslabila	oslabit	k5eAaPmAgFnS
už	už	k6eAd1
popáté	popáté	k4xO
za	za	k7c7
sebou	se	k3xPyFc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-07-02	2018-07-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
25	#num#	k4
nejhodnotnějších	hodnotný	k2eAgFnPc2d3
firem	firma	k1gFnPc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amerika	Amerika	k1gFnSc1
vede	vést	k5eAaImIp3nS
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
posiluje	posilovat	k5eAaImIp3nS
a	a	k8xC
Evropa	Evropa	k1gFnSc1
ztrácí	ztrácet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Forbes	forbes	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-08-03	2015-08-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Česká	český	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
je	být	k5eAaImIp3nS
31	#num#	k4
<g/>
.	.	kIx.
nejsilnější	silný	k2eAgFnSc2d3
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
trojice	trojice	k1gFnSc1
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
USA	USA	kA
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
a	a	k8xC
Čína	Čína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
HlídacíPes	HlídacíPes	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-05-21	2017-05-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
REVUE	revue	k1gFnPc1
PRO	pro	k7c4
MÉDIA	médium	k1gNnPc4
č.	č.	k?
4	#num#	k4
–	–	k?
Herbert	Herbert	k1gMnSc1
I.	I.	kA
Schiller	Schiller	k1gMnSc1
–	–	k?
Mass	Mass	k1gInSc1
Communications	Communications	k1gInSc1
and	and	k?
American	American	k1gInSc1
Empire	empir	k1gInSc5
<g/>
.	.	kIx.
rpm	rpm	k?
<g/>
.	.	kIx.
<g/>
fss	fss	k?
<g/>
.	.	kIx.
<g/>
muni	muni	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://politickymarketing.com/glossary/amerikanizace	http://politickymarketing.com/glossary/amerikanizace	k1gFnSc1
<g/>
↑	↑	k?
Amerigo	Amerigo	k6eAd1
Vespucci	Vespucce	k1gMnPc1
-	-	kIx~
skutečný	skutečný	k2eAgMnSc1d1
objevitel	objevitel	k1gMnSc1
Nového	Nového	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
HedvabnaStezka	HedvabnaStezka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
The	The	k1gMnSc1
forgotten	forgotten	k2eAgMnSc1d1
Irishman	Irishman	k1gMnSc1
who	who	k?
named	named	k1gMnSc1
the	the	k?
“	“	k?
<g/>
United	United	k1gMnSc1
States	States	k1gMnSc1
of	of	k?
America	America	k1gMnSc1
<g/>
”	”	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
IrishCentral	IrishCentral	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-03-30	2017-03-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SAFIRE	SAFIRE	kA
<g/>
,	,	kIx,
William	William	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
No	no	k9
Uncertain	Uncertain	k2eAgInSc1d1
Terms	Terms	k1gInSc1
<g/>
:	:	kIx,
More	mor	k1gInSc5
Writing	Writing	k1gInSc1
from	from	k1gMnSc1
the	the	k?
Popular	Popular	k1gMnSc1
"	"	kIx"
<g/>
On	on	k3xPp3gMnSc1
Language	language	k1gFnSc1
<g/>
"	"	kIx"
Column	Column	k1gInSc1
in	in	k?
The	The	k1gFnSc2
New	New	k1gFnPc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
Magazine	Magazin	k1gInSc5
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Simon	Simon	k1gMnSc1
and	and	k?
Schuster	Schuster	k1gMnSc1
382	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9780743249553	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
nBeqgG	nBeqgG	k?
<g/>
1	#num#	k4
<g/>
aqUkC	aqUkC	k?
<g/>
.	.	kIx.
↑	↑	k?
NIX	NIX	kA
<g/>
,	,	kIx,
Elizabeth	Elizabeth	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
How	How	k1gFnSc1
did	did	k?
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
D.C.	D.C.	k1gFnSc1
<g/>
,	,	kIx,
get	get	k?
its	its	k?
name	name	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
HISTORY	HISTORY	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Language	language	k1gFnPc4
Log	log	kA
<g/>
:	:	kIx,
Life	Life	k1gInSc1
in	in	k?
these	these	k1gFnPc1
<g/>
,	,	kIx,
uh	uh	k0
<g/>
,	,	kIx,
this	this	k6eAd1
United	United	k1gMnSc1
States	States	k1gMnSc1
<g/>
.	.	kIx.
itre	itre	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cis	cis	k1gNnSc1
<g/>
.	.	kIx.
<g/>
upenn	upenn	k1gNnSc1
<g/>
.	.	kIx.
<g/>
edu	edu	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kdo	kdo	k3yRnSc1,k3yInSc1,k3yQnSc1
byl	být	k5eAaImAgMnS
první	první	k4xOgMnSc1
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Nové	Nové	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
<g/>
:	:	kIx,
První	první	k4xOgMnPc1
obyvatelé	obyvatel	k1gMnPc1
Ameriky	Amerika	k1gFnSc2
nepřišli	přijít	k5eNaPmAgMnP
pěšky	pěšky	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
připluli	připlout	k5eAaPmAgMnP
v	v	k7c6
kánoích	kánoe	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gFnSc1
Geographic	Geographice	k1gInPc2
Česko	Česko	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Nový	nový	k2eAgInSc1d1
objev	objev	k1gInSc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
žili	žít	k5eAaImAgMnP
v	v	k7c6
USA	USA	kA
možná	možná	k9
už	už	k6eAd1
před	před	k7c7
22	#num#	k4
tisíci	tisíc	k4xCgInPc7
lety	léto	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-08-13	2014-08-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Starobylé	starobylý	k2eAgNnSc4d1
velkoměsto	velkoměsto	k1gNnSc4
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
i	i	k9
dnes	dnes	k6eAd1
ohromuje	ohromovat	k5eAaImIp3nS
svou	svůj	k3xOyFgFnSc7
velkolepostí	velkolepost	k1gFnSc7
<g/>
.	.	kIx.
refresher	refreshra	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Předkolumbovská	předkolumbovský	k2eAgFnSc1d1
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
–	–	k?
puebla	puebnout	k5eAaPmAgFnS
a	a	k8xC
experimenty	experiment	k1gInPc1
s	s	k7c7
civilizací	civilizace	k1gFnSc7
<g/>
.	.	kIx.
www.scienceworld.cz	www.scienceworld.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Monumental	Monumental	k1gMnSc1
Earthworks	Earthworksa	k1gFnPc2
of	of	k?
Poverty	Povert	k1gInPc1
Point	pointa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
UNESCO	Unesco	k1gNnSc1
World	Worlda	k1gFnPc2
Heritage	Heritage	k1gNnSc2
Centre	centr	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Irokézská	irokézský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
<g/>
,	,	kIx,
kmenový	kmenový	k2eAgInSc1d1
svaz	svaz	k1gInSc1
irokézských	irokézský	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
-	-	kIx~
CoJeCo	CoJeCo	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
-	-	kIx~
Vaše	váš	k3xOp2gFnSc1
encyklopedie	encyklopedie	k1gFnSc1
<g/>
.	.	kIx.
www.cojeco.cz	www.cojeco.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Perdue	Perdue	k1gInSc1
<g/>
,	,	kIx,
Theda	Theda	k1gFnSc1
<g/>
;	;	kIx,
Green	Green	k1gInSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
D.	D.	kA
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Columbia	Columbia	k1gFnSc1
Guide	Guid	k1gInSc5
to	ten	k3xDgNnSc1
American	American	k1gMnSc1
Indians	Indiansa	k1gFnPc2
of	of	k?
the	the	k?
Southeast	Southeast	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Columbia	Columbia	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
231	#num#	k4
<g/>
-	-	kIx~
<g/>
50602	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kdo	kdo	k3yQnSc1,k3yInSc1,k3yRnSc1
objevil	objevit	k5eAaPmAgMnS
Ameriku	Amerika	k1gFnSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
Vikingové	Viking	k1gMnPc1
tam	tam	k6eAd1
dopluli	doplout	k5eAaPmAgMnP
500	#num#	k4
let	léto	k1gNnPc2
před	před	k7c7
Kolumbem	Kolumbus	k1gMnSc7
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-10-17	2010-10-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Genocida	genocida	k1gFnSc1
amerických	americký	k2eAgMnPc2d1
indiánů	indián	k1gMnPc2
<g/>
:	:	kIx,
Krví	krvit	k5eAaImIp3nS
zbrocená	zbrocený	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
k	k	k7c3
pozitivní	pozitivní	k2eAgFnSc3d1
diskriminaci	diskriminace	k1gFnSc3
<g/>
.	.	kIx.
100	#num#	k4
<g/>
+	+	kIx~
<g/>
1	#num#	k4
zahraniční	zahraniční	k2eAgFnSc4d1
zajímavost	zajímavost	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-08-05	2018-08-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Válka	válka	k1gFnSc1
světů	svět	k1gInPc2
a	a	k8xC
mikrobů	mikrob	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aztékové	Azték	k1gMnPc1
<g/>
,	,	kIx,
Cortés	Cortés	k1gInSc1
a	a	k8xC
evropské	evropský	k2eAgFnSc2d1
infekce	infekce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vesmír	vesmír	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
https://digilib.phil.muni.cz/data/handle/11222.digilib/128682/monography.pdf	https://digilib.phil.muni.cz/data/handle/11222.digilib/128682/monography.pdf	k1gInSc1
<g/>
↑	↑	k?
Nový	nový	k2eAgInSc1d1
evangelikalismus	evangelikalismus	k1gInSc1
|	|	kIx~
Reformace	reformace	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.reformace.cz	www.reformace.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
JANA	Jana	k1gFnSc1
<g/>
,	,	kIx,
Dolejší	Dolejší	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Republikanismus	republikanismus	k1gInSc1
v	v	k7c6
americkém	americký	k2eAgNnSc6d1
politickém	politický	k2eAgNnSc6d1
myšlení	myšlení	k1gNnSc6
<g/>
.	.	kIx.
,	,	kIx,
2017	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diplomová	diplomový	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Jana	Jan	k1gMnSc2
Evangelisty	evangelista	k1gMnSc2
Purkyně	Purkyně	k1gFnSc1
v	v	k7c6
Ústí	ústí	k1gNnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
,	,	kIx,
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
.	.	kIx.
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Redeemers	Redeemers	k1gInSc1
in	in	k?
Reconstruction	Reconstruction	k1gInSc1
<g/>
:	:	kIx,
History	Histor	k1gInPc1
&	&	k?
Explanation	Explanation	k1gInSc1
-	-	kIx~
Video	video	k1gNnSc1
&	&	k?
Lesson	Lesson	k1gMnSc1
Transcript	Transcript	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Study	stud	k1gInPc4
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Po	po	k7c6
142	#num#	k4
letech	léto	k1gNnPc6
nejsou	být	k5eNaImIp3nP
USA	USA	kA
největší	veliký	k2eAgFnSc7d3
ekonomikou	ekonomika	k1gFnSc7
světa	svět	k1gInSc2
<g/>
,	,	kIx,
sesadila	sesadit	k5eAaPmAgFnS
je	být	k5eAaImIp3nS
Čína	Čína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-12-06	2014-12-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Mírotvorce	mírotvorce	k1gMnSc1
Woodrow	Woodrow	k1gMnSc1
Wilson	Wilson	k1gMnSc1
<g/>
:	:	kIx,
idea	idea	k1gFnSc1
světa	svět	k1gInSc2
ve	v	k7c6
Čtrnácti	čtrnáct	k4xCc6
bodech	bod	k1gInPc6
|	|	kIx~
Lidé	člověk	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-01-06	2018-01-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://www.dejepis.com/ucebnice/vyvoj-v-letech-1924-1929/	http://www.dejepis.com/ucebnice/vyvoj-v-letech-1924-1929/	k4
<g/>
↑	↑	k?
Krach	krach	k1gInSc1
na	na	k7c6
newyorské	newyorský	k2eAgFnSc6d1
burze	burza	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peníze	peníz	k1gInSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
New	New	k1gMnSc1
Deal	Deal	k1gMnSc1
–	–	k?
Sociologická	sociologický	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
<g/>
.	.	kIx.
encyklopedie	encyklopedie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
soc	soc	kA
<g/>
.	.	kIx.
<g/>
cas	cas	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Pearl	Pearl	k1gMnSc1
Harbor	Harbor	k1gMnSc1
<g/>
:	:	kIx,
Začátek	začátek	k1gInSc1
války	válka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
skončila	skončit	k5eAaPmAgFnS
atomovými	atomový	k2eAgInPc7d1
výbuchy	výbuch	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reflex	reflex	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://www.cojeco.cz/index.php?detail=1&	http://www.cojeco.cz/index.php?detail=1&	k6eAd1
Act	Act	k1gFnSc2
<g/>
↑	↑	k?
Před	před	k7c7
40	#num#	k4
lety	léto	k1gNnPc7
zavřeli	zavřít	k5eAaPmAgMnP
Američané	Američan	k1gMnPc1
„	„	k?
<g/>
zlaté	zlatý	k2eAgNnSc4d1
okno	okno	k1gNnSc4
<g/>
“	“	k?
z	z	k7c2
Bretton	Bretton	k1gInSc4
Woods	Woodsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Jaltská	jaltský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
měla	mít	k5eAaImAgFnS
prostý	prostý	k2eAgInSc4d1
cíl	cíl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projednat	projednat	k5eAaPmF
budoucnost	budoucnost	k1gFnSc4
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Marshallův	Marshallův	k2eAgInSc1d1
plán	plán	k1gInSc1
postavil	postavit	k5eAaPmAgInS
poválečnou	poválečný	k2eAgFnSc4d1
západní	západní	k2eAgFnSc4d1
Evropu	Evropa	k1gFnSc4
na	na	k7c4
nohy	noha	k1gFnPc4
<g/>
,	,	kIx,
Československo	Československo	k1gNnSc1
stálo	stát	k5eAaImAgNnS
mimo	mimo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Pojem	pojem	k1gInSc1
studená	studený	k2eAgFnSc1d1
válka	válka	k1gFnSc1
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
použit	použít	k5eAaPmNgInS
před	před	k7c7
šedesáti	šedesát	k4xCc7
lety	let	k1gInPc7
|	|	kIx~
ČeskéNoviny	ČeskéNovina	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
www.ceskenoviny.cz	www.ceskenoviny.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Před	před	k7c7
50	#num#	k4
lety	léto	k1gNnPc7
položil	položit	k5eAaPmAgMnS
Martin	Martin	k1gMnSc1
Luther	Luthra	k1gFnPc2
King	King	k1gMnSc1
život	život	k1gInSc4
při	při	k7c6
boji	boj	k1gInSc6
za	za	k7c4
lidská	lidský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
Afroameričanů	Afroameričan	k1gMnPc2
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
USA	USA	kA
-	-	kIx~
supervelmoc	supervelmoc	k1gFnSc4
<g/>
.	.	kIx.
dumfinanci	dumfinance	k1gFnSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2007-06-28	2007-06-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Moderní-Dějiny	Moderní-Dějina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
|	|	kIx~
Operace	operace	k1gFnSc1
Pouštní	pouštní	k2eAgFnSc2d1
bouře	bouř	k1gFnSc2
<g/>
.	.	kIx.
www.moderni-dejiny.cz	www.moderni-dejiny.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Teroristické	teroristický	k2eAgInPc4d1
útoky	útok	k1gInPc4
z	z	k7c2
11	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
trvale	trvale	k6eAd1
změnily	změnit	k5eAaPmAgFnP
podobu	podoba	k1gFnSc4
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Škobrtnutí	škobrtnutí	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
změnilo	změnit	k5eAaPmAgNnS
svět	svět	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finanční	finanční	k2eAgFnSc1d1
krize	krize	k1gFnSc1
měla	mít	k5eAaImAgFnS
za	za	k7c4
následek	následek	k1gInSc4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
chudí	chudý	k2eAgMnPc1d1
zchudli	zchudnout	k5eAaPmAgMnP
a	a	k8xC
bohatí	bohatit	k5eAaImIp3nP
zbohatli	zbohatnout	k5eAaPmAgMnP
|	|	kIx~
Firmy	firma	k1gFnPc4
a	a	k8xC
trhy	trh	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-09-15	2018-09-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zmírnění	zmírnění	k1gNnSc1
napětí	napětí	k1gNnSc2
ve	v	k7c6
vztazích	vztah	k1gInPc6
USA	USA	kA
a	a	k8xC
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
nový	nový	k2eAgInSc1d1
reset	reset	k1gInSc1
je	být	k5eAaImIp3nS
ale	ale	k8xC
zatím	zatím	k6eAd1
daleko	daleko	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Obchodní	obchodní	k2eAgFnSc1d1
válka	válka	k1gFnSc1
mezi	mezi	k7c7
USA	USA	kA
a	a	k8xC
Čínou	Čína	k1gFnSc7
<g/>
:	:	kIx,
Trump	Trump	k1gInSc1
nařídil	nařídit	k5eAaPmAgInS
zvýšit	zvýšit	k5eAaPmF
cla	clo	k1gNnPc4
na	na	k7c4
veškerý	veškerý	k3xTgInSc4
zbývající	zbývající	k2eAgInSc4d1
čínský	čínský	k2eAgInSc4d1
dovoz	dovoz	k1gInSc4
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
USA	USA	kA
a	a	k8xC
Evropa	Evropa	k1gFnSc1
mají	mít	k5eAaImIp3nP
zásadně	zásadně	k6eAd1
jiné	jiný	k2eAgFnPc1d1
priority	priorita	k1gFnPc1
<g/>
,	,	kIx,
ukázala	ukázat	k5eAaPmAgFnS
mnichovská	mnichovský	k2eAgFnSc1d1
konference	konference	k1gFnSc1
-	-	kIx~
Echo	echo	k1gNnSc1
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
echo	echo	k1gNnSc1
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-02-16	2019-02-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FŇUKAL	Fňukal	k1gMnSc1
<g/>
,	,	kIx,
Miloš	Miloš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
REGIONÁLNÍ	regionální	k2eAgFnSc1d1
GEOGRAFIE	geografie	k1gFnSc1
AMERIKY	Amerika	k1gFnSc2
<g/>
,	,	kIx,
8	#num#	k4
<g/>
.	.	kIx.
přednáška	přednáška	k1gFnSc1
<g/>
:	:	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
[	[	kIx(
<g/>
PDF	PDF	kA
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomouc	Olomouc	k1gFnSc1
<g/>
:	:	kIx,
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Statue	statue	k1gFnSc1
of	of	k?
Liberty	Libert	k1gInPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
UNESCO	Unesco	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Sandinisté	sandinista	k1gMnPc1
chtějí	chtít	k5eAaImIp3nP
z	z	k7c2
USA	USA	kA
vymáčknout	vymáčknout	k5eAaPmF
17	#num#	k4
miliard	miliarda	k4xCgFnPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2011	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
World	World	k1gMnSc1
—	—	k?
The	The	k1gMnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
-	-	kIx~
Central	Central	k1gFnSc1
Intelligence	Intelligenec	k1gMnSc2
Agency	Agenca	k1gMnSc2
<g/>
.	.	kIx.
www.cia.gov	www.cia.gov	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Válku	válek	k1gInSc2
odmítlo	odmítnout	k5eAaPmAgNnS
v	v	k7c6
ulicích	ulice	k1gFnPc6
šest	šest	k4xCc4
milionů	milion	k4xCgInPc2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
iDnes	iDnes	k1gMnSc1
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Síť	síť	k1gFnSc1
tajných	tajný	k2eAgFnPc2d1
věznic	věznice	k1gFnPc2
CIA	CIA	kA
<g/>
:	:	kIx,
Thajsko	Thajsko	k1gNnSc1
<g/>
,	,	kIx,
Rumunsko	Rumunsko	k1gNnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
<g/>
,	,	kIx,
Litva	Litva	k1gFnSc1
<g/>
,	,	kIx,
Egypt	Egypt	k1gInSc1
<g/>
....	....	k?
Hospodářské	hospodářský	k2eAgFnPc1d1
noviny	novina	k1gFnPc1
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vysvětlete	vysvětlit	k5eAaPmRp2nP
sledování	sledování	k1gNnSc4
Merkelové	Merkelová	k1gFnSc2
<g/>
,	,	kIx,
chtějí	chtít	k5eAaImIp3nP
Němci	Němec	k1gMnPc1
po	po	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
EK	EK	kA
mluví	mluvit	k5eAaImIp3nS
o	o	k7c6
totalitě	totalita	k1gFnSc6
<g/>
.	.	kIx.
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
24	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Válka	válka	k1gFnSc1
v	v	k7c6
Iráku	Irák	k1gInSc6
má	mít	k5eAaImIp3nS
čtyřikrát	čtyřikrát	k6eAd1
víc	hodně	k6eAd2
obětí	oběť	k1gFnPc2
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
uvádělo	uvádět	k5eAaImAgNnS
<g/>
,	,	kIx,
spočítali	spočítat	k5eAaPmAgMnP
akademici	akademik	k1gMnPc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2013	#num#	k4
<g/>
↑	↑	k?
Chaos	chaos	k1gInSc1
v	v	k7c6
Libyi	Libye	k1gFnSc6
je	být	k5eAaImIp3nS
následek	následek	k1gInSc4
mé	můj	k3xOp1gFnSc2
největší	veliký	k2eAgFnSc2d3
chyby	chyba	k1gFnSc2
<g/>
,	,	kIx,
přiznal	přiznat	k5eAaPmAgMnS
Obama	Obama	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Trump	Trump	k1gInSc1
jedná	jednat	k5eAaImIp3nS
v	v	k7c6
Saúdské	saúdský	k2eAgFnSc6d1
Arábii	Arábie	k1gFnSc6
<g/>
:	:	kIx,
Podepsány	podepsán	k2eAgInPc1d1
kontrakty	kontrakt	k1gInPc1
za	za	k7c4
350	#num#	k4
miliard	miliarda	k4xCgFnPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Konflikt	konflikt	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
„	„	k?
<g/>
nikoho	nikdo	k3yNnSc4
<g/>
“	“	k?
nezajímá	zajímat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
pustoší	pustošit	k5eAaImIp3nS
Jemen	Jemen	k1gInSc4
hladomorem	hladomor	k1gInSc7
s	s	k7c7
tichou	tichý	k2eAgFnSc7d1
podporou	podpora	k1gFnSc7
Západu	západ	k1gInSc2
<g/>
.	.	kIx.
wave	wave	k1gFnSc1
<g/>
.	.	kIx.
<g/>
rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
USA	USA	kA
obnovily	obnovit	k5eAaPmAgInP
všechny	všechen	k3xTgFnPc4
sankce	sankce	k1gFnPc4
proti	proti	k7c3
Íránu	Írán	k1gInSc3
<g/>
,	,	kIx,
chtějí	chtít	k5eAaImIp3nP
jej	on	k3xPp3gInSc4
ekonomicky	ekonomicky	k6eAd1
izolovat	izolovat	k5eAaBmF
a	a	k8xC
vyhladovět	vyhladovět	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Info	Info	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Woodward	Woodward	k1gMnSc1
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
and	and	k?
Duffy	Duff	k1gInPc1
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Chinese	Chinese	k1gFnSc1
Embassy	Embassa	k1gFnSc2
Role	role	k1gFnSc1
In	In	k1gFnSc1
Contributions	Contributions	k1gInSc4
Probed	Probed	k1gInSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
Post	posta	k1gFnPc2
<g/>
,	,	kIx,
13	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1997	#num#	k4
<g/>
↑	↑	k?
Findings	Findings	k1gInSc1
Link	Link	k1gMnSc1
Clinton	Clinton	k1gMnSc1
Allies	Allies	k1gMnSc1
to	ten	k3xDgNnSc4
Chinese	Chinese	k1gFnSc1
Intelligence	Intelligenec	k1gInSc2
<g/>
,	,	kIx,
The	The	k1gFnSc1
Washington	Washington	k1gInSc1
Post	post	k1gInSc1
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1998	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
15	#num#	k4
největších	veliký	k2eAgMnPc2d3
zahraničních	zahraniční	k2eAgMnPc2d1
věřitelů	věřitel	k1gMnPc2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Investicniweb	Investicniwba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Čínští	čínský	k2eAgMnPc1d1
kyberzločinci	kyberzločinec	k1gMnPc1
ukradli	ukradnout	k5eAaPmAgMnP
miliony	milion	k4xCgInPc4
otisků	otisk	k1gInPc2
prstů	prst	k1gInPc2
<g/>
,	,	kIx,
tvrdí	tvrdit	k5eAaImIp3nS
USA	USA	kA
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bránit	bránit	k5eAaImF
Číně	Čína	k1gFnSc3
v	v	k7c6
přístupu	přístup	k1gInSc6
na	na	k7c4
umělé	umělý	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
povede	povést	k5eAaPmIp3nS,k5eAaImIp3nS
k	k	k7c3
rozsáhlé	rozsáhlý	k2eAgFnSc3d1
válce	válka	k1gFnSc3
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
13	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Trumps	Trumpsa	k1gFnPc2
Truppe	Trupp	k1gInSc5
lässt	lässt	k1gInSc4
China	China	k1gFnSc1
zappeln	zappeln	k1gInSc1
(	(	kIx(
<g/>
Trumpův	Trumpův	k2eAgInSc1d1
tým	tým	k1gInSc1
nechává	nechávat	k5eAaImIp3nS
Čínu	Čína	k1gFnSc4
v	v	k7c6
nejistotě	nejistota	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
Frankfurter	Frankfurter	k1gInSc1
Allgemeine	Allgemein	k1gInSc5
Zeitung	Zeitung	k1gMnSc1
<g/>
,	,	kIx,
4	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2018	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
15	#num#	k4
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Amerika	Amerika	k1gFnSc1
und	und	k?
China	China	k1gFnSc1
bieten	bietno	k1gNnPc2
sich	sich	k1gInSc1
die	die	k?
Stirn	Stirn	k1gInSc1
(	(	kIx(
<g/>
Amerika	Amerika	k1gFnSc1
a	a	k8xC
Čína	Čína	k1gFnSc1
se	se	k3xPyFc4
staví	stavit	k5eAaImIp3nS,k5eAaBmIp3nS,k5eAaPmIp3nS
proti	proti	k7c3
sobě	se	k3xPyFc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frankfurter	Frankfurter	k1gInSc1
Allgemeine	Allgemein	k1gInSc5
Zeitung	Zeitung	k1gMnSc1
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2018	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
17	#num#	k4
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Trump	Trump	k1gMnSc1
will	will	k1gMnSc1
Chinas	Chinas	k1gMnSc1
Aufstieg	Aufstieg	k1gMnSc1
bremsen	bremsen	k2eAgMnSc1d1
(	(	kIx(
<g/>
Trump	Trump	k1gMnSc1
chce	chtít	k5eAaImIp3nS
zbrzdit	zbrzdit	k5eAaPmF
vzestup	vzestup	k1gInSc4
Číny	Čína	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frankfurter	Frankfurter	k1gInSc1
Allgemeine	Allgemein	k1gInSc5
Zeitung	Zeitung	k1gMnSc1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2018	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
17	#num#	k4
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Počet	počet	k1gInSc1
jaderných	jaderný	k2eAgFnPc2d1
hlavic	hlavice	k1gFnPc2
ve	v	k7c6
světě	svět	k1gInSc6
se	se	k3xPyFc4
snížil	snížit	k5eAaPmAgInS
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
ale	ale	k9
modernější	moderní	k2eAgMnSc1d2
a	a	k8xC
pro	pro	k7c4
státy	stát	k1gInPc4
důležitější	důležitý	k2eAgInPc4d2
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
FROM	FROM	kA
WOUNDED	WOUNDED	kA
KNEE	KNEE	kA
TO	to	k9
SYRIA	SYRIA	kA
–	–	k?
Zoltán	Zoltán	k1gMnSc1
Grossman	Grossman	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
U.	U.	kA
<g/>
S.	S.	kA
Military	Militar	k1gMnPc4
Base	basa	k1gFnSc3
Locations	Locations	k1gInSc1
<g/>
.	.	kIx.
nasaa-home	nasaa-hom	k1gInSc5
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
JOHNSON	JOHNSON	kA
<g/>
,	,	kIx,
Chalmers	Chalmers	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
America	Americ	k1gInSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Empire	empir	k1gInSc5
of	of	k?
Bases	Bases	k1gInSc1
<g/>
.	.	kIx.
www.commondreams.org	www.commondreams.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
2007	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
17	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2004	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.sott.net/articles/show/199500-The-Pentagon-is-the-planet-s-worst-polluter	http://www.sott.net/articles/show/199500-The-Pentagon-is-the-planet-s-worst-polluter	k1gInSc1
<g/>
↑	↑	k?
Top	topit	k5eAaImRp2nS
5	#num#	k4
facts	facts	k1gInSc1
on	on	k3xPp3gMnSc1
US	US	kA
Military	Militara	k1gFnSc2
Oil	Oil	k1gFnSc2
Consumption	Consumption	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://www.fas.org/irp/congress/2012_hr/030612mcraven.pdf	http://www.fas.org/irp/congress/2012_hr/030612mcraven.pdf	k1gInSc1
<g/>
↑	↑	k?
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
dnes	dnes	k6eAd1
válčí	válčit	k5eAaImIp3nP
ve	v	k7c6
více	hodně	k6eAd2
než	než	k8xS
stovce	stovka	k1gFnSc6
zemí	zem	k1gFnPc2
světa	svět	k1gInSc2
|	|	kIx~
8	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
|	|	kIx~
Daniel	Daniel	k1gMnSc1
Veselý	Veselý	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Britské	britský	k2eAgInPc1d1
listy	list	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-03-08	2012-03-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
People	People	k1gFnSc2
Archivováno	archivován	k2eAgNnSc1d1
6	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
.	.	kIx.
12	#num#	k4
June	jun	k1gMnSc5
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
American	American	k1gMnSc1
Fact	Fact	k1gMnSc1
Finder	Finder	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Accessed	Accessed	k1gInSc1
13	#num#	k4
June	jun	k1gMnSc5
2006.1	2006.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
United	United	k1gMnSc1
States	States	k1gMnSc1
on	on	k3xPp3gMnSc1
the	the	k?
World	Worldo	k1gNnPc2
Factbook	Factbook	k1gInSc1
<g/>
↑	↑	k?
Figure	Figur	k1gMnSc5
2	#num#	k4
–	–	k?
Fifteen	Fifteen	k2eAgMnSc1d1
Largest	Largest	k1gMnSc1
Ancestries	Ancestries	k1gMnSc1
<g/>
:	:	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Census	census	k1gInSc4
Bureau	Bureaus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
URL	URL	kA
accessed	accessed	k1gInSc4
30	#num#	k4
May	May	k1gMnSc1
2006	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
US	US	kA
Department	department	k1gInSc1
of	of	k?
Commerce	Commerka	k1gFnSc3
<g/>
,	,	kIx,
ancestry	ancestra	k1gFnPc1
in	in	k?
the	the	k?
US	US	kA
as	as	k9
published	published	k1gInSc4
on	on	k3xPp3gMnSc1
Factmonster	Factmonster	k1gMnSc1
<g/>
,	,	kIx,
2000	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
National	National	k1gFnSc1
Sex	sex	k1gInSc1
<g/>
,	,	kIx,
Race	Race	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Hispanic	Hispanice	k1gFnPc2
Origin	Origin	k2eAgInSc1d1
Population	Population	k1gInSc1
Estimates	Estimates	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ADAMS	ADAMS	kA
<g/>
,	,	kIx,
J.Q.	J.Q.	k1gFnSc1
Dealing	Dealing	k1gInSc1
with	with	k1gInSc1
Diversity	Diversit	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chicago	Chicago	k1gNnSc1
<g/>
,	,	kIx,
IL	IL	kA
<g/>
:	:	kIx,
Kendall	Kendall	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Hunt	hunt	k1gInSc1
Publishing	Publishing	k1gInSc1
Company	Compana	k1gFnSc2
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
0	#num#	k4
<g/>
-	-	kIx~
<g/>
7872	#num#	k4
<g/>
-	-	kIx~
<g/>
8145	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
↑	↑	k?
Statistical	Statistical	k1gMnSc1
Abstract	Abstract	k1gMnSc1
of	of	k?
the	the	k?
United	United	k1gMnSc1
States	States	k1gMnSc1
<g/>
:	:	kIx,
page	page	k1gInSc1
47	#num#	k4
<g/>
:	:	kIx,
Table	tablo	k1gNnSc6
47	#num#	k4
<g/>
:	:	kIx,
Languages	Languages	k1gInSc4
Spoken	Spoken	k2eAgInSc4d1
at	at	k?
Home	Home	k1gInSc4
by	by	kYmCp3nS
Language	language	k1gFnSc1
<g/>
:	:	kIx,
2003	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
U.	U.	kA
<g/>
S.	S.	kA
Government	Government	k1gMnSc1
Says	Says	k1gInSc4
47	#num#	k4
Million	Million	k1gInSc1
Americans	Americans	k1gInSc4
Below	Below	k1gMnSc2
Poverty	Povert	k1gInPc4
Line	linout	k5eAaImIp3nS
<g/>
.	.	kIx.
www.disinfo.com	www.disinfo.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
"	"	kIx"
<g/>
Yearbook	Yearbook	k1gInSc1
of	of	k?
Immigration	Immigration	k1gInSc1
Statistics	Statistics	k1gInSc1
<g/>
:	:	kIx,
2013	#num#	k4
-	-	kIx~
Legal	Legal	k1gInSc1
Permanent	permanent	k1gInSc1
Residents	Residents	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
vnitřní	vnitřní	k2eAgFnSc2d1
bezpečnosti	bezpečnost	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
↑	↑	k?
U.	U.	kA
<g/>
S.	S.	kA
Hispanic	Hispanice	k1gFnPc2
population	population	k1gInSc1
to	ten	k3xDgNnSc1
triple	tripl	k1gInSc5
by	by	kYmCp3nS
2050	#num#	k4
<g/>
,	,	kIx,
USATODAY	USATODAY	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
↑	↑	k?
Table	tablo	k1gNnSc6
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projections	Projections	k1gInSc1
of	of	k?
the	the	k?
Population	Population	k1gInSc1
by	by	kYmCp3nS
Sex	sex	k1gInSc1
<g/>
,	,	kIx,
Race	Race	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Hispanic	Hispanice	k1gFnPc2
Origin	Origin	k1gMnSc1
for	forum	k1gNnPc2
the	the	k?
United	United	k1gMnSc1
States	States	k1gMnSc1
<g/>
:	:	kIx,
2010	#num#	k4
to	ten	k3xDgNnSc1
2050	#num#	k4
[	[	kIx(
<g/>
Excel	Excel	kA
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Census	census	k1gInSc4
Bureau	Bureaus	k1gInSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
DOUGHERTY	DOUGHERTY	kA
<g/>
,	,	kIx,
Conor	Conor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Nears	Nearsa	k1gFnPc2
Racial	Racial	k1gMnSc1
Milestone	Mileston	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Wall	Wall	k1gMnSc1
Street	Street	k1gMnSc1
Journal	Journal	k1gMnSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
99	#num#	k4
<g/>
-	-	kIx~
<g/>
9660	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Americans	Americans	k1gInSc1
under	under	k1gInSc1
age	age	k?
1	#num#	k4
now	now	k?
mostly	mostnout	k5eAaPmAgFnP
minorities	minorities	k1gInSc4
<g/>
,	,	kIx,
but	but	k?
not	nota	k1gFnPc2
in	in	k?
Ohio	Ohio	k1gNnSc1
<g/>
:	:	kIx,
Statistical	Statistical	k1gMnSc1
Snapshot	Snapshot	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Plain	Plain	k1gMnSc1
Dealer	dealer	k1gMnSc1
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2012	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Babies	Babies	k1gMnSc1
Of	Of	k1gMnSc1
Color	Color	k1gMnSc1
Are	ar	k1gInSc5
Now	Now	k1gMnSc2
The	The	k1gMnSc7
Majority	majorita	k1gFnSc2
<g/>
,	,	kIx,
Census	census	k1gInSc4
Says	Saysa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gFnSc1
Public	publicum	k1gNnPc2
Radio	radio	k1gNnSc4
(	(	kIx(
<g/>
NPR	NPR	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Podle	podle	k7c2
<g/>
:	:	kIx,
United	United	k1gInSc1
States	Statesa	k1gFnPc2
Census	census	k1gInSc1
Bureau	Bureaus	k1gInSc2
<g/>
,	,	kIx,
sčítání	sčítání	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
a	a	k8xC
odhadů	odhad	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
USA	USA	kA
<g/>
:	:	kIx,
State	status	k1gInSc5
&	&	k?
County	Count	k1gInPc1
QuickFacts	QuickFacts	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Census	census	k1gInSc4
Bureau	Bureaus	k1gInSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2001	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Historical	Historical	k1gMnPc2
Census	census	k1gInSc4
Statistics	Statisticsa	k1gFnPc2
on	on	k3xPp3gMnSc1
Population	Population	k1gInSc1
Totals	Totals	k1gInSc4
By	by	k9
Race	Race	k1gFnSc1
<g/>
,	,	kIx,
1790	#num#	k4
to	ten	k3xDgNnSc1
1990	#num#	k4
<g/>
,	,	kIx,
and	and	k?
By	by	k9
Hispanic	Hispanice	k1gFnPc2
Origin	Origin	k1gMnSc1
<g/>
,	,	kIx,
1970	#num#	k4
to	ten	k3xDgNnSc1
1990	#num#	k4
<g/>
,	,	kIx,
For	forum	k1gNnPc2
The	The	k1gMnSc1
United	United	k1gMnSc1
States	States	k1gMnSc1
<g/>
,	,	kIx,
Regions	Regions	k1gInSc1
<g/>
,	,	kIx,
Divisions	Divisions	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
States	States	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
Český	český	k2eAgMnSc1d1
starosta	starosta	k1gMnSc1
Chicaga	Chicago	k1gNnSc2
chytil	chytit	k5eAaPmAgMnS
kulku	kulka	k1gFnSc4
pro	pro	k7c4
amerického	americký	k2eAgMnSc4d1
prezidenta	prezident	k1gMnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
tomu	ten	k3xDgNnSc3
už	už	k9
78	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2011	#num#	k4
<g/>
,	,	kIx,
<g/>
↑	↑	k?
Immigration	Immigration	k1gInSc1
1891-1924	1891-1924	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gFnSc1
Park	park	k1gInSc1
Service	Service	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Immigrant	Immigrant	k1gMnSc1
Ban	Ban	k1gMnSc1
<g/>
,	,	kIx,
1921	#num#	k4
<g/>
,	,	kIx,
1924	#num#	k4
<g/>
,	,	kIx,
1939	#num#	k4
Through	Through	k1gInSc1
1943	#num#	k4
—	—	k?
A	a	k8xC
Family	Famila	k1gFnPc1
History	Histor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Huffington	Huffington	k1gInSc4
Post	post	k1gInSc1
31	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
The	The	k1gFnSc1
Immigration	Immigration	k1gInSc1
Act	Act	k1gFnSc1
of	of	k?
1924	#num#	k4
(	(	kIx(
<g/>
The	The	k1gMnSc1
Johnson-Reed	Johnson-Reed	k1gMnSc1
Act	Act	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.S	U.S	k1gFnPc2
Department	department	k1gInSc4
of	of	k?
State	status	k1gInSc5
Office	Office	kA
of	of	k?
the	the	k?
Historian	Historian	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Table	tablo	k1gNnSc6
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Persons	Persons	k1gInSc1
obtaining	obtaining	k1gInSc4
lawful	lawfout	k5eAaPmAgInS
permanent	permanent	k1gInSc1
resident	resident	k1gMnSc1
status	status	k1gInSc1
<g/>
:	:	kIx,
fiscal	fiscat	k5eAaPmAgInS
years	years	k1gInSc4
1820	#num#	k4
to	ten	k3xDgNnSc4
2016	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Department	department	k1gInSc1
of	of	k?
Homeland	Homeland	k1gInSc4
Security	Securita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jennifer	Jennifer	k1gInSc1
Ludden	Luddna	k1gFnPc2
<g/>
.	.	kIx.
1965	#num#	k4
immigration	immigration	k1gInSc1
law	law	k?
changed	changed	k1gInSc1
face	fac	k1gMnSc2
of	of	k?
America	Americus	k1gMnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NPR	NPR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
U.	U.	kA
<g/>
S.	S.	kA
Legal	Legal	k1gInSc1
Permanent	permanent	k1gInSc1
Residents	Residents	k1gInSc1
<g/>
:	:	kIx,
2015	#num#	k4
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Office	Office	kA
of	of	k?
Immigration	Immigration	k1gInSc1
Statistics	Statisticsa	k1gFnPc2
Annual	Annual	k1gInSc1
Flow	Flow	k1gMnSc1
Report	report	k1gInSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Chicago	Chicago	k1gNnSc4
vs	vs	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trump	Trump	k1gInSc1
<g/>
.	.	kIx.
‚	‚	k?
<g/>
Ve	v	k7c6
150	#num#	k4
tisících	tisíc	k4xCgInPc6,k4xOgInPc6
domácností	domácnost	k1gFnPc2
je	on	k3xPp3gNnSc4
někdo	někdo	k3yInSc1
bez	bez	k7c2
dokumentů	dokument	k1gInPc2
<g/>
,	,	kIx,
<g/>
‘	‘	k?
říká	říkat	k5eAaImIp3nS
šéfka	šéfka	k1gFnSc1
Úřadu	úřad	k1gInSc2
pro	pro	k7c4
nové	nový	k2eAgMnPc4d1
Američany	Američan	k1gMnPc4
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
25	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Spor	spor	k1gInSc1
Trumpa	Trump	k1gMnSc2
se	s	k7c7
starosty	starosta	k1gMnPc7
o	o	k7c4
imigranty	imigrant	k1gMnPc4
ještě	ještě	k6eAd1
více	hodně	k6eAd2
rozděluje	rozdělovat	k5eAaImIp3nS
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
WARD	WARD	kA
<g/>
,	,	kIx,
Carol	Carol	k1gInSc1
Jane	Jan	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Native	Natiev	k1gFnSc2
Americans	Americansa	k1gFnPc2
in	in	k?
the	the	k?
School	School	k1gInSc1
System	Syst	k1gInSc7
<g/>
:	:	kIx,
Family	Famil	k1gInPc1
<g/>
,	,	kIx,
Community	Communit	k1gInPc1
<g/>
,	,	kIx,
and	and	k?
Academic	Academic	k1gMnSc1
Achievement	Achievement	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
United	United	k1gMnSc1
States	States	k1gMnSc1
of	of	k?
America	America	k1gMnSc1
<g/>
:	:	kIx,
Rowman	Rowman	k1gMnSc1
Altamira	Altamira	k1gMnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
267	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
5910	#num#	k4
<g/>
-	-	kIx~
<g/>
609	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Segregace	segregace	k1gFnSc1
na	na	k7c6
amerických	americký	k2eAgFnPc6d1
školách	škola	k1gFnPc6
kvete	kvést	k5eAaImIp3nS
i	i	k9
60	#num#	k4
let	léto	k1gNnPc2
po	po	k7c6
jejím	její	k3xOp3gNnSc6
zrušení	zrušení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Joe	Joe	k1gFnSc1
Biden	Biden	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
record	record	k6eAd1
on	on	k3xPp3gInSc1
school	school	k1gInSc1
desegregation	desegregation	k1gInSc1
busing	busing	k1gInSc1
<g/>
,	,	kIx,
explained	explained	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vox	Vox	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Amerika	Amerika	k1gFnSc1
zrušila	zrušit	k5eAaPmAgFnS
pozitivní	pozitivní	k2eAgFnSc4d1
diskriminaci	diskriminace	k1gFnSc4
na	na	k7c6
školách	škola	k1gFnPc6
<g/>
,	,	kIx,
menšiny	menšina	k1gFnPc1
se	se	k3xPyFc4
bouří	bouřit	k5eAaImIp3nP
<g/>
.	.	kIx.
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Obamacare	Obamacar	k1gMnSc5
selhává	selhávat	k5eAaImIp3nS
<g/>
?	?	kIx.
</s>
<s desamb="1">
Pojišťovny	pojišťovna	k1gFnPc1
dávají	dávat	k5eAaImIp3nP
od	od	k7c2
reformy	reforma	k1gFnSc2
ruce	ruka	k1gFnPc4
pryč	pryč	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Euro	euro	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
The	The	k1gFnSc1
Declaration	Declaration	k1gInSc1
of	of	k?
Independence	Independence	k1gFnSc1
<g/>
:	:	kIx,
Full	Full	k1gInSc1
text	text	k1gInSc1
<g/>
.	.	kIx.
www.ushistory.org	www.ushistory.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
AMADEO	AMADEO	kA
<g/>
,	,	kIx,
Kimberly	Kimberl	k1gInPc1
<g/>
.	.	kIx.
5	#num#	k4
Ways	Ways	k1gInSc4
Our	Our	k1gMnPc2
Founding	Founding	k1gInSc1
Fathers	Fathersa	k1gFnPc2
Protect	Protect	k1gMnSc1
The	The	k1gMnSc1
American	American	k1gMnSc1
Dream	Dream	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Balance	balance	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Herberg	Herberg	k1gInSc1
<g/>
,	,	kIx,
William	William	k1gInSc1
(	(	kIx(
<g/>
1955	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protestant	protestant	k1gMnSc1
<g/>
,	,	kIx,
Catholic	Catholice	k1gFnPc2
<g/>
,	,	kIx,
Jew	Jew	k1gFnSc1
<g/>
:	:	kIx,
an	an	k?
Essay	Essaa	k1gFnSc2
in	in	k?
American	American	k1gMnSc1
religious	religious	k1gMnSc1
sociology	sociolog	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
University	universita	k1gFnPc4
of	of	k?
Chicago	Chicago	k1gNnSc1
Press	Press	k1gInSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Puritánské	puritánský	k2eAgInPc4d1
kořeny	kořen	k1gInPc4
amerického	americký	k2eAgInSc2d1
mýtu	mýtus	k1gInSc2
<g/>
.	.	kIx.
blisty	blista	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CNN	CNN	kA
<g/>
,	,	kIx,
Lynda	Lynda	k1gFnSc1
Kinkade	Kinkad	k1gInSc5
<g/>
,	,	kIx,
<g/>
.	.	kIx.
</s>
<s desamb="1">
America	Americ	k1gInSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
poor	poor	k1gInSc1
becoming	becoming	k1gInSc1
more	mor	k1gInSc5
destitute	destitut	k1gInSc5
under	undra	k1gFnPc2
Trump	Trump	k1gInSc1
<g/>
,	,	kIx,
UN	UN	kA
report	report	k1gInSc1
says	says	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Foreign	Foreign	k1gInSc1
Policy	Polica	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
National	National	k1gFnSc1
Coalition	Coalition	k1gInSc1
for	forum	k1gNnPc2
the	the	k?
Homeless	Homeless	k1gInSc1
<g/>
.	.	kIx.
nationalhomeless	nationalhomeless	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HEMENWAY	HEMENWAY	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
;	;	kIx,
GRINSHTEYN	GRINSHTEYN	kA
<g/>
,	,	kIx,
Erin	Erin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Violent	Violent	k1gMnSc1
Death	Death	k1gMnSc1
Rates	Rates	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
US	US	kA
Compared	Compared	k1gMnSc1
with	with	k1gMnSc1
Other	Other	k1gMnSc1
High-income	High-incom	k1gInSc5
OECD	OECD	kA
Countries	Countriesa	k1gFnPc2
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
American	American	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
Medicine	Medicin	k1gInSc5
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
129	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
266	#num#	k4
<g/>
–	–	k?
<g/>
273	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
9343	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
amjmed	amjmed	k1gInSc1
<g/>
.2015	.2015	k4
<g/>
.10	.10	k4
<g/>
.025	.025	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
English	English	k1gInSc1
<g/>
)	)	kIx)
↑	↑	k?
FUCHS	Fuchs	k1gMnSc1
<g/>
,	,	kIx,
Erin	Erin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Why	Why	k1gFnSc1
Louisiana	Louisiana	k1gFnSc1
Is	Is	k1gFnSc2
The	The	k1gMnSc1
Murder	Murder	k1gMnSc1
Capital	Capital	k1gMnSc1
Of	Of	k1gMnSc1
America	America	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Business	business	k1gInSc1
Insider	insider	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
https://www.bjs.gov/content/pub/pdf/htus8008.pdf	https://www.bjs.gov/content/pub/pdf/htus8008.pdf	k1gInSc1
<g/>
↑	↑	k?
Maryland	Maryland	k1gInSc1
becomes	becomes	k1gMnSc1
latest	latest	k1gMnSc1
U.	U.	kA
<g/>
S.	S.	kA
state	status	k1gInSc5
to	ten	k3xDgNnSc4
abolish	abolish	k1gMnSc1
death	death	k1gMnSc1
penalty	penalta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reuters	Reuters	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Searchable	Searchable	k1gFnSc1
Execution	Execution	k1gInSc1
Database	Databasa	k1gFnSc3
|	|	kIx~
Death	Death	k1gInSc1
Penalty	penalty	k1gNnSc2
Information	Information	k1gInSc1
Center	centrum	k1gNnPc2
<g/>
.	.	kIx.
deathpenaltyinfo	deathpenaltyinfo	k6eAd1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
A	a	k8xC
dramatic	dramatice	k1gFnPc2
global	globat	k5eAaImAgInS
rise	rise	k1gInSc1
in	in	k?
the	the	k?
number	number	k1gInSc1
of	of	k?
executions	executions	k1gInSc1
recorded	recorded	k1gInSc1
in	in	k?
2015	#num#	k4
saw	saw	k?
more	mor	k1gInSc5
people	people	k6eAd1
put	put	k0
to	ten	k3xDgNnSc4
death	death	k1gMnSc1
than	than	k1gMnSc1
at	at	k?
any	any	k?
point	pointa	k1gFnPc2
in	in	k?
the	the	k?
last	last	k1gInSc4
quarter-century	quarter-centura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amnesty	Amnest	k1gInPc1
International	International	k1gMnSc1
USA	USA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
IADICOLA	IADICOLA	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
;	;	kIx,
SHUPE	SHUPE	kA
<g/>
,	,	kIx,
Anson	Anson	k1gMnSc1
D.	D.	kA
Violence	Violence	k1gFnPc1
<g/>
,	,	kIx,
Inequality	Inequalit	k1gInPc1
<g/>
,	,	kIx,
and	and	k?
Human	Human	k1gInSc1
Freedom	Freedom	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Rowman	Rowman	k1gMnSc1
&	&	k?
Littlefield	Littlefield	k1gMnSc1
537	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
9781442209497	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Google-Books-ID	Google-Books-ID	k1gFnSc1
<g/>
:	:	kIx,
KSp	KSp	k1gFnSc1
<g/>
0	#num#	k4
<g/>
Ulmx	Ulmx	k1gInSc1
<g/>
44	#num#	k4
<g/>
kC	kC	k?
<g/>
.	.	kIx.
↑	↑	k?
Report	report	k1gInSc1
<g/>
:	:	kIx,
Oklahoma	Oklahoma	k1gFnSc1
now	now	k?
'	'	kIx"
<g/>
world	world	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
prison	prison	k1gMnSc1
capital	capital	k1gMnSc1
<g/>
'	'	kIx"
<g/>
.	.	kIx.
www.kake.com	www.kake.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Největším	veliký	k2eAgInSc7d3
zabijákem	zabiják	k1gInSc7
Američanů	Američan	k1gMnPc2
do	do	k7c2
50	#num#	k4
let	léto	k1gNnPc2
jsou	být	k5eAaImIp3nP
drogy	droga	k1gFnPc1
<g/>
,	,	kIx,
závislost	závislost	k1gFnSc1
na	na	k7c6
opiátech	opiát	k1gInPc6
je	být	k5eAaImIp3nS
podle	podle	k7c2
statistik	statistika	k1gFnPc2
devastující	devastující	k2eAgFnSc1d1
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
US	US	kA
drug	drug	k1gMnSc1
overdose	overdosa	k1gFnSc3
deaths	deaths	k6eAd1
rose	rosa	k1gFnSc6
to	ten	k3xDgNnSc1
record	record	k6eAd1
72,000	72,000	k4
last	lastum	k1gNnPc2
year	yeara	k1gFnPc2
<g/>
,	,	kIx,
data	datum	k1gNnSc2
reveals	revealsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
U.	U.	kA
<g/>
S.	S.	kA
No	no	k9
Longer	Longer	k1gInSc4
World	World	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Largest	Largest	k1gMnSc1
Economy	Econom	k1gInPc4
<g/>
,	,	kIx,
Kimberly	Kimberl	k1gInPc4
Amadeo	Amadeo	k6eAd1
<g/>
,	,	kIx,
12	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2008	#num#	k4
<g/>
↑	↑	k?
China	China	k1gFnSc1
Eclipses	Eclipses	k1gMnSc1
U.	U.	kA
<g/>
S.	S.	kA
as	as	k1gInSc1
Biggest	Biggest	k1gInSc1
Trading	Trading	k1gInSc1
Nation	Nation	k1gInSc4
<g/>
↑	↑	k?
Nejcennější	cenný	k2eAgFnSc2d3
firmy	firma	k1gFnSc2
světa	svět	k1gInSc2
<g/>
:	:	kIx,
Trhu	trh	k1gInSc2
vládnou	vládnout	k5eAaImIp3nP
američtí	americký	k2eAgMnPc1d1
technologičtí	technologický	k2eAgMnPc1d1
obři	obr	k1gMnPc1
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
ztrácí	ztrácet	k5eAaImIp3nS
dech	dech	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-08-12	2019-08-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Nejcennější	cenný	k2eAgFnSc7d3
značkou	značka	k1gFnSc7
světa	svět	k1gInSc2
zůstává	zůstávat	k5eAaImIp3nS
Apple	Apple	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1
jsou	být	k5eAaImIp3nP
Google	Googl	k1gMnSc4
a	a	k8xC
Amazon	amazona	k1gFnPc2
<g/>
,	,	kIx,
vyplývá	vyplývat	k5eAaImIp3nS
z	z	k7c2
žebříčku	žebříček	k1gInSc2
společnosti	společnost	k1gFnSc2
Interbrand	Interbranda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
(	(	kIx(
<g/>
IHNED	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-10-17	2019-10-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Debt	Debt	k2eAgMnSc1d1
to	ten	k3xDgNnSc1
the	the	k?
Penny	penny	k1gInSc1
(	(	kIx(
<g/>
Daily	Daila	k1gFnPc1
History	Histor	k1gInPc1
Search	Search	k1gInSc4
Application	Application	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.treasurydirect.gov	www.treasurydirect.gov	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Major	major	k1gMnSc1
Foreign	Foreign	k1gMnSc1
Holders	Holdersa	k1gFnPc2
of	of	k?
Treasury	Treasura	k1gFnSc2
Securities	Securitiesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Department	department	k1gInSc1
of	of	k?
the	the	k?
Treasury	Treasur	k1gMnPc7
<g/>
/	/	kIx~
<g/>
Federal	Federal	k1gMnPc7
Reserve	Reserev	k1gFnSc2
Board	Board	k1gInSc1
(	(	kIx(
<g/>
Report	report	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
http://ticdata.treasury.gov/Publish/mfh.txt,	http://ticdata.treasury.gov/Publish/mfh.txt,	k?
17	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2018	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Pět	pět	k4xCc1
milionů	milion	k4xCgInPc2
imigrantů	imigrant	k1gMnPc2
unikne	uniknout	k5eAaPmIp3nS
vyhoštění	vyhoštění	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obama	Obama	k?
oznámil	oznámit	k5eAaPmAgMnS
klíčovou	klíčový	k2eAgFnSc4d1
reformu	reforma	k1gFnSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-11-21	2014-11-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
"	"	kIx"
<g/>
Obamova	Obamův	k2eAgFnSc1d1
imigrační	imigrační	k2eAgFnSc1d1
reforma	reforma	k1gFnSc1
má	mít	k5eAaImIp3nS
zatím	zatím	k6eAd1
utrum	utrum	k1gNnSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
.	.	kIx.
17	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Pět	pět	k4xCc4
nejúžasnějších	úžasný	k2eAgNnPc2d3
míst	místo	k1gNnPc2
USA	USA	kA
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
musíte	muset	k5eAaImIp2nP
vidět	vidět	k5eAaImF
<g/>
:	:	kIx,
Obří	obří	k2eAgFnSc1d1
arizonská	arizonský	k2eAgFnSc1d1
„	„	k?
<g/>
podkova	podkova	k1gFnSc1
<g/>
”	”	k?
a	a	k8xC
další	další	k2eAgFnPc4d1
lahůdky	lahůdka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Čtyři	čtyři	k4xCgMnPc1
prezidenti	prezident	k1gMnPc1
za	za	k7c4
milión	milión	k4xCgInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Monument	monument	k1gInSc1
Mount	Mount	k1gInSc4
Rushmore	Rushmor	k1gInSc5
udivuje	udivovat	k5eAaImIp3nS
už	už	k9
osmé	osmý	k4xOgNnSc4
desetiletí	desetiletí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Photos	Photos	k1gMnSc1
for	forum	k1gNnPc2
official	officiat	k5eAaImAgMnS,k5eAaPmAgMnS,k5eAaBmAgMnS
documents	documents	k1gInSc4
in	in	k?
the	the	k?
USA	USA	kA
/	/	kIx~
Passport	Passport	k1gInSc1
Photo	Phota	k1gMnSc5
Online	Onlin	k1gMnSc5
-	-	kIx~
Passport-Photo	Passport-Phota	k1gFnSc5
<g/>
.	.	kIx.
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
passport-photo	passport-phota	k1gFnSc5
<g/>
.	.	kIx.
<g/>
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
How	How	k1gFnSc4
Many	mana	k1gFnSc2
Cars	Cars	k1gInSc4
Per	pero	k1gNnPc2
Capita	Capit	k1gMnSc2
in	in	k?
the	the	k?
US	US	kA
(	(	kIx(
<g/>
Upd	Upd	k1gMnSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
National	National	k1gMnSc1
Household	Household	k1gMnSc1
Travel	Travel	k1gMnSc1
Survey	Survea	k1gFnSc2
-	-	kIx~
Highlights	Highlights	k1gInSc1
from	from	k1gInSc1
the	the	k?
2001	#num#	k4
<g/>
.	.	kIx.
web.archive.org	web.archive.org	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2005-05-13	2005-05-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
Matka	matka	k1gFnSc1
silnic	silnice	k1gFnPc2
<g/>
:	:	kIx,
Legendární	legendární	k2eAgFnSc1d1
Route	Rout	k1gInSc5
66	#num#	k4
lemují	lemovat	k5eAaImIp3nP
i	i	k9
cadillaky	cadillac	k1gInPc1
zahrabané	zahrabaný	k2eAgInPc1d1
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Why	Why	k1gMnSc1
don	don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Americans	Americans	k1gInSc1
ride	ridat	k5eAaPmIp3nS
trains	trains	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Economist	Economist	k1gMnSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
13	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
613	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
https://web.archive.org/web/20120509091022/http://www.amtrak.com/servlet/BlobServer?blobcol=urldata&	https://web.archive.org/web/20120509091022/http://www.amtrak.com/servlet/BlobServer?blobcol=urldata&	k1gInSc1
<g/>
↑	↑	k?
Preliminary	Preliminara	k1gFnSc2
World	Worlda	k1gFnPc2
Airport	Airport	k1gInSc1
Traffic	Traffic	k1gMnSc1
and	and	k?
Rankings	Rankings	k1gInSc1
2013	#num#	k4
-	-	kIx~
High	High	k1gMnSc1
Growth	Growth	k1gMnSc1
Dubai	Duba	k1gFnSc2
Moves	Moves	k1gMnSc1
Up	Up	k1gMnSc1
to	ten	k3xDgNnSc4
7	#num#	k4
<g/>
th	th	k?
Busiest	Busiest	k1gInSc1
Airport	Airport	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ACI	ACI	kA
World	Worlda	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014-03-31	2014-03-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Boeing	boeing	k1gInSc1
says	says	k1gInSc1
it	it	k?
<g/>
'	'	kIx"
<g/>
s	s	k7c7
flying	flying	k1gInSc1
high	high	k1gInSc1
despite	despit	k1gInSc5
recession	recession	k1gInSc1
-	-	kIx~
USATODAY	USATODAY	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
usatoday	usatodaa	k1gFnSc2
<g/>
30	#num#	k4
<g/>
.	.	kIx.
<g/>
usatoday	usatodaa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Top	topit	k5eAaImRp2nS
10	#num#	k4
Most	most	k1gInSc1
Popular	Popular	k1gMnSc1
Sports	Sportsa	k1gFnPc2
in	in	k?
America	America	k1gMnSc1
(	(	kIx(
<g/>
TV	TV	kA
Ratings	Ratings	k1gInSc1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
2019	#num#	k4
Update	update	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sporteology	Sporteolog	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-02-19	2019-02-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Americké	americký	k2eAgNnSc1d1
divadlo	divadlo	k1gNnSc1
<g/>
.	.	kIx.
leporelo	leporelo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
info	info	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Broadway	Broadwaa	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gInPc1
slavné	slavný	k2eAgInPc1d1
muzikály	muzikál	k1gInPc1
<g/>
:	:	kIx,
Prkna	prkno	k1gNnPc1
<g/>
,	,	kIx,
co	co	k9
znamenají	znamenat	k5eAaImIp3nP
svět	svět	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poznávací	poznávací	k2eAgInPc1d1
zájezdy	zájezd	k1gInPc1
|	|	kIx~
mazaně	mazaně	k6eAd1
s	s	k7c7
Radynacestu	Radynacest	k1gInSc6
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Time	Timus	k1gMnSc5
Europe	Europ	k1gMnSc5
Media	medium	k1gNnPc1
Planner	Planner	k1gMnSc1
<g/>
,	,	kIx,
http://www.time.com/time/europe/mediakit/circulation	http://www.time.com/time/europe/mediakit/circulation	k1gInSc1
<g/>
↑	↑	k?
What	What	k1gInSc1
<g/>
,	,	kIx,
When	When	k1gInSc1
and	and	k?
Where	Wher	k1gInSc5
Americans	Americansa	k1gFnPc2
Eat	Eat	k1gMnSc7
in	in	k?
2003	#num#	k4
|	|	kIx~
Newswise	Newswise	k1gFnSc1
<g/>
:	:	kIx,
News	News	k1gInSc1
for	forum	k1gNnPc2
Journalists	Journalists	k1gInSc1
<g/>
.	.	kIx.
www.newswise.com	www.newswise.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
https://jobstime.cz/kuchyne-texmex/	https://jobstime.cz/kuchyne-texmex/	k?
<g/>
↑	↑	k?
Americká	americký	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
<g/>
:	:	kIx,
Rychle	rychle	k6eAd1
<g/>
,	,	kIx,
nezdravě	zdravě	k6eNd1
a	a	k8xC
chutně	chutně	k6eAd1
<g/>
!	!	kIx.
</s>
<s desamb="1">
Aneb	Aneb	k?
lekce	lekce	k1gFnSc1
stolování	stolování	k1gNnPc2
pro	pro	k7c4
nenáročné	náročný	k2eNgMnPc4d1
turisty	turist	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poznávací	poznávací	k2eAgInPc1d1
zájezdy	zájezd	k1gInPc1
|	|	kIx~
mazaně	mazaně	k6eAd1
s	s	k7c7
Radynacestu	Radynacest	k1gInSc6
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
QS	QS	kA
World	World	k1gInSc4
University	universita	k1gFnSc2
Rankings	Rankings	k1gInSc4
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Top	topit	k5eAaImRp2nS
Universities	Universities	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-05-29	2018-05-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Americký	americký	k2eAgInSc1d1
sen	sen	k1gInSc1
</s>
<s>
Slib	slib	k1gInSc1
věrnosti	věrnost	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
amerických	americký	k2eAgMnPc2d1
prezidentů	prezident	k1gMnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
pečetí	pečeť	k1gFnSc7
států	stát	k1gInPc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
Seznam	seznam	k1gInSc1
vlajek	vlajka	k1gFnPc2
států	stát	k1gInPc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Téma	téma	k1gNnSc1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Kategorie	kategorie	k1gFnPc1
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgInPc4d1
ve	v	k7c6
Wikizprávách	Wikizpráva	k1gFnPc6
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Cestovní	cestovní	k2eAgFnSc1d1
příručka	příručka	k1gFnSc1
na	na	k7c6
Wikivoyage	Wikivoyage	k1gFnSc6
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Infoamerika	Infoamerika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Velvyslanectví	velvyslanectví	k1gNnSc1
USA	USA	kA
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Americké	americký	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Ministerstvo	ministerstvo	k1gNnSc1
zahraničních	zahraniční	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
–	–	k?
USA	USA	kA
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Sborník	sborník	k1gInSc1
z	z	k7c2
Bílého	bílý	k2eAgInSc2d1
domu	dům	k1gInSc2
k	k	k7c3
volnému	volný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
pod	pod	k7c7
licencí	licence	k1gFnSc7
Creative	Creativ	k1gInSc5
Commons	Commons	k1gInSc4
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
/	/	kIx~
Podnikání	podnikání	k1gNnPc1
a	a	k8xC
firmy	firma	k1gFnPc1
v	v	k7c6
USA	USA	kA
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
CDC	CDC	kA
<g/>
.	.	kIx.
<g/>
gov	gov	k?
–	–	k?
data	datum	k1gNnSc2
k	k	k7c3
zdravotnímu	zdravotní	k2eAgNnSc3d1
pojištění	pojištění	k1gNnSc3
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
HHS	HHS	kA
<g/>
.	.	kIx.
<g/>
gov	gov	k?
–	–	k?
průběh	průběh	k1gInSc4
a	a	k8xC
fakta	faktum	k1gNnPc4
k	k	k7c3
reformě	reforma	k1gFnSc3
zdravotního	zdravotní	k2eAgNnSc2d1
pojištění	pojištění	k1gNnSc2
dle	dle	k7c2
let	léto	k1gNnPc2
</s>
<s>
/	/	kIx~
<g/>
CDC	CDC	kA
data	datum	k1gNnSc2
k	k	k7c3
zdravotnímu	zdravotní	k2eAgNnSc3d1
pojištění	pojištění	k1gNnSc3
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
]	]	kIx)
</s>
<s>
http://www.nytimes.com/2013/02/17/magazine/do-illegal-immigrants-actually-hurt-the-us-economy.html?pagewanted=all&	http://www.nytimes.com/2013/02/17/magazine/do-illegal-immigrants-actually-hurt-the-us-economy.html?pagewanted=all&	k?
</s>
<s>
United	United	k1gMnSc1
States	States	k1gMnSc1
of	of	k?
America	America	k1gMnSc1
-	-	kIx~
Amnesty	Amnest	k1gInPc1
International	International	k1gFnSc1
Report	report	k1gInSc1
2011	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amnesty	Amnest	k1gInPc4
International	International	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
United	United	k1gMnSc1
States	States	k1gMnSc1
of	of	k?
America	America	k1gMnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Freedom	Freedom	k1gInSc1
House	house	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
USA	USA	kA
-	-	kIx~
o	o	k7c6
českých	český	k2eAgMnPc6d1
krajanech	krajan	k1gMnPc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
zahraničních	zahraniční	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
2002-06-20	2002-06-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
CIA	CIA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
World	World	k1gMnSc1
Factbook	Factbook	k1gInSc1
-	-	kIx~
United	United	k1gMnSc1
States	States	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rev	Rev	k1gFnSc1
<g/>
.	.	kIx.
2011-07-26	2011-07-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1
úřad	úřad	k1gInSc1
ČR	ČR	kA
ve	v	k7c6
Washingtonu	Washington	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souhrnná	souhrnný	k2eAgFnSc1d1
teritoriální	teritoriální	k2eAgFnSc1d1
informace	informace	k1gFnSc1
<g/>
:	:	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgFnSc2d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Businessinfo	Businessinfo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2011-04-01	2011-04-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
BEEMAN	BEEMAN	kA
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
R	R	kA
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gMnSc1
States	States	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopaedia	Encyclopaedium	k1gNnSc2
Britannica	Britannicum	k1gNnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gNnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc4
<g/>
}	}	kIx)
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgFnSc2d1
–	–	k?
United	United	k1gMnSc1
States	States	k1gMnSc1
of	of	k?
America	America	k1gMnSc1
–	–	k?
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
Státy	stát	k1gInPc1
</s>
<s>
Alabama	Alabama	k1gFnSc1
•	•	k?
Aljaška	Aljaška	k1gFnSc1
•	•	k?
Arizona	Arizona	k1gFnSc1
•	•	k?
Arkansas	Arkansas	k1gInSc1
•	•	k?
Colorado	Colorado	k1gNnSc1
•	•	k?
Connecticut	Connecticut	k1gMnSc1
•	•	k?
Delaware	Delawar	k1gMnSc5
•	•	k?
Florida	Florida	k1gFnSc1
•	•	k?
Georgie	Georgie	k1gFnSc1
•	•	k?
Havaj	Havaj	k1gFnSc1
•	•	k?
Idaho	Ida	k1gMnSc2
•	•	k?
Illinois	Illinois	k1gFnSc2
•	•	k?
Indiana	Indiana	k1gFnSc1
•	•	k?
Iowa	Iow	k1gInSc2
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Dakota	Dakota	k1gFnSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Karolína	Karolína	k1gFnSc1
•	•	k?
Kalifornie	Kalifornie	k1gFnSc2
•	•	k?
Kansas	Kansas	k1gInSc1
•	•	k?
Kentucky	Kentucka	k1gFnSc2
•	•	k?
Louisiana	Louisiana	k1gFnSc1
•	•	k?
Maine	Main	k1gInSc5
•	•	k?
Maryland	Marylanda	k1gFnPc2
•	•	k?
Massachusetts	Massachusetts	k1gNnSc4
•	•	k?
Michigan	Michigan	k1gInSc1
•	•	k?
Minnesota	Minnesota	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Mississippi	Mississippi	k1gFnSc2
•	•	k?
Missouri	Missouri	k1gFnSc2
•	•	k?
Montana	Montana	k1gFnSc1
•	•	k?
Nebraska	Nebraska	k1gFnSc1
•	•	k?
Nevada	Nevada	k1gFnSc1
•	•	k?
New	New	k1gMnSc5
Hampshire	Hampshir	k1gMnSc5
•	•	k?
New	New	k1gMnSc4
Jersey	Jersea	k1gFnSc2
•	•	k?
New	New	k1gFnSc1
York	York	k1gInSc1
•	•	k?
Nové	Nové	k2eAgNnSc1d1
Mexiko	Mexiko	k1gNnSc1
•	•	k?
Ohio	Ohio	k1gNnSc1
•	•	k?
Oklahoma	Oklahom	k1gMnSc2
•	•	k?
Oregon	Oregon	k1gMnSc1
•	•	k?
Pensylvánie	Pensylvánie	k1gFnSc1
•	•	k?
Rhode	Rhodos	k1gInSc5
Island	Island	k1gInSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Dakota	Dakota	k1gFnSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Karolína	Karolína	k1gFnSc1
•	•	k?
Tennessee	Tennessee	k1gInSc1
•	•	k?
Texas	Texas	k1gInSc1
•	•	k?
Utah	Utah	k1gInSc1
•	•	k?
Vermont	Vermont	k1gInSc1
•	•	k?
Virginie	Virginie	k1gFnSc1
•	•	k?
Washington	Washington	k1gInSc1
•	•	k?
Wisconsin	Wisconsin	k1gInSc1
•	•	k?
Wyoming	Wyoming	k1gInSc4
•	•	k?
Západní	západní	k2eAgFnSc2d1
Virginie	Virginie	k1gFnSc2
Federální	federální	k2eAgInSc1d1
distrikt	distrikt	k1gInSc1
</s>
<s>
District	District	k1gInSc1
of	of	k?
Columbia	Columbia	k1gFnSc1
Autonomní	autonomní	k2eAgFnSc1d1
ostrovní	ostrovní	k2eAgNnSc4d1
území	území	k1gNnSc4
</s>
<s>
Americká	americký	k2eAgFnSc1d1
Samoa	Samoa	k1gFnSc1
•	•	k?
Americké	americký	k2eAgInPc4d1
Panenské	panenský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
•	•	k?
Guam	Guam	k1gInSc1
•	•	k?
Portoriko	Portoriko	k1gNnSc1
•	•	k?
Severní	severní	k2eAgFnSc2d1
Mariany	Mariana	k1gFnSc2
Ostrovní	ostrovní	k2eAgNnSc4d1
území	území	k1gNnSc4
podpřímou	podpřímý	k2eAgFnSc7d1
správou	správa	k1gFnSc7
vlády	vláda	k1gFnSc2
USA	USA	kA
</s>
<s>
Bakerův	Bakerův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
•	•	k?
Howlandův	Howlandův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
•	•	k?
Jarvisův	Jarvisův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
•	•	k?
Johnstonův	Johnstonův	k2eAgInSc1d1
atol	atol	k1gInSc1
•	•	k?
Kingmanův	Kingmanův	k2eAgInSc1d1
útes	útes	k1gInSc1
•	•	k?
Midway	Midwaa	k1gFnSc2
•	•	k?
Navassa	Navassa	k1gFnSc1
•	•	k?
Palmyra	Palmyra	k1gFnSc1
•	•	k?
Wake	Wak	k1gInSc2
•	•	k?
sporná	sporný	k2eAgFnSc1d1
území	území	k1gNnSc6
<g/>
:	:	kIx,
Bajo	Bajo	k6eAd1
Nuevo	Nuevo	k1gNnSc1
•	•	k?
Serranilla	Serranilla	k1gMnSc1
</s>
<s>
Státy	stát	k1gInPc1
a	a	k8xC
území	území	k1gNnPc1
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
a	a	k8xC
Střední	střední	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
Nezávislé	závislý	k2eNgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Antigua	Antigua	k1gFnSc1
a	a	k8xC
Barbuda	Barbuda	k1gFnSc1
•	•	k?
Bahamy	Bahamy	k1gFnPc4
•	•	k?
Barbados	Barbados	k1gMnSc1
•	•	k?
Belize	Belize	k1gFnSc2
•	•	k?
Dominika	Dominik	k1gMnSc2
•	•	k?
Dominikánská	dominikánský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Grenada	Grenada	k1gFnSc1
•	•	k?
Guatemala	Guatemala	k1gFnSc1
•	•	k?
Haiti	Haiti	k1gNnSc4
•	•	k?
Honduras	Honduras	k1gInSc1
•	•	k?
Jamajka	Jamajka	k1gFnSc1
•	•	k?
Kanada	Kanada	k1gFnSc1
•	•	k?
Kostarika	Kostarika	k1gFnSc1
•	•	k?
Kuba	Kuba	k1gFnSc1
•	•	k?
Mexiko	Mexiko	k1gNnSc1
•	•	k?
Nikaragua	Nikaragua	k1gFnSc1
•	•	k?
Panama	Panama	k1gFnSc1
•	•	k?
Salvador	Salvador	k1gInSc4
•	•	k?
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgFnSc2d1
•	•	k?
Svatý	svatý	k1gMnSc1
Kryštof	Kryštof	k1gMnSc1
a	a	k8xC
Nevis	viset	k5eNaImRp2nS
•	•	k?
Svatá	svatý	k2eAgFnSc1d1
Lucie	Lucie	k1gFnSc1
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Vincenc	Vincenc	k1gMnSc1
a	a	k8xC
Grenadiny	grenadina	k1gFnPc1
•	•	k?
Trinidad	Trinidad	k1gInSc1
a	a	k8xC
Tobago	Tobago	k1gNnSc1
Součásti	součást	k1gFnSc2
suverénních	suverénní	k2eAgInPc2d1
států	stát	k1gInPc2
</s>
<s>
Francie	Francie	k1gFnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Guadeloupe	Guadeloupe	k1gFnSc1
•	•	k?
Martinik	Martinik	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Kolumbie	Kolumbie	k1gFnSc1
(	(	kIx(
<g/>
San	San	k1gFnSc1
Andrés	Andrés	k1gInSc1
a	a	k8xC
Providencia	Providencia	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Nizozemsko	Nizozemsko	k1gNnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Bonaire	Bonair	k1gMnSc5
•	•	k?
Saba	Sab	k1gInSc2
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Eustach	Eustach	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Venezuela	Venezuela	k1gFnSc1
(	(	kIx(
<g/>
Federální	federální	k2eAgFnSc1d1
dependence	dependence	k1gFnSc1
•	•	k?
Nueva	Nueva	k1gFnSc1
Esparta	esparto	k1gNnSc2
<g/>
)	)	kIx)
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
kolonie	kolonie	k1gFnSc2
azávislá	azávislý	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Dánské	dánský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Grónsko	Grónsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
Francie	Francie	k1gFnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Clippertonův	Clippertonův	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Bartoloměj	Bartoloměj	k1gMnSc1
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Martin	Martin	k1gMnSc1
•	•	k?
Saint	Saint	k1gMnSc1
Pierre	Pierr	k1gInSc5
a	a	k8xC
Miquelon	Miquelon	k1gInSc1
<g/>
)	)	kIx)
<g/>
Nizozemské	nizozemský	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Aruba	Aruba	k1gFnSc1
•	•	k?
Curaçao	curaçao	k1gNnSc1
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Martin	Martin	k1gMnSc1
<g/>
)	)	kIx)
<g/>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Anguilla	Anguilla	k1gFnSc1
•	•	k?
Bermudy	Bermudy	k1gFnPc4
•	•	k?
Britské	britský	k2eAgNnSc1d1
Panenské	panenský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Kajmanské	Kajmanský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Montserrat	Montserrat	k1gInSc1
•	•	k?
Turks	Turksa	k1gFnPc2
a	a	k8xC
Caicos	Caicosa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
<g/>
:	:	kIx,
(	(	kIx(
<g/>
Americké	americký	k2eAgInPc4d1
Panenské	panenský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
•	•	k?
Navassa	Navassa	k1gFnSc1
•	•	k?
Portoriko	Portoriko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Členství	členství	k1gNnSc1
v	v	k7c6
mezinárodních	mezinárodní	k2eAgFnPc6d1
organizacích	organizace	k1gFnPc6
</s>
<s>
Severoatlantická	severoatlantický	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
(	(	kIx(
<g/>
NATO	NATO	kA
<g/>
)	)	kIx)
</s>
<s>
Albánie	Albánie	k1gFnSc1
•	•	k?
Belgie	Belgie	k1gFnSc1
•	•	k?
Bulharsko	Bulharsko	k1gNnSc1
•	•	k?
Černá	černat	k5eAaImIp3nS
Hora	hora	k1gFnSc1
•	•	k?
Česko	Česko	k1gNnSc1
•	•	k?
Dánsko	Dánsko	k1gNnSc1
•	•	k?
Estonsko	Estonsko	k1gNnSc1
•	•	k?
Francie	Francie	k1gFnSc2
•	•	k?
Chorvatsko	Chorvatsko	k1gNnSc4
•	•	k?
Island	Island	k1gInSc1
•	•	k?
Itálie	Itálie	k1gFnSc2
•	•	k?
Kanada	Kanada	k1gFnSc1
•	•	k?
Litva	Litva	k1gFnSc1
•	•	k?
Lotyšsko	Lotyšsko	k1gNnSc1
•	•	k?
Lucembursko	Lucembursko	k1gNnSc1
•	•	k?
Maďarsko	Maďarsko	k1gNnSc1
•	•	k?
Německo	Německo	k1gNnSc1
•	•	k?
Nizozemsko	Nizozemsko	k1gNnSc1
•	•	k?
Norsko	Norsko	k1gNnSc1
•	•	k?
Polsko	Polsko	k1gNnSc1
•	•	k?
Portugalsko	Portugalsko	k1gNnSc1
•	•	k?
Rumunsko	Rumunsko	k1gNnSc1
•	•	k?
Řecko	Řecko	k1gNnSc1
•	•	k?
Severní	severní	k2eAgFnSc2d1
Makedonie	Makedonie	k1gFnSc2
•	•	k?
Slovensko	Slovensko	k1gNnSc1
•	•	k?
Slovinsko	Slovinsko	k1gNnSc1
•	•	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Španělsko	Španělsko	k1gNnSc1
•	•	k?
Turecko	Turecko	k1gNnSc1
•	•	k?
USA	USA	kA
</s>
<s>
G8	G8	k4
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
•	•	k?
Francie	Francie	k1gFnSc2
•	•	k?
Itálie	Itálie	k1gFnSc2
•	•	k?
Japonsko	Japonsko	k1gNnSc1
•	•	k?
Kanada	Kanada	k1gFnSc1
•	•	k?
Německo	Německo	k1gNnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
(	(	kIx(
<g/>
členství	členství	k1gNnSc1
pozastaveno	pozastaven	k2eAgNnSc1d1
<g/>
)	)	kIx)
•	•	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
<s>
G20	G20	k4
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Argentina	Argentina	k1gFnSc1
•	•	k?
Brazílie	Brazílie	k1gFnSc2
•	•	k?
Kanada	Kanada	k1gFnSc1
•	•	k?
Mexiko	Mexiko	k1gNnSc1
•	•	k?
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgFnSc2d1
•	•	k?
Indonésie	Indonésie	k1gFnSc2
•	•	k?
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
•	•	k?
Indie	Indie	k1gFnSc2
•	•	k?
Čína	Čína	k1gFnSc1
•	•	k?
Japonsko	Japonsko	k1gNnSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Turecko	Turecko	k1gNnSc1
•	•	k?
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
•	•	k?
Francie	Francie	k1gFnSc2
•	•	k?
Německo	Německo	k1gNnSc1
•	•	k?
Itálie	Itálie	k1gFnSc2
•	•	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Austrálie	Austrálie	k1gFnSc2
</s>
<s>
Státy	stát	k1gInPc1
a	a	k8xC
území	území	k1gNnSc1
s	s	k7c7
členstvím	členství	k1gNnSc7
v	v	k7c6
APEC	APEC	kA
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
•	•	k?
Brunej	Brunej	k1gFnSc1
•	•	k?
Čína	Čína	k1gFnSc1
•	•	k?
Filipíny	Filipíny	k1gFnPc4
•	•	k?
Hongkong	Hongkong	k1gInSc1
•	•	k?
Chile	Chile	k1gNnSc2
•	•	k?
Indonésie	Indonésie	k1gFnSc1
•	•	k?
Japonsko	Japonsko	k1gNnSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Kanada	Kanada	k1gFnSc1
•	•	k?
Malajsie	Malajsie	k1gFnSc1
•	•	k?
Mexiko	Mexiko	k1gNnSc1
•	•	k?
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
•	•	k?
Papua	Papuum	k1gNnSc2
Nová	nový	k2eAgFnSc1d1
Guinea	Guinea	k1gFnSc1
•	•	k?
Peru	prát	k5eAaImIp1nS
•	•	k?
Rusko	Rusko	k1gNnSc4
•	•	k?
Singapur	Singapur	k1gInSc1
•	•	k?
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgInPc4d1
•	•	k?
Thajsko	Thajsko	k1gNnSc1
•	•	k?
Tchaj-wan	Tchaj-wan	k1gInSc1
•	•	k?
Vietnam	Vietnam	k1gInSc1
</s>
<s>
Blízkovýchodní	blízkovýchodní	k2eAgInSc1d1
kvartet	kvartet	k1gInSc1
Jednající	jednající	k2eAgFnSc2d1
strany	strana	k1gFnSc2
</s>
<s>
Izrael	Izrael	k1gInSc1
·	·	k?
Palestinská	palestinský	k2eAgFnSc1d1
autonomie	autonomie	k1gFnSc2
Diplomatický	diplomatický	k2eAgInSc1d1
kvartet	kvartet	k1gInSc1
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
unie	unie	k1gFnSc1
(	(	kIx(
<g/>
Mogheriniová	Mogheriniový	k2eAgFnSc1d1
<g/>
)	)	kIx)
·	·	k?
Organizace	organizace	k1gFnSc2
spojených	spojený	k2eAgInPc2d1
národů	národ	k1gInPc2
(	(	kIx(
<g/>
Pan	Pan	k1gMnSc1
<g/>
)	)	kIx)
·	·	k?
Rusko	Rusko	k1gNnSc1
(	(	kIx(
<g/>
Lavrov	Lavrov	k1gInSc1
<g/>
)	)	kIx)
·	·	k?
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
(	(	kIx(
<g/>
Pompeo	Pompeo	k6eAd1
<g/>
)	)	kIx)
Zvláštní	zvláštní	k2eAgMnSc1d1
vyslanec	vyslanec	k1gMnSc1
</s>
<s>
Kito	Kito	k1gMnSc1
de	de	k?
Boer	Boer	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
128584	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4078704-7	4078704-7	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2331	#num#	k4
5230	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
78095330	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
130168302	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
78095330	#num#	k4
</s>
