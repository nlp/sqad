<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
se	se	k3xPyFc4
skládají	skládat	k5eAaImIp3nP
z	z	k7c2
50	#num#	k4
států	stát	k1gInPc2
(	(	kIx(
<g/>
state	status	k1gInSc5
/	/	kIx~
mn	mn	k?
<g/>
.	.	kIx.
č.	č.	k?
states	states	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jednoho	jeden	k4xCgInSc2
federálního	federální	k2eAgInSc2d1
distriktu	distrikt	k1gInSc2
–	–	k?
District	District	k1gMnSc1
of	of	k?
Columbia	Columbia	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
leží	ležet	k5eAaImIp3nS
federální	federální	k2eAgNnSc1d1
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
D.C.	D.C.	k1gFnSc1
(	(	kIx(
<g/>
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
přímo	přímo	k6eAd1
pod	pod	k7c4
jurisdikci	jurisdikce	k1gFnSc4
Kongresu	kongres	k1gInSc2
<g/>
,	,	kIx,
nespadá	spadat	k5eNaImIp3nS,k5eNaPmIp3nS
pod	pod	k7c4
žádný	žádný	k3yNgInSc4
stát	stát	k1gInSc4
a	a	k8xC
oficiálně	oficiálně	k6eAd1
není	být	k5eNaImIp3nS
státem	stát	k1gInSc7
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
s	s	k7c7
nimi	on	k3xPp3gMnPc7
bývá	bývat	k5eAaImIp3nS
často	často	k6eAd1
uváděn	uvádět	k5eAaImNgInS
<g/>
)	)	kIx)
a	a	k8xC
dalších	další	k2eAgNnPc2d1
nezačleněných	začleněný	k2eNgNnPc2d1
území	území	k1gNnPc2
<g/>
,	,	kIx,
např.	např.	kA
ostrovních	ostrovní	k2eAgFnPc2d1
teritorií	teritorium	k1gNnPc2
Portoriko	Portoriko	k1gNnSc4
a	a	k8xC
Severní	severní	k2eAgFnSc2d1
Mariany	Mariana	k1gFnSc2
<g/>
.	.	kIx.
</s>