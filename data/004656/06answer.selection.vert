<s>
Umělá	umělý	k2eAgFnSc1d1	umělá
inteligence	inteligence	k1gFnSc1	inteligence
(	(	kIx(	(
<g/>
UI	UI	kA	UI
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Artificial	Artificial	k1gInSc1	Artificial
intelligence	intelligence	k1gFnSc2	intelligence
<g/>
,	,	kIx,	,
AI	AI	kA	AI
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obor	obor	k1gInSc1	obor
informatiky	informatika	k1gFnSc2	informatika
zabývající	zabývající	k2eAgFnSc2d1	zabývající
se	se	k3xPyFc4	se
tvorbou	tvorba	k1gFnSc7	tvorba
strojů	stroj	k1gInPc2	stroj
vykazujících	vykazující	k2eAgInPc2d1	vykazující
známky	známka	k1gFnPc4	známka
inteligentního	inteligentní	k2eAgNnSc2d1	inteligentní
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
