<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
je	být	k5eAaImIp3nS	být
ČLR	ČLR	kA	ČLR
stálým	stálý	k2eAgInSc7d1	stálý
členem	člen	k1gInSc7	člen
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
s	s	k7c7	s
právem	právo	k1gNnSc7	právo
veta	veto	k1gNnSc2	veto
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
postavení	postavení	k1gNnSc1	postavení
odepřeno	odepřít	k5eAaPmNgNnS	odepřít
Čínské	čínský	k2eAgFnSc6d1	čínská
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
