<s>
Granodiorit	Granodiorit	k1gInSc1
</s>
<s>
Granodiorit	Granodiorit	k1gInSc1
Granodiorit	Granodiorit	k1gInSc1
<g/>
,	,	kIx,
Dúbrava	Dúbrava	k1gFnSc1
<g/>
,	,	kIx,
Nízké	nízký	k2eAgNnSc1d1
TatryZařazení	TatryZařazení	k1gNnSc1
</s>
<s>
vyvřelá	vyvřelý	k2eAgFnSc1d1
hornina	hornina	k1gFnSc1
Hlavní	hlavní	k2eAgFnSc1d1
minerály	minerál	k1gInPc4
</s>
<s>
křemen	křemen	k1gInSc1
<g/>
,	,	kIx,
plagioklas	plagioklas	k1gInSc1
<g/>
,	,	kIx,
ortoklas	ortoklas	k1gInSc1
<g/>
,	,	kIx,
biotit	biotit	k1gInSc1
<g/>
,	,	kIx,
amfibol	amfibol	k1gInSc1
Akcesorie	Akcesorie	k1gFnSc2
</s>
<s>
titanit	titanit	k1gInSc1
<g/>
,	,	kIx,
zirkon	zirkon	k1gInSc1
<g/>
,	,	kIx,
magnetit	magnetit	k1gInSc1
<g/>
,	,	kIx,
apatit	apatit	k1gInSc1
Textura	textura	k1gFnSc1
</s>
<s>
zrnitá	zrnitý	k2eAgFnSc1d1
Barva	barva	k1gFnSc1
<g/>
(	(	kIx(
<g/>
y	y	k?
<g/>
)	)	kIx)
</s>
<s>
bílá	bílý	k2eAgFnSc1d1
<g/>
,	,	kIx,
šedá	šedý	k2eAgFnSc1d1
</s>
<s>
Granodiorit	Granodiorit	k1gInSc1
amfibolicko-biotitický	amfibolicko-biotitický	k2eAgInSc1d1
(	(	kIx(
<g/>
Krhanice	krhanice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Granodiorit	Granodiorit	k1gInSc1
z	z	k7c2
lomu	lom	k1gInSc2
u	u	k7c2
obce	obec	k1gFnSc2
Mrač	mračit	k5eAaImRp2nS
</s>
<s>
Granodiorit	Granodiorit	k1gInSc1
je	být	k5eAaImIp3nS
hlubinná	hlubinný	k2eAgFnSc1d1
vyvřelá	vyvřelý	k2eAgFnSc1d1
hornina	hornina	k1gFnSc1
s	s	k7c7
podstatným	podstatný	k2eAgNnSc7d1
množstvím	množství	k1gNnSc7
křemene	křemen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
blízký	blízký	k2eAgInSc1d1
žule	žula	k1gFnSc3
(	(	kIx(
<g/>
granitu	granit	k1gInSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
které	který	k3yIgFnSc2,k3yQgFnSc2,k3yRgFnSc2
je	být	k5eAaImIp3nS
makroskopicky	makroskopicky	k6eAd1
těžko	těžko	k6eAd1
rozeznatelný	rozeznatelný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Označení	označení	k1gNnSc1
zavedl	zavést	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1893	#num#	k4
americký	americký	k2eAgMnSc1d1
geolog	geolog	k1gMnSc1
G.	G.	kA
F.	F.	kA
Becker	Becker	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Granodiority	Granodiorita	k1gFnPc1
bývají	bývat	k5eAaImIp3nP
světle	světle	k6eAd1
šedé	šedý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
stavba	stavba	k1gFnSc1
je	být	k5eAaImIp3nS
všesměrná	všesměrný	k2eAgFnSc1d1
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
rovnoměrně	rovnoměrně	k6eAd1
zrnitá	zrnitý	k2eAgFnSc1d1
<g/>
,	,	kIx,
zřídka	zřídka	k6eAd1
porfyrická	porfyrický	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Složení	složení	k1gNnSc1
</s>
<s>
Granodiorit	Granodiorit	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
v	v	k7c6
podstatném	podstatný	k2eAgNnSc6d1
množství	množství	k1gNnSc6
křemen	křemen	k1gInSc1
<g/>
,	,	kIx,
plagioklas	plagioklas	k1gInSc1
i	i	k8xC
draselný	draselný	k2eAgInSc1d1
živec	živec	k1gInSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
plagioklas	plagioklas	k1gInSc1
nad	nad	k7c7
draselným	draselný	k2eAgInSc7d1
živcem	živec	k1gInSc7
převládá	převládat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
dalších	další	k2eAgInPc2d1
minerálů	minerál	k1gInPc2
bývá	bývat	k5eAaImIp3nS
často	často	k6eAd1
přítomen	přítomen	k2eAgInSc1d1
biotit	biotit	k1gInSc1
a	a	k8xC
amfibol	amfibol	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muskovit	muskovit	k1gInSc1
bývá	bývat	k5eAaImIp3nS
přítomný	přítomný	k2eAgInSc1d1
pouze	pouze	k6eAd1
vzácně	vzácně	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
pokud	pokud	k8xS
ano	ano	k9
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
je	být	k5eAaImIp3nS
vždy	vždy	k6eAd1
doprovázen	doprovázet	k5eAaImNgInS
biotitem	biotit	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
turmalínem	turmalín	k1gInSc7
se	se	k3xPyFc4
u	u	k7c2
granodioritů	granodiorit	k1gInPc2
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
žul	žula	k1gFnPc2
nesetkáváme	setkávat	k5eNaImIp1nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výskyt	výskyt	k1gInSc1
</s>
<s>
Granodiority	Granodiorita	k1gFnPc1
mnohdy	mnohdy	k6eAd1
tvoří	tvořit	k5eAaImIp3nP
rozsáhlá	rozsáhlý	k2eAgNnPc4d1
tělesa	těleso	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
žulami	žula	k1gFnPc7
jsou	být	k5eAaImIp3nP
nejrozšířenějšími	rozšířený	k2eAgFnPc7d3
hlubinnými	hlubinný	k2eAgFnPc7d1
vyvřelými	vyvřelý	k2eAgFnPc7d1
horninami	hornina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
hojně	hojně	k6eAd1
např.	např.	kA
ve	v	k7c6
středočeském	středočeský	k2eAgInSc6d1
nebo	nebo	k8xC
brněnském	brněnský	k2eAgInSc6d1
plutonu	pluton	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Využití	využití	k1gNnSc1
</s>
<s>
Granodiorit	Granodiorit	k1gInSc1
je	být	k5eAaImIp3nS
těžen	těžit	k5eAaImNgInS
a	a	k8xC
zpracováván	zpracovávat	k5eAaImNgInS
v	v	k7c6
kamenickém	kamenický	k2eAgInSc6d1
průmyslu	průmysl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
něho	on	k3xPp3gInSc2
např.	např.	kA
dekorační	dekorační	k2eAgInPc1d1
obklady	obklad	k1gInPc1
<g/>
,	,	kIx,
různé	různý	k2eAgInPc1d1
obrubníky	obrubník	k1gInPc1
a	a	k8xC
dlažba	dlažba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Památky	památka	k1gFnPc1
</s>
<s>
Tzv.	tzv.	kA
Čertův	čertův	k2eAgInSc1d1
sloup	sloup	k1gInSc1
v	v	k7c6
Karlachových	Karlachův	k2eAgInPc6d1
sadech	sad	k1gInPc6
na	na	k7c6
Vyšehradě	Vyšehrad	k1gInSc6
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
www.geologie.estranky.cz	www.geologie.estranky.cz	k1gInSc1
-	-	kIx~
Přehled	přehled	k1gInSc1
názvů	název	k1gInPc2
hornin	hornina	k1gFnPc2
(	(	kIx(
<g/>
Online	Onlin	k1gInSc5
<g/>
)	)	kIx)
prístup	prístup	k1gInSc1
<g/>
:	:	kIx,
6.11	6.11	k4
<g/>
.2008	.2008	k4
<g/>
↑	↑	k?
HONS	Hons	k1gMnSc1
<g/>
,	,	kIx,
Richard	Richard	k1gMnSc1
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atlas	Atlas	k1gInSc1
našich	náš	k3xOp1gFnPc2
hornin	hornina	k1gFnPc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
ALADIN	ALADIN	kA
agency	agenca	k1gFnSc2
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
.	.	kIx.
200	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
906737	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
33	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
PETRÁNEK	PETRÁNEK	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
;	;	kIx,
A	a	k9
KOL	kol	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyklopedie	encyklopedie	k1gFnSc1
geologie	geologie	k1gFnSc2
<g/>
.	.	kIx.
1	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
geologická	geologický	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
352	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7075	#num#	k4
<g/>
-	-	kIx~
<g/>
901	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
93	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Václav	Václav	k1gMnSc1
Rybařík	rybařík	k1gMnSc1
<g/>
:	:	kIx,
Čertův	čertův	k2eAgInSc1d1
sloup	sloup	k1gInSc1
na	na	k7c6
Vyšehradě	Vyšehrad	k1gInSc6
<g/>
,	,	kIx,
pověsti	pověst	k1gFnPc4
<g/>
,	,	kIx,
dohady	dohad	k1gInPc4
<g/>
,	,	kIx,
fakta	faktum	k1gNnPc4
<g/>
,	,	kIx,
in	in	k?
<g/>
:	:	kIx,
revue	revue	k1gFnSc1
Kámen	kámen	k1gInSc1
<g/>
,	,	kIx,
online	onlinout	k5eAaPmIp3nS
<g/>
:	:	kIx,
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
granodiorit	granodiorita	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Běžné	běžný	k2eAgFnSc2d1
magmatické	magmatický	k2eAgFnSc2d1
horniny	hornina	k1gFnSc2
zatříděné	zatříděný	k2eAgFnSc2d1
podle	podle	k7c2
obsahu	obsah	k1gInSc2
oxidu	oxid	k1gInSc2
křemičitého	křemičitý	k2eAgInSc2d1
</s>
<s>
Typbez	Typbez	k1gMnSc1
SiO	SiO	k1gMnSc1
<g/>
2	#num#	k4
<g/>
Ultramafity	Ultramafita	k1gFnSc2
<g/>
<	<	kIx(
<g/>
45	#num#	k4
<g/>
%	%	kIx~
SiO	SiO	k1gFnSc2
<g/>
2	#num#	k4
<g/>
Mafity	Mafita	k1gFnSc2
<g/>
45	#num#	k4
<g/>
–	–	k?
<g/>
52	#num#	k4
<g/>
%	%	kIx~
SiO	SiO	k1gFnSc6
<g/>
2	#num#	k4
<g/>
Intermediální	intermediální	k2eAgFnSc2d1
horniny	hornina	k1gFnSc2
<g/>
52	#num#	k4
<g/>
–	–	k?
<g/>
63	#num#	k4
<g/>
%	%	kIx~
SiO	SiO	k1gFnSc2
<g/>
2	#num#	k4
<g/>
Intermediální	intermediální	k2eAgFnSc4d1
<g/>
–	–	k?
<g/>
felsické	felsický	k2eAgFnSc2d1
horniny	hornina	k1gFnSc2
<g/>
63	#num#	k4
<g/>
–	–	k?
<g/>
69	#num#	k4
<g/>
%	%	kIx~
SiO	SiO	k1gFnSc6
<g/>
2	#num#	k4
<g/>
Felsické	Felsický	k2eAgFnSc2d1
horniny	hornina	k1gFnSc2
<g/>
>	>	kIx)
<g/>
69	#num#	k4
<g/>
%	%	kIx~
SiO	SiO	k1gFnSc2
<g/>
2	#num#	k4
</s>
<s>
extruzivní	extruzivní	k2eAgFnPc1d1
horniny	hornina	k1gFnPc1
<g/>
:	:	kIx,
<g/>
žilné	žilný	k2eAgFnPc1d1
vyvřeliny	vyvřelina	k1gFnPc1
<g/>
:	:	kIx,
<g/>
hlubinné	hlubinný	k2eAgFnPc1d1
vyvřeliny	vyvřelina	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
TrachytSyenit	TrachytSyenit	k5eAaPmF,k5eAaImF
</s>
<s>
PikritPeridotit	PikritPeridotit	k5eAaImF,k5eAaPmF
</s>
<s>
ČedičDiabas	ČedičDiabas	k1gInSc1
/	/	kIx~
doleritGabro	doleritGabro	k6eAd1
</s>
<s>
AndezitMikrodiorit	AndezitMikrodiorit	k1gInSc1
(	(	kIx(
<g/>
dioritický	dioritický	k2eAgInSc1d1
porfyrit	porfyrit	k1gInSc1
<g/>
)	)	kIx)
<g/>
Diorit	diorit	k1gInSc1
<g/>
,	,	kIx,
gabrodiorit	gabrodiorit	k1gInSc1
</s>
<s>
DacitMikrogranodiorit	DacitMikrogranodiorit	k1gInSc1
(	(	kIx(
<g/>
granodioritový	granodioritový	k2eAgInSc1d1
porfyrit	porfyrit	k1gInSc1
<g/>
)	)	kIx)
<g/>
Granodiorit	Granodiorit	k1gInSc1
</s>
<s>
Ryolit	Ryolit	k1gInSc1
(	(	kIx(
<g/>
liparit	liparit	k1gInSc1
<g/>
)	)	kIx)
<g/>
MikrogranitŽula	MikrogranitŽula	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4331050-3	4331050-3	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
4839	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85056424	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85056424	#num#	k4
</s>
