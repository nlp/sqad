<s>
Avignonské	avignonský	k2eAgFnPc1d1
slečny	slečna	k1gFnPc1
</s>
<s>
Avignonské	avignonský	k2eAgInPc1d1
slečnyAutor	slečnyAutor	k1gInSc1
</s>
<s>
Pablo	Pablo	k1gMnSc1
Picasso	Picasso	k1gMnSc1
Rok	rok	k1gInSc1
vzniku	vznik	k1gInSc3
</s>
<s>
1907	#num#	k4
Technika	technika	k1gFnSc1
</s>
<s>
olej	olej	k1gInSc1
na	na	k7c6
plátně	plátno	k1gNnSc6
Rozměry	rozměra	k1gFnSc2
</s>
<s>
243,9	243,9	k4
×	×	k?
233,7	233,7	k4
cm	cm	kA
Umístění	umístění	k1gNnSc1
</s>
<s>
Muzeum	muzeum	k1gNnSc1
moderního	moderní	k2eAgNnSc2d1
umění	umění	k1gNnSc2
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
</s>
<s>
Avignonské	avignonský	k2eAgFnPc1d1
slečny	slečna	k1gFnPc1
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
Les	les	k1gInSc1
Demoiselles	Demoiselles	k1gInSc1
d	d	k?
<g/>
’	’	k?
<g/>
Avignon	Avignon	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
velká	velký	k2eAgFnSc1d1
olejomalba	olejomalba	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1907	#num#	k4
od	od	k7c2
španělského	španělský	k2eAgMnSc2d1
malíře	malíř	k1gMnSc2
Pabla	Pablo	k1gMnSc2
Picassa	Picasso	k1gMnSc2
(	(	kIx(
<g/>
1881	#num#	k4
<g/>
–	–	k?
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dílo	dílo	k1gNnSc1
zobrazuje	zobrazovat	k5eAaImIp3nS
pět	pět	k4xCc4
nahých	nahý	k2eAgFnPc2d1
prostitutek	prostitutka	k1gFnPc2
z	z	k7c2
nevěstince	nevěstinec	k1gInSc2
v	v	k7c6
ulici	ulice	k1gFnSc6
Avinyó	Avinyó	k1gFnSc2
(	(	kIx(
<g/>
Avignon	Avignon	k1gInSc1
<g/>
)	)	kIx)
v	v	k7c6
Barceloně	Barcelona	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
protokubistické	protokubistický	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
je	být	k5eAaImIp3nS
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
klíčové	klíčový	k2eAgInPc4d1
pro	pro	k7c4
další	další	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
kubismu	kubismus	k1gInSc2
i	i	k8xC
moderního	moderní	k2eAgNnSc2d1
umění	umění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Picasso	Picassa	k1gFnSc5
vytvořil	vytvořit	k5eAaPmAgMnS
při	při	k7c6
přípravě	příprava	k1gFnSc6
na	na	k7c4
finální	finální	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
stovky	stovka	k1gFnSc2
skic	skica	k1gFnPc2
a	a	k8xC
studií	studio	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Jedna	jeden	k4xCgFnSc1
z	z	k7c2
původních	původní	k2eAgFnPc2d1
verzí	verze	k1gFnPc2
na	na	k7c6
papíru	papír	k1gInSc6
byla	být	k5eAaImAgFnS
více	hodně	k6eAd2
narativní	narativní	k2eAgFnSc1d1
-	-	kIx~
uprostřed	uprostřed	k7c2
obrazu	obraz	k1gInSc2
seděl	sedět	k5eAaImAgMnS
námořník	námořník	k1gMnSc1
<g/>
,	,	kIx,
kterého	který	k3yRgMnSc4,k3yIgMnSc4,k3yQgMnSc4
obklopovalo	obklopovat	k5eAaImAgNnS
pět	pět	k4xCc4
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zleva	zleva	k6eAd1
za	za	k7c7
oponou	opona	k1gFnSc7
se	se	k3xPyFc4
vynořil	vynořit	k5eAaPmAgMnS
student	student	k1gMnSc1
s	s	k7c7
knihou	kniha	k1gFnSc7
jako	jako	k8xS,k8xC
nezúčastněný	zúčastněný	k2eNgInSc1d1
subjekt	subjekt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obraz	obraz	k1gInSc1
tak	tak	k6eAd1
akcentoval	akcentovat	k5eAaImAgInS
moralistní	moralistní	k2eAgInSc1d1
náboj	náboj	k1gInSc1
(	(	kIx(
<g/>
Memento	memento	k1gNnSc1
mori	mor	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
konečné	konečný	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
obrazu	obraz	k1gInSc2
ve	v	k7c6
tvaru	tvar	k1gInSc6
čtverce	čtverec	k1gInSc2
orientovaného	orientovaný	k2eAgInSc2d1
na	na	k7c4
výšku	výška	k1gFnSc4
zůstalo	zůstat	k5eAaPmAgNnS
pouze	pouze	k6eAd1
5	#num#	k4
žen	žena	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moralistní	moralistní	k2eAgInSc4d1
náboj	náboj	k1gInSc4
se	se	k3xPyFc4
vytratil	vytratit	k5eAaPmAgMnS
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
erotický	erotický	k2eAgInSc4d1
náboj	náboj	k1gInSc4
zůstal	zůstat	k5eAaPmAgMnS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
hlediska	hledisko	k1gNnSc2
vývoje	vývoj	k1gInSc2
Picassova	Picassův	k2eAgNnSc2d1
umění	umění	k1gNnSc2
jde	jít	k5eAaImIp3nS
o	o	k7c4
přelomové	přelomový	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
-	-	kIx~
tři	tři	k4xCgFnPc1
levé	levý	k2eAgFnPc1d1
postavy	postava	k1gFnPc1
mají	mít	k5eAaImIp3nP
obličeje	obličej	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
připomínají	připomínat	k5eAaImIp3nP
iberské	iberský	k2eAgFnPc1d1
masky	maska	k1gFnPc1
(	(	kIx(
<g/>
obdodně	obdodně	k6eAd1
jako	jako	k8xC,k8xS
portrét	portrét	k1gInSc1
Gertrudy	Gertruda	k1gFnSc2
Steinové	Steinová	k1gFnSc2
z	z	k7c2
r.	r.	kA
1906	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dvě	dva	k4xCgFnPc4
postavy	postava	k1gFnPc4
napravo	napravo	k6eAd1
byly	být	k5eAaImAgInP
inspirovány	inspirovat	k5eAaBmNgInP
africkými	africký	k2eAgFnPc7d1
maskami	maska	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotné	samotný	k2eAgNnSc1d1
dílo	dílo	k1gNnSc4
vytvořil	vytvořit	k5eAaPmAgInS
v	v	k7c6
Paříži	Paříž	k1gFnSc6
během	během	k7c2
léta	léto	k1gNnSc2
1907	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dílo	dílo	k1gNnSc1
po	po	k7c6
svém	svůj	k3xOyFgInSc6
vzniku	vznik	k1gInSc6
bylo	být	k5eAaImAgNnS
prakticky	prakticky	k6eAd1
neznámé	známý	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vidělo	vidět	k5eAaImAgNnS
je	on	k3xPp3gNnSc4
jen	jen	k9
několik	několik	k4yIc1
Picassových	Picassův	k2eAgMnPc2d1
přátel	přítel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1924	#num#	k4
je	být	k5eAaImIp3nS
koupil	koupit	k5eAaPmAgMnS
sběratel	sběratel	k1gMnSc1
Doucet	Doucet	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
později	pozdě	k6eAd2
nabídl	nabídnout	k5eAaPmAgMnS
Louvru	Louvre	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muzeum	muzeum	k1gNnSc1
však	však	k8xC
nabídku	nabídka	k1gFnSc4
odmítlo	odmítnout	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
po	po	k7c6
smrti	smrt	k1gFnSc6
sběratele	sběratel	k1gMnSc2
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1937	#num#	k4
dílo	dílo	k1gNnSc4
zakoupeno	zakoupen	k2eAgNnSc4d1
do	do	k7c2
Muzea	muzeum	k1gNnSc2
moderního	moderní	k2eAgNnSc2d1
umění	umění	k1gNnSc2
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
byl	být	k5eAaImAgInS
obraz	obraz	k1gInSc1
vystaven	vystavit	k5eAaPmNgInS
na	na	k7c6
Picassově	Picassův	k2eAgFnSc6d1
retrospektivní	retrospektivní	k2eAgFnSc6d1
výstavě	výstava	k1gFnSc6
tamním	tamní	k2eAgNnSc6d1
v	v	k7c6
muzeu	muzeum	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
započal	započnout	k5eAaPmAgInS
proces	proces	k1gInSc1
kanonizace	kanonizace	k1gFnSc2
Avignonských	avignonský	k2eAgFnPc2d1
slečen	slečna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významný	významný	k2eAgInSc1d1
přínos	přínos	k1gInSc1
k	k	k7c3
interpretaci	interpretace	k1gFnSc3
díla	dílo	k1gNnSc2
byly	být	k5eAaImAgFnP
práce	práce	k1gFnSc1
Lea	Lea	k1gFnSc1
Steinberga	Steinberga	k1gFnSc1
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
níže	nízce	k6eAd2
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Les	les	k1gInSc4
Demoiselles	Demoiselles	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Avignon	Avignon	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
je	být	k5eAaImIp3nS
také	také	k9
umístěna	umístit	k5eAaPmNgFnS
reprodukce	reprodukce	k1gFnSc1
obrazu	obraz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
The	The	k1gMnSc5
Private	Privat	k1gMnSc5
Life	Life	k1gFnPc7
of	of	k?
a	a	k8xC
Masterpiece	Masterpiece	k1gFnSc1
Series	Series	k1gInSc1
Three	Three	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
Worldwide	Worldwid	k1gInSc5
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
9	#num#	k4
<g/>
.	.	kIx.
17	#num#	k4
<g/>
,	,	kIx,
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FOSTER	FOSTER	kA
<g/>
,	,	kIx,
Hal	hala	k1gFnPc2
<g/>
;	;	kIx,
KRAUSS	KRAUSS	kA
<g/>
,	,	kIx,
Rosalind	Rosalinda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umění	umění	k1gNnSc1
po	po	k7c6
roce	rok	k1gInSc6
1900	#num#	k4
(	(	kIx(
<g/>
původním	původní	k2eAgInSc7d1
názvem	název	k1gInSc7
<g/>
:	:	kIx,
Art	Art	k1gFnSc1
since	since	k1gFnSc1
1900	#num#	k4
<g/>
,	,	kIx,
Thames	Thames	k1gMnSc1
<g/>
&	&	k?
<g/>
Hudson	Hudson	k1gMnSc1
<g/>
,	,	kIx,
London	London	k1gMnSc1
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
1	#num#	k4
<g/>
..	..	k?
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Slovart	Slovart	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
704	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7209	#num#	k4
<g/>
-	-	kIx~
<g/>
952	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
79	#num#	k4
<g/>
-	-	kIx~
<g/>
84	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
RICHARDSON	RICHARDSON	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
Life	Life	k1gInSc1
Of	Of	k1gFnSc2
Picasso	Picassa	k1gFnSc5
<g/>
,	,	kIx,
The	The	k1gMnSc1
Cubist	Cubist	k1gMnSc1
Rebel	rebel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Alfred	Alfred	k1gMnSc1
A.	A.	kA
Knopf	Knopf	k1gMnSc1
<g/>
,	,	kIx,
1991	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
307	#num#	k4
<g/>
-	-	kIx~
<g/>
26665	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
43	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Steinberg	Steinberg	k1gInSc1
<g/>
,	,	kIx,
L.	L.	kA
"	"	kIx"
<g/>
The	The	k1gMnSc1
Philosophical	Philosophical	k1gMnSc1
Brothel	Brothel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
October	October	k1gInSc1
<g/>
,	,	kIx,
no	no	k9
<g/>
.	.	kIx.
44	#num#	k4
<g/>
,	,	kIx,
Spring	Spring	k1gInSc1
1988	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
–	–	k?
<g/>
74	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
First	First	k1gMnSc1
published	published	k1gMnSc1
in	in	k?
Art	Art	k1gFnSc2
News	Newsa	k1gFnPc2
vol	vol	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
LXXI	LXXI	kA
<g/>
,	,	kIx,
September	September	k1gInSc1
<g/>
/	/	kIx~
<g/>
October	October	k1gInSc1
1972	#num#	k4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Výběr	výběr	k1gInSc1
známých	známý	k1gMnPc2
výtvarných	výtvarný	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
a	a	k8xC
jejich	jejich	k3xOp3gNnSc4
umístění	umístění	k1gNnSc4
Starověk	starověk	k1gInSc1
<g/>
,	,	kIx,
<g/>
antika	antika	k1gFnSc1
a	a	k8xC
starémimoevropskéumění	starémimoevropskéumění	k1gNnSc1
</s>
<s>
Svět	svět	k1gInSc1
</s>
<s>
Busta	busta	k1gFnSc1
královny	královna	k1gFnSc2
Nefertiti	Nefertiti	k1gFnSc1
(	(	kIx(
<g/>
Berlín	Berlín	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Láokoón	Láokoón	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnPc1
synové	syn	k1gMnPc1
(	(	kIx(
<g/>
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Moai	Moai	k1gNnSc1
(	(	kIx(
<g/>
Velikonoční	velikonoční	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
pravěké	pravěký	k2eAgFnSc2d1
malby	malba	k1gFnSc2
(	(	kIx(
<g/>
jeskyně	jeskyně	k1gFnSc1
Altamira	Altamira	k1gFnSc1
<g/>
,	,	kIx,
Lascaux	Lascaux	k1gInSc1
aj.	aj.	kA
<g/>
)	)	kIx)
•	•	k?
Rhodský	rhodský	k2eAgInSc4d1
kolos	kolos	k1gInSc4
(	(	kIx(
<g/>
zničen	zničen	k2eAgInSc4d1
<g/>
)	)	kIx)
•	•	k?
Terakotová	terakotový	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
(	(	kIx(
<g/>
Si-an	Si-an	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Tutanchamonova	Tutanchamonův	k2eAgFnSc1d1
pohřební	pohřební	k2eAgFnSc1d1
výbava	výbava	k1gFnSc1
(	(	kIx(
<g/>
Káhira	Káhira	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Velká	velký	k2eAgFnSc1d1
sfinga	sfinga	k1gFnSc1
(	(	kIx(
<g/>
Gíza	Gíza	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Venuše	Venuše	k1gFnSc1
Mélská	Mélská	k1gFnSc1
(	(	kIx(
<g/>
Paříž	Paříž	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Myrón	Myrón	k1gMnSc1
Diskobolos	Diskobolos	k1gMnSc1
(	(	kIx(
<g/>
dochovány	dochován	k2eAgFnPc1d1
kopie	kopie	k1gFnPc1
<g/>
,	,	kIx,
např.	např.	kA
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
Česko	Česko	k1gNnSc1
</s>
<s>
Hlava	hlava	k1gFnSc1
Kelta	Kelt	k1gMnSc2
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Věstonická	věstonický	k2eAgFnSc1d1
venuše	venuše	k1gFnSc1
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Středověk	středověk	k1gInSc1
</s>
<s>
Svět	svět	k1gInSc1
</s>
<s>
Kapitolská	kapitolský	k2eAgFnSc1d1
vlčice	vlčice	k1gFnSc1
(	(	kIx(
<g/>
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
bratři	bratr	k1gMnPc1
z	z	k7c2
Limburka	Limburek	k1gMnSc2
Přebohaté	přebohatý	k2eAgFnSc2d1
hodinky	hodinka	k1gFnSc2
vévody	vévoda	k1gMnSc2
z	z	k7c2
Berry	Berra	k1gFnSc2
(	(	kIx(
<g/>
Chantilly	Chantilla	k1gFnSc2
u	u	k7c2
Paříže	Paříž	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Eyck	Eyck	k1gInSc1
Svatba	svatba	k1gFnSc1
manželů	manžel	k1gMnPc2
Arnolfiniových	Arnolfiniový	k2eAgMnPc2d1
(	(	kIx(
<g/>
Londýn	Londýn	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Rublev	Rublev	k1gFnSc1
Ikona	ikona	k1gFnSc1
Nejsvětější	nejsvětější	k2eAgFnSc1d1
Trojice	trojice	k1gFnSc1
(	(	kIx(
<g/>
Moskva	Moskva	k1gFnSc1
<g/>
)	)	kIx)
Česko	Česko	k1gNnSc1
</s>
<s>
Kodex	kodex	k1gInSc1
vyšehradský	vyšehradský	k2eAgInSc1d1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Madona	Madona	k1gFnSc1
z	z	k7c2
Veveří	veveří	k2eAgFnSc2d1
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Palladium	palladium	k1gNnSc4
země	zem	k1gFnSc2
české	český	k2eAgFnSc2d1
(	(	kIx(
<g/>
Stará	starat	k5eAaImIp3nS
Boleslav	Boleslav	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Relikviář	relikviář	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Maura	Maur	k1gMnSc2
(	(	kIx(
<g/>
Bečov	Bečov	k1gInSc1
n.	n.	k?
T.	T.	kA
<g/>
)	)	kIx)
•	•	k?
Svatováclavská	svatováclavský	k2eAgFnSc1d1
zbroj	zbroj	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Vyšebrodský	vyšebrodský	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Zlatá	zlatý	k2eAgFnSc1d1
brána	brána	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Theodorik	Theodorika	k1gFnPc2
výzdoba	výzdoba	k1gFnSc1
kaple	kaple	k1gFnSc2
sv.	sv.	kA
Kříže	kříž	k1gInSc2
(	(	kIx(
<g/>
Karlštejn	Karlštejn	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Renesance	renesance	k1gFnSc1
a	a	k8xC
manýrismus	manýrismus	k1gInSc1
</s>
<s>
Svět	svět	k1gInSc1
</s>
<s>
Arcimboldo	Arcimboldo	k1gNnSc1
Čtvero	čtvero	k4xRgNnSc1
ročních	roční	k2eAgNnPc2d1
období	období	k1gNnPc2
(	(	kIx(
<g/>
Vídeň	Vídeň	k1gFnSc1
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
aj.	aj.	kA
<g/>
)	)	kIx)
/	/	kIx~
Rudolf	Rudolf	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
jako	jako	k8xS,k8xC
Vertumnus	Vertumnus	k1gMnSc1
(	(	kIx(
<g/>
Skokloster	Skokloster	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Bosch	Bosch	kA
Zahrada	zahrada	k1gFnSc1
pozemských	pozemský	k2eAgFnPc2d1
rozkoší	rozkoš	k1gFnPc2
(	(	kIx(
<g/>
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Botticelli	Botticell	k1gMnPc1
Primavera	Primavero	k1gNnSc2
a	a	k8xC
Zrození	zrození	k1gNnSc2
Venuše	Venuše	k1gFnSc2
(	(	kIx(
<g/>
Florencie	Florencie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Brueghel	Brueghel	k1gMnSc1
st.	st.	kA
Stavba	stavba	k1gFnSc1
babylonské	babylonský	k2eAgFnSc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
věže	věž	k1gFnPc1
(	(	kIx(
<g/>
Vídeň	Vídeň	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Donatello	Donatello	k1gNnSc1
David	David	k1gMnSc1
(	(	kIx(
<g/>
Florencie	Florencie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Dürer	Dürer	k1gInSc1
Melancholie	melancholie	k1gFnSc1
I	i	k9
(	(	kIx(
<g/>
více	hodně	k6eAd2
výtisků	výtisk	k1gInPc2
<g/>
)	)	kIx)
•	•	k?
Grünewald	Grünewald	k1gInSc1
Isenheimský	Isenheimský	k2eAgInSc1d1
oltář	oltář	k1gInSc1
(	(	kIx(
<g/>
Kolmar	Kolmar	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Leonardo	Leonardo	k1gMnSc1
Dáma	dáma	k1gFnSc1
s	s	k7c7
hranostajem	hranostaj	k1gMnSc7
(	(	kIx(
<g/>
Krakov	Krakov	k1gInSc1
<g/>
)	)	kIx)
/	/	kIx~
Madona	Madona	k1gFnSc1
ve	v	k7c6
skalách	skála	k1gFnPc6
a	a	k8xC
Mona	Mon	k2eAgFnSc1d1
Lisa	Lisa	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
Paříž	Paříž	k1gFnSc1
<g/>
)	)	kIx)
/	/	kIx~
Poslední	poslední	k2eAgFnSc1d1
večeře	večeře	k1gFnSc1
(	(	kIx(
<g/>
Milán	Milán	k1gInSc1
<g/>
)	)	kIx)
/	/	kIx~
Vitruviánský	Vitruviánský	k2eAgMnSc1d1
muž	muž	k1gMnSc1
(	(	kIx(
<g/>
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
•	•	k?
Michelangelo	Michelangela	k1gFnSc5
David	David	k1gMnSc1
(	(	kIx(
<g/>
Florencie	Florencie	k1gFnSc1
<g/>
)	)	kIx)
/	/	kIx~
Pieta	pieta	k1gFnSc1
(	(	kIx(
<g/>
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
/	/	kIx~
Poslední	poslední	k2eAgInSc1d1
soud	soud	k1gInSc1
a	a	k8xC
Stvoření	stvoření	k1gNnSc1
Adama	Adam	k1gMnSc2
(	(	kIx(
<g/>
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Raffael	Raffael	k1gInSc1
Raffaelovy	Raffaelův	k2eAgInPc1d1
sály	sál	k1gInPc1
(	(	kIx(
<g/>
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
/	/	kIx~
Sixtinská	sixtinský	k2eAgFnSc1d1
madona	madona	k1gFnSc1
(	(	kIx(
<g/>
Drážďany	Drážďany	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Tizian	Tiziana	k1gFnPc2
Urbinská	Urbinský	k2eAgFnSc1d1
Venuše	Venuše	k1gFnSc1
(	(	kIx(
<g/>
Florencie	Florencie	k1gFnSc1
<g/>
)	)	kIx)
Česko	Česko	k1gNnSc1
</s>
<s>
Pražské	pražský	k2eAgNnSc1d1
Jezulátko	Jezulátko	k1gNnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Dürer	Dürra	k1gFnPc2
Růžencová	růžencový	k2eAgFnSc1d1
slavnost	slavnost	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Tizian	Tizian	k1gMnSc1
Apollo	Apollo	k1gMnSc1
a	a	k8xC
Marsyas	Marsyas	k1gMnSc1
(	(	kIx(
<g/>
Kroměříž	Kroměříž	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Baroko	baroko	k1gNnSc1
a	a	k8xC
rokoko	rokoko	k1gNnSc1
</s>
<s>
Svět	svět	k1gInSc1
</s>
<s>
Bernini	Bernin	k2eAgMnPc1d1
Extáze	extáze	k1gFnSc2
svaté	svatý	k2eAgFnSc2d1
Terezy	Tereza	k1gFnSc2
(	(	kIx(
<g/>
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Caravaggio	Caravaggio	k6eAd1
Povolání	povolání	k1gNnSc1
svatého	svatý	k2eAgMnSc2d1
Matouše	Matouš	k1gMnSc2
(	(	kIx(
<g/>
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Duquesnoy	Duquesnoa	k1gFnSc2
Čurající	čurající	k2eAgMnSc1d1
chlapeček	chlapeček	k1gMnSc1
(	(	kIx(
<g/>
Brusel	Brusel	k1gInSc1
<g/>
,	,	kIx,
originál	originál	k1gInSc1
v	v	k7c6
městském	městský	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
<g/>
)	)	kIx)
•	•	k?
Rembrandt	Rembrandt	k1gInSc1
Anatomie	anatomie	k1gFnSc1
doktora	doktor	k1gMnSc2
Tulpa	Tulp	k1gMnSc2
(	(	kIx(
<g/>
Haag	Haag	k1gInSc1
<g/>
)	)	kIx)
/	/	kIx~
Noční	noční	k2eAgFnSc1d1
hlídka	hlídka	k1gFnSc1
(	(	kIx(
<g/>
Amsterdam	Amsterdam	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Rubens	Rubens	k1gInSc1
Vztyčení	vztyčení	k1gNnSc2
kříže	kříž	k1gInSc2
(	(	kIx(
<g/>
Antverpy	Antverpy	k1gFnPc1
<g/>
)	)	kIx)
•	•	k?
Salvi	Salev	k1gFnSc3
Fontána	fontána	k1gFnSc1
di	di	k?
Trevi	Treev	k1gFnSc3
(	(	kIx(
<g/>
Řím	Řím	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Velázquez	Velázquez	k1gInSc4
Dvorní	dvorní	k2eAgFnSc2d1
dámy	dáma	k1gFnSc2
(	(	kIx(
<g/>
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Vermeer	Vermeer	k1gInSc1
Dívka	dívka	k1gFnSc1
s	s	k7c7
perlou	perla	k1gFnSc7
(	(	kIx(
<g/>
Haag	Haag	k1gInSc1
<g/>
)	)	kIx)
Česko	Česko	k1gNnSc1
</s>
<s>
Sloup	sloup	k1gInSc1
Nejsvětější	nejsvětější	k2eAgFnSc2d1
Trojice	trojice	k1gFnSc2
(	(	kIx(
<g/>
Olomouc	Olomouc	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Sochy	socha	k1gFnPc1
na	na	k7c6
Karlově	Karlův	k2eAgInSc6d1
mostě	most	k1gInSc6
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Bendl	Bendl	k1gFnSc3
Mariánský	mariánský	k2eAgInSc1d1
sloup	sloup	k1gInSc1
•	•	k?
Braun	Braun	k1gMnSc1
výzdoba	výzdoba	k1gFnSc1
Hospitálu	hospitál	k1gInSc2
a	a	k8xC
Betlém	Betlém	k1gInSc1
(	(	kIx(
<g/>
Kuks	Kuks	k1gInSc1
a	a	k8xC
okolí	okolí	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Klasicismus	klasicismus	k1gInSc1
až	až	k9
realismus	realismus	k1gInSc1
</s>
<s>
Svět	svět	k1gInSc1
</s>
<s>
Bartholdi	Barthold	k1gMnPc1
Socha	Socha	k1gMnSc1
Svobody	Svoboda	k1gMnSc2
(	(	kIx(
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
David	David	k1gMnSc1
Maratova	Maratův	k2eAgFnSc1d1
smrt	smrt	k1gFnSc1
(	(	kIx(
<g/>
Brusel	Brusel	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Delacroix	Delacroix	k1gInSc1
Svoboda	Svoboda	k1gMnSc1
vede	vést	k5eAaImIp3nS
lid	lid	k1gInSc1
na	na	k7c4
barikády	barikáda	k1gFnPc4
(	(	kIx(
<g/>
Paříž	Paříž	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Falconet	Falconet	k1gInSc1
Měděný	měděný	k2eAgMnSc1d1
jezdec	jezdec	k1gMnSc1
(	(	kIx(
<g/>
Petrohrad	Petrohrad	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Friedrich	Friedrich	k1gMnSc1
Poutník	poutník	k1gMnSc1
nad	nad	k7c7
mořem	moře	k1gNnSc7
mlhy	mlha	k1gFnSc2
(	(	kIx(
<g/>
Hamburk	Hamburk	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Goya	Goy	k2eAgFnSc1d1
Nahá	nahý	k2eAgFnSc1d1
Maja	Maja	k1gFnSc1
a	a	k8xC
Popravy	poprava	k1gFnPc1
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1808	#num#	k4
(	(	kIx(
<g/>
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Hokusai	Hokusae	k1gFnSc4
36	#num#	k4
pohledů	pohled	k1gInPc2
na	na	k7c4
horu	hora	k1gFnSc4
Fudži	Fudž	k1gFnSc3
(	(	kIx(
<g/>
více	hodně	k6eAd2
exemplářů	exemplář	k1gInPc2
<g/>
)	)	kIx)
•	•	k?
Repin	Repin	k1gMnSc1
Burlaci	Burlace	k1gFnSc4
na	na	k7c6
Volze	Volha	k1gFnSc6
a	a	k8xC
Záporožští	záporožský	k2eAgMnPc1d1
kozáci	kozák	k1gMnPc1
píší	psát	k5eAaImIp3nP
dopis	dopis	k1gInSc4
tureckému	turecký	k2eAgMnSc3d1
sultánovi	sultán	k1gMnSc3
(	(	kIx(
<g/>
Petrohrad	Petrohrad	k1gInSc1
<g/>
)	)	kIx)
Česko	Česko	k1gNnSc1
</s>
<s>
Marold	Marold	k6eAd1
Bitva	bitva	k1gFnSc1
u	u	k7c2
Lipan	lipan	k1gMnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Myslbek	Myslbek	k1gInSc1
Pomník	pomník	k1gInSc1
sv.	sv.	kA
Václava	Václav	k1gMnSc2
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1
moderna	moderna	k1gFnSc1
</s>
<s>
Svět	svět	k1gInSc1
</s>
<s>
Böcklin	Böcklin	k2eAgInSc1d1
Ostrov	ostrov	k1gInSc1
mrtvých	mrtvý	k1gMnPc2
(	(	kIx(
<g/>
Basilej	Basilej	k1gFnSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
,	,	kIx,
Berlín	Berlín	k1gInSc1
a	a	k8xC
Lipsko	Lipsko	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Borglum	Borglum	k1gInSc1
Mount	Mount	k1gInSc1
Rushmore	Rushmor	k1gInSc5
(	(	kIx(
<g/>
Keyston	Keyston	k1gInSc4
<g/>
)	)	kIx)
•	•	k?
Dalí	Dalí	k2eAgFnSc1d1
Persistence	persistence	k1gFnSc1
paměti	paměť	k1gFnSc2
(	(	kIx(
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Duchamp	Duchamp	k1gInSc1
Akt	akt	k1gInSc1
sestupující	sestupující	k2eAgInSc1d1
se	s	k7c7
schodů	schod	k1gInPc2
(	(	kIx(
<g/>
Filadelfie	Filadelfie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Eriksen	Eriksna	k1gFnPc2
Malá	malý	k2eAgFnSc1d1
mořská	mořský	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
víla	víla	k1gFnSc1
(	(	kIx(
<g/>
Kodaň	Kodaň	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Gogh	Gogha	k1gFnPc2
Hvězdná	hvězdný	k2eAgFnSc1d1
noc	noc	k1gFnSc1
(	(	kIx(
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
/	/	kIx~
Slunečnice	slunečnice	k1gFnSc1
(	(	kIx(
<g/>
více	hodně	k6eAd2
verzí	verze	k1gFnSc7
<g/>
,	,	kIx,
např.	např.	kA
Mnichov	Mnichov	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Hopper	Hopper	k1gInSc4
Noční	noční	k2eAgMnPc1d1
ptáci	pták	k1gMnPc1
(	(	kIx(
<g/>
Chicago	Chicago	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Kahlo	Kahlo	k1gNnSc1
Dvě	dva	k4xCgFnPc1
Fridy	Frida	k1gFnPc1
(	(	kIx(
<g/>
Mexiko	Mexiko	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Klimt	Klimt	k2eAgInSc4d1
Polibek	polibek	k1gInSc4
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Vídeň	Vídeň	k1gFnSc1
<g/>
)	)	kIx)
/	/	kIx~
Zlatá	zlatý	k2eAgFnSc1d1
Adele	Adele	k1gFnSc1
(	(	kIx(
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Landowski	Landowski	k1gNnSc1
a	a	k8xC
Leonida	Leonida	k1gFnSc1
Kristus	Kristus	k1gMnSc1
Spasitel	spasitel	k1gMnSc1
(	(	kIx(
<g/>
Rio	Rio	k1gMnSc1
de	de	k?
Janeiro	Janeiro	k1gNnSc4
<g/>
)	)	kIx)
•	•	k?
Malevič	Malevič	k1gInSc1
Černý	Černý	k1gMnSc1
čtverec	čtverec	k1gInSc1
(	(	kIx(
<g/>
nejstarší	starý	k2eAgFnSc1d3
verze	verze	k1gFnSc1
Moskva	Moskva	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Manet	manet	k1gInSc1
Snídaně	snídaně	k1gFnSc2
v	v	k7c6
trávě	tráva	k1gFnSc6
(	(	kIx(
<g/>
Paříž	Paříž	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Monet	moneta	k1gFnPc2
Imprese	imprese	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
východ	východ	k1gInSc1
slunce	slunce	k1gNnSc2
(	(	kIx(
<g/>
Paříž	Paříž	k1gFnSc1
<g/>
)	)	kIx)
/	/	kIx~
Lekníny	leknín	k1gInPc1
(	(	kIx(
<g/>
asi	asi	k9
250	#num#	k4
verzí	verze	k1gFnPc2
<g/>
,	,	kIx,
např.	např.	kA
Paříž	Paříž	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Muchina	Muchina	k1gMnSc1
Dělník	dělník	k1gMnSc1
a	a	k8xC
kolchoznice	kolchoznice	k1gFnSc1
(	(	kIx(
<g/>
Moskva	Moskva	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Munch	Munch	k1gInSc1
Výkřik	výkřik	k1gInSc1
(	(	kIx(
<g/>
Oslo	Oslo	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Picasso	Picassa	k1gFnSc5
Avignonské	avignonský	k2eAgFnPc1d1
slečny	slečna	k1gFnPc1
(	(	kIx(
<g/>
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
)	)	kIx)
/	/	kIx~
Guernica	Guernica	k1gFnSc1
(	(	kIx(
<g/>
Madrid	Madrid	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Rodin	rodina	k1gFnPc2
Myslitel	myslitel	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
odlitek	odlitek	k1gInSc1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
)	)	kIx)
Česko	Česko	k1gNnSc1
</s>
<s>
Mucha	Mucha	k1gMnSc1
Slovanská	slovanský	k2eAgFnSc1d1
epopej	epopej	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Rousseau	Rousseau	k1gMnSc1
Vlastní	vlastnit	k5eAaImIp3nS
podobizna	podobizna	k1gFnSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Šaloun	Šaloun	k1gInSc1
Pomník	pomník	k1gInSc1
mistra	mistr	k1gMnSc2
Jana	Jan	k1gMnSc2
Husa	Hus	k1gMnSc2
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Poválečné	poválečný	k2eAgNnSc1d1
umění	umění	k1gNnSc1
</s>
<s>
Svět	svět	k1gInSc1
</s>
<s>
Pollock	Pollock	k6eAd1
No	no	k9
<g/>
.	.	kIx.
5	#num#	k4
<g/>
,	,	kIx,
1948	#num#	k4
•	•	k?
Vučetič	Vučetič	k1gMnSc1
a	a	k8xC
Nikitin	Nikitin	k1gMnSc1
Matka	matka	k1gFnSc1
vlast	vlast	k1gFnSc1
volá	volat	k5eAaImIp3nS
(	(	kIx(
<g/>
Volgograd	Volgograd	k1gInSc1
<g/>
)	)	kIx)
Česko	Česko	k1gNnSc1
</s>
<s>
Černý	Černý	k1gMnSc1
Miminka	miminko	k1gNnSc2
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Rujbr	Rujbr	k1gMnSc1
a	a	k8xC
Kameník	Kameník	k1gMnSc1
Brněnský	brněnský	k2eAgInSc1d1
orloj	orloj	k1gInSc1
•	•	k?
Svolinský	Svolinský	k2eAgInSc1d1
Olomoucký	olomoucký	k2eAgInSc1d1
orloj	orloj	k1gInSc1
•	•	k?
Švec	Švec	k1gMnSc1
Stalinův	Stalinův	k2eAgInSc4d1
pomník	pomník	k1gInSc4
(	(	kIx(
<g/>
zničen	zničen	k2eAgInSc4d1
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
umění	umění	k1gNnSc2
•	•	k?
Korunovační	korunovační	k2eAgInPc4d1
klenoty	klenot	k1gInPc4
•	•	k?
Sedm	sedm	k4xCc4
divů	div	k1gInPc2
světa	svět	k1gInSc2
•	•	k?
Seznam	seznam	k1gInSc1
malířů	malíř	k1gMnPc2
•	•	k?
Seznam	seznam	k1gInSc4
národních	národní	k2eAgFnPc2d1
kulturních	kulturní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
•	•	k?
Seznam	seznam	k1gInSc1
českých	český	k2eAgNnPc2d1
muzeí	muzeum	k1gNnPc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4250670-0	4250670-0	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
98087119	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
181566483	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
98087119	#num#	k4
</s>
