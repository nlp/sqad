<s>
Avignonské	avignonský	k2eAgFnPc1d1
slečny	slečna	k1gFnPc1
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
Les	les	k1gInSc1
Demoiselles	Demoiselles	k1gInSc1
d	d	k?
<g/>
’	’	k?
<g/>
Avignon	Avignon	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
velká	velký	k2eAgFnSc1d1
olejomalba	olejomalba	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1907	#num#	k4
od	od	k7c2
španělského	španělský	k2eAgMnSc2d1
malíře	malíř	k1gMnSc2
Pabla	Pablo	k1gMnSc2
Picassa	Picasso	k1gMnSc2
(	(	kIx(
<g/>
1881	#num#	k4
<g/>
–	–	k?
<g/>
1973	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
