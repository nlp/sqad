<s>
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Riesengebirge	Riesengebirge	k1gFnSc1	Riesengebirge
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
Karkonosze	Karkonosze	k1gFnSc1	Karkonosze
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
geomorfologickým	geomorfologický	k2eAgInSc7d1	geomorfologický
celkem	celek	k1gInSc7	celek
a	a	k8xC	a
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
pohořím	pohoří	k1gNnSc7	pohoří
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
České	český	k2eAgFnSc2d1	Česká
vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
.	.	kIx.	.
</s>
