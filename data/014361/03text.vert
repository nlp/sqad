<s>
Chris	Chris	k1gFnSc1
Evans	Evansa	k1gFnPc2
</s>
<s>
Chris	Chris	k1gFnSc1
Evans	Evansa	k1gFnPc2
Chris	Chris	k1gFnSc1
Evans	Evans	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
na	na	k7c6
Comic	Comice	k1gFnPc2
ConuRodné	ConuRodný	k2eAgFnSc2d1
jméno	jméno	k1gNnSc4
</s>
<s>
Christopher	Christophra	k1gFnPc2
Robert	Robert	k1gMnSc1
Evans	Evans	k1gInSc4
Narození	narození	k1gNnSc2
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1981	#num#	k4
(	(	kIx(
<g/>
39	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Boston	Boston	k1gInSc1
<g/>
,	,	kIx,
Massachusetts	Massachusetts	k1gNnSc1
<g/>
,	,	kIx,
USA	USA	kA
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Lincoln-Sudbury	Lincoln-Sudbura	k1gFnPc1
Regional	Regional	k1gFnSc2
High	High	k1gMnSc1
SchoolLee	SchoolLee	k1gFnPc2
Strasberg	Strasberg	k1gMnSc1
Theatre	Theatr	k1gInSc5
and	and	k?
Film	film	k1gInSc1
InstituteNewyorská	InstituteNewyorský	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Aktivní	aktivní	k2eAgFnSc1d1
roky	rok	k1gInPc4
</s>
<s>
od	od	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
Partner	partner	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Jessica	Jessic	k2eAgFnSc1d1
Bielová	Bielová	k1gFnSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
<g/>
Minka	Minka	k1gFnSc1
Kelly	Kella	k1gFnSc2
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
Jenny	Jenna	k1gFnSc2
Slateová	Slateový	k2eAgFnSc1d1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Scott	Scott	k2eAgInSc1d1
Evans	Evans	k1gInSc1
(	(	kIx(
<g/>
sourozenec	sourozenec	k1gMnSc1
<g/>
)	)	kIx)
<g/>
Mike	Mike	k1gInSc1
Capuano	Capuana	k1gFnSc5
(	(	kIx(
<g/>
strýc	strýc	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Významné	významný	k2eAgFnSc2d1
role	role	k1gFnSc2
</s>
<s>
Captain	Captain	k2eAgMnSc1d1
America	America	k1gMnSc1
Český	český	k2eAgInSc4d1
dabing	dabing	k1gInSc4
</s>
<s>
Libor	Libor	k1gMnSc1
Bouček	Bouček	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Christopher	Christopher	k1gMnSc1
Robert	Robert	k1gMnSc1
„	„	k?
<g/>
Chris	Chris	k1gFnSc2
<g/>
“	“	k?
Evans	Evans	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1981	#num#	k4
<g/>
,	,	kIx,
Boston	Boston	k1gInSc1
<g/>
,	,	kIx,
Massachusetts	Massachusetts	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
americký	americký	k2eAgMnSc1d1
herec	herec	k1gMnSc1
a	a	k8xC
režisér	režisér	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
jeho	jeho	k3xOp3gFnPc4
nejvýznamnější	významný	k2eAgFnPc4d3
role	role	k1gFnPc4
patří	patřit	k5eAaImIp3nS
Johnny	Johnn	k1gMnPc4
Storm	Storm	k1gInSc1
ve	v	k7c6
filmech	film	k1gInPc6
Fantastická	fantastický	k2eAgFnSc1d1
čtyřka	čtyřka	k1gFnSc1
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Fantastická	fantastický	k2eAgFnSc1d1
čtyřka	čtyřka	k1gFnSc1
a	a	k8xC
Silver	Silver	k1gMnSc1
Surfer	Surfer	k1gMnSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Kapitán	kapitán	k1gMnSc1
Amerika	Amerika	k1gFnSc1
ve	v	k7c6
filmové	filmový	k2eAgFnSc6d1
sérii	série	k1gFnSc6
Marvel	Marvela	k1gFnPc2
Cinematic	Cinematice	k1gFnPc2
Universe	Universe	k1gFnSc2
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
režijně	režijně	k6eAd1
debutoval	debutovat	k5eAaBmAgMnS
filmem	film	k1gInSc7
Noc	noc	k1gFnSc4
v	v	k7c4
New	New	k1gFnSc4
Yorku	York	k1gInSc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
si	se	k3xPyFc3
poprvé	poprvé	k6eAd1
zahrál	zahrát	k5eAaPmAgMnS
na	na	k7c4
Broadwayi	Broadwaye	k1gFnSc4
v	v	k7c6
divadelní	divadelní	k2eAgFnSc6d1
hře	hra	k1gFnSc6
Lobby	lobby	k1gFnPc2
Hero	Hero	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
Bostonu	Boston	k1gInSc6
<g/>
,	,	kIx,
vyrůstal	vyrůstat	k5eAaImAgMnS
v	v	k7c6
bostonském	bostonský	k2eAgNnSc6d1
předměstí	předměstí	k1gNnSc6
Sudbury	Sudbura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc4
matka	matka	k1gFnSc1
Lisa	Lisa	k1gFnSc1
<g/>
,	,	kIx,
napůl	napůl	k6eAd1
italského	italský	k2eAgNnSc2d1
a	a	k8xC
napůl	napůl	k6eAd1
irského	irský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
umělecká	umělecký	k2eAgFnSc1d1
ředitelka	ředitelka	k1gFnSc1
v	v	k7c6
divadle	divadlo	k1gNnSc6
Concord	Concord	k1gMnSc1
Youth	Youth	k1gMnSc1
Theater	Theater	k1gMnSc1
<g/>
,	,	kIx,
otec	otec	k1gMnSc1
Bob	Bob	k1gMnSc1
je	být	k5eAaImIp3nS
zubařem	zubař	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evans	Evans	k1gInSc1
má	mít	k5eAaImIp3nS
sestry	sestra	k1gFnPc4
Carly	Carla	k1gFnSc2
<g/>
,	,	kIx,
učitelku	učitelka	k1gFnSc4
dramatické	dramatický	k2eAgFnSc2d1
výchovy	výchova	k1gFnSc2
a	a	k8xC
angličtiny	angličtina	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
Shannu	Shanna	k1gMnSc4
a	a	k8xC
mladšího	mladý	k2eAgMnSc4d2
bratra	bratr	k1gMnSc4
Scotta	Scott	k1gMnSc4
<g/>
,	,	kIx,
rovněž	rovněž	k9
herce	herec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
strýc	strýc	k1gMnSc1
Mike	Mik	k1gFnSc2
Capuano	Capuana	k1gFnSc5
<g/>
,	,	kIx,
bratr	bratr	k1gMnSc1
jejich	jejich	k3xOp3gFnSc2
matky	matka	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
politik	politik	k1gMnSc1
působící	působící	k2eAgMnSc1d1
ve	v	k7c6
Sněmovně	sněmovna	k1gFnSc6
reprezentantů	reprezentant	k1gMnPc2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evans	Evans	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
vychováván	vychováván	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
katolík	katolík	k1gMnSc1
<g/>
,	,	kIx,
absolvoval	absolvovat	k5eAaPmAgMnS
Lincoln-Sudbury	Lincoln-Sudbur	k1gMnPc4
Regional	Regional	k1gMnSc1
High	High	k1gMnSc1
School	School	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Kariéra	kariéra	k1gFnSc1
</s>
<s>
Evans	Evans	k6eAd1
na	na	k7c6
konferenci	konference	k1gFnSc6
k	k	k7c3
filmu	film	k1gInSc3
Captain	Captain	k1gMnSc1
America	America	k1gMnSc1
<g/>
:	:	kIx,
Návrat	návrat	k1gInSc1
prvního	první	k4xOgMnSc2
Avengera	Avenger	k1gMnSc2
</s>
<s>
Po	po	k7c6
filmu	film	k1gInSc6
Bulšit	Bulšit	k1gFnSc2
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
si	se	k3xPyFc3
zahrál	zahrát	k5eAaPmAgMnS
hlavní	hlavní	k2eAgFnPc4d1
role	role	k1gFnPc4
ve	v	k7c6
filmech	film	k1gInPc6
Perfektní	perfektní	k2eAgNnPc4d1
skóre	skóre	k1gNnPc4
a	a	k8xC
Cellular	Cellular	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgMnS
ve	v	k7c6
dvou	dva	k4xCgInPc6
nezávislých	závislý	k2eNgInPc6d1
chicagských	chicagský	k2eAgInPc6d1
snímcích	snímek	k1gInPc6
<g/>
:	:	kIx,
Nelítostná	lítostný	k2eNgFnSc1d1
rasa	rasa	k1gFnSc1
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
si	se	k3xPyFc3
zahrál	zahrát	k5eAaPmAgMnS
postavu	postava	k1gFnSc4
Bryce	Bryec	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
London	London	k1gMnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
hrál	hrát	k5eAaImAgMnS
drogově	drogově	k6eAd1
závislého	závislý	k2eAgInSc2d1
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
má	mít	k5eAaImIp3nS
problém	problém	k1gInSc1
se	s	k7c7
vztahy	vztah	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
se	se	k3xPyFc4
jako	jako	k8xS,k8xC
Johnny	Johnen	k2eAgInPc1d1
Storm	Storm	k1gInSc1
představil	představit	k5eAaPmAgInS
v	v	k7c6
superhrdinském	superhrdinský	k2eAgInSc6d1
filmu	film	k1gInSc6
Fantastická	fantastický	k2eAgFnSc1d1
čtyřka	čtyřka	k1gFnSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
tuto	tento	k3xDgFnSc4
roli	role	k1gFnSc4
si	se	k3xPyFc3
zopakoval	zopakovat	k5eAaPmAgMnS
i	i	k9
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
ve	v	k7c6
snímku	snímek	k1gInSc6
Fantastická	fantastický	k2eAgFnSc1d1
čtyřka	čtyřka	k1gFnSc1
a	a	k8xC
Silver	Silver	k1gMnSc1
Surfer	Surfer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tentýž	týž	k3xTgInSc1
rok	rok	k1gInSc4
si	se	k3xPyFc3
zahrál	zahrát	k5eAaPmAgInS
astronauta	astronaut	k1gMnSc4
Mace	Mac	k1gMnSc4
ve	v	k7c6
filmu	film	k1gInSc6
Sunshine	Sunshin	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
2008	#num#	k4
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgInS
ve	v	k7c6
filmu	film	k1gInSc6
Street	Street	k1gMnSc1
Kings	Kings	k1gInSc1
a	a	k8xC
ve	v	k7c6
snímku	snímek	k1gInSc6
Ztráta	ztráta	k1gFnSc1
diamantové	diamantový	k2eAgFnSc2d1
slzy	slza	k1gFnSc2
podle	podle	k7c2
scénáře	scénář	k1gInSc2
Tennesseeho	Tennessee	k1gMnSc2
Williamse	Williams	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujícím	následující	k2eAgInSc6d1
roce	rok	k1gInSc6
hrál	hrát	k5eAaImAgInS
ve	v	k7c6
sci-fi	sci-fi	k1gNnSc6
thrilleru	thriller	k1gInSc2
Push	Pusha	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgNnSc6
sám	sám	k3xTgMnSc1
absolvoval	absolvovat	k5eAaPmAgMnS
i	i	k9
bojové	bojový	k2eAgFnPc4d1
scény	scéna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2011	#num#	k4
hraje	hrát	k5eAaImIp3nS
Kapitána	kapitán	k1gMnSc4
Americu	America	k1gMnSc4
v	v	k7c6
superhrdinských	superhrdinský	k2eAgInPc6d1
filmech	film	k1gInPc6
od	od	k7c2
Marvel	Marvela	k1gMnSc2
Studios	Studios	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
této	tento	k3xDgFnSc6
roli	role	k1gFnSc6
objevil	objevit	k5eAaPmAgMnS
ve	v	k7c6
snímku	snímek	k1gInSc6
Captain	Captain	k1gMnSc1
America	America	k1gMnSc1
<g/>
:	:	kIx,
První	první	k4xOgInSc1
Avenger	Avenger	k1gInSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
si	se	k3xPyFc3
tuto	tento	k3xDgFnSc4
postavu	postava	k1gFnSc4
zahrál	zahrát	k5eAaPmAgMnS
v	v	k7c6
navazujících	navazující	k2eAgInPc6d1
filmech	film	k1gInPc6
Avengers	Avengers	k1gInSc1
<g/>
,	,	kIx,
Thor	Thor	k1gInSc1
<g/>
:	:	kIx,
Temný	temný	k2eAgInSc1d1
svět	svět	k1gInSc1
<g/>
,	,	kIx,
Captain	Captain	k1gMnSc1
America	America	k1gMnSc1
<g/>
:	:	kIx,
Návrat	návrat	k1gInSc1
prvního	první	k4xOgMnSc2
Avengera	Avenger	k1gMnSc2
<g/>
,	,	kIx,
Avengers	Avengers	k1gInSc1
<g/>
:	:	kIx,
Age	Age	k1gMnSc1
of	of	k?
Ultron	Ultron	k1gMnSc1
<g/>
,	,	kIx,
Ant-Man	Ant-Man	k1gMnSc1
<g/>
,	,	kIx,
Captain	Captain	k1gMnSc1
America	America	k1gMnSc1
<g/>
:	:	kIx,
Občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
,	,	kIx,
Spider-Man	Spider-Man	k1gInSc1
<g/>
:	:	kIx,
Homecoming	Homecoming	k1gInSc1
<g/>
,	,	kIx,
Avengers	Avengers	k1gInSc1
<g/>
:	:	kIx,
Infinity	Infinit	k2eAgFnPc1d1
War	War	k1gFnPc1
<g/>
,	,	kIx,
Captain	Captain	k2eAgInSc1d1
Marvel	Marvel	k1gInSc1
a	a	k8xC
Avengers	Avengers	k1gInSc1
<g/>
:	:	kIx,
Endgame	Endgam	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
se	se	k3xPyFc4
společně	společně	k6eAd1
s	s	k7c7
Annou	Anna	k1gFnSc7
Faris	Faris	k1gFnPc2
objevil	objevit	k5eAaPmAgMnS
v	v	k7c6
hlavní	hlavní	k2eAgFnSc6d1
roli	role	k1gFnSc6
v	v	k7c6
romantické	romantický	k2eAgFnSc6d1
komedii	komedie	k1gFnSc6
Co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
ty	ten	k3xDgMnPc4
jsi	být	k5eAaImIp2nS
za	za	k7c4
číslo	číslo	k1gNnSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
2013	#num#	k4
nahradil	nahradit	k5eAaPmAgMnS
Jamese	Jamese	k1gFnSc2
Franca	Franca	k?
ve	v	k7c6
filmu	film	k1gInSc6
The	The	k1gMnSc1
Iceman	Iceman	k1gMnSc1
a	a	k8xC
zahrál	zahrát	k5eAaPmAgMnS
si	se	k3xPyFc3
také	také	k6eAd1
v	v	k7c6
jihokorejském	jihokorejský	k2eAgInSc6d1
filmu	film	k1gInSc6
Ledová	ledový	k2eAgFnSc1d1
archa	archa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
natočil	natočit	k5eAaBmAgMnS
svůj	svůj	k3xOyFgInSc4
režijní	režijní	k2eAgInSc4d1
celovečerní	celovečerní	k2eAgInSc4d1
debut	debut	k1gInSc4
Before	Befor	k1gInSc5
We	We	k1gFnPc4
Go	Go	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
měl	mít	k5eAaImAgInS
premiéru	premiéra	k1gFnSc4
na	na	k7c6
Filmovém	filmový	k2eAgInSc6d1
festivalu	festival	k1gInSc6
v	v	k7c6
Torontu	Toronto	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následující	následující	k2eAgInSc1d1
rok	rok	k1gInSc1
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgInS
po	po	k7c6
boku	bok	k1gInSc6
Michelle	Michelle	k1gFnSc2
Monaghan	Monaghana	k1gFnPc2
v	v	k7c6
romantické	romantický	k2eAgFnSc6d1
komedii	komedie	k1gFnSc6
Kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
věří	věřit	k5eAaImIp3nS
na	na	k7c4
lásku	láska	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
si	se	k3xPyFc3
zahrál	zahrát	k5eAaPmAgInS
ve	v	k7c6
filmu	film	k1gInSc6
Velký	velký	k2eAgInSc4d1
dar	dar	k1gInSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
zahrál	zahrát	k5eAaPmAgInS
strýce	strýc	k1gMnSc4
nadané	nadaný	k2eAgFnSc2d1
dívky	dívka	k1gFnSc2
<g/>
,	,	kIx,
o	o	k7c4
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
musel	muset	k5eAaImAgMnS
pečovat	pečovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
objevil	objevit	k5eAaPmAgMnS
na	na	k7c4
Broadwayi	Broadwaye	k1gFnSc4
v	v	k7c6
divadelní	divadelní	k2eAgFnSc6d1
hře	hra	k1gFnSc6
Lobby	lobby	k1gFnPc2
Hero	Hero	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
si	se	k3xPyFc3
zahrál	zahrát	k5eAaPmAgMnS
hlavní	hlavní	k2eAgFnSc4d1
roli	role	k1gFnSc4
agenta	agent	k1gMnSc2
Ariho	Ari	k1gMnSc2
Levinson	Levinson	k1gMnSc1
v	v	k7c6
thrillerovém	thrillerový	k2eAgInSc6d1
snímku	snímek	k1gInSc6
The	The	k1gFnSc2
Red	Red	k1gFnSc2
Sea	Sea	k1gFnSc2
Diving	Diving	k1gInSc1
Resort	resort	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
si	se	k3xPyFc3
zahrál	zahrát	k5eAaPmAgMnS
v	v	k7c6
mysteriózním	mysteriózní	k2eAgInSc6d1
snímku	snímek	k1gInSc6
Na	na	k7c4
nože	nůž	k1gInPc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
měl	mít	k5eAaImAgInS
premiéru	premiéra	k1gFnSc4
v	v	k7c6
listopadu	listopad	k1gInSc6
2019	#num#	k4
a	a	k8xC
získal	získat	k5eAaPmAgMnS
pozitivní	pozitivní	k2eAgFnPc4d1
reakce	reakce	k1gFnPc4
od	od	k7c2
kritiků	kritik	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
premiéru	premiér	k1gMnSc3
seriál	seriál	k1gInSc4
Defending	Defending	k1gInSc4
Jacob	Jacoba	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yIgInSc6,k3yQgInSc6
si	se	k3xPyFc3
jak	jak	k6eAd1
zahrál	zahrát	k5eAaPmAgMnS
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
ho	on	k3xPp3gMnSc4
také	také	k9
produkoval	produkovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
Osobní	osobní	k2eAgInSc1d1
život	život	k1gInSc1
</s>
<s>
Během	během	k7c2
let	léto	k1gNnPc2
2004	#num#	k4
až	až	k6eAd1
2006	#num#	k4
měl	mít	k5eAaImAgMnS
vztah	vztah	k1gInSc4
s	s	k7c7
herečkou	herečka	k1gFnSc7
Jessicou	Jessica	k1gFnSc7
Biel	Biel	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
září	září	k1gNnSc2
2012	#num#	k4
do	do	k7c2
října	říjen	k1gInSc2
2013	#num#	k4
byla	být	k5eAaImAgFnS
jeho	jeho	k3xOp3gFnSc1
přítelkyní	přítelkyně	k1gFnSc7
herečka	hereček	k1gMnSc2
Minka	mink	k1gMnSc2
Kelly	Kella	k1gMnSc2
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yRgFnSc7,k3yQgFnSc7,k3yIgFnSc7
již	již	k9
krátce	krátce	k6eAd1
chodil	chodit	k5eAaImAgMnS
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Podporuje	podporovat	k5eAaImIp3nS
práva	právo	k1gNnPc4
LGBT	LGBT	kA
skupin	skupina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
byl	být	k5eAaImAgInS
vychován	vychovat	k5eAaPmNgMnS
katolíky	katolík	k1gMnPc7
<g/>
,	,	kIx,
tak	tak	k9
vyjádřil	vyjádřit	k5eAaPmAgMnS
panteistické	panteistický	k2eAgNnSc4d1
vidění	vidění	k1gNnSc4
světa	svět	k1gInSc2
a	a	k8xC
velmi	velmi	k6eAd1
se	se	k3xPyFc4
zajímá	zajímat	k5eAaImIp3nS
o	o	k7c4
buddhismus	buddhismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
fanouškem	fanoušek	k1gMnSc7
fotbalového	fotbalový	k2eAgInSc2d1
týmu	tým	k1gInSc2
New	New	k1gFnSc2
England	Englanda	k1gFnPc2
Patriots	Patriotsa	k1gFnPc2
a	a	k8xC
figuroval	figurovat	k5eAaImAgMnS
jako	jako	k9
vypravěč	vypravěč	k1gMnSc1
v	v	k7c6
jejich	jejich	k3xOp3gInSc6
dokumentárním	dokumentární	k2eAgInSc6d1
snímku	snímek	k1gInSc6
s	s	k7c7
názvem	název	k1gInSc7
America	Americ	k1gInSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Game	game	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
Story	story	k1gFnSc2
of	of	k?
the	the	k?
2014	#num#	k4
New	New	k1gMnPc2
England	England	k1gInSc4
Patriots	Patriotsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Filmografie	filmografie	k1gFnSc1
</s>
<s>
Filmy	film	k1gInPc1
</s>
<s>
RokNázevRole	RokNázevRole	k1gFnSc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
1997	#num#	k4
</s>
<s>
Biodiversity	Biodiversit	k1gInPc1
<g/>
:	:	kIx,
Wild	Wild	k1gInSc1
About	About	k2eAgInSc1d1
Life	Life	k1gInSc1
<g/>
!	!	kIx.
</s>
<s>
Rick	Rick	k6eAd1
</s>
<s>
vzdělávací	vzdělávací	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
2000	#num#	k4
<g/>
The	The	k1gMnSc1
NewcomersJudd	NewcomersJudd	k1gMnSc1
</s>
<s>
2001	#num#	k4
<g/>
BulšitJake	BulšitJake	k1gNnSc1
Wyler	Wylra	k1gFnPc2
</s>
<s>
2003	#num#	k4
</s>
<s>
Paper	Paper	k1gMnSc1
Boy	boy	k1gMnSc1
</s>
<s>
krátkometrážní	krátkometrážní	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
2004	#num#	k4
<g/>
Perfektní	perfektní	k2eAgInSc1d1
skóreKyle	skóreKyl	k1gInSc5
Curtis	Curtis	k1gFnPc4
</s>
<s>
The	The	k?
Orphan	Orphan	k1gMnSc1
King	King	k1gMnSc1
</s>
<s>
Rick	Rick	k6eAd1
</s>
<s>
nevydaný	vydaný	k2eNgInSc1d1
film	film	k1gInSc1
</s>
<s>
CellularRyan	CellularRyan	k1gMnSc1
Hewitt	Hewitt	k1gMnSc1
</s>
<s>
2005	#num#	k4
<g/>
Nelítostná	lítostný	k2eNgFnSc1d1
rasaBryce	rasaBryce	k1gFnSc1
Langley	Langlea	k1gFnSc2
</s>
<s>
Fantastická	fantastický	k2eAgFnSc1d1
čtyřkaJohnny	čtyřkaJohnn	k1gInPc4
Storm	Storm	k1gInSc1
/	/	kIx~
Human	Human	k1gMnSc1
Torch	Torch	k1gMnSc1
</s>
<s>
LondonSyd	LondonSyd	k6eAd1
</s>
<s>
2007	#num#	k4
<g/>
Želvy	želva	k1gFnSc2
NinjaCasey	NinjaCasea	k1gFnSc2
Jones	Jones	k1gMnSc1
(	(	kIx(
<g/>
hlas	hlas	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
SunshineRobert	SunshineRobert	k1gInSc1
Mace	Mac	k1gFnSc2
</s>
<s>
Fantastická	fantastický	k2eAgFnSc1d1
čtyřka	čtyřka	k1gFnSc1
a	a	k8xC
Silver	Silver	k1gMnSc1
SurferJohnny	SurferJohnna	k1gFnSc2
Storm	Storm	k1gInSc1
/	/	kIx~
Human	Human	k1gMnSc1
Torch	Torch	k1gMnSc1
</s>
<s>
Holka	holka	k1gFnSc1
na	na	k7c6
hlídáníHayden	hlídáníHaydno	k1gNnPc2
„	„	k?
<g/>
Harvard	Harvard	k1gInSc1
Hottie	Hottie	k1gFnSc1
<g/>
“	“	k?
</s>
<s>
Bitva	bitva	k1gFnSc1
o	o	k7c4
planetu	planeta	k1gFnSc4
Terra	Terr	k1gInSc2
3	#num#	k4
<g/>
DStewart	DStewart	k1gInSc1
Stanton	Stanton	k1gInSc4
(	(	kIx(
<g/>
hlas	hlas	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
2008	#num#	k4
<g/>
Street	Streeta	k1gFnPc2
Kingsdetektiv	Kingsdetektiv	k1gInSc1
Paul	Paul	k1gMnSc1
Diskant	diskant	k1gInSc1
</s>
<s>
Ztráta	ztráta	k1gFnSc1
diamantové	diamantový	k2eAgFnSc2d1
slzyJimmy	slzyJimma	k1gFnSc2
Dobyne	Dobyn	k1gInSc5
</s>
<s>
2009	#num#	k4
<g/>
PushNick	PushNick	k1gMnSc1
Gant	Gant	k1gMnSc1
</s>
<s>
2010	#num#	k4
<g/>
ParchantiJake	ParchantiJake	k1gNnSc1
Jensen	Jensna	k1gFnPc2
</s>
<s>
Scott	Scott	k1gMnSc1
Pilgrim	Pilgrim	k1gMnSc1
proti	proti	k7c3
zbytku	zbytek	k1gInSc3
světa	svět	k1gInSc2
Lucas	Lucasa	k1gFnPc2
Lee	Lea	k1gFnSc6
</s>
<s>
2011	#num#	k4
<g/>
PunctureMichael	PunctureMichael	k1gMnSc1
David	David	k1gMnSc1
„	„	k?
<g/>
Mike	Mik	k1gFnSc2
<g/>
“	“	k?
Weiss	Weiss	k1gMnSc1
</s>
<s>
Captain	Captain	k1gMnSc1
America	Americus	k1gMnSc2
<g/>
:	:	kIx,
První	první	k4xOgFnPc1
AvengerSteve	AvengerSteev	k1gFnPc1
Rogers	Rogersa	k1gFnPc2
/	/	kIx~
Kapitán	kapitán	k1gMnSc1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
ty	ten	k3xDgMnPc4
jsi	být	k5eAaImIp2nS
za	za	k7c4
číslo	číslo	k1gNnSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
Colin	Colin	k1gMnSc1
Shea	Shea	k1gMnSc1
</s>
<s>
2012	#num#	k4
<g/>
AvengersSteve	AvengersSteev	k1gFnSc2
Rogers	Rogersa	k1gFnPc2
/	/	kIx~
Kapitán	kapitán	k1gMnSc1
Amerika	Amerika	k1gFnSc1
</s>
<s>
The	The	k?
IcemanRobert	IcemanRobert	k1gMnSc1
„	„	k?
<g/>
Mr	Mr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Freezy	Freez	k1gInPc4
<g/>
“	“	k?
Pronge	Prong	k1gFnSc2
</s>
<s>
2013	#num#	k4
<g/>
Ledová	ledový	k2eAgFnSc1d1
archaCurtis	archaCurtis	k1gFnSc1
Everett	Everetta	k1gFnPc2
</s>
<s>
Thor	Thor	k1gInSc1
<g/>
:	:	kIx,
Temný	temný	k2eAgInSc1d1
světSteve	světStevat	k5eAaPmIp3nS
Rogers	Rogers	k1gInSc1
/	/	kIx~
Kapitán	kapitán	k1gMnSc1
Amerika	Amerika	k1gFnSc1
</s>
<s>
cameo	cameo	k6eAd1
</s>
<s>
2014	#num#	k4
<g/>
Captain	Captain	k1gMnSc1
America	America	k1gMnSc1
<g/>
:	:	kIx,
Návrat	návrat	k1gInSc1
prvního	první	k4xOgMnSc2
AvengeraSteve	AvengeraSteev	k1gFnPc4
Rogers	Rogers	k1gInSc1
/	/	kIx~
Kapitán	kapitán	k1gMnSc1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Noc	noc	k1gFnSc1
v	v	k7c6
New	New	k1gFnSc6
YorkuNick	YorkuNick	k1gMnSc1
Vaughan	Vaughan	k1gMnSc1
</s>
<s>
režisérský	režisérský	k2eAgInSc1d1
debut	debut	k1gInSc1
<g/>
,	,	kIx,
také	také	k9
producent	producent	k1gMnSc1
</s>
<s>
2015	#num#	k4
<g/>
Kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
věří	věřit	k5eAaImIp3nS
na	na	k7c4
láskuvypravěč	láskuvypravěč	k1gMnSc1
</s>
<s>
také	také	k9
výkonný	výkonný	k2eAgMnSc1d1
producent	producent	k1gMnSc1
</s>
<s>
Avengers	Avengers	k1gInSc1
<g/>
:	:	kIx,
Age	Age	k1gFnSc1
of	of	k?
UltronSteve	UltronSteev	k1gFnSc2
Rogers	Rogersa	k1gFnPc2
/	/	kIx~
Kapitán	kapitán	k1gMnSc1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Ant-ManSteve	Ant-ManStevat	k5eAaPmIp3nS
Rogers	Rogers	k1gInSc1
/	/	kIx~
Kapitán	kapitán	k1gMnSc1
Amerika	Amerika	k1gFnSc1
</s>
<s>
cameo	cameo	k6eAd1
</s>
<s>
2016	#num#	k4
<g/>
Captain	Captain	k1gMnSc1
America	America	k1gMnSc1
<g/>
:	:	kIx,
Občanská	občanský	k2eAgFnSc1d1
válkaSteve	válkaStevat	k5eAaPmIp3nS
Rogers	Rogers	k1gInSc4
/	/	kIx~
Kapitán	kapitán	k1gMnSc1
Amerika	Amerika	k1gFnSc1
</s>
<s>
2017	#num#	k4
<g/>
Velký	velký	k2eAgInSc1d1
darFrank	darFrank	k1gInSc1
Adler	Adler	k1gMnSc1
</s>
<s>
Spider-Man	Spider-Man	k1gInSc1
<g/>
:	:	kIx,
HomecomingSteve	HomecomingSteev	k1gFnSc2
Rogers	Rogers	k1gInSc1
/	/	kIx~
Kapitán	kapitán	k1gMnSc1
Amerika	Amerika	k1gFnSc1
</s>
<s>
cameo	cameo	k6eAd1
</s>
<s>
2018	#num#	k4
<g/>
Avengers	Avengers	k1gInSc1
<g/>
:	:	kIx,
Infinity	Infinit	k2eAgFnPc1d1
WarSteve	WarSteev	k1gFnPc1
Rogers	Rogersa	k1gFnPc2
/	/	kIx~
Kapitán	kapitán	k1gMnSc1
Amerika	Amerika	k1gFnSc1
</s>
<s>
2019	#num#	k4
<g/>
Captain	Captain	k1gInSc1
MarvelSteve	MarvelSteev	k1gFnSc2
Rogers	Rogersa	k1gFnPc2
/	/	kIx~
Kapitán	kapitán	k1gMnSc1
Amerika	Amerika	k1gFnSc1
</s>
<s>
cameo	cameo	k6eAd1
</s>
<s>
Superpower	Superpower	k1gMnSc1
Dogsvypravěč	Dogsvypravěč	k1gMnSc1
</s>
<s>
The	The	k?
Red	Red	k1gFnSc1
Sea	Sea	k1gFnSc1
Diving	Diving	k1gInSc4
Resort	resort	k1gInSc1
</s>
<s>
Ari	Ari	k?
Levinson	Levinson	k1gInSc1
</s>
<s>
Avengers	Avengers	k1gInSc1
<g/>
:	:	kIx,
EndgameSteve	EndgameSteev	k1gFnSc2
Rogers	Rogers	k1gInSc1
/	/	kIx~
Kapitán	kapitán	k1gMnSc1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Na	na	k7c4
nožeRansom	nožeRansom	k1gInSc4
Drysdale	Drysdala	k1gFnSc3
</s>
<s>
Televize	televize	k1gFnSc1
</s>
<s>
RokNázevRolePoznámky	RokNázevRolePoznámka	k1gFnPc1
</s>
<s>
2000	#num#	k4
<g/>
Opposite	Opposit	k1gInSc5
SexCary	SexCar	k1gInPc4
Baston	Baston	k1gInSc1
<g/>
8	#num#	k4
dílů	díl	k1gInPc2
<g/>
;	;	kIx,
hlavní	hlavní	k2eAgFnPc4d1
role	role	k1gFnPc4
</s>
<s>
Just	just	k6eAd1
Married	Married	k1gInSc1
</s>
<s>
Josh	Josh	k1gMnSc1
</s>
<s>
nevysílaný	vysílaný	k2eNgInSc1d1
pilotní	pilotní	k2eAgInSc1d1
díl	díl	k1gInSc1
</s>
<s>
UprchlíkZack	UprchlíkZack	k1gMnSc1
Landerdíl	Landerdíl	k1gMnSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Guilt	Guilt	k1gInSc1
<g/>
“	“	k?
</s>
<s>
2001	#num#	k4
<g/>
Bostonská	bostonský	k2eAgFnSc1d1
středníNeil	středníNeil	k1gMnSc1
Mavromatesdíl	Mavromatesdíl	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Chapter	Chapter	k1gInSc1
Nine	Nine	k1gFnSc1
<g/>
“	“	k?
</s>
<s>
2002	#num#	k4
<g/>
EastwickAdamnevysílaný	EastwickAdamnevysílaný	k2eAgInSc1d1
pilotní	pilotní	k2eAgInSc1d1
díl	díl	k1gInSc1
</s>
<s>
2003	#num#	k4
<g/>
SkinBriandíl	SkinBriandíl	k1gInSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Pilot	pilot	k1gMnSc1
<g/>
“	“	k?
</s>
<s>
2008	#num#	k4
<g/>
Robot	robot	k1gMnSc1
Chickenvíce	Chickenvíce	k1gMnSc1
postavdíl	postavdíl	k1gMnSc1
<g/>
:	:	kIx,
„	„	k?
<g/>
Monstourage	Monstourage	k1gInSc1
<g/>
“	“	k?
<g/>
;	;	kIx,
dabing	dabing	k1gInSc4
</s>
<s>
2015	#num#	k4
</s>
<s>
America	America	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Game	game	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gFnSc1
2014	#num#	k4
New	New	k1gMnPc2
England	England	k1gInSc4
Patriots	Patriotsa	k1gFnPc2
</s>
<s>
vypravěč	vypravěč	k1gMnSc1
</s>
<s>
dokument	dokument	k1gInSc1
</s>
<s>
2017	#num#	k4
</s>
<s>
America	America	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Game	game	k1gInSc1
<g/>
:	:	kIx,
2016	#num#	k4
Patriots	Patriotsa	k1gFnPc2
</s>
<s>
dokument	dokument	k1gInSc1
</s>
<s>
2018	#num#	k4
</s>
<s>
Chain	Chain	k2eAgInSc1d1
of	of	k?
Command	Command	k1gInSc1
</s>
<s>
dokumentární	dokumentární	k2eAgInSc1d1
seriál	seriál	k1gInSc1
</s>
<s>
2020	#num#	k4
</s>
<s>
Defending	Defending	k1gInSc1
Jacob	Jacoba	k1gFnPc2
</s>
<s>
Andy	Anda	k1gFnPc1
Barber	Barbra	k1gFnPc2
</s>
<s>
8	#num#	k4
dílů	díl	k1gInPc2
<g/>
,	,	kIx,
také	také	k9
výkonný	výkonný	k2eAgMnSc1d1
producent	producent	k1gMnSc1
</s>
<s>
Videohry	videohra	k1gFnPc1
</s>
<s>
RokNázevRoleZemě	RokNázevRoleZemě	k6eAd1
vydání	vydání	k1gNnSc1
</s>
<s>
2005	#num#	k4
</s>
<s>
Fantastická	fantastický	k2eAgFnSc1d1
čtyřka	čtyřka	k1gFnSc1
</s>
<s>
Johnny	Johnna	k1gFnPc1
Storm	Storm	k1gInSc1
/	/	kIx~
Human	Human	k1gMnSc1
Torch	Torch	k1gMnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
2011	#num#	k4
</s>
<s>
Captain	Captain	k1gMnSc1
America	America	k1gMnSc1
<g/>
:	:	kIx,
Super	super	k2eAgInSc1d1
Soldier	Soldier	k1gInSc1
</s>
<s>
Steven	Steven	k2eAgMnSc1d1
„	„	k?
<g/>
Steve	Steve	k1gMnSc1
<g/>
“	“	k?
Rogers	Rogers	k1gInSc1
/	/	kIx~
Kapitán	kapitán	k1gMnSc1
Amerika	Amerika	k1gFnSc1
</s>
<s>
USA	USA	kA
</s>
<s>
2012	#num#	k4
</s>
<s>
Discovered	Discovered	k1gMnSc1
</s>
<s>
Sám	sám	k3xTgMnSc1
sebe	sebe	k3xPyFc4
</s>
<s>
2016	#num#	k4
</s>
<s>
Lego	lego	k1gNnSc1
Marvel	Marvela	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Avengers	Avengers	k1gInSc1
</s>
<s>
Steven	Steven	k2eAgMnSc1d1
„	„	k?
<g/>
Steve	Steve	k1gMnSc1
<g/>
“	“	k?
Rogers	Rogers	k1gInSc1
/	/	kIx~
Kapitán	kapitán	k1gMnSc1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Divadlo	divadlo	k1gNnSc1
</s>
<s>
RokNázevRolePoznámky	RokNázevRolePoznámka	k1gFnPc1
</s>
<s>
2017	#num#	k4
</s>
<s>
Our	Our	k?
Town	Town	k1gInSc1
</s>
<s>
Pan	Pan	k1gMnSc1
Webb	Webb	k1gMnSc1
</s>
<s>
Fox	fox	k1gInSc1
Theatre	Theatr	k1gInSc5
</s>
<s>
2018	#num#	k4
</s>
<s>
Lobby	lobby	k1gFnSc1
Hero	Hero	k6eAd1
</s>
<s>
Bill	Bill	k1gMnSc1
</s>
<s>
Helen	Helena	k1gFnPc2
Hayes	Hayesa	k1gFnPc2
Theatre	Theatr	k1gInSc5
<g/>
,	,	kIx,
Broadway	Broadway	k1gInPc5
</s>
<s>
Ocenění	ocenění	k1gNnSc1
a	a	k8xC
nominace	nominace	k1gFnSc1
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Kategorie	kategorie	k1gFnPc1
</s>
<s>
Nominovaná	nominovaný	k2eAgFnSc1d1
práce	práce	k1gFnSc1
</s>
<s>
Výsledek	výsledek	k1gInSc1
</s>
<s>
2005	#num#	k4
</s>
<s>
Filmové	filmový	k2eAgFnPc1d1
ceny	cena	k1gFnPc1
MTV	MTV	kA
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
tým	tým	k1gInSc4
na	na	k7c6
plátně	plátno	k1gNnSc6
</s>
<s>
Fantastická	fantastický	k2eAgFnSc1d1
čtyřka	čtyřka	k1gFnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
2007	#num#	k4
</s>
<s>
Teen	Teen	k1gInSc1
Choice	Choice	k1gFnSc2
Awards	Awardsa	k1gFnPc2
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
herec	herec	k1gMnSc1
v	v	k7c6
akčním	akční	k2eAgInSc6d1
filmu	film	k1gInSc6
</s>
<s>
Fantastická	fantastický	k2eAgFnSc1d1
čtyřka	čtyřka	k1gFnSc1
a	a	k8xC
Silver	Silver	k1gMnSc1
Surfer	Surfer	k1gMnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
souboj	souboj	k1gInSc4
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
2011	#num#	k4
</s>
<s>
Scream	Scream	k6eAd1
Awards	Awards	k1gInSc1
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3
superhrdina	superhrdina	k1gFnSc1
</s>
<s>
Captain	Captain	k1gMnSc1
America	America	k1gMnSc1
<g/>
:	:	kIx,
První	první	k4xOgInSc1
Avenger	Avenger	k1gInSc1
</s>
<s>
vítězství	vítězství	k1gNnSc1
</s>
<s>
Cena	cena	k1gFnSc1
Saturn	Saturn	k1gInSc1
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
herec	herec	k1gMnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
People	People	k6eAd1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Choice	Choice	k1gInPc5
Awards	Awards	k1gInSc1
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
filmový	filmový	k2eAgMnSc1d1
superhrdina	superhrdin	k2eAgMnSc4d1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Filmové	filmový	k2eAgFnPc1d1
ceny	cena	k1gFnPc1
MTV	MTV	kA
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
hrdina	hrdina	k1gMnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Teen	Teen	k1gInSc1
Choice	Choice	k1gFnSc2
Awards	Awardsa	k1gFnPc2
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
letní	letní	k2eAgMnSc1d1
herec	herec	k1gMnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
2012	#num#	k4
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
souboj	souboj	k1gInSc4
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Zloděj	zloděj	k1gMnSc1
scén	scéna	k1gFnPc2
</s>
<s>
Avengers	Avengers	k6eAd1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
2013	#num#	k4
</s>
<s>
People	People	k6eAd1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Choice	Choika	k1gFnSc6
Awards	Awards	k1gInSc4
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
herec	herec	k1gMnSc1
v	v	k7c6
akčním	akční	k2eAgInSc6d1
filmu	film	k1gInSc6
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
filmový	filmový	k2eAgMnSc1d1
superhrdina	superhrdin	k2eAgNnPc5d1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Filmové	filmový	k2eAgFnPc1d1
ceny	cena	k1gFnPc1
MTV	MTV	kA
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
souboj	souboj	k1gInSc4
(	(	kIx(
<g/>
společně	společně	k6eAd1
s	s	k7c7
celým	celý	k2eAgNnSc7d1
obsazením	obsazení	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
vítězství	vítězství	k1gNnSc1
</s>
<s>
2014	#num#	k4
</s>
<s>
Teen	Teen	k1gInSc1
Choice	Choice	k1gFnSc2
Awards	Awardsa	k1gFnPc2
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
herec	herec	k1gMnSc1
ve	v	k7c6
sci-fi	sci-fi	k1gNnSc6
nebo	nebo	k8xC
fantasy	fantas	k1gInPc4
</s>
<s>
Captain	Captain	k1gMnSc1
America	America	k1gMnSc1
<g/>
:	:	kIx,
Návrat	návrat	k1gInSc1
prvního	první	k4xOgMnSc2
Avengera	Avenger	k1gMnSc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
2015	#num#	k4
</s>
<s>
People	People	k6eAd1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Choice	Choice	k1gInPc5
Awards	Awards	k1gInSc1
</s>
<s>
Oblíbený	oblíbený	k2eAgMnSc1d1
herec	herec	k1gMnSc1
v	v	k7c6
akčním	akční	k2eAgInSc6d1
filmu	film	k1gInSc6
</s>
<s>
vítězství	vítězství	k1gNnSc1
</s>
<s>
Oblíbený	oblíbený	k2eAgInSc1d1
filmová	filmový	k2eAgFnSc1d1
dvojice	dvojice	k1gFnSc2
(	(	kIx(
<g/>
se	se	k3xPyFc4
Scarlett	Scarlett	k1gMnSc1
Johansson	Johansson	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Critics	Critics	k1gInSc1
<g/>
'	'	kIx"
Choice	Choice	k1gFnSc1
Movie	Movie	k1gFnSc2
Awards	Awardsa	k1gFnPc2
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
herec	herec	k1gMnSc1
v	v	k7c6
akčním	akční	k2eAgInSc6d1
filmu	film	k1gInSc6
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Filmové	filmový	k2eAgFnPc1d1
ceny	cena	k1gFnPc1
MTV	MTV	kA
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
souboj	souboj	k1gInSc4
(	(	kIx(
<g/>
Chris	Chris	k1gInSc1
Evans	Evans	k1gInSc1
vs	vs	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sebastian	Sebastian	k1gMnSc1
Stan	stan	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgInSc1d3
polibek	polibek	k1gInSc1
(	(	kIx(
<g/>
se	se	k3xPyFc4
Scarlett	Scarlett	k1gMnSc1
Johansson	Johansson	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
Saturn	Saturn	k1gInSc1
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3
filmová	filmový	k2eAgFnSc1d1
adaptace	adaptace	k1gFnSc1
komiksu	komiks	k1gInSc2
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
herec	herec	k1gMnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Teen	Teen	k1gInSc1
Choice	Choice	k1gFnSc2
Awards	Awardsa	k1gFnPc2
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
herec	herec	k1gMnSc1
ve	v	k7c6
sci-fi	sci-fi	k1gNnSc6
nebo	nebo	k8xC
fantasy	fantas	k1gInPc4
</s>
<s>
Avengers	Avengers	k1gInSc1
<g/>
:	:	kIx,
Age	Age	k1gMnSc1
of	of	k?
Ultron	Ultron	k1gMnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Zloděj	zloděj	k1gMnSc1
scén	scéna	k1gFnPc2
</s>
<s>
vítězství	vítězství	k1gNnSc1
</s>
<s>
2016	#num#	k4
</s>
<s>
Filmové	filmový	k2eAgFnPc1d1
ceny	cena	k1gFnPc1
MTV	MTV	kA
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
hrdina	hrdina	k1gMnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Kids	Kids	k1gInSc1
<g/>
'	'	kIx"
Choice	Choice	k1gFnSc1
Awards	Awardsa	k1gFnPc2
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
filmový	filmový	k2eAgMnSc1d1
herec	herec	k1gMnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Teen	Teen	k1gInSc1
Choice	Choice	k1gFnSc2
Awards	Awardsa	k1gFnPc2
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
filmový	filmový	k2eAgMnSc1d1
herec	herec	k1gMnSc1
<g/>
:	:	kIx,
sci-fi	sci-fi	k1gFnSc1
<g/>
/	/	kIx~
<g/>
fantasy	fantas	k1gInPc1
</s>
<s>
Captain	Captain	k1gMnSc1
America	America	k1gMnSc1
<g/>
:	:	kIx,
Občanská	občanský	k2eAgFnSc1d1
válka	válka	k1gFnSc1
</s>
<s>
vítězství	vítězství	k1gNnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgFnSc1d3
filmová	filmový	k2eAgFnSc1d1
chemie	chemie	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
se	s	k7c7
Sebastianem	Sebastian	k1gMnSc7
Stanem	stan	k1gInSc7
<g/>
,	,	kIx,
Anthony	Anthon	k1gMnPc7
Mackiem	Mackius	k1gMnSc7
<g/>
,	,	kIx,
Elizabeth	Elizabeth	k1gFnSc1
Olsen	Olsna	k1gFnPc2
a	a	k8xC
Jeremy	Jerema	k1gFnSc2
Rennerem	Renner	k1gMnSc7
<g/>
)	)	kIx)
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
filmový	filmový	k2eAgInSc4d1
polibek	polibek	k1gInSc4
(	(	kIx(
<g/>
s	s	k7c7
Emily	Emil	k1gMnPc7
VanCamp	VanCamp	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Critics	Critics	k1gInSc1
<g/>
'	'	kIx"
Choice	Choice	k1gFnSc1
Awards	Awardsa	k1gFnPc2
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
herec	herec	k1gMnSc1
<g/>
:	:	kIx,
akční	akční	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
2017	#num#	k4
</s>
<s>
People	People	k6eAd1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Choice	Choic	k1gMnPc4
Awards	Awardsa	k1gFnPc2
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
herec	herec	k1gMnSc1
<g/>
:	:	kIx,
akční	akční	k2eAgInSc1d1
film	film	k1gInSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Cena	cena	k1gFnSc1
Saturn	Saturn	k1gInSc1
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
herec	herec	k1gMnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Kids	Kids	k1gInSc1
<g/>
'	'	kIx"
Choice	Choice	k1gFnSc1
Awards	Awardsa	k1gFnPc2
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
filmový	filmový	k2eAgMnSc1d1
herec	herec	k1gMnSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
nakopávač	nakopávač	k1gMnSc1
zadků	zadek	k1gInPc2
</s>
<s>
vítězství	vítězství	k1gNnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgMnPc1d3
nepřátelé	nepřítel	k1gMnPc1
(	(	kIx(
<g/>
s	s	k7c7
Robertem	Robert	k1gMnSc7
Downeym	Downeym	k1gInSc4
Jr	Jr	k1gFnSc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
tým	tým	k1gInSc4
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Teen	Teen	k1gInSc1
Choice	Choice	k1gFnSc2
Awards	Awardsa	k1gFnPc2
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
filmový	filmový	k2eAgMnSc1d1
herec	herec	k1gMnSc1
<g/>
:	:	kIx,
drama	drama	k1gNnSc1
</s>
<s>
Velký	velký	k2eAgInSc1d1
dar	dar	k1gInSc1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
2018	#num#	k4
</s>
<s>
Drama	drama	k1gNnSc1
League	Leagu	k1gFnSc2
Awards	Awardsa	k1gFnPc2
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
výkon	výkon	k1gInSc4
</s>
<s>
Lobby	lobby	k1gFnSc1
Hero	Hero	k6eAd1
</s>
<s>
nominace	nominace	k1gFnSc1
</s>
<s>
Broadway	Broadwaa	k1gFnPc1
<g/>
.	.	kIx.
<g/>
com	com	k?
Audience	audience	k1gFnSc2
Awards	Awardsa	k1gFnPc2
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3
divadelní	divadelní	k2eAgMnSc1d1
herec	herec	k1gMnSc1
<g/>
:	:	kIx,
divadelní	divadelní	k2eAgFnSc1d1
hra	hra	k1gFnSc1
</s>
<s>
vítězství	vítězství	k1gNnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Chris	Chris	k1gFnSc2
Evans	Evans	k1gInSc1
(	(	kIx(
<g/>
actor	actor	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Chris	Chris	k1gFnSc2
Evans	Evans	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Chris	Chris	k1gInSc1
Evans	Evansa	k1gFnPc2
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Chris	Chris	k1gFnSc1
Evans	Evansa	k1gFnPc2
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
59594	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
136486983	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1469	#num#	k4
6211	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2005005812	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
27290860	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2005005812	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Film	film	k1gInSc1
</s>
<s>
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
