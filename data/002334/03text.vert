<s>
Botulismus	botulismus	k1gInSc1	botulismus
je	být	k5eAaImIp3nS	být
alimentární	alimentární	k2eAgFnSc1d1	alimentární
intoxikace	intoxikace	k1gFnSc1	intoxikace
vznikající	vznikající	k2eAgFnSc1d1	vznikající
po	po	k7c4	po
pozření	pozření	k1gNnSc4	pozření
neurotoxinů	neurotoxin	k1gInPc2	neurotoxin
produkovaných	produkovaný	k2eAgFnPc2d1	produkovaná
bakterií	bakterie	k1gFnPc2	bakterie
Clostridium	Clostridium	k1gNnSc4	Clostridium
botulinum	botulinum	k1gInSc4	botulinum
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
komerčně	komerčně	k6eAd1	komerčně
chované	chovaný	k2eAgFnSc2d1	chovaná
drůbeže	drůbež	k1gFnSc2	drůbež
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
vzácně	vzácně	k6eAd1	vzácně
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ojedinělým	ojedinělý	k2eAgNnPc3d1	ojedinělé
<g/>
,	,	kIx,	,
ekonomicky	ekonomicky	k6eAd1	ekonomicky
významným	významný	k2eAgMnPc3d1	významný
výskytům	výskyt	k1gInPc3	výskyt
botulismu	botulismus	k1gInSc2	botulismus
dochází	docházet	k5eAaImIp3nS	docházet
při	při	k7c6	při
hromadném	hromadný	k2eAgNnSc6d1	hromadné
postižení	postižení	k1gNnSc6	postižení
velkochovů	velkochov	k1gInPc2	velkochov
kachen	kachna	k1gFnPc2	kachna
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
bažantů	bažant	k1gMnPc2	bažant
<g/>
.	.	kIx.	.
</s>
<s>
Častější	častý	k2eAgInPc1d2	častější
jsou	být	k5eAaImIp3nP	být
výskyty	výskyt	k1gInPc1	výskyt
u	u	k7c2	u
migrujících	migrující	k2eAgInPc2d1	migrující
<g/>
,	,	kIx,	,
divoce	divoce	k6eAd1	divoce
žijících	žijící	k2eAgMnPc2d1	žijící
vodních	vodní	k2eAgMnPc2d1	vodní
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
výskyty	výskyt	k1gInPc4	výskyt
botulismu	botulismus	k1gInSc2	botulismus
u	u	k7c2	u
kuřat	kuře	k1gNnPc2	kuře
popsali	popsat	k5eAaPmAgMnP	popsat
Dickson	Dickson	k1gNnSc4	Dickson
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
a	a	k8xC	a
u	u	k7c2	u
kachen	kachna	k1gFnPc2	kachna
Kalmbach	Kalmbacha	k1gFnPc2	Kalmbacha
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bengston	Bengston	k1gInSc1	Bengston
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
popsal	popsat	k5eAaPmAgMnS	popsat
botulismus	botulismus	k1gInSc4	botulismus
u	u	k7c2	u
kuřat	kuře	k1gNnPc2	kuře
po	po	k7c6	po
pozření	pozření	k1gNnSc6	pozření
larev	larva	k1gFnPc2	larva
mouchy	moucha	k1gFnSc2	moucha
bzučivky	bzučivka	k1gFnSc2	bzučivka
zlaté	zlatá	k1gFnSc2	zlatá
(	(	kIx(	(
<g/>
Lucilia	Lucilia	k1gFnSc1	Lucilia
caesar	caesar	k1gMnSc1	caesar
<g/>
)	)	kIx)	)
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
provedl	provést	k5eAaPmAgInS	provést
první	první	k4xOgInSc1	první
průkaz	průkaz	k1gInSc1	průkaz
toxinu	toxin	k1gInSc2	toxin
u	u	k7c2	u
těchto	tento	k3xDgMnPc2	tento
bezobratlých	bezobratlý	k2eAgMnPc2d1	bezobratlý
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
toxémii	toxémie	k1gFnSc3	toxémie
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
dochází	docházet	k5eAaImIp3nS	docházet
po	po	k7c6	po
pozření	pozření	k1gNnSc6	pozření
neurotoxinu	neurotoxin	k1gInSc2	neurotoxin
obsaženého	obsažený	k2eAgInSc2d1	obsažený
v	v	k7c6	v
kadáverech	kadáver	k1gInPc6	kadáver
<g/>
,	,	kIx,	,
larvách	larva	k1gFnPc6	larva
much	moucha	k1gFnPc2	moucha
či	či	k8xC	či
brouků	brouk	k1gMnPc2	brouk
<g/>
,	,	kIx,	,
vodních	vodní	k2eAgMnPc2d1	vodní
korýšů	korýš	k1gMnPc2	korýš
nebo	nebo	k8xC	nebo
i	i	k9	i
v	v	k7c6	v
bahně	bahno	k1gNnSc6	bahno
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
rybníků	rybník	k1gInPc2	rybník
a	a	k8xC	a
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Toxin	toxin	k1gInSc1	toxin
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
i	i	k9	i
ve	v	k7c6	v
hnijících	hnijící	k2eAgFnPc6d1	hnijící
rostlinách	rostlina	k1gFnPc6	rostlina
během	během	k7c2	během
horkého	horký	k2eAgNnSc2d1	horké
léta	léto	k1gNnSc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
většina	většina	k1gFnSc1	většina
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
vnímavá	vnímavý	k2eAgFnSc1d1	vnímavá
k	k	k7c3	k
toxinům	toxin	k1gInPc3	toxin
C.	C.	kA	C.
botulinum	botulinum	k1gInSc4	botulinum
typu	typ	k1gInSc2	typ
C.	C.	kA	C.
U	u	k7c2	u
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
vodních	vodní	k2eAgMnPc2d1	vodní
ptáků	pták	k1gMnPc2	pták
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
enzootickým	enzootický	k2eAgFnPc3d1	enzootická
intoxikacím	intoxikace	k1gFnPc3	intoxikace
zejména	zejména	k9	zejména
po	po	k7c6	po
obdobích	období	k1gNnPc6	období
sucha	sucho	k1gNnSc2	sucho
nebo	nebo	k8xC	nebo
po	po	k7c6	po
povodních	povodeň	k1gFnPc6	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Mrchožraví	mrchožravý	k2eAgMnPc1d1	mrchožravý
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
považování	považování	k1gNnSc4	považování
za	za	k7c4	za
rezistentní	rezistentní	k2eAgNnSc4d1	rezistentní
k	k	k7c3	k
neurotoxinům	neurotoxina	k1gMnPc3	neurotoxina
C.	C.	kA	C.
botulinum	botulinum	k1gNnSc1	botulinum
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
mechanismus	mechanismus	k1gInSc1	mechanismus
není	být	k5eNaImIp3nS	být
ještě	ještě	k6eAd1	ještě
znám	znám	k2eAgMnSc1d1	znám
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
u	u	k7c2	u
dravců	dravec	k1gMnPc2	dravec
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
snížená	snížený	k2eAgFnSc1d1	snížená
vnímavost	vnímavost	k1gFnSc1	vnímavost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Jensena	Jensen	k2eAgMnSc4d1	Jensen
a	a	k8xC	a
Priceho	Price	k1gMnSc4	Price
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
však	však	k9	však
i	i	k9	i
tito	tento	k3xDgMnPc1	tento
ptáci	pták	k1gMnPc1	pták
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
postiženi	postihnout	k5eAaPmNgMnP	postihnout
botulismem	botulismus	k1gInSc7	botulismus
typu	typ	k1gInSc2	typ
C.	C.	kA	C.
Výskyt	výskyt	k1gInSc1	výskyt
botulismu	botulismus	k1gInSc2	botulismus
je	být	k5eAaImIp3nS	být
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
<g/>
.	.	kIx.	.
</s>
<s>
Zoohygienické	zoohygienický	k2eAgFnPc1d1	zoohygienický
podmínky	podmínka	k1gFnPc1	podmínka
v	v	k7c6	v
intenzivních	intenzivní	k2eAgInPc6d1	intenzivní
chovech	chov	k1gInPc6	chov
drůbeže	drůbež	k1gFnSc2	drůbež
preventivně	preventivně	k6eAd1	preventivně
zabraňují	zabraňovat	k5eAaImIp3nP	zabraňovat
vzniku	vznik	k1gInSc2	vznik
botulismu	botulismus	k1gInSc2	botulismus
<g/>
;	;	kIx,	;
přesto	přesto	k8xC	přesto
bylo	být	k5eAaImAgNnS	být
popsáno	popsat	k5eAaPmNgNnS	popsat
několik	několik	k4yIc1	několik
případů	případ	k1gInPc2	případ
těžkého	těžký	k2eAgNnSc2d1	těžké
onemocnění	onemocnění	k1gNnSc2	onemocnění
u	u	k7c2	u
výkrmových	výkrmový	k2eAgNnPc2d1	výkrmový
kuřat	kuře	k1gNnPc2	kuře
(	(	kIx(	(
<g/>
Page	Page	k1gNnPc2	Page
a	a	k8xC	a
Fletcher	Fletchra	k1gFnPc2	Fletchra
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
<g/>
;	;	kIx,	;
Dohm	Dohm	k1gMnSc1	Dohm
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
;	;	kIx,	;
Smart	Smarta	k1gFnPc2	Smarta
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
význam	význam	k1gInSc1	význam
botulismu	botulismus	k1gInSc2	botulismus
typu	typ	k1gInSc2	typ
C	C	kA	C
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
minimální	minimální	k2eAgInSc4d1	minimální
<g/>
.	.	kIx.	.
</s>
<s>
Botulismus	botulismus	k1gInSc1	botulismus
je	být	k5eAaImIp3nS	být
vyvoláván	vyvolávat	k5eAaImNgInS	vyvolávat
neurotoxinem	neurotoxin	k1gInSc7	neurotoxin
produkovaným	produkovaný	k2eAgInSc7d1	produkovaný
půdní	půdní	k2eAgFnSc7d1	půdní
bakterií	bakterie	k1gFnSc7	bakterie
Clostridium	Clostridium	k1gNnSc4	Clostridium
botulinum	botulinum	k1gInSc4	botulinum
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	s	k7c7	s
4	[number]	k4	4
biologicky	biologicky	k6eAd1	biologicky
odlišné	odlišný	k2eAgFnSc2d1	odlišná
skupiny	skupina	k1gFnSc2	skupina
bakterií	bakterie	k1gFnPc2	bakterie
C.	C.	kA	C.
botulinum	botulinum	k1gInSc1	botulinum
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
produkují	produkovat	k5eAaImIp3nP	produkovat
neurotoxin	neurotoxin	k1gInSc4	neurotoxin
stejných	stejný	k2eAgFnPc2d1	stejná
fyziologických	fyziologický	k2eAgFnPc2d1	fyziologická
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
8	[number]	k4	8
různých	různý	k2eAgInPc2d1	různý
antigenních	antigenní	k2eAgInPc2d1	antigenní
typů	typ	k1gInPc2	typ
(	(	kIx(	(
<g/>
A-G	A-G	k1gFnPc2	A-G
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toxigenní	Toxigenní	k2eAgFnSc1d1	Toxigenní
skupina	skupina	k1gFnSc1	skupina
bakterií	bakterie	k1gFnPc2	bakterie
typu	typ	k1gInSc2	typ
C	C	kA	C
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
subtypy	subtyp	k1gInPc4	subtyp
C1	C1	k1gMnPc2	C1
a	a	k8xC	a
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Produkující	produkující	k2eAgNnSc1d1	produkující
klostridium	klostridium	k1gNnSc1	klostridium
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
názvem	název	k1gInSc7	název
antigenního	antigenní	k2eAgInSc2d1	antigenní
typu	typ	k1gInSc2	typ
toxinu	toxin	k1gInSc2	toxin
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
botulismus	botulismus	k1gInSc1	botulismus
převážně	převážně	k6eAd1	převážně
způsobován	způsobovat	k5eAaImNgInS	způsobovat
typem	typ	k1gInSc7	typ
C	C	kA	C
<g/>
,	,	kIx,	,
ojediněle	ojediněle	k6eAd1	ojediněle
i	i	k9	i
typy	typ	k1gInPc1	typ
A	A	kA	A
a	a	k8xC	a
E.	E.	kA	E.
Neurotoxin	Neurotoxin	k1gInSc1	Neurotoxin
je	být	k5eAaImIp3nS	být
uvolňován	uvolňovat	k5eAaImNgInS	uvolňovat
z	z	k7c2	z
buněk	buňka	k1gFnPc2	buňka
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
při	při	k7c6	při
jejich	jejich	k3xOp3gInSc6	jejich
rozpadu	rozpad	k1gInSc6	rozpad
(	(	kIx(	(
<g/>
lýze	lýze	k1gFnSc1	lýze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Botulinové	botulinový	k2eAgInPc1d1	botulinový
toxiny	toxin	k1gInPc1	toxin
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nejsilnější	silný	k2eAgInPc4d3	nejsilnější
známé	známý	k2eAgInPc4d1	známý
toxiny	toxin	k1gInPc4	toxin
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Toxin	toxin	k1gInSc1	toxin
typu	typ	k1gInSc2	typ
C	C	kA	C
je	být	k5eAaImIp3nS	být
produkován	produkován	k2eAgInSc1d1	produkován
za	za	k7c2	za
anaerobních	anaerobní	k2eAgFnPc2d1	anaerobní
podmínek	podmínka	k1gFnPc2	podmínka
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
mezi	mezi	k7c7	mezi
10-47	[number]	k4	10-47
°	°	k?	°
<g/>
C	C	kA	C
s	s	k7c7	s
optimem	optimum	k1gNnSc7	optimum
produkce	produkce	k1gFnSc2	produkce
mezi	mezi	k7c4	mezi
35-37	[number]	k4	35-37
°	°	k?	°
<g/>
C.	C.	kA	C.
Toxiny	toxin	k1gInPc1	toxin
jsou	být	k5eAaImIp3nP	být
syntetizovány	syntetizován	k2eAgInPc1d1	syntetizován
bakteriální	bakteriální	k2eAgFnSc7d1	bakteriální
buňkou	buňka	k1gFnSc7	buňka
jako	jako	k8xS	jako
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
netoxické	toxický	k2eNgInPc4d1	netoxický
polypeptidy	polypeptid	k1gInPc4	polypeptid
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
rozštěpení	rozštěpení	k1gNnSc6	rozštěpení
bakteriálními	bakteriální	k2eAgFnPc7d1	bakteriální
proteázami	proteáza	k1gFnPc7	proteáza
nebo	nebo	k8xC	nebo
trypsinem	trypsin	k1gInSc7	trypsin
ve	v	k7c6	v
střevě	střevo	k1gNnSc6	střevo
hostitele	hostitel	k1gMnSc2	hostitel
nabývají	nabývat	k5eAaImIp3nP	nabývat
toxicity	toxicita	k1gFnPc1	toxicita
<g/>
.	.	kIx.	.
</s>
<s>
Botulotoxiny	botulotoxin	k1gInPc1	botulotoxin
jsou	být	k5eAaImIp3nP	být
ničeny	ničit	k5eAaImNgInP	ničit
teplem	teplo	k1gNnSc7	teplo
<g/>
,	,	kIx,	,
světlem	světlo	k1gNnSc7	světlo
<g/>
,	,	kIx,	,
zářením	záření	k1gNnSc7	záření
a	a	k8xC	a
vysoce	vysoce	k6eAd1	vysoce
alkalickým	alkalický	k2eAgNnSc7d1	alkalické
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Producenty	producent	k1gMnPc7	producent
botulotoxinu	botulotoxin	k1gInSc2	botulotoxin
jsou	být	k5eAaImIp3nP	být
anaerobní	anaerobní	k2eAgFnPc1d1	anaerobní
<g/>
,	,	kIx,	,
sporoformní	sporoformní	k2eAgFnPc1d1	sporoformní
a	a	k8xC	a
pohyblivé	pohyblivý	k2eAgFnPc1d1	pohyblivá
tyčinky	tyčinka	k1gFnPc1	tyčinka
C.	C.	kA	C.
botulinum	botulinum	k1gInSc4	botulinum
průměrné	průměrný	k2eAgFnSc2d1	průměrná
velikosti	velikost	k1gFnSc2	velikost
4-6	[number]	k4	4-6
x	x	k?	x
1,0	[number]	k4	1,0
μ	μ	k?	μ
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
často	často	k6eAd1	často
ojediněle	ojediněle	k6eAd1	ojediněle
nebo	nebo	k8xC	nebo
v	v	k7c6	v
krátkých	krátký	k2eAgInPc6d1	krátký
řetízcích	řetízek	k1gInPc6	řetízek
<g/>
.	.	kIx.	.
</s>
<s>
Subterminální	Subterminální	k2eAgInPc1d1	Subterminální
spory	spor	k1gInPc1	spor
jsou	být	k5eAaImIp3nP	být
oválně	oválně	k6eAd1	oválně
a	a	k8xC	a
tyčinku	tyčinka	k1gFnSc4	tyčinka
vydouvají	vydouvat	k5eAaImIp3nP	vydouvat
<g/>
.	.	kIx.	.
</s>
<s>
Termostabilita	Termostabilita	k1gFnSc1	Termostabilita
spor	spor	k1gInSc1	spor
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
podle	podle	k7c2	podle
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
krevním	krevní	k2eAgInSc6d1	krevní
agaru	agar	k1gInSc6	agar
pro	pro	k7c4	pro
anaeroby	anaerob	k1gMnPc4	anaerob
roste	růst	k5eAaImIp3nS	růst
C.	C.	kA	C.
botulinum	botulinum	k1gInSc1	botulinum
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
plochých	plochý	k2eAgFnPc2d1	plochá
<g/>
,	,	kIx,	,
šedobílých	šedobílý	k2eAgFnPc2d1	šedobílá
kolonií	kolonie	k1gFnPc2	kolonie
velikosti	velikost	k1gFnSc2	velikost
1-4	[number]	k4	1-4
mm	mm	kA	mm
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
hemolýzou	hemolýza	k1gFnSc7	hemolýza
<g/>
.	.	kIx.	.
</s>
<s>
Sacharolytické	Sacharolytický	k2eAgInPc1d1	Sacharolytický
typy	typ	k1gInPc1	typ
štěpí	štěpit	k5eAaImIp3nP	štěpit
omezený	omezený	k2eAgInSc4d1	omezený
počet	počet	k1gInSc4	počet
cukrů	cukr	k1gInPc2	cukr
při	při	k7c6	při
mírném	mírný	k2eAgInSc6d1	mírný
poklesu	pokles	k1gInSc6	pokles
pH	ph	kA	ph
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
typy	typ	k1gInPc1	typ
kromě	kromě	k7c2	kromě
typu	typ	k1gInSc2	typ
G	G	kA	G
produkují	produkovat	k5eAaImIp3nP	produkovat
lipázu	lipáza	k1gFnSc4	lipáza
<g/>
.	.	kIx.	.
</s>
<s>
Proteolytické	proteolytický	k2eAgInPc1d1	proteolytický
typy	typ	k1gInPc1	typ
hydrolyzují	hydrolyzovat	k5eAaBmIp3nP	hydrolyzovat
želatinu	želatina	k1gFnSc4	želatina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
botulismu	botulismus	k1gInSc3	botulismus
typu	typ	k1gInSc2	typ
C	C	kA	C
je	být	k5eAaImIp3nS	být
vnímavých	vnímavý	k2eAgInPc2d1	vnímavý
mnoho	mnoho	k4c4	mnoho
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
včetně	včetně	k7c2	včetně
domácí	domácí	k2eAgFnSc2d1	domácí
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
,	,	kIx,	,
bažantů	bažant	k1gMnPc2	bažant
a	a	k8xC	a
pštrosů	pštros	k1gMnPc2	pštros
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
byl	být	k5eAaImAgInS	být
pozorován	pozorovat	k5eAaImNgInS	pozorovat
u	u	k7c2	u
117	[number]	k4	117
ptačích	ptačí	k2eAgInPc2d1	ptačí
druhů	druh	k1gInPc2	druh
patřících	patřící	k2eAgInPc2d1	patřící
do	do	k7c2	do
22	[number]	k4	22
čeledí	čeleď	k1gFnPc2	čeleď
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
i	i	k9	i
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
a	a	k8xC	a
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
laboratorních	laboratorní	k2eAgMnPc2d1	laboratorní
hlodavců	hlodavec	k1gMnPc2	hlodavec
jsou	být	k5eAaImIp3nP	být
nejvnímavější	vnímavý	k2eAgFnPc1d3	nejvnímavější
myši	myš	k1gFnPc1	myš
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
také	také	k6eAd1	také
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
biologické	biologický	k2eAgFnSc3d1	biologická
testaci	testace	k1gFnSc3	testace
<g/>
,	,	kIx,	,
detekci	detekce	k1gFnSc3	detekce
toxinu	toxin	k1gInSc2	toxin
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
typizaci	typizace	k1gFnSc4	typizace
<g/>
.	.	kIx.	.
</s>
<s>
Botulismus	botulismus	k1gInSc1	botulismus
typu	typ	k1gInSc2	typ
A	A	kA	A
a	a	k8xC	a
E	E	kA	E
bývá	bývat	k5eAaImIp3nS	bývat
pozorován	pozorovat	k5eAaImNgInS	pozorovat
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
drobnochovech	drobnochovo	k1gNnPc6	drobnochovo
drůbeže	drůbež	k1gFnSc2	drůbež
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
zkrmováním	zkrmování	k1gNnSc7	zkrmování
zkažených	zkažený	k2eAgFnPc2d1	zkažená
potravin	potravina	k1gFnPc2	potravina
nebo	nebo	k8xC	nebo
u	u	k7c2	u
výkrmových	výkrmový	k2eAgNnPc2d1	výkrmový
kuřat	kuře	k1gNnPc2	kuře
po	po	k7c6	po
zkrmování	zkrmování	k1gNnSc6	zkrmování
kontaminovaného	kontaminovaný	k2eAgNnSc2d1	kontaminované
krmiva	krmivo	k1gNnSc2	krmivo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mořských	mořský	k2eAgMnPc2d1	mořský
ptáků	pták	k1gMnPc2	pták
byl	být	k5eAaImAgInS	být
botulismus	botulismus	k1gInSc1	botulismus
pozorován	pozorovat	k5eAaImNgInS	pozorovat
po	po	k7c4	po
pozření	pozření	k1gNnSc4	pozření
uhynulých	uhynulý	k2eAgFnPc2d1	uhynulá
ryb	ryba	k1gFnPc2	ryba
kontaminovaných	kontaminovaný	k2eAgFnPc2d1	kontaminovaná
toxinem	toxin	k1gInSc7	toxin
typu	typ	k1gInSc2	typ
E.	E.	kA	E.
Bakterie	bakterie	k1gFnSc2	bakterie
C.	C.	kA	C.
botulinum	botulinum	k1gInSc4	botulinum
typu	typ	k1gInSc2	typ
C	C	kA	C
jsou	být	k5eAaImIp3nP	být
rozšířeny	rozšířit	k5eAaPmNgFnP	rozšířit
celosvětově	celosvětově	k6eAd1	celosvětově
<g/>
,	,	kIx,	,
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
i	i	k8xC	i
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
nalézány	nalézat	k5eAaImNgFnP	nalézat
v	v	k7c4	v
prostředí	prostředí	k1gNnSc4	prostředí
drůbežích	drůbeží	k2eAgFnPc2d1	drůbeží
a	a	k8xC	a
bažantích	bažantí	k2eAgFnPc2d1	bažantí
farem	farma	k1gFnPc2	farma
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
obligátního	obligátní	k2eAgMnSc4d1	obligátní
parazita	parazit	k1gMnSc4	parazit
trávicího	trávicí	k2eAgInSc2d1	trávicí
traktu	trakt	k1gInSc2	trakt
divokých	divoký	k2eAgMnPc2d1	divoký
i	i	k8xC	i
domestikovaných	domestikovaný	k2eAgMnPc2d1	domestikovaný
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
jeho	jeho	k3xOp3gNnSc3	jeho
šíření	šíření	k1gNnSc3	šíření
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Častým	častý	k2eAgInSc7d1	častý
zdrojem	zdroj	k1gInSc7	zdroj
botulotoxinu	botulotoxin	k1gInSc2	botulotoxin
jsou	být	k5eAaImIp3nP	být
kadávery	kadáver	k1gInPc1	kadáver
uhynulých	uhynulý	k2eAgMnPc2d1	uhynulý
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
botulismu	botulismus	k1gInSc3	botulismus
typu	typ	k1gInSc2	typ
C	C	kA	C
dochází	docházet	k5eAaImIp3nS	docházet
nejčastěji	často	k6eAd3	často
pozřením	pozření	k1gNnSc7	pozření
preformovaného	preformovaný	k2eAgInSc2d1	preformovaný
botulotoxinu	botulotoxin	k1gInSc2	botulotoxin
<g/>
.	.	kIx.	.
</s>
<s>
Přibývá	přibývat	k5eAaImIp3nS	přibývat
ale	ale	k9	ale
důkazů	důkaz	k1gInPc2	důkaz
nasvědčujících	nasvědčující	k2eAgInPc2d1	nasvědčující
možnosti	možnost	k1gFnPc4	možnost
vzniku	vznik	k1gInSc2	vznik
endogenního	endogenní	k2eAgInSc2d1	endogenní
botulismu	botulismus	k1gInSc2	botulismus
jako	jako	k8xS	jako
následku	následek	k1gInSc2	následek
primární	primární	k2eAgFnSc2d1	primární
kolonizace	kolonizace	k1gFnSc2	kolonizace
střevního	střevní	k2eAgInSc2d1	střevní
traktu	trakt	k1gInSc2	trakt
bakterií	bakterie	k1gFnPc2	bakterie
C.	C.	kA	C.
botulinum	botulinum	k1gInSc1	botulinum
a	a	k8xC	a
produkce	produkce	k1gFnSc1	produkce
botulotoxinu	botulotoxin	k1gInSc2	botulotoxin
ve	v	k7c6	v
slepých	slepý	k2eAgNnPc6d1	slepé
střevech	střevo	k1gNnPc6	střevo
in	in	k?	in
vivo	vivo	k6eAd1	vivo
<g/>
.	.	kIx.	.
</s>
<s>
Podmínky	podmínka	k1gFnPc1	podmínka
vzniku	vznik	k1gInSc2	vznik
této	tento	k3xDgFnSc2	tento
formy	forma	k1gFnSc2	forma
botulismu	botulismus	k1gInSc2	botulismus
nejsou	být	k5eNaImIp3nP	být
plně	plně	k6eAd1	plně
známy	znám	k2eAgFnPc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
bažantů	bažant	k1gMnPc2	bažant
je	být	k5eAaImIp3nS	být
toxin	toxin	k1gInSc1	toxin
produkován	produkovat	k5eAaImNgInS	produkovat
již	již	k6eAd1	již
ve	v	k7c6	v
voleti	vole	k1gNnSc6	vole
(	(	kIx(	(
<g/>
Dinter	Dinter	k1gMnSc1	Dinter
a	a	k8xC	a
Kull	Kull	k1gMnSc1	Kull
<g/>
,	,	kIx,	,
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
trávicího	trávicí	k2eAgInSc2d1	trávicí
traktu	trakt	k1gInSc2	trakt
se	se	k3xPyFc4	se
botulotoxin	botulotoxin	k1gInSc1	botulotoxin
dostává	dostávat	k5eAaImIp3nS	dostávat
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
a	a	k8xC	a
do	do	k7c2	do
CNS	CNS	kA	CNS
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
blokuje	blokovat	k5eAaImIp3nS	blokovat
uvolňování	uvolňování	k1gNnSc1	uvolňování
acetylcholinu	acetylcholin	k1gInSc2	acetylcholin
na	na	k7c6	na
presynaptických	presynaptický	k2eAgFnPc6d1	presynaptická
membránách	membrána	k1gFnPc6	membrána
cholinergních	cholinergní	k2eAgInPc2d1	cholinergní
nervů	nerv	k1gInPc2	nerv
<g/>
.	.	kIx.	.
</s>
<s>
Poruchou	porucha	k1gFnSc7	porucha
přenosu	přenos	k1gInSc2	přenos
vzruchu	vzruch	k1gInSc2	vzruch
v	v	k7c6	v
synapsích	synapse	k1gFnPc6	synapse
motorických	motorický	k2eAgInPc2d1	motorický
nervů	nerv	k1gInPc2	nerv
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
parézám	paréza	k1gFnPc3	paréza
a	a	k8xC	a
k	k	k7c3	k
úhynu	úhyn	k1gInSc3	úhyn
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
respirační	respirační	k2eAgFnSc2d1	respirační
paralýzy	paralýza	k1gFnSc2	paralýza
<g/>
.	.	kIx.	.
</s>
<s>
Botulotoxin	botulotoxin	k1gInSc1	botulotoxin
také	také	k9	také
poškozuje	poškozovat	k5eAaImIp3nS	poškozovat
endotel	endotel	k1gInSc1	endotel
cév	céva	k1gFnPc2	céva
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
edému	edém	k1gInSc2	edém
a	a	k8xC	a
hemoragií	hemoragie	k1gFnPc2	hemoragie
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vysoké	vysoký	k2eAgFnSc6d1	vysoká
dávce	dávka	k1gFnSc6	dávka
botulotoxinu	botulotoxin	k1gInSc2	botulotoxin
se	se	k3xPyFc4	se
příznaky	příznak	k1gInPc1	příznak
objevují	objevovat	k5eAaImIp3nP	objevovat
během	během	k7c2	během
několika	několik	k4yIc2	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
při	při	k7c6	při
nízké	nízký	k2eAgFnSc6d1	nízká
dávce	dávka	k1gFnSc6	dávka
se	se	k3xPyFc4	se
paralýza	paralýza	k1gFnSc1	paralýza
objevuje	objevovat	k5eAaImIp3nS	objevovat
za	za	k7c4	za
1-2	[number]	k4	1-2
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Klinické	klinický	k2eAgInPc1d1	klinický
příznaky	příznak	k1gInPc1	příznak
u	u	k7c2	u
domácí	domácí	k2eAgFnSc2d1	domácí
drůbeže	drůbež	k1gFnSc2	drůbež
i	i	k8xC	i
bažantů	bažant	k1gMnPc2	bažant
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgInPc1d1	podobný
a	a	k8xC	a
začínají	začínat	k5eAaImIp3nP	začínat
obvykle	obvykle	k6eAd1	obvykle
paralýzou	paralýza	k1gFnSc7	paralýza
končetin	končetina	k1gFnPc2	končetina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
postupně	postupně	k6eAd1	postupně
přechází	přecházet	k5eAaImIp3nS	přecházet
na	na	k7c4	na
křídla	křídlo	k1gNnPc4	křídlo
<g/>
,	,	kIx,	,
krk	krk	k1gInSc1	krk
a	a	k8xC	a
oční	oční	k2eAgNnPc1d1	oční
víčka	víčko	k1gNnPc1	víčko
<g/>
.	.	kIx.	.
</s>
<s>
Postižení	postižený	k2eAgMnPc1d1	postižený
ptáci	pták	k1gMnPc1	pták
zpočátku	zpočátku	k6eAd1	zpočátku
posedávají	posedávat	k5eAaImIp3nP	posedávat
<g/>
,	,	kIx,	,
oči	oko	k1gNnPc1	oko
mají	mít	k5eAaImIp3nP	mít
zavřené	zavřený	k2eAgFnPc1d1	zavřená
<g/>
,	,	kIx,	,
peří	peří	k1gNnSc1	peří
je	být	k5eAaImIp3nS	být
naježené	naježený	k2eAgNnSc1d1	naježené
a	a	k8xC	a
po	po	k7c6	po
dotyku	dotyk	k1gInSc6	dotyk
vypadává	vypadávat	k5eAaImIp3nS	vypadávat
<g/>
.	.	kIx.	.
</s>
<s>
Neochotně	ochotně	k6eNd1	ochotně
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
a	a	k8xC	a
po	po	k7c6	po
přinucení	přinucení	k1gNnSc6	přinucení
k	k	k7c3	k
pohybu	pohyb	k1gInSc3	pohyb
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
nekoordinované	koordinovaný	k2eNgInPc1d1	nekoordinovaný
pohyby	pohyb	k1gInPc1	pohyb
nebo	nebo	k8xC	nebo
nejsou	být	k5eNaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
žádného	žádný	k3yNgInSc2	žádný
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Bližším	blízký	k2eAgNnSc7d2	bližší
vyšetřením	vyšetření	k1gNnSc7	vyšetření
lze	lze	k6eAd1	lze
zjistit	zjistit	k5eAaPmF	zjistit
namáhavé	namáhavý	k2eAgNnSc4d1	namáhavé
dýchání	dýchání	k1gNnSc4	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
úhynům	úhyn	k1gInPc3	úhyn
dochází	docházet	k5eAaImIp3nS	docházet
selháním	selhání	k1gNnSc7	selhání
srdečních	srdeční	k2eAgFnPc2d1	srdeční
a	a	k8xC	a
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Ojediněle	ojediněle	k6eAd1	ojediněle
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
uzdravení	uzdravení	k1gNnSc3	uzdravení
<g/>
,	,	kIx,	,
po	po	k7c6	po
toxinu	toxin	k1gInSc6	toxin
A	a	k9	a
častěji	často	k6eAd2	často
než	než	k8xS	než
po	po	k7c6	po
botulotoxinu	botulotoxin	k1gInSc6	botulotoxin
typu	typ	k1gInSc2	typ
C.	C.	kA	C.
Vodní	vodní	k2eAgFnSc4d1	vodní
drůbež	drůbež	k1gFnSc4	drůbež
může	moct	k5eAaImIp3nS	moct
uhynout	uhynout	k5eAaPmF	uhynout
utopením	utopení	k1gNnSc7	utopení
<g/>
.	.	kIx.	.
</s>
<s>
Morbidita	morbidita	k1gFnSc1	morbidita
i	i	k8xC	i
mortalita	mortalita	k1gFnSc1	mortalita
zavisejí	zaviset	k5eAaPmIp3nP	zaviset
na	na	k7c4	na
množství	množství	k1gNnSc4	množství
přijatého	přijatý	k2eAgInSc2d1	přijatý
toxinu	toxin	k1gInSc2	toxin
(	(	kIx(	(
<g/>
kumulace	kumulace	k1gFnSc1	kumulace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vodních	vodní	k2eAgMnPc2d1	vodní
divoce	divoce	k6eAd1	divoce
žijících	žijící	k2eAgMnPc2d1	žijící
ptáků	pták	k1gMnPc2	pták
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
masové	masový	k2eAgInPc1d1	masový
úhyny	úhyn	k1gInPc1	úhyn
<g/>
.	.	kIx.	.
</s>
<s>
Pitevní	pitevní	k2eAgInSc1d1	pitevní
nález	nález	k1gInSc1	nález
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
negativní	negativní	k2eAgFnSc7d1	negativní
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
toxická	toxický	k2eAgFnSc1d1	toxická
dávka	dávka	k1gFnSc1	dávka
neurotoxinu	neurotoxin	k1gInSc2	neurotoxin
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
imunogenní	imunogenní	k2eAgFnSc1d1	imunogenní
<g/>
,	,	kIx,	,
přežívající	přežívající	k2eAgFnSc1d1	přežívající
drůbež	drůbež	k1gFnSc1	drůbež
netvoří	tvořit	k5eNaImIp3nS	tvořit
imunitu	imunita	k1gFnSc4	imunita
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mrchožravých	mrchožravý	k2eAgMnPc2d1	mrchožravý
ptáků	pták	k1gMnPc2	pták
se	se	k3xPyFc4	se
však	však	k9	však
zjišťují	zjišťovat	k5eAaImIp3nP	zjišťovat
humorální	humorální	k2eAgFnPc4d1	humorální
protilátky	protilátka	k1gFnPc4	protilátka
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
je	být	k5eAaImIp3nS	být
vysvětlována	vysvětlován	k2eAgFnSc1d1	vysvětlována
jejich	jejich	k3xOp3gFnSc1	jejich
rezistence	rezistence	k1gFnSc1	rezistence
k	k	k7c3	k
botulotoxinu	botulotoxin	k1gInSc3	botulotoxin
po	po	k7c4	po
experimentální	experimentální	k2eAgFnSc4d1	experimentální
infekci	infekce	k1gFnSc4	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Předběžná	předběžný	k2eAgFnSc1d1	předběžná
diagnóza	diagnóza	k1gFnSc1	diagnóza
botulismu	botulismus	k1gInSc2	botulismus
je	být	k5eAaImIp3nS	být
založena	založen	k2eAgFnSc1d1	založena
na	na	k7c6	na
klinických	klinický	k2eAgInPc6d1	klinický
příznacích	příznak	k1gInPc6	příznak
a	a	k8xC	a
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
makro-	makro-	k?	makro-
i	i	k8xC	i
mikroskopických	mikroskopický	k2eAgFnPc2d1	mikroskopická
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Konečná	konečný	k2eAgFnSc1d1	konečná
diagnóza	diagnóza	k1gFnSc1	diagnóza
ale	ale	k8xC	ale
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
průkaz	průkaz	k1gInSc4	průkaz
botulotoxinu	botulotoxin	k1gInSc2	botulotoxin
v	v	k7c6	v
séru	sérum	k1gNnSc6	sérum
<g/>
,	,	kIx,	,
voleti	vole	k1gNnSc6	vole
nebo	nebo	k8xC	nebo
výplachu	výplach	k1gInSc6	výplach
trávicího	trávicí	k2eAgInSc2d1	trávicí
traktu	trakt	k1gInSc2	trakt
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
biologickým	biologický	k2eAgInSc7d1	biologický
pokusem	pokus	k1gInSc7	pokus
na	na	k7c6	na
myších	myš	k1gFnPc6	myš
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
předem	předem	k6eAd1	předem
imunizovány	imunizován	k2eAgFnPc1d1	imunizována
antitoxickými	antitoxický	k2eAgNnPc7d1	antitoxický
séry	sérum	k1gNnPc7	sérum
(	(	kIx(	(
<g/>
A-F	A-F	k1gMnPc7	A-F
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Izolace	izolace	k1gFnSc1	izolace
bakterií	bakterie	k1gFnPc2	bakterie
C.	C.	kA	C.
botulinum	botulinum	k1gInSc1	botulinum
má	mít	k5eAaImIp3nS	mít
minimální	minimální	k2eAgInSc4d1	minimální
diagnostický	diagnostický	k2eAgInSc4d1	diagnostický
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
i	i	k9	i
u	u	k7c2	u
zdravých	zdravý	k2eAgMnPc2d1	zdravý
ptáků	pták	k1gMnPc2	pták
ve	v	k7c6	v
střevech	střevo	k1gNnPc6	střevo
<g/>
,	,	kIx,	,
játrech	játra	k1gNnPc6	játra
a	a	k8xC	a
slezině	slezina	k1gFnSc6	slezina
<g/>
.	.	kIx.	.
</s>
<s>
Detekce	detekce	k1gFnSc1	detekce
C.	C.	kA	C.
botulinum	botulinum	k1gNnSc1	botulinum
v	v	k7c6	v
krmivu	krmivo	k1gNnSc6	krmivo
nebo	nebo	k8xC	nebo
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
např.	např.	kA	např.
v	v	k7c6	v
epizootologických	epizootologický	k2eAgNnPc6d1	epizootologické
studiích	studio	k1gNnPc6	studio
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetřovaný	vyšetřovaný	k2eAgInSc1d1	vyšetřovaný
materiál	materiál	k1gInSc1	materiál
se	se	k3xPyFc4	se
kultivuje	kultivovat	k5eAaImIp3nS	kultivovat
na	na	k7c6	na
anaerobních	anaerobní	k2eAgInPc6d1	anaerobní
krevních	krevní	k2eAgInPc6d1	krevní
agarech	agar	k1gInPc6	agar
<g/>
,	,	kIx,	,
biochemická	biochemický	k2eAgFnSc1d1	biochemická
identifikace	identifikace	k1gFnSc1	identifikace
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
komerčními	komerční	k2eAgInPc7d1	komerční
mikrotesty	mikrotest	k1gInPc7	mikrotest
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ANAEROtest	ANAEROtest	k1gInSc1	ANAEROtest
<g/>
,	,	kIx,	,
Lachema	lachema	k1gFnSc1	lachema
<g/>
)	)	kIx)	)
a	a	k8xC	a
toxiny	toxin	k1gInPc1	toxin
se	se	k3xPyFc4	se
určují	určovat	k5eAaImIp3nP	určovat
neutralizačním	neutralizační	k2eAgInSc7d1	neutralizační
testem	test	k1gInSc7	test
<g/>
.	.	kIx.	.
</s>
<s>
Diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
diagnostika	diagnostika	k1gFnSc1	diagnostika
<g/>
.	.	kIx.	.
</s>
<s>
Počáteční	počáteční	k2eAgInPc1d1	počáteční
klinické	klinický	k2eAgInPc1d1	klinický
příznaky	příznak	k1gInPc1	příznak
botulismu	botulismus	k1gInSc2	botulismus
mohou	moct	k5eAaImIp3nP	moct
připomínat	připomínat	k5eAaImF	připomínat
jiná	jiný	k2eAgNnPc4d1	jiné
onemocnění	onemocnění	k1gNnPc4	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
diagnostickými	diagnostický	k2eAgInPc7d1	diagnostický
znaky	znak	k1gInPc7	znak
jsou	být	k5eAaImIp3nP	být
absence	absence	k1gFnSc2	absence
patologických	patologický	k2eAgFnPc2d1	patologická
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
povaha	povaha	k1gFnSc1	povaha
a	a	k8xC	a
trvání	trvání	k1gNnSc1	trvání
paralýzy	paralýza	k1gFnSc2	paralýza
a	a	k8xC	a
průkaz	průkaz	k1gInSc4	průkaz
toxinu	toxin	k1gInSc2	toxin
<g/>
.	.	kIx.	.
</s>
<s>
Paralýza	paralýza	k1gFnSc1	paralýza
končetin	končetina	k1gFnPc2	končetina
a	a	k8xC	a
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
při	při	k7c6	při
Markově	Markův	k2eAgFnSc6d1	Markova
nemoci	nemoc	k1gFnSc6	nemoc
<g/>
,	,	kIx,	,
Newcastleské	Newcastleský	k2eAgFnSc3d1	Newcastleská
nemoci	nemoc	k1gFnSc3	nemoc
nebo	nebo	k8xC	nebo
aviární	aviární	k2eAgFnSc3d1	aviární
encefalomyelitidě	encefalomyelitida	k1gFnSc3	encefalomyelitida
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
odlišena	odlišit	k5eAaPmNgFnS	odlišit
izolací	izolace	k1gFnSc7	izolace
příčinného	příčinný	k2eAgInSc2d1	příčinný
viru	vir	k1gInSc2	vir
a	a	k8xC	a
nálezem	nález	k1gInSc7	nález
makro-	makro-	k?	makro-
anebo	anebo	k8xC	anebo
mikroskopických	mikroskopický	k2eAgFnPc2d1	mikroskopická
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Nutriční	nutriční	k2eAgInPc4d1	nutriční
deficience	deficienec	k1gInPc4	deficienec
nebo	nebo	k8xC	nebo
nežádoucí	žádoucí	k2eNgInPc4d1	nežádoucí
účinky	účinek	k1gInPc4	účinek
antikokcidik	antikokcidikum	k1gNnPc2	antikokcidikum
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zjištěny	zjistit	k5eAaPmNgInP	zjistit
vyšetřením	vyšetření	k1gNnSc7	vyšetření
krmiva	krmivo	k1gNnSc2	krmivo
<g/>
.	.	kIx.	.
</s>
<s>
Léčba	léčba	k1gFnSc1	léčba
botulismu	botulismus	k1gInSc2	botulismus
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
při	při	k7c6	při
individuálním	individuální	k2eAgNnSc6d1	individuální
ošetření	ošetření	k1gNnSc6	ošetření
<g/>
,	,	kIx,	,
u	u	k7c2	u
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
divokých	divoký	k2eAgMnPc2d1	divoký
ptáků	pták	k1gMnPc2	pták
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
velkochovech	velkochov	k1gInPc6	velkochov
drůbeže	drůbež	k1gFnSc2	drůbež
je	být	k5eAaImIp3nS	být
obtížná	obtížný	k2eAgFnSc1d1	obtížná
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
neprovádí	provádět	k5eNaImIp3nS	provádět
<g/>
.	.	kIx.	.
</s>
<s>
Prognóza	prognóza	k1gFnSc1	prognóza
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
nepříznivá	příznivý	k2eNgFnSc1d1	nepříznivá
<g/>
,	,	kIx,	,
výjimka	výjimka	k1gFnSc1	výjimka
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
jen	jen	k9	jen
u	u	k7c2	u
mírně	mírně	k6eAd1	mírně
postižených	postižený	k2eAgMnPc2d1	postižený
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
které	který	k3yRgMnPc4	který
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
izolovat	izolovat	k5eAaBmF	izolovat
a	a	k8xC	a
jednotlivě	jednotlivě	k6eAd1	jednotlivě
napájet	napájet	k5eAaImF	napájet
i	i	k8xC	i
krmit	krmit	k5eAaImF	krmit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pštrosů	pštros	k1gMnPc2	pštros
nebo	nebo	k8xC	nebo
ptáků	pták	k1gMnPc2	pták
v	v	k7c6	v
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
se	se	k3xPyFc4	se
osvědčil	osvědčit	k5eAaPmAgInS	osvědčit
specifický	specifický	k2eAgInSc1d1	specifický
antitoxin	antitoxin	k1gInSc1	antitoxin
typu	typ	k1gInSc2	typ
C	C	kA	C
<g/>
;	;	kIx,	;
komerčně	komerčně	k6eAd1	komerčně
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
antitoxin	antitoxin	k1gInSc1	antitoxin
C	C	kA	C
pro	pro	k7c4	pro
kožešinová	kožešinový	k2eAgNnPc4d1	kožešinové
zvířata	zvíře	k1gNnPc4	zvíře
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
podávat	podávat	k5eAaImF	podávat
ptákům	pták	k1gMnPc3	pták
v	v	k7c6	v
poloviční	poloviční	k2eAgFnSc6d1	poloviční
dávce	dávka	k1gFnSc6	dávka
<g/>
.	.	kIx.	.
</s>
<s>
Prevence	prevence	k1gFnSc1	prevence
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
chovu	chov	k1gInSc6	chov
vodní	vodní	k2eAgFnSc2d1	vodní
drůbeže	drůbež	k1gFnSc2	drůbež
na	na	k7c6	na
lokalitách	lokalita	k1gFnPc6	lokalita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
onemocnění	onemocnění	k1gNnSc1	onemocnění
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
častém	častý	k2eAgInSc6d1	častý
sběru	sběr	k1gInSc6	sběr
a	a	k8xC	a
neškodném	škodný	k2eNgNnSc6d1	neškodné
odstraňování	odstraňování	k1gNnSc6	odstraňování
kadáverů	kadáver	k1gInPc2	kadáver
<g/>
,	,	kIx,	,
zajištění	zajištění	k1gNnSc1	zajištění
přístupu	přístup	k1gInSc2	přístup
vodní	vodní	k2eAgFnSc2d1	vodní
drůbeže	drůbež	k1gFnSc2	drůbež
na	na	k7c4	na
tekoucí	tekoucí	k2eAgFnSc4d1	tekoucí
nebo	nebo	k8xC	nebo
hlubší	hluboký	k2eAgFnSc2d2	hlubší
stojaté	stojatý	k2eAgFnSc2d1	stojatá
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
provzdušňování	provzdušňování	k1gNnSc1	provzdušňování
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
snížení	snížení	k1gNnSc4	snížení
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
kontaminace	kontaminace	k1gFnSc2	kontaminace
prostředí	prostředí	k1gNnSc2	prostředí
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
provádět	provádět	k5eAaImF	provádět
častější	častý	k2eAgFnSc4d2	častější
výměnu	výměna	k1gFnSc4	výměna
podestýlky	podestýlka	k1gFnSc2	podestýlka
a	a	k8xC	a
pečlivou	pečlivý	k2eAgFnSc4d1	pečlivá
asanaci	asanace	k1gFnSc4	asanace
hal	hala	k1gFnPc2	hala
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
bažantů	bažant	k1gMnPc2	bažant
byla	být	k5eAaImAgFnS	být
popsána	popsat	k5eAaPmNgFnS	popsat
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
imunizace	imunizace	k1gFnSc2	imunizace
inaktivovaným	inaktivovaný	k2eAgInSc7d1	inaktivovaný
toxoidem	toxoid	k1gInSc7	toxoid
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
v	v	k7c6	v
experimentálních	experimentální	k2eAgFnPc6d1	experimentální
podmínkách	podmínka	k1gFnPc6	podmínka
u	u	k7c2	u
kuřata	kuře	k1gNnPc1	kuře
a	a	k8xC	a
kachen	kachna	k1gFnPc2	kachna
<g/>
.	.	kIx.	.
</s>
<s>
Vakcinace	vakcinace	k1gFnSc1	vakcinace
velkého	velký	k2eAgInSc2d1	velký
počtu	počet	k1gInSc2	počet
drůbeže	drůbež	k1gFnSc2	drůbež
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
nákladná	nákladný	k2eAgNnPc4d1	nákladné
a	a	k8xC	a
nepraktická	praktický	k2eNgNnPc4d1	nepraktické
<g/>
.	.	kIx.	.
</s>
