<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Budweis	Budweis	k1gFnSc1	Budweis
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
Böhmisch	Böhmisch	k1gMnSc1	Böhmisch
Budweis	Budweis	k1gFnSc2	Budweis
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
statutární	statutární	k2eAgNnSc1d1	statutární
město	město	k1gNnSc1	město
a	a	k8xC	a
správní	správní	k2eAgFnSc1d1	správní
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc1d1	kulturní
metropole	metropole	k1gFnSc1	metropole
Jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
