<p>
<s>
Herbert	Herbert	k1gMnSc1	Herbert
Pavera	Pavero	k1gNnSc2	Pavero
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1958	[number]	k4	1958
Bolatice	Bolatice	k1gFnSc1	Bolatice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
starosta	starosta	k1gMnSc1	starosta
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
senátor	senátor	k1gMnSc1	senátor
za	za	k7c4	za
obvod	obvod	k1gInSc4	obvod
č.	č.	k?	č.
68	[number]	k4	68
-	-	kIx~	-
Opava	Opava	k1gFnSc1	Opava
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2013	[number]	k4	2013
až	až	k9	až
2017	[number]	k4	2017
poslanec	poslanec	k1gMnSc1	poslanec
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
starosta	starosta	k1gMnSc1	starosta
obce	obec	k1gFnSc2	obec
Bolatice	Bolatice	k1gFnSc2	Bolatice
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
gymnázia	gymnázium	k1gNnSc2	gymnázium
v	v	k7c6	v
Hlučíně	Hlučín	k1gInSc6	Hlučín
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
učitelství	učitelství	k1gNnSc4	učitelství
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
chemie	chemie	k1gFnSc2	chemie
pro	pro	k7c4	pro
základní	základní	k2eAgFnPc4d1	základní
i	i	k8xC	i
střední	střední	k2eAgFnPc4d1	střední
školy	škola	k1gFnPc4	škola
na	na	k7c6	na
Pedagogické	pedagogický	k2eAgFnSc6d1	pedagogická
fakultě	fakulta	k1gFnSc6	fakulta
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
(	(	kIx(	(
<g/>
získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc1	titul
Mgr.	Mgr.	kA	Mgr.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Učil	učít	k5eAaPmAgMnS	učít
na	na	k7c6	na
základní	základní	k2eAgFnSc6d1	základní
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
-	-	kIx~	-
Kylešovicích	Kylešovice	k1gFnPc6	Kylešovice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
působil	působit	k5eAaImAgMnS	působit
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
jako	jako	k8xC	jako
zástupce	zástupce	k1gMnSc1	zástupce
ředitele	ředitel	k1gMnSc2	ředitel
ZŠ	ZŠ	kA	ZŠ
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1992	[number]	k4	1992
až	až	k9	až
1998	[number]	k4	1998
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
škole	škola	k1gFnSc6	škola
ředitelem	ředitel	k1gMnSc7	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
Angažuje	angažovat	k5eAaBmIp3nS	angažovat
se	se	k3xPyFc4	se
také	také	k9	také
jako	jako	k8xS	jako
předseda	předseda	k1gMnSc1	předseda
Sdružení	sdružení	k1gNnSc2	sdružení
obcí	obec	k1gFnPc2	obec
Hlučínska	Hlučínsko	k1gNnSc2	Hlučínsko
<g/>
.	.	kIx.	.
<g/>
Jeho	jeho	k3xOp3gInPc1	jeho
koníčky	koníček	k1gInPc1	koníček
jsou	být	k5eAaImIp3nP	být
sport	sport	k1gInSc4	sport
<g/>
,	,	kIx,	,
pěstování	pěstování	k1gNnSc4	pěstování
bonsají	bonsaj	k1gFnPc2	bonsaj
a	a	k8xC	a
moderování	moderování	k1gNnPc2	moderování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Herbert	Herbert	k1gInSc1	Herbert
Pavera	Pavero	k1gNnSc2	Pavero
je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politické	politický	k2eAgNnSc1d1	politické
působení	působení	k1gNnSc1	působení
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byl	být	k5eAaImAgMnS	být
poprvé	poprvé	k6eAd1	poprvé
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
Zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
obce	obec	k1gFnSc2	obec
Bolatice	Bolatice	k1gFnSc2	Bolatice
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Opava	Opava	k1gFnSc1	Opava
<g/>
.	.	kIx.	.
</s>
<s>
Mandát	mandát	k1gInSc1	mandát
zastupitele	zastupitel	k1gMnSc2	zastupitel
obhájil	obhájit	k5eAaPmAgInS	obhájit
i	i	k9	i
v	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
jako	jako	k8xS	jako
nestraník	nestraník	k1gMnSc1	nestraník
za	za	k7c7	za
"	"	kIx"	"
<g/>
Sdružení	sdružení	k1gNnSc1	sdružení
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1990	[number]	k4	1990
až	až	k9	až
1998	[number]	k4	1998
zároveň	zároveň	k6eAd1	zároveň
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xS	jako
místostarosta	místostarosta	k1gMnSc1	místostarosta
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
úspěšně	úspěšně	k6eAd1	úspěšně
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
jako	jako	k9	jako
nestraník	nestraník	k1gMnSc1	nestraník
opět	opět	k6eAd1	opět
za	za	k7c7	za
"	"	kIx"	"
<g/>
Sdružení	sdružení	k1gNnSc1	sdružení
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
starostou	starosta	k1gMnSc7	starosta
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
zastupitele	zastupitel	k1gMnSc2	zastupitel
i	i	k8xC	i
starosty	starosta	k1gMnSc2	starosta
pak	pak	k6eAd1	pak
obhájil	obhájit	k5eAaPmAgMnS	obhájit
ještě	ještě	k9	ještě
v	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
(	(	kIx(	(
<g/>
nestraník	nestraník	k1gMnSc1	nestraník
za	za	k7c4	za
SNK-ED	SNK-ED	k1gFnSc4	SNK-ED
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
(	(	kIx(	(
<g/>
nestraník	nestraník	k1gMnSc1	nestraník
za	za	k7c4	za
SNK-ED	SNK-ED	k1gFnSc4	SNK-ED
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
nestraník	nestraník	k1gMnSc1	nestraník
za	za	k7c4	za
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
obhájil	obhájit	k5eAaPmAgMnS	obhájit
post	post	k1gInSc4	post
zastupitele	zastupitel	k1gMnSc2	zastupitel
obce	obec	k1gFnSc2	obec
Bolatice	Bolatice	k1gFnSc2	Bolatice
<g/>
,	,	kIx,	,
když	když	k8xS	když
vedl	vést	k5eAaImAgMnS	vést
jako	jako	k9	jako
nestraník	nestraník	k1gMnSc1	nestraník
kandidátku	kandidátka	k1gFnSc4	kandidátka
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2014	[number]	k4	2014
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
zvolen	zvolit	k5eAaPmNgMnS	zvolit
starostou	starosta	k1gMnSc7	starosta
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
vedl	vést	k5eAaImAgMnS	vést
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
člena	člen	k1gMnSc2	člen
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
kandidátku	kandidátka	k1gFnSc4	kandidátka
subjektu	subjekt	k1gInSc2	subjekt
"	"	kIx"	"
<g/>
Společně	společně	k6eAd1	společně
pro	pro	k7c4	pro
Bolatice	Bolatice	k1gFnPc4	Bolatice
a	a	k8xC	a
Borovou	borový	k2eAgFnSc4d1	Borová
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
nezávislí	závislý	k2eNgMnPc1d1	nezávislý
kandidáti	kandidát	k1gMnPc1	kandidát
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
post	post	k1gInSc1	post
zastupitele	zastupitel	k1gMnSc2	zastupitel
obce	obec	k1gFnSc2	obec
obhájil	obhájit	k5eAaPmAgMnS	obhájit
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
opět	opět	k6eAd1	opět
starostou	starosta	k1gMnSc7	starosta
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
<g/>
Kandidatury	kandidatura	k1gFnSc2	kandidatura
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2002	[number]	k4	2002
-	-	kIx~	-
2013	[number]	k4	2013
nebyly	být	k5eNaImAgInP	být
úspěšné	úspěšný	k2eAgInPc1d1	úspěšný
-	-	kIx~	-
nejdříve	dříve	k6eAd3	dříve
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
(	(	kIx(	(
<g/>
nestraník	nestraník	k1gMnSc1	nestraník
za	za	k7c4	za
Sdružení	sdružení	k1gNnSc4	sdružení
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
)	)	kIx)	)
a	a	k8xC	a
pak	pak	k6eAd1	pak
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
(	(	kIx(	(
<g/>
nestraník	nestraník	k1gMnSc1	nestraník
za	za	k7c4	za
SNK-ED	SNK-ED	k1gFnSc4	SNK-ED
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
až	až	k9	až
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
ze	z	k7c2	z
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
místa	místo	k1gNnSc2	místo
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
jako	jako	k8xC	jako
nestraník	nestraník	k1gMnSc1	nestraník
za	za	k7c4	za
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
díky	díky	k7c3	díky
2	[number]	k4	2
719	[number]	k4	719
preferenčních	preferenční	k2eAgInPc2d1	preferenční
hlasů	hlas	k1gInPc2	hlas
zvolen	zvolen	k2eAgMnSc1d1	zvolen
a	a	k8xC	a
předskočil	předskočit	k5eAaPmAgMnS	předskočit
tak	tak	k9	tak
lídra	lídr	k1gMnSc4	lídr
kandidátky	kandidátka	k1gFnSc2	kandidátka
Pavola	Pavola	k1gFnSc1	Pavola
Lukšu	Lukšu	k1gMnSc1	Lukšu
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
Sněmovny	sněmovna	k1gFnSc2	sněmovna
nedostal	dostat	k5eNaPmAgInS	dostat
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2015	[number]	k4	2015
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
celostátním	celostátní	k2eAgInSc6d1	celostátní
sněmu	sněm	k1gInSc6	sněm
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
zvolen	zvolit	k5eAaPmNgInS	zvolit
členem	člen	k1gInSc7	člen
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
od	od	k7c2	od
přítomných	přítomný	k2eAgMnPc2d1	přítomný
delegátů	delegát	k1gMnPc2	delegát
získal	získat	k5eAaPmAgInS	získat
102	[number]	k4	102
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
člena	člen	k1gMnSc2	člen
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
strany	strana	k1gFnSc2	strana
zastával	zastávat	k5eAaImAgMnS	zastávat
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
zastává	zastávat	k5eAaImIp3nS	zastávat
funkci	funkce	k1gFnSc4	funkce
člena	člen	k1gMnSc2	člen
výkonného	výkonný	k2eAgInSc2d1	výkonný
výboru	výbor	k1gInSc2	výbor
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
byl	být	k5eAaImAgInS	být
lídrem	lídr	k1gMnSc7	lídr
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neuspěl	uspět	k5eNaPmAgMnS	uspět
a	a	k8xC	a
mandát	mandát	k1gInSc4	mandát
poslance	poslanec	k1gMnSc2	poslanec
tak	tak	k6eAd1	tak
neobhájil	obhájit	k5eNaPmAgMnS	obhájit
<g/>
.	.	kIx.	.
<g/>
Čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
do	do	k7c2	do
Zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
Moravskoslezského	moravskoslezský	k2eAgInSc2d1	moravskoslezský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
v	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
jako	jako	k8xC	jako
nestraník	nestraník	k1gMnSc1	nestraník
za	za	k7c4	za
Sdružení	sdružení	k1gNnSc4	sdružení
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
v	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
opět	opět	k6eAd1	opět
jako	jako	k8xS	jako
nestraník	nestraník	k1gMnSc1	nestraník
za	za	k7c4	za
Sdružení	sdružení	k1gNnSc4	sdružení
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
v	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
tentokrát	tentokrát	k6eAd1	tentokrát
jako	jako	k8xC	jako
nestraník	nestraník	k1gMnSc1	nestraník
za	za	k7c4	za
KDU-ČSL	KDU-ČSL	k1gFnSc4	KDU-ČSL
a	a	k8xC	a
v	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
nakonec	nakonec	k6eAd1	nakonec
jako	jako	k8xS	jako
nestraník	nestraník	k1gMnSc1	nestraník
za	za	k7c4	za
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
krajských	krajský	k2eAgFnPc6d1	krajská
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
byl	být	k5eAaImAgMnS	být
lídrem	lídr	k1gMnSc7	lídr
kandidátky	kandidátka	k1gFnSc2	kandidátka
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neuspěl	uspět	k5eNaPmAgMnS	uspět
a	a	k8xC	a
do	do	k7c2	do
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
se	se	k3xPyFc4	se
nedostal	dostat	k5eNaPmAgInS	dostat
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
PČR	PČR	kA	PČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
za	za	k7c4	za
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
v	v	k7c6	v
obvodu	obvod	k1gInSc6	obvod
č.	č.	k?	č.
68	[number]	k4	68
–	–	k?	–
Opava	Opava	k1gFnSc1	Opava
<g/>
.	.	kIx.	.
</s>
<s>
Podpořily	podpořit	k5eAaPmAgInP	podpořit
jej	on	k3xPp3gNnSc4	on
také	také	k9	také
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
hnutí	hnutí	k1gNnSc1	hnutí
STAN	stan	k1gInSc1	stan
<g/>
,	,	kIx,	,
Zelení	zelení	k1gNnSc1	zelení
a	a	k8xC	a
Strana	strana	k1gFnSc1	strana
soukromníků	soukromník	k1gMnPc2	soukromník
ČR	ČR	kA	ČR
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Společně	společně	k6eAd1	společně
pro	pro	k7c4	pro
Opavsko	Opavsko	k1gNnSc4	Opavsko
<g/>
:	:	kIx,	:
Koalice	koalice	k1gFnSc1	koalice
Starostů	Starosta	k1gMnPc2	Starosta
a	a	k8xC	a
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
<g/>
,	,	kIx,	,
ODS	ODS	kA	ODS
a	a	k8xC	a
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Strany	strana	k1gFnSc2	strana
zelených	zelená	k1gFnPc2	zelená
a	a	k8xC	a
Soukromníků	soukromník	k1gMnPc2	soukromník
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
36,27	[number]	k4	36,27
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
první	první	k4xOgNnSc4	první
kolo	kolo	k1gNnSc4	kolo
voleb	volba	k1gFnPc2	volba
a	a	k8xC	a
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
se	se	k3xPyFc4	se
utkal	utkat	k5eAaPmAgMnS	utkat
s	s	k7c7	s
nestraničkou	nestranička	k1gFnSc7	nestranička
za	za	k7c2	za
hnutí	hnutí	k1gNnSc2	hnutí
ANO	ano	k9	ano
2011	[number]	k4	2011
Simonou	Simona	k1gFnSc7	Simona
Horákovou	Horákův	k2eAgFnSc7d1	Horákova
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
porazil	porazit	k5eAaPmAgInS	porazit
poměrem	poměr	k1gInSc7	poměr
hlasů	hlas	k1gInPc2	hlas
68,50	[number]	k4	68,50
%	%	kIx~	%
:	:	kIx,	:
31,49	[number]	k4	31,49
%	%	kIx~	%
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
senátorem	senátor	k1gMnSc7	senátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Osobní	osobní	k2eAgFnSc1d1	osobní
stránka	stránka	k1gFnSc1	stránka
Herberta	Herbert	k1gMnSc2	Herbert
Pavery	Pavera	k1gFnSc2	Pavera
</s>
</p>
<p>
<s>
Herbert	Herbert	k1gMnSc1	Herbert
Pavera	Pavero	k1gNnSc2	Pavero
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
</s>
</p>
<p>
<s>
Herbert	Herbert	k1gMnSc1	Herbert
Pavera	Pavero	k1gNnSc2	Pavero
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
</s>
</p>
