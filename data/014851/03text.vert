<s>
Bedřich	Bedřich	k1gMnSc1
Steiner	Steiner	k1gMnSc1
</s>
<s>
Bedřich	Bedřich	k1gMnSc1
Steiner	Steiner	k1gMnSc1
</s>
<s>
Poslanec	poslanec	k1gMnSc1
Ústavodárného	ústavodárný	k2eAgInSc2d1
NS	NS	kA
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1946	#num#	k4
–	–	k?
1948	#num#	k4
</s>
<s>
Poslanec	poslanec	k1gMnSc1
Národního	národní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
ČSR	ČSR	kA
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1948	#num#	k4
–	–	k?
1954	#num#	k4
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
KSČ	KSČ	kA
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1913	#num#	k4
ZbraslaviceRakousko-Uhersko	ZbraslaviceRakousko-Uhersko	k1gNnSc4
Rakousko-Uhersko	Rakousko-Uherska	k1gMnSc5
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1983	#num#	k4
Národnost	národnost	k1gFnSc1
</s>
<s>
Češi	Čech	k1gMnPc1
Profese	profes	k1gFnSc2
</s>
<s>
politik	politik	k1gMnSc1
a	a	k8xC
advokát	advokát	k1gMnSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Sokolovská	sokolovský	k2eAgFnSc1d1
pamětní	pamětní	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Bedřich	Bedřich	k1gMnSc1
Steiner	Steiner	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1913	#num#	k4
Zbraslavice	Zbraslavice	k1gFnSc2
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1983	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
a	a	k8xC
československý	československý	k2eAgMnSc1d1
politik	politik	k1gMnSc1
Komunistické	komunistický	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Československa	Československo	k1gNnSc2
a	a	k8xC
poválečný	poválečný	k2eAgMnSc1d1
poslanec	poslanec	k1gMnSc1
Ústavodárného	ústavodárný	k2eAgNnSc2d1
Národního	národní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
a	a	k8xC
Národního	národní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
ČSR	ČSR	kA
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Byl	být	k5eAaImAgMnS
právníkem	právník	k1gMnSc7
židovského	židovský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
působil	působit	k5eAaImAgMnS
v	v	k7c6
zahraničním	zahraniční	k2eAgInSc6d1
odboji	odboj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bojoval	bojovat	k5eAaImAgMnS
v	v	k7c6
československé	československý	k2eAgFnSc6d1
vojenské	vojenský	k2eAgFnSc6d1
jednotce	jednotka	k1gFnSc6
v	v	k7c6
SSSR	SSSR	kA
u	u	k7c2
Sokolova	Sokolov	k1gInSc2
a	a	k8xC
zúčastnil	zúčastnit	k5eAaPmAgMnS
se	se	k3xPyFc4
Slovenského	slovenský	k2eAgNnSc2d1
národního	národní	k2eAgNnSc2d1
povstání	povstání	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
bojích	boj	k1gInPc6
byl	být	k5eAaImAgMnS
těžce	těžce	k6eAd1
raněn	ranit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
mu	on	k3xPp3gMnSc3
udělena	udělit	k5eAaPmNgFnS
Sokolovská	sokolovský	k2eAgFnSc1d1
pamětní	pamětní	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
a	a	k8xC
sovětská	sovětský	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
Za	za	k7c4
odvahu	odvaha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
čtyřnásobným	čtyřnásobný	k2eAgMnSc7d1
nositelem	nositel	k1gMnSc7
Československého	československý	k2eAgInSc2d1
válečného	válečný	k2eAgInSc2d1
kříže	kříž	k1gInSc2
1939	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
byl	být	k5eAaImAgInS
evidován	evidovat	k5eAaImNgInS
jako	jako	k8xC,k8xS
válečný	válečný	k2eAgMnSc1d1
invalida	invalida	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1949	#num#	k4
<g/>
–	–	k?
<g/>
1951	#num#	k4
působil	působit	k5eAaImAgMnS
jako	jako	k9
náměstek	náměstek	k1gMnSc1
ministra	ministr	k1gMnSc2
vnitřního	vnitřní	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
ředitelem	ředitel	k1gMnSc7
Krásné	krásný	k2eAgFnSc2d1
jizby	jizba	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
1946	#num#	k4
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
poslancem	poslanec	k1gMnSc7
Ústavodárného	ústavodárný	k2eAgNnSc2d1
Národního	národní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
za	za	k7c4
KSČ	KSČ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Setrval	setrvat	k5eAaPmAgMnS
zde	zde	k6eAd1
do	do	k7c2
konce	konec	k1gInSc2
funkčního	funkční	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
do	do	k7c2
voleb	volba	k1gFnPc2
do	do	k7c2
Národního	národní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
roku	rok	k1gInSc2
1948	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc6
byl	být	k5eAaImAgInS
zvolen	zvolen	k2eAgInSc1d1
do	do	k7c2
Národního	národní	k2eAgNnSc2d1
shromáždění	shromáždění	k1gNnSc2
za	za	k7c4
KSČ	KSČ	kA
ve	v	k7c6
volebním	volební	k2eAgInSc6d1
kraji	kraj	k1gInSc6
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
parlamentu	parlament	k1gInSc6
zasedal	zasedat	k5eAaImAgInS
až	až	k6eAd1
do	do	k7c2
konce	konec	k1gInSc2
funkčního	funkční	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
do	do	k7c2
voleb	volba	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1954	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Bedřich	Bedřich	k1gMnSc1
Steiner	Steiner	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Několik	několik	k4yIc4
tváří	tvář	k1gFnPc2
Výstava	výstava	k1gFnSc1
na	na	k7c4
galerii	galerie	k1gFnSc4
Židovské	židovský	k2eAgFnSc2d1
radnice	radnice	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
holocaust	holocaust	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
plukovník	plukovník	k1gMnSc1
JUDr.	JUDr.	kA
Steiner	Steiner	k1gMnSc1
Bedřich	Bedřich	k1gMnSc1
(	(	kIx(
<g/>
dle	dle	k7c2
Brož	brož	k1gFnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
<g/>
:	:	kIx,
HRDINOVÉ	Hrdinové	k2eAgMnSc1d1
OD	od	k7c2
SOKOLOVA	Sokolovo	k1gNnSc2
<g/>
,	,	kIx,
Avis	aviso	k1gNnPc2
2005	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
forum	forum	k1gNnSc1
<g/>
.	.	kIx.
<g/>
valka	valka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
jmenný	jmenný	k2eAgInSc4d1
rejstřík	rejstřík	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
jmenný	jmenný	k2eAgInSc4d1
rejstřík	rejstřík	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Bedřich	Bedřich	k1gMnSc1
Steiner	Steiner	k1gMnSc1
v	v	k7c6
parlamentu	parlament	k1gInSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kv	kv	k?
<g/>
2017940700	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
2147148632968830630008	#num#	k4
</s>
