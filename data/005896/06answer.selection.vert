<s>
Černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
byla	být	k5eAaImAgFnS	být
teoreticky	teoreticky	k6eAd1	teoreticky
předpovězena	předpovědit	k5eAaPmNgFnS	předpovědit
v	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
publikované	publikovaný	k2eAgFnSc2d1	publikovaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
Albertem	Albert	k1gMnSc7	Albert
Einsteinem	Einstein	k1gMnSc7	Einstein
<g/>
.	.	kIx.	.
</s>
