<s>
Černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
hmotný	hmotný	k2eAgInSc4d1	hmotný
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc1	jeho
gravitační	gravitační	k2eAgNnSc1d1	gravitační
pole	pole	k1gNnSc1	pole
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jisté	jistý	k2eAgFnSc6d1	jistá
oblasti	oblast	k1gFnSc6	oblast
časoprostoru	časoprostor	k1gInSc2	časoprostor
natolik	natolik	k6eAd1	natolik
silné	silný	k2eAgNnSc1d1	silné
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádný	žádný	k3yNgInSc4	žádný
objekt	objekt	k1gInSc4	objekt
včetně	včetně	k7c2	včetně
světla	světlo	k1gNnSc2	světlo
nemůže	moct	k5eNaImIp3nS	moct
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
byla	být	k5eAaImAgFnS	být
teoreticky	teoreticky	k6eAd1	teoreticky
předpovězena	předpovědit	k5eAaPmNgFnS	předpovědit
v	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
publikované	publikovaný	k2eAgFnSc2d1	publikovaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
Albertem	Albert	k1gMnSc7	Albert
Einsteinem	Einstein	k1gMnSc7	Einstein
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
ji	on	k3xPp3gFnSc4	on
není	být	k5eNaImIp3nS	být
možno	možno	k6eAd1	možno
pozorovat	pozorovat	k5eAaImF	pozorovat
přímo	přímo	k6eAd1	přímo
<g/>
,	,	kIx,	,
nemůžeme	moct	k5eNaImIp1nP	moct
stanovit	stanovit	k5eAaPmF	stanovit
korektně	korektně	k6eAd1	korektně
nic	nic	k3yNnSc4	nic
jako	jako	k9	jako
její	její	k3xOp3gNnSc4	její
datum	datum	k1gNnSc4	datum
objevu	objev	k1gInSc2	objev
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
můžeme	moct	k5eAaImIp1nP	moct
s	s	k7c7	s
určitostí	určitost	k1gFnSc7	určitost
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
prvním	první	k4xOgNnSc7	první
vážným	vážné	k1gNnSc7	vážné
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
prokázaným	prokázaný	k2eAgMnSc7d1	prokázaný
kandidátem	kandidát	k1gMnSc7	kandidát
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
hvězda	hvězda	k1gFnSc1	hvězda
v	v	k7c6	v
binárním	binární	k2eAgInSc6d1	binární
systému	systém	k1gInSc6	systém
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Labutě	labuť	k1gFnSc2	labuť
kryjící	kryjící	k2eAgFnSc2d1	kryjící
se	se	k3xPyFc4	se
s	s	k7c7	s
rentgenovým	rentgenový	k2eAgInSc7d1	rentgenový
zdrojem	zdroj	k1gInSc7	zdroj
Cygnus	Cygnus	k1gInSc1	Cygnus
X-	X-	k1gFnSc2	X-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
těleso	těleso	k1gNnSc4	těleso
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
příliš	příliš	k6eAd1	příliš
velkou	velký	k2eAgFnSc4d1	velká
hmotu	hmota	k1gFnSc4	hmota
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
být	být	k5eAaImF	být
neutronovou	neutronový	k2eAgFnSc7d1	neutronová
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
efekty	efekt	k1gInPc1	efekt
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
pozorováním	pozorování	k1gNnSc7	pozorování
<g/>
,	,	kIx,	,
především	především	k9	především
rentgenové	rentgenový	k2eAgNnSc4d1	rentgenové
záření	záření	k1gNnSc4	záření
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
perfektní	perfektní	k2eAgFnSc6d1	perfektní
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
teoretickou	teoretický	k2eAgFnSc7d1	teoretická
predikcí	predikce	k1gFnSc7	predikce
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
považujeme	považovat	k5eAaImIp1nP	považovat
za	za	k7c4	za
obecně	obecně	k6eAd1	obecně
prokázané	prokázaný	k2eAgInPc4d1	prokázaný
<g/>
,	,	kIx,	,
že	že	k8xS	že
černé	černý	k2eAgFnPc1d1	černá
díry	díra	k1gFnPc1	díra
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
centrech	centr	k1gInPc6	centr
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
aktivních	aktivní	k2eAgFnPc2d1	aktivní
galaktických	galaktický	k2eAgInPc6d1	galaktický
jádrech	jádro	k1gNnPc6	jádro
<g/>
,	,	kIx,	,
kvasarech	kvasar	k1gInPc6	kvasar
i	i	k8xC	i
v	v	k7c6	v
centrech	centrum	k1gNnPc6	centrum
některých	některý	k3yIgFnPc2	některý
kulových	kulový	k2eAgFnPc2d1	kulová
hvězdokup	hvězdokupa	k1gFnPc2	hvězdokupa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
obecné	obecný	k2eAgFnSc2d1	obecná
relativity	relativita	k1gFnSc2	relativita
nemůže	moct	k5eNaImIp3nS	moct
žádná	žádný	k3yNgFnSc1	žádný
hmota	hmota	k1gFnSc1	hmota
ani	ani	k8xC	ani
informace	informace	k1gFnSc1	informace
proudit	proudit	k5eAaImF	proudit
z	z	k7c2	z
nitra	nitro	k1gNnSc2	nitro
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
k	k	k7c3	k
vnějšímu	vnější	k2eAgMnSc3d1	vnější
pozorovateli	pozorovatel	k1gMnSc3	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
získat	získat	k5eAaPmF	získat
žádnou	žádný	k3yNgFnSc4	žádný
její	její	k3xOp3gFnSc1	její
část	část	k1gFnSc1	část
ani	ani	k8xC	ani
odražené	odražený	k2eAgNnSc1d1	odražené
světlo	světlo	k1gNnSc1	světlo
vyslané	vyslaný	k2eAgNnSc1d1	vyslané
z	z	k7c2	z
vnějšího	vnější	k2eAgInSc2d1	vnější
zdroje	zdroj	k1gInSc2	zdroj
či	či	k8xC	či
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
hmotě	hmota	k1gFnSc6	hmota
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
kvantově-mechanické	kvantověechanický	k2eAgInPc1d1	kvantově-mechanický
procesy	proces	k1gInPc1	proces
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
vyzařování	vyzařování	k1gNnSc4	vyzařování
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyzařování	vyzařování	k1gNnSc1	vyzařování
nezávisí	záviset	k5eNaImIp3nS	záviset
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
do	do	k7c2	do
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
spadlo	spadnout	k5eAaPmAgNnS	spadnout
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Představu	představa	k1gFnSc4	představa
tělesa	těleso	k1gNnSc2	těleso
tak	tak	k9	tak
hmotného	hmotný	k2eAgInSc2d1	hmotný
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
uniknout	uniknout	k5eAaPmF	uniknout
dokonce	dokonce	k9	dokonce
ani	ani	k8xC	ani
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
anglický	anglický	k2eAgMnSc1d1	anglický
geolog	geolog	k1gMnSc1	geolog
John	John	k1gMnSc1	John
Michell	Michell	k1gMnSc1	Michell
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1783	[number]	k4	1783
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
zaslané	zaslaný	k2eAgFnSc2d1	zaslaná
Královské	královský	k2eAgFnSc2d1	královská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
byla	být	k5eAaImAgFnS	být
Newtonovská	newtonovský	k2eAgFnSc1d1	newtonovská
teorie	teorie	k1gFnSc1	teorie
gravitace	gravitace	k1gFnSc1	gravitace
a	a	k8xC	a
pojem	pojem	k1gInSc1	pojem
únikové	únikový	k2eAgFnSc2d1	úniková
rychlosti	rychlost	k1gFnSc2	rychlost
dostatečně	dostatečně	k6eAd1	dostatečně
známá	známý	k2eAgFnSc1d1	známá
<g/>
.	.	kIx.	.
</s>
<s>
Michell	Michell	k1gMnSc1	Michell
vypočítal	vypočítat	k5eAaPmAgMnS	vypočítat
<g/>
,	,	kIx,	,
že	že	k8xS	že
těleso	těleso	k1gNnSc1	těleso
s	s	k7c7	s
poloměrem	poloměr	k1gInSc7	poloměr
500	[number]	k4	500
krát	krát	k6eAd1	krát
větším	veliký	k2eAgMnSc6d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
poloměr	poloměr	k1gInSc4	poloměr
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
a	a	k8xC	a
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
hustotou	hustota	k1gFnSc7	hustota
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
únikovou	únikový	k2eAgFnSc4d1	úniková
rychlost	rychlost	k1gFnSc4	rychlost
rovnou	rovnou	k6eAd1	rovnou
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
neviditelné	viditelný	k2eNgNnSc1d1	neviditelné
<g/>
.	.	kIx.	.
</s>
<s>
Parafráze	parafráze	k1gFnSc1	parafráze
jeho	jeho	k3xOp3gNnPc2	jeho
slov	slovo	k1gNnPc2	slovo
<g/>
:	:	kIx,	:
Kdyby	kdyby	k9	kdyby
koule	koule	k1gFnSc2	koule
stejné	stejný	k2eAgFnSc2d1	stejná
hustoty	hustota	k1gFnSc2	hustota
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
má	mít	k5eAaImIp3nS	mít
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
převýšila	převýšit	k5eAaPmAgFnS	převýšit
jeho	on	k3xPp3gInSc4	on
poloměr	poloměr	k1gInSc4	poloměr
pět	pět	k4xCc4	pět
set	sto	k4xCgNnPc2	sto
ku	k	k7c3	k
jedné	jeden	k4xCgFnSc6	jeden
<g/>
,	,	kIx,	,
potom	potom	k8xC	potom
by	by	kYmCp3nS	by
těleso	těleso	k1gNnSc4	těleso
padající	padající	k2eAgMnSc1d1	padající
ke	k	k7c3	k
sféře	sféra	k1gFnSc3	sféra
z	z	k7c2	z
nekonečné	konečný	k2eNgFnSc2d1	nekonečná
výšky	výška	k1gFnSc2	výška
získalo	získat	k5eAaPmAgNnS	získat
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
povrchu	povrch	k1gInSc6	povrch
rychlost	rychlost	k1gFnSc4	rychlost
větší	veliký	k2eAgFnSc2d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
<g />
.	.	kIx.	.
</s>
<s>
následně	následně	k6eAd1	následně
předpokládáme	předpokládat	k5eAaImIp1nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlo	světlo	k1gNnSc1	světlo
je	být	k5eAaImIp3nS	být
přitahované	přitahovaný	k2eAgNnSc1d1	přitahované
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
povrchu	povrch	k1gInSc2	povrch
silou	síla	k1gFnSc7	síla
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
vis	vis	k1gInSc1	vis
inertiae	inertiae	k6eAd1	inertiae
(	(	kIx(	(
<g/>
setrvačné	setrvačný	k2eAgFnSc2d1	setrvačná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostatními	ostatní	k2eAgNnPc7d1	ostatní
tělesy	těleso	k1gNnPc7	těleso
<g/>
,	,	kIx,	,
světlo	světlo	k1gNnSc1	světlo
vyzařované	vyzařovaný	k2eAgNnSc1d1	vyzařované
z	z	k7c2	z
takového	takový	k3xDgNnSc2	takový
tělesa	těleso	k1gNnSc2	těleso
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
díky	dík	k1gInPc7	dík
jeho	jeho	k3xOp3gFnSc2	jeho
přitažlivosti	přitažlivost	k1gFnSc2	přitažlivost
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
nepovažoval	považovat	k5eNaImAgInS	považovat
za	za	k7c4	za
pravděpodobné	pravděpodobný	k2eAgNnSc4d1	pravděpodobné
<g/>
,	,	kIx,	,
Michell	Michell	k1gMnSc1	Michell
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
o	o	k7c4	o
možnosti	možnost	k1gFnPc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c4	mnoho
takových	takový	k3xDgInPc2	takový
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1796	[number]	k4	1796
podpořil	podpořit	k5eAaPmAgMnS	podpořit
francouzský	francouzský	k2eAgMnSc1d1	francouzský
matematik	matematik	k1gMnSc1	matematik
Pierre	Pierr	k1gInSc5	Pierr
Simon	Simon	k1gMnSc1	Simon
de	de	k?	de
Laplace	Laplace	k1gFnSc2	Laplace
stejnou	stejný	k2eAgFnSc4d1	stejná
myšlenku	myšlenka	k1gFnSc4	myšlenka
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
a	a	k8xC	a
druhém	druhý	k4xOgInSc6	druhý
vydání	vydání	k1gNnSc6	vydání
své	svůj	k3xOyFgFnSc2	svůj
knihy	kniha	k1gFnSc2	kniha
Exposition	Exposition	k1gInSc1	Exposition
du	du	k?	du
Systeme	Systeme	k1gFnSc2	Systeme
du	du	k?	du
Monde	Mond	k1gMnSc5	Mond
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
podpora	podpora	k1gFnSc1	podpora
však	však	k9	však
zmizela	zmizet	k5eAaPmAgFnS	zmizet
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
vydáních	vydání	k1gNnPc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Podobným	podobný	k2eAgFnPc3d1	podobná
teoriím	teorie	k1gFnPc3	teorie
se	se	k3xPyFc4	se
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
věnovalo	věnovat	k5eAaImAgNnS	věnovat
minimum	minimum	k1gNnSc4	minimum
pozornosti	pozornost	k1gFnSc3	pozornost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlo	světlo	k1gNnSc1	světlo
je	být	k5eAaImIp3nS	být
vlnění	vlnění	k1gNnSc4	vlnění
bez	bez	k7c2	bez
hmotnosti	hmotnost	k1gFnSc2	hmotnost
neovlivnitelné	ovlivnitelný	k2eNgFnSc2d1	neovlivnitelná
gravitací	gravitace	k1gFnSc7	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
Albert	Albert	k1gMnSc1	Albert
Einstein	Einstein	k1gMnSc1	Einstein
teorii	teorie	k1gFnSc4	teorie
gravitace	gravitace	k1gFnSc2	gravitace
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
obecná	obecný	k2eAgFnSc1d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
gravitace	gravitace	k1gFnSc1	gravitace
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
Karl	Karl	k1gMnSc1	Karl
Schwarzschild	Schwarzschild	k1gMnSc1	Schwarzschild
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
řešení	řešení	k1gNnSc4	řešení
pro	pro	k7c4	pro
gravitační	gravitační	k2eAgNnSc4d1	gravitační
pole	pole	k1gNnSc4	pole
bodové	bodový	k2eAgFnSc2d1	bodová
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
dnes	dnes	k6eAd1	dnes
nazýváme	nazývat	k5eAaImIp1nP	nazývat
černou	černý	k2eAgFnSc7d1	černá
dírou	díra	k1gFnSc7	díra
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
opravdu	opravdu	k6eAd1	opravdu
teoreticky	teoreticky	k6eAd1	teoreticky
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Schwarzschildův	Schwarzschildův	k2eAgInSc1d1	Schwarzschildův
poloměr	poloměr	k1gInSc1	poloměr
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
poloměr	poloměr	k1gInSc1	poloměr
nerotující	rotující	k2eNgFnSc2d1	nerotující
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
nebyl	být	k5eNaImAgInS	být
dobře	dobře	k6eAd1	dobře
pochopený	pochopený	k2eAgInSc1d1	pochopený
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Schwarzschild	Schwarzschild	k1gMnSc1	Schwarzschild
ho	on	k3xPp3gMnSc4	on
nepovažoval	považovat	k5eNaImAgMnS	považovat
za	za	k7c4	za
fyzikální	fyzikální	k2eAgNnSc4d1	fyzikální
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dokázal	dokázat	k5eAaPmAgMnS	dokázat
Subrahmanyan	Subrahmanyan	k1gMnSc1	Subrahmanyan
Chandrasekhar	Chandrasekhar	k1gMnSc1	Chandrasekhar
<g/>
,	,	kIx,	,
že	že	k8xS	že
obecná	obecný	k2eAgFnSc1d1	obecná
relativita	relativita	k1gFnSc1	relativita
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nevyzařující	vyzařující	k2eNgNnSc1d1	vyzařující
těleso	těleso	k1gNnSc1	těleso
nad	nad	k7c4	nad
jistou	jistý	k2eAgFnSc4d1	jistá
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
známou	známý	k2eAgFnSc4d1	známá
jako	jako	k8xC	jako
Chandrasekharova	Chandrasekharův	k2eAgFnSc1d1	Chandrasekharova
mez	mez	k1gFnSc1	mez
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zhroutilo	zhroutit	k5eAaPmAgNnS	zhroutit
do	do	k7c2	do
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
by	by	kYmCp3nS	by
neexistovalo	existovat	k5eNaImAgNnS	existovat
nic	nic	k3yNnSc1	nic
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
by	by	kYmCp3nS	by
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
mohlo	moct	k5eAaImAgNnS	moct
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
jeho	jeho	k3xOp3gInPc3	jeho
argumentům	argument	k1gInPc3	argument
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
Arthur	Arthur	k1gMnSc1	Arthur
Eddington	Eddington	k1gInSc4	Eddington
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
něco	něco	k3yInSc4	něco
kolapsu	kolaps	k1gInSc6	kolaps
nevyhnutelně	vyhnutelně	k6eNd1	vyhnutelně
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
měli	mít	k5eAaImAgMnP	mít
pravdu	pravda	k1gFnSc4	pravda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
bílý	bílý	k1gMnSc1	bílý
trpaslík	trpaslík	k1gMnSc1	trpaslík
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
nad	nad	k7c4	nad
tuto	tento	k3xDgFnSc4	tento
mez	mez	k1gFnSc4	mez
se	se	k3xPyFc4	se
zhroutí	zhroutit	k5eAaPmIp3nS	zhroutit
do	do	k7c2	do
neutronové	neutronový	k2eAgFnSc2d1	neutronová
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
i	i	k9	i
neutronová	neutronový	k2eAgFnSc1d1	neutronová
hvězda	hvězda	k1gFnSc1	hvězda
se	se	k3xPyFc4	se
při	při	k7c6	při
hmotnosti	hmotnost	k1gFnSc6	hmotnost
nad	nad	k7c4	nad
tzv.	tzv.	kA	tzv.
Tolmanovu-Oppenheimerovu-Volkoffovu	Tolmanovu-Oppenheimerovu-Volkoffův	k2eAgFnSc4d1	Tolmanovu-Oppenheimerovu-Volkoffův
mez	mez	k1gFnSc4	mez
zhroutí	zhroutit	k5eAaPmIp3nS	zhroutit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
Robert	Robert	k1gMnSc1	Robert
Oppenheimer	Oppenheimer	k1gMnSc1	Oppenheimer
a	a	k8xC	a
H.	H.	kA	H.
Snyder	Snyder	k1gMnSc1	Snyder
předpověděli	předpovědět	k5eAaPmAgMnP	předpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
velmi	velmi	k6eAd1	velmi
hmotné	hmotný	k2eAgFnPc1d1	hmotná
hvězdy	hvězda	k1gFnPc1	hvězda
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
stát	stát	k5eAaPmF	stát
oběťmi	oběť	k1gFnPc7	oběť
dramatického	dramatický	k2eAgNnSc2d1	dramatické
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
zhroucení	zhroucení	k1gNnSc2	zhroucení
<g/>
.	.	kIx.	.
</s>
<s>
Černé	Černé	k2eAgFnPc1d1	Černé
díry	díra	k1gFnPc1	díra
by	by	kYmCp3nP	by
tak	tak	k9	tak
mohly	moct	k5eAaImAgInP	moct
přirozeně	přirozeně	k6eAd1	přirozeně
vznikat	vznikat	k5eAaImF	vznikat
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc1	takový
objekty	objekt	k1gInPc1	objekt
byly	být	k5eAaImAgInP	být
krátce	krátce	k6eAd1	krátce
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
zamrzlé	zamrzlý	k2eAgFnSc2d1	zamrzlá
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zhroucení	zhroucení	k1gNnSc1	zhroucení
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
pozorovatelné	pozorovatelný	k2eAgNnSc1d1	pozorovatelné
rapidně	rapidně	k6eAd1	rapidně
zpomaleně	zpomaleně	k6eAd1	zpomaleně
a	a	k8xC	a
se	s	k7c7	s
silně	silně	k6eAd1	silně
červeným	červený	k2eAgNnSc7d1	červené
spektrem	spektrum	k1gNnSc7	spektrum
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
Schwarzschildova	Schwarzschildův	k2eAgInSc2d1	Schwarzschildův
poloměru	poloměr	k1gInSc2	poloměr
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
hypotetické	hypotetický	k2eAgInPc1d1	hypotetický
objekty	objekt	k1gInPc1	objekt
však	však	k9	však
nebyly	být	k5eNaImAgInP	být
předmětem	předmět	k1gInSc7	předmět
většího	veliký	k2eAgInSc2d2	veliký
zájmu	zájem	k1gInSc2	zájem
až	až	k9	až
do	do	k7c2	do
pozdních	pozdní	k2eAgInPc2d1	pozdní
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
fyziků	fyzik	k1gMnPc2	fyzik
si	se	k3xPyFc3	se
totiž	totiž	k9	totiž
myslela	myslet	k5eAaImAgFnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
specifickou	specifický	k2eAgFnSc7d1	specifická
vlastností	vlastnost	k1gFnSc7	vlastnost
silně	silně	k6eAd1	silně
symetrických	symetrický	k2eAgNnPc2d1	symetrické
řešení	řešení	k1gNnPc2	řešení
popsaných	popsaný	k2eAgInPc2d1	popsaný
Schwarzschildem	Schwarzschild	k1gMnSc7	Schwarzschild
a	a	k8xC	a
že	že	k8xS	že
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
gravitačně	gravitačně	k6eAd1	gravitačně
kolabující	kolabující	k2eAgInSc1d1	kolabující
objekt	objekt	k1gInSc1	objekt
nestal	stát	k5eNaPmAgInS	stát
černou	černý	k2eAgFnSc7d1	černá
dírou	díra	k1gFnSc7	díra
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
černé	černý	k2eAgFnPc4d1	černá
díry	díra	k1gFnPc4	díra
znovu	znovu	k6eAd1	znovu
vzplanul	vzplanout	k5eAaPmAgMnS	vzplanout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
s	s	k7c7	s
pokrokem	pokrok	k1gInSc7	pokrok
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
teorie	teorie	k1gFnSc2	teorie
a	a	k8xC	a
pokusů	pokus	k1gInPc2	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Stephen	Stephen	k2eAgInSc1d1	Stephen
Hawking	Hawking	k1gInSc1	Hawking
dokázal	dokázat	k5eAaPmAgInS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
černé	černý	k2eAgFnPc1d1	černá
díry	díra	k1gFnPc1	díra
jsou	být	k5eAaImIp3nP	být
všeobecnou	všeobecný	k2eAgFnSc7d1	všeobecná
vlastností	vlastnost	k1gFnSc7	vlastnost
Einsteinovy	Einsteinův	k2eAgFnSc2d1	Einsteinova
teorie	teorie	k1gFnSc2	teorie
gravitace	gravitace	k1gFnSc2	gravitace
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
vyhnout	vyhnout	k5eAaPmF	vyhnout
při	při	k7c6	při
kolabování	kolabování	k1gNnSc6	kolabování
některých	některý	k3yIgInPc2	některý
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
ně	on	k3xPp3gMnPc4	on
rozproudil	rozproudit	k5eAaPmAgInS	rozproudit
v	v	k7c6	v
astronomické	astronomický	k2eAgFnSc6d1	astronomická
komunitě	komunita	k1gFnSc6	komunita
také	také	k9	také
objev	objev	k1gInSc4	objev
pulsaru	pulsar	k1gInSc2	pulsar
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
zavedl	zavést	k5eAaPmAgMnS	zavést
teoretický	teoretický	k2eAgMnSc1d1	teoretický
fyzik	fyzik	k1gMnSc1	fyzik
John	John	k1gMnSc1	John
Wheeler	Wheeler	k1gMnSc1	Wheeler
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byl	být	k5eAaImAgInS	být
příležitostně	příležitostně	k6eAd1	příležitostně
používaný	používaný	k2eAgInSc1d1	používaný
termín	termín	k1gInSc1	termín
černá	černý	k2eAgFnSc1d1	černá
hvězda	hvězda	k1gFnSc1	hvězda
nebo	nebo	k8xC	nebo
opisný	opisný	k2eAgInSc1d1	opisný
tvar	tvar	k1gInSc1	tvar
gravitačně	gravitačně	k6eAd1	gravitačně
zcela	zcela	k6eAd1	zcela
zhroucené	zhroucený	k2eAgNnSc4d1	zhroucené
těleso	těleso	k1gNnSc4	těleso
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
Tom	Tom	k1gMnSc1	Tom
Bolton	Bolton	k1gInSc4	Bolton
identifikoval	identifikovat	k5eAaBmAgMnS	identifikovat
Cygnus	Cygnus	k1gMnSc1	Cygnus
X-1	X-1	k1gMnSc1	X-1
jako	jako	k8xS	jako
černou	černý	k2eAgFnSc4d1	černá
díru	díra	k1gFnSc4	díra
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
dalekohledů	dalekohled	k1gInPc2	dalekohled
a	a	k8xC	a
přístrojů	přístroj	k1gInPc2	přístroj
na	na	k7c6	na
observatoři	observatoř	k1gFnSc6	observatoř
David	David	k1gMnSc1	David
Dunlap	Dunlap	k1gMnSc1	Dunlap
Observatory	Observator	k1gInPc4	Observator
náležící	náležící	k2eAgFnSc3d1	náležící
Torontské	torontský	k2eAgFnSc3d1	Torontská
univerzitě	univerzita	k1gFnSc3	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
aspektem	aspekt	k1gInSc7	aspekt
existence	existence	k1gFnSc2	existence
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
časoprostor	časoprostor	k1gInSc4	časoprostor
zakřivován	zakřivován	k2eAgInSc4d1	zakřivován
přítomností	přítomnost	k1gFnSc7	přítomnost
hmoty	hmota	k1gFnPc1	hmota
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
shoduje	shodovat	k5eAaImIp3nS	shodovat
se	s	k7c7	s
základními	základní	k2eAgInPc7d1	základní
principy	princip	k1gInPc7	princip
obecné	obecný	k2eAgFnSc2d1	obecná
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
Nejpozoruhodnější	pozoruhodný	k2eAgFnSc7d3	nejpozoruhodnější
vlastností	vlastnost	k1gFnSc7	vlastnost
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
důsledkem	důsledek	k1gInSc7	důsledek
zakřivení	zakřivení	k1gNnSc2	zakřivení
časoprostoru	časoprostor	k1gInSc2	časoprostor
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Horizont	horizont	k1gInSc1	horizont
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Myšlená	myšlený	k2eAgFnSc1d1	myšlená
kulová	kulový	k2eAgFnSc1d1	kulová
"	"	kIx"	"
<g/>
plocha	plocha	k1gFnSc1	plocha
<g/>
"	"	kIx"	"
obklopující	obklopující	k2eAgFnSc4d1	obklopující
hmotu	hmota	k1gFnSc4	hmota
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
horizont	horizont	k1gInSc1	horizont
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
horizontu	horizont	k1gInSc2	horizont
událostí	událost	k1gFnPc2	událost
je	být	k5eAaImIp3nS	být
úniková	únikový	k2eAgFnSc1d1	úniková
rychlost	rychlost	k1gFnSc1	rychlost
rovna	roven	k2eAgFnSc1d1	rovna
rychlosti	rychlost	k1gFnSc3	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Neobyčejně	obyčejně	k6eNd1	obyčejně
silné	silný	k2eAgNnSc1d1	silné
gravitační	gravitační	k2eAgNnSc1d1	gravitační
pole	pole	k1gNnSc1	pole
brání	bránit	k5eAaImIp3nS	bránit
všemu	všecek	k3xTgNnSc3	všecek
uvnitř	uvnitř	k7c2	uvnitř
horizontu	horizont	k1gInSc2	horizont
událostí	událost	k1gFnPc2	událost
uniknout	uniknout	k5eAaPmF	uniknout
přes	přes	k7c4	přes
jeho	jeho	k3xOp3gInSc4	jeho
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Cokoliv	cokoliv	k3yInSc1	cokoliv
z	z	k7c2	z
vnějšku	vnějšek	k1gInSc2	vnějšek
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
propadnout	propadnout	k5eAaPmF	propadnout
přes	přes	k7c4	přes
horizont	horizont	k1gInSc4	horizont
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
tomu	ten	k3xDgMnSc3	ten
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
kvantově	kvantově	k6eAd1	kvantově
mechanické	mechanický	k2eAgInPc1d1	mechanický
procesy	proces	k1gInPc1	proces
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
horizontu	horizont	k1gInSc2	horizont
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
vznik	vznik	k1gInSc4	vznik
virtuálních	virtuální	k2eAgInPc2d1	virtuální
párů	pár	k1gInPc2	pár
částic	částice	k1gFnPc2	částice
a	a	k8xC	a
antičástic	antičástice	k1gFnPc2	antičástice
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
vedou	vést	k5eAaImIp3nP	vést
například	například	k6eAd1	například
k	k	k7c3	k
efektu	efekt	k1gInSc3	efekt
tzv.	tzv.	kA	tzv.
vypařování	vypařování	k1gNnSc4	vypařování
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
ale	ale	k9	ale
částice	částice	k1gFnPc1	částice
přímo	přímo	k6eAd1	přímo
neunikají	unikat	k5eNaImIp3nP	unikat
zpod	zpod	k7c2	zpod
horizontu	horizont	k1gInSc2	horizont
událostí	událost	k1gFnPc2	událost
–	–	k?	–
proces	proces	k1gInSc1	proces
"	"	kIx"	"
<g/>
vypařování	vypařování	k1gNnSc1	vypařování
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
jiných	jiný	k2eAgInPc6d1	jiný
principech	princip	k1gInPc6	princip
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Hawkingova	Hawkingův	k2eAgNnSc2d1	Hawkingovo
vyjádření	vyjádření	k1gNnSc2	vyjádření
jde	jít	k5eAaImIp3nS	jít
ale	ale	k9	ale
o	o	k7c4	o
zdánlivý	zdánlivý	k2eAgInSc4d1	zdánlivý
horizont	horizont	k1gInSc4	horizont
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dobře	dobře	k6eAd1	dobře
odůvodněných	odůvodněný	k2eAgInPc2d1	odůvodněný
předpokladů	předpoklad	k1gInPc2	předpoklad
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
černé	černý	k2eAgFnPc1d1	černá
díry	díra	k1gFnPc1	díra
nemají	mít	k5eNaImIp3nP	mít
žádné	žádný	k3yNgFnPc4	žádný
pozorovatelné	pozorovatelný	k2eAgFnPc4d1	pozorovatelná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
byly	být	k5eAaImAgFnP	být
použitelné	použitelný	k2eAgFnPc1d1	použitelná
k	k	k7c3	k
objasnění	objasnění	k1gNnSc3	objasnění
jejich	jejich	k3xOp3gInSc2	jejich
"	"	kIx"	"
<g/>
vzhledu	vzhled	k1gInSc2	vzhled
<g/>
"	"	kIx"	"
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
obecné	obecný	k2eAgFnSc2d1	obecná
relativity	relativita	k1gFnSc2	relativita
můžeme	moct	k5eAaImIp1nP	moct
černé	černý	k2eAgFnPc4d1	černá
díry	díra	k1gFnPc4	díra
úplně	úplně	k6eAd1	úplně
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
třemi	tři	k4xCgInPc7	tři
parametry	parametr	k1gInPc7	parametr
<g/>
:	:	kIx,	:
hmota	hmota	k1gFnSc1	hmota
<g/>
,	,	kIx,	,
moment	moment	k1gInSc1	moment
hybnosti	hybnost	k1gFnSc2	hybnost
a	a	k8xC	a
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
(	(	kIx(	(
<g/>
čtvrtou	čtvrtá	k1gFnSc4	čtvrtá
teoreticky	teoreticky	k6eAd1	teoreticky
přípustnou	přípustný	k2eAgFnSc7d1	přípustná
vlastností	vlastnost	k1gFnSc7	vlastnost
je	být	k5eAaImIp3nS	být
magnetický	magnetický	k2eAgInSc1d1	magnetický
náboj	náboj	k1gInSc1	náboj
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
však	však	k9	však
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
pozorován	pozorován	k2eAgMnSc1d1	pozorován
nebyl	být	k5eNaImAgMnS	být
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Tento	tento	k3xDgInSc1	tento
princip	princip	k1gInSc1	princip
se	se	k3xPyFc4	se
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
frází	fráze	k1gFnPc2	fráze
"	"	kIx"	"
<g/>
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
nemají	mít	k5eNaImIp3nP	mít
vlasy	vlas	k1gInPc4	vlas
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
prvně	prvně	k?	prvně
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
John	John	k1gMnSc1	John
Wheeler	Wheeler	k1gMnSc1	Wheeler
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
tvrzení	tvrzení	k1gNnSc1	tvrzení
se	se	k3xPyFc4	se
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
teorii	teorie	k1gFnSc6	teorie
–	–	k?	–
teorie	teorie	k1gFnSc1	teorie
kvantová	kvantový	k2eAgFnSc1d1	kvantová
připouští	připouštět	k5eAaImIp3nS	připouštět
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
náboje	náboj	k1gInPc1	náboj
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
např.	např.	kA	např.
podivnost	podivnost	k1gFnSc1	podivnost
<g/>
,	,	kIx,	,
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
charakterizující	charakterizující	k2eAgFnSc2d1	charakterizující
elementární	elementární	k2eAgFnSc2d1	elementární
částice	částice	k1gFnSc2	částice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
projevit	projevit	k5eAaPmF	projevit
až	až	k9	až
v	v	k7c6	v
dostatečné	dostatečný	k2eAgFnSc6d1	dostatečná
blízkosti	blízkost	k1gFnSc6	blízkost
horizontu	horizont	k1gInSc2	horizont
událostí	událost	k1gFnPc2	událost
a	a	k8xC	a
nemají	mít	k5eNaImIp3nP	mít
astrofyzikální	astrofyzikální	k2eAgInSc4d1	astrofyzikální
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Objekty	objekt	k1gInPc1	objekt
v	v	k7c6	v
gravitačním	gravitační	k2eAgNnSc6d1	gravitační
poli	pole	k1gNnSc6	pole
jsou	být	k5eAaImIp3nP	být
vystaveny	vystavit	k5eAaPmNgInP	vystavit
zpomalení	zpomalení	k1gNnSc3	zpomalení
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
nazývaného	nazývaný	k2eAgInSc2d1	nazývaný
dilatace	dilatace	k1gFnSc2	dilatace
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fenomén	fenomén	k1gInSc1	fenomén
byl	být	k5eAaImAgInS	být
experimentálně	experimentálně	k6eAd1	experimentálně
potvrzen	potvrdit	k5eAaPmNgInS	potvrdit
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
s	s	k7c7	s
raketou	raketa	k1gFnSc7	raketa
Scout	Scout	k1gMnSc1	Scout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
a	a	k8xC	a
bere	brát	k5eAaImIp3nS	brát
jej	on	k3xPp3gMnSc4	on
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
například	například	k6eAd1	například
i	i	k8xC	i
navigační	navigační	k2eAgInSc1d1	navigační
systém	systém	k1gInSc1	systém
GPS	GPS	kA	GPS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
horizontu	horizont	k1gInSc2	horizont
událostí	událost	k1gFnSc7	událost
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
se	se	k3xPyFc4	se
dilatace	dilatace	k1gFnSc1	dilatace
času	čas	k1gInSc2	čas
projevuje	projevovat	k5eAaImIp3nS	projevovat
velmi	velmi	k6eAd1	velmi
výrazně	výrazně	k6eAd1	výrazně
<g/>
.	.	kIx.	.
</s>
<s>
Uvažme	uvázat	k5eAaPmRp1nP	uvázat
dva	dva	k4xCgMnPc4	dva
pozorovatele	pozorovatel	k1gMnPc4	pozorovatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
každý	každý	k3xTgMnSc1	každý
své	svůj	k3xOyFgFnPc1	svůj
hodinky	hodinka	k1gFnPc4	hodinka
seřízeny	seřízen	k2eAgFnPc1d1	seřízena
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
vzdáleného	vzdálený	k2eAgMnSc2d1	vzdálený
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
to	ten	k3xDgNnSc1	ten
vypadá	vypadat	k5eAaImIp3nS	vypadat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
by	by	kYmCp3nS	by
padajícímu	padající	k2eAgMnSc3d1	padající
pozorovateli	pozorovatel	k1gMnSc6	pozorovatel
trvalo	trvat	k5eAaImAgNnS	trvat
přiblížení	přiblížení	k1gNnSc1	přiblížení
k	k	k7c3	k
horizontu	horizont	k1gInSc3	horizont
událostí	událost	k1gFnPc2	událost
nekonečně	konečně	k6eNd1	konečně
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
</s>
<s>
Světlo	světlo	k1gNnSc1	světlo
vycházející	vycházející	k2eAgNnSc1d1	vycházející
z	z	k7c2	z
padajícího	padající	k2eAgMnSc2d1	padající
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
má	mít	k5eAaImIp3nS	mít
zvětšující	zvětšující	k2eAgNnSc1d1	zvětšující
se	se	k3xPyFc4	se
spektrální	spektrální	k2eAgInSc1d1	spektrální
rudý	rudý	k2eAgInSc1d1	rudý
posuv	posuv	k1gInSc1	posuv
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
horizontu	horizont	k1gInSc2	horizont
událostí	událost	k1gFnPc2	událost
roven	roven	k2eAgInSc1d1	roven
nekonečnu	nekonečno	k1gNnSc3	nekonečno
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
dilatace	dilatace	k1gFnSc2	dilatace
času	čas	k1gInSc2	čas
běží	běžet	k5eAaImIp3nS	běžet
čas	čas	k1gInSc1	čas
na	na	k7c6	na
hodinkách	hodinka	k1gFnPc6	hodinka
s	s	k7c7	s
pozorovatelem	pozorovatel	k1gMnSc7	pozorovatel
padajícím	padající	k2eAgMnSc7d1	padající
na	na	k7c4	na
černou	černý	k2eAgFnSc4d1	černá
díru	díra	k1gFnSc4	díra
a	a	k8xC	a
pozorovatelem	pozorovatel	k1gMnSc7	pozorovatel
vzdáleným	vzdálený	k2eAgMnSc7d1	vzdálený
různě	různě	k6eAd1	různě
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
efekt	efekt	k1gInSc1	efekt
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
na	na	k7c4	na
černou	černý	k2eAgFnSc4d1	černá
díru	díra	k1gFnSc4	díra
dopadá	dopadat	k5eAaImIp3nS	dopadat
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
vlastního	vlastní	k2eAgInSc2d1	vlastní
času	čas	k1gInSc2	čas
normálně	normálně	k6eAd1	normálně
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jej	on	k3xPp3gMnSc4	on
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
přiblížení	přiblížení	k1gNnSc1	přiblížení
k	k	k7c3	k
horizontu	horizont	k1gInSc3	horizont
událostí	událost	k1gFnPc2	událost
jeví	jevit	k5eAaImIp3nS	jevit
"	"	kIx"	"
<g/>
nekonečně	konečně	k6eNd1	konečně
<g/>
"	"	kIx"	"
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
relativita	relativita	k1gFnSc1	relativita
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
horizontem	horizont	k1gInSc7	horizont
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
singularita	singularita	k1gFnSc1	singularita
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
zakřivení	zakřivení	k1gNnSc1	zakřivení
časoprostoru	časoprostor	k1gInSc2	časoprostor
nekonečné	konečný	k2eNgFnSc2d1	nekonečná
a	a	k8xC	a
gravitační	gravitační	k2eAgFnSc2d1	gravitační
síly	síla	k1gFnSc2	síla
jsou	být	k5eAaImIp3nP	být
nekonečně	konečně	k6eNd1	konečně
velké	velký	k2eAgFnPc1d1	velká
<g/>
.	.	kIx.	.
</s>
<s>
Časoprostor	časoprostor	k1gInSc1	časoprostor
pod	pod	k7c7	pod
horizontem	horizont	k1gInSc7	horizont
událostí	událost	k1gFnPc2	událost
je	být	k5eAaImIp3nS	být
specifický	specifický	k2eAgInSc1d1	specifický
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
singularita	singularita	k1gFnSc1	singularita
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
z	z	k7c2	z
pozorovatelných	pozorovatelný	k2eAgFnPc2d1	pozorovatelná
budoucností	budoucnost	k1gFnPc2	budoucnost
každého	každý	k3xTgInSc2	každý
objektu	objekt	k1gInSc2	objekt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
projde	projít	k5eAaPmIp3nS	projít
horizontem	horizont	k1gInSc7	horizont
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vše	všechen	k3xTgNnSc1	všechen
uvnitř	uvnitř	k7c2	uvnitř
horizontu	horizont	k1gInSc2	horizont
událostí	událost	k1gFnPc2	událost
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
(	(	kIx(	(
<g/>
Penrose	Penrosa	k1gFnSc6	Penrosa
a	a	k8xC	a
Hawking	Hawking	k1gInSc1	Hawking
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
původním	původní	k2eAgInSc7d1	původní
návrhem	návrh	k1gInSc7	návrh
Johna	John	k1gMnSc2	John
Michella	Michell	k1gMnSc2	Michell
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1783	[number]	k4	1783
a	a	k8xC	a
relativistickým	relativistický	k2eAgNnSc7d1	relativistické
pojetím	pojetí	k1gNnSc7	pojetí
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
je	být	k5eAaImIp3nS	být
konceptuální	konceptuální	k2eAgFnSc1d1	konceptuální
nesrovnalost	nesrovnalost	k1gFnSc1	nesrovnalost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Michellově	Michellův	k2eAgFnSc6d1	Michellova
teorii	teorie	k1gFnSc6	teorie
se	se	k3xPyFc4	se
úniková	únikový	k2eAgFnSc1d1	úniková
rychlost	rychlost	k1gFnSc1	rychlost
rovnala	rovnat	k5eAaImAgFnS	rovnat
rychlosti	rychlost	k1gFnPc4	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
například	například	k6eAd1	například
stále	stále	k6eAd1	stále
teoreticky	teoreticky	k6eAd1	teoreticky
možné	možný	k2eAgNnSc1d1	možné
vytáhnout	vytáhnout	k5eAaPmF	vytáhnout
objekt	objekt	k1gInSc4	objekt
z	z	k7c2	z
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
pomocí	pomocí	k7c2	pomocí
lana	lano	k1gNnSc2	lano
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
relativita	relativita	k1gFnSc1	relativita
tuto	tento	k3xDgFnSc4	tento
mezeru	mezera	k1gFnSc4	mezera
eliminuje	eliminovat	k5eAaBmIp3nS	eliminovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jakmile	jakmile	k8xS	jakmile
je	být	k5eAaImIp3nS	být
objekt	objekt	k1gInSc4	objekt
za	za	k7c7	za
horizontem	horizont	k1gInSc7	horizont
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
časová	časový	k2eAgFnSc1d1	časová
osa	osa	k1gFnSc1	osa
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
konec	konec	k1gInSc4	konec
samotného	samotný	k2eAgInSc2d1	samotný
času	čas	k1gInSc2	čas
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
možný	možný	k2eAgInSc1d1	možný
návrat	návrat	k1gInSc1	návrat
světočáry	světočára	k1gFnSc2	světočára
ven	ven	k6eAd1	ven
přes	přes	k7c4	přes
horizont	horizont	k1gInSc4	horizont
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Očekává	očekávat	k5eAaImIp3nS	očekávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
budoucí	budoucí	k2eAgNnSc1d1	budoucí
zjemnění	zjemnění	k1gNnSc1	zjemnění
anebo	anebo	k8xC	anebo
zobecnění	zobecnění	k1gNnSc1	zobecnění
obecné	obecný	k2eAgFnSc2d1	obecná
relativity	relativita	k1gFnSc2	relativita
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
kvantové	kvantový	k2eAgFnSc2d1	kvantová
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
)	)	kIx)	)
změní	změnit	k5eAaPmIp3nS	změnit
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
podstatu	podstata	k1gFnSc4	podstata
nitra	nitro	k1gNnSc2	nitro
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
teoretiků	teoretik	k1gMnPc2	teoretik
interpretuje	interpretovat	k5eAaBmIp3nS	interpretovat
matematické	matematický	k2eAgFnPc4d1	matematická
rovnice	rovnice	k1gFnPc4	rovnice
popisující	popisující	k2eAgFnSc4d1	popisující
singularitu	singularita	k1gFnSc4	singularita
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
nekompletnost	nekompletnost	k1gFnSc4	nekompletnost
současné	současný	k2eAgFnSc2d1	současná
teorie	teorie	k1gFnSc2	teorie
a	a	k8xC	a
že	že	k8xS	že
k	k	k7c3	k
plnému	plný	k2eAgNnSc3d1	plné
pochopení	pochopení	k1gNnSc3	pochopení
singularity	singularita	k1gFnSc2	singularita
musí	muset	k5eAaImIp3nS	muset
do	do	k7c2	do
hry	hra	k1gFnSc2	hra
vstoupit	vstoupit	k5eAaPmF	vstoupit
nové	nový	k2eAgInPc4d1	nový
jevy	jev	k1gInPc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
otázka	otázka	k1gFnSc1	otázka
může	moct	k5eAaImIp3nS	moct
však	však	k9	však
být	být	k5eAaImF	být
pouze	pouze	k6eAd1	pouze
akademická	akademický	k2eAgFnSc1d1	akademická
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
hypotéza	hypotéza	k1gFnSc1	hypotéza
kosmické	kosmický	k2eAgFnSc2d1	kosmická
cenzury	cenzura	k1gFnSc2	cenzura
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
relativitě	relativita	k1gFnSc6	relativita
neexistují	existovat	k5eNaImIp3nP	existovat
nahé	nahý	k2eAgFnPc4d1	nahá
singularity	singularita	k1gFnPc4	singularita
<g/>
:	:	kIx,	:
všechny	všechen	k3xTgFnPc1	všechen
singularity	singularita	k1gFnPc1	singularita
jsou	být	k5eAaImIp3nP	být
schované	schovaná	k1gFnPc4	schovaná
za	za	k7c7	za
horizontem	horizont	k1gInSc7	horizont
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
a	a	k8xC	a
nelze	lze	k6eNd1	lze
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
prozkoumat	prozkoumat	k5eAaPmF	prozkoumat
<g/>
.	.	kIx.	.
</s>
<s>
Představme	představit	k5eAaPmRp1nP	představit
si	se	k3xPyFc3	se
nešťastného	šťastný	k2eNgMnSc4d1	nešťastný
kosmonauta	kosmonaut	k1gMnSc4	kosmonaut
padajícího	padající	k2eAgInSc2d1	padající
nohama	noha	k1gFnPc7	noha
napřed	napřed	k6eAd1	napřed
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
středu	střed	k1gInSc2	střed
nerotující	rotující	k2eNgFnSc2d1	nerotující
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
Schwarzschildova	Schwarzschildův	k2eAgInSc2d1	Schwarzschildův
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yRnSc7	co
blíže	blízce	k6eAd2	blízce
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
k	k	k7c3	k
horizontu	horizont	k1gInSc3	horizont
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
déle	dlouho	k6eAd2	dlouho
trvá	trvat	k5eAaImIp3nS	trvat
fotonům	foton	k1gInPc3	foton
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
<g/>
,	,	kIx,	,
uniknout	uniknout	k5eAaPmF	uniknout
gravitačnímu	gravitační	k2eAgNnSc3d1	gravitační
poli	pole	k1gNnSc3	pole
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálený	vzdálený	k2eAgMnSc1d1	vzdálený
pozorovatel	pozorovatel	k1gMnSc1	pozorovatel
uvidí	uvidět	k5eAaPmIp3nS	uvidět
kosmonautův	kosmonautův	k2eAgMnSc1d1	kosmonautův
zpomalující	zpomalující	k2eAgMnSc1d1	zpomalující
se	se	k3xPyFc4	se
sestup	sestup	k1gInSc1	sestup
při	při	k7c6	při
přibližování	přibližování	k1gNnSc6	přibližování
se	se	k3xPyFc4	se
k	k	k7c3	k
horizontu	horizont	k1gInSc3	horizont
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
kterého	který	k3yQgInSc2	který
zdánlivě	zdánlivě	k6eAd1	zdánlivě
nikdy	nikdy	k6eAd1	nikdy
nedosáhne	dosáhnout	k5eNaPmIp3nS	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Astronaut	astronaut	k1gMnSc1	astronaut
z	z	k7c2	z
vlastního	vlastní	k2eAgInSc2d1	vlastní
pohledu	pohled	k1gInSc2	pohled
překročí	překročit	k5eAaPmIp3nS	překročit
horizont	horizont	k1gInSc1	horizont
událostí	událost	k1gFnPc2	událost
a	a	k8xC	a
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
singularity	singularita	k1gFnPc4	singularita
v	v	k7c6	v
konečném	konečný	k2eAgInSc6d1	konečný
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
momentě	moment	k1gInSc6	moment
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
překročí	překročit	k5eAaPmIp3nS	překročit
horizont	horizont	k1gInSc1	horizont
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
ho	on	k3xPp3gNnSc4	on
nebude	být	k5eNaImBp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
z	z	k7c2	z
okolního	okolní	k2eAgInSc2d1	okolní
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pádu	pád	k1gInSc2	pád
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
všiml	všimnout	k5eAaPmAgMnS	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
světlo	světlo	k1gNnSc1	světlo
přicházející	přicházející	k2eAgNnSc1d1	přicházející
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
chodidel	chodidlo	k1gNnPc2	chodidlo
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
kolen	kolna	k1gFnPc2	kolna
a	a	k8xC	a
tak	tak	k6eAd1	tak
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
podléhá	podléhat	k5eAaImIp3nS	podléhat
zvětšujícímu	zvětšující	k2eAgInSc3d1	zvětšující
se	se	k3xPyFc4	se
rudému	rudý	k1gMnSc3	rudý
posuvu	posuv	k1gInSc2	posuv
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
neviditelným	viditelný	k2eNgMnSc7d1	Neviditelný
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
přiblíží	přiblížit	k5eAaPmIp3nS	přiblížit
k	k	k7c3	k
singularitě	singularita	k1gFnSc3	singularita
<g/>
,	,	kIx,	,
gradient	gradient	k1gInSc4	gradient
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
se	se	k3xPyFc4	se
od	od	k7c2	od
hlavy	hlava	k1gFnSc2	hlava
k	k	k7c3	k
chodidlům	chodidlo	k1gNnPc3	chodidlo
značně	značně	k6eAd1	značně
zvětší	zvětšit	k5eAaPmIp3nS	zvětšit
<g/>
.	.	kIx.	.
</s>
<s>
Bude	být	k5eAaImBp3nS	být
se	se	k3xPyFc4	se
cítit	cítit	k5eAaImF	cítit
natažený	natažený	k2eAgInSc4d1	natažený
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
ho	on	k3xPp3gNnSc4	on
roztrhnou	roztrhnout	k5eAaPmIp3nP	roztrhnout
slapové	slapový	k2eAgFnPc1d1	slapová
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
jeho	jeho	k3xOp3gNnPc6	jeho
chodidlech	chodidlo	k1gNnPc6	chodidlo
bude	být	k5eAaImBp3nS	být
působit	působit	k5eAaImF	působit
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnPc4d2	veliký
gravitace	gravitace	k1gFnPc4	gravitace
než	než	k8xS	než
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Blízko	blízko	k7c2	blízko
singularity	singularita	k1gFnSc2	singularita
se	se	k3xPyFc4	se
gradient	gradient	k1gInSc1	gradient
stane	stanout	k5eAaPmIp3nS	stanout
dostatečně	dostatečně	k6eAd1	dostatečně
velkým	velký	k2eAgInSc7d1	velký
k	k	k7c3	k
roztržení	roztržení	k1gNnSc3	roztržení
samotných	samotný	k2eAgInPc2d1	samotný
atomů	atom	k1gInPc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Bod	bod	k1gInSc1	bod
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
slapové	slapový	k2eAgFnPc1d1	slapová
síly	síla	k1gFnPc1	síla
stávají	stávat	k5eAaImIp3nP	stávat
zhoubnými	zhoubný	k2eAgNnPc7d1	zhoubné
<g/>
,	,	kIx,	,
závisí	záviset	k5eAaImIp3nP	záviset
na	na	k7c6	na
hmotě	hmota	k1gFnSc6	hmota
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
velké	velký	k2eAgFnPc4d1	velká
černé	černý	k2eAgFnPc4d1	černá
díry	díra	k1gFnPc4	díra
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
ty	ten	k3xDgMnPc4	ten
v	v	k7c6	v
centrech	centr	k1gInPc6	centr
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
tento	tento	k3xDgInSc1	tento
bod	bod	k1gInSc1	bod
ležet	ležet	k5eAaImF	ležet
až	až	k9	až
pod	pod	k7c7	pod
horizontem	horizont	k1gInSc7	horizont
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
může	moct	k5eAaImIp3nS	moct
teoreticky	teoreticky	k6eAd1	teoreticky
dostat	dostat	k5eAaPmF	dostat
přes	přes	k7c4	přes
horizont	horizont	k1gInSc4	horizont
událostí	událost	k1gFnPc2	událost
živý	živý	k2eAgMnSc1d1	živý
a	a	k8xC	a
v	v	k7c6	v
případech	případ	k1gInPc6	případ
obřích	obří	k2eAgFnPc2d1	obří
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
tento	tento	k3xDgInSc4	tento
přechod	přechod	k1gInSc4	přechod
nemusí	muset	k5eNaImIp3nS	muset
dokonce	dokonce	k9	dokonce
ani	ani	k9	ani
pocítit	pocítit	k5eAaPmF	pocítit
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
u	u	k7c2	u
malých	malý	k2eAgFnPc2d1	malá
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
slapové	slapový	k2eAgFnPc1d1	slapová
síly	síla	k1gFnPc1	síla
mohou	moct	k5eAaImIp3nP	moct
stát	stát	k5eAaPmF	stát
osudnými	osudný	k2eAgFnPc7d1	osudná
mnohem	mnohem	k6eAd1	mnohem
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
kosmonaut	kosmonaut	k1gMnSc1	kosmonaut
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
horizontu	horizont	k1gInSc3	horizont
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Horizont	horizont	k1gInSc1	horizont
událostí	událost	k1gFnPc2	událost
nerotující	rotující	k2eNgFnSc2d1	nerotující
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
je	být	k5eAaImIp3nS	být
kulová	kulový	k2eAgFnSc1d1	kulová
plocha	plocha	k1gFnSc1	plocha
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
singularita	singularita	k1gFnSc1	singularita
představuje	představovat	k5eAaImIp3nS	představovat
(	(	kIx(	(
<g/>
neformálně	formálně	k6eNd1	formálně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
)	)	kIx)	)
jeden	jeden	k4xCgInSc1	jeden
bod	bod	k1gInSc1	bod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
rotuje	rotovat	k5eAaImIp3nS	rotovat
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
radikálním	radikální	k2eAgFnPc3d1	radikální
změnám	změna	k1gFnPc3	změna
jak	jak	k6eAd1	jak
v	v	k7c6	v
okolním	okolní	k2eAgInSc6d1	okolní
prostoročase	prostoročas	k1gInSc6	prostoročas
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
samotném	samotný	k2eAgNnSc6d1	samotné
matematickém	matematický	k2eAgNnSc6d1	matematické
pojetí	pojetí	k1gNnSc6	pojetí
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
.	.	kIx.	.
</s>
<s>
Rotující	rotující	k2eAgFnSc1d1	rotující
černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
horizonty	horizont	k1gInPc4	horizont
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
<g/>
,	,	kIx,	,
Schwarzschildův	Schwarzschildův	k2eAgInSc1d1	Schwarzschildův
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
(	(	kIx(	(
<g/>
i	i	k8xC	i
co	co	k3yQnSc1	co
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přibývá	přibývat	k5eAaImIp3nS	přibývat
však	však	k9	však
ještě	ještě	k9	ještě
jeden	jeden	k4xCgMnSc1	jeden
vnitřní	vnitřní	k2eAgMnSc1d1	vnitřní
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Cauchyův	Cauchyův	k2eAgInSc1d1	Cauchyův
horizont	horizont	k1gInSc1	horizont
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
Schwarzschildovým	Schwarzschildův	k2eAgInSc7d1	Schwarzschildův
a	a	k8xC	a
Cauchyho	Cauchyha	k1gFnSc5	Cauchyha
horizontem	horizont	k1gInSc7	horizont
se	se	k3xPyFc4	se
všechna	všechen	k3xTgNnPc1	všechen
tělesa	těleso	k1gNnPc1	těleso
musí	muset	k5eAaImIp3nP	muset
pohybovat	pohybovat	k5eAaImF	pohybovat
směrem	směr	k1gInSc7	směr
ke	k	k7c3	k
středu	střed	k1gInSc3	střed
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
Cauchyho	Cauchy	k1gMnSc4	Cauchy
horizontem	horizont	k1gInSc7	horizont
je	být	k5eAaImIp3nS	být
však	však	k9	však
již	již	k6eAd1	již
opět	opět	k6eAd1	opět
možné	možný	k2eAgNnSc1d1	možné
zůstávat	zůstávat	k5eAaImF	zůstávat
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
zastavit	zastavit	k5eAaPmF	zastavit
pád	pád	k1gInSc4	pád
na	na	k7c4	na
singularitu	singularita	k1gFnSc4	singularita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
rotujících	rotující	k2eAgFnPc2d1	rotující
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
prstencová	prstencový	k2eAgFnSc1d1	prstencová
a	a	k8xC	a
prostorupodobná	prostorupodobný	k2eAgFnSc1d1	prostorupodobná
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
fyzikálního	fyzikální	k2eAgNnSc2d1	fyzikální
chápání	chápání	k1gNnSc3	chápání
okolí	okolí	k1gNnSc1	okolí
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
rotace	rotace	k1gFnSc2	rotace
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
v	v	k7c6	v
prostoročase	prostoročas	k1gInSc6	prostoročas
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
začíná	začínat	k5eAaImIp3nS	začínat
být	být	k5eAaImF	být
díky	díky	k7c3	díky
nenulovému	nulový	k2eNgInSc3d1	nenulový
momentu	moment	k1gInSc3	moment
hybnosti	hybnost	k1gFnSc2	hybnost
centrálního	centrální	k2eAgNnSc2d1	centrální
tělesa	těleso	k1gNnSc2	těleso
strháván	strháván	k2eAgInSc1d1	strháván
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
rotace	rotace	k1gFnSc2	rotace
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
známe	znát	k5eAaImIp1nP	znát
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
Lense-Thirringův	Lense-Thirringův	k2eAgInSc4d1	Lense-Thirringův
jev	jev	k1gInSc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
prostoročas	prostoročas	k1gInSc1	prostoročas
strháván	strhávat	k5eAaImNgInS	strhávat
rotací	rotace	k1gFnSc7	rotace
jakkoliv	jakkoliv	k6eAd1	jakkoliv
hmotného	hmotný	k2eAgNnSc2d1	hmotné
centrálního	centrální	k2eAgNnSc2d1	centrální
tělesa	těleso	k1gNnSc2	těleso
(	(	kIx(	(
<g/>
dokonce	dokonce	k9	dokonce
i	i	k9	i
Země	země	k1gFnSc1	země
<g/>
)	)	kIx)	)
avšak	avšak	k8xC	avšak
v	v	k7c6	v
případě	případ	k1gInSc6	případ
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
natolik	natolik	k6eAd1	natolik
významný	významný	k2eAgInSc4d1	významný
efekt	efekt	k1gInSc4	efekt
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
určité	určitý	k2eAgFnSc2d1	určitá
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
objekty	objekt	k1gInPc4	objekt
nemožné	možný	k2eNgNnSc1d1	nemožné
setrvávat	setrvávat	k5eAaImF	setrvávat
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
lokálně	lokálně	k6eAd1	lokálně
pohybovaly	pohybovat	k5eAaImAgInP	pohybovat
proti	proti	k7c3	proti
směru	směr	k1gInSc3	směr
rotace	rotace	k1gFnSc2	rotace
rychlostí	rychlost	k1gFnPc2	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterou	který	k3yQgFnSc7	který
je	být	k5eAaImIp3nS	být
setrvávání	setrvávání	k1gNnSc1	setrvávání
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
nemožné	nemožná	k1gFnSc2	nemožná
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
u	u	k7c2	u
rotujících	rotující	k2eAgFnPc2d1	rotující
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
nazývá	nazývat	k5eAaImIp3nS	nazývat
ergosféra	ergosféra	k1gFnSc1	ergosféra
(	(	kIx(	(
<g/>
obecněji	obecně	k6eAd2	obecně
také	také	k9	také
statická	statický	k2eAgFnSc1d1	statická
mez	mez	k1gFnSc1	mez
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ergosféra	Ergosféra	k1gFnSc1	Ergosféra
má	mít	k5eAaImIp3nS	mít
elipsoidní	elipsoidní	k2eAgInSc4d1	elipsoidní
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
na	na	k7c6	na
ose	osa	k1gFnSc6	osa
rotace	rotace	k1gFnSc1	rotace
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
horizont	horizont	k1gInSc4	horizont
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nad	nad	k7c7	nad
horizontem	horizont	k1gInSc7	horizont
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
objekty	objekt	k1gInPc4	objekt
pohybující	pohybující	k2eAgInPc4d1	pohybující
se	se	k3xPyFc4	se
v	v	k7c6	v
ergosféře	ergosféra	k1gFnSc6	ergosféra
nejen	nejen	k6eAd1	nejen
uniknout	uniknout	k5eAaPmF	uniknout
z	z	k7c2	z
gravitace	gravitace	k1gFnSc2	gravitace
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
za	za	k7c2	za
jistých	jistý	k2eAgFnPc2d1	jistá
okolností	okolnost	k1gFnPc2	okolnost
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
dokonce	dokonce	k9	dokonce
vymrštěny	vymrštěn	k2eAgFnPc4d1	vymrštěna
ven	ven	k6eAd1	ven
velmi	velmi	k6eAd1	velmi
vysokou	vysoký	k2eAgFnSc7d1	vysoká
rychlostí	rychlost	k1gFnSc7	rychlost
díky	díky	k7c3	díky
energii	energie	k1gFnSc3	energie
(	(	kIx(	(
<g/>
a	a	k8xC	a
momentu	moment	k1gInSc2	moment
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
)	)	kIx)	)
dodanými	dodaná	k1gFnPc7	dodaná
černou	černý	k2eAgFnSc7d1	černá
dírou	díra	k1gFnSc7	díra
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k9	i
její	její	k3xOp3gInSc4	její
název	název	k1gInSc4	název
ergosféra	ergosféra	k1gFnSc1	ergosféra
(	(	kIx(	(
<g/>
pracující	pracující	k2eAgFnSc1d1	pracující
sféra	sféra	k1gFnSc1	sféra
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
vykonávat	vykonávat	k5eAaImF	vykonávat
práci	práce	k1gFnSc4	práce
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
rotační	rotační	k2eAgFnSc2d1	rotační
energie	energie	k1gFnSc2	energie
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
Stephen	Stephen	k2eAgInSc1d1	Stephen
Hawking	Hawking	k1gInSc1	Hawking
dokázal	dokázat	k5eAaPmAgInS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
celková	celkový	k2eAgFnSc1d1	celková
plocha	plocha	k1gFnSc1	plocha
horizontu	horizont	k1gInSc2	horizont
událostí	událost	k1gFnPc2	událost
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
skupiny	skupina	k1gFnSc2	skupina
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
nikdy	nikdy	k6eAd1	nikdy
nezmenší	zmenšit	k5eNaPmIp3nS	zmenšit
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
tvrzení	tvrzení	k1gNnSc1	tvrzení
se	se	k3xPyFc4	se
příliš	příliš	k6eAd1	příliš
podobalo	podobat	k5eAaImAgNnS	podobat
druhému	druhý	k4xOgInSc3	druhý
termodynamickému	termodynamický	k2eAgInSc3d1	termodynamický
zákonu	zákon	k1gInSc3	zákon
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
plocha	plocha	k1gFnSc1	plocha
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
úlohu	úloha	k1gFnSc4	úloha
entropie	entropie	k1gFnSc2	entropie
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
Jacob	Jacoba	k1gFnPc2	Jacoba
Bekenstein	Bekenstein	k1gMnSc1	Bekenstein
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
entropie	entropie	k1gFnSc1	entropie
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
je	být	k5eAaImIp3nS	být
skutečně	skutečně	k6eAd1	skutečně
úměrná	úměrný	k2eAgFnSc1d1	úměrná
ploše	plocha	k1gFnSc6	plocha
jejího	její	k3xOp3gInSc2	její
horizontu	horizont	k1gInSc2	horizont
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
aplikoval	aplikovat	k5eAaBmAgInS	aplikovat
Hawking	Hawking	k1gInSc1	Hawking
kvantovou	kvantový	k2eAgFnSc4d1	kvantová
teorii	teorie	k1gFnSc4	teorie
pole	pole	k1gNnSc2	pole
na	na	k7c4	na
zakřivený	zakřivený	k2eAgInSc4d1	zakřivený
prostoročas	prostoročas	k1gInSc4	prostoročas
okolo	okolo	k7c2	okolo
horizontu	horizont	k1gInSc2	horizont
událostí	událost	k1gFnPc2	událost
a	a	k8xC	a
objevil	objevit	k5eAaPmAgMnS	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
můžou	můžou	k?	můžou
vyzařovat	vyzařovat	k5eAaImF	vyzařovat
tepelné	tepelný	k2eAgNnSc4d1	tepelné
záření	záření	k1gNnSc4	záření
<g/>
,	,	kIx,	,
známé	známý	k2eAgNnSc4d1	známé
jako	jako	k8xS	jako
Hawkingovo	Hawkingův	k2eAgNnSc4d1	Hawkingovo
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
prvního	první	k4xOgInSc2	první
zákona	zákon	k1gInSc2	zákon
mechaniky	mechanika	k1gFnSc2	mechanika
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
entropie	entropie	k1gFnSc1	entropie
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
čtvrtině	čtvrtina	k1gFnSc3	čtvrtina
plochy	plocha	k1gFnSc2	plocha
horizontu	horizont	k1gInSc2	horizont
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
výsledek	výsledek	k1gInSc1	výsledek
je	být	k5eAaImIp3nS	být
aplikovatelný	aplikovatelný	k2eAgInSc1d1	aplikovatelný
i	i	k9	i
na	na	k7c4	na
kosmologické	kosmologický	k2eAgInPc4d1	kosmologický
horizonty	horizont	k1gInPc4	horizont
jako	jako	k8xC	jako
de	de	k?	de
Sitterův	Sitterův	k2eAgInSc4d1	Sitterův
prostoročas	prostoročas	k1gInSc4	prostoročas
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
černé	černý	k2eAgFnPc1d1	černá
díry	díra	k1gFnPc1	díra
jsou	být	k5eAaImIp3nP	být
objekty	objekt	k1gInPc4	objekt
s	s	k7c7	s
maximální	maximální	k2eAgFnSc7d1	maximální
entropií	entropie	k1gFnSc7	entropie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
maximální	maximální	k2eAgFnSc1d1	maximální
entropie	entropie	k1gFnSc1	entropie
oblasti	oblast	k1gFnSc2	oblast
vesmíru	vesmír	k1gInSc2	vesmír
je	být	k5eAaImIp3nS	být
entropie	entropie	k1gFnSc1	entropie
největší	veliký	k2eAgFnSc2d3	veliký
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
vejde	vejít	k5eAaPmIp3nS	vejít
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
maximální	maximální	k2eAgFnSc1d1	maximální
entropie	entropie	k1gFnSc1	entropie
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
objemu	objem	k1gInSc6	objem
je	být	k5eAaImIp3nS	být
úměrná	úměrná	k1gFnSc1	úměrná
povrchu	povrch	k1gInSc2	povrch
tohoto	tento	k3xDgInSc2	tento
objemu	objem	k1gInSc2	objem
a	a	k8xC	a
ne	ne	k9	ne
objemu	objem	k1gInSc2	objem
jako	jako	k9	jako
takovému	takový	k3xDgNnSc3	takový
<g/>
,	,	kIx,	,
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
formulaci	formulace	k1gFnSc3	formulace
tzv.	tzv.	kA	tzv.
holografického	holografický	k2eAgInSc2d1	holografický
principu	princip	k1gInSc2	princip
<g/>
.	.	kIx.	.
</s>
<s>
Hawkingovo	Hawkingův	k2eAgNnSc1d1	Hawkingovo
záření	záření	k1gNnSc1	záření
vzniká	vznikat	k5eAaImIp3nS	vznikat
na	na	k7c6	na
horizontu	horizont	k1gInSc6	horizont
událostí	událost	k1gFnPc2	událost
a	a	k8xC	a
v	v	k7c6	v
současném	současný	k2eAgNnSc6d1	současné
pojetí	pojetí	k1gNnSc6	pojetí
nenese	nést	k5eNaImIp3nS	nést
žádnou	žádný	k3yNgFnSc4	žádný
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
vnitřku	vnitřek	k1gInSc6	vnitřek
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
kvantově-mechanický	kvantověechanický	k2eAgInSc4d1	kvantově-mechanický
projev	projev	k1gInSc4	projev
existence	existence	k1gFnSc2	existence
vakua	vakuum	k1gNnSc2	vakuum
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
černé	černý	k2eAgFnPc1d1	černá
díry	díra	k1gFnPc1	díra
nejsou	být	k5eNaImIp3nP	být
úplně	úplně	k6eAd1	úplně
černé	černý	k2eAgFnPc1d1	černá
a	a	k8xC	a
důsledkem	důsledek	k1gInSc7	důsledek
je	být	k5eAaImIp3nS	být
pomalé	pomalý	k2eAgNnSc1d1	pomalé
vypařování	vypařování	k1gNnSc1	vypařování
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc1	tento
efekty	efekt	k1gInPc1	efekt
zanedbatelné	zanedbatelný	k2eAgInPc1d1	zanedbatelný
pro	pro	k7c4	pro
astronomické	astronomický	k2eAgFnPc4d1	astronomická
černé	černý	k2eAgFnPc4d1	černá
díry	díra	k1gFnPc4	díra
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
významné	významný	k2eAgInPc1d1	významný
pro	pro	k7c4	pro
hypotetické	hypotetický	k2eAgFnPc4d1	hypotetická
miniaturní	miniaturní	k2eAgFnPc4d1	miniaturní
černé	černý	k2eAgFnPc4d1	černá
díry	díra	k1gFnPc4	díra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dominují	dominovat	k5eAaImIp3nP	dominovat
účinky	účinek	k1gInPc1	účinek
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
malé	malý	k2eAgFnPc1d1	malá
černé	černý	k2eAgFnPc1d1	černá
díry	díra	k1gFnPc1	díra
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
vypařují	vypařovat	k5eAaImIp3nP	vypařovat
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
mohou	moct	k5eAaImIp3nP	moct
i	i	k9	i
zmizet	zmizet	k5eAaPmF	zmizet
zcela	zcela	k6eAd1	zcela
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
má	mít	k5eAaImIp3nS	mít
každá	každý	k3xTgFnSc1	každý
černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
konečnou	konečný	k2eAgFnSc4d1	konečná
délku	délka	k1gFnSc4	délka
života	život	k1gInSc2	život
přímo	přímo	k6eAd1	přímo
úměrnou	úměrná	k1gFnSc7	úměrná
její	její	k3xOp3gFnSc2	její
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2004	[number]	k4	2004
Stephen	Stephen	k2eAgInSc1d1	Stephen
Hawking	Hawking	k1gInSc1	Hawking
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
se	s	k7c7	s
svými	svůj	k3xOyFgNnPc7	svůj
předchozími	předchozí	k2eAgNnPc7d1	předchozí
zjištěními	zjištění	k1gNnPc7	zjištění
<g/>
,	,	kIx,	,
prezentoval	prezentovat	k5eAaBmAgInS	prezentovat
nový	nový	k2eAgInSc1d1	nový
argument	argument	k1gInSc1	argument
<g/>
,	,	kIx,	,
že	že	k8xS	že
černé	černý	k2eAgFnPc1d1	černá
díry	díra	k1gFnPc1	díra
přece	přece	k9	přece
jen	jen	k9	jen
emitují	emitovat	k5eAaBmIp3nP	emitovat
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
pohlcují	pohlcovat	k5eAaImIp3nP	pohlcovat
<g/>
.	.	kIx.	.
</s>
<s>
Navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvantové	kvantový	k2eAgFnSc2d1	kvantová
perturbace	perturbace	k1gFnSc2	perturbace
horizontu	horizont	k1gInSc2	horizont
událostí	událost	k1gFnSc7	událost
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
dovolit	dovolit	k5eAaPmF	dovolit
uniknout	uniknout	k5eAaPmF	uniknout
informacím	informace	k1gFnPc3	informace
a	a	k8xC	a
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
vyvolané	vyvolaný	k2eAgNnSc4d1	vyvolané
Hawkingovo	Hawkingův	k2eAgNnSc4d1	Hawkingovo
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
ještě	ještě	k6eAd1	ještě
nebyla	být	k5eNaImAgFnS	být
prodiskutována	prodiskutovat	k5eAaPmNgFnS	prodiskutovat
ve	v	k7c6	v
vědecké	vědecký	k2eAgFnSc6d1	vědecká
komunitě	komunita	k1gFnSc6	komunita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyřeší	vyřešit	k5eAaPmIp3nS	vyřešit
informační	informační	k2eAgInSc1d1	informační
paradox	paradox	k1gInSc1	paradox
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
oznámení	oznámení	k1gNnSc1	oznámení
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
nové	nový	k2eAgFnSc6d1	nová
teorii	teorie	k1gFnSc6	teorie
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
nebývalou	bývalý	k2eNgFnSc4d1	bývalý
pozornost	pozornost	k1gFnSc4	pozornost
médií	médium	k1gNnPc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
skupina	skupina	k1gFnSc1	skupina
vědců	vědec	k1gMnPc2	vědec
na	na	k7c6	na
základě	základ	k1gInSc6	základ
simulací	simulace	k1gFnSc7	simulace
došla	dojít	k5eAaPmAgFnS	dojít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
černé	černý	k2eAgFnPc1d1	černá
díry	díra	k1gFnPc1	díra
nemusejí	muset	k5eNaImIp3nP	muset
vůbec	vůbec	k9	vůbec
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výpočtů	výpočet	k1gInPc2	výpočet
Hawkingovo	Hawkingův	k2eAgNnSc1d1	Hawkingovo
záření	záření	k1gNnSc1	záření
při	při	k7c6	při
hroucení	hroucení	k1gNnSc6	hroucení
hvězdy	hvězda	k1gFnSc2	hvězda
odnáší	odnášet	k5eAaImIp3nS	odnášet
tolik	tolik	k6eAd1	tolik
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
že	že	k8xS	že
ke	k	k7c3	k
kolapsu	kolaps	k1gInSc3	kolaps
nedojde	dojít	k5eNaPmIp3nS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Černé	Černé	k2eAgFnPc1d1	Černé
díry	díra	k1gFnPc1	díra
jsou	být	k5eAaImIp3nP	být
předpovězené	předpovězený	k2eAgInPc1d1	předpovězený
Einsteinovou	Einsteinův	k2eAgFnSc7d1	Einsteinova
teorií	teorie	k1gFnSc7	teorie
obecné	obecný	k2eAgFnSc2d1	obecná
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nejjednodušším	jednoduchý	k2eAgInSc6d3	nejjednodušší
případě	případ	k1gInSc6	případ
jsou	být	k5eAaImIp3nP	být
popsány	popsán	k2eAgFnPc4d1	popsána
tzv.	tzv.	kA	tzv.
Schwarzschildovou	Schwarzschildová	k1gFnSc7	Schwarzschildová
metrikou	metrika	k1gFnSc7	metrika
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgNnSc4d3	nejstarší
a	a	k8xC	a
nejjednodušší	jednoduchý	k2eAgNnSc4d3	nejjednodušší
exaktní	exaktní	k2eAgNnSc4d1	exaktní
řešení	řešení	k1gNnSc4	řešení
Einsteinových	Einsteinových	k2eAgFnPc2d1	Einsteinových
rovnic	rovnice	k1gFnPc2	rovnice
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
objeveno	objeven	k2eAgNnSc1d1	objeveno
Karlem	Karel	k1gMnSc7	Karel
Schwarzschildem	Schwarzschild	k1gMnSc7	Schwarzschild
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
řešení	řešení	k1gNnSc1	řešení
popisuje	popisovat	k5eAaImIp3nS	popisovat
zakřivení	zakřivení	k1gNnSc4	zakřivení
prostoročasu	prostoročas	k1gInSc2	prostoročas
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
nerotujícího	rotující	k2eNgInSc2d1	nerotující
sféricky	sféricky	k6eAd1	sféricky
symetrického	symetrický	k2eAgInSc2d1	symetrický
objektu	objekt	k1gInSc2	objekt
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jeho	jeho	k3xOp3gFnSc1	jeho
metrika	metrika	k1gFnSc1	metrika
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
s	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
−	−	k?	−
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
1	[number]	k4	1
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
G	G	kA	G
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
t	t	k?	t
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
1	[number]	k4	1
−	−	k?	−
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
G	G	kA	G
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
−	−	k?	−
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
Ω	Ω	k?	Ω
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
s	s	k7c7	s
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
-c	-c	k?	-c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
Gm	Gm	k1gFnSc2	Gm
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
t	t	k?	t
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
left	left	k1gInSc1	left
<g/>
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
Gm	Gm	k1gFnPc2	Gm
\	\	kIx~	\
<g/>
over	over	k1gInSc1	over
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
r	r	kA	r
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
r	r	kA	r
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
Ω	Ω	k?	Ω
:	:	kIx,	:
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
θ	θ	k?	θ
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
sin	sin	kA	sin
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
θ	θ	k?	θ
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
φ	φ	k?	φ
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc4	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
Omega	omega	k1gFnSc1	omega
^	^	kIx~	^
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
theta	theta	k1gFnSc1	theta
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
sin	sin	kA	sin
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
theta	theta	k1gFnSc1	theta
\	\	kIx~	\
<g/>
;	;	kIx,	;
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
phi	phi	k?	phi
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
standardní	standardní	k2eAgMnSc1d1	standardní
člen	člen	k1gMnSc1	člen
prostorového	prostorový	k2eAgInSc2d1	prostorový
úhlu	úhel	k1gInSc2	úhel
obdobný	obdobný	k2eAgMnSc1d1	obdobný
sférickým	sférický	k2eAgFnPc3d1	sférická
souřadnicím	souřadnice	k1gFnPc3	souřadnice
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Schwarzschildova	Schwarzschildův	k2eAgNnSc2d1	Schwarzschildovo
řešení	řešení	k1gNnSc2	řešení
se	se	k3xPyFc4	se
kulově	kulově	k6eAd1	kulově
symetrický	symetrický	k2eAgInSc1d1	symetrický
objekt	objekt	k1gInSc1	objekt
nevyhnutelně	vyhnutelně	k6eNd1	vyhnutelně
zhroutí	zhroutit	k5eAaPmIp3nS	zhroutit
vlivem	vliv	k1gInSc7	vliv
své	svůj	k3xOyFgFnSc2	svůj
vlastní	vlastní	k2eAgFnSc2d1	vlastní
gravitace	gravitace	k1gFnSc2	gravitace
do	do	k7c2	do
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
jeho	jeho	k3xOp3gInSc4	jeho
poloměr	poloměr	k1gInSc4	poloměr
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k9	jako
Schwarzschildův	Schwarzschildův	k2eAgInSc4d1	Schwarzschildův
poloměr	poloměr	k1gInSc4	poloměr
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
tímto	tento	k3xDgInSc7	tento
poloměrem	poloměr	k1gInSc7	poloměr
je	být	k5eAaImIp3nS	být
prostoročas	prostoročas	k1gInSc1	prostoročas
tak	tak	k6eAd1	tak
silně	silně	k6eAd1	silně
zakřivený	zakřivený	k2eAgInSc1d1	zakřivený
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc1	každý
světelný	světelný	k2eAgInSc1d1	světelný
paprsek	paprsek	k1gInSc1	paprsek
vyzářený	vyzářený	k2eAgInSc1d1	vyzářený
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
libovolným	libovolný	k2eAgInSc7d1	libovolný
směrem	směr	k1gInSc7	směr
bude	být	k5eAaImBp3nS	být
pohybovat	pohybovat	k5eAaImF	pohybovat
do	do	k7c2	do
středu	střed	k1gInSc2	střed
celého	celý	k2eAgInSc2d1	celý
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
se	se	k3xPyFc4	se
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
gravitační	gravitační	k2eAgFnSc1d1	gravitační
singularita	singularita	k1gFnSc1	singularita
<g/>
,	,	kIx,	,
oblast	oblast	k1gFnSc1	oblast
s	s	k7c7	s
teoreticky	teoreticky	k6eAd1	teoreticky
nekonečnou	konečný	k2eNgFnSc7d1	nekonečná
hustotou	hustota	k1gFnSc7	hustota
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
pod	pod	k7c7	pod
horizontem	horizont	k1gInSc7	horizont
událostí	událost	k1gFnPc2	událost
však	však	k9	však
již	již	k6eAd1	již
ve	v	k7c6	v
Schwarzschildových	Schwarzschildův	k2eAgFnPc6d1	Schwarzschildova
souřadnicích	souřadnice	k1gFnPc6	souřadnice
nelze	lze	k6eNd1	lze
popsat	popsat	k5eAaPmF	popsat
a	a	k8xC	a
užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
např.	např.	kA	např.
Kruskal-Szekeresových	Kruskal-Szekeresový	k2eAgFnPc2d1	Kruskal-Szekeresový
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
.	.	kIx.	.
</s>
<s>
Schwarzschildův	Schwarzschildův	k2eAgInSc4d1	Schwarzschildův
poloměr	poloměr	k1gInSc4	poloměr
ve	v	k7c6	v
výše	vysoce	k6eAd2	vysoce
zavedených	zavedený	k2eAgFnPc6d1	zavedená
souřadnicích	souřadnice	k1gFnPc6	souřadnice
je	být	k5eAaImIp3nS	být
vyjádřený	vyjádřený	k2eAgInSc1d1	vyjádřený
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
G	G	kA	G
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
rm	rm	k?	rm
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
Gm	Gm	k1gMnSc1	Gm
\	\	kIx~	\
<g/>
over	over	k1gMnSc1	over
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
G	G	kA	G
je	být	k5eAaImIp3nS	být
gravitační	gravitační	k2eAgFnSc1d1	gravitační
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
,	,	kIx,	,
m	m	kA	m
je	být	k5eAaImIp3nS	být
hmotnost	hmotnost	k1gFnSc4	hmotnost
objektu	objekt	k1gInSc2	objekt
a	a	k8xC	a
c	c	k0	c
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
objekt	objekt	k1gInSc4	objekt
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
Schwarzschildův	Schwarzschildův	k2eAgInSc4d1	Schwarzschildův
poloměr	poloměr	k1gInSc4	poloměr
9	[number]	k4	9
milimetrů	milimetr	k1gInPc2	milimetr
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
hustota	hustota	k1gFnSc1	hustota
Schwarzschildova	Schwarzschildův	k2eAgInSc2d1	Schwarzschildův
poloměru	poloměr	k1gInSc2	poloměr
se	se	k3xPyFc4	se
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
se	s	k7c7	s
zvětšováním	zvětšování	k1gNnSc7	zvětšování
hmotnosti	hmotnost	k1gFnSc2	hmotnost
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
Země	zem	k1gFnSc2	zem
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
hustotu	hustota	k1gFnSc4	hustota
2	[number]	k4	2
<g/>
×	×	k?	×
<g/>
1030	[number]	k4	1030
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m3	m3	k4	m3
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obří	obří	k2eAgFnSc1d1	obří
černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
109	[number]	k4	109
hmotností	hmotnost	k1gFnPc2	hmotnost
slunce	slunce	k1gNnSc2	slunce
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
hustotu	hustota	k1gFnSc4	hustota
okolo	okolo	k7c2	okolo
20	[number]	k4	20
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m3	m3	k4	m3
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
než	než	k8xS	než
hustota	hustota	k1gFnSc1	hustota
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
hustota	hustota	k1gFnSc1	hustota
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ρ	ρ	k?	ρ
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
6	[number]	k4	6
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
32	[number]	k4	32
π	π	k?	π
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
G	G	kA	G
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
rho	rho	k?	rho
=	=	kIx~	=
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
32	[number]	k4	32
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
m	m	kA	m
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
G	G	kA	G
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
střední	střední	k2eAgInSc4d1	střední
poloměr	poloměr	k1gInSc4	poloměr
Země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
6371	[number]	k4	6371
km	km	kA	km
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
její	její	k3xOp3gInSc1	její
objem	objem	k1gInSc1	objem
zmenšený	zmenšený	k2eAgInSc1d1	zmenšený
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
1026	[number]	k4	1026
krát	krát	k6eAd1	krát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zhroutila	zhroutit	k5eAaPmAgFnS	zhroutit
do	do	k7c2	do
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
těleso	těleso	k1gNnSc4	těleso
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
Schwarzschildův	Schwarzschildův	k2eAgInSc1d1	Schwarzschildův
poloměr	poloměr	k1gInSc1	poloměr
přibližně	přibližně	k6eAd1	přibližně
3	[number]	k4	3
km	km	kA	km
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
méně	málo	k6eAd2	málo
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
současný	současný	k2eAgInSc4d1	současný
poloměr	poloměr	k1gInSc4	poloměr
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
poloměr	poloměr	k1gInSc1	poloměr
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yRgInSc2	který
se	se	k3xPyFc4	se
Slunce	slunce	k1gNnSc1	slunce
nakonec	nakonec	k6eAd1	nakonec
smrští	smrštit	k5eAaPmIp3nS	smrštit
po	po	k7c6	po
vyhoření	vyhoření	k1gNnSc6	vyhoření
svého	svůj	k3xOyFgNnSc2	svůj
nukleárního	nukleární	k2eAgNnSc2d1	nukleární
paliva	palivo	k1gNnSc2	palivo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bude	být	k5eAaImBp3nS	být
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInSc4	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnější	hmotný	k2eAgFnPc1d2	hmotnější
hvězdy	hvězda	k1gFnPc1	hvězda
se	se	k3xPyFc4	se
však	však	k9	však
můžou	můžou	k?	můžou
zhroutit	zhroutit	k5eAaPmF	zhroutit
do	do	k7c2	do
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
na	na	k7c6	na
konci	konec	k1gInSc6	konec
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
jsou	být	k5eAaImIp3nP	být
černé	černý	k2eAgFnPc4d1	černá
díry	díra	k1gFnPc4	díra
předpovídané	předpovídaný	k2eAgNnSc4d1	předpovídané
i	i	k8xC	i
jinými	jiný	k2eAgNnPc7d1	jiné
řešeními	řešení	k1gNnPc7	řešení
Einsteinových	Einsteinových	k2eAgMnPc1d1	Einsteinových
rovnic	rovnice	k1gFnPc2	rovnice
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Kerrova	Kerrův	k2eAgFnSc1d1	Kerrova
metrika	metrika	k1gFnSc1	metrika
pro	pro	k7c4	pro
rotující	rotující	k2eAgFnPc4d1	rotující
černé	černý	k2eAgFnPc4d1	černá
díry	díra	k1gFnPc4	díra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
prstencovou	prstencový	k2eAgFnSc4d1	prstencová
singularitu	singularita	k1gFnSc4	singularita
<g/>
.	.	kIx.	.
</s>
<s>
Reissner-Nordströmova	Reissner-Nordströmův	k2eAgFnSc1d1	Reissner-Nordströmův
metrika	metrika	k1gFnSc1	metrika
popisuje	popisovat	k5eAaImIp3nS	popisovat
elektricky	elektricky	k6eAd1	elektricky
nabité	nabitý	k2eAgFnSc2d1	nabitá
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
.	.	kIx.	.
</s>
<s>
Nejobecnější	obecní	k2eAgNnSc1d3	obecní
řešení	řešení	k1gNnSc1	řešení
má	mít	k5eAaImIp3nS	mít
Kerr-Newmanovu	Kerr-Newmanův	k2eAgFnSc4d1	Kerr-Newmanův
metriku	metrika	k1gFnSc4	metrika
a	a	k8xC	a
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
případu	případ	k1gInSc3	případ
nabitých	nabitý	k2eAgFnPc2d1	nabitá
rotujících	rotující	k2eAgFnPc2d1	rotující
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
relativita	relativita	k1gFnSc1	relativita
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
jiné	jiný	k2eAgFnPc1d1	jiná
teorie	teorie	k1gFnPc1	teorie
gravitace	gravitace	k1gFnSc2	gravitace
<g/>
)	)	kIx)	)
nejen	nejen	k6eAd1	nejen
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
černé	černý	k2eAgFnPc1d1	černá
díry	díra	k1gFnPc1	díra
mohou	moct	k5eAaImIp3nP	moct
existovat	existovat	k5eAaImF	existovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
přímo	přímo	k6eAd1	přímo
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznikají	vznikat	k5eAaImIp3nP	vznikat
přirozeně	přirozeně	k6eAd1	přirozeně
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
modelů	model	k1gInPc2	model
vzniku	vznik	k1gInSc2	vznik
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
:	:	kIx,	:
Gravitační	gravitační	k2eAgInSc1d1	gravitační
kolaps	kolaps	k1gInSc1	kolaps
Hmota	hmota	k1gFnSc1	hmota
se	se	k3xPyFc4	se
gravitačně	gravitačně	k6eAd1	gravitačně
zhroutí	zhroutit	k5eAaPmIp3nS	zhroutit
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
prostoru	prostor	k1gInSc6	prostor
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
díky	díky	k7c3	díky
procesu	proces	k1gInSc3	proces
nazývanému	nazývaný	k2eAgMnSc3d1	nazývaný
gravitační	gravitační	k2eAgInSc4d1	gravitační
kolaps	kolaps	k1gInSc4	kolaps
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnPc1d3	nejznámější
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
procesů	proces	k1gInPc2	proces
jsou	být	k5eAaImIp3nP	být
některá	některý	k3yIgNnPc1	některý
finální	finální	k2eAgNnPc1d1	finální
stádia	stádium	k1gNnPc1	stádium
evoluce	evoluce	k1gFnSc2	evoluce
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
poklesne	poklesnout	k5eAaPmIp3nS	poklesnout
tlakový	tlakový	k2eAgInSc1d1	tlakový
gradient	gradient	k1gInSc1	gradient
(	(	kIx(	(
<g/>
tlak	tlak	k1gInSc1	tlak
záření	záření	k1gNnSc2	záření
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
)	)	kIx)	)
a	a	k8xC	a
hvězda	hvězda	k1gFnSc1	hvězda
se	se	k3xPyFc4	se
neudrží	udržet	k5eNaPmIp3nS	udržet
v	v	k7c6	v
hydrostatické	hydrostatický	k2eAgFnSc6d1	hydrostatická
rovnováze	rovnováha	k1gFnSc6	rovnováha
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
splněna	splněn	k2eAgFnSc1d1	splněna
podmínka	podmínka	k1gFnSc1	podmínka
dostatečného	dostatečný	k2eAgNnSc2d1	dostatečné
množství	množství	k1gNnSc2	množství
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
následný	následný	k2eAgInSc1d1	následný
kolaps	kolaps	k1gInSc1	kolaps
nebyl	být	k5eNaImAgMnS	být
zadržen	zadržet	k5eAaPmNgMnS	zadržet
například	například	k6eAd1	například
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
neutronové	neutronový	k2eAgFnSc2d1	neutronová
hvězdy	hvězda	k1gFnSc2	hvězda
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
degenerovaného	degenerovaný	k2eAgInSc2d1	degenerovaný
neutronového	neutronový	k2eAgInSc2d1	neutronový
plynu	plyn	k1gInSc2	plyn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kolaps	kolaps	k1gInSc1	kolaps
takové	takový	k3xDgFnSc2	takový
hvězdy	hvězda	k1gFnSc2	hvězda
pak	pak	k6eAd1	pak
není	být	k5eNaImIp3nS	být
možno	možno	k6eAd1	možno
zastavit	zastavit	k5eAaPmF	zastavit
-	-	kIx~	-
povrch	povrch	k1gInSc1	povrch
hvězdy	hvězda	k1gFnSc2	hvězda
se	se	k3xPyFc4	se
zhroutí	zhroutit	k5eAaPmIp3nS	zhroutit
pod	pod	k7c4	pod
horizont	horizont	k1gInSc4	horizont
událostí	událost	k1gFnPc2	událost
a	a	k8xC	a
nevyhnutelně	vyhnutelně	k6eNd1	vyhnutelně
skončí	skončit	k5eAaPmIp3nS	skončit
v	v	k7c6	v
singularitě	singularita	k1gFnSc6	singularita
<g/>
.	.	kIx.	.
</s>
<s>
Akumulace	akumulace	k1gFnSc1	akumulace
hmoty	hmota	k1gFnSc2	hmota
Když	když	k8xS	když
v	v	k7c6	v
nějakém	nějaký	k3yIgInSc6	nějaký
prostoru	prostor	k1gInSc6	prostor
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
gravitačních	gravitační	k2eAgFnPc2d1	gravitační
sil	síla	k1gFnPc2	síla
k	k	k7c3	k
seskupování	seskupování	k1gNnSc3	seskupování
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
gravitační	gravitační	k2eAgNnSc1d1	gravitační
pole	pole	k1gNnSc1	pole
takové	takový	k3xDgFnSc2	takový
oblasti	oblast	k1gFnSc2	oblast
sílí	sílet	k5eAaImIp3nS	sílet
–	–	k?	–
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jazyku	jazyk	k1gInSc6	jazyk
relativity	relativita	k1gFnSc2	relativita
–	–	k?	–
zakřivení	zakřivení	k1gNnSc1	zakřivení
prostoru	prostor	k1gInSc2	prostor
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
se	se	k3xPyFc4	se
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
úniková	únikový	k2eAgFnSc1d1	úniková
rychlost	rychlost	k1gFnSc1	rychlost
v	v	k7c6	v
nějaké	nějaký	k3yIgFnSc6	nějaký
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
působení	působení	k1gNnSc2	působení
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
rychlosti	rychlost	k1gFnPc4	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
se	se	k3xPyFc4	se
horizont	horizont	k1gInSc1	horizont
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
uvnitř	uvnitř	k7c2	uvnitř
kterého	který	k3yRgInSc2	který
musí	muset	k5eAaImIp3nS	muset
hmota	hmota	k1gFnSc1	hmota
nevyhnutelně	vyhnutelně	k6eNd1	vyhnutelně
skončit	skončit	k5eAaPmF	skončit
v	v	k7c6	v
singularitě	singularita	k1gFnSc6	singularita
<g/>
.	.	kIx.	.
</s>
<s>
Černé	Černé	k2eAgFnPc1d1	Černé
díry	díra	k1gFnPc1	díra
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
existují	existovat	k5eAaImIp3nP	existovat
jako	jako	k9	jako
dva	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
modelů	model	k1gInPc2	model
<g/>
:	:	kIx,	:
Primordiální	primordiální	k2eAgFnPc1d1	primordiální
černé	černý	k2eAgFnPc1d1	černá
díry	díra	k1gFnPc1	díra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohly	moct	k5eAaImAgFnP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
v	v	k7c6	v
období	období	k1gNnSc6	období
velmi	velmi	k6eAd1	velmi
raných	raný	k2eAgFnPc2d1	raná
fází	fáze	k1gFnPc2	fáze
vývoje	vývoj	k1gInSc2	vývoj
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Prozatím	prozatím	k6eAd1	prozatím
však	však	k9	však
nebyly	být	k5eNaImAgInP	být
observačně	observačně	k6eAd1	observačně
potvrzeny	potvrdit	k5eAaPmNgInP	potvrdit
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
netvoří	tvořit	k5eNaImIp3nS	tvořit
významnou	významný	k2eAgFnSc4d1	významná
část	část	k1gFnSc4	část
temné	temný	k2eAgFnSc2d1	temná
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Obří	obří	k2eAgFnPc1d1	obří
černé	černý	k2eAgFnPc1d1	černá
díry	díra	k1gFnPc1	díra
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
centrech	centr	k1gInPc6	centr
galaxií	galaxie	k1gFnPc2	galaxie
(	(	kIx(	(
<g/>
i	i	k9	i
včetně	včetně	k7c2	včetně
naší	náš	k3xOp1gFnSc2	náš
Mléčné	mléčný	k2eAgFnSc2d1	mléčná
dráhy	dráha	k1gFnSc2	dráha
<g/>
)	)	kIx)	)
a	a	k8xC	a
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
také	také	k9	také
kulových	kulový	k2eAgFnPc2d1	kulová
hvězdokup	hvězdokupa	k1gFnPc2	hvězdokupa
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
vytvoření	vytvoření	k1gNnSc2	vytvoření
horizontu	horizont	k1gInSc2	horizont
událostí	událost	k1gFnPc2	událost
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nakupení	nakupení	k1gNnSc2	nakupení
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
hmoty	hmota	k1gFnSc2	hmota
na	na	k7c6	na
relativně	relativně	k6eAd1	relativně
malém	malý	k2eAgInSc6d1	malý
prostoru	prostor	k1gInSc6	prostor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
hmotou	hmota	k1gFnSc7	hmota
myslí	myslet	k5eAaImIp3nP	myslet
i	i	k9	i
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
hvězdy	hvězda	k1gFnPc1	hvězda
případně	případně	k6eAd1	případně
i	i	k8xC	i
již	již	k6eAd1	již
existující	existující	k2eAgFnPc4d1	existující
menší	malý	k2eAgFnPc4d2	menší
černé	černý	k2eAgFnPc4d1	černá
díry	díra	k1gFnPc4	díra
<g/>
.	.	kIx.	.
</s>
<s>
Miniaturní	miniaturní	k2eAgFnSc4d1	miniaturní
a	a	k8xC	a
mikroskopické	mikroskopický	k2eAgFnSc2d1	mikroskopická
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
Proces	proces	k1gInSc4	proces
vzniku	vznik	k1gInSc2	vznik
miniaturních	miniaturní	k2eAgFnPc2d1	miniaturní
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
hypotézy	hypotéza	k1gFnSc2	hypotéza
a	a	k8xC	a
fikce	fikce	k1gFnSc2	fikce
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
existují	existovat	k5eAaImIp3nP	existovat
určité	určitý	k2eAgInPc1d1	určitý
náznaky	náznak	k1gInPc1	náznak
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
případě	případ	k1gInSc6	případ
urychlovače	urychlovač	k1gInSc2	urychlovač
s	s	k7c7	s
energií	energie	k1gFnSc7	energie
řádově	řádově	k6eAd1	řádově
TeV	TeV	k1gFnSc2	TeV
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
možno	možno	k6eAd1	možno
mikroskopickou	mikroskopický	k2eAgFnSc4d1	mikroskopická
černou	černý	k2eAgFnSc4d1	černá
díru	díra	k1gFnSc4	díra
vytvořit	vytvořit	k5eAaPmF	vytvořit
<g/>
.	.	kIx.	.
</s>
<s>
Takovým	takový	k3xDgNnSc7	takový
<g/>
,	,	kIx,	,
zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
LHC	LHC	kA	LHC
urychlovač	urychlovač	k1gInSc4	urychlovač
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
CERNu	CERNus	k1gInSc6	CERNus
uveden	uvést	k5eAaPmNgMnS	uvést
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
srážky	srážka	k1gFnSc2	srážka
těžkých	těžký	k2eAgNnPc2d1	těžké
atomových	atomový	k2eAgNnPc2d1	atomové
jader	jádro	k1gNnPc2	jádro
za	za	k7c2	za
vysoké	vysoký	k2eAgFnSc2d1	vysoká
energie	energie	k1gFnSc2	energie
existuje	existovat	k5eAaImIp3nS	existovat
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
hmota	hmota	k1gFnSc1	hmota
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
srážky	srážka	k1gFnSc2	srážka
se	se	k3xPyFc4	se
obklopí	obklopit	k5eAaPmIp3nP	obklopit
horizontem	horizont	k1gInSc7	horizont
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Takováto	takovýto	k3xDgFnSc1	takovýto
černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
obratem	obratem	k6eAd1	obratem
vypaří	vypařit	k5eAaPmIp3nS	vypařit
<g/>
.	.	kIx.	.
</s>
<s>
Vytvoření	vytvoření	k1gNnSc1	vytvoření
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
v	v	k7c6	v
urychlovačích	urychlovač	k1gInPc6	urychlovač
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
rozřešit	rozřešit	k5eAaPmF	rozřešit
tzv.	tzv.	kA	tzv.
paradox	paradox	k1gInSc4	paradox
unitarity	unitarita	k1gFnSc2	unitarita
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
otázce	otázka	k1gFnSc6	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
pádem	pád	k1gInSc7	pád
do	do	k7c2	do
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
kvantová	kvantový	k2eAgFnSc1d1	kvantová
informace	informace	k1gFnSc1	informace
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemůžeme	moct	k5eNaImIp1nP	moct
objevit	objevit	k5eAaPmF	objevit
černé	černý	k2eAgFnPc4d1	černá
díry	díra	k1gFnPc4	díra
podle	podle	k7c2	podle
světla	světlo	k1gNnSc2	světlo
vyzařovaného	vyzařovaný	k2eAgNnSc2d1	vyzařované
nebo	nebo	k8xC	nebo
odraženého	odražený	k2eAgNnSc2d1	odražené
od	od	k7c2	od
hmoty	hmota	k1gFnSc2	hmota
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
nitru	nitro	k1gNnSc6	nitro
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
objekty	objekt	k1gInPc1	objekt
však	však	k9	však
lze	lze	k6eAd1	lze
předpovědět	předpovědět	k5eAaPmF	předpovědět
pozorováním	pozorování	k1gNnSc7	pozorování
jevů	jev	k1gInPc2	jev
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
blízkosti	blízkost	k1gFnSc6	blízkost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
jevu	jev	k1gInSc2	jev
gravitační	gravitační	k2eAgFnSc2d1	gravitační
čočky	čočka	k1gFnSc2	čočka
<g/>
,	,	kIx,	,
a	a	k8xC	a
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zdánlivě	zdánlivě	k6eAd1	zdánlivě
obíhají	obíhat	k5eAaImIp3nP	obíhat
kolem	kolem	k7c2	kolem
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
není	být	k5eNaImIp3nS	být
žádná	žádný	k3yNgFnSc1	žádný
viditelná	viditelný	k2eAgFnSc1d1	viditelná
hmota	hmota	k1gFnSc1	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
pozorovatelné	pozorovatelný	k2eAgInPc1d1	pozorovatelný
projevy	projev	k1gInPc1	projev
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
způsobeny	způsobit	k5eAaPmNgFnP	způsobit
i	i	k9	i
jinými	jiný	k2eAgInPc7d1	jiný
objekty	objekt	k1gInPc7	objekt
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
není	být	k5eNaImIp3nS	být
snadné	snadný	k2eAgNnSc1d1	snadné
je	on	k3xPp3gInPc4	on
odlišit	odlišit	k5eAaPmF	odlišit
i	i	k9	i
od	od	k7c2	od
objektů	objekt	k1gInPc2	objekt
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
bílý	bílý	k2eAgMnSc1d1	bílý
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejviditelnější	viditelný	k2eAgInPc4d3	nejviditelnější
efekty	efekt	k1gInPc4	efekt
jsou	být	k5eAaImIp3nP	být
považované	považovaný	k2eAgInPc1d1	považovaný
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
hmoty	hmota	k1gFnSc2	hmota
padající	padající	k2eAgFnSc2d1	padající
do	do	k7c2	do
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hmota	hmota	k1gFnSc1	hmota
se	se	k3xPyFc4	se
dle	dle	k7c2	dle
předpovědí	předpověď	k1gFnPc2	předpověď
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
voda	voda	k1gFnSc1	voda
tekoucí	tekoucí	k2eAgFnSc1d1	tekoucí
do	do	k7c2	do
odtoku	odtok	k1gInSc2	odtok
<g/>
,	,	kIx,	,
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
do	do	k7c2	do
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
otáčejících	otáčející	k2eAgInPc2d1	otáčející
akrečních	akreční	k2eAgInPc2d1	akreční
disků	disk	k1gInPc2	disk
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
černou	černý	k2eAgFnSc7d1	černá
dírou	díra	k1gFnSc7	díra
pohlcena	pohltit	k5eAaPmNgFnS	pohltit
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
tření	tření	k1gNnSc1	tření
disk	disk	k1gInSc1	disk
extrémně	extrémně	k6eAd1	extrémně
zahřívá	zahřívat	k5eAaImIp3nS	zahřívat
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
vyzařování	vyzařování	k1gNnSc1	vyzařování
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
a	a	k8xC	a
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
je	být	k5eAaImIp3nS	být
neobyčejně	obyčejně	k6eNd1	obyčejně
účinný	účinný	k2eAgInSc1d1	účinný
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
přeměnit	přeměnit	k5eAaPmF	přeměnit
až	až	k9	až
50	[number]	k4	50
%	%	kIx~	%
zbytkové	zbytkový	k2eAgFnSc2d1	zbytková
hmoty	hmota	k1gFnSc2	hmota
na	na	k7c6	na
záření	záření	k1gNnSc6	záření
<g/>
,	,	kIx,	,
v	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
s	s	k7c7	s
termonukleární	termonukleární	k2eAgFnSc7d1	termonukleární
fúzí	fúze	k1gFnSc7	fúze
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
konvertovat	konvertovat	k5eAaBmF	konvertovat
pouze	pouze	k6eAd1	pouze
několik	několik	k4yIc4	několik
málo	málo	k4c4	málo
procent	procento	k1gNnPc2	procento
na	na	k7c4	na
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
pozorovatelné	pozorovatelný	k2eAgInPc1d1	pozorovatelný
efekty	efekt	k1gInPc1	efekt
jsou	být	k5eAaImIp3nP	být
úzké	úzký	k2eAgInPc4d1	úzký
výtrysky	výtrysk	k1gInPc4	výtrysk
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
v	v	k7c6	v
ose	osa	k1gFnSc6	osa
akrečního	akreční	k2eAgInSc2d1	akreční
disku	disk	k1gInSc2	disk
relativistickými	relativistický	k2eAgFnPc7d1	relativistická
rychlostmi	rychlost	k1gFnPc7	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Akreční	Akreční	k2eAgInPc1d1	Akreční
disky	disk	k1gInPc1	disk
<g/>
,	,	kIx,	,
výtrysky	výtrysk	k1gInPc1	výtrysk
a	a	k8xC	a
obíhající	obíhající	k2eAgInPc1d1	obíhající
objekty	objekt	k1gInPc1	objekt
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
nejen	nejen	k6eAd1	nejen
kolem	kolem	k7c2	kolem
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
okolo	okolo	k7c2	okolo
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
neutronové	neutronový	k2eAgFnPc1d1	neutronová
hvězdy	hvězda	k1gFnPc1	hvězda
a	a	k8xC	a
bílí	bílý	k2eAgMnPc1d1	bílý
trpaslíci	trpaslík	k1gMnPc1	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Dynamika	dynamika	k1gFnSc1	dynamika
těles	těleso	k1gNnPc2	těleso
okolo	okolo	k7c2	okolo
takovýchto	takovýto	k3xDgInPc2	takovýto
atraktorů	atraktor	k1gInPc2	atraktor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
nejsou	být	k5eNaImIp3nP	být
černými	černý	k2eAgFnPc7d1	černá
děrami	děra	k1gFnPc7	děra
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
dynamice	dynamika	k1gFnSc3	dynamika
objektů	objekt	k1gInPc2	objekt
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
aktivním	aktivní	k2eAgInSc7d1	aktivní
předmětem	předmět	k1gInSc7	předmět
výzkumu	výzkum	k1gInSc2	výzkum
zahrnujícím	zahrnující	k2eAgInSc7d1	zahrnující
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc4	pole
a	a	k8xC	a
plazmovou	plazmový	k2eAgFnSc4d1	plazmová
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
také	také	k9	také
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pozorování	pozorování	k1gNnSc1	pozorování
akrečního	akreční	k2eAgInSc2d1	akreční
disku	disk	k1gInSc2	disk
a	a	k8xC	a
orbitálních	orbitální	k2eAgInPc2d1	orbitální
pohybů	pohyb	k1gInPc2	pohyb
většinou	většinou	k6eAd1	většinou
pouze	pouze	k6eAd1	pouze
indikuje	indikovat	k5eAaBmIp3nS	indikovat
existenci	existence	k1gFnSc4	existence
kompaktního	kompaktní	k2eAgInSc2d1	kompaktní
objektu	objekt	k1gInSc2	objekt
s	s	k7c7	s
určitou	určitý	k2eAgFnSc7d1	určitá
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vypovídá	vypovídat	k5eAaImIp3nS	vypovídat
jen	jen	k9	jen
velice	velice	k6eAd1	velice
málo	málo	k6eAd1	málo
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
podstatě	podstata	k1gFnSc6	podstata
<g/>
.	.	kIx.	.
</s>
<s>
Identifikovat	identifikovat	k5eAaBmF	identifikovat
takový	takový	k3xDgInSc4	takový
objekt	objekt	k1gInSc4	objekt
jako	jako	k8xS	jako
černou	černý	k2eAgFnSc4d1	černá
díru	díra	k1gFnSc4	díra
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pouze	pouze	k6eAd1	pouze
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
prokáže	prokázat	k5eAaPmIp3nS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nemůže	moct	k5eNaImIp3nS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
jiné	jiný	k2eAgNnSc4d1	jiné
dostatečně	dostatečně	k6eAd1	dostatečně
hmotné	hmotný	k2eAgNnSc4d1	hmotné
a	a	k8xC	a
kompaktní	kompaktní	k2eAgNnSc4d1	kompaktní
těleso	těleso	k1gNnSc4	těleso
nebo	nebo	k8xC	nebo
provázaný	provázaný	k2eAgInSc4d1	provázaný
systém	systém	k1gInSc4	systém
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
astrofyziků	astrofyzik	k1gMnPc2	astrofyzik
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
obecné	obecný	k2eAgFnSc2d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
,	,	kIx,	,
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
koncentrace	koncentrace	k1gFnSc1	koncentrace
hmoty	hmota	k1gFnSc2	hmota
s	s	k7c7	s
dostatečnou	dostatečný	k2eAgFnSc7d1	dostatečná
hustotou	hustota	k1gFnSc7	hustota
musí	muset	k5eAaImIp3nS	muset
nevyhnutelně	vyhnutelně	k6eNd1	vyhnutelně
zhroutit	zhroutit	k5eAaPmF	zhroutit
do	do	k7c2	do
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
v	v	k7c6	v
kosmologicky	kosmologicky	k6eAd1	kosmologicky
krátkém	krátký	k2eAgInSc6d1	krátký
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
důležitý	důležitý	k2eAgInSc1d1	důležitý
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
černými	černý	k2eAgFnPc7d1	černá
děrami	děra	k1gFnPc7	děra
a	a	k8xC	a
jinými	jiný	k2eAgInPc7d1	jiný
kompaktními	kompaktní	k2eAgInPc7d1	kompaktní
objekty	objekt	k1gInPc7	objekt
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jakákoli	jakýkoli	k3yIgFnSc1	jakýkoli
kolabující	kolabující	k2eAgFnSc1d1	kolabující
hmota	hmota	k1gFnSc1	hmota
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
narazí	narazit	k5eAaPmIp3nS	narazit
na	na	k7c4	na
takový	takový	k3xDgInSc4	takový
kompaktní	kompaktní	k2eAgInSc4d1	kompaktní
hmotný	hmotný	k2eAgInSc4d1	hmotný
objekt	objekt	k1gInSc4	objekt
v	v	k7c6	v
relativistické	relativistický	k2eAgFnSc6d1	relativistická
rychlosti	rychlost	k1gFnSc6	rychlost
<g/>
,	,	kIx,	,
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
nepravidelná	pravidelný	k2eNgNnPc4d1	nepravidelné
vzplanutí	vzplanutí	k1gNnPc4	vzplanutí
rentgenového	rentgenový	k2eAgNnSc2d1	rentgenové
záření	záření	k1gNnSc2	záření
nebo	nebo	k8xC	nebo
jiného	jiný	k2eAgNnSc2d1	jiné
tvrdého	tvrdý	k2eAgNnSc2d1	tvrdé
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1	nedostatek
takovýchto	takovýto	k3xDgNnPc2	takovýto
vzplanutí	vzplanutí	k1gNnPc2	vzplanutí
kolem	kolem	k7c2	kolem
kompaktní	kompaktní	k2eAgFnSc2d1	kompaktní
koncentrace	koncentrace	k1gFnSc2	koncentrace
hmoty	hmota	k1gFnSc2	hmota
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
důkaz	důkaz	k1gInSc4	důkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
objekt	objekt	k1gInSc1	objekt
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
bez	bez	k7c2	bez
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
hmota	hmota	k1gFnSc1	hmota
náhle	náhle	k6eAd1	náhle
narazit	narazit	k5eAaPmF	narazit
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
celý	celý	k2eAgInSc1d1	celý
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
správně	správně	k6eAd1	správně
nazývá	nazývat	k5eAaImIp3nS	nazývat
gravitační	gravitační	k2eAgInSc1d1	gravitační
mikročočkový	mikročočkový	k2eAgInSc1d1	mikročočkový
efekt	efekt	k1gInSc1	efekt
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
obraz	obraz	k1gInSc4	obraz
zakřivila	zakřivit	k5eAaPmAgFnS	zakřivit
<g />
.	.	kIx.	.
</s>
<s>
jen	jen	k9	jen
jedna	jeden	k4xCgFnSc1	jeden
černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
gravitační	gravitační	k2eAgFnSc1d1	gravitační
čočka	čočka	k1gFnSc1	čočka
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
mnohdy	mnohdy	k6eAd1	mnohdy
více	hodně	k6eAd2	hodně
galaxiemi	galaxie	k1gFnPc7	galaxie
<g/>
)	)	kIx)	)
Dnes	dnes	k6eAd1	dnes
evidujeme	evidovat	k5eAaImIp1nP	evidovat
mnoho	mnoho	k4c1	mnoho
nepřímých	přímý	k2eNgInPc2d1	nepřímý
důkazů	důkaz	k1gInPc2	důkaz
astronomických	astronomický	k2eAgNnPc2d1	astronomické
pozorování	pozorování	k1gNnPc2	pozorování
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
hmotnostních	hmotnostní	k2eAgNnPc6d1	hmotnostní
pásmech	pásmo	k1gNnPc6	pásmo
<g/>
:	:	kIx,	:
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
typické	typický	k2eAgFnSc2d1	typická
hvězdy	hvězda	k1gFnSc2	hvězda
(	(	kIx(	(
<g/>
4	[number]	k4	4
–	–	k?	–
15	[number]	k4	15
hmotností	hmotnost	k1gFnPc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
<g/>
)	)	kIx)	)
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
střední	střední	k2eAgFnSc2d1	střední
hmotnosti	hmotnost	k1gFnSc2	hmotnost
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
kolem	kolem	k7c2	kolem
1000	[number]	k4	1000
Sluncí	slunce	k1gNnPc2	slunce
obří	obří	k2eAgFnSc2d1	obří
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
v	v	k7c6	v
řádech	řád	k1gInPc6	řád
od	od	k7c2	od
105	[number]	k4	105
do	do	k7c2	do
1010	[number]	k4	1010
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
Také	také	k9	také
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
důkazů	důkaz	k1gInPc2	důkaz
o	o	k7c6	o
černých	černý	k2eAgFnPc6d1	černá
dírách	díra	k1gFnPc6	díra
se	s	k7c7	s
střední	střední	k2eAgFnSc7d1	střední
hmotností	hmotnost	k1gFnSc7	hmotnost
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
několika	několik	k4yIc2	několik
set	sto	k4xCgNnPc2	sto
po	po	k7c4	po
tisíce	tisíc	k4xCgInPc4	tisíc
hmotností	hmotnost	k1gFnPc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
vznikají	vznikat	k5eAaImIp3nP	vznikat
obří	obří	k2eAgFnPc1d1	obří
černé	černý	k2eAgFnPc1d1	černá
díry	díra	k1gFnPc1	díra
<g/>
.	.	kIx.	.
</s>
<s>
Kandidáti	kandidát	k1gMnPc1	kandidát
na	na	k7c4	na
černé	černý	k2eAgFnPc4d1	černá
díry	díra	k1gFnPc4	díra
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
byli	být	k5eAaImAgMnP	být
identifikováni	identifikován	k2eAgMnPc1d1	identifikován
hlavně	hlavně	k6eAd1	hlavně
přítomností	přítomnost	k1gFnSc7	přítomnost
akrečních	akreční	k2eAgInPc2d1	akreční
disků	disk	k1gInPc2	disk
správné	správný	k2eAgFnSc2d1	správná
velikosti	velikost	k1gFnSc2	velikost
a	a	k8xC	a
rychlosti	rychlost	k1gFnSc2	rychlost
bez	bez	k7c2	bez
nepravidelných	pravidelný	k2eNgNnPc2d1	nepravidelné
vzplanutí	vzplanutí	k1gNnPc2	vzplanutí
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
očekávané	očekávaný	k2eAgInPc1d1	očekávaný
u	u	k7c2	u
akrečních	akreční	k2eAgInPc2d1	akreční
disků	disk	k1gInPc2	disk
při	při	k7c6	při
ostatních	ostatní	k2eAgInPc6d1	ostatní
kompaktních	kompaktní	k2eAgInPc6d1	kompaktní
objektech	objekt	k1gInPc6	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Černé	Černé	k2eAgFnPc1d1	Černé
díry	díra	k1gFnPc1	díra
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
hvězd	hvězda	k1gFnPc2	hvězda
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgInP	moct
zapříčinit	zapříčinit	k5eAaPmF	zapříčinit
výbuchy	výbuch	k1gInPc1	výbuch
gama	gama	k1gNnSc2	gama
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
pozorování	pozorování	k1gNnSc1	pozorování
takovýchto	takovýto	k3xDgInPc2	takovýto
výbuchů	výbuch	k1gInPc2	výbuch
u	u	k7c2	u
supernov	supernova	k1gFnPc2	supernova
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
snížilo	snížit	k5eAaPmAgNnS	snížit
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
tohoto	tento	k3xDgNnSc2	tento
spojení	spojení	k1gNnSc2	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Kandidáti	kandidát	k1gMnPc1	kandidát
na	na	k7c6	na
obří	obří	k2eAgFnSc6d1	obří
černé	černá	k1gFnSc6	černá
díry	díra	k1gFnSc2	díra
byli	být	k5eAaImAgMnP	být
nejdříve	dříve	k6eAd3	dříve
poskytnuti	poskytnout	k5eAaPmNgMnP	poskytnout
aktivními	aktivní	k2eAgInPc7d1	aktivní
galaktickými	galaktický	k2eAgNnPc7d1	Galaktické
jádry	jádro	k1gNnPc7	jádro
a	a	k8xC	a
kvasary	kvasar	k1gInPc7	kvasar
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
objevili	objevit	k5eAaPmAgMnP	objevit
radioamatéři	radioamatér	k1gMnPc1	radioamatér
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Výkonná	výkonný	k2eAgFnSc1d1	výkonná
přeměna	přeměna	k1gFnSc1	přeměna
hmoty	hmota	k1gFnSc2	hmota
na	na	k7c4	na
energii	energie	k1gFnSc4	energie
třením	tření	k1gNnSc7	tření
v	v	k7c6	v
akrečních	akreční	k2eAgInPc6d1	akreční
discích	disk	k1gInPc6	disk
okolo	okolo	k7c2	okolo
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
jediným	jediný	k2eAgNnSc7d1	jediné
vysvětlením	vysvětlení	k1gNnSc7	vysvětlení
pro	pro	k7c4	pro
vydatné	vydatný	k2eAgNnSc4d1	vydatné
množství	množství	k1gNnSc4	množství
energie	energie	k1gFnSc2	energie
generované	generovaný	k2eAgFnSc2d1	generovaná
těmito	tento	k3xDgInPc7	tento
objekty	objekt	k1gInPc7	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Uvedení	uvedení	k1gNnSc4	uvedení
této	tento	k3xDgFnSc2	tento
teorie	teorie	k1gFnSc2	teorie
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
odstranilo	odstranit	k5eAaPmAgNnS	odstranit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
námitku	námitka	k1gFnSc4	námitka
domněnky	domněnka	k1gFnSc2	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvasary	kvasar	k1gInPc4	kvasar
jsou	být	k5eAaImIp3nP	být
vzdálenými	vzdálený	k2eAgFnPc7d1	vzdálená
galaxiemi	galaxie	k1gFnPc7	galaxie
–	–	k?	–
totiž	totiž	k9	totiž
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádný	žádný	k3yNgInSc1	žádný
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
mechanizmus	mechanizmus	k1gInSc1	mechanizmus
nemůže	moct	k5eNaImIp3nS	moct
generovat	generovat	k5eAaImF	generovat
takové	takový	k3xDgNnSc4	takový
množství	množství	k1gNnSc4	množství
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnSc1	pozorování
pohybů	pohyb	k1gInPc2	pohyb
hvězd	hvězda	k1gFnPc2	hvězda
okolo	okolo	k7c2	okolo
galaktických	galaktický	k2eAgNnPc2d1	Galaktické
center	centrum	k1gNnPc2	centrum
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
všeobecnému	všeobecný	k2eAgNnSc3d1	všeobecné
přesvědčení	přesvědčení	k1gNnSc3	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
obří	obří	k2eAgFnPc1d1	obří
černé	černý	k2eAgFnPc1d1	černá
díry	díra	k1gFnPc1	díra
existují	existovat	k5eAaImIp3nP	existovat
v	v	k7c6	v
centrech	centrum	k1gNnPc6	centrum
většiny	většina	k1gFnSc2	většina
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
naší	náš	k3xOp1gFnSc2	náš
domovské	domovský	k2eAgFnSc2d1	domovská
Mléčné	mléčný	k2eAgFnSc2d1	mléčná
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Sagittarius	Sagittarius	k1gInSc1	Sagittarius
A	a	k9	a
<g/>
*	*	kIx~	*
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
shodně	shodně	k6eAd1	shodně
považovaný	považovaný	k2eAgMnSc1d1	považovaný
za	za	k7c4	za
věrohodného	věrohodný	k2eAgMnSc4d1	věrohodný
kandidáta	kandidát	k1gMnSc4	kandidát
pro	pro	k7c4	pro
polohu	poloha	k1gFnSc4	poloha
obří	obří	k2eAgFnSc2d1	obří
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
Mléčné	mléčný	k2eAgFnSc2d1	mléčná
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
představa	představa	k1gFnSc1	představa
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgFnPc1	všechen
galaxie	galaxie	k1gFnPc1	galaxie
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
mít	mít	k5eAaImF	mít
obří	obří	k2eAgFnSc4d1	obří
díru	díra	k1gFnSc4	díra
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
středech	střed	k1gInPc6	střed
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
plyn	plyn	k1gInSc4	plyn
a	a	k8xC	a
prach	prach	k1gInSc4	prach
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
generuje	generovat	k5eAaImIp3nS	generovat
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
záření	záření	k1gNnSc2	záření
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
pohltí	pohltit	k5eAaPmIp3nS	pohltit
všechnu	všechen	k3xTgFnSc4	všechen
okolní	okolní	k2eAgFnSc4d1	okolní
hmotu	hmota	k1gFnSc4	hmota
a	a	k8xC	a
proces	proces	k1gInSc1	proces
se	se	k3xPyFc4	se
zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
představa	představa	k1gFnSc1	představa
také	také	k9	také
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
neexistují	existovat	k5eNaImIp3nP	existovat
žádné	žádný	k3yNgInPc1	žádný
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
blízké	blízký	k2eAgMnPc4d1	blízký
kvasary	kvasar	k1gInPc4	kvasar
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
detaily	detail	k1gInPc1	detail
ještě	ještě	k6eAd1	ještě
nejsou	být	k5eNaImIp3nP	být
úplně	úplně	k6eAd1	úplně
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zdá	zdát	k5eAaImIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
růst	růst	k1gInSc1	růst
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
má	mít	k5eAaImIp3nS	mít
spojitost	spojitost	k1gFnSc1	spojitost
s	s	k7c7	s
růstem	růst	k1gInSc7	růst
kulovité	kulovitý	k2eAgFnSc2d1	kulovitá
části	část	k1gFnSc2	část
–	–	k?	–
eliptická	eliptický	k2eAgFnSc1d1	eliptická
galaxie	galaxie	k1gFnSc1	galaxie
nebo	nebo	k8xC	nebo
vypouklina	vypouklina	k1gFnSc1	vypouklina
ve	v	k7c6	v
spirální	spirální	k2eAgFnSc6d1	spirální
galaxii	galaxie	k1gFnSc6	galaxie
–	–	k?	–
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
neexistuje	existovat	k5eNaImIp3nS	existovat
důkaz	důkaz	k1gInSc1	důkaz
pro	pro	k7c4	pro
hmotné	hmotný	k2eAgFnPc4d1	hmotná
černé	černý	k2eAgFnPc4d1	černá
díry	díra	k1gFnPc4	díra
ve	v	k7c6	v
středech	střed	k1gInPc6	střed
uzavřených	uzavřený	k2eAgFnPc2d1	uzavřená
hvězdokup	hvězdokupa	k1gFnPc2	hvězdokupa
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
fundamentální	fundamentální	k2eAgFnSc4d1	fundamentální
odlišnost	odlišnost	k1gFnSc4	odlišnost
od	od	k7c2	od
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Sagittarius	Sagittarius	k1gMnSc1	Sagittarius
A	A	kA	A
<g/>
*	*	kIx~	*
<g/>
,	,	kIx,	,
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
naší	náš	k3xOp1gFnSc2	náš
galaxie	galaxie	k1gFnSc2	galaxie
Mléčné	mléčný	k2eAgFnSc2d1	mléčná
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc1	několik
bližších	blízký	k2eAgMnPc2d2	bližší
kandidátů	kandidát	k1gMnPc2	kandidát
na	na	k7c4	na
černé	černý	k2eAgFnPc4d1	černá
díry	díra	k1gFnPc4	díra
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
jsou	být	k5eAaImIp3nP	být
binární	binární	k2eAgInPc1d1	binární
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vysávají	vysávat	k5eAaImIp3nP	vysávat
hmotu	hmota	k1gFnSc4	hmota
z	z	k7c2	z
partnera	partner	k1gMnSc2	partner
přes	přes	k7c4	přes
akreční	akreční	k2eAgInSc4d1	akreční
disk	disk	k1gInSc4	disk
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
od	od	k7c2	od
tří	tři	k4xCgInPc2	tři
do	do	k7c2	do
několika	několik	k4yIc2	několik
desítek	desítka	k1gFnPc2	desítka
hmotností	hmotnost	k1gFnPc2	hmotnost
slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
klasické	klasický	k2eAgFnSc2d1	klasická
fyziky	fyzika	k1gFnSc2	fyzika
je	být	k5eAaImIp3nS	být
těleso	těleso	k1gNnSc1	těleso
velmi	velmi	k6eAd1	velmi
stálé	stálý	k2eAgNnSc1d1	stálé
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nemůže	moct	k5eNaImIp3nS	moct
zaniknout	zaniknout	k5eAaPmF	zaniknout
vlivem	vlivem	k7c2	vlivem
ztráty	ztráta	k1gFnSc2	ztráta
své	svůj	k3xOyFgFnSc2	svůj
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
žádná	žádný	k3yNgFnSc1	žádný
částice	částice	k1gFnSc1	částice
není	být	k5eNaImIp3nS	být
podle	podle	k7c2	podle
klasické	klasický	k2eAgFnSc2d1	klasická
fyziky	fyzika	k1gFnSc2	fyzika
schopna	schopen	k2eAgFnSc1d1	schopna
překonat	překonat	k5eAaPmF	překonat
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
jedinou	jediný	k2eAgFnSc7d1	jediná
přirozenou	přirozený	k2eAgFnSc7d1	přirozená
cestou	cesta	k1gFnSc7	cesta
zániku	zánik	k1gInSc2	zánik
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
jeví	jevit	k5eAaImIp3nS	jevit
její	její	k3xOp3gNnSc4	její
pohlcení	pohlcení	k1gNnSc4	pohlcení
jinou	jiný	k2eAgFnSc7d1	jiná
černou	černý	k2eAgFnSc7d1	černá
dírou	díra	k1gFnSc7	díra
tzv.	tzv.	kA	tzv.
gravitační	gravitační	k2eAgFnSc1d1	gravitační
srážka	srážka	k1gFnSc1	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
na	na	k7c4	na
systém	systém	k1gInSc4	systém
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
z	z	k7c2	z
moderní	moderní	k2eAgFnSc2d1	moderní
fyziky	fyzika	k1gFnSc2	fyzika
však	však	k9	však
vyvstává	vyvstávat	k5eAaImIp3nS	vyvstávat
další	další	k2eAgFnSc4d1	další
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
může	moct	k5eAaImIp3nS	moct
černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
zaniknout	zaniknout	k5eAaPmF	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
mechanika	mechanika	k1gFnSc1	mechanika
připouští	připouštět	k5eAaImIp3nS	připouštět
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgNnSc6	který
vlivem	vliv	k1gInSc7	vliv
neurčitosti	neurčitost	k1gFnSc2	neurčitost
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
částic	částice	k1gFnPc2	částice
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
neustále	neustále	k6eAd1	neustále
vznikají	vznikat	k5eAaImIp3nP	vznikat
a	a	k8xC	a
opět	opět	k6eAd1	opět
zanikají	zanikat	k5eAaImIp3nP	zanikat
páry	pára	k1gFnSc2	pára
částice-antičástice	částicentičástika	k1gFnSc3	částice-antičástika
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
takto	takto	k6eAd1	takto
vzniklých	vzniklý	k2eAgFnPc2d1	vzniklá
částic	částice	k1gFnPc2	částice
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
nad	nad	k7c7	nad
horizontem	horizont	k1gInSc7	horizont
událostí	událost	k1gFnPc2	událost
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
pod	pod	k7c7	pod
ním	on	k3xPp3gMnSc7	on
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
první	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
uniknout	uniknout	k5eAaPmF	uniknout
do	do	k7c2	do
nekonečna	nekonečno	k1gNnSc2	nekonečno
a	a	k8xC	a
snížit	snížit	k5eAaPmF	snížit
tím	ten	k3xDgInSc7	ten
hmotnost	hmotnost	k1gFnSc1	hmotnost
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Hawkingovo	Hawkingův	k2eAgNnSc1d1	Hawkingovo
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
kvantové	kvantový	k2eAgNnSc1d1	kvantové
vypařování	vypařování	k1gNnSc1	vypařování
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
popsán	popsat	k5eAaPmNgInS	popsat
britským	britský	k2eAgMnSc7d1	britský
astrofyzikem	astrofyzik	k1gMnSc7	astrofyzik
Stephenem	Stephen	k1gMnSc7	Stephen
Hawkingem	Hawking	k1gInSc7	Hawking
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
dvě	dva	k4xCgFnPc1	dva
černé	černý	k2eAgFnPc1d1	černá
díry	díra	k1gFnPc1	díra
gravitačně	gravitačně	k6eAd1	gravitačně
vázané	vázaný	k2eAgFnPc1d1	vázaná
a	a	k8xC	a
obíhají	obíhat	k5eAaImIp3nP	obíhat
dostatečně	dostatečně	k6eAd1	dostatečně
blízko	blízko	k7c2	blízko
společného	společný	k2eAgNnSc2d1	společné
těžiště	těžiště	k1gNnSc2	těžiště
<g/>
,	,	kIx,	,
vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
podle	podle	k7c2	podle
předpovědi	předpověď	k1gFnSc2	předpověď
obecné	obecný	k2eAgFnSc2d1	obecná
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
gravitační	gravitační	k2eAgFnSc2d1	gravitační
vlny	vlna	k1gFnSc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
soustava	soustava	k1gFnSc1	soustava
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
energii	energie	k1gFnSc4	energie
a	a	k8xC	a
černé	černý	k2eAgFnPc1d1	černá
díry	díra	k1gFnPc1	díra
se	se	k3xPyFc4	se
k	k	k7c3	k
sobě	se	k3xPyFc3	se
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
přibližují	přibližovat	k5eAaImIp3nP	přibližovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
momentu	moment	k1gInSc6	moment
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
gravitační	gravitační	k2eAgFnSc1d1	gravitační
interakce	interakce	k1gFnSc1	interakce
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
silná	silný	k2eAgFnSc1d1	silná
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
horizonty	horizont	k1gInPc1	horizont
černých	černá	k1gFnPc2	černá
děr	děra	k1gFnPc2	děra
začnou	začít	k5eAaPmIp3nP	začít
deformovat	deformovat	k5eAaImF	deformovat
<g/>
,	,	kIx,	,
až	až	k8xS	až
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
díry	díra	k1gFnPc1	díra
setkají	setkat	k5eAaPmIp3nP	setkat
a	a	k8xC	a
spojí	spojit	k5eAaPmIp3nP	spojit
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
plocha	plocha	k1gFnSc1	plocha
jejich	jejich	k3xOp3gInPc2	jejich
horizontů	horizont	k1gInPc2	horizont
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
v	v	k7c6	v
souhlasu	souhlas	k1gInSc6	souhlas
s	s	k7c7	s
termodynamikou	termodynamika	k1gFnSc7	termodynamika
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
zvětší	zvětšit	k5eAaPmIp3nS	zvětšit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
procesu	proces	k1gInSc6	proces
se	se	k3xPyFc4	se
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
velký	velký	k2eAgInSc1d1	velký
náboj	náboj	k1gInSc1	náboj
a	a	k8xC	a
především	především	k9	především
moment	moment	k1gInSc4	moment
hybnosti	hybnost	k1gFnSc2	hybnost
<g/>
,	,	kIx,	,
výsledná	výsledný	k2eAgFnSc1d1	výsledná
černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
<g/>
,	,	kIx,	,
za	za	k7c4	za
velmi	velmi	k6eAd1	velmi
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
vyzáří	vyzářit	k5eAaPmIp3nS	vyzářit
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
energie	energie	k1gFnSc2	energie
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
gravitačních	gravitační	k2eAgFnPc2d1	gravitační
vln	vlna	k1gFnPc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
srážky	srážka	k1gFnPc1	srážka
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
zachytit	zachytit	k5eAaPmF	zachytit
pomocí	pomocí	k7c2	pomocí
detektorů	detektor	k1gInPc2	detektor
gravitačních	gravitační	k2eAgFnPc2d1	gravitační
vln	vlna	k1gFnPc2	vlna
a	a	k8xC	a
po	po	k7c6	po
kterém	který	k3yRgNnSc6	který
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
usilovně	usilovně	k6eAd1	usilovně
pátrá	pátrat	k5eAaImIp3nS	pátrat
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
nelinearita	nelinearita	k1gFnSc1	nelinearita
Einsteinových	Einsteinových	k2eAgFnPc2d1	Einsteinových
rovnic	rovnice	k1gFnPc2	rovnice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zaručuje	zaručovat	k5eAaImIp3nS	zaručovat
stabilitu	stabilita	k1gFnSc4	stabilita
původních	původní	k2eAgFnPc2d1	původní
i	i	k8xC	i
výsledné	výsledný	k2eAgFnSc2d1	výsledná
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
také	také	k9	také
zdrojem	zdroj	k1gInSc7	zdroj
dosud	dosud	k6eAd1	dosud
nepřekonaných	překonaný	k2eNgFnPc2d1	nepřekonaná
potíží	potíž	k1gFnPc2	potíž
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
analytického	analytický	k2eAgMnSc2d1	analytický
i	i	k8xC	i
numerického	numerický	k2eAgNnSc2d1	numerické
řešení	řešení	k1gNnSc2	řešení
popisujícího	popisující	k2eAgNnSc2d1	popisující
srážku	srážka	k1gFnSc4	srážka
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
přesný	přesný	k2eAgInSc1d1	přesný
postup	postup	k1gInSc1	postup
zániku	zánik	k1gInSc2	zánik
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
a	a	k8xC	a
nebyl	být	k5eNaImAgInS	být
ještě	ještě	k6eAd1	ještě
ani	ani	k8xC	ani
pozorován	pozorovat	k5eAaImNgInS	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dosavadních	dosavadní	k2eAgInPc2d1	dosavadní
numerických	numerický	k2eAgInPc2d1	numerický
modelů	model	k1gInPc2	model
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
nerotující	rotující	k2eNgFnPc4d1	nerotující
černé	černý	k2eAgFnPc4d1	černá
díry	díra	k1gFnPc4	díra
o	o	k7c6	o
stejné	stejný	k2eAgFnSc6d1	stejná
hmotnosti	hmotnost	k1gFnSc6	hmotnost
se	se	k3xPyFc4	se
při	při	k7c6	při
srážce	srážka	k1gFnSc6	srážka
na	na	k7c4	na
gravitační	gravitační	k2eAgFnPc4d1	gravitační
vlny	vlna	k1gFnPc4	vlna
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
až	až	k9	až
3	[number]	k4	3
%	%	kIx~	%
jejich	jejich	k3xOp3gFnSc2	jejich
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
kvantové	kvantový	k2eAgFnSc2d1	kvantová
fyziky	fyzika	k1gFnSc2	fyzika
existuje	existovat	k5eAaImIp3nS	existovat
další	další	k2eAgFnSc4d1	další
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
může	moct	k5eAaImIp3nS	moct
zaniknout	zaniknout	k5eAaPmF	zaniknout
-	-	kIx~	-
pomocí	pomocí	k7c2	pomocí
tzv.	tzv.	kA	tzv.
Hawkingova	Hawkingův	k2eAgNnSc2d1	Hawkingovo
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
klasické	klasický	k2eAgFnSc2d1	klasická
fyziky	fyzika	k1gFnSc2	fyzika
může	moct	k5eAaImIp3nS	moct
černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
tělesa	těleso	k1gNnSc2	těleso
i	i	k8xC	i
záření	záření	k1gNnSc2	záření
absorbovat	absorbovat	k5eAaBmF	absorbovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nemůže	moct	k5eNaImIp3nS	moct
nic	nic	k3yNnSc1	nic
vyzařovat	vyzařovat	k5eAaImF	vyzařovat
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
by	by	kYmCp3nS	by
její	její	k3xOp3gFnSc1	její
teplota	teplota	k1gFnSc1	teplota
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
rovna	roven	k2eAgFnSc1d1	rovna
absolutní	absolutní	k2eAgFnSc3d1	absolutní
nule	nula	k1gFnSc3	nula
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
velikosti	velikost	k1gFnSc6	velikost
gravitace	gravitace	k1gFnSc2	gravitace
na	na	k7c6	na
myšleném	myšlený	k2eAgInSc6d1	myšlený
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znemožňuje	znemožňovat	k5eAaImIp3nS	znemožňovat
černé	černý	k2eAgFnSc3d1	černá
díře	díra	k1gFnSc3	díra
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
termodynamické	termodynamický	k2eAgFnPc4d1	termodynamická
rovnováhy	rovnováha	k1gFnPc4	rovnováha
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
vyslovil	vyslovit	k5eAaPmAgInS	vyslovit
Stephen	Stephen	k2eAgInSc1d1	Stephen
Hawking	Hawking	k1gInSc1	Hawking
hypotézu	hypotéza	k1gFnSc4	hypotéza
kvantového	kvantový	k2eAgNnSc2d1	kvantové
vypařování	vypařování	k1gNnSc2	vypařování
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
je	být	k5eAaImIp3nS	být
každá	každý	k3xTgFnSc1	každý
černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
schopna	schopen	k2eAgFnSc1d1	schopna
spontánně	spontánně	k6eAd1	spontánně
emitovat	emitovat	k5eAaBmF	emitovat
záření	záření	k1gNnSc4	záření
přesně	přesně	k6eAd1	přesně
takové	takový	k3xDgNnSc4	takový
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
kdyby	kdyby	kYmCp3nS	kdyby
byla	být	k5eAaImAgFnS	být
obyčejným	obyčejný	k2eAgNnSc7d1	obyčejné
černým	černý	k2eAgNnSc7d1	černé
tělesem	těleso	k1gNnSc7	těleso
zahřátým	zahřátý	k2eAgNnSc7d1	zahřáté
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
TH	TH	kA	TH
<g/>
=	=	kIx~	=
κ	κ	k?	κ
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
π	π	k?	π
úměrnou	úměrná	k1gFnSc4	úměrná
povrchové	povrchový	k2eAgFnSc3d1	povrchová
gravitaci	gravitace	k1gFnSc3	gravitace
κ	κ	k?	κ
<g />
.	.	kIx.	.
</s>
<s>
na	na	k7c6	na
horizontu	horizont	k1gInSc6	horizont
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Vzorec	vzorec	k1gInSc1	vzorec
platí	platit	k5eAaImIp3nS	platit
v	v	k7c6	v
Planckových	Planckův	k2eAgFnPc6d1	Planckova
jednotkách	jednotka	k1gFnPc6	jednotka
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Hawking	Hawking	k1gInSc1	Hawking
svým	svůj	k3xOyFgInSc7	svůj
výpočtem	výpočet	k1gInSc7	výpočet
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
úplně	úplně	k6eAd1	úplně
černá	černý	k2eAgFnSc1d1	černá
a	a	k8xC	a
že	že	k8xS	že
kvantové	kvantový	k2eAgInPc4d1	kvantový
zákony	zákon	k1gInPc4	zákon
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c6	na
horizontu	horizont	k1gInSc6	horizont
událostí	událost	k1gFnPc2	událost
neustále	neustále	k6eAd1	neustále
rodí	rodit	k5eAaImIp3nP	rodit
nové	nový	k2eAgFnPc1d1	nová
částice	částice	k1gFnPc1	částice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
odnášejí	odnášet	k5eAaImIp3nP	odnášet
část	část	k1gFnSc4	část
energie	energie	k1gFnSc2	energie
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
pryč	pryč	k6eAd1	pryč
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
zmenšují	zmenšovat	k5eAaImIp3nP	zmenšovat
hmotnost	hmotnost	k1gFnSc4	hmotnost
díry	díra	k1gFnSc2	díra
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
pozorování	pozorování	k1gNnPc1	pozorování
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
spektru	spektrum	k1gNnSc6	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Únik	únik	k1gInSc1	únik
částic	částice	k1gFnPc2	částice
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
pozvolný	pozvolný	k2eAgMnSc1d1	pozvolný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
získává	získávat	k5eAaImIp3nS	získávat
proces	proces	k1gInSc1	proces
na	na	k7c6	na
dynamičnosti	dynamičnost	k1gFnSc6	dynamičnost
až	až	k9	až
na	na	k7c6	na
konci	konec	k1gInSc6	konec
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
explozi	exploze	k1gFnSc3	exploze
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
se	se	k3xPyFc4	se
vypaří	vypařit	k5eAaPmIp3nS	vypařit
přibližně	přibližně	k6eAd1	přibližně
za	za	k7c4	za
1067	[number]	k4	1067
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
současným	současný	k2eAgNnSc7d1	současné
stářím	stáří	k1gNnSc7	stáří
vesmíru	vesmír	k1gInSc2	vesmír
1,37	[number]	k4	1,37
<g/>
×	×	k?	×
<g/>
1010	[number]	k4	1010
let	léto	k1gNnPc2	léto
skutečně	skutečně	k6eAd1	skutečně
velmi	velmi	k6eAd1	velmi
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
doba	doba	k1gFnSc1	doba
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
kvantového	kvantový	k2eAgNnSc2d1	kvantové
vyzařování	vyzařování	k1gNnSc2	vyzařování
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc1	proces
kreace	kreace	k1gFnSc2	kreace
páru	pára	k1gFnSc4	pára
částice-antičástice	částicentičástika	k1gFnSc3	částice-antičástika
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yRgInSc3	který
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
horizontu	horizont	k1gInSc2	horizont
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vytvářet	vytvářet	k5eAaImF	vytvářet
elementární	elementární	k2eAgFnPc1d1	elementární
částice	částice	k1gFnPc1	částice
na	na	k7c6	na
základě	základ	k1gInSc6	základ
kvantových	kvantový	k2eAgInPc2d1	kvantový
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
běžně	běžně	k6eAd1	běžně
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
neustálému	neustálý	k2eAgInSc3d1	neustálý
vzniku	vznik	k1gInSc3	vznik
a	a	k8xC	a
zániku	zánik	k1gInSc3	zánik
párů	pár	k1gInPc2	pár
částice-antičástice	částicentičástika	k1gFnSc3	částice-antičástika
(	(	kIx(	(
<g/>
fluktuace	fluktuace	k1gFnSc1	fluktuace
počtu	počet	k1gInSc2	počet
částic	částice	k1gFnPc2	částice
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
kvůli	kvůli	k7c3	kvůli
relacím	relace	k1gFnPc3	relace
neurčitosti	neurčitost	k1gFnSc2	neurčitost
nenulové	nulový	k2eNgFnPc1d1	nenulová
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
právě	právě	k9	právě
tvorbou	tvorba	k1gFnSc7	tvorba
těchto	tento	k3xDgInPc2	tento
párů	pár	k1gInPc2	pár
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
horizontu	horizont	k1gInSc2	horizont
událostí	událost	k1gFnPc2	událost
může	moct	k5eAaImIp3nS	moct
nastat	nastat	k5eAaPmF	nastat
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
z	z	k7c2	z
páru	pár	k1gInSc2	pár
zůstane	zůstat	k5eAaPmIp3nS	zůstat
jedna	jeden	k4xCgFnSc1	jeden
částice	částice	k1gFnSc1	částice
nad	nad	k7c7	nad
horizontem	horizont	k1gInSc7	horizont
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
druhá	druhý	k4xOgFnSc1	druhý
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
pod	pod	k7c7	pod
horizontem	horizont	k1gInSc7	horizont
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
tedy	tedy	k9	tedy
nevyhnutelně	vyhnutelně	k6eNd1	vyhnutelně
spadnout	spadnout	k5eAaPmF	spadnout
do	do	k7c2	do
singularity	singularita	k1gFnSc2	singularita
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
první	první	k4xOgFnSc1	první
částice	částice	k1gFnSc1	částice
unikne	uniknout	k5eAaPmIp3nS	uniknout
mimo	mimo	k7c4	mimo
dosah	dosah	k1gInSc4	dosah
gravitace	gravitace	k1gFnSc2	gravitace
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
,	,	kIx,	,
pozorovateli	pozorovatel	k1gMnSc6	pozorovatel
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
horizontu	horizont	k1gInSc2	horizont
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
částice	částice	k1gFnSc1	částice
vzniká	vznikat	k5eAaImIp3nS	vznikat
jakoby	jakoby	k8xS	jakoby
z	z	k7c2	z
ničeho	nic	k3yNnSc2	nic
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
horizontu	horizont	k1gInSc2	horizont
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
potřebná	potřebný	k2eAgFnSc1d1	potřebná
na	na	k7c6	na
vytvoření	vytvoření	k1gNnSc6	vytvoření
částice	částice	k1gFnSc2	částice
ubude	ubýt	k5eAaPmIp3nS	ubýt
z	z	k7c2	z
hmoty	hmota	k1gFnSc2	hmota
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
mnoho	mnoho	k4c1	mnoho
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
vypracování	vypracování	k1gNnSc3	vypracování
nové	nový	k2eAgFnSc2d1	nová
teorie	teorie	k1gFnSc2	teorie
rozšíření	rozšíření	k1gNnSc2	rozšíření
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
existuje	existovat	k5eAaImIp3nS	existovat
takřka	takřka	k6eAd1	takřka
pětkrát	pětkrát	k6eAd1	pětkrát
více	hodně	k6eAd2	hodně
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
než	než	k8xS	než
se	se	k3xPyFc4	se
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2004	[number]	k4	2004
astronomové	astronom	k1gMnPc1	astronom
objevili	objevit	k5eAaPmAgMnP	objevit
obří	obří	k2eAgFnSc4d1	obří
černou	černý	k2eAgFnSc4d1	černá
díru	díra	k1gFnSc4	díra
Q	Q	kA	Q
<g/>
0	[number]	k4	0
<g/>
906	[number]	k4	906
<g/>
+	+	kIx~	+
<g/>
6930	[number]	k4	6930
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
vzdálené	vzdálený	k2eAgFnSc2d1	vzdálená
galaxie	galaxie	k1gFnSc2	galaxie
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Velké	velký	k2eAgFnSc2d1	velká
medvědice	medvědice	k1gFnSc2	medvědice
<g/>
.	.	kIx.	.
</s>
<s>
Odhad	odhad	k1gInSc1	odhad
věku	věk	k1gInSc2	věk
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc2	hmotnost
takových	takový	k3xDgFnPc2	takový
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
nám	my	k3xPp1nPc3	my
může	moct	k5eAaImIp3nS	moct
pomoct	pomoct	k5eAaPmF	pomoct
určit	určit	k5eAaPmF	určit
věk	věk	k1gInSc4	věk
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2004	[number]	k4	2004
tým	tým	k1gInSc4	tým
astronomů	astronom	k1gMnPc2	astronom
oznámil	oznámit	k5eAaPmAgMnS	oznámit
objev	objev	k1gInSc4	objev
první	první	k4xOgFnSc2	první
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
střední	střední	k2eAgFnSc2d1	střední
hmotnosti	hmotnost	k1gFnSc2	hmotnost
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
galaxii	galaxie	k1gFnSc6	galaxie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obíhá	obíhat	k5eAaImIp3nS	obíhat
přibližně	přibližně	k6eAd1	přibližně
tři	tři	k4xCgInPc4	tři
světelné	světelný	k2eAgInPc4d1	světelný
roky	rok	k1gInPc4	rok
od	od	k7c2	od
Střelce	Střelec	k1gMnSc2	Střelec
A	A	kA	A
<g/>
*	*	kIx~	*
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
střední	střední	k2eAgFnSc1d1	střední
černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
asi	asi	k9	asi
1300	[number]	k4	1300
Sluncí	slunce	k1gNnPc2	slunce
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
uvnitř	uvnitř	k7c2	uvnitř
shluku	shluk	k1gInSc2	shluk
sedmi	sedm	k4xCc2	sedm
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
jako	jako	k8xC	jako
pozůstatek	pozůstatek	k1gInSc4	pozůstatek
velkého	velký	k2eAgInSc2d1	velký
shluku	shluk	k1gInSc2	shluk
hvězd	hvězda	k1gFnPc2	hvězda
roztrženého	roztržený	k2eAgInSc2d1	roztržený
galaktickým	galaktický	k2eAgInSc7d1	galaktický
středem	střed	k1gInSc7	střed
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
objev	objev	k1gInSc1	objev
může	moct	k5eAaImIp3nS	moct
podpořit	podpořit	k5eAaPmF	podpořit
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
obří	obří	k2eAgFnPc1d1	obří
černé	černý	k2eAgFnPc1d1	černá
díry	díra	k1gFnPc1	díra
se	se	k3xPyFc4	se
zvětšují	zvětšovat	k5eAaImIp3nP	zvětšovat
pohlcováním	pohlcování	k1gNnSc7	pohlcování
blízkých	blízký	k2eAgFnPc2d1	blízká
menších	malý	k2eAgFnPc2d2	menší
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
a	a	k8xC	a
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2005	[number]	k4	2005
byl	být	k5eAaImAgMnS	být
objeven	objeven	k2eAgMnSc1d1	objeven
modrý	modrý	k2eAgMnSc1d1	modrý
obr	obr	k1gMnSc1	obr
SDSS	SDSS	kA	SDSS
J	J	kA	J
<g/>
0	[number]	k4	0
<g/>
90745.0	[number]	k4	90745.0
<g/>
+	+	kIx~	+
<g/>
24507	[number]	k4	24507
opouštějící	opouštějící	k2eAgFnSc4d1	opouštějící
Mléčnou	mléčný	k2eAgFnSc4d1	mléčná
dráhu	dráha	k1gFnSc4	dráha
dvojnásobnou	dvojnásobný	k2eAgFnSc7d1	dvojnásobná
únikovou	únikový	k2eAgFnSc7d1	úniková
rychlostí	rychlost	k1gFnSc7	rychlost
(	(	kIx(	(
<g/>
0,0022	[number]	k4	0,0022
rychlosti	rychlost	k1gFnSc2	rychlost
světla	světlo	k1gNnSc2	světlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trajektorii	trajektorie	k1gFnSc4	trajektorie
hvězdy	hvězda	k1gFnSc2	hvězda
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
dohledat	dohledat	k5eAaPmF	dohledat
až	až	k9	až
zpět	zpět	k6eAd1	zpět
ke	k	k7c3	k
galaktickému	galaktický	k2eAgNnSc3d1	Galaktické
jádru	jádro	k1gNnSc3	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
rychlost	rychlost	k1gFnSc1	rychlost
této	tento	k3xDgFnSc2	tento
hvězdy	hvězda	k1gFnSc2	hvězda
podporuje	podporovat	k5eAaImIp3nS	podporovat
hypotézu	hypotéza	k1gFnSc4	hypotéza
existence	existence	k1gFnSc2	existence
obří	obří	k2eAgFnSc2d1	obří
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
naší	náš	k3xOp1gFnSc2	náš
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
mikročerných	mikročerný	k2eAgFnPc2d1	mikročerný
děr	děra	k1gFnPc2	děra
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
v	v	k7c6	v
částicových	částicový	k2eAgInPc6d1	částicový
urychlovačích	urychlovač	k1gInPc6	urychlovač
byl	být	k5eAaImAgInS	být
trochu	trochu	k6eAd1	trochu
nejistě	jistě	k6eNd1	jistě
ohlašován	ohlašován	k2eAgMnSc1d1	ohlašován
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
doposud	doposud	k6eAd1	doposud
nepotvrzen	potvrzen	k2eNgInSc1d1	nepotvrzen
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
není	být	k5eNaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
ani	ani	k8xC	ani
žádný	žádný	k3yNgMnSc1	žádný
pozorovaný	pozorovaný	k2eAgMnSc1d1	pozorovaný
kandidát	kandidát	k1gMnSc1	kandidát
na	na	k7c4	na
prvotní	prvotní	k2eAgFnSc4d1	prvotní
černou	černý	k2eAgFnSc4d1	černá
díru	díra	k1gFnSc4	díra
<g/>
.	.	kIx.	.
</s>
<s>
Australští	australský	k2eAgMnPc1d1	australský
vědci	vědec	k1gMnPc1	vědec
učinili	učinit	k5eAaImAgMnP	učinit
výpočet	výpočet	k1gInSc4	výpočet
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
maximalizovat	maximalizovat	k5eAaBmF	maximalizovat
dobu	doba	k1gFnSc4	doba
přežití	přežití	k1gNnSc2	přežití
v	v	k7c6	v
černé	černý	k2eAgFnSc6d1	černá
díře	díra	k1gFnSc6	díra
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
teorie	teorie	k1gFnSc1	teorie
v	v	k7c6	v
jednoduchosti	jednoduchost	k1gFnSc6	jednoduchost
říká	říkat	k5eAaImIp3nS	říkat
<g/>
:	:	kIx,	:
Existuje	existovat	k5eAaImIp3nS	existovat
jediná	jediný	k2eAgFnSc1d1	jediná
nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
cesta	cesta	k1gFnSc1	cesta
při	při	k7c6	při
pádu	pád	k1gInSc6	pád
do	do	k7c2	do
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
tj.	tj.	kA	tj.
dráha	dráha	k1gFnSc1	dráha
volného	volný	k2eAgInSc2d1	volný
pádu	pád	k1gInSc2	pád
z	z	k7c2	z
počátečního	počáteční	k2eAgInSc2d1	počáteční
klidu	klid	k1gInSc2	klid
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
kratší	krátký	k2eAgFnPc1d2	kratší
cesty	cesta	k1gFnPc1	cesta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
překročení	překročení	k1gNnSc2	překročení
horizontu	horizont	k1gInSc2	horizont
událostí	událost	k1gFnPc2	událost
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
kratších	krátký	k2eAgFnPc2d2	kratší
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zažehnout	zažehnout	k5eAaPmF	zažehnout
motory	motor	k1gInPc4	motor
rakety	raketa	k1gFnSc2	raketa
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
nejdelší	dlouhý	k2eAgNnPc4d3	nejdelší
a	a	k8xC	a
maximalizovat	maximalizovat	k5eAaBmF	maximalizovat
svůj	svůj	k3xOyFgInSc4	svůj
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2007	[number]	k4	2007
objevil	objevit	k5eAaPmAgInS	objevit
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
tým	tým	k1gInSc1	tým
astronomů	astronom	k1gMnPc2	astronom
z	z	k7c2	z
Kanady	Kanada	k1gFnSc2	Kanada
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
USA	USA	kA	USA
dalekohledem	dalekohled	k1gInSc7	dalekohled
CFHT	CFHT	kA	CFHT
na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
doposud	doposud	k6eAd1	doposud
neznámou	známý	k2eNgFnSc4d1	neznámá
černou	černý	k2eAgFnSc4d1	černá
díru	díra	k1gFnSc4	díra
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
13	[number]	k4	13
miliard	miliarda	k4xCgFnPc2	miliarda
světelných	světelný	k2eAgInPc2d1	světelný
let	let	k1gInSc4	let
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejvzdálenější	vzdálený	k2eAgFnSc4d3	nejvzdálenější
černou	černý	k2eAgFnSc4d1	černá
díru	díra	k1gFnSc4	díra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
zatím	zatím	k6eAd1	zatím
nalezena	naleznout	k5eAaPmNgFnS	naleznout
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
kvasaru	kvasar	k1gInSc2	kvasar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
publikován	publikovat	k5eAaBmNgInS	publikovat
objev	objev	k1gInSc1	objev
patrně	patrně	k6eAd1	patrně
největšího	veliký	k2eAgInSc2d3	veliký
binárního	binární	k2eAgInSc2d1	binární
systému	systém	k1gInSc2	systém
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgMnS	být
největší	veliký	k2eAgFnSc7d3	veliký
hvězdnou	hvězdný	k2eAgFnSc7d1	hvězdná
černou	černý	k2eAgFnSc7d1	černá
dírou	díra	k1gFnSc7	díra
objekt	objekt	k1gInSc1	objekt
GRS	GRS	kA	GRS
1915	[number]	k4	1915
<g/>
+	+	kIx~	+
<g/>
105	[number]	k4	105
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
odhadnutou	odhadnutý	k2eAgFnSc4d1	odhadnutá
na	na	k7c4	na
14	[number]	k4	14
plus	plus	k1gInSc4	plus
nebo	nebo	k8xC	nebo
minus	minus	k1gInSc4	minus
4	[number]	k4	4
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
objevitel	objevitel	k1gMnSc1	objevitel
Orosz	Orosz	k1gMnSc1	Orosz
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Nicméně	nicméně	k8xC	nicméně
hmotnost	hmotnost	k1gFnSc1	hmotnost
GRS	GRS	kA	GRS
1915	[number]	k4	1915
<g/>
+	+	kIx~	+
<g/>
105	[number]	k4	105
přišla	přijít	k5eAaPmAgFnS	přijít
na	na	k7c4	na
přetřes	přetřes	k1gInSc4	přetřes
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
posuzováno	posuzovat	k5eAaImNgNnS	posuzovat
několik	několik	k4yIc4	několik
alternativních	alternativní	k2eAgInPc2d1	alternativní
modelů	model	k1gInPc2	model
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
jako	jako	k9	jako
černé	černý	k2eAgFnPc1d1	černá
díry	díra	k1gFnPc1	díra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
fungují	fungovat	k5eAaImIp3nP	fungovat
bez	bez	k7c2	bez
singularity	singularita	k1gFnSc2	singularita
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vědců	vědec	k1gMnPc2	vědec
však	však	k9	však
považuje	považovat	k5eAaImIp3nS	považovat
tyto	tento	k3xDgInPc4	tento
koncepty	koncept	k1gInPc4	koncept
za	za	k7c4	za
vyumělkované	vyumělkovaný	k2eAgNnSc4d1	vyumělkované
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
složitější	složitý	k2eAgInPc1d2	složitější
a	a	k8xC	a
nepřinášejí	přinášet	k5eNaImIp3nP	přinášet
žádné	žádný	k3yNgInPc4	žádný
pozorovatelné	pozorovatelný	k2eAgInPc4d1	pozorovatelný
rozdíly	rozdíl	k1gInPc4	rozdíl
od	od	k7c2	od
černých	černý	k2eAgFnPc2d1	černá
děr	děra	k1gFnPc2	děra
(	(	kIx(	(
<g/>
nevyhovují	vyhovovat	k5eNaImIp3nP	vyhovovat
tedy	tedy	k9	tedy
logice	logika	k1gFnSc3	logika
tzv.	tzv.	kA	tzv.
Occamovy	Occamův	k2eAgFnSc2d1	Occamova
břitvy	břitva	k1gFnSc2	břitva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgMnPc1d3	nejvýznamnější
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
teorií	teorie	k1gFnPc2	teorie
je	být	k5eAaImIp3nS	být
teorie	teorie	k1gFnSc1	teorie
tzv.	tzv.	kA	tzv.
gravahvězda	gravahvězda	k1gFnSc1	gravahvězda
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Gravastar	Gravastar	k1gInSc1	Gravastar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2005	[number]	k4	2005
fyzik	fyzik	k1gMnSc1	fyzik
George	Georg	k1gInSc2	Georg
Chapline	Chaplin	k1gInSc5	Chaplin
z	z	k7c2	z
Národní	národní	k2eAgFnSc2d1	národní
laboratoře	laboratoř	k1gFnSc2	laboratoř
Lawrencea	Lawrenceus	k1gMnSc2	Lawrenceus
Livermora	Livermor	k1gMnSc2	Livermor
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
černé	černý	k2eAgFnPc1d1	černá
díry	díra	k1gFnPc1	díra
neexistují	existovat	k5eNaImIp3nP	existovat
a	a	k8xC	a
že	že	k8xS	že
objekty	objekt	k1gInPc1	objekt
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
považované	považovaný	k2eAgFnSc6d1	považovaná
za	za	k7c2	za
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
hvězdy	hvězda	k1gFnSc2	hvězda
z	z	k7c2	z
temné	temný	k2eAgFnSc2d1	temná
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Svoje	svůj	k3xOyFgInPc4	svůj
závěry	závěr	k1gInPc4	závěr
opírá	opírat	k5eAaImIp3nS	opírat
o	o	k7c4	o
výsledky	výsledek	k1gInPc4	výsledek
některých	některý	k3yIgFnPc2	některý
kvantově-mechanických	kvantověechanický	k2eAgFnPc2d1	kvantově-mechanická
analýz	analýza	k1gFnPc2	analýza
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
má	mít	k5eAaImIp3nS	mít
jeho	jeho	k3xOp3gInSc4	jeho
návrh	návrh	k1gInSc4	návrh
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jen	jen	k9	jen
malou	malý	k2eAgFnSc4d1	malá
podporu	podpora	k1gFnSc4	podpora
ve	v	k7c6	v
fyzikální	fyzikální	k2eAgFnSc6d1	fyzikální
obci	obec	k1gFnSc6	obec
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
citovaný	citovaný	k2eAgMnSc1d1	citovaný
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
<g/>
.	.	kIx.	.
</s>
