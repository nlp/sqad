<s>
František	František	k1gMnSc1	František
Křižík	Křižík	k1gMnSc1	Křižík
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1847	[number]	k4	1847
Plánice	plánice	k1gFnSc2	plánice
–	–	k?	–
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1941	[number]	k4	1941
Stádlec	Stádlec	k1gMnSc1	Stádlec
u	u	k7c2	u
Tábora	Tábor	k1gInSc2	Tábor
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
technik	technik	k1gMnSc1	technik
<g/>
,	,	kIx,	,
průmyslník	průmyslník	k1gMnSc1	průmyslník
a	a	k8xC	a
vynálezce	vynálezce	k1gMnSc1	vynálezce
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
nejznámějším	známý	k2eAgInSc7d3	nejznámější
vynálezem	vynález	k1gInSc7	vynález
byla	být	k5eAaImAgFnS	být
oblouková	obloukový	k2eAgFnSc1d1	oblouková
lampa	lampa	k1gFnSc1	lampa
se	s	k7c7	s
samočinnou	samočinný	k2eAgFnSc7d1	samočinná
regulací	regulace	k1gFnSc7	regulace
<g/>
.	.	kIx.	.
</s>
