<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
slovenský	slovenský	k2eAgMnSc1d1	slovenský
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
vědec	vědec	k1gMnSc1	vědec
<g/>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
?	?	kIx.	?
</s>
