<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Mistrík	Mistrík	k1gMnSc1	Mistrík
(	(	kIx(	(
<g/>
*	*	kIx~	*
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
vědec	vědec	k1gMnSc1	vědec
<g/>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc4	dětství
a	a	k8xC	a
vzdělání	vzdělání	k1gNnSc4	vzdělání
===	===	k?	===
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Mistrík	Mistrík	k1gMnSc1	Mistrík
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1966	[number]	k4	1966
v	v	k7c6	v
Banské	banský	k2eAgFnSc6d1	Banská
Bystrici	Bystrica	k1gFnSc6	Bystrica
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prožil	prožít	k5eAaPmAgMnS	prožít
své	svůj	k3xOyFgNnSc4	svůj
dětství	dětství	k1gNnSc4	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Maturoval	maturovat	k5eAaBmAgMnS	maturovat
na	na	k7c6	na
Gymnáziu	gymnázium	k1gNnSc6	gymnázium
Jozefa	Jozef	k1gMnSc2	Jozef
Gregora	Gregor	k1gMnSc2	Gregor
Tajovského	Tajovský	k1gMnSc2	Tajovský
v	v	k7c6	v
Banské	banský	k2eAgFnSc6d1	Banská
Bystrici	Bystrica	k1gFnSc6	Bystrica
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
Chemicko-technologickou	chemickoechnologický	k2eAgFnSc4d1	chemicko-technologická
fakultu	fakulta	k1gFnSc4	fakulta
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
vysoké	vysoký	k2eAgFnSc2d1	vysoká
školy	škola	k1gFnSc2	škola
technické	technický	k2eAgFnSc2d1	technická
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
<g/>
,	,	kIx,	,
obor	obor	k1gInSc4	obor
analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
doktorském	doktorský	k2eAgNnSc6d1	doktorské
studiu	studio	k1gNnSc6	studio
na	na	k7c6	na
vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
udělen	udělen	k2eAgInSc1d1	udělen
akademický	akademický	k2eAgInSc1d1	akademický
titul	titul	k1gInSc1	titul
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
PhD	PhD	k1gFnSc1	PhD
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vědecké	vědecký	k2eAgFnSc6d1	vědecká
práci	práce	k1gFnSc6	práce
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
jako	jako	k9	jako
hostující	hostující	k2eAgMnSc1d1	hostující
vědec	vědec	k1gMnSc1	vědec
na	na	k7c6	na
National	National	k1gFnSc6	National
Institute	institut	k1gInSc5	institut
of	of	k?	of
Standards	Standards	k1gInSc1	Standards
and	and	k?	and
Technology	technolog	k1gMnPc7	technolog
(	(	kIx(	(
<g/>
NIST	NIST	kA	NIST
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gaithersburg	Gaithersburg	k1gMnSc1	Gaithersburg
<g/>
,	,	kIx,	,
MD	MD	kA	MD
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kariéra	kariéra	k1gFnSc1	kariéra
===	===	k?	===
</s>
</p>
<p>
<s>
Mistrík	Mistrík	k1gInSc1	Mistrík
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
založil	založit	k5eAaPmAgMnS	založit
technologickou	technologický	k2eAgFnSc4d1	technologická
firmu	firma	k1gFnSc4	firma
HighChem	HighCh	k1gInSc7	HighCh
<g/>
,	,	kIx,	,
působící	působící	k2eAgFnSc7d1	působící
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
hmotnostní	hmotnostní	k2eAgFnSc2d1	hmotnostní
spektrometrie	spektrometrie	k1gFnSc2	spektrometrie
<g/>
,	,	kIx,	,
metabolomika	metabolomik	k1gMnSc2	metabolomik
a	a	k8xC	a
chemických	chemický	k2eAgFnPc2d1	chemická
analýz	analýza	k1gFnPc2	analýza
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
dodnes	dodnes	k6eAd1	dodnes
vede	vést	k5eAaImIp3nS	vést
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
člen	člen	k1gMnSc1	člen
vědecké	vědecký	k2eAgFnSc2d1	vědecká
řídící	řídící	k2eAgFnSc2d1	řídící
komise	komise	k1gFnSc2	komise
v	v	k7c6	v
konsorciu	konsorcium	k1gNnSc6	konsorcium
METAcancer	METAcancra	k1gFnPc2	METAcancra
zaměřeného	zaměřený	k2eAgMnSc2d1	zaměřený
na	na	k7c6	na
hledání	hledání	k1gNnSc6	hledání
biomarkerů	biomarker	k1gInPc2	biomarker
rakoviny	rakovina	k1gFnSc2	rakovina
prsu	prs	k1gInSc2	prs
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
patentu	patent	k1gInSc2	patent
k	k	k7c3	k
identifikaci	identifikace	k1gFnSc3	identifikace
malých	malý	k2eAgFnPc2d1	malá
molekul	molekula	k1gFnPc2	molekula
včetně	včetně	k7c2	včetně
nových	nový	k2eAgInPc2d1	nový
diagnostických	diagnostický	k2eAgInPc2d1	diagnostický
markerů	marker	k1gInPc2	marker
zákeřných	zákeřný	k2eAgFnPc2d1	zákeřná
chorob	choroba	k1gFnPc2	choroba
a	a	k8xC	a
původním	původní	k2eAgMnSc7d1	původní
autorem	autor	k1gMnSc7	autor
softwaru	software	k1gInSc2	software
Mass	Mass	k1gInSc1	Mass
Frontier	Frontier	k1gInSc1	Frontier
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2000	[number]	k4	2000
vědecko-výzkumných	vědeckoýzkumný	k2eAgFnPc6d1	vědecko-výzkumná
a	a	k8xC	a
průmyslových	průmyslový	k2eAgFnPc6d1	průmyslová
laboratořích	laboratoř	k1gFnPc6	laboratoř
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Vede	vést	k5eAaImIp3nS	vést
vývoj	vývoj	k1gInSc4	vývoj
cloudové	cloudový	k2eAgFnSc2d1	cloudová
databáze	databáze	k1gFnSc2	databáze
spektrálních	spektrální	k2eAgInPc2d1	spektrální
stromů	strom	k1gInPc2	strom
s	s	k7c7	s
názvem	název	k1gInSc7	název
mzCloud	mzClouda	k1gFnPc2	mzClouda
<g/>
,	,	kIx,	,
umožňující	umožňující	k2eAgFnSc3d1	umožňující
identifikaci	identifikace	k1gFnSc3	identifikace
lidských	lidský	k2eAgInPc2d1	lidský
metabolitů	metabolit	k1gInPc2	metabolit
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
,	,	kIx,	,
dopingových	dopingový	k2eAgFnPc2d1	dopingová
látek	látka	k1gFnPc2	látka
,	,	kIx,	,
environmentálních	environmentální	k2eAgInPc2d1	environmentální
kontaminantů	kontaminant	k1gInPc2	kontaminant
<g/>
,	,	kIx,	,
drog	droga	k1gFnPc2	droga
a	a	k8xC	a
omamných	omamný	k2eAgFnPc2d1	omamná
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
laureátem	laureát	k1gMnSc7	laureát
vědecké	vědecký	k2eAgFnSc2d1	vědecká
ceny	cena	k1gFnSc2	cena
"	"	kIx"	"
<g/>
Hlava	hlava	k1gFnSc1	hlava
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
za	za	k7c4	za
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
inovaci	inovace	k1gFnSc4	inovace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
znovuzvolen	znovuzvolit	k5eAaPmNgMnS	znovuzvolit
členy	člen	k1gMnPc7	člen
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
Metabolomics	Metabolomics	k1gInSc4	Metabolomics
Society	societa	k1gFnSc2	societa
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
ředitelů	ředitel	k1gMnPc2	ředitel
<g/>
.	.	kIx.	.
<g/>
Je	být	k5eAaImIp3nS	být
spoluautorem	spoluautor	k1gMnSc7	spoluautor
metody	metoda	k1gFnSc2	metoda
prohledávání	prohledávání	k1gNnSc2	prohledávání
hmotnostních	hmotnostní	k2eAgNnPc2d1	hmotnostní
spekter	spektrum	k1gNnPc2	spektrum
přes	přes	k7c4	přes
Google	Google	k1gInSc4	Google
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
v	v	k7c6	v
Nature	Natur	k1gMnSc5	Natur
Biotechnology	biotechnolog	k1gMnPc7	biotechnolog
<g/>
,	,	kIx,	,
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
vědeckých	vědecký	k2eAgInPc2d1	vědecký
časopisů	časopis	k1gInPc2	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
počin	počin	k1gInSc1	počin
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
podařil	podařit	k5eAaPmAgInS	podařit
vědci	vědec	k1gMnPc7	vědec
ze	z	k7c2	z
slovenské	slovenský	k2eAgFnSc2d1	slovenská
instituce	instituce	k1gFnSc2	instituce
po	po	k7c6	po
14	[number]	k4	14
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
<g/>
Přednášel	přednášet	k5eAaImAgInS	přednášet
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
vědeckých	vědecký	k2eAgFnPc6d1	vědecká
konferencích	konference	k1gFnPc6	konference
a	a	k8xC	a
sympoziích	sympozion	k1gNnPc6	sympozion
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
komentářů	komentář	k1gInPc2	komentář
v	v	k7c6	v
Deníku	deník	k1gInSc6	deník
N	N	kA	N
a	a	k8xC	a
na	na	k7c6	na
portálu	portál	k1gInSc6	portál
aktuality	aktualita	k1gFnSc2	aktualita
<g/>
.	.	kIx.	.
<g/>
sk	sk	k?	sk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Občanské	občanský	k2eAgFnSc2d1	občanská
aktivity	aktivita	k1gFnSc2	aktivita
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Politická	politický	k2eAgFnSc1d1	politická
činnost	činnost	k1gFnSc1	činnost
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
spoluzakládal	spoluzakládat	k5eAaImAgMnS	spoluzakládat
politickou	politický	k2eAgFnSc4d1	politická
stranu	strana	k1gFnSc4	strana
Sloboda	sloboda	k1gFnSc1	sloboda
a	a	k8xC	a
Solidarita	solidarita	k1gFnSc1	solidarita
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
členem	člen	k1gInSc7	člen
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kritika	kritika	k1gFnSc1	kritika
eurofondů	eurofond	k1gInPc2	eurofond
===	===	k?	===
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
vědců	vědec	k1gMnPc2	vědec
se	se	k3xPyFc4	se
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2017	[number]	k4	2017
otevřeně	otevřeně	k6eAd1	otevřeně
postavil	postavit	k5eAaPmAgMnS	postavit
proti	proti	k7c3	proti
skandálním	skandální	k2eAgFnPc3d1	skandální
praktikám	praktika	k1gFnPc3	praktika
přidělování	přidělování	k1gNnSc2	přidělování
eurofondů	eurofond	k1gMnPc2	eurofond
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
školství	školství	k1gNnSc2	školství
v	v	k7c6	v
objemu	objem	k1gInSc6	objem
600	[number]	k4	600
milionů	milion	k4xCgInPc2	milion
eur	euro	k1gNnPc2	euro
a	a	k8xC	a
kritiku	kritika	k1gFnSc4	kritika
i	i	k9	i
veřejně	veřejně	k6eAd1	veřejně
adresoval	adresovat	k5eAaBmAgMnS	adresovat
odpovědným	odpovědný	k2eAgMnSc7d1	odpovědný
osobám	osoba	k1gFnPc3	osoba
a	a	k8xC	a
institucím	instituce	k1gFnPc3	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Následné	následný	k2eAgInPc1d1	následný
výsledky	výsledek	k1gInPc1	výsledek
kontroly	kontrola	k1gFnSc2	kontrola
NKÚ	NKÚ	kA	NKÚ
potvrdily	potvrdit	k5eAaPmAgInP	potvrdit
závažné	závažný	k2eAgNnSc1d1	závažné
zjištění	zjištění	k1gNnSc3	zjištění
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgInPc4	který
Mistrík	Mistrík	k1gInSc1	Mistrík
poukazoval	poukazovat	k5eAaImAgInS	poukazovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
veřejně	veřejně	k6eAd1	veřejně
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
proti	proti	k7c3	proti
působení	působení	k1gNnSc3	působení
organizovaných	organizovaný	k2eAgFnPc2d1	organizovaná
skupin	skupina	k1gFnPc2	skupina
ve	v	k7c6	v
strukturálních	strukturální	k2eAgInPc6d1	strukturální
fondech	fond	k1gInPc6	fond
EU	EU	kA	EU
a	a	k8xC	a
upozornil	upozornit	k5eAaPmAgMnS	upozornit
<g/>
,	,	kIx,	,
že	že	k8xS	že
peníze	peníz	k1gInPc1	peníz
se	se	k3xPyFc4	se
nedostávají	dostávat	k5eNaImIp3nP	dostávat
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dělají	dělat	k5eAaImIp3nP	dělat
skutečný	skutečný	k2eAgInSc4d1	skutečný
výzkum	výzkum	k1gInSc4	výzkum
a	a	k8xC	a
posouvají	posouvat	k5eAaImIp3nP	posouvat
Slovensko	Slovensko	k1gNnSc4	Slovensko
dopředu	dopředu	k6eAd1	dopředu
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skupinám	skupina	k1gFnPc3	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
nazval	nazvat	k5eAaPmAgMnS	nazvat
"	"	kIx"	"
<g/>
eurofondová	eurofondový	k2eAgFnSc1d1	eurofondový
mafie	mafie	k1gFnSc1	mafie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Matej	Matej	k1gInSc1	Matej
Tóth	Tóth	k1gInSc1	Tóth
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
byl	být	k5eAaImAgMnS	být
Robert	Robert	k1gMnSc1	Robert
Mistrík	Mistrík	k1gMnSc1	Mistrík
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
odborného	odborný	k2eAgInSc2d1	odborný
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyvrátil	vyvrátit	k5eAaPmAgInS	vyvrátit
podezření	podezření	k1gNnSc4	podezření
z	z	k7c2	z
dopingu	doping	k1gInSc2	doping
Matěje	Matěj	k1gMnSc2	Matěj
Tótha	Tóth	k1gMnSc2	Tóth
<g/>
,	,	kIx,	,
olympijského	olympijský	k2eAgMnSc2d1	olympijský
vítěze	vítěz	k1gMnSc2	vítěz
a	a	k8xC	a
nejlepšího	dobrý	k2eAgMnSc2d3	nejlepší
slovenského	slovenský	k2eAgMnSc2d1	slovenský
sportovce	sportovec	k1gMnSc2	sportovec
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Mistrík	Mistrík	k1gInSc1	Mistrík
vedl	vést	k5eAaImAgInS	vést
i	i	k9	i
domácí	domácí	k2eAgFnSc4d1	domácí
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
prezentaci	prezentace	k1gFnSc4	prezentace
odborných	odborný	k2eAgInPc2d1	odborný
argumentů	argument	k1gInPc2	argument
prokazující	prokazující	k2eAgFnSc4d1	prokazující
nevinu	nevina	k1gFnSc4	nevina
Matěje	Matěj	k1gMnSc2	Matěj
Tótha	Tóth	k1gMnSc2	Tóth
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
zbaven	zbavit	k5eAaPmNgMnS	zbavit
všech	všecek	k3xTgNnPc2	všecek
podezření	podezření	k1gNnPc2	podezření
a	a	k8xC	a
očištěný	očištěný	k2eAgInSc1d1	očištěný
Slovenský	slovenský	k2eAgInSc1d1	slovenský
atletickým	atletický	k2eAgInSc7d1	atletický
svazem	svaz	k1gInSc7	svaz
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
atletickou	atletický	k2eAgFnSc7d1	atletická
federací	federace	k1gFnSc7	federace
IAAF	IAAF	kA	IAAF
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kandidatura	kandidatura	k1gFnSc1	kandidatura
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
===	===	k?	===
</s>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2018	[number]	k4	2018
oznámil	oznámit	k5eAaPmAgMnS	oznámit
svoji	svůj	k3xOyFgFnSc4	svůj
kandidaturu	kandidatura	k1gFnSc4	kandidatura
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
<g/>
,	,	kIx,	,
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2019	[number]	k4	2019
se	se	k3xPyFc4	se
kandidatury	kandidatura	k1gFnSc2	kandidatura
vzdal	vzdát	k5eAaPmAgMnS	vzdát
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
k	k	k7c3	k
podpoře	podpora	k1gFnSc3	podpora
Zuzany	Zuzana	k1gFnSc2	Zuzana
Čaputové	Čaput	k1gMnPc1	Čaput
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Laureát	laureát	k1gMnSc1	laureát
ceny	cena	k1gFnSc2	cena
Hlava	hlava	k1gFnSc1	hlava
Roku	rok	k1gInSc6	rok
2008	[number]	k4	2008
za	za	k7c4	za
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
inovaci	inovace	k1gFnSc4	inovace
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2018	[number]	k4	2018
mu	on	k3xPp3gNnSc3	on
prezident	prezident	k1gMnSc1	prezident
Andrej	Andrej	k1gMnSc1	Andrej
Kiska	Kiska	k1gMnSc1	Kiska
udělil	udělit	k5eAaPmAgMnS	udělit
Pribinův	Pribinův	k2eAgInSc4d1	Pribinův
kříž	kříž	k1gInSc4	kříž
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mimořádné	mimořádný	k2eAgNnSc4d1	mimořádné
poděkování	poděkování	k1gNnSc4	poděkování
VSC	VSC	kA	VSC
Dukla	Dukla	k1gFnSc1	Dukla
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
k	k	k7c3	k
očištění	očištění	k1gNnSc3	očištění
jména	jméno	k1gNnSc2	jméno
olympijského	olympijský	k2eAgMnSc2d1	olympijský
vítěze	vítěz	k1gMnSc2	vítěz
Matěje	Matěj	k1gMnSc2	Matěj
Tótha	Tóth	k1gMnSc2	Tóth
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Robert	Robert	k1gMnSc1	Robert
Mistrík	Mistrík	k1gMnSc1	Mistrík
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Vědecké	vědecký	k2eAgFnPc1d1	vědecká
publikace	publikace	k1gFnPc1	publikace
</s>
</p>
<p>
<s>
Přednášková	přednáškový	k2eAgFnSc1d1	přednášková
činnost	činnost	k1gFnSc1	činnost
</s>
</p>
<p>
<s>
Osobní	osobní	k2eAgFnSc1d1	osobní
stránka	stránka	k1gFnSc1	stránka
</s>
</p>
