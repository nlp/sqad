<s>
José	Josý	k2eAgInPc1d1	Josý
Echegaray	Echegaray	k1gInPc1	Echegaray
y	y	k?	y
Eizaguirre	Eizaguirr	k1gInSc5	Eizaguirr
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1832	[number]	k4	1832
<g/>
,	,	kIx,	,
Madrid	Madrid	k1gInSc1	Madrid
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
Madrid	Madrid	k1gInSc1	Madrid
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
španělský	španělský	k2eAgMnSc1d1	španělský
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
ekonom	ekonom	k1gMnSc1	ekonom
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
obdržel	obdržet	k5eAaPmAgMnS	obdržet
společně	společně	k6eAd1	společně
s	s	k7c7	s
francouzským	francouzský	k2eAgMnSc7d1	francouzský
básníkem	básník	k1gMnSc7	básník
Frédéricem	Frédéric	k1gMnSc7	Frédéric
Mistralem	mistral	k1gInSc7	mistral
<g/>
.	.	kIx.	.
</s>
<s>
José	Josý	k2eAgInPc1d1	Josý
Echegaray	Echegaray	k1gInPc1	Echegaray
y	y	k?	y
Eizaguirre	Eizaguirr	k1gInSc5	Eizaguirr
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
mládí	mládí	k1gNnSc2	mládí
obdivovatelem	obdivovatel	k1gMnSc7	obdivovatel
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
však	však	k9	však
technické	technický	k2eAgInPc4d1	technický
obory	obor	k1gInPc4	obor
a	a	k8xC	a
ekonomii	ekonomie	k1gFnSc4	ekonomie
<g/>
.	.	kIx.	.
</s>
<s>
Uplatnil	uplatnit	k5eAaPmAgInS	uplatnit
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
-	-	kIx~	-
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
španělské	španělský	k2eAgFnSc2d1	španělská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
dramatik	dramatik	k1gMnSc1	dramatik
si	se	k3xPyFc3	se
zachovával	zachovávat	k5eAaImAgMnS	zachovávat
přísný	přísný	k2eAgInSc4d1	přísný
morální	morální	k2eAgInSc4d1	morální
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
umělecké	umělecký	k2eAgNnSc4d1	umělecké
zobrazování	zobrazování	k1gNnSc4	zobrazování
lidských	lidský	k2eAgInPc2d1	lidský
osudů	osud	k1gInPc2	osud
a	a	k8xC	a
setkával	setkávat	k5eAaImAgMnS	setkávat
se	se	k3xPyFc4	se
nadšeným	nadšený	k2eAgInSc7d1	nadšený
souhlasem	souhlas	k1gInSc7	souhlas
četných	četný	k2eAgMnPc2d1	četný
obdivovatelů	obdivovatel	k1gMnPc2	obdivovatel
<g/>
.	.	kIx.	.
</s>
<s>
Objevovaly	objevovat	k5eAaImAgInP	objevovat
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
přísné	přísný	k2eAgInPc4d1	přísný
kritické	kritický	k2eAgInPc4d1	kritický
hlasy	hlas	k1gInPc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Říkalo	říkat	k5eAaImAgNnS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnPc1	jeho
dramata	drama	k1gNnPc1	drama
mají	mít	k5eAaImIp3nP	mít
podobu	podoba	k1gFnSc4	podoba
rovnic	rovnice	k1gFnPc2	rovnice
a	a	k8xC	a
matematických	matematický	k2eAgInPc2d1	matematický
problémů	problém	k1gInPc2	problém
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
psána	psán	k2eAgNnPc4d1	psáno
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
matematika	matematik	k1gMnSc2	matematik
a	a	k8xC	a
veřejného	veřejný	k2eAgMnSc2d1	veřejný
činitele	činitel	k1gMnSc2	činitel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
etické	etický	k2eAgFnPc1d1	etická
hodnoty	hodnota	k1gFnPc1	hodnota
neúprosně	úprosně	k6eNd1	úprosně
staví	stavit	k5eAaImIp3nP	stavit
proti	proti	k7c3	proti
slabostem	slabost	k1gFnPc3	slabost
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
však	však	k9	však
nemohl	moct	k5eNaImAgMnS	moct
popřít	popřít	k5eAaPmF	popřít
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
hlubokým	hluboký	k2eAgNnSc7d1	hluboké
mravním	mravní	k2eAgNnSc7d1	mravní
pojetím	pojetí	k1gNnSc7	pojetí
<g/>
.	.	kIx.	.
</s>
<s>
Echegeray	Echegeraa	k1gFnSc2	Echegeraa
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
asi	asi	k9	asi
šedesáti	šedesát	k4xCc2	šedesát
dramat	drama	k1gNnPc2	drama
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
hybnou	hybný	k2eAgFnSc7d1	hybná
silou	síla	k1gFnSc7	síla
je	být	k5eAaImIp3nS	být
romantická	romantický	k2eAgFnSc1d1	romantická
vášnivost	vášnivost	k1gFnSc1	vášnivost
a	a	k8xC	a
osudovost	osudovost	k1gFnSc1	osudovost
<g/>
,	,	kIx,	,
na	na	k7c4	na
něž	jenž	k3xRgMnPc4	jenž
se	se	k3xPyFc4	se
napojuje	napojovat	k5eAaImIp3nS	napojovat
pozitivistická	pozitivistický	k2eAgFnSc1d1	pozitivistická
didaktičnost	didaktičnost	k1gFnSc1	didaktičnost
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
jsou	být	k5eAaImIp3nP	být
psány	psát	k5eAaImNgInP	psát
ve	v	k7c6	v
verších	verš	k1gInPc6	verš
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
vypočítány	vypočítat	k5eAaPmNgInP	vypočítat
na	na	k7c4	na
efekt	efekt	k1gInSc4	efekt
a	a	k8xC	a
dočasně	dočasně	k6eAd1	dočasně
zaujaly	zaujmout	k5eAaPmAgFnP	zaujmout
i	i	k9	i
diváky	divák	k1gMnPc4	divák
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
Echegeray	Echegeraa	k1gFnPc4	Echegeraa
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
téměř	téměř	k6eAd1	téměř
neznámým	známý	k2eNgMnSc7d1	neznámý
autorem	autor	k1gMnSc7	autor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
francouzským	francouzský	k2eAgMnSc7d1	francouzský
básníkem	básník	k1gMnSc7	básník
Frédéricem	Frédéric	k1gMnSc7	Frédéric
Mistralem	mistral	k1gInSc7	mistral
udělena	udělit	k5eAaPmNgFnS	udělit
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
"	"	kIx"	"
<g/>
...	...	k?	...
se	s	k7c7	s
zřetelem	zřetel	k1gInSc7	zřetel
k	k	k7c3	k
obsáhlému	obsáhlý	k2eAgNnSc3d1	obsáhlé
a	a	k8xC	a
geniálnímu	geniální	k2eAgNnSc3d1	geniální
dílu	dílo	k1gNnSc3	dílo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
samostatným	samostatný	k2eAgInSc7d1	samostatný
a	a	k8xC	a
originálním	originální	k2eAgInSc7d1	originální
způsobem	způsob	k1gInSc7	způsob
oživilo	oživit	k5eAaPmAgNnS	oživit
velké	velký	k2eAgFnPc4d1	velká
tradice	tradice	k1gFnPc4	tradice
španělského	španělský	k2eAgNnSc2d1	španělské
dramatu	drama	k1gNnSc2	drama
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
citace	citace	k1gFnPc1	citace
z	z	k7c2	z
odůvodnění	odůvodnění	k1gNnSc2	odůvodnění
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
El	Ela	k1gFnPc2	Ela
libro	libra	k1gFnSc5	libra
talonario	talonario	k1gMnSc1	talonario
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
,	,	kIx,	,
Šeková	šekový	k2eAgFnSc1d1	šeková
knížka	knížka	k1gFnSc1	knížka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
La	la	k1gNnSc4	la
esposa	esposa	k1gFnSc1	esposa
del	del	k?	del
vengador	vengador	k1gInSc1	vengador
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
,	,	kIx,	,
Mstitelova	mstitelův	k2eAgFnSc1d1	mstitelův
žena	žena	k1gFnSc1	žena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
En	En	k1gMnSc1	En
puñ	puñ	k?	puñ
de	de	k?	de
la	la	k1gNnSc2	la
espada	espada	k1gFnSc1	espada
(	(	kIx(	(
<g/>
1875	[number]	k4	1875
<g/>
,	,	kIx,	,
Jílec	jílec	k1gInSc1	jílec
meče	meč	k1gInSc2	meč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
O	o	k7c4	o
locura	locur	k1gMnSc4	locur
o	o	k7c4	o
santidad	santidad	k1gInSc4	santidad
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1877	[number]	k4	1877
<g/>
,	,	kIx,	,
Světec	světec	k1gMnSc1	světec
či	či	k8xC	či
blázen	blázen	k1gMnSc1	blázen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
En	En	k1gMnSc1	En
el	ela	k1gFnPc2	ela
pilar	pilar	k1gMnSc1	pilar
y	y	k?	y
en	en	k?	en
la	la	k1gNnSc2	la
cruz	cruza	k1gFnPc2	cruza
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
<g/>
,	,	kIx,	,
Hranice	hranice	k1gFnSc1	hranice
a	a	k8xC	a
kříž	kříž	k1gInSc1	kříž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
En	En	k1gFnSc2	En
el	ela	k1gFnPc2	ela
seno	seno	k1gNnSc1	seno
de	de	k?	de
la	la	k1gNnPc2	la
muerte	muert	k1gInSc5	muert
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
muerte	muert	k1gInSc5	muert
en	en	k?	en
los	los	k1gInSc1	los
labios	labios	k1gInSc1	labios
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
El	Ela	k1gFnPc2	Ela
gran	grana	k1gFnPc2	grana
Galeoto	Galeota	k1gFnSc5	Galeota
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
,	,	kIx,	,
Velký	velký	k2eAgInSc1d1	velký
Galeoto	Galeota	k1gFnSc5	Galeota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Lo	Lo	k1gFnSc1	Lo
sublime	sublimat	k5eAaPmIp3nS	sublimat
en	en	k?	en
lo	lo	k?	lo
vulgar	vulgar	k1gInSc1	vulgar
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
En	En	k1gMnSc5	En
crítico	crítica	k1gMnSc5	crítica
incipiente	incipient	k1gMnSc5	incipient
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Malas	Malas	k1gMnSc1	Malas
Herencias	Herencias	k1gMnSc1	Herencias
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
El	Ela	k1gFnPc2	Ela
hijo	hijo	k0	hijo
de	de	k?	de
Don	Don	k1gMnSc1	Don
Juan	Juan	k1gMnSc1	Juan
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
Syn	syn	k1gMnSc1	syn
dona	don	k1gMnSc2	don
Juana	Juan	k1gMnSc2	Juan
<g/>
)	)	kIx)	)
El	Ela	k1gFnPc2	Ela
estigma	estigmum	k1gNnSc2	estigmum
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
,	,	kIx,	,
Znamení	znamení	k1gNnSc1	znamení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mancha	Mancha	k1gMnSc1	Mancha
que	que	k?	que
limpia	limpia	k1gFnSc1	limpia
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mariana	Mariana	k1gFnSc1	Mariana
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
duda	dud	k1gInSc2	dud
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
Nejistota	nejistota	k1gFnSc1	nejistota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
El	Ela	k1gFnPc2	Ela
loco	loco	k6eAd1	loco
dios	dios	k6eAd1	dios
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
Šílený	šílený	k2eAgMnSc1d1	šílený
bůh	bůh	k1gMnSc1	bůh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
A	a	k8xC	a
fuerza	fuerza	k1gFnSc1	fuerza
de	de	k?	de
arrastrarse	arrastrarse	k1gFnSc1	arrastrarse
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Recuerdos	Recuerdos	k1gMnSc1	Recuerdos
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
Vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
Galeotto	Galeotto	k1gNnSc4	Galeotto
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Knapp	Knapp	k1gMnSc1	Knapp
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jan	Jan	k1gMnSc1	Jan
Kühnl	Kühnl	k1gMnSc1	Kühnl
<g/>
,	,	kIx,	,
Světec	světec	k1gMnSc1	světec
či	či	k8xC	či
blázen	blázen	k1gMnSc1	blázen
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
,	,	kIx,	,
1893	[number]	k4	1893
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
<g/>
,	,	kIx,	,
Mariana	Mariana	k1gFnSc1	Mariana
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
<g/>
,	,	kIx,	,
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vrchlický	Vrchlický	k1gMnSc1	Vrchlický
<g/>
,	,	kIx,	,
Mstitel	mstitel	k1gMnSc1	mstitel
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Knapp	Knapp	k1gMnSc1	Knapp
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Bohdan	Bohdana	k1gFnPc2	Bohdana
K.	K.	kA	K.
Lachmann	Lachmann	k1gMnSc1	Lachmann
<g/>
,	,	kIx,	,
Hodinář	hodinář	k1gMnSc1	hodinář
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
R.	R.	kA	R.
Vilímek	Vilímek	k1gMnSc1	Vilímek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
K.	K.	kA	K.
Drzka	Drzek	k1gInSc2	Drzek
<g/>
,	,	kIx,	,
Šílený	šílený	k2eAgMnSc1d1	šílený
bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
Nakladatelské	nakladatelský	k2eAgNnSc1d1	nakladatelské
družstvo	družstvo	k1gNnSc1	družstvo
Máje	máje	k1gFnSc1	máje
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1937	[number]	k4	1937
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
a	a	k8xC	a
úvod	úvod	k1gInSc1	úvod
Jaromír	Jaromíra	k1gFnPc2	Jaromíra
Borecký	borecký	k2eAgMnSc1d1	borecký
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
José	Josý	k2eAgFnSc2d1	Josý
Echegaray	Echegaraa	k1gFnSc2	Echegaraa
y	y	k?	y
Eizaguirre	Eizaguirr	k1gInSc5	Eizaguirr
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Autor	autor	k1gMnSc1	autor
José	Josá	k1gFnSc2	Josá
de	de	k?	de
Echegaray	Echegaraa	k1gFnSc2	Echegaraa
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
José	José	k1gNnSc1	José
Echegaray	Echegaraa	k1gFnSc2	Echegaraa
y	y	k?	y
Eizaguirre	Eizaguirr	k1gInSc5	Eizaguirr
Nobel	Nobel	k1gMnSc1	Nobel
Prize	Prize	k1gFnSc2	Prize
bio	bio	k?	bio
http://nobelprize.org/nobel_prizes/literature/laureates/1904/eizaguirre-bio.html	[url]	k1gMnSc1	http://nobelprize.org/nobel_prizes/literature/laureates/1904/eizaguirre-bio.html
http://literature.nobel.brainparad.com/echegaray_jose.html	[url]	k1gMnSc1	http://literature.nobel.brainparad.com/echegaray_jose.html
</s>
