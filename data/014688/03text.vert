<s>
Žebříček	žebříček	k1gInSc1
ATP	atp	kA
</s>
<s>
Žebříček	žebříček	k1gInSc1
ATPFedEx	ATPFedEx	k1gInSc1
ATP	atp	kA
Rankings________________________Současné	Rankings________________________Současný	k2eAgFnSc2d1
světové	světový	k2eAgFnSc2d1
jedničky	jednička	k1gFnSc2
</s>
<s>
Novak	Novak	k6eAd1
DjokovićSrbsko	DjokovićSrbsko	k1gNnSc4
Srbsko	Srbsko	k1gNnSc1
<g/>
5	#num#	k4
<g/>
.	.	kIx.
období	období	k1gNnSc6
ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
od	od	k7c2
3	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2020	#num#	k4
(	(	kIx(
<g/>
462	#num#	k4
dní	den	k1gInPc2
<g/>
)	)	kIx)
<g/>
Mate	mást	k5eAaImIp3nS
Pavić	Pavić	k1gMnSc1
Chorvatsko	Chorvatsko	k1gNnSc4
Chorvatsko	Chorvatsko	k1gNnSc1
<g/>
2	#num#	k4
<g/>
.	.	kIx.
období	období	k1gNnSc6
ve	v	k7c6
čtyřhře	čtyřhra	k1gFnSc6
od	od	k7c2
5	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
(	(	kIx(
<g/>
35	#num#	k4
dní	den	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
__________________________Žebříček	__________________________Žebříček	k1gInSc1
zavedendvouhra	zavedendvouhra	k1gFnSc1
<g/>
:	:	kIx,
23	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1973	#num#	k4
<g/>
čtyřhra	čtyřhra	k1gFnSc1
<g/>
:	:	kIx,
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1976	#num#	k4
</s>
<s>
Žebříček	žebříček	k1gInSc1
ATP	atp	kA
(	(	kIx(
<g/>
oficiálně	oficiálně	k6eAd1
anglicky	anglicky	k6eAd1
FedEx	FedEx	kA
ATP	atp	kA
Rankings	Rankings	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
pořadí	pořadí	k1gNnSc1
tenistů	tenista	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yRgNnPc1
sestavuje	sestavovat	k5eAaImIp3nS
Asociace	asociace	k1gFnSc1
tenisových	tenisový	k2eAgMnPc2d1
profesionálů	profesionál	k1gMnPc2
(	(	kIx(
<g/>
ATP	atp	kA
<g/>
)	)	kIx)
na	na	k7c6
základě	základ	k1gInSc6
dlouhodobých	dlouhodobý	k2eAgInPc2d1
výsledků	výsledek	k1gInPc2
profesionálních	profesionální	k2eAgInPc2d1
turnajů	turnaj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktualizace	aktualizace	k1gFnSc1
pořadí	pořadí	k1gNnSc2
je	být	k5eAaImIp3nS
zveřejňována	zveřejňován	k2eAgFnSc1d1
každé	každý	k3xTgNnSc4
pondělí	pondělí	k1gNnSc4
(	(	kIx(
<g/>
vyjma	vyjma	k7c2
grandslamů	grandslam	k1gInPc2
a	a	k8xC
dvoutýdenních	dvoutýdenní	k2eAgFnPc2d1
událostí	událost	k1gFnPc2
Indian	Indiana	k1gFnPc2
Wells	Wellsa	k1gFnPc2
Masters	Masters	k1gInSc1
a	a	k8xC
Miami	Miami	k1gNnSc1
Masters	Mastersa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týdenní	týdenní	k2eAgFnSc1d1
periodicita	periodicita	k1gFnSc1
započala	započnout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Klasifikace	klasifikace	k1gFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
body	bod	k1gInPc4
získané	získaný	k2eAgInPc1d1
na	na	k7c6
okruzích	okruh	k1gInPc6
ATP	atp	kA
a	a	k8xC
ITF	ITF	kA
za	za	k7c4
předchozích	předchozí	k2eAgInPc2d1
52	#num#	k4
týdnů	týden	k1gInPc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
byla	být	k5eAaImAgFnS
v	v	k7c6
tenise	tenis	k1gInSc6
oficiálně	oficiálně	k6eAd1
zakotvena	zakotven	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
„	„	k?
<g/>
světové	světový	k2eAgFnPc4d1
jedničky	jednička	k1gFnPc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgNnSc1
vydání	vydání	k1gNnSc1
singlového	singlový	k2eAgInSc2d1
žebříčku	žebříček	k1gInSc2
proběhlo	proběhnout	k5eAaPmAgNnS
23	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1973	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Deblový	deblový	k2eAgInSc1d1
žebříček	žebříček	k1gInSc1
byl	být	k5eAaImAgInS
poprvé	poprvé	k6eAd1
zveřejněn	zveřejnit	k5eAaPmNgInS
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1976	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
hráče	hráč	k1gMnPc4
dvouhry	dvouhra	k1gFnSc2
a	a	k8xC
jednotlivce	jednotlivec	k1gMnSc2
ve	v	k7c6
čtyřhře	čtyřhra	k1gFnSc6
jsou	být	k5eAaImIp3nP
sestavovány	sestavován	k2eAgFnPc1d1
samostatné	samostatný	k2eAgFnPc1d1
vydání	vydání	k1gNnSc4
klasifikace	klasifikace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
základě	základ	k1gInSc6
pořadí	pořadí	k1gNnSc2
se	se	k3xPyFc4
hráči	hráč	k1gMnPc1
kvalifikují	kvalifikovat	k5eAaBmIp3nP
do	do	k7c2
jednotlivých	jednotlivý	k2eAgInPc2d1
turnajů	turnaj	k1gInPc2
různé	různý	k2eAgFnSc2d1
úrovně	úroveň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
postavení	postavení	k1gNnSc2
se	se	k3xPyFc4
také	také	k9
vychází	vycházet	k5eAaImIp3nS
při	při	k7c6
nasazování	nasazování	k1gNnSc6
v	v	k7c6
soutěži	soutěž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Především	především	k6eAd1
masmédii	masmédium	k1gNnPc7
je	být	k5eAaImIp3nS
žebříček	žebříček	k1gInSc1
používán	používat	k5eAaImNgInS
jako	jako	k8xS,k8xC
snadný	snadný	k2eAgInSc1d1
způsob	způsob	k1gInSc1
poměřování	poměřování	k1gNnSc2
úrovně	úroveň	k1gFnSc2
jednotlivých	jednotlivý	k2eAgMnPc2d1
tenistů	tenista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Vedle	vedle	k7c2
průběžného	průběžný	k2eAgInSc2d1
52	#num#	k4
<g/>
týdenního	týdenní	k2eAgInSc2d1
žebříčku	žebříček	k1gInSc2
pro	pro	k7c4
nasazování	nasazování	k1gNnSc4
do	do	k7c2
turnajů	turnaj	k1gInPc2
existuje	existovat	k5eAaImIp3nS
také	také	k9
klasifikace	klasifikace	k1gFnSc1
ATP	atp	kA
Race	Race	k1gFnSc1
(	(	kIx(
<g/>
tzv.	tzv.	kA
Race	Race	k1gNnSc1
to	ten	k3xDgNnSc1
[	[	kIx(
<g/>
city	city	k1gNnSc1
<g/>
]	]	kIx)
–	–	k?
závod	závod	k1gInSc1
do	do	k7c2
[	[	kIx(
<g/>
města	město	k1gNnSc2
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
začátku	začátek	k1gInSc6
každé	každý	k3xTgFnSc2
nové	nový	k2eAgFnSc2d1
sezóny	sezóna	k1gFnSc2
–	–	k?
v	v	k7c6
lednu	leden	k1gInSc6
–	–	k?
anulována	anulován	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
žebříček	žebříček	k1gInSc4
dvouhry	dvouhra	k1gFnSc2
a	a	k8xC
párů	pár	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATP	atp	kA
Race	Race	k1gFnPc2
je	být	k5eAaImIp3nS
rozhodující	rozhodující	k2eAgFnSc1d1
pro	pro	k7c4
účast	účast	k1gFnSc4
nejlepších	dobrý	k2eAgMnPc2d3
singlistů	singlista	k1gMnPc2
a	a	k8xC
deblových	deblový	k2eAgInPc2d1
párů	pár	k1gInPc2
na	na	k7c6
závěrečném	závěrečný	k2eAgInSc6d1
turnaji	turnaj	k1gInSc6
kalendářního	kalendářní	k2eAgInSc2d1
roku	rok	k1gInSc2
ATP	atp	kA
Finals	Finals	k1gInSc1
–	–	k?
Turnaji	turnaj	k1gInPc7
mistrů	mistr	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1973	#num#	k4
bylo	být	k5eAaImAgNnS
světovými	světový	k2eAgFnPc7d1
jedničkami	jednička	k1gFnPc7
26	#num#	k4
hráčů	hráč	k1gMnPc2
dvouhry	dvouhra	k1gFnSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
17	#num#	k4
zakončilo	zakončit	k5eAaPmAgNnS
na	na	k7c6
této	tento	k3xDgFnSc6
pozici	pozice	k1gFnSc6
sezónu	sezóna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
počátku	počátek	k1gInSc6
února	únor	k1gInSc2
2020	#num#	k4
se	se	k3xPyFc4
do	do	k7c2
čela	čelo	k1gNnSc2
klasifikace	klasifikace	k1gFnSc2
počtvrté	počtvrté	k4xO
vrátil	vrátit	k5eAaPmAgMnS
Srb	Srb	k1gMnSc1
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
díky	díky	k7c3
titulu	titul	k1gInSc3
na	na	k7c4
Australian	Australian	k1gInSc4
Open	Open	k1gInSc4
2020	#num#	k4
a	a	k8xC
zahájil	zahájit	k5eAaPmAgMnS
páté	pátý	k4xOgNnSc4
období	období	k1gNnSc4
v	v	k7c6
roli	role	k1gFnSc6
jedničky	jednička	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
čtyřhře	čtyřhra	k1gFnSc6
se	se	k3xPyFc4
na	na	k7c6
prvním	první	k4xOgNnSc6
místě	místo	k1gNnSc6
vystřídalo	vystřídat	k5eAaPmAgNnS
celkem	celkem	k6eAd1
54	#num#	k4
tenistů	tenista	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deblovou	deblový	k2eAgFnSc7d1
světovou	světový	k2eAgFnSc7d1
jedničkou	jednička	k1gFnSc7
se	se	k3xPyFc4
po	po	k7c6
triumfu	triumf	k1gInSc6
Miami	Miami	k1gNnSc2
Open	Open	k1gInSc4
2021	#num#	k4
stal	stát	k5eAaPmAgMnS
podruhé	podruhé	k6eAd1
Chorvat	Chorvat	k1gMnSc1
Mate	mást	k5eAaImIp3nS
Pavić	Pavić	k1gFnSc4
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Žebříček	žebříček	k1gInSc1
</s>
<s>
ATP	atp	kA
vznikla	vzniknout	k5eAaPmAgFnS
jako	jako	k9
odborová	odborový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Iniciátory	iniciátor	k1gInPc1
založení	založení	k1gNnSc2
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
Jack	Jack	k1gMnSc1
Kramer	Kramer	k1gMnSc1
<g/>
,	,	kIx,
Cliff	Cliff	k1gMnSc1
Drysdale	Drysdala	k1gFnSc6
a	a	k8xC
Donald	Donald	k1gMnSc1
Dell	Dell	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Význam	význam	k1gInSc1
organizace	organizace	k1gFnSc2
vzrostl	vzrůst	k5eAaPmAgInS
po	po	k7c6
Wimbledonu	Wimbledon	k1gInSc6
1973	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
81	#num#	k4
členů	člen	k1gMnPc2
bojkotovalo	bojkotovat	k5eAaImAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Dva	dva	k4xCgInPc4
měsíce	měsíc	k1gInPc4
poté	poté	k6eAd1
<g/>
,	,	kIx,
během	během	k7c2
srpna	srpen	k1gInSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
zaveden	zavést	k5eAaPmNgInS
bodový	bodový	k2eAgInSc1d1
žebříčkový	žebříčkový	k2eAgInSc1d1
systém	systém	k1gInSc1
k	k	k7c3
objektivizaci	objektivizace	k1gFnSc3
účasti	účast	k1gFnSc2
a	a	k8xC
nasazování	nasazování	k1gNnSc1
hráčů	hráč	k1gMnPc2
na	na	k7c6
turnajích	turnaj	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nový	nový	k2eAgInSc1d1
systém	systém	k1gInSc1
žebříčku	žebříček	k1gInSc2
ATP	atp	kA
byl	být	k5eAaImAgMnS
rychle	rychle	k6eAd1
implementován	implementovat	k5eAaImNgMnS
do	do	k7c2
mužského	mužský	k2eAgInSc2d1
tenisu	tenis	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Zatímco	zatímco	k8xS
drtivá	drtivý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
členů	člen	k1gMnPc2
ATP	atp	kA
se	se	k3xPyFc4
vyslovila	vyslovit	k5eAaPmAgFnS
ve	v	k7c4
prospěch	prospěch	k1gInSc4
objektivního	objektivní	k2eAgInSc2d1
přístupu	přístup	k1gInSc2
na	na	k7c4
turnaje	turnaj	k1gInPc4
podle	podle	k7c2
klasifikace	klasifikace	k1gFnSc2
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
jednička	jednička	k1gFnSc1
ATP	atp	kA
Ilie	Ilie	k1gFnSc1
Năstase	Năstas	k1gInSc6
se	se	k3xPyFc4
vyjádřila	vyjádřit	k5eAaPmAgNnP
kriticky	kriticky	k6eAd1
<g/>
,	,	kIx,
protože	protože	k8xS
„	„	k?
<g/>
každému	každý	k3xTgMnSc3
bylo	být	k5eAaImAgNnS
přiděleno	přidělit	k5eAaPmNgNnS
číslo	číslo	k1gNnSc1
visící	visící	k2eAgNnSc1d1
nad	nad	k7c7
dalšími	další	k2eAgInPc7d1
<g/>
,	,	kIx,
<g/>
“	“	k?
což	což	k3yRnSc1,k3yQnSc1
mezi	mezi	k7c7
tenisty	tenista	k1gMnPc7
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
více	hodně	k6eAd2
konkurenční	konkurenční	k2eAgInSc1d1
a	a	k8xC
méně	málo	k6eAd2
kolegiální	kolegiální	k2eAgFnSc6d1
atmosféře	atmosféra	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Původní	původní	k2eAgNnPc4d1
kritéria	kritérion	k1gNnPc4
k	k	k7c3
sestavování	sestavování	k1gNnSc3
žebříčku	žebříček	k1gInSc2
ATP	atp	kA
<g/>
,	,	kIx,
s	s	k7c7
týdenní	týdenní	k2eAgFnSc7d1
periodicitou	periodicita	k1gFnSc7
až	až	k6eAd1
od	od	k7c2
poloviny	polovina	k1gFnSc2
roku	rok	k1gInSc2
1979	#num#	k4
<g/>
,	,	kIx,
vycházela	vycházet	k5eAaImAgFnS
ze	z	k7c2
zprůměrování	zprůměrování	k1gNnSc2
bodových	bodový	k2eAgInPc2d1
výsledků	výsledek	k1gInPc2
hráče	hráč	k1gMnPc4
na	na	k7c6
všech	všecek	k3xTgInPc6
turnajích	turnaj	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
byla	být	k5eAaImAgFnS
metodika	metodika	k1gFnSc1
několikrát	několikrát	k6eAd1
revidována	revidovat	k5eAaImNgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
sezóny	sezóna	k1gFnSc2
1990	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ATP	atp	kA
získala	získat	k5eAaPmAgFnS
dominantní	dominantní	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
a	a	k8xC
kdy	kdy	k6eAd1
vznikl	vzniknout	k5eAaPmAgInS
okruh	okruh	k1gInSc1
ATP	atp	kA
Tour	Tour	k1gInSc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgNnP
žebříčková	žebříčkový	k2eAgNnPc1d1
kritéria	kritérion	k1gNnPc1
nahrazena	nahradit	k5eAaPmNgFnS
systémem	systém	k1gInSc7
nejlepších	dobrý	k2eAgInPc2d3
výsledků	výsledek	k1gInPc2
po	po	k7c6
vzoru	vzor	k1gInSc6
soutěžního	soutěžní	k2eAgNnSc2d1
alpského	alpský	k2eAgNnSc2d1
lyžování	lyžování	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Původně	původně	k6eAd1
se	se	k3xPyFc4
započítávaly	započítávat	k5eAaImAgInP
nejlepší	dobrý	k2eAgInPc1d3
výsledky	výsledek	k1gInPc1
ze	z	k7c2
čtrnácti	čtrnáct	k4xCc2
událostí	událost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rozšíření	rozšíření	k1gNnSc3
na	na	k7c4
osmnáct	osmnáct	k4xCc4
turnajů	turnaj	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
je	být	k5eAaImIp3nS
maximální	maximální	k2eAgInSc4d1
bodový	bodový	k2eAgInSc4d1
zisk	zisk	k1gInSc4
generován	generován	k2eAgInSc4d1
ze	z	k7c2
zápočtu	zápočet	k1gInSc2
povinně	povinně	k6eAd1
hraných	hraný	k2eAgInPc2d1
4	#num#	k4
Grand	grand	k1gMnSc1
Slamů	slam	k1gInPc2
<g/>
,	,	kIx,
8	#num#	k4
Mastersů	Masters	k1gInPc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
5	#num#	k4
turnajů	turnaj	k1gInPc2
v	v	k7c6
kategorii	kategorie	k1gFnSc6
ATP	atp	kA
500	#num#	k4
včetně	včetně	k7c2
bodově	bodově	k6eAd1
sem	sem	k6eAd1
náležíjícího	náležíjící	k2eAgInSc2d1
Monte-Carlo	Monte-Carlo	k1gNnSc4
Masters	Mastersa	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
maximální	maximální	k2eAgInSc4d1
zisk	zisk	k1gInSc4
19	#num#	k4
500	#num#	k4
bodů	bod	k1gInPc2
před	před	k7c7
ATP	atp	kA
Finals	Finals	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
po	po	k7c6
jeho	jeho	k3xOp3gNnSc6
skončení	skončení	k1gNnSc6
21	#num#	k4
000	#num#	k4
bodů	bod	k1gInPc2
v	v	k7c6
konečné	konečný	k2eAgFnSc6d1
klasifikaci	klasifikace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Žebříčky	žebříček	k1gInPc1
ATP	atp	kA
a	a	k8xC
WTA	WTA	kA
byly	být	k5eAaImAgFnP
16	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2020	#num#	k4
zmrazeny	zmrazit	k5eAaPmNgFnP
v	v	k7c6
důsledku	důsledek	k1gInSc6
koronavirové	koronavirový	k2eAgFnSc2d1
pandemie	pandemie	k1gFnSc2
na	na	k7c4
22	#num#	k4
týdnů	týden	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Ženská	ženský	k2eAgFnSc1d1
světová	světový	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
byla	být	k5eAaImAgFnS
obnovena	obnoven	k2eAgFnSc1d1
10	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2020	#num#	k4
se	se	k3xPyFc4
znovurozehráním	znovurozehrání	k1gNnSc7
sezóny	sezóna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mužský	mužský	k2eAgInSc4d1
žebříček	žebříček	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
vedl	vést	k5eAaImAgInS
Novak	Novak	k1gInSc4
Djoković	Djoković	k1gMnPc2
<g/>
,	,	kIx,
na	na	k7c4
ni	on	k3xPp3gFnSc4
navázal	navázat	k5eAaPmAgInS
24	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
s	s	k7c7
úvodním	úvodní	k2eAgInSc7d1
turnajem	turnaj	k1gInSc7
po	po	k7c6
přerušení	přerušení	k1gNnSc6
Cincinnati	Cincinnati	k1gFnSc2
Masters	Mastersa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Světové	světový	k2eAgFnPc1d1
jedničky	jednička	k1gFnPc1
</s>
<s>
Rumun	Rumun	k1gMnSc1
Ilie	Ilie	k1gNnSc2
Năstase	Năstas	k1gInSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
23	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1973	#num#	k4
historicky	historicky	k6eAd1
první	první	k4xOgFnSc7
světovou	světový	k2eAgFnSc7d1
jedničkou	jednička	k1gFnSc7
na	na	k7c6
žebříčku	žebříček	k1gInSc6
ATP	atp	kA
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1973	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
čele	čelo	k1gNnSc6
singlové	singlový	k2eAgFnSc2d1
klasifikace	klasifikace	k1gFnSc2
vystřídalo	vystřídat	k5eAaPmAgNnS
dvacet	dvacet	k4xCc1
šest	šest	k4xCc1
tenistů	tenista	k1gMnPc2
–	–	k?
tzv.	tzv.	kA
světových	světový	k2eAgFnPc2d1
jedniček	jednička	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Američan	Američan	k1gMnSc1
Pete	Pet	k1gMnSc2
Sampras	Sampras	k1gMnSc1
se	s	k7c7
Srbem	Srb	k1gMnSc7
Novakem	Novak	k1gMnSc7
Djokovićem	Djoković	k1gMnSc7
drží	držet	k5eAaImIp3nS
rekord	rekord	k1gInSc4
v	v	k7c6
počtu	počet	k1gInSc6
šesti	šest	k4xCc2
sezón	sezóna	k1gFnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
zakončili	zakončit	k5eAaPmAgMnP
na	na	k7c6
prvním	první	k4xOgNnSc6
místě	místo	k1gNnSc6
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Sampras	Samprasa	k1gFnPc2
navíc	navíc	k6eAd1
bez	bez	k7c2
přerušení	přerušení	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
také	také	k9
strávil	strávit	k5eAaPmAgMnS
na	na	k7c6
čele	čelo	k1gNnSc6
klasifikace	klasifikace	k1gFnSc2
nejvyšší	vysoký	k2eAgInSc4d3
počet	počet	k1gInSc4
týdnů	týden	k1gInPc2
<g/>
,	,	kIx,
když	když	k8xS
8	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
zahájil	zahájit	k5eAaPmAgMnS
311	#num#	k4
týden	týden	k1gInSc4
na	na	k7c6
pozici	pozice	k1gFnSc6
světové	světový	k2eAgFnSc2d1
jedničky	jednička	k1gFnSc2
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
překonal	překonat	k5eAaPmAgMnS
Rogera	Roger	k1gMnSc4
Federera	Federer	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Švýcarovi	Švýcar	k1gMnSc3
patří	patřit	k5eAaImIp3nS
první	první	k4xOgNnSc1
místo	místo	k1gNnSc1
v	v	k7c6
nejdelším	dlouhý	k2eAgNnSc6d3
období	období	k1gNnSc6
bez	bez	k7c2
přerušení	přerušení	k1gNnSc2
<g/>
,	,	kIx,
když	když	k8xS
vrchol	vrchol	k1gInSc4
neopustil	opustit	k5eNaPmAgMnS
237	#num#	k4
týdnů	týden	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dva	dva	k4xCgMnPc1
hráči	hráč	k1gMnPc1
<g/>
,	,	kIx,
československý	československý	k2eAgMnSc1d1
reprezentant	reprezentant	k1gMnSc1
Ivan	Ivan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
a	a	k8xC
Chilan	Chilan	k1gMnSc1
Marcelo	Marcela	k1gFnSc5
Ríos	Ríos	k1gInSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
na	na	k7c4
vrchol	vrchol	k1gInSc4
dostali	dostat	k5eAaPmAgMnP
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nP
předtím	předtím	k6eAd1
vyhráli	vyhrát	k5eAaPmAgMnP
jeden	jeden	k4xCgInSc4
z	z	k7c2
Grand	grand	k1gMnSc1
Slamů	slam	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Lendl	Lendl	k1gFnPc6
opanoval	opanovat	k5eAaPmAgInS
čelo	čelo	k1gNnSc4
21	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1983	#num#	k4
a	a	k8xC
premiérový	premiérový	k2eAgMnSc1d1
major	major	k1gMnSc1
získal	získat	k5eAaPmAgMnS
na	na	k7c4
French	French	k1gInSc4
Open	Opena	k1gFnPc2
1984	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Rios	Rios	k1gInSc1
klasifikaci	klasifikace	k1gFnSc6
ovládl	ovládnout	k5eAaPmAgInS
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1998	#num#	k4
a	a	k8xC
zůstává	zůstávat	k5eAaImIp3nS
jedinou	jediný	k2eAgFnSc7d1
světovou	světový	k2eAgFnSc7d1
jedničkou	jednička	k1gFnSc7
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc7
se	se	k3xPyFc4
nepodařilo	podařit	k5eNaPmAgNnS
dosáhnout	dosáhnout	k5eAaPmF
na	na	k7c4
grandslamový	grandslamový	k2eAgInSc4d1
vavřín	vavřín	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Nejkratší	krátký	k2eAgFnSc4d3
dobu	doba	k1gFnSc4
na	na	k7c6
tenisovém	tenisový	k2eAgInSc6d1
trůnu	trůn	k1gInSc6
strávil	strávit	k5eAaPmAgMnS
Australan	Australan	k1gMnSc1
Patrick	Patrick	k1gMnSc1
Rafter	Rafter	k1gMnSc1
a	a	k8xC
to	ten	k3xDgNnSc1
pouze	pouze	k6eAd1
jediný	jediný	k2eAgInSc4d1
týden	týden	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Australský	australský	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
Lleyton	Lleyton	k1gInSc4
Hewitt	Hewitt	k1gInSc1
drží	držet	k5eAaImIp3nS
rekord	rekord	k1gInSc4
nejmladší	mladý	k2eAgFnSc2d3
světové	světový	k2eAgFnSc2d1
jedničky	jednička	k1gFnSc2
–	–	k?
věkem	věk	k1gInSc7
20	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
268	#num#	k4
dní	den	k1gInPc2
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
nejmladšího	mladý	k2eAgMnSc2d3
takového	takový	k3xDgMnSc4
hráče	hráč	k1gMnSc4
v	v	k7c6
závěrečné	závěrečný	k2eAgFnSc6d1
klasifikaci	klasifikace	k1gFnSc6
sezóny	sezóna	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Naopak	naopak	k6eAd1
nejstarším	starý	k2eAgInPc3d3
se	se	k3xPyFc4
v	v	k7c6
tomto	tento	k3xDgInSc6
parametru	parametr	k1gInSc6
stal	stát	k5eAaPmAgMnS
Ivan	Ivan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
<g/>
,	,	kIx,
když	když	k8xS
sezonu	sezona	k1gFnSc4
1989	#num#	k4
zakončil	zakončit	k5eAaPmAgInS
ve	v	k7c6
věku	věk	k1gInSc6
29	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
299	#num#	k4
dní	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
si	se	k3xPyFc3
díky	díky	k7c3
finálové	finálový	k2eAgFnSc3d1
účasti	účast	k1gFnSc3
na	na	k7c6
MercedesCupu	MercedesCup	k1gInSc6
2018	#num#	k4
zajistil	zajistit	k5eAaPmAgMnS
pátý	pátý	k4xOgInSc4
návrat	návrat	k1gInSc4
na	na	k7c6
pozici	pozice	k1gFnSc6
světové	světový	k2eAgFnSc2d1
jedničky	jednička	k1gFnSc2
a	a	k8xC
ve	v	k7c6
věku	věk	k1gInSc6
36	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
10	#num#	k4
měsíců	měsíc	k1gInPc2
a	a	k8xC
9	#num#	k4
dní	den	k1gInPc2
se	s	k7c7
18	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2018	#num#	k4
stal	stát	k5eAaPmAgInS
nejstarším	starý	k2eAgMnSc7d3
mužem	muž	k1gMnSc7
na	na	k7c6
této	tento	k3xDgFnSc6
pozici	pozice	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Zároveň	zároveň	k6eAd1
byl	být	k5eAaImAgInS
i	i	k9
nejstarším	starý	k2eAgMnSc7d3
hráčem	hráč	k1gMnSc7
v	v	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
stovce	stovka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překonal	překonat	k5eAaPmAgMnS
tak	tak	k9
věkový	věkový	k2eAgInSc4d1
rekord	rekord	k1gInSc4
tehdy	tehdy	k6eAd1
33	#num#	k4
<g/>
letého	letý	k2eAgMnSc4d1
Andreho	Andre	k1gMnSc4
Agassiho	Agassi	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
figuroval	figurovat	k5eAaImAgMnS
na	na	k7c6
čele	čelo	k1gNnSc6
klasifikace	klasifikace	k1gFnSc2
a	a	k8xC
také	také	k6eAd1
představoval	představovat	k5eAaImAgMnS
nejstaršího	starý	k2eAgMnSc4d3
muže	muž	k1gMnSc4
v	v	k7c6
Top	topit	k5eAaImRp2nS
100	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Již	již	k9
čtvrtý	čtvrtý	k4xOgInSc1
Švýcarův	Švýcarův	k2eAgInSc1d1
návrat	návrat	k1gInSc1
do	do	k7c2
čela	čelo	k1gNnSc2
v	v	k7c6
únoru	únor	k1gInSc6
2018	#num#	k4
se	se	k3xPyFc4
uskutečnil	uskutečnit	k5eAaPmAgInS
po	po	k7c6
5	#num#	k4
letech	let	k1gInPc6
a	a	k8xC
106	#num#	k4
dnech	den	k1gInPc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamenalo	znamenat	k5eAaImAgNnS
nejdelší	dlouhý	k2eAgNnSc1d3
období	období	k1gNnSc1
mezi	mezi	k7c7
dvěma	dva	k4xCgNnPc7
kralováními	kralování	k1gNnPc7
mužskému	mužský	k2eAgInSc3d1
tenisu	tenis	k1gInSc3
<g/>
,	,	kIx,
když	když	k8xS
jedničkou	jednička	k1gFnSc7
nebyl	být	k5eNaImAgInS
mezi	mezi	k7c7
listopadem	listopad	k1gInSc7
2012	#num#	k4
a	a	k8xC
únorem	únor	k1gInSc7
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejčastěji	často	k6eAd3
se	se	k3xPyFc4
světové	světový	k2eAgFnPc1d1
jedničky	jednička	k1gFnPc1
střídaly	střídat	k5eAaImAgFnP
v	v	k7c6
sezóně	sezóna	k1gFnSc6
1983	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
na	na	k7c6
vrcholu	vrchol	k1gInSc6
klasifikace	klasifikace	k1gFnSc2
proměnilo	proměnit	k5eAaPmAgNnS
jméno	jméno	k1gNnSc1
tenisty	tenista	k1gMnPc4
desetkrát	desetkrát	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
prvního	první	k4xOgMnSc4
hráče	hráč	k1gMnSc4
rankingu	ranking	k1gInSc2
sváděli	svádět	k5eAaImAgMnP
daný	daný	k2eAgInSc4d1
rok	rok	k1gInSc4
boj	boj	k1gInSc1
John	John	k1gMnSc1
McEnroe	McEnroe	k1gFnSc1
<g/>
,	,	kIx,
Jimmy	Jimmo	k1gNnPc7
Connors	Connors	k1gInSc1
a	a	k8xC
Ivan	Ivan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Postupem	postup	k1gInSc7
do	do	k7c2
finále	finále	k1gNnSc2
pařížského	pařížský	k2eAgInSc2d1
BNP	BNP	kA
Paribas	Paribas	k1gInSc1
Masters	Masters	k1gInSc1
se	s	k7c7
29	#num#	k4
<g/>
letý	letý	k2eAgInSc1d1
Skot	skot	k1gInSc1
Andy	Anda	k1gFnSc2
Murray	Murraa	k1gFnSc2
stal	stát	k5eAaPmAgInS
7	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2016	#num#	k4
dvacátou	dvacátý	k4xOgFnSc4
šestou	šestý	k4xOgFnSc7
světovou	světový	k2eAgFnSc7d1
jedničkou	jednička	k1gFnSc7
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
první	první	k4xOgMnSc1
britský	britský	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
a	a	k8xC
nejstarší	starý	k2eAgMnSc1d3
debutant	debutant	k1gMnSc1
na	na	k7c6
této	tento	k3xDgFnSc6
pozici	pozice	k1gFnSc6
od	od	k7c2
června	červen	k1gInSc2
1974	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
do	do	k7c2
čela	čelo	k1gNnSc2
klasifikace	klasifikace	k1gFnSc2
vystoupal	vystoupat	k5eAaPmAgInS
30	#num#	k4
<g/>
letý	letý	k2eAgMnSc1d1
Australan	Australan	k1gMnSc1
John	John	k1gMnSc1
Newcombe	Newcomb	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovládnout	ovládnout	k5eAaPmF
pořadí	pořadí	k1gNnSc4
hráčů	hráč	k1gMnPc2
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podařilo	podařit	k5eAaPmAgNnS
ve	v	k7c4
své	své	k1gNnSc4
12	#num#	k4
<g/>
.	.	kIx.
profesionální	profesionální	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
a	a	k8xC
sedm	sedm	k4xCc4
let	léto	k1gNnPc2
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
poprvé	poprvé	k6eAd1
stal	stát	k5eAaPmAgMnS
světovou	světový	k2eAgFnSc7d1
dvojkou	dvojka	k1gFnSc7
<g/>
,	,	kIx,
když	když	k8xS
na	na	k7c6
tomto	tento	k3xDgNnSc6
místě	místo	k1gNnSc6
strávil	strávit	k5eAaPmAgMnS
76	#num#	k4
týdnů	týden	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Více	hodně	k6eAd2
týdnů	týden	k1gInPc2
na	na	k7c6
druhé	druhý	k4xOgFnSc6
příčce	příčka	k1gFnSc6
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
jedničkami	jednička	k1gFnPc7
<g/>
,	,	kIx,
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
vydrželi	vydržet	k5eAaPmAgMnP
pouze	pouze	k6eAd1
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
(	(	kIx(
<g/>
160	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Boris	Boris	k1gMnSc1
Becker	Becker	k1gMnSc1
(	(	kIx(
<g/>
108	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
předešlý	předešlý	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
,	,	kIx,
z	z	k7c2
něhož	jenž	k3xRgNnSc2
se	se	k3xPyFc4
žebříček	žebříček	k1gInSc1
tvořil	tvořit	k5eAaImAgInS
<g/>
,	,	kIx,
dosáhl	dosáhnout	k5eAaPmAgInS
Skot	skot	k1gInSc1
zápasové	zápasový	k2eAgFnSc2d1
bilance	bilance	k1gFnSc2
76	#num#	k4
<g/>
–	–	k?
<g/>
11	#num#	k4
se	s	k7c7
ziskem	zisk	k1gInSc7
sedmi	sedm	k4xCc2
titulů	titul	k1gInPc2
z	z	k7c2
jedenácti	jedenáct	k4xCc2
finále	finále	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Američan	Američan	k1gMnSc1
Mike	Mike	k1gNnSc2
Bryan	Bryan	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
v	v	k7c6
červenci	červenec	k1gInSc6
2018	#num#	k4
zahájil	zahájit	k5eAaPmAgMnS
třinácté	třináctý	k4xOgNnSc4
období	období	k1gNnSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
na	na	k7c4
vrchol	vrchol	k1gInSc4
deblové	deblový	k2eAgFnSc2d1
klasifikace	klasifikace	k1gFnSc2
vrátil	vrátit	k5eAaPmAgInS
ve	v	k7c6
40	#num#	k4
letech	let	k1gInPc6
a	a	k8xC
78	#num#	k4
dnech	den	k1gInPc6
jako	jako	k8xS,k8xC
nejstarší	starý	k2eAgFnSc1d3
světová	světový	k2eAgFnSc1d1
jednička	jednička	k1gFnSc1
ATP	atp	kA
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věkový	věkový	k2eAgInSc1d1
rekord	rekord	k1gInSc1
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
držel	držet	k5eAaImAgMnS
Daniel	Daniel	k1gMnSc1
Nestor	Nestor	k1gMnSc1
ze	z	k7c2
září	září	k1gNnSc2
2012	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
40	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
5	#num#	k4
dní	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
čele	čelo	k1gNnSc6
vydržel	vydržet	k5eAaPmAgInS
jeden	jeden	k4xCgInSc4
rok	rok	k1gInSc4
do	do	k7c2
skončení	skončení	k1gNnSc2
Wimbledonu	Wimbledon	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Způsob	způsob	k1gInSc1
sestavování	sestavování	k1gNnSc2
</s>
<s>
Způsob	způsob	k1gInSc1
započítávání	započítávání	k1gNnSc2
bodů	bod	k1gInPc2
do	do	k7c2
žebříčku	žebříček	k1gInSc2
ATP	atp	kA
se	se	k3xPyFc4
v	v	k7c6
historii	historie	k1gFnSc6
měnil	měnit	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mechanismus	mechanismus	k1gInSc1
výpočtu	výpočet	k1gInSc2
byl	být	k5eAaImAgInS
aktualizován	aktualizovat	k5eAaBmNgInS
od	od	k7c2
sezóny	sezóna	k1gFnSc2
2021	#num#	k4
<g/>
:	:	kIx,
</s>
<s>
Body	bod	k1gInPc1
se	se	k3xPyFc4
přidělují	přidělovat	k5eAaImIp3nP
za	za	k7c4
účast	účast	k1gFnSc4
(	(	kIx(
<g/>
postup	postup	k1gInSc4
do	do	k7c2
dalších	další	k2eAgNnPc2d1
kol	kolo	k1gNnPc2
<g/>
,	,	kIx,
vítězství	vítězství	k1gNnSc1
<g/>
)	)	kIx)
v	v	k7c6
jednotlivých	jednotlivý	k2eAgInPc6d1
turnajích	turnaj	k1gInPc6
okruhu	okruh	k1gInSc2
ATP	atp	kA
<g/>
,	,	kIx,
případně	případně	k6eAd1
i	i	k8xC
challengerech	challenger	k1gInPc6
a	a	k8xC
turnajích	turnaj	k1gInPc6
Futures	Futures	k1gMnSc1
okruhu	okruh	k1gInSc2
ITF	ITF	kA
<g/>
.	.	kIx.
</s>
<s>
Započítávají	započítávat	k5eAaImIp3nP
se	se	k3xPyFc4
body	bod	k1gInPc1
z	z	k7c2
devatenácti	devatenáct	k4xCc2
turnajů	turnaj	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
čtyř	čtyři	k4xCgNnPc2
grandslamů	grandslam	k1gInPc2
<g/>
,	,	kIx,
osmi	osm	k4xCc2
turnajů	turnaj	k1gInPc2
série	série	k1gFnSc2
Masters	Mastersa	k1gFnPc2
a	a	k8xC
sedmi	sedm	k4xCc2
dalších	další	k2eAgMnPc2d1
nejlepších	dobrý	k2eAgMnPc2d3
výsledků	výsledek	k1gInPc2
na	na	k7c6
akcích	akce	k1gFnPc6
ATP	atp	kA
Cup	cup	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
kategoriích	kategorie	k1gFnPc6
ATP	atp	kA
500	#num#	k4
<g/>
,	,	kIx,
ATP	atp	kA
250	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
challengerech	challenger	k1gInPc6
a	a	k8xC
událostech	událost	k1gFnPc6
ITF	ITF	kA
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
se	se	k3xPyFc4
tenista	tenista	k1gMnSc1
některých	některý	k3yIgNnPc6
„	„	k?
<g/>
povinných	povinný	k2eAgInPc2d1
<g/>
“	“	k?
turnajů	turnaj	k1gInPc2
neúčastnil	účastnit	k5eNaImAgInS
<g/>
,	,	kIx,
počítají	počítat	k5eAaImIp3nP
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
namísto	namísto	k7c2
toho	ten	k3xDgNnSc2
body	bod	k1gInPc1
z	z	k7c2
jiného	jiný	k2eAgInSc2d1
turnaje	turnaj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Účastníkům	účastník	k1gMnPc3
závěrečného	závěrečný	k2eAgInSc2d1
Turnaje	turnaj	k1gInSc2
mistrů	mistr	k1gMnPc2
se	se	k3xPyFc4
počítají	počítat	k5eAaImIp3nP
body	bod	k1gInPc1
i	i	k9
z	z	k7c2
tohoto	tento	k3xDgInSc2
turnaje	turnaj	k1gInSc2
(	(	kIx(
<g/>
mají	mít	k5eAaImIp3nP
tedy	tedy	k9
započítány	započítán	k2eAgInPc4d1
body	bod	k1gInPc4
z	z	k7c2
20	#num#	k4
namísto	namísto	k7c2
19	#num#	k4
turnajů	turnaj	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Body	bod	k1gInPc4
za	za	k7c4
turnaje	turnaj	k1gInPc4
ITF	ITF	kA
(	(	kIx(
<g/>
série	série	k1gFnSc1
Futures	Futures	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
započítávají	započítávat	k5eAaImIp3nP
až	až	k9
druhé	druhý	k4xOgNnSc4
pondělí	pondělí	k1gNnSc4
po	po	k7c6
týdnu	týden	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
turnaj	turnaj	k1gInSc1
konal	konat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s>
Body	bod	k1gInPc4
z	z	k7c2
turnajů	turnaj	k1gInPc2
zůstávají	zůstávat	k5eAaImIp3nP
v	v	k7c6
žebříčku	žebříček	k1gInSc6
započítány	započítán	k2eAgInPc1d1
52	#num#	k4
týdnů	týden	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výjimkou	výjimka	k1gFnSc7
jsou	být	k5eAaImIp3nP
body	bod	k1gInPc1
za	za	k7c4
Turnaj	turnaj	k1gInSc4
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
z	z	k7c2
žebříčku	žebříček	k1gInSc2
odečteny	odečten	k2eAgInPc1d1
následující	následující	k2eAgInPc1d1
pondělí	pondělí	k1gNnSc4
po	po	k7c6
posledním	poslední	k2eAgInSc6d1
turnaji	turnaj	k1gInSc6
ATP	atp	kA
následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
revidovaný	revidovaný	k2eAgInSc4d1
žebříček	žebříček	k1gInSc4
během	během	k7c2
koronavirové	koronavirový	k2eAgFnSc2d1
pandemie	pandemie	k1gFnSc2
se	se	k3xPyFc4
řídí	řídit	k5eAaImIp3nP
specifickými	specifický	k2eAgNnPc7d1
pravidly	pravidlo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
nejsou	být	k5eNaImIp3nP
přidělovány	přidělován	k2eAgInPc4d1
body	bod	k1gInPc4
ze	z	k7c2
zápasů	zápas	k1gInPc2
Davisova	Davisův	k2eAgInSc2d1
poháru	pohár	k1gInSc2
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
bodově	bodově	k6eAd1
nebyly	být	k5eNaImAgFnP
ohodnoceny	ohodnocen	k2eAgFnPc1d1
ani	ani	k8xC
Letní	letní	k2eAgFnPc1d1
olympijské	olympijský	k2eAgFnPc1d1
hry	hra	k1gFnPc1
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozdělování	rozdělování	k1gNnSc1
bodů	bod	k1gInPc2
</s>
<s>
ATP	atp	kA
Tour	Tour	k1gMnSc1
</s>
<s>
ATP	atp	kA
Tour	Tour	k1gInSc4
2021	#num#	k4
</s>
<s>
↓	↓	k?
<g/>
kategorie	kategorie	k1gFnSc1
/	/	kIx~
fáze	fáze	k1gFnSc1
<g/>
→	→	k?
</s>
<s>
vítězové	vítěz	k1gMnPc1
</s>
<s>
finalisté	finalista	k1gMnPc1
</s>
<s>
semifinalisté	semifinalista	k1gMnPc1
</s>
<s>
čtvrtfinalisté	čtvrtfinalista	k1gMnPc1
</s>
<s>
16	#num#	k4
v	v	k7c6
kole	kolo	k1gNnSc6
</s>
<s>
32	#num#	k4
v	v	k7c6
kole	kolo	k1gNnSc6
</s>
<s>
64	#num#	k4
v	v	k7c6
kole	kolo	k1gNnSc6
</s>
<s>
128	#num#	k4
v	v	k7c6
kole	kolo	k1gNnSc6
</s>
<s>
QV	QV	kA
</s>
<s>
Q3	Q3	k4
</s>
<s>
Q2	Q2	k4
</s>
<s>
Q1	Q1	k4
</s>
<s>
Grand	grand	k1gMnSc1
Slam	sláma	k1gFnPc2
(	(	kIx(
<g/>
128	#num#	k4
<g/>
S	s	k7c7
<g/>
)	)	kIx)
<g/>
20001200720360180904510251680	#num#	k4
</s>
<s>
Grand	grand	k1gMnSc1
Slam	sláma	k1gFnPc2
(	(	kIx(
<g/>
64	#num#	k4
<g/>
D	D	kA
<g/>
)	)	kIx)
<g/>
20001200720360180900	#num#	k4
<g/>
—	—	k?
<g/>
25	#num#	k4
<g/>
—	—	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
ATP	atp	kA
Finals	Finals	k1gInSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
S	s	k7c7
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
D	D	kA
<g/>
)	)	kIx)
<g/>
1500	#num#	k4
(	(	kIx(
<g/>
max	max	kA
<g/>
)	)	kIx)
<g/>
1100	#num#	k4
(	(	kIx(
<g/>
min	mina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
1000	#num#	k4
(	(	kIx(
<g/>
max	max	kA
<g/>
)	)	kIx)
<g/>
600	#num#	k4
(	(	kIx(
<g/>
min	mina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
600	#num#	k4
(	(	kIx(
<g/>
max	max	kA
<g/>
)	)	kIx)
<g/>
200	#num#	k4
(	(	kIx(
<g/>
min	mina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
200	#num#	k4
za	za	k7c4
každou	každý	k3xTgFnSc4
výhru	výhra	k1gFnSc4
v	v	k7c6
základní	základní	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
<g/>
,	,	kIx,
+400	+400	k4
za	za	k7c4
výhru	výhra	k1gFnSc4
v	v	k7c6
semifinále	semifinále	k1gNnSc6
<g/>
,	,	kIx,
+500	+500	k4
za	za	k7c4
výhru	výhra	k1gFnSc4
ve	v	k7c6
finále	finále	k1gNnSc6
</s>
<s>
ATP	atp	kA
Tour	Tour	k1gInSc1
Masters	Masters	k1gInSc1
1000	#num#	k4
(	(	kIx(
<g/>
96	#num#	k4
<g/>
S	s	k7c7
<g/>
)	)	kIx)
<g/>
10006003601809045251016	#num#	k4
<g/>
—	—	k?
<g/>
80	#num#	k4
</s>
<s>
ATP	atp	kA
Tour	Tour	k1gInSc1
Masters	Masters	k1gInSc1
1000	#num#	k4
(	(	kIx(
<g/>
56	#num#	k4
<g/>
S	s	k7c7
<g/>
/	/	kIx~
<g/>
48	#num#	k4
<g/>
S	s	k7c7
<g/>
)	)	kIx)
<g/>
1000600360180904510	#num#	k4
<g/>
—	—	k?
<g/>
25	#num#	k4
<g/>
—	—	k?
<g/>
160	#num#	k4
</s>
<s>
ATP	atp	kA
Tour	Tour	k1gInSc1
Masters	Masters	k1gInSc1
1000	#num#	k4
(	(	kIx(
<g/>
32	#num#	k4
<g/>
D	D	kA
<g/>
/	/	kIx~
<g/>
24	#num#	k4
<g/>
D	D	kA
<g/>
)	)	kIx)
<g/>
1000600360180900	#num#	k4
<g/>
—	—	k?
</s>
<s>
ATP	atp	kA
Cup	cup	k1gInSc4
<g/>
750	#num#	k4
(	(	kIx(
<g/>
max	max	kA
<g/>
)	)	kIx)
</s>
<s>
ATP	atp	kA
Tour	Tour	k1gInSc4
500	#num#	k4
(	(	kIx(
<g/>
48	#num#	k4
<g/>
S	s	k7c7
<g/>
)	)	kIx)
<g/>
5003001809045200	#num#	k4
<g/>
—	—	k?
<g/>
10	#num#	k4
<g/>
—	—	k?
<g/>
40	#num#	k4
</s>
<s>
ATP	atp	kA
Tour	Tour	k1gInSc4
500	#num#	k4
(	(	kIx(
<g/>
32	#num#	k4
<g/>
S	s	k7c7
<g/>
)	)	kIx)
<g/>
50030018090450	#num#	k4
<g/>
—	—	k?
<g/>
20	#num#	k4
<g/>
—	—	k?
<g/>
100	#num#	k4
</s>
<s>
ATP	atp	kA
Tour	Tour	k1gInSc4
500	#num#	k4
(	(	kIx(
<g/>
16	#num#	k4
<g/>
D	D	kA
<g/>
)	)	kIx)
<g/>
500300180900	#num#	k4
<g/>
—	—	k?
<g/>
20	#num#	k4
<g/>
—	—	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
ATP	atp	kA
Tour	Tour	k1gInSc4
250	#num#	k4
(	(	kIx(
<g/>
56	#num#	k4
<g/>
S	s	k7c7
<g/>
/	/	kIx~
<g/>
48	#num#	k4
<g/>
S	s	k7c7
<g/>
)	)	kIx)
<g/>
250150904520100	#num#	k4
<g/>
—	—	k?
<g/>
5300	#num#	k4
</s>
<s>
ATP	atp	kA
Tour	Tour	k1gInSc4
250	#num#	k4
(	(	kIx(
<g/>
32	#num#	k4
<g/>
S	s	k7c7
<g/>
/	/	kIx~
<g/>
28	#num#	k4
<g/>
S	s	k7c7
<g/>
)	)	kIx)
<g/>
2501509045200	#num#	k4
<g/>
—	—	k?
<g/>
12600	#num#	k4
</s>
<s>
ATP	atp	kA
Tour	Tour	k1gInSc4
250	#num#	k4
(	(	kIx(
<g/>
24	#num#	k4
<g/>
D	D	kA
<g/>
)	)	kIx)
<g/>
2501509045200	#num#	k4
<g/>
—	—	k?
</s>
<s>
ATP	atp	kA
Tour	Tour	k1gInSc4
250	#num#	k4
(	(	kIx(
<g/>
16	#num#	k4
<g/>
D	D	kA
<g/>
)	)	kIx)
<g/>
25015090450	#num#	k4
<g/>
—	—	k?
</s>
<s>
QV	QV	kA
–	–	k?
vítěz	vítěz	k1gMnSc1
kvalifikace	kvalifikace	k1gFnSc2
<g/>
,	,	kIx,
Q	Q	kA
<g/>
1	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
až	až	k9
3	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc4
kvalifikace	kvalifikace	k1gFnSc2
<g/>
,	,	kIx,
(	(	kIx(
<g/>
xS	xS	k?
<g/>
/	/	kIx~
<g/>
D	D	kA
<g/>
)	)	kIx)
–	–	k?
„	„	k?
<g/>
x	x	k?
<g/>
“	“	k?
počet	počet	k1gInSc4
hráčů	hráč	k1gMnPc2
dvouhry	dvouhra	k1gFnSc2
<g/>
/	/	kIx~
<g/>
čtyřhry	čtyřhra	k1gFnSc2
v	v	k7c6
soutěži	soutěž	k1gFnSc6
</s>
<s>
ATP	atp	kA
Challenger	Challenger	k1gMnSc1
Tour	Tour	k1gMnSc1
</s>
<s>
ATP	atp	kA
Challenger	Challenger	k1gMnSc1
Tour	Tour	k1gMnSc1
2021	#num#	k4
</s>
<s>
KategorieDvouhraČtyřhra	KategorieDvouhraČtyřhra	k1gFnSc1
</s>
<s>
vítězové	vítěz	k1gMnPc1
</s>
<s>
finalisté	finalista	k1gMnPc1
</s>
<s>
semifinalisté	semifinalista	k1gMnPc1
</s>
<s>
čtvrtfinalisté	čtvrtfinalista	k1gMnPc1
</s>
<s>
16	#num#	k4
v	v	k7c6
kole	kolo	k1gNnSc6
</s>
<s>
32	#num#	k4
v	v	k7c6
kole	kolo	k1gNnSc6
</s>
<s>
48	#num#	k4
v	v	k7c6
kole	kolo	k1gNnSc6
</s>
<s>
VQ	VQ	kA
</s>
<s>
vítězové	vítěz	k1gMnPc1
</s>
<s>
finalisté	finalista	k1gMnPc1
</s>
<s>
semifinalisté	semifinalista	k1gMnPc1
</s>
<s>
čtvrtfinalisté	čtvrtfinalista	k1gMnPc1
</s>
<s>
16	#num#	k4
v	v	k7c6
kole	kolo	k1gNnSc6
</s>
<s>
Challenger	Challenger	k1gInSc1
125125754525105011257545250	#num#	k4
</s>
<s>
Challenger	Challenger	k1gInSc1
11011065402095011106540200	#num#	k4
</s>
<s>
Challenger	Challenger	k1gInSc1
10010060351885011006035180	#num#	k4
</s>
<s>
Challenger	Challenger	k1gInSc1
90905533178501905533170	#num#	k4
</s>
<s>
Challenger	Challenger	k1gInSc1
80804829157401804829150	#num#	k4
</s>
<s>
VQ	VQ	kA
–	–	k?
vítěz	vítěz	k1gMnSc1
kvalifikace	kvalifikace	k1gFnSc2
</s>
<s>
Mužský	mužský	k2eAgInSc1d1
okruh	okruh	k1gInSc1
ITF	ITF	kA
</s>
<s>
Mužský	mužský	k2eAgInSc1d1
okruh	okruh	k1gInSc1
ITF	ITF	kA
2021	#num#	k4
</s>
<s>
↓	↓	k?
<g/>
kategorie	kategorie	k1gFnSc1
/	/	kIx~
fáze	fáze	k1gFnSc1
<g/>
→	→	k?
</s>
<s>
vítězové	vítěz	k1gMnPc1
</s>
<s>
finalisté	finalista	k1gMnPc1
</s>
<s>
semifinalisté	semifinalista	k1gMnPc1
</s>
<s>
čtvrtfinalisté	čtvrtfinalista	k1gMnPc1
</s>
<s>
16	#num#	k4
v	v	k7c6
kole	kolo	k1gNnSc6
</s>
<s>
32	#num#	k4
v	v	k7c6
kole	kolo	k1gNnSc6
</s>
<s>
ITF	ITF	kA
25	#num#	k4
000	#num#	k4
$	$	kIx~
<g/>
+	+	kIx~
<g/>
H	H	kA
(	(	kIx(
<g/>
S	s	k7c7
<g/>
)	)	kIx)
/	/	kIx~
ITF	ITF	kA
25	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
S	s	k7c7
<g/>
)	)	kIx)
<g/>
2012631	#num#	k4
<g/>
—	—	k?
</s>
<s>
ITF	ITF	kA
25	#num#	k4
000	#num#	k4
$	$	kIx~
<g/>
+	+	kIx~
<g/>
H	H	kA
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
/	/	kIx~
ITF	ITF	kA
25	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
<g/>
201263	#num#	k4
<g/>
—	—	k?
</s>
<s>
ITF	ITF	kA
15	#num#	k4
000	#num#	k4
$	$	kIx~
<g/>
+	+	kIx~
<g/>
H	H	kA
(	(	kIx(
<g/>
S	s	k7c7
<g/>
)	)	kIx)
/	/	kIx~
ITF	ITF	kA
15	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
S	s	k7c7
<g/>
)	)	kIx)
<g/>
106421	#num#	k4
<g/>
—	—	k?
</s>
<s>
ITF	ITF	kA
15	#num#	k4
000	#num#	k4
$	$	kIx~
<g/>
+	+	kIx~
<g/>
H	H	kA
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
/	/	kIx~
ITF	ITF	kA
15	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
<g/>
10642	#num#	k4
<g/>
—	—	k?
</s>
<s>
S	s	k7c7
–	–	k?
dvouzhra	dvouzhra	k1gFnSc1
<g/>
,	,	kIx,
D	D	kA
–	–	k?
čtyřhra	čtyřhra	k1gFnSc1
<g/>
;	;	kIx,
H	H	kA
–	–	k?
turnaj	turnaj	k1gInSc1
hráčům	hráč	k1gMnPc3
zajišťuje	zajišťovat	k5eAaImIp3nS
tzv.	tzv.	kA
Hospitality	Hospitalita	k1gFnSc2
</s>
<s>
Světové	světový	k2eAgFnPc1d1
jedničky	jednička	k1gFnPc1
ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
</s>
<s>
Seznam	seznam	k1gInSc1
uvádí	uvádět	k5eAaImIp3nS
tenisty	tenista	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
dosáhli	dosáhnout	k5eAaPmAgMnP
na	na	k7c6
pozici	pozice	k1gFnSc6
světové	světový	k2eAgFnSc2d1
jedničky	jednička	k1gFnSc2
ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
od	od	k7c2
srpna	srpen	k1gInSc2
1973	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
dvouhry	dvouhra	k1gFnSc2
</s>
<s>
Poř	Poř	k?
<g/>
.	.	kIx.
<g/>
tenistastátprvně	tenistastátprvně	k6eAd1
klasifikovántýdnů	klasifikovántýdn	k1gInPc2
celkem	celkem	k6eAd1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
Novak	Novak	k1gMnSc1
DjokovićSrbsko	DjokovićSrbsko	k1gNnSc4
Srbsko	Srbsko	k1gNnSc1
<g/>
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2011320	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
Roger	Roger	k1gMnSc1
FedererŠvýcarsko	FedererŠvýcarsko	k1gNnSc4
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
2	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2004310	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
Pete	Pete	k1gInSc1
SamprasSpojené	SamprasSpojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
<g/>
12	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1993286	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
<g/>
Ivan	Ivan	k1gMnSc1
LendlČeskoslovensko	LendlČeskoslovensko	k1gNnSc4
Československo	Československo	k1gNnSc1
<g/>
28	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1983270	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
<g/>
Jimmy	Jimm	k1gInPc1
ConnorsSpojené	ConnorsSpojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
<g/>
29	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1974269	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
Rafael	Rafael	k1gMnSc1
NadalŠpanělsko	NadalŠpanělsko	k1gNnSc4
Španělsko	Španělsko	k1gNnSc1
<g/>
18	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2008	#num#	k4
209	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
<g/>
John	John	k1gMnSc1
McEnroeSpojené	McEnroeSpojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgInPc4d1
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgInPc4d1
<g/>
3	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1980170	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
<g/>
Björn	Björn	k1gMnSc1
BorgŠvédsko	BorgŠvédsko	k1gNnSc4
Švédsko	Švédsko	k1gNnSc1
<g/>
23	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1977109	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
Andre	Andr	k1gInSc5
AgassiSpojené	AgassiSpojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
<g/>
10	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1995101	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
<g/>
Lleyton	Lleyton	k1gInSc1
HewittAustrálie	HewittAustrálie	k1gFnSc2
Austrálie	Austrálie	k1gFnSc2
<g/>
19	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
200180	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
<g/>
Stefan	Stefan	k1gMnSc1
EdbergŠvédsko	EdbergŠvédsko	k1gNnSc4
Švédsko	Švédsko	k1gNnSc1
<g/>
13	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
199072	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
<g/>
Jim	on	k3xPp3gMnPc3
CourierSpojené	CourierSpojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
<g/>
10	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
199258	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
<g/>
Gustavo	Gustava	k1gFnSc5
KuertenBrazílie	KuertenBrazílie	k1gFnPc1
Brazílie	Brazílie	k1gFnSc1
<g/>
4	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
200043	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
<g/>
Andy	Anda	k1gFnSc2
MurraySpojené	MurraySpojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
7	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
201641	#num#	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
Ilie	Ilie	k1gFnSc6
NăstaseRumunsko	NăstaseRumunsko	k1gNnSc4
Rumunsko	Rumunsko	k1gNnSc1
<g/>
23	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
197340	#num#	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
<g/>
Mats	Mats	k1gInSc1
WilanderŠvédsko	WilanderŠvédsko	k1gNnSc4
Švédsko	Švédsko	k1gNnSc1
<g/>
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
198820	#num#	k4
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
<g/>
Andy	Anda	k1gFnSc2
RoddickSpojené	RoddickSpojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
<g/>
3	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
200313	#num#	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
<g/>
Boris	Boris	k1gMnSc1
BeckerNěmecko	BeckerNěmecko	k1gNnSc4
Německo	Německo	k1gNnSc1
<g/>
28	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
199112	#num#	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
<g/>
Marat	Marat	k1gInSc1
SafinRusko	SafinRusko	k1gNnSc4
Rusko	Rusko	k1gNnSc1
<g/>
20	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
20009	#num#	k4
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
<g/>
John	John	k1gMnSc1
NewcombeAustrálie	NewcombeAustrálie	k1gFnSc2
Austrálie	Austrálie	k1gFnSc2
<g/>
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
19748	#num#	k4
</s>
<s>
Juan	Juan	k1gMnSc1
Carlos	Carlos	k1gMnSc1
FerreroŠpanělsko	FerreroŠpanělsko	k1gNnSc4
Španělsko	Španělsko	k1gNnSc1
<g/>
8	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
20038	#num#	k4
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
<g/>
Thomas	Thomas	k1gMnSc1
MusterRakousko	MusterRakousko	k1gNnSc4
Rakousko	Rakousko	k1gNnSc1
<g/>
12	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
19966	#num#	k4
</s>
<s>
Marcelo	Marcela	k1gFnSc5
RíosChile	RíosChila	k1gFnSc6
Chile	Chile	k1gNnSc2
<g/>
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
19986	#num#	k4
</s>
<s>
Jevgenij	Jevgenít	k5eAaPmRp2nS
KafelnikovRusko	KafelnikovRusko	k1gNnSc4
Rusko	Rusko	k1gNnSc1
<g/>
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
19996	#num#	k4
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
<g/>
Carlos	Carlos	k1gMnSc1
Moyà	Moyà	k1gNnSc4
Španělsko	Španělsko	k1gNnSc1
<g/>
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
19992	#num#	k4
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
<g/>
Patrick	Patrick	k1gInSc1
RafterAustrálie	RafterAustrálie	k1gFnSc2
Austrálie	Austrálie	k1gFnSc2
<g/>
26	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
19991	#num#	k4
</s>
<s>
–	–	k?
aktivní	aktivní	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
<g/>
;	;	kIx,
tučně	tučně	k6eAd1
–	–	k?
současná	současný	k2eAgFnSc1d1
světová	světový	k2eAgFnSc1d1
jednička	jednička	k1gFnSc1
<g/>
;	;	kIx,
stav	stav	k1gInSc1
k	k	k7c3
10	#num#	k4
<g/>
.	.	kIx.
květnu	květen	k1gInSc3
2021	#num#	k4
</s>
<s>
Bez	bez	k7c2
přerušení	přerušení	k1gNnSc2
–	–	k?
Top	topit	k5eAaImRp2nS
20	#num#	k4
dvouhry	dvouhra	k1gFnSc2
</s>
<s>
Srb	Srb	k1gMnSc1
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
setrval	setrvat	k5eAaPmAgMnS
na	na	k7c6
čele	čelo	k1gNnSc6
žebříčku	žebříček	k1gInSc2
nejdelší	dlouhý	k2eAgNnSc4d3
období	období	k1gNnSc4
<g/>
,	,	kIx,
když	když	k8xS
8	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
překonal	překonat	k5eAaPmAgInS
Federerův	Federerův	k2eAgInSc1d1
rekord	rekord	k1gInSc1
310	#num#	k4
týdnů	týden	k1gInPc2
na	na	k7c4
pozici	pozice	k1gFnSc4
světové	světový	k2eAgFnSc2d1
jedničky	jednička	k1gFnSc2
</s>
<s>
Poř	Poř	k?
<g/>
.	.	kIx.
</s>
<s>
tenista	tenista	k1gMnSc1
</s>
<s>
týdnů	týden	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Federer	Federer	k1gMnSc1
<g/>
,	,	kIx,
RogerRoger	RogerRoger	k1gMnSc1
Federer	Federer	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
237	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Connors	Connors	k1gInSc1
<g/>
,	,	kIx,
JimmyJimmy	JimmyJimm	k1gInPc1
Connors	Connorsa	k1gFnPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
160	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Lendl	Lendnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
IvanIvan	IvanIvan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
157	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Djoković	Djoković	k?
<g/>
,	,	kIx,
NovakNovak	NovakNovak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
122	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Sampras	Sampras	k1gMnSc1
<g/>
,	,	kIx,
PetePete	PetePe	k1gNnSc2
Sampras	Samprasa	k1gFnPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
102	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Connors	Connors	k1gInSc1
<g/>
,	,	kIx,
JimmyJimmy	JimmyJimm	k1gInPc1
Connors	Connorsa	k1gFnPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
84	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Sampras	Sampras	k1gMnSc1
<g/>
,	,	kIx,
PetePete	PetePe	k1gNnSc2
Sampras	Samprasa	k1gFnPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
82	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Lendl	Lendnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
IvanIvan	IvanIvan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
80	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Hewitt	Hewitt	k1gInSc1
<g/>
,	,	kIx,
LleytonLleyton	LleytonLleyton	k1gInSc1
Hewitt	Hewitt	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
75	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
McEnroe	McEnroe	k1gNnSc1
<g/>
,	,	kIx,
JohnJohn	JohnJohn	k1gNnSc1
McEnroe	McEnroe	k1gFnPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
58	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nadal	nadat	k5eAaPmAgMnS
<g/>
,	,	kIx,
RafaelRafael	RafaelRafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
56	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
McEnroe	McEnroe	k1gNnSc1
<g/>
,	,	kIx,
JohnJohn	JohnJohn	k1gNnSc1
McEnroe	McEnroe	k1gFnPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
53	#num#	k4
</s>
<s>
Djoković	Djoković	k?
<g/>
,	,	kIx,
NovakNovak	NovakNovak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
53	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Agassi	Agasse	k1gFnSc4
<g/>
,	,	kIx,
AndreAndre	AndreAndr	k1gInSc5
Agassi	Agass	k1gMnSc6
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
52	#num#	k4
</s>
<s>
Djoković	Djoković	k?
<g/>
,	,	kIx,
NovakNovak	NovakNovak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Federer	Federer	k1gMnSc1
<g/>
,	,	kIx,
RogerRoger	RogerRoger	k1gMnSc1
Federer	Federer	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
48	#num#	k4
</s>
<s>
Djoković	Djoković	k?
<g/>
,	,	kIx,
NovakNovak	NovakNovak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
48	#num#	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Borg	Borg	k1gMnSc1
<g/>
,	,	kIx,
BjörnBjörn	BjörnBjörn	k1gMnSc1
Borg	Borg	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
46	#num#	k4
</s>
<s>
Nadal	nadat	k5eAaPmAgMnS
<g/>
,	,	kIx,
RafaelRafael	RafaelRafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
46	#num#	k4
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Murray	Murray	k1gInPc1
<g/>
,	,	kIx,
AndyAndy	AndyAnd	k1gInPc1
Murray	Murraa	k1gFnSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
41	#num#	k4
</s>
<s>
–	–	k?
aktivní	aktivní	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
<g/>
;	;	kIx,
tučně	tučně	k6eAd1
–	–	k?
aktuální	aktuální	k2eAgNnSc4d1
období	období	k1gNnSc4
současné	současný	k2eAgFnSc2d1
světové	světový	k2eAgFnSc2d1
jedničky	jednička	k1gFnSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
probíhá	probíhat	k5eAaImIp3nS
v	v	k7c6
rámci	rámec	k1gInSc6
Top	topit	k5eAaImRp2nS
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Týdny	týden	k1gInPc1
na	na	k7c6
čele	čelo	k1gNnSc6
žebříčku	žebříček	k1gInSc2
dvouhry	dvouhra	k1gFnSc2
podle	podle	k7c2
státu	stát	k1gInSc2
</s>
<s>
Poř	Poř	k?
<g/>
.	.	kIx.
</s>
<s>
stát	stát	k1gInSc1
</s>
<s>
hráčů	hráč	k1gMnPc2
</s>
<s>
týdnů	týden	k1gInPc2
</s>
<s>
tenisté	tenista	k1gMnPc1
</s>
<s>
zdroj	zdroj	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
<s>
6896	#num#	k4
<g/>
Jimmy	Jimmo	k1gNnPc7
Connors	Connorsa	k1gFnPc2
<g/>
,	,	kIx,
John	John	k1gMnSc1
McEnroe	McEnro	k1gFnSc2
<g/>
,	,	kIx,
Jim	on	k3xPp3gMnPc3
Courier	Courier	k1gMnSc1
<g/>
,	,	kIx,
Pete	Pete	k1gFnSc1
Sampras	Sampras	k1gMnSc1
<g/>
,	,	kIx,
Andre	Andr	k1gMnSc5
Agassi	Agass	k1gMnSc5
<g/>
,	,	kIx,
Andy	Anda	k1gFnPc4
Roddick	Roddicka	k1gFnPc2
</s>
<s>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
1320	#num#	k4
<g/>
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
Švýcarsko	Švýcarsko	k1gNnSc1
</s>
<s>
1310	#num#	k4
<g/>
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Československo	Československo	k1gNnSc1
Československo	Československo	k1gNnSc1
</s>
<s>
1270	#num#	k4
<g/>
Ivan	Ivan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
</s>
<s>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
3219	#num#	k4
<g/>
Carlos	Carlos	k1gMnSc1
Moyà	Moyà	k1gMnSc1
<g/>
,	,	kIx,
Juan	Juan	k1gMnSc1
Carlos	Carlos	k1gMnSc1
Ferrero	Ferrero	k1gNnSc4
<g/>
,	,	kIx,
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
</s>
<s>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
Švédsko	Švédsko	k1gNnSc1
</s>
<s>
3201	#num#	k4
<g/>
Björn	Björn	k1gMnSc1
Borg	Borg	k1gMnSc1
<g/>
,	,	kIx,
Mats	Mats	k1gInSc1
Wilander	Wilander	k1gInSc1
<g/>
,	,	kIx,
Stefan	Stefan	k1gMnSc1
Edberg	Edberg	k1gMnSc1
</s>
<s>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
Austrálie	Austrálie	k1gFnSc2
</s>
<s>
389	#num#	k4
<g/>
John	John	k1gMnSc1
Newcombe	Newcomb	k1gInSc5
<g/>
,	,	kIx,
Patrick	Patrick	k1gMnSc1
Rafter	Rafter	k1gMnSc1
<g/>
,	,	kIx,
Lleyton	Lleyton	k1gInSc1
Hewitt	Hewitt	k2eAgInSc1d1
</s>
<s>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Brazílie	Brazílie	k1gFnSc1
Brazílie	Brazílie	k1gFnSc2
</s>
<s>
143	#num#	k4
<g/>
Gustavo	Gustava	k1gFnSc5
Kuerten	Kuertno	k1gNnPc2
</s>
<s>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
141	#num#	k4
<g/>
Andy	Anda	k1gFnPc4
Murray	Murraa	k1gFnSc2
</s>
<s>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rumunsko	Rumunsko	k1gNnSc1
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
140	#num#	k4
<g/>
Ilie	Ili	k1gInSc2
Năstase	Năstas	k1gInSc6
</s>
<s>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rusko	Rusko	k1gNnSc1
Rusko	Rusko	k1gNnSc1
</s>
<s>
215	#num#	k4
<g/>
Jevgenij	Jevgenij	k1gMnSc1
Kafelnikov	Kafelnikov	k1gInSc1
<g/>
,	,	kIx,
Marat	Marat	k2eAgInSc1d1
Safin	Safin	k1gInSc1
</s>
<s>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc1
</s>
<s>
112	#num#	k4
<g/>
Boris	Boris	k1gMnSc1
Becker	Becker	k1gMnSc1
</s>
<s>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
16	#num#	k4
<g/>
Thomas	Thomas	k1gMnSc1
Muster	Muster	k1gMnSc1
</s>
<s>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Chile	Chile	k1gNnSc1
Chile	Chile	k1gNnSc2
</s>
<s>
16	#num#	k4
<g/>
Marcelo	Marcela	k1gFnSc5
Ríos	Ríosa	k1gFnPc2
</s>
<s>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
stav	stav	k1gInSc1
k	k	k7c3
10	#num#	k4
<g/>
.	.	kIx.
květnu	květen	k1gInSc3
2021	#num#	k4
<g/>
;	;	kIx,
tučně	tučně	k6eAd1
–	–	k?
aktivní	aktivní	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
</s>
<s>
Konečný	konečný	k2eAgInSc4d1
žebříček	žebříček	k1gInSc4
dvouhry	dvouhra	k1gFnSc2
</s>
<s>
Rekordní	rekordní	k2eAgInSc1d1
počet	počet	k1gInSc1
šesti	šest	k4xCc2
zakončených	zakončený	k2eAgFnPc2d1
sezón	sezóna	k1gFnPc2
na	na	k7c6
čele	čelo	k1gNnSc6
světové	světový	k2eAgFnSc2d1
klasifikace	klasifikace	k1gFnSc2
drží	držet	k5eAaImIp3nS
Američan	Američan	k1gMnSc1
Pete	Pet	k1gFnSc2
Sampras	Sampras	k1gMnSc1
se	s	k7c7
Srbem	Srb	k1gMnSc7
Novakem	Novak	k1gMnSc7
Djokovićem	Djoković	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sampras	Sampras	k1gMnSc1
tento	tento	k3xDgInSc4
rekord	rekord	k1gInSc4
vytvořil	vytvořit	k5eAaPmAgMnS
bez	bez	k7c2
přerušení	přerušení	k1gNnSc2
v	v	k7c6
letech	let	k1gInPc6
1993	#num#	k4
<g/>
–	–	k?
<g/>
1998	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pouze	pouze	k6eAd1
pět	pět	k4xCc1
hráčů	hráč	k1gMnPc2
figurovalo	figurovat	k5eAaImAgNnS
na	na	k7c6
čele	čelo	k1gNnSc6
žebříčku	žebříček	k1gInSc2
po	po	k7c4
všechny	všechen	k3xTgInPc4
týdny	týden	k1gInPc4
jediného	jediný	k2eAgInSc2d1
kalendářního	kalendářní	k2eAgInSc2d1
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švýcar	Švýcar	k1gMnSc1
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
pak	pak	k6eAd1
jako	jako	k9
jediný	jediný	k2eAgMnSc1d1
dokázal	dokázat	k5eAaPmAgMnS
tento	tento	k3xDgInSc4
výkon	výkon	k1gInSc4
zopakovat	zopakovat	k5eAaPmF
třikrát	třikrát	k6eAd1
v	v	k7c6
řadě	řada	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
v	v	k7c6
letech	let	k1gInPc6
2005	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srb	Srb	k1gMnSc1
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
historicky	historicky	k6eAd1
první	první	k4xOgFnSc7
konečnou	konečný	k2eAgFnSc7d1
jedničkou	jednička	k1gFnSc7
i	i	k9
přesto	přesto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgInS
v	v	k7c6
průběhu	průběh	k1gInSc6
sezóny	sezóna	k1gFnSc2
postaven	postavit	k5eAaPmNgInS
mimo	mimo	k7c4
elitní	elitní	k2eAgFnSc4d1
světovou	světový	k2eAgFnSc4d1
dvacítku	dvacítka	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3
konečnou	konečný	k2eAgFnSc7d1
jedničkou	jednička	k1gFnSc7
se	se	k3xPyFc4
roku	rok	k1gInSc2
2020	#num#	k4
stal	stát	k5eAaPmAgMnS
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
ve	v	k7c6
33	#num#	k4
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Rekordní	rekordní	k2eAgFnSc1d1
11	#num#	k4
<g/>
leté	letý	k2eAgNnSc4d1
období	období	k1gNnSc4
mezi	mezi	k7c7
prvním	první	k4xOgNnSc7
a	a	k8xC
posledním	poslední	k2eAgNnSc7d1
zakončením	zakončení	k1gNnSc7
vytvořil	vytvořit	k5eAaPmAgMnS
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
období	období	k1gNnSc6
2000	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
se	se	k3xPyFc4
devětkrát	devětkrát	k6eAd1
o	o	k7c6
konečné	konečný	k2eAgFnSc6d1
jedničce	jednička	k1gFnSc6
rozhodovalo	rozhodovat	k5eAaImAgNnS
až	až	k9
na	na	k7c6
závěrečném	závěrečný	k2eAgInSc6d1
Turnaji	turnaj	k1gInSc6
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
sezónách	sezóna	k1gFnPc6
2000	#num#	k4
(	(	kIx(
<g/>
Kuerten	Kuertno	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
(	(	kIx(
<g/>
Hewitt	Hewitt	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2003	#num#	k4
(	(	kIx(
<g/>
Roddick	Roddick	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2009	#num#	k4
(	(	kIx(
<g/>
Federer	Federer	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2013	#num#	k4
(	(	kIx(
<g/>
Nadal	nadat	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2014	#num#	k4
(	(	kIx(
<g/>
Djoković	Djoković	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2016	#num#	k4
(	(	kIx(
<g/>
Murray	Murraa	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
2019	#num#	k4
(	(	kIx(
<g/>
Nadal	nadat	k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Počet	počet	k1gInSc1
zakončení	zakončení	k1gNnSc2
na	na	k7c6
čele	čelo	k1gNnSc6
žebříčku	žebříček	k1gInSc2
dvouhry	dvouhra	k1gFnSc2
</s>
<s>
Ivan	Ivan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
zakončil	zakončit	k5eAaPmAgMnS
sezónu	sezóna	k1gFnSc4
čtyřikrát	čtyřikrát	k6eAd1
jako	jako	k8xS,k8xC
světová	světový	k2eAgFnSc1d1
jednička	jednička	k1gFnSc1
</s>
<s>
početstáttenista	početstáttenista	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéSampras	americkéSamprasa	k1gFnPc2
<g/>
,	,	kIx,
PetePete	PetePe	k1gNnSc2
Sampras	Samprasa	k1gFnPc2
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
SrbskoDjoković	SrbskoDjoković	k1gMnSc1
<g/>
,	,	kIx,
NovakNovak	NovakNovak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
5	#num#	k4
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéConnors	americkéConnorsa	k1gFnPc2
<g/>
,	,	kIx,
JimmyJimmy	JimmyJimma	k1gFnSc2
Connors	Connorsa	k1gFnPc2
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
ŠvýcarskoFederer	ŠvýcarskoFederer	k1gMnSc1
<g/>
,	,	kIx,
RogerRoger	RogerRoger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
ŠpanělskoNadal	ŠpanělskoNadal	k1gMnSc1
<g/>
,	,	kIx,
RafaelRafael	RafaelRafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
</s>
<s>
4	#num#	k4
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéMcEnroe	americkéMcEnroe	k1gNnSc2
<g/>
,	,	kIx,
JohnJohn	JohnJohn	k1gMnSc1
McEnroe	McEnro	k1gFnSc2
</s>
<s>
Československo	Československo	k1gNnSc1
ČeskoslovenskoLendl	ČeskoslovenskoLendl	k1gMnSc1
<g/>
,	,	kIx,
IvanIvan	IvanIvan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
</s>
<s>
2	#num#	k4
<g/>
Švédsko	Švédsko	k1gNnSc1
ŠvédskoBorg	ŠvédskoBorg	k1gMnSc1
<g/>
,	,	kIx,
BjörnBjörn	BjörnBjörn	k1gMnSc1
Borg	Borg	k1gMnSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
ŠvédskoEdberg	ŠvédskoEdberg	k1gMnSc1
<g/>
,	,	kIx,
StefanStefan	StefanStefan	k1gMnSc1
Edberg	Edberg	k1gMnSc1
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
AustrálieHewitt	AustrálieHewitt	k1gInSc1
<g/>
,	,	kIx,
LleytonLleyton	LleytonLleyton	k1gInSc1
Hewitt	Hewitt	k2eAgInSc1d1
</s>
<s>
1	#num#	k4
<g/>
Rumunsko	Rumunsko	k1gNnSc1
RumunskoNăstase	RumunskoNăstasa	k1gFnSc3
<g/>
,	,	kIx,
IlieIlie	IlieIlie	k1gFnPc1
Năstase	Năstasa	k1gFnSc3
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
ŠvédskoWilander	ŠvédskoWilandra	k1gFnPc2
<g/>
,	,	kIx,
MatsMats	MatsMatsa	k1gFnPc2
Wilander	Wilandra	k1gFnPc2
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéCourier	americkéCourira	k1gFnPc2
<g/>
,	,	kIx,
JimJim	JimJim	k1gMnSc1
Courier	Courier	k1gMnSc1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéAgassi	americkéAgasse	k1gFnSc3
<g/>
,	,	kIx,
AndreAndre	AndreAndr	k1gInSc5
Agassi	Agass	k1gMnSc6
</s>
<s>
Brazílie	Brazílie	k1gFnSc1
BrazílieKuerten	BrazílieKuertno	k1gNnPc2
<g/>
,	,	kIx,
GustavoGustavo	GustavoGustava	k1gFnSc5
Kuerten	Kuerten	k2eAgInSc4d1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéRoddick	americkéRoddicka	k1gFnPc2
<g/>
,	,	kIx,
AndyAndy	AndyAnda	k1gFnSc2
Roddick	Roddicka	k1gFnPc2
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgFnSc2d1
královstvíMurray	královstvíMurraa	k1gFnSc2
<g/>
,	,	kIx,
AndyAndy	AndyAnda	k1gFnSc2
Murray	Murraa	k1gFnSc2
</s>
<s>
Jedničky	jednička	k1gFnPc1
na	na	k7c6
konečném	konečný	k2eAgInSc6d1
žebříčku	žebříček	k1gInSc6
dvouhry	dvouhra	k1gFnSc2
</s>
<s>
Pete	Pete	k6eAd1
Sampras	Sampras	k1gMnSc1
zakončil	zakončit	k5eAaPmAgMnS
šestkrát	šestkrát	k6eAd1
sezónu	sezóna	k1gFnSc4
jako	jako	k9
světová	světový	k2eAgFnSc1d1
jednička	jednička	k1gFnSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
představuje	představovat	k5eAaImIp3nS
rekord	rekord	k1gInSc4
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
tenista	tenista	k1gMnSc1
</s>
<s>
body	bod	k1gInPc1
ATP	atp	kA
</s>
<s>
zdroj	zdroj	k1gInSc1
</s>
<s>
1973	#num#	k4
</s>
<s>
Năstase	Năstase	k6eAd1
<g/>
,	,	kIx,
IlieIlie	IlieIlie	k1gFnSc1
Năstase	Năstas	k1gInSc6
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
—	—	k?
</s>
<s>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1974	#num#	k4
</s>
<s>
Connors	Connors	k1gInSc1
<g/>
,	,	kIx,
JimmyJimmy	JimmyJimm	k1gInPc1
Connors	Connorsa	k1gFnPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1975	#num#	k4
</s>
<s>
Connors	Connors	k1gInSc1
<g/>
,	,	kIx,
JimmyJimmy	JimmyJimm	k1gInPc1
Connors	Connorsa	k1gFnPc2
<g/>
§	§	k?
</s>
<s>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1976	#num#	k4
</s>
<s>
Connors	Connors	k1gInSc1
<g/>
,	,	kIx,
JimmyJimmy	JimmyJimm	k1gInPc1
Connors	Connorsa	k1gFnPc2
<g/>
§	§	k?
</s>
<s>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1977	#num#	k4
</s>
<s>
Connors	Connors	k6eAd1
<g/>
,	,	kIx,
JimmyJimmy	JimmyJimmo	k1gNnPc7
Connors	Connors	k1gInSc1
</s>
<s>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1978	#num#	k4
</s>
<s>
Connors	Connors	k1gInSc1
<g/>
,	,	kIx,
JimmyJimmy	JimmyJimm	k1gInPc1
Connors	Connorsa	k1gFnPc2
<g/>
§	§	k?
</s>
<s>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1979	#num#	k4
</s>
<s>
Borg	Borg	k1gMnSc1
<g/>
,	,	kIx,
BjörnBjörn	BjörnBjörn	k1gMnSc1
Borg	Borg	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1980	#num#	k4
</s>
<s>
Borg	Borg	k1gMnSc1
<g/>
,	,	kIx,
BjörnBjörn	BjörnBjörn	k1gMnSc1
Borg	Borg	k1gMnSc1
</s>
<s>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1981	#num#	k4
</s>
<s>
McEnroe	McEnroe	k1gNnSc1
<g/>
,	,	kIx,
JohnJohn	JohnJohn	k1gNnSc1
McEnroe	McEnroe	k1gFnPc2
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1982	#num#	k4
</s>
<s>
McEnroe	McEnroe	k1gNnSc1
<g/>
,	,	kIx,
JohnJohn	JohnJohn	k1gNnSc1
McEnroe	McEnro	k1gFnSc2
</s>
<s>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1983	#num#	k4
</s>
<s>
McEnroe	McEnroe	k1gNnSc1
<g/>
,	,	kIx,
JohnJohn	JohnJohn	k1gNnSc1
McEnroe	McEnro	k1gInSc2
</s>
<s>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1984	#num#	k4
</s>
<s>
McEnroe	McEnroe	k1gNnSc1
<g/>
,	,	kIx,
JohnJohn	JohnJohn	k1gNnSc1
McEnroe	McEnro	k1gFnSc2
</s>
<s>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1985	#num#	k4
</s>
<s>
Lendl	Lendnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
IvanIvan	IvanIvan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1986	#num#	k4
</s>
<s>
Lendl	Lendnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
IvanIvan	IvanIvan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
<g/>
§	§	k?
</s>
<s>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1987	#num#	k4
</s>
<s>
Lendl	Lendnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
IvanIvan	IvanIvan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
<g/>
§	§	k?
</s>
<s>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1988	#num#	k4
</s>
<s>
Wilander	Wilander	k1gInSc1
<g/>
,	,	kIx,
MatsMats	MatsMats	k1gInSc1
Wilander	Wilander	k1gInSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1989	#num#	k4
</s>
<s>
Lendl	Lendnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
IvanIvan	IvanIvan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
</s>
<s>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1990	#num#	k4
</s>
<s>
Edberg	Edberg	k1gMnSc1
<g/>
,	,	kIx,
StefanStefan	StefanStefan	k1gMnSc1
Edberg	Edberg	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1991	#num#	k4
</s>
<s>
Edberg	Edberg	k1gMnSc1
<g/>
,	,	kIx,
StefanStefan	StefanStefan	k1gMnSc1
Edberg	Edberg	k1gMnSc1
</s>
<s>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1992	#num#	k4
</s>
<s>
Courier	Courier	k1gMnSc1
<g/>
,	,	kIx,
JimJim	JimJim	k1gMnSc1
Courier	Courier	k1gMnSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1993	#num#	k4
</s>
<s>
Sampras	Sampras	k1gMnSc1
<g/>
,	,	kIx,
PetePete	PetePe	k1gNnSc2
Sampras	Samprasa	k1gFnPc2
(	(	kIx(
<g/>
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1994	#num#	k4
</s>
<s>
Sampras	Sampras	k1gMnSc1
<g/>
,	,	kIx,
PetePete	PetePe	k1gNnSc2
Sampras	Samprasa	k1gFnPc2
<g/>
§	§	k?
</s>
<s>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1995	#num#	k4
</s>
<s>
Sampras	Sampras	k1gMnSc1
<g/>
,	,	kIx,
PetePete	PetePe	k1gNnSc2
Sampras	Samprasa	k1gFnPc2
</s>
<s>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1996	#num#	k4
</s>
<s>
Sampras	Sampras	k1gMnSc1
<g/>
,	,	kIx,
PetePete	PetePe	k1gNnSc2
Sampras	Sampras	k1gInSc4
<g/>
3	#num#	k4
760	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1997	#num#	k4
</s>
<s>
Sampras	Sampras	k1gMnSc1
<g/>
,	,	kIx,
PetePete	PetePe	k1gNnSc2
Sampras	Samprasa	k1gFnPc2
<g/>
§	§	k?
<g/>
3	#num#	k4
666	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1998	#num#	k4
</s>
<s>
Sampras	Sampras	k1gMnSc1
<g/>
,	,	kIx,
PetePete	PetePe	k1gNnSc2
Sampras	Sampras	k1gInSc4
<g/>
3	#num#	k4
131	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1999	#num#	k4
</s>
<s>
Agassi	Agasse	k1gFnSc4
<g/>
,	,	kIx,
AndreAndre	AndreAndr	k1gInSc5
Agassi	Agass	k1gMnSc6
(	(	kIx(
<g/>
10	#num#	k4
<g/>
)	)	kIx)
<g/>
4	#num#	k4
059	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2000	#num#	k4
</s>
<s>
Kuerten	Kuerten	k2eAgMnSc1d1
<g/>
,	,	kIx,
GustavoGustavo	GustavoGustava	k1gFnSc5
Kuerten	Kuertno	k1gNnPc2
(	(	kIx(
<g/>
11	#num#	k4
<g/>
)	)	kIx)
<g/>
4	#num#	k4
195	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2001	#num#	k4
</s>
<s>
Hewitt	Hewitt	k1gInSc1
<g/>
,	,	kIx,
LleytonLleyton	LleytonLleyton	k1gInSc1
Hewitt	Hewitt	k1gInSc1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
)	)	kIx)
<g/>
4	#num#	k4
365	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2002	#num#	k4
</s>
<s>
Hewitt	Hewitt	k1gInSc1
<g/>
,	,	kIx,
LleytonLleyton	LleytonLleyton	k1gInSc1
Hewitt	Hewitt	k1gInSc1
<g/>
§	§	k?
<g/>
4	#num#	k4
485	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2003	#num#	k4
</s>
<s>
Roddick	Roddick	k1gInSc1
<g/>
,	,	kIx,
AndyAndy	AndyAnd	k1gInPc1
Roddick	Roddicka	k1gFnPc2
(	(	kIx(
<g/>
13	#num#	k4
<g/>
)	)	kIx)
<g/>
4	#num#	k4
535	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2004	#num#	k4
</s>
<s>
Federer	Federer	k1gMnSc1
<g/>
,	,	kIx,
RogerRoger	RogerRoger	k1gMnSc1
Federer	Federer	k1gMnSc1
(	(	kIx(
<g/>
14	#num#	k4
<g/>
)	)	kIx)
<g/>
6	#num#	k4
335	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2005	#num#	k4
</s>
<s>
Federer	Federer	k1gMnSc1
<g/>
,	,	kIx,
RogerRoger	RogerRoger	k1gMnSc1
Federer	Federer	k1gMnSc1
<g/>
§	§	k?
<g/>
6	#num#	k4
725	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2006	#num#	k4
</s>
<s>
Federer	Federer	k1gMnSc1
<g/>
,	,	kIx,
RogerRoger	RogerRoger	k1gMnSc1
Federer	Federer	k1gMnSc1
<g/>
§	§	k?
<g/>
8	#num#	k4
370	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2007	#num#	k4
</s>
<s>
Federer	Federer	k1gMnSc1
<g/>
,	,	kIx,
RogerRoger	RogerRoger	k1gMnSc1
Federer	Federer	k1gMnSc1
<g/>
§	§	k?
<g/>
7	#num#	k4
180	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2008	#num#	k4
</s>
<s>
Nadal	nadat	k5eAaPmAgMnS
<g/>
,	,	kIx,
RafaelRafael	RafaelRafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
(	(	kIx(
<g/>
15	#num#	k4
<g/>
)	)	kIx)
<g/>
6	#num#	k4
675	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2009	#num#	k4
</s>
<s>
Federer	Federer	k1gMnSc1
<g/>
,	,	kIx,
RogerRoger	RogerRoger	k1gMnSc1
Federer	Federer	k1gMnSc1
<g/>
10	#num#	k4
550	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2010	#num#	k4
</s>
<s>
Nadal	nadat	k5eAaPmAgMnS
<g/>
,	,	kIx,
RafaelRafael	RafaelRafael	k1gInSc1
Nadal	nadat	k5eAaPmAgInS
<g/>
12	#num#	k4
450	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2011	#num#	k4
</s>
<s>
Djoković	Djoković	k?
<g/>
,	,	kIx,
NovakNovak	NovakNovak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
(	(	kIx(
<g/>
16	#num#	k4
<g/>
)	)	kIx)
<g/>
13	#num#	k4
630	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2012	#num#	k4
</s>
<s>
Djoković	Djoković	k?
<g/>
,	,	kIx,
NovakNovak	NovakNovak	k1gMnSc1
Djoković	Djoković	k1gFnSc2
<g/>
12	#num#	k4
920	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2013	#num#	k4
</s>
<s>
Nadal	nadat	k5eAaPmAgMnS
<g/>
,	,	kIx,
RafaelRafael	RafaelRafael	k1gInSc1
Nadal	nadat	k5eAaPmAgInS
<g/>
13	#num#	k4
030	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2014	#num#	k4
</s>
<s>
Djoković	Djoković	k?
<g/>
,	,	kIx,
NovakNovak	NovakNovak	k1gMnSc1
Djoković	Djoković	k1gFnSc2
<g/>
11	#num#	k4
360	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2015	#num#	k4
</s>
<s>
Djoković	Djoković	k?
<g/>
,	,	kIx,
NovakNovak	NovakNovak	k1gMnSc1
Djoković	Djoković	k1gFnSc2
<g/>
16	#num#	k4
585	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2016	#num#	k4
</s>
<s>
Murray	Murray	k1gInPc1
<g/>
,	,	kIx,
AndyAndy	AndyAnd	k1gInPc1
Murray	Murraa	k1gFnSc2
(	(	kIx(
<g/>
17	#num#	k4
<g/>
)	)	kIx)
<g/>
12	#num#	k4
685	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2017	#num#	k4
</s>
<s>
Nadal	nadat	k5eAaPmAgMnS
<g/>
,	,	kIx,
RafaelRafael	RafaelRafael	k1gInSc1
Nadal	nadat	k5eAaPmAgInS
<g/>
10	#num#	k4
645	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2018	#num#	k4
</s>
<s>
Djoković	Djoković	k?
<g/>
,	,	kIx,
NovakNovak	NovakNovak	k1gMnSc1
Djoković	Djoković	k1gFnSc2
<g/>
9	#num#	k4
045	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2019	#num#	k4
</s>
<s>
Nadal	nadat	k5eAaPmAgMnS
<g/>
,	,	kIx,
RafaelRafael	RafaelRafael	k1gInSc1
Nadal	nadat	k5eAaPmAgInS
<g/>
9	#num#	k4
985	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2020	#num#	k4
</s>
<s>
Djoković	Djoković	k?
<g/>
,	,	kIx,
NovakNovak	NovakNovak	k1gMnSc1
Djoković	Djoković	k1gFnSc2
<g/>
12	#num#	k4
030	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
§	§	k?
–	–	k?
na	na	k7c6
čele	čelo	k1gNnSc6
žebříčku	žebříček	k1gInSc2
všechny	všechen	k3xTgInPc4
týdny	týden	k1gInPc4
kalendářního	kalendářní	k2eAgInSc2d1
roku	rok	k1gInSc2
</s>
<s>
Hráči	hráč	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
jedničkami	jednička	k1gFnPc7
bez	bez	k7c2
výhry	výhra	k1gFnSc2
na	na	k7c4
GS	GS	kA
dvouhry	dvouhra	k1gFnSc2
</s>
<s>
Tabulka	tabulka	k1gFnSc1
uvádí	uvádět	k5eAaImIp3nS
tenisty	tenista	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
nastoupili	nastoupit	k5eAaPmAgMnP
na	na	k7c6
pozici	pozice	k1gFnSc6
světové	světový	k2eAgFnSc2d1
jedničky	jednička	k1gFnSc2
bez	bez	k7c2
výhry	výhra	k1gFnSc2
na	na	k7c6
grandslamovém	grandslamový	k2eAgInSc6d1
turnaji	turnaj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Tenistadatum	Tenistadatum	k1gNnSc1
poprvé	poprvé	k6eAd1
jedničkouprvní	jedničkouprvní	k2eAgNnSc4d1
finále	finále	k1gNnSc4
na	na	k7c4
grandslamuprvní	grandslamuprvní	k2eAgInSc4d1
titul	titul	k1gInSc4
na	na	k7c4
grandslamuzdroj	grandslamuzdroj	k1gInSc4
</s>
<s>
Ivan	Ivan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1983	#num#	k4
<g/>
French	French	k1gMnSc1
Open	Open	k1gMnSc1
1981	#num#	k4
<g/>
French	Frencha	k1gFnPc2
Open	Open	k1gNnSc1
1984	#num#	k4
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Marcelo	Marcela	k1gFnSc5
Ríos	Ríos	k1gInSc1
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1998	#num#	k4
<g/>
Australian	Australian	k1gMnSc1
Open	Open	k1gInSc4
1998	#num#	k4
<g/>
žádný	žádný	k1gMnSc1
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hráči	hráč	k1gMnPc1
nejvýše	vysoce	k6eAd3,k6eAd1
postavení	postavení	k1gNnSc4
na	na	k7c4
2	#num#	k4
<g/>
.	.	kIx.
až	až	k9
5	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
</s>
<s>
Následující	následující	k2eAgInSc1d1
přehled	přehled	k1gInSc1
uvádí	uvádět	k5eAaImIp3nS
tenisty	tenista	k1gMnPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc7
nejvyšším	vysoký	k2eAgNnSc7d3
postavením	postavení	k1gNnSc7
na	na	k7c6
žebříčku	žebříček	k1gInSc6
ATP	atp	kA
ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
bylo	být	k5eAaImAgNnS
2	#num#	k4
<g/>
.	.	kIx.
až	až	k9
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Col-begin	Col-begin	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Hráči	hráč	k1gMnPc1
na	na	k7c4
2	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
žebříčku	žebříček	k1gInSc2
ATP	atp	kA
dvouhry	dvouhra	k1gFnSc2
</s>
<s>
Světová	světový	k2eAgFnSc1d1
dvojka	dvojka	k1gFnSc1
</s>
<s>
prvně	prvně	k?
klasifikován	klasifikovat	k5eAaImNgMnS
</s>
<s>
Manuel	Manuel	k1gMnSc1
Orantes	Orantes	k1gMnSc1
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1973	#num#	k4
</s>
<s>
Guillermo	Guillermo	k6eAd1
Vilas	Vilas	k1gInSc1
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1975	#num#	k4
</s>
<s>
Arthur	Arthur	k1gMnSc1
Ashe	Ash	k1gFnSc2
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1976	#num#	k4
</s>
<s>
Michael	Michael	k1gMnSc1
Stich	Stich	k1gMnSc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1993	#num#	k4
</s>
<s>
/	/	kIx~
Goran	Goran	k1gMnSc1
Ivanišević	Ivanišević	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1994	#num#	k4
</s>
<s>
Michael	Michael	k1gMnSc1
Chang	Chang	k1gMnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1996	#num#	k4
</s>
<s>
Petr	Petr	k1gMnSc1
Korda	Korda	k1gMnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1998	#num#	k4
</s>
<s>
À	À	k1gInSc1
Corretja	Corretj	k1gInSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1999	#num#	k4
</s>
<s>
Magnus	Magnus	k1gMnSc1
Norman	Norman	k1gMnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2000	#num#	k4
</s>
<s>
Tommy	Tomma	k1gFnPc1
Haas	Haasa	k1gFnPc2
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2002	#num#	k4
</s>
<s>
Daniil	Daniil	k1gMnSc1
Medveděv	Medveděv	k1gMnSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
</s>
<s>
Hráči	hráč	k1gMnPc1
na	na	k7c4
3	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
žebříčku	žebříček	k1gInSc2
ATP	atp	kA
dvouhry	dvouhra	k1gFnSc2
</s>
<s>
Světová	světový	k2eAgFnSc1d1
trojka	trojka	k1gFnSc1
</s>
<s>
prvně	prvně	k?
klasifikován	klasifikovat	k5eAaImNgMnS
</s>
<s>
Stan	stan	k1gInSc1
Smith	Smith	k1gMnSc1
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1973	#num#	k4
</s>
<s>
Tom	Tom	k1gMnSc1
Okker	Okker	k1gMnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1974	#num#	k4
</s>
<s>
Rod	rod	k1gInSc1
Laver	lavra	k1gFnPc2
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1974	#num#	k4
</s>
<s>
Brian	Brian	k1gMnSc1
Gottfried	Gottfried	k1gMnSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1977	#num#	k4
</s>
<s>
Vitas	Vitas	k1gInSc1
Gerulaitis	Gerulaitis	k1gFnSc1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1978	#num#	k4
</s>
<s>
Yannick	Yannick	k1gMnSc1
Noah	Noah	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1986	#num#	k4
</s>
<s>
Sergi	Sergi	k6eAd1
Bruguera	Bruguera	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1994	#num#	k4
</s>
<s>
Guillermo	Guillermo	k6eAd1
Coria	Coria	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2004	#num#	k4
</s>
<s>
David	David	k1gMnSc1
Nalbandian	Nalbandian	k1gMnSc1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2006	#num#	k4
</s>
<s>
Ivan	Ivan	k1gMnSc1
Ljubičić	Ljubičić	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2006	#num#	k4
</s>
<s>
Nikolaj	Nikolaj	k1gMnSc1
Davyděnko	Davyděnka	k1gFnSc5
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2006	#num#	k4
</s>
<s>
David	David	k1gMnSc1
Ferrer	Ferrer	k1gMnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2013	#num#	k4
</s>
<s>
Stan	stan	k1gInSc1
Wawrinka	Wawrinka	k1gFnSc1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2014	#num#	k4
</s>
<s>
Milos	Milos	k1gInSc1
Raonic	Raonice	k1gFnPc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2016	#num#	k4
</s>
<s>
Alexander	Alexandra	k1gFnPc2
Zverev	Zverva	k1gFnPc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2017	#num#	k4
</s>
<s>
Grigor	Grigor	k1gInSc1
Dimitrov	Dimitrov	k1gInSc1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2017	#num#	k4
</s>
<s>
Marin	Marina	k1gFnPc2
Čilić	Čilić	k1gFnPc2
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2018	#num#	k4
</s>
<s>
Juan	Juan	k1gMnSc1
Martín	Martín	k1gMnSc1
del	del	k?
Potro	Potro	k1gNnSc4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2018	#num#	k4
</s>
<s>
Dominic	Dominice	k1gFnPc2
Thiem	Thium	k1gNnSc7
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2020	#num#	k4
</s>
<s>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Col-begin	Col-begin	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Hráči	hráč	k1gMnPc1
na	na	k7c4
4	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
žebříčku	žebříček	k1gInSc2
ATP	atp	kA
dvouhry	dvouhra	k1gFnSc2
</s>
<s>
Světová	světový	k2eAgFnSc1d1
čtyřka	čtyřka	k1gFnSc1
</s>
<s>
prvně	prvně	k?
klasifikován	klasifikovat	k5eAaImNgMnS
</s>
<s>
Adriano	Adriana	k1gFnSc5
Panatta	Panatta	k1gMnSc1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1976	#num#	k4
</s>
<s>
Raúl	Raúl	k1gMnSc1
Ramírez	Ramírez	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1976	#num#	k4
</s>
<s>
Roscoe	Roscoe	k1gFnSc1
Tanner	Tannra	k1gFnPc2
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1979	#num#	k4
</s>
<s>
Gene	gen	k1gInSc5
Mayer	Mayer	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1980	#num#	k4
</s>
<s>
José	José	k1gNnSc7
Luis	Luisa	k1gFnPc2
Clerc	Clerc	k1gInSc4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1981	#num#	k4
</s>
<s>
Miloslav	Miloslav	k1gMnSc1
Mečíř	mečíř	k1gMnSc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1988	#num#	k4
</s>
<s>
Pat	pat	k1gInSc1
Cash	cash	k1gFnSc2
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1988	#num#	k4
</s>
<s>
Brad	brada	k1gFnPc2
Gilbert	Gilbert	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1990	#num#	k4
</s>
<s>
Andrés	Andrés	k6eAd1
Gómez	Gómez	k1gInSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1990	#num#	k4
</s>
<s>
Guy	Guy	k?
Forget	Forget	k1gInSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1991	#num#	k4
</s>
<s>
Andrij	Andrít	k5eAaPmRp2nS
Medveděv	Medveděv	k1gFnSc1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1994	#num#	k4
</s>
<s>
/	/	kIx~
Greg	Greg	k1gInSc1
Rusedski	Rusedsk	k1gFnSc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1997	#num#	k4
</s>
<s>
Jonas	Jonas	k1gMnSc1
Björkman	Björkman	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1997	#num#	k4
</s>
<s>
Richard	Richard	k1gMnSc1
Krajicek	Krajicka	k1gFnPc2
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1999	#num#	k4
</s>
<s>
Todd	Todd	k1gMnSc1
Martin	Martin	k1gMnSc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1999	#num#	k4
</s>
<s>
Thomas	Thomas	k1gMnSc1
Enqvist	Enqvist	k1gMnSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1999	#num#	k4
</s>
<s>
Nicolas	Nicolas	k1gMnSc1
Kiefer	Kiefer	k1gMnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2000	#num#	k4
</s>
<s>
Tim	Tim	k?
Henman	Henman	k1gMnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2002	#num#	k4
</s>
<s>
Sébastien	Sébastien	k2eAgInSc1d1
Grosjean	Grosjean	k1gInSc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2002	#num#	k4
</s>
<s>
James	James	k1gMnSc1
Blake	Blak	k1gFnSc2
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2006	#num#	k4
</s>
<s>
Robin	robin	k2eAgInSc1d1
Söderling	Söderling	k1gInSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2010	#num#	k4
</s>
<s>
Kei	Kei	k?
Nišikori	Nišikori	k1gNnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2015	#num#	k4
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Berdych	Berdych	k1gMnSc1
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2015	#num#	k4
</s>
<s>
Hráči	hráč	k1gMnPc1
na	na	k7c4
5	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
žebříčku	žebříček	k1gInSc2
ATP	atp	kA
dvouhry	dvouhra	k1gFnSc2
</s>
<s>
Světová	světový	k2eAgFnSc1d1
pětka	pětka	k1gFnSc1
</s>
<s>
prvně	prvně	k?
klasifikován	klasifikovat	k5eAaImNgMnS
</s>
<s>
Jan	Jan	k1gMnSc1
Kodeš	Kodeš	k1gMnSc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1973	#num#	k4
</s>
<s>
Eddie	Eddie	k1gFnSc1
Dibbs	Dibbsa	k1gFnPc2
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1978	#num#	k4
</s>
<s>
Harold	Harold	k1gMnSc1
Solomon	Solomon	k1gMnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1980	#num#	k4
</s>
<s>
Jimmy	Jimma	k1gFnPc1
Arias	Ariasa	k1gFnPc2
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1984	#num#	k4
</s>
<s>
Anders	Anders	k6eAd1
Järryd	Järryd	k1gInSc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1985	#num#	k4
</s>
<s>
Kevin	Kevin	k1gMnSc1
Curren	Currna	k1gFnPc2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1985	#num#	k4
</s>
<s>
Henri	Henr	k1gMnSc5
Leconte	Lecont	k1gMnSc5
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1986	#num#	k4
</s>
<s>
Cédric	Cédric	k1gMnSc1
Pioline	Piolin	k1gInSc5
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2000	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
Novák	Novák	k1gMnSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2002	#num#	k4
</s>
<s>
Rainer	Rainer	k1gMnSc1
Schüttler	Schüttler	k1gMnSc1
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2004	#num#	k4
</s>
<s>
Gastón	Gastón	k1gMnSc1
Gaudio	Gaudio	k1gMnSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2005	#num#	k4
</s>
<s>
Tommy	Tomma	k1gFnPc1
Robredo	Robredo	k1gNnSc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2006	#num#	k4
</s>
<s>
Fernando	Fernando	k6eAd1
González	González	k1gMnSc1
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2007	#num#	k4
</s>
<s>
Jo-Wilfried	Jo-Wilfried	k1gInSc1
Tsonga	Tsong	k1gMnSc2
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2012	#num#	k4
</s>
<s>
Kevin	Kevin	k1gMnSc1
Anderson	Anderson	k1gMnSc1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2018	#num#	k4
</s>
<s>
Stefanos	Stefanos	k1gMnSc1
Tsitsipas	Tsitsipas	k1gMnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2019	#num#	k4
</s>
<s>
poznámky	poznámka	k1gFnPc1
</s>
<s>
aktivní	aktivní	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
</s>
<s>
Top	topit	k5eAaImRp2nS
10	#num#	k4
na	na	k7c6
konečném	konečný	k2eAgInSc6d1
žebříčku	žebříček	k1gInSc6
</s>
<s>
Rok	rok	k1gInSc1
<g/>
1	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
10	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
</s>
<s>
1973	#num#	k4
</s>
<s>
I.	I.	kA
Năstase	Năstas	k1gInSc5
J.	J.	kA
Newcombe	Newcomb	k1gInSc5
J.	J.	kA
Connors	Connorsa	k1gFnPc2
T.	T.	kA
Okker	Okker	k1gMnSc1
S.	S.	kA
Smith	Smith	k1gMnSc1
K.	K.	kA
Rosewall	Rosewall	k1gMnSc1
M.	M.	kA
Orantes	Orantes	k1gMnSc1
R.	R.	kA
Laver	lavra	k1gFnPc2
J.	J.	kA
Kodeš	Kodeš	k1gMnSc1
A.	A.	kA
Ashe	Ashe	k1gFnSc1
</s>
<s>
1974	#num#	k4
</s>
<s>
J.	J.	kA
Connors	Connors	k1gInSc1
J.	J.	kA
Newcombe	Newcomb	k1gInSc5
B.	B.	kA
Borg	Borga	k1gFnPc2
R.	R.	kA
Laver	lavra	k1gFnPc2
G.	G.	kA
Vilas	Vilas	k1gMnSc1
T.	T.	kA
Okker	Okker	k1gMnSc1
A.	A.	kA
Ashe	Ashe	k1gNnSc2
K.	K.	kA
Rosewall	Rosewall	k1gMnSc1
S.	S.	kA
Smith	Smith	k1gMnSc1
I.	I.	kA
Năstase	Năstas	k1gMnSc5
</s>
<s>
1975	#num#	k4
</s>
<s>
J.	J.	kA
Connors	Connorsa	k1gFnPc2
G.	G.	kA
Vilas	Vilas	k1gMnSc1
B.	B.	kA
Borg	Borg	k1gMnSc1
A.	A.	kA
Ashe	Ashe	k1gNnSc2
M.	M.	kA
Orantes	Orantes	k1gMnSc1
K.	K.	kA
Rosewall	Rosewall	k1gMnSc1
I.	I.	kA
Năstase	Năstas	k1gInSc6
J.	J.	kA
Alexander	Alexandra	k1gFnPc2
R.	R.	kA
Tanner	Tanner	k1gMnSc1
R.	R.	kA
Laver	lavra	k1gFnPc2
</s>
<s>
1976	#num#	k4
</s>
<s>
J.	J.	kA
Connors	Connors	k1gInSc1
B.	B.	kA
Borg	Borg	k1gInSc1
I.	I.	kA
Năstase	Năstas	k1gInSc6
M.	M.	kA
Orantes	Orantesa	k1gFnPc2
R.	R.	kA
Ramírez	Ramírez	k1gMnSc1
G.	G.	kA
Vilas	Vilas	k1gMnSc1
A.	A.	kA
Panatta	Panatta	k1gMnSc1
H.	H.	kA
Solomon	Solomon	k1gMnSc1
E.	E.	kA
Dibbs	Dibbs	k1gInSc1
B.	B.	kA
Gottfried	Gottfried	k1gInSc1
</s>
<s>
1977	#num#	k4
</s>
<s>
J.	J.	kA
Connors	Connorsa	k1gFnPc2
G.	G.	kA
Vilas	Vilas	k1gMnSc1
B.	B.	kA
Borg	Borg	k1gMnSc1
V.	V.	kA
Gerulaitis	Gerulaitis	k1gFnSc1
B.	B.	kA
Gottfried	Gottfried	k1gInSc4
E.	E.	kA
Dibbs	Dibbs	k1gInSc1
M.	M.	kA
Orantes	Orantes	k1gInSc1
R.	R.	kA
Ramírez	Ramírez	k1gInSc1
I.	I.	kA
Năstase	Năstasa	k1gFnSc6
D.	D.	kA
Stockton	Stockton	k1gInSc1
</s>
<s>
1978	#num#	k4
</s>
<s>
J.	J.	kA
Connors	Connors	k1gInSc1
B.	B.	kA
Borg	Borg	k1gInSc1
G.	G.	kA
Vilas	Vilas	k1gInSc1
J.	J.	kA
McEnroe	McEnro	k1gFnSc2
V.	V.	kA
Gerulaitis	Gerulaitis	k1gFnSc1
E.	E.	kA
Dibbs	Dibbsa	k1gFnPc2
B.	B.	kA
Gottfried	Gottfried	k1gMnSc1
R.	R.	kA
Ramírez	Ramírez	k1gMnSc1
H.	H.	kA
Solomon	Solomon	k1gMnSc1
C.	C.	kA
Barazzutti	Barazzutť	k1gFnPc1
</s>
<s>
1979	#num#	k4
</s>
<s>
B.	B.	kA
Borg	Borg	k1gMnSc1
J.	J.	kA
Connors	Connors	k1gInSc1
J.	J.	kA
McEnroe	McEnro	k1gFnSc2
V.	V.	kA
Gerulaitis	Gerulaitis	k1gFnSc1
R.	R.	kA
Tanner	Tannra	k1gFnPc2
G.	G.	kA
Vilas	Vilas	k1gMnSc1
A.	A.	kA
Ashe	Ashe	k1gNnSc2
H.	H.	kA
Solomon	Solomon	k1gMnSc1
J.	J.	kA
Higueras	Higueras	k1gMnSc1
E.	E.	kA
Dibbs	Dibbs	k1gInSc1
</s>
<s>
1980	#num#	k4
</s>
<s>
B.	B.	kA
Borg	Borg	k1gMnSc1
J.	J.	kA
McEnroe	McEnroe	k1gNnSc2
J.	J.	kA
Connors	Connorsa	k1gFnPc2
G.	G.	kA
Mayer	Mayer	k1gMnSc1
G.	G.	kA
Vilas	Vilas	k1gMnSc1
I.	I.	kA
Lendl	Lendl	k1gMnSc1
H.	H.	kA
Solomon	Solomon	k1gMnSc1
JL	JL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Clerc	Clerc	k1gFnSc1
V.	V.	kA
Gerulaitis	Gerulaitis	k1gFnSc1
E.	E.	kA
Teltscher	Teltschra	k1gFnPc2
</s>
<s>
1981	#num#	k4
</s>
<s>
J.	J.	kA
McEnroe	McEnroe	k1gNnPc2
I.	I.	kA
Lendl	Lendl	k1gFnSc2
J.	J.	kA
Connors	Connors	k1gInSc1
B.	B.	kA
Borg	Borg	k1gInSc4
JL	JL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Clerc	Clerc	k1gInSc1
G.	G.	kA
Vilas	Vilasa	k1gFnPc2
G.	G.	kA
Mayer	Mayer	k1gMnSc1
E.	E.	kA
Teltscher	Teltschra	k1gFnPc2
V.	V.	kA
Gerulaitis	Gerulaitis	k1gFnSc1
P.	P.	kA
McNamara	McNamara	k1gFnSc1
</s>
<s>
1982	#num#	k4
</s>
<s>
J.	J.	kA
McEnroe	McEnroe	k1gNnPc2
J.	J.	kA
Connors	Connorsa	k1gFnPc2
I.	I.	kA
Lendl	Lendl	k1gMnSc1
G.	G.	kA
Vilas	Vilas	k1gMnSc1
V.	V.	kA
Gerulaitis	Gerulaitis	k1gFnSc1
JL	JL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Clerc	Clerc	k1gInSc1
M.	M.	kA
Wilander	Wilandra	k1gFnPc2
G.	G.	kA
Mayer	Mayer	k1gMnSc1
Y.	Y.	kA
Noah	Noah	k1gMnSc1
P.	P.	kA
McNamara	McNamara	k1gFnSc1
</s>
<s>
1983	#num#	k4
</s>
<s>
J.	J.	kA
McEnroe	McEnroe	k1gNnPc2
I.	I.	kA
Lendl	Lendl	k1gFnSc2
J.	J.	kA
Connors	Connorsa	k1gFnPc2
M.	M.	kA
Wilander	Wilander	k1gMnSc1
Y.	Y.	kA
Noah	Noah	k1gMnSc1
J.	J.	kA
Arias	Arias	k1gMnSc1
J.	J.	kA
Higueras	Higueras	k1gMnSc1
JL	JL	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Clerc	Clerc	k1gInSc1
K.	K.	kA
Curren	Currna	k1gFnPc2
G.	G.	kA
Mayer	Mayer	k1gMnSc1
</s>
<s>
1984	#num#	k4
</s>
<s>
J.	J.	kA
McEnroe	McEnroe	k1gNnPc2
J.	J.	kA
Connors	Connorsa	k1gFnPc2
I.	I.	kA
Lendl	Lendl	k1gMnSc1
M.	M.	kA
Wilander	Wilander	k1gMnSc1
A.	A.	kA
Gómez	Gómez	k1gMnSc1
A.	A.	kA
Järryd	Järryd	k1gMnSc1
H.	H.	kA
Sundström	Sundström	k1gInSc1
P.	P.	kA
Cash	cash	k1gFnSc2
E.	E.	kA
Teltscher	Teltschra	k1gFnPc2
Y.	Y.	kA
Noah	Noah	k1gMnSc1
</s>
<s>
1985	#num#	k4
</s>
<s>
I.	I.	kA
Lendl	Lendl	k1gMnSc1
J.	J.	kA
McEnroe	McEnroe	k1gNnSc2
M.	M.	kA
Wilander	Wilander	k1gMnSc1
J.	J.	kA
Connors	Connorsa	k1gFnPc2
S.	S.	kA
Edberg	Edberg	k1gMnSc1
B.	B.	kA
Becker	Becker	k1gMnSc1
Y.	Y.	kA
Noah	Noah	k1gMnSc1
A.	A.	kA
Järryd	Järryd	k1gMnSc1
M.	M.	kA
Mečíř	mečíř	k1gMnSc1
K.	K.	kA
Curren	Currna	k1gFnPc2
</s>
<s>
1986	#num#	k4
</s>
<s>
I.	I.	kA
Lendl	Lendl	k1gMnSc1
B.	B.	kA
Becker	Becker	k1gMnSc1
M.	M.	kA
Wilander	Wilander	k1gMnSc1
Y.	Y.	kA
Noah	Noah	k1gMnSc1
S.	S.	kA
Edberg	Edberg	k1gMnSc1
H.	H.	kA
Leconte	Lecont	k1gInSc5
J.	J.	kA
Nyström	Nyströmo	k1gNnPc2
J.	J.	kA
Connors	Connorsa	k1gFnPc2
M.	M.	kA
Mečíř	mečíř	k1gMnSc1
A.	A.	kA
Gómez	Gómez	k1gMnSc1
</s>
<s>
1987	#num#	k4
</s>
<s>
I.	I.	kA
Lendl	Lendl	k1gMnSc1
S.	S.	kA
Edberg	Edberg	k1gMnSc1
M.	M.	kA
Wilander	Wilander	k1gMnSc1
J.	J.	kA
Connors	Connorsa	k1gFnPc2
B.	B.	kA
Becker	Becker	k1gMnSc1
M.	M.	kA
Mečíř	mečíř	k1gMnSc1
P.	P.	kA
Cash	cash	k1gFnPc2
Y.	Y.	kA
Noah	Noah	k1gMnSc1
T.	T.	kA
Mayotte	Mayott	k1gInSc5
J.	J.	kA
McEnroe	McEnroe	k1gFnPc3
</s>
<s>
1988	#num#	k4
</s>
<s>
M.	M.	kA
Wilander	Wilandero	k1gNnPc2
I.	I.	kA
Lendl	Lendl	k1gFnSc2
A.	A.	kA
Agassi	Agass	k1gMnSc3
B.	B.	kA
Becker	Becker	k1gInSc1
S.	S.	kA
Edberg	Edberg	k1gInSc1
K.	K.	kA
Carlsson	Carlsson	k1gInSc1
J.	J.	kA
Connors	Connors	k1gInSc1
J.	J.	kA
Hlasek	Hlasek	k1gInSc1
H.	H.	kA
Leconte	Lecont	k1gInSc5
T.	T.	kA
Mayotte	Mayott	k1gInSc5
</s>
<s>
1989	#num#	k4
</s>
<s>
I.	I.	kA
Lendl	Lendl	k1gMnSc1
B.	B.	kA
Becker	Becker	k1gMnSc1
S.	S.	kA
Edberg	Edberg	k1gMnSc1
J.	J.	kA
McEnroe	McEnroe	k1gNnSc2
M.	M.	kA
Chang	Chang	k1gMnSc1
B.	B.	kA
Gilbert	Gilbert	k1gMnSc1
A.	A.	kA
Agassi	Agass	k1gMnSc3
A.	A.	kA
Krickstein	Krickstein	k1gInSc1
A.	A.	kA
Mancini	Mancin	k2eAgMnPc1d1
J.	J.	kA
Berger	Berger	k1gMnSc1
</s>
<s>
1990	#num#	k4
</s>
<s>
S.	S.	kA
Edberg	Edberg	k1gMnSc1
B.	B.	kA
Becker	Becker	k1gMnSc1
I.	I.	kA
Lendl	Lendl	k1gMnSc1
A.	A.	kA
Agassi	Agasse	k1gFnSc3
P.	P.	kA
Sampras	Sampras	k1gMnSc1
A.	A.	kA
Gómez	Gómez	k1gMnSc1
T.	T.	kA
Muster	Muster	k1gMnSc1
E.	E.	kA
Sánchez	Sánchez	k1gMnSc1
G.	G.	kA
Ivanišević	Ivanišević	k1gMnSc1
B.	B.	kA
Gilbert	Gilbert	k1gMnSc1
</s>
<s>
1991	#num#	k4
</s>
<s>
S.	S.	kA
Edberg	Edberg	k1gMnSc1
J.	J.	kA
Courier	Courier	k1gMnSc1
B.	B.	kA
Becker	Becker	k1gMnSc1
M.	M.	kA
Stich	Stich	k1gMnSc1
I.	I.	kA
Lendl	Lendl	k1gMnSc1
P.	P.	kA
Sampras	Sampras	k1gMnSc1
G.	G.	kA
Forget	Forget	k1gMnSc1
K.	K.	kA
Nováček	Nováček	k1gMnSc1
P.	P.	kA
Korda	Korda	k1gMnSc1
A.	A.	kA
Agassi	Agass	k1gMnSc5
</s>
<s>
1992	#num#	k4
</s>
<s>
J.	J.	kA
Courier	Courier	k1gMnSc1
S.	S.	kA
Edberg	Edberg	k1gMnSc1
P.	P.	kA
Sampras	Sampras	k1gMnSc1
G.	G.	kA
Ivanišević	Ivanišević	k1gMnSc1
B.	B.	kA
Becker	Becker	k1gMnSc1
M.	M.	kA
Chang	Chang	k1gMnSc1
P.	P.	kA
Korda	Korda	k1gMnSc1
I.	I.	kA
Lendl	Lendl	k1gMnSc1
A.	A.	kA
Agassi	Agasse	k1gFnSc3
R.	R.	kA
Krajicek	Krajicka	k1gFnPc2
</s>
<s>
1993	#num#	k4
</s>
<s>
P.	P.	kA
Sampras	Sampras	k1gMnSc1
M.	M.	kA
Stich	Stich	k1gMnSc1
J.	J.	kA
Courier	Courier	k1gMnSc1
S.	S.	kA
Bruguera	Bruguero	k1gNnSc2
S.	S.	kA
Edberg	Edberg	k1gMnSc1
A.	A.	kA
Medveděv	Medveděv	k1gMnSc1
G.	G.	kA
Ivanišević	Ivanišević	k1gMnSc1
M.	M.	kA
Chang	Chang	k1gMnSc1
T.	T.	kA
Muster	Muster	k1gMnSc1
C.	C.	kA
Pioline	Piolin	k1gInSc5
</s>
<s>
1994	#num#	k4
</s>
<s>
P.	P.	kA
Sampras	Sampras	k1gMnSc1
A.	A.	kA
Agassi	Agass	k1gMnSc3
B.	B.	kA
Becker	Becker	k1gInSc1
S.	S.	kA
Bruguera	Bruguero	k1gNnSc2
G.	G.	kA
Ivanišević	Ivanišević	k1gMnSc1
M.	M.	kA
Chang	Chang	k1gMnSc1
S.	S.	kA
Edberg	Edberg	k1gMnSc1
A.	A.	kA
Berasategui	Berasategui	k1gNnSc2
M.	M.	kA
Stich	Stich	k1gMnSc1
T.	T.	kA
Martin	Martin	k1gMnSc1
</s>
<s>
1995	#num#	k4
</s>
<s>
P.	P.	kA
Sampras	Sampras	k1gMnSc1
A.	A.	kA
Agassi	Agasse	k1gFnSc3
T.	T.	kA
Muster	Muster	k1gMnSc1
B.	B.	kA
Becker	Becker	k1gMnSc1
M.	M.	kA
Chang	Chang	k1gMnSc1
J.	J.	kA
Kafelnikov	Kafelnikov	k1gInSc1
T.	T.	kA
Enqvist	Enqvist	k1gMnSc1
J.	J.	kA
Courier	Courier	k1gMnSc1
W.	W.	kA
Ferreira	Ferreira	k1gMnSc1
G.	G.	kA
Ivanišević	Ivanišević	k1gMnSc1
</s>
<s>
1996	#num#	k4
</s>
<s>
P.	P.	kA
Sampras	Sampras	k1gMnSc1
M.	M.	kA
Chang	Chang	k1gMnSc1
J.	J.	kA
Kafelnikov	Kafelnikov	k1gInSc1
G.	G.	kA
Ivanišević	Ivanišević	k1gFnSc2
T.	T.	kA
Muster	Muster	k1gMnSc1
B.	B.	kA
Becker	Becker	k1gMnSc1
R.	R.	kA
Krajicek	Krajicko	k1gNnPc2
A.	A.	kA
Agassi	Agass	k1gMnSc3
T.	T.	kA
Enqvist	Enqvist	k1gInSc1
W.	W.	kA
Ferreira	Ferreir	k1gMnSc2
</s>
<s>
1997	#num#	k4
</s>
<s>
P.	P.	kA
Sampras	Sampras	k1gMnSc1
P.	P.	kA
Rafter	Rafter	k1gMnSc1
M.	M.	kA
Chang	Chang	k1gMnSc1
J.	J.	kA
Björkman	Björkman	k1gMnSc1
J.	J.	kA
Kafelnikov	Kafelnikov	k1gInSc1
G.	G.	kA
Rusedski	Rusedsk	k1gFnSc2
C.	C.	kA
Moyà	Moyà	k1gMnSc1
S.	S.	kA
Bruguera	Bruguero	k1gNnSc2
T.	T.	kA
Muster	Muster	k1gMnSc1
M.	M.	kA
Ríos	Ríos	k1gInSc1
</s>
<s>
1998	#num#	k4
</s>
<s>
P.	P.	kA
Sampras	Sampras	k1gMnSc1
M.	M.	kA
Ríos	Ríos	k1gInSc1
À	À	k?
Corretja	Corretj	k1gInSc2
P.	P.	kA
Rafter	Raftrum	k1gNnPc2
C.	C.	kA
Moyà	Moyà	k1gFnSc2
A.	A.	kA
Agassi	Agass	k1gMnSc3
T.	T.	kA
Henman	Henman	k1gMnSc1
K.	K.	kA
Kučera	Kučera	k1gMnSc1
G.	G.	kA
Rusedski	Rusedski	k1gNnSc2
R.	R.	kA
Krajicek	Krajicko	k1gNnPc2
</s>
<s>
1999	#num#	k4
</s>
<s>
A.	A.	kA
Agassi	Agass	k1gMnSc3
J.	J.	kA
Kafelnikov	Kafelnikov	k1gInSc1
P.	P.	kA
Sampras	Sampras	k1gInSc1
T.	T.	kA
Enqvist	Enqvist	k1gInSc4
G.	G.	kA
Kuerten	Kuerten	k2eAgInSc4d1
N.	N.	kA
Kiefer	Kiefer	k1gInSc4
T.	T.	kA
Martin	Martin	k2eAgInSc4d1
N.	N.	kA
Lapentti	Lapentť	k1gFnSc2
M.	M.	kA
Ríos	Ríos	k1gInSc4
R.	R.	kA
Krajicek	Krajicko	k1gNnPc2
</s>
<s>
2000	#num#	k4
</s>
<s>
G.	G.	kA
Kuerten	Kuertno	k1gNnPc2
M.	M.	kA
Safin	Safina	k1gFnPc2
P.	P.	kA
Sampras	Sampras	k1gMnSc1
M.	M.	kA
Norman	Norman	k1gMnSc1
J.	J.	kA
Kafelnikov	Kafelnikov	k1gInSc1
A.	A.	kA
Agassi	Agass	k1gMnSc3
L.	L.	kA
Hewitt	Hewitt	k1gInSc1
A.	A.	kA
Corretja	Corretj	k1gInSc2
T.	T.	kA
Enqvist	Enqvist	k1gMnSc1
T.	T.	kA
Henman	Henman	k1gMnSc1
</s>
<s>
2001	#num#	k4
</s>
<s>
L.	L.	kA
Hewitt	Hewittum	k1gNnPc2
G.	G.	kA
Kuerten	Kuerten	k2eAgInSc4d1
A.	A.	kA
Agassi	Agass	k1gMnSc3
J.	J.	kA
Kafelnikov	Kafelnikov	k1gInSc1
JC	JC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ferrero	Ferrero	k1gNnSc1
S.	S.	kA
Grosjean	Grosjeana	k1gFnPc2
P.	P.	kA
Rafter	Rafter	k1gMnSc1
T.	T.	kA
Haas	Haasa	k1gFnPc2
T.	T.	kA
Henman	Henman	k1gMnSc1
P.	P.	kA
Sampras	Sampras	k1gMnSc1
</s>
<s>
2002	#num#	k4
</s>
<s>
L.	L.	kA
Hewitt	Hewittum	k1gNnPc2
A.	A.	kA
Agassi	Agass	k1gMnSc3
M.	M.	kA
Safin	Safin	k1gInSc1
JC	JC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ferrero	Ferrero	k1gNnSc1
C.	C.	kA
Moyà	Moyà	k1gFnSc2
R.	R.	kA
Federer	Federer	k1gMnSc1
J.	J.	kA
Novák	Novák	k1gMnSc1
T.	T.	kA
Henman	Henman	k1gMnSc1
A.	A.	kA
Costa	Costa	k1gMnSc1
A.	A.	kA
Roddick	Roddick	k1gMnSc1
</s>
<s>
2003	#num#	k4
</s>
<s>
A.	A.	kA
Roddick	Roddick	k1gMnSc1
R.	R.	kA
Federer	Federer	k1gMnSc1
JC	JC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ferrero	Ferrero	k1gNnSc1
A.	A.	kA
Agassi	Agasse	k1gFnSc3
G.	G.	kA
Coria	Corium	k1gNnSc2
R.	R.	kA
Schüttler	Schüttler	k1gMnSc1
C.	C.	kA
Moyà	Moyà	k1gMnSc1
D.	D.	kA
Nalbandian	Nalbandian	k1gInSc1
M.	M.	kA
Philippoussis	Philippoussis	k1gFnSc2
S.	S.	kA
Grosjean	Grosjean	k1gMnSc1
</s>
<s>
2004	#num#	k4
</s>
<s>
R.	R.	kA
Federer	Federer	k1gMnSc1
A.	A.	kA
Roddick	Roddick	k1gMnSc1
L.	L.	kA
Hewitt	Hewitt	k1gMnSc1
M.	M.	kA
Safin	Safin	k1gMnSc1
C.	C.	kA
Moyà	Moyà	k1gMnSc1
T.	T.	kA
Henman	Henman	k1gMnSc1
G.	G.	kA
Coria	Corium	k1gNnSc2
A.	A.	kA
Agassi	Agasse	k1gFnSc4
D.	D.	kA
Nalbandian	Nalbandiana	k1gFnPc2
G.	G.	kA
Gaudio	Gaudio	k1gMnSc1
</s>
<s>
2005	#num#	k4
</s>
<s>
R.	R.	kA
Federer	Federer	k1gMnSc1
R.	R.	kA
Nadal	nadat	k5eAaPmAgMnS
A.	A.	kA
Roddick	Roddick	k1gMnSc1
L.	L.	kA
Hewitt	Hewitt	k1gMnSc1
N.	N.	kA
Davyděnko	Davyděnka	k1gFnSc5
D.	D.	kA
Nalbandian	Nalbandiana	k1gFnPc2
A.	A.	kA
Agassi	Agasse	k1gFnSc3
G.	G.	kA
Coria	Corium	k1gNnSc2
I.	I.	kA
Ljubičić	Ljubičić	k1gMnSc1
G.	G.	kA
Gaudio	Gaudio	k1gMnSc1
</s>
<s>
2006	#num#	k4
</s>
<s>
R.	R.	kA
Federer	Federer	k1gMnSc1
R.	R.	kA
Nadal	nadat	k5eAaPmAgMnS
N.	N.	kA
Davyděnko	Davyděnka	k1gFnSc5
J.	J.	kA
Blake	Blake	k1gNnPc2
I.	I.	kA
Ljubičić	Ljubičić	k1gFnSc2
A.	A.	kA
Roddick	Roddick	k1gInSc4
T.	T.	kA
Robredo	Robredo	k1gNnSc1
D.	D.	kA
Nalbandian	Nalbandiana	k1gFnPc2
M.	M.	kA
Ančić	Ančić	k1gFnSc2
F.	F.	kA
González	González	k1gMnSc1
</s>
<s>
2007	#num#	k4
</s>
<s>
R.	R.	kA
Federer	Federer	k1gInSc1
R.	R.	kA
Nadal	nadat	k5eAaPmAgInS
N.	N.	kA
Djoković	Djoković	k1gFnSc2
N.	N.	kA
Davyděnko	Davyděnka	k1gFnSc5
D.	D.	kA
Ferrer	Ferrra	k1gFnPc2
A.	A.	kA
Roddick	Roddick	k1gMnSc1
F.	F.	kA
González	González	k1gMnSc1
R.	R.	kA
Gasquet	Gasquet	k1gMnSc1
D.	D.	kA
Nalbandian	Nalbandian	k1gMnSc1
T.	T.	kA
Robredo	Robredo	k1gNnSc1
</s>
<s>
2008	#num#	k4
</s>
<s>
R.	R.	kA
Nadal	nadat	k5eAaPmAgInS
R.	R.	kA
Federer	Federero	k1gNnPc2
N.	N.	kA
Djoković	Djoković	k1gFnSc2
A.	A.	kA
Murray	Murra	k2eAgFnPc1d1
N.	N.	kA
Davyděnko	Davyděnka	k1gFnSc5
JW	JW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tsonga	Tsong	k1gMnSc2
G.	G.	kA
Simon	Simon	k1gMnSc1
A.	A.	kA
Roddick	Roddick	k1gMnSc1
JM	JM	kA
<g/>
.	.	kIx.
del	del	k?
Potro	Potro	k1gNnSc1
J.	J.	kA
Blake	Blak	k1gInSc2
</s>
<s>
2009	#num#	k4
</s>
<s>
R.	R.	kA
Federer	Federer	k1gInSc1
R.	R.	kA
Nadal	nadat	k5eAaPmAgInS
N.	N.	kA
Djoković	Djoković	k1gFnSc2
A.	A.	kA
Murray	Murraa	k1gFnSc2
JM	JM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Del	Del	k1gFnSc6
Potro	Potro	k1gNnSc1
N.	N.	kA
Davyděnko	Davyděnka	k1gFnSc5
A.	A.	kA
Roddick	Roddicka	k1gFnPc2
R.	R.	kA
Söderling	Söderling	k1gInSc1
F.	F.	kA
Verdasco	Verdasco	k6eAd1
JW	JW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tsonga	Tsonga	k1gFnSc1
</s>
<s>
2010	#num#	k4
</s>
<s>
R.	R.	kA
Nadal	nadat	k5eAaPmAgInS
R.	R.	kA
Federer	Federero	k1gNnPc2
N.	N.	kA
Djoković	Djoković	k1gFnSc2
A.	A.	kA
Murray	Murraa	k1gFnSc2
R.	R.	kA
Söderling	Söderling	k1gInSc4
T.	T.	kA
Berdych	Berdycha	k1gFnPc2
D.	D.	kA
Ferrer	Ferrer	k1gMnSc1
A.	A.	kA
Roddick	Roddick	k1gMnSc1
F.	F.	kA
Verdasco	Verdasco	k1gMnSc1
M.	M.	kA
Južnyj	Južnyj	k1gMnSc1
</s>
<s>
2011	#num#	k4
</s>
<s>
N.	N.	kA
Djoković	Djoković	k1gMnSc1
R.	R.	kA
Nadal	nadat	k5eAaPmAgMnS
R.	R.	kA
Federer	Federero	k1gNnPc2
A.	A.	kA
Murray	Murraa	k1gFnSc2
D.	D.	kA
Ferrer	Ferrer	k1gMnSc1
JW	JW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tsonga	Tsong	k1gMnSc2
T.	T.	kA
Berdych	Berdych	k1gMnSc1
M.	M.	kA
Fish	Fish	k1gMnSc1
J.	J.	kA
Tipsarević	Tipsarević	k1gMnSc1
N.	N.	kA
Almagro	Almagro	k1gNnSc1
</s>
<s>
2012	#num#	k4
</s>
<s>
N.	N.	kA
Djoković	Djoković	k1gMnSc1
R.	R.	kA
Federer	Federer	k1gInSc1
A.	A.	kA
Murray	Murraa	k1gFnSc2
R.	R.	kA
Nadal	nadat	k5eAaPmAgMnS
D.	D.	kA
Ferrer	Ferrer	k1gMnSc1
T.	T.	kA
Berdych	Berdych	k1gMnSc1
JM	JM	kA
<g/>
.	.	kIx.
del	del	k?
Potro	Potro	k1gNnSc1
JW	JW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tsonga	Tsonga	k1gFnSc1
J.	J.	kA
Tipsarević	Tipsarević	k1gFnSc2
R.	R.	kA
Gasquet	Gasquet	k1gMnSc1
</s>
<s>
2013	#num#	k4
</s>
<s>
R.	R.	kA
Nadal	nadat	k5eAaPmAgInS
N.	N.	kA
Djoković	Djoković	k1gFnSc2
D.	D.	kA
Ferrer	Ferrero	k1gNnPc2
A.	A.	kA
Murray	Murraa	k1gFnSc2
JM	JM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Del	Del	k1gFnSc6
Potro	Potro	k1gNnSc1
R.	R.	kA
Federer	Federra	k1gFnPc2
T.	T.	kA
Berdych	Berdych	k1gMnSc1
S.	S.	kA
Wawrinka	Wawrinka	k1gFnSc1
R.	R.	kA
Gasquet	Gasqueta	k1gFnPc2
JW	JW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tsonga	Tsonga	k1gFnSc1
</s>
<s>
2014	#num#	k4
</s>
<s>
N.	N.	kA
Djoković	Djoković	k1gMnSc1
R.	R.	kA
Federer	Federer	k1gMnSc1
R.	R.	kA
Nadal	nadat	k5eAaPmAgMnS
S.	S.	kA
Wawrinka	Wawrinka	k1gFnSc1
K.	K.	kA
Nišikori	Nišikori	k1gNnSc2
A.	A.	kA
Murray	Murraa	k1gFnSc2
T.	T.	kA
Berdych	Berdych	k1gMnSc1
M.	M.	kA
Raonic	Raonice	k1gFnPc2
M.	M.	kA
Čilić	Čilić	k1gFnSc2
D.	D.	kA
Ferrer	Ferrer	k1gMnSc1
</s>
<s>
2015	#num#	k4
</s>
<s>
N.	N.	kA
Djoković	Djoković	k1gFnPc2
A.	A.	kA
Murray	Murraa	k1gFnSc2
R.	R.	kA
Federer	Federer	k1gMnSc1
S.	S.	kA
Wawrinka	Wawrinka	k1gFnSc1
R.	R.	kA
Nadal	nadat	k5eAaPmAgMnS
T.	T.	kA
Berdych	Berdych	k1gMnSc1
D.	D.	kA
Ferrer	Ferrer	k1gMnSc1
K.	K.	kA
Nišikori	Nišikori	k1gNnSc2
R.	R.	kA
Gasquet	Gasquet	k1gMnSc1
JW	JW	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tsonga	Tsonga	k1gFnSc1
</s>
<s>
2016	#num#	k4
</s>
<s>
A.	A.	kA
Murray	Murra	k2eAgFnPc1d1
N.	N.	kA
Djoković	Djoković	k1gFnPc2
M.	M.	kA
Raonic	Raonice	k1gFnPc2
S.	S.	kA
Wawrinka	Wawrinka	k1gFnSc1
K.	K.	kA
Nišikori	Nišikori	k1gNnSc2
M.	M.	kA
Čilić	Čilić	k1gFnSc2
G.	G.	kA
Monfils	Monfils	k1gInSc1
D.	D.	kA
Thiem	Thiem	k1gInSc4
R.	R.	kA
Nadal	nadat	k5eAaPmAgMnS
T.	T.	kA
Berdych	Berdych	k1gMnSc1
</s>
<s>
2017	#num#	k4
</s>
<s>
R.	R.	kA
Nadal	nadat	k5eAaPmAgMnS
R.	R.	kA
Federer	Federer	k1gMnSc1
G.	G.	kA
Dimitrov	Dimitrov	k1gInSc1
A.	A.	kA
Zverev	Zverva	k1gFnPc2
D.	D.	kA
Thiem	Thiem	k1gInSc1
M.	M.	kA
Čilić	Čilić	k1gFnSc2
D.	D.	kA
Goffin	Goffin	k1gMnSc1
J.	J.	kA
Sock	Sock	k1gMnSc1
S.	S.	kA
Wawrinka	Wawrinka	k1gFnSc1
P.	P.	kA
Carreñ	Carreñ	k6eAd1
Busta	busta	k1gFnSc1
</s>
<s>
2018	#num#	k4
</s>
<s>
N.	N.	kA
Djoković	Djoković	k1gMnSc1
R.	R.	kA
Nadal	nadat	k5eAaPmAgMnS
R.	R.	kA
Federer	Federer	k1gMnSc1
A.	A.	kA
Zverev	Zvervo	k1gNnPc2
JM	JM	kA
<g/>
.	.	kIx.
del	del	k?
Potro	Potro	k1gNnSc1
K.	K.	kA
Anderson	Anderson	k1gMnSc1
M.	M.	kA
Čilić	Čilić	k1gMnSc1
D.	D.	kA
Thiem	Thiem	k1gInSc1
K.	K.	kA
Nišikori	Nišikor	k1gFnSc2
J.	J.	kA
Isner	Isner	k1gMnSc1
</s>
<s>
2019	#num#	k4
</s>
<s>
R.	R.	kA
Nadal	nadat	k5eAaPmAgMnS
N.	N.	kA
Djoković	Djoković	k1gMnSc1
R.	R.	kA
Federer	Federer	k1gMnSc1
D.	D.	kA
Thiem	Thiem	k1gInSc1
D.	D.	kA
Medveděv	Medveděv	k1gFnSc2
S.	S.	kA
Tsitsipas	Tsitsipas	k1gMnSc1
A.	A.	kA
Zverev	Zvervo	k1gNnPc2
M.	M.	kA
Berrettini	Berrettin	k2eAgMnPc1d1
R.	R.	kA
Bautista	Bautista	k1gMnSc1
Agut	Agut	k1gMnSc1
G.	G.	kA
Monfils	Monfils	k1gInSc1
</s>
<s>
2020	#num#	k4
</s>
<s>
N.	N.	kA
Djoković	Djoković	k1gMnSc1
R.	R.	kA
Nadal	nadat	k5eAaPmAgMnS
D.	D.	kA
Thiem	Thiem	k1gInSc1
D.	D.	kA
Medveděv	Medveděv	k1gFnSc2
R.	R.	kA
Federer	Federer	k1gMnSc1
S.	S.	kA
Tsitsipas	Tsitsipas	k1gMnSc1
A.	A.	kA
Zverev	Zverva	k1gFnPc2
A.	A.	kA
Rubljov	Rubljov	k1gInSc1
D.	D.	kA
Schwartzman	Schwartzman	k1gMnSc1
M.	M.	kA
Berrettini	Berrettin	k2eAgMnPc5d1
</s>
<s>
Světové	světový	k2eAgFnPc1d1
jedničky	jednička	k1gFnPc1
na	na	k7c6
čele	čelo	k1gNnSc6
dle	dle	k7c2
týdnů	týden	k1gInPc2
v	v	k7c6
desetiletích	desetiletí	k1gNnPc6
</s>
<s>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Col-begin	Col-begin	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
1970	#num#	k4
<g/>
–	–	k?
<g/>
1979	#num#	k4
</s>
<s>
251	#num#	k4
Connors	Connors	k1gInSc1
</s>
<s>
40	#num#	k4
Năstase	Năstas	k1gInSc6
</s>
<s>
33	#num#	k4
Borg	Borg	k1gMnSc1
</s>
<s>
8	#num#	k4
Newcombe	Newcomb	k1gInSc5
</s>
<s>
1980	#num#	k4
<g/>
–	–	k?
<g/>
1989	#num#	k4
</s>
<s>
238	#num#	k4
Lendl	Lendl	k1gMnSc1
</s>
<s>
170	#num#	k4
McEnroe	McEnro	k1gInSc2
</s>
<s>
76	#num#	k4
Borg	Borg	k1gMnSc1
</s>
<s>
20	#num#	k4
Wilander	Wilander	k1gInSc1
</s>
<s>
17	#num#	k4
Connors	Connors	k1gInSc1
</s>
<s>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Col-begin	Col-begin	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
1990	#num#	k4
<g/>
–	–	k?
<g/>
1999	#num#	k4
</s>
<s>
276	#num#	k4
Sampras	Sampras	k1gMnSc1
</s>
<s>
72	#num#	k4
Edberg	Edberg	k1gMnSc1
</s>
<s>
58	#num#	k4
Courier	Courier	k1gMnSc1
</s>
<s>
51	#num#	k4
Agassi	Agass	k1gMnSc6
</s>
<s>
32	#num#	k4
Lendl	Lendl	k1gMnSc1
</s>
<s>
12	#num#	k4
Becker	Becker	k1gMnSc1
</s>
<s>
6	#num#	k4
Muster	Muster	k1gInSc1
<g/>
,	,	kIx,
Ríos	Ríos	k1gInSc1
<g/>
,	,	kIx,
Kafelnikov	Kafelnikov	k1gInSc1
</s>
<s>
2	#num#	k4
Moyà	Moyà	k1gFnSc1
</s>
<s>
1	#num#	k4
Rafter	Rafter	k1gInSc1
</s>
<s>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
</s>
<s>
262	#num#	k4
Federer	Federer	k1gInSc1
</s>
<s>
80	#num#	k4
Hewitt	Hewitt	k1gInSc1
</s>
<s>
50	#num#	k4
Agassi	Agass	k1gMnPc7
</s>
<s>
46	#num#	k4
Nadal	nadat	k5eAaPmAgMnS
</s>
<s>
43	#num#	k4
Kuerten	Kuerten	k2eAgInSc1d1
</s>
<s>
13	#num#	k4
Roddick	Roddick	k1gInSc1
</s>
<s>
10	#num#	k4
Sampras	Sampras	k1gMnSc1
</s>
<s>
9	#num#	k4
Safin	Safin	k1gInSc1
</s>
<s>
8	#num#	k4
Ferrero	Ferrero	k1gNnSc1
</s>
<s>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Col-begin	Col-begin	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
</s>
<s>
275	#num#	k4
Djoković	Djoković	k1gFnSc1
</s>
<s>
159	#num#	k4
Nadal	nadat	k5eAaPmAgMnS
</s>
<s>
48	#num#	k4
Federer	Federer	k1gInSc1
</s>
<s>
41	#num#	k4
Murray	Murra	k2eAgInPc1d1
</s>
<s>
2020	#num#	k4
<g/>
–	–	k?
<g/>
2029	#num#	k4
</s>
<s>
45	#num#	k4
Djoković	Djoković	k1gFnSc1
</s>
<s>
4	#num#	k4
Nadal	nadat	k5eAaPmAgMnS
</s>
<s>
Aktualizováno	aktualizován	k2eAgNnSc1d1
20210510	#num#	k4
<g/>
a	a	k8xC
<g/>
10	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2021	#num#	k4
</s>
<s>
Světové	světový	k2eAgFnPc1d1
jedničky	jednička	k1gFnPc1
ve	v	k7c6
čtyřhře	čtyřhra	k1gFnSc6
</s>
<s>
Toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
seznam	seznam	k1gInSc1
hráčů	hráč	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
dosáhli	dosáhnout	k5eAaPmAgMnP
na	na	k7c6
pozici	pozice	k1gFnSc6
světové	světový	k2eAgFnSc2d1
jedničky	jednička	k1gFnSc2
v	v	k7c6
deblu	debl	k1gInSc6
od	od	k7c2
března	březen	k1gInSc2
1976	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
čtyřhry	čtyřhra	k1gFnSc2
</s>
<s>
Mike	Mike	k6eAd1
Bryan	Bryan	k1gInSc1
drží	držet	k5eAaImIp3nS
rekord	rekord	k1gInSc4
v	v	k7c6
počtu	počet	k1gInSc6
týdnů	týden	k1gInPc2
na	na	k7c6
čele	čelo	k1gNnSc6
deblové	deblový	k2eAgFnSc2d1
klasifikace	klasifikace	k1gFnSc2
ATP	atp	kA
</s>
<s>
Poř	Poř	k?
<g/>
.	.	kIx.
<g/>
státtenistatýdnů	státtenistatýdn	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéBryan	americkéBryany	k1gInPc2
<g/>
,	,	kIx,
MikeMike	MikeMike	k1gFnSc1
Bryan	Bryan	k1gInSc1
<g/>
506	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéBryan	americkéBryana	k1gFnPc2
<g/>
,	,	kIx,
BobBob	BobBoba	k1gFnPc2
Bryan	Bryany	k1gInPc2
<g/>
439	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéMcEnroe	americkéMcEnroe	k1gNnSc2
<g/>
,	,	kIx,
JohnJohn	JohnJohn	k1gMnSc1
McEnroe	McEnro	k1gFnSc2
<g/>
269	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
<g/>
Austrálie	Austrálie	k1gFnSc2
AustrálieWoodbridge	AustrálieWoodbridg	k1gFnSc2
<g/>
,	,	kIx,
ToddTodd	ToddTodd	k1gMnSc1
Woodbridge	Woodbridg	k1gFnSc2
<g/>
204	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
<g/>
Kanada	Kanada	k1gFnSc1
KanadaNestor	KanadaNestor	k1gMnSc1
<g/>
,	,	kIx,
DanielDaniel	DanielDaniel	k1gMnSc1
Nestor	Nestor	k1gMnSc1
<g/>
108	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
Švédsko	Švédsko	k1gNnSc1
ŠvédskoJärryd	ŠvédskoJärryda	k1gFnPc2
<g/>
,	,	kIx,
AndersAnders	AndersAnders	k1gInSc1
Järryd	Järryd	k1gInSc1
<g/>
107	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republikaMcMillan	republikaMcMillan	k1gInSc1
<g/>
,	,	kIx,
FrewFrew	FrewFrew	k1gFnSc1
McMillan	McMillan	k1gInSc1
<g/>
83	#num#	k4
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
AustrálieWoodforde	AustrálieWoodford	k1gInSc5
<g/>
,	,	kIx,
MarkMark	MarkMark	k1gInSc1
Woodforde	Woodford	k1gInSc5
<g/>
83	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
Švédsko	Švédsko	k1gNnSc1
ŠvédskoBjörkman	ŠvédskoBjörkman	k1gMnSc1
<g/>
,	,	kIx,
JonasJonas	JonasJonas	k1gMnSc1
Björkman	Björkman	k1gMnSc1
<g/>
74	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
<g/>
Nizozemsko	Nizozemsko	k1gNnSc1
NizozemskoHaarhuis	NizozemskoHaarhuis	k1gFnSc1
<g/>
,	,	kIx,
PaulPaul	PaulPaul	k1gInSc1
Haarhuis	Haarhuis	k1gFnSc1
<g/>
71	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
<g/>
Kolumbie	Kolumbie	k1gFnSc1
KolumbieFarah	KolumbieFarah	k1gMnSc1
<g/>
,	,	kIx,
RobertRobert	RobertRobert	k1gMnSc1
Farah	Farah	k1gMnSc1
<g/>
68	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
<g/>
Mexiko	Mexiko	k1gNnSc1
MexikoRamirez	MexikoRamireza	k1gFnPc2
<g/>
,	,	kIx,
RaulRaul	RaulRaul	k1gInSc1
Ramirez	Ramirez	k1gInSc1
<g/>
67	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
<g/>
Bahamy	Bahamy	k1gFnPc1
BahamyKnowles	BahamyKnowles	k1gInSc1
<g/>
,	,	kIx,
MarkMark	MarkMark	k1gInSc1
Knowles	Knowles	k1gInSc1
<g/>
65	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
<g/>
Nizozemsko	Nizozemsko	k1gNnSc1
NizozemskoEltingh	NizozemskoEltingha	k1gFnPc2
<g/>
,	,	kIx,
JaccoJacco	JaccoJacco	k6eAd1
Eltingh	Eltingh	k1gInSc1
<g/>
63	#num#	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéSeguso	americkéSegusa	k1gFnSc5
<g/>
,	,	kIx,
RobertRobert	RobertRobert	k1gMnSc1
Seguso	Segusa	k1gFnSc5
<g/>
62	#num#	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
<g/>
Bělorusko	Bělorusko	k1gNnSc1
BěloruskoMirnyj	BěloruskoMirnyj	k1gFnSc1
<g/>
,	,	kIx,
MaxMax	MaxMax	k1gInSc1
Mirnyj	Mirnyj	k1gFnSc1
<g/>
57	#num#	k4
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
<g/>
Brazílie	Brazílie	k1gFnSc2
BrazílieMelo	BrazílieMela	k1gFnSc5
<g/>
,	,	kIx,
MarceloMarcelo	MarceloMarcelo	k1gMnSc1
Melo	Melo	k?
<g/>
56	#num#	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
<g/>
Srbsko	Srbsko	k1gNnSc1
SrbskoZimonjić	SrbskoZimonjić	k1gFnSc1
<g/>
,	,	kIx,
NenadNenad	NenadNenad	k1gInSc1
Zimonjić	Zimonjić	k1gFnSc1
<g/>
50	#num#	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
<g/>
Austrálie	Austrálie	k1gFnSc2
AustrálieFitzgerald	AustrálieFitzgerald	k1gInSc1
<g/>
,	,	kIx,
JohnJohn	JohnJohn	k1gInSc1
Fitzgerald	Fitzgerald	k1gInSc1
<g/>
40	#num#	k4
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
<g/>
Indie	Indie	k1gFnSc1
IndiePaes	IndiePaes	k1gInSc1
<g/>
,	,	kIx,
LeanderLeander	LeanderLeander	k1gInSc1
Paes	Paes	k1gInSc1
<g/>
39	#num#	k4
</s>
<s>
Francie	Francie	k1gFnSc1
FrancieMahut	FrancieMahut	k1gInSc1
<g/>
,	,	kIx,
NicolasNicolas	NicolasNicolas	k1gInSc1
Mahut	Mahut	k1gInSc1
<g/>
39	#num#	k4
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
<g/>
Československo	Československo	k1gNnSc1
ČeskoslovenskoŠmíd	ČeskoslovenskoŠmída	k1gFnPc2
<g/>
,	,	kIx,
TomášTomáš	TomášTomáš	k1gMnSc1
Šmíd	Šmíd	k1gMnSc1
<g/>
34	#num#	k4
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
<g/>
Kolumbie	Kolumbie	k1gFnSc1
KolumbieCabal	KolumbieCabal	k1gInSc1
<g/>
,	,	kIx,
Juan	Juan	k1gMnSc1
SebastiánJuan	SebastiánJuan	k1gMnSc1
Sebastián	Sebastián	k1gMnSc1
Cabal	Cabal	k1gMnSc1
<g/>
29	#num#	k4
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republikaVisser	republikaVisser	k1gInSc1
<g/>
,	,	kIx,
DanieDanie	DanieDanie	k1gFnSc1
Visser	Visser	k1gInSc1
<g/>
27	#num#	k4
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéPugh	americkéPugha	k1gFnPc2
<g/>
,	,	kIx,
JimJim	JimJim	k1gInSc1
Pugh	Pugh	k1gInSc1
<g/>
26	#num#	k4
</s>
<s>
Finsko	Finsko	k1gNnSc1
FinskoKontinen	FinskoKontinna	k1gFnPc2
<g/>
,	,	kIx,
HenriHenri	HenriHenri	k1gNnSc7
Kontinen	Kontinna	k1gFnPc2
<g/>
26	#num#	k4
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéPate	americkéPat	k1gInSc5
<g/>
,	,	kIx,
DavidDavid	DavidDavid	k1gInSc1
Pate	pat	k1gInSc5
<g/>
25	#num#	k4
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéJohnson	americkéJohnsona	k1gFnPc2
<g/>
,	,	kIx,
DonaldDonald	DonaldDonald	k1gMnSc1
Johnson	Johnson	k1gMnSc1
<g/>
20	#num#	k4
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republikaAldrich	republikaAldrich	k1gInSc1
<g/>
,	,	kIx,
PieterPieter	PieterPieter	k1gInSc1
Aldrich	Aldrich	k1gInSc1
<g/>
19	#num#	k4
</s>
<s>
Francie	Francie	k1gFnSc1
FrancieNoah	FrancieNoah	k1gMnSc1
<g/>
,	,	kIx,
YannickYannick	YannickYannick	k1gMnSc1
Noah	Noah	k1gMnSc1
<g/>
19	#num#	k4
</s>
<s>
Polsko	Polsko	k1gNnSc1
PolskoKubot	PolskoKubot	k1gMnSc1
<g/>
,	,	kIx,
ŁukaszŁukasz	ŁukaszŁukasz	k1gMnSc1
Kubot	Kubot	k1gMnSc1
<g/>
19	#num#	k4
</s>
<s>
32	#num#	k4
<g/>
.	.	kIx.
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéPalmer	americkéPalmra	k1gFnPc2
<g/>
,	,	kIx,
JaredJared	JaredJared	k1gMnSc1
Palmer	Palmer	k1gMnSc1
<g/>
17	#num#	k4
</s>
<s>
Kanada	Kanada	k1gFnSc1
KanadaConnell	KanadaConnell	k1gMnSc1
<g/>
,	,	kIx,
GrantGrant	GrantGrant	k1gMnSc1
Connell	Connell	k1gMnSc1
<g/>
17	#num#	k4
</s>
<s>
34	#num#	k4
<g/>
.	.	kIx.
<g/>
Švédsko	Švédsko	k1gNnSc1
ŠvédskoEdberg	ŠvédskoEdberg	k1gMnSc1
<g/>
,	,	kIx,
StefanStefan	StefanStefan	k1gMnSc1
Edberg	Edberg	k1gMnSc1
<g/>
15	#num#	k4
</s>
<s>
35	#num#	k4
<g/>
.	.	kIx.
<g/>
Chorvatsko	Chorvatsko	k1gNnSc4
ChorvatskoPavić	ChorvatskoPavić	k1gFnSc2
<g/>
,	,	kIx,
MateMate	MateMat	k1gMnSc5
Pavić	Pavić	k1gMnSc5
<g/>
14	#num#	k4
</s>
<s>
36	#num#	k4
<g/>
.	.	kIx.
<g/>
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgInPc4d1
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americkéReneberg	americkéReneberg	k1gInSc1
<g/>
,	,	kIx,
RicheyRichey	RicheyRichey	k1gInPc1
Reneberg	Reneberg	k1gInSc1
<g/>
13	#num#	k4
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéGrabb	americkéGrabba	k1gFnPc2
<g/>
,	,	kIx,
JimJim	JimJim	k1gInSc1
Grabb	Grabb	k1gInSc1
<g/>
13	#num#	k4
</s>
<s>
Ekvádor	Ekvádor	k1gInSc1
EkvádorGómez	EkvádorGómez	k1gInSc1
<g/>
,	,	kIx,
AndrésAndrés	AndrésAndrés	k1gInSc1
Gómez	Gómez	k1gInSc1
<g/>
13	#num#	k4
</s>
<s>
39	#num#	k4
<g/>
.	.	kIx.
<g/>
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgInPc4d1
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americkéFleming	americkéFleming	k1gInSc1
<g/>
,	,	kIx,
PeterPeter	PeterPeter	k1gInSc1
Fleming	Fleming	k1gInSc1
<g/>
11	#num#	k4
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
NizozemskoOkker	NizozemskoOkker	k1gInSc1
<g/>
,	,	kIx,
TomTom	TomTom	k1gInSc1
Okker	Okker	k1gInSc1
<g/>
11	#num#	k4
</s>
<s>
41	#num#	k4
<g/>
.	.	kIx.
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéLeach	americkéLeacha	k1gFnPc2
<g/>
,	,	kIx,
RickRick	RickRick	k1gInSc1
Leach	Leach	k1gInSc1
<g/>
9	#num#	k4
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgFnSc2d1
královstvíMurray	královstvíMurraa	k1gFnSc2
<g/>
,	,	kIx,
JamieJamie	JamieJamie	k1gFnSc2
Murray	Murraa	k1gFnSc2
<g/>
9	#num#	k4
</s>
<s>
43	#num#	k4
<g/>
.	.	kIx.
<g/>
Zimbabwe	Zimbabw	k1gInSc2
ZimbabweBlack	ZimbabweBlack	k1gInSc1
<g/>
,	,	kIx,
ByronByron	ByronByron	k1gInSc1
Black	Black	k1gInSc1
<g/>
8	#num#	k4
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéSmith	americkéSmitha	k1gFnPc2
<g/>
,	,	kIx,
StanStan	StanStan	k1gInSc1
Smith	Smith	k1gInSc1
<g/>
8	#num#	k4
</s>
<s>
45	#num#	k4
<g/>
.	.	kIx.
<g/>
Jugoslávie	Jugoslávie	k1gFnSc1
JugoslávieŽivojinović	JugoslávieŽivojinović	k1gFnSc1
<g/>
,	,	kIx,
SlobodanSlobodan	SlobodanSlobodan	k1gInSc1
Živojinović	Živojinović	k1gFnSc1
<g/>
7	#num#	k4
</s>
<s>
46	#num#	k4
<g/>
.	.	kIx.
<g/>
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgInPc4d1
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americkéStark	americkéStark	k1gInSc1
<g/>
,	,	kIx,
JonathanJonathan	JonathanJonathan	k1gInSc1
Stark	Stark	k1gInSc1
<g/>
6	#num#	k4
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
ŠpanělskoSánchez	ŠpanělskoSánchez	k1gMnSc1
<g/>
,	,	kIx,
EmilioEmilio	EmilioEmilio	k1gMnSc1
Sánchez	Sánchez	k1gMnSc1
<g/>
6	#num#	k4
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republikaHewitt	republikaHewitt	k1gInSc1
<g/>
,	,	kIx,
BobBob	BobBoba	k1gFnPc2
Hewitt	Hewittum	k1gNnPc2
<g/>
6	#num#	k4
</s>
<s>
49	#num#	k4
<g/>
.	.	kIx.
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéO	americkéO	k?
<g/>
'	'	kIx"
<g/>
Brien	Brien	k1gInSc1
<g/>
,	,	kIx,
AlexAlex	AlexAlex	k1gInSc1
O	O	kA
<g/>
'	'	kIx"
<g/>
Brien	Brien	k1gInSc1
<g/>
5	#num#	k4
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéFlach	americkéFlacha	k1gFnPc2
<g/>
,	,	kIx,
KenKen	KenKen	k2eAgInSc1d1
Flach	Flach	k1gInSc1
<g/>
5	#num#	k4
</s>
<s>
51	#num#	k4
<g/>
.	.	kIx.
<g/>
Indie	Indie	k1gFnSc2
IndieBhúpatí	IndieBhúpatý	k2eAgMnPc1d1
<g/>
,	,	kIx,
MahešMaheš	MahešMaheš	k1gMnPc1
Bhúpatí	Bhúpatý	k2eAgMnPc1d1
<g/>
4	#num#	k4
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéGalbraith	americkéGalbraitha	k1gFnPc2
<g/>
,	,	kIx,
PatrickPatrick	PatrickPatrick	k1gInSc1
Galbraith	Galbraith	k1gInSc1
<g/>
4	#num#	k4
</s>
<s>
53	#num#	k4
<g/>
.	.	kIx.
<g/>
Austrálie	Austrálie	k1gFnSc1
AustrálieMcNamee	AustrálieMcNamee	k1gFnSc1
<g/>
,	,	kIx,
PaulPaul	PaulPaul	k1gInSc1
McNamee	McNamee	k1gFnSc1
<g/>
3	#num#	k4
</s>
<s>
54	#num#	k4
<g/>
.	.	kIx.
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéJones	americkéJonesa	k1gFnPc2
<g/>
,	,	kIx,
KellyKelly	KellyKell	k1gInPc1
Jones	Jones	k1gInSc1
<g/>
1	#num#	k4
</s>
<s>
–	–	k?
aktivní	aktivní	k2eAgMnSc1d1
hráč	hráč	k1gMnSc1
<g/>
;	;	kIx,
tučně	tučně	k6eAd1
–	–	k?
současná	současný	k2eAgFnSc1d1
světová	světový	k2eAgFnSc1d1
jednička	jednička	k1gFnSc1
<g/>
;	;	kIx,
stav	stav	k1gInSc1
k	k	k7c3
10	#num#	k4
<g/>
.	.	kIx.
květnu	květen	k1gInSc3
2021	#num#	k4
</s>
<s>
Bez	bez	k7c2
přerušení	přerušení	k1gNnSc2
–	–	k?
Top	topit	k5eAaImRp2nS
20	#num#	k4
čtyřhry	čtyřhra	k1gFnSc2
</s>
<s>
John	John	k1gMnSc1
McEnroe	McEnro	k1gInSc2
strávil	strávit	k5eAaPmAgMnS
na	na	k7c6
čele	čelo	k1gNnSc6
deblové	deblový	k2eAgFnSc2d1
klasifikace	klasifikace	k1gFnSc2
dvě	dva	k4xCgNnPc4
dlouhá	dlouhý	k2eAgNnPc4d1
období	období	k1gNnPc4
bez	bez	k7c2
přerušení	přerušení	k1gNnSc2
–	–	k?
108	#num#	k4
a	a	k8xC
97	#num#	k4
týdnů	týden	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Poř	Poř	k?
<g/>
.	.	kIx.
<g/>
státtenistatýdnů	státtenistatýdn	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéBryan	americkéBryany	k1gInPc2
<g/>
,	,	kIx,
MikeMike	MikeMike	k1gFnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
163	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéBryan	americkéBryana	k1gFnPc2
<g/>
,	,	kIx,
BobBob	BobBoba	k1gFnPc2
Bryan	Bryana	k1gFnPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
140	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéWoodbridge	americkéWoodbridge	k1gNnSc2
<g/>
,	,	kIx,
ToddTodd	ToddTodd	k1gMnSc1
Woodbridge	Woodbridg	k1gFnSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
125	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéMcEnroe	americkéMcEnro	k1gFnSc2
<g/>
,	,	kIx,
JohnJohn	JohnJohn	k1gNnSc1
McEnroe	McEnroe	k1gFnPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
108	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéMcEnroe	americkéMcEnro	k1gFnSc2
<g/>
,	,	kIx,
JohnJohn	JohnJohn	k1gNnSc1
McEnroe	McEnroe	k1gFnPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
97	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéBryan	americkéBryana	k1gFnPc2
<g/>
,	,	kIx,
BobBob	BobBoba	k1gFnPc2
Bryan	Bryana	k1gFnPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
90	#num#	k4
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéMike	americkéMike	k1gFnPc2
Bryan	Bryana	k1gFnPc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
90	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
<g/>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republikaMcMillan	republikaMcMillan	k1gInSc1
<g/>
,	,	kIx,
FrewFrew	FrewFrew	k1gMnSc1
McMillan	McMillan	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
80	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
Kolumbie	Kolumbie	k1gFnSc1
KolumbieFarah	KolumbieFarah	k1gMnSc1
<g/>
,	,	kIx,
RobertRobert	RobertRobert	k1gMnSc1
Farah	Farah	k1gMnSc1
<g/>
68	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéBob	americkéBoba	k1gFnPc2
Bryan	Bryana	k1gFnPc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
64	#num#	k4
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéMike	americkéMike	k1gFnPc2
Bryan	Bryana	k1gFnPc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
64	#num#	k4
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéBob	americkéBoba	k1gFnPc2
Bryan	Bryana	k1gFnPc2
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
64	#num#	k4
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéMike	americkéMike	k1gFnPc2
Bryan	Bryana	k1gFnPc2
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
64	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
<g/>
Mexiko	Mexiko	k1gNnSc1
MexikoRamírez	MexikoRamíreza	k1gFnPc2
<g/>
,	,	kIx,
RaúlRaúl	RaúlRaúl	k1gMnSc1
Ramírez	Ramírez	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
54	#num#	k4
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
Austrálie	Austrálie	k1gFnSc2
AustrálieWoodforde	AustrálieWoodford	k1gInSc5
<g/>
,	,	kIx,
MarkMark	MarkMark	k1gInSc1
Woodforde	Woodford	k1gInSc5
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
52	#num#	k4
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéMike	americkéMike	k1gFnPc2
Bryan	Bryana	k1gFnPc2
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
5	#num#	k4
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
<g/>
Švédsko	Švédsko	k1gNnSc1
ŠvédskoJärryd	ŠvédskoJärryda	k1gFnPc2
<g/>
,	,	kIx,
AndersAnders	AndersAndersa	k1gFnPc2
Järryd	Järryda	k1gFnPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
47	#num#	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
<g/>
Nizozemsko	Nizozemsko	k1gNnSc1
NizozemskoEltingh	NizozemskoEltingha	k1gFnPc2
<g/>
,	,	kIx,
JaccoJacco	JaccoJacco	k1gMnSc1
Eltingh	Eltingh	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
46	#num#	k4
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
<g/>
Indie	Indie	k1gFnSc1
IndiePaes	IndiePaes	k1gInSc1
<g/>
,	,	kIx,
LeanderLeander	LeanderLeander	k1gInSc1
Paes	Paes	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
39	#num#	k4
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
<g/>
Francie	Francie	k1gFnSc1
FrancieMahut	FrancieMahut	k1gMnSc1
<g/>
,	,	kIx,
NicolasNicolas	NicolasNicolas	k1gMnSc1
Mahut	Mahut	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
38	#num#	k4
</s>
<s>
tučně	tučně	k6eAd1
–	–	k?
aktuální	aktuální	k2eAgNnSc4d1
období	období	k1gNnSc4
současné	současný	k2eAgFnSc2d1
světové	světový	k2eAgFnSc2d1
jedničky	jednička	k1gFnSc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
probíhá	probíhat	k5eAaImIp3nS
v	v	k7c6
rámci	rámec	k1gInSc6
Top	topit	k5eAaImRp2nS
20	#num#	k4
<g/>
;	;	kIx,
</s>
<s>
Týdny	týden	k1gInPc1
na	na	k7c6
čele	čelo	k1gNnSc6
žebříčku	žebříček	k1gInSc2
čtyřhry	čtyřhra	k1gFnSc2
podle	podle	k7c2
státu	stát	k1gInSc2
</s>
<s>
Poř	Poř	k?
<g/>
.	.	kIx.
</s>
<s>
stát	stát	k1gInSc1
</s>
<s>
hráčů	hráč	k1gMnPc2
</s>
<s>
týdnů	týden	k1gInPc2
</s>
<s>
tenisté	tenista	k1gMnPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
<g/>
181	#num#	k4
439	#num#	k4
<g/>
John	John	k1gMnSc1
McEnroe	McEnroe	k1gFnSc4
<g/>
,	,	kIx,
Stan	stan	k1gInSc4
Smith	Smitha	k1gFnPc2
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
Fleming	Fleming	k1gInSc1
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
Seguso	Segusa	k1gFnSc5
<g/>
,	,	kIx,
Ken	Ken	k1gMnSc1
Flach	Flach	k1gMnSc1
<g/>
,	,	kIx,
Jim	on	k3xPp3gMnPc3
Grabb	Grabb	k1gInSc1
<g/>
,	,	kIx,
Jim	on	k3xPp3gMnPc3
Pugh	Pugh	k1gMnSc1
<g/>
,	,	kIx,
Rick	Rick	k1gMnSc1
Leach	Leach	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
Pate	pat	k1gInSc5
<g/>
,	,	kIx,
Kelly	Kello	k1gNnPc7
Jones	Jones	k1gInSc1
<g/>
,	,	kIx,
Richey	Richea	k1gFnPc1
Reneberg	Reneberg	k1gMnSc1
<g/>
,	,	kIx,
Patrick	Patrick	k1gMnSc1
Galbraith	Galbraith	k1gMnSc1
<g/>
,	,	kIx,
Jonathan	Jonathan	k1gMnSc1
Stark	Stark	k1gInSc1
<g/>
,	,	kIx,
Jared	Jared	k1gMnSc1
Palmer	Palmer	k1gMnSc1
<g/>
,	,	kIx,
Alex	Alex	k1gMnSc1
O	O	kA
<g/>
'	'	kIx"
<g/>
Brien	Brien	k1gInSc1
<g/>
,	,	kIx,
Donald	Donald	k1gMnSc1
Johnson	Johnson	k1gMnSc1
<g/>
,	,	kIx,
Bob	Bob	k1gMnSc1
Bryan	Bryan	k1gMnSc1
<g/>
,	,	kIx,
Mike	Mike	k1gFnSc1
Bryan	Bryana	k1gFnPc2
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
Austrálie	Austrálie	k1gFnSc2
Austrálie	Austrálie	k1gFnSc2
<g/>
4330	#num#	k4
<g/>
Paul	Paul	k1gMnSc1
McNamee	McName	k1gInSc2
<g/>
,	,	kIx,
John	John	k1gMnSc1
Fitzgerald	Fitzgerald	k1gMnSc1
<g/>
,	,	kIx,
Todd	Todd	k1gMnSc1
Woodbridge	Woodbridg	k1gFnSc2
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
Woodforde	Woodford	k1gInSc5
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
Švédsko	Švédsko	k1gNnSc4
Švédsko	Švédsko	k1gNnSc1
<g/>
3196	#num#	k4
<g/>
Anders	Andersa	k1gFnPc2
Järryd	Järryd	k1gMnSc1
<g/>
,	,	kIx,
Stefan	Stefan	k1gMnSc1
Edberg	Edberg	k1gMnSc1
<g/>
,	,	kIx,
Jonas	Jonas	k1gMnSc1
Björkman	Björkman	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
<g/>
Nizozemsko	Nizozemsko	k1gNnSc4
Nizozemsko	Nizozemsko	k1gNnSc1
<g/>
3145	#num#	k4
<g/>
Tom	Tom	k1gMnSc1
Okker	Okker	k1gMnSc1
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
Haarhuis	Haarhuis	k1gFnSc2
<g/>
,	,	kIx,
Jacco	Jacco	k1gMnSc1
Eltingh	Eltingh	k1gMnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
<g/>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
4135	#num#	k4
<g/>
Bob	Bob	k1gMnSc1
Hewitt	Hewitt	k1gMnSc1
<g/>
,	,	kIx,
Frew	Frew	k1gMnSc1
McMillan	McMillan	k1gMnSc1
<g/>
,	,	kIx,
Danie	Danie	k1gFnSc1
Visser	Visser	k1gMnSc1
<g/>
,	,	kIx,
Pieter	Pieter	k1gMnSc1
Aldrich	Aldrich	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
Kanada	Kanada	k1gFnSc1
Kanada	Kanada	k1gFnSc1
<g/>
2125	#num#	k4
<g/>
Grant	grant	k1gInSc1
Connell	Connell	k1gInSc4
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
Nestor	Nestor	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
<g/>
Kolumbie	Kolumbie	k1gFnSc1
Kolumbie	Kolumbie	k1gFnSc2
<g/>
297	#num#	k4
<g/>
Juan	Juan	k1gMnSc1
Sebastián	Sebastián	k1gMnSc1
Cabal	Cabal	k1gMnSc1
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
Farah	Farah	k1gMnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
<g/>
Mexiko	Mexiko	k1gNnSc4
Mexiko	Mexiko	k1gNnSc1
<g/>
167	#num#	k4
<g/>
Raúl	Raúlum	k1gNnPc2
Ramírez	Ramíreza	k1gFnPc2
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
Bahamy	Bahamy	k1gFnPc1
Bahamy	Bahamy	k1gFnPc1
<g/>
165	#num#	k4
<g/>
Mark	Mark	k1gMnSc1
Knowles	Knowles	k1gMnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
<g/>
Srbsko	Srbsko	k1gNnSc4
SrbskoJugoslávie	SrbskoJugoslávie	k1gFnSc2
Jugoslávie	Jugoslávie	k1gFnSc2
<g/>
257	#num#	k4
<g/>
Slobodan	Slobodan	k1gMnSc1
Živojinović	Živojinović	k1gMnSc1
<g/>
,	,	kIx,
Nenad	Nenad	k1gInSc1
Zimonjić	Zimonjić	k1gFnSc2
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc4
Bělorusko	Bělorusko	k1gNnSc1
<g/>
1	#num#	k4
<g/>
Max	max	kA
Mirnyj	Mirnyj	k1gMnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
Francie	Francie	k1gFnSc2
<g/>
2	#num#	k4
<g/>
Yannick	Yannick	k1gMnSc1
Noah	Noah	k1gMnSc1
<g/>
,	,	kIx,
Nicolas	Nicolas	k1gMnSc1
Mahut	Mahut	k1gMnSc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
<g/>
Brazílie	Brazílie	k1gFnSc1
Brazílie	Brazílie	k1gFnSc2
<g/>
156	#num#	k4
<g/>
Marcelo	Marcela	k1gFnSc5
Melo	Melo	k?
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
<g/>
Indie	Indie	k1gFnSc1
Indie	Indie	k1gFnSc2
<g/>
243	#num#	k4
<g/>
Maheš	Maheš	k1gMnSc1
Bhúpatí	Bhúpatý	k2eAgMnPc1d1
<g/>
,	,	kIx,
Leander	Leander	k1gMnSc1
Paes	Paesa	k1gFnPc2
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
<g/>
Československo	Československo	k1gNnSc4
Československo	Československo	k1gNnSc1
<g/>
134	#num#	k4
<g/>
Tomáš	Tomáš	k1gMnSc1
Šmíd	Šmíd	k1gMnSc1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
<g/>
Finsko	Finsko	k1gNnSc4
Finsko	Finsko	k1gNnSc1
<g/>
126	#num#	k4
<g/>
Henri	Henri	k1gNnPc2
Kontinen	Kontinna	k1gFnPc2
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
<g/>
Polsko	Polsko	k1gNnSc4
Polsko	Polsko	k1gNnSc1
<g/>
119	#num#	k4
<g/>
Łukasz	Łukasza	k1gFnPc2
Kubot	Kubota	k1gFnPc2
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
<g/>
Chorvatsko	Chorvatsko	k1gNnSc4
Chorvatsko	Chorvatsko	k1gNnSc1
<g/>
114	#num#	k4
<g/>
Mate	mást	k5eAaImIp3nS
Pavić	Pavić	k1gFnSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
<g/>
Ekvádor	Ekvádor	k1gInSc1
Ekvádor	Ekvádor	k1gInSc1
<g/>
113	#num#	k4
<g/>
Andrés	Andrésa	k1gFnPc2
Gómez	Gómeza	k1gFnPc2
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
<g/>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc2
<g/>
19	#num#	k4
<g/>
Jamie	Jamie	k1gFnSc2
Murray	Murraa	k1gFnSc2
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
<g/>
Zimbabwe	Zimbabwe	k1gNnSc1
Zimbabwe	Zimbabw	k1gFnSc2
<g/>
18	#num#	k4
<g/>
Byron	Byron	k1gMnSc1
Black	Black	k1gMnSc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
<g/>
Španělsko	Španělsko	k1gNnSc4
Španělsko	Španělsko	k1gNnSc1
<g/>
16	#num#	k4
<g/>
Emilio	Emilio	k1gMnSc1
Sánchez	Sánchez	k1gMnSc1
</s>
<s>
stav	stav	k1gInSc1
k	k	k7c3
10	#num#	k4
<g/>
.	.	kIx.
květnu	květen	k1gInSc3
2021	#num#	k4
<g/>
;	;	kIx,
tučně	tučně	k6eAd1
–	–	k?
aktivní	aktivní	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
</s>
<s>
Konečný	konečný	k2eAgInSc4d1
žebříček	žebříček	k1gInSc4
čtyřhry	čtyřhra	k1gFnSc2
</s>
<s>
Počet	počet	k1gInSc1
zakončení	zakončení	k1gNnSc2
na	na	k7c6
čele	čelo	k1gNnSc6
žebříčku	žebříček	k1gInSc2
čtyřhry	čtyřhra	k1gFnSc2
</s>
<s>
Mike	Mike	k6eAd1
Bryan	Bryan	k1gMnSc1
zakončil	zakončit	k5eAaPmAgMnS
desetkrát	desetkrát	k6eAd1
sezónu	sezóna	k1gFnSc4
na	na	k7c6
prvním	první	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc4
dvojče	dvojče	k1gNnSc4
Bob	Bob	k1gMnSc1
Bryan	Bryan	k1gMnSc1
pak	pak	k6eAd1
osmkrát	osmkrát	k6eAd1
</s>
<s>
státtenistapočet	státtenistapočet	k1gInSc1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéMike	americkéMike	k1gFnSc1
Bryan	Bryan	k1gInSc1
<g/>
10	#num#	k4
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéBob	americkéBoba	k1gFnPc2
Bryan	Bryana	k1gFnPc2
<g/>
8	#num#	k4
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéJohn	americkéJohna	k1gFnPc2
McEnroe	McEnro	k1gInSc2
<g/>
5	#num#	k4
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
AustrálieMark	AustrálieMark	k1gInSc1
Woodforde	Woodford	k1gMnSc5
<g/>
3	#num#	k4
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
AustrálieTodd	AustrálieTodda	k1gFnPc2
Woodbridge	Woodbridg	k1gFnSc2
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republikaFrew	republikaFrew	k?
McMillan	McMillan	k1gInSc4
<g/>
2	#num#	k4
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americkéRobert	americkéRobert	k1gInSc4
Seguso	Segusa	k1gFnSc5
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
ŠvédskoAnders	ŠvédskoAndersa	k1gFnPc2
Järryd	Järryda	k1gFnPc2
</s>
<s>
Bahamy	Bahamy	k1gFnPc1
BahamyMark	BahamyMark	k1gInSc1
Knowles	Knowles	k1gInSc4
</s>
<s>
Brazílie	Brazílie	k1gFnSc1
BrazílieMarcelo	BrazílieMarcelo	k1gFnSc2
Melo	Melo	k?
</s>
<s>
Kolumbie	Kolumbie	k1gFnSc1
KolumbieRobert	KolumbieRobert	k1gMnSc1
Farah	Farah	k1gMnSc1
</s>
<s>
Mexiko	Mexiko	k1gNnSc1
MexikoRaúl	MexikoRaúl	k1gMnSc1
Ramírez	Ramírez	k1gMnSc1
<g/>
1	#num#	k4
</s>
<s>
Československo	Československo	k1gNnSc1
ČeskoslovenskoTomáš	ČeskoslovenskoTomáš	k1gMnSc1
Šmíd	Šmíd	k1gMnSc1
</s>
<s>
Ekvádor	Ekvádor	k1gInSc1
EkvádorAndrés	EkvádorAndrésa	k1gFnPc2
Gómez	Gómeza	k1gFnPc2
</s>
<s>
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republikaPieter	republikaPieter	k1gInSc1
Aldrich	Aldrich	k1gInSc1
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
ŠvédskoDanie	ŠvédskoDanie	k1gFnSc2
Visser	Vissra	k1gFnPc2
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
AustrálieJohn	AustrálieJohn	k1gMnSc1
Fitzgerald	Fitzgerald	k1gMnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
KanadaGrant	KanadaGrant	k1gMnSc1
Connell	Connell	k1gMnSc1
</s>
<s>
Nizozemsko	Nizozemsko	k1gNnSc1
NizozemskoJacco	NizozemskoJacco	k1gMnSc1
Eltingh	Eltingh	k1gMnSc1
</s>
<s>
Indie	Indie	k1gFnSc1
IndieLeander	IndieLeandra	k1gFnPc2
Paes	Paesa	k1gFnPc2
</s>
<s>
Švédsko	Švédsko	k1gNnSc1
ŠvédskoJonas	ŠvédskoJonas	k1gMnSc1
Björkman	Björkman	k1gMnSc1
</s>
<s>
Bělorusko	Bělorusko	k1gNnSc1
BěloruskoMax	BěloruskoMax	k1gInSc1
Mirnyj	Mirnyj	k1gMnSc1
</s>
<s>
Kanada	Kanada	k1gFnSc1
KanadaDaniel	KanadaDaniel	k1gMnSc1
Nestor	Nestor	k1gMnSc1
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
SrbskoNenad	SrbskoNenad	k1gInSc1
Zimonjić	Zimonjić	k1gMnSc1
</s>
<s>
Francie	Francie	k1gFnSc1
FrancieNicolas	FrancieNicolas	k1gMnSc1
Mahut	Mahut	k1gMnSc1
</s>
<s>
Kolumbie	Kolumbie	k1gFnSc1
KolumbieJuan	KolumbieJuan	k1gMnSc1
Sebastián	Sebastián	k1gMnSc1
Cabal	Cabal	k1gMnSc1
</s>
<s>
Jedničky	jednička	k1gFnPc1
na	na	k7c6
konečném	konečný	k2eAgInSc6d1
žebříčku	žebříček	k1gInSc6
čtyřhry	čtyřhra	k1gFnSc2
</s>
<s>
roktenistatým	roktenistatý	k2eAgInSc7d1
</s>
<s>
1976	#num#	k4
Ramírez	Ramírez	k1gMnSc1
<g/>
,	,	kIx,
RaúlRaúl	RaúlRaúl	k1gMnSc1
Ramírez	Ramírez	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1977	#num#	k4
McMillan	McMillan	k1gMnSc1
<g/>
,	,	kIx,
FrewFrew	FrewFrew	k1gMnSc1
McMillan	McMillan	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1978	#num#	k4
McMillan	McMillan	k1gMnSc1
<g/>
,	,	kIx,
FrewFrew	FrewFrew	k1gMnSc1
McMillan	McMillan	k1gMnSc1
<g/>
§	§	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1979	#num#	k4
McEnroe	McEnroe	k1gNnSc1
<g/>
,	,	kIx,
JohnJohn	JohnJohn	k1gNnSc1
McEnroe	McEnroe	k1gFnPc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1980	#num#	k4
McEnroe	McEnroe	k1gNnSc1
<g/>
,	,	kIx,
JohnJohn	JohnJohn	k1gNnSc1
McEnroe	McEnroe	k1gFnPc2
<g/>
§	§	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1981	#num#	k4
McEnroe	McEnroe	k1gNnSc1
<g/>
,	,	kIx,
JohnJohn	JohnJohn	k1gNnSc1
McEnroe	McEnroe	k1gFnPc2
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1982	#num#	k4
McEnroe	McEnroe	k1gNnSc1
<g/>
,	,	kIx,
JohnJohn	JohnJohn	k1gNnSc1
McEnroe	McEnroe	k1gFnPc2
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1983	#num#	k4
McEnroe	McEnroe	k1gNnSc1
<g/>
,	,	kIx,
JohnJohn	JohnJohn	k1gNnSc1
McEnroe	McEnroe	k1gFnPc2
<g/>
§	§	k?
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
Peter	Petra	k1gFnPc2
Fleming	Fleming	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
John	John	k1gMnSc1
McEnroe	McEnro	k1gMnSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1984	#num#	k4
Šmíd	Šmíd	k1gMnSc1
<g/>
,	,	kIx,
TomášTomáš	TomášTomáš	k1gMnSc1
Šmíd	Šmíd	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
Mark	Mark	k1gMnSc1
Edmondson	Edmondson	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Stewart	Stewart	k1gMnSc1
<g/>
,	,	kIx,
SherwoodSherwood	SherwoodSherwood	k1gInSc1
Stewart	Stewart	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1985	#num#	k4
Seguso	Segusa	k1gFnSc5
<g/>
,	,	kIx,
RobertRobert	RobertRobert	k1gMnSc1
Seguso	Segusa	k1gFnSc5
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
Flach	Flach	k1gInSc1
<g/>
,	,	kIx,
KenKen	KenKen	k2eAgInSc1d1
Flach	Flach	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Seguso	Segusa	k1gFnSc5
<g/>
,	,	kIx,
RobertRobert	RobertRobert	k1gMnSc1
Seguso	Segusa	k1gFnSc5
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1986	#num#	k4
Gómez	Gómez	k1gInSc1
<g/>
,	,	kIx,
AndrésAndrés	AndrésAndrés	k1gInSc1
Gómez	Gómez	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
Gildemeister	Gildemeister	k1gInSc1
<g/>
,	,	kIx,
HansHans	HansHans	k1gInSc1
Gildemeister	Gildemeister	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Gómez	Gómez	k1gInSc1
<g/>
,	,	kIx,
AndrésAndrés	AndrésAndrés	k1gInSc1
Gómez	Gómez	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1987	#num#	k4
Seguso	Segusa	k1gFnSc5
<g/>
,	,	kIx,
RobertRobert	RobertRobert	k1gMnSc1
Seguso	Segusa	k1gFnSc5
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
Casal	Casal	k1gMnSc1
<g/>
,	,	kIx,
SergioSergio	SergioSergio	k1gMnSc1
Casal	Casal	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Sánchez	Sánchez	k1gMnSc1
<g/>
,	,	kIx,
EmilioEmilio	EmilioEmilio	k1gMnSc1
Sánchez	Sánchez	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1988	#num#	k4
Järryd	Järryd	k1gInSc1
<g/>
,	,	kIx,
AndersAnders	AndersAnders	k1gInSc1
Järryd	Järryd	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
Leach	Leach	k1gMnSc1
<g/>
,	,	kIx,
RobertRobert	RobertRobert	k1gMnSc1
Leach	Leach	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Pugh	Pugh	k1gMnSc1
<g/>
,	,	kIx,
JimJim	JimJim	k1gMnSc1
Pugh	Pugh	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1989	#num#	k4
Järryd	Järryd	k1gInSc1
<g/>
,	,	kIx,
AndersAnders	AndersAnders	k1gInSc1
Järryd	Järryd	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
Leach	Leach	k1gMnSc1
<g/>
,	,	kIx,
RobertRobert	RobertRobert	k1gMnSc1
Leach	Leach	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Pugh	Pugh	k1gMnSc1
<g/>
,	,	kIx,
JimJim	JimJim	k1gMnSc1
Pugh	Pugh	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1990	#num#	k4
Aldrich	Aldrich	k1gMnSc1
<g/>
,	,	kIx,
PieterPieter	PieterPieter	k1gMnSc1
Aldrich	Aldrich	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
Visser	Visser	k1gInSc1
<g/>
,	,	kIx,
DanieDanie	DanieDanie	k1gFnSc1
Visser	Visser	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
Aldrich	Aldrich	k1gMnSc1
<g/>
,	,	kIx,
PieterPieter	PieterPieter	k1gMnSc1
Aldrich	Aldrich	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Visser	Visser	k1gInSc1
<g/>
,	,	kIx,
DanieDanie	DanieDanie	k1gFnSc1
Visser	Visser	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1991	#num#	k4
Fitzgerald	Fitzgerald	k1gMnSc1
<g/>
,	,	kIx,
JohnJohn	JohnJohn	k1gMnSc1
Fitzgerald	Fitzgerald	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
Fitzgerald	Fitzgerald	k1gMnSc1
<g/>
,	,	kIx,
JohnJohn	JohnJohn	k1gMnSc1
Fitzgerald	Fitzgerald	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Järryd	Järryd	k1gInSc1
<g/>
,	,	kIx,
AndersAnders	AndersAnders	k1gInSc1
Järryd	Järryd	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1992	#num#	k4
Woodforde	Woodford	k1gInSc5
<g/>
,	,	kIx,
MarkMark	MarkMark	k1gInSc1
Woodforde	Woodford	k1gInSc5
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
Woodforde	Woodford	k1gMnSc5
<g/>
,	,	kIx,
MarkMark	MarkMark	k1gInSc1
Woodforde	Woodford	k1gInSc5
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Woodbridge	Woodbridge	k1gInSc1
<g/>
,	,	kIx,
ToddTodd	ToddTodd	k1gInSc1
Woodbridge	Woodbridge	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1993	#num#	k4
Connell	Connell	k1gMnSc1
<g/>
,	,	kIx,
GrantGrant	GrantGrant	k1gMnSc1
Connell	Connell	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
Connell	Connell	k1gMnSc1
<g/>
,	,	kIx,
GrantGrant	GrantGrant	k1gMnSc1
Connell	Connell	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Galbraith	Galbraith	k1gMnSc1
<g/>
,	,	kIx,
PatrickPatrick	PatrickPatrick	k1gMnSc1
Galbraith	Galbraith	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1994	#num#	k4
Haarhuis	Haarhuis	k1gInSc1
<g/>
,	,	kIx,
PaulPaul	PaulPaul	k1gInSc1
Haarhuis	Haarhuis	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
Eltingh	Eltingh	k1gMnSc1
<g/>
,	,	kIx,
JaccoJacco	JaccoJacco	k1gMnSc1
Eltingh	Eltingh	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Haarhuis	Haarhuis	k1gInSc1
<g/>
,	,	kIx,
PaulPaul	PaulPaul	k1gInSc1
Haarhuis	Haarhuis	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1995	#num#	k4
Woodbridge	Woodbridge	k1gNnSc1
<g/>
,	,	kIx,
ToddTodd	ToddTodd	k1gInSc1
Woodbridge	Woodbridge	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
Woodforde	Woodford	k1gMnSc5
<g/>
,	,	kIx,
MarkMark	MarkMark	k1gInSc1
Woodforde	Woodford	k1gInSc5
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Woodbridge	Woodbridge	k1gInSc1
<g/>
,	,	kIx,
ToddTodd	ToddTodd	k1gInSc1
Woodbridge	Woodbridge	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1996	#num#	k4
Woodbridge	Woodbridge	k1gNnSc1
<g/>
,	,	kIx,
ToddTodd	ToddTodd	k1gInSc1
Woodbridge	Woodbridge	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
Woodforde	Woodford	k1gMnSc5
<g/>
,	,	kIx,
MarkMark	MarkMark	k1gInSc1
Woodforde	Woodford	k1gInSc5
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
Woodforde	Woodford	k1gMnSc5
<g/>
,	,	kIx,
MarkMark	MarkMark	k1gInSc1
Woodforde	Woodford	k1gInSc5
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Woodbridge	Woodbridge	k1gInSc1
<g/>
,	,	kIx,
ToddTodd	ToddTodd	k1gInSc1
Woodbridge	Woodbridge	k1gInSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1997	#num#	k4
Woodbridge	Woodbridge	k1gNnSc1
<g/>
,	,	kIx,
ToddTodd	ToddTodd	k1gInSc1
Woodbridge	Woodbridge	k1gInSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
Woodforde	Woodford	k1gMnSc5
<g/>
,	,	kIx,
MarkMark	MarkMark	k1gInSc1
Woodforde	Woodford	k1gInSc5
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Woodbridge	Woodbridge	k1gInSc1
<g/>
,	,	kIx,
ToddTodd	ToddTodd	k1gInSc1
Woodbridge	Woodbridge	k1gInSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1998	#num#	k4
Eltingh	Eltingh	k1gMnSc1
<g/>
,	,	kIx,
JaccoJacco	JaccoJacco	k1gMnSc1
Eltingh	Eltingh	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
Eltingh	Eltingh	k1gMnSc1
<g/>
,	,	kIx,
JaccoJacco	JaccoJacco	k1gMnSc1
Eltingh	Eltingh	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Haarhuis	Haarhuis	k1gInSc1
<g/>
,	,	kIx,
PaulPaul	PaulPaul	k1gInSc1
Haarhuis	Haarhuis	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1999	#num#	k4
Paes	Paes	k1gInSc1
<g/>
,	,	kIx,
LeanderLeander	LeanderLeander	k1gInSc1
Paes	Paes	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
Bhúpatí	Bhúpatý	k2eAgMnPc1d1
<g/>
,	,	kIx,
MahešMaheš	MahešMaheš	k1gMnPc1
Bhúpatí	Bhúpatý	k2eAgMnPc1d1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Paes	Paes	k1gInSc1
<g/>
,	,	kIx,
LeanderLeander	LeanderLeander	k1gInSc1
Paes	Paes	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2000	#num#	k4
Woodforde	Woodford	k1gInSc5
<g/>
,	,	kIx,
MarkMark	MarkMark	k1gInSc1
Woodforde	Woodford	k1gInSc5
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
Woodforde	Woodford	k1gMnSc5
<g/>
,	,	kIx,
MarkMark	MarkMark	k1gInSc1
Woodforde	Woodford	k1gInSc5
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Woodbridge	Woodbridge	k1gInSc1
<g/>
,	,	kIx,
ToddTodd	ToddTodd	k1gInSc1
Woodbridge	Woodbridge	k1gInSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2001	#num#	k4
Björkman	Björkman	k1gMnSc1
<g/>
,	,	kIx,
JonasJonas	JonasJonas	k1gMnSc1
Björkman	Björkman	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
Björkman	Björkman	k1gMnSc1
<g/>
,	,	kIx,
JonasJonas	JonasJonas	k1gMnSc1
Björkman	Björkman	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Woodbridge	Woodbridge	k1gInSc1
<g/>
,	,	kIx,
ToddTodd	ToddTodd	k1gInSc1
Woodbridge	Woodbridge	k1gInSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2002	#num#	k4
Knowles	Knowles	k1gInSc1
<g/>
,	,	kIx,
MarkMark	MarkMark	k1gInSc1
Knowles	Knowles	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
Mark	Mark	k1gMnSc1
Knowles	Knowles	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Daniel	Daniel	k1gMnSc1
Nestor	Nestor	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2003	#num#	k4
Mirnyj	Mirnyj	k1gInSc1
<g/>
,	,	kIx,
MaxMax	MaxMax	k1gInSc1
Mirnyj	Mirnyj	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
Bob	Bob	k1gMnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Mike	Mike	k1gFnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2004	#num#	k4
Knowles	Knowles	k1gInSc1
<g/>
,	,	kIx,
MarkMark	MarkMark	k1gInSc1
Knowles	Knowles	k1gInSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
Daniel	Daniel	k1gMnSc1
Nestor	Nestor	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
Mark	Mark	k1gMnSc1
Knowles	Knowles	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Daniel	Daniel	k1gMnSc1
Nestor	Nestor	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2005	#num#	k4
Bob	Bob	k1gMnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
Mike	Mik	k1gInSc2
Bryan	Bryan	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
Bob	Bob	k1gMnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Mike	Mike	k1gFnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2006	#num#	k4
Bob	Bob	k1gMnSc1
Bryan	Bryan	k1gMnSc1
<g/>
§	§	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
Mike	Mik	k1gInSc2
Bryan	Bryan	k1gInSc1
<g/>
§	§	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
Bob	Bob	k1gMnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Mike	Mike	k1gFnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2007	#num#	k4
Bob	Bob	k1gMnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
Mike	Mik	k1gInSc2
Bryan	Bryan	k1gInSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
Bob	Bob	k1gMnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
<g/>
/	/	kIx~
Mike	Mike	k1gFnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2008	#num#	k4
Zimonjić	Zimonjić	k1gFnSc1
<g/>
,	,	kIx,
NenadNenad	NenadNenad	k1gInSc1
Zimonjić	Zimonjić	k1gFnSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
Daniel	Daniel	k1gMnSc1
Nestor	Nestor	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Nenad	Nenad	k1gInSc1
Zimonjić	Zimonjić	k1gFnSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2009	#num#	k4
Bob	Bob	k1gMnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
Mike	Mik	k1gInSc2
Bryan	Bryan	k1gInSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
Bob	Bob	k1gMnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
/	/	kIx~
Mike	Mike	k1gFnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2010	#num#	k4
Bob	Bob	k1gMnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
Mike	Mik	k1gInSc2
Bryan	Bryan	k1gInSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
Bob	Bob	k1gMnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Mike	Mike	k1gFnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2011	#num#	k4
Bob	Bob	k1gMnSc1
Bryan	Bryan	k1gMnSc1
<g/>
§	§	k?
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
Mike	Mik	k1gInSc2
Bryan	Bryan	k1gInSc1
<g/>
§	§	k?
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
Bob	Bob	k1gMnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Mike	Mike	k1gFnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2012	#num#	k4
Mike	Mike	k1gNnSc1
Bryan	Bryana	k1gFnPc2
(	(	kIx(
<g/>
7	#num#	k4
<g/>
)	)	kIx)
Bob	Bob	k1gMnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Mike	Mike	k1gFnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2013	#num#	k4
</s>
<s>
Bob	Bob	k1gMnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
)	)	kIx)
Bob	Bob	k1gMnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
9	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Mike	Mike	k1gFnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mike	Mike	k1gFnSc1
Bryan	Bryana	k1gFnPc2
<g/>
§	§	k?
(	(	kIx(
<g/>
8	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2014	#num#	k4
Bob	Bob	k1gMnSc1
Bryan	Bryan	k1gMnSc1
<g/>
§	§	k?
(	(	kIx(
<g/>
8	#num#	k4
<g/>
)	)	kIx)
Mike	Mik	k1gInSc2
Bryan	Bryan	k1gInSc1
<g/>
§	§	k?
(	(	kIx(
<g/>
9	#num#	k4
<g/>
)	)	kIx)
Bob	Bob	k1gMnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Mike	Mike	k1gFnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2015	#num#	k4
Marcelo	Marcela	k1gFnSc5
Melo	Melo	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
Jean-Julien	Jean-Julien	k2eAgInSc1d1
Rojer	Rojer	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Horia	Horia	k1gFnSc1
Tecău	Tecăus	k1gInSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2016	#num#	k4
Nicolas	Nicolas	k1gMnSc1
Mahut	Mahut	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
Jamie	Jamie	k1gFnSc2
Murray	Murraa	k1gFnSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Bruno	Bruno	k1gMnSc1
Soares	Soares	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2017	#num#	k4
Marcelo	Marcela	k1gFnSc5
Melo	Melo	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
Łukasz	Łukasz	k1gMnSc1
Kubot	Kubot	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Marcelo	Marcela	k1gFnSc5
Melo	Melo	k?
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2018	#num#	k4
Mike	Mike	k1gNnSc1
Bryan	Bryana	k1gFnPc2
(	(	kIx(
<g/>
10	#num#	k4
<g/>
)	)	kIx)
Oliver	Oliver	k1gMnSc1
Marach	Marach	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Mate	mást	k5eAaImIp3nS
Pavić	Pavić	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2019	#num#	k4
Juan	Juan	k1gMnSc1
Sebastián	Sebastián	k1gMnSc1
Cabal	Cabal	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
Robert	Robert	k1gMnSc1
Farah	Farah	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
Juan	Juan	k1gMnSc1
Sebastián	Sebastián	k1gMnSc1
Cabal	Cabal	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Robert	Robert	k1gMnSc1
Farah	Farah	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
2020	#num#	k4
Robert	Robert	k1gMnSc1
Farah	Farah	k1gMnSc1
<g/>
§	§	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
Mate	mást	k5eAaImIp3nS
Pavić	Pavić	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
/	/	kIx~
Bruno	Bruno	k1gMnSc1
Soares	Soares	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
§	§	k?
–	–	k?
na	na	k7c6
čele	čelo	k1gNnSc6
žebříčku	žebříček	k1gInSc2
všechny	všechen	k3xTgInPc4
týdny	týden	k1gInPc4
kalendářního	kalendářní	k2eAgInSc2d1
roku	rok	k1gInSc2
</s>
<s>
Hráči	hráč	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
stali	stát	k5eAaPmAgMnP
jedničkami	jednička	k1gFnPc7
bez	bez	k7c2
výhry	výhra	k1gFnSc2
na	na	k7c4
GS	GS	kA
čtyřhry	čtyřhra	k1gFnSc2
</s>
<s>
Tabulka	tabulka	k1gFnSc1
uvádí	uvádět	k5eAaImIp3nS
tenisty	tenista	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
nastoupili	nastoupit	k5eAaPmAgMnP
na	na	k7c6
pozici	pozice	k1gFnSc6
světové	světový	k2eAgFnSc2d1
jedničky	jednička	k1gFnSc2
bez	bez	k7c2
výhry	výhra	k1gFnSc2
na	na	k7c6
grandslamovém	grandslamový	k2eAgInSc6d1
turnaji	turnaj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Hráčdatum	Hráčdatum	k1gNnSc1
poprvé	poprvé	k6eAd1
jedničkouprvní	jedničkouprvní	k2eAgNnSc4d1
finále	finále	k1gNnSc4
na	na	k7c4
grandslamuprvní	grandslamuprvní	k2eAgInSc4d1
titul	titul	k1gInSc4
na	na	k7c6
grandslamu	grandslam	k1gInSc6
</s>
<s>
Stefan	Stefan	k1gMnSc1
Edberg	Edberg	k1gMnSc1
<g/>
9	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1986US	1986US	k4
Open	Open	k1gInSc1
1984	#num#	k4
<g/>
Australian	Australiany	k1gInPc2
Open	Open	k1gInSc1
1987	#num#	k4
</s>
<s>
David	David	k1gMnSc1
Pate	pat	k1gInSc5
<g/>
14	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1991	#num#	k4
<g/>
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
1991	#num#	k4
<g/>
Australian	Australiany	k1gInPc2
Open	Open	k1gInSc1
1991	#num#	k4
</s>
<s>
Kelly	Kell	k1gInPc1
Jones	Jones	k1gInSc1
<g/>
12	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1992	#num#	k4
<g/>
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
1992	#num#	k4
<g/>
žádný	žádný	k3yNgInSc4
</s>
<s>
Patrick	Patrick	k1gMnSc1
Galbraith	Galbraith	k1gMnSc1
<g/>
18	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1993	#num#	k4
<g/>
Wimbledon	Wimbledon	k1gInSc1
1993	#num#	k4
<g/>
žádný	žádný	k3yNgInSc4
</s>
<s>
Grant	grant	k1gInSc1
Connell	Connell	k1gInSc1
<g/>
15	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1993	#num#	k4
<g/>
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
1990	#num#	k4
<g/>
žádný	žádný	k3yNgInSc4
</s>
<s>
Byron	Byron	k1gInSc1
Black	Black	k1gInSc1
<g/>
14	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1994	#num#	k4
<g/>
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
1994	#num#	k4
<g/>
French	Frencha	k1gFnPc2
Open	Open	k1gNnSc1
1994	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
List	list	k1gInSc4
of	of	k?
ATP	atp	kA
number	number	k1gInSc4
1	#num#	k4
ranked	ranked	k1gMnSc1
singles	singles	k1gMnSc1
players	players	k6eAd1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
a	a	k8xC
List	list	k1gInSc1
of	of	k?
ATP	atp	kA
number	number	k1gInSc4
1	#num#	k4
ranked	ranked	k1gMnSc1
doubles	doubles	k1gMnSc1
players	players	k6eAd1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Rankings	Rankings	k1gInSc1
Explained	Explained	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
International	International	k1gFnSc1
Tennis	Tennis	k1gFnSc1
Federation	Federation	k1gInSc1
(	(	kIx(
<g/>
ITF	ITF	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
How	How	k1gMnSc1
It	It	k1gMnSc1
All	All	k1gMnSc1
Began	Began	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Association	Association	k1gInSc1
of	of	k?
Tennis	Tennis	k1gInSc1
Professionals	Professionals	k1gInSc1
(	(	kIx(
<g/>
ATP	atp	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Djokovic	Djokovice	k1gFnPc2
Wins	Wins	k1gInSc1
Eighth	Eighth	k1gMnSc1
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
Crown	Crown	k1gMnSc1
<g/>
,	,	kIx,
Returns	Returns	k1gInSc1
To	to	k9
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATP	atp	kA
Tour	Tour	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2020-02-02	2020-02-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Mektic	Mektice	k1gFnPc2
<g/>
/	/	kIx~
<g/>
Pavic	Pavic	k1gMnSc1
Make	Mak	k1gFnSc2
History	Histor	k1gInPc4
<g/>
,	,	kIx,
Storm	Storm	k1gInSc4
To	ten	k3xDgNnSc1
Miami	Miami	k1gNnSc1
Title	titla	k1gFnSc6
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATP	atp	kA
Tour	Tour	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2021-04-03	2021-04-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Mate	mást	k5eAaImIp3nS
Pavić	Pavić	k1gMnSc1
becomes	becomes	k1gMnSc1
world	world	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
No	no	k9
<g/>
.1	.1	k4
ranked	ranked	k1gMnSc1
doubles	doubles	k1gMnSc1
player	player	k1gMnSc1
|	|	kIx~
Croatia	Croatia	k1gFnSc1
Week	Weeka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Croatia	Croatia	k1gFnSc1
Week	Weeka	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-04-04	2021-04-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TIGNOR	TIGNOR	kA
<g/>
,	,	kIx,
Steve	Steve	k1gMnSc1
<g/>
.	.	kIx.
1973	#num#	k4
<g/>
:	:	kIx,
The	The	k1gMnPc2
men	men	k?
boycott	boycott	k2eAgInSc1d1
Wimbledon	Wimbledon	k1gInSc1
and	and	k?
shift	shift	k2eAgInSc1d1
power	power	k1gInSc1
to	ten	k3xDgNnSc1
the	the	k?
players	players	k1gInSc1
<g/>
.	.	kIx.
www.tennis.com	www.tennis.com	k1gInSc1
<g/>
.	.	kIx.
tennis	tennis	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
19	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
26	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
2016	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
BUDDELL	BUDDELL	kA
<g/>
,	,	kIx,
James	James	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Rankings	Rankingsa	k1gFnPc2
That	That	k2eAgInSc1d1
Changed	Changed	k1gInSc1
Tennis	Tennis	k1gFnSc1
(	(	kIx(
<g/>
Part	part	k1gInSc1
I	I	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
atpworldtour	atpworldtour	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
23-08-2013	23-08-2013	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
BUDDELL	BUDDELL	kA
<g/>
,	,	kIx,
James	James	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Rankings	Rankingsa	k1gFnPc2
That	That	k2eAgInSc1d1
Changed	Changed	k1gInSc1
Tennis	Tennis	k1gFnSc1
(	(	kIx(
<g/>
Part	part	k1gInSc1
II	II	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
atpworldtour	atpworldtour	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
23-08-2013	23-08-2013	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
TIGNOR	TIGNOR	kA
<g/>
,	,	kIx,
Steve	Steve	k1gMnSc1
<g/>
.	.	kIx.
1973	#num#	k4
<g/>
:	:	kIx,
The	The	k1gFnSc6
ATP	atp	kA
institutes	institutes	k1gInSc1
computer	computer	k1gInSc1
rankings	rankings	k1gInSc1
<g/>
.	.	kIx.
www.tennis.com	www.tennis.com	k1gInSc1
<g/>
.	.	kIx.
tennis	tennis	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
26	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
26	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
2016	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
Coronavirus	Coronavirus	k1gMnSc1
<g/>
:	:	kIx,
ATP	atp	kA
Tour	Tour	k1gMnSc1
off	off	k?
until	until	k1gMnSc1
August	August	k1gMnSc1
<g/>
,	,	kIx,
WTA	WTA	kA
extends	extends	k1gInSc1
suspension	suspension	k1gInSc1
<g/>
.	.	kIx.
uk	uk	k?
<g/>
.	.	kIx.
<g/>
sports	sports	k1gInSc1
<g/>
.	.	kIx.
<g/>
yahoo	yahoo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-05-15	2020-05-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ATP	atp	kA
Rankings	Rankings	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Association	Association	k1gInSc1
of	of	k?
Tennis	Tennis	k1gInSc1
Professionals	Professionals	k1gInSc1
<g/>
(	(	kIx(
<g/>
ATP	atp	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ITF	ITF	kA
Tennis	Tennis	k1gFnSc2
–	–	k?
How	How	k1gFnSc2
the	the	k?
Rankings	Rankings	k1gInSc1
Work	Work	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
International	International	k1gFnSc1
Tennis	Tennis	k1gFnSc1
Federation	Federation	k1gInSc1
<g/>
,	,	kIx,
August	August	k1gMnSc1
27	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Pete	Pete	k1gInSc1
Sampras	Sampras	k1gMnSc1
–	–	k?
Career	Career	k1gInSc1
Highlights	Highlights	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Association	Association	k1gInSc1
of	of	k?
Tennis	Tennis	k1gInSc1
Professionals	Professionals	k1gInSc1
(	(	kIx(
<g/>
ATP	atp	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Plus	plus	k1gInSc1
<g/>
:	:	kIx,
Tennis	Tennis	k1gFnSc1
—	—	k?
ATP	atp	kA
Tour	Tour	k1gMnSc1
World	World	k1gMnSc1
Championship	Championship	k1gMnSc1
<g/>
;	;	kIx,
Sampras	Sampras	k1gMnSc1
Is	Is	k1gMnSc1
Assured	Assured	k1gMnSc1
Of	Of	k1gMnSc1
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
Ranking	Ranking	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
,	,	kIx,
November	November	k1gInSc1
27	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
BERKOK	BERKOK	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Djokovic	Djokovice	k1gFnPc2
begins	begins	k6eAd1
311	#num#	k4
<g/>
th	th	k?
career	career	k1gMnSc1
week	week	k1gMnSc1
at	at	k?
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
breaking	breaking	k1gInSc1
Federer	Federer	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
record	record	k6eAd1
<g/>
.	.	kIx.
www.tennis.com	www.tennis.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gFnPc2
<g/>
,	,	kIx,
2021-03-08	2021-03-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
History	Histor	k1gInPc1
of	of	k?
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Association	Association	k1gInSc1
of	of	k?
Tennis	Tennis	k1gInSc1
Professionals	Professionals	k1gInSc1
(	(	kIx(
<g/>
ATP	atp	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
July	Jula	k1gFnSc2
12	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
FINN	FINN	kA
<g/>
,	,	kIx,
Robin	robin	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gInSc1
<g/>
;	;	kIx,
Rios	Rios	k1gInSc1
Dismantles	Dismantles	k1gMnSc1
Agassi	Agass	k1gMnSc3
and	and	k?
Seizes	Seizes	k1gMnSc1
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
Ranking	Ranking	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
,	,	kIx,
March	March	k1gInSc1
30	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Worthy	Wortha	k1gFnSc2
of	of	k?
really	realla	k1gFnSc2
high	high	k1gMnSc1
fives	fives	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CNN	CNN	kA
<g/>
,	,	kIx,
June	jun	k1gMnSc5
18	#num#	k4
<g/>
,	,	kIx,
1984	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
WILSTEIN	WILSTEIN	kA
<g/>
,	,	kIx,
Steve	Steve	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korda	Korda	k1gMnSc1
takes	takes	k1gMnSc1
Australian	Australian	k1gMnSc1
Open	Open	k1gInSc1
title	titla	k1gFnSc3
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
February	Februara	k1gFnSc2
1	#num#	k4
<g/>
,	,	kIx,
1998	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Player	Player	k1gMnSc1
biography	biographa	k1gFnSc2
–	–	k?
Marcelo	Marcela	k1gFnSc5
Ríos	Ríos	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
International	International	k1gFnSc1
Tennis	Tennis	k1gFnSc1
Federation	Federation	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lleyton	Lleyton	k1gInSc1
Hewitt	Hewitt	k1gInSc1
–	–	k?
Career	Career	k1gInSc1
Highlights	Highlights	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Association	Association	k1gInSc1
of	of	k?
Tennis	Tennis	k1gInSc1
Professionals	Professionals	k1gInSc1
(	(	kIx(
<g/>
ATP	atp	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Lleyton	Lleyton	k1gInSc1
Hewitt	Hewitt	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
ATP	atp	kA
Rankings	Rankings	k1gInSc1
Movers	Movers	k1gInSc1
<g/>
:	:	kIx,
Roger	Roger	k1gInSc1
Returns	Returnsa	k1gFnPc2
To	to	k9
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATP	atp	kA
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
18-06-2018	18-06-2018	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Luboš	Luboš	k1gMnSc1
Zabloudil	Zabloudil	k1gMnSc1
<g/>
,	,	kIx,
TenisPortal	TenisPortal	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světová	světový	k2eAgFnSc1d1
jednička	jednička	k1gFnSc1
Federer	Federra	k1gFnPc2
je	být	k5eAaImIp3nS
poprvé	poprvé	k6eAd1
nejstarším	starý	k2eAgMnSc7d3
hráčem	hráč	k1gMnSc7
v	v	k7c6
Top	topit	k5eAaImRp2nS
100	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
TenisPortal	TenisPortal	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2018-06-19	2018-06-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Petr	Petr	k1gMnSc1
Pokorný	Pokorný	k1gMnSc1
<g/>
,	,	kIx,
TenisPortal	TenisPortal	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Federer	Federer	k1gInSc1
v	v	k7c6
Rotterdamu	Rotterdam	k1gInSc6
přehrál	přehrát	k5eAaPmAgInS
Dimitrova	Dimitrův	k2eAgMnSc4d1
a	a	k8xC
slaví	slavit	k5eAaImIp3nS
97	#num#	k4
<g/>
.	.	kIx.
titul	titul	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
TenisPortal	TenisPortal	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2018-02-18	2018-02-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
James	James	k1gMnSc1
Buddell	Buddell	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Andy	Anda	k1gFnPc4
Murray	Murraa	k1gFnSc2
Rises	Risesa	k1gFnPc2
To	to	k9
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATP	atp	kA
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2016-11-05	2016-11-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Mike	Mike	k1gInSc1
Bryan	Bryan	k1gMnSc1
Clinches	Clinches	k1gMnSc1
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
By	by	k9
Reaching	Reaching	k1gInSc1
Wimbledon	Wimbledon	k1gInSc1
SF	SF	kA
With	With	k1gInSc1
Sock	Sock	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATP	atp	kA
Tour	Tour	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2018-07-10	2018-07-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Rankings	Rankings	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATP	atp	kA
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ITF	ITF	kA
confirms	confirms	k6eAd1
no	no	k9
ATP	atp	kA
points	points	k1gInSc1
will	wilnout	k5eAaPmAgMnS
be	be	k?
assigned	assigned	k1gMnSc1
at	at	k?
Olympic	Olympic	k1gMnSc1
Games	Games	k1gMnSc1
in	in	k?
Rio	Rio	k1gMnSc1
2016	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gInSc1
World	World	k1gInSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
13	#num#	k4
14	#num#	k4
Djokovic	Djokovice	k1gFnPc2
Becomes	Becomesa	k1gFnPc2
25	#num#	k4
<g/>
th	th	k?
Player	Player	k1gMnSc1
In	In	k1gMnSc1
History	Histor	k1gMnPc4
To	ten	k3xDgNnSc1
Rise	Rise	k1gNnSc1
To	to	k9
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATP	atp	kA
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
<g/>
,	,	kIx,
4-7-2011	4-7-2011	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
Djokovic	Djokovice	k1gFnPc2
Clinches	Clinches	k1gMnSc1
Year-End	Year-End	k1gMnSc1
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
In	In	k1gFnSc1
FedEx	FedEx	k1gInSc1
ATP	atp	kA
Rankings	Rankings	k1gInSc1
<g/>
;	;	kIx,
Equals	Equals	k1gInSc1
Sampras	Sampras	k1gMnSc1
<g/>
'	'	kIx"
Record	Record	k1gMnSc1
Of	Of	k1gMnSc1
Six	Six	k1gMnSc1
Year-End	Year-End	k1gMnSc1
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
Finishes	Finishes	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATP	atp	kA
Tour	Tour	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2020-11-06	2020-11-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
The	The	k1gMnPc2
Associated	Associated	k1gInSc4
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadal	nadat	k5eAaPmAgMnS
Ends	Ends	k1gInSc4
His	his	k1gNnSc2
Season	Seasona	k1gFnPc2
<g/>
,	,	kIx,
Handing	Handing	k1gInSc4
Djokovic	Djokovice	k1gFnPc2
the	the	k?
Year-End	Year-End	k1gInSc1
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
Ranking	Ranking	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
,	,	kIx,
2018-11-05	2018-11-05	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Nadal	nadat	k5eAaPmAgMnS
Clinches	Clinches	k1gMnSc1
Year-End	Year-End	k1gMnSc1
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
ATP	atp	kA
Ranking	Ranking	k1gInSc1
For	forum	k1gNnPc2
Fifth	Fifth	k1gMnSc1
Time	Time	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATP	atp	kA
Tour	Tour	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2019-11-14	2019-11-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Nastase	Nastasa	k1gFnSc6
<g/>
,	,	kIx,
Vilas	Vilas	k1gMnSc1
<g/>
,	,	kIx,
Cooper	Cooper	k1gMnSc1
to	ten	k3xDgNnSc4
enter	enter	k1gMnSc1
Tennis	Tennis	k1gFnSc2
Hall	Hall	k1gMnSc1
of	of	k?
Fame	Fame	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Gainesville	Gainesville	k1gFnSc2
Sun	Sun	kA
<g/>
,	,	kIx,
March	March	k1gInSc1
27	#num#	k4
<g/>
,	,	kIx,
1991	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
2	#num#	k4
<g/>
C.	C.	kA
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
Jimmy	Jimma	k1gFnPc1
Connors	Connors	k1gInSc1
–	–	k?
Career	Career	k1gInSc1
Highlights	Highlights	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Association	Association	k1gInSc1
of	of	k?
Tennis	Tennis	k1gInSc1
Professionals	Professionals	k1gInSc1
(	(	kIx(
<g/>
ATP	atp	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
CROUSE	CROUSE	kA
<g/>
,	,	kIx,
Karen	Karen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Federer	Federer	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Reign	Reign	k1gMnSc1
Goes	Goes	k1gInSc1
on	on	k3xPp3gMnSc1
and	and	k?
History	Histor	k1gInPc1
Follows	Follows	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
,	,	kIx,
February	Februar	k1gInPc1
27	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
Hewitt	Hewitt	k1gMnSc1
jubilant	jubilant	k1gMnSc1
as	as	k9
world	world	k6eAd1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Age	Age	k1gFnSc1
<g/>
,	,	kIx,
November	November	k1gInSc1
15	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
ROBSON	ROBSON	kA
<g/>
,	,	kIx,
Douglas	Douglas	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Federer	Federer	k1gMnSc1
may	may	k?
reclaim	reclaim	k6eAd1
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
rank	rank	k1gInSc1
<g/>
,	,	kIx,
but	but	k?
can	can	k?
he	he	k0
keep	keep	k1gInSc1
hold	hold	k1gInSc4
of	of	k?
spot	spot	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
USA	USA	kA
Today	Todaa	k1gFnSc2
<g/>
,	,	kIx,
June	jun	k1gMnSc5
29	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
FORD	ford	k1gInSc1
<g/>
,	,	kIx,
Bonnie	Bonnie	k1gFnSc1
D.	D.	kA
Tennis	Tennis	k1gFnPc1
still	still	k1gMnSc1
imbued	imbued	k1gMnSc1
in	in	k?
Lendl	Lendl	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
blood	blooda	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ESPN	ESPN	kA
Internet	Internet	k1gInSc1
Ventures	Ventures	k1gInSc1
<g/>
,	,	kIx,
April	April	k1gInSc1
9	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
Tennis	Tennis	k1gFnPc2
<g/>
;	;	kIx,
Winning	Winning	k1gInSc1
Courier	Courier	k1gInSc1
Stays	Stays	k1gInSc4
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
,	,	kIx,
November	November	k1gInSc1
21	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
CLAREY	CLAREY	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tennis	Tennis	k1gInSc1
<g/>
;	;	kIx,
A	a	k9
Victorious	Victorious	k1gInSc1
Kuerten	Kuerten	k2eAgInSc1d1
Clinches	Clinches	k1gInSc1
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc1
York	York	k1gInSc1
Times	Times	k1gInSc1
<g/>
,	,	kIx,
December	December	k1gInSc1
4	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Courier	Courier	k1gMnSc1
finishes	finishes	k1gMnSc1
year	year	k1gMnSc1
ranked	ranked	k1gMnSc1
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
McNeil	McNeila	k1gFnPc2
posts	posts	k6eAd1
another	anothra	k1gFnPc2
upset	upset	k1gInSc4
in	in	k?
Slims	Slims	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NewsBank	NewsBank	k1gInSc1
<g/>
,	,	kIx,
November	November	k1gInSc1
21	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
Pete	Pete	k1gFnPc2
Sampras	Sampras	k1gMnSc1
–	–	k?
Career	Career	k1gInSc1
Highlights	Highlights	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Association	Association	k1gInSc1
of	of	k?
Tennis	Tennis	k1gInSc1
Professionals	Professionals	k1gInSc1
(	(	kIx(
<g/>
ATP	atp	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
WILANSKY	WILANSKY	kA
<g/>
,	,	kIx,
Matt	Matt	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inside	Insid	k1gInSc5
the	the	k?
numbers	numbers	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ESPN	ESPN	kA
Internet	Internet	k1gInSc1
Ventures	Ventures	k1gInSc1
<g/>
,	,	kIx,
August	August	k1gMnSc1
31	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
2000	#num#	k4
ATP	atp	kA
Tour	Tour	k1gMnSc1
Year	Year	k1gMnSc1
End	End	k1gMnSc1
Rankings	Rankings	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Association	Association	k1gInSc1
of	of	k?
Tennis	Tennis	k1gInSc1
Professionals	Professionals	k1gInSc1
(	(	kIx(
<g/>
ATP	atp	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
2001	#num#	k4
ATP	atp	kA
Tour	Tour	k1gMnSc1
Year	Year	k1gMnSc1
End	End	k1gMnSc1
Rankings	Rankings	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Association	Association	k1gInSc1
of	of	k?
Tennis	Tennis	k1gInSc1
Professionals	Professionals	k1gInSc1
(	(	kIx(
<g/>
ATP	atp	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
2002	#num#	k4
ATP	atp	kA
Tour	Tour	k1gMnSc1
Year	Year	k1gMnSc1
End	End	k1gMnSc1
Rankings	Rankings	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
atpworldtour	atpworldtour	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
2003	#num#	k4
ATP	atp	kA
Year	Yeara	k1gFnPc2
End	End	k1gFnSc1
Rankings	Rankings	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Association	Association	k1gInSc1
of	of	k?
Tennis	Tennis	k1gInSc1
Professionals	Professionals	k1gInSc1
(	(	kIx(
<g/>
ATP	atp	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
2004	#num#	k4
ATP	atp	kA
Year	Yeara	k1gFnPc2
End	End	k1gFnSc1
Rankings	Rankings	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Association	Association	k1gInSc1
of	of	k?
Tennis	Tennis	k1gInSc1
Professionals	Professionals	k1gInSc1
(	(	kIx(
<g/>
ATP	atp	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
2005	#num#	k4
ATP	atp	kA
Year	Yeara	k1gFnPc2
End	End	k1gFnSc1
Rankings	Rankings	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Association	Association	k1gInSc1
of	of	k?
Tennis	Tennis	k1gInSc1
Professionals	Professionals	k1gInSc1
(	(	kIx(
<g/>
ATP	atp	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
2006	#num#	k4
ATP	atp	kA
Year	Yeara	k1gFnPc2
End	End	k1gFnSc1
Rankings	Rankings	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Association	Association	k1gInSc1
of	of	k?
Tennis	Tennis	k1gInSc1
Professionals	Professionals	k1gInSc1
(	(	kIx(
<g/>
ATP	atp	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
2007	#num#	k4
ATP	atp	kA
Year	Yeara	k1gFnPc2
End	End	k1gFnSc1
Rankings	Rankings	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
atpworldtour	atpworldtour	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Federer	Federer	k1gMnSc1
will	will	k1gMnSc1
finish	finish	k1gMnSc1
year	year	k1gMnSc1
at	at	k?
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
after	after	k1gInSc1
winning	winning	k1gInSc4
home	hom	k1gFnSc2
tourney	tournea	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
October	October	k1gInSc1
28	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
2008	#num#	k4
ATP	atp	kA
Year	Yeara	k1gFnPc2
End	End	k1gFnSc1
Rankings	Rankings	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Association	Association	k1gInSc1
of	of	k?
Tennis	Tennis	k1gInSc1
Professionals	Professionals	k1gInSc1
(	(	kIx(
<g/>
ATP	atp	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
2009	#num#	k4
ATP	atp	kA
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
Year	Year	k1gMnSc1
End	End	k1gMnSc1
Rankings	Rankings	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Association	Association	k1gInSc1
of	of	k?
Tennis	Tennis	k1gInSc1
Professionals	Professionals	k1gInSc1
(	(	kIx(
<g/>
ATP	atp	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
2010	#num#	k4
ATP	atp	kA
World	World	k1gMnSc1
Tour	Tour	k1gMnSc1
Year	Year	k1gMnSc1
End	End	k1gMnSc1
Rankings	Rankings	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
atpworldtour	atpworldtour	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Nadal	nadat	k5eAaPmAgMnS
clinches	clinches	k1gMnSc1
year-end	year-end	k1gMnSc1
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
for	forum	k1gNnPc2
second	seconda	k1gFnPc2
time	time	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Association	Association	k1gInSc1
of	of	k?
Tennis	Tennis	k1gInSc1
Professionals	Professionals	k1gInSc1
(	(	kIx(
<g/>
ATP	atp	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
September	September	k1gInSc1
16	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Djokovic	Djokovice	k1gFnPc2
Clinches	Clinches	k1gMnSc1
Year-End	Year-End	k1gMnSc1
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
For	forum	k1gNnPc2
First	First	k1gFnSc1
Time	Time	k1gFnPc7
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Association	Association	k1gInSc1
of	of	k?
Tennis	Tennis	k1gInSc1
Professionals	Professionals	k1gInSc1
(	(	kIx(
<g/>
ATP	atp	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
October	October	k1gInSc1
13	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Djokovic	Djokovice	k1gFnPc2
to	ten	k3xDgNnSc4
finish	finish	k1gInSc1
no	no	k9
<g/>
.	.	kIx.
1	#num#	k4
in	in	k?
South	South	k1gMnSc1
African	African	k1gMnSc1
Airways	Airwaysa	k1gFnPc2
ATP	atp	kA
Rankings	Rankingsa	k1gFnPc2
for	forum	k1gNnPc2
2	#num#	k4
<g/>
nd	nd	k?
straight	straight	k1gMnSc1
year	year	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Association	Association	k1gInSc1
of	of	k?
Tennis	Tennis	k1gInSc1
Professionals	Professionals	k1gInSc1
(	(	kIx(
<g/>
ATP	atp	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
October	October	k1gInSc1
29	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Nadal	nadat	k5eAaPmAgMnS
Clinches	Clinches	k1gMnSc1
Year-End	Year-End	k1gMnSc1
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
Emirates	Emirates	k1gInSc1
ATP	atp	kA
Rankings	Rankings	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
November	November	k1gInSc1
6	#num#	k4
<g/>
,	,	kIx,
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
http://sports.espn.go.com/sports/tennis/dailyResults	http://sports.espn.go.com/sports/tennis/dailyResults	k1gInSc1
<g/>
↑	↑	k?
Djokovic	Djokovice	k1gFnPc2
clinches	clinches	k1gMnSc1
year-end	year-end	k1gMnSc1
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
ranking	ranking	k1gInSc1
at	at	k?
ATP	atp	kA
World	World	k1gInSc1
Tour	Tour	k1gInSc1
Finals	Finals	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
tennis	tennis	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
14	#num#	k4
November	November	k1gInSc1
2014	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Djokovic	Djokovice	k1gFnPc2
Clinches	Clinches	k1gMnSc1
Year-End	Year-End	k1gMnSc1
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
Emirates	Emirates	k1gInSc1
ATP	atp	kA
Ranking	Ranking	k1gInSc1
For	forum	k1gNnPc2
Fourth	Fourth	k1gMnSc1
Time	Time	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
September	September	k1gInSc1
14	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Fendrich	fendrich	k1gMnSc1
<g/>
,	,	kIx,
Howard	Howard	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novak	Novak	k1gMnSc1
Djokovic	Djokovice	k1gFnPc2
clinches	clinches	k1gMnSc1
tennis	tennis	k1gFnSc2
<g/>
'	'	kIx"
year-end	year-end	k1gInSc1
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
ranking	ranking	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CBS	CBS	kA
Sports	Sportsa	k1gFnPc2
<g/>
,	,	kIx,
14	#num#	k4
September	September	k1gInSc1
2015	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Murray	Murraa	k1gFnSc2
beats	beatsa	k1gFnPc2
Djokovic	Djokovice	k1gFnPc2
in	in	k?
London	London	k1gMnSc1
<g/>
,	,	kIx,
finishes	finishes	k1gMnSc1
as	as	k9
year-end	year-end	k1gMnSc1
no	no	k9
<g/>
.	.	kIx.
1	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
20	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Nadal	nadat	k5eAaPmAgMnS
Clinches	Clinches	k1gMnSc1
Year-End	Year-End	k1gMnSc1
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
Emirates	Emirates	k1gInSc1
ATP	atp	kA
Ranking	Ranking	k1gInSc1
For	forum	k1gNnPc2
Fourth	Fourth	k1gMnSc1
Time	Time	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Žebříček	žebříček	k1gInSc1
WTA	WTA	kA
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Aktuální	aktuální	k2eAgInSc1d1
žebříček	žebříček	k1gInSc1
dvouhry	dvouhra	k1gFnSc2
na	na	k7c6
ATPtennis	ATPtennis	k1gFnSc6
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
Aktuální	aktuální	k2eAgInSc1d1
žebříček	žebříček	k1gInSc1
čtyřhry	čtyřhra	k1gFnSc2
na	na	k7c6
ATPtennis	ATPtennis	k1gFnSc6
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Žebříček	žebříček	k1gInSc1
ATP	atp	kA
♦	♦	k?
20210510	#num#	k4
<g/>
a	a	k8xC
<g/>
10	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2021	#num#	k4
♦	♦	k?
Žebříček	žebříček	k1gInSc1
WTA	WTA	kA
</s>
<s>
mužská	mužský	k2eAgFnSc1d1
dvouhramužská	dvouhramužský	k2eAgFnSc1d1
čtyřhraženská	čtyřhraženský	k2eAgFnSc1d1
dvouhraženská	dvouhraženský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
</s>
<s>
▬	▬	k?
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
</s>
<s>
▲	▲	k?
Daniil	Daniil	k1gMnSc1
Medveděv	Medveděv	k1gMnSc1
</s>
<s>
▼	▼	k?
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
</s>
<s>
▬	▬	k?
Dominic	Dominice	k1gFnPc2
Thiem	Thiem	k1gInSc4
</s>
<s>
▬	▬	k?
Stefanos	Stefanos	k1gMnSc1
Tsitsipas	Tsitsipas	k1gMnSc1
</s>
<s>
▬	▬	k?
Alexander	Alexandra	k1gFnPc2
Zverev	Zverev	k1gFnSc1
</s>
<s>
▬	▬	k?
Andrej	Andrej	k1gMnSc1
Rubljov	Rubljov	k1gInSc1
</s>
<s>
▬	▬	k?
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
</s>
<s>
▲	▲	k?
Matteo	Matteo	k6eAd1
Berrettini	Berrettin	k2eAgMnPc5d1
</s>
<s>
▼	▼	k?
Diego	Diego	k1gMnSc1
Schwartzman	Schwartzman	k1gMnSc1
</s>
<s>
▬	▬	k?
Mate	mást	k5eAaImIp3nS
Pavić	Pavić	k1gFnSc1
</s>
<s>
▬	▬	k?
Robert	Robert	k1gMnSc1
Farah	Farah	k1gMnSc1
</s>
<s>
▬	▬	k?
Juan	Juan	k1gMnSc1
Sebastián	Sebastián	k1gMnSc1
Cabal	Cabal	k1gMnSc1
</s>
<s>
▬	▬	k?
Nikola	Nikola	k1gMnSc1
Mektić	Mektić	k1gMnSc1
</s>
<s>
▲	▲	k?
Horacio	Horacio	k1gMnSc1
Zeballos	Zeballos	k1gMnSc1
</s>
<s>
▲	▲	k?
Marcel	Marcel	k1gMnSc1
Granollers	Granollers	k1gInSc1
</s>
<s>
▼	▼	k?
Nicolas	Nicolas	k1gMnSc1
Mahut	Mahut	k1gMnSc1
</s>
<s>
▼	▼	k?
Bruno	Bruno	k1gMnSc1
Soares	Soares	k1gMnSc1
</s>
<s>
▬	▬	k?
Ivan	Ivan	k1gMnSc1
Dodig	Dodig	k1gMnSc1
</s>
<s>
▼	▼	k?
Filip	Filip	k1gMnSc1
Polášek	Polášek	k1gMnSc1
</s>
<s>
▬	▬	k?
Ashleigh	Ashleigh	k1gInSc1
Bartyová	Bartyová	k1gFnSc1
</s>
<s>
▬	▬	k?
Naomi	Nao	k1gFnPc7
Ósakaová	Ósakaový	k2eAgNnPc4d1
</s>
<s>
▬	▬	k?
Simona	Simona	k1gFnSc1
Halepová	Halepová	k1gFnSc1
</s>
<s>
▲	▲	k?
Aryna	Aryna	k1gFnSc1
Sabalenková	Sabalenkový	k2eAgFnSc1d1
</s>
<s>
▼	▼	k?
Sofia	Sofia	k1gFnSc1
Keninová	Keninová	k1gFnSc1
</s>
<s>
▼	▼	k?
Elina	Elina	k1gFnSc1
Svitolinová	Svitolinový	k2eAgFnSc1d1
</s>
<s>
▼	▼	k?
Bianca	Bianc	k2eAgFnSc1d1
Andreescuová	Andreescuová	k1gFnSc1
</s>
<s>
▬	▬	k?
Serena	Seren	k2eAgFnSc1d1
Williamsová	Williamsová	k1gFnSc1
</s>
<s>
▬	▬	k?
Karolína	Karolína	k1gFnSc1
Plíšková	plíškový	k2eAgFnSc1d1
</s>
<s>
▲	▲	k?
Petra	Petra	k1gFnSc1
Kvitová	Kvitová	k1gFnSc1
</s>
<s>
▲	▲	k?
Elise	elise	k1gFnSc1
Mertensová	Mertensová	k1gFnSc1
</s>
<s>
▼	▼	k?
Sie	Sie	k1gFnSc1
Su-wej	Su-wej	k1gInSc1
</s>
<s>
▲	▲	k?
Aryna	Aryna	k1gFnSc1
Sabalenková	Sabalenkový	k2eAgFnSc1d1
</s>
<s>
▬	▬	k?
Barbora	Barbora	k1gFnSc1
Strýcová	Strýcová	k1gFnSc1
</s>
<s>
▬	▬	k?
Tímea	Tíme	k2eAgFnSc1d1
Babosová	Babosová	k1gFnSc1
</s>
<s>
▬	▬	k?
Kristina	Kristina	k1gFnSc1
Mladenovicová	Mladenovicový	k2eAgFnSc1d1
</s>
<s>
▬	▬	k?
Barbora	Barbora	k1gFnSc1
Krejčíková	Krejčíková	k1gFnSc1
</s>
<s>
▬	▬	k?
Kateřina	Kateřina	k1gFnSc1
Siniaková	Siniakový	k2eAgFnSc1d1
</s>
<s>
▲	▲	k?
Demi	Demi	k1gNnSc1
Schuursová	Schuursová	k1gFnSc1
</s>
<s>
▬	▬	k?
Nicole	Nicole	k1gFnSc1
Melicharová	Melicharová	k1gFnSc1
</s>
<s>
ATP	atp	kA
–	–	k?
Tenisté	tenista	k1gMnPc1
na	na	k7c6
prvním	první	k4xOgInSc6
místě	místo	k1gNnSc6
světového	světový	k2eAgInSc2d1
žebříčku	žebříček	k1gInSc2
</s>
<s>
Ilie	Ilie	k6eAd1
Năstase	Năstasa	k1gFnSc3
(	(	kIx(
<g/>
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
1974	#num#	k4
–	–	k?
40	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
John	John	k1gMnSc1
Newcombe	Newcomb	k1gInSc5
(	(	kIx(
<g/>
1974	#num#	k4
–	–	k?
8	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Jimmy	Jimm	k1gInPc1
Connors	Connors	k1gInSc1
(	(	kIx(
<g/>
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
1983	#num#	k4
–	–	k?
268	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Björn	Björn	k1gMnSc1
Borg	Borg	k1gMnSc1
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
1981	#num#	k4
–	–	k?
<g />
.	.	kIx.
</s>
<s hack="1">
109	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
John	John	k1gMnSc1
McEnroe	McEnro	k1gMnSc2
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
1985	#num#	k4
–	–	k?
170	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Ivan	Ivan	k1gMnSc1
Lendl	Lendl	k1gMnSc1
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
–	–	k?
270	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Mats	Mats	k1gInSc1
Wilander	Wilander	k1gInSc1
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
1989	#num#	k4
–	–	k?
20	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
;	;	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
–	–	k?
•	•	k?
Stefan	Stefan	k1gMnSc1
Edberg	Edberg	k1gMnSc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
–	–	k?
72	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Boris	Boris	k1gMnSc1
Becker	Becker	k1gMnSc1
(	(	kIx(
<g/>
1991	#num#	k4
–	–	k?
12	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Jim	on	k3xPp3gMnPc3
Courier	Courier	k1gMnSc1
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
1993	#num#	k4
–	–	k?
58	#num#	k4
t.	t.	k?
•	•	k?
Pete	Pete	k1gInSc1
Sampras	Sampras	k1gMnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
2000	#num#	k4
–	–	k?
286	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Andre	Andr	k1gMnSc5
Agassi	Agass	k1gMnSc5
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
2003	#num#	k4
–	–	k?
101	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Thomas	Thomas	k1gMnSc1
Muster	Muster	k1gMnSc1
(	(	kIx(
<g/>
1996	#num#	k4
–	–	k?
6	#num#	k4
<g/>
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Marcelo	Marcela	k1gFnSc5
Ríos	Ríosa	k1gFnPc2
(	(	kIx(
<g/>
1998	#num#	k4
–	–	k?
6	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Carlos	Carlos	k1gMnSc1
Moyà	Moyà	k1gMnSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
–	–	k?
2	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Jevgenij	Jevgenij	k1gFnSc1
Kafelnikov	Kafelnikov	k1gInSc1
(	(	kIx(
<g/>
1999	#num#	k4
–	–	k?
6	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Patrick	Patrick	k1gMnSc1
Rafter	Rafter	k1gMnSc1
(	(	kIx(
<g/>
1999	#num#	k4
–	–	k?
1	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Marat	Marat	k1gInSc1
Safin	Safin	k1gInSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
–	–	k?
9	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Gustavo	Gustava	k1gFnSc5
Kuerten	Kuertno	k1gNnPc2
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
2001	#num#	k4
–	–	k?
43	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Lleyton	Lleyton	k1gInSc1
Hewitt	Hewitt	k1gInSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
2003	#num#	k4
–	–	k?
80	#num#	k4
t	t	k?
•	•	k?
Juan	Juan	k1gMnSc1
Carlos	Carlos	k1gMnSc1
Ferrero	Ferrero	k1gNnSc4
(	(	kIx(
<g/>
2003	#num#	k4
–	–	k?
8	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Andy	Anda	k1gFnSc2
Roddick	Roddick	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
–	–	k?
13	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Roger	Roger	k1gMnSc1
Federer	Federer	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
310	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Rafael	Rafael	k1gMnSc1
Nadal	nadat	k5eAaPmAgMnS
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
–	–	k?
209	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Novak	Novak	k1gMnSc1
Djoković	Djoković	k1gMnSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
2021	#num#	k4
–	–	k?
320	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Andy	Anda	k1gFnSc2
Murray	Murraa	k1gFnSc2
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
–	–	k?
41	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
Žebříček	žebříček	k1gInSc1
ATP	atp	kA
byl	být	k5eAaImAgInS
zaveden	zavést	k5eAaPmNgInS
23	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1973	#num#	k4
•	•	k?
(	(	kIx(
<g/>
rok	rok	k1gInSc1
prvně	prvně	k?
<g/>
/	/	kIx~
<g/>
naposledy	naposledy	k6eAd1
umístěn	umístit	k5eAaPmNgInS
–	–	k?
počet	počet	k1gInSc1
týdnů	týden	k1gInPc2
(	(	kIx(
<g/>
t.	t.	k?
<g/>
))	))	k?
•	•	k?
současná	současný	k2eAgFnSc1d1
světová	světový	k2eAgFnSc1d1
jednička	jednička	k1gFnSc1
tučně	tučně	k6eAd1
<g/>
,	,	kIx,
stav	stav	k1gInSc1
k	k	k7c3
10	#num#	k4
<g/>
.	.	kIx.
květnu	květen	k1gInSc3
2021	#num#	k4
</s>
<s>
ATP	atp	kA
–	–	k?
Tenisté	tenista	k1gMnPc1
na	na	k7c6
prvním	první	k4xOgInSc6
místě	místo	k1gNnSc6
světového	světový	k2eAgInSc2d1
žebříčku	žebříček	k1gInSc2
ve	v	k7c6
čtyřhře	čtyřhra	k1gFnSc6
</s>
<s>
Bob	Bob	k1gMnSc1
Hewitt	Hewitt	k1gMnSc1
(	(	kIx(
<g/>
1976	#num#	k4
–	–	k?
6	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Raúl	Raúl	k1gMnSc1
Ramírez	Ramírez	k1gMnSc1
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
1977	#num#	k4
–	–	k?
62	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Frew	Frew	k1gMnSc1
McMillan	McMillan	k1gMnSc1
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
1979	#num#	k4
–	–	k?
85	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Tom	Tom	k1gMnSc1
Okker	Okker	k1gMnSc1
(	(	kIx(
<g/>
1979	#num#	k4
–	–	k?
11	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
John	John	k1gMnSc1
McEnroe	McEnro	k1gMnSc2
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
1989	#num#	k4
–	–	k?
269	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Stan	stan	k1gInSc1
Smith	Smith	k1gMnSc1
(	(	kIx(
<g/>
1981	#num#	k4
–	–	k?
8	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Paul	Paul	k1gMnSc1
McNamee	McName	k1gMnSc2
(	(	kIx(
<g/>
1981	#num#	k4
–	–	k?
3	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Peter	Peter	k1gMnSc1
Fleming	Fleming	k1gInSc1
(	(	kIx(
<g/>
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
1984	#num#	k4
–	–	k?
17	#num#	k4
t.	t.	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Tomáš	Tomáš	k1gMnSc1
Šmíd	Šmíd	k1gMnSc1
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
1985	#num#	k4
–	–	k?
34	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Anders	Anders	k1gInSc1
Järryd	Järryd	k1gInSc1
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
–	–	k?
107	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Robert	Robert	k1gMnSc1
Seguso	Segusa	k1gFnSc5
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
1988	#num#	k4
–	–	k?
62	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Ken	Ken	k1gMnSc1
Flach	Flach	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
1986	#num#	k4
–	–	k?
5	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Stefan	Stefan	k1gMnSc1
Edberg	Edberg	k1gMnSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
1987	#num#	k4
–	–	k?
15	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Yannick	Yannick	k1gMnSc1
Noah	Noah	k1gMnSc1
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
1987	#num#	k4
–	–	k?
19	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Slobodan	Slobodan	k1gMnSc1
Živojinović	Živojinović	k1gMnSc1
(	(	kIx(
<g/>
1986	#num#	k4
–	–	k?
7	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Andrés	Andrés	k1gInSc1
Gómez	Gómez	k1gInSc1
(	(	kIx(
<g/>
1986	#num#	k4
–	–	k?
13	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Emilio	Emilio	k1gMnSc1
Sánchez	Sánchez	k1gMnSc1
(	(	kIx(
<g/>
1989	#num#	k4
–	–	k?
6	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Jim	on	k3xPp3gMnPc3
Grabb	Grabb	k1gMnSc1
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
1993	#num#	k4
–	–	k?
13	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Jim	on	k3xPp3gMnPc3
Pugh	Pugh	k1gMnSc1
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
–	–	k?
26	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Danie	Danie	k1gFnSc1
Visser	Visser	k1gMnSc1
(	(	kIx(
<g/>
1990	#num#	k4
–	–	k?
27	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Rick	Rick	k1gMnSc1
Leach	Leach	k1gMnSc1
(	(	kIx(
<g/>
1990	#num#	k4
–	–	k?
9	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Pieter	Pieter	k1gMnSc1
Aldrich	Aldrich	k1gMnSc1
(	(	kIx(
<g/>
1990	#num#	k4
–	–	k?
19	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
David	David	k1gMnSc1
Pate	pat	k1gInSc5
(	(	kIx(
<g/>
1991	#num#	k4
–	–	k?
25	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
John	John	k1gMnSc1
Fitzgerald	Fitzgerald	k1gMnSc1
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
–	–	k?
40	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Todd	Todd	k1gInSc1
Woodbridge	Woodbridge	k1gFnSc1
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
–	–	k?
204	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Kelly	Kella	k1gFnSc2
Jones	Jones	k1gMnSc1
(	(	kIx(
<g/>
1992	#num#	k4
–	–	k?
1	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Mark	Mark	k1gMnSc1
Woodforde	Woodford	k1gInSc5
(	(	kIx(
<g/>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
–	–	k?
83	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Richey	Richey	k1gInPc1
Reneberg	Reneberg	k1gInSc1
(	(	kIx(
<g/>
1993	#num#	k4
–	–	k?
5	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Patrick	Patrick	k1gMnSc1
Galbraith	Galbraith	k1gMnSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
1994	#num#	k4
–	–	k?
4	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Jonathan	Jonathan	k1gMnSc1
Stark	Stark	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
–	–	k?
6	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Grant	grant	k1gInSc1
Connell	Connell	k1gInSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1994	#num#	k4
–	–	k?
17	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Paul	Paul	k1gMnSc1
Haarhuis	Haarhuis	k1gFnSc2
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
1999	#num#	k4
–	–	k?
71	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Byron	Byron	k1gMnSc1
Black	Black	k1gMnSc1
(	(	kIx(
<g/>
1994	#num#	k4
–	–	k?
8	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Jacco	Jacco	k1gMnSc1
Eltingh	Eltingh	k1gMnSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
1998	#num#	k4
–	–	k?
63	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Maheš	Maheš	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Bhúpatí	Bhúpatý	k2eAgMnPc1d1
(	(	kIx(
<g/>
1999	#num#	k4
–	–	k?
4	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Leander	Leander	k1gInSc1
Paes	Paes	k1gInSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
–	–	k?
39	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Jared	Jared	k1gMnSc1
Palmer	Palmer	k1gMnSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
–	–	k?
39	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Alex	Alex	k1gMnSc1
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Brien	Brien	k1gInSc4
(	(	kIx(
<g/>
2000	#num#	k4
–	–	k?
<g />
.	.	kIx.
</s>
<s hack="1">
5	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Jonas	Jonas	k1gMnSc1
Björkman	Björkman	k1gMnSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
2005	#num#	k4
–	–	k?
74	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Donald	Donald	k1gMnSc1
Johnson	Johnson	k1gMnSc1
(	(	kIx(
<g/>
2002	#num#	k4
–	–	k?
20	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Mark	Mark	k1gMnSc1
Knowles	Knowles	k1gMnSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
2005	#num#	k4
–	–	k?
65	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Daniel	Daniel	k1gMnSc1
Nestor	Nestor	k1gMnSc1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
–	–	k?
108	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Max	Max	k1gMnSc1
Mirnyj	Mirnyj	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
–	–	k?
57	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Bob	Bob	k1gMnSc1
Bryan	Bryan	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
–	–	k?
439	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Mike	Mike	k1gInSc1
Bryan	Bryan	k1gInSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
–	–	k?
506	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Nenad	Nenad	k1gInSc1
Zimonjić	Zimonjić	k1gFnSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
–	–	k?
40	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Marcelo	Marcela	k1gFnSc5
Melo	Melo	k?
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
–	–	k?
56	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Jamie	Jamie	k1gFnSc1
Murray	Murraa	k1gFnSc2
(	(	kIx(
<g/>
2016	#num#	k4
–	–	k?
9	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Nicolas	Nicolas	k1gMnSc1
Mahut	Mahut	k1gMnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
–	–	k?
39	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Henri	Henri	k1gNnSc2
Kontinen	Kontinna	k1gFnPc2
(	(	kIx(
<g/>
2017	#num#	k4
–	–	k?
26	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Łukasz	Łukasz	k1gMnSc1
Kubot	Kubot	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
–	–	k?
19	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Mate	mást	k5eAaImIp3nS
Pavić	Pavić	k1gFnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
2021	#num#	k4
–	–	k?
14	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Juan	Juan	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Sebastián	Sebastián	k1gMnSc1
Cabal	Cabal	k1gMnSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2020	#num#	k4
–	–	k?
29	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
•	•	k?
Robert	Robert	k1gMnSc1
Farah	Farah	k1gMnSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
2021	#num#	k4
–	–	k?
68	#num#	k4
t.	t.	k?
<g/>
)	)	kIx)
Žebříček	žebříček	k1gInSc1
ATP	atp	kA
byl	být	k5eAaImAgInS
zaveden	zavést	k5eAaPmNgInS
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1976	#num#	k4
•	•	k?
(	(	kIx(
<g/>
rok	rok	k1gInSc1
prvně	prvně	k?
<g/>
/	/	kIx~
<g/>
naposledy	naposledy	k6eAd1
jedničkou	jednička	k1gFnSc7
–	–	k?
počet	počet	k1gInSc4
týdnů	týden	k1gInPc2
(	(	kIx(
<g/>
t.	t.	k?
<g/>
))	))	k?
•	•	k?
současná	současný	k2eAgFnSc1d1
světová	světový	k2eAgFnSc1d1
jednička	jednička	k1gFnSc1
tučně	tučně	k6eAd1
<g/>
,	,	kIx,
stav	stav	k1gInSc1
k	k	k7c3
10	#num#	k4
<g/>
.	.	kIx.
květnu	květen	k1gInSc3
2021	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Tenis	tenis	k1gInSc1
|	|	kIx~
Sport	sport	k1gInSc1
</s>
