<s>
Bahamské	bahamský	k2eAgInPc1d1	bahamský
ostrovy	ostrov	k1gInPc1	ostrov
leží	ležet	k5eAaImIp3nP	ležet
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Floridy	Florida	k1gFnSc2	Florida
a	a	k8xC	a
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
.	.	kIx.	.
</s>
<s>
Souostroví	souostroví	k1gNnSc1	souostroví
tvoří	tvořit	k5eAaImIp3nS	tvořit
30	[number]	k4	30
větších	veliký	k2eAgMnPc2d2	veliký
<g/>
,	,	kIx,	,
700	[number]	k4	700
menších	malý	k2eAgMnPc2d2	menší
(	(	kIx(	(
<g/>
nazývaných	nazývaný	k2eAgMnPc2d1	nazývaný
dle	dle	k7c2	dle
velikosti	velikost	k1gFnSc2	velikost
cays	cays	k6eAd1	cays
nebo	nebo	k8xC	nebo
keys	keys	k6eAd1	keys
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
pramení	pramenit	k5eAaImIp3nS	pramenit
také	také	k9	také
původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
Bahamského	bahamský	k2eAgNnSc2d1	Bahamské
souostroví	souostroví	k1gNnSc2	souostroví
-	-	kIx~	-
Lucayské	Lucayské	k2eAgNnSc2d1	Lucayské
souostroví	souostroví	k1gNnSc2	souostroví
<g/>
)	)	kIx)	)
a	a	k8xC	a
cca	cca	kA	cca
2400	[number]	k4	2400
korálových	korálový	k2eAgInPc2d1	korálový
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Atlantském	atlantský	k2eAgInSc6d1	atlantský
oceáně	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgNnSc7	první
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
Kryštof	Kryštof	k1gMnSc1	Kryštof
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
objevil	objevit	k5eAaPmAgInS	objevit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
právě	právě	k9	právě
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
ostrovů	ostrov	k1gInPc2	ostrov
Bahamského	bahamský	k2eAgNnSc2d1	Bahamské
souostroví	souostroví	k1gNnSc2	souostroví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1492	[number]	k4	1492
přistál	přistát	k5eAaImAgInS	přistát
na	na	k7c4	na
ostrovu	ostrov	k1gInSc3	ostrov
San	San	k1gMnSc1	San
Salvador	Salvador	k1gMnSc1	Salvador
a	a	k8xC	a
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
ho	on	k3xPp3gMnSc4	on
za	za	k7c4	za
španělskou	španělský	k2eAgFnSc4d1	španělská
državu	država	k1gFnSc4	država
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
původního	původní	k2eAgNnSc2d1	původní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
odvezli	odvézt	k5eAaPmAgMnP	odvézt
Španělé	Španěl	k1gMnPc1	Španěl
jako	jako	k9	jako
otroky	otrok	k1gMnPc4	otrok
do	do	k7c2	do
zlatých	zlatý	k2eAgInPc2d1	zlatý
dolů	dol	k1gInPc2	dol
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
a	a	k8xC	a
na	na	k7c4	na
Haiti	Haiti	k1gNnSc4	Haiti
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc4	zbytek
vymřel	vymřít	k5eAaPmAgMnS	vymřít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
začali	začít	k5eAaPmAgMnP	začít
usazovat	usazovat	k5eAaImF	usazovat
Britové	Brit	k1gMnPc1	Brit
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1717	[number]	k4	1717
se	se	k3xPyFc4	se
Bahamy	Bahamy	k1gFnPc1	Bahamy
staly	stát	k5eAaPmAgFnP	stát
britskou	britský	k2eAgFnSc7d1	britská
kolonií	kolonie	k1gFnSc7	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1964	[number]	k4	1964
byla	být	k5eAaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
Bahamám	Bahamy	k1gFnPc3	Bahamy
samospráva	samospráva	k1gFnSc1	samospráva
a	a	k8xC	a
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1973	[number]	k4	1973
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
nezávislost	nezávislost	k1gFnSc1	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
sem	sem	k6eAd1	sem
přicházejí	přicházet	k5eAaImIp3nP	přicházet
uprchlíci	uprchlík	k1gMnPc1	uprchlík
z	z	k7c2	z
Haiti	Haiti	k1gNnSc2	Haiti
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
uniknout	uniknout	k5eAaPmF	uniknout
bídě	bída	k1gFnSc3	bída
a	a	k8xC	a
politickým	politický	k2eAgInPc3d1	politický
nepokojům	nepokoj	k1gInPc3	nepokoj
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Bahamy	Bahamy	k1gFnPc1	Bahamy
se	se	k3xPyFc4	se
administrativně	administrativně	k6eAd1	administrativně
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
okruhy	okruh	k1gInPc4	okruh
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
to	ten	k3xDgNnSc1	ten
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
oblasti	oblast	k1gFnPc4	oblast
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
New	New	k1gFnSc2	New
Providencu	Providencus	k1gInSc2	Providencus
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gFnPc2	on
31	[number]	k4	31
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
23	[number]	k4	23
se	se	k3xPyFc4	se
jich	on	k3xPp3gInPc2	on
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc1	zbytek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Bahamy	Bahamy	k1gFnPc1	Bahamy
tvoří	tvořit	k5eAaImIp3nP	tvořit
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
řetěz	řetěz	k1gInSc4	řetěz
nízkých	nízký	k2eAgInPc2d1	nízký
vápencových	vápencový	k2eAgInPc2d1	vápencový
a	a	k8xC	a
korálových	korálový	k2eAgInPc2d1	korálový
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2000	[number]	k4	2000
drobných	drobný	k2eAgNnPc2d1	drobné
skalisek	skalisko	k1gNnPc2	skalisko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Bahamách	Bahamy	k1gFnPc6	Bahamy
nenajdeme	najít	k5eNaPmIp1nP	najít
pohoří	pohoří	k1gNnSc1	pohoří
ani	ani	k8xC	ani
strmá	strmý	k2eAgNnPc1d1	strmé
pobřeží	pobřeží	k1gNnPc1	pobřeží
-	-	kIx~	-
převažují	převažovat	k5eAaImIp3nP	převažovat
rozlehlé	rozlehlý	k2eAgFnPc4d1	rozlehlá
pláže	pláž	k1gFnPc4	pláž
<g/>
.	.	kIx.	.
</s>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
ostrovů	ostrov	k1gInPc2	ostrov
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
malá	malý	k2eAgFnSc1d1	malá
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
tropické	tropický	k2eAgNnSc1d1	tropické
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
severovýchodních	severovýchodní	k2eAgInPc2d1	severovýchodní
pasátů	pasát	k1gInPc2	pasát
a	a	k8xC	a
teplého	teplý	k2eAgInSc2d1	teplý
Golfského	golfský	k2eAgInSc2d1	golfský
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
teploty	teplota	k1gFnPc1	teplota
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
v	v	k7c6	v
nejteplejším	teplý	k2eAgInSc6d3	nejteplejší
srpnu	srpen	k1gInSc6	srpen
28	[number]	k4	28
<g/>
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
nejstudenějším	studený	k2eAgInSc6d3	nejstudenější
únoru	únor	k1gInSc6	únor
21	[number]	k4	21
°	°	k?	°
<g/>
C.	C.	kA	C.
Srážky	srážka	k1gFnSc2	srážka
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
mezi	mezi	k7c7	mezi
1100	[number]	k4	1100
až	až	k9	až
1600	[number]	k4	1600
mm	mm	kA	mm
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
konci	konec	k1gInSc6	konec
souostroví	souostroví	k1gNnSc2	souostroví
méně	málo	k6eAd2	málo
než	než	k8xS	než
1000	[number]	k4	1000
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc4d1	jediné
přírodní	přírodní	k2eAgNnSc4d1	přírodní
bohatství	bohatství	k1gNnSc4	bohatství
Baham	Bahamy	k1gFnPc2	Bahamy
představují	představovat	k5eAaImIp3nP	představovat
pláže	pláž	k1gFnPc1	pláž
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
každoročně	každoročně	k6eAd1	každoročně
stávají	stávat	k5eAaImIp3nP	stávat
místem	místem	k6eAd1	místem
radovánek	radovánka	k1gFnPc2	radovánka
několika	několik	k4yIc2	několik
milionů	milion	k4xCgInPc2	milion
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
z	z	k7c2	z
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
práci	práce	k1gFnSc4	práce
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvěma	dva	k4xCgInPc7	dva
třetinám	třetina	k1gFnPc3	třetina
výdělečně	výdělečně	k6eAd1	výdělečně
činného	činný	k2eAgNnSc2d1	činné
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Těží	těžet	k5eAaImIp3nS	těžet
se	se	k3xPyFc4	se
sůl	sůl	k1gFnSc1	sůl
a	a	k8xC	a
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
také	také	k9	také
rafinérie	rafinérie	k1gFnSc1	rafinérie
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
je	být	k5eAaImIp3nS	být
pěstování	pěstování	k1gNnSc1	pěstování
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
a	a	k8xC	a
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
ananasy	ananas	k1gInPc4	ananas
a	a	k8xC	a
banány	banán	k1gInPc4	banán
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
sídlem	sídlo	k1gNnSc7	sídlo
řady	řada	k1gFnSc2	řada
bank	banka	k1gFnPc2	banka
a	a	k8xC	a
peněžních	peněžní	k2eAgInPc2d1	peněžní
ústavů	ústav	k1gInPc2	ústav
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
využívají	využívat	k5eAaImIp3nP	využívat
zdejšího	zdejší	k2eAgInSc2d1	zdejší
výhodného	výhodný	k2eAgInSc2d1	výhodný
daňového	daňový	k2eAgInSc2d1	daňový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
a	a	k8xC	a
provozuje	provozovat	k5eAaImIp3nS	provozovat
velké	velký	k2eAgNnSc1d1	velké
obchodní	obchodní	k2eAgNnSc1d1	obchodní
loďstvo	loďstvo	k1gNnSc1	loďstvo
plující	plující	k2eAgNnSc1d1	plující
pod	pod	k7c7	pod
levnou	levný	k2eAgFnSc7d1	levná
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Nassau	Nassaus	k1gInSc2	Nassaus
má	mít	k5eAaImIp3nS	mít
výhodně	výhodně	k6eAd1	výhodně
položený	položený	k2eAgInSc4d1	položený
<g/>
,	,	kIx,	,
kdysi	kdysi	k6eAd1	kdysi
strategicky	strategicky	k6eAd1	strategicky
významný	významný	k2eAgInSc1d1	významný
přístav	přístav	k1gInSc1	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
odvětvím	odvětví	k1gNnSc7	odvětví
lov	lov	k1gInSc4	lov
mořských	mořský	k2eAgFnPc2d1	mořská
hub	houba	k1gFnPc2	houba
<g/>
.	.	kIx.	.
</s>
<s>
Černoši	černoch	k1gMnPc1	černoch
tvoří	tvořit	k5eAaImIp3nP	tvořit
85	[number]	k4	85
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
12	[number]	k4	12
%	%	kIx~	%
běloši	běloch	k1gMnPc1	běloch
a	a	k8xC	a
3	[number]	k4	3
%	%	kIx~	%
ostatní	ostatní	k2eAgMnPc4d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
překvapivě	překvapivě	k6eAd1	překvapivě
bližší	blízký	k2eAgFnSc6d2	bližší
britské	britský	k2eAgFnSc6d1	britská
angličtině	angličtina	k1gFnSc6	angličtina
než	než	k8xS	než
té	ten	k3xDgFnSc2	ten
americké	americký	k2eAgFnSc2d1	americká
a	a	k8xC	a
které	který	k3yQgNnSc1	který
rozumí	rozumět	k5eAaImIp3nS	rozumět
většina	většina	k1gFnSc1	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
ní	on	k3xPp3gFnSc2	on
se	se	k3xPyFc4	se
na	na	k7c6	na
Bahamách	Bahamy	k1gFnPc6	Bahamy
hovoří	hovořit	k5eAaImIp3nS	hovořit
bahamskou	bahamský	k2eAgFnSc7d1	bahamská
kreolskou	kreolský	k2eAgFnSc7d1	kreolská
angličtinou	angličtina	k1gFnSc7	angličtina
a	a	k8xC	a
kreolskou	kreolský	k2eAgFnSc7d1	kreolská
francouzštinou	francouzština	k1gFnSc7	francouzština
(	(	kIx(	(
<g/>
kterou	který	k3yRgFnSc7	který
hovoří	hovořit	k5eAaImIp3nP	hovořit
převážně	převážně	k6eAd1	převážně
imigranti	imigrant	k1gMnPc1	imigrant
z	z	k7c2	z
Haiti	Haiti	k1gNnSc2	Haiti
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Baham	Bahamy	k1gFnPc2	Bahamy
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
náboženský	náboženský	k2eAgInSc1d1	náboženský
národ	národ	k1gInSc1	národ
<g/>
,	,	kIx,	,
země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
nejvíce	nejvíce	k6eAd1	nejvíce
kostelů	kostel	k1gInPc2	kostel
na	na	k7c4	na
osobu	osoba	k1gFnSc4	osoba
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
je	být	k5eAaImIp3nS	být
nejrozšířenějším	rozšířený	k2eAgNnSc7d3	nejrozšířenější
náboženstvím	náboženství	k1gNnSc7	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Nejdominantnější	dominantní	k2eAgFnSc1d3	nejdominantnější
je	být	k5eAaImIp3nS	být
Baptistská	baptistský	k2eAgFnSc1d1	baptistský
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
ve	v	k7c4	v
kterou	který	k3yQgFnSc4	který
věří	věřit	k5eAaImIp3nS	věřit
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
církve	církev	k1gFnPc1	církev
jsou	být	k5eAaImIp3nP	být
anglikáni	anglikán	k1gMnPc1	anglikán
<g/>
,	,	kIx,	,
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
a	a	k8xC	a
metodisté	metodista	k1gMnPc1	metodista
<g/>
.	.	kIx.	.
</s>
<s>
KAŠPAR	Kašpar	k1gMnSc1	Kašpar
<g/>
,	,	kIx,	,
Oldřich	Oldřich	k1gMnSc1	Oldřich
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Karibské	karibský	k2eAgFnSc2d1	karibská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
557	[number]	k4	557
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bahamy	Bahamy	k1gFnPc1	Bahamy
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Bahamy	Bahamy	k1gFnPc1	Bahamy
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Bahamas	Bahamasa	k1gFnPc2	Bahamasa
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gMnPc2	International
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bahamas	Bahamas	k1gInSc1	Bahamas
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc4	Freedom
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bureau	Burea	k1gMnSc3	Burea
of	of	k?	of
Western	Western	kA	Western
Hemispehere	Hemispeher	k1gInSc5	Hemispeher
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Not	k1gFnSc2	Not
<g/>
:	:	kIx,	:
Bahamas	Bahamas	k1gMnSc1	Bahamas
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Bahamas	Bahamas	k1gMnSc1	Bahamas
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
REV	REV	kA	REV
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Havaně	Havana	k1gFnSc6	Havana
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Bahamy	Bahamy	k1gFnPc1	Bahamy
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
27	[number]	k4	27
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
ALBURY	ALBURY	kA	ALBURY
<g/>
,	,	kIx,	,
E.	E.	kA	E.
Paul	Paul	k1gMnSc1	Paul
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Bahamas	Bahamas	k1gInSc1	Bahamas
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
