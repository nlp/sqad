<s>
Marek	Marek	k1gMnSc1	Marek
je	být	k5eAaImIp3nS	být
mužské	mužský	k2eAgNnSc4d1	mužské
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
latinského	latinský	k2eAgInSc2d1	latinský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
odvozené	odvozený	k2eAgNnSc1d1	odvozené
ze	z	k7c2	z
jména	jméno	k1gNnSc2	jméno
Martius	Martius	k1gInSc1	Martius
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
zasvěcený	zasvěcený	k2eAgInSc4d1	zasvěcený
bohu	bůh	k1gMnSc6	bůh
války	válka	k1gFnSc2	válka
<g/>
"	"	kIx"	"
Martovi	Mars	k1gMnSc6	Mars
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
českého	český	k2eAgInSc2d1	český
kalendáře	kalendář	k1gInSc2	kalendář
má	mít	k5eAaImIp3nS	mít
svátek	svátek	k1gInSc1	svátek
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
jména	jméno	k1gNnPc1	jméno
stejného	stejný	k2eAgInSc2d1	stejný
původu	původ	k1gInSc2	původ
<g/>
:	:	kIx,	:
Marcel	Marcel	k1gMnSc1	Marcel
a	a	k8xC	a
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Mareček	Mareček	k1gMnSc1	Mareček
<g/>
,	,	kIx,	,
Marča	Marča	k1gMnSc1	Marča
<g/>
,	,	kIx,	,
Mářa	Mářa	k1gFnSc1	Mářa
<g/>
,	,	kIx,	,
Mára	Mára	k1gMnSc1	Mára
<g/>
,	,	kIx,	,
Marko	Marko	k1gMnSc1	Marko
<g/>
,	,	kIx,	,
Mary	Mary	k1gFnSc1	Mary
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
mužskými	mužský	k2eAgNnPc7d1	mužské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
dvou	dva	k4xCgInPc2	dva
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
–	–	k?	–
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaPmF	vysledovat
trend	trend	k1gInSc4	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
<g />
.	.	kIx.	.
</s>
<s>
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgMnPc7d1	žijící
muži	muž	k1gMnPc7	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
+4,8	+4,8	k4	+4,8
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
poměrně	poměrně	k6eAd1	poměrně
značném	značný	k2eAgInSc6d1	značný
nárůstu	nárůst	k1gInSc6	nárůst
obliby	obliba	k1gFnSc2	obliba
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
ČSÚ	ČSÚ	kA	ČSÚ
se	se	k3xPyFc4	se
za	za	k7c4	za
leden	leden	k1gInSc4	leden
2006	[number]	k4	2006
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
17	[number]	k4	17
<g/>
.	.	kIx.	.
nejčastější	častý	k2eAgNnSc1d3	nejčastější
mužské	mužský	k2eAgNnSc1d1	mužské
jméno	jméno	k1gNnSc1	jméno
novorozenců	novorozenec	k1gMnPc2	novorozenec
<g/>
.	.	kIx.	.
sv.	sv.	kA	sv.
Marius	Marius	k1gInSc1	Marius
z	z	k7c2	z
Avenches	Avenchesa	k1gFnPc2	Avenchesa
<g/>
]	]	kIx)	]
–	–	k?	–
galsko-římský	galsko-římský	k2eAgMnSc1d1	galsko-římský
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
biskup	biskup	k1gMnSc1	biskup
v	v	k7c6	v
Aventicu	Aventicum	k1gNnSc6	Aventicum
<g/>
,	,	kIx,	,
světec	světec	k1gMnSc1	světec
sv.	sv.	kA	sv.
Marek	Marek	k1gMnSc1	Marek
Evangelista	evangelista	k1gMnSc1	evangelista
sv.	sv.	kA	sv.
Marek	Marek	k1gMnSc1	Marek
I.	I.	kA	I.
<g/>
,	,	kIx,	,
papež	papež	k1gMnSc1	papež
sv.	sv.	kA	sv.
Marek	Marek	k1gMnSc1	Marek
Efezský	Efezský	k2eAgMnSc1d1	Efezský
(	(	kIx(	(
<g/>
Marek	Marek	k1gMnSc1	Marek
<g />
.	.	kIx.	.
</s>
<s>
Eugenikos	Eugenikos	k1gInSc1	Eugenikos
<g/>
)	)	kIx)	)
bl.	bl.	k?	bl.
Marek	Marek	k1gMnSc1	Marek
z	z	k7c2	z
Aviana	Avian	k1gMnSc2	Avian
sv.	sv.	kA	sv.
Mauricius	Mauricius	k1gMnSc1	Mauricius
<g/>
,	,	kIx,	,
křesťanský	křesťanský	k2eAgMnSc1d1	křesťanský
mučedník	mučedník	k1gMnSc1	mučedník
Marcus	Marcus	k1gMnSc1	Marcus
Aurelius	Aurelius	k1gMnSc1	Aurelius
–	–	k?	–
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
a	a	k8xC	a
filosof	filosof	k1gMnSc1	filosof
Marcus	Marcus	k1gMnSc1	Marcus
Verrius	Verrius	k1gMnSc1	Verrius
Flaccus	Flaccus	k1gMnSc1	Flaccus
–	–	k?	–
římský	římský	k2eAgMnSc1d1	římský
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
vychovatel	vychovatel	k1gMnSc1	vychovatel
a	a	k8xC	a
gramatik	gramatik	k1gMnSc1	gramatik
Marcus	Marcus	k1gMnSc1	Marcus
Cornelius	Cornelius	k1gMnSc1	Cornelius
Fronto	fronta	k1gFnSc5	fronta
–	–	k?	–
římský	římský	k2eAgMnSc1d1	římský
řečník	řečník	k1gMnSc1	řečník
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
Marcus	Marcus	k1gMnSc1	Marcus
Aemilius	Aemilius	k1gMnSc1	Aemilius
Lepidus	Lepidus	k1gMnSc1	Lepidus
–	–	k?	–
římský	římský	k2eAgMnSc1d1	římský
senátor	senátor	k1gMnSc1	senátor
a	a	k8xC	a
konzul	konzul	k1gMnSc1	konzul
<g />
.	.	kIx.	.
</s>
<s>
Marcus	Marcus	k1gMnSc1	Marcus
Valerius	Valerius	k1gMnSc1	Valerius
Martialis	Martialis	k1gFnSc2	Martialis
–	–	k?	–
římský	římský	k2eAgMnSc1d1	římský
epigramik	epigramik	k1gMnSc1	epigramik
Marco	Marco	k1gMnSc1	Marco
Polo	polo	k6eAd1	polo
–	–	k?	–
italský	italský	k2eAgMnSc1d1	italský
cestovatel	cestovatel	k1gMnSc1	cestovatel
a	a	k8xC	a
mořeplavec	mořeplavec	k1gMnSc1	mořeplavec
Marcus	Marcus	k1gMnSc1	Marcus
Vipsanius	Vipsanius	k1gMnSc1	Vipsanius
Agrippa	Agrippa	k1gFnSc1	Agrippa
–	–	k?	–
římský	římský	k2eAgMnSc1d1	římský
generál	generál	k1gMnSc1	generál
Marcus	Marcus	k1gMnSc1	Marcus
Aurelius	Aurelius	k1gMnSc1	Aurelius
–	–	k?	–
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
a	a	k8xC	a
Filosof	filosof	k1gMnSc1	filosof
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
Filozof	filozof	k1gMnSc1	filozof
na	na	k7c6	na
trůně	trůn	k1gInSc6	trůn
Marek	Marek	k1gMnSc1	Marek
Eben	eben	k1gInSc1	eben
–	–	k?	–
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
Moderátor	moderátor	k1gMnSc1	moderátor
(	(	kIx(	(
<g/>
profese	profese	k1gFnSc1	profese
<g/>
)	)	kIx)	)
moderátor	moderátor	k1gMnSc1	moderátor
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
Marek	Marek	k1gMnSc1	Marek
<g />
.	.	kIx.	.
</s>
<s>
Grechuta	Grechut	k2eAgFnSc1d1	Grechut
–	–	k?	–
polský	polský	k2eAgMnSc1d1	polský
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
textař	textař	k1gMnSc1	textař
Marek	Marek	k1gMnSc1	Marek
Heinz	Heinz	k1gMnSc1	Heinz
–	–	k?	–
český	český	k2eAgMnSc1d1	český
fotbalista	fotbalista	k1gMnSc1	fotbalista
Marcus	Marcus	k1gMnSc1	Marcus
Aurelius	Aurelius	k1gMnSc1	Aurelius
Antonius	Antonius	k1gMnSc1	Antonius
Heliogabalus	Heliogabalus	k1gMnSc1	Heliogabalus
–	–	k?	–
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
Marek	Marek	k1gMnSc1	Marek
Holý	Holý	k1gMnSc1	Holý
–	–	k?	–
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
Marek	Marek	k1gMnSc1	Marek
Jankulovski	Jankulovsk	k1gFnSc2	Jankulovsk
–	–	k?	–
český	český	k2eAgMnSc1d1	český
fotbalista	fotbalista	k1gMnSc1	fotbalista
Marek	Marek	k1gMnSc1	Marek
Jiras	Jiras	k1gMnSc1	Jiras
–	–	k?	–
český	český	k2eAgMnSc1d1	český
kanoista	kanoista	k1gMnSc1	kanoista
Mark	Mark	k1gMnSc1	Mark
Knopfler	Knopfler	k1gMnSc1	Knopfler
–	–	k?	–
britský	britský	k2eAgMnSc1d1	britský
kytarista	kytarista	k1gMnSc1	kytarista
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
Mark	Mark	k1gMnSc1	Mark
Ravenhill	Ravenhill	k1gMnSc1	Ravenhill
–	–	k?	–
dramatik	dramatik	k1gMnSc1	dramatik
Mark	Mark	k1gMnSc1	Mark
Shepherd	Shepherd	k1gMnSc1	Shepherd
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
spisovatel	spisovatel	k1gMnSc1	spisovatel
Marek	Marek	k1gMnSc1	Marek
Suchý	Suchý	k1gMnSc1	Suchý
–	–	k?	–
český	český	k2eAgMnSc1d1	český
fotbalista	fotbalista	k1gMnSc1	fotbalista
Marek	Marek	k1gMnSc1	Marek
Taclík	Taclík	k1gMnSc1	Taclík
–	–	k?	–
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
Mark	Mark	k1gMnSc1	Mark
Twain	Twain	k1gMnSc1	Twain
–	–	k?	–
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
Marek	marka	k1gFnPc2	marka
Štilec	štilec	k1gInSc4	štilec
–	–	k?	–
český	český	k2eAgMnSc1d1	český
dirigent	dirigent	k1gMnSc1	dirigent
Marek	Marek	k1gMnSc1	Marek
Uram	Uram	k1gMnSc1	Uram
–	–	k?	–
slovenský	slovenský	k2eAgMnSc1d1	slovenský
hokejista	hokejista	k1gMnSc1	hokejista
Marek	Marek	k1gMnSc1	Marek
Vašut	Vašut	k1gMnSc1	Vašut
–	–	k?	–
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
Marek	Marek	k1gMnSc1	Marek
Židlický	Židlický	k1gMnSc1	Židlický
–	–	k?	–
český	český	k2eAgMnSc1d1	český
hokejista	hokejista	k1gMnSc1	hokejista
Marek	Marek	k1gMnSc1	Marek
Bersziak	Bersziak	k1gMnSc1	Bersziak
–	–	k?	–
maďarský	maďarský	k2eAgMnSc1d1	maďarský
šlechtic	šlechtic	k1gMnSc1	šlechtic
Marek	Marek	k1gMnSc1	Marek
Sekyra	Sekyra	k1gMnSc1	Sekyra
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
moderátor	moderátor	k1gMnSc1	moderátor
Mareček	Mareček	k1gMnSc1	Mareček
Markovič	Markovič	k1gMnSc1	Markovič
Marek	Marek	k1gMnSc1	Marek
(	(	kIx(	(
<g/>
příjmení	příjmení	k1gNnSc1	příjmení
<g/>
)	)	kIx)	)
–	–	k?	–
české	český	k2eAgNnSc4d1	české
příjmení	příjmení	k1gNnSc4	příjmení
Seznam	seznam	k1gInSc4	seznam
článků	článek	k1gInPc2	článek
začínajících	začínající	k2eAgInPc2d1	začínající
na	na	k7c4	na
"	"	kIx"	"
<g/>
Marek	Marek	k1gMnSc1	Marek
<g/>
"	"	kIx"	"
Seznam	seznam	k1gInSc1	seznam
článků	článek	k1gInPc2	článek
začínajících	začínající	k2eAgInPc2d1	začínající
na	na	k7c4	na
"	"	kIx"	"
<g/>
Mark	Mark	k1gMnSc1	Mark
<g/>
"	"	kIx"	"
Seznam	seznam	k1gInSc1	seznam
článků	článek	k1gInPc2	článek
začínajících	začínající	k2eAgInPc2d1	začínající
na	na	k7c4	na
"	"	kIx"	"
<g/>
Marcus	Marcus	k1gMnSc1	Marcus
<g/>
"	"	kIx"	"
Seznam	seznam	k1gInSc1	seznam
článků	článek	k1gInPc2	článek
začínajících	začínající	k2eAgInPc2d1	začínající
na	na	k7c4	na
"	"	kIx"	"
<g/>
Marco	Marco	k6eAd1	Marco
<g/>
"	"	kIx"	"
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Marek	Marek	k1gMnSc1	Marek
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
