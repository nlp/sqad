<s>
Státní	státní	k2eAgInSc1d1	státní
fond	fond	k1gInSc1	fond
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
české	český	k2eAgFnSc2d1	Česká
kinematografie	kinematografie	k1gFnSc2	kinematografie
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
českých	český	k2eAgInPc2d1	český
filmů	film	k1gInPc2	film
a	a	k8xC	a
filmařů	filmař	k1gMnPc2	filmař
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákona	zákon	k1gInSc2	zákon
ČNR	ČNR	kA	ČNR
č.	č.	k?	č.
241	[number]	k4	241
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
,	,	kIx,	,
ze	z	k7c2	z
dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
nástrojem	nástroj	k1gInSc7	nástroj
veřejné	veřejný	k2eAgFnSc2d1	veřejná
podpory	podpora	k1gFnSc2	podpora
filmové	filmový	k2eAgFnSc2d1	filmová
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
psaní	psaní	k1gNnSc4	psaní
scénářů	scénář	k1gInPc2	scénář
<g/>
,	,	kIx,	,
či	či	k8xC	či
výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
distribuce	distribuce	k1gFnSc1	distribuce
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Neméně	málo	k6eNd2	málo
důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
podpora	podpora	k1gFnSc1	podpora
technického	technický	k2eAgInSc2d1	technický
rozvoje	rozvoj	k1gInSc2	rozvoj
a	a	k8xC	a
modernizace	modernizace	k1gFnSc2	modernizace
kin	kino	k1gNnPc2	kino
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c4	na
spolufinancování	spolufinancování	k1gNnSc4	spolufinancování
projektů	projekt	k1gInPc2	projekt
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
propagace	propagace	k1gFnSc2	propagace
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
,	,	kIx,	,
např.	např.	kA	např.
festivalů	festival	k1gInPc2	festival
a	a	k8xC	a
přehlídek	přehlídka	k1gFnPc2	přehlídka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
členění	členění	k1gNnSc6	členění
rozpočtové	rozpočtový	k2eAgFnSc2d1	rozpočtová
soustavy	soustava	k1gFnSc2	soustava
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
fond	fond	k1gInSc1	fond
nachází	nacházet	k5eAaImIp3nS	nacházet
mezi	mezi	k7c7	mezi
mimorozpočtovými	mimorozpočtový	k2eAgInPc7d1	mimorozpočtový
fondy	fond	k1gInPc7	fond
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
konkrétně	konkrétně	k6eAd1	konkrétně
jako	jako	k8xC	jako
státní	státní	k2eAgInSc1d1	státní
účelový	účelový	k2eAgInSc1d1	účelový
fond	fond	k1gInSc1	fond
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
kultury	kultura	k1gFnSc2	kultura
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
ústředním	ústřední	k2eAgInSc7d1	ústřední
orgánem	orgán	k1gInSc7	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
zaštiťující	zaštiťující	k2eAgFnSc4d1	zaštiťující
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc4d1	kulturní
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
správou	správa	k1gFnSc7	správa
finančních	finanční	k2eAgInPc2d1	finanční
fondů	fond	k1gInPc2	fond
<g/>
,	,	kIx,	,
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
podpory	podpora	k1gFnSc2	podpora
a	a	k8xC	a
zachování	zachování	k1gNnSc4	zachování
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
kulturních	kulturní	k2eAgFnPc2d1	kulturní
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
jsou	být	k5eAaImIp3nP	být
státní	státní	k2eAgInPc1d1	státní
účelové	účelový	k2eAgInPc1d1	účelový
fondy	fond	k1gInPc1	fond
tvořeny	tvořit	k5eAaImNgInP	tvořit
výlučně	výlučně	k6eAd1	výlučně
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
zásady	zásada	k1gFnPc4	zásada
tvorby	tvorba	k1gFnSc2	tvorba
fondu	fond	k1gInSc2	fond
a	a	k8xC	a
způsob	způsob	k1gInSc4	způsob
čerpání	čerpání	k1gNnSc2	čerpání
jeho	jeho	k3xOp3gInPc2	jeho
prostředků	prostředek	k1gInPc2	prostředek
na	na	k7c4	na
přesně	přesně	k6eAd1	přesně
vymezené	vymezený	k2eAgInPc4d1	vymezený
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
fond	fond	k1gInSc1	fond
financuje	financovat	k5eAaBmIp3nS	financovat
specifickou	specifický	k2eAgFnSc4d1	specifická
oblast	oblast	k1gFnSc4	oblast
určenou	určený	k2eAgFnSc4d1	určená
jeho	jeho	k3xOp3gInSc7	jeho
statutem	statut	k1gInSc7	statut
a	a	k8xC	a
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
pak	pak	k6eAd1	pak
slouží	sloužit	k5eAaImIp3nS	sloužit
zejména	zejména	k9	zejména
k	k	k7c3	k
poskytování	poskytování	k1gNnSc3	poskytování
dotací	dotace	k1gFnPc2	dotace
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
půjček	půjčka	k1gFnPc2	půjčka
určitým	určitý	k2eAgInPc3d1	určitý
subjektům	subjekt	k1gInPc3	subjekt
za	za	k7c2	za
předem	předem	k6eAd1	předem
stanovených	stanovený	k2eAgFnPc2d1	stanovená
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
hospodaří	hospodařit	k5eAaImIp3nP	hospodařit
s	s	k7c7	s
veřejnými	veřejný	k2eAgInPc7d1	veřejný
prostředky	prostředek	k1gInPc7	prostředek
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
fondy	fond	k1gInPc1	fond
kontrolovány	kontrolovat	k5eAaImNgInP	kontrolovat
parlamentem	parlament	k1gInSc7	parlament
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
správce	správce	k1gMnSc1	správce
státního	státní	k2eAgInSc2d1	státní
fondu	fond	k1gInSc2	fond
je	být	k5eAaImIp3nS	být
povinen	povinen	k2eAgMnSc1d1	povinen
hospodařit	hospodařit	k5eAaImF	hospodařit
podle	podle	k7c2	podle
rozpočtu	rozpočet	k1gInSc2	rozpočet
státního	státní	k2eAgInSc2d1	státní
fondu	fond	k1gInSc2	fond
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
schvalován	schvalován	k2eAgInSc1d1	schvalován
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
na	na	k7c4	na
příslušný	příslušný	k2eAgInSc4d1	příslušný
kalendářní	kalendářní	k2eAgInSc4d1	kalendářní
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Fond	fond	k1gInSc1	fond
je	být	k5eAaImIp3nS	být
spravován	spravovat	k5eAaImNgInS	spravovat
Ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
kultury	kultura	k1gFnSc2	kultura
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Fondu	fond	k1gInSc2	fond
stojí	stát	k5eAaImIp3nS	stát
ministr	ministr	k1gMnSc1	ministr
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
za	za	k7c4	za
hospodaření	hospodaření	k1gNnSc4	hospodaření
s	s	k7c7	s
prostředky	prostředek	k1gInPc7	prostředek
Fondu	fond	k1gInSc2	fond
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
nemá	mít	k5eNaImIp3nS	mít
Fond	fond	k1gInSc1	fond
žádné	žádný	k3yNgFnSc2	žádný
organizační	organizační	k2eAgFnSc2d1	organizační
složky	složka	k1gFnSc2	složka
mimo	mimo	k7c4	mimo
sídlo	sídlo	k1gNnSc4	sídlo
správce	správce	k1gMnSc2	správce
Fondu	fond	k1gInSc2	fond
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
žádné	žádný	k3yNgMnPc4	žádný
zaměstnance	zaměstnanec	k1gMnPc4	zaměstnanec
a	a	k8xC	a
nezřizuje	zřizovat	k5eNaImIp3nS	zřizovat
žádné	žádný	k3yNgNnSc1	žádný
další	další	k2eAgMnSc1d1	další
jím	jíst	k5eAaImIp1nS	jíst
řízené	řízený	k2eAgFnPc4d1	řízená
příspěvkové	příspěvkový	k2eAgFnPc4d1	příspěvková
organizace	organizace	k1gFnPc4	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
kultury	kultura	k1gFnSc2	kultura
jako	jako	k8xC	jako
správce	správce	k1gMnSc2	správce
Fondu	fond	k1gInSc2	fond
sestavuje	sestavovat	k5eAaImIp3nS	sestavovat
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
rozpočtový	rozpočtový	k2eAgInSc4d1	rozpočtový
rok	rok	k1gInSc4	rok
návrh	návrh	k1gInSc1	návrh
rozpočtu	rozpočet	k1gInSc2	rozpočet
příjmů	příjem	k1gInPc2	příjem
a	a	k8xC	a
výdajů	výdaj	k1gInPc2	výdaj
<g/>
,	,	kIx,	,
přehled	přehled	k1gInSc4	přehled
pohledávek	pohledávka	k1gFnPc2	pohledávka
a	a	k8xC	a
závazků	závazek	k1gInPc2	závazek
a	a	k8xC	a
závěrečný	závěrečný	k2eAgInSc4d1	závěrečný
účet	účet	k1gInSc4	účet
Fondu	fond	k1gInSc2	fond
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
předkládá	předkládat	k5eAaImIp3nS	předkládat
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
financí	finance	k1gFnPc2	finance
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
rozpočtu	rozpočet	k1gInSc2	rozpočet
Fondu	fond	k1gInSc2	fond
je	být	k5eAaImIp3nS	být
posléze	posléze	k6eAd1	posléze
předložen	předložit	k5eAaPmNgInS	předložit
vládě	vláda	k1gFnSc3	vláda
vždy	vždy	k6eAd1	vždy
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
jej	on	k3xPp3gNnSc4	on
po	po	k7c6	po
případných	případný	k2eAgFnPc6d1	případná
úpravách	úprava	k1gFnPc6	úprava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
provede	provést	k5eAaPmIp3nS	provést
<g/>
,	,	kIx,	,
předkládá	předkládat	k5eAaImIp3nS	předkládat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
návrhem	návrh	k1gInSc7	návrh
státního	státní	k2eAgInSc2d1	státní
rozpočtu	rozpočet	k1gInSc2	rozpočet
ke	k	k7c3	k
schválení	schválení	k1gNnSc3	schválení
Poslanecké	poslanecký	k2eAgFnSc3d1	Poslanecká
sněmovně	sněmovna	k1gFnSc3	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Závěrečný	závěrečný	k2eAgInSc1d1	závěrečný
účet	účet	k1gInSc1	účet
Fondu	fond	k1gInSc2	fond
pak	pak	k6eAd1	pak
tvoří	tvořit	k5eAaImIp3nP	tvořit
přílohu	příloha	k1gFnSc4	příloha
státního	státní	k2eAgInSc2d1	státní
závěrečného	závěrečný	k2eAgInSc2d1	závěrečný
účtu	účet	k1gInSc2	účet
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
závěrečným	závěrečný	k2eAgInSc7d1	závěrečný
účtem	účet	k1gInSc7	účet
je	být	k5eAaImIp3nS	být
správcem	správce	k1gMnSc7	správce
Fondu	fond	k1gInSc2	fond
předkládáno	předkládat	k5eAaImNgNnS	předkládat
i	i	k9	i
vyhodnocení	vyhodnocení	k1gNnSc1	vyhodnocení
použití	použití	k1gNnSc2	použití
vynaložených	vynaložený	k2eAgInPc2d1	vynaložený
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgInPc4d3	nejvýznamnější
finanční	finanční	k2eAgInPc4d1	finanční
zdroje	zdroj	k1gInPc4	zdroj
Fondu	fond	k1gInSc2	fond
patří	patřit	k5eAaImIp3nP	patřit
příjmy	příjem	k1gInPc1	příjem
z	z	k7c2	z
obchodního	obchodní	k2eAgNnSc2d1	obchodní
využití	využití	k1gNnSc2	využití
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
Fond	fond	k1gInSc1	fond
vlastní	vlastní	k2eAgNnPc4d1	vlastní
autorská	autorský	k2eAgNnPc4d1	autorské
práva	právo	k1gNnPc4	právo
výrobce	výrobce	k1gMnPc4	výrobce
a	a	k8xC	a
odvody	odvod	k1gInPc4	odvod
příplatku	příplatek	k1gInSc2	příplatek
k	k	k7c3	k
ceně	cena	k1gFnSc3	cena
vstupného	vstupné	k1gNnSc2	vstupné
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
finanční	finanční	k2eAgFnSc1d1	finanční
podpora	podpora	k1gFnSc1	podpora
kinematografie	kinematografie	k1gFnSc1	kinematografie
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
státu	stát	k1gInSc2	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
nominálně	nominálně	k6eAd1	nominálně
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
postrádá	postrádat	k5eAaImIp3nS	postrádat
širší	široký	k2eAgFnSc4d2	širší
a	a	k8xC	a
stabilnější	stabilní	k2eAgFnSc4d2	stabilnější
základnu	základna	k1gFnSc4	základna
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
nepodařilo	podařit	k5eNaPmAgNnS	podařit
prosadit	prosadit	k5eAaPmF	prosadit
novely	novela	k1gFnPc4	novela
audiovizuálních	audiovizuální	k2eAgInPc2d1	audiovizuální
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
ji	on	k3xPp3gFnSc4	on
posílily	posílit	k5eAaPmAgFnP	posílit
například	například	k6eAd1	například
rozšířením	rozšíření	k1gNnSc7	rozšíření
zdrojů	zdroj	k1gInPc2	zdroj
příjmů	příjem	k1gInPc2	příjem
Fondu	fond	k1gInSc2	fond
kinematografie	kinematografie	k1gFnSc2	kinematografie
nebo	nebo	k8xC	nebo
zřízením	zřízení	k1gNnSc7	zřízení
Českého	český	k2eAgNnSc2d1	české
audiovizuálního	audiovizuální	k2eAgNnSc2d1	audiovizuální
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
podpořené	podpořený	k2eAgInPc4d1	podpořený
projekty	projekt	k1gInPc4	projekt
<g/>
.	.	kIx.	.
</s>
