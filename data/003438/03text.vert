<s>
Zebřička	zebřička	k1gFnSc1	zebřička
pestrá	pestrý	k2eAgFnSc1d1	pestrá
(	(	kIx(	(
<g/>
Taeniopygia	Taeniopygia	k1gFnSc1	Taeniopygia
guttata	guttata	k1gFnSc1	guttata
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
nejrozšířenějším	rozšířený	k2eAgInSc7d3	nejrozšířenější
a	a	k8xC	a
nejznámějším	známý	k2eAgInSc7d3	nejznámější
druhem	druh	k1gInSc7	druh
astrildovitých	astrildovitý	k2eAgMnPc2d1	astrildovitý
ptáků	pták	k1gMnPc2	pták
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
australském	australský	k2eAgInSc6d1	australský
kontinentě	kontinent	k1gInSc6	kontinent
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
nejchladnějších	chladný	k2eAgFnPc2d3	nejchladnější
a	a	k8xC	a
příliš	příliš	k6eAd1	příliš
vlhkých	vlhký	k2eAgFnPc2d1	vlhká
oblastí	oblast	k1gFnPc2	oblast
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Izolovaně	izolovaně	k6eAd1	izolovaně
žije	žít	k5eAaImIp3nS	žít
i	i	k9	i
v	v	k7c6	v
Indonésii	Indonésie	k1gFnSc6	Indonésie
<g/>
.	.	kIx.	.
</s>
<s>
Člověkem	člověk	k1gMnSc7	člověk
však	však	k9	však
byla	být	k5eAaImAgFnS	být
zavlečena	zavleknout	k5eAaPmNgFnS	zavleknout
i	i	k9	i
na	na	k7c4	na
Puerto	Puerta	k1gFnSc5	Puerta
Rico	Rico	k1gNnSc4	Rico
<g/>
,	,	kIx,	,
do	do	k7c2	do
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
a	a	k8xC	a
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgInSc1d1	populární
také	také	k9	také
jako	jako	k9	jako
klecový	klecový	k2eAgMnSc1d1	klecový
pták	pták	k1gMnSc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
dvě	dva	k4xCgFnPc1	dva
zeměpisné	zeměpisný	k2eAgFnPc1d1	zeměpisná
formy	forma	k1gFnPc1	forma
zebřiček	zebřička	k1gFnPc2	zebřička
<g/>
:	:	kIx,	:
zebřička	zebřička	k1gFnSc1	zebřička
pestrá	pestrý	k2eAgFnSc1d1	pestrá
ostrovní	ostrovní	k2eAgFnSc1d1	ostrovní
(	(	kIx(	(
<g/>
Taeniopygia	Taeniopygia	k1gFnSc1	Taeniopygia
guttata	guttata	k1gFnSc1	guttata
guttata	guttata	k1gFnSc1	guttata
<g/>
)	)	kIx)	)
zebřička	zebřička	k1gFnSc1	zebřička
pestrá	pestrý	k2eAgFnSc1d1	pestrá
australská	australský	k2eAgFnSc1d1	australská
(	(	kIx(	(
<g/>
Taeniopygia	Taeniopygia	k1gFnSc1	Taeniopygia
guttata	guttata	k1gFnSc1	guttata
castanotis	castanotis	k1gFnSc1	castanotis
<g/>
)	)	kIx)	)
Obývá	obývat	k5eAaImIp3nS	obývat
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
krajin	krajina	k1gFnPc2	krajina
<g/>
,	,	kIx,	,
nejhojněji	hojně	k6eAd3	hojně
se	se	k3xPyFc4	se
však	však	k9	však
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
stepích	step	k1gFnPc6	step
s	s	k7c7	s
roztroušeným	roztroušený	k2eAgInSc7d1	roztroušený
porostem	porost	k1gInSc7	porost
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
keřů	keř	k1gInPc2	keř
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
také	také	k9	také
do	do	k7c2	do
těsné	těsný	k2eAgFnSc2d1	těsná
blízkosti	blízkost	k1gFnSc2	blízkost
lidských	lidský	k2eAgNnPc2d1	lidské
obydlí	obydlí	k1gNnPc2	obydlí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
semenožravá	semenožravý	k2eAgFnSc1d1	semenožravý
<g/>
:	:	kIx,	:
Žere	žrát	k5eAaImIp3nS	žrát
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
prosa	proso	k1gNnSc2	proso
<g/>
,	,	kIx,	,
vyloupává	vyloupávat	k5eAaImIp3nS	vyloupávat
semena	semeno	k1gNnPc4	semeno
z	z	k7c2	z
klasů	klas	k1gInPc2	klas
běžných	běžný	k2eAgFnPc2d1	běžná
obilovin	obilovina	k1gFnPc2	obilovina
a	a	k8xC	a
trav	tráva	k1gFnPc2	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
jejich	jejich	k3xOp3gFnSc2	jejich
stravy	strava	k1gFnSc2	strava
je	být	k5eAaImIp3nS	být
ovoce	ovoce	k1gNnSc1	ovoce
a	a	k8xC	a
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
jablka	jablko	k1gNnPc1	jablko
<g/>
,	,	kIx,	,
salátová	salátový	k2eAgFnSc1d1	salátová
okurka	okurka	k1gFnSc1	okurka
<g/>
,	,	kIx,	,
strouhaná	strouhaný	k2eAgFnSc1d1	strouhaná
mrkev	mrkev	k1gFnSc1	mrkev
<g/>
,	,	kIx,	,
banány	banán	k1gInPc1	banán
<g/>
,	,	kIx,	,
hrozny	hrozen	k1gInPc1	hrozen
<g/>
,	,	kIx,	,
hlávkový	hlávkový	k2eAgInSc1d1	hlávkový
salát	salát	k1gInSc1	salát
<g/>
.	.	kIx.	.
</s>
<s>
Nenahraditelným	nahraditelný	k2eNgInSc7d1	nenahraditelný
zdrojem	zdroj	k1gInSc7	zdroj
minerálů	minerál	k1gInPc2	minerál
jsou	být	k5eAaImIp3nP	být
rozdrcené	rozdrcený	k2eAgFnPc1d1	rozdrcená
vaječné	vaječný	k2eAgFnPc1d1	vaječná
skořápky	skořápka	k1gFnPc1	skořápka
kura	kur	k1gMnSc2	kur
domácího	domácí	k1gMnSc2	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
<g/>
:	:	kIx,	:
ptačinec	ptačinec	k1gInSc1	ptačinec
žabinec	žabinec	k1gInSc1	žabinec
<g/>
,	,	kIx,	,
smetánku	smetánka	k1gFnSc4	smetánka
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
<g/>
,	,	kIx,	,
sedmikrásku	sedmikráska	k1gFnSc4	sedmikráska
a	a	k8xC	a
kokošku	kokoška	k1gFnSc4	kokoška
pastuší	pastuší	k2eAgFnSc4d1	pastuší
tobolku	tobolka	k1gFnSc4	tobolka
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pomněnku	pomněnka	k1gFnSc4	pomněnka
<g/>
,	,	kIx,	,
řeřišnici	řeřišnice	k1gFnSc4	řeřišnice
<g/>
,	,	kIx,	,
jitrocel	jitrocel	k1gInSc1	jitrocel
<g/>
,	,	kIx,	,
řebříček	řebříček	k1gInSc1	řebříček
<g/>
,	,	kIx,	,
šťovík	šťovík	k1gInSc1	šťovík
a	a	k8xC	a
pelyněk	pelyněk	k1gInSc1	pelyněk
černobýl	černobýl	k1gInSc1	černobýl
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
většiny	většina	k1gFnSc2	většina
zmiňovaných	zmiňovaný	k2eAgFnPc2d1	zmiňovaná
rostlin	rostlina	k1gFnPc2	rostlina
sbíráme	sbírat	k5eAaImIp1nP	sbírat
pouze	pouze	k6eAd1	pouze
polozralé	polozralý	k2eAgFnPc4d1	polozralý
semenice	semenice	k1gFnPc4	semenice
<g/>
.	.	kIx.	.
</s>
