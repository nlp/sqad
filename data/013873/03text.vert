<s>
Sentinelština	Sentinelština	k1gFnSc1
</s>
<s>
Sentinelština	Sentinelština	k1gFnSc1
Jazyky	jazyk	k1gMnPc4
a	a	k8xC
kmeny	kmen	k1gInPc4
Andaman	Andamany	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sentinelština	Sentinelština	k1gFnSc1
je	být	k5eAaImIp3nS
označena	označit	k5eAaPmNgFnS
šedě	šedě	k6eAd1
<g/>
,	,	kIx,
a	a	k8xC
je	být	k5eAaImIp3nS
u	u	k7c2
ní	on	k3xPp3gFnSc2
anglický	anglický	k2eAgInSc1d1
název	název	k1gInSc1
Sentinelese	Sentinelese	k1gFnSc2
Rozšíření	rozšíření	k1gNnSc2
</s>
<s>
Indie	Indie	k1gFnSc1
<g/>
,	,	kIx,
ostrov	ostrov	k1gInSc1
North	North	k1gInSc1
Sentinel	sentinel	k1gInSc1
Počet	počet	k1gInSc1
mluvčích	mluvčí	k1gFnPc2
</s>
<s>
100	#num#	k4
<g/>
-	-	kIx~
<g/>
250	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Klasifikace	klasifikace	k1gFnSc1
</s>
<s>
neklasifikované	klasifikovaný	k2eNgInPc1d1
jazyky	jazyk	k1gInPc1
Postavení	postavení	k1gNnSc2
Regulátor	regulátor	k1gMnSc1
</s>
<s>
není	být	k5eNaImIp3nS
Úřední	úřední	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
</s>
<s>
není	být	k5eNaImIp3nS
uznán	uznat	k5eAaPmNgInS
Kódy	kód	k1gInPc1
ISO	ISO	kA
639-1	639-1	k4
</s>
<s>
není	být	k5eNaImIp3nS
ISO	ISO	kA
639-2	639-2	k4
</s>
<s>
není	být	k5eNaImIp3nS
ISO	ISO	kA
639-3	639-3	k4
</s>
<s>
std	std	k?
Wikipedie	Wikipedie	k1gFnSc1
</s>
<s>
neexistuje	existovat	k5eNaImIp3nS
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sentinelština	Sentinelština	k1gFnSc1
je	být	k5eAaImIp3nS
jazyk	jazyk	k1gInSc1
Sentinelců	Sentinelec	k1gInPc2
z	z	k7c2
ostrova	ostrov	k1gInSc2
North	North	k1gInSc4
Sentinel	sentinel	k1gInSc1
(	(	kIx(
<g/>
Andamany	Andamany	k1gFnPc1
a	a	k8xC
Nikobary	Nikobary	k1gFnPc1
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
už	už	k6eAd1
300	#num#	k4
let	rok	k1gInPc2
nedošlo	dojít	k5eNaPmAgNnS
k	k	k7c3
žádnému	žádný	k3yNgInSc3
kontaktu	kontakt	k1gInSc3
Sentinelců	Sentinelec	k1gMnPc2
a	a	k8xC
zbytku	zbytek	k1gInSc2
světa	svět	k1gInSc2
<g/>
,	,	kIx,
nikdo	nikdo	k3yNnSc1
jiný	jiný	k2eAgMnSc1d1
než	než	k8xS
oni	on	k3xPp3gMnPc1
neumí	umět	k5eNaImIp3nP
tento	tento	k3xDgInSc4
jazyk	jazyk	k1gInSc4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
známo	znám	k2eAgNnSc1d1
jediné	jediný	k2eAgNnSc1d1
slovo	slovo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neexistuje	existovat	k5eNaImIp3nS
však	však	k9
ani	ani	k8xC
způsob	způsob	k1gInSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
informace	informace	k1gFnPc1
o	o	k7c6
sentinelštině	sentinelština	k1gFnSc6
získat	získat	k5eAaPmF
<g/>
,	,	kIx,
protože	protože	k8xS
indická	indický	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
nepovoluje	povolovat	k5eNaImIp3nS
nikomu	nikdo	k3yNnSc3
vstup	vstup	k1gInSc1
na	na	k7c4
ostrov	ostrov	k1gInSc4
a	a	k8xC
Sentinelci	Sentinelec	k1gMnPc1
jsou	být	k5eAaImIp3nP
vůči	vůči	k7c3
cizincům	cizinec	k1gMnPc3
velmi	velmi	k6eAd1
nepřátelští	přátelský	k2eNgMnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Klasifikace	klasifikace	k1gFnSc1
</s>
<s>
Mnoho	mnoho	k4c1
lidí	člověk	k1gMnPc2
předpokládá	předpokládat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
sentinelština	sentinelština	k1gFnSc1
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
andamanské	andamanský	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
andamanské	andamanský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
jsou	být	k5eAaImIp3nP
vlastně	vlastně	k9
jazykový	jazykový	k2eAgInSc1d1
svaz	svaz	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
ukrývá	ukrývat	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
jazykové	jazykový	k2eAgFnPc4d1
rodiny	rodina	k1gFnPc4
<g/>
:	:	kIx,
velkoandamanské	velkoandamanský	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
a	a	k8xC
onžské	onžský	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Onžskými	Onžský	k2eAgInPc7d1
jazyky	jazyk	k1gInPc7
mluví	mluvit	k5eAaImIp3nP
podstatně	podstatně	k6eAd1
méně	málo	k6eAd2
lidí	člověk	k1gMnPc2
<g/>
,	,	kIx,
než	než	k8xS
velkoandamanskými	velkoandamanský	k2eAgMnPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
tedy	tedy	k9
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
by	by	kYmCp3nS
sentinelština	sentinelština	k1gFnSc1
byla	být	k5eAaImAgFnS
andamanským	andamanský	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
<g/>
,	,	kIx,
řadila	řadit	k5eAaImAgFnS
by	by	kYmCp3nS
se	se	k3xPyFc4
mezi	mezi	k7c7
onžské	onžské	k2eAgInPc7d1
jazyky	jazyk	k1gInPc7
<g/>
,	,	kIx,
ovšem	ovšem	k9
existují	existovat	k5eAaImIp3nP
teorie	teorie	k1gFnPc1
<g/>
,	,	kIx,
že	že	k8xS
sentinelština	sentinelština	k1gFnSc1
je	být	k5eAaImIp3nS
jazyk	jazyk	k1gInSc4
andamanský	andamanský	k2eAgInSc4d1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sentinelština	Sentinelština	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
také	také	k9
izolovaný	izolovaný	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nedávné	dávný	k2eNgFnSc6d1
době	doba	k1gFnSc6
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
proběhla	proběhnout	k5eAaPmAgFnS
dvě	dva	k4xCgNnPc4
setkání	setkání	k1gNnPc4
se	se	k3xPyFc4
Sentinelci	Sentinelec	k1gMnPc1
<g/>
,	,	kIx,
ovšem	ovšem	k9
nikdo	nikdo	k3yNnSc1
nerozpoznal	rozpoznat	k5eNaPmAgMnS
jazyk	jazyk	k1gInSc4
<g/>
,	,	kIx,
kterým	který	k3yQgMnPc3,k3yIgMnPc3,k3yRgMnPc3
mluvili	mluvit	k5eAaImAgMnP
v	v	k7c6
krátkém	krátký	k2eAgInSc6d1
boji	boj	k1gInSc6
proti	proti	k7c3
nepřátelům	nepřítel	k1gMnPc3
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jazyk	jazyk	k1gInSc1
se	se	k3xPyFc4
proto	proto	k8xC
řadí	řadit	k5eAaImIp3nS
mezi	mezi	k7c4
neklasifikované	klasifikovaný	k2eNgInPc4d1
jazyky	jazyk	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Počet	počet	k1gInSc1
mluvčích	mluvčí	k1gMnPc2
</s>
<s>
Sentinelština	Sentinelština	k1gFnSc1
je	být	k5eAaImIp3nS
řazena	řadit	k5eAaImNgFnS
mezi	mezi	k7c4
ohrožené	ohrožený	k2eAgInPc4d1
jazyky	jazyk	k1gInPc4
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
malému	malý	k2eAgInSc3d1
počtu	počet	k1gInSc3
mluvčích	mluvčí	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
je	být	k5eAaImIp3nS
neznámý	známý	k2eNgMnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
mluvčích	mluvčí	k1gMnPc2
je	být	k5eAaImIp3nS
100	#num#	k4
<g/>
-	-	kIx~
<g/>
250	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrubý	hrubý	k2eAgInSc1d1
odhad	odhad	k1gInSc1
vlády	vláda	k1gFnSc2
Indie	Indie	k1gFnSc2
je	být	k5eAaImIp3nS
100	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
Sentinelců	Sentinelec	k1gInPc2
nejspíše	nejspíše	k9
ale	ale	k8xC
nemluví	mluvit	k5eNaImIp3nS
jiným	jiný	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Sentinelese	Sentinelese	k1gFnSc2
language	language	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
<g/>
https://books.google.cz/books?id=qtcmm6N6LPYC&	https://books.google.cz/books?id=qtcmm6N6LPYC&	k1gFnPc1
<g/>
↑	↑	k?
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
<g/>
https://books.google.cz/books?id=dQt6XWloU10C&	https://books.google.cz/books?id=dQt6XWloU10C&	k1gFnPc1
<g/>
↑	↑	k?
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
<g/>
http://www.survivalinternational.org/campaigns/mostisolated	http://www.survivalinternational.org/campaigns/mostisolated	k1gInSc1
<g/>
↑	↑	k?
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
<g/>
http://www.ethnologue.com/show_language.asp?code=std	http://www.ethnologue.com/show_language.asp?code=std	k1gInSc1
<g/>
↑	↑	k?
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
<g/>
https://books.google.cz/books?id=ocgNvcvYc9gC&	https://books.google.cz/books?id=ocgNvcvYc9gC&	k1gFnPc1
<g/>
↑	↑	k?
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
<g/>
http://glottolog.org/resource/languoid/id/sent1241	http://glottolog.org/resource/languoid/id/sent1241	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Andamanské	Andamanský	k2eAgInPc1d1
jazyky	jazyk	k1gInPc1
</s>
<s>
North	North	k1gInSc1
Sentinel	sentinel	k1gInSc1
</s>
<s>
Sentinelci	Sentinelec	k1gMnPc1
</s>
<s>
Andamanská	Andamanský	k2eAgFnSc1d1
kreolizovaná	kreolizovaný	k2eAgFnSc1d1
hindština	hindština	k1gFnSc1
</s>
