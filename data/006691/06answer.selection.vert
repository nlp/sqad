<s>
Hibernace	hibernace	k1gFnPc1	hibernace
<g/>
,	,	kIx,	,
též	též	k9	též
hybernace	hybernace	k1gFnSc1	hybernace
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
zimní	zimní	k2eAgInSc1d1	zimní
spánek	spánek	k1gInSc1	spánek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
specifická	specifický	k2eAgFnSc1d1	specifická
reakce	reakce	k1gFnSc1	reakce
živočichů	živočich	k1gMnPc2	živočich
na	na	k7c4	na
zimní	zimní	k2eAgNnSc4d1	zimní
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
klidovém	klidový	k2eAgInSc6d1	klidový
stavu	stav	k1gInSc6	stav
a	a	k8xC	a
při	při	k7c6	při
útlumu	útlum	k1gInSc6	útlum
fyziologických	fyziologický	k2eAgInPc2d1	fyziologický
procesů	proces	k1gInPc2	proces
(	(	kIx(	(
<g/>
projevující	projevující	k2eAgNnSc4d1	projevující
se	se	k3xPyFc4	se
především	především	k6eAd1	především
snížení	snížení	k1gNnSc1	snížení
tělesné	tělesný	k2eAgFnSc2d1	tělesná
teploty	teplota	k1gFnSc2	teplota
<g/>
)	)	kIx)	)
přečkávají	přečkávat	k5eAaImIp3nP	přečkávat
ve	v	k7c6	v
vhodném	vhodný	k2eAgInSc6d1	vhodný
úkrytu	úkryt	k1gInSc6	úkryt
<g/>
,	,	kIx,	,
zvaném	zvaný	k2eAgNnSc6d1	zvané
hibernakulum	hibernakulum	k1gNnSc1	hibernakulum
<g/>
,	,	kIx,	,
nepříznivé	příznivý	k2eNgNnSc1d1	nepříznivé
zimní	zimní	k2eAgNnSc1d1	zimní
období	období	k1gNnSc1	období
<g/>
.	.	kIx.	.
</s>
