<s>
Hibernace	hibernace	k1gFnPc1	hibernace
<g/>
,	,	kIx,	,
též	též	k9	též
hybernace	hybernace	k1gFnSc1	hybernace
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
zimní	zimní	k2eAgInSc1d1	zimní
spánek	spánek	k1gInSc1	spánek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
specifická	specifický	k2eAgFnSc1d1	specifická
reakce	reakce	k1gFnSc1	reakce
živočichů	živočich	k1gMnPc2	živočich
na	na	k7c4	na
zimní	zimní	k2eAgNnSc4d1	zimní
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
klidovém	klidový	k2eAgInSc6d1	klidový
stavu	stav	k1gInSc6	stav
a	a	k8xC	a
při	při	k7c6	při
útlumu	útlum	k1gInSc6	útlum
fyziologických	fyziologický	k2eAgInPc2d1	fyziologický
procesů	proces	k1gInPc2	proces
(	(	kIx(	(
<g/>
projevující	projevující	k2eAgNnSc4d1	projevující
se	se	k3xPyFc4	se
především	především	k6eAd1	především
snížení	snížení	k1gNnSc1	snížení
tělesné	tělesný	k2eAgFnSc2d1	tělesná
teploty	teplota	k1gFnSc2	teplota
<g/>
)	)	kIx)	)
přečkávají	přečkávat	k5eAaImIp3nP	přečkávat
ve	v	k7c6	v
vhodném	vhodný	k2eAgInSc6d1	vhodný
úkrytu	úkryt	k1gInSc6	úkryt
<g/>
,	,	kIx,	,
zvaném	zvaný	k2eAgNnSc6d1	zvané
hibernakulum	hibernakulum	k1gNnSc1	hibernakulum
<g/>
,	,	kIx,	,
nepříznivé	příznivý	k2eNgNnSc1d1	nepříznivé
zimní	zimní	k2eAgNnSc1d1	zimní
období	období	k1gNnSc1	období
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
známý	známý	k1gMnSc1	známý
např.	např.	kA	např.
u	u	k7c2	u
netopýrů	netopýr	k1gMnPc2	netopýr
<g/>
,	,	kIx,	,
ježků	ježek	k1gMnPc2	ježek
<g/>
,	,	kIx,	,
křečků	křeček	k1gMnPc2	křeček
<g/>
,	,	kIx,	,
plchů	plch	k1gMnPc2	plch
nebo	nebo	k8xC	nebo
syslů	sysel	k1gMnPc2	sysel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
se	se	k3xPyFc4	se
termín	termín	k1gInSc1	termín
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
stavu	stav	k1gInSc2	stav
nízké	nízký	k2eAgFnSc2d1	nízká
teploty	teplota	k1gFnSc2	teplota
(	(	kIx(	(
<g/>
podchlazení	podchlazení	k1gNnSc2	podchlazení
<g/>
)	)	kIx)	)
pacienta	pacient	k1gMnSc2	pacient
(	(	kIx(	(
<g/>
může	moct	k5eAaImIp3nS	moct
jít	jít	k5eAaImF	jít
jak	jak	k6eAd1	jak
o	o	k7c4	o
symptom	symptom	k1gInSc4	symptom
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
o	o	k7c4	o
cílený	cílený	k2eAgInSc4d1	cílený
léčebný	léčebný	k2eAgInSc4d1	léčebný
prostředek	prostředek	k1gInSc4	prostředek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
něj	on	k3xPp3gMnSc2	on
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
nepravý	pravý	k2eNgInSc4d1	nepravý
zimní	zimní	k2eAgInSc4d1	zimní
spánek	spánek	k1gInSc4	spánek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zvířata	zvíře	k1gNnPc1	zvíře
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
neaktivní	aktivní	k2eNgFnPc1d1	neaktivní
v	v	k7c6	v
úkrytu	úkryt	k1gInSc6	úkryt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnSc1	jejich
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
nesnižuje	snižovat	k5eNaImIp3nS	snižovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
stavu	stav	k1gInSc2	stav
probouzejí	probouzet	k5eAaImIp3nP	probouzet
a	a	k8xC	a
vycházejí	vycházet	k5eAaImIp3nP	vycházet
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
přečkávají	přečkávat	k5eAaImIp3nP	přečkávat
zimu	zima	k1gFnSc4	zima
např.	např.	kA	např.
jezevci	jezevec	k1gMnPc7	jezevec
nebo	nebo	k8xC	nebo
medvědi	medvěd	k1gMnPc1	medvěd
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
aridních	aridní	k2eAgFnPc6d1	aridní
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
u	u	k7c2	u
živočichů	živočich	k1gMnPc2	živočich
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
též	též	k9	též
letní	letní	k2eAgInSc1d1	letní
spánek	spánek	k1gInSc1	spánek
(	(	kIx(	(
<g/>
estivace	estivace	k1gFnSc1	estivace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
je	být	k5eAaImIp3nS	být
odpovídajícím	odpovídající	k2eAgInSc7d1	odpovídající
stavem	stav	k1gInSc7	stav
dormance	dormanka	k1gFnSc3	dormanka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
vnější	vnější	k2eAgFnPc4d1	vnější
podmínky	podmínka	k1gFnPc4	podmínka
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
utlumení	utlumení	k1gNnSc3	utlumení
růstu	růst	k1gInSc2	růst
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
