<p>
<s>
Dorozumívání	dorozumívání	k1gNnSc1	dorozumívání
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
komunikace	komunikace	k1gFnSc1	komunikace
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc1	proces
sdělování	sdělování	k1gNnSc2	sdělování
a	a	k8xC	a
výměny	výměna	k1gFnSc2	výměna
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
,	,	kIx,	,
názorů	názor	k1gInPc2	názor
<g/>
,	,	kIx,	,
a	a	k8xC	a
pocitů	pocit	k1gInPc2	pocit
mezi	mezi	k7c7	mezi
živými	živý	k2eAgFnPc7d1	živá
bytostmi	bytost	k1gFnPc7	bytost
<g/>
,	,	kIx,	,
lidmi	člověk	k1gMnPc7	člověk
i	i	k8xC	i
živočichy	živočich	k1gMnPc7	živočich
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
společné	společný	k2eAgFnSc2d1	společná
soustavy	soustava	k1gFnSc2	soustava
symbolů	symbol	k1gInPc2	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Zvířata	zvíře	k1gNnPc1	zvíře
se	se	k3xPyFc4	se
dorozumívají	dorozumívat	k5eAaImIp3nP	dorozumívat
různými	různý	k2eAgInPc7d1	různý
signály	signál	k1gInPc7	signál
(	(	kIx(	(
<g/>
zvukovými	zvukový	k2eAgInPc7d1	zvukový
<g/>
,	,	kIx,	,
pachovými	pachový	k2eAgInPc7d1	pachový
<g/>
,	,	kIx,	,
tancem	tanec	k1gInSc7	tanec
ap.	ap.	kA	ap.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
první	první	k4xOgFnSc7	první
signální	signální	k2eAgFnSc7d1	signální
soustavou	soustava	k1gFnSc7	soustava
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
dorozumívacím	dorozumívací	k2eAgInSc7d1	dorozumívací
prostředkem	prostředek	k1gInSc7	prostředek
člověka	člověk	k1gMnSc2	člověk
však	však	k9	však
je	být	k5eAaImIp3nS	být
verbální	verbální	k2eAgFnSc1d1	verbální
komunikace	komunikace	k1gFnSc1	komunikace
(	(	kIx(	(
<g/>
jazyk	jazyk	k1gInSc1	jazyk
a	a	k8xC	a
řeč	řeč	k1gFnSc1	řeč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
druhá	druhý	k4xOgFnSc1	druhý
signální	signální	k2eAgFnSc1d1	signální
soustava	soustava	k1gFnSc1	soustava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krom	krom	k7c2	krom
mluvené	mluvený	k2eAgFnSc2d1	mluvená
řeči	řeč	k1gFnSc2	řeč
a	a	k8xC	a
písma	písmo	k1gNnSc2	písmo
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
další	další	k2eAgInPc1d1	další
systémy	systém	k1gInPc1	systém
(	(	kIx(	(
<g/>
notový	notový	k2eAgInSc1d1	notový
záznam	záznam	k1gInSc1	záznam
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
chemické	chemický	k2eAgFnSc2d1	chemická
a	a	k8xC	a
matematické	matematický	k2eAgFnSc2d1	matematická
značky	značka	k1gFnSc2	značka
<g/>
,	,	kIx,	,
dopravní	dopravní	k2eAgFnSc2d1	dopravní
značky	značka	k1gFnSc2	značka
<g/>
,	,	kIx,	,
vlajková	vlajkový	k2eAgFnSc1d1	vlajková
a	a	k8xC	a
prstová	prstový	k2eAgFnSc1d1	prstová
abeceda	abeceda	k1gFnSc1	abeceda
<g/>
,	,	kIx,	,
Morseova	Morseův	k2eAgFnSc1d1	Morseova
abeceda	abeceda	k1gFnSc1	abeceda
ap.	ap.	kA	ap.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dálkové	dálkový	k2eAgNnSc4d1	dálkové
dorozumívání	dorozumívání	k1gNnSc4	dorozumívání
slouží	sloužit	k5eAaImIp3nS	sloužit
média	médium	k1gNnPc4	médium
a	a	k8xC	a
telekomunikace	telekomunikace	k1gFnPc4	telekomunikace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dorozumívání	dorozumívání	k1gNnSc4	dorozumívání
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
měřítku	měřítko	k1gNnSc6	měřítko
je	být	k5eAaImIp3nS	být
překážkou	překážka	k1gFnSc7	překážka
různost	různost	k1gFnSc1	různost
jazyků	jazyk	k1gMnPc2	jazyk
(	(	kIx(	(
<g/>
existuje	existovat	k5eAaImIp3nS	existovat
cca	cca	kA	cca
5000	[number]	k4	5000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
řeší	řešit	k5eAaImIp3nS	řešit
pomocí	pomocí	k7c2	pomocí
světových	světový	k2eAgInPc2d1	světový
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
překladů	překlad	k1gInPc2	překlad
<g/>
,	,	kIx,	,
tlumočení	tlumočení	k1gNnSc4	tlumočení
<g/>
,	,	kIx,	,
umělých	umělý	k2eAgInPc2d1	umělý
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
jazyků	jazyk	k1gInPc2	jazyk
nebo	nebo	k8xC	nebo
strojovým	strojový	k2eAgInSc7d1	strojový
překladem	překlad	k1gInSc7	překlad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dorozumívání	dorozumívání	k1gNnSc1	dorozumívání
probíhá	probíhat	k5eAaImIp3nS	probíhat
mezi	mezi	k7c7	mezi
"	"	kIx"	"
<g/>
zdrojem	zdroj	k1gInSc7	zdroj
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
adresátem	adresát	k1gMnSc7	adresát
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
každý	každý	k3xTgMnSc1	každý
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
komunikační	komunikační	k2eAgFnSc4d1	komunikační
situaci	situace	k1gFnSc4	situace
podílí	podílet	k5eAaImIp3nS	podílet
je	on	k3xPp3gNnSc4	on
zároveň	zároveň	k6eAd1	zároveň
zdroj	zdroj	k1gInSc1	zdroj
i	i	k8xC	i
adresát	adresát	k1gMnSc1	adresát
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
komunikaci	komunikace	k1gFnSc3	komunikace
dochází	docházet	k5eAaImIp3nS	docházet
vždy	vždy	k6eAd1	vždy
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
vysíláno	vysílat	k5eAaImNgNnS	vysílat
nebo	nebo	k8xC	nebo
přijímáno	přijímat	k5eAaImNgNnS	přijímat
sdělení	sdělení	k1gNnSc1	sdělení
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
připisován	připisován	k2eAgInSc4d1	připisován
nechtěný	chtěný	k2eNgInSc4d1	nechtěný
význam	význam	k1gInSc4	význam
signálům	signál	k1gInPc3	signál
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
formě	forma	k1gFnSc6	forma
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
komunikaci	komunikace	k1gFnSc3	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Pakliže	pakliže	k8xS	pakliže
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c4	o
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tzv.	tzv.	kA	tzv.
o	o	k7c6	o
metakomunikaci	metakomunikace	k1gFnSc6	metakomunikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Složky	složka	k1gFnSc2	složka
komunikace	komunikace	k1gFnSc2	komunikace
==	==	k?	==
</s>
</p>
<p>
<s>
Médium	médium	k1gNnSc1	médium
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
informace	informace	k1gFnSc1	informace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
kanál	kanál	k1gInSc1	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Technologický	technologický	k2eAgInSc1d1	technologický
kanál	kanál	k1gInSc1	kanál
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
např.	např.	kA	např.
mobilní	mobilní	k2eAgInSc1d1	mobilní
telefon	telefon	k1gInSc1	telefon
<g/>
,	,	kIx,	,
rádio	rádio	k1gNnSc1	rádio
je	být	k5eAaImIp3nS	být
akustický	akustický	k2eAgInSc4d1	akustický
kanál	kanál	k1gInSc4	kanál
<g/>
.	.	kIx.	.
</s>
<s>
Jakákoli	jakýkoli	k3yIgFnSc1	jakýkoli
překážka	překážka	k1gFnSc1	překážka
v	v	k7c6	v
přenosu	přenos	k1gInSc6	přenos
sdělení	sdělení	k1gNnSc2	sdělení
se	se	k3xPyFc4	se
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
šum	šum	k1gInSc1	šum
<g/>
.	.	kIx.	.
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
komunikace	komunikace	k1gFnSc1	komunikace
bez	bez	k7c2	bez
šumu	šum	k1gInSc2	šum
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
psychologického	psychologický	k2eAgInSc2d1	psychologický
charakteru	charakter	k1gInSc2	charakter
(	(	kIx(	(
<g/>
špatné	špatný	k2eAgFnPc1d1	špatná
zkušenosti	zkušenost	k1gFnPc1	zkušenost
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
sociální	sociální	k2eAgFnPc1d1	sociální
role	role	k1gFnPc1	role
<g/>
,	,	kIx,	,
předsudky	předsudek	k1gInPc1	předsudek
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
zdroj	zdroj	k1gInSc1	zdroj
i	i	k9	i
adresát	adresát	k1gMnSc1	adresát
rozumí	rozumět	k5eAaImIp3nS	rozumět
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kód	kód	k1gInSc4	kód
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
např.	např.	kA	např.
i	i	k8xC	i
systém	systém	k1gInSc1	systém
znakové	znakový	k2eAgFnSc2d1	znaková
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
,	,	kIx,	,
emotikony	emotikon	k1gInPc1	emotikon
<g/>
,	,	kIx,	,
světla	světlo	k1gNnPc1	světlo
na	na	k7c6	na
přechodech	přechod	k1gInPc6	přechod
apod.	apod.	kA	apod.
Důležitý	důležitý	k2eAgInSc1d1	důležitý
je	být	k5eAaImIp3nS	být
kontext	kontext	k1gInSc1	kontext
<g/>
,	,	kIx,	,
když	když	k8xS	když
dva	dva	k4xCgMnPc1	dva
dělají	dělat	k5eAaImIp3nP	dělat
totéž	týž	k3xTgNnSc4	týž
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
totéž	týž	k3xTgNnSc1	týž
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
jinému	jiný	k2eAgNnSc3d1	jiné
prostředí	prostředí	k1gNnSc3	prostředí
<g/>
,	,	kIx,	,
jiným	jiný	k2eAgMnPc3d1	jiný
lidem	člověk	k1gMnPc3	člověk
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samotné	samotný	k2eAgFnPc1d1	samotná
složky	složka	k1gFnPc1	složka
komunikace	komunikace	k1gFnSc2	komunikace
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
sluchové	sluchový	k2eAgNnSc1d1	sluchové
</s>
</p>
<p>
<s>
vizuální	vizuální	k2eAgMnSc1d1	vizuální
–	–	k?	–
text	text	k1gInSc1	text
a	a	k8xC	a
neverbální	verbální	k2eNgFnSc1d1	neverbální
komunikace	komunikace	k1gFnSc1	komunikace
</s>
</p>
<p>
<s>
haptické	haptický	k2eAgInPc4d1	haptický
(	(	kIx(	(
<g/>
dotykové	dotykový	k2eAgInPc4d1	dotykový
neboli	neboli	k8xC	neboli
taktilní	taktilní	k2eAgInPc4d1	taktilní
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
čichové	čichový	k2eAgFnPc1d1	čichová
(	(	kIx(	(
<g/>
olfaktorické	olfaktorický	k2eAgFnPc1d1	olfaktorická
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Morseova	Morseův	k2eAgFnSc1d1	Morseova
abeceda	abeceda	k1gFnSc1	abeceda
==	==	k?	==
</s>
</p>
<p>
<s>
Také	také	k9	také
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc7d1	důležitá
technikou	technika	k1gFnSc7	technika
dorozumívání	dorozumívání	k1gNnSc2	dorozumívání
je	být	k5eAaImIp3nS	být
Morseova	Morseův	k2eAgFnSc1d1	Morseova
abeceda	abeceda	k1gFnSc1	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnPc4	její
písmena	písmeno	k1gNnPc4	písmeno
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
Američan	Američan	k1gMnSc1	Američan
Samuel	Samuel	k1gMnSc1	Samuel
Morse	Morse	k1gMnSc1	Morse
jehož	jehož	k3xOyRp3gFnSc4	jehož
pravé	pravý	k2eAgNnSc1d1	pravé
jméno	jméno	k1gNnSc1	jméno
bylo	být	k5eAaImAgNnS	být
Samuel	Samuel	k1gMnSc1	Samuel
Finlay	Finla	k2eAgFnPc4d1	Finla
Breese	Breese	k1gFnPc4	Breese
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
on	on	k3xPp3gMnSc1	on
také	také	k9	také
uskutečnil	uskutečnit	k5eAaPmAgMnS	uskutečnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
první	první	k4xOgNnSc4	první
telegrafické	telegrafický	k2eAgNnSc4d1	telegrafické
spojení	spojení	k1gNnSc4	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Morseovka	morseovka	k1gFnSc1	morseovka
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
k	k	k7c3	k
vysílání	vysílání	k1gNnSc3	vysílání
a	a	k8xC	a
příjmu	příjem	k1gInSc3	příjem
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
soustavu	soustava	k1gFnSc4	soustava
krátkých	krátký	k2eAgInPc2d1	krátký
a	a	k8xC	a
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
kombinací	kombinace	k1gFnSc7	kombinace
se	se	k3xPyFc4	se
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
písmena	písmeno	k1gNnPc4	písmeno
<g/>
,	,	kIx,	,
číslice	číslice	k1gFnPc4	číslice
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
potřebné	potřebný	k2eAgFnPc4d1	potřebná
značky	značka	k1gFnPc4	značka
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
vysílání	vysílání	k1gNnSc1	vysílání
se	se	k3xPyFc4	se
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
roku	rok	k1gInSc2	rok
1887	[number]	k4	1887
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
každému	každý	k3xTgNnSc3	každý
písmenu	písmeno	k1gNnSc3	písmeno
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
tzv.	tzv.	kA	tzv.
pomocné	pomocný	k2eAgNnSc1d1	pomocné
slovo	slovo	k1gNnSc1	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
slova	slovo	k1gNnPc1	slovo
počtem	počet	k1gInSc7	počet
svých	svůj	k3xOyFgFnPc2	svůj
slabik	slabika	k1gFnPc2	slabika
a	a	k8xC	a
délkou	délka	k1gFnSc7	délka
svých	svůj	k3xOyFgFnPc2	svůj
samohlásek	samohláska	k1gFnPc2	samohláska
hned	hned	k6eAd1	hned
připomenou	připomenout	k5eAaPmIp3nP	připomenout
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
v	v	k7c6	v
morseovce	morseovka	k1gFnSc6	morseovka
písmeno	písmeno	k1gNnSc1	písmeno
vypadá	vypadat	k5eAaImIp3nS	vypadat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
psané	psaný	k2eAgFnSc6d1	psaná
podobě	podoba	k1gFnSc6	podoba
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
tečky	tečka	k1gFnPc1	tečka
a	a	k8xC	a
čárky	čárka	k1gFnPc1	čárka
<g/>
,	,	kIx,	,
jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
písmena	písmeno	k1gNnPc1	písmeno
se	se	k3xPyFc4	se
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
šikmou	šikmý	k2eAgFnSc7d1	šikmá
čarou	čára	k1gFnSc7	čára
a	a	k8xC	a
slova	slovo	k1gNnPc1	slovo
se	se	k3xPyFc4	se
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
dvěma	dva	k4xCgFnPc7	dva
šikmými	šikmý	k2eAgFnPc7d1	šikmá
čarami	čára	k1gFnPc7	čára
<g/>
.	.	kIx.	.
</s>
<s>
Morseovka	morseovka	k1gFnSc1	morseovka
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
vysílat	vysílat	k5eAaImF	vysílat
několika	několik	k4yIc7	několik
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
baterkou	baterka	k1gFnSc7	baterka
<g/>
,	,	kIx,	,
ohněm	oheň	k1gInSc7	oheň
<g/>
,	,	kIx,	,
vlajkami	vlajka	k1gFnPc7	vlajka
<g/>
,	,	kIx,	,
rukama	ruka	k1gFnPc7	ruka
<g/>
,	,	kIx,	,
píšťalkou	píšťalka	k1gFnSc7	píšťalka
<g/>
,	,	kIx,	,
bzučákem	bzučák	k1gInSc7	bzučák
<g/>
,	,	kIx,	,
ťukáním	ťukání	k1gNnSc7	ťukání
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
komunikačním	komunikační	k2eAgInSc7d1	komunikační
prostředkem	prostředek	k1gInSc7	prostředek
pro	pro	k7c4	pro
telegraf	telegraf	k1gInSc4	telegraf
a	a	k8xC	a
dálnopis	dálnopis	k1gInSc4	dálnopis
a	a	k8xC	a
pro	pro	k7c4	pro
vysílání	vysílání	k1gNnSc4	vysílání
lodních	lodní	k2eAgInPc2d1	lodní
signálů	signál	k1gInPc2	signál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
vytlačována	vytlačovat	k5eAaImNgFnS	vytlačovat
modernějšími	moderní	k2eAgInPc7d2	modernější
technickými	technický	k2eAgInPc7d1	technický
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
její	její	k3xOp3gNnSc1	její
praktické	praktický	k2eAgNnSc1d1	praktické
používání	používání	k1gNnSc1	používání
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
oblasti	oblast	k1gFnSc6	oblast
–	–	k?	–
lodní	lodní	k2eAgFnSc6d1	lodní
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Braillovo	Braillův	k2eAgNnSc4d1	Braillovo
písmo	písmo	k1gNnSc4	písmo
==	==	k?	==
</s>
</p>
<p>
<s>
Braillovo	Braillův	k2eAgNnSc4d1	Braillovo
písmo	písmo	k1gNnSc4	písmo
pro	pro	k7c4	pro
nevidomé	nevidomý	k1gMnPc4	nevidomý
sestavil	sestavit	k5eAaPmAgMnS	sestavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1824	[number]	k4	1824
patnáctiletý	patnáctiletý	k2eAgMnSc1d1	patnáctiletý
Francouz	Francouz	k1gMnSc1	Francouz
Louis	Louis	k1gMnSc1	Louis
Braille	Braille	k1gInSc4	Braille
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
sám	sám	k3xTgMnSc1	sám
oslepl	oslepnout	k5eAaPmAgMnS	oslepnout
ve	v	k7c6	v
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
znaky	znak	k1gInPc1	znak
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořen	k2eAgInPc1d1	tvořen
kombinací	kombinace	k1gFnSc7	kombinace
šesti	šest	k4xCc2	šest
bodů	bod	k1gInPc2	bod
plasticky	plasticky	k6eAd1	plasticky
vystupujících	vystupující	k2eAgMnPc2d1	vystupující
z	z	k7c2	z
papíru	papír	k1gInSc2	papír
či	či	k8xC	či
jiného	jiný	k2eAgInSc2d1	jiný
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Braille	Braille	k1gInSc1	Braille
se	se	k3xPyFc4	se
inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
dvanáctibodovým	dvanáctibodový	k2eAgNnSc7d1	dvanáctibodové
písmem	písmo	k1gNnSc7	písmo
vytvořeným	vytvořený	k2eAgNnSc7d1	vytvořené
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
předtím	předtím	k6eAd1	předtím
kapitánem	kapitán	k1gMnSc7	kapitán
Charlesem	Charles	k1gMnSc7	Charles
Barbierem	Barbier	k1gMnSc7	Barbier
ten	ten	k3xDgInSc4	ten
svůj	svůj	k3xOyFgInSc4	svůj
vynález	vynález	k1gInSc4	vynález
nazval	nazvat	k5eAaPmAgMnS	nazvat
"	"	kIx"	"
<g/>
noční	noční	k2eAgNnSc1d1	noční
písmo	písmo	k1gNnSc1	písmo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
písmo	písmo	k1gNnSc1	písmo
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
navrženo	navrhnout	k5eAaPmNgNnS	navrhnout
pro	pro	k7c4	pro
vojáky	voják	k1gMnPc4	voják
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
v	v	k7c6	v
zákopech	zákop	k1gInPc6	zákop
předávat	předávat	k5eAaImF	předávat
údaje	údaj	k1gInPc4	údaj
a	a	k8xC	a
nemuseli	muset	k5eNaImAgMnP	muset
svítit	svítit	k5eAaImF	svítit
<g/>
.	.	kIx.	.
</s>
<s>
Sestávalo	sestávat	k5eAaImAgNnS	sestávat
z	z	k7c2	z
12	[number]	k4	12
vypuklých	vypuklý	k2eAgInPc2d1	vypuklý
bodů	bod	k1gInPc2	bod
v	v	k7c6	v
různé	různý	k2eAgFnSc6d1	různá
kombinaci	kombinace	k1gFnSc6	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
však	však	k9	však
pro	pro	k7c4	pro
vojáky	voják	k1gMnPc4	voják
příliš	příliš	k6eAd1	příliš
složitý	složitý	k2eAgMnSc1d1	složitý
a	a	k8xC	a
armáda	armáda	k1gFnSc1	armáda
ho	on	k3xPp3gInSc4	on
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
Louis	Louis	k1gMnSc1	Louis
Braille	Braill	k1gMnSc4	Braill
si	se	k3xPyFc3	se
rychle	rychle	k6eAd1	rychle
uvědomil	uvědomit	k5eAaPmAgMnS	uvědomit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
užitečný	užitečný	k2eAgInSc1d1	užitečný
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
takovýto	takovýto	k3xDgInSc1	takovýto
systém	systém	k1gInSc1	systém
vyvýšených	vyvýšený	k2eAgInPc2d1	vyvýšený
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
několik	několik	k4yIc4	několik
dalších	další	k2eAgInPc2d1	další
měsíců	měsíc	k1gInPc2	měsíc
experimentoval	experimentovat	k5eAaImAgInS	experimentovat
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
systémy	systém	k1gInPc7	systém
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
nalezl	naleznout	k5eAaPmAgInS	naleznout
takový	takový	k3xDgInSc1	takový
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
potřeboval	potřebovat	k5eAaImAgInS	potřebovat
pouze	pouze	k6eAd1	pouze
6	[number]	k4	6
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
systém	systém	k1gInSc4	systém
zdokonaloval	zdokonalovat	k5eAaImAgMnS	zdokonalovat
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
dalších	další	k2eAgNnPc2d1	další
let	léto	k1gNnPc2	léto
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
zvláštní	zvláštní	k2eAgInPc4d1	zvláštní
kódy	kód	k1gInPc4	kód
pro	pro	k7c4	pro
matematiku	matematika	k1gFnSc4	matematika
a	a	k8xC	a
notový	notový	k2eAgInSc4d1	notový
záznam	záznam	k1gInSc4	záznam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1827	[number]	k4	1827
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
první	první	k4xOgFnSc1	první
kniha	kniha	k1gFnSc1	kniha
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
slepeckém	slepecký	k2eAgNnSc6d1	slepecké
písmu	písmo	k1gNnSc6	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
nový	nový	k2eAgInSc1d1	nový
systém	systém	k1gInSc1	systém
neuchytil	uchytit	k5eNaPmAgInS	uchytit
okamžitě	okamžitě	k6eAd1	okamžitě
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
konzervativní	konzervativní	k2eAgMnPc1d1	konzervativní
učitelé	učitel	k1gMnPc1	učitel
měli	mít	k5eAaImAgMnP	mít
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
nedůvěru	nedůvěra	k1gFnSc4	nedůvěra
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Braillovo	Braillův	k2eAgNnSc1d1	Braillovo
písmo	písmo	k1gNnSc1	písmo
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
publikováno	publikovat	k5eAaBmNgNnS	publikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1829	[number]	k4	1829
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1837	[number]	k4	1837
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
zdokonalená	zdokonalený	k2eAgFnSc1d1	zdokonalená
verze	verze	k1gFnSc1	verze
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
verze	verze	k1gFnSc1	verze
Braillova	Braillův	k2eAgNnSc2d1	Braillovo
slepeckého	slepecký	k2eAgNnSc2d1	slepecké
písma	písmo	k1gNnSc2	písmo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Louis	Louis	k1gMnSc1	Louis
Braille	Braille	k1gFnSc2	Braille
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
učitelem	učitel	k1gMnSc7	učitel
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
býval	bývat	k5eAaImAgMnS	bývat
studentem	student	k1gMnSc7	student
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
svými	svůj	k3xOyFgMnPc7	svůj
žáky	žák	k1gMnPc4	žák
respektován	respektován	k2eAgInSc4d1	respektován
a	a	k8xC	a
obdivován	obdivován	k2eAgInSc4d1	obdivován
<g/>
.	.	kIx.	.
</s>
<s>
Nedožil	dožít	k5eNaPmAgInS	dožít
se	se	k3xPyFc4	se
však	však	k9	však
rozšíření	rozšíření	k1gNnSc1	rozšíření
svého	svůj	k3xOyFgInSc2	svůj
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1852	[number]	k4	1852
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
43	[number]	k4	43
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
tuberkulózu	tuberkulóza	k1gFnSc4	tuberkulóza
<g/>
.	.	kIx.	.
</s>
<s>
Braillův	Braillův	k2eAgInSc1d1	Braillův
systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1854	[number]	k4	1854
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jej	on	k3xPp3gMnSc4	on
užívali	užívat	k5eAaImAgMnP	užívat
slepci	slepec	k1gMnPc1	slepec
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
Braillovo	Braillův	k2eAgNnSc1d1	Braillovo
slepecké	slepecký	k2eAgNnSc1d1	slepecké
písmo	písmo	k1gNnSc1	písmo
dostalo	dostat	k5eAaPmAgNnS	dostat
přes	přes	k7c4	přes
německou	německý	k2eAgFnSc4d1	německá
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
oblast	oblast	k1gFnSc4	oblast
do	do	k7c2	do
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několikaletém	několikaletý	k2eAgNnSc6d1	několikaleté
váhání	váhání	k1gNnSc6	váhání
spojeném	spojený	k2eAgNnSc6d1	spojené
s	s	k7c7	s
četnými	četný	k2eAgFnPc7d1	četná
polemikami	polemika	k1gFnPc7	polemika
Braillův	Braillův	k2eAgInSc4d1	Braillův
znakový	znakový	k2eAgInSc4d1	znakový
systém	systém	k1gInSc4	systém
jednoznačně	jednoznačně	k6eAd1	jednoznačně
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nemalé	malý	k2eNgFnSc6d1	nemalá
míře	míra	k1gFnSc6	míra
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
přispěli	přispět	k5eAaPmAgMnP	přispět
i	i	k9	i
sami	sám	k3xTgMnPc1	sám
nevidomí	nevidomý	k1gMnPc1	nevidomý
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
přijímali	přijímat	k5eAaImAgMnP	přijímat
Braillovo	Braillův	k2eAgNnSc4d1	Braillovo
písmo	písmo	k1gNnSc4	písmo
s	s	k7c7	s
neskrývaným	skrývaný	k2eNgInSc7d1	neskrývaný
zájmem	zájem	k1gInSc7	zájem
a	a	k8xC	a
radostí	radost	k1gFnSc7	radost
<g/>
,	,	kIx,	,
učili	učit	k5eAaImAgMnP	učit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
navzájem	navzájem	k6eAd1	navzájem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
samých	samý	k3xTgInPc6	samý
začátcích	začátek	k1gInPc6	začátek
se	se	k3xPyFc4	se
písmo	písmo	k1gNnSc1	písmo
vytvářelo	vytvářet	k5eAaImAgNnS	vytvářet
zcela	zcela	k6eAd1	zcela
ručně	ručně	k6eAd1	ručně
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
pomůcek	pomůcka	k1gFnPc2	pomůcka
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
"	"	kIx"	"
<g/>
psací	psací	k2eAgNnSc4d1	psací
<g/>
"	"	kIx"	"
zařízení	zařízení	k1gNnSc4	zařízení
sestávající	sestávající	k2eAgFnSc2d1	sestávající
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
kovových	kovový	k2eAgFnPc2d1	kovová
desek	deska	k1gFnPc2	deska
spojených	spojený	k2eAgFnPc2d1	spojená
navzájem	navzájem	k6eAd1	navzájem
"	"	kIx"	"
<g/>
pantem	pant	k1gInSc7	pant
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
desce	deska	k1gFnSc6	deska
byla	být	k5eAaImAgFnS	být
síť	síť	k1gFnSc1	síť
průchozích	průchozí	k2eAgFnPc2d1	průchozí
děr	děra	k1gFnPc2	děra
<g/>
,	,	kIx,	,
ve	v	k7c6	v
spodní	spodní	k2eAgFnPc1d1	spodní
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
síti	síť	k1gFnSc6	síť
zhotoveny	zhotoven	k2eAgInPc1d1	zhotoven
pouze	pouze	k6eAd1	pouze
mělké	mělký	k2eAgInPc1d1	mělký
důlky	důlek	k1gInPc1	důlek
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
desky	deska	k1gFnPc4	deska
se	se	k3xPyFc4	se
vložil	vložit	k5eAaPmAgInS	vložit
papír	papír	k1gInSc1	papír
a	a	k8xC	a
vhodným	vhodný	k2eAgInSc7d1	vhodný
nástrojem	nástroj	k1gInSc7	nástroj
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
otvorů	otvor	k1gInPc2	otvor
v	v	k7c6	v
papíru	papír	k1gInSc6	papír
vytlačily	vytlačit	k5eAaPmAgInP	vytlačit
příslušné	příslušný	k2eAgInPc1d1	příslušný
důlky	důlek	k1gInPc1	důlek
(	(	kIx(	(
<g/>
výstupky	výstupek	k1gInPc1	výstupek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgFnP	být
k	k	k7c3	k
ražení	ražení	k1gNnSc3	ražení
šestibodového	šestibodový	k2eAgNnSc2d1	šestibodové
Braillova	Braillův	k2eAgNnSc2d1	Braillovo
písma	písmo	k1gNnSc2	písmo
zkonstruovány	zkonstruovat	k5eAaPmNgInP	zkonstruovat
mechanické	mechanický	k2eAgInPc1d1	mechanický
psací	psací	k2eAgInPc1d1	psací
stroje	stroj	k1gInPc1	stroj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Počítačová	počítačový	k2eAgFnSc1d1	počítačová
technika	technika	k1gFnSc1	technika
však	však	k9	však
pronikla	proniknout	k5eAaPmAgFnS	proniknout
i	i	k9	i
sem	sem	k6eAd1	sem
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
softwarové	softwarový	k2eAgInPc1d1	softwarový
programy	program	k1gInPc1	program
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
převádějí	převádět	k5eAaImIp3nP	převádět
používaná	používaný	k2eAgNnPc1d1	používané
písma	písmo	k1gNnPc1	písmo
do	do	k7c2	do
Braillova	Braillův	k2eAgInSc2d1	Braillův
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dnes	dnes	k6eAd1	dnes
Braillova	Braillův	k2eAgFnSc1d1	Braillova
soustava	soustava	k1gFnSc1	soustava
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
63	[number]	k4	63
znaky	znak	k1gInPc7	znak
pro	pro	k7c4	pro
písmena	písmeno	k1gNnPc4	písmeno
<g/>
,	,	kIx,	,
interpunkční	interpunkční	k2eAgNnPc4d1	interpunkční
znaménka	znaménko	k1gNnPc4	znaménko
<g/>
,	,	kIx,	,
číslice	číslice	k1gFnPc4	číslice
a	a	k8xC	a
hudební	hudební	k2eAgFnPc1d1	hudební
značky	značka	k1gFnPc1	značka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
používána	používat	k5eAaImNgFnS	používat
téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
přizpůsobena	přizpůsoben	k2eAgFnSc1d1	přizpůsobena
téměř	téměř	k6eAd1	téměř
všem	všecek	k3xTgMnPc3	všecek
jazykům	jazyk	k1gMnPc3	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Francie	Francie	k1gFnSc1	Francie
ocenila	ocenit	k5eAaPmAgFnS	ocenit
Braillovy	Braillův	k2eAgFnPc4d1	Braillova
zásluhy	zásluha	k1gFnPc4	zásluha
<g/>
:	:	kIx,	:
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
převezla	převézt	k5eAaPmAgFnS	převézt
jeho	jeho	k3xOp3gInPc4	jeho
ostatky	ostatek	k1gInPc4	ostatek
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
uložila	uložit	k5eAaPmAgFnS	uložit
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Pantheonu	Pantheon	k1gInSc6	Pantheon
mezi	mezi	k7c4	mezi
ostatní	ostatní	k2eAgMnPc4d1	ostatní
velké	velký	k2eAgMnPc4d1	velký
Francouze	Francouz	k1gMnPc4	Francouz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Znaková	znakový	k2eAgFnSc1d1	znaková
řeč	řeč	k1gFnSc1	řeč
==	==	k?	==
</s>
</p>
<p>
<s>
Znaková	znakový	k2eAgFnSc1d1	znaková
řeč	řeč	k1gFnSc1	řeč
je	být	k5eAaImIp3nS	být
širší	široký	k2eAgInSc4d2	širší
pojem	pojem	k1gInSc4	pojem
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
sobě	sebe	k3xPyFc6	sebe
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
různé	různý	k2eAgInPc4d1	různý
dorozumívací	dorozumívací	k2eAgInPc4d1	dorozumívací
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
také	také	k9	také
znakovanou	znakovaný	k2eAgFnSc4d1	znakovaná
češtinu	čeština	k1gFnSc4	čeština
a	a	k8xC	a
znakový	znakový	k2eAgInSc4d1	znakový
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Díváte	dívat	k5eAaImIp2nP	dívat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
na	na	k7c4	na
pohyb	pohyb	k1gInSc4	pohyb
úst	ústa	k1gNnPc2	ústa
<g/>
,	,	kIx,	,
vidíte	vidět	k5eAaImIp2nP	vidět
mluvní	mluvní	k2eAgInPc4d1	mluvní
obrazy	obraz	k1gInPc4	obraz
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
pouze	pouze	k6eAd1	pouze
doplňovány	doplňován	k2eAgInPc4d1	doplňován
doprovodnými	doprovodný	k2eAgInPc7d1	doprovodný
znaky	znak	k1gInPc7	znak
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
útvar	útvar	k1gInSc1	útvar
ne	ne	k9	ne
příliš	příliš	k6eAd1	příliš
výstižně	výstižně	k6eAd1	výstižně
nazýván	nazývat	k5eAaImNgInS	nazývat
také	také	k9	také
znakovaná	znakovaný	k2eAgFnSc1d1	znakovaná
čeština	čeština	k1gFnSc1	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nápovědu	nápověda	k1gFnSc4	nápověda
při	při	k7c6	při
odezírání	odezírání	k1gNnSc6	odezírání
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
mluvenou	mluvený	k2eAgFnSc4d1	mluvená
češtinu	čeština	k1gFnSc4	čeština
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
někdy	někdy	k6eAd1	někdy
vyslovovanou	vyslovovaný	k2eAgFnSc4d1	vyslovovaná
bez	bez	k7c2	bez
hlasu	hlas	k1gInSc2	hlas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
doplněnou	doplněná	k1gFnSc4	doplněná
izolovanými	izolovaný	k2eAgInPc7d1	izolovaný
znaky	znak	k1gInPc7	znak
<g/>
.	.	kIx.	.
</s>
<s>
Abyste	aby	kYmCp2nP	aby
tedy	tedy	k9	tedy
správně	správně	k6eAd1	správně
porozuměli	porozumět	k5eAaPmAgMnP	porozumět
sdělení	sdělení	k1gNnSc4	sdělení
ve	v	k7c6	v
znakované	znakovaný	k2eAgFnSc6d1	znakovaná
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
musíte	muset	k5eAaImIp2nP	muset
umět	umět	k5eAaImF	umět
dobře	dobře	k6eAd1	dobře
česky	česky	k6eAd1	česky
a	a	k8xC	a
musíte	muset	k5eAaImIp2nP	muset
být	být	k5eAaImF	být
schopni	schopen	k2eAgMnPc1d1	schopen
odezírat	odezírat	k5eAaImF	odezírat
a	a	k8xC	a
znát	znát	k5eAaImF	znát
použité	použitý	k2eAgInPc4d1	použitý
znaky	znak	k1gInPc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
doprovodné	doprovodný	k2eAgInPc4d1	doprovodný
znaky	znak	k1gInPc4	znak
vám	vy	k3xPp2nPc3	vy
jen	jen	k9	jen
pomáhají	pomáhat	k5eAaImIp3nP	pomáhat
při	při	k7c6	při
výběru	výběr	k1gInSc6	výběr
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
máte	mít	k5eAaImIp2nP	mít
dešifrovat	dešifrovat	k5eAaBmF	dešifrovat
<g/>
.	.	kIx.	.
</s>
<s>
Znakovaná	Znakovaný	k2eAgFnSc1d1	Znakovaná
čeština	čeština	k1gFnSc1	čeština
je	být	k5eAaImIp3nS	být
uměle	uměle	k6eAd1	uměle
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
kompromisem	kompromis	k1gInSc7	kompromis
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
neznají	znát	k5eNaImIp3nP	znát
gramatiku	gramatika	k1gFnSc4	gramatika
znakové	znakový	k2eAgFnSc2d1	znaková
řeči	řeč	k1gFnSc2	řeč
a	a	k8xC	a
chtějí	chtít	k5eAaImIp3nP	chtít
používat	používat	k5eAaImF	používat
znaky	znak	k1gInPc4	znak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Znakový	znakový	k2eAgInSc1d1	znakový
jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
přirozený	přirozený	k2eAgInSc1d1	přirozený
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
jakémkoli	jakýkoli	k3yIgInSc6	jakýkoli
mluveném	mluvený	k2eAgInSc6d1	mluvený
jazyce	jazyk	k1gInSc6	jazyk
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
gramatiku	gramatika	k1gFnSc4	gramatika
<g/>
.	.	kIx.	.
</s>
<s>
Gramatika	gramatika	k1gFnSc1	gramatika
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
vyjádřena	vyjádřit	k5eAaPmNgFnS	vyjádřit
prostředky	prostředek	k1gInPc7	prostředek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
v	v	k7c6	v
mluvených	mluvený	k2eAgInPc6d1	mluvený
jazycích	jazyk	k1gInPc6	jazyk
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Prstová	prstový	k2eAgFnSc1d1	prstová
abeceda	abeceda	k1gFnSc1	abeceda
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
prstové	prstový	k2eAgFnSc2d1	prstová
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Zvládnutí	zvládnutí	k1gNnSc1	zvládnutí
plynulého	plynulý	k2eAgNnSc2d1	plynulé
používání	používání	k1gNnSc2	používání
prstové	prstový	k2eAgFnSc2d1	prstová
abecedy	abeceda	k1gFnSc2	abeceda
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
ruku	ruka	k1gFnSc4	ruka
však	však	k9	však
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
delší	dlouhý	k2eAgInSc4d2	delší
nácvik	nácvik	k1gInSc4	nácvik
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
začíná	začínat	k5eAaImIp3nS	začínat
procvičováním	procvičování	k1gNnSc7	procvičování
ohebnosti	ohebnost	k1gFnSc2	ohebnost
prstů	prst	k1gInPc2	prst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
obtížnější	obtížný	k2eAgMnSc1d2	obtížnější
je	být	k5eAaImIp3nS	být
však	však	k9	však
dešifrování	dešifrování	k1gNnSc1	dešifrování
této	tento	k3xDgFnSc2	tento
formy	forma	k1gFnSc2	forma
sdělení	sdělení	k1gNnSc2	sdělení
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
neslyšící	slyšící	k2eNgMnPc1d1	neslyšící
lidé	člověk	k1gMnPc1	člověk
u	u	k7c2	u
nás	my	k3xPp1nPc4	my
dávají	dávat	k5eAaImIp3nP	dávat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
potřeby	potřeba	k1gFnSc2	potřeba
přednost	přednost	k1gFnSc4	přednost
použití	použití	k1gNnSc4	použití
abecedy	abeceda	k1gFnSc2	abeceda
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
ruce	ruka	k1gFnPc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Psaní	psaní	k1gNnSc1	psaní
pomocí	pomocí	k7c2	pomocí
abecedy	abeceda	k1gFnSc2	abeceda
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
ruce	ruka	k1gFnPc4	ruka
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
o	o	k7c4	o
něco	něco	k3yInSc4	něco
pomalejší	pomalý	k2eAgMnSc1d2	pomalejší
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
tu	ten	k3xDgFnSc4	ten
výhodu	výhoda	k1gFnSc4	výhoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
čitelnější	čitelný	k2eAgFnSc1d2	čitelnější
i	i	k9	i
pro	pro	k7c4	pro
méně	málo	k6eAd2	málo
zkušené	zkušený	k2eAgMnPc4d1	zkušený
uživatele	uživatel	k1gMnPc4	uživatel
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
známa	znám	k2eAgFnSc1d1	známa
i	i	k8xC	i
mezi	mezi	k7c7	mezi
slyšícími	slyšící	k2eAgMnPc7d1	slyšící
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
snadné	snadný	k2eAgNnSc1d1	snadné
naučit	naučit	k5eAaPmF	naučit
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
i	i	k9	i
během	během	k7c2	během
několika	několik	k4yIc2	několik
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
laiky	laik	k1gMnPc4	laik
vhodnější	vhodný	k2eAgFnSc1d2	vhodnější
než	než	k8xS	než
abeceda	abeceda	k1gFnSc1	abeceda
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
výhodou	výhoda	k1gFnSc7	výhoda
prstové	prstový	k2eAgFnSc2d1	prstová
abecedy	abeceda	k1gFnSc2	abeceda
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
ruce	ruka	k1gFnPc4	ruka
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gNnSc1	její
podoba	podoba	k1gFnSc1	podoba
s	s	k7c7	s
latinskými	latinský	k2eAgNnPc7d1	latinské
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
abecedy	abeceda	k1gFnSc2	abeceda
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
ruce	ruka	k1gFnPc4	ruka
jsou	být	k5eAaImIp3nP	být
tvary	tvar	k1gInPc4	tvar
písmen	písmeno	k1gNnPc2	písmeno
snadno	snadno	k6eAd1	snadno
rozpoznatelné	rozpoznatelný	k2eAgInPc1d1	rozpoznatelný
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
v	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
praxi	praxe	k1gFnSc6	praxe
ani	ani	k8xC	ani
tyto	tento	k3xDgInPc1	tento
rozdíly	rozdíl	k1gInPc1	rozdíl
nečiní	činit	k5eNaImIp3nP	činit
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
prstové	prstový	k2eAgFnSc2d1	prstová
abecedy	abeceda	k1gFnSc2	abeceda
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
ruce	ruka	k1gFnPc4	ruka
potíže	potíž	k1gFnPc4	potíž
<g/>
.	.	kIx.	.
</s>
<s>
Prstová	prstový	k2eAgFnSc1d1	prstová
abeceda	abeceda	k1gFnSc1	abeceda
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
ruce	ruka	k1gFnPc4	ruka
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zobrazena	zobrazit	k5eAaPmNgNnP	zobrazit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
člověku	člověk	k1gMnSc3	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
právě	právě	k6eAd1	právě
používá	používat	k5eAaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
u	u	k7c2	u
písmen	písmeno	k1gNnPc2	písmeno
H	H	kA	H
<g/>
,	,	kIx,	,
CH	Ch	kA	Ch
<g/>
,	,	kIx,	,
N	N	kA	N
<g/>
,	,	kIx,	,
Y	Y	kA	Y
a	a	k8xC	a
Z	z	k7c2	z
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
znázornit	znázornit	k5eAaPmF	znázornit
pohled	pohled	k1gInSc4	pohled
z	z	k7c2	z
opačné	opačný	k2eAgFnSc2d1	opačná
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
uživatel	uživatel	k1gMnSc1	uživatel
prstové	prstový	k2eAgFnSc2d1	prstová
abecedy	abeceda	k1gFnSc2	abeceda
obvykle	obvykle	k6eAd1	obvykle
používá	používat	k5eAaImIp3nS	používat
ke	k	k7c3	k
složitějším	složitý	k2eAgFnPc3d2	složitější
polohám	poloha	k1gFnPc3	poloha
prstů	prst	k1gInPc2	prst
dominantní	dominantní	k2eAgFnSc4d1	dominantní
ruku	ruka	k1gFnSc4	ruka
(	(	kIx(	(
<g/>
v	v	k7c6	v
našem	náš	k3xOp1gInSc6	náš
případě	případ	k1gInSc6	případ
pravou	pravý	k2eAgFnSc4d1	pravá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeví	jevit	k5eAaImIp3nS	jevit
se	se	k3xPyFc4	se
partnerovi	partner	k1gMnSc3	partner
některá	některý	k3yIgNnPc1	některý
písmena	písmeno	k1gNnPc1	písmeno
zrcadlově	zrcadlově	k6eAd1	zrcadlově
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
není	být	k5eNaImIp3nS	být
na	na	k7c4	na
závadu	závada	k1gFnSc4	závada
čitelnosti	čitelnost	k1gFnSc2	čitelnost
písmen	písmeno	k1gNnPc2	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Diakritická	diakritický	k2eAgNnPc1d1	diakritické
znaménka	znaménko	k1gNnPc1	znaménko
(	(	kIx(	(
<g/>
čárky	čárka	k1gFnSc2	čárka
a	a	k8xC	a
háčky	háček	k1gInPc1	háček
nad	nad	k7c7	nad
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
u	u	k7c2	u
abecedy	abeceda	k1gFnSc2	abeceda
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
ruce	ruka	k1gFnPc4	ruka
naznačena	naznačit	k5eAaPmNgFnS	naznačit
pohybem	pohyb	k1gInSc7	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
čárce	čárka	k1gFnSc6	čárka
se	se	k3xPyFc4	se
ruce	ruka	k1gFnPc1	ruka
s	s	k7c7	s
naznačeným	naznačený	k2eAgNnSc7d1	naznačené
písmenem	písmeno	k1gNnSc7	písmeno
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
seshora	seshora	k6eAd1	seshora
dolů	dolů	k6eAd1	dolů
<g/>
,	,	kIx,	,
při	při	k7c6	při
naznačení	naznačení	k1gNnSc6	naznačení
háčku	háček	k1gInSc2	háček
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
zhoupnou	zhoupnout	k5eAaPmIp3nP	zhoupnout
<g/>
"	"	kIx"	"
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
někteří	některý	k3yIgMnPc1	některý
uživatelé	uživatel	k1gMnPc1	uživatel
prstové	prstový	k2eAgFnSc2d1	prstová
abecedy	abeceda	k1gFnSc2	abeceda
tato	tento	k3xDgNnPc1	tento
diakritická	diakritický	k2eAgNnPc1d1	diakritické
znaménka	znaménko	k1gNnPc1	znaménko
naznačí	naznačit	k5eAaPmIp3nP	naznačit
pomocí	pomocí	k7c2	pomocí
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
dorozumívání	dorozumívání	k1gNnSc2	dorozumívání
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
