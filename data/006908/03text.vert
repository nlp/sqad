<s>
Peroxid	peroxid	k1gInSc1	peroxid
barnatý	barnatý	k2eAgInSc1d1	barnatý
je	být	k5eAaImIp3nS	být
sloučenina	sloučenina	k1gFnSc1	sloučenina
barya	baryum	k1gNnSc2	baryum
a	a	k8xC	a
kyslíku	kyslík	k1gInSc2	kyslík
se	s	k7c7	s
vzorcem	vzorec	k1gInSc7	vzorec
BaO	BaO	k1gFnSc1	BaO
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Peroxid	peroxid	k1gInSc1	peroxid
barnatý	barnatý	k2eAgInSc1d1	barnatý
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
peroxidovou	peroxidový	k2eAgFnSc4d1	peroxidová
skupinu	skupina	k1gFnSc4	skupina
O	o	k7c6	o
<g/>
22	[number]	k4	22
<g/>
−	−	k?	−
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
atomy	atom	k1gInPc1	atom
kyslíku	kyslík	k1gInSc2	kyslík
vázány	vázat	k5eAaImNgInP	vázat
vzájemně	vzájemně	k6eAd1	vzájemně
a	a	k8xC	a
každý	každý	k3xTgMnSc1	každý
pak	pak	k6eAd1	pak
na	na	k7c4	na
atom	atom	k1gInSc4	atom
barya	baryum	k1gNnSc2	baryum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tuhé	tuhý	k2eAgFnSc6d1	tuhá
fázi	fáze	k1gFnSc6	fáze
má	můj	k3xOp1gFnSc1	můj
sloučenina	sloučenina	k1gFnSc1	sloučenina
stejnou	stejný	k2eAgFnSc4d1	stejná
strukturu	struktura	k1gFnSc4	struktura
jako	jako	k8xS	jako
karbid	karbid	k1gInSc4	karbid
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
<g/>
,	,	kIx,	,
CaC	CaC	k1gFnSc1	CaC
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
šedobílá	šedobílý	k2eAgFnSc1d1	šedobílá
látka	látka	k1gFnSc1	látka
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejrozšířenější	rozšířený	k2eAgInPc4d3	nejrozšířenější
anorganické	anorganický	k2eAgInPc4d1	anorganický
peroxidy	peroxid	k1gInPc4	peroxid
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
silným	silný	k2eAgNnSc7d1	silné
oxidačním	oxidační	k2eAgNnSc7d1	oxidační
činidlem	činidlo	k1gNnSc7	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
bělidlo	bělidlo	k1gNnSc1	bělidlo
<g/>
,	,	kIx,	,
také	také	k9	také
v	v	k7c6	v
zábavní	zábavní	k2eAgFnSc6d1	zábavní
pyrotechnice	pyrotechnika	k1gFnSc6	pyrotechnika
jako	jako	k8xS	jako
oxidant	oxidant	k1gInSc4	oxidant
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navíc	navíc	k6eAd1	navíc
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
živě	živě	k6eAd1	živě
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
jiné	jiný	k2eAgFnPc1d1	jiná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
barya	baryum	k1gNnSc2	baryum
<g/>
.	.	kIx.	.
</s>
<s>
Peroxid	peroxid	k1gInSc1	peroxid
barnatý	barnatý	k2eAgInSc1d1	barnatý
vzniká	vznikat	k5eAaImIp3nS	vznikat
zvratnou	zvratný	k2eAgFnSc7d1	zvratná
absorpcí	absorpce	k1gFnSc7	absorpce
O2	O2	k1gFnSc2	O2
oxidem	oxid	k1gInSc7	oxid
barnatým	barnatý	k2eAgInSc7d1	barnatý
<g/>
.	.	kIx.	.
</s>
<s>
Kyslík	kyslík	k1gInSc1	kyslík
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
zpět	zpět	k6eAd1	zpět
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
nad	nad	k7c7	nad
500	[number]	k4	500
°	°	k?	°
<g/>
C.	C.	kA	C.
2	[number]	k4	2
BaO	BaO	k1gFnPc1	BaO
+	+	kIx~	+
O2	O2	k1gFnSc1	O2
⇌	⇌	k?	⇌
2	[number]	k4	2
BaO	BaO	k1gFnSc1	BaO
<g/>
2	[number]	k4	2
Tato	tento	k3xDgFnSc1	tento
reakce	reakce	k1gFnSc1	reakce
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
překonaného	překonaný	k2eAgInSc2d1	překonaný
<g/>
)	)	kIx)	)
Brinova	Brinův	k2eAgInSc2d1	Brinův
procesu	proces	k1gInSc2	proces
separace	separace	k1gFnSc2	separace
kyslíku	kyslík	k1gInSc2	kyslík
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
oxidy	oxid	k1gInPc1	oxid
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
oxid	oxid	k1gInSc1	oxid
sodný	sodný	k2eAgInSc1d1	sodný
Na	na	k7c6	na
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
chovají	chovat	k5eAaImIp3nP	chovat
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Peroxid	peroxid	k1gInSc1	peroxid
barnatý	barnatý	k2eAgInSc1d1	barnatý
lze	lze	k6eAd1	lze
pomocí	pomocí	k7c2	pomocí
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
převést	převést	k5eAaPmF	převést
na	na	k7c4	na
peroxid	peroxid	k1gInSc4	peroxid
vodíku	vodík	k1gInSc2	vodík
<g/>
:	:	kIx,	:
BaO	BaO	k1gFnSc1	BaO
<g/>
2	[number]	k4	2
+	+	kIx~	+
H2SO4	H2SO4	k1gFnSc1	H2SO4
→	→	k?	→
H2O2	H2O2	k1gFnSc1	H2O2
+	+	kIx~	+
BaSO	basa	k1gFnSc5	basa
<g/>
4	[number]	k4	4
Nerozpustný	rozpustný	k2eNgInSc4d1	nerozpustný
síran	síran	k1gInSc4	síran
barnatý	barnatý	k2eAgInSc4d1	barnatý
se	se	k3xPyFc4	se
ze	z	k7c2	z
směsi	směs	k1gFnSc2	směs
odfiltruje	odfiltrovat	k5eAaPmIp3nS	odfiltrovat
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
barnatý	barnatý	k2eAgInSc4d1	barnatý
Peroxid	peroxid	k1gInSc4	peroxid
vodíku	vodík	k1gInSc2	vodík
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Barium	barium	k1gNnSc1	barium
peroxide	peroxid	k1gInSc5	peroxid
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
VOHLÍDAL	VOHLÍDAL	kA	VOHLÍDAL
<g/>
,	,	kIx,	,
JIŘÍ	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
ŠTULÍK	štulík	k1gInSc1	štulík
<g/>
,	,	kIx,	,
KAREL	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
JULÁK	JULÁK	kA	JULÁK
<g/>
,	,	kIx,	,
ALOIS	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
a	a	k8xC	a
analytické	analytický	k2eAgFnPc1d1	analytická
tabulky	tabulka	k1gFnPc1	tabulka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7169	[number]	k4	7169
<g/>
-	-	kIx~	-
<g/>
855	[number]	k4	855
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
MSDS	MSDS	kA	MSDS
at	at	k?	at
jtbaker	jtbaker	k1gInSc1	jtbaker
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
