<s>
Noricum	Noricum	k1gNnSc1	Noricum
bylo	být	k5eAaImAgNnS	být
keltské	keltský	k2eAgNnSc1d1	keltské
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Regnum	Regnum	k1gNnSc1	Regnum
Noricum	Noricum	k1gNnSc1	Noricum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
federace	federace	k1gFnSc1	federace
třinácti	třináct	k4xCc2	třináct
keltských	keltský	k2eAgMnPc2d1	keltský
a	a	k8xC	a
ilyrských	ilyrský	k2eAgMnPc2d1	ilyrský
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
začleněna	začlenit	k5eAaPmNgFnS	začlenit
do	do	k7c2	do
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
jako	jako	k8xS	jako
její	její	k3xOp3gFnSc2	její
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
většinu	většina	k1gFnSc4	většina
území	území	k1gNnSc1	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
Slovinska	Slovinsko	k1gNnSc2	Slovinsko
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgInSc4d1	jihovýchodní
cíp	cíp	k1gInSc4	cíp
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
sousedila	sousedit	k5eAaImAgFnS	sousedit
tato	tento	k3xDgFnSc1	tento
země	země	k1gFnSc1	země
s	s	k7c7	s
Raetií	Raetie	k1gFnSc7	Raetie
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Panonií	Panonie	k1gFnSc7	Panonie
a	a	k8xC	a
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severu	sever	k1gInSc6	sever
sahalo	sahat	k5eAaImAgNnS	sahat
území	území	k1gNnSc1	území
království	království	k1gNnSc2	království
až	až	k9	až
za	za	k7c4	za
řeku	řeka	k1gFnSc4	řeka
Dunaj	Dunaj	k1gInSc1	Dunaj
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
severní	severní	k2eAgFnSc7d1	severní
hranicí	hranice	k1gFnSc7	hranice
Norica	Noricum	k1gNnSc2	Noricum
teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
trvalém	trvalý	k2eAgNnSc6d1	trvalé
začlenění	začlenění	k1gNnSc6	začlenění
do	do	k7c2	do
Římského	římský	k2eAgNnSc2d1	římské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Archeologický	archeologický	k2eAgInSc1d1	archeologický
průzkum	průzkum	k1gInSc1	průzkum
hrobů	hrob	k1gInPc2	hrob
a	a	k8xC	a
solných	solný	k2eAgInPc2d1	solný
dolů	dol	k1gInPc2	dol
v	v	k7c6	v
Hallstattu	Hallstatt	k1gInSc6	Hallstatt
prokázal	prokázat	k5eAaPmAgMnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
v	v	k7c6	v
dávných	dávný	k2eAgFnPc6d1	dávná
dobách	doba	k1gFnPc6	doba
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
pozoruhodně	pozoruhodně	k6eAd1	pozoruhodně
vyspělá	vyspělý	k2eAgFnSc1d1	vyspělá
civilizace	civilizace	k1gFnSc1	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Hallstattské	hallstattský	k2eAgInPc4d1	hallstattský
hroby	hrob	k1gInPc4	hrob
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
zbraně	zbraň	k1gFnPc1	zbraň
a	a	k8xC	a
ornamenty	ornament	k1gInPc1	ornament
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgFnPc4d1	bronzová
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
železné	železný	k2eAgFnSc2d1	železná
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
teorií	teorie	k1gFnPc2	teorie
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
Noricum	Noricum	k1gNnSc1	Noricum
a	a	k8xC	a
některé	některý	k3yIgFnPc4	některý
další	další	k2eAgFnPc4d1	další
okolní	okolní	k2eAgFnPc4d1	okolní
oblasti	oblast	k1gFnPc4	oblast
pravlastí	pravlast	k1gFnPc2	pravlast
Homérových	Homérův	k2eAgInPc2d1	Homérův
Achajů	Achaj	k1gInPc2	Achaj
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
z	z	k7c2	z
období	období	k1gNnSc2	období
halštatské	halštatský	k2eAgFnSc2d1	halštatská
kultury	kultura	k1gFnSc2	kultura
tvořili	tvořit	k5eAaImAgMnP	tvořit
zřejmě	zřejmě	k6eAd1	zřejmě
Panonové	Panonové	k2eAgInSc4d1	Panonové
(	(	kIx(	(
<g/>
lid	lid	k1gInSc4	lid
spřízněný	spřízněný	k2eAgInSc4d1	spřízněný
s	s	k7c7	s
Ilyry	Ilyr	k1gMnPc7	Ilyr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
později	pozdě	k6eAd2	pozdě
splynuli	splynout	k5eAaPmAgMnP	splynout
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
keltskými	keltský	k2eAgInPc7d1	keltský
kmeny	kmen	k1gInPc7	kmen
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
sem	sem	k6eAd1	sem
dorazily	dorazit	k5eAaPmAgInP	dorazit
někdy	někdy	k6eAd1	někdy
po	po	k7c6	po
roce	rok	k1gInSc6	rok
450	[number]	k4	450
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
z	z	k7c2	z
nynějšího	nynější	k2eAgNnSc2d1	nynější
jihozápadního	jihozápadní	k2eAgNnSc2d1	jihozápadní
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
zaujímali	zaujímat	k5eAaImAgMnP	zaujímat
vůdčí	vůdčí	k2eAgNnSc4d1	vůdčí
postavení	postavení	k1gNnSc4	postavení
Tauriskové	Tauriskový	k2eAgFnSc2d1	Tauriskový
(	(	kIx(	(
<g/>
Taurisci	Taurisek	k1gMnPc1	Taurisek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Noricum	Noricum	k1gNnSc1	Noricum
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
dobách	doba	k1gFnPc6	doba
sloužilo	sloužit	k5eAaImAgNnS	sloužit
Keltům	Kelt	k1gMnPc3	Kelt
jako	jako	k8xS	jako
základna	základna	k1gFnSc1	základna
k	k	k7c3	k
invazím	invaze	k1gFnPc3	invaze
do	do	k7c2	do
Pádské	pádský	k2eAgFnSc2d1	Pádská
nížiny	nížina	k1gFnSc2	nížina
a	a	k8xC	a
na	na	k7c4	na
Apeninský	apeninský	k2eAgInSc4d1	apeninský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
užívali	užívat	k5eAaImAgMnP	užívat
pro	pro	k7c4	pro
Taurisky	Taurisek	k1gInPc4	Taurisek
pojmenování	pojmenování	k1gNnSc4	pojmenování
Norikové	Norikové	k?	Norikové
(	(	kIx(	(
<g/>
Norici	Norik	k1gMnPc1	Norik
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
(	(	kIx(	(
<g/>
odvozený	odvozený	k2eAgInSc1d1	odvozený
z	z	k7c2	z
hlavního	hlavní	k2eAgNnSc2d1	hlavní
sídla	sídlo	k1gNnSc2	sídlo
Taurisků	Taurisk	k1gInPc2	Taurisk
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
nazývalo	nazývat	k5eAaImAgNnS	nazývat
Noreia	Noreia	k1gFnSc1	Noreia
<g/>
)	)	kIx)	)
vztahovali	vztahovat	k5eAaImAgMnP	vztahovat
i	i	k9	i
na	na	k7c4	na
všechny	všechen	k3xTgInPc4	všechen
ostatní	ostatní	k2eAgInPc4d1	ostatní
norické	norický	k2eAgInPc4d1	norický
kmeny	kmen	k1gInPc4	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Přesná	přesný	k2eAgFnSc1d1	přesná
poloha	poloha	k1gFnSc1	poloha
Noreie	Noreie	k1gFnSc2	Noreie
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
dodnes	dodnes	k6eAd1	dodnes
neznámá	známý	k2eNgFnSc1d1	neznámá
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
názorů	názor	k1gInPc2	názor
se	se	k3xPyFc4	se
však	však	k9	však
nacházela	nacházet	k5eAaImAgFnS	nacházet
poblíž	poblíž	k7c2	poblíž
dnešní	dnešní	k2eAgFnSc2d1	dnešní
štýrské	štýrský	k2eAgFnSc2d1	štýrská
obce	obec	k1gFnSc2	obec
Neumarkt	Neumarkt	k1gInSc1	Neumarkt
in	in	k?	in
Steiermark	Steiermark	k1gInSc1	Steiermark
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
200	[number]	k4	200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
Tauriskové	Tauriskový	k2eAgFnPc1d1	Tauriskový
postavili	postavit	k5eAaPmAgMnP	postavit
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
svazku	svazek	k1gInSc2	svazek
třinácti	třináct	k4xCc3	třináct
keltských	keltský	k2eAgInPc2d1	keltský
a	a	k8xC	a
ilyrských	ilyrský	k2eAgInPc2d1	ilyrský
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byl	být	k5eAaImAgMnS	být
položen	položen	k2eAgInSc4d1	položen
základ	základ	k1gInSc4	základ
Norickému	norický	k2eAgNnSc3d1	norický
království	království	k1gNnSc3	království
(	(	kIx(	(
<g/>
Regnum	Regnum	k1gInSc4	Regnum
Noricum	Noricum	k1gNnSc1	Noricum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
tak	tak	k9	tak
představuje	představovat	k5eAaImIp3nS	představovat
první	první	k4xOgInSc1	první
politický	politický	k2eAgInSc1d1	politický
útvar	útvar	k1gInSc1	útvar
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
na	na	k7c6	na
rakouské	rakouský	k2eAgFnSc6d1	rakouská
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
dokonalejším	dokonalý	k2eAgFnPc3d2	dokonalejší
osevním	osevní	k2eAgFnPc3d1	osevní
metodám	metoda	k1gFnPc3	metoda
a	a	k8xC	a
technologickému	technologický	k2eAgInSc3d1	technologický
pokroku	pokrok	k1gInSc3	pokrok
výrazně	výrazně	k6eAd1	výrazně
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Norica	Noricum	k1gNnSc2	Noricum
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
již	již	k6eAd1	již
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
186	[number]	k4	186
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
vážný	vážný	k2eAgInSc1d1	vážný
nedostatek	nedostatek	k1gInSc1	nedostatek
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přimělo	přimět	k5eAaPmAgNnS	přimět
asi	asi	k9	asi
12	[number]	k4	12
000	[number]	k4	000
Taurisků	Taurisk	k1gInPc2	Taurisk
a	a	k8xC	a
Bojů	boj	k1gInPc2	boj
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
barbaři	barbar	k1gMnPc1	barbar
překonali	překonat	k5eAaPmAgMnP	překonat
Alpy	Alpy	k1gFnPc4	Alpy
a	a	k8xC	a
usadili	usadit	k5eAaPmAgMnP	usadit
se	se	k3xPyFc4	se
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Pádu	Pád	k1gInSc2	Pád
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
současného	současný	k2eAgInSc2d1	současný
Benátska	Benátsk	k1gInSc2	Benátsk
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
zde	zde	k6eAd1	zde
Římané	Říman	k1gMnPc1	Říman
založili	založit	k5eAaPmAgMnP	založit
kolonii	kolonie	k1gFnSc4	kolonie
Aquileiu	Aquileius	k1gMnSc3	Aquileius
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
brzy	brzy	k6eAd1	brzy
nabyla	nabýt	k5eAaPmAgFnS	nabýt
velkého	velký	k2eAgInSc2d1	velký
významu	význam	k1gInSc2	význam
jako	jako	k8xS	jako
tranzitní	tranzitní	k2eAgNnSc4d1	tranzitní
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
obchod	obchod	k1gInSc4	obchod
s	s	k7c7	s
oblastmi	oblast	k1gFnPc7	oblast
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Přivábeni	přiváben	k2eAgMnPc1d1	přiváben
obchodními	obchodní	k2eAgFnPc7d1	obchodní
možnostmi	možnost	k1gFnPc7	možnost
a	a	k8xC	a
nerostným	nerostný	k2eAgNnSc7d1	nerostné
bohatstvím	bohatství	k1gNnSc7	bohatství
Noriků	Norik	k1gMnPc2	Norik
navázali	navázat	k5eAaPmAgMnP	navázat
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
Římané	Říman	k1gMnPc1	Říman
přátelské	přátelský	k2eAgInPc4d1	přátelský
vztahy	vztah	k1gInPc4	vztah
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
získali	získat	k5eAaPmAgMnP	získat
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
ohromným	ohromný	k2eAgNnPc3d1	ohromné
nalezištím	naleziště	k1gNnPc3	naleziště
železné	železný	k2eAgFnSc2d1	železná
rudy	ruda	k1gFnPc1	ruda
v	v	k7c6	v
Noricu	Noricum	k1gNnSc6	Noricum
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Liviova	Liviův	k2eAgInSc2d1	Liviův
popisu	popis	k1gInSc2	popis
vyjednávalo	vyjednávat	k5eAaImAgNnS	vyjednávat
prý	prý	k9	prý
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
170	[number]	k4	170
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
římské	římský	k2eAgNnSc4d1	římské
poselstvo	poselstvo	k1gNnSc4	poselstvo
s	s	k7c7	s
norickým	norický	k2eAgMnSc7d1	norický
králem	král	k1gMnSc7	král
Cincibilem	Cincibil	k1gMnSc7	Cincibil
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
Římanům	Říman	k1gMnPc3	Říman
zaručil	zaručit	k5eAaPmAgMnS	zaručit
hospitium	hospitium	k1gNnSc1	hospitium
publicum	publicum	k1gNnSc1	publicum
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
pohostinné	pohostinný	k2eAgNnSc1d1	pohostinné
přátelství	přátelství	k1gNnSc1	přátelství
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byly	být	k5eAaImAgFnP	být
utuženy	utužit	k5eAaPmNgInP	utužit
vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
obchodní	obchodní	k2eAgInPc1d1	obchodní
svazky	svazek	k1gInPc1	svazek
a	a	k8xC	a
posílen	posílen	k2eAgInSc1d1	posílen
římský	římský	k2eAgInSc1d1	římský
vliv	vliv	k1gInSc1	vliv
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
prosperity	prosperita	k1gFnSc2	prosperita
vznikala	vznikat	k5eAaImAgNnP	vznikat
v	v	k7c6	v
Noricu	Noricum	k1gNnSc6	Noricum
první	první	k4xOgNnPc1	první
opevněná	opevněný	k2eAgNnPc1d1	opevněné
oppida	oppidum	k1gNnPc1	oppidum
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
byly	být	k5eAaImAgFnP	být
raženy	ražen	k2eAgFnPc1d1	ražena
mince	mince	k1gFnPc1	mince
podle	podle	k7c2	podle
řeckého	řecký	k2eAgInSc2d1	řecký
vzoru	vzor	k1gInSc2	vzor
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
120	[number]	k4	120
až	až	k8xS	až
115	[number]	k4	115
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
pronikly	proniknout	k5eAaPmAgFnP	proniknout
do	do	k7c2	do
Norica	Noricum	k1gNnSc2	Noricum
migrující	migrující	k2eAgInPc4d1	migrující
germánské	germánský	k2eAgInPc4d1	germánský
kmeny	kmen	k1gInPc4	kmen
Kimbrů	Kimbr	k1gMnPc2	Kimbr
a	a	k8xC	a
Teutonů	Teuton	k1gMnPc2	Teuton
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
střetly	střetnout	k5eAaPmAgFnP	střetnout
s	s	k7c7	s
Boji	boj	k1gInPc7	boj
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
kotlině	kotlina	k1gFnSc6	kotlina
(	(	kIx(	(
<g/>
Boiohaemum	Boiohaemum	k1gInSc1	Boiohaemum
<g/>
)	)	kIx)	)
a	a	k8xC	a
se	se	k3xPyFc4	se
Skordisky	Skordiska	k1gFnPc1	Skordiska
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Tváří	tvářet	k5eAaImIp3nS	tvářet
v	v	k7c4	v
tvář	tvář	k1gFnSc4	tvář
této	tento	k3xDgFnSc3	tento
hrozbě	hrozba	k1gFnSc3	hrozba
se	se	k3xPyFc4	se
Tauriskové	Tauriskový	k2eAgFnPc1d1	Tauriskový
obrátili	obrátit	k5eAaPmAgMnP	obrátit
na	na	k7c4	na
Římany	Říman	k1gMnPc4	Říman
se	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
římské	římský	k2eAgNnSc1d1	římské
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgNnSc3	jenž
velel	velet	k5eAaImAgMnS	velet
konzul	konzul	k1gMnSc1	konzul
Gnaeus	Gnaeus	k1gMnSc1	Gnaeus
Papirius	Papirius	k1gMnSc1	Papirius
Carbo	Carba	k1gFnSc5	Carba
<g/>
,	,	kIx,	,
vyslané	vyslaný	k2eAgFnPc4d1	vyslaná
vstříc	vstříc	k6eAd1	vstříc
útočníkům	útočník	k1gMnPc3	útočník
bylo	být	k5eAaImAgNnS	být
rozdrceno	rozdrcen	k2eAgNnSc1d1	rozdrceno
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Noreie	Noreie	k1gFnSc2	Noreie
<g/>
.	.	kIx.	.
</s>
<s>
Kimbrové	Kimbr	k1gMnPc1	Kimbr
se	se	k3xPyFc4	se
však	však	k9	však
navzdory	navzdory	k7c3	navzdory
svému	svůj	k3xOyFgNnSc3	svůj
vítězství	vítězství	k1gNnSc3	vítězství
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
z	z	k7c2	z
Norica	Noricum	k1gNnSc2	Noricum
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc4	několik
desetiletí	desetiletí	k1gNnPc2	desetiletí
poté	poté	k6eAd1	poté
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tlaku	tlak	k1gInSc2	tlak
germánského	germánský	k2eAgInSc2d1	germánský
kmene	kmen	k1gInSc2	kmen
Svébů	Svéb	k1gInPc2	Svéb
uvedeni	uvést	k5eAaPmNgMnP	uvést
do	do	k7c2	do
pohybu	pohyb	k1gInSc2	pohyb
keltští	keltský	k2eAgMnPc1d1	keltský
Bojové	bojový	k2eAgMnPc4d1	bojový
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
oblastí	oblast	k1gFnSc7	oblast
severně	severně	k6eAd1	severně
a	a	k8xC	a
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Norica	Noricum	k1gNnSc2	Noricum
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
58	[number]	k4	58
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
Bojové	bojový	k2eAgFnPc1d1	bojová
pokusili	pokusit	k5eAaPmAgMnP	pokusit
Noricum	Noricum	k1gNnSc4	Noricum
dobýt	dobýt	k5eAaPmF	dobýt
<g/>
,	,	kIx,	,
utrpěli	utrpět	k5eAaPmAgMnP	utrpět
však	však	k9	však
ničivou	ničivý	k2eAgFnSc4d1	ničivá
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
ale	ale	k9	ale
ohrožovali	ohrožovat	k5eAaImAgMnP	ohrožovat
Noricum	Noricum	k1gNnSc4	Noricum
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
dalších	další	k2eAgNnPc2d1	další
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
jejich	jejich	k3xOp3gFnSc1	jejich
říše	říše	k1gFnSc1	říše
konečně	konečně	k6eAd1	konečně
vyvrácena	vyvrácen	k2eAgFnSc1d1	vyvrácena
Dáky	Dák	k1gMnPc7	Dák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
49	[number]	k4	49
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
odeslal	odeslat	k5eAaPmAgMnS	odeslat
král	král	k1gMnSc1	král
Voccio	Voccio	k1gMnSc1	Voccio
Caesarovi	Caesar	k1gMnSc3	Caesar
300	[number]	k4	300
šlechticů	šlechtic	k1gMnPc2	šlechtic
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
jízdy	jízda	k1gFnSc2	jízda
přispěli	přispět	k5eAaPmAgMnP	přispět
k	k	k7c3	k
Caesarově	Caesarův	k2eAgFnSc3d1	Caesarova
vítězství	vítězství	k1gNnSc3	vítězství
v	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
definitivní	definitivní	k2eAgFnSc3d1	definitivní
porážce	porážka	k1gFnSc3	porážka
Bojů	boj	k1gInPc2	boj
expandovalo	expandovat	k5eAaImAgNnS	expandovat
norické	norický	k2eAgNnSc1d1	norický
království	království	k1gNnSc1	království
do	do	k7c2	do
povodí	povodí	k1gNnSc2	povodí
středního	střední	k2eAgInSc2d1	střední
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Moc	moc	k1gFnSc1	moc
Norica	Noricum	k1gNnSc2	Noricum
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
momentu	moment	k1gInSc6	moment
sahala	sahat	k5eAaImAgFnS	sahat
až	až	k6eAd1	až
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
pánve	pánev	k1gFnSc2	pánev
a	a	k8xC	a
po	po	k7c6	po
odražení	odražení	k1gNnSc6	odražení
útoku	útok	k1gInSc2	útok
Dáků	Dák	k1gMnPc2	Dák
také	také	k9	také
do	do	k7c2	do
současného	současný	k2eAgNnSc2d1	současné
západního	západní	k2eAgNnSc2d1	západní
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Norikům	Norik	k1gMnPc3	Norik
se	se	k3xPyFc4	se
tak	tak	k9	tak
podařilo	podařit	k5eAaPmAgNnS	podařit
vybudovat	vybudovat	k5eAaPmF	vybudovat
poslední	poslední	k2eAgInSc4d1	poslední
mocný	mocný	k2eAgInSc4d1	mocný
keltský	keltský	k2eAgInSc4d1	keltský
politický	politický	k2eAgInSc4d1	politický
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Noricum	Noricum	k1gNnSc1	Noricum
se	se	k3xPyFc4	se
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
těšilo	těšit	k5eAaImAgNnS	těšit
nezávislosti	nezávislost	k1gFnSc2	nezávislost
pod	pod	k7c7	pod
vládou	vláda	k1gFnSc7	vláda
vlastních	vlastní	k2eAgMnPc2d1	vlastní
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
ale	ale	k8xC	ale
Norikové	Norikové	k?	Norikové
napadli	napadnout	k5eAaPmAgMnP	napadnout
společně	společně	k6eAd1	společně
s	s	k7c7	s
Panony	Panon	k1gMnPc7	Panon
Histrii	Histria	k1gFnSc4	Histria
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
byli	být	k5eAaImAgMnP	být
odraženi	odražen	k2eAgMnPc1d1	odražen
prokonzulem	prokonzul	k1gMnSc7	prokonzul
Illyrica	Illyricum	k1gNnPc1	Illyricum
Publiem	Publium	k1gNnSc7	Publium
Siliem	Silius	k1gMnSc7	Silius
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
krále	král	k1gMnSc2	král
Voccia	Voccius	k1gMnSc2	Voccius
bylo	být	k5eAaImAgNnS	být
Noricum	Noricum	k1gNnSc1	Noricum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
15	[number]	k4	15
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
ustaveno	ustaven	k2eAgNnSc1d1	ustaveno
klientským	klientský	k2eAgNnSc7d1	klientské
královstvím	království	k1gNnSc7	království
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
ho	on	k3xPp3gNnSc4	on
nazývali	nazývat	k5eAaImAgMnP	nazývat
provincií	provincie	k1gFnSc7	provincie
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
si	se	k3xPyFc3	se
i	i	k9	i
nadále	nadále	k6eAd1	nadále
uchovávalo	uchovávat	k5eAaImAgNnS	uchovávat
určitou	určitý	k2eAgFnSc4d1	určitá
omezenou	omezený	k2eAgFnSc4d1	omezená
svébytnost	svébytnost	k1gFnSc4	svébytnost
<g/>
.	.	kIx.	.
</s>
<s>
Skutečnou	skutečný	k2eAgFnSc7d1	skutečná
římskou	římský	k2eAgFnSc7d1	římská
provincií	provincie	k1gFnSc7	provincie
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
císařského	císařský	k2eAgMnSc2d1	císařský
prokurátora	prokurátor	k1gMnSc2	prokurátor
se	se	k3xPyFc4	se
Noricum	Noricum	k1gNnSc1	Noricum
stalo	stát	k5eAaPmAgNnS	stát
teprve	teprve	k6eAd1	teprve
někdy	někdy	k6eAd1	někdy
po	po	k7c6	po
roce	rok	k1gInSc6	rok
40	[number]	k4	40
n.	n.	k?	n.
l.	l.	k?	l.
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Claudia	Claudia	k1gFnSc1	Claudia
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
staletích	staletí	k1gNnPc6	staletí
bylo	být	k5eAaImAgNnS	být
Noricum	Noricum	k1gNnSc1	Noricum
jako	jako	k8xS	jako
pohraniční	pohraniční	k2eAgNnSc1d1	pohraniční
území	území	k1gNnSc1	území
říše	říš	k1gFnSc2	říš
protkáno	protkat	k5eAaPmNgNnS	protkat
hustou	hustý	k2eAgFnSc7d1	hustá
sítí	síť	k1gFnSc7	síť
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yRnSc6	což
svědčí	svědčit	k5eAaImIp3nP	svědčit
četné	četný	k2eAgInPc1d1	četný
nálezy	nález	k1gInPc1	nález
milníků	milník	k1gInPc2	milník
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
archeologické	archeologický	k2eAgInPc4d1	archeologický
objevy	objev	k1gInPc4	objev
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
budováním	budování	k1gNnSc7	budování
kolonií	kolonie	k1gFnPc2	kolonie
napomohlo	napomoct	k5eAaPmAgNnS	napomoct
postupné	postupný	k2eAgNnSc1d1	postupné
romanizaci	romanizace	k1gFnSc4	romanizace
zdejších	zdejší	k2eAgMnPc2d1	zdejší
Keltů	Kelt	k1gMnPc2	Kelt
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
nejdůležitější	důležitý	k2eAgFnPc4d3	nejdůležitější
komunikace	komunikace	k1gFnPc4	komunikace
spojující	spojující	k2eAgInSc1d1	spojující
Řím	Řím	k1gInSc1	Řím
s	s	k7c7	s
Carnuntem	Carnunt	k1gInSc7	Carnunt
vedla	vést	k5eAaImAgFnS	vést
přes	přes	k7c4	přes
města	město	k1gNnPc4	město
Aquileia	Aquileium	k1gNnSc2	Aquileium
<g/>
,	,	kIx,	,
Emona	Emona	k1gFnSc1	Emona
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Lublaň	Lublaň	k1gFnSc1	Lublaň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Celeia	Celeia	k1gFnSc1	Celeia
(	(	kIx(	(
<g/>
Celje	Celje	k1gFnSc1	Celje
<g/>
)	)	kIx)	)
a	a	k8xC	a
Poetovio	Poetovio	k1gMnSc1	Poetovio
(	(	kIx(	(
<g/>
Ptuj	Ptuj	k1gMnSc1	Ptuj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Carnuntum	Carnuntum	k1gNnSc1	Carnuntum
původně	původně	k6eAd1	původně
náleželo	náležet	k5eAaImAgNnS	náležet
k	k	k7c3	k
Noricu	Noricum	k1gNnSc3	Noricum
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
6	[number]	k4	6
n.	n.	k?	n.
l.	l.	k?	l.
bylo	být	k5eAaImAgNnS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
Vídeňskou	vídeňský	k2eAgFnSc7d1	Vídeňská
pánví	pánev	k1gFnSc7	pánev
připojeno	připojit	k5eAaPmNgNnS	připojit
k	k	k7c3	k
provincii	provincie	k1gFnSc3	provincie
Panonii	Panonie	k1gFnSc3	Panonie
<g/>
.	.	kIx.	.
</s>
<s>
Strategicky	strategicky	k6eAd1	strategicky
velmi	velmi	k6eAd1	velmi
významná	významný	k2eAgFnSc1d1	významná
byla	být	k5eAaImAgFnS	být
také	také	k9	také
silnice	silnice	k1gFnSc1	silnice
táhnoucí	táhnoucí	k2eAgFnSc1d1	táhnoucí
se	se	k3xPyFc4	se
z	z	k7c2	z
panonské	panonský	k2eAgFnSc2d1	Panonská
Vindobony	Vindobona	k1gFnSc2	Vindobona
(	(	kIx(	(
<g/>
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
podél	podél	k7c2	podél
toku	tok	k1gInSc2	tok
Dunaje	Dunaj	k1gInSc2	Dunaj
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
do	do	k7c2	do
Lauriaca	Lauriac	k1gInSc2	Lauriac
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
Raetie	Raetie	k1gFnSc2	Raetie
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznačnějšími	význačný	k2eAgNnPc7d3	nejvýznačnější
městy	město	k1gNnPc7	město
a	a	k8xC	a
koloniemi	kolonie	k1gFnPc7	kolonie
Norica	Noricum	k1gNnSc2	Noricum
v	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
etapě	etapa	k1gFnSc6	etapa
jeho	jeho	k3xOp3gFnPc2	jeho
dějin	dějiny	k1gFnPc2	dějiny
bylo	být	k5eAaImAgNnS	být
Virunum	Virunum	k1gNnSc4	Virunum
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Maria	Maria	k1gFnSc1	Maria
Saal	Saala	k1gFnPc2	Saala
poblíž	poblíž	k7c2	poblíž
Klagenfurtu	Klagenfurt	k1gInSc2	Klagenfurt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Celeia	Celeia	k1gFnSc1	Celeia
<g/>
,	,	kIx,	,
Juvavum	Juvavum	k1gInSc1	Juvavum
(	(	kIx(	(
<g/>
Salzburg	Salzburg	k1gInSc1	Salzburg
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lauriacum	Lauriacum	k1gInSc1	Lauriacum
(	(	kIx(	(
<g/>
Lorch	Lorch	k1gInSc1	Lorch
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
sousední	sousední	k2eAgFnPc4d1	sousední
Raetie	Raetie	k1gFnPc4	Raetie
zaujímalo	zaujímat	k5eAaImAgNnS	zaujímat
také	také	k9	také
Noricum	Noricum	k1gNnSc1	Noricum
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
existence	existence	k1gFnSc2	existence
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
postavení	postavení	k1gNnSc2	postavení
předsunuté	předsunutý	k2eAgFnSc2d1	předsunutá
obranné	obranný	k2eAgFnSc2d1	obranná
linie	linie	k1gFnSc2	linie
Itálie	Itálie	k1gFnSc2	Itálie
vůči	vůči	k7c3	vůči
vpádům	vpád	k1gInPc3	vpád
germánských	germánský	k2eAgMnPc2d1	germánský
kmenů	kmen	k1gInPc2	kmen
zpoza	zpoza	k7c2	zpoza
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
řadu	řada	k1gFnSc4	řada
pevností	pevnost	k1gFnPc2	pevnost
(	(	kIx(	(
<g/>
Limes	Limes	k1gMnSc1	Limes
Romanus	Romanus	k1gMnSc1	Romanus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
střežily	střežit	k5eAaImAgFnP	střežit
břehy	břeh	k1gInPc4	břeh
Dunaje	Dunaj	k1gInSc2	Dunaj
a	a	k8xC	a
alpské	alpský	k2eAgInPc4d1	alpský
průsmyky	průsmyk	k1gInPc4	průsmyk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
markomanských	markomanský	k2eAgFnPc2d1	markomanská
válek	válka	k1gFnPc2	válka
(	(	kIx(	(
<g/>
166	[number]	k4	166
<g/>
-	-	kIx~	-
<g/>
180	[number]	k4	180
<g/>
)	)	kIx)	)
umístil	umístit	k5eAaPmAgMnS	umístit
Marcus	Marcus	k1gMnSc1	Marcus
Aurelius	Aurelius	k1gMnSc1	Aurelius
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
provincie	provincie	k1gFnSc2	provincie
jednu	jeden	k4xCgFnSc4	jeden
římskou	římský	k2eAgFnSc4d1	římská
legii	legie	k1gFnSc4	legie
(	(	kIx(	(
<g/>
Legio	Legio	k1gMnSc1	Legio
II	II	kA	II
Italica	Italica	k1gMnSc1	Italica
Pia	Pius	k1gMnSc2	Pius
<g/>
)	)	kIx)	)
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
početnými	početný	k2eAgInPc7d1	početný
pomocnými	pomocný	k2eAgInPc7d1	pomocný
sbory	sbor	k1gInPc7	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
vojenské	vojenský	k2eAgFnPc1d1	vojenská
síly	síla	k1gFnPc1	síla
zde	zde	k6eAd1	zde
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
měly	mít	k5eAaImAgInP	mít
své	svůj	k3xOyFgNnSc4	svůj
stálé	stálý	k2eAgNnSc4d1	stálé
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Velitel	velitel	k1gMnSc1	velitel
legie	legie	k1gFnSc2	legie
působil	působit	k5eAaImAgMnS	působit
zároveň	zároveň	k6eAd1	zároveň
jako	jako	k8xS	jako
místodržitel	místodržitel	k1gMnSc1	místodržitel
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Diocletiana	Diocletiana	k1gFnSc1	Diocletiana
bylo	být	k5eAaImAgNnS	být
Noricum	Noricum	k1gNnSc1	Noricum
na	na	k7c6	na
konci	konec	k1gInSc6	konec
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
provincií	provincie	k1gFnPc2	provincie
<g/>
:	:	kIx,	:
Noricum	Noricum	k1gNnSc1	Noricum
ripense	ripense	k1gFnSc2	ripense
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
pobřežní	pobřežní	k2eAgNnSc1d1	pobřežní
Noricum	Noricum	k1gNnSc1	Noricum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
při	při	k7c6	při
řece	řeka	k1gFnSc6	řeka
Dunaji	Dunaj	k1gInSc3	Dunaj
<g/>
)	)	kIx)	)
a	a	k8xC	a
Noricum	Noricum	k1gNnSc1	Noricum
mediterraneum	mediterraneum	k1gNnSc1	mediterraneum
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
vnitrozemní	vnitrozemní	k2eAgNnSc1d1	vnitrozemní
Noricum	Noricum	k1gNnSc1	Noricum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jižněji	jižně	k6eAd2	jižně
položený	položený	k2eAgInSc4d1	položený
a	a	k8xC	a
hornatější	hornatý	k2eAgInSc4d2	hornatější
region	region	k1gInSc4	region
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
tyto	tento	k3xDgFnPc1	tento
provincie	provincie	k1gFnPc1	provincie
spadaly	spadat	k5eAaPmAgFnP	spadat
do	do	k7c2	do
ilyrské	ilyrský	k2eAgFnSc2d1	ilyrská
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
italské	italský	k2eAgFnSc2d1	italská
prefektury	prefektura	k1gFnSc2	prefektura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
pozvolného	pozvolný	k2eAgNnSc2d1	pozvolné
šíření	šíření	k1gNnSc2	šíření
křesťanství	křesťanství	k1gNnSc2	křesťanství
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Noricu	Noricum	k1gNnSc6	Noricum
založeno	založit	k5eAaPmNgNnS	založit
několik	několik	k4yIc1	několik
biskupství	biskupství	k1gNnPc2	biskupství
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
v	v	k7c6	v
období	období	k1gNnSc6	období
zmatků	zmatek	k1gInPc2	zmatek
během	během	k7c2	během
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zhroucení	zhroucení	k1gNnSc6	zhroucení
západořímské	západořímský	k2eAgFnSc2d1	Západořímská
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
v	v	k7c6	v
Noricu	Noricum	k1gNnSc6	Noricum
ještě	ještě	k6eAd1	ještě
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
udržel	udržet	k5eAaPmAgInS	udržet
římský	římský	k2eAgInSc1d1	římský
správní	správní	k2eAgInSc1d1	správní
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
přetrval	přetrvat	k5eAaPmAgInS	přetrvat
až	až	k6eAd1	až
do	do	k7c2	do
příchodu	příchod	k1gInSc2	příchod
Avarů	Avar	k1gMnPc2	Avar
a	a	k8xC	a
Slovanů	Slovan	k1gMnPc2	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Noricum	Noricum	k1gNnSc4	Noricum
byla	být	k5eAaImAgFnS	být
hornatá	hornatý	k2eAgFnSc1d1	hornatá
země	země	k1gFnSc1	země
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
chudou	chudý	k2eAgFnSc7d1	chudá
půdou	půda	k1gFnSc7	půda
<g/>
,	,	kIx,	,
oplývala	oplývat	k5eAaImAgFnS	oplývat
však	však	k9	však
hojnými	hojný	k2eAgFnPc7d1	hojná
nalezišti	naleziště	k1gNnSc3	naleziště
železné	železný	k2eAgFnSc2d1	železná
rudy	ruda	k1gFnSc2	ruda
<g/>
,	,	kIx,	,
kterýmžto	kterýmžto	k?	kterýmžto
materiálem	materiál	k1gInSc7	materiál
zásobovalo	zásobovat	k5eAaImAgNnS	zásobovat
kovárny	kovárna	k1gFnSc2	kovárna
a	a	k8xC	a
zbrojní	zbrojní	k2eAgFnSc2d1	zbrojní
manufaktury	manufaktura	k1gFnSc2	manufaktura
v	v	k7c6	v
Panonii	Panonie	k1gFnSc6	Panonie
<g/>
,	,	kIx,	,
Moesii	Moesie	k1gFnSc6	Moesie
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Proslulou	proslulý	k2eAgFnSc4d1	proslulá
norickou	norický	k2eAgFnSc4d1	norická
ocel	ocel	k1gFnSc4	ocel
užívali	užívat	k5eAaImAgMnP	užívat
Římané	Říman	k1gMnPc1	Říman
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
kvalitních	kvalitní	k2eAgFnPc2d1	kvalitní
a	a	k8xC	a
ceněných	ceněný	k2eAgFnPc2d1	ceněná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Norica	Noricum	k1gNnSc2	Noricum
byli	být	k5eAaImAgMnP	být
stateční	statečný	k2eAgMnPc1d1	statečný
a	a	k8xC	a
bojovní	bojovný	k2eAgMnPc1d1	bojovný
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
než	než	k8xS	než
zemědělství	zemědělství	k1gNnSc2	zemědělství
věnovali	věnovat	k5eAaPmAgMnP	věnovat
pastevectví	pastevectví	k1gNnSc4	pastevectví
skotu	skot	k1gInSc2	skot
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
Římany	Říman	k1gMnPc4	Říman
byly	být	k5eAaImAgInP	být
místní	místní	k2eAgInPc1d1	místní
hluboké	hluboký	k2eAgInPc1d1	hluboký
lesy	les	k1gInPc1	les
vykáceny	vykácet	k5eAaPmNgInP	vykácet
a	a	k8xC	a
bažiny	bažina	k1gFnPc1	bažina
odvodněny	odvodněn	k2eAgFnPc1d1	odvodněn
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byla	být	k5eAaImAgFnS	být
výrazně	výrazně	k6eAd1	výrazně
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
úrodnost	úrodnost	k1gFnSc1	úrodnost
země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
zde	zde	k6eAd1	zde
bylo	být	k5eAaImAgNnS	být
nalezeno	nalézt	k5eAaBmNgNnS	nalézt
veliké	veliký	k2eAgNnSc1d1	veliké
množství	množství	k1gNnSc1	množství
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
