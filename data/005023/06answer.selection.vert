<s>
Noricum	Noricum	k1gNnSc1	Noricum
bylo	být	k5eAaImAgNnS	být
keltské	keltský	k2eAgNnSc1d1	keltské
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Regnum	Regnum	k1gNnSc1	Regnum
Noricum	Noricum	k1gNnSc1	Noricum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přesněji	přesně	k6eAd2	přesně
federace	federace	k1gFnSc1	federace
třinácti	třináct	k4xCc2	třináct
keltských	keltský	k2eAgMnPc2d1	keltský
a	a	k8xC	a
ilyrských	ilyrský	k2eAgMnPc2d1	ilyrský
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
později	pozdě	k6eAd2	pozdě
začleněna	začlenit	k5eAaPmNgFnS	začlenit
do	do	k7c2	do
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
jako	jako	k8xC	jako
její	její	k3xOp3gFnSc2	její
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
