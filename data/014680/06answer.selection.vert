<s>
V	v	k7c6
roce	rok	k1gInSc6
1873	#num#	k4
do	do	k7c2
parlamentu	parlament	k1gInSc2
nastupoval	nastupovat	k5eAaImAgMnS
za	za	k7c4
blok	blok	k1gInSc4
ústavověrných	ústavověrný	k2eAgNnPc2d1
(	(	kIx(
<g/>
tzv.	tzv.	kA
Ústavní	ústavní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
centralisticky	centralisticky	k6eAd1
a	a	k8xC
provídeňsky	provídeňsky	k6eAd1
orientovaná	orientovaný	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
jehož	jehož	k3xOyRp3gInSc6
rámci	rámec	k1gInSc6
představoval	představovat	k5eAaImAgInS
křídlo	křídlo	k1gNnSc4
Strany	strana	k1gFnSc2
ústavověrného	ústavověrný	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Jako	jako	k8xC,k8xS
ústavověrný	ústavověrný	k2eAgMnSc1d1
statkář	statkář	k1gMnSc1
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
i	i	k9
po	po	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1879	#num#	k4
<g/>
<g />
.	.	kIx.
</s>