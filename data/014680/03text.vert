<s>
Gustav	Gustav	k1gMnSc1
Suttner	Suttner	k1gMnSc1
</s>
<s>
Gustav	Gustav	k1gMnSc1
Suttner	Suttner	k1gMnSc1
Gustav	Gustav	k1gMnSc1
Suttner	Suttner	k1gMnSc1
</s>
<s>
Poslanec	poslanec	k1gMnSc1
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1870	#num#	k4
–	–	k?
1897	#num#	k4
</s>
<s>
Poslanec	poslanec	k1gMnSc1
Dolnorakouského	dolnorakouský	k2eAgInSc2d1
zemského	zemský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1861	#num#	k4
–	–	k?
1877	#num#	k4
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
1878	#num#	k4
–	–	k?
1896	#num#	k4
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
Ústavní	ústavní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
(	(	kIx(
<g/>
ústavověrný	ústavověrný	k2eAgInSc1d1
velkostatek	velkostatek	k1gInSc1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
Klub	klub	k1gInSc1
liberálů	liberál	k1gMnPc2
–	–	k?
staroněmci	staroněmec	k1gMnPc7
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
Sjednoc	Sjednoc	k1gFnSc1
<g/>
.	.	kIx.
levice	levice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
Německorak	Německorak	k1gInSc1
<g/>
.	.	kIx.
klub	klub	k1gInSc1
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
Sjednoc	Sjednoc	k1gFnSc1
<g/>
.	.	kIx.
něm.	něm.	k?
levice	levice	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1826	#num#	k4
KirchstettenRakouské	KirchstettenRakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc4
Rakouské	rakouský	k2eAgNnSc4d1
císařství	císařství	k1gNnSc4
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1900	#num#	k4
VídeňRakousko-Uhersko	VídeňRakousko-Uhersko	k1gNnSc4
Rakousko-Uhersko	Rakousko-Uherska	k1gFnSc5
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Gustav	Gustav	k1gMnSc1
von	von	k1gInSc4
Suttner	Suttner	k1gInSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1826	#num#	k4
Kirchstetten	Kirchstettno	k1gNnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
27	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1900	#num#	k4
Vídeň	Vídeň	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
rakouský	rakouský	k2eAgMnSc1d1
politik	politik	k1gMnSc1
německé	německý	k2eAgFnSc2d1
národnosti	národnost	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
poslanec	poslanec	k1gMnSc1
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
Působil	působit	k5eAaImAgMnS
jako	jako	k9
statkář	statkář	k1gMnSc1
v	v	k7c6
rodném	rodný	k2eAgInSc6d1
Kirchstettenu	Kirchstetten	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studoval	studovat	k5eAaImAgMnS
na	na	k7c6
Vídeňské	vídeňský	k2eAgFnSc6d1
univerzitě	univerzita	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Absolvoval	absolvovat	k5eAaPmAgMnS
Tereziánskou	tereziánský	k2eAgFnSc4d1
akademii	akademie	k1gFnSc4
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
ústředního	ústřední	k2eAgInSc2d1
výboru	výbor	k1gInSc2
c.	c.	k?
k.	k.	k?
Zemědělské	zemědělský	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zasloužil	zasloužit	k5eAaPmAgMnS
se	se	k3xPyFc4
o	o	k7c4
rozvoj	rozvoj	k1gInSc4
silniční	silniční	k2eAgFnSc2d1
sítě	síť	k1gFnSc2
a	a	k8xC
péče	péče	k1gFnSc2
o	o	k7c4
chudé	chudý	k2eAgMnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1867	#num#	k4
byl	být	k5eAaImAgMnS
povýšen	povýšit	k5eAaPmNgMnS
na	na	k7c4
barona	baron	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Podnikl	podniknout	k5eAaPmAgInS
studijní	studijní	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
do	do	k7c2
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Byl	být	k5eAaImAgMnS
veřejně	veřejně	k6eAd1
a	a	k8xC
politicky	politicky	k6eAd1
aktivní	aktivní	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zasedal	zasedat	k5eAaImAgMnS
jako	jako	k9
poslanec	poslanec	k1gMnSc1
Dolnorakouského	dolnorakouský	k2eAgInSc2d1
zemského	zemský	k2eAgInSc2d1
sněmu	sněm	k1gInSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
nastoupil	nastoupit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1861	#num#	k4
za	za	k7c4
velkostatkářskou	velkostatkářský	k2eAgFnSc4d1
kurii	kurie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Členem	člen	k1gMnSc7
sněmu	sněm	k1gInSc2
zůstal	zůstat	k5eAaPmAgMnS
do	do	k7c2
roku	rok	k1gInSc2
1877	#num#	k4
a	a	k8xC
pak	pak	k6eAd1
se	se	k3xPyFc4
na	na	k7c4
něj	on	k3xPp3gMnSc4
vrátil	vrátit	k5eAaPmAgInS
roku	rok	k1gInSc2
1878	#num#	k4
a	a	k8xC
poslancem	poslanec	k1gMnSc7
byl	být	k5eAaImAgInS
do	do	k7c2
roku	rok	k1gInSc2
1896	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Byl	být	k5eAaImAgInS
také	také	k9
poslancem	poslanec	k1gMnSc7
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
(	(	kIx(
<g/>
celostátního	celostátní	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
Předlitavska	Předlitavsko	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kam	kam	k6eAd1
ho	on	k3xPp3gMnSc4
zvolil	zvolit	k5eAaPmAgInS
zemský	zemský	k2eAgInSc1d1
sněm	sněm	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1870	#num#	k4
(	(	kIx(
<g/>
Říšská	říšský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
byla	být	k5eAaImAgFnS
tehdy	tehdy	k6eAd1
ještě	ještě	k6eAd1
volena	volit	k5eAaImNgFnS
nepřímo	přímo	k6eNd1
zemskými	zemský	k2eAgInPc7d1
sněmy	sněm	k1gInPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opětovně	opětovně	k6eAd1
sem	sem	k6eAd1
byl	být	k5eAaImAgInS
delegován	delegovat	k5eAaBmNgInS
zemským	zemský	k2eAgInSc7d1
sněmem	sněm	k1gInSc7
roku	rok	k1gInSc2
1871	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uspěl	uspět	k5eAaPmAgMnS
i	i	k9
v	v	k7c6
prvních	první	k4xOgFnPc6
přímých	přímý	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1873	#num#	k4
za	za	k7c4
kurii	kurie	k1gFnSc4
velkostatkářskou	velkostatkářský	k2eAgFnSc4d1
v	v	k7c6
Dolních	dolní	k2eAgInPc6d1
Rakousích	Rakousy	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1879	#num#	k4
<g/>
,	,	kIx,
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1885	#num#	k4
a	a	k8xC
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1891	#num#	k4
zde	zde	k6eAd1
mandát	mandát	k1gInSc4
obhájil	obhájit	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1873	#num#	k4
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
jako	jako	k9
baron	baron	k1gMnSc1
Gustav	Gustav	k1gMnSc1
Suttner	Suttner	k1gMnSc1
<g/>
,	,	kIx,
statkář	statkář	k1gMnSc1
<g/>
,	,	kIx,
bytem	byt	k1gInSc7
Vídeň	Vídeň	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1873	#num#	k4
do	do	k7c2
parlamentu	parlament	k1gInSc2
nastupoval	nastupovat	k5eAaImAgMnS
za	za	k7c4
blok	blok	k1gInSc4
ústavověrných	ústavověrný	k2eAgNnPc2d1
(	(	kIx(
<g/>
tzv.	tzv.	kA
Ústavní	ústavní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
centralisticky	centralisticky	k6eAd1
a	a	k8xC
provídeňsky	provídeňsky	k6eAd1
orientovaná	orientovaný	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
jehož	jehož	k3xOyRp3gInSc6
rámci	rámec	k1gInSc6
představoval	představovat	k5eAaImAgInS
křídlo	křídlo	k1gNnSc4
Strany	strana	k1gFnSc2
ústavověrného	ústavověrný	k2eAgInSc2d1
velkostatku	velkostatek	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Jako	jako	k8xC,k8xS
ústavověrný	ústavověrný	k2eAgMnSc1d1
statkář	statkář	k1gMnSc1
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
i	i	k9
po	po	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1879	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
Říšské	říšský	k2eAgFnSc6d1
radě	rada	k1gFnSc6
se	se	k3xPyFc4
v	v	k7c6
říjnu	říjen	k1gInSc6
1879	#num#	k4
uvádí	uvádět	k5eAaImIp3nS
jako	jako	k9
člen	člen	k1gMnSc1
staroněmeckého	staroněmecký	k2eAgInSc2d1
(	(	kIx(
<g/>
staroliberálního	staroliberální	k2eAgInSc2d1
<g/>
)	)	kIx)
Klubu	klub	k1gInSc2
liberálů	liberál	k1gMnPc2
(	(	kIx(
<g/>
Club	club	k1gInSc1
der	drát	k5eAaImRp2nS
Liberalen	Liberalen	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
1881	#num#	k4
byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
klubu	klub	k1gInSc2
Sjednocené	sjednocený	k2eAgFnSc2d1
levice	levice	k1gFnSc2
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
do	do	k7c2
kterého	který	k3yRgNnSc2,k3yQgNnSc2,k3yIgNnSc2
se	se	k3xPyFc4
spojilo	spojit	k5eAaPmAgNnS
několik	několik	k4yIc1
ústavověrných	ústavověrný	k2eAgFnPc2d1
(	(	kIx(
<g/>
liberálně	liberálně	k6eAd1
a	a	k8xC
centralisticky	centralisticky	k6eAd1
orientovaných	orientovaný	k2eAgInPc2d1
<g/>
)	)	kIx)
politických	politický	k2eAgInPc2d1
proudů	proud	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c4
tento	tento	k3xDgInSc4
klub	klub	k1gInSc4
uspěl	uspět	k5eAaPmAgMnS
i	i	k9
ve	v	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1885	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
rozpadu	rozpad	k1gInSc6
Sjednocené	sjednocený	k2eAgFnSc2d1
levice	levice	k1gFnSc2
přešel	přejít	k5eAaPmAgInS
do	do	k7c2
frakce	frakce	k1gFnSc2
Německorakouský	německorakouský	k2eAgInSc1d1
klub	klub	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1890	#num#	k4
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
jako	jako	k9
poslanec	poslanec	k1gMnSc1
obnoveného	obnovený	k2eAgInSc2d1
klubu	klub	k1gInSc2
německých	německý	k2eAgMnPc2d1
liberálů	liberál	k1gMnPc2
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
oficiálně	oficiálně	k6eAd1
nazývaného	nazývaný	k2eAgInSc2d1
Sjednocená	sjednocený	k2eAgFnSc1d1
německá	německý	k2eAgFnSc1d1
levice	levice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
I	i	k9
ve	v	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1891	#num#	k4
byl	být	k5eAaImAgInS
na	na	k7c4
Říšskou	říšský	k2eAgFnSc4d1
radu	rada	k1gFnSc4
zvolen	zvolit	k5eAaPmNgMnS
za	za	k7c2
klub	klub	k1gInSc4
Sjednocené	sjednocený	k2eAgFnSc2d1
německé	německý	k2eAgFnSc2d1
levice	levice	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
říjnu	říjen	k1gInSc6
1900	#num#	k4
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
bytě	byt	k1gInSc6
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
BIOGRAPHISCHES	BIOGRAPHISCHES	kA
HANDBUCH	HANDBUCH	kA
des	des	k1gNnPc2
NÖ	NÖ	kA
LANDTAGES	LANDTAGES	kA
1861	#num#	k4
–	–	k?
1921	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
landtag-noe	landtag-noe	k1gFnSc1
<g/>
.	.	kIx.
<g/>
at	at	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Abg	Abg	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frhr	Frhr	k1gInSc1
<g/>
.	.	kIx.
v.	v.	k?
Suttner	Suttner	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Das	Das	k1gMnSc2
Vaterland	vaterland	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říjen	říjen	k1gInSc1
1900	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
296	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Databáze	databáze	k1gFnSc2
stenografických	stenografický	k2eAgInPc2d1
protokolů	protokol	k1gInPc2
a	a	k8xC
rejstříků	rejstřík	k1gInPc2
Říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
z	z	k7c2
příslušných	příslušný	k2eAgNnPc2d1
volebních	volební	k2eAgNnPc2d1
období	období	k1gNnPc2
<g/>
,	,	kIx,
http://alex.onb.ac.at/spa.htm.	http://alex.onb.ac.at/spa.htm.	k?
<g/>
↑	↑	k?
http://alex.onb.ac.at/cgi-content/alex?aid=spa&	http://alex.onb.ac.at/cgi-content/alex?aid=spa&	k?
<g/>
↑	↑	k?
Statistika	statistika	k1gFnSc1
nové	nový	k2eAgFnPc1d1
sněmovny	sněmovna	k1gFnPc1
poslanců	poslanec	k1gMnPc2
říšské	říšský	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
,	,	kIx,
Národní	národní	k2eAgFnSc2d1
listy	lista	k1gFnSc2
31	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
1873	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2	#num#	k4
<g/>
.	.	kIx.
http://kramerius.nkp.cz/kramerius/PShowPageDoc.do?id=5877662	http://kramerius.nkp.cz/kramerius/PShowPageDoc.do?id=5877662	k4
<g/>
↑	↑	k?
Celkový	celkový	k2eAgInSc1d1
výsledek	výsledek	k1gInSc1
voleb	volba	k1gFnPc2
do	do	k7c2
rady	rada	k1gFnSc2
říšské	říšský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInPc1d1
listy	list	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Červenec	červenec	k1gInSc1
1879	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
19	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
164	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Parlamentarisches	Parlamentarisches	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Salzburger	Salzburger	k1gMnSc1
Volksblatt	Volksblatt	k1gMnSc1
<g/>
:	:	kIx,
unabh	unabh	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tageszeitung	Tageszeitung	k1gMnSc1
f.	f.	k?
Stadt	Stadt	k1gMnSc1
u.	u.	k?
Land	Land	k1gInSc1
Salzburg	Salzburg	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říjen	říjen	k1gInSc1
1879	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
9	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
126	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Teplitz-Schönauer	Teplitz-Schönauer	k1gMnSc1
Anzeiger	Anzeiger	k1gMnSc1
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
1881	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
68	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
4	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Výsledek	výsledek	k1gInSc1
voleb	volba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Našinec	našinec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Červen	červen	k1gInSc1
1885	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
69	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Südsteirische	Südsteirische	k1gNnSc2
Post	posta	k1gFnPc2
<g/>
,	,	kIx,
13	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
1887	#num#	k4
<g/>
,	,	kIx,
č.	č.	k?
29	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Südsteirische	Südsteirische	k1gNnSc2
Post	posta	k1gFnPc2
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
1891	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
2	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Národní	národní	k2eAgFnSc2d1
listy	lista	k1gFnSc2
<g/>
,	,	kIx,
25	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
1891	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
127883231	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
1820	#num#	k4
1493	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
15806306	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Politika	politika	k1gFnSc1
|	|	kIx~
Rakousko	Rakousko	k1gNnSc1
</s>
