<s>
Krokodýl	krokodýl	k1gMnSc1	krokodýl
(	(	kIx(	(
<g/>
Crocodylus	Crocodylus	k1gMnSc1	Crocodylus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc4	rod
velkých	velký	k2eAgInPc2d1	velký
vodních	vodní	k2eAgInPc2d1	vodní
plazů	plaz	k1gInPc2	plaz
<g/>
,	,	kIx,	,
náležejících	náležející	k2eAgInPc2d1	náležející
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
krokodýlovitých	krokodýlovitý	k2eAgMnPc2d1	krokodýlovitý
<g/>
.	.	kIx.	.
</s>
<s>
Krokodýli	krokodýl	k1gMnPc1	krokodýl
jsou	být	k5eAaImIp3nP	být
predátoři	predátor	k1gMnPc1	predátor
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
tropických	tropický	k2eAgFnPc6d1	tropická
oblastech	oblast	k1gFnPc6	oblast
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
Ameriky	Amerika	k1gFnSc2	Amerika
i	i	k8xC	i
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Vlastním	vlastní	k2eAgNnSc7d1	vlastní
životním	životní	k2eAgNnSc7d1	životní
prostředím	prostředí	k1gNnSc7	prostředí
krokodýlů	krokodýl	k1gMnPc2	krokodýl
je	být	k5eAaImIp3nS	být
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
přirozeně	přirozeně	k6eAd1	přirozeně
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
nejraději	rád	k6eAd3	rád
pomalé	pomalý	k2eAgFnSc2d1	pomalá
říční	říční	k2eAgFnSc2d1	říční
toky	toka	k1gFnSc2	toka
a	a	k8xC	a
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
<g/>
,	,	kIx,	,
např.	např.	kA	např.
krokodýl	krokodýl	k1gMnSc1	krokodýl
mořský	mořský	k2eAgMnSc1d1	mořský
žijí	žít	k5eAaImIp3nP	žít
však	však	k9	však
i	i	k9	i
v	v	k7c6	v
mořích	moře	k1gNnPc6	moře
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
potravou	potrava	k1gFnSc7	potrava
jsou	být	k5eAaImIp3nP	být
různí	různý	k2eAgMnPc1d1	různý
savci	savec	k1gMnPc1	savec
<g/>
,	,	kIx,	,
ptáci	pták	k1gMnPc1	pták
a	a	k8xC	a
ryby	ryba	k1gFnPc1	ryba
a	a	k8xC	a
také	také	k9	také
jiní	jiný	k2eAgMnPc1d1	jiný
plazi	plaz	k1gMnPc1	plaz
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
loví	lovit	k5eAaImIp3nP	lovit
a	a	k8xC	a
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
na	na	k7c6	na
jejich	jejich	k3xOp3gFnPc6	jejich
mršinách	mršina	k1gFnPc6	mršina
<g/>
.	.	kIx.	.
</s>
<s>
Krokodýlů	krokodýl	k1gMnPc2	krokodýl
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
jak	jak	k8xC	jak
2500	[number]	k4	2500
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
lepší	dobrý	k2eAgFnSc4d2	lepší
přehlednost	přehlednost	k1gFnSc4	přehlednost
se	se	k3xPyFc4	se
však	však	k9	však
používá	používat	k5eAaImIp3nS	používat
nejčastěji	často	k6eAd3	často
méně	málo	k6eAd2	málo
přesné	přesný	k2eAgNnSc4d1	přesné
členění	členění	k1gNnSc4	členění
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
biotopů	biotop	k1gInPc2	biotop
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
krokodýlů	krokodýl	k1gMnPc2	krokodýl
je	být	k5eAaImIp3nS	být
kryto	krýt	k5eAaImNgNnS	krýt
rohovitými	rohovitý	k2eAgInPc7d1	rohovitý
štíty	štít	k1gInPc7	štít
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterými	který	k3yRgFnPc7	který
leží	ležet	k5eAaImIp3nP	ležet
kostěné	kostěný	k2eAgFnPc1d1	kostěná
desky	deska	k1gFnPc1	deska
<g/>
.	.	kIx.	.
</s>
<s>
Krokodýli	krokodýl	k1gMnPc1	krokodýl
plavou	plavat	k5eAaImIp3nP	plavat
pomocí	pomocí	k7c2	pomocí
silného	silný	k2eAgNnSc2d1	silné
<g/>
,	,	kIx,	,
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
zploštělého	zploštělý	k2eAgInSc2d1	zploštělý
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Končetiny	končetina	k1gFnPc1	končetina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
při	při	k7c6	při
plavání	plavání	k1gNnSc6	plavání
mají	mít	k5eAaImIp3nP	mít
přitisknuté	přitisknutý	k2eAgFnPc1d1	přitisknutá
k	k	k7c3	k
tělu	tělo	k1gNnSc3	tělo
<g/>
,	,	kIx,	,
jim	on	k3xPp3gMnPc3	on
slouží	sloužit	k5eAaImIp3nS	sloužit
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
nebo	nebo	k8xC	nebo
mělkém	mělký	k2eAgInSc6d1	mělký
dnu	den	k1gInSc6	den
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc1	oko
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
svrchní	svrchní	k2eAgFnSc6d1	svrchní
straně	strana	k1gFnSc6	strana
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jim	on	k3xPp3gMnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
sledování	sledování	k1gNnSc4	sledování
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zbytek	zbytek	k1gInSc1	zbytek
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
ponořen	ponořen	k2eAgMnSc1d1	ponořen
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
za	za	k7c7	za
očima	oko	k1gNnPc7	oko
jsou	být	k5eAaImIp3nP	být
ušní	ušní	k2eAgInPc4d1	ušní
otvory	otvor	k1gInPc4	otvor
<g/>
,	,	kIx,	,
chráněné	chráněný	k2eAgNnSc1d1	chráněné
kožním	kožní	k2eAgInSc7d1	kožní
záhybem	záhyb	k1gInSc7	záhyb
<g/>
.	.	kIx.	.
</s>
<s>
Komunikace	komunikace	k1gFnSc1	komunikace
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
zvuků	zvuk	k1gInPc2	zvuk
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
mj.	mj.	kA	mj.
mezi	mezi	k7c7	mezi
rodiči	rodič	k1gMnPc7	rodič
a	a	k8xC	a
mláďaty	mládě	k1gNnPc7	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Svaly	sval	k1gInPc1	sval
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
upínají	upínat	k5eAaImIp3nP	upínat
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
čelisti	čelist	k1gFnSc6	čelist
<g/>
,	,	kIx,	,
dokáží	dokázat	k5eAaPmIp3nP	dokázat
vyvinout	vyvinout	k5eAaPmF	vyvinout
obrovský	obrovský	k2eAgInSc4d1	obrovský
tlak	tlak	k1gInSc4	tlak
při	při	k7c6	při
skousávání	skousávání	k1gNnSc6	skousávání
své	svůj	k3xOyFgFnSc2	svůj
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
schopný	schopný	k2eAgMnSc1d1	schopný
rozdrtit	rozdrtit	k5eAaPmF	rozdrtit
i	i	k9	i
kosti	kost	k1gFnPc1	kost
<g/>
;	;	kIx,	;
naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
otevírání	otevírání	k1gNnSc4	otevírání
čelistí	čelist	k1gFnPc2	čelist
tolik	tolik	k6eAd1	tolik
síly	síla	k1gFnSc2	síla
nepotřebují	potřebovat	k5eNaImIp3nP	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Žaludek	žaludek	k1gInSc1	žaludek
krokodýla	krokodýl	k1gMnSc2	krokodýl
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
přibližně	přibližně	k6eAd1	přibližně
jako	jako	k8xC	jako
basketbalový	basketbalový	k2eAgInSc4d1	basketbalový
míč	míč	k1gInSc4	míč
<g/>
.	.	kIx.	.
</s>
<s>
Krokodýli	krokodýl	k1gMnPc1	krokodýl
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
značných	značný	k2eAgFnPc2d1	značná
velikostí	velikost	k1gFnPc2	velikost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jim	on	k3xPp3gMnPc3	on
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
napadnout	napadnout	k5eAaPmF	napadnout
a	a	k8xC	a
zabít	zabít	k5eAaPmF	zabít
velké	velký	k2eAgInPc4d1	velký
druhy	druh	k1gInPc4	druh
skotu	skot	k1gInSc2	skot
i	i	k8xC	i
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
známým	známý	k2eAgInSc7d1	známý
druhem	druh	k1gInSc7	druh
krokodýla	krokodýl	k1gMnSc2	krokodýl
je	být	k5eAaImIp3nS	být
krokodýl	krokodýl	k1gMnSc1	krokodýl
mořský	mořský	k2eAgMnSc1d1	mořský
(	(	kIx(	(
<g/>
Crocodilus	Crocodilus	k1gMnSc1	Crocodilus
porosus	porosus	k1gMnSc1	porosus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
délky	délka	k1gFnPc4	délka
i	i	k9	i
přes	přes	k7c4	přes
7	[number]	k4	7
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc2	hmotnost
kolem	kolem	k7c2	kolem
1	[number]	k4	1
tuny	tuna	k1gFnSc2	tuna
(	(	kIx(	(
<g/>
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
největším	veliký	k2eAgMnSc7d3	veliký
žijícím	žijící	k2eAgMnSc7d1	žijící
plazem	plaz	k1gMnSc7	plaz
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
