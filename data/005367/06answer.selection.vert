<s>
Krokodýli	krokodýl	k1gMnPc1	krokodýl
jsou	být	k5eAaImIp3nP	být
predátoři	predátor	k1gMnPc1	predátor
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
tropických	tropický	k2eAgFnPc6d1	tropická
oblastech	oblast	k1gFnPc6	oblast
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
Ameriky	Amerika	k1gFnSc2	Amerika
i	i	k8xC	i
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
