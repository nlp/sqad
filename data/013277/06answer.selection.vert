<s>
Kefíja	Kefíja	k1gFnSc1	Kefíja
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
<g/>
:	:	kIx,	:
ك	ك	k?	ك
<g/>
,	,	kIx,	,
kū	kū	k?	kū
<g/>
,	,	kIx,	,
pl.	pl.	k?	pl.
ك	ك	k?	ك
<g/>
,	,	kIx,	,
kū	kū	k?	kū
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
šemag	šemag	k1gMnSc1	šemag
či	či	k8xC	či
hovorově	hovorově	k6eAd1	hovorově
palestina	palestin	k2eAgFnSc1d1	palestin
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgFnSc1d1	tradiční
arabská	arabský	k2eAgFnSc1d1	arabská
mužská	mužský	k2eAgFnSc1d1	mužská
pokrývka	pokrývka	k1gFnSc1	pokrývka
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
