<p>
<s>
Kefíja	Kefíja	k1gFnSc1	Kefíja
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
<g/>
:	:	kIx,	:
ك	ك	k?	ك
<g/>
,	,	kIx,	,
kū	kū	k?	kū
<g/>
,	,	kIx,	,
pl.	pl.	k?	pl.
ك	ك	k?	ك
<g/>
,	,	kIx,	,
kū	kū	k?	kū
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
též	též	k9	též
šemag	šemag	k1gMnSc1	šemag
či	či	k8xC	či
hovorově	hovorově	k6eAd1	hovorově
palestina	palestin	k2eAgFnSc1d1	palestin
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgFnSc1d1	tradiční
arabská	arabský	k2eAgFnSc1d1	arabská
mužská	mužský	k2eAgFnSc1d1	mužská
pokrývka	pokrývka	k1gFnSc1	pokrývka
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
ji	on	k3xPp3gFnSc4	on
čtvercová	čtvercový	k2eAgFnSc1d1	čtvercová
tkanina	tkanina	k1gFnSc1	tkanina
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
bavlněná	bavlněný	k2eAgFnSc1d1	bavlněná
<g/>
,	,	kIx,	,
různým	různý	k2eAgInSc7d1	různý
způsobem	způsob	k1gInSc7	způsob
přehnutá	přehnutý	k2eAgFnSc1d1	přehnutá
a	a	k8xC	a
složená	složený	k2eAgFnSc1d1	složená
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
pro	pro	k7c4	pro
oblasti	oblast	k1gFnPc4	oblast
se	s	k7c7	s
suchým	suchý	k2eAgNnSc7d1	suché
a	a	k8xC	a
teplým	teplý	k2eAgNnSc7d1	teplé
podnebím	podnebí	k1gNnSc7	podnebí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
ochranu	ochrana	k1gFnSc4	ochrana
proti	proti	k7c3	proti
přímému	přímý	k2eAgNnSc3d1	přímé
slunci	slunce	k1gNnSc3	slunce
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
tak	tak	k9	tak
příležitostně	příležitostně	k6eAd1	příležitostně
ochranu	ochrana	k1gFnSc4	ochrana
úst	ústa	k1gNnPc2	ústa
a	a	k8xC	a
očí	oko	k1gNnPc2	oko
proti	proti	k7c3	proti
zvířenému	zvířený	k2eAgInSc3d1	zvířený
prachu	prach	k1gInSc3	prach
a	a	k8xC	a
písku	písek	k1gInSc3	písek
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
různý	různý	k2eAgInSc4d1	různý
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
klasický	klasický	k2eAgInSc4d1	klasický
patří	patřit	k5eAaImIp3nS	patřit
kostkovaný	kostkovaný	k2eAgInSc4d1	kostkovaný
vzor	vzor	k1gInSc4	vzor
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
bílé	bílý	k2eAgFnSc2d1	bílá
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
(	(	kIx(	(
<g/>
např.	např.	kA	např.
černé	černý	k2eAgFnPc1d1	černá
<g/>
,	,	kIx,	,
červené	červený	k2eAgFnPc1d1	červená
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgFnPc1d1	zelená
<g/>
)	)	kIx)	)
barvy	barva	k1gFnPc1	barva
<g/>
.	.	kIx.	.
<g/>
Kefíja	Kefíja	k1gFnSc1	Kefíja
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
symbolem	symbol	k1gInSc7	symbol
palestinského	palestinský	k2eAgInSc2d1	palestinský
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
<g/>
.	.	kIx.	.
</s>
<s>
Zpopularizovaná	zpopularizovaný	k2eAgFnSc1d1	zpopularizovaná
byla	být	k5eAaImAgFnS	být
především	především	k9	především
vůdcem	vůdce	k1gMnSc7	vůdce
Organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
osvobození	osvobození	k1gNnSc4	osvobození
Palestiny	Palestina	k1gFnSc2	Palestina
Jásirem	Jásir	k1gMnSc7	Jásir
Arafatem	Arafat	k1gMnSc7	Arafat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
palestinskými	palestinský	k2eAgMnPc7d1	palestinský
Araby	Arab	k1gMnPc7	Arab
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
barvy	barva	k1gFnSc2	barva
kefíji	kefít	k5eAaPmIp1nS	kefít
možné	možný	k2eAgNnSc1d1	možné
identifikovat	identifikovat	k5eAaBmF	identifikovat
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
organizace	organizace	k1gFnSc2	organizace
či	či	k8xC	či
čím	čí	k3xOyQgNnSc7	čí
sympatizantem	sympatizant	k1gMnSc7	sympatizant
dotyčný	dotyčný	k2eAgMnSc1d1	dotyčný
je	být	k5eAaImIp3nS	být
(	(	kIx(	(
<g/>
zelenobílá	zelenobílý	k2eAgFnSc1d1	zelenobílá
kefíja	kefíja	k1gFnSc1	kefíja
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
Hamasu	Hamas	k1gInSc2	Hamas
<g/>
,	,	kIx,	,
černobílá	černobílý	k2eAgFnSc1d1	černobílá
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
Fatahu	Fatah	k1gInSc2	Fatah
a	a	k8xC	a
červenobílá	červenobílý	k2eAgFnSc1d1	červenobílá
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
Lidové	lidový	k2eAgFnSc2d1	lidová
fronty	fronta	k1gFnSc2	fronta
pro	pro	k7c4	pro
osvobození	osvobození	k1gNnSc4	osvobození
Palestiny	Palestina	k1gFnSc2	Palestina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
palestinské	palestinský	k2eAgMnPc4d1	palestinský
Araby	Arab	k1gMnPc4	Arab
však	však	k9	však
barva	barva	k1gFnSc1	barva
kefíji	kefít	k5eAaPmIp1nS	kefít
tyto	tento	k3xDgFnPc4	tento
asociace	asociace	k1gFnPc4	asociace
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kefíja	Kefíja	k1gFnSc1	Kefíja
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
i	i	k9	i
mimo	mimo	k7c4	mimo
arabské	arabský	k2eAgFnPc4d1	arabská
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
její	její	k3xOp3gNnSc4	její
nošení	nošení	k1gNnSc4	nošení
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
jak	jak	k6eAd1	jak
propalestinskou	propalestinský	k2eAgFnSc4d1	propalestinská
podporu	podpora	k1gFnSc4	podpora
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
využití	využití	k1gNnSc1	využití
obyčejného	obyčejný	k2eAgInSc2d1	obyčejný
módního	módní	k2eAgInSc2d1	módní
doplňku	doplněk	k1gInSc2	doplněk
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
i	i	k9	i
modrobílé	modrobílý	k2eAgFnSc3d1	modrobílá
kefíji	kefíj	k1gFnSc3	kefíj
s	s	k7c7	s
hebrejskými	hebrejský	k2eAgInPc7d1	hebrejský
nápisy	nápis	k1gInPc7	nápis
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
představují	představovat	k5eAaImIp3nP	představovat
proizraelskou	proizraelský	k2eAgFnSc4d1	proizraelská
podporu	podpora	k1gFnSc4	podpora
a	a	k8xC	a
hlášení	hlášení	k1gNnSc1	hlášení
se	se	k3xPyFc4	se
k	k	k7c3	k
židovskému	židovský	k2eAgInSc3d1	židovský
národu	národ	k1gInSc3	národ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Keffiyeh	Keffiyeha	k1gFnPc2	Keffiyeha
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Keffieh	Keffieh	k1gInSc4	Keffieh
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kefíja	Kefíj	k1gInSc2	Kefíj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Instructables	Instructables	k1gInSc1	Instructables
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
–	–	k?	–
Návod	návod	k1gInSc1	návod
jak	jak	k6eAd1	jak
si	se	k3xPyFc3	se
nasadit	nasadit	k5eAaPmF	nasadit
kefíju	kefíju	k5eAaPmIp1nS	kefíju
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Jerusalem	Jerusalem	k1gInSc1	Jerusalem
Post	post	k1gInSc1	post
–	–	k?	–
Pope	pop	k1gInSc5	pop
meets	meetsa	k1gFnPc2	meetsa
Palestinians	Palestiniansa	k1gFnPc2	Palestiniansa
in	in	k?	in
Vatican	Vatican	k1gMnSc1	Vatican
City	city	k1gNnSc1	city
<g/>
,	,	kIx,	,
receives	receives	k1gInSc1	receives
'	'	kIx"	'
<g/>
keffiyeh	keffiyeh	k1gInSc1	keffiyeh
<g/>
'	'	kIx"	'
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Aruc	Aruc	k1gInSc1	Aruc
Ševa	Šev	k1gInSc2	Šev
–	–	k?	–
Israeli	Israel	k1gInSc6	Israel
Keffiyah	Keffiyah	k1gInSc1	Keffiyah
Spreads	Spreads	k1gInSc1	Spreads
Beyond	Beyond	k1gInSc1	Beyond
the	the	k?	the
Zionist	Zionist	k1gInSc1	Zionist
Freedom	Freedom	k1gInSc1	Freedom
Alliance	Alliance	k1gFnSc1	Alliance
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
</s>
</p>
