<p>
<s>
Paul	Paul	k1gMnSc1	Paul
Andrews	Andrewsa	k1gFnPc2	Andrewsa
(	(	kIx(	(
<g/>
*	*	kIx~	*
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
1958	[number]	k4	1958
<g/>
,	,	kIx,	,
Chingford	Chingford	k1gInSc1	Chingford
<g/>
,	,	kIx,	,
Essex	Essex	k1gInSc1	Essex
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k1gMnSc1	známý
jako	jako	k8xS	jako
Paul	Paul	k1gMnSc1	Paul
Di	Di	k1gMnSc1	Di
<g/>
'	'	kIx"	'
<g/>
Anno	Anna	k1gFnSc5	Anna
je	on	k3xPp3gInPc4	on
bývalý	bývalý	k2eAgMnSc1d1	bývalý
zpěvák	zpěvák	k1gMnSc1	zpěvák
heavy	heava	k1gFnSc2	heava
metalové	metalový	k2eAgFnSc2d1	metalová
skupiny	skupina	k1gFnSc2	skupina
Iron	iron	k1gInSc1	iron
Maiden	Maidno	k1gNnPc2	Maidno
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1978	[number]	k4	1978
až	až	k9	až
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
Iron	iron	k1gInSc1	iron
Maiden	Maidna	k1gFnPc2	Maidna
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1981	[number]	k4	1981
vyhozen	vyhozen	k2eAgInSc1d1	vyhozen
kvůli	kvůli	k7c3	kvůli
alkoholu	alkohol	k1gInSc3	alkohol
a	a	k8xC	a
drogám	droga	k1gFnPc3	droga
<g/>
;	;	kIx,	;
poté	poté	k6eAd1	poté
vydal	vydat	k5eAaPmAgMnS	vydat
mnoho	mnoho	k4c4	mnoho
alb	album	k1gNnPc2	album
ať	ať	k8xC	ať
už	už	k6eAd1	už
sólově	sólově	k6eAd1	sólově
nebo	nebo	k8xC	nebo
jako	jako	k8xS	jako
člen	člen	k1gMnSc1	člen
skupin	skupina	k1gFnPc2	skupina
jako	jako	k9	jako
Gogmagog	Gogmagog	k1gMnSc1	Gogmagog
<g/>
,	,	kIx,	,
Di	Di	k1gMnSc1	Di
<g/>
'	'	kIx"	'
<g/>
Anno	Anna	k1gFnSc5	Anna
<g/>
,	,	kIx,	,
Battlezone	Battlezon	k1gMnSc5	Battlezon
<g/>
,	,	kIx,	,
Praying	Praying	k1gInSc4	Praying
Mantis	mantisa	k1gFnPc2	mantisa
a	a	k8xC	a
Killers	Killersa	k1gFnPc2	Killersa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
zvěsti	zvěst	k1gFnPc1	zvěst
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgInS	moct
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
Bruce	Bruce	k1gFnSc1	Bruce
Dickinsona	Dickinsona	k1gFnSc1	Dickinsona
do	do	k7c2	do
Iron	iron	k1gInSc4	iron
Maiden	Maidna	k1gFnPc2	Maidna
vrátit	vrátit	k5eAaPmF	vrátit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
klamné	klamný	k2eAgFnPc4d1	klamná
zvěsti	zvěst	k1gFnPc4	zvěst
<g/>
.	.	kIx.	.
</s>
</p>
