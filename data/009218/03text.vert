<p>
<s>
Vizigóti	Vizigót	k1gMnPc1	Vizigót
neboli	neboli	k8xC	neboli
Západní	západní	k2eAgMnPc1d1	západní
Gótové	Gót	k1gMnPc1	Gót
byli	být	k5eAaImAgMnP	být
kmen	kmen	k1gInSc4	kmen
východogermánského	východogermánský	k2eAgNnSc2d1	východogermánský
etnika	etnikum	k1gNnSc2	etnikum
Gótů	Gót	k1gMnPc2	Gót
hovořícího	hovořící	k2eAgInSc2d1	hovořící
gótštinou	gótština	k1gFnSc7	gótština
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
pocházeli	pocházet	k5eAaImAgMnP	pocházet
Gótové	Gót	k1gMnPc1	Gót
z	z	k7c2	z
jižního	jižní	k2eAgNnSc2d1	jižní
Švédska	Švédsko	k1gNnSc2	Švédsko
a	a	k8xC	a
ostrova	ostrov	k1gInSc2	ostrov
Gotlandu	Gotland	k1gInSc2	Gotland
<g/>
,	,	kIx,	,
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
se	se	k3xPyFc4	se
zapsali	zapsat	k5eAaPmAgMnP	zapsat
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
raného	raný	k2eAgInSc2d1	raný
středověku	středověk	k1gInSc2	středověk
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Vizigóti	Vizigót	k1gMnPc1	Vizigót
vznikli	vzniknout	k5eAaPmAgMnP	vzniknout
rozdělením	rozdělení	k1gNnSc7	rozdělení
gótského	gótský	k2eAgNnSc2d1	gótské
etnika	etnikum	k1gNnSc2	etnikum
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
nazývala	nazývat	k5eAaImAgFnS	nazývat
Ostrogóti	Ostrogót	k1gMnPc5	Ostrogót
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
a	a	k8xC	a
rané	raný	k2eAgFnPc1d1	raná
dějiny	dějiny	k1gFnPc1	dějiny
Gótů	Gót	k1gMnPc2	Gót
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Vizigóti	Vizigót	k1gMnPc1	Vizigót
v	v	k7c6	v
době	doba	k1gFnSc6	doba
stěhování	stěhování	k1gNnSc2	stěhování
národů	národ	k1gInPc2	národ
==	==	k?	==
</s>
</p>
<p>
<s>
Vizigóti	Vizigót	k1gMnPc1	Vizigót
bývají	bývat	k5eAaImIp3nP	bývat
spojováni	spojovat	k5eAaImNgMnP	spojovat
především	především	k9	především
se	s	k7c7	s
Španělskem	Španělsko	k1gNnSc7	Španělsko
<g/>
,	,	kIx,	,
antickou	antický	k2eAgFnSc7d1	antická
Hispánií	Hispánie	k1gFnSc7	Hispánie
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
Pyrenejský	pyrenejský	k2eAgInSc4d1	pyrenejský
poloostrov	poloostrov	k1gInSc4	poloostrov
však	však	k9	však
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
spletitá	spletitý	k2eAgFnSc1d1	spletitá
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
376	[number]	k4	376
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
Gótové	Gót	k1gMnPc1	Gót
přepadeni	přepaden	k2eAgMnPc1d1	přepaden
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Černomoří	Černomoří	k1gNnSc2	Černomoří
Huny	Hun	k1gMnPc4	Hun
<g/>
,	,	kIx,	,
stáhly	stáhnout	k5eAaPmAgInP	stáhnout
se	se	k3xPyFc4	se
některé	některý	k3yIgFnSc2	některý
skupiny	skupina	k1gFnSc2	skupina
za	za	k7c4	za
Dunaj	Dunaj	k1gInSc4	Dunaj
na	na	k7c6	na
území	území	k1gNnSc6	území
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
zde	zde	k6eAd1	zde
císařem	císař	k1gMnSc7	císař
Valentem	Valentem	k?	Valentem
usazeny	usadit	k5eAaPmNgFnP	usadit
roku	rok	k1gInSc2	rok
376	[number]	k4	376
jako	jako	k8xC	jako
foederáti	foederát	k1gMnPc1	foederát
v	v	k7c6	v
naději	naděje	k1gFnSc6	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
posílí	posílit	k5eAaPmIp3nS	posílit
obranu	obrana	k1gFnSc4	obrana
římské	římský	k2eAgFnSc2d1	římská
hranice	hranice	k1gFnSc2	hranice
proti	proti	k7c3	proti
dalším	další	k2eAgMnPc3d1	další
barbarům	barbar	k1gMnPc3	barbar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
říši	říš	k1gFnSc6	říš
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
panoval	panovat	k5eAaImAgInS	panovat
značný	značný	k2eAgInSc1d1	značný
nedostatek	nedostatek	k1gInSc1	nedostatek
pracovní	pracovní	k2eAgFnSc2d1	pracovní
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Snaha	snaha	k1gFnSc1	snaha
zotročit	zotročit	k5eAaPmF	zotročit
vizigótské	vizigótský	k2eAgMnPc4d1	vizigótský
bojovníky	bojovník	k1gMnPc4	bojovník
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
velkému	velký	k2eAgNnSc3d1	velké
protiřímskému	protiřímský	k2eAgNnSc3d1	protiřímské
povstání	povstání	k1gNnSc3	povstání
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
vzápětí	vzápětí	k6eAd1	vzápětí
přidalo	přidat	k5eAaPmAgNnS	přidat
zbídačelé	zbídačelý	k2eAgNnSc4d1	zbídačelé
místní	místní	k2eAgNnSc4d1	místní
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
,	,	kIx,	,
především	především	k9	především
otroci	otrok	k1gMnPc1	otrok
těžce	těžce	k6eAd1	těžce
pracující	pracující	k2eAgMnPc1d1	pracující
v	v	k7c6	v
thráckých	thrácký	k2eAgInPc6d1	thrácký
dolech	dol	k1gInPc6	dol
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
378	[number]	k4	378
se	se	k3xPyFc4	se
povstalcům	povstalec	k1gMnPc3	povstalec
postavilo	postavit	k5eAaPmAgNnS	postavit
římské	římský	k2eAgNnSc1d1	římské
vojsko	vojsko	k1gNnSc1	vojsko
u	u	k7c2	u
Adrianopole	Adrianopole	k1gFnSc2	Adrianopole
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
byli	být	k5eAaImAgMnP	být
tvrdě	tvrdě	k6eAd1	tvrdě
poraženi	poražen	k2eAgMnPc1d1	poražen
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
Valens	Valens	k1gInSc4	Valens
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
svým	svůj	k3xOyFgNnSc7	svůj
zraněním	zranění	k1gNnSc7	zranění
z	z	k7c2	z
bitvy	bitva	k1gFnSc2	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
380	[number]	k4	380
vtrhli	vtrhnout	k5eAaPmAgMnP	vtrhnout
Vizigóti	Vizigót	k1gMnPc1	Vizigót
do	do	k7c2	do
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Theodosia	Theodosium	k1gNnSc2	Theodosium
I.	I.	kA	I.
roku	rok	k1gInSc2	rok
379	[number]	k4	379
se	se	k3xPyFc4	se
novému	nový	k2eAgMnSc3d1	nový
císaři	císař	k1gMnSc3	císař
podařilo	podařit	k5eAaPmAgNnS	podařit
Góty	Gót	k1gMnPc4	Gót
pacifikovat	pacifikovat	k5eAaBmF	pacifikovat
obdarováním	obdarování	k1gNnSc7	obdarování
pozemky	pozemka	k1gFnPc1	pozemka
a	a	k8xC	a
dobytkem	dobytek	k1gInSc7	dobytek
<g/>
.	.	kIx.	.
</s>
<s>
Uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
přiměl	přimět	k5eAaPmAgMnS	přimět
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
do	do	k7c2	do
Moesie	Moesie	k1gFnSc2	Moesie
a	a	k8xC	a
Thrákie	Thrákie	k1gFnSc2	Thrákie
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
usídlil	usídlit	k5eAaPmAgMnS	usídlit
jako	jako	k8xC	jako
foederáty	foederát	k1gInPc4	foederát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
Alaricha	Alarich	k1gMnSc2	Alarich
I.	I.	kA	I.
(	(	kIx(	(
<g/>
395	[number]	k4	395
<g/>
–	–	k?	–
<g/>
410	[number]	k4	410
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
spíše	spíše	k9	spíše
vojenským	vojenský	k2eAgMnSc7d1	vojenský
vůdcem	vůdce	k1gMnSc7	vůdce
svého	svůj	k3xOyFgInSc2	svůj
lidu	lid	k1gInSc2	lid
než	než	k8xS	než
skutečným	skutečný	k2eAgMnSc7d1	skutečný
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
podnikali	podnikat	k5eAaImAgMnP	podnikat
Vizigóti	Vizigót	k1gMnPc1	Vizigót
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
tažení	tažení	k1gNnPc4	tažení
na	na	k7c4	na
východ	východ	k1gInSc4	východ
i	i	k9	i
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
vytáhli	vytáhnout	k5eAaPmAgMnP	vytáhnout
proti	proti	k7c3	proti
Konstantinopoli	Konstantinopol	k1gInSc3	Konstantinopol
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgInS	snažit
se	se	k3xPyFc4	se
císařský	císařský	k2eAgInSc1d1	císařský
dvůr	dvůr	k1gInSc1	dvůr
zbavit	zbavit	k5eAaPmF	zbavit
tohoto	tento	k3xDgMnSc4	tento
nebezpečného	bezpečný	k2eNgMnSc4d1	nebezpečný
protivníka	protivník	k1gMnSc4	protivník
<g/>
,	,	kIx,	,
a	a	k8xC	a
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
proto	proto	k8xC	proto
Alarichovu	Alarichův	k2eAgFnSc4d1	Alarichova
pozornost	pozornost	k1gFnSc4	pozornost
do	do	k7c2	do
západořímské	západořímský	k2eAgFnSc2d1	Západořímská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zápasila	zápasit	k5eAaImAgFnS	zápasit
s	s	k7c7	s
útoky	útok	k1gInPc7	útok
mnoha	mnoho	k4c2	mnoho
barbarských	barbarský	k2eAgInPc2d1	barbarský
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Vizigóti	Vizigót	k1gMnPc1	Vizigót
došli	dojít	k5eAaPmAgMnP	dojít
roku	rok	k1gInSc2	rok
395	[number]	k4	395
až	až	k9	až
do	do	k7c2	do
Panonie	Panonie	k1gFnSc2	Panonie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
na	na	k7c4	na
východ	východ	k1gInSc4	východ
a	a	k8xC	a
zpustošili	zpustošit	k5eAaPmAgMnP	zpustošit
Řecko	Řecko	k1gNnSc4	Řecko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alarich	Alarich	k1gMnSc1	Alarich
se	se	k3xPyFc4	se
představy	představa	k1gFnSc2	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
dobude	dobýt	k5eAaPmIp3nS	dobýt
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
,	,	kIx,	,
nevzdal	vzdát	k5eNaPmAgInS	vzdát
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
nového	nový	k2eAgNnSc2d1	nové
století	století	k1gNnSc2	století
odvedl	odvést	k5eAaPmAgInS	odvést
Vizigóty	Vizigót	k1gMnPc4	Vizigót
na	na	k7c4	na
západ	západ	k1gInSc4	západ
<g/>
.	.	kIx.	.
</s>
<s>
Využil	využít	k5eAaPmAgInS	využít
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vynikající	vynikající	k2eAgMnSc1d1	vynikající
římský	římský	k2eAgMnSc1d1	římský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
Stilicho	Stilicha	k1gFnSc5	Stilicha
byl	být	k5eAaImAgInS	být
zaměstnán	zaměstnat	k5eAaPmNgMnS	zaměstnat
mimo	mimo	k7c4	mimo
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
západořímský	západořímský	k2eAgMnSc1d1	západořímský
císař	císař	k1gMnSc1	císař
Honorius	Honorius	k1gMnSc1	Honorius
přesídlil	přesídlit	k5eAaPmAgMnS	přesídlit
ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
z	z	k7c2	z
Mediolana	Mediolan	k1gMnSc2	Mediolan
do	do	k7c2	do
Ravenny	Ravenna	k1gFnSc2	Ravenna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
401	[number]	k4	401
přitáhli	přitáhnout	k5eAaPmAgMnP	přitáhnout
barbaři	barbar	k1gMnPc1	barbar
k	k	k7c3	k
Římu	Řím	k1gInSc3	Řím
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
jeho	jeho	k3xOp3gNnSc2	jeho
dobývání	dobývání	k1gNnSc2	dobývání
upustili	upustit	k5eAaPmAgMnP	upustit
po	po	k7c6	po
odražení	odražení	k1gNnSc6	odražení
útoku	útok	k1gInSc2	útok
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
402	[number]	k4	402
a	a	k8xC	a
403	[number]	k4	403
byli	být	k5eAaImAgMnP	být
poraženi	poražen	k2eAgMnPc1d1	poražen
vojsky	vojsky	k6eAd1	vojsky
Stilichona	Stilichona	k1gFnSc1	Stilichona
a	a	k8xC	a
přinuceni	přinutit	k5eAaPmNgMnP	přinutit
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
na	na	k7c4	na
Balkán	Balkán	k1gInSc4	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
však	však	k9	však
Alarich	Alarich	k1gInSc1	Alarich
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
se	s	k7c7	s
Stilichonem	Stilichon	k1gInSc7	Stilichon
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
společném	společný	k2eAgInSc6d1	společný
útoku	útok	k1gInSc6	útok
proti	proti	k7c3	proti
východořímské	východořímský	k2eAgFnSc3d1	Východořímská
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
neuskutečnil	uskutečnit	k5eNaPmAgInS	uskutečnit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Stilicho	Stilicha	k1gFnSc5	Stilicha
musel	muset	k5eAaImAgMnS	muset
bránit	bránit	k5eAaImF	bránit
Itálii	Itálie	k1gFnSc4	Itálie
a	a	k8xC	a
Galii	Galie	k1gFnSc4	Galie
před	před	k7c7	před
barbarskými	barbarský	k2eAgInPc7d1	barbarský
útoky	útok	k1gInPc7	útok
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgMnS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alarich	Alarich	k1gMnSc1	Alarich
požadoval	požadovat	k5eAaImAgMnS	požadovat
za	za	k7c4	za
neuskutečněnou	uskutečněný	k2eNgFnSc4d1	neuskutečněná
dohodu	dohoda	k1gFnSc4	dohoda
výkupné	výkupný	k2eAgFnSc2d1	výkupná
a	a	k8xC	a
roku	rok	k1gInSc2	rok
409	[number]	k4	409
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
znovu	znovu	k6eAd1	znovu
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
velkému	velký	k2eAgNnSc3d1	velké
zděšení	zděšení	k1gNnSc3	zděšení
obyvatel	obyvatel	k1gMnPc2	obyvatel
Itálie	Itálie	k1gFnSc2	Itálie
padl	padnout	k5eAaPmAgInS	padnout
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
410	[number]	k4	410
pod	pod	k7c7	pod
náporem	nápor	k1gInSc7	nápor
Vizigótů	Vizigót	k1gMnPc2	Vizigót
Řím	Řím	k1gInSc4	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc4	ten
první	první	k4xOgMnPc1	první
barbaři	barbar	k1gMnPc1	barbar
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
město	město	k1gNnSc1	město
dobýt	dobýt	k5eAaPmF	dobýt
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
však	však	k9	však
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
přeplaví	přeplavit	k5eAaPmIp3nS	přeplavit
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
do	do	k7c2	do
římské	římský	k2eAgFnSc2d1	římská
provincie	provincie	k1gFnSc2	provincie
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
rozkládala	rozkládat	k5eAaImAgFnS	rozkládat
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
Tunisu	Tunis	k1gInSc2	Tunis
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
na	na	k7c4	na
italský	italský	k2eAgInSc4d1	italský
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Loďstvo	loďstvo	k1gNnSc1	loďstvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	on	k3xPp3gMnPc4	on
mělo	mít	k5eAaImAgNnS	mít
přepravit	přepravit	k5eAaPmF	přepravit
na	na	k7c4	na
africké	africký	k2eAgNnSc4d1	africké
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
,	,	kIx,	,
však	však	k9	však
ztroskotalo	ztroskotat	k5eAaPmAgNnS	ztroskotat
a	a	k8xC	a
Alarich	Alarich	k1gMnSc1	Alarich
nedlouho	nedlouho	k1gNnSc4	nedlouho
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Říše	říš	k1gFnPc1	říš
Vizigótů	Vizigót	k1gMnPc2	Vizigót
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Založení	založení	k1gNnSc1	založení
tolosánského	tolosánský	k2eAgNnSc2d1	tolosánský
království	království	k1gNnSc2	království
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Vizigótská	vizigótský	k2eAgFnSc1d1	Vizigótská
říše	říše	k1gFnSc1	říše
v	v	k7c6	v
Hispánii	Hispánie	k1gFnSc6	Hispánie
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
vizigótských	vizigótský	k2eAgMnPc2d1	vizigótský
králů	král	k1gMnPc2	král
</s>
</p>
<p>
<s>
Foederati	Foederat	k1gMnPc1	Foederat
(	(	kIx(	(
<g/>
Gótové	Gót	k1gMnPc1	Gót
jako	jako	k8xC	jako
římští	římský	k2eAgMnPc1d1	římský
federáti	federát	k1gMnPc1	federát
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vizigóti	Vizigót	k1gMnPc1	Vizigót
&	&	k?	&
Západní	západní	k2eAgMnPc1d1	západní
Gótové	Gót	k1gMnPc1	Gót
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Vizigóti	Vizigót	k1gMnPc1	Vizigót
na	na	k7c6	na
Antice	antika	k1gFnSc6	antika
</s>
</p>
<p>
<s>
Svět	svět	k1gInSc1	svět
barbarů	barbar	k1gMnPc2	barbar
</s>
</p>
