<s>
Kodikologie	Kodikologie	k1gFnSc1	Kodikologie
je	být	k5eAaImIp3nS	být
pomocná	pomocný	k2eAgFnSc1d1	pomocná
věda	věda	k1gFnSc1	věda
historická	historický	k2eAgFnSc1d1	historická
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
studuje	studovat	k5eAaImIp3nS	studovat
rukopisné	rukopisný	k2eAgFnSc2d1	rukopisná
knihy	kniha	k1gFnSc2	kniha
neúřední	úřední	k2eNgFnSc2d1	neúřední
(	(	kIx(	(
<g/>
literární	literární	k2eAgFnSc2d1	literární
<g/>
)	)	kIx)	)
povahy	povaha	k1gFnSc2	povaha
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
kodexy	kodex	k1gInPc7	kodex
<g/>
.	.	kIx.	.
</s>
