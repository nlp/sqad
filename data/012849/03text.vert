<p>
<s>
Kodikologie	Kodikologie	k1gFnSc1	Kodikologie
je	být	k5eAaImIp3nS	být
pomocná	pomocný	k2eAgFnSc1d1	pomocná
věda	věda	k1gFnSc1	věda
historická	historický	k2eAgFnSc1d1	historická
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
studuje	studovat	k5eAaImIp3nS	studovat
rukopisné	rukopisný	k2eAgFnSc2d1	rukopisná
knihy	kniha	k1gFnSc2	kniha
neúřední	úřední	k2eNgFnSc2d1	neúřední
(	(	kIx(	(
<g/>
literární	literární	k2eAgFnSc2d1	literární
<g/>
)	)	kIx)	)
povahy	povaha	k1gFnSc2	povaha
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
kodexy	kodex	k1gInPc7	kodex
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
je	on	k3xPp3gInPc4	on
jako	jako	k8xC	jako
jedinečné	jedinečný	k2eAgInPc4d1	jedinečný
objekty	objekt	k1gInPc4	objekt
a	a	k8xC	a
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
jejich	jejich	k3xOp3gInSc4	jejich
vznik	vznik	k1gInSc4	vznik
a	a	k8xC	a
funkci	funkce	k1gFnSc4	funkce
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Sleduje	sledovat	k5eAaImIp3nS	sledovat
vnitřní	vnitřní	k2eAgFnPc4d1	vnitřní
proměny	proměna	k1gFnPc4	proměna
a	a	k8xC	a
osudy	osud	k1gInPc4	osud
literárních	literární	k2eAgInPc2d1	literární
rukopisů	rukopis	k1gInPc2	rukopis
i	i	k9	i
jako	jako	k9	jako
součásti	součást	k1gFnPc1	součást
určitých	určitý	k2eAgInPc2d1	určitý
celků	celek	k1gInPc2	celek
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
skriptorií	skriptorium	k1gNnPc2	skriptorium
<g/>
,	,	kIx,	,
iluminátorských	iluminátorský	k2eAgFnPc2d1	iluminátorská
dílen	dílna	k1gFnPc2	dílna
a	a	k8xC	a
knihoven	knihovna	k1gFnPc2	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
i	i	k9	i
problémy	problém	k1gInPc1	problém
penetrace	penetrace	k1gFnSc2	penetrace
(	(	kIx(	(
<g/>
putováním	putování	k1gNnSc7	putování
rukopisů	rukopis	k1gInPc2	rukopis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
obchodem	obchod	k1gInSc7	obchod
s	s	k7c7	s
rukopisnými	rukopisný	k2eAgInPc7d1	rukopisný
kodexy	kodex	k1gInPc7	kodex
<g/>
,	,	kIx,	,
dějinami	dějiny	k1gFnPc7	dějiny
středověkých	středověký	k2eAgFnPc2d1	středověká
knihoven	knihovna	k1gFnPc2	knihovna
a	a	k8xC	a
v	v	k7c6	v
novověku	novověk	k1gInSc6	novověk
vývojem	vývoj	k1gInSc7	vývoj
rukopisných	rukopisný	k2eAgNnPc2d1	rukopisné
oddělení	oddělení	k1gNnPc2	oddělení
knihoven	knihovna	k1gFnPc2	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Kodex	kodex	k1gInSc1	kodex
sloužil	sloužit	k5eAaImAgInS	sloužit
k	k	k7c3	k
uchování	uchování	k1gNnSc3	uchování
a	a	k8xC	a
předávání	předávání	k1gNnSc3	předávání
společensky	společensky	k6eAd1	společensky
zajímavých	zajímavý	k2eAgFnPc2d1	zajímavá
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
zapsaných	zapsaný	k2eAgFnPc2d1	zapsaná
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kodikologie	Kodikologie	k1gFnSc1	Kodikologie
je	být	k5eAaImIp3nS	být
velkým	velký	k2eAgInSc7d1	velký
přínosem	přínos	k1gInSc7	přínos
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
historiografii	historiografie	k1gFnSc4	historiografie
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Kodikologii	Kodikologie	k1gFnSc4	Kodikologie
využívají	využívat	k5eAaPmIp3nP	využívat
i	i	k9	i
dějiny	dějiny	k1gFnPc1	dějiny
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
kodex	kodex	k1gInSc1	kodex
původně	původně	k6eAd1	původně
označoval	označovat	k5eAaImAgInS	označovat
svazek	svazek	k1gInSc1	svazek
voskových	voskový	k2eAgFnPc2d1	vosková
destiček	destička	k1gFnPc2	destička
či	či	k8xC	či
svitek	svitek	k1gInSc1	svitek
papyrusu	papyrus	k1gInSc2	papyrus
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
byl	být	k5eAaImAgInS	být
přenesen	přenést	k5eAaPmNgInS	přenést
na	na	k7c4	na
mnohalistovou	mnohalistový	k2eAgFnSc4d1	mnohalistový
knihu	kniha	k1gFnSc4	kniha
<g/>
,	,	kIx,	,
do	do	k7c2	do
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
pergamenovou	pergamenový	k2eAgFnSc7d1	pergamenová
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
papírovou	papírový	k2eAgFnSc4d1	papírová
(	(	kIx(	(
<g/>
nejstarší	starý	k2eAgFnSc1d3	nejstarší
papírna	papírna	k1gFnSc1	papírna
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1490	[number]	k4	1490
–	–	k?	–
Zbraslav	Zbraslav	k1gFnSc1	Zbraslav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
formou	forma	k1gFnSc7	forma
dokumentu	dokument	k1gInSc2	dokument
byl	být	k5eAaImAgMnS	být
rotulus	rotulus	k1gInSc4	rotulus
<g/>
,	,	kIx,	,
svitek	svitek	k1gInSc4	svitek
<g/>
.	.	kIx.	.
</s>
<s>
Kodikologie	Kodikologie	k1gFnSc1	Kodikologie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
až	až	k9	až
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
světového	světový	k2eAgMnSc2d1	světový
zakladatele	zakladatel	k1gMnSc2	zakladatel
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
Alphonse	Alphonsa	k1gFnSc3	Alphonsa
(	(	kIx(	(
<g/>
Alain	Alain	k1gMnSc1	Alain
<g/>
)	)	kIx)	)
Dain	Dain	k1gMnSc1	Dain
<g/>
,	,	kIx,	,
zakladatelem	zakladatel	k1gMnSc7	zakladatel
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
Václav	Václav	k1gMnSc1	Václav
Vojtíšek	Vojtíšek	k1gMnSc1	Vojtíšek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
rukopisy	rukopis	k1gInPc7	rukopis
zabývá	zabývat	k5eAaImIp3nS	zabývat
hlavně	hlavně	k9	hlavně
Komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
soupis	soupis	k1gInSc4	soupis
rukopisů	rukopis	k1gInPc2	rukopis
při	při	k7c6	při
AV	AV	kA	AV
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
založena	založen	k2eAgFnSc1d1	založena
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
časopis	časopis	k1gInSc1	časopis
Studie	studie	k1gFnSc2	studie
o	o	k7c6	o
rukopisech	rukopis	k1gInPc6	rukopis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kodikologie	Kodikologie	k1gFnSc1	Kodikologie
tedy	tedy	k9	tedy
studuje	studovat	k5eAaImIp3nS	studovat
rukopisy	rukopis	k1gInPc4	rukopis
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nebyly	být	k5eNaImAgInP	být
vedeny	vést	k5eAaImNgInP	vést
jako	jako	k9	jako
úřední	úřední	k2eAgFnPc1d1	úřední
knihy	kniha	k1gFnPc1	kniha
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gNnPc1	jejichž
původní	původní	k2eAgNnPc1d1	původní
sepsání	sepsání	k1gNnPc1	sepsání
a	a	k8xC	a
pozdější	pozdní	k2eAgInPc1d2	pozdější
opisy	opis	k1gInPc1	opis
sledovaly	sledovat	k5eAaImAgInP	sledovat
literární	literární	k2eAgInPc4d1	literární
<g/>
,	,	kIx,	,
vědecké	vědecký	k2eAgInPc4d1	vědecký
či	či	k8xC	či
dokumentační	dokumentační	k2eAgInPc4d1	dokumentační
cíle	cíl	k1gInPc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Kodikologie	Kodikologie	k1gFnSc1	Kodikologie
má	mít	k5eAaImIp3nS	mít
především	především	k6eAd1	především
antické	antický	k2eAgInPc4d1	antický
či	či	k8xC	či
středověké	středověký	k2eAgInPc4d1	středověký
objekty	objekt	k1gInPc4	objekt
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
rukopisná	rukopisný	k2eAgFnSc1d1	rukopisná
kniha	kniha	k1gFnSc1	kniha
se	se	k3xPyFc4	se
vyskytovala	vyskytovat	k5eAaImAgFnS	vyskytovat
do	do	k7c2	do
vynálezu	vynález	k1gInSc2	vynález
knihtisku	knihtisk	k1gInSc2	knihtisk
(	(	kIx(	(
<g/>
v	v	k7c6	v
pol.	pol.	k?	pol.
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
–	–	k?	–
Johannes	Johannes	k1gMnSc1	Johannes
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
pak	pak	k6eAd1	pak
navazuje	navazovat	k5eAaImIp3nS	navazovat
knihověda	knihověda	k1gFnSc1	knihověda
(	(	kIx(	(
<g/>
zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
tištěnými	tištěný	k2eAgFnPc7d1	tištěná
knihami	kniha	k1gFnPc7	kniha
<g/>
)	)	kIx)	)
a	a	k8xC	a
bibliologie	bibliologie	k1gFnSc1	bibliologie
(	(	kIx(	(
<g/>
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
knize	kniha	k1gFnSc6	kniha
vůbec	vůbec	k9	vůbec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Funkce	funkce	k1gFnSc1	funkce
rukopisu	rukopis	k1gInSc2	rukopis
se	se	k3xPyFc4	se
měnila	měnit	k5eAaImAgFnS	měnit
podle	podle	k7c2	podle
držby	držba	k1gFnSc2	držba
<g/>
/	/	kIx~	/
<g/>
itele	itele	k1gInSc1	itele
kodexu	kodex	k1gInSc2	kodex
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
se	se	k3xPyFc4	se
primární	primární	k2eAgFnSc1d1	primární
(	(	kIx(	(
<g/>
původní	původní	k2eAgFnSc1d1	původní
<g/>
)	)	kIx)	)
funkce	funkce	k1gFnSc1	funkce
a	a	k8xC	a
sekundární	sekundární	k2eAgFnSc1d1	sekundární
(	(	kIx(	(
<g/>
druhotná	druhotný	k2eAgFnSc1d1	druhotná
<g/>
)	)	kIx)	)
funkce	funkce	k1gFnSc1	funkce
–	–	k?	–
těch	ten	k3xDgInPc2	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
primární	primární	k2eAgFnSc2d1	primární
funkce	funkce	k1gFnSc2	funkce
dělíme	dělit	k5eAaImIp1nP	dělit
kodexy	kodex	k1gInPc4	kodex
na	na	k7c4	na
4	[number]	k4	4
skupiny	skupina	k1gFnSc2	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
liturgické	liturgický	k2eAgInPc1d1	liturgický
a	a	k8xC	a
náboženské	náboženský	k2eAgInPc1d1	náboženský
rukopisy	rukopis	k1gInPc1	rukopis
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
studijní	studijní	k2eAgInPc1d1	studijní
rukopisy	rukopis	k1gInPc1	rukopis
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
odborné	odborný	k2eAgInPc4d1	odborný
a	a	k8xC	a
vědecké	vědecký	k2eAgInPc4d1	vědecký
rukopisy	rukopis	k1gInPc4	rukopis
i	i	k8xC	i
školní	školní	k2eAgFnPc4d1	školní
učebnice	učebnice	k1gFnPc4	učebnice
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
nestudijní	studijní	k2eNgInPc1d1	studijní
rukopisy	rukopis	k1gInPc1	rukopis
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
beletrie	beletrie	k1gFnSc2	beletrie
<g/>
,	,	kIx,	,
poezie	poezie	k1gFnSc2	poezie
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
rukopisy	rukopis	k1gInPc1	rukopis
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
jako	jako	k8xC	jako
zábavná	zábavný	k2eAgFnSc1d1	zábavná
četba	četba	k1gFnSc1	četba
bez	bez	k7c2	bez
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
odborného	odborný	k2eAgInSc2d1	odborný
cíle	cíl	k1gInSc2	cíl
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
reprezentační	reprezentační	k2eAgInPc1d1	reprezentační
a	a	k8xC	a
bibliofilské	bibliofilský	k2eAgInPc1d1	bibliofilský
rukopisy	rukopis	k1gInPc1	rukopis
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
všechny	všechen	k3xTgInPc4	všechen
honosně	honosně	k6eAd1	honosně
vyzdobené	vyzdobený	k2eAgInPc4d1	vyzdobený
rukopisy	rukopis	k1gInPc4	rukopis
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
obsah	obsah	k1gInSc4	obsah
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
krásné	krásný	k2eAgFnPc1d1	krásná
knihy	kniha	k1gFnPc1	kniha
mohly	moct	k5eAaImAgFnP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
náboženská	náboženský	k2eAgFnSc1d1	náboženská
<g/>
,	,	kIx,	,
studijní	studijní	k2eAgFnSc1d1	studijní
i	i	k8xC	i
nestudijní	studijní	k2eNgNnPc1d1	studijní
témata	téma	k1gNnPc1	téma
<g/>
.	.	kIx.	.
<g/>
Součástí	součást	k1gFnSc7	součást
výzkumu	výzkum	k1gInSc2	výzkum
kodikologie	kodikologie	k1gFnSc2	kodikologie
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
další	další	k2eAgInPc1d1	další
osudy	osud	k1gInPc1	osud
kodexů	kodex	k1gInPc2	kodex
po	po	k7c6	po
jejich	jejich	k3xOp3gInSc6	jejich
vzniku	vznik	k1gInSc6	vznik
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
rukopisy	rukopis	k1gInPc7	rukopis
a	a	k8xC	a
dějiny	dějiny	k1gFnPc4	dějiny
kodexových	kodexový	k2eAgFnPc2d1	kodexová
knihoven	knihovna	k1gFnPc2	knihovna
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
jak	jak	k8xS	jak
knihoven	knihovna	k1gFnPc2	knihovna
institucionálních	institucionální	k2eAgFnPc2d1	institucionální
(	(	kIx(	(
<g/>
církevních	církevní	k2eAgFnPc2d1	církevní
a	a	k8xC	a
světských	světský	k2eAgFnPc2d1	světská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
knihoven	knihovna	k1gFnPc2	knihovna
individuálních	individuální	k2eAgFnPc2d1	individuální
<g/>
,	,	kIx,	,
soukromých	soukromý	k2eAgFnPc2d1	soukromá
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
knihoven	knihovna	k1gFnPc2	knihovna
obecně	obecně	k6eAd1	obecně
tvoří	tvořit	k5eAaImIp3nP	tvořit
součást	součást	k1gFnSc4	součást
bibliografie	bibliografie	k1gFnSc2	bibliografie
<g/>
.	.	kIx.	.
</s>
</p>
