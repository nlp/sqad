<s>
Kryštof	Kryštof	k1gMnSc1	Kryštof
Dientzenhofer	Dientzenhofer	k1gMnSc1	Dientzenhofer
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1655	[number]	k4	1655
<g/>
,	,	kIx,	,
St.	st.	kA	st.
Margarethen	Margarethen	k1gInSc1	Margarethen
u	u	k7c2	u
Brannenburgu	Brannenburg	k1gInSc2	Brannenburg
<g/>
,	,	kIx,	,
Bavorsko	Bavorsko	k1gNnSc1	Bavorsko
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1722	[number]	k4	1722
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
stavitel	stavitel	k1gMnSc1	stavitel
a	a	k8xC	a
architekt	architekt	k1gMnSc1	architekt
německého	německý	k2eAgInSc2d1	německý
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
představitelem	představitel	k1gMnSc7	představitel
vrcholného	vrcholný	k2eAgNnSc2d1	vrcholné
baroka	baroko	k1gNnSc2	baroko
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
mu	on	k3xPp3gMnSc3	on
připisována	připisován	k2eAgFnSc1d1	připisována
skupina	skupina	k1gFnSc1	skupina
staveb	stavba	k1gFnPc2	stavba
dynamického	dynamický	k2eAgNnSc2d1	dynamické
baroka	baroko	k1gNnSc2	baroko
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yIgMnPc3	který
patří	patřit	k5eAaImIp3nS	patřit
kaple	kaple	k1gFnSc1	kaple
Zjevení	zjevení	k1gNnSc1	zjevení
Páně	páně	k2eAgFnSc1d1	páně
ve	v	k7c6	v
Smiřicích	Smiřice	k1gFnPc6	Smiřice
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Malé	Malé	k2eAgFnSc6d1	Malé
Straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Markéty	Markéta	k1gFnSc2	Markéta
v	v	k7c6	v
břevnovském	břevnovský	k2eAgInSc6d1	břevnovský
klášteře	klášter	k1gInSc6	klášter
a	a	k8xC	a
kostel	kostel	k1gInSc1	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Kláry	Klára	k1gFnSc2	Klára
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgInSc1d1	barokní
areál	areál	k1gInSc1	areál
Skalka	Skalka	k1gMnSc1	Skalka
v	v	k7c6	v
Mníšku	Mníšek	k1gInSc6	Mníšek
pod	pod	k7c7	pod
Brdy	Brdy	k1gInPc7	Brdy
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgFnPc6d1	další
stavbách	stavba	k1gFnPc6	stavba
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
působil	působit	k5eAaImAgMnS	působit
také	také	k9	také
jako	jako	k9	jako
pevnostní	pevnostní	k2eAgMnSc1d1	pevnostní
stavitel	stavitel	k1gMnSc1	stavitel
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
a	a	k8xC	a
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
jako	jako	k8xS	jako
klášterní	klášterní	k2eAgMnSc1d1	klášterní
stavitel	stavitel	k1gMnSc1	stavitel
břevnovsko-broumovského	břevnovskoroumovský	k2eAgInSc2d1	břevnovsko-broumovský
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
synem	syn	k1gMnSc7	syn
byl	být	k5eAaImAgMnS	být
slavný	slavný	k2eAgMnSc1d1	slavný
barokní	barokní	k2eAgMnSc1d1	barokní
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
stavitel	stavitel	k1gMnSc1	stavitel
Kilián	Kilián	k1gMnSc1	Kilián
Ignác	Ignác	k1gMnSc1	Ignác
Dientzenhofer	Dientzenhofer	k1gMnSc1	Dientzenhofer
<g/>
.	.	kIx.	.
</s>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
hornobavorské	hornobavorský	k2eAgFnSc2d1	hornobavorský
stavitelské	stavitelský	k2eAgFnSc2d1	stavitelská
rodiny	rodina	k1gFnSc2	rodina
-	-	kIx~	-
byl	být	k5eAaImAgMnS	být
prostředním	prostřednět	k5eAaImIp1nS	prostřednět
z	z	k7c2	z
pěti	pět	k4xCc2	pět
bratří	bratr	k1gMnPc2	bratr
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
stali	stát	k5eAaPmAgMnP	stát
významnými	významný	k2eAgMnPc7d1	významný
staviteli	stavitel	k1gMnPc7	stavitel
<g/>
.	.	kIx.	.
</s>
<s>
Vyučil	vyučit	k5eAaPmAgMnS	vyučit
se	s	k7c7	s
zedníkem	zedník	k1gMnSc7	zedník
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1677	[number]	k4	1677
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
živil	živit	k5eAaImAgMnS	živit
jako	jako	k9	jako
polír	polír	k1gMnSc1	polír
u	u	k7c2	u
mistra	mistr	k1gMnSc2	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1685	[number]	k4	1685
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
vdovou	vdova	k1gFnSc7	vdova
po	po	k7c6	po
staviteli	stavitel	k1gMnSc6	stavitel
Janovi	Jan	k1gMnSc6	Jan
Jiřím	Jiří	k1gMnSc6	Jiří
Aichbauerovi	Aichbauer	k1gMnSc6	Aichbauer
Annou	Anna	k1gFnSc7	Anna
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1689	[number]	k4	1689
dostal	dostat	k5eAaPmAgMnS	dostat
povolení	povolení	k1gNnSc4	povolení
provozovat	provozovat	k5eAaImF	provozovat
stavitelskou	stavitelský	k2eAgFnSc4d1	stavitelská
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
už	už	k6eAd1	už
pracoval	pracovat	k5eAaImAgMnS	pracovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
Abrahamem	Abraham	k1gMnSc7	Abraham
Leuthnerem	Leuthner	k1gMnSc7	Leuthner
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
ostrovského	ostrovský	k2eAgInSc2d1	ostrovský
zámku	zámek	k1gInSc2	zámek
u	u	k7c2	u
Karlových	Karlův	k2eAgInPc2d1	Karlův
Varů	Vary	k1gInPc2	Vary
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1698	[number]	k4	1698
začal	začít	k5eAaPmAgInS	začít
působit	působit	k5eAaImF	působit
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
stavěl	stavět	k5eAaImAgInS	stavět
dvůr	dvůr	k1gInSc1	dvůr
zámečku	zámeček	k1gInSc2	zámeček
v	v	k7c6	v
Hamrníkách	Hamrníkách	k?	Hamrníkách
nebo	nebo	k8xC	nebo
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Kláry	Klára	k1gFnSc2	Klára
v	v	k7c6	v
Chebu	Cheb	k1gInSc6	Cheb
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgInS	začít
samostatně	samostatně	k6eAd1	samostatně
budovat	budovat	k5eAaImF	budovat
i	i	k9	i
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Kryštof	Kryštof	k1gMnSc1	Kryštof
Dientzenhofer	Dientzenhofer	k1gMnSc1	Dientzenhofer
je	být	k5eAaImIp3nS	být
spolehlivě	spolehlivě	k6eAd1	spolehlivě
doložen	doložen	k2eAgInSc1d1	doložen
pouze	pouze	k6eAd1	pouze
jako	jako	k8xC	jako
stavitel	stavitel	k1gMnSc1	stavitel
<g/>
.	.	kIx.	.
</s>
<s>
Autorství	autorství	k1gNnSc1	autorství
níže	nízce	k6eAd2	nízce
uvedených	uvedený	k2eAgFnPc2d1	uvedená
staveb	stavba	k1gFnPc2	stavba
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
bádání	bádání	k1gNnSc2	bádání
různých	různý	k2eAgMnPc2d1	různý
historiků	historik	k1gMnPc2	historik
architektury	architektura	k1gFnSc2	architektura
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tvůrčí	tvůrčí	k2eAgInSc1d1	tvůrčí
vklad	vklad	k1gInSc1	vklad
architekta	architekt	k1gMnSc2	architekt
není	být	k5eNaImIp3nS	být
spolehlivě	spolehlivě	k6eAd1	spolehlivě
doložen	doložen	k2eAgMnSc1d1	doložen
a	a	k8xC	a
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
případů	případ	k1gInPc2	případ
je	být	k5eAaImIp3nS	být
podloženě	podloženě	k6eAd1	podloženě
zpochybňován	zpochybňován	k2eAgInSc1d1	zpochybňován
<g/>
.	.	kIx.	.
</s>
<s>
Hradčany	Hradčany	k1gInPc1	Hradčany
<g/>
,	,	kIx,	,
Loreta	loreta	k1gFnSc1	loreta
Hradčany	Hradčany	k1gInPc4	Hradčany
<g/>
,	,	kIx,	,
Šternberský	šternberský	k2eAgInSc1d1	šternberský
palác	palác	k1gInSc1	palác
(	(	kIx(	(
<g/>
Národní	národní	k2eAgFnSc1d1	národní
galerie	galerie	k1gFnSc1	galerie
<g/>
)	)	kIx)	)
-	-	kIx~	-
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Giovanniho	Giovanni	k1gMnSc2	Giovanni
Battisty	Battista	k1gMnSc2	Battista
Alliprandiho	Alliprandi	k1gMnSc2	Alliprandi
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
dům	dům	k1gInSc1	dům
"	"	kIx"	"
<g/>
U	u	k7c2	u
bílé	bílý	k2eAgFnSc2d1	bílá
kuželky	kuželka	k1gFnSc2	kuželka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
66	[number]	k4	66
<g/>
)	)	kIx)	)
Břevnovský	břevnovský	k2eAgInSc4d1	břevnovský
klášter	klášter	k1gInSc4	klášter
Hradčany	Hradčany	k1gInPc7	Hradčany
<g/>
,	,	kIx,	,
býv.	býv.	k?	býv.
klášter	klášter	k1gInSc1	klášter
voršilek	voršilka	k1gFnPc2	voršilka
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
<g />
.	.	kIx.	.
</s>
<s>
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
dům	dům	k1gInSc1	dům
"	"	kIx"	"
<g/>
Weisbírovský	Weisbírovský	k2eAgMnSc1d1	Weisbírovský
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
13	[number]	k4	13
<g/>
)	)	kIx)	)
Hradčany	Hradčany	k1gInPc1	Hradčany
<g/>
,	,	kIx,	,
Rožmberský	rožmberský	k2eAgInSc1d1	rožmberský
dům	dům	k1gInSc1	dům
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
63	[number]	k4	63
<g/>
)	)	kIx)	)
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
Kaiserštejnský	Kaiserštejnský	k2eAgInSc1d1	Kaiserštejnský
palác	palác	k1gInSc1	palác
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
Vrtbovský	Vrtbovský	k2eAgInSc4d1	Vrtbovský
palác	palác	k1gInSc4	palác
Hradčany	Hradčany	k1gInPc7	Hradčany
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
Nové	Nová	k1gFnSc2	Nová
<g />
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Spálená	spálený	k2eAgFnSc1d1	spálená
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
Břevnovský	břevnovský	k2eAgInSc1d1	břevnovský
klášter	klášter	k1gInSc1	klášter
<g/>
,	,	kIx,	,
bazilika	bazilika	k1gFnSc1	bazilika
sv.	sv.	kA	sv.
Markéty	Markéta	k1gFnSc2	Markéta
Malá	malý	k2eAgFnSc1d1	malá
Strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
chrám	chrám	k1gInSc1	chrám
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
-	-	kIx~	-
západní	západní	k2eAgNnSc1d1	západní
průčelí	průčelí	k1gNnSc1	průčelí
a	a	k8xC	a
část	část	k1gFnSc1	část
lodi	loď	k1gFnSc2	loď
<g/>
;	;	kIx,	;
sporné	sporný	k2eAgInPc1d1	sporný
Klášter	klášter	k1gInSc1	klášter
Teplá	teplat	k5eAaImIp3nS	teplat
-	-	kIx~	-
úpravy	úprava	k1gFnPc1	úprava
a	a	k8xC	a
stavba	stavba	k1gFnSc1	stavba
špitální	špitální	k2eAgFnSc2d1	špitální
kaple	kaple	k1gFnSc2	kaple
Nejsvětější	nejsvětější	k2eAgFnSc2d1	nejsvětější
Trojice	trojice	k1gFnSc2	trojice
(	(	kIx(	(
<g/>
1692	[number]	k4	1692
<g/>
-	-	kIx~	-
<g/>
1699	[number]	k4	1699
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Úterý	úterý	k1gNnSc1	úterý
<g/>
,	,	kIx,	,
chrám	chrám	k1gInSc1	chrám
Narození	narození	k1gNnSc2	narození
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
-	-	kIx~	-
<g/>
1698	[number]	k4	1698
<g/>
)	)	kIx)	)
-	-	kIx~	-
nejisté	jistý	k2eNgInPc1d1	nejistý
Chlum	chlum	k1gInSc1	chlum
svaté	svatá	k1gFnSc2	svatá
Máří	Máří	k?	Máří
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
křižovníků	křižovník	k1gMnPc2	křižovník
(	(	kIx(	(
<g/>
1690	[number]	k4	1690
<g/>
-	-	kIx~	-
<g/>
1701	[number]	k4	1701
<g/>
)	)	kIx)	)
-	-	kIx~	-
sporná	sporný	k2eAgFnSc1d1	sporná
účast	účast	k1gFnSc1	účast
Skalka	skalka	k1gFnSc1	skalka
u	u	k7c2	u
Mníšku	Mníšek	k1gInSc2	Mníšek
pod	pod	k7c7	pod
Brdy	brdo	k1gNnPc7	brdo
<g/>
,	,	kIx,	,
kaple	kaple	k1gFnSc1	kaple
svaté	svatý	k2eAgFnSc2d1	svatá
Máří	Máří	k?	Máří
Magdaleny	Magdalena	k1gFnSc2	Magdalena
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1692	[number]	k4	1692
<g/>
-	-	kIx~	-
<g/>
1693	[number]	k4	1693
<g/>
)	)	kIx)	)
Cheb	Cheb	k1gInSc1	Cheb
<g/>
,	,	kIx,	,
Pevnostní	pevnostní	k2eAgFnPc1d1	pevnostní
stavby	stavba	k1gFnPc1	stavba
(	(	kIx(	(
<g/>
1698	[number]	k4	1698
<g/>
-	-	kIx~	-
<g/>
1701	[number]	k4	1701
<g/>
)	)	kIx)	)
Smiřice	Smiřice	k1gFnPc1	Smiřice
<g/>
,	,	kIx,	,
zámecká	zámecký	k2eAgFnSc1d1	zámecká
kaple	kaple	k1gFnSc1	kaple
Zjevení	zjevení	k1gNnSc1	zjevení
Páně	páně	k2eAgFnSc2d1	páně
(	(	kIx(	(
<g/>
1699	[number]	k4	1699
<g/>
-	-	kIx~	-
<g/>
1711	[number]	k4	1711
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzor	vzor	k1gInSc4	vzor
Guariniho	Guarini	k1gMnSc2	Guarini
kostel	kostel	k1gInSc4	kostel
San	San	k1gMnSc1	San
Lorenzo	Lorenza	k1gFnSc5	Lorenza
v	v	k7c6	v
Turíně	Turín	k1gInSc6	Turín
-	-	kIx~	-
připsáno	připsán	k2eAgNnSc1d1	připsáno
<g />
.	.	kIx.	.
</s>
<s>
Obořiště	Obořiště	k1gNnSc1	Obořiště
<g/>
,	,	kIx,	,
klášterní	klášterní	k2eAgInSc1d1	klášterní
kostel	kostel	k1gInSc1	kostel
paulánů	paulán	k1gMnPc2	paulán
(	(	kIx(	(
<g/>
1702	[number]	k4	1702
<g/>
-	-	kIx~	-
<g/>
1711	[number]	k4	1711
<g/>
)	)	kIx)	)
-	-	kIx~	-
připsáno	připsat	k5eAaPmNgNnS	připsat
Cheb	Cheb	k1gInSc1	Cheb
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Kláry	Klára	k1gFnSc2	Klára
(	(	kIx(	(
<g/>
1703	[number]	k4	1703
<g/>
-	-	kIx~	-
<g/>
1711	[number]	k4	1711
<g/>
)	)	kIx)	)
-	-	kIx~	-
připsáno	připsat	k5eAaPmNgNnS	připsat
Nová	nový	k2eAgFnSc1d1	nová
Paka	Paka	k1gFnSc1	Paka
<g/>
,	,	kIx,	,
paulánský	paulánský	k2eAgInSc1d1	paulánský
kostel	kostel	k1gInSc1	kostel
P.	P.	kA	P.
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
1709	[number]	k4	1709
<g/>
-	-	kIx~	-
<g/>
1724	[number]	k4	1724
<g/>
)	)	kIx)	)
-	-	kIx~	-
připisováno	připisován	k2eAgNnSc1d1	připisováno
Vernéřovice	Vernéřovice	k1gFnPc4	Vernéřovice
u	u	k7c2	u
Náchoda	Náchod	k1gInSc2	Náchod
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Michala	Michal	k1gMnSc2	Michal
(	(	kIx(	(
<g/>
1719	[number]	k4	1719
<g/>
-	-	kIx~	-
<g/>
1721	[number]	k4	1721
<g/>
)	)	kIx)	)
-	-	kIx~	-
nejisté	jistý	k2eNgFnSc3d1	nejistá
Ruprechtice	Ruprechtika	k1gFnSc3	Ruprechtika
u	u	k7c2	u
Náchoda	Náchod	k1gInSc2	Náchod
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
(	(	kIx(	(
<g/>
1720	[number]	k4	1720
<g/>
-	-	kIx~	-
<g/>
1723	[number]	k4	1723
<g/>
)	)	kIx)	)
-	-	kIx~	-
nejisté	jistý	k2eNgNnSc1d1	nejisté
<g/>
.	.	kIx.	.
</s>
<s>
Dominantní	dominantní	k2eAgFnSc4d1	dominantní
nepřehlédnutelnou	přehlédnutelný	k2eNgFnSc4d1	nepřehlédnutelná
stavbu	stavba	k1gFnSc4	stavba
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
projekčně	projekčně	k6eAd1	projekčně
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
Kryštof	Kryštof	k1gMnSc1	Kryštof
Dientzenhofer	Dientzenhofer	k1gMnSc1	Dientzenhofer
<g/>
.	.	kIx.	.
</s>
<s>
Architektonické	architektonický	k2eAgNnSc4d1	architektonické
řešení	řešení	k1gNnSc4	řešení
římsy	římsa	k1gFnSc2	římsa
a	a	k8xC	a
mělkých	mělký	k2eAgInPc2d1	mělký
svislých	svislý	k2eAgInPc2d1	svislý
výstupků	výstupek	k1gInPc2	výstupek
na	na	k7c6	na
zdi	zeď	k1gFnSc6	zeď
má	mít	k5eAaImIp3nS	mít
typické	typický	k2eAgInPc4d1	typický
rysy	rys	k1gInPc4	rys
prací	práce	k1gFnPc2	práce
syna	syn	k1gMnSc4	syn
Kiliána	Kilián	k1gMnSc2	Kilián
Ignáce	Ignác	k1gMnSc2	Ignác
<g/>
.	.	kIx.	.
</s>
