<s>
Santa	Santa	k1gMnSc1	Santa
Cruz	Cruz	k1gMnSc1	Cruz
de	de	k?	de
Tenerife	Tenerif	k1gInSc5	Tenerif
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Tenerife	Tenerif	k1gInSc5	Tenerif
<g/>
,	,	kIx,	,
jednom	jeden	k4xCgMnSc6	jeden
z	z	k7c2	z
Kanárských	kanárský	k2eAgInPc2d1	kanárský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
autonomním	autonomní	k2eAgNnSc6d1	autonomní
společenství	společenství	k1gNnSc6	společenství
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
