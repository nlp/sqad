<p>
<s>
Santa	Santa	k1gMnSc1	Santa
Cruz	Cruz	k1gMnSc1	Cruz
de	de	k?	de
Tenerife	Tenerif	k1gInSc5	Tenerif
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Tenerife	Tenerif	k1gInSc5	Tenerif
<g/>
,	,	kIx,	,
jednom	jeden	k4xCgMnSc6	jeden
z	z	k7c2	z
Kanárských	kanárský	k2eAgInPc2d1	kanárský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
autonomním	autonomní	k2eAgNnSc6d1	autonomní
společenství	společenství	k1gNnSc6	společenství
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
Santa	Santa	k1gMnSc1	Santa
Cruz	Cruz	k1gMnSc1	Cruz
de	de	k?	de
Tenerife	Tenerif	k1gMnSc5	Tenerif
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
žije	žít	k5eAaImIp3nS	žít
přes	přes	k7c4	přes
400	[number]	k4	400
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nS	sídlet
Parlament	parlament	k1gInSc1	parlament
Kanárských	kanárský	k2eAgInPc2d1	kanárský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
se	se	k3xPyFc4	se
recipročně	recipročně	k6eAd1	recipročně
střídá	střídat	k5eAaImIp3nS	střídat
s	s	k7c7	s
Las	laso	k1gNnPc2	laso
Palmas	Palmas	k1gInSc1	Palmas
de	de	k?	de
Gran	Gran	k1gInSc1	Gran
Canaria	Canarium	k1gNnSc2	Canarium
<g/>
)	)	kIx)	)
a	a	k8xC	a
poloviny	polovina	k1gFnSc2	polovina
ministerstev	ministerstvo	k1gNnPc2	ministerstvo
autonomního	autonomní	k2eAgNnSc2d1	autonomní
společenství	společenství	k1gNnSc2	společenství
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tam	tam	k6eAd1	tam
také	také	k9	také
soud	soud	k1gInSc1	soud
<g/>
.	.	kIx.	.
</s>
<s>
Jezdí	jezdit	k5eAaImIp3nP	jezdit
tam	tam	k6eAd1	tam
tramvaje	tramvaj	k1gFnPc1	tramvaj
a	a	k8xC	a
autobusy	autobus	k1gInPc1	autobus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejreprezentativnějších	reprezentativní	k2eAgFnPc2d3	nejreprezentativnější
budov	budova	k1gFnPc2	budova
je	být	k5eAaImIp3nS	být
auditorium	auditorium	k1gNnSc1	auditorium
postavené	postavený	k2eAgNnSc1d1	postavené
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
moderní	moderní	k2eAgFnSc2d1	moderní
španělské	španělský	k2eAgFnSc2d1	španělská
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
hostí	hostit	k5eAaImIp3nS	hostit
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
světových	světový	k2eAgInPc2d1	světový
karnevalů	karneval	k1gInPc2	karneval
<g/>
,	,	kIx,	,
karneval	karneval	k1gInSc1	karneval
v	v	k7c6	v
Santa	Sant	k1gInSc2	Sant
Cruz	Cruza	k1gFnPc2	Cruza
de	de	k?	de
Tenerife	Tenerif	k1gMnSc5	Tenerif
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Auditorio	Auditorio	k1gMnSc1	Auditorio
de	de	k?	de
Tenerife	Tenerif	k1gInSc5	Tenerif
<g/>
:	:	kIx,	:
budova	budova	k1gFnSc1	budova
s	s	k7c7	s
koncertní	koncertní	k2eAgFnSc7d1	koncertní
síní	síň	k1gFnSc7	síň
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
španělský	španělský	k2eAgMnSc1d1	španělský
architekt	architekt	k1gMnSc1	architekt
Santiago	Santiago	k1gNnSc1	Santiago
Calatrava	Calatrava	k1gFnSc1	Calatrava
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
započala	započnout	k5eAaPmAgFnS	započnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
a	a	k8xC	a
otevřena	otevřen	k2eAgFnSc1d1	otevřena
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Úkolem	úkol	k1gInSc7	úkol
nebylo	být	k5eNaImAgNnS	být
postavit	postavit	k5eAaPmF	postavit
jen	jen	k9	jen
koncertní	koncertní	k2eAgFnSc4d1	koncertní
síň	síň	k1gFnSc4	síň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vytvořit	vytvořit	k5eAaPmF	vytvořit
také	také	k9	také
dynamické	dynamický	k2eAgNnSc4d1	dynamické
<g/>
,	,	kIx,	,
monumentální	monumentální	k2eAgNnSc4d1	monumentální
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
také	také	k9	také
symbolem	symbol	k1gInSc7	symbol
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plaza	plaz	k1gMnSc4	plaz
de	de	k?	de
Españ	Españ	k1gMnSc4	Españ
<g/>
:	:	kIx,	:
náměstí	náměstí	k1gNnSc2	náměstí
města	město	k1gNnSc2	město
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
centrální	centrální	k2eAgFnSc7d1	centrální
kašnou	kašna	k1gFnSc7	kašna
a	a	k8xC	a
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
soustředěny	soustředit	k5eAaPmNgFnP	soustředit
administrativní	administrativní	k2eAgFnPc1d1	administrativní
budovy	budova	k1gFnPc1	budova
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Parque	Parque	k6eAd1	Parque
Gacía	Gacía	k1gFnSc1	Gacía
Sanabria	Sanabrium	k1gNnSc2	Sanabrium
<g/>
:	:	kIx,	:
největší	veliký	k2eAgInSc1d3	veliký
městský	městský	k2eAgInSc1d1	městský
park	park	k1gInSc1	park
v	v	k7c6	v
souostroví	souostroví	k1gNnSc6	souostroví
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
symbolů	symbol	k1gInPc2	symbol
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Iglesia	Iglesia	k1gFnSc1	Iglesia
de	de	k?	de
la	la	k1gNnPc2	la
Concepción	Concepción	k1gInSc1	Concepción
<g/>
:	:	kIx,	:
hlavní	hlavní	k2eAgInSc1d1	hlavní
kostel	kostel	k1gInSc1	kostel
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
postaven	postavit	k5eAaPmNgInS	postavit
byl	být	k5eAaImAgInS	být
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1500	[number]	k4	1500
<g/>
,	,	kIx,	,
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
velký	velký	k2eAgInSc4d1	velký
zvon	zvon	k1gInSc4	zvon
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
populárně	populárně	k6eAd1	populárně
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
Katedrála	katedrála	k1gFnSc1	katedrála
Santa	Santa	k1gMnSc1	Santa
Cruz	Cruz	k1gMnSc1	Cruz
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
ne	ne	k9	ne
katedrála	katedrála	k1gFnSc1	katedrála
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Iglesia	Iglesia	k1gFnSc1	Iglesia
de	de	k?	de
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
de	de	k?	de
Asís	Asís	k1gInSc1	Asís
<g/>
:	:	kIx,	:
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
kostelů	kostel	k1gInPc2	kostel
s	s	k7c7	s
uctívaným	uctívaný	k2eAgInSc7d1	uctívaný
obrazem	obraz	k1gInSc7	obraz
Krista	Kristus	k1gMnSc2	Kristus
ve	v	k7c6	v
městě	město	k1gNnSc6	město
s	s	k7c7	s
názvem	název	k1gInSc7	název
Pán	pán	k1gMnSc1	pán
Protivenství	protivenství	k1gNnSc2	protivenství
(	(	kIx(	(
<g/>
Señ	Señ	k1gFnSc1	Señ
de	de	k?	de
las	laso	k1gNnPc2	laso
Tribulaciones	Tribulaciones	k1gInSc1	Tribulaciones
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Playa	Playa	k1gFnSc1	Playa
de	de	k?	de
Las	laso	k1gNnPc2	laso
Teresitas	Teresitas	k1gInSc1	Teresitas
<g/>
:	:	kIx,	:
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
a	a	k8xC	a
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
pláží	pláž	k1gFnPc2	pláž
na	na	k7c6	na
Kanárských	kanárský	k2eAgInPc6d1	kanárský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Castillo	Castillo	k1gNnSc1	Castillo
de	de	k?	de
San	San	k1gFnSc1	San
Andrés	Andrés	k1gInSc1	Andrés
<g/>
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
bývalá	bývalý	k2eAgFnSc1d1	bývalá
vojenská	vojenský	k2eAgFnSc1d1	vojenská
pevnost	pevnost	k1gFnSc1	pevnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
San	San	k1gMnPc2	San
Andrés	Andrésa	k1gFnPc2	Andrésa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zednářský	zednářský	k2eAgInSc1d1	zednářský
chrám	chrám	k1gInSc1	chrám
v	v	k7c6	v
Santa	Sant	k1gInSc2	Sant
Cruz	Cruza	k1gFnPc2	Cruza
de	de	k?	de
Tenerife	Tenerif	k1gInSc5	Tenerif
<g/>
:	:	kIx,	:
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
příklad	příklad	k1gInSc4	příklad
zednářského	zednářský	k2eAgInSc2d1	zednářský
chrámu	chrám	k1gInSc2	chrám
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Karneval	karneval	k1gInSc4	karneval
==	==	k?	==
</s>
</p>
<p>
<s>
Karneval	karneval	k1gInSc1	karneval
v	v	k7c6	v
Santa	Sant	k1gInSc2	Sant
Cruz	Cruza	k1gFnPc2	Cruza
de	de	k?	de
Tenerife	Tenerif	k1gInSc5	Tenerif
je	být	k5eAaImIp3nS	být
druhý	druhý	k4xOgInSc1	druhý
nejslavnější	slavný	k2eAgInSc1d3	nejslavnější
karneval	karneval	k1gInSc1	karneval
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
hned	hned	k6eAd1	hned
po	po	k7c6	po
karnevalu	karneval	k1gInSc6	karneval
v	v	k7c6	v
Rio	Rio	k1gFnSc6	Rio
de	de	k?	de
Janeiru	Janeir	k1gInSc2	Janeir
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyvrcholení	vyvrcholení	k1gNnSc1	vyvrcholení
bývá	bývat	k5eAaImIp3nS	bývat
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
před	před	k7c7	před
Popeleční	popeleční	k2eAgFnSc7d1	popeleční
středou	středa	k1gFnSc7	středa
o	o	k7c6	o
Velikonocích	Velikonoce	k1gFnPc6	Velikonoce
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
lidé	člověk	k1gMnPc1	člověk
vycházejí	vycházet	k5eAaImIp3nP	vycházet
do	do	k7c2	do
ulic	ulice	k1gFnPc2	ulice
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
kostýmech	kostým	k1gInPc6	kostým
a	a	k8xC	a
slaví	slavit	k5eAaImIp3nP	slavit
konec	konec	k1gInSc4	konec
masopustu	masopust	k1gInSc2	masopust
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Aranda	Aranda	k1gFnSc1	Aranda
de	de	k?	de
Duero	Duero	k1gNnSc1	Duero
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
</s>
</p>
<p>
<s>
San	San	k?	San
Antonio	Antonio	k1gMnSc1	Antonio
<g/>
,	,	kIx,	,
Texas	Texas	kA	Texas
<g/>
,	,	kIx,	,
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Miami	Miami	k1gNnSc1	Miami
<g/>
,	,	kIx,	,
Florida	Florida	k1gFnSc1	Florida
<g/>
,	,	kIx,	,
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Santa	Santa	k1gMnSc1	Santa
Cruz	Cruza	k1gFnPc2	Cruza
de	de	k?	de
la	la	k1gNnSc1	la
Sierra	Sierra	k1gFnSc1	Sierra
<g/>
,	,	kIx,	,
Bolívie	Bolívie	k1gFnSc1	Bolívie
</s>
</p>
<p>
<s>
Caracas	Caracas	k1gInSc1	Caracas
<g/>
,	,	kIx,	,
Venezuela	Venezuela	k1gFnSc1	Venezuela
</s>
</p>
<p>
<s>
Cádiz	Cádiz	k1gInSc1	Cádiz
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
</s>
</p>
<p>
<s>
Rio	Rio	k?	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc1	Janeiro
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
</s>
</p>
<p>
<s>
Nice	Nice	k1gFnSc1	Nice
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
</s>
</p>
<p>
<s>
Santa	Santa	k1gMnSc1	Santa
Cruz	Cruz	k1gMnSc1	Cruz
del	del	k?	del
Norte	Nort	k1gMnSc5	Nort
<g/>
,	,	kIx,	,
Kuba	Kuba	k1gFnSc1	Kuba
</s>
</p>
<p>
<s>
Ciudad	Ciudad	k6eAd1	Ciudad
de	de	k?	de
Guatemala	Guatemala	k1gFnSc1	Guatemala
<g/>
,	,	kIx,	,
Guatemala	Guatemala	k1gFnSc1	Guatemala
</s>
</p>
<p>
<s>
==	==	k?	==
Slavní	slavný	k2eAgMnPc1d1	slavný
rodáci	rodák	k1gMnPc1	rodák
==	==	k?	==
</s>
</p>
<p>
<s>
Ángel	Ángela	k1gFnPc2	Ángela
Guimerá	Guimerý	k2eAgFnSc1d1	Guimerý
–	–	k?	–
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
</s>
</p>
<p>
<s>
Leopoldo	Leopolda	k1gFnSc5	Leopolda
O	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
Donnell	Donnell	k1gMnSc1	Donnell
–	–	k?	–
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Sergio	Sergio	k1gMnSc1	Sergio
Rodríguez	Rodríguez	k1gMnSc1	Rodríguez
Gómez	Gómez	k1gMnSc1	Gómez
–	–	k?	–
basketbalista	basketbalista	k1gMnSc1	basketbalista
</s>
</p>
<p>
<s>
Pedro	Pedro	k1gNnSc1	Pedro
Guerra	Guerr	k1gInSc2	Guerr
–	–	k?	–
textař	textař	k1gMnSc1	textař
</s>
</p>
<p>
<s>
Alberto	Alberta	k1gFnSc5	Alberta
Vázquez-Figueroa	Vázquez-Figueroa	k1gMnSc1	Vázquez-Figueroa
–	–	k?	–
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Tenerife	Tenerif	k1gMnSc5	Tenerif
</s>
</p>
<p>
<s>
Auditorium	auditorium	k1gNnSc1	auditorium
na	na	k7c6	na
Tenerife	Tenerif	k1gMnSc5	Tenerif
</s>
</p>
<p>
<s>
Karneval	karneval	k1gInSc1	karneval
v	v	k7c6	v
Santa	Sant	k1gInSc2	Sant
Cruz	Cruza	k1gFnPc2	Cruza
de	de	k?	de
Tenerife	Tenerif	k1gMnSc5	Tenerif
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Santa	Sant	k1gInSc2	Sant
Cruz	Cruz	k1gInSc1	Cruz
de	de	k?	de
Tenerife	Tenerif	k1gInSc5	Tenerif
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Stránky	stránka	k1gFnSc2	stránka
města	město	k1gNnSc2	město
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
pro	pro	k7c4	pro
turisty	turist	k1gMnPc4	turist
</s>
</p>
