<s>
Démonologie	démonologie	k1gFnSc1	démonologie
je	být	k5eAaImIp3nS	být
systematická	systematický	k2eAgFnSc1d1	systematická
nauka	nauka	k1gFnSc1	nauka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
démony	démon	k1gMnPc4	démon
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc7	jejich
vlivem	vliv	k1gInSc7	vliv
na	na	k7c4	na
lidský	lidský	k2eAgInSc4d1	lidský
život	život	k1gInSc4	život
a	a	k8xC	a
lidské	lidský	k2eAgInPc4d1	lidský
hříchy	hřích	k1gInPc4	hřích
<g/>
.	.	kIx.	.
</s>
