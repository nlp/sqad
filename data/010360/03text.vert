<p>
<s>
Démonologie	démonologie	k1gFnSc1	démonologie
je	být	k5eAaImIp3nS	být
systematická	systematický	k2eAgFnSc1d1	systematická
nauka	nauka	k1gFnSc1	nauka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
démony	démon	k1gMnPc4	démon
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc7	jejich
vlivem	vliv	k1gInSc7	vliv
na	na	k7c4	na
lidský	lidský	k2eAgInSc4d1	lidský
život	život	k1gInSc4	život
a	a	k8xC	a
lidské	lidský	k2eAgInPc4d1	lidský
hříchy	hřích	k1gInPc4	hřích
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
démonologie	démonologie	k1gFnSc2	démonologie
je	být	k5eAaImIp3nS	být
utvořeno	utvořen	k2eAgNnSc1d1	utvořeno
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
řeckých	řecký	k2eAgInPc2d1	řecký
základů	základ	k1gInPc2	základ
<g/>
:	:	kIx,	:
Daemon	Daemona	k1gFnPc2	Daemona
(	(	kIx(	(
<g/>
δ	δ	k?	δ
[	[	kIx(	[
<g/>
ð	ð	k?	ð
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
démon	démon	k1gMnSc1	démon
<g/>
,	,	kIx,	,
ďábel	ďábel	k1gMnSc1	ďábel
a	a	k8xC	a
logos	logos	k1gInSc1	logos
(	(	kIx(	(
<g/>
λ	λ	k?	λ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
slovo	slovo	k1gNnSc4	slovo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
řeč	řeč	k1gFnSc1	řeč
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
rozum	rozum	k1gInSc1	rozum
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Opačná	opačný	k2eAgFnSc1d1	opačná
nauka	nauka	k1gFnSc1	nauka
je	být	k5eAaImIp3nS	být
angelologie	angelologie	k1gFnPc4	angelologie
<g/>
.	.	kIx.	.
</s>
<s>
Démonologie	démonologie	k1gFnSc1	démonologie
je	být	k5eAaImIp3nS	být
ortodoxní	ortodoxní	k2eAgFnSc1d1	ortodoxní
součást	součást	k1gFnSc1	součást
theologie	theologie	k1gFnSc2	theologie
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
je	být	k5eAaImIp3nS	být
démonologie	démonologie	k1gFnSc1	démonologie
postavena	postaven	k2eAgFnSc1d1	postavena
na	na	k7c6	na
Starém	starý	k2eAgInSc6d1	starý
a	a	k8xC	a
Novém	nový	k2eAgInSc6d1	nový
zákonu	zákon	k1gInSc6	zákon
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
teologického	teologický	k2eAgNnSc2d1	teologické
učení	učení	k1gNnSc2	učení
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
být	být	k5eAaImF	být
pohlíženo	pohlížen	k2eAgNnSc1d1	pohlíženo
také	také	k9	také
jako	jako	k8xC	jako
na	na	k7c4	na
součást	součást	k1gFnSc4	součást
angelologie	angelologie	k1gFnSc2	angelologie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c4	o
padlých	padlý	k2eAgInPc2d1	padlý
andělích	andělí	k2eAgInPc2d1	andělí
<g/>
.	.	kIx.	.
</s>
<s>
Neexistuje	existovat	k5eNaImIp3nS	existovat
často	často	k6eAd1	často
jako	jako	k9	jako
samostatná	samostatný	k2eAgFnSc1d1	samostatná
oddělená	oddělený	k2eAgFnSc1d1	oddělená
část	část	k1gFnSc1	část
teologie	teologie	k1gFnSc2	teologie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nýbrž	nýbrž	k8xC	nýbrž
jen	jen	k9	jen
jako	jako	k9	jako
komentáře	komentář	k1gInPc4	komentář
k	k	k7c3	k
biblickým	biblický	k2eAgInPc3d1	biblický
textům	text	k1gInPc3	text
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úvod	úvod	k1gInSc1	úvod
==	==	k?	==
</s>
</p>
<p>
<s>
Předmětem	předmět	k1gInSc7	předmět
výzkumu	výzkum	k1gInSc2	výzkum
démonologie	démonologie	k1gFnSc2	démonologie
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
původ	původ	k1gInSc1	původ
a	a	k8xC	a
bytí	bytí	k1gNnSc1	bytí
démonů	démon	k1gMnPc2	démon
</s>
</p>
<p>
<s>
počet	počet	k1gInSc1	počet
démonů	démon	k1gMnPc2	démon
</s>
</p>
<p>
<s>
přirozená	přirozený	k2eAgFnSc1d1	přirozená
povaha	povaha	k1gFnSc1	povaha
démonů	démon	k1gMnPc2	démon
</s>
</p>
<p>
<s>
jejich	jejich	k3xOp3gFnSc1	jejich
nematerializace	nematerializace	k1gFnSc1	nematerializace
<g/>
,	,	kIx,	,
nehmotnost	nehmotnost	k1gFnSc1	nehmotnost
</s>
</p>
<p>
<s>
jejich	jejich	k3xOp3gFnSc1	jejich
přirozená	přirozený	k2eAgFnSc1d1	přirozená
nesmrtelnost	nesmrtelnost	k1gFnSc1	nesmrtelnost
</s>
</p>
<p>
<s>
rozum	rozum	k1gInSc1	rozum
<g/>
,	,	kIx,	,
vůle	vůle	k1gFnSc1	vůle
a	a	k8xC	a
síla	síla	k1gFnSc1	síla
démonů	démon	k1gMnPc2	démon
</s>
</p>
<p>
<s>
zkouška	zkouška	k1gFnSc1	zkouška
anděla	anděl	k1gMnSc2	anděl
</s>
</p>
<p>
<s>
pád	pád	k1gInSc1	pád
anděla	anděl	k1gMnSc2	anděl
</s>
</p>
<p>
<s>
zamítnutí	zamítnutí	k1gNnSc1	zamítnutí
zlého	zlý	k2eAgMnSc2d1	zlý
anděla	anděl	k1gMnSc2	anděl
(	(	kIx(	(
<g/>
démona	démon	k1gMnSc2	démon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
působení	působení	k1gNnSc1	působení
padlého	padlý	k1gMnSc2	padlý
<g/>
,	,	kIx,	,
zlého	zlý	k2eAgMnSc2d1	zlý
anděla	anděl	k1gMnSc2	anděl
(	(	kIx(	(
<g/>
démona	démon	k1gMnSc2	démon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
jednotlivce	jednotlivec	k1gMnPc4	jednotlivec
</s>
</p>
<p>
<s>
pokušení	pokušení	k1gNnSc4	pokušení
k	k	k7c3	k
hříchu	hřích	k1gInSc3	hřích
</s>
</p>
<p>
<s>
způsobení	způsobení	k1gNnSc1	způsobení
psychické	psychický	k2eAgFnSc2d1	psychická
bolesti	bolest	k1gFnSc2	bolest
</s>
</p>
<p>
<s>
posedlost	posedlost	k1gFnSc1	posedlost
</s>
</p>
<p>
<s>
v	v	k7c6	v
božím	boží	k2eAgInSc6d1	boží
léčebném	léčebný	k2eAgInSc6d1	léčebný
úmyslu	úmysl	k1gInSc6	úmysl
</s>
</p>
<p>
<s>
Působení	působení	k1gNnSc1	působení
démonů	démon	k1gMnPc2	démon
v	v	k7c6	v
božím	boží	k2eAgInSc6d1	boží
světovém	světový	k2eAgInSc6d1	světový
úmyslu	úmysl	k1gInSc6	úmysl
</s>
</p>
<p>
<s>
Působení	působení	k1gNnSc1	působení
démonů	démon	k1gMnPc2	démon
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
</s>
</p>
<p>
<s>
nepovolené	povolený	k2eNgInPc1d1	nepovolený
démonické	démonický	k2eAgInPc1d1	démonický
kulty	kult	k1gInPc1	kult
</s>
</p>
<p>
<s>
čarodějnictví	čarodějnictví	k1gNnSc1	čarodějnictví
</s>
</p>
<p>
<s>
věštění	věštění	k1gNnSc1	věštění
budoucnosti	budoucnost	k1gFnSc2	budoucnost
</s>
</p>
<p>
<s>
použití	použití	k1gNnSc1	použití
siderického	siderický	k2eAgNnSc2d1	siderické
kyvadla	kyvadlo	k1gNnSc2	kyvadlo
</s>
</p>
<p>
<s>
černé	černý	k2eAgFnPc1d1	černá
mše	mše	k1gFnPc1	mše
</s>
</p>
<p>
<s>
řád	řád	k1gInSc1	řád
démonů	démon	k1gMnPc2	démon
</s>
</p>
<p>
<s>
zjevení	zjevený	k2eAgMnPc1d1	zjevený
démonůDémoni	démonůDémon	k1gMnPc1	démonůDémon
hrají	hrát	k5eAaImIp3nP	hrát
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
ve	v	k7c4	v
filozofii	filozofie	k1gFnSc4	filozofie
<g/>
,	,	kIx,	,
religionistice	religionistika	k1gFnSc3	religionistika
a	a	k8xC	a
poezii	poezie	k1gFnSc3	poezie
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
pevné	pevný	k2eAgNnSc1d1	pevné
místo	místo	k1gNnSc1	místo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
<g/>
,	,	kIx,	,
v	v	k7c6	v
národopisu	národopis	k1gInSc6	národopis
i	i	k8xC	i
v	v	k7c6	v
mytologii	mytologie	k1gFnSc6	mytologie
ale	ale	k8xC	ale
pro	pro	k7c4	pro
historii	historie	k1gFnSc4	historie
a	a	k8xC	a
religionistiku	religionistika	k1gFnSc4	religionistika
však	však	k9	však
zůstane	zůstat	k5eAaPmIp3nS	zůstat
navždy	navždy	k6eAd1	navždy
jen	jen	k9	jen
součástí	součást	k1gFnSc7	součást
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
podle	podle	k7c2	podle
vlastností	vlastnost	k1gFnPc2	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
historii	historie	k1gFnSc6	historie
lidstva	lidstvo	k1gNnSc2	lidstvo
se	se	k3xPyFc4	se
již	již	k6eAd1	již
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
mnoho	mnoho	k4c1	mnoho
pokusů	pokus	k1gInPc2	pokus
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
klasifikace	klasifikace	k1gFnSc2	klasifikace
démonů	démon	k1gMnPc2	démon
do	do	k7c2	do
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
;	;	kIx,	;
obzvláště	obzvláště	k6eAd1	obzvláště
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Systémy	systém	k1gInPc1	systém
jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgInP	založit
na	na	k7c6	na
vlastnostech	vlastnost	k1gFnPc6	vlastnost
démona	démon	k1gMnSc2	démon
<g/>
,	,	kIx,	,
na	na	k7c6	na
hříchu	hřích	k1gInSc6	hřích
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
člověka	člověk	k1gMnSc4	člověk
nebo	nebo	k8xC	nebo
na	na	k7c6	na
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
démon	démon	k1gMnSc1	démon
nejsilnější	silný	k2eAgMnSc1d3	nejsilnější
<g/>
.	.	kIx.	.
</s>
<s>
Případně	případně	k6eAd1	případně
také	také	k9	také
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gMnSc2	jejich
oponenta	oponent	k1gMnSc2	oponent
z	z	k7c2	z
angelologie	angelologie	k1gFnSc2	angelologie
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgInSc7	který
soupeří	soupeřit	k5eAaImIp3nP	soupeřit
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
výpis	výpis	k1gInSc4	výpis
stěžejních	stěžejní	k2eAgNnPc2d1	stěžejní
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
autoři	autor	k1gMnPc1	autor
pohlížejí	pohlížet	k5eAaImIp3nP	pohlížet
na	na	k7c4	na
démony	démon	k1gMnPc4	démon
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
hledisek	hledisko	k1gNnPc2	hledisko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Šalamounův	Šalamounův	k2eAgInSc1d1	Šalamounův
testament	testament	k1gInSc1	testament
===	===	k?	===
</s>
</p>
<p>
<s>
Šalamounův	Šalamounův	k2eAgInSc1d1	Šalamounův
testament	testament	k1gInSc1	testament
je	být	k5eAaImIp3nS	být
starý	starý	k2eAgInSc1d1	starý
testament	testament	k1gInSc1	testament
<g/>
,	,	kIx,	,
pseudoepigrafické	pseudoepigrafický	k2eAgNnSc1d1	pseudoepigrafický
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
napsané	napsaný	k2eAgInPc1d1	napsaný
v	v	k7c6	v
době	doba	k1gFnSc6	doba
mezi	mezi	k7c7	mezi
1	[number]	k4	1
<g/>
.	.	kIx.	.
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
králem	král	k1gMnSc7	král
Šalamounem	Šalamoun	k1gMnSc7	Šalamoun
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
Šalamoun	Šalamoun	k1gMnSc1	Šalamoun
důkladně	důkladně	k6eAd1	důkladně
popisuje	popisovat	k5eAaImIp3nS	popisovat
démony	démon	k1gMnPc4	démon
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
zotročil	zotročit	k5eAaPmAgMnS	zotročit
aby	aby	kYmCp3nP	aby
mu	on	k3xPp3gMnSc3	on
pomohli	pomoct	k5eAaPmAgMnP	pomoct
postavit	postavit	k5eAaPmF	postavit
jeho	jeho	k3xOp3gInSc4	jeho
chrám	chrám	k1gInSc4	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Nastiňuje	nastiňovat	k5eAaImIp3nS	nastiňovat
otázky	otázka	k1gFnPc4	otázka
ohledně	ohledně	k7c2	ohledně
úmluvy	úmluva	k1gFnSc2	úmluva
s	s	k7c7	s
démony	démon	k1gMnPc7	démon
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zmařena	zmařen	k2eAgFnSc1d1	zmařena
<g/>
,	,	kIx,	,
a	a	k8xC	a
dává	dávat	k5eAaImIp3nS	dávat
také	také	k9	také
odpovědi	odpověď	k1gFnPc1	odpověď
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
chovat	chovat	k5eAaImF	chovat
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
sami	sám	k3xTgMnPc1	sám
dokázali	dokázat	k5eAaPmAgMnP	dokázat
ovládnout	ovládnout	k5eAaPmF	ovládnout
padlé	padlý	k2eAgFnPc4d1	padlá
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Datace	datace	k1gFnSc1	datace
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
sporná	sporný	k2eAgFnSc1d1	sporná
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
do	do	k7c2	do
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
určitě	určitě	k6eAd1	určitě
nejstarší	starý	k2eAgNnSc4d3	nejstarší
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
konkrétním	konkrétní	k2eAgInSc7d1	konkrétní
průzkumem	průzkum	k1gInSc7	průzkum
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
démonů	démon	k1gMnPc2	démon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Psellova	Psellův	k2eAgFnSc1d1	Psellův
klasifikace	klasifikace	k1gFnSc1	klasifikace
démonů	démon	k1gMnPc2	démon
===	===	k?	===
</s>
</p>
<p>
<s>
Další	další	k2eAgNnSc4d1	další
rozdělení	rozdělení	k1gNnSc4	rozdělení
démonů	démon	k1gMnPc2	démon
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Michael	Michael	k1gMnSc1	Michael
Psellos	Psellos	k1gMnSc1	Psellos
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
nepochybně	pochybně	k6eNd1	pochybně
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
Franceska	Franceska	k1gFnSc1	Franceska
Maria	Mario	k1gMnSc2	Mario
Guazzu	Guazz	k1gInSc2	Guazz
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
této	tento	k3xDgFnSc3	tento
problematice	problematika	k1gFnSc3	problematika
věnoval	věnovat	k5eAaImAgInS	věnovat
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyhýbají	vyhýbat	k5eAaImIp3nP	vyhýbat
se	se	k3xPyFc4	se
dennímu	denní	k2eAgNnSc3d1	denní
světlu	světlo	k1gNnSc3	světlo
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
neviditelní	viditelný	k2eNgMnPc1d1	Neviditelný
lidem	člověk	k1gMnPc3	člověk
</s>
</p>
<p>
<s>
Démoni	démon	k1gMnPc1	démon
ohně	oheň	k1gInSc2	oheň
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
pobývají	pobývat	k5eAaImIp3nP	pobývat
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
nás	my	k3xPp1nPc2	my
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
===	===	k?	===
Spinova	Spinův	k2eAgFnSc1d1	Spinův
klasifikace	klasifikace	k1gFnSc1	klasifikace
démonů	démon	k1gMnPc2	démon
===	===	k?	===
</s>
</p>
<p>
<s>
Alfonso	Alfonso	k1gMnSc1	Alfonso
de	de	k?	de
Spina	spina	k1gFnSc1	spina
roku	rok	k1gInSc2	rok
1467	[number]	k4	1467
připravil	připravit	k5eAaPmAgMnS	připravit
rozdělení	rozdělení	k1gNnSc4	rozdělení
démonů	démon	k1gMnPc2	démon
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
několika	několik	k4yIc6	několik
kritériích	kritérion	k1gNnPc6	kritérion
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Démoni	démon	k1gMnPc1	démon
osudu	osud	k1gInSc2	osud
</s>
</p>
<p>
<s>
Goblinové	Goblinový	k2eAgNnSc1d1	Goblinový
</s>
</p>
<p>
<s>
Incubus	Incubus	k1gMnSc1	Incubus
a	a	k8xC	a
succubus	succubus	k1gMnSc1	succubus
</s>
</p>
<p>
<s>
Chodící	chodící	k2eAgFnSc2d1	chodící
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
armády	armáda	k1gFnSc2	armáda
démonů	démon	k1gMnPc2	démon
</s>
</p>
<p>
<s>
Čarodějnická	čarodějnický	k2eAgNnPc1d1	čarodějnické
zvířata	zvíře	k1gNnPc1	zvíře
</s>
</p>
<p>
<s>
Noční	noční	k2eAgFnPc1d1	noční
můry	můra	k1gFnPc1	můra
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Druden	Drudna	k1gFnPc2	Drudna
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Démoni	démon	k1gMnPc1	démon
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
zrozeni	zrodit	k5eAaPmNgMnP	zrodit
ze	z	k7c2	z
spojení	spojení	k1gNnSc2	spojení
démona	démon	k1gMnSc2	démon
a	a	k8xC	a
existence	existence	k1gFnSc2	existence
člověka	člověk	k1gMnSc4	člověk
</s>
</p>
<p>
<s>
Lháři	lhář	k1gMnPc1	lhář
a	a	k8xC	a
zlomyslní	zlomyslný	k2eAgMnPc1d1	zlomyslný
démoni	démon	k1gMnPc1	démon
</s>
</p>
<p>
<s>
Démoni	démon	k1gMnPc1	démon
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
útočí	útočit	k5eAaImIp3nP	útočit
na	na	k7c6	na
svaté	svatý	k2eAgFnSc6d1	svatá
</s>
</p>
<p>
<s>
Démoni	démon	k1gMnPc1	démon
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
staré	starý	k2eAgFnPc4d1	stará
ženy	žena	k1gFnPc4	žena
k	k	k7c3	k
čarodějnictví	čarodějnictví	k1gNnSc3	čarodějnictví
</s>
</p>
<p>
<s>
===	===	k?	===
Binsfeldova	Binsfeldův	k2eAgFnSc1d1	Binsfeldův
klasifikace	klasifikace	k1gFnSc1	klasifikace
démonů	démon	k1gMnPc2	démon
===	===	k?	===
</s>
</p>
<p>
<s>
Binsfeldova	Binsfeldův	k2eAgFnSc1d1	Binsfeldův
klasifikace	klasifikace	k1gFnSc1	klasifikace
démonů	démon	k1gMnPc2	démon
byla	být	k5eAaImAgFnS	být
publikována	publikován	k2eAgFnSc1d1	publikována
roku	rok	k1gInSc2	rok
1589	[number]	k4	1589
Petrem	Petr	k1gMnSc7	Petr
Binsfeldem	Binsfeld	k1gMnSc7	Binsfeld
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
rozdělení	rozdělení	k1gNnSc1	rozdělení
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
sedmi	sedm	k4xCc6	sedm
hlavních	hlavní	k2eAgInPc6d1	hlavní
hříších	hřích	k1gInPc6	hřích
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
démon	démon	k1gMnSc1	démon
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
člověka	člověk	k1gMnSc2	člověk
ke	k	k7c3	k
spáchání	spáchání	k1gNnSc3	spáchání
jednoho	jeden	k4xCgInSc2	jeden
hříchu	hřích	k1gInSc2	hřích
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lucifer	Lucifer	k1gMnSc1	Lucifer
<g/>
:	:	kIx,	:
pýcha	pýcha	k1gFnSc1	pýcha
</s>
</p>
<p>
<s>
Leviathan	Leviathan	k1gInSc1	Leviathan
<g/>
:	:	kIx,	:
závist	závist	k1gFnSc1	závist
</s>
</p>
<p>
<s>
Satan	satan	k1gInSc1	satan
<g/>
:	:	kIx,	:
hněv	hněv	k1gInSc1	hněv
</s>
</p>
<p>
<s>
Belphegor	Belphegor	k1gInSc1	Belphegor
<g/>
:	:	kIx,	:
lenost	lenost	k1gFnSc1	lenost
</s>
</p>
<p>
<s>
Mammon	Mammon	k1gMnSc1	Mammon
<g/>
:	:	kIx,	:
lakota	lakota	k1gMnSc1	lakota
</s>
</p>
<p>
<s>
Belzebub	Belzebub	k1gMnSc1	Belzebub
<g/>
:	:	kIx,	:
obžerství	obžerství	k1gNnSc1	obžerství
</s>
</p>
<p>
<s>
Asmodeus	Asmodeus	k1gInSc1	Asmodeus
<g/>
:	:	kIx,	:
smilstvo	smilstvo	k1gNnSc1	smilstvo
</s>
</p>
<p>
<s>
===	===	k?	===
Guazzova	Guazzův	k2eAgFnSc1d1	Guazzův
klasifikace	klasifikace	k1gFnSc1	klasifikace
démonů	démon	k1gMnPc2	démon
===	===	k?	===
</s>
</p>
<p>
<s>
Francesco	Francesco	k6eAd1	Francesco
Maria	Maria	k1gFnSc1	Maria
Guazzo	Guazza	k1gFnSc5	Guazza
připravil	připravit	k5eAaPmAgInS	připravit
své	svůj	k3xOyFgNnSc4	svůj
rozdělení	rozdělení	k1gNnSc4	rozdělení
démonů	démon	k1gMnPc2	démon
podle	podle	k7c2	podle
předešlé	předešlý	k2eAgFnSc2d1	předešlá
práce	práce	k1gFnSc2	práce
Michaela	Michael	k1gMnSc2	Michael
Psella	Psell	k1gMnSc2	Psell
<g/>
.	.	kIx.	.
</s>
<s>
Publikoval	publikovat	k5eAaBmAgMnS	publikovat
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Compendium	Compendium	k1gNnSc1	Compendium
Maleficarum	Maleficarum	k1gInSc4	Maleficarum
r.	r.	kA	r.
1608	[number]	k4	1608
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Démoni	démon	k1gMnPc1	démon
vzduchu	vzduch	k1gInSc2	vzduch
vyšší	vysoký	k2eAgFnSc2d2	vyšší
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
vazbu	vazba	k1gFnSc4	vazba
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Démoni	démon	k1gMnPc1	démon
vzduchu	vzduch	k1gInSc2	vzduch
nižší	nízký	k2eAgFnSc2d2	nižší
úrovně	úroveň	k1gFnSc2	úroveň
<g/>
,	,	kIx,	,
zodpovídají	zodpovídat	k5eAaImIp3nP	zodpovídat
za	za	k7c4	za
bouře	bouř	k1gFnPc4	bouř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Démoni	démon	k1gMnPc1	démon
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
pobývají	pobývat	k5eAaImIp3nP	pobývat
na	na	k7c6	na
polích	pole	k1gNnPc6	pole
<g/>
,	,	kIx,	,
v	v	k7c6	v
jeskyních	jeskyně	k1gFnPc6	jeskyně
a	a	k8xC	a
lesích	les	k1gInPc6	les
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Démoni	démon	k1gMnPc1	démon
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
démoni	démon	k1gMnPc1	démon
ženského	ženský	k2eAgInSc2d1	ženský
rodu	rod	k1gInSc2	rod
<g/>
,	,	kIx,	,
ničí	ničit	k5eAaImIp3nS	ničit
vodní	vodní	k2eAgMnPc4d1	vodní
živočichy	živočich	k1gMnPc4	živočich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Démoni	démon	k1gMnPc1	démon
podsvětí	podsvětí	k1gNnSc2	podsvětí
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
zodpovídají	zodpovídat	k5eAaImIp3nP	zodpovídat
za	za	k7c4	za
ochranu	ochrana	k1gFnSc4	ochrana
skrytých	skrytý	k2eAgInPc2d1	skrytý
pokladů	poklad	k1gInPc2	poklad
<g/>
,	,	kIx,	,
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
zemětřesení	zemětřesení	k1gNnSc4	zemětřesení
a	a	k8xC	a
rozpadávání	rozpadávání	k1gNnSc4	rozpadávání
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Démoni	démon	k1gMnPc1	démon
noci	noc	k1gFnSc2	noc
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
černí	černý	k2eAgMnPc1d1	černý
a	a	k8xC	a
zlí	zlý	k2eAgMnPc1d1	zlý
<g/>
,	,	kIx,	,
vyhýbají	vyhýbat	k5eAaImIp3nP	vyhýbat
se	se	k3xPyFc4	se
dennímu	denní	k2eAgNnSc3d1	denní
světlu	světlo	k1gNnSc3	světlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Michaelisova	Michaelisův	k2eAgFnSc1d1	Michaelisova
klasifikace	klasifikace	k1gFnSc1	klasifikace
démonů	démon	k1gMnPc2	démon
===	===	k?	===
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1613	[number]	k4	1613
Sebastian	Sebastian	k1gMnSc1	Sebastian
Michaelis	Michaelis	k1gFnSc4	Michaelis
napsal	napsat	k5eAaBmAgMnS	napsat
knihu	kniha	k1gFnSc4	kniha
Admirable	Admirable	k1gMnSc1	Admirable
History	Histor	k1gInPc4	Histor
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgMnPc4	který
představil	představit	k5eAaPmAgMnS	představit
rozdělení	rozdělení	k1gNnSc4	rozdělení
démonů	démon	k1gMnPc2	démon
tak	tak	k9	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
mu	on	k3xPp3gMnSc3	on
našeptával	našeptávat	k5eAaImAgMnS	našeptávat
sám	sám	k3xTgMnSc1	sám
démon	démon	k1gMnSc1	démon
Berith	Berith	k1gMnSc1	Berith
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozdělení	rozdělení	k1gNnSc1	rozdělení
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
hierarchiích	hierarchie	k1gFnPc6	hierarchie
<g/>
,	,	kIx,	,
hříších	hřích	k1gInPc6	hřích
nebo	nebo	k8xC	nebo
pokušeních	pokušení	k1gNnPc6	pokušení
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
spáchány	spáchat	k5eAaPmNgInP	spáchat
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
démonova	démonův	k2eAgMnSc4d1	démonův
protivníka	protivník	k1gMnSc4	protivník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
hříchu	hřích	k1gInSc3	hřích
dokázal	dokázat	k5eAaPmAgInS	dokázat
odolat	odolat	k5eAaPmF	odolat
a	a	k8xC	a
nepadl	padnout	k5eNaPmAgMnS	padnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hodně	hodně	k6eAd1	hodně
jmen	jméno	k1gNnPc2	jméno
svatých	svatá	k1gFnPc2	svatá
jsou	být	k5eAaImIp3nP	být
ponechána	ponechat	k5eAaPmNgFnS	ponechat
S.	S.	kA	S.
Michaelisem	Michaelis	k1gInSc7	Michaelis
bez	bez	k1gInSc1	bez
přídomku	přídomek	k1gInSc2	přídomek
<g/>
,	,	kIx,	,
a	a	k8xC	a
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
se	se	k3xPyFc4	se
hledá	hledat	k5eAaImIp3nS	hledat
správný	správný	k2eAgInSc1d1	správný
význam	význam	k1gInSc1	význam
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
jen	jen	k9	jen
dohadovat	dohadovat	k5eAaImF	dohadovat
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
například	například	k6eAd1	například
svatý	svatý	k2eAgMnSc1d1	svatý
Jan	Jan	k1gMnSc1	Jan
jednou	jednou	k6eAd1	jednou
neznamená	znamenat	k5eNaImIp3nS	znamenat
sv.	sv.	kA	sv.
Jan	Jan	k1gMnSc1	Jan
Křtitel	křtitel	k1gMnSc1	křtitel
a	a	k8xC	a
podruhé	podruhé	k6eAd1	podruhé
sv.	sv.	kA	sv.
Jan	Jan	k1gMnSc1	Jan
Evangelista	evangelista	k1gMnSc1	evangelista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
První	první	k4xOgFnSc1	první
hierarchie	hierarchie	k1gFnSc1	hierarchie
====	====	k?	====
</s>
</p>
<p>
<s>
Belzebub	Belzebub	k1gMnSc1	Belzebub
<g/>
:	:	kIx,	:
arogance	arogance	k1gFnSc1	arogance
<g/>
;	;	kIx,	;
protivník	protivník	k1gMnSc1	protivník
Svatý	svatý	k1gMnSc1	svatý
František	František	k1gMnSc1	František
z	z	k7c2	z
Assisi	Assise	k1gFnSc6	Assise
(	(	kIx(	(
<g/>
???	???	k?	???
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Leviathan	Leviathan	k1gMnSc1	Leviathan
<g/>
:	:	kIx,	:
zloba	zloba	k1gMnSc1	zloba
<g/>
,	,	kIx,	,
útočí	útočit	k5eAaImIp3nS	útočit
na	na	k7c6	na
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
náboženské	náboženský	k2eAgFnSc6d1	náboženská
věřící	věřící	k1gFnSc6	věřící
<g/>
;	;	kIx,	;
protivník	protivník	k1gMnSc1	protivník
Svatý	svatý	k1gMnSc1	svatý
Petr	Petr	k1gMnSc1	Petr
</s>
</p>
<p>
<s>
Asmodai	Asmodai	k1gNnSc1	Asmodai
<g/>
:	:	kIx,	:
smilstvo	smilstvo	k1gNnSc1	smilstvo
<g/>
;	;	kIx,	;
protivník	protivník	k1gMnSc1	protivník
Svatý	svatý	k1gMnSc1	svatý
Jan	Jan	k1gMnSc1	Jan
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Křtitel	křtitel	k1gMnSc1	křtitel
nebo	nebo	k8xC	nebo
Jan	Jan	k1gMnSc1	Jan
Evangelista	evangelista	k1gMnSc1	evangelista
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Berith	Berith	k1gInSc1	Berith
<g/>
:	:	kIx,	:
vraždění	vraždění	k1gNnSc1	vraždění
a	a	k8xC	a
rouhání	rouhání	k1gNnSc1	rouhání
<g/>
;	;	kIx,	;
protivník	protivník	k1gMnSc1	protivník
Svatý	svatý	k1gMnSc1	svatý
Barnabáš	Barnabáš	k1gMnSc1	Barnabáš
</s>
</p>
<p>
<s>
Astaroth	Astaroth	k1gInSc1	Astaroth
<g/>
:	:	kIx,	:
lenost	lenost	k1gFnSc1	lenost
a	a	k8xC	a
marnivost	marnivost	k1gFnSc1	marnivost
<g/>
;	;	kIx,	;
protivník	protivník	k1gMnSc1	protivník
Svatý	svatý	k1gMnSc1	svatý
Bartoloměj	Bartoloměj	k1gMnSc1	Bartoloměj
</s>
</p>
<p>
<s>
Verrin	Verrin	k1gInSc1	Verrin
<g/>
:	:	kIx,	:
nesnášenlivost	nesnášenlivost	k1gFnSc1	nesnášenlivost
<g/>
;	;	kIx,	;
protivník	protivník	k1gMnSc1	protivník
Svatý	svatý	k1gMnSc1	svatý
Dominik	Dominik	k1gMnSc1	Dominik
</s>
</p>
<p>
<s>
Gressil	Gressit	k5eAaPmAgMnS	Gressit
<g/>
:	:	kIx,	:
nemravnost	nemravnost	k1gFnSc1	nemravnost
<g/>
,	,	kIx,	,
nečistota	nečistota	k1gFnSc1	nečistota
a	a	k8xC	a
nepoctivost	nepoctivost	k1gFnSc1	nepoctivost
<g/>
;	;	kIx,	;
protivník	protivník	k1gMnSc1	protivník
Svatý	svatý	k1gMnSc1	svatý
Bernard	Bernard	k1gMnSc1	Bernard
</s>
</p>
<p>
<s>
Sonneillon	Sonneillon	k1gInSc1	Sonneillon
<g/>
:	:	kIx,	:
nenávist	nenávist	k1gFnSc1	nenávist
<g/>
;	;	kIx,	;
protivník	protivník	k1gMnSc1	protivník
Svatý	svatý	k1gMnSc1	svatý
Štěpán	Štěpán	k1gMnSc1	Štěpán
</s>
</p>
<p>
<s>
====	====	k?	====
Druhá	druhý	k4xOgFnSc1	druhý
hierarchie	hierarchie	k1gFnSc1	hierarchie
====	====	k?	====
</s>
</p>
<p>
<s>
Lilith	Lilith	k1gInSc1	Lilith
<g/>
:	:	kIx,	:
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
Adama	Adam	k1gMnSc2	Adam
<g/>
,	,	kIx,	,
succubus	succubus	k1gInSc1	succubus
</s>
</p>
<p>
<s>
====	====	k?	====
Třetí	třetí	k4xOgFnSc1	třetí
hierarchie	hierarchie	k1gFnSc1	hierarchie
====	====	k?	====
</s>
</p>
<p>
<s>
Belial	Belial	k1gInSc1	Belial
<g/>
:	:	kIx,	:
arogance	arogance	k1gFnSc1	arogance
<g/>
;	;	kIx,	;
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
<g/>
;	;	kIx,	;
protivník	protivník	k1gMnSc1	protivník
Sv.	sv.	kA	sv.
František	František	k1gMnSc1	František
z	z	k7c2	z
Pauly	Paula	k1gFnSc2	Paula
</s>
</p>
<p>
<s>
Olivier	Olivier	k1gMnSc1	Olivier
<g/>
:	:	kIx,	:
divokost	divokost	k1gFnSc1	divokost
<g/>
,	,	kIx,	,
nenasytnost	nenasytnost	k1gFnSc1	nenasytnost
a	a	k8xC	a
zloba	zloba	k1gFnSc1	zloba
<g/>
;	;	kIx,	;
protivník	protivník	k1gMnSc1	protivník
Svatý	svatý	k1gMnSc1	svatý
Vavřinec	Vavřinec	k1gMnSc1	Vavřinec
</s>
</p>
<p>
<s>
Jouvart	Jouvart	k1gInSc1	Jouvart
<g/>
:	:	kIx,	:
sexualita	sexualita	k1gFnSc1	sexualita
<g/>
;	;	kIx,	;
protivník	protivník	k1gMnSc1	protivník
necitován	citovat	k5eNaBmNgMnS	citovat
</s>
</p>
<p>
<s>
===	===	k?	===
Barrettova	Barrettův	k2eAgFnSc1d1	Barrettova
klasifikace	klasifikace	k1gFnSc1	klasifikace
démonů	démon	k1gMnPc2	démon
===	===	k?	===
</s>
</p>
<p>
<s>
Francis	Francis	k1gFnSc1	Francis
Barrett	Barretta	k1gFnPc2	Barretta
<g/>
,	,	kIx,	,
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Magus	Magus	k1gMnSc1	Magus
(	(	kIx(	(
<g/>
1801	[number]	k4	1801
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
démony	démon	k1gMnPc4	démon
na	na	k7c4	na
prince	princ	k1gMnPc4	princ
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
představují	představovat	k5eAaImIp3nP	představovat
zlo	zlo	k1gNnSc4	zlo
<g/>
,	,	kIx,	,
na	na	k7c4	na
osoby	osoba	k1gFnPc4	osoba
nebo	nebo	k8xC	nebo
věci	věc	k1gFnPc4	věc
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Mammon	Mammon	k1gNnSc1	Mammon
<g/>
:	:	kIx,	:
svůdci	svůdce	k1gMnPc1	svůdce
</s>
</p>
<p>
<s>
Asmodai	Asmodai	k1gNnSc1	Asmodai
<g/>
:	:	kIx,	:
ohavná	ohavný	k2eAgFnSc1d1	ohavná
pomsta	pomsta	k1gFnSc1	pomsta
</s>
</p>
<p>
<s>
Satan	satan	k1gInSc1	satan
<g/>
:	:	kIx,	:
čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
</s>
</p>
<p>
<s>
Pithius	Pithius	k1gInSc1	Pithius
<g/>
:	:	kIx,	:
lháři	lhář	k1gMnPc1	lhář
a	a	k8xC	a
lživí	lživý	k2eAgMnPc1d1	lživý
démoni	démon	k1gMnPc1	démon
</s>
</p>
<p>
<s>
Belial	Belial	k1gMnSc1	Belial
<g/>
:	:	kIx,	:
podvodník	podvodník	k1gMnSc1	podvodník
a	a	k8xC	a
neprávo	neprávo	k1gNnSc1	neprávo
</s>
</p>
<p>
<s>
Merihem	Merih	k1gInSc7	Merih
<g/>
:	:	kIx,	:
mor	mor	k1gInSc1	mor
a	a	k8xC	a
zhoubná	zhoubný	k2eAgFnSc1d1	zhoubná
nákaza	nákaza	k1gFnSc1	nákaza
</s>
</p>
<p>
<s>
Abaddon	Abaddon	k1gNnSc1	Abaddon
<g/>
:	:	kIx,	:
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
zlo	zlo	k1gNnSc1	zlo
proti	proti	k7c3	proti
dobru	dobro	k1gNnSc3	dobro
</s>
</p>
<p>
<s>
Astaroth	Astaroth	k1gInSc1	Astaroth
<g/>
:	:	kIx,	:
vyšetřovatelé	vyšetřovatel	k1gMnPc1	vyšetřovatel
a	a	k8xC	a
obviňovatelé	obviňovatel	k1gMnPc1	obviňovatel
</s>
</p>
<p>
<s>
==	==	k?	==
Klasifikace	klasifikace	k1gFnPc1	klasifikace
podle	podle	k7c2	podle
měsíců	měsíc	k1gInPc2	měsíc
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
věřilo	věřit	k5eAaImAgNnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
démon	démon	k1gMnSc1	démon
je	být	k5eAaImIp3nS	být
nejsilnější	silný	k2eAgMnSc1d3	nejsilnější
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
období	období	k1gNnSc6	období
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Belial	Belial	k1gInSc1	Belial
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
</s>
</p>
<p>
<s>
Leviathan	Leviathan	k1gInSc1	Leviathan
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
</s>
</p>
<p>
<s>
Satan	satan	k1gInSc1	satan
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
</s>
</p>
<p>
<s>
Belphegor	Belphegor	k1gInSc1	Belphegor
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
</s>
</p>
<p>
<s>
Lucifer	Lucifer	k1gMnSc1	Lucifer
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
</s>
</p>
<p>
<s>
Berith	Berith	k1gInSc1	Berith
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
</s>
</p>
<p>
<s>
Belzebub	Belzebub	k1gMnSc1	Belzebub
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
</s>
</p>
<p>
<s>
Astaroth	Astaroth	k1gInSc1	Astaroth
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
</s>
</p>
<p>
<s>
Thammuz	Thammuz	k1gInSc1	Thammuz
v	v	k7c6	v
září	září	k1gNnSc6	září
</s>
</p>
<p>
<s>
Baal	Baal	k1gInSc1	Baal
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
</s>
</p>
<p>
<s>
Asmodai	Asmodai	k6eAd1	Asmodai
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
</s>
</p>
<p>
<s>
Moloch	Moloch	k1gMnSc1	Moloch
v	v	k7c6	v
prosinciToto	prosinciTota	k1gFnSc5	prosinciTota
rozdělení	rozdělení	k1gNnSc4	rozdělení
má	mít	k5eAaImIp3nS	mít
spíše	spíše	k9	spíše
astrologický	astrologický	k2eAgInSc4d1	astrologický
základ	základ	k1gInSc4	základ
<g/>
,	,	kIx,	,
než	než	k8xS	než
religiózní	religiózní	k2eAgInSc1d1	religiózní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klasifikace	klasifikace	k1gFnSc2	klasifikace
podle	podle	k7c2	podle
funkce	funkce	k1gFnSc2	funkce
==	==	k?	==
</s>
</p>
<p>
<s>
Dochovalo	dochovat	k5eAaPmAgNnS	dochovat
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
rozdělení	rozdělení	k1gNnSc2	rozdělení
podle	podle	k7c2	podle
funkce	funkce	k1gFnSc2	funkce
<g/>
,	,	kIx,	,
sepsaných	sepsaný	k2eAgFnPc2d1	sepsaná
v	v	k7c6	v
nemnoha	nemnoha	k1gFnSc1	nemnoha
grimoárech	grimoár	k1gMnPc6	grimoár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Le	Le	k1gMnSc2	Le
Dragon	Dragon	k1gMnSc1	Dragon
Rouge	rouge	k1gFnSc1	rouge
(	(	kIx(	(
<g/>
Velký	velký	k2eAgInSc1d1	velký
Grimoár	Grimoár	k1gInSc1	Grimoár
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
mnoho	mnoho	k4c1	mnoho
prací	práce	k1gFnPc2	práce
o	o	k7c6	o
mystickém	mystický	k2eAgInSc6d1	mystický
životě	život	k1gInSc6	život
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
Le	Le	k1gMnSc1	Le
Dragon	Dragon	k1gMnSc1	Dragon
Rouge	rouge	k1gFnSc1	rouge
(	(	kIx(	(
<g/>
Rudý	rudý	k2eAgInSc1d1	rudý
drak	drak	k1gInSc1	drak
<g/>
)	)	kIx)	)
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
krále	král	k1gMnSc2	král
Šalamouna	Šalamoun	k1gMnSc2	Šalamoun
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
kněží	kněz	k1gMnPc2	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
publikována	publikovat	k5eAaBmNgFnS	publikovat
v	v	k7c6	v
r.	r.	kA	r.
1517	[number]	k4	1517
egypťanem	egypťan	k1gMnSc7	egypťan
Alibecekem	Alibecek	k1gInSc7	Alibecek
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
však	však	k9	však
byla	být	k5eAaImAgFnS	být
populárně	populárně	k6eAd1	populárně
přepisována	přepisovat	k5eAaImNgFnS	přepisovat
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Grimoár	Grimoár	k1gMnSc1	Grimoár
popisuje	popisovat	k5eAaImIp3nS	popisovat
detaily	detail	k1gInPc4	detail
různých	různý	k2eAgMnPc2d1	různý
obyvatel	obyvatel	k1gMnPc2	obyvatel
pekla	péct	k5eAaImAgFnS	péct
<g/>
,	,	kIx,	,
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jej	on	k3xPp3gMnSc4	on
navštívit	navštívit	k5eAaPmF	navštívit
a	a	k8xC	a
navázat	navázat	k5eAaPmF	navázat
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
spojení	spojení	k1gNnSc4	spojení
a	a	k8xC	a
obdržet	obdržet	k5eAaPmF	obdržet
tak	tak	k6eAd1	tak
magickou	magický	k2eAgFnSc4d1	magická
sílu	síla	k1gFnSc4	síla
a	a	k8xC	a
splnit	splnit	k5eAaPmF	splnit
si	se	k3xPyFc3	se
svůj	svůj	k3xOyFgInSc4	svůj
záměr	záměr	k1gInSc4	záměr
<g/>
.	.	kIx.	.
</s>
<s>
Démoni	démon	k1gMnPc1	démon
z	z	k7c2	z
pekla	peklo	k1gNnSc2	peklo
jsou	být	k5eAaImIp3nP	být
rozděleni	rozdělit	k5eAaPmNgMnP	rozdělit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
různé	různý	k2eAgFnPc4d1	různá
vrstvy	vrstva	k1gFnPc4	vrstva
od	od	k7c2	od
generálů	generál	k1gMnPc2	generál
až	až	k9	až
po	po	k7c4	po
důstojníky	důstojník	k1gMnPc4	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
veterán	veterán	k1gMnSc1	veterán
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Le	Le	k1gMnSc1	Le
Dragon	Dragon	k1gMnSc1	Dragon
Rouge	rouge	k1gFnSc3	rouge
nosí	nosit	k5eAaImIp3nS	nosit
onyxový	onyxový	k2eAgInSc1d1	onyxový
kámen	kámen	k1gInSc1	kámen
se	s	k7c7	s
znakem	znak	k1gInSc7	znak
červeného	červený	k2eAgMnSc2d1	červený
draka	drak	k1gMnSc2	drak
v	v	k7c6	v
rudé	rudý	k2eAgFnSc6d1	rudá
sklovině	sklovina	k1gFnSc6	sklovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pseudomonarchia	Pseudomonarchia	k1gFnSc1	Pseudomonarchia
Daemonum	Daemonum	k1gInSc1	Daemonum
===	===	k?	===
</s>
</p>
<p>
<s>
Knihu	kniha	k1gFnSc4	kniha
Pseudomonarchia	Pseudomonarchius	k1gMnSc2	Pseudomonarchius
Daemonum	Daemonum	k1gNnSc4	Daemonum
napsal	napsat	k5eAaPmAgMnS	napsat
Johann	Johann	k1gMnSc1	Johann
Weyer	Weyer	k1gMnSc1	Weyer
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
grimoár	grimoár	k1gInSc4	grimoár
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
seznam	seznam	k1gInSc1	seznam
démonů	démon	k1gMnPc2	démon
a	a	k8xC	a
příslušné	příslušný	k2eAgInPc4d1	příslušný
časy	čas	k1gInPc4	čas
a	a	k8xC	a
rituály	rituál	k1gInPc4	rituál
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
vyvolání	vyvolání	k1gNnSc4	vyvolání
"	"	kIx"	"
<g/>
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
Ježíše	Ježíš	k1gMnSc2	Ježíš
a	a	k8xC	a
Ducha	duch	k1gMnSc2	duch
svatého	svatý	k2eAgMnSc2d1	svatý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
knize	kniha	k1gFnSc6	kniha
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
mnohem	mnohem	k6eAd1	mnohem
zjednodušeněji	zjednodušeně	k6eAd2	zjednodušeně
než	než	k8xS	než
v	v	k7c6	v
níže	nízce	k6eAd2	nízce
citované	citovaný	k2eAgFnSc6d1	citovaná
knize	kniha	k1gFnSc6	kniha
Menší	malý	k2eAgInPc1d2	menší
klíčky	klíček	k1gInPc1	klíček
Šalamounovy	Šalamounův	k2eAgInPc1d1	Šalamounův
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
napsána	napsat	k5eAaBmNgFnS	napsat
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1583	[number]	k4	1583
<g/>
,	,	kIx,	,
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
seznam	seznam	k1gInSc4	seznam
šedesáti	šedesát	k4xCc2	šedesát
devíti	devět	k4xCc2	devět
démonů	démon	k1gMnPc2	démon
<g/>
.	.	kIx.	.
</s>
<s>
Chybí	chybit	k5eAaPmIp3nP	chybit
démoni	démon	k1gMnPc1	démon
Vassago	Vassago	k1gMnSc1	Vassago
<g/>
,	,	kIx,	,
Seir	Seir	k1gMnSc1	Seir
<g/>
,	,	kIx,	,
Dantalion	Dantalion	k1gInSc1	Dantalion
a	a	k8xC	a
Andromalius	Andromalius	k1gInSc1	Andromalius
<g/>
.	.	kIx.	.
</s>
<s>
Pseudomonarchia	Pseudomonarchia	k1gFnSc1	Pseudomonarchia
Daemonum	Daemonum	k1gInSc1	Daemonum
démonům	démon	k1gMnPc3	démon
nepřisuzuje	přisuzovat	k5eNaImIp3nS	přisuzovat
žádné	žádný	k3yNgInPc4	žádný
znaky	znak	k1gInPc4	znak
ani	ani	k8xC	ani
atributy	atribut	k1gInPc4	atribut
<g/>
.	.	kIx.	.
</s>
<s>
Weyer	Weyer	k1gMnSc1	Weyer
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
jinými	jiný	k2eAgInPc7d1	jiný
grimoáry	grimoár	k1gInPc7	grimoár
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
od	od	k7c2	od
krále	král	k1gMnSc2	král
Šalamouna	Šalamoun	k1gMnSc2	Šalamoun
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Goetie	Goetie	k1gFnSc2	Goetie
===	===	k?	===
</s>
</p>
<p>
<s>
Nese	nést	k5eAaImIp3nS	nést
podtitul	podtitul	k1gInSc1	podtitul
Menší	malý	k2eAgInSc1d2	menší
klíčky	klíček	k1gInPc7	klíček
Šalamounovy	Šalamounův	k2eAgFnSc2d1	Šalamounova
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
označení	označení	k1gNnSc1	označení
středověkého	středověký	k2eAgInSc2d1	středověký
grimoáru	grimoár	k1gInSc2	grimoár
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgFnPc2d3	nejpopulárnější
knih	kniha	k1gFnPc2	kniha
démonolgie	démonolgie	k1gFnSc2	démonolgie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
magická	magický	k2eAgFnSc1d1	magická
příručka	příručka	k1gFnSc1	příručka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
popis	popis	k1gInSc4	popis
a	a	k8xC	a
charakteristiku	charakteristika	k1gFnSc4	charakteristika
všech	všecek	k3xTgInPc2	všecek
sedmdesáti	sedmdesát	k4xCc2	sedmdesát
dvou	dva	k4xCgInPc2	dva
démonů	démon	k1gMnPc2	démon
Goetie	Goetie	k1gFnSc2	Goetie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
též	též	k9	též
komplexní	komplexní	k2eAgInSc4d1	komplexní
návod	návod	k1gInSc4	návod
pro	pro	k7c4	pro
evokaci	evokace	k1gFnSc4	evokace
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
uvedených	uvedený	k2eAgInPc2d1	uvedený
démonů	démon	k1gMnPc2	démon
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
vyobrazeny	vyobrazit	k5eAaPmNgInP	vyobrazit
magické	magický	k2eAgInPc1d1	magický
evokační	evokační	k2eAgInPc1d1	evokační
kruhy	kruh	k1gInPc1	kruh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
evokatér	evokatér	k1gMnSc1	evokatér
k	k	k7c3	k
evokaci	evokace	k1gFnSc3	evokace
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
kvůli	kvůli	k7c3	kvůli
ochraně	ochrana	k1gFnSc3	ochrana
před	před	k7c7	před
démon	démon	k1gMnSc1	démon
a	a	k8xC	a
vypsána	vypsán	k2eAgFnSc1d1	vypsána
všechna	všechen	k3xTgNnPc4	všechen
potřebná	potřebný	k2eAgNnPc4d1	potřebné
zaklínání	zaklínání	k1gNnPc4	zaklínání
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
s	s	k7c7	s
jednotlivými	jednotlivý	k2eAgMnPc7d1	jednotlivý
démony	démon	k1gMnPc7	démon
jednat	jednat	k5eAaImF	jednat
a	a	k8xC	a
jak	jak	k6eAd1	jak
vůči	vůči	k7c3	vůči
nim	on	k3xPp3gInPc3	on
přistupovat	přistupovat	k5eAaImF	přistupovat
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
posloužilo	posloužit	k5eAaPmAgNnS	posloužit
coby	coby	k?	coby
vzor	vzor	k1gInSc1	vzor
knize	kniha	k1gFnSc6	kniha
Johana	Johan	k1gMnSc2	Johan
Wiera	Wier	k1gMnSc2	Wier
Pseudomonarchia	Pseudomonarchia	k1gFnSc1	Pseudomonarchia
Daemonum	Daemonum	k1gInSc1	Daemonum
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
Goetie	Goetie	k1gFnSc1	Goetie
tvoří	tvořit	k5eAaImIp3nS	tvořit
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
dílů	díl	k1gInPc2	díl
knihy	kniha	k1gFnSc2	kniha
Lemegeton	Lemegeton	k1gInSc1	Lemegeton
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
vydalo	vydat	k5eAaPmAgNnS	vydat
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Vodnář	vodnář	k1gMnSc1	vodnář
a	a	k8xC	a
lóže	lóže	k1gFnSc1	lóže
OLDM	OLDM	kA	OLDM
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
verze	verze	k1gFnPc1	verze
se	se	k3xPyFc4	se
obsahově	obsahově	k6eAd1	obsahově
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc1	jejich
srovnání	srovnání	k1gNnSc1	srovnání
je	být	k5eAaImIp3nS	být
náročné	náročný	k2eAgNnSc1d1	náročné
<g/>
,	,	kIx,	,
věnují	věnovat	k5eAaPmIp3nP	věnovat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
různí	různý	k2eAgMnPc1d1	různý
kritici	kritik	k1gMnPc1	kritik
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
OLDM	OLDM	kA	OLDM
připomíná	připomínat	k5eAaImIp3nS	připomínat
spíše	spíše	k9	spíše
zdánlivě	zdánlivě	k6eAd1	zdánlivě
snahu	snaha	k1gFnSc4	snaha
reprodukovat	reprodukovat	k5eAaBmF	reprodukovat
grimoár	grimoár	k1gInSc4	grimoár
z	z	k7c2	z
období	období	k1gNnSc2	období
novověku	novověk	k1gInSc2	novověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgFnPc1	některý
úpravy	úprava	k1gFnPc1	úprava
<g/>
,	,	kIx,	,
součásti	součást	k1gFnPc1	součást
a	a	k8xC	a
dodatky	dodatek	k1gInPc1	dodatek
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
post-	post-	k?	post-
<g/>
)	)	kIx)	)
<g/>
moderní	moderní	k2eAgFnSc4d1	moderní
-	-	kIx~	-
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
zjevná	zjevný	k2eAgFnSc1d1	zjevná
současná	současný	k2eAgFnSc1d1	současná
invence	invence	k1gFnSc1	invence
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
starší	starý	k2eAgFnPc4d2	starší
zavedené	zavedený	k2eAgFnPc4d1	zavedená
části	část	k1gFnPc4	část
postrádá	postrádat	k5eAaImIp3nS	postrádat
a	a	k8xC	a
v	v	k7c6	v
celku	celek	k1gInSc6	celek
nevytváří	vytvářet	k5eNaImIp3nS	vytvářet
dojem	dojem	k1gInSc4	dojem
ani	ani	k8xC	ani
snahy	snaha	k1gFnPc4	snaha
o	o	k7c4	o
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
starého	starý	k2eAgInSc2d1	starý
grimoáru	grimoár	k1gInSc2	grimoár
<g/>
.	.	kIx.	.
</s>
<s>
Verze	verze	k1gFnSc1	verze
Vodnáře	vodnář	k1gMnSc2	vodnář
postrádá	postrádat	k5eAaImIp3nS	postrádat
některé	některý	k3yIgInPc4	některý
archaické	archaický	k2eAgInPc4d1	archaický
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
obsahem	obsah	k1gInSc7	obsah
i	i	k9	i
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
přítomnosti	přítomnost	k1gFnSc3	přítomnost
dalších	další	k2eAgInPc2d1	další
souvisejících	související	k2eAgInPc2d1	související
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
míněna	míněn	k2eAgFnSc1d1	míněna
jako	jako	k8xS	jako
kniha	kniha	k1gFnSc1	kniha
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
současnou	současný	k2eAgFnSc4d1	současná
démonologickou	démonologický	k2eAgFnSc4d1	démonologický
praxi	praxe	k1gFnSc4	praxe
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
verze	verze	k1gFnPc1	verze
jsou	být	k5eAaImIp3nP	být
problematické	problematický	k2eAgInPc1d1	problematický
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
kvality	kvalita	k1gFnSc2	kvalita
obsahu	obsah	k1gInSc2	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Záměr	záměr	k1gInSc1	záměr
publikace	publikace	k1gFnSc2	publikace
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
OLDM	OLDM	kA	OLDM
je	být	k5eAaImIp3nS	být
nejasný	jasný	k2eNgMnSc1d1	nejasný
<g/>
,	,	kIx,	,
vzhledem	vzhled	k1gInSc7	vzhled
i	i	k9	i
k	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
publikacím	publikace	k1gFnPc3	publikace
a	a	k8xC	a
doplňujícím	doplňující	k2eAgInPc3d1	doplňující
textům	text	k1gInPc3	text
ve	v	k7c6	v
vydání	vydání	k1gNnSc6	vydání
nakl	nakla	k1gFnPc2	nakla
<g/>
.	.	kIx.	.
</s>
<s>
Vodnář	vodnář	k1gMnSc1	vodnář
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
se	se	k3xPyFc4	se
domnívat	domnívat	k5eAaImF	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
prakticky	prakticky	k6eAd1	prakticky
zaměřenou	zaměřený	k2eAgFnSc4d1	zaměřená
démonologickou	démonologický	k2eAgFnSc4d1	démonologický
knihu	kniha	k1gFnSc4	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
verze	verze	k1gFnPc1	verze
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
původnímu	původní	k2eAgInSc3d1	původní
textu	text	k1gInSc3	text
či	či	k8xC	či
textům	text	k1gInPc3	text
tendenci	tendence	k1gFnSc4	tendence
k	k	k7c3	k
přijímání	přijímání	k1gNnSc3	přijímání
prvků	prvek	k1gInPc2	prvek
z	z	k7c2	z
rozličných	rozličný	k2eAgInPc2d1	rozličný
zdrojů	zdroj	k1gInPc2	zdroj
v	v	k7c6	v
post-moderním	postoderní	k2eAgMnSc6d1	post-moderní
duchu	duch	k1gMnSc6	duch
techniky	technika	k1gFnSc2	technika
koláže	koláž	k1gFnPc1	koláž
a	a	k8xC	a
vynechávání	vynechávání	k1gNnPc1	vynechávání
částí	část	k1gFnPc2	část
původních	původní	k2eAgFnPc2d1	původní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Demonology	Demonolog	k1gMnPc7	Demonolog
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Classification	Classification	k1gInSc1	Classification
of	of	k?	of
demons	demons	k6eAd1	demons
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
August	August	k1gMnSc1	August
Ukert	Ukert	k1gInSc1	Ukert
<g/>
:	:	kIx,	:
Über	Über	k1gInSc1	Über
Dämonen	Dämonen	k2eAgInSc1d1	Dämonen
<g/>
,	,	kIx,	,
Heroen	Heroen	k2eAgInSc1d1	Heroen
und	und	k?	und
Genien	Genien	k1gInSc1	Genien
(	(	kIx(	(
<g/>
Leipzig	Leipzig	k1gInSc1	Leipzig
1850	[number]	k4	1850
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
P.	P.	kA	P.
Tillich	Tillich	k1gMnSc1	Tillich
<g/>
:	:	kIx,	:
Das	Das	k1gFnSc1	Das
Dämonische	Dämonische	k1gFnSc1	Dämonische
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
M.	M.	kA	M.
Nilsson	Nilsson	k1gNnSc1	Nilsson
<g/>
:	:	kIx,	:
Geschichte	Geschicht	k1gMnSc5	Geschicht
der	drát	k5eAaImRp2nS	drát
griechischen	griechischen	k1gInSc4	griechischen
Religion	religion	k1gInSc1	religion
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Egon	Egon	k1gMnSc1	Egon
von	von	k1gInSc4	von
Petersdorff	Petersdorff	k1gInSc1	Petersdorff
<g/>
:	:	kIx,	:
Daemonologie	Daemonologie	k1gFnSc1	Daemonologie
2	[number]	k4	2
Bd	Bd	k1gFnPc2	Bd
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Bd	Bd	k1gMnSc1	Bd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
:	:	kIx,	:
Daemonen	Daemonen	k1gInSc1	Daemonen
im	im	k?	im
Weltenplan	Weltenplan	k1gInSc1	Weltenplan
<g/>
.	.	kIx.	.
<g/>
;	;	kIx,	;
<g/>
Bd	Bd	k1gMnSc1	Bd
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
:	:	kIx,	:
Daemonen	Daemonen	k1gInSc1	Daemonen
am	am	k?	am
Werk	Werk	k1gInSc1	Werk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Christiana	Christian	k1gMnSc4	Christian
Verlag	Verlag	k1gInSc4	Verlag
<g/>
,	,	kIx,	,
Stein	Stein	k1gMnSc1	Stein
a.	a.	k?	a.
Rhein	Rhein	k1gMnSc1	Rhein
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tomáš	Tomáš	k1gMnSc1	Tomáš
Akvinský	Akvinský	k2eAgMnSc1d1	Akvinský
<g/>
,	,	kIx,	,
Summa	Summa	k1gFnSc1	Summa
Theologica	Theologica	k1gFnSc1	Theologica
(	(	kIx(	(
<g/>
1274	[number]	k4	1274
<g/>
)	)	kIx)	)
–	–	k?	–
čarodějnictví	čarodějnictví	k1gNnSc1	čarodějnictví
</s>
</p>
<p>
<s>
The	The	k?	The
Sworn	Sworn	k1gMnSc1	Sworn
Book	Book	k1gMnSc1	Book
of	of	k?	of
Honorius	Honorius	k1gMnSc1	Honorius
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
–	–	k?	–
čarodějnictví	čarodějnictví	k1gNnSc1	čarodějnictví
</s>
</p>
<p>
<s>
Nicholas	Nicholas	k1gMnSc1	Nicholas
Magni	Magn	k1gMnPc1	Magn
<g/>
,	,	kIx,	,
Tractatus	Tractatus	k1gMnSc1	Tractatus
de	de	k?	de
superstitionibus	superstitionibus	k1gMnSc1	superstitionibus
(	(	kIx(	(
<g/>
1405	[number]	k4	1405
<g/>
)	)	kIx)	)
–	–	k?	–
křesťanství	křesťanství	k1gNnSc2	křesťanství
</s>
</p>
<p>
<s>
Johannes	Johannes	k1gMnSc1	Johannes
Hartlieb	Hartliba	k1gFnPc2	Hartliba
<g/>
,	,	kIx,	,
Buch	buch	k1gInSc1	buch
aller	aller	k1gInSc1	aller
verpoten	verpoten	k2eAgInSc1d1	verpoten
kunst	kunst	k1gInSc1	kunst
(	(	kIx(	(
<g/>
1456	[number]	k4	1456
<g/>
)	)	kIx)	)
–	–	k?	–
čarodějnictví	čarodějnictví	k1gNnSc1	čarodějnictví
</s>
</p>
<p>
<s>
Kniha	kniha	k1gFnSc1	kniha
Abramelinova	Abramelinův	k2eAgFnSc1d1	Abramelinova
(	(	kIx(	(
<g/>
1450	[number]	k4	1450
<g/>
)	)	kIx)	)
–	–	k?	–
čarodějnictví	čarodějnictví	k1gNnSc1	čarodějnictví
</s>
</p>
<p>
<s>
Heinrich	Heinrich	k1gMnSc1	Heinrich
Kramer	Kramero	k1gNnPc2	Kramero
a	a	k8xC	a
Jacob	Jacoba	k1gFnPc2	Jacoba
Sprenger	Sprenger	k1gMnSc1	Sprenger
<g/>
,	,	kIx,	,
Malleus	Malleus	k1gMnSc1	Malleus
Maleficarum	Maleficarum	k1gInSc1	Maleficarum
(	(	kIx(	(
<g/>
1486	[number]	k4	1486
<g/>
)	)	kIx)	)
–	–	k?	–
křesťanství	křesťanství	k1gNnSc2	křesťanství
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
of	of	k?	of
Arles	Arles	k1gMnSc1	Arles
<g/>
,	,	kIx,	,
Tractatus	Tractatus	k1gMnSc1	Tractatus
de	de	k?	de
superstitionibus	superstitionibus	k1gMnSc1	superstitionibus
(	(	kIx(	(
<g/>
1515	[number]	k4	1515
<g/>
)	)	kIx)	)
–	–	k?	–
křesťanství	křesťanství	k1gNnSc2	křesťanství
</s>
</p>
<p>
<s>
Šalamounův	Šalamounův	k2eAgInSc1d1	Šalamounův
klíč	klíč	k1gInSc1	klíč
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
–	–	k?	–
čarodějnictví	čarodějnictví	k1gNnSc1	čarodějnictví
</s>
</p>
<p>
<s>
Nicolas	Nicolas	k1gInSc1	Nicolas
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Daemonolatreiae	Daemonolatreiae	k1gNnSc2	Daemonolatreiae
libri	libr	k1gFnSc2	libr
tres	tresa	k1gFnPc2	tresa
(	(	kIx(	(
<g/>
1595	[number]	k4	1595
<g/>
)	)	kIx)	)
–	–	k?	–
křesťanství	křesťanství	k1gNnSc2	křesťanství
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Démon	démon	k1gMnSc1	démon
</s>
</p>
<p>
<s>
Hřích	hřích	k1gInSc1	hřích
</s>
</p>
<p>
<s>
Sedm	sedm	k4xCc1	sedm
hlavních	hlavní	k2eAgInPc2d1	hlavní
hříchů	hřích	k1gInPc2	hřích
</s>
</p>
<p>
<s>
Anděl	Anděl	k1gMnSc1	Anděl
</s>
</p>
<p>
<s>
Satanismus	satanismus	k1gInSc1	satanismus
</s>
</p>
<p>
<s>
Dante	Dante	k1gMnSc1	Dante
Alighieri	Alighier	k1gFnSc2	Alighier
-	-	kIx~	-
Božská	božský	k2eAgFnSc1d1	božská
komedie	komedie	k1gFnSc1	komedie
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Démonologie	démonologie	k1gFnSc2	démonologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Daemonologie	Daemonologie	k1gFnSc2	Daemonologie
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
Z	z	k7c2	z
list	lista	k1gFnPc2	lista
of	of	k?	of
Demons	Demons	k1gInSc1	Demons
<g/>
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
anglicky	anglicky	k6eAd1	anglicky
</s>
</p>
<p>
<s>
OFS	OFS	kA	OFS
Demonolatry	Demonolatr	k1gInPc4	Demonolatr
anglicky	anglicky	k6eAd1	anglicky
</s>
</p>
<p>
<s>
Lemegeton	Lemegeton	k1gInSc1	Lemegeton
OLDM	OLDM	kA	OLDM
</s>
</p>
