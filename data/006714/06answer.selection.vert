<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
deismus	deismus	k1gInSc1	deismus
<g/>
"	"	kIx"	"
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
kalvínský	kalvínský	k2eAgMnSc1d1	kalvínský
teolog	teolog	k1gMnSc1	teolog
Pierre	Pierr	k1gInSc5	Pierr
Viret	Viret	k1gMnSc1	Viret
roku	rok	k1gInSc2	rok
1564	[number]	k4	1564
jako	jako	k8xS	jako
pejorativní	pejorativní	k2eAgNnSc4d1	pejorativní
označení	označení	k1gNnSc4	označení
svých	svůj	k3xOyFgMnPc2	svůj
protivníků	protivník	k1gMnPc2	protivník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
odmítali	odmítat	k5eAaImAgMnP	odmítat
Boží	boží	k2eAgNnPc4d1	boží
zjevení	zjevení	k1gNnPc4	zjevení
a	a	k8xC	a
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
