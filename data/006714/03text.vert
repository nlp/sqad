<s>
Deismus	deismus	k1gInSc1	deismus
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgMnSc2d1	latinský
deus	deus	k6eAd1	deus
–	–	k?	–
Bůh	bůh	k1gMnSc1	bůh
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
souhrnné	souhrnný	k2eAgNnSc4d1	souhrnné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
náboženské	náboženský	k2eAgInPc4d1	náboženský
a	a	k8xC	a
světové	světový	k2eAgInPc4d1	světový
názory	názor	k1gInPc4	názor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
racionalistickou	racionalistický	k2eAgFnSc7d1	racionalistická
kritikou	kritika	k1gFnSc7	kritika
křesťanství	křesťanství	k1gNnSc2	křesťanství
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vyústily	vyústit	k5eAaPmAgFnP	vyústit
do	do	k7c2	do
francouzského	francouzský	k2eAgMnSc2d1	francouzský
a	a	k8xC	a
německého	německý	k2eAgNnSc2d1	německé
osvícenství	osvícenství	k1gNnSc2	osvícenství
<g/>
.	.	kIx.	.
</s>
<s>
Angličtí	anglický	k2eAgMnPc1d1	anglický
deisté	deista	k1gMnPc1	deista
většinou	většinou	k6eAd1	většinou
uznávali	uznávat	k5eAaImAgMnP	uznávat
hlavní	hlavní	k2eAgInPc4d1	hlavní
metafyzické	metafyzický	k2eAgInPc4d1	metafyzický
principy	princip	k1gInPc4	princip
křesťanství	křesťanství	k1gNnSc2	křesťanství
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
Bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
stvořil	stvořit	k5eAaPmAgMnS	stvořit
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
že	že	k8xS	že
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
rozum	rozum	k1gInSc4	rozum
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgNnSc4d1	vlastní
svědomí	svědomí	k1gNnSc4	svědomí
a	a	k8xC	a
tedy	tedy	k9	tedy
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
a	a	k8xC	a
odmítali	odmítat	k5eAaImAgMnP	odmítat
všechno	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
lidské	lidský	k2eAgNnSc1d1	lidské
chápání	chápání	k1gNnSc1	chápání
<g/>
:	:	kIx,	:
Boží	boží	k2eAgNnSc1d1	boží
zjevení	zjevení	k1gNnSc1	zjevení
<g/>
,	,	kIx,	,
proroctví	proroctví	k1gNnSc1	proroctví
<g/>
,	,	kIx,	,
zázraky	zázrak	k1gInPc1	zázrak
<g/>
,	,	kIx,	,
autoritu	autorita	k1gFnSc4	autorita
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
i	i	k9	i
představu	představa	k1gFnSc4	představa
posmrtného	posmrtný	k2eAgInSc2d1	posmrtný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Svět	svět	k1gInSc1	svět
sice	sice	k8xC	sice
je	být	k5eAaImIp3nS	být
dílo	dílo	k1gNnSc4	dílo
Boží	boží	k2eAgFnSc2d1	boží
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
si	se	k3xPyFc3	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
však	však	k9	však
vystačí	vystačit	k5eAaBmIp3nS	vystačit
sám	sám	k3xTgMnSc1	sám
svým	svůj	k3xOyFgInSc7	svůj
rozumem	rozum	k1gInSc7	rozum
a	a	k8xC	a
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
žádné	žádný	k3yNgInPc4	žádný
nadpřirozené	nadpřirozený	k2eAgInPc4d1	nadpřirozený
zásahy	zásah	k1gInPc4	zásah
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
zákonitého	zákonitý	k2eAgInSc2d1	zákonitý
běhu	běh	k1gInSc2	běh
<g/>
.	.	kIx.	.
</s>
<s>
Deismus	deismus	k1gInSc1	deismus
i	i	k8xC	i
osvícenství	osvícenství	k1gNnSc1	osvícenství
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
spoléhají	spoléhat	k5eAaImIp3nP	spoléhat
na	na	k7c4	na
individuální	individuální	k2eAgFnPc4d1	individuální
lidské	lidský	k2eAgFnPc4d1	lidská
možnosti	možnost	k1gFnPc4	možnost
a	a	k8xC	a
schopnosti	schopnost	k1gFnPc4	schopnost
pokroku	pokrok	k1gInSc2	pokrok
<g/>
,	,	kIx,	,
prosazují	prosazovat	k5eAaImIp3nP	prosazovat
lidskou	lidský	k2eAgFnSc4d1	lidská
samostatnost	samostatnost	k1gFnSc4	samostatnost
<g/>
,	,	kIx,	,
individuální	individuální	k2eAgFnSc4d1	individuální
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
toleranci	tolerance	k1gFnSc4	tolerance
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgInPc1d1	podobný
názory	názor	k1gInPc1	názor
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
už	už	k6eAd1	už
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
(	(	kIx(	(
<g/>
Platón	platón	k1gInSc1	platón
<g/>
,	,	kIx,	,
Epikúros	Epikúrosa	k1gFnPc2	Epikúrosa
<g/>
,	,	kIx,	,
Plútarchos	Plútarchos	k1gMnSc1	Plútarchos
<g/>
,	,	kIx,	,
Cicero	Cicero	k1gMnSc1	Cicero
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
buddhismu	buddhismus	k1gInSc6	buddhismus
nebo	nebo	k8xC	nebo
v	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
islámu	islám	k1gInSc6	islám
(	(	kIx(	(
<g/>
Averroes	Averroes	k1gInSc1	Averroes
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
převládají	převládat	k5eAaImIp3nP	převládat
<g/>
.	.	kIx.	.
</s>
<s>
Německý	německý	k2eAgMnSc1d1	německý
sociolog	sociolog	k1gMnSc1	sociolog
Max	Max	k1gMnSc1	Max
Weber	Weber	k1gMnSc1	Weber
mluvil	mluvit	k5eAaImAgMnS	mluvit
o	o	k7c6	o
procesu	proces	k1gInSc6	proces
"	"	kIx"	"
<g/>
odkouzlení	odkouzlení	k1gNnSc6	odkouzlení
světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
deismus	deismus	k1gInSc1	deismus
<g/>
"	"	kIx"	"
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
kalvínský	kalvínský	k2eAgMnSc1d1	kalvínský
teolog	teolog	k1gMnSc1	teolog
Pierre	Pierr	k1gInSc5	Pierr
Viret	Viret	k1gMnSc1	Viret
roku	rok	k1gInSc2	rok
1564	[number]	k4	1564
jako	jako	k8xC	jako
pejorativní	pejorativní	k2eAgNnSc4d1	pejorativní
označení	označení	k1gNnSc4	označení
svých	svůj	k3xOyFgMnPc2	svůj
protivníků	protivník	k1gMnPc2	protivník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
odmítali	odmítat	k5eAaImAgMnP	odmítat
Boží	boží	k2eAgNnPc4d1	boží
zjevení	zjevení	k1gNnPc4	zjevení
a	a	k8xC	a
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Deistické	deistický	k2eAgInPc1d1	deistický
názory	názor	k1gInPc1	názor
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
s	s	k7c7	s
rozpadem	rozpad	k1gInSc7	rozpad
středověkého	středověký	k2eAgInSc2d1	středověký
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
s	s	k7c7	s
oslabením	oslabení	k1gNnSc7	oslabení
veřejného	veřejný	k2eAgInSc2d1	veřejný
vlivu	vliv	k1gInSc2	vliv
náboženství	náboženství	k1gNnSc2	náboženství
v	v	k7c6	v
renesanci	renesance	k1gFnSc6	renesance
<g/>
,	,	kIx,	,
s	s	k7c7	s
objevem	objev	k1gInSc7	objev
antické	antický	k2eAgFnSc2d1	antická
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
se	s	k7c7	s
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
zájmem	zájem	k1gInSc7	zájem
o	o	k7c4	o
poznávání	poznávání	k1gNnSc4	poznávání
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
se	s	k7c7	s
zámořskými	zámořský	k2eAgInPc7d1	zámořský
objevy	objev	k1gInPc7	objev
i	i	k9	i
zápasem	zápas	k1gInSc7	zápas
zejména	zejména	k9	zejména
vzdělaných	vzdělaný	k2eAgFnPc2d1	vzdělaná
vrstev	vrstva	k1gFnPc2	vrstva
o	o	k7c4	o
větší	veliký	k2eAgFnSc4d2	veliký
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
mezi	mezi	k7c7	mezi
vzdělanci	vzdělanec	k1gMnPc7	vzdělanec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
jako	jako	k9	jako
reakce	reakce	k1gFnPc1	reakce
na	na	k7c6	na
občanské	občanský	k2eAgFnSc6d1	občanská
a	a	k8xC	a
náboženské	náboženský	k2eAgFnSc2d1	náboženská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
vyvolané	vyvolaný	k2eAgNnSc1d1	vyvolané
reformací	reformace	k1gFnSc7	reformace
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
naléhavé	naléhavý	k2eAgFnSc2d1	naléhavá
potřeby	potřeba	k1gFnSc2	potřeba
náboženské	náboženský	k2eAgFnSc2d1	náboženská
tolerance	tolerance	k1gFnSc2	tolerance
<g/>
.	.	kIx.	.
</s>
<s>
Erasmus	Erasmus	k1gMnSc1	Erasmus
Rotterdamský	rotterdamský	k2eAgMnSc1d1	rotterdamský
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
More	mor	k1gInSc5	mor
<g/>
,	,	kIx,	,
Michel	Michel	k1gMnSc1	Michel
de	de	k?	de
Montaigne	Montaign	k1gMnSc5	Montaign
<g/>
,	,	kIx,	,
Jean	Jean	k1gMnSc1	Jean
Bodin	Bodin	k1gMnSc1	Bodin
nebo	nebo	k8xC	nebo
Hugo	Hugo	k1gMnSc1	Hugo
Grotius	Grotius	k1gMnSc1	Grotius
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
všichni	všechen	k3xTgMnPc1	všechen
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
o	o	k7c4	o
rozumové	rozumový	k2eAgNnSc4d1	rozumové
umírnění	umírnění	k1gNnSc4	umírnění
náboženských	náboženský	k2eAgInPc2d1	náboženský
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Deisté	deista	k1gMnPc1	deista
zastávali	zastávat	k5eAaImAgMnP	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgNnSc1	jaký
lidská	lidský	k2eAgFnSc1d1	lidská
společnost	společnost	k1gFnSc1	společnost
nutně	nutně	k6eAd1	nutně
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
založit	založit	k5eAaPmF	založit
na	na	k7c6	na
rozumovém	rozumový	k2eAgInSc6d1	rozumový
náhledu	náhled	k1gInSc6	náhled
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
všem	všecek	k3xTgMnPc3	všecek
lidem	člověk	k1gMnPc3	člověk
stejně	stejně	k6eAd1	stejně
vrozený	vrozený	k2eAgMnSc1d1	vrozený
<g/>
,	,	kIx,	,
sami	sám	k3xTgMnPc1	sám
se	se	k3xPyFc4	se
nazývali	nazývat	k5eAaImAgMnP	nazývat
"	"	kIx"	"
<g/>
svobodnými	svobodný	k2eAgMnPc7d1	svobodný
mysliteli	myslitel	k1gMnPc7	myslitel
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
freethinkers	freethinkers	k6eAd1	freethinkers
<g/>
)	)	kIx)	)
a	a	k8xC	a
opírali	opírat	k5eAaImAgMnP	opírat
se	se	k3xPyFc4	se
o	o	k7c4	o
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
teologii	teologie	k1gFnSc4	teologie
a	a	k8xC	a
přirozené	přirozený	k2eAgNnSc4d1	přirozené
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Jakožto	jakožto	k8xS	jakožto
racionalistické	racionalistický	k2eAgNnSc4d1	racionalistické
a	a	k8xC	a
individualistické	individualistický	k2eAgNnSc4d1	individualistické
hnutí	hnutí	k1gNnPc4	hnutí
nebyl	být	k5eNaImAgInS	být
deismus	deismus	k1gInSc1	deismus
dlouho	dlouho	k6eAd1	dlouho
příliš	příliš	k6eAd1	příliš
populární	populární	k2eAgMnSc1d1	populární
a	a	k8xC	a
oslovoval	oslovovat	k5eAaImAgMnS	oslovovat
především	především	k9	především
vzdělance	vzdělanec	k1gMnSc4	vzdělanec
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgInSc1d1	britský
deismus	deismus	k1gInSc1	deismus
začíná	začínat	k5eAaImIp3nS	začínat
dílem	dílo	k1gNnSc7	dílo
Herberta	Herbert	k1gMnSc2	Herbert
z	z	k7c2	z
Cherbury	Cherbura	k1gFnSc2	Cherbura
(	(	kIx(	(
<g/>
1581	[number]	k4	1581
<g/>
-	-	kIx~	-
<g/>
1648	[number]	k4	1648
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zastánce	zastánce	k1gMnSc2	zastánce
přirozeného	přirozený	k2eAgMnSc2d1	přirozený
<g/>
,	,	kIx,	,
rozumově	rozumově	k6eAd1	rozumově
založeného	založený	k2eAgNnSc2d1	založené
křesťanství	křesťanství	k1gNnSc2	křesťanství
a	a	k8xC	a
"	"	kIx"	"
<g/>
vrozených	vrozený	k2eAgFnPc2d1	vrozená
idejí	idea	k1gFnPc2	idea
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgMnPc7	jenž
si	se	k3xPyFc3	se
člověk	člověk	k1gMnSc1	člověk
i	i	k8xC	i
společnost	společnost	k1gFnSc1	společnost
mohou	moct	k5eAaImIp3nP	moct
vystačit	vystačit	k5eAaBmF	vystačit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
latinská	latinský	k2eAgFnSc1d1	Latinská
kniha	kniha	k1gFnSc1	kniha
"	"	kIx"	"
<g/>
O	o	k7c6	o
pravdě	pravda	k1gFnSc6	pravda
<g/>
"	"	kIx"	"
vyšla	vyjít	k5eAaPmAgFnS	vyjít
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
roku	rok	k1gInSc2	rok
1624	[number]	k4	1624
<g/>
.	.	kIx.	.
</s>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Hobbes	Hobbes	k1gMnSc1	Hobbes
(	(	kIx(	(
<g/>
1588	[number]	k4	1588
<g/>
-	-	kIx~	-
<g/>
1679	[number]	k4	1679
<g/>
)	)	kIx)	)
a	a	k8xC	a
Baruch	Baruch	k1gMnSc1	Baruch
Spinoza	Spinoz	k1gMnSc2	Spinoz
(	(	kIx(	(
<g/>
1632	[number]	k4	1632
<g/>
-	-	kIx~	-
<g/>
1677	[number]	k4	1677
<g/>
)	)	kIx)	)
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
přidali	přidat	k5eAaPmAgMnP	přidat
kritiku	kritika	k1gFnSc4	kritika
Bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1690	[number]	k4	1690
ale	ale	k8xC	ale
vydal	vydat	k5eAaPmAgMnS	vydat
John	John	k1gMnSc1	John
Locke	Lock	k1gFnSc2	Lock
(	(	kIx(	(
<g/>
1632	[number]	k4	1632
<g/>
-	-	kIx~	-
<g/>
1704	[number]	k4	1704
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Esej	esej	k1gFnSc1	esej
o	o	k7c6	o
lidském	lidský	k2eAgInSc6d1	lidský
rozumu	rozum	k1gInSc6	rozum
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
právě	právě	k9	právě
myšlenku	myšlenka	k1gFnSc4	myšlenka
vrozených	vrozený	k2eAgFnPc2d1	vrozená
idejí	idea	k1gFnPc2	idea
ostře	ostro	k6eAd1	ostro
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
a	a	k8xC	a
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Oporou	opora	k1gFnSc7	opora
deistického	deistický	k2eAgNnSc2d1	deistický
myšlení	myšlení	k1gNnSc2	myšlení
tak	tak	k6eAd1	tak
přestal	přestat	k5eAaPmAgInS	přestat
být	být	k5eAaImF	být
rozum	rozum	k1gInSc4	rozum
a	a	k8xC	a
božský	božský	k2eAgInSc4d1	božský
zákon	zákon	k1gInSc4	zákon
a	a	k8xC	a
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
místo	místo	k1gNnSc4	místo
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
přímá	přímý	k2eAgFnSc1d1	přímá
a	a	k8xC	a
zejména	zejména	k9	zejména
smyslová	smyslový	k2eAgFnSc1d1	smyslová
zkušenost	zkušenost	k1gFnSc1	zkušenost
(	(	kIx(	(
<g/>
empirismus	empirismus	k1gInSc1	empirismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
deisty	deista	k1gMnPc7	deista
objevují	objevovat	k5eAaImIp3nP	objevovat
radikální	radikální	k2eAgMnPc1d1	radikální
skeptici	skeptik	k1gMnPc1	skeptik
(	(	kIx(	(
<g/>
David	David	k1gMnSc1	David
Hume	Hum	k1gFnSc2	Hum
<g/>
)	)	kIx)	)
a	a	k8xC	a
materialisté	materialista	k1gMnPc1	materialista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
pak	pak	k6eAd1	pak
převládli	převládnout	k5eAaPmAgMnP	převládnout
ve	v	k7c6	v
francouzském	francouzský	k2eAgNnSc6d1	francouzské
osvícenství	osvícenství	k1gNnSc1	osvícenství
(	(	kIx(	(
<g/>
Voltaire	Voltair	k1gMnSc5	Voltair
<g/>
,	,	kIx,	,
Rousseau	Rousseau	k1gMnSc1	Rousseau
<g/>
,	,	kIx,	,
Pierre	Pierr	k1gMnSc5	Pierr
Bayle	Bayl	k1gMnSc5	Bayl
<g/>
,	,	kIx,	,
encyklopedisté	encyklopedista	k1gMnPc1	encyklopedista
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
myšlenka	myšlenka	k1gFnSc1	myšlenka
dějinného	dějinný	k2eAgNnSc2d1	dějinné
(	(	kIx(	(
<g/>
automatického	automatický	k2eAgMnSc2d1	automatický
<g/>
)	)	kIx)	)
pokroku	pokrok	k1gInSc2	pokrok
byla	být	k5eAaImAgFnS	být
anglickému	anglický	k2eAgInSc3d1	anglický
deismu	deismus	k1gInSc3	deismus
ještě	ještě	k6eAd1	ještě
cizí	cizí	k2eAgMnSc1d1	cizí
<g/>
.	.	kIx.	.
</s>
<s>
Zato	zato	k6eAd1	zato
měl	mít	k5eAaImAgInS	mít
deismus	deismus	k1gInSc1	deismus
velký	velký	k2eAgInSc1d1	velký
politický	politický	k2eAgInSc4d1	politický
význam	význam	k1gInSc4	význam
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
spojen	spojit	k5eAaPmNgMnS	spojit
se	s	k7c7	s
zápasem	zápas	k1gInSc7	zápas
o	o	k7c4	o
občanské	občanský	k2eAgFnPc4d1	občanská
svobody	svoboda	k1gFnPc4	svoboda
a	a	k8xC	a
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Kritika	kritika	k1gFnSc1	kritika
náboženství	náboženství	k1gNnSc2	náboženství
a	a	k8xC	a
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
římský	římský	k2eAgInSc4d1	římský
katolicismus	katolicismus	k1gInSc4	katolicismus
a	a	k8xC	a
papeže	papež	k1gMnSc2	papež
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
obrátila	obrátit	k5eAaPmAgFnS	obrátit
i	i	k9	i
proti	proti	k7c3	proti
Bibli	bible	k1gFnSc3	bible
a	a	k8xC	a
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
mnoho	mnoho	k4c4	mnoho
polemik	polemika	k1gFnPc2	polemika
<g/>
.	.	kIx.	.
</s>
<s>
Deisté	deista	k1gMnPc1	deista
byli	být	k5eAaImAgMnP	být
většinou	většinou	k6eAd1	většinou
zastánci	zastánce	k1gMnPc1	zastánce
republikánského	republikánský	k2eAgNnSc2d1	republikánské
zřízení	zřízení	k1gNnSc2	zřízení
a	a	k8xC	a
měli	mít	k5eAaImAgMnP	mít
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
vliv	vliv	k1gInSc4	vliv
na	na	k7c6	na
formulaci	formulace	k1gFnSc6	formulace
myšlenky	myšlenka	k1gFnSc2	myšlenka
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
i	i	k9	i
na	na	k7c4	na
vznik	vznik	k1gInSc4	vznik
politického	politický	k2eAgInSc2d1	politický
liberalismu	liberalismus	k1gInSc2	liberalismus
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zažil	zažít	k5eAaPmAgMnS	zažít
deismus	deismus	k1gInSc4	deismus
v	v	k7c6	v
méně	málo	k6eAd2	málo
racionalistické	racionalistický	k2eAgFnSc6d1	racionalistická
podobě	podoba	k1gFnSc6	podoba
novou	nový	k2eAgFnSc4d1	nová
vlnu	vlna	k1gFnSc4	vlna
oživení	oživení	k1gNnSc2	oživení
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Podstatně	podstatně	k6eAd1	podstatně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
nejen	nejen	k6eAd1	nejen
vznik	vznik	k1gInSc1	vznik
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc6	jejich
ústavu	ústav	k1gInSc6	ústav
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
celý	celý	k2eAgInSc1d1	celý
duchovní	duchovní	k2eAgInSc1d1	duchovní
život	život	k1gInSc1	život
v	v	k7c6	v
USA	USA	kA	USA
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
Thomas	Thomas	k1gMnSc1	Thomas
Jefferson	Jefferson	k1gMnSc1	Jefferson
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Madison	Madison	k1gMnSc1	Madison
<g/>
,	,	kIx,	,
Henry	Henry	k1gMnSc1	Henry
Wadsworth	Wadsworth	k1gMnSc1	Wadsworth
Longfellow	Longfellow	k1gMnSc1	Longfellow
<g/>
,	,	kIx,	,
Ralph	Ralph	k1gMnSc1	Ralph
Waldo	Waldo	k1gNnSc1	Waldo
Emerson	Emerson	k1gMnSc1	Emerson
<g/>
,	,	kIx,	,
Walt	Walt	k1gMnSc1	Walt
Whitman	Whitman	k1gMnSc1	Whitman
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Unitářské	unitářský	k2eAgNnSc1d1	unitářské
hnutí	hnutí	k1gNnSc1	hnutí
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
z	z	k7c2	z
amerického	americký	k2eAgInSc2d1	americký
deismu	deismus	k1gInSc2	deismus
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Světová	světový	k2eAgFnSc1d1	světová
unie	unie	k1gFnSc1	unie
deistů	deista	k1gMnPc2	deista
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
WUD	WUD	kA	WUD
<g/>
)	)	kIx)	)
a	a	k8xC	a
deistické	deistický	k2eAgFnPc4d1	deistická
myšlenky	myšlenka	k1gFnPc4	myšlenka
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
ve	v	k7c6	v
vyjádřeních	vyjádření	k1gNnPc6	vyjádření
mnoha	mnoho	k4c2	mnoho
spisovatelů	spisovatel	k1gMnPc2	spisovatel
<g/>
,	,	kIx,	,
umělců	umělec	k1gMnPc2	umělec
i	i	k8xC	i
vědců	vědec	k1gMnPc2	vědec
–	–	k?	–
například	například	k6eAd1	například
A.	A.	kA	A.
Einsteina	Einstein	k1gMnSc2	Einstein
<g/>
.	.	kIx.	.
</s>
<s>
Deistické	deistický	k2eAgNnSc4d1	deistický
myšlení	myšlení	k1gNnSc4	myšlení
a	a	k8xC	a
literaturu	literatura	k1gFnSc4	literatura
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
smyslu	smysl	k1gInSc6	smysl
charakterizují	charakterizovat	k5eAaBmIp3nP	charakterizovat
některé	některý	k3yIgInPc4	některý
pojmy	pojem	k1gInPc4	pojem
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
nahrazovali	nahrazovat	k5eAaImAgMnP	nahrazovat
tradiční	tradiční	k2eAgInSc4d1	tradiční
pojem	pojem	k1gInSc4	pojem
Boha	bůh	k1gMnSc2	bůh
<g/>
:	:	kIx,	:
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
bytost	bytost	k1gFnSc1	bytost
Prozřetelnost	prozřetelnost	k1gFnSc4	prozřetelnost
Božský	božský	k2eAgMnSc1d1	božský
hodinář	hodinář	k1gMnSc1	hodinář
Architekt	architekt	k1gMnSc1	architekt
vesmíru	vesmír	k1gInSc2	vesmír
Bůh	bůh	k1gMnSc1	bůh
přírody	příroda	k1gFnSc2	příroda
Otec	otec	k1gMnSc1	otec
světel	světlo	k1gNnPc2	světlo
Trvalejší	trvalý	k2eAgInSc1d2	trvalejší
význam	význam	k1gInSc1	význam
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
deismu	deismus	k1gInSc2	deismus
následující	následující	k2eAgMnSc1d1	následující
zastánci	zastánce	k1gMnPc1	zastánce
a	a	k8xC	a
představitelé	představitel	k1gMnPc1	představitel
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
jejich	jejich	k3xOp3gMnSc4	jejich
uvedená	uvedený	k2eAgNnPc1d1	uvedené
díla	dílo	k1gNnPc1	dílo
<g/>
:	:	kIx,	:
Herbert	Herbert	k1gInSc1	Herbert
z	z	k7c2	z
<g />
.	.	kIx.	.
</s>
<s>
Cherbury	Cherbura	k1gFnPc1	Cherbura
(	(	kIx(	(
<g/>
1581	[number]	k4	1581
<g/>
-	-	kIx~	-
<g/>
1648	[number]	k4	1648
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
O	o	k7c6	o
pravdě	pravda	k1gFnSc6	pravda
<g/>
"	"	kIx"	"
1624	[number]	k4	1624
Thomas	Thomas	k1gMnSc1	Thomas
Hobbes	Hobbes	k1gMnSc1	Hobbes
(	(	kIx(	(
<g/>
1588	[number]	k4	1588
<g/>
-	-	kIx~	-
<g/>
1679	[number]	k4	1679
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Leviathan	Leviathan	k1gMnSc1	Leviathan
<g/>
"	"	kIx"	"
1651	[number]	k4	1651
John	John	k1gMnSc1	John
Locke	Locke	k1gFnSc4	Locke
(	(	kIx(	(
<g/>
1632	[number]	k4	1632
<g/>
-	-	kIx~	-
<g/>
1704	[number]	k4	1704
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Esej	esej	k1gFnSc1	esej
o	o	k7c6	o
lidském	lidský	k2eAgInSc6d1	lidský
rozumu	rozum	k1gInSc6	rozum
1690	[number]	k4	1690
Matthew	Matthew	k1gMnSc1	Matthew
Tindall	Tindall	k1gMnSc1	Tindall
(	(	kIx(	(
<g/>
1657	[number]	k4	1657
<g/>
-	-	kIx~	-
<g/>
1733	[number]	k4	1733
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
staré	starý	k2eAgNnSc1d1	staré
jako	jako	k8xS	jako
Stvoření	stvoření	k1gNnSc1	stvoření
<g/>
"	"	kIx"	"
1730	[number]	k4	1730
John	John	k1gMnSc1	John
Toland	Toland	k1gInSc1	Toland
(	(	kIx(	(
<g/>
1670	[number]	k4	1670
<g/>
-	-	kIx~	-
<g/>
1722	[number]	k4	1722
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
bez	bez	k7c2	bez
tajemství	tajemství	k1gNnSc2	tajemství
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
1696	[number]	k4	1696
Anthony	Anthona	k1gFnSc2	Anthona
Ashley	Ashlea	k1gFnSc2	Ashlea
Cooper	Cooper	k1gMnSc1	Cooper
<g/>
,	,	kIx,	,
Earl	earl	k1gMnSc1	earl
z	z	k7c2	z
Shaftesbury	Shaftesbura	k1gFnSc2	Shaftesbura
(	(	kIx(	(
<g/>
1671	[number]	k4	1671
<g/>
-	-	kIx~	-
<g/>
1713	[number]	k4	1713
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Zkoumání	zkoumání	k1gNnPc4	zkoumání
ohledně	ohledně	k7c2	ohledně
ctnosti	ctnost	k1gFnSc2	ctnost
či	či	k8xC	či
zásluhy	zásluha	k1gFnSc2	zásluha
<g/>
"	"	kIx"	"
1699	[number]	k4	1699
Lord	lord	k1gMnSc1	lord
Bolingbroke	Bolingbroke	k1gFnSc4	Bolingbroke
(	(	kIx(	(
<g/>
1678	[number]	k4	1678
<g/>
-	-	kIx~	-
<g/>
1751	[number]	k4	1751
<g/>
)	)	kIx)	)
David	David	k1gMnSc1	David
Hume	Hum	k1gMnSc2	Hum
(	(	kIx(	(
<g/>
1711	[number]	k4	1711
<g/>
-	-	kIx~	-
<g/>
1776	[number]	k4	1776
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Pojednání	pojednání	k1gNnSc2	pojednání
o	o	k7c6	o
lidské	lidský	k2eAgFnSc6d1	lidská
přirozenosti	přirozenost	k1gFnSc6	přirozenost
<g/>
"	"	kIx"	"
1739	[number]	k4	1739
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
Přirozené	přirozený	k2eAgFnPc1d1	přirozená
dějiny	dějiny	k1gFnPc1	dějiny
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
"	"	kIx"	"
1757	[number]	k4	1757
Benjamin	Benjamin	k1gMnSc1	Benjamin
Franklin	Franklin	k1gInSc1	Franklin
(	(	kIx(	(
<g/>
1706	[number]	k4	1706
<g/>
-	-	kIx~	-
<g/>
1790	[number]	k4	1790
<g/>
)	)	kIx)	)
Thomas	Thomas	k1gMnSc1	Thomas
Paine	Pain	k1gInSc5	Pain
(	(	kIx(	(
<g/>
1736	[number]	k4	1736
<g/>
-	-	kIx~	-
<g/>
1809	[number]	k4	1809
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Věk	věk	k1gInSc1	věk
rozumu	rozum	k1gInSc2	rozum
<g/>
"	"	kIx"	"
1793-94	[number]	k4	1793-94
Thomas	Thomas	k1gMnSc1	Thomas
Jefferson	Jefferson	k1gMnSc1	Jefferson
(	(	kIx(	(
<g/>
1743	[number]	k4	1743
<g/>
-	-	kIx~	-
<g/>
1826	[number]	k4	1826
<g/>
)	)	kIx)	)
James	James	k1gMnSc1	James
Madison	Madison	k1gMnSc1	Madison
(	(	kIx(	(
<g/>
1751	[number]	k4	1751
<g/>
-	-	kIx~	-
<g/>
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
</s>
