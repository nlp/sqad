<p>
<s>
Historie	historie	k1gFnSc1	historie
Svorné	svorný	k2eAgFnSc2d1	svorná
sedmy	sedma	k1gFnSc2	sedma
je	být	k5eAaImIp3nS	být
kniha	kniha	k1gFnSc1	kniha
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
napsal	napsat	k5eAaBmAgMnS	napsat
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Foglar	Foglar	k1gMnSc1	Foglar
<g/>
.	.	kIx.	.
</s>
<s>
Pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c4	o
o	o	k7c6	o
kamarádství	kamarádství	k1gNnSc6	kamarádství
party	party	k1gFnSc4	party
sedmi	sedm	k4xCc2	sedm
dětí	dítě	k1gFnPc2	dítě
z	z	k7c2	z
domu	dům	k1gInSc2	dům
U	u	k7c2	u
Grošáka	grošák	k1gMnSc2	grošák
<g/>
,	,	kIx,	,
o	o	k7c6	o
jejich	jejich	k3xOp3gNnSc6	jejich
poznávání	poznávání	k1gNnSc6	poznávání
přírody	příroda	k1gFnSc2	příroda
i	i	k8xC	i
skautských	skautský	k2eAgFnPc2d1	skautská
dovedností	dovednost	k1gFnPc2	dovednost
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
skautských	skautský	k2eAgInPc2d1	skautský
oddílů	oddíl	k1gInPc2	oddíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
Foglarových	Foglarův	k2eAgFnPc2d1	Foglarova
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
hlavními	hlavní	k2eAgFnPc7d1	hlavní
postavami	postava	k1gFnPc7	postava
vesměs	vesměs	k6eAd1	vesměs
jen	jen	k9	jen
chlapci	chlapec	k1gMnPc1	chlapec
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
v	v	k7c6	v
rovnocenné	rovnocenný	k2eAgFnSc6d1	rovnocenná
míře	míra	k1gFnSc6	míra
hoši	hoch	k1gMnPc1	hoch
i	i	k8xC	i
děvčata	děvče	k1gNnPc1	děvče
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Junák	junák	k1gMnSc1	junák
</s>
</p>
<p>
<s>
Literatura	literatura	k1gFnSc1	literatura
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
</s>
</p>
