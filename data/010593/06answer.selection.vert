<s>
Korunka	korunka	k1gFnSc1	korunka
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
část	část	k1gFnSc1	část
zubu	zub	k1gInSc2	zub
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyčnívá	vyčnívat	k5eAaImIp3nS	vyčnívat
ze	z	k7c2	z
zubního	zubní	k2eAgNnSc2d1	zubní
lůžka	lůžko	k1gNnSc2	lůžko
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pokrytá	pokrytý	k2eAgFnSc1d1	pokrytá
sklovinou	sklovina	k1gFnSc7	sklovina
<g/>
,	,	kIx,	,
nejtvrdší	tvrdý	k2eAgFnSc7d3	nejtvrdší
látkou	látka	k1gFnSc7	látka
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
