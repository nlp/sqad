<s>
Divokej	Divokej	k?	Divokej
Bill	Bill	k1gMnSc1	Bill
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
z	z	k7c2	z
Úval	úval	k1gInSc4	úval
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
tzv.	tzv.	kA	tzv.
folk	folk	k1gInSc1	folk
rock	rock	k1gInSc1	rock
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Koncertuje	koncertovat	k5eAaImIp3nS	koncertovat
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
také	také	k9	také
turné	turné	k1gNnSc4	turné
se	s	k7c7	s
skupinami	skupina	k1gFnPc7	skupina
Čechomor	Čechomora	k1gFnPc2	Čechomora
(	(	kIx(	(
<g/>
10	[number]	k4	10
:	:	kIx,	:
20	[number]	k4	20
Connection	Connection	k1gInSc1	Connection
Tour	Tour	k1gInSc4	Tour
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tři	tři	k4xCgFnPc1	tři
sestry	sestra	k1gFnPc1	sestra
(	(	kIx(	(
<g/>
Turné	turné	k1gNnSc1	turné
pro	pro	k7c4	pro
radost	radost	k1gFnSc4	radost
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
odzpívala	odzpívat	k5eAaPmAgFnS	odzpívat
turné	turné	k1gNnSc4	turné
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
The	The	k1gFnSc2	The
Levellers	Levellersa	k1gFnPc2	Levellersa
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
CD	CD	kA	CD
má	mít	k5eAaImIp3nS	mít
název	název	k1gInSc4	název
15	[number]	k4	15
<g/>
.	.	kIx.	.
</s>
<s>
Úvalská	Úvalský	k2eAgFnSc1d1	Úvalská
kapela	kapela	k1gFnSc1	kapela
Divokej	Divokej	k?	Divokej
Bill	Bill	k1gMnSc1	Bill
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dvou	dva	k4xCgNnPc2	dva
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
ustalovala	ustalovat	k5eAaImAgFnS	ustalovat
sestava	sestava	k1gFnSc1	sestava
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
začala	začít	k5eAaPmAgFnS	začít
koncertovat	koncertovat	k5eAaImF	koncertovat
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
hrála	hrát	k5eAaImAgFnS	hrát
i	i	k9	i
na	na	k7c6	na
prvních	první	k4xOgInPc6	první
ročnících	ročník	k1gInPc6	ročník
Rock	rock	k1gInSc1	rock
for	forum	k1gNnPc2	forum
People	People	k1gFnSc2	People
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
navázala	navázat	k5eAaPmAgFnS	navázat
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
Ameba	Ameba	k1gMnSc1	Ameba
Production	Production	k1gInSc1	Production
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
o	o	k7c4	o
kapelu	kapela	k1gFnSc4	kapela
stará	starat	k5eAaImIp3nS	starat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
kapela	kapela	k1gFnSc1	kapela
přibrala	přibrat	k5eAaPmAgFnS	přibrat
do	do	k7c2	do
sestavy	sestava	k1gFnSc2	sestava
banjo	banjo	k1gNnSc1	banjo
jeho	jeho	k3xOp3gInSc1	jeho
zvuk	zvuk	k1gInSc1	zvuk
se	se	k3xPyFc4	se
definitivně	definitivně	k6eAd1	definitivně
ujasnil	ujasnit	k5eAaPmAgMnS	ujasnit
a	a	k8xC	a
kapela	kapela	k1gFnSc1	kapela
nahrála	nahrát	k5eAaBmAgFnS	nahrát
demosnímek	demosnímek	k1gInSc4	demosnímek
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgMnSc7	který
se	se	k3xPyFc4	se
přihlásila	přihlásit	k5eAaPmAgFnS	přihlásit
do	do	k7c2	do
několika	několik	k4yIc2	několik
vyhledávacích	vyhledávací	k2eAgFnPc2d1	vyhledávací
soutěží	soutěž	k1gFnPc2	soutěž
(	(	kIx(	(
<g/>
Rock	rock	k1gInSc1	rock
Nymburk	Nymburk	k1gInSc1	Nymburk
<g/>
,	,	kIx,	,
Jim	on	k3xPp3gFnPc3	on
Beam	Beam	k1gFnPc3	Beam
rock	rock	k1gInSc4	rock
<g/>
,	,	kIx,	,
Broumovská	broumovský	k2eAgFnSc1d1	Broumovská
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
Rock	rock	k1gInSc1	rock
fest	fest	k6eAd1	fest
Dobříš	dobřit	k5eAaImIp2nS	dobřit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
většinu	většina	k1gFnSc4	většina
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgMnSc4	ten
skupina	skupina	k1gFnSc1	skupina
podepsala	podepsat	k5eAaPmAgFnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
firmou	firma	k1gFnSc7	firma
Monitor	monitor	k1gInSc1	monitor
EMI	EMI	kA	EMI
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
desky	deska	k1gFnPc4	deska
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
vychází	vycházet	k5eAaImIp3nS	vycházet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Propustka	propustka	k1gFnSc1	propustka
do	do	k7c2	do
pekel	peklo	k1gNnPc2	peklo
<g/>
.	.	kIx.	.
</s>
<s>
Začali	začít	k5eAaPmAgMnP	začít
jezdit	jezdit	k5eAaImF	jezdit
po	po	k7c6	po
klubech	klub	k1gInPc6	klub
jako	jako	k8xS	jako
předskokani	předskokan	k1gMnPc1	předskokan
Nahoru	nahoru	k6eAd1	nahoru
po	po	k7c6	po
schodišti	schodiště	k1gNnSc6	schodiště
dolů	dol	k1gInPc2	dol
band	banda	k1gFnPc2	banda
a	a	k8xC	a
spřízněných	spřízněný	k2eAgFnPc2d1	spřízněná
Třech	tři	k4xCgFnPc2	tři
sester	sestra	k1gFnPc2	sestra
<g/>
,	,	kIx,	,
dostali	dostat	k5eAaPmAgMnP	dostat
se	se	k3xPyFc4	se
tak	tak	k9	tak
do	do	k7c2	do
povědomí	povědomí	k1gNnSc2	povědomí
klubového	klubový	k2eAgNnSc2d1	klubové
publika	publikum	k1gNnSc2	publikum
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
deska	deska	k1gFnSc1	deska
s	s	k7c7	s
názvem	název	k1gInSc7	název
Svatá	svatý	k2eAgFnSc1d1	svatá
pravda	pravda	k1gFnSc1	pravda
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
jim	on	k3xPp3gMnPc3	on
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
přinesla	přinést	k5eAaPmAgFnS	přinést
možnost	možnost	k1gFnSc4	možnost
jet	jet	k5eAaImF	jet
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Žlutým	žlutý	k2eAgMnSc7d1	žlutý
psem	pes	k1gMnSc7	pes
velkou	velký	k2eAgFnSc7d1	velká
Staropramen	staropramen	k1gInSc4	staropramen
Tour	Toura	k1gFnPc2	Toura
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
třetí	třetí	k4xOgFnSc6	třetí
desce	deska	k1gFnSc6	deska
Mezi	mezi	k7c7	mezi
nima	nima	k?	nima
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
kapela	kapela	k1gFnSc1	kapela
poprvé	poprvé	k6eAd1	poprvé
použila	použít	k5eAaPmAgFnS	použít
syntezátory	syntezátor	k1gInPc4	syntezátor
<g/>
.	.	kIx.	.
</s>
<s>
Pilotní	pilotní	k2eAgInSc1d1	pilotní
singl	singl	k1gInSc1	singl
Znamení	znamení	k1gNnSc2	znamení
otevřel	otevřít	k5eAaPmAgInS	otevřít
kapele	kapela	k1gFnSc3	kapela
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
rádií	rádio	k1gNnPc2	rádio
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
získala	získat	k5eAaPmAgFnS	získat
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
desku	deska	k1gFnSc4	deska
za	za	k7c4	za
deset	deset	k4xCc4	deset
tisíc	tisíc	k4xCgInPc2	tisíc
prodaných	prodaný	k2eAgInPc2d1	prodaný
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
vydat	vydat	k5eAaPmF	vydat
úspěšné	úspěšný	k2eAgNnSc1d1	úspěšné
live	live	k1gNnSc1	live
album	album	k1gNnSc1	album
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
DVD	DVD	kA	DVD
nazvané	nazvaný	k2eAgNnSc1d1	nazvané
podle	podle	k7c2	podle
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
nahráno	nahrát	k5eAaPmNgNnS	nahrát
-	-	kIx~	-
Lucerna	lucerna	k1gFnSc1	lucerna
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
"	"	kIx"	"
<g/>
Venca	Venca	k1gMnSc1	Venca
<g/>
"	"	kIx"	"
Bláha	Bláha	k1gMnSc1	Bláha
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
Miloš	Miloš	k1gMnSc1	Miloš
"	"	kIx"	"
<g/>
Jurda	Jurda	k1gMnSc1	Jurda
<g/>
"	"	kIx"	"
Jurač	Jurač	k1gMnSc1	Jurač
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Štěpán	Štěpán	k1gMnSc1	Štěpán
Karbulka	Karbulka	k1gFnSc1	Karbulka
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
megafon	megafon	k1gInSc1	megafon
<g/>
,	,	kIx,	,
tanec	tanec	k1gInSc1	tanec
Adam	Adam	k1gMnSc1	Adam
Karlík	Karlík	k1gMnSc1	Karlík
-	-	kIx~	-
bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
smyčcového	smyčcový	k2eAgInSc2d1	smyčcový
orchestru	orchestr	k1gInSc2	orchestr
Vox	Vox	k1gFnSc1	Vox
Bohemicalis	Bohemicalis	k1gFnSc1	Bohemicalis
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
Divokej	Divokej	k?	Divokej
<g />
.	.	kIx.	.
</s>
<s>
Bill	Bill	k1gMnSc1	Bill
<g/>
,	,	kIx,	,
housle	housle	k1gFnPc4	housle
Roman	Romana	k1gFnPc2	Romana
"	"	kIx"	"
<g/>
Prochajda	Prochajda	k1gMnSc1	Prochajda
<g/>
"	"	kIx"	"
Procházka	Procházka	k1gMnSc1	Procházka
-	-	kIx~	-
akustická	akustický	k2eAgFnSc1d1	akustická
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
zpěv	zpěv	k1gInSc1	zpěv
Honza	Honza	k1gMnSc1	Honza
"	"	kIx"	"
<g/>
Jack	Jack	k1gMnSc1	Jack
<g/>
"	"	kIx"	"
Bártl	Bártl	k1gMnSc1	Bártl
-	-	kIx~	-
banjo	banjo	k1gNnSc1	banjo
Martin	Martin	k1gMnSc1	Martin
"	"	kIx"	"
<g/>
Pecan	Pecan	k1gMnSc1	Pecan
<g/>
"	"	kIx"	"
Pecka	Pecka	k1gMnSc1	Pecka
-	-	kIx~	-
akordeon	akordeon	k1gInSc1	akordeon
Marek	Marek	k1gMnSc1	Marek
"	"	kIx"	"
<g/>
Mára	Mára	k1gMnSc1	Mára
<g/>
"	"	kIx"	"
Žežulka	Žežulka	k1gMnSc1	Žežulka
-	-	kIx~	-
bicí	bicí	k2eAgMnSc1d1	bicí
Ondřej	Ondřej	k1gMnSc1	Ondřej
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
-	-	kIx~	-
banjo	banjo	k1gNnSc1	banjo
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Ota	Ota	k1gMnSc1	Ota
Smrkovský	Smrkovský	k2eAgMnSc1d1	Smrkovský
-	-	kIx~	-
bicí	bicí	k2eAgMnSc1d1	bicí
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
-	-	kIx~	-
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Honza	Honza	k1gMnSc1	Honza
"	"	kIx"	"
<g/>
Kiska	Kiska	k1gMnSc1	Kiska
<g/>
"	"	kIx"	"
Veselý	Veselý	k1gMnSc1	Veselý
-	-	kIx~	-
bicí	bicí	k2eAgMnSc1d1	bicí
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Lukáš	Lukáš	k1gMnSc1	Lukáš
Toman	Toman	k1gMnSc1	Toman
-	-	kIx~	-
didgeridoo	didgeridoo	k1gMnSc1	didgeridoo
(	(	kIx(	(
<g/>
†	†	k?	†
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Plakala	plakat	k5eAaImAgFnS	plakat
Dávno	dávno	k1gNnSc4	dávno
<g />
.	.	kIx.	.
</s>
<s>
Pocit	pocit	k1gInSc1	pocit
Rozárka	Rozárka	k1gFnSc1	Rozárka
Miláčku	miláček	k1gMnSc3	miláček
Lásko	láska	k1gFnSc5	láska
Svatá	svatý	k2eAgFnSc1d1	svatá
Pravda	pravda	k9	pravda
Šibenice	šibenice	k1gFnSc1	šibenice
Alkohol	alkohol	k1gInSc1	alkohol
Divokej	Divokej	k?	Divokej
Bill	Bill	k1gMnSc1	Bill
Síť	síť	k1gFnSc4	síť
Malování	malování	k1gNnSc2	malování
Dolsin	Dolsina	k1gFnPc2	Dolsina
Tsunami	tsunami	k1gNnSc2	tsunami
Osm	osm	k4xCc1	osm
statečných	statečný	k2eAgMnPc2d1	statečný
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Propustka	propustka	k1gFnSc1	propustka
do	do	k7c2	do
pekel	peklo	k1gNnPc2	peklo
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Svatá	svatý	k2eAgFnSc1d1	svatá
pravda	pravda	k1gFnSc1	pravda
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Mezi	mezi	k7c7	mezi
nima	nima	k?	nima
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Divokej	Divokej	k?	Divokej
Bill	Bill	k1gMnSc1	Bill
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Mlsná	mlsný	k2eAgFnSc1d1	mlsná
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Unisono	unisono	k6eAd1	unisono
best	best	k1gInSc1	best
of	of	k?	of
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
15	[number]	k4	15
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Lucerna	lucerna	k1gFnSc1	lucerna
live	live	k1gFnSc1	live
(	(	kIx(	(
<g/>
CD	CD	kA	CD
<g/>
+	+	kIx~	+
<g/>
DVD	DVD	kA	DVD
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Rock	rock	k1gInSc1	rock
for	forum	k1gNnPc2	forum
People	People	k1gFnSc2	People
(	(	kIx(	(
<g/>
CD	CD	kA	CD
<g/>
+	+	kIx~	+
<g/>
DVD	DVD	kA	DVD
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
G2	G2	k1gFnSc6	G2
Acoustic	Acoustice	k1gFnPc2	Acoustice
Stage	Stag	k1gFnSc2	Stag
(	(	kIx(	(
<g/>
CD	CD	kA	CD
<g/>
+	+	kIx~	+
<g/>
DVD	DVD	kA	DVD
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Platinum	Platinum	k1gInSc1	Platinum
Collection	Collection	k1gInSc1	Collection
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
CD	CD	kA	CD
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgInSc1d1	český
slavík	slavík	k1gInSc1	slavík
2006	[number]	k4	2006
s	s	k7c7	s
33	[number]	k4	33
377	[number]	k4	377
body	bod	k1gInPc7	bod
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Divokej	Divokej	k?	Divokej
Bill	Bill	k1gMnSc1	Bill
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Texty	text	k1gInPc1	text
písní	píseň	k1gFnPc2	píseň
Oficiální	oficiální	k2eAgInSc1d1	oficiální
facebook	facebook	k1gInSc1	facebook
stránky	stránka	k1gFnSc2	stránka
Divokej	Divokej	k?	Divokej
Bill	Bill	k1gMnSc1	Bill
<g/>
,	,	kIx,	,
o	o	k7c6	o
skupině	skupina	k1gFnSc6	skupina
</s>
