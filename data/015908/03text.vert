<s>
Gustav	Gustav	k1gMnSc1
Kroupa	Kroupa	k1gMnSc1
</s>
<s>
Gustav	Gustav	k1gMnSc1
Kroupa	Kroupa	k1gMnSc1
Narození	narození	k1gNnPc2
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1857	#num#	k4
<g/>
Mutějovice	Mutějovice	k1gFnSc2
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1935	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
77	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Vídeň	Vídeň	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
inženýr	inženýr	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Gustav	Gustav	k1gMnSc1
Kroupa	Kroupa	k1gMnSc1
(	(	kIx(
<g/>
30	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1857	#num#	k4
v	v	k7c6
Mutějovicích	Mutějovice	k1gFnPc6
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1935	#num#	k4
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
česko-rakouský	česko-rakouský	k2eAgMnSc1d1
důlní	důlní	k2eAgMnSc1d1
inženýr	inženýr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1886	#num#	k4
byl	být	k5eAaImAgMnS
činný	činný	k2eAgMnSc1d1
jako	jako	k9
inženýr	inženýr	k1gMnSc1
a	a	k8xC
správce	správce	k1gMnSc1
uranových	uranový	k2eAgInPc2d1
dolů	dol	k1gInPc2
v	v	k7c6
Jáchymově	Jáchymov	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
od	od	k7c2
roku	rok	k1gInSc2
1898	#num#	k4
ředitelem	ředitel	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Činnost	činnost	k1gFnSc1
</s>
<s>
Kroupa	Kroupa	k1gMnSc1
nechal	nechat	k5eAaPmAgMnS
v	v	k7c6
Bezdružicích	Bezdružice	k1gFnPc6
nainstalovat	nainstalovat	k5eAaPmF
a	a	k8xC
soustředit	soustředit	k5eAaPmF
vyklápění	vyklápění	k1gNnSc4
odpadních	odpadní	k2eAgInPc2d1
zbytků	zbytek	k1gInPc2
po	po	k7c6
louhování	louhování	k1gNnSc6
rudy	ruda	k1gFnSc2
při	při	k7c6
zpracování	zpracování	k1gNnSc6
smolince	smolinec	k1gInSc2
(	(	kIx(
<g/>
uraninitu	uraninit	k1gInSc2
<g/>
)	)	kIx)
při	při	k7c6
výrobě	výroba	k1gFnSc6
uranu	uran	k1gInSc2
a	a	k8xC
nechal	nechat	k5eAaPmAgMnS
je	být	k5eAaImIp3nS
převážet	převážet	k5eAaImF
k	k	k7c3
získávání	získávání	k1gNnSc3
stříbra	stříbro	k1gNnSc2
do	do	k7c2
příbramských	příbramský	k2eAgInPc2d1
dolů	dol	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
také	také	k9
podpořil	podpořit	k5eAaPmAgInS
vědeckou	vědecký	k2eAgFnSc4d1
práci	práce	k1gFnSc4
Marie	Maria	k1gFnSc2
Curie-Sklodovské	Curie-Sklodovský	k2eAgFnSc2d1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
pro	pro	k7c4
svůj	svůj	k3xOyFgInSc4
výzkum	výzkum	k1gInSc4
potřebovala	potřebovat	k5eAaImAgFnS
značné	značný	k2eAgNnSc4d1
množství	množství	k1gNnSc4
smolince	smolinec	k1gInSc2
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jí	jíst	k5eAaImIp3nS
a	a	k8xC
jejímu	její	k3xOp3gMnSc3
manželovi	manžel	k1gMnSc3
přenechával	přenechávat	k5eAaImAgInS
odpadní	odpadní	k2eAgInSc1d1
materiál	materiál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přitom	přitom	k6eAd1
z	z	k7c2
dodaných	dodaný	k2eAgInPc2d1
10	#num#	k4
tun	tuna	k1gFnPc2
smolincového	smolincový	k2eAgInSc2d1
odpadu	odpad	k1gInSc2
Marie	Maria	k1gFnSc2
a	a	k8xC
Pierre	Pierr	k1gInSc5
Curieovi	Curieův	k2eAgMnPc1d1
izolovali	izolovat	k5eAaBmAgMnP
3	#num#	k4
gramy	gram	k1gInPc4
radioaktivních	radioaktivní	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
těchto	tento	k3xDgInPc6
výzkumech	výzkum	k1gInPc6
byly	být	k5eAaImAgInP
objeveny	objevit	k5eAaPmNgInP
nové	nový	k2eAgInPc1d1
radioaktivní	radioaktivní	k2eAgInPc1d1
prvky	prvek	k1gInPc1
radium	radium	k1gNnSc1
a	a	k8xC
polonium	polonium	k1gNnSc1
jako	jako	k8xC,k8xS
vedlejší	vedlejší	k2eAgInPc1d1
produkty	produkt	k1gInPc1
při	při	k7c6
zpracování	zpracování	k1gNnSc6
uranu	uran	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
1903	#num#	k4
až	až	k9
1914	#num#	k4
Kroupa	Kroupa	k1gMnSc1
působil	působit	k5eAaImAgMnS
jako	jako	k9
redaktor	redaktor	k1gMnSc1
rakouského	rakouský	k2eAgInSc2d1
časopisu	časopis	k1gInSc2
o	o	k7c6
hornictví	hornictví	k1gNnSc6
a	a	k8xC
hutnictví	hutnictví	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1915	#num#	k4
až	až	k9
1919	#num#	k4
byl	být	k5eAaImAgMnS
šéfredaktorem	šéfredaktor	k1gMnSc7
časopisu	časopis	k1gInSc2
„	„	k?
<g/>
Bergbau	Bergbaus	k1gInSc2
und	und	k?
Hütte	Hütt	k1gInSc5
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Gustav	Gustav	k1gMnSc1
Kroupa	Kroupa	k1gMnSc1
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
1017835055	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0003	#num#	k4
5980	#num#	k4
8867	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
220641135	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Těžba	těžba	k1gFnSc1
</s>
