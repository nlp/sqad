<s>
Klávesová	klávesový	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
</s>
<s>
Menu	menu	k1gNnSc1
aplikace	aplikace	k1gFnSc2
Mozilla	Mozillo	k1gNnSc2
Firefox	Firefox	k1gInSc4
3.0	3.0	k4
se	se	k3xPyFc4
zeleně	zeleně	k6eAd1
vyznačenými	vyznačený	k2eAgFnPc7d1
klávesovými	klávesový	k2eAgFnPc7d1
zkratkami	zkratka	k1gFnPc7
a	a	k8xC
žlutě	žlutě	k6eAd1
zvýrazněnými	zvýrazněný	k2eAgNnPc7d1
písmeny	písmeno	k1gNnPc7
<g/>
,	,	kIx,
kterými	který	k3yQgFnPc7,k3yIgFnPc7,k3yRgFnPc7
lze	lze	k6eAd1
vybrat	vybrat	k5eAaPmF
příslušnou	příslušný	k2eAgFnSc4d1
položku	položka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Ukázka	ukázka	k1gFnSc1
dvou	dva	k4xCgNnPc2
menu	menu	k1gNnPc2
v	v	k7c6
Mac	Mac	kA
OS	OS	kA
Finderu	Findera	k1gFnSc4
s	s	k7c7
klávesovými	klávesový	k2eAgFnPc7d1
zkratkami	zkratka	k1gFnPc7
uvedenými	uvedený	k2eAgFnPc7d1
v	v	k7c6
pravém	pravý	k2eAgInSc6d1
sloupci	sloupec	k1gInSc6
jednotlivých	jednotlivý	k2eAgFnPc2d1
položek	položka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Klávesová	klávesový	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
keyboard	keyboard	k1gMnSc1
shortcut	shortcut	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
kombinace	kombinace	k1gFnSc1
několika	několik	k4yIc2
kláves	klávesa	k1gFnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc1
současné	současný	k2eAgNnSc1d1
stisknutí	stisknutí	k1gNnSc1
vyvolá	vyvolat	k5eAaPmIp3nS
nějakou	nějaký	k3yIgFnSc4
funkci	funkce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Klávesová	klávesový	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
dvou	dva	k4xCgFnPc2
až	až	k6eAd1
tří	tři	k4xCgFnPc2
kláves	klávesa	k1gFnPc2
(	(	kIx(
<g/>
větší	veliký	k2eAgInSc1d2
počet	počet	k1gInSc1
ovšem	ovšem	k9
není	být	k5eNaImIp3nS
vyloučen	vyloučit	k5eAaPmNgMnS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
typicky	typicky	k6eAd1
poslední	poslední	k2eAgNnSc1d1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
plnovýznamová	plnovýznamový	k2eAgFnSc1d1
klávesa	klávesa	k1gFnSc1
(	(	kIx(
=	=	kIx~
jiná	jiný	k2eAgFnSc1d1
než	než	k8xS
předřazovací	předřazovací	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgFnPc1d1
klávesy	klávesa	k1gFnPc1
jsou	být	k5eAaImIp3nP
tzv.	tzv.	kA
klávesy	klávesa	k1gFnSc2
předřazovací	předřazovací	k2eAgFnSc2d1
(	(	kIx(
<g/>
Shift	Shift	kA
<g/>
,	,	kIx,
Ctrl	Ctrl	kA
<g/>
,	,	kIx,
Alt	Alt	kA
a	a	k8xC
Alt	Alt	kA
Gr	Gr	k1gFnSc2
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
též	též	k9
klávesa	klávesa	k1gFnSc1
s	s	k7c7
logem	log	k1gInSc7
(	(	kIx(
<g/>
např.	např.	kA
Windows	Windows	kA
<g/>
,	,	kIx,
Macintosh	Macintosh	kA
<g/>
)	)	kIx)
nebo	nebo	k8xC
klávesa	klávesa	k1gFnSc1
Fn	Fn	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pořadí	pořadí	k1gNnSc1
stisku	stisk	k1gInSc2
předřazovacích	předřazovací	k2eAgFnPc2d1
kláves	klávesa	k1gFnPc2
bývá	bývat	k5eAaImIp3nS
libovolné	libovolný	k2eAgNnSc1d1
<g/>
,	,	kIx,
plnovýznamová	plnovýznamový	k2eAgFnSc1d1
klávesa	klávesa	k1gFnSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
stisknuta	stisknout	k5eAaPmNgFnS
jako	jako	k9
poslední	poslední	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Klávesová	klávesový	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
se	se	k3xPyFc4
zapisuje	zapisovat	k5eAaImIp3nS
jako	jako	k9
sekvence	sekvence	k1gFnSc1
tisknutých	tisknutý	k2eAgFnPc2d1
kláves	klávesa	k1gFnPc2
<g/>
,	,	kIx,
většinou	většina	k1gFnSc7
oddělených	oddělený	k2eAgFnPc6d1
mezerou	mezera	k1gFnSc7
nebo	nebo	k8xC
znakem	znak	k1gInSc7
plus	plus	k1gInSc1
(	(	kIx(
<g/>
+	+	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ojediněle	ojediněle	k6eAd1
i	i	k9
jiným	jiný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
(	(	kIx(
<g/>
např.	např.	kA
znakem	znak	k1gInSc7
minus	minus	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgFnPc1d1
klávesové	klávesový	k2eAgFnPc1d1
zkratky	zkratka	k1gFnPc1
jsou	být	k5eAaImIp3nP
vždy	vždy	k6eAd1
silně	silně	k6eAd1
závislé	závislý	k2eAgNnSc1d1
na	na	k7c6
použitém	použitý	k2eAgInSc6d1
hardware	hardware	k1gInSc1
a	a	k8xC
software	software	k1gInSc1
<g/>
,	,	kIx,
autoři	autor	k1gMnPc1
programového	programový	k2eAgNnSc2d1
vybavení	vybavení	k1gNnSc2
se	se	k3xPyFc4
však	však	k9
obvykle	obvykle	k6eAd1
snaží	snažit	k5eAaImIp3nP
dodržovat	dodržovat	k5eAaImF
určité	určitý	k2eAgInPc4d1
zažité	zažitý	k2eAgInPc4d1
standardy	standard	k1gInPc4
pro	pro	k7c4
nejobvyklejší	obvyklý	k2eAgFnPc4d3
funkce	funkce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Typy	typ	k1gInPc1
klávesových	klávesový	k2eAgFnPc2d1
zkratek	zkratka	k1gFnPc2
</s>
<s>
Restartovací	Restartovací	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
kbd	kbd	k?
<g/>
.	.	kIx.
<g/>
Sablona__Klavesa	Sablona__Klavesa	k1gFnSc1
<g/>
{	{	kIx(
<g/>
background-color	background-color	k1gInSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
F	F	kA
<g/>
7	#num#	k4
<g/>
F	F	kA
<g/>
7	#num#	k4
<g/>
F	F	kA
<g/>
7	#num#	k4
<g/>
;	;	kIx,
<g/>
background-image	background-imag	k1gInSc2
<g/>
:	:	kIx,
<g/>
linear-gradient	linear-gradient	k1gMnSc1
<g/>
(	(	kIx(
<g/>
rgba	rgba	k1gMnSc1
<g/>
(	(	kIx(
<g/>
255,255,255	255,255,255	k4
<g/>
,	,	kIx,
<g/>
.4	.4	k4
<g/>
)	)	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
<g/>
rgba	rgba	k1gFnSc1
<g/>
(	(	kIx(
<g/>
0,0	0,0	k4
<g/>
,0	,0	k4
<g/>
,	,	kIx,
<g/>
.1	.1	k4
<g/>
))	))	k?
<g/>
;	;	kIx,
<g/>
border	border	k1gInSc1
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
px	px	k?
solid	solid	k1gInSc1
<g/>
;	;	kIx,
<g/>
border-color	border-color	k1gInSc1
<g/>
:	:	kIx,
<g/>
#	#	kIx~
<g/>
DDD	DDD	kA
#	#	kIx~
<g/>
a	a	k8xC
<g/>
2	#num#	k4
<g/>
a	a	k8xC
<g/>
9	#num#	k4
<g/>
b	b	k?
<g/>
1	#num#	k4
#	#	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
888	#num#	k4
#	#	kIx~
<g/>
CCC	CCC	kA
<g/>
;	;	kIx,
<g/>
border-radius	border-radius	k1gMnSc1
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
px	px	k?
<g/>
;	;	kIx,
<g/>
padding	padding	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
.4	.4	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
text-shadow	text-shadow	k?
<g/>
:	:	kIx,
<g/>
0	#num#	k4
1	#num#	k4
<g/>
px	px	k?
rgba	rgba	k1gMnSc1
<g/>
(	(	kIx(
<g/>
255,255,255	255,255,255	k4
<g/>
,	,	kIx,
<g/>
.5	.5	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
<g/>
white-space	white-space	k1gFnSc1
<g/>
:	:	kIx,
<g/>
nowrap	nowrap	k1gMnSc1
<g/>
}	}	kIx)
<g/>
Ctrl	Ctrl	kA
+	+	kIx~
Alt	Alt	kA
+	+	kIx~
Del	Del	k1gMnSc1
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc1d1
klávesová	klávesový	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
definovaná	definovaný	k2eAgFnSc1d1
už	už	k6eAd1
v	v	k7c6
BIOSu	BIOSus	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
stisk	stisk	k1gInSc1
způsobí	způsobit	k5eAaPmIp3nS
restart	restart	k1gInSc4
počítače	počítač	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operační	operační	k2eAgInSc1d1
systém	systém	k1gInSc1
Windows	Windows	kA
pod	pod	k7c4
tuto	tento	k3xDgFnSc4
zkratku	zkratka	k1gFnSc4
umístil	umístit	k5eAaPmAgMnS
Správce	správce	k1gMnSc1
úloh	úloha	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
ale	ale	k9
také	také	k9
obsahuje	obsahovat	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
restartu	restart	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Formátovací	formátovací	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
</s>
<s>
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
k	k	k7c3
formátování	formátování	k1gNnSc3
textu	text	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
základní	základní	k2eAgInPc4d1
a	a	k8xC
široce	široko	k6eAd1
podporované	podporovaný	k2eAgNnSc1d1
patří	patřit	k5eAaImIp3nS
<g/>
:	:	kIx,
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
B	B	kA
–	–	k?
tučný	tučný	k2eAgInSc4d1
text	text	k1gInSc4
(	(	kIx(
<g/>
bold	bold	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
I	i	k9
–	–	k?
kurzíva	kurzíva	k1gFnSc1
(	(	kIx(
<g/>
italic	italice	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
U	u	k7c2
–	–	k?
podtržení	podtržení	k1gNnSc2
(	(	kIx(
<g/>
underline	underlin	k1gInSc5
<g/>
)	)	kIx)
</s>
<s>
Zkratka	zkratka	k1gFnSc1
pro	pro	k7c4
práci	práce	k1gFnSc4
se	s	k7c7
schránkou	schránka	k1gFnSc7
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS
uživateli	uživatel	k1gMnSc3
pracovat	pracovat	k5eAaImF
se	se	k3xPyFc4
schránkou	schránka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
aplikací	aplikace	k1gFnPc2
používá	používat	k5eAaImIp3nS
pro	pro	k7c4
práci	práce	k1gFnSc4
se	s	k7c7
schránkou	schránka	k1gFnSc7
tyto	tento	k3xDgFnPc4
klávesové	klávesový	k2eAgFnPc4d1
zkratky	zkratka	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
C	C	kA
–	–	k?
kopírovat	kopírovat	k5eAaImF
data	datum	k1gNnPc4
do	do	k7c2
schránky	schránka	k1gFnSc2
(	(	kIx(
<g/>
copy	cop	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
X	X	kA
–	–	k?
vyjmout	vyjmout	k5eAaPmF
data	datum	k1gNnPc4
do	do	k7c2
schránky	schránka	k1gFnSc2
(	(	kIx(
<g/>
cut	cut	k?
<g/>
)	)	kIx)
</s>
<s>
Ctrl	Ctrl	kA
+	+	kIx~
V	v	k7c6
–	–	k?
vložit	vložit	k5eAaPmF
data	datum	k1gNnPc4
ze	z	k7c2
schránky	schránka	k1gFnSc2
do	do	k7c2
aplikace	aplikace	k1gFnSc2
(	(	kIx(
<g/>
paste	pást	k5eAaImRp2nP
<g/>
)	)	kIx)
</s>
<s>
Zkratka	zkratka	k1gFnSc1
zvláštního	zvláštní	k2eAgInSc2d1
znaku	znak	k1gInSc2
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS
napsat	napsat	k5eAaPmF,k5eAaBmF
znak	znak	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
není	být	k5eNaImIp3nS
na	na	k7c4
klávesnici	klávesnice	k1gFnSc4
přímo	přímo	k6eAd1
dostupný	dostupný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takovýchto	takovýto	k3xDgFnPc2
zkratek	zkratka	k1gFnPc2
je	být	k5eAaImIp3nS
veliké	veliký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejpoužívanější	používaný	k2eAgFnPc4d3
patří	patřit	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
pro	pro	k7c4
zavináč	zavináč	k1gInSc4
(	(	kIx(
<g/>
@	@	kIx~
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Alt	alt	k1gInSc1
Gr	Gr	k1gFnSc2
+	+	kIx~
V	V	kA
<g/>
,	,	kIx,
resp.	resp.	kA
Ctrl	Ctrl	kA
+	+	kIx~
Alt	Alt	kA
+	+	kIx~
V	V	kA
(	(	kIx(
<g/>
u	u	k7c2
rozložení	rozložení	k1gNnSc2
kláves	klávesa	k1gFnPc2
CS	CS	kA
a	a	k8xC
SK	Sk	kA
QWERTZ	QWERTZ	kA
<g/>
,	,	kIx,
SK	Sk	kA
QWERTY	QWERTY	kA
<g/>
)	)	kIx)
nebo	nebo	k8xC
Alt	Alt	kA
Gr	Gr	k1gFnSc1
+	+	kIx~
ě	ě	k?
(	(	kIx(
<g/>
CS	CS	kA
QWERTY	QWERTY	kA
<g/>
)	)	kIx)
nebo	nebo	k8xC
Alt	Alt	kA
+	+	kIx~
64	#num#	k4
(	(	kIx(
<g/>
na	na	k7c6
numerické	numerický	k2eAgFnSc6d1
klávesnici	klávesnice	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Funkční	funkční	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
</s>
<s>
Vyvolá	vyvolat	k5eAaPmIp3nS
nějakou	nějaký	k3yIgFnSc4
funkci	funkce	k1gFnSc4
aplikace	aplikace	k1gFnSc2
nebo	nebo	k8xC
operačního	operační	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
závislá	závislý	k2eAgFnSc1d1
na	na	k7c6
použité	použitý	k2eAgFnSc6d1
aplikaci	aplikace	k1gFnSc6
a	a	k8xC
operačním	operační	k2eAgInSc6d1
systému	systém	k1gInSc6
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
i	i	k9
na	na	k7c6
verzi	verze	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
má	mít	k5eAaImIp3nS
uživatel	uživatel	k1gMnSc1
možnost	možnost	k1gFnSc4
si	se	k3xPyFc3
zkratku	zkratka	k1gFnSc4
nastavit	nastavit	k5eAaPmF
<g/>
,	,	kIx,
změnit	změnit	k5eAaPmF
nebo	nebo	k8xC
zrušit	zrušit	k5eAaPmF
podle	podle	k7c2
svého	svůj	k3xOyFgNnSc2
přání	přání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Alt	Alt	kA
kódování	kódování	k1gNnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Klávesová	klávesový	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1
heslo	heslo	k1gNnSc4
klávesová	klávesový	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Jan	Jan	k1gMnSc1
Zaškolný	Zaškolný	k2eAgMnSc1d1
<g/>
:	:	kIx,
Klávesové	klávesový	k2eAgFnPc1d1
zkratky	zkratka	k1gFnPc1
</s>
<s>
Klávesové	klávesový	k2eAgFnPc1d1
zkratky	zkratka	k1gFnPc1
ve	v	k7c6
Windows	Windows	kA
<g/>
,	,	kIx,
oficiální	oficiální	k2eAgFnSc1d1
webu	web	k1gInSc3
Microsoftu	Microsoft	k2eAgFnSc4d1
</s>
<s>
Přehled	přehled	k1gInSc1
klávesových	klávesový	k2eAgFnPc2d1
zkratek	zkratka	k1gFnPc2
pro	pro	k7c4
Windows	Windows	kA
a	a	k8xC
Mac	Mac	kA
</s>
<s>
Kompletní	kompletní	k2eAgInSc1d1
souhrn	souhrn	k1gInSc1
klávesových	klávesový	k2eAgFnPc2d1
zkratek	zkratka	k1gFnPc2
pro	pro	k7c4
systémy	systém	k1gInPc4
Office	Office	kA
<g/>
,	,	kIx,
Windows	Windows	kA
a	a	k8xC
macOS	macOS	k?
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4488108-3	4488108-3	k4
</s>
