<p>
<s>
V	v	k7c6	v
geometrii	geometrie	k1gFnSc6	geometrie
je	být	k5eAaImIp3nS	být
čtverec	čtverec	k1gInSc1	čtverec
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
čtyřúhelník	čtyřúhelník	k1gInSc1	čtyřúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
rovinný	rovinný	k2eAgInSc1d1	rovinný
útvar	útvar	k1gInSc4	útvar
ohraničený	ohraničený	k2eAgInSc4d1	ohraničený
čtyřmi	čtyři	k4xCgFnPc7	čtyři
shodnými	shodný	k2eAgFnPc7d1	shodná
úsečkami	úsečka	k1gFnPc7	úsečka
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
všechny	všechen	k3xTgInPc1	všechen
vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
úhly	úhel	k1gInPc1	úhel
jsou	být	k5eAaImIp3nP	být
shodné	shodný	k2eAgInPc1d1	shodný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přeneseně	přeneseně	k6eAd1	přeneseně
má	mít	k5eAaImIp3nS	mít
čtverec	čtverec	k1gInSc1	čtverec
v	v	k7c6	v
algebře	algebra	k1gFnSc6	algebra
význam	význam	k1gInSc4	význam
druhé	druhý	k4xOgFnSc2	druhý
mocniny	mocnina	k1gFnSc2	mocnina
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obsah	obsah	k1gInSc1	obsah
čtverce	čtverec	k1gInSc2	čtverec
je	být	k5eAaImIp3nS	být
roven	roven	k2eAgInSc1d1	roven
druhé	druhý	k4xOgFnSc6	druhý
mocnině	mocnina	k1gFnSc6	mocnina
délky	délka	k1gFnSc2	délka
jeho	jeho	k3xOp3gFnSc2	jeho
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
čtverec	čtverec	k1gInSc4	čtverec
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
chápeme	chápat	k5eAaImIp1nP	chápat
jako	jako	k9	jako
druhá	druhý	k4xOgFnSc1	druhý
mocnina	mocnina	k1gFnSc1	mocnina
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Čtverec	čtverec	k1gInSc1	čtverec
je	být	k5eAaImIp3nS	být
rovnoběžník	rovnoběžník	k1gInSc4	rovnoběžník
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
ho	on	k3xPp3gMnSc4	on
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
případ	případ	k1gInSc4	případ
obdélníku	obdélník	k1gInSc2	obdélník
nebo	nebo	k8xC	nebo
kosočtverce	kosočtverec	k1gInSc2	kosočtverec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protilehlé	protilehlý	k2eAgFnPc1d1	protilehlá
strany	strana	k1gFnPc1	strana
jsou	být	k5eAaImIp3nP	být
rovnoběžné	rovnoběžný	k2eAgFnPc1d1	rovnoběžná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
strany	strana	k1gFnPc1	strana
jsou	být	k5eAaImIp3nP	být
shodné	shodný	k2eAgFnPc1d1	shodná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgInPc1	všechen
vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
úhly	úhel	k1gInPc1	úhel
jsou	být	k5eAaImIp3nP	být
pravé	pravý	k2eAgInPc1d1	pravý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Úhlopříčky	úhlopříčka	k1gFnPc1	úhlopříčka
čtverce	čtverec	k1gInPc1	čtverec
jsou	být	k5eAaImIp3nP	být
shodné	shodný	k2eAgInPc1d1	shodný
a	a	k8xC	a
navzájem	navzájem	k6eAd1	navzájem
kolmé	kolmý	k2eAgFnPc1d1	kolmá
<g/>
,	,	kIx,	,
půlí	půlit	k5eAaImIp3nP	půlit
jeho	jeho	k3xOp3gInPc1	jeho
úhly	úhel	k1gInPc1	úhel
i	i	k9	i
sebe	sebe	k3xPyFc4	sebe
navzájem	navzájem	k6eAd1	navzájem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čtverci	čtverec	k1gInSc3	čtverec
lze	lze	k6eAd1	lze
jakožto	jakožto	k8xS	jakožto
pravidelnému	pravidelný	k2eAgInSc3d1	pravidelný
mnohoúhelníku	mnohoúhelník	k1gInSc3	mnohoúhelník
opsat	opsat	k5eAaPmF	opsat
i	i	k9	i
vepsat	vepsat	k5eAaPmF	vepsat
kružnici	kružnice	k1gFnSc4	kružnice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zároveň	zároveň	k6eAd1	zároveň
tětivový	tětivový	k2eAgInSc1d1	tětivový
čtyřúhelník	čtyřúhelník	k1gInSc1	čtyřúhelník
i	i	k8xC	i
tečnový	tečnový	k2eAgInSc1d1	tečnový
čtyřúhelník	čtyřúhelník	k1gInSc1	čtyřúhelník
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tedy	tedy	k9	tedy
dvojstředový	dvojstředový	k2eAgInSc1d1	dvojstředový
čtyřúhelník	čtyřúhelník	k1gInSc1	čtyřúhelník
a	a	k8xC	a
středy	střed	k1gInPc1	střed
kružnice	kružnice	k1gFnSc2	kružnice
opsané	opsaný	k2eAgInPc1d1	opsaný
i	i	k8xC	i
vepsané	vepsaný	k2eAgInPc1d1	vepsaný
splývají	splývat	k5eAaImIp3nP	splývat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čtverec	čtverec	k1gInSc1	čtverec
má	mít	k5eAaImIp3nS	mít
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
obdélníků	obdélník	k1gInPc2	obdélník
s	s	k7c7	s
daným	daný	k2eAgInSc7d1	daný
obvodem	obvod	k1gInSc7	obvod
největší	veliký	k2eAgInSc4d3	veliký
obsah	obsah	k1gInSc4	obsah
a	a	k8xC	a
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
obdélníků	obdélník	k1gInPc2	obdélník
s	s	k7c7	s
daným	daný	k2eAgInSc7d1	daný
obsahem	obsah	k1gInSc7	obsah
nejmenší	malý	k2eAgInSc4d3	nejmenší
obvod	obvod	k1gInSc4	obvod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Euklidovskou	euklidovský	k2eAgFnSc4d1	euklidovská
rovinu	rovina	k1gFnSc4	rovina
lze	lze	k6eAd1	lze
definovat	definovat	k5eAaBmF	definovat
jako	jako	k9	jako
dvojrozměrný	dvojrozměrný	k2eAgInSc4d1	dvojrozměrný
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
existuje	existovat	k5eAaImIp3nS	existovat
čtverec	čtverec	k1gInSc1	čtverec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzorce	vzorec	k1gInPc1	vzorec
==	==	k?	==
</s>
</p>
<p>
<s>
Pomocí	pomocí	k7c2	pomocí
délky	délka	k1gFnSc2	délka
strany	strana	k1gFnSc2	strana
čtverce	čtverec	k1gInSc2	čtverec
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
</s>
</p>
<p>
<s>
obvod	obvod	k1gInSc1	obvod
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
O	O	kA	O
<g/>
=	=	kIx~	=
<g/>
4	[number]	k4	4
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
obsah	obsah	k1gInSc1	obsah
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
S	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
</p>
<p>
<s>
délka	délka	k1gFnSc1	délka
úhlopříčky	úhlopříčka	k1gFnSc2	úhlopříčka
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
u	u	k7c2	u
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
u	u	k7c2	u
<g/>
=	=	kIx~	=
<g/>
a	a	k8xC	a
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
<s>
poloměr	poloměr	k1gInSc1	poloměr
kružnice	kružnice	k1gFnSc2	kružnice
opsané	opsaný	k2eAgFnSc2d1	opsaná
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
u	u	k7c2	u
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
u	u	k7c2	u
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
<s>
poloměr	poloměr	k1gInSc1	poloměr
kružnice	kružnice	k1gFnSc2	kružnice
vepsané	vepsaný	k2eAgFnSc2d1	vepsaná
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r_	r_	k?	r_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Geometrický	geometrický	k2eAgInSc1d1	geometrický
útvar	útvar	k1gInSc1	útvar
</s>
</p>
<p>
<s>
Rovnoběžník	rovnoběžník	k1gInSc1	rovnoběžník
</s>
</p>
<p>
<s>
Čtyřúhelník	čtyřúhelník	k1gInSc1	čtyřúhelník
</s>
</p>
<p>
<s>
Obdélník	obdélník	k1gInSc1	obdélník
</s>
</p>
<p>
<s>
Kružnice	kružnice	k1gFnSc1	kružnice
opsaná	opsaný	k2eAgFnSc1d1	opsaná
</s>
</p>
<p>
<s>
Kružnice	kružnice	k1gFnSc1	kružnice
vepsaná	vepsaný	k2eAgFnSc1d1	vepsaná
</s>
</p>
<p>
<s>
Krychle	krychle	k1gFnSc1	krychle
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
čtverec	čtverec	k1gInSc1	čtverec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
čtverec	čtverec	k1gInSc1	čtverec
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
