<s>
Socha	socha	k1gFnSc1	socha
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Statue	statue	k1gFnSc1	statue
of	of	k?	of
Liberty	Libert	k1gInPc7	Libert
<g/>
,	,	kIx,	,
plným	plný	k2eAgInSc7d1	plný
názvem	název	k1gInSc7	název
Liberty	Libert	k1gInPc1	Libert
Enlightening	Enlightening	k1gInSc1	Enlightening
the	the	k?	the
World	World	k1gInSc1	World
(	(	kIx(	(
<g/>
z	z	k7c2	z
francouzského	francouzský	k2eAgNnSc2d1	francouzské
La	la	k1gNnSc2	la
Liberté	Liberta	k1gMnPc1	Liberta
éclairant	éclairant	k1gMnSc1	éclairant
le	le	k?	le
monde	mond	k1gInSc5	mond
čili	čili	k8xC	čili
"	"	kIx"	"
<g/>
Svoboda	svoboda	k1gFnSc1	svoboda
přinášející	přinášející	k2eAgNnSc1d1	přinášející
světlo	světlo	k1gNnSc1	světlo
světu	svět	k1gInSc3	svět
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
socha	socha	k1gFnSc1	socha
stojící	stojící	k2eAgFnSc1d1	stojící
na	na	k7c6	na
Ostrově	ostrov	k1gInSc6	ostrov
svobody	svoboda	k1gFnSc2	svoboda
u	u	k7c2	u
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
