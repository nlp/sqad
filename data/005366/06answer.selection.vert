<s>
Krokodýl	krokodýl	k1gMnSc1	krokodýl
mořský	mořský	k2eAgMnSc1d1	mořský
(	(	kIx(	(
<g/>
Crocodylus	Crocodylus	k1gMnSc1	Crocodylus
porosus	porosus	k1gMnSc1	porosus
Schneider	Schneider	k1gMnSc1	Schneider
<g/>
,	,	kIx,	,
1821	[number]	k4	1821
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
také	také	k9	také
jako	jako	k8xC	jako
krokodýl	krokodýl	k1gMnSc1	krokodýl
pobřežní	pobřežní	k1gMnSc1	pobřežní
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
z	z	k7c2	z
dnes	dnes	k6eAd1	dnes
žijících	žijící	k2eAgMnPc2d1	žijící
krokodýlů	krokodýl	k1gMnPc2	krokodýl
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
největším	veliký	k2eAgInSc7d3	veliký
a	a	k8xC	a
nejmohutnějším	mohutný	k2eAgInSc7d3	nejmohutnější
plazem	plaz	k1gInSc7	plaz
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
