<s>
Krokodýl	krokodýl	k1gMnSc1	krokodýl
mořský	mořský	k2eAgMnSc1d1	mořský
(	(	kIx(	(
<g/>
Crocodylus	Crocodylus	k1gMnSc1	Crocodylus
porosus	porosus	k1gMnSc1	porosus
Schneider	Schneider	k1gMnSc1	Schneider
<g/>
,	,	kIx,	,
1821	[number]	k4	1821
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
také	také	k9	také
jako	jako	k8xC	jako
krokodýl	krokodýl	k1gMnSc1	krokodýl
pobřežní	pobřežní	k1gMnSc1	pobřežní
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
z	z	k7c2	z
dnes	dnes	k6eAd1	dnes
žijících	žijící	k2eAgMnPc2d1	žijící
krokodýlů	krokodýl	k1gMnPc2	krokodýl
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
největším	veliký	k2eAgInSc7d3	veliký
a	a	k8xC	a
nejmohutnějším	mohutný	k2eAgInSc7d3	nejmohutnější
plazem	plaz	k1gInSc7	plaz
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
délky	délka	k1gFnPc4	délka
přes	přes	k7c4	přes
šest	šest	k4xCc4	šest
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc2	hmotnost
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jedné	jeden	k4xCgFnSc2	jeden
tuny	tuna	k1gFnSc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
řádně	řádně	k6eAd1	řádně
změřený	změřený	k2eAgMnSc1d1	změřený
jedinec	jedinec	k1gMnSc1	jedinec
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
délky	délka	k1gFnSc2	délka
7	[number]	k4	7
metru	metr	k1gInSc2	metr
a	a	k8xC	a
hmotnosti	hmotnost	k1gFnSc2	hmotnost
asi	asi	k9	asi
1,2	[number]	k4	1,2
tuny	tuna	k1gFnSc2	tuna
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Obývá	obývat	k5eAaImIp3nS	obývat
jižní	jižní	k2eAgFnSc3d1	jižní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc3d1	jihovýchodní
Asii	Asie	k1gFnSc3	Asie
včetně	včetně	k7c2	včetně
Indonésie	Indonésie	k1gFnSc2	Indonésie
a	a	k8xC	a
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
až	až	k9	až
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
severní	severní	k2eAgFnSc2d1	severní
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Libuje	libovat	k5eAaImIp3nS	libovat
si	se	k3xPyFc3	se
v	v	k7c6	v
mořích	moře	k1gNnPc6	moře
s	s	k7c7	s
teplejší	teplý	k2eAgFnSc7d2	teplejší
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
nejraději	rád	k6eAd3	rád
ve	v	k7c6	v
vodách	voda	k1gFnPc6	voda
Indického	indický	k2eAgInSc2d1	indický
a	a	k8xC	a
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
mezi	mezi	k7c7	mezi
Indií	Indie	k1gFnSc7	Indie
a	a	k8xC	a
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
exempláře	exemplář	k1gInPc1	exemplář
byly	být	k5eAaImAgInP	být
pozorovány	pozorován	k2eAgInPc1d1	pozorován
na	na	k7c6	na
širém	širý	k2eAgNnSc6d1	širé
moři	moře	k1gNnSc6	moře
až	až	k9	až
tisíc	tisíc	k4xCgInPc2	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
břehů	břeh	k1gInPc2	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
zadních	zadní	k2eAgFnPc6d1	zadní
končetinách	končetina	k1gFnPc6	končetina
mezi	mezi	k7c7	mezi
prsty	prst	k1gInPc7	prst
plovací	plovací	k2eAgFnSc2d1	plovací
blány	blána	k1gFnSc2	blána
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
plavání	plavání	k1gNnSc3	plavání
nepoužívá	používat	k5eNaImIp3nS	používat
a	a	k8xC	a
plave	plavat	k5eAaImIp3nS	plavat
pomocí	pomocí	k7c2	pomocí
vlnění	vlnění	k1gNnSc2	vlnění
svalnatého	svalnatý	k2eAgInSc2d1	svalnatý
ocasu	ocas	k1gInSc2	ocas
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
výborný	výborný	k2eAgMnSc1d1	výborný
plavec	plavec	k1gMnSc1	plavec
<g/>
,	,	kIx,	,
udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
dokáže	dokázat	k5eAaPmIp3nS	dokázat
uplavat	uplavat	k5eAaPmF	uplavat
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
i	i	k9	i
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1000	[number]	k4	1000
km	km	kA	km
po	po	k7c6	po
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
nepatří	patřit	k5eNaImIp3nS	patřit
k	k	k7c3	k
vysloveně	vysloveně	k6eAd1	vysloveně
mořským	mořský	k2eAgMnPc3d1	mořský
živočichům	živočich	k1gMnPc3	živočich
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
krokodýl	krokodýl	k1gMnSc1	krokodýl
mořský	mořský	k2eAgMnSc1d1	mořský
vůbec	vůbec	k9	vůbec
není	být	k5eNaImIp3nS	být
vybíravý	vybíravý	k2eAgInSc1d1	vybíravý
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
zprvu	zprvu	k6eAd1	zprvu
živí	živit	k5eAaImIp3nS	živit
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
měkkýši	měkkýš	k1gMnPc1	měkkýš
a	a	k8xC	a
pulci	pulec	k1gMnPc1	pulec
<g/>
.	.	kIx.	.
</s>
<s>
Dospělci	dospělec	k1gMnPc1	dospělec
loví	lovit	k5eAaImIp3nP	lovit
zvířata	zvíře	k1gNnPc4	zvíře
od	od	k7c2	od
malých	malý	k2eAgInPc2d1	malý
krabů	krab	k1gInPc2	krab
a	a	k8xC	a
želv	želva	k1gFnPc2	želva
přes	přes	k7c4	přes
ryby	ryba	k1gFnPc4	ryba
a	a	k8xC	a
obojživelníky	obojživelník	k1gMnPc4	obojživelník
až	až	k9	až
po	po	k7c4	po
velké	velký	k2eAgMnPc4d1	velký
savce	savec	k1gMnPc4	savec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
kořist	kořist	k1gFnSc4	kořist
útočí	útočit	k5eAaImIp3nS	útočit
bleskurychle	bleskurychle	k6eAd1	bleskurychle
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
popadne	popadnout	k5eAaPmIp3nS	popadnout
ji	on	k3xPp3gFnSc4	on
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
jde	jít	k5eAaImIp3nS	jít
po	po	k7c6	po
krku	krk	k1gInSc6	krk
<g/>
)	)	kIx)	)
a	a	k8xC	a
stáhne	stáhnout	k5eAaPmIp3nS	stáhnout
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
utopí	utopit	k5eAaPmIp3nP	utopit
<g/>
.	.	kIx.	.
</s>
<s>
Krokodýli	krokodýl	k1gMnPc1	krokodýl
loví	lovit	k5eAaImIp3nP	lovit
kořist	kořist	k1gFnSc4	kořist
také	také	k9	také
zespoda	zespoda	k7c2	zespoda
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
pak	pak	k6eAd1	pak
útočí	útočit	k5eAaImIp3nS	útočit
na	na	k7c4	na
měkké	měkký	k2eAgNnSc4d1	měkké
břicho	břicho	k1gNnSc4	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
do	do	k7c2	do
kořisti	kořist	k1gFnSc2	kořist
zakousne	zakousnout	k5eAaPmIp3nS	zakousnout
svými	svůj	k3xOyFgFnPc7	svůj
velkými	velký	k2eAgFnPc7d1	velká
čelistmi	čelist	k1gFnPc7	čelist
a	a	k8xC	a
otáčením	otáčení	k1gNnSc7	otáčení
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
rve	rvát	k5eAaImIp3nS	rvát
velké	velký	k2eAgInPc4d1	velký
kusy	kus	k1gInPc4	kus
masa	maso	k1gNnSc2	maso
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
dravec	dravec	k1gMnSc1	dravec
<g/>
,	,	kIx,	,
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
dospělého	dospělý	k2eAgMnSc2d1	dospělý
jedince	jedinec	k1gMnSc2	jedinec
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
bývají	bývat	k5eAaImIp3nP	bývat
zranění	zranění	k1gNnPc4	zranění
smrtelná	smrtelný	k2eAgNnPc4d1	smrtelné
<g/>
,	,	kIx,	,
ročně	ročně	k6eAd1	ročně
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
zhruba	zhruba	k6eAd1	zhruba
300	[number]	k4	300
lidských	lidský	k2eAgInPc2d1	lidský
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
vyhynutí	vyhynutí	k1gNnSc2	vyhynutí
<g/>
.	.	kIx.	.
</s>
<s>
Lovci	lovec	k1gMnPc1	lovec
usilují	usilovat	k5eAaImIp3nP	usilovat
o	o	k7c4	o
opětovné	opětovný	k2eAgNnSc4d1	opětovné
povolení	povolení	k1gNnSc4	povolení
lovu	lov	k1gInSc2	lov
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vzácná	vzácný	k2eAgFnSc1d1	vzácná
kůže	kůže	k1gFnSc1	kůže
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
luxusních	luxusní	k2eAgFnPc2d1	luxusní
bot	bota	k1gFnPc2	bota
a	a	k8xC	a
kabelek	kabelka	k1gFnPc2	kabelka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mase	masa	k1gFnSc6	masa
je	být	k5eAaImIp3nS	být
poptávka	poptávka	k1gFnSc1	poptávka
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Dospělosti	dospělost	k1gFnPc1	dospělost
a	a	k8xC	a
schopnosti	schopnost	k1gFnPc1	schopnost
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
samice	samice	k1gFnPc1	samice
v	v	k7c6	v
10	[number]	k4	10
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
v	v	k7c6	v
16	[number]	k4	16
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
klade	klást	k5eAaImIp3nS	klást
25	[number]	k4	25
až	až	k9	až
60	[number]	k4	60
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zahrabává	zahrabávat	k5eAaImIp3nS	zahrabávat
asi	asi	k9	asi
půl	půl	k1xP	půl
metru	metr	k1gInSc2	metr
hluboko	hluboko	k6eAd1	hluboko
do	do	k7c2	do
bahnité	bahnitý	k2eAgFnSc2d1	bahnitá
půdy	půda	k1gFnSc2	půda
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
asi	asi	k9	asi
50	[number]	k4	50
až	až	k9	až
60	[number]	k4	60
metrů	metr	k1gInPc2	metr
od	od	k7c2	od
břehu	břeh	k1gInSc2	břeh
toku	tok	k1gInSc2	tok
nebo	nebo	k8xC	nebo
nádrže	nádrž	k1gFnSc2	nádrž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
snůšku	snůška	k1gFnSc4	snůška
navrší	navršit	k5eAaPmIp3nS	navršit
kupu	kupa	k1gFnSc4	kupa
listí	listí	k1gNnSc2	listí
<g/>
,	,	kIx,	,
větví	větev	k1gFnPc2	větev
a	a	k8xC	a
bahna	bahno	k1gNnSc2	bahno
vysokou	vysoký	k2eAgFnSc7d1	vysoká
téměř	téměř	k6eAd1	téměř
metr	metr	k1gInSc4	metr
a	a	k8xC	a
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
měřící	měřící	k2eAgNnSc1d1	měřící
7	[number]	k4	7
až	až	k9	až
9	[number]	k4	9
m.	m.	k?	m.
Toto	tento	k3xDgNnSc1	tento
obrovské	obrovský	k2eAgNnSc1d1	obrovské
hnízdo	hnízdo	k1gNnSc1	hnízdo
samice	samice	k1gFnSc2	samice
hlídá	hlídat	k5eAaImIp3nS	hlídat
až	až	k9	až
do	do	k7c2	do
vylíhnutí	vylíhnutí	k1gNnSc2	vylíhnutí
mláďat	mládě	k1gNnPc2	mládě
<g/>
,	,	kIx,	,
ukrytá	ukrytý	k2eAgFnSc1d1	ukrytá
v	v	k7c6	v
louži	louž	k1gFnSc6	louž
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
si	se	k3xPyFc3	se
vyhrabe	vyhrabat	k5eAaPmIp3nS	vyhrabat
ve	v	k7c6	v
vlhké	vlhký	k2eAgFnSc6d1	vlhká
půdě	půda	k1gFnSc6	půda
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
z	z	k7c2	z
vajec	vejce	k1gNnPc2	vejce
zahřívaných	zahřívaný	k2eAgInPc2d1	zahřívaný
tlejícím	tlející	k2eAgInSc7d1	tlející
materiálem	materiál	k1gInSc7	materiál
líhnou	líhnout	k5eAaImIp3nP	líhnout
obvykle	obvykle	k6eAd1	obvykle
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
měsících	měsíc	k1gInPc6	měsíc
(	(	kIx(	(
<g/>
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
hnízdě	hnízdo	k1gNnSc6	hnízdo
bývá	bývat	k5eAaImIp3nS	bývat
až	až	k9	až
32	[number]	k4	32
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
nepříznivých	příznivý	k2eNgFnPc6d1	nepříznivá
podmínkách	podmínka	k1gFnPc6	podmínka
až	až	k8xS	až
po	po	k7c6	po
pěti	pět	k4xCc6	pět
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
střeží	střežit	k5eAaImIp3nS	střežit
i	i	k9	i
vylíhlá	vylíhlý	k2eAgNnPc4d1	vylíhlé
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
měří	měřit	k5eAaImIp3nP	měřit
kolem	kolem	k7c2	kolem
25	[number]	k4	25
cm	cm	kA	cm
<g/>
,	,	kIx,	,
po	po	k7c4	po
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
