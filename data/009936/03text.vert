<p>
<s>
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1956	[number]	k4	1956
Vsetín	Vsetín	k1gInSc1	Vsetín
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
2002	[number]	k4	2002
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
druhý	druhý	k4xOgMnSc1	druhý
předseda	předseda	k1gMnSc1	předseda
Občanské	občanský	k2eAgFnSc2d1	občanská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
v	v	k7c6	v
období	období	k1gNnSc6	období
2006	[number]	k4	2006
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
také	také	k9	také
předsedou	předseda	k1gMnSc7	předseda
Rady	rada	k1gFnSc2	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
<g/>
Kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
ziskem	zisk	k1gInSc7	zisk
221	[number]	k4	221
689	[number]	k4	689
hlasů	hlas	k1gInPc2	hlas
(	(	kIx(	(
<g/>
4,30	[number]	k4	4,30
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
Vojenské	vojenský	k2eAgNnSc4d1	vojenské
gymnázium	gymnázium	k1gNnSc4	gymnázium
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
z	z	k7c2	z
Trocnova	Trocnov	k1gInSc2	Trocnov
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
a	a	k8xC	a
poté	poté	k6eAd1	poté
strojní	strojní	k2eAgFnSc4d1	strojní
fakultu	fakulta	k1gFnSc4	fakulta
VUT	VUT	kA	VUT
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
informací	informace	k1gFnPc2	informace
jeho	jeho	k3xOp3gMnPc2	jeho
učitelů	učitel	k1gMnPc2	učitel
otištěných	otištěný	k2eAgMnPc2d1	otištěný
časopisem	časopis	k1gInSc7	časopis
Týden	týden	k1gInSc1	týden
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
středoškolských	středoškolský	k2eAgNnPc2d1	středoškolské
studií	studio	k1gNnPc2	studio
aktivním	aktivní	k2eAgMnSc7d1	aktivní
členem	člen	k1gMnSc7	člen
komunistické	komunistický	k2eAgFnSc2d1	komunistická
organizace	organizace	k1gFnSc2	organizace
Socialistický	socialistický	k2eAgInSc1d1	socialistický
svaz	svaz	k1gInSc1	svaz
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
projektant	projektant	k1gMnSc1	projektant
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
jako	jako	k9	jako
samostatný	samostatný	k2eAgMnSc1d1	samostatný
projektant	projektant	k1gMnSc1	projektant
v	v	k7c4	v
OKR	okr	k1gInSc4	okr
,	,	kIx,	,
závodu	závod	k1gInSc3	závod
Automatizace	automatizace	k1gFnSc2	automatizace
a	a	k8xC	a
mechanizace	mechanizace	k1gFnSc2	mechanizace
k.ú.o.	k.ú.o.	k?	k.ú.o.
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
Výstavní	výstavní	k2eAgInSc1d1	výstavní
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1987	[number]	k4	1987
<g/>
–	–	k?	–
<g/>
1991	[number]	k4	1991
pak	pak	k6eAd1	pak
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Energoprojektu	energoprojekt	k1gInSc6	energoprojekt
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
závod	závod	k1gInSc1	závod
Ostrava	Ostrava	k1gFnSc1	Ostrava
jako	jako	k8xS	jako
vedoucí	vedoucí	k1gMnSc1	vedoucí
projektant	projektant	k1gMnSc1	projektant
specialista	specialista	k1gMnSc1	specialista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
spoluzaložil	spoluzaložit	k5eAaPmAgInS	spoluzaložit
a	a	k8xC	a
řídil	řídit	k5eAaImAgInS	řídit
obchodní	obchodní	k2eAgFnSc4d1	obchodní
společnost	společnost	k1gFnSc4	společnost
VAE	VAE	kA	VAE
<g/>
,	,	kIx,	,
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
s	s	k7c7	s
r.	r.	kA	r.
o.	o.	k?	o.
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
výkonný	výkonný	k2eAgMnSc1d1	výkonný
ředitel	ředitel	k1gMnSc1	ředitel
a	a	k8xC	a
generální	generální	k2eAgMnSc1d1	generální
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
energetickými	energetický	k2eAgInPc7d1	energetický
projekty	projekt	k1gInPc7	projekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
společnost	společnost	k1gFnSc1	společnost
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
bez	bez	k7c2	bez
likvidace	likvidace	k1gFnSc2	likvidace
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
právním	právní	k2eAgMnSc7d1	právní
nástupcem	nástupce	k1gMnSc7	nástupce
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
obchodní	obchodní	k2eAgFnSc1d1	obchodní
společnost	společnost	k1gFnSc1	společnost
VAE	VAE	kA	VAE
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
zastával	zastávat	k5eAaImAgMnS	zastávat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
funkci	funkce	k1gFnSc6	funkce
předsedy	předseda	k1gMnSc2	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politická	politický	k2eAgFnSc1d1	politická
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Před	před	k7c7	před
listopadem	listopad	k1gInSc7	listopad
1989	[number]	k4	1989
nebyl	být	k5eNaImAgInS	být
politicky	politicky	k6eAd1	politicky
organizován	organizován	k2eAgMnSc1d1	organizován
v	v	k7c6	v
žádné	žádný	k3yNgFnSc6	žádný
straně	strana	k1gFnSc6	strana
<g/>
;	;	kIx,	;
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
podprůměrné	podprůměrný	k2eAgFnSc6d1	podprůměrná
finanční	finanční	k2eAgFnSc3d1	finanční
situaci	situace	k1gFnSc3	situace
nabídku	nabídka	k1gFnSc4	nabídka
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
jak	jak	k8xC	jak
uvádí	uvádět	k5eAaImIp3nS	uvádět
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
byla	být	k5eAaImAgFnS	být
proti	proti	k7c3	proti
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
nepřijmout	přijmout	k5eNaPmF	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Občanském	občanský	k2eAgNnSc6d1	občanské
fóru	fórum	k1gNnSc6	fórum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Občanské	občanský	k2eAgFnSc2d1	občanská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
senátu	senát	k1gInSc2	senát
za	za	k7c4	za
obvod	obvod	k1gInSc4	obvod
č.	č.	k?	č.
70	[number]	k4	70
<g/>
,	,	kIx,	,
Ostrava-město	Ostravaěsta	k1gFnSc5	Ostrava-města
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1998	[number]	k4	1998
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
senátního	senátní	k2eAgInSc2d1	senátní
klubu	klub	k1gInSc2	klub
ODS	ODS	kA	ODS
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
místopředsedou	místopředseda	k1gMnSc7	místopředseda
Senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2002	[number]	k4	2002
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
po	po	k7c6	po
Václavu	Václav	k1gMnSc6	Václav
Klausovi	Klaus	k1gMnSc6	Klaus
předsedou	předseda	k1gMnSc7	předseda
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
Topolánkova	Topolánkův	k2eAgNnSc2d1	Topolánkovo
předsednictví	předsednictví	k1gNnSc2	předsednictví
ODS	ODS	kA	ODS
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
tato	tento	k3xDgNnPc4	tento
strana	strana	k1gFnSc1	strana
volby	volba	k1gFnSc2	volba
senátní	senátní	k2eAgFnSc2d1	senátní
<g/>
,	,	kIx,	,
krajské	krajský	k2eAgFnSc2d1	krajská
<g/>
,	,	kIx,	,
evropské	evropský	k2eAgFnSc2d1	Evropská
<g/>
,	,	kIx,	,
komunální	komunální	k2eAgFnSc2d1	komunální
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
i	i	k9	i
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
již	již	k6eAd1	již
jako	jako	k8xC	jako
strana	strana	k1gFnSc1	strana
vládní	vládní	k2eAgFnSc2d1	vládní
ODS	ODS	kA	ODS
drtivě	drtivě	k6eAd1	drtivě
prohrála	prohrát	k5eAaPmAgFnS	prohrát
krajské	krajský	k2eAgFnPc4d1	krajská
a	a	k8xC	a
senátní	senátní	k2eAgFnPc4d1	senátní
volby	volba	k1gFnPc4	volba
a	a	k8xC	a
ztratila	ztratit	k5eAaPmAgFnS	ztratit
tak	tak	k9	tak
všechny	všechen	k3xTgMnPc4	všechen
své	svůj	k3xOyFgMnPc4	svůj
hejtmany	hejtman	k1gMnPc4	hejtman
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
i	i	k9	i
nadpoloviční	nadpoloviční	k2eAgFnSc4d1	nadpoloviční
většinu	většina	k1gFnSc4	většina
v	v	k7c6	v
senátu	senát	k1gInSc6	senát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
stranickou	stranický	k2eAgFnSc4d1	stranická
funkci	funkce	k1gFnSc4	funkce
však	však	k9	však
Topolánek	Topolánek	k1gInSc4	Topolánek
i	i	k8xC	i
po	po	k7c6	po
těchto	tento	k3xDgFnPc6	tento
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2004	[number]	k4	2004
a	a	k8xC	a
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
obhájil	obhájit	k5eAaPmAgMnS	obhájit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Volby	volba	k1gFnPc1	volba
2006	[number]	k4	2006
a	a	k8xC	a
sestavování	sestavování	k1gNnSc2	sestavování
vlády	vláda	k1gFnSc2	vláda
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
byl	být	k5eAaImAgMnS	být
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
volebním	volební	k2eAgMnSc7d1	volební
lídrem	lídr	k1gMnSc7	lídr
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
nejvíce	hodně	k6eAd3	hodně
preferenčních	preferenční	k2eAgInPc2d1	preferenční
hlasů	hlas	k1gInPc2	hlas
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
kandidátů	kandidát	k1gMnPc2	kandidát
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
vítěz	vítěz	k1gMnSc1	vítěz
voleb	volba	k1gFnPc2	volba
byl	být	k5eAaImAgMnS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
sestavením	sestavení	k1gNnSc7	sestavení
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
trojkoalici	trojkoalice	k1gFnSc6	trojkoalice
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
KDU-ČSL	KDU-ČSL	k1gFnSc7	KDU-ČSL
a	a	k8xC	a
Stranou	strana	k1gFnSc7	strana
zelených	zelená	k1gFnPc2	zelená
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
tato	tento	k3xDgFnSc1	tento
měla	mít	k5eAaImAgFnS	mít
dohromady	dohromady	k6eAd1	dohromady
pouze	pouze	k6eAd1	pouze
100	[number]	k4	100
hlasů	hlas	k1gInPc2	hlas
ze	z	k7c2	z
101	[number]	k4	101
hlasů	hlas	k1gInPc2	hlas
potřebných	potřebný	k2eAgInPc2d1	potřebný
pro	pro	k7c4	pro
vyslovení	vyslovení	k1gNnSc4	vyslovení
důvěry	důvěra	k1gFnSc2	důvěra
vládě	vláda	k1gFnSc3	vláda
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
přítomno	přítomno	k1gNnSc1	přítomno
všech	všecek	k3xTgMnPc2	všecek
200	[number]	k4	200
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
stačí	stačit	k5eAaBmIp3nS	stačit
většina	většina	k1gFnSc1	většina
přítomných	přítomný	k2eAgMnPc2d1	přítomný
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trojkoalice	Trojkoalice	k1gFnSc1	Trojkoalice
nepřesvědčila	přesvědčit	k5eNaPmAgFnS	přesvědčit
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
podpořila	podpořit	k5eAaPmAgFnS	podpořit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
Topolánek	Topolánek	k1gInSc1	Topolánek
přistoupil	přistoupit	k5eAaPmAgInS	přistoupit
na	na	k7c4	na
jednobarevnou	jednobarevný	k2eAgFnSc4d1	jednobarevná
vládu	vláda	k1gFnSc4	vláda
ODS	ODS	kA	ODS
tolerovanou	tolerovaný	k2eAgFnSc7d1	tolerovaná
ČSSD	ČSSD	kA	ČSSD
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
niž	jenž	k3xRgFnSc4	jenž
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
získat	získat	k5eAaPmF	získat
podporu	podpora	k1gFnSc4	podpora
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgFnSc1	první
Topolánkova	Topolánkův	k2eAgFnSc1d1	Topolánkova
vláda	vláda	k1gFnSc1	vláda
===	===	k?	===
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
jednání	jednání	k1gNnSc1	jednání
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Paroubkem	Paroubek	k1gMnSc7	Paroubek
zkrachovala	zkrachovat	k5eAaPmAgFnS	zkrachovat
<g/>
,	,	kIx,	,
předložil	předložit	k5eAaPmAgMnS	předložit
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
návrh	návrh	k1gInSc4	návrh
vlády	vláda	k1gFnSc2	vláda
složené	složený	k2eAgFnSc2d1	složená
z	z	k7c2	z
devíti	devět	k4xCc2	devět
členů	člen	k1gInPc2	člen
ODS	ODS	kA	ODS
a	a	k8xC	a
šesti	šest	k4xCc2	šest
nestraníků	nestraník	k1gMnPc2	nestraník
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
požádala	požádat	k5eAaPmAgFnS	požádat
o	o	k7c4	o
důvěru	důvěra	k1gFnSc4	důvěra
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
ji	on	k3xPp3gFnSc4	on
nezískala	získat	k5eNaPmAgFnS	získat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vládu	vláda	k1gFnSc4	vláda
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
96	[number]	k4	96
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
,	,	kIx,	,
99	[number]	k4	99
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
proti	proti	k7c3	proti
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
podala	podat	k5eAaPmAgFnS	podat
tato	tento	k3xDgFnSc1	tento
vláda	vláda	k1gFnSc1	vláda
demisi	demise	k1gFnSc4	demise
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Druhá	druhý	k4xOgFnSc1	druhý
Topolánkova	Topolánkův	k2eAgFnSc1d1	Topolánkova
vláda	vláda	k1gFnSc1	vláda
===	===	k?	===
</s>
</p>
<p>
<s>
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
prezident	prezident	k1gMnSc1	prezident
Klaus	Klaus	k1gMnSc1	Klaus
znovu	znovu	k6eAd1	znovu
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
Topolánka	Topolánek	k1gMnSc4	Topolánek
premiérem	premiér	k1gMnSc7	premiér
a	a	k8xC	a
pověřil	pověřit	k5eAaPmAgMnS	pověřit
ho	on	k3xPp3gInSc4	on
sestavením	sestavení	k1gNnSc7	sestavení
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
požádal	požádat	k5eAaPmAgMnS	požádat
premiér	premiér	k1gMnSc1	premiér
prezidenta	prezident	k1gMnSc2	prezident
o	o	k7c4	o
jmenování	jmenování	k1gNnSc4	jmenování
vlády	vláda	k1gFnSc2	vláda
složené	složený	k2eAgFnSc2d1	složená
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
a	a	k8xC	a
Strany	strana	k1gFnSc2	strana
zelených	zelená	k1gFnPc2	zelená
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
ji	on	k3xPp3gFnSc4	on
ale	ale	k9	ale
zprvu	zprvu	k6eAd1	zprvu
jmenovat	jmenovat	k5eAaBmF	jmenovat
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
očekával	očekávat	k5eAaImAgMnS	očekávat
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nebude	být	k5eNaImBp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
přeběhlících	přeběhlík	k1gMnPc6	přeběhlík
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
také	také	k9	také
výhrady	výhrada	k1gFnPc4	výhrada
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
personálnímu	personální	k2eAgNnSc3d1	personální
složení	složení	k1gNnSc3	složení
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
jmenována	jmenován	k2eAgFnSc1d1	jmenována
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2007	[number]	k4	2007
a	a	k8xC	a
o	o	k7c4	o
10	[number]	k4	10
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
získala	získat	k5eAaPmAgFnS	získat
důvěru	důvěra	k1gFnSc4	důvěra
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
poslanci	poslanec	k1gMnPc1	poslanec
ČSSD	ČSSD	kA	ČSSD
Miloš	Miloš	k1gMnSc1	Miloš
Melčák	Melčák	k1gMnSc1	Melčák
a	a	k8xC	a
Michal	Michal	k1gMnSc1	Michal
Pohanka	Pohanka	k1gMnSc1	Pohanka
opustili	opustit	k5eAaPmAgMnP	opustit
před	před	k7c7	před
hlasováním	hlasování	k1gNnSc7	hlasování
zasedací	zasedací	k2eAgInSc1d1	zasedací
sál	sál	k1gInSc1	sál
Sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
.	.	kIx.	.
</s>
<s>
Miloš	Miloš	k1gMnSc1	Miloš
Melčák	Melčák	k1gMnSc1	Melčák
a	a	k8xC	a
Michal	Michal	k1gMnSc1	Michal
Pohanka	Pohanka	k1gMnSc1	Pohanka
později	pozdě	k6eAd2	pozdě
uvedli	uvést	k5eAaPmAgMnP	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
tak	tak	k6eAd1	tak
učinili	učinit	k5eAaPmAgMnP	učinit
po	po	k7c6	po
poradě	porada	k1gFnSc6	porada
s	s	k7c7	s
Milošem	Miloš	k1gMnSc7	Miloš
Zemanem	Zeman	k1gMnSc7	Zeman
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vládu	vláda	k1gFnSc4	vláda
tedy	tedy	k9	tedy
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
všech	všecek	k3xTgMnPc2	všecek
100	[number]	k4	100
poslanců	poslanec	k1gMnPc2	poslanec
koalice	koalice	k1gFnSc2	koalice
<g/>
,	,	kIx,	,
97	[number]	k4	97
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
proti	proti	k7c3	proti
<g/>
,	,	kIx,	,
2	[number]	k4	2
se	se	k3xPyFc4	se
nezúčastnili	zúčastnit	k5eNaPmAgMnP	zúčastnit
a	a	k8xC	a
1	[number]	k4	1
se	se	k3xPyFc4	se
zdržel	zdržet	k5eAaPmAgInS	zdržet
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Musím	muset	k5eAaImIp1nS	muset
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
mě	já	k3xPp1nSc4	já
celkem	celkem	k6eAd1	celkem
překvapilo	překvapit	k5eAaPmAgNnS	překvapit
<g/>
,	,	kIx,	,
příjemně	příjemně	k6eAd1	příjemně
<g/>
,	,	kIx,	,
že	že	k8xS	že
oba	dva	k4xCgMnPc1	dva
jsou	být	k5eAaImIp3nP	být
fiskálně	fiskálně	k6eAd1	fiskálně
odpovědní	odpovědný	k2eAgMnPc1d1	odpovědný
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jim	on	k3xPp3gMnPc3	on
je	být	k5eAaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemohou	moct	k5eNaImIp3nP	moct
sáhnout	sáhnout	k5eAaPmF	sáhnout
do	do	k7c2	do
podstaty	podstata	k1gFnSc2	podstata
problému	problém	k1gInSc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Uvědomují	uvědomovat	k5eAaImIp3nP	uvědomovat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgFnPc4d1	nutná
změny	změna	k1gFnPc4	změna
provést	provést	k5eAaPmF	provést
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
pochválil	pochválit	k5eAaPmAgMnS	pochválit
oba	dva	k4xCgMnPc4	dva
"	"	kIx"	"
<g/>
přeběhlé	přeběhlý	k2eAgMnPc4d1	přeběhlý
<g/>
"	"	kIx"	"
poslance	poslanec	k1gMnPc4	poslanec
premiér	premiér	k1gMnSc1	premiér
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reforma	reforma	k1gFnSc1	reforma
veřejných	veřejný	k2eAgFnPc2d1	veřejná
financí	finance	k1gFnPc2	finance
je	být	k5eAaImIp3nS	být
stěžejním	stěžejní	k2eAgInSc7d1	stěžejní
projektem	projekt	k1gInSc7	projekt
koaliční	koaliční	k2eAgFnSc2d1	koaliční
vlády	vláda	k1gFnSc2	vláda
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
KDU-ČSL	KDU-ČSL	k1gFnSc2	KDU-ČSL
a	a	k8xC	a
Strany	strana	k1gFnSc2	strana
zelených	zelená	k1gFnPc2	zelená
<g/>
.	.	kIx.	.
</s>
<s>
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
angažuje	angažovat	k5eAaBmIp3nS	angažovat
v	v	k7c6	v
jednáních	jednání	k1gNnPc6	jednání
s	s	k7c7	s
USA	USA	kA	USA
o	o	k7c6	o
umístění	umístění	k1gNnSc6	umístění
radaru	radar	k1gInSc2	radar
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
bude	být	k5eAaImBp3nS	být
součástí	součást	k1gFnSc7	součást
protiraketové	protiraketový	k2eAgFnSc2d1	protiraketová
obrany	obrana	k1gFnSc2	obrana
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2007	[number]	k4	2007
představil	představit	k5eAaPmAgMnS	představit
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
5	[number]	k4	5
premiérských	premiérský	k2eAgFnPc2d1	premiérská
priorit	priorita	k1gFnPc2	priorita
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
podpořil	podpořit	k5eAaPmAgInS	podpořit
rozvoj	rozvoj	k1gInSc1	rozvoj
vědy	věda	k1gFnSc2	věda
prezentací	prezentace	k1gFnPc2	prezentace
projektu	projekt	k1gInSc2	projekt
VIVAT	vivat	k1gInSc4	vivat
<g/>
.	.	kIx.	.
</s>
<s>
Topolánkovo	Topolánkův	k2eAgNnSc4d1	Topolánkovo
stanovisko	stanovisko	k1gNnSc4	stanovisko
vůči	vůči	k7c3	vůči
EU	EU	kA	EU
je	být	k5eAaImIp3nS	být
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
<g/>
,	,	kIx,	,
akcentuje	akcentovat	k5eAaImIp3nS	akcentovat
zejména	zejména	k9	zejména
její	její	k3xOp3gInSc1	její
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
význam	význam	k1gInSc1	význam
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
hlasování	hlasování	k1gNnSc1	hlasování
o	o	k7c4	o
setrvání	setrvání	k1gNnSc4	setrvání
vlády	vláda	k1gFnSc2	vláda
vedené	vedený	k2eAgFnSc2d1	vedená
Mirkem	Mirek	k1gMnSc7	Mirek
Topolánkem	Topolánek	k1gMnSc7	Topolánek
<g/>
.	.	kIx.	.
</s>
<s>
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
vládě	vláda	k1gFnSc3	vláda
nedůvěru	nedůvěra	k1gFnSc4	nedůvěra
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
kabinetu	kabinet	k1gInSc3	kabinet
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
97	[number]	k4	97
poslanců	poslanec	k1gMnPc2	poslanec
opozice	opozice	k1gFnSc2	opozice
(	(	kIx(	(
<g/>
ČSSD	ČSSD	kA	ČSSD
a	a	k8xC	a
KSČM	KSČM	kA	KSČM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rebelové	rebel	k1gMnPc1	rebel
ODS	ODS	kA	ODS
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Tlustý	tlustý	k2eAgMnSc1d1	tlustý
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Schwippel	Schwipplo	k1gNnPc2	Schwipplo
a	a	k8xC	a
bývalé	bývalý	k2eAgFnSc2d1	bývalá
poslankyně	poslankyně	k1gFnSc2	poslankyně
SZ	SZ	kA	SZ
Věra	Věra	k1gFnSc1	Věra
Jakubková	Jakubkový	k2eAgFnSc1d1	Jakubková
a	a	k8xC	a
Olga	Olga	k1gFnSc1	Olga
Zubová	zubový	k2eAgFnSc1d1	Zubová
<g/>
.	.	kIx.	.
</s>
<s>
Opozice	opozice	k1gFnSc1	opozice
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
101	[number]	k4	101
<g/>
:	:	kIx,	:
<g/>
97	[number]	k4	97
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2009	[number]	k4	2009
podal	podat	k5eAaPmAgMnS	podat
premiér	premiér	k1gMnSc1	premiér
demisi	demise	k1gFnSc4	demise
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
odstoupila	odstoupit	k5eAaPmAgFnS	odstoupit
celá	celý	k2eAgFnSc1d1	celá
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rezignace	rezignace	k1gFnSc2	rezignace
na	na	k7c4	na
politické	politický	k2eAgFnPc4d1	politická
funkce	funkce	k1gFnPc4	funkce
===	===	k?	===
</s>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2009	[number]	k4	2009
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
poslanecký	poslanecký	k2eAgInSc4d1	poslanecký
mandát	mandát	k1gInSc4	mandát
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
ČSSD	ČSSD	kA	ČSSD
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebude	být	k5eNaImBp3nS	být
hlasovat	hlasovat	k5eAaImF	hlasovat
pro	pro	k7c4	pro
rozpuštění	rozpuštění	k1gNnSc4	rozpuštění
poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
by	by	kYmCp3nS	by
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
vypsání	vypsání	k1gNnSc4	vypsání
předčasných	předčasný	k2eAgFnPc2d1	předčasná
parlamentních	parlamentní	k2eAgFnPc2d1	parlamentní
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
debatě	debata	k1gFnSc6	debata
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
televizi	televize	k1gFnSc6	televize
pak	pak	k6eAd1	pak
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvažuje	zvažovat	k5eAaImIp3nS	zvažovat
setrvání	setrvání	k1gNnSc4	setrvání
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
i	i	k8xC	i
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
ODS	ODS	kA	ODS
<g/>
.1	.1	k4	.1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
na	na	k7c4	na
post	post	k1gInSc4	post
volebního	volební	k2eAgMnSc2d1	volební
lídra	lídr	k1gMnSc2	lídr
ODS	ODS	kA	ODS
kvůli	kvůli	k7c3	kvůli
svým	svůj	k3xOyFgNnPc3	svůj
vyjádřením	vyjádření	k1gNnPc3	vyjádření
pro	pro	k7c4	pro
časopis	časopis	k1gInSc4	časopis
LUI	LUI	kA	LUI
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
pravomoci	pravomoc	k1gFnPc1	pravomoc
přešly	přejít	k5eAaPmAgFnP	přejít
na	na	k7c4	na
pověřeného	pověřený	k2eAgMnSc4d1	pověřený
místopředsedu	místopředseda	k1gMnSc4	místopředseda
a	a	k8xC	a
volebního	volební	k2eAgMnSc4d1	volební
lídra	lídr	k1gMnSc4	lídr
pro	pro	k7c4	pro
parlamentní	parlamentní	k2eAgFnPc4d1	parlamentní
volby	volba	k1gFnPc4	volba
2010	[number]	k4	2010
Petra	Petr	k1gMnSc2	Petr
Nečase	Nečas	k1gMnSc2	Nečas
(	(	kIx(	(
<g/>
který	který	k3yQgMnSc1	který
stranu	strana	k1gFnSc4	strana
vedl	vést	k5eAaImAgMnS	vést
až	až	k9	až
do	do	k7c2	do
dalšího	další	k2eAgInSc2d1	další
volebního	volební	k2eAgInSc2d1	volební
kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
jako	jako	k8xS	jako
nový	nový	k2eAgMnSc1d1	nový
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
post	post	k1gInSc4	post
předsedy	předseda	k1gMnSc2	předseda
strany	strana	k1gFnSc2	strana
pak	pak	k6eAd1	pak
oficiálně	oficiálně	k6eAd1	oficiálně
rezignoval	rezignovat	k5eAaBmAgInS	rezignovat
až	až	k9	až
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
den	den	k1gInSc4	den
po	po	k7c6	po
zasedání	zasedání	k1gNnSc6	zasedání
výkonné	výkonný	k2eAgFnSc2d1	výkonná
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
<g/>
Odstranění	odstranění	k1gNnSc1	odstranění
Topolánka	Topolánek	k1gMnSc2	Topolánek
z	z	k7c2	z
předsednického	předsednický	k2eAgInSc2d1	předsednický
postu	post	k1gInSc2	post
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vnímáno	vnímat	k5eAaImNgNnS	vnímat
jako	jako	k9	jako
ukázka	ukázka	k1gFnSc1	ukázka
mediální	mediální	k2eAgFnSc2d1	mediální
manipulace	manipulace	k1gFnSc2	manipulace
<g/>
.	.	kIx.	.
</s>
<s>
Topolánek	Topolánek	k1gInSc1	Topolánek
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
jako	jako	k8xC	jako
předseda	předseda	k1gMnSc1	předseda
již	již	k6eAd1	již
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
pro	pro	k7c4	pro
ODS	ODS	kA	ODS
přítěží	přítěž	k1gFnSc7	přítěž
<g/>
,	,	kIx,	,
strana	strana	k1gFnSc1	strana
ale	ale	k8xC	ale
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
odstranění	odstranění	k1gNnSc4	odstranění
použila	použít	k5eAaPmAgFnS	použít
výroky	výrok	k1gInPc4	výrok
<g/>
,	,	kIx,	,
kterých	který	k3yQgMnPc2	který
se	se	k3xPyFc4	se
nedopustil	dopustit	k5eNaPmAgMnS	dopustit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výsledku	výsledek	k1gInSc6	výsledek
však	však	k9	však
nahrazení	nahrazení	k1gNnSc1	nahrazení
Topolánka	Topolánka	k1gFnSc1	Topolánka
Petrem	Petr	k1gMnSc7	Petr
Nečasem	Nečas	k1gMnSc7	Nečas
pomohlo	pomoct	k5eAaPmAgNnS	pomoct
ODS	ODS	kA	ODS
k	k	k7c3	k
lepšímu	dobrý	k2eAgInSc3d2	lepší
výsledku	výsledek	k1gInSc3	výsledek
v	v	k7c6	v
nadcházejících	nadcházející	k2eAgFnPc6d1	nadcházející
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
k	k	k7c3	k
sestavení	sestavení	k1gNnSc3	sestavení
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
politiky	politika	k1gFnSc2	politika
===	===	k?	===
</s>
</p>
<p>
<s>
Topolánek	Topolánek	k1gMnSc1	Topolánek
přednášel	přednášet	k5eAaImAgMnS	přednášet
na	na	k7c6	na
NF	NF	kA	NF
VŠE	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
sháněl	shánět	k5eAaImAgMnS	shánět
zakázky	zakázka	k1gFnPc4	zakázka
pro	pro	k7c4	pro
firmu	firma	k1gFnSc4	firma
VAE	VAE	kA	VAE
Controls	Controlsa	k1gFnPc2	Controlsa
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
CTY	CTY	kA	CTY
Group	Group	k1gMnSc1	Group
snažil	snažit	k5eAaImAgMnS	snažit
získat	získat	k5eAaPmF	získat
zakázky	zakázka	k1gFnPc4	zakázka
ve	v	k7c6	v
strojírenství	strojírenství	k1gNnSc6	strojírenství
a	a	k8xC	a
petrochemickém	petrochemický	k2eAgInSc6d1	petrochemický
průmyslu	průmysl	k1gInSc6	průmysl
v	v	k7c6	v
Kazachstánu	Kazachstán	k1gInSc6	Kazachstán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
září	září	k1gNnSc2	září
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
předsedou	předseda	k1gMnSc7	předseda
výkonné	výkonný	k2eAgFnSc2d1	výkonná
rady	rada	k1gFnSc2	rada
Teplárenského	teplárenský	k2eAgNnSc2d1	Teplárenské
sdružení	sdružení	k1gNnSc2	sdružení
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
předsedou	předseda	k1gMnSc7	předseda
dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
rady	rada	k1gFnSc2	rada
Elektrárny	elektrárna	k1gFnSc2	elektrárna
Opatovice	Opatovice	k1gFnSc2	Opatovice
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2015	[number]	k4	2015
oznámil	oznámit	k5eAaPmAgMnS	oznámit
předseda	předseda	k1gMnSc1	předseda
ODS	ODS	kA	ODS
Petr	Petr	k1gMnSc1	Petr
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
přestal	přestat	k5eAaPmAgMnS	přestat
platit	platit	k5eAaImF	platit
členské	členský	k2eAgInPc4d1	členský
příspěvky	příspěvek	k1gInPc4	příspěvek
a	a	k8xC	a
proto	proto	k8xC	proto
už	už	k6eAd1	už
není	být	k5eNaImIp3nS	být
členem	člen	k1gMnSc7	člen
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
<g/>
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
knihy	kniha	k1gFnSc2	kniha
Hlavně	hlavně	k6eAd1	hlavně
se	se	k3xPyFc4	se
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
neposrat	posrat	k5eNaPmF	posrat
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
odkrývá	odkrývat	k5eAaImIp3nS	odkrývat
mnohé	mnohé	k1gNnSc4	mnohé
z	z	k7c2	z
pozadí	pozadí	k1gNnSc2	pozadí
české	český	k2eAgFnSc2d1	Česká
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
díky	díky	k7c3	díky
úplatkům	úplatek	k1gInPc3	úplatek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kandidatura	kandidatura	k1gFnSc1	kandidatura
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2017	[number]	k4	2017
oznámil	oznámit	k5eAaPmAgMnS	oznámit
svou	svůj	k3xOyFgFnSc4	svůj
kandidaturu	kandidatura	k1gFnSc4	kandidatura
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
kandidovat	kandidovat	k5eAaImF	kandidovat
bylo	být	k5eAaImAgNnS	být
spíše	spíše	k9	spíše
emotivní	emotivní	k2eAgNnSc1d1	emotivní
než	než	k8xS	než
rozumové	rozumový	k2eAgNnSc1d1	rozumové
<g/>
.	.	kIx.	.
</s>
<s>
Vyjednávat	vyjednávat	k5eAaImF	vyjednávat
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
kandidaturu	kandidatura	k1gFnSc4	kandidatura
začal	začít	k5eAaPmAgMnS	začít
u	u	k7c2	u
poslanců	poslanec	k1gMnPc2	poslanec
a	a	k8xC	a
senátorů	senátor	k1gMnPc2	senátor
až	až	k6eAd1	až
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
termín	termín	k1gInSc1	termín
pro	pro	k7c4	pro
odevzdání	odevzdání	k1gNnSc4	odevzdání
kandidátních	kandidátní	k2eAgFnPc2d1	kandidátní
listin	listina	k1gFnPc2	listina
na	na	k7c4	na
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
byl	být	k5eAaImAgInS	být
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
kandidaturu	kandidatura	k1gFnSc4	kandidatura
uvítala	uvítat	k5eAaPmAgFnS	uvítat
například	například	k6eAd1	například
místopředsedkyně	místopředsedkyně	k1gFnSc1	místopředsedkyně
ODS	ODS	kA	ODS
Alexandra	Alexandr	k1gMnSc2	Alexandr
Udženija	Udženijus	k1gMnSc2	Udženijus
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
bývala	bývat	k5eAaImAgFnS	bývat
jeho	jeho	k3xOp3gMnSc7	jeho
oponentem	oponent	k1gMnSc7	oponent
<g/>
,	,	kIx,	,
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
strany	strana	k1gFnSc2	strana
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kalousek	Kalousek	k1gMnSc1	Kalousek
<g/>
.	.	kIx.	.
</s>
<s>
Šanci	šance	k1gFnSc4	šance
získat	získat	k5eAaPmF	získat
hlasy	hlas	k1gInPc4	hlas
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
mít	mít	k5eAaImF	mít
podle	podle	k7c2	podle
politologa	politolog	k1gMnSc2	politolog
Ladislava	Ladislav	k1gMnSc2	Ladislav
Mrklase	Mrklasa	k1gFnSc3	Mrklasa
od	od	k7c2	od
podstatné	podstatný	k2eAgFnSc2d1	podstatná
části	část	k1gFnSc2	část
voličů	volič	k1gMnPc2	volič
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k9	i
Starostů	Starosta	k1gMnPc2	Starosta
a	a	k8xC	a
KDU-ČSL	KDU-ČSL	k1gMnPc2	KDU-ČSL
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2017	[number]	k4	2017
byla	být	k5eAaImAgFnS	být
Topolánkova	Topolánkův	k2eAgFnSc1d1	Topolánkova
kandidatura	kandidatura	k1gFnSc1	kandidatura
podpořena	podpořit	k5eAaPmNgFnS	podpořit
Stranou	strana	k1gFnSc7	strana
soukromníků	soukromník	k1gMnPc2	soukromník
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
podpoře	podpora	k1gFnSc3	podpora
10	[number]	k4	10
senátorů	senátor	k1gMnPc2	senátor
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
devíti	devět	k4xCc2	devět
prezidentských	prezidentský	k2eAgMnPc2d1	prezidentský
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Soukromý	soukromý	k2eAgInSc4d1	soukromý
život	život	k1gInSc4	život
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2010	[number]	k4	2010
se	se	k3xPyFc4	se
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
s	s	k7c7	s
Pavlou	Pavla	k1gFnSc7	Pavla
Topolánkovou	Topolánkův	k2eAgFnSc7d1	Topolánkova
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
má	mít	k5eAaImIp3nS	mít
dcery	dcera	k1gFnPc4	dcera
Petru	Petr	k1gMnSc3	Petr
<g/>
,	,	kIx,	,
Janu	Jan	k1gMnSc3	Jan
a	a	k8xC	a
syna	syn	k1gMnSc4	syn
Tomáše	Tomáš	k1gMnSc4	Tomáš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Lucií	Lucie	k1gFnSc7	Lucie
Talmanovou	Talmanův	k2eAgFnSc7d1	Talmanova
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
žil	žít	k5eAaImAgMnS	žít
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
syna	syn	k1gMnSc4	syn
Nicolase	Nicolasa	k1gFnSc6	Nicolasa
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
první	první	k4xOgInPc1	první
veřejné	veřejný	k2eAgInPc1d1	veřejný
dohady	dohad	k1gInPc1	dohad
o	o	k7c6	o
Topolánkově	Topolánkův	k2eAgInSc6d1	Topolánkův
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
Talmanové	Talmanová	k1gFnSc3	Talmanová
<g/>
,	,	kIx,	,
oznámila	oznámit	k5eAaPmAgFnS	oznámit
jeho	jeho	k3xOp3gMnPc3	jeho
manželka	manželka	k1gFnSc1	manželka
Pavla	Pavla	k1gFnSc1	Pavla
Topolánková	Topolánková	k1gFnSc1	Topolánková
svou	svůj	k3xOyFgFnSc4	svůj
kandidaturu	kandidatura	k1gFnSc4	kandidatura
proti	proti	k7c3	proti
ODS	ODS	kA	ODS
v	v	k7c6	v
senátních	senátní	k2eAgFnPc6d1	senátní
volbách	volba	k1gFnPc6	volba
<g/>
;	;	kIx,	;
u	u	k7c2	u
voličů	volič	k1gMnPc2	volič
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
ale	ale	k8xC	ale
za	za	k7c4	za
stranu	strana	k1gFnSc4	strana
Politika	politika	k1gFnSc1	politika
21	[number]	k4	21
neuspěla	uspět	k5eNaPmAgFnS	uspět
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2009	[number]	k4	2009
až	až	k8xS	až
2011	[number]	k4	2011
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
rallye	rallye	k1gFnSc2	rallye
na	na	k7c6	na
soutěži	soutěž	k1gFnSc6	soutěž
Pražský	pražský	k2eAgInSc4d1	pražský
Rallysprint	Rallysprint	k1gInSc4	Rallysprint
jako	jako	k8xC	jako
navigátor	navigátor	k1gInSc4	navigátor
Václava	Václav	k1gMnSc2	Václav
Pecha	Pech	k1gMnSc2	Pech
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obchodní	obchodní	k2eAgFnSc2d1	obchodní
<g/>
,	,	kIx,	,
řídící	řídící	k2eAgFnSc2d1	řídící
a	a	k8xC	a
správní	správní	k2eAgFnSc2d1	správní
aktivity	aktivita	k1gFnSc2	aktivita
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
výpisu	výpis	k1gInSc2	výpis
z	z	k7c2	z
obchodního	obchodní	k2eAgInSc2d1	obchodní
rejstříku	rejstřík	k1gInSc2	rejstřík
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
1991	[number]	k4	1991
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
1996	[number]	k4	1996
společník	společník	k1gMnSc1	společník
<g/>
,	,	kIx,	,
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
1993	[number]	k4	1993
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
1996	[number]	k4	1996
jednatel	jednatel	k1gMnSc1	jednatel
VAE	VAE	kA	VAE
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
1994	[number]	k4	1994
do	do	k7c2	do
října	říjen	k1gInSc2	říjen
1995	[number]	k4	1995
člen	člen	k1gMnSc1	člen
dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
rady	rada	k1gFnSc2	rada
Černá	černý	k2eAgFnSc1d1	černá
louka	louka	k1gFnSc1	louka
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Ostravská	ostravský	k2eAgFnSc1d1	Ostravská
obchodní	obchodní	k2eAgFnSc1d1	obchodní
společnost	společnost	k1gFnSc1	společnost
<g/>
)	)	kIx)	)
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
od	od	k7c2	od
března	březen	k1gInSc2	březen
1995	[number]	k4	1995
do	do	k7c2	do
září	září	k1gNnSc2	září
1998	[number]	k4	1998
člen	člen	k1gMnSc1	člen
dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
rady	rada	k1gFnSc2	rada
CR	cr	k0	cr
Fontána	fontána	k1gFnSc1	fontána
penzijní	penzijní	k2eAgFnSc1d1	penzijní
fond	fond	k1gInSc4	fond
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
1996	[number]	k4	1996
člen	člen	k1gMnSc1	člen
<g/>
,	,	kIx,	,
od	od	k7c2	od
března	březen	k1gInSc2	březen
1997	[number]	k4	1997
místopředseda	místopředseda	k1gMnSc1	místopředseda
<g/>
,	,	kIx,	,
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
2000	[number]	k4	2000
předseda	předseda	k1gMnSc1	předseda
dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
rady	rada	k1gFnSc2	rada
Dalkia	Dalkia	k1gFnSc1	Dalkia
Ostrava	Ostrava	k1gFnSc1	Ostrava
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Zásobování	zásobování	k1gNnPc2	zásobování
teplem	teplo	k1gNnSc7	teplo
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
)	)	kIx)	)
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
1996	[number]	k4	1996
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
1998	[number]	k4	1998
člen	člen	k1gMnSc1	člen
dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
rady	rada	k1gFnSc2	rada
Vítkovické	vítkovický	k2eAgFnSc2d1	Vítkovická
stavby	stavba	k1gFnSc2	stavba
Ostrava	Ostrava	k1gFnSc1	Ostrava
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
1994	[number]	k4	1994
do	do	k7c2	do
června	červen	k1gInSc2	červen
1998	[number]	k4	1998
jednatel	jednatel	k1gMnSc1	jednatel
a	a	k8xC	a
společník	společník	k1gMnSc1	společník
Montikon	Montikon	k1gMnSc1	Montikon
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
JJM	JJM	kA	JJM
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
HPT	HPT	kA	HPT
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
1994	[number]	k4	1994
člen	člen	k1gMnSc1	člen
<g/>
,	,	kIx,	,
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
1996	[number]	k4	1996
místopředseda	místopředseda	k1gMnSc1	místopředseda
<g/>
,	,	kIx,	,
od	od	k7c2	od
července	červenec	k1gInSc2	červenec
2001	[number]	k4	2001
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
2003	[number]	k4	2003
předseda	předseda	k1gMnSc1	předseda
představenstva	představenstvo	k1gNnSc2	představenstvo
Free	Free	k1gFnSc1	Free
Zone	Zone	k1gFnSc1	Zone
Ostrava	Ostrava	k1gFnSc1	Ostrava
a.s.	a.s.	k?	a.s.
–	–	k?	–
Free	Free	k1gFnSc1	Free
Zone	Zone	k1gFnSc1	Zone
Ostrava	Ostrava	k1gFnSc1	Ostrava
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2002	[number]	k4	2002
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
2003	[number]	k4	2003
člen	člen	k1gMnSc1	člen
dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
rady	rada	k1gFnSc2	rada
Elektrotrans	Elektrotrans	k1gInSc1	Elektrotrans
a.s.	a.s.	k?	a.s.
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
od	od	k7c2	od
září	září	k1gNnSc2	září
2011	[number]	k4	2011
předseda	předseda	k1gMnSc1	předseda
výkonné	výkonný	k2eAgFnSc2d1	výkonná
rady	rada	k1gFnSc2	rada
Teplárenského	teplárenský	k2eAgNnSc2d1	Teplárenské
sdružení	sdružení	k1gNnSc2	sdružení
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kritika	kritika	k1gFnSc1	kritika
a	a	k8xC	a
aféry	aféra	k1gFnPc1	aféra
==	==	k?	==
</s>
</p>
<p>
<s>
Lobbista	lobbista	k1gMnSc1	lobbista
a	a	k8xC	a
Topolánkův	Topolánkův	k2eAgMnSc1d1	Topolánkův
přítel	přítel	k1gMnSc1	přítel
Marek	Marek	k1gMnSc1	Marek
Dalík	Dalík	k1gMnSc1	Dalík
byl	být	k5eAaImAgMnS	být
médii	médium	k1gNnPc7	médium
a	a	k8xC	a
opozicí	opozice	k1gFnSc7	opozice
často	často	k6eAd1	často
kritizovaný	kritizovaný	k2eAgInSc1d1	kritizovaný
za	za	k7c4	za
neetické	etický	k2eNgFnPc4d1	neetická
metody	metoda	k1gFnPc4	metoda
a	a	k8xC	a
pro	pro	k7c4	pro
nadměrný	nadměrný	k2eAgInSc4d1	nadměrný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
Topolánka	Topolánek	k1gMnSc4	Topolánek
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
Topolánek	Topolánek	k1gInSc1	Topolánek
vyzdvihl	vyzdvihnout	k5eAaPmAgInS	vyzdvihnout
Dalíkovu	Dalíkův	k2eAgFnSc4d1	Dalíkova
práci	práce	k1gFnSc4	práce
pro	pro	k7c4	pro
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2007	[number]	k4	2007
Topolánek	Topolánek	k1gMnSc1	Topolánek
způsobil	způsobit	k5eAaPmAgMnS	způsobit
aféru	aféra	k1gFnSc4	aféra
<g/>
,	,	kIx,	,
když	když	k8xS	když
ukázal	ukázat	k5eAaPmAgInS	ukázat
prostředníček	prostředníček	k1gInSc1	prostředníček
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
opozičním	opoziční	k2eAgMnPc3d1	opoziční
poslancům	poslanec	k1gMnPc3	poslanec
stěžujícím	stěžující	k2eAgMnPc3d1	stěžující
si	se	k3xPyFc3	se
na	na	k7c4	na
nepřítomnost	nepřítomnost	k1gFnSc4	nepřítomnost
členů	člen	k1gMnPc2	člen
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
vzápětí	vzápětí	k6eAd1	vzápětí
po	po	k7c6	po
jejich	jejich	k3xOp3gInSc6	jejich
ironickém	ironický	k2eAgInSc6d1	ironický
potlesku	potlesk	k1gInSc6	potlesk
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
dostavil	dostavit	k5eAaPmAgMnS	dostavit
do	do	k7c2	do
jednacího	jednací	k2eAgInSc2d1	jednací
sálu	sál	k1gInSc2	sál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
nonverbální	nonverbální	k2eAgFnSc4d1	nonverbální
komunikaci	komunikace	k1gFnSc4	komunikace
s	s	k7c7	s
ministrem	ministr	k1gMnSc7	ministr
Miroslavem	Miroslav	k1gMnSc7	Miroslav
Kalouskem	Kalousek	k1gMnSc7	Kalousek
a	a	k8xC	a
omluvil	omluvit	k5eAaPmAgMnS	omluvit
se	se	k3xPyFc4	se
za	za	k7c4	za
ně	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
poté	poté	k6eAd1	poté
říkal	říkat	k5eAaImAgMnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kalouskovi	Kalousek	k1gMnSc3	Kalousek
ukazoval	ukazovat	k5eAaImAgMnS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jednička	jednička	k1gFnSc1	jednička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žaloval	žalovat	k5eAaImAgMnS	žalovat
novináře	novinář	k1gMnPc4	novinář
za	za	k7c4	za
zveřejnění	zveřejnění	k1gNnSc4	zveřejnění
odposlechů	odposlech	k1gInPc2	odposlech
v	v	k7c6	v
kauze	kauza	k1gFnSc6	kauza
údajného	údajný	k2eAgNnSc2d1	údajné
podplácení	podplácení	k1gNnSc2	podplácení
poslance	poslanec	k1gMnSc2	poslanec
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Kořistky	Kořistka	k1gFnSc2	Kořistka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
mimo	mimo	k6eAd1	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
říkal	říkat	k5eAaImAgMnS	říkat
jak	jak	k8xC	jak
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
poradcem	poradce	k1gMnSc7	poradce
Markem	Marek	k1gMnSc7	Marek
Dalíkem	Dalík	k1gMnSc7	Dalík
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
Martin	Martin	k1gMnSc1	Martin
Bormann	Bormann	k1gMnSc1	Bormann
a	a	k8xC	a
jehož	jehož	k3xOyRp3gFnSc1	jehož
část	část	k1gFnSc1	část
veřejnosti	veřejnost	k1gFnSc2	veřejnost
a	a	k8xC	a
politici	politik	k1gMnPc1	politik
ODS	ODS	kA	ODS
kritizují	kritizovat	k5eAaImIp3nP	kritizovat
za	za	k7c4	za
příliš	příliš	k6eAd1	příliš
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
předsedu	předseda	k1gMnSc4	předseda
<g/>
)	)	kIx)	)
stojí	stát	k5eAaImIp3nS	stát
a	a	k8xC	a
padá	padat	k5eAaImIp3nS	padat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
proces	proces	k1gInSc1	proces
prohrál	prohrát	k5eAaPmAgInS	prohrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kopl	kopnout	k5eAaPmAgMnS	kopnout
do	do	k7c2	do
auta	auto	k1gNnSc2	auto
jednoho	jeden	k4xCgMnSc2	jeden
novináře	novinář	k1gMnSc2	novinář
Blesku	blesk	k1gInSc2	blesk
a	a	k8xC	a
údajně	údajně	k6eAd1	údajně
křičel	křičet	k5eAaImAgMnS	křičet
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
zabije	zabít	k5eAaPmIp3nS	zabít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
později	pozdě	k6eAd2	pozdě
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nelíbilo	líbit	k5eNaImAgNnS	líbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
novináři	novinář	k1gMnPc1	novinář
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
denně	denně	k6eAd1	denně
pronásledují	pronásledovat	k5eAaImIp3nP	pronásledovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
za	za	k7c2	za
inspirování	inspirování	k1gNnSc2	inspirování
se	se	k3xPyFc4	se
nacistickým	nacistický	k2eAgInSc7d1	nacistický
slovníkem	slovník	k1gInSc7	slovník
a	a	k8xC	a
občas	občas	k6eAd1	občas
i	i	k9	i
vulgární	vulgární	k2eAgNnSc1d1	vulgární
vyjadřování	vyjadřování	k1gNnSc1	vyjadřování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
občanské	občanský	k2eAgNnSc1d1	občanské
sdružení	sdružení	k1gNnSc1	sdružení
Becario	Becario	k6eAd1	Becario
údajně	údajně	k6eAd1	údajně
80	[number]	k4	80
%	%	kIx~	%
příjmů	příjem	k1gInPc2	příjem
od	od	k7c2	od
sponzorů	sponzor	k1gMnPc2	sponzor
utratí	utratit	k5eAaPmIp3nS	utratit
na	na	k7c4	na
vlastní	vlastní	k2eAgInPc4d1	vlastní
platy	plat	k1gInPc4	plat
a	a	k8xC	a
organizování	organizování	k1gNnSc4	organizování
golfových	golfový	k2eAgInPc2d1	golfový
turnajů	turnaj	k1gInPc2	turnaj
a	a	k8xC	a
20	[number]	k4	20
%	%	kIx~	%
na	na	k7c4	na
stipendia	stipendium	k1gNnPc4	stipendium
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2009	[number]	k4	2009
se	se	k3xPyFc4	se
za	za	k7c2	za
nevyjasněných	vyjasněný	k2eNgFnPc2d1	nevyjasněná
okolností	okolnost	k1gFnPc2	okolnost
účastnil	účastnit	k5eAaImAgInS	účastnit
neformálního	formální	k2eNgNnSc2d1	neformální
setkání	setkání	k1gNnSc2	setkání
s	s	k7c7	s
vlivnými	vlivný	k2eAgMnPc7d1	vlivný
lobbisty	lobbista	k1gMnPc7	lobbista
a	a	k8xC	a
podnikateli	podnikatel	k1gMnPc7	podnikatel
v	v	k7c6	v
Toskánsku	Toskánsko	k1gNnSc6	Toskánsko
(	(	kIx(	(
<g/>
podrobněji	podrobně	k6eAd2	podrobně
toskánská	toskánský	k2eAgFnSc1d1	Toskánská
aféra	aféra	k1gFnSc1	aféra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vnesl	vnést	k5eAaPmAgMnS	vnést
do	do	k7c2	do
české	český	k2eAgFnSc2d1	Česká
politiky	politika	k1gFnSc2	politika
a	a	k8xC	a
českých	český	k2eAgNnPc2d1	české
médií	médium	k1gNnPc2	médium
pojem	pojem	k1gInSc4	pojem
krajští	krajský	k2eAgMnPc1d1	krajský
kmotři	kmotr	k1gMnPc1	kmotr
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
kmotry	kmotr	k1gMnPc7	kmotr
patřil	patřit	k5eAaImAgMnS	patřit
severomoravský	severomoravský	k2eAgMnSc1d1	severomoravský
podnikatel	podnikatel	k1gMnSc1	podnikatel
s	s	k7c7	s
vazbami	vazba	k1gFnPc7	vazba
na	na	k7c4	na
Topolánka	Topolánek	k1gMnSc4	Topolánek
Tomáš	Tomáš	k1gMnSc1	Tomáš
Chrenek	Chrenka	k1gFnPc2	Chrenka
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2010	[number]	k4	2010
se	se	k3xPyFc4	se
Topolánek	Topolánek	k1gMnSc1	Topolánek
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
pro	pro	k7c4	pro
gaye	gay	k1gMnSc4	gay
LUI	LUI	kA	LUI
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
o	o	k7c4	o
premiérovi	premiér	k1gMnSc3	premiér
Fischerovi	Fischer	k1gMnSc3	Fischer
a	a	k8xC	a
ministru	ministr	k1gMnSc3	ministr
dopravy	doprava	k1gFnSc2	doprava
Gustavu	Gustav	k1gMnSc3	Gustav
Slamečkovi	Slameček	k1gMnSc3	Slameček
<g/>
.	.	kIx.	.
<g/>
Druhé	druhý	k4xOgNnSc1	druhý
zvolení	zvolení	k1gNnSc1	zvolení
Václava	Václav	k1gMnSc2	Václav
Klause	Klaus	k1gMnSc2	Klaus
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
podpora	podpora	k1gFnSc1	podpora
pro	pro	k7c4	pro
premiéra	premiér	k1gMnSc4	premiér
Jana	Jan	k1gMnSc2	Jan
Fischera	Fischer	k1gMnSc2	Fischer
a	a	k8xC	a
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
<g />
.	.	kIx.	.
</s>
<s>
Tlustý	tlustý	k2eAgMnSc1d1	tlustý
coby	coby	k?	coby
ministr	ministr	k1gMnSc1	ministr
financí	finance	k1gFnPc2	finance
<g/>
:	:	kIx,	:
tři	tři	k4xCgFnPc1	tři
hlavní	hlavní	k2eAgFnPc1d1	hlavní
personální	personální	k2eAgFnPc1d1	personální
chyby	chyba	k1gFnPc1	chyba
<g/>
,	,	kIx,	,
kterých	který	k3yIgFnPc2	který
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
svých	svůj	k3xOyFgNnPc2	svůj
vlastních	vlastní	k2eAgNnPc2d1	vlastní
slov	slovo	k1gNnPc2	slovo
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
ve	v	k7c6	v
špičkové	špičkový	k2eAgFnSc6d1	špičková
politice	politika	k1gFnSc6	politika
dopustil	dopustit	k5eAaPmAgMnS	dopustit
<g/>
.	.	kIx.	.
<g/>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
šéf	šéf	k1gMnSc1	šéf
civilní	civilní	k2eAgFnSc2d1	civilní
rozvědky	rozvědka	k1gFnSc2	rozvědka
gen.	gen.	kA	gen.
Karel	Karel	k1gMnSc1	Karel
Randák	Randák	k1gMnSc1	Randák
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přinesl	přinést	k5eAaPmAgMnS	přinést
fotky	fotka	k1gFnPc4	fotka
v	v	k7c6	v
toskánské	toskánský	k2eAgFnSc6d1	Toskánská
aféře	aféra	k1gFnSc6	aféra
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
kandidaturě	kandidatur	k1gInSc6	kandidatur
Topolánka	Topolánka	k1gFnSc1	Topolánka
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
Daniela	Daniel	k1gMnSc2	Daniel
Křetínského	Křetínský	k1gMnSc2	Křetínský
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
zájmových	zájmový	k2eAgFnPc2d1	zájmová
skupin	skupina	k1gFnPc2	skupina
z	z	k7c2	z
byznysu	byznys	k1gInSc2	byznys
<g/>
.	.	kIx.	.
</s>
<s>
Randák	Randák	k1gInSc1	Randák
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
mesiášský	mesiášský	k2eAgInSc4d1	mesiášský
komplex	komplex	k1gInSc4	komplex
Topolánkovo	Topolánkův	k2eAgNnSc1d1	Topolánkovo
stavění	stavění	k1gNnSc1	stavění
se	se	k3xPyFc4	se
do	do	k7c2	do
role	role	k1gFnSc2	role
záchrance	záchranka	k1gFnSc6	záchranka
státu	stát	k1gInSc2	stát
před	před	k7c7	před
znovuzvolením	znovuzvolení	k1gNnSc7	znovuzvolení
Miloše	Miloš	k1gMnSc2	Miloš
Zemana	Zeman	k1gMnSc2	Zeman
za	za	k7c4	za
prezidenta	prezident	k1gMnSc4	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Expremiér	expremiér	k1gMnSc1	expremiér
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
Randák	Randák	k1gInSc1	Randák
<g/>
,	,	kIx,	,
daleko	daleko	k6eAd1	daleko
víc	hodně	k6eAd2	hodně
dehonestoval	dehonestovat	k5eAaBmAgInS	dehonestovat
funkci	funkce	k1gFnSc4	funkce
premiéra	premiér	k1gMnSc2	premiér
než	než	k8xS	než
Zeman	Zeman	k1gMnSc1	Zeman
funkci	funkce	k1gFnSc4	funkce
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
<g/>
Otazníky	otazník	k1gInPc1	otazník
vzbuzují	vzbuzovat	k5eAaImIp3nP	vzbuzovat
majetkové	majetkový	k2eAgInPc1d1	majetkový
poměry	poměr	k1gInPc1	poměr
Mirka	Mirek	k1gMnSc2	Mirek
Topolánka	Topolánek	k1gMnSc2	Topolánek
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
luxusního	luxusní	k2eAgInSc2d1	luxusní
bytu	byt	k1gInSc2	byt
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Špindlerově	Špindlerův	k2eAgInSc6d1	Špindlerův
Mlýně	mlýn	k1gInSc6	mlýn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mirek	Mirka	k1gFnPc2	Mirka
Topolánek	Topolánka	k1gFnPc2	Topolánka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
kandidatury	kandidatura	k1gFnSc2	kandidatura
</s>
</p>
<p>
<s>
Blog	Blog	k1gInSc1	Blog
Mirka	Mirek	k1gMnSc2	Mirek
Topolánka	Topolánka	k1gFnSc1	Topolánka
na	na	k7c4	na
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Senátu	senát	k1gInSc2	senát
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
senátorském	senátorský	k2eAgNnSc6d1	senátorské
období	období	k1gNnSc6	období
</s>
</p>
<p>
<s>
Mirek	Mirek	k1gMnSc1	Mirek
Topolánek	Topolánek	k1gMnSc1	Topolánek
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Senátu	senát	k1gInSc2	senát
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
senátorském	senátorský	k2eAgNnSc6d1	senátorské
období	období	k1gNnSc6	období
</s>
</p>
