<s>
Jean-François	Jean-François	k1gMnSc1
Champollion	Champollion	k1gMnSc1
</s>
<s>
Jean-François	Jean-François	k1gInSc4
Champollion	Champollion	k1gInSc1
Narození	narození	k1gNnSc1
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1790	#num#	k4
Figeac	Figeac	k1gFnSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1832	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
41	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Paříž	Paříž	k1gFnSc1
Příčina	příčina	k1gFnSc1
úmrtí	úmrtí	k1gNnPc2
</s>
<s>
srdeční	srdeční	k2eAgFnSc1d1
zástava	zástava	k1gFnSc1
a	a	k8xC
cévní	cévní	k2eAgFnSc1d1
mozková	mozkový	k2eAgFnSc1d1
příhoda	příhoda	k1gFnSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Hřbitov	hřbitov	k1gInSc1
Pè	Pè	k1gFnSc2
(	(	kIx(
<g/>
48	#num#	k4
<g/>
°	°	k?
<g/>
51	#num#	k4
<g/>
′	′	k?
<g/>
36	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
2	#num#	k4
<g/>
°	°	k?
<g/>
23	#num#	k4
<g/>
′	′	k?
<g/>
39	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
)	)	kIx)
Povolání	povolání	k1gNnSc1
</s>
<s>
egyptolog	egyptolog	k1gMnSc1
<g/>
,	,	kIx,
vysokoškolský	vysokoškolský	k2eAgMnSc1d1
učitel	učitel	k1gMnSc1
<g/>
,	,	kIx,
jazykovědec	jazykovědec	k1gMnSc1
<g/>
,	,	kIx,
archeolog	archeolog	k1gMnSc1
<g/>
,	,	kIx,
filozof	filozof	k1gMnSc1
<g/>
,	,	kIx,
epigrafik	epigrafika	k1gFnPc2
<g/>
,	,	kIx,
profesor	profesor	k1gMnSc1
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
<g/>
,	,	kIx,
entomolog	entomolog	k1gMnSc1
a	a	k8xC
klasický	klasický	k2eAgMnSc1d1
učenec	učenec	k1gMnSc1
Národnost	národnost	k1gFnSc4
</s>
<s>
Francouzi	Francouz	k1gMnPc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Lycée	Lycée	k1gFnSc1
Stendhal	Stendhal	k1gMnSc1
(	(	kIx(
<g/>
Grenoble	Grenoble	k1gInSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
1804	#num#	k4
<g/>
–	–	k?
<g/>
1807	#num#	k4
<g/>
)	)	kIx)
<g/>
Institut	institut	k1gInSc1
national	nationat	k5eAaImAgInS,k5eAaPmAgInS
des	des	k1gNnSc7
langues	langues	k1gInSc1
et	et	k?
civilisations	civilisations	k1gInSc1
orientalesCollè	orientalesCollè	k1gFnPc2
de	de	k?
FranceUniversité	FranceUniversita	k1gMnPc1
Grenoble-Alpes	Grenoble-Alpes	k1gMnSc1
Témata	téma	k1gNnPc4
</s>
<s>
Egyptské	egyptský	k2eAgInPc1d1
hieroglyfy	hieroglyf	k1gInPc1
<g/>
,	,	kIx,
dějiny	dějiny	k1gFnPc1
<g/>
,	,	kIx,
lingvistika	lingvistika	k1gFnSc1
a	a	k8xC
egyptologie	egyptologie	k1gFnSc1
Ocenění	ocenění	k1gNnSc2
</s>
<s>
rytíř	rytíř	k1gMnSc1
Čestné	čestný	k2eAgFnSc2d1
legieŘád	legieŘáda	k1gFnPc2
svatého	svatý	k2eAgMnSc2d1
Josefa	Josef	k1gMnSc2
Rodiče	rodič	k1gMnSc2
</s>
<s>
Jacques	Jacques	k1gMnSc1
Champollion	Champollion	k1gInSc4
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Jacques	Jacques	k1gMnSc1
Joseph	Joseph	k1gMnSc1
Champollion-Figeac	Champollion-Figeac	k1gFnSc1
<g/>
,	,	kIx,
Thérè	Thérè	k1gFnSc1
Champollion	Champollion	k1gInSc1
a	a	k8xC
Marie	Marie	k1gFnSc1
Champollion	Champollion	k1gInSc1
(	(	kIx(
<g/>
sourozenci	sourozenec	k1gMnPc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
galerie	galerie	k1gFnSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
původní	původní	k2eAgInPc4d1
texty	text	k1gInPc4
na	na	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jean-François	Jean-François	k1gInSc1
Champollion	Champollion	k1gInSc1
(	(	kIx(
<g/>
23	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc4
1790	#num#	k4
Figeac	Figeac	k1gInSc4
–	–	k?
4	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc4
1832	#num#	k4
Paříž	Paříž	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
francouzský	francouzský	k2eAgMnSc1d1
archeolog	archeolog	k1gMnSc1
a	a	k8xC
zakladatel	zakladatel	k1gMnSc1
egyptologie	egyptologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1822	#num#	k4
rozluštil	rozluštit	k5eAaPmAgInS
jako	jako	k9
první	první	k4xOgInPc4
egyptské	egyptský	k2eAgInPc4d1
hieroglyfy	hieroglyf	k1gInPc4
s	s	k7c7
pomocí	pomoc	k1gFnSc7
trojjazyčné	trojjazyčný	k2eAgFnSc2d1
Rosettské	rosettský	k2eAgFnSc2d1
desky	deska	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	s	k7c7
23	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1790	#num#	k4
v	v	k7c6
jihofrancouzském	jihofrancouzský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Figeac	Figeac	k1gFnSc4
<g/>
;	;	kIx,
kvůli	kvůli	k7c3
tmavé	tmavý	k2eAgFnSc3d1
pleti	pleť	k1gFnSc3
byl	být	k5eAaImAgMnS
přezdíván	přezdíván	k2eAgMnSc1d1
„	„	k?
<g/>
Egypťan	Egypťan	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
obdařen	obdařit	k5eAaPmNgMnS
výjimečným	výjimečný	k2eAgMnSc7d1
jazykovým	jazykový	k2eAgNnSc7d1
nadáním	nadání	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
v	v	k7c6
pěti	pět	k4xCc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
naučil	naučit	k5eAaPmAgMnS
sám	sám	k3xTgMnSc1
číst	číst	k5eAaImF
a	a	k8xC
psát	psát	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
devíti	devět	k4xCc2
letech	let	k1gInPc6
uměl	umět	k5eAaImAgInS
řecky	řecky	k6eAd1
a	a	k8xC
latinsky	latinsky	k6eAd1
<g/>
;	;	kIx,
jako	jako	k8xC,k8xS
jedenáctiletý	jedenáctiletý	k2eAgMnSc1d1
četl	číst	k5eAaImAgMnS
bibli	bible	k1gFnSc4
z	z	k7c2
hebrejského	hebrejský	k2eAgInSc2d1
originálu	originál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
také	také	k6eAd1
sepsal	sepsat	k5eAaPmAgMnS
svoji	svůj	k3xOyFgFnSc4
první	první	k4xOgFnSc4
knihu	kniha	k1gFnSc4
„	„	k?
<g/>
Dějiny	dějiny	k1gFnPc1
slavných	slavný	k2eAgMnPc2d1
psů	pes	k1gMnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
13	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
začal	začít	k5eAaPmAgMnS
se	se	k3xPyFc4
učit	učit	k5eAaImF
arabsky	arabsky	k6eAd1
a	a	k8xC
syrsky	syrsky	k6eAd1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
koptsky	koptsky	k6eAd1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
patnáctiletý	patnáctiletý	k2eAgMnSc1d1
studoval	studovat	k5eAaImAgMnS
perštinu	perština	k1gFnSc4
<g/>
,	,	kIx,
jazyk	jazyk	k1gInSc4
zend	zenda	k1gFnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
chaldejštinu	chaldejština	k1gFnSc4
<g/>
,	,	kIx,
sanskrt	sanskrt	k1gInSc4
a	a	k8xC
pro	pro	k7c4
rozptýlení	rozptýlení	k1gNnSc4
také	také	k9
čínštinu	čínština	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
1808	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
členem	člen	k1gMnSc7
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
v	v	k7c6
Grenoblu	Grenoble	k1gInSc6
<g/>
;	;	kIx,
uvedl	uvést	k5eAaPmAgMnS
se	s	k7c7
přednáškou	přednáška	k1gFnSc7
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yQgFnSc6,k3yIgFnSc6,k3yRgFnSc6
přečetl	přečíst	k5eAaPmAgMnS
úvod	úvod	k1gInSc4
ke	k	k7c3
své	svůj	k3xOyFgFnSc3
knize	kniha	k1gFnSc3
„	„	k?
<g/>
Egypt	Egypt	k1gInSc1
za	za	k7c2
vlády	vláda	k1gFnSc2
faraónů	faraón	k1gMnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
období	období	k1gNnSc6
vlády	vláda	k1gFnSc2
císaře	císař	k1gMnSc2
Napoleona	Napoleon	k1gMnSc2
však	však	k8xC
byl	být	k5eAaImAgMnS
zastáncem	zastánce	k1gMnSc7
Napoleonovi	Napoleon	k1gMnSc3
nebezpečných	bezpečný	k2eNgFnPc2d1
republikánských	republikánský	k2eAgFnPc2d1
myšlenek	myšlenka	k1gFnPc2
a	a	k8xC
později	pozdě	k6eAd2
vystupoval	vystupovat	k5eAaImAgMnS
i	i	k9
proti	proti	k7c3
Ludvíkovi	Ludvík	k1gMnSc3
XVIII	XVIII	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
mu	on	k3xPp3gMnSc3
vyneslo	vynést	k5eAaPmAgNnS
snížení	snížení	k1gNnSc1
platu	plat	k1gInSc2
a	a	k8xC
zákaz	zákaz	k1gInSc4
přednášek	přednáška	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
Napoleon	Napoleon	k1gMnSc1
vrátil	vrátit	k5eAaPmAgMnS
z	z	k7c2
vyhnanství	vyhnanství	k1gNnSc2
na	na	k7c6
Elbě	Elba	k1gFnSc6
<g/>
,	,	kIx,
Champollion	Champollion	k1gInSc1
se	se	k3xPyFc4
s	s	k7c7
ním	on	k3xPp3gMnSc7
setkal	setkat	k5eAaPmAgMnS
a	a	k8xC
představil	představit	k5eAaPmAgMnS
mu	on	k3xPp3gMnSc3
koptský	koptský	k2eAgInSc4d1
slovník	slovník	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
pracoval	pracovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
definitivní	definitivní	k2eAgFnSc6d1
porážce	porážka	k1gFnSc6
Napoleona	Napoleon	k1gMnSc2
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Waterloo	Waterloo	k1gNnSc2
roku	rok	k1gInSc2
1815	#num#	k4
byl	být	k5eAaImAgInS
Champollion	Champollion	k1gInSc1
označen	označit	k5eAaPmNgInS
za	za	k7c4
velezrádce	velezrádce	k1gMnPc4
a	a	k8xC
hrozilo	hrozit	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
odeslán	odeslat	k5eAaPmNgInS
na	na	k7c4
galeje	galej	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
létě	léto	k1gNnSc6
roku	rok	k1gInSc2
1821	#num#	k4
odešel	odejít	k5eAaPmAgMnS
z	z	k7c2
Grenoblu	Grenoble	k1gInSc2
<g/>
,	,	kIx,
později	pozdě	k6eAd2
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgInS
zpět	zpět	k6eAd1
domů	dům	k1gInPc2
a	a	k8xC
plánoval	plánovat	k5eAaImAgMnS
odejít	odejít	k5eAaPmF
do	do	k7c2
Egypta	Egypt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Rozluštění	rozluštění	k1gNnSc1
egyptských	egyptský	k2eAgInPc2d1
hieroglyfů	hieroglyf	k1gInPc2
</s>
<s>
Detail	detail	k1gInSc1
Rossetské	Rossetský	k2eAgFnSc2d1
desky	deska	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Champollion	Champollion	k1gInSc1
začal	začít	k5eAaPmAgInS
studovat	studovat	k5eAaImF
nejdříve	dříve	k6eAd3
démotické	démotický	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
<g/>
,	,	kIx,
a	a	k8xC
potom	potom	k6eAd1
hieroglyfy	hieroglyf	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zkoumal	zkoumat	k5eAaImAgMnS
a	a	k8xC
vyluštil	vyluštit	k5eAaPmAgMnS
deset	deset	k4xCc4
písmen	písmeno	k1gNnPc2
egyptské	egyptský	k2eAgFnSc2d1
abecedy	abeceda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Souběžně	souběžně	k6eAd1
s	s	k7c7
ním	on	k3xPp3gMnSc7
učinil	učinit	k5eAaPmAgInS,k5eAaImAgInS
v	v	k7c6
tomto	tento	k3xDgInSc6
směru	směr	k1gInSc6
velký	velký	k2eAgInSc1d1
objev	objev	k1gInSc1
také	také	k9
britský	britský	k2eAgMnSc1d1
přírodovědec	přírodovědec	k1gMnSc1
Thomas	Thomas	k1gMnSc1
Young	Young	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
rozluštil	rozluštit	k5eAaPmAgInS
5	#num#	k4
hieroglyfických	hieroglyfický	k2eAgInPc2d1
znaků	znak	k1gInPc2
a	a	k8xC
určil	určit	k5eAaPmAgInS
jejich	jejich	k3xOp3gFnSc4
fonetickou	fonetický	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
<g/>
;	;	kIx,
dále	daleko	k6eAd2
objevil	objevit	k5eAaPmAgMnS
dva	dva	k4xCgInPc4
zvláštní	zvláštní	k2eAgInPc4d1
znaky	znak	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
v	v	k7c6
démotickém	démotický	k2eAgInSc6d1
textu	text	k1gInSc6
označovaly	označovat	k5eAaImAgInP
začátek	začátek	k1gInSc4
a	a	k8xC
konec	konec	k1gInSc4
vlastního	vlastní	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
a	a	k8xC
potvrdil	potvrdit	k5eAaPmAgMnS
domněnku	domněnka	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
staroegyptském	staroegyptský	k2eAgNnSc6d1
písmu	písmo	k1gNnSc6
se	se	k3xPyFc4
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
vlastních	vlastní	k2eAgNnPc2d1
jmen	jméno	k1gNnPc2
nevyskytují	vyskytovat	k5eNaImIp3nP
samohlásky	samohláska	k1gFnPc1
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
v	v	k7c6
arabštině	arabština	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Champollionovi	Champollion	k1gMnSc3
se	se	k3xPyFc4
však	však	k9
podařilo	podařit	k5eAaPmAgNnS
rozluštit	rozluštit	k5eAaPmF
celý	celý	k2eAgInSc4d1
systém	systém	k1gInSc4
egyptského	egyptský	k2eAgNnSc2d1
písma	písmo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odhalil	odhalit	k5eAaPmAgMnS
vztahy	vztah	k1gInPc4
mezi	mezi	k7c7
hieroglyfickým	hieroglyfický	k2eAgNnSc7d1
a	a	k8xC
démotickým	démotický	k2eAgNnSc7d1
písmem	písmo	k1gNnSc7
a	a	k8xC
zároveň	zároveň	k6eAd1
vztahy	vztah	k1gInPc4
obou	dva	k4xCgFnPc6
těchto	tento	k3xDgNnPc2
písem	písmo	k1gNnPc2
k	k	k7c3
třetímu	třetí	k4xOgMnSc3
–	–	k?
hieratickému	hieratický	k2eAgInSc3d1
<g/>
,	,	kIx,
tj.	tj.	kA
kněžskému	kněžský	k2eAgMnSc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
23	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
1822	#num#	k4
přednesl	přednést	k5eAaPmAgInS
Champollion	Champollion	k1gInSc1
svůj	svůj	k3xOyFgInSc4
výklad	výklad	k1gInSc4
a	a	k8xC
zároveň	zároveň	k6eAd1
důkaz	důkaz	k1gInSc4
jeho	jeho	k3xOp3gFnSc2
správnosti	správnost	k1gFnSc2
před	před	k7c7
členy	člen	k1gMnPc7
akademie	akademie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
Rosettské	rosettský	k2eAgFnSc6d1
desce	deska	k1gFnSc6
se	se	k3xPyFc4
zachovalo	zachovat	k5eAaPmAgNnS
celkem	celkem	k6eAd1
1419	#num#	k4
hieroglyfů	hieroglyf	k1gInPc2
<g/>
,	,	kIx,
kterým	který	k3yQgMnSc7,k3yIgMnSc7,k3yRgMnSc7
odpovídá	odpovídat	k5eAaImIp3nS
486	#num#	k4
řeckých	řecký	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
<g/>
,	,	kIx,
každé	každý	k3xTgNnSc1
slovo	slovo	k1gNnSc1
se	se	k3xPyFc4
tedy	tedy	k9
píše	psát	k5eAaImIp3nS
průměrně	průměrně	k6eAd1
třemi	tři	k4xCgInPc7
znaky	znak	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jména	jméno	k1gNnPc4
králů	král	k1gMnPc2
psali	psát	k5eAaImAgMnP
Egypťané	Egypťan	k1gMnPc1
zřejmě	zřejmě	k6eAd1
na	na	k7c4
znak	znak	k1gInSc4
úcty	úcta	k1gFnSc2
do	do	k7c2
oválných	oválný	k2eAgInPc2d1
rámečků	rámeček	k1gInPc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
kartuší	kartuš	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
znakům	znak	k1gInPc3
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
neměly	mít	k5eNaImAgInP
fonetickou	fonetický	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
přidával	přidávat	k5eAaImAgInS
vysvětlující	vysvětlující	k2eAgInSc1d1
znak	znak	k1gInSc1
–	–	k?
determinativ	determinativ	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samohlásky	samohláska	k1gFnPc4
Egypťané	Egypťan	k1gMnPc1
pravděpodobně	pravděpodobně	k6eAd1
nepoužívali	používat	k5eNaImAgMnP
kvůli	kvůli	k7c3
nářečním	nářeční	k2eAgInPc3d1
rozdílům	rozdíl	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyskytovaly	vyskytovat	k5eAaImAgInP
se	se	k3xPyFc4
však	však	k9
i	i	k9
kombinace	kombinace	k1gFnSc1
jednotlivých	jednotlivý	k2eAgInPc2d1
znaků	znak	k1gInPc2
<g/>
,	,	kIx,
např.	např.	kA
„	„	k?
<g/>
j	j	k?
<g/>
“	“	k?
se	se	k3xPyFc4
označovalo	označovat	k5eAaImAgNnS
obrázkem	obrázek	k1gInSc7
třtiny	třtina	k1gFnSc2
<g/>
,	,	kIx,
spojilo	spojit	k5eAaPmAgNnS
se	se	k3xPyFc4
s	s	k7c7
determinativem	determinativ	k1gInSc7
označujícím	označující	k2eAgInSc7d1
chůzi	chůze	k1gFnSc4
(	(	kIx(
<g/>
nohy	noha	k1gFnPc4
<g/>
)	)	kIx)
a	a	k8xC
vznikl	vzniknout	k5eAaPmAgInS
ideogram	ideogram	k1gInSc4
vyjadřující	vyjadřující	k2eAgFnSc2d1
„	„	k?
<g/>
chodit	chodit	k5eAaImF
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
směru	směr	k1gInSc2
čtení	čtení	k1gNnSc2
znaků	znak	k1gInPc2
<g/>
,	,	kIx,
hieroglyfy	hieroglyf	k1gInPc1
se	se	k3xPyFc4
čtou	číst	k5eAaImIp3nP
zprava	zprava	k6eAd1
nebo	nebo	k8xC
zleva	zleva	k6eAd1
podle	podle	k7c2
orientace	orientace	k1gFnSc2
určitých	určitý	k2eAgInPc2d1
symbolů	symbol	k1gInPc2
<g/>
,	,	kIx,
např.	např.	kA
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
se	se	k3xPyFc4
dívá	dívat	k5eAaImIp3nS
sova	sova	k1gFnSc1
(	(	kIx(
<g/>
písmeno	písmeno	k1gNnSc1
„	„	k?
<g/>
m	m	kA
<g/>
“	“	k?
<g/>
)	)	kIx)
–	–	k?
čte	číst	k5eAaImIp3nS
se	se	k3xPyFc4
proti	proti	k7c3
směru	směr	k1gInSc3
jejího	její	k3xOp3gInSc2
pohledu	pohled	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Datum	datum	k1gNnSc1
Champollionovy	Champollionův	k2eAgFnSc2d1
přednášky	přednáška	k1gFnSc2
se	se	k3xPyFc4
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
den	den	k1gInSc4
zrodu	zrod	k1gInSc2
egyptologie	egyptologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
roku	rok	k1gInSc2
1828	#num#	k4
odjel	odjet	k5eAaPmAgInS
Champollion	Champollion	k1gInSc1
do	do	k7c2
Egypta	Egypt	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
objevil	objevit	k5eAaPmAgInS
starý	starý	k2eAgInSc1d1
chrám	chrám	k1gInSc1
zasvěcený	zasvěcený	k2eAgInSc1d1
bohyni	bohyně	k1gFnSc4
Eset	Eset	k1gInSc4
na	na	k7c6
ostrově	ostrov	k1gInSc6
Fílé	Fílá	k1gFnSc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
pokračoval	pokračovat	k5eAaImAgInS
do	do	k7c2
Karnaku	Karnak	k1gInSc2
a	a	k8xC
do	do	k7c2
Théb	Théby	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
dva	dva	k4xCgInPc4
a	a	k8xC
půl	půl	k1xP
roku	rok	k1gInSc2
později	pozdě	k6eAd2
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
Francie	Francie	k1gFnSc2
<g/>
;	;	kIx,
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
Paříži	Paříž	k1gFnSc6
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1832	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Jean-François	Jean-François	k1gFnSc1
Champollion	Champollion	k1gInSc1
<g/>
:	:	kIx,
Précis	Précis	k1gFnSc1
du	du	k?
systè	systè	k5eAaPmIp3nS
hiéroglyphique	hiéroglyphique	k6eAd1
des	des	k1gNnSc1
anciens	anciensa	k1gFnPc2
Égyptiens	Égyptiensa	k1gFnPc2
<g/>
,	,	kIx,
Treuttel	Treuttela	k1gFnPc2
et	et	k?
Würtz	Würtz	k1gMnSc1
<g/>
,	,	kIx,
Paříž	Paříž	k1gFnSc1
1824	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
BOPP	BOPP	kA
<g/>
,	,	kIx,
Franz	Franz	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Compaative	Compaativ	k1gInSc5
Grammar	Grammara	k1gFnPc2
Sanscrit	Sanscrit	k1gInSc1
<g/>
,	,	kIx,
Zend	Zend	k1gMnSc1
<g/>
,	,	kIx,
Latin	Latin	k1gMnSc1
<g/>
,	,	kIx,
German	German	k1gMnSc1
and	and	k?
Slavonic	Slavonice	k1gFnPc2
Languages	Languages	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
London	London	k1gMnSc1
<g/>
:	:	kIx,
Madden	Maddna	k1gFnPc2
and	and	k?
Malcolom	Malcolom	k1gInSc1
<g/>
,	,	kIx,
1845	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Přepis	přepis	k1gInSc1
egyptských	egyptský	k2eAgInPc2d1
hieroglyfů	hieroglyf	k1gInPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Jean-François	Jean-François	k1gFnSc2
Champollion	Champollion	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Autor	autor	k1gMnSc1
Jean-François	Jean-François	k1gFnSc2
Champollion	Champollion	k1gInSc1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Jean-François	Jean-François	k1gFnSc1
Champollion	Champollion	k1gInSc4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
20000700754	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118746219	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
0886	#num#	k4
1508	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79145670	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
34454460	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79145670	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Francie	Francie	k1gFnSc1
|	|	kIx~
Jazyk	jazyk	k1gInSc1
</s>
