<s>
Usain	Usain	k1gMnSc1	Usain
St.	st.	kA	st.
Leo	Leo	k1gMnSc1	Leo
Bolt	Bolt	k1gMnSc1	Bolt
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jamajský	jamajský	k2eAgMnSc1d1	jamajský
sportovec	sportovec	k1gMnSc1	sportovec
<g/>
,	,	kIx,	,
atlet-sprinter	atletprinter	k1gMnSc1	atlet-sprinter
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
držitel	držitel	k1gMnSc1	držitel
tří	tři	k4xCgInPc2	tři
atletických	atletický	k2eAgInPc2d1	atletický
světových	světový	k2eAgInPc2d1	světový
rekordů	rekord	k1gInPc2	rekord
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
s	s	k7c7	s
časem	čas	k1gInSc7	čas
9,58	[number]	k4	9,58
s	s	k7c7	s
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zaběhl	zaběhnout	k5eAaPmAgMnS	zaběhnout
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
běhu	běh	k1gInSc2	běh
na	na	k7c4	na
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
s	s	k7c7	s
časem	čas	k1gInSc7	čas
19,19	[number]	k4	19,19
s	s	k7c7	s
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2009	[number]	k4	2009
a	a	k8xC	a
ve	v	k7c6	v
štafetě	štafeta	k1gFnSc6	štafeta
na	na	k7c4	na
4	[number]	k4	4
x	x	k?	x
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
časem	časem	k6eAd1	časem
36,84	[number]	k4	36,84
s	s	k7c7	s
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zaběhl	zaběhnout	k5eAaPmAgMnS	zaběhnout
jamajským	jamajský	k2eAgMnSc7d1	jamajský
tým	tým	k1gInSc1	tým
na	na	k7c4	na
LOH	LOH	kA	LOH
2012	[number]	k4	2012
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Drží	držet	k5eAaImIp3nS	držet
také	také	k9	také
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c6	na
neoficiální	neoficiální	k2eAgFnSc6d1	neoficiální
trati	trať	k1gFnSc6	trať
150	[number]	k4	150
metrů	metr	k1gInPc2	metr
časem	časem	k6eAd1	časem
14,35	[number]	k4	14,35
s.	s.	k?	s.
z	z	k7c2	z
května	květen	k1gInSc2	květen
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Usain	Usain	k2eAgMnSc1d1	Usain
Bolt	Bolt	k1gMnSc1	Bolt
je	být	k5eAaImIp3nS	být
prvním	první	k4xOgMnSc7	první
člověkem	člověk	k1gMnSc7	člověk
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dokázal	dokázat	k5eAaPmAgMnS	dokázat
zaběhnout	zaběhnout	k5eAaPmF	zaběhnout
nejkratší	krátký	k2eAgFnSc4d3	nejkratší
trať	trať	k1gFnSc4	trať
regulérně	regulérně	k6eAd1	regulérně
pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
9,70	[number]	k4	9,70
sekund	sekunda	k1gFnPc2	sekunda
i	i	k8xC	i
9,60	[number]	k4	9,60
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
atletickém	atletický	k2eAgInSc6d1	atletický
MS	MS	kA	MS
2009	[number]	k4	2009
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
byla	být	k5eAaImAgFnS	být
provedena	proveden	k2eAgFnSc1d1	provedena
vědecká	vědecký	k2eAgFnSc1d1	vědecká
analýza	analýza	k1gFnSc1	analýza
Boltova	Boltův	k2eAgInSc2d1	Boltův
běhu	běh	k1gInSc2	běh
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ukázala	ukázat	k5eAaPmAgFnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
stometrové	stometrový	k2eAgFnSc2d1	stometrová
trati	trať	k1gFnSc2	trať
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
maximální	maximální	k2eAgFnPc4d1	maximální
rychlosti	rychlost	k1gFnPc4	rychlost
44,72	[number]	k4	44,72
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
(	(	kIx(	(
<g/>
letmých	letmý	k2eAgFnPc2d1	letmá
20	[number]	k4	20
metrů	metr	k1gInPc2	metr
za	za	k7c4	za
1,61	[number]	k4	1,61
s.	s.	k?	s.
<g/>
)	)	kIx)	)
a	a	k8xC	a
průměrné	průměrný	k2eAgFnSc3d1	průměrná
rychlosti	rychlost	k1gFnSc3	rychlost
37,58	[number]	k4	37,58
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
analýzou	analýza	k1gFnSc7	analýza
na	na	k7c4	na
OH	OH	kA	OH
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
spekulace	spekulace	k1gFnPc1	spekulace
o	o	k7c6	o
možném	možný	k2eAgInSc6d1	možný
Boltově	Boltův	k2eAgInSc6d1	Boltův
času	čas	k1gInSc6	čas
na	na	k7c4	na
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
závěr	závěr	k1gInSc1	závěr
stovky	stovka	k1gFnSc2	stovka
nevypustil	vypustit	k5eNaPmAgInS	vypustit
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
odhady	odhad	k1gInPc1	odhad
hovořily	hovořit	k5eAaImAgInP	hovořit
již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
o	o	k7c6	o
čase	čas	k1gInSc6	čas
9,64	[number]	k4	9,64
s.	s.	k?	s.
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc4	ten
však	však	k9	však
Bolt	Bolt	k1gMnSc1	Bolt
výrazně	výrazně	k6eAd1	výrazně
překonal	překonat	k5eAaPmAgMnS	překonat
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Bolt	Bolt	k1gMnSc1	Bolt
dal	dát	k5eAaPmAgMnS	dát
na	na	k7c6	na
zmíněném	zmíněný	k2eAgNnSc6d1	zmíněné
MS	MS	kA	MS
v	v	k7c6	v
atletice	atletika	k1gFnSc6	atletika
2009	[number]	k4	2009
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
odpověď	odpověď	k1gFnSc4	odpověď
<g/>
,	,	kIx,	,
když	když	k8xS	když
jeho	jeho	k3xOp3gInSc7	jeho
průběhem	průběh	k1gInSc7	průběh
cíle	cíl	k1gInSc2	cíl
stometrové	stometrový	k2eAgFnSc2d1	stometrová
tratě	trať	k1gFnSc2	trať
byly	být	k5eAaImAgFnP	být
výrazně	výrazně	k6eAd1	výrazně
překonány	překonán	k2eAgFnPc1d1	překonána
dosavadní	dosavadní	k2eAgFnPc1d1	dosavadní
předpokládané	předpokládaný	k2eAgFnPc1d1	předpokládaná
hranice	hranice	k1gFnPc1	hranice
lidských	lidský	k2eAgFnPc2d1	lidská
možností	možnost	k1gFnPc2	možnost
časem	čas	k1gInSc7	čas
9,58	[number]	k4	9,58
s	s	k7c7	s
(	(	kIx(	(
<g/>
překonal	překonat	k5eAaPmAgMnS	překonat
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
rekord	rekord	k1gInSc4	rekord
o	o	k7c4	o
0,11	[number]	k4	0,11
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
také	také	k9	také
velmi	velmi	k6eAd1	velmi
výrazného	výrazný	k2eAgInSc2d1	výrazný
výkonu	výkon	k1gInSc2	výkon
<g/>
,	,	kIx,	,
když	když	k8xS	když
již	již	k6eAd1	již
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
překonal	překonat	k5eAaPmAgMnS	překonat
o	o	k7c4	o
0,02	[number]	k4	0,02
s	s	k7c7	s
famózní	famózní	k2eAgFnSc7d1	famózní
rekord	rekord	k1gInSc1	rekord
Michaela	Michael	k1gMnSc2	Michael
Johnsona	Johnson	k1gMnSc2	Johnson
z	z	k7c2	z
finále	finále	k1gNnSc2	finále
OH	OH	kA	OH
v	v	k7c6	v
Atlantě	Atlanta	k1gFnSc6	Atlanta
1996	[number]	k4	1996
časem	čas	k1gInSc7	čas
19,30	[number]	k4	19,30
s	s	k7c7	s
při	při	k7c6	při
průměrné	průměrný	k2eAgFnSc6d1	průměrná
rychlosti	rychlost	k1gFnSc6	rychlost
37,30	[number]	k4	37,30
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Na	na	k7c4	na
MS	MS	kA	MS
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
ubral	ubrat	k5eAaPmAgMnS	ubrat
ještě	ještě	k9	ještě
dalších	další	k2eAgInPc2d1	další
0,11	[number]	k4	0,11
s	s	k7c7	s
a	a	k8xC	a
famózním	famózní	k2eAgInSc7d1	famózní
časem	čas	k1gInSc7	čas
19,19	[number]	k4	19,19
s	s	k7c7	s
opět	opět	k6eAd1	opět
překonal	překonat	k5eAaPmAgMnS	překonat
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
(	(	kIx(	(
<g/>
přestože	přestože	k8xS	přestože
běžel	běžet	k5eAaImAgMnS	běžet
do	do	k7c2	do
protivětru	protivítr	k1gInSc2	protivítr
-0,3	-0,3	k4	-0,3
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
;	;	kIx,	;
průměrná	průměrný	k2eAgFnSc1d1	průměrná
rychlost	rychlost	k1gFnSc1	rychlost
činí	činit	k5eAaImIp3nS	činit
37,52	[number]	k4	37,52
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
stovku	stovka	k1gFnSc4	stovka
běžel	běžet	k5eAaImAgMnS	běžet
Bolt	Bolt	k1gMnSc1	Bolt
za	za	k7c4	za
9,92	[number]	k4	9,92
s.	s.	k?	s.
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc7	druhý
údajně	údajně	k6eAd1	údajně
za	za	k7c2	za
9,27	[number]	k4	9,27
s.	s.	k?	s.
Všechny	všechen	k3xTgInPc1	všechen
ostatní	ostatní	k2eAgInPc1d1	ostatní
výkony	výkon	k1gInPc1	výkon
jsou	být	k5eAaImIp3nP	být
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
o	o	k7c4	o
zhruba	zhruba	k6eAd1	zhruba
0,4	[number]	k4	0,4
sekundy	sekunda	k1gFnSc2	sekunda
pomalejší	pomalý	k2eAgFnSc2d2	pomalejší
(	(	kIx(	(
<g/>
19,58	[number]	k4	19,58
s	s	k7c7	s
a	a	k8xC	a
výše	vysoce	k6eAd2	vysoce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jamajská	jamajský	k2eAgFnSc1d1	jamajská
štafeta	štafeta	k1gFnSc1	štafeta
s	s	k7c7	s
Boltem	Bolt	k1gMnSc7	Bolt
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
úseku	úsek	k1gInSc6	úsek
na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
překonala	překonat	k5eAaPmAgFnS	překonat
předchozí	předchozí	k2eAgInSc4d1	předchozí
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
také	také	k9	také
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
štafetového	štafetový	k2eAgInSc2d1	štafetový
běhu	běh	k1gInSc2	běh
4	[number]	k4	4
x	x	k?	x
100	[number]	k4	100
m	m	kA	m
časem	čas	k1gInSc7	čas
37,10	[number]	k4	37,10
s.	s.	k?	s.
Bolt	Bolt	k1gInSc1	Bolt
výrazně	výrazně	k6eAd1	výrazně
přispěl	přispět	k5eAaPmAgInS	přispět
nejrychlejším	rychlý	k2eAgInSc7d3	nejrychlejší
časem	čas	k1gInSc7	čas
na	na	k7c6	na
svém	své	k1gNnSc6	své
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
úseku	úsek	k1gInSc2	úsek
za	za	k7c4	za
8,94	[number]	k4	8,94
s.	s.	k?	s.
Bolt	Bolt	k1gMnSc1	Bolt
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejmladších	mladý	k2eAgMnPc2d3	nejmladší
světových	světový	k2eAgMnPc2d1	světový
rekordmanů	rekordman	k1gMnPc2	rekordman
na	na	k7c6	na
těchto	tento	k3xDgFnPc6	tento
tratích	trať	k1gFnPc6	trať
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jejich	jejich	k3xOp3gNnSc6	jejich
vytvoření	vytvoření	k1gNnSc6	vytvoření
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
mu	on	k3xPp3gMnSc3	on
ještě	ještě	k9	ještě
nebylo	být	k5eNaImAgNnS	být
ani	ani	k9	ani
22	[number]	k4	22
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
štafety	štafeta	k1gFnSc2	štafeta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
Bolt	Bolt	k1gMnSc1	Bolt
havaroval	havarovat	k5eAaPmAgMnS	havarovat
se	s	k7c7	s
svým	svůj	k1gMnSc7	svůj
BMW	BMW	kA	BMW
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nic	nic	k3yNnSc1	nic
vážného	vážný	k2eAgNnSc2d1	vážné
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
nestalo	stát	k5eNaPmAgNnS	stát
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
ošetření	ošetření	k1gNnSc4	ošetření
v	v	k7c6	v
místní	místní	k2eAgFnSc6d1	místní
nemocnici	nemocnice	k1gFnSc6	nemocnice
propuštěn	propuštěn	k2eAgMnSc1d1	propuštěn
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
však	však	k9	však
znovu	znovu	k6eAd1	znovu
představil	představit	k5eAaPmAgMnS	představit
v	v	k7c6	v
oslnivé	oslnivý	k2eAgFnSc6d1	oslnivá
formě	forma	k1gFnSc6	forma
a	a	k8xC	a
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
Manchesteru	Manchester	k1gInSc6	Manchester
zaběhl	zaběhnout	k5eAaPmAgMnS	zaběhnout
opět	opět	k6eAd1	opět
nejrychlejší	rychlý	k2eAgInSc4d3	nejrychlejší
čas	čas	k1gInSc4	čas
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
na	na	k7c6	na
méně	málo	k6eAd2	málo
vypisované	vypisovaný	k2eAgFnSc6d1	vypisovaná
trati	trať	k1gFnSc6	trať
150	[number]	k4	150
metrů	metr	k1gInPc2	metr
časem	časem	k6eAd1	časem
14,35	[number]	k4	14,35
s.	s.	k?	s.
Dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
pak	pak	k6eAd1	pak
zaběhl	zaběhnout	k5eAaPmAgMnS	zaběhnout
při	při	k7c6	při
mítinku	mítink	k1gInSc6	mítink
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
tretra	tretra	k1gFnSc1	tretra
Ostrava	Ostrava	k1gFnSc1	Ostrava
skvělý	skvělý	k2eAgInSc4d1	skvělý
čas	čas	k1gInSc4	čas
9,77	[number]	k4	9,77
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
<g />
.	.	kIx.	.
</s>
<s>
však	však	k9	však
byl	být	k5eAaImAgInS	být
znehodnocen	znehodnocen	k2eAgInSc1d1	znehodnocen
větrem	vítr	k1gInSc7	vítr
v	v	k7c4	v
zádech	zádech	k1gInSc4	zádech
těsně	těsně	k6eAd1	těsně
nad	nad	k7c7	nad
povolenou	povolený	k2eAgFnSc7d1	povolená
hodnotou	hodnota	k1gFnSc7	hodnota
-	-	kIx~	-
2,1	[number]	k4	2,1
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
na	na	k7c6	na
mítinku	mítink	k1gInSc6	mítink
Zlaté	zlatý	k2eAgFnSc2d1	zlatá
Tretry	tretra	k1gFnSc2	tretra
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
stal	stát	k5eAaPmAgMnS	stát
druhým	druhý	k4xOgMnSc7	druhý
člověkem	člověk	k1gMnSc7	člověk
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dokázal	dokázat	k5eAaPmAgMnS	dokázat
běžet	běžet	k5eAaImF	běžet
netradiční	tradiční	k2eNgFnSc4d1	netradiční
trať	trať	k1gFnSc4	trať
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
pod	pod	k7c4	pod
31	[number]	k4	31
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Časem	čas	k1gInSc7	čas
30,97	[number]	k4	30,97
s.	s.	k?	s.
v	v	k7c6	v
těžkém	těžký	k2eAgInSc6d1	těžký
dešti	dešť	k1gInSc6	dešť
zaostal	zaostat	k5eAaPmAgInS	zaostat
jen	jen	k9	jen
o	o	k7c4	o
12	[number]	k4	12
setin	setina	k1gFnPc2	setina
za	za	k7c7	za
světovým	světový	k2eAgInSc7d1	světový
rekordem	rekord	k1gInSc7	rekord
Michaela	Michael	k1gMnSc2	Michael
Johnsona	Johnson	k1gMnSc2	Johnson
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
neobhájil	obhájit	k5eNaPmAgInS	obhájit
na	na	k7c6	na
MS	MS	kA	MS
v	v	k7c6	v
korejském	korejský	k2eAgNnSc6d1	korejské
Tegu	Tegum	k1gNnSc6	Tegum
titul	titul	k1gInSc4	titul
Mistra	mistr	k1gMnSc2	mistr
světa	svět	k1gInSc2	svět
na	na	k7c4	na
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
diskvalifikace	diskvalifikace	k1gFnSc2	diskvalifikace
po	po	k7c6	po
brzkém	brzký	k2eAgInSc6d1	brzký
startu	start	k1gInSc6	start
z	z	k7c2	z
bloků	blok	k1gInPc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
trati	trať	k1gFnSc6	trať
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
však	však	k9	však
s	s	k7c7	s
převahou	převaha	k1gFnSc7	převaha
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
v	v	k7c6	v
pátém	pátý	k4xOgInSc6	pátý
nejrychlejším	rychlý	k2eAgInSc6d3	nejrychlejší
čase	čas	k1gInSc6	čas
historie	historie	k1gFnSc2	historie
19,40	[number]	k4	19,40
s.	s.	k?	s.
Běžel	běžet	k5eAaImAgInS	běžet
tak	tak	k9	tak
již	již	k9	již
potřetí	potřetí	k4xO	potřetí
regulérně	regulérně	k6eAd1	regulérně
pod	pod	k7c7	pod
19,5	[number]	k4	19,5
sekundy	sekund	k1gInPc7	sekund
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
výjimkami	výjimka	k1gFnPc7	výjimka
(	(	kIx(	(
<g/>
Michael	Michael	k1gMnSc1	Michael
Johnson	Johnson	k1gMnSc1	Johnson
v	v	k7c6	v
Atlantě	Atlanta	k1gFnSc6	Atlanta
1996	[number]	k4	1996
a	a	k8xC	a
Yohan	Yohan	k1gInSc1	Yohan
Blake	Blak	k1gFnSc2	Blak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
nepovedlo	povést	k5eNaPmAgNnS	povést
nikomu	nikdo	k3yNnSc3	nikdo
jinému	jiný	k2eAgMnSc3d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
témže	týž	k3xTgInSc6	týž
šampionátu	šampionát	k1gInSc6	šampionát
dovedl	dovést	k5eAaPmAgMnS	dovést
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
úseku	úsek	k1gInSc6	úsek
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
4	[number]	k4	4
<g/>
x	x	k?	x
<g/>
100	[number]	k4	100
<g/>
m	m	kA	m
jamajskou	jamajský	k2eAgFnSc4d1	jamajská
štafetu	štafeta	k1gFnSc4	štafeta
k	k	k7c3	k
novému	nový	k2eAgInSc3d1	nový
světovému	světový	k2eAgInSc3d1	světový
rekordu	rekord	k1gInSc3	rekord
37,04	[number]	k4	37,04
s.	s.	k?	s.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Diamantovou	diamantový	k2eAgFnSc4d1	Diamantová
ligu	liga	k1gFnSc4	liga
na	na	k7c4	na
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
na	na	k7c6	na
dvojnásobné	dvojnásobný	k2eAgFnSc6d1	dvojnásobná
trati	trať	k1gFnSc6	trať
mu	on	k3xPp3gMnSc3	on
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
chyběl	chybět	k5eAaImAgInS	chybět
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Bolt	Bolt	k1gMnSc1	Bolt
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
také	také	k9	také
na	na	k7c6	na
MS	MS	kA	MS
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
porazil	porazit	k5eAaPmAgMnS	porazit
svého	svůj	k3xOyFgMnSc4	svůj
velkého	velký	k2eAgMnSc4d1	velký
soupeře	soupeř	k1gMnSc4	soupeř
Justina	Justin	k1gMnSc4	Justin
Gatlina	Gatlin	k2eAgMnSc4d1	Gatlin
v	v	k7c6	v
bězích	běh	k1gInPc6	běh
na	na	k7c4	na
100	[number]	k4	100
i	i	k9	i
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
získal	získat	k5eAaPmAgMnS	získat
další	další	k2eAgFnPc4d1	další
3	[number]	k4	3
zlaté	zlatá	k1gFnPc4	zlatá
olympijské	olympijský	k2eAgFnSc2d1	olympijská
medaile	medaile	k1gFnSc2	medaile
(	(	kIx(	(
<g/>
běh	běh	k1gInSc1	běh
na	na	k7c4	na
100	[number]	k4	100
m	m	kA	m
<g/>
,	,	kIx,	,
běh	běh	k1gInSc1	běh
na	na	k7c4	na
200	[number]	k4	200
m	m	kA	m
a	a	k8xC	a
štafeta	štafeta	k1gFnSc1	štafeta
4	[number]	k4	4
<g/>
x	x	k?	x
<g/>
100	[number]	k4	100
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
již	již	k9	již
na	na	k7c6	na
třetí	třetí	k4xOgFnSc6	třetí
olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
trojnásobným	trojnásobný	k2eAgInSc7d1	trojnásobný
zlatým	zlatý	k2eAgInSc7d1	zlatý
hattrickem	hattrick	k1gInSc7	hattrick
a	a	k8xC	a
světovými	světový	k2eAgInPc7d1	světový
rekordy	rekord	k1gInPc7	rekord
na	na	k7c4	na
100	[number]	k4	100
a	a	k8xC	a
200	[number]	k4	200
m	m	kA	m
se	se	k3xPyFc4	se
navždy	navždy	k6eAd1	navždy
zapsal	zapsat	k5eAaPmAgMnS	zapsat
do	do	k7c2	do
historie	historie	k1gFnSc2	historie
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
atletů	atlet	k1gMnPc2	atlet
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Bolt	Bolt	k1gMnSc1	Bolt
je	být	k5eAaImIp3nS	být
nejrychlejším	rychlý	k2eAgMnSc7d3	nejrychlejší
člověkem	člověk	k1gMnSc7	člověk
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
změřena	změřen	k2eAgFnSc1d1	změřena
maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
44,72	[number]	k4	44,72
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
světových	světový	k2eAgInPc2d1	světový
rekordů	rekord	k1gInPc2	rekord
v	v	k7c6	v
nejkratších	krátký	k2eAgInPc6d3	nejkratší
sprintech	sprint	k1gInPc6	sprint
i	i	k8xC	i
na	na	k7c6	na
posledním	poslední	k2eAgInSc6d1	poslední
úseku	úsek	k1gInSc6	úsek
štafety	štafeta	k1gFnSc2	štafeta
na	na	k7c4	na
4	[number]	k4	4
x	x	k?	x
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
a	a	k8xC	a
2012	[number]	k4	2012
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vítězem	vítěz	k1gMnSc7	vítěz
ankety	anketa	k1gFnSc2	anketa
Atlet	atlet	k1gMnSc1	atlet
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
a	a	k8xC	a
2016	[number]	k4	2016
byl	být	k5eAaImAgMnS	být
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
sportovcem	sportovec	k1gMnSc7	sportovec
světa	svět	k1gInSc2	svět
za	za	k7c4	za
uplynulý	uplynulý	k2eAgInSc4d1	uplynulý
rok	rok	k1gInSc4	rok
a	a	k8xC	a
převzal	převzít	k5eAaPmAgInS	převzít
světovou	světový	k2eAgFnSc4d1	světová
sportovní	sportovní	k2eAgFnSc4d1	sportovní
cenu	cena	k1gFnSc4	cena
Laureus	Laureus	k1gMnSc1	Laureus
<g/>
.	.	kIx.	.
</s>
<s>
Běh	běh	k1gInSc1	běh
na	na	k7c4	na
100	[number]	k4	100
m	m	kA	m
9,58	[number]	k4	9,58
s	s	k7c7	s
(	(	kIx(	(
<g/>
SR	SR	kA	SR
<g/>
,	,	kIx,	,
MS	MS	kA	MS
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
<g/>
)	)	kIx)	)
Běh	běh	k1gInSc1	běh
na	na	k7c4	na
150	[number]	k4	150
metrů	metr	k1gInPc2	metr
(	(	kIx(	(
<g/>
neoficiální	oficiální	k2eNgFnSc1d1	neoficiální
trať	trať	k1gFnSc1	trať
<g/>
)	)	kIx)	)
14,35	[number]	k4	14,35
s	s	k7c7	s
(	(	kIx(	(
<g/>
SR	SR	kA	SR
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
Manchester	Manchester	k1gInSc1	Manchester
<g/>
)	)	kIx)	)
Běh	běh	k1gInSc1	běh
na	na	k7c4	na
200	[number]	k4	200
m	m	kA	m
19,19	[number]	k4	19,19
s	s	k7c7	s
(	(	kIx(	(
<g/>
SR	SR	kA	SR
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
MS	MS	kA	MS
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
<g/>
)	)	kIx)	)
Běh	běh	k1gInSc1	běh
na	na	k7c4	na
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
30,97	[number]	k4	30,97
s	s	k7c7	s
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
čas	čas	k1gInSc4	čas
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
Tretra	tretra	k1gFnSc1	tretra
Ostrava	Ostrava	k1gFnSc1	Ostrava
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Běh	běh	k1gInSc1	běh
na	na	k7c4	na
400	[number]	k4	400
m	m	kA	m
45,28	[number]	k4	45,28
s	s	k7c7	s
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Štafeta	štafeta	k1gFnSc1	štafeta
4	[number]	k4	4
x	x	k?	x
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
<g />
.	.	kIx.	.
</s>
<s>
36,84	[number]	k4	36,84
s	s	k7c7	s
(	(	kIx(	(
<g/>
SR	SR	kA	SR
<g/>
,	,	kIx,	,
LOH	LOH	kA	LOH
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
Neoficiálně	oficiálně	k6eNd1	oficiálně
-	-	kIx~	-
letmých	letmý	k2eAgFnPc2d1	letmá
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
za	za	k7c4	za
8,65	[number]	k4	8,65
sekundy	sekunda	k1gFnSc2	sekunda
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
změřená	změřený	k2eAgFnSc1d1	změřená
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
dosažená	dosažený	k2eAgFnSc1d1	dosažená
člověkem	člověk	k1gMnSc7	člověk
(	(	kIx(	(
<g/>
44,72	[number]	k4	44,72
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
;	;	kIx,	;
při	při	k7c6	při
SR	SR	kA	SR
9,58	[number]	k4	9,58
s	s	k7c7	s
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
