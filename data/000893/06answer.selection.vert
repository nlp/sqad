<s>
Usain	Usain	k1gMnSc1	Usain
St.	st.	kA	st.
Leo	Leo	k1gMnSc1	Leo
Bolt	Bolt	k1gMnSc1	Bolt
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jamajský	jamajský	k2eAgMnSc1d1	jamajský
sportovec	sportovec	k1gMnSc1	sportovec
<g/>
,	,	kIx,	,
atlet-sprinter	atletprinter	k1gMnSc1	atlet-sprinter
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
držitel	držitel	k1gMnSc1	držitel
tří	tři	k4xCgInPc2	tři
atletických	atletický	k2eAgInPc2d1	atletický
světových	světový	k2eAgInPc2d1	světový
rekordů	rekord	k1gInPc2	rekord
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
s	s	k7c7	s
časem	čas	k1gInSc7	čas
9,58	[number]	k4	9,58
s	s	k7c7	s
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zaběhl	zaběhnout	k5eAaPmAgMnS	zaběhnout
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
běhu	běh	k1gInSc2	běh
na	na	k7c4	na
200	[number]	k4	200
metrů	metr	k1gInPc2	metr
s	s	k7c7	s
časem	čas	k1gInSc7	čas
19,19	[number]	k4	19,19
s	s	k7c7	s
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2009	[number]	k4	2009
a	a	k8xC	a
ve	v	k7c6	v
štafetě	štafeta	k1gFnSc6	štafeta
na	na	k7c4	na
4	[number]	k4	4
x	x	k?	x
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
časem	časem	k6eAd1	časem
36,84	[number]	k4	36,84
s	s	k7c7	s
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zaběhl	zaběhnout	k5eAaPmAgMnS	zaběhnout
jamajským	jamajský	k2eAgMnSc7d1	jamajský
tým	tým	k1gInSc1	tým
na	na	k7c4	na
LOH	LOH	kA	LOH
2012	[number]	k4	2012
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
