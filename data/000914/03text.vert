<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
(	(	kIx(	(
<g/>
svátek	svátek	k1gInSc1	svátek
23	[number]	k4	23
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
křestní	křestní	k2eAgNnSc4d1	křestní
mužské	mužský	k2eAgNnSc4d1	mužské
jméno	jméno	k1gNnSc4	jméno
slovanského	slovanský	k2eAgInSc2d1	slovanský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
z	z	k7c2	z
"	"	kIx"	"
<g/>
woj	woj	k?	woj
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
:	:	kIx,	:
wój	wój	k?	wój
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
slovanský	slovanský	k2eAgInSc1d1	slovanský
kořen	kořen	k1gInSc1	kořen
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
tvořící	tvořící	k2eAgNnPc1d1	tvořící
slova	slovo	k1gNnPc1	slovo
jako	jako	k8xS	jako
wojovnik	wojovnik	k1gInSc1	wojovnik
znamenající	znamenající	k2eAgMnSc1d1	znamenající
válečník	válečník	k1gMnSc1	válečník
a	a	k8xC	a
následně	následně	k6eAd1	následně
slovo	slovo	k1gNnSc1	slovo
wojna	wojno	k1gNnSc2	wojno
-	-	kIx~	-
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ciech	Ciech	k1gInSc4	Ciech
(	(	kIx(	(
<g/>
z	z	k7c2	z
dřívějšího	dřívější	k2eAgInSc2d1	dřívější
tvaru	tvar	k1gInSc2	tvar
tech	tech	k?	tech
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znamenající	znamenající	k2eAgFnSc4d1	znamenající
radost	radost	k1gFnSc4	radost
<g/>
.	.	kIx.	.
</s>
<s>
Složením	složení	k1gNnSc7	složení
dvou	dva	k4xCgFnPc2	dva
těchto	tento	k3xDgNnPc2	tento
slov	slovo	k1gNnPc2	slovo
má	mít	k5eAaImIp3nS	mít
jméno	jméno	k1gNnSc4	jméno
význam	význam	k1gInSc1	význam
buď	buď	k8xC	buď
"	"	kIx"	"
<g/>
Radost	radost	k1gFnSc1	radost
z	z	k7c2	z
války	válka	k1gFnSc2	válka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Usměvavý	usměvavý	k2eAgMnSc1d1	usměvavý
válečník	válečník	k1gMnSc1	válečník
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
Útěcha	útěcha	k1gFnSc1	útěcha
voje	voj	k1gFnSc2	voj
<g/>
"	"	kIx"	"
-	-	kIx~	-
tedy	tedy	k9	tedy
vojska	vojsko	k1gNnSc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Vojta	Vojta	k1gMnSc1	Vojta
<g/>
,	,	kIx,	,
Vojtík	Vojtík	k1gMnSc1	Vojtík
<g/>
,	,	kIx,	,
Vojtíšek	Vojtíšek	k1gMnSc1	Vojtíšek
<g/>
,	,	kIx,	,
Vojtínek	Vojtínek	k1gMnSc1	Vojtínek
<g/>
,	,	kIx,	,
Vojcek	Vojcka	k1gFnPc2	Vojcka
<g/>
,	,	kIx,	,
Těch	ten	k3xDgFnPc2	ten
<g/>
,	,	kIx,	,
Těšek	Těšek	k6eAd1	Těšek
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
uvádí	uvádět	k5eAaImIp3nS	uvádět
četnost	četnost	k1gFnSc4	četnost
jména	jméno	k1gNnSc2	jméno
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
pořadí	pořadí	k1gNnSc2	pořadí
mezi	mezi	k7c7	mezi
mužskými	mužský	k2eAgNnPc7d1	mužské
jmény	jméno	k1gNnPc7	jméno
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
dvou	dva	k4xCgInPc2	dva
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgInPc4	který
jsou	být	k5eAaImIp3nP	být
dostupné	dostupný	k2eAgInPc1d1	dostupný
údaje	údaj	k1gInPc1	údaj
MV	MV	kA	MV
ČR	ČR	kA	ČR
-	-	kIx~	-
lze	lze	k6eAd1	lze
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
tedy	tedy	k9	tedy
vysledovat	vysledovat	k5eAaImF	vysledovat
<g />
.	.	kIx.	.
</s>
<s>
trend	trend	k1gInSc1	trend
v	v	k7c6	v
užívání	užívání	k1gNnSc6	užívání
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
:	:	kIx,	:
Změna	změna	k1gFnSc1	změna
procentního	procentní	k2eAgNnSc2d1	procentní
zastoupení	zastoupení	k1gNnSc2	zastoupení
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
mezi	mezi	k7c7	mezi
žijícími	žijící	k2eAgMnPc7d1	žijící
muži	muž	k1gMnPc7	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
procentní	procentní	k2eAgFnSc1d1	procentní
změna	změna	k1gFnSc1	změna
se	se	k3xPyFc4	se
započítáním	započítání	k1gNnSc7	započítání
celkového	celkový	k2eAgInSc2d1	celkový
úbytku	úbytek	k1gInSc2	úbytek
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
ČR	ČR	kA	ČR
za	za	k7c4	za
sledované	sledovaný	k2eAgInPc4d1	sledovaný
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
+6,0	+6,0	k4	+6,0
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
poměrně	poměrně	k6eAd1	poměrně
značném	značný	k2eAgInSc6d1	značný
nárůstu	nárůst	k1gInSc6	nárůst
obliby	obliba	k1gFnSc2	obliba
tohoto	tento	k3xDgNnSc2	tento
jména	jméno	k1gNnSc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
ČSÚ	ČSÚ	kA	ČSÚ
se	se	k3xPyFc4	se
za	za	k7c4	za
leden	leden	k1gInSc4	leden
2006	[number]	k4	2006
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
7	[number]	k4	7
<g/>
.	.	kIx.	.
nejčastější	častý	k2eAgNnSc1d3	nejčastější
mužské	mužský	k2eAgNnSc1d1	mužské
jméno	jméno	k1gNnSc1	jméno
novorozenců	novorozenec	k1gMnPc2	novorozenec
<g/>
.	.	kIx.	.
německy	německy	k6eAd1	německy
a	a	k8xC	a
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Adalbert	Adalbert	k1gMnSc1	Adalbert
polsky	polsky	k6eAd1	polsky
<g/>
:	:	kIx,	:
Wojciech	Wojciech	k1gInSc1	Wojciech
maďarsky	maďarsky	k6eAd1	maďarsky
<g/>
:	:	kIx,	:
Béla	Béla	k1gMnSc1	Béla
italsky	italsky	k6eAd1	italsky
<g/>
:	:	kIx,	:
Adalberto	Adalberta	k1gFnSc5	Adalberta
latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
Adalbertus	Adalbertus	k1gInSc1	Adalbertus
(	(	kIx(	(
<g/>
Voitechus	Voitechus	k1gInSc1	Voitechus
<g/>
)	)	kIx)	)
španělsky	španělsky	k6eAd1	španělsky
<g/>
:	:	kIx,	:
Edilberto	Edilberta	k1gFnSc5	Edilberta
Jméno	jméno	k1gNnSc1	jméno
Adalbert	Adalbert	k1gMnSc1	Adalbert
bývá	bývat	k5eAaImIp3nS	bývat
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
německý	německý	k2eAgInSc4d1	německý
protějšek	protějšek	k1gInSc4	protějšek
českého	český	k2eAgNnSc2d1	české
jména	jméno	k1gNnSc2	jméno
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
v	v	k7c6	v
německy	německy	k6eAd1	německy
psaných	psaný	k2eAgFnPc6d1	psaná
matrikách	matrika	k1gFnPc6	matrika
českých	český	k2eAgFnPc2d1	Česká
farností	farnost	k1gFnPc2	farnost
z	z	k7c2	z
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
Vojtěchové	Vojtěchové	k2eAgFnSc7d1	Vojtěchové
většinou	většina	k1gFnSc7	většina
zapsáni	zapsán	k2eAgMnPc1d1	zapsán
jako	jako	k8xS	jako
Adalbertové	Adalbert	k1gMnPc1	Adalbert
<g/>
.	.	kIx.	.
</s>
<s>
Kuriózní	kuriózní	k2eAgInSc1d1	kuriózní
je	být	k5eAaImIp3nS	být
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
jindy	jindy	k6eAd1	jindy
mají	mít	k5eAaImIp3nP	mít
různojazyčné	různojazyčný	k2eAgFnPc4d1	různojazyčný
varianty	varianta	k1gFnPc4	varianta
společný	společný	k2eAgInSc4d1	společný
původ	původ	k1gInSc4	původ
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
původcem	původce	k1gMnSc7	původce
Svatý	svatý	k2eAgMnSc1d1	svatý
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
slovanskou	slovanský	k2eAgFnSc4d1	Slovanská
oblast	oblast	k1gFnSc4	oblast
známý	známý	k2eAgMnSc1d1	známý
pod	pod	k7c7	pod
svým	svůj	k3xOyFgNnSc7	svůj
biřmovacím	biřmovací	k2eAgNnSc7d1	biřmovací
jménem	jméno	k1gNnSc7	jméno
Adalbert	Adalbert	k1gMnSc1	Adalbert
<g/>
.	.	kIx.	.
</s>
<s>
Známí	známý	k2eAgMnPc1d1	známý
nositelé	nositel	k1gMnPc1	nositel
jména	jméno	k1gNnSc2	jméno
Svatý	svatý	k1gMnSc1	svatý
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Slavníkovců	Slavníkovec	k1gMnPc2	Slavníkovec
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Bednář	Bednář	k1gMnSc1	Bednář
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
sociolog	sociolog	k1gMnSc1	sociolog
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Hynais	Hynais	k1gInSc1	Hynais
-	-	kIx~	-
významný	významný	k2eAgMnSc1d1	významný
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
opony	opona	k1gFnSc2	opona
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Dyk	Dyk	k?	Dyk
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Kotek	Kotek	k1gMnSc1	Kotek
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Kryšpín	Kryšpín	k1gMnSc1	Kryšpín
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
konstruktér	konstruktér	k1gMnSc1	konstruktér
lokomotiv	lokomotiva	k1gFnPc2	lokomotiva
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Steklač	Steklač	k1gMnSc1	Steklač
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
<g />
.	.	kIx.	.
</s>
<s>
spisovatel	spisovatel	k1gMnSc1	spisovatel
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Filip	Filip	k1gMnSc1	Filip
-	-	kIx~	-
současný	současný	k2eAgMnSc1d1	současný
český	český	k2eAgMnSc1d1	český
politik	politik	k1gMnSc1	politik
Alberto	Alberta	k1gFnSc5	Alberta
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Frič	Frič	k1gMnSc1	Frič
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
botanik	botanik	k1gMnSc1	botanik
a	a	k8xC	a
cestovatel	cestovatel	k1gMnSc1	cestovatel
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Jarník	jarník	k1gInSc1	jarník
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
matematik	matematik	k1gMnSc1	matematik
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Zamarovský	Zamarovský	k2eAgMnSc1d1	Zamarovský
-	-	kIx~	-
slovenský	slovenský	k2eAgMnSc1d1	slovenský
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Náprstek	náprstek	k1gInSc1	náprstek
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
národopisec	národopisec	k1gMnSc1	národopisec
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
muzea	muzeum	k1gNnSc2	muzeum
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dnes	dnes	k6eAd1	dnes
nese	nést	k5eAaImIp3nS	nést
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Bernatský	Bernatský	k2eAgMnSc1d1	Bernatský
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
moderátor	moderátor	k1gMnSc1	moderátor
Ludvík	Ludvík	k1gMnSc1	Ludvík
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
československý	československý	k2eAgMnSc1d1	československý
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
poúnorový	poúnorový	k2eAgMnSc1d1	poúnorový
poslanec	poslanec	k1gMnSc1	poslanec
KSČ	KSČ	kA	KSČ
Tomáš	Tomáš	k1gMnSc1	Tomáš
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
automobilový	automobilový	k2eAgMnSc1d1	automobilový
závodník	závodník	k1gMnSc1	závodník
Václav	Václav	k1gMnSc1	Václav
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
-	-	kIx~	-
český	český	k2eAgMnSc1d1	český
cestovatel	cestovatel	k1gMnSc1	cestovatel
a	a	k8xC	a
polárník	polárník	k1gMnSc1	polárník
Důl	důl	k1gInSc1	důl
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Seznam	seznam	k1gInSc1	seznam
článků	článek	k1gInPc2	článek
začínajících	začínající	k2eAgInPc2d1	začínající
na	na	k7c4	na
"	"	kIx"	"
<g/>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
"	"	kIx"	"
Seznam	seznam	k1gInSc1	seznam
článků	článek	k1gInPc2	článek
začínajících	začínající	k2eAgInPc2d1	začínající
na	na	k7c4	na
"	"	kIx"	"
<g/>
Vojta	Vojta	k1gMnSc1	Vojta
<g/>
"	"	kIx"	"
Vojta	Vojta	k1gMnSc1	Vojta
Vojtíšek	Vojtíšek	k1gMnSc1	Vojtíšek
</s>
