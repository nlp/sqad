<s>
Jaká	jaký	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
zkratka	zkratka	k1gFnSc1
pro	pro	k7c4
svobodný	svobodný	k2eAgInSc4d1
a	a	k8xC
otevřený	otevřený	k2eAgInSc4d1
mezinárodní	mezinárodní	k2eAgInSc4d1
standard	standard	k1gInSc4
pro	pro	k7c4
formátování	formátování	k1gNnSc4
souborů	soubor	k1gInPc2
dvourozměrné	dvourozměrný	k2eAgFnSc2d1
vektorové	vektorový	k2eAgFnSc2d1
grafiky	grafika	k1gFnSc2
<g/>
,	,	kIx,
rastrové	rastrový	k2eAgFnSc2d1
grafiky	grafika	k1gFnSc2
a	a	k8xC
textu	text	k1gInSc2
<g/>
?	?	kIx.
</s>