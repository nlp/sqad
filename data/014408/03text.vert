<s>
CGM	CGM	kA
</s>
<s>
CGM	CGM	kA
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
z	z	k7c2
anglického	anglický	k2eAgMnSc2d1
Computer	computer	k1gInSc1
Graphics	Graphics	k1gInSc1
Metafile	Metafila	k1gFnSc3
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
a	a	k8xC
otevřený	otevřený	k2eAgInSc1d1
mezinárodní	mezinárodní	k2eAgInSc1d1
standard	standard	k1gInSc1
pro	pro	k7c4
formát	formát	k1gInSc4
souborů	soubor	k1gInPc2
dvourozměrné	dvourozměrný	k2eAgFnSc2d1
vektorové	vektorový	k2eAgFnSc2d1
grafiky	grafika	k1gFnSc2
<g/>
,	,	kIx,
rastrové	rastrový	k2eAgFnSc2d1
grafiky	grafika	k1gFnSc2
a	a	k8xC
textu	text	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Definovala	definovat	k5eAaBmAgFnS
jej	on	k3xPp3gNnSc4
Mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
pro	pro	k7c4
normalizaci	normalizace	k1gFnSc4
jako	jako	k8xC,k8xS
ISO	ISO	kA
8632	#num#	k4
a	a	k8xC
Mezinárodní	mezinárodní	k2eAgFnSc1d1
elektrotechnická	elektrotechnický	k2eAgFnSc1d1
komise	komise	k1gFnSc1
jako	jako	k8xS,k8xC
ISO	ISO	kA
<g/>
/	/	kIx~
<g/>
IEC	IEC	kA
8632	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
standard	standard	k1gInSc1
jej	on	k3xPp3gMnSc4
uznává	uznávat	k5eAaImIp3nS
také	také	k9
Americký	americký	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
standardizační	standardizační	k2eAgInSc1d1
institut	institut	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konsorcium	konsorcium	k1gNnSc1
W3C	W3C	k1gFnPc2
definovalo	definovat	k5eAaBmAgNnS
WebCGM	WebCGM	k1gFnSc4
pro	pro	k7c4
použití	použití	k1gNnSc4
CGM	CGM	kA
na	na	k7c6
webu	web	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
běžné	běžný	k2eAgFnSc6d1
počítačové	počítačový	k2eAgFnSc6d1
grafice	grafika	k1gFnSc6
se	se	k3xPyFc4
CGM	CGM	kA
příliš	příliš	k6eAd1
nepoužívá	používat	k5eNaImIp3nS
<g/>
,	,	kIx,
například	například	k6eAd1
na	na	k7c6
webu	web	k1gInSc6
je	být	k5eAaImIp3nS
upřednostňován	upřednostňován	k2eAgInSc4d1
formát	formát	k1gInSc4
SVG	SVG	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významnější	významný	k2eAgNnSc4d2
je	být	k5eAaImIp3nS
využití	využití	k1gNnSc4
CGM	CGM	kA
v	v	k7c6
průmyslu	průmysl	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
tam	tam	k6eAd1
má	mít	k5eAaImIp3nS
konkurenci	konkurence	k1gFnSc4
<g/>
,	,	kIx,
například	například	k6eAd1
formát	formát	k1gInSc1
DXF	DXF	kA
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Computer	computer	k1gInSc1
Graphics	Graphics	k1gInSc1
Metafile	Metafila	k1gFnSc3
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Přehledné	přehledný	k2eAgFnPc1d1
informace	informace	k1gFnPc1
o	o	k7c6
CGM	CGM	kA
na	na	k7c6
stránkách	stránka	k1gFnPc6
amerického	americký	k2eAgInSc2d1
Národního	národní	k2eAgInSc2d1
institutu	institut	k1gInSc2
standardů	standard	k1gInPc2
a	a	k8xC
technologie	technologie	k1gFnSc2
</s>
<s>
2.1	2.1	k4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
na	na	k7c6
stránkách	stránka	k1gFnPc6
W3C	W3C	k1gFnSc2
–	–	k?
neplatný	platný	k2eNgInSc1d1
odkaz	odkaz	k1gInSc1
!	!	kIx.
</s>
