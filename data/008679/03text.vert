<p>
<s>
Obří	obří	k2eAgInPc1d1	obří
hrnce	hrnec	k1gInPc1	hrnec
jsou	být	k5eAaImIp3nP	být
geomorfologické	geomorfologický	k2eAgInPc1d1	geomorfologický
tvary	tvar	k1gInPc1	tvar
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
kruhových	kruhový	k2eAgFnPc2d1	kruhová
nebo	nebo	k8xC	nebo
elipsovitých	elipsovitý	k2eAgFnPc2d1	elipsovitá
prohlubní	prohlubeň	k1gFnPc2	prohlubeň
ve	v	k7c6	v
skalním	skalní	k2eAgNnSc6d1	skalní
dně	dno	k1gNnSc6	dno
nebo	nebo	k8xC	nebo
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
skalních	skalní	k2eAgInPc2d1	skalní
bloků	blok	k1gInPc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vznik	vznik	k1gInSc1	vznik
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
eroze	eroze	k1gFnSc2	eroze
<g/>
,	,	kIx,	,
t.j.	t.j.	k?	t.j.
abrazívního	abrazívní	k2eAgNnSc2d1	abrazívní
působení	působení	k1gNnSc2	působení
pevného	pevný	k2eAgInSc2d1	pevný
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
unášeného	unášený	k2eAgInSc2d1	unášený
vířivým	vířivý	k2eAgInSc7d1	vířivý
proudem	proud	k1gInSc7	proud
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
říčních	říční	k2eAgNnPc6d1	říční
korytech	koryto	k1gNnPc6	koryto
<g/>
,	,	kIx,	,
na	na	k7c6	na
mořském	mořský	k2eAgInSc6d1	mořský
břehu	břeh	k1gInSc6	břeh
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
ledovci	ledovec	k1gInPc7	ledovec
nebo	nebo	k8xC	nebo
v	v	k7c6	v
krasových	krasový	k2eAgFnPc6d1	krasová
dutinách	dutina	k1gFnPc6	dutina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
práci	práce	k1gFnSc6	práce
Josefa	Josef	k1gMnSc2	Josef
Rubína	Rubín	k1gMnSc2	Rubín
a	a	k8xC	a
Břetislava	Břetislav	k1gMnSc2	Břetislav
Balatky	balatka	k1gFnSc2	balatka
Atlas	Atlas	k1gMnSc1	Atlas
skalních	skalní	k2eAgInPc2d1	skalní
<g/>
,	,	kIx,	,
zemních	zemní	k2eAgInPc2d1	zemní
a	a	k8xC	a
půdních	půdní	k2eAgInPc2d1	půdní
tvarů	tvar	k1gInPc2	tvar
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obří	obří	k2eAgInSc1d1	obří
hrnec	hrnec	k1gInSc1	hrnec
definován	definovat	k5eAaBmNgInS	definovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
prohlubeň	prohlubeň	k1gFnSc4	prohlubeň
pravidelného	pravidelný	k2eAgInSc2d1	pravidelný
tvaru	tvar	k1gInSc2	tvar
v	v	k7c6	v
korytech	koryto	k1gNnPc6	koryto
vodních	vodní	k2eAgMnPc2d1	vodní
toků	tok	k1gInPc2	tok
<g/>
,	,	kIx,	,
v	v	k7c6	v
podledovcovém	podledovcový	k2eAgInSc6d1	podledovcový
skalním	skalní	k2eAgInSc6d1	skalní
dnu	den	k1gInSc6	den
<g/>
,	,	kIx,	,
na	na	k7c6	na
mořském	mořský	k2eAgMnSc6d1	mořský
nebo	nebo	k8xC	nebo
jezerním	jezerní	k2eAgNnSc6d1	jezerní
pobřeží	pobřeží	k1gNnSc6	pobřeží
a	a	k8xC	a
ve	v	k7c6	v
stropech	strop	k1gInPc6	strop
jeskyní	jeskyně	k1gFnPc2	jeskyně
<g/>
.	.	kIx.	.
</s>
<s>
Kotel	kotel	k1gInSc1	kotel
vzniká	vznikat	k5eAaImIp3nS	vznikat
evorzní	evorzeň	k1gFnSc7	evorzeň
(	(	kIx(	(
<g/>
vířivou	vířivý	k2eAgFnSc7d1	vířivá
<g/>
)	)	kIx)	)
činností	činnost	k1gFnSc7	činnost
proudící	proudící	k2eAgFnPc1d1	proudící
vody	voda	k1gFnPc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
zpravidla	zpravidla	k6eAd1	zpravidla
kruhový	kruhový	k2eAgInSc4d1	kruhový
nebo	nebo	k8xC	nebo
elipsovitý	elipsovitý	k2eAgInSc4d1	elipsovitý
půdorys	půdorys	k1gInSc4	půdorys
a	a	k8xC	a
válcovitý	válcovitý	k2eAgInSc4d1	válcovitý
<g/>
,	,	kIx,	,
půlkulovitý	půlkulovitý	k2eAgInSc4d1	půlkulovitý
<g/>
,	,	kIx,	,
kuželovitý	kuželovitý	k2eAgInSc4d1	kuželovitý
nebo	nebo	k8xC	nebo
miskovitý	miskovitý	k2eAgInSc4d1	miskovitý
tvar	tvar	k1gInSc4	tvar
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podmínky	podmínka	k1gFnSc2	podmínka
vzniku	vznik	k1gInSc2	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Jak	jak	k8xC	jak
uvádějí	uvádět	k5eAaImIp3nP	uvádět
J.	J.	kA	J.
Rubín	rubín	k1gInSc1	rubín
a	a	k8xC	a
B.	B.	kA	B.
Balatka	balatka	k1gFnSc1	balatka
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Atlas	Atlas	k1gInSc1	Atlas
skalních	skalní	k2eAgInPc2d1	skalní
<g/>
,	,	kIx,	,
zemních	zemní	k2eAgInPc2d1	zemní
a	a	k8xC	a
půdních	půdní	k2eAgInPc2d1	půdní
tvarů	tvar	k1gInPc2	tvar
<g/>
,	,	kIx,	,
podmínkou	podmínka	k1gFnSc7	podmínka
vzniku	vznik	k1gInSc2	vznik
obřích	obří	k2eAgInPc2d1	obří
hrnců	hrnec	k1gInPc2	hrnec
v	v	k7c6	v
korytech	koryto	k1gNnPc6	koryto
toků	tok	k1gInPc2	tok
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgFnPc4d1	vhodná
geomorfologické	geomorfologický	k2eAgFnPc4d1	geomorfologická
<g/>
,	,	kIx,	,
geologické	geologický	k2eAgFnPc4d1	geologická
a	a	k8xC	a
hydrologické	hydrologický	k2eAgFnPc4d1	hydrologická
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Obří	obří	k2eAgInPc1d1	obří
hrnce	hrnec	k1gInPc1	hrnec
vznikají	vznikat	k5eAaImIp3nP	vznikat
evorzí	evorze	k1gFnSc7	evorze
<g/>
,	,	kIx,	,
t.j.	t.j.	k?	t.j.
rotačním	rotační	k2eAgInSc7d1	rotační
pohybem	pohyb	k1gInSc7	pohyb
vody	voda	k1gFnSc2	voda
s	s	k7c7	s
pevnými	pevný	k2eAgFnPc7d1	pevná
částicemi	částice	k1gFnPc7	částice
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
či	či	k8xC	či
stěnách	stěna	k1gFnPc6	stěna
prohlubní	prohlubeň	k1gFnPc2	prohlubeň
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgFnPc2d1	hlavní
podmínek	podmínka	k1gFnPc2	podmínka
vzniku	vznik	k1gInSc2	vznik
těchto	tento	k3xDgInPc2	tento
evorzních	evorzní	k2eAgInPc2d1	evorzní
tvarů	tvar	k1gInPc2	tvar
je	být	k5eAaImIp3nS	být
především	především	k9	především
výraznější	výrazný	k2eAgInSc4d2	výraznější
sklon	sklon	k1gInSc4	sklon
vodního	vodní	k2eAgInSc2d1	vodní
toku	tok	k1gInSc2	tok
<g/>
,	,	kIx,	,
dostatečný	dostatečný	k2eAgInSc1d1	dostatečný
průtok	průtok	k1gInSc1	průtok
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
příznivé	příznivý	k2eAgFnPc1d1	příznivá
vlastnosti	vlastnost	k1gFnPc1	vlastnost
místních	místní	k2eAgFnPc2d1	místní
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Obří	obří	k2eAgInPc1d1	obří
hrnce	hrnec	k1gInPc1	hrnec
vznikají	vznikat	k5eAaImIp3nP	vznikat
nejrychleji	rychle	k6eAd3	rychle
v	v	k7c6	v
měkkých	měkký	k2eAgFnPc6d1	měkká
horninách	hornina	k1gFnPc6	hornina
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
existence	existence	k1gFnSc1	existence
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
druhu	druh	k1gInSc6	druh
hornin	hornina	k1gFnPc2	hornina
však	však	k9	však
není	být	k5eNaImIp3nS	být
trvalá	trvalý	k2eAgFnSc1d1	trvalá
<g/>
.	.	kIx.	.
</s>
<s>
Dokonalé	dokonalý	k2eAgInPc1d1	dokonalý
a	a	k8xC	a
stabilní	stabilní	k2eAgInPc1d1	stabilní
tvary	tvar	k1gInPc1	tvar
vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
hlubinných	hlubinný	k2eAgFnPc6d1	hlubinná
vyvřelinách	vyvřelina	k1gFnPc6	vyvřelina
a	a	k8xC	a
některých	některý	k3yIgFnPc6	některý
krystalických	krystalický	k2eAgFnPc6d1	krystalická
břidlicích	břidlice	k1gFnPc6	břidlice
<g/>
.	.	kIx.	.
</s>
<s>
Nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
obřích	obří	k2eAgInPc2d1	obří
hrnců	hrnec	k1gInPc2	hrnec
jsou	být	k5eAaImIp3nP	být
horniny	hornina	k1gFnPc1	hornina
silně	silně	k6eAd1	silně
rozpukané	rozpukaný	k2eAgFnPc1d1	rozpukaná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obří	obří	k2eAgInPc1d1	obří
hrnce	hrnec	k1gInPc1	hrnec
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Fluviální	fluviální	k2eAgInPc1d1	fluviální
obří	obří	k2eAgInPc1d1	obří
hrnce	hrnec	k1gInPc1	hrnec
===	===	k?	===
</s>
</p>
<p>
<s>
Obří	obří	k2eAgInPc1d1	obří
hrnce	hrnec	k1gInPc1	hrnec
se	se	k3xPyFc4	se
v	v	k7c6	v
korytech	koryto	k1gNnPc6	koryto
vodních	vodní	k2eAgMnPc2d1	vodní
toků	tok	k1gInPc2	tok
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
na	na	k7c6	na
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
příklady	příklad	k1gInPc4	příklad
z	z	k7c2	z
různých	různý	k2eAgNnPc2d1	různé
míst	místo	k1gNnPc2	místo
můžeme	moct	k5eAaImIp1nP	moct
jmenovat	jmenovat	k5eAaBmF	jmenovat
Vydru	vydra	k1gFnSc4	vydra
<g/>
,	,	kIx,	,
Křemelnou	křemelný	k2eAgFnSc4d1	Křemelná
a	a	k8xC	a
horní	horní	k2eAgInSc1d1	horní
tok	tok	k1gInSc1	tok
Otavy	Otava	k1gFnSc2	Otava
na	na	k7c6	na
Šumavě	Šumava	k1gFnSc6	Šumava
<g/>
,	,	kIx,	,
koryto	koryto	k1gNnSc4	koryto
Vltavy	Vltava	k1gFnSc2	Vltava
pod	pod	k7c7	pod
Čertovou	čertový	k2eAgFnSc7d1	Čertová
stěnou	stěna	k1gFnSc7	stěna
mezi	mezi	k7c7	mezi
Lipnem	Lipno	k1gNnSc7	Lipno
a	a	k8xC	a
Vyšším	vysoký	k2eAgInSc7d2	vyšší
Brodem	Brod	k1gInSc7	Brod
<g/>
,	,	kIx,	,
horní	horní	k2eAgInSc4d1	horní
tok	tok	k1gInSc4	tok
Labe	Labe	k1gNnSc2	Labe
<g/>
,	,	kIx,	,
Divokou	divoký	k2eAgFnSc4d1	divoká
Orlici	Orlice	k1gFnSc4	Orlice
u	u	k7c2	u
Zemské	zemský	k2eAgFnSc2d1	zemská
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
Sázavu	Sázava	k1gFnSc4	Sázava
u	u	k7c2	u
Stvořidel	Stvořidlo	k1gNnPc2	Stvořidlo
nebo	nebo	k8xC	nebo
peřeje	peřej	k1gFnSc2	peřej
Studeného	Studeného	k2eAgInSc2d1	Studeného
potoka	potok	k1gInSc2	potok
na	na	k7c6	na
území	území	k1gNnSc6	území
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Vysoký	vysoký	k2eAgInSc1d1	vysoký
vodopád	vodopád	k1gInSc1	vodopád
nedaleko	daleko	k6eNd1	daleko
Pradědu	praděd	k1gMnSc3	praděd
v	v	k7c6	v
Jeseníkách	Jeseníky	k1gInPc6	Jeseníky
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Černé	Černé	k2eAgNnSc4d1	Černé
Desné	Desné	k1gNnSc4	Desné
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
mezi	mezi	k7c7	mezi
Desnou	Desna	k1gFnSc7	Desna
a	a	k8xC	a
Souší	souš	k1gFnSc7	souš
v	v	k7c6	v
Jizerských	jizerský	k2eAgFnPc6d1	Jizerská
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
chráněnou	chráněný	k2eAgFnSc7d1	chráněná
přírodní	přírodní	k2eAgFnSc7d1	přírodní
památkou	památka	k1gFnSc7	památka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
vodopádů	vodopád	k1gInPc2	vodopád
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Hrncový	hrncový	k2eAgMnSc1d1	hrncový
podle	podle	k7c2	podle
zdejších	zdejší	k2eAgInPc2d1	zdejší
obřích	obří	k2eAgInPc2d1	obří
hrnců	hrnec	k1gInPc2	hrnec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
soutěsce	soutěska	k1gFnSc6	soutěska
Koryto	koryto	k1gNnSc1	koryto
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Doubravě	Doubrava	k1gFnSc6	Doubrava
u	u	k7c2	u
Chotěboře	Chotěboř	k1gFnSc2	Chotěboř
pod	pod	k7c7	pod
dolním	dolní	k2eAgInSc7d1	dolní
vodopádem	vodopád	k1gInSc7	vodopád
lze	lze	k6eAd1	lze
zhruba	zhruba	k6eAd1	zhruba
1	[number]	k4	1
metr	metr	k1gInSc4	metr
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
spatřit	spatřit	k5eAaPmF	spatřit
obří	obří	k2eAgInSc4d1	obří
hrnec	hrnec	k1gInSc4	hrnec
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
120	[number]	k4	120
cm	cm	kA	cm
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
dutina	dutina	k1gFnSc1	dutina
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hloubky	hloubka	k1gFnSc2	hloubka
105	[number]	k4	105
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
korytě	koryto	k1gNnSc6	koryto
říčky	říčka	k1gFnSc2	říčka
Mumlavy	Mumlavy	k?	Mumlavy
(	(	kIx(	(
<g/>
Mumlavské	Mumlavský	k2eAgInPc1d1	Mumlavský
vodopády	vodopád	k1gInPc1	vodopád
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
žulovém	žulový	k2eAgNnSc6d1	žulové
podloží	podloží	k1gNnSc6	podloží
a	a	k8xC	a
balvanech	balvan	k1gInPc6	balvan
vyhloubeny	vyhlouben	k2eAgInPc4d1	vyhlouben
četné	četný	k2eAgInPc4d1	četný
obří	obří	k2eAgInPc4d1	obří
hrnce	hrnec	k1gInPc4	hrnec
<g/>
,	,	kIx,	,
místními	místní	k2eAgFnPc7d1	místní
lidmi	člověk	k1gMnPc7	člověk
zvané	zvaný	k2eAgFnPc4d1	zvaná
Čertova	čertův	k2eAgNnPc4d1	Čertovo
oka	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInPc4d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
má	mít	k5eAaImIp3nS	mít
rozměry	rozměr	k1gInPc4	rozměr
7	[number]	k4	7
x	x	k?	x
5,5	[number]	k4	5,5
m.	m.	k?	m.
Takové	takový	k3xDgInPc1	takový
několikametrové	několikametrový	k2eAgInPc1d1	několikametrový
útvary	útvar	k1gInPc1	útvar
bývají	bývat	k5eAaImIp3nP	bývat
někdy	někdy	k6eAd1	někdy
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k8xC	jako
obří	obří	k2eAgInPc1d1	obří
kotle	kotel	k1gInPc1	kotel
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vývojově	vývojově	k6eAd1	vývojově
dokonalých	dokonalý	k2eAgInPc2d1	dokonalý
tvarů	tvar	k1gInPc2	tvar
se	se	k3xPyFc4	se
poměr	poměr	k1gInSc1	poměr
hloubky	hloubka	k1gFnSc2	hloubka
prohlubní	prohlubeň	k1gFnPc2	prohlubeň
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
průměru	průměr	k1gInSc3	průměr
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
2	[number]	k4	2
:	:	kIx,	:
1	[number]	k4	1
do	do	k7c2	do
5	[number]	k4	5
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
území	území	k1gNnSc6	území
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
Moravy	Morava	k1gFnSc2	Morava
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
kolem	kolem	k7c2	kolem
3000	[number]	k4	3000
těchto	tento	k3xDgInPc2	tento
evorzních	evorzní	k2eAgInPc2d1	evorzní
tvarů	tvar	k1gInPc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
polovina	polovina	k1gFnSc1	polovina
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
počtu	počet	k1gInSc2	počet
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
severovýchodních	severovýchodní	k2eAgFnPc6d1	severovýchodní
Čechách	Čechy	k1gFnPc6	Čechy
–	–	k?	–
konkrétně	konkrétně	k6eAd1	konkrétně
jen	jen	k9	jen
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Návarova	Návarův	k2eAgInSc2d1	Návarův
v	v	k7c6	v
korytě	koryto	k1gNnSc6	koryto
říčky	říčka	k1gFnSc2	říčka
Kamenice	Kamenice	k1gFnSc2	Kamenice
jich	on	k3xPp3gMnPc2	on
bylo	být	k5eAaImAgNnS	být
napočítáno	napočítat	k5eAaPmNgNnS	napočítat
na	na	k7c4	na
500	[number]	k4	500
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
evorzních	evorzní	k2eAgInPc2d1	evorzní
tvarů	tvar	k1gInPc2	tvar
na	na	k7c6	na
území	území	k1gNnSc6	území
ČR	ČR	kA	ČR
–	–	k?	–
celkem	celek	k1gInSc7	celek
925	[number]	k4	925
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
ne	ne	k9	ne
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
případech	případ	k1gInPc6	případ
dokonale	dokonale	k6eAd1	dokonale
vyvinutých	vyvinutý	k2eAgInPc2d1	vyvinutý
–	–	k?	–
byl	být	k5eAaImAgInS	být
zaregistrován	zaregistrovat	k5eAaPmNgInS	zaregistrovat
v	v	k7c6	v
korytě	koryto	k1gNnSc6	koryto
řeky	řeka	k1gFnSc2	řeka
Jizery	Jizera	k1gFnSc2	Jizera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Evorzní	Evorzní	k2eAgInPc4d1	Evorzní
tvary	tvar	k1gInPc4	tvar
v	v	k7c6	v
podzemních	podzemní	k2eAgFnPc6d1	podzemní
dutinách	dutina	k1gFnPc6	dutina
===	===	k?	===
</s>
</p>
<p>
<s>
Výskytu	výskyt	k1gInSc3	výskyt
krasových	krasový	k2eAgInPc2d1	krasový
jevů	jev	k1gInPc2	jev
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
i	i	k9	i
četnost	četnost	k1gFnSc4	četnost
evorzních	evorzní	k2eAgInPc2d1	evorzní
tvarů	tvar	k1gInPc2	tvar
na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
a	a	k8xC	a
stropech	strop	k1gInPc6	strop
podzemních	podzemní	k2eAgFnPc2d1	podzemní
prostor	prostora	k1gFnPc2	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Obří	obří	k2eAgInPc1d1	obří
hrnce	hrnec	k1gInPc1	hrnec
vznikají	vznikat	k5eAaImIp3nP	vznikat
v	v	k7c6	v
podzemních	podzemní	k2eAgFnPc6d1	podzemní
prostorách	prostora	k1gFnPc6	prostora
při	při	k7c6	při
povodních	povodeň	k1gFnPc6	povodeň
nebo	nebo	k8xC	nebo
působením	působení	k1gNnSc7	působení
podzemních	podzemní	k2eAgInPc2d1	podzemní
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vazbě	vazba	k1gFnSc6	vazba
na	na	k7c4	na
specifické	specifický	k2eAgFnPc4d1	specifická
podmínky	podmínka	k1gFnPc4	podmínka
vzniku	vznik	k1gInSc2	vznik
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
označení	označení	k1gNnSc1	označení
tlakové	tlakový	k2eAgInPc1d1	tlakový
obří	obří	k2eAgInPc1d1	obří
hrnce	hrnec	k1gInPc1	hrnec
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
tvary	tvar	k1gInPc1	tvar
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
například	například	k6eAd1	například
na	na	k7c6	na
stropě	strop	k1gInSc6	strop
poblíž	poblíž	k7c2	poblíž
Srbského	srbský	k2eAgInSc2d1	srbský
sifonu	sifon	k1gInSc2	sifon
v	v	k7c6	v
jeskynním	jeskynní	k2eAgInSc6d1	jeskynní
komplexu	komplex	k1gInSc6	komplex
Býčí	býčí	k2eAgFnSc1d1	býčí
skála	skála	k1gFnSc1	skála
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
krasu	kras	k1gInSc6	kras
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Chýnovských	Chýnovský	k2eAgFnPc6d1	Chýnovská
jeskyních	jeskyně	k1gFnPc6	jeskyně
východně	východně	k6eAd1	východně
od	od	k7c2	od
jihočeského	jihočeský	k2eAgInSc2d1	jihočeský
Tábora	Tábor	k1gInSc2	Tábor
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
méně	málo	k6eAd2	málo
známých	známý	k2eAgNnPc2d1	známé
míst	místo	k1gNnPc2	místo
lze	lze	k6eAd1	lze
jmenovat	jmenovat	k5eAaImF	jmenovat
lokalitu	lokalita	k1gFnSc4	lokalita
Na	na	k7c6	na
Dolech	dol	k1gInPc6	dol
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Rychnov	Rychnov	k1gInSc1	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
ve	v	k7c6	v
stropech	strop	k1gInPc6	strop
podzemních	podzemní	k2eAgFnPc2d1	podzemní
prostor	prostora	k1gFnPc2	prostora
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
obří	obří	k2eAgInPc1d1	obří
hrnce	hrnec	k1gInPc1	hrnec
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
30	[number]	k4	30
až	až	k9	až
50	[number]	k4	50
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
krasu	kras	k1gInSc6	kras
<g/>
,	,	kIx,	,
v	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
na	na	k7c6	na
Stydlých	stydlý	k2eAgFnPc6d1	Stydlá
vodách	voda	k1gFnPc6	voda
mezi	mezi	k7c7	mezi
obcemi	obec	k1gFnPc7	obec
Svatý	svatý	k1gMnSc1	svatý
Jan	Jan	k1gMnSc1	Jan
pod	pod	k7c7	pod
Skalou	Skala	k1gMnSc7	Skala
a	a	k8xC	a
Bubovice	Bubovice	k1gFnSc2	Bubovice
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Aragonitová	Aragonitový	k2eAgFnSc1d1	Aragonitová
jeskyně	jeskyně	k1gFnSc1	jeskyně
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
evorzní	evorzní	k2eAgFnPc1d1	evorzní
prohlubně	prohlubeň	k1gFnPc1	prohlubeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Rubín	rubín	k1gInSc1	rubín
<g/>
,	,	kIx,	,
Břetislav	Břetislav	k1gMnSc1	Břetislav
Balatka	balatka	k1gFnSc1	balatka
<g/>
:	:	kIx,	:
Atlas	Atlas	k1gInSc1	Atlas
skalních	skalní	k2eAgInPc2d1	skalní
<g/>
,	,	kIx,	,
zemních	zemní	k2eAgInPc2d1	zemní
a	a	k8xC	a
půdních	půdní	k2eAgInPc2d1	půdní
tvarů	tvar	k1gInPc2	tvar
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1986	[number]	k4	1986
</s>
</p>
<p>
<s>
Břetislav	Břetislav	k1gMnSc1	Břetislav
Balatka	balatka	k1gFnSc1	balatka
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Sládek	Sládek	k1gMnSc1	Sládek
<g/>
:	:	kIx,	:
Evorzní	Evorzní	k2eAgInPc1d1	Evorzní
tvary	tvar	k1gInPc1	tvar
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
geneze	geneze	k1gFnSc2	geneze
<g/>
,	,	kIx,	,
ČSAV	ČSAV	kA	ČSAV
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1977	[number]	k4	1977
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Dvořák	Dvořák	k1gMnSc1	Dvořák
<g/>
:	:	kIx,	:
Obří	obří	k2eAgFnSc1d1	obří
hrnce	hrnec	k1gInSc2	hrnec
na	na	k7c6	na
Jizeře	Jizera	k1gFnSc6	Jizera
<g/>
,	,	kIx,	,
časopis	časopis	k1gInSc1	časopis
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
a	a	k8xC	a
Jizerské	jizerský	k2eAgFnPc1d1	Jizerská
hory	hora	k1gFnPc1	hora
č.	č.	k?	č.
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
4	[number]	k4	4
-	-	kIx~	-
7	[number]	k4	7
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Eroze	eroze	k1gFnSc1	eroze
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Obří	obří	k2eAgInPc1d1	obří
hrnce	hrnec	k1gInPc1	hrnec
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://www.geopark-ceskyraj.cz/?D=62	[url]	k4	http://www.geopark-ceskyraj.cz/?D=62
</s>
</p>
