<p>
<s>
Kampanologie	Kampanologie	k1gFnSc1	Kampanologie
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgMnSc2d1	latinský
campana	campan	k1gMnSc2	campan
–	–	k?	–
zvon	zvon	k1gInSc1	zvon
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
o	o	k7c6	o
zvonech	zvon	k1gInPc6	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
<g/>
,	,	kIx,	,
vývojem	vývoj	k1gInSc7	vývoj
<g/>
,	,	kIx,	,
výzdobou	výzdoba	k1gFnSc7	výzdoba
<g/>
,	,	kIx,	,
funkcí	funkce	k1gFnSc7	funkce
a	a	k8xC	a
akustikou	akustika	k1gFnSc7	akustika
zvonů	zvon	k1gInPc2	zvon
<g/>
,	,	kIx,	,
metodami	metoda	k1gFnPc7	metoda
jejich	jejich	k3xOp3gFnSc2	jejich
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
ladění	ladění	k1gNnSc4	ladění
a	a	k8xC	a
způsoby	způsob	k1gInPc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
hrát	hrát	k5eAaImF	hrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
termín	termín	k1gInSc1	termín
používá	používat	k5eAaImIp3nS	používat
spíše	spíše	k9	spíše
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
prací	práce	k1gFnSc7	práce
s	s	k7c7	s
relativně	relativně	k6eAd1	relativně
velkými	velký	k2eAgInPc7d1	velký
věžními	věžní	k2eAgInPc7d1	věžní
zvony	zvon	k1gInPc7	zvon
<g/>
,	,	kIx,	,
než	než	k8xS	než
menšími	malý	k2eAgFnPc7d2	menší
soustavami	soustava	k1gFnPc7	soustava
např.	např.	kA	např.
ručních	ruční	k2eAgInPc2d1	ruční
zvonků	zvonek	k1gInPc2	zvonek
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
přiřazována	přiřazován	k2eAgFnSc1d1	přiřazován
k	k	k7c3	k
pomocným	pomocný	k2eAgFnPc3d1	pomocná
vědám	věda	k1gFnPc3	věda
historickým	historický	k2eAgFnPc3d1	historická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
zvonařství	zvonařství	k1gNnSc2	zvonařství
</s>
</p>
<p>
<s>
Výroba	výroba	k1gFnSc1	výroba
zvonů	zvon	k1gInPc2	zvon
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Flodr	Flodr	k1gInSc1	Flodr
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
,	,	kIx,	,
Technologie	technologie	k1gFnSc1	technologie
středověkého	středověký	k2eAgNnSc2d1	středověké
zvonařství	zvonařství	k1gNnSc2	zvonařství
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Univerzita	univerzita	k1gFnSc1	univerzita
Jana	Jan	k1gMnSc2	Jan
Evangelisty	evangelista	k1gMnSc2	evangelista
Purkyně	Purkyně	k1gFnSc2	Purkyně
1983	[number]	k4	1983
</s>
</p>
<p>
<s>
Chvátal	Chvátal	k1gMnSc1	Chvátal
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
<g/>
,	,	kIx,	,
Pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
sondu	sonda	k1gFnSc4	sonda
do	do	k7c2	do
činnosti	činnost	k1gFnSc2	činnost
zvonařské	zvonařský	k2eAgFnSc2d1	zvonařská
dílny	dílna	k1gFnSc2	dílna
v	v	k7c6	v
období	období	k1gNnSc6	období
gotiky	gotika	k1gFnSc2	gotika
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Rozbor	rozbor	k1gInSc1	rozbor
radničních	radniční	k2eAgInPc2d1	radniční
účtů	účet	k1gInPc2	účet
města	město	k1gNnSc2	město
Zhořelce	Zhořelec	k1gInSc2	Zhořelec
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1377	[number]	k4	1377
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ústecký	ústecký	k2eAgInSc1d1	ústecký
sborník	sborník	k1gInSc1	sborník
historický	historický	k2eAgInSc1d1	historický
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
s.	s.	k?	s.
249-260	[number]	k4	249-260
</s>
</p>
<p>
<s>
Chvátal	Chvátal	k1gMnSc1	Chvátal
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
–	–	k?	–
Krčmář	Krčmář	k1gMnSc1	Krčmář
<g/>
,	,	kIx,	,
Luděk	Luděk	k1gMnSc1	Luděk
<g/>
,	,	kIx,	,
Obrazy	obraz	k1gInPc1	obraz
z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
českého	český	k2eAgNnSc2d1	české
zvonařství	zvonařství	k1gNnSc2	zvonařství
<g/>
.	.	kIx.	.
</s>
<s>
Rekviziční	rekviziční	k2eAgFnPc1d1	rekviziční
fotografie	fotografia	k1gFnPc1	fotografia
z	z	k7c2	z
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
ze	z	k7c2	z
sbírek	sbírka	k1gFnPc2	sbírka
Západočeského	západočeský	k2eAgNnSc2d1	Západočeské
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Domažlice	Domažlice	k1gFnPc1	Domažlice
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Českého	český	k2eAgInSc2d1	český
lesa	les	k1gInSc2	les
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
Jančarová	Jančarová	k1gFnSc1	Jančarová
<g/>
,	,	kIx,	,
Markéta	Markéta	k1gFnSc1	Markéta
<g/>
,	,	kIx,	,
Kampanologický	Kampanologický	k2eAgInSc1d1	Kampanologický
průzkum	průzkum	k1gInSc1	průzkum
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Opava	Opava	k1gFnSc1	Opava
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
provedeném	provedený	k2eAgInSc6d1	provedený
institucionálním	institucionální	k2eAgInSc6d1	institucionální
výzkumu	výzkum	k1gInSc6	výzkum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sborník	sborník	k1gInSc1	sborník
Státního	státní	k2eAgInSc2d1	státní
památkového	památkový	k2eAgInSc2d1	památkový
ústavu	ústav	k1gInSc2	ústav
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
vydáno	vydat	k5eAaPmNgNnS	vydat
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kol	kol	k7c2	kol
<g/>
.	.	kIx.	.
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
Zvony	zvon	k1gInPc1	zvon
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Sborník	sborník	k1gInSc1	sborník
statí	stať	k1gFnPc2	stať
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Vysoké	vysoký	k2eAgNnSc1d1	vysoké
učení	učení	k1gNnSc1	učení
technické	technický	k2eAgNnSc1d1	technické
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
2002	[number]	k4	2002
</s>
</p>
<p>
<s>
Zvony	zvon	k1gInPc1	zvon
a	a	k8xC	a
zvonařství	zvonařství	k1gNnSc1	zvonařství
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Národní	národní	k2eAgNnSc1d1	národní
technické	technický	k2eAgNnSc1d1	technické
muzeum	muzeum	k1gNnSc1	muzeum
2002	[number]	k4	2002
</s>
</p>
<p>
<s>
Kybalová	Kybalová	k1gFnSc1	Kybalová
<g/>
,	,	kIx,	,
Ludmila	Ludmila	k1gFnSc1	Ludmila
–	–	k?	–
Lunga	Lunga	k1gFnSc1	Lunga
<g/>
,	,	kIx,	,
Radek	Radek	k1gMnSc1	Radek
–	–	k?	–
Vácha	Vácha	k1gMnSc1	Vácha
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
Pražské	pražský	k2eAgInPc1d1	pražský
zvony	zvon	k1gInPc1	zvon
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Rybka	rybka	k1gFnSc1	rybka
Publishers	Publishers	k1gInSc1	Publishers
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
Mlčák	Mlčák	k1gMnSc1	Mlčák
<g/>
,	,	kIx,	,
Leoš	Leoš	k1gMnSc1	Leoš
–	–	k?	–
Šrámek	Šrámek	k1gMnSc1	Šrámek
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
,	,	kIx,	,
K	k	k7c3	k
dílu	dílo	k1gNnSc3	dílo
opavských	opavský	k2eAgMnPc2d1	opavský
renesančních	renesanční	k2eAgMnPc2d1	renesanční
zvonařů	zvonař	k1gMnPc2	zvonař
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Slezského	slezský	k2eAgNnSc2d1	Slezské
zemského	zemský	k2eAgNnSc2d1	zemské
muzea	muzeum	k1gNnSc2	muzeum
49	[number]	k4	49
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
č.	č.	k?	č.
3	[number]	k4	3
<g/>
,	,	kIx,	,
s.	s.	k?	s.
232-246	[number]	k4	232-246
</s>
</p>
<p>
<s>
Mlčák	Mlčák	k1gMnSc1	Mlčák
<g/>
,	,	kIx,	,
Leoš	Leoš	k1gMnSc1	Leoš
<g/>
,	,	kIx,	,
Heraldická	heraldický	k2eAgFnSc1d1	heraldická
a	a	k8xC	a
sfragistická	sfragistický	k2eAgFnSc1d1	sfragistický
výzdoba	výzdoba	k1gFnSc1	výzdoba
gotických	gotický	k2eAgInPc2d1	gotický
a	a	k8xC	a
renesančních	renesanční	k2eAgInPc2d1	renesanční
zvonů	zvon	k1gInPc2	zvon
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Slezského	slezský	k2eAgNnSc2d1	Slezské
zemského	zemský	k2eAgNnSc2d1	zemské
muzea	muzeum	k1gNnSc2	muzeum
50	[number]	k4	50
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
č.	č.	k?	č.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
1-7	[number]	k4	1-7
</s>
</p>
<p>
<s>
Mlčák	Mlčák	k1gMnSc1	Mlčák
<g/>
,	,	kIx,	,
Leoš	Leoš	k1gMnSc1	Leoš
<g/>
,	,	kIx,	,
Zvony	zvon	k1gInPc1	zvon
na	na	k7c6	na
Šumpersku	Šumpersko	k1gNnSc6	Šumpersko
a	a	k8xC	a
Jesenicku	Jesenicko	k1gNnSc6	Jesenicko
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
památkový	památkový	k2eAgInSc1d1	památkový
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
územně-odborné	územnědborný	k2eAgNnSc1d1	územně-odborný
pracoviště	pracoviště	k1gNnSc1	pracoviště
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Katalog	katalog	k1gInSc1	katalog
zvonů	zvon	k1gInPc2	zvon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Švihálek	Švihálek	k1gMnSc1	Švihálek
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
,	,	kIx,	,
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nP	rodit
zvony	zvon	k1gInPc1	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Jota	jota	k1gFnSc1	jota
1997	[number]	k4	1997
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kampanologie	kampanologie	k1gFnSc2	kampanologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kampanologie	kampanologie	k1gFnSc2	kampanologie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
O	o	k7c6	o
pomocných	pomocný	k2eAgFnPc6d1	pomocná
vědách	věda	k1gFnPc6	věda
historických	historický	k2eAgFnPc6d1	historická
</s>
</p>
