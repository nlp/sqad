<p>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
I.	I.	kA	I.
Bonaparte	bonapart	k1gInSc5	bonapart
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1769	[number]	k4	1769
Ajaccio	Ajaccio	k1gNnSc4	Ajaccio
–	–	k?	–
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1821	[number]	k4	1821
Svatá	svatý	k2eAgFnSc1d1	svatá
Helena	Helena	k1gFnSc1	Helena
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
a	a	k8xC	a
státník	státník	k1gMnSc1	státník
<g/>
,	,	kIx,	,
císař	císař	k1gMnSc1	císař
v	v	k7c6	v
letech	let	k1gInPc6	let
1804	[number]	k4	1804
<g/>
–	–	k?	–
<g/>
1814	[number]	k4	1814
a	a	k8xC	a
poté	poté	k6eAd1	poté
sto	sto	k4xCgNnSc1	sto
dní	den	k1gInPc2	den
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
jara	jaro	k1gNnSc2	jaro
a	a	k8xC	a
léta	léto	k1gNnSc2	léto
1815	[number]	k4	1815
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
udělal	udělat	k5eAaPmAgInS	udělat
závratnou	závratný	k2eAgFnSc4d1	závratná
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
:	:	kIx,	:
ve	v	k7c6	v
24	[number]	k4	24
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgMnS	být
generálem	generál	k1gMnSc7	generál
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
třicítce	třicítka	k1gFnSc6	třicítka
prvním	první	k4xOgMnSc7	první
mužem	muž	k1gMnSc7	muž
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
a	a	k8xC	a
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
své	svůj	k3xOyFgFnSc2	svůj
moci	moc	k1gFnSc2	moc
ovládal	ovládat	k5eAaImAgInS	ovládat
většinu	většina	k1gFnSc4	většina
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Rychlý	rychlý	k2eAgInSc1d1	rychlý
byl	být	k5eAaImAgInS	být
i	i	k9	i
jeho	jeho	k3xOp3gInSc1	jeho
pád	pád	k1gInSc1	pád
<g/>
;	;	kIx,	;
závěr	závěr	k1gInSc1	závěr
života	život	k1gInSc2	život
strávil	strávit	k5eAaPmAgInS	strávit
ve	v	k7c6	v
vyhnanství	vyhnanství	k1gNnSc6	vyhnanství
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	napoleon	k1gInSc1	napoleon
bývá	bývat	k5eAaImIp3nS	bývat
vzorem	vzor	k1gInSc7	vzor
vojevůdcovských	vojevůdcovský	k2eAgFnPc2d1	vojevůdcovská
schopností	schopnost	k1gFnPc2	schopnost
a	a	k8xC	a
cílevědomosti	cílevědomost	k1gFnSc2	cílevědomost
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
však	však	k9	však
projevoval	projevovat	k5eAaImAgMnS	projevovat
lhostejnost	lhostejnost	k1gFnSc4	lhostejnost
k	k	k7c3	k
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc2	jenž
viděl	vidět	k5eAaImAgMnS	vidět
jen	jen	k9	jen
prostředky	prostředek	k1gInPc1	prostředek
svých	svůj	k3xOyFgInPc2	svůj
cílů	cíl	k1gInPc2	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Dokázal	dokázat	k5eAaPmAgInS	dokázat
národ	národ	k1gInSc1	národ
vybičovat	vybičovat	k5eAaPmF	vybičovat
k	k	k7c3	k
nesmírnému	smírný	k2eNgNnSc3d1	nesmírné
úsilí	úsilí	k1gNnSc3	úsilí
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
obrovských	obrovský	k2eAgFnPc2d1	obrovská
obětí	oběť	k1gFnPc2	oběť
na	na	k7c6	na
lidských	lidský	k2eAgInPc6d1	lidský
životech	život	k1gInPc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
francouzských	francouzský	k2eAgMnPc2d1	francouzský
občanů	občan	k1gMnPc2	občan
během	během	k7c2	během
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
více	hodně	k6eAd2	hodně
než	než	k8xS	než
milion	milion	k4xCgInSc4	milion
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
na	na	k7c6	na
Korsice	Korsika	k1gFnSc6	Korsika
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
nepříliš	příliš	k6eNd1	příliš
zámožného	zámožný	k2eAgMnSc4d1	zámožný
příslušníka	příslušník	k1gMnSc4	příslušník
úřednické	úřednický	k2eAgFnSc2d1	úřednická
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
devíti	devět	k4xCc6	devět
letech	léto	k1gNnPc6	léto
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jako	jako	k9	jako
stipendista	stipendista	k1gMnSc1	stipendista
vzdělával	vzdělávat	k5eAaImAgMnS	vzdělávat
na	na	k7c6	na
vojenských	vojenský	k2eAgFnPc6d1	vojenská
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
16	[number]	k4	16
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
převzal	převzít	k5eAaPmAgMnS	převzít
starost	starost	k1gFnSc4	starost
o	o	k7c4	o
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
předčasně	předčasně	k6eAd1	předčasně
dokončil	dokončit	k5eAaPmAgInS	dokončit
studia	studio	k1gNnPc4	studio
a	a	k8xC	a
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
podporučíka	podporučík	k1gMnSc2	podporučík
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
dělostřelecký	dělostřelecký	k2eAgMnSc1d1	dělostřelecký
důstojník	důstojník	k1gMnSc1	důstojník
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1793	[number]	k4	1793
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c6	o
dobytí	dobytí	k1gNnSc6	dobytí
pevnosti	pevnost	k1gFnSc2	pevnost
Toulon	Toulon	k1gInSc4	Toulon
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
byl	být	k5eAaImAgInS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
brigádního	brigádní	k2eAgMnSc2d1	brigádní
generála	generál	k1gMnSc2	generál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1795	[number]	k4	1795
úspěšně	úspěšně	k6eAd1	úspěšně
potlačil	potlačit	k5eAaPmAgMnS	potlačit
roajalistické	roajalistický	k2eAgNnSc4d1	roajalistické
povstání	povstání	k1gNnSc4	povstání
v	v	k7c6	v
pařížských	pařížský	k2eAgFnPc6d1	Pařížská
ulicích	ulice	k1gFnPc6	ulice
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
divizním	divizní	k2eAgMnSc7d1	divizní
generálem	generál	k1gMnSc7	generál
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
direktoriem	direktorium	k1gNnSc7	direktorium
vyslán	vyslat	k5eAaPmNgInS	vyslat
do	do	k7c2	do
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Rakouskem	Rakousko	k1gNnSc7	Rakousko
a	a	k8xC	a
pověřen	pověřit	k5eAaPmNgInS	pověřit
velením	velení	k1gNnSc7	velení
nad	nad	k7c7	nad
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
armádou	armáda	k1gFnSc7	armáda
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
chopil	chopit	k5eAaPmAgMnS	chopit
nabízené	nabízený	k2eAgFnSc3d1	nabízená
příležitosti	příležitost	k1gFnSc3	příležitost
a	a	k8xC	a
řadou	řada	k1gFnSc7	řada
vítězství	vítězství	k1gNnSc2	vítězství
přinutil	přinutit	k5eAaPmAgInS	přinutit
Rakousko	Rakousko	k1gNnSc4	Rakousko
požádat	požádat	k5eAaPmF	požádat
o	o	k7c4	o
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1799	[number]	k4	1799
<g/>
,	,	kIx,	,
po	po	k7c6	po
nepříliš	příliš	k6eNd1	příliš
vydařeném	vydařený	k2eAgNnSc6d1	vydařené
tažení	tažení	k1gNnSc6	tažení
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Napoleon	Napoleon	k1gMnSc1	Napoleon
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
politického	politický	k2eAgInSc2d1	politický
převratu	převrat	k1gInSc2	převrat
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
omezit	omezit	k5eAaPmF	omezit
moc	moc	k6eAd1	moc
dvou	dva	k4xCgFnPc2	dva
zákonodárných	zákonodárný	k2eAgFnPc2d1	zákonodárná
sněmoven	sněmovna	k1gFnPc2	sněmovna
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
mnohem	mnohem	k6eAd1	mnohem
silnější	silný	k2eAgFnSc2d2	silnější
výkonné	výkonný	k2eAgFnSc2d1	výkonná
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jej	on	k3xPp3gMnSc4	on
vyneslo	vynést	k5eAaPmAgNnS	vynést
do	do	k7c2	do
pozice	pozice	k1gFnSc2	pozice
vůdčího	vůdčí	k2eAgInSc2d1	vůdčí
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
vládnoucích	vládnoucí	k2eAgMnPc2d1	vládnoucí
konzulů	konzul	k1gMnPc2	konzul
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
zastával	zastávat	k5eAaImAgMnS	zastávat
doživotní	doživotní	k2eAgFnSc4d1	doživotní
funkci	funkce	k1gFnSc4	funkce
prvního	první	k4xOgMnSc2	první
konzula	konzul	k1gMnSc2	konzul
a	a	k8xC	a
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
přiměl	přimět	k5eAaPmAgInS	přimět
senát	senát	k1gInSc1	senát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gMnSc4	on
zvolil	zvolit	k5eAaPmAgMnS	zvolit
francouzským	francouzský	k2eAgMnSc7d1	francouzský
císařem	císař	k1gMnSc7	císař
<g/>
.	.	kIx.	.
</s>
<s>
Vládu	vláda	k1gFnSc4	vláda
si	se	k3xPyFc3	se
za	za	k7c4	za
téměř	téměř	k6eAd1	téměř
nepřetržitého	přetržitý	k2eNgInSc2d1	nepřetržitý
válečného	válečný	k2eAgInSc2d1	válečný
stavu	stav	k1gInSc2	stav
podržel	podržet	k5eAaPmAgInS	podržet
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
krachu	krach	k1gInSc6	krach
tažení	tažení	k1gNnSc2	tažení
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
vojsky	vojsky	k6eAd1	vojsky
VI	VI	kA	VI
<g/>
.	.	kIx.	.
koalice	koalice	k1gFnSc2	koalice
zatlačen	zatlačen	k2eAgInSc1d1	zatlačen
až	až	k9	až
k	k	k7c3	k
francouzské	francouzský	k2eAgFnSc3d1	francouzská
metropoli	metropol	k1gFnSc3	metropol
a	a	k8xC	a
vlastními	vlastní	k2eAgMnPc7d1	vlastní
maršály	maršál	k1gMnPc7	maršál
přinucen	přinutit	k5eAaPmNgMnS	přinutit
abdikovat	abdikovat	k5eAaBmF	abdikovat
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
byl	být	k5eAaImAgMnS	být
poslán	poslat	k5eAaPmNgMnS	poslat
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Elba	Elb	k1gInSc2	Elb
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
po	po	k7c6	po
necelém	celý	k2eNgInSc6d1	necelý
roce	rok	k1gInSc6	rok
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
bez	bez	k7c2	bez
jediného	jediný	k2eAgInSc2d1	jediný
výstřelu	výstřel	k1gInSc2	výstřel
opět	opět	k6eAd1	opět
obsadil	obsadit	k5eAaPmAgMnS	obsadit
císařský	císařský	k2eAgInSc4d1	císařský
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
se	se	k3xPyFc4	se
však	však	k9	však
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
vojenskopolitické	vojenskopolitický	k2eAgFnSc2d1	vojenskopolitická
izolace	izolace	k1gFnSc2	izolace
a	a	k8xC	a
obnovené	obnovený	k2eAgNnSc1d1	obnovené
císařství	císařství	k1gNnSc1	císařství
přetrvalo	přetrvat	k5eAaPmAgNnS	přetrvat
110	[number]	k4	110
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Napoleonova	Napoleonův	k2eAgFnSc1d1	Napoleonova
vláda	vláda	k1gFnSc1	vláda
skončila	skončit	k5eAaPmAgFnS	skončit
porážkou	porážka	k1gFnSc7	porážka
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Waterloo	Waterloo	k1gNnSc2	Waterloo
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
abdikoval	abdikovat	k5eAaBmAgMnS	abdikovat
podruhé	podruhé	k6eAd1	podruhé
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
se	se	k3xPyFc4	se
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
jej	on	k3xPp3gNnSc4	on
odsoudili	odsoudit	k5eAaPmAgMnP	odsoudit
k	k	k7c3	k
doživotnímu	doživotní	k2eAgNnSc3d1	doživotní
vyhnanství	vyhnanství	k1gNnSc3	vyhnanství
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Svatá	svatý	k2eAgFnSc1d1	svatá
Helena	Helena	k1gFnSc1	Helena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
po	po	k7c6	po
necelých	celý	k2eNgNnPc6d1	necelé
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
pobytu	pobyt	k1gInSc2	pobyt
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
jednapadesáti	jednapadesát	k4xCc2	jednapadesát
let	léto	k1gNnPc2	léto
zemřel	zemřít	k5eAaPmAgInS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
Napoleon	napoleon	k1gInSc1	napoleon
svedl	svést	k5eAaPmAgInS	svést
okolo	okolo	k7c2	okolo
šedesáti	šedesát	k4xCc2	šedesát
bitev	bitva	k1gFnPc2	bitva
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
více	hodně	k6eAd2	hodně
než	než	k8xS	než
Alexandr	Alexandr	k1gMnSc1	Alexandr
Makedonský	makedonský	k2eAgMnSc1d1	makedonský
<g/>
,	,	kIx,	,
Hannibal	Hannibal	k1gInSc1	Hannibal
<g/>
,	,	kIx,	,
Caesar	Caesar	k1gMnSc1	Caesar
a	a	k8xC	a
Alexandr	Alexandr	k1gMnSc1	Alexandr
Suvorov	Suvorovo	k1gNnPc2	Suvorovo
dohromady	dohromady	k6eAd1	dohromady
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c4	po
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc4d1	celé
další	další	k2eAgNnSc4d1	další
století	století	k1gNnSc4	století
byla	být	k5eAaImAgFnS	být
vojenská	vojenský	k2eAgFnSc1d1	vojenská
teorie	teorie	k1gFnSc1	teorie
i	i	k8xC	i
praxe	praxe	k1gFnSc1	praxe
posuzována	posuzován	k2eAgFnSc1d1	posuzována
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
pravidel	pravidlo	k1gNnPc2	pravidlo
a	a	k8xC	a
přizpůsobována	přizpůsobován	k2eAgNnPc4d1	přizpůsobováno
jeho	jeho	k3xOp3gInPc4	jeho
pojetí	pojetí	k1gNnSc1	pojetí
válečnictví	válečnictví	k1gNnSc2	válečnictví
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Napoleonovy	Napoleonův	k2eAgFnSc2d1	Napoleonova
éry	éra	k1gFnSc2	éra
se	se	k3xPyFc4	se
Francie	Francie	k1gFnSc1	Francie
změnila	změnit	k5eAaPmAgFnS	změnit
ze	z	k7c2	z
stavovského	stavovský	k2eAgInSc2d1	stavovský
feudálního	feudální	k2eAgInSc2d1	feudální
státu	stát	k1gInSc2	stát
v	v	k7c6	v
sociálně	sociálně	k6eAd1	sociálně
i	i	k8xC	i
občansky	občansky	k6eAd1	občansky
nově	nově	k6eAd1	nově
strukturovanou	strukturovaný	k2eAgFnSc4d1	strukturovaná
společnost	společnost	k1gFnSc4	společnost
a	a	k8xC	a
určovala	určovat	k5eAaImAgFnS	určovat
politický	politický	k2eAgInSc4d1	politický
trend	trend	k1gInSc4	trend
ve	v	k7c6	v
značné	značný	k2eAgFnSc6d1	značná
části	část	k1gFnSc6	část
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
i	i	k9	i
obsáhlou	obsáhlý	k2eAgFnSc4d1	obsáhlá
reformu	reforma	k1gFnSc4	reforma
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
správy	správa	k1gFnSc2	správa
země	zem	k1gFnSc2	zem
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1804	[number]	k4	1804
vydal	vydat	k5eAaPmAgInS	vydat
nový	nový	k2eAgInSc1d1	nový
občanský	občanský	k2eAgInSc1d1	občanský
zákoník	zákoník	k1gInSc1	zákoník
(	(	kIx(	(
<g/>
Code	Code	k1gFnSc1	Code
civil	civil	k1gMnSc1	civil
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
vzorem	vzor	k1gInSc7	vzor
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
evropské	evropský	k2eAgFnPc4d1	Evropská
země	zem	k1gFnPc4	zem
a	a	k8xC	a
francouzské	francouzský	k2eAgNnSc4d1	francouzské
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
dodnes	dodnes	k6eAd1	dodnes
navazuje	navazovat	k5eAaImIp3nS	navazovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzestup	vzestup	k1gInSc4	vzestup
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mládí	mládí	k1gNnSc1	mládí
===	===	k?	===
</s>
</p>
<p>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
(	(	kIx(	(
<g/>
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Napoleone	Napoleon	k1gMnSc5	Napoleon
di	di	k?	di
Buonaparte	Buonapart	k1gInSc5	Buonapart
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
v	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1769	[number]	k4	1769
v	v	k7c6	v
korsickém	korsický	k2eAgNnSc6d1	korsické
městě	město	k1gNnSc6	město
Ajaccio	Ajaccio	k6eAd1	Ajaccio
jako	jako	k8xS	jako
druhý	druhý	k4xOgMnSc1	druhý
syn	syn	k1gMnSc1	syn
nepříliš	příliš	k6eNd1	příliš
zámožného	zámožný	k2eAgMnSc4d1	zámožný
příslušníka	příslušník	k1gMnSc4	příslušník
úřednické	úřednický	k2eAgFnSc2d1	úřednická
šlechty	šlechta	k1gFnSc2	šlechta
advokáta	advokát	k1gMnSc2	advokát
Carla	Carl	k1gMnSc2	Carl
Buonaparta	Buonapart	k1gMnSc2	Buonapart
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
Laetitie	Laetitie	k1gFnSc2	Laetitie
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Ramolino	Ramolin	k2eAgNnSc1d1	Ramolin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
jej	on	k3xPp3gMnSc4	on
ovlivnili	ovlivnit	k5eAaPmAgMnP	ovlivnit
korsičtí	korsický	k2eAgMnPc1d1	korsický
nacionalisté	nacionalista	k1gMnPc1	nacionalista
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
Francouze	Francouz	k1gMnSc4	Francouz
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
cizí	cizí	k2eAgMnPc4d1	cizí
utlačovatele	utlačovatel	k1gMnPc4	utlačovatel
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
byl	být	k5eAaImAgInS	být
totiž	totiž	k9	totiž
vojensky	vojensky	k6eAd1	vojensky
připojen	připojit	k5eAaPmNgInS	připojit
k	k	k7c3	k
Francii	Francie	k1gFnSc3	Francie
jen	jen	k9	jen
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
před	před	k7c7	před
Napoleonovým	Napoleonův	k2eAgNnSc7d1	Napoleonovo
narozením	narození	k1gNnSc7	narození
a	a	k8xC	a
obyvatelé	obyvatel	k1gMnPc1	obyvatel
litovali	litovat	k5eAaImAgMnP	litovat
ztráty	ztráta	k1gFnPc4	ztráta
politické	politický	k2eAgFnSc2d1	politická
samostatnosti	samostatnost	k1gFnSc2	samostatnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
aristokracií	aristokracie	k1gFnSc7	aristokracie
žila	žít	k5eAaImAgFnS	žít
rodina	rodina	k1gFnSc1	rodina
Buonapartů	Buonapart	k1gMnPc2	Buonapart
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
skromných	skromný	k2eAgInPc6d1	skromný
poměrech	poměr	k1gInPc6	poměr
<g/>
,	,	kIx,	,
nouzí	nouze	k1gFnPc2	nouze
však	však	k8xC	však
netrpěla	trpět	k5eNaImAgFnS	trpět
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
asesorem	asesor	k1gMnSc7	asesor
(	(	kIx(	(
<g/>
poradcem	poradce	k1gMnSc7	poradce
<g/>
)	)	kIx)	)
královského	královský	k2eAgInSc2d1	královský
soudu	soud	k1gInSc2	soud
a	a	k8xC	a
ve	v	k7c6	v
volných	volný	k2eAgFnPc6d1	volná
chvílích	chvíle	k1gFnPc6	chvíle
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
zejména	zejména	k9	zejména
literární	literární	k2eAgFnSc3d1	literární
činnosti	činnost	k1gFnSc3	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
výchovu	výchova	k1gFnSc4	výchova
osmi	osm	k4xCc2	osm
dětí	dítě	k1gFnPc2	dítě
dohlížela	dohlížet	k5eAaImAgFnS	dohlížet
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
vychovala	vychovat	k5eAaPmAgFnS	vychovat
s	s	k7c7	s
láskou	láska	k1gFnSc7	láska
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
poměrně	poměrně	k6eAd1	poměrně
tvrdě	tvrdě	k6eAd1	tvrdě
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
měla	mít	k5eAaImAgFnS	mít
své	svůj	k3xOyFgInPc4	svůj
důvody	důvod	k1gInPc4	důvod
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
dosvědčil	dosvědčit	k5eAaPmAgMnS	dosvědčit
později	pozdě	k6eAd2	pozdě
sám	sám	k3xTgMnSc1	sám
Napoleon	Napoleon	k1gMnSc1	Napoleon
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
devět	devět	k4xCc4	devět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
odvezl	odvézt	k5eAaPmAgMnS	odvézt
jej	on	k3xPp3gMnSc4	on
otec	otec	k1gMnSc1	otec
i	i	k9	i
s	s	k7c7	s
jeho	jeho	k3xOp3gMnSc7	jeho
starším	starý	k2eAgMnSc7d2	starší
bratrem	bratr	k1gMnSc7	bratr
Josefem	Josef	k1gMnSc7	Josef
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
ho	on	k3xPp3gMnSc4	on
nejprve	nejprve	k6eAd1	nejprve
umístil	umístit	k5eAaPmAgMnS	umístit
v	v	k7c6	v
koleji	kolej	k1gFnSc6	kolej
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Autun	Autuna	k1gFnPc2	Autuna
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgMnS	být
jako	jako	k8xC	jako
stipendista	stipendista	k1gMnSc1	stipendista
přijat	přijmout	k5eAaPmNgMnS	přijmout
do	do	k7c2	do
vojenského	vojenský	k2eAgNnSc2d1	vojenské
učiliště	učiliště	k1gNnSc2	učiliště
v	v	k7c6	v
Brienne-le-Château	Briennee-Châteaus	k1gInSc6	Brienne-le-Châteaus
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
poté	poté	k6eAd1	poté
strávil	strávit	k5eAaPmAgMnS	strávit
Napoleon	Napoleon	k1gMnSc1	Napoleon
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pozdějšího	pozdní	k2eAgMnSc2d2	pozdější
ministra	ministr	k1gMnSc2	ministr
Bourrienna	Bourrienn	k1gMnSc2	Bourrienn
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
jeho	jeho	k3xOp3gMnSc7	jeho
přítelem	přítel	k1gMnSc7	přítel
<g/>
,	,	kIx,	,
projevoval	projevovat	k5eAaImAgMnS	projevovat
Napoleon	Napoleon	k1gMnSc1	Napoleon
mimořádné	mimořádný	k2eAgNnSc4d1	mimořádné
nadání	nadání	k1gNnSc4	nadání
v	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
předmětu	předmět	k1gInSc6	předmět
vždy	vždy	k6eAd1	vždy
první	první	k4xOgNnPc1	první
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
škole	škola	k1gFnSc6	škola
však	však	k9	však
vytrpěl	vytrpět	k5eAaPmAgMnS	vytrpět
mnoho	mnoho	k6eAd1	mnoho
od	od	k7c2	od
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
posmívali	posmívat	k5eAaImAgMnP	posmívat
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
malou	malý	k2eAgFnSc4d1	malá
výšku	výška	k1gFnSc4	výška
a	a	k8xC	a
špatnou	špatný	k2eAgFnSc4d1	špatná
francouzštinu	francouzština	k1gFnSc4	francouzština
<g/>
,	,	kIx,	,
nacož	nacož	k6eAd1	nacož
Napoleon	Napoleon	k1gMnSc1	Napoleon
reagoval	reagovat	k5eAaBmAgMnS	reagovat
slovními	slovní	k2eAgInPc7d1	slovní
i	i	k8xC	i
fyzickými	fyzický	k2eAgInPc7d1	fyzický
útoky	útok	k1gInPc7	útok
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tuto	tento	k3xDgFnSc4	tento
nepřízeň	nepřízeň	k1gFnSc4	nepřízeň
pilně	pilně	k6eAd1	pilně
studoval	studovat	k5eAaImAgMnS	studovat
a	a	k8xC	a
mimo	mimo	k7c4	mimo
matematiku	matematika	k1gFnSc4	matematika
měl	mít	k5eAaImAgMnS	mít
výborné	výborný	k2eAgFnPc4d1	výborná
známky	známka	k1gFnPc4	známka
také	také	k9	také
ze	z	k7c2	z
zeměpisu	zeměpis	k1gInSc2	zeměpis
<g/>
,	,	kIx,	,
dějepisu	dějepis	k1gInSc2	dějepis
i	i	k8xC	i
jiných	jiný	k2eAgInPc2d1	jiný
předmětů	předmět	k1gInPc2	předmět
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
latiny	latina	k1gFnSc2	latina
a	a	k8xC	a
němčiny	němčina	k1gFnSc2	němčina
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
patnácti	patnáct	k4xCc6	patnáct
letech	let	k1gInPc6	let
věku	věk	k1gInSc2	věk
Napoleon	Napoleon	k1gMnSc1	Napoleon
své	svůj	k3xOyFgNnSc4	svůj
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
v	v	k7c6	v
Brienne-le-Château	Briennee-Châteaum	k1gNnSc6	Brienne-le-Châteaum
úspěšně	úspěšně	k6eAd1	úspěšně
ukončil	ukončit	k5eAaPmAgInS	ukončit
a	a	k8xC	a
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1784	[number]	k4	1784
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c4	na
prestižní	prestižní	k2eAgFnSc4d1	prestižní
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
(	(	kIx(	(
<g/>
École	Écol	k1gMnSc5	Écol
Militaire	Militair	k1gMnSc5	Militair
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Specializoval	specializovat	k5eAaBmAgMnS	specializovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
dělostřelectví	dělostřelectví	k1gNnSc2	dělostřelectví
<g/>
,	,	kIx,	,
po	po	k7c6	po
jednom	jeden	k4xCgInSc6	jeden
roce	rok	k1gInSc6	rok
studií	studie	k1gFnPc2	studie
úspěšně	úspěšně	k6eAd1	úspěšně
složil	složit	k5eAaPmAgMnS	složit
zkoušky	zkouška	k1gFnPc4	zkouška
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
poslán	poslat	k5eAaPmNgMnS	poslat
jako	jako	k8xS	jako
podporučík	podporučík	k1gMnSc1	podporučík
do	do	k7c2	do
pluku	pluk	k1gInSc2	pluk
ležícího	ležící	k2eAgInSc2d1	ležící
ve	v	k7c6	v
Valence	valence	k1gFnSc1	valence
nedaleko	nedaleko	k7c2	nedaleko
Lyonu	Lyon	k1gInSc2	Lyon
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
již	již	k6eAd1	již
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1785	[number]	k4	1785
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
ještě	ještě	k9	ještě
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
dorazila	dorazit	k5eAaPmAgFnS	dorazit
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
začal	začít	k5eAaPmAgMnS	začít
své	svůj	k3xOyFgFnSc3	svůj
matce	matka	k1gFnSc3	matka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zůstala	zůstat	k5eAaPmAgFnS	zůstat
s	s	k7c7	s
malými	malý	k2eAgFnPc7d1	malá
dětmi	dítě	k1gFnPc7	dítě
téměř	téměř	k6eAd1	téměř
bez	bez	k7c2	bez
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
finančně	finančně	k6eAd1	finančně
vypomáhat	vypomáhat	k5eAaImF	vypomáhat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
deseti	deset	k4xCc6	deset
měsících	měsíc	k1gInPc6	měsíc
služby	služba	k1gFnSc2	služba
ve	v	k7c4	v
Valence	valence	k1gFnPc4	valence
si	se	k3xPyFc3	se
vymohl	vymoct	k5eAaPmAgInS	vymoct
v	v	k7c6	v
září	září	k1gNnSc6	září
1786	[number]	k4	1786
dovolenou	dovolená	k1gFnSc4	dovolená
k	k	k7c3	k
cestě	cesta	k1gFnSc3	cesta
na	na	k7c4	na
Korsiku	Korsika	k1gFnSc4	Korsika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pobyl	pobýt	k5eAaPmAgMnS	pobýt
více	hodně	k6eAd2	hodně
než	než	k8xS	než
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
vyřizování	vyřizování	k1gNnSc3	vyřizování
soudních	soudní	k2eAgInPc2d1	soudní
sporů	spor	k1gInPc2	spor
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
sice	sice	k8xC	sice
odjel	odjet	k5eAaPmAgMnS	odjet
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1787	[number]	k4	1787
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1788	[number]	k4	1788
opět	opět	k6eAd1	opět
do	do	k7c2	do
Ajaccia	Ajaccium	k1gNnSc2	Ajaccium
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
pluku	pluk	k1gInSc3	pluk
La	la	k1gNnSc1	la
Fè	Fè	k1gFnSc2	Fè
<g/>
–	–	k?	–
<g/>
Artillerie	Artillerie	k1gFnSc2	Artillerie
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
přemístil	přemístit	k5eAaPmAgInS	přemístit
do	do	k7c2	do
Auxonne	Auxonn	k1gInSc5	Auxonn
v	v	k7c6	v
Burgundsku	Burgundsko	k1gNnSc6	Burgundsko
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
zpět	zpět	k6eAd1	zpět
až	až	k9	až
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1788	[number]	k4	1788
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
služby	služba	k1gFnSc2	služba
u	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
pluku	pluk	k1gInSc2	pluk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
nyní	nyní	k6eAd1	nyní
posádkou	posádka	k1gFnSc7	posádka
při	při	k7c6	při
dělostřelecké	dělostřelecký	k2eAgFnSc6d1	dělostřelecká
škole	škola	k1gFnSc6	škola
<g/>
,	,	kIx,	,
posílal	posílat	k5eAaImAgMnS	posílat
Napoleon	Napoleon	k1gMnSc1	Napoleon
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgInSc2	svůj
platu	plat	k1gInSc2	plat
matce	matka	k1gFnSc3	matka
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volném	volný	k2eAgInSc6d1	volný
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
soukromému	soukromý	k2eAgNnSc3d1	soukromé
studiu	studio	k1gNnSc3	studio
vojenské	vojenský	k2eAgFnSc2d1	vojenská
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
zeměpisu	zeměpis	k1gInSc2	zeměpis
<g/>
,	,	kIx,	,
filosofie	filosofie	k1gFnSc2	filosofie
a	a	k8xC	a
osvícenské	osvícenský	k2eAgFnSc2d1	osvícenská
literatury	literatura	k1gFnSc2	literatura
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zahájení	zahájení	k1gNnSc1	zahájení
kariéry	kariéra	k1gFnSc2	kariéra
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Auxonne	Auxonn	k1gInSc5	Auxonn
zastihly	zastihnout	k5eAaPmAgInP	zastihnout
Napoleona	Napoleon	k1gMnSc2	Napoleon
události	událost	k1gFnSc2	událost
Velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pluk	pluk	k1gInSc1	pluk
La	la	k1gNnSc2	la
Fè	Fè	k1gFnSc2	Fè
<g/>
–	–	k?	–
<g/>
Artillerie	Artillerie	k1gFnSc2	Artillerie
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgMnS	připojit
k	k	k7c3	k
povstalcům	povstalec	k1gMnPc3	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
otevřela	otevřít	k5eAaPmAgFnS	otevřít
cesta	cesta	k1gFnSc1	cesta
pro	pro	k7c4	pro
politickou	politický	k2eAgFnSc4d1	politická
kariéru	kariéra	k1gFnSc4	kariéra
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
a	a	k8xC	a
půl	půl	k1xP	půl
měsíce	měsíc	k1gInSc2	měsíc
po	po	k7c6	po
vzpouře	vzpoura	k1gFnSc6	vzpoura
jednotky	jednotka	k1gFnSc2	jednotka
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
další	další	k2eAgFnSc4d1	další
dovolenou	dovolená	k1gFnSc4	dovolená
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Ajaccia	Ajaccius	k1gMnSc2	Ajaccius
dorazil	dorazit	k5eAaPmAgMnS	dorazit
opět	opět	k6eAd1	opět
koncem	koncem	k7c2	koncem
září	září	k1gNnSc2	září
1789	[number]	k4	1789
a	a	k8xC	a
neprodleně	prodleně	k6eNd1	prodleně
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
místního	místní	k2eAgNnSc2d1	místní
politického	politický	k2eAgNnSc2d1	politické
dění	dění	k1gNnSc2	dění
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Josefem	Josef	k1gMnSc7	Josef
stál	stát	k5eAaImAgMnS	stát
u	u	k7c2	u
počátků	počátek	k1gInPc2	počátek
revolučního	revoluční	k2eAgNnSc2d1	revoluční
hnutí	hnutí	k1gNnSc2	hnutí
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
písemnou	písemný	k2eAgFnSc4d1	písemná
výzvu	výzva	k1gFnSc4	výzva
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
odsouhlasilo	odsouhlasit	k5eAaPmAgNnS	odsouhlasit
zrovnoprávnění	zrovnoprávnění	k1gNnSc4	zrovnoprávnění
Korsiky	Korsika	k1gFnSc2	Korsika
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
částmi	část	k1gFnPc7	část
francouzského	francouzský	k2eAgNnSc2d1	francouzské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
amnestie	amnestie	k1gFnSc1	amnestie
všem	všecek	k3xTgMnPc3	všecek
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
kdysi	kdysi	k6eAd1	kdysi
bojovali	bojovat	k5eAaImAgMnP	bojovat
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
návrat	návrat	k1gInSc4	návrat
i	i	k8xC	i
někdejšímu	někdejší	k2eAgMnSc3d1	někdejší
vůdci	vůdce	k1gMnSc3	vůdce
protifrancouzského	protifrancouzský	k2eAgInSc2d1	protifrancouzský
odboje	odboj	k1gInSc2	odboj
Pascalu	Pascal	k1gMnSc3	Pascal
Paolimu	Paolim	k1gMnSc3	Paolim
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
Napoleona	Napoleon	k1gMnSc4	Napoleon
po	po	k7c4	po
celé	celý	k2eAgNnSc4d1	celé
jeho	jeho	k3xOp3gNnSc4	jeho
dětství	dětství	k1gNnSc4	dětství
ústředním	ústřední	k2eAgMnSc7d1	ústřední
hrdinou	hrdina	k1gMnSc7	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
starší	starý	k2eAgMnSc1d2	starší
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
Korsičané	Korsičan	k1gMnPc1	Korsičan
záhy	záhy	k6eAd1	záhy
zvolili	zvolit	k5eAaPmAgMnP	zvolit
předsedou	předseda	k1gMnSc7	předseda
direktoria	direktorium	k1gNnPc4	direktorium
departmentu	department	k1gInSc2	department
a	a	k8xC	a
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
vůči	vůči	k7c3	vůči
svému	svůj	k3xOyFgMnSc3	svůj
obdivovateli	obdivovatel	k1gMnSc3	obdivovatel
velmi	velmi	k6eAd1	velmi
chladně	chladně	k6eAd1	chladně
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnPc1	jejich
cesty	cesta	k1gFnPc1	cesta
se	se	k3xPyFc4	se
neshodují	shodovat	k5eNaImIp3nP	shodovat
<g/>
.	.	kIx.	.
</s>
<s>
Snahou	snaha	k1gFnSc7	snaha
Paoliho	Paoli	k1gMnSc4	Paoli
byla	být	k5eAaImAgFnS	být
korsická	korsický	k2eAgFnSc1d1	Korsická
samostatnost	samostatnost	k1gFnSc1	samostatnost
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
Napoleon	Napoleon	k1gMnSc1	Napoleon
viděl	vidět	k5eAaImAgMnS	vidět
v	v	k7c6	v
revoluci	revoluce	k1gFnSc6	revoluce
nové	nový	k2eAgFnSc2d1	nová
možnosti	možnost	k1gFnSc2	možnost
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
tedy	tedy	k9	tedy
mladý	mladý	k2eAgMnSc1d1	mladý
dělostřelecký	dělostřelecký	k2eAgMnSc1d1	dělostřelecký
důstojník	důstojník	k1gMnSc1	důstojník
po	po	k7c6	po
několikaměsíčním	několikaměsíční	k2eAgInSc6d1	několikaměsíční
pobytu	pobyt	k1gInSc6	pobyt
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
rodišti	rodiště	k1gNnSc6	rodiště
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
veškeré	veškerý	k3xTgFnPc1	veškerý
jeho	jeho	k3xOp3gFnPc1	jeho
snahy	snaha	k1gFnPc1	snaha
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
bez	bez	k7c2	bez
větších	veliký	k2eAgInPc2d2	veliký
výsledků	výsledek	k1gInPc2	výsledek
<g/>
,	,	kIx,	,
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Auxonne	Auxonn	k1gInSc5	Auxonn
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
matce	matka	k1gFnSc3	matka
ulehčil	ulehčit	k5eAaPmAgInS	ulehčit
v	v	k7c6	v
rodinných	rodinný	k2eAgInPc6d1	rodinný
výdajích	výdaj	k1gInPc6	výdaj
<g/>
,	,	kIx,	,
odvážel	odvážet	k5eAaImAgMnS	odvážet
s	s	k7c7	s
sebou	se	k3xPyFc7	se
i	i	k8xC	i
mladšího	mladý	k2eAgMnSc4d2	mladší
bratra	bratr	k1gMnSc4	bratr
Ludvíka	Ludvík	k1gMnSc4	Ludvík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
své	svůj	k3xOyFgFnSc2	svůj
jednotky	jednotka	k1gFnSc2	jednotka
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1791	[number]	k4	1791
<g/>
,	,	kIx,	,
po	po	k7c6	po
šesti	šest	k4xCc6	šest
letech	léto	k1gNnPc6	léto
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
povýšen	povýšen	k2eAgMnSc1d1	povýšen
na	na	k7c4	na
poručíka	poručík	k1gMnSc4	poručík
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
převelen	převelet	k5eAaPmNgMnS	převelet
ke	k	k7c3	k
4	[number]	k4	4
<g/>
.	.	kIx.	.
dělostřeleckému	dělostřelecký	k2eAgInSc3d1	dělostřelecký
pluku	pluk	k1gInSc3	pluk
ve	v	k7c4	v
Valence	valence	k1gFnPc4	valence
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
stálo	stát	k5eAaImAgNnS	stát
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
jeho	jeho	k3xOp3gFnSc2	jeho
vojenské	vojenský	k2eAgFnSc2d1	vojenská
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
jakobínského	jakobínský	k2eAgInSc2d1	jakobínský
Spolku	spolek	k1gInSc2	spolek
přátel	přítel	k1gMnPc2	přítel
konstituce	konstituce	k1gFnSc2	konstituce
a	a	k8xC	a
záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc7	jeho
tajemníkem	tajemník	k1gMnSc7	tajemník
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
po	po	k7c6	po
povýšení	povýšení	k1gNnSc6	povýšení
požádal	požádat	k5eAaPmAgInS	požádat
o	o	k7c4	o
další	další	k2eAgFnSc4d1	další
dovolenou	dovolená	k1gFnSc4	dovolená
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
dvanáctitýdenní	dvanáctitýdenní	k2eAgFnSc1d1	dvanáctitýdenní
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
na	na	k7c4	na
Korsiku	Korsika	k1gFnSc4	Korsika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
zvolit	zvolit	k5eAaPmF	zvolit
podplukovníkem	podplukovník	k1gMnSc7	podplukovník
praporu	prapor	k1gInSc2	prapor
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
jeho	on	k3xPp3gInSc4	on
odchod	odchod	k1gInSc4	odchod
z	z	k7c2	z
dělostřeleckého	dělostřelecký	k2eAgInSc2d1	dělostřelecký
pluku	pluk	k1gInSc2	pluk
<g/>
)	)	kIx)	)
a	a	k8xC	a
definitivně	definitivně	k6eAd1	definitivně
se	se	k3xPyFc4	se
odcizil	odcizit	k5eAaPmAgMnS	odcizit
s	s	k7c7	s
Pascalem	pascal	k1gInSc7	pascal
Paolim	Paolima	k1gFnPc2	Paolima
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	s	k7c7	s
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1792	[number]	k4	1792
Bonapartovi	Bonapartův	k2eAgMnPc1d1	Bonapartův
dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
střetli	střetnout	k5eAaPmAgMnP	střetnout
s	s	k7c7	s
řádnou	řádný	k2eAgFnSc7d1	řádná
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
byly	být	k5eAaImAgFnP	být
ztráty	ztráta	k1gFnPc4	ztráta
jak	jak	k8xC	jak
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
tak	tak	k9	tak
mezi	mezi	k7c4	mezi
civilisty	civilista	k1gMnPc4	civilista
<g/>
.	.	kIx.	.
<g/>
Kvůli	kvůli	k7c3	kvůli
této	tento	k3xDgFnSc3	tento
akci	akce	k1gFnSc3	akce
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
pokusil	pokusit	k5eAaPmAgMnS	pokusit
obsadit	obsadit	k5eAaPmF	obsadit
pevnost	pevnost	k1gFnSc4	pevnost
v	v	k7c6	v
Ajacciu	Ajaccium	k1gNnSc6	Ajaccium
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Napoleon	Napoleon	k1gMnSc1	Napoleon
povolán	povolat	k5eAaPmNgMnS	povolat
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
po	po	k7c4	po
udání	udání	k1gNnSc4	udání
svých	svůj	k3xOyFgMnPc2	svůj
nepřátel	nepřítel	k1gMnPc2	nepřítel
zodpovídal	zodpovídat	k5eAaImAgMnS	zodpovídat
u	u	k7c2	u
výboru	výbor	k1gInSc2	výbor
vojenského	vojenský	k2eAgNnSc2d1	vojenské
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
francouzské	francouzský	k2eAgFnSc2d1	francouzská
metropole	metropol	k1gFnSc2	metropol
dorazil	dorazit	k5eAaPmAgMnS	dorazit
koncem	koncem	k7c2	koncem
května	květen	k1gInSc2	květen
a	a	k8xC	a
bez	bez	k7c2	bez
větších	veliký	k2eAgInPc2d2	veliký
problémů	problém	k1gInPc2	problém
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
vedená	vedený	k2eAgFnSc1d1	vedená
záležitost	záležitost	k1gFnSc1	záležitost
byla	být	k5eAaImAgFnS	být
zamítnuta	zamítnout	k5eAaPmNgFnS	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
skvělé	skvělý	k2eAgFnSc3d1	skvělá
politické	politický	k2eAgFnSc3d1	politická
reputaci	reputace	k1gFnSc3	reputace
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
žádost	žádost	k1gFnSc4	žádost
opět	opět	k6eAd1	opět
umístěn	umístit	k5eAaPmNgMnS	umístit
ke	k	k7c3	k
4	[number]	k4	4
<g/>
.	.	kIx.	.
dělostřeleckému	dělostřelecký	k2eAgInSc3d1	dělostřelecký
pluku	pluk	k1gInSc3	pluk
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
povýšen	povýšit	k5eAaPmNgMnS	povýšit
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
kapitána	kapitán	k1gMnSc2	kapitán
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
se	s	k7c7	s
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
stal	stát	k5eAaPmAgMnS	stát
nahodilým	nahodilý	k2eAgMnSc7d1	nahodilý
svědkem	svědek	k1gMnSc7	svědek
útoku	útok	k1gInSc2	útok
na	na	k7c4	na
Tuilerijský	tuilerijský	k2eAgInSc4d1	tuilerijský
palác	palác	k1gInSc4	palác
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
vyžádal	vyžádat	k5eAaPmAgInS	vyžádat
ještě	ještě	k9	ještě
jednu	jeden	k4xCgFnSc4	jeden
dovolenou	dovolená	k1gFnSc4	dovolená
a	a	k8xC	a
opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
na	na	k7c4	na
rodný	rodný	k2eAgInSc4d1	rodný
ostrov	ostrov	k1gInSc4	ostrov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zůstal	zůstat	k5eAaPmAgInS	zůstat
plných	plný	k2eAgInPc2d1	plný
osm	osm	k4xCc4	osm
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Znova	znova	k6eAd1	znova
se	se	k3xPyFc4	se
chopil	chopit	k5eAaPmAgMnS	chopit
velení	velení	k1gNnSc2	velení
pluku	pluk	k1gInSc2	pluk
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
a	a	k8xC	a
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
nevydařené	vydařený	k2eNgFnPc1d1	nevydařená
invaze	invaze	k1gFnPc1	invaze
na	na	k7c6	na
Sardinii	Sardinie	k1gFnSc6	Sardinie
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
akce	akce	k1gFnSc2	akce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
podniknutá	podniknutý	k2eAgFnSc1d1	podniknutá
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
pařížských	pařížský	k2eAgMnPc2d1	pařížský
vládních	vládní	k2eAgMnPc2d1	vládní
činitelů	činitel	k1gMnPc2	činitel
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
však	však	k9	však
značně	značně	k6eAd1	značně
zmatený	zmatený	k2eAgInSc4d1	zmatený
a	a	k8xC	a
jediný	jediný	k2eAgInSc4d1	jediný
profesionální	profesionální	k2eAgInSc4d1	profesionální
výkon	výkon	k1gInSc4	výkon
podal	podat	k5eAaPmAgMnS	podat
Napoleon	Napoleon	k1gMnSc1	Napoleon
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
svými	svůj	k3xOyFgMnPc7	svůj
bateriemi	baterie	k1gFnPc7	baterie
umlčel	umlčet	k5eAaPmAgMnS	umlčet
pevnosti	pevnost	k1gFnSc2	pevnost
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
La	la	k1gNnSc2	la
Maddalena	Maddalen	k2eAgNnPc1d1	Maddalen
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
vojenské	vojenský	k2eAgFnSc6d1	vojenská
operaci	operace	k1gFnSc6	operace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
díky	díky	k7c3	díky
politické	politický	k2eAgFnSc3d1	politická
motivaci	motivace	k1gFnSc3	motivace
čelních	čelní	k2eAgInPc2d1	čelní
korsických	korsický	k2eAgInPc2d1	korsický
představitelů	představitel	k1gMnPc2	představitel
předem	předem	k6eAd1	předem
odsouzena	odsoudit	k5eAaPmNgFnS	odsoudit
k	k	k7c3	k
nezdaru	nezdar	k1gInSc3	nezdar
<g/>
,	,	kIx,	,
označil	označit	k5eAaPmAgInS	označit
Napoleonův	Napoleonův	k2eAgMnSc1d1	Napoleonův
bratr	bratr	k1gMnSc1	bratr
Lucien	Lucien	k2eAgMnSc1d1	Lucien
v	v	k7c6	v
jakobínském	jakobínský	k2eAgInSc6d1	jakobínský
klubu	klub	k1gInSc6	klub
v	v	k7c6	v
Toulonu	Toulon	k1gInSc6	Toulon
korsického	korsický	k2eAgInSc2d1	korsický
předsedu	předseda	k1gMnSc4	předseda
direktoria	direktorium	k1gNnSc2	direktorium
za	za	k7c4	za
zrádce	zrádce	k1gMnSc4	zrádce
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
pařížský	pařížský	k2eAgInSc1d1	pařížský
Konvent	konvent	k1gInSc1	konvent
vydal	vydat	k5eAaPmAgInS	vydat
rozkaz	rozkaz	k1gInSc4	rozkaz
k	k	k7c3	k
Paoliho	Paoli	k1gMnSc2	Paoli
zatčení	zatčení	k1gNnSc4	zatčení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
toho	ten	k3xDgNnSc2	ten
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
korsický	korsický	k2eAgMnSc1d1	korsický
vůdce	vůdce	k1gMnSc1	vůdce
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
lid	lid	k1gInSc4	lid
k	k	k7c3	k
válce	válka	k1gFnSc3	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
a	a	k8xC	a
bratři	bratr	k1gMnPc1	bratr
Buonapartové	Buonapartová	k1gFnSc2	Buonapartová
byli	být	k5eAaImAgMnP	být
odsouzeni	odsoudit	k5eAaPmNgMnP	odsoudit
k	k	k7c3	k
veřejnému	veřejný	k2eAgNnSc3d1	veřejné
prokletí	prokletí	k1gNnSc3	prokletí
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
rodný	rodný	k2eAgInSc1d1	rodný
dům	dům	k1gInSc1	dům
byl	být	k5eAaImAgInS	být
vydrancován	vydrancován	k2eAgMnSc1d1	vydrancován
a	a	k8xC	a
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
zakrátko	zakrátko	k6eAd1	zakrátko
následovala	následovat	k5eAaImAgFnS	následovat
i	i	k9	i
matka	matka	k1gFnSc1	matka
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jeho	jeho	k3xOp3gMnPc7	jeho
bratry	bratr	k1gMnPc7	bratr
a	a	k8xC	a
sestrami	sestra	k1gFnPc7	sestra
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jen	jen	k9	jen
za	za	k7c2	za
velkých	velký	k2eAgFnPc2d1	velká
obtíží	obtíž	k1gFnPc2	obtíž
podařilo	podařit	k5eAaPmAgNnS	podařit
emigrovat	emigrovat	k5eAaBmF	emigrovat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Mladému	mladý	k2eAgMnSc3d1	mladý
uprchlíkovi	uprchlík	k1gMnSc3	uprchlík
pak	pak	k9	pak
nezbylo	zbýt	k5eNaPmAgNnS	zbýt
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
nedostatečného	dostatečný	k2eNgInSc2d1	nedostatečný
platu	plat	k1gInSc2	plat
postarat	postarat	k5eAaPmF	postarat
o	o	k7c4	o
celou	celý	k2eAgFnSc4d1	celá
rodinu	rodina	k1gFnSc4	rodina
a	a	k8xC	a
žít	žít	k5eAaImF	žít
v	v	k7c6	v
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgFnSc6d2	veliký
nouzi	nouze	k1gFnSc6	nouze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
posledním	poslední	k2eAgInSc6d1	poslední
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
Korsiky	Korsika	k1gFnSc2	Korsika
byl	být	k5eAaImAgInS	být
Napoleon	napoleon	k1gInSc1	napoleon
převelen	převelet	k5eAaPmNgInS	převelet
k	k	k7c3	k
francouzské	francouzský	k2eAgFnSc3d1	francouzská
armádě	armáda	k1gFnSc3	armáda
operující	operující	k2eAgFnSc3d1	operující
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Cestou	cesta	k1gFnSc7	cesta
na	na	k7c4	na
nové	nový	k2eAgNnSc4d1	nové
působiště	působiště	k1gNnPc4	působiště
navštívil	navštívit	k5eAaPmAgInS	navštívit
svého	svůj	k3xOyFgMnSc2	svůj
politického	politický	k2eAgMnSc2d1	politický
ochránce	ochránce	k1gMnSc2	ochránce
a	a	k8xC	a
přítele	přítel	k1gMnSc2	přítel
Salicettiho	Salicetti	k1gMnSc2	Salicetti
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
dvou	dva	k4xCgMnPc2	dva
politických	politický	k2eAgMnPc2d1	politický
komisařů	komisař	k1gMnPc2	komisař
u	u	k7c2	u
armády	armáda	k1gFnSc2	armáda
obléhající	obléhající	k2eAgNnSc1d1	obléhající
francouzské	francouzský	k2eAgNnSc1d1	francouzské
přístavní	přístavní	k2eAgNnSc1d1	přístavní
město	město	k1gNnSc1	město
Toulon	Toulon	k1gInSc1	Toulon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
za	za	k7c4	za
podpory	podpora	k1gFnPc4	podpora
anglického	anglický	k2eAgMnSc2d1	anglický
a	a	k8xC	a
španělského	španělský	k2eAgNnSc2d1	španělské
válečného	válečný	k2eAgNnSc2d1	válečné
loďstva	loďstvo	k1gNnSc2	loďstvo
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
povstání	povstání	k1gNnSc1	povstání
proti	proti	k7c3	proti
pařížské	pařížský	k2eAgFnSc3d1	Pařížská
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
bojových	bojový	k2eAgFnPc2d1	bojová
akcí	akce	k1gFnPc2	akce
zraněn	zraněn	k2eAgMnSc1d1	zraněn
velitel	velitel	k1gMnSc1	velitel
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
Dommartin	Dommartin	k1gMnSc1	Dommartin
<g/>
,	,	kIx,	,
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
Salicetti	Salicetti	k1gNnSc4	Salicetti
uvolněný	uvolněný	k2eAgInSc1d1	uvolněný
post	post	k1gInSc1	post
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
Korsičan	Korsičan	k1gMnSc1	Korsičan
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
zařazením	zařazení	k1gNnSc7	zařazení
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1793	[number]	k4	1793
se	s	k7c7	s
před	před	k7c7	před
Toulonem	Toulon	k1gInSc7	Toulon
ujal	ujmout	k5eAaPmAgInS	ujmout
svých	svůj	k3xOyFgFnPc2	svůj
nových	nový	k2eAgFnPc2d1	nová
povinností	povinnost	k1gFnPc2	povinnost
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
obléhání	obléhání	k1gNnSc1	obléhání
pod	pod	k7c7	pod
vrchním	vrchní	k2eAgNnSc7d1	vrchní
velením	velení	k1gNnSc7	velení
generála	generál	k1gMnSc2	generál
Carteauxe	Carteauxe	k1gFnSc2	Carteauxe
probíhalo	probíhat	k5eAaImAgNnS	probíhat
značně	značně	k6eAd1	značně
amatérským	amatérský	k2eAgInSc7d1	amatérský
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
Bonaparte	bonapart	k1gInSc5	bonapart
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
k	k	k7c3	k
překonání	překonání	k1gNnSc3	překonání
nepřátelského	přátelský	k2eNgInSc2d1	nepřátelský
odporu	odpor	k1gInSc2	odpor
pokusil	pokusit	k5eAaPmAgMnS	pokusit
navrhnout	navrhnout	k5eAaPmF	navrhnout
novou	nový	k2eAgFnSc4d1	nová
koncepci	koncepce	k1gFnSc4	koncepce
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
nebyl	být	k5eNaImAgMnS	být
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
přijat	přijmout	k5eAaPmNgMnS	přijmout
<g/>
,	,	kIx,	,
povolení	povolení	k1gNnPc1	povolení
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
provedení	provedení	k1gNnSc3	provedení
Napoleon	Napoleon	k1gMnSc1	Napoleon
obdržel	obdržet	k5eAaPmAgMnS	obdržet
teprve	teprve	k6eAd1	teprve
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jej	on	k3xPp3gNnSc4	on
podpořil	podpořit	k5eAaPmAgMnS	podpořit
vlivný	vlivný	k2eAgMnSc1d1	vlivný
komisař	komisař	k1gMnSc1	komisař
Konventu	konvent	k1gInSc2	konvent
Gasparin	Gasparin	k1gInSc1	Gasparin
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
stál	stát	k5eAaImAgInS	stát
za	za	k7c7	za
vystřídáním	vystřídání	k1gNnSc7	vystřídání
Carteauxe	Carteauxe	k1gFnSc2	Carteauxe
generálem	generál	k1gMnSc7	generál
Dugommierem	Dugommier	k1gMnSc7	Dugommier
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1793	[number]	k4	1793
zahájily	zahájit	k5eAaPmAgFnP	zahájit
Napoleonem	napoleon	k1gInSc7	napoleon
nově	nově	k6eAd1	nově
rozmístěné	rozmístěný	k2eAgFnSc2d1	rozmístěná
baterie	baterie	k1gFnSc2	baterie
palbu	palba	k1gFnSc4	palba
na	na	k7c6	na
britské	britský	k2eAgNnSc4d1	Britské
opevnění	opevnění	k1gNnSc4	opevnění
Fort	Fort	k?	Fort
Mulgrave	Mulgrav	k1gInSc5	Mulgrav
<g/>
,	,	kIx,	,
přezdívané	přezdívaný	k2eAgFnPc4d1	přezdívaná
Francouzi	Francouz	k1gMnPc7	Francouz
Malý	malý	k2eAgInSc4d1	malý
Gibraltar	Gibraltar	k1gInSc4	Gibraltar
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
na	na	k7c4	na
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
generálnímu	generální	k2eAgInSc3d1	generální
útoku	útok	k1gInSc3	útok
<g/>
.	.	kIx.	.
</s>
<s>
Úvodní	úvodní	k2eAgInSc1d1	úvodní
nápor	nápor	k1gInSc1	nápor
tří	tři	k4xCgInPc2	tři
proudů	proud	k1gInPc2	proud
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
velel	velet	k5eAaImAgMnS	velet
generál	generál	k1gMnSc1	generál
Dugommier	Dugommier	k1gMnSc1	Dugommier
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
obránci	obránce	k1gMnSc3	obránce
odražen	odražen	k2eAgMnSc1d1	odražen
<g/>
,	,	kIx,	,
teprve	teprve	k6eAd1	teprve
nápor	nápor	k1gInSc1	nápor
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
kolony	kolona	k1gFnSc2	kolona
pod	pod	k7c7	pod
přímým	přímý	k2eAgNnSc7d1	přímé
Bonapartovým	Bonapartův	k2eAgNnSc7d1	Bonapartovo
velením	velení	k1gNnSc7	velení
přinesl	přinést	k5eAaPmAgInS	přinést
úspěch	úspěch	k1gInSc1	úspěch
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
při	při	k7c6	při
postupu	postup	k1gInSc6	postup
byl	být	k5eAaImAgMnS	být
pod	pod	k7c7	pod
Napoleonem	Napoleon	k1gMnSc7	Napoleon
zastřelen	zastřelen	k2eAgMnSc1d1	zastřelen
kůň	kůň	k1gMnSc1	kůň
a	a	k8xC	a
při	při	k7c6	při
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
útoku	útok	k1gInSc6	útok
utržil	utržit	k5eAaPmAgMnS	utržit
ránu	rána	k1gFnSc4	rána
bodákem	bodák	k1gInSc7	bodák
na	na	k7c6	na
stehně	stehno	k1gNnSc6	stehno
<g/>
.	.	kIx.	.
</s>
<s>
Anglické	anglický	k2eAgFnPc1d1	anglická
a	a	k8xC	a
španělské	španělský	k2eAgFnPc1d1	španělská
lodě	loď	k1gFnPc1	loď
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
této	tento	k3xDgFnSc2	tento
akce	akce	k1gFnSc2	akce
vystaveny	vystavit	k5eAaPmNgInP	vystavit
střelbě	střelba	k1gFnSc6	střelba
republikánských	republikánský	k2eAgNnPc2d1	republikánské
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
odpluly	odplout	k5eAaPmAgFnP	odplout
pryč	pryč	k6eAd1	pryč
z	z	k7c2	z
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
sice	sice	k8xC	sice
trvaly	trvat	k5eAaImAgInP	trvat
ještě	ještě	k6eAd1	ještě
celý	celý	k2eAgInSc4d1	celý
následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
již	již	k9	již
nic	nic	k3yNnSc1	nic
nezměnilo	změnit	k5eNaPmAgNnS	změnit
na	na	k7c6	na
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Toulon	Toulon	k1gInSc1	Toulon
padl	padnout	k5eAaImAgInS	padnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Generál	generál	k1gMnSc1	generál
===	===	k?	===
</s>
</p>
<p>
<s>
Úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
Napoleon	Napoleon	k1gMnSc1	Napoleon
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
u	u	k7c2	u
Toulonu	Toulon	k1gInSc2	Toulon
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
pro	pro	k7c4	pro
tehdejší	tehdejší	k2eAgFnSc4d1	tehdejší
mladou	mladý	k2eAgFnSc4d1	mladá
generaci	generace	k1gFnSc4	generace
symbolem	symbol	k1gInSc7	symbol
náhlého	náhlý	k2eAgInSc2d1	náhlý
a	a	k8xC	a
rychlého	rychlý	k2eAgInSc2d1	rychlý
obratu	obrat	k1gInSc2	obrat
na	na	k7c6	na
životní	životní	k2eAgFnSc6d1	životní
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čtyřiadvaceti	čtyřiadvacet	k4xCc6	čtyřiadvacet
letech	léto	k1gNnPc6	léto
mu	on	k3xPp3gMnSc3	on
vynesl	vynést	k5eAaPmAgInS	vynést
hodnost	hodnost	k1gFnSc4	hodnost
brigádního	brigádní	k2eAgMnSc2d1	brigádní
generála	generál	k1gMnSc2	generál
a	a	k8xC	a
také	také	k9	také
důvěru	důvěra	k1gFnSc4	důvěra
tehdy	tehdy	k6eAd1	tehdy
vládnoucího	vládnoucí	k2eAgInSc2d1	vládnoucí
politického	politický	k2eAgInSc2d1	politický
klubu	klub	k1gInSc2	klub
jakobínů	jakobín	k1gMnPc2	jakobín
<g/>
.6	.6	k4	.6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1794	[number]	k4	1794
byl	být	k5eAaImAgMnS	být
Napoleon	Napoleon	k1gMnSc1	Napoleon
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
velitelem	velitel	k1gMnSc7	velitel
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
v	v	k7c6	v
Italské	italský	k2eAgFnSc6d1	italská
armádě	armáda	k1gFnSc6	armáda
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
pozici	pozice	k1gFnSc6	pozice
však	však	k9	však
působil	působit	k5eAaImAgMnS	působit
jen	jen	k9	jen
necelých	celý	k2eNgInPc2d1	necelý
šest	šest	k4xCc4	šest
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
9	[number]	k4	9
<g/>
.	.	kIx.	.
thermidoru	thermidor	k1gInSc2	thermidor
r.	r.	kA	r.
IIIfranc	IIIfranc	k1gFnSc1	IIIfranc
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
1794	[number]	k4	1794
<g/>
)	)	kIx)	)
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
k	k	k7c3	k
převratu	převrat	k1gInSc3	převrat
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
průběhu	průběh	k1gInSc6	průběh
politickou	politický	k2eAgFnSc4d1	politická
moc	moc	k1gFnSc4	moc
převzal	převzít	k5eAaPmAgInS	převzít
Konvent	konvent	k1gInSc1	konvent
a	a	k8xC	a
čelní	čelní	k2eAgMnPc1d1	čelní
vůdci	vůdce	k1gMnPc1	vůdce
jakobínů	jakobín	k1gMnPc2	jakobín
byli	být	k5eAaImAgMnP	být
pozatýkáni	pozatýkán	k2eAgMnPc1d1	pozatýkán
a	a	k8xC	a
dalšího	další	k2eAgInSc2d1	další
dne	den	k1gInSc2	den
popraveni	popravit	k5eAaPmNgMnP	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
akcí	akce	k1gFnSc7	akce
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vlně	vlna	k1gFnSc3	vlna
zatýkání	zatýkání	k1gNnSc2	zatýkání
osob	osoba	k1gFnPc2	osoba
blízkých	blízký	k2eAgFnPc2d1	blízká
popraveným	popravený	k2eAgMnPc3d1	popravený
jakobínům	jakobín	k1gMnPc3	jakobín
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
byl	být	k5eAaImAgMnS	být
zatčen	zatčen	k2eAgMnSc1d1	zatčen
i	i	k9	i
Napoleon	Napoleon	k1gMnSc1	Napoleon
<g/>
.	.	kIx.	.
</s>
<s>
Komisaři	komisar	k1gMnPc1	komisar
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jej	on	k3xPp3gMnSc4	on
vyšetřovali	vyšetřovat	k5eAaImAgMnP	vyšetřovat
<g/>
,	,	kIx,	,
však	však	k9	však
neshledali	shledat	k5eNaPmAgMnP	shledat
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
osobě	osoba	k1gFnSc6	osoba
nic	nic	k3yNnSc1	nic
kompromitujícího	kompromitující	k2eAgNnSc2d1	kompromitující
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
čtrnácti	čtrnáct	k4xCc6	čtrnáct
dnech	den	k1gInPc6	den
věznění	věznění	k1gNnSc2	věznění
propuštěn	propustit	k5eAaPmNgMnS	propustit
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
jako	jako	k9	jako
velitel	velitel	k1gMnSc1	velitel
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
účastnil	účastnit	k5eAaImAgMnS	účastnit
expedice	expedice	k1gFnPc4	expedice
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
úkolem	úkol	k1gInSc7	úkol
bylo	být	k5eAaImAgNnS	být
dobýt	dobýt	k5eAaPmF	dobýt
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
paolistů	paolista	k1gMnPc2	paolista
Korsiku	Korsika	k1gFnSc4	Korsika
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
výprava	výprava	k1gFnSc1	výprava
narazila	narazit	k5eAaPmAgFnS	narazit
na	na	k7c4	na
britské	britský	k2eAgNnSc4d1	Britské
námořnictvo	námořnictvo	k1gNnSc4	námořnictvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ji	on	k3xPp3gFnSc4	on
přinutilo	přinutit	k5eAaPmAgNnS	přinutit
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Bonaparte	bonapart	k1gInSc5	bonapart
tedy	tedy	k9	tedy
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
učiněna	učiněn	k2eAgFnSc1d1	učiněna
nabídka	nabídka	k1gFnSc1	nabídka
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
generálem	generál	k1gMnSc7	generál
pěchoty	pěchota	k1gFnSc2	pěchota
ve	v	k7c6	v
Vendée	Vendée	k1gFnSc6	Vendée
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
alternativu	alternativa	k1gFnSc4	alternativa
uplatnění	uplatnění	k1gNnSc2	uplatnění
však	však	k9	však
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
prodloužil	prodloužit	k5eAaPmAgMnS	prodloužit
si	se	k3xPyFc3	se
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
dovolenou	dovolená	k1gFnSc4	dovolená
a	a	k8xC	a
setrval	setrvat	k5eAaPmAgMnS	setrvat
ve	v	k7c6	v
francouzské	francouzský	k2eAgFnSc6d1	francouzská
metropoli	metropol	k1gFnSc6	metropol
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
v	v	k7c6	v
obnošené	obnošený	k2eAgFnSc6d1	obnošená
uniformě	uniforma	k1gFnSc6	uniforma
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
scházela	scházet	k5eAaImAgFnS	scházet
politická	politický	k2eAgFnSc1d1	politická
elita	elita	k1gFnSc1	elita
<g/>
,	,	kIx,	,
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
upozorňovat	upozorňovat	k5eAaImF	upozorňovat
předkládáním	předkládání	k1gNnSc7	předkládání
plánů	plán	k1gInPc2	plán
a	a	k8xC	a
návrhů	návrh	k1gInPc2	návrh
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jakobínské	jakobínský	k2eAgFnSc3d1	jakobínská
minulosti	minulost	k1gFnSc3	minulost
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
složité	složitý	k2eAgNnSc1d1	složité
se	se	k3xPyFc4	se
prosadit	prosadit	k5eAaPmF	prosadit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc4	jeho
memoranda	memorandum	k1gNnPc4	memorandum
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc2	jenž
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
novou	nový	k2eAgFnSc4d1	nová
strategickou	strategický	k2eAgFnSc4d1	strategická
koncepci	koncepce	k1gFnSc4	koncepce
pro	pro	k7c4	pro
Italskou	italský	k2eAgFnSc4d1	italská
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
vlivného	vlivný	k2eAgMnSc4d1	vlivný
politika	politik	k1gMnSc4	politik
Douleceta	Doulecet	k1gMnSc4	Doulecet
de	de	k?	de
Pontécoulant	Pontécoulant	k1gMnSc1	Pontécoulant
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgNnSc3	jenž
byl	být	k5eAaImAgInS	být
Bonaparte	bonapart	k1gInSc5	bonapart
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1795	[number]	k4	1795
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
šéfem	šéf	k1gMnSc7	šéf
topografického	topografický	k2eAgNnSc2d1	topografické
oddělení	oddělení	k1gNnSc2	oddělení
Výboru	výbor	k1gInSc2	výbor
veřejného	veřejný	k2eAgNnSc2d1	veřejné
blaha	blaho	k1gNnSc2	blaho
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgInSc1d1	další
obrat	obrat	k1gInSc1	obrat
v	v	k7c6	v
Napoleonově	Napoleonův	k2eAgFnSc6d1	Napoleonova
kariéře	kariéra	k1gFnSc6	kariéra
nastal	nastat	k5eAaPmAgInS	nastat
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
pařížských	pařížský	k2eAgFnPc2d1	Pařížská
sekcí	sekce	k1gFnPc2	sekce
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
velké	velký	k2eAgFnSc3d1	velká
roajalistické	roajalistický	k2eAgFnSc3d1	roajalistická
revoltě	revolta	k1gFnSc3	revolta
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
vendémiairu	vendémiair	k1gInSc2	vendémiair
r.	r.	kA	r.
IVfranc	IVfranc	k1gFnSc1	IVfranc
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1795	[number]	k4	1795
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
narychlo	narychlo	k6eAd1	narychlo
zařazen	zařadit	k5eAaPmNgInS	zařadit
do	do	k7c2	do
Vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
armády	armáda	k1gFnSc2	armáda
pod	pod	k7c4	pod
velení	velení	k1gNnSc4	velení
Paula	Paul	k1gMnSc4	Paul
Barrase	Barrasa	k1gFnSc6	Barrasa
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
dát	dát	k5eAaPmF	dát
rozkaz	rozkaz	k1gInSc4	rozkaz
Joachimu	Joachim	k1gMnSc3	Joachim
Muratovi	Murat	k1gMnSc3	Murat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
21	[number]	k4	21
<g/>
.	.	kIx.	.
pluku	pluk	k1gInSc2	pluk
jízdních	jízdní	k2eAgMnPc2d1	jízdní
myslivců	myslivec	k1gMnPc2	myslivec
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
děl	dělo	k1gNnPc2	dělo
z	z	k7c2	z
vojenského	vojenský	k2eAgInSc2d1	vojenský
tábora	tábor	k1gInSc2	tábor
Sablons	Sablons	k1gInSc1	Sablons
u	u	k7c2	u
Neuilly	Neuilla	k1gFnSc2	Neuilla
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
těchto	tento	k3xDgFnPc2	tento
zbraní	zbraň	k1gFnPc2	zbraň
pak	pak	k6eAd1	pak
rozmístil	rozmístit	k5eAaPmAgMnS	rozmístit
severně	severně	k6eAd1	severně
od	od	k7c2	od
sídla	sídlo	k1gNnSc2	sídlo
Konventu	konvent	k1gInSc2	konvent
v	v	k7c6	v
Palais	Palais	k1gFnSc6	Palais
des	des	k1gNnSc2	des
Tuileries	Tuileriesa	k1gFnPc2	Tuileriesa
a	a	k8xC	a
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
jimi	on	k3xPp3gInPc7	on
zcela	zcela	k6eAd1	zcela
rozvrátil	rozvrátit	k5eAaPmAgInS	rozvrátit
útok	útok	k1gInSc1	útok
čtyřnásobné	čtyřnásobný	k2eAgFnSc2d1	čtyřnásobná
přesily	přesila	k1gFnSc2	přesila
povstalců	povstalec	k1gMnPc2	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
čin	čin	k1gInSc4	čin
mu	on	k3xPp3gMnSc3	on
vynesl	vynést	k5eAaPmAgMnS	vynést
hodnost	hodnost	k1gFnSc4	hodnost
divizního	divizní	k2eAgMnSc2d1	divizní
generála	generál	k1gMnSc2	generál
a	a	k8xC	a
velení	velení	k1gNnSc1	velení
nad	nad	k7c7	nad
Vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
měsících	měsíc	k1gInPc6	měsíc
Napoleon	napoleon	k1gInSc1	napoleon
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
značné	značný	k2eAgNnSc4d1	značné
úsilí	úsilí	k1gNnSc4	úsilí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
svěřeno	svěřit	k5eAaPmNgNnS	svěřit
velení	velení	k1gNnSc1	velení
nad	nad	k7c7	nad
Italskou	italský	k2eAgFnSc7d1	italská
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
strategické	strategický	k2eAgInPc4d1	strategický
záměry	záměr	k1gInPc4	záměr
předložil	předložit	k5eAaPmAgMnS	předložit
členu	člen	k1gInSc3	člen
Výkonného	výkonný	k2eAgNnSc2d1	výkonné
direktoria	direktorium	k1gNnSc2	direktorium
Lazare	Lazar	k1gMnSc5	Lazar
Carnotovi	Carnot	k1gMnSc6	Carnot
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pochopil	pochopit	k5eAaPmAgMnS	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
ambiciózní	ambiciózní	k2eAgInSc4d1	ambiciózní
projekt	projekt	k1gInSc4	projekt
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
má	mít	k5eAaImIp3nS	mít
naději	naděje	k1gFnSc4	naděje
na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1796	[number]	k4	1796
<g/>
,	,	kIx,	,
po	po	k7c6	po
několika	několik	k4yIc6	několik
rozhovorech	rozhovor	k1gInPc6	rozhovor
s	s	k7c7	s
Napoleonem	napoleon	k1gInSc7	napoleon
<g/>
,	,	kIx,	,
Carnot	Carnota	k1gFnPc2	Carnota
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
Bonapartovo	Bonapartův	k2eAgNnSc4d1	Bonapartovo
jmenování	jmenování	k1gNnSc4	jmenování
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
Italské	italský	k2eAgFnSc2d1	italská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
post	post	k1gInSc1	post
byl	být	k5eAaImAgInS	být
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
přidělen	přidělit	k5eAaPmNgInS	přidělit
po	po	k7c6	po
demisi	demise	k1gFnSc6	demise
jejího	její	k3xOp3gMnSc2	její
dosavadního	dosavadní	k2eAgMnSc2d1	dosavadní
velitele	velitel	k1gMnSc2	velitel
generála	generál	k1gMnSc2	generál
Schérera	Schérer	k1gMnSc2	Schérer
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgInP	následovat
přípravy	příprava	k1gFnPc4	příprava
k	k	k7c3	k
odjezdu	odjezd	k1gInSc3	odjezd
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gInSc6	jejichž
průběhu	průběh	k1gInSc6	průběh
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
)	)	kIx)	)
si	se	k3xPyFc3	se
Napoleon	napoleon	k1gInSc4	napoleon
vzal	vzít	k5eAaPmAgInS	vzít
za	za	k7c4	za
manželku	manželka	k1gFnSc4	manželka
o	o	k7c4	o
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
starší	starý	k2eAgNnSc4d2	starší
Joséphine	Joséphin	k1gInSc5	Joséphin
de	de	k?	de
Beauharnais	Beauharnais	k1gFnSc7	Beauharnais
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
patrně	patrně	k6eAd1	patrně
seznámil	seznámit	k5eAaPmAgMnS	seznámit
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
událostech	událost	k1gFnPc6	událost
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
vendémiairu	vendémiair	k1gInSc2	vendémiair
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
po	po	k7c6	po
svatbě	svatba	k1gFnSc6	svatba
kvapně	kvapně	k6eAd1	kvapně
zamířil	zamířit	k5eAaPmAgMnS	zamířit
do	do	k7c2	do
hlavního	hlavní	k2eAgInSc2d1	hlavní
stanu	stan	k1gInSc2	stan
Italské	italský	k2eAgFnSc2d1	italská
armády	armáda	k1gFnSc2	armáda
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Nice	Nice	k1gFnSc2	Nice
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
dorazil	dorazit	k5eAaPmAgInS	dorazit
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
Nazítří	nazítří	k6eAd1	nazítří
pak	pak	k6eAd1	pak
vydal	vydat	k5eAaPmAgMnS	vydat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc2	první
a	a	k8xC	a
nejspíše	nejspíše	k9	nejspíše
nejslavnější	slavný	k2eAgFnSc4d3	nejslavnější
proklamaci	proklamace	k1gFnSc4	proklamace
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
ke	k	k7c3	k
svým	svůj	k3xOyFgMnPc3	svůj
vojákům	voják	k1gMnPc3	voják
kdy	kdy	k6eAd1	kdy
pronesl	pronést	k5eAaPmAgMnS	pronést
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
příštích	příští	k2eAgInPc6d1	příští
dnech	den	k1gInPc6	den
pak	pak	k6eAd1	pak
Napoleon	napoleon	k1gInSc4	napoleon
v	v	k7c6	v
rychlosti	rychlost	k1gFnSc6	rychlost
zkonsolidoval	zkonsolidovat	k5eAaPmAgMnS	zkonsolidovat
a	a	k8xC	a
zrevidoval	zrevidovat	k5eAaPmAgMnS	zrevidovat
síly	síla	k1gFnPc4	síla
vojska	vojsko	k1gNnSc2	vojsko
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
proti	proti	k7c3	proti
nepříteli	nepřítel	k1gMnSc3	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
italské	italský	k2eAgNnSc4d1	italské
území	území	k1gNnSc4	území
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
po	po	k7c6	po
čtyřdenním	čtyřdenní	k2eAgInSc6d1	čtyřdenní
pochodu	pochod	k1gInSc2	pochod
alpskými	alpský	k2eAgInPc7d1	alpský
průsmyky	průsmyk	k1gInPc7	průsmyk
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
vojáci	voják	k1gMnPc1	voják
se	se	k3xPyFc4	se
zanedlouho	zanedlouho	k6eAd1	zanedlouho
zapojil	zapojit	k5eAaPmAgInS	zapojit
do	do	k7c2	do
bojových	bojový	k2eAgFnPc2d1	bojová
operací	operace	k1gFnPc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujících	následující	k2eAgInPc2d1	následující
dvou	dva	k4xCgInPc2	dva
týdnů	týden	k1gInPc2	týden
pak	pak	k6eAd1	pak
svedl	svést	k5eAaPmAgInS	svést
šest	šest	k4xCc4	šest
vítězných	vítězný	k2eAgFnPc2d1	vítězná
bitev	bitva	k1gFnPc2	bitva
s	s	k7c7	s
rakousko	rakousko	k6eAd1	rakousko
<g/>
–	–	k?	–
<g/>
piemontskými	piemontský	k2eAgInPc7d1	piemontský
sbory	sbor	k1gInPc7	sbor
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
projevil	projevit	k5eAaPmAgInS	projevit
pohrdání	pohrdání	k1gNnSc4	pohrdání
před	před	k7c7	před
osobním	osobní	k2eAgNnSc7d1	osobní
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
a	a	k8xC	a
odvahu	odvaha	k1gFnSc4	odvaha
hraničící	hraničící	k2eAgFnSc4d1	hraničící
s	s	k7c7	s
drzostí	drzost	k1gFnSc7	drzost
<g/>
.	.	kIx.	.
</s>
<s>
Bleskovým	bleskový	k2eAgInSc7d1	bleskový
postupem	postup	k1gInSc7	postup
přinutil	přinutit	k5eAaPmAgInS	přinutit
Piemont	Piemont	k1gInSc1	Piemont
uzavřít	uzavřít	k5eAaPmF	uzavřít
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
mír	mír	k1gInSc1	mír
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnPc1	jeho
muži	muž	k1gMnPc1	muž
přepravili	přepravit	k5eAaPmAgMnP	přepravit
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Pád	Pád	k1gInSc1	Pád
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
boji	boj	k1gInSc6	boj
s	s	k7c7	s
Rakušany	Rakušan	k1gMnPc7	Rakušan
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
dalších	další	k2eAgNnPc2d1	další
vítězství	vítězství	k1gNnPc2	vítězství
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Lodi	loď	k1gFnSc2	loď
<g/>
,	,	kIx,	,
obsazením	obsazení	k1gNnSc7	obsazení
Milána	Milán	k1gInSc2	Milán
<g/>
,	,	kIx,	,
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Castiglione	Castiglion	k1gInSc5	Castiglion
<g/>
,	,	kIx,	,
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
arcolský	arcolský	k2eAgInSc4d1	arcolský
most	most	k1gInSc4	most
<g/>
,	,	kIx,	,
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Rivoli	Rivole	k1gFnSc3	Rivole
aj.	aj.	kA	aj.
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
zatlačil	zatlačit	k5eAaPmAgMnS	zatlačit
rakouské	rakouský	k2eAgFnPc4d1	rakouská
jednotky	jednotka	k1gFnPc4	jednotka
až	až	k6eAd1	až
k	k	k7c3	k
Vídni	Vídeň	k1gFnSc3	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
císařského	císařský	k2eAgInSc2d1	císařský
dvora	dvůr	k1gInSc2	dvůr
blízkost	blízkost	k1gFnSc1	blízkost
Francouzů	Francouz	k1gMnPc2	Francouz
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
paniku	panika	k1gFnSc4	panika
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
důsledku	důsledek	k1gInSc6	důsledek
se	se	k3xPyFc4	se
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
7	[number]	k4	7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1797	[number]	k4	1797
požádat	požádat	k5eAaPmF	požádat
o	o	k7c4	o
příměří	příměří	k1gNnSc4	příměří
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
nebylo	být	k5eNaImAgNnS	být
přerušeno	přerušit	k5eAaPmNgNnS	přerušit
ani	ani	k9	ani
po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
vypršení	vypršení	k1gNnSc6	vypršení
a	a	k8xC	a
po	po	k7c6	po
sérii	série	k1gFnSc6	série
jednání	jednání	k1gNnSc6	jednání
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
podepsaly	podepsat	k5eAaPmAgFnP	podepsat
konečnou	konečný	k2eAgFnSc4d1	konečná
mírovou	mírový	k2eAgFnSc4d1	mírová
smlouvu	smlouva	k1gFnSc4	smlouva
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
v	v	k7c6	v
Louvru	Louvre	k1gInSc6	Louvre
uspořádán	uspořádán	k2eAgInSc4d1	uspořádán
banket	banket	k1gInSc4	banket
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
členem	člen	k1gMnSc7	člen
matematické	matematický	k2eAgFnSc2d1	matematická
sekce	sekce	k1gFnSc2	sekce
francouzské	francouzský	k2eAgFnSc2d1	francouzská
Akademie	akademie	k1gFnSc2	akademie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1798	[number]	k4	1798
byl	být	k5eAaImAgInS	být
Napoleon	napoleon	k1gInSc1	napoleon
pověřen	pověřit	k5eAaPmNgInS	pověřit
velením	velení	k1gNnSc7	velení
připravované	připravovaný	k2eAgFnSc2d1	připravovaná
invaze	invaze	k1gFnSc2	invaze
do	do	k7c2	do
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
akci	akce	k1gFnSc4	akce
však	však	k9	však
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
síle	síla	k1gFnSc3	síla
anglického	anglický	k2eAgNnSc2d1	anglické
loďstva	loďstvo	k1gNnSc2	loďstvo
pokládal	pokládat	k5eAaImAgMnS	pokládat
za	za	k7c4	za
hazard	hazard	k1gInSc4	hazard
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
ní	on	k3xPp3gFnSc2	on
Direktoriu	direktorium	k1gNnSc6	direktorium
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
expedici	expedice	k1gFnSc4	expedice
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
realizaci	realizace	k1gFnSc4	realizace
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
nastínil	nastínit	k5eAaPmAgMnS	nastínit
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Talleyrand	Talleyranda	k1gFnPc2	Talleyranda
<g/>
.	.	kIx.	.
</s>
<s>
Francii	Francie	k1gFnSc4	Francie
mělo	mít	k5eAaImAgNnS	mít
dobytí	dobytí	k1gNnSc4	dobytí
této	tento	k3xDgFnSc2	tento
země	zem	k1gFnSc2	zem
otevřít	otevřít	k5eAaPmF	otevřít
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
Orientu	Orient	k1gInSc3	Orient
a	a	k8xC	a
připravit	připravit	k5eAaPmF	připravit
jí	on	k3xPp3gFnSc3	on
pozici	pozice	k1gFnSc3	pozice
k	k	k7c3	k
možnému	možný	k2eAgNnSc3d1	možné
vyhnání	vyhnání	k1gNnSc3	vyhnání
Britů	Brit	k1gMnPc2	Brit
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1	oficiální
velení	velení	k1gNnSc1	velení
výpravy	výprava	k1gFnSc2	výprava
obdržel	obdržet	k5eAaPmAgInS	obdržet
Napoleon	napoleon	k1gInSc1	napoleon
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
a	a	k8xC	a
již	již	k6eAd1	již
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
flotila	flotila	k1gFnSc1	flotila
lodí	loď	k1gFnPc2	loď
s	s	k7c7	s
jeho	jeho	k3xOp3gMnPc7	jeho
muži	muž	k1gMnPc7	muž
zamířila	zamířit	k5eAaPmAgFnS	zamířit
přes	přes	k7c4	přes
Maltu	Malta	k1gFnSc4	Malta
k	k	k7c3	k
africkému	africký	k2eAgNnSc3d1	africké
pobřeží	pobřeží	k1gNnSc3	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
plavbě	plavba	k1gFnSc6	plavba
se	se	k3xPyFc4	se
Francouzům	Francouz	k1gMnPc3	Francouz
podařilo	podařit	k5eAaPmAgNnS	podařit
se	s	k7c7	s
štěstím	štěstí	k1gNnSc7	štěstí
uniknout	uniknout	k5eAaPmF	uniknout
britskému	britský	k2eAgNnSc3d1	Britské
loďstvu	loďstvo	k1gNnSc3	loďstvo
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
admirála	admirál	k1gMnSc2	admirál
Nelsona	Nelson	k1gMnSc2	Nelson
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
celé	celý	k2eAgNnSc1d1	celé
uskupení	uskupení	k1gNnSc1	uskupení
dorazilo	dorazit	k5eAaPmAgNnS	dorazit
k	k	k7c3	k
Alexandrii	Alexandrie	k1gFnSc3	Alexandrie
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pobřežní	pobřežní	k2eAgNnSc1d1	pobřežní
město	město	k1gNnSc1	město
padlo	padnout	k5eAaPmAgNnS	padnout
do	do	k7c2	do
Napoleonových	Napoleonových	k2eAgFnPc2d1	Napoleonových
rukou	ruka	k1gFnPc2	ruka
již	již	k6eAd1	již
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
otevřela	otevřít	k5eAaPmAgFnS	otevřít
cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
Káhiru	Káhira	k1gFnSc4	Káhira
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
se	se	k3xPyFc4	se
za	za	k7c2	za
pochodu	pochod	k1gInSc2	pochod
vítězně	vítězně	k6eAd1	vítězně
střetl	střetnout	k5eAaPmAgInS	střetnout
s	s	k7c7	s
Egypťany	Egypťan	k1gMnPc7	Egypťan
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
pyramid	pyramida	k1gFnPc2	pyramida
a	a	k8xC	a
dalšího	další	k2eAgInSc2d1	další
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
zmocnil	zmocnit	k5eAaPmAgInS	zmocnit
i	i	k9	i
samotné	samotný	k2eAgFnSc2d1	samotná
egyptské	egyptský	k2eAgFnSc2d1	egyptská
metropole	metropol	k1gFnSc2	metropol
<g/>
.	.	kIx.	.
</s>
<s>
Nadšení	nadšení	k1gNnSc1	nadšení
z	z	k7c2	z
dosažených	dosažený	k2eAgNnPc2d1	dosažené
vítězství	vítězství	k1gNnPc2	vítězství
na	na	k7c6	na
souši	souš	k1gFnSc6	souš
však	však	k9	však
zkalil	zkalit	k5eAaPmAgMnS	zkalit
neúspěch	neúspěch	k1gInSc4	neúspěch
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
se	se	k3xPyFc4	se
admirálu	admirál	k1gMnSc3	admirál
Nelsonovi	Nelson	k1gMnSc3	Nelson
podařilo	podařit	k5eAaPmAgNnS	podařit
porazit	porazit	k5eAaPmF	porazit
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
flotilu	flotila	k1gFnSc4	flotila
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Abúkíru	Abúkír	k1gInSc2	Abúkír
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
Napoleon	napoleon	k1gInSc1	napoleon
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
naprosté	naprostý	k2eAgFnSc2d1	naprostá
izolace	izolace	k1gFnSc2	izolace
<g/>
.	.	kIx.	.
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1799	[number]	k4	1799
tedy	tedy	k9	tedy
zahájil	zahájit	k5eAaPmAgMnS	zahájit
ofenzívu	ofenzíva	k1gFnSc4	ofenzíva
do	do	k7c2	do
Sýrie	Sýrie	k1gFnSc2	Sýrie
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
dobyl	dobýt	k5eAaPmAgMnS	dobýt
El	Ela	k1gFnPc2	Ela
Arish	Arish	k1gInSc4	Arish
a	a	k8xC	a
Jaffu	Jaffa	k1gFnSc4	Jaffa
a	a	k8xC	a
oblehl	oblehnout	k5eAaPmAgMnS	oblehnout
Akru	akr	k1gInSc3	akr
<g/>
.	.	kIx.	.
</s>
<s>
Vojsko	vojsko	k1gNnSc1	vojsko
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
před	před	k7c7	před
hradbami	hradba	k1gFnPc7	hradba
města	město	k1gNnSc2	město
postiženo	postihnout	k5eAaPmNgNnS	postihnout
morovou	morový	k2eAgFnSc7d1	morová
epidemií	epidemie	k1gFnSc7	epidemie
a	a	k8xC	a
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
nezbylo	zbýt	k5eNaPmAgNnS	zbýt
než	než	k8xS	než
ustoupit	ustoupit	k5eAaPmF	ustoupit
zpět	zpět	k6eAd1	zpět
ke	k	k7c3	k
Káhiře	Káhira	k1gFnSc3	Káhira
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
sice	sice	k8xC	sice
porazil	porazit	k5eAaPmAgMnS	porazit
turecké	turecký	k2eAgNnSc4d1	turecké
vojsko	vojsko	k1gNnSc4	vojsko
ohrožující	ohrožující	k2eAgFnSc4d1	ohrožující
Alexandrii	Alexandrie	k1gFnSc4	Alexandrie
v	v	k7c6	v
pozemní	pozemní	k2eAgFnSc6d1	pozemní
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Abúkíru	Abúkír	k1gInSc2	Abúkír
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
chvíli	chvíle	k1gFnSc6	chvíle
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
egyptské	egyptský	k2eAgNnSc1d1	egyptské
tažení	tažení	k1gNnSc1	tažení
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
slepou	slepý	k2eAgFnSc7d1	slepá
uličkou	ulička	k1gFnSc7	ulička
jak	jak	k8xC	jak
ze	z	k7c2	z
strategického	strategický	k2eAgInSc2d1	strategický
<g/>
,	,	kIx,	,
tak	tak	k9	tak
z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
osobního	osobní	k2eAgNnSc2d1	osobní
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tím	ten	k3xDgNnSc7	ten
spíše	spíše	k9	spíše
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
střetnutí	střetnutí	k1gNnSc6	střetnutí
u	u	k7c2	u
Abúkíru	Abúkír	k1gInSc2	Abúkír
obdržel	obdržet	k5eAaPmAgMnS	obdržet
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
Francie	Francie	k1gFnSc1	Francie
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
od	od	k7c2	od
spojených	spojený	k2eAgFnPc2d1	spojená
rusko-rakouských	ruskoakouský	k2eAgFnPc2d1	rusko-rakouská
sil	síla	k1gFnPc2	síla
zdrcující	zdrcující	k2eAgFnSc2d1	zdrcující
porážky	porážka	k1gFnSc2	porážka
a	a	k8xC	a
domácí	domácí	k2eAgFnSc1d1	domácí
politická	politický	k2eAgFnSc1d1	politická
scéna	scéna	k1gFnSc1	scéna
spěje	spět	k5eAaImIp3nS	spět
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
nezadržitelné	zadržitelný	k2eNgFnSc3d1	nezadržitelná
krizi	krize	k1gFnSc3	krize
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
okolnosti	okolnost	k1gFnPc1	okolnost
nakonec	nakonec	k6eAd1	nakonec
stály	stát	k5eAaImAgFnP	stát
za	za	k7c7	za
Napoleonovým	Napoleonův	k2eAgNnSc7d1	Napoleonovo
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
ráno	ráno	k6eAd1	ráno
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1799	[number]	k4	1799
potají	potají	k6eAd1	potají
odplout	odplout	k5eAaPmF	odplout
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
zanechat	zanechat	k5eAaPmF	zanechat
celé	celý	k2eAgNnSc4d1	celé
vojsko	vojsko	k1gNnSc4	vojsko
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
jej	on	k3xPp3gInSc4	on
bouřlivě	bouřlivě	k6eAd1	bouřlivě
vítaly	vítat	k5eAaImAgInP	vítat
davy	dav	k1gInPc1	dav
občanů	občan	k1gMnPc2	občan
jako	jako	k8xS	jako
hrdinu	hrdina	k1gMnSc4	hrdina
a	a	k8xC	a
dobyvatele	dobyvatel	k1gMnSc4	dobyvatel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přijel	přijet	k5eAaPmAgMnS	přijet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zachránil	zachránit	k5eAaPmAgMnS	zachránit
svoji	svůj	k3xOyFgFnSc4	svůj
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nejhorší	zlý	k2eAgFnSc1d3	nejhorší
tíseň	tíseň	k1gFnSc1	tíseň
však	však	k9	však
již	již	k6eAd1	již
minula	minout	k5eAaImAgFnS	minout
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
stal	stát	k5eAaPmAgMnS	stát
účastníkem	účastník	k1gMnSc7	účastník
politického	politický	k2eAgInSc2d1	politický
převratu	převrat	k1gInSc2	převrat
zorganizovaného	zorganizovaný	k2eAgInSc2d1	zorganizovaný
členem	člen	k1gMnSc7	člen
Direktoria	direktorium	k1gNnSc2	direktorium
Emmanuelem	Emmanuel	k1gMnSc7	Emmanuel
Josephem	Joseph	k1gInSc7	Joseph
Sieyè	Sieyè	k1gMnSc7	Sieyè
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zamýšlel	zamýšlet	k5eAaImAgInS	zamýšlet
omezit	omezit	k5eAaPmF	omezit
moc	moc	k6eAd1	moc
dvou	dva	k4xCgFnPc2	dva
zákonodárných	zákonodárný	k2eAgFnPc2d1	zákonodárná
sněmoven	sněmovna	k1gFnPc2	sněmovna
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
mnohem	mnohem	k6eAd1	mnohem
silnější	silný	k2eAgFnSc2d2	silnější
výkonné	výkonný	k2eAgFnSc2d1	výkonná
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Naivně	naivně	k6eAd1	naivně
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Napoleon	napoleon	k1gInSc1	napoleon
spokojí	spokojit	k5eAaPmIp3nS	spokojit
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
rolí	role	k1gFnSc7	role
povolného	povolný	k2eAgInSc2d1	povolný
vojenského	vojenský	k2eAgInSc2d1	vojenský
nástroje	nástroj	k1gInSc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Bonaparte	bonapart	k1gInSc5	bonapart
nic	nic	k3yNnSc1	nic
ohledně	ohledně	k7c2	ohledně
převratu	převrat	k1gInSc2	převrat
nenavrhoval	navrhovat	k5eNaImAgMnS	navrhovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
plán	plán	k1gInSc1	plán
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
připraven	připraven	k2eAgInSc1d1	připraven
do	do	k7c2	do
nejmenších	malý	k2eAgFnPc2d3	nejmenší
podrobností	podrobnost	k1gFnPc2	podrobnost
<g/>
.	.	kIx.	.
</s>
<s>
Obratně	obratně	k6eAd1	obratně
však	však	k9	však
využil	využít	k5eAaPmAgMnS	využít
nabízené	nabízený	k2eAgFnPc4d1	nabízená
příležitosti	příležitost	k1gFnPc4	příležitost
a	a	k8xC	a
po	po	k7c6	po
událostech	událost	k1gFnPc6	událost
18	[number]	k4	18
<g/>
.	.	kIx.	.
–	–	k?	–
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
brumairu	brumaire	k1gInSc2	brumaire
r.	r.	kA	r.
VIIIfranc	VIIIfranc	k1gFnSc1	VIIIfranc
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1799	[number]	k4	1799
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
za	za	k7c2	za
dramatických	dramatický	k2eAgFnPc2d1	dramatická
okolností	okolnost	k1gFnPc2	okolnost
zanikly	zaniknout	k5eAaPmAgFnP	zaniknout
jak	jak	k6eAd1	jak
Direktorium	direktorium	k1gNnSc4	direktorium
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
Rada	rada	k1gFnSc1	rada
starších	starší	k1gMnPc2	starší
a	a	k8xC	a
Rada	rada	k1gFnSc1	rada
pěti	pět	k4xCc2	pět
set	sto	k4xCgNnPc2	sto
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
novém	nový	k2eAgNnSc6d1	nové
a	a	k8xC	a
zinscenovaném	zinscenovaný	k2eAgNnSc6d1	zinscenované
zasedání	zasedání	k1gNnSc6	zasedání
Rady	rada	k1gFnSc2	rada
pěti	pět	k4xCc3	pět
set	sto	k4xCgNnPc2	sto
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
prvním	první	k4xOgMnSc7	první
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
konzulů	konzul	k1gMnPc2	konzul
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
pravomoci	pravomoc	k1gFnSc6	pravomoc
bylo	být	k5eAaImAgNnS	být
iniciovat	iniciovat	k5eAaBmF	iniciovat
tvorbu	tvorba	k1gFnSc4	tvorba
všech	všecek	k3xTgInPc2	všecek
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
jmenovat	jmenovat	k5eAaImF	jmenovat
ministry	ministr	k1gMnPc4	ministr
a	a	k8xC	a
činitele	činitel	k1gMnPc4	činitel
<g/>
,	,	kIx,	,
vyhlašovat	vyhlašovat	k5eAaImF	vyhlašovat
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
uzavírat	uzavírat	k5eAaImF	uzavírat
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
dalších	další	k2eAgInPc2d1	další
dvou	dva	k4xCgMnPc2	dva
konzulů	konzul	k1gMnPc2	konzul
byla	být	k5eAaImAgFnS	být
pouze	pouze	k6eAd1	pouze
poradní	poradní	k2eAgInSc4d1	poradní
a	a	k8xC	a
konzultativní	konzultativní	k2eAgInSc4d1	konzultativní
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
Sieyè	Sieyè	k1gMnSc1	Sieyè
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
ústřední	ústřední	k2eAgFnSc4d1	ústřední
vládu	vláda	k1gFnSc4	vláda
získat	získat	k5eAaPmF	získat
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
limitovat	limitovat	k5eAaBmF	limitovat
Bonapartovu	Bonapartův	k2eAgFnSc4d1	Bonapartova
moc	moc	k1gFnSc4	moc
formálními	formální	k2eAgNnPc7d1	formální
omezeními	omezení	k1gNnPc7	omezení
<g/>
,	,	kIx,	,
ničeho	nic	k3yNnSc2	nic
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
za	za	k7c7	za
třicetiletým	třicetiletý	k2eAgMnSc7d1	třicetiletý
Korsičanem	Korsičan	k1gMnSc7	Korsičan
stála	stát	k5eAaImAgFnS	stát
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
francouzský	francouzský	k2eAgInSc1d1	francouzský
národ	národ	k1gInSc1	národ
toužící	toužící	k2eAgInSc1d1	toužící
po	po	k7c4	po
nalezení	nalezení	k1gNnSc4	nalezení
stability	stabilita	k1gFnSc2	stabilita
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
v	v	k7c6	v
plebiscitu	plebiscit	k1gInSc6	plebiscit
ze	z	k7c2	z
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1800	[number]	k4	1800
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
i	i	k8xC	i
nové	nový	k2eAgFnSc3d1	nová
ústavě	ústava	k1gFnSc3	ústava
výraznou	výrazný	k2eAgFnSc4d1	výrazná
podporu	podpora	k1gFnSc4	podpora
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
3	[number]	k4	3
011	[number]	k4	011
007	[number]	k4	007
voličů	volič	k1gMnPc2	volič
<g/>
,	,	kIx,	,
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
jich	on	k3xPp3gMnPc2	on
vyjádřilo	vyjádřit	k5eAaPmAgNnS	vyjádřit
1562	[number]	k4	1562
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
První	první	k4xOgMnSc1	první
konzul	konzul	k1gMnSc1	konzul
===	===	k?	===
</s>
</p>
<p>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vypracováním	vypracování	k1gNnSc7	vypracování
nové	nový	k2eAgFnSc2d1	nová
ústavy	ústava	k1gFnSc2	ústava
čekala	čekat	k5eAaImAgFnS	čekat
na	na	k7c4	na
Napoleona	Napoleon	k1gMnSc4	Napoleon
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
úkolů	úkol	k1gInPc2	úkol
spojených	spojený	k2eAgInPc2d1	spojený
s	s	k7c7	s
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
frontě	fronta	k1gFnSc6	fronta
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
zejména	zejména	k9	zejména
finanční	finanční	k2eAgFnSc1d1	finanční
situace	situace	k1gFnSc1	situace
<g/>
.	.	kIx.	.
</s>
<s>
Vyprázdněnou	vyprázdněný	k2eAgFnSc4d1	vyprázdněná
státní	státní	k2eAgFnSc4d1	státní
kasu	kasa	k1gFnSc4	kasa
postupně	postupně	k6eAd1	postupně
zacelil	zacelit	k5eAaPmAgInS	zacelit
důsledným	důsledný	k2eAgNnSc7d1	důsledné
spořením	spoření	k1gNnSc7	spoření
<g/>
,	,	kIx,	,
kontrolou	kontrola	k1gFnSc7	kontrola
vynaložených	vynaložený	k2eAgFnPc2d1	vynaložená
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
daňovou	daňový	k2eAgFnSc7d1	daňová
politikou	politika	k1gFnSc7	politika
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
zcela	zcela	k6eAd1	zcela
jasně	jasně	k6eAd1	jasně
vymezený	vymezený	k2eAgInSc4d1	vymezený
obsah	obsah	k1gInSc4	obsah
a	a	k8xC	a
vysokými	vysoký	k2eAgFnPc7d1	vysoká
půjčkami	půjčka	k1gFnPc7	půjčka
získanými	získaný	k2eAgFnPc7d1	získaná
v	v	k7c4	v
Nizozemí	Nizozemí	k1gNnSc4	Nizozemí
a	a	k8xC	a
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
daně	daň	k1gFnPc4	daň
samotné	samotný	k2eAgFnPc4d1	samotná
<g/>
,	,	kIx,	,
Napoleon	napoleon	k1gInSc1	napoleon
značně	značně	k6eAd1	značně
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
daň	daň	k1gFnSc4	daň
nepřímou	přímý	k2eNgFnSc4d1	nepřímá
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
výrazně	výrazně	k6eAd1	výrazně
snížil	snížit	k5eAaPmAgInS	snížit
daň	daň	k1gFnSc4	daň
přímou	přímý	k2eAgFnSc4d1	přímá
<g/>
.	.	kIx.	.
</s>
<s>
Uvědomoval	uvědomovat	k5eAaImAgInS	uvědomovat
si	se	k3xPyFc3	se
též	též	k6eAd1	též
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
stát	stát	k1gInSc4	stát
je	být	k5eAaImIp3nS	být
prospěšná	prospěšný	k2eAgFnSc1d1	prospěšná
podpora	podpora	k1gFnSc1	podpora
podnikání	podnikání	k1gNnSc2	podnikání
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
za	za	k7c4	za
prvořadý	prvořadý	k2eAgInSc4d1	prvořadý
zájem	zájem	k1gInSc4	zájem
považoval	považovat	k5eAaImAgMnS	považovat
rozvoj	rozvoj	k1gInSc4	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
důkladně	důkladně	k6eAd1	důkladně
zreorganizoval	zreorganizovat	k5eAaPmAgInS	zreorganizovat
celý	celý	k2eAgInSc1d1	celý
centrální	centrální	k2eAgInSc1d1	centrální
systém	systém	k1gInSc1	systém
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1800	[number]	k4	1800
založil	založit	k5eAaPmAgMnS	založit
Francouzskou	francouzský	k2eAgFnSc4d1	francouzská
banku	banka	k1gFnSc4	banka
<g/>
,	,	kIx,	,
k	k	k7c3	k
boji	boj	k1gInSc3	boj
proti	proti	k7c3	proti
roajalistům	roajalista	k1gMnPc3	roajalista
a	a	k8xC	a
jakobínům	jakobín	k1gMnPc3	jakobín
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
silný	silný	k2eAgInSc4d1	silný
a	a	k8xC	a
rozvětvený	rozvětvený	k2eAgInSc4d1	rozvětvený
policejní	policejní	k2eAgInSc4d1	policejní
aparát	aparát	k1gInSc4	aparát
(	(	kIx(	(
<g/>
dlouholetým	dlouholetý	k2eAgMnSc7d1	dlouholetý
ministrem	ministr	k1gMnSc7	ministr
policie	policie	k1gFnSc2	policie
byl	být	k5eAaImAgMnS	být
Joseph	Joseph	k1gInSc4	Joseph
Fouché	Fouchý	k2eAgFnPc1d1	Fouchý
<g/>
)	)	kIx)	)
a	a	k8xC	a
kritiku	kritika	k1gFnSc4	kritika
umlčel	umlčet	k5eAaPmAgMnS	umlčet
opatřením	opatření	k1gNnSc7	opatření
z	z	k7c2	z
27	[number]	k4	27
<g/>
.	.	kIx.	.
nivôsu	nivôs	k1gInSc2	nivôs
r.	r.	kA	r.
VIIIfranc	VIIIfranc	k1gFnSc1	VIIIfranc
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1800	[number]	k4	1800
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
zastavilo	zastavit	k5eAaPmAgNnS	zastavit
tisk	tisk	k1gInSc4	tisk
šedesáti	šedesát	k4xCc7	šedesát
z	z	k7c2	z
celkových	celkový	k2eAgInPc2d1	celkový
sedmdesáti	sedmdesát	k4xCc2	sedmdesát
tří	tři	k4xCgInPc2	tři
vycházejících	vycházející	k2eAgFnPc2d1	vycházející
novin	novina	k1gFnPc2	novina
a	a	k8xC	a
časopisů	časopis	k1gInPc2	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvního	první	k4xOgNnSc2	první
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
též	též	k9	též
vypořádal	vypořádat	k5eAaPmAgMnS	vypořádat
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
vlnou	vlna	k1gFnSc7	vlna
loupežnictví	loupežnictví	k1gNnSc2	loupežnictví
podél	podél	k7c2	podél
pozemních	pozemní	k2eAgFnPc2d1	pozemní
komunikací	komunikace	k1gFnPc2	komunikace
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
utlumil	utlumit	k5eAaPmAgInS	utlumit
stále	stále	k6eAd1	stále
doutnající	doutnající	k2eAgNnSc4d1	doutnající
roajalistické	roajalistický	k2eAgNnSc4d1	roajalistické
povstání	povstání	k1gNnSc4	povstání
ve	v	k7c6	v
Vendée	Vendée	k1gFnSc6	Vendée
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vnitrostátních	vnitrostátní	k2eAgFnPc2d1	vnitrostátní
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
souvisely	souviset	k5eAaImAgFnP	souviset
se	s	k7c7	s
správou	správa	k1gFnSc7	správa
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Napoleon	Napoleon	k1gMnSc1	Napoleon
pustil	pustit	k5eAaPmAgMnS	pustit
i	i	k9	i
do	do	k7c2	do
budování	budování	k1gNnSc2	budování
vojenské	vojenský	k2eAgFnSc2d1	vojenská
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
armády	armáda	k1gFnPc1	armáda
II	II	kA	II
<g/>
.	.	kIx.	.
koalice	koalice	k1gFnPc1	koalice
získaly	získat	k5eAaPmAgFnP	získat
zpět	zpět	k6eAd1	zpět
celou	celý	k2eAgFnSc4d1	celá
Itálii	Itálie	k1gFnSc4	Itálie
a	a	k8xC	a
stály	stát	k5eAaImAgFnP	stát
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
teprve	teprve	k6eAd1	teprve
od	od	k7c2	od
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1800	[number]	k4	1800
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
s	s	k7c7	s
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
možným	možný	k2eAgNnSc7d1	možné
utajením	utajení	k1gNnSc7	utajení
vytvářet	vytvářet	k5eAaImF	vytvářet
novou	nový	k2eAgFnSc4d1	nová
<g/>
,	,	kIx,	,
takzvanou	takzvaný	k2eAgFnSc4d1	takzvaná
Záložní	záložní	k2eAgFnSc4d1	záložní
armádu	armáda	k1gFnSc4	armáda
(	(	kIx(	(
<g/>
Armée	Armée	k1gFnSc4	Armée
de	de	k?	de
Réserve	Réserev	k1gFnSc2	Réserev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Místem	místo	k1gNnSc7	místo
shromažďování	shromažďování	k1gNnSc2	shromažďování
vojsk	vojsko	k1gNnPc2	vojsko
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
Dijon	Dijon	k1gNnSc1	Dijon
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
blízké	blízký	k2eAgNnSc4d1	blízké
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
s	s	k7c7	s
organizováním	organizování	k1gNnSc7	organizování
nové	nový	k2eAgFnSc2d1	nová
úderné	úderný	k2eAgFnSc2d1	úderná
síly	síla	k1gFnSc2	síla
první	první	k4xOgMnSc1	první
konzul	konzul	k1gMnSc1	konzul
posiloval	posilovat	k5eAaImAgMnS	posilovat
Rýnskou	rýnský	k2eAgFnSc4d1	Rýnská
armádu	armáda	k1gFnSc4	armáda
generála	generál	k1gMnSc2	generál
Moreaua	Moreauus	k1gMnSc2	Moreauus
a	a	k8xC	a
vydal	vydat	k5eAaPmAgMnS	vydat
pokyny	pokyn	k1gInPc7	pokyn
veliteli	velitel	k1gMnPc7	velitel
Italské	italský	k2eAgFnSc2d1	italská
armády	armáda	k1gFnSc2	armáda
André	André	k1gMnSc3	André
Massénovi	Massén	k1gMnSc3	Massén
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pouze	pouze	k6eAd1	pouze
odvracel	odvracet	k5eAaImAgInS	odvracet
pozornost	pozornost	k1gFnSc4	pozornost
Rakušanů	Rakušan	k1gMnPc2	Rakušan
<g/>
.	.	kIx.	.
</s>
<s>
Osobně	osobně	k6eAd1	osobně
se	se	k3xPyFc4	se
odebral	odebrat	k5eAaPmAgMnS	odebrat
do	do	k7c2	do
Dijonu	Dijon	k1gInSc2	Dijon
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
zamířil	zamířit	k5eAaPmAgMnS	zamířit
do	do	k7c2	do
Ženevy	Ženeva	k1gFnSc2	Ženeva
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
směřovaly	směřovat	k5eAaImAgFnP	směřovat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
celky	celek	k1gInPc4	celek
Záložní	záložní	k2eAgFnSc2d1	záložní
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
dostali	dostat	k5eAaPmAgMnP	dostat
vojáci	voják	k1gMnPc1	voják
povel	povel	k1gInSc1	povel
vyrazit	vyrazit	k5eAaPmF	vyrazit
k	k	k7c3	k
pochodu	pochod	k1gInSc3	pochod
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nepříteli	nepřítel	k1gMnSc3	nepřítel
vpadli	vpadnout	k5eAaPmAgMnP	vpadnout
do	do	k7c2	do
týlu	týl	k1gInSc2	týl
průchodem	průchod	k1gInSc7	průchod
Alp	Alpy	k1gFnPc2	Alpy
skrze	skrze	k?	skrze
Velký	velký	k2eAgInSc1d1	velký
Svatobernardský	svatobernardský	k2eAgInSc1d1	svatobernardský
průsmyk	průsmyk	k1gInSc1	průsmyk
a	a	k8xC	a
Svatogotthardský	Svatogotthardský	k2eAgInSc1d1	Svatogotthardský
průsmyk	průsmyk	k1gInSc1	průsmyk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
17	[number]	k4	17
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
květnem	květen	k1gInSc7	květen
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
hlavní	hlavní	k2eAgFnPc1d1	hlavní
francouzské	francouzský	k2eAgFnPc1d1	francouzská
síly	síla	k1gFnPc1	síla
Lombardie	Lombardie	k1gFnSc2	Lombardie
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
do	do	k7c2	do
Milána	Milán	k1gInSc2	Milán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
dnech	den	k1gInPc6	den
pak	pak	k6eAd1	pak
Napoleon	Napoleon	k1gMnSc1	Napoleon
rozptýlil	rozptýlit	k5eAaPmAgMnS	rozptýlit
svou	svůj	k3xOyFgFnSc4	svůj
armádu	armáda	k1gFnSc4	armáda
v	v	k7c6	v
domnění	domnění	k1gNnSc6	domnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nepřítel	nepřítel	k1gMnSc1	nepřítel
bude	být	k5eAaImBp3nS	být
snažit	snažit	k5eAaImF	snažit
vyklidit	vyklidit	k5eAaPmF	vyklidit
Alessandrii	Alessandrie	k1gFnSc4	Alessandrie
a	a	k8xC	a
ustoupí	ustoupit	k5eAaPmIp3nS	ustoupit
směrem	směr	k1gInSc7	směr
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Pád	Pád	k1gInSc1	Pád
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
protivník	protivník	k1gMnSc1	protivník
baron	baron	k1gMnSc1	baron
Michael	Michael	k1gMnSc1	Michael
von	von	k1gInSc4	von
Melas	melasa	k1gFnPc2	melasa
však	však	k9	však
místo	místo	k7c2	místo
ústupu	ústup	k1gInSc2	ústup
soustřeďoval	soustřeďovat	k5eAaImAgMnS	soustřeďovat
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
udeřil	udeřit	k5eAaPmAgMnS	udeřit
na	na	k7c4	na
značně	značně	k6eAd1	značně
oslabenou	oslabený	k2eAgFnSc4d1	oslabená
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
armádu	armáda	k1gFnSc4	armáda
u	u	k7c2	u
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
usedlosti	usedlost	k1gFnSc2	usedlost
s	s	k7c7	s
názvem	název	k1gInSc7	název
Vella	Vella	k1gFnSc1	Vella
di	di	k?	di
Marengo	marengo	k1gNnSc1	marengo
<g/>
.	.	kIx.	.
</s>
<s>
Naprosto	naprosto	k6eAd1	naprosto
zaskočený	zaskočený	k2eAgMnSc1d1	zaskočený
Napoleon	Napoleon	k1gMnSc1	Napoleon
nebyl	být	k5eNaImAgMnS	být
s	s	k7c7	s
to	ten	k3xDgNnSc1	ten
silnějšímu	silný	k2eAgMnSc3d2	silnější
soupeři	soupeř	k1gMnSc3	soupeř
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
a	a	k8xC	a
zdálo	zdát	k5eAaImAgNnS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Marenga	marengo	k1gNnSc2	marengo
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
jeho	jeho	k3xOp3gInSc7	jeho
výrazným	výrazný	k2eAgInSc7d1	výrazný
neúspěchem	neúspěch	k1gInSc7	neúspěch
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
páté	pátý	k4xOgFnSc6	pátý
hodině	hodina	k1gFnSc6	hodina
odpolední	odpolední	k1gNnSc2	odpolední
však	však	k9	však
na	na	k7c4	na
bojiště	bojiště	k1gNnSc4	bojiště
dorazilo	dorazit	k5eAaPmAgNnS	dorazit
5000	[number]	k4	5000
mužů	muž	k1gMnPc2	muž
generála	generál	k1gMnSc4	generál
Desaixe	Desaixe	k1gFnSc2	Desaixe
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
osobně	osobně	k6eAd1	osobně
zvrátil	zvrátit	k5eAaPmAgMnS	zvrátit
průběh	průběh	k1gInSc4	průběh
střetu	střet	k1gInSc2	střet
ve	v	k7c4	v
francouzské	francouzský	k2eAgNnSc4d1	francouzské
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
podepsaly	podepsat	k5eAaPmAgFnP	podepsat
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
příměří	příměří	k1gNnSc2	příměří
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nějž	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
měli	mít	k5eAaImAgMnP	mít
Rakušané	Rakušan	k1gMnPc1	Rakušan
stáhnout	stáhnout	k5eAaPmF	stáhnout
severně	severně	k6eAd1	severně
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Pád	Pád	k1gInSc1	Pád
<g/>
.	.	kIx.	.
</s>
<s>
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
dopomohl	dopomoct	k5eAaPmAgMnS	dopomoct
triumf	triumf	k1gInSc4	triumf
k	k	k7c3	k
upevnění	upevnění	k1gNnSc3	upevnění
pozice	pozice	k1gFnSc2	pozice
prvního	první	k4xOgMnSc4	první
konzula	konzul	k1gMnSc4	konzul
i	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Marenga	marengo	k1gNnSc2	marengo
válku	válka	k1gFnSc4	válka
neukončila	ukončit	k5eNaPmAgFnS	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
stranami	strana	k1gFnPc7	strana
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
obnoveno	obnoven	k2eAgNnSc1d1	obnoveno
a	a	k8xC	a
k	k	k7c3	k
definitivnímu	definitivní	k2eAgNnSc3d1	definitivní
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
generála	generál	k1gMnSc2	generál
Moreaua	Moreauus	k1gMnSc2	Moreauus
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Hohenlindenu	Hohenlinden	k1gInSc2	Hohenlinden
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1801	[number]	k4	1801
mezi	mezi	k7c7	mezi
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Rakouskem	Rakousko	k1gNnSc7	Rakousko
v	v	k7c6	v
Lunéville	Lunévill	k1gInSc6	Lunévill
podepsána	podepsat	k5eAaPmNgFnS	podepsat
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1802	[number]	k4	1802
dorazil	dorazit	k5eAaPmAgMnS	dorazit
první	první	k4xOgMnSc1	první
konzul	konzul	k1gMnSc1	konzul
Bonaparte	bonapart	k1gInSc5	bonapart
do	do	k7c2	do
Lyonu	Lyon	k1gInSc6	Lyon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
zahájeno	zahájen	k2eAgNnSc1d1	zahájeno
zasedání	zasedání	k1gNnSc1	zasedání
Poradního	poradní	k2eAgNnSc2d1	poradní
shromáždění	shromáždění	k1gNnSc2	shromáždění
Cisalpinské	Cisalpinský	k2eAgFnSc2d1	Cisalpinská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
průběhu	průběh	k1gInSc6	průběh
pronesl	pronést	k5eAaPmAgMnS	pronést
projev	projev	k1gInSc4	projev
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
shromážděným	shromážděný	k2eAgNnSc7d1	shromážděné
delegátům	delegát	k1gMnPc3	delegát
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
zřídit	zřídit	k5eAaPmF	zřídit
Italskou	italský	k2eAgFnSc4d1	italská
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
přítomnými	přítomný	k1gMnPc7	přítomný
tím	ten	k3xDgNnSc7	ten
vzbudil	vzbudit	k5eAaPmAgMnS	vzbudit
značné	značný	k2eAgNnSc4d1	značné
nadšení	nadšení	k1gNnSc4	nadšení
a	a	k8xC	a
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
zasedání	zasedání	k1gNnSc2	zasedání
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
italským	italský	k2eAgMnSc7d1	italský
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
floreálu	floreál	k1gInSc2	floreál
roku	rok	k1gInSc2	rok
Xfranc	Xfranc	k1gFnSc1	Xfranc
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1802	[number]	k4	1802
<g/>
)	)	kIx)	)
vydal	vydat	k5eAaPmAgInS	vydat
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
veřejném	veřejný	k2eAgNnSc6d1	veřejné
vyučování	vyučování	k1gNnSc6	vyučování
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
zřizoval	zřizovat	k5eAaImAgInS	zřizovat
veřejná	veřejný	k2eAgNnPc4d1	veřejné
lycea	lyceum	k1gNnPc4	lyceum
na	na	k7c6	na
území	území	k1gNnSc6	území
celé	celý	k2eAgFnSc2d1	celá
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
položil	položit	k5eAaPmAgInS	položit
tak	tak	k8xC	tak
základ	základ	k1gInSc1	základ
státního	státní	k2eAgInSc2d1	státní
vzdělávacího	vzdělávací	k2eAgInSc2d1	vzdělávací
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
Université	Universita	k1gMnPc1	Universita
de	de	k?	de
France	Franc	k1gMnSc2	Franc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
diplomatické	diplomatický	k2eAgFnSc6d1	diplomatická
frontě	fronta	k1gFnSc6	fronta
urovnal	urovnat	k5eAaPmAgMnS	urovnat
poměry	poměr	k1gInPc4	poměr
s	s	k7c7	s
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
zástupci	zástupce	k1gMnPc7	zástupce
uzavřel	uzavřít	k5eAaPmAgMnS	uzavřít
v	v	k7c6	v
Amiensu	Amiens	k1gInSc6	Amiens
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1802	[number]	k4	1802
mírovou	mírový	k2eAgFnSc4d1	mírová
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
vzájemného	vzájemný	k2eAgNnSc2d1	vzájemné
vyrovnání	vyrovnání	k1gNnSc2	vyrovnání
byly	být	k5eAaImAgFnP	být
Francii	Francie	k1gFnSc6	Francie
navráceny	navrátit	k5eAaPmNgFnP	navrátit
zámořské	zámořský	k2eAgFnPc1d1	zámořská
kolonie	kolonie	k1gFnPc1	kolonie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
předtím	předtím	k6eAd1	předtím
na	na	k7c4	na
krátko	krátko	k6eAd1	krátko
obsadili	obsadit	k5eAaPmAgMnP	obsadit
britští	britský	k2eAgMnPc1d1	britský
vojáci	voják	k1gMnPc1	voják
(	(	kIx(	(
<g/>
Saint-Domingue	Saint-Domingu	k1gInPc1	Saint-Domingu
<g/>
,	,	kIx,	,
Malé	Malé	k2eAgFnPc1d1	Malé
Antily	Antily	k1gFnPc1	Antily
<g/>
,	,	kIx,	,
Maskarény	Maskaréna	k1gFnPc1	Maskaréna
a	a	k8xC	a
pobřeží	pobřeží	k1gNnSc1	pobřeží
Guyany	Guyana	k1gFnSc2	Guyana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
formálním	formální	k2eAgInSc6d1	formální
převzetí	převzetí	k1gNnSc2	převzetí
těchto	tento	k3xDgNnPc2	tento
území	území	k1gNnPc2	území
se	se	k3xPyFc4	se
Napoleon	napoleon	k1gInSc1	napoleon
postavil	postavit	k5eAaPmAgInS	postavit
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
francouzských	francouzský	k2eAgMnPc2d1	francouzský
plantážníků	plantážník	k1gMnPc2	plantážník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
smířit	smířit	k5eAaPmF	smířit
s	s	k7c7	s
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
Konventu	konvent	k1gInSc2	konvent
o	o	k7c4	o
propuštění	propuštění	k1gNnSc4	propuštění
otroků	otrok	k1gMnPc2	otrok
<g/>
,	,	kIx,	,
a	a	k8xC	a
obnovil	obnovit	k5eAaPmAgInS	obnovit
otrokářské	otrokářský	k2eAgInPc4d1	otrokářský
zákony	zákon	k1gInPc4	zákon
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
zrušeny	zrušit	k5eAaPmNgInP	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
vyslal	vyslat	k5eAaPmAgMnS	vyslat
20	[number]	k4	20
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
Rýnské	rýnský	k2eAgFnSc2d1	Rýnská
armády	armáda	k1gFnSc2	armáda
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
svého	svůj	k3xOyFgMnSc2	svůj
švagra	švagr	k1gMnSc2	švagr
<g/>
,	,	kIx,	,
generála	generál	k1gMnSc2	generál
Leclerca	Leclercus	k1gMnSc2	Leclercus
(	(	kIx(	(
<g/>
manžela	manžel	k1gMnSc2	manžel
své	svůj	k3xOyFgNnSc1	svůj
sestry	sestra	k1gFnPc1	sestra
Pauliny	Paulin	k2eAgFnPc1d1	Paulina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
potlačili	potlačit	k5eAaPmAgMnP	potlačit
povstání	povstání	k1gNnSc2	povstání
černochů	černoch	k1gMnPc2	černoch
vedených	vedený	k2eAgFnPc2d1	vedená
Toussaintem	Toussaint	k1gMnSc7	Toussaint
Louverturem	Louvertur	k1gMnSc7	Louvertur
v	v	k7c6	v
kolonii	kolonie	k1gFnSc6	kolonie
Saint-Domingue	Saint-Domingue	k1gNnPc2	Saint-Domingue
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnPc1d1	dnešní
Haiti	Haiti	k1gNnPc1	Haiti
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Napoleon	napoleon	k1gInSc1	napoleon
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
věnoval	věnovat	k5eAaPmAgMnS	věnovat
i	i	k8xC	i
upevňování	upevňování	k1gNnSc4	upevňování
své	svůj	k3xOyFgFnSc2	svůj
pozice	pozice	k1gFnSc2	pozice
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
Státní	státní	k2eAgFnSc4d1	státní
radu	rada	k1gFnSc4	rada
zbavit	zbavit	k5eAaPmF	zbavit
všech	všecek	k3xTgFnPc2	všecek
pravomocí	pravomoc	k1gFnPc2	pravomoc
podílet	podílet	k5eAaImF	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
přípravě	příprava	k1gFnSc6	příprava
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
pomocí	pomocí	k7c2	pomocí
neústavních	ústavní	k2eNgFnPc2d1	neústavní
machinací	machinace	k1gFnPc2	machinace
zorganizoval	zorganizovat	k5eAaPmAgMnS	zorganizovat
referendum	referendum	k1gNnSc4	referendum
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
byl	být	k5eAaImAgInS	být
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1802	[number]	k4	1802
zvolen	zvolit	k5eAaPmNgMnS	zvolit
doživotním	doživotní	k2eAgMnSc7d1	doživotní
konsulem	konsul	k1gMnSc7	konsul
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
byl	být	k5eAaImAgInS	být
vyšachovaný	vyšachovaný	k2eAgInSc1d1	vyšachovaný
senát	senát	k1gInSc1	senát
nucen	nutit	k5eAaImNgMnS	nutit
jej	on	k3xPp3gMnSc4	on
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
úřadu	úřad	k1gInSc2	úřad
potvrdit	potvrdit	k5eAaPmF	potvrdit
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
Napoleona	Napoleon	k1gMnSc4	Napoleon
spáchán	spáchán	k2eAgInSc1d1	spáchán
atentát	atentát	k1gInSc1	atentát
<g/>
,	,	kIx,	,
když	když	k8xS	když
odjížděl	odjíždět	k5eAaImAgMnS	odjíždět
na	na	k7c4	na
představení	představení	k1gNnSc4	představení
do	do	k7c2	do
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Následné	následný	k2eAgNnSc1d1	následné
vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
prokázalo	prokázat	k5eAaPmAgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
výbuchem	výbuch	k1gInSc7	výbuch
pekelného	pekelný	k2eAgInSc2d1	pekelný
stroje	stroj	k1gInSc2	stroj
nedaleko	nedaleko	k7c2	nedaleko
jeho	jeho	k3xOp3gInSc2	jeho
kočáru	kočár	k1gInSc2	kočár
stáli	stát	k5eAaImAgMnP	stát
příslušníci	příslušník	k1gMnPc1	příslušník
strany	strana	k1gFnSc2	strana
Šuanů	Šuan	k1gMnPc2	Šuan
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1803	[number]	k4	1803
Napoleon	napoleon	k1gInSc1	napoleon
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
mocenský	mocenský	k2eAgInSc1d1	mocenský
nátlak	nátlak	k1gInSc1	nátlak
na	na	k7c4	na
švýcarské	švýcarský	k2eAgMnPc4d1	švýcarský
povstalce	povstalec	k1gMnPc4	povstalec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
podepsání	podepsání	k1gNnSc6	podepsání
lunévillského	lunévillský	k2eAgInSc2d1	lunévillský
míru	mír	k1gInSc2	mír
a	a	k8xC	a
odchodu	odchod	k1gInSc2	odchod
francouzských	francouzský	k2eAgNnPc2d1	francouzské
vojsk	vojsko	k1gNnPc2	vojsko
ozbrojenou	ozbrojený	k2eAgFnSc7d1	ozbrojená
mocí	moc	k1gFnSc7	moc
postavili	postavit	k5eAaPmAgMnP	postavit
proti	proti	k7c3	proti
uspořádání	uspořádání	k1gNnSc3	uspořádání
země	zem	k1gFnSc2	zem
podle	podle	k7c2	podle
diktátu	diktát	k1gInSc2	diktát
Direktoria	direktorium	k1gNnSc2	direktorium
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
podepsání	podepsání	k1gNnSc6	podepsání
listiny	listina	k1gFnSc2	listina
Acte	Acte	k1gNnSc3	Acte
de	de	k?	de
médiation	médiation	k1gInSc1	médiation
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
protektorem	protektor	k1gInSc7	protektor
(	(	kIx(	(
<g/>
zprostředkovatelem	zprostředkovatel	k1gMnSc7	zprostředkovatel
<g/>
)	)	kIx)	)
Helvetské	helvetský	k2eAgFnSc2d1	helvetská
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
diplomatické	diplomatický	k2eAgFnSc6d1	diplomatická
roztržce	roztržka	k1gFnSc6	roztržka
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
Francie	Francie	k1gFnSc2	Francie
do	do	k7c2	do
válečného	válečný	k2eAgInSc2d1	válečný
stavu	stav	k1gInSc2	stav
s	s	k7c7	s
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
Napoleona	Napoleon	k1gMnSc4	Napoleon
přinutilo	přinutit	k5eAaPmAgNnS	přinutit
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
plánů	plán	k1gInPc2	plán
souvisejících	související	k2eAgInPc2d1	související
s	s	k7c7	s
dosavadní	dosavadní	k2eAgFnSc7d1	dosavadní
koloniální	koloniální	k2eAgFnSc7d1	koloniální
politikou	politika	k1gFnSc7	politika
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
prvotní	prvotní	k2eAgInPc4d1	prvotní
úspěchy	úspěch	k1gInPc4	úspěch
nedokázalo	dokázat	k5eNaPmAgNnS	dokázat
jeho	jeho	k3xOp3gNnSc1	jeho
vojsko	vojsko	k1gNnSc1	vojsko
potlačit	potlačit	k5eAaPmF	potlačit
povstání	povstání	k1gNnSc4	povstání
v	v	k7c6	v
Saint-Domingue	Saint-Domingue	k1gFnSc6	Saint-Domingue
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Francie	Francie	k1gFnSc1	Francie
nebude	být	k5eNaImBp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
udržet	udržet	k5eAaPmF	udržet
námořní	námořní	k2eAgNnSc4d1	námořní
spojení	spojení	k1gNnSc4	spojení
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
državami	država	k1gFnPc7	država
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Mississippi	Mississippi	k1gFnSc2	Mississippi
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
odprodal	odprodat	k5eAaPmAgInS	odprodat
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
část	část	k1gFnSc4	část
Louisiany	Louisiana	k1gFnSc2	Louisiana
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
americkým	americký	k2eAgInPc3d1	americký
za	za	k7c4	za
částku	částka	k1gFnSc4	částka
15	[number]	k4	15
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
<g/>
Přestože	přestože	k8xS	přestože
pro	pro	k7c4	pro
nadcházející	nadcházející	k2eAgInSc4d1	nadcházející
konflikt	konflikt	k1gInSc4	konflikt
Británie	Británie	k1gFnSc2	Británie
disponovala	disponovat	k5eAaBmAgFnS	disponovat
značnou	značný	k2eAgFnSc7d1	značná
námořní	námořní	k2eAgFnSc7d1	námořní
převahou	převaha	k1gFnSc7	převaha
<g/>
,	,	kIx,	,
Napoleon	napoleon	k1gInSc1	napoleon
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
intenzivně	intenzivně	k6eAd1	intenzivně
zabýval	zabývat	k5eAaImAgMnS	zabývat
přípravou	příprava	k1gFnSc7	příprava
invaze	invaze	k1gFnSc2	invaze
přes	přes	k7c4	přes
Lamanšský	lamanšský	k2eAgInSc4d1	lamanšský
průliv	průliv	k1gInSc4	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
si	se	k3xPyFc3	se
naprosto	naprosto	k6eAd1	naprosto
jist	jist	k2eAgMnSc1d1	jist
svým	svůj	k3xOyFgNnSc7	svůj
vítězstvím	vítězství	k1gNnSc7	vítězství
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
začal	začít	k5eAaPmAgInS	začít
i	i	k9	i
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
Evropy	Evropa	k1gFnSc2	Evropa
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
agresivní	agresivní	k2eAgFnSc4d1	agresivní
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1803	[number]	k4	1803
anektoval	anektovat	k5eAaBmAgInS	anektovat
Hannoversko	Hannoversko	k1gNnSc4	Hannoversko
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1804	[number]	k4	1804
nechal	nechat	k5eAaPmAgMnS	nechat
z	z	k7c2	z
území	území	k1gNnSc2	území
Bádenska	Bádensko	k1gNnSc2	Bádensko
unést	unést	k5eAaPmF	unést
a	a	k8xC	a
po	po	k7c6	po
zinscenovaném	zinscenovaný	k2eAgInSc6d1	zinscenovaný
procesu	proces	k1gInSc6	proces
popravit	popravit	k5eAaPmF	popravit
blízkého	blízký	k2eAgMnSc4d1	blízký
příbuzného	příbuzný	k1gMnSc4	příbuzný
bývalého	bývalý	k2eAgMnSc4d1	bývalý
francouzského	francouzský	k2eAgMnSc4d1	francouzský
krále	král	k1gMnSc4	král
Ludvíka	Ludvík	k1gMnSc4	Ludvík
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
vévodu	vévoda	k1gMnSc4	vévoda
Antoina	Antoin	k1gMnSc4	Antoin
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Enghien	Enghien	k1gInSc1	Enghien
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
skutku	skutek	k1gInSc3	skutek
jej	on	k3xPp3gMnSc4	on
vedly	vést	k5eAaImAgInP	vést
zejména	zejména	k9	zejména
indicie	indicie	k1gFnSc2	indicie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
naznačovaly	naznačovat	k5eAaImAgFnP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mladý	mladý	k2eAgMnSc1d1	mladý
příslušník	příslušník	k1gMnSc1	příslušník
rodu	rod	k1gInSc2	rod
Bourbonů	bourbon	k1gInPc2	bourbon
je	být	k5eAaImIp3nS	být
zapleten	zaplést	k5eAaPmNgInS	zaplést
do	do	k7c2	do
spiknutí	spiknutí	k1gNnSc2	spiknutí
připravovaného	připravovaný	k2eAgInSc2d1	připravovaný
britskými	britský	k2eAgMnPc7d1	britský
agenty	agent	k1gMnPc7	agent
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
byl	být	k5eAaImAgInS	být
státní	státní	k2eAgInSc1d1	státní
převrat	převrat	k1gInSc1	převrat
a	a	k8xC	a
Napoleonova	Napoleonův	k2eAgFnSc1d1	Napoleonova
násilná	násilný	k2eAgFnSc1d1	násilná
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
měsíce	měsíc	k1gInSc2	měsíc
Napoleon	napoleon	k1gInSc1	napoleon
vydal	vydat	k5eAaPmAgInS	vydat
nový	nový	k2eAgInSc1d1	nový
občanský	občanský	k2eAgInSc1d1	občanský
zákoník	zákoník	k1gInSc1	zákoník
Code	Code	k1gFnSc1	Code
civil	civil	k1gMnSc1	civil
neboli	neboli	k8xC	neboli
Code	Code	k1gNnSc1	Code
Napoleon	Napoleon	k1gMnSc1	Napoleon
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
přes	přes	k7c4	přes
mnohé	mnohé	k1gNnSc4	mnohé
novely	novela	k1gFnSc2	novela
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
doposud	doposud	k6eAd1	doposud
základem	základ	k1gInSc7	základ
francouzského	francouzský	k2eAgNnSc2d1	francouzské
občanského	občanský	k2eAgNnSc2d1	občanské
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
silně	silně	k6eAd1	silně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
právní	právní	k2eAgFnSc4d1	právní
vědu	věda	k1gFnSc4	věda
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
uskutečnění	uskutečnění	k1gNnSc3	uskutečnění
dalších	další	k2eAgFnPc2d1	další
ambicí	ambice	k1gFnPc2	ambice
Napoleonovi	Napoleonův	k2eAgMnPc1d1	Napoleonův
dopomohla	dopomoct	k5eAaPmAgFnS	dopomoct
obě	dva	k4xCgNnPc4	dva
odhalená	odhalený	k2eAgNnPc4d1	odhalené
spiknutí	spiknutí	k1gNnPc4	spiknutí
usilující	usilující	k2eAgFnSc1d1	usilující
o	o	k7c4	o
jeho	on	k3xPp3gInSc4	on
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
záminkou	záminka	k1gFnSc7	záminka
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
zmařit	zmařit	k5eAaPmF	zmařit
pokusy	pokus	k1gInPc1	pokus
budoucích	budoucí	k2eAgMnPc2d1	budoucí
atentátníků	atentátník	k1gMnPc2	atentátník
zavedením	zavedení	k1gNnSc7	zavedení
dědičného	dědičný	k2eAgNnSc2d1	dědičné
následnictví	následnictví	k1gNnSc2	následnictví
<g/>
,	,	kIx,	,
přiměl	přimět	k5eAaPmAgInS	přimět
senát	senát	k1gInSc1	senát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jej	on	k3xPp3gNnSc2	on
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1804	[number]	k4	1804
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
císařem	císař	k1gMnSc7	císař
<g/>
.	.	kIx.	.
</s>
<s>
Impozantní	impozantní	k2eAgFnSc1d1	impozantní
korunovace	korunovace	k1gFnSc1	korunovace
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
papeže	papež	k1gMnSc2	papež
Pia	Pius	k1gMnSc2	Pius
VII	VII	kA	VII
<g/>
.	.	kIx.	.
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
Notre	Notr	k1gInSc5	Notr
<g/>
–	–	k?	–
<g/>
Dame	Dame	k1gNnPc1	Dame
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
Napoleon	napoleon	k1gInSc1	napoleon
dobře	dobře	k6eAd1	dobře
chápal	chápat	k5eAaImAgInS	chápat
důležitost	důležitost	k1gFnSc4	důležitost
symbolů	symbol	k1gInPc2	symbol
<g/>
,	,	kIx,	,
vystoupal	vystoupat	k5eAaPmAgInS	vystoupat
při	při	k7c6	při
církevním	církevní	k2eAgInSc6d1	církevní
obřadu	obřad	k1gInSc6	obřad
k	k	k7c3	k
oltáři	oltář	k1gInSc3	oltář
<g/>
,	,	kIx,	,
uchopil	uchopit	k5eAaPmAgInS	uchopit
korunu	koruna	k1gFnSc4	koruna
ležící	ležící	k2eAgFnSc4d1	ležící
na	na	k7c6	na
podstavci	podstavec	k1gInSc6	podstavec
<g/>
,	,	kIx,	,
otočil	otočit	k5eAaPmAgMnS	otočit
se	s	k7c7	s
zády	záda	k1gNnPc7	záda
ke	k	k7c3	k
všem	všecek	k3xTgNnPc3	všecek
asistujícím	asistující	k2eAgNnPc3d1	asistující
včetně	včetně	k7c2	včetně
papeže	papež	k1gMnSc2	papež
a	a	k8xC	a
za	za	k7c4	za
císaře	císař	k1gMnSc4	císař
Francouzů	Francouz	k1gMnPc2	Francouz
se	se	k3xPyFc4	se
korunoval	korunovat	k5eAaBmAgMnS	korunovat
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
vložil	vložit	k5eAaPmAgMnS	vložit
korunu	koruna	k1gFnSc4	koruna
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
své	svůj	k3xOyFgFnSc2	svůj
manželky	manželka	k1gFnSc2	manželka
Josefiny	Josefin	k2eAgInPc1d1	Josefin
a	a	k8xC	a
po	po	k7c6	po
oficiálním	oficiální	k2eAgNnSc6d1	oficiální
uvedení	uvedení	k1gNnSc6	uvedení
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
převzal	převzít	k5eAaPmAgInS	převzít
tradiční	tradiční	k2eAgInSc1d1	tradiční
symboly	symbol	k1gInPc7	symbol
panovnické	panovnický	k2eAgFnSc2d1	panovnická
moci	moc	k1gFnSc2	moc
<g/>
:	:	kIx,	:
říšské	říšský	k2eAgNnSc1d1	říšské
jablko	jablko	k1gNnSc1	jablko
<g/>
,	,	kIx,	,
žezlo	žezlo	k1gNnSc1	žezlo
a	a	k8xC	a
meč	meč	k1gInSc1	meč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Císařství	císařství	k1gNnSc2	císařství
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Nástup	nástup	k1gInSc1	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
a	a	k8xC	a
vítězství	vítězství	k1gNnSc4	vítězství
nad	nad	k7c7	nad
Rakouskem	Rakousko	k1gNnSc7	Rakousko
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1804	[number]	k4	1804
Napoleon	napoleon	k1gInSc1	napoleon
k	k	k7c3	k
Francii	Francie	k1gFnSc3	Francie
připojil	připojit	k5eAaPmAgInS	připojit
Ligurskou	ligurský	k2eAgFnSc4d1	Ligurská
republiku	republika	k1gFnSc4	republika
a	a	k8xC	a
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1805	[number]	k4	1805
podnikl	podniknout	k5eAaPmAgMnS	podniknout
vítěznou	vítězný	k2eAgFnSc4d1	vítězná
cestu	cesta	k1gFnSc4	cesta
po	po	k7c6	po
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
se	se	k3xPyFc4	se
korunovat	korunovat	k5eAaBmF	korunovat
italským	italský	k2eAgMnSc7d1	italský
králem	král	k1gMnSc7	král
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
jeho	jeho	k3xOp3gFnSc1	jeho
územní	územní	k2eAgFnSc1d1	územní
rozpínavost	rozpínavost	k1gFnSc1	rozpínavost
a	a	k8xC	a
bezohledné	bezohledný	k2eAgNnSc1d1	bezohledné
počínání	počínání	k1gNnSc1	počínání
již	již	k6eAd1	již
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
vzbuzovaly	vzbuzovat	k5eAaImAgInP	vzbuzovat
hněv	hněv	k1gInSc4	hněv
a	a	k8xC	a
nelibost	nelibost	k1gFnSc4	nelibost
u	u	k7c2	u
cara	car	k1gMnSc2	car
Alexandra	Alexandr	k1gMnSc2	Alexandr
I.	I.	kA	I.
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1803	[number]	k4	1803
prozatím	prozatím	k6eAd1	prozatím
neúspěšně	úspěšně	k6eNd1	úspěšně
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
uzavřít	uzavřít	k5eAaPmF	uzavřít
protifrancouzskou	protifrancouzský	k2eAgFnSc4d1	protifrancouzská
útočnou	útočný	k2eAgFnSc4d1	útočná
alianci	aliance	k1gFnSc4	aliance
s	s	k7c7	s
vládci	vládce	k1gMnPc7	vládce
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýraznějšímu	výrazný	k2eAgNnSc3d3	nejvýraznější
ochlazení	ochlazení	k1gNnSc3	ochlazení
francouzsko-ruských	francouzskouský	k2eAgInPc2d1	francouzsko-ruský
vztahů	vztah	k1gInPc2	vztah
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
popravě	poprava	k1gFnSc6	poprava
vévody	vévoda	k1gMnSc2	vévoda
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Enghien	Enghien	k1gInSc1	Enghien
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1804	[number]	k4	1804
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Napoleon	napoleon	k1gInSc1	napoleon
nechápal	chápat	k5eNaImAgInS	chápat
důvod	důvod	k1gInSc4	důvod
carova	carův	k2eAgNnSc2d1	carovo
jednání	jednání	k1gNnSc2	jednání
a	a	k8xC	a
společně	společně	k6eAd1	společně
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
diplomaty	diplomat	k1gInPc7	diplomat
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgInS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
jen	jen	k9	jen
o	o	k7c4	o
dočasnou	dočasný	k2eAgFnSc4d1	dočasná
záležitost	záležitost	k1gFnSc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgMnSc1d1	ruský
panovník	panovník	k1gMnSc1	panovník
však	však	k9	však
zatvrzele	zatvrzele	k6eAd1	zatvrzele
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
spojenectví	spojenectví	k1gNnSc4	spojenectví
s	s	k7c7	s
některou	některý	k3yIgFnSc7	některý
z	z	k7c2	z
evropských	evropský	k2eAgFnPc2d1	Evropská
velmocí	velmoc	k1gFnPc2	velmoc
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnPc3	jeho
pověřencům	pověřenec	k1gMnPc3	pověřenec
podařilo	podařit	k5eAaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
úspěchu	úspěch	k1gInSc3	úspěch
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
dohodu	dohoda	k1gFnSc4	dohoda
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1805	[number]	k4	1805
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
alianci	aliance	k1gFnSc3	aliance
se	se	k3xPyFc4	se
po	po	k7c6	po
diplomatickém	diplomatický	k2eAgInSc6d1	diplomatický
nátlaku	nátlak	k1gInSc6	nátlak
následně	následně	k6eAd1	následně
připojil	připojit	k5eAaPmAgInS	připojit
i	i	k9	i
rakouský	rakouský	k2eAgMnSc1d1	rakouský
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
I.	I.	kA	I.
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
proti	proti	k7c3	proti
Francii	Francie	k1gFnSc3	Francie
popudila	popudit	k5eAaPmAgFnS	popudit
zejména	zejména	k9	zejména
Napoleonova	Napoleonův	k2eAgFnSc1d1	Napoleonova
anexe	anexe	k1gFnSc1	anexe
Janovska	Janovsko	k1gNnSc2	Janovsko
a	a	k8xC	a
Luccy	Lucca	k1gFnSc2	Lucca
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
podepsání	podepsání	k1gNnSc3	podepsání
britsko-rusko-rakouské	britskouskoakouský	k2eAgFnSc2d1	britsko-rusko-rakouský
spojenecké	spojenecký	k2eAgFnSc2d1	spojenecká
deklarace	deklarace	k1gFnSc2	deklarace
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
III	III	kA	III
<g/>
.	.	kIx.	.
koalice	koalice	k1gFnSc1	koalice
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
<g/>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Napoleon	Napoleon	k1gMnSc1	Napoleon
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
obranné	obranný	k2eAgInPc4d1	obranný
válce	válec	k1gInPc4	válec
na	na	k7c6	na
několika	několik	k4yIc6	několik
frontách	fronta	k1gFnPc6	fronta
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
zaútočit	zaútočit	k5eAaPmF	zaútočit
na	na	k7c4	na
nejbližšího	blízký	k2eAgMnSc4d3	nejbližší
nepřítele	nepřítel	k1gMnSc4	nepřítel
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
bylo	být	k5eAaImAgNnS	být
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
vypořádat	vypořádat	k5eAaPmF	vypořádat
s	s	k7c7	s
Rusy	Rus	k1gMnPc7	Rus
<g/>
.	.	kIx.	.
</s>
<s>
Tažení	tažení	k1gNnSc1	tažení
se	se	k3xPyFc4	se
účastnilo	účastnit	k5eAaImAgNnS	účastnit
na	na	k7c4	na
200	[number]	k4	200
000	[number]	k4	000
francouzských	francouzský	k2eAgMnPc2d1	francouzský
vojáků	voják	k1gMnPc2	voják
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Anglické	anglický	k2eAgFnSc2d1	anglická
armády	armáda	k1gFnSc2	armáda
(	(	kIx(	(
<g/>
sbory	sbor	k1gInPc1	sbor
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
byly	být	k5eAaImAgInP	být
připraveny	připravit	k5eAaPmNgInP	připravit
u	u	k7c2	u
Lamanšského	lamanšský	k2eAgInSc2d1	lamanšský
průlivu	průliv	k1gInSc2	průliv
na	na	k7c6	na
invazi	invaze	k1gFnSc6	invaze
do	do	k7c2	do
Británie	Británie	k1gFnSc2	Británie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
kampaň	kampaň	k1gFnSc1	kampaň
nesla	nést	k5eAaImAgFnS	nést
nové	nový	k2eAgNnSc4d1	nové
Napoleonovo	Napoleonův	k2eAgNnSc4d1	Napoleonovo
přízvisko	přízvisko	k1gNnSc4	přízvisko
Grande	grand	k1gMnSc5	grand
Armée	Armée	k1gFnPc6	Armée
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Allemagne	Allemagn	k1gInSc5	Allemagn
–	–	k?	–
Velká	velký	k2eAgFnSc1d1	velká
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
(	(	kIx(	(
<g/>
všeobecně	všeobecně	k6eAd1	všeobecně
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
jen	jen	k9	jen
Grande	grand	k1gMnSc5	grand
Armée	Armée	k1gFnSc7	Armée
–	–	k?	–
Velká	velký	k2eAgFnSc1d1	velká
armáda	armáda	k1gFnSc1	armáda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Francouzští	francouzský	k2eAgMnPc1d1	francouzský
vojáci	voják	k1gMnPc1	voják
tedy	tedy	k9	tedy
místo	místo	k1gNnSc4	místo
připravovaného	připravovaný	k2eAgNnSc2d1	připravované
vylodění	vylodění	k1gNnSc2	vylodění
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
v	v	k7c6	v
sedmi	sedm	k4xCc6	sedm
proudech	proud	k1gInPc6	proud
do	do	k7c2	do
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
soustřeďoval	soustřeďovat	k5eAaImAgMnS	soustřeďovat
podmaršálek	podmaršálek	k1gMnSc1	podmaršálek
Karl	Karl	k1gMnSc1	Karl
Mack	Mack	k1gMnSc1	Mack
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
namáhavých	namáhavý	k2eAgInPc6d1	namáhavý
pochodech	pochod	k1gInPc6	pochod
se	se	k3xPyFc4	se
přesunovali	přesunovat	k5eAaImAgMnP	přesunovat
rychlostí	rychlost	k1gFnSc7	rychlost
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
km	km	kA	km
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
rakouský	rakouský	k2eAgMnSc1d1	rakouský
vrchní	vrchní	k2eAgMnSc1d1	vrchní
velitel	velitel	k1gMnSc1	velitel
tušil	tušit	k5eAaImAgMnS	tušit
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
dorazily	dorazit	k5eAaPmAgInP	dorazit
sbory	sbor	k1gInPc1	sbor
francouzských	francouzský	k2eAgMnPc2d1	francouzský
maršálů	maršál	k1gMnPc2	maršál
Soulta	Soult	k1gMnSc2	Soult
<g/>
,	,	kIx,	,
Davouta	Davout	k1gMnSc2	Davout
a	a	k8xC	a
Lanese	Lanese	k1gFnPc4	Lanese
a	a	k8xC	a
Muratovo	Muratův	k2eAgNnSc4d1	Muratovo
jezdectvo	jezdectvo	k1gNnSc4	jezdectvo
k	k	k7c3	k
Dunaji	Dunaj	k1gInSc3	Dunaj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
překročily	překročit	k5eAaPmAgFnP	překročit
60	[number]	k4	60
kilometrů	kilometr	k1gInPc2	kilometr
v	v	k7c6	v
Mackově	Mackův	k2eAgInSc6d1	Mackův
týlu	týl	k1gInSc6	týl
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
podmaršálek	podmaršálek	k1gMnSc1	podmaršálek
i	i	k9	i
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
rakouské	rakouský	k2eAgFnSc2d1	rakouská
armády	armáda	k1gFnSc2	armáda
obklíčen	obklíčit	k5eAaPmNgMnS	obklíčit
u	u	k7c2	u
Ulmu	Ulmus	k1gInSc2	Ulmus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
před	před	k7c7	před
svým	svůj	k3xOyFgMnSc7	svůj
protivníkem	protivník	k1gMnSc7	protivník
kapituloval	kapitulovat	k5eAaBmAgMnS	kapitulovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
obklíčení	obklíčení	k1gNnSc2	obklíčení
ho	on	k3xPp3gMnSc4	on
nevyprostila	vyprostit	k5eNaPmAgFnS	vyprostit
ani	ani	k8xC	ani
Podolská	podolský	k2eAgFnSc1d1	Podolská
armáda	armáda	k1gFnSc1	armáda
ruského	ruský	k2eAgMnSc2d1	ruský
generála	generál	k1gMnSc2	generál
Kutuzova	Kutuzův	k2eAgMnSc2d1	Kutuzův
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
vyslal	vyslat	k5eAaPmAgMnS	vyslat
Rakušanům	Rakušan	k1gMnPc3	Rakušan
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
jejich	jejich	k3xOp3gMnSc1	jejich
spojenec	spojenec	k1gMnSc1	spojenec
car	car	k1gMnSc1	car
Alexandr	Alexandr	k1gMnSc1	Alexandr
I.	I.	kA	I.
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
pokračujícího	pokračující	k2eAgNnSc2d1	pokračující
tažení	tažení	k1gNnSc2	tažení
padla	padnout	k5eAaPmAgNnP	padnout
do	do	k7c2	do
francouzských	francouzský	k2eAgFnPc2d1	francouzská
rukou	ruka	k1gFnPc2	ruka
i	i	k8xC	i
Vídeň	Vídeň	k1gFnSc1	Vídeň
a	a	k8xC	a
Napoleon	napoleon	k1gInSc1	napoleon
se	se	k3xPyFc4	se
ve	v	k7c6	v
stopách	stopa	k1gFnPc6	stopa
ustupujících	ustupující	k2eAgMnPc2d1	ustupující
Rusů	Rus	k1gMnPc2	Rus
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
rozdrtil	rozdrtit	k5eAaPmAgMnS	rozdrtit
vojska	vojsko	k1gNnPc4	vojsko
koalice	koalice	k1gFnSc2	koalice
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Slavkova	Slavkov	k1gInSc2	Slavkov
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
přinutil	přinutit	k5eAaPmAgMnS	přinutit
Rakušany	Rakušan	k1gMnPc4	Rakušan
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
tzv.	tzv.	kA	tzv.
prešpurský	prešpurský	k2eAgInSc4d1	prešpurský
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
pro	pro	k7c4	pro
ně	on	k3xPp3gNnSc4	on
znamenal	znamenat	k5eAaImAgInS	znamenat
ztrátu	ztráta	k1gFnSc4	ztráta
italských	italský	k2eAgMnPc2d1	italský
a	a	k8xC	a
velké	velký	k2eAgFnSc3d1	velká
části	část	k1gFnSc3	část
německých	německý	k2eAgNnPc2d1	německé
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Prusku	Prusko	k1gNnSc6	Prusko
a	a	k8xC	a
Polsku	Polsko	k1gNnSc6	Polsko
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
u	u	k7c2	u
Slavkova	Slavkov	k1gInSc2	Slavkov
se	se	k3xPyFc4	se
Napoleon	napoleon	k1gInSc1	napoleon
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
jako	jako	k8xS	jako
celý	celý	k2eAgInSc4d1	celý
francouzský	francouzský	k2eAgInSc4d1	francouzský
národ	národ	k1gInSc4	národ
si	se	k3xPyFc3	se
i	i	k9	i
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
z	z	k7c2	z
osobních	osobní	k2eAgInPc2d1	osobní
i	i	k8xC	i
dynastických	dynastický	k2eAgInPc2d1	dynastický
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
upřímně	upřímně	k6eAd1	upřímně
přál	přát	k5eAaImAgMnS	přát
evropský	evropský	k2eAgInSc4d1	evropský
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Odpor	odpor	k1gInSc1	odpor
zbytků	zbytek	k1gInPc2	zbytek
III	III	kA	III
<g/>
.	.	kIx.	.
koalice	koalice	k1gFnSc2	koalice
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
zlomen	zlomen	k2eAgInSc1d1	zlomen
<g/>
.	.	kIx.	.
</s>
<s>
Ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
Alexandr	Alexandr	k1gMnSc1	Alexandr
I.	I.	kA	I.
s	s	k7c7	s
Napoleonem	napoleon	k1gInSc7	napoleon
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
jednat	jednat	k5eAaImF	jednat
bez	bez	k7c2	bez
dalšího	další	k2eAgInSc2d1	další
boje	boj	k1gInSc2	boj
a	a	k8xC	a
porážka	porážka	k1gFnSc1	porážka
francouzsko-španělského	francouzsko-španělský	k2eAgNnSc2d1	francouzsko-španělské
loďstva	loďstvo	k1gNnSc2	loďstvo
v	v	k7c6	v
námořní	námořní	k2eAgFnSc6d1	námořní
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Trafalgaru	Trafalgar	k1gInSc2	Trafalgar
proti	proti	k7c3	proti
britské	britský	k2eAgFnSc3d1	britská
flotě	flota	k1gFnSc3	flota
vedené	vedený	k2eAgFnSc2d1	vedená
viceadmirálem	viceadmirál	k1gMnSc7	viceadmirál
Horatiem	Horatius	k1gMnSc7	Horatius
Nelsonem	Nelson	k1gMnSc7	Nelson
zmařila	zmařit	k5eAaPmAgFnS	zmařit
francouzské	francouzský	k2eAgFnPc4d1	francouzská
naděje	naděje	k1gFnPc4	naděje
na	na	k7c6	na
invazi	invaze	k1gFnSc6	invaze
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
pokoření	pokoření	k1gNnSc4	pokoření
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
i	i	k9	i
v	v	k7c6	v
prozatím	prozatím	k6eAd1	prozatím
neutrálním	neutrální	k2eAgNnSc6d1	neutrální
Prusku	Prusko	k1gNnSc6	Prusko
začaly	začít	k5eAaPmAgFnP	začít
sílit	sílit	k5eAaImF	sílit
protifrancouzské	protifrancouzský	k2eAgFnPc1d1	protifrancouzská
válečné	válečný	k2eAgFnPc1d1	válečná
tendence	tendence	k1gFnPc1	tendence
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
Prusku	Prusko	k1gNnSc6	Prusko
zapříčinil	zapříčinit	k5eAaPmAgMnS	zapříčinit
i	i	k9	i
sám	sám	k3xTgMnSc1	sám
Napoleon	Napoleon	k1gMnSc1	Napoleon
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
výraznému	výrazný	k2eAgNnSc3d1	výrazné
narušení	narušení	k1gNnSc3	narušení
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
vztahů	vztah	k1gInPc2	vztah
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1805	[number]	k4	1805
<g/>
,	,	kIx,	,
když	když	k8xS	když
sbor	sbor	k1gInSc1	sbor
maršála	maršál	k1gMnSc4	maršál
Bernadotta	Bernadott	k1gInSc2	Bernadott
s	s	k7c7	s
císařovým	císařův	k2eAgNnSc7d1	císařovo
vědomím	vědomí	k1gNnSc7	vědomí
prošel	projít	k5eAaPmAgMnS	projít
skrze	skrze	k?	skrze
Ansbašsko	Ansbašsko	k1gNnSc4	Ansbašsko
a	a	k8xC	a
porušil	porušit	k5eAaPmAgMnS	porušit
tak	tak	k6eAd1	tak
pruskou	pruský	k2eAgFnSc4d1	pruská
neutralitu	neutralita	k1gFnSc4	neutralita
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
trhlinu	trhlina	k1gFnSc4	trhlina
ve	v	k7c6	v
vzájemných	vzájemný	k2eAgInPc6d1	vzájemný
vztazích	vztah	k1gInPc6	vztah
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
Napoleonem	napoleon	k1gInSc7	napoleon
dojednaná	dojednaný	k2eAgFnSc1d1	dojednaná
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
z	z	k7c2	z
francouzské	francouzský	k2eAgFnSc2d1	francouzská
strany	strana	k1gFnSc2	strana
nerealizovaná	realizovaný	k2eNgFnSc1d1	nerealizovaná
výměna	výměna	k1gFnSc1	výměna
švýcarského	švýcarský	k2eAgInSc2d1	švýcarský
kantonu	kanton	k1gInSc2	kanton
Neuchâtel	Neuchâtela	k1gFnPc2	Neuchâtela
za	za	k7c4	za
Hannoversko	Hannoversko	k1gNnSc4	Hannoversko
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
podezřelý	podezřelý	k2eAgInSc1d1	podezřelý
obchod	obchod	k1gInSc1	obchod
dokonce	dokonce	k9	dokonce
Napoleon	Napoleon	k1gMnSc1	Napoleon
znesvětil	znesvětit	k5eAaPmAgMnS	znesvětit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1806	[number]	k4	1806
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
jím	on	k3xPp3gMnSc7	on
obsazené	obsazený	k2eAgInPc4d1	obsazený
Hannoversko	Hannoverska	k1gFnSc5	Hannoverska
anglickému	anglický	k2eAgMnSc3d1	anglický
králi	král	k1gMnSc3	král
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
zároveň	zároveň	k6eAd1	zároveň
legitimním	legitimní	k2eAgMnSc7d1	legitimní
panovníkem	panovník	k1gMnSc7	panovník
Hannoverska	Hannoversk	k1gInSc2	Hannoversk
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
v	v	k7c6	v
Prusku	Prusko	k1gNnSc6	Prusko
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
jednoznačné	jednoznačný	k2eAgNnSc4d1	jednoznačné
válečné	válečný	k2eAgNnSc4d1	válečné
pohnutí	pohnutí	k1gNnSc4	pohnutí
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
znepokojení	znepokojení	k1gNnSc1	znepokojení
pociťovalo	pociťovat	k5eAaImAgNnS	pociťovat
Prusko	Prusko	k1gNnSc4	Prusko
nad	nad	k7c7	nad
vytvořením	vytvoření	k1gNnSc7	vytvoření
Rýnského	rýnský	k2eAgInSc2d1	rýnský
spolku	spolek	k1gInSc2	spolek
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
byl	být	k5eAaImAgInS	být
Napoleonův	Napoleonův	k2eAgInSc1d1	Napoleonův
krok	krok	k1gInSc1	krok
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
upevnění	upevnění	k1gNnSc3	upevnění
jeho	jeho	k3xOp3gFnSc2	jeho
nadvlády	nadvláda	k1gFnSc2	nadvláda
nad	nad	k7c7	nad
rozsáhlými	rozsáhlý	k2eAgNnPc7d1	rozsáhlé
územími	území	k1gNnPc7	území
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
opatření	opatření	k1gNnSc1	opatření
zapříčinilo	zapříčinit	k5eAaPmAgNnS	zapříčinit
definitivní	definitivní	k2eAgInSc4d1	definitivní
zánik	zánik	k1gInSc4	zánik
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
mocenské	mocenský	k2eAgFnPc4d1	mocenská
ambice	ambice	k1gFnPc4	ambice
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
Napoleon	napoleon	k1gInSc1	napoleon
také	také	k9	také
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1806	[number]	k4	1806
nařídil	nařídit	k5eAaPmAgMnS	nařídit
maršálu	maršál	k1gMnSc3	maršál
Massénovi	Massén	k1gMnSc3	Massén
obsadit	obsadit	k5eAaPmF	obsadit
Papežský	papežský	k2eAgInSc4d1	papežský
stát	stát	k1gInSc4	stát
a	a	k8xC	a
pevninskou	pevninský	k2eAgFnSc4d1	pevninská
část	část	k1gFnSc4	část
Království	království	k1gNnSc2	království
obojí	oboj	k1gFnPc2	oboj
Sicílie	Sicílie	k1gFnSc2	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Smrtí	smrt	k1gFnSc7	smrt
jeho	jeho	k3xOp3gMnSc2	jeho
velkého	velký	k2eAgMnSc2d1	velký
protivníka	protivník	k1gMnSc2	protivník
<g/>
,	,	kIx,	,
britského	britský	k2eAgMnSc2d1	britský
premiéra	premiér	k1gMnSc2	premiér
Williama	William	k1gMnSc2	William
Pitta	Pitt	k1gMnSc2	Pitt
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Napoleonovi	Napoleonův	k2eAgMnPc1d1	Napoleonův
nakonec	nakonec	k6eAd1	nakonec
otevřela	otevřít	k5eAaPmAgFnS	otevřít
možnost	možnost	k1gFnSc4	možnost
jednat	jednat	k5eAaImF	jednat
s	s	k7c7	s
ostrovní	ostrovní	k2eAgFnSc7d1	ostrovní
zemí	zem	k1gFnSc7	zem
o	o	k7c4	o
míru	míra	k1gFnSc4	míra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ratifikace	ratifikace	k1gFnSc1	ratifikace
tohoto	tento	k3xDgInSc2	tento
míru	mír	k1gInSc2	mír
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
být	být	k5eAaImF	být
nemožnou	nemožná	k1gFnSc4	nemožná
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prvních	první	k4xOgInPc6	první
měsících	měsíc	k1gInPc6	měsíc
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
1806	[number]	k4	1806
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
v	v	k7c6	v
Prusku	Prusko	k1gNnSc6	Prusko
začala	začít	k5eAaPmAgFnS	začít
vyostřovat	vyostřovat	k5eAaImF	vyostřovat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
zjevně	zjevně	k6eAd1	zjevně
nepřátelské	přátelský	k2eNgFnPc4d1	nepřátelská
nálady	nálada	k1gFnPc4	nálada
mezi	mezi	k7c7	mezi
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
jejími	její	k3xOp3gMnPc7	její
protivníky	protivník	k1gMnPc7	protivník
Napoleon	Napoleon	k1gMnSc1	Napoleon
do	do	k7c2	do
poslední	poslední	k2eAgFnSc2d1	poslední
chvíle	chvíle	k1gFnSc2	chvíle
váhal	váhat	k5eAaImAgInS	váhat
s	s	k7c7	s
válečnými	válečný	k2eAgFnPc7d1	válečná
přípravami	příprava	k1gFnPc7	příprava
a	a	k8xC	a
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
krize	krize	k1gFnSc1	krize
bude	být	k5eAaImBp3nS	být
zažehnána	zažehnán	k2eAgFnSc1d1	zažehnána
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
však	však	k9	však
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nP	zářit
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnPc2	jeho
nepřátelé	nepřítel	k1gMnPc1	nepřítel
sjednotili	sjednotit	k5eAaPmAgMnP	sjednotit
ve	v	k7c6	v
IV	IV	kA	IV
<g/>
.	.	kIx.	.
koalici	koalice	k1gFnSc4	koalice
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
byly	být	k5eAaImAgFnP	být
zainteresovány	zainteresován	k2eAgFnPc1d1	zainteresována
Anglie	Anglie	k1gFnPc1	Anglie
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
a	a	k8xC	a
Prusko	Prusko	k1gNnSc1	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
podzimu	podzim	k1gInSc2	podzim
pak	pak	k6eAd1	pak
Prusko	Prusko	k1gNnSc1	Prusko
předložilo	předložit	k5eAaPmAgNnS	předložit
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
ultimátum	ultimátum	k1gNnSc4	ultimátum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bez	bez	k7c2	bez
odezvy	odezva	k1gFnSc2	odezva
vypršelo	vypršet	k5eAaPmAgNnS	vypršet
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
císař	císař	k1gMnSc1	císař
neváhal	váhat	k5eNaImAgMnS	váhat
a	a	k8xC	a
druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
jeho	jeho	k3xOp3gFnSc1	jeho
armáda	armáda	k1gFnSc1	armáda
překročila	překročit	k5eAaPmAgFnS	překročit
hranice	hranice	k1gFnPc4	hranice
Saska	Sasko	k1gNnSc2	Sasko
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1806	[number]	k4	1806
<g/>
)	)	kIx)	)
Napoleon	Napoleon	k1gMnSc1	Napoleon
porazil	porazit	k5eAaPmAgMnS	porazit
část	část	k1gFnSc4	část
pruské	pruský	k2eAgFnSc2d1	pruská
armády	armáda	k1gFnSc2	armáda
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Jeny	jen	k1gInPc4	jen
a	a	k8xC	a
hlavní	hlavní	k2eAgFnSc4d1	hlavní
část	část	k1gFnSc4	část
nepřátelských	přátelský	k2eNgFnPc2d1	nepřátelská
sil	síla	k1gFnPc2	síla
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
rozdrtil	rozdrtit	k5eAaPmAgMnS	rozdrtit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Auerstedtu	Auerstedt	k1gInSc2	Auerstedt
maršál	maršál	k1gMnSc1	maršál
Louis-Nicolas	Louis-Nicolas	k1gMnSc1	Louis-Nicolas
Davout	Davout	k1gMnSc1	Davout
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
jediného	jediný	k2eAgInSc2d1	jediný
dne	den	k1gInSc2	den
tak	tak	k6eAd1	tak
přestala	přestat	k5eAaPmAgFnS	přestat
pruská	pruský	k2eAgFnSc1d1	pruská
armáda	armáda	k1gFnSc1	armáda
téměř	téměř	k6eAd1	téměř
zcela	zcela	k6eAd1	zcela
existovat	existovat	k5eAaImF	existovat
jako	jako	k8xC	jako
bojová	bojový	k2eAgFnSc1d1	bojová
síla	síla	k1gFnSc1	síla
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnPc1	její
ztráty	ztráta	k1gFnPc1	ztráta
činily	činit	k5eAaImAgFnP	činit
45	[number]	k4	45
000	[number]	k4	000
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
,	,	kIx,	,
zraněných	zraněný	k1gMnPc2	zraněný
a	a	k8xC	a
zajatých	zajatá	k1gFnPc2	zajatá
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
přišla	přijít	k5eAaPmAgFnS	přijít
o	o	k7c4	o
200	[number]	k4	200
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
porážka	porážka	k1gFnSc1	porážka
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
naprostý	naprostý	k2eAgInSc4d1	naprostý
morální	morální	k2eAgInSc4d1	morální
rozklad	rozklad	k1gInSc4	rozklad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
kolaps	kolaps	k1gInSc4	kolaps
zažádal	zažádat	k5eAaPmAgMnS	zažádat
pruský	pruský	k2eAgMnSc1d1	pruský
král	král	k1gMnSc1	král
22	[number]	k4	22
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
o	o	k7c4	o
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
Napoleon	napoleon	k1gInSc4	napoleon
si	se	k3xPyFc3	se
však	však	k9	však
při	při	k7c6	při
zdlouhavém	zdlouhavý	k2eAgNnSc6d1	zdlouhavé
jednání	jednání	k1gNnSc6	jednání
s	s	k7c7	s
vyslanci	vyslanec	k1gMnPc1	vyslanec
pruského	pruský	k2eAgMnSc2d1	pruský
krále	král	k1gMnSc2	král
Fridricha	Fridrich	k1gMnSc4	Fridrich
Viléma	Vilém	k1gMnSc2	Vilém
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
do	do	k7c2	do
Königsbergu	Königsberg	k1gInSc2	Königsberg
<g/>
,	,	kIx,	,
záměrně	záměrně	k6eAd1	záměrně
kladl	klást	k5eAaImAgMnS	klást
nesplnitelné	splnitelný	k2eNgFnPc4d1	nesplnitelná
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
po	po	k7c6	po
příměří	příměří	k1gNnSc6	příměří
ani	ani	k8xC	ani
míru	míra	k1gFnSc4	míra
s	s	k7c7	s
Pruskem	Prusko	k1gNnSc7	Prusko
netoužil	toužit	k5eNaImAgMnS	toužit
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
vstoupila	vstoupit	k5eAaPmAgNnP	vstoupit
francouzská	francouzský	k2eAgNnPc1d1	francouzské
vojska	vojsko	k1gNnPc1	vojsko
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dalších	další	k2eAgInPc2d1	další
bojů	boj	k1gInPc2	boj
se	se	k3xPyFc4	se
Francouzi	Francouz	k1gMnPc1	Francouz
převalili	převalit	k5eAaPmAgMnP	převalit
přes	přes	k7c4	přes
celé	celý	k2eAgNnSc4d1	celé
Západní	západní	k2eAgNnSc4d1	západní
Prusko	Prusko	k1gNnSc4	Prusko
až	až	k9	až
k	k	k7c3	k
břehům	břeh	k1gInPc3	břeh
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zajali	zajmout	k5eAaPmAgMnP	zajmout
tisíce	tisíc	k4xCgInPc1	tisíc
nepřátelských	přátelský	k2eNgMnPc2d1	nepřátelský
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
podepsal	podepsat	k5eAaPmAgMnS	podepsat
Napoleon	Napoleon	k1gMnSc1	Napoleon
dekrety	dekret	k1gInPc4	dekret
o	o	k7c4	o
zavedení	zavedení	k1gNnSc4	zavedení
kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
blokády	blokáda	k1gFnSc2	blokáda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
uzavírala	uzavírat	k5eAaImAgFnS	uzavírat
přístavy	přístav	k1gInPc4	přístav
Francií	Francie	k1gFnPc2	Francie
ovládaných	ovládaný	k2eAgFnPc2d1	ovládaná
zemí	zem	k1gFnPc2	zem
anglickému	anglický	k2eAgInSc3d1	anglický
exportu	export	k1gInSc3	export
i	i	k8xC	i
importu	import	k1gInSc3	import
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
listopadovém	listopadový	k2eAgInSc6d1	listopadový
týdnu	týden	k1gInSc6	týden
pak	pak	k6eAd1	pak
dal	dát	k5eAaPmAgMnS	dát
svým	svůj	k3xOyFgInPc3	svůj
sborům	sbor	k1gInPc3	sbor
povel	povel	k1gInSc1	povel
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
vojensky	vojensky	k6eAd1	vojensky
střetnout	střetnout	k5eAaPmF	střetnout
s	s	k7c7	s
carem	car	k1gMnSc7	car
Alexandrem	Alexandr	k1gMnSc7	Alexandr
I.	I.	kA	I.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
pomalým	pomalý	k2eAgNnSc7d1	pomalé
tempem	tempo	k1gNnSc7	tempo
posunoval	posunovat	k5eAaImAgMnS	posunovat
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
pruskému	pruský	k2eAgMnSc3d1	pruský
panovníkovi	panovník	k1gMnSc3	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Polské	polský	k2eAgFnPc1d1	polská
vyšší	vysoký	k2eAgFnPc1d2	vyšší
vrstvy	vrstva	k1gFnPc1	vrstva
vnímaly	vnímat	k5eAaImAgFnP	vnímat
francouzského	francouzský	k2eAgMnSc2d1	francouzský
císaře	císař	k1gMnSc2	císař
jako	jako	k8xS	jako
možného	možný	k2eAgMnSc4d1	možný
obnovitele	obnovitel	k1gMnSc4	obnovitel
své	svůj	k3xOyFgFnSc2	svůj
ztracené	ztracený	k2eAgFnSc2d1	ztracená
samostatnosti	samostatnost	k1gFnSc2	samostatnost
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
myšlence	myšlenka	k1gFnSc3	myšlenka
choval	chovat	k5eAaImAgInS	chovat
poměrně	poměrně	k6eAd1	poměrně
chladně	chladně	k6eAd1	chladně
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
se	se	k3xPyFc4	se
Napoleon	napoleon	k1gInSc1	napoleon
s	s	k7c7	s
Rusy	Rus	k1gMnPc7	Rus
střetl	střetnout	k5eAaPmAgMnS	střetnout
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Pultuska	Pultusek	k1gMnSc2	Pultusek
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
ze	z	k7c2	z
strategického	strategický	k2eAgNnSc2d1	strategické
hlediska	hledisko	k1gNnSc2	hledisko
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
vítězství	vítězství	k1gNnSc3	vítězství
francouzské	francouzský	k2eAgFnSc2d1	francouzská
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zima	zima	k6eAd1	zima
i	i	k9	i
vyčerpání	vyčerpání	k1gNnSc2	vyčerpání
vlastních	vlastní	k2eAgMnPc2d1	vlastní
vojáků	voják	k1gMnPc2	voják
přinutily	přinutit	k5eAaPmAgFnP	přinutit
Napoleona	Napoleon	k1gMnSc4	Napoleon
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
armádu	armáda	k1gFnSc4	armáda
ubytoval	ubytovat	k5eAaPmAgMnS	ubytovat
do	do	k7c2	do
zimních	zimní	k2eAgInPc2d1	zimní
kvartýrů	kvartýrů	k?	kvartýrů
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Varšavy	Varšava	k1gFnSc2	Varšava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zavalen	zavalit	k5eAaPmNgMnS	zavalit
prací	práce	k1gFnSc7	práce
a	a	k8xC	a
starostí	starost	k1gFnSc7	starost
o	o	k7c4	o
armádu	armáda	k1gFnSc4	armáda
prožil	prožít	k5eAaPmAgMnS	prožít
první	první	k4xOgFnSc4	první
fázi	fáze	k1gFnSc4	fáze
svého	svůj	k3xOyFgInSc2	svůj
milostného	milostný	k2eAgInSc2d1	milostný
vztahu	vztah	k1gInSc2	vztah
k	k	k7c3	k
polské	polský	k2eAgFnSc3d1	polská
vlastence	vlastenka	k1gFnSc3	vlastenka
<g/>
,	,	kIx,	,
hraběnce	hraběnka	k1gFnSc3	hraběnka
Marii	Maria	k1gFnSc3	Maria
Walewské	Walewská	k1gFnSc3	Walewská
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
vojenské	vojenský	k2eAgFnPc1d1	vojenská
operace	operace	k1gFnPc1	operace
však	však	k9	však
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
nedaly	dát	k5eNaPmAgFnP	dát
dlouho	dlouho	k6eAd1	dlouho
čekat	čekat	k5eAaImF	čekat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Rusové	Rus	k1gMnPc1	Rus
obnovili	obnovit	k5eAaPmAgMnP	obnovit
ofenzivu	ofenziva	k1gFnSc4	ofenziva
již	již	k6eAd1	již
na	na	k7c6	na
konci	konec	k1gInSc6	konec
ledna	leden	k1gInSc2	leden
1807	[number]	k4	1807
<g/>
.	.	kIx.	.
</s>
<s>
Manévry	manévr	k1gInPc1	manévr
této	tento	k3xDgFnSc2	tento
akce	akce	k1gFnSc2	akce
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
nerozhodnou	rozhodnout	k5eNaPmIp3nP	rozhodnout
bitvou	bitva	k1gFnSc7	bitva
u	u	k7c2	u
Jílového	Jílové	k1gNnSc2	Jílové
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
císař	císař	k1gMnSc1	císař
Francouzů	Francouz	k1gMnPc2	Francouz
jako	jako	k8xC	jako
velitel	velitel	k1gMnSc1	velitel
selhal	selhat	k5eAaPmAgMnS	selhat
<g/>
,	,	kIx,	,
především	především	k9	především
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
včas	včas	k6eAd1	včas
zkoncentrovat	zkoncentrovat	k5eAaPmF	zkoncentrovat
svá	svůj	k3xOyFgNnPc4	svůj
vojska	vojsko	k1gNnPc4	vojsko
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
jednotky	jednotka	k1gFnPc1	jednotka
do	do	k7c2	do
bitvy	bitva	k1gFnSc2	bitva
nezasáhly	zasáhnout	k5eNaPmAgInP	zasáhnout
včas	včas	k6eAd1	včas
nebo	nebo	k8xC	nebo
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
padlo	padnout	k5eAaImAgNnS	padnout
či	či	k8xC	či
bylo	být	k5eAaImAgNnS	být
raněno	ranit	k5eAaPmNgNnS	ranit
15	[number]	k4	15
000	[number]	k4	000
francouzských	francouzský	k2eAgMnPc2d1	francouzský
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
třetina	třetina	k1gFnSc1	třetina
mužů	muž	k1gMnPc2	muž
nasazených	nasazený	k2eAgMnPc2d1	nasazený
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
doznění	doznění	k1gNnSc6	doznění
bojů	boj	k1gInPc2	boj
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
opět	opět	k6eAd1	opět
stáhly	stáhnout	k5eAaPmAgFnP	stáhnout
do	do	k7c2	do
zimních	zimní	k2eAgInPc2d1	zimní
kvartýrů	kvartýrů	k?	kvartýrů
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
válečné	válečný	k2eAgFnPc1d1	válečná
operace	operace	k1gFnPc1	operace
polského	polský	k2eAgNnSc2d1	polské
tažení	tažení	k1gNnSc2	tažení
se	se	k3xPyFc4	se
odehrály	odehrát	k5eAaPmAgFnP	odehrát
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
jarních	jarní	k2eAgInPc6d1	jarní
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejich	jejich	k3xOp3gInSc6	jejich
průběhu	průběh	k1gInSc6	průběh
se	se	k3xPyFc4	se
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1807	[number]	k4	1807
podařilo	podařit	k5eAaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
důležitého	důležitý	k2eAgNnSc2d1	důležité
vítězství	vítězství	k1gNnSc2	vítězství
nad	nad	k7c7	nad
Ruskem	Rusko	k1gNnSc7	Rusko
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Friedlandu	Friedland	k1gInSc2	Friedland
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
o	o	k7c6	o
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
vstoupit	vstoupit	k5eAaPmF	vstoupit
do	do	k7c2	do
Tylže	Tylž	k1gFnSc2	Tylž
<g/>
.	.	kIx.	.
</s>
<s>
Vzápětí	vzápětí	k6eAd1	vzápětí
car	car	k1gMnSc1	car
Alexandr	Alexandr	k1gMnSc1	Alexandr
zažádal	zažádat	k5eAaPmAgMnS	zažádat
o	o	k7c4	o
mírové	mírový	k2eAgInPc4d1	mírový
rozhovory	rozhovor	k1gInPc4	rozhovor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
probíhaly	probíhat	k5eAaImAgInP	probíhat
na	na	k7c6	na
voru	vor	k1gInSc6	vor
ukotveném	ukotvený	k2eAgInSc6d1	ukotvený
uprostřed	uprostřed	k7c2	uprostřed
řeky	řeka	k1gFnSc2	řeka
Němen	Němna	k1gFnPc2	Němna
a	a	k8xC	a
vyvrcholily	vyvrcholit	k5eAaPmAgInP	vyvrcholit
ve	v	k7c6	v
dnech	den	k1gInPc6	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1807	[number]	k4	1807
podepsáním	podepsání	k1gNnSc7	podepsání
tzv.	tzv.	kA	tzv.
tylžského	tylžský	k2eAgInSc2d1	tylžský
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Válka	válka	k1gFnSc1	válka
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
===	===	k?	===
</s>
</p>
<p>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
pouhé	pouhý	k2eAgInPc4d1	pouhý
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
po	po	k7c6	po
ratifikaci	ratifikace	k1gFnSc6	ratifikace
mírových	mírový	k2eAgFnPc2d1	mírová
podmínek	podmínka	k1gFnPc2	podmínka
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
vládce	vládce	k1gMnSc1	vládce
obrovského	obrovský	k2eAgInSc2d1	obrovský
konglomerátu	konglomerát	k1gInSc2	konglomerát
rozkládajícího	rozkládající	k2eAgNnSc2d1	rozkládající
se	se	k3xPyFc4	se
od	od	k7c2	od
Královce	Královec	k1gInSc2	Královec
po	po	k7c4	po
Pyreneje	Pyreneje	k1gFnPc4	Pyreneje
<g/>
,	,	kIx,	,
od	od	k7c2	od
Varšavy	Varšava	k1gFnSc2	Varšava
až	až	k9	až
do	do	k7c2	do
Neapole	Neapol	k1gFnSc2	Neapol
<g/>
,	,	kIx,	,
od	od	k7c2	od
Antverp	Antverpy	k1gFnPc2	Antverpy
k	k	k7c3	k
horám	hora	k1gFnPc3	hora
severozápadního	severozápadní	k2eAgInSc2d1	severozápadní
Balkánu	Balkán	k1gInSc2	Balkán
<g/>
,	,	kIx,	,
od	od	k7c2	od
Hamburku	Hamburk	k1gInSc2	Hamburk
po	po	k7c4	po
ostrov	ostrov	k1gInSc4	ostrov
Korfu	Korfu	k1gNnSc2	Korfu
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vědomím	vědomí	k1gNnSc7	vědomí
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc7	jeho
jediným	jediný	k2eAgMnSc7d1	jediný
protivníkem	protivník	k1gMnSc7	protivník
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
pouze	pouze	k6eAd1	pouze
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
uskutečňování	uskutečňování	k1gNnSc2	uskutečňování
svého	svůj	k3xOyFgInSc2	svůj
nového	nový	k2eAgInSc2d1	nový
cíle	cíl	k1gInSc2	cíl
–	–	k?	–
ovládnutí	ovládnutí	k1gNnSc1	ovládnutí
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
každodenních	každodenní	k2eAgFnPc2d1	každodenní
opulentních	opulentní	k2eAgFnPc2d1	opulentní
hostin	hostina	k1gFnPc2	hostina
<g/>
,	,	kIx,	,
banketů	banket	k1gInPc2	banket
a	a	k8xC	a
plesů	ples	k1gInPc2	ples
začal	začít	k5eAaPmAgMnS	začít
formovat	formovat	k5eAaImF	formovat
novou	nový	k2eAgFnSc4d1	nová
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gNnSc7	jejíž
velením	velení	k1gNnSc7	velení
byl	být	k5eAaImAgInS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
Napoleonův	Napoleonův	k2eAgMnSc1d1	Napoleonův
přítel	přítel	k1gMnSc1	přítel
ještě	ještě	k9	ještě
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
obléhání	obléhání	k1gNnSc2	obléhání
Toulonu	Toulon	k1gInSc3	Toulon
Andoche	Andoche	k1gNnSc2	Andoche
Junot	Junota	k1gFnPc2	Junota
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
a	a	k8xC	a
po	po	k7c6	po
sérii	série	k1gFnSc6	série
rychlých	rychlý	k2eAgInPc2d1	rychlý
pochodů	pochod	k1gInPc2	pochod
obsadil	obsadit	k5eAaPmAgInS	obsadit
Lisabon	Lisabon	k1gInSc1	Lisabon
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nějž	jenž	k3xRgNnSc2	jenž
uprchla	uprchnout	k5eAaPmAgFnS	uprchnout
celá	celý	k2eAgFnSc1d1	celá
královská	královský	k2eAgFnSc1d1	královská
rodina	rodina	k1gFnSc1	rodina
do	do	k7c2	do
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
.	.	kIx.	.
<g/>
Dalším	další	k2eAgInSc7d1	další
cílem	cíl	k1gInSc7	cíl
Napoleonovy	Napoleonův	k2eAgFnSc2d1	Napoleonova
územní	územní	k2eAgFnSc2d1	územní
rozpínavosti	rozpínavost	k1gFnSc2	rozpínavost
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
spojenecké	spojenecký	k2eAgNnSc1d1	spojenecké
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc1	jeho
jižní	jižní	k2eAgMnSc1d1	jižní
soused	soused	k1gMnSc1	soused
připojil	připojit	k5eAaPmAgMnS	připojit
k	k	k7c3	k
dodržování	dodržování	k1gNnSc3	dodržování
berlínských	berlínský	k2eAgInPc2d1	berlínský
dekretů	dekret	k1gInPc2	dekret
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
francouzskému	francouzský	k2eAgMnSc3d1	francouzský
císaři	císař	k1gMnSc3	císař
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
obyvatelé	obyvatel	k1gMnPc1	obyvatel
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
závislí	závislý	k2eAgMnPc1d1	závislý
na	na	k7c6	na
anglickém	anglický	k2eAgInSc6d1	anglický
dovozu	dovoz	k1gInSc6	dovoz
i	i	k8xC	i
vývozu	vývoz	k1gInSc6	vývoz
a	a	k8xC	a
budou	být	k5eAaImBp3nP	být
se	se	k3xPyFc4	se
snažit	snažit	k5eAaImF	snažit
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
závazky	závazek	k1gInPc4	závazek
oklikami	oklika	k1gFnPc7	oklika
obcházet	obcházet	k5eAaImF	obcházet
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
Španělsko	Španělsko	k1gNnSc1	Španělsko
zcela	zcela	k6eAd1	zcela
ovládnout	ovládnout	k5eAaPmF	ovládnout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zemi	zem	k1gFnSc4	zem
nezbylo	zbýt	k5eNaPmAgNnS	zbýt
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
systému	systém	k1gInSc2	systém
kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
blokády	blokáda	k1gFnSc2	blokáda
naprosto	naprosto	k6eAd1	naprosto
podřídit	podřídit	k5eAaPmF	podřídit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
složité	složitý	k2eAgFnSc6d1	složitá
politické	politický	k2eAgFnSc6d1	politická
hře	hra	k1gFnSc6	hra
nakonec	nakonec	k6eAd1	nakonec
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
úspěchu	úspěch	k1gInSc3	úspěch
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1808	[number]	k4	1808
postranními	postranní	k2eAgFnPc7d1	postranní
intrikami	intrika	k1gFnPc7	intrika
přinutil	přinutit	k5eAaPmAgMnS	přinutit
španělského	španělský	k2eAgMnSc4d1	španělský
krále	král	k1gMnSc4	král
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
abdikovat	abdikovat	k5eAaBmF	abdikovat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
prince	princ	k1gMnSc2	princ
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
odstoupení	odstoupení	k1gNnSc6	odstoupení
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
odeslal	odeslat	k5eAaPmAgMnS	odeslat
stížnost	stížnost	k1gFnSc4	stížnost
na	na	k7c4	na
synovo	synův	k2eAgNnSc4d1	synovo
jednání	jednání	k1gNnSc4	jednání
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
jej	on	k3xPp3gMnSc4	on
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
své	svůj	k3xOyFgInPc4	svůj
nároky	nárok	k1gInPc4	nárok
přijel	přijet	k5eAaPmAgMnS	přijet
obhájit	obhájit	k5eAaPmF	obhájit
do	do	k7c2	do
Bayonne	Bayonn	k1gMnSc5	Bayonn
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
jihofrancouzského	jihofrancouzský	k2eAgNnSc2d1	jihofrancouzské
města	město	k1gNnSc2	město
sesazený	sesazený	k2eAgMnSc1d1	sesazený
král	král	k1gMnSc1	král
dorazil	dorazit	k5eAaPmAgMnS	dorazit
i	i	k9	i
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
chotí	choť	k1gFnSc7	choť
Marií	Maria	k1gFnPc2	Maria
<g/>
,	,	kIx,	,
namísto	namísto	k7c2	namísto
slibovaného	slibovaný	k2eAgNnSc2d1	slibované
slyšení	slyšení	k1gNnSc2	slyšení
se	se	k3xPyFc4	se
však	však	k9	však
oba	dva	k4xCgMnPc1	dva
manželé	manžel	k1gMnPc1	manžel
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
zajetí	zajetí	k1gNnSc2	zajetí
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
machinacemi	machinace	k1gFnPc7	machinace
Napoleon	napoleon	k1gInSc4	napoleon
posiloval	posilovat	k5eAaImAgMnS	posilovat
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
přítomnost	přítomnost	k1gFnSc4	přítomnost
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
a	a	k8xC	a
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1808	[number]	k4	1808
francouzské	francouzský	k2eAgFnSc2d1	francouzská
ozbrojené	ozbrojený	k2eAgFnSc2d1	ozbrojená
síly	síla	k1gFnSc2	síla
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
čítaly	čítat	k5eAaImAgInP	čítat
na	na	k7c6	na
80	[number]	k4	80
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc7	jejich
velením	velení	k1gNnSc7	velení
byl	být	k5eAaImAgInS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
císařův	císařův	k2eAgMnSc1d1	císařův
švagr	švagr	k1gMnSc1	švagr
<g/>
,	,	kIx,	,
maršál	maršál	k1gMnSc1	maršál
Joachim	Joachim	k1gMnSc1	Joachim
Murat	Murat	k2eAgMnSc1d1	Murat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nedlouho	nedlouho	k1gNnSc1	nedlouho
po	po	k7c6	po
svých	svůj	k3xOyFgMnPc6	svůj
rodičích	rodič	k1gMnPc6	rodič
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
Bayonne	Bayonn	k1gInSc5	Bayonn
vylákán	vylákat	k5eAaPmNgInS	vylákat
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
uvězněn	uvěznit	k5eAaPmNgMnS	uvěznit
i	i	k9	i
král	král	k1gMnSc1	král
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
<g/>
,	,	kIx,	,
na	na	k7c4	na
jehož	jenž	k3xRgNnSc4	jenž
místo	místo	k1gNnSc4	místo
Napoleon	napoleon	k1gInSc4	napoleon
neprodleně	prodleně	k6eNd1	prodleně
dosadil	dosadit	k5eAaPmAgMnS	dosadit
svého	svůj	k3xOyFgMnSc4	svůj
bratra	bratr	k1gMnSc4	bratr
Josefa	Josef	k1gMnSc4	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Španělska	Španělsko	k1gNnSc2	Španělsko
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
dozvěděli	dozvědět	k5eAaPmAgMnP	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
má	mít	k5eAaImIp3nS	mít
odebrat	odebrat	k5eAaPmF	odebrat
i	i	k9	i
nejmladší	mladý	k2eAgMnPc1d3	nejmladší
syn	syn	k1gMnSc1	syn
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
don	don	k1gMnSc1	don
Francisco	Francisco	k1gMnSc1	Francisco
de	de	k?	de
Paolo	Paolo	k1gNnSc4	Paolo
<g/>
,	,	kIx,	,
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
sice	sice	k8xC	sice
Joachim	Joachim	k1gMnSc1	Joachim
Murat	Murat	k1gInSc4	Murat
potlačil	potlačit	k5eAaPmAgMnS	potlačit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
několika	několik	k4yIc2	několik
dnů	den	k1gInPc2	den
vzpoura	vzpoura	k1gFnSc1	vzpoura
zachvátila	zachvátit	k5eAaPmAgFnS	zachvátit
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgNnSc4d1	celé
území	území	k1gNnSc4	území
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Protifrancouzské	protifrancouzský	k2eAgFnPc1d1	protifrancouzská
akce	akce	k1gFnPc1	akce
nakonec	nakonec	k6eAd1	nakonec
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
takových	takový	k3xDgInPc2	takový
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
vyžádaly	vyžádat	k5eAaPmAgFnP	vyžádat
pozornost	pozornost	k1gFnSc4	pozornost
samotného	samotný	k2eAgMnSc4d1	samotný
Napoleona	Napoleon	k1gMnSc4	Napoleon
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
první	první	k4xOgFnPc1	první
operace	operace	k1gFnPc1	operace
proti	proti	k7c3	proti
juntám	junta	k1gFnPc3	junta
(	(	kIx(	(
<g/>
provinčním	provinční	k2eAgFnPc3d1	provinční
vládám	vláda	k1gFnPc3	vláda
<g/>
)	)	kIx)	)
řídil	řídit	k5eAaImAgInS	řídit
z	z	k7c2	z
Bayonne	Bayonn	k1gInSc5	Bayonn
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
22	[number]	k4	22
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
byl	být	k5eAaImAgMnS	být
kvůli	kvůli	k7c3	kvůli
císařovu	císařův	k2eAgInSc3d1	císařův
chybnému	chybný	k2eAgInSc3d1	chybný
rozkazu	rozkaz	k1gInSc3	rozkaz
donucen	donucen	k2eAgInSc1d1	donucen
kapitulovat	kapitulovat	k5eAaBmF	kapitulovat
sbor	sbor	k1gInSc4	sbor
generála	generál	k1gMnSc2	generál
Duponta	Dupont	k1gMnSc2	Dupont
v	v	k7c6	v
Bailénu	Bailén	k1gInSc6	Bailén
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
velmi	velmi	k6eAd1	velmi
oživilo	oživit	k5eAaPmAgNnS	oživit
španělský	španělský	k2eAgInSc4d1	španělský
odpor	odpor	k1gInSc4	odpor
a	a	k8xC	a
oslabilo	oslabit	k5eAaPmAgNnS	oslabit
morálku	morálka	k1gFnSc4	morálka
Francouzů	Francouz	k1gMnPc2	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
Andaluské	andaluský	k2eAgFnSc3d1	andaluská
armádě	armáda	k1gFnSc3	armáda
se	se	k3xPyFc4	se
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tohoto	tento	k3xDgInSc2	tento
úspěchu	úspěch	k1gInSc2	úspěch
otevřela	otevřít	k5eAaPmAgFnS	otevřít
cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
Madrid	Madrid	k1gInSc4	Madrid
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
posádkou	posádka	k1gFnSc7	posádka
po	po	k7c6	po
desetidenním	desetidenní	k2eAgInSc6d1	desetidenní
pobytu	pobyt	k1gInSc6	pobyt
narychlo	narychlo	k6eAd1	narychlo
opustil	opustit	k5eAaPmAgMnS	opustit
i	i	k9	i
král	král	k1gMnSc1	král
Josef	Josef	k1gMnSc1	Josef
Bonaparte	bonapart	k1gInSc5	bonapart
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
debakl	debakl	k1gInSc1	debakl
přišel	přijít	k5eAaPmAgInS	přijít
ve	v	k7c4	v
velmi	velmi	k6eAd1	velmi
nevhodnou	vhodný	k2eNgFnSc4d1	nevhodná
chvíli	chvíle	k1gFnSc4	chvíle
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
k	k	k7c3	k
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
již	již	k6eAd1	již
dorazily	dorazit	k5eAaPmAgInP	dorazit
první	první	k4xOgFnPc4	první
zprávy	zpráva	k1gFnPc4	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rakousko	Rakousko	k1gNnSc1	Rakousko
nejspíš	nejspíš	k9	nejspíš
zbrojí	zbrojit	k5eAaImIp3nS	zbrojit
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
válce	válka	k1gFnSc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
junt	junta	k1gFnPc2	junta
k	k	k7c3	k
vylodění	vylodění	k1gNnSc3	vylodění
anglické	anglický	k2eAgFnSc2d1	anglická
pozorovací	pozorovací	k2eAgFnSc2d1	pozorovací
armády	armáda	k1gFnSc2	armáda
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
sira	sir	k1gMnSc2	sir
Arthura	Arthur	k1gMnSc2	Arthur
Wellesleyho	Wellesley	k1gMnSc2	Wellesley
v	v	k7c6	v
zálivu	záliv	k1gInSc6	záliv
Mondego	Mondego	k1gNnSc1	Mondego
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgInPc6d1	následující
dnech	den	k1gInPc6	den
britský	britský	k2eAgMnSc1d1	britský
velitel	velitel	k1gMnSc1	velitel
porazil	porazit	k5eAaPmAgMnS	porazit
Junotův	Junotův	k2eAgInSc4d1	Junotův
sbor	sbor	k1gInSc4	sbor
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Vimeira	Vimeiro	k1gNnSc2	Vimeiro
(	(	kIx(	(
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
Napoleona	Napoleon	k1gMnSc4	Napoleon
připravil	připravit	k5eAaPmAgMnS	připravit
o	o	k7c4	o
celé	celý	k2eAgNnSc4d1	celé
Portugalsko	Portugalsko	k1gNnSc4	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
okolnosti	okolnost	k1gFnPc1	okolnost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
hrozba	hrozba	k1gFnSc1	hrozba
boje	boj	k1gInSc2	boj
na	na	k7c6	na
dvou	dva	k4xCgFnPc6	dva
frontách	fronta	k1gFnPc6	fronta
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
přinutily	přinutit	k5eAaPmAgFnP	přinutit
francouzského	francouzský	k2eAgMnSc2d1	francouzský
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
operací	operace	k1gFnSc7	operace
ve	v	k7c6	v
vzbouřené	vzbouřený	k2eAgFnSc6d1	vzbouřená
zemi	zem	k1gFnSc6	zem
ujal	ujmout	k5eAaPmAgInS	ujmout
osobně	osobně	k6eAd1	osobně
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
však	však	k9	však
pokusil	pokusit	k5eAaPmAgMnS	pokusit
upevnit	upevnit	k5eAaPmF	upevnit
spojenectví	spojenectví	k1gNnSc4	spojenectví
s	s	k7c7	s
carem	car	k1gMnSc7	car
Alexandrem	Alexandr	k1gMnSc7	Alexandr
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
setkání	setkání	k1gNnSc3	setkání
nazvanému	nazvaný	k2eAgInSc3d1	nazvaný
erfurtská	erfurtský	k2eAgFnSc1d1	Erfurtská
konference	konference	k1gFnSc2	konference
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
září	září	k1gNnSc6	září
1808	[number]	k4	1808
a	a	k8xC	a
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
od	od	k7c2	od
ruského	ruský	k2eAgMnSc2d1	ruský
panovníka	panovník	k1gMnSc2	panovník
příslibu	příslib	k1gInSc2	příslib
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
země	země	k1gFnSc1	země
zůstane	zůstat	k5eAaPmIp3nS	zůstat
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
Rakousko	Rakousko	k1gNnSc1	Rakousko
stane	stanout	k5eAaPmIp3nS	stanout
agresorem	agresor	k1gMnSc7	agresor
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
vydal	vydat	k5eAaPmAgMnS	vydat
Napoleon	napoleon	k1gInSc4	napoleon
instrukce	instrukce	k1gFnSc2	instrukce
k	k	k7c3	k
rozdělení	rozdělení	k1gNnSc3	rozdělení
Velké	velký	k2eAgFnSc2d1	velká
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
ke	k	k7c3	k
svým	svůj	k3xOyFgMnPc3	svůj
mužům	muž	k1gMnPc3	muž
ve	v	k7c6	v
Vitórii	Vitórie	k1gFnSc6	Vitórie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
necelý	celý	k2eNgInSc4d1	necelý
měsíc	měsíc	k1gInSc4	měsíc
trvajícím	trvající	k2eAgNnSc6d1	trvající
tažení	tažení	k1gNnSc6	tažení
pak	pak	k6eAd1	pak
4	[number]	k4	4
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
opět	opět	k6eAd1	opět
obsadil	obsadit	k5eAaPmAgInS	obsadit
Madrid	Madrid	k1gInSc1	Madrid
a	a	k8xC	a
nastěhoval	nastěhovat	k5eAaPmAgInS	nastěhovat
se	se	k3xPyFc4	se
do	do	k7c2	do
paláce	palác	k1gInSc2	palác
El	Ela	k1gFnPc2	Ela
Escorial	Escorial	k1gMnSc1	Escorial
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
vydal	vydat	k5eAaPmAgInS	vydat
čtyři	čtyři	k4xCgInPc4	čtyři
dekrety	dekret	k1gInPc4	dekret
<g/>
,	,	kIx,	,
kterými	který	k3yIgInPc7	který
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
zrušil	zrušit	k5eAaPmAgMnS	zrušit
veškerá	veškerý	k3xTgNnPc4	veškerý
feudální	feudální	k2eAgNnPc4d1	feudální
práva	právo	k1gNnPc4	právo
i	i	k8xC	i
vrchnostenské	vrchnostenský	k2eAgInPc4d1	vrchnostenský
soudy	soud	k1gInPc4	soud
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
klášterů	klášter	k1gInPc2	klášter
<g/>
,	,	kIx,	,
celou	celý	k2eAgFnSc4d1	celá
Svatou	svatý	k2eAgFnSc4d1	svatá
inkvizici	inkvizice	k1gFnSc4	inkvizice
<g/>
,	,	kIx,	,
a	a	k8xC	a
odstranil	odstranit	k5eAaPmAgMnS	odstranit
celní	celní	k2eAgFnPc4d1	celní
bariéry	bariéra	k1gFnPc4	bariéra
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
španělskými	španělský	k2eAgFnPc7d1	španělská
provinciemi	provincie	k1gFnPc7	provincie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
dnech	den	k1gInPc6	den
pak	pak	k6eAd1	pak
Napoleon	napoleon	k1gInSc4	napoleon
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
vojáky	voják	k1gMnPc7	voják
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
hlouběji	hluboko	k6eAd2	hluboko
do	do	k7c2	do
španělského	španělský	k2eAgNnSc2d1	španělské
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
odříznout	odříznout	k5eAaPmF	odříznout
jednotky	jednotka	k1gFnPc1	jednotka
britského	britský	k2eAgMnSc2d1	britský
generála	generál	k1gMnSc2	generál
Moora	Moor	k1gMnSc2	Moor
(	(	kIx(	(
<g/>
jenž	jenž	k3xRgMnSc1	jenž
nahradil	nahradit	k5eAaPmAgMnS	nahradit
Wellingtona	Wellington	k1gMnSc4	Wellington
<g/>
)	)	kIx)	)
od	od	k7c2	od
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
válečnou	válečný	k2eAgFnSc4d1	válečná
operaci	operace	k1gFnSc4	operace
však	však	k9	však
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
naléhavé	naléhavý	k2eAgFnPc4d1	naléhavá
zprávy	zpráva	k1gFnPc4	zpráva
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
Paříže	Paříž	k1gFnSc2	Paříž
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
dnech	den	k1gInPc6	den
roku	rok	k1gInSc2	rok
1809	[number]	k4	1809
donutily	donutit	k5eAaPmAgFnP	donutit
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
války	válka	k1gFnSc2	válka
páté	pátý	k4xOgFnSc2	pátý
koalice	koalice	k1gFnSc2	koalice
===	===	k?	===
</s>
</p>
<p>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
zpráv	zpráva	k1gFnPc2	zpráva
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
níž	jenž	k3xRgFnSc3	jenž
Napoleon	napoleon	k1gInSc1	napoleon
opustil	opustit	k5eAaPmAgInS	opustit
nedokončené	dokončený	k2eNgNnSc4d1	nedokončené
tažení	tažení	k1gNnSc4	tažení
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
očekávaná	očekávaný	k2eAgFnSc1d1	očekávaná
informace	informace	k1gFnSc1	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
spojeneckého	spojenecký	k2eAgNnSc2d1	spojenecké
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
se	se	k3xPyFc4	se
shromažďují	shromažďovat	k5eAaImIp3nP	shromažďovat
mohutné	mohutný	k2eAgFnSc2d1	mohutná
síly	síla	k1gFnSc2	síla
Rakouského	rakouský	k2eAgNnSc2d1	rakouské
císařství	císařství	k1gNnSc2	císařství
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
znepokojující	znepokojující	k2eAgFnSc7d1	znepokojující
zvěstí	zvěst	k1gFnSc7	zvěst
byl	být	k5eAaImAgInS	být
podezřele	podezřele	k6eAd1	podezřele
těsný	těsný	k2eAgInSc1d1	těsný
svazek	svazek	k1gInSc1	svazek
jindy	jindy	k6eAd1	jindy
vzájemně	vzájemně	k6eAd1	vzájemně
nepřátelsky	přátelsky	k6eNd1	přátelsky
naladěných	naladěný	k2eAgMnPc2d1	naladěný
ministrů	ministr	k1gMnPc2	ministr
Talleyranda	Talleyrando	k1gNnSc2	Talleyrando
a	a	k8xC	a
Fouchého	Fouchý	k2eAgInSc2d1	Fouchý
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
Napoleon	napoleon	k1gInSc1	napoleon
dorazil	dorazit	k5eAaPmAgInS	dorazit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
a	a	k8xC	a
o	o	k7c4	o
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
k	k	k7c3	k
sobě	se	k3xPyFc3	se
do	do	k7c2	do
pracovny	pracovna	k1gFnSc2	pracovna
pozval	pozvat	k5eAaPmAgMnS	pozvat
nejvyšší	vysoký	k2eAgMnPc4d3	nejvyšší
hodnostáře	hodnostář	k1gMnPc4	hodnostář
císařství	císařství	k1gNnSc2	císařství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
přítomnosti	přítomnost	k1gFnSc6	přítomnost
vyčetl	vyčíst	k5eAaPmAgMnS	vyčíst
Talleyrandovi	Talleyrandův	k2eAgMnPc1d1	Talleyrandův
veškeré	veškerý	k3xTgFnPc4	veškerý
intrikování	intrikování	k1gNnSc2	intrikování
proti	proti	k7c3	proti
své	svůj	k3xOyFgFnSc3	svůj
osobě	osoba	k1gFnSc3	osoba
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
mu	on	k3xPp3gMnSc3	on
přestal	přestat	k5eAaPmAgMnS	přestat
věnovat	věnovat	k5eAaPmF	věnovat
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
nasazením	nasazení	k1gNnSc7	nasazení
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
pustil	pustit	k5eAaPmAgMnS	pustit
do	do	k7c2	do
příprav	příprava	k1gFnPc2	příprava
války	válka	k1gFnSc2	válka
v	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Nezbývalo	zbývat	k5eNaImAgNnS	zbývat
mu	on	k3xPp3gMnSc3	on
mnoho	mnoho	k4c1	mnoho
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
9	[number]	k4	9
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
překročily	překročit	k5eAaPmAgFnP	překročit
rakouské	rakouský	k2eAgFnPc1d1	rakouská
jednotky	jednotka	k1gFnPc1	jednotka
řeku	řeka	k1gFnSc4	řeka
Inn	Inn	k1gFnPc1	Inn
a	a	k8xC	a
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
do	do	k7c2	do
Mnichova	Mnichov	k1gInSc2	Mnichov
<g/>
.	.	kIx.	.
</s>
<s>
Tentokrát	tentokrát	k6eAd1	tentokrát
se	se	k3xPyFc4	se
Napoleon	napoleon	k1gInSc1	napoleon
nepokoušel	pokoušet	k5eNaImAgInS	pokoušet
nepřítele	nepřítel	k1gMnSc4	nepřítel
předejít	předejít	k5eAaPmF	předejít
obvyklou	obvyklý	k2eAgFnSc7d1	obvyklá
útočnou	útočný	k2eAgFnSc7d1	útočná
strategií	strategie	k1gFnSc7	strategie
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
si	se	k3xPyFc3	se
z	z	k7c2	z
politického	politický	k2eAgNnSc2d1	politické
hlediska	hledisko	k1gNnSc2	hledisko
přál	přát	k5eAaImAgMnS	přát
vyhovět	vyhovět	k5eAaPmF	vyhovět
podmínkám	podmínka	k1gFnPc3	podmínka
erfurtské	erfurtský	k2eAgFnSc2d1	Erfurtská
dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
války	válka	k1gFnSc2	válka
však	však	k9	však
neprodleně	prodleně	k6eNd1	prodleně
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
do	do	k7c2	do
Ingolstadtu	Ingolstadt	k1gInSc2	Ingolstadt
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
osobně	osobně	k6eAd1	osobně
převzal	převzít	k5eAaPmAgInS	převzít
velení	velení	k1gNnSc4	velení
armády	armáda	k1gFnSc2	armáda
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
vydal	vydat	k5eAaPmAgInS	vydat
rozkaz	rozkaz	k1gInSc1	rozkaz
ke	k	k7c3	k
shromáždění	shromáždění	k1gNnSc3	shromáždění
roztroušených	roztroušený	k2eAgInPc2d1	roztroušený
sborů	sbor	k1gInPc2	sbor
a	a	k8xC	a
zdařilým	zdařilý	k2eAgInSc7d1	zdařilý
tahem	tah	k1gInSc7	tah
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
řezenskou	řezenský	k2eAgFnSc7d1	řezenská
operací	operace	k1gFnSc7	operace
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
reprezentovanou	reprezentovaný	k2eAgFnSc7d1	reprezentovaná
bitvou	bitva	k1gFnSc7	bitva
u	u	k7c2	u
Teung	Teunga	k1gFnPc2	Teunga
<g/>
–	–	k?	–
<g/>
Hausenu	Hausen	k2eAgFnSc4d1	Hausena
<g/>
,	,	kIx,	,
bitvou	bitva	k1gFnSc7	bitva
u	u	k7c2	u
Abensbergu	Abensberg	k1gInSc2	Abensberg
a	a	k8xC	a
bitvou	bitva	k1gFnSc7	bitva
u	u	k7c2	u
Landshutu	Landshut	k1gInSc2	Landshut
<g/>
,	,	kIx,	,
přinutil	přinutit	k5eAaPmAgInS	přinutit
rakouské	rakouský	k2eAgNnSc4d1	rakouské
vojsko	vojsko	k1gNnSc4	vojsko
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
nejzkušenějšího	zkušený	k2eAgMnSc2d3	nejzkušenější
císařského	císařský	k2eAgMnSc2d1	císařský
velitele	velitel	k1gMnSc2	velitel
arcivévody	arcivévoda	k1gMnSc2	arcivévoda
Karla	Karel	k1gMnSc2	Karel
k	k	k7c3	k
rozhodující	rozhodující	k2eAgFnSc3d1	rozhodující
bitvě	bitva	k1gFnSc3	bitva
u	u	k7c2	u
Eggmühlu	Eggmühl	k1gInSc2	Eggmühl
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
střetnutí	střetnutí	k1gNnSc6	střetnutí
Napoleon	Napoleon	k1gMnSc1	Napoleon
vedl	vést	k5eAaImAgMnS	vést
svou	svůj	k3xOyFgFnSc4	svůj
armádu	armáda	k1gFnSc4	armáda
proti	proti	k7c3	proti
Vídni	Vídeň	k1gFnSc3	Vídeň
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
zmocnil	zmocnit	k5eAaPmAgMnS	zmocnit
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dobytí	dobytí	k1gNnSc1	dobytí
rakouské	rakouský	k2eAgFnSc2d1	rakouská
metropole	metropol	k1gFnSc2	metropol
představovalo	představovat	k5eAaImAgNnS	představovat
jen	jen	k9	jen
dílčí	dílčí	k2eAgNnSc1d1	dílčí
a	a	k8xC	a
spíš	spíš	k9	spíš
prestižní	prestižní	k2eAgInSc4d1	prestižní
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
francouzskému	francouzský	k2eAgNnSc3d1	francouzské
vojsku	vojsko	k1gNnSc3	vojsko
ze	z	k7c2	z
strategického	strategický	k2eAgNnSc2d1	strategické
hlediska	hledisko	k1gNnSc2	hledisko
přinesl	přinést	k5eAaPmAgInS	přinést
pouze	pouze	k6eAd1	pouze
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
pravé	pravý	k2eAgFnSc2d1	pravá
strany	strana	k1gFnSc2	strana
Dunaje	Dunaj	k1gInSc2	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
dál	daleko	k6eAd2	daleko
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
stopách	stopa	k1gFnPc6	stopa
arcivévody	arcivévoda	k1gMnSc2	arcivévoda
Karla	Karel	k1gMnSc2	Karel
<g/>
,	,	kIx,	,
chyběly	chybět	k5eAaImAgFnP	chybět
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nepřítel	nepřítel	k1gMnSc1	nepřítel
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
,	,	kIx,	,
nehledě	hledět	k5eNaImSgMnS	hledět
na	na	k7c4	na
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rakušané	Rakušan	k1gMnPc1	Rakušan
strhli	strhnout	k5eAaPmAgMnP	strhnout
všechny	všechen	k3xTgInPc4	všechen
mosty	most	k1gInPc4	most
přes	přes	k7c4	přes
Dunaj	Dunaj	k1gInSc4	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
postavit	postavit	k5eAaPmF	postavit
dva	dva	k4xCgInPc1	dva
nové	nový	k2eAgInPc1d1	nový
přechody	přechod	k1gInPc1	přechod
přes	přes	k7c4	přes
rozvodněné	rozvodněný	k2eAgNnSc4d1	rozvodněné
říční	říční	k2eAgNnSc4d1	říční
koryto	koryto	k1gNnSc4	koryto
nad	nad	k7c7	nad
i	i	k8xC	i
pod	pod	k7c7	pod
Vídní	Vídeň	k1gFnSc7	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
zarytému	zarytý	k2eAgInSc3d1	zarytý
rakouskému	rakouský	k2eAgInSc3d1	rakouský
odporu	odpor	k1gInSc3	odpor
však	však	k9	však
vystavěl	vystavět	k5eAaPmAgMnS	vystavět
most	most	k1gInSc4	most
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
u	u	k7c2	u
vesničky	vesnička	k1gFnSc2	vesnička
Aspern	Aspern	k1gInSc1	Aspern
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
asi	asi	k9	asi
10	[number]	k4	10
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
okupovaného	okupovaný	k2eAgNnSc2d1	okupované
velkoměsta	velkoměsto	k1gNnSc2	velkoměsto
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
přepravil	přepravit	k5eAaPmAgMnS	přepravit
svou	svůj	k3xOyFgFnSc4	svůj
armádu	armáda	k1gFnSc4	armáda
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
Lobau	Lobaus	k1gInSc2	Lobaus
na	na	k7c4	na
levý	levý	k2eAgInSc4d1	levý
břeh	břeh	k1gInSc4	břeh
Dunaje	Dunaj	k1gInSc2	Dunaj
a	a	k8xC	a
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
na	na	k7c4	na
arcivévodovu	arcivévodův	k2eAgFnSc4d1	arcivévodova
armádu	armáda	k1gFnSc4	armáda
u	u	k7c2	u
vesnic	vesnice	k1gFnPc2	vesnice
Aspern	Aspern	k1gInSc1	Aspern
a	a	k8xC	a
Essling	Essling	k1gInSc1	Essling
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
první	první	k4xOgFnSc4	první
porážku	porážka	k1gFnSc4	porážka
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
dosavadní	dosavadní	k2eAgFnSc6d1	dosavadní
vojenské	vojenský	k2eAgFnSc6d1	vojenská
kariéře	kariéra	k1gFnSc6	kariéra
(	(	kIx(	(
<g/>
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Aspern	Asperna	k1gFnPc2	Asperna
a	a	k8xC	a
Esslingu	Essling	k1gInSc2	Essling
<g/>
)	)	kIx)	)
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
opět	opět	k5eAaPmF	opět
se	se	k3xPyFc4	se
stáhnout	stáhnout	k5eAaPmF	stáhnout
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Lobau	Lobaus	k1gInSc2	Lobaus
<g/>
.	.	kIx.	.
</s>
<s>
Veden	veden	k2eAgInSc1d1	veden
politickými	politický	k2eAgInPc7d1	politický
důvody	důvod	k1gInPc7	důvod
chápal	chápat	k5eAaImAgMnS	chápat
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
místo	místo	k1gNnSc1	místo
nesmí	smět	k5eNaImIp3nS	smět
opustit	opustit	k5eAaPmF	opustit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
veřejné	veřejný	k2eAgNnSc1d1	veřejné
mínění	mínění	k1gNnSc1	mínění
by	by	kYmCp3nS	by
jeho	on	k3xPp3gInSc4	on
ústup	ústup	k1gInSc4	ústup
vnímalo	vnímat	k5eAaImAgNnS	vnímat
jako	jako	k9	jako
další	další	k2eAgNnSc1d1	další
velké	velký	k2eAgNnSc1d1	velké
rakouské	rakouský	k2eAgNnSc1d1	rakouské
vítězství	vítězství	k1gNnSc1	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vědomím	vědomí	k1gNnSc7	vědomí
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
prestižních	prestižní	k2eAgInPc2d1	prestižní
důvodů	důvod	k1gInPc2	důvod
musí	muset	k5eAaImIp3nS	muset
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
rozhodujícího	rozhodující	k2eAgInSc2d1	rozhodující
obratu	obrat	k1gInSc2	obrat
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
dalších	další	k2eAgInPc2d1	další
šest	šest	k4xCc4	šest
týdnů	týden	k1gInPc2	týden
s	s	k7c7	s
pedantskou	pedantský	k2eAgFnSc7d1	pedantská
pečlivostí	pečlivost	k1gFnSc7	pečlivost
připravoval	připravovat	k5eAaImAgInS	připravovat
k	k	k7c3	k
druhému	druhý	k4xOgInSc3	druhý
pokusu	pokus	k1gInSc3	pokus
o	o	k7c6	o
překročení	překročení	k1gNnSc6	překročení
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
francouzský	francouzský	k2eAgMnSc1d1	francouzský
císař	císař	k1gMnSc1	císař
převedl	převést	k5eAaPmAgMnS	převést
svých	svůj	k3xOyFgInPc6	svůj
134	[number]	k4	134
000	[number]	k4	000
pěšáků	pěšák	k1gMnPc2	pěšák
<g/>
,	,	kIx,	,
27	[number]	k4	27
000	[number]	k4	000
jezdců	jezdec	k1gMnPc2	jezdec
a	a	k8xC	a
433	[number]	k4	433
děl	dělo	k1gNnPc2	dělo
opět	opět	k6eAd1	opět
přes	přes	k7c4	přes
Dunaj	Dunaj	k1gInSc4	Dunaj
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
vyvinout	vyvinout	k5eAaPmF	vyvinout
nápor	nápor	k1gInSc4	nápor
do	do	k7c2	do
levého	levý	k2eAgInSc2d1	levý
boku	bok	k1gInSc2	bok
nepřátelského	přátelský	k2eNgNnSc2d1	nepřátelské
postavení	postavení	k1gNnSc2	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dvoudenním	dvoudenní	k2eAgNnSc6d1	dvoudenní
zápolení	zápolení	k1gNnSc6	zápolení
jeho	jeho	k3xOp3gMnSc3	jeho
muži	muž	k1gMnPc1	muž
obrátili	obrátit	k5eAaPmAgMnP	obrátit
bitvu	bitva	k1gFnSc4	bitva
u	u	k7c2	u
Wagramu	Wagram	k1gInSc2	Wagram
ve	v	k7c4	v
svůj	svůj	k3xOyFgInSc4	svůj
prospěch	prospěch	k1gInSc4	prospěch
<g/>
.	.	kIx.	.
</s>
<s>
Střetnutí	střetnutí	k1gNnSc1	střetnutí
samotné	samotný	k2eAgNnSc1d1	samotné
znamenalo	znamenat	k5eAaImAgNnS	znamenat
pro	pro	k7c4	pro
Napoleona	Napoleon	k1gMnSc4	Napoleon
obrovský	obrovský	k2eAgInSc1d1	obrovský
triumf	triumf	k1gInSc1	triumf
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
Evropu	Evropa	k1gFnSc4	Evropa
návrat	návrat	k1gInSc4	návrat
pod	pod	k7c4	pod
jeho	jeho	k3xOp3gNnSc4	jeho
žezlo	žezlo	k1gNnSc4	žezlo
<g/>
.	.	kIx.	.
</s>
<s>
Okázale	okázale	k6eAd1	okázale
pak	pak	k6eAd1	pak
slavil	slavit	k5eAaImAgInS	slavit
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
,	,	kIx,	,
tažení	tažení	k1gNnSc4	tažení
však	však	k9	však
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
neskončilo	skončit	k5eNaPmAgNnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
vyrazily	vyrazit	k5eAaPmAgInP	vyrazit
francouzské	francouzský	k2eAgInPc1d1	francouzský
sbory	sbor	k1gInPc1	sbor
dál	daleko	k6eAd2	daleko
za	za	k7c7	za
ustupujícími	ustupující	k2eAgMnPc7d1	ustupující
Rakušany	Rakušan	k1gMnPc7	Rakušan
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dalších	další	k2eAgInPc2d1	další
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Znojma	Znojmo	k1gNnSc2	Znojmo
<g/>
,	,	kIx,	,
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
Karel	Karel	k1gMnSc1	Karel
uzavřít	uzavřít	k5eAaPmF	uzavřít
příměří	příměří	k1gNnSc4	příměří
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
Napoleon	Napoleon	k1gMnSc1	Napoleon
přijal	přijmout	k5eAaPmAgMnS	přijmout
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
bylo	být	k5eAaImAgNnS	být
stvrzeno	stvrzen	k2eAgNnSc1d1	stvrzeno
podepsáním	podepsání	k1gNnSc7	podepsání
schönnbrunnské	schönnbrunnský	k2eAgFnSc2d1	schönnbrunnský
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
Rakousku	Rakousko	k1gNnSc3	Rakousko
přinesla	přinést	k5eAaPmAgFnS	přinést
další	další	k2eAgFnPc4d1	další
územní	územní	k2eAgFnPc4d1	územní
ztráty	ztráta	k1gFnPc4	ztráta
a	a	k8xC	a
nutnost	nutnost	k1gFnSc4	nutnost
Francii	Francie	k1gFnSc4	Francie
vyrovnat	vyrovnat	k5eAaPmF	vyrovnat
válečné	válečný	k2eAgFnPc4d1	válečná
náhrady	náhrada	k1gFnPc4	náhrada
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
85	[number]	k4	85
milionů	milion	k4xCgInPc2	milion
franků	frank	k1gInPc2	frank
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
jednání	jednání	k1gNnSc2	jednání
v	v	k7c6	v
Schönbrunnu	Schönbrunn	k1gInSc6	Schönbrunn
dorazila	dorazit	k5eAaPmAgFnS	dorazit
za	za	k7c7	za
Napoleonem	napoleon	k1gInSc7	napoleon
hraběnka	hraběnka	k1gFnSc1	hraběnka
Walewská	Walewská	k1gFnSc1	Walewská
a	a	k8xC	a
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
mu	on	k3xPp3gMnSc3	on
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
čeká	čekat	k5eAaImIp3nS	čekat
jeho	jeho	k3xOp3gNnSc4	jeho
dítě	dítě	k1gNnSc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
novině	novina	k1gFnSc6	novina
se	se	k3xPyFc4	se
francouzský	francouzský	k2eAgMnSc1d1	francouzský
císař	císař	k1gMnSc1	císař
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rozvede	rozvést	k5eAaPmIp3nS	rozvést
s	s	k7c7	s
Josefínou	Josefína	k1gFnSc7	Josefína
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
úmyslem	úmysl	k1gInSc7	úmysl
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
pokračovat	pokračovat	k5eAaImF	pokračovat
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
s	s	k7c7	s
polskou	polský	k2eAgFnSc7d1	polská
šlechtičnou	šlechtična	k1gFnSc7	šlechtična
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1810	[number]	k4	1810
porodila	porodit	k5eAaPmAgFnS	porodit
syna	syn	k1gMnSc4	syn
jménem	jméno	k1gNnSc7	jméno
Alexandre	Alexandr	k1gInSc5	Alexandr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc1	jeho
zrak	zrak	k1gInSc1	zrak
se	se	k3xPyFc4	se
upnul	upnout	k5eAaPmAgInS	upnout
k	k	k7c3	k
nevěstě	nevěsta	k1gFnSc3	nevěsta
z	z	k7c2	z
urozenějšího	urozený	k2eAgInSc2d2	urozený
a	a	k8xC	a
dynasticky	dynasticky	k6eAd1	dynasticky
silnějšího	silný	k2eAgInSc2d2	silnější
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
stát	stát	k5eAaImF	stát
Marie	Marie	k1gFnSc1	Marie
Luisa	Luisa	k1gFnSc1	Luisa
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
rakouského	rakouský	k2eAgMnSc2d1	rakouský
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
se	se	k3xPyFc4	se
rozvedl	rozvést	k5eAaPmAgMnS	rozvést
s	s	k7c7	s
císařovnou	císařovna	k1gFnSc7	císařovna
Josefínou	Josefína	k1gFnSc7	Josefína
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1810	[number]	k4	1810
v	v	k7c4	v
Saint-Cloud	Saint-Cloud	k1gInSc4	Saint-Cloud
při	při	k7c6	při
občanském	občanský	k2eAgInSc6d1	občanský
obřadu	obřad	k1gInSc6	obřad
pojal	pojmout	k5eAaPmAgMnS	pojmout
za	za	k7c4	za
choť	choť	k1gFnSc4	choť
habsburskou	habsburský	k2eAgFnSc4d1	habsburská
princeznu	princezna	k1gFnSc4	princezna
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Louvru	Louvre	k1gInSc6	Louvre
k	k	k7c3	k
obřadu	obřad	k1gInSc3	obřad
církevnímu	církevní	k2eAgInSc3d1	církevní
<g/>
.	.	kIx.	.
20	[number]	k4	20
března	březen	k1gInSc2	březen
1811	[number]	k4	1811
Marie	Marie	k1gFnSc1	Marie
Luisa	Luisa	k1gFnSc1	Luisa
porodila	porodit	k5eAaPmAgFnS	porodit
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
obdržel	obdržet	k5eAaPmAgMnS	obdržet
titul	titul	k1gInSc4	titul
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
a	a	k8xC	a
při	při	k7c6	při
křtu	křest	k1gInSc6	křest
provedeném	provedený	k2eAgInSc6d1	provedený
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
Notre-Dame	Notre-Dam	k1gInSc5	Notre-Dam
dostal	dostat	k5eAaPmAgMnS	dostat
jméno	jméno	k1gNnSc4	jméno
Napoleon	Napoleon	k1gMnSc1	Napoleon
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
podepsání	podepsání	k1gNnSc6	podepsání
schönnbrunnské	schönnbrunnský	k2eAgFnSc2d1	schönnbrunnský
smlouvy	smlouva	k1gFnSc2	smlouva
se	se	k3xPyFc4	se
Napoleon	Napoleon	k1gMnSc1	Napoleon
začal	začít	k5eAaPmAgMnS	začít
plnou	plný	k2eAgFnSc7d1	plná
měrou	míra	k1gFnSc7wR	míra
zabývat	zabývat	k5eAaImF	zabývat
bojem	boj	k1gInSc7	boj
proti	proti	k7c3	proti
Velké	velký	k2eAgFnSc3d1	velká
Británii	Británie	k1gFnSc3	Británie
a	a	k8xC	a
Španělsku	Španělsko	k1gNnSc3	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
dříve	dříve	k6eAd2	dříve
pracoval	pracovat	k5eAaImAgMnS	pracovat
od	od	k7c2	od
šesti	šest	k4xCc2	šest
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
do	do	k7c2	do
pozdního	pozdní	k2eAgInSc2d1	pozdní
večera	večer	k1gInSc2	večer
<g/>
,	,	kIx,	,
válka	válka	k1gFnSc1	válka
na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
však	však	k9	však
i	i	k9	i
přes	přes	k7c4	přes
jeho	jeho	k3xOp3gFnSc4	jeho
veškerou	veškerý	k3xTgFnSc4	veškerý
snahu	snaha	k1gFnSc4	snaha
pohlcovala	pohlcovat	k5eAaImAgNnP	pohlcovat
další	další	k2eAgNnPc1d1	další
francouzská	francouzský	k2eAgNnPc1d1	francouzské
vojska	vojsko	k1gNnPc1	vojsko
i	i	k8xC	i
finance	finance	k1gFnPc1	finance
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Wagramu	Wagram	k1gInSc2	Wagram
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
situaci	situace	k1gFnSc4	situace
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
frontě	fronta	k1gFnSc6	fronta
vyřeší	vyřešit	k5eAaPmIp3nS	vyřešit
osobně	osobně	k6eAd1	osobně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
kroku	krok	k1gInSc3	krok
se	se	k3xPyFc4	se
neodhodlal	odhodlat	k5eNaPmAgMnS	odhodlat
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
nebránily	bránit	k5eNaImAgInP	bránit
žádné	žádný	k3yNgInPc1	žádný
závažné	závažný	k2eAgInPc1d1	závažný
politické	politický	k2eAgInPc1d1	politický
či	či	k8xC	či
strategické	strategický	k2eAgInPc1d1	strategický
důvody	důvod	k1gInPc1	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Vůči	vůči	k7c3	vůči
Británii	Británie	k1gFnSc3	Británie
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
stupňovat	stupňovat	k5eAaImF	stupňovat
především	především	k9	především
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
nátlak	nátlak	k1gInSc4	nátlak
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
blokády	blokáda	k1gFnSc2	blokáda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
doufal	doufat	k5eAaImAgMnS	doufat
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
ostrovní	ostrovní	k2eAgFnSc4d1	ostrovní
velmoc	velmoc	k1gFnSc4	velmoc
dohnat	dohnat	k5eAaPmF	dohnat
k	k	k7c3	k
finančnímu	finanční	k2eAgInSc3d1	finanční
krachu	krach	k1gInSc3	krach
a	a	k8xC	a
mírové	mírový	k2eAgFnSc3d1	mírová
kapitulaci	kapitulace	k1gFnSc3	kapitulace
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
snaha	snaha	k1gFnSc1	snaha
však	však	k9	však
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
jen	jen	k9	jen
částečného	částečný	k2eAgInSc2d1	částečný
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
pašerákům	pašerák	k1gMnPc3	pašerák
se	se	k3xPyFc4	se
anglické	anglický	k2eAgNnSc1d1	anglické
zboží	zboží	k1gNnSc1	zboží
i	i	k9	i
nadále	nadále	k6eAd1	nadále
dostávalo	dostávat	k5eAaImAgNnS	dostávat
ke	k	k7c3	k
svým	svůj	k3xOyFgMnPc3	svůj
evropským	evropský	k2eAgMnPc3d1	evropský
zákazníkům	zákazník	k1gMnPc3	zákazník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1810	[number]	k4	1810
<g/>
–	–	k?	–
<g/>
1811	[number]	k4	1811
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
Francie	Francie	k1gFnSc1	Francie
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
velké	velký	k2eAgFnPc4d1	velká
banky	banka	k1gFnPc4	banka
a	a	k8xC	a
obchodníky	obchodník	k1gMnPc4	obchodník
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
i	i	k9	i
na	na	k7c4	na
výrobní	výrobní	k2eAgNnPc4d1	výrobní
odvětví	odvětví	k1gNnPc4	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
musel	muset	k5eAaImAgMnS	muset
podniknout	podniknout	k5eAaPmF	podniknout
řadu	řada	k1gFnSc4	řada
opatření	opatření	k1gNnSc2	opatření
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
důsledky	důsledek	k1gInPc1	důsledek
krize	krize	k1gFnSc2	krize
zmírnil	zmírnit	k5eAaPmAgMnS	zmírnit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
chápal	chápat	k5eAaImAgMnS	chápat
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
úplnému	úplný	k2eAgNnSc3d1	úplné
ekonomickému	ekonomický	k2eAgNnSc3d1	ekonomické
zotavení	zotavení	k1gNnSc3	zotavení
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
pouze	pouze	k6eAd1	pouze
po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
berlínských	berlínský	k2eAgInPc2d1	berlínský
dekretů	dekret	k1gInPc2	dekret
<g/>
.	.	kIx.	.
</s>
<s>
Zrealizovat	zrealizovat	k5eAaPmF	zrealizovat
akt	akt	k1gInSc4	akt
takového	takový	k3xDgInSc2	takový
významu	význam	k1gInSc2	význam
si	se	k3xPyFc3	se
však	však	k9	však
mohl	moct	k5eAaImAgInS	moct
dovolit	dovolit	k5eAaPmF	dovolit
teprve	teprve	k6eAd1	teprve
až	až	k6eAd1	až
porazí	porazit	k5eAaPmIp3nS	porazit
Brity	Brit	k1gMnPc4	Brit
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
i	i	k9	i
přes	přes	k7c4	přes
dohodu	dohoda	k1gFnSc4	dohoda
sjednanou	sjednaný	k2eAgFnSc4d1	sjednaná
v	v	k7c6	v
Tylži	Tylž	k1gFnSc6	Tylž
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
otevíral	otevírat	k5eAaImAgMnS	otevírat
přístavy	přístav	k1gInPc4	přístav
ruský	ruský	k2eAgMnSc1d1	ruský
car	car	k1gMnSc1	car
Alexandr	Alexandr	k1gMnSc1	Alexandr
I.	I.	kA	I.
</s>
</p>
<p>
<s>
===	===	k?	===
Ruské	ruský	k2eAgNnSc1d1	ruské
tažení	tažení	k1gNnSc1	tažení
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
vážnější	vážní	k2eAgFnSc1d2	vážnější
roztržka	roztržka	k1gFnSc1	roztržka
mezi	mezi	k7c7	mezi
vládci	vládce	k1gMnPc7	vládce
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
k	k	k7c3	k
podzimu	podzim	k1gInSc3	podzim
roku	rok	k1gInSc2	rok
1810	[number]	k4	1810
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
car	car	k1gMnSc1	car
Alexandr	Alexandr	k1gMnSc1	Alexandr
I.	I.	kA	I.
ignoroval	ignorovat	k5eAaImAgMnS	ignorovat
ujednání	ujednání	k1gNnSc4	ujednání
o	o	k7c6	o
kontinentální	kontinentální	k2eAgFnSc6d1	kontinentální
blokádě	blokáda	k1gFnSc6	blokáda
a	a	k8xC	a
umožnil	umožnit	k5eAaPmAgInS	umožnit
dvanácti	dvanáct	k4xCc2	dvanáct
stovkám	stovka	k1gFnPc3	stovka
britských	britský	k2eAgInPc2d1	britský
obchodních	obchodní	k2eAgInPc2d1	obchodní
lodí	loď	k1gFnPc2	loď
vyložit	vyložit	k5eAaPmF	vyložit
zboží	zboží	k1gNnSc4	zboží
v	v	k7c6	v
pobaltských	pobaltský	k2eAgInPc6d1	pobaltský
přístavech	přístav	k1gInPc6	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	napoleon	k1gInSc1	napoleon
jako	jako	k8xC	jako
odvetu	odvet	k1gInSc2	odvet
anektoval	anektovat	k5eAaBmAgInS	anektovat
vévodství	vévodství	k1gNnSc4	vévodství
Oldenburské	oldenburský	k2eAgNnSc4d1	oldenburský
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
pro	pro	k7c4	pro
ruského	ruský	k2eAgMnSc4d1	ruský
vládce	vládce	k1gMnSc4	vládce
představovalo	představovat	k5eAaImAgNnS	představovat
velmi	velmi	k6eAd1	velmi
výraznou	výrazný	k2eAgFnSc4d1	výrazná
urážku	urážka	k1gFnSc4	urážka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
choť	choť	k1gFnSc1	choť
oldenburského	oldenburský	k2eAgMnSc2d1	oldenburský
následníka	následník	k1gMnSc2	následník
trůnu	trůn	k1gInSc2	trůn
byla	být	k5eAaImAgFnS	být
Alexandrovou	Alexandrův	k2eAgFnSc7d1	Alexandrova
sestrou	sestra	k1gFnSc7	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Car	car	k1gMnSc1	car
kontroval	kontrovat	k5eAaImAgMnS	kontrovat
novým	nový	k2eAgMnSc7d1	nový
celním	celní	k2eAgInSc7d1	celní
tarifem	tarif	k1gInSc7	tarif
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zvyšoval	zvyšovat	k5eAaImAgMnS	zvyšovat
sazby	sazba	k1gFnPc4	sazba
na	na	k7c4	na
dovoz	dovoz	k1gInSc4	dovoz
luxusního	luxusní	k2eAgNnSc2d1	luxusní
francouzského	francouzský	k2eAgNnSc2d1	francouzské
zboží	zboží	k1gNnSc2	zboží
a	a	k8xC	a
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
tedy	tedy	k9	tedy
začal	začít	k5eAaPmAgMnS	začít
na	na	k7c4	na
ruského	ruský	k2eAgMnSc4d1	ruský
panovníka	panovník	k1gMnSc4	panovník
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
diplomatický	diplomatický	k2eAgInSc4d1	diplomatický
nátlak	nátlak	k1gInSc4	nátlak
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
však	však	k9	však
selhal	selhat	k5eAaPmAgInS	selhat
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
pro	pro	k7c4	pro
důraznější	důrazný	k2eAgNnSc4d2	důraznější
opatření	opatření	k1gNnSc4	opatření
–	–	k?	–
válku	válek	k1gInSc2	válek
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
shromáždil	shromáždit	k5eAaPmAgInS	shromáždit
dosud	dosud	k6eAd1	dosud
největší	veliký	k2eAgFnSc4d3	veliký
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
kdy	kdy	k6eAd1	kdy
operovala	operovat	k5eAaImAgFnS	operovat
na	na	k7c6	na
evropském	evropský	k2eAgNnSc6d1	Evropské
bojišti	bojiště	k1gNnSc6	bojiště
<g/>
,	,	kIx,	,
čítající	čítající	k2eAgMnSc1d1	čítající
na	na	k7c4	na
440	[number]	k4	440
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
počtu	počet	k1gInSc2	počet
bylo	být	k5eAaImAgNnS	být
125	[number]	k4	125
000	[number]	k4	000
rodilých	rodilý	k2eAgMnPc2d1	rodilý
Francouzů	Francouz	k1gMnPc2	Francouz
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc4	zbytek
tvořily	tvořit	k5eAaImAgInP	tvořit
kontingenty	kontingent	k1gInPc1	kontingent
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
loajálních	loajální	k2eAgMnPc2d1	loajální
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Soustřeďování	soustřeďování	k1gNnSc1	soustřeďování
vojsk	vojsko	k1gNnPc2	vojsko
trvalo	trvat	k5eAaImAgNnS	trvat
téměř	téměř	k6eAd1	téměř
celou	celý	k2eAgFnSc4d1	celá
první	první	k4xOgFnSc4	první
polovinu	polovina	k1gFnSc4	polovina
roku	rok	k1gInSc2	rok
1812	[number]	k4	1812
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
v	v	k7c4	v
šest	šest	k4xCc4	šest
hodin	hodina	k1gFnPc2	hodina
ráno	ráno	k6eAd1	ráno
se	se	k3xPyFc4	se
Napoleon	napoleon	k1gInSc1	napoleon
v	v	k7c6	v
doprovodu	doprovod	k1gInSc6	doprovod
císařovny	císařovna	k1gFnSc2	císařovna
odebral	odebrat	k5eAaPmAgInS	odebrat
z	z	k7c2	z
paláce	palác	k1gInSc2	palác
Saint-Cloud	Saint-Clouda	k1gFnPc2	Saint-Clouda
k	k	k7c3	k
Velké	velký	k2eAgFnSc3d1	velká
armádě	armáda	k1gFnSc3	armáda
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Drážďan	Drážďany	k1gInPc2	Drážďany
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
něj	on	k3xPp3gNnSc2	on
shromáždili	shromáždit	k5eAaPmAgMnP	shromáždit
panovníci	panovník	k1gMnPc1	panovník
většiny	většina	k1gFnSc2	většina
jím	jíst	k5eAaImIp1nS	jíst
podrobených	podrobený	k2eAgInPc2d1	podrobený
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Následujících	následující	k2eAgInPc2d1	následující
čtrnáct	čtrnáct	k4xCc4	čtrnáct
dní	den	k1gInPc2	den
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
saské	saský	k2eAgFnSc6d1	saská
metropoli	metropol	k1gFnSc6	metropol
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgFnP	označovat
jako	jako	k8xC	jako
Drážďanská	drážďanský	k2eAgFnSc1d1	Drážďanská
apoteóza	apoteóza	k1gFnSc1	apoteóza
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
vrcholným	vrcholný	k2eAgInSc7d1	vrcholný
okamžikem	okamžik	k1gInSc7	okamžik
Napoleonovy	Napoleonův	k2eAgFnSc2d1	Napoleonova
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
pánem	pán	k1gMnSc7	pán
nad	nad	k7c7	nad
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
,	,	kIx,	,
císařem	císař	k1gMnSc7	císař
nad	nad	k7c7	nad
císaři	císař	k1gMnPc7	císař
a	a	k8xC	a
králi	král	k1gMnPc7	král
<g/>
,	,	kIx,	,
korunované	korunovaný	k2eAgFnPc4d1	korunovaná
hlavy	hlava	k1gFnPc4	hlava
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
tvořily	tvořit	k5eAaImAgFnP	tvořit
jeho	on	k3xPp3gInSc4	on
dvůr	dvůr	k1gInSc4	dvůr
a	a	k8xC	a
průvod	průvod	k1gInSc4	průvod
<g/>
!	!	kIx.	!
</s>
<s>
Z	z	k7c2	z
Drážďan	Drážďany	k1gInPc2	Drážďany
císař	císař	k1gMnSc1	císař
Francouzů	Francouz	k1gMnPc2	Francouz
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Královce	Královec	k1gInSc2	Královec
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
zamířil	zamířit	k5eAaPmAgMnS	zamířit
do	do	k7c2	do
Wikowischki	Wikowischk	k1gFnSc2	Wikowischk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vydal	vydat	k5eAaPmAgMnS	vydat
pro	pro	k7c4	pro
vyčkávající	vyčkávající	k2eAgMnPc4d1	vyčkávající
vojáky	voják	k1gMnPc4	voják
proklamaci	proklamace	k1gFnSc6	proklamace
o	o	k7c6	o
nadcházejícím	nadcházející	k2eAgNnSc6d1	nadcházející
tažení	tažení	k1gNnSc6	tažení
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
dal	dát	k5eAaPmAgMnS	dát
těmto	tento	k3xDgMnPc3	tento
mužům	muž	k1gMnPc3	muž
pokyn	pokyn	k1gInSc4	pokyn
k	k	k7c3	k
pochodu	pochod	k1gInSc3	pochod
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Němen	Němna	k1gFnPc2	Němna
(	(	kIx(	(
<g/>
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
po	po	k7c6	po
postavení	postavení	k1gNnSc6	postavení
třech	tři	k4xCgInPc2	tři
mostů	most	k1gInPc2	most
u	u	k7c2	u
Jurburku	Jurburk	k1gInSc2	Jurburk
<g/>
,	,	kIx,	,
Kovna	Kovn	k1gInSc2	Kovn
a	a	k8xC	a
Olity	Olita	k1gFnSc2	Olita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
plánoval	plánovat	k5eAaImAgMnS	plánovat
válku	válka	k1gFnSc4	válka
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
co	co	k9	co
nejrychleji	rychle	k6eAd3	rychle
ukončit	ukončit	k5eAaPmF	ukončit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vnutí	vnutit	k5eAaPmIp3nS	vnutit
nepříteli	nepřítel	k1gMnSc3	nepřítel
rozhodující	rozhodující	k2eAgFnSc4d1	rozhodující
bitvu	bitva	k1gFnSc4	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
jeho	jeho	k3xOp3gMnSc1	jeho
protivník	protivník	k1gMnSc1	protivník
generálporučík	generálporučík	k1gMnSc1	generálporučík
Barclay	Barclaa	k1gFnSc2	Barclaa
de	de	k?	de
Tolly	Tolla	k1gFnSc2	Tolla
<g/>
,	,	kIx,	,
vědom	vědom	k2eAgMnSc1d1	vědom
si	se	k3xPyFc3	se
skutečnosti	skutečnost	k1gFnSc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
proti	proti	k7c3	proti
Francouzům	Francouz	k1gMnPc3	Francouz
může	moct	k5eAaImIp3nS	moct
postavit	postavit	k5eAaPmF	postavit
pouze	pouze	k6eAd1	pouze
256	[number]	k4	256
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
rozdělených	rozdělená	k1gFnPc2	rozdělená
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
armád	armáda	k1gFnPc2	armáda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
taktiku	taktika	k1gFnSc4	taktika
spálené	spálený	k2eAgFnSc2d1	spálená
země	zem	k1gFnSc2	zem
a	a	k8xC	a
vtahovat	vtahovat	k5eAaImF	vtahovat
francouzská	francouzský	k2eAgNnPc4d1	francouzské
vojska	vojsko	k1gNnPc4	vojsko
hlouběji	hluboko	k6eAd2	hluboko
do	do	k7c2	do
rozlehlého	rozlehlý	k2eAgNnSc2d1	rozlehlé
ruského	ruský	k2eAgNnSc2d1	ruské
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
pokusil	pokusit	k5eAaPmAgMnS	pokusit
obě	dva	k4xCgFnPc4	dva
nepřátelské	přátelský	k2eNgFnPc4d1	nepřátelská
armády	armáda	k1gFnPc4	armáda
obejít	obejít	k5eAaPmF	obejít
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
tato	tento	k3xDgFnSc1	tento
strategie	strategie	k1gFnSc1	strategie
neslavila	slavit	k5eNaImAgFnS	slavit
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
protivníkovy	protivníkův	k2eAgFnPc1d1	protivníkova
rozdělené	rozdělený	k2eAgFnPc1d1	rozdělená
síly	síla	k1gFnPc1	síla
spojily	spojit	k5eAaPmAgFnP	spojit
u	u	k7c2	u
Smolenska	Smolensko	k1gNnSc2	Smolensko
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
většímu	veliký	k2eAgInSc3d2	veliký
střetu	střet	k1gInSc3	střet
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Generálporučík	generálporučík	k1gMnSc1	generálporučík
Dochturov	Dochturov	k1gInSc4	Dochturov
s	s	k7c7	s
20	[number]	k4	20
000	[number]	k4	000
muži	muž	k1gMnPc7	muž
po	po	k7c4	po
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
bránil	bránit	k5eAaImAgMnS	bránit
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
kryl	krýt	k5eAaImAgInS	krýt
ústup	ústup	k1gInSc4	ústup
celého	celý	k2eAgNnSc2d1	celé
vojska	vojsko	k1gNnSc2	vojsko
a	a	k8xC	a
Rusům	Rus	k1gMnPc3	Rus
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
podařilo	podařit	k5eAaPmAgNnS	podařit
vyklouznout	vyklouznout	k5eAaPmF	vyklouznout
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dobytém	dobytý	k2eAgNnSc6d1	dobyté
Smolensku	Smolensko	k1gNnSc6	Smolensko
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
Bonaparte	bonapart	k1gInSc5	bonapart
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
má	mít	k5eAaImIp3nS	mít
zastavit	zastavit	k5eAaPmF	zastavit
postup	postup	k1gInSc4	postup
nebo	nebo	k8xC	nebo
táhnout	táhnout	k5eAaImF	táhnout
až	až	k9	až
k	k	k7c3	k
Moskvě	Moskva	k1gFnSc3	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
první	první	k4xOgFnSc4	první
alternativu	alternativa	k1gFnSc4	alternativa
hovořil	hovořit	k5eAaImAgMnS	hovořit
zejména	zejména	k9	zejména
tristní	tristní	k2eAgInSc4d1	tristní
stav	stav	k1gInSc4	stav
zásobování	zásobování	k1gNnSc2	zásobování
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
komunikačních	komunikační	k2eAgFnPc2d1	komunikační
linií	linie	k1gFnPc2	linie
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
postupně	postupně	k6eAd1	postupně
drolil	drolit	k5eAaImAgMnS	drolit
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
chybějící	chybějící	k2eAgMnPc4d1	chybějící
koně	kůň	k1gMnPc4	kůň
pro	pro	k7c4	pro
kavalérii	kavalérie	k1gFnSc4	kavalérie
a	a	k8xC	a
pokles	pokles	k1gInSc4	pokles
morálky	morálka	k1gFnSc2	morálka
zapříčiněný	zapříčiněný	k2eAgInSc4d1	zapříčiněný
hladem	hlad	k1gMnSc7	hlad
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
začaly	začít	k5eAaPmAgFnP	začít
propukat	propukat	k5eAaImF	propukat
epidemie	epidemie	k1gFnSc1	epidemie
úplavice	úplavice	k1gFnSc2	úplavice
a	a	k8xC	a
tyfu	tyf	k1gInSc2	tyf
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
si	se	k3xPyFc3	se
francouzský	francouzský	k2eAgMnSc1d1	francouzský
císař	císař	k1gMnSc1	císař
nemohl	moct	k5eNaImAgMnS	moct
dovolit	dovolit	k5eAaPmF	dovolit
ztratit	ztratit	k5eAaPmF	ztratit
tvář	tvář	k1gFnSc4	tvář
před	před	k7c7	před
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
sázce	sázka	k1gFnSc6	sázka
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
pověst	pověst	k1gFnSc1	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Počítal	počítat	k5eAaImAgMnS	počítat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
sil	síla	k1gFnPc2	síla
má	mít	k5eAaImIp3nS	mít
ještě	ještě	k9	ještě
dost	dost	k6eAd1	dost
a	a	k8xC	a
věřil	věřit	k5eAaImAgMnS	věřit
ve	v	k7c4	v
svou	svůj	k3xOyFgFnSc4	svůj
šťastnou	šťastný	k2eAgFnSc4d1	šťastná
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Přiklonil	přiklonit	k5eAaPmAgMnS	přiklonit
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
k	k	k7c3	k
volbě	volba	k1gFnSc3	volba
pokračovat	pokračovat	k5eAaImF	pokračovat
dál	daleko	k6eAd2	daleko
k	k	k7c3	k
ruské	ruský	k2eAgFnSc3d1	ruská
metropoli	metropol	k1gFnSc3	metropol
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
toto	tento	k3xDgNnSc1	tento
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
bylo	být	k5eAaImAgNnS	být
podle	podle	k7c2	podle
řady	řada	k1gFnSc2	řada
historiků	historik	k1gMnPc2	historik
hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
jeho	jeho	k3xOp3gFnSc2	jeho
pozdější	pozdní	k2eAgFnSc2d2	pozdější
porážky	porážka	k1gFnSc2	porážka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
24	[number]	k4	24
<g/>
.	.	kIx.	.
na	na	k7c4	na
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
Francouzi	Francouz	k1gMnPc1	Francouz
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
ve	v	k7c6	v
stopách	stopa	k1gFnPc6	stopa
ustupujících	ustupující	k2eAgMnPc2d1	ustupující
Rusů	Rus	k1gMnPc2	Rus
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Vjazmu	Vjazma	k1gFnSc4	Vjazma
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
našli	najít	k5eAaPmAgMnP	najít
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
zásob	zásoba	k1gFnPc2	zásoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
nestačil	stačit	k5eNaBmAgMnS	stačit
protivník	protivník	k1gMnSc1	protivník
zničit	zničit	k5eAaPmF	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Car	car	k1gMnSc1	car
Alexandr	Alexandr	k1gMnSc1	Alexandr
mezitím	mezitím	k6eAd1	mezitím
odvolal	odvolat	k5eAaPmAgMnS	odvolat
z	z	k7c2	z
postu	post	k1gInSc2	post
vrchního	vrchní	k2eAgMnSc2d1	vrchní
velitele	velitel	k1gMnSc2	velitel
ruské	ruský	k2eAgFnSc2d1	ruská
armády	armáda	k1gFnSc2	armáda
Barclaye	Barclay	k1gMnSc2	Barclay
de	de	k?	de
Tollyho	Tolly	k1gMnSc2	Tolly
a	a	k8xC	a
nahradil	nahradit	k5eAaPmAgInS	nahradit
jej	on	k3xPp3gMnSc4	on
generálem	generál	k1gMnSc7	generál
Kutuzovem	Kutuzov	k1gInSc7	Kutuzov
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
přinucen	přinutit	k5eAaPmNgInS	přinutit
politickými	politický	k2eAgFnPc7d1	politická
okolnostmi	okolnost	k1gFnPc7	okolnost
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgInS	dát
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
vytoužené	vytoužený	k2eAgNnSc4d1	vytoužené
generální	generální	k2eAgNnSc4d1	generální
střetnutí	střetnutí	k1gNnSc4	střetnutí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
u	u	k7c2	u
Borodina	Borodin	k2eAgInSc2d1	Borodin
došlo	dojít	k5eAaPmAgNnS	dojít
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
žádné	žádný	k3yNgFnSc3	žádný
straně	strana	k1gFnSc3	strana
nepřinesla	přinést	k5eNaPmAgFnS	přinést
rozhodující	rozhodující	k2eAgFnSc4d1	rozhodující
převahu	převaha	k1gFnSc4	převaha
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
o	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
Napoleon	napoleon	k1gInSc4	napoleon
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Ubytoval	ubytovat	k5eAaPmAgInS	ubytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Kremlu	Kreml	k1gInSc6	Kreml
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
po	po	k7c4	po
celé	celý	k2eAgNnSc4d1	celé
tažení	tažení	k1gNnSc4	tažení
i	i	k9	i
zde	zde	k6eAd1	zde
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
vyprávění	vyprávění	k1gNnSc2	vyprávění
očitých	očitý	k2eAgMnPc2d1	očitý
svědků	svědek	k1gMnPc2	svědek
<g/>
)	)	kIx)	)
psal	psát	k5eAaImAgInS	psát
sto	sto	k4xCgNnSc4	sto
dopisů	dopis	k1gInPc2	dopis
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
řídil	řídit	k5eAaImAgMnS	řídit
všechny	všechen	k3xTgFnPc4	všechen
státní	státní	k2eAgFnPc4d1	státní
záležitosti	záležitost	k1gFnPc4	záležitost
své	svůj	k3xOyFgFnSc2	svůj
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
zabýval	zabývat	k5eAaImAgInS	zabývat
se	se	k3xPyFc4	se
diplomacií	diplomacie	k1gFnSc7	diplomacie
i	i	k8xC	i
problémy	problém	k1gInPc7	problém
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
a	a	k8xC	a
současně	současně	k6eAd1	současně
řešil	řešit	k5eAaImAgInS	řešit
otázky	otázka	k1gFnPc4	otázka
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
mu	on	k3xPp3gMnSc3	on
přinášelo	přinášet	k5eAaImAgNnS	přinášet
ruské	ruský	k2eAgNnSc1d1	ruské
tažení	tažení	k1gNnSc1	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Mohutné	mohutný	k2eAgInPc4d1	mohutný
požáry	požár	k1gInPc4	požár
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
od	od	k7c2	od
prvních	první	k4xOgInPc2	první
dnů	den	k1gInPc2	den
zachvacovaly	zachvacovat	k5eAaImAgFnP	zachvacovat
celé	celý	k2eAgNnSc4d1	celé
velkoměsto	velkoměsto	k1gNnSc4	velkoměsto
<g/>
,	,	kIx,	,
jej	on	k3xPp3gInSc4	on
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
donutily	donutit	k5eAaPmAgInP	donutit
kremelskou	kremelský	k2eAgFnSc4d1	kremelská
rezidenci	rezidence	k1gFnSc4	rezidence
opustit	opustit	k5eAaPmF	opustit
<g/>
,	,	kIx,	,
druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
však	však	k9	však
oheň	oheň	k1gInSc1	oheň
umírnil	umírnit	k5eAaPmAgInS	umírnit
a	a	k8xC	a
umožnil	umožnit	k5eAaPmAgInS	umožnit
mu	on	k3xPp3gMnSc3	on
návrat	návrat	k1gInSc4	návrat
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
<g/>
S	s	k7c7	s
přibývajícími	přibývající	k2eAgInPc7d1	přibývající
dny	den	k1gInPc7	den
si	se	k3xPyFc3	se
francouzský	francouzský	k2eAgMnSc1d1	francouzský
císař	císař	k1gMnSc1	císař
začínal	začínat	k5eAaImAgMnS	začínat
uvědomovat	uvědomovat	k5eAaImF	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Moskva	Moskva	k1gFnSc1	Moskva
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
další	další	k2eAgFnSc7d1	další
slepou	slepý	k2eAgFnSc7d1	slepá
uličkou	ulička	k1gFnSc7	ulička
jeho	jeho	k3xOp3gFnSc2	jeho
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
maršálů	maršál	k1gMnPc2	maršál
a	a	k8xC	a
generálů	generál	k1gMnPc2	generál
mu	on	k3xPp3gMnSc3	on
radili	radit	k5eAaImAgMnP	radit
ve	v	k7c6	v
velkoměstě	velkoměsto	k1gNnSc6	velkoměsto
přezimovat	přezimovat	k5eAaBmF	přezimovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Napoleon	Napoleon	k1gMnSc1	Napoleon
dobře	dobře	k6eAd1	dobře
chápal	chápat	k5eAaImAgMnS	chápat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
pozice	pozice	k1gFnSc1	pozice
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
spojenců	spojenec	k1gMnPc2	spojenec
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
pevná	pevný	k2eAgFnSc1d1	pevná
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
mohl	moct	k5eAaImAgMnS	moct
na	na	k7c4	na
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
dovolit	dovolit	k5eAaPmF	dovolit
zůstat	zůstat	k5eAaPmF	zůstat
mimo	mimo	k7c4	mimo
evropské	evropský	k2eAgNnSc4d1	Evropské
dění	dění	k1gNnSc4	dění
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
neúnosnou	únosný	k2eNgFnSc7d1	neúnosná
měrou	míra	k1gFnSc7wR	míra
začaly	začít	k5eAaPmAgInP	začít
množit	množit	k5eAaImF	množit
souboje	souboj	k1gInPc1	souboj
<g/>
,	,	kIx,	,
na	na	k7c6	na
denním	denní	k2eAgInSc6d1	denní
pořádku	pořádek	k1gInSc6	pořádek
byly	být	k5eAaImAgFnP	být
výtržnosti	výtržnost	k1gFnPc1	výtržnost
a	a	k8xC	a
kázeň	kázeň	k1gFnSc1	kázeň
i	i	k8xC	i
morálka	morálka	k1gFnSc1	morálka
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
poddůstojníků	poddůstojník	k1gMnPc2	poddůstojník
rapidně	rapidně	k6eAd1	rapidně
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
<g/>
.	.	kIx.	.
</s>
<s>
Pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
nabídnout	nabídnout	k5eAaPmF	nabídnout
Alexandrovi	Alexandr	k1gMnSc3	Alexandr
I.	I.	kA	I.
mír	mír	k1gInSc1	mír
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc4	ten
však	však	k9	však
Napoleonovu	Napoleonův	k2eAgFnSc4d1	Napoleonova
nabídku	nabídka	k1gFnSc4	nabídka
přijmout	přijmout	k5eAaPmF	přijmout
nemohl	moct	k5eNaImAgInS	moct
a	a	k8xC	a
ani	ani	k8xC	ani
nechtěl	chtít	k5eNaImAgMnS	chtít
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
vydal	vydat	k5eAaPmAgMnS	vydat
císař	císař	k1gMnSc1	císař
Francouzů	Francouz	k1gMnPc2	Francouz
dispozice	dispozice	k1gFnSc2	dispozice
nasvědčující	nasvědčující	k2eAgFnSc1d1	nasvědčující
brzké	brzký	k2eAgFnSc3d1	brzká
evakuaci	evakuace	k1gFnSc3	evakuace
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
maršál	maršál	k1gMnSc1	maršál
Murat	Murat	k1gInSc4	Murat
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
porážku	porážka	k1gFnSc4	porážka
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Tarutina	Tarutin	k2eAgMnSc2d1	Tarutin
a	a	k8xC	a
Napoleon	napoleon	k1gInSc1	napoleon
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g />
.	.	kIx.	.
</s>
<s>
vyrazit	vyrazit	k5eAaPmF	vyrazit
z	z	k7c2	z
Moskvy	Moskva	k1gFnSc2	Moskva
vstříc	vstříc	k6eAd1	vstříc
Kutuzovovi	Kutuzovův	k2eAgMnPc1d1	Kutuzovův
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
na	na	k7c6	na
Kalugu	Kalug	k1gInSc6	Kalug
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
byl	být	k5eAaImAgInS	být
však	však	k9	však
francouzský	francouzský	k2eAgInSc1d1	francouzský
předvoj	předvoj	k1gInSc1	předvoj
zastaven	zastavit	k5eAaPmNgInS	zastavit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Malojaroslavce	Malojaroslavec	k1gMnSc2	Malojaroslavec
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přimělo	přimět	k5eAaPmAgNnS	přimět
Napoleona	Napoleon	k1gMnSc4	Napoleon
vydat	vydat	k5eAaPmF	vydat
rozkaz	rozkaz	k1gInSc4	rozkaz
k	k	k7c3	k
návratu	návrat	k1gInSc3	návrat
zpět	zpět	k6eAd1	zpět
ke	k	k7c3	k
Smolensku	Smolensko	k1gNnSc3	Smolensko
tou	ten	k3xDgFnSc7	ten
samou	samý	k3xTgFnSc7	samý
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
již	již	k9	již
armáda	armáda	k1gFnSc1	armáda
během	během	k7c2	během
ruské	ruský	k2eAgFnSc2d1	ruská
kampaně	kampaň	k1gFnSc2	kampaň
protáhla	protáhnout	k5eAaPmAgFnS	protáhnout
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
udeřily	udeřit	k5eAaPmAgInP	udeřit
první	první	k4xOgInPc1	první
mrazy	mráz	k1gInPc1	mráz
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
předznamenaly	předznamenat	k5eAaPmAgFnP	předznamenat
atmosféru	atmosféra	k1gFnSc4	atmosféra
následujícího	následující	k2eAgInSc2d1	následující
exodu	exodus	k1gInSc2	exodus
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
snášel	snášet	k5eAaImAgMnS	snášet
útrapy	útrapa	k1gFnSc2	útrapa
společně	společně	k6eAd1	společně
s	s	k7c7	s
vojáky	voják	k1gMnPc7	voják
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgInS	snažit
osobním	osobní	k2eAgInSc7d1	osobní
příkladem	příklad	k1gInSc7	příklad
povzbudit	povzbudit	k5eAaPmF	povzbudit
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celá	k1gFnPc1	celá
hodiny	hodina	k1gFnSc2	hodina
se	se	k3xPyFc4	se
brodil	brodit	k5eAaImAgInS	brodit
opřen	opřen	k2eAgInSc1d1	opřen
o	o	k7c4	o
hůl	hůl	k1gFnSc4	hůl
sněhem	sníh	k1gInSc7	sníh
a	a	k8xC	a
rozmlouval	rozmlouvat	k5eAaImAgInS	rozmlouvat
s	s	k7c7	s
pochodujícími	pochodující	k2eAgMnPc7d1	pochodující
<g/>
,	,	kIx,	,
vezl	vézt	k5eAaImAgMnS	vézt
se	se	k3xPyFc4	se
v	v	k7c6	v
kočáře	kočár	k1gInSc6	kočár
nebo	nebo	k8xC	nebo
pochodoval	pochodovat	k5eAaImAgInS	pochodovat
uprostřed	uprostřed	k7c2	uprostřed
vojáků	voják	k1gMnPc2	voják
gardy	garda	k1gFnSc2	garda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Smolenska	Smolensko	k1gNnSc2	Smolensko
dorazil	dorazit	k5eAaPmAgMnS	dorazit
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
8	[number]	k4	8
<g/>
.	.	kIx.	.
na	na	k7c4	na
9	[number]	k4	9
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
strávil	strávit	k5eAaPmAgMnS	strávit
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
<g/>
,	,	kIx,	,
shromáždil	shromáždit	k5eAaPmAgInS	shromáždit
rozptýlené	rozptýlený	k2eAgInPc4d1	rozptýlený
zbytky	zbytek	k1gInPc4	zbytek
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
městu	město	k1gNnSc3	město
Orša	Orš	k1gInSc2	Orš
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadem	listopad	k1gInSc7	listopad
byl	být	k5eAaImAgMnS	být
<g/>
,	,	kIx,	,
zle	zle	k6eAd1	zle
tísněný	tísněný	k2eAgMnSc1d1	tísněný
z	z	k7c2	z
několika	několik	k4yIc2	několik
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
přinucen	přinucen	k2eAgMnSc1d1	přinucen
k	k	k7c3	k
třídenní	třídenní	k2eAgFnSc3d1	třídenní
bitvě	bitva	k1gFnSc3	bitva
u	u	k7c2	u
Krasného	Krasný	k2eAgInSc2d1	Krasný
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
značné	značný	k2eAgFnPc4d1	značná
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
,	,	kIx,	,
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
postoupit	postoupit	k5eAaPmF	postoupit
až	až	k9	až
k	k	k7c3	k
Berezině	Berezina	k1gFnSc3	Berezina
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
sapérům	sapérům	k?	sapérům
podařilo	podařit	k5eAaPmAgNnS	podařit
postavit	postavit	k5eAaPmF	postavit
dva	dva	k4xCgInPc4	dva
mosty	most	k1gInPc4	most
přes	přes	k7c4	přes
vodní	vodní	k2eAgInSc4d1	vodní
tok	tok	k1gInSc4	tok
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
s	s	k7c7	s
gardou	garda	k1gFnSc7	garda
se	se	k3xPyFc4	se
přepravili	přepravit	k5eAaPmAgMnP	přepravit
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
břeh	břeh	k1gInSc4	břeh
<g/>
,	,	kIx,	,
za	za	k7c2	za
dramatických	dramatický	k2eAgFnPc2d1	dramatická
okolností	okolnost	k1gFnPc2	okolnost
oba	dva	k4xCgInPc1	dva
přechody	přechod	k1gInPc1	přechod
ucpaly	ucpat	k5eAaPmAgInP	ucpat
davy	dav	k1gInPc1	dav
nedisciplinovaných	disciplinovaný	k2eNgMnPc2d1	nedisciplinovaný
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
doprovodných	doprovodný	k2eAgMnPc2d1	doprovodný
civilistů	civilista	k1gMnPc2	civilista
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
panické	panický	k2eAgNnSc1d1	panické
chování	chování	k1gNnSc1	chování
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
vysokým	vysoký	k2eAgFnPc3d1	vysoká
ztrátám	ztráta	k1gFnPc3	ztráta
na	na	k7c6	na
lidských	lidský	k2eAgInPc6d1	lidský
životech	život	k1gInPc6	život
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Bereziny	Berezina	k1gFnSc2	Berezina
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
Napoleon	napoleon	k1gInSc1	napoleon
k	k	k7c3	k
Vilnu	Vilno	k1gNnSc3	Vilno
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
dorazil	dorazit	k5eAaPmAgInS	dorazit
6	[number]	k4	6
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
inkognito	inkognito	k6eAd1	inkognito
projížděl	projíždět	k5eAaImAgInS	projíždět
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
půli	půle	k1gFnSc6	půle
prosince	prosinec	k1gInSc2	prosinec
dorazil	dorazit	k5eAaPmAgInS	dorazit
na	na	k7c4	na
hranice	hranice	k1gFnPc4	hranice
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Věrohodné	věrohodný	k2eAgFnPc1d1	věrohodná
statistiky	statistika	k1gFnPc1	statistika
říkají	říkat	k5eAaImIp3nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
680	[number]	k4	680
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
Napoleon	Napoleon	k1gMnSc1	Napoleon
postupně	postupně	k6eAd1	postupně
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
ruského	ruský	k2eAgNnSc2d1	ruské
tažení	tažení	k1gNnSc2	tažení
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jich	on	k3xPp3gInPc2	on
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
do	do	k7c2	do
svých	svůj	k3xOyFgMnPc2	svůj
vlastí	vlast	k1gFnSc7	vlast
93	[number]	k4	93
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Válka	válka	k1gFnSc1	válka
šesté	šestý	k4xOgFnSc2	šestý
koalice	koalice	k1gFnSc2	koalice
a	a	k8xC	a
abdikace	abdikace	k1gFnSc2	abdikace
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
dorazil	dorazit	k5eAaPmAgInS	dorazit
Napoleon	napoleon	k1gInSc1	napoleon
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1812	[number]	k4	1812
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
utrpěnou	utrpěný	k2eAgFnSc4d1	utrpěná
porážku	porážka	k1gFnSc4	porážka
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
jist	jist	k2eAgMnSc1d1	jist
<g/>
,	,	kIx,	,
že	že	k8xS	že
brzy	brzy	k6eAd1	brzy
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
svými	svůj	k3xOyFgInPc7	svůj
počty	počet	k1gInPc7	počet
předčí	předčit	k5eAaBmIp3nP	předčit
Grande	grand	k1gMnSc5	grand
Armée	Armée	k1gNnPc3	Armée
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
ruského	ruský	k2eAgNnSc2d1	ruské
tažení	tažení	k1gNnSc2	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Optimisticky	optimisticky	k6eAd1	optimisticky
věřil	věřit	k5eAaImAgMnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
jara	jaro	k1gNnSc2	jaro
shromáždí	shromáždět	k5eAaImIp3nS	shromáždět
včetně	včetně	k7c2	včetně
spojeneckých	spojenecký	k2eAgInPc2d1	spojenecký
kontingentů	kontingent	k1gInPc2	kontingent
na	na	k7c4	na
450	[number]	k4	450
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kutuzovova	Kutuzovův	k2eAgFnSc1d1	Kutuzovova
armáda	armáda	k1gFnSc1	armáda
bude	být	k5eAaImBp3nS	být
značně	značně	k6eAd1	značně
prořídlá	prořídlý	k2eAgFnSc1d1	prořídlá
a	a	k8xC	a
vyčerpaná	vyčerpaný	k2eAgFnSc1d1	vyčerpaná
a	a	k8xC	a
nebude	být	k5eNaImBp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
se	se	k3xPyFc4	se
s	s	k7c7	s
Francouzi	Francouz	k1gMnPc7	Francouz
měřit	měřit	k5eAaImF	měřit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
podobné	podobný	k2eAgFnSc6d1	podobná
situaci	situace	k1gFnSc6	situace
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
od	od	k7c2	od
rána	ráno	k1gNnSc2	ráno
do	do	k7c2	do
večera	večer	k1gInSc2	večer
věnoval	věnovat	k5eAaImAgMnS	věnovat
otázkám	otázka	k1gFnPc3	otázka
výzbroje	výzbroj	k1gFnSc2	výzbroj
<g/>
,	,	kIx,	,
výstroje	výstroj	k1gFnSc2	výstroj
a	a	k8xC	a
výcviku	výcvik	k1gInSc2	výcvik
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
byly	být	k5eAaImAgInP	být
zbytky	zbytek	k1gInPc1	zbytek
Grande	grand	k1gMnSc5	grand
Armée	Armée	k1gFnPc2	Armée
vytlačovány	vytlačovat	k5eAaImNgInP	vytlačovat
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
tohoto	tento	k3xDgInSc2	tento
vývoje	vývoj	k1gInSc2	vývoj
konfliktu	konflikt	k1gInSc2	konflikt
pruský	pruský	k2eAgMnSc1d1	pruský
král	král	k1gMnSc1	král
Fridrich	Fridrich	k1gMnSc1	Fridrich
Vilém	Vilém	k1gMnSc1	Vilém
III	III	kA	III
<g/>
.	.	kIx.	.
podepsal	podepsat	k5eAaPmAgInS	podepsat
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1813	[number]	k4	1813
s	s	k7c7	s
Rusy	Rus	k1gMnPc4	Rus
spojeneckou	spojenecký	k2eAgFnSc4d1	spojenecká
smlouvu	smlouva	k1gFnSc4	smlouva
a	a	k8xC	a
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Francii	Francie	k1gFnSc3	Francie
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
rakouský	rakouský	k2eAgMnSc1d1	rakouský
císař	císař	k1gMnSc1	císař
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c6	na
příbuzenství	příbuzenství	k1gNnSc6	příbuzenství
s	s	k7c7	s
Napoleonem	Napoleon	k1gMnSc7	Napoleon
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
s	s	k7c7	s
carem	car	k1gMnSc7	car
Alexandrem	Alexandr	k1gMnSc7	Alexandr
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
příměří	příměří	k1gNnSc6	příměří
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
slezském	slezský	k2eAgInSc6d1	slezský
Bolesławieci	Bolesławieec	k1gInSc6	Bolesławieec
maršál	maršál	k1gMnSc1	maršál
Kutuzov	Kutuzov	k1gInSc1	Kutuzov
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
nahradil	nahradit	k5eAaPmAgMnS	nahradit
hrabě	hrabě	k1gMnSc1	hrabě
Wittgenstein	Wittgenstein	k1gMnSc1	Wittgenstein
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1813	[number]	k4	1813
císař	císař	k1gMnSc1	císař
Francouzů	Francouz	k1gMnPc2	Francouz
ustanovil	ustanovit	k5eAaPmAgMnS	ustanovit
svou	svůj	k3xOyFgFnSc4	svůj
manželku	manželka	k1gFnSc4	manželka
Marii	Maria	k1gFnSc4	Maria
Luisu	Luisa	k1gFnSc4	Luisa
regentkou	regentka	k1gFnSc7	regentka
<g/>
,	,	kIx,	,
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
regentskou	regentský	k2eAgFnSc4d1	regentská
radu	rada	k1gFnSc4	rada
a	a	k8xC	a
o	o	k7c4	o
pět	pět	k4xCc4	pět
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
odebral	odebrat	k5eAaPmAgInS	odebrat
k	k	k7c3	k
nové	nový	k2eAgFnSc3d1	nová
armádě	armáda	k1gFnSc3	armáda
do	do	k7c2	do
Mohuče	Mohuč	k1gFnSc2	Mohuč
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
24	[number]	k4	24
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
téhož	týž	k3xTgInSc2	týž
měsíce	měsíc	k1gInSc2	měsíc
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
proti	proti	k7c3	proti
pomalu	pomalu	k6eAd1	pomalu
postupujícím	postupující	k2eAgMnPc3d1	postupující
spojencům	spojenec	k1gMnPc3	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
dvě	dva	k4xCgFnPc1	dva
nové	nový	k2eAgFnPc1d1	nová
úderné	úderný	k2eAgFnPc1d1	úderná
armády	armáda	k1gFnPc1	armáda
<g/>
,	,	kIx,	,
Labská	labský	k2eAgFnSc1d1	Labská
a	a	k8xC	a
Mohanská	mohanský	k2eAgFnSc1d1	Mohanská
<g/>
,	,	kIx,	,
dohromady	dohromady	k6eAd1	dohromady
čítaly	čítat	k5eAaImAgInP	čítat
na	na	k7c4	na
202	[number]	k4	202
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
však	však	k9	však
byly	být	k5eAaImAgFnP	být
tím	ten	k3xDgNnSc7	ten
nejchatrnějším	chatrný	k2eAgNnSc7d3	chatrný
uskupením	uskupení	k1gNnSc7	uskupení
<g/>
,	,	kIx,	,
jakému	jaký	k3yQgInSc3	jaký
kdy	kdy	k6eAd1	kdy
velel	velet	k5eAaImAgMnS	velet
<g/>
.	.	kIx.	.
</s>
<s>
Mužstvo	mužstvo	k1gNnSc4	mužstvo
tvořili	tvořit	k5eAaImAgMnP	tvořit
převážně	převážně	k6eAd1	převážně
rekruti	rekrut	k1gMnPc1	rekrut
odvodových	odvodový	k2eAgInPc2d1	odvodový
ročníků	ročník	k1gInPc2	ročník
1815	[number]	k4	1815
<g/>
,	,	kIx,	,
dvacetiletí	dvacetiletý	k2eAgMnPc1d1	dvacetiletý
mladíci	mladík	k1gMnPc1	mladík
<g/>
,	,	kIx,	,
o	o	k7c6	o
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
Napoleon	Napoleon	k1gMnSc1	Napoleon
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k9	ještě
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Důstojníci	důstojník	k1gMnPc1	důstojník
a	a	k8xC	a
poddůstojníci	poddůstojník	k1gMnPc1	poddůstojník
byli	být	k5eAaImAgMnP	být
většinou	většina	k1gFnSc7	většina
postarší	postarší	k2eAgMnPc1d1	postarší
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
čerstvě	čerstvě	k6eAd1	čerstvě
povýšení	povýšení	k1gNnSc2	povýšení
a	a	k8xC	a
bez	bez	k7c2	bez
zkušeností	zkušenost	k1gFnPc2	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Nejhorší	zlý	k2eAgFnSc1d3	nejhorší
situace	situace	k1gFnSc1	situace
byla	být	k5eAaImAgFnS	být
u	u	k7c2	u
kavalérie	kavalérie	k1gFnSc2	kavalérie
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
mohl	moct	k5eAaImAgMnS	moct
francouzský	francouzský	k2eAgMnSc1d1	francouzský
císař	císař	k1gMnSc1	císař
počítat	počítat	k5eAaImF	počítat
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
15	[number]	k4	15
000	[number]	k4	000
koní	kůň	k1gMnPc2	kůň
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
polovina	polovina	k1gFnSc1	polovina
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
disponoval	disponovat	k5eAaBmAgMnS	disponovat
nepřítel	nepřítel	k1gMnSc1	nepřítel
(	(	kIx(	(
<g/>
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
bojů	boj	k1gInPc2	boj
stanulo	stanout	k5eAaPmAgNnS	stanout
proti	proti	k7c3	proti
24	[number]	k4	24
000	[number]	k4	000
spojeneckým	spojenecký	k2eAgInPc3d1	spojenecký
jezdcům	jezdec	k1gInPc3	jezdec
8000	[number]	k4	8000
kavaleristů	kavalerist	k1gMnPc2	kavalerist
francouzských	francouzský	k2eAgMnPc2d1	francouzský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s>
května	květen	k1gInSc2	květen
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
střetly	střetnout	k5eAaPmAgFnP	střetnout
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Lützenu	Lützen	k2eAgFnSc4d1	Lützen
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
po	po	k7c6	po
urputných	urputný	k2eAgInPc6d1	urputný
bojích	boj	k1gInPc6	boj
Francouzi	Francouz	k1gMnPc1	Francouz
přinutili	přinutit	k5eAaPmAgMnP	přinutit
spojence	spojenec	k1gMnPc4	spojenec
spořádaně	spořádaně	k6eAd1	spořádaně
se	se	k3xPyFc4	se
stáhnout	stáhnout	k5eAaPmF	stáhnout
z	z	k7c2	z
bojiště	bojiště	k1gNnSc2	bojiště
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ztratili	ztratit	k5eAaPmAgMnP	ztratit
na	na	k7c4	na
18	[number]	k4	18
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
(	(	kIx(	(
<g/>
nepřítel	nepřítel	k1gMnSc1	nepřítel
zhruba	zhruba	k6eAd1	zhruba
11	[number]	k4	11
500	[number]	k4	500
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
Napoleon	Napoleon	k1gMnSc1	Napoleon
získal	získat	k5eAaPmAgMnS	získat
zpět	zpět	k6eAd1	zpět
Drážďany	Drážďany	k1gInPc4	Drážďany
a	a	k8xC	a
mezi	mezi	k7c7	mezi
21	[number]	k4	21
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
květnem	květen	k1gInSc7	květen
napadl	napadnout	k5eAaPmAgMnS	napadnout
koaliční	koaliční	k2eAgFnSc2d1	koaliční
armády	armáda	k1gFnSc2	armáda
u	u	k7c2	u
Budyšína	Budyšín	k1gInSc2	Budyšín
<g/>
.	.	kIx.	.
</s>
<s>
Dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
dalšího	další	k2eAgNnSc2d1	další
krvavého	krvavý	k2eAgNnSc2d1	krvavé
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
přišel	přijít	k5eAaPmAgMnS	přijít
o	o	k7c4	o
22	[number]	k4	22
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Nepřítel	nepřítel	k1gMnSc1	nepřítel
opět	opět	k6eAd1	opět
spořádaně	spořádaně	k6eAd1	spořádaně
ustoupil	ustoupit	k5eAaPmAgMnS	ustoupit
<g/>
.	.	kIx.	.
<g/>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
francouzskému	francouzský	k2eAgMnSc3d1	francouzský
císaři	císař	k1gMnSc3	císař
podařilo	podařit	k5eAaPmAgNnS	podařit
zatlačit	zatlačit	k5eAaPmF	zatlačit
síly	síla	k1gFnPc4	síla
koalice	koalice	k1gFnSc2	koalice
o	o	k7c4	o
300	[number]	k4	300
kilometrů	kilometr	k1gInPc2	kilometr
zpět	zpět	k6eAd1	zpět
a	a	k8xC	a
vydobýt	vydobýt	k5eAaPmF	vydobýt
dvě	dva	k4xCgNnPc1	dva
vítězství	vítězství	k1gNnPc1	vítězství
<g/>
,	,	kIx,	,
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
s	s	k7c7	s
protivníky	protivník	k1gMnPc7	protivník
tzv.	tzv.	kA	tzv.
pläswitzskou	pläswitzský	k2eAgFnSc4d1	pläswitzský
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
příměří	příměří	k1gNnSc6	příměří
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1813	[number]	k4	1813
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
protáhlo	protáhnout	k5eAaPmAgNnS	protáhnout
až	až	k6eAd1	až
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
většina	většina	k1gFnSc1	většina
historiků	historik	k1gMnPc2	historik
tuto	tento	k3xDgFnSc4	tento
přestávku	přestávka	k1gFnSc4	přestávka
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
jako	jako	k9	jako
největší	veliký	k2eAgFnSc4d3	veliký
chybu	chyba	k1gFnSc4	chyba
Napoleonova	Napoleonův	k2eAgInSc2d1	Napoleonův
života	život	k1gInSc2	život
a	a	k8xC	a
odklon	odklon	k1gInSc1	odklon
od	od	k7c2	od
jím	on	k3xPp3gMnSc7	on
samým	samý	k3xTgMnSc7	samý
přijatých	přijatý	k2eAgFnPc2d1	přijatá
strategických	strategický	k2eAgFnPc2d1	strategická
zásad	zásada	k1gFnPc2	zásada
<g/>
.	.	kIx.	.
</s>
<s>
Spojenci	spojenec	k1gMnPc1	spojenec
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
vystupovalo	vystupovat	k5eAaImAgNnS	vystupovat
v	v	k7c6	v
roli	role	k1gFnSc6	role
ozbrojeného	ozbrojený	k2eAgMnSc2d1	ozbrojený
prostředníka	prostředník	k1gMnSc2	prostředník
<g/>
,	,	kIx,	,
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
mezičase	mezičas	k1gInSc6	mezičas
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
Napoleonovi	Napoleonův	k2eAgMnPc1d1	Napoleonův
mírové	mírový	k2eAgNnSc4d1	Mírové
ujednání	ujednání	k1gNnPc4	ujednání
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgMnSc1d1	francouzský
císař	císař	k1gMnSc1	císař
byl	být	k5eAaImAgMnS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
uzavřít	uzavřít	k5eAaPmF	uzavřít
mír	mír	k1gInSc4	mír
jedině	jedině	k6eAd1	jedině
na	na	k7c6	na
základě	základ	k1gInSc6	základ
status	status	k1gInSc4	status
quo	quo	k?	quo
ante	antat	k5eAaPmIp3nS	antat
bellum	bellum	k1gInSc4	bellum
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
druhou	druhý	k4xOgFnSc7	druhý
stranou	strana	k1gFnSc7	strana
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
byly	být	k5eAaImAgInP	být
žádány	žádat	k5eAaImNgInP	žádat
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
územní	územní	k2eAgInPc1d1	územní
ústupky	ústupek	k1gInPc1	ústupek
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
rokování	rokování	k1gNnSc1	rokování
tedy	tedy	k9	tedy
ztroskotalo	ztroskotat	k5eAaPmAgNnS	ztroskotat
a	a	k8xC	a
jediným	jediný	k2eAgInSc7d1	jediný
výsledkem	výsledek	k1gInSc7	výsledek
byl	být	k5eAaImAgInS	být
vstup	vstup	k1gInSc1	vstup
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
Švédska	Švédsko	k1gNnSc2	Švédsko
do	do	k7c2	do
řad	řada	k1gFnPc2	řada
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Sotva	sotva	k6eAd1	sotva
vypršelo	vypršet	k5eAaPmAgNnS	vypršet
příměří	příměří	k1gNnSc4	příměří
<g/>
,	,	kIx,	,
zahájil	zahájit	k5eAaPmAgMnS	zahájit
Napoleon	Napoleon	k1gMnSc1	Napoleon
válečné	válečný	k2eAgFnSc2d1	válečná
operace	operace	k1gFnSc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Poněvadž	poněvadž	k8xS	poněvadž
mu	on	k3xPp3gMnSc3	on
hlavní	hlavní	k2eAgNnSc1d1	hlavní
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
hrozilo	hrozit	k5eAaImAgNnS	hrozit
od	od	k7c2	od
České	český	k2eAgFnSc2d1	Česká
armády	armáda	k1gFnSc2	armáda
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
nového	nový	k2eAgMnSc2d1	nový
vrchního	vrchní	k2eAgMnSc2d1	vrchní
velitele	velitel	k1gMnSc2	velitel
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
sil	síla	k1gFnPc2	síla
knížete	kníže	k1gMnSc2	kníže
Schwarzenberga	Schwarzenberg	k1gMnSc2	Schwarzenberg
(	(	kIx(	(
<g/>
ten	ten	k3xDgMnSc1	ten
nahradil	nahradit	k5eAaPmAgMnS	nahradit
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
spojenců	spojenec	k1gMnPc2	spojenec
hraběte	hrabě	k1gMnSc2	hrabě
Wittgensteina	Wittgenstein	k1gMnSc2	Wittgenstein
<g/>
)	)	kIx)	)
blížící	blížící	k2eAgMnSc1d1	blížící
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
Krušnohoří	Krušnohoří	k1gNnSc4	Krušnohoří
k	k	k7c3	k
saské	saský	k2eAgFnSc3d1	saská
metropoli	metropol	k1gFnSc3	metropol
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
přetnout	přetnout	k5eAaPmF	přetnout
operační	operační	k2eAgFnSc2d1	operační
linie	linie	k1gFnSc2	linie
a	a	k8xC	a
napadnout	napadnout	k5eAaPmF	napadnout
ji	on	k3xPp3gFnSc4	on
z	z	k7c2	z
týlu	týl	k1gInSc2	týl
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
záměr	záměr	k1gInSc1	záměr
však	však	k9	však
nedokončil	dokončit	k5eNaPmAgInS	dokončit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
přispěchat	přispěchat	k5eAaPmF	přispěchat
ohroženému	ohrožený	k2eAgNnSc3d1	ohrožené
městu	město	k1gNnSc3	město
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
přímo	přímo	k6eAd1	přímo
a	a	k8xC	a
26	[number]	k4	26
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
došlo	dojít	k5eAaPmAgNnS	dojít
u	u	k7c2	u
Drážďan	Drážďany	k1gInPc2	Drážďany
k	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Spojenci	spojenec	k1gMnPc1	spojenec
utrpěli	utrpět	k5eAaPmAgMnP	utrpět
drtivou	drtivý	k2eAgFnSc4d1	drtivá
porážku	porážka	k1gFnSc4	porážka
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
ustupovat	ustupovat	k5eAaImF	ustupovat
k	k	k7c3	k
českým	český	k2eAgFnPc3d1	Česká
hranicím	hranice	k1gFnPc3	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pronásledování	pronásledování	k1gNnSc6	pronásledování
však	však	k9	však
Bonaparte	bonapart	k1gInSc5	bonapart
příliš	příliš	k6eAd1	příliš
vysunul	vysunout	k5eAaPmAgInS	vysunout
sbor	sbor	k1gInSc1	sbor
generála	generál	k1gMnSc2	generál
Vandamma	Vandamm	k1gMnSc2	Vandamm
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Chlumce	Chlumec	k1gInSc2	Chlumec
a	a	k8xC	a
Přestanova	Přestanův	k2eAgInSc2d1	Přestanův
obklíčen	obklíčit	k5eAaPmNgMnS	obklíčit
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
téměř	téměř	k6eAd1	téměř
přestal	přestat	k5eAaPmAgMnS	přestat
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
fatálně	fatálně	k6eAd1	fatálně
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dařilo	dařit	k5eAaImAgNnS	dařit
i	i	k9	i
jiným	jiný	k2eAgMnPc3d1	jiný
velitelům	velitel	k1gMnPc3	velitel
sborů	sbor	k1gInPc2	sbor
a	a	k8xC	a
Napoleonova	Napoleonův	k2eAgFnSc1d1	Napoleonova
kontrola	kontrola	k1gFnSc1	kontrola
nad	nad	k7c7	nad
tažením	tažení	k1gNnSc7	tažení
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
hroutit	hroutit	k5eAaImF	hroutit
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
zůstal	zůstat	k5eAaPmAgInS	zůstat
sevřen	sevřít	k5eAaPmNgInS	sevřít
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
stran	strana	k1gFnPc2	strana
<g/>
;	;	kIx,	;
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
jej	on	k3xPp3gInSc4	on
ohrožovala	ohrožovat	k5eAaImAgFnS	ohrožovat
Severní	severní	k2eAgFnSc1d1	severní
armáda	armáda	k1gFnSc1	armáda
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
jeho	on	k3xPp3gMnSc2	on
bývalého	bývalý	k2eAgMnSc2d1	bývalý
maršála	maršál	k1gMnSc2	maršál
Bernadotta	Bernadott	k1gMnSc2	Bernadott
<g/>
,	,	kIx,	,
nynějšího	nynější	k2eAgMnSc2d1	nynější
švédského	švédský	k2eAgMnSc2d1	švédský
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
z	z	k7c2	z
východu	východ	k1gInSc2	východ
Slezská	slezský	k2eAgFnSc1d1	Slezská
armáda	armáda	k1gFnSc1	armáda
generála	generál	k1gMnSc2	generál
Blüchera	Blücher	k1gMnSc2	Blücher
a	a	k8xC	a
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
Česká	český	k2eAgFnSc1d1	Česká
armáda	armáda	k1gFnSc1	armáda
knížete	kníže	k1gMnSc2	kníže
Schwarzenberga	Schwarzenberg	k1gMnSc2	Schwarzenberg
<g/>
.	.	kIx.	.
</s>
<s>
Rýnská	rýnský	k2eAgFnSc1d1	Rýnská
konfederace	konfederace	k1gFnSc1	konfederace
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vybudoval	vybudovat	k5eAaPmAgInS	vybudovat
na	na	k7c6	na
troskách	troska	k1gFnPc6	troska
římskoněmecké	římskoněmecký	k2eAgFnSc2d1	římskoněmecká
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
rozpadat	rozpadat	k5eAaImF	rozpadat
<g/>
,	,	kIx,	,
vojáci	voják	k1gMnPc1	voják
německých	německý	k2eAgInPc2d1	německý
kontingentů	kontingent	k1gInPc2	kontingent
odmítali	odmítat	k5eAaImAgMnP	odmítat
bojovat	bojovat	k5eAaImF	bojovat
pod	pod	k7c7	pod
francouzskými	francouzský	k2eAgInPc7d1	francouzský
prapory	prapor	k1gInPc7	prapor
a	a	k8xC	a
houfně	houfně	k6eAd1	houfně
dezertovali	dezertovat	k5eAaBmAgMnP	dezertovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sérii	série	k1gFnSc6	série
nevydařených	vydařený	k2eNgInPc2d1	nevydařený
pokusů	pokus	k1gInPc2	pokus
jednotlivé	jednotlivý	k2eAgFnSc2d1	jednotlivá
spojenecké	spojenecký	k2eAgFnSc2d1	spojenecká
armády	armáda	k1gFnSc2	armáda
porazit	porazit	k5eAaPmF	porazit
samostatně	samostatně	k6eAd1	samostatně
se	se	k3xPyFc4	se
Napoleon	napoleon	k1gInSc1	napoleon
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
vybudovat	vybudovat	k5eAaPmF	vybudovat
novou	nový	k2eAgFnSc4d1	nová
operační	operační	k2eAgFnSc4d1	operační
linii	linie	k1gFnSc4	linie
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
však	však	k9	však
nechybělo	chybět	k5eNaImAgNnS	chybět
mnoho	mnoho	k6eAd1	mnoho
a	a	k8xC	a
zůstal	zůstat	k5eAaPmAgInS	zůstat
uvězněn	uvěznit	k5eAaPmNgInS	uvěznit
v	v	k7c6	v
kleštích	kleště	k1gFnPc6	kleště
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
pokusili	pokusit	k5eAaPmAgMnP	pokusit
odříznout	odříznout	k5eAaPmF	odříznout
od	od	k7c2	od
zbytku	zbytek	k1gInSc2	zbytek
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Přesvědčován	přesvědčován	k2eAgInSc1d1	přesvědčován
svými	svůj	k3xOyFgMnPc7	svůj
maršály	maršál	k1gMnPc7	maršál
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
stáhl	stáhnout	k5eAaPmAgInS	stáhnout
až	až	k9	až
k	k	k7c3	k
přirozeným	přirozený	k2eAgFnPc3d1	přirozená
francouzským	francouzský	k2eAgFnPc3d1	francouzská
hranicím	hranice	k1gFnPc3	hranice
a	a	k8xC	a
tam	tam	k6eAd1	tam
soustředil	soustředit	k5eAaPmAgMnS	soustředit
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
přednější	přední	k2eAgMnPc4d2	přednější
uchovat	uchovat	k5eAaPmF	uchovat
si	se	k3xPyFc3	se
zbytek	zbytek	k1gInSc4	zbytek
politické	politický	k2eAgFnSc2d1	politická
prestiže	prestiž	k1gFnSc2	prestiž
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
vybojovat	vybojovat	k5eAaPmF	vybojovat
bitvu	bitva	k1gFnSc4	bitva
u	u	k7c2	u
Lipska	Lipsko	k1gNnSc2	Lipsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
třídenním	třídenní	k2eAgInSc6d1	třídenní
a	a	k8xC	a
současně	současně	k6eAd1	současně
největším	veliký	k2eAgInSc6d3	veliký
střetu	střet	k1gInSc6	střet
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
však	však	k9	však
Napoleon	Napoleon	k1gMnSc1	Napoleon
ztratil	ztratit	k5eAaPmAgMnS	ztratit
na	na	k7c4	na
70	[number]	k4	70
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
drtivou	drtivý	k2eAgFnSc4d1	drtivá
porážku	porážka	k1gFnSc4	porážka
<g/>
.	.	kIx.	.
</s>
<s>
Důsledky	důsledek	k1gInPc1	důsledek
tohoto	tento	k3xDgInSc2	tento
debaklu	debakl	k1gInSc2	debakl
jej	on	k3xPp3gInSc2	on
přinutily	přinutit	k5eAaPmAgInP	přinutit
ustoupit	ustoupit	k5eAaPmF	ustoupit
až	až	k9	až
za	za	k7c4	za
Rýn	Rýn	k1gInSc4	Rýn
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
válka	válka	k1gFnSc1	válka
přenesla	přenést	k5eAaPmAgFnS	přenést
na	na	k7c4	na
francouzské	francouzský	k2eAgNnSc4d1	francouzské
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mimo	mimo	k7c4	mimo
obrovské	obrovský	k2eAgFnPc4d1	obrovská
ztráty	ztráta	k1gFnPc4	ztráta
při	při	k7c6	při
tažení	tažení	k1gNnSc6	tažení
samotném	samotný	k2eAgNnSc6d1	samotné
Grande	grand	k1gMnSc5	grand
Armée	Armé	k1gInPc4	Armé
téměř	téměř	k6eAd1	téměř
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
zdecimovala	zdecimovat	k5eAaPmAgFnS	zdecimovat
epidemie	epidemie	k1gFnSc1	epidemie
tyfu	tyf	k1gInSc2	tyf
a	a	k8xC	a
Napoleon	Napoleon	k1gMnSc1	Napoleon
byl	být	k5eAaImAgMnS	být
nucen	nutit	k5eAaImNgMnS	nutit
opatřit	opatřit	k5eAaPmF	opatřit
jí	on	k3xPp3gFnSc3	on
další	další	k2eAgFnPc4d1	další
lidské	lidský	k2eAgFnPc4d1	lidská
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
kvůli	kvůli	k7c3	kvůli
novým	nový	k2eAgFnPc3d1	nová
daním	daň	k1gFnPc3	daň
a	a	k8xC	a
odvodům	odvod	k1gInPc3	odvod
se	se	k3xPyFc4	se
francouzské	francouzský	k2eAgNnSc1d1	francouzské
nadšení	nadšení	k1gNnSc1	nadšení
začalo	začít	k5eAaPmAgNnS	začít
pomalu	pomalu	k6eAd1	pomalu
vytrácet	vytrácet	k5eAaImF	vytrácet
a	a	k8xC	a
nastoupilo	nastoupit	k5eAaPmAgNnS	nastoupit
všeobecné	všeobecný	k2eAgNnSc1d1	všeobecné
volání	volání	k1gNnSc1	volání
po	po	k7c6	po
míru	mír	k1gInSc6	mír
<g/>
.	.	kIx.	.
</s>
<s>
Nabídku	nabídka	k1gFnSc4	nabídka
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
uzavření	uzavření	k1gNnSc3	uzavření
Napoleon	Napoleon	k1gMnSc1	Napoleon
obdržel	obdržet	k5eAaPmAgMnS	obdržet
ještě	ještě	k6eAd1	ještě
téhož	týž	k3xTgInSc2	týž
měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
ponechávala	ponechávat	k5eAaImAgFnS	ponechávat
mu	on	k3xPp3gMnSc3	on
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
i	i	k8xC	i
Porýní	Porýní	k1gNnPc4	Porýní
a	a	k8xC	a
Belgii	Belgie	k1gFnSc4	Belgie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
přijal	přijmout	k5eAaPmAgMnS	přijmout
ji	on	k3xPp3gFnSc4	on
až	až	k6eAd1	až
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
spojenci	spojenec	k1gMnPc1	spojenec
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
pro	pro	k7c4	pro
domácí	domácí	k2eAgFnSc4d1	domácí
válku	válka	k1gFnSc4	válka
shromáždit	shromáždit	k5eAaPmF	shromáždit
zhruba	zhruba	k6eAd1	zhruba
56	[number]	k4	56
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
však	však	k9	však
měli	mít	k5eAaImAgMnP	mít
čelit	čelit	k5eAaImF	čelit
čtyřnásobné	čtyřnásobný	k2eAgFnSc3d1	čtyřnásobná
přesile	přesila	k1gFnSc3	přesila
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
vojenské	vojenský	k2eAgFnPc1d1	vojenská
operace	operace	k1gFnPc1	operace
začaly	začít	k5eAaPmAgFnP	začít
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1813	[number]	k4	1813
<g/>
,	,	kIx,	,
když	když	k8xS	když
40	[number]	k4	40
000	[number]	k4	000
rakouských	rakouský	k2eAgMnPc2d1	rakouský
vojáků	voják	k1gMnPc2	voják
překročilo	překročit	k5eAaPmAgNnS	překročit
hranice	hranice	k1gFnPc4	hranice
vazalské	vazalský	k2eAgFnSc2d1	vazalská
Helvetské	helvetský	k2eAgFnSc2d1	helvetská
konfederace	konfederace	k1gFnSc2	konfederace
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gMnPc1	jejíž
obyvatelé	obyvatel	k1gMnPc1	obyvatel
i	i	k9	i
přes	přes	k7c4	přes
nepříznivý	příznivý	k2eNgInSc4d1	nepříznivý
politický	politický	k2eAgInSc4d1	politický
vývoj	vývoj	k1gInSc4	vývoj
zachovali	zachovat	k5eAaPmAgMnP	zachovat
věrnost	věrnost	k1gFnSc4	věrnost
Francii	Francie	k1gFnSc4	Francie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
armádě	armáda	k1gFnSc3	armáda
Napoleon	Napoleon	k1gMnSc1	Napoleon
odjel	odjet	k5eAaPmAgMnS	odjet
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
24	[number]	k4	24
<g/>
.	.	kIx.	.
na	na	k7c4	na
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1814	[number]	k4	1814
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c4	po
následující	následující	k2eAgInPc4d1	následující
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
jen	jen	k9	jen
oddálit	oddálit	k5eAaPmF	oddálit
nevyhnutelné	vyhnutelný	k2eNgNnSc4d1	nevyhnutelné
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
skvěle	skvěle	k6eAd1	skvěle
zvládnutými	zvládnutý	k2eAgInPc7d1	zvládnutý
manévry	manévr	k1gInPc7	manévr
schopen	schopen	k2eAgMnSc1d1	schopen
zasazovat	zasazovat	k5eAaImF	zasazovat
nepříteli	nepřítel	k1gMnSc3	nepřítel
rychlé	rychlý	k2eAgFnSc2d1	rychlá
porážky	porážka	k1gFnSc2	porážka
<g/>
,	,	kIx,	,
koaliční	koaliční	k2eAgNnPc4d1	koaliční
vojska	vojsko	k1gNnPc4	vojsko
jej	on	k3xPp3gInSc4	on
krok	krok	k1gInSc4	krok
za	za	k7c7	za
krokem	krok	k1gInSc7	krok
zatlačovala	zatlačovat	k5eAaImAgFnS	zatlačovat
k	k	k7c3	k
Paříži	Paříž	k1gFnSc3	Paříž
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgNnP	být
na	na	k7c4	na
konferenci	konference	k1gFnSc4	konference
v	v	k7c6	v
Châtillonu	Châtillon	k1gInSc6	Châtillon
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
–	–	k?	–
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g />
.	.	kIx.	.
</s>
<s>
1814	[number]	k4	1814
<g/>
)	)	kIx)	)
učiněna	učinit	k5eAaImNgFnS	učinit
ještě	ještě	k9	ještě
jedna	jeden	k4xCgFnSc1	jeden
mírová	mírový	k2eAgFnSc1d1	mírová
nabídka	nabídka	k1gFnSc1	nabídka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ponechávala	ponechávat	k5eAaImAgFnS	ponechávat
hranice	hranice	k1gFnPc4	hranice
Francie	Francie	k1gFnSc2	Francie
o	o	k7c6	o
stejné	stejný	k2eAgFnSc6d1	stejná
rozloze	rozloha	k1gFnSc6	rozloha
jako	jako	k9	jako
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1792	[number]	k4	1792
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
její	její	k3xOp3gNnSc4	její
přijetí	přijetí	k1gNnSc4	přijetí
císař	císař	k1gMnSc1	císař
Francouzů	Francouz	k1gMnPc2	Francouz
oddaloval	oddalovat	k5eAaImAgMnS	oddalovat
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
pozbyla	pozbýt	k5eAaPmAgFnS	pozbýt
platnosti	platnost	k1gFnSc2	platnost
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
připravil	připravit	k5eAaPmAgMnS	připravit
i	i	k9	i
o	o	k7c4	o
svou	svůj	k3xOyFgFnSc4	svůj
poslední	poslední	k2eAgFnSc4d1	poslední
šanci	šance	k1gFnSc4	šance
udržet	udržet	k5eAaPmF	udržet
se	se	k3xPyFc4	se
na	na	k7c6	na
trůnu	trůn	k1gInSc6	trůn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
dnech	den	k1gInPc6	den
pak	pak	k6eAd1	pak
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
těžké	těžký	k2eAgFnPc4d1	těžká
porážky	porážka	k1gFnPc4	porážka
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Laonu	Laon	k1gInSc2	Laon
a	a	k8xC	a
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Arcis-sur-Aube	Arcisur-Aub	k1gInSc5	Arcis-sur-Aub
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
nevzdával	vzdávat	k5eNaImAgMnS	vzdávat
a	a	k8xC	a
rychlým	rychlý	k2eAgNnSc7d1	rychlé
manévrováním	manévrování	k1gNnSc7	manévrování
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
nad	nad	k7c7	nad
protivníkem	protivník	k1gMnSc7	protivník
získat	získat	k5eAaPmF	získat
byť	byť	k8xS	byť
dočasnou	dočasný	k2eAgFnSc4d1	dočasná
výhodu	výhoda	k1gFnSc4	výhoda
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ničemu	nic	k3yNnSc3	nic
podobnému	podobný	k2eAgNnSc3d1	podobné
se	se	k3xPyFc4	se
však	však	k9	však
neschylovalo	schylovat	k5eNaImAgNnS	schylovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
spojenci	spojenec	k1gMnPc1	spojenec
po	po	k7c6	po
zadržení	zadržení	k1gNnSc6	zadržení
Napoleonovy	Napoleonův	k2eAgFnSc2d1	Napoleonova
korespondence	korespondence	k1gFnSc2	korespondence
změnili	změnit	k5eAaPmAgMnP	změnit
strategii	strategie	k1gFnSc4	strategie
<g/>
,	,	kIx,	,
zanechali	zanechat	k5eAaPmAgMnP	zanechat
honu	hon	k1gInSc6	hon
na	na	k7c4	na
hlavní	hlavní	k2eAgFnSc4d1	hlavní
císařskou	císařský	k2eAgFnSc4d1	císařská
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
začali	začít	k5eAaPmAgMnP	začít
si	se	k3xPyFc3	se
razit	razit	k5eAaImF	razit
cestu	cesta	k1gFnSc4	cesta
rovnou	rovnou	k6eAd1	rovnou
na	na	k7c4	na
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
metropoli	metropole	k1gFnSc4	metropole
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
jim	on	k3xPp3gMnPc3	on
maršál	maršál	k1gMnSc1	maršál
Marmont	Marmont	k1gMnSc1	Marmont
formálně	formálně	k6eAd1	formálně
předal	předat	k5eAaPmAgMnS	předat
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
Napoleon	Napoleon	k1gMnSc1	Napoleon
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
armádou	armáda	k1gFnSc7	armáda
přitáhl	přitáhnout	k5eAaPmAgInS	přitáhnout
k	k	k7c3	k
Fontainebleau	Fontainebleaum	k1gNnSc3	Fontainebleaum
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
chtěl	chtít	k5eAaImAgMnS	chtít
táhnout	táhnout	k5eAaImF	táhnout
Paříži	Paříž	k1gFnSc3	Paříž
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
před	před	k7c4	před
něj	on	k3xPp3gNnSc2	on
předstoupilo	předstoupit	k5eAaPmAgNnS	předstoupit
několik	několik	k4yIc1	několik
jeho	jeho	k3xOp3gMnPc2	jeho
maršálů	maršál	k1gMnPc2	maršál
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vyjádřili	vyjádřit	k5eAaPmAgMnP	vyjádřit
pochyby	pochyba	k1gFnPc4	pochyba
o	o	k7c6	o
dalším	další	k2eAgInSc6d1	další
odporu	odpor	k1gInSc6	odpor
a	a	k8xC	a
požadovali	požadovat	k5eAaImAgMnP	požadovat
jeho	jeho	k3xOp3gFnSc4	jeho
abdikaci	abdikace	k1gFnSc4	abdikace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
chvíli	chvíle	k1gFnSc6	chvíle
váhání	váhání	k1gNnPc2	váhání
císař	císař	k1gMnSc1	císař
uchopil	uchopit	k5eAaPmAgInS	uchopit
brk	brk	k1gInSc4	brk
a	a	k8xC	a
napsal	napsat	k5eAaPmAgMnS	napsat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Těmito	tento	k3xDgInPc7	tento
řádky	řádek	k1gInPc7	řádek
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
vzdal	vzdát	k5eAaPmAgMnS	vzdát
titulu	titul	k1gInSc2	titul
císaře	císař	k1gMnSc2	císař
a	a	k8xC	a
svým	svůj	k3xOyFgMnSc7	svůj
následníkem	následník	k1gMnSc7	následník
určil	určit	k5eAaPmAgMnS	určit
syna	syn	k1gMnSc4	syn
Napoleona	Napoleon	k1gMnSc4	Napoleon
Františka	František	k1gMnSc4	František
<g/>
.	.	kIx.	.
</s>
<s>
Car	car	k1gMnSc1	car
Alexandr	Alexandr	k1gMnSc1	Alexandr
se	se	k3xPyFc4	se
však	však	k9	však
postavil	postavit	k5eAaPmAgMnS	postavit
proti	proti	k7c3	proti
regentství	regentství	k1gNnSc4	regentství
Marie-Louisy	Marie-Louis	k1gInPc4	Marie-Louis
a	a	k8xC	a
požadoval	požadovat	k5eAaImAgMnS	požadovat
bezpodmínečnou	bezpodmínečný	k2eAgFnSc4d1	bezpodmínečná
abdikaci	abdikace	k1gFnSc4	abdikace
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
excísař	excísař	k1gMnSc1	excísař
vzdá	vzdát	k5eAaPmIp3nS	vzdát
trůnu	trůn	k1gInSc2	trůn
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
dynastii	dynastie	k1gFnSc4	dynastie
Bonapartů	bonapart	k1gInPc2	bonapart
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
Napoleon	napoleon	k1gInSc1	napoleon
přistoupil	přistoupit	k5eAaPmAgInS	přistoupit
i	i	k9	i
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
podmínku	podmínka	k1gFnSc4	podmínka
a	a	k8xC	a
nadále	nadále	k6eAd1	nadále
setrval	setrvat	k5eAaPmAgMnS	setrvat
v	v	k7c6	v
zámku	zámek	k1gInSc6	zámek
ve	v	k7c6	v
Fontainebleau	Fontainebleaus	k1gInSc6	Fontainebleaus
obklopen	obklopen	k2eAgInSc1d1	obklopen
posledními	poslední	k2eAgInPc7d1	poslední
věrnými	věrný	k2eAgInPc7d1	věrný
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
jedem	jed	k1gInSc7	jed
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
sebou	se	k3xPyFc7	se
nosil	nosit	k5eAaImAgInS	nosit
od	od	k7c2	od
ruského	ruský	k2eAgNnSc2d1	ruské
tažení	tažení	k1gNnSc2	tažení
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
smrtící	smrtící	k2eAgFnSc1d1	smrtící
látka	látka	k1gFnSc1	látka
neúčinkovala	účinkovat	k5eNaImAgFnS	účinkovat
a	a	k8xC	a
vyzvracel	vyzvracet	k5eAaPmAgMnS	vyzvracet
ji	on	k3xPp3gFnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
osm	osm	k4xCc4	osm
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rozloučil	rozloučit	k5eAaPmAgInS	rozloučit
s	s	k7c7	s
vojáky	voják	k1gMnPc7	voják
gardy	garda	k1gFnSc2	garda
a	a	k8xC	a
doprovázen	doprovázen	k2eAgMnSc1d1	doprovázen
komisaři	komisar	k1gMnPc1	komisar
vítězných	vítězný	k2eAgFnPc2d1	vítězná
mocností	mocnost	k1gFnPc2	mocnost
odjel	odjet	k5eAaPmAgInS	odjet
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Elba	Elb	k1gInSc2	Elb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vojenství	vojenství	k1gNnSc2	vojenství
===	===	k?	===
</s>
</p>
<p>
<s>
Napoleon	napoleon	k1gInSc1	napoleon
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
válečnictví	válečnictví	k1gNnSc2	válečnictví
zanechal	zanechat	k5eAaPmAgMnS	zanechat
stopu	stopa	k1gFnSc4	stopa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
promítala	promítat	k5eAaImAgFnS	promítat
do	do	k7c2	do
vojenské	vojenský	k2eAgFnSc2d1	vojenská
teorie	teorie	k1gFnSc2	teorie
i	i	k8xC	i
praxe	praxe	k1gFnSc2	praxe
ještě	ještě	k9	ještě
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
pádu	pád	k1gInSc6	pád
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgMnS	být
autorem	autor	k1gMnSc7	autor
mnohých	mnohý	k2eAgFnPc2d1	mnohá
pouček	poučka	k1gFnPc2	poučka
a	a	k8xC	a
definic	definice	k1gFnPc2	definice
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
jimi	on	k3xPp3gMnPc7	on
sám	sám	k3xTgMnSc1	sám
doslova	doslova	k6eAd1	doslova
řídil	řídit	k5eAaImAgMnS	řídit
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
uzavřením	uzavření	k1gNnSc7	uzavření
pläswitzské	pläswitzský	k2eAgFnSc2d1	pläswitzský
dohody	dohoda	k1gFnSc2	dohoda
o	o	k7c6	o
příměří	příměří	k1gNnSc6	příměří
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Budyšína	Budyšín	k1gInSc2	Budyšín
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1813	[number]	k4	1813
vzalo	vzít	k5eAaPmAgNnS	vzít
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
ofenzivním	ofenzivní	k2eAgNnSc7d1	ofenzivní
myšlením	myšlení	k1gNnSc7	myšlení
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Začínej	začínat	k5eAaImRp2nS	začínat
tažení	tažení	k1gNnSc4	tažení
po	po	k7c6	po
zralé	zralý	k2eAgFnSc6d1	zralá
úvaze	úvaha	k1gFnSc6	úvaha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
když	když	k8xS	když
je	on	k3xPp3gInPc4	on
začneš	začít	k5eAaPmIp2nS	začít
<g/>
,	,	kIx,	,
bojuj	bojovat	k5eAaImRp2nS	bojovat
do	do	k7c2	do
krajnosti	krajnost	k1gFnSc2	krajnost
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
sis	sis	k?	sis
nedal	dát	k5eNaPmAgMnS	dát
iniciativu	iniciativa	k1gFnSc4	iniciativa
vyrvat	vyrvat	k5eAaPmF	vyrvat
z	z	k7c2	z
rukou	ruka	k1gFnPc2	ruka
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Své	svůj	k3xOyFgFnPc4	svůj
myšlenky	myšlenka	k1gFnPc4	myšlenka
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
oboru	obor	k1gInSc6	obor
nikdy	nikdy	k6eAd1	nikdy
systematicky	systematicky	k6eAd1	systematicky
nesepsal	sepsat	k5eNaPmAgMnS	sepsat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
poznámek	poznámka	k1gFnPc2	poznámka
bylo	být	k5eAaImAgNnS	být
vyňato	vynít	k5eAaPmNgNnS	vynít
zhruba	zhruba	k6eAd1	zhruba
115	[number]	k4	115
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
víceméně	víceméně	k9	víceméně
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
většinu	většina	k1gFnSc4	většina
jeho	jeho	k3xOp3gFnPc2	jeho
základních	základní	k2eAgFnPc2d1	základní
úvah	úvaha	k1gFnPc2	úvaha
o	o	k7c6	o
strategii	strategie	k1gFnSc6	strategie
a	a	k8xC	a
taktice	taktika	k1gFnSc6	taktika
<g/>
.	.	kIx.	.
<g/>
Napoleonova	Napoleonův	k2eAgFnSc1d1	Napoleonova
éra	éra	k1gFnSc1	éra
byla	být	k5eAaImAgFnS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
nasazováním	nasazování	k1gNnSc7	nasazování
obrovských	obrovský	k2eAgInPc2d1	obrovský
počtů	počet	k1gInPc2	počet
vojsk	vojsko	k1gNnPc2	vojsko
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
proto	proto	k8xC	proto
nepřestával	přestávat	k5eNaImAgMnS	přestávat
opakovat	opakovat	k5eAaImF	opakovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
masy	masa	k1gFnPc1	masa
rozhodují	rozhodovat	k5eAaImIp3nP	rozhodovat
vše	všechen	k3xTgNnSc4	všechen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
víře	víra	k1gFnSc6	víra
definoval	definovat	k5eAaBmAgMnS	definovat
vojevůdcovské	vojevůdcovský	k2eAgNnSc4d1	vojevůdcovský
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
podle	podle	k7c2	podle
něj	on	k3xPp3gMnSc2	on
předně	předně	k6eAd1	předně
spočívalo	spočívat	k5eAaImAgNnS	spočívat
v	v	k7c6	v
dovednosti	dovednost	k1gFnSc6	dovednost
vojevůdce	vojevůdce	k1gMnSc2	vojevůdce
sehnat	sehnat	k5eAaPmF	sehnat
<g/>
,	,	kIx,	,
vyzbrojit	vyzbrojit	k5eAaPmF	vyzbrojit
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
vycvičit	vycvičit	k5eAaPmF	vycvičit
velké	velký	k2eAgInPc4d1	velký
prapory	prapor	k1gInPc4	prapor
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
masové	masový	k2eAgFnSc2d1	masová
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gInPc4	on
měl	mít	k5eAaImAgMnS	mít
ve	v	k7c4	v
chvíli	chvíle	k1gFnSc4	chvíle
rozhodujícího	rozhodující	k2eAgInSc2d1	rozhodující
úderu	úder	k1gInSc2	úder
soustředěny	soustředit	k5eAaPmNgFnP	soustředit
na	na	k7c6	na
potřebném	potřebný	k2eAgNnSc6d1	potřebné
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jich	on	k3xPp3gMnPc2	on
<g />
.	.	kIx.	.
</s>
<s>
dovedl	dovést	k5eAaPmAgMnS	dovést
nešetřit	šetřit	k5eNaImF	šetřit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
to	ten	k3xDgNnSc1	ten
k	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
nutné	nutný	k2eAgNnSc4d1	nutné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nevyhýbal	vyhýbat	k5eNaImAgMnS	vyhýbat
bitvě	bitva	k1gFnSc3	bitva
a	a	k8xC	a
neodkládal	odkládat	k5eNaImAgMnS	odkládat
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
své	svůj	k3xOyFgFnPc1	svůj
síly	síla	k1gFnPc1	síla
soustředěny	soustředit	k5eAaPmNgFnP	soustředit
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
aby	aby	kYmCp3nS	aby
hledal	hledat	k5eAaImAgInS	hledat
co	co	k9	co
nejrychlejší	rychlý	k2eAgNnSc4d3	nejrychlejší
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
nějaké	nějaký	k3yIgFnPc4	nějaký
vyhlídky	vyhlídka	k1gFnPc4	vyhlídka
na	na	k7c4	na
vítězství	vítězství	k1gNnSc4	vítězství
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
aby	aby	kYmCp3nS	aby
nalezl	naleznout	k5eAaPmAgMnS	naleznout
v	v	k7c6	v
nepřátelské	přátelský	k2eNgFnSc6d1	nepřátelská
sestavě	sestava	k1gFnSc6	sestava
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
němuž	jenž	k3xRgInSc3	jenž
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
vést	vést	k5eAaImF	vést
rozhodující	rozhodující	k2eAgInSc4d1	rozhodující
úder	úder	k1gInSc4	úder
<g/>
.	.	kIx.	.
</s>
<s>
Úlohu	úloha	k1gFnSc4	úloha
štěstí	štěstí	k1gNnSc2	štěstí
a	a	k8xC	a
náhody	náhoda	k1gFnSc2	náhoda
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
Napoleon	Napoleon	k1gMnSc1	Napoleon
nepopíral	popírat	k5eNaImAgMnS	popírat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
spíš	spíš	k9	spíš
zastával	zastávat	k5eAaImAgMnS	zastávat
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
více	hodně	k6eAd2	hodně
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
vojevůdcových	vojevůdcův	k2eAgFnPc6d1	vojevůdcova
schopnostech	schopnost	k1gFnPc6	schopnost
<g/>
,	,	kIx,	,
intelektu	intelekt	k1gInSc6	intelekt
<g/>
,	,	kIx,	,
vědomostech	vědomost	k1gFnPc6	vědomost
<g/>
,	,	kIx,	,
pohotovosti	pohotovost	k1gFnPc1	pohotovost
<g/>
,	,	kIx,	,
vynalézavosti	vynalézavost	k1gFnPc1	vynalézavost
a	a	k8xC	a
schopnosti	schopnost	k1gFnPc1	schopnost
k	k	k7c3	k
metodickým	metodický	k2eAgFnPc3d1	metodická
operacím	operace	k1gFnPc3	operace
<g/>
.	.	kIx.	.
<g/>
Válka	válka	k1gFnSc1	válka
samotná	samotný	k2eAgFnSc1d1	samotná
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
podle	podle	k7c2	podle
Napoleona	Napoleon	k1gMnSc2	Napoleon
metodická	metodický	k2eAgFnSc1d1	metodická
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
hluboce	hluboko	k6eAd1	hluboko
promyšlená	promyšlený	k2eAgFnSc1d1	promyšlená
a	a	k8xC	a
jedině	jedině	k6eAd1	jedině
tehdy	tehdy	k6eAd1	tehdy
mohla	moct	k5eAaImAgFnS	moct
mít	mít	k5eAaImF	mít
naději	naděje	k1gFnSc4	naděje
na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
rozuměl	rozumět	k5eAaImAgMnS	rozumět
mapě	mapa	k1gFnSc3	mapa
<g/>
,	,	kIx,	,
uměl	umět	k5eAaImAgMnS	umět
ji	on	k3xPp3gFnSc4	on
užívat	užívat	k5eAaImF	užívat
jako	jako	k8xS	jako
nikdo	nikdo	k3yNnSc1	nikdo
jiný	jiný	k2eAgMnSc1d1	jiný
a	a	k8xC	a
dokázal	dokázat	k5eAaPmAgMnS	dokázat
proto	proto	k8xC	proto
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
počátkem	počátek	k1gInSc7	počátek
samotného	samotný	k2eAgNnSc2d1	samotné
tažení	tažení	k1gNnSc2	tažení
důkladně	důkladně	k6eAd1	důkladně
naplánovat	naplánovat	k5eAaBmF	naplánovat
pohyby	pohyb	k1gInPc4	pohyb
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
sborů	sbor	k1gInPc2	sbor
<g/>
,	,	kIx,	,
trasy	trasa	k1gFnSc2	trasa
zásobovacích	zásobovací	k2eAgFnPc2d1	zásobovací
linií	linie	k1gFnPc2	linie
i	i	k9	i
odhadovaný	odhadovaný	k2eAgInSc4d1	odhadovaný
postup	postup	k1gInSc4	postup
nepřítele	nepřítel	k1gMnSc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
vyhrát	vyhrát	k5eAaPmF	vyhrát
tažení	tažení	k1gNnSc4	tažení
už	už	k9	už
před	před	k7c7	před
první	první	k4xOgFnSc7	první
bitvou	bitva	k1gFnSc7	bitva
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
protivníkem	protivník	k1gMnSc7	protivník
<g/>
,	,	kIx,	,
pokusil	pokusit	k5eAaPmAgMnS	pokusit
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
za	za	k7c4	za
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
výhodných	výhodný	k2eAgFnPc2d1	výhodná
podmínek	podmínka	k1gFnPc2	podmínka
vmanévrovat	vmanévrovat	k5eAaPmF	vmanévrovat
do	do	k7c2	do
rozhodující	rozhodující	k2eAgFnSc2d1	rozhodující
bitvy	bitva	k1gFnSc2	bitva
a	a	k8xC	a
zničit	zničit	k5eAaPmF	zničit
živou	živý	k2eAgFnSc4d1	živá
sílu	síla	k1gFnSc4	síla
jeho	jeho	k3xOp3gFnPc2	jeho
polních	polní	k2eAgFnPc2d1	polní
armád	armáda	k1gFnPc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
obsazováním	obsazování	k1gNnSc7	obsazování
nejdůležitějších	důležitý	k2eAgNnPc2d3	nejdůležitější
strategických	strategický	k2eAgNnPc2d1	strategické
a	a	k8xC	a
politických	politický	k2eAgNnPc2d1	politické
středisek	středisko	k1gNnPc2	středisko
okupované	okupovaný	k2eAgFnSc2d1	okupovaná
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
asi	asi	k9	asi
nejzásadnější	zásadní	k2eAgFnSc4d3	nejzásadnější
koncepci	koncepce	k1gFnSc4	koncepce
pro	pro	k7c4	pro
dosažení	dosažení	k1gNnSc4	dosažení
výhodných	výhodný	k2eAgFnPc2d1	výhodná
podmínek	podmínka	k1gFnPc2	podmínka
převzal	převzít	k5eAaPmAgInS	převzít
od	od	k7c2	od
francouzského	francouzský	k2eAgMnSc2d1	francouzský
důstojníka	důstojník	k1gMnSc4	důstojník
jménem	jméno	k1gNnSc7	jméno
Pierre-Joseph	Pierre-Joseph	k1gMnSc1	Pierre-Joseph
de	de	k?	de
Bourcet	Bourcet	k1gMnSc4	Bourcet
a	a	k8xC	a
vlastním	vlastní	k2eAgNnSc7d1	vlastní
přičiněním	přičinění	k1gNnSc7	přičinění
ji	on	k3xPp3gFnSc4	on
dovedl	dovést	k5eAaPmAgMnS	dovést
k	k	k7c3	k
dokonalosti	dokonalost	k1gFnSc3	dokonalost
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
koordinované	koordinovaný	k2eAgInPc4d1	koordinovaný
pohyby	pohyb	k1gInPc4	pohyb
několika	několik	k4yIc2	několik
armádních	armádní	k2eAgInPc2d1	armádní
sborů	sbor	k1gInPc2	sbor
(	(	kIx(	(
<g/>
batallion	batallion	k1gInSc4	batallion
carré	carrý	k2eAgFnPc1d1	carrý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
samostatně	samostatně	k6eAd1	samostatně
operovaly	operovat	k5eAaImAgFnP	operovat
na	na	k7c6	na
širokém	široký	k2eAgNnSc6d1	široké
území	území	k1gNnSc6	území
a	a	k8xC	a
znemožňovaly	znemožňovat	k5eAaImAgFnP	znemožňovat
nepříteli	nepřítel	k1gMnSc3	nepřítel
odhadnout	odhadnout	k5eAaPmF	odhadnout
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
směřuje	směřovat	k5eAaImIp3nS	směřovat
Napoleonův	Napoleonův	k2eAgInSc4d1	Napoleonův
hlavní	hlavní	k2eAgInSc4d1	hlavní
úder	úder	k1gInSc4	úder
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
sbory	sbor	k1gInPc1	sbor
byly	být	k5eAaImAgInP	být
složeny	složit	k5eAaPmNgInP	složit
z	z	k7c2	z
několika	několik	k4yIc2	několik
divizí	divize	k1gFnPc2	divize
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
disponovala	disponovat	k5eAaBmAgFnS	disponovat
všemi	všecek	k3xTgInPc7	všecek
druhy	druh	k1gInPc7	druh
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
byl	být	k5eAaImAgInS	být
každý	každý	k3xTgInSc1	každý
sbor	sbor	k1gInSc1	sbor
schopen	schopen	k2eAgInSc1d1	schopen
čelit	čelit	k5eAaImF	čelit
den	den	k1gInSc4	den
nebo	nebo	k8xC	nebo
dva	dva	k4xCgInPc4	dva
nepřátelskému	přátelský	k2eNgInSc3d1	nepřátelský
náporu	nápor	k1gInSc3	nápor
a	a	k8xC	a
poskytnout	poskytnout	k5eAaPmF	poskytnout
tak	tak	k6eAd1	tak
čas	čas	k1gInSc4	čas
ostatním	ostatní	k2eAgFnPc3d1	ostatní
kolonám	kolona	k1gFnPc3	kolona
armády	armáda	k1gFnSc2	armáda
sevřít	sevřít	k5eAaPmF	sevřít
protivníka	protivník	k1gMnSc4	protivník
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kdykoliv	kdykoliv	k6eAd1	kdykoliv
se	se	k3xPyFc4	se
Napoleonovi	Napoleonův	k2eAgMnPc1d1	Napoleonův
naskytla	naskytnout	k5eAaPmAgFnS	naskytnout
vhodná	vhodný	k2eAgFnSc1d1	vhodná
příležitost	příležitost	k1gFnSc1	příležitost
<g/>
,	,	kIx,	,
zkombinoval	zkombinovat	k5eAaPmAgMnS	zkombinovat
rychlý	rychlý	k2eAgInSc4d1	rychlý
přesun	přesun	k1gInSc4	přesun
s	s	k7c7	s
klamnými	klamný	k2eAgInPc7d1	klamný
manévry	manévr	k1gInPc7	manévr
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
obešel	obejít	k5eAaPmAgMnS	obejít
křídla	křídlo	k1gNnPc4	křídlo
nepřátelské	přátelský	k2eNgFnSc2d1	nepřátelská
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
přiblížil	přiblížit	k5eAaPmAgMnS	přiblížit
se	se	k3xPyFc4	se
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
komunikační	komunikační	k2eAgFnSc3d1	komunikační
linii	linie	k1gFnSc3	linie
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
stočil	stočit	k5eAaPmAgMnS	stočit
a	a	k8xC	a
přiměl	přimět	k5eAaPmAgMnS	přimět
protivníka	protivník	k1gMnSc4	protivník
k	k	k7c3	k
boji	boj	k1gInSc3	boj
v	v	k7c6	v
nevýhodném	výhodný	k2eNgNnSc6d1	nevýhodné
postavení	postavení	k1gNnSc6	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
obchvat	obchvat	k1gInSc1	obchvat
nepřátelské	přátelský	k2eNgFnSc2d1	nepřátelská
armády	armáda	k1gFnSc2	armáda
má	mít	k5eAaImIp3nS	mít
smysl	smysl	k1gInSc4	smysl
jen	jen	k9	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
protivníkova	protivníkův	k2eAgInSc2d1	protivníkův
týlu	týl	k1gInSc2	týl
a	a	k8xC	a
když	když	k8xS	když
tento	tento	k3xDgInSc1	tento
obchvat	obchvat	k1gInSc1	obchvat
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
zasáhnou	zasáhnout	k5eAaPmIp3nP	zasáhnout
i	i	k9	i
obchvatná	obchvatný	k2eAgNnPc4d1	obchvatný
vojska	vojsko	k1gNnPc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
proslavený	proslavený	k2eAgMnSc1d1	proslavený
Manœ	Manœ	k1gMnSc1	Manœ
sur	sur	k?	sur
les	les	k1gInSc1	les
derriè	derriè	k?	derriè
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
Napoleon	Napoleon	k1gMnSc1	Napoleon
využil	využít	k5eAaPmAgMnS	využít
třicetkrát	třicetkrát	k6eAd1	třicetkrát
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgInSc2	tento
útočného	útočný	k2eAgInSc2d1	útočný
prvku	prvek	k1gInSc2	prvek
využíval	využívat	k5eAaImAgInS	využívat
na	na	k7c6	na
několikakilometrové	několikakilometrový	k2eAgFnSc6d1	několikakilometrová
frontě	fronta	k1gFnSc6	fronta
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
podstata	podstata	k1gFnSc1	podstata
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
silné	silný	k2eAgNnSc4d1	silné
vojsko	vojsko	k1gNnSc4	vojsko
bojem	boj	k1gInSc7	boj
či	či	k8xC	či
hrozbou	hrozba	k1gFnSc7	hrozba
útoku	útok	k1gInSc2	útok
vázalo	vázat	k5eAaImAgNnS	vázat
hlavní	hlavní	k2eAgFnSc4d1	hlavní
nepřátelskou	přátelský	k2eNgFnSc4d1	nepřátelská
linii	linie	k1gFnSc4	linie
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
silná	silný	k2eAgFnSc1d1	silná
kolona	kolona	k1gFnSc1	kolona
pak	pak	k6eAd1	pak
pronikla	proniknout	k5eAaPmAgFnS	proniknout
podél	podél	k7c2	podél
protivníkova	protivníkův	k2eAgNnSc2d1	protivníkovo
křídla	křídlo	k1gNnSc2	křídlo
až	až	k9	až
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
týlu	týl	k1gInSc3	týl
<g/>
,	,	kIx,	,
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
strategickou	strategický	k2eAgFnSc4d1	strategická
přehradu	přehrada	k1gFnSc4	přehrada
nebo	nebo	k8xC	nebo
bariéru	bariéra	k1gFnSc4	bariéra
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
zásobovací	zásobovací	k2eAgFnSc6d1	zásobovací
a	a	k8xC	a
ústupové	ústupový	k2eAgFnSc6d1	ústupová
linii	linie	k1gFnSc6	linie
<g/>
,	,	kIx,	,
a	a	k8xC	a
přinutila	přinutit	k5eAaPmAgFnS	přinutit
jej	on	k3xPp3gMnSc4	on
k	k	k7c3	k
ústupu	ústup	k1gInSc3	ústup
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
byl	být	k5eAaImAgInS	být
manévr	manévr	k1gInSc1	manévr
proveden	provést	k5eAaPmNgInS	provést
včas	včas	k6eAd1	včas
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgInS	moct
vést	vést	k5eAaImF	vést
k	k	k7c3	k
protivníkově	protivníkův	k2eAgFnSc3d1	protivníkova
porážce	porážka	k1gFnSc3	porážka
či	či	k8xC	či
naprostému	naprostý	k2eAgNnSc3d1	naprosté
zničení	zničení	k1gNnSc3	zničení
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prvních	první	k4xOgFnPc6	první
svých	svůj	k3xOyFgFnPc6	svůj
válkách	válka	k1gFnPc6	válka
Napoleon	napoleon	k1gInSc4	napoleon
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
taktiku	taktika	k1gFnSc4	taktika
armád	armáda	k1gFnPc2	armáda
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
vysílal	vysílat	k5eAaImAgInS	vysílat
kupředu	kupředu	k6eAd1	kupředu
pohyblivé	pohyblivý	k2eAgFnPc1d1	pohyblivá
linie	linie	k1gFnPc1	linie
střelců	střelec	k1gMnPc2	střelec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
připravovali	připravovat	k5eAaImAgMnP	připravovat
hlavní	hlavní	k2eAgInSc4d1	hlavní
úder	úder	k1gInSc4	úder
a	a	k8xC	a
uvolňovali	uvolňovat	k5eAaImAgMnP	uvolňovat
cestu	cesta	k1gFnSc4	cesta
útočným	útočný	k2eAgFnPc3d1	útočná
kolonám	kolona	k1gFnPc3	kolona
<g/>
.	.	kIx.	.
</s>
<s>
Obecné	obecný	k2eAgFnSc2d1	obecná
veřejnosti	veřejnost	k1gFnSc2	veřejnost
asi	asi	k9	asi
nejznámější	známý	k2eAgFnSc4d3	nejznámější
techniku	technika	k1gFnSc4	technika
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
úder	úder	k1gInSc1	úder
taranem	taran	k1gInSc7	taran
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
útok	útok	k1gInSc1	útok
obrovské	obrovský	k2eAgFnSc2d1	obrovská
masy	masa	k1gFnSc2	masa
pěchoty	pěchota	k1gFnSc2	pěchota
postupující	postupující	k2eAgFnSc2d1	postupující
v	v	k7c6	v
sevřených	sevřený	k2eAgFnPc6d1	sevřená
řadách	řada	k1gFnPc6	řada
se	s	k7c7	s
záměrem	záměr	k1gInSc7	záměr
prorazit	prorazit	k5eAaPmF	prorazit
střed	střed	k1gInSc4	střed
nepřátelského	přátelský	k2eNgNnSc2d1	nepřátelské
postavení	postavení	k1gNnSc2	postavení
<g/>
,	,	kIx,	,
Bonaparte	bonapart	k1gInSc5	bonapart
poprvé	poprvé	k6eAd1	poprvé
použil	použít	k5eAaPmAgMnS	použít
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Wagramu	Wagram	k1gInSc2	Wagram
<g/>
.	.	kIx.	.
<g/>
Velice	velice	k6eAd1	velice
důsledně	důsledně	k6eAd1	důsledně
se	se	k3xPyFc4	se
Napoleon	Napoleon	k1gMnSc1	Napoleon
věnoval	věnovat	k5eAaPmAgMnS	věnovat
také	také	k9	také
morálce	morálka	k1gFnSc3	morálka
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Ponechal	ponechat	k5eAaPmAgMnS	ponechat
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
zákaz	zákaz	k1gInSc4	zákaz
používání	používání	k1gNnSc2	používání
tělesných	tělesný	k2eAgInPc2d1	tělesný
trestů	trest	k1gInPc2	trest
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jak	jak	k6eAd1	jak
pravil	pravit	k5eAaBmAgMnS	pravit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Co	co	k3yRnSc1	co
možno	možno	k6eAd1	možno
očekávat	očekávat	k5eAaImF	očekávat
od	od	k7c2	od
zneuctěných	zneuctěný	k2eAgMnPc2d1	zneuctěný
lidí	člověk	k1gMnPc2	člověk
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
a	a	k8xC	a
namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
apelovat	apelovat	k5eAaImF	apelovat
na	na	k7c4	na
čest	čest	k1gFnSc4	čest
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
svolával	svolávat	k5eAaImAgMnS	svolávat
vojáky	voják	k1gMnPc4	voják
i	i	k8xC	i
důstojníky	důstojník	k1gMnPc4	důstojník
a	a	k8xC	a
dotazoval	dotazovat	k5eAaImAgMnS	dotazovat
se	se	k3xPyFc4	se
jich	on	k3xPp3gNnPc2	on
na	na	k7c4	na
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
se	se	k3xPyFc4	se
nejvíc	nejvíc	k6eAd1	nejvíc
vyznamenali	vyznamenat	k5eAaPmAgMnP	vyznamenat
<g/>
.	.	kIx.	.
</s>
<s>
Těm	ten	k3xDgMnPc3	ten
pak	pak	k6eAd1	pak
buď	buď	k8xC	buď
udělil	udělit	k5eAaPmAgInS	udělit
vyšší	vysoký	k2eAgFnSc4d2	vyšší
hodnost	hodnost	k1gFnSc4	hodnost
<g/>
,	,	kIx,	,
řád	řád	k1gInSc4	řád
či	či	k8xC	či
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
se	se	k3xPyFc4	se
spíš	spíš	k9	spíš
spoléhal	spoléhat	k5eAaImAgMnS	spoléhat
na	na	k7c4	na
odměny	odměna	k1gFnPc4	odměna
než	než	k8xS	než
na	na	k7c4	na
tresty	trest	k1gInPc4	trest
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
pokud	pokud	k8xS	pokud
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c4	na
těžká	těžký	k2eAgNnPc4d1	těžké
provinění	provinění	k1gNnPc4	provinění
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
provinilci	provinilec	k1gMnPc1	provinilec
obvykle	obvykle	k6eAd1	obvykle
rychle	rychle	k6eAd1	rychle
odsouzeni	odsoudit	k5eAaPmNgMnP	odsoudit
k	k	k7c3	k
popravě	poprava	k1gFnSc3	poprava
<g/>
.	.	kIx.	.
</s>
<s>
Pocitu	pocit	k1gInSc3	pocit
sounáležitosti	sounáležitost	k1gFnSc2	sounáležitost
s	s	k7c7	s
vlastí	vlast	k1gFnSc7	vlast
<g/>
,	,	kIx,	,
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
jednotkou	jednotka	k1gFnSc7	jednotka
dosahoval	dosahovat	k5eAaImAgMnS	dosahovat
několika	několik	k4yIc2	několik
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Proklamacemi	proklamace	k1gFnPc7	proklamace
<g/>
,	,	kIx,	,
osobním	osobní	k2eAgInSc7d1	osobní
příkladem	příklad	k1gInSc7	příklad
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
sám	sám	k3xTgMnSc1	sám
ochoten	ochoten	k2eAgMnSc1d1	ochoten
čelit	čelit	k5eAaImF	čelit
nepřátelské	přátelský	k2eNgFnSc3d1	nepřátelská
palbě	palba	k1gFnSc3	palba
<g/>
,	,	kIx,	,
plukovními	plukovní	k2eAgInPc7d1	plukovní
prapory	prapor	k1gInPc7	prapor
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnPc3	který
vojáci	voják	k1gMnPc1	voják
kvůli	kvůli	k7c3	kvůli
orlům	orel	k1gMnPc3	orel
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
špicím	špice	k1gFnPc3	špice
říkali	říkat	k5eAaImAgMnP	říkat
kukačky	kukačka	k1gFnPc4	kukačka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rovněž	rovněž	k9	rovněž
i	i	k9	i
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jako	jako	k8xS	jako
muž	muž	k1gMnSc1	muž
revoluce	revoluce	k1gFnSc2	revoluce
dokázal	dokázat	k5eAaPmAgMnS	dokázat
projít	projít	k5eAaPmF	projít
mezi	mezi	k7c7	mezi
vojáky	voják	k1gMnPc7	voják
<g/>
,	,	kIx,	,
pochválit	pochválit	k5eAaPmF	pochválit
<g/>
,	,	kIx,	,
připomenout	připomenout	k5eAaPmF	připomenout
<g/>
,	,	kIx,	,
zažertovat	zažertovat	k5eAaPmF	zažertovat
<g/>
.	.	kIx.	.
</s>
<s>
Historky	historka	k1gFnPc1	historka
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
císař	císař	k1gMnSc1	císař
promluvil	promluvit	k5eAaPmAgMnS	promluvit
s	s	k7c7	s
prostým	prostý	k2eAgMnSc7d1	prostý
vojákem	voják	k1gMnSc7	voják
letěly	letět	k5eAaImAgFnP	letět
od	od	k7c2	od
úst	ústa	k1gNnPc2	ústa
k	k	k7c3	k
ústům	ústa	k1gNnPc3	ústa
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
stával	stávat	k5eAaImAgMnS	stávat
jejich	jejich	k3xOp3gMnSc7	jejich
císařem	císař	k1gMnSc7	císař
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Správa	správa	k1gFnSc1	správa
Francie	Francie	k1gFnSc2	Francie
===	===	k?	===
</s>
</p>
<p>
<s>
Již	již	k6eAd1	již
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
prvního	první	k4xOgNnSc2	první
konzula	konzul	k1gMnSc4	konzul
byl	být	k5eAaImAgMnS	být
Napoleon	Napoleon	k1gMnSc1	Napoleon
nucen	nutit	k5eAaImNgMnS	nutit
podniknout	podniknout	k5eAaPmF	podniknout
první	první	k4xOgInPc4	první
kroky	krok	k1gInPc4	krok
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vypořádal	vypořádat	k5eAaPmAgMnS	vypořádat
s	s	k7c7	s
důsledky	důsledek	k1gInPc7	důsledek
finanční	finanční	k2eAgFnSc2d1	finanční
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
následkem	následkem	k7c2	následkem
tristního	tristní	k2eAgNnSc2d1	tristní
hospodaření	hospodaření	k1gNnSc2	hospodaření
Direktoria	direktorium	k1gNnSc2	direktorium
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgMnS	vydat
nové	nový	k2eAgInPc4d1	nový
předpisy	předpis	k1gInPc4	předpis
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
nimž	jenž	k3xRgFnPc3	jenž
finanční	finanční	k2eAgFnSc3d1	finanční
úředníci	úředník	k1gMnPc1	úředník
–	–	k?	–
výběrčí	výběrčí	k1gFnSc1	výběrčí
<g/>
,	,	kIx,	,
berní	berní	k1gMnPc1	berní
<g/>
,	,	kIx,	,
vrchní	vrchní	k2eAgMnPc1d1	vrchní
berní	berní	k1gMnPc1	berní
a	a	k8xC	a
ředitelé	ředitel	k1gMnPc1	ředitel
daní	daň	k1gFnPc2	daň
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
zajistit	zajistit	k5eAaPmF	zajistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
peníze	peníz	k1gInPc1	peníz
daňových	daňový	k2eAgMnPc2d1	daňový
poplatníků	poplatník	k1gMnPc2	poplatník
směřovaly	směřovat	k5eAaImAgFnP	směřovat
do	do	k7c2	do
veřejných	veřejný	k2eAgFnPc2d1	veřejná
pokladen	pokladna	k1gFnPc2	pokladna
pravidelně	pravidelně	k6eAd1	pravidelně
a	a	k8xC	a
v	v	k7c4	v
přesně	přesně	k6eAd1	přesně
stanovený	stanovený	k2eAgInSc4d1	stanovený
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
tak	tak	k9	tak
dělo	dít	k5eAaBmAgNnS	dít
jednou	jednou	k6eAd1	jednou
měsíčně	měsíčně	k6eAd1	měsíčně
<g/>
,	,	kIx,	,
po	po	k7c4	po
bankovní	bankovní	k2eAgFnSc4d1	bankovní
krizi	krize	k1gFnSc4	krize
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1805	[number]	k4	1805
pak	pak	k6eAd1	pak
každý	každý	k3xTgInSc4	každý
desátý	desátý	k4xOgInSc4	desátý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nový	nový	k2eAgInSc1d1	nový
systém	systém	k1gInSc1	systém
vykazoval	vykazovat	k5eAaImAgInS	vykazovat
maximální	maximální	k2eAgInPc4d1	maximální
výnosy	výnos	k1gInPc4	výnos
s	s	k7c7	s
minimálním	minimální	k2eAgInSc7d1	minimální
počtem	počet	k1gInSc7	počet
úředníků	úředník	k1gMnPc2	úředník
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
konzulátu	konzulát	k1gInSc2	konzulát
i	i	k9	i
v	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
císařství	císařství	k1gNnSc2	císařství
mohl	moct	k5eAaImAgInS	moct
stát	stát	k1gInSc1	stát
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnSc3	jeho
efektivitě	efektivita	k1gFnSc3	efektivita
disponovat	disponovat	k5eAaBmF	disponovat
částkou	částka	k1gFnSc7	částka
750	[number]	k4	750
<g/>
–	–	k?	–
<g/>
800	[number]	k4	800
milionů	milion	k4xCgInPc2	milion
franků	frank	k1gInPc2	frank
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Dozor	dozor	k1gInSc4	dozor
nad	nad	k7c7	nad
peněžním	peněžní	k2eAgInSc7d1	peněžní
aparátem	aparát	k1gInSc7	aparát
prováděl	provádět	k5eAaImAgInS	provádět
osobně	osobně	k6eAd1	osobně
Napoleon	napoleon	k1gInSc1	napoleon
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
si	se	k3xPyFc3	se
od	od	k7c2	od
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
nechával	nechávat	k5eAaImAgMnS	nechávat
třikrát	třikrát	k6eAd1	třikrát
do	do	k7c2	do
měsíce	měsíc	k1gInSc2	měsíc
předkládat	předkládat	k5eAaImF	předkládat
výkazy	výkaz	k1gInPc4	výkaz
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
oddělení	oddělení	k1gNnPc2	oddělení
finanční	finanční	k2eAgFnSc2d1	finanční
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
stránková	stránkový	k2eAgFnSc1d1	stránková
folia	folio	k1gNnSc2	folio
důsledně	důsledně	k6eAd1	důsledně
pročítal	pročítat	k5eAaImAgInS	pročítat
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nejasností	nejasnost	k1gFnPc2	nejasnost
se	se	k3xPyFc4	se
dožadoval	dožadovat	k5eAaImAgInS	dožadovat
vysvětlení	vysvětlení	k1gNnPc4	vysvětlení
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
přinutilo	přinutit	k5eAaPmAgNnS	přinutit
úředníky	úředník	k1gMnPc4	úředník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
účetní	účetní	k2eAgFnPc4d1	účetní
knihy	kniha	k1gFnPc4	kniha
drželi	držet	k5eAaImAgMnP	držet
v	v	k7c6	v
naprostém	naprostý	k2eAgInSc6d1	naprostý
pořádku	pořádek	k1gInSc6	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádně	mimořádně	k6eAd1	mimořádně
významným	významný	k2eAgInSc7d1	významný
počinem	počin	k1gInSc7	počin
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
finančnictví	finančnictví	k1gNnSc2	finančnictví
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
přetrval	přetrvat	k5eAaPmAgInS	přetrvat
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
založení	založení	k1gNnSc1	založení
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
banky	banka	k1gFnSc2	banka
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1800	[number]	k4	1800
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Napoleonovým	Napoleonův	k2eAgInSc7d1	Napoleonův
záměrem	záměr	k1gInSc7	záměr
bylo	být	k5eAaImAgNnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
instituci	instituce	k1gFnSc4	instituce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
soukromou	soukromý	k2eAgFnSc7d1	soukromá
společností	společnost	k1gFnSc7	společnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
stát	stát	k5eAaPmF	stát
jen	jen	k9	jen
omezenou	omezený	k2eAgFnSc4d1	omezená
účast	účast	k1gFnSc4	účast
a	a	k8xC	a
menšinový	menšinový	k2eAgInSc4d1	menšinový
podíl	podíl	k1gInSc4	podíl
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zajistil	zajistit	k5eAaPmAgInS	zajistit
by	by	kYmCp3nS	by
její	její	k3xOp3gNnSc1	její
legislativu	legislativa	k1gFnSc4	legislativa
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
iniciativu	iniciativa	k1gFnSc4	iniciativa
při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
založení	založení	k1gNnSc6	založení
proto	proto	k8xC	proto
převzal	převzít	k5eAaPmAgMnS	převzít
J.	J.	kA	J.
F.	F.	kA	F.
Perregaux	Perregaux	k1gInSc4	Perregaux
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
svolal	svolat	k5eAaPmAgMnS	svolat
skupinu	skupina	k1gFnSc4	skupina
finančníků	finančník	k1gMnPc2	finančník
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
navýšila	navýšit	k5eAaPmAgFnS	navýšit
základní	základní	k2eAgInSc4d1	základní
kapitál	kapitál	k1gInSc4	kapitál
banky	banka	k1gFnSc2	banka
na	na	k7c4	na
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
franků	frank	k1gInPc2	frank
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gNnSc7	její
řízením	řízení	k1gNnSc7	řízení
bylo	být	k5eAaImAgNnS	být
pověřeno	pověřit	k5eAaPmNgNnS	pověřit
patnáct	patnáct	k4xCc1	patnáct
ředitelů	ředitel	k1gMnPc2	ředitel
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
volilo	volit	k5eAaImAgNnS	volit
200	[number]	k4	200
největších	veliký	k2eAgMnPc2d3	veliký
podílníků	podílník	k1gMnPc2	podílník
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
hmotnému	hmotný	k2eAgNnSc3d1	hmotné
zainteresování	zainteresování	k1gNnSc3	zainteresování
francouzské	francouzský	k2eAgFnSc2d1	francouzská
finanční	finanční	k2eAgFnSc2d1	finanční
elity	elita	k1gFnSc2	elita
na	na	k7c6	na
prosperitě	prosperita	k1gFnSc6	prosperita
projektu	projekt	k1gInSc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Opatřením	opatření	k1gNnSc7	opatření
ze	z	k7c2	z
7	[number]	k4	7
<g/>
.	.	kIx.	.
germinalu	germinal	k1gInSc2	germinal
r.	r.	kA	r.
XIfranc	XIfranc	k1gFnSc1	XIfranc
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
1803	[number]	k4	1803
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
základem	základ	k1gInSc7	základ
měnového	měnový	k2eAgInSc2d1	měnový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
franc	franc	k6eAd1	franc
de	de	k?	de
germinal	germinat	k5eAaPmAgMnS	germinat
<g/>
,	,	kIx,	,
Napoleon	napoleon	k1gInSc1	napoleon
přísně	přísně	k6eAd1	přísně
reguloval	regulovat	k5eAaImAgInS	regulovat
kovový	kovový	k2eAgInSc1d1	kovový
obsah	obsah	k1gInSc1	obsah
nových	nový	k2eAgFnPc2d1	nová
mincí	mince	k1gFnPc2	mince
všech	všecek	k3xTgFnPc2	všecek
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
bimetalickém	bimetalický	k2eAgInSc6d1	bimetalický
standardu	standard	k1gInSc6	standard
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
vymezil	vymezit	k5eAaPmAgInS	vymezit
poměr	poměr	k1gInSc1	poměr
zlata	zlato	k1gNnSc2	zlato
ke	k	k7c3	k
stříbru	stříbro	k1gNnSc3	stříbro
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
15,5	[number]	k4	15,5
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
velkou	velká	k1gFnSc7	velká
finanční	finanční	k2eAgFnSc4d1	finanční
reformu	reforma	k1gFnSc4	reforma
provedl	provést	k5eAaPmAgMnS	provést
císař	císař	k1gMnSc1	císař
Francouzů	Francouz	k1gMnPc2	Francouz
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
ministra	ministr	k1gMnSc2	ministr
financí	finance	k1gFnPc2	finance
Gaudina	Gaudin	k2eAgInSc2d1	Gaudin
a	a	k8xC	a
šéfa	šéf	k1gMnSc2	šéf
státní	státní	k2eAgFnSc2d1	státní
pokladny	pokladna	k1gFnSc2	pokladna
Molliena	Mollieno	k1gNnSc2	Mollieno
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
míru	mír	k1gInSc2	mír
v	v	k7c6	v
Tylži	Tylž	k1gFnSc6	Tylž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1807	[number]	k4	1807
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
této	tento	k3xDgFnSc2	tento
reorganizace	reorganizace	k1gFnSc2	reorganizace
bylo	být	k5eAaImAgNnS	být
vytvoření	vytvoření	k1gNnSc1	vytvoření
nového	nový	k2eAgInSc2d1	nový
ústředního	ústřední	k2eAgInSc2d1	ústřední
úřadu	úřad	k1gInSc2	úřad
Cour	Cour	k?	Cour
des	des	k1gNnSc1	des
comptes	comptes	k1gMnSc1	comptes
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
sloužil	sloužit	k5eAaImAgMnS	sloužit
k	k	k7c3	k
účinnějším	účinný	k2eAgFnPc3d2	účinnější
revizím	revize	k1gFnPc3	revize
státních	státní	k2eAgFnPc2d1	státní
financí	finance	k1gFnPc2	finance
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
změn	změna	k1gFnPc2	změna
ve	v	k7c6	v
finanční	finanční	k2eAgFnSc6d1	finanční
správě	správa	k1gFnSc6	správa
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
důchody	důchod	k1gInPc4	důchod
císařství	císařství	k1gNnSc2	císařství
plně	plně	k6eAd1	plně
kryly	krýt	k5eAaImAgInP	krýt
státní	státní	k2eAgInPc1d1	státní
výdaje	výdaj	k1gInPc1	výdaj
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
nákladů	náklad	k1gInPc2	náklad
na	na	k7c4	na
vydržování	vydržování	k1gNnSc4	vydržování
armády	armáda	k1gFnSc2	armáda
ve	v	k7c6	v
válečném	válečný	k2eAgInSc6d1	válečný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významným	významný	k2eAgInSc7d1	významný
počinem	počin	k1gInSc7	počin
v	v	k7c6	v
období	období	k1gNnSc6	období
konzulátu	konzulát	k1gInSc2	konzulát
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Napoleonova	Napoleonův	k2eAgFnSc1d1	Napoleonova
iniciativa	iniciativa	k1gFnSc1	iniciativa
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zákonodárství	zákonodárství	k1gNnSc2	zákonodárství
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
vítězného	vítězný	k2eAgNnSc2d1	vítězné
tažení	tažení	k1gNnSc2	tažení
zakončeného	zakončený	k2eAgNnSc2d1	zakončené
bitvou	bitva	k1gFnSc7	bitva
u	u	k7c2	u
Marenga	marengo	k1gNnSc2	marengo
vydal	vydat	k5eAaPmAgInS	vydat
nařízení	nařízení	k1gNnPc4	nařízení
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
komise	komise	k1gFnSc2	komise
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1800	[number]	k4	1800
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
vypracovat	vypracovat	k5eAaPmF	vypracovat
znění	znění	k1gNnSc4	znění
návrhu	návrh	k1gInSc2	návrh
nového	nový	k2eAgInSc2d1	nový
občanského	občanský	k2eAgInSc2d1	občanský
zákoníku	zákoník	k1gInSc2	zákoník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
stát	stát	k1gInSc1	stát
základní	základní	k2eAgInSc1d1	základní
právní	právní	k2eAgFnSc7d1	právní
normou	norma	k1gFnSc7	norma
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
jí	on	k3xPp3gFnSc2	on
ovládaných	ovládaný	k2eAgFnPc6d1	ovládaná
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgInSc1d1	nový
kodex	kodex	k1gInSc1	kodex
byl	být	k5eAaImAgInS	být
koncipovaný	koncipovaný	k2eAgInSc1d1	koncipovaný
řadou	řada	k1gFnSc7	řada
právních	právní	k2eAgMnPc2d1	právní
expertů	expert	k1gMnPc2	expert
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
(	(	kIx(	(
<g/>
Tronchet	Tronchet	k1gMnSc1	Tronchet
<g/>
,	,	kIx,	,
Bigot	Bigot	k1gMnSc1	Bigot
de	de	k?	de
Prémaneneu	Prémanenea	k1gMnSc4	Prémanenea
<g/>
,	,	kIx,	,
Cambacérè	Cambacérè	k1gMnSc4	Cambacérè
<g/>
,	,	kIx,	,
Malleville	Mallevill	k1gMnSc4	Mallevill
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Napoleon	Napoleon	k1gMnSc1	Napoleon
často	často	k6eAd1	často
zasahoval	zasahovat	k5eAaImAgMnS	zasahovat
do	do	k7c2	do
utváření	utváření	k1gNnSc2	utváření
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
návrhů	návrh	k1gInPc2	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1804	[number]	k4	1804
vešel	vejít	k5eAaPmAgInS	vejít
zákoník	zákoník	k1gInSc1	zákoník
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
pod	pod	k7c7	pod
názvy	název	k1gInPc7	název
Code	Cod	k1gFnSc2	Cod
civil	civil	k1gMnSc1	civil
nebo	nebo	k8xC	nebo
Code	Code	k1gFnSc1	Code
Napoleon	napoleon	k1gInSc1	napoleon
a	a	k8xC	a
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
2281	[number]	k4	2281
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
definitivně	definitivně	k6eAd1	definitivně
odstraňovaly	odstraňovat	k5eAaImAgInP	odstraňovat
feudalismus	feudalismus	k1gInSc4	feudalismus
a	a	k8xC	a
jakékoliv	jakýkoliv	k3yIgFnPc4	jakýkoliv
stopy	stopa	k1gFnPc4	stopa
lenního	lenní	k2eAgNnSc2d1	lenní
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
například	například	k6eAd1	například
potlačil	potlačit	k5eAaPmAgMnS	potlačit
práva	právo	k1gNnPc4	právo
prvorozených	prvorozený	k2eAgMnPc2d1	prvorozený
a	a	k8xC	a
stanovil	stanovit	k5eAaPmAgInS	stanovit
dělbu	dělba	k1gFnSc4	dělba
majetku	majetek	k1gInSc2	majetek
mezi	mezi	k7c4	mezi
všechny	všechen	k3xTgMnPc4	všechen
dědice	dědic	k1gMnPc4	dědic
mužského	mužský	k2eAgNnSc2d1	mužské
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
formálně	formálně	k6eAd1	formálně
uznal	uznat	k5eAaPmAgInS	uznat
oprávněnost	oprávněnost	k1gFnSc4	oprávněnost
dřívějšího	dřívější	k2eAgInSc2d1	dřívější
prodeje	prodej	k1gInSc2	prodej
státních	státní	k2eAgInPc2d1	státní
pozemků	pozemek	k1gInPc2	pozemek
zkonfiskovaných	zkonfiskovaný	k2eAgInPc2d1	zkonfiskovaný
církvi	církev	k1gFnSc3	církev
a	a	k8xC	a
emigrantům	emigrant	k1gMnPc3	emigrant
nebo	nebo	k8xC	nebo
stanovil	stanovit	k5eAaPmAgMnS	stanovit
další	další	k2eAgFnPc4d1	další
podmínky	podmínka	k1gFnPc4	podmínka
dědictví	dědictví	k1gNnSc2	dědictví
a	a	k8xC	a
rozvodů	rozvod	k1gInPc2	rozvod
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
obsahem	obsah	k1gInSc7	obsah
však	však	k9	však
také	také	k9	také
značně	značně	k6eAd1	značně
posílil	posílit	k5eAaPmAgInS	posílit
moc	moc	k1gFnSc4	moc
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
vměšování	vměšování	k1gNnSc4	vměšování
do	do	k7c2	do
soukromých	soukromý	k2eAgFnPc2d1	soukromá
záležitostí	záležitost	k1gFnPc2	záležitost
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
Code	Code	k1gInSc1	Code
Napoleon	napoleon	k1gInSc1	napoleon
stal	stát	k5eAaPmAgInS	stát
oficiálním	oficiální	k2eAgInSc7d1	oficiální
pilířem	pilíř	k1gInSc7	pilíř
francouzské	francouzský	k2eAgFnSc2d1	francouzská
právní	právní	k2eAgFnSc2d1	právní
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
i	i	k9	i
přes	přes	k7c4	přes
mnohé	mnohý	k2eAgFnPc4d1	mnohá
novely	novela	k1gFnPc4	novela
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
doposud	doposud	k6eAd1	doposud
základem	základ	k1gInSc7	základ
francouzského	francouzský	k2eAgNnSc2d1	francouzské
občanského	občanský	k2eAgNnSc2d1	občanské
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
jím	jíst	k5eAaImIp1nS	jíst
formulované	formulovaný	k2eAgFnPc4d1	formulovaná
zásady	zásada	k1gFnPc4	zásada
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
občanské	občanský	k2eAgNnSc4d1	občanské
zákonodárství	zákonodárství	k1gNnSc4	zákonodárství
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
prací	práce	k1gFnSc7	práce
na	na	k7c4	na
občanského	občanský	k2eAgInSc2d1	občanský
zákoníku	zákoník	k1gInSc2	zákoník
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
Napoleonův	Napoleonův	k2eAgInSc4d1	Napoleonův
příkaz	příkaz	k1gInSc4	příkaz
vypracován	vypracován	k2eAgInSc1d1	vypracován
i	i	k8xC	i
speciální	speciální	k2eAgInSc1d1	speciální
kodex	kodex	k1gInSc1	kodex
pro	pro	k7c4	pro
obchod	obchod	k1gInSc4	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
ustanovení	ustanovení	k1gNnPc1	ustanovení
regulovala	regulovat	k5eAaImAgNnP	regulovat
a	a	k8xC	a
právně	právně	k6eAd1	právně
zabezpečovala	zabezpečovat	k5eAaImAgFnS	zabezpečovat
obchodní	obchodní	k2eAgFnSc1d1	obchodní
transakce	transakce	k1gFnSc1	transakce
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
na	na	k7c6	na
burse	bursa	k1gFnSc6	bursa
a	a	k8xC	a
bankovní	bankovní	k2eAgNnSc4d1	bankovní
<g/>
,	,	kIx,	,
směnárenské	směnárenský	k2eAgNnSc4d1	směnárenské
i	i	k8xC	i
notářské	notářský	k2eAgNnSc4d1	notářské
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
týkalo	týkat	k5eAaImAgNnS	týkat
obchodních	obchodní	k2eAgFnPc2d1	obchodní
operací	operace	k1gFnPc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Nesporným	sporný	k2eNgInSc7d1	nesporný
krokem	krok	k1gInSc7	krok
vzad	vzad	k6eAd1	vzad
byl	být	k5eAaImAgInS	být
trestní	trestní	k2eAgInSc1d1	trestní
kodex	kodex	k1gInSc1	kodex
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
trest	trest	k1gInSc4	trest
smrti	smrt	k1gFnSc2	smrt
i	i	k8xC	i
tělesné	tělesný	k2eAgInPc4d1	tělesný
tresty	trest	k1gInPc4	trest
důtkami	důtka	k1gFnPc7	důtka
(	(	kIx(	(
<g/>
ty	ten	k3xDgFnPc1	ten
byly	být	k5eAaImAgFnP	být
zrušeny	zrušit	k5eAaPmNgFnP	zrušit
za	za	k7c2	za
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cejchování	cejchování	k1gNnSc1	cejchování
rozžhaveným	rozžhavený	k2eAgNnSc7d1	rozžhavené
železem	železo	k1gNnSc7	železo
a	a	k8xC	a
nejpřísnější	přísný	k2eAgInPc4d3	nejpřísnější
tresty	trest	k1gInPc4	trest
za	za	k7c4	za
delikty	delikt	k1gInPc4	delikt
proti	proti	k7c3	proti
vlastnictví	vlastnictví	k1gNnSc3	vlastnictví
<g/>
.	.	kIx.	.
<g/>
O	o	k7c4	o
další	další	k2eAgFnSc4d1	další
reformu	reforma	k1gFnSc4	reforma
se	se	k3xPyFc4	se
Napoleon	napoleon	k1gInSc4	napoleon
zasadil	zasadit	k5eAaPmAgInS	zasadit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
,	,	kIx,	,
když	když	k8xS	když
11	[number]	k4	11
<g/>
.	.	kIx.	.
floreálu	floreál	k1gInSc2	floreál
r.	r.	kA	r.
Xfranc	Xfranc	k1gFnSc1	Xfranc
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1802	[number]	k4	1802
<g/>
)	)	kIx)	)
vydal	vydat	k5eAaPmAgInS	vydat
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
veřejném	veřejný	k2eAgNnSc6d1	veřejné
vyučování	vyučování	k1gNnSc6	vyučování
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
vládním	vládní	k2eAgNnSc7d1	vládní
nařízením	nařízení	k1gNnSc7	nařízení
zřizoval	zřizovat	k5eAaImAgMnS	zřizovat
na	na	k7c6	na
území	území	k1gNnSc6	území
Francie	Francie	k1gFnSc2	Francie
veřejná	veřejný	k2eAgNnPc1d1	veřejné
lycea	lyceum	k1gNnPc1	lyceum
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
sloužit	sloužit	k5eAaImF	sloužit
především	především	k9	především
k	k	k7c3	k
výchově	výchova	k1gFnSc3	výchova
nových	nový	k2eAgMnPc2d1	nový
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
položil	položit	k5eAaPmAgMnS	položit
základ	základ	k1gInSc4	základ
státního	státní	k2eAgInSc2d1	státní
vzdělávacího	vzdělávací	k2eAgInSc2d1	vzdělávací
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
Université	Universita	k1gMnPc1	Universita
de	de	k?	de
France	Franc	k1gMnSc2	Franc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výuka	výuka	k1gFnSc1	výuka
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
institucích	instituce	k1gFnPc6	instituce
probíhala	probíhat	k5eAaImAgFnS	probíhat
zcela	zcela	k6eAd1	zcela
ve	v	k7c6	v
vojenském	vojenský	k2eAgMnSc6d1	vojenský
duchu	duch	k1gMnSc6	duch
<g/>
,	,	kIx,	,
žáci	žák	k1gMnPc1	žák
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
země	zem	k1gFnSc2	zem
a	a	k8xC	a
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
věku	věk	k1gInSc6	věk
měli	mít	k5eAaImAgMnP	mít
mít	mít	k5eAaImF	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
výuku	výuka	k1gFnSc4	výuka
v	v	k7c4	v
tutéž	týž	k3xTgFnSc4	týž
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
hodiny	hodina	k1gFnPc4	hodina
ohlašoval	ohlašovat	k5eAaImAgMnS	ohlašovat
bubeník	bubeník	k1gMnSc1	bubeník
<g/>
,	,	kIx,	,
studenti	student	k1gMnPc1	student
byli	být	k5eAaImAgMnP	být
oblečeni	obléct	k5eAaPmNgMnP	obléct
v	v	k7c6	v
uniformách	uniforma	k1gFnPc6	uniforma
<g/>
,	,	kIx,	,
trestalo	trestat	k5eAaImAgNnS	trestat
se	s	k7c7	s
zavíráním	zavírání	k1gNnSc7	zavírání
atd.	atd.	kA	atd.
Absolventi	absolvent	k1gMnPc1	absolvent
lyceí	lyceum	k1gNnPc2	lyceum
byli	být	k5eAaImAgMnP	být
po	po	k7c6	po
složení	složení	k1gNnSc6	složení
doplňovací	doplňovací	k2eAgFnSc2d1	doplňovací
zkoušky	zkouška	k1gFnSc2	zkouška
přijímáni	přijímat	k5eAaImNgMnP	přijímat
do	do	k7c2	do
vyšších	vysoký	k2eAgFnPc2d2	vyšší
a	a	k8xC	a
odborných	odborný	k2eAgNnPc2d1	odborné
vojenských	vojenský	k2eAgNnPc2d1	vojenské
učilišť	učiliště	k1gNnPc2	učiliště
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
uplatnit	uplatnit	k5eAaPmF	uplatnit
v	v	k7c6	v
občanských	občanský	k2eAgInPc6d1	občanský
oborech	obor	k1gInPc6	obor
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
veřejném	veřejný	k2eAgNnSc6d1	veřejné
vyučování	vyučování	k1gNnSc6	vyučování
Napoleon	napoleon	k1gInSc1	napoleon
v	v	k7c6	v
období	období	k1gNnSc2	období
prvního	první	k4xOgNnSc2	první
císařství	císařství	k1gNnSc2	císařství
ještě	ještě	k9	ještě
doplnil	doplnit	k5eAaPmAgMnS	doplnit
dekrety	dekret	k1gInPc4	dekret
z	z	k7c2	z
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1806	[number]	k4	1806
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1808	[number]	k4	1808
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nařízení	nařízení	k1gNnSc1	nařízení
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
vzdělávací	vzdělávací	k2eAgInPc4d1	vzdělávací
stupně	stupeň	k1gInPc4	stupeň
na	na	k7c4	na
základní	základní	k2eAgFnPc4d1	základní
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnPc4d1	střední
a	a	k8xC	a
vyšší	vysoký	k2eAgFnPc4d2	vyšší
<g/>
,	,	kIx,	,
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
nebo	nebo	k8xC	nebo
jeho	jeho	k3xOp3gMnSc3	jeho
zástupci	zástupce	k1gMnSc3	zástupce
<g/>
,	,	kIx,	,
velmistru	velmistr	k1gMnSc3	velmistr
university	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
předepisovat	předepisovat	k5eAaImF	předepisovat
osnovy	osnova	k1gFnPc4	osnova
a	a	k8xC	a
školský	školský	k2eAgInSc4d1	školský
řád	řád	k1gInSc4	řád
a	a	k8xC	a
jmenovat	jmenovat	k5eAaImF	jmenovat
veškerý	veškerý	k3xTgInSc4	veškerý
personál	personál	k1gInSc4	personál
školních	školní	k2eAgFnPc2d1	školní
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
dobu	doba	k1gFnSc4	doba
svého	svůj	k3xOyFgNnSc2	svůj
panování	panování	k1gNnSc2	panování
Napoleon	Napoleon	k1gMnSc1	Napoleon
věnoval	věnovat	k5eAaImAgMnS	věnovat
svou	svůj	k3xOyFgFnSc4	svůj
pozornost	pozornost	k1gFnSc4	pozornost
také	také	k9	také
hospodářským	hospodářský	k2eAgNnSc7d1	hospodářské
odvětvím	odvětví	k1gNnSc7	odvětví
<g/>
.	.	kIx.	.
</s>
<s>
Určitou	určitý	k2eAgFnSc4d1	určitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
rozvoji	rozvoj	k1gInSc6	rozvoj
těchto	tento	k3xDgInPc2	tento
oborů	obor	k1gInPc2	obor
hrálo	hrát	k5eAaImAgNnS	hrát
zavedení	zavedení	k1gNnSc1	zavedení
systému	systém	k1gInSc2	systém
kontinentální	kontinentální	k2eAgFnSc2d1	kontinentální
blokády	blokáda	k1gFnSc2	blokáda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
Evropany	Evropan	k1gMnPc4	Evropan
odřízla	odříznout	k5eAaPmAgFnS	odříznout
od	od	k7c2	od
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
dodávek	dodávka	k1gFnPc2	dodávka
anglického	anglický	k2eAgNnSc2d1	anglické
a	a	k8xC	a
koloniálního	koloniální	k2eAgNnSc2d1	koloniální
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Absence	absence	k1gFnSc1	absence
hospodářsky	hospodářsky	k6eAd1	hospodářsky
silnějšího	silný	k2eAgMnSc2d2	silnější
konkurenta	konkurent	k1gMnSc2	konkurent
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
důležitým	důležitý	k2eAgInSc7d1	důležitý
stimulem	stimul	k1gInSc7	stimul
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
francouzského	francouzský	k2eAgInSc2d1	francouzský
lehkého	lehký	k2eAgInSc2d1	lehký
a	a	k8xC	a
těžkého	těžký	k2eAgInSc2d1	těžký
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Začaly	začít	k5eAaPmAgFnP	začít
se	se	k3xPyFc4	se
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
cukrovary	cukrovar	k1gInPc4	cukrovar
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zaplňovaly	zaplňovat	k5eAaImAgFnP	zaplňovat
poptávku	poptávka	k1gFnSc4	poptávka
trhu	trh	k1gInSc2	trh
po	po	k7c6	po
třtinovém	třtinový	k2eAgInSc6d1	třtinový
nebo	nebo	k8xC	nebo
koloniálním	koloniální	k2eAgInSc6d1	koloniální
cukru	cukr	k1gInSc6	cukr
<g/>
,	,	kIx,	,
v	v	k7c6	v
chemickém	chemický	k2eAgInSc6d1	chemický
průmyslu	průmysl	k1gInSc6	průmysl
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
pokroků	pokrok	k1gInPc2	pokrok
převážně	převážně	k6eAd1	převážně
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
umělých	umělý	k2eAgNnPc2d1	umělé
barviv	barvivo	k1gNnPc2	barvivo
a	a	k8xC	a
bělidel	bělidlo	k1gNnPc2	bělidlo
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
surového	surový	k2eAgNnSc2d1	surové
železa	železo	k1gNnSc2	železo
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1790	[number]	k4	1790
<g/>
–	–	k?	–
<g/>
1810	[number]	k4	1810
více	hodně	k6eAd2	hodně
než	než	k8xS	než
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS	zdvojnásobit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
textilnictví	textilnictví	k1gNnSc6	textilnictví
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
rozkrýt	rozkrýt	k5eAaPmF	rozkrýt
tajemství	tajemství	k1gNnSc1	tajemství
výroby	výroba	k1gFnSc2	výroba
anglických	anglický	k2eAgFnPc2d1	anglická
křížových	křížový	k2eAgFnPc2d1	křížová
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
bezmála	bezmála	k6eAd1	bezmála
čtyřicet	čtyřicet	k4xCc1	čtyřicet
přádelen	přádelna	k1gFnPc2	přádelna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
na	na	k7c4	na
20	[number]	k4	20
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1800	[number]	k4	1800
<g/>
–	–	k?	–
<g/>
1811	[number]	k4	1811
se	se	k3xPyFc4	se
v	v	k7c6	v
Lyonu	Lyon	k1gInSc6	Lyon
ztrojnásobila	ztrojnásobit	k5eAaPmAgFnS	ztrojnásobit
výroba	výroba	k1gFnSc1	výroba
tkanin	tkanina	k1gFnPc2	tkanina
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1812	[number]	k4	1812
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
stálo	stát	k5eAaImAgNnS	stát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
mechanických	mechanický	k2eAgFnPc2d1	mechanická
přádelen	přádelna	k1gFnPc2	přádelna
<g/>
.	.	kIx.	.
</s>
<s>
Stroje	stroj	k1gInPc1	stroj
se	se	k3xPyFc4	se
zaváděly	zavádět	k5eAaImAgInP	zavádět
i	i	k9	i
do	do	k7c2	do
výroby	výroba	k1gFnSc2	výroba
hedvábí	hedvábí	k1gNnSc2	hedvábí
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgNnSc4	jenž
zpracování	zpracování	k1gNnSc4	zpracování
značně	značně	k6eAd1	značně
urychlil	urychlit	k5eAaPmAgInS	urychlit
Jacquardův	Jacquardův	k2eAgInSc1d1	Jacquardův
stav	stav	k1gInSc1	stav
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Napoleon	Napoleon	k1gMnSc1	Napoleon
vypsal	vypsat	k5eAaPmAgMnS	vypsat
milionovou	milionový	k2eAgFnSc4d1	milionová
odměnu	odměna	k1gFnSc4	odměna
pro	pro	k7c4	pro
toho	ten	k3xDgMnSc4	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
vynalezne	vynaleznout	k5eAaPmIp3nS	vynaleznout
nejefektivnější	efektivní	k2eAgInSc1d3	nejefektivnější
stav	stav	k1gInSc1	stav
na	na	k7c4	na
tkaní	tkaní	k1gNnSc4	tkaní
lnu	len	k1gInSc2	len
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dopomohlo	dopomoct	k5eAaPmAgNnS	dopomoct
k	k	k7c3	k
revolučnímu	revoluční	k2eAgInSc3d1	revoluční
objevu	objev	k1gInSc3	objev
inženýra	inženýr	k1gMnSc2	inženýr
Filipa	Filip	k1gMnSc2	Filip
de	de	k?	de
Girard	Girard	k1gMnSc1	Girard
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
výrobních	výrobní	k2eAgNnPc2d1	výrobní
odvětví	odvětví	k1gNnPc2	odvětví
Napoleon	napoleon	k1gInSc4	napoleon
v	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
po	po	k7c6	po
převzetí	převzetí	k1gNnSc6	převzetí
moci	moc	k1gFnSc2	moc
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
několik	několik	k4yIc4	několik
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
výstav	výstava	k1gFnPc2	výstava
a	a	k8xC	a
dekrety	dekret	k1gInPc1	dekret
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1811	[number]	k4	1811
a	a	k8xC	a
1812	[number]	k4	1812
zřídil	zřídit	k5eAaPmAgInS	zřídit
a	a	k8xC	a
organizoval	organizovat	k5eAaBmAgInS	organizovat
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
továren	továrna	k1gFnPc2	továrna
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
známkách	známka	k1gFnPc6	známka
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
nestability	nestabilita	k1gFnSc2	nestabilita
často	často	k6eAd1	často
půjčoval	půjčovat	k5eAaImAgMnS	půjčovat
peníze	peníz	k1gInPc4	peníz
továrnám	továrna	k1gFnPc3	továrna
<g/>
,	,	kIx,	,
obchodním	obchodní	k2eAgInPc3d1	obchodní
domům	dům	k1gInPc3	dům
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
dělníkům	dělník	k1gMnPc3	dělník
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
pracovali	pracovat	k5eAaImAgMnP	pracovat
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jim	on	k3xPp3gMnPc3	on
pomohl	pomoct	k5eAaPmAgMnS	pomoct
z	z	k7c2	z
krize	krize	k1gFnSc2	krize
nebo	nebo	k8xC	nebo
těžkého	těžký	k2eAgNnSc2d1	těžké
postavení	postavení	k1gNnSc2	postavení
<g/>
.	.	kIx.	.
</s>
<s>
Napoleonova	Napoleonův	k2eAgFnSc1d1	Napoleonova
podpora	podpora	k1gFnSc1	podpora
domácího	domácí	k2eAgInSc2d1	domácí
průmyslu	průmysl	k1gInSc2	průmysl
se	se	k3xPyFc4	se
odrazila	odrazit	k5eAaPmAgFnS	odrazit
i	i	k9	i
v	v	k7c6	v
nových	nový	k2eAgInPc6d1	nový
departmentech	department	k1gInPc6	department
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
anektovaných	anektovaný	k2eAgFnPc6d1	anektovaná
zemích	zem	k1gFnPc6	zem
uplatňoval	uplatňovat	k5eAaImAgInS	uplatňovat
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
neumožňovala	umožňovat	k5eNaImAgFnS	umožňovat
místním	místní	k2eAgMnPc3d1	místní
průmyslníkům	průmyslník	k1gMnPc3	průmyslník
konkurovat	konkurovat	k5eAaImF	konkurovat
francouzskému	francouzský	k2eAgNnSc3d1	francouzské
zboží	zboží	k1gNnSc3	zboží
jak	jak	k8xS	jak
na	na	k7c6	na
území	území	k1gNnSc6	území
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
vlasti	vlast	k1gFnSc6	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
přístupem	přístup	k1gInSc7	přístup
císař	císař	k1gMnSc1	císař
pobuřoval	pobuřovat	k5eAaImAgMnS	pobuřovat
<g/>
,	,	kIx,	,
likvidoval	likvidovat	k5eAaBmAgMnS	likvidovat
a	a	k8xC	a
utlačoval	utlačovat	k5eAaImAgMnS	utlačovat
nejen	nejen	k6eAd1	nejen
průmyslníky	průmyslník	k1gMnPc4	průmyslník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
obchodníky	obchodník	k1gMnPc4	obchodník
a	a	k8xC	a
spotřebitelské	spotřebitelský	k2eAgFnPc1d1	spotřebitelská
vrstvy	vrstva	k1gFnPc1	vrstva
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
své	své	k1gNnSc4	své
svého	svůj	k3xOyFgNnSc2	svůj
rozsáhlého	rozsáhlý	k2eAgNnSc2d1	rozsáhlé
impéria	impérium	k1gNnSc2	impérium
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
staré	starý	k2eAgFnSc2d1	stará
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jednání	jednání	k1gNnSc1	jednání
nakonec	nakonec	k6eAd1	nakonec
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
podmaněných	podmaněný	k2eAgFnPc6d1	Podmaněná
zemích	zem	k1gFnPc6	zem
začala	začít	k5eAaPmAgFnS	začít
upadat	upadat	k5eAaImF	upadat
kupní	kupní	k2eAgFnSc1d1	kupní
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zapříčinilo	zapříčinit	k5eAaPmAgNnS	zapříčinit
nižší	nízký	k2eAgFnSc4d2	nižší
poptávku	poptávka	k1gFnSc4	poptávka
po	po	k7c6	po
francouzském	francouzský	k2eAgNnSc6d1	francouzské
zboží	zboží	k1gNnSc6	zboží
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
po	po	k7c6	po
hospodářské	hospodářský	k2eAgFnSc6d1	hospodářská
krizi	krize	k1gFnSc6	krize
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1811	[number]	k4	1811
proto	proto	k8xC	proto
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
redukována	redukovat	k5eAaBmNgFnS	redukovat
a	a	k8xC	a
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
velmi	velmi	k6eAd1	velmi
výrazně	výrazně	k6eAd1	výrazně
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
bilance	bilance	k1gFnSc1	bilance
vývozu	vývoz	k1gInSc2	vývoz
v	v	k7c6	v
období	období	k1gNnSc6	období
Napoleonovy	Napoleonův	k2eAgFnSc2d1	Napoleonova
vlády	vláda	k1gFnSc2	vláda
však	však	k9	však
i	i	k9	i
přes	přes	k7c4	přes
jeho	jeho	k3xOp3gFnSc4	jeho
veškerou	veškerý	k3xTgFnSc4	veškerý
snahu	snaha	k1gFnSc4	snaha
nikdy	nikdy	k6eAd1	nikdy
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
dorovnat	dorovnat	k5eAaPmF	dorovnat
celkové	celkový	k2eAgFnSc2d1	celková
ztráty	ztráta	k1gFnSc2	ztráta
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
hodnoty	hodnota	k1gFnPc4	hodnota
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
obchodu	obchod	k1gInSc2	obchod
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1787	[number]	k4	1787
<g/>
–	–	k?	–
<g/>
89	[number]	k4	89
se	se	k3xPyFc4	se
Francii	Francie	k1gFnSc3	Francie
podařilo	podařit	k5eAaPmAgNnS	podařit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1826	[number]	k4	1826
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
Konzulátu	konzulát	k1gInSc2	konzulát
i	i	k8xC	i
Prvního	první	k4xOgMnSc2	první
císařství	císařství	k1gNnPc2	císařství
Napoleon	napoleon	k1gInSc1	napoleon
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
růst	růst	k1gInSc4	růst
francouzského	francouzský	k2eAgNnSc2d1	francouzské
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
,	,	kIx,	,
získaného	získaný	k2eAgNnSc2d1	získané
zejména	zejména	k9	zejména
odčerpáváním	odčerpávání	k1gNnSc7	odčerpávání
financí	finance	k1gFnPc2	finance
z	z	k7c2	z
podmaněné	podmaněný	k2eAgFnSc2d1	Podmaněná
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
výstavbou	výstavba	k1gFnSc7	výstavba
nových	nový	k2eAgFnPc2d1	nová
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
honosných	honosný	k2eAgFnPc2d1	honosná
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
krásných	krásný	k2eAgNnPc2d1	krásné
nábřeží	nábřeží	k1gNnPc2	nábřeží
nebo	nebo	k8xC	nebo
širokých	široký	k2eAgInPc2d1	široký
pařížských	pařížský	k2eAgInPc2d1	pařížský
mostů	most	k1gInPc2	most
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
pokrokem	pokrok	k1gInSc7	pokrok
však	však	k9	však
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c4	v
zavádění	zavádění	k1gNnPc4	zavádění
despotických	despotický	k2eAgFnPc2d1	despotická
vládních	vládní	k2eAgFnPc2d1	vládní
metod	metoda	k1gFnPc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
nebyl	být	k5eNaImAgMnS	být
ochoten	ochoten	k2eAgMnSc1d1	ochoten
akceptovat	akceptovat	k5eAaBmF	akceptovat
nic	nic	k3yNnSc4	nic
podobného	podobný	k2eAgNnSc2d1	podobné
svobodě	svoboda	k1gFnSc3	svoboda
tisku	tisk	k1gInSc2	tisk
<g/>
,	,	kIx,	,
opatřením	opatření	k1gNnSc7	opatření
z	z	k7c2	z
27	[number]	k4	27
<g/>
.	.	kIx.	.
nivôsu	nivôs	k1gInSc2	nivôs
r.	r.	kA	r.
VIIIfranc	VIIIfranc	k1gFnSc1	VIIIfranc
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1800	[number]	k4	1800
<g/>
)	)	kIx)	)
zastavil	zastavit	k5eAaPmAgInS	zastavit
tisk	tisk	k1gInSc1	tisk
šedesáti	šedesát	k4xCc2	šedesát
z	z	k7c2	z
celkových	celkový	k2eAgInPc2d1	celkový
sedmdesáti	sedmdesát	k4xCc2	sedmdesát
tří	tři	k4xCgInPc2	tři
vycházejících	vycházející	k2eAgMnPc2d1	vycházející
časopisů	časopis	k1gInPc2	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
dalších	další	k2eAgNnPc6d1	další
třináct	třináct	k4xCc4	třináct
a	a	k8xC	a
zbývající	zbývající	k2eAgMnPc1d1	zbývající
čtyři	čtyři	k4xCgMnPc1	čtyři
se	se	k3xPyFc4	se
octly	octnout	k5eAaPmAgFnP	octnout
pod	pod	k7c7	pod
bedlivým	bedlivý	k2eAgInSc7d1	bedlivý
dohledem	dohled	k1gInSc7	dohled
ministra	ministr	k1gMnSc2	ministr
policie	policie	k1gFnSc2	policie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
organizaci	organizace	k1gFnSc6	organizace
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
správy	správa	k1gFnSc2	správa
ponechal	ponechat	k5eAaPmAgInS	ponechat
rozdělení	rozdělení	k1gNnSc4	rozdělení
Francie	Francie	k1gFnSc2	Francie
na	na	k7c4	na
departmenty	department	k1gInPc4	department
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
ve	v	k7c6	v
městech	město	k1gNnPc6	město
i	i	k8xC	i
na	na	k7c6	na
vesnicích	vesnice	k1gFnPc6	vesnice
odstranil	odstranit	k5eAaPmAgMnS	odstranit
všechny	všechen	k3xTgInPc4	všechen
volené	volený	k2eAgInPc4d1	volený
úřady	úřad	k1gInPc4	úřad
a	a	k8xC	a
samosprávy	samospráva	k1gFnPc4	samospráva
<g/>
.	.	kIx.	.
</s>
<s>
Šéfem	šéf	k1gMnSc7	šéf
každého	každý	k3xTgInSc2	každý
departmentu	department	k1gInSc2	department
byl	být	k5eAaImAgMnS	být
jmenován	jmenován	k2eAgMnSc1d1	jmenován
prefekt	prefekt	k1gMnSc1	prefekt
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnSc6	jehož
kompetenci	kompetence	k1gFnSc6	kompetence
bylo	být	k5eAaImAgNnS	být
zbavovat	zbavovat	k5eAaImF	zbavovat
i	i	k8xC	i
jmenovat	jmenovat	k5eAaBmF	jmenovat
do	do	k7c2	do
úřadů	úřad	k1gInPc2	úřad
jemu	on	k3xPp3gNnSc3	on
odpovědné	odpovědný	k2eAgFnPc1d1	odpovědná
municipální	municipální	k2eAgFnPc1d1	municipální
rady	rada	k1gFnPc1	rada
i	i	k9	i
starosty	starosta	k1gMnPc4	starosta
<g/>
.	.	kIx.	.
</s>
<s>
Jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
prefekti	prefekt	k1gMnPc1	prefekt
byli	být	k5eAaImAgMnP	být
podřízení	podřízený	k1gMnPc1	podřízený
ministrovi	ministr	k1gMnSc3	ministr
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
řídil	řídit	k5eAaImAgMnS	řídit
veškerou	veškerý	k3xTgFnSc4	veškerý
administrativu	administrativa	k1gFnSc4	administrativa
země	zem	k1gFnSc2	zem
včetně	včetně	k7c2	včetně
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
veřejných	veřejný	k2eAgFnPc2d1	veřejná
prací	práce	k1gFnPc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgFnPc1d1	důležitá
změny	změna	k1gFnPc1	změna
nastaly	nastat	k5eAaPmAgFnP	nastat
také	také	k9	také
v	v	k7c6	v
soudnictví	soudnictví	k1gNnSc6	soudnictví
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1802	[number]	k4	1802
zrušen	zrušen	k2eAgInSc1d1	zrušen
porotní	porotní	k2eAgInSc1d1	porotní
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
poroty	porota	k1gFnPc1	porota
často	často	k6eAd1	často
brzdily	brzdit	k5eAaImAgFnP	brzdit
Napoleonovy	Napoleonův	k2eAgInPc4d1	Napoleonův
záměry	záměr	k1gInPc4	záměr
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zprošťovaly	zprošťovat	k5eAaImAgFnP	zprošťovat
viny	vina	k1gFnPc1	vina
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
chtěl	chtít	k5eAaImAgMnS	chtít
vidět	vidět	k5eAaImF	vidět
za	za	k7c7	za
mřížemi	mříž	k1gFnPc7	mříž
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
také	také	k9	také
rozhodoval	rozhodovat	k5eAaImAgMnS	rozhodovat
o	o	k7c6	o
jmenování	jmenování	k1gNnSc6	jmenování
a	a	k8xC	a
služebním	služební	k2eAgInSc6d1	služební
postupu	postup	k1gInSc6	postup
soudců	soudce	k1gMnPc2	soudce
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
záhy	záhy	k6eAd1	záhy
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
prvního	první	k4xOgNnSc2	první
konzula	konzul	k1gMnSc4	konzul
Napoleon	napoleon	k1gInSc1	napoleon
rozpoutal	rozpoutat	k5eAaPmAgInS	rozpoutat
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
jakobínům	jakobín	k1gMnPc3	jakobín
i	i	k8xC	i
roajalistům	roajalista	k1gMnPc3	roajalista
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
cíleně	cíleně	k6eAd1	cíleně
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
vyhladit	vyhladit	k5eAaPmF	vyhladit
veškeré	veškerý	k3xTgFnPc4	veškerý
vzpomínky	vzpomínka	k1gFnPc4	vzpomínka
na	na	k7c4	na
období	období	k1gNnSc4	období
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
povoleno	povolen	k2eAgNnSc1d1	povoleno
se	se	k3xPyFc4	se
byť	byť	k8xS	byť
i	i	k9	i
jen	jen	k9	jen
zmiňovat	zmiňovat	k5eAaImF	zmiňovat
třeba	třeba	k6eAd1	třeba
o	o	k7c6	o
některé	některý	k3yIgFnSc6	některý
z	z	k7c2	z
osobností	osobnost	k1gFnPc2	osobnost
revolučního	revoluční	k2eAgNnSc2d1	revoluční
období	období	k1gNnSc2	období
a	a	k8xC	a
do	do	k7c2	do
země	zem	k1gFnSc2	zem
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
například	například	k6eAd1	například
zakázáno	zakázán	k2eAgNnSc1d1	zakázáno
dovážet	dovážet	k5eAaImF	dovážet
německý	německý	k2eAgInSc4d1	německý
tisk	tisk	k1gInSc4	tisk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
sice	sice	k8xC	sice
kritizoval	kritizovat	k5eAaImAgInS	kritizovat
revoluční	revoluční	k2eAgInPc4d1	revoluční
ideály	ideál	k1gInPc4	ideál
a	a	k8xC	a
uznával	uznávat	k5eAaImAgInS	uznávat
Napoleona	Napoleon	k1gMnSc4	Napoleon
za	za	k7c4	za
jejich	jejich	k3xOp3gNnSc4	jejich
potření	potření	k1gNnSc4	potření
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
i	i	k9	i
tímto	tento	k3xDgInSc7	tento
obsahem	obsah	k1gInSc7	obsah
upomínal	upomínat	k5eAaImAgMnS	upomínat
na	na	k7c4	na
revoluční	revoluční	k2eAgFnPc4d1	revoluční
události	událost	k1gFnPc4	událost
<g/>
.	.	kIx.	.
</s>
<s>
Přísně	přísně	k6eAd1	přísně
byly	být	k5eAaImAgFnP	být
zakázány	zakázat	k5eAaPmNgFnP	zakázat
i	i	k9	i
všechny	všechen	k3xTgMnPc4	všechen
cestopisné	cestopisný	k2eAgMnPc4d1	cestopisný
průvodce	průvodce	k1gMnPc4	průvodce
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
připomínaly	připomínat	k5eAaImAgInP	připomínat
měsíce	měsíc	k1gInPc1	měsíc
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Bastily	Bastila	k1gFnSc2	Bastila
a	a	k8xC	a
v	v	k7c6	v
tiskárnách	tiskárna	k1gFnPc6	tiskárna
byly	být	k5eAaImAgFnP	být
neustále	neustále	k6eAd1	neustále
prováděny	prováděn	k2eAgFnPc1d1	prováděna
policejní	policejní	k2eAgFnPc1d1	policejní
prohlídky	prohlídka	k1gFnPc1	prohlídka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
jara	jaro	k1gNnSc2	jaro
roku	rok	k1gInSc2	rok
1807	[number]	k4	1807
se	se	k3xPyFc4	se
Napoleon	Napoleon	k1gMnSc1	Napoleon
definitivně	definitivně	k6eAd1	definitivně
rozešel	rozejít	k5eAaPmAgMnS	rozejít
s	s	k7c7	s
ideály	ideál	k1gInPc7	ideál
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
když	když	k8xS	když
kolem	kolem	k6eAd1	kolem
něj	on	k3xPp3gNnSc4	on
začala	začít	k5eAaPmAgFnS	začít
vznikat	vznikat	k5eAaImF	vznikat
nová	nový	k2eAgFnSc1d1	nová
aristokracie	aristokracie	k1gFnSc1	aristokracie
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
zavedl	zavést	k5eAaPmAgInS	zavést
úřad	úřad	k1gInSc1	úřad
císařského	císařský	k2eAgMnSc2d1	císařský
maršála	maršál	k1gMnSc2	maršál
a	a	k8xC	a
zřídil	zřídit	k5eAaPmAgInS	zřídit
Řád	řád	k1gInSc1	řád
čestné	čestný	k2eAgFnSc2d1	čestná
legie	legie	k1gFnSc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Císařovnu	císařovna	k1gFnSc4	císařovna
Josefínu	Josefína	k1gFnSc4	Josefína
obklopily	obklopit	k5eAaPmAgFnP	obklopit
dvorní	dvorní	k2eAgFnPc1d1	dvorní
dámy	dáma	k1gFnPc1	dáma
ze	z	k7c2	z
starých	starý	k2eAgFnPc2d1	stará
šlechtických	šlechtický	k2eAgFnPc2d1	šlechtická
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
byly	být	k5eAaImAgInP	být
zavedeny	zaveden	k2eAgInPc1d1	zaveden
tituly	titul	k1gInPc1	titul
vévodů	vévoda	k1gMnPc2	vévoda
<g/>
,	,	kIx,	,
knížat	kníže	k1gMnPc2wR	kníže
<g/>
,	,	kIx,	,
hrabat	hrabě	k1gNnPc2	hrabě
a	a	k8xC	a
baronů	baron	k1gMnPc2	baron
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Francie	Francie	k1gFnSc2	Francie
se	se	k3xPyFc4	se
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
staronovým	staronový	k2eAgNnSc7d1	staronové
uspořádáním	uspořádání	k1gNnSc7	uspořádání
smířili	smířit	k5eAaPmAgMnP	smířit
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
chápali	chápat	k5eAaImAgMnP	chápat
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
úřadu	úřad	k1gInSc3	úřad
císaře	císař	k1gMnSc2	císař
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
císařský	císařský	k2eAgInSc4d1	císařský
dvůr	dvůr	k1gInSc4	dvůr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Církevní	církevní	k2eAgFnSc1d1	církevní
politika	politika	k1gFnSc1	politika
===	===	k?	===
</s>
</p>
<p>
<s>
Součástí	součást	k1gFnSc7	součást
dědictví	dědictví	k1gNnSc2	dědictví
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
Napoleon	Napoleon	k1gMnSc1	Napoleon
získal	získat	k5eAaPmAgMnS	získat
po	po	k7c6	po
zmatcích	zmatek	k1gInPc6	zmatek
Velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
rozkol	rozkol	k1gInSc1	rozkol
v	v	k7c6	v
římskokatolické	římskokatolický	k2eAgFnSc6d1	Římskokatolická
církvi	církev	k1gFnSc6	církev
a	a	k8xC	a
naprosté	naprostý	k2eAgNnSc1d1	naprosté
odcizení	odcizení	k1gNnSc1	odcizení
Římu	Řím	k1gInSc3	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
papeže	papež	k1gMnSc4	papež
Pia	Pius	k1gMnSc2	Pius
VI	VI	kA	VI
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c4	v
Valence	valence	k1gFnPc4	valence
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1799	[number]	k4	1799
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Napoleon	Napoleon	k1gMnSc1	Napoleon
chopil	chopit	k5eAaPmAgMnS	chopit
nabízené	nabízený	k2eAgFnPc4d1	nabízená
příležitosti	příležitost	k1gFnPc4	příležitost
a	a	k8xC	a
z	z	k7c2	z
ryze	ryze	k6eAd1	ryze
pragmatických	pragmatický	k2eAgInPc2d1	pragmatický
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
s	s	k7c7	s
novým	nový	k2eAgMnSc7d1	nový
náměstkem	náměstek	k1gMnSc7	náměstek
Ježíše	Ježíš	k1gMnSc2	Ježíš
Krista	Kristus	k1gMnSc2	Kristus
Piem	Pius	k1gMnSc7	Pius
VII	VII	kA	VII
<g/>
.	.	kIx.	.
pokusil	pokusit	k5eAaPmAgMnS	pokusit
usmířit	usmířit	k5eAaPmF	usmířit
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
byl	být	k5eAaImAgMnS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
obezřetný	obezřetný	k2eAgMnSc1d1	obezřetný
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
od	od	k7c2	od
září	září	k1gNnSc2	září
roku	rok	k1gInSc2	rok
1800	[number]	k4	1800
byly	být	k5eAaImAgFnP	být
papežské	papežský	k2eAgFnPc1d1	Papežská
državy	država	k1gFnPc1	država
okupované	okupovaný	k2eAgFnPc1d1	okupovaná
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
k	k	k7c3	k
dohodě	dohoda	k1gFnSc3	dohoda
došlo	dojít	k5eAaPmAgNnS	dojít
teprve	teprve	k6eAd1	teprve
až	až	k9	až
po	po	k7c6	po
Napoleonově	Napoleonův	k2eAgInSc6d1	Napoleonův
přímém	přímý	k2eAgInSc6d1	přímý
nátlaku	nátlak	k1gInSc6	nátlak
<g/>
.	.	kIx.	.
</s>
<s>
Konkordát	konkordát	k1gInSc1	konkordát
byl	být	k5eAaImAgInS	být
podepsán	podepsat	k5eAaPmNgInS	podepsat
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1801	[number]	k4	1801
a	a	k8xC	a
i	i	k9	i
přes	přes	k7c4	přes
poměrnou	poměrný	k2eAgFnSc4d1	poměrná
stručnost	stručnost	k1gFnSc4	stručnost
budil	budit	k5eAaImAgInS	budit
dojem	dojem	k1gInSc1	dojem
rozumného	rozumný	k2eAgInSc2d1	rozumný
kompromisu	kompromis	k1gInSc2	kompromis
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	napoleon	k1gInSc1	napoleon
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
uznal	uznat	k5eAaPmAgMnS	uznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
katolicismus	katolicismus	k1gInSc1	katolicismus
je	být	k5eAaImIp3nS	být
náboženství	náboženství	k1gNnSc1	náboženství
velké	velký	k2eAgFnSc2d1	velká
většiny	většina	k1gFnSc2	většina
Francouzů	Francouz	k1gMnPc2	Francouz
a	a	k8xC	a
jako	jako	k8xC	jako
takový	takový	k3xDgInSc1	takový
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
svobodně	svobodně	k6eAd1	svobodně
a	a	k8xC	a
otevřeně	otevřeně	k6eAd1	otevřeně
praktikován	praktikovat	k5eAaImNgInS	praktikovat
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
policejními	policejní	k2eAgInPc7d1	policejní
předpisy	předpis	k1gInPc7	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Pius	Pius	k1gMnSc1	Pius
VII	VII	kA	VII
<g/>
.	.	kIx.	.
formálně	formálně	k6eAd1	formálně
uznal	uznat	k5eAaPmAgInS	uznat
legitimitu	legitimita	k1gFnSc4	legitimita
konzulární	konzulární	k2eAgFnSc2d1	konzulární
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
mu	on	k3xPp3gInSc3	on
bylo	být	k5eAaImAgNnS	být
přiznáno	přiznán	k2eAgNnSc4d1	přiznáno
právo	právo	k1gNnSc4	právo
kanonické	kanonický	k2eAgFnSc2d1	kanonická
investitury	investitura	k1gFnSc2	investitura
<g/>
.	.	kIx.	.
</s>
<s>
Konkordát	konkordát	k1gInSc1	konkordát
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
množství	množství	k1gNnSc4	množství
dalších	další	k2eAgNnPc2d1	další
ustanovení	ustanovení	k1gNnPc2	ustanovení
jak	jak	k8xC	jak
o	o	k7c6	o
biskupech	biskup	k1gInPc6	biskup
i	i	k8xC	i
nižším	nízký	k2eAgInSc6d2	nižší
kléru	klér	k1gInSc6	klér
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
o	o	k7c6	o
revolučním	revoluční	k2eAgNnSc6d1	revoluční
uspořádání	uspořádání	k1gNnSc6	uspořádání
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
však	však	k9	však
Napoleon	napoleon	k1gInSc1	napoleon
k	k	k7c3	k
listině	listina	k1gFnSc3	listina
připojil	připojit	k5eAaPmAgMnS	připojit
dalších	další	k2eAgNnPc2d1	další
77	[number]	k4	77
organických	organický	k2eAgInPc2d1	organický
článků	článek	k1gInPc2	článek
<g/>
,	,	kIx,	,
kterými	který	k3yQgFnPc7	který
byla	být	k5eAaImAgFnS	být
svoboda	svoboda	k1gFnSc1	svoboda
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
galikánském	galikánský	k2eAgMnSc6d1	galikánský
duchu	duch	k1gMnSc6	duch
velmi	velmi	k6eAd1	velmi
omezena	omezen	k2eAgFnSc1d1	omezena
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nebyl	být	k5eNaImAgInS	být
požádán	požádat	k5eAaPmNgMnS	požádat
o	o	k7c6	o
konzultaci	konzultace	k1gFnSc6	konzultace
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
cítil	cítit	k5eAaImAgMnS	cítit
uražen	urazit	k5eAaPmNgMnS	urazit
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
nakonec	nakonec	k6eAd1	nakonec
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
s	s	k7c7	s
účastí	účast	k1gFnSc7	účast
na	na	k7c6	na
císařské	císařský	k2eAgFnSc6d1	císařská
korunovaci	korunovace	k1gFnSc6	korunovace
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
si	se	k3xPyFc3	se
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
kroku	krok	k1gInSc2	krok
sliboval	slibovat	k5eAaImAgInS	slibovat
posílení	posílení	k1gNnSc4	posílení
vlastní	vlastní	k2eAgFnSc2d1	vlastní
mocenské	mocenský	k2eAgFnSc2d1	mocenská
pozice	pozice	k1gFnSc2	pozice
a	a	k8xC	a
doufal	doufat	k5eAaImAgMnS	doufat
v	v	k7c6	v
obnovení	obnovení	k1gNnSc6	obnovení
papežského	papežský	k2eAgInSc2d1	papežský
státu	stát	k1gInSc2	stát
v	v	k7c6	v
dřívějším	dřívější	k2eAgInSc6d1	dřívější
územním	územní	k2eAgInSc6d1	územní
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
požadavku	požadavek	k1gInSc3	požadavek
však	však	k9	však
francouzský	francouzský	k2eAgMnSc1d1	francouzský
císař	císař	k1gMnSc1	císař
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
vyhovět	vyhovět	k5eAaPmF	vyhovět
a	a	k8xC	a
papež	papež	k1gMnSc1	papež
porušil	porušit	k5eAaPmAgMnS	porušit
ustanovení	ustanovení	k1gNnSc4	ustanovení
konkordátu	konkordát	k1gInSc2	konkordát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nepotvrdil	potvrdit	k5eNaPmAgMnS	potvrdit
Napoleonem	napoleon	k1gInSc7	napoleon
jmenované	jmenovaný	k2eAgInPc4d1	jmenovaný
biskupy	biskup	k1gInPc4	biskup
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
zdráhal	zdráhat	k5eAaImAgInS	zdráhat
uzavřít	uzavřít	k5eAaPmF	uzavřít
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
spojenectví	spojenectví	k1gNnSc2	spojenectví
proti	proti	k7c3	proti
Anglii	Anglie	k1gFnSc3	Anglie
a	a	k8xC	a
své	svůj	k3xOyFgInPc4	svůj
přístavy	přístav	k1gInPc4	přístav
ponechával	ponechávat	k5eAaImAgMnS	ponechávat
otevřeny	otevřít	k5eAaPmNgFnP	otevřít
britským	britský	k2eAgFnPc3d1	britská
lodím	loď	k1gFnPc3	loď
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	napoleon	k1gInSc1	napoleon
proto	proto	k8xC	proto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1808	[number]	k4	1808
nechal	nechat	k5eAaPmAgInS	nechat
obsadit	obsadit	k5eAaPmF	obsadit
Řím	Řím	k1gInSc1	Řím
a	a	k8xC	a
k	k	k7c3	k
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubnu	duben	k1gInSc6	duben
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
anektoval	anektovat	k5eAaBmAgMnS	anektovat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
italského	italský	k2eAgNnSc2d1	italské
království	království	k1gNnSc2	království
provincie	provincie	k1gFnSc2	provincie
Urbino	Urbin	k2eAgNnSc1d1	Urbino
<g/>
,	,	kIx,	,
Macerata	Macerata	k1gFnSc1	Macerata
<g/>
,	,	kIx,	,
Ancona	Ancona	k1gFnSc1	Ancona
a	a	k8xC	a
Camerino	Camerino	k1gNnSc1	Camerino
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
strategických	strategický	k2eAgInPc2d1	strategický
důvodů	důvod	k1gInPc2	důvod
pak	pak	k6eAd1	pak
dekrety	dekret	k1gInPc1	dekret
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1809	[number]	k4	1809
připojil	připojit	k5eAaPmAgInS	připojit
i	i	k9	i
zbytek	zbytek	k1gInSc1	zbytek
papežských	papežský	k2eAgFnPc2d1	Papežská
držav	država	k1gFnPc2	država
k	k	k7c3	k
Francii	Francie	k1gFnSc3	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
čin	čin	k1gInSc4	čin
Pius	Pius	k1gMnSc1	Pius
VII	VII	kA	VII
<g/>
.	.	kIx.	.
uveřejnil	uveřejnit	k5eAaPmAgInS	uveřejnit
bulu	bula	k1gFnSc4	bula
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
uvrhl	uvrhnout	k5eAaPmAgMnS	uvrhnout
Napoleona	Napoleon	k1gMnSc2	Napoleon
do	do	k7c2	do
klatby	klatba	k1gFnSc2	klatba
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
však	však	k9	však
docílil	docílit	k5eAaPmAgMnS	docílit
jen	jen	k9	jen
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
exkomunikovaný	exkomunikovaný	k2eAgMnSc1d1	exkomunikovaný
císař	císař	k1gMnSc1	císař
nechal	nechat	k5eAaPmAgMnS	nechat
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
zajmout	zajmout	k5eAaPmF	zajmout
a	a	k8xC	a
odvézt	odvézt	k5eAaPmF	odvézt
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
po	po	k7c4	po
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
držen	držen	k2eAgInSc4d1	držen
ve	v	k7c6	v
vazbě	vazba	k1gFnSc6	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
sídlil	sídlit	k5eAaImAgInS	sídlit
v	v	k7c6	v
Savoně	Savon	k1gInSc6	Savon
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1812	[number]	k4	1812
pak	pak	k6eAd1	pak
ve	v	k7c6	v
Fontainebleau	Fontainebleaus	k1gInSc6	Fontainebleaus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pobýval	pobývat	k5eAaImAgMnS	pobývat
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
četnického	četnický	k2eAgMnSc4d1	četnický
kapitána	kapitán	k1gMnSc4	kapitán
Lagrose	Lagrosa	k1gFnSc6	Lagrosa
<g/>
,	,	kIx,	,
převlečeného	převlečený	k2eAgMnSc2d1	převlečený
za	za	k7c4	za
komořího	komoří	k1gMnSc4	komoří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1811	[number]	k4	1811
se	se	k3xPyFc4	se
Napoleon	Napoleon	k1gMnSc1	Napoleon
pokusil	pokusit	k5eAaPmAgMnS	pokusit
papeže	papež	k1gMnSc4	papež
přimět	přimět	k5eAaPmF	přimět
k	k	k7c3	k
povolnosti	povolnost	k1gFnPc4	povolnost
svoláním	svolání	k1gNnSc7	svolání
národního	národní	k2eAgInSc2d1	národní
koncilu	koncil	k1gInSc2	koncil
<g/>
,	,	kIx,	,
ničeho	nic	k3yNnSc2	nic
však	však	k9	však
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Piovi	Pius	k1gMnSc3	Pius
VII	VII	kA	VII
<g/>
.	.	kIx.	.
proto	proto	k8xC	proto
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
možnost	možnost	k1gFnSc4	možnost
návratu	návrat	k1gInSc2	návrat
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
uzná	uznat	k5eAaPmIp3nS	uznat
tam	tam	k6eAd1	tam
dosazenou	dosazený	k2eAgFnSc4d1	dosazená
světskou	světský	k2eAgFnSc4d1	světská
moc	moc	k1gFnSc4	moc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
tento	tento	k3xDgInSc1	tento
návrh	návrh	k1gInSc1	návrh
nebyl	být	k5eNaImAgInS	být
vyslyšen	vyslyšet	k5eAaPmNgInS	vyslyšet
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1813	[number]	k4	1813
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
oboustrannému	oboustranný	k2eAgNnSc3d1	oboustranné
formálnímu	formální	k2eAgNnSc3d1	formální
setkání	setkání	k1gNnSc3	setkání
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
císař	císař	k1gMnSc1	císař
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
usmířit	usmířit	k5eAaPmF	usmířit
alespoň	alespoň	k9	alespoň
katolíky	katolík	k1gMnPc7	katolík
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nejevili	jevit	k5eNaImAgMnP	jevit
pro	pro	k7c4	pro
zacházení	zacházení	k1gNnSc4	zacházení
s	s	k7c7	s
hlavou	hlava	k1gFnSc7	hlava
církve	církev	k1gFnSc2	církev
mnoho	mnoho	k4c4	mnoho
sympatií	sympatie	k1gFnPc2	sympatie
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
panovníci	panovník	k1gMnPc1	panovník
si	se	k3xPyFc3	se
prokazovali	prokazovat	k5eAaImAgMnP	prokazovat
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
zdvořilost	zdvořilost	k1gFnSc4	zdvořilost
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
schůzka	schůzka	k1gFnSc1	schůzka
nepřinesla	přinést	k5eNaPmAgFnS	přinést
nic	nic	k3yNnSc1	nic
reálného	reálný	k2eAgNnSc2d1	reálné
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
sice	sice	k8xC	sice
donutil	donutit	k5eAaPmAgMnS	donutit
Pia	Pius	k1gMnSc4	Pius
VII	VII	kA	VII
<g/>
.	.	kIx.	.
podepsat	podepsat	k5eAaPmF	podepsat
nový	nový	k2eAgInSc4d1	nový
konkordát	konkordát	k1gInSc4	konkordát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Řím	Řím	k1gInSc1	Řím
mu	on	k3xPp3gNnSc3	on
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
(	(	kIx(	(
<g/>
navíc	navíc	k6eAd1	navíc
Pius	Pius	k1gMnSc1	Pius
VII	VII	kA	VII
<g/>
.	.	kIx.	.
svůj	svůj	k3xOyFgInSc4	svůj
závazek	závazek	k1gInSc4	závazek
po	po	k7c6	po
dvou	dva	k4xCgInPc6	dva
měsících	měsíc	k1gInPc6	měsíc
odvolal	odvolat	k5eAaPmAgInS	odvolat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Svého	svůj	k3xOyFgMnSc4	svůj
zajatce	zajatec	k1gMnSc4	zajatec
propustil	propustit	k5eAaPmAgMnS	propustit
až	až	k6eAd1	až
v	v	k7c6	v
samém	samý	k3xTgInSc6	samý
závěru	závěr	k1gInSc6	závěr
prvního	první	k4xOgNnSc2	první
císařství	císařství	k1gNnSc2	císařství
10	[number]	k4	10
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1814	[number]	k4	1814
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
se	se	k3xPyFc4	se
papež	papež	k1gMnSc1	papež
vrátil	vrátit	k5eAaPmAgMnS	vrátit
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Koloniální	koloniální	k2eAgFnSc1d1	koloniální
politika	politika	k1gFnSc1	politika
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
světě	svět	k1gInSc6	svět
===	===	k?	===
</s>
</p>
<p>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
nástupu	nástup	k1gInSc6	nástup
Napoleona	Napoleon	k1gMnSc2	Napoleon
Bonaparta	Bonapart	k1gMnSc2	Bonapart
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
Francie	Francie	k1gFnSc2	Francie
se	se	k3xPyFc4	se
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
budování	budování	k1gNnSc6	budování
francouzské	francouzský	k2eAgFnSc2d1	francouzská
koloniální	koloniální	k2eAgFnSc2d1	koloniální
říše	říš	k1gFnSc2	říš
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
přiměl	přimět	k5eAaPmAgInS	přimět
Španělsko	Španělsko	k1gNnSc4	Španělsko
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Francii	Francie	k1gFnSc4	Francie
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
Louisianu	Louisian	k1gInSc2	Louisian
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
předtím	předtím	k6eAd1	předtím
Francouzi	Francouz	k1gMnPc1	Francouz
vzdali	vzdát	k5eAaPmAgMnP	vzdát
po	po	k7c6	po
prohrané	prohraný	k2eAgFnSc6d1	prohraná
sedmileté	sedmiletý	k2eAgFnSc6d1	sedmiletá
válce	válka	k1gFnSc6	válka
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
velmi	velmi	k6eAd1	velmi
znepokojovalo	znepokojovat	k5eAaImAgNnS	znepokojovat
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1795	[number]	k4	1795
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
mezi	mezi	k7c7	mezi
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Španělskem	Španělsko	k1gNnSc7	Španělsko
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
basilejský	basilejský	k2eAgInSc1d1	basilejský
mír	mír	k1gInSc1	mír
<g/>
,	,	kIx,	,
patřil	patřit	k5eAaImAgInS	patřit
Francii	Francie	k1gFnSc4	Francie
oficiálně	oficiálně	k6eAd1	oficiálně
</s>
<s>
také	také	k9	také
celý	celý	k2eAgInSc4d1	celý
ostrov	ostrov	k1gInSc4	ostrov
Santo	Sant	k2eAgNnSc1d1	Santo
Domingo	Domingo	k1gNnSc1	Domingo
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
Hispaniola	Hispaniola	k1gFnSc1	Hispaniola
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jí	on	k3xPp3gFnSc7	on
patřila	patřit	k5eAaImAgFnS	patřit
jen	jen	k9	jen
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
ostrova	ostrov	k1gInSc2	ostrov
s	s	k7c7	s
francouzským	francouzský	k2eAgInSc7d1	francouzský
názvem	název	k1gInSc7	název
Saint-Domingue	Saint-Domingu	k1gFnSc2	Saint-Domingu
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
předání	předání	k1gNnSc3	předání
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
územím	území	k1gNnSc7	území
španělské	španělský	k2eAgFnSc2d1	španělská
kolonie	kolonie	k1gFnSc2	kolonie
(	(	kIx(	(
<g/>
s	s	k7c7	s
názvem	název	k1gInSc7	název
Santo	Sant	k2eAgNnSc1d1	Santo
Domingo	Domingo	k1gNnSc1	Domingo
<g/>
)	)	kIx)	)
na	na	k7c6	na
východě	východ	k1gInSc6	východ
ostrova	ostrov	k1gInSc2	ostrov
nikdy	nikdy	k6eAd1	nikdy
nedošlo	dojít	k5eNaPmAgNnS	dojít
a	a	k8xC	a
španělské	španělský	k2eAgInPc4d1	španělský
úřady	úřad	k1gInPc4	úřad
ji	on	k3xPp3gFnSc4	on
nadále	nadále	k6eAd1	nadále
fakticky	fakticky	k6eAd1	fakticky
ovládaly	ovládat	k5eAaImAgFnP	ovládat
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
hodlal	hodlat	k5eAaImAgMnS	hodlat
Napoleon	napoleon	k1gInSc4	napoleon
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
brzy	brzy	k6eAd1	brzy
udělat	udělat	k5eAaPmF	udělat
jádro	jádro	k1gNnSc4	jádro
svého	svůj	k3xOyFgNnSc2	svůj
budoucího	budoucí	k2eAgNnSc2d1	budoucí
karibského	karibský	k2eAgNnSc2d1	Karibské
impéria	impérium	k1gNnSc2	impérium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
aspektem	aspekt	k1gInSc7	aspekt
Napoleonovy	Napoleonův	k2eAgFnSc2d1	Napoleonova
koloniální	koloniální	k2eAgFnSc2d1	koloniální
politiky	politika	k1gFnSc2	politika
byl	být	k5eAaImAgMnS	být
jeho	on	k3xPp3gInSc4	on
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
černošským	černošský	k2eAgMnPc3d1	černošský
obyvatelům	obyvatel	k1gMnPc3	obyvatel
kolonií	kolonie	k1gFnPc2	kolonie
a	a	k8xC	a
k	k	k7c3	k
otroctví	otroctví	k1gNnSc3	otroctví
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Napoleon	Napoleon	k1gMnSc1	Napoleon
měl	mít	k5eAaImAgMnS	mít
vůči	vůči	k7c3	vůči
černochům	černoch	k1gMnPc3	černoch
předsudky	předsudek	k1gInPc1	předsudek
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
jako	jako	k8xS	jako
student	student	k1gMnSc1	student
vojenského	vojenský	k2eAgNnSc2d1	vojenské
učiliště	učiliště	k1gNnSc2	učiliště
v	v	k7c6	v
Brienne	Brienn	k1gInSc5	Brienn
šikanoval	šikanovat	k5eAaImAgMnS	šikanovat
spolužáka	spolužák	k1gMnSc4	spolužák
rasově	rasově	k6eAd1	rasově
smíšeného	smíšený	k2eAgInSc2d1	smíšený
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
generálů	generál	k1gMnPc2	generál
<g/>
,	,	kIx,	,
Thomasem	Thomas	k1gMnSc7	Thomas
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Dumasem	Dumas	k1gMnSc7	Dumas
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
také	také	k9	také
rasově	rasově	k6eAd1	rasově
smíšeného	smíšený	k2eAgInSc2d1	smíšený
původu	původ	k1gInSc2	původ
a	a	k8xC	a
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
kolonie	kolonie	k1gFnSc2	kolonie
Saint-Domingue	Saint-Domingu	k1gFnSc2	Saint-Domingu
<g/>
,	,	kIx,	,
jednal	jednat	k5eAaImAgMnS	jednat
s	s	k7c7	s
neskrývaným	skrývaný	k2eNgNnSc7d1	neskrývané
pohrdáním	pohrdání	k1gNnSc7	pohrdání
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
svých	svůj	k3xOyFgInPc2	svůj
plánů	plán	k1gInPc2	plán
s	s	k7c7	s
ostrovem	ostrov	k1gInSc7	ostrov
zahrnul	zahrnout	k5eAaPmAgInS	zahrnout
i	i	k9	i
klauzuli	klauzule	k1gFnSc4	klauzule
<g/>
,	,	kIx,	,
že	že	k8xS	že
bílé	bílý	k2eAgFnPc1d1	bílá
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
dopustily	dopustit	k5eAaPmAgFnP	dopustit
rasového	rasový	k2eAgNnSc2d1	rasové
prohřešku	prohřešek	k1gInSc2	prohřešek
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
měly	mít	k5eAaImAgFnP	mít
sex	sex	k1gInSc4	sex
s	s	k7c7	s
nebělošskými	bělošský	k2eNgMnPc7d1	bělošský
důstojníky	důstojník	k1gMnPc7	důstojník
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
deportovány	deportovat	k5eAaBmNgFnP	deportovat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k9	jednou
Napoleon	Napoleon	k1gMnSc1	Napoleon
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsem	být	k5eAaImIp1nS	být
pro	pro	k7c4	pro
bílé	bílý	k2eAgNnSc4d1	bílé
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsem	být	k5eAaImIp1nS	být
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
;	;	kIx,	;
nemám	mít	k5eNaImIp1nS	mít
jiného	jiný	k2eAgInSc2d1	jiný
důvodu	důvod	k1gInSc2	důvod
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
důvod	důvod	k1gInSc1	důvod
je	být	k5eAaImIp3nS	být
dostatečný	dostatečný	k2eAgInSc1d1	dostatečný
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
někdo	někdo	k3yInSc1	někdo
zaručit	zaručit	k5eAaPmF	zaručit
svobodu	svoboda	k1gFnSc4	svoboda
Afričanům	Afričan	k1gMnPc3	Afričan
<g/>
,	,	kIx,	,
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
neměli	mít	k5eNaImAgMnP	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
civilizaci	civilizace	k1gFnSc4	civilizace
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
Napoleonova	Napoleonův	k2eAgFnSc1d1	Napoleonova
ústava	ústava	k1gFnSc1	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1799	[number]	k4	1799
znamenala	znamenat	k5eAaImAgFnS	znamenat
pro	pro	k7c4	pro
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
kolonií	kolonie	k1gFnSc7	kolonie
velkou	velký	k2eAgFnSc7d1	velká
nejistotu	nejistota	k1gFnSc4	nejistota
<g/>
:	:	kIx,	:
neobsahovala	obsahovat	k5eNaImAgFnS	obsahovat
prohlášení	prohlášení	k1gNnSc4	prohlášení
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
předchozích	předchozí	k2eAgFnPc2d1	předchozí
revolučních	revoluční	k2eAgFnPc2d1	revoluční
ústav	ústava	k1gFnPc2	ústava
<g/>
,	,	kIx,	,
a	a	k8xC	a
článek	článek	k1gInSc1	článek
91	[number]	k4	91
stanovoval	stanovovat	k5eAaImAgInS	stanovovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kolonie	kolonie	k1gFnSc2	kolonie
budou	být	k5eAaImBp3nP	být
spravovány	spravovat	k5eAaImNgInP	spravovat
"	"	kIx"	"
<g/>
zvláštními	zvláštní	k2eAgInPc7d1	zvláštní
zákony	zákon	k1gInPc7	zákon
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
naznačovalo	naznačovat	k5eAaImAgNnS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gMnPc1	jejich
obyvatelé	obyvatel	k1gMnPc1	obyvatel
nebudou	být	k5eNaImBp3nP	být
mít	mít	k5eAaImF	mít
stejná	stejný	k2eAgNnPc1d1	stejné
práva	právo	k1gNnPc1	právo
jako	jako	k8xS	jako
obyvatelé	obyvatel	k1gMnPc1	obyvatel
mateřské	mateřský	k2eAgFnSc2d1	mateřská
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
to	ten	k3xDgNnSc1	ten
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
pocítili	pocítit	k5eAaPmAgMnP	pocítit
například	například	k6eAd1	například
otroci	otrok	k1gMnPc1	otrok
v	v	k7c6	v
ostrovních	ostrovní	k2eAgFnPc6d1	ostrovní
koloniích	kolonie	k1gFnPc6	kolonie
v	v	k7c6	v
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
<g/>
:	:	kIx,	:
ačkoliv	ačkoliv	k8xS	ačkoliv
teoreticky	teoreticky	k6eAd1	teoreticky
bylo	být	k5eAaImAgNnS	být
otroctví	otroctví	k1gNnSc4	otroctví
na	na	k7c6	na
všech	všecek	k3xTgNnPc6	všecek
francouzských	francouzský	k2eAgNnPc6d1	francouzské
zámořských	zámořský	k2eAgNnPc6d1	zámořské
územích	území	k1gNnPc6	území
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
již	již	k9	již
v	v	k7c6	v
době	doba	k1gFnSc6	doba
první	první	k4xOgFnSc2	první
revoluční	revoluční	k2eAgFnSc2d1	revoluční
republiky	republika	k1gFnSc2	republika
roku	rok	k1gInSc2	rok
1794	[number]	k4	1794
<g/>
,	,	kIx,	,
bílí	bílý	k2eAgMnPc1d1	bílý
kolonisté	kolonista	k1gMnPc1	kolonista
těmto	tento	k3xDgFnPc3	tento
snahám	snaha	k1gFnPc3	snaha
úspěšně	úspěšně	k6eAd1	úspěšně
odporovali	odporovat	k5eAaImAgMnP	odporovat
<g/>
,	,	kIx,	,
až	až	k8xS	až
je	být	k5eAaImIp3nS	být
nakonec	nakonec	k6eAd1	nakonec
Napoleon	napoleon	k1gInSc4	napoleon
na	na	k7c6	na
základě	základ	k1gInSc6	základ
zmíněného	zmíněný	k2eAgNnSc2d1	zmíněné
ustanovení	ustanovení	k1gNnSc2	ustanovení
nové	nový	k2eAgFnSc2d1	nová
ústavy	ústava	k1gFnSc2	ústava
odvolal	odvolat	k5eAaPmAgInS	odvolat
<g/>
.	.	kIx.	.
<g/>
Napoleonův	Napoleonův	k2eAgInSc1d1	Napoleonův
postoj	postoj	k1gInSc1	postoj
k	k	k7c3	k
otázce	otázka	k1gFnSc3	otázka
otroctví	otroctví	k1gNnSc1	otroctví
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
již	již	k9	již
k	k	k7c3	k
faktickému	faktický	k2eAgNnSc3d1	faktické
zrušení	zrušení	k1gNnSc3	zrušení
otroctví	otroctví	k1gNnSc2	otroctví
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
,	,	kIx,	,
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
zcela	zcela	k6eAd1	zcela
jednoznačný	jednoznačný	k2eAgInSc1d1	jednoznačný
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1799	[number]	k4	1799
a	a	k8xC	a
1800	[number]	k4	1800
se	se	k3xPyFc4	se
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
popud	popud	k1gInSc4	popud
k	k	k7c3	k
otázce	otázka	k1gFnSc3	otázka
otroctví	otroctví	k1gNnSc2	otroctví
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
řada	řada	k1gFnSc1	řada
osobností	osobnost	k1gFnPc2	osobnost
obeznámených	obeznámený	k2eAgFnPc2d1	obeznámená
s	s	k7c7	s
problematikou	problematika	k1gFnSc7	problematika
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
obdržel	obdržet	k5eAaPmAgMnS	obdržet
celé	celý	k2eAgNnSc4d1	celé
spektrum	spektrum	k1gNnSc4	spektrum
názorů	názor	k1gInPc2	názor
od	od	k7c2	od
požadavků	požadavek	k1gInPc2	požadavek
na	na	k7c4	na
obnovení	obnovení	k1gNnSc4	obnovení
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mělo	mít	k5eAaImAgNnS	mít
pomoci	pomoct	k5eAaPmF	pomoct
znovu	znovu	k6eAd1	znovu
postavit	postavit	k5eAaPmF	postavit
na	na	k7c4	na
nohy	noha	k1gFnPc4	noha
místní	místní	k2eAgFnSc2d1	místní
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
návrat	návrat	k1gInSc1	návrat
k	k	k7c3	k
předchozímu	předchozí	k2eAgInSc3d1	předchozí
stavu	stav	k1gInSc3	stav
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
nemožný	možný	k2eNgInSc1d1	nemožný
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
toto	tento	k3xDgNnSc4	tento
váhání	váhání	k1gNnSc4	váhání
se	se	k3xPyFc4	se
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1799	[number]	k4	1799
snažil	snažit	k5eAaImAgMnS	snažit
ujistit	ujistit	k5eAaPmF	ujistit
obyvatele	obyvatel	k1gMnSc4	obyvatel
Santo	Santo	k1gNnSc1	Santo
Dominga	Dominga	k1gFnSc1	Dominga
<g/>
,	,	kIx,	,
že	že	k8xS	že
principy	princip	k1gInPc1	princip
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
rovnosti	rovnost	k1gFnSc2	rovnost
černošského	černošský	k2eAgNnSc2d1	černošské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
ostrova	ostrov	k1gInSc2	ostrov
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
Francii	Francie	k1gFnSc4	Francie
svaté	svatá	k1gFnSc2	svatá
a	a	k8xC	a
nedotknutelné	nedotknutelné	k?	nedotknutelné
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
místního	místní	k2eAgMnSc4d1	místní
vůdce	vůdce	k1gMnSc4	vůdce
<g/>
,	,	kIx,	,
bývalého	bývalý	k2eAgMnSc4d1	bývalý
otroka	otrok	k1gMnSc4	otrok
Toussainta	Toussaint	k1gMnSc4	Toussaint
Louverture	Louvertur	k1gMnSc5	Louvertur
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
však	však	k9	však
neudělalo	udělat	k5eNaPmAgNnS	udělat
velký	velký	k2eAgInSc4d1	velký
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
obyvatelé	obyvatel	k1gMnPc1	obyvatel
ostrova	ostrov	k1gInSc2	ostrov
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
svobodu	svoboda	k1gFnSc4	svoboda
nevděčí	vděčit	k5eNaImIp3nS	vděčit
Francouzům	Francouz	k1gMnPc3	Francouz
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
koloniích	kolonie	k1gFnPc6	kolonie
nadále	nadále	k6eAd1	nadále
udržují	udržovat	k5eAaImIp3nP	udržovat
otroctví	otroctví	k1gNnSc4	otroctví
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
své	svůj	k3xOyFgFnSc3	svůj
vlastní	vlastní	k2eAgFnSc3d1	vlastní
síle	síla	k1gFnSc3	síla
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
například	například	k6eAd1	například
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
otroctví	otroctví	k1gNnSc1	otroctví
zůstane	zůstat	k5eAaPmIp3nS	zůstat
zachováno	zachovat	k5eAaPmNgNnS	zachovat
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Réunion	Réunion	k1gInSc1	Réunion
(	(	kIx(	(
<g/>
který	který	k3yIgMnSc1	který
pak	pak	k6eAd1	pak
roku	rok	k1gInSc2	rok
1809	[number]	k4	1809
nechal	nechat	k5eAaPmAgMnS	nechat
přejmenovat	přejmenovat	k5eAaPmF	přejmenovat
na	na	k7c4	na
Île	Île	k1gFnSc4	Île
de	de	k?	de
Bonaparte	bonapart	k1gInSc5	bonapart
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
)	)	kIx)	)
či	či	k8xC	či
na	na	k7c6	na
Martiniku	Martinik	k1gInSc6	Martinik
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
geografickou	geografický	k2eAgFnSc4d1	geografická
blízkost	blízkost	k1gFnSc4	blízkost
pro	pro	k7c4	pro
obyvatele	obyvatel	k1gMnPc4	obyvatel
kolonie	kolonie	k1gFnSc2	kolonie
Saint-Domingue	Saint-Domingu	k1gInSc2	Saint-Domingu
zvláště	zvláště	k6eAd1	zvláště
velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc1	význam
<g/>
.	.	kIx.	.
<g/>
Svůj	svůj	k3xOyFgInSc1	svůj
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
Karibik	Karibik	k1gInSc4	Karibik
Napoleon	napoleon	k1gInSc1	napoleon
zaměřil	zaměřit	k5eAaPmAgInS	zaměřit
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
musel	muset	k5eAaImAgMnS	muset
roku	rok	k1gInSc2	rok
1801	[number]	k4	1801
definitivně	definitivně	k6eAd1	definitivně
stáhnout	stáhnout	k5eAaPmF	stáhnout
svá	svůj	k3xOyFgNnPc4	svůj
vojska	vojsko	k1gNnPc4	vojsko
z	z	k7c2	z
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Centrem	centr	k1gInSc7	centr
jeho	jeho	k3xOp3gInSc2	jeho
zájmu	zájem	k1gInSc2	zájem
kromě	kromě	k7c2	kromě
Santo	Santo	k1gNnSc4	Santo
Dominga	Dominga	k1gFnSc1	Dominga
a	a	k8xC	a
Louisiany	Louisian	k1gInPc1	Louisian
byly	být	k5eAaImAgInP	být
především	především	k9	především
ostrovy	ostrov	k1gInPc1	ostrov
Guadeloupe	Guadeloupe	k1gFnSc2	Guadeloupe
<g/>
,	,	kIx,	,
Martinik	Martinik	k1gMnSc1	Martinik
a	a	k8xC	a
Tobago	Tobago	k1gMnSc1	Tobago
<g/>
.	.	kIx.	.
</s>
<s>
Napoleonově	Napoleonův	k2eAgFnSc3d1	Napoleonova
snaze	snaha	k1gFnSc3	snaha
o	o	k7c4	o
vybudování	vybudování	k1gNnSc4	vybudování
koloniálního	koloniální	k2eAgNnSc2d1	koloniální
impéria	impérium	k1gNnSc2	impérium
v	v	k7c6	v
Karibiku	Karibik	k1gInSc6	Karibik
a	a	k8xC	a
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Francie	Francie	k1gFnSc1	Francie
stala	stát	k5eAaPmAgFnS	stát
dominantní	dominantní	k2eAgFnSc7d1	dominantní
silou	síla	k1gFnSc7	síla
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Mexického	mexický	k2eAgInSc2d1	mexický
zálivu	záliv	k1gInSc2	záliv
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
napomáhaly	napomáhat	k5eAaBmAgInP	napomáhat
jeho	jeho	k3xOp3gInPc1	jeho
úspěchy	úspěch	k1gInPc1	úspěch
v	v	k7c6	v
bitvách	bitva	k1gFnPc6	bitva
proti	proti	k7c3	proti
britským	britský	k2eAgMnPc3d1	britský
spojencům	spojenec	k1gMnPc3	spojenec
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
nim	on	k3xPp3gFnPc3	on
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
roku	rok	k1gInSc3	rok
1802	[number]	k4	1802
podařilo	podařit	k5eAaPmAgNnS	podařit
Brity	Brit	k1gMnPc4	Brit
přimět	přimět	k5eAaPmF	přimět
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
podepsali	podepsat	k5eAaPmAgMnP	podepsat
amienský	amienský	k2eAgInSc4d1	amienský
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
základě	základ	k1gInSc6	základ
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
její	její	k3xOp3gMnPc1	její
spojenci	spojenec	k1gMnPc1	spojenec
získali	získat	k5eAaPmAgMnP	získat
zpět	zpět	k6eAd1	zpět
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc4	všechen
kolonie	kolonie	k1gFnPc4	kolonie
zabrané	zabraný	k2eAgFnPc4d1	zabraná
Británií	Británie	k1gFnSc7	Británie
v	v	k7c4	v
období	období	k1gNnSc4	období
francouzských	francouzský	k2eAgFnPc2d1	francouzská
revolučních	revoluční	k2eAgFnPc2d1	revoluční
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Ceylonu	Ceylon	k1gInSc2	Ceylon
(	(	kIx(	(
<g/>
odňatého	odňatý	k2eAgInSc2d1	odňatý
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
Trinidadu	Trinidad	k1gInSc2	Trinidad
(	(	kIx(	(
<g/>
odňatého	odňatý	k2eAgInSc2d1	odňatý
Francii	Francie	k1gFnSc6	Francie
<g/>
</s>
<s>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
Santo	Santo	k1gNnSc4	Santo
Domingu	Doming	k1gInSc2	Doming
se	se	k3xPyFc4	se
však	však	k9	však
situace	situace	k1gFnSc1	situace
pro	pro	k7c4	pro
Napoleona	Napoleon	k1gMnSc4	Napoleon
začala	začít	k5eAaPmAgFnS	začít
komplikovat	komplikovat	k5eAaBmF	komplikovat
<g/>
.	.	kIx.	.
</s>
<s>
Nejpozději	pozdě	k6eAd3	pozdě
během	během	k7c2	během
roku	rok	k1gInSc2	rok
1801	[number]	k4	1801
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
Toussaint	Toussaint	k1gInSc1	Toussaint
Louverture	Louvertur	k1gMnSc5	Louvertur
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
samostatnost	samostatnost	k1gFnSc4	samostatnost
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yQnSc4	což
Napoleon	napoleon	k1gInSc1	napoleon
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
zareagoval	zareagovat	k5eAaPmAgMnS	zareagovat
vysláním	vyslání	k1gNnSc7	vyslání
vojenské	vojenský	k2eAgFnSc2d1	vojenská
expedice	expedice	k1gFnSc2	expedice
o	o	k7c4	o
20	[number]	k4	20
000	[number]	k4	000
mužích	muž	k1gMnPc6	muž
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
získání	získání	k1gNnSc2	získání
konečné	konečný	k2eAgFnSc2d1	konečná
a	a	k8xC	a
úplné	úplný	k2eAgFnSc2d1	úplná
kontroly	kontrola	k1gFnSc2	kontrola
nad	nad	k7c7	nad
ostrovem	ostrov	k1gInSc7	ostrov
však	však	k9	však
měla	mít	k5eAaImAgFnS	mít
expedice	expedice	k1gFnSc1	expedice
ještě	ještě	k9	ještě
další	další	k2eAgInSc4d1	další
cíl	cíl	k1gInSc4	cíl
<g/>
:	:	kIx,	:
připravit	připravit	k5eAaPmF	připravit
invazi	invaze	k1gFnSc4	invaze
do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Konsolidovaná	konsolidovaný	k2eAgNnPc1d1	konsolidované
území	území	k1gNnPc1	území
v	v	k7c6	v
Louisianě	Louisiana	k1gFnSc6	Louisiana
pak	pak	k6eAd1	pak
zase	zase	k9	zase
měla	mít	k5eAaImAgFnS	mít
zásobovat	zásobovat	k5eAaImF	zásobovat
francouzské	francouzský	k2eAgInPc4d1	francouzský
ostrovy	ostrov	k1gInPc4	ostrov
v	v	k7c6	v
Karibiku	Karibik	k1gInSc6	Karibik
<g/>
.	.	kIx.	.
<g/>
Vyslaná	vyslaný	k2eAgFnSc1d1	vyslaná
expedice	expedice	k1gFnSc1	expedice
narazila	narazit	k5eAaPmAgFnS	narazit
na	na	k7c4	na
silný	silný	k2eAgInSc4d1	silný
odpor	odpor	k1gInSc4	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
zpočátku	zpočátku	k6eAd1	zpočátku
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
řadu	řada	k1gFnSc4	řada
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
,	,	kIx,	,
Louverture	Louvertur	k1gMnSc5	Louvertur
se	se	k3xPyFc4	se
nenechal	nechat	k5eNaPmAgMnS	nechat
přinutit	přinutit	k5eAaPmF	přinutit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
podřídil	podřídit	k5eAaPmAgMnS	podřídit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ani	ani	k9	ani
Napoleonovým	Napoleonův	k2eAgInSc7d1	Napoleonův
osobním	osobní	k2eAgInSc7d1	osobní
dopisem	dopis	k1gInSc7	dopis
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
mu	on	k3xPp3gMnSc3	on
poslal	poslat	k5eAaPmAgMnS	poslat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
jeho	jeho	k3xOp3gMnPc2	jeho
dvou	dva	k4xCgMnPc2	dva
synů	syn	k1gMnPc2	syn
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
tehdy	tehdy	k6eAd1	tehdy
studovali	studovat	k5eAaImAgMnP	studovat
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
se	se	k3xPyFc4	se
Louvertura	Louvertura	k1gFnSc1	Louvertura
podařilo	podařit	k5eAaPmAgNnS	podařit
zajmout	zajmout	k5eAaPmF	zajmout
a	a	k8xC	a
deportovat	deportovat	k5eAaBmF	deportovat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgInPc1d2	pozdější
Napoleonovy	Napoleonův	k2eAgInPc1d1	Napoleonův
kroky	krok	k1gInPc1	krok
situaci	situace	k1gFnSc3	situace
dále	daleko	k6eAd2	daleko
ztížily	ztížit	k5eAaPmAgFnP	ztížit
a	a	k8xC	a
přiměly	přimět	k5eAaPmAgFnP	přimět
povstalce	povstalec	k1gMnPc4	povstalec
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
odporu	odpor	k1gInSc3	odpor
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1802	[number]	k4	1802
Napoleon	napoleon	k1gInSc1	napoleon
schválil	schválit	k5eAaPmAgInS	schválit
zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
kterým	který	k3yQgNnSc7	který
definitivně	definitivně	k6eAd1	definitivně
zastavil	zastavit	k5eAaPmAgInS	zastavit
rušení	rušení	k1gNnSc4	rušení
otroctví	otroctví	k1gNnPc2	otroctví
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dosud	dosud	k6eAd1	dosud
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
nebylo	být	k5eNaImAgNnS	být
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
například	například	k6eAd1	například
Martinik	Martinik	k1gInSc1	Martinik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
rasistický	rasistický	k2eAgInSc4d1	rasistický
postoj	postoj	k1gInSc4	postoj
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
znovu	znovu	k6eAd1	znovu
projevil	projevit	k5eAaPmAgInS	projevit
v	v	k7c6	v
dekretu	dekret	k1gInSc6	dekret
z	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
zakázal	zakázat	k5eAaPmAgInS	zakázat
vstup	vstup	k1gInSc1	vstup
na	na	k7c6	na
území	území	k1gNnSc6	území
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
Francie	Francie	k1gFnSc2	Francie
všem	všecek	k3xTgMnPc3	všecek
černochům	černoch	k1gMnPc3	černoch
i	i	k8xC	i
lidem	člověk	k1gMnPc3	člověk
smíšeného	smíšený	k2eAgInSc2d1	smíšený
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
1802	[number]	k4	1802
vojenské	vojenský	k2eAgFnSc2d1	vojenská
expedice	expedice	k1gFnSc2	expedice
z	z	k7c2	z
Napoleonova	Napoleonův	k2eAgInSc2d1	Napoleonův
příkazu	příkaz	k1gInSc2	příkaz
obnovily	obnovit	k5eAaPmAgInP	obnovit
otroctví	otroctví	k1gNnPc2	otroctví
ve	v	k7c6	v
Francouzské	francouzský	k2eAgFnSc6d1	francouzská
Guayaně	Guayana	k1gFnSc6	Guayana
a	a	k8xC	a
po	po	k7c6	po
krátkých	krátký	k2eAgInPc6d1	krátký
krvavých	krvavý	k2eAgInPc6d1	krvavý
bojích	boj	k1gInPc6	boj
také	také	k9	také
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Guadeloupe	Guadeloupe	k1gFnSc2	Guadeloupe
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
vyloučit	vyloučit	k5eAaPmF	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Napoleon	napoleon	k1gInSc1	napoleon
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
byl	být	k5eAaImAgInS	být
pokusil	pokusit	k5eAaPmAgInS	pokusit
obnovit	obnovit	k5eAaPmF	obnovit
otroctví	otroctví	k1gNnSc4	otroctví
i	i	k9	i
na	na	k7c4	na
Santo	Santo	k1gNnSc4	Santo
Domingu	Doming	k1gInSc2	Doming
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
silách	síla	k1gFnPc6	síla
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
situaci	situace	k1gFnSc6	situace
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
těžko	těžko	k6eAd1	těžko
proveditelné	proveditelný	k2eAgNnSc1d1	proveditelné
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
neutuchajícího	utuchající	k2eNgInSc2d1	neutuchající
odporu	odpor	k1gInSc2	odpor
povstalců	povstalec	k1gMnPc2	povstalec
vedly	vést	k5eAaImAgInP	vést
jeho	jeho	k3xOp3gInPc1	jeho
činy	čin	k1gInPc1	čin
také	také	k9	také
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c4	na
povstaleckou	povstalecký	k2eAgFnSc4d1	povstalecká
stranu	strana	k1gFnSc4	strana
začali	začít	k5eAaPmAgMnP	začít
přidávat	přidávat	k5eAaImF	přidávat
i	i	k9	i
francouzští	francouzský	k2eAgMnPc1d1	francouzský
velitelé	velitel	k1gMnPc1	velitel
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc1	jejich
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
černé	černé	k1gNnSc4	černé
pleti	pleť	k1gFnSc2	pleť
či	či	k8xC	či
smíšeného	smíšený	k2eAgInSc2d1	smíšený
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
než	než	k8xS	než
s	s	k7c7	s
povstalci	povstalec	k1gMnPc7	povstalec
se	se	k3xPyFc4	se
francouzské	francouzský	k2eAgInPc1d1	francouzský
oddíly	oddíl	k1gInPc1	oddíl
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
ovšem	ovšem	k9	ovšem
potýkaly	potýkat	k5eAaImAgFnP	potýkat
se	s	k7c7	s
žlutou	žlutý	k2eAgFnSc7d1	žlutá
zimnicí	zimnice	k1gFnSc7	zimnice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
vojska	vojsko	k1gNnSc2	vojsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
jeho	on	k3xPp3gMnSc2	on
velitele	velitel	k1gMnSc2	velitel
<g/>
,	,	kIx,	,
Napoleonova	Napoleonův	k2eAgMnSc2d1	Napoleonův
švagra	švagr	k1gMnSc2	švagr
generála	generál	k1gMnSc2	generál
Charlese	Charles	k1gMnSc2	Charles
Leclerca	Leclercus	k1gMnSc2	Leclercus
<g/>
.	.	kIx.	.
<g/>
Neúspěch	neúspěch	k1gInSc1	neúspěch
na	na	k7c4	na
Santo	Santo	k1gNnSc4	Santo
Domingu	Doming	k1gInSc2	Doming
znemožnil	znemožnit	k5eAaPmAgMnS	znemožnit
Napoleonovu	Napoleonův	k2eAgFnSc4d1	Napoleonova
snahu	snaha	k1gFnSc4	snaha
o	o	k7c4	o
konsolidaci	konsolidace	k1gFnSc4	konsolidace
francouzského	francouzský	k2eAgNnSc2d1	francouzské
území	území	k1gNnSc2	území
v	v	k7c6	v
Louisianě	Louisian	k1gInSc6	Louisian
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1803	[number]	k4	1803
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
války	válka	k1gFnSc2	válka
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
situaci	situace	k1gFnSc3	situace
zámořskými	zámořský	k2eAgInPc7d1	zámořský
problémy	problém	k1gInPc7	problém
dále	daleko	k6eAd2	daleko
nekomplikovat	komplikovat	k5eNaBmF	komplikovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1803	[number]	k4	1803
proto	proto	k8xC	proto
prodal	prodat	k5eAaPmAgInS	prodat
Louisianu	Louisiana	k1gFnSc4	Louisiana
Spojeným	spojený	k2eAgInPc3d1	spojený
státům	stát	k1gInPc3	stát
americkým	americký	k2eAgInPc3d1	americký
za	za	k7c4	za
15	[number]	k4	15
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
expedice	expedice	k1gFnSc1	expedice
na	na	k7c4	na
Santo	Santo	k1gNnSc4	Santo
Domingu	Doming	k1gInSc2	Doming
přestala	přestat	k5eAaPmAgFnS	přestat
dostávat	dostávat	k5eAaImF	dostávat
posily	posila	k1gFnPc4	posila
a	a	k8xC	a
opuštěné	opuštěný	k2eAgNnSc4d1	opuštěné
francouzské	francouzský	k2eAgNnSc4d1	francouzské
expediční	expediční	k2eAgNnSc4d1	expediční
vojsko	vojsko	k1gNnSc4	vojsko
se	se	k3xPyFc4	se
po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Vertiè	Vertiè	k1gFnSc2	Vertiè
<g/>
,	,	kIx,	,
utrpěné	utrpěný	k2eAgFnSc2d1	utrpěná
od	od	k7c2	od
černošské	černošský	k2eAgFnSc2d1	černošská
armády	armáda	k1gFnSc2	armáda
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Jean-Jacquese	Jean-Jacquese	k1gFnSc2	Jean-Jacquese
Dessalinese	Dessalinese	k1gFnSc2	Dessalinese
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1803	[number]	k4	1803
vzdalo	vzdát	k5eAaPmAgNnS	vzdát
Angličanům	Angličan	k1gMnPc3	Angličan
<g/>
.	.	kIx.	.
<g/>
Napoleonův	Napoleonův	k2eAgInSc4d1	Napoleonův
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
završení	završení	k1gNnSc4	završení
dlouhodobých	dlouhodobý	k2eAgFnPc2d1	dlouhodobá
francouzských	francouzský	k2eAgFnPc2d1	francouzská
snah	snaha	k1gFnPc2	snaha
o	o	k7c4	o
zámořské	zámořský	k2eAgNnSc4d1	zámořské
impérium	impérium	k1gNnSc4	impérium
stál	stát	k5eAaImAgMnS	stát
život	život	k1gInSc4	život
asi	asi	k9	asi
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
francouzských	francouzský	k2eAgMnPc2d1	francouzský
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
námořníků	námořník	k1gMnPc2	námořník
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
podobný	podobný	k2eAgInSc1d1	podobný
počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
byl	být	k5eAaImAgInS	být
také	také	k9	také
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
vzbouřenců	vzbouřenec	k1gMnPc2	vzbouřenec
a	a	k8xC	a
civilistů	civilista	k1gMnPc2	civilista
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
kolonii	kolonie	k1gFnSc6	kolonie
Saint-Domingue	Saint-Domingu	k1gInSc2	Saint-Domingu
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pamětech	paměť	k1gFnPc6	paměť
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
Napoleon	Napoleon	k1gMnSc1	Napoleon
sepsal	sepsat	k5eAaPmAgMnS	sepsat
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
pádu	pád	k1gInSc6	pád
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Svatá	svatý	k2eAgFnSc1d1	svatá
Helena	Helena	k1gFnSc1	Helena
<g/>
,	,	kIx,	,
označil	označit	k5eAaPmAgMnS	označit
karibské	karibský	k2eAgNnSc4d1	Karibské
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
největších	veliký	k2eAgFnPc2d3	veliký
chyb	chyba	k1gFnPc2	chyba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Závěr	závěr	k1gInSc1	závěr
kariéry	kariéra	k1gFnSc2	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Panovníkem	panovník	k1gMnSc7	panovník
Elby	Elba	k1gMnSc2	Elba
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
nevelký	velký	k2eNgInSc4d1	nevelký
středomořský	středomořský	k2eAgInSc4d1	středomořský
ostrov	ostrov	k1gInSc4	ostrov
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
8000	[number]	k4	8000
hektarů	hektar	k1gInPc2	hektar
dorazil	dorazit	k5eAaPmAgInS	dorazit
Napoleon	napoleon	k1gInSc1	napoleon
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1814	[number]	k4	1814
<g/>
,	,	kIx,	,
v	v	k7c4	v
týž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Paříž	Paříž	k1gFnSc1	Paříž
přivítala	přivítat	k5eAaPmAgFnS	přivítat
krále	král	k1gMnSc4	král
Ludvíka	Ludvík	k1gMnSc4	Ludvík
XVIII	XVIII	kA	XVIII
<g/>
.	.	kIx.	.
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Bourbonů	bourbon	k1gInPc2	bourbon
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
smlouvy	smlouva	k1gFnSc2	smlouva
z	z	k7c2	z
Fontainebleau	Fontainebleaus	k1gInSc2	Fontainebleaus
ze	z	k7c2	z
dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1814	[number]	k4	1814
byl	být	k5eAaImAgInS	být
ostrov	ostrov	k1gInSc1	ostrov
prohlášen	prohlásit	k5eAaPmNgInS	prohlásit
samostatným	samostatný	k2eAgInSc7d1	samostatný
státem	stát	k1gInSc7	stát
jako	jako	k8xS	jako
Knížectví	knížectví	k1gNnSc1	knížectví
Elba	Elba	k1gMnSc1	Elba
a	a	k8xC	a
Napoleon	Napoleon	k1gMnSc1	Napoleon
byl	být	k5eAaImAgMnS	být
ustanoven	ustanovit	k5eAaPmNgMnS	ustanovit
jeho	jeho	k3xOp3gMnSc7	jeho
svrchovaným	svrchovaný	k2eAgMnSc7d1	svrchovaný
panovníkem	panovník	k1gMnSc7	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
byl	být	k5eAaImAgMnS	být
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
přiřčen	přiřčen	k2eAgInSc4d1	přiřčen
roční	roční	k2eAgInSc4d1	roční
důchod	důchod	k1gInSc4	důchod
dvou	dva	k4xCgInPc2	dva
milionů	milion	k4xCgInPc2	milion
franků	frank	k1gInPc2	frank
z	z	k7c2	z
francouzské	francouzský	k2eAgFnSc2d1	francouzská
státní	státní	k2eAgFnSc2d1	státní
pokladny	pokladna	k1gFnSc2	pokladna
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
ovšem	ovšem	k9	ovšem
jeden	jeden	k4xCgMnSc1	jeden
milion	milion	k4xCgInSc1	milion
franků	frank	k1gInPc2	frank
náležel	náležet	k5eAaImAgInS	náležet
císařovně	císařovna	k1gFnSc6	císařovna
Marii	Maria	k1gFnSc3	Maria
Luise	Luisa	k1gFnSc3	Luisa
<g/>
.	.	kIx.	.
</s>
<s>
Napoleonovi	Napoleonův	k2eAgMnPc1d1	Napoleonův
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
stát	stát	k1gInSc1	stát
přenechán	přenechán	k2eAgInSc1d1	přenechán
k	k	k7c3	k
doživotnímu	doživotní	k2eAgNnSc3d1	doživotní
užívání	užívání	k1gNnSc3	užívání
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
mu	on	k3xPp3gNnSc3	on
byl	být	k5eAaImAgInS	být
ponechán	ponechat	k5eAaPmNgInS	ponechat
jeho	on	k3xPp3gInSc4	on
císařský	císařský	k2eAgInSc4d1	císařský
titul	titul	k1gInSc4	titul
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
příjezdu	příjezd	k1gInSc6	příjezd
na	na	k7c6	na
Elbu	Elbus	k1gInSc6	Elbus
neměl	mít	k5eNaImAgInS	mít
Napoleon	napoleon	k1gInSc1	napoleon
žádné	žádný	k3yNgInPc4	žádný
plány	plán	k1gInPc4	plán
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
politický	politický	k2eAgInSc4d1	politický
život	život	k1gInSc4	život
pokládal	pokládat	k5eAaImAgMnS	pokládat
za	za	k7c4	za
uzavřenou	uzavřený	k2eAgFnSc4d1	uzavřená
kapitolu	kapitola	k1gFnSc4	kapitola
<g/>
.	.	kIx.	.
<g/>
Aby	aby	kYmCp3nP	aby
své	svůj	k3xOyFgNnSc4	svůj
malé	malý	k2eAgNnSc4d1	malé
panství	panství	k1gNnSc4	panství
uživil	uživit	k5eAaPmAgMnS	uživit
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaPmF	věnovat
hospodaření	hospodaření	k1gNnSc4	hospodaření
<g/>
,	,	kIx,	,
rozvoji	rozvoj	k1gInSc6	rozvoj
řemesel	řemeslo	k1gNnPc2	řemeslo
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
chovu	chov	k1gInSc2	chov
ovcí	ovce	k1gFnPc2	ovce
a	a	k8xC	a
těžbě	těžba	k1gFnSc3	těžba
železné	železný	k2eAgFnSc2d1	železná
</s>
<s>
rudy	ruda	k1gFnPc1	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgMnS	vydat
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
knížectví	knížectví	k1gNnSc4	knížectví
novou	nový	k2eAgFnSc4d1	nová
ústavu	ústava	k1gFnSc4	ústava
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgInS	založit
policejní	policejní	k2eAgInSc1d1	policejní
sbor	sbor	k1gInSc1	sbor
<g/>
,	,	kIx,	,
nařídil	nařídit	k5eAaPmAgInS	nařídit
očkování	očkování	k1gNnSc4	očkování
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
povinnou	povinný	k2eAgFnSc4d1	povinná
školní	školní	k2eAgFnSc4d1	školní
docházku	docházka	k1gFnSc4	docházka
<g/>
,	,	kIx,	,
postavil	postavit	k5eAaPmAgMnS	postavit
dvě	dva	k4xCgFnPc4	dva
nemocnice	nemocnice	k1gFnPc4	nemocnice
a	a	k8xC	a
zakázal	zakázat	k5eAaPmAgInS	zakázat
házet	házet	k5eAaImF	házet
odpadky	odpadek	k1gInPc4	odpadek
z	z	k7c2	z
oken	okno	k1gNnPc2	okno
<g/>
.	.	kIx.	.
</s>
<s>
Nezanedbával	zanedbávat	k5eNaImAgMnS	zanedbávat
ani	ani	k8xC	ani
svou	svůj	k3xOyFgFnSc4	svůj
miniaturní	miniaturní	k2eAgFnSc4d1	miniaturní
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
místo	místo	k7c2	místo
spojenci	spojenec	k1gMnPc7	spojenec
povolených	povolený	k2eAgFnPc2d1	povolená
tří	tři	k4xCgFnPc2	tři
stovek	stovka	k1gFnPc2	stovka
vojáků	voják	k1gMnPc2	voják
brzy	brzy	k6eAd1	brzy
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
počtu	počet	k1gInSc2	počet
2000	[number]	k4	2000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
sídle	sídlo	k1gNnSc6	sídlo
pořádal	pořádat	k5eAaImAgMnS	pořádat
audience	audience	k1gFnPc4	audience
i	i	k8xC	i
plesy	ples	k1gInPc4	ples
pro	pro	k7c4	pro
místní	místní	k2eAgFnSc4d1	místní
vyšší	vysoký	k2eAgFnSc4d2	vyšší
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
organizoval	organizovat	k5eAaBmAgMnS	organizovat
dvorský	dvorský	k2eAgInSc4d1	dvorský
život	život	k1gInSc4	život
a	a	k8xC	a
ukazoval	ukazovat	k5eAaImAgMnS	ukazovat
se	se	k3xPyFc4	se
občasným	občasný	k2eAgMnPc3d1	občasný
turistům	turist	k1gMnPc3	turist
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgNnSc4	který
představoval	představovat	k5eAaImAgInS	představovat
velkou	velký	k2eAgFnSc4d1	velká
atrakci	atrakce	k1gFnSc4	atrakce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
svého	svůj	k3xOyFgInSc2	svůj
pobytu	pobyt	k1gInSc2	pobyt
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
se	se	k3xPyFc4	se
těšil	těšit	k5eAaImAgMnS	těšit
z	z	k7c2	z
přítomnosti	přítomnost	k1gFnSc2	přítomnost
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
Leticie	Leticie	k1gFnSc2	Leticie
<g/>
,	,	kIx,	,
sestry	sestra	k1gFnSc2	sestra
Paulíny	Paulína	k1gFnSc2	Paulína
a	a	k8xC	a
hraběnky	hraběnka	k1gFnSc2	hraběnka
Walewské	Walewská	k1gFnSc2	Walewská
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sem	sem	k6eAd1	sem
přibyla	přibýt	k5eAaPmAgFnS	přibýt
i	i	k9	i
s	s	k7c7	s
jejich	jejich	k3xOp3gMnSc7	jejich
synem	syn	k1gMnSc7	syn
Alexandrem	Alexandr	k1gMnSc7	Alexandr
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
podzimu	podzim	k1gInSc2	podzim
1814	[number]	k4	1814
však	však	k9	však
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
začaly	začít	k5eAaPmAgFnP	začít
pronikat	pronikat	k5eAaImF	pronikat
rozporuplné	rozporuplný	k2eAgFnPc1d1	rozporuplná
zprávy	zpráva	k1gFnPc1	zpráva
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těm	ten	k3xDgFnPc3	ten
negativním	negativní	k2eAgFnPc3d1	negativní
patřila	patřit	k5eAaImAgFnS	patřit
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
někdejší	někdejší	k2eAgMnSc1d1	někdejší
Napoleonův	Napoleonův	k2eAgMnSc1d1	Napoleonův
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Talleyrand	Talleyranda	k1gFnPc2	Talleyranda
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
internaci	internace	k1gFnSc6	internace
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
<g/>
,	,	kIx,	,
vzdálenější	vzdálený	k2eAgInSc4d2	vzdálenější
ostrov	ostrov	k1gInSc4	ostrov
(	(	kIx(	(
<g/>
již	již	k6eAd1	již
tenkrát	tenkrát	k6eAd1	tenkrát
padlo	padnout	k5eAaPmAgNnS	padnout
jméno	jméno	k1gNnSc1	jméno
Svatá	svatá	k1gFnSc1	svatá
Helena	Helena	k1gFnSc1	Helena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
prosakovaly	prosakovat	k5eAaImAgFnP	prosakovat
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
pokusech	pokus	k1gInPc6	pokus
jej	on	k3xPp3gMnSc4	on
otrávit	otrávit	k5eAaPmF	otrávit
a	a	k8xC	a
Španělé	Španěl	k1gMnPc1	Španěl
navrhovali	navrhovat	k5eAaImAgMnP	navrhovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ostrov	ostrov	k1gInSc1	ostrov
obsadí	obsadit	k5eAaPmIp3nS	obsadit
a	a	k8xC	a
francouzského	francouzský	k2eAgMnSc2d1	francouzský
excísaře	excísař	k1gMnSc2	excísař
zajmou	zajmout	k5eAaPmIp3nP	zajmout
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
Ludvík	Ludvík	k1gMnSc1	Ludvík
XVIII	XVIII	kA	XVIII
<g/>
.	.	kIx.	.
ještě	ještě	k9	ještě
stále	stále	k6eAd1	stále
nezačal	začít	k5eNaPmAgInS	začít
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
vyplácet	vyplácet	k5eAaImF	vyplácet
přislíbenou	přislíbený	k2eAgFnSc4d1	přislíbená
dvoumilionovou	dvoumilionový	k2eAgFnSc4d1	dvoumilionová
apanáž	apanáž	k1gFnSc4	apanáž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
se	se	k3xPyFc4	se
dozvídal	dozvídat	k5eAaImAgMnS	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
přibývá	přibývat	k5eAaImIp3nS	přibývat
nespokojených	spokojený	k2eNgMnPc2d1	nespokojený
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
kterým	který	k3yQgMnSc7	který
se	se	k3xPyFc4	se
nelíbil	líbit	k5eNaImAgMnS	líbit
návrat	návrat	k1gInSc4	návrat
forem	forma	k1gFnPc2	forma
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
institucí	instituce	k1gFnPc2	instituce
starého	starý	k2eAgInSc2d1	starý
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Předrevoluční	předrevoluční	k2eAgFnSc1d1	předrevoluční
šlechta	šlechta	k1gFnSc1	šlechta
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
vracela	vracet	k5eAaImAgFnS	vracet
z	z	k7c2	z
emigrace	emigrace	k1gFnSc2	emigrace
<g/>
,	,	kIx,	,
zabrala	zabrat	k5eAaPmAgFnS	zabrat
výnosné	výnosný	k2eAgInPc4d1	výnosný
úřady	úřad	k1gInPc4	úřad
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
<g/>
,	,	kIx,	,
císařská	císařský	k2eAgFnSc1d1	císařská
šlechta	šlechta	k1gFnSc1	šlechta
byla	být	k5eAaImAgFnS	být
přehlížena	přehlížet	k5eAaImNgFnS	přehlížet
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
bouřit	bouřit	k5eAaImF	bouřit
<g/>
.	.	kIx.	.
</s>
<s>
Vojáci	voják	k1gMnPc1	voják
stále	stále	k6eAd1	stále
častěji	často	k6eAd2	často
vzpomínali	vzpomínat	k5eAaImAgMnP	vzpomínat
na	na	k7c4	na
Napoleona	Napoleon	k1gMnSc4	Napoleon
a	a	k8xC	a
v	v	k7c6	v
Bourbonech	bourbon	k1gInPc6	bourbon
spatřovali	spatřovat	k5eAaImAgMnP	spatřovat
jen	jen	k9	jen
nutné	nutný	k2eAgNnSc4d1	nutné
zlo	zlo	k1gNnSc4	zlo
vnucené	vnucený	k2eAgFnSc2d1	vnucená
zvenčí	zvenčí	k6eAd1	zvenčí
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
důvodem	důvod	k1gInSc7	důvod
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
nespokojenosti	nespokojenost	k1gFnSc3	nespokojenost
bylo	být	k5eAaImAgNnS	být
masové	masový	k2eAgNnSc1d1	masové
propouštění	propouštění	k1gNnSc1	propouštění
do	do	k7c2	do
penze	penze	k1gFnSc2	penze
nebo	nebo	k8xC	nebo
na	na	k7c4	na
poloviční	poloviční	k2eAgNnSc4d1	poloviční
služné	služné	k1gNnSc4	služné
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
i	i	k9	i
další	další	k2eAgFnPc1d1	další
skutečnosti	skutečnost	k1gFnPc1	skutečnost
z	z	k7c2	z
francouzského	francouzský	k2eAgNnSc2d1	francouzské
dění	dění	k1gNnSc2	dění
Napoleon	napoleon	k1gInSc4	napoleon
pozorně	pozorně	k6eAd1	pozorně
sledoval	sledovat	k5eAaImAgMnS	sledovat
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
si	se	k3xPyFc3	se
proto	proto	k8xC	proto
dobře	dobře	k6eAd1	dobře
vědom	vědom	k2eAgMnSc1d1	vědom
nálad	nálada	k1gFnPc2	nálada
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nímž	jenž	k3xRgMnSc7	jenž
sílil	sílit	k5eAaImAgMnS	sílit
odpor	odpor	k1gInSc4	odpor
ke	k	k7c3	k
královské	královský	k2eAgFnSc3d1	královská
dynastii	dynastie	k1gFnSc3	dynastie
a	a	k8xC	a
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
obnově	obnova	k1gFnSc6	obnova
císařství	císařství	k1gNnSc2	císařství
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
proto	proto	k8xC	proto
uvažovat	uvažovat	k5eAaImF	uvažovat
o	o	k7c6	o
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Definitivně	definitivně	k6eAd1	definitivně
se	se	k3xPyFc4	se
však	však	k9	však
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
až	až	k6eAd1	až
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
1815	[number]	k4	1815
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ministr	ministr	k1gMnSc1	ministr
policie	policie	k1gFnSc2	policie
Fouché	Fouchý	k2eAgFnSc2d1	Fouchý
se	se	k3xPyFc4	se
chystá	chystat	k5eAaImIp3nS	chystat
sjednotit	sjednotit	k5eAaPmF	sjednotit
opozici	opozice	k1gFnSc4	opozice
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
připravuje	připravovat	k5eAaImIp3nS	připravovat
politický	politický	k2eAgInSc4d1	politický
převrat	převrat	k1gInSc4	převrat
<g/>
.	.	kIx.	.
</s>
<s>
Příznivá	příznivý	k2eAgFnSc1d1	příznivá
situace	situace	k1gFnSc1	situace
nastala	nastat	k5eAaPmAgFnS	nastat
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ostrov	ostrov	k1gInSc1	ostrov
dočasně	dočasně	k6eAd1	dočasně
opustil	opustit	k5eAaPmAgInS	opustit
Napoleonův	Napoleonův	k2eAgInSc4d1	Napoleonův
dohled	dohled	k1gInSc4	dohled
plukovník	plukovník	k1gMnSc1	plukovník
Champbell	Champbell	k1gMnSc1	Champbell
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1815	[number]	k4	1815
císař	císař	k1gMnSc1	císař
nalodil	nalodit	k5eAaPmAgMnS	nalodit
svých	svůj	k3xOyFgMnPc2	svůj
1100	[number]	k4	1100
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
80	[number]	k4	80
koní	kůň	k1gMnPc2	kůň
a	a	k8xC	a
4	[number]	k4	4
děla	dělo	k1gNnSc2	dělo
na	na	k7c4	na
zrekvírované	zrekvírovaný	k2eAgFnPc4d1	zrekvírovaný
lodě	loď	k1gFnPc4	loď
a	a	k8xC	a
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
k	k	k7c3	k
francouzskému	francouzský	k2eAgNnSc3d1	francouzské
pobřeží	pobřeží	k1gNnSc3	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Zabavená	zabavený	k2eAgNnPc1d1	zabavené
plavidla	plavidlo	k1gNnPc1	plavidlo
se	se	k3xPyFc4	se
k	k	k7c3	k
francouzskému	francouzský	k2eAgNnSc3d1	francouzské
pobřeží	pobřeží	k1gNnSc3	pobřeží
vydala	vydat	k5eAaPmAgFnS	vydat
samostatně	samostatně	k6eAd1	samostatně
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
druhý	druhý	k4xOgInSc4	druhý
či	či	k8xC	či
třetí	třetí	k4xOgInSc4	třetí
den	den	k1gInSc4	den
po	po	k7c6	po
vyplutí	vyplutí	k1gNnSc6	vyplutí
se	se	k3xPyFc4	se
však	však	k9	však
celá	celý	k2eAgFnSc1d1	celá
výprava	výprava	k1gFnSc1	výprava
octla	octnout	k5eAaPmAgFnS	octnout
v	v	k7c6	v
ohrožení	ohrožení	k1gNnSc6	ohrožení
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
k	k	k7c3	k
Napoleonově	Napoleonův	k2eAgFnSc3d1	Napoleonova
brize	briga	k1gFnSc3	briga
Inconstant	Inconstant	k1gMnSc1	Inconstant
se	se	k3xPyFc4	se
přiblížila	přiblížit	k5eAaPmAgFnS	přiblížit
fregata	fregata	k1gFnSc1	fregata
Zephyr	Zephyra	k1gFnPc2	Zephyra
<g/>
.	.	kIx.	.
</s>
<s>
Granátníci	granátník	k1gMnPc1	granátník
dostali	dostat	k5eAaPmAgMnP	dostat
rozkaz	rozkaz	k1gInSc4	rozkaz
sundat	sundat	k5eAaPmF	sundat
vysoké	vysoký	k2eAgFnPc4d1	vysoká
medvědice	medvědice	k1gFnPc4	medvědice
<g/>
,	,	kIx,	,
lehnout	lehnout	k5eAaPmF	lehnout
si	se	k3xPyFc3	se
na	na	k7c4	na
palubu	paluba	k1gFnSc4	paluba
a	a	k8xC	a
připravit	připravit	k5eAaPmF	připravit
se	se	k3xPyFc4	se
na	na	k7c4	na
zahákování	zahákování	k1gNnSc4	zahákování
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
kapitán	kapitán	k1gMnSc1	kapitán
Zephyru	Zephyr	k1gInSc2	Zephyr
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
paluby	paluba	k1gFnSc2	paluba
zvědavě	zvědavě	k6eAd1	zvědavě
vyptával	vyptávat	k5eAaImAgMnS	vyptávat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgMnSc1d1	velký
muž	muž	k1gMnSc1	muž
na	na	k7c6	na
Elbě	Elba	k1gFnSc6	Elba
<g/>
,	,	kIx,	,
postačila	postačit	k5eAaPmAgFnS	postačit
mu	on	k3xPp3gMnSc3	on
odpověď	odpověď	k1gFnSc4	odpověď
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zašeptal	zašeptat	k5eAaPmAgMnS	zašeptat
Napoleon	Napoleon	k1gMnSc1	Napoleon
do	do	k7c2	do
ucha	ucho	k1gNnSc2	ucho
kapitánovi	kapitán	k1gMnSc3	kapitán
Tailladovi	Taillada	k1gMnSc3	Taillada
<g/>
,	,	kIx,	,
že	že	k8xS	že
báječně	báječně	k6eAd1	báječně
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
víc	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
nezajímal	zajímat	k5eNaImAgMnS	zajímat
a	a	k8xC	a
Inconstant	Inconstant	k1gMnSc1	Inconstant
opět	opět	k6eAd1	opět
propustil	propustit	k5eAaPmAgMnS	propustit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sto	sto	k4xCgNnSc4	sto
dnů	den	k1gInPc2	den
===	===	k?	===
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1815	[number]	k4	1815
dorazilo	dorazit	k5eAaPmAgNnS	dorazit
sedm	sedm	k4xCc1	sedm
lodí	loď	k1gFnPc2	loď
Napoleonovy	Napoleonův	k2eAgFnSc2d1	Napoleonova
malé	malý	k2eAgFnSc2d1	malá
flotily	flotila	k1gFnSc2	flotila
do	do	k7c2	do
zálivu	záliv	k1gInSc2	záliv
Juan	Juan	k1gMnSc1	Juan
nedaleko	nedaleko	k7c2	nedaleko
Antibes	Antibesa	k1gFnPc2	Antibesa
<g/>
.	.	kIx.	.
</s>
<s>
Vojáci	voják	k1gMnPc1	voják
se	se	k3xPyFc4	se
vylodili	vylodit	k5eAaPmAgMnP	vylodit
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
a	a	k8xC	a
kolem	kolem	k7c2	kolem
páté	pátý	k4xOgFnSc2	pátý
hodiny	hodina	k1gFnSc2	hodina
odpolední	odpolední	k1gNnSc2	odpolední
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Paříž	Paříž	k1gFnSc4	Paříž
skrze	skrze	k?	skrze
alpské	alpský	k2eAgFnSc2d1	alpská
stezky	stezka	k1gFnSc2	stezka
Horní	horní	k2eAgFnSc2d1	horní
Provence	Provence	k1gFnSc2	Provence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přímořském	přímořský	k2eAgInSc6d1	přímořský
kraji	kraj	k1gInSc6	kraj
Napoleonova	Napoleonův	k2eAgFnSc1d1	Napoleonova
přítomnost	přítomnost	k1gFnSc1	přítomnost
mnoho	mnoho	k6eAd1	mnoho
radosti	radost	k1gFnSc2	radost
u	u	k7c2	u
obyvatel	obyvatel	k1gMnPc2	obyvatel
nevyvolávala	vyvolávat	k5eNaImAgFnS	vyvolávat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
čím	čí	k3xOyQgNnSc7	čí
více	hodně	k6eAd2	hodně
postupoval	postupovat	k5eAaImAgInS	postupovat
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
větším	veliký	k2eAgNnSc7d2	veliký
nadšením	nadšení	k1gNnSc7	nadšení
se	se	k3xPyFc4	se
setkával	setkávat	k5eAaImAgMnS	setkávat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
vojsku	vojsko	k1gNnSc3	vojsko
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
přidalo	přidat	k5eAaPmAgNnS	přidat
i	i	k9	i
1500	[number]	k4	1500
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
dělníků	dělník	k1gMnPc2	dělník
<g/>
,	,	kIx,	,
s	s	k7c7	s
vlastními	vlastní	k2eAgFnPc7d1	vlastní
puškami	puška	k1gFnPc7	puška
či	či	k8xC	či
různými	různý	k2eAgFnPc7d1	různá
chladnými	chladný	k2eAgFnPc7d1	chladná
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
pozvolna	pozvolna	k6eAd1	pozvolna
zvětšující	zvětšující	k2eAgInSc1d1	zvětšující
konvoj	konvoj	k1gInSc1	konvoj
dostal	dostat	k5eAaPmAgInS	dostat
ke	k	k7c3	k
Grenoblu	Grenoble	k1gInSc3	Grenoble
<g/>
,	,	kIx,	,
místní	místní	k2eAgMnSc1d1	místní
velitel	velitel	k1gMnSc1	velitel
posádky	posádka	k1gFnSc2	posádka
generál	generál	k1gMnSc1	generál
Marchond	Marchond	k1gMnSc1	Marchond
proti	proti	k7c3	proti
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
jako	jako	k8xC	jako
předvoj	předvoj	k1gInSc4	předvoj
vyslal	vyslat	k5eAaPmAgMnS	vyslat
prapor	prapor	k1gInSc4	prapor
5	[number]	k4	5
<g/>
.	.	kIx.	.
pěšího	pěší	k2eAgInSc2d1	pěší
pluku	pluk	k1gInSc2	pluk
s	s	k7c7	s
rotou	rota	k1gFnSc7	rota
ženistů	ženista	k1gMnPc2	ženista
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
vyhodit	vyhodit	k5eAaPmF	vyhodit
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
mosty	most	k1gInPc4	most
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
obě	dva	k4xCgFnPc1	dva
kolony	kolona	k1gFnPc1	kolona
narazily	narazit	k5eAaPmAgFnP	narazit
<g/>
,	,	kIx,	,
vojáci	voják	k1gMnPc1	voják
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
rozvinovat	rozvinovat	k5eAaImF	rozvinovat
do	do	k7c2	do
bojové	bojový	k2eAgFnSc2d1	bojová
sestavy	sestava	k1gFnSc2	sestava
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	napoleon	k1gInSc1	napoleon
postupoval	postupovat	k5eAaImAgInS	postupovat
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
svých	svůj	k3xOyFgMnPc2	svůj
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
protivník	protivník	k1gMnSc1	protivník
kapitán	kapitán	k1gMnSc1	kapitán
Randon	Randon	k1gMnSc1	Randon
z	z	k7c2	z
královské	královský	k2eAgFnSc2d1	královská
armády	armáda	k1gFnSc2	armáda
zavelel	zavelet	k5eAaPmAgMnS	zavelet
k	k	k7c3	k
palbě	palba	k1gFnSc3	palba
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgMnSc1	žádný
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
podřízených	podřízený	k1gMnPc2	podřízený
však	však	k8xC	však
rozkazu	rozkaz	k1gInSc2	rozkaz
neuposlechl	uposlechnout	k5eNaPmAgInS	uposlechnout
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Vojáci	voják	k1gMnPc1	voják
pátého	pátý	k4xOgMnSc4	pátý
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
jsem	být	k5eAaImIp1nS	být
váš	váš	k3xOp2gMnSc1	váš
císař	císař	k1gMnSc1	císař
<g/>
.	.	kIx.	.
</s>
<s>
Poznáváte	poznávat	k5eAaImIp2nP	poznávat
mě	já	k3xPp1nSc4	já
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
zavolal	zavolat	k5eAaPmAgMnS	zavolat
Bonaparte	bonapart	k1gInSc5	bonapart
a	a	k8xC	a
rozhalil	rozhalit	k5eAaPmAgMnS	rozhalit
svůj	svůj	k3xOyFgInSc4	svůj
myslivecký	myslivecký	k2eAgInSc4d1	myslivecký
kabát	kabát	k1gInSc4	kabát
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
vámi	vy	k3xPp2nPc7	vy
voják	voják	k1gMnSc1	voják
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
chce	chtít	k5eAaImIp3nS	chtít
zabít	zabít	k5eAaPmF	zabít
svého	svůj	k3xOyFgMnSc4	svůj
císaře	císař	k1gMnSc4	císař
<g/>
,	,	kIx,	,
tady	tady	k6eAd1	tady
stojím	stát	k5eAaImIp1nS	stát
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Po	po	k7c6	po
chvilce	chvilka	k1gFnSc6	chvilka
začali	začít	k5eAaPmAgMnP	začít
vojáci	voják	k1gMnPc1	voják
5	[number]	k4	5
<g/>
.	.	kIx.	.
řadového	řadový	k2eAgInSc2d1	řadový
odhazovat	odhazovat	k5eAaImF	odhazovat
pušky	puška	k1gFnSc2	puška
a	a	k8xC	a
za	za	k7c2	za
pokřiku	pokřik	k1gInSc2	pokřik
"	"	kIx"	"
<g/>
Vive	Vive	k1gFnSc1	Vive
ľ	ľ	k?	ľ
Empereur	Empereura	k1gFnPc2	Empereura
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Ať	ať	k9	ať
žije	žít	k5eAaImIp3nS	žít
císař	císař	k1gMnSc1	císař
<g/>
)	)	kIx)	)
přebíhali	přebíhat	k5eAaImAgMnP	přebíhat
k	k	k7c3	k
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
či	či	k8xC	či
stejně	stejně	k6eAd1	stejně
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zachovaly	zachovat	k5eAaPmAgFnP	zachovat
i	i	k9	i
ostatní	ostatní	k2eAgFnPc1d1	ostatní
jednotky	jednotka	k1gFnPc1	jednotka
stojící	stojící	k2eAgFnPc4d1	stojící
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
Napoleon	napoleon	k1gInSc1	napoleon
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
Lyonu	Lyon	k1gInSc2	Lyon
a	a	k8xC	a
o	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
stranu	strana	k1gFnSc4	strana
přešel	přejít	k5eAaPmAgMnS	přejít
i	i	k9	i
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
iniciátorů	iniciátor	k1gInPc2	iniciátor
první	první	k4xOgFnSc2	první
abdikace	abdikace	k1gFnSc2	abdikace
maršál	maršál	k1gMnSc1	maršál
Ney	Ney	k1gMnSc1	Ney
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
tvořil	tvořit	k5eAaImAgInS	tvořit
jedinou	jediný	k2eAgFnSc4d1	jediná
vážnější	vážní	k2eAgFnSc4d2	vážnější
překážku	překážka	k1gFnSc4	překážka
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
trůnem	trůn	k1gInSc7	trůn
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
za	za	k7c2	za
davového	davový	k2eAgNnSc2d1	davové
nadšení	nadšení	k1gNnSc2	nadšení
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
císař	císař	k1gMnSc1	císař
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Opětovné	opětovný	k2eAgFnPc1d1	opětovná
vlády	vláda	k1gFnPc1	vláda
nad	nad	k7c7	nad
Francií	Francie	k1gFnSc7	Francie
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
bez	bez	k7c2	bez
jediného	jediný	k2eAgInSc2d1	jediný
výstřelu	výstřel	k1gInSc2	výstřel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
Napoleon	napoleon	k1gInSc1	napoleon
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
vítězství	vítězství	k1gNnSc4	vítězství
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
frontě	fronta	k1gFnSc6	fronta
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
neměl	mít	k5eNaImAgInS	mít
obdoby	obdoba	k1gFnPc4	obdoba
<g/>
,	,	kIx,	,
zahraničněpolitická	zahraničněpolitický	k2eAgFnSc1d1	zahraničněpolitická
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
obrátila	obrátit	k5eAaPmAgFnS	obrátit
v	v	k7c4	v
jeho	on	k3xPp3gInSc4	on
neprospěch	neprospěch	k1gInSc4	neprospěch
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
vítězných	vítězný	k2eAgFnPc2d1	vítězná
mocností	mocnost	k1gFnPc2	mocnost
na	na	k7c6	na
kongresu	kongres	k1gInSc6	kongres
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c6	na
postupu	postup	k1gInSc6	postup
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gFnSc3	jeho
osobě	osoba	k1gFnSc3	osoba
a	a	k8xC	a
jako	jako	k8xS	jako
narušitel	narušitel	k1gMnSc1	narušitel
pokoje	pokoj	k1gInSc2	pokoj
světa	svět	k1gInSc2	svět
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
vydán	vydat	k5eAaPmNgInS	vydat
veřejnému	veřejný	k2eAgNnSc3d1	veřejné
soudnímu	soudní	k2eAgNnSc3d1	soudní
stíhání	stíhání	k1gNnSc3	stíhání
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	napoleon	k1gInSc1	napoleon
však	však	k9	však
nutně	nutně	k6eAd1	nutně
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
mír	mír	k1gInSc4	mír
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
uznání	uznání	k1gNnSc4	uznání
svého	svůj	k3xOyFgInSc2	svůj
návratu	návrat	k1gInSc2	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgInS	snažit
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
písemně	písemně	k6eAd1	písemně
kontaktovat	kontaktovat	k5eAaImF	kontaktovat
všechny	všechen	k3xTgInPc4	všechen
evropské	evropský	k2eAgInPc4d1	evropský
dvory	dvůr	k1gInPc4	dvůr
a	a	k8xC	a
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
je	on	k3xPp3gMnPc4	on
o	o	k7c6	o
svých	svůj	k3xOyFgInPc6	svůj
mírových	mírový	k2eAgInPc6d1	mírový
úmyslech	úmysl	k1gInPc6	úmysl
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nikdo	nikdo	k3yNnSc1	nikdo
mu	on	k3xPp3gNnSc3	on
na	na	k7c4	na
jeho	jeho	k3xOp3gInPc4	jeho
dopisy	dopis	k1gInPc4	dopis
neodpovídal	odpovídat	k5eNaImAgInS	odpovídat
<g/>
;	;	kIx,	;
někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
panovníků	panovník	k1gMnPc2	panovník
je	být	k5eAaImIp3nS	být
ani	ani	k9	ani
neotevřeli	otevřít	k5eNaPmAgMnP	otevřít
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
této	tento	k3xDgFnSc2	tento
situace	situace	k1gFnSc2	situace
si	se	k3xPyFc3	se
byl	být	k5eAaImAgInS	být
dobře	dobře	k6eAd1	dobře
vědom	vědom	k2eAgMnSc1d1	vědom
<g/>
,	,	kIx,	,
že	že	k8xS	že
jediným	jediný	k2eAgInSc7d1	jediný
nástrojem	nástroj	k1gInSc7	nástroj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jej	on	k3xPp3gMnSc4	on
může	moct	k5eAaImIp3nS	moct
udržet	udržet	k5eAaPmF	udržet
na	na	k7c6	na
trůnu	trůn	k1gInSc6	trůn
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
června	červen	k1gInSc2	červen
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
shromáždit	shromáždit	k5eAaPmF	shromáždit
na	na	k7c4	na
200	[number]	k4	200
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc3	který
však	však	k9	však
musel	muset	k5eAaImAgInS	muset
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
osmi	osm	k4xCc2	osm
samostatných	samostatný	k2eAgInPc2d1	samostatný
celků	celek	k1gInPc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
nejsilnějším	silný	k2eAgNnSc7d3	nejsilnější
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Armée	Armée	k1gFnSc1	Armée
du	du	k?	du
Nord	Nord	k1gInSc1	Nord
–	–	k?	–
Severní	severní	k2eAgFnSc1d1	severní
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
nesla	nést	k5eAaImAgFnS	nést
hlavní	hlavní	k2eAgFnSc4d1	hlavní
tíhu	tíha	k1gFnSc4	tíha
nadcházejících	nadcházející	k2eAgFnPc2d1	nadcházející
bojových	bojový	k2eAgFnPc2d1	bojová
operací	operace	k1gFnPc2	operace
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
s	s	k7c7	s
budováním	budování	k1gNnSc7	budování
úderné	úderný	k2eAgFnSc2d1	úderná
a	a	k8xC	a
obranné	obranný	k2eAgFnSc2d1	obranná
síly	síla	k1gFnSc2	síla
musel	muset	k5eAaImAgInS	muset
Napoleon	napoleon	k1gInSc1	napoleon
zkonsolidovat	zkonsolidovat	k5eAaPmF	zkonsolidovat
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
měsíčních	měsíční	k2eAgFnPc6d1	měsíční
tahanicích	tahanice	k1gFnPc6	tahanice
vydal	vydat	k5eAaPmAgInS	vydat
jako	jako	k8xS	jako
doplněk	doplněk	k1gInSc1	doplněk
císařské	císařský	k2eAgFnSc2d1	císařská
ústavy	ústava	k1gFnSc2	ústava
tzv.	tzv.	kA	tzv.
Dodatečný	dodatečný	k2eAgInSc1d1	dodatečný
akt	akt	k1gInSc1	akt
(	(	kIx(	(
<g/>
Acte	Acte	k1gInSc1	Acte
Additionel	Additionel	k1gInSc1	Additionel
<g/>
)	)	kIx)	)
a	a	k8xC	a
generál	generál	k1gMnSc1	generál
Grouchy	Groucha	k1gFnSc2	Groucha
potlačil	potlačit	k5eAaPmAgMnS	potlačit
roajalistické	roajalistický	k2eAgNnSc1d1	roajalistické
povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
králi	král	k1gMnSc6	král
věrných	věrný	k2eAgInPc6d1	věrný
departementech	departement	k1gInPc6	departement
(	(	kIx(	(
<g/>
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
čin	čin	k1gInSc4	čin
obdržel	obdržet	k5eAaPmAgMnS	obdržet
maršálskou	maršálský	k2eAgFnSc4d1	maršálská
hůl	hůl	k1gFnSc4	hůl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
nově	nova	k1gFnSc3	nova
utvořené	utvořený	k2eAgFnSc2d1	utvořená
VII	VII	kA	VII
<g/>
.	.	kIx.	.
koalici	koalice	k1gFnSc3	koalice
se	se	k3xPyFc4	se
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
nabízely	nabízet	k5eAaImAgInP	nabízet
dvě	dva	k4xCgFnPc4	dva
možnosti	možnost	k1gFnPc4	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Jednak	jednak	k8xC	jednak
defenzivní	defenzivní	k2eAgInSc1d1	defenzivní
způsob	způsob	k1gInSc1	způsob
vedení	vedení	k1gNnSc2	vedení
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
nesl	nést	k5eAaImAgInS	nést
stejná	stejný	k2eAgNnPc4d1	stejné
úskalí	úskalí	k1gNnPc4	úskalí
jako	jako	k8xS	jako
střetnutí	střetnutí	k1gNnSc4	střetnutí
s	s	k7c7	s
VI	VI	kA	VI
<g/>
.	.	kIx.	.
koalicí	koalice	k1gFnSc7	koalice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1814	[number]	k4	1814
<g/>
,	,	kIx,	,
a	a	k8xC	a
potom	potom	k6eAd1	potom
ofenzivně	ofenzivně	k6eAd1	ofenzivně
<g/>
–	–	k?	–
<g/>
agresivní	agresivní	k2eAgInSc4d1	agresivní
postup	postup	k1gInSc4	postup
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
něhož	jenž	k3xRgInSc2	jenž
by	by	kYmCp3nS	by
ničil	ničit	k5eAaImAgMnS	ničit
nepřátelské	přátelský	k2eNgFnSc2d1	nepřátelská
armády	armáda	k1gFnSc2	armáda
jednu	jeden	k4xCgFnSc4	jeden
po	po	k7c4	po
druhé	druhý	k4xOgMnPc4	druhý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
neprospěch	neprospěch	k1gInSc4	neprospěch
prve	prve	k6eAd1	prve
jmenované	jmenovaný	k2eAgFnSc2d1	jmenovaná
strategie	strategie	k1gFnSc2	strategie
hovořil	hovořit	k5eAaImAgInS	hovořit
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
doposud	doposud	k6eAd1	doposud
neseděl	sedět	k5eNaImAgMnS	sedět
na	na	k7c6	na
trůnu	trůn	k1gInSc6	trůn
tak	tak	k6eAd1	tak
pevně	pevně	k6eAd1	pevně
a	a	k8xC	a
nebyl	být	k5eNaImAgMnS	být
si	se	k3xPyFc3	se
jist	jist	k2eAgInSc1d1	jist
opozicí	opozice	k1gFnSc7	opozice
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
situace	situace	k1gFnSc1	situace
nedovolovala	dovolovat	k5eNaImAgFnS	dovolovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
umožnil	umožnit	k5eAaPmAgInS	umožnit
nepříteli	nepřítel	k1gMnSc3	nepřítel
obsadit	obsadit	k5eAaPmF	obsadit
a	a	k8xC	a
zničit	zničit	k5eAaPmF	zničit
rozsáhlá	rozsáhlý	k2eAgNnPc4d1	rozsáhlé
francouzská	francouzský	k2eAgNnPc4d1	francouzské
území	území	k1gNnPc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
vyrazit	vyrazit	k5eAaPmF	vyrazit
spojencům	spojenec	k1gMnPc3	spojenec
vstříc	vstříc	k6eAd1	vstříc
<g/>
.	.	kIx.	.
</s>
<s>
Vojáky	voják	k1gMnPc7	voják
národních	národní	k2eAgFnPc2d1	národní
gard	garda	k1gFnPc2	garda
zabezpečil	zabezpečit	k5eAaPmAgInS	zabezpečit
pobřeží	pobřeží	k1gNnSc4	pobřeží
i	i	k8xC	i
hranice	hranice	k1gFnPc4	hranice
s	s	k7c7	s
německými	německý	k2eAgInPc7d1	německý
státy	stát	k1gInPc7	stát
a	a	k8xC	a
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Armée	Armé	k1gInSc2	Armé
du	du	k?	du
Nord	Nord	k1gMnSc1	Nord
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
využít	využít	k5eAaPmF	využít
momentu	moment	k1gInSc3	moment
překvapení	překvapení	k1gNnSc2	překvapení
a	a	k8xC	a
vklínit	vklínit	k5eAaPmF	vklínit
se	se	k3xPyFc4	se
mezi	mezi	k7c4	mezi
obě	dva	k4xCgFnPc4	dva
nejbližší	blízký	k2eAgFnPc4d3	nejbližší
spojenecké	spojenecký	k2eAgFnPc4d1	spojenecká
armády	armáda	k1gFnPc4	armáda
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
začal	začít	k5eAaPmAgMnS	začít
potají	potají	k6eAd1	potají
soustřeďovat	soustřeďovat	k5eAaImF	soustřeďovat
své	svůj	k3xOyFgInPc4	svůj
sbory	sbor	k1gInPc4	sbor
mezi	mezi	k7c7	mezi
řekami	řeka	k1gFnPc7	řeka
Sambrou	Sambra	k1gFnSc7	Sambra
a	a	k8xC	a
Mázou	Máza	k1gFnSc7	Máza
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
jinými	jiný	k2eAgInPc7d1	jiný
ostentativními	ostentativní	k2eAgInPc7d1	ostentativní
manévry	manévr	k1gInPc7	manévr
snažil	snažit	k5eAaImAgMnS	snažit
vyvolat	vyvolat	k5eAaPmF	vyvolat
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
útok	útok	k1gInSc1	útok
jeho	jeho	k3xOp3gFnPc2	jeho
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
bude	být	k5eAaImBp3nS	být
směřovat	směřovat	k5eAaImF	směřovat
mezi	mezi	k7c4	mezi
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
Brusel	Brusel	k1gInSc4	Brusel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velitel	velitel	k1gMnSc1	velitel
anglo-batavské	angloatavský	k2eAgFnSc2d1	anglo-batavský
armády	armáda	k1gFnSc2	armáda
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Wellingtonu	Wellington	k1gInSc2	Wellington
reagoval	reagovat	k5eAaBmAgMnS	reagovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
si	se	k3xPyFc3	se
Napoleon	Napoleon	k1gMnSc1	Napoleon
přál	přát	k5eAaImAgMnS	přát
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
armádu	armáda	k1gFnSc4	armáda
rozmístil	rozmístit	k5eAaPmAgMnS	rozmístit
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
Brusel-Ostende	Brusel-Ostend	k1gInSc5	Brusel-Ostend
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
Prusy	Prus	k1gMnPc4	Prus
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
maršála	maršál	k1gMnSc2	maršál
Blüchera	Blücher	k1gMnSc2	Blücher
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
necelých	celý	k2eNgInPc2d1	necelý
60	[number]	k4	60
kilometrů	kilometr	k1gInPc2	kilometr
široká	široký	k2eAgFnSc1d1	široká
mezera	mezera	k1gFnSc1	mezera
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
střežil	střežit	k5eAaImAgMnS	střežit
pouze	pouze	k6eAd1	pouze
I.	I.	kA	I.
pruský	pruský	k2eAgInSc1d1	pruský
sbor	sbor	k1gInSc1	sbor
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
<g/>
,	,	kIx,	,
generálmajora	generálmajor	k1gMnSc4	generálmajor
Zietena	Zieten	k2eAgMnSc4d1	Zieten
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
těchto	tento	k3xDgFnPc2	tento
okolností	okolnost	k1gFnPc2	okolnost
Bonaparte	bonapart	k1gInSc5	bonapart
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
opustil	opustit	k5eAaPmAgMnS	opustit
Paříž	Paříž	k1gFnSc4	Paříž
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
později	pozdě	k6eAd2	pozdě
vyrazila	vyrazit	k5eAaPmAgFnS	vyrazit
do	do	k7c2	do
škvíry	škvíra	k1gFnSc2	škvíra
mezi	mezi	k7c7	mezi
koaličními	koaliční	k2eAgFnPc7d1	koaliční
armádami	armáda	k1gFnPc7	armáda
i	i	k9	i
Armée	Armée	k1gInSc4	Armée
du	du	k?	du
Nord	Nord	k1gInSc1	Nord
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
Francouzi	Francouz	k1gMnPc1	Francouz
překročili	překročit	k5eAaPmAgMnP	překročit
belgické	belgický	k2eAgFnPc4d1	belgická
hranice	hranice	k1gFnPc4	hranice
a	a	k8xC	a
za	za	k7c2	za
prvních	první	k4xOgInPc2	první
bojů	boj	k1gInPc2	boj
se	s	k7c7	s
Zietenem	Zieten	k1gInSc7	Zieten
se	se	k3xPyFc4	se
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
města	město	k1gNnSc2	město
Charleroi	Charlero	k1gFnSc2	Charlero
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgMnS	být
pruský	pruský	k2eAgMnSc1d1	pruský
generál	generál	k1gMnSc1	generál
v	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
hodinách	hodina	k1gFnPc6	hodina
setrvale	setrvale	k6eAd1	setrvale
zatlačován	zatlačován	k2eAgInSc4d1	zatlačován
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vést	vést	k5eAaImF	vést
mistrovský	mistrovský	k2eAgInSc4d1	mistrovský
zdržovací	zdržovací	k2eAgInSc4d1	zdržovací
boj	boj	k1gInSc4	boj
<g/>
,	,	kIx,	,
během	během	k7c2	během
nějž	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
proudech	proud	k1gInPc6	proud
spořádaně	spořádaně	k6eAd1	spořádaně
stahoval	stahovat	k5eAaImAgMnS	stahovat
na	na	k7c4	na
Brusel	Brusel	k1gInSc4	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
ústup	ústup	k1gInSc1	ústup
vedl	vést	k5eAaImAgInS	vést
směrem	směr	k1gInSc7	směr
přes	přes	k7c4	přes
křižovatku	křižovatka	k1gFnSc4	křižovatka
cest	cesta	k1gFnPc2	cesta
u	u	k7c2	u
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
vesničky	vesnička	k1gFnSc2	vesnička
Quatre-Bras	Quatre-Brasa	k1gFnPc2	Quatre-Brasa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
narychlo	narychlo	k6eAd1	narychlo
shromažďoval	shromažďovat	k5eAaImAgMnS	shromažďovat
vévoda	vévoda	k1gMnSc1	vévoda
z	z	k7c2	z
Wellingtonu	Wellington	k1gInSc2	Wellington
<g/>
,	,	kIx,	,
a	a	k8xC	a
současně	současně	k6eAd1	současně
k	k	k7c3	k
vesnici	vesnice	k1gFnSc3	vesnice
Ligny	Ligna	k1gFnSc2	Ligna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
soustřeďovaly	soustřeďovat	k5eAaImAgInP	soustřeďovat
zbylé	zbylý	k2eAgInPc1d1	zbylý
sbory	sbor	k1gInPc1	sbor
maršála	maršál	k1gMnSc2	maršál
Blüchera	Blücher	k1gMnSc2	Blücher
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
okolí	okolí	k1gNnSc2	okolí
těchto	tento	k3xDgNnPc2	tento
míst	místo	k1gNnPc2	místo
k	k	k7c3	k
těžkým	těžký	k2eAgInPc3d1	těžký
bojům	boj	k1gInPc3	boj
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
Napoleon	Napoleon	k1gMnSc1	Napoleon
osobně	osobně	k6eAd1	osobně
střetl	střetnout	k5eAaPmAgMnS	střetnout
s	s	k7c7	s
Prusy	Prus	k1gMnPc7	Prus
u	u	k7c2	u
Ligny	Ligna	k1gFnSc2	Ligna
a	a	k8xC	a
maršál	maršál	k1gMnSc1	maršál
Ney	Ney	k1gMnSc1	Ney
udeřil	udeřit	k5eAaPmAgMnS	udeřit
na	na	k7c4	na
část	část	k1gFnSc4	část
anglo	anglo	k1gNnSc1	anglo
<g/>
–	–	k?	–
<g/>
batavské	batavský	k2eAgFnSc2d1	Batavská
armády	armáda	k1gFnSc2	armáda
u	u	k7c2	u
Quatre-Bras	Quatre-Brasa	k1gFnPc2	Quatre-Brasa
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
bitvách	bitva	k1gFnPc6	bitva
získali	získat	k5eAaPmAgMnP	získat
Francouzi	Francouz	k1gMnPc1	Francouz
úplné	úplný	k2eAgFnSc2d1	úplná
vítězství	vítězství	k1gNnSc3	vítězství
a	a	k8xC	a
spojenci	spojenec	k1gMnPc1	spojenec
byli	být	k5eAaImAgMnP	být
nuceni	nucen	k2eAgMnPc1d1	nucen
ustoupit	ustoupit	k5eAaPmF	ustoupit
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
Napoleon	Napoleon	k1gMnSc1	Napoleon
vyslal	vyslat	k5eAaPmAgMnS	vyslat
ve	v	k7c6	v
stopách	stopa	k1gFnPc6	stopa
maršála	maršál	k1gMnSc2	maršál
Blüchera	Blücher	k1gMnSc2	Blücher
třiatřicetitisícový	třiatřicetitisícový	k2eAgInSc4d1	třiatřicetitisícový
sbor	sbor	k1gInSc4	sbor
maršála	maršál	k1gMnSc4	maršál
Grouchyho	Grouchy	k1gMnSc4	Grouchy
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
za	za	k7c7	za
Wellingtonem	Wellington	k1gInSc7	Wellington
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Brusel	Brusel	k1gInSc4	Brusel
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
kvůli	kvůli	k7c3	kvůli
opožděnému	opožděný	k2eAgNnSc3d1	opožděné
zahájení	zahájení	k1gNnSc3	zahájení
pronásledování	pronásledování	k1gNnSc2	pronásledování
<g/>
,	,	kIx,	,
špatnému	špatný	k2eAgNnSc3d1	špatné
počasí	počasí	k1gNnSc3	počasí
a	a	k8xC	a
schopnému	schopný	k2eAgInSc3d1	schopný
zadnímu	zadní	k2eAgInSc3d1	zadní
voji	voj	k1gInSc3	voj
anglo-batavské	angloatavský	k2eAgFnSc2d1	anglo-batavský
armády	armáda	k1gFnSc2	armáda
se	se	k3xPyFc4	se
britskému	britský	k2eAgMnSc3d1	britský
veliteli	velitel	k1gMnPc7	velitel
podařilo	podařit	k5eAaPmAgNnS	podařit
spořádaně	spořádaně	k6eAd1	spořádaně
stáhnout	stáhnout	k5eAaPmF	stáhnout
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
zaujmout	zaujmout	k5eAaPmF	zaujmout
postavení	postavení	k1gNnSc4	postavení
u	u	k7c2	u
statku	statek	k1gInSc2	statek
(	(	kIx(	(
<g/>
či	či	k8xC	či
hostince	hostinec	k1gInPc1	hostinec
<g/>
)	)	kIx)	)
Belle	bell	k1gInSc5	bell
Alliance	Allianka	k1gFnSc6	Allianka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
svést	svést	k5eAaPmF	svést
generální	generální	k2eAgFnSc4d1	generální
bitvu	bitva	k1gFnSc4	bitva
(	(	kIx(	(
<g/>
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
si	se	k3xPyFc3	se
již	již	k9	již
byl	být	k5eAaImAgMnS	být
jist	jist	k2eAgMnSc1d1	jist
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
stačí	stačit	k5eAaBmIp3nS	stačit
pruská	pruský	k2eAgFnSc1d1	pruská
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
soustřeďující	soustřeďující	k2eAgFnSc1d1	soustřeďující
se	se	k3xPyFc4	se
u	u	k7c2	u
Wavre	Wavr	k1gInSc5	Wavr
<g/>
,	,	kIx,	,
připojit	připojit	k5eAaPmF	připojit
včas	včas	k6eAd1	včas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
Napoleon	Napoleon	k1gMnSc1	Napoleon
promarnil	promarnit	k5eAaPmAgMnS	promarnit
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
dopoledne	dopoledne	k1gNnSc2	dopoledne
a	a	k8xC	a
na	na	k7c4	na
protivníka	protivník	k1gMnSc4	protivník
zaútočil	zaútočit	k5eAaPmAgMnS	zaútočit
teprve	teprve	k6eAd1	teprve
kolem	kolem	k7c2	kolem
jedenácté	jedenáctý	k4xOgFnSc2	jedenáctý
hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
ani	ani	k8xC	ani
přes	přes	k7c4	přes
počáteční	počáteční	k2eAgNnSc4d1	počáteční
několikatisícové	několikatisícový	k2eAgNnSc4d1	několikatisícové
přečíslení	přečíslení	k1gNnSc4	přečíslení
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
jeho	jeho	k3xOp3gNnSc7	jeho
vojáci	voják	k1gMnPc1	voják
nad	nad	k7c7	nad
Brity	Brit	k1gMnPc7	Brit
získat	získat	k5eAaPmF	získat
rozhodující	rozhodující	k2eAgFnSc4d1	rozhodující
převahu	převaha	k1gFnSc4	převaha
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
v	v	k7c6	v
odpoledních	odpolední	k2eAgFnPc6d1	odpolední
hodinách	hodina	k1gFnPc6	hodina
na	na	k7c4	na
francouzské	francouzský	k2eAgNnSc4d1	francouzské
pravé	pravý	k2eAgNnSc4d1	pravé
křídlo	křídlo	k1gNnSc4	křídlo
nečekaně	nečekaně	k6eAd1	nečekaně
zaútočilo	zaútočit	k5eAaPmAgNnS	zaútočit
50	[number]	k4	50
000	[number]	k4	000
Prusů	Prus	k1gMnPc2	Prus
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
o	o	k7c6	o
výsledku	výsledek	k1gInSc6	výsledek
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Waterloo	Waterloo	k1gNnSc2	Waterloo
rozhodnuto	rozhodnut	k2eAgNnSc1d1	rozhodnuto
<g/>
.	.	kIx.	.
</s>
<s>
Naposled	naposled	k6eAd1	naposled
kolem	kolem	k7c2	kolem
deváté	devátý	k4xOgFnSc2	devátý
hodiny	hodina	k1gFnSc2	hodina
večerní	večerní	k2eAgFnSc6d1	večerní
se	se	k3xPyFc4	se
Napoleon	Napoleon	k1gMnSc1	Napoleon
pokusil	pokusit	k5eAaPmAgMnS	pokusit
rozprášenou	rozprášený	k2eAgFnSc4d1	rozprášená
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
armádu	armáda	k1gFnSc4	armáda
prchající	prchající	k2eAgFnSc4d1	prchající
na	na	k7c4	na
Genape	Genap	k1gInSc5	Genap
a	a	k8xC	a
Maison	Maison	k1gInSc1	Maison
du	du	k?	du
Roi	Roi	k1gFnSc2	Roi
zastavit	zastavit	k5eAaPmF	zastavit
a	a	k8xC	a
postavit	postavit	k5eAaPmF	postavit
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
již	již	k6eAd1	již
nezůstávalo	zůstávat	k5eNaImAgNnS	zůstávat
mnoho	mnoho	k4c1	mnoho
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
by	by	kYmCp3nP	by
uposlechli	uposlechnout	k5eAaPmAgMnP	uposlechnout
<g/>
.	.	kIx.	.
<g/>
Vědom	vědom	k2eAgInSc1d1	vědom
si	se	k3xPyFc3	se
bezvýchodnosti	bezvýchodnost	k1gFnPc4	bezvýchodnost
své	svůj	k3xOyFgFnSc2	svůj
situace	situace	k1gFnSc2	situace
odjel	odjet	k5eAaPmAgMnS	odjet
nedlouho	dlouho	k6eNd1	dlouho
poté	poté	k6eAd1	poté
francouzský	francouzský	k2eAgMnSc1d1	francouzský
císař	císař	k1gMnSc1	císař
z	z	k7c2	z
bojiště	bojiště	k1gNnSc2	bojiště
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
jedné	jeden	k4xCgFnSc2	jeden
hodiny	hodina	k1gFnSc2	hodina
ranní	ranní	k1gFnSc2	ranní
dorazil	dorazit	k5eAaPmAgMnS	dorazit
ke	k	k7c3	k
Quatre-Bras	Quatre-Bras	k1gInSc4	Quatre-Bras
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c6	na
chvíli	chvíle	k1gFnSc6	chvíle
zůstal	zůstat	k5eAaPmAgInS	zůstat
stát	stát	k1gInSc1	stát
nad	nad	k7c7	nad
těly	tělo	k1gNnPc7	tělo
mrtvých	mrtvý	k1gMnPc2	mrtvý
vojáků	voják	k1gMnPc2	voják
z	z	k7c2	z
předchozí	předchozí	k2eAgFnSc2d1	předchozí
bitvy	bitva	k1gFnSc2	bitva
<g/>
;	;	kIx,	;
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
chvíli	chvíle	k1gFnSc4	chvíle
mu	on	k3xPp3gMnSc3	on
prý	prý	k9	prý
z	z	k7c2	z
očí	oko	k1gNnPc2	oko
tekly	téct	k5eAaImAgInP	téct
slzy	slza	k1gFnPc4	slza
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
soukromém	soukromý	k2eAgInSc6d1	soukromý
obřadu	obřad	k1gInSc6	obřad
se	se	k3xPyFc4	se
odebral	odebrat	k5eAaPmAgInS	odebrat
na	na	k7c4	na
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Branami	brána	k1gFnPc7	brána
francouzské	francouzský	k2eAgFnSc2d1	francouzská
metropole	metropol	k1gFnSc2	metropol
projel	projet	k5eAaPmAgMnS	projet
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
cestu	cesta	k1gFnSc4	cesta
údajně	údajně	k6eAd1	údajně
dřímal	dřímat	k5eAaImAgMnS	dřímat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
mezitím	mezitím	k6eAd1	mezitím
začal	začít	k5eAaPmAgMnS	začít
ministr	ministr	k1gMnSc1	ministr
policie	policie	k1gFnSc2	policie
Joseph	Joseph	k1gMnSc1	Joseph
Fouché	Fouchý	k2eAgInPc1d1	Fouchý
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
poslaneckou	poslanecký	k2eAgFnSc4d1	Poslanecká
sněmovnu	sněmovna	k1gFnSc4	sněmovna
i	i	k8xC	i
sněmovnu	sněmovna	k1gFnSc4	sněmovna
pairů	pair	k1gMnPc2	pair
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
následkem	následkem	k7c2	následkem
těchto	tento	k3xDgFnPc2	tento
intrik	intrika	k1gFnPc2	intrika
požadovaly	požadovat	k5eAaImAgFnP	požadovat
Napoleonovu	Napoleonův	k2eAgFnSc4d1	Napoleonova
okamžitou	okamžitý	k2eAgFnSc4d1	okamžitá
abdikaci	abdikace	k1gFnSc4	abdikace
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
vše	všechen	k3xTgNnSc4	všechen
přijal	přijmout	k5eAaPmAgMnS	přijmout
se	s	k7c7	s
zdánlivým	zdánlivý	k2eAgInSc7d1	zdánlivý
klidem	klid	k1gInSc7	klid
a	a	k8xC	a
abdikaci	abdikace	k1gFnSc4	abdikace
podepsal	podepsat	k5eAaPmAgMnS	podepsat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
Napoleona	Napoleon	k1gMnSc2	Napoleon
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
strávil	strávit	k5eAaPmAgMnS	strávit
v	v	k7c6	v
Elysejském	elysejský	k2eAgInSc6d1	elysejský
paláci	palác	k1gInSc6	palác
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Rochefortu	Rochefort	k1gInSc2	Rochefort
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
odplul	odplout	k5eAaPmAgMnS	odplout
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
vydal	vydat	k5eAaPmAgInS	vydat
se	se	k3xPyFc4	se
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
půdu	půda	k1gFnSc4	půda
Anglie	Anglie	k1gFnSc2	Anglie
však	však	k9	však
nikdy	nikdy	k6eAd1	nikdy
nevstoupil	vstoupit	k5eNaPmAgMnS	vstoupit
a	a	k8xC	a
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
internaci	internace	k1gFnSc6	internace
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Svaté	svatý	k2eAgFnSc2d1	svatá
Heleny	Helena	k1gFnSc2	Helena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vyhnanství	vyhnanství	k1gNnSc3	vyhnanství
na	na	k7c6	na
Svaté	svatý	k2eAgFnSc6d1	svatá
Heleně	Helena	k1gFnSc6	Helena
===	===	k?	===
</s>
</p>
<p>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Napoleon	Napoleon	k1gMnSc1	Napoleon
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
jméno	jméno	k1gNnSc4	jméno
místa	místo	k1gNnSc2	místo
své	svůj	k3xOyFgFnSc2	svůj
internace	internace	k1gFnSc2	internace
<g/>
,	,	kIx,	,
protestoval	protestovat	k5eAaBmAgMnS	protestovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
domníval	domnívat	k5eAaImAgMnS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
nikdo	nikdo	k3yNnSc1	nikdo
nemá	mít	k5eNaImIp3nS	mít
právo	právo	k1gNnSc4	právo
zacházet	zacházet	k5eAaImF	zacházet
jako	jako	k9	jako
s	s	k7c7	s
válečným	válečný	k2eAgMnSc7d1	válečný
zajatcem	zajatec	k1gMnSc7	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
odcházelo	odcházet	k5eAaImAgNnS	odcházet
na	na	k7c4	na
Svatou	svatý	k2eAgFnSc4d1	svatá
Helenu	Helena	k1gFnSc4	Helena
jen	jen	k6eAd1	jen
několik	několik	k4yIc4	několik
nejbližších	blízký	k2eAgFnPc2d3	nejbližší
osob	osoba	k1gFnPc2	osoba
<g/>
;	;	kIx,	;
maršál	maršál	k1gMnSc1	maršál
Bertrand	Bertrand	k1gInSc1	Bertrand
a	a	k8xC	a
generál	generál	k1gMnSc1	generál
Montholon	Montholon	k1gInSc1	Montholon
si	se	k3xPyFc3	se
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přiváželi	přivážet	k5eAaImAgMnP	přivážet
i	i	k9	i
své	svůj	k3xOyFgFnSc2	svůj
manželky	manželka	k1gFnSc2	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
neměli	mít	k5eNaImAgMnP	mít
Britové	Brit	k1gMnPc1	Brit
pro	pro	k7c4	pro
francouzského	francouzský	k2eAgMnSc4d1	francouzský
excísaře	excísař	k1gMnSc4	excísař
připraveno	připravit	k5eAaPmNgNnS	připravit
vhodné	vhodný	k2eAgNnSc1d1	vhodné
ubytování	ubytování	k1gNnSc1	ubytování
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
přechodně	přechodně	k6eAd1	přechodně
usídlil	usídlit	k5eAaPmAgMnS	usídlit
v	v	k7c6	v
zahradním	zahradní	k2eAgInSc6d1	zahradní
domku	domek	k1gInSc6	domek
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Briars	Briarsa	k1gFnPc2	Briarsa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Šípková	šípkový	k2eAgFnSc1d1	šípková
růže	růže	k1gFnSc1	růže
<g/>
)	)	kIx)	)
na	na	k7c6	na
pozemcích	pozemek	k1gInPc6	pozemek
statku	statek	k1gInSc2	statek
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
patřil	patřit	k5eAaImAgMnS	patřit
anglickému	anglický	k2eAgMnSc3d1	anglický
velkoobchodníkovi	velkoobchodník	k1gMnSc3	velkoobchodník
Belcombovi	Belcomba	k1gMnSc3	Belcomba
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
šestnáctiletou	šestnáctiletý	k2eAgFnSc7d1	šestnáctiletá
obchodníkovou	obchodníkův	k2eAgFnSc7d1	obchodníkova
dcerou	dcera	k1gFnSc7	dcera
Betsy	Betsa	k1gFnSc2	Betsa
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
i	i	k9	i
v	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
pobytu	pobyt	k1gInSc2	pobyt
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
vítanou	vítaný	k2eAgFnSc7d1	vítaná
společnicí	společnice	k1gFnSc7	společnice
a	a	k8xC	a
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
pociťoval	pociťovat	k5eAaImAgMnS	pociťovat
zřejmě	zřejmě	k6eAd1	zřejmě
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
jen	jen	k6eAd1	jen
otcovskou	otcovský	k2eAgFnSc4d1	otcovská
náklonnost	náklonnost	k1gFnSc4	náklonnost
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Napoleon	napoleon	k1gInSc1	napoleon
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
do	do	k7c2	do
vily	vila	k1gFnSc2	vila
Longwood	Longwooda	k1gFnPc2	Longwooda
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
postavení	postavení	k1gNnSc4	postavení
snášel	snášet	k5eAaImAgInS	snášet
stoicky	stoicky	k6eAd1	stoicky
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
lidé	člověk	k1gMnPc1	člověk
kolem	kolem	k7c2	kolem
něj	on	k3xPp3gNnSc2	on
v	v	k7c4	v
jeho	jeho	k3xOp3gNnSc4	jeho
chování	chování	k1gNnSc4	chování
a	a	k8xC	a
výrazu	výraz	k1gInSc2	výraz
často	často	k6eAd1	často
pozorovali	pozorovat	k5eAaImAgMnP	pozorovat
melancholickou	melancholický	k2eAgFnSc4d1	melancholická
tesknotu	tesknota	k1gFnSc4	tesknota
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
mnoho	mnoho	k6eAd1	mnoho
četl	číst	k5eAaImAgMnS	číst
<g/>
,	,	kIx,	,
jezdil	jezdit	k5eAaImAgMnS	jezdit
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
,	,	kIx,	,
chodil	chodit	k5eAaImAgMnS	chodit
na	na	k7c4	na
procházky	procházka	k1gFnPc4	procházka
a	a	k8xC	a
diktoval	diktovat	k5eAaImAgMnS	diktovat
své	svůj	k3xOyFgFnSc2	svůj
paměti	paměť	k1gFnSc2	paměť
hraběti	hrabě	k1gMnSc3	hrabě
de	de	k?	de
Las	laso	k1gNnPc2	laso
Cases	Cases	k1gInSc1	Cases
<g/>
.	.	kIx.	.
<g/>
Jeho	jeho	k3xOp3gFnSc1	jeho
volnost	volnost	k1gFnSc1	volnost
byla	být	k5eAaImAgFnS	být
nečekaně	nečekaně	k6eAd1	nečekaně
omezena	omezit	k5eAaPmNgFnS	omezit
<g/>
,	,	kIx,	,
když	když	k8xS	když
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
dorazil	dorazit	k5eAaPmAgMnS	dorazit
nový	nový	k2eAgMnSc1d1	nový
guvernér	guvernér	k1gMnSc1	guvernér
sir	sir	k1gMnSc1	sir
Hudson	Hudson	k1gMnSc1	Hudson
Lowe	Lowe	k1gFnSc1	Lowe
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
pevně	pevně	k6eAd1	pevně
odhodlaný	odhodlaný	k2eAgMnSc1d1	odhodlaný
udělat	udělat	k5eAaPmF	udělat
všechno	všechen	k3xTgNnSc4	všechen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Napoleon	Napoleon	k1gMnSc1	Napoleon
opět	opět	k6eAd1	opět
neoctl	octnout	k5eNaPmAgMnS	octnout
na	na	k7c6	na
svobodě	svoboda	k1gFnSc6	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Omezil	omezit	k5eAaPmAgMnS	omezit
proto	proto	k8xC	proto
možnosti	možnost	k1gFnPc1	možnost
jeho	on	k3xPp3gInSc2	on
pohybu	pohyb	k1gInSc2	pohyb
po	po	k7c6	po
ostrově	ostrov	k1gInSc6	ostrov
<g/>
,	,	kIx,	,
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
přímou	přímý	k2eAgFnSc4d1	přímá
ostrahu	ostraha	k1gFnSc4	ostraha
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
rapidně	rapidně	k6eAd1	rapidně
snížil	snížit	k5eAaPmAgMnS	snížit
výdaje	výdaj	k1gInPc4	výdaj
jeho	jeho	k3xOp3gFnSc2	jeho
domácnosti	domácnost	k1gFnSc2	domácnost
a	a	k8xC	a
zavedl	zavést	k5eAaPmAgMnS	zavést
další	další	k2eAgFnPc4d1	další
menší	malý	k2eAgFnPc4d2	menší
či	či	k8xC	či
větší	veliký	k2eAgFnPc4d2	veliký
restrikce	restrikce	k1gFnPc4	restrikce
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
měly	mít	k5eAaImAgFnP	mít
jeho	jeho	k3xOp3gFnSc4	jeho
vězni	vězeň	k1gMnPc1	vězeň
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
stále	stále	k6eAd1	stále
pokládal	pokládat	k5eAaImAgMnS	pokládat
za	za	k7c4	za
generála	generál	k1gMnSc4	generál
<g/>
,	,	kIx,	,
ztížit	ztížit	k5eAaPmF	ztížit
podmínky	podmínka	k1gFnPc4	podmínka
jeho	on	k3xPp3gInSc2	on
pobytu	pobyt	k1gInSc2	pobyt
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
proto	proto	k8xC	proto
zahájil	zahájit	k5eAaPmAgMnS	zahájit
proti	proti	k7c3	proti
Lowemu	Lowemo	k1gNnSc3	Lowemo
bezohlednou	bezohledný	k2eAgFnSc4d1	bezohledná
kampaň	kampaň	k1gFnSc4	kampaň
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
jej	on	k3xPp3gNnSc2	on
vyvést	vyvést	k5eAaPmF	vyvést
z	z	k7c2	z
rovnováhy	rovnováha	k1gFnSc2	rovnováha
malichernými	malicherný	k2eAgInPc7d1	malicherný
dotazy	dotaz	k1gInPc7	dotaz
ohledně	ohledně	k7c2	ohledně
protokolu	protokol	k1gInSc2	protokol
a	a	k8xC	a
svých	svůj	k3xOyFgFnPc2	svůj
osobních	osobní	k2eAgFnPc2d1	osobní
výsad	výsada	k1gFnPc2	výsada
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
spor	spor	k1gInSc1	spor
však	však	k9	však
vyústil	vyústit	k5eAaPmAgInS	vyústit
ve	v	k7c4	v
stále	stále	k6eAd1	stále
větší	veliký	k2eAgFnSc4d2	veliký
Napoleonovu	Napoleonův	k2eAgFnSc4d1	Napoleonova
izolaci	izolace	k1gFnSc4	izolace
a	a	k8xC	a
návštěvníci	návštěvník	k1gMnPc1	návštěvník
získávali	získávat	k5eAaImAgMnP	získávat
jen	jen	k9	jen
stěží	stěží	k6eAd1	stěží
svolení	svolení	k1gNnSc4	svolení
k	k	k7c3	k
návštěvám	návštěva	k1gFnPc3	návštěva
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1818	[number]	k4	1818
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
za	za	k7c4	za
odesílání	odesílání	k1gNnSc4	odesílání
tajných	tajný	k2eAgFnPc2d1	tajná
Napoleonových	Napoleonových	k2eAgFnPc2d1	Napoleonových
zpráv	zpráva	k1gFnPc2	zpráva
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Lowem	Lowem	k1gInSc4	Lowem
vypovězena	vypovědět	k5eAaPmNgFnS	vypovědět
rodina	rodina	k1gFnSc1	rodina
Belcombeových	Belcombeův	k2eAgInPc2d1	Belcombeův
a	a	k8xC	a
excísař	excísař	k1gMnSc1	excísař
se	se	k3xPyFc4	se
po	po	k7c6	po
ztrátě	ztráta	k1gFnSc6	ztráta
své	svůj	k3xOyFgFnSc2	svůj
mladé	mladý	k2eAgFnSc2d1	mladá
společnice	společnice	k1gFnSc2	společnice
stával	stávat	k5eAaImAgInS	stávat
stále	stále	k6eAd1	stále
zasmušilejším	zasmušilý	k2eAgNnSc6d2	zasmušilý
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
Loweových	Loweův	k2eAgNnPc6d1	Loweův
opatřeních	opatření	k1gNnPc6	opatření
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
začaly	začít	k5eAaPmAgFnP	začít
výrazně	výrazně	k6eAd1	výrazně
zhoršovat	zhoršovat	k5eAaImF	zhoršovat
Napoleonovy	Napoleonův	k2eAgFnPc4d1	Napoleonova
životní	životní	k2eAgFnPc4d1	životní
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
začínal	začínat	k5eAaImAgInS	začínat
být	být	k5eAaImF	být
vlhký	vlhký	k2eAgInSc1d1	vlhký
<g/>
,	,	kIx,	,
množily	množit	k5eAaImAgInP	množit
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
krysy	krysa	k1gFnPc1	krysa
<g/>
,	,	kIx,	,
na	na	k7c6	na
tapetách	tapeta	k1gFnPc6	tapeta
se	se	k3xPyFc4	se
tvořila	tvořit	k5eAaImAgFnS	tvořit
plíseň	plíseň	k1gFnSc1	plíseň
a	a	k8xC	a
následkem	následkem	k7c2	následkem
zimy	zima	k1gFnSc2	zima
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
zvyšovala	zvyšovat	k5eAaImAgFnS	zvyšovat
nad	nad	k7c4	nad
únosnou	únosný	k2eAgFnSc4d1	únosná
mez	mez	k1gFnSc4	mez
<g/>
,	,	kIx,	,
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
níž	jenž	k3xRgFnSc3	jenž
byl	být	k5eAaImAgInS	být
nucen	nutit	k5eAaImNgMnS	nutit
rozštípat	rozštípat	k5eAaPmF	rozštípat
i	i	k9	i
některý	některý	k3yIgInSc4	některý
nábytek	nábytek	k1gInSc4	nábytek
<g/>
,	,	kIx,	,
trpěl	trpět	k5eAaImAgInS	trpět
ustavičným	ustavičný	k2eAgNnSc7d1	ustavičné
nachlazením	nachlazení	k1gNnSc7	nachlazení
a	a	k8xC	a
revmatizmem	revmatizmus	k1gInSc7	revmatizmus
<g/>
.	.	kIx.	.
</s>
<s>
Takřka	takřka	k6eAd1	takřka
jedinými	jediný	k2eAgMnPc7d1	jediný
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
života	život	k1gInSc2	život
Napoleon	Napoleon	k1gMnSc1	Napoleon
stýkal	stýkat	k5eAaImAgMnS	stýkat
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
kromě	kromě	k7c2	kromě
sloužících	sloužící	k1gMnPc2	sloužící
pouze	pouze	k6eAd1	pouze
korsický	korsický	k2eAgMnSc1d1	korsický
rodák	rodák	k1gMnSc1	rodák
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
doktor	doktor	k1gMnSc1	doktor
Antommarchi	Antommarche	k1gFnSc4	Antommarche
<g/>
,	,	kIx,	,
maršál	maršál	k1gMnSc1	maršál
Bertrand	Bertrand	k1gInSc1	Bertrand
a	a	k8xC	a
generál	generál	k1gMnSc1	generál
Montholon	Montholon	k1gInSc1	Montholon
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupujícím	postupující	k2eAgInSc7d1	postupující
časem	čas	k1gInSc7	čas
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
rapidně	rapidně	k6eAd1	rapidně
zhoršoval	zhoršovat	k5eAaImAgInS	zhoršovat
a	a	k8xC	a
osobně	osobně	k6eAd1	osobně
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
trpí	trpět	k5eAaImIp3nS	trpět
rakovinou	rakovina	k1gFnSc7	rakovina
žaludku	žaludek	k1gInSc2	žaludek
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
nemoci	nemoc	k1gFnSc6	nemoc
vtipkoval	vtipkovat	k5eAaImAgMnS	vtipkovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Rakovina	rakovina	k1gFnSc1	rakovina
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
Waterloo	Waterloo	k1gNnSc1	Waterloo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1821	[number]	k4	1821
<g/>
,	,	kIx,	,
upoutaný	upoutaný	k2eAgInSc1d1	upoutaný
na	na	k7c6	na
lůžku	lůžko	k1gNnSc6	lůžko
a	a	k8xC	a
trýzněn	trýzněn	k2eAgInSc1d1	trýzněn
stále	stále	k6eAd1	stále
častějšími	častý	k2eAgFnPc7d2	častější
bolestmi	bolest	k1gFnPc7	bolest
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
diktovat	diktovat	k5eAaImF	diktovat
závěť	závěť	k1gFnSc4	závěť
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
rozdal	rozdat	k5eAaPmAgMnS	rozdat
své	svůj	k3xOyFgNnSc4	svůj
jmění	jmění	k1gNnSc4	jmění
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
čítalo	čítat	k5eAaImAgNnS	čítat
200	[number]	k4	200
milionů	milion	k4xCgInPc2	milion
franků	frank	k1gInPc2	frank
ve	v	k7c6	v
zlatě	zlato	k1gNnSc6	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Polovinu	polovina	k1gFnSc4	polovina
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
mezi	mezi	k7c7	mezi
vojáky	voják	k1gMnPc7	voják
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
bojovali	bojovat	k5eAaImAgMnP	bojovat
v	v	k7c6	v
řadách	řada	k1gFnPc6	řada
jeho	jeho	k3xOp3gFnPc2	jeho
armád	armáda	k1gFnPc2	armáda
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
půlku	půlka	k1gFnSc4	půlka
pak	pak	k6eAd1	pak
odkázal	odkázat	k5eAaPmAgInS	odkázat
krajům	kraj	k1gInPc3	kraj
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
byly	být	k5eAaImAgInP	být
nejvíce	nejvíce	k6eAd1	nejvíce
postiženy	postižen	k2eAgInPc1d1	postižen
válkami	válka	k1gFnPc7	válka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1814	[number]	k4	1814
a	a	k8xC	a
1815	[number]	k4	1815
<g/>
.	.	kIx.	.
</s>
<s>
Testament	testament	k1gInSc1	testament
dokončil	dokončit	k5eAaPmAgInS	dokončit
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
týdnech	týden	k1gInPc6	týden
jej	on	k3xPp3gMnSc4	on
nemoc	nemoc	k1gFnSc1	nemoc
zmohla	zmoct	k5eAaPmAgFnS	zmoct
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebyl	být	k5eNaImAgInS	být
schopen	schopen	k2eAgInSc1d1	schopen
věnovat	věnovat	k5eAaImF	věnovat
se	se	k3xPyFc4	se
materiálním	materiální	k2eAgFnPc3d1	materiální
záležitostem	záležitost	k1gFnPc3	záležitost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Smrt	smrt	k1gFnSc1	smrt
a	a	k8xC	a
pohřby	pohřeb	k1gInPc1	pohřeb
===	===	k?	===
</s>
</p>
<p>
<s>
Sužován	sužován	k2eAgMnSc1d1	sužován
bolestí	bolestit	k5eAaImIp3nS	bolestit
a	a	k8xC	a
křečovými	křečový	k2eAgInPc7d1	křečový
záchvaty	záchvat	k1gInPc7	záchvat
Napoleon	Napoleon	k1gMnSc1	Napoleon
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c4	v
podvečer	podvečer	k1gInSc4	podvečer
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1821	[number]	k4	1821
v	v	k7c4	v
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
49	[number]	k4	49
hodin	hodina	k1gFnPc2	hodina
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
jednapadesáti	jednapadesát	k4xCc2	jednapadesát
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
provedené	provedený	k2eAgFnSc6d1	provedená
pitvě	pitva	k1gFnSc6	pitva
se	s	k7c7	s
všemi	všecek	k3xTgFnPc7	všecek
vojenskými	vojenský	k2eAgFnPc7d1	vojenská
poctami	pocta	k1gFnPc7	pocta
pohřbeno	pohřbít	k5eAaPmNgNnS	pohřbít
u	u	k7c2	u
pramene	pramen	k1gInSc2	pramen
Torbett	Torbetta	k1gFnPc2	Torbetta
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Spring	Spring	k1gInSc1	Spring
na	na	k7c6	na
pozemcích	pozemek	k1gInPc6	pozemek
Longwoodu	Longwood	k1gInSc2	Longwood
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1840	[number]	k4	1840
<g/>
,	,	kIx,	,
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Filipa	Filip	k1gMnSc2	Filip
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
Napoleonovy	Napoleonův	k2eAgInPc1d1	Napoleonův
ostatky	ostatek	k1gInPc1	ostatek
exhumovány	exhumovat	k5eAaBmNgInP	exhumovat
a	a	k8xC	a
převezeny	převézt	k5eAaPmNgInP	převézt
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byly	být	k5eAaImAgInP	být
slavnostně	slavnostně	k6eAd1	slavnostně
uloženy	uložit	k5eAaPmNgInP	uložit
do	do	k7c2	do
katafalku	katafalk	k1gInSc2	katafalk
z	z	k7c2	z
růžového	růžový	k2eAgInSc2d1	růžový
mramoru	mramor	k1gInSc2	mramor
v	v	k7c6	v
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
Invalidovně	invalidovna	k1gFnSc6	invalidovna
<g/>
.	.	kIx.	.
<g/>
Většina	většina	k1gFnSc1	většina
pozdějších	pozdní	k2eAgMnPc2d2	pozdější
historiků	historik	k1gMnPc2	historik
a	a	k8xC	a
publicistů	publicista	k1gMnPc2	publicista
předpokládala	předpokládat	k5eAaImAgFnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
skonem	skon	k1gInSc7	skon
excísaře	excísař	k1gMnSc2	excísař
stálo	stát	k5eAaImAgNnS	stát
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
i	i	k9	i
jeho	on	k3xPp3gInSc4	on
otec	otec	k1gMnSc1	otec
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
sestry	sestra	k1gFnPc4	sestra
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
rakovina	rakovina	k1gFnSc1	rakovina
žaludku	žaludek	k1gInSc2	žaludek
<g/>
.	.	kIx.	.
</s>
<s>
Podezření	podezření	k1gNnSc4	podezření
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
chorobu	choroba	k1gFnSc4	choroba
měl	mít	k5eAaImAgMnS	mít
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
i	i	k9	i
sám	sám	k3xTgMnSc1	sám
Napoleon	Napoleon	k1gMnSc1	Napoleon
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zveřejněné	zveřejněný	k2eAgFnSc2d1	zveřejněná
pitevní	pitevní	k2eAgFnSc2d1	pitevní
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
zpracoval	zpracovat	k5eAaPmAgMnS	zpracovat
jeho	jeho	k3xOp3gMnSc1	jeho
osobní	osobní	k2eAgMnSc1d1	osobní
lékař	lékař	k1gMnSc1	lékař
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Antommarchi	Antommarch	k1gMnSc6	Antommarch
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
též	též	k9	též
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c6	o
zánětu	zánět	k1gInSc6	zánět
jater	játra	k1gNnPc2	játra
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
kvůli	kvůli	k7c3	kvůli
znění	znění	k1gNnSc3	znění
excísařovy	excísařův	k2eAgFnSc2d1	excísařův
druhé	druhý	k4xOgFnSc2	druhý
závěti	závěť	k1gFnSc2	závěť
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
obvinil	obvinit	k5eAaPmAgInS	obvinit
vrahy	vrah	k1gMnPc4	vrah
najaté	najatý	k2eAgMnPc4d1	najatý
Angličany	Angličan	k1gMnPc4	Angličan
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
hypotézy	hypotéza	k1gFnPc1	hypotéza
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
naznačovaly	naznačovat	k5eAaImAgFnP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
značný	značný	k2eAgInSc4d1	značný
zájem	zájem	k1gInSc4	zájem
odborné	odborný	k2eAgFnSc2d1	odborná
i	i	k8xC	i
laické	laický	k2eAgFnSc2d1	laická
veřejnosti	veřejnost	k1gFnSc2	veřejnost
činnost	činnost	k1gFnSc1	činnost
švédského	švédský	k2eAgMnSc2d1	švédský
stomatologa	stomatolog	k1gMnSc2	stomatolog
a	a	k8xC	a
amatérského	amatérský	k2eAgMnSc2d1	amatérský
toxikologa	toxikolog	k1gMnSc2	toxikolog
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Stena	Sten	k1gMnSc2	Sten
Forshufvuda	Forshufvud	k1gMnSc2	Forshufvud
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
při	při	k7c6	při
pročítání	pročítání	k1gNnSc6	pročítání
pamětí	paměť	k1gFnPc2	paměť
císařova	císařův	k2eAgMnSc2d1	císařův
komorníka	komorník	k1gMnSc2	komorník
Louise	Louis	k1gMnSc2	Louis
Marchanda	Marchanda	k1gFnSc1	Marchanda
(	(	kIx(	(
<g/>
Mémoires	Mémoires	k1gInSc1	Mémoires
de	de	k?	de
Marchand	Marchand	k1gInSc1	Marchand
<g/>
)	)	kIx)	)
dospěl	dochvít	k5eAaPmAgMnS	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
Napoleon	napoleon	k1gInSc1	napoleon
otráven	otrávit	k5eAaPmNgInS	otrávit
arzenikem	arzenik	k1gInSc7	arzenik
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
získat	získat	k5eAaPmF	získat
fyzický	fyzický	k2eAgInSc4d1	fyzický
důkaz	důkaz	k1gInSc4	důkaz
své	svůj	k3xOyFgFnSc2	svůj
teorie	teorie	k1gFnSc2	teorie
posléze	posléze	k6eAd1	posléze
zahájil	zahájit	k5eAaPmAgMnS	zahájit
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Forshufvud	Forshufvud	k1gInSc1	Forshufvud
výzkum	výzkum	k1gInSc1	výzkum
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
císařovy	císařův	k2eAgInPc1d1	císařův
vlasy	vlas	k1gInPc1	vlas
ustřižené	ustřižený	k2eAgInPc1d1	ustřižený
v	v	k7c4	v
den	den	k1gInSc4	den
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
skutečně	skutečně	k6eAd1	skutečně
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
nestandardně	standardně	k6eNd1	standardně
vysoké	vysoký	k2eAgNnSc4d1	vysoké
množství	množství	k1gNnSc4	množství
podezřívané	podezřívaný	k2eAgFnSc2d1	podezřívaná
chemikálie	chemikálie	k1gFnSc2	chemikálie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
hypotézu	hypotéza	k1gFnSc4	hypotéza
však	však	k9	však
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
patrně	patrně	k6eAd1	patrně
vyvrátili	vyvrátit	k5eAaPmAgMnP	vyvrátit
výzkumníci	výzkumník	k1gMnPc1	výzkumník
některých	některý	k3yIgFnPc2	některý
italských	italský	k2eAgFnPc2d1	italská
univerzit	univerzita	k1gFnPc2	univerzita
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nejmodernějšími	moderní	k2eAgFnPc7d3	nejmodernější
vědeckými	vědecký	k2eAgFnPc7d1	vědecká
metodami	metoda	k1gFnPc7	metoda
prokázali	prokázat	k5eAaPmAgMnP	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
dnešního	dnešní	k2eAgInSc2d1	dnešní
pohledu	pohled	k1gInSc2	pohled
extrémně	extrémně	k6eAd1	extrémně
vysoké	vysoký	k2eAgNnSc1d1	vysoké
množství	množství	k1gNnSc1	množství
arzeniku	arzenik	k1gInSc2	arzenik
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
tělech	tělo	k1gNnPc6	tělo
Napoleonových	Napoleonová	k1gFnPc2	Napoleonová
současníků	současník	k1gMnPc2	současník
běžným	běžný	k2eAgInSc7d1	běžný
jevem	jev	k1gInSc7	jev
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
otazníky	otazník	k1gInPc1	otazník
vzbudily	vzbudit	k5eAaPmAgInP	vzbudit
rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
uspořádání	uspořádání	k1gNnSc6	uspořádání
hrobu	hrob	k1gInSc2	hrob
a	a	k8xC	a
uložení	uložení	k1gNnSc2	uložení
těla	tělo	k1gNnSc2	tělo
při	při	k7c6	při
Napoleonově	Napoleonův	k2eAgInSc6d1	Napoleonův
pohřbu	pohřeb	k1gInSc6	pohřeb
a	a	k8xC	a
exhumaci	exhumace	k1gFnSc3	exhumace
nebo	nebo	k8xC	nebo
odlišnosti	odlišnost	k1gFnSc3	odlišnost
ve	v	k7c6	v
vzhledu	vzhled	k1gInSc6	vzhled
a	a	k8xC	a
rozměrech	rozměr	k1gInPc6	rozměr
jeho	jeho	k3xOp3gFnPc2	jeho
posmrtných	posmrtný	k2eAgFnPc2d1	posmrtná
masek	maska	k1gFnPc2	maska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnost	osobnost	k1gFnSc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
===	===	k?	===
</s>
</p>
<p>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
byl	být	k5eAaImAgMnS	být
menší	malý	k2eAgFnPc4d2	menší
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třiadvaceti	třiadvacet	k4xCc6	třiadvacet
letech	léto	k1gNnPc6	léto
měřil	měřit	k5eAaImAgInS	měřit
155	[number]	k4	155
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
statný	statný	k2eAgMnSc1d1	statný
v	v	k7c6	v
ramenou	rameno	k1gNnPc6	rameno
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc1	jeho
postava	postava	k1gFnSc1	postava
však	však	k9	však
zároveň	zároveň	k6eAd1	zároveň
budila	budit	k5eAaImAgFnS	budit
dojem	dojem	k1gInSc4	dojem
celkové	celkový	k2eAgFnSc2d1	celková
štíhlosti	štíhlost	k1gFnSc2	štíhlost
<g/>
.	.	kIx.	.
</s>
<s>
Pěknou	pěkný	k2eAgFnSc4d1	pěkná
románskou	románský	k2eAgFnSc4d1	románská
hlavu	hlava	k1gFnSc4	hlava
mu	on	k3xPp3gMnSc3	on
porůstaly	porůstat	k5eAaImAgFnP	porůstat
hnědé	hnědý	k2eAgFnPc1d1	hnědá
vlasy	vlas	k1gInPc4	vlas
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
olivovou	olivový	k2eAgFnSc4d1	olivová
pleť	pleť	k1gFnSc4	pleť
<g/>
,	,	kIx,	,
světlé	světlý	k2eAgNnSc4d1	světlé
modrozelené	modrozelený	k2eAgNnSc4d1	modrozelené
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
nad	nad	k7c7	nad
vystouplou	vystouplý	k2eAgFnSc7d1	vystouplá
bradou	brada	k1gFnSc7	brada
mírně	mírně	k6eAd1	mírně
našpulené	našpulený	k2eAgInPc4d1	našpulený
rty	ret	k1gInPc4	ret
<g/>
.	.	kIx.	.
</s>
<s>
Vyslanec	vyslanec	k1gMnSc1	vyslanec
toskánského	toskánský	k2eAgNnSc2d1	toskánské
velkovévodství	velkovévodství	k1gNnSc2	velkovévodství
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
době	doba	k1gFnSc6	doba
prvního	první	k4xOgNnSc2	první
italského	italský	k2eAgNnSc2d1	italské
tažení	tažení	k1gNnSc2	tažení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1796	[number]	k4	1796
popsal	popsat	k5eAaPmAgMnS	popsat
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1808	[number]	k4	1808
začal	začít	k5eAaPmAgMnS	začít
mít	mít	k5eAaImF	mít
císař	císař	k1gMnSc1	císař
Francouzů	Francouz	k1gMnPc2	Francouz
sklon	sklona	k1gFnPc2	sklona
k	k	k7c3	k
tělnatosti	tělnatost	k1gFnSc3	tělnatost
a	a	k8xC	a
svaly	sval	k1gInPc4	sval
ve	v	k7c6	v
tváři	tvář	k1gFnSc6	tvář
mu	on	k3xPp3gMnSc3	on
ztuhly	ztuhnout	k5eAaPmAgFnP	ztuhnout
do	do	k7c2	do
tvrdého	tvrdý	k2eAgInSc2d1	tvrdý
výrazu	výraz	k1gInSc2	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Dvorní	dvorní	k2eAgFnSc1d1	dvorní
dáma	dáma	k1gFnSc1	dáma
bývalé	bývalý	k2eAgFnSc2d1	bývalá
pruské	pruský	k2eAgFnSc2d1	pruská
královny	královna	k1gFnSc2	královna
Alžběty	Alžběta	k1gFnSc2	Alžběta
Kristýny	Kristýna	k1gFnSc2	Kristýna
hraběnka	hraběnka	k1gFnSc1	hraběnka
von	von	k1gInSc1	von
Voss	Voss	k1gInSc1	Voss
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
pamětech	paměť	k1gFnPc6	paměť
(	(	kIx(	(
<g/>
Neunundsechzig	Neunundsechzig	k1gInSc4	Neunundsechzig
Jahre	Jahr	k1gInSc5	Jahr
am	am	k?	am
Preußischen	Preußischen	k2eAgInSc4d1	Preußischen
Hofe	Hofe	k1gInSc4	Hofe
<g/>
)	)	kIx)	)
po	po	k7c6	po
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
Napoleonem	Napoleon	k1gMnSc7	Napoleon
zapsala	zapsat	k5eAaPmAgFnS	zapsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gInSc4	on
spatřila	spatřit	k5eAaPmAgFnS	spatřit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
s	s	k7c7	s
tlustou	tlustý	k2eAgFnSc7d1	tlustá
odulou	odulý	k2eAgFnSc7d1	odulá
tváří	tvář	k1gFnSc7	tvář
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
korpulentního	korpulentní	k2eAgNnSc2d1	korpulentní
<g/>
,	,	kIx,	,
nadto	nadto	k6eAd1	nadto
ještě	ještě	k9	ještě
malého	malý	k2eAgMnSc2d1	malý
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
postrádajícího	postrádající	k2eAgNnSc2d1	postrádající
eleganci	elegance	k1gFnSc3	elegance
<g/>
.	.	kIx.	.
</s>
<s>
Svýma	svůj	k3xOyFgNnPc7	svůj
velkýma	velký	k2eAgNnPc7d1	velké
očima	oko	k1gNnPc7	oko
loupá	loupat	k5eAaImIp3nS	loupat
zasmušile	zasmušile	k6eAd1	zasmušile
kolem	kolem	k6eAd1	kolem
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc1	jeho
rysy	rys	k1gInPc1	rys
mají	mít	k5eAaImIp3nP	mít
krutý	krutý	k2eAgInSc4d1	krutý
výraz	výraz	k1gInSc4	výraz
<g/>
,	,	kIx,	,
vypadá	vypadat	k5eAaImIp3nS	vypadat
jako	jako	k9	jako
ztělesnění	ztělesnění	k1gNnSc4	ztělesnění
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
ústa	ústa	k1gNnPc4	ústa
má	mít	k5eAaImIp3nS	mít
dobře	dobře	k6eAd1	dobře
tvarovaná	tvarovaný	k2eAgFnSc1d1	tvarovaná
a	a	k8xC	a
zuby	zub	k1gInPc7	zub
má	mít	k5eAaImIp3nS	mít
dobré	dobrý	k2eAgNnSc1d1	dobré
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
proměnlivost	proměnlivost	k1gFnSc4	proměnlivost
císařových	císařův	k2eAgInPc2d1	císařův
rysů	rys	k1gInPc2	rys
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgMnSc1d1	osobní
tajemník	tajemník	k1gMnSc1	tajemník
baron	baron	k1gMnSc1	baron
Méneval	Méneval	k1gMnSc1	Méneval
(	(	kIx(	(
<g/>
Napoléon	Napoléon	k1gMnSc1	Napoléon
et	et	k?	et
Marie-Louise	Marie-Louise	k1gFnSc1	Marie-Louise
<g/>
:	:	kIx,	:
Souvenirs	Souvenirs	k1gInSc1	Souvenirs
Historiques	Historiques	k1gMnSc1	Historiques
de	de	k?	de
Baron	baron	k1gMnSc1	baron
Méneval	Méneval	k1gFnSc3	Méneval
sv.	sv.	kA	sv.
3	[number]	k4	3
<g/>
)	)	kIx)	)
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
chvílích	chvíle	k1gFnPc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Napoleon	Napoleon	k1gMnSc1	Napoleon
rozčílil	rozčílit	k5eAaPmAgMnS	rozčílit
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
jeho	jeho	k3xOp3gInSc4	jeho
obličej	obličej	k1gInSc4	obličej
krutý	krutý	k2eAgInSc1d1	krutý
výraz	výraz	k1gInSc1	výraz
<g/>
.	.	kIx.	.
</s>
<s>
Mohutně	mohutně	k6eAd1	mohutně
prý	prý	k9	prý
vraštil	vraštit	k5eAaImAgMnS	vraštit
čelo	čelo	k1gNnSc4	čelo
i	i	k8xC	i
obočí	obočí	k1gNnSc4	obočí
a	a	k8xC	a
z	z	k7c2	z
očí	oko	k1gNnPc2	oko
mu	on	k3xPp3gMnSc3	on
sršely	sršet	k5eAaImAgInP	sršet
blesky	blesk	k1gInPc1	blesk
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
bouře	bouř	k1gFnSc2	bouř
nadmuly	nadmout	k5eAaPmAgFnP	nadmout
nosní	nosní	k2eAgFnPc1d1	nosní
dírky	dírka	k1gFnPc1	dírka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
však	však	k9	však
také	také	k9	také
dodává	dodávat	k5eAaImIp3nS	dodávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c2	za
normálního	normální	k2eAgInSc2d1	normální
stavu	stav	k1gInSc2	stav
byla	být	k5eAaImAgFnS	být
Napoleonova	Napoleonův	k2eAgFnSc1d1	Napoleonova
tvář	tvář	k1gFnSc1	tvář
klidná	klidný	k2eAgFnSc1d1	klidná
a	a	k8xC	a
vlídně	vlídně	k6eAd1	vlídně
vážná	vážný	k2eAgFnSc1d1	vážná
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
nejdobrotivějším	dobrotivý	k2eAgInSc7d3	dobrotivý
úsměvem	úsměv	k1gInSc7	úsměv
se	se	k3xPyFc4	se
rozzářila	rozzářit	k5eAaPmAgFnS	rozzářit
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
dobré	dobrý	k2eAgFnSc6d1	dobrá
náladě	nálada	k1gFnSc6	nálada
<g/>
,	,	kIx,	,
chtěl	chtít	k5eAaImAgMnS	chtít
být	být	k5eAaImF	být
příjemný	příjemný	k2eAgMnSc1d1	příjemný
lidem	člověk	k1gMnPc3	člověk
okolo	okolo	k6eAd1	okolo
nebo	nebo	k8xC	nebo
pookřál	pookřát	k5eAaPmAgMnS	pookřát
<g/>
.	.	kIx.	.
<g/>
Prefekt	prefekt	k1gMnSc1	prefekt
císařského	císařský	k2eAgInSc2d1	císařský
paláce	palác	k1gInSc2	palác
popsal	popsat	k5eAaPmAgMnS	popsat
Napoleona	Napoleon	k1gMnSc2	Napoleon
jako	jako	k8xC	jako
pětačtyřicetiletého	pětačtyřicetiletý	k2eAgMnSc2d1	pětačtyřicetiletý
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Další	další	k2eAgMnPc1d1	další
Napoleonovi	Napoleonův	k2eAgMnPc1d1	Napoleonův
současníci	současník	k1gMnPc1	současník
se	se	k3xPyFc4	se
podivovali	podivovat	k5eAaImAgMnP	podivovat
nad	nad	k7c7	nad
žlutou	žlutý	k2eAgFnSc7d1	žlutá
barvou	barva	k1gFnSc7	barva
jeho	jeho	k3xOp3gInSc2	jeho
obličeje	obličej	k1gInSc2	obličej
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yRgFnSc4	který
nejspíš	nejspíš	k9	nejspíš
stálo	stát	k5eAaImAgNnS	stát
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sklonem	sklon	k1gInSc7	sklon
k	k	k7c3	k
tělnatosti	tělnatost	k1gFnSc3	tělnatost
onemocnění	onemocnění	k1gNnPc2	onemocnění
jater	játra	k1gNnPc2	játra
<g/>
.	.	kIx.	.
</s>
<s>
Hrabě	Hrabě	k1gMnSc1	Hrabě
de	de	k?	de
Las	laso	k1gNnPc2	laso
Cases	Cases	k1gInSc1	Cases
dále	daleko	k6eAd2	daleko
konstatoval	konstatovat	k5eAaBmAgInS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
císař	císař	k1gMnSc1	císař
měl	mít	k5eAaImAgMnS	mít
i	i	k9	i
přes	přes	k7c4	přes
silné	silný	k2eAgInPc4d1	silný
údy	úd	k1gInPc4	úd
měkké	měkký	k2eAgNnSc4d1	měkké
svalstvo	svalstvo	k1gNnSc4	svalstvo
a	a	k8xC	a
i	i	k9	i
přes	přes	k7c4	přes
značně	značně	k6eAd1	značně
rozložitou	rozložitý	k2eAgFnSc4d1	rozložitá
hruď	hruď	k1gFnSc4	hruď
byl	být	k5eAaImAgInS	být
stále	stále	k6eAd1	stále
prochladlý	prochladlý	k2eAgInSc1d1	prochladlý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pitevní	pitevní	k2eAgFnSc6d1	pitevní
zprávě	zpráva	k1gFnSc6	zpráva
ze	z	k7c2	z
Svaté	svatý	k2eAgFnSc2d1	svatá
Heleny	Helena	k1gFnSc2	Helena
britský	britský	k2eAgMnSc1d1	britský
chirurg	chirurg	k1gMnSc1	chirurg
Walter	Walter	k1gMnSc1	Walter
Henry	Henry	k1gMnSc1	Henry
(	(	kIx(	(
<g/>
Events	Events	k1gInSc1	Events
of	of	k?	of
a	a	k8xC	a
military	militara	k1gFnSc2	militara
life	lif	k1gInSc2	lif
<g/>
)	)	kIx)	)
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Napoleonova	Napoleonův	k2eAgFnSc1d1	Napoleonova
pokožka	pokožka	k1gFnSc1	pokožka
byla	být	k5eAaImAgFnS	být
velmi	velmi	k6eAd1	velmi
bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
jemná	jemný	k2eAgFnSc1d1	jemná
<g/>
,	,	kIx,	,
po	po	k7c6	po
těle	tělo	k1gNnSc6	tělo
měl	mít	k5eAaImAgMnS	mít
jen	jen	k9	jen
málo	málo	k4c4	málo
chloupků	chloupek	k1gInPc2	chloupek
a	a	k8xC	a
vlasy	vlas	k1gInPc4	vlas
měl	mít	k5eAaImAgMnS	mít
jemné	jemný	k2eAgNnSc4d1	jemné
a	a	k8xC	a
hedvábné	hedvábný	k2eAgNnSc4d1	hedvábné
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
trup	trup	k1gInSc1	trup
prý	prý	k9	prý
byl	být	k5eAaImAgInS	být
štíhlý	štíhlý	k2eAgInSc1d1	štíhlý
a	a	k8xC	a
zženštilý	zženštilý	k2eAgInSc1d1	zženštilý
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
stydká	stydký	k2eAgFnSc1d1	stydká
kost	kost	k1gFnSc1	kost
velmi	velmi	k6eAd1	velmi
připomínala	připomínat	k5eAaImAgFnS	připomínat
Venušin	Venušin	k2eAgInSc4d1	Venušin
pahorek	pahorek	k1gInSc4	pahorek
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Svaly	sval	k1gInPc1	sval
na	na	k7c6	na
hrudníku	hrudník	k1gInSc6	hrudník
byly	být	k5eAaImAgInP	být
malé	malý	k2eAgInPc4d1	malý
<g/>
,	,	kIx,	,
ramena	rameno	k1gNnPc4	rameno
úzká	úzký	k2eAgNnPc4d1	úzké
a	a	k8xC	a
boky	boka	k1gFnSc2	boka
široké	široký	k2eAgFnSc2d1	široká
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgFnPc1d1	osobní
partie	partie	k1gFnPc1	partie
<g/>
,	,	kIx,	,
varlata	varle	k1gNnPc1	varle
a	a	k8xC	a
penis	penis	k1gInSc1	penis
<g/>
,	,	kIx,	,
vypadaly	vypadat	k5eAaPmAgFnP	vypadat
malé	malý	k2eAgFnPc1d1	malá
<g/>
,	,	kIx,	,
takřka	takřka	k6eAd1	takřka
chlapecké	chlapecký	k2eAgInPc1d1	chlapecký
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
duševních	duševní	k2eAgFnPc2d1	duševní
schopností	schopnost	k1gFnPc2	schopnost
si	se	k3xPyFc3	se
Napoleon	Napoleon	k1gMnSc1	Napoleon
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
nejvíce	hodně	k6eAd3	hodně
cenil	cenit	k5eAaImAgMnS	cenit
železné	železný	k2eAgFnSc2d1	železná
vůle	vůle	k1gFnSc2	vůle
<g/>
,	,	kIx,	,
pevnosti	pevnost	k1gFnPc1	pevnost
ducha	duch	k1gMnSc2	duch
a	a	k8xC	a
zvláštní	zvláštní	k2eAgFnSc2d1	zvláštní
statečnosti	statečnost	k1gFnSc2	statečnost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
spočívala	spočívat	k5eAaImAgFnS	spočívat
ve	v	k7c6	v
schopnosti	schopnost	k1gFnSc6	schopnost
převzít	převzít	k5eAaPmF	převzít
nejstrašnější	strašný	k2eAgFnSc4d3	nejstrašnější
a	a	k8xC	a
nejtěžší	těžký	k2eAgFnSc4d3	nejtěžší
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
za	za	k7c4	za
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
výbornou	výborný	k2eAgFnSc4d1	výborná
paměť	paměť	k1gFnSc4	paměť
<g/>
;	;	kIx,	;
sám	sám	k3xTgMnSc1	sám
ji	on	k3xPp3gFnSc4	on
označoval	označovat	k5eAaImAgInS	označovat
za	za	k7c4	za
pozoruhodnou	pozoruhodný	k2eAgFnSc4d1	pozoruhodná
vlastnost	vlastnost	k1gFnSc4	vlastnost
a	a	k8xC	a
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Najednou	najednou	k6eAd1	najednou
se	se	k3xPyFc4	se
prý	prý	k9	prý
dokázal	dokázat	k5eAaPmAgInS	dokázat
zaměstnávat	zaměstnávat	k5eAaImF	zaměstnávat
i	i	k9	i
třemi	tři	k4xCgNnPc7	tři
naprosto	naprosto	k6eAd1	naprosto
odlišnými	odlišný	k2eAgFnPc7d1	odlišná
záležitostmi	záležitost	k1gFnPc7	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
s	s	k7c7	s
to	ten	k3xDgNnSc4	ten
diktovat	diktovat	k5eAaImF	diktovat
dvěma	dva	k4xCgMnPc3	dva
sekretářům	sekretář	k1gMnPc3	sekretář
o	o	k7c4	o
dvou	dva	k4xCgFnPc6	dva
různých	různý	k2eAgFnPc6d1	různá
věcech	věc	k1gFnPc6	věc
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
tím	ten	k3xDgNnSc7	ten
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
logická	logický	k2eAgFnSc1d1	logická
jasnost	jasnost	k1gFnSc1	jasnost
jeho	jeho	k3xOp3gFnPc2	jeho
myšlenek	myšlenka	k1gFnPc2	myšlenka
nebo	nebo	k8xC	nebo
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
,	,	kIx,	,
a	a	k8xC	a
číst	číst	k5eAaImF	číst
o	o	k7c6	o
věci	věc	k1gFnSc6	věc
třetí	třetí	k4xOgFnSc6	třetí
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
pracoval	pracovat	k5eAaImAgMnS	pracovat
dlouho	dlouho	k6eAd1	dlouho
do	do	k7c2	do
noci	noc	k1gFnSc2	noc
a	a	k8xC	a
spal	spát	k5eAaImAgMnS	spát
v	v	k7c6	v
přerušovaných	přerušovaný	k2eAgInPc6d1	přerušovaný
intervalech	interval	k1gInPc6	interval
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
své	svůj	k3xOyFgFnSc2	svůj
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
vojenských	vojenský	k2eAgNnPc2d1	vojenské
tažení	tažení	k1gNnSc2	tažení
byl	být	k5eAaImAgInS	být
mnohdy	mnohdy	k6eAd1	mnohdy
okolnostmi	okolnost	k1gFnPc7	okolnost
přinucen	přinutit	k5eAaPmNgMnS	přinutit
budit	budit	k5eAaImF	budit
se	se	k3xPyFc4	se
i	i	k9	i
několikrát	několikrát	k6eAd1	několikrát
za	za	k7c4	za
noc	noc	k1gFnSc4	noc
a	a	k8xC	a
přesto	přesto	k8xC	přesto
okamžitě	okamžitě	k6eAd1	okamžitě
vstal	vstát	k5eAaPmAgMnS	vstát
a	a	k8xC	a
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
očí	oko	k1gNnPc2	oko
se	se	k3xPyFc4	se
nedalo	dát	k5eNaPmAgNnS	dát
uhodnout	uhodnout	k5eAaPmF	uhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k6eAd1	právě
spal	spát	k5eAaImAgMnS	spát
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
byl	být	k5eAaImAgInS	být
okamžitě	okamžitě	k6eAd1	okamžitě
schopen	schopen	k2eAgMnSc1d1	schopen
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
ve	v	k7c4	v
kteroukoliv	kterýkoliv	k3yIgFnSc4	kterýkoliv
jinou	jiný	k2eAgFnSc4d1	jiná
dobu	doba	k1gFnSc4	doba
přes	přes	k7c4	přes
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
Napoleonova	Napoleonův	k2eAgNnSc2d1	Napoleonovo
blízkého	blízký	k2eAgNnSc2d1	blízké
okolí	okolí	k1gNnSc2	okolí
se	se	k3xPyFc4	se
shodují	shodovat	k5eAaImIp3nP	shodovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
měl	mít	k5eAaImAgMnS	mít
dvě	dva	k4xCgFnPc4	dva
tváře	tvář	k1gFnPc4	tvář
<g/>
,	,	kIx,	,
soukromou	soukromý	k2eAgFnSc4d1	soukromá
a	a	k8xC	a
politickou	politický	k2eAgFnSc4d1	politická
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
soukromí	soukromí	k1gNnSc2	soukromí
týká	týkat	k5eAaImIp3nS	týkat
<g/>
,	,	kIx,	,
dokázal	dokázat	k5eAaPmAgMnS	dokázat
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
upnout	upnout	k5eAaPmF	upnout
na	na	k7c4	na
osoby	osoba	k1gFnPc4	osoba
kolem	kolem	k7c2	kolem
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
si	se	k3xPyFc3	se
na	na	k7c4	na
někoho	někdo	k3yInSc4	někdo
zvykl	zvyknout	k5eAaPmAgMnS	zvyknout
<g/>
,	,	kIx,	,
nepřišlo	přijít	k5eNaPmAgNnS	přijít
mu	on	k3xPp3gMnSc3	on
na	na	k7c4	na
mysl	mysl	k1gFnSc4	mysl
se	se	k3xPyFc4	se
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
odloučit	odloučit	k5eAaPmF	odloučit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
viděl	vidět	k5eAaImAgMnS	vidět
jeho	jeho	k3xOp3gFnPc4	jeho
chyby	chyba	k1gFnPc4	chyba
<g/>
,	,	kIx,	,
odsuzoval	odsuzovat	k5eAaImAgMnS	odsuzovat
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
nebyl	být	k5eNaImAgMnS	být
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
volbou	volba	k1gFnSc7	volba
spokojen	spokojit	k5eAaPmNgMnS	spokojit
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
nebyl	být	k5eNaImAgMnS	být
ani	ani	k8xC	ani
nenávistný	nenávistný	k2eAgMnSc1d1	nenávistný
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
mstivý	mstivý	k2eAgInSc1d1	mstivý
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
se	se	k3xPyFc4	se
rozčílit	rozčílit	k5eAaPmF	rozčílit
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgInSc4	svůj
korsický	korsický	k2eAgInSc4d1	korsický
temperament	temperament	k1gInSc4	temperament
a	a	k8xC	a
zlost	zlost	k1gFnSc4	zlost
dát	dát	k5eAaPmF	dát
najevo	najevo	k6eAd1	najevo
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
vše	všechen	k3xTgNnSc1	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Církevní	církevní	k2eAgMnSc1d1	církevní
diplomat	diplomat	k1gMnSc1	diplomat
de	de	k?	de
Pradt	Pradt	k1gMnSc1	Pradt
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Hřímalo	hřímat	k5eAaImAgNnS	hřímat
sice	sice	k8xC	sice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neuhodilo	uhodit	k5eNaPmAgNnS	uhodit
<g/>
;	;	kIx,	;
bouřné	bouřný	k2eAgNnSc4d1	bouřný
mračno	mračno	k1gNnSc4	mračno
se	se	k3xPyFc4	se
rozptýlilo	rozptýlit	k5eAaPmAgNnS	rozptýlit
v	v	k7c6	v
krupobití	krupobití	k1gNnSc6	krupobití
<g/>
,	,	kIx,	,
v	v	k7c6	v
lijáku	liják	k1gInSc6	liják
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
již	již	k6eAd1	již
za	za	k7c4	za
okamžik	okamžik	k1gInSc4	okamžik
nepřikládal	přikládat	k5eNaImAgMnS	přikládat
nejmenšího	malý	k2eAgInSc2d3	nejmenší
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Svému	svůj	k3xOyFgNnSc3	svůj
okolí	okolí	k1gNnSc3	okolí
se	se	k3xPyFc4	se
jevil	jevit	k5eAaImAgInS	jevit
mírný	mírný	k2eAgInSc1d1	mírný
a	a	k8xC	a
značně	značně	k6eAd1	značně
shovívavý	shovívavý	k2eAgMnSc1d1	shovívavý
k	k	k7c3	k
lidské	lidský	k2eAgFnSc3d1	lidská
slabosti	slabost	k1gFnSc3	slabost
<g/>
.	.	kIx.	.
</s>
<s>
Osoby	osoba	k1gFnPc1	osoba
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
chtěl	chtít	k5eAaImAgMnS	chtít
být	být	k5eAaImF	být
příjemný	příjemný	k2eAgMnSc1d1	příjemný
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
rád	rád	k6eAd1	rád
dobíral	dobírat	k5eAaImAgMnS	dobírat
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
osobitý	osobitý	k2eAgInSc4d1	osobitý
humor	humor	k1gInSc4	humor
zpestřoval	zpestřovat	k5eAaImAgInS	zpestřovat
hlučným	hlučný	k2eAgMnSc7d1	hlučný
a	a	k8xC	a
žertovným	žertovný	k2eAgInSc7d1	žertovný
smíchem	smích	k1gInSc7	smích
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
lichotkou	lichotka	k1gFnSc7	lichotka
z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
strany	strana	k1gFnSc2	strana
byly	být	k5eAaImAgFnP	být
různé	různý	k2eAgFnPc1d1	různá
fyzické	fyzický	k2eAgFnPc1d1	fyzická
důvěrnosti	důvěrnost	k1gFnPc1	důvěrnost
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
používal	používat	k5eAaImAgMnS	používat
pochvalné	pochvalný	k2eAgNnSc4d1	pochvalné
vytahání	vytahání	k1gNnSc4	vytahání
za	za	k7c4	za
uši	ucho	k1gNnPc4	ucho
nebo	nebo	k8xC	nebo
poplácávání	poplácávání	k1gNnPc4	poplácávání
po	po	k7c6	po
tváři	tvář	k1gFnSc6	tvář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
intimních	intimní	k2eAgInPc6d1	intimní
vztazích	vztah	k1gInPc6	vztah
se	se	k3xPyFc4	se
vyznačoval	vyznačovat	k5eAaImAgInS	vyznačovat
důvěrností	důvěrnost	k1gFnSc7	důvěrnost
<g/>
,	,	kIx,	,
laskavostí	laskavost	k1gFnSc7	laskavost
i	i	k8xC	i
něhou	něha	k1gFnSc7	něha
<g/>
.	.	kIx.	.
</s>
<s>
Násilnost	násilnost	k1gFnSc4	násilnost
mu	on	k3xPp3gMnSc3	on
prý	prý	k9	prý
nebyla	být	k5eNaImAgFnS	být
vrozená	vrozený	k2eAgFnSc1d1	vrozená
a	a	k8xC	a
pokud	pokud	k8xS	pokud
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
usiloval	usilovat	k5eAaImAgMnS	usilovat
<g/>
,	,	kIx,	,
nikdo	nikdo	k3yNnSc1	nikdo
se	se	k3xPyFc4	se
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
ovládat	ovládat	k5eAaImF	ovládat
jako	jako	k9	jako
on	on	k3xPp3gMnSc1	on
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
mluva	mluva	k1gFnSc1	mluva
byla	být	k5eAaImAgFnS	být
suchá	suchý	k2eAgFnSc1d1	suchá
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ne	ne	k9	ne
nepřístojná	přístojný	k2eNgFnSc1d1	nepřístojná
ani	ani	k8xC	ani
pokořující	pokořující	k2eAgFnSc1d1	pokořující
<g/>
.	.	kIx.	.
</s>
<s>
Výraznějším	výrazný	k2eAgInSc7d2	výraznější
způsobem	způsob	k1gInSc7	způsob
se	se	k3xPyFc4	se
Napoleonova	Napoleonův	k2eAgFnSc1d1	Napoleonova
osobnost	osobnost	k1gFnSc1	osobnost
změnila	změnit	k5eAaPmAgFnS	změnit
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
první	první	k4xOgFnSc6	první
abdikaci	abdikace	k1gFnSc6	abdikace
<g/>
.	.	kIx.	.
</s>
<s>
Historik	historik	k1gMnSc1	historik
Albert	Albert	k1gMnSc1	Albert
Z.	Z.	kA	Z.
Manfred	Manfred	k1gMnSc1	Manfred
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
si	se	k3xPyFc3	se
při	při	k7c6	při
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
poli	pole	k1gNnSc6	pole
uvykl	uvyknout	k5eAaPmAgInS	uvyknout
snášet	snášet	k5eAaImF	snášet
tvrdé	tvrdý	k2eAgFnPc4d1	tvrdá
životní	životní	k2eAgFnPc4d1	životní
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
v	v	k7c6	v
císařském	císařský	k2eAgInSc6d1	císařský
paláci	palác	k1gInSc6	palác
si	se	k3xPyFc3	se
Napoleon	napoleon	k1gInSc1	napoleon
liboval	libovat	k5eAaImAgInS	libovat
ve	v	k7c6	v
fyzickém	fyzický	k2eAgInSc6d1	fyzický
komfortu	komfort	k1gInSc6	komfort
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
pracovní	pracovní	k2eAgInSc4d1	pracovní
den	den	k1gInSc4	den
začínal	začínat	k5eAaImAgMnS	začínat
nalačno	nalačno	k6eAd1	nalačno
poradami	porada	k1gFnPc7	porada
s	s	k7c7	s
nejbližšími	blízký	k2eAgFnPc7d3	nejbližší
osobami	osoba	k1gFnPc7	osoba
nebo	nebo	k8xC	nebo
pročítáním	pročítání	k1gNnSc7	pročítání
dokumentů	dokument	k1gInPc2	dokument
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgInP	být
připraveny	připravit	k5eAaPmNgInP	připravit
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Snídal	snídat	k5eAaImAgMnS	snídat
obvykle	obvykle	k6eAd1	obvykle
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
však	však	k9	však
ne	ne	k9	ne
déle	dlouho	k6eAd2	dlouho
než	než	k8xS	než
deset	deset	k4xCc4	deset
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
pracoven	pracovna	k1gFnPc2	pracovna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgInS	věnovat
správě	správa	k1gFnSc3	správa
území	území	k1gNnSc2	území
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gFnSc7	jeho
nadvládou	nadvláda	k1gFnSc7	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
obědu	oběd	k1gInSc3	oběd
konzumoval	konzumovat	k5eAaBmAgMnS	konzumovat
prostou	prostý	k2eAgFnSc4d1	prostá
stravu	strava	k1gFnSc4	strava
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zapíjel	zapíjet	k5eAaImAgMnS	zapíjet
kávou	káva	k1gFnSc7	káva
a	a	k8xC	a
ředěným	ředěný	k2eAgNnSc7d1	ředěné
burgundským	burgundský	k2eAgNnSc7d1	burgundské
vínem	víno	k1gNnSc7	víno
<g/>
.	.	kIx.	.
</s>
<s>
Večeřel	večeřet	k5eAaImAgMnS	večeřet
obvykle	obvykle	k6eAd1	obvykle
kolem	kolem	k6eAd1	kolem
šesté	šestý	k4xOgNnSc1	šestý
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
císařovny	císařovna	k1gFnSc2	císařovna
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
pak	pak	k6eAd1	pak
strávil	strávit	k5eAaPmAgMnS	strávit
společnou	společný	k2eAgFnSc4d1	společná
hodinku	hodinka	k1gFnSc4	hodinka
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
povolal	povolat	k5eAaPmAgMnS	povolat
svého	svůj	k3xOyFgMnSc4	svůj
knihovníka	knihovník	k1gMnSc4	knihovník
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
mu	on	k3xPp3gMnSc3	on
přinesl	přinést	k5eAaPmAgInS	přinést
výběr	výběr	k1gInSc1	výběr
nejnovějších	nový	k2eAgFnPc2d3	nejnovější
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
vydal	vydat	k5eAaPmAgMnS	vydat
instrukce	instrukce	k1gFnPc4	instrukce
na	na	k7c4	na
ráno	ráno	k1gNnSc4	ráno
a	a	k8xC	a
uléhal	uléhat	k5eAaImAgMnS	uléhat
do	do	k7c2	do
postele	postel	k1gFnSc2	postel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
jej	on	k3xPp3gInSc4	on
občas	občas	k6eAd1	občas
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
Josefína	Josefína	k1gFnSc1	Josefína
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gNnSc3	on
z	z	k7c2	z
nohou	noha	k1gFnPc2	noha
jeho	jeho	k3xOp3gFnSc2	jeho
postele	postel	k1gFnSc2	postel
předčítala	předčítat	k5eAaImAgFnS	předčítat
<g/>
.	.	kIx.	.
<g/>
Jak	jak	k8xC	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
naznačeno	naznačit	k5eAaPmNgNnS	naznačit
<g/>
,	,	kIx,	,
jinou	jiný	k2eAgFnSc7d1	jiná
masku	maska	k1gFnSc4	maska
si	se	k3xPyFc3	se
přisvojoval	přisvojovat	k5eAaImAgInS	přisvojovat
před	před	k7c7	před
vnějším	vnější	k2eAgInSc7d1	vnější
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
<s>
Napoleonův	Napoleonův	k2eAgMnSc1d1	Napoleonův
životopisec	životopisec	k1gMnSc1	životopisec
a	a	k8xC	a
znalec	znalec	k1gMnSc1	znalec
doby	doba	k1gFnSc2	doba
Jevgenij	Jevgenij	k1gMnSc1	Jevgenij
Tarle	Tarle	k1gFnSc1	Tarle
stručně	stručně	k6eAd1	stručně
konstatuje	konstatovat	k5eAaBmIp3nS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Napoleonovými	Napoleonův	k2eAgFnPc7d1	Napoleonova
hlavními	hlavní	k2eAgFnPc7d1	hlavní
vášněmi	vášeň	k1gFnPc7	vášeň
byly	být	k5eAaImAgFnP	být
sláva	sláva	k1gFnSc1	sláva
a	a	k8xC	a
zejména	zejména	k9	zejména
moc	moc	k6eAd1	moc
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
mnohými	mnohý	k2eAgMnPc7d1	mnohý
dalšími	další	k2eAgMnPc7d1	další
historiky	historik	k1gMnPc7	historik
se	se	k3xPyFc4	se
shoduje	shodovat	k5eAaImIp3nS	shodovat
v	v	k7c6	v
názoru	názor	k1gInSc6	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
dosahování	dosahování	k1gNnSc6	dosahování
svých	svůj	k3xOyFgInPc2	svůj
cílů	cíl	k1gInPc2	cíl
byl	být	k5eAaImAgMnS	být
naprosto	naprosto	k6eAd1	naprosto
lhostejný	lhostejný	k2eAgMnSc1d1	lhostejný
k	k	k7c3	k
lidem	člověk	k1gMnPc3	člověk
a	a	k8xC	a
viděl	vidět	k5eAaImAgMnS	vidět
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
jen	jen	k9	jen
prostředky	prostředek	k1gInPc1	prostředek
a	a	k8xC	a
nástroje	nástroj	k1gInPc1	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Zapomínal	zapomínat	k5eAaImAgMnS	zapomínat
na	na	k7c4	na
ohledy	ohled	k1gInPc4	ohled
vůči	vůči	k7c3	vůči
svým	svůj	k3xOyFgMnPc3	svůj
spolupracovníkům	spolupracovník	k1gMnPc3	spolupracovník
i	i	k8xC	i
vojákům	voják	k1gMnPc3	voják
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
svým	svůj	k3xOyFgMnPc3	svůj
podřízeným	podřízený	k1gMnPc3	podřízený
nikdy	nikdy	k6eAd1	nikdy
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
zcela	zcela	k6eAd1	zcela
nedůvěřovat	důvěřovat	k5eNaImF	důvěřovat
a	a	k8xC	a
ani	ani	k9	ani
jim	on	k3xPp3gMnPc3	on
neuměl	umět	k5eNaImAgMnS	umět
svěřit	svěřit	k5eAaPmF	svěřit
plnou	plný	k2eAgFnSc4d1	plná
zodpovědnost	zodpovědnost	k1gFnSc4	zodpovědnost
a	a	k8xC	a
soustavně	soustavně	k6eAd1	soustavně
kontroloval	kontrolovat	k5eAaImAgMnS	kontrolovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gInPc1	jeho
příkazy	příkaz	k1gInPc1	příkaz
plněny	plnit	k5eAaImNgInP	plnit
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
silný	silný	k2eAgInSc4d1	silný
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
rodinu	rodina	k1gFnSc4	rodina
mnohdy	mnohdy	k6eAd1	mnohdy
nebral	brát	k5eNaImAgMnS	brát
ohledy	ohled	k1gInPc7	ohled
ani	ani	k8xC	ani
na	na	k7c4	na
city	cit	k1gInPc4	cit
svých	svůj	k3xOyFgMnPc2	svůj
příbuzných	příbuzný	k1gMnPc2	příbuzný
a	a	k8xC	a
očekával	očekávat	k5eAaImAgMnS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
podřídí	podřídit	k5eAaPmIp3nS	podřídit
jeho	jeho	k3xOp3gFnSc4	jeho
vůli	vůle	k1gFnSc4	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
nadřazenost	nadřazenost	k1gFnSc4	nadřazenost
si	se	k3xPyFc3	se
často	často	k6eAd1	často
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
zuřivostí	zuřivost	k1gFnSc7	zuřivost
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
dalších	další	k2eAgInPc2d1	další
přihlížejících	přihlížející	k2eAgInPc2d1	přihlížející
směřoval	směřovat	k5eAaImAgInS	směřovat
proti	proti	k7c3	proti
některé	některý	k3yIgFnSc3	některý
přítomné	přítomný	k2eAgFnSc3d1	přítomná
osobě	osoba	k1gFnSc3	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
projevovat	projevovat	k5eAaImF	projevovat
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
vulgární	vulgární	k2eAgInSc1d1	vulgární
a	a	k8xC	a
hrubý	hrubý	k2eAgInSc1d1	hrubý
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc4	ten
okolnosti	okolnost	k1gFnPc1	okolnost
vyžadovaly	vyžadovat	k5eAaImAgFnP	vyžadovat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
schopen	schopen	k2eAgMnSc1d1	schopen
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
kterýkoliv	kterýkoliv	k3yIgMnSc1	kterýkoliv
řadový	řadový	k2eAgMnSc1d1	řadový
voják	voják	k1gMnSc1	voják
vystavit	vystavit	k5eAaPmF	vystavit
svou	svůj	k3xOyFgFnSc4	svůj
osobu	osoba	k1gFnSc4	osoba
nepřátelským	přátelský	k2eNgFnPc3d1	nepřátelská
střelám	střela	k1gFnPc3	střela
<g/>
.	.	kIx.	.
</s>
<s>
Učinil	učinit	k5eAaPmAgInS	učinit
tak	tak	k9	tak
například	například	k6eAd1	například
u	u	k7c2	u
Toulonu	Toulon	k1gInSc2	Toulon
<g/>
,	,	kIx,	,
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
na	na	k7c6	na
mostu	most	k1gInSc2	most
u	u	k7c2	u
Arcole	Arcole	k1gFnSc2	Arcole
nebo	nebo	k8xC	nebo
na	na	k7c6	na
městském	městský	k2eAgInSc6d1	městský
hřbitově	hřbitov	k1gInSc6	hřbitov
u	u	k7c2	u
Pruského	pruský	k2eAgNnSc2d1	pruské
Jílového	Jílové	k1gNnSc2	Jílové
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
omývali	omývat	k5eAaImAgMnP	omývat
Napoleonovu	Napoleonův	k2eAgFnSc4d1	Napoleonova
mrtvolu	mrtvola	k1gFnSc4	mrtvola
<g/>
,	,	kIx,	,
nalezli	naleznout	k5eAaPmAgMnP	naleznout
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
stopy	stopa	k1gFnPc4	stopa
neznámých	známý	k2eNgFnPc2d1	neznámá
ran	rána	k1gFnPc2	rána
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
se	se	k3xPyFc4	se
vědělo	vědět	k5eAaImAgNnS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
zraněn	zranit	k5eAaPmNgMnS	zranit
u	u	k7c2	u
Toulonu	Toulon	k1gInSc2	Toulon
<g/>
,	,	kIx,	,
Řezna	Řezno	k1gNnSc2	Řezno
a	a	k8xC	a
Drážďan	Drážďany	k1gInPc2	Drážďany
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
tyto	tento	k3xDgFnPc1	tento
nově	nově	k6eAd1	nově
odhalené	odhalený	k2eAgFnPc1d1	odhalená
jizvy	jizva	k1gFnPc1	jizva
svědčily	svědčit	k5eAaImAgFnP	svědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
neváhal	váhat	k5eNaImAgInS	váhat
skrýt	skrýt	k5eAaPmF	skrýt
svá	svůj	k3xOyFgNnPc4	svůj
zranění	zranění	k1gNnPc4	zranění
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vojáky	voják	k1gMnPc4	voják
neuvedl	uvést	k5eNaPmAgMnS	uvést
do	do	k7c2	do
zmatku	zmatek	k1gInSc2	zmatek
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xC	jako
panovník	panovník	k1gMnSc1	panovník
je	být	k5eAaImIp3nS	být
Napoleon	napoleon	k1gInSc1	napoleon
často	často	k6eAd1	často
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c2	za
diktátora	diktátor	k1gMnSc2	diktátor
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
typické	typický	k2eAgNnSc4d1	typické
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
bezohledně	bezohledně	k6eAd1	bezohledně
skoncovat	skoncovat	k5eAaPmF	skoncovat
s	s	k7c7	s
veškerou	veškerý	k3xTgFnSc7	veškerý
opozicí	opozice	k1gFnSc7	opozice
i	i	k8xC	i
kritikou	kritika	k1gFnSc7	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Osoby	osoba	k1gFnPc4	osoba
podezřelé	podezřelý	k2eAgFnPc4d1	podezřelá
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
skutků	skutek	k1gInPc2	skutek
nechával	nechávat	k5eAaImAgMnS	nechávat
bez	bez	k7c2	bez
procesů	proces	k1gInPc2	proces
zavírat	zavírat	k5eAaImF	zavírat
do	do	k7c2	do
věznic	věznice	k1gFnPc2	věznice
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
nutil	nutit	k5eAaImAgMnS	nutit
obývat	obývat	k5eAaImF	obývat
vyhrazená	vyhrazený	k2eAgNnPc1d1	vyhrazené
místa	místo	k1gNnPc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
pod	pod	k7c7	pod
neustálým	neustálý	k2eAgInSc7d1	neustálý
dozorem	dozor	k1gInSc7	dozor
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
cenzury	cenzura	k1gFnSc2	cenzura
<g/>
,	,	kIx,	,
četnictva	četnictvo	k1gNnSc2	četnictvo
a	a	k8xC	a
informátorů	informátor	k1gMnPc2	informátor
snažil	snažit	k5eAaImAgMnS	snažit
držet	držet	k5eAaImF	držet
i	i	k9	i
veřejné	veřejný	k2eAgNnSc4d1	veřejné
mínění	mínění	k1gNnSc4	mínění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ženy	žena	k1gFnPc1	žena
a	a	k8xC	a
rodina	rodina	k1gFnSc1	rodina
===	===	k?	===
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
naplňování	naplňování	k1gNnSc6	naplňování
osobních	osobní	k2eAgFnPc2d1	osobní
ambicí	ambice	k1gFnPc2	ambice
hrály	hrát	k5eAaImAgInP	hrát
v	v	k7c6	v
Napoleonově	Napoleonův	k2eAgInSc6d1	Napoleonův
životě	život	k1gInSc6	život
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
i	i	k8xC	i
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
první	první	k4xOgFnSc7	první
dětskou	dětský	k2eAgFnSc7d1	dětská
láskou	láska	k1gFnSc7	láska
prý	prý	k9	prý
byla	být	k5eAaImAgFnS	být
dívenka	dívenka	k1gFnSc1	dívenka
Giacominetta	Giacominetta	k1gFnSc1	Giacominetta
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
rodném	rodný	k2eAgNnSc6d1	rodné
Ajacciu	Ajaccium	k1gNnSc6	Ajaccium
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
sexuální	sexuální	k2eAgFnSc4d1	sexuální
zkušenost	zkušenost	k1gFnSc4	zkušenost
zřejmě	zřejmě	k6eAd1	zřejmě
zažil	zažít	k5eAaPmAgMnS	zažít
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1787	[number]	k4	1787
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
s	s	k7c7	s
prostitutkou	prostitutka	k1gFnSc7	prostitutka
vykonávající	vykonávající	k2eAgFnSc1d1	vykonávající
své	svůj	k3xOyFgNnSc4	svůj
řemeslo	řemeslo	k1gNnSc4	řemeslo
poblíž	poblíž	k6eAd1	poblíž
Palais-Royal	Palais-Royal	k1gMnSc1	Palais-Royal
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
větší	veliký	k2eAgFnSc4d2	veliký
milostnou	milostný	k2eAgFnSc4d1	milostná
avantýru	avantýra	k1gFnSc4	avantýra
prožil	prožít	k5eAaPmAgMnS	prožít
s	s	k7c7	s
madame	madame	k1gFnSc7	madame
Naudinovou	Naudinový	k2eAgFnSc7d1	Naudinový
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
Auxonne	Auxonn	k1gInSc5	Auxonn
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
ženou	žena	k1gFnSc7	žena
v	v	k7c4	v
pořadí	pořadí	k1gNnSc4	pořadí
byla	být	k5eAaImAgFnS	být
Louise	Louis	k1gMnSc4	Louis
Turreau	Turreaa	k1gMnSc4	Turreaa
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
v	v	k7c6	v
Nizze	Nizza	k1gFnSc6	Nizza
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přijela	přijet	k5eAaPmAgFnS	přijet
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
<g/>
.	.	kIx.	.
</s>
<s>
Skutečně	skutečně	k6eAd1	skutečně
první	první	k4xOgFnSc7	první
Napoleonovou	Napoleonová	k1gFnSc7	Napoleonová
femme	femmat	k5eAaPmIp3nS	femmat
fatale	fatale	k6eAd1	fatale
však	však	k9	však
byla	být	k5eAaImAgFnS	být
Désirée	Désirée	k1gFnSc1	Désirée
Clary	Clara	k1gFnSc2	Clara
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
obchodníka	obchodník	k1gMnSc2	obchodník
s	s	k7c7	s
hedvábím	hedvábí	k1gNnSc7	hedvábí
v	v	k7c6	v
Marseille	Marseille	k1gFnSc6	Marseille
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc4	její
starší	starý	k2eAgFnSc4d2	starší
sestru	sestra	k1gFnSc4	sestra
pojal	pojmout	k5eAaPmAgMnS	pojmout
za	za	k7c4	za
manželku	manželka	k1gFnSc4	manželka
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
Napoleon	Napoleon	k1gMnSc1	Napoleon
se	se	k3xPyFc4	se
netajil	tajit	k5eNaImAgMnS	tajit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
podobné	podobný	k2eAgInPc4d1	podobný
záměry	záměr	k1gInPc4	záměr
s	s	k7c7	s
Desirée	Desirée	k1gFnSc7	Desirée
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
se	se	k3xPyFc4	se
však	však	k9	však
Napoleon	Napoleon	k1gMnSc1	Napoleon
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
krásnou	krásný	k2eAgFnSc7d1	krásná
vdovou	vdova	k1gFnSc7	vdova
Josefínou	Josefína	k1gFnSc7	Josefína
de	de	k?	de
Beauharnais	Beauharnais	k1gFnSc2	Beauharnais
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
láska	láska	k1gFnSc1	láska
záhy	záhy	k6eAd1	záhy
vzala	vzít	k5eAaPmAgFnS	vzít
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
<g/>
.	.	kIx.	.
</s>
<s>
Desirée	Desiréat	k5eAaPmIp3nS	Desiréat
se	se	k3xPyFc4	se
po	po	k7c6	po
čase	čas	k1gInSc6	čas
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
Napoleonova	Napoleonův	k2eAgMnSc4d1	Napoleonův
maršála	maršál	k1gMnSc4	maršál
Bernadotta	Bernadott	k1gInSc2	Bernadott
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1818	[number]	k4	1818
se	se	k3xPyFc4	se
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
boku	bok	k1gInSc6	bok
stala	stát	k5eAaPmAgFnS	stát
švédskou	švédský	k2eAgFnSc7d1	švédská
královnou	královna	k1gFnSc7	královna
<g/>
.	.	kIx.	.
<g/>
Napoleon	napoleon	k1gInSc1	napoleon
byl	být	k5eAaImAgInS	být
Josefínou	Josefína	k1gFnSc7	Josefína
naprosto	naprosto	k6eAd1	naprosto
učarován	učarován	k2eAgMnSc1d1	učarován
a	a	k8xC	a
nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c4	po
jejich	jejich	k3xOp3gNnSc4	jejich
seznámení	seznámení	k1gNnSc4	seznámení
jí	jíst	k5eAaImIp3nS	jíst
učinil	učinit	k5eAaPmAgMnS	učinit
nabídku	nabídka	k1gFnSc4	nabídka
k	k	k7c3	k
sňatku	sňatek	k1gInSc3	sňatek
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
city	cit	k1gInPc1	cit
nebyly	být	k5eNaImAgInP	být
oboustranné	oboustranný	k2eAgInPc4d1	oboustranný
<g/>
,	,	kIx,	,
svolila	svolit	k5eAaPmAgFnS	svolit
a	a	k8xC	a
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1796	[number]	k4	1796
byli	být	k5eAaImAgMnP	být
oba	dva	k4xCgMnPc1	dva
sezdáni	sezdán	k2eAgMnPc1d1	sezdán
při	při	k7c6	při
občanském	občanský	k2eAgInSc6d1	občanský
obřadu	obřad	k1gInSc6	obřad
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
nato	nato	k6eAd1	nato
Napoleon	Napoleon	k1gMnSc1	Napoleon
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
se	se	k3xPyFc4	se
však	však	k9	však
začala	začít	k5eAaPmAgFnS	začít
milostně	milostně	k6eAd1	milostně
stýkat	stýkat	k5eAaImF	stýkat
s	s	k7c7	s
plukovníkem	plukovník	k1gMnSc7	plukovník
husarů	husar	k1gMnPc2	husar
Hippolytem	Hippolyt	k1gInSc7	Hippolyt
Charlesem	Charles	k1gMnSc7	Charles
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
udržovala	udržovat	k5eAaImAgFnS	udržovat
milostný	milostný	k2eAgInSc4d1	milostný
poměr	poměr	k1gInSc4	poměr
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
odplul	odplout	k5eAaPmAgMnS	odplout
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
si	se	k3xPyFc3	se
mladý	mladý	k2eAgMnSc1d1	mladý
generál	generál	k1gMnSc1	generál
<g/>
,	,	kIx,	,
deprimovaný	deprimovaný	k2eAgInSc1d1	deprimovaný
Josefíninými	Josefínin	k2eAgInPc7d1	Josefínin
poklesky	poklesek	k1gInPc7	poklesek
<g/>
,	,	kIx,	,
vyhlédl	vyhlédnout	k5eAaPmAgMnS	vyhlédnout
manželku	manželka	k1gFnSc4	manželka
jednoho	jeden	k4xCgInSc2	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgMnPc2	svůj
důstojníků	důstojník	k1gMnPc2	důstojník
jménem	jméno	k1gNnSc7	jméno
Pauline	Paulin	k1gInSc5	Paulin
Fouresová	Fouresový	k2eAgFnSc1d1	Fouresový
<g/>
,	,	kIx,	,
přezdívanou	přezdívaný	k2eAgFnSc4d1	přezdívaná
Bellilote	Bellilot	k1gInSc5	Bellilot
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
tato	tento	k3xDgFnSc1	tento
avantýra	avantýra	k1gFnSc1	avantýra
vydržela	vydržet	k5eAaPmAgFnS	vydržet
oběma	dva	k4xCgMnPc7	dva
milencům	milenec	k1gMnPc3	milenec
zhruba	zhruba	k6eAd1	zhruba
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
znamenala	znamenat	k5eAaImAgFnS	znamenat
konec	konec	k1gInSc4	konec
Napoleonovy	Napoleonův	k2eAgFnSc2d1	Napoleonova
fascinace	fascinace	k1gFnSc2	fascinace
Josefínou	Josefína	k1gFnSc7	Josefína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
na	na	k7c4	na
post	post	k1gInSc4	post
prvního	první	k4xOgMnSc2	první
konzula	konzul	k1gMnSc2	konzul
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
proto	proto	k6eAd1	proto
nebylo	být	k5eNaImAgNnS	být
již	již	k6eAd1	již
zatěžko	zatěžko	k6eAd1	zatěžko
začít	začít	k5eAaPmF	začít
si	se	k3xPyFc3	se
s	s	k7c7	s
patnáctiletou	patnáctiletý	k2eAgFnSc7d1	patnáctiletá
divadelní	divadelní	k2eAgFnSc7d1	divadelní
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
sám	sám	k3xTgMnSc1	sám
přezdíval	přezdívat	k5eAaImAgInS	přezdívat
Georgina	Georgina	k1gFnSc1	Georgina
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
intimní	intimní	k2eAgInSc1d1	intimní
vztah	vztah	k1gInSc1	vztah
trval	trvat	k5eAaImAgInS	trvat
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
a	a	k8xC	a
následovnicí	následovnice	k1gFnSc7	následovnice
mladičké	mladičký	k2eAgFnSc2d1	mladičká
herečky	herečka	k1gFnSc2	herečka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
dvorních	dvorní	k2eAgFnPc2d1	dvorní
dam	dáma	k1gFnPc2	dáma
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Antoinette	Antoinette	k1gFnSc1	Antoinette
Duchâtelová	Duchâtelová	k1gFnSc1	Duchâtelová
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
milenci	milenec	k1gMnPc1	milenec
se	se	k3xPyFc4	se
stýkali	stýkat	k5eAaImAgMnP	stýkat
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Napoleon	napoleon	k1gInSc1	napoleon
nakonec	nakonec	k6eAd1	nakonec
romanci	romance	k1gFnSc4	romance
ukončil	ukončit	k5eAaPmAgMnS	ukončit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jeho	jeho	k3xOp3gFnSc1	jeho
milenka	milenka	k1gFnSc1	milenka
se	se	k3xPyFc4	se
své	svůj	k3xOyFgFnSc6	svůj
vítězství	vítězství	k1gNnSc4	vítězství
nad	nad	k7c7	nad
císařovnou	císařovna	k1gFnSc7	císařovna
snažila	snažit	k5eAaImAgFnS	snažit
veřejně	veřejně	k6eAd1	veřejně
demonstrovat	demonstrovat	k5eAaBmF	demonstrovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
takové	takový	k3xDgNnSc4	takový
chování	chování	k1gNnSc4	chování
nestrpěl	strpět	k5eNaPmAgMnS	strpět
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
Napoleonovou	Napoleonův	k2eAgFnSc7d1	Napoleonova
významnou	významný	k2eAgFnSc7d1	významná
metresou	metresa	k1gFnSc7	metresa
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Eleonore	Eleonor	k1gInSc5	Eleonor
Dénuelle	Dénuell	k1gInSc5	Dénuell
De	De	k?	De
la	la	k0	la
Plaigne	Plaign	k1gMnSc5	Plaign
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
ho	on	k3xPp3gInSc4	on
seznámila	seznámit	k5eAaPmAgFnS	seznámit
jeho	jeho	k3xOp3gFnSc1	jeho
sestra	sestra	k1gFnSc1	sestra
Caroline	Carolin	k1gInSc5	Carolin
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
záměrem	záměr	k1gInSc7	záměr
(	(	kIx(	(
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
znepříjemnit	znepříjemnit	k5eAaPmF	znepříjemnit
život	život	k1gInSc4	život
své	svůj	k3xOyFgFnSc2	svůj
švagrové	švagrová	k1gFnSc2	švagrová
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
toto	tento	k3xDgNnSc1	tento
císařovo	císařův	k2eAgNnSc1d1	císařovo
milostné	milostný	k2eAgNnSc1d1	milostné
dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
ukojení	ukojení	k1gNnSc4	ukojení
jeho	jeho	k3xOp3gFnSc2	jeho
sexuální	sexuální	k2eAgFnSc2d1	sexuální
potřeby	potřeba	k1gFnSc2	potřeba
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
monografií	monografie	k1gFnPc2	monografie
prý	prý	k9	prý
dokonce	dokonce	k9	dokonce
bylo	být	k5eAaImAgNnS	být
dívčiným	dívčin	k2eAgInSc7d1	dívčin
úkolem	úkol	k1gInSc7	úkol
vyřešit	vyřešit	k5eAaPmF	vyřešit
stále	stále	k6eAd1	stále
ožehavější	ožehavý	k2eAgFnSc4d2	ožehavější
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
Napoleon	napoleon	k1gInSc1	napoleon
plodný	plodný	k2eAgInSc1d1	plodný
či	či	k8xC	či
nikoliv	nikoliv	k9	nikoliv
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
netrvalo	trvat	k5eNaImAgNnS	trvat
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
dívka	dívka	k1gFnSc1	dívka
otěhotněla	otěhotnět	k5eAaPmAgFnS	otěhotnět
a	a	k8xC	a
porodila	porodit	k5eAaPmAgFnS	porodit
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
dala	dát	k5eAaPmAgFnS	dát
jméno	jméno	k1gNnSc4	jméno
Léon	Léona	k1gFnPc2	Léona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
narodil	narodit	k5eAaPmAgMnS	narodit
první	první	k4xOgMnSc1	první
nemanželský	manželský	k2eNgMnSc1d1	nemanželský
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
navázal	navázat	k5eAaPmAgMnS	navázat
Napoleon	Napoleon	k1gMnSc1	Napoleon
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
intimní	intimní	k2eAgInSc4d1	intimní
poměr	poměr	k1gInSc4	poměr
s	s	k7c7	s
hraběnkou	hraběnka	k1gFnSc7	hraběnka
Walewskou	Walewský	k2eAgFnSc7d1	Walewský
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
zplodil	zplodit	k5eAaPmAgMnS	zplodit
svého	svůj	k3xOyFgMnSc2	svůj
druhého	druhý	k4xOgMnSc2	druhý
potomka	potomek	k1gMnSc2	potomek
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
pokřtěn	pokřtít	k5eAaPmNgMnS	pokřtít
jako	jako	k9	jako
Florian	Florian	k1gMnSc1	Florian
Alexander	Alexandra	k1gFnPc2	Alexandra
Josef	Josef	k1gMnSc1	Josef
Walewski	Walewsk	k1gFnSc2	Walewsk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dynastických	dynastický	k2eAgInPc2d1	dynastický
důvodů	důvod	k1gInPc2	důvod
však	však	k9	však
císař	císař	k1gMnSc1	císař
stále	stále	k6eAd1	stále
naléhavěji	naléhavě	k6eAd2	naléhavě
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
zplodit	zplodit	k5eAaPmF	zplodit
legitimního	legitimní	k2eAgMnSc4d1	legitimní
dědice	dědic	k1gMnSc4	dědic
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
míru	mír	k1gInSc2	mír
v	v	k7c6	v
Tylži	Tylž	k1gFnSc6	Tylž
a	a	k8xC	a
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
s	s	k7c7	s
konečnou	konečný	k2eAgFnSc7d1	konečná
platností	platnost	k1gFnSc7	platnost
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
ukončí	ukončit	k5eAaPmIp3nS	ukončit
prozatím	prozatím	k6eAd1	prozatím
neplodné	plodný	k2eNgNnSc1d1	neplodné
manželství	manželství	k1gNnSc1	manželství
s	s	k7c7	s
císařovnou	císařovna	k1gFnSc7	císařovna
Josefínou	Josefína	k1gFnSc7	Josefína
a	a	k8xC	a
ožení	oženit	k5eAaPmIp3nP	oženit
se	se	k3xPyFc4	se
s	s	k7c7	s
dívkou	dívka	k1gFnSc7	dívka
náležející	náležející	k2eAgFnSc7d1	náležející
k	k	k7c3	k
některé	některý	k3yIgFnSc3	některý
z	z	k7c2	z
předních	přední	k2eAgFnPc2d1	přední
evropských	evropský	k2eAgFnPc2d1	Evropská
panovnických	panovnický	k2eAgFnPc2d1	panovnická
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozvázání	rozvázání	k1gNnSc3	rozvázání
sňatku	sňatek	k1gInSc2	sňatek
došlo	dojít	k5eAaPmAgNnS	dojít
kvůli	kvůli	k7c3	kvůli
politickým	politický	k2eAgFnPc3d1	politická
okolnostem	okolnost	k1gFnPc3	okolnost
až	až	k9	až
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1809	[number]	k4	1809
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1810	[number]	k4	1810
při	při	k7c6	při
občanském	občanský	k2eAgInSc6d1	občanský
obřadu	obřad	k1gInSc6	obřad
konaném	konaný	k2eAgInSc6d1	konaný
v	v	k7c4	v
Saint-Cloud	Saint-Cloud	k1gInSc4	Saint-Cloud
Napoleon	Napoleon	k1gMnSc1	Napoleon
pojal	pojmout	k5eAaPmAgMnS	pojmout
za	za	k7c4	za
choť	choť	k1gFnSc4	choť
osmnáctiletou	osmnáctiletý	k2eAgFnSc4d1	osmnáctiletá
habsburskou	habsburský	k2eAgFnSc4d1	habsburská
princeznu	princezna	k1gFnSc4	princezna
Marii	Maria	k1gFnSc4	Maria
Luisu	Luisa	k1gFnSc4	Luisa
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
rakouského	rakouský	k2eAgMnSc2d1	rakouský
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
Oproti	oproti	k7c3	oproti
svým	svůj	k3xOyFgFnPc3	svůj
zvyklostem	zvyklost	k1gFnPc3	zvyklost
se	se	k3xPyFc4	se
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
týdnech	týden	k1gInPc6	týden
a	a	k8xC	a
měsících	měsíc	k1gInPc6	měsíc
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
sňatku	sňatek	k1gInSc2	sňatek
od	od	k7c2	od
své	svůj	k3xOyFgFnSc2	svůj
novomanželky	novomanželka	k1gFnSc2	novomanželka
ani	ani	k8xC	ani
nehnul	hnout	k5eNaPmAgMnS	hnout
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
z	z	k7c2	z
očí	oko	k1gNnPc2	oko
číst	číst	k5eAaImF	číst
každé	každý	k3xTgNnSc4	každý
přání	přání	k1gNnSc4	přání
ještě	ještě	k9	ještě
dřív	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
vyslovila	vyslovit	k5eAaPmAgFnS	vyslovit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
starostlivost	starostlivost	k1gFnSc1	starostlivost
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
znásobila	znásobit	k5eAaPmAgFnS	znásobit
<g/>
,	,	kIx,	,
když	když	k8xS	když
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Marie	Marie	k1gFnSc1	Marie
Luisa	Luisa	k1gFnSc1	Luisa
je	být	k5eAaImIp3nS	být
těhotná	těhotný	k2eAgFnSc1d1	těhotná
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sňatku	sňatek	k1gInSc6	sňatek
s	s	k7c7	s
habsburskou	habsburský	k2eAgFnSc7d1	habsburská
arcivévodkyní	arcivévodkyně	k1gFnSc7	arcivévodkyně
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
neteří	neteř	k1gFnSc7	neteř
popravené	popravený	k2eAgFnSc2d1	popravená
královny	královna	k1gFnSc2	královna
Marie	Maria	k1gFnSc2	Maria
Antoinetty	Antoinetta	k1gFnSc2	Antoinetta
<g/>
,	,	kIx,	,
však	však	k9	však
Francouzi	Francouz	k1gMnPc1	Francouz
spatřovali	spatřovat	k5eAaImAgMnP	spatřovat
cosi	cosi	k3yInSc1	cosi
urážlivého	urážlivý	k2eAgNnSc2d1	urážlivé
a	a	k8xC	a
žádný	žádný	k3yNgInSc1	žádný
Napoleonův	Napoleonův	k2eAgInSc1d1	Napoleonův
politický	politický	k2eAgInSc1d1	politický
akt	akt	k1gInSc1	akt
nebyl	být	k5eNaImAgInS	být
tak	tak	k6eAd1	tak
nepopulární	populární	k2eNgMnSc1d1	nepopulární
jako	jako	k8xS	jako
tento	tento	k3xDgInSc1	tento
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
období	období	k1gNnSc6	období
mezi	mezi	k7c7	mezi
sňatkem	sňatek	k1gInSc7	sňatek
s	s	k7c7	s
Josefínou	Josefína	k1gFnSc7	Josefína
a	a	k8xC	a
Marií	Maria	k1gFnSc7	Maria
Luisou	Luisa	k1gFnSc7	Luisa
žil	žít	k5eAaImAgInS	žít
vitální	vitální	k2eAgInSc1d1	vitální
Napoleon	napoleon	k1gInSc1	napoleon
velmi	velmi	k6eAd1	velmi
promiskuitním	promiskuitní	k2eAgInSc7d1	promiskuitní
životem	život	k1gInSc7	život
a	a	k8xC	a
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
nepřeberné	přeberný	k2eNgNnSc4d1	nepřeberné
množství	množství	k1gNnSc4	množství
milenek	milenka	k1gFnPc2	milenka
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
přitahovalo	přitahovat	k5eAaImAgNnS	přitahovat
jeho	jeho	k3xOp3gNnSc1	jeho
charisma	charisma	k1gNnSc1	charisma
i	i	k9	i
nimbus	nimbus	k1gInSc1	nimbus
císaře	císař	k1gMnSc2	císař
a	a	k8xC	a
neporazitelného	porazitelný	k2eNgMnSc2d1	neporazitelný
vojevůdce	vojevůdce	k1gMnSc2	vojevůdce
<g/>
.	.	kIx.	.
</s>
<s>
Prozaik	prozaik	k1gMnSc1	prozaik
a	a	k8xC	a
Napoleonův	Napoleonův	k2eAgMnSc1d1	Napoleonův
životopisec	životopisec	k1gMnSc1	životopisec
Stendhal	Stendhal	k1gMnSc1	Stendhal
(	(	kIx(	(
<g/>
Vie	Vie	k1gMnSc1	Vie
de	de	k?	de
Napoléon	Napoléon	k1gMnSc1	Napoléon
<g/>
)	)	kIx)	)
publikoval	publikovat	k5eAaBmAgMnS	publikovat
zajímavý	zajímavý	k2eAgInSc4d1	zajímavý
popis	popis	k1gInSc4	popis
mnohých	mnohé	k1gNnPc2	mnohé
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
schůzek	schůzka	k1gFnPc2	schůzka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
Napoleonův	Napoleonův	k2eAgInSc4d1	Napoleonův
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
ženám	žena	k1gFnPc3	žena
obecně	obecně	k6eAd1	obecně
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc1	všechen
nasvědčuje	nasvědčovat	k5eAaImIp3nS	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
jednání	jednání	k1gNnSc6	jednání
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
se	se	k3xPyFc4	se
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
řídil	řídit	k5eAaImAgMnS	řídit
zásadami	zásada	k1gFnPc7	zásada
francouzské	francouzský	k2eAgFnSc2d1	francouzská
galantnosti	galantnost	k1gFnSc2	galantnost
<g/>
.	.	kIx.	.
</s>
<s>
Dvorní	dvorní	k2eAgFnSc1d1	dvorní
dáma	dáma	k1gFnSc1	dáma
císařovny	císařovna	k1gFnSc2	císařovna
Josefíny	Josefína	k1gFnSc2	Josefína
Claire	Clair	k1gInSc5	Clair
de	de	k?	de
Rémusatová	Rémusatový	k2eAgFnSc1d1	Rémusatový
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
memoárech	memoáry	k1gInPc6	memoáry
(	(	kIx(	(
<g/>
Mémoires	Mémoires	k1gMnSc1	Mémoires
de	de	k?	de
madame	madame	k1gFnSc2	madame
de	de	k?	de
Rémusat	Rémusat	k1gFnSc2	Rémusat
<g/>
)	)	kIx)	)
dokonce	dokonce	k9	dokonce
tvrdila	tvrdit	k5eAaImAgFnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ženami	žena	k1gFnPc7	žena
opovrhoval	opovrhovat	k5eAaImAgMnS	opovrhovat
<g/>
.	.	kIx.	.
</s>
<s>
Zřídka	zřídka	k6eAd1	zřídka
ho	on	k3xPp3gMnSc4	on
prý	prý	k9	prý
slyšela	slyšet	k5eAaImAgFnS	slyšet
hovořit	hovořit	k5eAaImF	hovořit
s	s	k7c7	s
dámami	dáma	k1gFnPc7	dáma
od	od	k7c2	od
dvora	dvůr	k1gInSc2	dvůr
zdvořilým	zdvořilý	k2eAgInSc7d1	zdvořilý
tónem	tón	k1gInSc7	tón
a	a	k8xC	a
podle	podle	k7c2	podle
jejího	její	k3xOp3gNnSc2	její
mínění	mínění	k1gNnSc2	mínění
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
byl	být	k5eAaImAgMnS	být
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
krutý	krutý	k2eAgInSc1d1	krutý
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
příležitostech	příležitost	k1gFnPc6	příležitost
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
jejich	jejich	k3xOp3gInPc4	jejich
účesy	účes	k1gInPc4	účes
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
daly	dát	k5eAaPmAgInP	dát
kladnou	kladný	k2eAgFnSc4d1	kladná
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
dotaz	dotaz	k1gInSc4	dotaz
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jsou	být	k5eAaImIp3nP	být
vdané	vdaný	k2eAgFnPc1d1	vdaná
<g/>
,	,	kIx,	,
zajímalo	zajímat	k5eAaImAgNnS	zajímat
ho	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
ještě	ještě	k9	ještě
nejsou	být	k5eNaImIp3nP	být
těhotné	těhotný	k2eAgFnPc1d1	těhotná
<g/>
.	.	kIx.	.
</s>
<s>
Napoleonův	Napoleonův	k2eAgMnSc1d1	Napoleonův
komorník	komorník	k1gMnSc1	komorník
Louis	Louis	k1gMnSc1	Louis
Constant	Constant	k1gMnSc1	Constant
Wairy	Waira	k1gFnSc2	Waira
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
pamětech	paměť	k1gFnPc6	paměť
(	(	kIx(	(
<g/>
Mémoires	Mémoiresa	k1gFnPc2	Mémoiresa
de	de	k?	de
Constant	Constant	k1gMnSc1	Constant
<g/>
,	,	kIx,	,
premier	premier	k1gMnSc1	premier
valet	valet	k1gMnSc1	valet
de	de	k?	de
chambre	chambr	k1gInSc5	chambr
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
empereur	empereur	k1gMnSc1	empereur
<g/>
)	)	kIx)	)
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
podotýká	podotýkat	k5eAaImIp3nS	podotýkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
císař	císař	k1gMnSc1	císař
ženy	žena	k1gFnSc2	žena
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
pouhý	pouhý	k2eAgInSc4d1	pouhý
nástroj	nástroj	k1gInSc4	nástroj
k	k	k7c3	k
plození	plození	k1gNnSc3	plození
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc4	jejich
povinnosti	povinnost	k1gFnPc4	povinnost
spatřoval	spatřovat	k5eAaImAgMnS	spatřovat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
podřízenosti	podřízenost	k1gFnSc6	podřízenost
a	a	k8xC	a
závislosti	závislost	k1gFnSc6	závislost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literární	literární	k2eAgNnSc4d1	literární
dílo	dílo	k1gNnSc4	dílo
===	===	k?	===
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
budou	být	k5eAaImBp3nP	být
opomenuty	opomenout	k5eAaPmNgInP	opomenout
Napoleonovy	Napoleonův	k2eAgInPc1d1	Napoleonův
osobní	osobní	k2eAgInPc1d1	osobní
zápisky	zápisek	k1gInPc4	zápisek
z	z	k7c2	z
jinošských	jinošský	k2eAgNnPc2d1	jinošské
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
konstatovat	konstatovat	k5eAaBmF	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
chopil	chopit	k5eAaPmAgMnS	chopit
pera	pero	k1gNnSc2	pero
k	k	k7c3	k
většímu	veliký	k2eAgInSc3d2	veliký
literárnímu	literární	k2eAgInSc3d1	literární
počinu	počin	k1gInSc3	počin
v	v	k7c6	v
Auxonne	Auxonn	k1gMnSc5	Auxonn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sepsal	sepsat	k5eAaPmAgMnS	sepsat
několik	několik	k4yIc4	několik
pojednání	pojednání	k1gNnPc2	pojednání
o	o	k7c6	o
balistice	balistika	k1gFnSc6	balistika
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgInPc4	jenž
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
Poznámky	poznámka	k1gFnSc2	poznámka
z	z	k7c2	z
Pamětí	paměť	k1gFnPc2	paměť
markýze	markýz	k1gMnSc2	markýz
de	de	k?	de
Velliè	Velliè	k1gMnSc2	Velliè
<g/>
,	,	kIx,	,
Principy	princip	k1gInPc1	princip
dělostřelectví	dělostřelectví	k1gNnSc2	dělostřelectví
či	či	k8xC	či
Zápis	zápis	k1gInSc1	zápis
o	o	k7c6	o
rozmístění	rozmístění	k1gNnSc6	rozmístění
děl	dělo	k1gNnPc2	dělo
při	při	k7c6	při
vrhání	vrhání	k1gNnSc6	vrhání
bomb	bomba	k1gFnPc2	bomba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInPc6	jeho
sešitech	sešit	k1gInPc6	sešit
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
historická	historický	k2eAgNnPc1d1	historické
pojednání	pojednání	k1gNnPc1	pojednání
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
poznámky	poznámka	k1gFnPc4	poznámka
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
panování	panování	k1gNnSc2	panování
pruského	pruský	k2eAgMnSc2d1	pruský
krále	král	k1gMnSc2	král
Fridricha	Fridrich	k1gMnSc2	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
poznámky	poznámka	k1gFnPc4	poznámka
o	o	k7c6	o
dějinách	dějiny	k1gFnPc6	dějiny
Sorbonny	Sorbonna	k1gFnSc2	Sorbonna
a	a	k8xC	a
mnohé	mnohý	k2eAgFnPc4d1	mnohá
další	další	k2eAgNnPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
patří	patřit	k5eAaImIp3nS	patřit
též	též	k9	též
několik	několik	k4yIc1	několik
jeho	jeho	k3xOp3gMnPc2	jeho
beletristických	beletristický	k2eAgMnPc2d1	beletristický
pokusů	pokus	k1gInPc2	pokus
i	i	k8xC	i
pokročilých	pokročilý	k2eAgFnPc2d1	pokročilá
prací	práce	k1gFnPc2	práce
a	a	k8xC	a
filosoficko-politických	filosofickoolitický	k2eAgFnPc2d1	filosoficko-politický
studií	studie	k1gFnPc2	studie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
rozmanité	rozmanitý	k2eAgFnPc1d1	rozmanitá
žánrem	žánr	k1gInSc7	žánr
i	i	k8xC	i
charakterem	charakter	k1gInSc7	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Jmenovat	jmenovat	k5eAaBmF	jmenovat
lze	lze	k6eAd1	lze
například	například	k6eAd1	například
novely	novela	k1gFnSc2	novela
Hrabě	Hrabě	k1gMnSc1	Hrabě
Essex	Essex	k1gInSc1	Essex
(	(	kIx(	(
<g/>
1788	[number]	k4	1788
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Prorokova	prorokův	k2eAgFnSc1d1	Prorokova
maska	maska	k1gFnSc1	maska
(	(	kIx(	(
<g/>
1789	[number]	k4	1789
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dialog	dialog	k1gInSc1	dialog
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
(	(	kIx(	(
<g/>
1791	[number]	k4	1791
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Řeč	řeč	k1gFnSc1	řeč
o	o	k7c6	o
lásce	láska	k1gFnSc6	láska
ke	k	k7c3	k
slávě	sláva	k1gFnSc3	sláva
a	a	k8xC	a
lásce	láska	k1gFnSc3	láska
k	k	k7c3	k
vlasti	vlast	k1gFnSc3	vlast
<g/>
,	,	kIx,	,
Traktát	traktát	k1gInSc4	traktát
pro	pro	k7c4	pro
Lyonskou	lyonský	k2eAgFnSc4d1	Lyonská
akademii	akademie	k1gFnSc4	akademie
(	(	kIx(	(
<g/>
esej	esej	k1gFnSc4	esej
<g />
.	.	kIx.	.
</s>
<s>
se	se	k3xPyFc4	se
ucházela	ucházet	k5eAaImAgFnS	ucházet
o	o	k7c4	o
literární	literární	k2eAgFnSc4d1	literární
cenu	cena	k1gFnSc4	cena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Návrhy	návrh	k1gInPc1	návrh
ústavy	ústava	k1gFnSc2	ústava
společnosti	společnost	k1gFnSc2	společnost
Calotte	Calott	k1gInSc5	Calott
(	(	kIx(	(
<g/>
1788	[number]	k4	1788
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filosofické	filosofický	k2eAgNnSc1d1	filosofické
Pojednání	pojednání	k1gNnSc1	pojednání
o	o	k7c4	o
štěstí	štěstí	k1gNnSc4	štěstí
(	(	kIx(	(
<g/>
Discours	Discours	k1gInSc1	Discours
sur	sur	k?	sur
le	le	k?	le
bonheur	bonheura	k1gFnPc2	bonheura
<g/>
)	)	kIx)	)
či	či	k8xC	či
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
autobiografický	autobiografický	k2eAgInSc4d1	autobiografický
román	román	k1gInSc4	román
Glison	Glisona	k1gFnPc2	Glisona
a	a	k8xC	a
Eugénie	Eugénie	k1gFnSc2	Eugénie
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
láska	láska	k1gFnSc1	láska
mladého	mladý	k2eAgMnSc2d1	mladý
Bonaparta	Bonapart	k1gMnSc2	Bonapart
k	k	k7c3	k
Desirée	Desirée	k1gNnSc3	Desirée
Claryové	Claryová	k1gFnSc2	Claryová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1789	[number]	k4	1789
Napoleon	napoleon	k1gInSc1	napoleon
dokončil	dokončit	k5eAaPmAgInS	dokončit
nástin	nástin	k1gInSc4	nástin
dějin	dějiny	k1gFnPc2	dějiny
Korsiky	Korsika	k1gFnSc2	Korsika
a	a	k8xC	a
dočkal	dočkat	k5eAaPmAgMnS	dočkat
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
pochvalné	pochvalný	k2eAgFnSc2d1	pochvalná
kritiky	kritika	k1gFnSc2	kritika
tehdy	tehdy	k6eAd1	tehdy
populárního	populární	k2eAgMnSc2d1	populární
spisovatele	spisovatel	k1gMnSc2	spisovatel
Raynala	Raynala	k1gMnSc2	Raynala
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
dal	dát	k5eAaPmAgInS	dát
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
k	k	k7c3	k
posouzení	posouzení	k1gNnSc3	posouzení
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
asi	asi	k9	asi
nejuznávanější	uznávaný	k2eAgFnSc7d3	nejuznávanější
písemnou	písemný	k2eAgFnSc7d1	písemná
prací	práce	k1gFnSc7	práce
však	však	k9	však
byla	být	k5eAaImAgFnS	být
novela	novela	k1gFnSc1	novela
Večeře	večeře	k1gFnSc1	večeře
v	v	k7c6	v
Beaucairu	Beaucairo	k1gNnSc6	Beaucairo
(	(	kIx(	(
<g/>
Le	Le	k1gMnSc1	Le
souper	souper	k1gNnSc2	souper
de	de	k?	de
Beaucaire	Beaucair	k1gMnSc5	Beaucair
<g/>
,	,	kIx,	,
1793	[number]	k4	1793
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
napsal	napsat	k5eAaPmAgMnS	napsat
v	v	k7c6	v
bouřlivém	bouřlivý	k2eAgNnSc6d1	bouřlivé
období	období	k1gNnSc6	období
před	před	k7c7	před
Toulonem	Toulon	k1gInSc7	Toulon
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
práci	práce	k1gFnSc4	práce
nejprve	nejprve	k6eAd1	nejprve
vydal	vydat	k5eAaPmAgInS	vydat
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
skrovných	skrovný	k2eAgInPc2d1	skrovný
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zanedlouho	zanedlouho	k6eAd1	zanedlouho
byla	být	k5eAaImAgFnS	být
publikovaná	publikovaný	k2eAgFnSc1d1	publikovaná
na	na	k7c4	na
náklady	náklad	k1gInPc4	náklad
státu	stát	k1gInSc2	stát
a	a	k8xC	a
upřela	upřít	k5eAaPmAgFnS	upřít
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
pozornost	pozornost	k1gFnSc4	pozornost
vedoucích	vedoucí	k2eAgMnPc2d1	vedoucí
mužů	muž	k1gMnPc2	muž
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
dalších	další	k2eAgNnPc2d1	další
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
historii	historie	k1gFnSc3	historie
uchovalo	uchovat	k5eAaPmAgNnS	uchovat
nesmírné	smírný	k2eNgNnSc1d1	nesmírné
množství	množství	k1gNnSc1	množství
Napoleonových	Napoleonových	k2eAgInPc2d1	Napoleonových
dopisů	dopis	k1gInPc2	dopis
příbuzným	příbuzný	k1gMnPc3	příbuzný
<g/>
,	,	kIx,	,
přátelům	přítel	k1gMnPc3	přítel
a	a	k8xC	a
zejména	zejména	k9	zejména
Josefíně	Josefína	k1gFnSc6	Josefína
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
korespondence	korespondence	k1gFnSc1	korespondence
byla	být	k5eAaImAgFnS	být
za	za	k7c4	za
panování	panování	k1gNnSc4	panování
jeho	jeho	k3xOp3gMnSc2	jeho
synovce	synovec	k1gMnSc2	synovec
Napoleona	Napoleon	k1gMnSc2	Napoleon
III	III	kA	III
<g/>
.	.	kIx.	.
shromážděna	shromážděn	k2eAgFnSc1d1	shromážděna
a	a	k8xC	a
vydána	vydán	k2eAgFnSc1d1	vydána
ve	v	k7c6	v
dvaatřiceti	dvaatřicet	k4xCc2	dvaatřicet
svazcích	svazek	k1gInPc6	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
určitou	určitý	k2eAgFnSc4d1	určitá
formu	forma	k1gFnSc4	forma
literární	literární	k2eAgFnSc2d1	literární
činnosti	činnost	k1gFnSc2	činnost
lze	lze	k6eAd1	lze
také	také	k9	také
považovat	považovat	k5eAaImF	považovat
psaní	psaní	k1gNnSc4	psaní
proklamací	proklamace	k1gFnPc2	proklamace
a	a	k8xC	a
mnohdy	mnohdy	k6eAd1	mnohdy
velmi	velmi	k6eAd1	velmi
propagandistických	propagandistický	k2eAgInPc2d1	propagandistický
bulletinů	bulletin	k1gInPc2	bulletin
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
předkládal	předkládat	k5eAaImAgInS	předkládat
veřejnosti	veřejnost	k1gFnSc2	veřejnost
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
dosažené	dosažený	k2eAgInPc4d1	dosažený
úspěchy	úspěch	k1gInPc4	úspěch
i	i	k8xC	i
neúspěchy	neúspěch	k1gInPc4	neúspěch
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
posledních	poslední	k2eAgFnPc2d1	poslední
dochovaných	dochovaný	k2eAgFnPc2d1	dochovaná
památek	památka	k1gFnPc2	památka
na	na	k7c4	na
Napoleona	Napoleon	k1gMnSc4	Napoleon
jsou	být	k5eAaImIp3nP	být
jím	on	k3xPp3gNnSc7	on
diktované	diktovaný	k2eAgFnSc2d1	diktovaná
paměti	paměť	k1gFnSc2	paměť
(	(	kIx(	(
<g/>
Mémorial	Mémorial	k1gMnSc1	Mémorial
de	de	k?	de
Saint-Hélè	Saint-Hélè	k1gMnSc1	Saint-Hélè
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
na	na	k7c6	na
Svaté	svatý	k2eAgFnSc6d1	svatá
Heleně	Helena	k1gFnSc6	Helena
sepsal	sepsat	k5eAaPmAgMnS	sepsat
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1823	[number]	k4	1823
vydal	vydat	k5eAaPmAgMnS	vydat
jeho	jeho	k3xOp3gMnSc1	jeho
přítel	přítel	k1gMnSc1	přítel
Emmanuel	Emmanuel	k1gMnSc1	Emmanuel
de	de	k?	de
Las	laso	k1gNnPc2	laso
Cases	Casesa	k1gFnPc2	Casesa
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
několikasvazkové	několikasvazkový	k2eAgNnSc1d1	několikasvazkové
dílo	dílo	k1gNnSc1	dílo
však	však	k9	však
nemá	mít	k5eNaImIp3nS	mít
objektivní	objektivní	k2eAgFnSc4d1	objektivní
historickou	historický	k2eAgFnSc4d1	historická
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
odrazem	odraz	k1gInSc7	odraz
Napoleonova	Napoleonův	k2eAgInSc2d1	Napoleonův
záměru	záměr	k1gInSc2	záměr
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
budoucnost	budoucnost	k1gFnSc1	budoucnost
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
o	o	k7c6	o
událostech	událost	k1gFnPc6	událost
představu	představa	k1gFnSc4	představa
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnPc2	jeho
přání	přání	k1gNnPc2	přání
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgFnPc1d1	zajímavá
však	však	k9	však
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gFnPc1	jeho
hojné	hojný	k2eAgFnPc1d1	hojná
poznámky	poznámka	k1gFnPc1	poznámka
a	a	k8xC	a
diktáty	diktát	k1gInPc1	diktát
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
svého	svůj	k3xOyFgInSc2	svůj
druhého	druhý	k4xOgInSc2	druhý
exilu	exil	k1gInSc2	exil
vedl	vést	k5eAaImAgMnS	vést
o	o	k7c6	o
válkách	válka	k1gFnPc6	válka
<g/>
,	,	kIx,	,
vojenském	vojenský	k2eAgNnSc6d1	vojenské
umění	umění	k1gNnSc6	umění
jiných	jiný	k2eAgMnPc2d1	jiný
vojevůdců	vojevůdce	k1gMnPc2	vojevůdce
(	(	kIx(	(
<g/>
Caesara	Caesar	k1gMnSc2	Caesar
<g/>
,	,	kIx,	,
Turenna	Turenn	k1gMnSc2	Turenn
<g/>
,	,	kIx,	,
Fridricha	Fridrich	k1gMnSc2	Fridrich
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
)	)	kIx)	)
a	a	k8xC	a
vojenství	vojenství	k1gNnSc4	vojenství
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Celkové	celkový	k2eAgNnSc1d1	celkové
hodnocení	hodnocení	k1gNnSc1	hodnocení
===	===	k?	===
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Napoleona	Napoleon	k1gMnSc2	Napoleon
Bonaparta	Bonapart	k1gMnSc2	Bonapart
zanechala	zanechat	k5eAaPmAgFnS	zanechat
v	v	k7c6	v
celosvětových	celosvětový	k2eAgFnPc6d1	celosvětová
dějinách	dějiny	k1gFnPc6	dějiny
nesmazatelný	smazatelný	k2eNgInSc4d1	nesmazatelný
odraz	odraz	k1gInSc4	odraz
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
odraz	odraz	k1gInSc1	odraz
značně	značně	k6eAd1	značně
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
získal	získat	k5eAaPmAgMnS	získat
podobu	podoba	k1gFnSc4	podoba
heroické	heroický	k2eAgFnSc2d1	heroická
legendy	legenda	k1gFnSc2	legenda
<g/>
,	,	kIx,	,
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnPc1	druhý
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
tématem	téma	k1gNnSc7	téma
až	až	k6eAd1	až
démonickým	démonický	k2eAgNnSc7d1	démonické
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nesporným	sporný	k2eNgInSc7d1	nesporný
faktem	fakt	k1gInSc7	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
pozitivního	pozitivní	k2eAgInSc2d1	pozitivní
Napoleonova	Napoleonův	k2eAgInSc2d1	Napoleonův
kultu	kult	k1gInSc2	kult
sehrály	sehrát	k5eAaPmAgFnP	sehrát
zásadní	zásadní	k2eAgFnSc4d1	zásadní
roli	role	k1gFnSc4	role
Paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
na	na	k7c6	na
Svaté	svatý	k2eAgFnSc6d1	svatá
Heleně	Helena	k1gFnSc6	Helena
sepsal	sepsat	k5eAaPmAgMnS	sepsat
hrabě	hrabě	k1gMnSc1	hrabě
Las	laso	k1gNnPc2	laso
Cases	Casesa	k1gFnPc2	Casesa
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgMnSc1d1	britský
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
oxfordský	oxfordský	k2eAgMnSc1d1	oxfordský
profesor	profesor	k1gMnSc1	profesor
Geoffrey	Geoffrea	k1gFnSc2	Geoffrea
Ellis	Ellis	k1gFnSc2	Ellis
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
poznamenal	poznamenat	k5eAaPmAgMnS	poznamenat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Podobný	podobný	k2eAgInSc4d1	podobný
vliv	vliv	k1gInSc4	vliv
měly	mít	k5eAaImAgInP	mít
i	i	k9	i
vícesvazkové	vícesvazkový	k2eAgFnPc1d1	vícesvazková
dějiny	dějiny	k1gFnPc1	dějiny
bývalého	bývalý	k2eAgMnSc2d1	bývalý
císařského	císařský	k2eAgMnSc2d1	císařský
diplomata	diplomat	k1gMnSc2	diplomat
barona	baron	k1gMnSc2	baron
Bignona	Bignon	k1gMnSc2	Bignon
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
obdržel	obdržet	k5eAaPmAgMnS	obdržet
od	od	k7c2	od
Napoleona	Napoleon	k1gMnSc2	Napoleon
na	na	k7c6	na
Svaté	svatý	k2eAgFnSc6d1	svatá
Heleně	Helena	k1gFnSc6	Helena
úkol	úkol	k1gInSc4	úkol
sepsat	sepsat	k5eAaPmF	sepsat
historii	historie	k1gFnSc4	historie
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
spisovatel	spisovatel	k1gMnSc1	spisovatel
sám	sám	k3xTgMnSc1	sám
přes	přes	k7c4	přes
počáteční	počáteční	k2eAgInSc4d1	počáteční
obdiv	obdiv	k1gInSc4	obdiv
k	k	k7c3	k
císařově	císařův	k2eAgFnSc3d1	císařova
osobě	osoba	k1gFnSc3	osoba
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
používal	používat	k5eAaImAgMnS	používat
despotických	despotický	k2eAgFnPc2d1	despotická
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
ovšem	ovšem	k9	ovšem
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
nutné	nutný	k2eAgInPc4d1	nutný
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
vládě	vláda	k1gFnSc6	vláda
Francie	Francie	k1gFnSc2	Francie
obnoven	obnoven	k2eAgInSc4d1	obnoven
řád	řád	k1gInSc4	řád
a	a	k8xC	a
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
čest	čest	k1gFnSc1	čest
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c7	za
vytvářením	vytváření	k1gNnSc7	vytváření
nepřátelské	přátelský	k2eNgFnSc2d1	nepřátelská
varianty	varianta	k1gFnSc2	varianta
Napoleonova	Napoleonův	k2eAgInSc2d1	Napoleonův
obrazu	obraz	k1gInSc2	obraz
stáli	stát	k5eAaImAgMnP	stát
v	v	k7c6	v
prvopočátcích	prvopočátek	k1gInPc6	prvopočátek
Madame	madame	k1gFnSc2	madame
de	de	k?	de
Staël	Staël	k1gMnSc1	Staël
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
François	François	k1gFnSc2	François
Chateaubriand	Chateaubriand	k1gInSc1	Chateaubriand
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
prvně	prvně	k?	prvně
jmenovanou	jmenovaná	k1gFnSc4	jmenovaná
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1803	[number]	k4	1803
vypovězena	vypovědět	k5eAaPmNgFnS	vypovědět
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
císař	císař	k1gMnSc1	císař
triumfujícím	triumfující	k2eAgMnSc7d1	triumfující
generálem	generál	k1gMnSc7	generál
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
od	od	k7c2	od
něhož	jenž	k3xRgInSc2	jenž
se	se	k3xPyFc4	se
hodně	hodně	k6eAd1	hodně
očekávalo	očekávat	k5eAaImAgNnS	očekávat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
všechno	všechen	k3xTgNnSc1	všechen
toto	tento	k3xDgNnSc1	tento
očekávání	očekávání	k1gNnSc1	očekávání
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
osobou	osoba	k1gFnSc7	osoba
systematicky	systematicky	k6eAd1	systematicky
zdeformováno	zdeformován	k2eAgNnSc1d1	zdeformováno
zneužitím	zneužití	k1gNnSc7	zneužití
moci	moc	k1gFnSc2	moc
<g/>
.	.	kIx.	.
</s>
<s>
Útoky	útok	k1gInPc1	útok
Madame	madame	k1gFnSc2	madame
de	de	k?	de
Staël	Staël	k1gInSc1	Staël
byly	být	k5eAaImAgFnP	být
namířeny	namířit	k5eAaPmNgInP	namířit
zejména	zejména	k9	zejména
na	na	k7c4	na
Napoleonovu	Napoleonův	k2eAgFnSc4d1	Napoleonova
bezuzdnou	bezuzdný	k2eAgFnSc4d1	bezuzdná
domýšlivost	domýšlivost	k1gFnSc4	domýšlivost
a	a	k8xC	a
ambice	ambice	k1gFnPc4	ambice
<g/>
,	,	kIx,	,
na	na	k7c4	na
infantilní	infantilní	k2eAgNnSc4d1	infantilní
zaslepení	zaslepení	k1gNnSc4	zaslepení
osobní	osobní	k2eAgFnSc7d1	osobní
slávou	sláva	k1gFnSc7	sláva
<g/>
,	,	kIx,	,
na	na	k7c4	na
despotické	despotický	k2eAgFnPc4d1	despotická
metody	metoda	k1gFnPc4	metoda
atd.	atd.	kA	atd.
Celkově	celkově	k6eAd1	celkově
Napoleona	Napoleon	k1gMnSc2	Napoleon
vykreslila	vykreslit	k5eAaPmAgFnS	vykreslit
jako	jako	k8xS	jako
nemilosrdného	milosrdný	k2eNgMnSc4d1	nemilosrdný
egoistu	egoista	k1gMnSc4	egoista
<g/>
,	,	kIx,	,
manipulátora	manipulátor	k1gMnSc4	manipulátor
bez	bez	k7c2	bez
skrupulí	skrupule	k1gFnPc2	skrupule
<g/>
,	,	kIx,	,
vykořeněného	vykořeněný	k2eAgMnSc4d1	vykořeněný
outsidera	outsider	k1gMnSc4	outsider
bez	bez	k7c2	bez
skutečného	skutečný	k2eAgNnSc2d1	skutečné
vědomí	vědomí	k1gNnSc2	vědomí
vlasti	vlast	k1gFnSc2	vlast
<g/>
,	,	kIx,	,
ignoranta	ignorant	k1gMnSc4	ignorant
a	a	k8xC	a
zhoubnou	zhoubný	k2eAgFnSc4d1	zhoubná
zrůdu	zrůda	k1gFnSc4	zrůda
dějin	dějiny	k1gFnPc2	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Chateaubriand	Chateaubriand	k1gInSc1	Chateaubriand
na	na	k7c4	na
francouzského	francouzský	k2eAgMnSc4d1	francouzský
císaře	císař	k1gMnSc4	císař
pohlížel	pohlížet	k5eAaImAgMnS	pohlížet
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
katolického	katolický	k2eAgMnSc2d1	katolický
vzdělance	vzdělanec	k1gMnSc2	vzdělanec
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgInS	být
znechucen	znechutit	k5eAaPmNgInS	znechutit
cynickou	cynický	k2eAgFnSc7d1	cynická
manipulací	manipulace	k1gFnSc7	manipulace
konkordátu	konkordát	k1gInSc2	konkordát
s	s	k7c7	s
církví	církev	k1gFnSc7	církev
a	a	k8xC	a
uvězněním	uvěznění	k1gNnSc7	uvěznění
Pia	Pius	k1gMnSc2	Pius
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Nejen	nejen	k6eAd1	nejen
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
díle	díl	k1gInSc6	díl
Paměti	paměť	k1gFnPc1	paměť
ze	z	k7c2	z
záhrobí	záhrobí	k1gNnSc2	záhrobí
(	(	kIx(	(
<g/>
Mémoires	Mémoires	k1gInSc1	Mémoires
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
outre	outr	k1gMnSc5	outr
tombe	tomb	k1gMnSc5	tomb
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
v	v	k7c6	v
období	období	k1gNnSc6	období
rozkvětu	rozkvět	k1gInSc2	rozkvět
napoleonské	napoleonský	k2eAgFnSc2d1	napoleonská
legendy	legenda	k1gFnSc2	legenda
několikrát	několikrát	k6eAd1	několikrát
pokusil	pokusit	k5eAaPmAgMnS	pokusit
tento	tento	k3xDgInSc4	tento
mýtus	mýtus	k1gInSc4	mýtus
vyvrátit	vyvrátit	k5eAaPmF	vyvrátit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
Evropy	Evropa	k1gFnSc2	Evropa
byl	být	k5eAaImAgInS	být
Napoleonův	Napoleonův	k2eAgInSc1d1	Napoleonův
odkaz	odkaz	k1gInSc1	odkaz
vnímán	vnímat	k5eAaImNgInS	vnímat
stejně	stejně	k6eAd1	stejně
rozporuplně	rozporuplně	k6eAd1	rozporuplně
jako	jako	k8xS	jako
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ani	ani	k8xC	ani
zde	zde	k6eAd1	zde
tento	tento	k3xDgInSc4	tento
antagonismus	antagonismus	k1gInSc4	antagonismus
nelze	lze	k6eNd1	lze
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
souhrnně	souhrnně	k6eAd1	souhrnně
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
míra	míra	k1gFnSc1	míra
obdivu	obdiv	k1gInSc2	obdiv
i	i	k8xC	i
negativismu	negativismus	k1gInSc2	negativismus
se	se	k3xPyFc4	se
lišila	lišit	k5eAaImAgFnS	lišit
podle	podle	k7c2	podle
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
anektovaných	anektovaný	k2eAgFnPc2d1	anektovaná
Francií	Francie	k1gFnPc2	Francie
byla	být	k5eAaImAgFnS	být
jiná	jiný	k2eAgFnSc1d1	jiná
než	než	k8xS	než
na	na	k7c6	na
území	území	k1gNnSc6	území
vazalů	vazal	k1gMnPc2	vazal
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
a	a	k8xC	a
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgInSc7	svůj
postupem	postup	k1gInSc7	postup
mimo	mimo	k7c4	mimo
hranice	hranice	k1gFnPc4	hranice
císařství	císařství	k1gNnSc2	císařství
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
Napoleon	napoleon	k1gInSc1	napoleon
převážně	převážně	k6eAd1	převážně
odpor	odpor	k1gInSc4	odpor
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
ve	v	k7c4	v
svůj	svůj	k3xOyFgInSc4	svůj
prospěch	prospěch	k1gInSc4	prospěch
využila	využít	k5eAaPmAgFnS	využít
různá	různý	k2eAgNnPc4d1	různé
nacionalistická	nacionalistický	k2eAgNnPc4d1	nacionalistické
a	a	k8xC	a
liberální	liberální	k2eAgNnPc4d1	liberální
hnutí	hnutí	k1gNnPc4	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
však	však	k9	však
opět	opět	k6eAd1	opět
lišila	lišit	k5eAaImAgFnS	lišit
podle	podle	k7c2	podle
územního	územní	k2eAgInSc2d1	územní
celku	celek	k1gInSc2	celek
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
rozdílná	rozdílný	k2eAgFnSc1d1	rozdílná
například	například	k6eAd1	například
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
nebo	nebo	k8xC	nebo
Itálii	Itálie	k1gFnSc6	Itálie
oproti	oproti	k7c3	oproti
střední	střední	k2eAgFnSc3d1	střední
Evropě	Evropa	k1gFnSc3	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
lidové	lidový	k2eAgFnPc1d1	lidová
vrstvy	vrstva	k1gFnPc1	vrstva
často	často	k6eAd1	často
kladly	klást	k5eAaImAgFnP	klást
Napoleona	Napoleon	k1gMnSc2	Napoleon
vedle	vedle	k7c2	vedle
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jeho	jeho	k3xOp3gFnSc4	jeho
úlohu	úloha	k1gFnSc4	úloha
reformátora	reformátor	k1gMnSc2	reformátor
sehrál	sehrát	k5eAaPmAgMnS	sehrát
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Ilyrsku	Ilyrsek	k1gInSc6	Ilyrsek
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
v	v	k7c6	v
polských	polský	k2eAgFnPc6d1	polská
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
<g/>
Při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
všestranné	všestranný	k2eAgFnSc2d1	všestranná
odpovědi	odpověď	k1gFnSc2	odpověď
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
nalezla	nalézt	k5eAaBmAgFnS	nalézt
klíč	klíč	k1gInSc4	klíč
k	k	k7c3	k
Napoleonovým	Napoleonův	k2eAgFnPc3d1	Napoleonova
ambicím	ambice	k1gFnPc3	ambice
<g/>
,	,	kIx,	,
zaznívá	zaznívat	k5eAaImIp3nS	zaznívat
u	u	k7c2	u
starších	starý	k2eAgMnPc2d2	starší
i	i	k8xC	i
nejmodernějších	moderní	k2eAgMnPc2d3	nejmodernější
historiků	historik	k1gMnPc2	historik
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
otázka	otázka	k1gFnSc1	otázka
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gInSc7	jeho
úmyslem	úmysl	k1gInSc7	úmysl
postupnou	postupný	k2eAgFnSc7d1	postupná
integrací	integrace	k1gFnSc7	integrace
podmaněných	podmaněný	k2eAgFnPc2d1	Podmaněná
zemí	zem	k1gFnPc2	zem
vybudovat	vybudovat	k5eAaPmF	vybudovat
celoevropskou	celoevropský	k2eAgFnSc7d1	celoevropská
<g/>
,	,	kIx,	,
potažmo	potažmo	k6eAd1	potažmo
celosvětovou	celosvětový	k2eAgFnSc4d1	celosvětová
universální	universální	k2eAgFnSc4d1	universální
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
úvaze	úvaha	k1gFnSc3	úvaha
vedlo	vést	k5eAaImAgNnS	vést
zejména	zejména	k9	zejména
jeho	jeho	k3xOp3gNnSc1	jeho
konstatování	konstatování	k1gNnSc1	konstatování
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Můj	můj	k3xOp1gInSc1	můj
osud	osud	k1gInSc1	osud
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
nenaplnil	naplnit	k5eNaPmAgMnS	naplnit
<g/>
.	.	kIx.	.
</s>
<s>
Chci	chtít	k5eAaImIp1nS	chtít
splnit	splnit	k5eAaPmF	splnit
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
zatím	zatím	k6eAd1	zatím
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
načrtnuto	načrtnut	k2eAgNnSc1d1	načrtnuto
<g/>
.	.	kIx.	.
</s>
<s>
Chci	chtít	k5eAaImIp1nS	chtít
evropský	evropský	k2eAgInSc4d1	evropský
zákoník	zákoník	k1gInSc4	zákoník
<g/>
,	,	kIx,	,
evropský	evropský	k2eAgInSc4d1	evropský
apelační	apelační	k2eAgInSc4d1	apelační
soud	soud	k1gInSc4	soud
<g/>
,	,	kIx,	,
společnou	společný	k2eAgFnSc4d1	společná
měnu	měna	k1gFnSc4	měna
<g/>
,	,	kIx,	,
stejné	stejný	k2eAgFnPc4d1	stejná
váhy	váha	k1gFnPc4	váha
a	a	k8xC	a
míry	míra	k1gFnPc4	míra
<g/>
,	,	kIx,	,
stejné	stejný	k2eAgInPc4d1	stejný
zákony	zákon	k1gInPc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
národů	národ	k1gInPc2	národ
Evropy	Evropa	k1gFnSc2	Evropa
musím	muset	k5eAaImIp1nS	muset
udělat	udělat	k5eAaPmF	udělat
jediný	jediný	k2eAgMnSc1d1	jediný
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Těmto	tento	k3xDgFnPc3	tento
slovům	slovo	k1gNnPc3	slovo
by	by	kYmCp3nP	by
napovídala	napovídat	k5eAaBmAgFnS	napovídat
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
snaha	snaha	k1gFnSc1	snaha
o	o	k7c6	o
anexi	anexe	k1gFnSc6	anexe
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
většiny	většina	k1gFnSc2	většina
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
podle	podle	k7c2	podle
Geoffrey	Geoffrea	k1gFnSc2	Geoffrea
Ellise	Ellise	k1gFnSc2	Ellise
je	být	k5eAaImIp3nS	být
představa	představa	k1gFnSc1	představa
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Napoleon	Napoleon	k1gMnSc1	Napoleon
byl	být	k5eAaImAgMnS	být
raný	raný	k2eAgMnSc1d1	raný
architekt	architekt	k1gMnSc1	architekt
moderní	moderní	k2eAgFnSc2d1	moderní
evropské	evropský	k2eAgFnSc2d1	Evropská
myšlenky	myšlenka	k1gFnSc2	myšlenka
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
značně	značně	k6eAd1	značně
zjednodušená	zjednodušený	k2eAgFnSc1d1	zjednodušená
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
jednostrannost	jednostrannost	k1gFnSc1	jednostrannost
vojenských	vojenský	k2eAgInPc2d1	vojenský
požadavků	požadavek	k1gInPc2	požadavek
<g/>
,	,	kIx,	,
kontinentální	kontinentální	k2eAgFnSc1d1	kontinentální
blokáda	blokáda	k1gFnSc1	blokáda
proti	proti	k7c3	proti
Británii	Británie	k1gFnSc3	Británie
<g/>
,	,	kIx,	,
vyhrazené	vyhrazený	k2eAgInPc1d1	vyhrazený
trhy	trh	k1gInPc1	trh
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
zneužívání	zneužívání	k1gNnSc1	zneužívání
veřejné	veřejný	k2eAgFnSc2d1	veřejná
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
podmaněných	podmaněný	k2eAgInPc6d1	podmaněný
státech	stát	k1gInPc6	stát
k	k	k7c3	k
osobním	osobní	k2eAgInPc3d1	osobní
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Napoleonovu	Napoleonův	k2eAgInSc3d1	Napoleonův
postoji	postoj	k1gInSc3	postoj
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
tématu	téma	k1gNnSc3	téma
leccos	leccos	k3yInSc1	leccos
vypoví	vypovědět	k5eAaPmIp3nS	vypovědět
i	i	k9	i
dopis	dopis	k1gInSc4	dopis
ze	z	k7c2	z
srpna	srpen	k1gInSc2	srpen
1810	[number]	k4	1810
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bez	bez	k7c2	bez
obalu	obal	k1gInSc2	obal
konstatuje	konstatovat	k5eAaBmIp3nS	konstatovat
<g/>
:	:	kIx,	:
Francie	Francie	k1gFnSc1	Francie
především	především	k9	především
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
spíš	spíš	k9	spíš
nasvědčuje	nasvědčovat	k5eAaImIp3nS	nasvědčovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bonapartovy	Bonapartův	k2eAgFnPc1d1	Bonapartova
ambice	ambice	k1gFnPc1	ambice
nebyly	být	k5eNaImAgFnP	být
podřízeny	podřídit	k5eAaPmNgFnP	podřídit
nějakému	nějaký	k3yIgInSc3	nějaký
dokonalému	dokonalý	k2eAgInSc3d1	dokonalý
plánu	plán	k1gInSc3	plán
nebo	nebo	k8xC	nebo
konceptu	koncept	k1gInSc3	koncept
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíš	spíš	k9	spíš
se	se	k3xPyFc4	se
rozvíjely	rozvíjet	k5eAaImAgInP	rozvíjet
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
postupném	postupný	k2eAgInSc6d1	postupný
pragmatickém	pragmatický	k2eAgInSc6d1	pragmatický
procesu	proces	k1gInSc6	proces
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odraz	odraz	k1gInSc4	odraz
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
==	==	k?	==
</s>
</p>
<p>
<s>
Epocha	epocha	k1gFnSc1	epocha
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
fenomenální	fenomenální	k2eAgFnSc1d1	fenomenální
dráha	dráha	k1gFnSc1	dráha
Napoleonova	Napoleonův	k2eAgInSc2d1	Napoleonův
života	život	k1gInSc2	život
zanechaly	zanechat	k5eAaPmAgFnP	zanechat
svůj	svůj	k3xOyFgInSc4	svůj
odraz	odraz	k1gInSc4	odraz
nejen	nejen	k6eAd1	nejen
ve	v	k7c6	v
sférách	sféra	k1gFnPc6	sféra
politických	politický	k2eAgMnPc2d1	politický
a	a	k8xC	a
vojenských	vojenský	k2eAgMnPc2d1	vojenský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
uměleckém	umělecký	k2eAgNnSc6d1	umělecké
<g/>
.	.	kIx.	.
</s>
<s>
Klasicistický	klasicistický	k2eAgInSc4d1	klasicistický
sloh	sloh	k1gInSc4	sloh
z	z	k7c2	z
období	období	k1gNnSc2	období
císařství	císařství	k1gNnSc2	císařství
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
počátku	počátek	k1gInSc6	počátek
svého	svůj	k3xOyFgInSc2	svůj
vývoje	vývoj	k1gInSc2	vývoj
stal	stát	k5eAaPmAgInS	stát
slohem	sloh	k1gInSc7	sloh
památníků	památník	k1gInPc2	památník
a	a	k8xC	a
výrazem	výraz	k1gInSc7	výraz
reprezentace	reprezentace	k1gFnSc2	reprezentace
císařské	císařský	k2eAgFnSc2d1	císařská
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
empír	empír	k1gInSc1	empír
a	a	k8xC	a
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
se	se	k3xPyFc4	se
především	především	k9	především
v	v	k7c6	v
nábytku	nábytek	k1gInSc6	nábytek
<g/>
,	,	kIx,	,
módě	móda	k1gFnSc6	móda
a	a	k8xC	a
interiérové	interiérový	k2eAgFnSc3d1	interiérová
dekoraci	dekorace	k1gFnSc3	dekorace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
malířství	malířství	k1gNnSc6	malířství
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
řada	řada	k1gFnSc1	řada
stěžejních	stěžejní	k2eAgNnPc2d1	stěžejní
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
byla	být	k5eAaImAgNnP	být
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
pořízena	pořídit	k5eAaPmNgFnS	pořídit
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
samotného	samotný	k2eAgMnSc2d1	samotný
Napoleona	Napoleon	k1gMnSc2	Napoleon
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
šlo	jít	k5eAaImAgNnS	jít
především	především	k9	především
o	o	k7c4	o
oslavu	oslava	k1gFnSc4	oslava
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
osoby	osoba	k1gFnPc4	osoba
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
autorům	autor	k1gMnPc3	autor
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
službách	služba	k1gFnPc6	služba
patřil	patřit	k5eAaImAgMnS	patřit
zakladatel	zakladatel	k1gMnSc1	zakladatel
klasicismu	klasicismus	k1gInSc2	klasicismus
ve	v	k7c6	v
francouzském	francouzský	k2eAgNnSc6d1	francouzské
malířství	malířství	k1gNnSc6	malířství
Jacques-Louis	Jacques-Louis	k1gFnSc1	Jacques-Louis
David	David	k1gMnSc1	David
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jako	jako	k8xC	jako
nadšený	nadšený	k2eAgMnSc1d1	nadšený
čtenář	čtenář	k1gMnSc1	čtenář
antické	antický	k2eAgFnSc2d1	antická
literatury	literatura	k1gFnSc2	literatura
nabyl	nabýt	k5eAaPmAgInS	nabýt
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Napoleonovi	Napoleon	k1gMnSc6	Napoleon
lze	lze	k6eAd1	lze
spatřovat	spatřovat	k5eAaImF	spatřovat
antického	antický	k2eAgMnSc4d1	antický
héroa	héros	k1gMnSc4	héros
<g/>
,	,	kIx,	,
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc7	jeho
nadšeným	nadšený	k2eAgMnSc7d1	nadšený
přívržencem	přívrženec	k1gMnSc7	přívrženec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1804	[number]	k4	1804
jej	on	k3xPp3gMnSc4	on
první	první	k4xOgMnSc1	první
konzul	konzul	k1gMnSc1	konzul
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
svým	svůj	k3xOyFgMnPc3	svůj
prvním	první	k4xOgMnSc6	první
malířem	malíř	k1gMnSc7	malíř
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1808	[number]	k4	1808
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
obraz	obraz	k1gInSc4	obraz
Napoleonova	Napoleonův	k2eAgFnSc1d1	Napoleonova
korunovace	korunovace	k1gFnSc1	korunovace
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
důstojníkem	důstojník	k1gMnSc7	důstojník
čestné	čestný	k2eAgFnSc2d1	čestná
legie	legie	k1gFnSc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
jeho	jeho	k3xOp3gNnPc3	jeho
známým	známý	k2eAgNnPc3d1	známé
dílům	dílo	k1gNnPc3	dílo
patří	patřit	k5eAaImIp3nS	patřit
obrazy	obraz	k1gInPc4	obraz
Bonaparte	bonapart	k1gInSc5	bonapart
překračující	překračující	k2eAgFnSc1d1	překračující
Alpy	Alpy	k1gFnPc1	Alpy
(	(	kIx(	(
<g/>
1800	[number]	k4	1800
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rozdílení	rozdílení	k1gNnSc1	rozdílení
orlů	orel	k1gMnPc2	orel
(	(	kIx(	(
<g/>
1810	[number]	k4	1810
<g/>
)	)	kIx)	)
a	a	k8xC	a
studie	studie	k1gFnSc1	studie
Napoleona	Napoleon	k1gMnSc2	Napoleon
v	v	k7c6	v
pracovně	pracovna	k1gFnSc6	pracovna
v	v	k7c6	v
Tuilerijském	tuilerijský	k2eAgInSc6d1	tuilerijský
paláci	palác	k1gInSc6	palác
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
neméně	málo	k6eNd2	málo
slavným	slavný	k2eAgMnPc3d1	slavný
malířům	malíř	k1gMnPc3	malíř
napoleonského	napoleonský	k2eAgNnSc2d1	napoleonské
období	období	k1gNnSc2	období
náleží	náležet	k5eAaImIp3nS	náležet
Davidův	Davidův	k2eAgMnSc1d1	Davidův
žák	žák	k1gMnSc1	žák
Antoine-Jean	Antoine-Jeana	k1gFnPc2	Antoine-Jeana
Gros	gros	k1gInSc1	gros
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
se	se	k3xPyFc4	se
s	s	k7c7	s
Bonapartem	bonapart	k1gInSc7	bonapart
seznámil	seznámit	k5eAaPmAgMnS	seznámit
v	v	k7c6	v
Milánu	Milán	k1gInSc3	Milán
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
od	od	k7c2	od
generála	generál	k1gMnSc2	generál
Italské	italský	k2eAgFnSc2d1	italská
armády	armáda	k1gFnSc2	armáda
získal	získat	k5eAaPmAgMnS	získat
zakázku	zakázka	k1gFnSc4	zakázka
na	na	k7c4	na
obraz	obraz	k1gInSc4	obraz
Bonaparte	bonapart	k1gInSc5	bonapart
u	u	k7c2	u
Arcole	Arcole	k1gFnSc2	Arcole
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
byl	být	k5eAaImAgMnS	být
vybrán	vybrat	k5eAaPmNgMnS	vybrat
pro	pro	k7c4	pro
zobrazení	zobrazení	k1gNnSc4	zobrazení
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Nazaretu	Nazaret	k1gInSc2	Nazaret
<g/>
,	,	kIx,	,
oslavující	oslavující	k2eAgFnSc6d1	oslavující
generála	generál	k1gMnSc4	generál
Junota	Junota	k1gFnSc1	Junota
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
však	však	k9	však
na	na	k7c4	na
Napoleonovo	Napoleonův	k2eAgNnSc4d1	Napoleonovo
přání	přání	k1gNnSc4	přání
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
a	a	k8xC	a
místo	místo	k7c2	místo
něj	on	k3xPp3gInSc2	on
ztvárnil	ztvárnit	k5eAaPmAgInS	ztvárnit
obraz	obraz	k1gInSc1	obraz
Bonaparte	bonapart	k1gInSc5	bonapart
u	u	k7c2	u
malomocných	malomocný	k1gMnPc2	malomocný
v	v	k7c6	v
Jaffě	Jaffa	k1gFnSc6	Jaffa
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
jeho	jeho	k3xOp3gNnSc4	jeho
pracím	práce	k1gFnPc3	práce
patří	patřit	k5eAaImIp3nS	patřit
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Abukiru	Abukir	k1gInSc2	Abukir
(	(	kIx(	(
<g/>
1806	[number]	k4	1806
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Napoleon	napoleon	k1gInSc1	napoleon
u	u	k7c2	u
pruského	pruský	k2eAgNnSc2d1	pruské
Jílového	Jílové	k1gNnSc2	Jílové
(	(	kIx(	(
<g/>
za	za	k7c4	za
něj	on	k3xPp3gMnSc4	on
byl	být	k5eAaImAgInS	být
dekorován	dekorován	k2eAgInSc1d1	dekorován
řádem	řád	k1gInSc7	řád
čestné	čestný	k2eAgFnSc2d1	čestná
legie	legie	k1gFnSc2	legie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Bonaparte	bonapart	k1gInSc5	bonapart
promlouvá	promlouvat	k5eAaImIp3nS	promlouvat
k	k	k7c3	k
vojsku	vojsko	k1gNnSc3	vojsko
před	před	k7c7	před
bitvou	bitva	k1gFnSc7	bitva
u	u	k7c2	u
pyramid	pyramida	k1gFnPc2	pyramida
<g/>
,	,	kIx,	,
Setkání	setkání	k1gNnSc1	setkání
Napoleona	Napoleon	k1gMnSc2	Napoleon
s	s	k7c7	s
císařem	císař	k1gMnSc7	císař
Františkem	František	k1gMnSc7	František
I.	I.	kA	I.
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Slavkova	Slavkov	k1gInSc2	Slavkov
atd.	atd.	kA	atd.
Uměleckou	umělecký	k2eAgFnSc7d1	umělecká
interpretací	interpretace	k1gFnSc7	interpretace
Napoleona	Napoleon	k1gMnSc2	Napoleon
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc2	jeho
významných	významný	k2eAgMnPc2d1	významný
činů	čin	k1gInPc2	čin
<g/>
,	,	kIx,	,
příslušníků	příslušník	k1gMnPc2	příslušník
klanu	klan	k1gInSc2	klan
Bonapartů	bonapart	k1gInPc2	bonapart
i	i	k8xC	i
vojáků	voják	k1gMnPc2	voják
jeho	jeho	k3xOp3gFnPc2	jeho
armád	armáda	k1gFnPc2	armáda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
celá	celý	k2eAgFnSc1d1	celá
plejáda	plejáda	k1gFnSc1	plejáda
domácích	domácí	k2eAgMnPc2d1	domácí
výtvarníků	výtvarník	k1gMnPc2	výtvarník
nejen	nejen	k6eAd1	nejen
za	za	k7c2	za
éry	éra	k1gFnSc2	éra
císařství	císařství	k1gNnSc2	císařství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
po	po	k7c6	po
císařově	císařův	k2eAgFnSc6d1	císařova
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tyto	tento	k3xDgMnPc4	tento
malíře	malíř	k1gMnPc4	malíř
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Ernest	Ernest	k1gMnSc1	Ernest
Meissonier	Meissonier	k1gMnSc1	Meissonier
<g/>
,	,	kIx,	,
Henri	Henri	k1gNnSc1	Henri
Félix	Félix	k1gInSc1	Félix
Emmanuel	Emmanuel	k1gMnSc1	Emmanuel
Philippoteaux	Philippoteaux	k1gInSc1	Philippoteaux
<g/>
,	,	kIx,	,
Édouard	Édouard	k1gInSc1	Édouard
Detaille	Detaille	k1gFnSc1	Detaille
<g/>
,	,	kIx,	,
Louis-François	Louis-François	k1gFnSc1	Louis-François
Lejeune	Lejeun	k1gInSc5	Lejeun
<g/>
,	,	kIx,	,
Joseph-Louis	Joseph-Louis	k1gInSc1	Joseph-Louis
Hippolyte	Hippolyt	k1gInSc5	Hippolyt
Bellangé	Bellangý	k2eAgFnSc3d1	Bellangý
<g/>
,	,	kIx,	,
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
</s>
<s>
Stejné	stejný	k2eAgInPc1d1	stejný
náměty	námět	k1gInPc1	námět
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
mnohdy	mnohdy	k6eAd1	mnohdy
značně	značně	k6eAd1	značně
nacionalistických	nacionalistický	k2eAgNnPc6d1	nacionalistické
dílech	dílo	k1gNnPc6	dílo
zobrazovala	zobrazovat	k5eAaImAgFnS	zobrazovat
i	i	k9	i
řada	řada	k1gFnSc1	řada
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
svým	svůj	k3xOyFgInSc7	svůj
ostře	ostro	k6eAd1	ostro
kritickým	kritický	k2eAgInSc7d1	kritický
postojem	postoj	k1gInSc7	postoj
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
době	doba	k1gFnSc3	doba
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
odpůrce	odpůrce	k1gMnPc4	odpůrce
klasicismu	klasicismus	k1gInSc2	klasicismus
Španěl	Španěl	k1gMnSc1	Španěl
Francisco	Francisco	k1gMnSc1	Francisco
Goya	Goya	k1gMnSc1	Goya
(	(	kIx(	(
<g/>
Poprava	poprava	k1gFnSc1	poprava
v	v	k7c6	v
La	la	k1gNnSc6	la
Moncloa	Moncloum	k1gNnSc2	Moncloum
<g/>
,	,	kIx,	,
El	Ela	k1gFnPc2	Ela
Dos	Dos	k1gFnSc2	Dos
de	de	k?	de
Mayo	Mayo	k1gMnSc1	Mayo
<g/>
,	,	kIx,	,
grafický	grafický	k2eAgInSc1d1	grafický
cyklus	cyklus	k1gInSc1	cyklus
Los	los	k1gInSc1	los
desastres	desastres	k1gInSc1	desastres
de	de	k?	de
la	la	k1gNnSc1	la
guerra	guerr	k1gInSc2	guerr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dílům	díl	k1gInPc3	díl
ilustrativního	ilustrativní	k2eAgInSc2d1	ilustrativní
charakteru	charakter	k1gInSc2	charakter
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
věnovali	věnovat	k5eAaPmAgMnP	věnovat
například	například	k6eAd1	například
britská	britský	k2eAgFnSc1d1	britská
malířka	malířka	k1gFnSc1	malířka
Elisabeth	Elisabeth	k1gInSc1	Elisabeth
Thompsonová	Thompsonová	k1gFnSc1	Thompsonová
alias	alias	k9	alias
Lady	lady	k1gFnSc1	lady
Butlerová	Butlerová	k1gFnSc1	Butlerová
<g/>
,	,	kIx,	,
Polák	Polák	k1gMnSc1	Polák
Jerzy	Jerza	k1gFnSc2	Jerza
Kossak	Kossak	k1gMnSc1	Kossak
<g/>
,	,	kIx,	,
ukrajinský	ukrajinský	k2eAgMnSc1d1	ukrajinský
rodák	rodák	k1gMnSc1	rodák
Felician	Feliciana	k1gFnPc2	Feliciana
von	von	k1gInSc1	von
Myrbach	Myrbach	k1gInSc4	Myrbach
<g/>
,	,	kIx,	,
Rus	Rus	k1gFnSc4	Rus
s	s	k7c7	s
bavorskými	bavorský	k2eAgMnPc7d1	bavorský
předky	předek	k1gMnPc7	předek
Gottfried	Gottfried	k1gInSc4	Gottfried
Willewalde	Willewald	k1gInSc5	Willewald
aj.	aj.	kA	aj.
Svůj	svůj	k3xOyFgInSc4	svůj
ohlas	ohlas	k1gInSc4	ohlas
zanechal	zanechat	k5eAaPmAgMnS	zanechat
Napoleon	napoleon	k1gInSc4	napoleon
i	i	k9	i
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgInPc4d3	nejvýznamnější
projekty	projekt	k1gInPc4	projekt
vybudované	vybudovaný	k2eAgInPc4d1	vybudovaný
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
oslavu	oslava	k1gFnSc4	oslava
patří	patřit	k5eAaImIp3nS	patřit
zejména	zejména	k9	zejména
Vendômský	Vendômský	k2eAgInSc1d1	Vendômský
sloup	sloup	k1gInSc1	sloup
a	a	k8xC	a
Vítězný	vítězný	k2eAgInSc1d1	vítězný
oblouk	oblouk	k1gInSc1	oblouk
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Prve	prve	k6eAd1	prve
jmenované	jmenovaný	k2eAgNnSc1d1	jmenované
dílo	dílo	k1gNnSc1	dílo
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
vrcholu	vrchol	k1gInSc6	vrchol
nese	nést	k5eAaImIp3nS	nést
Napoleonovu	Napoleonův	k2eAgFnSc4d1	Napoleonova
sochu	socha	k1gFnSc4	socha
a	a	k8xC	a
lze	lze	k6eAd1	lze
jej	on	k3xPp3gMnSc4	on
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
kopii	kopie	k1gFnSc4	kopie
Trajánova	Trajánův	k2eAgInSc2d1	Trajánův
sloupu	sloup	k1gInSc2	sloup
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
Vítězného	vítězný	k2eAgInSc2d1	vítězný
oblouku	oblouk	k1gInSc2	oblouk
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
architekt	architekt	k1gMnSc1	architekt
Jean-François	Jean-François	k1gFnSc2	Jean-François
Chalgrin	Chalgrin	k1gInSc1	Chalgrin
a	a	k8xC	a
nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
skulptury	skulptura	k1gFnPc1	skulptura
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
James	James	k1gMnSc1	James
Pradier	Pradier	k1gMnSc1	Pradier
<g/>
,	,	kIx,	,
François	François	k1gFnSc1	François
Rude	Rude	k1gFnSc1	Rude
<g/>
,	,	kIx,	,
Jean-Pierre	Jean-Pierr	k1gInSc5	Jean-Pierr
Cortot	Cortot	k1gInSc1	Cortot
a	a	k8xC	a
Antoine	Antoin	k1gInSc5	Antoin
Étex	Étex	k1gInSc4	Étex
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
mistrovské	mistrovský	k2eAgNnSc1d1	mistrovské
dílo	dílo	k1gNnSc1	dílo
klasicistické	klasicistický	k2eAgFnSc2d1	klasicistická
architektury	architektura	k1gFnSc2	architektura
je	být	k5eAaImIp3nS	být
pozoruhodné	pozoruhodný	k2eAgNnSc1d1	pozoruhodné
nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
věrnost	věrnost	k1gFnSc4	věrnost
antickým	antický	k2eAgFnPc3d1	antická
formám	forma	k1gFnPc3	forma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
umístění	umístění	k1gNnSc4	umístění
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
dalšími	další	k2eAgNnPc7d1	další
díly	dílo	k1gNnPc7	dílo
nelze	lze	k6eNd1	lze
opomenout	opomenout	k5eAaPmF	opomenout
kupříkladu	kupříkladu	k6eAd1	kupříkladu
sochu	socha	k1gFnSc4	socha
François	François	k1gFnSc2	François
Rudeaua	Rudeau	k1gInSc2	Rudeau
Zmrtvýchvstání	zmrtvýchvstání	k1gNnSc2	zmrtvýchvstání
Napoleonovo	Napoleonův	k2eAgNnSc1d1	Napoleonovo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
francouzského	francouzský	k2eAgMnSc4d1	francouzský
císaře	císař	k1gMnSc4	císař
jako	jako	k8xC	jako
postavu	postava	k1gFnSc4	postava
podobnou	podobný	k2eAgFnSc4d1	podobná
Kristu	Krista	k1gFnSc4	Krista
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skulptura	skulptura	k1gFnSc1	skulptura
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
odhalení	odhalení	k1gNnSc6	odhalení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1847	[number]	k4	1847
upoutala	upoutat	k5eAaPmAgFnS	upoutat
pozornost	pozornost	k1gFnSc1	pozornost
množství	množství	k1gNnSc2	množství
poutníků	poutník	k1gMnPc2	poutník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Napoleon	napoleon	k1gInSc1	napoleon
a	a	k8xC	a
éra	éra	k1gFnSc1	éra
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
zanechaly	zanechat	k5eAaPmAgFnP	zanechat
svůj	svůj	k3xOyFgInSc4	svůj
odraz	odraz	k1gInSc4	odraz
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
jak	jak	k6eAd1	jak
o	o	k7c4	o
díla	dílo	k1gNnPc4	dílo
dějepisná	dějepisný	k2eAgNnPc4d1	dějepisné
a	a	k8xC	a
životopisná	životopisný	k2eAgNnPc4d1	životopisné
<g/>
,	,	kIx,	,
tak	tak	k9	tak
o	o	k7c4	o
memoáry	memoáry	k1gInPc4	memoáry
pamětníků	pamětník	k1gMnPc2	pamětník
<g/>
,	,	kIx,	,
beletrii	beletrie	k1gFnSc4	beletrie
<g/>
,	,	kIx,	,
poezii	poezie	k1gFnSc4	poezie
nebo	nebo	k8xC	nebo
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
francouzské	francouzský	k2eAgMnPc4d1	francouzský
prozaiky	prozaik	k1gMnPc4	prozaik
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zachytili	zachytit	k5eAaPmAgMnP	zachytit
atmosféru	atmosféra	k1gFnSc4	atmosféra
císařství	císařství	k1gNnSc2	císařství
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nP	patřit
například	například	k6eAd1	například
spisovatelé	spisovatel	k1gMnPc1	spisovatel
Stendhal	Stendhal	k1gMnSc1	Stendhal
(	(	kIx(	(
<g/>
Červený	Červený	k1gMnSc1	Červený
a	a	k8xC	a
černý	černý	k2eAgMnSc1d1	černý
<g/>
,	,	kIx,	,
Kartouza	kartouza	k1gFnSc1	kartouza
Parmská	parmský	k2eAgFnSc1d1	parmská
<g/>
,	,	kIx,	,
Lucien	Lucien	k2eAgInSc1d1	Lucien
Leuwen	Leuwen	k1gInSc1	Leuwen
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Victor	Victor	k1gMnSc1	Victor
Hugo	Hugo	k1gMnSc1	Hugo
<g/>
,	,	kIx,	,
Bernard	Bernard	k1gMnSc1	Bernard
Simiot	Simiot	k1gMnSc1	Simiot
(	(	kIx(	(
<g/>
Carbec	Carbec	k1gMnSc1	Carbec
<g/>
,	,	kIx,	,
mon	mon	k?	mon
Empereur	Empereur	k1gMnSc1	Empereur
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Honoré	Honorý	k2eAgFnSc2d1	Honorý
de	de	k?	de
Balzac	Balzac	k1gMnSc1	Balzac
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zahraničním	zahraniční	k2eAgMnPc3d1	zahraniční
autorům	autor	k1gMnPc3	autor
nepochybně	pochybně	k6eNd1	pochybně
náleží	náležet	k5eAaImIp3nS	náležet
Lev	Lev	k1gMnSc1	Lev
Nikolajevič	Nikolajevič	k1gMnSc1	Nikolajevič
Tolstoj	Tolstoj	k1gMnSc1	Tolstoj
(	(	kIx(	(
<g/>
Vojna	vojna	k1gFnSc1	vojna
a	a	k8xC	a
mír	mír	k1gInSc1	mír
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Adam	Adam	k1gMnSc1	Adam
Mickiewicz	Mickiewicz	k1gMnSc1	Mickiewicz
(	(	kIx(	(
<g/>
Pan	Pan	k1gMnSc1	Pan
Tadeáš	Tadeáš	k1gMnSc1	Tadeáš
čili	čili	k8xC	čili
poslední	poslední	k2eAgInSc1d1	poslední
nájezd	nájezd	k1gInSc1	nájezd
na	na	k7c6	na
Litvě	Litva	k1gFnSc6	Litva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tématům	téma	k1gNnPc3	téma
kolem	kolem	k7c2	kolem
Napoleonovy	Napoleonův	k2eAgFnSc2d1	Napoleonova
osoby	osoba	k1gFnSc2	osoba
se	se	k3xPyFc4	se
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
věnuje	věnovat	k5eAaPmIp3nS	věnovat
i	i	k9	i
filmová	filmový	k2eAgFnSc1d1	filmová
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pořady	pořad	k1gInPc4	pořad
jak	jak	k8xS	jak
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
dokumentaristů	dokumentarista	k1gMnPc2	dokumentarista
<g/>
,	,	kIx,	,
tak	tak	k9	tak
dramatiků	dramatik	k1gMnPc2	dramatik
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
filmů	film	k1gInPc2	film
o	o	k7c6	o
francouzském	francouzský	k2eAgInSc6d1	francouzský
císaři	císař	k1gMnSc3	císař
byl	být	k5eAaImAgInS	být
natočen	natočen	k2eAgInSc1d1	natočen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
životopisnou	životopisný	k2eAgFnSc4d1	životopisná
fresku	freska	k1gFnSc4	freska
francouzského	francouzský	k2eAgMnSc2d1	francouzský
režiséra	režisér	k1gMnSc2	režisér
Abela	Abel	k1gMnSc2	Abel
Ganceho	Gance	k1gMnSc2	Gance
s	s	k7c7	s
názvem	název	k1gInSc7	název
Napoleon	Napoleon	k1gMnSc1	Napoleon
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
novější	nový	k2eAgNnPc4d2	novější
kinematografická	kinematografický	k2eAgNnPc4d1	kinematografické
díla	dílo	k1gNnPc4	dílo
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
TV	TV	kA	TV
seriály	seriál	k1gInPc1	seriál
Napoleon	napoleon	k1gInSc1	napoleon
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
lásky	láska	k1gFnSc2	láska
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Napoleon	napoleon	k1gInSc1	napoleon
a	a	k8xC	a
Josefína	Josefína	k1gFnSc1	Josefína
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
dva	dva	k4xCgInPc4	dva
několikadílné	několikadílný	k2eAgInPc4d1	několikadílný
pořady	pořad	k1gInPc4	pořad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
a	a	k8xC	a
2012	[number]	k4	2012
s	s	k7c7	s
názvem	název	k1gInSc7	název
Napoleon	Napoleon	k1gMnSc1	Napoleon
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
životopisným	životopisný	k2eAgInPc3d1	životopisný
filmům	film	k1gInPc3	film
náleží	náležet	k5eAaImIp3nS	náležet
dramata	drama	k1gNnPc4	drama
Monsieur	Monsieura	k1gFnPc2	Monsieura
N.	N.	kA	N.
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Austerlitz	Austerlitz	k1gMnSc1	Austerlitz
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Waterloo	Waterloo	k1gNnSc1	Waterloo
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
dílů	díl	k1gInPc2	díl
série	série	k1gFnPc4	série
Nesmrtelní	smrtelní	k2eNgMnPc1d1	nesmrtelní
válečníci	válečník	k1gMnPc1	válečník
s	s	k7c7	s
podtitulem	podtitul	k1gInSc7	podtitul
Napoleon	Napoleon	k1gMnSc1	Napoleon
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historiografie	historiografie	k1gFnSc2	historiografie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
historického	historický	k2eAgNnSc2d1	historické
bádání	bádání	k1gNnSc2	bádání
lze	lze	k6eAd1	lze
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
Napoleonovou	Napoleonův	k2eAgFnSc7d1	Napoleonova
osobou	osoba	k1gFnSc7	osoba
a	a	k8xC	a
epochou	epocha	k1gFnSc7	epocha
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
několika	několik	k4yIc6	několik
tisících	tisíc	k4xCgInPc6	tisíc
publikovaných	publikovaný	k2eAgInPc6d1	publikovaný
odborných	odborný	k2eAgInPc6d1	odborný
spisech	spis	k1gInPc6	spis
<g/>
.	.	kIx.	.
</s>
<s>
Autoři	autor	k1gMnPc1	autor
těchto	tento	k3xDgFnPc2	tento
monografií	monografie	k1gFnPc2	monografie
se	se	k3xPyFc4	se
ať	ať	k9	ať
už	už	k6eAd1	už
objektivně	objektivně	k6eAd1	objektivně
nebo	nebo	k8xC	nebo
subjektivně	subjektivně	k6eAd1	subjektivně
snaží	snažit	k5eAaImIp3nP	snažit
pochopit	pochopit	k5eAaPmF	pochopit
Napoleonovy	Napoleonův	k2eAgFnPc4d1	Napoleonova
ambice	ambice	k1gFnPc4	ambice
a	a	k8xC	a
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
příčinu	příčina	k1gFnSc4	příčina
jeho	jeho	k3xOp3gInPc2	jeho
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
práce	práce	k1gFnSc1	práce
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
záplavu	záplava	k1gFnSc4	záplava
pamfletů	pamflet	k1gInPc2	pamflet
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
po	po	k7c6	po
restauraci	restaurace	k1gFnSc6	restaurace
Bourbonů	bourbon	k1gInPc2	bourbon
rázu	ráz	k1gInSc2	ráz
značně	značně	k6eAd1	značně
patriotického	patriotický	k2eAgInSc2d1	patriotický
až	až	k8xS	až
glorifikujícího	glorifikující	k2eAgInSc2d1	glorifikující
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
rozsáhlejší	rozsáhlý	k2eAgFnPc4d2	rozsáhlejší
rané	raný	k2eAgFnPc4d1	raná
publikace	publikace	k1gFnPc4	publikace
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
patří	patřit	k5eAaImIp3nP	patřit
dvacetisvazkové	dvacetisvazkový	k2eAgFnPc1d1	dvacetisvazkový
Dějiny	dějiny	k1gFnPc1	dějiny
Konzulátu	konzulát	k1gInSc2	konzulát
a	a	k8xC	a
Císařství	císařství	k1gNnSc1	císařství
(	(	kIx(	(
<g/>
Histoire	Histoir	k1gMnSc5	Histoir
du	du	k?	du
Consulat	Consulat	k1gMnSc3	Consulat
et	et	k?	et
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Empire	empir	k1gInSc5	empir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
velmi	velmi	k6eAd1	velmi
kvalitně	kvalitně	k6eAd1	kvalitně
zpracoval	zpracovat	k5eAaPmAgInS	zpracovat
Adolphe	Adolph	k1gFnSc2	Adolph
Thiers	Thiers	k1gInSc1	Thiers
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
tento	tento	k3xDgMnSc1	tento
autor	autor	k1gMnSc1	autor
prakticky	prakticky	k6eAd1	prakticky
nezabývá	zabývat	k5eNaImIp3nS	zabývat
hospodářskými	hospodářský	k2eAgFnPc7d1	hospodářská
otázkami	otázka	k1gFnPc7	otázka
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
jeho	jeho	k3xOp3gFnPc4	jeho
práce	práce	k1gFnPc4	práce
u	u	k7c2	u
veřejnosti	veřejnost	k1gFnSc2	veřejnost
značný	značný	k2eAgInSc4d1	značný
ohlas	ohlas	k1gInSc4	ohlas
a	a	k8xC	a
díky	díky	k7c3	díky
kvalitnímu	kvalitní	k2eAgInSc3d1	kvalitní
literárnímu	literární	k2eAgInSc3d1	literární
stylu	styl	k1gInSc3	styl
byla	být	k5eAaImAgFnS	být
čtenářsky	čtenářsky	k6eAd1	čtenářsky
nadstandardně	nadstandardně	k6eAd1	nadstandardně
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvním	první	k4xOgMnPc3	první
Napoleonovým	Napoleonův	k2eAgMnPc3d1	Napoleonův
životopiscům	životopisec	k1gMnPc3	životopisec
patřil	patřit	k5eAaImAgInS	patřit
také	také	k6eAd1	také
skotský	skotský	k2eAgMnSc1d1	skotský
prozaik	prozaik	k1gMnSc1	prozaik
sir	sir	k1gMnSc1	sir
Walter	Walter	k1gMnSc1	Walter
Scott	Scott	k1gMnSc1	Scott
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgNnSc1d1	populární
devítisvazkové	devítisvazkový	k2eAgNnSc1d1	devítisvazkový
anglicko-nacionalistické	anglickoacionalistický	k2eAgNnSc1d1	anglicko-nacionalistický
dílo	dílo	k1gNnSc1	dílo
Život	život	k1gInSc1	život
Napoleona	Napoleon	k1gMnSc2	Napoleon
Bonaparta	Bonapart	k1gMnSc2	Bonapart
(	(	kIx(	(
<g/>
Life	Life	k1gInSc1	Life
of	of	k?	of
Napoleon	napoleon	k1gInSc1	napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vůči	vůči	k7c3	vůči
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
značně	značně	k6eAd1	značně
nepřátelské	přátelský	k2eNgFnPc1d1	nepřátelská
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dokumentace	dokumentace	k1gFnSc2	dokumentace
více	hodně	k6eAd2	hodně
než	než	k8xS	než
nedostatečné	dostatečný	k2eNgFnPc1d1	nedostatečná
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
bývalého	bývalý	k2eAgMnSc2d1	bývalý
francouzského	francouzský	k2eAgMnSc2d1	francouzský
císaře	císař	k1gMnSc2	císař
začala	začít	k5eAaPmAgFnS	začít
vycházet	vycházet	k5eAaImF	vycházet
i	i	k9	i
řada	řada	k1gFnSc1	řada
memoárů	memoáry	k1gInPc2	memoáry
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
sepsaly	sepsat	k5eAaPmAgInP	sepsat
jemu	on	k3xPp3gNnSc3	on
blízké	blízký	k2eAgFnPc1d1	blízká
osoby	osoba	k1gFnPc1	osoba
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
francouzská	francouzský	k2eAgFnSc1d1	francouzská
vláda	vláda	k1gFnSc1	vláda
vydala	vydat	k5eAaPmAgFnS	vydat
třicet	třicet	k4xCc4	třicet
dva	dva	k4xCgInPc4	dva
kvartových	kvartový	k2eAgInPc2d1	kvartový
svazků	svazek	k1gInPc2	svazek
dopisů	dopis	k1gInPc2	dopis
<g/>
,	,	kIx,	,
dekretů	dekret	k1gInPc2	dekret
a	a	k8xC	a
příkazů	příkaz	k1gInPc2	příkaz
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
osobně	osobně	k6eAd1	osobně
diktoval	diktovat	k5eAaImAgInS	diktovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
i	i	k8xC	i
Anglii	Anglie	k1gFnSc6	Anglie
začalo	začít	k5eAaPmAgNnS	začít
přibývat	přibývat	k5eAaImF	přibývat
prací	práce	k1gFnSc7	práce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zabývaly	zabývat	k5eAaImAgFnP	zabývat
popisem	popis	k1gInSc7	popis
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
tažení	tažení	k1gNnPc2	tažení
a	a	k8xC	a
bitev	bitva	k1gFnPc2	bitva
<g/>
,	,	kIx,	,
Napoleonovou	Napoleonův	k2eAgFnSc7d1	Napoleonova
diplomacií	diplomacie	k1gFnSc7	diplomacie
<g/>
,	,	kIx,	,
administrativou	administrativa	k1gFnSc7	administrativa
i	i	k8xC	i
jeho	jeho	k3xOp3gNnSc7	jeho
zákonodárstvím	zákonodárství	k1gNnSc7	zákonodárství
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
autorem	autor	k1gMnSc7	autor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
oprostit	oprostit	k5eAaPmF	oprostit
od	od	k7c2	od
napoleonského	napoleonský	k2eAgInSc2d1	napoleonský
mýtu	mýtus	k1gInSc2	mýtus
a	a	k8xC	a
nevědeckého	vědecký	k2eNgNnSc2d1	nevědecké
nahlížení	nahlížení	k1gNnSc2	nahlížení
na	na	k7c4	na
napoleonskou	napoleonský	k2eAgFnSc4d1	napoleonská
epochu	epocha	k1gFnSc4	epocha
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
emigrant	emigrant	k1gMnSc1	emigrant
a	a	k8xC	a
nepřítel	nepřítel	k1gMnSc1	nepřítel
bonapartismu	bonapartismus	k1gInSc2	bonapartismus
plukovník	plukovník	k1gMnSc1	plukovník
Charras	Charras	k1gMnSc1	Charras
v	v	k7c6	v
práci	práce	k1gFnSc6	práce
o	o	k7c6	o
tažení	tažení	k1gNnSc6	tažení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1815	[number]	k4	1815
(	(	kIx(	(
<g/>
Histoire	Histoir	k1gInSc5	Histoir
de	de	k?	de
la	la	k1gNnSc6	la
campagne	campagnout	k5eAaPmIp3nS	campagnout
de	de	k?	de
1815	[number]	k4	1815
<g/>
:	:	kIx,	:
Waterloo	Waterloo	k1gNnSc2	Waterloo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pojetím	pojetí	k1gNnSc7	pojetí
svého	svůj	k3xOyFgNnSc2	svůj
díla	dílo	k1gNnSc2	dílo
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
napoleonské	napoleonský	k2eAgFnSc3d1	napoleonská
legendě	legenda	k1gFnSc3	legenda
snažil	snažit	k5eAaImAgMnS	snažit
vystoupit	vystoupit	k5eAaPmF	vystoupit
i	i	k9	i
Edgar	Edgar	k1gMnSc1	Edgar
Quinet	Quinet	k1gMnSc1	Quinet
a	a	k8xC	a
negativní	negativní	k2eAgInSc1d1	negativní
postoj	postoj	k1gInSc1	postoj
vůči	vůči	k7c3	vůči
romantické	romantický	k2eAgFnSc3d1	romantická
hrdinské	hrdinský	k2eAgFnSc3d1	hrdinská
škole	škola	k1gFnSc3	škola
zaujal	zaujmout	k5eAaPmAgInS	zaujmout
také	také	k9	také
Napoleonovi	Napoleon	k1gMnSc6	Napoleon
nepříznivě	příznivě	k6eNd1	příznivě
nakloněný	nakloněný	k2eAgMnSc1d1	nakloněný
Pierre	Pierr	k1gInSc5	Pierr
Lanfrey	Lanfrey	k1gInPc1	Lanfrey
(	(	kIx(	(
<g/>
Histoire	Histoir	k1gInSc5	Histoir
de	de	k?	de
Napoléon	Napoléon	k1gMnSc1	Napoléon
Ier	Ier	k1gMnSc1	Ier
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
studie	studie	k1gFnSc1	studie
se	se	k3xPyFc4	se
dočkala	dočkat	k5eAaPmAgFnS	dočkat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
deseti	deset	k4xCc2	deset
vydání	vydání	k1gNnPc2	vydání
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
autor	autor	k1gMnSc1	autor
snažil	snažit	k5eAaImAgMnS	snažit
bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
nadšenému	nadšený	k2eAgInSc3d1	nadšený
tónu	tón	k1gInSc3	tón
dosavadní	dosavadní	k2eAgFnSc2d1	dosavadní
Napoleonovy	Napoleonův	k2eAgFnSc2d1	Napoleonova
historiografie	historiografie	k1gFnSc2	historiografie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
proti	proti	k7c3	proti
dílu	dílo	k1gNnSc3	dílo
Thieresovu	Thieresův	k2eAgNnSc3d1	Thieresův
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
zcela	zcela	k6eAd1	zcela
nevědecky	vědecky	k6eNd1	vědecky
zveličil	zveličit	k5eAaPmAgMnS	zveličit
jeho	jeho	k3xOp3gFnSc4	jeho
zápornou	záporný	k2eAgFnSc4d1	záporná
historickou	historický	k2eAgFnSc4d1	historická
úlohu	úloha	k1gFnSc4	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
proudy	proud	k1gInPc1	proud
se	se	k3xPyFc4	se
ve	v	k7c6	v
francouzské	francouzský	k2eAgFnSc6d1	francouzská
historické	historický	k2eAgFnSc6d1	historická
vědě	věda	k1gFnSc6	věda
objevily	objevit	k5eAaPmAgInP	objevit
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Druhého	druhý	k4xOgNnSc2	druhý
císařství	císařství	k1gNnSc2	císařství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
Třetí	třetí	k4xOgFnSc2	třetí
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ještě	ještě	k6eAd1	ještě
hrozila	hrozit	k5eAaImAgFnS	hrozit
restaurace	restaurace	k1gFnSc1	restaurace
Bourbonů	bourbon	k1gInPc2	bourbon
<g/>
,	,	kIx,	,
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
republikánsky	republikánsky	k6eAd1	republikánsky
naladění	naladěný	k2eAgMnPc1d1	naladěný
autoři	autor	k1gMnPc1	autor
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
napoleonské	napoleonský	k2eAgFnSc3d1	napoleonská
legendě	legenda	k1gFnSc3	legenda
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
například	například	k6eAd1	například
historik	historik	k1gMnSc1	historik
Hyppolite	Hyppolit	k1gInSc5	Hyppolit
Taine	Tain	k1gInSc5	Tain
(	(	kIx(	(
<g/>
Les	les	k1gInSc4	les
origines	originesa	k1gFnPc2	originesa
de	de	k?	de
la	la	k1gNnSc1	la
France	Franc	k1gMnSc2	Franc
contemporaine	contemporainout	k5eAaPmIp3nS	contemporainout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokládal	pokládat	k5eAaImAgMnS	pokládat
někdejšího	někdejší	k2eAgMnSc4d1	někdejší
císaře	císař	k1gMnSc4	císař
za	za	k7c4	za
dědice	dědic	k1gMnSc4	dědic
italských	italský	k2eAgInPc2d1	italský
kondotiérů	kondotiér	k1gMnPc2	kondotiér
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
jediným	jediný	k2eAgInSc7d1	jediný
smyslem	smysl	k1gInSc7	smysl
života	život	k1gInSc2	život
byla	být	k5eAaImAgFnS	být
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
přehnaně	přehnaně	k6eAd1	přehnaně
pohrdavým	pohrdavý	k2eAgInSc7d1	pohrdavý
způsobem	způsob	k1gInSc7	způsob
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
veškeré	veškerý	k3xTgInPc4	veškerý
Napoleonovy	Napoleonův	k2eAgInPc4d1	Napoleonův
počiny	počin	k1gInPc4	počin
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
byly	být	k5eAaImAgInP	být
mj.	mj.	kA	mj.
důkazem	důkaz	k1gInSc7	důkaz
nenasytné	nasytný	k2eNgFnSc2d1	nenasytná
touhy	touha	k1gFnSc2	touha
ovládat	ovládat	k5eAaImF	ovládat
všechno	všechen	k3xTgNnSc4	všechen
kolem	kolem	k7c2	kolem
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Osmisvazkové	osmisvazkový	k2eAgNnSc1d1	osmisvazkové
dílo	dílo	k1gNnSc1	dílo
Alberta	Albert	k1gMnSc2	Albert
Sorela	Sorel	k1gMnSc2	Sorel
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Europe	Europ	k1gInSc5	Europ
et	et	k?	et
la	la	k1gNnPc1	la
révolution	révolution	k1gInSc1	révolution
française	française	k1gFnSc1	française
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
začalo	začít	k5eAaPmAgNnS	začít
vycházet	vycházet	k5eAaImF	vycházet
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
pohlíží	pohlížet	k5eAaImIp3nS	pohlížet
na	na	k7c4	na
Napoleonovy	Napoleonův	k2eAgInPc4d1	Napoleonův
útočné	útočný	k2eAgInPc4d1	útočný
války	válek	k1gInPc4	válek
jako	jako	k8xS	jako
na	na	k7c6	na
čistě	čistě	k6eAd1	čistě
obranné	obranný	k2eAgFnSc6d1	obranná
<g/>
.	.	kIx.	.
</s>
<s>
Vlastenecká	vlastenecký	k2eAgFnSc1d1	vlastenecká
premisa	premisa	k1gFnSc1	premisa
Sorelovy	Sorelův	k2eAgFnSc2d1	Sorelův
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
po	po	k7c6	po
prusko-francouzské	pruskorancouzský	k2eAgFnSc6d1	prusko-francouzská
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
na	na	k7c4	na
několik	několik	k4yIc4	několik
dalších	další	k2eAgFnPc2d1	další
desítek	desítka	k1gFnPc2	desítka
let	léto	k1gNnPc2	léto
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
francouzskou	francouzský	k2eAgFnSc4d1	francouzská
historickou	historický	k2eAgFnSc4d1	historická
vědu	věda	k1gFnSc4	věda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pokouší	pokoušet	k5eAaImIp3nS	pokoušet
čtenáře	čtenář	k1gMnSc2	čtenář
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Francie	Francie	k1gFnSc1	Francie
na	na	k7c4	na
nikoho	nikdo	k3yNnSc4	nikdo
neútočí	útočit	k5eNaImIp3nS	útočit
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jen	jen	k9	jen
brání	bránit	k5eAaImIp3nP	bránit
a	a	k8xC	a
hájí	hájit	k5eAaImIp3nP	hájit
své	svůj	k3xOyFgFnPc4	svůj
"	"	kIx"	"
<g/>
přirozené	přirozený	k2eAgFnPc4d1	přirozená
<g/>
"	"	kIx"	"
rýnské	rýnský	k2eAgFnPc4d1	Rýnská
a	a	k8xC	a
alpské	alpský	k2eAgFnPc4d1	alpská
hranice	hranice	k1gFnPc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Nejnadanějším	nadaný	k2eAgMnSc7d3	nejnadanější
Sorelovým	Sorelův	k2eAgMnSc7d1	Sorelův
následníkem	následník	k1gMnSc7	následník
byl	být	k5eAaImAgMnS	být
Albert	Albert	k1gMnSc1	Albert
Vandal	Vandal	k1gMnSc1	Vandal
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Napoleon	Napoleon	k1gMnSc1	Napoleon
a	a	k8xC	a
Alexandr	Alexandr	k1gMnSc1	Alexandr
(	(	kIx(	(
<g/>
Napoléon	Napoléon	k1gMnSc1	Napoléon
et	et	k?	et
Alexandre	Alexandr	k1gInSc5	Alexandr
Ier	Ier	k1gMnSc5	Ier
<g/>
)	)	kIx)	)
konstatuje	konstatovat	k5eAaBmIp3nS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
válce	válka	k1gFnSc6	válka
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
Napoleon	Napoleon	k1gMnSc1	Napoleon
nenese	nést	k5eNaImIp3nS	nést
žádnou	žádný	k3yNgFnSc4	žádný
vinu	vina	k1gFnSc4	vina
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jej	on	k3xPp3gMnSc4	on
hodnotí	hodnotit	k5eAaImIp3nS	hodnotit
i	i	k9	i
ve	v	k7c6	v
dvousvazkové	dvousvazkový	k2eAgFnSc6d1	dvousvazková
publikaci	publikace	k1gFnSc6	publikace
Povýšení	povýšení	k1gNnPc2	povýšení
Bonaparta	Bonaparta	k1gFnSc1	Bonaparta
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
avè	avè	k?	avè
de	de	k?	de
Bonaparte	bonapart	k1gInSc5	bonapart
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
Napoleon	napoleon	k1gInSc1	napoleon
nenese	nést	k5eNaImIp3nS	nést
vinu	vina	k1gFnSc4	vina
na	na	k7c6	na
ničem	nic	k3yNnSc6	nic
<g/>
,	,	kIx,	,
co	co	k9	co
se	se	k3xPyFc4	se
událo	udát	k5eAaPmAgNnS	udát
před	před	k7c7	před
18	[number]	k4	18
<g/>
.	.	kIx.	.
brumairem	brumaire	k1gInSc7	brumaire
a	a	k8xC	a
současně	současně	k6eAd1	současně
ani	ani	k8xC	ani
není	být	k5eNaImIp3nS	být
vinen	vinen	k2eAgMnSc1d1	vinen
zavedením	zavedení	k1gNnSc7	zavedení
despotického	despotický	k2eAgInSc2d1	despotický
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
desetiletí	desetiletí	k1gNnSc6	desetiletí
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
vzniku	vznik	k1gInSc6	vznik
se	se	k3xPyFc4	se
dílo	dílo	k1gNnSc1	dílo
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
osmnácti	osmnáct	k4xCc2	osmnáct
vydání	vydání	k1gNnPc2	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
stovky	stovka	k1gFnPc1	stovka
odborných	odborný	k2eAgFnPc2d1	odborná
publikací	publikace	k1gFnPc2	publikace
v	v	k7c6	v
rozličných	rozličný	k2eAgInPc6d1	rozličný
trendech	trend	k1gInPc6	trend
vznikaly	vznikat	k5eAaImAgFnP	vznikat
i	i	k9	i
po	po	k7c6	po
celé	celá	k1gFnSc6	celá
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
evropská	evropský	k2eAgFnSc1d1	Evropská
napoleonská	napoleonský	k2eAgFnSc1d1	napoleonská
historiografie	historiografie	k1gFnSc1	historiografie
vždy	vždy	k6eAd1	vždy
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
daleko	daleko	k6eAd1	daleko
za	za	k7c7	za
publikační	publikační	k2eAgFnSc7d1	publikační
činností	činnost	k1gFnSc7	činnost
historiků	historik	k1gMnPc2	historik
Francouzských	francouzský	k2eAgMnPc2d1	francouzský
<g/>
.	.	kIx.	.
<g/>
Nejmodernější	moderní	k2eAgFnSc2d3	nejmodernější
odborné	odborný	k2eAgFnSc2d1	odborná
práce	práce	k1gFnSc2	práce
tematicky	tematicky	k6eAd1	tematicky
věnované	věnovaný	k2eAgNnSc1d1	věnované
období	období	k1gNnSc1	období
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
dílo	dílo	k1gNnSc4	dílo
historika	historik	k1gMnSc2	historik
Musée	Musé	k1gFnSc2	Musé
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Armée	Armée	k1gFnSc1	Armée
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
Olega	Oleg	k1gMnSc2	Oleg
Sokolova	Sokolovo	k1gNnSc2	Sokolovo
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Armée	Armé	k1gMnSc2	Armé
de	de	k?	de
Napoléon	Napoléon	k1gMnSc1	Napoléon
<g/>
;	;	kIx,	;
Austerlitz	Austerlitz	k1gMnSc1	Austerlitz
–	–	k?	–
Napoléon	Napoléon	k1gMnSc1	Napoléon
<g/>
,	,	kIx,	,
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Europe	Europ	k1gInSc5	Europ
et	et	k?	et
la	la	k1gNnSc7	la
Russie	Russie	k1gFnSc2	Russie
<g/>
;	;	kIx,	;
Le	Le	k1gMnSc1	Le
Combat	Combat	k1gMnSc1	Combat
de	de	k?	de
deux	deux	k1gInSc1	deux
Empires	Empires	k1gInSc1	Empires
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historika	historik	k1gMnSc2	historik
Alaina	Alain	k1gMnSc2	Alain
Pigearda	Pigeard	k1gMnSc2	Pigeard
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Armée	Armée	k1gInSc1	Armée
de	de	k?	de
Napoléon	Napoléon	k1gInSc1	Napoléon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jeana	Jean	k1gMnSc2	Jean
Tularda	Tulard	k1gMnSc2	Tulard
(	(	kIx(	(
<g/>
Dictionnaire	Dictionnair	k1gInSc5	Dictionnair
Napoléon	Napoléon	k1gNnSc4	Napoléon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
profesora	profesor	k1gMnSc4	profesor
Oxfordské	oxfordský	k2eAgFnSc2d1	Oxfordská
university	universita	k1gFnSc2	universita
Geoffreye	Geoffrey	k1gFnSc2	Geoffrey
Ellise	Ellise	k1gFnSc2	Ellise
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českých	český	k2eAgInPc6d1	český
poměrech	poměr	k1gInPc6	poměr
nejnovější	nový	k2eAgFnSc2d3	nejnovější
odborné	odborný	k2eAgFnSc2d1	odborná
knihy	kniha	k1gFnSc2	kniha
o	o	k7c6	o
napoleonských	napoleonský	k2eAgFnPc6d1	napoleonská
válkách	válka	k1gFnPc6	válka
publikuje	publikovat	k5eAaBmIp3nS	publikovat
historik	historik	k1gMnSc1	historik
Jiří	Jiří	k1gMnSc1	Jiří
Kovařík	Kovařík	k1gMnSc1	Kovařík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Biografie	biografie	k1gFnSc2	biografie
===	===	k?	===
</s>
</p>
<p>
<s>
ARONSON	ARONSON	kA	ARONSON
<g/>
,	,	kIx,	,
Theo	Thea	k1gFnSc5	Thea
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	napoleon	k1gInSc1	napoleon
a	a	k8xC	a
Josefína	Josefína	k1gFnSc1	Josefína
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Nora	Nora	k1gFnSc1	Nora
Doležalová	Doležalová	k1gFnSc1	Doležalová
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Iris	iris	k1gInSc1	iris
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
224	[number]	k4	224
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
901268	[number]	k4	901268
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BARNETT	BARNETT	kA	BARNETT
<g/>
,	,	kIx,	,
Correlli	Correlle	k1gFnSc4	Correlle
<g/>
.	.	kIx.	.
</s>
<s>
Bonaparte	bonapart	k1gInSc5	bonapart
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jan	Jan	k1gMnSc1	Jan
Kozák	Kozák	k1gMnSc1	Kozák
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Jota	jota	k1gFnSc1	jota
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
422	[number]	k4	422
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7217	[number]	k4	7217
<g/>
-	-	kIx~	-
<g/>
352	[number]	k4	352
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BONAPARTE	bonapart	k1gInSc5	bonapart
<g/>
,	,	kIx,	,
Napoleon	napoleon	k1gInSc1	napoleon
<g/>
.	.	kIx.	.
</s>
<s>
Napoleonovy	Napoleonův	k2eAgFnPc1d1	Napoleonova
paměti	paměť	k1gFnPc1	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Julius	Julius	k1gMnSc1	Julius
Albert	Albert	k1gMnSc1	Albert
<g/>
,	,	kIx,	,
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
336	[number]	k4	336
s.	s.	k?	s.
</s>
</p>
<p>
<s>
BOUHLER	BOUHLER	kA	BOUHLER
<g/>
,	,	kIx,	,
Filip	Filip	k1gMnSc1	Filip
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Orbis	orbis	k1gInSc1	orbis
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
378	[number]	k4	378
s.	s.	k?	s.
</s>
</p>
<p>
<s>
CASTELOT	CASTELOT	kA	CASTELOT
<g/>
,	,	kIx,	,
André	André	k1gMnSc1	André
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Jiří	Jiří	k1gMnSc1	Jiří
Žák	Žák	k1gMnSc1	Žák
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
560	[number]	k4	560
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7023	[number]	k4	7023
<g/>
-	-	kIx~	-
<g/>
265	[number]	k4	265
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
ELLIS	ELLIS	kA	ELLIS
<g/>
,	,	kIx,	,
Geoffrey	Geoffrey	k1gInPc7	Geoffrey
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
248	[number]	k4	248
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
405	[number]	k4	405
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KAUFFMANN	KAUFFMANN	kA	KAUFFMANN
<g/>
,	,	kIx,	,
Jean-Paul	Jean-Paul	k1gInSc1	Jean-Paul
<g/>
.	.	kIx.	.
</s>
<s>
Černý	černý	k2eAgInSc1d1	černý
pokoj	pokoj	k1gInSc1	pokoj
v	v	k7c6	v
Longwoodu	Longwood	k1gInSc6	Longwood
:	:	kIx,	:
Napoleonova	Napoleonův	k2eAgFnSc1d1	Napoleonova
cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
Svatou	svatý	k2eAgFnSc4d1	svatá
Helenu	Helena	k1gFnSc4	Helena
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
185	[number]	k4	185
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
1062	[number]	k4	1062
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOVAŘÍK	Kovařík	k1gMnSc1	Kovařík
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Bonaparte	bonapart	k1gInSc5	bonapart
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Akcent	akcent	k1gInSc1	akcent
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
470	[number]	k4	470
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7268	[number]	k4	7268
<g/>
-	-	kIx~	-
<g/>
466	[number]	k4	466
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOVAŘÍK	Kovařík	k1gMnSc1	Kovařík
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Dobyvatel	dobyvatel	k1gMnSc1	dobyvatel
Bonaparte	bonapart	k1gInSc5	bonapart
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Akcent	akcent	k1gInSc1	akcent
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
526	[number]	k4	526
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7268	[number]	k4	7268
<g/>
-	-	kIx~	-
<g/>
571	[number]	k4	571
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOVAŘÍK	Kovařík	k1gMnSc1	Kovařík
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Napoleonova	Napoleonův	k2eAgNnSc2d1	Napoleonovo
tažení	tažení	k1gNnSc2	tažení
I.	I.	kA	I.
–	–	k?	–
Vítězné	vítězný	k2eAgInPc4d1	vítězný
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Akcent	akcent	k1gInSc1	akcent
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
485	[number]	k4	485
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7268	[number]	k4	7268
<g/>
-	-	kIx~	-
<g/>
261	[number]	k4	261
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
KOVAŘÍK	Kovařík	k1gMnSc1	Kovařík
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Napoleonova	Napoleonův	k2eAgNnPc1d1	Napoleonovo
tažení	tažení	k1gNnPc1	tažení
II	II	kA	II
<g/>
.	.	kIx.	.
–	–	k?	–
Nejistá	jistý	k2eNgNnPc4d1	nejisté
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Akcent	akcent	k1gInSc1	akcent
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
668	[number]	k4	668
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7268	[number]	k4	7268
<g/>
-	-	kIx~	-
<g/>
271	[number]	k4	271
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOVAŘÍK	Kovařík	k1gMnSc1	Kovařík
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Napoleonova	Napoleonův	k2eAgNnPc1d1	Napoleonovo
tažení	tažení	k1gNnPc1	tažení
III	III	kA	III
<g/>
.	.	kIx.	.
–	–	k?	–
Proti	proti	k7c3	proti
všem	všecek	k3xTgMnPc3	všecek
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Akcent	akcent	k1gInSc1	akcent
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
593	[number]	k4	593
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7268	[number]	k4	7268
<g/>
-	-	kIx~	-
<g/>
296	[number]	k4	296
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOVAŘÍK	Kovařík	k1gMnSc1	Kovařík
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Napoleonova	Napoleonův	k2eAgNnPc1d1	Napoleonovo
tažení	tažení	k1gNnPc1	tažení
IV	IV	kA	IV
<g/>
.	.	kIx.	.
–	–	k?	–
Pád	Pád	k1gInSc1	Pád
orla	orel	k1gMnSc2	orel
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Akcent	akcent	k1gInSc1	akcent
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
571	[number]	k4	571
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7268	[number]	k4	7268
<g/>
-	-	kIx~	-
<g/>
307	[number]	k4	307
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOVAŘÍK	Kovařík	k1gMnSc1	Kovařík
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
100	[number]	k4	100
dní	den	k1gInPc2	den
z	z	k7c2	z
Elby	Elba	k1gFnSc2	Elba
k	k	k7c3	k
Waterloo	Waterloo	k1gNnSc3	Waterloo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Elka	Elka	k1gFnSc1	Elka
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
358	[number]	k4	358
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902353	[number]	k4	902353
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOVAŘÍK	Kovařík	k1gMnSc1	Kovařík
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
1812	[number]	k4	1812
–	–	k?	–
Napoleonovo	Napoleonův	k2eAgNnSc1d1	Napoleonovo
ruské	ruský	k2eAgNnSc1d1	ruské
tažení	tažení	k1gNnSc1	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Hart	Hart	k1gInSc1	Hart
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
520	[number]	k4	520
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86529	[number]	k4	86529
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOVAŘÍK	Kovařík	k1gMnSc1	Kovařík
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
1809	[number]	k4	1809
–	–	k?	–
Orel	Orel	k1gMnSc1	Orel
proti	proti	k7c3	proti
orlu	orel	k1gMnSc3	orel
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Hart	Hart	k1gInSc1	Hart
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
466	[number]	k4	466
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86529	[number]	k4	86529
<g/>
-	-	kIx~	-
<g/>
37	[number]	k4	37
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOVAŘÍK	Kovařík	k1gMnSc1	Kovařík
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Napoleonova	Napoleonův	k2eAgFnSc1d1	Napoleonova
invaze	invaze	k1gFnSc1	invaze
1807	[number]	k4	1807
<g/>
-	-	kIx~	-
<g/>
1810	[number]	k4	1810
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Akcent	akcent	k1gInSc1	akcent
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
590	[number]	k4	590
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7268	[number]	k4	7268
<g/>
-	-	kIx~	-
<g/>
683	[number]	k4	683
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOVAŘÍK	Kovařík	k1gMnSc1	Kovařík
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Napoleonova	Napoleonův	k2eAgFnSc1d1	Napoleonova
prohra	prohra	k1gFnSc1	prohra
1810	[number]	k4	1810
<g/>
-	-	kIx~	-
<g/>
1814	[number]	k4	1814
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Akcent	akcent	k1gInSc1	akcent
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
592	[number]	k4	592
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7268	[number]	k4	7268
<g/>
-	-	kIx~	-
<g/>
750	[number]	k4	750
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LACOUR-GAYET	LACOUR-GAYET	k?	LACOUR-GAYET
<g/>
,	,	kIx,	,
Georges	Georges	k1gMnSc1	Georges
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
F.	F.	kA	F.
<g/>
Borový	borový	k2eAgMnSc1d1	borový
<g/>
,	,	kIx,	,
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
851	[number]	k4	851
s.	s.	k?	s.
</s>
</p>
<p>
<s>
LUDWIG	LUDWIG	kA	LUDWIG
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Karel	Karel	k1gMnSc1	Karel
Hoch	hoch	k1gMnSc1	hoch
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
592	[number]	k4	592
s.	s.	k?	s.
</s>
</p>
<p>
<s>
MANFRED	MANFRED	kA	MANFRED
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
Z.	Z.	kA	Z.
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
616	[number]	k4	616
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
205	[number]	k4	205
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
129	[number]	k4	129
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TARLE	TARLE	kA	TARLE
<g/>
,	,	kIx,	,
Jevgenij	Jevgenij	k1gMnSc1	Jevgenij
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Eduard	Eduard	k1gMnSc1	Eduard
Kubala	Kubala	k1gMnSc1	Kubala
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
1950	[number]	k4	1950
<g/>
.	.	kIx.	.
450	[number]	k4	450
s.	s.	k?	s.
</s>
</p>
<p>
<s>
WINTR	WINTR	kA	WINTR
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
soupeři	soupeř	k1gMnPc7	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
360	[number]	k4	360
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
349	[number]	k4	349
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WINTR	WINTR	kA	WINTR
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
maršálové	maršál	k1gMnPc1	maršál
a	a	k8xC	a
ministři	ministr	k1gMnPc1	ministr
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
360	[number]	k4	360
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
372	[number]	k4	372
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
WINTR	WINTR	kA	WINTR
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Bonapartové	Bonapartové	k?	Bonapartové
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
160	[number]	k4	160
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
525	[number]	k4	525
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sekundární	sekundární	k2eAgFnSc1d1	sekundární
literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ČORNEJ	ČORNEJ	kA	ČORNEJ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
KUČERA	Kučera	k1gMnSc1	Kučera
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
;	;	kIx,	;
VANÍČEK	Vaníček	k1gMnSc1	Vaníček
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
<g/>
.	.	kIx.	.
</s>
<s>
Evropa	Evropa	k1gFnSc1	Evropa
králů	král	k1gMnPc2	král
a	a	k8xC	a
císařů	císař	k1gMnPc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ivo	Ivo	k1gMnSc1	Ivo
Železný	Železný	k1gMnSc1	Železný
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
410	[number]	k4	410
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
237	[number]	k4	237
<g/>
-	-	kIx~	-
<g/>
3941	[number]	k4	3941
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DUPUY	DUPUY	kA	DUPUY
<g/>
,	,	kIx,	,
R.	R.	kA	R.
Ernest	Ernest	k1gMnSc1	Ernest
<g/>
;	;	kIx,	;
DUPUY	DUPUY	kA	DUPUY
<g/>
,	,	kIx,	,
Trevor	Trevor	k1gMnSc1	Trevor
N.	N.	kA	N.
Historie	historie	k1gFnSc1	historie
vojenství	vojenství	k1gNnSc4	vojenství
:	:	kIx,	:
Harperova	Harperův	k2eAgFnSc1d1	Harperova
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1700	[number]	k4	1700
do	do	k7c2	do
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Perském	perský	k2eAgInSc6d1	perský
zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Forma	forma	k1gFnSc1	forma
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
952	[number]	k4	952
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7213	[number]	k4	7213
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FURET	FURET	kA	FURET
<g/>
,	,	kIx,	,
François	François	k1gFnSc1	François
<g/>
.	.	kIx.	.
</s>
<s>
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Turgota	Turgot	k1gMnSc2	Turgot
k	k	k7c3	k
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
1770	[number]	k4	1770
<g/>
-	-	kIx~	-
<g/>
1814	[number]	k4	1814
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
450	[number]	k4	450
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
452	[number]	k4	452
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HARTMANN	Hartmann	k1gMnSc1	Hartmann
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Claus	Claus	k1gMnSc1	Claus
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Francouzští	francouzský	k2eAgMnPc1d1	francouzský
králové	král	k1gMnPc1	král
a	a	k8xC	a
císaři	císař	k1gMnPc1	císař
v	v	k7c6	v
novověku	novověk	k1gInSc6	novověk
:	:	kIx,	:
od	od	k7c2	od
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XII	XII	kA	XII
<g/>
.	.	kIx.	.
k	k	k7c3	k
Napoleonovi	Napoleon	k1gMnSc3	Napoleon
III	III	kA	III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
1498	[number]	k4	1498
<g/>
-	-	kIx~	-
<g/>
1870	[number]	k4	1870
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
467	[number]	k4	467
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
517	[number]	k4	517
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HRYCH	HRYCH	kA	HRYCH
<g/>
,	,	kIx,	,
Ervín	Ervín	k1gMnSc1	Ervín
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
kniha	kniha	k1gFnSc1	kniha
vojevůdců	vojevůdce	k1gMnPc2	vojevůdce
<g/>
,	,	kIx,	,
bitev	bitva	k1gFnPc2	bitva
a	a	k8xC	a
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Regia	Regia	k1gFnSc1	Regia
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
720	[number]	k4	720
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86367	[number]	k4	86367
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KAUFFMANN	KAUFFMANN	kA	KAUFFMANN
<g/>
,	,	kIx,	,
Jean-Paul	Jean-Paul	k1gInSc1	Jean-Paul
<g/>
.	.	kIx.	.
</s>
<s>
Černý	černý	k2eAgInSc1d1	černý
pokoj	pokoj	k1gInSc1	pokoj
v	v	k7c6	v
Longwoodu	Longwood	k1gInSc6	Longwood
:	:	kIx,	:
Napoleonova	Napoleonův	k2eAgFnSc1d1	Napoleonova
cesta	cesta	k1gFnSc1	cesta
na	na	k7c4	na
Svatou	svatý	k2eAgFnSc4d1	svatá
Helenu	Helena	k1gFnSc4	Helena
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
185	[number]	k4	185
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
1062	[number]	k4	1062
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
POLIŠENSKÝ	POLIŠENSKÝ	kA	POLIŠENSKÝ
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Napoleon	napoleon	k1gInSc1	napoleon
a	a	k8xC	a
srdce	srdce	k1gNnSc1	srdce
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
280	[number]	k4	280
s.	s.	k?	s.
</s>
</p>
<p>
<s>
POPKIN	POPKIN	kA	POPKIN
<g/>
,	,	kIx,	,
Jeremy	Jerem	k1gInPc7	Jerem
D.	D.	kA	D.
A	a	k8xC	a
Concise	Concise	k1gFnSc1	Concise
History	Histor	k1gInPc1	Histor
of	of	k?	of
the	the	k?	the
Haitian	Haitian	k1gInSc1	Haitian
Revolution	Revolution	k1gInSc1	Revolution
<g/>
.	.	kIx.	.
</s>
<s>
Chichester	Chichester	k1gInSc1	Chichester
<g/>
,	,	kIx,	,
West	West	k2eAgInSc1d1	West
Sussex	Sussex	k1gInSc1	Sussex
<g/>
:	:	kIx,	:
Wiley-Blackwell	Wiley-Blackwell	k1gInSc1	Wiley-Blackwell
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
4051	[number]	k4	4051
<g/>
-	-	kIx~	-
<g/>
9820	[number]	k4	9820
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Atentát	atentát	k1gInSc1	atentát
na	na	k7c4	na
Napoleona	Napoleon	k1gMnSc4	Napoleon
Bonaparta	Bonapart	k1gMnSc4	Bonapart
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Napoleon	napoleon	k1gInSc1	napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Napoleon	napoleon	k1gInSc1	napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Autor	autor	k1gMnSc1	autor
Napoléon	Napoléon	k1gMnSc1	Napoléon
Bonaparte	bonapart	k1gInSc5	bonapart
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Napoleon	napoleon	k1gInSc1	napoleon
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Napoleon	napoleon	k1gInSc1	napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Napoleon	napoleon	k1gInSc1	napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
</s>
</p>
<p>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
napoleonská	napoleonský	k2eAgFnSc1d1	napoleonská
společnost	společnost	k1gFnSc1	společnost
</s>
</p>
<p>
<s>
Třináctá	třináctý	k4xOgFnSc1	třináctý
komnata	komnata	k1gFnSc1	komnata
Napoleonova	Napoleonův	k2eAgFnSc1d1	Napoleonova
<g/>
:	:	kIx,	:
Obraz	obraz	k1gInSc1	obraz
Napoleona	Napoleon	k1gMnSc2	Napoleon
Bonaparta	Bonapart	k1gMnSc2	Bonapart
v	v	k7c6	v
mobiliárních	mobiliární	k2eAgInPc6d1	mobiliární
fondech	fond	k1gInPc6	fond
Národního	národní	k2eAgInSc2d1	národní
památkového	památkový	k2eAgInSc2d1	památkový
ústavu	ústav	k1gInSc2	ústav
</s>
</p>
