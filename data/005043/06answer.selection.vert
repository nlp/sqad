<s>
Napoleon	Napoleon	k1gMnSc1
Bonaparte	bonapart	k1gInSc5
(	(	kIx(
<g/>
rodným	rodný	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Napoleone	Napoleon	k1gMnSc5
di	di	k?
Buonaparte	Buonapart	k1gInSc5
<g/>
)	)	kIx)
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgInS
v	v	k7c6
úterý	úterý	k1gNnSc6
15	[number]	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1769	[number]	k4
v	v	k7c6
korsickém	korsický	k2eAgNnSc6d1
městě	město	k1gNnSc6
Ajaccio	Ajaccio	k6eAd1
jako	jako	k8xS
druhý	druhý	k4xOgMnSc1
syn	syn	k1gMnSc1
nepříliš	příliš	k6eNd1
zámožného	zámožný	k2eAgMnSc4d1
příslušníka	příslušník	k1gMnSc4
úřednické	úřednický	k2eAgFnSc2d1
šlechty	šlechta	k1gFnSc2
advokáta	advokát	k1gMnSc2
Carla	Carl	k1gMnSc2
Buonaparta	Buonapart	k1gMnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
ženy	žena	k1gFnSc2
Laetitie	Laetitie	k1gFnSc2
roz	roz	k?
<g/>
.	.	kIx.
</s>