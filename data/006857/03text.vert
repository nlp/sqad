<s>
Defrosted	Defrosted	k1gInSc1	Defrosted
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgNnSc4	druhý
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
psychedelic	psychedelice	k1gFnPc2	psychedelice
rockové	rockový	k2eAgFnSc2d1	rocková
skupiny	skupina	k1gFnSc2	skupina
Frijid	Frijida	k1gFnPc2	Frijida
Pink	pink	k2eAgFnPc4d1	pink
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgFnPc4d1	vydaná
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
u	u	k7c2	u
Parrot	Parrota	k1gFnPc2	Parrota
Records	Recordsa	k1gFnPc2	Recordsa
<g/>
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
<g/>
Black	Black	k1gInSc1	Black
Lace	laka	k1gFnSc3	laka
<g/>
"	"	kIx"	"
-	-	kIx~	-
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
10	[number]	k4	10
"	"	kIx"	"
<g/>
Sing	Sing	k1gInSc1	Sing
A	a	k8xC	a
Song	song	k1gInSc1	song
For	forum	k1gNnPc2	forum
Freedom	Freedom	k1gInSc1	Freedom
<g/>
"	"	kIx"	"
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
<g />
.	.	kIx.	.
</s>
<s>
0	[number]	k4	0
"	"	kIx"	"
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
ll	ll	k?	ll
Never	Never	k1gInSc1	Never
Be	Be	k1gFnSc2	Be
Lonely	Lonela	k1gFnSc2	Lonela
<g/>
"	"	kIx"	"
-	-	kIx~	-
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
"	"	kIx"	"
<g/>
Bye	Bye	k1gFnSc1	Bye
Bye	Bye	k1gFnSc1	Bye
Blues	blues	k1gFnSc1	blues
<g/>
"	"	kIx"	"
-	-	kIx~	-
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
56	[number]	k4	56
"	"	kIx"	"
<g/>
Pain	Pain	k1gMnSc1	Pain
In	In	k1gMnSc1	In
My	my	k3xPp1nPc1	my
Heart	Heart	k1gInSc4	Heart
<g/>
"	"	kIx"	"
-	-	kIx~	-
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
19	[number]	k4	19
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Sloony	Sloona	k1gFnPc1	Sloona
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
instrumental	instrumental	k1gMnSc1	instrumental
<g/>
)	)	kIx)	)
-	-	kIx~	-
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
36	[number]	k4	36
"	"	kIx"	"
<g/>
I	I	kA	I
<g/>
'	'	kIx"	'
<g/>
m	m	kA	m
Movin	Movin	k1gMnSc1	Movin
<g/>
'	'	kIx"	'
<g/>
"	"	kIx"	"
-	-	kIx~	-
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
53	[number]	k4	53
"	"	kIx"	"
<g/>
I	i	k9	i
Haven	Haven	k1gInSc1	Haven
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Got	Got	k1gFnSc1	Got
The	The	k1gFnSc1	The
Time	Time	k1gFnSc1	Time
<g/>
"	"	kIx"	"
-	-	kIx~	-
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
<g />
.	.	kIx.	.
</s>
<s>
21	[number]	k4	21
"	"	kIx"	"
<g/>
We	We	k1gFnSc2	We
<g/>
'	'	kIx"	'
<g/>
re	re	k9	re
Gonna	Gonen	k2eAgFnSc1d1	Gonna
Be	Be	k1gFnSc1	Be
There	Ther	k1gInSc5	Ther
<g/>
"	"	kIx"	"
-	-	kIx~	-
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
28	[number]	k4	28
"	"	kIx"	"
<g/>
Shorty	Shorta	k1gFnSc2	Shorta
Kline	klinout	k5eAaImIp3nS	klinout
<g/>
"	"	kIx"	"
-	-	kIx~	-
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
19	[number]	k4	19
"	"	kIx"	"
<g/>
I	i	k9	i
Love	lov	k1gInSc5	lov
Her	hra	k1gFnPc2	hra
<g/>
"	"	kIx"	"
-	-	kIx~	-
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
"	"	kIx"	"
<g/>
Lost	Lost	k2eAgInSc1d1	Lost
Son	son	k1gInSc1	son
<g/>
"	"	kIx"	"
-	-	kIx~	-
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
45	[number]	k4	45
Kelly	Kella	k1gFnSc2	Kella
Green	Grena	k1gFnPc2	Grena
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
cowbell	cowbell	k1gMnSc1	cowbell
Gary	Gara	k1gFnSc2	Gara
Ray	Ray	k1gMnSc1	Ray
Thompson	Thompson	k1gMnSc1	Thompson
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
Tom	Tom	k1gMnSc1	Tom
Beaudry	Beaudr	k1gInPc1	Beaudr
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
Richard	Richard	k1gMnSc1	Richard
Stevers	Stevers	k1gInSc1	Stevers
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
,	,	kIx,	,
tympani	tympan	k1gMnPc1	tympan
Larry	Larra	k1gFnSc2	Larra
Zelanka	Zelanka	k1gFnSc1	Zelanka
-	-	kIx~	-
klávesy	klávesa	k1gFnSc2	klávesa
</s>
