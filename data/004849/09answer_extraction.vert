tvorba	tvorba	k1gFnSc1
se	se	k3xPyFc4
vyznačuje	vyznačovat	k5eAaImIp3nS
opakujícími	opakující	k2eAgInPc7d1
se	se	k3xPyFc4
tématy	téma	k1gNnPc7
<g/>
,	,	kIx,
jako	jako	k8xC
vzájemný	vzájemný	k2eAgInSc1d1
vztah	vztah	k1gInSc1
člověka	člověk	k1gMnSc2
<g/>
,	,	kIx,
přírody	příroda	k1gFnSc2
a	a	k8xC
techniky	technika	k1gFnSc2
<g/>
,	,	kIx,
obtížnost	obtížnost	k1gFnSc1
jednání	jednání	k1gNnSc2
podle	podle	k7c2
pacifistické	pacifistický	k2eAgFnSc2d1
etiky	etika	k1gFnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
pouť	pouť	k1gFnSc1
mladých	mladý	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
za	za	k7c7
nezávislostí	nezávislost	k1gFnSc7
a	a	k8xC
vlastní	vlastní	k2eAgFnSc7d1
identitou	identita	k1gFnSc7
