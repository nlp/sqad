<s>
Druhý	druhý	k4xOgInSc1	druhý
největší	veliký	k2eAgInSc1d3	veliký
měsíc	měsíc	k1gInSc1	měsíc
Saturnu	Saturn	k1gInSc2	Saturn
je	být	k5eAaImIp3nS	být
Rhea	Rhea	k1gFnSc1	Rhea
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
známý	známý	k2eAgInSc4d1	známý
měsíc	měsíc	k1gInSc4	měsíc
ve	v	k7c6	v
sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
bez	bez	k7c2	bez
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
