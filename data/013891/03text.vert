<s>
Muckovské	Muckovský	k2eAgInPc1d1
vápencové	vápencový	k2eAgInPc1d1
lomy	lom	k1gInPc1
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuPřírodní	infoboxuPřírodní	k2eAgFnSc3d1
památkaMuckovské	památkaMuckovský	k2eAgFnPc4d1
vápencové	vápencový	k2eAgFnPc4d1
lomyIUCN	lomyIUCN	k?
kategorie	kategorie	k1gFnPc4
IV	IV	kA
(	(	kIx(
<g/>
Oblast	oblast	k1gFnSc1
výskytu	výskyt	k1gInSc2
druhu	druh	k1gInSc2
<g/>
)	)	kIx)
LomZákladní	LomZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1990	#num#	k4
Vyhlásil	vyhlásit	k5eAaPmAgMnS
</s>
<s>
Okresní	okresní	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
výbor	výbor	k1gInSc1
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
Nadm	Nadm	k1gInSc1
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
804	#num#	k4
<g/>
–	–	k?
<g/>
824	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
3,33	3,33	k4
ha	ha	kA
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Okres	okres	k1gInSc1
</s>
<s>
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
Umístění	umístění	k1gNnSc1
</s>
<s>
Černá	Černá	k1gFnSc1
v	v	k7c6
Pošumaví	Pošumaví	k1gNnSc6
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
44	#num#	k4
<g/>
′	′	k?
<g/>
16,76	16,76	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
8	#num#	k4
<g/>
′	′	k?
<g/>
35,92	35,92	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Muckovské	Muckovský	k2eAgInPc1d1
vápencové	vápencový	k2eAgInPc1d1
lomy	lom	k1gInPc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Kód	kód	k1gInSc1
</s>
<s>
1284	#num#	k4
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1
památky	památka	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Muckovské	Muckovský	k2eAgInPc1d1
vápencové	vápencový	k2eAgInPc1d1
lomy	lom	k1gInPc1
je	být	k5eAaImIp3nS
přírodní	přírodní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
ev.	ev.	k?
č.	č.	k?
1284	#num#	k4
poblíž	poblíž	k7c2
obce	obec	k1gFnSc2
Černá	černý	k2eAgFnSc1d1
v	v	k7c6
Pošumaví	Pošumaví	k1gNnSc6
v	v	k7c6
okrese	okres	k1gInSc6
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Chráněné	chráněný	k2eAgNnSc1d1
území	území	k1gNnSc1
spravuje	spravovat	k5eAaImIp3nS
Krajský	krajský	k2eAgInSc4d1
úřad	úřad	k1gInSc4
Jihočeského	jihočeský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Předmět	předmět	k1gInSc1
ochrany	ochrana	k1gFnSc2
</s>
<s>
Předmětem	předmět	k1gInSc7
ochrany	ochrana	k1gFnSc2
jsou	být	k5eAaImIp3nP
vápencové	vápencový	k2eAgInPc1d1
jámové	jámový	k2eAgInPc1d1
lomy	lom	k1gInPc1
po	po	k7c6
neobvyklé	obvyklý	k2eNgFnSc6d1
hlubinné	hlubinný	k2eAgFnSc6d1
těžbě	těžba	k1gFnSc6
s	s	k7c7
působivými	působivý	k2eAgInPc7d1
monumenty	monument	k1gInPc7
skalních	skalní	k2eAgInPc2d1
pilířů	pilíř	k1gInPc2
<g/>
,	,	kIx,
studijní	studijní	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
významných	významný	k2eAgInPc2d1
geologických	geologický	k2eAgInPc2d1
fenoménů	fenomén	k1gInPc2
<g/>
,	,	kIx,
zimoviště	zimoviště	k1gNnSc1
a	a	k8xC
shromaždiště	shromaždiště	k1gNnSc1
devíti	devět	k4xCc2
druhů	druh	k1gInPc2
netopýrů	netopýr	k1gMnPc2
s	s	k7c7
dlouholetým	dlouholetý	k2eAgInSc7d1
výzkumem	výzkum	k1gInSc7
<g/>
,	,	kIx,
botanická	botanický	k2eAgFnSc1d1
lokalita	lokalita	k1gFnSc1
s	s	k7c7
chráněnými	chráněný	k2eAgInPc7d1
druhy	druh	k1gInPc7
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Celkový	celkový	k2eAgInSc1d1
pohled	pohled	k1gInSc1
</s>
<s>
Zvonek	zvonek	k1gInSc1
</s>
<s>
Pilíře	pilíř	k1gInPc1
</s>
<s>
Přístup	přístup	k1gInSc1
k	k	k7c3
lomu	lom	k1gInSc3
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Otevřená	otevřený	k2eAgFnSc1d1
data	datum	k1gNnSc2
AOPK	AOPK	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
↑	↑	k?
PP	PP	kA
Muckovské	Muckovský	k2eAgInPc4d1
vápencové	vápencový	k2eAgInPc4d1
lomy	lom	k1gInPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
AOPK	AOPK	kA
ČR	ČR	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
v	v	k7c6
okrese	okres	k1gInSc6
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ALBRECHT	Albrecht	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
a	a	k8xC
kolektiv	kolektiv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Českobudějovicko	Českobudějovicko	k1gNnSc1
v	v	k7c6
<g/>
:	:	kIx,
Mackovčin	Mackovčin	k2eAgMnSc1d1
<g/>
,	,	kIx,
P.	P.	kA
a	a	k8xC
Sedláček	Sedláček	k1gMnSc1
<g/>
,	,	kIx,
M.	M.	kA
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Chráněná	chráněný	k2eAgNnPc4d1
území	území	k1gNnPc4
ČR	ČR	kA
<g/>
,	,	kIx,
svazek	svazek	k1gInSc1
VIII	VIII	kA
<g/>
..	..	k?
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Agentura	agentura	k1gFnSc1
ochrany	ochrana	k1gFnSc2
přírody	příroda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
ČR	ČR	kA
a	a	k8xC
EkoCentrum	EkoCentrum	k1gNnSc1
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
807	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86064	#num#	k4
<g/>
-	-	kIx~
<g/>
65	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Muckovské	Muckovský	k2eAgInPc1d1
vápencové	vápencový	k2eAgInPc1d1
lomy	lom	k1gInPc1
<g/>
,	,	kIx,
s.	s.	k?
182	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Muckovské	Muckovský	k2eAgNnSc1d1
vápencové	vápencový	k2eAgInPc1d1
lomy	lom	k1gInPc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Plán	plán	k1gInSc1
péče	péče	k1gFnSc2
pro	pro	k7c4
přírodní	přírodní	k2eAgFnSc4d1
památku	památka	k1gFnSc4
Muckovské	Muckovský	k2eAgInPc4d1
vápencové	vápencový	k2eAgInPc4d1
lomy	lom	k1gInPc4
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
Národní	národní	k2eAgInSc1d1
parky	park	k1gInPc4
</s>
<s>
Šumava	Šumava	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc1d1
parky	park	k1gInPc1
</s>
<s>
Novohradské	novohradský	k2eAgFnPc1d1
hory	hora	k1gFnPc1
•	•	k?
Poluška	Poluška	k1gFnSc1
•	•	k?
Soběnovská	Soběnovský	k2eAgFnSc1d1
vrchovina	vrchovina	k1gFnSc1
•	•	k?
Vyšebrodsko	Vyšebrodsko	k1gNnSc4
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
Blanský	blanský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Šumava	Šumava	k1gFnSc1
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnPc4d1
rezervace	rezervace	k1gFnPc4
</s>
<s>
Čertova	čertův	k2eAgFnSc1d1
stěna-Luč	stěna-Lučet	k5eAaPmRp2nS
•	•	k?
Vyšenské	Vyšenský	k2eAgInPc4d1
kopce	kopec	k1gInPc4
•	•	k?
Žofínský	žofínský	k2eAgInSc1d1
prales	prales	k1gInSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Olšina	olšina	k1gFnSc1
Přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Bořinka	bořinka	k1gFnSc1
•	•	k?
Český	český	k2eAgInSc1d1
Jílovec	jílovec	k1gInSc1
•	•	k?
Dívčí	dívčí	k2eAgInSc1d1
kámen	kámen	k1gInSc1
•	•	k?
Holubovské	Holubovské	k2eAgInSc2d1
hadce	hadec	k1gInSc2
•	•	k?
Jaronínská	Jaronínský	k2eAgFnSc1d1
bučina	bučina	k1gFnSc1
•	•	k?
Ježová	Ježová	k1gFnSc1
•	•	k?
Kleť	Kleť	k1gFnSc1
•	•	k?
Kozí	kozí	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
Kyselovský	Kyselovský	k2eAgInSc1d1
les	les	k1gInSc1
•	•	k?
Malá	malý	k2eAgFnSc1d1
skála	skála	k1gFnSc1
•	•	k?
Na	na	k7c6
Mokřinách	mokřina	k1gFnPc6
•	•	k?
Niva	niva	k1gFnSc1
Horského	Horského	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
Niva	niva	k1gFnSc1
Horského	Horského	k2eAgInSc2d1
potoka	potok	k1gInSc2
II	II	kA
•	•	k?
Olšov	Olšov	k1gInSc1
•	•	k?
Otov	Otov	k1gInSc1
•	•	k?
Otovský	Otovský	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Pivonické	Pivonický	k2eAgFnSc2d1
skály	skála	k1gFnSc2
•	•	k?
Pláničský	Pláničský	k2eAgInSc4d1
rybník	rybník	k1gInSc4
•	•	k?
Pod	pod	k7c7
Borkovou	Borková	k1gFnSc7
•	•	k?
Ptačí	ptačí	k2eAgFnSc1d1
stěna	stěna	k1gFnSc1
•	•	k?
Rapotická	Rapotický	k2eAgFnSc1d1
březina	březina	k1gFnSc1
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc2
Borková	Borková	k1gFnSc1
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc1
Kapličky	kaplička	k1gFnSc2
•	•	k?
Rožnov	Rožnov	k1gInSc1
•	•	k?
Světlá	světlat	k5eAaImIp3nS
•	•	k?
Ševcova	Ševcův	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Vysoký	vysoký	k2eAgInSc1d1
kámen	kámen	k1gInSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Besednické	besednický	k2eAgInPc1d1
vltavíny	vltavín	k1gInPc1
•	•	k?
Cvičák	cvičák	k1gInSc1
•	•	k?
Házlův	Házlův	k2eAgInSc1d1
kříž	kříž	k1gInSc1
•	•	k?
Hejdlovský	Hejdlovský	k2eAgInSc1d1
potok	potok	k1gInSc1
•	•	k?
Horní	horní	k2eAgInSc1d1
luka	luka	k1gNnPc1
•	•	k?
Horní	horní	k2eAgFnSc1d1
Malše	Malše	k1gFnSc1
•	•	k?
Jasánky	jasánek	k1gInPc4
•	•	k?
Kalamandra	kalamandra	k1gFnSc1
•	•	k?
Kotlina	kotlina	k1gFnSc1
pod	pod	k7c7
Pláničským	Pláničský	k2eAgInSc7d1
rybníkem	rybník	k1gInSc7
•	•	k?
Meandry	meandr	k1gInPc4
Chvalšinského	Chvalšinský	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
Medvědí	medvědí	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Mokřad	mokřad	k1gInSc1
u	u	k7c2
Borského	Borského	k2eAgInSc2d1
rybníka	rybník	k1gInSc2
•	•	k?
Muckovské	Muckovský	k2eAgInPc4d1
vápencové	vápencový	k2eAgInPc4d1
lomy	lom	k1gInPc4
•	•	k?
Multerberské	Multerberský	k2eAgNnSc4d1
rašeliniště	rašeliniště	k1gNnSc4
•	•	k?
Myslivna	myslivna	k1gFnSc1
•	•	k?
Na	na	k7c6
Stráži	stráž	k1gFnSc6
•	•	k?
Olšina	olšina	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c6
Novolhotském	Novolhotský	k2eAgInSc6d1
lese	les	k1gInSc6
•	•	k?
Pestřice	Pestřice	k1gFnSc2
•	•	k?
Pohořské	Pohořský	k2eAgNnSc4d1
rašeliniště	rašeliniště	k1gNnSc4
•	•	k?
Prameniště	prameniště	k1gNnSc2
Hamerského	hamerský	k2eAgInSc2d1
potoka	potok	k1gInSc2
u	u	k7c2
Zvonkové	zvonkový	k2eAgFnSc2d1
•	•	k?
Prameniště	prameniště	k1gNnSc4
Pohořského	Pohořský	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
Provázková	provázkový	k2eAgFnSc1d1
louka	louka	k1gFnSc1
•	•	k?
Račínská	Račínský	k2eAgNnPc4d1
prameniště	prameniště	k1gNnSc4
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc1
Bobovec	Bobovec	k1gMnSc1
•	•	k?
Rašeliniště	rašeliniště	k1gNnSc2
Kyselov	Kyselovo	k1gNnPc2
•	•	k?
Slavkovické	Slavkovický	k2eAgFnSc2d1
louky	louka	k1gFnSc2
•	•	k?
Slavkovský	slavkovský	k2eAgInSc1d1
chlumek	chlumek	k1gInSc1
•	•	k?
Spáleniště	spáleniště	k1gNnSc2
•	•	k?
Stodůlecký	Stodůlecký	k2eAgInSc4d1
vrch	vrch	k1gInSc4
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Kříž	Kříž	k1gMnSc1
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Tomáš	Tomáš	k1gMnSc1
•	•	k?
Šimečkova	Šimečkův	k2eAgFnSc1d1
stráň	stráň	k1gFnSc1
•	•	k?
U	u	k7c2
tří	tři	k4xCgInPc2
můstků	můstek	k1gInPc2
•	•	k?
Uhlířský	uhlířský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Úval	úval	k1gInSc1
Zvonková	zvonkový	k2eAgFnSc1d1
•	•	k?
Velké	velký	k2eAgNnSc4d1
bahno	bahno	k1gNnSc4
•	•	k?
Vltava	Vltava	k1gFnSc1
u	u	k7c2
Blanského	blanský	k2eAgInSc2d1
lesa	les	k1gInSc2
•	•	k?
Výří	výří	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Žestov	Žestov	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Příroda	příroda	k1gFnSc1
</s>
