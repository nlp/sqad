<p>
<s>
Triton	triton	k1gMnSc1	triton
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
Neptun	Neptun	k1gInSc1	Neptun
I	i	k9	i
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgMnSc1d3	veliký
z	z	k7c2	z
měsíců	měsíc	k1gInPc2	měsíc
planety	planeta	k1gFnSc2	planeta
Neptun	Neptun	k1gMnSc1	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
objeven	objevit	k5eAaPmNgInS	objevit
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1846	[number]	k4	1846
britským	britský	k2eAgMnSc7d1	britský
astronomem	astronom	k1gMnSc7	astronom
Williamem	William	k1gInSc7	William
Lassellem	Lassell	k1gMnSc7	Lassell
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jediný	jediný	k2eAgInSc1d1	jediný
známý	známý	k2eAgInSc1d1	známý
velký	velký	k2eAgInSc1d1	velký
měsíc	měsíc	k1gInSc1	měsíc
ve	v	k7c6	v
Sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
s	s	k7c7	s
retrográdním	retrográdní	k2eAgInSc7d1	retrográdní
pohybem	pohyb	k1gInSc7	pohyb
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
obíhá	obíhat	k5eAaImIp3nS	obíhat
v	v	k7c6	v
protisměru	protisměr	k1gInSc6	protisměr
rotace	rotace	k1gFnSc2	rotace
své	svůj	k3xOyFgFnSc2	svůj
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
2700	[number]	k4	2700
km	km	kA	km
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
sedmého	sedmý	k4xOgInSc2	sedmý
největšího	veliký	k2eAgInSc2d3	veliký
měsíce	měsíc	k1gInSc2	měsíc
ve	v	k7c6	v
Sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
retrográdní	retrográdní	k2eAgFnSc3d1	retrográdní
dráze	dráha	k1gFnSc3	dráha
a	a	k8xC	a
složení	složení	k1gNnSc3	složení
podobnému	podobný	k2eAgNnSc3d1	podobné
Plutu	Pluto	k1gNnSc3	Pluto
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
%	%	kIx~	%
Tritonu	triton	k1gInSc2	triton
tvoří	tvořit	k5eAaImIp3nS	tvořit
led	led	k1gInSc1	led
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
povrch	povrch	k1gInSc1	povrch
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
zmrzlého	zmrzlý	k2eAgInSc2d1	zmrzlý
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
vrstvy	vrstva	k1gFnSc2	vrstva
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zřejmě	zřejmě	k6eAd1	zřejmě
skrývá	skrývat	k5eAaImIp3nS	skrývat
pevné	pevný	k2eAgNnSc1d1	pevné
jádro	jádro	k1gNnSc1	jádro
z	z	k7c2	z
hornin	hornina	k1gFnPc2	hornina
a	a	k8xC	a
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
tvoří	tvořit	k5eAaImIp3nS	tvořit
až	až	k9	až
dvě	dva	k4xCgFnPc4	dva
třetiny	třetina	k1gFnPc4	třetina
jeho	jeho	k3xOp3gFnSc2	jeho
celkové	celkový	k2eAgFnSc2d1	celková
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hustota	hustota	k1gFnSc1	hustota
Tritonu	triton	k1gInSc2	triton
je	být	k5eAaImIp3nS	být
2,061	[number]	k4	2,061
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm	cm	kA	cm
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
Triton	triton	k1gInSc1	triton
je	být	k5eAaImIp3nS	být
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
měsíců	měsíc	k1gInPc2	měsíc
ve	v	k7c6	v
Sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
geologicky	geologicky	k6eAd1	geologicky
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
relativně	relativně	k6eAd1	relativně
mladý	mladý	k2eAgInSc1d1	mladý
povrch	povrch	k1gInSc1	povrch
má	mít	k5eAaImIp3nS	mít
složitou	složitý	k2eAgFnSc4d1	složitá
geologickou	geologický	k2eAgFnSc4d1	geologická
historii	historie	k1gFnSc4	historie
a	a	k8xC	a
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
kryovulkány	kryovulkán	k1gInPc1	kryovulkán
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
chrlí	chrlit	k5eAaImIp3nP	chrlit
dusík	dusík	k1gInSc4	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
dusíkatá	dusíkatý	k2eAgFnSc1d1	dusíkatá
atmosféra	atmosféra	k1gFnSc1	atmosféra
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
řídká	řídký	k2eAgFnSc1d1	řídká
<g/>
,	,	kIx,	,
atmosférický	atmosférický	k2eAgInSc1d1	atmosférický
tlak	tlak	k1gInSc1	tlak
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hodnoty	hodnota	k1gFnPc4	hodnota
méně	málo	k6eAd2	málo
než	než	k8xS	než
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
70	[number]	k4	70
000	[number]	k4	000
tlaku	tlak	k1gInSc2	tlak
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
moře	moře	k1gNnSc2	moře
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
okolo	okolo	k7c2	okolo
Tritonu	triton	k1gInSc2	triton
proletěla	proletět	k5eAaPmAgFnS	proletět
sonda	sonda	k1gFnSc1	sonda
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
poprvé	poprvé	k6eAd1	poprvé
provedla	provést	k5eAaPmAgFnS	provést
důkladné	důkladný	k2eAgNnSc4d1	důkladné
pozorování	pozorování	k1gNnSc4	pozorování
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
vesmírné	vesmírný	k2eAgFnPc4d1	vesmírná
mise	mise	k1gFnPc4	mise
k	k	k7c3	k
Tritonu	triton	k1gInSc3	triton
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
nepodařilo	podařit	k5eNaPmAgNnS	podařit
sehnat	sehnat	k5eAaPmF	sehnat
finance	finance	k1gFnPc4	finance
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Objevení	objevení	k1gNnSc1	objevení
a	a	k8xC	a
pojmenování	pojmenování	k1gNnSc1	pojmenování
==	==	k?	==
</s>
</p>
<p>
<s>
Triton	triton	k1gMnSc1	triton
objevil	objevit	k5eAaPmAgMnS	objevit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
observatoři	observatoř	k1gFnSc6	observatoř
poblíž	poblíž	k7c2	poblíž
Liverpoolu	Liverpool	k1gInSc2	Liverpool
britský	britský	k2eAgMnSc1d1	britský
astronom	astronom	k1gMnSc1	astronom
William	William	k1gInSc4	William
Lassell	Lassell	k1gInSc1	Lassell
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1846	[number]	k4	1846
<g/>
,	,	kIx,	,
pouhých	pouhý	k2eAgInPc2d1	pouhý
17	[number]	k4	17
dní	den	k1gInPc2	den
po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
samotného	samotný	k2eAgInSc2d1	samotný
Neptunu	Neptun	k1gInSc2	Neptun
německými	německý	k2eAgMnPc7d1	německý
astronomy	astronom	k1gMnPc7	astronom
Johannem	Johann	k1gMnSc7	Johann
Gottfriedem	Gottfried	k1gMnSc7	Gottfried
Gallem	Gall	k1gMnSc7	Gall
a	a	k8xC	a
Heinrichem	Heinrich	k1gMnSc7	Heinrich
Louisem	Louis	k1gMnSc7	Louis
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Arrestem	Arrest	k1gMnSc7	Arrest
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
získali	získat	k5eAaPmAgMnP	získat
souřadnice	souřadnice	k1gFnPc4	souřadnice
planety	planeta	k1gFnSc2	planeta
z	z	k7c2	z
výpočtů	výpočet	k1gInPc2	výpočet
francouzského	francouzský	k2eAgMnSc2d1	francouzský
astronoma	astronom	k1gMnSc2	astronom
a	a	k8xC	a
matematika	matematik	k1gMnSc2	matematik
Urbaina	Urbain	k1gMnSc2	Urbain
Le	Le	k1gMnSc2	Le
Verriera	Verrier	k1gMnSc2	Verrier
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lassell	Lassell	k1gInSc4	Lassell
<g/>
,	,	kIx,	,
povoláním	povolání	k1gNnSc7	povolání
sládek	sládek	k1gMnSc1	sládek
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
stavět	stavět	k5eAaImF	stavět
svůj	svůj	k3xOyFgInSc4	svůj
amatérský	amatérský	k2eAgInSc4d1	amatérský
dalekohled	dalekohled	k1gInSc4	dalekohled
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1820	[number]	k4	1820
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
John	John	k1gMnSc1	John
Herschel	Herschel	k1gMnSc1	Herschel
dověděl	dovědět	k5eAaPmAgMnS	dovědět
o	o	k7c6	o
objevu	objev	k1gInSc6	objev
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
,	,	kIx,	,
napsal	napsat	k5eAaPmAgInS	napsat
Lassellovi	Lassell	k1gMnSc3	Lassell
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zkusil	zkusit	k5eAaPmAgMnS	zkusit
najít	najít	k5eAaPmF	najít
jeho	jeho	k3xOp3gInPc4	jeho
potenciální	potenciální	k2eAgInPc4d1	potenciální
měsíce	měsíc	k1gInPc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Lassell	Lassell	k1gInSc4	Lassell
objevil	objevit	k5eAaPmAgInS	objevit
Triton	triton	k1gInSc1	triton
pouhých	pouhý	k2eAgInPc2d1	pouhý
osm	osm	k4xCc4	osm
dní	den	k1gInPc2	den
poté	poté	k6eAd1	poté
<g/>
.	.	kIx.	.
</s>
<s>
Lassell	Lassell	k1gMnSc1	Lassell
také	také	k9	také
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
objevil	objevit	k5eAaPmAgInS	objevit
Neptunovy	Neptunův	k2eAgInPc4d1	Neptunův
prstence	prstenec	k1gInPc4	prstenec
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
existence	existence	k1gFnSc1	existence
těchto	tento	k3xDgInPc2	tento
prstenů	prsten	k1gInPc2	prsten
později	pozdě	k6eAd2	pozdě
opravdu	opravdu	k6eAd1	opravdu
prokázána	prokázat	k5eAaPmNgFnS	prokázat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
slabé	slabý	k2eAgFnPc1d1	slabá
a	a	k8xC	a
temné	temný	k2eAgFnPc1d1	temná
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
Lassellově	Lassellův	k2eAgInSc6d1	Lassellův
objevu	objev	k1gInSc6	objev
pochybuje	pochybovat	k5eAaImIp3nS	pochybovat
<g/>
.	.	kIx.	.
<g/>
Triton	triton	k1gInSc1	triton
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
po	po	k7c6	po
řeckém	řecký	k2eAgMnSc6d1	řecký
bohu	bůh	k1gMnSc6	bůh
moří	mořit	k5eAaImIp3nP	mořit
Tritonovi	tritonův	k2eAgMnPc1d1	tritonův
(	(	kIx(	(
<g/>
Τ	Τ	k?	Τ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
synu	syn	k1gMnSc3	syn
Poseidóna	Poseidóna	k1gFnSc1	Poseidóna
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
římského	římský	k2eAgMnSc2d1	římský
boha	bůh	k1gMnSc2	bůh
Neptuna	Neptun	k1gMnSc2	Neptun
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
jméno	jméno	k1gNnSc4	jméno
poprvé	poprvé	k6eAd1	poprvé
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
Camille	Camille	k1gInSc1	Camille
Flammarion	Flammarion	k1gInSc4	Flammarion
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Populární	populární	k2eAgFnSc2d1	populární
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oficiálně	oficiálně	k6eAd1	oficiálně
byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
Triton	triton	k1gInSc1	triton
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
až	až	k9	až
o	o	k7c4	o
několik	několik	k4yIc4	několik
desetiletí	desetiletí	k1gNnPc2	desetiletí
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
objevu	objev	k1gInSc2	objev
druhého	druhý	k4xOgInSc2	druhý
měsíce	měsíc	k1gInSc2	měsíc
Neptunu	Neptun	k1gInSc2	Neptun
Nereidy	nereida	k1gFnSc2	nereida
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
se	se	k3xPyFc4	se
Tritonu	triton	k1gInSc6	triton
říkalo	říkat	k5eAaImAgNnS	říkat
"	"	kIx"	"
<g/>
Neptunův	Neptunův	k2eAgInSc4d1	Neptunův
satelit	satelit	k1gInSc4	satelit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Lassell	Lassell	k1gMnSc1	Lassell
svůj	svůj	k3xOyFgInSc4	svůj
objev	objev	k1gInSc4	objev
nepojmenoval	pojmenovat	k5eNaPmAgMnS	pojmenovat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
o	o	k7c4	o
pár	pár	k4xCyI	pár
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
název	název	k1gInSc1	název
osmého	osmý	k4xOgInSc2	osmý
měsíce	měsíc	k1gInSc2	měsíc
Saturnu	Saturn	k1gInSc2	Saturn
(	(	kIx(	(
<g/>
Hyperion	Hyperion	k1gInSc1	Hyperion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svého	svůj	k1gMnSc4	svůj
dalšího	další	k2eAgInSc2d1	další
objevu	objev	k1gInSc2	objev
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc4	třetí
a	a	k8xC	a
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
měsíc	měsíc	k1gInSc4	měsíc
Uranu	Uran	k1gInSc2	Uran
(	(	kIx(	(
<g/>
Ariel	Ariel	k1gInSc1	Ariel
a	a	k8xC	a
Umbriel	Umbriel	k1gInSc1	Umbriel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
Lassell	Lassell	k1gMnSc1	Lassell
objevil	objevit	k5eAaPmAgMnS	objevit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1851	[number]	k4	1851
<g/>
,	,	kIx,	,
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
John	John	k1gMnSc1	John
Herschel	Herschel	k1gMnSc1	Herschel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Oběžná	oběžný	k2eAgFnSc1d1	oběžná
dráha	dráha	k1gFnSc1	dráha
a	a	k8xC	a
rotace	rotace	k1gFnSc1	rotace
==	==	k?	==
</s>
</p>
<p>
<s>
Triton	triton	k1gInSc1	triton
je	být	k5eAaImIp3nS	být
výjimečný	výjimečný	k2eAgInSc1d1	výjimečný
mezi	mezi	k7c7	mezi
velkými	velký	k2eAgInPc7d1	velký
měsíci	měsíc	k1gInPc7	měsíc
Sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
svou	svůj	k3xOyFgFnSc7	svůj
retrográdní	retrográdní	k2eAgFnSc7d1	retrográdní
dráhou	dráha	k1gFnSc7	dráha
okolo	okolo	k7c2	okolo
své	svůj	k3xOyFgFnSc2	svůj
planety	planeta	k1gFnSc2	planeta
(	(	kIx(	(
<g/>
obíhá	obíhat	k5eAaImIp3nS	obíhat
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
opačném	opačný	k2eAgInSc6d1	opačný
k	k	k7c3	k
rotaci	rotace	k1gFnSc3	rotace
planety	planeta	k1gFnSc2	planeta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vnějších	vnější	k2eAgInPc2d1	vnější
nepravidelných	pravidelný	k2eNgInPc2d1	nepravidelný
měsíců	měsíc	k1gInPc2	měsíc
Jupiteru	Jupiter	k1gInSc2	Jupiter
a	a	k8xC	a
Saturnu	Saturn	k1gInSc2	Saturn
a	a	k8xC	a
některé	některý	k3yIgInPc4	některý
vnější	vnější	k2eAgInPc4d1	vnější
měsíce	měsíc	k1gInPc4	měsíc
Uranu	Uran	k1gInSc2	Uran
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
retrográdní	retrográdní	k2eAgFnSc4d1	retrográdní
dráhu	dráha	k1gFnSc4	dráha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ty	ten	k3xDgMnPc4	ten
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
vzdálenější	vzdálený	k2eAgInPc4d2	vzdálenější
od	od	k7c2	od
svých	svůj	k3xOyFgFnPc2	svůj
planet	planeta	k1gFnPc2	planeta
a	a	k8xC	a
také	také	k9	také
menší	malý	k2eAgInSc1d2	menší
<g/>
;	;	kIx,	;
průměr	průměr	k1gInSc1	průměr
největšího	veliký	k2eAgInSc2d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
Phoebe	Phoeb	k1gMnSc5	Phoeb
<g/>
,	,	kIx,	,
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
jen	jen	k9	jen
8	[number]	k4	8
%	%	kIx~	%
průměru	průměr	k1gInSc2	průměr
(	(	kIx(	(
<g/>
a	a	k8xC	a
0,03	[number]	k4	0,03
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
)	)	kIx)	)
Tritonu	triton	k1gInSc2	triton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Triton	triton	k1gMnSc1	triton
kolem	kolem	k7c2	kolem
Neptunu	Neptun	k1gInSc2	Neptun
obíhá	obíhat	k5eAaImIp3nS	obíhat
vázaně	vázaně	k6eAd1	vázaně
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
planetě	planeta	k1gFnSc3	planeta
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
přivrácená	přivrácený	k2eAgFnSc1d1	přivrácená
jedna	jeden	k4xCgFnSc1	jeden
strana	strana	k1gFnSc1	strana
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nezvyklému	zvyklý	k2eNgInSc3d1	nezvyklý
sklonu	sklon	k1gInSc3	sklon
dráhy	dráha	k1gFnSc2	dráha
je	být	k5eAaImIp3nS	být
Tritonova	tritonův	k2eAgFnSc1d1	Tritonova
osa	osa	k1gFnSc1	osa
rotace	rotace	k1gFnSc2	rotace
nakloněna	naklonit	k5eAaPmNgFnS	naklonit
o	o	k7c4	o
157	[number]	k4	157
<g/>
°	°	k?	°
vůči	vůči	k7c3	vůči
ose	osa	k1gFnSc3	osa
rotace	rotace	k1gFnSc2	rotace
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
nakloněna	naklonit	k5eAaPmNgFnS	naklonit
o	o	k7c4	o
30	[number]	k4	30
<g/>
°	°	k?	°
vůči	vůči	k7c3	vůči
rovině	rovina	k1gFnSc3	rovina
Neptunova	Neptunův	k2eAgInSc2d1	Neptunův
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
sklonů	sklon	k1gInPc2	sklon
os	osa	k1gFnPc2	osa
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Tritonova	tritonův	k2eAgFnSc1d1	Tritonova
osa	osa	k1gFnSc1	osa
rotace	rotace	k1gFnSc2	rotace
leží	ležet	k5eAaImIp3nS	ležet
blízko	blízko	k6eAd1	blízko
rovině	rovina	k1gFnSc3	rovina
Neptunova	Neptunův	k2eAgInSc2d1	Neptunův
oběhu	oběh	k1gInSc2	oběh
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
na	na	k7c6	na
Tritonu	triton	k1gInSc6	triton
během	během	k7c2	během
Neptunova	Neptunův	k2eAgInSc2d1	Neptunův
roku	rok	k1gInSc2	rok
střídavě	střídavě	k6eAd1	střídavě
svítí	svítit	k5eAaImIp3nS	svítit
slunce	slunce	k1gNnSc4	slunce
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
polárních	polární	k2eAgFnPc2d1	polární
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
má	mít	k5eAaImIp3nS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
za	za	k7c4	za
následek	následek	k1gInSc4	následek
výrazné	výrazný	k2eAgInPc4d1	výrazný
sezónní	sezónní	k2eAgInPc4d1	sezónní
výkyvy	výkyv	k1gInPc4	výkyv
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dráha	dráha	k1gFnSc1	dráha
oběhu	oběh	k1gInSc2	oběh
Tritonu	triton	k1gInSc2	triton
kolem	kolem	k7c2	kolem
Neptunu	Neptun	k1gInSc2	Neptun
má	mít	k5eAaImIp3nS	mít
tvar	tvar	k1gInSc1	tvar
téměř	téměř	k6eAd1	téměř
dokonalé	dokonalý	k2eAgFnSc2d1	dokonalá
kružnice	kružnice	k1gFnSc2	kružnice
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
nulovou	nulový	k2eAgFnSc7d1	nulová
excentricitou	excentricita	k1gFnSc7	excentricita
<g/>
.	.	kIx.	.
</s>
<s>
Působením	působení	k1gNnSc7	působení
slapových	slapový	k2eAgFnPc2d1	slapová
sil	síla	k1gFnPc2	síla
se	se	k3xPyFc4	se
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
průměrná	průměrný	k2eAgFnSc1d1	průměrná
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
Tritonu	triton	k1gInSc2	triton
od	od	k7c2	od
Neptunu	Neptun	k1gInSc2	Neptun
a	a	k8xC	a
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
asi	asi	k9	asi
za	za	k7c4	za
3,6	[number]	k4	3,6
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
Triton	triton	k1gMnSc1	triton
překročí	překročit	k5eAaPmIp3nS	překročit
Rocheovu	Rocheův	k2eAgFnSc4d1	Rocheova
mez	mez	k1gFnSc4	mez
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Triton	triton	k1gMnSc1	triton
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
buď	buď	k8xC	buď
srazí	srazit	k5eAaPmIp3nP	srazit
s	s	k7c7	s
atmosférou	atmosféra	k1gFnSc7	atmosféra
Neptunu	Neptun	k1gInSc2	Neptun
a	a	k8xC	a
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
rozpadne	rozpadnout	k5eAaPmIp3nS	rozpadnout
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
prstenec	prstenec	k1gInSc1	prstenec
podobný	podobný	k2eAgInSc1d1	podobný
tomu	ten	k3xDgNnSc3	ten
okolo	okolo	k7c2	okolo
Saturnu	Saturn	k1gInSc2	Saturn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
a	a	k8xC	a
vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
Měsíce	měsíc	k1gInPc1	měsíc
s	s	k7c7	s
retrográdní	retrográdní	k2eAgFnSc7d1	retrográdní
dráhou	dráha	k1gFnSc7	dráha
nemohly	moct	k5eNaImAgFnP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
oblasti	oblast	k1gFnSc6	oblast
sluneční	sluneční	k2eAgFnSc2d1	sluneční
mlhoviny	mlhovina	k1gFnSc2	mlhovina
jako	jako	k8xC	jako
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
obíhají	obíhat	k5eAaImIp3nP	obíhat
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Triton	triton	k1gMnSc1	triton
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
Kuiperově	Kuiperův	k2eAgInSc6d1	Kuiperův
pásu	pás	k1gInSc6	pás
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
ho	on	k3xPp3gInSc4	on
Neptun	Neptun	k1gInSc4	Neptun
zachytil	zachytit	k5eAaPmAgMnS	zachytit
<g/>
.	.	kIx.	.
</s>
<s>
Kuiperův	Kuiperův	k2eAgInSc1d1	Kuiperův
pás	pás	k1gInSc1	pás
je	být	k5eAaImIp3nS	být
prstenec	prstenec	k1gInSc1	prstenec
složený	složený	k2eAgInSc1d1	složený
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
malých	malý	k2eAgNnPc2d1	malé
zmrzlých	zmrzlý	k2eAgNnPc2d1	zmrzlé
těles	těleso	k1gNnPc2	těleso
rozkládající	rozkládající	k2eAgFnPc1d1	rozkládající
se	se	k3xPyFc4	se
od	od	k7c2	od
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
Neptunu	Neptun	k1gInSc2	Neptun
až	až	k9	až
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
přibližně	přibližně	k6eAd1	přibližně
55	[number]	k4	55
AU	au	k0	au
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
ale	ale	k9	ale
i	i	k9	i
několik	několik	k4yIc1	několik
trpasličích	trpasličí	k2eAgFnPc2d1	trpasličí
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Pluto	plut	k2eAgNnSc1d1	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
Triton	triton	k1gInSc1	triton
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
o	o	k7c4	o
málo	málo	k6eAd1	málo
větší	veliký	k2eAgNnSc4d2	veliký
než	než	k8xS	než
Pluto	plut	k2eAgNnSc4d1	Pluto
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
téměř	téměř	k6eAd1	téměř
stejné	stejný	k2eAgNnSc1d1	stejné
chemické	chemický	k2eAgNnSc1d1	chemické
složení	složení	k1gNnSc1	složení
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
obě	dva	k4xCgNnPc1	dva
tělesa	těleso	k1gNnPc1	těleso
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
oblasti	oblast	k1gFnSc6	oblast
Sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
<g/>
Navrhovaná	navrhovaný	k2eAgFnSc1d1	navrhovaná
myšlenka	myšlenka	k1gFnSc1	myšlenka
zachycení	zachycení	k1gNnSc2	zachycení
Tritonu	triton	k1gInSc2	triton
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
několik	několik	k4yIc4	několik
vlastností	vlastnost	k1gFnPc2	vlastnost
Neptunova	Neptunův	k2eAgInSc2d1	Neptunův
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
neobvykle	obvykle	k6eNd1	obvykle
velikou	veliký	k2eAgFnSc4d1	veliká
excentricitu	excentricita	k1gFnSc4	excentricita
měsíce	měsíc	k1gInSc2	měsíc
Nereidy	nereida	k1gFnSc2	nereida
a	a	k8xC	a
malý	malý	k2eAgInSc1d1	malý
počet	počet	k1gInSc1	počet
měsíců	měsíc	k1gInPc2	měsíc
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
plynnými	plynný	k2eAgMnPc7d1	plynný
obry	obr	k1gMnPc7	obr
<g/>
.	.	kIx.	.
</s>
<s>
Tritonova	tritonův	k2eAgFnSc1d1	Tritonova
původní	původní	k2eAgFnSc1d1	původní
excentrická	excentrický	k2eAgFnSc1d1	excentrická
dráha	dráha	k1gFnSc1	dráha
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
křížila	křížit	k5eAaImAgFnS	křížit
s	s	k7c7	s
drahami	draha	k1gFnPc7	draha
nepravidelných	pravidelný	k2eNgMnPc2d1	nepravidelný
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
přerušila	přerušit	k5eAaPmAgFnS	přerušit
by	by	kYmCp3nS	by
dráhy	dráha	k1gFnPc1	dráha
menších	malý	k2eAgMnPc2d2	menší
přirozených	přirozený	k2eAgMnPc2d1	přirozený
satelitů	satelit	k1gMnPc2	satelit
gravitačními	gravitační	k2eAgFnPc7d1	gravitační
silami	síla	k1gFnPc7	síla
<g/>
.	.	kIx.	.
<g/>
Excentrická	excentrický	k2eAgFnSc1d1	excentrická
dráha	dráha	k1gFnSc1	dráha
po	po	k7c6	po
zachycení	zachycení	k1gNnSc6	zachycení
Tritonu	triton	k1gInSc2	triton
by	by	kYmCp3nS	by
také	také	k9	také
měla	mít	k5eAaImAgFnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
oteplování	oteplování	k1gNnSc2	oteplování
vnitřku	vnitřek	k1gInSc2	vnitřek
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
by	by	kYmCp3nS	by
vnitřek	vnitřek	k1gInSc1	vnitřek
Tritonu	triton	k1gInSc2	triton
zůstal	zůstat	k5eAaPmAgInS	zůstat
několik	několik	k4yIc4	několik
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
v	v	k7c6	v
tekutém	tekutý	k2eAgInSc6d1	tekutý
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
podporují	podporovat	k5eAaImIp3nP	podporovat
současná	současný	k2eAgNnPc4d1	současné
zjištění	zjištění	k1gNnPc4	zjištění
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
zdroj	zdroj	k1gInSc1	zdroj
tepla	teplo	k1gNnSc2	teplo
zmizel	zmizet	k5eAaPmAgInS	zmizet
se	s	k7c7	s
zakulacením	zakulacení	k1gNnSc7	zakulacení
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zachycení	zachycení	k1gNnSc1	zachycení
Tritonu	triton	k1gInSc2	triton
mohlo	moct	k5eAaImAgNnS	moct
proběhnout	proběhnout	k5eAaPmF	proběhnout
dvěma	dva	k4xCgInPc7	dva
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
těleso	těleso	k1gNnSc1	těleso
gravitačně	gravitačně	k6eAd1	gravitačně
zachyceno	zachytit	k5eAaPmNgNnS	zachytit
planetou	planeta	k1gFnSc7	planeta
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
ztratit	ztratit	k5eAaPmF	ztratit
energii	energie	k1gFnSc4	energie
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
zpomalit	zpomalit	k5eAaPmF	zpomalit
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nemohlo	moct	k5eNaImAgNnS	moct
uniknout	uniknout	k5eAaPmF	uniknout
z	z	k7c2	z
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
teorie	teorie	k1gFnSc1	teorie
zní	znět	k5eAaImIp3nS	znět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Triton	triton	k1gInSc1	triton
byl	být	k5eAaImAgInS	být
zpomalen	zpomalit	k5eAaPmNgInS	zpomalit
kolizí	kolize	k1gFnSc7	kolize
s	s	k7c7	s
jiným	jiný	k2eAgNnSc7d1	jiné
tělesem	těleso	k1gNnSc7	těleso
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
buď	buď	k8xC	buď
s	s	k7c7	s
takovým	takový	k3xDgNnSc7	takový
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
okolo	okolo	k7c2	okolo
Neptunu	Neptun	k1gInSc2	Neptun
procházelo	procházet	k5eAaImAgNnS	procházet
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1	nepravděpodobné
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
s	s	k7c7	s
jiným	jiný	k2eAgInSc7d1	jiný
měsícem	měsíc	k1gInSc7	měsíc
Neptunu	Neptun	k1gInSc2	Neptun
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
pravděpodobnější	pravděpodobný	k2eAgMnSc1d2	pravděpodobnější
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
teorie	teorie	k1gFnSc1	teorie
navrhuje	navrhovat	k5eAaImIp3nS	navrhovat
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
Triton	triton	k1gInSc1	triton
před	před	k7c7	před
zachycením	zachycení	k1gNnSc7	zachycení
tvořil	tvořit	k5eAaImAgInS	tvořit
s	s	k7c7	s
jiným	jiný	k2eAgInSc7d1	jiný
objektem	objekt	k1gInSc7	objekt
binární	binární	k2eAgInSc1d1	binární
systém	systém	k1gInSc1	systém
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
například	například	k6eAd1	například
Charon	Charon	k1gMnSc1	Charon
s	s	k7c7	s
Plutem	Pluto	k1gMnSc7	Pluto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
binární	binární	k2eAgInSc1d1	binární
systém	systém	k1gInSc1	systém
přiblížil	přiblížit	k5eAaPmAgInS	přiblížit
k	k	k7c3	k
Neptunu	Neptun	k1gInSc3	Neptun
<g/>
,	,	kIx,	,
orbitální	orbitální	k2eAgFnSc1d1	orbitální
energie	energie	k1gFnSc1	energie
se	se	k3xPyFc4	se
přesunula	přesunout	k5eAaPmAgFnS	přesunout
z	z	k7c2	z
Tritonu	triton	k1gInSc2	triton
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
gravitačního	gravitační	k2eAgNnSc2d1	gravitační
pole	pole	k1gNnSc2	pole
Neptunu	Neptun	k1gInSc2	Neptun
odpoutal	odpoutat	k5eAaPmAgMnS	odpoutat
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
teorii	teorie	k1gFnSc4	teorie
podporují	podporovat	k5eAaImIp3nP	podporovat
některá	některý	k3yIgNnPc1	některý
zjištění	zjištění	k1gNnPc1	zjištění
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
že	že	k8xS	že
mezi	mezi	k7c7	mezi
objekty	objekt	k1gInPc7	objekt
Kuiperova	Kuiperův	k2eAgInSc2d1	Kuiperův
pásu	pás	k1gInSc2	pás
jsou	být	k5eAaImIp3nP	být
binární	binární	k2eAgInPc1d1	binární
systémy	systém	k1gInPc1	systém
velmi	velmi	k6eAd1	velmi
běžné	běžný	k2eAgInPc1d1	běžný
<g/>
.	.	kIx.	.
</s>
<s>
Takovéto	takovýto	k3xDgFnPc1	takovýto
události	událost	k1gFnPc1	událost
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
Neptunu	Neptun	k1gInSc2	Neptun
nebo	nebo	k8xC	nebo
během	během	k7c2	během
jeho	on	k3xPp3gNnSc2	on
migračního	migrační	k2eAgNnSc2d1	migrační
období	období	k1gNnSc2	období
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
charakteristika	charakteristika	k1gFnSc1	charakteristika
==	==	k?	==
</s>
</p>
<p>
<s>
Triton	triton	k1gMnSc1	triton
je	být	k5eAaImIp3nS	být
sedmý	sedmý	k4xOgInSc4	sedmý
největší	veliký	k2eAgInSc4d3	veliký
měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
šestnácté	šestnáctý	k4xOgNnSc1	šestnáctý
největší	veliký	k2eAgNnSc1d3	veliký
těleso	těleso	k1gNnSc1	těleso
Sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnPc4d2	veliký
než	než	k8xS	než
trpasličí	trpasličí	k2eAgFnPc4d1	trpasličí
planety	planeta	k1gFnPc4	planeta
Pluto	Pluto	k1gNnSc1	Pluto
a	a	k8xC	a
Eris	Eris	k1gFnSc1	Eris
<g/>
.	.	kIx.	.
</s>
<s>
Zabírá	zabírat	k5eAaImIp3nS	zabírat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
99,5	[number]	k4	99,5
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnPc4	hmotnost
objektů	objekt	k1gInPc2	objekt
obíhajících	obíhající	k2eAgInPc2d1	obíhající
okolo	okolo	k7c2	okolo
Neptunu	Neptun	k1gInSc2	Neptun
včetně	včetně	k7c2	včetně
jeho	jeho	k3xOp3gInPc2	jeho
prstenců	prstenec	k1gInPc2	prstenec
a	a	k8xC	a
dvanácti	dvanáct	k4xCc2	dvanáct
dalších	další	k2eAgInPc2d1	další
měsíců	měsíc	k1gInPc2	měsíc
Jeho	jeho	k3xOp3gFnSc1	jeho
hmotnost	hmotnost	k1gFnSc1	hmotnost
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
celková	celkový	k2eAgFnSc1d1	celková
hmotnost	hmotnost	k1gFnSc1	hmotnost
všech	všecek	k3xTgInPc2	všecek
známých	známý	k2eAgInPc2d1	známý
měsíců	měsíc	k1gInPc2	měsíc
ve	v	k7c6	v
Sluneční	sluneční	k2eAgFnSc6d1	sluneční
soustavě	soustava	k1gFnSc6	soustava
menších	malý	k2eAgFnPc2d2	menší
než	než	k8xS	než
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Velikostí	velikost	k1gFnSc7	velikost
poloměru	poloměr	k1gInSc2	poloměr
<g/>
,	,	kIx,	,
hustotou	hustota	k1gFnSc7	hustota
(	(	kIx(	(
<g/>
2,061	[number]	k4	2,061
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teplotou	teplota	k1gFnSc7	teplota
a	a	k8xC	a
chemickým	chemický	k2eAgNnSc7d1	chemické
složením	složení	k1gNnSc7	složení
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
Plutu	plut	k1gInSc3	plut
<g/>
.	.	kIx.	.
<g/>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
u	u	k7c2	u
Pluta	Pluto	k1gNnSc2	Pluto
je	být	k5eAaImIp3nS	být
55	[number]	k4	55
%	%	kIx~	%
Tritonova	tritonův	k2eAgInSc2d1	tritonův
povrchu	povrch	k1gInSc2	povrch
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
zmrzlým	zmrzlý	k2eAgInSc7d1	zmrzlý
dusíkem	dusík	k1gInSc7	dusík
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
35	[number]	k4	35
%	%	kIx~	%
vodním	vodní	k2eAgInSc7d1	vodní
ledem	led	k1gInSc7	led
a	a	k8xC	a
zbylých	zbylý	k2eAgInPc2d1	zbylý
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
%	%	kIx~	%
suchým	suchý	k2eAgInSc7d1	suchý
ledem	led	k1gInSc7	led
(	(	kIx(	(
<g/>
zmrzlý	zmrzlý	k2eAgInSc1d1	zmrzlý
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
také	také	k9	také
methan	methan	k1gInSc1	methan
(	(	kIx(	(
<g/>
0,1	[number]	k4	0,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
oxid	oxid	k1gInSc1	oxid
uhelnatý	uhelnatý	k2eAgInSc1d1	uhelnatý
(	(	kIx(	(
<g/>
0,05	[number]	k4	0,05
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
je	být	k5eAaImIp3nS	být
možná	možná	k9	možná
i	i	k9	i
amoniak	amoniak	k1gInSc4	amoniak
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
výskytu	výskyt	k1gInSc2	výskyt
dihydrátu	dihydrát	k1gInSc2	dihydrát
amoniaku	amoniak	k1gInSc2	amoniak
v	v	k7c6	v
litosféře	litosféra	k1gFnSc6	litosféra
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
Tritonu	triton	k1gInSc2	triton
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zhruba	zhruba	k6eAd1	zhruba
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
45	[number]	k4	45
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nP	tvořit
vodní	vodní	k2eAgInSc4d1	vodní
led	led	k1gInSc4	led
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
hornina	hornina	k1gFnSc1	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
povrch	povrch	k1gInSc1	povrch
zabírá	zabírat	k5eAaImIp3nS	zabírat
plochu	plocha	k1gFnSc4	plocha
23	[number]	k4	23
milionů	milion	k4xCgInPc2	milion
km2	km2	k4	km2
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
4,5	[number]	k4	4,5
%	%	kIx~	%
povrchu	povrch	k1gInSc2	povrch
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Triton	triton	k1gMnSc1	triton
má	mít	k5eAaImIp3nS	mít
poměrně	poměrně	k6eAd1	poměrně
vysoké	vysoký	k2eAgNnSc4d1	vysoké
albedo	albedo	k1gNnSc4	albedo
<g/>
,	,	kIx,	,
odráží	odrážet	k5eAaImIp3nS	odrážet
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
95	[number]	k4	95
%	%	kIx~	%
slunečních	sluneční	k2eAgInPc2d1	sluneční
paprsků	paprsek	k1gInPc2	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
odráží	odrážet	k5eAaImIp3nS	odrážet
pouze	pouze	k6eAd1	pouze
11	[number]	k4	11
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Triton	triton	k1gInSc1	triton
je	být	k5eAaImIp3nS	být
diferenciovaný	diferenciovaný	k2eAgInSc1d1	diferenciovaný
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
Země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
pevné	pevný	k2eAgNnSc4d1	pevné
jádro	jádro	k1gNnSc4	jádro
<g/>
,	,	kIx,	,
plášť	plášť	k1gInSc4	plášť
a	a	k8xC	a
kůru	kůra	k1gFnSc4	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Plášť	plášť	k1gInSc1	plášť
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
je	být	k5eAaImIp3nS	být
obaleno	obalen	k2eAgNnSc1d1	obaleno
jádro	jádro	k1gNnSc1	jádro
tvořené	tvořený	k2eAgNnSc1d1	tvořené
kovy	kov	k1gInPc7	kov
a	a	k8xC	a
horninami	hornina	k1gFnPc7	hornina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
ledu	led	k1gInSc2	led
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
Tritonu	triton	k1gInSc2	triton
je	být	k5eAaImIp3nS	být
dostatek	dostatek	k1gInSc1	dostatek
hornin	hornina	k1gFnPc2	hornina
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
radioaktivní	radioaktivní	k2eAgInSc1d1	radioaktivní
rozpad	rozpad	k1gInSc1	rozpad
podporoval	podporovat	k5eAaImAgInS	podporovat
proudění	proudění	k1gNnSc4	proudění
tepla	teplo	k1gNnSc2	teplo
v	v	k7c6	v
plášti	plášť	k1gInSc6	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
teplo	teplo	k1gNnSc1	teplo
může	moct	k5eAaImIp3nS	moct
stačit	stačit	k5eAaBmF	stačit
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dalo	dát	k5eAaPmAgNnS	dát
vznik	vznik	k1gInSc1	vznik
podpovrchovému	podpovrchový	k2eAgInSc3d1	podpovrchový
oceánu	oceán	k1gInSc3	oceán
(	(	kIx(	(
<g/>
podobnému	podobný	k2eAgNnSc3d1	podobné
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
má	mít	k5eAaImIp3nS	mít
možná	možný	k2eAgFnSc1d1	možná
Europa	Europa	k1gFnSc1	Europa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Potencionální	potencionální	k2eAgFnSc1d1	potencionální
vrstva	vrstva	k1gFnSc1	vrstva
tekuté	tekutý	k2eAgFnSc2d1	tekutá
vody	voda	k1gFnSc2	voda
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
dát	dát	k5eAaPmF	dát
vznik	vznik	k1gInSc4	vznik
primitivním	primitivní	k2eAgFnPc3d1	primitivní
formám	forma	k1gFnPc3	forma
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Atmosféra	atmosféra	k1gFnSc1	atmosféra
===	===	k?	===
</s>
</p>
<p>
<s>
Tritonova	tritonův	k2eAgFnSc1d1	Tritonova
atmosféra	atmosféra	k1gFnSc1	atmosféra
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
především	především	k9	především
dusíkem	dusík	k1gInSc7	dusík
se	se	k3xPyFc4	se
stopami	stopa	k1gFnPc7	stopa
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgInSc2d1	uhelnatý
a	a	k8xC	a
blízko	blízko	k6eAd1	blízko
povrchu	povrch	k1gInSc2	povrch
i	i	k9	i
se	s	k7c7	s
stopami	stopa	k1gFnPc7	stopa
metanu	metan	k1gInSc2	metan
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nejspíše	nejspíše	k9	nejspíše
vypařováním	vypařování	k1gNnSc7	vypařování
dusíku	dusík	k1gInSc2	dusík
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
u	u	k7c2	u
Pluta	Pluto	k1gNnSc2	Pluto
<g/>
.	.	kIx.	.
</s>
<s>
Povrchová	povrchový	k2eAgFnSc1d1	povrchová
teplota	teplota	k1gFnSc1	teplota
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
minimálně	minimálně	k6eAd1	minimálně
35,6	[number]	k4	35,6
K	k	k7c3	k
(	(	kIx(	(
<g/>
−	−	k?	−
<g/>
237,6	[number]	k4	237,6
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dusíkatý	dusíkatý	k2eAgInSc1d1	dusíkatý
led	led	k1gInSc1	led
má	mít	k5eAaImIp3nS	mít
krystalickou	krystalický	k2eAgFnSc4d1	krystalická
hexagonální	hexagonální	k2eAgFnSc4d1	hexagonální
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
přechod	přechod	k1gInSc4	přechod
mezi	mezi	k7c7	mezi
touto	tento	k3xDgFnSc7	tento
teplejší	teplý	k2eAgFnSc7d2	teplejší
strukturou	struktura	k1gFnSc7	struktura
a	a	k8xC	a
krychlovou	krychlový	k2eAgFnSc7d1	krychlová
strukturou	struktura	k1gFnSc7	struktura
nastává	nastávat	k5eAaImIp3nS	nastávat
právě	právě	k9	právě
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
teplotě	teplota	k1gFnSc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
něco	něco	k3yInSc1	něco
málo	málo	k6eAd1	málo
nad	nad	k7c7	nad
40	[number]	k4	40
K.	K.	kA	K.
Tento	tento	k3xDgInSc4	tento
rozsah	rozsah	k1gInSc4	rozsah
teploty	teplota	k1gFnSc2	teplota
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
na	na	k7c6	na
Plutu	plut	k1gInSc6	plut
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
44	[number]	k4	44
K	K	kA	K
(	(	kIx(	(
<g/>
−	−	k?	−
<g/>
229	[number]	k4	229
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tlak	tlak	k1gInSc1	tlak
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
1,4	[number]	k4	1,4
<g/>
–	–	k?	–
<g/>
1,9	[number]	k4	1,9
pascalů	pascal	k1gInPc2	pascal
(	(	kIx(	(
<g/>
0,014	[number]	k4	0,014
<g/>
–	–	k?	–
<g/>
0,019	[number]	k4	0,019
millibarů	millibar	k1gInPc2	millibar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Turbulence	turbulence	k1gFnSc1	turbulence
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
troposféru	troposféra	k1gFnSc4	troposféra
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
8	[number]	k4	8
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Triton	triton	k1gMnSc1	triton
má	mít	k5eAaImIp3nS	mít
místo	místo	k1gNnSc4	místo
běžné	běžný	k2eAgFnSc2d1	běžná
stratosféry	stratosféra	k1gFnSc2	stratosféra
rovnou	rovnou	k6eAd1	rovnou
termosféru	termosféra	k1gFnSc4	termosféra
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
od	od	k7c2	od
8	[number]	k4	8
do	do	k7c2	do
950	[number]	k4	950
km	km	kA	km
a	a	k8xC	a
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
ještě	ještě	k6eAd1	ještě
exosféru	exosféra	k1gFnSc4	exosféra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vrchních	vrchní	k2eAgFnPc6d1	vrchní
vrstvách	vrstva	k1gFnPc6	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
teplota	teplota	k1gFnSc1	teplota
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
hodnoty	hodnota	k1gFnSc2	hodnota
95	[number]	k4	95
±	±	k?	±
5	[number]	k4	5
K	K	kA	K
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
teplu	teplo	k1gNnSc3	teplo
získanému	získaný	k2eAgInSc3d1	získaný
z	z	k7c2	z
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
částech	část	k1gFnPc6	část
troposféry	troposféra	k1gFnSc2	troposféra
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
mlhy	mlha	k1gFnPc1	mlha
složené	složený	k2eAgFnPc1d1	složená
nejspíše	nejspíše	k9	nejspíše
z	z	k7c2	z
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
a	a	k8xC	a
nitrilů	nitril	k1gInPc2	nitril
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
působením	působení	k1gNnSc7	působení
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
na	na	k7c4	na
methan	methan	k1gInSc4	methan
<g/>
.	.	kIx.	.
</s>
<s>
Dusík	dusík	k1gInSc1	dusík
na	na	k7c6	na
Tritonu	triton	k1gInSc6	triton
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
mračna	mračna	k1gFnSc1	mračna
sahající	sahající	k2eAgFnSc1d1	sahající
1	[number]	k4	1
až	až	k9	až
3	[number]	k4	3
km	km	kA	km
nad	nad	k7c4	nad
jeho	jeho	k3xOp3gInSc4	jeho
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
<g/>
Pozorování	pozorování	k1gNnSc1	pozorování
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
naznačila	naznačit	k5eAaPmAgFnS	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
měsíc	měsíc	k1gInSc1	měsíc
má	mít	k5eAaImIp3nS	mít
hustější	hustý	k2eAgFnSc4d2	hustší
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
předtím	předtím	k6eAd1	předtím
myslelo	myslet	k5eAaImAgNnS	myslet
z	z	k7c2	z
dat	datum	k1gNnPc2	datum
sondy	sonda	k1gFnSc2	sonda
Voyager	Voyager	k1gInSc4	Voyager
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgNnPc7d1	další
pozorováními	pozorování	k1gNnPc7	pozorování
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
Tritonu	triton	k1gInSc6	triton
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1989	[number]	k4	1989
a	a	k8xC	a
1998	[number]	k4	1998
zvedla	zvednout	k5eAaPmAgFnS	zvednout
o	o	k7c4	o
5	[number]	k4	5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
pozorování	pozorování	k1gNnPc1	pozorování
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
měsíc	měsíc	k1gInSc1	měsíc
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
k	k	k7c3	k
neobvykle	obvykle	k6eNd1	obvykle
teplému	teplý	k2eAgNnSc3d1	teplé
období	období	k1gNnSc3	období
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nastává	nastávat	k5eAaImIp3nS	nastávat
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
několik	několik	k4yIc4	několik
stovek	stovka	k1gFnPc2	stovka
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
možných	možný	k2eAgFnPc2d1	možná
příčin	příčina	k1gFnPc2	příčina
je	být	k5eAaImIp3nS	být
snížení	snížení	k1gNnSc1	snížení
albeda	albed	k1gMnSc2	albed
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
by	by	kYmCp3nS	by
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
vstřebat	vstřebat	k5eAaPmF	vstřebat
více	hodně	k6eAd2	hodně
tepla	teplo	k1gNnSc2	teplo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Povrch	povrch	k1gInSc1	povrch
===	===	k?	===
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
znalosti	znalost	k1gFnPc1	znalost
o	o	k7c6	o
povrchu	povrch	k1gInSc6	povrch
měsíce	měsíc	k1gInSc2	měsíc
byly	být	k5eAaImAgFnP	být
zjištěny	zjistit	k5eAaPmNgFnP	zjistit
sondou	sonda	k1gFnSc7	sonda
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
40	[number]	k4	40
%	%	kIx~	%
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
Voyager	Voyager	k1gInSc1	Voyager
zmapoval	zmapovat	k5eAaPmAgInS	zmapovat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
kaňony	kaňon	k1gInPc1	kaňon
<g/>
,	,	kIx,	,
skály	skála	k1gFnPc1	skála
a	a	k8xC	a
ledové	ledový	k2eAgFnPc1d1	ledová
plochy	plocha	k1gFnPc1	plocha
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
ze	z	k7c2	z
zmrzlého	zmrzlý	k2eAgInSc2d1	zmrzlý
methanu	methan	k1gInSc2	methan
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
plochý	plochý	k2eAgInSc1d1	plochý
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
výškové	výškový	k2eAgNnSc1d1	výškové
převýšení	převýšení	k1gNnSc1	převýšení
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
body	bod	k1gInPc7	bod
nepřesahuje	přesahovat	k5eNaImIp3nS	přesahovat
výškový	výškový	k2eAgInSc1d1	výškový
kilometr	kilometr	k1gInSc1	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
měsíci	měsíc	k1gInSc6	měsíc
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
málo	málo	k4c1	málo
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
analýzy	analýza	k1gFnSc2	analýza
hustoty	hustota	k1gFnSc2	hustota
a	a	k8xC	a
rozmístění	rozmístění	k1gNnSc1	rozmístění
kráterů	kráter	k1gInPc2	kráter
vyplynulo	vyplynout	k5eAaPmAgNnS	vyplynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Tritonův	tritonův	k2eAgInSc1d1	tritonův
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
geologickém	geologický	k2eAgInSc6d1	geologický
smyslu	smysl	k1gInSc6	smysl
velmi	velmi	k6eAd1	velmi
mladý	mladý	k2eAgMnSc1d1	mladý
<g/>
,	,	kIx,	,
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc1	jeho
stáří	stáří	k1gNnSc1	stáří
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
mezi	mezi	k7c7	mezi
6	[number]	k4	6
miliony	milion	k4xCgInPc7	milion
a	a	k8xC	a
50	[number]	k4	50
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Kryovulkanismus	Kryovulkanismus	k1gInSc4	Kryovulkanismus
====	====	k?	====
</s>
</p>
<p>
<s>
Triton	triton	k1gMnSc1	triton
je	být	k5eAaImIp3nS	být
geologicky	geologicky	k6eAd1	geologicky
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
;	;	kIx,	;
jeho	on	k3xPp3gInSc4	on
povrch	povrch	k1gInSc4	povrch
je	být	k5eAaImIp3nS	být
mladý	mladý	k2eAgMnSc1d1	mladý
a	a	k8xC	a
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
mnoho	mnoho	k4c1	mnoho
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
složen	složen	k2eAgMnSc1d1	složen
ze	z	k7c2	z
zmrzlých	zmrzlý	k2eAgFnPc2d1	zmrzlá
složek	složka	k1gFnPc2	složka
<g/>
,	,	kIx,	,
podpovrchové	podpovrchový	k2eAgInPc1d1	podpovrchový
procesy	proces	k1gInPc1	proces
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgInPc1d1	podobný
jako	jako	k8xC	jako
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
sopky	sopka	k1gFnPc1	sopka
a	a	k8xC	a
riftová	riftový	k2eAgNnPc1d1	riftový
údolí	údolí	k1gNnPc1	údolí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
namísto	namísto	k7c2	namísto
roztavených	roztavený	k2eAgFnPc2d1	roztavená
hornin	hornina	k1gFnPc2	hornina
vyvrhují	vyvrhovat	k5eAaImIp3nP	vyvrhovat
amoniak	amoniak	k1gInSc1	amoniak
a	a	k8xC	a
led	led	k1gInSc1	led
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k6eAd1wR	povrch
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
údolí	údolí	k1gNnPc1	údolí
a	a	k8xC	a
hřebeny	hřeben	k1gInPc1	hřeben
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nejspíše	nejspíše	k9	nejspíše
důsledek	důsledek	k1gInSc1	důsledek
tektoniky	tektonika	k1gFnSc2	tektonika
a	a	k8xC	a
ledového	ledový	k2eAgInSc2d1	ledový
vulkanismu	vulkanismus	k1gInSc2	vulkanismus
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
povrchových	povrchový	k2eAgInPc2d1	povrchový
útvarů	útvar	k1gInPc2	útvar
je	být	k5eAaImIp3nS	být
endogenního	endogenní	k2eAgInSc2d1	endogenní
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
spíše	spíše	k9	spíše
vulkanického	vulkanický	k2eAgInSc2d1	vulkanický
než	než	k8xS	než
tektonického	tektonický	k2eAgInSc2d1	tektonický
<g/>
.	.	kIx.	.
<g/>
Když	když	k8xS	když
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
zkoumal	zkoumat	k5eAaImAgInS	zkoumat
Triton	triton	k1gInSc1	triton
<g/>
,	,	kIx,	,
astronomové	astronom	k1gMnPc1	astronom
zaznamenali	zaznamenat	k5eAaPmAgMnP	zaznamenat
několik	několik	k4yIc4	několik
gejzírům	gejzír	k1gInPc3	gejzír
podobných	podobný	k2eAgFnPc2d1	podobná
erupcí	erupce	k1gFnPc2	erupce
špatně	špatně	k6eAd1	špatně
viditelného	viditelný	k2eAgInSc2d1	viditelný
dusíku	dusík	k1gInSc2	dusík
doprovázeného	doprovázený	k2eAgInSc2d1	doprovázený
prachovými	prachový	k2eAgFnPc7d1	prachová
částicemi	částice	k1gFnPc7	částice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
stoupaly	stoupat	k5eAaImAgFnP	stoupat
až	až	k9	až
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
8	[number]	k4	8
km	km	kA	km
nad	nad	k7c4	nad
povrch	povrch	k1gInSc4	povrch
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
Zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
měsíci	měsíc	k1gInSc6	měsíc
Io	Io	k1gMnPc2	Io
a	a	k8xC	a
Enceladem	Encelad	k1gInSc7	Encelad
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
Triton	triton	k1gInSc1	triton
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
známých	známý	k2eAgInPc2d1	známý
objektů	objekt	k1gInPc2	objekt
Sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
byla	být	k5eAaImAgFnS	být
pozorována	pozorován	k2eAgFnSc1d1	pozorována
aktivní	aktivní	k2eAgFnSc1d1	aktivní
vulkanická	vulkanický	k2eAgFnSc1d1	vulkanická
činnost	činnost	k1gFnSc1	činnost
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Venuše	Venuše	k1gFnSc1	Venuše
<g/>
,	,	kIx,	,
Mars	Mars	k1gInSc1	Mars
<g/>
,	,	kIx,	,
Europa	Europa	k1gFnSc1	Europa
<g/>
,	,	kIx,	,
Titan	titan	k1gInSc1	titan
a	a	k8xC	a
Dione	Dion	k1gInSc5	Dion
jsou	být	k5eAaImIp3nP	být
možná	možná	k9	možná
také	také	k9	také
vulkanicky	vulkanicky	k6eAd1	vulkanicky
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgInPc1	všechen
pozorované	pozorovaný	k2eAgInPc1d1	pozorovaný
gejzíry	gejzír	k1gInPc1	gejzír
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgInP	nacházet
mezi	mezi	k7c4	mezi
50	[number]	k4	50
<g/>
°	°	k?	°
a	a	k8xC	a
57	[number]	k4	57
<g/>
°	°	k?	°
jižní	jižní	k2eAgFnSc2d1	jižní
šířky	šířka	k1gFnSc2	šířka
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
blízká	blízký	k2eAgFnSc1d1	blízká
subsolárnímu	subsolární	k2eAgInSc3d1	subsolární
bodu	bod	k1gInSc3	bod
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
potřebné	potřebný	k2eAgNnSc1d1	potřebné
teplo	teplo	k1gNnSc1	teplo
je	být	k5eAaImIp3nS	být
dodávané	dodávaný	k2eAgNnSc1d1	dodávané
Sluncem	slunce	k1gNnSc7	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
povrch	povrch	k1gInSc1	povrch
Tritonu	triton	k1gInSc2	triton
tvoří	tvořit	k5eAaImIp3nS	tvořit
průsvitná	průsvitný	k2eAgFnSc1d1	průsvitná
vrstva	vrstva	k1gFnSc1	vrstva
zmrzlého	zmrzlý	k2eAgInSc2d1	zmrzlý
dusíku	dusík	k1gInSc2	dusík
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vrstva	vrstva	k1gFnSc1	vrstva
tmavšího	tmavý	k2eAgInSc2d2	tmavší
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
podobný	podobný	k2eAgInSc1d1	podobný
jev	jev	k1gInSc1	jev
jako	jako	k8xC	jako
skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšení	zvýšení	k1gNnSc1	zvýšení
teploty	teplota	k1gFnSc2	teplota
o	o	k7c4	o
pouhé	pouhý	k2eAgInPc4d1	pouhý
4	[number]	k4	4
K	k	k7c3	k
oproti	oproti	k7c3	oproti
okolní	okolní	k2eAgFnSc3d1	okolní
teplotě	teplota	k1gFnSc3	teplota
37	[number]	k4	37
K	K	kA	K
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
způsobit	způsobit	k5eAaPmF	způsobit
erupci	erupce	k1gFnSc4	erupce
pozorovaných	pozorovaný	k2eAgInPc2d1	pozorovaný
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
výtrysky	výtrysk	k1gInPc1	výtrysk
jsou	být	k5eAaImIp3nP	být
odlišné	odlišný	k2eAgInPc1d1	odlišný
od	od	k7c2	od
kryovulkanických	kryovulkanický	k2eAgInPc2d1	kryovulkanický
procesů	proces	k1gInPc2	proces
na	na	k7c6	na
jiných	jiný	k2eAgInPc6d1	jiný
objektech	objekt	k1gInPc6	objekt
Sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hraje	hrát	k5eAaImIp3nS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
teplo	teplo	k1gNnSc4	teplo
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
podobné	podobný	k2eAgInPc1d1	podobný
procesy	proces	k1gInPc1	proces
jako	jako	k8xC	jako
u	u	k7c2	u
Tritonu	triton	k1gInSc2	triton
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
<g/>
,	,	kIx,	,
probíhají	probíhat	k5eAaImIp3nP	probíhat
i	i	k9	i
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polární	polární	k2eAgFnSc6d1	polární
čepičce	čepička	k1gFnSc6	čepička
Marsu	Mars	k1gInSc2	Mars
během	během	k7c2	během
každého	každý	k3xTgNnSc2	každý
jara	jaro	k1gNnSc2	jaro
<g/>
.	.	kIx.	.
<g/>
Erupce	erupce	k1gFnSc1	erupce
gejzíru	gejzír	k1gInSc2	gejzír
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
až	až	k9	až
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Snímky	snímek	k1gInPc4	snímek
sondy	sonda	k1gFnSc2	sonda
Voyger	Voyger	k1gInSc1	Voyger
2	[number]	k4	2
ukázaly	ukázat	k5eAaPmAgInP	ukázat
množství	množství	k1gNnSc4	množství
oblastí	oblast	k1gFnPc2	oblast
s	s	k7c7	s
tmavým	tmavý	k2eAgInSc7d1	tmavý
materiálem	materiál	k1gInSc7	materiál
v	v	k7c6	v
oblastí	oblast	k1gFnPc2	oblast
gejzírů	gejzír	k1gInPc2	gejzír
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1977	[number]	k4	1977
a	a	k8xC	a
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
okolo	okolo	k7c2	okolo
měsíce	měsíc	k1gInSc2	měsíc
proletěl	proletět	k5eAaPmAgInS	proletět
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
ztratil	ztratit	k5eAaPmAgMnS	ztratit
Triton	triton	k1gMnSc1	triton
svou	svůj	k3xOyFgFnSc4	svůj
původní	původní	k2eAgFnSc4d1	původní
načervenalou	načervenalý	k2eAgFnSc4d1	načervenalá
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
zbledl	zblednout	k5eAaPmAgMnS	zblednout
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
těchto	tento	k3xDgNnPc2	tento
dvanácti	dvanáct	k4xCc2	dvanáct
let	léto	k1gNnPc2	léto
světlejší	světlý	k2eAgInSc1d2	světlejší
zamrzlý	zamrzlý	k2eAgInSc1d1	zamrzlý
dusík	dusík	k1gInSc1	dusík
pokryl	pokrýt	k5eAaPmAgInS	pokrýt
vrstvu	vrstva	k1gFnSc4	vrstva
červeného	červený	k2eAgInSc2d1	červený
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Polární	polární	k2eAgFnSc1d1	polární
čepička	čepička	k1gFnSc1	čepička
<g/>
,	,	kIx,	,
hřbety	hřbet	k1gInPc4	hřbet
a	a	k8xC	a
roviny	rovina	k1gFnPc4	rovina
====	====	k?	====
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
polární	polární	k2eAgFnSc1d1	polární
oblast	oblast	k1gFnSc1	oblast
měsíce	měsíc	k1gInSc2	měsíc
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
čepičkou	čepička	k1gFnSc7	čepička
zmrzlého	zmrzlý	k2eAgInSc2d1	zmrzlý
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
metanu	metan	k1gInSc2	metan
posetou	posetý	k2eAgFnSc7d1	posetá
impaktními	impaktní	k2eAgInPc7d1	impaktní
krátery	kráter	k1gInPc7	kráter
a	a	k8xC	a
gejzíry	gejzír	k1gInPc7	gejzír
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
severním	severní	k2eAgNnSc6d1	severní
pólu	pólo	k1gNnSc6	pólo
se	se	k3xPyFc4	se
toho	ten	k3xDgNnSc2	ten
ví	vědět	k5eAaImIp3nS	vědět
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
při	při	k7c6	při
průletu	průlet	k1gInSc6	průlet
sondy	sonda	k1gFnSc2	sonda
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
ale	ale	k9	ale
<g/>
,	,	kIx,	,
že	že	k8xS	že
Triton	triton	k1gMnSc1	triton
severní	severní	k2eAgFnSc4d1	severní
polární	polární	k2eAgFnSc4d1	polární
čepičku	čepička	k1gFnSc4	čepička
má	mít	k5eAaImIp3nS	mít
<g/>
.	.	kIx.	.
<g/>
Vyvýšené	vyvýšený	k2eAgFnPc1d1	vyvýšená
roviny	rovina	k1gFnPc1	rovina
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
polokouli	polokoule	k1gFnSc6	polokoule
Tritonu	triton	k1gInSc2	triton
překrývají	překrývat	k5eAaImIp3nP	překrývat
starší	starý	k2eAgInPc1d2	starší
útvary	útvar	k1gInPc1	útvar
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
výsledkem	výsledek	k1gInSc7	výsledek
vulkanických	vulkanický	k2eAgInPc2d1	vulkanický
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Roviny	rovina	k1gFnPc1	rovina
jsou	být	k5eAaImIp3nP	být
prosety	prosít	k5eAaPmNgFnP	prosít
prohlubněmi	prohlubeň	k1gFnPc7	prohlubeň
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterých	který	k3yRgInPc2	který
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
láva	láva	k1gFnSc1	láva
unikala	unikat	k5eAaImAgFnS	unikat
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
lávy	láva	k1gFnSc2	láva
není	být	k5eNaImIp3nS	být
s	s	k7c7	s
určitostí	určitost	k1gFnPc2	určitost
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejspíše	nejspíše	k9	nejspíše
to	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
směs	směs	k1gFnSc1	směs
amoniaku	amoniak	k1gInSc2	amoniak
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
Tritonu	triton	k1gInSc6	triton
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
čtyři	čtyři	k4xCgFnPc4	čtyři
roviny	rovina	k1gFnPc4	rovina
zhruba	zhruba	k6eAd1	zhruba
kruhového	kruhový	k2eAgInSc2d1	kruhový
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
zatím	zatím	k6eAd1	zatím
nejplošší	plochý	k2eAgNnPc1d3	plochý
objevená	objevený	k2eAgNnPc1d1	objevené
místa	místo	k1gNnPc1	místo
na	na	k7c6	na
měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgInSc1d1	maximální
rozdíl	rozdíl	k1gInSc1	rozdíl
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
těchto	tento	k3xDgFnPc2	tento
rovin	rovina	k1gFnPc2	rovina
je	být	k5eAaImIp3nS	být
200	[number]	k4	200
m.	m.	k?	m.
Vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
nejspíše	nejspíše	k9	nejspíše
erupcí	erupce	k1gFnPc2	erupce
ledové	ledový	k2eAgFnSc2d1	ledová
lávy	láva	k1gFnSc2	láva
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
se	se	k3xPyFc4	se
také	také	k9	také
nacházejí	nacházet	k5eAaImIp3nP	nacházet
spletité	spletitý	k2eAgFnPc1d1	spletitá
struktury	struktura	k1gFnPc1	struktura
tvořené	tvořený	k2eAgInPc4d1	tvořený
hřbety	hřbet	k1gInPc4	hřbet
a	a	k8xC	a
údolími	údolí	k1gNnPc7	údolí
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
kvůli	kvůli	k7c3	kvůli
střídajícím	střídající	k2eAgInPc3d1	střídající
se	se	k3xPyFc4	se
obdobím	období	k1gNnSc7	období
ochlazování	ochlazování	k1gNnPc2	ochlazování
a	a	k8xC	a
oteplování	oteplování	k1gNnSc2	oteplování
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
také	také	k9	také
možná	možná	k9	možná
tektonického	tektonický	k2eAgInSc2d1	tektonický
původu	původ	k1gInSc2	původ
a	a	k8xC	a
mohlo	moct	k5eAaImAgNnS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
horizontálním	horizontální	k2eAgInSc7d1	horizontální
zlomem	zlom	k1gInSc7	zlom
nebo	nebo	k8xC	nebo
riftem	rift	k1gInSc7	rift
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
vypadají	vypadat	k5eAaPmIp3nP	vypadat
jako	jako	k9	jako
hřbety	hřbet	k1gInPc4	hřbet
na	na	k7c6	na
Europě	Europa	k1gFnSc6	Europa
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
tak	tak	k6eAd1	tak
mít	mít	k5eAaImF	mít
stejný	stejný	k2eAgInSc4d1	stejný
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rovníkové	rovníkový	k2eAgFnSc6d1	Rovníková
oblasti	oblast	k1gFnSc6	oblast
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
zlomy	zlom	k1gInPc4	zlom
se	s	k7c7	s
souběžnými	souběžný	k2eAgNnPc7d1	souběžné
pohořími	pohoří	k1gNnPc7	pohoří
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
rýhy	rýha	k1gFnPc4	rýha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vrásčitý	vrásčitý	k2eAgInSc4d1	vrásčitý
povrch	povrch	k1gInSc4	povrch
====	====	k?	====
</s>
</p>
<p>
<s>
Západní	západní	k2eAgFnSc1d1	západní
polokoule	polokoule	k1gFnSc1	polokoule
Tritonu	triton	k1gInSc2	triton
je	být	k5eAaImIp3nS	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
směsicí	směsice	k1gFnSc7	směsice
útvarů	útvar	k1gInPc2	útvar
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
v	v	k7c6	v
celku	celek	k1gInSc6	celek
připomínají	připomínat	k5eAaImIp3nP	připomínat
povrch	povrch	k7c2wR	povrch
melounu	meloun	k1gInSc2	meloun
kantalupu	kantalup	k1gInSc2	kantalup
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
částí	část	k1gFnSc7	část
povrchu	povrch	k1gInSc2	povrch
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
většinu	většina	k1gFnSc4	většina
západní	západní	k2eAgFnSc2d1	západní
polokoule	polokoule	k1gFnSc2	polokoule
Tritonu	triton	k1gInSc2	triton
<g/>
.	.	kIx.	.
<g/>
Takovýto	takovýto	k3xDgInSc4	takovýto
vrásčitý	vrásčitý	k2eAgInSc4d1	vrásčitý
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
většiny	většina	k1gFnSc2	většina
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
znečištěného	znečištěný	k2eAgInSc2d1	znečištěný
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ze	z	k7c2	z
zatím	zatím	k6eAd1	zatím
objevených	objevený	k2eAgInPc2d1	objevený
objektů	objekt	k1gInPc2	objekt
objevuje	objevovat	k5eAaImIp3nS	objevovat
jen	jen	k9	jen
na	na	k7c6	na
Tritonu	triton	k1gInSc6	triton
<g/>
.	.	kIx.	.
</s>
<s>
Prolákliny	proláklina	k1gFnPc1	proláklina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
km	km	kA	km
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejsou	být	k5eNaImIp3nP	být
výsledkem	výsledek	k1gInSc7	výsledek
dopadu	dopad	k1gInSc2	dopad
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mají	mít	k5eAaImIp3nP	mít
podobnou	podobný	k2eAgFnSc4d1	podobná
velikost	velikost	k1gFnSc4	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgInP	vzniknout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
působením	působení	k1gNnSc7	působení
diapirismu	diapirismus	k1gInSc2	diapirismus
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
méně	málo	k6eAd2	málo
hustý	hustý	k2eAgInSc1d1	hustý
materiál	materiál	k1gInSc1	materiál
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
skrze	skrze	k?	skrze
hustší	hustý	k2eAgInSc1d2	hustší
materiál	materiál	k1gInSc1	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
teorie	teorie	k1gFnPc4	teorie
jejich	jejich	k3xOp3gInSc2	jejich
vzniku	vznik	k1gInSc2	vznik
patří	patřit	k5eAaImIp3nS	patřit
působení	působení	k1gNnSc4	působení
kryovulkanismu	kryovulkanismus	k1gInSc2	kryovulkanismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Impaktní	Impaktní	k2eAgInPc1d1	Impaktní
krátery	kráter	k1gInPc1	kráter
====	====	k?	====
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
neustálým	neustálý	k2eAgFnPc3d1	neustálá
geologickým	geologický	k2eAgFnPc3d1	geologická
aktivitám	aktivita	k1gFnPc3	aktivita
je	být	k5eAaImIp3nS	být
výskyt	výskyt	k1gInSc1	výskyt
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Tritonu	triton	k1gInSc2	triton
celkem	celkem	k6eAd1	celkem
výjimečný	výjimečný	k2eAgInSc1d1	výjimečný
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Voyager	Voyager	k1gMnSc1	Voyager
2	[number]	k4	2
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
krátery	kráter	k1gInPc4	kráter
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
měsíci	měsíc	k1gInSc6	měsíc
<g/>
,	,	kIx,	,
nalezl	nalézt	k5eAaBmAgMnS	nalézt
pouze	pouze	k6eAd1	pouze
179	[number]	k4	179
takových	takový	k3xDgInPc2	takový
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
nesporně	sporně	k6eNd1	sporně
impaktní	impaktní	k2eAgFnPc1d1	impaktní
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
na	na	k7c6	na
Uranově	Uranův	k2eAgInSc6d1	Uranův
měsíci	měsíc	k1gInSc6	měsíc
Mirandě	Miranda	k1gFnSc3	Miranda
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
povrch	povrch	k1gInSc4	povrch
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc4d1	velký
pouze	pouze	k6eAd1	pouze
3	[number]	k4	3
%	%	kIx~	%
povrchu	povrch	k1gInSc3	povrch
Tritonu	triton	k1gInSc2	triton
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
835	[number]	k4	835
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
nám	my	k3xPp1nPc3	my
známý	známý	k2eAgInSc1d1	známý
impaktní	impaktní	k2eAgInSc1d1	impaktní
kráter	kráter	k1gInSc1	kráter
na	na	k7c6	na
Tritonu	triton	k1gInSc6	triton
má	mít	k5eAaImIp3nS	mít
poloměr	poloměr	k1gInSc4	poloměr
27	[number]	k4	27
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
i	i	k8xC	i
větší	veliký	k2eAgInPc1d2	veliký
krátery	kráter	k1gInPc1	kráter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ty	ten	k3xDgInPc1	ten
nejspíše	nejspíše	k9	nejspíše
nejsou	být	k5eNaImIp3nP	být
impaktního	impaktní	k2eAgMnSc4d1	impaktní
původu	původa	k1gMnSc4	původa
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
vulkanického	vulkanický	k2eAgInSc2d1	vulkanický
<g/>
.	.	kIx.	.
<g/>
Většina	většina	k1gFnSc1	většina
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
je	být	k5eAaImIp3nS	být
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
oběhu	oběh	k1gInSc2	oběh
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
rovníku	rovník	k1gInSc2	rovník
mezi	mezi	k7c7	mezi
30	[number]	k4	30
<g/>
°	°	k?	°
a	a	k8xC	a
70	[number]	k4	70
<g/>
°	°	k?	°
zeměpisné	zeměpisný	k2eAgFnSc2d1	zeměpisná
délky	délka	k1gFnSc2	délka
<g/>
,	,	kIx,	,
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
v	v	k7c6	v
oběžné	oběžný	k2eAgFnSc6d1	oběžná
dráze	dráha	k1gFnSc6	dráha
Neptunu	Neptun	k1gInSc2	Neptun
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedna	jeden	k4xCgFnSc1	jeden
strana	strana	k1gFnSc1	strana
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
natočena	natočen	k2eAgFnSc1d1	natočena
k	k	k7c3	k
Neptunu	Neptun	k1gInSc3	Neptun
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
astronomové	astronom	k1gMnPc1	astronom
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
straně	strana	k1gFnSc6	strana
(	(	kIx(	(
<g/>
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
oběhu	oběh	k1gInSc3	oběh
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
impaktních	impaktní	k2eAgInPc2d1	impaktní
kráterů	kráter	k1gInPc2	kráter
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
Voyager	Voyager	k1gInSc1	Voyager
prozkoumal	prozkoumat	k5eAaPmAgInS	prozkoumat
jen	jen	k9	jen
40	[number]	k4	40
%	%	kIx~	%
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tento	tento	k3xDgInSc1	tento
závěr	závěr	k1gInSc1	závěr
zatím	zatím	k6eAd1	zatím
není	být	k5eNaImIp3nS	být
potvrzen	potvrzen	k2eAgInSc1d1	potvrzen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pozorování	pozorování	k1gNnPc1	pozorování
a	a	k8xC	a
průzkum	průzkum	k1gInSc1	průzkum
==	==	k?	==
</s>
</p>
<p>
<s>
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
oběžné	oběžný	k2eAgFnPc1d1	oběžná
dráhy	dráha	k1gFnPc1	dráha
Tritonu	triton	k1gInSc2	triton
byly	být	k5eAaImAgFnP	být
určeny	určit	k5eAaPmNgInP	určit
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
přesností	přesnost	k1gFnSc7	přesnost
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
objeveno	objevit	k5eAaPmNgNnS	objevit
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
retrográdní	retrográdní	k2eAgFnSc4d1	retrográdní
dráhu	dráha	k1gFnSc4	dráha
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
úhlem	úhel	k1gInSc7	úhel
inklinace	inklinace	k1gFnSc2	inklinace
k	k	k7c3	k
rovině	rovina	k1gFnSc3	rovina
Neptunova	Neptunův	k2eAgInSc2d1	Neptunův
oběhu	oběh	k1gInSc2	oběh
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
detailní	detailní	k2eAgNnSc1d1	detailní
pozorování	pozorování	k1gNnSc1	pozorování
samotného	samotný	k2eAgInSc2d1	samotný
měsíce	měsíc	k1gInSc2	měsíc
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
bylo	být	k5eAaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgMnS	být
koncem	konec	k1gInSc7	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pozorován	pozorovat	k5eAaImNgMnS	pozorovat
Voyagerem	Voyager	k1gInSc7	Voyager
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
Před	před	k7c7	před
Voyagerem	Voyager	k1gInSc7	Voyager
2	[number]	k4	2
si	se	k3xPyFc3	se
astronomové	astronom	k1gMnPc1	astronom
mysleli	myslet	k5eAaImAgMnP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
Tritonu	triton	k1gInSc6	triton
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
moře	moře	k1gNnSc4	moře
s	s	k7c7	s
tekutým	tekutý	k2eAgInSc7d1	tekutý
dusíkem	dusík	k1gInSc7	dusík
a	a	k8xC	a
atmosféra	atmosféra	k1gFnSc1	atmosféra
z	z	k7c2	z
dusíku	dusík	k1gInSc2	dusík
a	a	k8xC	a
methanu	methan	k1gInSc2	methan
s	s	k7c7	s
hustotou	hustota	k1gFnSc7	hustota
až	až	k9	až
30	[number]	k4	30
%	%	kIx~	%
hustoty	hustota	k1gFnSc2	hustota
atmosféry	atmosféra	k1gFnSc2	atmosféra
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
odhady	odhad	k1gInPc1	odhad
se	se	k3xPyFc4	se
ale	ale	k9	ale
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
býti	být	k5eAaImF	být
silně	silně	k6eAd1	silně
přeceněné	přeceněný	k2eAgNnSc1d1	přeceněné
<g/>
.	.	kIx.	.
</s>
<s>
Hustou	hustý	k2eAgFnSc4d1	hustá
atmosféru	atmosféra	k1gFnSc4	atmosféra
měl	mít	k5eAaImAgMnS	mít
měsíc	měsíc	k1gInSc4	měsíc
nejspíše	nejspíše	k9	nejspíše
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
vzniku	vznik	k1gInSc6	vznik
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgInSc1	první
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
změření	změření	k1gNnSc4	změření
průměru	průměr	k1gInSc2	průměr
měsíce	měsíc	k1gInSc2	měsíc
provedl	provést	k5eAaPmAgMnS	provést
Gerard	Gerard	k1gMnSc1	Gerard
Kuiper	Kuiper	k1gMnSc1	Kuiper
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1954	[number]	k4	1954
<g/>
.	.	kIx.	.
</s>
<s>
Naměřil	naměřit	k5eAaBmAgMnS	naměřit
hodnotu	hodnota	k1gFnSc4	hodnota
3800	[number]	k4	3800
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc4d1	další
měření	měření	k1gNnSc4	měření
dávala	dávat	k5eAaImAgFnS	dávat
hodnoty	hodnota	k1gFnPc4	hodnota
mezi	mezi	k7c7	mezi
2500	[number]	k4	2500
a	a	k8xC	a
6000	[number]	k4	6000
km	km	kA	km
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
od	od	k7c2	od
velikostí	velikost	k1gFnPc2	velikost
trochu	trochu	k6eAd1	trochu
menších	malý	k2eAgInPc2d2	menší
než	než	k8xS	než
průměr	průměr	k1gInSc1	průměr
Měsíce	měsíc	k1gInSc2	měsíc
po	po	k7c4	po
téměř	téměř	k6eAd1	téměř
polovinu	polovina	k1gFnSc4	polovina
průměru	průměr	k1gInSc2	průměr
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
z	z	k7c2	z
Voyageru	Voyager	k1gInSc2	Voyager
2	[number]	k4	2
z	z	k7c2	z
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1989	[number]	k4	1989
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
mnohem	mnohem	k6eAd1	mnohem
přesnějšímu	přesný	k2eAgInSc3d2	přesnější
odhadu	odhad	k1gInSc3	odhad
na	na	k7c4	na
2706	[number]	k4	2706
km	km	kA	km
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byla	být	k5eAaImAgNnP	být
provedena	proveden	k2eAgNnPc1d1	provedeno
pozorování	pozorování	k1gNnPc1	pozorování
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
vyšší	vysoký	k2eAgFnSc4d2	vyšší
hustotu	hustota	k1gFnSc4	hustota
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
než	než	k8xS	než
naměřil	naměřit	k5eAaBmAgInS	naměřit
Voyager	Voyager	k1gInSc1	Voyager
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
Nové	Nové	k2eAgInPc1d1	Nové
návrhy	návrh	k1gInPc1	návrh
na	na	k7c4	na
mise	mise	k1gFnPc4	mise
k	k	k7c3	k
Neptunovu	Neptunův	k2eAgInSc3d1	Neptunův
systému	systém	k1gInSc3	systém
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
uskutečněny	uskutečnit	k5eAaPmNgInP	uskutečnit
v	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
desetiletí	desetiletí	k1gNnSc6	desetiletí
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
předkládali	předkládat	k5eAaImAgMnP	předkládat
vědci	vědec	k1gMnPc1	vědec
NASA	NASA	kA	NASA
v	v	k7c6	v
několika	několik	k4yIc2	několik
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
počítaly	počítat	k5eAaImAgFnP	počítat
s	s	k7c7	s
Tritonem	triton	k1gInSc7	triton
jako	jako	k8xS	jako
s	s	k7c7	s
hlavním	hlavní	k2eAgInSc7d1	hlavní
cílem	cíl	k1gInSc7	cíl
a	a	k8xC	a
několik	několik	k4yIc1	několik
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
navrhovalo	navrhovat	k5eAaImAgNnS	navrhovat
možnost	možnost	k1gFnSc4	možnost
povrchové	povrchový	k2eAgFnSc2d1	povrchová
sondy	sonda	k1gFnSc2	sonda
(	(	kIx(	(
<g/>
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k8xC	jako
Huygens	Huygens	k1gInSc4	Huygens
na	na	k7c6	na
Titanu	titan	k1gInSc6	titan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
NASA	NASA	kA	NASA
se	se	k3xPyFc4	se
ale	ale	k9	ale
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
systémy	systém	k1gInPc4	systém
Jupiteru	Jupiter	k1gInSc2	Jupiter
a	a	k8xC	a
Saturnu	Saturn	k1gInSc2	Saturn
a	a	k8xC	a
na	na	k7c4	na
žádné	žádný	k3yNgInPc4	žádný
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
návrhů	návrh	k1gInPc2	návrh
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
finance	finance	k1gFnPc4	finance
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Triton	triton	k1gMnSc1	triton
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
humoristickém	humoristický	k2eAgNnSc6d1	humoristické
sci-fi	scii	k1gNnSc6	sci-fi
románu	román	k1gInSc2	román
Nekonečno	nekonečno	k1gNnSc1	nekonečno
vítá	vítat	k5eAaImIp3nS	vítat
ohleduplné	ohleduplný	k2eAgInPc4d1	ohleduplný
řidiče	řidič	k1gInPc4	řidič
dvojice	dvojice	k1gFnSc2	dvojice
britských	britský	k2eAgMnPc2d1	britský
autorů	autor	k1gMnPc2	autor
Rob	roba	k1gFnPc2	roba
Grant	grant	k1gInSc1	grant
a	a	k8xC	a
Doug	Doug	k1gMnSc1	Doug
Naylor	Naylor	k1gMnSc1	Naylor
je	být	k5eAaImIp3nS	být
Triton	triton	k1gInSc4	triton
cílovou	cílový	k2eAgFnSc7d1	cílová
destinací	destinace	k1gFnSc7	destinace
kosmické	kosmický	k2eAgFnSc2d1	kosmická
těžební	těžební	k2eAgFnSc2d1	těžební
lodi	loď	k1gFnSc2	loď
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Triton	triton	k1gMnSc1	triton
(	(	kIx(	(
<g/>
moon	moon	k1gMnSc1	moon
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Triton	triton	k1gInSc1	triton
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Triton	triton	k1gMnSc1	triton
Profile	profil	k1gInSc5	profil
at	at	k?	at
NASA	NASA	kA	NASA
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Solar	Solar	k1gInSc1	Solar
System	Syst	k1gInSc7	Syst
Exploration	Exploration	k1gInSc4	Exploration
site	sit	k1gInSc2	sit
</s>
</p>
<p>
<s>
Triton	triton	k1gMnSc1	triton
page	pag	k1gFnSc2	pag
at	at	k?	at
The	The	k1gFnSc2	The
Nine	Nin	k1gFnSc2	Nin
<g/>
8	[number]	k4	8
Planets	Planetsa	k1gFnPc2	Planetsa
</s>
</p>
<p>
<s>
Triton	triton	k1gMnSc1	triton
page	pag	k1gFnSc2	pag
at	at	k?	at
Views	Views	k1gInSc1	Views
of	of	k?	of
the	the	k?	the
Solar	Solar	k1gInSc1	Solar
System	Syst	k1gInSc7	Syst
</s>
</p>
<p>
<s>
Movie	Movie	k1gFnSc1	Movie
of	of	k?	of
Triton	triton	k1gInSc1	triton
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
rotation	rotation	k1gInSc1	rotation
from	from	k1gMnSc1	from
the	the	k?	the
National	National	k1gFnSc2	National
Oceanic	Oceanice	k1gFnPc2	Oceanice
and	and	k?	and
Atmospheric	Atmospherice	k1gFnPc2	Atmospherice
Administration	Administration	k1gInSc1	Administration
site	sit	k1gInSc2	sit
</s>
</p>
<p>
<s>
Triton	triton	k1gMnSc1	triton
images	images	k1gMnSc1	images
from	from	k1gMnSc1	from
Planetary	Planetara	k1gFnSc2	Planetara
Photojournal	Photojournal	k1gMnSc1	Photojournal
</s>
</p>
<p>
<s>
Gazetteer	Gazetteer	k1gMnSc1	Gazetteer
of	of	k?	of
Triton	triton	k1gMnSc1	triton
Nomenclature	Nomenclatur	k1gMnSc5	Nomenclatur
from	from	k1gInSc1	from
USGS	USGS	kA	USGS
Neptune	Neptun	k1gInSc5	Neptun
system	systo	k1gNnSc7	systo
page	pag	k1gInSc2	pag
</s>
</p>
<p>
<s>
Paul	Paul	k1gMnSc1	Paul
Schenk	Schenk	k1gMnSc1	Schenk
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
3D	[number]	k4	3D
images	images	k1gMnSc1	images
and	and	k?	and
flyover	flyover	k1gMnSc1	flyover
videos	videos	k1gMnSc1	videos
of	of	k?	of
Triton	triton	k1gMnSc1	triton
and	and	k?	and
other	other	k1gMnSc1	other
outer	outer	k1gMnSc1	outer
solar	solar	k1gMnSc1	solar
system	syst	k1gMnSc7	syst
satellites	satellites	k1gMnSc1	satellites
</s>
</p>
