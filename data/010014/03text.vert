<p>
<s>
Nosorožík	nosorožík	k1gMnSc1	nosorožík
kapucínek	kapucínek	k1gMnSc1	kapucínek
(	(	kIx(	(
<g/>
Oryctes	Oryctes	k1gMnSc1	Oryctes
nasicornis	nasicornis	k1gFnSc2	nasicornis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgMnSc1d1	velký
brouk	brouk	k1gMnSc1	brouk
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
vrubounovitých	vrubounovitý	k2eAgMnPc2d1	vrubounovitý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
chráněný	chráněný	k2eAgInSc4d1	chráněný
zákonem	zákon	k1gInSc7	zákon
jako	jako	k8xC	jako
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
2,5	[number]	k4	2,5
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
cm	cm	kA	cm
velkého	velký	k2eAgMnSc4d1	velký
brouka	brouk	k1gMnSc4	brouk
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
dozadu	dozadu	k6eAd1	dozadu
zahnutý	zahnutý	k2eAgInSc4d1	zahnutý
roh	roh	k1gInSc4	roh
<g/>
.	.	kIx.	.
</s>
<s>
Larvy	larva	k1gFnPc1	larva
jsou	být	k5eAaImIp3nP	být
žluté	žlutý	k2eAgFnPc1d1	žlutá
a	a	k8xC	a
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
délky	délka	k1gFnPc4	délka
až	až	k9	až
12	[number]	k4	12
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vajíčka	vajíčko	k1gNnPc1	vajíčko
nosorožíků	nosorožík	k1gMnPc2	nosorožík
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
velká	velký	k2eAgNnPc1d1	velké
<g/>
,	,	kIx,	,
běložlutě	běložlutě	k6eAd1	běložlutě
zbarvená	zbarvený	k2eAgFnSc1d1	zbarvená
a	a	k8xC	a
oválovitého	oválovitý	k2eAgInSc2d1	oválovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
můžeme	moct	k5eAaImIp1nP	moct
zřídka	zřídka	k6eAd1	zřídka
setkat	setkat	k5eAaPmF	setkat
od	od	k7c2	od
konce	konec	k1gInSc2	konec
jara	jaro	k1gNnSc2	jaro
do	do	k7c2	do
léta	léto	k1gNnSc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
soumračný	soumračný	k2eAgMnSc1d1	soumračný
tvor	tvor	k1gMnSc1	tvor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
den	den	k1gInSc4	den
skrývá	skrývat	k5eAaImIp3nS	skrývat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
úkrytu	úkryt	k1gInSc6	úkryt
<g/>
.	.	kIx.	.
</s>
<s>
Létá	létat	k5eAaImIp3nS	létat
zejména	zejména	k9	zejména
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
a	a	k8xC	a
v	v	k7c4	v
podvečer	podvečer	k1gInSc4	podvečer
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
může	moct	k5eAaImIp3nS	moct
nalétat	nalétat	k5eAaBmF	nalétat
na	na	k7c4	na
různé	různý	k2eAgInPc4d1	různý
zdroje	zdroj	k1gInPc4	zdroj
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Larvy	larva	k1gFnPc1	larva
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
trouchnivějícím	trouchnivějící	k2eAgNnSc6d1	trouchnivějící
dřevě	dřevo	k1gNnSc6	dřevo
a	a	k8xC	a
v	v	k7c6	v
pařeništích	pařeniště	k1gNnPc6	pařeniště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
vzácný	vzácný	k2eAgMnSc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
nosorožík	nosorožík	k1gMnSc1	nosorožík
kapucínek	kapucínka	k1gFnPc2	kapucínka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Oryctes	Oryctes	k1gInSc1	Oryctes
nasicornis	nasicornis	k1gInSc1	nasicornis
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Mapování	mapování	k1gNnSc1	mapování
výskytu	výskyt	k1gInSc2	výskyt
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
