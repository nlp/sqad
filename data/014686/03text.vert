<s>
Dub	dub	k1gInSc1
u	u	k7c2
Mokrosuk	Mokrosuka	k1gFnPc2
</s>
<s>
Památný	památný	k2eAgInSc1d1
strom	strom	k1gInSc1
(	(	kIx(
<g/>
zaniklý	zaniklý	k2eAgMnSc1d1
-	-	kIx~
chráněný	chráněný	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
Dub	Dub	k1gMnSc1
u	u	k7c2
Mokrosuk	Mokrosuk	k1gMnSc1
duben	duben	k1gInSc4
2017	#num#	k4
<g/>
Druh	druh	k1gInSc4
</s>
<s>
dub	dub	k1gInSc1
letníQuercus	letníQuercus	k1gMnSc1
robur	robur	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Evidenční	evidenční	k2eAgFnSc1d1
č.	č.	k?
</s>
<s>
102485	#num#	k4
(	(	kIx(
<g/>
9285	#num#	k4
<g/>
)	)	kIx)
Zánik	zánik	k1gInSc1
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2017	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Ochrana	ochrana	k1gFnSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
1985	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Význam	význam	k1gInSc1
ochrany	ochrana	k1gFnSc2
</s>
<s>
věk	věk	k1gInSc1
<g/>
,	,	kIx,
vzrůst	vzrůst	k1gInSc1
<g/>
,	,	kIx,
<g/>
krajinná	krajinný	k2eAgFnSc1d1
dominanta	dominanta	k1gFnSc1
Poloha	poloha	k1gFnSc1
Kraj	kraj	k7c2
</s>
<s>
Plzeňský	plzeňský	k2eAgInSc1d1
Okres	okres	k1gInSc1
</s>
<s>
Klatovy	Klatovy	k1gInPc1
Obec	obec	k1gFnSc1
</s>
<s>
Mokrosuky	Mokrosuk	k1gInPc1
Katastr	katastr	k1gInSc4
</s>
<s>
Mokrosuky	Mokrosuk	k1gInPc1
Poloha	poloha	k1gFnSc1
</s>
<s>
U	u	k7c2
svítivých	svítivý	k2eAgFnPc2d1
hlav	hlava	k1gFnPc2
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
510	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
16	#num#	k4
<g/>
′	′	k?
<g/>
27,13	27,13	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
°	°	k?
<g/>
27	#num#	k4
<g/>
′	′	k?
<g/>
45,2	45,2	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Dub	dub	k1gInSc1
u	u	k7c2
Mokrosuk	Mokrosuka	k1gFnPc2
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dub	dub	k1gInSc1
u	u	k7c2
Mokrosuk	Mokrosuka	k1gFnPc2
byl	být	k5eAaImAgInS
památný	památný	k2eAgInSc1d1
strom	strom	k1gInSc1
u	u	k7c2
vsi	ves	k1gFnSc2
Mokrosuky	Mokrosuk	k1gInPc1
<g/>
,	,	kIx,
severozápadně	severozápadně	k6eAd1
od	od	k7c2
Sušice	Sušice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přibližně	přibližně	k6eAd1
čtyřistaletý	čtyřistaletý	k2eAgInSc1d1
dub	dub	k1gInSc1
letní	letní	k2eAgInSc1d1
(	(	kIx(
<g/>
Quercus	Quercus	k1gMnSc1
robur	robur	k1gMnSc1
<g/>
)	)	kIx)
rostl	růst	k5eAaImAgInS
na	na	k7c6
východním	východní	k2eAgInSc6d1
konci	konec	k1gInSc6
vsi	ves	k1gFnSc2
u	u	k7c2
fotbalového	fotbalový	k2eAgNnSc2d1
hřiště	hřiště	k1gNnSc2
a	a	k8xC
křižovatky	křižovatka	k1gFnSc2
na	na	k7c4
Lešišov	Lešišov	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
510	#num#	k4
m.	m.	kA
Obvod	obvod	k1gInSc1
jeho	jeho	k3xPp3gInSc2
kmene	kmen	k1gInSc2
měřil	měřit	k5eAaImAgInS
495	#num#	k4
cm	cm	kA
a	a	k8xC
koruna	koruna	k1gFnSc1
stromu	strom	k1gInSc2
dosahovala	dosahovat	k5eAaImAgFnS
do	do	k7c2
výšky	výška	k1gFnSc2
19	#num#	k4
m	m	kA
(	(	kIx(
<g/>
měření	měření	k1gNnSc1
1997	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dub	dub	k1gInSc1
byl	být	k5eAaImAgInS
chráněn	chránit	k5eAaImNgInS
od	od	k7c2
roku	rok	k1gInSc2
1985	#num#	k4
pro	pro	k7c4
svůj	svůj	k3xOyFgInSc4
vzrůst	vzrůst	k1gInSc4
<g/>
,	,	kIx,
věk	věk	k1gInSc4
a	a	k8xC
jako	jako	k9
krajinná	krajinný	k2eAgFnSc1d1
dominanta	dominanta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dutý	dutý	k2eAgInSc1d1
kmen	kmen	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
minulosti	minulost	k1gFnSc6
vypálen	vypálen	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Padl	padnout	k5eAaImAgInS,k5eAaPmAgInS
v	v	k7c6
říjnu	říjen	k1gInSc6
2017	#num#	k4
během	během	k7c2
orkánu	orkán	k1gInSc2
Herwart	Herwarta	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
při	při	k7c6
600	#num#	k4
<g/>
.	.	kIx.
oslavách	oslava	k1gFnPc6
výročí	výročí	k1gNnSc2
založení	založení	k1gNnSc2
vsi	ves	k1gFnSc2
byl	být	k5eAaImAgInS
zasazen	zasazen	k2eAgInSc1d1
nový	nový	k2eAgInSc1d1
strom	strom	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
a	a	k8xC
pověsti	pověst	k1gFnPc1
</s>
<s>
Pod	pod	k7c7
stromem	strom	k1gInSc7
jsou	být	k5eAaImIp3nP
prý	prý	k9
pochováni	pochován	k2eAgMnPc1d1
švédští	švédský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
z	z	k7c2
třicetileté	třicetiletý	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Blízko	blízko	k7c2
stromu	strom	k1gInSc2
je	být	k5eAaImIp3nS
hrob	hrob	k1gInSc1
obětí	oběť	k1gFnPc2
moru	mor	k1gInSc2
ze	z	k7c2
Sušicka	Sušicko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těmto	tento	k3xDgFnPc3
místům	místo	k1gNnPc3
se	se	k3xPyFc4
proto	proto	k8xC
říká	říkat	k5eAaImIp3nS
Na	na	k7c6
hřbitově	hřbitov	k1gInSc6
<g/>
,	,	kIx,
podle	podle	k7c2
pověsti	pověst	k1gFnSc2
pak	pak	k6eAd1
U	u	k7c2
svítivých	svítivý	k2eAgFnPc2d1
hlav	hlava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prý	prý	k9
se	se	k3xPyFc4
tu	tu	k6eAd1
za	za	k7c4
noci	noc	k1gFnPc4
zjevují	zjevovat	k5eAaImIp3nP
kostlivci	kostlivec	k1gMnPc1
<g/>
,	,	kIx,
jimž	jenž	k3xRgMnPc3
z	z	k7c2
lebek	lebka	k1gFnPc2
svítí	svítit	k5eAaImIp3nS
ohnivé	ohnivý	k2eAgFnPc4d1
oči	oko	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgFnPc4d1
zajímavosti	zajímavost	k1gFnPc4
</s>
<s>
V	v	k7c4
podvečer	podvečer	k1gInSc4
7	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2005	#num#	k4
si	se	k3xPyFc3
dvě	dva	k4xCgFnPc4
asi	asi	k9
patnáctilé	patnáctilý	k2eAgFnPc1d1
dívky	dívka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
byly	být	k5eAaImAgFnP
v	v	k7c6
Mokrosukách	Mokrosuka	k1gFnPc6
u	u	k7c2
svých	svůj	k3xOyFgFnPc2
babiček	babička	k1gFnPc2
na	na	k7c6
prázdninách	prázdniny	k1gFnPc6
<g/>
,	,	kIx,
hrály	hrát	k5eAaImAgFnP
na	na	k7c6
sousedním	sousední	k2eAgNnSc6d1
hřišti	hřiště	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
po	po	k7c6
osmnácté	osmnáctý	k4xOgFnSc6
hodině	hodina	k1gFnSc6
začalo	začít	k5eAaPmAgNnS
pršet	pršet	k5eAaImF
<g/>
,	,	kIx,
dívky	dívka	k1gFnPc1
se	se	k3xPyFc4
přesunuly	přesunout	k5eAaPmAgFnP
pod	pod	k7c4
strom	strom	k1gInSc4
<g/>
,	,	kIx,
jedna	jeden	k4xCgFnSc1
se	se	k3xPyFc4
schovala	schovat	k5eAaPmAgFnS
do	do	k7c2
dutiny	dutina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spustilo	spustit	k5eAaPmAgNnS
se	se	k3xPyFc4
krupobití	krupobití	k1gNnSc1
<g/>
,	,	kIx,
do	do	k7c2
dubu	dub	k1gInSc2
sjel	sjet	k5eAaPmAgInS
blesk	blesk	k1gInSc1
<g/>
,	,	kIx,
první	první	k4xOgInSc1
v	v	k7c6
této	tento	k3xDgFnSc6
bouřce	bouřka	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
zasáhl	zasáhnout	k5eAaPmAgMnS
obě	dva	k4xCgFnPc4
dívky	dívka	k1gFnPc4
–	–	k?
jednu	jeden	k4xCgFnSc4
vyhodil	vyhodit	k5eAaPmAgMnS
z	z	k7c2
dutiny	dutina	k1gFnSc2
kmenu	kmen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dub	dub	k1gInSc1
začal	začít	k5eAaPmAgInS
hořet	hořet	k5eAaImF
a	a	k8xC
svědci	svědek	k1gMnPc1
resuscitovali	resuscitovat	k5eAaImAgMnP
obě	dva	k4xCgFnPc4
dívky	dívka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
byla	být	k5eAaImAgFnS
převezena	převézt	k5eAaPmNgFnS
při	při	k7c6
vědomí	vědomí	k1gNnSc6
do	do	k7c2
nemocnice	nemocnice	k1gFnSc2
v	v	k7c6
Klatovech	Klatovy	k1gInPc6
<g/>
,	,	kIx,
druhá	druhý	k4xOgFnSc1
(	(	kIx(
<g/>
14	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
v	v	k7c6
bezvědomí	bezvědomí	k1gNnSc6
a	a	k8xC
se	s	k7c7
selhávajícími	selhávající	k2eAgFnPc7d1
životními	životní	k2eAgFnPc7d1
funkcemi	funkce	k1gFnPc7
přepravena	přepravit	k5eAaPmNgFnS
letecky	letecky	k6eAd1
do	do	k7c2
Fakultní	fakultní	k2eAgFnSc2d1
nemocnice	nemocnice	k1gFnSc2
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
byli	být	k5eAaImAgMnP
zraněni	zranit	k5eAaPmNgMnP
dva	dva	k4xCgMnPc1
chlapci	chlapec	k1gMnPc1
<g/>
,	,	kIx,
kterým	který	k3yQgMnPc3,k3yRgMnPc3,k3yIgMnPc3
blesk	blesk	k1gInSc1
způsobil	způsobit	k5eAaPmAgInS
popáleniny	popálenina	k1gFnPc4
na	na	k7c6
nohou	noha	k1gFnPc6
a	a	k8xC
brnění	brnění	k1gNnSc3
prstů	prst	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hasiči	hasič	k1gMnPc1
během	běh	k1gInSc7
téměř	téměř	k6eAd1
dvouhodinového	dvouhodinový	k2eAgInSc2d1
zásahu	zásah	k1gInSc2
dub	dub	k1gInSc4
uhasili	uhasit	k5eAaPmAgMnP
ze	z	k7c2
žebříku	žebřík	k1gInSc2
<g/>
,	,	kIx,
také	také	k9
vydlabali	vydlabat	k5eAaPmAgMnP
doutnající	doutnající	k2eAgNnSc4d1
dřevo	dřevo	k1gNnSc4
v	v	k7c6
dutině	dutina	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dub	Dub	k1gMnSc1
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgMnS
ve	v	k7c6
14	#num#	k4
<g/>
.	.	kIx.
dílu	díl	k1gInSc2
pořadu	pořad	k1gInSc2
Paměť	paměť	k1gFnSc1
stromů	strom	k1gInPc2
České	český	k2eAgFnSc2d1
televize	televize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Památné	památný	k2eAgInPc1d1
a	a	k8xC
významné	významný	k2eAgInPc1d1
stromy	strom	k1gInPc1
v	v	k7c6
okolí	okolí	k1gNnSc6
</s>
<s>
Lípa	lípa	k1gFnSc1
u	u	k7c2
Zelených	Zelených	k2eAgMnPc2d1
</s>
<s>
Lešišovská	Lešišovský	k2eAgFnSc1d1
lípa	lípa	k1gFnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Registr	registr	k1gInSc4
památných	památný	k2eAgInPc2d1
stromů	strom	k1gInPc2
AOPK	AOPK	kA
ČR	ČR	kA
drusop	drusop	k1gInSc4
<g/>
.	.	kIx.
<g/>
nature	natur	k1gMnSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
1	#num#	k4
2	#num#	k4
Dům	dům	k1gInSc1
dětí	dítě	k1gFnPc2
a	a	k8xC
mládeže	mládež	k1gFnSc2
<g/>
,	,	kIx,
Centrum	centrum	k1gNnSc1
volného	volný	k2eAgInSc2d1
času	čas	k1gInSc2
Kralupy	Kralupy	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smutná	Smutná	k1gFnSc1
zpráva	zpráva	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dům	dům	k1gInSc1
dětí	dítě	k1gFnPc2
a	a	k8xC
mládeže	mládež	k1gFnSc2
<g/>
,	,	kIx,
Centrum	centrum	k1gNnSc1
volného	volný	k2eAgInSc2d1
času	čas	k1gInSc2
Kralupy	Kralupy	k1gInPc4
<g/>
,	,	kIx,
2017	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
HRUŠKOVÁ	Hrušková	k1gFnSc1
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památné	památný	k2eAgInPc1d1
stromy	strom	k1gInPc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ilustrace	ilustrace	k1gFnSc1
Jaroslav	Jaroslav	k1gMnSc1
Turek	Turek	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Silva	Silva	k1gFnSc1
Regina	Regina	k1gFnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
192	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902033	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svojšice	Svojšice	k1gFnPc1
nejsou	být	k5eNaImIp3nP
jen	jen	k9
jedny	jeden	k4xCgInPc4
<g/>
,	,	kIx,
s.	s.	k?
133	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
NĚMEC	Němec	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Památné	památný	k2eAgInPc1d1
stromy	strom	k1gInPc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
Slezsku	Slezsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Olympia	Olympia	k1gFnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
224	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7033	#num#	k4
<g/>
-	-	kIx~
<g/>
781	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Západní	západní	k2eAgFnPc1d1
Čechy	Čechy	k1gFnPc1
<g/>
,	,	kIx,
s.	s.	k?
100	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Do	do	k7c2
dvou	dva	k4xCgFnPc2
dívek	dívka	k1gFnPc2
uhodil	uhodit	k5eAaPmAgInS
blesk	blesk	k1gInSc1
zpravy	zprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Elektrický	elektrický	k2eAgInSc4d1
výboj	výboj	k1gInSc4
zranil	zranit	k5eAaPmAgMnS
čtyři	čtyři	k4xCgFnPc4
děti	dítě	k1gFnPc4
blesk	blesk	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Fotografie	fotografia	k1gFnPc1
dubu	dub	k1gInSc2
</s>
<s>
Do	do	k7c2
dvou	dva	k4xCgFnPc2
dívek	dívka	k1gFnPc2
uhodil	uhodit	k5eAaPmAgInS
blesk	blesk	k1gInSc1
(	(	kIx(
<g/>
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Stromy	strom	k1gInPc1
</s>
