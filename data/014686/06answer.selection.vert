<s desamb="1">
Přibližně	přibližně	k6eAd1
čtyřistaletý	čtyřistaletý	k2eAgInSc1d1
dub	dub	k1gInSc1
letní	letní	k2eAgInSc1d1
(	(	kIx(
<g/>
Quercus	Quercus	k1gMnSc1
robur	robur	k1gMnSc1
<g/>
)	)	kIx)
rostl	růst	k5eAaImAgInS
na	na	k7c6
východním	východní	k2eAgInSc6d1
konci	konec	k1gInSc6
vsi	ves	k1gFnSc2
u	u	k7c2
fotbalového	fotbalový	k2eAgNnSc2d1
hřiště	hřiště	k1gNnSc2
a	a	k8xC
křižovatky	křižovatka	k1gFnSc2
na	na	k7c4
Lešišov	Lešišov	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
510	#num#	k4
m.	m.	kA
Obvod	obvod	k1gInSc1
jeho	jeho	k3xPp3gInSc2
kmene	kmen	k1gInSc2
měřil	měřit	k5eAaImAgInS
495	#num#	k4
cm	cm	kA
a	a	k8xC
koruna	koruna	k1gFnSc1
stromu	strom	k1gInSc2
dosahovala	dosahovat	k5eAaImAgFnS
do	do	k7c2
výšky	výška	k1gFnSc2
19	#num#	k4
m	m	kA
(	(	kIx(
<g/>
měření	měření	k1gNnSc1
1997	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>