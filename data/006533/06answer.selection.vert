<s>
byla	být	k5eAaImAgFnS	být
pravidlem	pravidlo	k1gNnSc7	pravidlo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
mi	já	k3xPp1nSc3	já
pomáhalo	pomáhat	k5eAaImAgNnS	pomáhat
nalézat	nalézat	k5eAaImF	nalézat
další	další	k2eAgNnSc1d1	další
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Descartes	Descartes	k1gMnSc1	Descartes
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
Boha	bůh	k1gMnSc4	bůh
jakožto	jakožto	k8xS	jakožto
nekonečnou	konečný	k2eNgFnSc4d1	nekonečná
a	a	k8xC	a
nestvořenou	stvořený	k2eNgFnSc4d1	nestvořená
substanci	substance	k1gFnSc4	substance
<g/>
,	,	kIx,	,
a	a	k8xC	a
vedle	vedle	k7c2	vedle
něho	on	k3xPp3gMnSc2	on
jen	jen	k9	jen
dvě	dva	k4xCgFnPc1	dva
substance	substance	k1gFnPc1	substance
stvořené	stvořený	k2eAgFnPc1d1	stvořená
<g/>
:	:	kIx,	:
res	res	k?	res
extensa	extensa	k1gFnSc1	extensa
<g/>
,	,	kIx,	,
svět	svět	k1gInSc1	svět
těles	těleso	k1gNnPc2	těleso
<g/>
,	,	kIx,	,
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
substanci	substance	k1gFnSc4	substance
s	s	k7c7	s
hlavním	hlavní	k2eAgInSc7d1	hlavní
atributem	atribut	k1gInSc7	atribut
rozprostraněnosti	rozprostraněnost	k1gFnSc2	rozprostraněnost
<g/>
;	;	kIx,	;
res	res	k?	res
cogitans	cogitans	k1gInSc4	cogitans
<g/>
,	,	kIx,	,
nerozprostraněnou	rozprostraněný	k2eNgFnSc4d1	rozprostraněný
duchovní	duchovní	k2eAgFnSc4d1	duchovní
substanci	substance	k1gFnSc4	substance
s	s	k7c7	s
atributem	atribut	k1gInSc7	atribut
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
,	,	kIx,	,
neprostorový	prostorový	k2eNgMnSc1d1	prostorový
a	a	k8xC	a
netělesný	tělesný	k2eNgMnSc1d1	netělesný
duch	duch	k1gMnSc1	duch
<g/>
,	,	kIx,	,
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
.	.	kIx.	.
</s>
