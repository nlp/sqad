<s>
Kentucky	Kentucka	k1gFnPc1
(	(	kIx(
<g/>
anglická	anglický	k2eAgFnSc1d1
výslovnost	výslovnost	k1gFnSc1
[	[	kIx(
<g/>
kə	kə	k1gNnSc1
<g/>
]	]	kIx)
<g/>
IPA	IPA	kA
<g/>
,	,	kIx,
oficiálně	oficiálně	k6eAd1
Commonwealth	Commonwealth	k1gInSc1
of	of	k?
Kentucky	Kentucka	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
unijní	unijní	k2eAgInSc4d1
stát	stát	k1gInSc4
nacházející	nacházející	k2eAgInSc4d1
se	se	k3xPyFc4
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
jižní	jižní	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>