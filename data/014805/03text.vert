<s>
Kentucky	Kentucky	k6eAd1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
státu	stát	k1gInSc6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Kentucky	Kentucka	k1gFnSc2
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Kentucky	Kentucky	k6eAd1
</s>
<s>
Commonwealth	Commonwealth	k1gInSc1
of	of	k?
Kentucky	Kentucka	k1gFnSc2
</s>
<s>
Vlajka	vlajka	k1gFnSc1
</s>
<s>
Pečeť	pečeť	k1gFnSc1
</s>
<s>
Přezdívka	přezdívka	k1gFnSc1
<g/>
:	:	kIx,
Bluegrass	bluegrass	k1gInSc1
State	status	k1gInSc5
</s>
<s>
Úřední	úřední	k2eAgFnSc1d1
jazykyangličtina	jazykyangličtina	k1gFnSc1
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1
městoFrankfort	městoFrankfort	k1gInSc1
</s>
<s>
Největší	veliký	k2eAgInSc1d3
městoLouisville	městoLouisville	k1gInSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
•	•	k?
Celkem	celkem	k6eAd1
•	•	k?
z	z	k7c2
toho	ten	k3xDgNnSc2
souš	souš	k1gFnSc1
•	•	k?
z	z	k7c2
toho	ten	k3xDgNnSc2
vodstvo	vodstvo	k1gNnSc1
</s>
<s>
37	#num#	k4
<g/>
.	.	kIx.
v	v	k7c6
USA104	USA104	k1gFnSc6
659	#num#	k4
km²	km²	k?
<g/>
102	#num#	k4
880	#num#	k4
km²	km²	k?
<g/>
1	#num#	k4
779	#num#	k4
km²	km²	k?
(	(	kIx(
<g/>
1,7	1,7	k4
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc4
•	•	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
•	•	k?
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
v	v	k7c6
USA	USA	kA
4	#num#	k4
436	#num#	k4
974	#num#	k4
(	(	kIx(
<g/>
odhad	odhad	k1gInSc1
2016	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
43	#num#	k4
obyv	obyva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
v	v	k7c6
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
Počet	počet	k1gInSc1
okresů	okres	k1gInPc2
</s>
<s>
120	#num#	k4
</s>
<s>
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
východní	východní	k2eAgFnSc2d1
Kentucky	Kentucka	k1gFnSc2
<g/>
:	:	kIx,
<g/>
UTC	UTC	kA
<g/>
−	−	k?
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
−	−	k?
<g/>
4	#num#	k4
(	(	kIx(
<g/>
letní	letní	k2eAgInSc1d1
čas	čas	k1gInSc1
<g/>
)	)	kIx)
<g/>
západní	západní	k2eAgFnSc2d1
Kentucky	Kentucka	k1gFnSc2
<g/>
:	:	kIx,
<g/>
UTC	UTC	kA
<g/>
−	−	k?
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
−	−	k?
<g/>
5	#num#	k4
(	(	kIx(
<g/>
letní	letní	k2eAgInSc4d1
čas	čas	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Poloha	poloha	k1gFnSc1
</s>
<s>
37	#num#	k4
<g/>
°	°	k?
<g/>
36	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
85	#num#	k4
<g/>
°	°	k?
<g/>
18	#num#	k4
<g/>
′	′	k?
z.	z.	k?
d.	d.	k?
</s>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
•	•	k?
Nejvyšší	vysoký	k2eAgInSc1d3
bod	bod	k1gInSc1
•	•	k?
Průměrná	průměrný	k2eAgFnSc1d1
výška	výška	k1gFnSc1
•	•	k?
Nejnižší	nízký	k2eAgInSc4d3
bod	bod	k1gInSc4
</s>
<s>
1263	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
230	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
78	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
</s>
<s>
GuvernérAndy	GuvernérAnd	k1gInPc1
Beshear	Besheara	k1gFnPc2
(	(	kIx(
<g/>
D	D	kA
<g/>
)	)	kIx)
</s>
<s>
SenátořiMitch	SenátořiMitch	k1gMnSc1
McConnell	McConnell	k1gMnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
<g/>
Rand	rand	k1gInSc1
Paul	Paul	k1gMnSc1
(	(	kIx(
<g/>
R	R	kA
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
zkratky	zkratka	k1gFnPc1
•	•	k?
Poštovní	poštovní	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
•	•	k?
Tradiční	tradiční	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
•	•	k?
ISO	ISO	kA
3166-2	3166-2	k4
</s>
<s>
KYKy	KYKy	k1gInPc1
<g/>
.	.	kIx.
<g/>
US-KY	US-KY	k1gFnSc1
</s>
<s>
Přistoupení	přistoupení	k1gNnSc1
do	do	k7c2
Unie	unie	k1gFnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1792	#num#	k4
(	(	kIx(
<g/>
15	#num#	k4
<g/>
.	.	kIx.
stát	stát	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
kentucky	kentucka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
gov	gov	k?
</s>
<s>
Kentucky	Kentucka	k1gFnPc1
(	(	kIx(
<g/>
anglická	anglický	k2eAgFnSc1d1
výslovnost	výslovnost	k1gFnSc1
[	[	kIx(
<g/>
kə	kə	k1gNnSc1
<g/>
]	]	kIx)
<g/>
IPA	IPA	kA
<g/>
,	,	kIx,
oficiálně	oficiálně	k6eAd1
Commonwealth	Commonwealth	k1gInSc1
of	of	k?
Kentucky	Kentucka	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
unijní	unijní	k2eAgInSc4d1
stát	stát	k1gInSc4
nacházející	nacházející	k2eAgInSc4d1
se	se	k3xPyFc4
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
jižní	jižní	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kentucky	Kentucka	k1gFnPc1
hraničí	hraničit	k5eAaImIp3nP
na	na	k7c6
severu	sever	k1gInSc6
s	s	k7c7
Indianou	Indiana	k1gFnSc7
a	a	k8xC
Ohiem	Ohio	k1gNnSc7
<g/>
,	,	kIx,
na	na	k7c6
východě	východ	k1gInSc6
se	s	k7c7
Západní	západní	k2eAgFnSc7d1
Virginií	Virginie	k1gFnSc7
a	a	k8xC
Virginií	Virginie	k1gFnSc7
<g/>
,	,	kIx,
na	na	k7c6
jihu	jih	k1gInSc6
s	s	k7c7
Tennessee	Tennessee	k1gFnSc7
a	a	k8xC
na	na	k7c6
západě	západ	k1gInSc6
se	s	k7c7
státy	stát	k1gInPc7
Missouri	Missouri	k1gFnSc2
a	a	k8xC
Illinois	Illinois	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Se	s	k7c7
svou	svůj	k3xOyFgFnSc7
rozlohou	rozloha	k1gFnSc7
104	#num#	k4
659	#num#	k4
km²	km²	k?
je	být	k5eAaImIp3nS
Kentucky	Kentucka	k1gFnSc2
37	#num#	k4
<g/>
.	.	kIx.
největším	veliký	k2eAgInSc7d3
státem	stát	k1gInSc7
USA	USA	kA
<g/>
,	,	kIx,
v	v	k7c6
počtu	počet	k1gInSc6
obyvatel	obyvatel	k1gMnPc2
(	(	kIx(
<g/>
4,4	4,4	k4
milionu	milion	k4xCgInSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
26	#num#	k4
<g/>
.	.	kIx.
nejlidnatějším	lidnatý	k2eAgInSc7d3
státem	stát	k1gInSc7
a	a	k8xC
s	s	k7c7
hodnotou	hodnota	k1gFnSc7
hustoty	hustota	k1gFnSc2
zalidnění	zalidnění	k1gNnSc2
43	#num#	k4
obyvatel	obyvatel	k1gMnPc2
na	na	k7c6
km²	km²	k?
je	být	k5eAaImIp3nS
na	na	k7c4
22	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
je	být	k5eAaImIp3nS
Frankfort	Frankfort	k1gInSc1
s	s	k7c7
25	#num#	k4
tisíci	tisíc	k4xCgInPc7
obyvateli	obyvatel	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největšími	veliký	k2eAgMnPc7d3
městy	město	k1gNnPc7
jsou	být	k5eAaImIp3nP
Louisville	Louisville	k1gNnSc4
s	s	k7c7
610	#num#	k4
tisíci	tisíc	k4xCgInPc7
obyvateli	obyvatel	k1gMnPc7
<g/>
,	,	kIx,
dále	daleko	k6eAd2
Lexington	Lexington	k1gInSc1
(	(	kIx(
<g/>
310	#num#	k4
tisíc	tisíc	k4xCgInSc4
obyv	obyvum	k1gNnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Bowling	bowling	k1gInSc1
Green	Green	k1gInSc1
(	(	kIx(
<g/>
60	#num#	k4
tisíc	tisíc	k4xCgInSc4
obyv	obyvum	k1gNnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Owensboro	Owensbora	k1gFnSc5
(	(	kIx(
<g/>
60	#num#	k4
tisíc	tisíc	k4xCgInSc4
obyv	obyvum	k1gNnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
a	a	k8xC
Covington	Covington	k1gInSc1
(	(	kIx(
<g/>
40	#num#	k4
tisíc	tisíc	k4xCgInSc4
obyv	obyvum	k1gNnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšším	vysoký	k2eAgInSc7d3
bodem	bod	k1gInSc7
státu	stát	k1gInSc2
je	být	k5eAaImIp3nS
vrchol	vrchol	k1gInSc1
Black	Blacka	k1gFnPc2
Mountain	Mountain	k1gInSc4
s	s	k7c7
nadmořskou	nadmořský	k2eAgFnSc7d1
výškou	výška	k1gFnSc7
1263	#num#	k4
m	m	kA
v	v	k7c6
pohoří	pohoří	k1gNnSc6
Cumberland	Cumberlando	k1gNnPc2
Mountains	Mountainsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největšími	veliký	k2eAgInPc7d3
toky	tok	k1gInPc7
jsou	být	k5eAaImIp3nP
řeky	řeka	k1gFnSc2
Mississippi	Mississippi	k1gFnSc2
<g/>
,	,	kIx,
tvořící	tvořící	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
s	s	k7c7
Missouri	Missouri	k1gFnSc7
<g/>
,	,	kIx,
Ohio	Ohio	k1gNnSc1
<g/>
,	,	kIx,
vytvářející	vytvářející	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
s	s	k7c7
Ohiem	Ohio	k1gNnSc7
<g/>
,	,	kIx,
Indianou	Indiana	k1gFnSc7
a	a	k8xC
Illinois	Illinois	k1gFnPc2
<g/>
,	,	kIx,
Tennessee	Tennessee	k1gFnPc2
<g/>
,	,	kIx,
Cumberland	Cumberlanda	k1gFnPc2
a	a	k8xC
Kentucky	Kentucka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Území	území	k1gNnSc1
dnešního	dnešní	k2eAgInSc2d1
státu	stát	k1gInSc2
Kentucky	Kentucka	k1gFnSc2
bylo	být	k5eAaImAgNnS
před	před	k7c7
příchodem	příchod	k1gInSc7
evropských	evropský	k2eAgMnPc2d1
osadníků	osadník	k1gMnPc2
osídleno	osídlit	k5eAaPmNgNnS
indiánskými	indiánský	k2eAgInPc7d1
kmeny	kmen	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jihu	jih	k1gInSc6
to	ten	k3xDgNnSc4
byli	být	k5eAaImAgMnP
Čikasavové	Čikasavový	k2eAgFnSc6d1
a	a	k8xC
Kríkové	Kríkový	k2eAgFnSc6d1
<g/>
,	,	kIx,
na	na	k7c6
jihovýchodě	jihovýchod	k1gInSc6
Čerokíové	Čerokíová	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
Šavanové	Šavanová	k1gFnSc2
<g/>
,	,	kIx,
ve	v	k7c6
střední	střední	k2eAgFnSc6d1
části	část	k1gFnSc6
kmen	kmen	k1gInSc1
Tutelo	tutela	k1gFnSc5
<g/>
,	,	kIx,
hovořící	hovořící	k2eAgInSc1d1
siuským	siuský	k2eAgInSc7d1
jazykem	jazyk	k1gInSc7
<g/>
,	,	kIx,
na	na	k7c4
severovýchod	severovýchod	k1gInSc4
zasahovali	zasahovat	k5eAaImAgMnP
Irokézové	Irokéz	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
území	území	k1gNnSc6
pozdějšího	pozdní	k2eAgMnSc2d2
Kentucky	Kentucka	k1gFnPc1
první	první	k4xOgMnPc1
evropští	evropský	k2eAgMnPc1d1
průzkumníci	průzkumník	k1gMnPc1
prokazatelně	prokazatelně	k6eAd1
dorazili	dorazit	k5eAaPmAgMnP
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stálá	stálý	k2eAgFnSc1d1
britská	britský	k2eAgFnSc1d1
osídlení	osídlení	k1gNnSc6
začala	začít	k5eAaPmAgNnP
vznikat	vznikat	k5eAaImF
až	až	k9
o	o	k7c6
století	století	k1gNnSc6
později	pozdě	k6eAd2
–	–	k?
první	první	k4xOgInSc4
v	v	k7c6
roce	rok	k1gInSc6
1774	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblast	oblast	k1gFnSc1
byla	být	k5eAaImAgFnS
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
součástí	součást	k1gFnPc2
Virginie	Virginie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
zde	zde	k6eAd1
zřídila	zřídit	k5eAaPmAgFnS
okres	okres	k1gInSc4
Kentucky	Kentucka	k1gFnSc2
(	(	kIx(
<g/>
Kentucky	Kentucka	k1gFnSc2
County	Counta	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
pravděpodobně	pravděpodobně	k6eAd1
získal	získat	k5eAaPmAgMnS
své	svůj	k3xOyFgNnSc4
jméno	jméno	k1gNnSc4
z	z	k7c2
irokézského	irokézský	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
kenhtà	kenhtà	k?
<g/>
:	:	kIx,
<g/>
ke	k	k7c3
<g/>
,	,	kIx,
označující	označující	k2eAgFnSc4d1
louku	louka	k1gFnSc4
nebo	nebo	k8xC
prérii	prérie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
se	se	k3xPyFc4
region	region	k1gInSc1
z	z	k7c2
Virginie	Virginie	k1gFnSc2
vydělil	vydělit	k5eAaPmAgInS
a	a	k8xC
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1792	#num#	k4
se	se	k3xPyFc4
Kentucky	Kentucka	k1gFnPc4
stalo	stát	k5eAaPmAgNnS
15	#num#	k4
<g/>
.	.	kIx.
státem	stát	k1gInSc7
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
</s>
<s>
Podle	podle	k7c2
sčítání	sčítání	k1gNnSc2
lidu	lid	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
4	#num#	k4
339	#num#	k4
367	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Území	území	k1gNnSc1
bylo	být	k5eAaImAgNnS
postupně	postupně	k6eAd1
osidlováno	osidlovat	k5eAaImNgNnS
osadníky	osadník	k1gMnPc7
koncem	koncem	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
bojů	boj	k1gInPc2
s	s	k7c7
Indiány	Indián	k1gMnPc7
se	se	k3xPyFc4
zde	zde	k6eAd1
účastnil	účastnit	k5eAaImAgInS
i	i	k9
George	George	k1gInSc1
Washington	Washington	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obyvatelé	obyvatel	k1gMnPc1
jsou	být	k5eAaImIp3nP
především	především	k6eAd1
britského	britský	k2eAgInSc2d1
původu	původ	k1gInSc2
i	i	k9
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
většinou	většinou	k6eAd1
žijí	žít	k5eAaImIp3nP
na	na	k7c6
venkově	venkov	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Rasové	rasový	k2eAgNnSc1d1
složení	složení	k1gNnSc1
</s>
<s>
87,8	87,8	k4
%	%	kIx~
Bílí	bílý	k2eAgMnPc1d1
Američané	Američan	k1gMnPc1
</s>
<s>
7,8	7,8	k4
%	%	kIx~
Afroameričané	Afroameričan	k1gMnPc1
</s>
<s>
0,2	0,2	k4
%	%	kIx~
Američtí	americký	k2eAgMnPc1d1
indiáni	indián	k1gMnPc1
</s>
<s>
1,1	1,1	k4
%	%	kIx~
Asijští	asijský	k2eAgMnPc1d1
Američané	Američan	k1gMnPc1
</s>
<s>
0,1	0,1	k4
%	%	kIx~
Pacifičtí	pacifický	k2eAgMnPc1d1
ostrované	ostrovan	k1gMnPc1
</s>
<s>
1,3	1,3	k4
%	%	kIx~
Jiná	jiný	k2eAgFnSc1d1
rasa	rasa	k1gFnSc1
</s>
<s>
1,7	1,7	k4
%	%	kIx~
Dvě	dva	k4xCgNnPc1
nebo	nebo	k8xC
více	hodně	k6eAd2
ras	rasa	k1gFnPc2
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1
hispánského	hispánský	k2eAgInSc2d1
nebo	nebo	k8xC
latinskoamerického	latinskoamerický	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
,	,	kIx,
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
rasu	rasa	k1gFnSc4
<g/>
,	,	kIx,
tvořili	tvořit	k5eAaImAgMnP
3,1	3,1	k4
%	%	kIx~
populace	populace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Náboženství	náboženství	k1gNnSc1
</s>
<s>
křesťané	křesťan	k1gMnPc1
–	–	k?
86	#num#	k4
%	%	kIx~
</s>
<s>
protestanti	protestant	k1gMnPc1
–	–	k?
70	#num#	k4
%	%	kIx~
</s>
<s>
baptisté	baptista	k1gMnPc1
–	–	k?
35	#num#	k4
%	%	kIx~
</s>
<s>
metodisté	metodista	k1gMnPc1
–	–	k?
5	#num#	k4
%	%	kIx~
</s>
<s>
letniční	letniční	k2eAgFnPc1d1
církve	církev	k1gFnPc1
-	-	kIx~
4	#num#	k4
%	%	kIx~
</s>
<s>
Church	Church	k1gMnSc1
of	of	k?
Christ	Christ	k1gMnSc1
-	-	kIx~
3	#num#	k4
%	%	kIx~
</s>
<s>
luteráni	luterán	k1gMnPc1
-	-	kIx~
2	#num#	k4
%	%	kIx~
</s>
<s>
presbyteriáni	presbyterián	k1gMnPc1
–	–	k?
2	#num#	k4
%	%	kIx~
</s>
<s>
jiní	jiný	k2eAgMnPc1d1
protestanti	protestant	k1gMnPc1
–	–	k?
19	#num#	k4
%	%	kIx~
</s>
<s>
římští	římský	k2eAgMnPc1d1
katolíci	katolík	k1gMnPc1
–	–	k?
15	#num#	k4
%	%	kIx~
</s>
<s>
jiní	jiný	k2eAgMnPc1d1
křesťané	křesťan	k1gMnPc1
–	–	k?
1	#num#	k4
%	%	kIx~
</s>
<s>
židovství	židovství	k1gNnSc1
-	-	kIx~
0,01	0,01	k4
%	%	kIx~
</s>
<s>
jiná	jiný	k2eAgNnPc1d1
náboženství	náboženství	k1gNnPc1
–	–	k?
<	<	kIx(
<g/>
1	#num#	k4
%	%	kIx~
</s>
<s>
bez	bez	k7c2
vyznání	vyznání	k1gNnPc2
–	–	k?
14	#num#	k4
%	%	kIx~
</s>
<s>
Události	událost	k1gFnPc1
a	a	k8xC
zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Největší	veliký	k2eAgNnSc1d3
město	město	k1gNnSc1
v	v	k7c6
Kentucky	Kentucka	k1gFnPc4
Louisville	Louisville	k1gNnSc2
se	se	k3xPyFc4
proslavilo	proslavit	k5eAaPmAgNnS
závody	závod	k1gInPc4
na	na	k7c6
koních	kůň	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
například	například	k6eAd1
závodiště	závodiště	k1gNnSc1
Churchill	Churchilla	k1gFnPc2
Downs	Downsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
něm	on	k3xPp3gMnSc6
se	se	k3xPyFc4
každoročně	každoročně	k6eAd1
první	první	k4xOgFnSc4
sobotu	sobota	k1gFnSc4
v	v	k7c6
květnu	květen	k1gInSc6
koná	konat	k5eAaImIp3nS
Kentucky	Kentucka	k1gFnPc4
Derby	derby	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
celoamerickém	celoamerický	k2eAgNnSc6d1
měřítku	měřítko	k1gNnSc6
významné	významný	k2eAgInPc4d1
rovinové	rovinový	k2eAgInPc4d1
dostihy	dostih	k1gInPc4
tříletých	tříletý	k2eAgMnPc2d1
anglických	anglický	k2eAgMnPc2d1
plnokrevníků	plnokrevník	k1gMnPc2
(	(	kIx(
<g/>
Thoroughbreads	Thoroughbreads	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
závody	závod	k1gInPc1
jsou	být	k5eAaImIp3nP
součástí	součást	k1gFnSc7
a	a	k8xC
vyvrcholením	vyvrcholení	k1gNnSc7
14	#num#	k4
dní	den	k1gInPc2
trvající	trvající	k2eAgFnSc2d1
velké	velký	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
slavnosti	slavnost	k1gFnSc2
zvané	zvaný	k2eAgFnSc2d1
Kentucky	Kentucka	k1gFnSc2
Derby	derby	k1gNnSc1
Festival	festival	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Stát	stát	k1gInSc1
je	být	k5eAaImIp3nS
známý	známý	k2eAgInSc1d1
výrobou	výroba	k1gFnSc7
bourbonu	bourbon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
domovským	domovský	k2eAgInSc7d1
státem	stát	k1gInSc7
celosvětově	celosvětově	k6eAd1
činné	činný	k2eAgFnSc2d1
gastronomické	gastronomický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
KFC	KFC	kA
–	–	k?
Kentucky	Kentucka	k1gFnSc2
Fried	Fried	k1gMnSc1
Chicken	Chicken	k1gInSc1
(	(	kIx(
<g/>
Kentucká	Kentucký	k2eAgNnPc1d1
smažená	smažený	k2eAgNnPc1d1
kuřata	kuře	k1gNnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnozí	mnohý	k2eAgMnPc1d1
farmáři	farmář	k1gMnPc1
v	v	k7c4
Kentucky	Kentucka	k1gFnPc4
se	se	k3xPyFc4
zabývají	zabývat	k5eAaImIp3nP
chovem	chov	k1gInSc7
ušlechtilých	ušlechtilý	k2eAgMnPc2d1
koní	kůň	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Table	tablo	k1gNnSc6
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annual	Annual	k1gMnSc1
Estimates	Estimates	k1gMnSc1
of	of	k?
the	the	k?
Resident	resident	k1gMnSc1
Population	Population	k1gInSc1
for	forum	k1gNnPc2
the	the	k?
United	United	k1gMnSc1
States	States	k1gMnSc1
<g/>
,	,	kIx,
Regions	Regions	k1gInSc1
<g/>
,	,	kIx,
States	States	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Puerto	Puerta	k1gFnSc5
Rico	Rico	k1gNnSc1
<g/>
:	:	kIx,
April	April	k1gInSc1
1	#num#	k4
<g/>
,	,	kIx,
2010	#num#	k4
to	ten	k3xDgNnSc1
July	Jula	k1gMnPc7
1	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
(	(	kIx(
<g/>
NST-EST	NST-EST	k1gFnSc1
<g/>
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gMnSc1
States	States	k1gMnSc1
Census	census	k1gInSc4
Bureau	Bureaus	k1gInSc2
<g/>
,	,	kIx,
Population	Population	k1gInSc1
Division	Division	k1gInSc1
<g/>
,	,	kIx,
2016-12	2016-12	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
United	United	k1gMnSc1
States	States	k1gMnSc1
Census	census	k1gInSc4
Bureau	Bureaus	k1gInSc2
<g/>
,	,	kIx,
sčítání	sčítání	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
American	American	k1gMnSc1
FactFinder	FactFinder	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kentucky	Kentucka	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Kentucky	Kentucka	k1gFnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
státu	stát	k1gInSc2
Kentucky	Kentucka	k1gFnPc1
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Okresy	okres	k1gInPc1
státu	stát	k1gInSc2
Kentucky	Kentucka	k1gFnSc2
</s>
<s>
Adair	Adair	k1gMnSc1
•	•	k?
Allen	Allen	k1gMnSc1
•	•	k?
Anderson	Anderson	k1gMnSc1
•	•	k?
Ballard	Ballard	k1gMnSc1
•	•	k?
Barren	Barrno	k1gNnPc2
•	•	k?
Bath	Bath	k1gMnSc1
•	•	k?
Bell	bell	k1gInSc1
•	•	k?
Boone	Boon	k1gInSc5
•	•	k?
Bourbon	bourbon	k1gInSc1
•	•	k?
Boyd	Boyd	k1gInSc1
•	•	k?
Boyle	Boyle	k1gInSc1
•	•	k?
Bracken	Bracken	k1gInSc1
•	•	k?
Breathitt	Breathitt	k1gInSc1
•	•	k?
Breckinridge	Breckinridg	k1gFnSc2
•	•	k?
Bullitt	Bullitt	k1gMnSc1
•	•	k?
Butler	Butler	k1gMnSc1
•	•	k?
Caldwell	Caldwell	k1gMnSc1
•	•	k?
Calloway	Callowaa	k1gFnSc2
•	•	k?
Campbell	Campbell	k1gMnSc1
•	•	k?
Carlisle	Carlisle	k1gMnSc1
•	•	k?
Carroll	Carroll	k1gMnSc1
•	•	k?
Carter	Carter	k1gMnSc1
•	•	k?
Casey	Casea	k1gFnSc2
•	•	k?
Christian	Christian	k1gMnSc1
•	•	k?
Clark	Clark	k1gInSc1
•	•	k?
Clay	Claa	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Clinton	Clinton	k1gMnSc1
•	•	k?
Crittenden	Crittendna	k1gFnPc2
•	•	k?
Cumberland	Cumberland	k1gInSc1
•	•	k?
Daviess	Daviess	k1gInSc1
•	•	k?
Edmonson	Edmonson	k1gInSc1
•	•	k?
Elliott	Elliott	k1gInSc1
•	•	k?
Estill	Estill	k1gInSc1
•	•	k?
Fayette	Fayett	k1gInSc5
•	•	k?
Fleming	Fleming	k1gInSc1
•	•	k?
Floyd	Floyd	k1gInSc1
•	•	k?
Franklin	Franklin	k1gInSc1
•	•	k?
Fulton	Fulton	k1gInSc1
•	•	k?
Gallatin	Gallatina	k1gFnPc2
•	•	k?
Garrard	Garrard	k1gMnSc1
•	•	k?
Grant	grant	k1gInSc1
•	•	k?
Graves	Graves	k1gInSc1
•	•	k?
Grayson	Grayson	k1gInSc1
•	•	k?
Green	Green	k1gInSc1
•	•	k?
Greenup	Greenup	k1gInSc1
•	•	k?
Hancock	Hancock	k1gInSc1
•	•	k?
Hardin	Hardina	k1gFnPc2
•	•	k?
Harlan	Harlan	k1gMnSc1
•	•	k?
Harrison	Harrison	k1gMnSc1
•	•	k?
Hart	Hart	k1gMnSc1
•	•	k?
Henderson	Henderson	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Henry	Henry	k1gMnSc1
•	•	k?
Hickman	Hickman	k1gMnSc1
•	•	k?
Hopkins	Hopkins	k1gInSc1
•	•	k?
Jackson	Jackson	k1gMnSc1
•	•	k?
Jefferson	Jefferson	k1gMnSc1
•	•	k?
Jessamine	Jessamin	k1gInSc5
•	•	k?
Johnson	Johnson	k1gMnSc1
•	•	k?
Kenton	Kenton	k1gInSc1
•	•	k?
Knott	Knott	k1gInSc1
•	•	k?
Knox	Knox	k1gInSc1
•	•	k?
LaRue	LaRue	k1gInSc1
•	•	k?
Laurel	Laurel	k1gInSc1
•	•	k?
Lawrence	Lawrence	k1gFnSc2
•	•	k?
Lee	Lea	k1gFnSc6
•	•	k?
Leslie	Leslie	k1gFnSc1
•	•	k?
Letcher	Letchra	k1gFnPc2
•	•	k?
Lewis	Lewis	k1gFnSc2
•	•	k?
Lincoln	Lincoln	k1gMnSc1
•	•	k?
Livingston	Livingston	k1gInSc1
•	•	k?
Logan	Logan	k1gInSc1
•	•	k?
Lyon	Lyon	k1gInSc1
•	•	k?
Madison	Madison	k1gInSc1
•	•	k?
Magoffin	Magoffin	k1gInSc1
•	•	k?
Marion	Marion	k1gInSc1
•	•	k?
Marshall	Marshall	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Martin	Martin	k1gMnSc1
•	•	k?
Mason	mason	k1gMnSc1
•	•	k?
McCracken	McCracken	k1gInSc1
•	•	k?
McCreary	McCreara	k1gFnSc2
•	•	k?
McLean	McLean	k1gMnSc1
•	•	k?
Meade	Mead	k1gInSc5
•	•	k?
Menifee	Menife	k1gFnSc2
•	•	k?
Mercer	Mercer	k1gMnSc1
•	•	k?
Metcalfe	Metcalf	k1gInSc5
•	•	k?
Monroe	Monroe	k1gFnSc1
•	•	k?
Montgomery	Montgomera	k1gFnSc2
•	•	k?
Morgan	morgan	k1gMnSc1
•	•	k?
Muhlenberg	Muhlenberg	k1gMnSc1
•	•	k?
Nelson	Nelson	k1gMnSc1
•	•	k?
Nicholas	Nicholas	k1gMnSc1
•	•	k?
Ohio	Ohio	k1gNnSc4
•	•	k?
Oldham	Oldham	k1gInSc1
•	•	k?
Owen	Owen	k1gInSc1
•	•	k?
Owsley	Owslea	k1gFnSc2
•	•	k?
Pendleton	Pendleton	k1gInSc1
•	•	k?
Perry	Perra	k1gFnSc2
•	•	k?
Pike	Pik	k1gFnSc2
•	•	k?
Powell	Powell	k1gMnSc1
•	•	k?
Pulaski	Pulask	k1gFnSc2
•	•	k?
Robertson	Robertson	k1gMnSc1
•	•	k?
Rockcastle	Rockcastle	k1gMnSc1
•	•	k?
Rowan	Rowan	k1gMnSc1
•	•	k?
Russell	Russell	k1gMnSc1
•	•	k?
Scott	Scott	k1gMnSc1
•	•	k?
Shelby	Shelba	k1gFnSc2
•	•	k?
Simpson	Simpson	k1gMnSc1
•	•	k?
Spencer	Spencer	k1gMnSc1
•	•	k?
Simpson	Simpson	k1gMnSc1
•	•	k?
Taylor	Taylor	k1gMnSc1
•	•	k?
Todd	Todd	k1gMnSc1
•	•	k?
Trigg	Trigg	k1gMnSc1
•	•	k?
Trimble	Trimble	k1gMnSc1
•	•	k?
Union	union	k1gInSc1
•	•	k?
Warren	Warrna	k1gFnPc2
•	•	k?
<g/>
Washington	Washington	k1gInSc1
•	•	k?
Wayne	Wayn	k1gInSc5
•	•	k?
Webster	Webstrum	k1gNnPc2
•	•	k?
Whitley	Whitlea	k1gFnSc2
•	•	k?
Wolfe	Wolf	k1gMnSc5
•	•	k?
Woodford	Woodfordo	k1gNnPc2
</s>
<s>
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgFnSc2d1
–	–	k?
United	United	k1gMnSc1
States	States	k1gMnSc1
of	of	k?
America	America	k1gMnSc1
–	–	k?
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
Státy	stát	k1gInPc1
</s>
<s>
Alabama	Alabama	k1gFnSc1
•	•	k?
Aljaška	Aljaška	k1gFnSc1
•	•	k?
Arizona	Arizona	k1gFnSc1
•	•	k?
Arkansas	Arkansas	k1gInSc1
•	•	k?
Colorado	Colorado	k1gNnSc1
•	•	k?
Connecticut	Connecticut	k1gMnSc1
•	•	k?
Delaware	Delawar	k1gMnSc5
•	•	k?
Florida	Florida	k1gFnSc1
•	•	k?
Georgie	Georgie	k1gFnSc1
•	•	k?
Havaj	Havaj	k1gFnSc1
•	•	k?
Idaho	Ida	k1gMnSc2
•	•	k?
Illinois	Illinois	k1gFnSc2
•	•	k?
Indiana	Indiana	k1gFnSc1
•	•	k?
Iowa	Iow	k1gInSc2
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Dakota	Dakota	k1gFnSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Karolína	Karolína	k1gFnSc1
•	•	k?
Kalifornie	Kalifornie	k1gFnSc2
•	•	k?
Kansas	Kansas	k1gInSc1
•	•	k?
Kentucky	Kentucka	k1gFnSc2
•	•	k?
Louisiana	Louisiana	k1gFnSc1
•	•	k?
Maine	Main	k1gInSc5
•	•	k?
Maryland	Marylanda	k1gFnPc2
•	•	k?
Massachusetts	Massachusetts	k1gNnSc4
•	•	k?
Michigan	Michigan	k1gInSc1
•	•	k?
Minnesota	Minnesota	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Mississippi	Mississippi	k1gFnSc2
•	•	k?
Missouri	Missouri	k1gFnSc2
•	•	k?
Montana	Montana	k1gFnSc1
•	•	k?
Nebraska	Nebraska	k1gFnSc1
•	•	k?
Nevada	Nevada	k1gFnSc1
•	•	k?
New	New	k1gMnSc5
Hampshire	Hampshir	k1gMnSc5
•	•	k?
New	New	k1gMnSc4
Jersey	Jersea	k1gFnSc2
•	•	k?
New	New	k1gFnSc1
York	York	k1gInSc1
•	•	k?
Nové	Nové	k2eAgNnSc1d1
Mexiko	Mexiko	k1gNnSc1
•	•	k?
Ohio	Ohio	k1gNnSc1
•	•	k?
Oklahoma	Oklahom	k1gMnSc2
•	•	k?
Oregon	Oregon	k1gMnSc1
•	•	k?
Pensylvánie	Pensylvánie	k1gFnSc1
•	•	k?
Rhode	Rhodos	k1gInSc5
Island	Island	k1gInSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Dakota	Dakota	k1gFnSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Karolína	Karolína	k1gFnSc1
•	•	k?
Tennessee	Tennessee	k1gInSc1
•	•	k?
Texas	Texas	k1gInSc1
•	•	k?
Utah	Utah	k1gInSc1
•	•	k?
Vermont	Vermont	k1gInSc1
•	•	k?
Virginie	Virginie	k1gFnSc1
•	•	k?
Washington	Washington	k1gInSc1
•	•	k?
Wisconsin	Wisconsin	k1gInSc1
•	•	k?
Wyoming	Wyoming	k1gInSc4
•	•	k?
Západní	západní	k2eAgFnSc2d1
Virginie	Virginie	k1gFnSc2
Federální	federální	k2eAgInSc1d1
distrikt	distrikt	k1gInSc1
</s>
<s>
District	District	k1gInSc1
of	of	k?
Columbia	Columbia	k1gFnSc1
Autonomní	autonomní	k2eAgFnSc1d1
ostrovní	ostrovní	k2eAgNnSc4d1
území	území	k1gNnSc4
</s>
<s>
Americká	americký	k2eAgFnSc1d1
Samoa	Samoa	k1gFnSc1
•	•	k?
Americké	americký	k2eAgInPc4d1
Panenské	panenský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
•	•	k?
Guam	Guam	k1gInSc1
•	•	k?
Portoriko	Portoriko	k1gNnSc1
•	•	k?
Severní	severní	k2eAgFnSc2d1
Mariany	Mariana	k1gFnSc2
Ostrovní	ostrovní	k2eAgNnSc4d1
území	území	k1gNnSc4
podpřímou	podpřímý	k2eAgFnSc7d1
správou	správa	k1gFnSc7
vlády	vláda	k1gFnSc2
USA	USA	kA
</s>
<s>
Bakerův	Bakerův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
•	•	k?
Howlandův	Howlandův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
•	•	k?
Jarvisův	Jarvisův	k2eAgInSc1d1
ostrov	ostrov	k1gInSc1
•	•	k?
Johnstonův	Johnstonův	k2eAgInSc1d1
atol	atol	k1gInSc1
•	•	k?
Kingmanův	Kingmanův	k2eAgInSc1d1
útes	útes	k1gInSc1
•	•	k?
Midway	Midwaa	k1gFnSc2
•	•	k?
Navassa	Navassa	k1gFnSc1
•	•	k?
Palmyra	Palmyra	k1gFnSc1
•	•	k?
Wake	Wak	k1gInSc2
•	•	k?
sporná	sporný	k2eAgFnSc1d1
území	území	k1gNnSc6
<g/>
:	:	kIx,
Bajo	Bajo	k6eAd1
Nuevo	Nuevo	k1gNnSc1
•	•	k?
Serranilla	Serranilla	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
251176	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4097823-0	4097823-0	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0004	#num#	k4
0617	#num#	k4
4335	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79018583	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
144869566	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79018583	#num#	k4
</s>
