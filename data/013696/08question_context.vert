<s desamb="1">
Sestává	sestávat	k5eAaImIp3nS
ze	z	k7c2
tří	tři	k4xCgFnPc2
složek	složka	k1gFnPc2
<g/>
:	:	kIx,
společné	společný	k2eAgFnSc2d1
dvojice	dvojice	k1gFnSc2
Alfa	Alfa	k1gFnSc1
Centauri	Centauri	k1gFnSc1
A	A	kA
(	(	kIx(
<g/>
též	též	k9
Rigil	Rigil	k1gMnSc1
Kentaurus	Kentaurus	k1gMnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
a	a	k8xC
Alfa	Alfa	k1gFnSc1
Centauri	Centauri	k1gFnSc1
B	B	kA
(	(	kIx(
<g/>
též	též	k9
Toliman	Toliman	k1gMnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
a	a	k8xC
malého	malý	k2eAgMnSc2d1
a	a	k8xC
vizuálně	vizuálně	k6eAd1
slabého	slabý	k2eAgNnSc2d1
červeného	červené	k1gNnSc2
trpaslíka	trpaslík	k1gMnSc2
Alfa	Alfa	k1gFnSc1
Centauri	Centauri	k1gFnSc1
C	C	kA
<g />
.	.	kIx.
</s>
<s>
Alfa	Alfa	k1gFnSc1
Centauri	Centauri	k1gFnSc1
(	(	kIx(
<g/>
α	α	kA
Centauri	Centauri	k1gFnSc1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
α	α	kA
Cen	Cen	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
trojhvězda	trojhvězda	k1gFnSc1
a	a	k8xC
svou	svůj	k3xOyFgFnSc7
vzdáleností	vzdálenost	k1gFnSc7
4,37	4,37	k4
světelného	světelný	k2eAgInSc2d1
roku	rok	k1gInSc2
(	(	kIx(
<g/>
1,34	1,34	k4
parseku	parsek	k1gInSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
také	také	k9
nejbližší	blízký	k2eAgInSc1d3
hvězdný	hvězdný	k2eAgInSc1d1
systém	systém	k1gInSc1
od	od	k7c2
sluneční	sluneční	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
<g/>
.	.	kIx.
</s>