<s>
Alfa	alfa	k1gFnSc1
Centauri	Centaur	k1gFnSc2
</s>
<s>
Alfa	alfa	k1gFnSc1
Centauri	Centaur	k1gFnSc2
Poměr	poměra	k1gFnPc2
průměrů	průměr	k1gInPc2
(	(	kIx(
<g/>
Slunce	slunce	k1gNnSc2
<g/>
)	)	kIx)
<g/>
/	/	kIx~
<g/>
Alfa	alfa	k1gFnSc1
Centauri	Centaur	k1gFnSc2
A	A	kA
<g/>
/	/	kIx~
<g/>
Alfa	alfa	k1gFnSc1
Centauri	Centaur	k1gFnSc2
B	B	kA
<g/>
/	/	kIx~
<g/>
Proxima	Proxima	k1gFnSc1
Centauri	Centaur	k1gFnSc2
Astrometrická	Astrometrický	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
(	(	kIx(
<g/>
Ekvinokcium	ekvinokcium	k1gNnSc1
J	J	kA
<g/>
2000.0	2000.0	k4
<g/>
)	)	kIx)
Souhvězdí	souhvězdí	k1gNnSc2
</s>
<s>
Kentaur	kentaur	k1gMnSc1
(	(	kIx(
<g/>
Centaurus	Centaurus	k1gMnSc1
<g/>
)	)	kIx)
Rektascenze	rektascenze	k1gFnSc1
</s>
<s>
14	#num#	k4
<g/>
h	h	k?
39	#num#	k4
<g/>
m	m	kA
35,90	35,90	k4
<g/>
s	s	k7c7
Deklinace	deklinace	k1gFnSc2
</s>
<s>
–	–	k?
<g/>
60	#num#	k4
<g/>
°	°	k?
50	#num#	k4
<g/>
'	'	kIx"
7,0	7,0	k4
<g/>
"	"	kIx"
Vzdálenost	vzdálenost	k1gFnSc1
</s>
<s>
4,365	4,365	k4
±	±	k?
0,007	0,007	k4
ly	ly	k?
<g/>
(	(	kIx(
<g/>
1,338	1,338	k4
±	±	k?
0,002	0,002	k4
pc	pc	k?
<g/>
)	)	kIx)
Barevný	barevný	k2eAgInSc1d1
index	index	k1gInSc1
(	(	kIx(
<g/>
U-B	U-B	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
0,24	0,24	k4
<g/>
/	/	kIx~
<g/>
0,64	0,64	k4
<g/>
/	/	kIx~
<g/>
1,54	1,54	k4
Barevný	barevný	k2eAgInSc4d1
index	index	k1gInSc4
(	(	kIx(
<g/>
B-V	B-V	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
0,65	0,65	k4
<g/>
/	/	kIx~
<g/>
0,85	0,85	k4
<g/>
/	/	kIx~
<g/>
1,97	1,97	k4
Barevný	barevný	k2eAgInSc4d1
index	index	k1gInSc4
(	(	kIx(
<g/>
V-R	V-R	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
</s>
<s desamb="1">
<g/>
/	/	kIx~
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
/	/	kIx~
<g/>
1,68	1,68	k4
Barevný	barevný	k2eAgInSc4d1
index	index	k1gInSc4
(	(	kIx(
<g/>
R-I	R-I	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
</s>
<s desamb="1">
<g/>
/	/	kIx~
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
/	/	kIx~
<g/>
2,04	2,04	k4
Zdánlivá	zdánlivý	k2eAgFnSc1d1
hvězdná	hvězdný	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
</s>
<s>
-0.01	-0.01	k4
<g/>
/	/	kIx~
<g/>
+	+	kIx~
<g/>
1.34	1.34	k4
<g/>
/	/	kIx~
<g/>
+	+	kIx~
<g/>
11.05	11.05	k4
Absolutní	absolutní	k2eAgFnSc1d1
hvězdná	hvězdný	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
</s>
<s>
4,1	4,1	k4
Vlastní	vlastní	k2eAgInSc1d1
pohyb	pohyb	k1gInSc1
v	v	k7c6
rektascenzi	rektascenze	k1gFnSc6
</s>
<s>
−	−	k?
600	#num#	k4
mas	masa	k1gFnPc2
<g/>
/	/	kIx~
<g/>
rok	rok	k1gInSc4
Vlastní	vlastní	k2eAgInSc4d1
pohyb	pohyb	k1gInSc4
v	v	k7c6
deklinaci	deklinace	k1gFnSc6
</s>
<s>
700	#num#	k4
mas	masa	k1gFnPc2
<g/>
/	/	kIx~
<g/>
rok	rok	k1gInSc4
Fyzikální	fyzikální	k2eAgFnSc2d1
charakteristiky	charakteristika	k1gFnSc2
Spektrální	spektrální	k2eAgInSc1d1
typ	typ	k1gInSc1
</s>
<s>
G	G	kA
<g/>
2	#num#	k4
<g/>
V	V	kA
<g/>
/	/	kIx~
<g/>
K	K	kA
<g/>
1	#num#	k4
<g/>
V	V	kA
<g/>
/	/	kIx~
<g/>
M	M	kA
<g/>
5,5	5,5	k4
<g/>
Ve	v	k7c4
Hmotnost	hmotnost	k1gFnSc4
</s>
<s>
1,100	1,100	k4
<g/>
/	/	kIx~
<g/>
0,907	0,907	k4
<g/>
/	/	kIx~
<g/>
0,1	0,1	k4
M	M	kA
<g/>
☉	☉	k?
Poloměr	poloměr	k1gInSc1
</s>
<s>
1,227	1,227	k4
<g/>
/	/	kIx~
<g/>
0,865	0,865	k4
<g/>
/	/	kIx~
<g/>
0,2	0,2	k4
R	R	kA
<g/>
☉	☉	k?
Zářivý	zářivý	k2eAgInSc4d1
výkon	výkon	k1gInSc4
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
</s>
<s>
1,519	1,519	k4
<g/>
/	/	kIx~
<g/>
0,500	0,500	k4
<g/>
/	/	kIx~
<g/>
0,000	0,000	k4
06	#num#	k4
L	L	kA
<g/>
☉	☉	k?
Povrchová	povrchový	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
</s>
<s>
5800	#num#	k4
<g/>
/	/	kIx~
<g/>
5300	#num#	k4
<g/>
/	/	kIx~
<g/>
2700	#num#	k4
K	k	k7c3
Stáří	stáří	k1gNnSc3
</s>
<s>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
×	×	k?
<g/>
109	#num#	k4
Další	další	k2eAgInSc1d1
označení	označení	k1gNnSc4
Henry	henry	k1gInSc2
Draper	Draper	k1gInSc4
Catalogue	Catalogue	k1gNnSc2
</s>
<s>
HD	HD	kA
128620	#num#	k4
Bright	Bright	k1gInSc4
Star	Star	kA
katalog	katalog	k1gInSc1
</s>
<s>
HR	hr	k6eAd1
5459	#num#	k4
SAO	SAO	kA
katalog	katalog	k1gInSc4
</s>
<s>
SAO	SAO	kA
252838	#num#	k4
Katalog	katalog	k1gInSc1
Hipparcos	Hipparcos	k1gInSc4
</s>
<s>
HIP	hip	k0
71683	#num#	k4
Tychův	Tychův	k2eAgInSc4d1
katalog	katalog	k1gInSc4
</s>
<s>
TYC	TYC	kA
9007-5849-1	9007-5849-1	k4
General	General	k1gFnSc2
Catalogue	Catalogue	k1gFnPc2
</s>
<s>
GC	GC	kA
19728	#num#	k4
Bayerovo	Bayerův	k2eAgNnSc1d1
označení	označení	k1gNnSc1
</s>
<s>
α	α	k?
Cen	cena	k1gFnPc2
Synonyma	synonymum	k1gNnPc4
</s>
<s>
Rigil	Rigil	k1gMnSc1
Kentaurus	Kentaurus	k1gMnSc1
<g/>
,	,	kIx,
Rigilkent	Rigilkent	k1gMnSc1
<g/>
,	,	kIx,
Toliman	Toliman	k1gMnSc1
<g/>
,	,	kIx,
CCDM	CCDM	kA
J	J	kA
<g/>
14396	#num#	k4
<g/>
-	-	kIx~
<g/>
6050	#num#	k4
<g/>
AB	AB	kA
<g/>
,	,	kIx,
CD-60	CD-60	k1gFnSc1
5293	#num#	k4
<g/>
,	,	kIx,
CPD-60	CPD-60	k1gFnSc1
5483	#num#	k4
<g/>
,	,	kIx,
FK5	FK5	k1gFnSc1
538	#num#	k4
<g/>
,	,	kIx,
GCRV	GCRV	kA
8519	#num#	k4
<g/>
,	,	kIx,
IDS	IDS	kA
14328-6025	14328-6025	k4
AB	AB	kA
<g/>
,	,	kIx,
IRAS	IRAS	kA
14359	#num#	k4
<g/>
-	-	kIx~
<g/>
6037	#num#	k4
<g/>
,	,	kIx,
WDS	WDS	kA
J	J	kA
<g/>
14396	#num#	k4
<g/>
-	-	kIx~
<g/>
6050	#num#	k4
<g/>
AB	AB	kA
<g/>
,	,	kIx,
WDS	WDS	kA
J14403-6051AB	J14403-6051AB	k1gFnSc1
(	(	kIx(
<g/>
V	V	kA
<g/>
)	)	kIx)
–	–	k?
měření	měření	k1gNnSc2
provedena	proveden	k2eAgFnSc1d1
ve	v	k7c6
viditelném	viditelný	k2eAgInSc6d1
světleNěkterá	světleNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Alfa	Alfa	k1gFnSc1
Centauri	Centauri	k1gFnSc1
(	(	kIx(
<g/>
α	α	kA
Centauri	Centauri	k1gFnSc1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
α	α	kA
Cen	Cen	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
trojhvězda	trojhvězda	k1gFnSc1
a	a	k8xC
svou	svůj	k3xOyFgFnSc7
vzdáleností	vzdálenost	k1gFnSc7
4,37	4,37	k4
světelného	světelný	k2eAgInSc2d1
roku	rok	k1gInSc2
(	(	kIx(
<g/>
1,34	1,34	k4
parseku	parsek	k1gInSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
také	také	k9
nejbližší	blízký	k2eAgInSc1d3
hvězdný	hvězdný	k2eAgInSc1d1
systém	systém	k1gInSc1
od	od	k7c2
sluneční	sluneční	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sestává	sestávat	k5eAaImIp3nS
ze	z	k7c2
tří	tři	k4xCgFnPc2
složek	složka	k1gFnPc2
<g/>
:	:	kIx,
společné	společný	k2eAgFnSc2d1
dvojice	dvojice	k1gFnSc2
Alfa	Alfa	k1gFnSc1
Centauri	Centauri	k1gFnSc1
A	A	kA
(	(	kIx(
<g/>
též	též	k9
Rigil	Rigil	k1gMnSc1
Kentaurus	Kentaurus	k1gMnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
a	a	k8xC
Alfa	Alfa	k1gFnSc1
Centauri	Centauri	k1gFnSc1
B	B	kA
(	(	kIx(
<g/>
též	též	k9
Toliman	Toliman	k1gMnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
a	a	k8xC
malého	malý	k2eAgMnSc2d1
a	a	k8xC
vizuálně	vizuálně	k6eAd1
slabého	slabý	k2eAgNnSc2d1
červeného	červené	k1gNnSc2
trpaslíka	trpaslík	k1gMnSc2
Alfa	Alfa	k1gFnSc1
Centauri	Centauri	k1gFnSc1
C	C	kA
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
též	též	k9
Proxima	Proxima	k1gFnSc1
Centauri	Centauri	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
gravitačně	gravitačně	k6eAd1
spojen	spojit	k5eAaPmNgInS
se	s	k7c7
složkami	složka	k1gFnPc7
A	A	kA
a	a	k8xC
B.	B.	kA
Lidskému	lidský	k2eAgNnSc3d1
oku	oko	k1gNnSc3
se	se	k3xPyFc4
dvě	dva	k4xCgFnPc1
hlavní	hlavní	k2eAgFnPc1d1
hvězdy	hvězda	k1gFnPc1
zdají	zdát	k5eAaImIp3nP
jako	jako	k8xS
jeden	jeden	k4xCgInSc1
bod	bod	k1gInSc1
se	s	k7c7
zdánlivou	zdánlivý	k2eAgFnSc7d1
magnitudou	magnituda	k1gFnSc7
−	−	k?
<g/>
,	,	kIx,
takže	takže	k8xS
tvoří	tvořit	k5eAaImIp3nP
nejjasnější	jasný	k2eAgFnSc4d3
hvězdu	hvězda	k1gFnSc4
v	v	k7c6
souhvězdí	souhvězdí	k1gNnSc6
Kentaura	Kentaur	k1gMnSc2
a	a	k8xC
po	po	k7c6
Siriu	Sirius	k1gNnSc6
a	a	k8xC
Canopu	Canop	k1gNnSc6
třetí	třetí	k4xOgFnSc4
nejjasnější	jasný	k2eAgFnSc4d3
hvězdu	hvězda	k1gFnSc4
noční	noční	k2eAgFnSc2d1
oblohy	obloha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zatímco	zatímco	k8xS
Alfa	alfa	k1gFnSc1
Centauri	Centaur	k1gFnSc2
A	A	kA
(	(	kIx(
<g/>
α	α	k?
Cen	cena	k1gFnPc2
A	A	kA
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
110	#num#	k4
%	%	kIx~
hmotnosti	hmotnost	k1gFnSc3
Slunce	slunce	k1gNnSc2
a	a	k8xC
151,9	151,9	k4
%	%	kIx~
jeho	jeho	k3xOp3gFnSc2
svítivosti	svítivost	k1gFnSc2
<g/>
,	,	kIx,
Alfa	alfa	k1gFnSc1
Centauri	Centauri	k1gNnSc2
B	B	kA
(	(	kIx(
<g/>
α	α	k?
Cen	cena	k1gFnPc2
B	B	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
menší	malý	k2eAgMnSc1d2
a	a	k8xC
chladnější	chladný	k2eAgMnSc1d2
a	a	k8xC
má	mít	k5eAaImIp3nS
90,7	90,7	k4
%	%	kIx~
hmotnosti	hmotnost	k1gFnSc3
Slunce	slunce	k1gNnSc2
a	a	k8xC
44,5	44,5	k4
%	%	kIx~
jeho	jeho	k3xOp3gFnSc2
svítivosti	svítivost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Jejich	jejich	k3xOp3gFnSc1
oběžná	oběžný	k2eAgFnSc1d1
doba	doba	k1gFnSc1
kolem	kolem	k7c2
společného	společný	k2eAgNnSc2d1
těžiště	těžiště	k1gNnSc2
trvá	trvat	k5eAaImIp3nS
79,91	79,91	k4
roku	rok	k1gInSc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
vzájemná	vzájemný	k2eAgFnSc1d1
vzdálenost	vzdálenost	k1gFnSc1
osciluje	oscilovat	k5eAaImIp3nS
mezi	mezi	k7c7
vzdáleností	vzdálenost	k1gFnSc7
Slunce	slunce	k1gNnSc2
a	a	k8xC
Pluta	Pluto	k1gNnSc2
a	a	k8xC
vzdáleností	vzdálenost	k1gFnSc7
Slunce	slunce	k1gNnSc2
a	a	k8xC
Saturnu	Saturn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Proxima	Proxima	k1gFnSc1
Centauri	Centaur	k1gFnSc2
(	(	kIx(
<g/>
α	α	k?
Cen	cena	k1gFnPc2
C	C	kA
<g/>
)	)	kIx)
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
4,24	4,24	k4
světelného	světelný	k2eAgInSc2d1
roku	rok	k1gInSc2
(	(	kIx(
<g/>
1,29	1,29	k4
parseku	parsec	k1gInSc2
<g/>
)	)	kIx)
od	od	k7c2
Slunce	slunce	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
z	z	k7c2
ní	on	k3xPp3gFnSc2
činí	činit	k5eAaImIp3nS
Slunci	slunce	k1gNnSc3
nejbližší	blízký	k2eAgFnSc4d3
hvězdu	hvězda	k1gFnSc4
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
není	být	k5eNaImIp3nS
pouhým	pouhý	k2eAgNnSc7d1
okem	oke	k1gNnSc7
viditelná	viditelný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdálenost	vzdálenost	k1gFnSc1
mezi	mezi	k7c7
Proximou	Proxima	k1gFnSc7
a	a	k8xC
dvojhvězdou	dvojhvězda	k1gFnSc7
Alfa	alfa	k1gNnSc2
Centauri	Centaur	k1gFnSc2
AB	AB	kA
je	být	k5eAaImIp3nS
0,2	0,2	k4
světelného	světelný	k2eAgInSc2d1
roku	rok	k1gInSc2
(	(	kIx(
<g/>
cca	cca	kA
0,06	0,06	k4
parseku	parsec	k1gInSc2
nebo	nebo	k8xC
15	#num#	k4
000	#num#	k4
astronomických	astronomický	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
U	u	k7c2
Proximy	Proxima	k1gFnSc2
byla	být	k5eAaImAgFnS
roku	rok	k1gInSc2
2016	#num#	k4
v	v	k7c6
obyvatelné	obyvatelný	k2eAgFnSc6d1
zóně	zóna	k1gFnSc6
objevena	objeven	k2eAgFnSc1d1
exoplaneta	exoplaneta	k1gFnSc1
<g/>
,	,	kIx,
svou	svůj	k3xOyFgFnSc7
velikostí	velikost	k1gFnSc7
podobná	podobný	k2eAgFnSc1d1
Zemi	zem	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
označena	označit	k5eAaPmNgFnS
Proxima	Proxima	k1gFnSc1
Centauri	Centaur	k1gFnSc2
b.	b.	k?
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pojmenování	pojmenování	k1gNnPc1
</s>
<s>
α	α	k?
Centauri	Centauri	k1gNnSc1
(	(	kIx(
<g/>
Alfa	alfa	k1gFnSc1
Centauri	Centaur	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
Bayerovo	Bayerův	k2eAgNnSc1d1
označení	označení	k1gNnSc1
hvězdného	hvězdný	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Převládá	převládat	k5eAaImIp3nS
nad	nad	k7c7
tradičním	tradiční	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Rigil	Rigil	k1gMnSc1
Kentaurus	Kentaurus	k1gMnSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
polatinštěný	polatinštěný	k2eAgInSc1d1
arabský	arabský	k2eAgInSc1d1
název	název	k1gInSc1
Rijl	Rijla	k1gFnPc2
Qanṭ	Qanṭ	k1gFnSc2
ج	ج	k?
ر	ر	k?
ا	ا	k?
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
ze	z	k7c2
slovního	slovní	k2eAgNnSc2d1
spojení	spojení	k1gNnSc2
Rijl	Rijla	k1gFnPc2
al-Qanṭ	al-Qanṭ	k1gFnSc2
<g/>
,	,	kIx,
tj.	tj.	kA
„	„	k?
<g/>
Noha	noh	k1gMnSc2
Kentaura	kentaur	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
dalším	další	k2eAgNnPc3d1
jménům	jméno	k1gNnPc3
hvězdy	hvězda	k1gFnSc2
patří	patřit	k5eAaImIp3nS
Rigil	Rigil	k1gMnSc1
Kent	Kent	k1gMnSc1
<g/>
,	,	kIx,
Rigel	Rigel	k1gMnSc1
Kent	Kent	k1gInSc1
(	(	kIx(
<g/>
obě	dva	k4xCgFnPc1
to	ten	k3xDgNnSc1
jsou	být	k5eAaImIp3nP
zkrácené	zkrácený	k2eAgFnPc1d1
verze	verze	k1gFnPc1
Rigil	Rigil	k1gMnSc1
Kentaurus	Kentaurus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
případně	případně	k6eAd1
historické	historický	k2eAgInPc4d1
názvy	název	k1gInPc4
Toliman	Toliman	k1gMnSc1
a	a	k8xC
Bungula	Bungula	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Alfu	alfa	k1gFnSc4
Centauri	Centaur	k1gFnSc2
C	C	kA
objevil	objevit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1915	#num#	k4
skotský	skotský	k1gInSc1
astronom	astronom	k1gMnSc1
Robert	Robert	k1gMnSc1
Innes	Innes	k1gMnSc1
<g/>
,	,	kIx,
ředitel	ředitel	k1gMnSc1
observatoře	observatoř	k1gFnSc2
v	v	k7c6
jihoafrickém	jihoafrický	k2eAgInSc6d1
Johannesburgu	Johannesburg	k1gInSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
navrhl	navrhnout	k5eAaPmAgInS
označení	označení	k1gNnSc4
Proxima	Proximum	k1gNnSc2
Centauri	Centauri	k1gNnSc2
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
přesněji	přesně	k6eAd2
Proxima	Proxima	k1gFnSc1
Centaurus	Centaurus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Název	název	k1gInSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
latiny	latina	k1gFnSc2
a	a	k8xC
znamená	znamenat	k5eAaImIp3nS
„	„	k?
<g/>
nejbližší	blízký	k2eAgFnSc1d3
[	[	kIx(
<g/>
hvězda	hvězda	k1gFnSc1
<g/>
]	]	kIx)
z	z	k7c2
Kentaura	kentaur	k1gMnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
ustanovila	ustanovit	k5eAaPmAgFnS
Mezinárodní	mezinárodní	k2eAgFnSc1d1
astronomická	astronomický	k2eAgFnSc1d1
unie	unie	k1gFnSc1
(	(	kIx(
<g/>
IAU	IAU	kA
<g/>
)	)	kIx)
pracovní	pracovní	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
pro	pro	k7c4
jména	jméno	k1gNnPc4
hvězd	hvězda	k1gFnPc2
(	(	kIx(
<g/>
Working	Working	k1gInSc1
Group	Group	k1gInSc1
on	on	k3xPp3gMnSc1
Star	Star	kA
Names	Namesa	k1gFnPc2
<g/>
,	,	kIx,
zkratkou	zkratka	k1gFnSc7
WGSN	WGSN	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
má	mít	k5eAaImIp3nS
zkatalogizovat	zkatalogizovat	k5eAaPmF
a	a	k8xC
standardizovat	standardizovat	k5eAaBmF
vlastní	vlastní	k2eAgNnPc4d1
jména	jméno	k1gNnPc4
hvězd	hvězda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
WGSN	WGSN	kA
uvedla	uvést	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
případě	případ	k1gInSc6
vícenásobných	vícenásobný	k2eAgFnPc2d1
hvězd	hvězda	k1gFnPc2
by	by	kYmCp3nS
se	se	k3xPyFc4
jméno	jméno	k1gNnSc1
mělo	mít	k5eAaImAgNnS
vztahovat	vztahovat	k5eAaImF
k	k	k7c3
nejjasnější	jasný	k2eAgFnSc3d3
složce	složka	k1gFnSc3
při	při	k7c6
vizuálním	vizuální	k2eAgNnSc6d1
pozorování	pozorování	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2016	#num#	k4
schválila	schválit	k5eAaPmAgFnS
jméno	jméno	k1gNnSc4
Proxima	Proxima	k1gNnSc4
Centauri	Centaur	k1gFnSc2
pro	pro	k7c4
hvězdu	hvězda	k1gFnSc4
Alfa	alfa	k1gNnSc2
Centauri	Centauri	k1gNnSc2
C	C	kA
<g/>
,	,	kIx,
dne	den	k1gInSc2
6	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2016	#num#	k4
jméno	jméno	k1gNnSc4
Rigil	Rigil	k1gMnSc1
Kentaurus	Kentaurus	k1gMnSc1
pro	pro	k7c4
hvězdu	hvězda	k1gFnSc4
Alfa	alfa	k1gNnSc2
Centauri	Centauri	k1gNnSc2
A	A	kA
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
10	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2018	#num#	k4
také	také	k9
jméno	jméno	k1gNnSc4
Toliman	Toliman	k1gMnSc1
pro	pro	k7c4
Alfu	alfa	k1gFnSc4
Centauri	Centaur	k1gFnSc2
B	B	kA
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
tak	tak	k6eAd1
v	v	k7c6
katalogu	katalog	k1gInSc6
IAU	IAU	kA
nacházejí	nacházet	k5eAaImIp3nP
pod	pod	k7c7
těmito	tento	k3xDgInPc7
názvy	název	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Alfa	alfa	k1gFnSc1
Centauri	Centaur	k1gFnSc2
je	být	k5eAaImIp3nS
nejbližší	blízký	k2eAgInSc4d3
hvězdný	hvězdný	k2eAgInSc4d1
systém	systém	k1gInSc4
od	od	k7c2
Slunce	slunce	k1gNnSc2
<g/>
,	,	kIx,
vzdálený	vzdálený	k2eAgMnSc1d1
4,35	4,35	k4
světelných	světelný	k2eAgNnPc2d1
let	léto	k1gNnPc2
nebo	nebo	k8xC
1,34	1,34	k4
pc	pc	k?
<g/>
.	.	kIx.
</s>
<s>
Pozice	pozice	k1gFnSc1
Alfy	alfa	k1gFnSc2
Centauri	Centaur	k1gFnSc2
</s>
<s>
Systém	systém	k1gInSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
tří	tři	k4xCgFnPc2
hvězd	hvězda	k1gFnPc2
<g/>
:	:	kIx,
Alfa	alfa	k1gFnSc1
Centauri	Centaur	k1gFnSc2
A	A	kA
<g/>
,	,	kIx,
Alfa	alfa	k1gFnSc1
Centauri	Centaur	k1gFnSc2
B	B	kA
a	a	k8xC
Alfa	alfa	k1gFnSc1
Centauri	Centaur	k1gFnSc2
C	C	kA
(	(	kIx(
<g/>
také	také	k9
nazývaná	nazývaný	k2eAgFnSc1d1
Proxima	Proxima	k1gFnSc1
Centauri	Centaur	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnPc1
dvě	dva	k4xCgFnPc1
jmenované	jmenovaná	k1gFnPc1
tvoří	tvořit	k5eAaImIp3nP
těsnou	těsný	k2eAgFnSc4d1
dvojhvězdu	dvojhvězda	k1gFnSc4
s	s	k7c7
oběžnou	oběžný	k2eAgFnSc7d1
dobou	doba	k1gFnSc7
80	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oběžná	oběžný	k2eAgFnSc1d1
doba	doba	k1gFnSc1
kolem	kolem	k7c2
společného	společný	k2eAgNnSc2d1
těžiště	těžiště	k1gNnSc2
je	být	k5eAaImIp3nS
v	v	k7c6
průměrné	průměrný	k2eAgFnSc6d1
vzdálenosti	vzdálenost	k1gFnSc6
23	#num#	k4
AU	au	k0
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
o	o	k7c4
trošku	troška	k1gFnSc4
více	hodně	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
vzdálenost	vzdálenost	k1gFnSc4
Uranu	Uran	k1gInSc2
od	od	k7c2
Slunce	slunce	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
jsou	být	k5eAaImIp3nP
tak	tak	k6eAd1
blízko	blízko	k6eAd1
sebe	sebe	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
pouhým	pouhý	k2eAgNnSc7d1
okem	oke	k1gNnSc7
nelze	lze	k6eNd1
rozlišit	rozlišit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozlišit	rozlišit	k5eAaPmF
je	on	k3xPp3gNnSc4
lze	lze	k6eAd1
ale	ale	k8xC
už	už	k6eAd1
s	s	k7c7
dalekohledem	dalekohled	k1gInSc7
s	s	k7c7
5	#num#	k4
<g/>
cm	cm	kA
objektivem	objektiv	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Alfa	alfa	k1gFnSc1
Centauri	Centaur	k1gFnSc2
C	C	kA
<g/>
,	,	kIx,
známější	známý	k2eAgFnSc1d2
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Proxima	Proximum	k1gNnSc2
Centauri	Centaur	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
červený	červený	k2eAgMnSc1d1
trpaslík	trpaslík	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
je	být	k5eAaImIp3nS
od	od	k7c2
obou	dva	k4xCgFnPc2
jmenovaných	jmenovaný	k2eAgFnPc2d1
hvězd	hvězda	k1gFnPc2
vzdálen	vzdáleno	k1gNnPc2
13	#num#	k4
000	#num#	k4
AU	au	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
by	by	kYmCp3nS
obíhal	obíhat	k5eAaImAgMnS
kolem	kolem	k7c2
společného	společný	k2eAgNnSc2d1
těžiště	těžiště	k1gNnSc2
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
by	by	kYmCp3nS
odhadovaná	odhadovaný	k2eAgFnSc1d1
doba	doba	k1gFnSc1
oběhu	oběh	k1gInSc2
více	hodně	k6eAd2
než	než	k8xS
500	#num#	k4
000	#num#	k4
let	léto	k1gNnPc2
<g/>
;	;	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
prokázali	prokázat	k5eAaPmAgMnP
astronomové	astronom	k1gMnPc1
z	z	k7c2
Pařížské	pařížský	k2eAgFnSc2d1
observatoře	observatoř	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
hvězda	hvězda	k1gFnSc1
skutečně	skutečně	k6eAd1
součástí	součást	k1gFnSc7
systému	systém	k1gInSc2
Alfa	alfa	k1gNnSc2
Centauri	Centaur	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
Proxima	Proxima	k1gFnSc1
latinsky	latinsky	k6eAd1
znamená	znamenat	k5eAaImIp3nS
nejbližší	blízký	k2eAgFnSc1d3
(	(	kIx(
<g/>
v	v	k7c6
ženském	ženský	k2eAgInSc6d1
rodu	rod	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Centauri	Centauri	k1gNnSc1
název	název	k1gInSc1
souhvězdí	souhvězdí	k1gNnPc2
v	v	k7c6
genitivu	genitiv	k1gInSc6
(	(	kIx(
<g/>
název	název	k1gInSc1
tedy	tedy	k9
znamená	znamenat	k5eAaImIp3nS
Nejbližší	blízký	k2eAgNnSc1d3
[	[	kIx(
<g/>
rozuměj	rozumět	k5eAaImRp2nS
„	„	k?
<g/>
hvězda	hvězda	k1gFnSc1
<g/>
“	“	k?
<g/>
]	]	kIx)
v	v	k7c6
Kentauru	kentaur	k1gMnSc6
(	(	kIx(
<g/>
doslova	doslova	k6eAd1
Kentaura	kentaur	k1gMnSc2
<g/>
))	))	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc4
vzdálenost	vzdálenost	k1gFnSc4
od	od	k7c2
Země	zem	k1gFnSc2
je	být	k5eAaImIp3nS
jen	jen	k9
4,24	4,24	k4
světelných	světelný	k2eAgNnPc2d1
let	léto	k1gNnPc2
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
je	být	k5eAaImIp3nS
zároveň	zároveň	k6eAd1
druhou	druhý	k4xOgFnSc7
nejbližší	blízký	k2eAgFnSc7d3
hvězdou	hvězda	k1gFnSc7
<g/>
,	,	kIx,
hned	hned	k6eAd1
po	po	k7c6
Slunci	slunce	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouhým	pouhý	k2eAgNnSc7d1
okem	oke	k1gNnSc7
však	však	k9
není	být	k5eNaImIp3nS
viditelná	viditelný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Planety	planeta	k1gFnPc1
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
potřebuje	potřebovat	k5eAaImIp3nS
aktualizaci	aktualizace	k1gFnSc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
obsahuje	obsahovat	k5eAaImIp3nS
zastaralé	zastaralý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
odrážela	odrážet	k5eAaImAgFnS
aktuální	aktuální	k2eAgInSc4d1
stav	stav	k1gInSc4
a	a	k8xC
nedávné	dávný	k2eNgFnPc4d1
události	událost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podívejte	podívat	k5eAaPmRp2nP,k5eAaImRp2nP
se	se	k3xPyFc4
též	též	k9
na	na	k7c4
diskusní	diskusní	k2eAgFnSc4d1
stránku	stránka	k1gFnSc4
<g/>
,	,	kIx,
zda	zda	k8xS
tam	tam	k6eAd1
nejsou	být	k5eNaImIp3nP
náměty	námět	k1gInPc4
k	k	k7c3
doplnění	doplnění	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
informace	informace	k1gFnPc4
nemažte	mazat	k5eNaImRp2nP
<g/>
,	,	kIx,
raději	rád	k6eAd2
je	on	k3xPp3gInPc4
převeďte	převést	k5eAaPmRp2nP
do	do	k7c2
minulého	minulý	k2eAgInSc2d1
času	čas	k1gInSc2
a	a	k8xC
případně	případně	k6eAd1
přesuňte	přesunout	k5eAaPmRp2nP
do	do	k7c2
části	část	k1gFnSc2
článku	článek	k1gInSc2
věnované	věnovaný	k2eAgFnPc4d1
dějinám	dějiny	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2012	#num#	k4
ohlásila	ohlásit	k5eAaPmAgFnS
ESA	eso	k1gNnPc1
objev	objev	k1gInSc1
první	první	k4xOgFnSc2
planety	planeta	k1gFnSc2
obíhající	obíhající	k2eAgNnPc1d1
Alfa	alfa	k1gNnPc1
Centauri	Centaur	k1gFnSc2
B.	B.	kA
Planeta	planeta	k1gFnSc1
Alfa	alfa	k1gFnSc1
Centauri	Centaur	k1gFnSc2
Bb	Bb	k1gFnSc2
je	být	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
velikosti	velikost	k1gFnSc3
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
není	být	k5eNaImIp3nS
v	v	k7c6
obyvatelné	obyvatelný	k2eAgFnSc6d1
zóně	zóna	k1gFnSc6
ve	v	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
by	by	kYmCp3nP
mohla	moct	k5eAaImAgFnS
existovat	existovat	k5eAaImF
voda	voda	k1gFnSc1
v	v	k7c6
kapalném	kapalný	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povrchovou	povrchový	k2eAgFnSc4d1
teplotu	teplota	k1gFnSc4
má	mít	k5eAaImIp3nS
nejméně	málo	k6eAd3
1	#num#	k4
500	#num#	k4
K	k	k7c3
(	(	kIx(
<g/>
zhruba	zhruba	k6eAd1
1	#num#	k4
200	#num#	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
a	a	k8xC
hvězdu	hvězda	k1gFnSc4
obíhá	obíhat	k5eAaImIp3nS
za	za	k7c4
3,2	3,2	k4
dní	den	k1gInPc2
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
6	#num#	k4
milionů	milion	k4xCgInPc2
km	km	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Zatím	zatím	k6eAd1
je	být	k5eAaImIp3nS
jisté	jistý	k2eAgNnSc1d1
že	že	k8xS
okolo	okolo	k7c2
žádné	žádný	k3yNgFnSc2
z	z	k7c2
hvězd	hvězda	k1gFnPc2
systému	systém	k1gInSc2
Alfa	alfa	k1gFnSc1
Centauri	Centaur	k1gFnSc2
neobíhá	obíhat	k5eNaImIp3nS
žádná	žádný	k3yNgFnSc1
obří	obří	k2eAgFnSc1d1
plynná	plynný	k2eAgFnSc1d1
planeta	planeta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
srpnu	srpen	k1gInSc6
2016	#num#	k4
oznámila	oznámit	k5eAaPmAgFnS
Evropská	evropský	k2eAgFnSc1d1
jižní	jižní	k2eAgFnSc1d1
observatoř	observatoř	k1gFnSc1
objev	objev	k1gInSc1
nejbližší	blízký	k2eAgInPc1d3
exoplanety	exoplanet	k1gInPc1
Proxima	Proxim	k1gMnSc2
Centauri	Centaur	k1gFnSc2
b	b	k?
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
obyvatelné	obyvatelný	k2eAgFnSc6d1
zóně	zóna	k1gFnSc6
hvězdy	hvězda	k1gFnSc2
Proxima	Proximum	k1gNnSc2
Centauri	Centaur	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Poloha	poloha	k1gFnSc1
na	na	k7c6
obloze	obloha	k1gFnSc6
</s>
<s>
Spolu	spolu	k6eAd1
se	s	k7c7
4,4	4,4	k4
<g/>
°	°	k?
vzdálenou	vzdálený	k2eAgFnSc7d1
hvězdou	hvězda	k1gFnSc7
beta	beta	k1gNnSc2
Centauri	Centaur	k1gFnSc2
a	a	k8xC
třemi	tři	k4xCgFnPc7
nejjasnějšími	jasný	k2eAgFnPc7d3
hvězdami	hvězda	k1gFnPc7
souhvězdí	souhvězdí	k1gNnPc2
Jižního	jižní	k2eAgInSc2d1
kříže	kříž	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
od	od	k7c2
dvojhvězdy	dvojhvězda	k1gFnSc2
leží	ležet	k5eAaImIp3nS
západně	západně	k6eAd1
<g/>
,	,	kIx,
tvoří	tvořit	k5eAaImIp3nP
nejvýraznější	výrazný	k2eAgNnSc4d3
nahromadění	nahromadění	k1gNnSc4
hvězd	hvězda	k1gFnPc2
první	první	k4xOgFnSc3
velikosti	velikost	k1gFnSc3
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
obloze	obloha	k1gFnSc6
v	v	k7c6
úhlové	úhlový	k2eAgFnSc6d1
vzdálenosti	vzdálenost	k1gFnSc6
natažené	natažený	k2eAgFnSc2d1
dlaně	dlaň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Alfa	alfa	k1gFnSc1
a	a	k8xC
beta	beta	k1gNnSc1
Centauri	Centauri	k1gNnSc2
ukazují	ukazovat	k5eAaImIp3nP
směr	směr	k1gInSc4
k	k	k7c3
souhvězdí	souhvězdí	k1gNnSc3
Jižního	jižní	k2eAgInSc2d1
kříže	kříž	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sloužily	sloužit	k5eAaImAgInP
jako	jako	k8xS,k8xC
ukazatel	ukazatel	k1gInSc1
na	na	k7c6
souhvězdí	souhvězdí	k1gNnSc6
Jižního	jižní	k2eAgInSc2d1
kříže	kříž	k1gInSc2
a	a	k8xC
k	k	k7c3
rozeznání	rozeznání	k1gNnSc3
pravého	pravý	k2eAgInSc2d1
Jižního	jižní	k2eAgInSc2d1
kříže	kříž	k1gInSc2
od	od	k7c2
často	často	k6eAd1
zaměňovaného	zaměňovaný	k2eAgInSc2d1
nepravého	pravý	k2eNgInSc2d1
Jižního	jižní	k2eAgInSc2d1
kříže	kříž	k1gInSc2
směrem	směr	k1gInSc7
na	na	k7c4
východ	východ	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
tvoří	tvořit	k5eAaImIp3nS
asterismus	asterismus	k1gInSc4
hvězd	hvězda	k1gFnPc2
v	v	k7c6
souhvězdí	souhvězdí	k1gNnSc6
Plachet	plachta	k1gFnPc2
<g/>
,	,	kIx,
Lodní	lodní	k2eAgFnSc2d1
zádě	záď	k1gFnSc2
a	a	k8xC
Lodního	lodní	k2eAgInSc2d1
kýlu	kýl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Falešný	falešný	k2eAgInSc1d1
Jižní	jižní	k2eAgInSc1d1
kříž	kříž	k1gInSc1
tvoří	tvořit	k5eAaImIp3nS
pouhým	pouhý	k2eAgNnSc7d1
okem	oke	k1gNnSc7
viditelné	viditelný	k2eAgFnSc2d1
hvězdy	hvězda	k1gFnSc2
Avior	Aviora	k1gFnPc2
<g/>
,	,	kIx,
Turais	Turais	k1gFnPc2
<g/>
,	,	kIx,
κ	κ	k?
Vel	velo	k1gNnPc2
a	a	k8xC
δ	δ	k?
Vel	velo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Alfa	alfa	k1gFnSc1
a	a	k8xC
beta	beta	k1gNnSc1
Centauri	Centaur	k1gFnSc2
leží	ležet	k5eAaImIp3nS
hluboko	hluboko	k6eAd1
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
obloze	obloha	k1gFnSc6
<g/>
,	,	kIx,
ze	z	k7c2
střední	střední	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
nejsou	být	k5eNaImIp3nP
viditelné	viditelný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižně	jižně	k6eAd1
od	od	k7c2
33	#num#	k4
<g/>
.	.	kIx.
rovnoběžky	rovnoběžka	k1gFnSc2
je	být	k5eAaImIp3nS
hvězda	hvězda	k1gFnSc1
cirkumpolární	cirkumpolární	k2eAgFnSc1d1
a	a	k8xC
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
nad	nad	k7c7
obzorem	obzor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Alfa	alfa	k1gFnSc1
Centauri	Centaur	k1gFnSc2
jako	jako	k8xC,k8xS
dvojhvězda	dvojhvězda	k1gFnSc1
</s>
<s>
Alfa	alfa	k1gFnSc1
Centauri	Centaur	k1gFnSc2
</s>
<s>
Zdánlivá	zdánlivý	k2eAgFnSc1d1
a	a	k8xC
skutečná	skutečný	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
alfy	alfa	k1gFnSc2
Centauri	Centaur	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
nakreslena	nakreslen	k2eAgFnSc1d1
skutečná	skutečný	k2eAgFnSc1d1
a	a	k8xC
zdánlivá	zdánlivý	k2eAgFnSc1d1
oběžná	oběžný	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
složky	složka	k1gFnSc2
B	B	kA
ke	k	k7c3
složce	složka	k1gFnSc3
A	a	k8xC
dvojhvězdy	dvojhvězda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slabá	slabý	k2eAgFnSc1d1
elipsa	elipsa	k1gFnSc1
ukazuje	ukazovat	k5eAaImIp3nS
zdánlivou	zdánlivý	k2eAgFnSc4d1
oběžnou	oběžný	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
ji	on	k3xPp3gFnSc4
vidí	vidět	k5eAaImIp3nS
pozorovatel	pozorovatel	k1gMnSc1
ze	z	k7c2
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolmý	kolmý	k2eAgInSc4d1
pohled	pohled	k1gInSc4
na	na	k7c4
oběžnou	oběžný	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
(	(	kIx(
<g/>
velká	velký	k2eAgFnSc1d1
elipsa	elipsa	k1gFnSc1
<g/>
)	)	kIx)
zvýrazňuje	zvýrazňovat	k5eAaImIp3nS
skutečnou	skutečný	k2eAgFnSc4d1
oběžnou	oběžný	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Dvojhvězda	dvojhvězda	k1gFnSc1
má	mít	k5eAaImIp3nS
absolutní	absolutní	k2eAgFnSc4d1
hvězdnou	hvězdný	k2eAgFnSc4d1
velikost	velikost	k1gFnSc4
4,1	4,1	k4
mag	mag	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouhým	pouhý	k2eAgNnSc7d1
okem	oke	k1gNnSc7
nejsou	být	k5eNaImIp3nP
obě	dva	k4xCgFnPc1
složky	složka	k1gFnPc1
dvojhvězdy	dvojhvězda	k1gFnSc2
ze	z	k7c2
Země	zem	k1gFnSc2
rozlišitelné	rozlišitelný	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teprve	teprve	k6eAd1
dalekohled	dalekohled	k1gInSc1
s	s	k7c7
5	#num#	k4
<g/>
cm	cm	kA
objektivem	objektiv	k1gInSc7
rozliší	rozlišit	k5eAaPmIp3nS
obě	dva	k4xCgFnPc1
složky	složka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Jednou	jeden	k4xCgFnSc7
za	za	k7c4
79,9	79,9	k4
let	léto	k1gNnPc2
oběhnou	oběhnout	k5eAaPmIp3nP
obě	dva	k4xCgFnPc1
složky	složka	k1gFnPc1
dvojhvězdy	dvojhvězda	k1gFnSc2
na	na	k7c6
silně	silně	k6eAd1
eliptických	eliptický	k2eAgFnPc6d1
dráhách	dráha	k1gFnPc6
o	o	k7c6
excentricitě	excentricita	k1gFnSc6
0,519	0,519	k4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
jejich	jejich	k3xOp3gFnSc1
vzdálenost	vzdálenost	k1gFnSc1
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
od	od	k7c2
11,5	11,5	k4
až	až	k9
36,3	36,3	k4
AU	au	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Minimum	minimum	k1gNnSc1
odpovídá	odpovídat	k5eAaImIp3nS
vzdálenosti	vzdálenost	k1gFnPc4
Saturnu	Saturn	k1gInSc2
<g/>
,	,	kIx,
maximum	maximum	k1gNnSc4
vzdálenosti	vzdálenost	k1gFnSc2
Neptunu	Neptun	k1gInSc2
od	od	k7c2
Slunce	slunce	k1gNnSc2
<g/>
;	;	kIx,
velká	velký	k2eAgFnSc1d1
poloosa	poloosa	k1gFnSc1
dráhy	dráha	k1gFnSc2
je	být	k5eAaImIp3nS
23,9	23,9	k4
AU	au	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
1995	#num#	k4
byla	být	k5eAaImAgFnS
dosažena	dosáhnout	k5eAaPmNgFnS
nejvyšší	vysoký	k2eAgFnSc1d3
vzdálenost	vzdálenost	k1gFnSc1
obou	dva	k4xCgFnPc2
složek	složka	k1gFnPc2
<g/>
,	,	kIx,
nejnižší	nízký	k2eAgFnSc1d3
vzdálenost	vzdálenost	k1gFnSc1
bude	být	k5eAaImBp3nS
dosažena	dosáhnout	k5eAaPmNgFnS
v	v	k7c6
květnu	květen	k1gInSc6
2035	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
velké	velký	k2eAgFnSc2d1
poloosy	poloosa	k1gFnSc2
dráhy	dráha	k1gFnSc2
a	a	k8xC
z	z	k7c2
oběžné	oběžný	k2eAgFnSc2d1
doby	doba	k1gFnSc2
hvězdy	hvězda	k1gFnSc2
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
spočítat	spočítat	k5eAaPmF
hmotnost	hmotnost	k1gFnSc4
obou	dva	k4xCgFnPc2
složek	složka	k1gFnPc2
<g/>
,	,	kIx,
vychází	vycházet	k5eAaImIp3nS
2,08	2,08	k4
hmotnosti	hmotnost	k1gFnSc2
našeho	náš	k3xOp1gNnSc2
Slunce	slunce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Úhlová	úhlový	k2eAgFnSc1d1
vzdálenost	vzdálenost	k1gFnSc1
a	a	k8xC
poziční	poziční	k2eAgInSc1d1
úhel	úhel	k1gInSc1
obou	dva	k4xCgFnPc2
složek	složka	k1gFnPc2
se	se	k3xPyFc4
díky	dík	k1gInPc7
relativně	relativně	k6eAd1
krátké	krátký	k2eAgFnPc4d1
oběžné	oběžný	k2eAgFnPc4d1
dráhy	dráha	k1gFnPc4
zřetelně	zřetelně	k6eAd1
mění	měnit	k5eAaImIp3nS
během	během	k7c2
několika	několik	k4yIc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
jednoho	jeden	k4xCgInSc2
oběhu	oběh	k1gInSc2
se	se	k3xPyFc4
úhlová	úhlový	k2eAgFnSc1d1
vzdálenost	vzdálenost	k1gFnSc1
pohybuje	pohybovat	k5eAaImIp3nS
2	#num#	k4
<g/>
″	″	k?
až	až	k9
22	#num#	k4
<g/>
″	″	k?
<g/>
.	.	kIx.
</s>
<s>
Nejčastěji	často	k6eAd3
uváděné	uváděný	k2eAgFnPc4d1
aktuální	aktuální	k2eAgFnPc4d1
polohy	poloha	k1gFnPc4
všech	všecek	k3xTgFnPc2
tří	tři	k4xCgFnPc2
hvězd	hvězda	k1gFnPc2
v	v	k7c6
literatuře	literatura	k1gFnSc6
<g/>
,	,	kIx,
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
měření	měření	k1gNnSc2
astrometrické	astrometrický	k2eAgFnSc2d1
družice	družice	k1gFnSc2
Hipparcos	Hipparcosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Relativní	relativní	k2eAgFnSc1d1
poloha	poloha	k1gFnSc1
složky	složka	k1gFnSc2
B	B	kA
ke	k	k7c3
složce	složka	k1gFnSc3
A	a	k9
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Úhlová	úhlový	k2eAgFnSc1d1
vzdálenost	vzdálenost	k1gFnSc1
</s>
<s>
Poziční	poziční	k2eAgInSc1d1
úhel	úhel	k1gInSc1
</s>
<s>
1990	#num#	k4
</s>
<s>
19,7	19,7	k4
<g/>
″	″	k?
</s>
<s>
215	#num#	k4
<g/>
°	°	k?
</s>
<s>
1995	#num#	k4
</s>
<s>
17,3	17,3	k4
<g/>
″	″	k?
</s>
<s>
218	#num#	k4
<g/>
°	°	k?
</s>
<s>
2000	#num#	k4
</s>
<s>
14,1	14,1	k4
<g/>
″	″	k?
</s>
<s>
222	#num#	k4
<g/>
°	°	k?
</s>
<s>
2005	#num#	k4
</s>
<s>
10,5	10,5	k4
<g/>
″	″	k?
</s>
<s>
230	#num#	k4
<g/>
°	°	k?
</s>
<s>
2010	#num#	k4
</s>
<s>
6,8	6,8	k4
<g/>
″	″	k?
</s>
<s>
245	#num#	k4
<g/>
°	°	k?
</s>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
</s>
<s>
Alfa	alfa	k1gFnSc1
Centauri	Centaur	k1gFnSc2
A	A	kA
a	a	k8xC
B	B	kA
je	být	k5eAaImIp3nS
pár	pár	k4xCyI
hvězd	hvězda	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
vznikl	vzniknout	k5eAaPmAgInS
společně	společně	k6eAd1
před	před	k7c7
6,5	6,5	k4
±	±	k?
0,3	0,3	k4
miliardami	miliarda	k4xCgFnPc7
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obě	dva	k4xCgFnPc1
jsou	být	k5eAaImIp3nP
hvězdami	hvězda	k1gFnPc7
hlavní	hlavní	k2eAgFnSc2d1
posloupnosti	posloupnost	k1gFnSc2
a	a	k8xC
nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
ve	v	k7c6
stabilní	stabilní	k2eAgFnSc6d1
fázi	fáze	k1gFnSc6
fúze	fúze	k1gFnSc1
vodíku	vodík	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
přeměny	přeměna	k1gFnSc2
v	v	k7c4
helium	helium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alfa	alfa	k1gFnSc1
Centauri	Centaur	k1gFnSc2
A	a	k9
je	být	k5eAaImIp3nS
hmotnější	hmotný	k2eAgFnSc1d2
než	než	k8xS
Alfa	alfa	k1gFnSc1
Centauri	Centaur	k1gFnSc2
B	B	kA
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
kratší	krátký	k2eAgNnSc1d2
dobu	doba	k1gFnSc4
na	na	k7c4
hlavní	hlavní	k2eAgFnPc4d1
posloupnosti	posloupnost	k1gFnPc4
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
změní	změnit	k5eAaPmIp3nS
v	v	k7c4
červeného	červený	k2eAgMnSc4d1
obra	obr	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
proto	proto	k8xC
v	v	k7c6
protikladu	protiklad	k1gInSc6
s	s	k7c7
menší	malý	k2eAgFnSc7d2
Alfa	alfa	k1gNnSc4
Centauri	Centaur	k1gFnSc2
B	B	kA
již	již	k6eAd1
polovinu	polovina	k1gFnSc4
života	život	k1gInSc2
za	za	k7c7
sebou	se	k3xPyFc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proxima	Proxima	k1gFnSc1
Centauri	Centauri	k1gNnSc2
je	být	k5eAaImIp3nS
stará	starý	k2eAgFnSc1d1
4,85	4,85	k4
miliard	miliarda	k4xCgFnPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
Alfy	alfa	k1gFnSc2
Centauri	Centaur	k1gFnSc2
A	A	kA
a	a	k8xC
B	B	kA
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
někdy	někdy	k6eAd1
zvané	zvaný	k2eAgInPc1d1
α	α	k?
Cen	cena	k1gFnPc2
AB	AB	kA
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
provedena	proveden	k2eAgFnSc1d1
detailní	detailní	k2eAgNnSc4d1
pozorování	pozorování	k1gNnSc4
vlnění	vlnění	k1gNnSc2
na	na	k7c6
povrchu	povrch	k1gInSc6
hvězdy	hvězda	k1gFnSc2
<g/>
,	,	kIx,
ze	z	k7c2
kterých	který	k3yQgFnPc2,k3yIgFnPc2,k3yRgFnPc2
hvězdná	hvězdný	k2eAgFnSc1d1
seismologie	seismologie	k1gFnSc1
dokáže	dokázat	k5eAaPmIp3nS
vyvodit	vyvodit	k5eAaBmF,k5eAaPmF
závěry	závěr	k1gInPc4
o	o	k7c6
vnitřní	vnitřní	k2eAgFnSc6d1
stavbě	stavba	k1gFnSc6
hvězdy	hvězda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kombinaci	kombinace	k1gFnSc6
s	s	k7c7
tradičními	tradiční	k2eAgFnPc7d1
metodami	metoda	k1gFnPc7
pozorování	pozorování	k1gNnSc4
astronomové	astronom	k1gMnPc1
obdrželi	obdržet	k5eAaPmAgMnP
přesné	přesný	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
vlastností	vlastnost	k1gFnSc7
hvězdy	hvězda	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
nelze	lze	k6eNd1
získat	získat	k5eAaPmF
jednotlivými	jednotlivý	k2eAgFnPc7d1
metodami	metoda	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Porovnání	porovnání	k1gNnSc1
podílu	podíl	k1gInSc2
jednotlivých	jednotlivý	k2eAgInPc2d1
prvků	prvek	k1gInPc2
v	v	k7c6
procentech	procento	k1gNnPc6
</s>
<s>
Jméno	jméno	k1gNnSc1
</s>
<s>
vodík	vodík	k1gInSc1
</s>
<s>
helium	helium	k1gNnSc1
</s>
<s>
těžké	těžký	k2eAgInPc1d1
prvky	prvek	k1gInPc1
</s>
<s>
α	α	k?
Centauri	Centauri	k1gNnSc1
A	a	k9
</s>
<s>
71,5	71,5	k4
</s>
<s>
25,8	25,8	k4
</s>
<s>
2,74	2,74	k4
</s>
<s>
α	α	k?
Centauri	Centauri	k1gNnSc1
B	B	kA
</s>
<s>
69,4	69,4	k4
</s>
<s>
27,7	27,7	k4
</s>
<s>
2,89	2,89	k4
</s>
<s>
Slunce	slunce	k1gNnSc1
</s>
<s>
73,3	73,3	k4
</s>
<s>
24,5	24,5	k4
</s>
<s>
1,81	1,81	k4
</s>
<s>
Parametry	parametr	k1gInPc1
</s>
<s>
Porovnání	porovnání	k1gNnSc1
hvězd	hvězda	k1gFnPc2
v	v	k7c6
systému	systém	k1gInSc6
Alfa	alfa	k1gFnSc1
Centauri	Centauri	k1gNnSc2
a	a	k8xC
Slunce	slunce	k1gNnSc2
<g/>
:	:	kIx,
</s>
<s>
Parametr	parametr	k1gInSc1
</s>
<s>
α	α	k?
Centauri	Centauri	k1gNnSc1
A	a	k9
</s>
<s>
α	α	k?
Centauri	Centauri	k1gNnSc1
B	B	kA
</s>
<s>
Proxima	Proxima	k1gFnSc1
</s>
<s>
Slunce	slunce	k1gNnSc1
</s>
<s>
Jednotka	jednotka	k1gFnSc1
</s>
<s>
věk	věk	k1gInSc1
</s>
<s>
6520	#num#	k4
</s>
<s>
6520	#num#	k4
</s>
<s>
4850	#num#	k4
</s>
<s>
4650	#num#	k4
</s>
<s>
milionů	milion	k4xCgInPc2
let	léto	k1gNnPc2
</s>
<s>
velikost	velikost	k1gFnSc1
</s>
<s>
1,100	1,100	k4
</s>
<s>
0,907	0,907	k4
</s>
<s>
0,123	0,123	k4
</s>
<s>
1,000	1,000	k4
</s>
<s>
hmotnost	hmotnost	k1gFnSc1
Slunce	slunce	k1gNnSc2
</s>
<s>
poloměr	poloměr	k1gInSc1
</s>
<s>
1,227	1,227	k4
</s>
<s>
0,865	0,865	k4
</s>
<s>
0,145	0,145	k4
</s>
<s>
1,000	1,000	k4
</s>
<s>
poloměr	poloměr	k1gInSc1
Slunce	slunce	k1gNnSc2
</s>
<s>
teplota	teplota	k1gFnSc1
</s>
<s>
5790	#num#	k4
</s>
<s>
5260	#num#	k4
</s>
<s>
3040	#num#	k4
</s>
<s>
5770	#num#	k4
</s>
<s>
Kelvin	kelvin	k1gInSc1
</s>
<s>
Alfa	alfa	k1gFnSc1
Centauri	Centaur	k1gFnSc2
ve	v	k7c6
fikci	fikce	k1gFnSc6
</s>
<s>
Jako	jako	k9
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejjasnějších	jasný	k2eAgFnPc2d3
hvězd	hvězda	k1gFnPc2
na	na	k7c6
noční	noční	k2eAgFnSc6d1
obloze	obloha	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
nejbližší	blízký	k2eAgMnSc1d3
známý	známý	k1gMnSc1
hvězdný	hvězdný	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
,	,	kIx,
Alfa	alfa	k1gFnSc1
Centauri	Centaur	k1gFnSc2
systém	systém	k1gInSc1
hrála	hrát	k5eAaImAgFnS
roli	role	k1gFnSc4
v	v	k7c6
mnoha	mnoho	k4c6
smyšlených	smyšlený	k2eAgFnPc6d1
pracích	práce	k1gFnPc6
literatury	literatura	k1gFnSc2
<g/>
,	,	kIx,
populární	populární	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
televizi	televize	k1gFnSc6
a	a	k8xC
filmu	film	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Písně	píseň	k1gFnPc1
kosmické	kosmický	k2eAgFnSc2d1
(	(	kIx(
<g/>
1878	#num#	k4
<g/>
)	)	kIx)
–	–	k?
jedna	jeden	k4xCgFnSc1
z	z	k7c2
básní	báseň	k1gFnPc2
Jana	Jan	k1gMnSc2
Nerudy	Neruda	k1gMnSc2
ve	v	k7c6
sbírce	sbírka	k1gFnSc6
Písně	píseň	k1gFnSc2
kosmické	kosmický	k2eAgFnSc2d1
se	se	k3xPyFc4
obrací	obracet	k5eAaImIp3nS
k	k	k7c3
Alfě	alfa	k1gFnSc3
Centauri	Centaur	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Homo	Homo	k1gNnSc1
Sol	sol	k1gNnSc2
(	(	kIx(
<g/>
1940	#num#	k4
<g/>
)	)	kIx)
–	–	k?
sci-fi	sci-fi	k1gFnSc1
povídka	povídka	k1gFnSc1
Isaaca	Isaaca	k1gFnSc1
Asimova	Asimův	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
lidstvo	lidstvo	k1gNnSc1
objeví	objevit	k5eAaPmIp3nS
cestování	cestování	k1gNnSc1
hyperprostorem	hyperprostor	k1gInSc7
<g/>
,	,	kIx,
zaměří	zaměřit	k5eAaPmIp3nS
svoji	svůj	k3xOyFgFnSc4
první	první	k4xOgFnSc4
misi	mise	k1gFnSc4
k	k	k7c3
přistání	přistání	k1gNnSc3
na	na	k7c6
planetě	planeta	k1gFnSc6
Alpha	Alph	k1gMnSc2
Centauri	Centaur	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
mu	on	k3xPp3gMnSc3
poté	poté	k6eAd1
otevře	otevřít	k5eAaPmIp3nS
dveře	dveře	k1gFnPc4
k	k	k7c3
přijetí	přijetí	k1gNnSc3
do	do	k7c2
galaktické	galaktický	k2eAgFnSc2d1
federace	federace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Planeta	planeta	k1gFnSc1
tří	tři	k4xCgInPc2
sluncí	slunce	k1gNnPc2
(	(	kIx(
<g/>
1954	#num#	k4
<g/>
)	)	kIx)
–	–	k?
sci-fi	sci-fi	k1gNnSc1
kniha	kniha	k1gFnSc1
Vladimíra	Vladimír	k1gMnSc2
Babuly	babula	k1gFnSc2
(	(	kIx(
<g/>
2	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
trilogie	trilogie	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ikarie	Ikarie	k1gFnSc1
XB	XB	kA
1	#num#	k4
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
)	)	kIx)
–	–	k?
sci-fi	sci-fi	k1gNnPc2
film	film	k1gInSc1
Jindřicha	Jindřich	k1gMnSc2
Poláka	Polák	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kosmická	kosmický	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Ikarie	Ikarie	k1gFnSc2
XB	XB	kA
1	#num#	k4
se	se	k3xPyFc4
s	s	k7c7
vědci	vědec	k1gMnPc7
a	a	k8xC
astronauty	astronaut	k1gMnPc7
na	na	k7c6
palubě	paluba	k1gFnSc6
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
na	na	k7c4
cestu	cesta	k1gFnSc4
k	k	k7c3
Bílé	bílý	k2eAgFnSc3d1
planetě	planeta	k1gFnSc3
v	v	k7c6
soustavě	soustava	k1gFnSc6
Alfa	alfa	k1gFnSc1
Centauri	Centaur	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Klany	klan	k1gInPc1
alfanského	alfanský	k2eAgInSc2d1
měsíce	měsíc	k1gInSc2
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
–	–	k?
sci-fi	sci-fi	k1gNnSc1
kniha	kniha	k1gFnSc1
spisovatele	spisovatel	k1gMnSc2
Philipa	Philip	k1gMnSc2
K.	K.	kA
Dicka	Dicek	k1gMnSc2
<g/>
,	,	kIx,
vykresluje	vykreslovat	k5eAaImIp3nS
snahu	snaha	k1gFnSc4
znovu	znovu	k6eAd1
prosadit	prosadit	k5eAaPmF
autoritu	autorita	k1gFnSc4
lidí	člověk	k1gMnPc2
nad	nad	k7c7
jejich	jejich	k3xOp3gFnSc7
bývalou	bývalý	k2eAgFnSc7d1
kolonií	kolonie	k1gFnSc7
na	na	k7c6
obytném	obytný	k2eAgInSc6d1
měsíci	měsíc	k1gInSc6
Alpha	Alpha	k1gMnSc1
III	III	kA
M	M	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
u	u	k7c2
třetí	třetí	k4xOgFnSc2
obří	obří	k2eAgFnSc2d1
planety	planeta	k1gFnSc2
v	v	k7c6
Alfa	alfa	k1gNnSc6
Centauri	Centaur	k1gFnSc2
nazvané	nazvaný	k2eAgFnSc2d1
Alphane	Alphan	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Muž	muž	k1gMnSc1
v	v	k7c6
labyrintu	labyrint	k1gInSc6
(	(	kIx(
<g/>
angl.	angl.	k?
The	The	k1gMnSc1
Man	Man	k1gMnSc1
in	in	k?
the	the	k?
Maze	Maga	k1gFnSc3
<g/>
)	)	kIx)
–	–	k?
postava	postava	k1gFnSc1
jménem	jméno	k1gNnSc7
Ned	Ned	k1gFnSc2
Rawlins	Rawlinsa	k1gFnPc2
se	se	k3xPyFc4
letěla	letět	k5eAaImAgFnS
po	po	k7c6
studiích	studio	k1gNnPc6
podívat	podívat	k5eAaPmF,k5eAaImF
na	na	k7c4
planetu	planeta	k1gFnSc4
Alfa	alfa	k1gNnSc2
Centauri	Centauri	k1gNnSc2
IV	IV	kA
v	v	k7c6
systému	systém	k1gInSc6
Alfa	alfa	k1gNnSc2
Centauri	Centaur	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vědeckofantastický	vědeckofantastický	k2eAgInSc4d1
román	román	k1gInSc4
z	z	k7c2
roku	rok	k1gInSc2
1969	#num#	k4
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
americký	americký	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
Robert	Robert	k1gMnSc1
Silverberg	Silverberg	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Neuromancer	Neuromancer	k1gInSc1
(	(	kIx(
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
–	–	k?
kyberpunková	kyberpunkový	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
Williama	William	k1gMnSc2
Gibsona	Gibson	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nová	nový	k2eAgFnSc1d1
umělá	umělý	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
Neuromancer	Neuromancra	k1gFnPc2
najde	najít	k5eAaPmIp3nS
řadu	řada	k1gFnSc4
přenosů	přenos	k1gInPc2
zaznamenaných	zaznamenaný	k2eAgInPc2d1
zpět	zpět	k6eAd1
v	v	k7c6
sedmdesátých	sedmdesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
naznačují	naznačovat	k5eAaImIp3nP
přítomnost	přítomnost	k1gFnSc4
umělé	umělý	k2eAgFnSc2d1
inteligence	inteligence	k1gFnSc2
v	v	k7c6
systému	systém	k1gInSc6
Alpha	Alpha	k1gFnSc1
Centauri	Centauri	k1gNnSc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
se	se	k3xPyFc4
jejich	jejich	k3xOp3gFnSc1
komunikace	komunikace	k1gFnSc1
propojí	propojit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Zpěv	zpěv	k1gInSc1
vzdálené	vzdálený	k2eAgFnSc2d1
Země	zem	k1gFnSc2
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
–	–	k?
vědecko-fantastická	vědecko-fantastický	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
spisovatele	spisovatel	k1gMnSc2
Arthura	Arthur	k1gMnSc2
C.	C.	kA
Clarka	Clarek	k1gMnSc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
existovala	existovat	k5eAaImAgFnS
planeta	planeta	k1gFnSc1
Pasadena	Pasaden	k2eAgFnSc1d1
v	v	k7c6
orbitě	orbita	k1gFnSc6
hvězdy	hvězda	k1gFnSc2
Alfa	alfa	k1gNnSc2
Centauri	Centaur	k1gFnSc2
A	A	kA
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
hůře	zle	k6eAd2
obyvatelná	obyvatelný	k2eAgFnSc1d1
podle	podle	k7c2
proměnné	proměnný	k2eAgFnSc2d1
blízkosti	blízkost	k1gFnSc2
k	k	k7c3
Alfa	alfa	k1gNnSc3
Centauri	Centaur	k1gFnSc2
B	B	kA
<g/>
,	,	kIx,
ke	k	k7c3
které	který	k3yIgMnPc4,k3yRgMnPc4,k3yQgMnPc4
lidstvo	lidstvo	k1gNnSc1
vyšle	vyslat	k5eAaPmIp3nS
loď	loď	k1gFnSc4
s	s	k7c7
lidskými	lidský	k2eAgNnPc7d1
a	a	k8xC
jinými	jiný	k2eAgNnPc7d1
embryi	embryo	k1gNnPc7
savců	savec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
loď	loď	k1gFnSc1
se	se	k3xPyFc4
sem	sem	k6eAd1
ze	z	k7c2
Sluneční	sluneční	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
vydala	vydat	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2553	#num#	k4
a	a	k8xC
kolonii	kolonie	k1gFnSc3
se	se	k3xPyFc4
vedlo	vést	k5eAaImAgNnS
překvapivě	překvapivě	k6eAd1
dobře	dobře	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Civilization	Civilization	k1gInSc1
I-V	I-V	k1gFnSc2
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
–	–	k?
série	série	k1gFnSc2
strategických	strategický	k2eAgFnPc2d1
počítačových	počítačový	k2eAgFnPc2d1
her	hra	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
bylo	být	k5eAaImAgNnS
možným	možný	k2eAgNnSc7d1
vítězstvím	vítězství	k1gNnSc7
být	být	k5eAaImF
první	první	k4xOgFnSc2
civilizace	civilizace	k1gFnSc2
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vyvine	vyvinout	k5eAaPmIp3nS
a	a	k8xC
pošle	poslat	k5eAaPmIp3nS
koloniální	koloniální	k2eAgFnSc1d1
loď	loď	k1gFnSc1
do	do	k7c2
hvězdného	hvězdný	k2eAgInSc2d1
systému	systém	k1gInSc2
Alfa	alfa	k1gFnSc1
Centauri	Centaur	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
Sid	Sid	k1gFnSc2
Meier	Meira	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Alpha	Alpha	k1gFnSc1
Centauri	Centauri	k1gNnPc7
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
pokračováním	pokračování	k1gNnSc7
těchto	tento	k3xDgFnPc2
sérií	série	k1gFnPc2
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
sedm	sedm	k4xCc1
konkurenčních	konkurenční	k2eAgFnPc2d1
ideologických	ideologický	k2eAgFnPc2d1
frakcí	frakce	k1gFnPc2
přiletí	přiletět	k5eAaPmIp3nS
na	na	k7c4
planetu	planeta	k1gFnSc4
Chiron	Chiron	k1gInSc4
v	v	k7c6
Alfa	alfa	k1gNnSc6
Centauri	Centauri	k1gNnSc2
a	a	k8xC
snaží	snažit	k5eAaImIp3nP
se	se	k3xPyFc4
ji	on	k3xPp3gFnSc4
kolonizovat	kolonizovat	k5eAaBmF
<g/>
.	.	kIx.
</s>
<s>
Encounter	Encounter	k1gInSc1
with	witha	k1gFnPc2
Tiber	Tibera	k1gFnPc2
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
–	–	k?
sci-fi	sci-fi	k1gFnSc1
kniha	kniha	k1gFnSc1
bývalého	bývalý	k2eAgMnSc2d1
astronauta	astronaut	k1gMnSc2
Buzze	Buzze	k1gFnSc2
Aldrina	Aldrin	k1gMnSc2
a	a	k8xC
spisovatele	spisovatel	k1gMnSc2
Johna	John	k1gMnSc2
Barnse	Barns	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
9000	#num#	k4
lety	let	k1gInPc7
druh	druh	k1gMnSc1
obývající	obývající	k2eAgInSc4d1
měsíc	měsíc	k1gInSc4
Tiber	Tibera	k1gFnPc2
plynného	plynný	k2eAgMnSc2d1
obra	obr	k1gMnSc2
u	u	k7c2
Alfa	alfa	k1gNnSc2
Centauri	Centaur	k1gFnSc2
A	a	k9
navštívil	navštívit	k5eAaPmAgMnS
Zemi	zem	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
zanechal	zanechat	k5eAaPmAgInS
encyklopedii	encyklopedie	k1gFnSc4
s	s	k7c7
shromážděnými	shromážděný	k2eAgInPc7d1
poznatky	poznatek	k1gInPc7
o	o	k7c6
jejich	jejich	k3xOp3gFnSc6
rase	rasa	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
objevena	objevit	k5eAaPmNgFnS
ve	v	k7c6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
vede	vést	k5eAaImIp3nS
lidstvo	lidstvo	k1gNnSc4
k	k	k7c3
expedici	expedice	k1gFnSc3
k	k	k7c3
Tiberu	Tiber	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Ztraceni	ztracen	k2eAgMnPc1d1
ve	v	k7c6
vesmíru	vesmír	k1gInSc6
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
–	–	k?
film	film	k1gInSc1
režírovaný	režírovaný	k2eAgInSc1d1
Stephenem	Stephen	k1gMnSc7
Hopkinsem	Hopkins	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2058	#num#	k4
když	když	k8xS
bude	být	k5eAaImBp3nS
Země	země	k1gFnSc1
brzy	brzy	k6eAd1
neobyvatelná	obyvatelný	k2eNgFnSc1d1
kvůli	kvůli	k7c3
globálnímu	globální	k2eAgNnSc3d1
znečišťování	znečišťování	k1gNnSc3
<g/>
,	,	kIx,
povede	povést	k5eAaPmIp3nS,k5eAaImIp3nS
profesor	profesor	k1gMnSc1
John	John	k1gMnSc1
Robinson	Robinson	k1gMnSc1
svou	svůj	k3xOyFgFnSc4
rodinu	rodina	k1gFnSc4
k	k	k7c3
obyvatelné	obyvatelný	k2eAgFnSc3d1
planetě	planeta	k1gFnSc3
Alpha	Alph	k1gMnSc2
Prime	prim	k1gInSc5
v	v	k7c6
systému	systém	k1gInSc6
Alfa	alfa	k1gFnSc1
Centauri	Centauri	k1gNnSc2
aby	aby	kYmCp3nS
ji	on	k3xPp3gFnSc4
připravil	připravit	k5eAaPmAgMnS
pro	pro	k7c4
kolonizaci	kolonizace	k1gFnSc4
vybudováním	vybudování	k1gNnSc7
hyperbrány	hyperbrán	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s>
Impostor	Impostor	k1gInSc1
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
–	–	k?
film	film	k1gInSc1
na	na	k7c4
motivy	motiv	k1gInPc4
krátké	krátký	k2eAgFnSc2d1
povídky	povídka	k1gFnSc2
Philipa	Philip	k1gMnSc2
K.	K.	kA
Dicka	Dicek	k1gMnSc2
<g/>
,	,	kIx,
vyobrazuje	vyobrazovat	k5eAaImIp3nS
válku	válka	k1gFnSc4
lidí	člověk	k1gMnPc2
v	v	k7c6
roce	rok	k1gInSc6
2079	#num#	k4
s	s	k7c7
civilizací	civilizace	k1gFnSc7
z	z	k7c2
Alfa	alfa	k1gNnSc2
Centauri	Centaur	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Transformers	Transformers	k1gInSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
–	–	k?
film	film	k1gInSc1
režírovaný	režírovaný	k2eAgMnSc1d1
Michael	Michael	k1gMnSc1
Bayem	Bayem	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cybertron	Cybertron	k1gInSc1
v	v	k7c6
systému	systém	k1gInSc6
Alfa	alfa	k1gFnSc1
Centauri	Centauri	k1gNnSc2
byla	být	k5eAaImAgFnS
domovská	domovský	k2eAgFnSc1d1
planeta	planeta	k1gFnSc1
Transformers	Transformersa	k1gFnPc2
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
kovový	kovový	k2eAgInSc4d1
povrch	povrch	k1gInSc4
a	a	k8xC
atmosféru	atmosféra	k1gFnSc4
dýchatelnou	dýchatelný	k2eAgFnSc4d1
lidmi	člověk	k1gMnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
kapalná	kapalný	k2eAgFnSc1d1
voda	voda	k1gFnSc1
byla	být	k5eAaImAgFnS
tak	tak	k6eAd1
vzácná	vzácný	k2eAgFnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
její	její	k3xOp3gFnSc1
existence	existence	k1gFnSc1
byla	být	k5eAaImAgFnS
na	na	k7c6
pochybách	pochyba	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Problém	problém	k1gInSc1
tří	tři	k4xCgNnPc2
těles	těleso	k1gNnPc2
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
-	-	kIx~
Sci-fi	sci-fi	k1gFnSc1
kniha	kniha	k1gFnSc1
ze	z	k7c2
série	série	k1gFnSc2
čínského	čínský	k2eAgMnSc4d1
autora	autor	k1gMnSc4
Liou	Lioa	k1gMnSc4
Cch	Cch	k1gMnSc4
<g/>
'	'	kIx"
<g/>
-sina	-sina	k6eAd1
Vzpomínky	vzpomínka	k1gFnPc1
na	na	k7c6
Zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příběh	příběh	k1gInSc1
pojednává	pojednávat	k5eAaImIp3nS
o	o	k7c6
plánované	plánovaný	k2eAgFnSc6d1
invazi	invaze	k1gFnSc6
<g/>
/	/	kIx~
<g/>
exodu	exodus	k1gInSc6
mimozemšťanů	mimozemšťan	k1gMnPc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
Trisolaranů	Trisolaran	k1gInPc2
<g/>
,	,	kIx,
řešících	řešící	k2eAgMnPc2d1
existenciální	existenciální	k2eAgFnSc4d1
krizi	krize	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
zapříčiňuje	zapříčiňovat	k5eAaImIp3nS
neustále	neustále	k6eAd1
hrozící	hrozící	k2eAgNnSc4d1
nebezpečí	nebezpečí	k1gNnSc4
zničení	zničení	k1gNnSc2
jejich	jejich	k3xOp3gFnSc2
planety	planeta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
žánr	žánr	k1gInSc4
tzv.	tzv.	kA
high	high	k1gInSc1
sci-fi	sci-fi	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Avatar	Avatar	k1gInSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
–	–	k?
oscarový	oscarový	k2eAgInSc1d1
film	film	k1gInSc1
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
a	a	k8xC
režíroval	režírovat	k5eAaImAgMnS
James	James	k1gMnSc1
Cameron	Cameron	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odehrává	odehrávat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
2154	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
RDA	RDA	kA
Corporation	Corporation	k1gInSc1
těží	těžet	k5eAaImIp3nS
vzácný	vzácný	k2eAgInSc4d1
minerál	minerál	k1gInSc4
zvaný	zvaný	k2eAgInSc4d1
unobtanium	unobtanium	k1gNnSc4
na	na	k7c6
Pandoře	Pandora	k1gFnSc6
<g/>
,	,	kIx,
obyvatelném	obyvatelný	k2eAgInSc6d1
měsíci	měsíc	k1gInSc6
plynného	plynný	k2eAgMnSc2d1
obra	obr	k1gMnSc2
Polyphema	Polyphem	k1gMnSc2
obíhající	obíhající	k2eAgFnSc4d1
hvězdu	hvězda	k1gFnSc4
Alfa	alfa	k1gNnSc2
Centauri	Centauri	k1gNnSc2
A.	A.	kA
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Alpha	Alph	k1gMnSc2
Centauri	Centaur	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
WILKINSON	WILKINSON	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnSc1
Eyes	Eyesa	k1gFnPc2
on	on	k3xPp3gMnSc1
the	the	k?
Sun	Sun	kA
<g/>
:	:	kIx,
A	A	kA
Guide	Guid	k1gInSc5
to	ten	k3xDgNnSc1
Satellite	Satellit	k1gInSc5
Images	Images	k1gMnSc1
and	and	k?
Amateur	Amateur	k1gMnSc1
Observation	Observation	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Springer-Verlag	Springer-Verlag	k1gInSc1
Berlin	berlina	k1gFnPc2
Heidelberg	Heidelberg	k1gInSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
642	#num#	k4
<g/>
-	-	kIx~
<g/>
22838	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.100	10.100	k4
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
978	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
642	#num#	k4
<g/>
-	-	kIx~
<g/>
22839	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
_	_	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
The	The	k1gFnSc2
Sun	Sun	kA
and	and	k?
Stars	Stars	k1gInSc1
<g/>
,	,	kIx,
s.	s.	k?
219	#num#	k4
<g/>
–	–	k?
<g/>
236	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
IAU	IAU	kA
Catalog	Catalog	k1gMnSc1
of	of	k?
Star	Star	kA
Names	Names	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kervella	Kervella	k1gMnSc1
<g/>
,	,	kIx,
P.	P.	kA
<g/>
,	,	kIx,
Thevenin	Thevenin	k2eAgMnSc1d1
<g/>
,	,	kIx,
F.	F.	kA
(	(	kIx(
<g/>
March	March	k1gInSc1
15	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
A	a	k9
Family	Famila	k1gFnPc1
Portrait	Portrait	k1gInSc1
of	of	k?
the	the	k?
Alpha	Alpha	k1gFnSc1
Centauri	Centauri	k1gNnSc2
System	Syst	k1gInSc7
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
ESO	eso	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Datum	datum	k1gNnSc1
přístupu	přístup	k1gInSc2
15	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HARTKOPF	HARTKOPF	kA
<g/>
,	,	kIx,
W.	W.	kA
<g/>
;	;	kIx,
MASON	mason	k1gMnSc1
<g/>
,	,	kIx,
D.	D.	kA
M.	M.	kA
Sixth	Sixth	k1gMnSc1
Catalog	Catalog	k1gMnSc1
of	of	k?
Orbits	Orbits	k1gInSc1
of	of	k?
Visual	Visual	k1gInSc1
Binaries	Binaries	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Naval	navalit	k5eAaPmRp2nS
Observatory	Observator	k1gMnPc7
<g/>
,	,	kIx,
2008	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Reipurth	Reipurth	k1gInSc1
<g/>
,	,	kIx,
B.	B.	kA
<g/>
,	,	kIx,
Mikkola	Mikkola	k1gFnSc1
<g/>
,	,	kIx,
S.	S.	kA
"	"	kIx"
<g/>
Formation	Formation	k1gInSc1
of	of	k?
the	the	k?
Widest	Widest	k1gMnSc1
Binaries	Binaries	k1gMnSc1
from	from	k1gMnSc1
Dynamical	Dynamical	k1gMnSc1
Unfolding	Unfolding	k1gInSc1
of	of	k?
Triple	tripl	k1gInSc5
Systems	Systemsa	k1gFnPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aohoku	Aohok	k1gInSc2
Place	plac	k1gInSc6
<g/>
,	,	kIx,
HI	hi	k0
<g/>
:	:	kIx,
Institute	institut	k1gInSc5
for	forum	k1gNnPc2
Astronomy	astronom	k1gMnPc4
<g/>
,	,	kIx,
Univ	Univ	k1gInSc1
<g/>
.	.	kIx.
of	of	k?
Hawaii	Hawaie	k1gFnSc4
at	at	k?
Manoa	Manoa	k1gFnSc1
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
PDF	PDF	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Datum	datum	k1gNnSc1
přístupu	přístup	k1gInSc2
15	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
CHANG	CHANG	kA
<g/>
,	,	kIx,
Kenneth	Kenneth	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
One	One	k1gFnSc1
Star	star	k1gFnSc2
Over	Overa	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
Planet	planeta	k1gFnPc2
That	That	k1gMnSc1
Might	Might	k1gMnSc1
Be	Be	k1gMnSc1
Another	Anothra	k1gFnPc2
Earth	Earth	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
New	New	k1gFnSc2
York	York	k1gInSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Kunitzsch	Kunitzsch	k1gInSc1
P.	P.	kA
<g/>
,	,	kIx,
&	&	k?
Smart	Smart	k1gInSc1
<g/>
,	,	kIx,
T.	T.	kA
<g/>
,	,	kIx,
A	A	kA
Dictionary	Dictionara	k1gFnPc1
of	of	k?
Modern	Modern	k1gMnSc1
star	star	k1gFnSc2
Names	Names	k1gMnSc1
<g/>
:	:	kIx,
A	a	k9
Short	Short	k1gInSc1
Guide	Guid	k1gInSc5
to	ten	k3xDgNnSc1
254	#num#	k4
Star	Star	kA
Names	Names	k1gMnSc1
and	and	k?
Their	Their	k1gInSc1
Derivations	Derivations	k1gInSc1
<g/>
,	,	kIx,
Cambridge	Cambridge	k1gFnSc1
<g/>
,	,	kIx,
Sky	Sky	k1gFnSc1
Pub	Pub	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Corp	Corp	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
27	#num#	k4
<g/>
↑	↑	k?
Davis	Davis	k1gInSc1
Jr	Jr	k1gFnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
G.	G.	kA
A.	A.	kA
<g/>
,	,	kIx,
"	"	kIx"
<g/>
The	The	k1gFnSc1
Pronunciations	Pronunciations	k1gInSc1
<g/>
,	,	kIx,
Derivations	Derivations	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Meanings	Meanings	k1gInSc1
of	of	k?
a	a	k8xC
Selected	Selected	k1gInSc1
List	list	k1gInSc1
of	of	k?
Star	Star	kA
Names	Names	k1gInSc1
<g/>
,	,	kIx,
<g/>
"	"	kIx"
<g/>
Popular	Popular	k1gInSc1
Astronomy	astronom	k1gMnPc4
<g/>
,	,	kIx,
Ročník	ročník	k1gInSc1
LII	LII	kA
<g/>
,	,	kIx,
Č.	Č.	kA
3	#num#	k4
<g/>
,	,	kIx,
říjen	říjen	k1gInSc1
1944	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
16	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
INNES	INNES	kA
<g/>
,	,	kIx,
R.	R.	kA
T.	T.	kA
A.	A.	kA
A	A	kA
Faint	Fainto	k1gNnPc2
Star	Star	kA
of	of	k?
Large	Large	k1gInSc1
Proper	proprat	k5eAaPmRp2nS
Motion	Motion	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Circular	Circular	k1gMnSc1
of	of	k?
the	the	k?
Union	union	k1gInSc1
Observatory	Observator	k1gMnPc4
Johannesburg	Johannesburg	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Říjen	říjen	k1gInSc1
1915	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
30	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
235	#num#	k4
<g/>
–	–	k?
<g/>
236	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
1915	#num#	k4
<g/>
CiUO	CiUO	k1gFnPc2
<g/>
..	..	k?
<g/>
.30	.30	k4
<g/>
.	.	kIx.
<g/>
.235	.235	k4
<g/>
I.	I.	kA
↑	↑	k?
ALDEN	ALDEN	kA
<g/>
,	,	kIx,
Harold	Harold	k1gMnSc1
L.	L.	kA
Alpha	Alpha	k1gMnSc1
and	and	k?
Proxima	Proxim	k1gMnSc4
Centauri	Centaur	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Astronomical	Astronomical	k1gMnSc1
Journal	Journal	k1gMnSc1
<g/>
.	.	kIx.
1928	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
39	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
913	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
20	#num#	k4
<g/>
–	–	k?
<g/>
23	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.108	10.108	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
104871	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
1928	#num#	k4
<g/>
AJ	aj	kA
<g/>
....	....	k?
<g/>
.39	.39	k4
<g/>
..	..	k?
<g/>
.20	.20	k4
<g/>
A.	A.	kA
↑	↑	k?
INNES	INNES	kA
<g/>
,	,	kIx,
R.	R.	kA
T.	T.	kA
A.	A.	kA
Parallax	Parallax	k1gInSc1
of	of	k?
the	the	k?
Faint	Faint	k1gInSc1
Proper	proprat	k5eAaPmRp2nS
Motion	Motion	k1gInSc1
Star	Star	kA
Near	Near	k1gInSc1
Alpha	Alpha	k1gFnSc1
of	of	k?
Centaurus	Centaurus	k1gInSc1
<g/>
.	.	kIx.
1900	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
R.	R.	kA
<g/>
A.	A.	kA
14	#num#	k4
h	h	k?
22	#num#	k4
<g/>
m	m	kA
55	#num#	k4
<g/>
s.	s.	k?
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
s	s	k7c7
6	#num#	k4
<g/>
t.	t.	k?
Dec-	Dec-	k1gMnSc1
<g/>
62	#num#	k4
<g/>
°	°	k?
15	#num#	k4
<g/>
'	'	kIx"
<g/>
2	#num#	k4
0	#num#	k4
<g/>
'	'	kIx"
<g/>
8	#num#	k4
t.	t.	k?
Circular	Circular	k1gMnSc1
of	of	k?
the	the	k?
Union	union	k1gInSc1
Observatory	Observator	k1gMnPc4
Johannesburg	Johannesburg	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Září	září	k1gNnSc1
1917	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
40	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
331	#num#	k4
<g/>
–	–	k?
<g/>
336	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bibcode	Bibcod	k1gInSc5
<g/>
:	:	kIx,
<g/>
1917	#num#	k4
<g/>
CiUO	CiUO	k1gFnPc2
<g/>
..	..	k?
<g/>
.40	.40	k4
<g/>
.	.	kIx.
<g/>
.331	.331	k4
<g/>
I.	I.	kA
↑	↑	k?
In	In	k1gMnSc1
<g/>
:	:	kIx,
STEVENSON	STEVENSON	kA
<g/>
,	,	kIx,
Angus	Angus	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
OUP	OUP	kA
Oxford	Oxford	k1gInSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0	#num#	k4
<g/>
199571120	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
1431	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
IAU	IAU	kA
Working	Working	k1gInSc1
Group	Group	k1gInSc1
on	on	k3xPp3gMnSc1
Star	Star	kA
Names	Names	k1gMnSc1
(	(	kIx(
<g/>
WGSN	WGSN	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
International	International	k1gFnSc1
Astronomical	Astronomical	k1gFnSc1
Union	union	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Bulletin	bulletin	k1gInSc1
of	of	k?
the	the	k?
IAU	IAU	kA
Working	Working	k1gInSc1
Group	Group	k1gInSc1
on	on	k3xPp3gMnSc1
Star	Star	kA
Names	Namesa	k1gFnPc2
<g/>
,	,	kIx,
No	no	k9
<g/>
.	.	kIx.
2	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
kar	kar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alfa	alfa	k1gFnSc1
Centauri	Centaur	k1gFnSc2
už	už	k6eAd1
neexistuje	existovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Astronomická	astronomický	k2eAgFnSc1d1
unie	unie	k1gFnSc1
ji	on	k3xPp3gFnSc4
přejmenovala	přejmenovat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT	ČT	kA
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-12-01	2016-12-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
https://www.stoplusjednicka.cz/potvrzeno-proxima-centauri-skutecne-obiha-sve-dva-hvezdne-sousedy	https://www.stoplusjednicka.cz/potvrzeno-proxima-centauri-skutecne-obiha-sve-dva-hvezdne-souseda	k1gMnSc2
<g/>
↑	↑	k?
Vědci	vědec	k1gMnPc1
objevili	objevit	k5eAaPmAgMnP
planetu	planeta	k1gFnSc4
u	u	k7c2
Alpha	Alph	k1gMnSc2
Centauri	Centaur	k1gFnSc2
<g/>
,	,	kIx,
nejbližšího	blízký	k2eAgInSc2d3
hvězdného	hvězdný	k2eAgInSc2d1
systému	systém	k1gInSc2
od	od	k7c2
Slunce	slunce	k1gNnSc2
<g/>
↑	↑	k?
Astronomové	astronom	k1gMnPc1
žádají	žádat	k5eAaImIp3nP
veřejnost	veřejnost	k1gFnSc4
o	o	k7c4
penízky	penízek	k1gInPc4
na	na	k7c4
hledání	hledání	k1gNnSc4
planety	planeta	k1gFnSc2
u	u	k7c2
Alfa	alfa	k1gNnSc2
Centauri	Centaur	k1gFnSc2
<g/>
↑	↑	k?
SILVERBERG	SILVERBERG	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muž	muž	k1gMnSc1
v	v	k7c6
labyrintu	labyrint	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
AFSF	AFSF	kA
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85390	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
první	první	k4xOgFnSc1
<g/>
,	,	kIx,
s.	s.	k?
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
nejbližších	blízký	k2eAgFnPc2d3
hvězd	hvězda	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
nejbližších	blízký	k2eAgFnPc2d3
jasných	jasný	k2eAgFnPc2d1
hvězd	hvězda	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
nejjasnějších	jasný	k2eAgFnPc2d3
hvězd	hvězda	k1gFnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Alfa	alfa	k1gNnSc2
Centauri	Centaur	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
Alfa	alfa	k1gFnSc1
Centauri	Centaur	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c4
Alfa	alfa	k1gNnSc4
Centauri	Centaur	k1gFnSc2
</s>
<s>
Obrázek	obrázek	k1gInSc1
NASA	NASA	kA
Alfa	alfa	k1gFnSc1
Centauri	Centaur	k1gFnSc2
</s>
<s>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-vector	skin-vector	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-vesmir	souradnice-vesmir	k1gInSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gMnSc1
span	span	k1gMnSc1
<g/>
.	.	kIx.
<g/>
souradnice-vesmir	souradnice-vesmir	k1gMnSc1
<g/>
{	{	kIx(
<g/>
background	background	k1gMnSc1
<g/>
:	:	kIx,
<g/>
url	url	k?
<g/>
(	(	kIx(
<g/>
"	"	kIx"
<g/>
//	//	k?
<g/>
upload	upload	k1gInSc1
<g/>
.	.	kIx.
<g/>
wikimedia	wikimedium	k1gNnSc2
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
org	org	k?
<g/>
/	/	kIx~
<g/>
wikipedia	wikipedium	k1gNnSc2
<g/>
/	/	kIx~
<g/>
commons	commons	k6eAd1
<g/>
/	/	kIx~
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
<g/>
/	/	kIx~
<g/>
Skymap	Skymap	k1gInSc1
<g/>
.	.	kIx.
<g/>
png	png	k?
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
right	right	k2eAgInSc1d1
no-repeat	no-repeat	k1gInSc1
<g/>
;	;	kIx,
<g/>
padding-right	padding-right	k1gInSc1
<g/>
:	:	kIx,
<g/>
28	#num#	k4
<g/>
px	px	k?
<g/>
}	}	kIx)
<g/>
body	bod	k1gInPc4
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
skin-vector	skin-vector	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-blok	souradnice-blok	k1gInSc1
sup	sup	k1gMnSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-vector	skin-vector	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-inline	souradnice-inlin	k1gInSc5
sup	sup	k1gMnSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-blok	souradnice-blok	k1gInSc1
sup	sup	k1gMnSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-inline	souradnice-inlin	k1gInSc5
sup	sup	k1gMnSc1
<g/>
{	{	kIx(
<g/>
vertical-align	vertical-align	k1gNnSc1
<g/>
:	:	kIx,
<g/>
top	topit	k5eAaImRp2nS
<g/>
}	}	kIx)
<g/>
body	bod	k1gInPc4
<g/>
.	.	kIx.
<g/>
skin-vector	skin-vector	k1gMnSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gMnSc1
span	span	k1gMnSc1
<g/>
.	.	kIx.
<g/>
souradnice-blok	souradnice-blok	k1gInSc1
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
souradnice-blok	souradnice-blok	k1gInSc1
<g/>
{	{	kIx(
<g/>
font-family	font-famit	k5eAaPmAgFnP
<g/>
:	:	kIx,
<g/>
monospace	monospace	k1gFnSc1
<g/>
;	;	kIx,
<g/>
font-size	font-size	k1gFnSc1
<g/>
:	:	kIx,
<g/>
85	#num#	k4
<g/>
%	%	kIx~
<g/>
}	}	kIx)
<g/>
body	bod	k1gInPc4
<g/>
.	.	kIx.
<g/>
skin-vector	skin-vector	k1gMnSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gMnSc1
span	span	k1gMnSc1
<g/>
.	.	kIx.
<g/>
souradnice-inline	souradnice-inlin	k1gInSc5
<g/>
,	,	kIx,
<g/>
body	bod	k1gInPc1
<g/>
.	.	kIx.
<g/>
skin-monobook	skin-monobook	k1gInSc1
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
span	span	k1gInSc1
<g/>
.	.	kIx.
<g/>
souradnice-inline	souradnice-inlin	k1gInSc5
<g/>
{	{	kIx(
<g/>
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
none	none	k1gInSc1
<g/>
;	;	kIx,
<g/>
padding-right	padding-right	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Hvězdy	hvězda	k1gFnPc1
souhvězdí	souhvězdí	k1gNnSc2
Kentaura	kentaur	k1gMnSc2
</s>
<s>
π	π	k?
•	•	k?
ο	ο	k?
<g/>
1	#num#	k4
•	•	k?
ο	ο	k?
<g/>
2	#num#	k4
•	•	k?
A	a	k8xC
•	•	k?
C1	C1	k1gMnSc1
•	•	k?
C2	C2	k1gMnSc1
•	•	k?
λ	λ	k?
•	•	k?
C3	C3	k1gFnSc2
•	•	k?
j	j	k?
•	•	k?
B	B	kA
•	•	k?
E	E	kA
•	•	k?
δ	δ	k?
•	•	k?
ρ	ρ	k?
•	•	k?
D	D	kA
•	•	k?
F	F	kA
•	•	k?
x	x	k?
<g/>
1	#num#	k4
•	•	k?
x	x	k?
<g/>
2	#num#	k4
•	•	k?
G	G	kA
•	•	k?
σ	σ	k?
•	•	k?
u	u	k7c2
•	•	k?
τ	τ	k?
•	•	k?
l	l	kA
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
γ	γ	k?
•	•	k?
w	w	k?
•	•	k?
p	p	k?
•	•	k?
e	e	k0
•	•	k?
n	n	k0
•	•	k?
H	H	kA
•	•	k?
ξ	ξ	k?
<g/>
1	#num#	k4
•	•	k?
f	f	k?
•	•	k?
ξ	ξ	k?
<g/>
2	#num#	k4
•	•	k?
r	r	kA
•	•	k?
ι	ι	k?
•	•	k?
J	J	kA
•	•	k?
m	m	kA
•	•	k?
K	k	k7c3
•	•	k?
d	d	k?
•	•	k?
ε	ε	k?
•	•	k?
Q	Q	kA
•	•	k?
1	#num#	k4
(	(	kIx(
<g/>
i	i	k8xC
<g/>
)	)	kIx)
•	•	k?
M	M	kA
•	•	k?
z	z	k7c2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
ν	ν	k?
•	•	k?
2	#num#	k4
(	(	kIx(
<g/>
g	g	kA
<g/>
)	)	kIx)
•	•	k?
μ	μ	k?
•	•	k?
N	N	kA
•	•	k?
3	#num#	k4
(	(	kIx(
<g/>
k	k	k7c3
<g/>
)	)	kIx)
•	•	k?
4	#num#	k4
(	(	kIx(
<g/>
h	h	k?
<g/>
)	)	kIx)
•	•	k?
y	y	k?
•	•	k?
ζ	ζ	k?
•	•	k?
φ	φ	k?
•	•	k?
υ	υ	k?
<g/>
1	#num#	k4
•	•	k?
υ	υ	k?
<g/>
2	#num#	k4
•	•	k?
β	β	k?
(	(	kIx(
<g/>
Hadar	Hadar	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Agena	Agena	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
χ	χ	k?
•	•	k?
θ	θ	k?
(	(	kIx(
<g/>
Menkent	Menkent	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
v	v	k7c6
•	•	k?
ψ	ψ	k?
•	•	k?
a	a	k8xC
•	•	k?
η	η	k?
•	•	k?
α	α	k?
(	(	kIx(
<g/>
Rigil	Rigil	k1gMnSc1
Kentaurus	Kentaurus	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
b	b	k?
•	•	k?
c	c	k0
<g/>
1	#num#	k4
•	•	k?
c	c	k0
<g/>
2	#num#	k4
•	•	k?
κ	κ	k?
Seznam	seznam	k1gInSc1
hvězd	hvězda	k1gFnPc2
v	v	k7c6
souhvězdí	souhvězdí	k1gNnSc6
Kentaura	kentaur	k1gMnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Astronomie	astronomie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
7544148-2	7544148-2	k4
</s>
