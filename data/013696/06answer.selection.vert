<s hack="1">
(	(	kIx(
<g/>
též	též	k9
Proxima	Proxima	k1gFnSc1
Centauri	Centauri	k1gFnSc1
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgInSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
gravitačně	gravitačně	k6eAd1
spojen	spojit	k5eAaPmNgInS
se	s	k7c7
složkami	složka	k1gFnPc7
A	A	kA
a	a	k8xC
B.	B.	kA
Lidskému	lidský	k2eAgNnSc3d1
oku	oko	k1gNnSc3
se	se	k3xPyFc4
dvě	dva	k4xCgFnPc1
hlavní	hlavní	k2eAgFnPc1d1
hvězdy	hvězda	k1gFnPc1
zdají	zdát	k5eAaImIp3nP
jako	jako	k8xS
jeden	jeden	k4xCgInSc1
bod	bod	k1gInSc1
se	s	k7c7
zdánlivou	zdánlivý	k2eAgFnSc7d1
magnitudou	magnituda	k1gFnSc7
−	−	k?
<g/>
,	,	kIx,
takže	takže	k8xS
tvoří	tvořit	k5eAaImIp3nP
nejjasnější	jasný	k2eAgFnSc4d3
hvězdu	hvězda	k1gFnSc4
v	v	k7c6
souhvězdí	souhvězdí	k1gNnSc6
Kentaura	Kentaur	k1gMnSc2
a	a	k8xC
po	po	k7c6
Siriu	Sirius	k1gNnSc6
a	a	k8xC
Canopu	Canop	k1gNnSc6
třetí	třetí	k4xOgFnSc4
nejjasnější	jasný	k2eAgFnSc4d3
hvězdu	hvězda	k1gFnSc4
noční	noční	k2eAgFnSc2d1
oblohy	obloha	k1gFnSc2
<g/>
.	.	kIx.
</s>