<s>
Kolik	kolika	k1gFnPc2	kolika
existuje	existovat	k5eAaImIp3nS	existovat
u	u	k7c2	u
hub	houba	k1gFnPc2	houba
typů	typ	k1gInPc2	typ
haploidních	haploidní	k2eAgNnPc2d1	haploidní
mycelií	mycelium	k1gNnPc2	mycelium
(	(	kIx(	(
<g/>
gametofytů	gametofyt	k1gInPc2	gametofyt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
spolu	spolu	k6eAd1	spolu
splývají	splývat	k5eAaImIp3nP	splývat
a	a	k8xC	a
vyměňují	vyměňovat	k5eAaImIp3nP	vyměňovat
si	se	k3xPyFc3	se
jádra	jádro	k1gNnPc1	jádro
<g/>
?	?	kIx.	?
</s>
