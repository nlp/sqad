<s>
Rodozměna	rodozměna	k1gFnSc1	rodozměna
(	(	kIx(	(
<g/>
metageneze	metageneze	k1gFnSc1	metageneze
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
životní	životní	k2eAgInSc1d1	životní
cyklus	cyklus	k1gInSc1	cyklus
určitých	určitý	k2eAgFnPc2d1	určitá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
hub	houba	k1gFnPc2	houba
a	a	k8xC	a
jednobuněčných	jednobuněčný	k2eAgMnPc2d1	jednobuněčný
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
vlastně	vlastně	k9	vlastně
o	o	k7c6	o
střídání	střídání	k1gNnSc6	střídání
dvou	dva	k4xCgFnPc2	dva
fází	fáze	k1gFnPc2	fáze
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jednoho	jeden	k4xCgInSc2	jeden
životního	životní	k2eAgInSc2d1	životní
cyklu	cyklus	k1gInSc2	cyklus
(	(	kIx(	(
<g/>
generace	generace	k1gFnSc1	generace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
fáze	fáze	k1gFnPc1	fáze
jsou	být	k5eAaImIp3nP	být
gametofyt	gametofyt	k1gInSc4	gametofyt
a	a	k8xC	a
sporofyt	sporofyt	k1gInSc4	sporofyt
<g/>
.	.	kIx.	.
</s>
<s>
Gametofyt	gametofyt	k1gInSc1	gametofyt
je	být	k5eAaImIp3nS	být
haploidní	haploidní	k2eAgInSc1d1	haploidní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
sadu	sada	k1gFnSc4	sada
chromozomů	chromozom	k1gInPc2	chromozom
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
<g/>
,	,	kIx,	,
a	a	k8xC	a
sporofyt	sporofyt	k1gInSc1	sporofyt
je	být	k5eAaImIp3nS	být
diploidní	diploidní	k2eAgInSc1d1	diploidní
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
termíny	termín	k1gInPc4	termín
rodozměna	rodozměna	k1gFnSc1	rodozměna
a	a	k8xC	a
metageneze	metageneze	k1gFnSc1	metageneze
panuje	panovat	k5eAaImIp3nS	panovat
nejasný	jasný	k2eNgInSc1d1	nejasný
vztah	vztah	k1gInSc1	vztah
a	a	k8xC	a
zatímco	zatímco	k8xS	zatímco
někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
synonyma	synonymum	k1gNnPc4	synonymum
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
je	být	k5eAaImIp3nS	být
metageneze	metageneze	k1gFnSc1	metageneze
chápána	chápán	k2eAgFnSc1d1	chápána
jako	jako	k8xC	jako
speciální	speciální	k2eAgInSc1d1	speciální
případ	případ	k1gInSc1	případ
rodozměny	rodozměna	k1gFnSc2	rodozměna
některých	některý	k3yIgMnPc2	některý
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
rodozměnu	rodozměna	k1gFnSc4	rodozměna
izomorfickou	izomorfický	k2eAgFnSc4d1	izomorfická
<g/>
,	,	kIx,	,
u	u	k7c2	u
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
gametofyt	gametofyt	k1gInSc1	gametofyt
a	a	k8xC	a
sporofyt	sporofyt	k1gInSc1	sporofyt
navenek	navenek	k6eAd1	navenek
podobají	podobat	k5eAaImIp3nP	podobat
<g/>
,	,	kIx,	,
a	a	k8xC	a
rodozměnu	rodozměna	k1gFnSc4	rodozměna
heteromorfickou	heteromorfický	k2eAgFnSc4d1	heteromorfická
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
jsou	být	k5eAaImIp3nP	být
obě	dva	k4xCgFnPc1	dva
fáze	fáze	k1gFnPc1	fáze
snadno	snadno	k6eAd1	snadno
rozlišitelné	rozlišitelný	k2eAgFnPc1d1	rozlišitelná
<g/>
.	.	kIx.	.
</s>
<s>
Haploidní	haploidní	k2eAgFnSc1d1	haploidní
rostlina	rostlina	k1gFnSc1	rostlina
(	(	kIx(	(
<g/>
gametofyt	gametofyt	k1gInSc1	gametofyt
<g/>
)	)	kIx)	)
produkuje	produkovat	k5eAaImIp3nS	produkovat
gamety	gamet	k1gInPc4	gamet
prostou	prostý	k2eAgFnSc7d1	prostá
mitózou	mitóza	k1gFnSc7	mitóza
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
takové	takový	k3xDgFnPc1	takový
gamety	gameta	k1gFnPc1	gameta
(	(	kIx(	(
<g/>
z	z	k7c2	z
dvou	dva	k4xCgMnPc2	dva
jedinců	jedinec	k1gMnPc2	jedinec
stejného	stejný	k2eAgInSc2d1	stejný
druhu	druh	k1gInSc2	druh
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
obě	dva	k4xCgFnPc1	dva
z	z	k7c2	z
jednoho	jeden	k4xCgMnSc2	jeden
jedince	jedinec	k1gMnSc2	jedinec
<g/>
)	)	kIx)	)
splývají	splývat	k5eAaImIp3nP	splývat
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vzniká	vznikat	k5eAaImIp3nS	vznikat
diploidní	diploidní	k2eAgFnSc1d1	diploidní
zygota	zygota	k1gFnSc1	zygota
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
diploidní	diploidní	k2eAgNnSc1d1	diploidní
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
mnohobuněčný	mnohobuněčný	k2eAgInSc1d1	mnohobuněčný
<g/>
)	)	kIx)	)
organismus	organismus	k1gInSc1	organismus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
sporofyt	sporofyt	k1gInSc4	sporofyt
<g/>
.	.	kIx.	.
</s>
<s>
Sporofyt	sporofyt	k1gInSc1	sporofyt
meioticky	meioticky	k6eAd1	meioticky
produkuje	produkovat	k5eAaImIp3nS	produkovat
haploidní	haploidní	k2eAgInPc4d1	haploidní
spory	spor	k1gInPc4	spor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nijak	nijak	k6eAd1	nijak
nesplývají	splývat	k5eNaImIp3nP	splývat
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
pohlavních	pohlavní	k2eAgFnPc2d1	pohlavní
buněk	buňka	k1gFnPc2	buňka
gametofytu	gametofyt	k1gInSc2	gametofyt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
začnou	začít	k5eAaPmIp3nP	začít
klíčit	klíčit	k5eAaImF	klíčit
a	a	k8xC	a
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
gametofyt	gametofyt	k1gInSc4	gametofyt
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
cyklus	cyklus	k1gInSc1	cyklus
uzavírá	uzavírat	k5eAaImIp3nS	uzavírat
<g/>
.	.	kIx.	.
</s>
<s>
Teoreticky	teoreticky	k6eAd1	teoreticky
u	u	k7c2	u
všech	všecek	k3xTgInPc2	všecek
pohlavně	pohlavně	k6eAd1	pohlavně
se	se	k3xPyFc4	se
rozmnožujících	rozmnožující	k2eAgInPc2d1	rozmnožující
organismů	organismus	k1gInPc2	organismus
bychom	by	kYmCp1nP	by
mohli	moct	k5eAaImAgMnP	moct
najít	najít	k5eAaPmF	najít
gametofyt	gametofyt	k1gInSc4	gametofyt
(	(	kIx(	(
<g/>
pohlavní	pohlavní	k2eAgFnPc1d1	pohlavní
buňky	buňka	k1gFnPc1	buňka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vědců	vědec	k1gMnPc2	vědec
se	se	k3xPyFc4	se
však	však	k9	však
shoduje	shodovat	k5eAaImIp3nS	shodovat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
rodozměnu	rodozměna	k1gFnSc4	rodozměna
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
považovat	považovat	k5eAaImF	považovat
jen	jen	k9	jen
případy	případ	k1gInPc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsou	být	k5eAaImIp3nP	být
gametofyt	gametofyt	k1gInSc4	gametofyt
i	i	k8xC	i
sporofyt	sporofyt	k1gInSc4	sporofyt
mnohobuněčné	mnohobuněčný	k2eAgFnPc1d1	mnohobuněčná
<g/>
..	..	k?	..
</s>
<s>
U	u	k7c2	u
hub	houba	k1gFnPc2	houba
existují	existovat	k5eAaImIp3nP	existovat
dva	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
haploidních	haploidní	k2eAgNnPc2d1	haploidní
mycelií	mycelium	k1gNnPc2	mycelium
(	(	kIx(	(
<g/>
gametofytů	gametofyt	k1gInPc2	gametofyt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
spolu	spolu	k6eAd1	spolu
splývají	splývat	k5eAaImIp3nP	splývat
a	a	k8xC	a
vyměňují	vyměňovat	k5eAaImIp3nP	vyměňovat
si	se	k3xPyFc3	se
jádra	jádro	k1gNnPc1	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
plazmogamie	plazmogamie	k1gFnSc2	plazmogamie
a	a	k8xC	a
spojení	spojení	k1gNnSc2	spojení
dvou	dva	k4xCgNnPc2	dva
jader	jádro	k1gNnPc2	jádro
do	do	k7c2	do
výsledné	výsledný	k2eAgFnSc2d1	výsledná
diploidní	diploidní	k2eAgFnSc2d1	diploidní
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
karyogamie	karyogamie	k1gFnSc1	karyogamie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
sporofyt	sporofyt	k1gInSc1	sporofyt
však	však	k9	však
nemá	mít	k5eNaImIp3nS	mít
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
trvání	trvání	k1gNnSc2	trvání
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
vzniká	vznikat	k5eAaImIp3nS	vznikat
meiózou	meióza	k1gFnSc7	meióza
haploidní	haploidní	k2eAgInSc4d1	haploidní
gametofyt	gametofyt	k1gInSc4	gametofyt
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
jednobuněční	jednobuněční	k2eAgMnPc1d1	jednobuněční
prvoci	prvok	k1gMnPc1	prvok
prochází	procházet	k5eAaImIp3nP	procházet
rodozměnou	rodozměna	k1gFnSc7	rodozměna
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
hlenek	hlenka	k1gFnPc2	hlenka
<g/>
,	,	kIx,	,
dírkonošců	dírkonošce	k1gMnPc2	dírkonošce
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
mořských	mořský	k2eAgFnPc2d1	mořská
řas	řasa	k1gFnPc2	řasa
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Mechorosty	mechorost	k1gInPc1	mechorost
<g/>
,	,	kIx,	,
Kapraďorosty	kapraďorost	k1gInPc1	kapraďorost
a	a	k8xC	a
Přesličky	přeslička	k1gFnPc1	přeslička
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
je	být	k5eAaImIp3nS	být
rodozměna	rodozměna	k1gFnSc1	rodozměna
výrazná	výrazný	k2eAgFnSc1d1	výrazná
u	u	k7c2	u
mechorostů	mechorost	k1gInPc2	mechorost
a	a	k8xC	a
kapraďorostů	kapraďorost	k1gInPc2	kapraďorost
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
semenných	semenný	k2eAgFnPc2d1	semenná
rostlin	rostlina	k1gFnPc2	rostlina
je	být	k5eAaImIp3nS	být
fáze	fáze	k1gFnSc1	fáze
gametofytu	gametofyt	k1gInSc2	gametofyt
silně	silně	k6eAd1	silně
potlačena	potlačen	k2eAgFnSc1d1	potlačena
<g/>
,	,	kIx,	,
gametofyt	gametofyt	k1gInSc1	gametofyt
semenných	semenný	k2eAgFnPc2d1	semenná
rostlin	rostlina	k1gFnPc2	rostlina
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
mikrogametofyt	mikrogametofyt	k1gInSc1	mikrogametofyt
(	(	kIx(	(
<g/>
mikroprothalium	mikroprothalium	k1gNnSc1	mikroprothalium
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
samčí	samčí	k2eAgFnPc4d1	samčí
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
,	,	kIx,	,
megagametofyt	megagametofyt	k1gInSc1	megagametofyt
(	(	kIx(	(
<g/>
megaprothalium	megaprothalium	k1gNnSc1	megaprothalium
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
samičí	samičí	k2eAgNnSc4d1	samičí
<g/>
.	.	kIx.	.
</s>
<s>
Mikrogametofyt	Mikrogametofyt	k1gInSc1	Mikrogametofyt
představuje	představovat	k5eAaImIp3nS	představovat
pylové	pylový	k2eAgNnSc4d1	pylové
zrno	zrno	k1gNnSc4	zrno
(	(	kIx(	(
<g/>
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
mikrospoře	mikrospora	k1gFnSc3	mikrospora
-	-	kIx~	-
samčímu	samčí	k2eAgInSc3d1	samčí
výtrusu	výtrus	k1gInSc3	výtrus
<g/>
)	)	kIx)	)
a	a	k8xC	a
pylová	pylový	k2eAgFnSc1d1	pylová
láčka	láčka	k1gFnSc1	láčka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
prorůstá	prorůstat	k5eAaImIp3nS	prorůstat
vajíčkem	vajíčko	k1gNnSc7	vajíčko
(	(	kIx(	(
<g/>
u	u	k7c2	u
krytosemenných	krytosemenný	k2eAgFnPc2d1	krytosemenná
r.	r.	kA	r.
napřed	napřed	k6eAd1	napřed
pestíkem	pestík	k1gInSc7	pestík
<g/>
)	)	kIx)	)
k	k	k7c3	k
zárodečnému	zárodečný	k2eAgInSc3d1	zárodečný
vaku	vak	k1gInSc3	vak
(	(	kIx(	(
<g/>
megagametofytu	megagametofyt	k1gInSc2	megagametofyt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zárodečný	zárodečný	k2eAgInSc1d1	zárodečný
vak	vak	k1gInSc1	vak
vzniká	vznikat	k5eAaImIp3nS	vznikat
trojnásobným	trojnásobný	k2eAgNnSc7d1	trojnásobné
mitotickým	mitotický	k2eAgNnSc7d1	mitotické
dělením	dělení	k1gNnSc7	dělení
megaspory	megaspora	k1gFnSc2	megaspora
<g/>
.	.	kIx.	.
</s>
