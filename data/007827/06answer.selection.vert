<s>
Motýli	motýl	k1gMnPc1	motýl
vidí	vidět	k5eAaImIp3nS	vidět
ultrafialové	ultrafialový	k2eAgNnSc4d1	ultrafialové
světlo	světlo	k1gNnSc4	světlo
s	s	k7c7	s
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
délkou	délka	k1gFnSc7	délka
kratší	krátký	k2eAgFnSc7d2	kratší
než	než	k8xS	než
400	[number]	k4	400
nanometrů	nanometr	k1gInPc2	nanometr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nevidí	vidět	k5eNaImIp3nS	vidět
naopak	naopak	k6eAd1	naopak
červenou	červený	k2eAgFnSc4d1	červená
<g/>
.	.	kIx.	.
</s>
