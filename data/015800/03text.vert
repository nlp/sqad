<s>
Milešovka	Milešovka	k1gFnSc1
</s>
<s>
Milešovka	Milešovka	k1gFnSc1
Milešovka	Milešovka	k1gFnSc1
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
837	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Prominence	prominence	k1gFnSc2
</s>
<s>
620	#num#	k4
m	m	kA
↓	↓	k?
sníženina	sníženina	k1gFnSc1
J	J	kA
od	od	k7c2
Počerad	Počerada	k1gFnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Izolace	izolace	k1gFnPc1
</s>
<s>
20	#num#	k4
km	km	kA
→	→	k?
Stropník	Stropník	k1gInSc1
<g/>
,	,	kIx,
Krušné	krušný	k2eAgFnPc1d1
hory	hora	k1gFnPc1
Seznamy	seznam	k1gInPc4
</s>
<s>
Nejprominentnější	prominentní	k2eAgFnPc1d3
hory	hora	k1gFnPc1
CZ	CZ	kA
#	#	kIx~
<g/>
6	#num#	k4
<g/>
Hory	hora	k1gFnSc2
Českého	český	k2eAgNnSc2d1
středohoří	středohoří	k1gNnSc2
#	#	kIx~
<g/>
1	#num#	k4
Poznámka	poznámka	k1gFnSc1
</s>
<s>
meteorologická	meteorologický	k2eAgFnSc1d1
observatoř	observatoř	k1gFnSc1
<g/>
,	,	kIx,
výhled	výhled	k1gInSc1
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc4
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
/	/	kIx~
Milešovské	Milešovský	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
/	/	kIx~
Kostomlatské	Kostomlatský	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
/	/	kIx~
Bořislavská	Bořislavský	k2eAgFnSc1d1
hornatina	hornatina	k1gFnSc1
/	/	kIx~
Milešovská	Milešovský	k2eAgFnSc1d1
část	část	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
33	#num#	k4
<g/>
′	′	k?
<g/>
18	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
°	°	k?
<g/>
55	#num#	k4
<g/>
′	′	k?
<g/>
54	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Milešovka	Milešovka	k1gFnSc1
</s>
<s>
Hornina	hornina	k1gFnSc1
</s>
<s>
znělec	znělec	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuNárodní	infoboxuNárodní	k2eAgFnSc3d1
přírodní	přírodní	k2eAgFnSc3d1
rezervaceMilešovka	rezervaceMilešovka	k1gFnSc1
suťové	suťový	k2eAgFnSc2d1
bučiny	bučina	k1gFnSc2
na	na	k7c6
severním	severní	k2eAgInSc6d1
svahuZákladní	svahuZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Vyhlášení	vyhlášení	k1gNnSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1951	#num#	k4
Vyhlásil	vyhlásit	k5eAaPmAgMnS
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1
školství	školství	k1gNnSc2
<g/>
,	,	kIx,
věd	věda	k1gFnPc2
a	a	k8xC
umění	umění	k1gNnSc1
Nadm	Nadma	k1gFnPc2
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
550	#num#	k4
<g/>
–	–	k?
<g/>
837	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Okres	okres	k1gInSc1
</s>
<s>
Litoměřice	Litoměřice	k1gInPc1
Umístění	umístění	k1gNnSc1
</s>
<s>
Velemín	Velemín	k1gInSc1
(	(	kIx(
<g/>
k.	k.	k?
ú.	ú.	k?
Milešov	Milešov	k1gInSc1
u	u	k7c2
Lovosic	Lovosice	k1gInPc2
<g/>
)	)	kIx)
Další	další	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Kód	kód	k1gInSc1
</s>
<s>
242	#num#	k4
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Milešovka	Milešovka	k1gFnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Milleschauer	Milleschauer	k1gMnSc1
<g/>
,	,	kIx,
Donnersberg	Donnersberg	k1gMnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc1d3
hora	hora	k1gFnSc1
Českého	český	k2eAgNnSc2d1
středohoří	středohoří	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
vrchol	vrchol	k1gInSc1
leží	ležet	k5eAaImIp3nS
ve	v	k7c6
výšce	výška	k1gFnSc6
836,7	836,7	k4
metrů	metr	k1gInPc2
nad	nad	k7c7
mořem	moře	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Jméno	jméno	k1gNnSc4
získala	získat	k5eAaPmAgFnS
podle	podle	k7c2
nedalekého	daleký	k2eNgInSc2d1
Milešova	Milešův	k2eAgInSc2d1
(	(	kIx(
<g/>
2	#num#	k4
km	km	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
zde	zde	k6eAd1
zřízena	zřídit	k5eAaPmNgFnS
národní	národní	k2eAgFnSc1d1
přírodní	přírodní	k2eAgFnSc1d1
rezervace	rezervace	k1gFnSc1
(	(	kIx(
<g/>
51,3	51,3	k4
ha	ha	kA
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1951	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
vrcholu	vrchol	k1gInSc2
hory	hora	k1gFnSc2
je	být	k5eAaImIp3nS
kruhový	kruhový	k2eAgInSc4d1
rozhled	rozhled	k1gInSc4
do	do	k7c2
dalekého	daleký	k2eAgNnSc2d1
okolí	okolí	k1gNnSc2
<g/>
,	,	kIx,
Milešovka	Milešovka	k1gFnSc1
totiž	totiž	k9
všechny	všechen	k3xTgInPc4
ostatní	ostatní	k2eAgInPc4d1
vrchy	vrch	k1gInPc4
v	v	k7c6
Českém	český	k2eAgNnSc6d1
středohoří	středohoří	k1gNnSc6
výrazně	výrazně	k6eAd1
převyšuje	převyšovat	k5eAaImIp3nS
a	a	k8xC
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
za	za	k7c4
největrnější	větrný	k2eAgFnSc4d3
horu	hora	k1gFnSc4
Česka	Česko	k1gNnSc2
<g/>
,	,	kIx,
bezvětří	bezvětří	k1gNnSc1
je	být	k5eAaImIp3nS
tu	tu	k6eAd1
jen	jen	k9
osm	osm	k4xCc4
dní	den	k1gInPc2
v	v	k7c6
roce	rok	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
opětnému	opětný	k2eAgNnSc3d1
vyhlášení	vyhlášení	k1gNnSc3
národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
ve	v	k7c6
vrcholové	vrcholový	k2eAgFnSc6d1
partii	partie	k1gFnSc6
Milešovky	Milešovka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgNnPc2d1
vyhlášení	vyhlášení	k1gNnPc2
odstranilo	odstranit	k5eAaPmAgNnS
rozdíly	rozdíl	k1gInPc4
mezi	mezi	k7c7
historickými	historický	k2eAgInPc7d1
údaji	údaj	k1gInPc7
a	a	k8xC
současnou	současný	k2eAgFnSc7d1
rozlohou	rozloha	k1gFnSc7
území	území	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
vyžaduje	vyžadovat	k5eAaImIp3nS
ochranu	ochrana	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Příroda	příroda	k1gFnSc1
</s>
<s>
Na	na	k7c6
příkrých	příkrý	k2eAgFnPc6d1
stráních	stráň	k1gFnPc6
roste	růst	k5eAaImIp3nS
především	především	k9
dub	dub	k1gInSc1
<g/>
,	,	kIx,
občas	občas	k6eAd1
buk	buk	k1gInSc1
<g/>
,	,	kIx,
javor	javor	k1gInSc1
nebo	nebo	k8xC
smrk	smrk	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
sutích	suť	k1gFnPc6
rozpadávající	rozpadávající	k2eAgFnSc4d1
se	se	k3xPyFc4
skály	skála	k1gFnPc1
lze	lze	k6eAd1
najít	najít	k5eAaPmF
celou	celý	k2eAgFnSc4d1
řadu	řada	k1gFnSc4
vzácných	vzácný	k2eAgFnPc2d1
chráněných	chráněný	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Významnou	významný	k2eAgFnSc7d1
lokalitou	lokalita	k1gFnSc7
jsou	být	k5eAaImIp3nP
Výří	výří	k2eAgFnPc1d1
skály	skála	k1gFnPc1
<g/>
,	,	kIx,
asi	asi	k9
30	#num#	k4
metrů	metr	k1gInPc2
vysoká	vysoký	k2eAgFnSc1d1
skalní	skalní	k2eAgFnSc1d1
stěna	stěna	k1gFnSc1
bez	bez	k7c2
lesnatého	lesnatý	k2eAgInSc2d1
porostu	porost	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvete	kvést	k5eAaImIp3nS
zde	zde	k6eAd1
lilie	lilie	k1gFnSc1
zlatohlavá	zlatohlavý	k2eAgFnSc1d1
<g/>
,	,	kIx,
kosatec	kosatec	k1gInSc1
bezlistý	bezlistý	k2eAgInSc1d1
<g/>
,	,	kIx,
plicník	plicník	k1gInSc1
lékařský	lékařský	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
se	se	k3xPyFc4
tu	tu	k6eAd1
vyskytuje	vyskytovat	k5eAaImIp3nS
kapradinka	kapradinka	k1gFnSc1
skalní	skalní	k2eAgFnSc1d1
a	a	k8xC
medvědice	medvědice	k1gFnSc1
lékařská	lékařský	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Podnebí	podnebí	k1gNnSc1
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1
roční	roční	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
za	za	k7c4
období	období	k1gNnSc4
1905	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
činí	činit	k5eAaImIp3nS
na	na	k7c6
Milešovce	Milešovka	k1gFnSc6
+5,2	+5,2	k4
°	°	k?
<g/>
C	C	kA
(	(	kIx(
<g/>
maximum	maximum	k1gNnSc4
+6,9	+6,9	k4
°	°	k?
<g/>
C	C	kA
v	v	k7c6
roce	rok	k1gInSc6
1934	#num#	k4
<g/>
;	;	kIx,
minimum	minimum	k1gNnSc4
+3,4	+3,4	k4
°	°	k?
<g/>
C	C	kA
v	v	k7c6
roce	rok	k1gInSc6
1940	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejchladnějším	chladný	k2eAgInSc7d3
měsícem	měsíc	k1gInSc7
je	být	k5eAaImIp3nS
leden	leden	k1gInSc1
s	s	k7c7
průměrnou	průměrný	k2eAgFnSc7d1
teplotou	teplota	k1gFnSc7
−	−	k?
°	°	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
C	C	kA
<g/>
,	,	kIx,
nejteplejším	teplý	k2eAgNnSc7d3
červenec	červenec	k1gInSc4
s	s	k7c7
průměrnou	průměrný	k2eAgFnSc7d1
teplotou	teplota	k1gFnSc7
+14,6	+14,6	k4
°	°	k?
<g/>
C.	C.	kA
Nejvyšší	vysoký	k2eAgFnSc1d3
teplota	teplota	k1gFnSc1
(	(	kIx(
<g/>
+	+	kIx~
<g/>
34,7	34,7	k4
°	°	k?
<g/>
C	C	kA
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
na	na	k7c6
Milešovce	Milešovka	k1gFnSc6
naměřena	naměřen	k2eAgFnSc1d1
5	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1957	#num#	k4
<g/>
,	,	kIx,
absolutně	absolutně	k6eAd1
nejnižší	nízký	k2eAgFnSc1d3
9	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1956	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
rtuť	rtuť	k1gFnSc1
teploměru	teploměr	k1gInSc2
na	na	k7c6
Milešovce	Milešovka	k1gFnSc6
propadla	propadlo	k1gNnSc2
k	k	k7c3
−	−	k?
°	°	k?
<g/>
C.	C.	kA
Nejteplejší	teplý	k2eAgInSc1d3
den	den	k1gInSc1
je	být	k5eAaImIp3nS
1	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc4
s	s	k7c7
průměrnou	průměrný	k2eAgFnSc7d1
teplotou	teplota	k1gFnSc7
+15,5	+15,5	k4
°	°	k?
<g/>
C.	C.	kA
Naopak	naopak	k6eAd1
nejchladnějšími	chladný	k2eAgInPc7d3
dny	den	k1gInPc7
s	s	k7c7
průměrnou	průměrný	k2eAgFnSc7d1
teplotou	teplota	k1gFnSc7
−	−	k?
°	°	k?
<g/>
C	C	kA
jsou	být	k5eAaImIp3nP
12	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
26	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1
roční	roční	k2eAgFnPc1d1
srážky	srážka	k1gFnPc1
za	za	k7c4
období	období	k1gNnSc4
1905	#num#	k4
<g/>
–	–	k?
<g/>
1994	#num#	k4
dosahují	dosahovat	k5eAaImIp3nP
na	na	k7c6
Milešovce	Milešovka	k1gFnSc6
557,0	557,0	k4
mm	mm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdeštivějším	deštivý	k2eAgInSc7d3
měsícem	měsíc	k1gInSc7
je	být	k5eAaImIp3nS
červenec	červenec	k1gInSc1
(	(	kIx(
<g/>
73,0	73,0	k4
mm	mm	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejsušším	suchý	k2eAgNnSc7d3
únor	únor	k1gInSc4
(	(	kIx(
<g/>
30,6	30,6	k4
mm	mm	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvlhčím	vlhký	k2eAgNnSc7d3
byl	být	k5eAaImAgInS
s	s	k7c7
hodnotou	hodnota	k1gFnSc7
815,4	815,4	k4
mm	mm	kA
rok	rok	k1gInSc4
1941	#num#	k4
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
nejsušším	suchý	k2eAgInSc6d3
(	(	kIx(
<g/>
348,3	348,3	k4
mm	mm	kA
<g/>
)	)	kIx)
rok	rok	k1gInSc4
1933	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdeštivějším	deštivý	k2eAgInSc7d3
měsícem	měsíc	k1gInSc7
v	v	k7c6
historii	historie	k1gFnSc6
měření	měření	k1gNnSc2
byl	být	k5eAaImAgInS
srpen	srpen	k1gInSc1
1970	#num#	k4
(	(	kIx(
<g/>
224,1	224,1	k4
mm	mm	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
v	v	k7c6
říjnu	říjen	k1gInSc6
1908	#num#	k4
nepršelo	pršet	k5eNaImAgNnS
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrná	průměrný	k2eAgFnSc1d1
maximální	maximální	k2eAgFnSc1d1
výška	výška	k1gFnSc1
sněhové	sněhový	k2eAgFnSc2d1
pokrývky	pokrývka	k1gFnSc2
je	být	k5eAaImIp3nS
na	na	k7c6
Milešovce	Milešovka	k1gFnSc6
nejvyšší	vysoký	k2eAgFnSc6d3
v	v	k7c6
únoru	únor	k1gInSc6
(	(	kIx(
<g/>
35	#num#	k4
cm	cm	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Maximální	maximální	k2eAgFnSc1d1
zaznamenaná	zaznamenaný	k2eAgFnSc1d1
výška	výška	k1gFnSc1
sněhu	sníh	k1gInSc2
na	na	k7c6
Milešovce	Milešovka	k1gFnSc6
byla	být	k5eAaImAgFnS
v	v	k7c6
březnu	březen	k1gInSc6
1965	#num#	k4
(	(	kIx(
<g/>
135	#num#	k4
cm	cm	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Milešovka	Milešovka	k1gFnSc1
je	být	k5eAaImIp3nS
díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
poloze	poloha	k1gFnSc3
a	a	k8xC
výšce	výška	k1gFnSc3
největrnější	větrný	k2eAgFnSc7d3
stanicí	stanice	k1gFnSc7
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
(	(	kIx(
<g/>
tvoří	tvořit	k5eAaImIp3nP
nejvyšší	vysoký	k2eAgInSc4d3
bod	bod	k1gInSc4
v	v	k7c6
dalekém	daleký	k2eAgNnSc6d1
okolí	okolí	k1gNnSc6
<g/>
,	,	kIx,
nejbližší	blízký	k2eAgFnSc7d3
překážkou	překážka	k1gFnSc7
jsou	být	k5eAaImIp3nP
Krušné	krušný	k2eAgFnPc1d1
hory	hora	k1gFnPc1
<g/>
;	;	kIx,
její	její	k3xOp3gInSc4
povrch	povrch	k1gInSc4
o	o	k7c6
malé	malý	k2eAgFnSc6d1
ploše	plocha	k1gFnSc6
znamená	znamenat	k5eAaImIp3nS
menší	malý	k2eAgNnSc4d2
tření	tření	k1gNnSc4
<g/>
,	,	kIx,
málo	málo	k6eAd1
zmírňující	zmírňující	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
proudění	proudění	k1gNnSc2
vzduchu	vzduch	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrná	průměrný	k2eAgFnSc1d1
roční	roční	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
větru	vítr	k1gInSc2
činí	činit	k5eAaImIp3nS
8,6	8,6	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
,	,	kIx,
největrnějšími	větrný	k2eAgInPc7d3
měsíci	měsíc	k1gInPc7
jsou	být	k5eAaImIp3nP
listopad	listopad	k1gInSc4
a	a	k8xC
prosinec	prosinec	k1gInSc4
(	(	kIx(
<g/>
10	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nejméně	málo	k6eAd3
větrnými	větrný	k2eAgInPc7d1
květen	květen	k1gInSc4
a	a	k8xC
srpen	srpen	k1gInSc4
(	(	kIx(
<g/>
7,5	7,5	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Převládající	převládající	k2eAgInSc4d1
proudění	proudění	k1gNnSc1
jsou	být	k5eAaImIp3nP
na	na	k7c6
Milešovce	Milešovka	k1gFnSc6
západní	západní	k2eAgFnSc6d1
(	(	kIx(
<g/>
relativní	relativní	k2eAgFnSc4d1
četnost	četnost	k1gFnSc4
20,0	20,0	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
severozápadní	severozápadní	k2eAgFnSc4d1
(	(	kIx(
<g/>
relativní	relativní	k2eAgFnSc4d1
četnost	četnost	k1gFnSc4
15,4	15,4	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
v	v	k7c6
1,6	1,6	k4
%	%	kIx~
roku	rok	k1gInSc2
vládne	vládnout	k5eAaImIp3nS
na	na	k7c6
Milešovce	Milešovka	k1gFnSc6
bezvětří	bezvětří	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgFnSc1d3
rychlost	rychlost	k1gFnSc1
nárazu	náraz	k1gInSc2
větru	vítr	k1gInSc2
zde	zde	k6eAd1
byla	být	k5eAaImAgFnS
zaznamenána	zaznamenán	k2eAgFnSc1d1
14	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1967	#num#	k4
(	(	kIx(
<g/>
překročila	překročit	k5eAaPmAgFnS
horní	horní	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
registrace	registrace	k1gFnSc2
přístroje	přístroj	k1gInSc2
<g/>
,	,	kIx,
tj.	tj.	kA
50	#num#	k4
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
Na	na	k7c6
vrcholu	vrchol	k1gInSc6
Milešovky	Milešovka	k1gFnSc2
vznikla	vzniknout	k5eAaPmAgFnS
ve	v	k7c6
dvacátých	dvacátý	k4xOgNnPc6
letech	léto	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
jedna	jeden	k4xCgFnSc1
z	z	k7c2
prvních	první	k4xOgFnPc2
horských	horský	k2eAgFnPc2d1
restaurací	restaurace	k1gFnPc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1905	#num#	k4
byla	být	k5eAaImAgFnS
na	na	k7c6
vrcholu	vrchol	k1gInSc6
zřízena	zřídit	k5eAaPmNgFnS
meteorologická	meteorologický	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
a	a	k8xC
u	u	k7c2
ní	on	k3xPp3gFnSc2
pak	pak	k6eAd1
19	#num#	k4
metrů	metr	k1gInPc2
vysoká	vysoký	k2eAgFnSc1d1
rozhledna	rozhledna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
její	její	k3xOp3gNnSc4
zbudování	zbudování	k1gNnSc4
se	se	k3xPyFc4
zasloužil	zasloužit	k5eAaPmAgMnS
teplický	teplický	k2eAgMnSc1d1
průmyslník	průmyslník	k1gMnSc1
a	a	k8xC
nadšený	nadšený	k2eAgMnSc1d1
alpinista	alpinista	k1gMnSc1
Reginald	Reginald	k1gMnSc1
Czermack	Czermack	k1gMnSc1
<g/>
,	,	kIx,
čestný	čestný	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
Horského	Horského	k2eAgInSc2d1
spolku	spolek	k1gInSc2
pro	pro	k7c4
severozápadní	severozápadní	k2eAgFnPc4d1
Čechy	Čechy	k1gFnPc4
v	v	k7c6
Teplicích	Teplice	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
návrh	návrh	k1gInSc4
stanice	stanice	k1gFnSc2
bylo	být	k5eAaImAgNnS
vypsáno	vypsán	k2eAgNnSc1d1
výběrové	výběrový	k2eAgNnSc1d1
řízení	řízení	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
zvítězil	zvítězit	k5eAaPmAgInS
projekt	projekt	k1gInSc1
architekta	architekt	k1gMnSc2
J.	J.	kA
Hocka	Hocek	k1gMnSc2
v	v	k7c6
romantickém	romantický	k2eAgMnSc6d1
duchu	duch	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s>
Dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
na	na	k7c6
vrcholu	vrchol	k1gInSc6
hory	hora	k1gFnSc2
stálé	stálý	k2eAgNnSc1d1
meteorologické	meteorologický	k2eAgNnSc1d1
pracoviště	pracoviště	k1gNnSc1
Ústavu	ústav	k1gInSc2
fyziky	fyzika	k1gFnSc2
atmosféry	atmosféra	k1gFnSc2
<g/>
,	,	kIx,
součást	součást	k1gFnSc1
Akademie	akademie	k1gFnSc2
věd	věda	k1gFnPc2
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
příznivého	příznivý	k2eAgNnSc2d1
počasí	počasí	k1gNnSc2
může	moct	k5eAaImIp3nS
rozhlednu	rozhledna	k1gFnSc4
navštívit	navštívit	k5eAaPmF
veřejnost	veřejnost	k1gFnSc4
a	a	k8xC
ve	v	k7c6
věži	věž	k1gFnSc6
si	se	k3xPyFc3
prohlédnout	prohlédnout	k5eAaPmF
stálou	stálý	k2eAgFnSc4d1
expozici	expozice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výstup	výstup	k1gInSc1
je	být	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
provázen	provázet	k5eAaImNgInS
odborným	odborný	k2eAgInSc7d1
výkladem	výklad	k1gInSc7
o	o	k7c6
historii	historie	k1gFnSc6
a	a	k8xC
výsledcích	výsledek	k1gInPc6
meteorologických	meteorologický	k2eAgNnPc2d1
pozorování	pozorování	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vrcholu	vrchol	k1gInSc6
se	se	k3xPyFc4
též	též	k9
nachází	nacházet	k5eAaImIp3nS
významný	významný	k2eAgInSc1d1
telekomunikační	telekomunikační	k2eAgInSc1d1
bod	bod	k1gInSc1
<g/>
,	,	kIx,
užívaný	užívaný	k2eAgInSc1d1
zejména	zejména	k9
pro	pro	k7c4
dálkový	dálkový	k2eAgInSc4d1
přenos	přenos	k1gInSc4
dat	datum	k1gNnPc2
(	(	kIx(
<g/>
mimo	mimo	k7c4
jiné	jiný	k2eAgInPc4d1
i	i	k9
pro	pro	k7c4
komunikaci	komunikace	k1gFnSc4
mezi	mezi	k7c7
Prahou	Praha	k1gFnSc7
a	a	k8xC
některými	některý	k3yIgFnPc7
městy	město	k1gNnPc7
v	v	k7c6
severních	severní	k2eAgFnPc6d1
Čechách	Čechy	k1gFnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Funguje	fungovat	k5eAaImIp3nS
zde	zde	k6eAd1
také	také	k9
stálá	stálý	k2eAgFnSc1d1
meteorologická	meteorologický	k2eAgFnSc1d1
a	a	k8xC
telekomunikační	telekomunikační	k2eAgFnSc1d1
služba	služba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
zajištění	zajištění	k1gNnSc4
provozu	provoz	k1gInSc2
na	na	k7c6
vrcholku	vrcholek	k1gInSc6
hory	hora	k1gFnSc2
byla	být	k5eAaImAgFnS
od	od	k7c2
severu	sever	k1gInSc2
zřízena	zřídit	k5eAaPmNgFnS
malá	malý	k2eAgFnSc1d1
nákladní	nákladní	k2eAgFnSc1d1
lanová	lanový	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Vrchol	vrchol	k1gInSc1
Milešovky	Milešovka	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1892	#num#	k4
Pravěké	pravěký	k2eAgInPc4d1
doklady	doklad	k1gInPc4
o	o	k7c6
přítomnosti	přítomnost	k1gFnSc6
člověka	člověk	k1gMnSc2
na	na	k7c6
Milešovce	Milešovka	k1gFnSc6
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
období	období	k1gNnSc2
neolitu	neolit	k1gInSc2
(	(	kIx(
<g/>
cca	cca	kA
5600	#num#	k4
<g/>
–	–	k?
<g/>
4200	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l	l	kA
<g/>
)	)	kIx)
a	a	k8xC
mají	mít	k5eAaImIp3nP
podobu	podoba	k1gFnSc4
kamenných	kamenný	k2eAgFnPc2d1
seker	sekera	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bronzové	bronzový	k2eAgInPc1d1
předměty	předmět	k1gInPc1
pocházející	pocházející	k2eAgInPc1d1
z	z	k7c2
mladší	mladý	k2eAgFnSc2d2
a	a	k8xC
pozdní	pozdní	k2eAgFnSc2d1
doby	doba	k1gFnSc2
bronzové	bronzový	k2eAgFnSc2d1
(	(	kIx(
<g/>
cca	cca	kA
1300	#num#	k4
<g/>
–	–	k?
<g/>
750	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
se	se	k3xPyFc4
ve	v	k7c6
větším	veliký	k2eAgNnSc6d2
měřítku	měřítko	k1gNnSc6
nalezly	nalézt	k5eAaBmAgInP,k5eAaPmAgInP
na	na	k7c6
kopcích	kopec	k1gInPc6
Českého	český	k2eAgNnSc2d1
středohoří	středohoří	k1gNnSc2
<g/>
,	,	kIx,
mj.	mj.	kA
na	na	k7c6
Hradišťanech	Hradišťan	k1gMnPc6
<g/>
,	,	kIx,
Deblíku	Deblík	k1gMnSc3
<g/>
,	,	kIx,
Plešivci	plešivec	k1gMnSc3
a	a	k8xC
také	také	k9
na	na	k7c6
Milešovce	Milešovka	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byla	být	k5eAaImAgFnS
nalezena	naleznout	k5eAaPmNgFnS,k5eAaBmNgFnS
rovněž	rovněž	k9
keramika	keramika	k1gFnSc1
z	z	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zřejmě	zřejmě	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
doklady	doklad	k1gInPc4
jakéhosi	jakýsi	k3yIgInSc2
kultu	kult	k1gInSc2
hor.	hor.	k?
Prostřednictvím	prostřednictvím	k7c2
střepů	střep	k1gInPc2
doložili	doložit	k5eAaPmAgMnP
archeologové	archeolog	k1gMnPc1
na	na	k7c6
Milešovce	Milešovka	k1gFnSc6
pobyt	pobyt	k1gInSc4
Keltů	Kelt	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
může	moct	k5eAaImIp3nS
souviset	souviset	k5eAaImF
s	s	k7c7
jejím	její	k3xOp3gNnSc7
využitím	využití	k1gNnSc7
jako	jako	k8xC,k8xS
signálního	signální	k2eAgInSc2d1
bodu	bod	k1gInSc2
nebo	nebo	k8xC
kultovního	kultovní	k2eAgNnSc2d1
místa	místo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žádná	žádný	k3yNgFnSc1
z	z	k7c2
pravěkých	pravěký	k2eAgFnPc2d1
kultur	kultura	k1gFnPc2
však	však	k9
na	na	k7c6
hoře	hora	k1gFnSc6
nevytvořila	vytvořit	k5eNaPmAgFnS
opevnění	opevnění	k1gNnSc4
nebo	nebo	k8xC
sídliště	sídliště	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstarší	starý	k2eAgInSc1d3
archeologický	archeologický	k2eAgInSc1d1
materiál	materiál	k1gInSc1
spojený	spojený	k2eAgInSc1d1
se	s	k7c7
Slovany	Slovan	k1gInPc7
jsou	být	k5eAaImIp3nP
střepy	střep	k1gInPc4
ze	z	k7c2
starší	starý	k2eAgFnSc2d2
doby	doba	k1gFnSc2
hradištní	hradištní	k2eAgFnSc2d1
(	(	kIx(
<g/>
cca	cca	kA
650	#num#	k4
<g/>
–	–	k?
<g/>
800	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
hoře	hora	k1gFnSc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1521	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
je	být	k5eAaImIp3nS
v	v	k7c6
deskách	deska	k1gFnPc6
dvorských	dvorský	k2eAgInPc2d1
vypsaný	vypsaný	k2eAgInSc4d1
nemovitý	movitý	k2eNgInSc4d1
majetek	majetek	k1gInSc4
Václava	Václav	k1gMnSc2
Kostomlatského	Kostomlatský	k2eAgMnSc2d1
z	z	k7c2
Vřesovic	Vřesovice	k1gFnPc2
<g/>
,	,	kIx,
jemuž	jenž	k3xRgNnSc3
patřily	patřit	k5eAaImAgFnP
mj.	mj.	kA
i	i	k8xC
Černčice	Černčice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zápisu	zápis	k1gInSc6
se	se	k3xPyFc4
uvádí	uvádět	k5eAaImIp3nS
"	"	kIx"
<g/>
les	les	k1gInSc1
v	v	k7c6
Milessowske	Milessowske	k1gNnSc6
horze	horze	k1gFnSc2
<g/>
...	...	k?
s	s	k7c7
rybníčkem	rybníček	k1gInSc7
<g/>
,	,	kIx,
kterýž	kterýž	k?
leží	ležet	k5eAaImIp3nS
pod	pod	k7c7
Milessowskú	Milessowskú	k1gMnSc7
horú	horú	k?
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Hora	hora	k1gFnSc1
získala	získat	k5eAaPmAgFnS
pojmenování	pojmenování	k1gNnSc4
podle	podle	k7c2
názvu	název	k1gInSc2
vesnice	vesnice	k1gFnSc2
Milešova	Milešův	k2eAgFnSc1d1
<g/>
,	,	kIx,
doložené	doložený	k2eAgNnSc1d1
písemně	písemně	k6eAd1
od	od	k7c2
konce	konec	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jméno	jméno	k1gNnSc1
Milešov	Milešov	k1gInSc1
zřejmě	zřejmě	k6eAd1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
vlastního	vlastní	k2eAgNnSc2d1
jména	jméno	k1gNnSc2
Mileš	Mileš	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
ve	v	k7c6
staročeštině	staročeština	k1gFnSc6
znamenalo	znamenat	k5eAaImAgNnS
"	"	kIx"
<g/>
milý	milý	k2eAgInSc1d1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Německý	německý	k2eAgInSc1d1
název	název	k1gInSc1
Donnersberg	Donnersberg	k1gInSc1
(	(	kIx(
<g/>
Hromová	hromový	k2eAgFnSc1d1
hora	hora	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
až	až	k9
mladšího	mladý	k2eAgNnSc2d2
data	datum	k1gNnSc2
a	a	k8xC
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
poněmčováním	poněmčování	k1gNnSc7
kraje	kraj	k1gInSc2
po	po	k7c6
třicetileté	třicetiletý	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
tento	tento	k3xDgInSc4
název	název	k1gInSc4
ve	v	k7c6
tvaru	tvar	k1gInSc6
"	"	kIx"
<g/>
Tonner	Tonner	k1gMnSc1
Berg	Berg	k1gMnSc1
<g/>
"	"	kIx"
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
v	v	k7c6
roce	rok	k1gInSc6
1712	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Název	název	k1gInSc1
hory	hora	k1gFnSc2
se	se	k3xPyFc4
také	také	k9
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
predikátu	predikát	k1gInSc2
litoměřické	litoměřický	k2eAgFnSc2d1
patricijské	patricijský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
Mrázů	Mráz	k1gMnPc2
z	z	k7c2
Milešovky	Milešovka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
I.	I.	kA
jí	jíst	k5eAaImIp3nS
ho	on	k3xPp3gInSc4
udělil	udělit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1558	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Rodině	rodina	k1gFnSc3
patřil	patřit	k5eAaImAgInS
Dům	dům	k1gInSc1
Kalich	kalich	k1gInSc1
na	na	k7c6
litoměřickém	litoměřický	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Letecký	letecký	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
vrchol	vrchol	k1gInSc4
Milešovky	Milešovka	k1gFnSc2
Milešovku	Milešovka	k1gFnSc4
popsal	popsat	k5eAaPmAgMnS
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
díle	díl	k1gInSc6
Miscellanea	Miscellane	k2eAgFnSc1d1
historica	historica	k1gFnSc1
regni	regeň	k1gFnSc3
Bohemiae	Bohemia	k1gInSc2
(	(	kIx(
<g/>
Rozmanitosti	rozmanitost	k1gFnSc2
z	z	k7c2
historie	historie	k1gFnSc2
Království	království	k1gNnSc2
českého	český	k2eAgNnSc2d1
<g/>
)	)	kIx)
učený	učený	k2eAgMnSc1d1
jezuita	jezuita	k1gMnSc1
Bohuslav	Bohuslav	k1gMnSc1
Balbín	Balbín	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1679	#num#	k4
na	na	k7c4
horu	hora	k1gFnSc4
vystoupal	vystoupat	k5eAaPmAgInS
hned	hned	k6eAd1
několikrát	několikrát	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Balbín	Balbín	k1gMnSc1
Milešovku	Milešovka	k1gFnSc4
nazval	nazvat	k5eAaBmAgMnS,k5eAaPmAgMnS
královnou	královna	k1gFnSc7
Českého	český	k2eAgNnSc2d1
středohoří	středohoří	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zachytil	zachytit	k5eAaPmAgMnS
i	i	k9
zvyklost	zvyklost	k1gFnSc4
obyvatel	obyvatel	k1gMnPc2
okolí	okolí	k1gNnSc2
předpovídat	předpovídat	k5eAaImF
podle	podle	k7c2
hory	hora	k1gFnSc2
počasí	počasí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
vrchol	vrchol	k1gInSc4
Milešovky	Milešovka	k1gFnSc2
<g/>
,	,	kIx,
píše	psát	k5eAaImIp3nS
Balbín	Balbín	k1gInSc4
<g/>
,	,	kIx,
zahalen	zahalen	k2eAgInSc4d1
mraky	mrak	k1gInPc4
<g/>
,	,	kIx,
přijde	přijít	k5eAaPmIp3nS
s	s	k7c7
jistotou	jistota	k1gFnSc7
déšť	déšť	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
jsou	být	k5eAaImIp3nP
<g/>
-li	-li	k?
mraky	mrak	k1gInPc1
nad	nad	k7c7
ostatními	ostatní	k2eAgInPc7d1
kopci	kopec	k1gInPc7
a	a	k8xC
nad	nad	k7c7
Milešovkou	Milešovka	k1gFnSc7
nikoliv	nikoliv	k9
<g/>
,	,	kIx,
pršet	pršet	k5eAaImF
nebude	být	k5eNaImBp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
vsích	ves	k1gFnPc6
v	v	k7c6
okolí	okolí	k1gNnSc6
hory	hora	k1gFnSc2
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
způsob	způsob	k1gInSc1
předpovědi	předpověď	k1gFnPc4
používá	používat	k5eAaImIp3nS
často	často	k6eAd1
i	i	k9
v	v	k7c6
současnosti	současnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
turistiky	turistika	k1gFnSc2
</s>
<s>
Návštěvy	návštěva	k1gFnPc1
Milešovky	Milešovka	k1gFnSc2
turistického	turistický	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
začaly	začít	k5eAaPmAgFnP
na	na	k7c6
přelomu	přelom	k1gInSc6
18	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
souvisely	souviset	k5eAaImAgInP
s	s	k7c7
rozvojem	rozvoj	k1gInSc7
lázeňství	lázeňství	k1gNnSc1
v	v	k7c6
Teplicích	Teplice	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Už	už	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1808	#num#	k4
mělo	mít	k5eAaImAgNnS
Milešovku	Milešovka	k1gFnSc4
navštívit	navštívit	k5eAaPmF
kolem	kolem	k7c2
šesti	šest	k4xCc2
tisíc	tisíc	k4xCgInPc2
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1810	#num#	k4
na	na	k7c4
horu	hora	k1gFnSc4
vystoupil	vystoupit	k5eAaPmAgMnS
Johann	Johann	k1gMnSc1
Wolfgang	Wolfgang	k1gMnSc1
Goethe	Goethe	k1gFnSc4
<g/>
,	,	kIx,
mezi	mezi	k7c4
roky	rok	k1gInPc4
1819	#num#	k4
<g/>
–	–	k?
<g/>
1839	#num#	k4
pravidelně	pravidelně	k6eAd1
pruský	pruský	k2eAgMnSc1d1
král	král	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Vilém	Vilém	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Panovníka	panovník	k1gMnSc2
několikrát	několikrát	k6eAd1
doprovázel	doprovázet	k5eAaImAgMnS
přírodovědec	přírodovědec	k1gMnSc1
Alexander	Alexandra	k1gFnPc2
von	von	k1gInSc4
Humboldt	Humboldt	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
v	v	k7c6
dopisu	dopis	k1gInSc6
příteli	přítel	k1gMnSc3
označil	označit	k5eAaPmAgMnS
výhled	výhled	k1gInSc4
z	z	k7c2
Milešovky	Milešovka	k1gFnSc2
za	za	k7c7
třetí	třetí	k4xOgFnSc7
nejkrásnější	krásný	k2eAgFnSc7d3
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Německý	německý	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
a	a	k8xC
cestovatel	cestovatel	k1gMnSc1
Hanns	Hannsa	k1gFnPc2
Heinz	Heinz	k1gMnSc1
Ewers	Ewersa	k1gFnPc2
však	však	k9
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
knize	kniha	k1gFnSc6
Mit	Mit	k1gFnSc2
meinen	meinen	k2eAgInSc1d1
Augen	Augen	k1gInSc1
(	(	kIx(
<g/>
Mýma	můj	k3xOp1gFnPc7
očima	oko	k1gNnPc7
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
podobná	podobný	k2eAgNnPc4d1
hodnocení	hodnocení	k1gNnPc4
Humboldt	Humboldt	k1gMnSc1
používal	používat	k5eAaImAgMnS
pro	pro	k7c4
většinu	většina	k1gFnSc4
míst	místo	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yQgNnPc4,k3yIgNnPc4,k3yRgNnPc4
navštívil	navštívit	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Obdivně	obdivně	k6eAd1
se	se	k3xPyFc4
o	o	k7c6
Českém	český	k2eAgNnSc6d1
středohoří	středohoří	k1gNnSc6
s	s	k7c7
Milešovkou	Milešovka	k1gFnSc7
vyjádřil	vyjádřit	k5eAaPmAgMnS
např.	např.	kA
i	i	k8xC
dramatik	dramatik	k1gMnSc1
Heinrich	Heinrich	k1gMnSc1
von	von	k1gInSc4
Kleist	Kleist	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Krátce	krátce	k6eAd1
před	před	k7c7
smrtí	smrt	k1gFnSc7
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1836	#num#	k4
<g/>
,	,	kIx,
vystoupil	vystoupit	k5eAaPmAgMnS
na	na	k7c4
vrchol	vrchol	k1gInSc4
v	v	k7c6
doprovodu	doprovod	k1gInSc6
literáta	literát	k1gMnSc2
Antonína	Antonín	k1gMnSc2
Pflegera-Kopidlanského	Pflegera-Kopidlanský	k2eAgMnSc2d1
(	(	kIx(
<g/>
1812	#num#	k4
<g/>
–	–	k?
<g/>
1896	#num#	k4
<g/>
)	)	kIx)
Karel	Karel	k1gMnSc1
Hynek	Hynek	k1gMnSc1
Mácha	Mácha	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výhled	výhled	k1gInSc1
ho	on	k3xPp3gMnSc4
okouzlil	okouzlit	k5eAaPmAgInS
natolik	natolik	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
začal	začít	k5eAaPmAgInS
recitovat	recitovat	k5eAaImF
své	svůj	k3xOyFgFnPc4
básně	báseň	k1gFnPc4
a	a	k8xC
zazpíval	zazpívat	k5eAaPmAgMnS
si	se	k3xPyFc3
s	s	k7c7
přítomnými	přítomný	k2eAgMnPc7d1
českými	český	k2eAgMnPc7d1
turisty	turist	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Jaroslava	Jaroslav	k1gMnSc2
Vrchlického	Vrchlický	k1gMnSc2
inspiroval	inspirovat	k5eAaBmAgMnS
výstup	výstup	k1gInSc4
na	na	k7c4
horu	hora	k1gFnSc4
k	k	k7c3
sepsání	sepsání	k1gNnSc3
básně	báseň	k1gFnSc2
Na	na	k7c6
Milešovce	Milešovka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
ještě	ještě	k6eAd1
nebyla	být	k5eNaImAgFnS
cesta	cesta	k1gFnSc1
na	na	k7c4
vrchol	vrchol	k1gInSc4
pro	pro	k7c4
méně	málo	k6eAd2
fyzicky	fyzicky	k6eAd1
zdatné	zdatný	k2eAgMnPc4d1
návštěvníky	návštěvník	k1gMnPc4
jednoduchá	jednoduchý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lázeňští	lázeňský	k2eAgMnPc1d1
hosté	host	k1gMnPc1
mohli	moct	k5eAaImAgMnP
dojet	dojet	k5eAaPmF
kočárem	kočár	k1gInSc7
z	z	k7c2
Teplic	Teplice	k1gFnPc2
jen	jen	k9
do	do	k7c2
Bořislavi	Bořislaev	k1gFnSc6
nebo	nebo	k8xC
Žalan	Žalan	k1gInSc4
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
pokračovali	pokračovat	k5eAaImAgMnP
pěšky	pěšky	k6eAd1
do	do	k7c2
Bílky	bílek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Silnice	silnice	k1gFnSc1
do	do	k7c2
této	tento	k3xDgFnSc2
osady	osada	k1gFnSc2
bezprostředně	bezprostředně	k6eAd1
pod	pod	k7c7
Milešovkou	Milešovka	k1gFnSc7
byla	být	k5eAaImAgFnS
totiž	totiž	k9
vybudována	vybudovat	k5eAaPmNgFnS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
1836	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Výstupy	výstup	k1gInPc1
bohatých	bohatý	k2eAgMnPc2d1
lázeňských	lázeňský	k2eAgMnPc2d1
hostů	host	k1gMnPc2
na	na	k7c4
Milešovku	Milešovka	k1gFnSc4
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
pro	pro	k7c4
část	část	k1gFnSc4
obyvatel	obyvatel	k1gMnPc2
Bílky	Bílek	k1gMnPc4
zdrojem	zdroj	k1gInSc7
přivýdělku	přivýdělek	k1gInSc2
<g/>
,	,	kIx,
když	když	k8xS
působili	působit	k5eAaImAgMnP
jako	jako	k9
nosiči	nosič	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
speciálně	speciálně	k6eAd1
upravených	upravený	k2eAgFnPc6d1
sedačkách	sedačka	k1gFnPc6
si	se	k3xPyFc3
posadili	posadit	k5eAaPmAgMnP
hosta	host	k1gMnSc4
na	na	k7c4
záda	záda	k1gNnPc4
a	a	k8xC
takto	takto	k6eAd1
ho	on	k3xPp3gMnSc4
za	za	k7c4
3	#num#	k4
krejcary	krejcar	k1gInPc4
donesli	donést	k5eAaPmAgMnP
až	až	k9
na	na	k7c4
vrchol	vrchol	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zimní	zimní	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
Milešovku	Milešovka	k1gFnSc4
První	první	k4xOgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
zajistil	zajistit	k5eAaPmAgMnS
návštěvníkům	návštěvník	k1gMnPc3
základní	základní	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
k	k	k7c3
delšímu	dlouhý	k2eAgInSc3d2
pobytu	pobyt	k1gInSc3
na	na	k7c6
vrcholu	vrchol	k1gInSc6
hory	hora	k1gFnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
Antonín	Antonín	k1gMnSc1
Veber	Veber	k1gMnSc1
(	(	kIx(
<g/>
†	†	k?
1858	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hostinský	hostinský	k2eAgInSc1d1
nejprve	nejprve	k6eAd1
v	v	k7c6
Milešově	Milešův	k2eAgFnSc6d1
a	a	k8xC
později	pozdě	k6eAd2
ve	v	k7c6
Velemíně	Velemína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veber	Veber	k1gInSc1
pochopil	pochopit	k5eAaPmAgInS
možnosti	možnost	k1gFnPc4
<g/>
,	,	kIx,
jaké	jaký	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
poskytování	poskytování	k1gNnSc1
stravovacích	stravovací	k2eAgFnPc2d1
a	a	k8xC
ubytovacích	ubytovací	k2eAgFnPc2d1
možností	možnost	k1gFnPc2
na	na	k7c6
Milešovce	Milešovka	k1gFnSc6
nabízí	nabízet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1820	#num#	k4
zde	zde	k6eAd1
se	s	k7c7
souhlasem	souhlas	k1gInSc7
správy	správa	k1gFnSc2
hrzánovského	hrzánovský	k2eAgNnSc2d1
panství	panství	k1gNnSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
hora	hora	k1gFnSc1
patřila	patřit	k5eAaImAgFnS
<g/>
,	,	kIx,
zřídil	zřídit	k5eAaPmAgInS
provizorní	provizorní	k2eAgInSc1d1
hostinec	hostinec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nP
mohli	moct	k5eAaImAgMnP
návštěvníci	návštěvník	k1gMnPc1
zažít	zažít	k5eAaPmF
pověstný	pověstný	k2eAgInSc4d1
východ	východ	k1gInSc4
slunce	slunce	k1gNnSc2
<g/>
,	,	kIx,
postavil	postavit	k5eAaPmAgMnS
zde	zde	k6eAd1
roku	rok	k1gInSc2
1825	#num#	k4
několik	několik	k4yIc1
přístřešků	přístřešek	k1gInPc2
<g/>
,	,	kIx,
umožňující	umožňující	k2eAgNnSc1d1
přenocování	přenocování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Veber	Veber	k1gMnSc1
se	se	k3xPyFc4
také	také	k9
zasloužil	zasloužit	k5eAaPmAgMnS
o	o	k7c4
zřízení	zřízení	k1gNnSc4
pohodlných	pohodlný	k2eAgFnPc2d1
přístupových	přístupový	k2eAgFnPc2d1
cest	cesta	k1gFnPc2
na	na	k7c4
vrchol	vrchol	k1gInSc4
z	z	k7c2
Bílky	bílek	k1gInPc1
a	a	k8xC
z	z	k7c2
Velemína	Velemín	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Veber	Veber	k1gInSc1
na	na	k7c6
kopci	kopec	k1gInSc6
zřídil	zřídit	k5eAaPmAgInS
také	také	k9
obchod	obchod	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
prodávala	prodávat	k5eAaImAgFnS
bižuterie	bižuterie	k1gFnPc4
<g/>
,	,	kIx,
české	český	k2eAgNnSc4d1
sklo	sklo	k1gNnSc4
<g/>
,	,	kIx,
minerály	minerál	k1gInPc4
z	z	k7c2
okolí	okolí	k1gNnSc2
a	a	k8xC
další	další	k2eAgInPc4d1
upomínkové	upomínkový	k2eAgInPc4d1
předměty	předmět	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospoda	Hospoda	k?
byla	být	k5eAaImAgFnS
časem	časem	k6eAd1
rozšířena	rozšířit	k5eAaPmNgFnS
na	na	k7c4
dvě	dva	k4xCgFnPc4
jídelny	jídelna	k1gFnPc4
<g/>
,	,	kIx,
poblíž	poblíž	k6eAd1
postavena	postaven	k2eAgFnSc1d1
kaplička	kaplička	k1gFnSc1
a	a	k8xC
upravilo	upravit	k5eAaPmAgNnS
se	se	k3xPyFc4
také	také	k9
prostranství	prostranství	k1gNnSc1
k	k	k7c3
tanci	tanec	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
Vebera	Vebero	k1gNnSc2
byla	být	k5eAaImAgNnP
také	také	k9
na	na	k7c6
vrcholu	vrchol	k1gInSc6
postavena	postaven	k2eAgFnSc1d1
čtyřmetrová	čtyřmetrový	k2eAgFnSc1d1
rozhledna	rozhledna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
Veberově	Veberův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
se	se	k3xPyFc4
nájemci	nájemce	k1gMnPc1
restaurace	restaurace	k1gFnSc2
střídali	střídat	k5eAaImAgMnP
<g/>
,	,	kIx,
v	v	k7c6
únoru	únor	k1gInSc6
1905	#num#	k4
však	však	k9
budova	budova	k1gFnSc1
vyhořela	vyhořet	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
ale	ale	k9
postavila	postavit	k5eAaPmAgFnS
ledeburská	ledeburský	k2eAgFnSc1d1
lesní	lesní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
hospodu	hospodu	k?
novou	nový	k2eAgFnSc7d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c4
provoz	provoz	k1gInSc4
na	na	k7c6
Milešovce	Milešovka	k1gFnSc6
se	se	k3xPyFc4
staral	starat	k5eAaImAgInS
Nordwestböhmische	Nordwestböhmische	k1gNnSc7
Gebirgsvereins-Verband	Gebirgsvereins-Verband	k1gInSc1
Teplitz	Teplitz	k1gInSc1
(	(	kIx(
<g/>
Horský	horský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
pro	pro	k7c4
severozápadní	severozápadní	k2eAgFnPc4d1
Čechy	Čechy	k1gFnPc4
Teplice	teplice	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
založený	založený	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1880	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
také	také	k9
přišel	přijít	k5eAaPmAgMnS
s	s	k7c7
myšlenkou	myšlenka	k1gFnSc7
zřídit	zřídit	k5eAaPmF
na	na	k7c6
vrcholu	vrchol	k1gInSc6
meteorologickou	meteorologický	k2eAgFnSc4d1
observatoř	observatoř	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zájem	zájem	k1gInSc1
o	o	k7c4
Milešovku	Milešovka	k1gFnSc4
nebyl	být	k5eNaImAgInS
nikdy	nikdy	k6eAd1
přerušen	přerušit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
od	od	k7c2
roku	rok	k1gInSc2
1958	#num#	k4
pořádal	pořádat	k5eAaImAgInS
turistický	turistický	k2eAgInSc1d1
oddíl	oddíl	k1gInSc1
z	z	k7c2
Ústí	ústí	k1gNnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
každou	každý	k3xTgFnSc4
první	první	k4xOgFnSc4
jarní	jarní	k2eAgFnSc4d1
neděli	neděle	k1gFnSc4
na	na	k7c6
Milešovce	Milešovka	k1gFnSc6
horskou	horský	k2eAgFnSc4d1
slavnost	slavnost	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Milešovka	Milešovka	k1gFnSc1
je	být	k5eAaImIp3nS
jako	jako	k9
dobře	dobře	k6eAd1
přístupný	přístupný	k2eAgInSc4d1
vrchol	vrchol	k1gInSc4
s	s	k7c7
vynikajícím	vynikající	k2eAgInSc7d1
výhledem	výhled	k1gInSc7
turisticky	turisticky	k6eAd1
velmi	velmi	k6eAd1
atraktivní	atraktivní	k2eAgMnSc1d1
a	a	k8xC
je	být	k5eAaImIp3nS
hojně	hojně	k6eAd1
navštěvována	navštěvován	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Značené	značený	k2eAgFnPc1d1
turistické	turistický	k2eAgFnPc1d1
trasy	trasa	k1gFnPc1
vedou	vést	k5eAaImIp3nP
z	z	k7c2
Velemína	Velemín	k1gInSc2
<g/>
,	,	kIx,
Bílky	bílek	k1gInPc1
<g/>
,	,	kIx,
Černčic	Černčice	k1gFnPc2
a	a	k8xC
Milešova	Milešův	k2eAgNnSc2d1
<g/>
,	,	kIx,
z	z	k7c2
Paškapole	Paškapole	k1gFnSc2
se	se	k3xPyFc4
napojuje	napojovat	k5eAaImIp3nS
žlutá	žlutý	k2eAgFnSc1d1
turistická	turistický	k2eAgFnSc1d1
značka	značka	k1gFnSc1
na	na	k7c4
cestu	cesta	k1gFnSc4
od	od	k7c2
Velemína	Velemín	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vrcholu	vrchol	k1gInSc6
je	být	k5eAaImIp3nS
o	o	k7c6
sobotách	sobota	k1gFnPc6
<g/>
,	,	kIx,
nedělích	neděle	k1gFnPc6
a	a	k8xC
svátcích	svátek	k1gInPc6
otevřená	otevřený	k2eAgFnSc1d1
restaurace	restaurace	k1gFnSc1
<g/>
,	,	kIx,
od	od	k7c2
pátku	pátek	k1gInSc2
do	do	k7c2
neděle	neděle	k1gFnSc2
je	být	k5eAaImIp3nS
otevřený	otevřený	k2eAgInSc4d1
bufet	bufet	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rovněž	rovněž	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
nachází	nacházet	k5eAaImIp3nS
řada	řada	k1gFnSc1
informačních	informační	k2eAgFnPc2d1
tabulí	tabule	k1gFnPc2
věnovaných	věnovaný	k2eAgFnPc2d1
historii	historie	k1gFnSc4
meteorologické	meteorologický	k2eAgFnSc2d1
observatoře	observatoř	k1gFnSc2
<g/>
,	,	kIx,
okolní	okolní	k2eAgFnSc3d1
přírodě	příroda	k1gFnSc3
a	a	k8xC
meteorologii	meteorologie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Milešovka	Milešovka	k1gFnSc1
od	od	k7c2
Milešova	Milešův	k2eAgInSc2d1
</s>
<s>
Meteorologická	meteorologický	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
s	s	k7c7
rozhlednou	rozhledna	k1gFnSc7
</s>
<s>
Nákladní	nákladní	k2eAgFnSc1d1
lanovka	lanovka	k1gFnSc1
</s>
<s>
Milešovka	Milešovka	k1gFnSc1
od	od	k7c2
jihovýchodu	jihovýchod	k1gInSc2
</s>
<s>
Milešovka	Milešovka	k1gFnSc1
od	od	k7c2
Černčic	Černčice	k1gFnPc2
</s>
<s>
Milešovka	Milešovka	k1gFnSc1
a	a	k8xC
Milešov	Milešov	k1gInSc1
z	z	k7c2
Ostrého	ostrý	k2eAgInSc2d1
</s>
<s>
Meteorologická	meteorologický	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1910	#num#	k4
</s>
<s>
Zimní	zimní	k2eAgInSc1d1
výhled	výhled	k1gInSc1
z	z	k7c2
Milešovky	Milešovka	k1gFnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
České	český	k2eAgFnSc2d1
hory	hora	k1gFnSc2
s	s	k7c7
prominencí	prominence	k1gFnSc7
>	>	kIx)
500	#num#	k4
metrů	metr	k1gInPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ultratisicovky	Ultratisicovka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Základní	základní	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
ČR	ČR	kA
1	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
000	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeměměřický	zeměměřický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HOCEK	HOCEK	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Milešovka	Milešovka	k1gFnSc1
<g/>
:	:	kIx,
rekordmanka	rekordmanka	k1gFnSc1
Českého	český	k2eAgNnSc2d1
středohoří	středohoří	k1gNnSc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
a.	a.	k?
s.	s.	k?
<g/>
,	,	kIx,
2007-10-11	2007-10-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://www.zakonyprolidi.cz/cs/2013-449	http://www.zakonyprolidi.cz/cs/2013-449	k4
<g/>
↑	↑	k?
PLAČEK	Plaček	k1gMnSc1
<g/>
,	,	kIx,
Štěpán	Štěpán	k1gMnSc1
<g/>
;	;	kIx,
KROPÁČEK	Kropáček	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přehled	přehled	k1gInSc1
ochrany	ochrana	k1gFnSc2
české	český	k2eAgFnSc2d1
přírody	příroda	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
-	-	kIx~
Seznam	seznam	k1gInSc1
přírodních	přírodní	k2eAgFnPc2d1
památek	památka	k1gFnPc2
narůstá	narůstat	k5eAaImIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktualne	Aktualn	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
↑	↑	k?
Severní	severní	k2eAgFnPc1d1
Čechy	Čechy	k1gFnPc1
<g/>
:	:	kIx,
České	český	k2eAgNnSc1d1
Středohoří	středohoří	k1gNnSc1
<g/>
:	:	kIx,
Milešovka	Milešovka	k1gFnSc1
<g/>
.	.	kIx.
www.prostor-ad.cz	www.prostor-ad.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prostor	prostor	k1gInSc1
-	-	kIx~
architektura	architektura	k1gFnSc1
<g/>
,	,	kIx,
interiér	interiér	k1gInSc1
<g/>
,	,	kIx,
design	design	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ŠTEKL	ŠTEKL	k?
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
(	(	kIx(
<g/>
ed	ed	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Milešovka	Milešovka	k1gFnSc1
a	a	k8xC
milešovský	milešovský	k2eAgInSc1d1
region	region	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
182	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
1376	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Kotyza	Kotyza	k1gFnSc1
<g/>
,	,	kIx,
Oldřich	Oldřich	k1gMnSc1
<g/>
,	,	kIx,
Historie	historie	k1gFnSc1
okolí	okolí	k1gNnSc2
Milešovky	Milešovka	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
11	#num#	k4
<g/>
–	–	k?
<g/>
17	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
Milešovka	Milešovka	k1gFnSc1
2005	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Milešovka	Milešovka	k1gFnSc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
29	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
PROFOUS	PROFOUS	kA
<g/>
,	,	kIx,
Antonín	Antonín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místní	místní	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
:	:	kIx,
jejich	jejich	k3xOp3gInSc4
vznik	vznik	k1gInSc4
<g/>
,	,	kIx,
původní	původní	k2eAgInSc4d1
význam	význam	k1gInSc4
a	a	k8xC
změny	změna	k1gFnPc4
<g/>
,	,	kIx,
díl	díl	k1gInSc4
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Státní	státní	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
učebnic	učebnice	k1gFnPc2
<g/>
,	,	kIx,
1951	#num#	k4
<g/>
.	.	kIx.
632	#num#	k4
s.	s.	k?
S.	S.	kA
76	#num#	k4
<g/>
–	–	k?
<g/>
77	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Milešovka	Milešovka	k1gFnSc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
31	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Genealogie	genealogie	k1gFnSc1
Mrázů	Mráz	k1gMnPc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Olympia	Olympia	k1gFnSc1
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
.	.	kIx.
304	#num#	k4
s.	s.	k?
Kapitola	kapitola	k1gFnSc1
Smetana	Smetana	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
,	,	kIx,
Historie	historie	k1gFnSc1
turistiky	turistika	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
55	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
Smetana	smetana	k1gFnSc1
1985	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
↑	↑	k?
BALBÍN	BALBÍN	kA
<g/>
,	,	kIx,
Bohuslav	Bohuslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krásy	krása	k1gFnSc2
a	a	k8xC
bohatství	bohatství	k1gNnSc2
české	český	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Panorama	panorama	k1gNnSc1
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
.	.	kIx.
351	#num#	k4
s.	s.	k?
S.	S.	kA
73	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Smetana	Smetana	k1gMnSc1
1985	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
55	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
URBAN	Urban	k1gMnSc1
<g/>
,	,	kIx,
F.	F.	kA
J.	J.	kA
Severočeská	severočeský	k2eAgFnSc1d1
historie	historie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Most	most	k1gInSc1
<g/>
:	:	kIx,
Severočeské	severočeský	k2eAgNnSc1d1
menšinové	menšinový	k2eAgNnSc1d1
knihkupectví	knihkupectví	k1gNnSc1
<g/>
,	,	kIx,
1924	#num#	k4
<g/>
.	.	kIx.
271	#num#	k4
s.	s.	k?
S.	S.	kA
161	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
EWERS	EWERS	kA
<g/>
,	,	kIx,
Hanns	Hanns	k1gInSc4
Heinz	Heinza	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mit	Mit	k1gFnSc1
meinen	meinna	k1gFnPc2
Augen	Augna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fahrten	Fahrten	k2eAgInSc1d1
durch	durch	k1gInSc1
die	die	k?
lateinische	lateinischat	k5eAaPmIp3nS
Welt	Welt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
München	München	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
<g/>
,	,	kIx,
1914	#num#	k4
<g/>
.	.	kIx.
350	#num#	k4
s.	s.	k?
S.	S.	kA
304	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
SITTE	SITTE	kA
<g/>
,	,	kIx,
Franz	Franz	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
slunné	slunný	k2eAgFnSc6d1
straně	strana	k1gFnSc6
Milešovky	Milešovka	k1gFnSc2
<g/>
/	/	kIx~
<g/>
An	An	k1gFnSc1
der	drát	k5eAaImRp2nS
Sonnenseite	Sonnenseit	k1gInSc5
des	des	k1gNnSc3
Milleschauers	Milleschauers	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duchcov	Duchcov	k1gInSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Kapucín	kapucín	k1gMnSc1
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
.	.	kIx.
152	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86467	#num#	k4
<g/>
-	-	kIx~
<g/>
43	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
87	#num#	k4
<g/>
–	–	k?
<g/>
88	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Dále	daleko	k6eAd2
jen	jen	k9
Sitte	Sitt	k1gInSc5
2011	#num#	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
↑	↑	k?
MÁCHA	Mácha	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
Hynek	Hynek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrady	hrad	k1gInPc1
spatřené	spatřený	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Panorama	panorama	k1gNnSc1
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
246	#num#	k4
s.	s.	k?
S.	S.	kA
29	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Text	text	k1gInSc4
básně	báseň	k1gFnSc2
Archivováno	archivovat	k5eAaBmNgNnS
4	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
.	.	kIx.
<g/>
↑	↑	k?
KOLEKTIV	kolektivum	k1gNnPc2
AUTORŮ	autor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bořislav	Bořislava	k1gFnPc2
a	a	k8xC
okolí	okolí	k1gNnSc2
v	v	k7c6
čase	čas	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bořislav	Bořislava	k1gFnPc2
<g/>
:	:	kIx,
Obec	obec	k1gFnSc1
Bořislav	Bořislava	k1gFnPc2
<g/>
,	,	kIx,
2014	#num#	k4
<g/>
.	.	kIx.
245	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
260	#num#	k4
<g/>
-	-	kIx~
<g/>
70	#num#	k4
<g/>
-	-	kIx~
<g/>
91	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
114	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ŠPECINGR	ŠPECINGR	kA
<g/>
,	,	kIx,
Otakar	Otakar	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Českým	český	k2eAgNnSc7d1
středohořím	středohoří	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Sportovní	sportovní	k2eAgNnSc1d1
a	a	k8xC
turistické	turistický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
<g/>
,	,	kIx,
1957	#num#	k4
<g/>
.	.	kIx.
139	#num#	k4
s.	s.	k?
S.	S.	kA
97	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
BOROVSKÝ	Borovský	k1gMnSc1
<g/>
,	,	kIx,
F.	F.	kA
A.	A.	kA
(	(	kIx(
<g/>
ed	ed	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čechy	Čech	k1gMnPc4
VII	VII	kA
<g/>
,	,	kIx,
Středohoří	středohoří	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
J.	J.	kA
Otto	Otto	k1gMnSc1
<g/>
,	,	kIx,
1892	#num#	k4
<g/>
.	.	kIx.
260	#num#	k4
s.	s.	k?
S.	S.	kA
58	#num#	k4
<g/>
–	–	k?
<g/>
59	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Milešovka	Milešovka	k1gFnSc1
2005	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
38	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Sitte	Sitt	k1gInSc5
2011	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
88	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ŠTEKL	ŠTEKL	k?
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
;	;	kIx,
BRÁZDIL	brázdit	k5eAaImAgMnS
<g/>
,	,	kIx,
Rudolf	Rudolf	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klimatické	klimatický	k2eAgInPc1d1
poměry	poměr	k1gInPc1
Milešovky	Milešovka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
433	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
744	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
</s>
<s>
EHRLICH	EHRLICH	kA
<g/>
,	,	kIx,
R.	R.	kA
Der	drát	k5eAaImRp2nS
Donnersberg	Donnersberg	k1gMnSc1
(	(	kIx(
<g/>
Milleschauer	Milleschauer	k1gMnSc1
<g/>
)	)	kIx)
in	in	k?
Böhmen	Böhmen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Erzgebirgs-Zeitung	Erzgebirgs-Zeitunga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
1920	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
41	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
EHRLICH	EHRLICH	kA
<g/>
,	,	kIx,
R.	R.	kA
Der	drát	k5eAaImRp2nS
Donnersberg	Donnersberg	k1gMnSc1
oder	odra	k1gFnPc2
Milleschauer	Milleschauer	k1gMnSc1
in	in	k?
Böhmischen	Böhmischen	k1gInSc1
Mittelgebirge	Mittelgebirge	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teplitz-Schönau	Teplitz-Schönaus	k1gInSc2
<g/>
:	:	kIx,
Vlastním	vlastní	k2eAgInSc7d1
nákladem	náklad	k1gInSc7
<g/>
,	,	kIx,
1921	#num#	k4
<g/>
.	.	kIx.
24	#num#	k4
s.	s.	k?
</s>
<s>
EHRLICH	EHRLICH	kA
<g/>
,	,	kIx,
R.	R.	kA
Der	drát	k5eAaImRp2nS
Donnersberg	Donnersberg	k1gInSc1
in	in	k?
geschichtlicher	geschichtlichra	k1gFnPc2
<g/>
,	,	kIx,
kartographischer	kartographischra	k1gFnPc2
und	und	k?
touristischer	touristischra	k1gFnPc2
Hinsicht	Hinsicht	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Erzgebirgs-Zeitung	Erzgebirgs-Zeitunga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
1929	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
52	#num#	k4
<g/>
,	,	kIx,
55	#num#	k4
<g/>
–	–	k?
<g/>
60	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
FIŠÁK	FIŠÁK	kA
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
;	;	kIx,
ŘEZÁČOVÁ	Řezáčová	k1gFnSc1
<g/>
,	,	kIx,
Daniela	Daniela	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncentrace	koncentrace	k1gFnSc1
polutantů	polutant	k1gInPc2
v	v	k7c6
mlžné	mlžný	k2eAgFnSc6d1
(	(	kIx(
<g/>
oblačné	oblačný	k2eAgFnSc6d1
<g/>
)	)	kIx)
vodě	voda	k1gFnSc6
na	na	k7c6
Milešovce	Milešovka	k1gFnSc6
při	při	k7c6
vybraných	vybraný	k2eAgFnPc6d1
epizodách	epizoda	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Meteorologické	meteorologický	k2eAgFnPc4d1
zprávy	zpráva	k1gFnPc4
<g/>
.	.	kIx.
2001	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
53	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
6	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
169	#num#	k4
<g/>
–	–	k?
<g/>
178	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
26	#num#	k4
<g/>
-	-	kIx~
<g/>
1173	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
FIŠÁK	FIŠÁK	kA
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
;	;	kIx,
ŘEZÁČOVÁ	Řezáčová	k1gFnSc1
<g/>
,	,	kIx,
Daniela	Daniela	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Comparison	Comparison	k1gNnSc1
between	between	k1gInSc1
Pollutant	Pollutant	k1gMnSc1
Concentration	Concentration	k1gInSc1
in	in	k?
the	the	k?
Samples	Samples	k1gMnSc1
of	of	k?
Fog	Fog	k1gMnSc1
and	and	k?
Rime	Rim	k1gFnSc2
Water	Water	k1gMnSc1
Collected	Collected	k1gMnSc1
at	at	k?
Mt	Mt	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Milešovka	Milešovka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Studia	studio	k1gNnPc1
geophysica	geophysicus	k1gMnSc2
et	et	k?
geodaetica	geodaeticus	k1gMnSc2
<g/>
.	.	kIx.
2001	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
45	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
319	#num#	k4
<g/>
–	–	k?
<g/>
324	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
39	#num#	k4
<g/>
-	-	kIx~
<g/>
3169	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
FIŠÁK	FIŠÁK	kA
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Synoptic	Synoptice	k1gFnPc2
situations	situationsa	k1gFnPc2
and	and	k?
pollutant	pollutant	k1gMnSc1
concentrations	concentrations	k6eAd1
in	in	k?
fog	fog	k?
water	water	k1gMnSc1
samples	samples	k1gMnSc1
from	from	k1gMnSc1
the	the	k?
Milešovka	Milešovka	k1gFnSc1
Mt	Mt	k1gFnSc2
<g/>
..	..	k?
Studia	studio	k1gNnSc2
geophysica	geophysic	k1gInSc2
et	et	k?
geodaetica	geodaetica	k1gMnSc1
<g/>
.	.	kIx.
2004	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
48	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
469	#num#	k4
<g/>
–	–	k?
<g/>
481	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
39	#num#	k4
<g/>
-	-	kIx~
<g/>
3169	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
GREGOR	Gregor	k1gMnSc1
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klimatický	klimatický	k2eAgInSc1d1
kalendář	kalendář	k1gInSc1
Milešovky	Milešovka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Meteorologické	meteorologický	k2eAgFnPc4d1
zprávy	zpráva	k1gFnPc4
<g/>
.	.	kIx.
1954	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
7	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
93	#num#	k4
<g/>
–	–	k?
<g/>
95	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
26	#num#	k4
<g/>
-	-	kIx~
<g/>
1173	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
HIBSCH	HIBSCH	kA
<g/>
,	,	kIx,
J.	J.	kA
E.	E.	kA
Der	drát	k5eAaImRp2nS
Donnersberg	Donnersberg	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Erzgebirgs-Zeitung	Erzgebirgs-Zeitung	k1gInSc1
<g/>
.	.	kIx.
1929	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
50	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
26	#num#	k4
<g/>
–	–	k?
<g/>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
ZACHAROV	ZACHAROV	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
;	;	kIx,
BLIŽŇÁK	BLIŽŇÁK	kA
<g/>
,	,	kIx,
V.	V.	kA
<g/>
;	;	kIx,
CHLÁDOVÁ	CHLÁDOVÁ	kA
<g/>
,	,	kIx,
Z.	Z.	kA
<g/>
;	;	kIx,
MATÚŠEK	MATÚŠEK	kA
<g/>
,	,	kIx,
M.	M.	kA
<g/>
;	;	kIx,
PEŠICE	PEŠICE	kA
<g/>
,	,	kIx,
P.	P.	kA
<g/>
;	;	kIx,
SEDLÁK	Sedlák	k1gMnSc1
<g/>
,	,	kIx,
P.	P.	kA
<g/>
,	,	kIx,
SOKOL	Sokol	k1gMnSc1
<g/>
,	,	kIx,
Z.	Z.	kA
Observatoř	observatoř	k1gFnSc1
Milešovka	Milešovka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Středisko	středisko	k1gNnSc1
společných	společný	k2eAgFnPc2d1
činností	činnost	k1gFnPc2
AV	AV	kA
ČR	ČR	kA
pro	pro	k7c4
Ústav	ústav	k1gInSc4
fyziky	fyzika	k1gFnSc2
atmosféry	atmosféra	k1gFnSc2
AV	AV	kA
ČR	ČR	kA
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
23	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
270	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
124	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Chráněná	chráněný	k2eAgFnSc1d1
krajinná	krajinný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
národních	národní	k2eAgFnPc2d1
přírodních	přírodní	k2eAgFnPc2d1
rezervací	rezervace	k1gFnPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Seznam	seznam	k1gInSc1
chráněných	chráněný	k2eAgNnPc2d1
území	území	k1gNnPc2
v	v	k7c6
okrese	okres	k1gInSc6
Litoměřice	Litoměřice	k1gInPc4
</s>
<s>
Seznam	seznam	k1gInSc1
památných	památný	k2eAgInPc2d1
stromů	strom	k1gInPc2
v	v	k7c6
okrese	okres	k1gInSc6
Litoměřice	Litoměřice	k1gInPc4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Milešovka	Milešovka	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Milešovka	Milešovka	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Klimatologické	klimatologický	k2eAgFnPc1d1
normály	normála	k1gFnPc1
stanice	stanice	k1gFnSc2
Milešovka	Milešovka	k1gFnSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Stránky	stránka	k1gFnPc4
Obecně	obecně	k6eAd1
prospěšné	prospěšný	k2eAgFnPc4d1
společnosti	společnost	k1gFnPc4
Milešovka	Milešovka	k1gFnSc1
</s>
<s>
Prohlížeč	prohlížeč	k1gMnSc1
webových	webový	k2eAgFnPc2d1
kamer	kamera	k1gFnPc2
ČHMÚ	ČHMÚ	kA
s	s	k7c7
vysokým	vysoký	k2eAgNnSc7d1
rozlišením	rozlišení	k1gNnSc7
(	(	kIx(
<g/>
Milešovka	Milešovka	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Hora	hora	k1gFnSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
Českého	český	k2eAgInSc2d1
středohoří	středohoří	k1gNnSc6
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
Stránky	stránka	k1gFnPc1
závodu	závod	k1gInSc2
Milešovka	Milešovka	k1gFnSc1
365	#num#	k4
</s>
<s>
Informace	informace	k1gFnPc1
o	o	k7c6
hoře	hora	k1gFnSc6
s	s	k7c7
fotografiemi	fotografia	k1gFnPc7
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Geologický	geologický	k2eAgInSc1d1
vývoj	vývoj	k1gInSc1
Milešovky	Milešovka	k1gFnSc2
(	(	kIx(
<g/>
video	video	k1gNnSc4
ČGS	ČGS	kA
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Chráněná	chráněný	k2eAgNnPc1d1
území	území	k1gNnPc1
v	v	k7c6
okrese	okres	k1gInSc6
Litoměřice	Litoměřice	k1gInPc1
Přírodní	přírodní	k2eAgInPc1d1
parky	park	k1gInPc1
</s>
<s>
Dolní	dolní	k2eAgNnSc1d1
Poohří	Poohří	k1gNnSc1
Chráněné	chráněný	k2eAgFnSc2d1
krajinné	krajinný	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
</s>
<s>
České	český	k2eAgNnSc1d1
středohoří	středohoří	k1gNnSc1
•	•	k?
Kokořínsko	Kokořínsko	k1gNnSc1
–	–	k?
Máchův	Máchův	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
rezervace	rezervace	k1gFnSc2
</s>
<s>
Lovoš	Lovoš	k1gMnSc1
•	•	k?
Milešovka	Milešovka	k1gFnSc1
•	•	k?
Sedlo	sedlo	k1gNnSc4
Národní	národní	k2eAgFnSc2d1
přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Bílé	bílý	k2eAgFnSc2d1
stráně	stráň	k1gFnSc2
•	•	k?
Borečský	Borečský	k2eAgInSc1d1
vrch	vrch	k1gInSc1
•	•	k?
Dubí	dubí	k1gNnSc2
hora	hora	k1gFnSc1
•	•	k?
Kleneč	Kleneč	k1gFnSc1
Přírodní	přírodní	k2eAgFnPc1d1
rezervace	rezervace	k1gFnPc1
</s>
<s>
Březina	Březina	k1gMnSc1
•	•	k?
Holý	Holý	k1gMnSc1
vrch	vrch	k1gInSc1
•	•	k?
Holý	holý	k2eAgInSc1d1
vrch	vrch	k1gInSc1
u	u	k7c2
Hlinné	hlinný	k2eAgFnSc2d1
•	•	k?
Kalvárie	Kalvárie	k1gFnSc2
•	•	k?
Lipská	lipský	k2eAgFnSc1d1
hora	hora	k1gFnSc1
•	•	k?
Loužek	loužka	k1gFnPc2
•	•	k?
Mokřady	mokřad	k1gInPc1
dolní	dolní	k2eAgFnSc2d1
Liběchovky	Liběchovka	k1gFnSc2
•	•	k?
Myslivna	myslivna	k1gFnSc1
•	•	k?
Na	na	k7c6
Černčí	Černčí	k2eAgFnSc6d1
•	•	k?
Pístecký	Pístecký	k2eAgInSc1d1
les	les	k1gInSc1
Přírodní	přírodní	k2eAgFnSc2d1
památky	památka	k1gFnSc2
</s>
<s>
Bílé	bílý	k2eAgFnPc1d1
stráně	stráň	k1gFnPc1
u	u	k7c2
Štětí	štětit	k5eAaImIp3nP
•	•	k?
Dobříňský	Dobříňský	k2eAgInSc1d1
háj	háj	k1gInSc1
•	•	k?
Evaňská	Evaňský	k2eAgFnSc1d1
rokle	rokle	k1gFnSc1
•	•	k?
Říp	Říp	k1gInSc1
•	•	k?
Hradiště	Hradiště	k1gNnSc1
•	•	k?
Košťálov	Košťálovo	k1gNnPc2
•	•	k?
Koštice	Koštice	k1gFnSc2
•	•	k?
Kuzov	Kuzov	k1gInSc1
•	•	k?
Mokřad	mokřad	k1gInSc1
pod	pod	k7c7
Terezínskou	terezínský	k2eAgFnSc7d1
pevností	pevnost	k1gFnSc7
•	•	k?
Na	na	k7c6
Dlouhé	Dlouhé	k2eAgFnSc6d1
stráni	stráň	k1gFnSc6
•	•	k?
Písčiny	písčina	k1gFnSc2
u	u	k7c2
Oleška	Olešek	k1gInSc2
•	•	k?
Plešivec	plešivec	k1gMnSc1
•	•	k?
Radobýl	Radobýl	k1gMnSc1
•	•	k?
Radouň	Radouň	k1gFnSc1
•	•	k?
Skalky	skalka	k1gFnSc2
u	u	k7c2
Třebutiček	Třebutička	k1gFnPc2
•	•	k?
Slatiniště	slatiniště	k1gNnSc4
u	u	k7c2
Vrbky	vrbka	k1gFnSc2
•	•	k?
Sovice	sovice	k1gFnSc2
u	u	k7c2
Brzánek	Brzánka	k1gFnPc2
•	•	k?
Stráně	stráň	k1gFnSc2
nad	nad	k7c7
Suchým	suchý	k2eAgInSc7d1
potokem	potok	k1gInSc7
•	•	k?
Stráně	stráň	k1gFnSc2
u	u	k7c2
Drahobuzi	Drahobuze	k1gFnSc4
•	•	k?
Stráně	stráň	k1gFnPc1
u	u	k7c2
Velkého	velký	k2eAgInSc2d1
Újezdu	Újezd	k1gInSc2
•	•	k?
Údolí	údolí	k1gNnSc1
Podbradeckého	Podbradecký	k2eAgInSc2d1
potoka	potok	k1gInSc2
•	•	k?
V	v	k7c6
kuksu	kuks	k1gInSc6
•	•	k?
Vrch	vrch	k1gInSc1
Hazmburk	Hazmburk	k1gInSc1
•	•	k?
Vrbka	vrbka	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
|	|	kIx~
Příroda	příroda	k1gFnSc1
</s>
