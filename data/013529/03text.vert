<s>
Juliana	Julian	k1gMnSc4
z	z	k7c2
Norwiche	Norwich	k1gFnSc2
</s>
<s>
Juliana	Julian	k1gMnSc4
z	z	k7c2
Norwiche	Norwich	k1gFnSc2
Narození	narození	k1gNnSc2
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1342	#num#	k4
<g/>
Norfolk	Norfolko	k1gNnPc2
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
1416	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
73	#num#	k4
<g/>
–	–	k?
<g/>
74	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Norwich	Norwich	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
teoložka	teoložka	k1gFnSc1
a	a	k8xC
spisovatelka	spisovatelka	k1gFnSc1
Nábož	Nábož	k1gFnSc1
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc1
</s>
<s>
katolická	katolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Juliana	Juliana	k1gFnSc1
z	z	k7c2
Norwiche	Norwich	k1gFnSc2
(	(	kIx(
<g/>
1342	#num#	k4
<g/>
–	–	k?
asi	asi	k9
1416	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
anglická	anglický	k2eAgFnSc1d1
poustevnice	poustevnice	k1gFnSc1
a	a	k8xC
křesťanská	křesťanský	k2eAgFnSc1d1
mystička	mystička	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známa	známo	k1gNnPc4
je	být	k5eAaImIp3nS
jako	jako	k9
autorka	autorka	k1gFnSc1
mystického	mystický	k2eAgInSc2d1
spisu	spis	k1gInSc2
Zjevení	zjevení	k1gNnSc1
božské	božský	k2eAgFnSc2d1
lásky	láska	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žila	žít	k5eAaImAgFnS
v	v	k7c6
cele	cela	k1gFnSc6
u	u	k7c2
kostela	kostel	k1gInSc2
sv.	sv.	kA
Juliána	Julián	k1gMnSc2
v	v	k7c6
Norwichi	Norwich	k1gFnSc6
u	u	k7c2
řeky	řeka	k1gFnSc2
Wensum	Wensum	k1gInSc1
<g/>
,	,	kIx,
podle	podle	k7c2
zasvěcení	zasvěcení	k1gNnSc2
tohoto	tento	k3xDgInSc2
kostela	kostel	k1gInSc2
zřejmě	zřejmě	k6eAd1
převzala	převzít	k5eAaPmAgFnS
i	i	k8xC
své	svůj	k3xOyFgNnSc4
řeholní	řeholní	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mystický	mystický	k2eAgInSc1d1
zážitek	zážitek	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
její	její	k3xOp3gFnSc7
iniciací	iniciace	k1gFnSc7
jako	jako	k8xS,k8xC
vizionářky	vizionářka	k1gFnPc4
<g/>
,	,	kIx,
zažila	zažít	k5eAaPmAgFnS
8	#num#	k4
<g/>
.	.	kIx.
nebo	nebo	k8xC
13	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1373	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vážně	vážně	k6eAd1
onemocněla	onemocnět	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Julian	Julian	k1gMnSc1
of	of	k?
Norwich	Norwich	k1gMnSc1
<g/>
,	,	kIx,
English	English	k1gMnSc1
mystic	mystice	k1gFnPc2
—	—	k?
Encyclopaedia	Encyclopaedium	k1gNnSc2
Britannica	Britannica	k1gFnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Miroslav	Miroslav	k1gMnSc1
Zvelebil	Zvelebil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Juliana	Julian	k1gMnSc2
z	z	k7c2
Norwiche	Norwich	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Getsemany	Getsemany	k1gInPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Juliana	Julian	k1gMnSc2
z	z	k7c2
Norwiche	Norwich	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19990004103	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118558692	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
7980	#num#	k4
1193	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79089397	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
97938503	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79089397	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
|	|	kIx~
Středověk	středověk	k1gInSc1
</s>
