<s>
Cyklistická	cyklistický	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
12	#num#	k4
</s>
<s>
Cyklistická	cyklistický	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
</s>
<s>
U	u	k7c2
Mlýnské	mlýnský	k2eAgFnSc2d1
stoky	stoka	k1gFnSc2
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
</s>
<s>
U	u	k7c2
Dlouhého	Dlouhého	k2eAgInSc2d1
mostu	most	k1gInSc2
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
</s>
<s>
Cyklistická	cyklistický	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
12	#num#	k4
je	být	k5eAaImIp3nS
cyklistická	cyklistický	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
Klubu	klub	k1gInSc2
českých	český	k2eAgMnPc2d1
turistů	turist	k1gMnPc2
I.	I.	kA
třídy	třída	k1gFnSc2
určená	určený	k2eAgFnSc1d1
pro	pro	k7c4
cyklistiku	cyklistika	k1gFnSc4
vedená	vedený	k2eAgFnSc1d1
mezi	mezi	k7c7
obcemi	obec	k1gFnPc7
Dolní	dolní	k2eAgFnSc7d1
Dvořiště	dvořiště	k1gNnSc4
a	a	k8xC
Lom	lom	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prochází	procházet	k5eAaImIp3nS
okresy	okres	k1gInPc4
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
<g/>
,	,	kIx,
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
a	a	k8xC
Tábor	Tábor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Délka	délka	k1gFnSc1
trasy	trasa	k1gFnSc2
je	být	k5eAaImIp3nS
111,5	111,5	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzdálenost	vzdálenost	k1gFnSc4
vzdušnou	vzdušný	k2eAgFnSc7d1
čarou	čára	k1gFnSc7
je	být	k5eAaImIp3nS
81	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
evropské	evropský	k2eAgFnSc2d1
cyklistické	cyklistický	k2eAgFnSc2d1
trasy	trasa	k1gFnSc2
EuroVelo	EuroVela	k1gFnSc5
7	#num#	k4
(	(	kIx(
<g/>
Středoevropská	středoevropský	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Křížení	křížení	k1gNnSc4
s	s	k7c7
jinými	jiný	k2eAgFnPc7d1
cyklistickými	cyklistický	k2eAgFnPc7d1
trasami	trasa	k1gFnPc7
</s>
<s>
kmbodnavazující	kmbodnavazující	k2eAgMnSc1d1
<g/>
,	,	kIx,
křižující	křižující	k2eAgInSc1d1
trasysouřadnicenadm	trasysouřadnicenadm	k1gInSc1
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
0	#num#	k4
<g/>
Dolní	dolní	k2eAgInSc1d1
Dvořiště	dvořiště	k1gNnSc4
<g/>
,	,	kIx,
hraniční	hraniční	k2eAgInSc4d1
přechod	přechod	k1gInSc4
<g/>
48	#num#	k4
<g/>
°	°	k?
<g/>
38	#num#	k4
<g/>
′	′	k?
<g/>
38	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
27	#num#	k4
<g/>
′	′	k?
<g/>
15	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
*	*	kIx~
<g/>
624	#num#	k4
m	m	kA
*	*	kIx~
</s>
<s>
27,5	27,5	k4
<g/>
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
104748	#num#	k4
<g/>
°	°	k?
<g/>
48	#num#	k4
<g/>
′	′	k?
<g/>
53	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
18	#num#	k4
<g/>
′	′	k?
<g/>
47	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
*	*	kIx~
<g/>
519	#num#	k4
m	m	kA
*	*	kIx~
</s>
<s>
53	#num#	k4
<g/>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
,	,	kIx,
Háječek	háječek	k1gInSc1
122	#num#	k4
1018	#num#	k4
1050	#num#	k4
1096	#num#	k4
110048	#num#	k4
<g/>
°	°	k?
<g/>
58	#num#	k4
<g/>
′	′	k?
<g/>
20	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
28	#num#	k4
<g/>
′	′	k?
<g/>
22	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
*	*	kIx~
<g/>
385	#num#	k4
m	m	kA
*	*	kIx~
</s>
<s>
63	#num#	k4
<g/>
Hluboká	Hluboká	k1gFnSc1
nad	nad	k7c7
Vltavou	Vltava	k1gFnSc7
<g/>
,	,	kIx,
rozcestí	rozcestí	k1gNnSc6
1078	#num#	k4
1079	#num#	k4
1080	#num#	k4
1081	#num#	k4
108249	#num#	k4
<g/>
°	°	k?
<g/>
2	#num#	k4
<g/>
′	′	k?
<g/>
57	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
26	#num#	k4
<g/>
′	′	k?
<g/>
29	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
*	*	kIx~
<g/>
375	#num#	k4
m	m	kA
*	*	kIx~
</s>
<s>
111,5	111,5	k4
<g/>
Lom	lom	k1gInSc1
11	#num#	k4
31	#num#	k4
3249	#num#	k4
<g/>
°	°	k?
<g/>
21	#num#	k4
<g/>
′	′	k?
<g/>
46	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
37	#num#	k4
<g/>
′	′	k?
<g/>
41	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
*	*	kIx~
<g/>
465	#num#	k4
m	m	kA
*	*	kIx~
</s>
<s>
*	*	kIx~
přibližný	přibližný	k2eAgInSc4d1
údaj	údaj	k1gInSc4
</s>
<s>
Obce	obec	k1gFnPc1
na	na	k7c6
trase	trasa	k1gFnSc6
</s>
<s>
Značka	značka	k1gFnSc1
trasy	trasa	k1gFnSc2
</s>
<s>
Dolní	dolní	k2eAgNnSc1d1
Dvořiště	dvořiště	k1gNnSc1
</s>
<s>
Rožmitál	Rožmitál	k1gInSc1
na	na	k7c6
Šumavě	Šumava	k1gFnSc6
</s>
<s>
Český	český	k2eAgInSc1d1
Krumlov	Krumlov	k1gInSc1
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
Koruna	koruna	k1gFnSc1
</s>
<s>
Kamenný	kamenný	k2eAgInSc1d1
Újezd	Újezd	k1gInSc1
</s>
<s>
Boršov	Boršov	k1gInSc1
nad	nad	k7c7
Vltavou	Vltava	k1gFnSc7
</s>
<s>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
</s>
<s>
Hluboká	Hluboká	k1gFnSc1
nad	nad	k7c7
Vltavou	Vltava	k1gFnSc7
</s>
<s>
Dolní	dolní	k2eAgNnSc1d1
Bukovsko	Bukovsko	k1gNnSc1
</s>
<s>
Zálší	Zálší	k1gNnSc1
(	(	kIx(
<g/>
okres	okres	k1gInSc1
Tábor	Tábor	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Hlavatce	hlavatec	k1gInPc1
(	(	kIx(
<g/>
okres	okres	k1gInSc1
Tábor	Tábor	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Želeč	Želeč	k1gInSc1
(	(	kIx(
<g/>
okres	okres	k1gInSc1
Tábor	Tábor	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Lom	lom	k1gInSc1
(	(	kIx(
<g/>
okres	okres	k1gInSc1
Tábor	Tábor	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Dostupné	dostupný	k2eAgFnPc1d1
mapy	mapa	k1gFnPc1
ke	k	k7c3
článku	článek	k1gInSc3
</s>
<s>
OSMMapy	OSMMapa	k1gFnPc1
<g/>
.	.	kIx.
<g/>
czBing	czBing	k1gInSc1
</s>
<s>
Souřadnice	souřadnice	k1gFnSc1
ve	v	k7c6
formátu	formát	k1gInSc6
KML	KML	kA
<g/>
:	:	kIx,
Export	export	k1gInSc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Cyklistická	cyklistický	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
12	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Cyklistická	cyklistický	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
12	#num#	k4
na	na	k7c6
webu	web	k1gInSc6
Cyklotrasy	cyklotrasa	k1gFnSc2
</s>
<s>
Cyklistická	cyklistický	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
12	#num#	k4
v	v	k7c6
projektu	projekt	k1gInSc6
OpenStreetMap	OpenStreetMap	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Cyklistika	cyklistika	k1gFnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
</s>
