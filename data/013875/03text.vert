<s>
Horst	Horst	k1gMnSc1
Naudé	Naudý	k2eAgFnSc2d1
</s>
<s>
Horst	Horst	k5eAaPmF
Naudé	Naudý	k2eAgNnSc4d1
Narození	narození	k1gNnSc4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1895	#num#	k4
<g/>
Charlottenburg	Charlottenburg	k1gInSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1983	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
87	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Bad	Bad	k1gFnSc1
Neuenahr	Neuenahra	k1gFnPc2
Politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
NSDAP	NSDAP	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Horst	Horst	k1gMnSc1
Rudolf	Rudolf	k1gMnSc1
Hans	Hans	k1gMnSc1
Georg	Georg	k1gInSc4
Naudé	Naudý	k2eAgNnSc1d1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1895	#num#	k4
Charlottenburg	Charlottenburg	k1gInSc1
–	–	k?
19	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1983	#num#	k4
Bad	Bad	k1gMnSc1
Neuenahr	Neuenahr	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
byl	být	k5eAaImAgMnS
německý	německý	k2eAgMnSc1d1
právník	právník	k1gMnSc1
a	a	k8xC
hejtman	hejtman	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Naudé	Naudý	k2eAgNnSc1d1
byl	být	k5eAaImAgInS
vzděláním	vzdělání	k1gNnSc7
právník	právník	k1gMnSc1
a	a	k8xC
bojoval	bojovat	k5eAaImAgMnS
v	v	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1920	#num#	k4
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k8xS
soudní	soudní	k2eAgMnSc1d1
úředník	úředník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1929	#num#	k4
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
vládním	vládní	k2eAgMnSc7d1
rádou	ráda	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
se	se	k3xPyFc4
připojil	připojit	k5eAaPmAgInS
k	k	k7c3
Německé	německý	k2eAgFnSc3d1
lidové	lidový	k2eAgFnSc3d1
straně	strana	k1gFnSc3
(	(	kIx(
<g/>
DVP	DVP	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naudé	Naudý	k2eAgFnSc6d1
vstoupil	vstoupit	k5eAaPmAgMnS
do	do	k7c2
NSDAP	NSDAP	kA
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1933	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Číslo	číslo	k1gNnSc1
člena	člen	k1gMnSc2
NSDAP	NSDAP	kA
měl	mít	k5eAaImAgInS
1	#num#	k4
774	#num#	k4
979	#num#	k4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1933	#num#	k4
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaImNgMnS,k5eAaBmNgMnS
dočasným	dočasný	k2eAgMnSc7d1
správcem	správce	k1gMnSc7
okresu	okres	k1gInSc2
v	v	k7c6
kraji	kraj	k1gInSc6
Fischhausen	Fischhausen	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1934	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1939	#num#	k4
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k9
správce	správce	k1gMnSc1
okresu	okres	k1gInSc2
okres	okres	k1gInSc1
Quedlinburg	Quedlinburg	k1gInSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1939	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
Naudé	Naudý	k2eAgFnSc2d1
ministrem	ministr	k1gMnSc7
v	v	k7c6
Protektorátu	protektorát	k1gInSc6
Čechy	Čechy	k1gFnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
působil	působit	k5eAaImAgMnS
jako	jako	k8xC,k8xS
vedoucí	vedoucí	k1gMnSc1
moravského	moravský	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
protektora	protektor	k1gMnSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1941	#num#	k4
byl	být	k5eAaImAgInS
viceprezidentem	viceprezident	k1gMnSc7
Čech	Čechy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1945	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
1947	#num#	k4
byl	být	k5eAaImAgInS
uvězněn	uvěznit	k5eAaPmNgMnS
v	v	k7c6
americkém	americký	k2eAgNnSc6d1
zajetí	zajetí	k1gNnSc6
<g/>
.	.	kIx.
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Erlebnisse	Erlebnisse	k1gFnSc1
und	und	k?
Erkenntnisse	Erkenntnisse	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Als	Als	k1gFnSc1
politischer	politischra	k1gFnPc2
Beamter	Beamter	k1gMnSc1
im	im	k?
Protektorat	Protektorat	k1gInSc1
Böhmen	Böhmen	k1gInSc1
und	und	k?
Mähren	Mährna	k1gFnPc2
1939	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
,	,	kIx,
München	München	k1gInSc1
<g/>
,	,	kIx,
1975	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Horst	Horst	k1gFnSc1
Naudé	Naudý	k2eAgNnSc1d1
na	na	k7c6
německé	německý	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
matrika	matrika	k1gFnSc1
zemřelých	zemřelý	k1gMnPc2
města	město	k1gNnSc2
Bad	Bad	k1gFnSc2
Neuenahru	Neuenahra	k1gFnSc4
Nr	Nr	k1gFnSc4
<g/>
.	.	kIx.
319	#num#	k4
<g/>
/	/	kIx~
<g/>
1983	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Christian	Christian	k1gMnSc1
Rohrer	Rohrer	k1gMnSc1
<g/>
:	:	kIx,
Nationalsozialistische	Nationalsozialistische	k1gFnSc1
Macht	Machta	k1gFnPc2
in	in	k?
Ostpreussen	Ostpreussna	k1gFnPc2
<g/>
,	,	kIx,
Meidenbauer	Meidenbaura	k1gFnPc2
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
S.	S.	kA
590	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Reinhold	Reinhold	k1gMnSc1
Zilch	Zilch	k1gMnSc1
<g/>
,	,	kIx,
Bärbel	Bärbel	k1gMnSc1
Holtz	Holtz	k1gMnSc1
(	(	kIx(
<g/>
Bearb	Bearb	k1gMnSc1
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Die	Die	k1gMnSc1
Protokolle	Protokoll	k1gMnSc2
des	des	k1gNnSc2
Preußischen	Preußischen	k2eAgInSc1d1
Staatsministeriums	Staatsministeriums	k1gInSc1
1817	#num#	k4
<g/>
–	–	k?
<g/>
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
38	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bd	Bd	k1gFnSc1
<g/>
.	.	kIx.
12	#num#	k4
<g/>
/	/	kIx~
<g/>
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Berlin-Brandenburgische	Berlin-Brandenburgische	k1gInSc1
Akademie	akademie	k1gFnSc2
der	drát	k5eAaImRp2nS
Wissenschaften	Wissenschaften	k2eAgInSc4d1
(	(	kIx(
<g/>
Hrsg	Hrsg	k1gInSc4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Acta	Act	k2eAgFnSc1d1
Borussica	Borussica	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neue	Neue	k1gFnSc1
Folge	Folge	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olms-Weidmann	Olms-Weidmann	k1gNnSc1
<g/>
,	,	kIx,
Hildesheim	Hildesheim	k1gInSc1
2003	#num#	k4
<g/>
,	,	kIx,
S.	S.	kA
650	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Online	Onlin	k1gMnSc5
<g/>
;	;	kIx,
PDF	PDF	kA
2,2	2,2	k4
MB	MB	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Herausgegeben	Herausgegeben	k2eAgInSc4d1
von	von	k1gInSc4
Ulf	Ulf	k1gMnSc1
Brunnbauer	Brunnbauer	k1gMnSc1
<g/>
,	,	kIx,
Andreas	Andreas	k1gMnSc1
Helmedach	Helmedach	k1gMnSc1
<g/>
,	,	kIx,
Stefan	Stefan	k1gMnSc1
Troebst	Troebst	k1gMnSc1
<g/>
:	:	kIx,
Schnittstellen	Schnittstellen	k1gInSc1
<g/>
:	:	kIx,
Gesellschaft	Gesellschaft	k1gInSc1
<g/>
,	,	kIx,
Nation	Nation	k1gInSc1
<g/>
,	,	kIx,
Konflikt	konflikt	k1gInSc1
und	und	k?
Erinnerung	Erinnerung	k1gInSc1
in	in	k?
...	...	k?
<g/>
,	,	kIx,
S.	S.	kA
319	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Prezident	prezident	k1gMnSc1
země	zem	k1gFnSc2
České	český	k2eAgFnSc2d1
Prezidenti	prezident	k1gMnPc1
zemské	zemský	k2eAgFnSc2d1
správy	správa	k1gFnSc2
politické	politický	k2eAgFnSc2d1
</s>
<s>
Jan	Jan	k1gMnSc1
Kosina	Kosina	k1gMnSc1
(	(	kIx(
<g/>
1918	#num#	k4
<g/>
–	–	k?
<g/>
1926	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hugo	Hugo	k1gMnSc1
Kubát	Kubát	k1gMnSc1
(	(	kIx(
<g/>
1926	#num#	k4
<g/>
–	–	k?
<g/>
1928	#num#	k4
<g/>
)	)	kIx)
Zemští	zemský	k2eAgMnPc1d1
prezidenti	prezident	k1gMnPc1
</s>
<s>
Hugo	Hugo	k1gMnSc1
Kubát	Kubát	k1gMnSc1
(	(	kIx(
<g/>
1928	#num#	k4
<g/>
–	–	k?
<g/>
1932	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Josef	Josef	k1gMnSc1
Sobotka	Sobotka	k1gMnSc1
(	(	kIx(
<g/>
1933	#num#	k4
<g/>
–	–	k?
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Richard	Richard	k1gMnSc1
Bienert	Bienert	k1gMnSc1
(	(	kIx(
<g/>
1939	#num#	k4
<g/>
–	–	k?
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Horst	Horst	k1gInSc4
Naudé	Naudý	k2eAgNnSc1d1
(	(	kIx(
<g/>
úřadující	úřadující	k2eAgMnSc1d1
viceprezident	viceprezident	k1gMnSc1
<g/>
;	;	kIx,
1942	#num#	k4
<g/>
–	–	k?
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
Předsedové	předseda	k1gMnPc1
zemského	zemský	k2eAgInSc2d1
národního	národní	k2eAgInSc2d1
výboru	výbor	k1gInSc2
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Kopřiva	Kopřiva	k1gMnSc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
uk	uk	k?
<g/>
2011658855	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
118586580	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
0908	#num#	k4
0271	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
12839006	#num#	k4
</s>
