<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
IUPAC	IUPAC	kA	IUPAC
předložila	předložit	k5eAaPmAgFnS	předložit
k	k	k7c3	k
veřejné	veřejný	k2eAgFnSc3d1	veřejná
diskusi	diskuse	k1gFnSc3	diskuse
své	svůj	k3xOyFgNnSc4	svůj
doporučení	doporučení	k1gNnSc4	doporučení
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
prvek	prvek	k1gInSc1	prvek
livermorium	livermorium	k1gNnSc1	livermorium
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
sídla	sídlo	k1gNnSc2	sídlo
ústavu	ústav	k1gInSc2	ústav
Lawrence	Lawrence	k1gFnSc2	Lawrence
Livermore	Livermor	k1gInSc5	Livermor
National	National	k1gMnSc2	National
Laboratory	Laborator	k1gInPc4	Laborator
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
poprvé	poprvé	k6eAd1	poprvé
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
syntézu	syntéza	k1gFnSc4	syntéza
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
