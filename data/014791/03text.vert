<s>
Skenderija	Skenderija	k6eAd1
</s>
<s>
Skenderija	Skenderija	k6eAd1
Pohled	pohled	k1gInSc1
na	na	k7c4
halu	hala	k1gFnSc4
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1969	#num#	k4
Přestavba	přestavba	k1gFnSc1
</s>
<s>
1984	#num#	k4
<g/>
,	,	kIx,
počátek	počátek	k1gInSc4
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
Pojmenováno	pojmenován	k2eAgNnSc4d1
po	po	k7c6
</s>
<s>
Skender-paša	Skender-paš	k2eAgFnSc1d1
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Sarajevo	Sarajevo	k1gNnSc1
<g/>
,	,	kIx,
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
43	#num#	k4
<g/>
°	°	k?
<g/>
51	#num#	k4
<g/>
′	′	k?
<g/>
16,42	16,42	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
24	#num#	k4
<g/>
′	′	k?
<g/>
50,7	50,7	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hala	hala	k1gFnSc1
na	na	k7c6
východoněmecké	východoněmecký	k2eAgFnSc6d1
poštovní	poštovní	k2eAgFnSc6d1
známce	známka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Skenderija	Skenderija	k6eAd1
je	být	k5eAaImIp3nS
sportovně-kulturně-obchodní	sportovně-kulturně-obchodní	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
v	v	k7c6
Sarajevu	Sarajevo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vybudováno	vybudován	k2eAgNnSc4d1
bylo	být	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1969	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
levém	levý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
řeky	řeka	k1gFnSc2
Miljacky	Miljacka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celková	celkový	k2eAgFnSc1d1
užitná	užitný	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
centra	centrum	k1gNnSc2
činí	činit	k5eAaImIp3nS
70	#num#	k4
000	#num#	k4
m²	m²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnSc7
areálu	areál	k1gInSc2
je	být	k5eAaImIp3nS
sál	sál	k1gInSc1
zimních	zimní	k2eAgInPc2d1
sportů	sport	k1gInPc2
Mirzy	Mirza	k1gFnSc2
Delibašić	Delibašić	k1gFnSc1
a	a	k8xC
Dům	dům	k1gInSc1
mládeže	mládež	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Skenderija	Skenderija	k1gMnSc1
nese	nést	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
název	název	k1gInSc4
podle	podle	k7c2
Skender-paši	Skender-paš	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
na	na	k7c6
místě	místo	k1gNnSc6
současné	současný	k2eAgFnSc2d1
haly	hala	k1gFnSc2
nechal	nechat	k5eAaPmAgMnS
vybudovat	vybudovat	k5eAaPmF
v	v	k7c6
roce	rok	k1gInSc6
1499	#num#	k4
tržiště	tržiště	k1gNnSc2
s	s	k7c7
11	#num#	k4
stánky	stánek	k1gInPc7
<g/>
,	,	kIx,
zájezdní	zájezdní	k2eAgInSc1d1
hostinec	hostinec	k1gInSc1
(	(	kIx(
<g/>
karavanseraj	karavanseraj	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
tekke	tekke	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc4
syn	syn	k1gMnSc1
Mustafa	Mustaf	k1gMnSc2
beg	beg	k1gMnSc1
Skenderpašić	Skenderpašić	k1gMnSc1
zde	zde	k6eAd1
poté	poté	k6eAd1
nechal	nechat	k5eAaPmAgMnS
zbudovat	zbudovat	k5eAaPmF
v	v	k7c6
roce	rok	k1gInSc6
1518	#num#	k4
mešitu	mešita	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
však	však	k9
po	po	k7c6
několika	několik	k4yIc6
desítkách	desítka	k1gFnPc6
letech	let	k1gInPc6
zhroutil	zhroutit	k5eAaPmAgInS
a	a	k8xC
dochoval	dochovat	k5eAaPmAgInS
se	se	k3xPyFc4
jen	jen	k9
její	její	k3xOp3gInSc1
minaret	minaret	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
řešili	řešit	k5eAaImAgMnP
představitelé	představitel	k1gMnPc1
města	město	k1gNnSc2
Sarajeva	Sarajevo	k1gNnSc2
problém	problém	k1gInSc1
s	s	k7c7
nedostatkem	nedostatek	k1gInSc7
sportovišť	sportoviště	k1gNnPc2
ve	v	k7c6
městě	město	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nacházel	nacházet	k5eAaImAgMnS
se	se	k3xPyFc4
zde	zde	k6eAd1
nepříliš	příliš	k6eNd1
vyhovující	vyhovující	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
stadion	stadion	k1gInSc1
bez	bez	k7c2
tribun	tribuna	k1gFnPc2
a	a	k8xC
staré	starý	k2eAgInPc4d1
domy	dům	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
byly	být	k5eAaImAgInP
v	v	k7c6
nepříliš	příliš	k6eNd1
uspokojivém	uspokojivý	k2eAgInSc6d1
stavu	stav	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skenderija	Skenderij	k1gInSc2
byla	být	k5eAaImAgFnS
specifická	specifický	k2eAgFnSc1d1
lokalita	lokalita	k1gFnSc1
s	s	k7c7
nízkými	nízký	k2eAgInPc7d1
domy	dům	k1gInPc7
<g/>
,	,	kIx,
mnohdy	mnohdy	k6eAd1
ještě	ještě	k9
z	z	k7c2
období	období	k1gNnSc2
turecké	turecký	k2eAgFnSc2d1
nadvlády	nadvláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
proto	proto	k8xC
padlo	padnout	k5eAaImAgNnS,k5eAaPmAgNnS
rozhodnutí	rozhodnutí	k1gNnSc4
právě	právě	k9
zde	zde	k6eAd1
vybudovat	vybudovat	k5eAaPmF
nové	nový	k2eAgNnSc4d1
sportovní	sportovní	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
byl	být	k5eAaImAgInS
původní	původní	k2eAgInSc1d1
minaret	minaret	k1gInSc1
historické	historický	k2eAgFnSc2d1
mešity	mešita	k1gFnSc2
zbořen	zbořit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nové	Nové	k2eAgFnSc1d1
sportovní	sportovní	k2eAgFnSc1d1
centrum	centrum	k1gNnSc1
navrhl	navrhnout	k5eAaPmAgMnS
prominentní	prominentní	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
jugoslávský	jugoslávský	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
Živorad	Živorada	k1gFnPc2
Janković	Janković	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgMnS
na	na	k7c6
návrzích	návrh	k1gInPc6
řady	řada	k1gFnSc2
dalších	další	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
tohoto	tento	k3xDgInSc2
typu	typ	k1gInSc2
po	po	k7c6
celé	celý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavba	stavba	k1gFnSc1
centra	centrum	k1gNnSc2
byla	být	k5eAaImAgFnS
dokončena	dokončen	k2eAgFnSc1d1
dne	den	k1gInSc2
29	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1969	#num#	k4
(	(	kIx(
<g/>
na	na	k7c4
den	den	k1gInSc4
republiky	republika	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slavnostní	slavnostní	k2eAgInSc1d1
otevření	otevření	k1gNnSc6
doprovázelo	doprovázet	k5eAaImAgNnS
promítání	promítání	k1gNnSc1
filmu	film	k1gInSc2
Bitva	bitva	k1gFnSc1
na	na	k7c6
Neretvě	Neretva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
se	s	k7c7
zimními	zimní	k2eAgFnPc7d1
olympijskými	olympijský	k2eAgFnPc7d1
hrami	hra	k1gFnPc7
v	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
se	se	k3xPyFc4
konaly	konat	k5eAaImAgInP
v	v	k7c6
Sarajevu	Sarajevo	k1gNnSc6
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
jednak	jednak	k8xC
vybudována	vybudován	k2eAgFnSc1d1
nová	nový	k2eAgFnSc1d1
hala	hala	k1gFnSc1
(	(	kIx(
<g/>
Zetra	Zetra	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
byla	být	k5eAaImAgFnS
původní	původní	k2eAgFnSc1d1
Skenderija	Skenderija	k1gFnSc1
rozšířena	rozšířit	k5eAaPmNgFnS
a	a	k8xC
zmodernizována	zmodernizovat	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Během	během	k7c2
války	válka	k1gFnSc2
v	v	k7c6
Bosně	Bosna	k1gFnSc6
a	a	k8xC
Hercegoviny	Hercegovina	k1gFnSc2
byla	být	k5eAaImAgFnS
hala	hala	k1gFnSc1
ostřelována	ostřelován	k2eAgFnSc1d1
granátomety	granátomet	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
areálu	areál	k1gInSc2
se	se	k3xPyFc4
dochovala	dochovat	k5eAaPmAgFnS
bez	bez	k7c2
závažnějších	závažný	k2eAgFnPc2d2
škod	škoda	k1gFnPc2
<g/>
,	,	kIx,
původní	původní	k2eAgInSc1d1
Dům	dům	k1gInSc1
mládeže	mládež	k1gFnSc2
vyhořel	vyhořet	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
byla	být	k5eAaImAgFnS
hala	hala	k1gFnSc1
zprivatizována	zprivatizovat	k5eAaPmNgFnS
a	a	k8xC
na	na	k7c6
začátku	začátek	k1gInSc6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
opět	opět	k6eAd1
rekonstruována	rekonstruován	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
se	se	k3xPyFc4
střecha	střecha	k1gFnSc1
haly	hala	k1gFnSc2
pro	pro	k7c4
zimní	zimní	k2eAgInPc4d1
sporty	sport	k1gInPc4
zhroutila	zhroutit	k5eAaPmAgFnS
pod	pod	k7c7
náporem	nápor	k1gInSc7
napadaného	napadaný	k2eAgInSc2d1
sněhu	sníh	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
С	С	k?
na	na	k7c6
srbské	srbský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Článek	článek	k1gInSc1
na	na	k7c6
portálu	portál	k1gInSc6
ba	ba	k9
<g/>
.	.	kIx.
<g/>
n	n	k0
<g/>
1	#num#	k4
<g/>
info	info	k1gNnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
bosensky	bosensky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Skenderija	Skenderij	k1gInSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
|	|	kIx~
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
</s>
