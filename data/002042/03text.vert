<s>
Harry	Harr	k1gMnPc4	Harr
Potter	Pottrum	k1gNnPc2	Pottrum
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
řady	řada	k1gFnSc2	řada
fantasy	fantas	k1gInPc4	fantas
románů	román	k1gInPc2	román
britské	britský	k2eAgFnSc2d1	britská
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
J.	J.	kA	J.
K.	K.	kA	K.
Rowlingové	Rowlingový	k2eAgFnPc1d1	Rowlingová
<g/>
.	.	kIx.	.
</s>
<s>
Romány	román	k1gInPc1	román
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
dobrodružství	dobrodružství	k1gNnSc4	dobrodružství
mladého	mladý	k2eAgMnSc2d1	mladý
čaroděje	čaroděj	k1gMnSc2	čaroděj
<g/>
,	,	kIx,	,
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
Ronalda	Ronald	k1gMnSc2	Ronald
Weasleyho	Weasley	k1gMnSc2	Weasley
a	a	k8xC	a
Hermiony	Hermion	k1gInPc4	Hermion
Grangerové	Grangerová	k1gFnSc2	Grangerová
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
všichni	všechen	k3xTgMnPc1	všechen
studují	studovat	k5eAaImIp3nP	studovat
na	na	k7c6	na
Škole	škola	k1gFnSc6	škola
čar	čára	k1gFnPc2	čára
a	a	k8xC	a
kouzel	kouzlo	k1gNnPc2	kouzlo
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
tématem	téma	k1gNnSc7	téma
příběhu	příběh	k1gInSc2	příběh
je	být	k5eAaImIp3nS	být
Harryho	Harry	k1gMnSc2	Harry
souboj	souboj	k1gInSc1	souboj
s	s	k7c7	s
černokněžníkem	černokněžník	k1gMnSc7	černokněžník
Tomem	Tom	k1gMnSc7	Tom
Rojvolem	Rojvol	k1gMnSc7	Rojvol
Raddlem	Raddlo	k1gNnSc7	Raddlo
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Tom	Tom	k1gMnSc1	Tom
Marvolo	Marvola	k1gFnSc5	Marvola
Riddle	Riddle	k1gMnSc5	Riddle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známým	známý	k1gMnSc7	známý
spíše	spíše	k9	spíše
jako	jako	k9	jako
Lord	lord	k1gMnSc1	lord
Voldemort	Voldemort	k1gInSc1	Voldemort
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
dosažení	dosažení	k1gNnSc1	dosažení
vlastní	vlastní	k2eAgFnSc2d1	vlastní
nesmrtelnosti	nesmrtelnost	k1gFnSc2	nesmrtelnost
<g/>
,	,	kIx,	,
dobytí	dobytí	k1gNnSc4	dobytí
čarodějnického	čarodějnický	k2eAgInSc2d1	čarodějnický
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
podmanění	podmanění	k1gNnSc1	podmanění
nekouzelníků	nekouzelník	k1gMnPc2	nekouzelník
(	(	kIx(	(
<g/>
mudlů	mudl	k1gMnPc2	mudl
<g/>
)	)	kIx)	)
a	a	k8xC	a
zničení	zničení	k1gNnSc1	zničení
všech	všecek	k3xTgMnPc2	všecek
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
mu	on	k3xPp3gMnSc3	on
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
samotného	samotný	k2eAgMnSc4d1	samotný
Harryho	Harry	k1gMnSc4	Harry
Pottera	Potter	k1gMnSc4	Potter
<g/>
.	.	kIx.	.
</s>
<s>
Romány	román	k1gInPc1	román
jsou	být	k5eAaImIp3nP	být
směsicí	směsice	k1gFnSc7	směsice
žánrů	žánr	k1gInPc2	žánr
fantasy	fantas	k1gInPc4	fantas
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
vývojového	vývojový	k2eAgInSc2d1	vývojový
románu	román	k1gInSc2	román
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
mystery	myster	k1gInPc1	myster
<g/>
,	,	kIx,	,
thrilleru	thriller	k1gInSc2	thriller
<g/>
,	,	kIx,	,
dobrodružného	dobrodružný	k2eAgInSc2d1	dobrodružný
románu	román	k1gInSc2	román
a	a	k8xC	a
romantického	romantický	k2eAgInSc2d1	romantický
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
bývají	bývat	k5eAaImIp3nP	bývat
řazeny	řadit	k5eAaImNgFnP	řadit
mezi	mezi	k7c4	mezi
dětskou	dětský	k2eAgFnSc4d1	dětská
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
zabývají	zabývat	k5eAaImIp3nP	zabývat
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
vážnými	vážný	k2eAgNnPc7d1	vážné
tématy	téma	k1gNnPc7	téma
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Rowlingové	Rowlingový	k2eAgFnSc2d1	Rowlingová
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgNnPc2d1	hlavní
témat	téma	k1gNnPc2	téma
smrt	smrt	k1gFnSc1	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
publikace	publikace	k1gFnSc2	publikace
první	první	k4xOgFnSc2	první
knihy	kniha	k1gFnSc2	kniha
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
kámen	kámen	k1gInSc1	kámen
mudrců	mudrc	k1gMnPc2	mudrc
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
and	and	k?	and
the	the	k?	the
Philosopher	Philosophra	k1gFnPc2	Philosophra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Stone	ston	k1gInSc5	ston
<g/>
)	)	kIx)	)
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
romány	román	k1gInPc1	román
získaly	získat	k5eAaPmAgInP	získat
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
popularitu	popularita	k1gFnSc4	popularita
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
a	a	k8xC	a
dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
i	i	k9	i
velmi	velmi	k6eAd1	velmi
dobrého	dobrý	k2eAgNnSc2d1	dobré
přijetí	přijetí	k1gNnSc2	přijetí
u	u	k7c2	u
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgInP	objevovat
i	i	k9	i
kritické	kritický	k2eAgInPc1d1	kritický
ohlasy	ohlas	k1gInPc1	ohlas
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
temnému	temný	k2eAgInSc3d1	temný
podtónu	podtón	k1gInSc3	podtón
románů	román	k1gInPc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
těmito	tento	k3xDgFnPc7	tento
knihami	kniha	k1gFnPc7	kniha
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
též	též	k9	též
velkého	velký	k2eAgInSc2d1	velký
obchodního	obchodní	k2eAgInSc2d1	obchodní
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
přibližně	přibližně	k6eAd1	přibližně
450	[number]	k4	450
miliónů	milión	k4xCgInPc2	milión
výtisků	výtisk	k1gInPc2	výtisk
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
Harry	Harr	k1gInPc7	Harr
Potter	Pottra	k1gFnPc2	Pottra
stal	stát	k5eAaPmAgMnS	stát
nejlépe	dobře	k6eAd3	dobře
prodávanou	prodávaný	k2eAgFnSc7d1	prodávaná
románovou	románový	k2eAgFnSc7d1	románová
sérií	série	k1gFnSc7	série
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
čtyři	čtyři	k4xCgInPc1	čtyři
romány	román	k1gInPc1	román
také	také	k9	také
dosáhly	dosáhnout	k5eAaPmAgInP	dosáhnout
rekordu	rekord	k1gInSc3	rekord
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
rychlosti	rychlost	k1gFnSc3	rychlost
jejich	jejich	k3xOp3gFnSc2	jejich
prodeje	prodej	k1gFnSc2	prodej
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
o	o	k7c4	o
Harrym	Harrym	k1gInSc4	Harrym
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
byly	být	k5eAaImAgFnP	být
přeloženy	přeložit	k5eAaPmNgFnP	přeložit
do	do	k7c2	do
67	[number]	k4	67
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Původními	původní	k2eAgMnPc7d1	původní
hlavními	hlavní	k2eAgMnPc7d1	hlavní
vydavateli	vydavatel	k1gMnPc7	vydavatel
knih	kniha	k1gFnPc2	kniha
byla	být	k5eAaImAgFnS	být
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Bloomsbury	Bloomsbura	k1gFnSc2	Bloomsbura
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
Scholastic	Scholastice	k1gFnPc2	Scholastice
Press	Pressa	k1gFnPc2	Pressa
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
knihy	kniha	k1gFnSc2	kniha
vydala	vydat	k5eAaPmAgFnS	vydat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgNnPc2d1	další
vydavatelství	vydavatelství	k1gNnPc2	vydavatelství
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Všech	všecek	k3xTgMnPc2	všecek
sedm	sedm	k4xCc1	sedm
románů	román	k1gInPc2	román
o	o	k7c4	o
Harrym	Harrym	k1gInSc4	Harrym
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
bylo	být	k5eAaImAgNnS	být
společností	společnost	k1gFnSc7	společnost
Warner	Warnra	k1gFnPc2	Warnra
Bros	Brosa	k1gFnPc2	Brosa
<g/>
.	.	kIx.	.
</s>
<s>
Pictures	Pictures	k1gInSc4	Pictures
převedeno	převést	k5eAaPmNgNnS	převést
také	také	k9	také
na	na	k7c4	na
filmové	filmový	k2eAgNnSc4d1	filmové
plátno	plátno	k1gNnSc4	plátno
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
poslední	poslední	k2eAgMnPc1d1	poslední
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
účely	účel	k1gInPc4	účel
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
také	také	k9	také
navázala	navázat	k5eAaPmAgFnS	navázat
různé	různý	k2eAgFnPc4d1	různá
obchodní	obchodní	k2eAgFnPc4d1	obchodní
aktivity	aktivita	k1gFnPc4	aktivita
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
např.	např.	kA	např.
britské	britský	k2eAgFnSc2d1	britská
noviny	novina	k1gFnSc2	novina
The	The	k1gFnSc2	The
Times	Timesa	k1gFnPc2	Timesa
ocenily	ocenit	k5eAaPmAgFnP	ocenit
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
značku	značka	k1gFnSc4	značka
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
na	na	k7c6	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
15	[number]	k4	15
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Úvodní	úvodní	k2eAgInPc4d1	úvodní
díly	díl	k1gInPc4	díl
řady	řada	k1gFnSc2	řada
knih	kniha	k1gFnPc2	kniha
o	o	k7c4	o
Harrym	Harrym	k1gInSc4	Harrym
Potterovi	Potter	k1gMnSc3	Potter
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
mladším	mladý	k2eAgFnPc3d2	mladší
dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
autorčiným	autorčin	k2eAgInSc7d1	autorčin
záměrem	záměr	k1gInSc7	záměr
ale	ale	k9	ale
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
stárli	stárnout	k5eAaImAgMnP	stárnout
i	i	k9	i
čtenáři	čtenář	k1gMnPc1	čtenář
<g/>
.	.	kIx.	.
</s>
<s>
Sice	sice	k8xC	sice
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
nepodařilo	podařit	k5eNaPmAgNnS	podařit
vydávat	vydávat	k5eAaPmF	vydávat
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
další	další	k2eAgFnSc4d1	další
knihu	kniha	k1gFnSc4	kniha
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pozdější	pozdní	k2eAgInPc1d2	pozdější
díly	díl	k1gInPc1	díl
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
mnohem	mnohem	k6eAd1	mnohem
staršímu	starý	k2eAgNnSc3d2	starší
obecenstvu	obecenstvo	k1gNnSc3	obecenstvo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
odráží	odrážet	k5eAaImIp3nS	odrážet
i	i	k9	i
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
kromě	kromě	k7c2	kromě
"	"	kIx"	"
<g/>
dětských	dětský	k2eAgFnPc2d1	dětská
<g/>
"	"	kIx"	"
verzí	verze	k1gFnPc2	verze
knih	kniha	k1gFnPc2	kniha
jsou	být	k5eAaImIp3nP	být
vydávány	vydáván	k2eAgInPc1d1	vydáván
i	i	k8xC	i
edice	edice	k1gFnPc4	edice
pro	pro	k7c4	pro
dospělé	dospělý	k2eAgMnPc4d1	dospělý
čtenáře	čtenář	k1gMnPc4	čtenář
(	(	kIx(	(
<g/>
lišící	lišící	k2eAgMnSc1d1	lišící
se	se	k3xPyFc4	se
hlavně	hlavně	k6eAd1	hlavně
obálkou	obálka	k1gFnSc7	obálka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nápad	nápad	k1gInSc1	nápad
na	na	k7c4	na
příběh	příběh	k1gInSc4	příběh
o	o	k7c6	o
kouzelnickém	kouzelnický	k2eAgInSc6d1	kouzelnický
učni	učeň	k1gMnPc7	učeň
dostala	dostat	k5eAaPmAgFnS	dostat
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
vlakem	vlak	k1gInSc7	vlak
z	z	k7c2	z
Manchesteru	Manchester	k1gInSc2	Manchester
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gNnSc7	její
oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
místem	místo	k1gNnSc7	místo
pro	pro	k7c4	pro
psaní	psaní	k1gNnSc4	psaní
se	se	k3xPyFc4	se
během	během	k7c2	během
tvorby	tvorba	k1gFnSc2	tvorba
prvního	první	k4xOgInSc2	první
dílu	díl	k1gInSc2	díl
stala	stát	k5eAaPmAgFnS	stát
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
edinburských	edinburský	k2eAgFnPc2d1	edinburská
kaváren	kavárna	k1gFnPc2	kavárna
<g/>
.	.	kIx.	.
</s>
<s>
Prodeje	prodej	k1gInPc1	prodej
knih	kniha	k1gFnPc2	kniha
udělaly	udělat	k5eAaPmAgInP	udělat
z	z	k7c2	z
Rowlingové	Rowlingový	k2eAgInPc1d1	Rowlingový
dolarovou	dolarový	k2eAgFnSc4d1	dolarová
miliardářku	miliardářka	k1gFnSc4	miliardářka
a	a	k8xC	a
1062	[number]	k4	1062
<g/>
.	.	kIx.	.
nejbohatší	bohatý	k2eAgFnSc4d3	nejbohatší
osobu	osoba	k1gFnSc4	osoba
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
kniha	kniha	k1gFnSc1	kniha
popisuje	popisovat	k5eAaImIp3nS	popisovat
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
v	v	k7c6	v
životě	život	k1gInSc6	život
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
začátkem	začátek	k1gInSc7	začátek
školních	školní	k2eAgFnPc2d1	školní
prázdnin	prázdniny	k1gFnPc2	prázdniny
a	a	k8xC	a
konče	konče	k7c7	konče
závěrem	závěr	k1gInSc7	závěr
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
díle	díl	k1gInSc6	díl
je	být	k5eAaImIp3nS	být
Harrymu	Harrym	k1gInSc3	Harrym
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc3	jeho
spolužákům	spolužák	k1gMnPc3	spolužák
11	[number]	k4	11
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
šestém	šestý	k4xOgInSc6	šestý
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
blíží	blížit	k5eAaImIp3nS	blížit
k	k	k7c3	k
17	[number]	k4	17
<g/>
.	.	kIx.	.
narozeninám	narozeniny	k1gFnPc3	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
pátého	pátý	k4xOgInSc2	pátý
dílu	díl	k1gInSc2	díl
měly	mít	k5eAaImAgFnP	mít
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
knihy	kniha	k1gFnPc1	kniha
vzrůstající	vzrůstající	k2eAgFnSc4d1	vzrůstající
délku	délka	k1gFnSc4	délka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
šestý	šestý	k4xOgInSc1	šestý
díl	díl	k1gInSc1	díl
tuto	tento	k3xDgFnSc4	tento
tendenci	tendence	k1gFnSc4	tendence
narušil	narušit	k5eAaPmAgMnS	narušit
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
časem	časem	k6eAd1	časem
komplikuje	komplikovat	k5eAaBmIp3nS	komplikovat
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
politických	politický	k2eAgFnPc2d1	politická
zápletek	zápletka	k1gFnPc2	zápletka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
také	také	k9	také
přibývá	přibývat	k5eAaImIp3nS	přibývat
temnějších	temný	k2eAgInPc2d2	temnější
momentů	moment	k1gInPc2	moment
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sérii	série	k1gFnSc6	série
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
nalézt	nalézt	k5eAaBmF	nalézt
řadu	řada	k1gFnSc4	řada
opakujících	opakující	k2eAgInPc2d1	opakující
se	se	k3xPyFc4	se
motivů	motiv	k1gInPc2	motiv
<g/>
.	.	kIx.	.
</s>
<s>
Začátek	začátek	k1gInSc1	začátek
každého	každý	k3xTgInSc2	každý
dílu	díl	k1gInSc2	díl
popisuje	popisovat	k5eAaImIp3nS	popisovat
Harryho	Harry	k1gMnSc2	Harry
prázdniny	prázdniny	k1gFnPc4	prázdniny
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tráví	trávit	k5eAaImIp3nP	trávit
u	u	k7c2	u
své	svůj	k3xOyFgFnSc2	svůj
tety	teta	k1gFnSc2	teta
a	a	k8xC	a
strýce	strýc	k1gMnSc4	strýc
<g/>
,	,	kIx,	,
odtržen	odtrhnout	k5eAaPmNgMnS	odtrhnout
od	od	k7c2	od
kouzelnického	kouzelnický	k2eAgInSc2d1	kouzelnický
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
toužebně	toužebně	k6eAd1	toužebně
očekávaný	očekávaný	k2eAgInSc4d1	očekávaný
odjezd	odjezd	k1gInSc4	odjezd
do	do	k7c2	do
Školy	škola	k1gFnSc2	škola
čar	čára	k1gFnPc2	čára
a	a	k8xC	a
kouzel	kouzlo	k1gNnPc2	kouzlo
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
Harry	Harra	k1gFnPc1	Harra
učí	učit	k5eAaImIp3nP	učit
čarovat	čarovat	k5eAaImF	čarovat
<g/>
,	,	kIx,	,
připravovat	připravovat	k5eAaImF	připravovat
kouzelné	kouzelný	k2eAgInPc4d1	kouzelný
lektvary	lektvar	k1gInPc4	lektvar
nebo	nebo	k8xC	nebo
hrát	hrát	k5eAaImF	hrát
famfrpál	famfrpál	k1gInSc4	famfrpál
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
musí	muset	k5eAaImIp3nP	muset
překonávat	překonávat	k5eAaImF	překonávat
řadu	řada	k1gFnSc4	řada
potíží	potíž	k1gFnPc2	potíž
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
vyrovnat	vyrovnat	k5eAaBmF	vyrovnat
se	se	k3xPyFc4	se
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
nepřítelem	nepřítel	k1gMnSc7	nepřítel
Dracem	Drace	k1gMnSc7	Drace
Malfoyem	Malfoy	k1gInSc7	Malfoy
vycházet	vycházet	k5eAaImF	vycházet
s	s	k7c7	s
neoblíbeným	oblíbený	k2eNgMnSc7d1	neoblíbený
učitelem	učitel	k1gMnSc7	učitel
Severusem	Severus	k1gMnSc7	Severus
Snapem	Snap	k1gMnSc7	Snap
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
postavit	postavit	k5eAaPmF	postavit
proti	proti	k7c3	proti
všem	všecek	k3xTgMnPc3	všecek
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
škole	škola	k1gFnSc6	škola
(	(	kIx(	(
<g/>
Tajemná	tajemný	k2eAgFnSc1d1	tajemná
komnata	komnata	k1gFnSc1	komnata
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
Ohnivého	ohnivý	k2eAgInSc2d1	ohnivý
poháru	pohár	k1gInSc2	pohár
a	a	k8xC	a
Fénixův	fénixův	k2eAgInSc1d1	fénixův
řád	řád	k1gInSc1	řád
<g/>
)	)	kIx)	)
bojovat	bojovat	k5eAaImF	bojovat
s	s	k7c7	s
mozkomory	mozkomor	k1gInPc7	mozkomor
(	(	kIx(	(
<g/>
Vězeň	vězeň	k1gMnSc1	vězeň
z	z	k7c2	z
Azkabanu	Azkaban	k1gInSc2	Azkaban
<g/>
,	,	kIx,	,
začátek	začátek	k1gInSc1	začátek
Fénixova	fénixův	k2eAgInSc2d1	fénixův
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
Relikvie	relikvie	k1gFnSc2	relikvie
smrti	smrt	k1gFnSc2	smrt
<g/>
)	)	kIx)	)
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
osu	osa	k1gFnSc4	osa
příběhu	příběh	k1gInSc2	příběh
bude	být	k5eAaImBp3nS	být
tvořit	tvořit	k5eAaImF	tvořit
7	[number]	k4	7
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
všechny	všechen	k3xTgFnPc1	všechen
již	již	k9	již
byly	být	k5eAaImAgFnP	být
vydány	vydat	k5eAaPmNgFnP	vydat
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
díl	díl	k1gInSc1	díl
je	být	k5eAaImIp3nS	být
trochu	trochu	k6eAd1	trochu
temnější	temný	k2eAgInSc1d2	temnější
než	než	k8xS	než
předchozí	předchozí	k2eAgMnSc1d1	předchozí
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
věkem	věk	k1gInSc7	věk
hlavního	hlavní	k2eAgMnSc2d1	hlavní
hrdiny	hrdina	k1gMnSc2	hrdina
stoupá	stoupat	k5eAaImIp3nS	stoupat
i	i	k9	i
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
představované	představovaný	k2eAgNnSc4d1	představované
Harryho	Harry	k1gMnSc2	Harry
úhlavním	úhlavní	k2eAgMnSc7d1	úhlavní
nepřítelem	nepřítel	k1gMnSc7	nepřítel
a	a	k8xC	a
vrahem	vrah	k1gMnSc7	vrah
jeho	jeho	k3xOp3gMnPc2	jeho
rodičů	rodič	k1gMnPc2	rodič
-	-	kIx~	-
Lordem	lord	k1gMnSc7	lord
Voldemortem	Voldemort	k1gInSc7	Voldemort
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
jsou	být	k5eAaImIp3nP	být
napsány	napsán	k2eAgFnPc1d1	napsána
er-formou	erorma	k1gFnSc7	er-forma
a	a	k8xC	a
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
Harryho	Harry	k1gMnSc2	Harry
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkami	výjimka	k1gFnPc7	výjimka
v	v	k7c6	v
prvním	první	k4xOgMnSc6	první
<g/>
,	,	kIx,	,
čtvrtém	čtvrtý	k4xOgMnSc6	čtvrtý
<g/>
,	,	kIx,	,
šestém	šestý	k4xOgInSc6	šestý
a	a	k8xC	a
sedmém	sedmý	k4xOgInSc6	sedmý
dílu	díl	k1gInSc6	díl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
je	být	k5eAaImIp3nS	být
srovnávána	srovnávat	k5eAaImNgFnS	srovnávat
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
známých	známý	k2eAgInPc2d1	známý
příběhů	příběh	k1gInPc2	příběh
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Letopisů	letopis	k1gInPc2	letopis
Narnie	Narnie	k1gFnSc2	Narnie
(	(	kIx(	(
<g/>
C.	C.	kA	C.
S.	S.	kA	S.
Lewis	Lewis	k1gFnPc2	Lewis
<g/>
)	)	kIx)	)
a	a	k8xC	a
Pána	pán	k1gMnSc2	pán
prstenů	prsten	k1gInPc2	prsten
(	(	kIx(	(
<g/>
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkien	Tolkina	k1gFnPc2	Tolkina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
je	on	k3xPp3gInPc4	on
zařadit	zařadit	k5eAaPmF	zařadit
i	i	k9	i
mezi	mezi	k7c4	mezi
britské	britský	k2eAgInPc4d1	britský
romány	román	k1gInPc4	román
s	s	k7c7	s
tematikou	tematika	k1gFnSc7	tematika
života	život	k1gInSc2	život
na	na	k7c6	na
internátních	internátní	k2eAgFnPc6d1	internátní
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Části	část	k1gFnPc1	část
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgMnPc6	který
Harry	Harr	k1gInPc4	Harr
přebývá	přebývat	k5eAaImIp3nS	přebývat
u	u	k7c2	u
svých	svůj	k3xOyFgMnPc2	svůj
příbuzných	příbuzný	k1gMnPc2	příbuzný
(	(	kIx(	(
<g/>
Dursleyových	Dursleyův	k2eAgMnPc2d1	Dursleyův
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zase	zase	k9	zase
mohou	moct	k5eAaImIp3nP	moct
připomínat	připomínat	k5eAaImF	připomínat
dílo	dílo	k1gNnSc4	dílo
Roalda	Roald	k1gMnSc2	Roald
Dahla	Dahl	k1gMnSc2	Dahl
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
také	také	k9	také
podobá	podobat	k5eAaImIp3nS	podobat
řadě	řada	k1gFnSc6	řada
filmů	film	k1gInPc2	film
Star	Star	kA	Star
Wars	Wars	k1gInSc1	Wars
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgNnPc6	který
musí	muset	k5eAaImIp3nP	muset
Luke	Luk	k1gFnPc1	Luk
Skywalker	Skywalker	k1gInSc4	Skywalker
žít	žít	k5eAaImF	žít
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
tetou	teta	k1gFnSc7	teta
a	a	k8xC	a
strýcem	strýc	k1gMnSc7	strýc
až	až	k6eAd1	až
do	do	k7c2	do
dovršení	dovršení	k1gNnSc6	dovršení
devatenácti	devatenáct	k4xCc2	devatenáct
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
pravdu	pravda	k1gFnSc4	pravda
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
osudu	osud	k1gInSc6	osud
a	a	k8xC	a
o	o	k7c6	o
hrozbě	hrozba	k1gFnSc6	hrozba
představované	představovaný	k2eAgFnSc6d1	představovaná
Impériem	impérium	k1gNnSc7	impérium
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
prvky	prvek	k1gInPc1	prvek
příběhu	příběh	k1gInSc2	příběh
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
dostaly	dostat	k5eAaPmAgFnP	dostat
do	do	k7c2	do
skutečného	skutečný	k2eAgInSc2d1	skutečný
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Bertíkovy	Bertíkův	k2eAgFnPc1d1	Bertíkova
fazolky	fazolka	k1gFnPc1	fazolka
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
jménem	jméno	k1gNnSc7	jméno
společnost	společnost	k1gFnSc1	společnost
Jelly	Jella	k1gFnPc1	Jella
Belly	bell	k1gInPc1	bell
prodává	prodávat	k5eAaImIp3nS	prodávat
skutečné	skutečný	k2eAgInPc4d1	skutečný
bonbóny	bonbón	k1gInPc4	bonbón
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
osobní	osobní	k2eAgFnSc6d1	osobní
stránce	stránka	k1gFnSc6	stránka
uveřejňuje	uveřejňovat	k5eAaImIp3nS	uveřejňovat
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
některé	některý	k3yIgFnPc4	některý
drobné	drobný	k2eAgFnPc4d1	drobná
informace	informace	k1gFnPc4	informace
a	a	k8xC	a
nápovědy	nápověda	k1gFnPc4	nápověda
o	o	k7c6	o
ději	děj	k1gInSc6	děj
dosud	dosud	k6eAd1	dosud
nevydaných	vydaný	k2eNgInPc2d1	nevydaný
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
vyvrácení	vyvrácení	k1gNnSc4	vyvrácení
teorií	teorie	k1gFnPc2	teorie
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgInPc7	který
přišli	přijít	k5eAaPmAgMnP	přijít
fanoušci	fanoušek	k1gMnPc1	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
mohou	moct	k5eAaImIp3nP	moct
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
dokonce	dokonce	k9	dokonce
hlasovat	hlasovat	k5eAaImF	hlasovat
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
otázku	otázka	k1gFnSc4	otázka
má	mít	k5eAaImIp3nS	mít
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
zodpovědět	zodpovědět	k5eAaPmF	zodpovědět
jako	jako	k8xC	jako
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
nejznámějším	známý	k2eAgInSc7d3	nejznámější
omylem	omyl	k1gInSc7	omyl
fanoušků	fanoušek	k1gMnPc2	fanoušek
je	být	k5eAaImIp3nS	být
postava	postava	k1gFnSc1	postava
Marka	Marek	k1gMnSc2	Marek
Evanse	Evans	k1gMnSc2	Evans
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
letmo	letmo	k6eAd1	letmo
zmíněn	zmínit	k5eAaPmNgInS	zmínit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
pátého	pátý	k4xOgInSc2	pátý
dílu	díl	k1gInSc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gNnSc2	jeho
příjmení	příjmení	k1gNnSc2	příjmení
shoduje	shodovat	k5eAaImIp3nS	shodovat
s	s	k7c7	s
rodným	rodný	k2eAgNnSc7d1	rodné
příjmením	příjmení	k1gNnSc7	příjmení
Harryho	Harry	k1gMnSc2	Harry
matky	matka	k1gFnSc2	matka
(	(	kIx(	(
<g/>
Lily	lít	k5eAaImAgFnP	lít
Evansová	Evansová	k1gFnSc1	Evansová
<g/>
,	,	kIx,	,
provdaná	provdaný	k2eAgFnSc1d1	provdaná
Lily	lít	k5eAaImAgFnP	lít
Potterová	Potterová	k1gFnSc1	Potterová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vedlo	vést	k5eAaImAgNnS	vést
mnoho	mnoho	k6eAd1	mnoho
fanoušků	fanoušek	k1gMnPc2	fanoušek
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mark	Mark	k1gMnSc1	Mark
bude	být	k5eAaImBp3nS	být
hrát	hrát	k5eAaImF	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
závěrečných	závěrečný	k2eAgInPc6d1	závěrečný
dílech	díl	k1gInPc6	díl
série	série	k1gFnSc2	série
<g/>
.	.	kIx.	.
</s>
<s>
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
to	ten	k3xDgNnSc1	ten
vyvrátila	vyvrátit	k5eAaPmAgFnS	vyvrátit
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
už	už	k6eAd1	už
ani	ani	k8xC	ani
neobjeví	objevit	k5eNaPmIp3nS	objevit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
chvíle	chvíle	k1gFnSc2	chvíle
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
o	o	k7c4	o
Harrym	Harrym	k1gInSc4	Harrym
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
8	[number]	k4	8
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
dílů	díl	k1gInPc2	díl
série	série	k1gFnSc2	série
napsala	napsat	k5eAaPmAgFnS	napsat
autorka	autorka	k1gFnSc1	autorka
pod	pod	k7c4	pod
pseudonymy	pseudonym	k1gInPc4	pseudonym
další	další	k2eAgFnPc1d1	další
tři	tři	k4xCgFnPc1	tři
doplňkové	doplňkový	k2eAgFnPc1d1	doplňková
publikace	publikace	k1gFnPc1	publikace
(	(	kIx(	(
<g/>
Bajky	bajka	k1gFnPc1	bajka
barda	bard	k1gMnSc2	bard
Beedleho	Beedle	k1gMnSc2	Beedle
<g/>
,	,	kIx,	,
Famfrpál	Famfrpál	k1gInSc4	Famfrpál
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
věků	věk	k1gInPc2	věk
a	a	k8xC	a
Fantastická	fantastický	k2eAgNnPc4d1	fantastické
zvířata	zvíře	k1gNnPc4	zvíře
a	a	k8xC	a
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
najít	najít	k5eAaPmF	najít
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
britskou	britský	k2eAgFnSc4d1	britská
nadaci	nadace	k1gFnSc4	nadace
Comic	Comic	k1gMnSc1	Comic
Relief	Relief	k1gMnSc1	Relief
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
prodalo	prodat	k5eAaPmAgNnS	prodat
okolo	okolo	k7c2	okolo
450	[number]	k4	450
miliónů	milión	k4xCgInPc2	milión
výtisků	výtisk	k1gInPc2	výtisk
a	a	k8xC	a
knihy	kniha	k1gFnSc2	kniha
o	o	k7c4	o
Harry	Harr	k1gMnPc4	Harr
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
v	v	k7c6	v
ČR	ČR	kA	ČR
obsadily	obsadit	k5eAaPmAgInP	obsadit
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
o	o	k7c4	o
nejoblíbenější	oblíbený	k2eAgFnSc4d3	nejoblíbenější
knihu	kniha	k1gFnSc4	kniha
českých	český	k2eAgMnPc2d1	český
čtenářů	čtenář	k1gMnPc2	čtenář
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
anketa	anketa	k1gFnSc1	anketa
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
knihovnách	knihovna	k1gFnPc6	knihovna
a	a	k8xC	a
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc1	Harr
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
kámen	kámen	k1gInSc1	kámen
mudrců	mudrc	k1gMnPc2	mudrc
<g/>
,	,	kIx,	,
v	v	k7c6	v
originále	originál	k1gInSc6	originál
jako	jako	k8xC	jako
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
and	and	k?	and
the	the	k?	the
Philosopher	Philosophra	k1gFnPc2	Philosophra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Stone	ston	k1gInSc5	ston
</s>
<s>
Vydání	vydání	k1gNnSc1	vydání
originálu	originál	k1gInSc2	originál
<g/>
:	:	kIx,	:
26	[number]	k4	26
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
1997	[number]	k4	1997
</s>
<s>
Vydání	vydání	k1gNnSc2	vydání
překladu	překlad	k1gInSc6	překlad
<g/>
:	:	kIx,	:
únor	únor	k1gInSc4	únor
2000	[number]	k4	2000
</s>
<s>
Poznámka	poznámka	k1gFnSc1	poznámka
<g/>
:	:	kIx,	:
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
byl	být	k5eAaImAgInS	být
název	název	k1gInSc1	název
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
Harry	Harra	k1gFnPc4	Harra
Potter	Pottra	k1gFnPc2	Pottra
and	and	k?	and
the	the	k?	the
Sorcerer	Sorcerer	k1gInSc1	Sorcerer
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Stone	ston	k1gInSc5	ston
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tamější	tamější	k2eAgMnSc1d1	tamější
vydavatel	vydavatel	k1gMnSc1	vydavatel
předpokládal	předpokládat	k5eAaImAgMnS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
americké	americký	k2eAgFnPc1d1	americká
děti	dítě	k1gFnPc1	dítě
legendu	legenda	k1gFnSc4	legenda
o	o	k7c6	o
kameni	kámen	k1gInSc6	kámen
mudrců	mudrc	k1gMnPc2	mudrc
neznají	neznat	k5eAaImIp3nP	neznat
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc1	Harr
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
tajemná	tajemný	k2eAgFnSc1d1	tajemná
komnata	komnata	k1gFnSc1	komnata
<g/>
,	,	kIx,	,
v	v	k7c6	v
originále	originál	k1gInSc6	originál
jako	jako	k8xS	jako
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
and	and	k?	and
the	the	k?	the
Chamber	Chamber	k1gInSc1	Chamber
of	of	k?	of
Secrets	Secrets	k1gInSc1	Secrets
Vydání	vydání	k1gNnSc2	vydání
originálu	originál	k1gInSc2	originál
<g/>
:	:	kIx,	:
1998	[number]	k4	1998
Vydání	vydání	k1gNnSc1	vydání
překladu	překlad	k1gInSc2	překlad
<g/>
:	:	kIx,	:
říjen	říjen	k1gInSc1	říjen
2000	[number]	k4	2000
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
vězeň	vězeň	k1gMnSc1	vězeň
z	z	k7c2	z
Azkabanu	Azkaban	k1gInSc2	Azkaban
<g/>
,	,	kIx,	,
v	v	k7c6	v
originále	originál	k1gInSc6	originál
jako	jako	k8xC	jako
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
and	and	k?	and
the	the	k?	the
Prisoner	Prisoner	k1gInSc1	Prisoner
of	of	k?	of
Azkaban	Azkabany	k1gInPc2	Azkabany
Vydání	vydání	k1gNnPc2	vydání
originálu	originál	k1gInSc2	originál
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
1999	[number]	k4	1999
Vydání	vydání	k1gNnSc1	vydání
překladu	překlad	k1gInSc2	překlad
<g/>
:	:	kIx,	:
2000	[number]	k4	2000
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
Ohnivý	ohnivý	k2eAgInSc4d1	ohnivý
pohár	pohár	k1gInSc4	pohár
<g/>
,	,	kIx,	,
v	v	k7c6	v
originále	originál	k1gInSc6	originál
jako	jako	k8xC	jako
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
and	and	k?	and
the	the	k?	the
Goblet	Goblet	k1gInSc1	Goblet
of	of	k?	of
Fire	Fir	k1gInSc2	Fir
Vydání	vydání	k1gNnPc2	vydání
originálu	originál	k1gInSc2	originál
<g/>
:	:	kIx,	:
2000	[number]	k4	2000
Vydání	vydání	k1gNnSc1	vydání
překladu	překlad	k1gInSc2	překlad
<g/>
:	:	kIx,	:
prosinec	prosinec	k1gInSc1	prosinec
2001	[number]	k4	2001
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
Fénixův	fénixův	k2eAgInSc4d1	fénixův
řád	řád	k1gInSc4	řád
<g/>
,	,	kIx,	,
v	v	k7c6	v
originále	originál	k1gInSc6	originál
jako	jako	k8xS	jako
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
and	and	k?	and
the	the	k?	the
Order	Order	k1gInSc1	Order
of	of	k?	of
<g />
.	.	kIx.	.
</s>
<s>
the	the	k?	the
Phoenix	Phoenix	k1gInSc1	Phoenix
Čas	čas	k1gInSc1	čas
děje	dít	k5eAaImIp3nS	dít
<g/>
:	:	kIx,	:
1995	[number]	k4	1995
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
Vydání	vydání	k1gNnSc1	vydání
originálu	originál	k1gInSc2	originál
<g/>
:	:	kIx,	:
21	[number]	k4	21
<g/>
.	.	kIx.	.
červen	červen	k1gInSc1	červen
2003	[number]	k4	2003
Vydání	vydání	k1gNnPc2	vydání
překladu	překlad	k1gInSc2	překlad
<g/>
:	:	kIx,	:
29	[number]	k4	29
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2004	[number]	k4	2004
Harry	Harr	k1gInPc7	Harr
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
princ	princ	k1gMnSc1	princ
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
v	v	k7c6	v
originále	originál	k1gInSc6	originál
jako	jako	k8xS	jako
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
and	and	k?	and
the	the	k?	the
Half-Blood	Half-Blood	k1gInSc4	Half-Blood
Prince	princ	k1gMnSc2	princ
Vydání	vydání	k1gNnSc2	vydání
originálu	originál	k1gInSc2	originál
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
16	[number]	k4	16
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
2005	[number]	k4	2005
Vydání	vydání	k1gNnPc2	vydání
překladu	překlad	k1gInSc2	překlad
<g/>
:	:	kIx,	:
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosinec	prosinec	k1gInSc1	prosinec
2005	[number]	k4	2005
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
relikvie	relikvie	k1gFnSc2	relikvie
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
v	v	k7c6	v
originále	originál	k1gInSc6	originál
jako	jako	k8xS	jako
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
and	and	k?	and
the	the	k?	the
Deathly	Deathly	k1gFnPc4	Deathly
Hallows	Hallows	k1gInSc4	Hallows
Vydání	vydání	k1gNnSc1	vydání
originálu	originál	k1gInSc2	originál
<g/>
:	:	kIx,	:
21	[number]	k4	21
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
2007	[number]	k4	2007
Vydání	vydání	k1gNnPc2	vydání
překladu	překlad	k1gInSc2	překlad
<g/>
:	:	kIx,	:
31	[number]	k4	31
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
2008	[number]	k4	2008
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
prokleté	prokletý	k2eAgNnSc1d1	prokleté
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
v	v	k7c6	v
originále	originál	k1gInSc6	originál
jako	jako	k8xS	jako
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
and	and	k?	and
the	the	k?	the
Cursed	Cursed	k1gMnSc1	Cursed
Child	Child	k1gMnSc1	Child
Vydání	vydání	k1gNnSc2	vydání
originálu	originál	k1gInSc2	originál
<g/>
:	:	kIx,	:
31	[number]	k4	31
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
2016	[number]	k4	2016
Vydání	vydání	k1gNnPc2	vydání
překladu	překlad	k1gInSc2	překlad
<g/>
:	:	kIx,	:
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2016	[number]	k4	2016
Knihy	kniha	k1gFnSc2	kniha
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
tak	tak	k9	tak
populární	populární	k2eAgFnPc1d1	populární
<g/>
,	,	kIx,	,
že	že	k8xS	že
řada	řada	k1gFnSc1	řada
knihkupectví	knihkupectví	k1gNnSc2	knihkupectví
pořádá	pořádat	k5eAaImIp3nS	pořádat
půlnoční	půlnoční	k2eAgInSc4d1	půlnoční
prodej	prodej	k1gInSc4	prodej
v	v	k7c4	v
den	den	k1gInSc4	den
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
vydání	vydání	k1gNnSc2	vydání
dalšího	další	k2eAgInSc2d1	další
dílu	díl	k1gInSc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
byly	být	k5eAaImAgFnP	být
přeloženy	přeložit	k5eAaPmNgFnP	přeložit
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
latinská	latinský	k2eAgFnSc1d1	Latinská
či	či	k8xC	či
starořecká	starořecký	k2eAgFnSc1d1	starořecká
verze	verze	k1gFnSc1	verze
(	(	kIx(	(
<g/>
překlad	překlad	k1gInSc1	překlad
Fénixova	fénixův	k2eAgInSc2d1	fénixův
řádu	řád	k1gInSc2	řád
je	být	k5eAaImIp3nS	být
prý	prý	k9	prý
nejdelší	dlouhý	k2eAgFnSc7d3	nejdelší
knihou	kniha	k1gFnSc7	kniha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
jazyku	jazyk	k1gInSc6	jazyk
za	za	k7c2	za
posledních	poslední	k2eAgNnPc2d1	poslední
1000	[number]	k4	1000
let	léto	k1gNnPc2	léto
uveřejněna	uveřejnit	k5eAaPmNgFnS	uveřejnit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
speciální	speciální	k2eAgFnPc1d1	speciální
americké	americký	k2eAgFnPc1d1	americká
verze	verze	k1gFnPc1	verze
s	s	k7c7	s
lehce	lehko	k6eAd1	lehko
upraveným	upravený	k2eAgInSc7d1	upravený
slovníkem	slovník	k1gInSc7	slovník
u	u	k7c2	u
výrazů	výraz	k1gInPc2	výraz
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
a	a	k8xC	a
americké	americký	k2eAgFnSc6d1	americká
angličtině	angličtina	k1gFnSc6	angličtina
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nás	my	k3xPp1nPc2	my
jsou	být	k5eAaImIp3nP	být
autory	autor	k1gMnPc4	autor
oficiálního	oficiální	k2eAgInSc2d1	oficiální
českého	český	k2eAgInSc2d1	český
překladu	překlad	k1gInSc2	překlad
prvních	první	k4xOgFnPc2	první
7	[number]	k4	7
knih	kniha	k1gFnPc2	kniha
bratři	bratr	k1gMnPc1	bratr
Pavel	Pavel	k1gMnSc1	Pavel
a	a	k8xC	a
Vladimír	Vladimír	k1gMnSc1	Vladimír
Medkovi	Medek	k1gMnSc3	Medek
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc4d1	poslední
knihu	kniha	k1gFnSc4	kniha
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Petr	Petr	k1gMnSc1	Petr
Eliáš	Eliáš	k1gMnSc1	Eliáš
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
kniha	kniha	k1gFnSc1	kniha
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
prokleté	prokletý	k2eAgNnSc1d1	prokleté
dítě	dítě	k1gNnSc1	dítě
je	být	k5eAaImIp3nS	být
napsána	napsat	k5eAaPmNgFnS	napsat
formou	forma	k1gFnSc7	forma
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
napsat	napsat	k5eAaPmF	napsat
o	o	k7c4	o
Harrym	Harrym	k1gInSc4	Harrym
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
další	další	k2eAgMnPc1d1	další
knihu	kniha	k1gFnSc4	kniha
i	i	k9	i
po	po	k7c4	po
vydání	vydání	k1gNnSc4	vydání
závěrečného	závěrečný	k2eAgInSc2d1	závěrečný
sedmého	sedmý	k4xOgInSc2	sedmý
dílu	díl	k1gInSc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
o	o	k7c4	o
jakousi	jakýsi	k3yIgFnSc4	jakýsi
encyklopedii	encyklopedie	k1gFnSc4	encyklopedie
kouzelnického	kouzelnický	k2eAgInSc2d1	kouzelnický
světa	svět	k1gInSc2	svět
s	s	k7c7	s
informacemi	informace	k1gFnPc7	informace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
příliš	příliš	k6eAd1	příliš
podružné	podružný	k2eAgInPc1d1	podružný
<g/>
,	,	kIx,	,
než	než	k8xS	než
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vešly	vejít	k5eAaPmAgFnP	vejít
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
některého	některý	k3yIgInSc2	některý
z	z	k7c2	z
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Oznámila	oznámit	k5eAaPmAgFnS	oznámit
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
nenapíše	napsat	k5eNaPmIp3nS	napsat
žádný	žádný	k3yNgInSc4	žádný
prequel	prequel	k1gInSc4	prequel
k	k	k7c3	k
příběhu	příběh	k1gInSc3	příběh
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
všechny	všechen	k3xTgFnPc4	všechen
podstatné	podstatný	k2eAgFnPc4d1	podstatná
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
minulosti	minulost	k1gFnSc6	minulost
kouzelnického	kouzelnický	k2eAgInSc2d1	kouzelnický
světa	svět	k1gInSc2	svět
již	již	k6eAd1	již
stihla	stihnout	k5eAaPmAgFnS	stihnout
uvést	uvést	k5eAaPmF	uvést
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
sérii	série	k1gFnSc6	série
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc1	Harr
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
kámen	kámen	k1gInSc1	kámen
mudrců	mudrc	k1gMnPc2	mudrc
Pevná	pevný	k2eAgFnSc1d1	pevná
vazba	vazba	k1gFnSc1	vazba
<g/>
:	:	kIx,	:
6,1	[number]	k4	6,1
milionů	milion	k4xCgInPc2	milion
Paperback	paperback	k1gInSc1	paperback
<g/>
:	:	kIx,	:
10,9	[number]	k4	10,9
milionů	milion	k4xCgInPc2	milion
Harry	Harr	k1gInPc1	Harr
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
tajemná	tajemný	k2eAgFnSc1d1	tajemná
komnata	komnata	k1gFnSc1	komnata
Pevná	pevný	k2eAgFnSc1d1	pevná
vazba	vazba	k1gFnSc1	vazba
<g/>
:	:	kIx,	:
7,1	[number]	k4	7,1
milionů	milion	k4xCgInPc2	milion
Paperback	paperback	k1gInSc1	paperback
<g/>
:	:	kIx,	:
7,5	[number]	k4	7,5
milionů	milion	k4xCgInPc2	milion
Harry	Harr	k1gInPc7	Harr
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
vězeň	vězeň	k1gMnSc1	vězeň
z	z	k7c2	z
Azkabanu	Azkaban	k1gInSc2	Azkaban
Pevná	pevný	k2eAgFnSc1d1	pevná
vazba	vazba	k1gFnSc1	vazba
<g/>
:	:	kIx,	:
7,6	[number]	k4	7,6
milionů	milion	k4xCgInPc2	milion
Paperback	paperback	k1gInSc1	paperback
<g/>
:	:	kIx,	:
5,2	[number]	k4	5,2
milionů	milion	k4xCgInPc2	milion
Harry	Harr	k1gInPc7	Harr
<g />
.	.	kIx.	.
</s>
<s>
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
ohnivý	ohnivý	k2eAgInSc1d1	ohnivý
pohár	pohár	k1gInSc1	pohár
Pevná	pevný	k2eAgFnSc1d1	pevná
vazba	vazba	k1gFnSc1	vazba
<g/>
:	:	kIx,	:
8,9	[number]	k4	8,9
milionů	milion	k4xCgInPc2	milion
Paperback	paperback	k1gInSc1	paperback
<g/>
:	:	kIx,	:
3,4	[number]	k4	3,4
milionů	milion	k4xCgInPc2	milion
Harry	Harr	k1gInPc1	Harr
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Fénixův	fénixův	k2eAgInSc1d1	fénixův
řád	řád	k1gInSc1	řád
Pevná	pevný	k2eAgFnSc1d1	pevná
vazba	vazba	k1gFnSc1	vazba
<g/>
:	:	kIx,	:
12,2	[number]	k4	12,2
milionů	milion	k4xCgInPc2	milion
Paperback	paperback	k1gInSc1	paperback
<g/>
:	:	kIx,	:
1,6	[number]	k4	1,6
milionů	milion	k4xCgInPc2	milion
Harry	Harr	k1gInPc7	Harr
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
princ	princ	k1gMnSc1	princ
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
6,9	[number]	k4	6,9
milionů	milion	k4xCgInPc2	milion
(	(	kIx(	(
<g/>
během	během	k7c2	během
prvních	první	k4xOgNnPc2	první
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
11	[number]	k4	11
milionů	milion	k4xCgInPc2	milion
(	(	kIx(	(
<g/>
k	k	k7c3	k
21.9	[number]	k4	21.9
<g/>
.2005	.2005	k4	.2005
<g/>
)	)	kIx)	)
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
relikvie	relikvie	k1gFnSc1	relikvie
smrti	smrt	k1gFnSc2	smrt
11	[number]	k4	11
milionů	milion	k4xCgInPc2	milion
(	(	kIx(	(
<g/>
během	během	k7c2	během
prvních	první	k4xOgNnPc2	první
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
Po	po	k7c6	po
velkém	velký	k2eAgInSc6d1	velký
úspěchu	úspěch	k1gInSc6	úspěch
knih	kniha	k1gFnPc2	kniha
byly	být	k5eAaImAgFnP	být
rovněž	rovněž	k9	rovněž
natočeny	natočen	k2eAgInPc4d1	natočen
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgMnPc4	který
autorka	autorka	k1gFnSc1	autorka
přímo	přímo	k6eAd1	přímo
dohlížela	dohlížet	k5eAaImAgFnS	dohlížet
<g/>
.	.	kIx.	.
</s>
<s>
Trvala	trvat	k5eAaImAgFnS	trvat
také	také	k9	také
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
všichni	všechen	k3xTgMnPc1	všechen
herci	herec	k1gMnPc1	herec
byli	být	k5eAaImAgMnP	být
Angličané	Angličan	k1gMnPc1	Angličan
<g/>
,	,	kIx,	,
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
hrané	hraný	k2eAgInPc4d1	hraný
filmy	film	k1gInPc4	film
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
nich	on	k3xPp3gInPc6	on
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
dodrženy	dodržet	k5eAaPmNgFnP	dodržet
události	událost	k1gFnPc1	událost
a	a	k8xC	a
prostředí	prostředí	k1gNnSc1	prostředí
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
díle	díl	k1gInSc6	díl
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
rozdíly	rozdíl	k1gInPc1	rozdíl
větší	veliký	k2eAgInPc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Harryho	Harry	k1gMnSc4	Harry
Pottera	Potter	k1gMnSc4	Potter
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
dílech	díl	k1gInPc6	díl
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
Daniel	Daniel	k1gMnSc1	Daniel
Radcliffe	Radcliff	k1gInSc5	Radcliff
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
filmů	film	k1gInPc2	film
o	o	k7c4	o
Harrym	Harrym	k1gInSc4	Harrym
Potterovi	Potter	k1gMnSc3	Potter
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vysoká	vysoký	k2eAgFnSc1d1	vysoká
a	a	k8xC	a
vedle	vedle	k7c2	vedle
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
dílů	díl	k1gInPc2	díl
Pána	pán	k1gMnSc4	pán
Prstenů	prsten	k1gInPc2	prsten
<g/>
,	,	kIx,	,
Hvězdných	hvězdný	k2eAgFnPc2d1	hvězdná
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
Pirátů	pirát	k1gMnPc2	pirát
z	z	k7c2	z
Karibiku	Karibik	k1gInSc2	Karibik
a	a	k8xC	a
filmů	film	k1gInPc2	film
Titanic	Titanic	k1gInSc1	Titanic
a	a	k8xC	a
Avatar	Avatar	k1gInSc1	Avatar
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
nejúspěšnějším	úspěšný	k2eAgInPc3d3	nejúspěšnější
filmům	film	k1gInPc3	film
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
díl	díl	k1gInSc1	díl
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2004	[number]	k4	2004
obsadil	obsadit	k5eAaPmAgMnS	obsadit
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
s	s	k7c7	s
celosvětovou	celosvětový	k2eAgFnSc7d1	celosvětová
tržbou	tržba	k1gFnSc7	tržba
968,8	[number]	k4	968,8
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
pak	pak	k6eAd1	pak
osmé	osmý	k4xOgNnSc4	osmý
místo	místo	k1gNnSc4	místo
s	s	k7c7	s
tržbou	tržba	k1gFnSc7	tržba
866,3	[number]	k4	866,3
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
státech	stát	k1gInPc6	stát
stále	stále	k6eAd1	stále
v	v	k7c6	v
distribuci	distribuce	k1gFnSc6	distribuce
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
čtrnácté	čtrnáctý	k4xOgNnSc4	čtrnáctý
místo	místo	k1gNnSc4	místo
s	s	k7c7	s
tržbou	tržba	k1gFnSc7	tržba
789,5	[number]	k4	789,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Poznámky	poznámka	k1gFnPc1	poznámka
<g/>
:	:	kIx,	:
U	u	k7c2	u
dílu	díl	k1gInSc2	díl
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
Kámen	kámen	k1gInSc4	kámen
mudrců	mudrc	k1gMnPc2	mudrc
byly	být	k5eAaImAgFnP	být
všechny	všechen	k3xTgFnPc1	všechen
scény	scéna	k1gFnPc1	scéna
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
byl	být	k5eAaImAgInS	být
zmíněn	zmíněn	k2eAgInSc1d1	zmíněn
Kámen	kámen	k1gInSc1	kámen
mudrců	mudrc	k1gMnPc2	mudrc
<g/>
,	,	kIx,	,
točeny	točen	k2eAgFnPc1d1	točena
dvakrát	dvakrát	k6eAd1	dvakrát
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
v	v	k7c6	v
USA	USA	kA	USA
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
kámen	kámen	k1gInSc1	kámen
nazýván	nazývat	k5eAaImNgInS	nazývat
"	"	kIx"	"
<g/>
Sorcerer	Sorcerer	k1gInSc1	Sorcerer
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Stone	ston	k1gInSc5	ston
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
natáčen	natáčet	k5eAaImNgInS	natáčet
s	s	k7c7	s
americkými	americký	k2eAgMnPc7d1	americký
herci	herec	k1gMnPc7	herec
v	v	k7c6	v
USA	USA	kA	USA
v	v	k7c6	v
poněkud	poněkud	k6eAd1	poněkud
modernějším	moderní	k2eAgNnSc6d2	modernější
pojetí	pojetí	k1gNnSc6	pojetí
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
autorka	autorka	k1gFnSc1	autorka
jednoznačně	jednoznačně	k6eAd1	jednoznačně
zamítla	zamítnout	k5eAaPmAgFnS	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgInSc3	ten
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
nakonec	nakonec	k6eAd1	nakonec
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
pracovat	pracovat	k5eAaImF	pracovat
Steven	Steven	k2eAgMnSc1d1	Steven
Spielberg	Spielberg	k1gMnSc1	Spielberg
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
posledního	poslední	k2eAgInSc2d1	poslední
dílu	díl	k1gInSc2	díl
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
co	co	k9	co
nejvíce	nejvíce	k6eAd1	nejvíce
důležitých	důležitý	k2eAgInPc2d1	důležitý
faktů	fakt	k1gInPc2	fakt
a	a	k8xC	a
událostí	událost	k1gFnPc2	událost
z	z	k7c2	z
knižní	knižní	k2eAgFnSc2d1	knižní
předlohy	předloha	k1gFnSc2	předloha
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
filmy	film	k1gInPc1	film
celkem	celkem	k6eAd1	celkem
vydělaly	vydělat	k5eAaPmAgInP	vydělat
7,723	[number]	k4	7,723
<g/>
5	[number]	k4	5
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
–	–	k?	–
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
druhou	druhý	k4xOgFnSc4	druhý
nejúspěšnější	úspěšný	k2eAgFnSc4d3	nejúspěšnější
filmovou	filmový	k2eAgFnSc4d1	filmová
franšízu	franšíza	k1gFnSc4	franšíza
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
a	a	k8xC	a
poslední	poslední	k2eAgInSc4d1	poslední
díl	díl	k1gInSc4	díl
byly	být	k5eAaImAgInP	být
komerčně	komerčně	k6eAd1	komerčně
nejúspěšnější	úspěšný	k2eAgInPc1d3	nejúspěšnější
filmy	film	k1gInPc1	film
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
vyšly	vyjít	k5eAaPmAgFnP	vyjít
<g/>
,	,	kIx,	,
Poslední	poslední	k2eAgInSc1d1	poslední
díl	díl	k1gInSc1	díl
je	být	k5eAaImIp3nS	být
8	[number]	k4	8
<g/>
.	.	kIx.	.
komerčně	komerčně	k6eAd1	komerčně
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
filmem	film	k1gInSc7	film
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
fantastickým	fantastický	k2eAgInSc7d1	fantastický
filmem	film	k1gInSc7	film
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
"	"	kIx"	"
<g/>
pouze	pouze	k6eAd1	pouze
<g/>
"	"	kIx"	"
107	[number]	k4	107
<g/>
.	.	kIx.	.
nejvýnosnějším	výnosný	k2eAgInSc7d3	nejvýnosnější
filmem	film	k1gInSc7	film
při	při	k7c6	při
započtení	započtení	k1gNnSc6	započtení
inflace	inflace	k1gFnSc2	inflace
<g/>
.	.	kIx.	.
</s>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Radcliffe	Radcliff	k1gInSc5	Radcliff
-	-	kIx~	-
Harry	Harr	k1gInPc4	Harr
Potter	Potter	k1gInSc1	Potter
Emma	Emma	k1gFnSc1	Emma
Watson	Watson	k1gMnSc1	Watson
-	-	kIx~	-
Hermiona	Hermion	k1gMnSc2	Hermion
Grangerová	Grangerová	k1gFnSc1	Grangerová
Rupert	Rupert	k1gMnSc1	Rupert
Grint	Grint	k1gMnSc1	Grint
-	-	kIx~	-
Ronald	Ronald	k1gMnSc1	Ronald
Weasley	Weaslea	k1gFnSc2	Weaslea
Bonnie	Bonnie	k1gFnSc1	Bonnie
Wright	Wright	k1gMnSc1	Wright
-	-	kIx~	-
Ginny	Ginna	k1gMnSc2	Ginna
Weasleyová	Weasleyový	k2eAgFnSc1d1	Weasleyová
Katie	Katie	k1gFnSc1	Katie
Leung	Leunga	k1gFnPc2	Leunga
-	-	kIx~	-
Cho	cho	k0	cho
Changová	Changový	k2eAgFnSc1d1	Changová
Evanna	Evanna	k1gFnSc1	Evanna
Lynch	Lynch	k1gMnSc1	Lynch
-	-	kIx~	-
Lenka	Lenka	k1gFnSc1	Lenka
Láskorádová	Láskorádový	k2eAgFnSc1d1	Láskorádová
(	(	kIx(	(
<g/>
Luna	luna	k1gFnSc1	luna
Lovegood	Lovegood	k1gInSc1	Lovegood
<g/>
)	)	kIx)	)
Jason	Jason	k1gInSc1	Jason
Isaacs	Isaacs	k1gInSc1	Isaacs
-	-	kIx~	-
Lucius	Lucius	k1gMnSc1	Lucius
Malfoy	Malfoa	k1gFnSc2	Malfoa
Tom	Tom	k1gMnSc1	Tom
Felton	Felton	k1gInSc1	Felton
-	-	kIx~	-
Draco	Draco	k1gMnSc1	Draco
Malfoy	Malfoa	k1gFnSc2	Malfoa
Alan	Alan	k1gMnSc1	Alan
Rickman	Rickman	k1gMnSc1	Rickman
-	-	kIx~	-
Severus	Severus	k1gMnSc1	Severus
<g />
.	.	kIx.	.
</s>
<s>
Snape	Snapat	k5eAaPmIp3nS	Snapat
Robbie	Robbie	k1gFnSc1	Robbie
Coltrane	Coltran	k1gInSc5	Coltran
-	-	kIx~	-
Rubeus	Rubeus	k1gInSc1	Rubeus
Hagrid	Hagrid	k1gInSc1	Hagrid
Maggie	Maggie	k1gFnSc1	Maggie
Smith	Smith	k1gMnSc1	Smith
-	-	kIx~	-
Minerva	Minerva	k1gFnSc1	Minerva
McGonagallová	McGonagallová	k1gFnSc1	McGonagallová
David	David	k1gMnSc1	David
Bradley	Bradlea	k1gMnSc2	Bradlea
-	-	kIx~	-
Argus	Argus	k1gMnSc1	Argus
Filch	Filch	k1gMnSc1	Filch
Matthew	Matthew	k1gMnSc1	Matthew
Lewis	Lewis	k1gFnSc1	Lewis
-	-	kIx~	-
Neville	Neville	k1gFnSc1	Neville
Longbottom	Longbottom	k1gInSc1	Longbottom
Helena	Helena	k1gFnSc1	Helena
Bonham	Bonham	k1gInSc4	Bonham
Carter	Carter	k1gInSc1	Carter
-	-	kIx~	-
Bellatrix	Bellatrix	k1gInSc1	Bellatrix
Lestrangeová	Lestrangeová	k1gFnSc1	Lestrangeová
David	David	k1gMnSc1	David
Thewlis	Thewlis	k1gInSc1	Thewlis
-	-	kIx~	-
Remus	Remus	k1gMnSc1	Remus
Lupin	lupina	k1gFnPc2	lupina
Julie	Julie	k1gFnSc2	Julie
Walters	Walters	k1gInSc1	Walters
-	-	kIx~	-
Molly	Moll	k1gMnPc4	Moll
Weasleyová	Weasleyová	k1gFnSc1	Weasleyová
Timothy	Timotha	k1gFnSc2	Timotha
Spall	Spall	k1gMnSc1	Spall
-	-	kIx~	-
Peter	Peter	k1gMnSc1	Peter
Pettigrew	Pettigrew	k1gFnSc2	Pettigrew
Jessie	Jessie	k1gFnSc1	Jessie
Cave	Cave	k1gFnSc1	Cave
-	-	kIx~	-
Levandule	levandule	k1gFnSc1	levandule
Brownová	Brownová	k1gFnSc1	Brownová
Helen	Helena	k1gFnPc2	Helena
McCror	McCrora	k1gFnPc2	McCrora
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Narcisa	Narcisa	k1gFnSc1	Narcisa
Malfoyová	Malfoyová	k1gFnSc1	Malfoyová
Warwick	Warwick	k1gInSc4	Warwick
Davis	Davis	k1gInSc1	Davis
-	-	kIx~	-
Filius	Filius	k1gInSc1	Filius
Kratiknot	kratiknot	k1gInSc1	kratiknot
Robert	Robert	k1gMnSc1	Robert
Pattinson	Pattinson	k1gMnSc1	Pattinson
-	-	kIx~	-
Cedric	Cedric	k1gMnSc1	Cedric
Diggory	Diggora	k1gFnSc2	Diggora
James	James	k1gMnSc1	James
Phelps	Phelps	k1gInSc1	Phelps
-	-	kIx~	-
Fred	Fred	k1gMnSc1	Fred
Weasley	Weaslea	k1gFnSc2	Weaslea
Oliver	Oliver	k1gMnSc1	Oliver
Phelps	Phelps	k1gInSc1	Phelps
-	-	kIx~	-
George	Georg	k1gMnSc2	Georg
Weasley	Weaslea	k1gMnSc2	Weaslea
Natalia	Natalius	k1gMnSc2	Natalius
Tena	Tenus	k1gMnSc2	Tenus
-	-	kIx~	-
Nymfadora	Nymfadora	k1gFnSc1	Nymfadora
Tonksová	Tonksová	k1gFnSc1	Tonksová
Gary	Gara	k1gFnSc2	Gara
Oldman	Oldman	k1gMnSc1	Oldman
-	-	kIx~	-
Sirius	Sirius	k1gMnSc1	Sirius
Black	Black	k1gMnSc1	Black
Mark	Mark	k1gMnSc1	Mark
Williams	Williams	k1gInSc1	Williams
-	-	kIx~	-
Arthur	Arthur	k1gMnSc1	Arthur
Weasley	Weaslea	k1gFnSc2	Weaslea
Hero	Hero	k1gMnSc1	Hero
Fiennes-Tiffin	Fiennes-Tiffin	k1gMnSc1	Fiennes-Tiffin
-	-	kIx~	-
Tom	Tom	k1gMnSc1	Tom
Raddle	Raddle	k1gMnSc1	Raddle
-	-	kIx~	-
v	v	k7c6	v
11	[number]	k4	11
letech	léto	k1gNnPc6	léto
Frank	Frank	k1gMnSc1	Frank
Dillane	Dillan	k1gMnSc5	Dillan
-	-	kIx~	-
teenager	teenager	k1gMnSc1	teenager
<g />
.	.	kIx.	.
</s>
<s>
Tom	Tom	k1gMnSc1	Tom
Raddle	Raddle	k1gMnSc1	Raddle
Scarlett	Scarlett	k1gMnSc1	Scarlett
Byrne	Byrn	k1gInSc5	Byrn
-	-	kIx~	-
Pansy	Pansa	k1gFnSc2	Pansa
Parkinsonová	Parkinsonový	k2eAgFnSc1d1	Parkinsonová
Gemma	Gemma	k1gFnSc1	Gemma
Jones	Jones	k1gMnSc1	Jones
-	-	kIx~	-
madam	madam	k1gFnSc1	madam
Pomfreyová	Pomfreyový	k2eAgFnSc1d1	Pomfreyová
Freddie	Freddie	k1gFnSc1	Freddie
Stroma	stroma	k1gNnSc2	stroma
-	-	kIx~	-
Cormac	Cormac	k1gFnSc1	Cormac
McLaggen	McLaggen	k1gInSc1	McLaggen
Alfred	Alfred	k1gMnSc1	Alfred
Enoch	Enoch	k1gMnSc1	Enoch
-	-	kIx~	-
Dean	Dean	k1gMnSc1	Dean
Thomas	Thomas	k1gMnSc1	Thomas
Devon	devon	k1gInSc4	devon
Murray	Murraa	k1gFnSc2	Murraa
-	-	kIx~	-
Seamus	Seamus	k1gInSc1	Seamus
Finnigan	Finnigan	k1gInSc1	Finnigan
Georgina	Georgina	k1gFnSc1	Georgina
Leonidas	Leonidas	k1gMnSc1	Leonidas
-	-	kIx~	-
Katie	Katie	k1gFnSc1	Katie
Bellová	Bellová	k1gFnSc1	Bellová
Isabella	Isabella	k1gFnSc1	Isabella
Laughland	Laughland	k1gInSc1	Laughland
-	-	kIx~	-
Leana	Leana	k1gFnSc1	Leana
Afshan	Afshan	k1gMnSc1	Afshan
Azad	Azad	k1gMnSc1	Azad
-	-	kIx~	-
Padma	Padma	k1gFnSc1	Padma
Patilová	Patilový	k2eAgFnSc1d1	Patilová
Shefali	Shefali	k1gFnSc1	Shefali
Chowdhury	Chowdhura	k1gFnSc2	Chowdhura
-	-	kIx~	-
Parvati	Parvati	k1gFnSc1	Parvati
Patilová	Patilový	k2eAgFnSc1d1	Patilová
Anna	Anna	k1gFnSc1	Anna
Shaffer	Shaffer	k1gMnSc1	Shaffer
-	-	kIx~	-
Romilda	Romilda	k1gMnSc1	Romilda
<g />
.	.	kIx.	.
</s>
<s>
Vaneová	Vaneová	k1gFnSc1	Vaneová
Richard	Richard	k1gMnSc1	Richard
Harris	Harris	k1gInSc1	Harris
-	-	kIx~	-
Albus	Albus	k1gMnSc1	Albus
Brumbál	brumbál	k1gMnSc1	brumbál
(	(	kIx(	(
<g/>
do	do	k7c2	do
druhého	druhý	k4xOgInSc2	druhý
dílu	díl	k1gInSc2	díl
<g/>
)	)	kIx)	)
Michael	Michael	k1gMnSc1	Michael
Gambon	Gambon	k1gMnSc1	Gambon
-	-	kIx~	-
Albus	Albus	k1gMnSc1	Albus
Brumbál	brumbál	k1gMnSc1	brumbál
(	(	kIx(	(
<g/>
od	od	k7c2	od
třetího	třetí	k4xOgInSc2	třetí
dílu	díl	k1gInSc2	díl
<g/>
)	)	kIx)	)
Ralph	Ralph	k1gMnSc1	Ralph
Fiennes	Fiennes	k1gMnSc1	Fiennes
-	-	kIx~	-
Lord	lord	k1gMnSc1	lord
Voldemort	Voldemort	k1gInSc4	Voldemort
Clémence	Clémence	k1gFnSc2	Clémence
Poésy	Poésa	k1gFnSc2	Poésa
-	-	kIx~	-
Fleur	Fleur	k1gMnSc1	Fleur
Delacour	Delacour	k1gMnSc1	Delacour
Kenneth	Kenneth	k1gMnSc1	Kenneth
Branagh	Branagh	k1gMnSc1	Branagh
-	-	kIx~	-
Zlatoslav	Zlatoslav	k1gMnSc1	Zlatoslav
Lockhart	Lockharta	k1gFnPc2	Lockharta
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
and	and	k?	and
the	the	k?	the
Half-blood	Halflood	k1gInSc1	Half-blood
Prince	princ	k1gMnSc2	princ
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc7	první
knihou	kniha	k1gFnSc7	kniha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
vydána	vydat	k5eAaPmNgFnS	vydat
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
chvíli	chvíle	k1gFnSc4	chvíle
jak	jak	k8xC	jak
v	v	k7c6	v
normální	normální	k2eAgFnSc6d1	normální
tištěné	tištěný	k2eAgFnSc6d1	tištěná
<g/>
,	,	kIx,	,
tak	tak	k9	tak
v	v	k7c6	v
brailleovské	brailleovský	k2eAgFnSc6d1	brailleovský
edici	edice	k1gFnSc6	edice
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
Sudičky	sudička	k1gFnSc2	sudička
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Weird	Weird	k1gMnSc1	Weird
Sisters	Sisters	k1gInSc1	Sisters
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
na	na	k7c6	na
Bradavickém	Bradavický	k2eAgInSc6d1	Bradavický
plese	ples	k1gInSc6	ples
ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
dílu	díl	k1gInSc6	díl
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odkazem	odkaz	k1gInSc7	odkaz
na	na	k7c4	na
dílo	dílo	k1gNnSc4	dílo
Williama	William	k1gMnSc2	William
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
skladeb	skladba	k1gFnPc2	skladba
soundtracku	soundtrack	k1gInSc2	soundtrack
k	k	k7c3	k
třetímu	třetí	k4xOgInSc3	třetí
filmu	film	k1gInSc3	film
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
ztvárněním	ztvárnění	k1gNnSc7	ztvárnění
písně	píseň	k1gFnSc2	píseň
tří	tři	k4xCgFnPc2	tři
sudiček	sudička	k1gFnPc2	sudička
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
Macbeth	Macbetha	k1gFnPc2	Macbetha
<g/>
.	.	kIx.	.
</s>
<s>
Bradavický	Bradavický	k2eAgInSc1d1	Bradavický
vlak	vlak	k1gInSc1	vlak
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
studenti	student	k1gMnPc1	student
dopravují	dopravovat	k5eAaImIp3nP	dopravovat
z	z	k7c2	z
Londýna	Londýn	k1gInSc2	Londýn
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
vyjíždí	vyjíždět	k5eAaImIp3nS	vyjíždět
z	z	k7c2	z
nástupiště	nástupiště	k1gNnSc2	nástupiště
"	"	kIx"	"
<g/>
devět	devět	k4xCc4	devět
a	a	k8xC	a
tři	tři	k4xCgFnPc4	tři
čtvrtě	čtvrt	k1gFnPc4	čtvrt
<g/>
"	"	kIx"	"
na	na	k7c4	na
King	King	k1gInSc4	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Cross	Cross	k1gInSc1	Cross
Station	station	k1gInSc1	station
<g/>
.	.	kIx.	.
</s>
<s>
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
zde	zde	k6eAd1	zde
vyšla	vyjít	k5eAaPmAgFnS	vyjít
z	z	k7c2	z
britské	britský	k2eAgFnSc2d1	britská
legendy	legenda	k1gFnSc2	legenda
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
10	[number]	k4	10
<g/>
.	.	kIx.	.
nástupištěm	nástupiště	k1gNnSc7	nástupiště
pochováno	pochovat	k5eAaPmNgNnS	pochovat
tělo	tělo	k1gNnSc1	tělo
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
keltských	keltský	k2eAgMnPc2d1	keltský
válečníků	válečník	k1gMnPc2	válečník
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c4	na
'	'	kIx"	'
<g/>
King	King	k1gInSc4	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Cross	Cross	k1gInSc1	Cross
Station	station	k1gInSc1	station
<g/>
'	'	kIx"	'
seznámili	seznámit	k5eAaPmAgMnP	seznámit
její	její	k3xOp3gMnPc4	její
rodiče	rodič	k1gMnPc4	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nádraží	nádraží	k1gNnSc6	nádraží
'	'	kIx"	'
<g/>
King	King	k1gMnSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Cross	Cross	k1gInSc1	Cross
Station	station	k1gInSc1	station
<g/>
'	'	kIx"	'
není	být	k5eNaImIp3nS	být
žádná	žádný	k3yNgFnSc1	žádný
přepážka	přepážka	k1gFnSc1	přepážka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
vedla	vést	k5eAaImAgFnS	vést
na	na	k7c4	na
nástupiště	nástupiště	k1gNnSc4	nástupiště
9	[number]	k4	9
<g/>
3⁄	3⁄	k?	3⁄
Místo	místo	k7c2	místo
ní	on	k3xPp3gFnSc2	on
jsou	být	k5eAaImIp3nP	být
tam	tam	k6eAd1	tam
obyčejné	obyčejný	k2eAgInPc1d1	obyčejný
kovové	kovový	k2eAgInPc1d1	kovový
sloupy	sloup	k1gInPc1	sloup
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
popisování	popisování	k1gNnSc6	popisování
představovala	představovat	k5eAaImAgFnS	představovat
Paddingtonské	Paddingtonský	k2eAgNnSc4d1	Paddingtonský
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Přítelem	přítel	k1gMnSc7	přítel
a	a	k8xC	a
spolupracovníkem	spolupracovník	k1gMnSc7	spolupracovník
Albuse	Albuse	k1gFnSc2	Albuse
Brumbála	brumbál	k1gMnSc2	brumbál
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
Kámen	kámen	k1gInSc1	kámen
mudrců	mudrc	k1gMnPc2	mudrc
je	být	k5eAaImIp3nS	být
Nicolas	Nicolas	k1gMnSc1	Nicolas
Flamel	Flamel	k1gMnSc1	Flamel
<g/>
.	.	kIx.	.
</s>
<s>
Alchymista	alchymista	k1gMnSc1	alchymista
stejného	stejný	k2eAgNnSc2d1	stejné
jména	jméno	k1gNnSc2	jméno
skutečně	skutečně	k6eAd1	skutečně
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
ženě	žena	k1gFnSc3	žena
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
několik	několik	k4yIc4	několik
ulic	ulice	k1gFnPc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Flamela	Flamel	k1gMnSc2	Flamel
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
postavou	postava	k1gFnSc7	postava
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
Natalie	Natalie	k1gFnSc1	Natalie
McDonaldová	McDonaldová	k1gFnSc1	McDonaldová
<g/>
,	,	kIx,	,
studentka	studentka	k1gFnSc1	studentka
zařazená	zařazený	k2eAgFnSc1d1	zařazená
do	do	k7c2	do
Nebelvírské	Nebelvírský	k2eAgFnSc2d1	Nebelvírská
koleje	kolej	k1gFnSc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
Harryho	Harryha	k1gFnSc5	Harryha
fanynka	fanynka	k1gFnSc1	fanynka
poslala	poslat	k5eAaPmAgFnS	poslat
autorce	autorka	k1gFnSc3	autorka
e-mail	eail	k1gInSc1	e-mail
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
však	však	k9	však
stihla	stihnout	k5eAaPmAgFnS	stihnout
odepsat	odepsat	k5eAaPmF	odepsat
až	až	k9	až
den	den	k1gInSc4	den
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
dívka	dívka	k1gFnSc1	dívka
zemřela	zemřít	k5eAaPmAgFnS	zemřít
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
smrti	smrt	k1gFnSc6	smrt
udržovala	udržovat	k5eAaImAgFnS	udržovat
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
její	její	k3xOp3gFnSc7	její
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
zařadit	zařadit	k5eAaPmF	zařadit
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
dílu	díl	k1gInSc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c4	mnoho
scén	scéna	k1gFnPc2	scéna
odehrávajících	odehrávající	k2eAgFnPc2d1	odehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
Bradavické	Bradavický	k2eAgFnSc6d1	Bradavická
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
natáčelo	natáčet	k5eAaImAgNnS	natáčet
na	na	k7c6	na
Oxfordské	oxfordský	k2eAgFnSc6d1	Oxfordská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
J.	J.	kA	J.
R.	R.	kA	R.
R.	R.	kA	R.
Tolkien	Tolkina	k1gFnPc2	Tolkina
<g/>
,	,	kIx,	,
i	i	k8xC	i
J.	J.	kA	J.
K.	K.	kA	K.
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
stvořila	stvořit	k5eAaPmAgFnS	stvořit
při	při	k7c6	při
psaní	psaní	k1gNnSc6	psaní
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
fantasy	fantas	k1gInPc1	fantas
svět	svět	k1gInSc1	svět
s	s	k7c7	s
vlastními	vlastní	k2eAgInPc7d1	vlastní
zákony	zákon	k1gInPc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
klasické	klasický	k2eAgFnSc2d1	klasická
fantasy	fantas	k1gInPc4	fantas
tolkienovského	tolkienovský	k2eAgInSc2d1	tolkienovský
typu	typ	k1gInSc2	typ
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
Harry	Harr	k1gInPc4	Harr
Potter	Pottra	k1gFnPc2	Pottra
neodehrává	odehrávat	k5eNaImIp3nS	odehrávat
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
podobném	podobný	k2eAgInSc6d1	podobný
např.	např.	kA	např.
evropskému	evropský	k2eAgInSc3d1	evropský
středověku	středověk	k1gInSc3	středověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakémsi	jakýsi	k3yIgInSc6	jakýsi
paralelním	paralelní	k2eAgInSc6d1	paralelní
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Čarodějové	čaroděj	k1gMnPc1	čaroděj
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
(	(	kIx(	(
<g/>
a	a	k8xC	a
jiných	jiný	k2eAgInPc6d1	jiný
státech	stát	k1gInPc6	stát
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
postupně	postupně	k6eAd1	postupně
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
v	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
díle	dílo	k1gNnSc6	dílo
<g/>
)	)	kIx)	)
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
je	on	k3xPp3gMnPc4	on
obyčejní	obyčejný	k2eAgMnPc1d1	obyčejný
lidé	člověk	k1gMnPc1	člověk
(	(	kIx(	(
<g/>
neschopní	schopný	k2eNgMnPc1d1	neschopný
kouzlit	kouzlit	k5eAaImF	kouzlit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvaní	zvaný	k2eAgMnPc1d1	zvaný
mudlové	mudl	k1gMnPc1	mudl
neobjevili	objevit	k5eNaPmAgMnP	objevit
<g/>
,	,	kIx,	,
a	a	k8xC	a
nechtěli	chtít	k5eNaImAgMnP	chtít
po	po	k7c6	po
nich	on	k3xPp3gMnPc6	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
řešili	řešit	k5eAaImAgMnP	řešit
jejich	jejich	k3xOp3gInPc4	jejich
obyčejné	obyčejný	k2eAgInPc4d1	obyčejný
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Dovedně	dovedně	k6eAd1	dovedně
se	se	k3xPyFc4	se
skrývají	skrývat	k5eAaImIp3nP	skrývat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
je	on	k3xPp3gNnSc4	on
mudlové	mudl	k1gMnPc1	mudl
neodhalili	odhalit	k5eNaPmAgMnP	odhalit
-	-	kIx~	-
ochrana	ochrana	k1gFnSc1	ochrana
světa	svět	k1gInSc2	svět
čarodějů	čaroděj	k1gMnPc2	čaroděj
před	před	k7c7	před
nežádoucí	žádoucí	k2eNgFnSc7d1	nežádoucí
pozorností	pozornost	k1gFnSc7	pozornost
ostatních	ostatní	k2eAgMnPc2d1	ostatní
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
zákonů	zákon	k1gInPc2	zákon
kouzelnického	kouzelnický	k2eAgInSc2d1	kouzelnický
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
dodržováním	dodržování	k1gNnSc7	dodržování
bdí	bdít	k5eAaImIp3nS	bdít
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
.	.	kIx.	.
</s>
<s>
Kouzelníci	kouzelník	k1gMnPc1	kouzelník
tak	tak	k6eAd1	tak
žijí	žít	k5eAaImIp3nP	žít
stranou	strana	k1gFnSc7	strana
od	od	k7c2	od
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
kouzelnických	kouzelnický	k2eAgFnPc6d1	kouzelnická
vesnicích	vesnice	k1gFnPc6	vesnice
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Prasinky	Prasinka	k1gFnSc2	Prasinka
<g/>
,	,	kIx,	,
Godrikův	Godrikův	k2eAgInSc4d1	Godrikův
důl	důl	k1gInSc4	důl
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
londýnské	londýnský	k2eAgFnPc1d1	londýnská
ulice	ulice	k1gFnPc1	ulice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
skrytě	skrytě	k6eAd1	skrytě
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
kouzel	kouzlo	k1gNnPc2	kouzlo
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
mudlovských	mudlovský	k2eAgNnPc6d1	mudlovské
městech	město	k1gNnPc6	město
a	a	k8xC	a
ulicích	ulice	k1gFnPc6	ulice
(	(	kIx(	(
<g/>
v	v	k7c6	v
pátém	pátý	k4xOgInSc6	pátý
díle	díl	k1gInSc6	díl
je	být	k5eAaImIp3nS	být
takto	takto	k6eAd1	takto
zmíněno	zmíněn	k2eAgNnSc1d1	zmíněno
sídlo	sídlo	k1gNnSc1	sídlo
Fénixova	fénixův	k2eAgInSc2d1	fénixův
řádu	řád	k1gInSc2	řád
v	v	k7c6	v
nezakreslitelném	zakreslitelný	k2eNgInSc6d1	nezakreslitelný
domě	dům	k1gInSc6	dům
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc1	kniha
o	o	k7c4	o
Harrym	Harrym	k1gInSc4	Harrym
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
se	se	k3xPyFc4	se
věnují	věnovat	k5eAaImIp3nP	věnovat
především	především	k6eAd1	především
kouzelnické	kouzelnický	k2eAgFnSc3d1	kouzelnická
škole	škola	k1gFnSc3	škola
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Bradavice	bradavice	k1gFnSc1	bradavice
(	(	kIx(	(
<g/>
v	v	k7c4	v
orig	orig	k1gInSc4	orig
<g/>
.	.	kIx.	.
</s>
<s>
Hogwarts	Hogwarts	k1gInSc1	Hogwarts
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Škola	škola	k1gFnSc1	škola
má	mít	k5eAaImIp3nS	mít
sedm	sedm	k4xCc4	sedm
ročníků	ročník	k1gInPc2	ročník
a	a	k8xC	a
čtyři	čtyři	k4xCgFnPc4	čtyři
koleje	kolej	k1gFnPc4	kolej
<g/>
:	:	kIx,	:
Nebelvír	Nebelvír	k1gMnSc1	Nebelvír
(	(	kIx(	(
<g/>
orig	orig	k1gMnSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
Gryffindor	Gryffindor	k1gInSc1	Gryffindor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Havraspár	Havraspár	k1gMnSc1	Havraspár
(	(	kIx(	(
<g/>
orig	orig	k1gMnSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
Ravenclaw	Ravenclaw	k?	Ravenclaw
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mrzimor	Mrzimor	k1gMnSc1	Mrzimor
(	(	kIx(	(
<g/>
orig	orig	k1gMnSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
Hufflepuff	Hufflepuff	k1gInSc1	Hufflepuff
<g/>
)	)	kIx)	)
a	a	k8xC	a
Zmijozel	Zmijozel	k1gMnSc1	Zmijozel
(	(	kIx(	(
<g/>
orig	orig	k1gMnSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
Slytherin	Slytherin	k1gInSc1	Slytherin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
škola	škola	k1gFnSc1	škola
internátní	internátní	k2eAgFnSc1d1	internátní
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
většina	většina	k1gFnSc1	většina
žáků	žák	k1gMnPc2	žák
zde	zde	k6eAd1	zde
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
)	)	kIx)	)
stráví	strávit	k5eAaPmIp3nP	strávit
celý	celý	k2eAgInSc4d1	celý
školní	školní	k2eAgInSc4d1	školní
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Vyučují	vyučovat	k5eAaImIp3nP	vyučovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
různé	různý	k2eAgInPc4d1	různý
čarodějné	čarodějný	k2eAgInPc4d1	čarodějný
předměty	předmět	k1gInPc4	předmět
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
kouzelné	kouzelný	k2eAgFnSc2d1	kouzelná
formule	formule	k1gFnSc2	formule
<g/>
,	,	kIx,	,
přeměňování	přeměňování	k1gNnSc2	přeměňování
<g/>
,	,	kIx,	,
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
kouzelné	kouzelný	k2eAgMnPc4d1	kouzelný
tvory	tvor	k1gMnPc4	tvor
<g/>
,	,	kIx,	,
obrana	obrana	k1gFnSc1	obrana
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
<g/>
,	,	kIx,	,
bylinkářství	bylinkářství	k1gNnSc2	bylinkářství
<g/>
,	,	kIx,	,
jasnovidectví	jasnovidectví	k1gNnSc2	jasnovidectví
<g/>
,	,	kIx,	,
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
,	,	kIx,	,
studia	studio	k1gNnSc2	studio
mudlů	mudl	k1gInPc2	mudl
<g/>
,	,	kIx,	,
starodávné	starodávný	k2eAgFnPc1d1	starodávná
runy	runa	k1gFnPc1	runa
<g/>
,	,	kIx,	,
věštění	věštěný	k2eAgMnPc1d1	věštěný
z	z	k7c2	z
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
lektvary	lektvar	k1gInPc1	lektvar
<g/>
,	,	kIx,	,
<g/>
atd.	atd.	kA	atd.
<g/>
.	.	kIx.	.
.	.	kIx.	.
</s>
<s>
Ředitelem	ředitel	k1gMnSc7	ředitel
školy	škola	k1gFnSc2	škola
je	být	k5eAaImIp3nS	být
slavný	slavný	k2eAgMnSc1d1	slavný
čaroděj	čaroděj	k1gMnSc1	čaroděj
Albus	Albus	k1gMnSc1	Albus
Brumbál	brumbál	k1gMnSc1	brumbál
(	(	kIx(	(
<g/>
orig	orig	k1gMnSc1	orig
<g/>
.	.	kIx.	.
</s>
<s>
Albus	Albus	k1gMnSc1	Albus
Dumbledore	Dumbledor	k1gInSc5	Dumbledor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zlo	zlo	k1gNnSc1	zlo
v	v	k7c6	v
kouzelnickém	kouzelnický	k2eAgInSc6d1	kouzelnický
světě	svět	k1gInSc6	svět
představují	představovat	k5eAaImIp3nP	představovat
kromě	kromě	k7c2	kromě
běžných	běžný	k2eAgInPc2d1	běžný
piklů	pikel	k1gInPc2	pikel
a	a	k8xC	a
kouzelníků	kouzelník	k1gMnPc2	kouzelník
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
nemají	mít	k5eNaImIp3nP	mít
rádi	rád	k2eAgMnPc1d1	rád
mudly	mudla	k1gMnSc2	mudla
či	či	k8xC	či
čaroděje	čaroděj	k1gMnSc2	čaroděj
z	z	k7c2	z
mudlovské	mudlovský	k2eAgFnSc2d1	mudlovská
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
především	především	k9	především
stoupenci	stoupenec	k1gMnPc1	stoupenec
Lorda	lord	k1gMnSc2	lord
Voldemorta	Voldemort	k1gMnSc2	Voldemort
<g/>
,	,	kIx,	,
nejmocnějšího	mocný	k2eAgMnSc2d3	nejmocnější
čaroděje	čaroděj	k1gMnPc4	čaroděj
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
takzvaní	takzvaný	k2eAgMnPc1d1	takzvaný
smrtijedi	smrtijed	k1gMnPc1	smrtijed
se	se	k3xPyFc4	se
dorozumívají	dorozumívat	k5eAaImIp3nP	dorozumívat
pomocí	pomocí	k7c2	pomocí
zvláštních	zvláštní	k2eAgNnPc2d1	zvláštní
znamení	znamení	k1gNnPc2	znamení
vypálených	vypálený	k2eAgNnPc2d1	vypálené
na	na	k7c4	na
předloktí	předloktí	k1gNnSc4	předloktí
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
nim	on	k3xPp3gInPc3	on
bojují	bojovat	k5eAaImIp3nP	bojovat
speciálně	speciálně	k6eAd1	speciálně
školení	školený	k2eAgMnPc1d1	školený
kouzelníci	kouzelník	k1gMnPc1	kouzelník
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
bystrozoři	bystrozor	k1gMnPc1	bystrozor
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Smrtijedi	Smrtijed	k1gMnPc1	Smrtijed
obvykle	obvykle	k6eAd1	obvykle
po	po	k7c6	po
dopadení	dopadení	k1gNnSc1	dopadení
a	a	k8xC	a
odsouzení	odsouzení	k1gNnSc1	odsouzení
skončí	skončit	k5eAaPmIp3nS	skončit
v	v	k7c6	v
kouzelnickém	kouzelnický	k2eAgNnSc6d1	kouzelnické
vězení	vězení	k1gNnSc6	vězení
-	-	kIx~	-
Azkabanu	Azkaban	k1gInSc2	Azkaban
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
střeženo	střežit	k5eAaImNgNnS	střežit
děsivými	děsivý	k2eAgMnPc7d1	děsivý
strážci	strážce	k1gMnPc7	strážce
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
mozkomory	mozkomor	k1gInPc7	mozkomor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kvikálkově	Kvikálkův	k2eAgFnSc6d1	Kvikálkova
<g/>
,	,	kIx,	,
Zobí	Zobí	k2eAgFnSc6d1	Zobí
ulici	ulice	k1gFnSc6	ulice
číslo	číslo	k1gNnSc1	číslo
4	[number]	k4	4
<g/>
,	,	kIx,	,
žila	žít	k5eAaImAgFnS	žít
rodina	rodina	k1gFnSc1	rodina
Dursleyových	Dursleyových	k2eAgFnSc1d1	Dursleyových
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
obyčejná	obyčejný	k2eAgFnSc1d1	obyčejná
rodina	rodina	k1gFnSc1	rodina
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
poslední	poslední	k2eAgFnSc1d1	poslední
<g/>
,	,	kIx,	,
<g/>
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
společného	společný	k2eAgMnSc4d1	společný
s	s	k7c7	s
něčím	něčí	k3xOyIgMnSc7	něčí
zvláštím	zvláštit	k5eAaPmIp1nS	zvláštit
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
nechtěli	chtít	k5eNaImAgMnP	chtít
aby	aby	kYmCp3nS	aby
někdo	někdo	k3yInSc1	někdo
věděl	vědět	k5eAaImAgMnS	vědět
o	o	k7c6	o
Potterových	Potterův	k2eAgFnPc6d1	Potterova
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gMnPc2	jejich
kouzelnických	kouzelnický	k2eAgMnPc2d1	kouzelnický
příbuzných	příbuzný	k1gMnPc2	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Godrikově	Godrikův	k2eAgInSc6d1	Godrikův
dole	dol	k1gInSc6	dol
mezitím	mezitím	k6eAd1	mezitím
Pán	pán	k1gMnSc1	pán
zla	zlo	k1gNnSc2	zlo
<g/>
,	,	kIx,	,
Lord	lord	k1gMnSc1	lord
Voldemort	Voldemort	k1gInSc4	Voldemort
Pozabíjel	pozabíjet	k5eAaPmAgMnS	pozabíjet
všechny	všechen	k3xTgMnPc4	všechen
Potterovi	Potter	k1gMnSc3	Potter
až	až	k9	až
na	na	k7c4	na
Malého	Malý	k1gMnSc4	Malý
Harryho	Harry	k1gMnSc4	Harry
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přežil	přežít	k5eAaPmAgMnS	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Voldemort	Voldemort	k1gInSc1	Voldemort
byl	být	k5eAaImAgInS	být
zneškodněn	zneškodnit	k5eAaPmNgInS	zneškodnit
,	,	kIx,	,
<g/>
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
že	že	k8xS	že
se	se	k3xPyFc4	se
od	od	k7c2	od
Harryho	Harry	k1gMnSc2	Harry
kletba	kletba	k1gFnSc1	kletba
odrazila	odrazit	k5eAaPmAgFnS	odrazit
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc1	Harr
Skončil	skončit	k5eAaPmAgMnS	skončit
u	u	k7c2	u
Dursleyových	Dursleyův	k2eAgNnPc2d1	Dursleyův
a	a	k8xC	a
celých	celý	k2eAgNnPc2d1	celé
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
tam	tam	k6eAd1	tam
musel	muset	k5eAaImAgInS	muset
žít	žít	k5eAaImF	žít
s	s	k7c7	s
jeho	jeho	k3xOp3gMnSc7	jeho
bratrancem	bratranec	k1gMnSc7	bratranec
Dudlym	Dudlym	k1gInSc4	Dudlym
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ho	on	k3xPp3gNnSc4	on
neustále	neustále	k6eAd1	neustále
bil	bít	k5eAaImAgMnS	bít
a	a	k8xC	a
tyranizoval	tyranizovat	k5eAaImAgMnS	tyranizovat
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k9	jednou
mu	on	k3xPp3gMnSc3	on
přišel	přijít	k5eAaPmAgMnS	přijít
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
<g/>
který	který	k3yRgInSc1	který
ho	on	k3xPp3gMnSc4	on
strýc	strýc	k1gMnSc1	strýc
Vernon	Vernon	k1gMnSc1	Vernon
Dursley	Durslea	k1gFnSc2	Durslea
nenechal	nechat	k5eNaPmAgMnS	nechat
přečíst	přečíst	k5eAaPmF	přečíst
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
jednou	jednou	k6eAd1	jednou
pro	pro	k7c4	pro
Harryho	Harry	k1gMnSc4	Harry
přišel	přijít	k5eAaPmAgInS	přijít
Poloobr	Poloobr	k1gInSc1	Poloobr
Hagrid	Hagrid	k1gInSc1	Hagrid
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgInS	dát
dopis	dopis	k1gInSc1	dopis
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
ho	on	k3xPp3gMnSc4	on
přečíst	přečíst	k5eAaPmF	přečíst
.	.	kIx.	.
</s>
<s>
Přijali	přijmout	k5eAaPmAgMnP	přijmout
ho	on	k3xPp3gInSc4	on
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
čar	čára	k1gFnPc2	čára
a	a	k8xC	a
kouzel	kouzlo	k1gNnPc2	kouzlo
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Harry	Harra	k1gFnSc2	Harra
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
našel	najít	k5eAaPmAgMnS	najít
nového	nový	k2eAgMnSc4d1	nový
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
kamaráda	kamarád	k1gMnSc4	kamarád
Rona	Ron	k1gMnSc2	Ron
Weasleyho	Weasley	k1gMnSc2	Weasley
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
co	co	k3yQnSc4	co
přeprali	přeprat	k5eAaPmAgMnP	přeprat
horského	horský	k2eAgMnSc4d1	horský
trola	trol	k1gMnSc4	trol
<g/>
,	,	kIx,	,
skamarádili	skamarádit	k5eAaPmAgMnP	skamarádit
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
Hermionou	Hermiona	k1gFnSc7	Hermiona
Grangerovou	Grangerová	k1gFnSc7	Grangerová
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
zjišťovali	zjišťovat	k5eAaImAgMnP	zjišťovat
další	další	k2eAgMnPc1d1	další
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
věci	věc	k1gFnPc1	věc
o	o	k7c6	o
Kameni	kámen	k1gInSc6	kámen
mudrců	mudrc	k1gMnPc2	mudrc
<g/>
,	,	kIx,	,
a	a	k8xC	a
nakonci	nakonek	k1gMnPc1	nakonek
roku	rok	k1gInSc3	rok
ho	on	k3xPp3gMnSc4	on
museli	muset	k5eAaImAgMnP	muset
zachránit	zachránit	k5eAaPmF	zachránit
před	před	k7c7	před
profesorem	profesor	k1gMnSc7	profesor
Quirrellem	Quirrell	k1gMnSc7	Quirrell
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ho	on	k3xPp3gMnSc4	on
chtěl	chtít	k5eAaImAgMnS	chtít
ukrást	ukrást	k5eAaPmF	ukrást
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
spolčený	spolčený	k2eAgMnSc1d1	spolčený
s	s	k7c7	s
lordem	lord	k1gMnSc7	lord
Voldemortem	Voldemort	k1gInSc7	Voldemort
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
už	už	k6eAd1	už
jel	jet	k5eAaImAgMnS	jet
"	"	kIx"	"
<g/>
domů	domů	k6eAd1	domů
<g/>
"	"	kIx"	"
a	a	k8xC	a
musel	muset	k5eAaImAgInS	muset
přežít	přežít	k5eAaPmF	přežít
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
měsícu	měsícu	k6eAd1	měsícu
u	u	k7c2	u
Dursleyových	Dursleyová	k1gFnPc2	Dursleyová
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
oblíbenost	oblíbenost	k1gFnSc4	oblíbenost
a	a	k8xC	a
komerční	komerční	k2eAgNnSc4d1	komerční
pozadí	pozadí	k1gNnSc4	pozadí
okolo	okolo	k7c2	okolo
vydávání	vydávání	k1gNnSc2	vydávání
a	a	k8xC	a
další	další	k2eAgNnSc4d1	další
zpracování	zpracování	k1gNnSc4	zpracování
literárního	literární	k2eAgNnSc2d1	literární
díla	dílo	k1gNnSc2	dílo
jsou	být	k5eAaImIp3nP	být
kritické	kritický	k2eAgInPc1d1	kritický
a	a	k8xC	a
pochvalné	pochvalný	k2eAgInPc1d1	pochvalný
komentáře	komentář	k1gInPc1	komentář
na	na	k7c4	na
literární	literární	k2eAgNnSc4d1	literární
dílo	dílo	k1gNnSc4	dílo
i	i	k9	i
zfilmované	zfilmovaný	k2eAgNnSc1d1	zfilmované
provedení	provedení	k1gNnSc1	provedení
hojné	hojný	k2eAgNnSc1d1	hojné
a	a	k8xC	a
nejednoznačné	jednoznačný	k2eNgNnSc1d1	nejednoznačné
<g/>
,	,	kIx,	,
ba	ba	k9	ba
mnohdy	mnohdy	k6eAd1	mnohdy
zcela	zcela	k6eAd1	zcela
opačné	opačný	k2eAgInPc1d1	opačný
<g/>
.	.	kIx.	.
</s>
<s>
Popisované	popisovaný	k2eAgFnPc4d1	popisovaná
vady	vada	k1gFnPc4	vada
čtenáři	čtenář	k1gMnPc1	čtenář
-	-	kIx~	-
fanoušci	fanoušek	k1gMnPc1	fanoušek
totiž	totiž	k9	totiž
obvykle	obvykle	k6eAd1	obvykle
neřeší	řešit	k5eNaImIp3nS	řešit
a	a	k8xC	a
nad	nad	k7c7	nad
jednoduchostí	jednoduchost	k1gFnSc7	jednoduchost
řešení	řešení	k1gNnPc2	řešení
zápletek	zápletek	k1gInSc1	zápletek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
první	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
<g/>
,	,	kIx,	,
vyřešení	vyřešení	k1gNnSc4	vyřešení
hlavní	hlavní	k2eAgFnSc2d1	hlavní
zápletky	zápletka	k1gFnSc2	zápletka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
podivnostmi	podivnost	k1gFnPc7	podivnost
v	v	k7c6	v
chování	chování	k1gNnSc6	chování
a	a	k8xC	a
okolnostech	okolnost	k1gFnPc6	okolnost
se	se	k3xPyFc4	se
nepozastavují	pozastavovat	k5eNaImIp3nP	pozastavovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
druhý	druhý	k4xOgInSc4	druhý
díl	díl	k1gInSc4	díl
<g/>
,	,	kIx,	,
přeživší	přeživší	k2eAgNnSc4d1	přeživší
dítě	dítě	k1gNnSc4	dítě
fotografující	fotografující	k2eAgFnSc1d1	fotografující
baziliška	baziliška	k1gFnSc1	baziliška
<g/>
,	,	kIx,	,
Harry	Harra	k1gMnSc2	Harra
P.	P.	kA	P.
který	který	k3yRgInSc4	který
neustále	neustále	k6eAd1	neustále
sebevražedně	sebevražedně	k6eAd1	sebevražedně
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
smrtelná	smrtelný	k2eAgNnPc4d1	smrtelné
nebezpečí	nebezpečí	k1gNnPc4	nebezpečí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cílovou	cílový	k2eAgFnSc7d1	cílová
skupinou	skupina	k1gFnSc7	skupina
čtenářů	čtenář	k1gMnPc2	čtenář
jsou	být	k5eAaImIp3nP	být
nezletilé	zletilý	k2eNgFnPc1d1	nezletilá
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
tři	tři	k4xCgInPc4	tři
díly	díl	k1gInPc4	díl
série	série	k1gFnSc2	série
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
v	v	k7c4	v
knižní	knižní	k2eAgInSc4d1	knižní
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ve	v	k7c6	v
filmové	filmový	k2eAgFnSc6d1	filmová
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
hodnoceny	hodnotit	k5eAaImNgInP	hodnotit
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
zdařilé	zdařilý	k2eAgFnPc1d1	zdařilá
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgMnPc4d1	považován
za	za	k7c4	za
přelomová	přelomový	k2eAgNnPc4d1	přelomové
díla	dílo	k1gNnPc4	dílo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
odlákala	odlákat	k5eAaPmAgNnP	odlákat
děti	dítě	k1gFnPc4	dítě
od	od	k7c2	od
obrazovek	obrazovka	k1gFnPc2	obrazovka
zpět	zpět	k6eAd1	zpět
ke	k	k7c3	k
knihám	kniha	k1gFnPc3	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
negativně	negativně	k6eAd1	negativně
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
fanoušky	fanoušek	k1gMnPc4	fanoušek
(	(	kIx(	(
<g/>
i	i	k8xC	i
dívkami	dívka	k1gFnPc7	dívka
<g/>
)	)	kIx)	)
hodnoceny	hodnotit	k5eAaImNgFnP	hodnotit
milostné	milostný	k2eAgFnPc1d1	milostná
pletky	pletka	k1gFnPc1	pletka
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
Princ	princ	k1gMnSc1	princ
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Klíčová	klíčový	k2eAgFnSc1d1	klíčová
otázka	otázka	k1gFnSc1	otázka
pro	pro	k7c4	pro
obeznámené	obeznámený	k2eAgNnSc4d1	obeznámené
s	s	k7c7	s
předlohou	předloha	k1gFnSc7	předloha
tedy	tedy	k8xC	tedy
zněla	znět	k5eAaImAgFnS	znět
<g/>
:	:	kIx,	:
O	o	k7c4	o
kolik	kolik	k4yRc4	kolik
bude	být	k5eAaImBp3nS	být
film	film	k1gInSc1	film
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
blbý	blbý	k2eAgMnSc1d1	blbý
než	než	k8xS	než
nesnesitelně	snesitelně	k6eNd1	snesitelně
patetická	patetický	k2eAgFnSc1d1	patetická
<g/>
,	,	kIx,	,
hloupě	hloupě	k6eAd1	hloupě
vysvětlující	vysvětlující	k2eAgFnPc1d1	vysvětlující
a	a	k8xC	a
přes	přes	k7c4	přes
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
snahu	snaha	k1gFnSc4	snaha
o	o	k7c4	o
"	"	kIx"	"
<g/>
temnou	temný	k2eAgFnSc4d1	temná
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
"	"	kIx"	"
vlastně	vlastně	k9	vlastně
infantilně	infantilně	k6eAd1	infantilně
naivní	naivní	k2eAgFnSc1d1	naivní
kniha	kniha	k1gFnSc1	kniha
<g/>
?	?	kIx.	?
</s>
<s>
"	"	kIx"	"
<g/>
Deset	deset	k4xCc1	deset
let	léto	k1gNnPc2	léto
s	s	k7c7	s
Potterem	Potter	k1gInSc7	Potter
bohatě	bohatě	k6eAd1	bohatě
stačilo	stačit	k5eAaBmAgNnS	stačit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Harry	Harra	k1gFnPc1	Harra
Potter	Potter	k1gInSc4	Potter
nám	my	k3xPp1nPc3	my
dospěl	dochvít	k5eAaPmAgMnS	dochvít
<g/>
.	.	kIx.	.
</s>
<s>
Chlapec	chlapec	k1gMnSc1	chlapec
zrozený	zrozený	k2eAgMnSc1d1	zrozený
v	v	k7c6	v
bolesti	bolest	k1gFnSc6	bolest
a	a	k8xC	a
krvi	krev	k1gFnSc6	krev
dokončuje	dokončovat	k5eAaImIp3nS	dokončovat
osudovou	osudový	k2eAgFnSc4d1	osudová
cestu	cesta	k1gFnSc4	cesta
a	a	k8xC	a
vy	vy	k3xPp2nPc1	vy
byste	by	kYmCp2nP	by
u	u	k7c2	u
toho	ten	k3xDgNnSc2	ten
neměli	mít	k5eNaImAgMnP	mít
chybět	chybět	k5eAaImF	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Tohle	tenhle	k3xDgNnSc1	tenhle
už	už	k9	už
není	být	k5eNaImIp3nS	být
pohádka	pohádka	k1gFnSc1	pohádka
<g/>
.	.	kIx.	.
</s>
<s>
Tohle	tenhle	k3xDgNnSc1	tenhle
je	být	k5eAaImIp3nS	být
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
dobrá	dobrý	k2eAgFnSc1d1	dobrá
adaptace	adaptace	k1gFnSc1	adaptace
a	a	k8xC	a
zatraceně	zatraceně	k6eAd1	zatraceně
dobrý	dobrý	k2eAgInSc1d1	dobrý
film	film	k1gInSc1	film
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
Relikvií	relikvie	k1gFnPc2	relikvie
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
téměř	téměř	k6eAd1	téměř
dokonalá	dokonalý	k2eAgFnSc1d1	dokonalá
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
<g/>
...	...	k?	...
...	...	k?	...
<g/>
je	být	k5eAaImIp3nS	být
strhující	strhující	k2eAgInSc4d1	strhující
a	a	k8xC	a
znamenitě	znamenitě	k6eAd1	znamenitě
zahraná	zahraný	k2eAgFnSc1d1	zahraná
i	i	k8xC	i
natočená	natočený	k2eAgFnSc1d1	natočená
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
...	...	k?	...
<g/>
běžní	běžný	k2eAgMnPc1d1	běžný
návštěvníci	návštěvník	k1gMnPc1	návštěvník
kin	kino	k1gNnPc2	kino
budou	být	k5eAaImBp3nP	být
možná	možná	k9	možná
díky	dík	k1gInPc1	dík
velkoleposti	velkolepost	k1gFnSc2	velkolepost
efektů	efekt	k1gInPc2	efekt
odcházet	odcházet	k5eAaImF	odcházet
ze	z	k7c2	z
sálu	sál	k1gInSc2	sál
s	s	k7c7	s
pocitem	pocit	k1gInSc7	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
tom	ten	k3xDgMnSc6	ten
Potterovi	Potter	k1gMnSc6	Potter
přece	přece	k9	přece
jenom	jenom	k9	jenom
něco	něco	k3yInSc4	něco
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Skalní	skalní	k2eAgMnPc1d1	skalní
fanoušci	fanoušek	k1gMnPc1	fanoušek
knižní	knižní	k2eAgFnSc2d1	knižní
předlohy	předloha	k1gFnSc2	předloha
ale	ale	k8xC	ale
film	film	k1gInSc1	film
nejspíš	nejspíš	k9	nejspíš
zklame	zklamat	k5eAaPmIp3nS	zklamat
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
(	(	kIx(	(
<g/>
Poslední	poslední	k2eAgInSc1d1	poslední
díl	díl	k1gInSc1	díl
zfilmované	zfilmovaný	k2eAgFnSc2d1	zfilmovaná
ságy	sága	k1gFnSc2	sága
<g/>
)	)	kIx)	)
Je	být	k5eAaImIp3nS	být
fanouškovskou	fanouškovský	k2eAgFnSc7d1	fanouškovská
záležitostí	záležitost	k1gFnSc7	záležitost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
spoléhá	spoléhat	k5eAaImIp3nS	spoléhat
na	na	k7c4	na
oddané	oddaný	k2eAgMnPc4d1	oddaný
čtenáře	čtenář	k1gMnPc4	čtenář
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jsou	být	k5eAaImIp3nP	být
zvědaví	zvědavý	k2eAgMnPc1d1	zvědavý
jak	jak	k8xS	jak
si	se	k3xPyFc3	se
tvůrci	tvůrce	k1gMnPc1	tvůrce
poradili	poradit	k5eAaPmAgMnP	poradit
se	s	k7c7	s
zpracováním	zpracování	k1gNnSc7	zpracování
jejich	jejich	k3xOp3gInPc2	jejich
oblíbených	oblíbený	k2eAgInPc2d1	oblíbený
motivů	motiv	k1gInPc2	motiv
z	z	k7c2	z
knihy	kniha	k1gFnSc2	kniha
a	a	k8xC	a
nezatěžují	zatěžovat	k5eNaImIp3nP	zatěžovat
se	se	k3xPyFc4	se
<g />
.	.	kIx.	.
</s>
<s>
až	až	k9	až
tolik	tolik	k6eAd1	tolik
požadavky	požadavek	k1gInPc4	požadavek
logiky	logika	k1gFnSc2	logika
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
návaznosti	návaznost	k1gFnSc2	návaznost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Jistě	jistě	k9	jistě
<g/>
,	,	kIx,	,
každý	každý	k3xTgMnSc1	každý
má	mít	k5eAaImIp3nS	mít
rád	rád	k2eAgMnSc1d1	rád
dobré	dobrý	k2eAgInPc4d1	dobrý
konce	konec	k1gInPc4	konec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
proč	proč	k6eAd1	proč
postavit	postavit	k5eAaPmF	postavit
před	před	k7c4	před
čtenáře	čtenář	k1gMnSc4	čtenář
celé	celý	k2eAgNnSc1d1	celé
defilé	defilé	k1gNnSc1	defilé
postav	postava	k1gFnPc2	postava
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
všech	všecek	k3xTgFnPc2	všecek
jejich	jejich	k3xOp3gFnPc2	jejich
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
a	a	k8xC	a
pěkně	pěkně	k6eAd1	pěkně
polopaticky	polopaticky	k6eAd1	polopaticky
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
moc	moc	k6eAd1	moc
jsou	být	k5eAaImIp3nP	být
všichni	všechen	k3xTgMnPc1	všechen
šťastní	šťastný	k2eAgMnPc1d1	šťastný
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
opravdu	opravdu	k6eAd1	opravdu
záhada	záhada	k1gFnSc1	záhada
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
pohádky	pohádka	k1gFnPc4	pohádka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
Rowlingová	Rowlingový	k2eAgFnSc1d1	Rowlingová
čerpá	čerpat	k5eAaImIp3nS	čerpat
většinu	většina	k1gFnSc4	většina
inspirace	inspirace	k1gFnSc2	inspirace
<g/>
,	,	kIx,	,
takto	takto	k6eAd1	takto
banálně	banálně	k6eAd1	banálně
nekončí	končit	k5eNaImIp3nS	končit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
filmy	film	k1gInPc1	film
o	o	k7c4	o
Harry	Harra	k1gFnPc4	Harra
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
komerční	komerční	k2eAgFnSc1d1	komerční
rychlokvaška	rychlokvaška	k1gFnSc1	rychlokvaška
<g/>
,	,	kIx,	,
bohužel	bohužel	k6eAd1	bohužel
nezakryje	zakrýt	k5eNaPmIp3nS	zakrýt
ani	ani	k9	ani
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gInPc4	on
točí	točit	k5eAaImIp3nP	točit
schopní	schopný	k2eAgMnPc1d1	schopný
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
výsledek	výsledek	k1gInSc1	výsledek
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
kladný	kladný	k2eAgInSc1d1	kladný
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
být	být	k5eAaImF	být
rádi	rád	k2eAgMnPc1d1	rád
<g/>
,	,	kIx,	,
že	že	k8xS	že
tenhle	tenhle	k3xDgInSc4	tenhle
hon	hon	k1gInSc4	hon
za	za	k7c7	za
penězi	peníze	k1gInPc7	peníze
dostali	dostat	k5eAaPmAgMnP	dostat
do	do	k7c2	do
rukou	ruka	k1gFnSc7	ruka
tvůrci	tvůrce	k1gMnPc1	tvůrce
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
si	se	k3xPyFc3	se
s	s	k7c7	s
nelehkým	lehký	k2eNgInSc7d1	nelehký
úkolem	úkol	k1gInSc7	úkol
natočit	natočit	k5eAaBmF	natočit
filmy	film	k1gInPc4	film
podle	podle	k7c2	podle
rozepsané	rozepsaný	k2eAgFnSc2d1	rozepsaná
ságy	sága	k1gFnSc2	sága
dokázali	dokázat	k5eAaPmAgMnP	dokázat
poradit	poradit	k5eAaPmF	poradit
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Film	film	k1gInSc1	film
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
Princ	princ	k1gMnSc1	princ
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
relativizuje	relativizovat	k5eAaImIp3nS	relativizovat
měřítka	měřítko	k1gNnPc1	měřítko
špatného	špatný	k2eAgInSc2d1	špatný
a	a	k8xC	a
dobrého	dobrý	k2eAgInSc2d1	dobrý
<g/>
...	...	k?	...
...	...	k?	...
<g/>
Fanouškovský	fanouškovský	k2eAgMnSc1d1	fanouškovský
"	"	kIx"	"
<g/>
naivní	naivní	k2eAgMnSc1d1	naivní
<g/>
"	"	kIx"	"
divák	divák	k1gMnSc1	divák
vnímající	vnímající	k2eAgInSc4d1	vnímající
film	film	k1gInSc4	film
v	v	k7c6	v
potterovských	potterovský	k2eAgFnPc6d1	potterovská
souvislostech	souvislost	k1gFnPc6	souvislost
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
jediný	jediný	k2eAgMnSc1d1	jediný
žádoucí	žádoucí	k2eAgMnSc1d1	žádoucí
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
nakročil	nakročit	k5eAaPmAgMnS	nakročit
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
správnou	správný	k2eAgFnSc7d1	správná
nohou	noha	k1gFnSc7	noha
<g/>
.	.	kIx.	.
</s>
<s>
Technicky	technicky	k6eAd1	technicky
bezchybný	bezchybný	k2eAgInSc4d1	bezchybný
<g/>
,	,	kIx,	,
herecky	herecky	k6eAd1	herecky
vysoce	vysoce	k6eAd1	vysoce
nadprůměrný	nadprůměrný	k2eAgInSc4d1	nadprůměrný
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
ozvučený	ozvučený	k2eAgInSc1d1	ozvučený
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
připomíná	připomínat	k5eAaImIp3nS	připomínat
dospělý	dospělý	k2eAgInSc1d1	dospělý
thriller	thriller	k1gInSc1	thriller
<g/>
,	,	kIx,	,
nežli	nežli	k8xS	nežli
dětskou	dětský	k2eAgFnSc4d1	dětská
fantasy	fantas	k1gInPc4	fantas
a	a	k8xC	a
jehož	jehož	k3xOyRp3gFnPc4	jehož
režisér	režisér	k1gMnSc1	režisér
místy	místy	k6eAd1	místy
koketuje	koketovat	k5eAaImIp3nS	koketovat
s	s	k7c7	s
genialitou	genialita	k1gFnSc7	genialita
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
díla	dílo	k1gNnSc2	dílo
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
kritik	kritika	k1gFnPc2	kritika
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
propracovaná	propracovaný	k2eAgFnSc1d1	propracovaná
<g/>
,	,	kIx,	,
vedlejší	vedlejší	k2eAgFnPc1d1	vedlejší
postavy	postava	k1gFnPc1	postava
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
načrtnuty	načrtnut	k2eAgInPc1d1	načrtnut
<g/>
,	,	kIx,	,
sekundujív	sekundujív	k1gInSc1	sekundujív
rolích	role	k1gFnPc6	role
klaunů	klaun	k1gMnPc2	klaun
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
nápomocny	nápomocen	k2eAgInPc1d1	nápomocen
při	při	k7c6	při
rozuzlování	rozuzlování	k1gNnSc6	rozuzlování
zápletek	zápletka	k1gFnPc2	zápletka
rádců	rádce	k1gMnPc2	rádce
s	s	k7c7	s
nečekanými	čekaný	k2eNgFnPc7d1	nečekaná
schopnostmi	schopnost	k1gFnPc7	schopnost
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
jsou	být	k5eAaImIp3nP	být
vybaveny	vybavit	k5eAaPmNgFnP	vybavit
rekvizitami	rekvizita	k1gFnPc7	rekvizita
právě	právě	k6eAd1	právě
vhodnými	vhodný	k2eAgInPc7d1	vhodný
ke	k	k7c3	k
geniálnímu	geniální	k2eAgNnSc3d1	geniální
řešení	řešení	k1gNnSc3	řešení
neřešitelného	řešitelný	k2eNgInSc2d1	neřešitelný
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
postavy	postava	k1gFnPc1	postava
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
nebo	nebo	k8xC	nebo
zcela	zcela	k6eAd1	zcela
zahaleny	zahalen	k2eAgInPc1d1	zahalen
oparem	opar	k1gInSc7	opar
tajemství	tajemství	k1gNnPc2	tajemství
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
tu	tu	k6eAd1	tu
a	a	k8xC	a
tam	tam	k6eAd1	tam
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
nečekané	čekaný	k2eNgInPc4d1	nečekaný
obraty	obrat	k1gInPc4	obrat
<g/>
,	,	kIx,	,
překvapivá	překvapivý	k2eAgNnPc1d1	překvapivé
spojení	spojení	k1gNnPc1	spojení
nebo	nebo	k8xC	nebo
sentimentální	sentimentální	k2eAgInPc1d1	sentimentální
okamžiky	okamžik	k1gInPc1	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
věrohodnost	věrohodnost	k1gFnSc1	věrohodnost
v	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
se	se	k3xPyFc4	se
náhle	náhle	k6eAd1	náhle
vynořivších	vynořivší	k2eAgMnPc2d1	vynořivší
bratrů	bratr	k1gMnPc2	bratr
<g/>
,	,	kIx,	,
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
donedávna	donedávna	k6eAd1	donedávna
neviditelných	viditelný	k2eNgMnPc2d1	Neviditelný
všemocných	všemocný	k2eAgMnPc2d1	všemocný
skřítků	skřítek	k1gMnPc2	skřítek
(	(	kIx(	(
<g/>
v	v	k7c6	v
roli	role	k1gFnSc6	role
otroků	otrok	k1gMnPc2	otrok
<g/>
)	)	kIx)	)
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
situace	situace	k1gFnPc1	situace
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
zvládnuty	zvládnut	k2eAgFnPc1d1	zvládnuta
lépe	dobře	k6eAd2	dobře
(	(	kIx(	(
<g/>
přátelé	přítel	k1gMnPc1	přítel
otce	otec	k1gMnSc2	otec
H.	H.	kA	H.
<g/>
P.	P.	kA	P.
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
vězeň	vězeň	k1gMnSc1	vězeň
z	z	k7c2	z
Azkabanu	Azkaban	k1gInSc2	Azkaban
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
hůře	zle	k6eAd2	zle
(	(	kIx(	(
<g/>
bratr	bratr	k1gMnSc1	bratr
ředitele	ředitel	k1gMnSc2	ředitel
školy	škola	k1gFnSc2	škola
v	v	k7c4	v
Relikvie	relikvie	k1gFnPc4	relikvie
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
motivy	motiv	k1gInPc7	motiv
konání	konání	k1gNnSc2	konání
Snapea	Snapeus	k1gMnSc2	Snapeus
v	v	k7c4	v
Relikvie	relikvie	k1gFnPc4	relikvie
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
přehled	přehled	k1gInSc1	přehled
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Schopnosti	schopnost	k1gFnPc1	schopnost
protivníka	protivník	k1gMnSc2	protivník
hlavního	hlavní	k2eAgMnSc2d1	hlavní
hrdiny	hrdina	k1gMnSc2	hrdina
rostou	růst	k5eAaImIp3nP	růst
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
schopnostmi	schopnost	k1gFnPc7	schopnost
hlavního	hlavní	k2eAgMnSc2d1	hlavní
hrdiny	hrdina	k1gMnSc2	hrdina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dodává	dodávat	k5eAaImIp3nS	dodávat
příběhu	příběh	k1gInSc3	příběh
poutavost	poutavost	k1gFnSc4	poutavost
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
atraktantem	atraktant	k1gMnSc7	atraktant
příběhu	příběh	k1gInSc2	příběh
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
názorů	názor	k1gInPc2	názor
recenzentů	recenzent	k1gMnPc2	recenzent
nové	nový	k2eAgNnSc4d1	nové
románové	románový	k2eAgNnSc4d1	románové
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc4	jeho
popis	popis	k1gInSc4	popis
a	a	k8xC	a
rekvizity	rekvizita	k1gFnPc4	rekvizita
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
zcela	zcela	k6eAd1	zcela
jedinečné	jedinečný	k2eAgNnSc1d1	jedinečné
náhle	náhle	k6eAd1	náhle
se	se	k3xPyFc4	se
objevující	objevující	k2eAgFnPc1d1	objevující
rekvizity	rekvizita	k1gFnPc1	rekvizita
zcela	zcela	k6eAd1	zcela
příhodné	příhodný	k2eAgFnPc1d1	příhodná
k	k	k7c3	k
řešení	řešení	k1gNnSc3	řešení
nemožných	možný	k2eNgFnPc2d1	nemožná
situací	situace	k1gFnPc2	situace
a	a	k8xC	a
neřešitelných	řešitelný	k2eNgInPc2d1	neřešitelný
problémů	problém	k1gInPc2	problém
není	být	k5eNaImIp3nS	být
nouze	nouze	k1gFnSc1	nouze
<g/>
.	.	kIx.	.
</s>
<s>
Takovými	takový	k3xDgFnPc7	takový
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
náhle	náhle	k6eAd1	náhle
se	se	k3xPyFc4	se
kdekoliv	kdekoliv	k6eAd1	kdekoliv
objevující	objevující	k2eAgInSc1d1	objevující
meč	meč	k1gInSc1	meč
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
jej	on	k3xPp3gMnSc4	on
potřebujete	potřebovat	k5eAaImIp2nP	potřebovat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
popisovaná	popisovaný	k2eAgFnSc1d1	popisovaná
"	"	kIx"	"
<g/>
místnost	místnost	k1gFnSc1	místnost
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
"	"	kIx"	"
vchod	vchod	k1gInSc1	vchod
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
potřebujete	potřebovat	k5eAaImIp2nP	potřebovat
a	a	k8xC	a
místnost	místnost	k1gFnSc1	místnost
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
jakou	jaký	k3yQgFnSc7	jaký
ji	on	k3xPp3gFnSc4	on
potřebujete	potřebovat	k5eAaImIp2nP	potřebovat
s	s	k7c7	s
potřebným	potřebný	k2eAgNnSc7d1	potřebné
vybavením	vybavení	k1gNnSc7	vybavení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obraceč	obraceč	k1gInSc1	obraceč
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
používá	používat	k5eAaImIp3nS	používat
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
vedlejší	vedlejší	k2eAgFnPc1d1	vedlejší
postav	postav	k1gInSc4	postav
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
dílů	díl	k1gInPc2	díl
série	série	k1gFnSc2	série
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
kritiky	kritika	k1gFnSc2	kritika
je	být	k5eAaImIp3nS	být
neefektivní	efektivní	k2eNgNnSc1d1	neefektivní
kouskování	kouskování	k1gNnSc1	kouskování
strhujícího	strhující	k2eAgInSc2d1	strhující
závěru	závěr	k1gInSc2	závěr
posledního	poslední	k2eAgInSc2d1	poslední
dílu	díl	k1gInSc2	díl
série	série	k1gFnSc2	série
(	(	kIx(	(
<g/>
bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
přerušení	přerušení	k1gNnPc4	přerušení
<g/>
,	,	kIx,	,
bitva	bitva	k1gFnSc1	bitva
v	v	k7c6	v
lese	les	k1gInSc6	les
<g/>
,	,	kIx,	,
přerušení	přerušení	k1gNnSc1	přerušení
bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
školu	škola	k1gFnSc4	škola
<g/>
)	)	kIx)	)
a	a	k8xC	a
jediná	jediný	k2eAgFnSc1d1	jediná
pro	pro	k7c4	pro
čtenáře	čtenář	k1gMnPc4	čtenář
citově	citově	k6eAd1	citově
vážná	vážný	k2eAgFnSc1d1	vážná
událost	událost	k1gFnSc1	událost
-	-	kIx~	-
úmrtí	úmrtí	k1gNnSc1	úmrtí
oblíbené	oblíbený	k2eAgFnSc2d1	oblíbená
sovy	sova	k1gFnSc2	sova
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
kritizován	kritizován	k2eAgInSc4d1	kritizován
jako	jako	k8xS	jako
vševysvětlující	vševysvětlující	k2eAgInSc4d1	vševysvětlující
<g/>
,	,	kIx,	,
utápějící	utápějící	k2eAgInSc4d1	utápějící
se	se	k3xPyFc4	se
ve	v	k7c6	v
faktech	fakt	k1gInPc6	fakt
(	(	kIx(	(
<g/>
o	o	k7c6	o
nečekaných	čekaný	k2eNgInPc6d1	nečekaný
<g/>
,	,	kIx,	,
neobvyklých	obvyklý	k2eNgInPc6d1	neobvyklý
objektech	objekt	k1gInPc6	objekt
a	a	k8xC	a
situacích	situace	k1gFnPc6	situace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kritiky	kritika	k1gFnPc1	kritika
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
jak	jak	k6eAd1	jak
zmatky	zmatek	k1gInPc4	zmatek
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
dílech	díl	k1gInPc6	díl
ságy	sága	k1gFnSc2	sága
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
činí	činit	k5eAaImIp3nS	činit
i	i	k9	i
pro	pro	k7c4	pro
dospělejší	dospělý	k2eAgMnPc4d2	dospělejší
čtenáře	čtenář	k1gMnPc4	čtenář
dílo	dílo	k1gNnSc1	dílo
hůře	zle	k6eAd2	zle
pochopitelné	pochopitelný	k2eAgNnSc1d1	pochopitelné
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
zcela	zcela	k6eAd1	zcela
nadbytečné	nadbytečný	k2eAgInPc1d1	nadbytečný
a	a	k8xC	a
v	v	k7c6	v
celku	celek	k1gInSc6	celek
neorganicky	organicky	k6eNd1	organicky
působící	působící	k2eAgFnSc2d1	působící
části	část	k1gFnSc2	část
filmu	film	k1gInSc2	film
a	a	k8xC	a
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Kritizována	kritizován	k2eAgFnSc1d1	kritizována
je	být	k5eAaImIp3nS	být
jednotná	jednotný	k2eAgFnSc1d1	jednotná
šablona	šablona	k1gFnSc1	šablona
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
zápletek	zápletka	k1gFnPc2	zápletka
i	i	k8xC	i
celých	celý	k2eAgInPc2d1	celý
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Divák	divák	k1gMnSc1	divák
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
čtenář	čtenář	k1gMnSc1	čtenář
nemá	mít	k5eNaImIp3nS	mít
možnost	možnost	k1gFnSc4	možnost
odhadovat	odhadovat	k5eAaImF	odhadovat
a	a	k8xC	a
hledat	hledat	k5eAaImF	hledat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
postavou	postava	k1gFnSc7	postava
řešení	řešení	k1gNnSc2	řešení
<g/>
,	,	kIx,	,
odhalovat	odhalovat	k5eAaImF	odhalovat
<g/>
,	,	kIx,	,
obávat	obávat	k5eAaImF	obávat
se	se	k3xPyFc4	se
předem	předem	k6eAd1	předem
<g/>
.	.	kIx.	.
</s>
<s>
Divák	divák	k1gMnSc1	divák
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
čtenář	čtenář	k1gMnSc1	čtenář
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
roli	role	k1gFnSc6	role
němého	němý	k2eAgMnSc4d1	němý
diváka	divák	k1gMnSc4	divák
vlečeného	vlečený	k2eAgInSc2d1	vlečený
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
nečekané	čekaný	k2eNgFnSc3d1	nečekaná
rekvizitě	rekvizita	k1gFnSc3	rekvizita
a	a	k8xC	a
nečekanému	čekaný	k2eNgNnSc3d1	nečekané
odhalení	odhalení	k1gNnSc3	odhalení
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
díly	díl	k1gInPc1	díl
ságy	sága	k1gFnSc2	sága
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc1	jejich
filmová	filmový	k2eAgNnPc1d1	filmové
zpracování	zpracování	k1gNnPc1	zpracování
jsou	být	k5eAaImIp3nP	být
bezcílnou	bezcílný	k2eAgFnSc7d1	bezcílná
výplní	výplň	k1gFnSc7	výplň
<g/>
,	,	kIx,	,
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
popsat	popsat	k5eAaPmF	popsat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
...	...	k?	...
<g/>
bezdějové	bezdějový	k2eAgNnSc1d1	bezdějové
bloudění	bloudění	k1gNnSc1	bloudění
mezi	mezi	k7c7	mezi
podružnostmi	podružnost	k1gFnPc7	podružnost
potterovského	potterovský	k2eAgInSc2d1	potterovský
světa	svět	k1gInSc2	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
i	i	k8xC	i
kniha	kniha	k1gFnSc1	kniha
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
pouhou	pouhý	k2eAgFnSc4d1	pouhá
iluzi	iluze	k1gFnSc4	iluze
dramatičnosti	dramatičnost	k1gFnSc2	dramatičnost
<g/>
.	.	kIx.	.
</s>
<s>
Výkony	výkon	k1gInPc1	výkon
postav	postava	k1gFnPc2	postava
filmu	film	k1gInSc2	film
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k8xC	jako
amatérské	amatérský	k2eAgInPc1d1	amatérský
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
momentech	moment	k1gInPc6	moment
přepjaté	přepjatý	k2eAgFnSc2d1	přepjatá
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
dětští	dětský	k2eAgMnPc1d1	dětský
herci	herec	k1gMnPc1	herec
oceňováni	oceňován	k2eAgMnPc1d1	oceňován
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
hrají	hrát	k5eAaImIp3nP	hrát
jak	jak	k8xC	jak
jim	on	k3xPp3gMnPc3	on
síly	síla	k1gFnPc4	síla
stačí	stačit	k5eAaBmIp3nP	stačit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
četné	četný	k2eAgFnPc4d1	četná
knižní	knižní	k2eAgFnPc4d1	knižní
parodie	parodie	k1gFnPc4	parodie
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
patří	patřit	k5eAaImIp3nP	patřit
například	například	k6eAd1	například
Barry	Barr	k1gInPc1	Barr
Trotter	Trottra	k1gFnPc2	Trottra
(	(	kIx(	(
<g/>
od	od	k7c2	od
Michaela	Michael	k1gMnSc2	Michael
Gerbera	gerbera	k1gFnSc1	gerbera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Porri	Porri	k1gNnSc1	Porri
Gatter	Gattra	k1gFnPc2	Gattra
(	(	kIx(	(
<g/>
běloruská	běloruský	k2eAgFnSc1d1	Běloruská
řada	řada	k1gFnSc1	řada
knih	kniha	k1gFnPc2	kniha
-	-	kIx~	-
Andreyi	Andreyi	k1gNnSc1	Andreyi
Zhvalevskiyi	Zhvalevskiy	k1gFnSc2	Zhvalevskiy
a	a	k8xC	a
Igor	Igor	k1gMnSc1	Igor
<g/>
'	'	kIx"	'
<g/>
Miyt	Miyt	k1gMnSc1	Miyt
<g/>
'	'	kIx"	'
<g/>
ko	ko	k?	ko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tanya	Tany	k2eAgFnSc1d1	Tanya
Grotterová	Grotterová	k1gFnSc1	Grotterová
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Dmitri	Dmitri	k1gNnSc1	Dmitri
Yemetz	Yemetza	k1gFnPc2	Yemetza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Heri	Heri	k1gNnSc1	Heri
Kókler	Kókler	k1gMnSc1	Kókler
(	(	kIx(	(
<g/>
K.	K.	kA	K.
B.	B.	kA	B.
Rottring	Rottring	k1gInSc1	Rottring
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
maďarsko	maďarsko	k1gNnSc1	maďarsko
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
je	být	k5eAaImIp3nS	být
podepsán	podepsat	k5eAaPmNgMnS	podepsat
pseudonymem	pseudonym	k1gInSc7	pseudonym
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
kromě	kromě	k7c2	kromě
slovní	slovní	k2eAgFnSc2d1	slovní
hříčky	hříčka	k1gFnSc2	hříčka
na	na	k7c4	na
"	"	kIx"	"
<g/>
J.	J.	kA	J.
K.	K.	kA	K.
Rowling	Rowling	k1gInSc4	Rowling
<g/>
"	"	kIx"	"
připomínají	připomínat	k5eAaImIp3nP	připomínat
jeho	jeho	k3xOp3gFnPc4	jeho
iniciály	iniciála	k1gFnPc4	iniciála
zkratku	zkratka	k1gFnSc4	zkratka
"	"	kIx"	"
<g/>
kb	kb	kA	kb
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
znamenající	znamenající	k2eAgInSc1d1	znamenající
"	"	kIx"	"
<g/>
přibližně	přibližně	k6eAd1	přibližně
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
"	"	kIx"	"
<g/>
rotring	rotring	k1gInSc1	rotring
<g/>
"	"	kIx"	"
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
maďarštině	maďarština	k1gFnSc6	maďarština
"	"	kIx"	"
<g/>
propiska	propiska	k1gFnSc1	propiska
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Natočeny	natočen	k2eAgFnPc1d1	natočena
byly	být	k5eAaImAgFnP	být
také	také	k9	také
dvě	dva	k4xCgFnPc1	dva
scénky	scénka	k1gFnPc1	scénka
pro	pro	k7c4	pro
pořad	pořad	k1gInSc4	pořad
Saturday	Saturdaa	k1gFnSc2	Saturdaa
Night	Night	k2eAgInSc4d1	Night
Live	Live	k1gInSc4	Live
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
díle	díl	k1gInSc6	díl
Simpsonů	Simpson	k1gMnPc2	Simpson
se	se	k3xPyFc4	se
Bart	Bart	k1gInSc1	Bart
a	a	k8xC	a
Lisa	Lisa	k1gFnSc1	Lisa
ocitli	ocitnout	k5eAaPmAgMnP	ocitnout
v	v	k7c6	v
rolích	role	k1gFnPc6	role
kouzelnických	kouzelnický	k2eAgMnPc2d1	kouzelnický
učňů	učeň	k1gMnPc2	učeň
a	a	k8xC	a
museli	muset	k5eAaImAgMnP	muset
porazit	porazit	k5eAaPmF	porazit
temného	temný	k2eAgMnSc4d1	temný
čaroděje	čaroděj	k1gMnSc4	čaroděj
Lorda	lord	k1gMnSc4	lord
Montymorta	Montymort	k1gMnSc4	Montymort
(	(	kIx(	(
<g/>
Montgomery	Montgomera	k1gFnSc2	Montgomera
Burns	Burns	k1gInSc1	Burns
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Knižní	knižní	k2eAgFnSc1d1	knižní
česká	český	k2eAgFnSc1d1	Česká
parodie	parodie	k1gFnSc1	parodie
<g/>
:	:	kIx,	:
Harry	Harr	k1gInPc1	Harr
Trottel	Trottela	k1gFnPc2	Trottela
a	a	k8xC	a
kámen	kámen	k1gInSc1	kámen
MUDr.	MUDr.	kA	MUDr.
Tse	Tse	k1gFnSc2	Tse
<g/>
,	,	kIx,	,
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
bylo	být	k5eAaImAgNnS	být
navázáno	navázán	k2eAgNnSc1d1	navázáno
pokračování	pokračování	k1gNnSc1	pokračování
Harry	Harra	k1gFnSc2	Harra
Trottel	Trottela	k1gFnPc2	Trottela
a	a	k8xC	a
Ta	ten	k3xDgNnPc1	ten
jemná	jemný	k2eAgNnPc1d1	jemné
tenata	tenata	k1gNnPc1	tenata
<g/>
,	,	kIx,	,
Harry	Harr	k1gInPc1	Harr
Trottel	Trottela	k1gFnPc2	Trottela
a	a	k8xC	a
posel	posít	k5eAaPmAgMnS	posít
Talibánu	Talibán	k2eAgFnSc4d1	Talibán
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
ještě	ještě	k9	ještě
nenavazující	navazující	k2eNgNnSc4d1	nenavazující
pokračování	pokračování	k1gNnSc4	pokračování
My	my	k3xPp1nPc1	my
děti	dítě	k1gFnPc4	dítě
ze	z	k7c2	z
stanice	stanice	k1gFnSc2	stanice
Kaiserchrocht	Kaiserchrochta	k1gFnPc2	Kaiserchrochta
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
jistě	jistě	k6eAd1	jistě
stojí	stát	k5eAaImIp3nS	stát
také	také	k9	také
internetový	internetový	k2eAgInSc1d1	internetový
fenomén	fenomén	k1gInSc1	fenomén
A	a	k9	a
Very	Very	k1gInPc4	Very
Potter	Potter	k1gMnSc1	Potter
Musical	musical	k1gInSc4	musical
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgInSc4d1	divadelní
muzikál	muzikál	k1gInSc4	muzikál
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
skupinou	skupina	k1gFnSc7	skupina
studentů	student	k1gMnPc2	student
Michiganské	michiganský	k2eAgFnSc2d1	Michiganská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
říká	říkat	k5eAaImIp3nS	říkat
Team	team	k1gInSc4	team
Starkid	Starkida	k1gFnPc2	Starkida
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
představení	představení	k1gNnSc1	představení
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
prvky	prvek	k1gInPc4	prvek
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
volně	volně	k6eAd1	volně
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
navazuje	navazovat	k5eAaImIp3nS	navazovat
pokračování	pokračování	k1gNnSc1	pokračování
A	A	kA	A
Very	Vera	k1gMnSc2	Vera
Potter	Potter	k1gMnSc1	Potter
Sequel	Sequel	k1gMnSc1	Sequel
a	a	k8xC	a
A	a	k8xC	a
Very	Vera	k1gFnSc2	Vera
Potter	Potter	k1gMnSc1	Potter
Senior	senior	k1gMnSc1	senior
Year	Year	k1gMnSc1	Year
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
slovenská	slovenský	k2eAgFnSc1d1	slovenská
internetová	internetový	k2eAgFnSc1d1	internetová
parodie	parodie	k1gFnSc1	parodie
vydávaná	vydávaný	k2eAgFnSc1d1	vydávaná
Studiem	studio	k1gNnSc7	studio
POG	POG	kA	POG
Dano	Dana	k1gFnSc5	Dana
Drevo	Drevo	k1gNnSc1	Drevo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
dvě	dva	k4xCgFnPc4	dva
série	série	k1gFnPc4	série
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
dokončená	dokončený	k2eAgFnSc1d1	dokončená
série	série	k1gFnSc1	série
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
parodován	parodován	k2eAgInSc1d1	parodován
díl	díl	k1gInSc1	díl
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
Tajemná	tajemný	k2eAgFnSc1d1	tajemná
komnata	komnata	k1gFnSc1	komnata
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Dano	Dana	k1gFnSc5	Dana
Drevo	Drevo	k1gNnSc1	Drevo
a	a	k8xC	a
Tajomna	Tajomen	k2eAgFnSc1d1	Tajomen
komnata	komnata	k1gFnSc1	komnata
<g/>
"	"	kIx"	"
má	mít	k5eAaImIp3nS	mít
22	[number]	k4	22
částí	část	k1gFnPc2	část
a	a	k8xC	a
úspěšnější	úspěšný	k2eAgMnSc1d2	úspěšnější
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
série	série	k1gFnSc2	série
"	"	kIx"	"
<g/>
Dano	Dana	k1gFnSc5	Dana
Drevo	Drevo	k1gNnSc4	Drevo
a	a	k8xC	a
Turnaj	turnaj	k1gInSc4	turnaj
Mekyho	Meky	k1gMnSc2	Meky
Žbirku	Žbirek	k1gInSc2	Žbirek
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
parodovaná	parodovaný	k2eAgFnSc1d1	parodovaná
z	z	k7c2	z
částí	část	k1gFnPc2	část
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Ohnivý	ohnivý	k2eAgInSc1d1	ohnivý
pohár	pohár	k1gInSc1	pohár
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2014	[number]	k4	2014
jedenáct	jedenáct	k4xCc1	jedenáct
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Příznivci	příznivec	k1gMnPc1	příznivec
Vladimira	Vladimiro	k1gNnSc2	Vladimiro
Putina	putin	k2eAgInSc2d1	putin
nařknuli	nařknout	k5eAaPmAgMnP	nařknout
tvůrce	tvůrce	k1gMnPc4	tvůrce
filmových	filmový	k2eAgInPc2d1	filmový
adaptací	adaptace	k1gFnSc7	adaptace
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
ztvárnění	ztvárnění	k1gNnSc6	ztvárnění
postavy	postava	k1gFnPc4	postava
skřítka	skřítek	k1gMnSc2	skřítek
Dobbyho	Dobby	k1gMnSc2	Dobby
vyšli	vyjít	k5eAaPmAgMnP	vyjít
ze	z	k7c2	z
vzhledu	vzhled	k1gInSc2	vzhled
tohoto	tento	k3xDgMnSc2	tento
ruského	ruský	k2eAgMnSc2d1	ruský
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Kanadský	kanadský	k2eAgMnSc1d1	kanadský
exministr	exministr	k1gMnSc1	exministr
Pierre	Pierr	k1gInSc5	Pierr
Pettigrew	Pettigrew	k1gFnPc7	Pettigrew
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
Harrymu	Harrym	k1gInSc2	Harrym
zrádce	zrádce	k1gMnSc2	zrádce
jeho	jeho	k3xOp3gMnPc2	jeho
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Pettigrew	Pettigrew	k1gMnSc1	Pettigrew
<g/>
.	.	kIx.	.
</s>
<s>
Přezdívku	přezdívka	k1gFnSc4	přezdívka
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
dostala	dostat	k5eAaPmAgFnS	dostat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
osobností	osobnost	k1gFnPc2	osobnost
-	-	kIx~	-
nizozemský	nizozemský	k2eAgMnSc1d1	nizozemský
premiér	premiér	k1gMnSc1	premiér
Jan	Jan	k1gMnSc1	Jan
Peter	Peter	k1gMnSc1	Peter
Balkenende	Balkenend	k1gInSc5	Balkenend
<g/>
,	,	kIx,	,
bulharský	bulharský	k2eAgMnSc1d1	bulharský
politik	politik	k1gMnSc1	politik
Nikolay	Nikolaa	k1gFnSc2	Nikolaa
Vassilev	Vassilev	k1gMnSc1	Vassilev
nebo	nebo	k8xC	nebo
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
skokan	skokan	k1gMnSc1	skokan
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
Simon	Simon	k1gMnSc1	Simon
Ammann	Ammann	k1gMnSc1	Ammann
<g/>
.	.	kIx.	.
</s>
<s>
Série	série	k1gFnSc1	série
o	o	k7c4	o
Harry	Harra	k1gFnPc4	Harra
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
sporů	spor	k1gInPc2	spor
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Americké	americký	k2eAgFnSc2d1	americká
asociace	asociace	k1gFnSc2	asociace
knihoven	knihovna	k1gFnPc2	knihovna
patří	patřit	k5eAaImIp3nS	patřit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
tyto	tento	k3xDgInPc4	tento
knihy	kniha	k1gFnPc1	kniha
mezi	mezi	k7c4	mezi
nejžádanější	žádaný	k2eAgNnSc4d3	nejžádanější
<g/>
.	.	kIx.	.
</s>
<s>
Stížnosti	stížnost	k1gFnPc1	stížnost
směřují	směřovat	k5eAaImIp3nP	směřovat
často	často	k6eAd1	často
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
knihy	kniha	k1gFnPc1	kniha
podporují	podporovat	k5eAaImIp3nP	podporovat
okultismus	okultismus	k1gInSc4	okultismus
<g/>
,	,	kIx,	,
satanismus	satanismus	k1gInSc4	satanismus
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
násilnické	násilnický	k2eAgInPc1d1	násilnický
a	a	k8xC	a
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
motivy	motiv	k1gInPc4	motiv
kritizující	kritizující	k2eAgInSc4d1	kritizující
rodinný	rodinný	k2eAgInSc4d1	rodinný
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
křesťanské	křesťanský	k2eAgInPc1d1	křesťanský
spolky	spolek	k1gInPc1	spolek
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
zavrhují	zavrhovat	k5eAaImIp3nP	zavrhovat
coby	coby	k?	coby
přímou	přímý	k2eAgFnSc4d1	přímá
propagaci	propagace	k1gFnSc4	propagace
čarodějnictví	čarodějnictví	k1gNnSc2	čarodějnictví
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc4	kniha
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
i	i	k9	i
bývalý	bývalý	k2eAgMnSc1d1	bývalý
papež	papež	k1gMnSc1	papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
ještě	ještě	k9	ještě
kardinálem	kardinál	k1gMnSc7	kardinál
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
řada	řada	k1gFnSc1	řada
církevních	církevní	k2eAgMnPc2d1	církevní
hodnostářů	hodnostář	k1gMnPc2	hodnostář
knihy	kniha	k1gFnSc2	kniha
naopak	naopak	k6eAd1	naopak
podpořila	podpořit	k5eAaPmAgFnS	podpořit
<g/>
.	.	kIx.	.
</s>
<s>
Stížnosti	stížnost	k1gFnPc1	stížnost
věřících	věřící	k1gMnPc2	věřící
byly	být	k5eAaImAgFnP	být
parodovány	parodován	k2eAgFnPc4d1	parodována
např.	např.	kA	např.
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Simpsonovi	Simpson	k1gMnSc3	Simpson
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
věrný	věrný	k2eAgMnSc1d1	věrný
křesťan	křesťan	k1gMnSc1	křesťan
Ned	Ned	k1gFnSc2	Ned
Flanders	Flanders	k1gInSc1	Flanders
předčítá	předčítat	k5eAaImIp3nS	předčítat
Harryho	Harry	k1gMnSc4	Harry
Pottera	Potter	k1gMnSc4	Potter
svým	svůj	k3xOyFgFnPc3	svůj
dětem	dítě	k1gFnPc3	dítě
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
příběh	příběh	k1gInSc1	příběh
uzavírá	uzavírat	k5eAaPmIp3nS	uzavírat
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
a	a	k8xC	a
tak	tak	k9	tak
se	se	k3xPyFc4	se
Harry	Harr	k1gInPc7	Harr
Potter	Pottra	k1gFnPc2	Pottra
propadl	propadnout	k5eAaPmAgMnS	propadnout
se	s	k7c7	s
všemi	všecek	k3xTgMnPc7	všecek
svými	svůj	k3xOyFgMnPc7	svůj
kouzelnickými	kouzelnický	k2eAgMnPc7d1	kouzelnický
kamarády	kamarád	k1gMnPc7	kamarád
do	do	k7c2	do
pekla	peklo	k1gNnSc2	peklo
za	za	k7c4	za
provozování	provozování	k1gNnSc4	provozování
čarodějnictví	čarodějnictví	k1gNnSc2	čarodějnictví
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Synové	syn	k1gMnPc1	syn
pak	pak	k6eAd1	pak
zatleskají	zatleskat	k5eAaPmIp3nP	zatleskat
a	a	k8xC	a
Ned	Ned	k1gMnPc1	Ned
vhodí	vhodit	k5eAaPmIp3nP	vhodit
knihu	kniha	k1gFnSc4	kniha
do	do	k7c2	do
krbu	krb	k1gInSc2	krb
<g/>
.	.	kIx.	.
</s>
<s>
Světem	svět	k1gInSc7	svět
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
početná	početný	k2eAgFnSc1d1	početná
komunita	komunita	k1gFnSc1	komunita
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
,	,	kIx,	,
uznávaná	uznávaný	k2eAgFnSc1d1	uznávaná
i	i	k9	i
samotnou	samotný	k2eAgFnSc7d1	samotná
autorkou	autorka	k1gFnSc7	autorka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
kratochvíle	kratochvíle	k1gFnPc4	kratochvíle
oddaných	oddaný	k2eAgMnPc2d1	oddaný
českých	český	k2eAgMnPc2d1	český
čtenářů	čtenář	k1gMnPc2	čtenář
patří	patřit	k5eAaImIp3nS	patřit
psaní	psaní	k1gNnSc4	psaní
vlastních	vlastní	k2eAgInPc2d1	vlastní
příběhů	příběh	k1gInPc2	příběh
ze	z	k7c2	z
světa	svět	k1gInSc2	svět
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
-	-	kIx~	-
tzv.	tzv.	kA	tzv.
fanfiction	fanfiction	k1gInSc1	fanfiction
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
český	český	k2eAgMnSc1d1	český
"	"	kIx"	"
<g/>
Harry	Harr	k1gInPc1	Harr
Potter	Pottrum	k1gNnPc2	Pottrum
a	a	k8xC	a
Pán	pán	k1gMnSc1	pán
smrti	smrt	k1gFnSc2	smrt
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
alternativa	alternativa	k1gFnSc1	alternativa
k	k	k7c3	k
6	[number]	k4	6
<g/>
.	.	kIx.	.
dílu	díl	k1gInSc2	díl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čítající	čítající	k2eAgFnSc1d1	čítající
přes	přes	k7c4	přes
dva	dva	k4xCgInPc4	dva
tisíce	tisíc	k4xCgInPc1	tisíc
stran	strana	k1gFnPc2	strana
o	o	k7c6	o
stech	sto	k4xCgNnPc6	sto
kapitolách	kapitola	k1gFnPc6	kapitola
<g/>
,	,	kIx,	,
na	na	k7c4	na
nějž	jenž	k3xRgMnSc4	jenž
navazuje	navazovat	k5eAaImIp3nS	navazovat
pokračování	pokračování	k1gNnSc1	pokračování
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
Pán	pán	k1gMnSc1	pán
Pána	pán	k1gMnSc2	pán
smrti	smrt	k1gFnSc2	smrt
<g/>
<g />
.	.	kIx.	.
</s>
<s>
"	"	kIx"	"
mající	mající	k2eAgMnPc1d1	mající
nyní	nyní	k6eAd1	nyní
již	již	k9	již
dvanáct	dvanáct	k4xCc4	dvanáct
set	set	k1gInSc4	set
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
slovenská	slovenský	k2eAgFnSc1d1	slovenská
trilogie	trilogie	k1gFnSc1	trilogie
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
Zlatá	zlatý	k2eAgFnSc1d1	zlatá
zlatá	zlatý	k2eAgFnSc1d1	zlatá
strela	strela	k1gFnSc1	strela
(	(	kIx(	(
<g/>
začínající	začínající	k2eAgInPc1d1	začínající
pozměněnými	pozměněný	k2eAgFnPc7d1	pozměněná
událostmi	událost	k1gFnPc7	událost
na	na	k7c6	na
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
na	na	k7c6	na
konci	konec	k1gInSc6	konec
5	[number]	k4	5
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
180	[number]	k4	180
kapitol	kapitola	k1gFnPc2	kapitola
a	a	k8xC	a
necelých	celý	k2eNgNnPc2d1	necelé
1.200.000	[number]	k4	1.200.000
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
"	"	kIx"	"
;	;	kIx,	;
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
"	"	kIx"	"
Příznivci	příznivec	k1gMnPc1	příznivec
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
se	se	k3xPyFc4	se
také	také	k9	také
scházejí	scházet	k5eAaImIp3nP	scházet
a	a	k8xC	a
setkávají	setkávat	k5eAaImIp3nP	setkávat
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
tzv.	tzv.	kA	tzv.
internetových	internetový	k2eAgFnPc2d1	internetová
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
sami	sám	k3xTgMnPc1	sám
stávají	stávat	k5eAaImIp3nP	stávat
studenty	student	k1gMnPc7	student
školy	škola	k1gFnSc2	škola
čar	čára	k1gFnPc2	čára
a	a	k8xC	a
kouzel	kouzlo	k1gNnPc2	kouzlo
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
a	a	k8xC	a
zažívají	zažívat	k5eAaImIp3nP	zažívat
přitom	přitom	k6eAd1	přitom
různá	různý	k2eAgNnPc1d1	různé
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
školy	škola	k1gFnPc1	škola
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c6	na
principu	princip	k1gInSc6	princip
her	hra	k1gFnPc2	hra
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
výjimky	výjimka	k1gFnPc1	výjimka
potvrzující	potvrzující	k2eAgNnSc1d1	potvrzující
pravidlo	pravidlo	k1gNnSc1	pravidlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
hráč	hráč	k1gMnSc1	hráč
si	se	k3xPyFc3	se
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
svojí	svojit	k5eAaImIp3nS	svojit
postavu	postav	k1gInSc2	postav
a	a	k8xC	a
s	s	k7c7	s
tou	ten	k3xDgFnSc7	ten
pak	pak	k6eAd1	pak
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
školní	školní	k2eAgNnSc4d1	školní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
mnohdy	mnohdy	k6eAd1	mnohdy
reprezentováno	reprezentovat	k5eAaImNgNnS	reprezentovat
chatovacími	chatovací	k2eAgFnPc7d1	chatovací
místnostmi	místnost	k1gFnPc7	místnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hráči	hráč	k1gMnPc1	hráč
prezentují	prezentovat	k5eAaBmIp3nP	prezentovat
své	svůj	k3xOyFgFnPc4	svůj
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc1	jejich
řeč	řeč	k1gFnSc1	řeč
i	i	k8xC	i
aktivity	aktivita	k1gFnSc2	aktivita
textem	text	k1gInSc7	text
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
školy	škola	k1gFnPc1	škola
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
liší	lišit	k5eAaImIp3nP	lišit
jak	jak	k8xS	jak
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
herního	herní	k2eAgNnSc2d1	herní
prostředí	prostředí	k1gNnSc2	prostředí
(	(	kIx(	(
<g/>
mezihry	mezihra	k1gFnPc1	mezihra
<g/>
,	,	kIx,	,
doplňky	doplněk	k1gInPc1	doplněk
<g/>
,	,	kIx,	,
RPG	RPG	kA	RPG
<g/>
,	,	kIx,	,
výuka	výuka	k1gFnSc1	výuka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
časovým	časový	k2eAgInSc7d1	časový
rámcem	rámec	k1gInSc7	rámec
(	(	kIx(	(
<g/>
před	před	k7c7	před
knihami	kniha	k1gFnPc7	kniha
<g/>
,	,	kIx,	,
aktuální	aktuální	k2eAgFnSc1d1	aktuální
současnost	současnost	k1gFnSc1	současnost
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
počtem	počet	k1gInSc7	počet
herních	herní	k2eAgInPc2d1	herní
školních	školní	k2eAgInPc2d1	školní
roků	rok	k1gInPc2	rok
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
reálný	reálný	k2eAgInSc4d1	reálný
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
rozdílné	rozdílný	k2eAgNnSc4d1	rozdílné
tempo	tempo	k1gNnSc4	tempo
herního	herní	k2eAgInSc2d1	herní
času	čas	k1gInSc2	čas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
autorka	autorka	k1gFnSc1	autorka
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
stránek	stránka	k1gFnPc2	stránka
Pottermore	Pottermor	k1gMnSc5	Pottermor
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
fanouškům	fanoušek	k1gMnPc3	fanoušek
nabízejí	nabízet	k5eAaImIp3nP	nabízet
možnost	možnost	k1gFnSc4	možnost
stát	stát	k5eAaImF	stát
se	s	k7c7	s
studenty	student	k1gMnPc7	student
Bradavic	bradavice	k1gFnPc2	bradavice
<g/>
,	,	kIx,	,
zažít	zažít	k5eAaPmF	zažít
příběh	příběh	k1gInSc4	příběh
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
kůži	kůže	k1gFnSc4	kůže
a	a	k8xC	a
dozvědět	dozvědět	k5eAaPmF	dozvědět
se	se	k3xPyFc4	se
něco	něco	k3yInSc4	něco
nového	nový	k2eAgMnSc4d1	nový
o	o	k7c6	o
světě	svět	k1gInSc6	svět
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
takovýchto	takovýto	k3xDgInPc2	takovýto
škol	škola	k1gFnPc2	škola
hned	hned	k6eAd1	hned
několik	několik	k4yIc1	několik
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgInPc4d3	veliký
takové	takový	k3xDgInPc4	takový
projekty	projekt	k1gInPc4	projekt
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Hogwarts	Hogwarts	k1gInSc1	Hogwarts
Online	Onlin	k1gInSc5	Onlin
CZ	CZ	kA	CZ
(	(	kIx(	(
<g/>
HOCZ	HOCZ	kA	HOCZ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
E-Bradavice	E-Bradavice	k1gFnSc1	E-Bradavice
<g/>
,	,	kIx,	,
Hogwarts	Hogwarts	k1gInSc1	Hogwarts
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
CRUCIO	CRUCIO	kA	CRUCIO
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
BradaviceCity	BradaviceCit	k1gInPc1	BradaviceCit
<g/>
,	,	kIx,	,
Bradavice	bradavice	k1gFnSc1	bradavice
online	onlinout	k5eAaPmIp3nS	onlinout
(	(	kIx(	(
<g/>
BRADON	BRADON	kA	BRADON
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hrad	hrad	k1gInSc1	hrad
Bradavice	bradavice	k1gFnSc2	bradavice
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
těchto	tento	k3xDgInPc2	tento
projektů	projekt	k1gInPc2	projekt
zde	zde	k6eAd1	zde
fungují	fungovat	k5eAaImIp3nP	fungovat
i	i	k9	i
projekty	projekt	k1gInPc1	projekt
poněkud	poněkud	k6eAd1	poněkud
odlišné	odlišný	k2eAgInPc1d1	odlišný
-	-	kIx~	-
Život	život	k1gInSc1	život
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
,	,	kIx,	,
Svět	svět	k1gInSc1	svět
kouzel	kouzlo	k1gNnPc2	kouzlo
a	a	k8xC	a
Bradavice-online	Bradavicenlin	k1gInSc5	Bradavice-onlin
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
škol	škola	k1gFnPc2	škola
nejsou	být	k5eNaImIp3nP	být
webovou	webový	k2eAgFnSc7d1	webová
textovou	textový	k2eAgFnSc7d1	textová
školou	škola	k1gFnSc7	škola
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
se	se	k3xPyFc4	se
v	v	k7c6	v
interaktivním	interaktivní	k2eAgNnSc6d1	interaktivní
grafickém	grafický	k2eAgNnSc6d1	grafické
prostředí	prostředí	k1gNnSc6	prostředí
a	a	k8xC	a
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
nainstalování	nainstalování	k1gNnSc4	nainstalování
dodatečného	dodatečný	k2eAgInSc2d1	dodatečný
software	software	k1gInSc1	software
<g/>
.	.	kIx.	.
</s>
<s>
Sága	sága	k1gFnSc1	sága
o	o	k7c4	o
Harry	Harra	k1gFnPc4	Harra
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
významně	významně	k6eAd1	významně
zasáhla	zasáhnout	k5eAaPmAgNnP	zasáhnout
i	i	k9	i
do	do	k7c2	do
PC	PC	kA	PC
herní	herní	k2eAgFnSc2d1	herní
scény	scéna	k1gFnSc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
každého	každý	k3xTgInSc2	každý
filmového	filmový	k2eAgInSc2d1	filmový
dílu	díl	k1gInSc2	díl
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
hra	hra	k1gFnSc1	hra
pod	pod	k7c7	pod
křídly	křídlo	k1gNnPc7	křídlo
vydavatele	vydavatel	k1gMnSc2	vydavatel
Electronic	Electronice	k1gFnPc2	Electronice
Arts	Artsa	k1gFnPc2	Artsa
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
celkem	celkem	k6eAd1	celkem
o	o	k7c4	o
9	[number]	k4	9
titulů	titul	k1gInPc2	titul
<g/>
:	:	kIx,	:
2001	[number]	k4	2001
-	-	kIx~	-
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Kámen	kámen	k1gInSc1	kámen
mudrců	mudrc	k1gMnPc2	mudrc
(	(	kIx(	(
<g/>
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gInSc1	Potter
and	and	k?	and
the	the	k?	the
Philisopher	Philisophra	k1gFnPc2	Philisophra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
/	/	kIx~	/
Sorcerer	Sorcerer	k1gMnSc1	Sorcerer
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
stone	ston	k1gInSc5	ston
<g/>
)	)	kIx)	)
2002	[number]	k4	2002
-	-	kIx~	-
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Tajemná	tajemný	k2eAgFnSc1d1	tajemná
komnata	komnata	k1gFnSc1	komnata
(	(	kIx(	(
<g/>
Harry	Harr	k1gInPc1	Harr
Potter	Pottra	k1gFnPc2	Pottra
and	and	k?	and
the	the	k?	the
Chamber	Chamber	k1gInSc1	Chamber
of	of	k?	of
Secrets	Secrets	k1gInSc1	Secrets
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
2003	[number]	k4	2003
˗	˗	k?	˗
<g/>
̶	̶	k?	̶
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
famfrpálu	famfrpál	k1gInSc6	famfrpál
(	(	kIx(	(
<g/>
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
and	and	k?	and
the	the	k?	the
Quiditch	Quiditch	k1gInSc1	Quiditch
world	world	k1gMnSc1	world
cup	cup	k1gInSc1	cup
<g/>
)	)	kIx)	)
2004	[number]	k4	2004
-	-	kIx~	-
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
Vězeň	vězeň	k1gMnSc1	vězeň
z	z	k7c2	z
Azkabanu	Azkaban	k1gInSc2	Azkaban
(	(	kIx(	(
<g/>
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
and	and	k?	and
the	the	k?	the
Prisoner	Prisoner	k1gMnSc1	Prisoner
of	of	k?	of
Azkaban	Azkaban	k1gMnSc1	Azkaban
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
-	-	kIx~	-
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Ohnivý	ohnivý	k2eAgInSc1d1	ohnivý
pohár	pohár	k1gInSc1	pohár
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gInSc1	Potter
and	and	k?	and
the	the	k?	the
Goblet	Goblet	k1gInSc1	Goblet
of	of	k?	of
fire	fire	k1gInSc1	fire
<g/>
)	)	kIx)	)
2007	[number]	k4	2007
-	-	kIx~	-
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Fénixův	fénixův	k2eAgInSc1d1	fénixův
řád	řád	k1gInSc1	řád
(	(	kIx(	(
<g/>
Harry	Harr	k1gInPc1	Harr
Potter	Pottra	k1gFnPc2	Pottra
and	and	k?	and
the	the	k?	the
Order	Order	k1gInSc1	Order
of	of	k?	of
Phoenix	Phoenix	k1gInSc1	Phoenix
<g/>
)	)	kIx)	)
2009	[number]	k4	2009
-	-	kIx~	-
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
Princ	princ	k1gMnSc1	princ
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
(	(	kIx(	(
<g/>
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gInSc1	Potter
and	and	k?	and
the	the	k?	the
Half	halfa	k1gFnPc2	halfa
Blood	Blooda	k1gFnPc2	Blooda
Prince	princ	k1gMnSc2	princ
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
-	-	kIx~	-
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Relikvie	relikvie	k1gFnSc1	relikvie
smrti	smrt	k1gFnSc2	smrt
1	[number]	k4	1
(	(	kIx(	(
<g/>
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
and	and	k?	and
the	the	k?	the
Deathly	Deathly	k1gFnSc1	Deathly
Hallows	Hallows	k1gInSc1	Hallows
part	part	k1gInSc1	part
1	[number]	k4	1
<g/>
)	)	kIx)	)
2011	[number]	k4	2011
-	-	kIx~	-
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Relikvie	relikvie	k1gFnSc1	relikvie
smrti	smrt	k1gFnSc2	smrt
2	[number]	k4	2
(	(	kIx(	(
<g/>
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
and	and	k?	and
the	the	k?	the
Deathly	Deathly	k1gFnSc1	Deathly
Hallows	Hallows	k1gInSc1	Hallows
part	part	k1gInSc1	part
2	[number]	k4	2
<g/>
)	)	kIx)	)
První	první	k4xOgFnSc1	první
čtveřice	čtveřice	k1gFnSc1	čtveřice
her	hra	k1gFnPc2	hra
byla	být	k5eAaImAgFnS	být
kritiky	kritika	k1gFnSc2	kritika
přijata	přijmout	k5eAaPmNgFnS	přijmout
vesměs	vesměs	k6eAd1	vesměs
pozitivně	pozitivně	k6eAd1	pozitivně
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
díl	díl	k1gInSc1	díl
obdržel	obdržet	k5eAaPmAgInS	obdržet
na	na	k7c6	na
metacritic	metacritice	k1gFnPc2	metacritice
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
nadprůměrné	nadprůměrný	k2eAgNnSc4d1	nadprůměrné
hodnocení	hodnocení	k1gNnSc4	hodnocení
65	[number]	k4	65
%	%	kIx~	%
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
77	[number]	k4	77
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Vězeň	vězeň	k1gMnSc1	vězeň
z	z	k7c2	z
Azkabanu	Azkaban	k1gInSc2	Azkaban
obdržel	obdržet	k5eAaPmAgInS	obdržet
průměrně	průměrně	k6eAd1	průměrně
67	[number]	k4	67
%	%	kIx~	%
a	a	k8xC	a
Ohnivý	ohnivý	k2eAgInSc4d1	ohnivý
pohár	pohár	k1gInSc4	pohár
66	[number]	k4	66
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pátý	pátý	k4xOgInSc1	pátý
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
Fénixův	fénixův	k2eAgInSc1d1	fénixův
řád	řád	k1gInSc1	řád
si	se	k3xPyFc3	se
odnesl	odnést	k5eAaPmAgInS	odnést
hodnocení	hodnocení	k1gNnSc4	hodnocení
63	[number]	k4	63
%	%	kIx~	%
a	a	k8xC	a
Princ	princ	k1gMnSc1	princ
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
64	[number]	k4	64
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kritiků	kritik	k1gMnPc2	kritik
pak	pak	k6eAd1	pak
propadla	propadnout	k5eAaPmAgFnS	propadnout
zejména	zejména	k9	zejména
poslední	poslední	k2eAgFnSc1d1	poslední
dvojice	dvojice	k1gFnSc1	dvojice
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
získaly	získat	k5eAaPmAgFnP	získat
pouze	pouze	k6eAd1	pouze
37	[number]	k4	37
%	%	kIx~	%
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
43	[number]	k4	43
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Hratelnost	hratelnost	k1gFnSc1	hratelnost
se	se	k3xPyFc4	se
u	u	k7c2	u
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
dílů	díl	k1gInPc2	díl
výrazně	výrazně	k6eAd1	výrazně
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
všechny	všechen	k3xTgFnPc1	všechen
sledují	sledovat	k5eAaImIp3nP	sledovat
dějové	dějový	k2eAgFnPc1d1	dějová
linie	linie	k1gFnPc1	linie
příslušných	příslušný	k2eAgFnPc2d1	příslušná
knížek	knížka	k1gFnPc2	knížka
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
tři	tři	k4xCgFnPc4	tři
hry	hra	k1gFnPc4	hra
jsou	být	k5eAaImIp3nP	být
tradiční	tradiční	k2eAgFnPc4d1	tradiční
akční	akční	k2eAgFnPc4d1	akční
adventury	adventura	k1gFnPc4	adventura
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
hrají	hrát	k5eAaImIp3nP	hrát
velkou	velká	k1gFnSc4	velká
roli	role	k1gFnSc4	role
logické	logický	k2eAgFnSc2d1	logická
hádanky	hádanka	k1gFnSc2	hádanka
a	a	k8xC	a
objevování	objevování	k1gNnSc2	objevování
bradavické	bradavický	k2eAgFnSc2d1	bradavická
školy	škola	k1gFnSc2	škola
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
dílu	díl	k1gInSc6	díl
je	být	k5eAaImIp3nS	být
přidány	přidán	k2eAgInPc1d1	přidán
populární	populární	k2eAgInPc1d1	populární
RPG	RPG	kA	RPG
prvky	prvek	k1gInPc1	prvek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výrazně	výrazně	k6eAd1	výrazně
byla	být	k5eAaImAgFnS	být
omezena	omezit	k5eAaPmNgFnS	omezit
svoboda	svoboda	k1gFnSc1	svoboda
pohybu	pohyb	k1gInSc2	pohyb
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
herní	herní	k2eAgInSc1d1	herní
svět	svět	k1gInSc1	svět
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
otevřený	otevřený	k2eAgInSc1d1	otevřený
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
předchozích	předchozí	k2eAgFnPc6d1	předchozí
hrách	hra	k1gFnPc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Pátý	pátý	k4xOgMnSc1	pátý
i	i	k8xC	i
šestý	šestý	k4xOgInSc1	šestý
díl	díl	k1gInSc1	díl
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vrátily	vrátit	k5eAaPmAgFnP	vrátit
k	k	k7c3	k
otevřenému	otevřený	k2eAgInSc3d1	otevřený
světu	svět	k1gInSc3	svět
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
hráč	hráč	k1gMnSc1	hráč
odměňován	odměňován	k2eAgMnSc1d1	odměňován
za	za	k7c4	za
aktivní	aktivní	k2eAgNnSc4d1	aktivní
prohledávání	prohledávání	k1gNnSc4	prohledávání
Bradavic	bradavice	k1gFnPc2	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
odlišné	odlišný	k2eAgInPc1d1	odlišný
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
dva	dva	k4xCgInPc1	dva
zbývající	zbývající	k2eAgInPc1d1	zbývající
tituly	titul	k1gInPc1	titul
podle	podle	k7c2	podle
poslední	poslední	k2eAgFnSc2d1	poslední
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
dvou	dva	k4xCgInPc2	dva
posledních	poslední	k2eAgInPc2d1	poslední
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
jsou	být	k5eAaImIp3nP	být
více	hodně	k6eAd2	hodně
orientovány	orientovat	k5eAaBmNgInP	orientovat
na	na	k7c4	na
dospělé	dospělý	k2eAgNnSc4d1	dospělé
publikum	publikum	k1gNnSc4	publikum
a	a	k8xC	a
dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
zařadit	zařadit	k5eAaPmF	zařadit
do	do	k7c2	do
žánrů	žánr	k1gInPc2	žánr
stříleček	střílečka	k1gFnPc2	střílečka
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
třetí	třetí	k4xOgFnSc2	třetí
osoby	osoba	k1gFnSc2	osoba
s	s	k7c7	s
intenzivním	intenzivní	k2eAgNnSc7d1	intenzivní
využíváním	využívání	k1gNnSc7	využívání
systému	systém	k1gInSc2	systém
krytí	krytí	k1gNnSc2	krytí
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
zpopularizovala	zpopularizovat	k5eAaPmAgFnS	zpopularizovat
videoherní	videoherní	k2eAgFnSc1d1	videoherní
série	série	k1gFnSc1	série
Gears	Gearsa	k1gFnPc2	Gearsa
of	of	k?	of
War	War	k1gFnPc2	War
<g/>
.	.	kIx.	.
</s>
<s>
Vydavatel	vydavatel	k1gMnSc1	vydavatel
Warner	Warner	k1gMnSc1	Warner
Bros	Bros	k1gInSc4	Bros
<g/>
.	.	kIx.	.
</s>
<s>
Interactive	Interactiv	k1gInSc5	Interactiv
Entertainment	Entertainment	k1gInSc1	Entertainment
pod	pod	k7c7	pod
svou	svůj	k3xOyFgFnSc7	svůj
značkou	značka	k1gFnSc7	značka
Lego	lego	k1gNnSc1	lego
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
dvě	dva	k4xCgFnPc4	dva
hry	hra	k1gFnPc4	hra
podle	podle	k7c2	podle
knih	kniha	k1gFnPc2	kniha
o	o	k7c4	o
Harry	Harra	k1gFnPc4	Harra
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
<g/>
.	.	kIx.	.
</s>
<s>
Lego	lego	k1gNnSc1	lego
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
<g/>
:	:	kIx,	:
Years	Years	k1gInSc4	Years
1-4	[number]	k4	1-4
Lego	lego	k1gNnSc4	lego
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
<g/>
:	:	kIx,	:
Years	Years	k1gInSc1	Years
5-7	[number]	k4	5-7
Obě	dva	k4xCgFnPc1	dva
hry	hra	k1gFnPc1	hra
jsou	být	k5eAaImIp3nP	být
typickými	typický	k2eAgMnPc7d1	typický
zástupci	zástupce	k1gMnPc7	zástupce
Lego	lego	k1gNnSc1	lego
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
kritikou	kritika	k1gFnSc7	kritika
i	i	k8xC	i
herní	herní	k2eAgFnSc7d1	herní
veřejností	veřejnost	k1gFnSc7	veřejnost
přijímány	přijímat	k5eAaImNgInP	přijímat
velmi	velmi	k6eAd1	velmi
kladně	kladně	k6eAd1	kladně
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
podporují	podporovat	k5eAaImIp3nP	podporovat
kooperaci	kooperace	k1gFnSc4	kooperace
u	u	k7c2	u
jednoho	jeden	k4xCgInSc2	jeden
počítače	počítač	k1gInSc2	počítač
a	a	k8xC	a
soustředí	soustředit	k5eAaPmIp3nS	soustředit
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
sbírání	sbírání	k1gNnSc6	sbírání
<g/>
,	,	kIx,	,
objevování	objevování	k1gNnSc6	objevování
a	a	k8xC	a
řešení	řešení	k1gNnSc6	řešení
logických	logický	k2eAgFnPc2d1	logická
hádanek	hádanka	k1gFnPc2	hádanka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
obě	dva	k4xCgFnPc4	dva
hry	hra	k1gFnPc4	hra
ze	z	k7c2	z
série	série	k1gFnSc2	série
Lego	lego	k1gNnSc1	lego
Harry	Harra	k1gFnSc2	Harra
Potter	Pottra	k1gFnPc2	Pottra
existují	existovat	k5eAaImIp3nP	existovat
volně	volně	k6eAd1	volně
dostupné	dostupný	k2eAgFnPc1d1	dostupná
demoverze	demoverze	k1gFnPc1	demoverze
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
jsou	být	k5eAaImIp3nP	být
zpracovány	zpracovat	k5eAaPmNgInP	zpracovat
spíše	spíše	k9	spíše
vtipně	vtipně	k6eAd1	vtipně
<g/>
,	,	kIx,	,
celé	celý	k2eAgNnSc4d1	celé
prostředí	prostředí	k1gNnSc4	prostředí
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
z	z	k7c2	z
lega	lego	k1gNnSc2	lego
<g/>
.	.	kIx.	.
</s>
<s>
Data	datum	k1gNnPc1	datum
v	v	k7c4	v
Harrym	Harrym	k1gInSc4	Harrym
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
Kouzla	kouzlo	k1gNnPc4	kouzlo
z	z	k7c2	z
knih	kniha	k1gFnPc2	kniha
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
Seznam	seznam	k1gInSc1	seznam
postav	postava	k1gFnPc2	postava
z	z	k7c2	z
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
Kouzelné	kouzelný	k2eAgInPc1d1	kouzelný
předměty	předmět	k1gInPc1	předmět
z	z	k7c2	z
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
Škola	škola	k1gFnSc1	škola
čar	čára	k1gFnPc2	čára
a	a	k8xC	a
kouzel	kouzlo	k1gNnPc2	kouzlo
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
Fantastická	fantastický	k2eAgNnPc1d1	fantastické
zvířata	zvíře	k1gNnPc1	zvíře
a	a	k8xC	a
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
najít	najít	k5eAaPmF	najít
</s>
