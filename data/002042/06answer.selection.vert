<s>
Od	od	k7c2	od
publikace	publikace	k1gFnSc2	publikace
první	první	k4xOgFnSc2	první
knihy	kniha	k1gFnSc2	kniha
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
kámen	kámen	k1gInSc1	kámen
mudrců	mudrc	k1gMnPc2	mudrc
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
and	and	k?	and
the	the	k?	the
Philosopher	Philosophra	k1gFnPc2	Philosophra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Stone	ston	k1gInSc5	ston
<g/>
)	)	kIx)	)
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
romány	román	k1gInPc1	román
získaly	získat	k5eAaPmAgInP	získat
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
popularitu	popularita	k1gFnSc4	popularita
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
a	a	k8xC	a
dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
i	i	k9	i
velmi	velmi	k6eAd1	velmi
dobrého	dobrý	k2eAgNnSc2d1	dobré
přijetí	přijetí	k1gNnSc2	přijetí
u	u	k7c2	u
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
