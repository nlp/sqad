<s>
Buvol	buvol	k1gMnSc1	buvol
africký	africký	k2eAgMnSc1d1	africký
(	(	kIx(	(
<g/>
Syncerus	Syncerus	k1gMnSc1	Syncerus
caffer	caffer	k1gMnSc1	caffer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nepřesně	přesně	k6eNd1	přesně
označovaný	označovaný	k2eAgInSc1d1	označovaný
také	také	k9	také
jako	jako	k9	jako
buvol	buvol	k1gMnSc1	buvol
kaferský	kaferský	k2eAgMnSc1d1	kaferský
je	být	k5eAaImIp3nS	být
mohutný	mohutný	k2eAgMnSc1d1	mohutný
býložravec	býložravec	k1gMnSc1	býložravec
afrických	africký	k2eAgFnPc2d1	africká
savan	savana	k1gFnPc2	savana
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
domovinou	domovina	k1gFnSc7	domovina
jsou	být	k5eAaImIp3nP	být
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
oblasti	oblast	k1gFnPc1	oblast
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
.	.	kIx.	.
</s>
<s>
Stavbou	stavba	k1gFnSc7	stavba
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
domácímu	domácí	k2eAgInSc3d1	domácí
skotu	skot	k1gInSc3	skot
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
poměrně	poměrně	k6eAd1	poměrně
nízké	nízký	k2eAgFnPc4d1	nízká
končetiny	končetina	k1gFnPc4	končetina
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
mohutný	mohutný	k2eAgInSc1d1	mohutný
trup	trup	k1gInSc1	trup
a	a	k8xC	a
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
ocas	ocas	k1gInSc4	ocas
<g/>
,	,	kIx,	,
ukončený	ukončený	k2eAgInSc4d1	ukončený
střapcem	střapec	k1gInSc7	střapec
chlupů	chlup	k1gInPc2	chlup
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc4	dva
pohlaví	pohlaví	k1gNnPc4	pohlaví
mají	mít	k5eAaImIp3nP	mít
zahnuté	zahnutý	k2eAgInPc1d1	zahnutý
rohy	roh	k1gInPc1	roh
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
stýkají	stýkat	k5eAaImIp3nP	stýkat
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
tři	tři	k4xCgInPc4	tři
poddruhy	poddruh	k1gInPc4	poddruh
<g/>
:	:	kIx,	:
větší	veliký	k2eAgFnSc4d2	veliký
<g/>
,	,	kIx,	,
savanovou	savanový	k2eAgFnSc4d1	savanová
formu	forma	k1gFnSc4	forma
<g/>
,	,	kIx,	,
buvola	buvol	k1gMnSc2	buvol
kaferského	kaferský	k2eAgMnSc2d1	kaferský
(	(	kIx(	(
<g/>
S.	S.	kA	S.
caffer	caffer	k1gMnSc1	caffer
caffer	caffer	k1gMnSc1	caffer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
menšího	malý	k2eAgMnSc2d2	menší
pralesního	pralesní	k2eAgMnSc2d1	pralesní
buvola	buvol	k1gMnSc2	buvol
<g/>
,	,	kIx,	,
S.	S.	kA	S.
caffer	caffer	k1gMnSc1	caffer
nanus	nanus	k1gMnSc1	nanus
a	a	k8xC	a
středně	středně	k6eAd1	středně
velkého	velký	k2eAgMnSc4d1	velký
buvola	buvol	k1gMnSc4	buvol
krátkorohého	krátkorohý	k2eAgInSc2d1	krátkorohý
S.	S.	kA	S.
caffer	caffra	k1gFnPc2	caffra
brachyceros	brachycerosa	k1gFnPc2	brachycerosa
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
poddruhy	poddruh	k1gInPc1	poddruh
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
mohou	moct	k5eAaImIp3nP	moct
křížit	křížit	k5eAaImF	křížit
<g/>
.	.	kIx.	.
</s>
<s>
Buvol	buvol	k1gMnSc1	buvol
kaferský	kaferský	k2eAgMnSc1d1	kaferský
je	být	k5eAaImIp3nS	být
nápadný	nápadný	k2eAgMnSc1d1	nápadný
svojí	svůj	k3xOyFgFnSc7	svůj
velikostí	velikost	k1gFnSc7	velikost
<g/>
:	:	kIx,	:
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
1,4	[number]	k4	1,4
m	m	kA	m
-	-	kIx~	-
1,7	[number]	k4	1,7
m	m	kA	m
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
může	moct	k5eAaImIp3nS	moct
překračovat	překračovat	k5eAaImF	překračovat
čtyři	čtyři	k4xCgInPc4	čtyři
metry	metr	k1gInPc4	metr
<g/>
.	.	kIx.	.
</s>
<s>
Vzrostlý	vzrostlý	k2eAgMnSc1d1	vzrostlý
býk	býk	k1gMnSc1	býk
může	moct	k5eAaImIp3nS	moct
vážit	vážit	k5eAaImF	vážit
až	až	k9	až
900	[number]	k4	900
kilogramů	kilogram	k1gInPc2	kilogram
<g/>
,	,	kIx,	,
krávy	kráva	k1gFnPc1	kráva
jsou	být	k5eAaImIp3nP	být
asi	asi	k9	asi
poloviční	poloviční	k2eAgMnSc1d1	poloviční
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
tmavě	tmavě	k6eAd1	tmavě
hnědá	hnědý	k2eAgFnSc1d1	hnědá
až	až	k8xS	až
černá	černý	k2eAgFnSc1d1	černá
<g/>
.	.	kIx.	.
</s>
<s>
Rohy	roh	k1gInPc1	roh
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
m	m	kA	m
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zahnuté	zahnutý	k2eAgFnPc1d1	zahnutá
do	do	k7c2	do
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
se	se	k3xPyFc4	se
stýkají	stýkat	k5eAaImIp3nP	stýkat
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
typického	typický	k2eAgInSc2d1	typický
štítu	štít	k1gInSc2	štít
<g/>
.	.	kIx.	.
</s>
<s>
Buvol	buvol	k1gMnSc1	buvol
pralesní	pralesní	k2eAgMnSc1d1	pralesní
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
menší	malý	k2eAgFnSc1d2	menší
<g/>
,	,	kIx,	,
v	v	k7c6	v
kohoutku	kohoutek	k1gInSc6	kohoutek
měří	měřit	k5eAaImIp3nS	měřit
100	[number]	k4	100
až	až	k9	až
130	[number]	k4	130
centimetrů	centimetr	k1gInPc2	centimetr
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
tří	tři	k4xCgInPc2	tři
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
váha	váha	k1gFnSc1	váha
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
200	[number]	k4	200
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
opět	opět	k6eAd1	opět
menší	malý	k2eAgInSc4d2	menší
než	než	k8xS	než
býci	býk	k1gMnPc1	býk
<g/>
.	.	kIx.	.
</s>
<s>
Srst	srst	k1gFnSc1	srst
je	být	k5eAaImIp3nS	být
červenohnědá	červenohnědý	k2eAgFnSc1d1	červenohnědá
<g/>
,	,	kIx,	,
u	u	k7c2	u
telat	tele	k1gNnPc2	tele
až	až	k6eAd1	až
žlutavá	žlutavý	k2eAgFnSc1d1	žlutavá
<g/>
.	.	kIx.	.
</s>
<s>
Rohy	roh	k1gInPc4	roh
má	mít	k5eAaImIp3nS	mít
krátké	krátký	k2eAgNnSc1d1	krátké
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
slabé	slabý	k2eAgFnPc1d1	slabá
<g/>
,	,	kIx,	,
zahnuté	zahnutý	k2eAgFnPc1d1	zahnutá
dozadu	dozadu	k6eAd1	dozadu
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
čele	čelo	k1gNnSc6	čelo
se	se	k3xPyFc4	se
rohy	roh	k1gInPc7	roh
nestýkají	stýkat	k5eNaImIp3nP	stýkat
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
jeho	on	k3xPp3gNnSc2	on
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
přizpůsobena	přizpůsoben	k2eAgFnSc1d1	přizpůsobena
pohybu	pohyb	k1gInSc2	pohyb
v	v	k7c6	v
hustém	hustý	k2eAgInSc6d1	hustý
porostu	porost	k1gInSc6	porost
<g/>
.	.	kIx.	.
</s>
<s>
Buvol	buvol	k1gMnSc1	buvol
krátkorohý	krátkorohý	k2eAgMnSc1d1	krátkorohý
je	být	k5eAaImIp3nS	být
vzácný	vzácný	k2eAgMnSc1d1	vzácný
a	a	k8xC	a
málo	málo	k6eAd1	málo
známý	známý	k2eAgMnSc1d1	známý
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
poněkud	poněkud	k6eAd1	poněkud
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
buvol	buvol	k1gMnSc1	buvol
kaferský	kaferský	k2eAgMnSc1d1	kaferský
<g/>
,	,	kIx,	,
kterému	který	k3yQgInSc3	který
se	se	k3xPyFc4	se
podobá	podobat	k5eAaImIp3nS	podobat
zbarvením	zbarvení	k1gNnSc7	zbarvení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tvarem	tvar	k1gInSc7	tvar
rohů	roh	k1gInPc2	roh
připomíná	připomínat	k5eAaImIp3nS	připomínat
spíš	spíš	k9	spíš
buvola	buvol	k1gMnSc2	buvol
pralesního	pralesní	k2eAgMnSc2d1	pralesní
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Súdánu	Súdán	k1gInSc2	Súdán
<g/>
,	,	kIx,	,
Čadu	Čad	k1gInSc2	Čad
a	a	k8xC	a
Nigeru	Niger	k1gInSc2	Niger
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
hranici	hranice	k1gFnSc6	hranice
rozšíření	rozšíření	k1gNnPc2	rozšíření
buvola	buvol	k1gMnSc2	buvol
afrického	africký	k2eAgInSc2d1	africký
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
Západní	západní	k2eAgFnSc1d1	západní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
savany	savana	k1gFnPc1	savana
a	a	k8xC	a
deštné	deštný	k2eAgInPc1d1	deštný
pralesy	prales	k1gInPc1	prales
<g/>
.	.	kIx.	.
</s>
<s>
Buvol	buvol	k1gMnSc1	buvol
kaferský	kaferský	k2eAgMnSc1d1	kaferský
preferuje	preferovat	k5eAaImIp3nS	preferovat
otevřené	otevřený	k2eAgFnPc4d1	otevřená
savany	savana	k1gFnPc4	savana
s	s	k7c7	s
travním	travní	k2eAgInSc7d1	travní
porostem	porost	k1gInSc7	porost
<g/>
,	,	kIx,	,
buvol	buvol	k1gMnSc1	buvol
pralesní	pralesní	k2eAgMnSc1d1	pralesní
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
primárním	primární	k2eAgInSc6d1	primární
deštném	deštný	k2eAgInSc6d1	deštný
pralese	prales	k1gInSc6	prales
nebo	nebo	k8xC	nebo
v	v	k7c6	v
bambusových	bambusový	k2eAgInPc6d1	bambusový
porostech	porost	k1gInPc6	porost
<g/>
.	.	kIx.	.
</s>
<s>
Buvol	buvol	k1gMnSc1	buvol
krátkorohý	krátkorohý	k2eAgMnSc1d1	krátkorohý
obývá	obývat	k5eAaImIp3nS	obývat
savany	savana	k1gFnSc2	savana
a	a	k8xC	a
bažinaté	bažinatý	k2eAgFnSc2d1	bažinatá
oblasti	oblast	k1gFnSc2	oblast
západní	západní	k2eAgFnSc2d1	západní
a	a	k8xC	a
středozápadní	středozápadní	k2eAgFnSc2d1	středozápadní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
od	od	k7c2	od
okolí	okolí	k1gNnSc2	okolí
Čadského	čadský	k2eAgNnSc2d1	Čadské
jezera	jezero	k1gNnSc2	jezero
až	až	k9	až
po	po	k7c4	po
Senegal	Senegal	k1gInSc4	Senegal
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
vždy	vždy	k6eAd1	vždy
ve	v	k7c6	v
stádě	stádo	k1gNnSc6	stádo
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
do	do	k7c2	do
tisícihlavých	tisícihlavý	k2eAgNnPc2d1	tisícihlavé
stád	stádo	k1gNnPc2	stádo
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
menších	malý	k2eAgFnPc6d2	menší
rodinných	rodinný	k2eAgFnPc6d1	rodinná
skupinách	skupina	k1gFnPc6	skupina
tvořených	tvořený	k2eAgFnPc2d1	tvořená
20-30	[number]	k4	20-30
jedinci	jedinec	k1gMnPc7	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
kravami	kráva	k1gFnPc7	kráva
panuje	panovat	k5eAaImIp3nS	panovat
hierarchické	hierarchický	k2eAgNnSc1d1	hierarchické
uspořádání	uspořádání	k1gNnSc1	uspořádání
<g/>
,	,	kIx,	,
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
ale	ale	k8xC	ale
stojí	stát	k5eAaImIp3nP	stát
dospělí	dospělý	k2eAgMnPc1d1	dospělý
samci	samec	k1gMnPc1	samec
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
býci	býk	k1gMnPc1	býk
žijí	žít	k5eAaImIp3nP	žít
odděleně	odděleně	k6eAd1	odděleně
<g/>
.	.	kIx.	.
</s>
<s>
Pasou	pást	k5eAaImIp3nP	pást
se	se	k3xPyFc4	se
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
den	den	k1gInSc4	den
odpočívají	odpočívat	k5eAaImIp3nP	odpočívat
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
<g/>
.	.	kIx.	.
</s>
<s>
Savanoví	Savanový	k2eAgMnPc1d1	Savanový
buvoli	buvol	k1gMnPc1	buvol
dávají	dávat	k5eAaImIp3nP	dávat
přednost	přednost	k1gFnSc4	přednost
územím	území	k1gNnSc7	území
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mají	mít	k5eAaImIp3nP	mít
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
vodě	voda	k1gFnSc3	voda
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
chladit	chladit	k5eAaImF	chladit
válením	válení	k1gNnSc7	válení
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nebo	nebo	k8xC	nebo
v	v	k7c6	v
bahně	bahno	k1gNnSc6	bahno
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
domovského	domovský	k2eAgInSc2d1	domovský
okrsku	okrsek	k1gInSc2	okrsek
jednoho	jeden	k4xCgInSc2	jeden
stáda	stádo	k1gNnPc1	stádo
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
velikosti	velikost	k1gFnSc6	velikost
<g/>
,	,	kIx,	,
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
od	od	k7c2	od
60	[number]	k4	60
do	do	k7c2	do
1000	[number]	k4	1000
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Rozmnožování	rozmnožování	k1gNnSc1	rozmnožování
je	být	k5eAaImIp3nS	být
vázáno	vázat	k5eAaImNgNnS	vázat
na	na	k7c4	na
cykly	cyklus	k1gInPc1	cyklus
období	období	k1gNnSc2	období
sucha	sucho	k1gNnSc2	sucho
a	a	k8xC	a
dešťů	dešť	k1gInPc2	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Kráva	kráva	k1gFnSc1	kráva
je	být	k5eAaImIp3nS	být
březí	březí	k2eAgFnSc1d1	březí
340	[number]	k4	340
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
tele	tele	k1gNnSc1	tele
přichází	přicházet	k5eAaImIp3nS	přicházet
na	na	k7c4	na
svět	svět	k1gInSc4	svět
po	po	k7c6	po
období	období	k1gNnSc6	období
deštů	dešt	k1gInPc2	dešt
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
hojnost	hojnost	k1gFnSc1	hojnost
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pěti	pět	k4xCc6	pět
až	až	k9	až
15	[number]	k4	15
měsících	měsíc	k1gInPc6	měsíc
je	být	k5eAaImIp3nS	být
odstaveno	odstaven	k2eAgNnSc1d1	odstaveno
<g/>
,	,	kIx,	,
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
až	až	k9	až
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Buvoli	buvol	k1gMnPc1	buvol
afričtí	africký	k2eAgMnPc1d1	africký
jsou	být	k5eAaImIp3nP	být
dlouhověká	dlouhověký	k2eAgNnPc1d1	dlouhověké
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
dožívají	dožívat	k5eAaImIp3nP	dožívat
se	se	k3xPyFc4	se
až	až	k9	až
30	[number]	k4	30
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
silná	silný	k2eAgFnSc1d1	silná
a	a	k8xC	a
rychlá	rychlý	k2eAgNnPc1d1	rychlé
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
dokážou	dokázat	k5eAaPmIp3nP	dokázat
běžet	běžet	k5eAaImF	běžet
rychlostí	rychlost	k1gFnSc7	rychlost
až	až	k9	až
55	[number]	k4	55
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
a	a	k8xC	a
útočícímu	útočící	k2eAgMnSc3d1	útočící
predátorovi	predátor	k1gMnSc3	predátor
se	se	k3xPyFc4	se
urputně	urputně	k6eAd1	urputně
brání	bránit	k5eAaImIp3nS	bránit
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgMnSc7d1	jediný
přirozeným	přirozený	k2eAgMnSc7d1	přirozený
nepřítelem	nepřítel	k1gMnSc7	nepřítel
dospělého	dospělý	k1gMnSc2	dospělý
buvola	buvol	k1gMnSc2	buvol
je	být	k5eAaImIp3nS	být
lev	lev	k1gInSc1	lev
<g/>
,	,	kIx,	,
telata	tele	k1gNnPc1	tele
někdy	někdy	k6eAd1	někdy
padnou	padnout	k5eAaPmIp3nP	padnout
za	za	k7c4	za
oběť	oběť	k1gFnSc4	oběť
hyenám	hyena	k1gFnPc3	hyena
nebo	nebo	k8xC	nebo
krokodýlům	krokodýl	k1gMnPc3	krokodýl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
představují	představovat	k5eAaImIp3nP	představovat
pro	pro	k7c4	pro
buvoly	buvol	k1gMnPc4	buvol
velké	velký	k2eAgNnSc1d1	velké
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
infekční	infekční	k2eAgFnSc2d1	infekční
choroby	choroba	k1gFnSc2	choroba
<g/>
,	,	kIx,	,
přenášené	přenášený	k2eAgNnSc1d1	přenášené
domácím	domácí	k2eAgInSc7d1	domácí
skotem	skot	k1gInSc7	skot
domorodců	domorodec	k1gMnPc2	domorodec
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Masajů	Masaj	k1gInPc2	Masaj
<g/>
.	.	kIx.	.
</s>
<s>
Buvol	buvol	k1gMnSc1	buvol
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vnímavý	vnímavý	k2eAgMnSc1d1	vnímavý
např.	např.	kA	např.
k	k	k7c3	k
moru	mor	k1gInSc3	mor
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
slintavce	slintavka	k1gFnSc3	slintavka
a	a	k8xC	a
kulhavce	kulhavka	k1gFnSc3	kulhavka
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
odolný	odolný	k2eAgInSc1d1	odolný
proti	proti	k7c3	proti
naganě	nagana	k1gFnSc3	nagana
<g/>
.	.	kIx.	.
</s>
<s>
Místy	místy	k6eAd1	místy
buvolům	buvol	k1gMnPc3	buvol
komlikuje	komlikovat	k5eAaPmIp3nS	komlikovat
život	život	k1gInSc4	život
tzv.	tzv.	kA	tzv.
říční	říční	k2eAgFnSc1d1	říční
slepota	slepota	k1gFnSc1	slepota
<g/>
,	,	kIx,	,
parazitóza	parazitóza	k1gFnSc1	parazitóza
způsobená	způsobený	k2eAgFnSc1d1	způsobená
vlasovcem	vlasovec	k1gMnSc7	vlasovec
<g/>
,	,	kIx,	,
žijícím	žijící	k2eAgMnSc7d1	žijící
v	v	k7c6	v
podkoží	podkoží	k1gNnSc1	podkoží
či	či	k8xC	či
v	v	k7c6	v
oku	oko	k1gNnSc6	oko
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
choroba	choroba	k1gFnSc1	choroba
je	být	k5eAaImIp3nS	být
přenášena	přenášet	k5eAaImNgFnS	přenášet
muchničkami	muchnička	k1gFnPc7	muchnička
a	a	k8xC	a
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
i	i	k9	i
lidí	člověk	k1gMnPc2	člověk
či	či	k8xC	či
domáích	domáích	k2eAgNnPc2d2	domáích
zvířat	zvíře	k1gNnPc2	zvíře
ve	v	k7c6	v
vlhkých	vlhký	k2eAgFnPc6d1	vlhká
oblastech	oblast	k1gFnPc6	oblast
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jediného	jediný	k2eAgMnSc4d1	jediný
zástupce	zástupce	k1gMnSc4	zástupce
rodu	rod	k1gInSc2	rod
Syncerus	Syncerus	k1gInSc1	Syncerus
<g/>
.	.	kIx.	.
</s>
<s>
Asijský	asijský	k2eAgMnSc1d1	asijský
domestikovaný	domestikovaný	k2eAgMnSc1d1	domestikovaný
buvol	buvol	k1gMnSc1	buvol
domácí	domácí	k2eAgMnPc1d1	domácí
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
asijští	asijský	k2eAgMnPc1d1	asijský
buvoli	buvol	k1gMnPc1	buvol
proto	proto	k8xC	proto
nejsou	být	k5eNaImIp3nP	být
jeho	jeho	k3xOp3gMnPc7	jeho
blízkými	blízký	k2eAgMnPc7d1	blízký
příbuznými	příbuzný	k1gMnPc7	příbuzný
<g/>
.	.	kIx.	.
</s>
<s>
Prehistorickými	prehistorický	k2eAgMnPc7d1	prehistorický
předchůdci	předchůdce	k1gMnPc7	předchůdce
buvola	buvol	k1gMnSc2	buvol
afrického	africký	k2eAgInSc2d1	africký
byli	být	k5eAaImAgMnP	být
zástupci	zástupce	k1gMnPc7	zástupce
rodu	rod	k1gInSc2	rod
Pelorovis	Pelorovis	k1gFnSc1	Pelorovis
<g/>
,	,	kIx,	,
obrovská	obrovský	k2eAgNnPc1d1	obrovské
zvířata	zvíře	k1gNnPc1	zvíře
s	s	k7c7	s
podivně	podivně	k6eAd1	podivně
zahnutými	zahnutý	k2eAgInPc7d1	zahnutý
rohy	roh	k1gInPc7	roh
<g/>
.	.	kIx.	.
</s>
<s>
Žili	žít	k5eAaImAgMnP	žít
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
a	a	k8xC	a
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
v	v	k7c6	v
období	období	k1gNnSc6	období
pliocénu	pliocén	k1gInSc2	pliocén
a	a	k8xC	a
pleistocénu	pleistocén	k1gInSc2	pleistocén
<g/>
,	,	kIx,	,
před	před	k7c7	před
asi	asi	k9	asi
2,5	[number]	k4	2,5
milionu	milion	k4xCgInSc2	milion
lety	léto	k1gNnPc7	léto
až	až	k8xS	až
5000	[number]	k4	5000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
"	"	kIx"	"
<g/>
velké	velký	k2eAgFnSc2d1	velká
pětky	pětka	k1gFnSc2	pětka
<g/>
"	"	kIx"	"
nejnebezpečnějších	bezpečný	k2eNgNnPc2d3	nejnebezpečnější
zvířat	zvíře	k1gNnPc2	zvíře
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
hrochem	hroch	k1gMnSc7	hroch
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
zřejmě	zřejmě	k6eAd1	zřejmě
nejvíce	nejvíce	k6eAd1	nejvíce
smrtelných	smrtelný	k2eAgFnPc2d1	smrtelná
nehod	nehoda	k1gFnPc2	nehoda
lovců	lovec	k1gMnPc2	lovec
a	a	k8xC	a
účastníků	účastník	k1gMnPc2	účastník
safari	safari	k1gNnSc2	safari
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
buvol	buvol	k1gMnSc1	buvol
postřelený	postřelený	k2eAgMnSc1d1	postřelený
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
skutečně	skutečně	k6eAd1	skutečně
nebezpečným	bezpečný	k2eNgMnSc7d1	nebezpečný
soupeřem	soupeř	k1gMnSc7	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Postřelený	postřelený	k2eAgInSc1d1	postřelený
nebo	nebo	k8xC	nebo
i	i	k9	i
vyrušený	vyrušený	k2eAgMnSc1d1	vyrušený
odchází	odcházet	k5eAaImIp3nS	odcházet
do	do	k7c2	do
křoví	křoví	k1gNnSc2	křoví
nebo	nebo	k8xC	nebo
do	do	k7c2	do
rákosí	rákosí	k1gNnSc2	rákosí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
svého	svůj	k3xOyFgMnSc4	svůj
pronásledovatele	pronásledovatel	k1gMnSc4	pronásledovatel
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
ho	on	k3xPp3gMnSc4	on
dokonce	dokonce	k9	dokonce
obchází	obcházet	k5eAaImIp3nS	obcházet
a	a	k8xC	a
napadne	napadnout	k5eAaPmIp3nS	napadnout
zezadu	zezadu	k6eAd1	zezadu
<g/>
.	.	kIx.	.
</s>
<s>
Nepřítele	nepřítel	k1gMnSc4	nepřítel
-	-	kIx~	-
člověka	člověk	k1gMnSc4	člověk
nebo	nebo	k8xC	nebo
například	například	k6eAd1	například
i	i	k8xC	i
lvici	lvice	k1gFnSc4	lvice
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
nabodnout	nabodnout	k5eAaPmF	nabodnout
na	na	k7c4	na
rohy	roh	k1gInPc4	roh
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
podaří	podařit	k5eAaPmIp3nS	podařit
<g/>
,	,	kIx,	,
několikrát	několikrát	k6eAd1	několikrát
svou	svůj	k3xOyFgFnSc4	svůj
oběť	oběť	k1gFnSc4	oběť
vyhodí	vyhodit	k5eAaPmIp3nS	vyhodit
do	do	k7c2	do
vzuchu	vzuch	k1gInSc2	vzuch
a	a	k8xC	a
pak	pak	k6eAd1	pak
ji	on	k3xPp3gFnSc4	on
rozdupe	rozdupat	k5eAaPmIp3nS	rozdupat
kopyty	kopyto	k1gNnPc7	kopyto
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
bývá	bývat	k5eAaImIp3nS	bývat
útok	útok	k1gInSc1	útok
buvola	buvol	k1gMnSc2	buvol
zpravidla	zpravidla	k6eAd1	zpravidla
smrtelný	smrtelný	k2eAgInSc1d1	smrtelný
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
znepokojován	znepokojován	k2eAgMnSc1d1	znepokojován
<g/>
,	,	kIx,	,
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
sám	sám	k3xTgMnSc1	sám
od	od	k7c2	od
sebe	se	k3xPyFc2	se
neútočí	útočit	k5eNaImIp3nP	útočit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
slona	slon	k1gMnSc2	slon
a	a	k8xC	a
nosorožce	nosorožec	k1gMnSc2	nosorožec
také	také	k9	také
neútočí	útočit	k5eNaImIp3nS	útočit
na	na	k7c4	na
automobily	automobil	k1gInPc4	automobil
<g/>
.	.	kIx.	.
</s>
<s>
Buvol	buvol	k1gMnSc1	buvol
africký	africký	k2eAgMnSc1d1	africký
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
zoo	zoo	k1gNnSc6	zoo
příliš	příliš	k6eAd1	příliš
oblíbeným	oblíbený	k2eAgMnSc7d1	oblíbený
chovancem	chovanec	k1gMnSc7	chovanec
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
výběh	výběh	k1gInSc1	výběh
brzy	brzy	k6eAd1	brzy
změní	změnit	k5eAaPmIp3nS	změnit
v	v	k7c4	v
bažinu	bažina	k1gFnSc4	bažina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
agresívní	agresívní	k2eAgMnSc1d1	agresívní
a	a	k8xC	a
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
málo	málo	k6eAd1	málo
atraktivní	atraktivní	k2eAgFnSc1d1	atraktivní
<g/>
:	:	kIx,	:
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
návštěvníků	návštěvník	k1gMnPc2	návštěvník
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
"	"	kIx"	"
<g/>
divná	divný	k2eAgFnSc1d1	divná
černá	černý	k2eAgFnSc1d1	černá
kráva	kráva	k1gFnSc1	kráva
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
chován	chovat	k5eAaImNgInS	chovat
pouze	pouze	k6eAd1	pouze
v	v	k7c4	v
ZOO	zoo	k1gFnSc4	zoo
Dvůr	Dvůr	k1gInSc4	Dvůr
Králové	Králová	k1gFnSc2	Králová
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
chová	chovat	k5eAaImIp3nS	chovat
oba	dva	k4xCgInPc4	dva
poddruhy	poddruh	k1gInPc4	poddruh
<g/>
,	,	kIx,	,
buvola	buvol	k1gMnSc2	buvol
kaferského	kaferský	k2eAgMnSc2d1	kaferský
(	(	kIx(	(
<g/>
Syncerus	Syncerus	k1gInSc1	Syncerus
caffer	caffer	k1gMnSc1	caffer
caffer	caffer	k1gMnSc1	caffer
<g/>
)	)	kIx)	)
a	a	k8xC	a
buvola	buvol	k1gMnSc2	buvol
pralesního	pralesní	k2eAgMnSc2d1	pralesní
(	(	kIx(	(
<g/>
Syncerus	Syncerus	k1gInSc1	Syncerus
caffer	caffer	k1gMnSc1	caffer
nanus	nanus	k1gMnSc1	nanus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Buvol	buvol	k1gMnSc1	buvol
africký	africký	k2eAgMnSc1d1	africký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
