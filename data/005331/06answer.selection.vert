<s>
Buvol	buvol	k1gMnSc1	buvol
africký	africký	k2eAgMnSc1d1	africký
(	(	kIx(	(
<g/>
Syncerus	Syncerus	k1gMnSc1	Syncerus
caffer	caffer	k1gMnSc1	caffer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nepřesně	přesně	k6eNd1	přesně
označovaný	označovaný	k2eAgInSc1d1	označovaný
také	také	k9	také
jako	jako	k9	jako
buvol	buvol	k1gMnSc1	buvol
kaferský	kaferský	k2eAgMnSc1d1	kaferský
je	být	k5eAaImIp3nS	být
mohutný	mohutný	k2eAgMnSc1d1	mohutný
býložravec	býložravec	k1gMnSc1	býložravec
afrických	africký	k2eAgFnPc2d1	africká
savan	savana	k1gFnPc2	savana
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc7	jeho
domovinou	domovina	k1gFnSc7	domovina
jsou	být	k5eAaImIp3nP	být
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
oblasti	oblast	k1gFnPc1	oblast
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
.	.	kIx.	.
</s>
