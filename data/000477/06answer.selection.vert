<s>
Suezský	suezský	k2eAgInSc1d1	suezský
průplav	průplav	k1gInSc1	průplav
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
ق	ق	k?	ق
ا	ا	k?	ا
<g/>
,	,	kIx,	,
hebrejsky	hebrejsky	k6eAd1	hebrejsky
ת	ת	k?	ת
ס	ס	k?	ס
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
též	též	k9	též
Suez	Suez	k1gInSc1	Suez
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
193	[number]	k4	193
km	km	kA	km
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
průplav	průplav	k1gInSc4	průplav
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
spojující	spojující	k2eAgNnSc4d1	spojující
Středozemní	středozemní	k2eAgNnSc4d1	středozemní
a	a	k8xC	a
Rudé	rudý	k2eAgNnSc4d1	Rudé
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
