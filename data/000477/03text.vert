<s>
Suezský	suezský	k2eAgInSc1d1	suezský
průplav	průplav	k1gInSc1	průplav
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
ق	ق	k?	ق
ا	ا	k?	ا
<g/>
,	,	kIx,	,
hebrejsky	hebrejsky	k6eAd1	hebrejsky
ת	ת	k?	ת
ס	ס	k?	ס
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
též	též	k9	též
Suez	Suez	k1gInSc1	Suez
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
193	[number]	k4	193
km	km	kA	km
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
průplav	průplav	k1gInSc4	průplav
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
spojující	spojující	k2eAgNnSc4d1	spojující
Středozemní	středozemní	k2eAgNnSc4d1	středozemní
a	a	k8xC	a
Rudé	rudý	k2eAgNnSc4d1	Rudé
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Kanál	kanál	k1gInSc1	kanál
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
(	(	kIx(	(
<g/>
severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
<g/>
)	)	kIx)	)
Velkým	velký	k2eAgNnSc7d1	velké
Hořkým	hořký	k2eAgNnSc7d1	hořké
jezerem	jezero	k1gNnSc7	jezero
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
Sinajem	Sinaj	k1gInSc7	Sinaj
(	(	kIx(	(
<g/>
Asie	Asie	k1gFnSc2	Asie
<g/>
)	)	kIx)	)
a	a	k8xC	a
Afrikou	Afrika	k1gFnSc7	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
lodím	lodit	k5eAaImIp1nS	lodit
přímou	přímý	k2eAgFnSc4d1	přímá
cestu	cesta	k1gFnSc4	cesta
mezi	mezi	k7c7	mezi
Středozemním	středozemní	k2eAgNnSc7d1	středozemní
a	a	k8xC	a
Rudým	rudý	k2eAgNnSc7d1	Rudé
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
dříve	dříve	k6eAd2	dříve
musely	muset	k5eAaImAgFnP	muset
buďto	buďto	k8xC	buďto
obeplouvat	obeplouvat	k5eAaImF	obeplouvat
Afriku	Afrika	k1gFnSc4	Afrika
kolem	kolem	k7c2	kolem
mysu	mys	k1gInSc2	mys
Dobré	dobrý	k2eAgFnSc2d1	dobrá
naděje	naděje	k1gFnSc2	naděje
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
náklad	náklad	k1gInSc1	náklad
přes	přes	k7c4	přes
Suezskou	suezský	k2eAgFnSc4d1	Suezská
šíji	šíje	k1gFnSc4	šíje
přepravovat	přepravovat	k5eAaImF	přepravovat
po	po	k7c6	po
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
plavbě	plavba	k1gFnSc3	plavba
kolem	kolem	k7c2	kolem
Afriky	Afrika	k1gFnSc2	Afrika
se	se	k3xPyFc4	se
cesta	cesta	k1gFnSc1	cesta
Suezským	suezský	k2eAgInSc7d1	suezský
průplavem	průplav	k1gInSc7	průplav
například	například	k6eAd1	například
z	z	k7c2	z
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
do	do	k7c2	do
Rotterdamu	Rotterdam	k1gInSc2	Rotterdam
zkrátila	zkrátit	k5eAaPmAgFnS	zkrátit
o	o	k7c4	o
42	[number]	k4	42
%	%	kIx~	%
<g/>
,	,	kIx,	,
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
o	o	k7c4	o
30	[number]	k4	30
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozšíření	rozšíření	k1gNnSc6	rozšíření
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
mohou	moct	k5eAaImIp3nP	moct
průplavem	průplav	k1gInSc7	průplav
plout	plout	k5eAaImF	plout
lodi	loď	k1gFnSc3	loď
s	s	k7c7	s
maximálním	maximální	k2eAgInSc7d1	maximální
užitečným	užitečný	k2eAgInSc7d1	užitečný
nákladem	náklad	k1gInSc7	náklad
až	až	k9	až
240	[number]	k4	240
tisíc	tisíc	k4xCgInSc4	tisíc
tun	tuna	k1gFnPc2	tuna
(	(	kIx(	(
<g/>
DWT	DWT	kA	DWT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc4	všechen
současné	současný	k2eAgFnPc4d1	současná
lodi	loď	k1gFnPc4	loď
kromě	kromě	k7c2	kromě
asi	asi	k9	asi
třetiny	třetina	k1gFnPc4	třetina
těch	ten	k3xDgInPc2	ten
největších	veliký	k2eAgInPc2d3	veliký
supertankerů	supertanker	k1gInPc2	supertanker
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
dále	daleko	k6eAd2	daleko
objíždějí	objíždět	k5eAaImIp3nP	objíždět
Afriku	Afrika	k1gFnSc4	Afrika
starou	starý	k2eAgFnSc7d1	stará
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
musely	muset	k5eAaImAgFnP	muset
lodi	loď	k1gFnPc4	loď
plout	plout	k5eAaImF	plout
v	v	k7c6	v
konvojích	konvoj	k1gInPc6	konvoj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
míjely	míjet	k5eAaImAgInP	míjet
na	na	k7c6	na
rozšířených	rozšířený	k2eAgFnPc6d1	rozšířená
výhybnách	výhybna	k1gFnPc6	výhybna
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
vlaky	vlak	k1gInPc1	vlak
na	na	k7c6	na
jednokolejné	jednokolejný	k2eAgFnSc6d1	jednokolejná
trati	trať	k1gFnSc6	trať
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
od	od	k7c2	od
rozšíření	rozšíření	k1gNnSc2	rozšíření
a	a	k8xC	a
dostavby	dostavba	k1gFnPc1	dostavba
35	[number]	k4	35
km	km	kA	km
nového	nový	k2eAgInSc2d1	nový
úseku	úsek	k1gInSc2	úsek
průplavu	průplav	k1gInSc2	průplav
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Nový	nový	k2eAgInSc1d1	nový
Suezský	suezský	k2eAgInSc1d1	suezský
průplav	průplav	k1gInSc1	průplav
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
plout	plout	k5eAaImF	plout
nezávisle	závisle	k6eNd1	závisle
oběma	dva	k4xCgInPc7	dva
směry	směr	k1gInPc7	směr
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
kapacita	kapacita	k1gFnSc1	kapacita
průplavu	průplav	k1gInSc2	průplav
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
ze	z	k7c2	z
49	[number]	k4	49
na	na	k7c4	na
97	[number]	k4	97
lodí	loď	k1gFnPc2	loď
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
snížily	snížit	k5eAaPmAgFnP	snížit
čekací	čekací	k2eAgFnPc1d1	čekací
doby	doba	k1gFnPc1	doba
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
z	z	k7c2	z
11	[number]	k4	11
na	na	k7c4	na
3	[number]	k4	3
hodiny	hodina	k1gFnSc2	hodina
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zkrátila	zkrátit	k5eAaPmAgFnS	zkrátit
doba	doba	k1gFnSc1	doba
plavby	plavba	k1gFnSc2	plavba
průplavem	průplav	k1gInSc7	průplav
na	na	k7c6	na
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Suezský	suezský	k2eAgInSc1d1	suezský
průplav	průplav	k1gInSc1	průplav
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
také	také	k9	také
ekosystém	ekosystém	k1gInSc1	ekosystém
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yIgInSc2	který
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
lessepsovské	lessepsovský	k2eAgFnSc2d1	lessepsovský
migrace	migrace	k1gFnSc2	migrace
pronikají	pronikat	k5eAaImIp3nP	pronikat
organismy	organismus	k1gInPc4	organismus
z	z	k7c2	z
Rudého	rudý	k2eAgNnSc2d1	Rudé
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Suezský	suezský	k2eAgInSc1d1	suezský
kanál	kanál	k1gInSc1	kanál
není	být	k5eNaImIp3nS	být
prvním	první	k4xOgInSc7	první
průplavem	průplav	k1gInSc7	průplav
umožňujícím	umožňující	k2eAgInSc7d1	umožňující
plavbu	plavba	k1gFnSc4	plavba
mezi	mezi	k7c7	mezi
Rudým	rudý	k2eAgNnSc7d1	Rudé
a	a	k8xC	a
Středozemním	středozemní	k2eAgNnSc7d1	středozemní
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1	předchůdce
Suezského	suezský	k2eAgInSc2d1	suezský
kanálu	kanál	k1gInSc2	kanál
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
již	již	k6eAd1	již
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
když	když	k8xS	když
Egypťané	Egypťan	k1gMnPc1	Egypťan
propojili	propojit	k5eAaPmAgMnP	propojit
Rudé	rudý	k2eAgNnSc4d1	Rudé
moře	moře	k1gNnSc4	moře
a	a	k8xC	a
deltu	delta	k1gFnSc4	delta
řeky	řeka	k1gFnSc2	řeka
Nil	Nil	k1gInSc1	Nil
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
příliš	příliš	k6eAd1	příliš
životaschopné	životaschopný	k2eAgNnSc1d1	životaschopné
řešení	řešení	k1gNnSc1	řešení
-	-	kIx~	-
delta	delta	k1gFnSc1	delta
Nilu	Nil	k1gInSc2	Nil
je	být	k5eAaImIp3nS	být
neustále	neustále	k6eAd1	neustále
zanášena	zanášen	k2eAgFnSc1d1	zanášena
sedimenty	sediment	k1gInPc1	sediment
<g/>
:	:	kIx,	:
trasa	trasa	k1gFnSc1	trasa
sjízdných	sjízdný	k2eAgFnPc2d1	sjízdná
cest	cesta	k1gFnPc2	cesta
i	i	k8xC	i
hloubka	hloubka	k1gFnSc1	hloubka
severní	severní	k2eAgFnSc2d1	severní
části	část	k1gFnSc2	část
cesty	cesta	k1gFnSc2	cesta
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
měnily	měnit	k5eAaImAgFnP	měnit
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
dost	dost	k6eAd1	dost
obtížné	obtížný	k2eAgNnSc1d1	obtížné
tudy	tudy	k6eAd1	tudy
proplout	proplout	k5eAaPmF	proplout
<g/>
.	.	kIx.	.
</s>
<s>
Průplav	průplav	k1gInSc1	průplav
časem	časem	k6eAd1	časem
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejpozději	pozdě	k6eAd3	pozdě
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
pokusil	pokusit	k5eAaPmAgMnS	pokusit
neúspěšně	úspěšně	k6eNd1	úspěšně
obnovit	obnovit	k5eAaPmF	obnovit
faraón	faraón	k1gMnSc1	faraón
Nechos	Nechos	k1gMnSc1	Nechos
II	II	kA	II
<g/>
.	.	kIx.	.
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
600	[number]	k4	600
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Až	až	k6eAd1	až
perský	perský	k2eAgMnSc1d1	perský
král	král	k1gMnSc1	král
Dáreios	Dáreios	k1gMnSc1	Dáreios
I.	I.	kA	I.
byl	být	k5eAaImAgMnS	být
o	o	k7c4	o
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
úspěšnější	úspěšný	k2eAgNnPc1d2	úspěšnější
<g/>
.	.	kIx.	.
</s>
<s>
Kanál	kanál	k1gInSc1	kanál
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
znovu	znovu	k6eAd1	znovu
obnoven	obnovit	k5eAaPmNgInS	obnovit
například	například	k6eAd1	například
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
za	za	k7c4	za
egyptského	egyptský	k2eAgMnSc4d1	egyptský
krále	král	k1gMnSc4	král
Ptolemaia	Ptolemaios	k1gMnSc2	Ptolemaios
II	II	kA	II
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
počátkem	počátkem	k7c2	počátkem
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
římským	římský	k2eAgMnSc7d1	římský
císařem	císař	k1gMnSc7	císař
Traianem	Traian	k1gMnSc7	Traian
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgInSc1d1	starý
kanál	kanál	k1gInSc1	kanál
byl	být	k5eAaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
645	[number]	k4	645
<g/>
,	,	kIx,	,
když	když	k8xS	když
Arabové	Arab	k1gMnPc1	Arab
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Egypt	Egypt	k1gInSc4	Egypt
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
roku	rok	k1gInSc2	rok
770	[number]	k4	770
jej	on	k3xPp3gInSc4	on
dal	dát	k5eAaPmAgInS	dát
chálif	chálif	k1gInSc1	chálif
Al	ala	k1gFnPc2	ala
Mansur	Mansura	k1gFnPc2	Mansura
ze	z	k7c2	z
strategických	strategický	k2eAgInPc2d1	strategický
důvodů	důvod	k1gInPc2	důvod
uzavřít	uzavřít	k5eAaPmF	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1504	[number]	k4	1504
Benátčané	Benátčan	k1gMnPc1	Benátčan
marně	marně	k6eAd1	marně
žádali	žádat	k5eAaImAgMnP	žádat
o	o	k7c4	o
povolení	povolení	k1gNnSc4	povolení
ke	k	k7c3	k
stavbě	stavba	k1gFnSc3	stavba
průplavu	průplav	k1gInSc2	průplav
<g/>
.	.	kIx.	.
</s>
<s>
Filosof	filosof	k1gMnSc1	filosof
Gottfried	Gottfried	k1gMnSc1	Gottfried
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Leibniz	Leibniz	k1gMnSc1	Leibniz
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
roku	rok	k1gInSc2	rok
1671	[number]	k4	1671
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
francouzského	francouzský	k2eAgMnSc4d1	francouzský
krále	král	k1gMnSc4	král
Ludvíka	Ludvík	k1gMnSc4	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
obavy	obava	k1gFnPc1	obava
z	z	k7c2	z
rozdílu	rozdíl	k1gInSc2	rozdíl
hladin	hladina	k1gFnPc2	hladina
a	a	k8xC	a
zanášení	zanášení	k1gNnSc2	zanášení
kanálu	kanál	k1gInSc2	kanál
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
překonat	překonat	k5eAaPmF	překonat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
prokopání	prokopání	k1gNnSc6	prokopání
průplavu	průplav	k1gInSc2	průplav
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgMnS	zajímat
Napoleon	Napoleon	k1gMnSc1	Napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
roku	rok	k1gInSc2	rok
1799	[number]	k4	1799
při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
egyptském	egyptský	k2eAgNnSc6d1	egyptské
tažení	tažení	k1gNnSc6	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Dal	dát	k5eAaPmAgMnS	dát
vypracovat	vypracovat	k5eAaPmF	vypracovat
studii	studie	k1gFnSc4	studie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mylně	mylně	k6eAd1	mylně
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozdíl	rozdíl	k1gInSc1	rozdíl
hladin	hladina	k1gFnPc2	hladina
obou	dva	k4xCgNnPc2	dva
moří	moře	k1gNnPc2	moře
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
9	[number]	k4	9
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
byl	být	k5eAaImAgInS	být
projekt	projekt	k1gInSc1	projekt
opět	opět	k6eAd1	opět
opuštěn	opustit	k5eAaPmNgInS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
projekt	projekt	k1gInSc4	projekt
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
zajímat	zajímat	k5eAaImF	zajímat
francouzský	francouzský	k2eAgMnSc1d1	francouzský
inženýr	inženýr	k1gMnSc1	inženýr
a	a	k8xC	a
diplomat	diplomat	k1gMnSc1	diplomat
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Lesseps	Lesseps	k1gInSc4	Lesseps
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
konzulem	konzul	k1gMnSc7	konzul
v	v	k7c6	v
Alexandrii	Alexandrie	k1gFnSc6	Alexandrie
<g/>
.	.	kIx.	.
</s>
<s>
Dal	dát	k5eAaPmAgMnS	dát
si	se	k3xPyFc3	se
vypracovat	vypracovat	k5eAaPmF	vypracovat
podrobný	podrobný	k2eAgInSc4d1	podrobný
projekt	projekt	k1gInSc4	projekt
od	od	k7c2	od
rakouského	rakouský	k2eAgMnSc2d1	rakouský
projektanta	projektant	k1gMnSc2	projektant
Aloise	Alois	k1gMnSc4	Alois
Negrelliho	Negrelli	k1gMnSc4	Negrelli
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
projektoval	projektovat	k5eAaBmAgInS	projektovat
také	také	k9	také
karlínský	karlínský	k2eAgInSc1d1	karlínský
viadukt	viadukt	k1gInSc1	viadukt
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
se	se	k3xPyFc4	se
Lesseps	Lesseps	k1gInSc1	Lesseps
stal	stát	k5eAaPmAgInS	stát
konzulem	konzul	k1gMnSc7	konzul
v	v	k7c6	v
Káhiře	Káhira	k1gFnSc6	Káhira
a	a	k8xC	a
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
získal	získat	k5eAaPmAgMnS	získat
koncesi	koncese	k1gFnSc4	koncese
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
průplavu	průplav	k1gInSc2	průplav
od	od	k7c2	od
egyptského	egyptský	k2eAgMnSc2d1	egyptský
místokrále	místokrál	k1gMnSc2	místokrál
Saída	Saíd	k1gMnSc2	Saíd
Paši	paša	k1gMnSc2	paša
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
později	pozdě	k6eAd2	pozdě
zakoupil	zakoupit	k5eAaPmAgInS	zakoupit
44	[number]	k4	44
<g/>
%	%	kIx~	%
podíl	podíl	k1gInSc4	podíl
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
Compagnie	Compagnie	k1gFnSc2	Compagnie
universelle	universelle	k1gFnSc2	universelle
du	du	k?	du
canal	canal	k1gInSc1	canal
maritime	maritimat	k5eAaPmIp3nS	maritimat
de	de	k?	de
Suez	Suez	k1gInSc1	Suez
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kanál	kanál	k1gInSc4	kanál
stavěla	stavět	k5eAaImAgFnS	stavět
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
investorů	investor	k1gMnPc2	investor
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
Ruska	Rusko	k1gNnSc2	Rusko
i	i	k8xC	i
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
britská	britský	k2eAgFnSc1d1	britská
vláda	vláda	k1gFnSc1	vláda
stavbu	stavba	k1gFnSc4	stavba
zprvu	zprvu	k6eAd1	zprvu
odmítala	odmítat	k5eAaImAgFnS	odmítat
a	a	k8xC	a
snažila	snažit	k5eAaImAgFnS	snažit
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc2	on
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1859	[number]	k4	1859
byla	být	k5eAaImAgFnS	být
stavba	stavba	k1gFnSc1	stavba
slavnostně	slavnostně	k6eAd1	slavnostně
zahájena	zahájit	k5eAaPmNgFnS	zahájit
na	na	k7c6	na
středomořském	středomořský	k2eAgNnSc6d1	středomořské
pobřeží	pobřeží	k1gNnSc6	pobřeží
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
později	pozdě	k6eAd2	pozdě
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
Port	port	k1gInSc1	port
Said	Saido	k1gNnPc2	Saido
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pustém	pustý	k2eAgNnSc6d1	pusté
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
nejprve	nejprve	k6eAd1	nejprve
postavit	postavit	k5eAaPmF	postavit
malý	malý	k2eAgInSc1d1	malý
přístav	přístav	k1gInSc1	přístav
<g/>
,	,	kIx,	,
sklady	sklad	k1gInPc1	sklad
a	a	k8xC	a
ubytovny	ubytovna	k1gFnPc1	ubytovna
pro	pro	k7c4	pro
dělníky	dělník	k1gMnPc4	dělník
<g/>
,	,	kIx,	,
kterých	který	k3yQgMnPc2	který
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
pracovalo	pracovat	k5eAaImAgNnS	pracovat
až	až	k9	až
34	[number]	k4	34
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Všechen	všechen	k3xTgInSc1	všechen
materiál	materiál	k1gInSc1	materiál
se	se	k3xPyFc4	se
přivážel	přivážet	k5eAaImAgInS	přivážet
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
potraviny	potravina	k1gFnPc4	potravina
přiváželo	přivážet	k5eAaImAgNnS	přivážet
až	až	k9	až
1	[number]	k4	1
800	[number]	k4	800
velbloudů	velbloud	k1gMnPc2	velbloud
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
hloubíli	hloubínout	k5eAaImAgMnP	hloubínout
průplav	průplav	k1gInSc4	průplav
dělníci	dělník	k1gMnPc1	dělník
ručně	ručně	k6eAd1	ručně
<g/>
,	,	kIx,	,
vytěžený	vytěžený	k2eAgInSc1d1	vytěžený
materiál	materiál	k1gInSc1	materiál
se	se	k3xPyFc4	se
přenášel	přenášet	k5eAaImAgInS	přenášet
v	v	k7c6	v
koších	koš	k1gInPc6	koš
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
postupně	postupně	k6eAd1	postupně
vznikaly	vznikat	k5eAaImAgFnP	vznikat
parní	parní	k2eAgFnPc1d1	parní
bagrovací	bagrovací	k2eAgFnPc1d1	bagrovací
lodě	loď	k1gFnPc1	loď
a	a	k8xC	a
mechanické	mechanický	k2eAgInPc1d1	mechanický
transportéry	transportér	k1gInPc1	transportér
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
nejprve	nejprve	k6eAd1	nejprve
zkonstruovat	zkonstruovat	k5eAaPmF	zkonstruovat
a	a	k8xC	a
vyrobit	vyrobit	k5eAaPmF	vyrobit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
se	se	k3xPyFc4	se
vystřídalo	vystřídat	k5eAaPmAgNnS	vystřídat
asi	asi	k9	asi
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
,	,	kIx,	,
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
lidských	lidský	k2eAgFnPc6d1	lidská
obětech	oběť	k1gFnPc6	oběť
(	(	kIx(	(
<g/>
údajně	údajně	k6eAd1	údajně
až	až	k9	až
120	[number]	k4	120
tisíc	tisíc	k4xCgInSc4	tisíc
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
silně	silně	k6eAd1	silně
přehnané	přehnaný	k2eAgFnPc1d1	přehnaná
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
probíhala	probíhat	k5eAaImAgFnS	probíhat
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
původní	původní	k2eAgInSc1d1	původní
rozpočet	rozpočet	k1gInSc1	rozpočet
byl	být	k5eAaImAgInS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvojnásobně	dvojnásobně	k6eAd1	dvojnásobně
překročen	překročit	k5eAaPmNgInS	překročit
<g/>
.	.	kIx.	.
</s>
<s>
Průplav	průplav	k1gInSc1	průplav
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřen	otevřít	k5eAaPmNgInS	otevřít
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1869	[number]	k4	1869
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
obrovský	obrovský	k2eAgInSc4d1	obrovský
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
efekt	efekt	k1gInSc4	efekt
na	na	k7c4	na
zámořský	zámořský	k2eAgInSc4d1	zámořský
obchod	obchod	k1gInSc4	obchod
a	a	k8xC	a
pronikání	pronikání	k1gNnSc4	pronikání
Evropanů	Evropan	k1gMnPc2	Evropan
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
prospěch	prospěch	k1gInSc4	prospěch
přinesl	přinést	k5eAaPmAgInS	přinést
průplav	průplav	k1gInSc1	průplav
Itálii	Itálie	k1gFnSc4	Itálie
a	a	k8xC	a
zejména	zejména	k9	zejména
Rakousko-Uhersku	Rakousko-Uherska	k1gFnSc4	Rakousko-Uherska
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
také	také	k9	také
výrazně	výrazně	k6eAd1	výrazně
podílelo	podílet	k5eAaImAgNnS	podílet
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc1	provoz
ovšem	ovšem	k9	ovšem
nebyl	být	k5eNaImAgInS	být
rentabilní	rentabilní	k2eAgInSc1d1	rentabilní
a	a	k8xC	a
Egypt	Egypt	k1gInSc1	Egypt
se	se	k3xPyFc4	se
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
bankrotu	bankrot	k1gInSc2	bankrot
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1875	[number]	k4	1875
převzala	převzít	k5eAaPmAgFnS	převzít
britská	britský	k2eAgFnSc1d1	britská
vláda	vláda	k1gFnSc1	vláda
podíl	podíl	k1gInSc4	podíl
od	od	k7c2	od
Ismáila	Ismáil	k1gMnSc2	Ismáil
Paši	paša	k1gMnSc2	paša
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
Saída	Saíd	k1gMnSc2	Saíd
Paši	paša	k1gMnSc2	paša
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
největším	veliký	k2eAgMnSc7d3	veliký
akcionářem	akcionář	k1gMnSc7	akcionář
společnosti	společnost	k1gFnSc2	společnost
vlastnící	vlastnící	k2eAgInSc4d1	vlastnící
Suezský	suezský	k2eAgInSc4d1	suezský
průplav	průplav	k1gInSc4	průplav
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
proti	proti	k7c3	proti
britskému	britský	k2eAgNnSc3d1	Britské
vedení	vedení	k1gNnSc3	vedení
povstání	povstání	k1gNnSc2	povstání
"	"	kIx"	"
<g/>
Mladých	mladý	k2eAgMnPc2d1	mladý
Egypťanů	Egypťan	k1gMnPc2	Egypťan
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
Británie	Británie	k1gFnSc1	Británie
potlačila	potlačit	k5eAaPmAgFnS	potlačit
a	a	k8xC	a
Egypt	Egypt	k1gInSc1	Egypt
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
vojensky	vojensky	k6eAd1	vojensky
obsadila	obsadit	k5eAaPmAgFnS	obsadit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
Konstantinopolská	konstantinopolský	k2eAgFnSc1d1	konstantinopolská
smlouva	smlouva	k1gFnSc1	smlouva
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
Suezský	suezský	k2eAgInSc4d1	suezský
kanál	kanál	k1gInSc4	kanál
za	za	k7c4	za
neutrální	neutrální	k2eAgNnSc4d1	neutrální
území	území	k1gNnSc4	území
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
zaručila	zaručit	k5eAaPmAgFnS	zaručit
volný	volný	k2eAgInSc4d1	volný
průjezd	průjezd	k1gInSc4	průjezd
všem	všecek	k3xTgFnPc3	všecek
zemím	zem	k1gFnPc3	zem
v	v	k7c6	v
době	doba	k1gFnSc6	doba
míru	mír	k1gInSc2	mír
i	i	k8xC	i
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
britská	britský	k2eAgFnSc1d1	britská
armáda	armáda	k1gFnSc1	armáda
Egypt	Egypt	k1gInSc1	Egypt
opustila	opustit	k5eAaPmAgFnS	opustit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
anglo-egyptská	anglogyptský	k2eAgFnSc1d1	anglo-egyptský
smlouva	smlouva	k1gFnSc1	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
dohled	dohled	k1gInSc4	dohled
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
nad	nad	k7c7	nad
průplavem	průplav	k1gInSc7	průplav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
Druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
Italové	Ital	k1gMnPc1	Ital
marně	marně	k6eAd1	marně
snažili	snažit	k5eAaImAgMnP	snažit
průplav	průplav	k1gInSc4	průplav
dobýt	dobýt	k5eAaPmF	dobýt
a	a	k8xC	a
obsadit	obsadit	k5eAaPmF	obsadit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
Egypt	Egypt	k1gInSc1	Egypt
smlouvu	smlouva	k1gFnSc4	smlouva
vypověděl	vypovědět	k5eAaPmAgInS	vypovědět
a	a	k8xC	a
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
posléze	posléze	k6eAd1	posléze
souhlasilo	souhlasit	k5eAaImAgNnS	souhlasit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
stáhne	stáhnout	k5eAaPmIp3nS	stáhnout
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Gamal	Gamal	k1gMnSc1	Gamal
Násir	Násir	k1gMnSc1	Násir
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
kanál	kanál	k1gInSc1	kanál
znárodnil	znárodnit	k5eAaPmAgInS	znárodnit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
Suezské	suezský	k2eAgFnSc6d1	Suezská
krizi	krize	k1gFnSc6	krize
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
okolí	okolí	k1gNnSc3	okolí
průplavu	průplav	k1gInSc2	průplav
stalo	stát	k5eAaPmAgNnS	stát
dějištěm	dějiště	k1gNnSc7	dějiště
bojů	boj	k1gInPc2	boj
mezi	mezi	k7c7	mezi
Egyptem	Egypt	k1gInSc7	Egypt
a	a	k8xC	a
izraelsko-britsko-francouzskou	izraelskoritskorancouzský	k2eAgFnSc7d1	izraelsko-britsko-francouzský
koalicí	koalice	k1gFnSc7	koalice
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
toho	ten	k3xDgNnSc2	ten
byl	být	k5eAaImAgInS	být
kanál	kanál	k1gInSc1	kanál
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1956	[number]	k4	1956
<g/>
-	-	kIx~	-
<g/>
1957	[number]	k4	1957
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
šestidenní	šestidenní	k2eAgFnSc2d1	šestidenní
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Suezský	suezský	k2eAgInSc4d1	suezský
průplav	průplav	k1gInSc4	průplav
demarkační	demarkační	k2eAgFnSc7d1	demarkační
linií	linie	k1gFnSc7	linie
mezi	mezi	k7c7	mezi
Egyptem	Egypt	k1gInSc7	Egypt
a	a	k8xC	a
Izraelem	Izrael	k1gInSc7	Izrael
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
okupoval	okupovat	k5eAaBmAgInS	okupovat
Sinajský	sinajský	k2eAgInSc4d1	sinajský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
další	další	k2eAgInSc4d1	další
uzavření	uzavření	k1gNnSc4	uzavření
průplavu	průplav	k1gInSc2	průplav
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
oblast	oblast	k1gFnSc1	oblast
kolem	kolem	k7c2	kolem
kanálu	kanál	k1gInSc2	kanál
dějištěm	dějiště	k1gNnSc7	dějiště
nových	nový	k2eAgInPc2d1	nový
urputných	urputný	k2eAgInPc2d1	urputný
bojů	boj	k1gInPc2	boj
mezi	mezi	k7c7	mezi
Izraelem	Izrael	k1gInSc7	Izrael
a	a	k8xC	a
Egyptem	Egypt	k1gInSc7	Egypt
během	během	k7c2	během
jomkipurské	jomkipurský	k2eAgFnSc2d1	jomkipurská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nejdříve	dříve	k6eAd3	dříve
egyptská	egyptský	k2eAgFnSc1d1	egyptská
armáda	armáda	k1gFnSc1	armáda
překonala	překonat	k5eAaPmAgFnS	překonat
průplav	průplav	k1gInSc4	průplav
a	a	k8xC	a
vkročila	vkročit	k5eAaPmAgFnS	vkročit
na	na	k7c4	na
Sinaj	Sinaj	k1gInSc4	Sinaj
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
posléze	posléze	k6eAd1	posléze
zastavena	zastavit	k5eAaPmNgFnS	zastavit
a	a	k8xC	a
izraelská	izraelský	k2eAgFnSc1d1	izraelská
armáda	armáda	k1gFnSc1	armáda
sama	sám	k3xTgFnSc1	sám
překročila	překročit	k5eAaPmAgFnS	překročit
průplav	průplav	k1gInSc4	průplav
a	a	k8xC	a
vpadla	vpadnout	k5eAaPmAgFnS	vpadnout
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Průplav	průplav	k1gInSc1	průplav
zůstal	zůstat	k5eAaPmAgInS	zůstat
uzavřen	uzavřít	k5eAaPmNgInS	uzavřít
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
Průplav	průplav	k1gInSc1	průplav
nemá	mít	k5eNaImIp3nS	mít
žádná	žádný	k3yNgNnPc4	žádný
zdymadla	zdymadlo	k1gNnPc4	zdymadlo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
hladinami	hladina	k1gFnPc7	hladina
moří	mořit	k5eAaImIp3nS	mořit
i	i	k9	i
příliv	příliv	k1gInSc1	příliv
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
koncích	konec	k1gInPc6	konec
je	být	k5eAaImIp3nS	být
zanedbatelný	zanedbatelný	k2eAgInSc1d1	zanedbatelný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
prohloubení	prohloubení	k1gNnSc3	prohloubení
průplavu	průplav	k1gInSc2	průplav
z	z	k7c2	z
18	[number]	k4	18
na	na	k7c4	na
20	[number]	k4	20
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
současných	současný	k2eAgNnPc2d1	současné
kritérií	kritérion	k1gNnPc2	kritérion
mohou	moct	k5eAaImIp3nP	moct
do	do	k7c2	do
průplavu	průplav	k1gInSc2	průplav
vplout	vplout	k5eAaPmF	vplout
lodě	loď	k1gFnPc4	loď
s	s	k7c7	s
maximálním	maximální	k2eAgInSc7d1	maximální
užitečným	užitečný	k2eAgInSc7d1	užitečný
nákladem	náklad	k1gInSc7	náklad
(	(	kIx(	(
<g/>
DWT	DWT	kA	DWT
<g/>
)	)	kIx)	)
až	až	k9	až
240	[number]	k4	240
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
lodi	loď	k1gFnSc2	loď
není	být	k5eNaImIp3nS	být
omezena	omezit	k5eAaPmNgFnS	omezit
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgInSc4d1	maximální
ponor	ponor	k1gInSc4	ponor
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
20,1	[number]	k4	20,1
metru	metr	k1gInSc2	metr
při	při	k7c6	při
maximální	maximální	k2eAgFnSc6d1	maximální
šířce	šířka	k1gFnSc6	šířka
50	[number]	k4	50
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
12,2	[number]	k4	12,2
metru	metr	k1gInSc2	metr
při	při	k7c6	při
maximální	maximální	k2eAgFnSc6d1	maximální
šířce	šířka	k1gFnSc6	šířka
77,5	[number]	k4	77,5
metru	metr	k1gInSc2	metr
a	a	k8xC	a
dobrém	dobrý	k2eAgNnSc6d1	dobré
počasí	počasí	k1gNnSc6	počasí
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
výška	výška	k1gFnSc1	výška
lodi	loď	k1gFnSc2	loď
je	být	k5eAaImIp3nS	být
omezena	omezit	k5eAaPmNgFnS	omezit
na	na	k7c4	na
68	[number]	k4	68
m	m	kA	m
mostem	most	k1gInSc7	most
přes	přes	k7c4	přes
Suezský	suezský	k2eAgInSc4d1	suezský
průplav	průplav	k1gInSc4	průplav
<g/>
.	.	kIx.	.
</s>
<s>
Každou	každý	k3xTgFnSc4	každý
loď	loď	k1gFnSc4	loď
musí	muset	k5eAaImIp3nP	muset
doprovázet	doprovázet	k5eAaImF	doprovázet
čtyři	čtyři	k4xCgMnPc1	čtyři
egyptští	egyptský	k2eAgMnPc1d1	egyptský
lodivodi	lodivod	k1gMnPc1	lodivod
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
4	[number]	k4	4
úseky	úsek	k1gInPc4	úsek
průplavu	průplav	k1gInSc2	průplav
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgFnSc1d1	maximální
dovolená	dovolený	k2eAgFnSc1d1	dovolená
rychlost	rychlost	k1gFnSc1	rychlost
je	být	k5eAaImIp3nS	být
11	[number]	k4	11
až	až	k9	až
16	[number]	k4	16
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedocházelo	docházet	k5eNaImAgNnS	docházet
k	k	k7c3	k
poškozování	poškozování	k1gNnSc3	poškozování
břehů	břeh	k1gInPc2	břeh
<g/>
,	,	kIx,	,
a	a	k8xC	a
poplatky	poplatek	k1gInPc1	poplatek
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
kolem	kolem	k7c2	kolem
300	[number]	k4	300
tisíc	tisíc	k4xCgInPc2	tisíc
USD	USD	kA	USD
za	za	k7c4	za
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
loď	loď	k1gFnSc4	loď
<g/>
.	.	kIx.	.
</s>
<s>
Průplavem	průplav	k1gInSc7	průplav
proplulo	proplout	k5eAaPmAgNnS	proplout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
asi	asi	k9	asi
20	[number]	k4	20
000	[number]	k4	000
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
8	[number]	k4	8
%	%	kIx~	%
celosvětového	celosvětový	k2eAgInSc2d1	celosvětový
lodního	lodní	k2eAgInSc2d1	lodní
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Správa	správa	k1gFnSc1	správa
průplavu	průplav	k1gInSc2	průplav
na	na	k7c6	na
průjezdních	průjezdní	k2eAgInPc6d1	průjezdní
poplatcích	poplatek	k1gInPc6	poplatek
ročně	ročně	k6eAd1	ročně
utrží	utržit	k5eAaPmIp3nS	utržit
asi	asi	k9	asi
5	[number]	k4	5
miliard	miliarda	k4xCgFnPc2	miliarda
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozšíření	rozšíření	k1gNnSc6	rozšíření
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
očekává	očekávat	k5eAaImIp3nS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
lodí	loď	k1gFnPc2	loď
zdvojnásobí	zdvojnásobit	k5eAaPmIp3nS	zdvojnásobit
<g/>
,	,	kIx,	,
plavba	plavba	k1gFnSc1	plavba
zkrátí	zkrátit	k5eAaPmIp3nS	zkrátit
na	na	k7c4	na
11	[number]	k4	11
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
příjmy	příjem	k1gInPc4	příjem
zvýší	zvýšit	k5eAaPmIp3nP	zvýšit
až	až	k9	až
na	na	k7c4	na
13	[number]	k4	13
mld	mld	k?	mld
USD	USD	kA	USD
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
byl	být	k5eAaImAgInS	být
pod	pod	k7c7	pod
průplavem	průplav	k1gInSc7	průplav
prokopán	prokopat	k5eAaPmNgInS	prokopat
první	první	k4xOgInSc1	první
silniční	silniční	k2eAgInSc1d1	silniční
tunel	tunel	k1gInSc1	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
problémům	problém	k1gInPc3	problém
s	s	k7c7	s
průsaky	průsak	k1gInPc7	průsak
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
postaven	postaven	k2eAgInSc1d1	postaven
nový	nový	k2eAgInSc1d1	nový
tunel	tunel	k1gInSc1	tunel
uvnitř	uvnitř	k7c2	uvnitř
původního	původní	k2eAgMnSc2d1	původní
<g/>
.	.	kIx.	.
</s>
<s>
Plán	plán	k1gInSc1	plán
dalšího	další	k2eAgInSc2d1	další
rozvoje	rozvoj	k1gInSc2	rozvoj
suezské	suezský	k2eAgFnSc2d1	Suezská
oblasti	oblast	k1gFnSc2	oblast
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
stavbu	stavba	k1gFnSc4	stavba
dalších	další	k2eAgInPc2d1	další
sedmi	sedm	k4xCc2	sedm
tunelů	tunel	k1gInPc2	tunel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
silniční	silniční	k2eAgInSc1d1	silniční
zavěšený	zavěšený	k2eAgInSc1d1	zavěšený
most	most	k1gInSc1	most
(	(	kIx(	(
<g/>
Most	most	k1gInSc1	most
egyptsko-japonského	egyptskoaponský	k2eAgNnSc2d1	egyptsko-japonský
přátelství	přátelství	k1gNnSc2	přátelství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
vede	vést	k5eAaImIp3nS	vést
přes	přes	k7c4	přes
průplav	průplav	k1gInSc4	průplav
také	také	k9	také
elektrické	elektrický	k2eAgNnSc1d1	elektrické
vedení	vedení	k1gNnSc1	vedení
o	o	k7c6	o
napětí	napětí	k1gNnSc6	napětí
500	[number]	k4	500
kV	kV	k?	kV
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
vede	vést	k5eAaImIp3nS	vést
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
průplavu	průplav	k1gInSc2	průplav
železniční	železniční	k2eAgFnSc4d1	železniční
trať	trať	k1gFnSc4	trať
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgInSc1d1	železniční
most	most	k1gInSc1	most
přes	přes	k7c4	přes
průplav	průplav	k1gInSc4	průplav
byl	být	k5eAaImAgInS	být
zničen	zničit	k5eAaPmNgInS	zničit
během	během	k7c2	během
šestidenní	šestidenní	k2eAgFnSc2d1	šestidenní
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
postaven	postavit	k5eAaPmNgInS	postavit
byl	být	k5eAaImAgInS	být
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
otočný	otočný	k2eAgInSc1d1	otočný
most	most	k1gInSc1	most
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
