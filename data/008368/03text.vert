<p>
<s>
Cembalo	cembalo	k1gNnSc1	cembalo
(	(	kIx(	(
<g/>
zkrácenina	zkrácenina	k1gFnSc1	zkrácenina
z	z	k7c2	z
clavicembalo	clavicembalo	k1gNnSc4	clavicembalo
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
také	také	k9	také
clavecin	clavecin	k1gInSc1	clavecin
<g/>
,	,	kIx,	,
harpsichord	harpsichord	k1gInSc1	harpsichord
nebo	nebo	k8xC	nebo
spinet	spinet	k1gInSc1	spinet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
klávesový	klávesový	k2eAgInSc1d1	klávesový
nástroj	nástroj	k1gInSc1	nástroj
<g/>
,	,	kIx,	,
u	u	k7c2	u
nějž	jenž	k3xRgNnSc2	jenž
vzniká	vznikat	k5eAaImIp3nS	vznikat
zvuk	zvuk	k1gInSc1	zvuk
trsáním	trsání	k1gNnSc7	trsání
hrotu	hrot	k1gInSc2	hrot
o	o	k7c4	o
struny	struna	k1gFnPc4	struna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Praotcem	praotec	k1gMnSc7	praotec
cembala	cembalo	k1gNnSc2	cembalo
byl	být	k5eAaImAgInS	být
řecký	řecký	k2eAgInSc1d1	řecký
monochord	monochord	k1gInSc1	monochord
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
přístroj	přístroj	k1gInSc1	přístroj
na	na	k7c4	na
měření	měření	k1gNnSc4	měření
hudebních	hudební	k2eAgInPc2d1	hudební
intervalů	interval	k1gInPc2	interval
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
jediná	jediný	k2eAgFnSc1d1	jediná
struna	struna	k1gFnSc1	struna
napjatá	napjatý	k2eAgFnSc1d1	napjatá
na	na	k7c6	na
ozvučné	ozvučný	k2eAgFnSc6d1	ozvučná
desce	deska	k1gFnSc6	deska
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
posuvnou	posuvný	k2eAgFnSc7d1	posuvná
kobylkou	kobylka	k1gFnSc7	kobylka
dělila	dělit	k5eAaImAgFnS	dělit
na	na	k7c6	na
různě	různě	k6eAd1	různě
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
úseky	úsek	k1gInPc4	úsek
<g/>
,	,	kIx,	,
a	a	k8xC	a
ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
rozeznívaly	rozeznívat	k5eAaImAgFnP	rozeznívat
paličkou	palička	k1gFnSc7	palička
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
plektron	plektron	k1gNnSc1	plektron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvýšením	zvýšení	k1gNnSc7	zvýšení
počtu	počet	k1gInSc2	počet
strun	struna	k1gFnPc2	struna
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
polychord	polychord	k1gInSc1	polychord
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
již	již	k6eAd1	již
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
zamýšlen	zamýšlen	k2eAgMnSc1d1	zamýšlen
jako	jako	k8xS	jako
hudební	hudební	k2eAgInSc1d1	hudební
nástroj	nástroj	k1gInSc1	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
značně	značně	k6eAd1	značně
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
a	a	k8xC	a
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
zemích	zem	k1gFnPc6	zem
byl	být	k5eAaImAgMnS	být
znám	znám	k2eAgMnSc1d1	znám
pod	pod	k7c7	pod
různými	různý	k2eAgInPc7d1	různý
názvy	název	k1gInPc7	název
(	(	kIx(	(
<g/>
helikon	helikon	k1gInSc1	helikon
<g/>
,	,	kIx,	,
manicorde	manicorde	k6eAd1	manicorde
<g/>
,	,	kIx,	,
manicordium	manicordium	k1gNnSc1	manicordium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Připojením	připojení	k1gNnSc7	připojení
klaviatury	klaviatura	k1gFnSc2	klaviatura
a	a	k8xC	a
trsacího	trsací	k2eAgInSc2d1	trsací
mechanizmu	mechanizmus	k1gInSc2	mechanizmus
rozechvívajícího	rozechvívající	k2eAgInSc2d1	rozechvívající
struny	struna	k1gFnSc2	struna
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
<g/>
,	,	kIx,	,
nejspíše	nejspíše	k9	nejspíše
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
klavichord	klavichord	k1gInSc1	klavichord
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
šachovnice	šachovnice	k1gFnSc1	šachovnice
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
exaquir	exaquira	k1gFnPc2	exaquira
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podstatným	podstatný	k2eAgInSc7d1	podstatný
rysem	rys	k1gInSc7	rys
všech	všecek	k3xTgInPc2	všecek
těchto	tento	k3xDgInPc2	tento
nástrojů	nástroj	k1gInPc2	nástroj
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgFnPc1	všechen
struny	struna	k1gFnPc1	struna
jsou	být	k5eAaImIp3nP	být
stejně	stejně	k6eAd1	stejně
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
naladěné	naladěný	k2eAgInPc1d1	naladěný
na	na	k7c4	na
stejnou	stejný	k2eAgFnSc4d1	stejná
výšku	výška	k1gFnSc4	výška
a	a	k8xC	a
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
se	se	k3xPyFc4	se
výška	výška	k1gFnSc1	výška
tónu	tón	k1gInSc2	tón
mění	měnit	k5eAaImIp3nS	měnit
zkracováním	zkracování	k1gNnSc7	zkracování
délky	délka	k1gFnSc2	délka
struny	struna	k1gFnSc2	struna
posuvem	posuv	k1gInSc7	posuv
kobylky	kobylka	k1gFnSc2	kobylka
a	a	k8xC	a
změnou	změna	k1gFnSc7	změna
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jazýčky	jazýček	k1gInPc1	jazýček
dotýkají	dotýkat	k5eAaImIp3nP	dotýkat
strun	struna	k1gFnPc2	struna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Paralelně	paralelně	k6eAd1	paralelně
s	s	k7c7	s
klavichordem	klavichord	k1gInSc7	klavichord
vzniká	vznikat	k5eAaImIp3nS	vznikat
klavicembalo	klavicembat	k5eAaPmAgNnS	klavicembat
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
klavichordu	klavichord	k1gInSc2	klavichord
liší	lišit	k5eAaImIp3nP	lišit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnPc1	jeho
struny	struna	k1gFnPc1	struna
jsou	být	k5eAaImIp3nP	být
různě	různě	k6eAd1	různě
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
je	být	k5eAaImIp3nS	být
naladěna	naladit	k5eAaPmNgFnS	naladit
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
výšku	výška	k1gFnSc4	výška
tónu	tón	k1gInSc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
tón	tón	k1gInSc4	tón
struna	struna	k1gFnSc1	struna
jedna	jeden	k4xCgFnSc1	jeden
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
dvě	dva	k4xCgNnPc4	dva
až	až	k9	až
tři	tři	k4xCgNnPc4	tři
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Cembalo	cembalo	k1gNnSc1	cembalo
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
skříně	skříň	k1gFnSc2	skříň
<g/>
,	,	kIx,	,
klaviatury	klaviatura	k1gFnSc2	klaviatura
<g/>
,	,	kIx,	,
nohou	noha	k1gFnPc2	noha
<g/>
,	,	kIx,	,
ozvučné	ozvučný	k2eAgFnSc2d1	ozvučná
desky	deska	k1gFnSc2	deska
a	a	k8xC	a
strun	struna	k1gFnPc2	struna
<g/>
.	.	kIx.	.
</s>
<s>
Tón	tón	k1gInSc1	tón
se	se	k3xPyFc4	se
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
trsnutím	trsnutí	k1gNnSc7	trsnutí
havraního	havraní	k2eAgInSc2d1	havraní
brku	brk	k1gInSc2	brk
<g/>
,	,	kIx,	,
koženého	kožený	k2eAgInSc2d1	kožený
trnu	trn	k1gInSc2	trn
nebo	nebo	k8xC	nebo
kovového	kovový	k2eAgInSc2d1	kovový
háčku	háček	k1gInSc2	háček
o	o	k7c4	o
strunu	struna	k1gFnSc4	struna
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
ze	z	k7c2	z
strun	struna	k1gFnPc2	struna
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgNnSc4d1	vlastní
dusítko	dusítko	k1gNnSc4	dusítko
<g/>
.	.	kIx.	.
</s>
<s>
Vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
se	se	k3xPyFc4	se
tak	tak	k9	tak
zvonivý	zvonivý	k2eAgInSc1d1	zvonivý
<g/>
,	,	kIx,	,
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
krátký	krátký	k2eAgInSc4d1	krátký
tón	tón	k1gInSc4	tón
neschopný	schopný	k2eNgInSc4d1	neschopný
dynamických	dynamický	k2eAgFnPc2d1	dynamická
změn	změna	k1gFnPc2	změna
ani	ani	k8xC	ani
způsobu	způsob	k1gInSc2	způsob
trsnutí	trsnutí	k1gNnSc2	trsnutí
o	o	k7c4	o
strunu	struna	k1gFnSc4	struna
jiný	jiný	k2eAgMnSc1d1	jiný
<g/>
,	,	kIx,	,
než	než	k8xS	než
který	který	k3yRgInSc1	který
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
konstrukce	konstrukce	k1gFnSc2	konstrukce
nástroje	nástroj	k1gInPc1	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Cembalo	cembalo	k1gNnSc1	cembalo
tedy	tedy	k9	tedy
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
klavíru	klavír	k1gInSc2	klavír
nemá	mít	k5eNaImIp3nS	mít
pedály	pedál	k1gInPc4	pedál
–	–	k?	–
hráč	hráč	k1gMnSc1	hráč
hraje	hrát	k5eAaImIp3nS	hrát
pouze	pouze	k6eAd1	pouze
rukama	ruka	k1gFnPc7	ruka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Neschopnost	neschopnost	k1gFnSc1	neschopnost
dynamiky	dynamika	k1gFnSc2	dynamika
je	být	k5eAaImIp3nS	být
však	však	k9	však
vyvážená	vyvážený	k2eAgFnSc1d1	vyvážená
možností	možnost	k1gFnSc7	možnost
změny	změna	k1gFnSc2	změna
barvy	barva	k1gFnSc2	barva
tónu	tón	k1gInSc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgFnP	používat
dvě	dva	k4xCgFnPc1	dva
klaviatury	klaviatura	k1gFnPc1	klaviatura
odpovídající	odpovídající	k2eAgFnPc1d1	odpovídající
dvěma	dva	k4xCgFnPc7	dva
soustavám	soustava	k1gFnPc3	soustava
strun	struna	k1gFnPc2	struna
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jedna	jeden	k4xCgFnSc1	jeden
bývá	bývat	k5eAaImIp3nS	bývat
zdvojena	zdvojit	k5eAaPmNgFnS	zdvojit
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
varhan	varhany	k1gFnPc2	varhany
bývá	bývat	k5eAaImIp3nS	bývat
klavicembalo	klavicembat	k5eAaImAgNnS	klavicembat
vybaveno	vybavit	k5eAaPmNgNnS	vybavit
rejstříky	rejstřík	k1gInPc5	rejstřík
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
čtyřstopý	čtyřstopý	k2eAgInSc1d1	čtyřstopý
zdvojuje	zdvojovat	k5eAaImIp3nS	zdvojovat
tón	tón	k1gInSc1	tón
tónem	tón	k1gInSc7	tón
o	o	k7c6	o
oktávu	oktáv	k1gInSc6	oktáv
vyšším	vysoký	k2eAgInSc6d2	vyšší
<g/>
,	,	kIx,	,
šestnáctistopý	šestnáctistopý	k2eAgMnSc1d1	šestnáctistopý
naopak	naopak	k6eAd1	naopak
s	s	k7c7	s
tónem	tón	k1gInSc7	tón
o	o	k7c6	o
oktávu	oktáv	k1gInSc6	oktáv
nižším	nízký	k2eAgInSc6d2	nižší
<g/>
,	,	kIx,	,
osmistopý	osmistopý	k2eAgMnSc1d1	osmistopý
učiní	učinit	k5eAaImIp3nS	učinit
funkční	funkční	k2eAgFnPc4d1	funkční
spodní	spodní	k2eAgFnSc4d1	spodní
klaviaturu	klaviatura	k1gFnSc4	klaviatura
a	a	k8xC	a
dalším	další	k2eAgInSc7d1	další
rejstříkem	rejstřík	k1gInSc7	rejstřík
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
obě	dva	k4xCgFnPc1	dva
klávesnice	klávesnice	k1gFnPc1	klávesnice
spojit	spojit	k5eAaPmF	spojit
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgInPc2d1	další
efektů	efekt	k1gInPc2	efekt
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
jinými	jiný	k2eAgInPc7d1	jiný
technickými	technický	k2eAgInPc7d1	technický
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zavedení	zavedení	k1gNnSc6	zavedení
loutnového	loutnový	k2eAgInSc2d1	loutnový
rejstříku	rejstřík	k1gInSc2	rejstřík
se	se	k3xPyFc4	se
ke	k	k7c3	k
strunám	struna	k1gFnPc3	struna
přiloží	přiložit	k5eAaPmIp3nS	přiložit
lišta	lišta	k1gFnSc1	lišta
opatřená	opatřený	k2eAgFnSc1d1	opatřená
plstí	plst	k1gFnSc7	plst
<g/>
,	,	kIx,	,
u	u	k7c2	u
buffového	buffový	k2eAgInSc2d1	buffový
rejstříku	rejstřík	k1gInSc2	rejstřík
zase	zase	k9	zase
pergamenový	pergamenový	k2eAgInSc4d1	pergamenový
pásek	pásek	k1gInSc4	pásek
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
byly	být	k5eAaImAgFnP	být
konstruovány	konstruovat	k5eAaImNgInP	konstruovat
rejstříky	rejstřík	k1gInPc1	rejstřík
harfové	harfový	k2eAgInPc1d1	harfový
<g/>
,	,	kIx,	,
bubnové	bubnový	k2eAgInPc1d1	bubnový
<g/>
,	,	kIx,	,
zvonkové	zvonkový	k2eAgInPc1d1	zvonkový
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Transpoziční	transpoziční	k2eAgInSc1d1	transpoziční
rejstřík	rejstřík	k1gInSc1	rejstřík
zase	zase	k9	zase
posouvá	posouvat	k5eAaImIp3nS	posouvat
celou	celý	k2eAgFnSc4d1	celá
klaviaturu	klaviatura	k1gFnSc4	klaviatura
o	o	k7c4	o
šířku	šířka	k1gFnSc4	šířka
jedné	jeden	k4xCgFnSc2	jeden
či	či	k8xC	či
více	hodně	k6eAd2	hodně
kláves	klávesa	k1gFnPc2	klávesa
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
skutečný	skutečný	k2eAgInSc1d1	skutečný
zvuk	zvuk	k1gInSc1	zvuk
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
šířku	šířka	k1gFnSc4	šířka
posunutí	posunutí	k1gNnSc1	posunutí
vyšší	vysoký	k2eAgFnSc2d2	vyšší
či	či	k8xC	či
nižší	nízký	k2eAgFnSc2d2	nižší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podstatné	podstatný	k2eAgNnSc1d1	podstatné
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
hudebník	hudebník	k1gMnSc1	hudebník
u	u	k7c2	u
klavicembala	klavicembal	k1gMnSc2	klavicembal
seděl	sedět	k5eAaImAgMnS	sedět
čelem	čelo	k1gNnSc7	čelo
ke	k	k7c3	k
strunám	struna	k1gFnPc3	struna
nataženým	natažený	k2eAgFnPc3d1	natažená
napříč	napříč	k7c7	napříč
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
konstruovanému	konstruovaný	k2eAgInSc3d1	konstruovaný
nástroji	nástroj	k1gInSc3	nástroj
říkalo	říkat	k5eAaImAgNnS	říkat
virginal	virginal	k1gInSc4	virginal
<g/>
,	,	kIx,	,
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
spinet	spineto	k1gNnPc2	spineto
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nástrojaře	nástrojař	k1gMnSc2	nástrojař
Giovanni	Giovaneň	k1gFnSc6	Giovaneň
Spinettiho	Spinetti	k1gMnSc2	Spinetti
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nástroj	nástroj	k1gInSc1	nástroj
významně	významně	k6eAd1	významně
zdokonalil	zdokonalit	k5eAaPmAgInS	zdokonalit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
vývojem	vývoj	k1gInSc7	vývoj
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
posazení	posazení	k1gNnSc1	posazení
hráče	hráč	k1gMnSc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Hudebník	hudebník	k1gMnSc1	hudebník
je	být	k5eAaImIp3nS	být
přesazen	přesadit	k5eAaPmNgMnS	přesadit
na	na	k7c4	na
užší	úzký	k2eAgFnSc4d2	užší
stranu	strana	k1gFnSc4	strana
nástroje	nástroj	k1gInPc1	nástroj
a	a	k8xC	a
struny	struna	k1gFnPc1	struna
jsou	být	k5eAaImIp3nP	být
nataženy	natáhnout	k5eAaPmNgFnP	natáhnout
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Nástroj	nástroj	k1gInSc1	nástroj
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
značně	značně	k6eAd1	značně
podobá	podobat	k5eAaImIp3nS	podobat
cimbálu	cimbál	k1gInSc3	cimbál
a	a	k8xC	a
vžil	vžít	k5eAaPmAgMnS	vžít
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
něj	on	k3xPp3gInSc4	on
název	název	k1gInSc4	název
clavicembalo	clavicembalo	k1gNnSc1	clavicembalo
nebo	nebo	k8xC	nebo
zkráceně	zkráceně	k6eAd1	zkráceně
cembalo	cembalo	k1gNnSc1	cembalo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xC	jako
harpsichord	harpsichord	k1gMnSc1	harpsichord
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
podoby	podoba	k1gFnSc2	podoba
s	s	k7c7	s
ptačím	ptačí	k2eAgNnSc7d1	ptačí
křídlem	křídlo	k1gNnSc7	křídlo
se	se	k3xPyFc4	se
takto	takto	k6eAd1	takto
konstruovanému	konstruovaný	k2eAgNnSc3d1	konstruované
cembalu	cembalo	k1gNnSc3	cembalo
říkalo	říkat	k5eAaImAgNnS	říkat
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
přeneslo	přenést	k5eAaPmAgNnS	přenést
i	i	k9	i
na	na	k7c4	na
dnešní	dnešní	k2eAgInSc4d1	dnešní
koncertní	koncertní	k2eAgInSc4d1	koncertní
kladívkový	kladívkový	k2eAgInSc4d1	kladívkový
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
Virginaly	virginal	k1gInPc1	virginal
čili	čili	k8xC	čili
spinety	spinet	k1gInPc1	spinet
byly	být	k5eAaImAgInP	být
kompaktnější	kompaktní	k2eAgInPc1d2	kompaktnější
a	a	k8xC	a
skladnější	skladní	k2eAgInPc1d2	skladní
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
mohly	moct	k5eAaImAgFnP	moct
putovat	putovat	k5eAaImF	putovat
i	i	k9	i
do	do	k7c2	do
domácností	domácnost	k1gFnPc2	domácnost
středostavových	středostavový	k2eAgFnPc2d1	středostavový
a	a	k8xC	a
muzikálně	muzikálně	k6eAd1	muzikálně
založených	založený	k2eAgFnPc2d1	založená
rodin	rodina	k1gFnPc2	rodina
<g/>
;	;	kIx,	;
cembala	cembalo	k1gNnSc2	cembalo
"	"	kIx"	"
<g/>
křídlové	křídlový	k2eAgFnSc2d1	křídlová
<g/>
"	"	kIx"	"
konstrukce	konstrukce	k1gFnSc2	konstrukce
byla	být	k5eAaImAgFnS	být
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
rozložitější	rozložitý	k2eAgInSc4d2	rozložitější
a	a	k8xC	a
vhodnější	vhodný	k2eAgInSc4d2	vhodnější
spíše	spíše	k9	spíše
do	do	k7c2	do
rozlehlejších	rozlehlý	k2eAgInPc2d2	rozlehlejší
(	(	kIx(	(
<g/>
koncertních	koncertní	k2eAgInPc2d1	koncertní
<g/>
)	)	kIx)	)
sálů	sál	k1gInPc2	sál
–	–	k?	–
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
lepší	dobrý	k2eAgFnPc4d2	lepší
akustické	akustický	k2eAgFnPc4d1	akustická
možnosti	možnost	k1gFnPc4	možnost
dané	daný	k2eAgFnSc2d1	daná
volitelným	volitelný	k2eAgNnSc7d1	volitelné
sklápěním	sklápění	k1gNnSc7	sklápění
desky	deska	k1gFnSc2	deska
přikrývající	přikrývající	k2eAgFnSc2d1	přikrývající
struny	struna	k1gFnSc2	struna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
vynalezena	vynaleznout	k5eAaPmNgFnS	vynaleznout
kladívková	kladívkový	k2eAgFnSc1d1	kladívková
mechanika	mechanika	k1gFnSc1	mechanika
a	a	k8xC	a
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
zdokonalení	zdokonalení	k1gNnSc6	zdokonalení
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc6	rozšíření
se	se	k3xPyFc4	se
po	po	k7c6	po
celé	celá	k1gFnSc6	celá
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
zdálo	zdát	k5eAaImAgNnS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
cembalo	cembalo	k1gNnSc1	cembalo
bude	být	k5eAaImBp3nS	být
již	již	k6eAd1	již
jen	jen	k9	jen
muzejním	muzejní	k2eAgInSc7d1	muzejní
exponátem	exponát	k1gInSc7	exponát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
neočekávané	očekávaný	k2eNgFnSc3d1	neočekávaná
renesanci	renesance	k1gFnSc3	renesance
cembala	cembalo	k1gNnSc2	cembalo
<g/>
,	,	kIx,	,
když	když	k8xS	když
umělci	umělec	k1gMnPc1	umělec
i	i	k9	i
obecenstvo	obecenstvo	k1gNnSc4	obecenstvo
rozpoznali	rozpoznat	k5eAaPmAgMnP	rozpoznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
skladby	skladba	k1gFnPc1	skladba
psané	psaný	k2eAgFnPc1d1	psaná
pro	pro	k7c4	pro
cembalo	cembalo	k1gNnSc4	cembalo
znějí	znět	k5eAaImIp3nP	znět
autenticky	autenticky	k6eAd1	autenticky
a	a	k8xC	a
barevněji	barevně	k6eAd2	barevně
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
provedeny	provést	k5eAaPmNgInP	provést
na	na	k7c6	na
originálním	originální	k2eAgInSc6d1	originální
nástroji	nástroj	k1gInSc6	nástroj
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
klavír	klavír	k1gInSc1	klavír
se	se	k3xPyFc4	se
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
nedokonalou	dokonalý	k2eNgFnSc7d1	nedokonalá
náhražkou	náhražka	k1gFnSc7	náhražka
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
současní	současný	k2eAgMnPc1d1	současný
skladatelé	skladatel	k1gMnPc1	skladatel
ocenili	ocenit	k5eAaPmAgMnP	ocenit
zvukové	zvukový	k2eAgFnPc4d1	zvuková
možnosti	možnost	k1gFnPc4	možnost
cembala	cembalo	k1gNnSc2	cembalo
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
kladívkový	kladívkový	k2eAgInSc4d1	kladívkový
klavír	klavír	k1gInSc4	klavír
nemůže	moct	k5eNaImIp3nS	moct
napodobit	napodobit	k5eAaPmF	napodobit
<g/>
,	,	kIx,	,
a	a	k8xC	a
pro	pro	k7c4	pro
cembalo	cembalo	k1gNnSc4	cembalo
vzniká	vznikat	k5eAaImIp3nS	vznikat
i	i	k9	i
nová	nový	k2eAgFnSc1d1	nová
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
J.	J.	kA	J.
E.	E.	kA	E.
Jiránek	Jiránek	k1gMnSc1	Jiránek
<g/>
,	,	kIx,	,
Tomáš	Tomáš	k1gMnSc1	Tomáš
Hejzlar	Hejzlar	k1gInSc1	Hejzlar
<g/>
:	:	kIx,	:
Světem	svět	k1gInSc7	svět
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Panton	Panton	k1gInSc1	Panton
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Cimbál	cimbál	k1gInSc1	cimbál
(	(	kIx(	(
<g/>
cembalo	cembalo	k1gNnSc1	cembalo
ungarico	ungarico	k1gNnSc1	ungarico
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
cembalo	cembalo	k1gNnSc4	cembalo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
cembalo	cembalo	k1gNnSc4	cembalo
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
