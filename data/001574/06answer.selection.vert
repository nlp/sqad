<s>
Erwin	Erwin	k1gMnSc1	Erwin
Rudolf	Rudolf	k1gMnSc1	Rudolf
Josef	Josef	k1gMnSc1	Josef
Alexander	Alexandra	k1gFnPc2	Alexandra
Schrödinger	Schrödinger	k1gMnSc1	Schrödinger
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1887	[number]	k4	1887
Vídeň	Vídeň	k1gFnSc1	Vídeň
-	-	kIx~	-
4	[number]	k4	4
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1961	[number]	k4	1961
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
rakouský	rakouský	k2eAgMnSc1d1	rakouský
teoretický	teoretický	k2eAgMnSc1d1	teoretický
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
především	především	k9	především
formulací	formulace	k1gFnPc2	formulace
nerelativistické	relativistický	k2eNgFnSc2d1	nerelativistická
vlnové	vlnový	k2eAgFnSc2d1	vlnová
rovnice	rovnice	k1gFnSc2	rovnice
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
hmotných	hmotný	k2eAgFnPc2d1	hmotná
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
počest	počest	k1gFnSc4	počest
nazýváme	nazývat	k5eAaImIp1nP	nazývat
Schrödingerova	Schrödingerův	k2eAgFnSc1d1	Schrödingerova
rovnice	rovnice	k1gFnSc1	rovnice
<g/>
.	.	kIx.	.
</s>
