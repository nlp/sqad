<s>
Perla	perla	k1gFnSc1	perla
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc4d1	malý
lesklý	lesklý	k2eAgInSc4d1	lesklý
kulovitý	kulovitý	k2eAgInSc4d1	kulovitý
předmět	předmět	k1gInSc4	předmět
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
uvnitř	uvnitř	k7c2	uvnitř
perlotvorky	perlotvorka	k1gFnSc2	perlotvorka
<g/>
,	,	kIx,	,
mořské	mořský	k2eAgFnSc2d1	mořská
ústřice	ústřice	k1gFnSc2	ústřice
nebo	nebo	k8xC	nebo
perlorodky	perlorodka	k1gFnSc2	perlorodka
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
perlorodky	perlorodka	k1gFnPc4	perlorodka
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
perly	perla	k1gFnPc1	perla
tvořeny	tvořen	k2eAgFnPc1d1	tvořena
vrstvami	vrstva	k1gFnPc7	vrstva
uhličitanu	uhličitan	k1gInSc2	uhličitan
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
<g/>
.	.	kIx.	.
</s>
<s>
Ideální	ideální	k2eAgFnPc1d1	ideální
perly	perla	k1gFnPc1	perla
jsou	být	k5eAaImIp3nP	být
dokonale	dokonale	k6eAd1	dokonale
kulaté	kulatý	k2eAgInPc4d1	kulatý
a	a	k8xC	a
hladké	hladký	k2eAgInPc4d1	hladký
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
přírodních	přírodní	k2eAgFnPc2d1	přírodní
perel	perla	k1gFnPc2	perla
však	však	k9	však
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
nepravidelných	pravidelný	k2eNgInPc6d1	nepravidelný
tvarech	tvar	k1gInPc6	tvar
a	a	k8xC	a
barvách	barva	k1gFnPc6	barva
(	(	kIx(	(
<g/>
barokní	barokní	k2eAgFnSc1d1	barokní
perly	perla	k1gFnPc1	perla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Perly	perla	k1gFnPc1	perla
jsou	být	k5eAaImIp3nP	být
ceněné	ceněný	k2eAgInPc4d1	ceněný
především	především	k6eAd1	především
ve	v	k7c6	v
šperkovnictví	šperkovnictví	k1gNnSc6	šperkovnictví
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
perly	perla	k1gFnPc1	perla
vznikají	vznikat	k5eAaImIp3nP	vznikat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
do	do	k7c2	do
schránky	schránka	k1gFnSc2	schránka
ústřice	ústřice	k1gFnSc2	ústřice
vnikne	vniknout	k5eAaPmIp3nS	vniknout
cizí	cizí	k2eAgNnSc4d1	cizí
tělísko	tělísko	k1gNnSc4	tělísko
-	-	kIx~	-
např.	např.	kA	např.
zrnko	zrnko	k1gNnSc1	zrnko
písku	písek	k1gInSc2	písek
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyvolá	vyvolat	k5eAaPmIp3nS	vyvolat
obrannou	obranný	k2eAgFnSc4d1	obranná
reakci	reakce	k1gFnSc4	reakce
ústřice	ústřice	k1gFnSc2	ústřice
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
začne	začít	k5eAaPmIp3nS	začít
vylučovat	vylučovat	k5eAaImF	vylučovat
perleťovou	perleťový	k2eAgFnSc4d1	perleťová
hmotu	hmota	k1gFnSc4	hmota
usazující	usazující	k2eAgFnSc4d1	usazující
se	se	k3xPyFc4	se
na	na	k7c6	na
cizím	cizí	k2eAgNnSc6d1	cizí
tělese	těleso	k1gNnSc6	těleso
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
ústřici	ústřice	k1gFnSc6	ústřice
může	moct	k5eAaImIp3nS	moct
růst	růst	k1gInSc4	růst
současně	současně	k6eAd1	současně
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
perla	perla	k1gFnSc1	perla
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgFnPc1d1	přírodní
perly	perla	k1gFnPc1	perla
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
vzácné	vzácný	k2eAgInPc1d1	vzácný
a	a	k8xC	a
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
žádané	žádaný	k2eAgNnSc1d1	žádané
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
kvalitě	kvalita	k1gFnSc3	kvalita
a	a	k8xC	a
tvaru	tvar	k1gInSc3	tvar
perleťové	perleťový	k2eAgFnSc2d1	perleťová
hmoty	hmota	k1gFnSc2	hmota
<g/>
;	;	kIx,	;
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
koupit	koupit	k5eAaPmF	koupit
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
aukcích	aukce	k1gFnPc6	aukce
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
prodávaných	prodávaný	k2eAgFnPc2d1	prodávaná
perel	perla	k1gFnPc2	perla
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
chovů	chov	k1gInPc2	chov
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
perly	perl	k1gInPc1	perl
vznikají	vznikat	k5eAaImIp3nP	vznikat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
perly	perla	k1gFnPc1	perla
přírodní	přírodní	k2eAgFnPc1d1	přírodní
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
,	,	kIx,	,
že	že	k8xS	že
cizí	cizí	k2eAgNnSc4d1	cizí
tělísko	tělísko	k1gNnSc4	tělísko
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
ústřic	ústřice	k1gFnPc2	ústřice
zavedeno	zavést	k5eAaPmNgNnS	zavést
úmyslně	úmyslně	k6eAd1	úmyslně
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
růstu	růst	k1gInSc2	růst
perly	perla	k1gFnSc2	perla
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
jednoho	jeden	k4xCgInSc2	jeden
roku	rok	k1gInSc2	rok
do	do	k7c2	do
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hlavním	hlavní	k2eAgFnPc3d1	hlavní
oblastem	oblast	k1gFnPc3	oblast
chovu	chov	k1gInSc2	chov
perel	perla	k1gFnPc2	perla
patří	patřit	k5eAaImIp3nS	patřit
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
Tahiti	Tahiti	k1gNnSc1	Tahiti
a	a	k8xC	a
Vietnam	Vietnam	k1gInSc1	Vietnam
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
několik	několik	k4yIc4	několik
základních	základní	k2eAgInPc2d1	základní
druhů	druh	k1gInPc2	druh
perel	perla	k1gFnPc2	perla
<g/>
:	:	kIx,	:
Perly	perl	k1gInPc1	perl
Akoya	Akoyum	k1gNnSc2	Akoyum
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
slaná	slaný	k2eAgFnSc1d1	slaná
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
Vietnamu	Vietnam	k1gInSc2	Vietnam
či	či	k8xC	či
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Mívají	mívat	k5eAaImIp3nP	mívat
velikost	velikost	k1gFnSc4	velikost
5	[number]	k4	5
až	až	k9	až
9	[number]	k4	9
mm	mm	kA	mm
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
barva	barva	k1gFnSc1	barva
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
rozličné	rozličný	k2eAgNnSc1d1	rozličné
zbarvení	zbarvení	k1gNnSc1	zbarvení
<g/>
:	:	kIx,	:
krémové	krémový	k2eAgFnPc1d1	krémová
<g/>
,	,	kIx,	,
růžové	růžový	k2eAgFnPc1d1	růžová
<g/>
,	,	kIx,	,
stříbřité	stříbřitý	k2eAgFnPc1d1	stříbřitá
nebo	nebo	k8xC	nebo
zelené	zelený	k2eAgFnPc1d1	zelená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teplejších	teplý	k2eAgFnPc6d2	teplejší
vodách	voda	k1gFnPc6	voda
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Číny	Čína	k1gFnSc2	Čína
mohou	moct	k5eAaImIp3nP	moct
perly	perl	k1gInPc4	perl
růst	růst	k5eAaImF	růst
až	až	k9	až
dvakrát	dvakrát	k6eAd1	dvakrát
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
v	v	k7c6	v
chladnějších	chladný	k2eAgFnPc6d2	chladnější
japonských	japonský	k2eAgFnPc6d1	japonská
vodách	voda	k1gFnPc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tahitské	tahitský	k2eAgInPc1d1	tahitský
perly	perl	k1gInPc1	perl
jsou	být	k5eAaImIp3nP	být
produkovány	produkován	k2eAgMnPc4d1	produkován
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
druhem	druh	k1gInSc7	druh
ústřic	ústřice	k1gFnPc2	ústřice
s	s	k7c7	s
černými	černý	k2eAgInPc7d1	černý
okraji	okraj	k1gInPc7	okraj
vyskytujícím	vyskytující	k2eAgInPc3d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
Polynésie	Polynésie	k1gFnSc2	Polynésie
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
perly	perl	k1gInPc1	perl
jsou	být	k5eAaImIp3nP	být
vzácné	vzácný	k2eAgInPc1d1	vzácný
i	i	k9	i
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
těchto	tento	k3xDgFnPc2	tento
ústřic	ústřice	k1gFnPc2	ústřice
v	v	k7c6	v
chovech	chov	k1gInPc6	chov
přežívá	přežívat	k5eAaImIp3nS	přežívat
jen	jen	k9	jen
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
perly	perla	k1gFnPc1	perla
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
navzájem	navzájem	k6eAd1	navzájem
značně	značně	k6eAd1	značně
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
např.	např.	kA	např.
pro	pro	k7c4	pro
sestavení	sestavení	k1gNnSc4	sestavení
náhrdelníku	náhrdelník	k1gInSc2	náhrdelník
je	být	k5eAaImIp3nS	být
jich	on	k3xPp3gFnPc2	on
potřeba	potřeba	k6eAd1	potřeba
roztřídit	roztřídit	k5eAaPmF	roztřídit
stovky	stovka	k1gFnPc4	stovka
<g/>
.	.	kIx.	.
</s>
<s>
Perly	perla	k1gFnPc1	perla
z	z	k7c2	z
jižního	jižní	k2eAgInSc2d1	jižní
Pacifiku	Pacifik	k1gInSc2	Pacifik
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
Barmě	Barma	k1gFnSc6	Barma
<g/>
,	,	kIx,	,
Indonésii	Indonésie	k1gFnSc6	Indonésie
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
sousedních	sousední	k2eAgFnPc6d1	sousední
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
měří	měřit	k5eAaImIp3nS	měřit
1	[number]	k4	1
cm	cm	kA	cm
-	-	kIx~	-
2	[number]	k4	2
cm	cm	kA	cm
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
prodávají	prodávat	k5eAaImIp3nP	prodávat
za	za	k7c4	za
vysoké	vysoký	k2eAgFnPc4d1	vysoká
ceny	cena	k1gFnPc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
zbarvení	zbarvení	k1gNnSc1	zbarvení
bývá	bývat	k5eAaImIp3nS	bývat
bílé	bílý	k2eAgNnSc1d1	bílé
<g/>
,	,	kIx,	,
žluté	žlutý	k2eAgFnPc1d1	žlutá
<g/>
,	,	kIx,	,
žlutooranžové	žlutooranžový	k2eAgFnPc1d1	žlutooranžová
nebo	nebo	k8xC	nebo
modravé	modravý	k2eAgFnPc1d1	modravá
<g/>
;	;	kIx,	;
navíc	navíc	k6eAd1	navíc
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
růžový	růžový	k2eAgMnSc1d1	růžový
<g/>
,	,	kIx,	,
zelený	zelený	k2eAgInSc1d1	zelený
nebo	nebo	k8xC	nebo
modrý	modrý	k2eAgInSc1d1	modrý
nádech	nádech	k1gInSc1	nádech
<g/>
.	.	kIx.	.
</s>
<s>
Perly	perla	k1gFnPc1	perla
Mabe	Mabe	k1gNnSc2	Mabe
mají	mít	k5eAaImIp3nP	mít
tvar	tvar	k1gInSc4	tvar
polokoule	polokoule	k1gFnSc2	polokoule
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznikají	vznikat	k5eAaImIp3nP	vznikat
usazené	usazený	k2eAgFnPc1d1	usazená
uvnitř	uvnitř	k7c2	uvnitř
ústřic	ústřice	k1gFnPc2	ústřice
na	na	k7c6	na
stěnách	stěna	k1gFnPc6	stěna
jejich	jejich	k3xOp3gFnPc2	jejich
schránek	schránka	k1gFnPc2	schránka
<g/>
.	.	kIx.	.
</s>
<s>
Pěstují	pěstovat	k5eAaImIp3nP	pěstovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
USA	USA	kA	USA
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgInSc3	svůj
tvaru	tvar	k1gInSc3	tvar
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
především	především	k9	především
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
prstenů	prsten	k1gInPc2	prsten
a	a	k8xC	a
náušnic	náušnice	k1gFnPc2	náušnice
k	k	k7c3	k
nimž	jenž	k3xRgInPc3	jenž
jsou	být	k5eAaImIp3nP	být
upevňovány	upevňován	k2eAgFnPc1d1	upevňována
plochou	plocha	k1gFnSc7	plocha
stranou	stranou	k6eAd1	stranou
<g/>
.	.	kIx.	.
</s>
<s>
Sladkovodní	sladkovodní	k2eAgInPc1d1	sladkovodní
perly	perl	k1gInPc1	perl
jsou	být	k5eAaImIp3nP	být
produkovány	produkován	k2eAgFnPc4d1	produkována
mj.	mj.	kA	mj.
perlorodkou	perlorodka	k1gFnSc7	perlorodka
říční	říční	k2eAgFnSc7d1	říční
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chovu	chov	k1gInSc6	chov
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
cizí	cizí	k2eAgNnPc4d1	cizí
tělíska	tělísko	k1gNnPc4	tělísko
menší	malý	k2eAgFnSc2d2	menší
než	než	k8xS	než
v	v	k7c6	v
mořských	mořský	k2eAgFnPc6d1	mořská
farmách	farma	k1gFnPc6	farma
<g/>
.	.	kIx.	.
</s>
<s>
Sladkovodní	sladkovodní	k2eAgInPc1d1	sladkovodní
perly	perl	k1gInPc1	perl
jsou	být	k5eAaImIp3nP	být
levnější	levný	k2eAgInPc1d2	levnější
než	než	k8xS	než
mořské	mořský	k2eAgInPc1d1	mořský
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
perlorodce	perlorodka	k1gFnSc6	perlorodka
může	moct	k5eAaImIp3nS	moct
růst	růst	k1gInSc4	růst
až	až	k9	až
20	[number]	k4	20
perel	perla	k1gFnPc2	perla
současně	současně	k6eAd1	současně
<g/>
...	...	k?	...
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Perla	perla	k1gFnSc1	perla
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
