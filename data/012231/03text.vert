<p>
<s>
Dytík	Dytík	k1gMnSc1	Dytík
úhorní	úhorní	k2eAgMnSc1d1	úhorní
(	(	kIx(	(
<g/>
Burhinus	Burhinus	k1gMnSc1	Burhinus
oedicnemus	oedicnemus	k1gMnSc1	oedicnemus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velkým	velký	k2eAgInSc7d1	velký
druhem	druh	k1gInSc7	druh
bahňáka	bahňák	k1gMnSc2	bahňák
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
dytíkovitých	dytíkovitý	k2eAgFnPc2d1	dytíkovitý
(	(	kIx(	(
<g/>
Burhinidae	Burhinidae	k1gFnPc2	Burhinidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Velikosti	velikost	k1gFnPc1	velikost
racka	racek	k1gMnSc2	racek
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
38	[number]	k4	38
<g/>
–	–	k?	–
<g/>
45	[number]	k4	45
cm	cm	kA	cm
<g/>
,	,	kIx,	,
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
76	[number]	k4	76
<g/>
–	–	k?	–
<g/>
88	[number]	k4	88
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
nenápadně	nápadně	k6eNd1	nápadně
zbarvený	zbarvený	k2eAgMnSc1d1	zbarvený
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
snadno	snadno	k6eAd1	snadno
uniká	unikat	k5eAaImIp3nS	unikat
pozornosti	pozornost	k1gFnSc3	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
křídle	křídlo	k1gNnSc6	křídlo
má	mít	k5eAaImIp3nS	mít
kontrastní	kontrastní	k2eAgFnSc2d1	kontrastní
černé	černý	k2eAgFnSc2d1	černá
a	a	k8xC	a
bílé	bílý	k2eAgFnSc2d1	bílá
proužky	proužka	k1gFnSc2	proužka
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýraznější	výrazný	k2eAgFnPc1d3	nejvýraznější
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gFnPc1	jeho
velké	velká	k1gFnPc1	velká
žluté	žlutý	k2eAgFnPc1d1	žlutá
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
žlutý	žlutý	k2eAgInSc4d1	žlutý
zobák	zobák	k1gInSc4	zobák
s	s	k7c7	s
černou	černý	k2eAgFnSc7d1	černá
špičkou	špička	k1gFnSc7	špička
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
pohlaví	pohlaví	k1gNnPc1	pohlaví
jsou	být	k5eAaImIp3nP	být
zbarvena	zbarvit	k5eAaPmNgNnP	zbarvit
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
postrádají	postrádat	k5eAaImIp3nP	postrádat
výrazné	výrazný	k2eAgNnSc4d1	výrazné
kontrastní	kontrastní	k2eAgNnSc4d1	kontrastní
zbarvení	zbarvení	k1gNnSc4	zbarvení
na	na	k7c6	na
křídlech	křídlo	k1gNnPc6	křídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
na	na	k7c6	na
rozsáhlém	rozsáhlý	k2eAgNnSc6d1	rozsáhlé
území	území	k1gNnSc6	území
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Částečně	částečně	k6eAd1	částečně
tažný	tažný	k2eAgInSc1d1	tažný
<g/>
,	,	kIx,	,
se	s	k7c7	s
zimovišti	zimoviště	k1gNnPc7	zimoviště
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
<g/>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
krajině	krajina	k1gFnSc6	krajina
(	(	kIx(	(
<g/>
vřesoviště	vřesoviště	k1gNnSc1	vřesoviště
<g/>
,	,	kIx,	,
okraje	okraj	k1gInPc1	okraj
stepí	step	k1gFnPc2	step
<g/>
,	,	kIx,	,
vyschlé	vyschlý	k2eAgNnSc4d1	vyschlé
bahno	bahno	k1gNnSc4	bahno
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
hnízdil	hnízdit	k5eAaImAgInS	hnízdit
také	také	k9	také
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
především	především	k9	především
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
nebylo	být	k5eNaImAgNnS	být
hnízdění	hnízdění	k1gNnSc1	hnízdění
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
.	.	kIx.	.
</s>
<s>
Zvláště	zvláště	k6eAd1	zvláště
chráněný	chráněný	k2eAgInSc1d1	chráněný
jako	jako	k8xC	jako
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekologie	ekologie	k1gFnSc1	ekologie
==	==	k?	==
</s>
</p>
<p>
<s>
Nejaktivnější	aktivní	k2eAgFnSc1d3	nejaktivnější
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
hlavně	hlavně	k6eAd1	hlavně
bezobratlými	bezobratlí	k1gMnPc7	bezobratlí
(	(	kIx(	(
<g/>
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
žížalami	žížala	k1gFnPc7	žížala
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
občas	občas	k6eAd1	občas
loví	lovit	k5eAaImIp3nP	lovit
i	i	k9	i
malé	malý	k2eAgMnPc4d1	malý
savce	savec	k1gMnPc4	savec
nebo	nebo	k8xC	nebo
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
je	být	k5eAaImIp3nS	být
důlek	důlek	k1gInSc4	důlek
v	v	k7c6	v
holé	holý	k2eAgFnSc6d1	holá
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
snůška	snůška	k1gFnSc1	snůška
čítá	čítat	k5eAaImIp3nS	čítat
2	[number]	k4	2
53,7	[number]	k4	53,7
×	×	k?	×
38,6	[number]	k4	38,6
mm	mm	kA	mm
velká	velký	k2eAgNnPc4d1	velké
vejce	vejce	k1gNnPc4	vejce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
==	==	k?	==
</s>
</p>
<p>
<s>
Dytík	Dytík	k1gInSc1	Dytík
úhorní	úhorní	k2eAgInSc1d1	úhorní
je	být	k5eAaImIp3nS	být
chován	chovat	k5eAaImNgInS	chovat
ve	v	k7c4	v
38	[number]	k4	38
evropských	evropský	k2eAgFnPc2d1	Evropská
zoo	zoo	k1gFnPc2	zoo
(	(	kIx(	(
<g/>
stav	stav	k1gInSc1	stav
podzim	podzim	k1gInSc1	podzim
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
v	v	k7c6	v
zoo	zoo	k1gFnSc6	zoo
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
je	být	k5eAaImIp3nS	být
chován	chovat	k5eAaImNgInS	chovat
v	v	k7c6	v
pěti	pět	k4xCc6	pět
zoologických	zoologický	k2eAgFnPc6d1	zoologická
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Dvůr	Dvůr	k1gInSc1	Dvůr
Králové	Králová	k1gFnSc2	Králová
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Hluboká	Hluboká	k1gFnSc1	Hluboká
</s>
</p>
<p>
<s>
Zoopark	zoopark	k1gInSc1	zoopark
Chomutov	Chomutov	k1gInSc1	Chomutov
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Plzeň	Plzeň	k1gFnSc1	Plzeň
</s>
</p>
<p>
<s>
Zoo	zoo	k1gFnSc1	zoo
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
===	===	k?	===
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
Zoo	zoo	k1gFnSc6	zoo
Praha	Praha	k1gFnSc1	Praha
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
dytík	dytík	k1gInSc1	dytík
úhorní	úhorní	k2eAgFnSc2d1	úhorní
se	se	k3xPyFc4	se
do	do	k7c2	do
Zoo	zoo	k1gFnSc2	zoo
Praha	Praha	k1gFnSc1	Praha
dostal	dostat	k5eAaPmAgMnS	dostat
snad	snad	k9	snad
již	již	k6eAd1	již
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Údaje	údaj	k1gInPc1	údaj
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
zcela	zcela	k6eAd1	zcela
přesné	přesný	k2eAgInPc1d1	přesný
a	a	k8xC	a
kompletní	kompletní	k2eAgInPc1d1	kompletní
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc4	počátek
současného	současný	k2eAgInSc2d1	současný
chovu	chov	k1gInSc2	chov
se	se	k3xPyFc4	se
datují	datovat	k5eAaImIp3nP	datovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
podařil	podařit	k5eAaPmAgInS	podařit
první	první	k4xOgInSc1	první
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
odchov	odchov	k1gInSc1	odchov
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
byly	být	k5eAaImAgInP	být
chovány	chován	k2eAgInPc1d1	chován
dva	dva	k4xCgInPc1	dva
páry	pár	k1gInPc1	pár
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
odchovat	odchovat	k5eAaPmF	odchovat
samici	samice	k1gFnSc4	samice
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
chováno	chovat	k5eAaImNgNnS	chovat
5	[number]	k4	5
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2019	[number]	k4	2019
se	se	k3xPyFc4	se
vylíhlo	vylíhnout	k5eAaPmAgNnS	vylíhnout
další	další	k2eAgNnSc1d1	další
mládě	mládě	k1gNnSc1	mládě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
ptáků	pták	k1gMnPc2	pták
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
dytík	dytík	k1gMnSc1	dytík
úhorní	úhorní	k2eAgMnSc1d1	úhorní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
dytík	dytík	k1gMnSc1	dytík
úhorní	úhorní	k2eAgMnSc1d1	úhorní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Burhinus	Burhinus	k1gInSc1	Burhinus
oedicnemus	oedicnemus	k1gInSc1	oedicnemus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
