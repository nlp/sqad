<s>
Lineární	lineární	k2eAgInSc1d1	lineární
přenos	přenos	k1gInSc1	přenos
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
LET	let	k1gInSc1	let
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
popisující	popisující	k2eAgFnSc1d1	popisující
hustotu	hustota	k1gFnSc4	hustota
předávání	předávání	k1gNnSc2	předávání
energie	energie	k1gFnSc2	energie
prostředí	prostředí	k1gNnSc4	prostředí
ionizujícím	ionizující	k2eAgNnSc7d1	ionizující
zářením	záření	k1gNnSc7	záření
<g/>
.	.	kIx.	.
</s>
