<p>
<s>
Biret	biret	k1gInSc1	biret
je	být	k5eAaImIp3nS	být
liturgická	liturgický	k2eAgFnSc1d1	liturgická
pokrývka	pokrývka	k1gFnSc1	pokrývka
hlavy	hlava	k1gFnSc2	hlava
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c6	v
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
při	při	k7c6	při
běžném	běžný	k2eAgNnSc6d1	běžné
nošení	nošení	k1gNnSc6	nošení
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
klerikou	klerika	k1gFnSc7	klerika
a	a	k8xC	a
při	při	k7c6	při
různých	různý	k2eAgInPc6d1	různý
liturgických	liturgický	k2eAgInPc6d1	liturgický
úkonech	úkon	k1gInPc6	úkon
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
pohřeb	pohřeb	k1gInSc1	pohřeb
<g/>
,	,	kIx,	,
svatba	svatba	k1gFnSc1	svatba
<g/>
,	,	kIx,	,
křest	křest	k1gInSc1	křest
<g/>
,	,	kIx,	,
požehnání	požehnání	k1gNnSc1	požehnání
atd.	atd.	kA	atd.
Nejde	jít	k5eNaImIp3nS	jít
však	však	k9	však
používat	používat	k5eAaImF	používat
při	při	k7c6	při
mši	mše	k1gFnSc6	mše
svaté	svatý	k2eAgFnSc6d1	svatá
<g/>
.	.	kIx.	.
</s>
<s>
Kněz	kněz	k1gMnSc1	kněz
nosí	nosit	k5eAaImIp3nS	nosit
biret	biret	k1gInSc4	biret
černé	černý	k2eAgFnSc2d1	černá
barvy	barva	k1gFnSc2	barva
podle	podle	k7c2	podle
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
diecésních	diecésní	k2eAgInPc2d1	diecésní
zvyků	zvyk	k1gInPc2	zvyk
např.	např.	kA	např.
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
rohy	roh	k1gInPc7	roh
a	a	k8xC	a
se	s	k7c7	s
střapcem	střapec	k1gInSc7	střapec
a	a	k8xC	a
pod	pod	k7c7	pod
<g/>
.	.	kIx.	.
</s>
<s>
Kněží	kněz	k1gMnPc1	kněz
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
monsignore	monsignore	k1gMnSc1	monsignore
nosí	nosit	k5eAaImIp3nS	nosit
biret	biret	k1gInSc4	biret
černý	černý	k2eAgInSc4d1	černý
s	s	k7c7	s
fialovým	fialový	k2eAgInSc7d1	fialový
střapcem	střapec	k1gInSc7	střapec
<g/>
.	.	kIx.	.
</s>
<s>
Biskupové	biskup	k1gMnPc1	biskup
<g/>
,	,	kIx,	,
kanovníci	kanovník	k1gMnPc1	kanovník
a	a	k8xC	a
církevní	církevní	k2eAgMnPc1d1	církevní
soudní	soudní	k2eAgMnPc1d1	soudní
vikáři	vikář	k1gMnPc1	vikář
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
monsignore	monsignore	k1gMnSc1	monsignore
nosí	nosit	k5eAaImIp3nS	nosit
biret	biret	k1gInSc4	biret
fialové	fialový	k2eAgFnSc2d1	fialová
barvy	barva	k1gFnSc2	barva
se	s	k7c7	s
střapcem	střapec	k1gInSc7	střapec
<g/>
.	.	kIx.	.
</s>
<s>
Kardinálové	kardinál	k1gMnPc1	kardinál
nosí	nosit	k5eAaImIp3nP	nosit
biret	biret	k1gInSc4	biret
červené	červený	k2eAgFnSc2d1	červená
barvy	barva	k1gFnSc2	barva
bez	bez	k7c2	bez
střapce	střapec	k1gInSc2	střapec
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
řeholníci	řeholník	k1gMnPc1	řeholník
pak	pak	k6eAd1	pak
používají	používat	k5eAaImIp3nP	používat
biret	biret	k1gInSc4	biret
jiné	jiný	k2eAgFnSc2d1	jiná
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
např.	např.	kA	např.
premonstráti	premonstrát	k1gMnPc1	premonstrát
nosí	nosit	k5eAaImIp3nP	nosit
biret	biret	k1gInSc4	biret
bílý	bílý	k2eAgInSc4d1	bílý
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
rohy	roh	k1gInPc7	roh
<g/>
.	.	kIx.	.
</s>
<s>
Ministranti	ministrant	k1gMnPc1	ministrant
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
používat	používat	k5eAaImF	používat
biret	biret	k1gInSc4	biret
černé	černý	k2eAgFnSc2d1	černá
barvy	barva	k1gFnSc2	barva
se	s	k7c7	s
střapcem	střapec	k1gInSc7	střapec
a	a	k8xC	a
čtyřmi	čtyři	k4xCgInPc7	čtyři
rohy	roh	k1gInPc7	roh
(	(	kIx(	(
<g/>
používají	používat	k5eAaImIp3nP	používat
ho	on	k3xPp3gNnSc4	on
většinou	většinou	k6eAd1	většinou
při	při	k7c6	při
pohřbech	pohřeb	k1gInPc6	pohřeb
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Infule	infule	k1gFnSc1	infule
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Biret	biret	k1gInSc1	biret
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
