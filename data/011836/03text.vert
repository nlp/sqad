<p>
<s>
Ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
UV	UV	kA	UV
<g/>
,	,	kIx,	,
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
ultraviolet	ultraviolet	k1gInSc1	ultraviolet
<g/>
)	)	kIx)	)
záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
s	s	k7c7	s
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
délkou	délka	k1gFnSc7	délka
kratší	krátký	k2eAgFnSc7d2	kratší
než	než	k8xS	než
má	mít	k5eAaImIp3nS	mít
viditelné	viditelný	k2eAgNnSc1d1	viditelné
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
delší	dlouhý	k2eAgMnSc1d2	delší
než	než	k8xS	než
má	mít	k5eAaImIp3nS	mít
rentgenové	rentgenový	k2eAgNnSc4d1	rentgenové
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
je	být	k5eAaImIp3nS	být
neviditelné	viditelný	k2eNgNnSc1d1	neviditelné
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
však	však	k9	však
živočichové	živočich	k1gMnPc1	živočich
(	(	kIx(	(
<g/>
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
plazi	plaz	k1gMnPc1	plaz
<g/>
,	,	kIx,	,
některý	některý	k3yIgInSc4	některý
hmyz	hmyz	k1gInSc4	hmyz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jej	on	k3xPp3gMnSc4	on
dokáží	dokázat	k5eAaPmIp3nP	dokázat
vnímat	vnímat	k5eAaImF	vnímat
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
přirozeným	přirozený	k2eAgInSc7d1	přirozený
zdrojem	zdroj	k1gInSc7	zdroj
je	být	k5eAaImIp3nS	být
Slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Objev	objev	k1gInSc4	objev
==	==	k?	==
</s>
</p>
<p>
<s>
Ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
záření	záření	k1gNnSc1	záření
objevil	objevit	k5eAaPmAgMnS	objevit
německý	německý	k2eAgMnSc1d1	německý
fyzik	fyzik	k1gMnSc1	fyzik
Johann	Johann	k1gMnSc1	Johann
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Ritter	Ritter	k1gMnSc1	Ritter
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1801	[number]	k4	1801
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenoval	pojmenovat	k5eAaPmAgInS	pojmenovat
ho	on	k3xPp3gInSc4	on
"	"	kIx"	"
<g/>
dezoxidační	dezoxidační	k2eAgInSc4d1	dezoxidační
<g/>
"	"	kIx"	"
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Nynější	nynější	k2eAgInSc1d1	nynější
název	název	k1gInSc1	název
dostal	dostat	k5eAaPmAgInS	dostat
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Souvislost	souvislost	k1gFnSc1	souvislost
s	s	k7c7	s
evolucí	evoluce	k1gFnSc7	evoluce
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
moderních	moderní	k2eAgInPc2d1	moderní
modelů	model	k1gInPc2	model
evoluce	evoluce	k1gFnSc2	evoluce
je	být	k5eAaImIp3nS	být
vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
evoluce	evoluce	k1gFnSc1	evoluce
prvotních	prvotní	k2eAgInPc2d1	prvotní
proteinů	protein	k1gInPc2	protein
a	a	k8xC	a
enzymů	enzym	k1gInPc2	enzym
schopných	schopný	k2eAgMnPc2d1	schopný
reprodukce	reprodukce	k1gFnSc2	reprodukce
připisován	připisován	k2eAgInSc4d1	připisován
právě	právě	k6eAd1	právě
existenci	existence	k1gFnSc4	existence
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sousední	sousední	k2eAgInPc1d1	sousední
dvoušroubovicové	dvoušroubovicový	k2eAgInPc1d1	dvoušroubovicový
páry	pár	k1gInPc1	pár
thyminu	thymin	k1gInSc2	thymin
v	v	k7c6	v
DNA	DNA	kA	DNA
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
spojit	spojit	k5eAaPmF	spojit
do	do	k7c2	do
kovalentní	kovalentní	k2eAgFnSc2d1	kovalentní
vazby	vazba	k1gFnSc2	vazba
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
přerušit	přerušit	k5eAaPmF	přerušit
vlákno	vlákno	k1gNnSc1	vlákno
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
reproduktivní	reproduktivní	k2eAgInPc1d1	reproduktivní
enzymy	enzym	k1gInPc1	enzym
nedokáží	dokázat	k5eNaPmIp3nP	dokázat
zkopírovat	zkopírovat	k5eAaPmF	zkopírovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
během	během	k7c2	během
genetické	genetický	k2eAgFnSc2d1	genetická
replikace	replikace	k1gFnSc2	replikace
či	či	k8xC	či
syntézy	syntéza	k1gFnSc2	syntéza
proteinů	protein	k1gInPc2	protein
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
posunutí	posunutí	k1gNnSc3	posunutí
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
orientovaných	orientovaný	k2eAgFnPc2d1	orientovaná
bází	báze	k1gFnPc2	báze
DNA	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
konečným	konečný	k2eAgInSc7d1	konečný
důsledkem	důsledek	k1gInSc7	důsledek
je	být	k5eAaImIp3nS	být
selhání	selhání	k1gNnSc1	selhání
přenosu	přenos	k1gInSc2	přenos
genetické	genetický	k2eAgFnPc4d1	genetická
informace	informace	k1gFnPc4	informace
a	a	k8xC	a
smrt	smrt	k1gFnSc4	smrt
organizmu	organizmus	k1gInSc2	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
prokaryotické	prokaryotický	k2eAgInPc1d1	prokaryotický
organismy	organismus	k1gInPc1	organismus
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
přibližovaly	přibližovat	k5eAaImAgInP	přibližovat
hladině	hladina	k1gFnSc3	hladina
prehistorických	prehistorický	k2eAgInPc2d1	prehistorický
oceánů	oceán	k1gInPc2	oceán
–	–	k?	–
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
zformována	zformován	k2eAgFnSc1d1	zformována
ozónová	ozónový	k2eAgFnSc1d1	ozónová
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
,	,	kIx,	,
blokující	blokující	k2eAgFnSc4d1	blokující
většinu	většina	k1gFnSc4	většina
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
záření	záření	k1gNnSc2	záření
–	–	k?	–
neustále	neustále	k6eAd1	neustále
hynuly	hynout	k5eAaImAgFnP	hynout
<g/>
.	.	kIx.	.
</s>
<s>
Těch	ten	k3xDgMnPc2	ten
několik	několik	k4yIc1	několik
málo	málo	k6eAd1	málo
přeživších	přeživší	k2eAgFnPc2d1	přeživší
si	se	k3xPyFc3	se
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
enzymy	enzym	k1gInPc4	enzym
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
přepracovaly	přepracovat	k5eAaPmAgFnP	přepracovat
a	a	k8xC	a
rozbily	rozbít	k5eAaPmAgFnP	rozbít
thyminové	thyminový	k2eAgFnPc1d1	thyminový
kovalentní	kovalentní	k2eAgFnPc1d1	kovalentní
vazby	vazba	k1gFnPc1	vazba
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
excision	excision	k1gInSc4	excision
repair	repaira	k1gFnPc2	repaira
enzymes	enzymesa	k1gFnPc2	enzymesa
–	–	k?	–
enzymy	enzym	k1gInPc1	enzym
opravující	opravující	k2eAgInPc1d1	opravující
vynechání	vynechání	k1gNnSc4	vynechání
při	při	k7c6	při
spiralizaci	spiralizace	k1gFnSc6	spiralizace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
enzymů	enzym	k1gInPc2	enzym
a	a	k8xC	a
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nP	účastnit
moderní	moderní	k2eAgFnPc1d1	moderní
mitózy	mitóza	k1gFnPc1	mitóza
a	a	k8xC	a
meiózy	meióza	k1gFnPc1	meióza
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
extrémně	extrémně	k6eAd1	extrémně
podobné	podobný	k2eAgInPc1d1	podobný
enzymům	enzym	k1gInPc3	enzym
opravujícím	opravující	k2eAgInSc7d1	opravující
vynechání	vynechání	k1gNnSc4	vynechání
při	při	k7c6	při
spiralizaci	spiralizace	k1gFnSc6	spiralizace
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
potomky	potomek	k1gMnPc4	potomek
enzymů	enzym	k1gInPc2	enzym
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
poprvé	poprvé	k6eAd1	poprvé
přestály	přestát	k5eAaPmAgInP	přestát
působení	působení	k1gNnSc4	působení
ultrafialového	ultrafialový	k2eAgNnSc2d1	ultrafialové
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
==	==	k?	==
</s>
</p>
<p>
<s>
UV	UV	kA	UV
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
oblast	oblast	k1gFnSc1	oblast
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
blízké	blízký	k2eAgNnSc4d1	blízké
ultrafialové	ultrafialový	k2eAgNnSc4d1	ultrafialové
záření	záření	k1gNnSc4	záření
o	o	k7c6	o
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
400	[number]	k4	400
<g/>
–	–	k?	–
<g/>
200	[number]	k4	200
nm	nm	k?	nm
<g/>
)	)	kIx)	)
a	a	k8xC	a
daleké	daleký	k2eAgNnSc1d1	daleké
ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
záření	záření	k1gNnSc1	záření
(	(	kIx(	(
<g/>
200	[number]	k4	200
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
nm	nm	k?	nm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
energií	energie	k1gFnSc7	energie
fotonů	foton	k1gInPc2	foton
mezi	mezi	k7c7	mezi
3,1	[number]	k4	3,1
a	a	k8xC	a
124	[number]	k4	124
eV.	eV.	k?	eV.
</s>
</p>
<p>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
na	na	k7c4	na
spektrální	spektrální	k2eAgFnPc4d1	spektrální
oblasti	oblast	k1gFnPc4	oblast
(	(	kIx(	(
<g/>
též	též	k9	též
"	"	kIx"	"
<g/>
typy	typ	k1gInPc4	typ
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
UVA	UVA	kA	UVA
<g/>
,	,	kIx,	,
UVB	UVB	kA	UVB
a	a	k8xC	a
UVC	UVC	kA	UVC
je	být	k5eAaImIp3nS	být
především	především	k9	především
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
biologických	biologický	k2eAgInPc2d1	biologický
účinků	účinek	k1gInPc2	účinek
UV	UV	kA	UV
záření	záření	k1gNnSc1	záření
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
vzduchoprázdné	vzduchoprázdný	k2eAgNnSc1d1	vzduchoprázdný
ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
<g/>
"	"	kIx"	"
záření	záření	k1gNnSc1	záření
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglické	anglický	k2eAgFnSc6d1	anglická
literatuře	literatura	k1gFnSc6	literatura
vacuum	vacuum	k1gInSc4	vacuum
ultraviolet	ultraviolet	k5eAaImF	ultraviolet
<g/>
,	,	kIx,	,
VUV	VUV	kA	VUV
<g/>
)	)	kIx)	)
naráží	narážet	k5eAaImIp3nS	narážet
na	na	k7c4	na
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
záření	záření	k1gNnSc2	záření
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
dopadu	dopad	k1gInSc6	dopad
na	na	k7c4	na
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
pohlcován	pohlcován	k2eAgInSc4d1	pohlcován
vzduchem	vzduch	k1gInSc7	vzduch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
hluboké	hluboký	k2eAgNnSc1d1	hluboké
ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
<g/>
"	"	kIx"	"
záření	záření	k1gNnSc1	záření
(	(	kIx(	(
<g/>
deep	deep	k1gInSc1	deep
ultraviolet	ultraviolet	k1gInSc1	ultraviolet
<g/>
,	,	kIx,	,
DUV	DUV	kA	DUV
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
používáno	používat	k5eAaImNgNnS	používat
ve	v	k7c6	v
fotolitografii	fotolitografie	k1gFnSc6	fotolitografie
a	a	k8xC	a
technologiích	technologie	k1gFnPc6	technologie
používající	používající	k2eAgInPc4d1	používající
principu	princip	k1gInSc6	princip
laseru	laser	k1gInSc6	laser
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
UVA	UVA	kA	UVA
===	===	k?	===
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
délku	délka	k1gFnSc4	délka
od	od	k7c2	od
315	[number]	k4	315
do	do	k7c2	do
400	[number]	k4	400
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
99	[number]	k4	99
%	%	kIx~	%
UV	UV	kA	UV
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
dopadne	dopadnout	k5eAaPmIp3nS	dopadnout
na	na	k7c4	na
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
spektrální	spektrální	k2eAgFnSc2d1	spektrální
oblasti	oblast	k1gFnSc2	oblast
UVA	UVA	kA	UVA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
UVB	UVB	kA	UVB
===	===	k?	===
</s>
</p>
<p>
<s>
Záření	záření	k1gNnSc1	záření
UVB	UVB	kA	UVB
má	mít	k5eAaImIp3nS	mít
vlnovou	vlnový	k2eAgFnSc4d1	vlnová
délku	délka	k1gFnSc4	délka
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
od	od	k7c2	od
280	[number]	k4	280
do	do	k7c2	do
315	[number]	k4	315
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
z	z	k7c2	z
převážné	převážný	k2eAgFnSc2d1	převážná
většiny	většina	k1gFnSc2	většina
absorbováno	absorbovat	k5eAaBmNgNnS	absorbovat
ozónem	ozón	k1gInSc7	ozón
ve	v	k7c6	v
stratosféře	stratosféra	k1gFnSc6	stratosféra
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
ozónové	ozónový	k2eAgFnSc6d1	ozónová
vrstvě	vrstva	k1gFnSc6	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
typického	typický	k2eAgNnSc2d1	typické
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
350	[number]	k4	350
<g/>
–	–	k?	–
<g/>
900	[number]	k4	900
W	W	kA	W
<g/>
/	/	kIx~	/
<g/>
m2	m2	k4	m2
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
dopadá	dopadat	k5eAaImIp3nS	dopadat
na	na	k7c4	na
nejvyšší	vysoký	k2eAgFnPc4d3	nejvyšší
vrstvy	vrstva	k1gFnPc4	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
neproniká	pronikat	k5eNaImIp3nS	pronikat
prakticky	prakticky	k6eAd1	prakticky
žádné	žádný	k3yNgFnPc4	žádný
UV	UV	kA	UV
záření	záření	k1gNnSc1	záření
s	s	k7c7	s
vlnovou	vlnový	k2eAgFnSc7d1	vlnová
délkou	délka	k1gFnSc7	délka
pod	pod	k7c7	pod
cca	cca	kA	cca
295	[number]	k4	295
nm	nm	k?	nm
<g/>
;	;	kIx,	;
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
hranice	hranice	k1gFnSc2	hranice
se	se	k3xPyFc4	se
na	na	k7c4	na
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
dostává	dostávat	k5eAaImIp3nS	dostávat
měkčí	měkký	k2eAgInSc1d2	měkčí
UV	UV	kA	UV
záření	záření	k1gNnSc1	záření
–	–	k?	–
záření	záření	k1gNnSc1	záření
UVA	UVA	kA	UVA
o	o	k7c6	o
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
400	[number]	k4	400
nm	nm	k?	nm
se	se	k3xPyFc4	se
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
dostane	dostat	k5eAaPmIp3nS	dostat
550	[number]	k4	550
mW	mW	k?	mW
<g/>
/	/	kIx~	/
<g/>
m2	m2	k4	m2
(	(	kIx(	(
<g/>
z	z	k7c2	z
přibližně	přibližně	k6eAd1	přibližně
1700	[number]	k4	1700
mW	mW	k?	mW
<g/>
/	/	kIx~	/
<g/>
m2	m2	k4	m2
z	z	k7c2	z
horních	horní	k2eAgFnPc2d1	horní
vrstev	vrstva	k1gFnPc2	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
ozón	ozón	k1gInSc1	ozón
a	a	k8xC	a
kyslík	kyslík	k1gInSc1	kyslík
propustí	propustit	k5eAaPmIp3nS	propustit
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
Země	zem	k1gFnSc2	zem
zhruba	zhruba	k6eAd1	zhruba
třetinu	třetina	k1gFnSc4	třetina
UV	UV	kA	UV
záření	záření	k1gNnSc1	záření
<g/>
.	.	kIx.	.
<g/>
Záření	záření	k1gNnSc1	záření
UVB	UVB	kA	UVB
je	být	k5eAaImIp3nS	být
zhoubné	zhoubný	k2eAgNnSc1d1	zhoubné
pro	pro	k7c4	pro
živé	živý	k2eAgInPc4d1	živý
organismy	organismus	k1gInPc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
schopná	schopný	k2eAgFnSc1d1	schopná
rozkládat	rozkládat	k5eAaImF	rozkládat
nebo	nebo	k8xC	nebo
narušovat	narušovat	k5eAaImF	narušovat
bílkoviny	bílkovina	k1gFnPc4	bílkovina
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnPc4d1	jiná
životně	životně	k6eAd1	životně
důležité	důležitý	k2eAgFnPc1d1	důležitá
organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
s	s	k7c7	s
vážnými	vážný	k2eAgInPc7d1	vážný
následky	následek	k1gInPc7	následek
pro	pro	k7c4	pro
metabolismus	metabolismus	k1gInSc4	metabolismus
postiženého	postižený	k2eAgMnSc2d1	postižený
jedince	jedinec	k1gMnSc2	jedinec
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
zasažena	zasažen	k2eAgFnSc1d1	zasažena
DNA	dna	k1gFnSc1	dna
<g/>
)	)	kIx)	)
vedoucí	vedoucí	k1gFnSc1	vedoucí
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
rakoviny	rakovina	k1gFnSc2	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
zvýšení	zvýšení	k1gNnSc4	zvýšení
intenzity	intenzita	k1gFnSc2	intenzita
UVB	UVB	kA	UVB
záření	záření	k1gNnSc2	záření
o	o	k7c4	o
každá	každý	k3xTgFnSc1	každý
2	[number]	k4	2
%	%	kIx~	%
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
zvýšení	zvýšení	k1gNnSc1	zvýšení
výskytu	výskyt	k1gInSc2	výskyt
rakoviny	rakovina	k1gFnSc2	rakovina
kůže	kůže	k1gFnSc2	kůže
o	o	k7c4	o
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
kůže	kůže	k1gFnSc2	kůže
má	mít	k5eAaImIp3nS	mít
UVB	UVB	kA	UVB
největší	veliký	k2eAgInSc4d3	veliký
dopad	dopad	k1gInSc4	dopad
i	i	k9	i
na	na	k7c4	na
oči	oko	k1gNnPc4	oko
(	(	kIx(	(
<g/>
potažmo	potažmo	k6eAd1	potažmo
zrak	zrak	k1gInSc1	zrak
<g/>
)	)	kIx)	)
–	–	k?	–
takto	takto	k6eAd1	takto
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
záření	záření	k1gNnSc1	záření
dokáže	dokázat	k5eAaPmIp3nS	dokázat
poničit	poničit	k5eAaPmF	poničit
až	až	k9	až
zcela	zcela	k6eAd1	zcela
spálit	spálit	k5eAaPmF	spálit
tyčinky	tyčinka	k1gFnPc4	tyčinka
a	a	k8xC	a
čípky	čípek	k1gInPc4	čípek
<g/>
,	,	kIx,	,
gangliové	gangliový	k2eAgFnPc4d1	gangliová
buňky	buňka	k1gFnPc4	buňka
a	a	k8xC	a
nervová	nervový	k2eAgNnPc1d1	nervové
zakončení	zakončení	k1gNnPc1	zakončení
v	v	k7c6	v
rohovce	rohovka	k1gFnSc6	rohovka
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
sněžná	sněžný	k2eAgFnSc1d1	sněžná
slepota	slepota	k1gFnSc1	slepota
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Větší	veliký	k2eAgInSc1d2	veliký
dopad	dopad	k1gInSc1	dopad
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
jednobuněčné	jednobuněčný	k2eAgInPc4d1	jednobuněčný
organismy	organismus	k1gInPc4	organismus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
zničit	zničit	k5eAaPmF	zničit
zcela	zcela	k6eAd1	zcela
(	(	kIx(	(
<g/>
dokáže	dokázat	k5eAaPmIp3nS	dokázat
změnit	změnit	k5eAaPmF	změnit
strukturu	struktura	k1gFnSc4	struktura
molekuly	molekula	k1gFnSc2	molekula
DNA	dno	k1gNnSc2	dno
nesoucí	nesoucí	k2eAgFnSc4d1	nesoucí
genetickou	genetický	k2eAgFnSc4d1	genetická
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
vyvolat	vyvolat	k5eAaPmF	vyvolat
poškození	poškození	k1gNnSc4	poškození
funkcí	funkce	k1gFnPc2	funkce
organel	organela	k1gFnPc2	organela
<g/>
,	,	kIx,	,
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
osmotický	osmotický	k2eAgInSc4d1	osmotický
tlak	tlak	k1gInSc4	tlak
nebo	nebo	k8xC	nebo
spustit	spustit	k5eAaPmF	spustit
lyzi	lyzi	k6eAd1	lyzi
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Proniká	pronikat	k5eAaImIp3nS	pronikat
i	i	k9	i
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
několika	několik	k4yIc2	několik
metrů	metr	k1gMnPc2	metr
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
však	však	k9	však
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
většina	většina	k1gFnSc1	většina
podvodních	podvodní	k2eAgInPc2d1	podvodní
organismů	organismus	k1gInPc2	organismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
UVB	UVB	kA	UVB
záření	záření	k1gNnSc1	záření
též	též	k9	též
negativně	negativně	k6eAd1	negativně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
vzrůst	vzrůst	k1gInSc1	vzrůst
zelených	zelený	k2eAgFnPc2d1	zelená
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
účinnost	účinnost	k1gFnSc1	účinnost
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
třeba	třeba	k6eAd1	třeba
celkovou	celkový	k2eAgFnSc4d1	celková
plochu	plocha	k1gFnSc4	plocha
jejich	jejich	k3xOp3gInPc2	jejich
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dvou	dva	k4xCgFnPc2	dva
třetin	třetina	k1gFnPc2	třetina
hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
plodin	plodina	k1gFnPc2	plodina
byl	být	k5eAaImAgInS	být
zjištěn	zjištěn	k2eAgInSc1d1	zjištěn
úbytek	úbytek	k1gInSc1	úbytek
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
produkce	produkce	k1gFnSc2	produkce
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
zvýšeným	zvýšený	k2eAgNnSc7d1	zvýšené
působením	působení	k1gNnSc7	působení
UVB	UVB	kA	UVB
záření	záření	k1gNnSc2	záření
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
sóji	sója	k1gFnSc2	sója
každé	každý	k3xTgNnSc4	každý
jedno	jeden	k4xCgNnSc4	jeden
procento	procento	k1gNnSc4	procento
zvýšení	zvýšení	k1gNnSc2	zvýšení
UVB	UVB	kA	UVB
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
procentuálnímu	procentuální	k2eAgInSc3d1	procentuální
úbytku	úbytek	k1gInSc3	úbytek
úrody	úroda	k1gFnSc2	úroda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
zvýšené	zvýšený	k2eAgNnSc4d1	zvýšené
působení	působení	k1gNnSc4	působení
UVB	UVB	kA	UVB
záření	záření	k1gNnSc4	záření
by	by	kYmCp3nP	by
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c4	v
nepředvídatelné	předvídatelný	k2eNgFnPc4d1	nepředvídatelná
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
morfologii	morfologie	k1gFnSc6	morfologie
biosféry	biosféra	k1gFnSc2	biosféra
(	(	kIx(	(
<g/>
každý	každý	k3xTgMnSc1	každý
živočišný	živočišný	k2eAgMnSc1d1	živočišný
či	či	k8xC	či
rostlinný	rostlinný	k2eAgInSc1d1	rostlinný
druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
UV	UV	kA	UV
záření	záření	k1gNnSc1	záření
různě	různě	k6eAd1	různě
citlivý	citlivý	k2eAgMnSc1d1	citlivý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trend	trend	k1gInSc1	trend
směřující	směřující	k2eAgInSc1d1	směřující
k	k	k7c3	k
dominanci	dominance	k1gFnSc3	dominance
odolnějších	odolný	k2eAgInPc2d2	odolnější
druhů	druh	k1gInPc2	druh
nad	nad	k7c7	nad
méně	málo	k6eAd2	málo
odolnějšími	odolný	k2eAgInPc7d2	odolnější
by	by	kYmCp3nP	by
odstartoval	odstartovat	k5eAaPmAgInS	odstartovat
nesmírně	smírně	k6eNd1	smírně
složitou	složitý	k2eAgFnSc4d1	složitá
síť	síť	k1gFnSc4	síť
kauzuálních	kauzuální	k2eAgInPc2d1	kauzuální
mezidruhových	mezidruhový	k2eAgInPc2d1	mezidruhový
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
důsledky	důsledek	k1gInPc4	důsledek
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
odhadnout	odhadnout	k5eAaPmF	odhadnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
UVC	UVC	kA	UVC
===	===	k?	===
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
nejtvrdší	tvrdý	k2eAgMnSc1d3	nejtvrdší
UV	UV	kA	UV
záření	záření	k1gNnSc3	záření
–	–	k?	–
jeho	jeho	k3xOp3gFnSc1	jeho
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
280	[number]	k4	280
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
záření	záření	k1gNnSc1	záření
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
způsobů	způsob	k1gInPc2	způsob
vzniku	vznik	k1gInSc2	vznik
ozónu	ozón	k1gInSc2	ozón
–	–	k?	–
při	při	k7c6	při
dopadu	dopad	k1gInSc6	dopad
na	na	k7c4	na
dvojatomární	dvojatomární	k2eAgFnSc4d1	dvojatomární
molekulu	molekula	k1gFnSc4	molekula
kyslíku	kyslík	k1gInSc2	kyslík
jí	jíst	k5eAaImIp3nS	jíst
toto	tento	k3xDgNnSc1	tento
záření	záření	k1gNnSc1	záření
dodá	dodat	k5eAaPmIp3nS	dodat
energii	energie	k1gFnSc4	energie
pro	pro	k7c4	pro
vznik	vznik	k1gInSc4	vznik
ozónu	ozón	k1gInSc2	ozón
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
touto	tento	k3xDgFnSc7	tento
reakcí	reakce	k1gFnSc7	reakce
absorbován	absorbovat	k5eAaBmNgMnS	absorbovat
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
plynný	plynný	k2eAgInSc1d1	plynný
kyslík	kyslík	k1gInSc1	kyslík
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc4d1	významný
inhibitor	inhibitor	k1gInSc4	inhibitor
dopadu	dopad	k1gInSc2	dopad
UVC	UVC	kA	UVC
záření	záření	k1gNnSc2	záření
na	na	k7c4	na
zemský	zemský	k2eAgInSc4d1	zemský
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Záření	záření	k1gNnSc1	záření
UVC	UVC	kA	UVC
je	být	k5eAaImIp3nS	být
prokazatelně	prokazatelně	k6eAd1	prokazatelně
zhoubné	zhoubný	k2eAgInPc1d1	zhoubný
(	(	kIx(	(
<g/>
karcinogenní	karcinogenní	k2eAgInPc1d1	karcinogenní
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
živé	živý	k2eAgInPc4d1	živý
organismy	organismus	k1gInPc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
UVB	UVB	kA	UVB
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
proniknout	proniknout	k5eAaPmF	proniknout
jen	jen	k9	jen
několika	několik	k4yIc7	několik
vrstvami	vrstva	k1gFnPc7	vrstva
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
penetrace	penetrace	k1gFnSc1	penetrace
UVC	UVC	kA	UVC
pletivy	pletivo	k1gNnPc7	pletivo
a	a	k8xC	a
tkáněmi	tkáň	k1gFnPc7	tkáň
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
poměrně	poměrně	k6eAd1	poměrně
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
UV	UV	kA	UV
záření	záření	k1gNnSc1	záření
již	již	k9	již
začíná	začínat	k5eAaImIp3nS	začínat
být	být	k5eAaImF	být
ionizující	ionizující	k2eAgMnSc1d1	ionizující
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
VUV	VUV	kA	VUV
===	===	k?	===
</s>
</p>
<p>
<s>
Vacuum	Vacuum	k1gInSc4	Vacuum
UV	UV	kA	UV
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
200	[number]	k4	200
nm	nm	k?	nm
oficiálně	oficiálně	k6eAd1	oficiálně
patří	patřit	k5eAaImIp3nS	patřit
pod	pod	k7c4	pod
rozdělení	rozdělení	k1gNnSc4	rozdělení
UVC	UVC	kA	UVC
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
štěpí	štěpit	k5eAaImIp3nS	štěpit
kyslík	kyslík	k1gInSc4	kyslík
na	na	k7c4	na
ozon	ozon	k1gInSc4	ozon
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
udávaný	udávaný	k2eAgInSc1d1	udávaný
rozsah	rozsah	k1gInSc1	rozsah
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
200	[number]	k4	200
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
EUV	EUV	kA	EUV
===	===	k?	===
</s>
</p>
<p>
<s>
Extrémní	extrémní	k2eAgNnSc1d1	extrémní
ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
záření	záření	k1gNnSc1	záření
s	s	k7c7	s
vlnovými	vlnový	k2eAgFnPc7d1	vlnová
délkami	délka	k1gFnPc7	délka
nižšími	nízký	k2eAgFnPc7d2	nižší
než	než	k8xS	než
31	[number]	k4	31
nm	nm	k?	nm
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
některých	některý	k3yIgInPc6	některý
chemických	chemický	k2eAgInPc6d1	chemický
procesech	proces	k1gInPc6	proces
ionosféry	ionosféra	k1gFnSc2	ionosféra
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
její	její	k3xOp3gFnPc1	její
nejsvrchnější	svrchní	k2eAgFnPc1d3	nejsvrchnější
vrstvy	vrstva	k1gFnPc1	vrstva
(	(	kIx(	(
<g/>
vrstvy	vrstva	k1gFnPc1	vrstva
F	F	kA	F
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
svítidla	svítidlo	k1gNnPc4	svítidlo
na	na	k7c4	na
kontrolu	kontrola	k1gFnSc4	kontrola
např.	např.	kA	např.
cenných	cenný	k2eAgInPc2d1	cenný
papírů	papír	k1gInPc2	papír
<g/>
,	,	kIx,	,
kreditních	kreditní	k2eAgFnPc2d1	kreditní
karet	kareta	k1gFnPc2	kareta
<g/>
,	,	kIx,	,
některých	některý	k3yIgInPc2	některý
dokladů	doklad	k1gInPc2	doklad
</s>
</p>
<p>
<s>
výbojkové	výbojkový	k2eAgFnPc1d1	výbojková
obloukové	obloukový	k2eAgFnPc1d1	oblouková
lampy	lampa	k1gFnPc1	lampa
(	(	kIx(	(
<g/>
xenon	xenon	k1gInSc1	xenon
<g/>
,	,	kIx,	,
měď	měď	k1gFnSc1	měď
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
astronomie	astronomie	k1gFnSc1	astronomie
(	(	kIx(	(
<g/>
Wienův	Wienův	k2eAgInSc1d1	Wienův
zákon	zákon	k1gInSc1	zákon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
dezinfekce	dezinfekce	k1gFnSc1	dezinfekce
<g/>
,	,	kIx,	,
dezinsekce	dezinsekce	k1gFnSc1	dezinsekce
</s>
</p>
<p>
<s>
spektrofotometrie	spektrofotometrie	k1gFnSc1	spektrofotometrie
</s>
</p>
<p>
<s>
analýza	analýza	k1gFnSc1	analýza
minerálů	minerál	k1gInPc2	minerál
</s>
</p>
<p>
<s>
spektroskopie	spektroskopie	k1gFnSc1	spektroskopie
</s>
</p>
<p>
<s>
genetika	genetika	k1gFnSc1	genetika
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
markery	marker	k1gInPc4	marker
-	-	kIx~	-
"	"	kIx"	"
<g/>
značkovací	značkovací	k2eAgFnPc1d1	značkovací
látky	látka	k1gFnPc1	látka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
chemické	chemický	k2eAgInPc1d1	chemický
markery	marker	k1gInPc1	marker
</s>
</p>
<p>
<s>
biochemie	biochemie	k1gFnSc1	biochemie
</s>
</p>
<p>
<s>
fotochemoterapie	fotochemoterapie	k1gFnSc1	fotochemoterapie
</s>
</p>
<p>
<s>
fototerapie	fototerapie	k1gFnSc1	fototerapie
</s>
</p>
<p>
<s>
fotolitografie	fotolitografie	k1gFnSc1	fotolitografie
</s>
</p>
<p>
<s>
laserová	laserový	k2eAgFnSc1d1	laserová
technologie	technologie	k1gFnSc1	technologie
</s>
</p>
<p>
<s>
kontrola	kontrola	k1gFnSc1	kontrola
elektrického	elektrický	k2eAgInSc2d1	elektrický
průboje	průboj	k1gInSc2	průboj
</s>
</p>
<p>
<s>
sterilizace	sterilizace	k1gFnSc1	sterilizace
(	(	kIx(	(
<g/>
v	v	k7c6	v
biologických	biologický	k2eAgFnPc6d1	biologická
laboratořích	laboratoř	k1gFnPc6	laboratoř
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
čištění	čištění	k1gNnSc1	čištění
vody	voda	k1gFnSc2	voda
</s>
</p>
<p>
<s>
zpracování	zpracování	k1gNnSc1	zpracování
jídla	jídlo	k1gNnSc2	jídlo
</s>
</p>
<p>
<s>
detektory	detektor	k1gInPc1	detektor
požáru	požár	k1gInSc2	požár
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
neviditelný	viditelný	k2eNgInSc1d1	neviditelný
inkoust	inkoust	k1gInSc1	inkoust
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
opalování	opalování	k1gNnSc1	opalování
</s>
</p>
<p>
<s>
vymazávání	vymazávání	k1gNnSc1	vymazávání
paměťových	paměťový	k2eAgInPc2d1	paměťový
modulů	modul	k1gInPc2	modul
EPROM	EPROM	kA	EPROM
</s>
</p>
<p>
<s>
příprava	příprava	k1gFnSc1	příprava
polymerů	polymer	k1gInPc2	polymer
s	s	k7c7	s
nízkou	nízký	k2eAgFnSc7d1	nízká
povrchovou	povrchový	k2eAgFnSc7d1	povrchová
energií	energie	k1gFnSc7	energie
pro	pro	k7c4	pro
lepidla	lepidlo	k1gNnPc4	lepidlo
a	a	k8xC	a
laky	laka	k1gFnPc4	laka
</s>
</p>
<p>
<s>
stabilizátory	stabilizátor	k1gInPc1	stabilizátor
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
plastů	plast	k1gInPc2	plast
<g/>
,	,	kIx,	,
kosmetiky	kosmetika	k1gFnSc2	kosmetika
a	a	k8xC	a
filmů	film	k1gInPc2	film
</s>
</p>
<p>
<s>
archeologie	archeologie	k1gFnSc1	archeologie
(	(	kIx(	(
<g/>
čtení	čtení	k1gNnSc1	čtení
poškozených	poškozený	k2eAgInPc2d1	poškozený
papyrů	papyr	k1gInPc2	papyr
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
soudní	soudní	k2eAgNnSc1d1	soudní
znalectví	znalectví	k1gNnSc1	znalectví
</s>
</p>
<p>
<s>
odborné	odborný	k2eAgInPc1d1	odborný
posudky	posudek	k1gInPc1	posudek
obrazů	obraz	k1gInPc2	obraz
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Ultraviolet	Ultraviolet	k1gInSc1	Ultraviolet
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Meadowsová	Meadowsová	k1gFnSc1	Meadowsová
<g/>
,	,	kIx,	,
D.H.	D.H.	k1gFnSc1	D.H.
<g/>
,	,	kIx,	,
Randers	Randers	k1gInSc1	Randers
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Překročení	překročení	k1gNnSc1	překročení
mezí	mez	k1gFnPc2	mez
<g/>
,	,	kIx,	,
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
</s>
</p>
<p>
<s>
Pekárek	Pekárek	k1gMnSc1	Pekárek
<g/>
,	,	kIx,	,
L.	L.	kA	L.
<g/>
,	,	kIx,	,
Šístek	Šístek	k?	Šístek
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
,	,	kIx,	,
Jelínek	Jelínek	k1gMnSc1	Jelínek
<g/>
,	,	kIx,	,
L.	L.	kA	L.
<g/>
:	:	kIx,	:
Neionizující	ionizující	k2eNgNnSc1d1	neionizující
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
expozice	expozice	k1gFnSc1	expozice
a	a	k8xC	a
zdravotní	zdravotní	k2eAgNnPc1d1	zdravotní
rizika	riziko	k1gNnPc1	riziko
,	,	kIx,	,
<g/>
SZÚ	SZÚ	kA	SZÚ
2006	[number]	k4	2006
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Fotodermatitida	Fotodermatitida	k1gFnSc1	Fotodermatitida
</s>
</p>
<p>
<s>
UV	UV	kA	UV
index	index	k1gInSc1	index
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
ultrafialové	ultrafialový	k2eAgFnSc2d1	ultrafialová
záření	záření	k1gNnSc4	záření
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
