<s>
Aristotelés	Aristotelés	k6eAd1	Aristotelés
ze	z	k7c2	z
Stageiry	Stageira	k1gFnSc2	Stageira
byl	být	k5eAaImAgMnS	být
filosof	filosof	k1gMnSc1	filosof
vrcholného	vrcholný	k2eAgNnSc2d1	vrcholné
období	období	k1gNnSc2	období
řecké	řecký	k2eAgFnSc2d1	řecká
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
nejvýznamnější	významný	k2eAgMnSc1d3	nejvýznamnější
žák	žák	k1gMnSc1	žák
Platonův	Platonův	k2eAgMnSc1d1	Platonův
a	a	k8xC	a
vychovatel	vychovatel	k1gMnSc1	vychovatel
Alexandra	Alexandr	k1gMnSc2	Alexandr
Makedonského	makedonský	k2eAgMnSc2d1	makedonský
<g/>
.	.	kIx.	.
</s>
