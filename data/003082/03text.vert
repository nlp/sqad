<s>
Papoušci	Papoušek	k1gMnPc1	Papoušek
(	(	kIx(	(
<g/>
Psittaciformes	Psittaciformes	k1gInSc1	Psittaciformes
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
vývojově	vývojově	k6eAd1	vývojově
starý	starý	k2eAgInSc4d1	starý
řád	řád	k1gInSc4	řád
hlučných	hlučný	k2eAgMnPc2d1	hlučný
a	a	k8xC	a
pestrých	pestrý	k2eAgMnPc2d1	pestrý
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
domovem	domov	k1gInSc7	domov
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
tropické	tropický	k2eAgFnPc1d1	tropická
oblasti	oblast	k1gFnPc1	oblast
světa	svět	k1gInSc2	svět
-	-	kIx~	-
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
Tichomoří	Tichomoří	k1gNnSc2	Tichomoří
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
a	a	k8xC	a
Střední	střední	k2eAgFnSc1d1	střední
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
jejich	jejich	k3xOp3gNnSc2	jejich
rozšíření	rozšíření	k1gNnSc2	rozšíření
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
k	k	k7c3	k
Ohňové	ohňový	k2eAgFnSc3d1	ohňová
zemi	zem	k1gFnSc3	zem
v	v	k7c6	v
Patagonii	Patagonie	k1gFnSc6	Patagonie
a	a	k8xC	a
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Kdysi	kdysi	k6eAd1	kdysi
obývali	obývat	k5eAaImAgMnP	obývat
i	i	k9	i
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
ucelený	ucelený	k2eAgInSc1d1	ucelený
řád	řád	k1gInSc1	řád
ptáků	pták	k1gMnPc2	pták
zahrnující	zahrnující	k2eAgFnSc2d1	zahrnující
322	[number]	k4	322
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Papoušci	Papoušek	k1gMnPc1	Papoušek
jsou	být	k5eAaImIp3nP	být
druhově	druhově	k6eAd1	druhově
vyhraněnou	vyhraněný	k2eAgFnSc7d1	vyhraněná
skupinou	skupina	k1gFnSc7	skupina
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
charakteristických	charakteristický	k2eAgInPc2d1	charakteristický
znaků	znak	k1gInPc2	znak
<g/>
:	:	kIx,	:
Zobák	zobák	k1gInSc1	zobák
papoušků	papoušek	k1gMnPc2	papoušek
je	být	k5eAaImIp3nS	být
neobyčejně	obyčejně	k6eNd1	obyčejně
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
dokáží	dokázat	k5eAaPmIp3nP	dokázat
jím	on	k3xPp3gNnSc7	on
překousnout	překousnout	k5eAaPmF	překousnout
i	i	k9	i
dráty	drát	k1gInPc1	drát
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
vysoký	vysoký	k2eAgInSc4d1	vysoký
hákovitě	hákovitě	k6eAd1	hákovitě
zahnutý	zahnutý	k2eAgInSc4d1	zahnutý
zobák	zobák	k1gInSc4	zobák
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
horní	horní	k2eAgFnSc1d1	horní
čelist	čelist	k1gFnSc1	čelist
výrazně	výrazně	k6eAd1	výrazně
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
spodní	spodní	k2eAgNnSc1d1	spodní
<g/>
.	.	kIx.	.
</s>
<s>
Horní	horní	k2eAgFnSc1d1	horní
čelist	čelist	k1gFnSc1	čelist
není	být	k5eNaImIp3nS	být
pevně	pevně	k6eAd1	pevně
srostlá	srostlý	k2eAgFnSc1d1	srostlá
s	s	k7c7	s
lebkou	lebka	k1gFnSc7	lebka
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
pohybovat	pohybovat	k5eAaImF	pohybovat
nahoru	nahoru	k6eAd1	nahoru
a	a	k8xC	a
dolů	dolů	k6eAd1	dolů
<g/>
,	,	kIx,	,
spodní	spodní	k2eAgFnSc1d1	spodní
čelist	čelist	k1gFnSc1	čelist
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
i	i	k9	i
do	do	k7c2	do
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
papoušků	papoušek	k1gMnPc2	papoušek
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pohyblivý	pohyblivý	k2eAgMnSc1d1	pohyblivý
<g/>
,	,	kIx,	,
masivní	masivní	k2eAgMnSc1d1	masivní
<g/>
,	,	kIx,	,
svalnatý	svalnatý	k2eAgMnSc1d1	svalnatý
a	a	k8xC	a
s	s	k7c7	s
chuťovými	chuťový	k2eAgFnPc7d1	chuťová
buňkami	buňka	k1gFnPc7	buňka
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
špičce	špička	k1gFnSc6	špička
dokonce	dokonce	k9	dokonce
jamku	jamka	k1gFnSc4	jamka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
lžička	lžička	k1gFnSc1	lžička
např.	např.	kA	např.
k	k	k7c3	k
vybíraní	vybíraný	k2eAgMnPc1d1	vybíraný
semen	semeno	k1gNnPc2	semeno
z	z	k7c2	z
lahvovitých	lahvovitý	k2eAgInPc2d1	lahvovitý
plodů	plod	k1gInPc2	plod
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jakýsi	jakýsi	k3yIgInSc1	jakýsi
kartáček	kartáček	k1gInSc1	kartáček
ke	k	k7c3	k
sbírání	sbírání	k1gNnSc3	sbírání
nektaru	nektar	k1gInSc2	nektar
z	z	k7c2	z
květů	květ	k1gInPc2	květ
(	(	kIx(	(
<g/>
loriové	lori	k1gMnPc1	lori
<g/>
)	)	kIx)	)
Zobák	zobák	k1gInSc1	zobák
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jazykem	jazyk	k1gInSc7	jazyk
všestranně	všestranně	k6eAd1	všestranně
důmyslným	důmyslný	k2eAgInSc7d1	důmyslný
nástrojem	nástroj	k1gInSc7	nástroj
<g/>
,	,	kIx,	,
uzpůsobeným	uzpůsobený	k2eAgInSc7d1	uzpůsobený
k	k	k7c3	k
louskání	louskání	k1gNnSc3	louskání
semen	semeno	k1gNnPc2	semeno
a	a	k8xC	a
pojídání	pojídání	k1gNnSc2	pojídání
plodů	plod	k1gInPc2	plod
a	a	k8xC	a
k	k	k7c3	k
přidržování	přidržování	k1gNnSc3	přidržování
na	na	k7c6	na
větvích	větev	k1gFnPc6	větev
<g/>
.	.	kIx.	.
</s>
<s>
Noha	noha	k1gFnSc1	noha
papoušků	papoušek	k1gMnPc2	papoušek
je	být	k5eAaImIp3nS	být
samostatným	samostatný	k2eAgInSc7d1	samostatný
nástrojem	nástroj	k1gInSc7	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Běhák	běhák	k1gInSc1	běhák
je	být	k5eAaImIp3nS	být
krátký	krátký	k2eAgInSc1d1	krátký
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
otáčivý	otáčivý	k2eAgMnSc1d1	otáčivý
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
prsty	prst	k1gInPc4	prst
směřují	směřovat	k5eAaImIp3nP	směřovat
do	do	k7c2	do
předu	příst	k5eAaImIp1nS	příst
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
dozadu	dozadu	k6eAd1	dozadu
(	(	kIx(	(
<g/>
zygodaktylní	zygodaktylní	k2eAgFnSc1d1	zygodaktylní
noha	noha	k1gFnSc1	noha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
noha	noha	k1gFnSc1	noha
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
dokonale	dokonale	k6eAd1	dokonale
stavěna	stavěn	k2eAgFnSc1d1	stavěna
ke	k	k7c3	k
šplhání	šplhání	k1gNnSc3	šplhání
po	po	k7c6	po
větvích	větev	k1gFnPc6	větev
i	i	k8xC	i
k	k	k7c3	k
uchopení	uchopení	k1gNnSc3	uchopení
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
jejímu	její	k3xOp3gNnSc3	její
podání	podání	k1gNnSc1	podání
do	do	k7c2	do
zobáku	zobák	k1gInSc2	zobák
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
v	v	k7c6	v
ptačí	ptačí	k2eAgFnSc6d1	ptačí
říši	říš	k1gFnSc6	říš
ojedinělé	ojedinělý	k2eAgInPc1d1	ojedinělý
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
velmi	velmi	k6eAd1	velmi
pestré	pestrý	k2eAgNnSc1d1	pestré
opeření	opeření	k1gNnSc1	opeření
je	být	k5eAaImIp3nS	být
udržováno	udržovat	k5eAaImNgNnS	udržovat
v	v	k7c6	v
dobrém	dobrý	k2eAgInSc6d1	dobrý
stavu	stav	k1gInSc6	stav
prachovým	prachový	k2eAgInSc7d1	prachový
pudrem	pudr	k1gInSc7	pudr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
v	v	k7c6	v
pudrotvorném	pudrotvorný	k2eAgNnSc6d1	pudrotvorný
peří	peří	k1gNnSc6	peří
<g/>
.	.	kIx.	.
</s>
<s>
Papoušci	Papoušek	k1gMnPc1	Papoušek
mají	mít	k5eAaImIp3nP	mít
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
slabě	slabě	k6eAd1	slabě
vyvinutou	vyvinutý	k2eAgFnSc4d1	vyvinutá
kostrční	kostrční	k2eAgFnSc4d1	kostrční
žlázu	žláza	k1gFnSc4	žláza
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k8xS	tak
si	se	k3xPyFc3	se
peří	peří	k1gNnSc1	peří
nemastí	mastit	k5eNaImIp3nS	mastit
tukem	tuk	k1gInSc7	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
dlouhověcí	dlouhověký	k2eAgMnPc1d1	dlouhověký
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
větší	veliký	k2eAgMnPc1d2	veliký
papoušci	papoušek	k1gMnPc1	papoušek
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
dožít	dožít	k5eAaPmF	dožít
velmi	velmi	k6eAd1	velmi
vysokého	vysoký	k2eAgInSc2d1	vysoký
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Andulka	Andulka	k1gFnSc1	Andulka
žije	žít	k5eAaImIp3nS	žít
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
korela	korela	k1gFnSc1	korela
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
amazoňan	amazoňan	k1gInSc1	amazoňan
portorický	portorický	k2eAgInSc1d1	portorický
82	[number]	k4	82
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kakadu	kakadu	k1gMnSc1	kakadu
žlutočečelatý	žlutočečelatý	k2eAgMnSc1d1	žlutočečelatý
až	až	k6eAd1	až
119	[number]	k4	119
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
samozřejmě	samozřejmě	k6eAd1	samozřejmě
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
chovatelské	chovatelský	k2eAgFnSc2d1	chovatelská
péče	péče	k1gFnSc2	péče
<g/>
.	.	kIx.	.
</s>
<s>
Potrava	potrava	k1gFnSc1	potrava
je	být	k5eAaImIp3nS	být
vesměs	vesměs	k6eAd1	vesměs
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
sbírají	sbírat	k5eAaImIp3nP	sbírat
také	také	k9	také
hmyz	hmyz	k1gInSc4	hmyz
a	a	k8xC	a
žížaly	žížala	k1gFnPc4	žížala
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
období	období	k1gNnSc6	období
krmení	krmení	k1gNnPc2	krmení
mláďat	mládě	k1gNnPc2	mládě
<g/>
.	.	kIx.	.
</s>
<s>
Páry	pár	k1gInPc1	pár
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
celoživotní	celoživotní	k2eAgNnSc1d1	celoživotní
<g/>
,	,	kIx,	,
partneři	partner	k1gMnPc1	partner
k	k	k7c3	k
sobě	se	k3xPyFc3	se
vysloveně	vysloveně	k6eAd1	vysloveně
lnou	lnout	k5eAaImIp3nP	lnout
a	a	k8xC	a
s	s	k7c7	s
projevy	projev	k1gInPc7	projev
náklonnosti	náklonnost	k1gFnSc2	náklonnost
lze	lze	k6eAd1	lze
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
pozorovat	pozorovat	k5eAaImF	pozorovat
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
si	se	k3xPyFc3	se
vzájemně	vzájemně	k6eAd1	vzájemně
probírají	probírat	k5eAaImIp3nP	probírat
peří	peří	k1gNnSc4	peří
<g/>
,	,	kIx,	,
dotýkají	dotýkat	k5eAaImIp3nP	dotýkat
se	se	k3xPyFc4	se
zobáky	zobák	k1gInPc1	zobák
a	a	k8xC	a
krmí	krmit	k5eAaImIp3nP	krmit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
dobu	doba	k1gFnSc4	doba
hnízdění	hnízdění	k1gNnSc2	hnízdění
žijí	žít	k5eAaImIp3nP	žít
sociálně	sociálně	k6eAd1	sociálně
a	a	k8xC	a
sdružují	sdružovat	k5eAaImIp3nP	sdružovat
se	se	k3xPyFc4	se
do	do	k7c2	do
obrovských	obrovský	k2eAgNnPc2d1	obrovské
hejn	hejno	k1gNnPc2	hejno
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
všichni	všechen	k3xTgMnPc1	všechen
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
snášejí	snášet	k5eAaImIp3nP	snášet
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
(	(	kIx(	(
<g/>
malé	malý	k2eAgInPc1d1	malý
druhy	druh	k1gInPc1	druh
až	až	k9	až
8	[number]	k4	8
<g/>
)	)	kIx)	)
bílých	bílý	k2eAgNnPc2d1	bílé
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
<s>
Krmivá	krmivý	k2eAgNnPc1d1	krmivý
mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
holá	holý	k2eAgFnSc1d1	holá
a	a	k8xC	a
slepá	slepý	k2eAgFnSc1d1	slepá
<g/>
,	,	kIx,	,
rodiče	rodič	k1gMnPc1	rodič
je	on	k3xPp3gFnPc4	on
krmí	krmit	k5eAaImIp3nP	krmit
kaší	kaše	k1gFnSc7	kaše
z	z	k7c2	z
volete	vole	k1gNnSc2	vole
až	až	k9	až
do	do	k7c2	do
vylétnutí	vylétnutí	k1gNnSc2	vylétnutí
z	z	k7c2	z
hnízda	hnízdo	k1gNnSc2	hnízdo
a	a	k8xC	a
i	i	k9	i
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
po	po	k7c6	po
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k8xS	co
mláďata	mládě	k1gNnPc1	mládě
hnízdo	hnízdo	k1gNnSc1	hnízdo
opustí	opustit	k5eAaPmIp3nP	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Papoušci	Papoušek	k1gMnPc1	Papoušek
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
vedle	vedle	k7c2	vedle
některých	některý	k3yIgFnPc2	některý
krkavcovitých	krkavcovitý	k2eAgFnPc2d1	krkavcovitý
<g/>
)	)	kIx)	)
nejinteligentnější	inteligentní	k2eAgMnPc1d3	nejinteligentnější
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
ptáků	pták	k1gMnPc2	pták
nejvíce	hodně	k6eAd3	hodně
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
koncový	koncový	k2eAgInSc4d1	koncový
mozek	mozek	k1gInSc4	mozek
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Mají	mít	k5eAaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
věrně	věrně	k6eAd1	věrně
napodobovat	napodobovat	k5eAaImF	napodobovat
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
zvuky	zvuk	k1gInPc4	zvuk
včetně	včetně	k7c2	včetně
lidských	lidský	k2eAgNnPc2d1	lidské
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
nemluví	mluvit	k5eNaImIp3nS	mluvit
však	však	k9	však
s	s	k7c7	s
citovým	citový	k2eAgNnSc7d1	citové
zabarvením	zabarvení	k1gNnSc7	zabarvení
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
papoušci	papoušek	k1gMnPc1	papoušek
dokážou	dokázat	k5eAaPmIp3nP	dokázat
zapískat	zapískat	k5eAaPmF	zapískat
i	i	k9	i
celé	celý	k2eAgFnPc4d1	celá
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
jiní	jiný	k1gMnPc1	jiný
napodobují	napodobovat	k5eAaImIp3nP	napodobovat
hlas	hlas	k1gInSc4	hlas
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
pláčou	plakat	k5eAaImIp3nP	plakat
<g/>
,	,	kIx,	,
smějí	smát	k5eAaImIp3nP	smát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
kašlou	kašlat	k5eAaImIp3nP	kašlat
apod.	apod.	kA	apod.
Zvláště	zvláště	k6eAd1	zvláště
nadaní	nadaný	k2eAgMnPc1d1	nadaný
papoušci	papoušek	k1gMnPc1	papoušek
znají	znát	k5eAaImIp3nP	znát
až	až	k9	až
200	[number]	k4	200
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Papoušek	Papoušek	k1gMnSc1	Papoušek
přitom	přitom	k6eAd1	přitom
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nerozlišuje	rozlišovat	k5eNaImIp3nS	rozlišovat
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
učit	učit	k5eAaImF	učit
a	a	k8xC	a
co	co	k3yQnSc4	co
pro	pro	k7c4	pro
něho	on	k3xPp3gInSc4	on
není	být	k5eNaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
nesmí	smět	k5eNaImIp3nS	smět
slyšet	slyšet	k5eAaImF	slyšet
ta	ten	k3xDgNnPc4	ten
slova	slovo	k1gNnPc4	slovo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
nechceme	chtít	k5eNaImIp1nP	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
opakoval	opakovat	k5eAaImAgMnS	opakovat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Občas	občas	k6eAd1	občas
komolí	komolit	k5eAaImIp3nS	komolit
dohromady	dohromady	k6eAd1	dohromady
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
slyší	slyšet	k5eAaImIp3nP	slyšet
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
schopnost	schopnost	k1gFnSc4	schopnost
smysluplné	smysluplný	k2eAgFnSc2d1	smysluplná
řeči	řeč	k1gFnSc2	řeč
(	(	kIx(	(
<g/>
Papoušek	Papoušek	k1gMnSc1	Papoušek
šedý	šedý	k2eAgMnSc1d1	šedý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Papoušci	Papoušek	k1gMnPc1	Papoušek
jsou	být	k5eAaImIp3nP	být
nejznámější	známý	k2eAgMnSc1d3	nejznámější
a	a	k8xC	a
nejoblíbenější	oblíbený	k2eAgFnSc7d3	nejoblíbenější
skupinou	skupina	k1gFnSc7	skupina
exotických	exotický	k2eAgMnPc2d1	exotický
ptáků	pták	k1gMnPc2	pták
chovaných	chovaný	k2eAgMnPc2d1	chovaný
pro	pro	k7c4	pro
okrasu	okrasa	k1gFnSc4	okrasa
a	a	k8xC	a
potěšení	potěšení	k1gNnSc4	potěšení
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
jednak	jednak	k8xC	jednak
jejich	jejich	k3xOp3gNnSc7	jejich
pestrým	pestrý	k2eAgNnSc7d1	pestré
zbarvením	zbarvení	k1gNnSc7	zbarvení
a	a	k8xC	a
přítulností	přítulnost	k1gFnSc7	přítulnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zvláštními	zvláštní	k2eAgInPc7d1	zvláštní
znaky	znak	k1gInPc7	znak
chování	chování	k1gNnSc2	chování
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
ucelený	ucelený	k2eAgInSc4d1	ucelený
řád	řád	k1gInSc4	řád
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc1	historie
chovu	chov	k1gInSc2	chov
papoušků	papoušek	k1gMnPc2	papoušek
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
do	do	k7c2	do
dob	doba	k1gFnPc2	doba
Alexandra	Alexandr	k1gMnSc2	Alexandr
Makedonského	makedonský	k2eAgMnSc2d1	makedonský
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
patrně	patrně	k6eAd1	patrně
dovezl	dovézt	k5eAaPmAgMnS	dovézt
ochočené	ochočený	k2eAgMnPc4d1	ochočený
papoušky	papoušek	k1gMnPc4	papoušek
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
tvoří	tvořit	k5eAaImIp3nS	tvořit
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
papoušky	papoušek	k1gMnPc7	papoušek
<g/>
,	,	kIx,	,
celosvětově	celosvětově	k6eAd1	celosvětově
největší	veliký	k2eAgInSc1d3	veliký
objem	objem	k1gInSc1	objem
obchodu	obchod	k1gInSc2	obchod
se	s	k7c7	s
zvířaty	zvíře	k1gNnPc7	zvíře
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
pro	pro	k7c4	pro
mnohé	mnohý	k2eAgMnPc4d1	mnohý
druhy	druh	k1gMnPc4	druh
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
vyhubení	vyhubení	k1gNnSc2	vyhubení
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
přísnou	přísný	k2eAgFnSc4d1	přísná
ochranu	ochrana	k1gFnSc4	ochrana
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ročně	ročně	k6eAd1	ročně
odchytí	odchytit	k5eAaPmIp3nS	odchytit
pro	pro	k7c4	pro
komerční	komerční	k2eAgInPc4d1	komerční
účely	účel	k1gInPc4	účel
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
miliónů	milión	k4xCgInPc2	milión
papoušků	papoušek	k1gMnPc2	papoušek
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
druhy	druh	k1gInPc1	druh
papoušků	papoušek	k1gMnPc2	papoušek
kromě	kromě	k7c2	kromě
andulky	andulka	k1gFnSc2	andulka
<g/>
,	,	kIx,	,
korely	korela	k1gFnSc2	korela
a	a	k8xC	a
alexandra	alexandra	k1gFnSc1	alexandra
malého	malý	k1gMnSc2	malý
jsou	být	k5eAaImIp3nP	být
zařazeni	zařadit	k5eAaPmNgMnP	zařadit
v	v	k7c6	v
příloze	příloha	k1gFnSc6	příloha
CITES	CITES	kA	CITES
1	[number]	k4	1
nebo	nebo	k8xC	nebo
CITES	CITES	kA	CITES
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc4	řád
papoušci	papoušek	k1gMnPc1	papoušek
(	(	kIx(	(
<g/>
Psittaciformes	Psittaciformes	k1gMnSc1	Psittaciformes
<g/>
)	)	kIx)	)
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
tři	tři	k4xCgFnPc4	tři
žijící	žijící	k2eAgFnPc4d1	žijící
čeledi	čeleď	k1gFnPc4	čeleď
<g/>
.	.	kIx.	.
</s>
<s>
Kakaduovití	Kakaduovití	k1gMnPc1	Kakaduovití
(	(	kIx(	(
<g/>
Cacatuidae	Cacatuidae	k1gInSc1	Cacatuidae
<g/>
)	)	kIx)	)
Papouškovití	papouškovitý	k2eAgMnPc1d1	papouškovitý
(	(	kIx(	(
<g/>
Psittacidae	Psittacidae	k1gNnSc7	Psittacidae
<g/>
)	)	kIx)	)
Loriovití	Loriovití	k1gMnPc5	Loriovití
(	(	kIx(	(
<g/>
Loriidae	Loriidae	k1gNnSc4	Loriidae
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Papoušci	Papoušek	k1gMnPc1	Papoušek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
papoušek	papoušek	k1gMnSc1	papoušek
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Téma	téma	k1gNnSc1	téma
Papoušek	Papoušek	k1gMnSc1	Papoušek
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
