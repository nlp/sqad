<s>
Papoušci	Papoušek	k1gMnPc1	Papoušek
jsou	být	k5eAaImIp3nP	být
(	(	kIx(	(
<g/>
vedle	vedle	k7c2	vedle
některých	některý	k3yIgFnPc2	některý
krkavcovitých	krkavcovitý	k2eAgFnPc2d1	krkavcovitý
<g/>
)	)	kIx)	)
nejinteligentnější	inteligentní	k2eAgMnPc1d3	nejinteligentnější
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
ze	z	k7c2	z
všech	všecek	k3xTgMnPc2	všecek
ptáků	pták	k1gMnPc2	pták
nejvíce	hodně	k6eAd3	hodně
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
koncový	koncový	k2eAgInSc4d1	koncový
mozek	mozek	k1gInSc4	mozek
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
