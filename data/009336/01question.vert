<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
vlna	vlna	k1gFnSc1	vlna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
jakoukoliv	jakýkoliv	k3yIgFnSc7	jakýkoliv
náhlou	náhlý	k2eAgFnSc7d1	náhlá
deformací	deformace	k1gFnSc7	deformace
horniny	hornina	k1gFnSc2	hornina
<g/>
?	?	kIx.	?
</s>
