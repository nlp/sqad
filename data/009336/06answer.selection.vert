<s>
Seismická	seismický	k2eAgFnSc1d1	seismická
vlna	vlna	k1gFnSc1	vlna
je	být	k5eAaImIp3nS	být
vlna	vlna	k1gFnSc1	vlna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
jakoukoliv	jakýkoliv	k3yIgFnSc7	jakýkoliv
náhlou	náhlý	k2eAgFnSc7d1	náhlá
deformací	deformace	k1gFnSc7	deformace
horniny	hornina	k1gFnSc2	hornina
<g/>
.	.	kIx.	.
</s>
