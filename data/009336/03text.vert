<p>
<s>
Seismická	seismický	k2eAgFnSc1d1	seismická
vlna	vlna	k1gFnSc1	vlna
je	být	k5eAaImIp3nS	být
vlna	vlna	k1gFnSc1	vlna
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
jakoukoliv	jakýkoliv	k3yIgFnSc7	jakýkoliv
náhlou	náhlý	k2eAgFnSc7d1	náhlá
deformací	deformace	k1gFnSc7	deformace
horniny	hornina	k1gFnSc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Deformace	deformace	k1gFnSc1	deformace
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
vyvolána	vyvolat	k5eAaPmNgFnS	vyvolat
zemětřesením	zemětřesení	k1gNnSc7	zemětřesení
<g/>
,	,	kIx,	,
zřícením	zřícení	k1gNnSc7	zřícení
skalního	skalní	k2eAgInSc2d1	skalní
masívu	masív	k1gInSc2	masív
<g/>
,	,	kIx,	,
umělým	umělý	k2eAgInSc7d1	umělý
výbuchem	výbuch	k1gInSc7	výbuch
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
praktické	praktický	k2eAgNnSc4d1	praktické
využití	využití	k1gNnSc4	využití
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
studia	studio	k1gNnSc2	studio
Země	zem	k1gFnSc2	zem
mají	mít	k5eAaImIp3nP	mít
však	však	k9	však
význam	význam	k1gInSc4	význam
jen	jen	k6eAd1	jen
ty	ten	k3xDgInPc1	ten
otřesy	otřes	k1gInPc1	otřes
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
projít	projít	k5eAaPmF	projít
celým	celý	k2eAgNnSc7d1	celé
zemským	zemský	k2eAgNnSc7d1	zemské
tělesem	těleso	k1gNnSc7	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
obrovské	obrovský	k2eAgFnPc1d1	obrovská
energie	energie	k1gFnPc1	energie
jsou	být	k5eAaImIp3nP	být
uvolňovány	uvolňovat	k5eAaImNgFnP	uvolňovat
pouze	pouze	k6eAd1	pouze
při	při	k7c6	při
větších	veliký	k2eAgNnPc6d2	veliký
zemětřeseních	zemětřesení	k1gNnPc6	zemětřesení
a	a	k8xC	a
nebo	nebo	k8xC	nebo
lidskou	lidský	k2eAgFnSc7d1	lidská
činností	činnost	k1gFnSc7	činnost
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
atomového	atomový	k2eAgInSc2d1	atomový
výbuchu	výbuch	k1gInSc2	výbuch
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
a	a	k8xC	a
nebo	nebo	k8xC	nebo
výbuchu	výbuch	k1gInSc2	výbuch
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
obrovského	obrovský	k2eAgNnSc2d1	obrovské
množství	množství	k1gNnSc2	množství
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
deformace	deformace	k1gFnSc2	deformace
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
energie	energie	k1gFnSc1	energie
uvolnila	uvolnit	k5eAaPmAgFnS	uvolnit
<g/>
.	.	kIx.	.
</s>
<s>
Prostředí	prostředí	k1gNnSc1	prostředí
vystavené	vystavená	k1gFnSc3	vystavená
deformaci	deformace	k1gFnSc3	deformace
má	mít	k5eAaImIp3nS	mít
tendenci	tendence	k1gFnSc4	tendence
se	se	k3xPyFc4	se
elasticky	elasticky	k6eAd1	elasticky
snažit	snažit	k5eAaImF	snažit
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
původního	původní	k2eAgInSc2d1	původní
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
seismickou	seismický	k2eAgFnSc4d1	seismická
vlnu	vlna	k1gFnSc4	vlna
nebo	nebo	k8xC	nebo
spíše	spíše	k9	spíše
celou	celý	k2eAgFnSc4d1	celá
sérii	série	k1gFnSc4	série
těchto	tento	k3xDgFnPc2	tento
vln	vlna	k1gFnPc2	vlna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
měření	měření	k1gNnSc3	měření
seismických	seismický	k2eAgFnPc2d1	seismická
vln	vlna	k1gFnPc2	vlna
slouží	sloužit	k5eAaImIp3nP	sloužit
seismografy	seismograf	k1gInPc1	seismograf
a	a	k8xC	a
obor	obor	k1gInSc1	obor
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
seismologie	seismologie	k1gFnSc1	seismologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
seismických	seismický	k2eAgFnPc2d1	seismická
vln	vlna	k1gFnPc2	vlna
==	==	k?	==
</s>
</p>
<p>
<s>
Seismické	seismický	k2eAgFnPc1d1	seismická
vlny	vlna	k1gFnPc1	vlna
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
ve	v	k7c6	v
4	[number]	k4	4
základních	základní	k2eAgInPc6d1	základní
typech	typ	k1gInPc6	typ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
P-vlny	Plna	k1gFnSc2	P-vlna
===	===	k?	===
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
seismické	seismický	k2eAgFnPc4d1	seismická
vlny	vlna	k1gFnPc4	vlna
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
rychlostí	rychlost	k1gFnPc2	rychlost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jim	on	k3xPp3gMnPc3	on
také	také	k6eAd1	také
dalo	dát	k5eAaPmAgNnS	dát
své	svůj	k3xOyFgNnSc1	svůj
jméno	jméno	k1gNnSc1	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
seismickou	seismický	k2eAgFnSc4d1	seismická
stanici	stanice	k1gFnSc4	stanice
dorazí	dorazit	k5eAaPmIp3nS	dorazit
jako	jako	k9	jako
první	první	k4xOgInPc4	první
primary	primar	k1gInPc4	primar
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
podélné	podélný	k2eAgNnSc4d1	podélné
vlnění	vlnění	k1gNnSc4	vlnění
tj.	tj.	kA	tj.
o	o	k7c4	o
vlny	vlna	k1gFnPc4	vlna
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
průchozí	průchozí	k2eAgNnSc4d1	průchozí
těleso	těleso	k1gNnSc4	těleso
<g/>
/	/	kIx~	/
<g/>
hmotu	hmota	k1gFnSc4	hmota
stlačují	stlačovat	k5eAaImIp3nP	stlačovat
a	a	k8xC	a
rozpínají	rozpínat	k5eAaImIp3nP	rozpínat
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
šíření	šíření	k1gNnSc2	šíření
vln	vlna	k1gFnPc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
také	také	k9	také
nazývají	nazývat	k5eAaImIp3nP	nazývat
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
vlastnost	vlastnost	k1gFnSc4	vlastnost
elastické	elastický	k2eAgFnSc2d1	elastická
tlakové	tlakový	k2eAgFnSc2d1	tlaková
vlny	vlna	k1gFnSc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
projít	projít	k5eAaPmF	projít
skrz	skrz	k6eAd1	skrz
celé	celý	k2eAgNnSc4d1	celé
zemské	zemský	k2eAgNnSc4d1	zemské
těleso	těleso	k1gNnSc4	těleso
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
nám	my	k3xPp1nPc3	my
přináší	přinášet	k5eAaImIp3nS	přinášet
cenné	cenný	k2eAgInPc4d1	cenný
poznatky	poznatek	k1gInPc4	poznatek
o	o	k7c4	o
složení	složení	k1gNnSc4	složení
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vlny	vlna	k1gFnPc1	vlna
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
na	na	k7c6	na
přechodných	přechodný	k2eAgFnPc6d1	přechodná
oblastech	oblast	k1gFnPc6	oblast
měnit	měnit	k5eAaImF	měnit
svojí	svojit	k5eAaImIp3nP	svojit
rychlost	rychlost	k1gFnSc1	rychlost
a	a	k8xC	a
směr	směr	k1gInSc1	směr
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
zpětně	zpětně	k6eAd1	zpětně
odvodit	odvodit	k5eAaPmF	odvodit
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgInPc1	jaký
minerály	minerál	k1gInPc1	minerál
<g/>
/	/	kIx~	/
<g/>
horniny	hornina	k1gFnPc1	hornina
se	se	k3xPyFc4	se
ve	v	k7c6	v
vrstvách	vrstva	k1gFnPc6	vrstva
nacházejí	nacházet	k5eAaImIp3nP	nacházet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
S-vlny	Slna	k1gFnSc2	S-vlna
===	===	k?	===
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
příčné	příčný	k2eAgNnSc4d1	příčné
vlnění	vlnění	k1gNnSc4	vlnění
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
rychlost	rychlost	k1gFnSc1	rychlost
je	být	k5eAaImIp3nS	být
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
u	u	k7c2	u
P-vln	Plna	k1gFnPc2	P-vlna
<g/>
.	.	kIx.	.
</s>
<s>
Dorazí	dorazit	k5eAaPmIp3nS	dorazit
na	na	k7c6	na
pozorovací	pozorovací	k2eAgFnSc6d1	pozorovací
stanici	stanice	k1gFnSc6	stanice
až	až	k9	až
tedy	tedy	k9	tedy
jako	jako	k9	jako
druhé	druhý	k4xOgFnSc2	druhý
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jim	on	k3xPp3gMnPc3	on
dalo	dát	k5eAaPmAgNnS	dát
jejich	jejich	k3xOp3gInSc4	jejich
název	název	k1gInSc4	název
secondary	secondara	k1gFnSc2	secondara
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jejich	jejich	k3xOp3gInSc6	jejich
průchodu	průchod	k1gInSc6	průchod
začne	začít	k5eAaPmIp3nS	začít
těleso	těleso	k1gNnSc1	těleso
oscilovat	oscilovat	k5eAaImF	oscilovat
<g/>
,	,	kIx,	,
částice	částice	k1gFnPc1	částice
tělesa	těleso	k1gNnSc2	těleso
začnou	začít	k5eAaPmIp3nP	začít
kmitat	kmitat	k5eAaImF	kmitat
kolmo	kolmo	k6eAd1	kolmo
ke	k	k7c3	k
směru	směr	k1gInSc3	směr
procházejícího	procházející	k2eAgNnSc2d1	procházející
vlnění	vlnění	k1gNnSc2	vlnění
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
destruktivní	destruktivní	k2eAgInPc4d1	destruktivní
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
fyzikální	fyzikální	k2eAgFnSc2d1	fyzikální
podstaty	podstata	k1gFnSc2	podstata
vlnění	vlnění	k1gNnSc2	vlnění
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
S-vlny	Slna	k1gFnPc1	S-vlna
nemohou	moct	k5eNaImIp3nP	moct
procházet	procházet	k5eAaImF	procházet
kapalným	kapalný	k2eAgNnSc7d1	kapalné
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
kapalina	kapalina	k1gFnSc1	kapalina
nemá	mít	k5eNaImIp3nS	mít
žádnou	žádný	k3yNgFnSc4	žádný
tuhost	tuhost	k1gFnSc4	tuhost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rayleighovy	Rayleighův	k2eAgFnSc2d1	Rayleighův
vlny	vlna	k1gFnSc2	vlna
===	===	k?	===
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vlnění	vlnění	k1gNnSc4	vlnění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
popsat	popsat	k5eAaPmF	popsat
jako	jako	k9	jako
kruhové	kruhový	k2eAgNnSc4d1	kruhové
vlnění	vlnění	k1gNnSc4	vlnění
v	v	k7c6	v
extrémních	extrémní	k2eAgInPc6d1	extrémní
případech	případ	k1gInPc6	případ
po	po	k7c6	po
elipsových	elipsový	k2eAgFnPc6d1	elipsová
drahách	draha	k1gFnPc6	draha
(	(	kIx(	(
<g/>
podobá	podobat	k5eAaImIp3nS	podobat
se	se	k3xPyFc4	se
vlnění	vlnění	k1gNnSc4	vlnění
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
mořských	mořský	k2eAgFnPc6d1	mořská
vlnách	vlna	k1gFnPc6	vlna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
vlny	vlna	k1gFnPc1	vlna
se	se	k3xPyFc4	se
šíří	šířit	k5eAaImIp3nP	šířit
při	při	k7c6	při
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
její	její	k3xOp3gFnSc3	její
oscilaci	oscilace	k1gFnSc3	oscilace
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejpomalejší	pomalý	k2eAgFnPc4d3	nejpomalejší
zemětřesné	zemětřesný	k2eAgFnPc4d1	zemětřesná
vlny	vlna	k1gFnPc4	vlna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Loveovy	Loveův	k2eAgFnSc2d1	Loveův
vlny	vlna	k1gFnSc2	vlna
===	===	k?	===
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vlnění	vlnění	k1gNnSc4	vlnění
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
částice	částice	k1gFnPc1	částice
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
vzhledem	vzhled	k1gInSc7	vzhled
ke	k	k7c3	k
směru	směr	k1gInSc3	směr
vlnění	vlnění	k1gNnSc2	vlnění
na	na	k7c4	na
šířku	šířka	k1gFnSc4	šířka
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
úhlu	úhel	k1gInSc6	úhel
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
Rayleighovy	Rayleighův	k2eAgFnPc1d1	Rayleighův
vlny	vlna	k1gFnPc1	vlna
se	se	k3xPyFc4	se
i	i	k9	i
tyto	tento	k3xDgFnPc1	tento
vlny	vlna	k1gFnPc1	vlna
šíří	šířit	k5eAaImIp3nP	šířit
při	při	k7c6	při
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Loveovy	Loveův	k2eAgFnPc4d1	Loveův
vlny	vlna	k1gFnPc4	vlna
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
také	také	k6eAd1	také
označení	označení	k1gNnSc1	označení
Q	Q	kA	Q
-	-	kIx~	-
vlny	vlna	k1gFnSc2	vlna
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
Q	Q	kA	Q
waves	waves	k1gInSc1	waves
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
existence	existence	k1gFnSc1	existence
byla	být	k5eAaImAgFnS	být
matematicky	matematicky	k6eAd1	matematicky
předpovězena	předpovězen	k2eAgFnSc1d1	předpovězena
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
A.	A.	kA	A.
E.	E.	kA	E.
H.	H.	kA	H.
Lovem	lov	k1gInSc7	lov
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yRgInSc6	který
převzaly	převzít	k5eAaPmAgInP	převzít
i	i	k9	i
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Zemským	zemský	k2eAgInSc7d1	zemský
pláštěm	plášť	k1gInSc7	plášť
cestují	cestovat	k5eAaImIp3nP	cestovat
pomaleji	pomale	k6eAd2	pomale
než	než	k8xS	než
P	P	kA	P
a	a	k8xC	a
S	s	k7c7	s
vlny	vlna	k1gFnSc2	vlna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
Rayleighovy	Rayleighův	k2eAgFnPc1d1	Rayleighův
vlny	vlna	k1gFnPc1	vlna
<g/>
.	.	kIx.	.
</s>
</p>
