<s>
Severoatlantická	severoatlantický	k2eAgFnSc1d1	Severoatlantická
aliance	aliance	k1gFnSc1	aliance
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
North	North	k1gInSc1	North
Atlantic	Atlantice	k1gFnPc2	Atlantice
Treaty	Treata	k1gFnSc2	Treata
Organization	Organization	k1gInSc1	Organization
–	–	k?	–
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
Organisation	Organisation	k1gInSc4	Organisation
du	du	k?	du
Traité	Traitý	k2eAgFnSc2d1	Traitý
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Atlantique	Atlantiqu	k1gMnSc2	Atlantiqu
Nord	Nord	k1gInSc1	Nord
–	–	k?	–
OTAN	OTAN	kA	OTAN
<g/>
;	;	kIx,	;
doslova	doslova	k6eAd1	doslova
Organizace	organizace	k1gFnSc1	organizace
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
euroatlantický	euroatlantický	k2eAgInSc1d1	euroatlantický
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
vojenský	vojenský	k2eAgInSc1d1	vojenský
pakt	pakt	k1gInSc1	pakt
<g/>
.	.	kIx.	.
</s>
