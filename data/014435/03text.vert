<s>
Parthenón	Parthenón	k1gInSc1
</s>
<s>
Parthenón	Parthenón	k1gInSc1
-	-	kIx~
pohled	pohled	k1gInSc1
ze	z	k7c2
západu	západ	k1gInSc2
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
Pantheon	Pantheon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Parthenón	Parthenón	k1gInSc1
(	(	kIx(
<g/>
Π	Π	kIx~
<g/>
,	,	kIx,
řec.	řec.	kA
chrám	chrám	k1gInSc1
panen	panna	k1gFnPc2
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
hlavním	hlavní	k2eAgInSc7d1
chrámem	chrám	k1gInSc7
antických	antický	k2eAgFnPc2d1
Athén	Athény	k1gFnPc2
zasvěceným	zasvěcený	k2eAgInSc3d1
bohyni	bohyně	k1gFnSc3
Athéně	Athéna	k1gFnSc3
Parthenos	Parthenos	k1gFnSc3
(	(	kIx(
<g/>
Panenské	panenský	k2eAgFnSc3d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
vystavěn	vystavět	k5eAaPmNgMnS
v	v	k7c6
letech	léto	k1gNnPc6
447	#num#	k4
–	–	k?
438	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
na	na	k7c6
athénské	athénský	k2eAgFnSc6d1
Akropoli	Akropole	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
můžeme	moct	k5eAaImIp1nP
jeho	jeho	k3xOp3gInPc4
pozůstatky	pozůstatek	k1gInPc4
spatřit	spatřit	k5eAaPmF
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Stručně	stručně	k6eAd1
</s>
<s>
Chrám	chrám	k1gInSc1
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
vedle	vedle	k7c2
komplexu	komplex	k1gInSc2
dalších	další	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
na	na	k7c4
Akropoli	Akropole	k1gFnSc4
v	v	k7c6
době	doba	k1gFnSc6
největšího	veliký	k2eAgInSc2d3
rozmachu	rozmach	k1gInSc2
Athén	Athéna	k1gFnPc2
za	za	k7c4
Perikla	Perikla	k1gFnPc4
jako	jako	k8xS,k8xC
okázalá	okázalý	k2eAgFnSc1d1
demonstrace	demonstrace	k1gFnSc1
bohatství	bohatství	k1gNnSc2
(	(	kIx(
<g/>
pokladnice	pokladnice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
moci	moc	k1gFnSc2
Athén	Athéna	k1gFnPc2
a	a	k8xC
v	v	k7c6
neposlední	poslední	k2eNgFnSc6d1
řadě	řada	k1gFnSc6
jako	jako	k8xS,k8xC
oslava	oslava	k1gFnSc1
demokracie	demokracie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uměleckou	umělecký	k2eAgFnSc7d1
záštitou	záštita	k1gFnSc7
byl	být	k5eAaImAgInS
největší	veliký	k2eAgMnSc1d3
antický	antický	k2eAgMnSc1d1
sochař	sochař	k1gMnSc1
Feidiás	Feidiása	k1gFnPc2
a	a	k8xC
jeho	jeho	k3xOp3gMnPc4
architekty	architekt	k1gMnPc4
byli	být	k5eAaImAgMnP
Iktinos	Iktinosa	k1gFnPc2
a	a	k8xC
Kallikratés	Kallikratésa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svému	svůj	k3xOyFgInSc3
původnímu	původní	k2eAgInSc3d1
účelu	účel	k1gInSc3
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
chrám	chrám	k1gInSc1
Athény	Athéna	k1gFnSc2
<g/>
,	,	kIx,
sloužil	sloužit	k5eAaImAgInS
do	do	k7c2
4	#num#	k4
<g/>
.	.	kIx.
stol	stol	k1gInSc1
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
tedy	tedy	k8xC
téměř	téměř	k6eAd1
1000	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
křesťanství	křesťanství	k1gNnSc2
byl	být	k5eAaImAgInS
přeměněn	přeměnit	k5eAaPmNgInS
na	na	k7c4
chrám	chrám	k1gInSc4
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
výzdoba	výzdoba	k1gFnSc1
byla	být	k5eAaImAgFnS
poškozena	poškodit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
R.	R.	kA
1687	#num#	k4
utrpěla	utrpět	k5eAaPmAgFnS
stavba	stavba	k1gFnSc1
největší	veliký	k2eAgNnSc4d3
poškození	poškození	k1gNnSc4
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
historii	historie	k1gFnSc6
po	po	k7c6
zásahu	zásah	k1gInSc6
turecké	turecký	k2eAgFnSc2d1
prachárny	prachárna	k1gFnSc2
bombou	bomba	k1gFnSc7
z	z	k7c2
benátské	benátský	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
občasnému	občasný	k2eAgNnSc3d1
odvážení	odvážení	k1gNnSc3
zlomků	zlomek	k1gInPc2
sochařské	sochařský	k2eAgFnSc2d1
výzdoby	výzdoba	k1gFnSc2
do	do	k7c2
měst	město	k1gNnPc2
západní	západní	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	on	k3xPp3gInPc4
můžeme	moct	k5eAaImIp1nP
v	v	k7c6
muzeích	muzeum	k1gNnPc6
najít	najít	k5eAaPmF
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parthenón	Parthenón	k1gInSc1
je	být	k5eAaImIp3nS
zapsán	zapsat	k5eAaPmNgInS
na	na	k7c6
seznamu	seznam	k1gInSc6
UNESCO	UNESCO	kA
a	a	k8xC
od	od	k7c2
r.	r.	kA
1975	#num#	k4
prochází	procházet	k5eAaImIp3nS
soustavnou	soustavný	k2eAgFnSc7d1
rekonstrukcí	rekonstrukce	k1gFnSc7
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
i	i	k9
za	za	k7c4
přispění	přispění	k1gNnSc4
fondů	fond	k1gInPc2
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Parthenón	Parthenón	k1gInSc1
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
jako	jako	k8xC,k8xS
řecký	řecký	k2eAgInSc1d1
chrám	chrám	k1gInSc1
dórského	dórský	k2eAgInSc2d1
řádu	řád	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
některé	některý	k3yIgInPc1
jeho	jeho	k3xOp3gInPc1
prvky	prvek	k1gInPc1
jsou	být	k5eAaImIp3nP
řádu	řád	k1gInSc2
iónského	iónský	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
stavbě	stavba	k1gFnSc3
bylo	být	k5eAaImAgNnS
použito	použít	k5eAaPmNgNnS
mramoru	mramor	k1gInSc2
z	z	k7c2
nedalekých	daleký	k2eNgInPc2d1
lomů	lom	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
starověku	starověk	k1gInSc6
byl	být	k5eAaImAgInS
vnímán	vnímat	k5eAaImNgInS
jako	jako	k8xC,k8xS
nejkrásnější	krásný	k2eAgInSc1d3
dórský	dórský	k2eAgInSc1d1
chrám	chrám	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uvnitř	uvnitř	k6eAd1
stávala	stávat	k5eAaImAgFnS
12	#num#	k4
m	m	kA
vysoká	vysoký	k2eAgFnSc1d1
socha	socha	k1gFnSc1
bohyně	bohyně	k1gFnSc2
Athény	Athéna	k1gFnSc2
ze	z	k7c2
zlata	zlato	k1gNnSc2
a	a	k8xC
slonoviny	slonovina	k1gFnSc2
–	–	k?
dílo	dílo	k1gNnSc1
sochaře	sochař	k1gMnSc2
Feidia	Feidium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vlysu	vlys	k1gInSc6
jsou	být	k5eAaImIp3nP
zobrazeny	zobrazit	k5eAaPmNgInP
mytologické	mytologický	k2eAgInPc1d1
výjevy	výjev	k1gInPc1
–	–	k?
boj	boj	k1gInSc1
bohů	bůh	k1gMnPc2
s	s	k7c7
giganty	gigant	k1gInPc7
<g/>
,	,	kIx,
boj	boj	k1gInSc1
Řeků	Řek	k1gMnPc2
s	s	k7c7
Amazonkami	Amazonka	k1gFnPc7
<g/>
,	,	kIx,
boj	boj	k1gInSc1
Lapithů	Lapith	k1gMnPc2
s	s	k7c7
kentaury	kentaur	k1gMnPc7
a	a	k8xC
Pád	Pád	k1gInSc1
Tróje	Trója	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
iónském	iónský	k2eAgInSc6d1
vlysu	vlys	k1gInSc6
okolo	okolo	k7c2
vnitřní	vnitřní	k2eAgFnSc2d1
celly	cello	k1gNnPc7
je	být	k5eAaImIp3nS
zobrazen	zobrazen	k2eAgInSc1d1
průvod	průvod	k1gInSc1
athénských	athénský	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
k	k	k7c3
chrámu	chrám	k1gInSc3
Athény	Athéna	k1gFnSc2
během	během	k7c2
panathénajských	panathénajský	k2eAgFnPc2d1
slavností	slavnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sochařská	sochařský	k2eAgFnSc1d1
výzdoba	výzdoba	k1gFnSc1
ve	v	k7c6
štítech	štít	k1gInPc6
znázorňuje	znázorňovat	k5eAaImIp3nS
zrození	zrození	k1gNnSc1
Athény	Athéna	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gNnSc1
vítězství	vítězství	k1gNnSc1
nad	nad	k7c7
Poseidónem	Poseidón	k1gInSc7
o	o	k7c4
vládu	vláda	k1gFnSc4
nad	nad	k7c7
Attikou	Attika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návrh	návrh	k1gInSc1
sochařské	sochařský	k2eAgFnSc2d1
výzdoby	výzdoba	k1gFnSc2
a	a	k8xC
částečně	částečně	k6eAd1
i	i	k9
provedení	provedení	k1gNnSc4
pochází	pocházet	k5eAaImIp3nS
taktéž	taktéž	k?
od	od	k7c2
Feidia	Feidium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Panoramatický	panoramatický	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
Athény	Athéna	k1gFnPc4
s	s	k7c7
výraznou	výrazný	k2eAgFnSc7d1
dominantou	dominanta	k1gFnSc7
Parthenónu	Parthenón	k1gInSc2
na	na	k7c6
Akropoli	Akropole	k1gFnSc6
</s>
<s>
Historie	historie	k1gFnSc1
chrámu	chrám	k1gInSc2
</s>
<s>
Původ	původ	k1gInSc1
kultu	kult	k1gInSc2
Athény	Athéna	k1gFnSc2
a	a	k8xC
předchůdci	předchůdce	k1gMnPc1
chrámu	chrám	k1gInSc2
</s>
<s>
Uctívání	uctívání	k1gNnSc1
bohyně	bohyně	k1gFnSc2
Athény	Athéna	k1gFnSc2
v	v	k7c6
Attice	Attika	k1gFnSc6
je	být	k5eAaImIp3nS
prastarého	prastarý	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
Athény	Athéna	k1gFnSc2
odvozuje	odvozovat	k5eAaImIp3nS
své	svůj	k3xOyFgNnSc4
jméno	jméno	k1gNnSc4
od	od	k7c2
jakýchsi	jakýsi	k3yIgInPc2
posvátných	posvátný	k2eAgInPc2d1
subjektů	subjekt	k1gInPc2
„	„	k?
<g/>
athénas	athénas	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
nakonec	nakonec	k6eAd1
přešly	přejít	k5eAaPmAgInP
v	v	k7c6
antropomorfní	antropomorfní	k2eAgFnSc6d1
(	(	kIx(
<g/>
člověku	člověk	k1gMnSc6
podobnou	podobný	k2eAgFnSc4d1
<g/>
)	)	kIx)
abstrakci	abstrakce	k1gFnSc4
Athény	Athéna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
představa	představa	k1gFnSc1
bohyně	bohyně	k1gFnSc2
–	–	k?
matky	matka	k1gFnSc2
byla	být	k5eAaImAgNnP
pozdějšími	pozdní	k2eAgFnPc7d2
společenskými	společenský	k2eAgFnPc7d1
změnami	změna	k1gFnPc7
upravena	upravit	k5eAaPmNgFnS
na	na	k7c4
panenskou	panenský	k2eAgFnSc4d1
bohyni	bohyně	k1gFnSc4
s	s	k7c7
vyloženě	vyloženě	k6eAd1
mužskými	mužský	k2eAgInPc7d1
atributy	atribut	k1gInPc7
tak	tak	k8xS,k8xC
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
ji	on	k3xPp3gFnSc4
známe	znát	k5eAaImIp1nP
z	z	k7c2
historické	historický	k2eAgFnSc2d1
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jako	jako	k8xS,k8xC
strážnému	strážný	k2eAgNnSc3d1
božstvu	božstvo	k1gNnSc3
jí	on	k3xPp3gFnSc2
byl	být	k5eAaImAgMnS
zasvěcen	zasvěcen	k2eAgInSc4d1
severní	severní	k2eAgInSc4d1
cíp	cíp	k1gInSc4
Akropole	Akropole	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
chrám	chrám	k1gInSc1
<g/>
,	,	kIx,
o	o	k7c6
kterém	který	k3yQgNnSc6,k3yRgNnSc6,k3yIgNnSc6
víme	vědět	k5eAaImIp1nP
z	z	k7c2
historické	historický	k2eAgFnSc2d1
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
nechal	nechat	k5eAaPmAgMnS
vystavět	vystavět	k5eAaPmF
(	(	kIx(
<g/>
570	#num#	k4
–	–	k?
566	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
se	s	k7c7
založením	založení	k1gNnSc7
velkých	velký	k2eAgFnPc2d1
Panathénají	Panathénají	k1gFnPc2
tyran	tyran	k1gMnSc1
Peisistratos	Peisistratos	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jmenoval	jmenovat	k5eAaImAgMnS,k5eAaBmAgMnS
se	se	k3xPyFc4
Hekatompedon	Hekatompedon	k1gMnSc1
(	(	kIx(
<g/>
Chrám	chrám	k1gInSc1
sta	sto	k4xCgNnPc4
stop	stopa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
zhotoven	zhotovit	k5eAaPmNgInS
z	z	k7c2
tufu	tuf	k1gInSc2
a	a	k8xC
stával	stávat	k5eAaImAgInS
na	na	k7c6
místě	místo	k1gNnSc6
Parthenónu	Parthenón	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
dřívějších	dřívější	k2eAgInPc6d1
chrámech	chrám	k1gInPc6
mnoho	mnoho	k6eAd1
nevíme	vědět	k5eNaImIp1nP
a	a	k8xC
jejich	jejich	k3xOp3gInPc1
zbytky	zbytek	k1gInPc1
nalezeny	nalézt	k5eAaBmNgInP,k5eAaPmNgInP
nebyly	být	k5eNaImAgInP
nepochybně	pochybně	k6eNd1
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
existovaly	existovat	k5eAaImAgFnP
<g/>
-li	-li	k?
<g/>
,	,	kIx,
tak	tak	k6eAd1
byly	být	k5eAaImAgInP
postaveny	postavit	k5eAaPmNgInP
ze	z	k7c2
dřeva	dřevo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
roku	rok	k1gInSc2
525	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
byla	být	k5eAaImAgFnS
započata	započat	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
prvního	první	k4xOgInSc2
chrámu	chrám	k1gInSc2
z	z	k7c2
mramoru	mramor	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
měl	mít	k5eAaImAgInS
Peisistratův	Peisistratův	k2eAgInSc4d1
chrám	chrám	k1gInSc4
nahradit	nahradit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavba	stavba	k1gFnSc1
dospěla	dochvít	k5eAaPmAgFnS
ke	k	k7c3
vztyčení	vztyčení	k1gNnSc3
sloupů	sloup	k1gInPc2
<g/>
,	,	kIx,
když	když	k8xS
byla	být	k5eAaImAgFnS
pobořena	pobořit	k5eAaPmNgFnS
perským	perský	k2eAgNnSc7d1
vojskem	vojsko	k1gNnSc7
v	v	k7c6
řecko-perských	řecko-perský	k2eAgFnPc6d1
válkách	válka	k1gFnPc6
v	v	k7c6
letech	léto	k1gNnPc6
480	#num#	k4
a	a	k8xC
479	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Stavba	stavba	k1gFnSc1
Parthenónu	Parthenón	k1gInSc2
</s>
<s>
Po	po	k7c6
porážce	porážka	k1gFnSc6
perského	perský	k2eAgNnSc2d1
vojska	vojsko	k1gNnSc2
u	u	k7c2
Platají	Platají	k1gFnSc2
(	(	kIx(
<g/>
roku	rok	k1gInSc2
479	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
Athénami	Athéna	k1gFnPc7
v	v	k7c6
roce	rok	k1gInSc6
477	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
založen	založen	k2eAgInSc1d1
Délský	Délský	k2eAgInSc1d1
spolek	spolek	k1gInSc1
s	s	k7c7
cílem	cíl	k1gInSc7
sjednotit	sjednotit	k5eAaPmF
obce	obec	k1gFnSc2
k	k	k7c3
odporu	odpor	k1gInSc3
proti	proti	k7c3
Perské	perský	k2eAgFnSc3d1
říši	říš	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každá	každý	k3xTgFnSc1
členská	členský	k2eAgFnSc1d1
obec	obec	k1gFnSc1
měla	mít	k5eAaImAgFnS
za	za	k7c4
povinnost	povinnost	k1gFnSc4
poskytnout	poskytnout	k5eAaPmF
určitý	určitý	k2eAgInSc4d1
počet	počet	k1gInSc4
triér	triéra	k1gFnPc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
se	se	k3xPyFc4
mohla	moct	k5eAaImAgFnS
z	z	k7c2
této	tento	k3xDgFnSc2
povinnosti	povinnost	k1gFnSc2
vykoupit	vykoupit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
zdrojem	zdroj	k1gInSc7
značných	značný	k2eAgInPc2d1
příjmů	příjem	k1gInPc2
pro	pro	k7c4
Athény	Athéna	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
měly	mít	k5eAaImAgFnP
triér	triér	k1gInSc4
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
ostatní	ostatní	k2eAgFnPc1d1
obce	obec	k1gFnPc1
se	se	k3xPyFc4
stávaly	stávat	k5eAaImAgFnP
závislými	závislý	k2eAgInPc7d1
na	na	k7c6
Athénách	Athéna	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Potom	potom	k6eAd1
<g/>
,	,	kIx,
co	co	k9
perská	perský	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
za	za	k7c2
řecko-perských	řecko-perský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
dvakrát	dvakrát	k6eAd1
vyplenila	vyplenit	k5eAaPmAgFnS
Athény	Athéna	k1gFnPc4
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
třeba	třeba	k9
toto	tento	k3xDgNnSc1
město	město	k1gNnSc1
včetně	včetně	k7c2
chrámů	chrám	k1gInPc2
opravit	opravit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
však	však	k9
přišly	přijít	k5eAaPmAgFnP
na	na	k7c4
řadu	řada	k1gFnSc4
stavby	stavba	k1gFnSc2
praktického	praktický	k2eAgInSc2d1
významu	význam	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kimón	Kimón	k1gInSc1
dal	dát	k5eAaPmAgInS
opravit	opravit	k5eAaPmF
opevnění	opevnění	k1gNnSc4
Akropole	Akropole	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potom	potom	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
do	do	k7c2
čela	čelo	k1gNnSc2
obce	obec	k1gFnSc2
postavil	postavit	k5eAaPmAgInS
Periklés	Periklés	k1gInSc1
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
o	o	k7c6
rozsáhlé	rozsáhlý	k2eAgFnSc6d1
stavební	stavební	k2eAgFnSc6d1
obnově	obnova	k1gFnSc6
Athén	Athéna	k1gFnPc2
<g/>
.	.	kIx.
451-448	451-448	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
byly	být	k5eAaImAgFnP
vybudovány	vybudován	k2eAgFnPc1d1
loděnice	loděnice	k1gFnPc1
<g/>
,	,	kIx,
suché	suchý	k2eAgInPc1d1
doky	dok	k1gInPc1
a	a	k8xC
arzenál	arzenál	k1gInSc1
v	v	k7c6
Peiraieu	Peiraieus	k1gInSc6
a	a	k8xC
současně	současně	k6eAd1
byl	být	k5eAaImAgInS
zvýšen	zvýšit	k5eAaPmNgInS
počet	počet	k1gInSc1
triér	triéra	k1gFnPc2
na	na	k7c4
300	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Peiraieu	Peiraieum	k1gNnSc6
byla	být	k5eAaImAgFnS
také	také	k9
vystavěna	vystavěn	k2eAgFnSc1d1
obilní	obilní	k2eAgFnSc1d1
tržnice	tržnice	k1gFnSc1
pro	pro	k7c4
zabezpečení	zabezpečení	k1gNnSc4
výživy	výživa	k1gFnSc2
města	město	k1gNnSc2
a	a	k8xC
ještě	ještě	k9
před	před	k7c7
skončením	skončení	k1gNnSc7
řecko-perských	řecko-perský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
také	také	k9
opevnění	opevnění	k1gNnSc1
Athén	Athéna	k1gFnPc2
s	s	k7c7
tzv.	tzv.	kA
dlouhými	dlouhý	k2eAgFnPc7d1
zdmi	zeď	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
chránily	chránit	k5eAaImAgFnP
cestu	cesta	k1gFnSc4
z	z	k7c2
Peiraiea	Peiraie	k1gInSc2
do	do	k7c2
Athén	Athéna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posléze	posléze	k6eAd1
se	se	k3xPyFc4
započalo	započnout	k5eAaPmAgNnS
s	s	k7c7
přestavbou	přestavba	k1gFnSc7
chrámů	chrám	k1gInPc2
a	a	k8xC
kulturních	kulturní	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
a	a	k8xC
roku	rok	k1gInSc2
455	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
byla	být	k5eAaImAgFnS
započata	započnout	k5eAaPmNgFnS
i	i	k9
přestavba	přestavba	k1gFnSc1
jižního	jižní	k2eAgInSc2d1
cípu	cíp	k1gInSc2
Akropole	Akropole	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
měl	mít	k5eAaImAgInS
stát	stát	k5eAaImF,k5eAaPmF
chrámek	chrámek	k1gInSc1
Athény	Athéna	k1gFnSc2
Níky	Níka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
záměry	záměr	k1gInPc1
však	však	k9
byly	být	k5eAaImAgInP
přerušeny	přerušit	k5eAaPmNgInP
ve	v	k7c4
prospěch	prospěch	k1gInSc4
ucelenější	ucelený	k2eAgFnSc2d2
přestavby	přestavba	k1gFnSc2
Akropole	Akropole	k1gFnSc2
a	a	k8xC
vybudování	vybudování	k1gNnSc4
hlavního	hlavní	k2eAgInSc2d1
Athénina	Athénin	k2eAgInSc2d1
chrámu	chrám	k1gInSc2
–	–	k?
Parthenónu	Parthenón	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Plán	plán	k1gInSc1
chrámu	chrám	k1gInSc2
navrhl	navrhnout	k5eAaPmAgInS
architekt	architekt	k1gMnSc1
Iktínos	Iktínosa	k1gFnPc2
a	a	k8xC
roku	rok	k1gInSc2
447	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
byla	být	k5eAaImAgFnS
započata	započat	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
chrámu	chrámat	k5eAaImIp1nS
při	při	k7c6
jižním	jižní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
akropolské	akropolský	k2eAgFnSc2d1
plošiny	plošina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stavbu	stavba	k1gFnSc4
vedl	vést	k5eAaImAgInS
architekt	architekt	k1gMnSc1
Kallikratés	Kallikratésa	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
hlavní	hlavní	k2eAgInSc1d1
dozor	dozor	k1gInSc1
nad	nad	k7c7
stavbami	stavba	k1gFnPc7
na	na	k7c4
Akropoli	Akropole	k1gFnSc4
byl	být	k5eAaImAgInS
svěřen	svěřit	k5eAaPmNgMnS
Periklovu	Periklův	k2eAgMnSc3d1
příteli	přítel	k1gMnSc3
<g/>
,	,	kIx,
sochaři	sochař	k1gMnSc3
Feidiovi	Feidius	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
Periklés	Periklés	k1gInSc4
byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
stavební	stavební	k2eAgFnSc2d1
komise	komise	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
442	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
byly	být	k5eAaImAgInP
vztyčeny	vztyčit	k5eAaPmNgInP
sloupy	sloup	k1gInPc1
a	a	k8xC
roku	rok	k1gInSc2
438	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
byly	být	k5eAaImAgFnP
Feidiovou	Feidiový	k2eAgFnSc7d1
sochařskou	sochařský	k2eAgFnSc7d1
dílnou	dílna	k1gFnSc7
umístěny	umístěn	k2eAgFnPc1d1
metopy	metopa	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současně	současně	k6eAd1
byla	být	k5eAaImAgNnP
o	o	k7c6
velkých	velký	k2eAgInPc6d1
Panathénajích	Panathénaj	k1gInPc6
umístěna	umístit	k5eAaPmNgFnS
i	i	k8xC
kultovní	kultovní	k2eAgFnSc1d1
socha	socha	k1gFnSc1
Athény	Athéna	k1gFnSc2
Parthenos	Parthenosa	k1gFnPc2
ze	z	k7c2
zlata	zlato	k1gNnSc2
a	a	k8xC
slonoviny	slonovina	k1gFnSc2
–	–	k?
taktéž	taktéž	k?
Feidiovo	Feidiův	k2eAgNnSc4d1
dílo	dílo	k1gNnSc4
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
heslo	heslo	k1gNnSc4
Feidiás	Feidiás	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
byl	být	k5eAaImAgInS
chrám	chrám	k1gInSc1
také	také	k6eAd1
zasvěcen	zasvětit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
den	den	k1gInSc4
byl	být	k5eAaImAgInS
Periklés	Periklés	k1gInSc1
epistatem	epistat	k1gInSc7
–	–	k?
nejvyšším	vysoký	k2eAgMnSc7d3
úředníkem	úředník	k1gMnSc7
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
kterým	který	k3yIgMnSc7,k3yRgMnSc7,k3yQgMnSc7
se	se	k3xPyFc4
občan	občan	k1gMnSc1
mohl	moct	k5eAaImAgMnS
stát	stát	k5eAaImF,k5eAaPmF
na	na	k7c4
jeden	jeden	k4xCgInSc4
den	den	k1gInSc4
<g/>
,	,	kIx,
jedinkrát	jedinkrát	k6eAd1
v	v	k7c6
životě	život	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sochařská	sochařský	k2eAgFnSc1d1
výzdoba	výzdoba	k1gFnSc1
však	však	k9
stále	stále	k6eAd1
ještě	ještě	k6eAd1
nebyla	být	k5eNaImAgFnS
zcela	zcela	k6eAd1
dokončena	dokončit	k5eAaPmNgFnS
<g/>
,	,	kIx,
neboť	neboť	k8xC
k	k	k7c3
osazení	osazení	k1gNnSc3
soch	socha	k1gFnPc2
do	do	k7c2
štítů	štít	k1gInPc2
došlo	dojít	k5eAaPmAgNnS
až	až	k9
v	v	k7c6
roce	rok	k1gInSc6
432	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
Mezitím	mezitím	k6eAd1
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
budovat	budovat	k5eAaImF
monumentální	monumentální	k2eAgInSc1d1
přístup	přístup	k1gInSc1
na	na	k7c4
Akropol	Akropol	k1gInSc4
–	–	k?
Propylaje	Propylaje	k1gFnSc2
–	–	k?
pod	pod	k7c7
vedením	vedení	k1gNnSc7
architekta	architekt	k1gMnSc2
Mnésikla	Mnésikla	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Smyslem	smysl	k1gInSc7
staveb	stavba	k1gFnPc2
byla	být	k5eAaImAgFnS
oslava	oslava	k1gFnSc1
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
její	její	k3xOp3gFnSc2
moci	moc	k1gFnSc2
a	a	k8xC
také	také	k9
oslava	oslava	k1gFnSc1
demokracie	demokracie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athény	Athéna	k1gFnPc1
se	se	k3xPyFc4
staly	stát	k5eAaPmAgFnP
nejvýstavnějším	výstavní	k2eAgNnSc7d3
městem	město	k1gNnSc7
starověkého	starověký	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
rozsáhlá	rozsáhlý	k2eAgFnSc1d1
stavební	stavební	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
si	se	k3xPyFc3
však	však	k9
vyžádala	vyžádat	k5eAaPmAgFnS
značné	značný	k2eAgInPc4d1
finanční	finanční	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k8xS,k8xC
např.	např.	kA
stavby	stavba	k1gFnSc2
v	v	k7c4
Peiraieu	Peiraiea	k1gFnSc4
stály	stát	k5eAaImAgInP
1000	#num#	k4
talentů	talent	k1gInPc2
stříbra	stříbro	k1gNnSc2
<g/>
,	,	kIx,
Parthenón	Parthenón	k1gInSc1
asi	asi	k9
700	#num#	k4
talentů	talent	k1gInPc2
<g/>
,	,	kIx,
Propylaje	Propylaje	k1gFnSc1
400	#num#	k4
talentů	talent	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Socha	socha	k1gFnSc1
Athény	Athéna	k1gFnSc2
Parthenos	Parthenosa	k1gFnPc2
přišla	přijít	k5eAaPmAgFnS
na	na	k7c4
900	#num#	k4
talentů	talent	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
čehož	což	k3yRnSc2,k3yQnSc2
ale	ale	k8xC
616	#num#	k4
talentů	talent	k1gInPc2
byla	být	k5eAaImAgFnS
hodnota	hodnota	k1gFnSc1
zlatého	zlatý	k2eAgInSc2d1
hávu	háv	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
snímatelný	snímatelný	k2eAgMnSc1d1
a	a	k8xC
tvořil	tvořit	k5eAaImAgInS
finanční	finanční	k2eAgFnSc4d1
rezervu	rezerva	k1gFnSc4
obce	obec	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
talent	talent	k1gInSc1
odpovídá	odpovídat	k5eAaImIp3nS
26,2	26,2	k4
kg	kg	kA
stříbra	stříbro	k1gNnSc2
a	a	k8xC
jehož	jehož	k3xOyRp3gFnSc1
kupní	kupní	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
byla	být	k5eAaImAgFnS
vyšší	vysoký	k2eAgFnSc1d2
než	než	k8xS
dnes	dnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
dva	dva	k4xCgInPc4
talenty	talent	k1gInPc4
bylo	být	k5eAaImAgNnS
možno	možno	k9
pořídit	pořídit	k5eAaPmF
válečnou	válečný	k2eAgFnSc4d1
triéru	triéra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
náklady	náklad	k1gInPc1
byly	být	k5eAaImAgInP
hrazeny	hradit	k5eAaImNgInP
z	z	k7c2
pokladny	pokladna	k1gFnSc2
bohyně	bohyně	k1gFnSc2
Athény	Athéna	k1gFnSc2
i	i	k9
z	z	k7c2
pokladen	pokladna	k1gFnPc2
jiných	jiný	k2eAgMnPc2d1
bohů	bůh	k1gMnPc2
a	a	k8xC
dalších	další	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
obce	obec	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
však	však	k9
již	již	k6eAd1
nedostačovalo	dostačovat	k5eNaImAgNnS
a	a	k8xC
Periklés	Periklés	k1gInSc4
rozhodl	rozhodnout	k5eAaPmAgMnS
o	o	k7c4
použití	použití	k1gNnSc4
prostředků	prostředek	k1gInPc2
pokladny	pokladna	k1gFnSc2
Délského	Délský	k2eAgInSc2d1
spolku	spolek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
přemístěna	přemístit	k5eAaPmNgFnS
z	z	k7c2
ostrova	ostrov	k1gInSc2
Délu	Délos	k1gInSc2
do	do	k7c2
Athén	Athéna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
příležitostí	příležitost	k1gFnSc7
pro	pro	k7c4
aristokratické	aristokratický	k2eAgMnPc4d1
odpůrce	odpůrce	k1gMnPc4
Perikla	Perikla	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
napadli	napadnout	k5eAaPmAgMnP
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
stavební	stavební	k2eAgInSc4d1
program	program	k1gInSc4
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
jim	on	k3xPp3gMnPc3
ideově	ideově	k6eAd1
nevyhovoval	vyhovovat	k5eNaImAgMnS
<g/>
,	,	kIx,
a	a	k8xC
i	i	k9
samotného	samotný	k2eAgMnSc4d1
Perikla	Perikla	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vůdce	vůdce	k1gMnSc1
aristokratů	aristokrat	k1gMnPc2
Thúkididés	Thúkididés	k1gInSc1
prohlásil	prohlásit	k5eAaPmAgInS
použití	použití	k1gNnSc4
prostředků	prostředek	k1gInPc2
spolku	spolek	k1gInSc2
jako	jako	k8xS,k8xC
zneužití	zneužití	k1gNnSc2
moci	moc	k1gFnSc2
a	a	k8xC
poskvrnění	poskvrnění	k1gNnSc2
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
Perikles	Perikles	k1gMnSc1
odpověděl	odpovědět	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
město	město	k1gNnSc1
nemusí	muset	k5eNaImIp3nS
odpovídat	odpovídat	k5eAaImF
z	z	k7c2
nakládáni	nakládat	k5eAaImNgMnP
s	s	k7c7
těmito	tento	k3xDgInPc7
prostředky	prostředek	k1gInPc7
<g/>
,	,	kIx,
dokud	dokud	k8xS
plní	plnit	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
zač	zač	k6eAd1
byly	být	k5eAaImAgInP
zaplaceny	zaplacen	k2eAgInPc1d1
<g/>
,	,	kIx,
totiž	totiž	k9
dokud	dokud	k6eAd1
spojenecké	spojenecký	k2eAgFnSc2d1
obce	obec	k1gFnSc2
ochraňuje	ochraňovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
pozdějšího	pozdní	k2eAgMnSc2d2
spisovatele	spisovatel	k1gMnSc2
se	se	k3xPyFc4
na	na	k7c6
konci	konec	k1gInSc6
rozpravy	rozprava	k1gFnSc2
Periklés	Periklés	k1gInSc1
otázal	otázat	k5eAaPmAgMnS
<g/>
,	,	kIx,
zda	zda	k8xS
jsou	být	k5eAaImIp3nP
výdaje	výdaj	k1gInPc1
příliš	příliš	k6eAd1
vysoké	vysoký	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
dostal	dostat	k5eAaPmAgInS
kladnou	kladný	k2eAgFnSc4d1
odpověď	odpověď	k1gFnSc4
<g/>
,	,	kIx,
tak	tak	k6eAd1
slíbil	slíbit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
bude	být	k5eAaImBp3nS
financovat	financovat	k5eAaBmF
stavbu	stavba	k1gFnSc4
ze	z	k7c2
svých	svůj	k3xOyFgInPc2
vlastních	vlastní	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
na	na	k7c4
každou	každý	k3xTgFnSc4
stavbu	stavba	k1gFnSc4
nechá	nechat	k5eAaPmIp3nS
vytesat	vytesat	k5eAaPmF
své	svůj	k3xOyFgNnSc4
jméno	jméno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okamžitě	okamžitě	k6eAd1
mu	on	k3xPp3gMnSc3
byl	být	k5eAaImAgMnS
odhlasován	odhlasován	k2eAgInSc4d1
přísun	přísun	k1gInSc4
dalších	další	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
<g/>
,	,	kIx,
na	na	k7c6
jaře	jaro	k1gNnSc6
roku	rok	k1gInSc2
443	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
byl	být	k5eAaImAgInS
Thúkididés	Thúkididés	k1gInSc1
ostrakizován	ostrakizovat	k5eAaBmNgInS
a	a	k8xC
se	s	k7c7
stavbou	stavba	k1gFnSc7
se	se	k3xPyFc4
pokračovalo	pokračovat	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
stavbách	stavba	k1gFnPc6
pracovalo	pracovat	k5eAaImAgNnS
mnoho	mnoho	k4c1
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
omezen	omezit	k5eAaPmNgInS
počet	počet	k1gInSc1
otroků	otrok	k1gMnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
dopomohlo	dopomoct	k5eAaPmAgNnS
výdělku	výdělek	k1gInSc6
i	i	k9
chudým	chudý	k2eAgMnPc3d1
občanům	občan	k1gMnPc3
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
<g/>
Nechtěl	Nechtěl	k1gMnSc1
jsem	být	k5eAaImIp1nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ta	ten	k3xDgFnSc1
část	část	k1gFnSc1
chudších	chudý	k2eAgMnPc2d2
pracujících	pracující	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
neslouží	sloužit	k5eNaImIp3nP
v	v	k7c6
armádě	armáda	k1gFnSc6
<g/>
,	,	kIx,
přišla	přijít	k5eAaPmAgFnS
o	o	k7c4
stejné	stejný	k2eAgFnPc4d1
výhody	výhoda	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
rovněž	rovněž	k9
aby	aby	kYmCp3nS
jich	on	k3xPp3gFnPc2
nepožívala	požívat	k5eNaImAgFnS
v	v	k7c6
lenosti	lenost	k1gFnSc6
a	a	k8xC
nečinnosti	nečinnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uskutečnil	uskutečnit	k5eAaPmAgMnS
jsem	být	k5eAaImIp1nS
tedy	tedy	k9
v	v	k7c6
zájmu	zájem	k1gInSc6
lidu	lid	k1gInSc2
ty	ten	k3xDgInPc4
rozsáhlé	rozsáhlý	k2eAgInPc4d1
stavební	stavební	k2eAgInPc4d1
plány	plán	k1gInPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc1
provedení	provedení	k1gNnSc1
zaměstnávalo	zaměstnávat	k5eAaImAgNnS
dlouho	dlouho	k6eAd1
množství	množství	k1gNnSc1
pracovníků	pracovník	k1gMnPc2
různých	různý	k2eAgInPc2d1
oborů	obor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
i	i	k9
obyvatelé	obyvatel	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
jinak	jinak	k6eAd1
zůstávali	zůstávat	k5eAaImAgMnP
doma	doma	k6eAd1
<g/>
,	,	kIx,
nabyli	nabýt	k5eAaPmAgMnP
právo	právo	k1gNnSc4
dostat	dostat	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
díl	díl	k1gInSc4
z	z	k7c2
veřejných	veřejný	k2eAgInPc2d1
peněz	peníze	k1gInPc2
.	.	kIx.
</s>
<s desamb="1">
Tak	tak	k6eAd1
přinesly	přinést	k5eAaPmAgFnP
a	a	k8xC
rozdělily	rozdělit	k5eAaPmAgFnP
tyto	tento	k3xDgFnPc1
práce	práce	k1gFnPc1
blahobyt	blahobyt	k1gInSc4
lidem	lid	k1gInSc7
všeho	všecek	k3xTgInSc2
věku	věk	k1gInSc2
a	a	k8xC
všech	všecek	k3xTgFnPc2
schopností	schopnost	k1gFnPc2
<g/>
.	.	kIx.
<g/>
“	“	k?
(	(	kIx(
<g/>
Periklova	Periklův	k2eAgFnSc1d1
řeč	řeč	k1gFnSc1
podle	podle	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
životopisu	životopis	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Pozdější	pozdní	k2eAgFnSc1d2
historie	historie	k1gFnSc1
</s>
<s>
Jak	jak	k6eAd1
již	již	k6eAd1
bylo	být	k5eAaImAgNnS
řečeno	říct	k5eAaPmNgNnS
<g/>
,	,	kIx,
chrám	chrám	k1gInSc1
sloužil	sloužit	k5eAaImAgInS
kultu	kult	k1gInSc3
Athény	Athéna	k1gFnSc2
téměř	téměř	k6eAd1
tisíc	tisíc	k4xCgInSc1
let	let	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c4
4	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
v	v	k7c6
Řecku	Řecko	k1gNnSc6
upevnilo	upevnit	k5eAaPmAgNnS
křesťanství	křesťanství	k1gNnSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
Parthenón	Parthenón	k1gInSc1
přeměněn	přeměnit	k5eAaPmNgInS
v	v	k7c4
křesťanský	křesťanský	k2eAgInSc4d1
chrám	chrám	k1gInSc4
zasvěcený	zasvěcený	k2eAgInSc4d1
Panně	Panna	k1gFnSc3
Marii	Maria	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Tehdy	tehdy	k6eAd1
byla	být	k5eAaImAgFnS
také	také	k6eAd1
poškozena	poškodit	k5eAaPmNgFnS
jeho	jeho	k3xOp3gFnSc1
sochařská	sochařský	k2eAgFnSc1d1
výzdoba	výzdoba	k1gFnSc1
vnějšího	vnější	k2eAgInSc2d1
vlysu	vlys	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fanatičtí	fanatický	k2eAgMnPc1d1
křesťané	křesťan	k1gMnPc1
ji	on	k3xPp3gFnSc4
otloukli	otlouct	k5eAaPmAgMnP
<g/>
,	,	kIx,
protože	protože	k8xS
znázorňovala	znázorňovat	k5eAaImAgFnS
pohanské	pohanský	k2eAgNnSc4d1
náboženství	náboženství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ponechali	ponechat	k5eAaPmAgMnP
pouze	pouze	k6eAd1
jižní	jižní	k2eAgFnPc4d1
metopy	metopa	k1gFnPc4
<g/>
,	,	kIx,
neboť	neboť	k8xC
na	na	k7c6
nich	on	k3xPp3gInPc6
zobrazení	zobrazený	k2eAgMnPc1d1
kentauři	kentaur	k1gMnPc1
jim	on	k3xPp3gMnPc3
připomínali	připomínat	k5eAaImAgMnP
ďábly	ďábel	k1gMnPc7
<g/>
,	,	kIx,
a	a	k8xC
krajní	krajní	k2eAgFnSc1d1
východní	východní	k2eAgFnSc1d1
metopa	metopa	k1gFnSc1
severní	severní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
rozmluva	rozmluva	k1gFnSc1
Athény	Athéna	k1gFnSc2
a	a	k8xC
Afrodíty	Afrodíta	k1gMnSc2
připomínala	připomínat	k5eAaImAgFnS
výjev	výjev	k1gInSc4
Zvěstování	zvěstování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Iónský	iónský	k2eAgInSc4d1
vlys	vlys	k1gInSc4
kolem	kolem	k6eAd1
celly	cello	k1gNnPc7
a	a	k8xC
výzdoba	výzdoba	k1gFnSc1
štítů	štít	k1gInPc2
byla	být	k5eAaImAgFnS
mimo	mimo	k7c4
jejich	jejich	k3xOp3gInSc4
dosah	dosah	k1gInSc4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Více	hodně	k6eAd2
o	o	k7c6
výzdobě	výzdoba	k1gFnSc6
viz	vidět	k5eAaImRp2nS
níže	nízce	k6eAd2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Feidiova	Feidiův	k2eAgFnSc1d1
kultovní	kultovní	k2eAgFnSc1d1
socha	socha	k1gFnSc1
byla	být	k5eAaImAgFnS
již	již	k6eAd1
předtím	předtím	k6eAd1
odvezena	odvézt	k5eAaPmNgFnS
do	do	k7c2
Konstantinopole	Konstantinopol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
několika	několik	k4yIc3
stavebním	stavební	k2eAgFnPc3d1
změnám	změna	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc1d1
vchod	vchod	k1gInSc1
byl	být	k5eAaImAgInS
přeložen	přeložit	k5eAaPmNgInS
na	na	k7c4
západ	západ	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
bývalé	bývalý	k2eAgFnSc2d1
zadní	zadní	k2eAgFnSc2d1
předsíně	předsíň	k1gFnSc2
se	s	k7c7
dvěma	dva	k4xCgFnPc7
malými	malá	k1gFnPc7
a	a	k8xC
jedněmi	jeden	k4xCgFnPc7
velkými	velký	k2eAgFnPc7d1
dveřmi	dveře	k1gFnPc7
vcházelo	vcházet	k5eAaImAgNnS
do	do	k7c2
hlavní	hlavní	k2eAgFnSc2d1
síně	síň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Severně	severně	k6eAd1
v	v	k7c6
hlavní	hlavní	k2eAgFnSc6d1
síní	síň	k1gFnSc7
byla	být	k5eAaImAgFnS
umístěna	umístit	k5eAaPmNgFnS
kazatelna	kazatelna	k1gFnSc1
<g/>
,	,	kIx,
jižně	jižně	k6eAd1
biskupské	biskupský	k2eAgNnSc4d1
křeslo	křeslo	k1gNnSc4
a	a	k8xC
v	v	k7c6
apsidě	apsida	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
vybudována	vybudovat	k5eAaPmNgFnS
do	do	k7c2
původní	původní	k2eAgFnSc2d1
předsíně	předsíň	k1gFnSc2
na	na	k7c6
východní	východní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
<g/>
,	,	kIx,
stál	stát	k5eAaImAgInS
oltář	oltář	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sloupy	sloup	k1gInPc4
byly	být	k5eAaImAgInP
jinak	jinak	k6eAd1
uspořádány	uspořádat	k5eAaPmNgInP
a	a	k8xC
byla	být	k5eAaImAgFnS
zřízena	zřídit	k5eAaPmNgFnS
galerie	galerie	k1gFnSc1
pro	pro	k7c4
ženy	žena	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastropení	zastropení	k1gNnSc1
bylo	být	k5eAaImAgNnS
tehdy	tehdy	k6eAd1
klenuté	klenutý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
stěny	stěna	k1gFnPc4
byly	být	k5eAaImAgFnP
doplněny	doplnit	k5eAaPmNgFnP
křesťanské	křesťanský	k2eAgFnPc1d1
malby	malba	k1gFnPc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Více	hodně	k6eAd2
o	o	k7c6
původní	původní	k2eAgFnSc6d1
architektonické	architektonický	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
viz	vidět	k5eAaImRp2nS
níže	nízce	k6eAd2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
Roku	rok	k1gInSc2
1204	#num#	k4
byl	být	k5eAaImAgInS
chrám	chrám	k1gInSc1
odevzdán	odevzdán	k2eAgInSc1d1
římské	římský	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
strana	strana	k1gFnSc1
chrámu	chrám	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
značně	značně	k6eAd1
utrpěla	utrpět	k5eAaPmAgFnS
výbuchem	výbuch	k1gInSc7
roku	rok	k1gInSc2
1687	#num#	k4
<g/>
.	.	kIx.
<g/>
Roku	rok	k1gInSc2
1460	#num#	k4
<g/>
,	,	kIx,
potom	potom	k6eAd1
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
se	se	k3xPyFc4
Řecka	Řecko	k1gNnSc2
zmocnila	zmocnit	k5eAaPmAgFnS
Osmanská	osmanský	k2eAgFnSc1d1
říše	říše	k1gFnSc1
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
chrám	chrám	k1gInSc1
přeměněn	přeměnit	k5eAaPmNgInS
na	na	k7c4
mešitu	mešita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turci	Turek	k1gMnPc1
sochařské	sochařský	k2eAgFnSc6d1
výzdobě	výzdoba	k1gFnSc6
neublížili	ublížit	k5eNaPmAgMnP
a	a	k8xC
jejich	jejich	k3xOp3gFnPc1
stavební	stavební	k2eAgFnPc1d1
úpravy	úprava	k1gFnPc1
se	se	k3xPyFc4
omezily	omezit	k5eAaPmAgFnP
pouze	pouze	k6eAd1
na	na	k7c4
přístavbu	přístavba	k1gFnSc4
minaretu	minaret	k1gInSc2
u	u	k7c2
jihozápadního	jihozápadní	k2eAgInSc2d1
rohu	roh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1687	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
největšímu	veliký	k2eAgNnSc3d3
poškození	poškození	k1gNnSc3
chrámu	chrám	k1gInSc2
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
Athén	Athéna	k1gFnPc2
tehdy	tehdy	k6eAd1
vnikl	vniknout	k5eAaPmAgMnS
s	s	k7c7
benátským	benátský	k2eAgNnSc7d1
vojskem	vojsko	k1gNnSc7
hrabě	hrabě	k1gMnSc1
Königsmark	Königsmark	k1gInSc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
podvelitel	podvelitel	k1gMnSc1
vojevůdce	vojevůdce	k1gMnSc2
Francesca	Francescum	k1gNnSc2
Morosiniho	Morosini	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Turecká	turecký	k2eAgFnSc1d1
posádka	posádka	k1gFnSc1
se	se	k3xPyFc4
i	i	k9
se	s	k7c7
zásobami	zásoba	k1gFnPc7
prachu	prach	k1gInSc2
opevnila	opevnit	k5eAaPmAgNnP
na	na	k7c4
Akropoli	Akropole	k1gFnSc4
a	a	k8xC
za	za	k7c4
prachárnu	prachárna	k1gFnSc4
zvolila	zvolit	k5eAaPmAgFnS
právě	právě	k9
Parthenón	Parthenón	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
jej	on	k3xPp3gMnSc4
benátská	benátský	k2eAgNnPc1d1
vojska	vojsko	k1gNnPc1
začala	začít	k5eAaPmAgNnP
ostřelovat	ostřelovat	k5eAaImF
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
jejich	jejich	k3xOp3gMnPc1
velitelé	velitel	k1gMnPc1
význam	význam	k1gInSc4
stavby	stavba	k1gFnSc2
znali	znát	k5eAaImAgMnP
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1687	#num#	k4
zasáhla	zasáhnout	k5eAaPmAgFnS
jedna	jeden	k4xCgFnSc1
benátská	benátský	k2eAgFnSc1d1
střela	střela	k1gFnSc1
Parthenón	Parthenón	k1gInSc4
<g/>
,	,	kIx,
pronikla	proniknout	k5eAaPmAgFnS
jeho	jeho	k3xOp3gNnSc1
střechou	střecha	k1gFnSc7
a	a	k8xC
způsobila	způsobit	k5eAaPmAgFnS
výbuch	výbuch	k1gInSc4
uloženého	uložený	k2eAgInSc2d1
prachu	prach	k1gInSc2
<g/>
.	.	kIx.
300	#num#	k4
lidí	člověk	k1gMnPc2
přišlo	přijít	k5eAaPmAgNnS
o	o	k7c4
život	život	k1gInSc4
a	a	k8xC
stavba	stavba	k1gFnSc1
se	se	k3xPyFc4
rozbortila	rozbortit	k5eAaPmAgFnS
na	na	k7c4
dvě	dva	k4xCgFnPc4
části	část	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
tři	tři	k4xCgInPc4
dny	den	k1gInPc4
s	s	k7c7
turecký	turecký	k2eAgMnSc1d1
velitel	velitel	k1gMnSc1
vzdal	vzdát	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
památku	památka	k1gFnSc4
vítězství	vítězství	k1gNnSc2
chtěl	chtít	k5eAaImAgMnS
velitel	velitel	k1gMnSc1
Morosini	Morosin	k2eAgMnPc1d1
odvézt	odvézt	k5eAaPmF
sochařskou	sochařský	k2eAgFnSc4d1
výzdobu	výzdoba	k1gFnSc4
západního	západní	k2eAgInSc2d1
štítu	štít	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
díky	díky	k7c3
neobratné	obratný	k2eNgFnSc3d1
manipulaci	manipulace	k1gFnSc3
se	se	k3xPyFc4
střední	střední	k2eAgFnSc1d1
část	část	k1gFnSc1
štítu	štít	k1gInSc2
zřítila	zřítit	k5eAaPmAgFnS
a	a	k8xC
mramorové	mramorový	k2eAgFnSc2d1
plastiky	plastika	k1gFnSc2
se	se	k3xPyFc4
roztříštily	roztříštit	k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
části	část	k1gFnSc2
posbírali	posbírat	k5eAaPmAgMnP
benátští	benátský	k2eAgMnPc1d1
vojáci	voják	k1gMnPc1
a	a	k8xC
odvezli	odvézt	k5eAaPmAgMnP
je	on	k3xPp3gMnPc4
do	do	k7c2
různých	různý	k2eAgFnPc2d1
částí	část	k1gFnPc2
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
byl	být	k5eAaImAgInS
počátek	počátek	k1gInSc1
rozptýlení	rozptýlení	k1gNnSc2
parthenónských	parthenónský	k2eAgInPc2d1
mramorů	mramor	k1gInPc2
po	po	k7c6
evropských	evropský	k2eAgFnPc6d1
sbírkách	sbírka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
ve	v	k7c6
válce	válka	k1gFnSc6
o	o	k7c4
osvobození	osvobození	k1gNnSc4
Řecka	Řecko	k1gNnSc2
utrpěl	utrpět	k5eAaPmAgMnS
Parthenón	Parthenón	k1gInSc4
v	v	k7c6
letech	léto	k1gNnPc6
1826	#num#	k4
a	a	k8xC
1827	#num#	k4
jisté	jistý	k2eAgNnSc1d1
poškození	poškození	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potom	potom	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
Řecko	Řecko	k1gNnSc1
získalo	získat	k5eAaPmAgNnS
nezávislost	nezávislost	k1gFnSc4
na	na	k7c6
Osmanské	osmanský	k2eAgFnSc6d1
říši	říš	k1gFnSc6
(	(	kIx(
<g/>
1829	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nastala	nastat	k5eAaPmAgFnS
nová	nový	k2eAgFnSc1d1
epocha	epocha	k1gFnSc1
v	v	k7c6
dějinách	dějiny	k1gFnPc6
této	tento	k3xDgFnSc2
stavby	stavba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zvědavci	zvědavec	k1gMnPc1
<g/>
,	,	kIx,
sběratelé	sběratel	k1gMnPc1
a	a	k8xC
učenci	učenec	k1gMnPc1
</s>
<s>
Věhlas	věhlas	k1gInSc1
Řecka	Řecko	k1gNnSc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
kultury	kultura	k1gFnSc2
nabyl	nabýt	k5eAaPmAgInS
obrovských	obrovský	k2eAgInPc2d1
rozměrů	rozměr	k1gInPc2
již	již	k6eAd1
v	v	k7c6
antice	antika	k1gFnSc6
a	a	k8xC
přetrval	přetrvat	k5eAaPmAgInS
i	i	k9
její	její	k3xOp3gInSc1
konec	konec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
to	ten	k3xDgNnSc4
především	především	k9
spisy	spis	k1gInPc4
římských	římský	k2eAgMnPc2d1
autorů	autor	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yRgMnPc4,k3yQgMnPc4,k3yIgMnPc4
zprostředkovávaly	zprostředkovávat	k5eAaImAgInP
ve	v	k7c6
středověku	středověk	k1gInSc6
informace	informace	k1gFnSc2
o	o	k7c6
antickém	antický	k2eAgNnSc6d1
Řecku	Řecko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
vykopávky	vykopávka	k1gFnPc4
a	a	k8xC
studium	studium	k1gNnSc4
architektonických	architektonický	k2eAgFnPc2d1
památek	památka	k1gFnPc2
tenkrát	tenkrát	k6eAd1
nikdo	nikdo	k3yNnSc1
nepomyslel	pomyslet	k5eNaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Situace	situace	k1gFnSc1
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
měnit	měnit	k5eAaImF
s	s	k7c7
nástupem	nástup	k1gInSc7
renesance	renesance	k1gFnSc2
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
vzbudila	vzbudit	k5eAaPmAgFnS
zájem	zájem	k1gInSc4
o	o	k7c4
antické	antický	k2eAgFnPc4d1
památky	památka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sběratelé	sběratel	k1gMnPc1
starožitností	starožitnost	k1gFnPc2
a	a	k8xC
umělci	umělec	k1gMnPc1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
např.	např.	kA
Michelangelo	Michelangela	k1gFnSc5
Buonarroti	Buonarrot	k1gMnPc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
soustředili	soustředit	k5eAaPmAgMnP
na	na	k7c4
dostupnější	dostupný	k2eAgFnPc4d2
památky	památka	k1gFnPc4
na	na	k7c6
území	území	k1gNnSc6
Itálie	Itálie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řecko	Řecko	k1gNnSc1
bylo	být	k5eAaImAgNnS
součástí	součást	k1gFnSc7
Osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
omezovalo	omezovat	k5eAaImAgNnS
možnosti	možnost	k1gFnPc4
badatelů	badatel	k1gMnPc2
z	z	k7c2
křesťanské	křesťanský	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
do	do	k7c2
země	zem	k1gFnSc2
vypravovali	vypravovat	k5eAaImAgMnP
zejména	zejména	k9
dobrodružní	dobrodružný	k2eAgMnPc1d1
kupci	kupec	k1gMnPc1
a	a	k8xC
cestovatelé	cestovatel	k1gMnPc1
bez	bez	k7c2
odborného	odborný	k2eAgNnSc2d1
vzdělání	vzdělání	k1gNnSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
např.	např.	kA
Cyriacus	Cyriacus	k1gInSc1
z	z	k7c2
Ancony	Ancona	k1gFnSc2
v	v	k7c6
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
V	v	k7c6
samotném	samotný	k2eAgNnSc6d1
Řecku	Řecko	k1gNnSc6
existovaly	existovat	k5eAaImAgFnP
řídké	řídký	k2eAgInPc4d1
pokusy	pokus	k1gInPc4
nalézt	nalézt	k5eAaBmF,k5eAaPmF
ztracená	ztracený	k2eAgNnPc4d1
města	město	k1gNnPc4
podle	podle	k7c2
Strabónových	Strabónový	k2eAgInPc2d1
a	a	k8xC
Pausániových	Pausániový	k2eAgInPc2d1
textů	text	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnPc4
vzdělaní	vzdělaný	k2eAgMnPc1d1
cestovatelé	cestovatel	k1gMnPc1
se	se	k3xPyFc4
do	do	k7c2
Řecka	Řecko	k1gNnSc2
vydali	vydat	k5eAaPmAgMnP
koncem	koncem	k7c2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
o	o	k7c4
sto	sto	k4xCgNnSc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
konečně	konečně	k6eAd1
vzbudily	vzbudit	k5eAaPmAgFnP
v	v	k7c6
Evropě	Evropa	k1gFnSc6
nádherné	nádherný	k2eAgFnSc2d1
kresby	kresba	k1gFnSc2
architektonických	architektonický	k2eAgFnPc2d1
památek	památka	k1gFnPc2
zájem	zájem	k1gInSc4
o	o	k7c4
Řecko	Řecko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
těch	ten	k3xDgMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
do	do	k7c2
doby	doba	k1gFnSc2
osvobození	osvobození	k1gNnSc2
Řecka	Řecko	k1gNnSc2
navštívili	navštívit	k5eAaPmAgMnP
Parthenón	Parthenón	k1gInSc4
<g/>
,	,	kIx,
vystupují	vystupovat	k5eAaImIp3nP
do	do	k7c2
popředí	popředí	k1gNnSc2
dvě	dva	k4xCgFnPc4
osobnosti	osobnost	k1gFnPc4
<g/>
:	:	kIx,
Jacques	Jacques	k1gMnSc1
Carrey	Carrea	k1gFnSc2
a	a	k8xC
Thomas	Thomas	k1gMnSc1
Bruce	Bruce	k1gMnSc1
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
hrabě	hrabě	k1gMnSc1
z	z	k7c2
Elginu	Elgin	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Jacques	Jacques	k1gMnSc1
Carrey	Carrea	k1gFnSc2
byl	být	k5eAaImAgMnS
vlámský	vlámský	k2eAgMnSc1d1
malíř	malíř	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
doprovázel	doprovázet	k5eAaImAgMnS
francouzského	francouzský	k2eAgMnSc4d1
velvyslance	velvyslanec	k1gMnSc4
u	u	k7c2
tureckého	turecký	k2eAgInSc2d1
dvora	dvůr	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1674	#num#	k4
se	se	k3xPyFc4
vydal	vydat	k5eAaPmAgInS
do	do	k7c2
Athén	Athéna	k1gFnPc2
a	a	k8xC
pořídil	pořídit	k5eAaPmAgMnS
jednoduché	jednoduchý	k2eAgFnPc4d1
kresby	kresba	k1gFnPc4
prakticky	prakticky	k6eAd1
celé	celý	k2eAgFnPc4d1
tehdy	tehdy	k6eAd1
uchované	uchovaný	k2eAgFnPc4d1
sochařské	sochařský	k2eAgFnPc4d1
výzdoby	výzdoba	k1gFnPc4
Parthenónu	Parthenón	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
skici	skica	k1gFnPc1
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
nás	my	k3xPp1nPc4
velkým	velký	k2eAgInSc7d1
přínosem	přínos	k1gInSc7
<g/>
,	,	kIx,
neboť	neboť	k8xC
krátce	krátce	k6eAd1
potom	potom	k6eAd1
byl	být	k5eAaImAgInS
chrám	chrám	k1gInSc1
za	za	k7c2
benátského	benátský	k2eAgInSc2d1
útoku	útok	k1gInSc2
velmi	velmi	k6eAd1
poškozen	poškodit	k5eAaPmNgInS
výbuchem	výbuch	k1gInSc7
prachárny	prachárna	k1gFnSc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
výše	výše	k1gFnSc2,k1gFnSc2wB
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Lord	lord	k1gMnSc1
Elgin	Elgin	k1gMnSc1
byl	být	k5eAaImAgMnS
anglický	anglický	k2eAgMnSc1d1
velvyslanec	velvyslanec	k1gMnSc1
v	v	k7c6
Cařihradě	Cařihrad	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1800	#num#	k4
získal	získat	k5eAaPmAgMnS
od	od	k7c2
sultána	sultán	k1gMnSc2
povolení	povolení	k1gNnSc2
odvézt	odvézt	k5eAaPmF
„	„	k?
<g/>
nějaké	nějaký	k3yIgInPc4
kameny	kámen	k1gInPc4
<g/>
“	“	k?
z	z	k7c2
trosek	troska	k1gFnPc2
Parthenónu	Parthenón	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
byla	být	k5eAaImAgNnP
do	do	k7c2
Londýna	Londýn	k1gInSc2
odvezena	odvezen	k2eAgFnSc1d1
velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
zbývající	zbývající	k2eAgFnSc2d1
sochařské	sochařský	k2eAgFnSc2d1
výzdoby	výzdoba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anglický	anglický	k2eAgMnSc1d1
básník	básník	k1gMnSc1
lord	lord	k1gMnSc1
Byron	Byron	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
si	se	k3xPyFc3
Řecko	Řecko	k1gNnSc4
oblíbil	oblíbit	k5eAaPmAgMnS
<g/>
,	,	kIx,
to	ten	k3xDgNnSc4
tehdy	tehdy	k6eAd1
označil	označit	k5eAaPmAgMnS
za	za	k7c4
barbarský	barbarský	k2eAgInSc4d1
čin	čin	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
důsledky	důsledek	k1gInPc1
tohoto	tento	k3xDgInSc2
činu	čin	k1gInSc2
nebyly	být	k5eNaImAgInP
dosud	dosud	k6eAd1
napraveny	napraven	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Probíhající	probíhající	k2eAgFnSc1d1
rekonstrukce	rekonstrukce	k1gFnSc1
chrámu	chrám	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Současnost	současnost	k1gFnSc1
chrámu	chrám	k1gInSc2
</s>
<s>
Potom	potom	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
Řekové	Řek	k1gMnPc1
získali	získat	k5eAaPmAgMnP
v	v	k7c6
roce	rok	k1gInSc6
1832	#num#	k4
ve	v	k7c6
válce	válka	k1gFnSc6
o	o	k7c4
nezávislost	nezávislost	k1gFnSc4
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
Athénami	Athéna	k1gFnPc7
<g/>
,	,	kIx,
počali	počnout	k5eAaPmAgMnP
se	se	k3xPyFc4
o	o	k7c4
Akropolis	Akropolis	k1gFnSc4
a	a	k8xC
Parthenón	Parthenón	k1gInSc4
starat	starat	k5eAaImF
jako	jako	k9
o	o	k7c4
svůj	svůj	k3xOyFgInSc4
národní	národní	k2eAgInSc4d1
symbol	symbol	k1gInSc4
<g/>
,	,	kIx,
památník	památník	k1gInSc4
veliké	veliký	k2eAgFnSc2d1
minulosti	minulost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Záštitu	záštita	k1gFnSc4
nad	nad	k7c7
ním	on	k3xPp3gInSc7
převzala	převzít	k5eAaPmAgFnS
řecká	řecký	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ihned	ihned	k6eAd1
byly	být	k5eAaImAgFnP
odstraněny	odstraněn	k2eAgFnPc1d1
středověké	středověký	k2eAgFnPc1d1
a	a	k8xC
turecké	turecký	k2eAgFnPc1d1
stavby	stavba	k1gFnPc1
z	z	k7c2
Akropole	Akropole	k1gFnSc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
minaretu	minaret	k1gInSc2
a	a	k8xC
mešity	mešita	k1gFnSc2
vestavěné	vestavěný	k2eAgFnSc2d1
do	do	k7c2
trosek	troska	k1gFnPc2
Parthenónu	Parthenón	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
počátku	počátek	k1gInSc2
novodobé	novodobý	k2eAgFnSc2d1
státní	státní	k2eAgFnSc2d1
suverenity	suverenita	k1gFnSc2
Řecka	Řecko	k1gNnSc2
byly	být	k5eAaImAgFnP
snahy	snaha	k1gFnPc1
o	o	k7c4
odbornou	odborný	k2eAgFnSc4d1
rekonstrukci	rekonstrukce	k1gFnSc4
nebo	nebo	k8xC
konzervaci	konzervace	k1gFnSc4
chrámu	chrám	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
však	však	k9
ztroskotávaly	ztroskotávat	k5eAaImAgFnP
na	na	k7c6
nedostatku	nedostatek	k1gInSc6
prostředků	prostředek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parthenón	Parthenón	k1gInSc1
byl	být	k5eAaImAgInS
přesto	přesto	k8xC
několikrát	několikrát	k6eAd1
opravován	opravován	k2eAgMnSc1d1
<g/>
,	,	kIx,
avšak	avšak	k8xC
ne	ne	k9
vždy	vždy	k6eAd1
na	na	k7c6
dostatečně	dostatečně	k6eAd1
odborné	odborný	k2eAgFnSc6d1
výši	výše	k1gFnSc6,k1gFnSc6wB
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
na	na	k7c6
konci	konec	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
vyzvala	vyzvat	k5eAaPmAgFnS
vláda	vláda	k1gFnSc1
Řecka	Řecko	k1gNnSc2
všechny	všechen	k3xTgInPc1
národy	národ	k1gInPc1
Evropy	Evropa	k1gFnSc2
ke	k	k7c3
společnému	společný	k2eAgNnSc3d1
úsilí	úsilí	k1gNnSc3
na	na	k7c6
opravě	oprava	k1gFnSc6
této	tento	k3xDgFnSc2
význačné	význačný	k2eAgFnSc2d1
stavby	stavba	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
ale	ale	k9
nemohly	moct	k5eNaImAgFnP
mít	mít	k5eAaImF
takové	takový	k3xDgFnPc4
snahy	snaha	k1gFnPc4
naději	nadát	k5eAaBmIp1nS
na	na	k7c4
úspěch	úspěch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1975	#num#	k4
začala	začít	k5eAaPmAgFnS
řecká	řecký	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
vyvíjet	vyvíjet	k5eAaImF
soustředěnou	soustředěný	k2eAgFnSc4d1
snahu	snaha	k1gFnSc4
o	o	k7c4
obnovu	obnova	k1gFnSc4
Parthenónu	Parthenón	k1gInSc2
a	a	k8xC
Akropole	Akropole	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
projekt	projekt	k1gInSc1
získal	získat	k5eAaPmAgInS
v	v	k7c6
pozdější	pozdní	k2eAgFnSc6d2
době	doba	k1gFnSc6
podporu	podpora	k1gFnSc4
z	z	k7c2
fondů	fond	k1gInPc2
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
zřízena	zřízen	k2eAgFnSc1d1
architektonická	architektonický	k2eAgFnSc1d1
komise	komise	k1gFnSc1
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
úkolem	úkol	k1gInSc7
bylo	být	k5eAaImAgNnS
dokumentovat	dokumentovat	k5eAaBmF
rozmístění	rozmístění	k1gNnSc1
všech	všecek	k3xTgMnPc2
artefaktů	artefakt	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
zůstaly	zůstat	k5eAaPmAgInP
na	na	k7c6
místě	místo	k1gNnSc6
a	a	k8xC
jejich	jejich	k3xOp3gInSc1
pravděpodobný	pravděpodobný	k2eAgInSc1d1
původ	původ	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomuto	tento	k3xDgInSc3
účelu	účel	k1gInSc3
začaly	začít	k5eAaPmAgInP
být	být	k5eAaImF
později	pozdě	k6eAd2
využívány	využíván	k2eAgFnPc4d1
počítačové	počítačový	k2eAgFnPc4d1
simulace	simulace	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Komise	komise	k1gFnSc1
zjistila	zjistit	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
některých	některý	k3yIgFnPc6
otázkách	otázka	k1gFnPc6
nebyly	být	k5eNaImAgFnP
předchozí	předchozí	k2eAgFnPc1d1
rekonstrukce	rekonstrukce	k1gFnPc1
správné	správný	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Cílem	cíl	k1gInSc7
rekonstrukce	rekonstrukce	k1gFnSc2
není	být	k5eNaImIp3nS
navrácení	navrácení	k1gNnSc1
Parthenónu	Parthenón	k1gInSc2
do	do	k7c2
stavu	stav	k1gInSc2
před	před	k7c7
explozí	exploze	k1gFnSc7
roku	rok	k1gInSc2
1687	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
s	s	k7c7
pomocí	pomoc	k1gFnSc7
mramoru	mramor	k1gInSc2
z	z	k7c2
původních	původní	k2eAgInPc2d1
lomů	lom	k1gInPc2
oprava	oprava	k1gFnSc1
částí	část	k1gFnPc2
důležitých	důležitý	k2eAgFnPc2d1
z	z	k7c2
hlediska	hledisko	k1gNnSc2
stability	stabilita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nesprávné	správný	k2eNgInPc1d1
rekonstrukční	rekonstrukční	k2eAgInPc1d1
zásahy	zásah	k1gInPc1
byly	být	k5eAaImAgInP
odstraněny	odstranit	k5eAaPmNgInP
a	a	k8xC
velké	velká	k1gFnPc1
originální	originální	k2eAgFnPc1d1
části	část	k1gFnPc1
mramoru	mramor	k1gInSc2
jsou	být	k5eAaImIp3nP
postupně	postupně	k6eAd1
vraceny	vracen	k2eAgInPc1d1
na	na	k7c4
původní	původní	k2eAgNnSc4d1
místo	místo	k1gNnSc4
i	i	k9
pomocí	pomocí	k7c2
moderních	moderní	k2eAgInPc2d1
stavebních	stavební	k2eAgInPc2d1
materiálů	materiál	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křehké	křehký	k2eAgFnPc4d1
části	část	k1gFnPc4
výzdoby	výzdoba	k1gFnSc2
byly	být	k5eAaImAgInP
staženy	stáhnout	k5eAaPmNgInP
a	a	k8xC
uloženy	uložit	k5eAaPmNgInP
v	v	k7c6
akropolském	akropolský	k2eAgNnSc6d1
muzeu	muzeum	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3
nebezpečím	nebezpečí	k1gNnSc7
pro	pro	k7c4
současný	současný	k2eAgInSc4d1
Parthenón	Parthenón	k1gInSc4
je	být	k5eAaImIp3nS
již	již	k6eAd1
150	#num#	k4
let	léto	k1gNnPc2
trvající	trvající	k2eAgFnSc1d1
zvýšená	zvýšený	k2eAgFnSc1d1
eroze	eroze	k1gFnSc1
způsobená	způsobený	k2eAgFnSc1d1
exhalacemi	exhalace	k1gFnPc7
z	z	k7c2
otopu	otop	k1gInSc2
černým	černý	k2eAgNnSc7d1
uhlím	uhlí	k1gNnSc7
a	a	k8xC
později	pozdě	k6eAd2
z	z	k7c2
výfuků	výfuk	k1gInPc2
aut	auto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Architektonická	architektonický	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
</s>
<s>
Umístění	umístění	k1gNnSc1
v	v	k7c6
prostoru	prostor	k1gInSc6
</s>
<s>
Parthenón	Parthenón	k1gInSc1
stojí	stát	k5eAaImIp3nS
u	u	k7c2
jižního	jižní	k2eAgInSc2d1
okraje	okraj	k1gInSc2
Akropole	Akropole	k1gFnSc2
asi	asi	k9
35	#num#	k4
metrů	metr	k1gInPc2
od	od	k7c2
v	v	k7c6
době	doba	k1gFnSc6
jeho	jeho	k3xOp3gFnSc2
stavby	stavba	k1gFnSc2
stojícího	stojící	k2eAgInSc2d1
starého	starý	k2eAgInSc2d1
chrámu	chrám	k1gInSc2
Athény	Athéna	k1gFnSc2
–	–	k?
Hekatompedonu	Hekatompedona	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
jižního	jižní	k2eAgInSc2d1
okraje	okraj	k1gInSc2
výšiny	výšina	k1gFnSc2
je	být	k5eAaImIp3nS
vzdálen	vzdálit	k5eAaPmNgInS
rovněž	rovněž	k9
35	#num#	k4
–	–	k?
40	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Delší	dlouhý	k2eAgFnSc7d2
stranou	strana	k1gFnSc7
svého	svůj	k3xOyFgInSc2
obdélníkového	obdélníkový	k2eAgInSc2d1
půdorysu	půdorys	k1gInSc2
je	být	k5eAaImIp3nS
orientován	orientován	k2eAgInSc1d1
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
Hekatompedon	Hekatompedon	k1gInSc1
<g/>
,	,	kIx,
severo-východo-východním	severo-východo-východní	k2eAgInSc7d1
směrem	směr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
průčelí	průčelí	k1gNnSc4
v	v	k7c6
tomto	tento	k3xDgInSc6
směru	směr	k1gInSc6
byt	byt	k1gInSc1
také	také	k6eAd1
původně	původně	k6eAd1
umístěn	umístěn	k2eAgInSc1d1
vchod	vchod	k1gInSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
na	na	k7c6
opačné	opačný	k2eAgFnSc6d1
straně	strana	k1gFnSc6
od	od	k7c2
hlavního	hlavní	k2eAgInSc2d1
vstupu	vstup	k1gInSc2
na	na	k7c4
Akropol	Akropol	k1gInSc4
–	–	k?
Propylají	Propylají	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starověký	starověký	k2eAgInSc1d1
návštěvník	návštěvník	k1gMnSc1
Akropole	Akropole	k1gFnPc4
tedy	tedy	k9
po	po	k7c6
opuštění	opuštění	k1gNnSc6
Propylají	Propylají	k1gMnSc1
spatřil	spatřit	k5eAaPmAgMnS
jeho	jeho	k3xOp3gNnPc4
zadní	zadní	k2eAgNnPc4d1
průčelí	průčelí	k1gNnPc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
bylo	být	k5eAaImAgNnS
částečně	částečně	k6eAd1
zakryto	zakrýt	k5eAaPmNgNnS
dalšími	další	k2eAgFnPc7d1
stavbami	stavba	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Plán	plán	k1gInSc1
Akropole	Akropole	k1gFnSc1
<g/>
,	,	kIx,
Larousse	Larousse	k1gFnSc1
1922	#num#	k4
</s>
<s>
Chrámová	chrámový	k2eAgFnSc1d1
budova	budova	k1gFnSc1
</s>
<s>
Budova	budova	k1gFnSc1
chrámu	chrám	k1gInSc2
je	být	k5eAaImIp3nS
69,51	69,51	k4
m	m	kA
dlouhá	dlouhý	k2eAgFnSc1d1
30,86	30,86	k4
m	m	kA
široká	široký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
46	#num#	k4
dórských	dórský	k2eAgInPc2d1
sloupů	sloup	k1gInPc2
umístěných	umístěný	k2eAgInPc2d1
po	po	k7c6
svém	svůj	k3xOyFgInSc6
obvodu	obvod	k1gInSc6
<g/>
,	,	kIx,
z	z	k7c2
čehož	což	k3yRnSc2,k3yQnSc2
v	v	k7c6
průčelí	průčelí	k1gNnSc6
je	být	k5eAaImIp3nS
po	po	k7c6
osmi	osm	k4xCc6
sloupech	sloup	k1gInPc6
a	a	k8xC
na	na	k7c6
stranách	strana	k1gFnPc6
po	po	k7c6
sedmnácti	sedmnáct	k4xCc6
–	–	k?
peripteros	peripterosa	k1gFnPc2
oktastylos	oktastylos	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sloupy	sloup	k1gInPc7
mají	mít	k5eAaImIp3nP
průměr	průměr	k1gInSc4
1,9	1,9	k4
m	m	kA
a	a	k8xC
jsou	být	k5eAaImIp3nP
10,4	10,4	k4
m	m	kA
vysoké	vysoká	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podstava	podstava	k1gFnSc1
budovy	budova	k1gFnSc2
–	–	k?
krepidóma	krepidóma	k1gFnSc1
má	mít	k5eAaImIp3nS
tři	tři	k4xCgInPc4
stupně	stupeň	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1
část	část	k1gFnSc1
chrámu	chrám	k1gInSc2
–	–	k?
náos	náos	k6eAd1
má	mít	k5eAaImIp3nS
v	v	k7c6
obou	dva	k4xCgNnPc6
průčelích	průčelí	k1gNnPc6
po	po	k7c6
šesti	šest	k4xCc6
dórských	dórský	k2eAgInPc6d1
sloupech	sloup	k1gInPc6
(	(	kIx(
<g/>
amfiprostylos	amfiprostylos	k1gMnSc1
hexastylos	hexastylos	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
měří	měřit	k5eAaImIp3nS
59,02	59,02	k4
:	:	kIx,
21,72	21,72	k4
m.	m.	k?
Skládá	skládat	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
obvyklých	obvyklý	k2eAgFnPc2d1
tří	tři	k4xCgFnPc2
částí	část	k1gFnPc2
<g/>
:	:	kIx,
z	z	k7c2
předsíně	předsíň	k1gFnSc2
(	(	kIx(
<g/>
pronáos	pronáos	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ze	z	k7c2
svatyně	svatyně	k1gFnSc2
(	(	kIx(
<g/>
náos	náos	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
ze	z	k7c2
zadní	zadní	k2eAgFnSc2d1
síně	síň	k1gFnSc2
(	(	kIx(
<g/>
opisthodomos	opisthodomos	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Materiálem	materiál	k1gInSc7
použitým	použitý	k2eAgInSc7d1
ke	k	k7c3
stavbě	stavba	k1gFnSc3
je	být	k5eAaImIp3nS
mramor	mramor	k1gInSc1
z	z	k7c2
nedalekých	daleký	k2eNgInPc2d1
lomů	lom	k1gInPc2
v	v	k7c4
Penteliku	Pentelika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Podoba	podoba	k1gFnSc1
místností	místnost	k1gFnPc2
</s>
<s>
Svatyně	svatyně	k1gFnSc1
byla	být	k5eAaImAgFnS
ještě	ještě	k6eAd1
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c4
dvě	dva	k4xCgFnPc4
místnosti	místnost	k1gFnPc4
ve	v	k7c6
větší	veliký	k2eAgFnSc6d2
východní	východní	k2eAgFnSc6d1
svatyni	svatyně	k1gFnSc6
a	a	k8xC
menší	malý	k2eAgFnSc3d2
západní	západní	k2eAgFnSc3d1
pokladnici	pokladnice	k1gFnSc3
–	–	k?
parthenón	parthenón	k1gMnSc1
v	v	k7c6
užším	úzký	k2eAgInSc6d2
smyslu	smysl	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Východní	východní	k2eAgFnSc1d1
část	část	k1gFnSc1
byla	být	k5eAaImAgFnS
19,22	19,22	k4
m	m	kA
široká	široký	k2eAgFnSc1d1
a	a	k8xC
29,22	29,22	k4
m	m	kA
dlouhá	dlouhý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Této	tento	k3xDgFnSc2
části	část	k1gFnSc2
se	se	k3xPyFc4
ve	v	k7c6
starověku	starověk	k1gInSc6
říkalo	říkat	k5eAaImAgNnS
Nový	nový	k2eAgInSc4d1
chrám	chrám	k1gInSc4
sta	sto	k4xCgNnPc4
stop	stopa	k1gFnPc2
(	(	kIx(
<g/>
Hekatompedos	Hekatompedosa	k1gFnPc2
<g/>
)	)	kIx)
díky	díky	k7c3
rozměrům	rozměr	k1gInPc3
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
ještě	ještě	k6eAd1
se	s	k7c7
stěnou	stěna	k1gFnSc7
odpovídají	odpovídat	k5eAaImIp3nP
stu	sto	k4xCgNnSc3
attických	attický	k2eAgFnPc2d1
stop	stopa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hekatompedos	Hekatompedos	k1gInSc1
byl	být	k5eAaImAgInS
rozdělen	rozdělit	k5eAaPmNgInS
na	na	k7c4
tři	tři	k4xCgFnPc4
lodi	loď	k1gFnPc4
dvěma	dva	k4xCgFnPc7
řadami	řada	k1gFnPc7
dórských	dórský	k2eAgInPc2d1
sloupů	sloup	k1gInPc2
po	po	k7c6
stranách	strana	k1gFnPc6
a	a	k8xC
třemi	tři	k4xCgInPc7
sloupy	sloup	k1gInPc7
proti	proti	k7c3
vnitřní	vnitřní	k2eAgFnSc3d1
stěně	stěna	k1gFnSc3
místnosti	místnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prostřední	prostřední	k2eAgFnSc6d1
lodi	loď	k1gFnSc6
byla	být	k5eAaImAgFnS
umístěna	umístit	k5eAaPmNgFnS
kultovní	kultovní	k2eAgFnSc1d1
socha	socha	k1gFnSc1
Athény	Athéna	k1gFnSc2
Parthenos	Parthenosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strop	strop	k1gInSc4
této	tento	k3xDgFnSc2
místnosti	místnost	k1gFnSc2
byl	být	k5eAaImAgInS
bohatě	bohatě	k6eAd1
malovaný	malovaný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
pokladnice	pokladnice	k1gFnSc2
Parthenónu	Parthenón	k1gInSc2
se	se	k3xPyFc4
vstupovalo	vstupovat	k5eAaImAgNnS
dveřmi	dveře	k1gFnPc7
ze	z	k7c2
zadní	zadní	k2eAgFnSc2d1
předsíně	předsíň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místnost	místnost	k1gFnSc1
byla	být	k5eAaImAgFnS
13,35	13,35	k4
m	m	kA
hluboká	hluboký	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
kamenný	kamenný	k2eAgInSc1d1
kazetový	kazetový	k2eAgInSc1d1
strop	strop	k1gInSc1
byl	být	k5eAaImAgInS
nesen	nést	k5eAaImNgInS
čtyřmi	čtyři	k4xCgInPc7
iónskými	iónský	k2eAgInPc7d1
sloupy	sloup	k1gInPc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
dělilo	dělit	k5eAaImAgNnS
místnost	místnost	k1gFnSc4
ve	v	k7c4
tři	tři	k4xCgFnPc4
lodi	loď	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Půdorys	půdorys	k1gInSc1
chrámu	chrám	k1gInSc2
v	v	k7c6
původní	původní	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
.	.	kIx.
1-	1-	k4
předsíň	předsíň	k1gFnSc1
(	(	kIx(
<g/>
pronáos	pronáos	k1gInSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
2-	2-	k4
hlavní	hlavní	k2eAgFnSc1d1
síň	síň	k1gFnSc1
(	(	kIx(
<g/>
náos	náos	k1gInSc1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
3-	3-	k4
podstavec	podstavec	k1gInSc1
kultovní	kultovní	k2eAgFnSc2d1
sochy	socha	k1gFnSc2
<g/>
;	;	kIx,
4-	4-	k4
parthenón	parthenón	k1gInSc4
v	v	k7c6
už	už	k6eAd1
<g/>
.	.	kIx.
smyslu	smysl	k1gInSc6
-	-	kIx~
pokladnice	pokladnice	k1gFnSc1
<g/>
;	;	kIx,
5	#num#	k4
-	-	kIx~
zadní	zadní	k2eAgFnSc1d1
předsíň	předsíň	k1gFnSc1
(	(	kIx(
<g/>
opisthodomos	opisthodomos	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Optické	optický	k2eAgFnPc1d1
korekce	korekce	k1gFnPc1
</s>
<s>
Při	při	k7c6
stavbě	stavba	k1gFnSc6
chrámu	chrám	k1gInSc2
bylo	být	k5eAaImAgNnS
dbáno	dbán	k2eAgNnSc1d1
na	na	k7c4
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
nejvyšší	vysoký	k2eAgInSc1d3
estetický	estetický	k2eAgInSc1d1
účinek	účinek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
uplatněna	uplatněn	k2eAgFnSc1d1
řada	řada	k1gFnSc1
optických	optický	k2eAgFnPc2d1
korekcí	korekce	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
zvyšují	zvyšovat	k5eAaImIp3nP
estetický	estetický	k2eAgInSc4d1
účinek	účinek	k1gInSc4
stavby	stavba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInSc1d1
stupeň	stupeň	k1gInSc1
podstavy	podstava	k1gFnSc2
chrámu	chrámat	k5eAaImIp1nS
–	–	k?
stylobat	stylobat	k5eAaImF,k5eAaPmF
se	se	k3xPyFc4
od	od	k7c2
středu	střed	k1gInSc2
svažuje	svažovat	k5eAaImIp3nS
k	k	k7c3
nárožím	nároží	k1gNnPc3
o	o	k7c6
60	#num#	k4
mm	mm	kA
a	a	k8xC
o	o	k7c4
110	#num#	k4
mm	mm	kA
k	k	k7c3
bočním	boční	k2eAgFnPc3d1
stranám	strana	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kladí	kladí	k1gNnSc1
jsou	být	k5eAaImIp3nP
zaoblena	zaoblen	k2eAgFnSc1d1
směrem	směr	k1gInSc7
ke	k	k7c3
středu	střed	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sloupy	sloup	k1gInPc7
jsou	být	k5eAaImIp3nP
mírně	mírně	k6eAd1
nakloněny	naklonit	k5eAaPmNgFnP
od	od	k7c2
středu	střed	k1gInSc2
a	a	k8xC
ty	ten	k3xDgMnPc4
na	na	k7c6
rozích	roh	k1gInPc6
jsou	být	k5eAaImIp3nP
o	o	k7c4
něco	něco	k3yInSc4
většího	veliký	k2eAgNnSc2d2
průměru	průměr	k1gInSc6
než	než	k8xS
ostatní	ostatní	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Dekorační	dekorační	k2eAgInPc1d1
prvky	prvek	k1gInPc1
</s>
<s>
Vnější	vnější	k2eAgInSc1d1
vlys	vlys	k1gInSc1
byl	být	k5eAaImAgInS
typickým	typický	k2eAgInSc7d1
vlysem	vlys	k1gInSc7
dórského	dórský	k2eAgInSc2d1
řádu	řád	k1gInSc2
a	a	k8xC
skládal	skládat	k5eAaImAgMnS
se	se	k3xPyFc4
z	z	k7c2
92	#num#	k4
metop	metopa	k1gFnPc2
(	(	kIx(
<g/>
desek	deska	k1gFnPc2
opatřených	opatřený	k2eAgFnPc2d1
výzdobou	výzdoba	k1gFnSc7
<g/>
)	)	kIx)
oddělených	oddělený	k2eAgFnPc2d1
triglyfy	triglyf	k1gInPc4
(	(	kIx(
<g/>
užší	úzký	k2eAgFnSc1d2
deska	deska	k1gFnSc1
se	s	k7c7
dvěma	dva	k4xCgInPc7
výžlabky	výžlabek	k1gInPc7
uprostřed	uprostřed	k6eAd1
a	a	k8xC
polovinou	polovina	k1gFnSc7
po	po	k7c6
stranách	strana	k1gFnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
každé	každý	k3xTgNnSc4
mezisloupí	mezisloupí	k1gNnSc4
připadaly	připadat	k5eAaPmAgInP,k5eAaImAgInP
dvě	dva	k4xCgFnPc4
metopy	metopa	k1gFnPc4
a	a	k8xC
dva	dva	k4xCgInPc4
triglyfy	triglyf	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
obou	dva	k4xCgInPc6
štítech	štít	k1gInPc6
byla	být	k5eAaImAgFnS
umístěna	umístěn	k2eAgFnSc1d1
bohatá	bohatý	k2eAgFnSc1d1
sochařská	sochařský	k2eAgFnSc1d1
výzdoba	výzdoba	k1gFnSc1
a	a	k8xC
další	další	k2eAgFnPc1d1
sochy	socha	k1gFnPc1
byly	být	k5eAaImAgFnP
umístěny	umístit	k5eAaPmNgFnP
na	na	k7c6
koncích	konec	k1gInPc6
střešního	střešní	k2eAgInSc2d1
hřebenu	hřeben	k1gInSc2
a	a	k8xC
v	v	k7c6
rozích	roh	k1gInPc6
štítů	štít	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vnitřní	vnitřní	k2eAgInSc1d1
vlys	vlys	k1gInSc1
kolem	kolem	k7c2
svatyně	svatyně	k1gFnSc2
byl	být	k5eAaImAgInS
iónský	iónský	k2eAgMnSc1d1
<g/>
,	,	kIx,
tzn.	tzn.	kA
nepřerušovaný	přerušovaný	k2eNgInSc4d1
triglyfy	triglyf	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
zmírnění	zmírnění	k1gNnSc4
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
iónského	iónský	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
byly	být	k5eAaImAgFnP
v	v	k7c6
pravidelných	pravidelný	k2eAgInPc6d1
intervalech	interval	k1gInPc6
umístěny	umístit	k5eAaPmNgFnP
krátké	krátký	k2eAgFnPc1d1
lišty	lišta	k1gFnPc1
s	s	k7c7
kapkami	kapka	k1gFnPc7
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
u	u	k7c2
dórského	dórský	k2eAgInSc2d1
vlysu	vlys	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Sochařská	sochařský	k2eAgFnSc1d1
výzdoba	výzdoba	k1gFnSc1
</s>
<s>
Sochařská	sochařský	k2eAgFnSc1d1
výzdoba	výzdoba	k1gFnSc1
Parthenónu	Parthenón	k1gInSc2
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
obsáhlá	obsáhlý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sestává	sestávat	k5eAaImIp3nS
sousoší	sousoší	k1gNnSc4
v	v	k7c6
obou	dva	k4xCgInPc6
štítech	štít	k1gInPc6
<g/>
,	,	kIx,
devadesáti	devadesát	k4xCc2
dvou	dva	k4xCgFnPc2
reliéfů	reliéf	k1gInPc2
na	na	k7c6
metopách	metopa	k1gFnPc6
vnějšího	vnější	k2eAgInSc2d1
dórského	dórský	k2eAgInSc2d1
vlysu	vlys	k1gInSc2
a	a	k8xC
téměř	téměř	k6eAd1
160	#num#	k4
m	m	kA
dlouhého	dlouhý	k2eAgInSc2d1
iónského	iónský	k2eAgInSc2d1
vlysu	vlys	k1gInSc2
vnitřní	vnitřní	k2eAgFnSc2d1
svatyně	svatyně	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ideový	ideový	k2eAgInSc1d1
obsah	obsah	k1gInSc1
této	tento	k3xDgFnSc2
výzdoby	výzdoba	k1gFnSc2
je	být	k5eAaImIp3nS
dán	dát	k5eAaPmNgInS
účelem	účel	k1gInSc7
stavby	stavba	k1gFnSc2
a	a	k8xC
dobou	doba	k1gFnSc7
<g/>
,	,	kIx,
ve	v	k7c4
které	který	k3yQgMnPc4,k3yIgMnPc4,k3yRgMnPc4
byla	být	k5eAaImAgFnS
vystavěna	vystavěn	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc4
především	především	k6eAd1
zdůraznění	zdůraznění	k1gNnSc4
věčného	věčný	k2eAgInSc2d1
světového	světový	k2eAgInSc2d1
řádu	řád	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
zaštiťují	zaštiťovat	k5eAaImIp3nP
bohové	bůh	k1gMnPc1
(	(	kIx(
<g/>
gigantomachie	gigantomachie	k1gFnSc1
–	–	k?
východní	východní	k2eAgNnSc4d1
průčelí	průčelí	k1gNnSc4
<g/>
,	,	kIx,
oslava	oslava	k1gFnSc1
demokracie	demokracie	k1gFnSc2
<g/>
,	,	kIx,
obce	obec	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gFnSc2
patronky	patronka	k1gFnSc2
Pallas	Pallas	k1gMnSc1
Athény	Athéna	k1gFnSc2
(	(	kIx(
<g/>
štíty	štít	k1gInPc1
a	a	k8xC
iónský	iónský	k2eAgInSc1d1
vlys	vlys	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
vítězství	vítězství	k1gNnSc1
Athén	Athéna	k1gFnPc2
v	v	k7c6
Řecko-perských	řecko-perský	k2eAgFnPc6d1
válkách	válka	k1gFnPc6
(	(	kIx(
<g/>
amazonomachie	amazonomachie	k1gFnSc1
–	–	k?
západní	západní	k2eAgNnSc4d1
průčelí	průčelí	k1gNnSc4
a	a	k8xC
Pád	Pád	k1gInSc1
Tróje	Trója	k1gFnSc2
–	–	k?
severní	severní	k2eAgFnSc2d1
metopy	metopa	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
nakonec	nakonec	k6eAd1
nevyhnutelnost	nevyhnutelnost	k1gFnSc1
boje	boj	k1gInSc2
s	s	k7c7
barbary	barbar	k1gMnPc7
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
nejistý	jistý	k2eNgInSc1d1
výsledek	výsledek	k1gInSc1
(	(	kIx(
<g/>
kentauromachie	kentauromachie	k1gFnSc2
–	–	k?
jižní	jižní	k2eAgFnSc2d1
metopy	metopa	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mytické	mytický	k2eAgInPc4d1
náměty	námět	k1gInPc4
i	i	k8xC
znázornění	znázornění	k1gNnSc4
panathénajského	panathénajský	k2eAgInSc2d1
průvodu	průvod	k1gInSc2
se	se	k3xPyFc4
soustřeďují	soustřeďovat	k5eAaImIp3nP
k	k	k7c3
oslavě	oslava	k1gFnSc3
Athén	Athéna	k1gFnPc2
a	a	k8xC
do	do	k7c2
legendární	legendární	k2eAgFnSc2d1
minulosti	minulost	k1gFnSc2
soustřeďují	soustřeďovat	k5eAaImIp3nP
rozmach	rozmach	k1gInSc4
města	město	k1gNnSc2
a	a	k8xC
demokracie	demokracie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
má	mít	k5eAaImIp3nS
prvořadou	prvořadý	k2eAgFnSc4d1
roli	role	k1gFnSc4
ochránkyně	ochránkyně	k1gFnSc2
města	město	k1gNnSc2
Athéna	Athéna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
některých	některý	k3yIgInPc6
námětech	námět	k1gInPc6
je	být	k5eAaImIp3nS
ústřední	ústřední	k2eAgFnSc7d1
postavou	postava	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
v	v	k7c6
ostatních	ostatní	k1gNnPc6
je	být	k5eAaImIp3nS
zdůrazněn	zdůrazněn	k2eAgInSc4d1
její	její	k3xOp3gInSc4
podíl	podíl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
Athénám	Athéna	k1gFnPc3
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
líčeni	líčit	k5eAaImNgMnP
i	i	k8xC
ostatní	ostatní	k2eAgMnPc1d1
bozi	bůh	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politickým	politický	k2eAgNnSc7d1
hlediskem	hledisko	k1gNnSc7
je	být	k5eAaImIp3nS
motivován	motivovat	k5eAaBmNgInS
i	i	k9
výběr	výběr	k1gInSc1
znázorněných	znázorněný	k2eAgMnPc2d1
mýtických	mýtický	k2eAgMnPc2d1
hrdinů	hrdina	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Upřednostňováni	upřednostňován	k2eAgMnPc1d1
jsou	být	k5eAaImIp3nP
tudíž	tudíž	k8xC
hrdinové	hrdina	k1gMnPc1
attičtí	attičtět	k5eAaPmIp3nP
a	a	k8xC
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS
se	se	k3xPyFc4
u	u	k7c2
nich	on	k3xPp3gMnPc2
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gFnSc7
ochránkyní	ochránkyně	k1gFnSc7
je	být	k5eAaImIp3nS
Athéna	Athéna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několikrát	několikrát	k6eAd1
je	být	k5eAaImIp3nS
tu	tu	k6eAd1
zejména	zejména	k9
Théseus	Théseus	k1gMnSc1
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zato	zato	k6eAd1
nejslavnější	slavný	k2eAgMnSc1d3
hrdina	hrdina	k1gMnSc1
řeckých	řecký	k2eAgFnPc2d1
bájí	báj	k1gFnPc2
–	–	k?
Héraklés	Héraklés	k1gInSc1
je	být	k5eAaImIp3nS
znázorněn	znázornit	k5eAaPmNgInS
pouze	pouze	k6eAd1
v	v	k7c6
gigantomachii	gigantomachie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
jeho	jeho	k3xOp3gFnSc1
účast	účast	k1gFnSc1
nešla	jít	k5eNaImAgFnS
pominout	pominout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Héraklés	Héraklés	k1gInSc4
totiž	totiž	k9
Athéňanům	Athéňan	k1gMnPc3
příliš	příliš	k6eAd1
připomínal	připomínat	k5eAaImAgMnS
Spartu	Sparta	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgMnS
považován	považován	k2eAgInSc4d1
za	za	k7c2
předka	předek	k1gMnSc2
obou	dva	k4xCgInPc2
panovnických	panovnický	k2eAgInPc2d1
rodů	rod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
důležitý	důležitý	k2eAgInSc1d1
moment	moment	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
západním	západní	k2eAgInSc6d1
štítu	štít	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
Athéna	Athéna	k1gFnSc1
a	a	k8xC
Poseidón	Poseidón	k1gInSc1
ve	v	k7c6
sporu	spor	k1gInSc6
podřizují	podřizovat	k5eAaImIp3nP
soudcovskému	soudcovský	k2eAgInSc3d1
rozhodnutí	rozhodnutí	k1gNnSc6
vládce	vládce	k1gMnPc4
Athén	Athéna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výkon	výkon	k1gInSc1
práva	právo	k1gNnSc2
v	v	k7c6
Athénách	Athéna	k1gFnPc6
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
stvrzen	stvrzen	k2eAgMnSc1d1
a	a	k8xC
posvěcen	posvěcen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Výtvarná	výtvarný	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
celé	celý	k2eAgFnSc2d1
výzdoby	výzdoba	k1gFnSc2
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
sevřená	sevřený	k2eAgFnSc1d1
a	a	k8xC
jednotná	jednotný	k2eAgFnSc1d1
<g/>
,	,	kIx,
zachovává	zachovávat	k5eAaImIp3nS
dějovou	dějový	k2eAgFnSc4d1
i	i	k8xC
obrazovou	obrazový	k2eAgFnSc4d1
celistvost	celistvost	k1gFnSc4
a	a	k8xC
liší	lišit	k5eAaImIp3nS
se	se	k3xPyFc4
zejména	zejména	k9
v	v	k7c6
kvalitě	kvalita	k1gFnSc6
provedení	provedení	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k8xC
slohovém	slohový	k2eAgNnSc6d1
pojetí	pojetí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
příbuzná	příbuzná	k1gFnSc1
reliéfu	reliéf	k1gInSc2
štítu	štít	k1gInSc2
soch	socha	k1gFnPc2
Athény	Athéna	k1gFnSc2
Parthenos	Parthenosa	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
upomíná	upomínat	k5eAaImIp3nS
na	na	k7c4
vliv	vliv	k1gInSc4
Feidiův	Feidiův	k2eAgInSc4d1
<g/>
,	,	kIx,
vliv	vliv	k1gInSc1
hlavního	hlavní	k2eAgMnSc2d1
umělce	umělec	k1gMnSc2
majícího	mající	k2eAgInSc2d1
dohled	dohled	k1gInSc4
nad	nad	k7c7
stavbou	stavba	k1gFnSc7
Parthenónu	Parthenón	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
Feidiás	Feidiás	k1gInSc1
výzdobu	výzdoba	k1gFnSc4
navrhl	navrhnout	k5eAaPmAgInS
<g/>
,	,	kIx,
vypracoval	vypracovat	k5eAaPmAgInS
její	její	k3xOp3gInSc1
model	model	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
možná	možná	k9
některé	některý	k3yIgFnPc4
části	část	k1gFnPc4
výzdoby	výzdoba	k1gFnSc2
i	i	k8xC
sám	sám	k3xTgMnSc1
realizoval	realizovat	k5eAaBmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
rozdílů	rozdíl	k1gInPc2
jednotlivých	jednotlivý	k2eAgFnPc2d1
desek	deska	k1gFnPc2
lze	lze	k6eAd1
rozpoznat	rozpoznat	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
výzdobu	výzdoba	k1gFnSc4
tvořil	tvořit	k5eAaImAgInS
kolektiv	kolektiv	k1gInSc1
sochařů	sochař	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
lišili	lišit	k5eAaImAgMnP
svými	svůj	k3xOyFgFnPc7
schopnostmi	schopnost	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
sochaři	sochař	k1gMnPc1
nezvládli	zvládnout	k5eNaPmAgMnP
původní	původní	k2eAgInSc4d1
návrh	návrh	k1gInSc4
v	v	k7c6
otočení	otočení	k1gNnSc6
těl	tělo	k1gNnPc2
a	a	k8xC
tváří	tvář	k1gFnPc2
postav	postava	k1gFnPc2
směrem	směr	k1gInSc7
k	k	k7c3
divákovi	divák	k1gMnSc3
<g/>
,	,	kIx,
někde	někde	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
nakupení	nakupení	k1gNnSc3
končetin	končetina	k1gFnPc2
zápasících	zápasící	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozdíl	rozdíl	k1gInSc1
je	být	k5eAaImIp3nS
i	i	k9
v	v	k7c6
kompozicích	kompozice	k1gFnPc6
–	–	k?
vyskytuje	vyskytovat	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
jednoduchá	jednoduchý	k2eAgFnSc1d1
úhlopříčná	úhlopříčný	k2eAgFnSc1d1
kompozice	kompozice	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
pokročilá	pokročilý	k2eAgFnSc1d1
kompozice	kompozice	k1gFnSc1
kruhová	kruhový	k2eAgFnSc1d1
s	s	k7c7
bohatším	bohatý	k2eAgNnSc7d2
vyjádřením	vyjádření	k1gNnSc7
akce	akce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozmanitost	rozmanitost	k1gFnSc1
se	se	k3xPyFc4
projevuje	projevovat	k5eAaImIp3nS
i	i	k9
v	v	k7c6
pojetí	pojetí	k1gNnSc6
akce	akce	k1gFnSc2
<g/>
,	,	kIx,
detailech	detail	k1gInPc6
drapérie	drapérie	k1gFnPc1
a	a	k8xC
jiných	jiný	k2eAgInPc6d1
aspektech	aspekt	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
rozdíly	rozdíl	k1gInPc4
se	se	k3xPyFc4
nejvíce	hodně	k6eAd3,k6eAd1
projevují	projevovat	k5eAaImIp3nP
na	na	k7c6
metopách	metopa	k1gFnPc6
dórského	dórský	k2eAgInSc2d1
vlysu	vlys	k1gInSc2
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
na	na	k7c6
výjevech	výjev	k1gInPc6
kentauromachie	kentauromachie	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
sledují	sledovat	k5eAaImIp3nP
tak	tak	k9
pořadí	pořadí	k1gNnSc4
<g/>
,	,	kIx,
v	v	k7c6
jakém	jaký	k3yQgNnSc6,k3yRgNnSc6,k3yIgNnSc6
byla	být	k5eAaImAgFnS
výzdoba	výzdoba	k1gFnSc1
vyhotovována	vyhotovovat	k5eAaImNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
obrazem	obraz	k1gInSc7
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
jak	jak	k6eAd1
se	se	k3xPyFc4
sochařský	sochařský	k2eAgInSc4d1
kolektiv	kolektiv	k1gInSc4
postupně	postupně	k6eAd1
dopracovával	dopracovávat	k5eAaImAgMnS
jednotnému	jednotný	k2eAgNnSc3d1
podání	podání	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Východní	východní	k2eAgNnSc1d1
průčelí	průčelí	k1gNnSc1
-	-	kIx~
Gigantomachie	gigantomachie	k1gFnSc1
</s>
<s>
Námětem	námět	k1gInSc7
reliéfu	reliéf	k1gInSc2
na	na	k7c6
metopách	metopa	k1gFnPc6
východního	východní	k2eAgNnSc2d1
průčelí	průčelí	k1gNnSc2
je	být	k5eAaImIp3nS
gigantomachie	gigantomachie	k1gFnSc1
–	–	k?
souboj	souboj	k1gInSc1
bohů	bůh	k1gMnPc2
s	s	k7c7
giganty	gigant	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
byli	být	k5eAaImAgMnP
potomky	potomek	k1gMnPc4
předchůdců	předchůdce	k1gMnPc2
současných	současný	k2eAgMnPc2d1
bohů	bůh	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Porážkou	porážka	k1gFnSc7
těchto	tento	k3xDgMnPc2
prvních	první	k4xOgMnPc2
bohů	bůh	k1gMnPc2
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
Kronem	Kron	k1gMnSc7
(	(	kIx(
<g/>
otec	otec	k1gMnSc1
Diův	Diův	k2eAgMnSc1d1
<g/>
)	)	kIx)
začal	začít	k5eAaPmAgMnS
současný	současný	k2eAgInSc4d1
řád	řád	k1gInSc4
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Giganti	gigant	k1gMnPc1
později	pozdě	k6eAd2
proti	proti	k7c3
bohům	bůh	k1gMnPc3
povstali	povstat	k5eAaPmAgMnP
a	a	k8xC
tento	tento	k3xDgInSc1
boj	boj	k1gInSc1
je	být	k5eAaImIp3nS
námětem	námět	k1gInSc7
reliéfu	reliéf	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
východního	východní	k2eAgNnSc2d1
průčelí	průčelí	k1gNnSc2
se	se	k3xPyFc4
zachovaly	zachovat	k5eAaPmAgFnP
pouze	pouze	k6eAd1
základní	základní	k2eAgFnPc4d1
desky	deska	k1gFnPc4
s	s	k7c7
obrysy	obrys	k1gInPc7
přiléhajících	přiléhající	k2eAgFnPc2d1
figur	figura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
každé	každý	k3xTgFnSc6
ze	z	k7c2
14	#num#	k4
<g/>
.	.	kIx.
metop	metopa	k1gFnPc2
byl	být	k5eAaImAgInS
zobrazen	zobrazit	k5eAaPmNgInS
vždy	vždy	k6eAd1
jeden	jeden	k4xCgInSc4
souboj	souboj	k1gInSc4
–	–	k?
jeden	jeden	k4xCgMnSc1
zástupce	zástupce	k1gMnPc4
bohů	bůh	k1gMnPc2
bojující	bojující	k2eAgMnSc1d1
s	s	k7c7
gigantem	gigant	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
dvanácti	dvanáct	k4xCc2
bohů	bůh	k1gMnPc2
se	se	k3xPyFc4
boje	boj	k1gInSc2
účastnil	účastnit	k5eAaImAgInS
i	i	k9
Héraklés	Héraklés	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
podle	podle	k7c2
pověstí	pověst	k1gFnPc2
boj	boj	k1gInSc4
rozhodl	rozhodnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Souboj	souboj	k1gInSc1
uvozuje	uvozovat	k5eAaImIp3nS
na	na	k7c6
čtrnácté	čtrnáctý	k4xOgFnSc6
metopě	metopa	k1gFnSc6
při	při	k7c6
severním	severní	k2eAgInSc6d1
rohu	roh	k1gInSc6
bůh	bůh	k1gMnSc1
slunce	slunce	k1gNnSc2
Hélios	Hélios	k1gMnSc1
<g/>
,	,	kIx,
kterak	kterak	k8xS
se	se	k3xPyFc4
vynořuje	vynořovat	k5eAaImIp3nS
s	s	k7c7
vozem	vůz	k1gInSc7
z	z	k7c2
moře	moře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrcholem	vrchol	k1gInSc7
je	být	k5eAaImIp3nS
naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
určením	určení	k1gNnSc7
budovy	budova	k1gFnSc2
<g/>
,	,	kIx,
třetí	třetí	k4xOgFnSc1
metopa	metopa	k1gFnSc1
od	od	k7c2
jihu	jih	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
bohyně	bohyně	k1gFnSc1
vítězství	vítězství	k1gNnSc2
Niké	Niké	k1gFnSc2
klade	klást	k5eAaImIp3nS
zvítězivší	zvítězivší	k2eAgFnSc3d1
Athéně	Athéna	k1gFnSc3
věnec	věnec	k1gInSc4
na	na	k7c4
hlavu	hlava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
upozorňuje	upozorňovat	k5eAaImIp3nS
na	na	k7c4
roli	role	k1gFnSc4
Athény	Athéna	k1gFnSc2
při	při	k7c6
boji	boj	k1gInSc6
a	a	k8xC
svým	svůj	k3xOyFgNnSc7
umístěním	umístění	k1gNnSc7
výjev	výjev	k1gInSc4
koresponduje	korespondovat	k5eAaImIp3nS
s	s	k7c7
tradicí	tradice	k1gFnSc7
předávání	předávání	k1gNnSc2
peplu	peplos	k1gInSc2
Athéně	Athéna	k1gFnSc3
při	při	k7c6
panathénaských	panathénaský	k2eAgFnPc6d1
slavnostech	slavnost	k1gFnPc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
zobrazena	zobrazit	k5eAaPmNgFnS
na	na	k7c6
iónském	iónský	k2eAgInSc6d1
vlysu	vlys	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Námětem	námět	k1gInSc7
vyšívání	vyšívání	k1gNnSc2
na	na	k7c6
peplu	peplos	k1gInSc6
byla	být	k5eAaImAgFnS
právě	právě	k9
gigantomachie	gigantomachie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Metopa	metopa	k1gFnSc1
s	s	k7c7
Athénou	Athéna	k1gFnSc7
oživuje	oživovat	k5eAaImIp3nS
kompozici	kompozice	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
obsahuje	obsahovat	k5eAaImIp3nS
tři	tři	k4xCgFnPc4
postavy	postava	k1gFnPc4
<g/>
,	,	kIx,
společně	společně	k6eAd1
se	se	k3xPyFc4
symetricky	symetricky	k6eAd1
umístěnou	umístěný	k2eAgFnSc7d1
dvanáctou	dvanáctý	k4xOgFnSc7
metopou	metopa	k1gFnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
za	za	k7c7
Héraklem	Hérakles	k1gMnSc7
a	a	k8xC
gigantem	gigant	k1gMnSc7
střílí	střílet	k5eAaImIp3nS
Eros	Eros	k1gMnSc1
z	z	k7c2
luku	luk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eros	Eros	k1gMnSc1
ovšem	ovšem	k9
střílí	střílet	k5eAaImIp3nS
na	na	k7c4
sousední	sousední	k2eAgFnSc4d1
metopu	metopa	k1gFnSc4
–	–	k?
na	na	k7c4
protivníka	protivník	k1gMnSc4
své	svůj	k3xOyFgFnSc2
matky	matka	k1gFnSc2
Afrodity	Afrodita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobně	podobně	k6eAd1
jsou	být	k5eAaImIp3nP
významově	významově	k6eAd1
spojeny	spojit	k5eAaPmNgInP
i	i	k9
jiné	jiný	k2eAgFnPc1d1
dvojice	dvojice	k1gFnPc1
metop	metopa	k1gFnPc2
<g/>
,	,	kIx,
když	když	k8xS
vedle	vedle	k7c2
bojujících	bojující	k2eAgMnPc2d1
bohů	bůh	k1gMnPc2
opatrují	opatrovat	k5eAaImIp3nP
jejich	jejich	k3xOp3gNnPc1
spřežení	spřežení	k1gNnPc1
příslušné	příslušný	k2eAgFnSc2d1
bohyně	bohyně	k1gFnSc2
–	–	k?
Apollónovi	Apollón	k1gMnSc6
sestra	sestra	k1gFnSc1
Artemis	Artemis	k1gFnSc1
<g/>
,	,	kIx,
Poseidónovi	Poseidónův	k2eAgMnPc1d1
manželka	manželka	k1gFnSc1
Amfitrité	Amfitritý	k2eAgFnSc2d1
a	a	k8xC
Héra	Héra	k1gFnSc1
Diovi	Diův	k2eAgMnPc1d1
na	na	k7c6
dvou	dva	k4xCgFnPc6
prostředních	prostřední	k2eAgFnPc6d1
metopách	metopa	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
prvky	prvek	k1gInPc7
sjednocovaly	sjednocovat	k5eAaImAgFnP
celý	celý	k2eAgInSc4d1
výjev	výjev	k1gInSc4
přes	přes	k7c4
rozdělení	rozdělení	k1gNnSc4
na	na	k7c4
jednotlivé	jednotlivý	k2eAgFnPc4d1
metopy	metopa	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Gigantové	Gigantové	k?
jsou	být	k5eAaImIp3nP
již	již	k6eAd1
vesměs	vesměs	k6eAd1
poraženi	poražen	k2eAgMnPc1d1
a	a	k8xC
bohové	bůh	k1gMnPc1
triumfují	triumfovat	k5eAaBmIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výjev	výjev	k1gInSc1
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS
věčnost	věčnost	k1gFnSc4
a	a	k8xC
posvátnost	posvátnost	k1gFnSc4
vesmírného	vesmírný	k2eAgInSc2d1
řádu	řád	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
bohové	bůh	k1gMnPc1
ochraňují	ochraňovat	k5eAaImIp3nP
před	před	k7c7
bezbožnými	bezbožný	k2eAgInPc7d1
pokusy	pokus	k1gInPc7
o	o	k7c4
změnu	změna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Detail	detail	k1gInSc1
západních	západní	k2eAgFnPc2d1
metop	metopa	k1gFnPc2
<g/>
,	,	kIx,
ilustruje	ilustrovat	k5eAaBmIp3nS
současný	současný	k2eAgInSc1d1
stav	stav	k1gInSc1
chrámu	chrám	k1gInSc2
po	po	k7c6
2500	#num#	k4
letech	let	k1gInPc6
válek	válka	k1gFnPc2
<g/>
,	,	kIx,
plenění	plenění	k1gNnSc2
<g/>
,	,	kIx,
vandalismu	vandalismus	k1gInSc2
<g/>
,	,	kIx,
restaurátorských	restaurátorský	k2eAgInPc2d1
pokusů	pokus	k1gInPc2
a	a	k8xC
eroze	eroze	k1gFnPc4
způsobené	způsobený	k2eAgFnPc4d1
znečištěním	znečištění	k1gNnSc7
vzduchu	vzduch	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Západní	západní	k2eAgNnSc1d1
průčelí	průčelí	k1gNnSc1
–	–	k?
Amazonomachie	Amazonomachie	k1gFnSc2
</s>
<s>
Zobrazení	zobrazení	k1gNnSc1
Amazonmachie	Amazonmachie	k1gFnSc2
oslavuje	oslavovat	k5eAaImIp3nS
vítězství	vítězství	k1gNnSc1
Athén	Athéna	k1gFnPc2
v	v	k7c6
Řecko-perských	řecko-perský	k2eAgFnPc6d1
válkách	válka	k1gFnPc6
<g/>
,	,	kIx,
neboť	neboť	k8xC
i	i	k9
Amazonky	Amazonka	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
Peršané	Peršan	k1gMnPc1
<g/>
,	,	kIx,
vtrhly	vtrhnout	k5eAaPmAgFnP
na	na	k7c4
Akropol	Akropol	k1gInSc4
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
byly	být	k5eAaImAgInP
zahnány	zahnat	k5eAaPmNgInP
Théseem	Théseum	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
metopy	metopa	k1gFnPc1
byly	být	k5eAaImAgFnP
značně	značně	k6eAd1
poškozeny	poškodit	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jedné	jeden	k4xCgFnSc6
krajní	krajní	k2eAgFnSc6d1
desce	deska	k1gFnSc6
se	se	k3xPyFc4
zachovala	zachovat	k5eAaPmAgFnS
část	část	k1gFnSc1
reliéfu	reliéf	k1gInSc2
s	s	k7c7
osamoceným	osamocený	k2eAgMnSc7d1
jezdcem	jezdec	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
ostatních	ostatní	k2eAgFnPc6d1
metopách	metopa	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
byly	být	k5eAaImAgFnP
zcela	zcela	k6eAd1
otlučeny	otlučen	k2eAgFnPc1d1
byl	být	k5eAaImAgInS
zobrazen	zobrazit	k5eAaPmNgInS
střídavě	střídavě	k6eAd1
souboj	souboj	k1gInSc1
pěšáka	pěšák	k1gMnSc2
s	s	k7c7
jezdcem	jezdec	k1gMnSc7
a	a	k8xC
souboj	souboj	k1gInSc4
dvou	dva	k4xCgMnPc2
pěších	pěší	k2eAgMnPc2d1
bojovníků	bojovník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jednou	k6eAd1
vítězil	vítězit	k5eAaImAgMnS
Řek	Řek	k1gMnSc1
<g/>
,	,	kIx,
jednou	jednou	k6eAd1
Amazonka	Amazonka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vítězství	vítězství	k1gNnSc1
tedy	tedy	k8xC
není	být	k5eNaImIp3nS
tak	tak	k6eAd1
samozřejmé	samozřejmý	k2eAgNnSc1d1
jako	jako	k8xC,k8xS
vítězství	vítězství	k1gNnSc1
bohů	bůh	k1gMnPc2
v	v	k7c6
protilehlém	protilehlý	k2eAgNnSc6d1
průčelí	průčelí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Severní	severní	k2eAgFnPc1d1
metopy	metopa	k1gFnPc1
–	–	k?
Pád	Pád	k1gInSc1
Tróje	Trója	k1gFnSc2
</s>
<s>
Na	na	k7c6
severních	severní	k2eAgFnPc6d1
metopách	metopa	k1gFnPc6
byl	být	k5eAaImAgInS
zobrazen	zobrazit	k5eAaPmNgInS
pád	pád	k1gInSc1
Tróje	Trója	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
výjevů	výjev	k1gInPc2
je	být	k5eAaImIp3nS
patrné	patrný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Že	že	k8xS
návrh	návrh	k1gInSc1
vycházel	vycházet	k5eAaImAgInS
ze	z	k7c2
starého	starý	k2eAgNnSc2d1
pojetí	pojetí	k1gNnSc2
zobrazování	zobrazování	k1gNnSc2
tohoto	tento	k3xDgInSc2
mýtu	mýtus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
Feidiás	Feidiás	k1gInSc1
nechal	nechat	k5eAaPmAgInS
ovlivnit	ovlivnit	k5eAaPmF
malířem	malíř	k1gMnSc7
Polygnotem	Polygnot	k1gMnSc7
z	z	k7c2
Thasu	Thas	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
podobným	podobný	k2eAgInSc7d1
výjevem	výjev	k1gInSc7
vyzdobil	vyzdobit	k5eAaPmAgInS
budovu	budova	k1gFnSc4
v	v	k7c6
Delfech	Delf	k1gInPc6
a	a	k8xC
pracoval	pracovat	k5eAaImAgInS
také	také	k9
v	v	k7c6
Athénách	Athéna	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Výjevy	výjev	k1gInPc1
zkázy	zkáza	k1gFnSc2
města	město	k1gNnSc2
je	být	k5eAaImIp3nS
uvozeno	uvodit	k5eAaPmNgNnS
zobrazením	zobrazení	k1gNnSc7
Hélia	hélium	k1gNnSc2
na	na	k7c6
první	první	k4xOgFnSc6
metopě	metopa	k1gFnSc6
ze	z	k7c2
západu	západ	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
paralele	paralela	k1gFnSc6
s	s	k7c7
ním	on	k3xPp3gInSc7
je	být	k5eAaImIp3nS
na	na	k7c6
čtvrté	čtvrtý	k4xOgFnSc6
desce	deska	k1gFnSc6
z	z	k7c2
východu	východ	k1gInSc2
znázorněna	znázornit	k5eAaPmNgFnS
bohyně	bohyně	k1gFnSc1
Měsíce	měsíc	k1gInSc2
Seléné	Seléná	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c4
filozofii	filozofie	k1gFnSc4
Anaxagora	Anaxagor	k1gMnSc2
z	z	k7c2
Klazomen	Klazomen	k2eAgMnSc1d1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
žil	žít	k5eAaImAgMnS
v	v	k7c6
Athénách	Athéna	k1gFnPc6
a	a	k8xC
jehož	jehož	k3xOyRp3gInSc4
Feidiás	Feidiás	k1gInSc4
obdivoval	obdivovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s>
Zbývající	zbývající	k2eAgFnPc1d1
tři	tři	k4xCgFnPc1
metopy	metopa	k1gFnPc1
u	u	k7c2
východního	východní	k2eAgInSc2d1
rohu	roh	k1gInSc2
byly	být	k5eAaImAgFnP
věnovány	věnovat	k5eAaPmNgFnP,k5eAaImNgFnP
ohlasům	ohlas	k1gInPc3
události	událost	k1gFnSc3
mezi	mezi	k7c7
bohy	bůh	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
každé	každý	k3xTgFnSc6
metopě	metopa	k1gFnSc6
zobrazeno	zobrazen	k2eAgNnSc1d1
setkání	setkání	k1gNnSc1
Olympana	Olympan	k1gMnSc4
přejícího	přející	k2eAgMnSc4d1
Řekům	Řek	k1gMnPc3
se	s	k7c7
stoupencem	stoupenec	k1gMnSc7
Trójanů	Trójan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dobrém	dobrý	k2eAgInSc6d1
stavu	stav	k1gInSc6
se	se	k3xPyFc4
zachovala	zachovat	k5eAaPmAgFnS
metopa	metopa	k1gFnSc1
se	s	k7c7
dvěma	dva	k4xCgFnPc7
aktérkami	aktérka	k1gFnPc7
osudné	osudný	k2eAgFnPc1d1
Paridovy	Paridův	k2eAgFnPc1d1
volby	volba	k1gFnPc1
–	–	k?
Athénou	Athéna	k1gFnSc7
a	a	k8xC
Afroditou	Afrodita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrazně	výrazně	k6eAd1
ženská	ženský	k2eAgFnSc1d1
Afrodita	Afrodita	k1gFnSc1
se	se	k3xPyFc4
shlíží	shlížet	k5eAaImIp3nS
v	v	k7c6
zrcadle	zrcadlo	k1gNnSc6
<g/>
,	,	kIx,
lhostejná	lhostejný	k2eAgFnSc1d1
již	již	k6eAd1
k	k	k7c3
osudu	osud	k1gInSc3
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
ní	on	k3xPp3gFnSc7
kontrastuje	kontrastovat	k5eAaImIp3nS
její	její	k3xOp3gFnSc2
odpůrkyně	odpůrkyně	k1gFnSc2
–	–	k?
strohá	strohý	k2eAgFnSc1d1
<g/>
,	,	kIx,
dívčí	dívčí	k2eAgFnSc1d1
Athéna	Athéna	k1gFnSc1
–	–	k?
ztělesnění	ztělesnění	k1gNnSc2
bojové	bojový	k2eAgFnSc2d1
energie	energie	k1gFnSc2
<g/>
,	,	kIx,
energie	energie	k1gFnSc1
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
dala	dát	k5eAaPmAgFnS
Řekům	Řek	k1gMnPc3
zvítězit	zvítězit	k5eAaPmF
nad	nad	k7c7
Trójou	Trója	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
některých	některý	k3yIgFnPc6
metopách	metopa	k1gFnPc6
se	se	k3xPyFc4
připomínal	připomínat	k5eAaImAgInS
podíl	podíl	k1gInSc1
Athén	Athéna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
tu	tu	k6eAd1
Meneláův	Meneláův	k2eAgMnSc1d1
kormidelník	kormidelník	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
zahynul	zahynout	k5eAaPmAgMnS
při	při	k7c6
návratu	návrat	k1gInSc6
v	v	k7c6
Atice	Atika	k1gFnSc6
a	a	k8xC
měl	mít	k5eAaImAgMnS
tam	tam	k6eAd1
svatyni	svatyně	k1gFnSc4
<g/>
,	,	kIx,
uctívanou	uctívaný	k2eAgFnSc4d1
námořníky	námořník	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
zde	zde	k6eAd1
byly	být	k5eAaImAgFnP
Théseovi	Théseův	k2eAgMnPc1d1
synové	syn	k1gMnPc1
<g/>
,	,	kIx,
kterak	kterak	k8xS
osvobozují	osvobozovat	k5eAaImIp3nP
Théseovu	Théseův	k2eAgFnSc4d1
matku	matka	k1gFnSc4
ze	z	k7c2
zajetí	zajetí	k1gNnSc2
Trójanů	Trójan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výjev	výjev	k1gInSc1
měl	mít	k5eAaImAgInS
zřejmě	zřejmě	k6eAd1
připomínat	připomínat	k5eAaImF
podíl	podíl	k1gInSc4
Athén	Athéna	k1gFnPc2
při	při	k7c6
osvobození	osvobození	k1gNnSc6
řeckých	řecký	k2eAgFnPc2d1
obcí	obec	k1gFnPc2
v	v	k7c6
Malé	Malé	k2eAgFnSc6d1
Asii	Asie	k1gFnSc6
od	od	k7c2
Peršanů	peršan	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
dalším	další	k2eAgInPc3d1
výjevům	výjev	k1gInPc3
patří	patřit	k5eAaImIp3nS
setkání	setkání	k1gNnSc1
Meneláa	Menelá	k1gInSc2
s	s	k7c7
Helenou	Helena	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Král	Král	k1gMnSc1
<g/>
,	,	kIx,
okouzlen	okouzlen	k2eAgMnSc1d1
její	její	k3xOp3gFnSc7
krásou	krása	k1gFnSc7
<g/>
,	,	kIx,
upouští	upouštět	k5eAaImIp3nS
meč	meč	k1gInSc1
a	a	k8xC
zapomíná	zapomínat	k5eAaImIp3nS
na	na	k7c4
její	její	k3xOp3gFnSc4
vinu	vina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připomínal	připomínat	k5eAaImAgMnS
se	se	k3xPyFc4
zde	zde	k6eAd1
i	i	k9
útěk	útěk	k1gInSc4
Aenea	Aeneas	k1gMnSc2
<g/>
,	,	kIx,
mytologického	mytologický	k2eAgMnSc2d1
prapředka	prapředek	k1gMnSc2
Římanů	Říman	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
hořícího	hořící	k2eAgNnSc2d1
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Jižní	jižní	k2eAgFnSc2d1
metopy	metopa	k1gFnSc2
–	–	k?
Kentauromachie	Kentauromachie	k1gFnSc2
</s>
<s>
Tato	tento	k3xDgFnSc1
řada	řada	k1gFnSc1
metop	metopa	k1gFnPc2
je	být	k5eAaImIp3nS
věnována	věnovat	k5eAaImNgFnS,k5eAaPmNgFnS
dalšímu	další	k2eAgNnSc3d1
mýtickému	mýtický	k2eAgInSc3d1
námětu	námět	k1gInSc6
Kentauromachii	Kentauromachie	k1gFnSc4
<g/>
,	,	kIx,
neboli	neboli	k8xC
boji	boj	k1gInSc6
Lapithů	Lapith	k1gMnPc2
s	s	k7c7
Kentaury	kentaur	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kentauři	kentaur	k1gMnPc1
byli	být	k5eAaImAgMnP
pozvaní	pozvaný	k2eAgMnPc1d1
na	na	k7c4
svatební	svatební	k2eAgFnSc4d1
hostinu	hostina	k1gFnSc4
vládce	vládce	k1gMnSc1
mýtického	mýtický	k2eAgInSc2d1
kmene	kmen	k1gInSc2
Lapithů	Lapith	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
Théseův	Théseův	k2eAgMnSc1d1
přítel	přítel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kentauři	kentaur	k1gMnPc1
se	se	k3xPyFc4
však	však	k9
v	v	k7c6
podnapilosti	podnapilost	k1gFnSc6
pokusili	pokusit	k5eAaPmAgMnP
zmocnit	zmocnit	k5eAaPmF
lapithských	lapithský	k2eAgFnPc2d1
žen	žena	k1gFnPc2
a	a	k8xC
mladíků	mladík	k1gInPc2
a	a	k8xC
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
boji	boj	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Ze	z	k7c2
32	#num#	k4
desek	deska	k1gFnPc2
této	tento	k3xDgFnSc2
řady	řada	k1gFnSc2
se	se	k3xPyFc4
většina	většina	k1gFnSc1
zachovala	zachovat	k5eAaPmAgFnS
<g/>
,	,	kIx,
vyjma	vyjma	k7c2
desek	deska	k1gFnPc2
uprostřed	uprostřed	k7c2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
známe	znát	k5eAaImIp1nP
z	z	k7c2
Carreyho	Carrey	k1gMnSc4
kreseb	kresba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
třech	tři	k4xCgFnPc6
metopách	metopa	k1gFnPc6
je	být	k5eAaImIp3nS
znázorněn	znázorněn	k2eAgMnSc1d1
kentaur	kentaur	k1gMnSc1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
unáší	unášet	k5eAaImIp3nS
lapithskou	lapithský	k2eAgFnSc4d1
ženu	žena	k1gFnSc4
<g/>
,	,	kIx,
na	na	k7c6
ostatních	ostatní	k2eAgInPc6d1
je	být	k5eAaImIp3nS
vždy	vždy	k6eAd1
zápasící	zápasící	k2eAgFnSc1d1
dvojice	dvojice	k1gFnSc1
kentaura	kentaur	k1gMnSc2
a	a	k8xC
lapithského	lapithský	k1gMnSc2
muže	muž	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k1gNnSc1
zbraní	zbraň	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
nebyly	být	k5eNaImAgFnP
v	v	k7c6
dosahu	dosah	k1gInSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
bojuje	bojovat	k5eAaImIp3nS
vybavením	vybavení	k1gNnSc7
hodovní	hodovní	k2eAgFnSc2d1
síně	síň	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
velikými	veliký	k2eAgInPc7d1
džbány	džbán	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boj	boj	k1gInSc1
není	být	k5eNaImIp3nS
rozhodnut	rozhodnut	k2eAgMnSc1d1
a	a	k8xC
vítězství	vítězství	k1gNnSc1
Lapithů	Lapith	k1gInPc2
se	se	k3xPyFc4
střídají	střídat	k5eAaImIp3nP
s	s	k7c7
jejich	jejich	k3xOp3gFnPc7
porážkami	porážka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
narážkou	narážka	k1gFnSc7
na	na	k7c4
nejistý	jistý	k2eNgInSc4d1
výsledek	výsledek	k1gInSc4
bojů	boj	k1gInPc2
s	s	k7c7
barbary	barbar	k1gMnPc7
<g/>
,	,	kIx,
které	který	k3yRgMnPc4,k3yQgMnPc4,k3yIgMnPc4
kentauři	kentaur	k1gMnPc1
představují	představovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
metopách	metopa	k1gFnPc6
se	se	k3xPyFc4
střídají	střídat	k5eAaImIp3nP
různá	různý	k2eAgNnPc1d1
pojetí	pojetí	k1gNnPc1
boje	boj	k1gInSc2
<g/>
,	,	kIx,
různé	různý	k2eAgFnPc1d1
situace	situace	k1gFnSc1
atd.	atd.	kA
Zkroucená	zkroucený	k2eAgFnSc1d1
a	a	k8xC
propletená	propletený	k2eAgNnPc1d1
těla	tělo	k1gNnPc1
v	v	k7c6
divokém	divoký	k2eAgInSc6d1
zápasu	zápas	k1gInSc6
jsou	být	k5eAaImIp3nP
střídána	střídat	k5eAaImNgNnP
křečemi	křeč	k1gFnPc7
umírajících	umírající	k1gFnPc2
a	a	k8xC
triumfy	triumf	k1gInPc4
vítězících	vítězící	k2eAgFnPc2d1
<g/>
,	,	kIx,
zoufalství	zoufalství	k1gNnSc4
poražených	poražený	k1gMnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k8xC
soucitem	soucit	k1gInSc7
s	s	k7c7
poraženým	poražený	k1gMnSc7
protivníkem	protivník	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
metopě	metopa	k1gFnSc6
u	u	k7c2
západního	západní	k2eAgInSc2d1
rohu	roh	k1gInSc2
je	být	k5eAaImIp3nS
zobrazen	zobrazen	k2eAgMnSc1d1
kentaur	kentaur	k1gMnSc1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
svírá	svírat	k5eAaImIp3nS
krk	krk	k1gInSc4
soupeře	soupeř	k1gMnSc2
levicí	levice	k1gFnSc7
a	a	k8xC
pravicí	pravice	k1gFnSc7
se	se	k3xPyFc4
jej	on	k3xPp3gMnSc4
chystá	chystat	k5eAaImIp3nS
dorazit	dorazit	k5eAaPmF
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jinde	jinde	k6eAd1
se	se	k3xPyFc4
jeden	jeden	k4xCgMnSc1
z	z	k7c2
Lapithů	Lapith	k1gMnPc2
pokouší	pokoušet	k5eAaImIp3nS
zneškodnit	zneškodnit	k5eAaPmF
soupeře	soupeř	k1gMnPc4
mečem	meč	k1gInSc7
<g/>
,	,	kIx,
když	když	k8xS
jej	on	k3xPp3gMnSc4
srazil	srazit	k5eAaPmAgMnS
na	na	k7c4
kolena	koleno	k1gNnPc4
a	a	k8xC
rozpřahuje	rozpřahovat	k5eAaImIp3nS
se	se	k3xPyFc4
k	k	k7c3
smrtící	smrtící	k2eAgFnSc3d1
ráně	rána	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
třetí	třetí	k4xOgFnSc6
metopě	metopa	k1gFnSc6
Lapithův	Lapithův	k2eAgInSc4d1
útok	útok	k1gInSc4
na	na	k7c4
kentaura	kentaur	k1gMnSc4
ještě	ještě	k6eAd1
nepřináší	přinášet	k5eNaImIp3nS
vítězství	vítězství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
další	další	k2eAgFnSc6d1
metopě	metopa	k1gFnSc6
se	se	k3xPyFc4
skrčený	skrčený	k2eAgInSc1d1
Lapithos	Lapithos	k1gInSc1
brání	bránit	k5eAaImIp3nS
štítem	štít	k1gInSc7
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
kentaur	kentaur	k1gMnSc1
se	se	k3xPyFc4
jej	on	k3xPp3gMnSc4
snaží	snažit	k5eAaImIp3nS
rozdrtit	rozdrtit	k5eAaPmF
džbánem	džbán	k1gInSc7
<g/>
,	,	kIx,
přidržuje	přidržovat	k5eAaImIp3nS
jej	on	k3xPp3gInSc4
kopytem	kopyto	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuhý	tuhý	k2eAgInSc1d1
boj	boj	k1gInSc1
je	být	k5eAaImIp3nS
na	na	k7c6
sedmé	sedmý	k4xOgFnSc6
metopě	metopa	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
jeden	jeden	k4xCgMnSc1
z	z	k7c2
kentaurů	kentaur	k1gMnPc2
vzepjal	vzepnout	k5eAaPmAgInS
na	na	k7c4
zadní	zadní	k2eAgNnSc4d1
v	v	k7c6
očekávání	očekávání	k1gNnSc6
prudkého	prudký	k2eAgInSc2d1
útoku	útok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinde	jinde	k6eAd1
vmáčkl	vmáčknout	k5eAaPmAgInS
Lapithos	Lapithos	k1gInSc1
v	v	k7c6
poslední	poslední	k2eAgFnSc6d1
obraně	obrana	k1gFnSc6
své	svůj	k3xOyFgInPc4
prsty	prst	k1gInPc4
do	do	k7c2
oka	oko	k1gNnSc2
svého	svůj	k3xOyFgMnSc4
téměř	téměř	k6eAd1
triumfujícího	triumfující	k2eAgMnSc4d1
protivníka	protivník	k1gMnSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
výraz	výraz	k1gInSc1
tváře	tvář	k1gFnSc2
se	se	k3xPyFc4
změnil	změnit	k5eAaPmAgInS
v	v	k7c4
divokou	divoký	k2eAgFnSc4d1
bolest	bolest	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nad	nad	k7c7
tělem	tělo	k1gNnSc7
Lapitha	Lapith	k1gMnSc2
umírajícího	umírající	k1gMnSc2
v	v	k7c6
mrtvolné	mrtvolný	k2eAgFnSc6d1
křeči	křeč	k1gFnSc6
se	se	k3xPyFc4
vzpíná	vzpínat	k5eAaImIp3nS
vítězící	vítězící	k2eAgMnSc1d1
kentaur	kentaur	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jinde	jinde	k6eAd1
Lapithos	Lapithos	k1gInSc1
hrdě	hrdě	k6eAd1
svírá	svírat	k5eAaImIp3nS
bezmocného	bezmocný	k1gMnSc4
kentaura	kentaur	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Obličeje	obličej	k1gInPc1
Lapithů	Lapith	k1gMnPc2
jsou	být	k5eAaImIp3nP
víceméně	víceméně	k9
stejné	stejný	k2eAgFnPc1d1
a	a	k8xC
odpovídají	odpovídat	k5eAaImIp3nP
typu	typ	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
později	pozdě	k6eAd2
použit	použít	k5eAaPmNgInS
na	na	k7c6
iónském	iónský	k2eAgInSc6d1
vlysu	vlys	k1gInSc6
kolem	kolem	k7c2
náu	náu	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krásní	krásnit	k5eAaImIp3nS
<g/>
,	,	kIx,
zdatní	zdatný	k2eAgMnPc1d1
mladíci	mladík	k1gMnPc1
jsou	být	k5eAaImIp3nP
bez	bez	k7c2
výrazu	výraz	k1gInSc2
a	a	k8xC
osobní	osobní	k2eAgFnSc2d1
charakteristiky	charakteristika	k1gFnSc2
–	–	k?
představitelé	představitel	k1gMnPc1
občanského	občanský	k2eAgInSc2d1
ideálu	ideál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
protikladu	protiklad	k1gInSc6
k	k	k7c3
tvářím	tvář	k1gFnPc3
kentaurů	kentaur	k1gMnPc2
jejichž	jejichž	k3xOyRp3gFnSc1
tváře	tvář	k1gFnPc1
nabývají	nabývat	k5eAaImIp3nP
značně	značně	k6eAd1
rozmanitých	rozmanitý	k2eAgFnPc2d1
podob	podoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
archaického	archaický	k2eAgNnSc2d1
pojetí	pojetí	k1gNnSc2
divokých	divoký	k2eAgFnPc2d1
tváří	tvář	k1gFnPc2
po	po	k7c4
obličeje	obličej	k1gInPc4
zasažené	zasažený	k2eAgNnSc1d1
bolestí	bolest	k1gFnSc7
<g/>
,	,	kIx,
či	či	k8xC
stářím	stáří	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tvářích	tvář	k1gFnPc6
kentaurů	kentaur	k1gMnPc2
se	se	k3xPyFc4
projevuje	projevovat	k5eAaImIp3nS
umění	umění	k1gNnSc2
sochařů	sochař	k1gMnPc2
vyjádřit	vyjádřit	k5eAaPmF
vnitřní	vnitřní	k2eAgFnSc4d1
charakteristiku	charakteristika	k1gFnSc4
postav	postava	k1gFnPc2
–	–	k?
psychologický	psychologický	k2eAgInSc4d1
výraz	výraz	k1gInSc4
<g/>
,	,	kIx,
citovou	citový	k2eAgFnSc4d1
a	a	k8xC
myšlenkovou	myšlenkový	k2eAgFnSc4d1
zaujatost	zaujatost	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
postavy	postava	k1gFnPc4
idealizovaných	idealizovaný	k2eAgInPc2d1
Lapithů	Lapith	k1gInPc2
nedovolují	dovolovat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
souvislosti	souvislost	k1gFnSc6
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
zmínit	zmínit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
znázornění	znázornění	k1gNnSc4
hlavy	hlava	k1gFnSc2
jednoho	jeden	k4xCgMnSc2
z	z	k7c2
kentaurů	kentaur	k1gMnPc2
vykazuje	vykazovat	k5eAaImIp3nS
shody	shoda	k1gFnSc2
s	s	k7c7
hlavou	hlava	k1gFnSc7
Daidala	Daidal	k1gMnSc2
na	na	k7c6
štítě	štít	k1gInSc6
Athény	Athéna	k1gFnSc2
Parthenos	Parthenosa	k1gFnPc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
Feidiás	Feidiás	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
byl	být	k5eAaImAgMnS
Feidiův	Feidiův	k2eAgInSc4d1
pokus	pokus	k1gInSc4
o	o	k7c4
jakýsi	jakýsi	k3yIgInSc4
autoportrét	autoportrét	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
pozoruhodnou	pozoruhodný	k2eAgFnSc7d1
vlastností	vlastnost	k1gFnSc7
hlav	hlava	k1gFnPc2
kentaurů	kentaur	k1gMnPc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
ze	z	k7c2
strany	strana	k1gFnSc2
odvrácené	odvrácený	k2eAgFnSc2d1
od	od	k7c2
diváka	divák	k1gMnSc2
splňují	splňovat	k5eAaImIp3nP
vysoké	vysoký	k2eAgFnPc4d1
estetické	estetický	k2eAgFnPc4d1
normy	norma	k1gFnPc4
a	a	k8xC
zároveň	zároveň	k6eAd1
jsou	být	k5eAaImIp3nP
si	se	k3xPyFc3
z	z	k7c2
této	tento	k3xDgFnSc2
strany	strana	k1gFnSc2
vzájemně	vzájemně	k6eAd1
podobné	podobný	k2eAgFnPc1d1
natolik	natolik	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
prozrazují	prozrazovat	k5eAaImIp3nP
jednotný	jednotný	k2eAgInSc4d1
(	(	kIx(
<g/>
Feidiův	Feidiův	k2eAgInSc4d1
<g/>
)	)	kIx)
návrh	návrh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Vítězství	vítězství	k1gNnSc1
nad	nad	k7c7
kentaurem	kentaur	k1gMnSc7
<g/>
,	,	kIx,
jižní	jižní	k2eAgFnSc1d1
metopa	metopa	k1gFnSc1
XVII	XVII	kA
</s>
<s>
Vítězství	vítězství	k1gNnSc1
kentaura	kentaur	k1gMnSc2
<g/>
,	,	kIx,
jižní	jižní	k2eAgFnSc1d1
metopa	metopa	k1gFnSc1
XVIII	XVIII	kA
</s>
<s>
Boj	boj	k1gInSc1
s	s	k7c7
kentaurem	kentaur	k1gMnSc7
<g/>
,	,	kIx,
jižní	jižní	k2eAgFnSc1d1
metopa	metopa	k1gFnSc1
XXX	XXX	kA
</s>
<s>
Boj	boj	k1gInSc1
s	s	k7c7
kentaurem	kentaur	k1gMnSc7
<g/>
,	,	kIx,
jižní	jižní	k2eAgFnSc1d1
metopa	metopa	k1gFnSc1
XXXI	XXXI	kA
</s>
<s>
Iónský	iónský	k2eAgInSc1d1
vlys	vlys	k1gInSc1
</s>
<s>
Význačným	význačný	k2eAgInSc7d1
sochařským	sochařský	k2eAgInSc7d1
a	a	k8xC
architektonickým	architektonický	k2eAgInSc7d1
prvkem	prvek	k1gInSc7
na	na	k7c6
Parthenónu	Parthenón	k1gInSc6
je	být	k5eAaImIp3nS
iónský	iónský	k2eAgInSc4d1
vlys	vlys	k1gInSc4
věnčící	věnčící	k2eAgInSc4d1
celou	celý	k2eAgFnSc4d1
svatyni	svatyně	k1gFnSc4
pod	pod	k7c7
vnější	vnější	k2eAgFnSc7d1
kolonádou	kolonáda	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
prvek	prvek	k1gInSc1
se	se	k3xPyFc4
u	u	k7c2
dórských	dórský	k2eAgInPc2d1
chrámů	chrám	k1gInPc2
užíval	užívat	k5eAaImAgMnS
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
zřídka	zřídka	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
asi	asi	k9
pět	pět	k4xCc1
dalších	další	k2eAgInPc2d1
případů	případ	k1gInPc2
<g/>
,	,	kIx,
vlys	vlys	k1gInSc1
na	na	k7c6
Parthenónu	Parthenón	k1gInSc6
je	být	k5eAaImIp3nS
předčí	předčít	k5eAaPmIp3nP,k5eAaBmIp3nP
svým	svůj	k3xOyFgInSc7
rozsahem	rozsah	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Není	být	k5eNaImIp3nS
to	ten	k3xDgNnSc1
však	však	k9
pouze	pouze	k6eAd1
délka	délka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
činí	činit	k5eAaImIp3nS
Parthenónský	Parthenónský	k2eAgInSc4d1
vlys	vlys	k1gInSc4
tak	tak	k8xC,k8xS
výjimečným	výjimečný	k2eAgInSc7d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
i	i	k9
námět	námět	k1gInSc1
jeho	jeho	k3xOp3gFnSc2
sochařské	sochařský	k2eAgFnSc2d1
výzdoby	výzdoba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
jím	on	k3xPp3gNnSc7
totiž	totiž	k9
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
výzdoby	výzdoba	k1gFnSc2
ostatních	ostatní	k2eAgInPc2d1
chrámů	chrám	k1gInPc2
a	a	k8xC
vnějšího	vnější	k2eAgInSc2d1
vlysu	vlys	k1gInSc2
Parthenónu	Parthenón	k1gInSc2
<g/>
,	,	kIx,
výjev	výjev	k1gInSc1
ze	z	k7c2
skutečného	skutečný	k2eAgInSc2d1
života	život	k1gInSc2
athénské	athénský	k2eAgFnSc2d1
obce	obec	k1gFnSc2
a	a	k8xC
nikoli	nikoli	k9
výjev	výjev	k1gInSc1
mytologický	mytologický	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dlouhé	dlouhý	k2eAgFnSc6d1
linii	linie	k1gFnSc6
ovinující	ovinující	k2eAgFnSc4d1
celou	celý	k2eAgFnSc4d1
svatyni	svatyně	k1gFnSc4
přibližuje	přibližovat	k5eAaImIp3nS
přípravy	příprava	k1gFnSc2
a	a	k8xC
průběh	průběh	k1gInSc4
panathénajského	panathénajský	k2eAgInSc2d1
průvodu	průvod	k1gInSc2
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
vyvrcholením	vyvrcholení	k1gNnSc7
panathénajských	panathénajský	k2eAgFnPc2d1
slavností	slavnost	k1gFnPc2
–	–	k?
nejvýznačnějšího	význačný	k2eAgInSc2d3
svátku	svátek	k1gInSc2
starověkých	starověký	k2eAgFnPc2d1
Athén	Athéna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoliv	ačkoliv	k8xS
výjev	výjev	k1gInSc1
není	být	k5eNaImIp3nS
mytologický	mytologický	k2eAgInSc1d1
<g/>
,	,	kIx,
přístup	přístup	k1gInSc1
k	k	k7c3
němu	on	k3xPp3gInSc3
se	se	k3xPyFc4
neliší	lišit	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nenajdeme	najít	k5eNaPmIp1nP
tu	tu	k6eAd1
znázornění	znázornění	k1gNnSc4
konkrétních	konkrétní	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
spíše	spíše	k9
ideálu	ideál	k1gInSc2
občana	občan	k1gMnSc2
bez	bez	k7c2
individualizujících	individualizující	k2eAgFnPc2d1
charakteristik	charakteristika	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výjev	výjev	k1gInSc1
je	být	k5eAaImIp3nS
nadčasový	nadčasový	k2eAgInSc1d1
a	a	k8xC
promítá	promítat	k5eAaImIp3nS
se	se	k3xPyFc4
do	do	k7c2
něj	on	k3xPp3gNnSc2
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
výjevů	výjev	k1gInPc2
mytologických	mytologický	k2eAgInPc2d1
<g/>
,	,	kIx,
víra	víra	k1gFnSc1
ve	v	k7c4
věčnost	věčnost	k1gFnSc4
obce	obec	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
výjevu	výjev	k1gInSc2
vlysu	vlys	k1gInSc2
</s>
<s>
Vyprávění	vyprávění	k1gNnSc1
začíná	začínat	k5eAaImIp3nS
na	na	k7c6
západním	západní	k2eAgNnSc6d1
průčelí	průčelí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zde	zde	k6eAd1
se	se	k3xPyFc4
nám	my	k3xPp1nPc3
otevírá	otevírat	k5eAaImIp3nS
pohled	pohled	k1gInSc1
na	na	k7c4
přípravy	příprava	k1gFnPc4
průvodu	průvod	k1gInSc2
v	v	k7c6
athénské	athénský	k2eAgFnSc6d1
čtvrti	čtvrt	k1gFnSc6
Kerameikos	Kerameikosa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athénští	athénský	k2eAgMnPc1d1
mladíci	mladík	k1gMnPc1
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nP
pod	pod	k7c7
dohledem	dohled	k1gInSc7
zkušených	zkušený	k2eAgMnPc2d1
velitelů	velitel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
jsou	být	k5eAaImIp3nP
již	již	k9
na	na	k7c6
koních	kůň	k1gMnPc6
<g/>
,	,	kIx,
jiní	jiný	k2eAgMnPc1d1
rozmlouvají	rozmlouvat	k5eAaImIp3nP
<g/>
,	,	kIx,
nebo	nebo	k8xC
dokončují	dokončovat	k5eAaImIp3nP
poslední	poslední	k2eAgFnPc4d1
přípravy	příprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
zvířatech	zvíře	k1gNnPc6
je	být	k5eAaImIp3nS
patrna	patrn	k2eAgFnSc1d1
netrpělivost	netrpělivost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
bezstarostností	bezstarostnost	k1gFnSc7
mladíků	mladík	k1gMnPc2
a	a	k8xC
neklidem	neklid	k1gInSc7
jejich	jejich	k3xOp3gMnPc2
koní	kůň	k1gMnPc2
kontrastuje	kontrastovat	k5eAaImIp3nS
přísná	přísný	k2eAgFnSc1d1
postava	postava	k1gFnSc1
velitele	velitel	k1gMnSc2
jízdy	jízda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nechybí	chybět	k5eNaImIp3nS,k5eNaPmIp3nS
tu	tu	k6eAd1
ani	ani	k8xC
hlasatelé	hlasatel	k1gMnPc1
a	a	k8xC
pořadatelé	pořadatel	k1gMnPc1
slavností	slavnost	k1gFnPc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
v	v	k7c6
celém	celý	k2eAgInSc6d1
následujícím	následující	k2eAgInSc6d1
průvodu	průvod	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
severní	severní	k2eAgFnSc6d1
a	a	k8xC
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
vlysu	vlys	k1gInSc2
je	být	k5eAaImIp3nS
průvod	průvod	k1gInSc1
zobrazen	zobrazit	k5eAaPmNgInS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
pokračuje	pokračovat	k5eAaImIp3nS
athénskými	athénský	k2eAgFnPc7d1
ulicemi	ulice	k1gFnPc7
na	na	k7c4
Akropol	Akropol	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdříve	dříve	k6eAd3
jsou	být	k5eAaImIp3nP
tu	tu	k6eAd1
jezdci	jezdec	k1gMnPc1
ve	v	k7c6
spořádaných	spořádaný	k2eAgInPc6d1
útvarech	útvar	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
na	na	k7c6
začátku	začátek	k1gInSc6
jižní	jižní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
připomínají	připomínat	k5eAaImIp3nP
vojenský	vojenský	k2eAgInSc4d1
jezdecký	jezdecký	k2eAgInSc4d1
oddíl	oddíl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátek	začátek	k1gInSc1
severní	severní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
je	být	k5eAaImIp3nS
však	však	k9
věnován	věnovat	k5eAaPmNgInS,k5eAaImNgInS
ještě	ještě	k9
přípravám	příprava	k1gFnPc3
<g/>
:	:	kIx,
jezdec	jezdec	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
doprovodu	doprovod	k1gInSc6
podkoního	podkoní	k1gMnSc2
si	se	k3xPyFc3
upravuje	upravovat	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
šat	šat	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
několika	několik	k4yIc2
místech	místo	k1gNnPc6
vyčnívá	vyčnívat	k5eAaImIp3nS
z	z	k7c2
řady	řada	k1gFnSc2
jezdců	jezdec	k1gMnPc2
jednotlivá	jednotlivý	k2eAgFnSc1d1
postava	postava	k1gFnSc1
téměř	téměř	k6eAd1
tak	tak	k6eAd1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
jezdecká	jezdecký	k2eAgFnSc1d1
socha	socha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
jízdou	jízda	k1gFnSc7
jedou	jet	k5eAaImIp3nP
válečné	válečný	k2eAgInPc1d1
vozy	vůz	k1gInPc1
tažené	tažený	k2eAgInPc1d1
dvěma	dva	k4xCgFnPc7
<g/>
,	,	kIx,
nebo	nebo	k8xC
čtyřmi	čtyři	k4xCgInPc7
koňmi	kůň	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vozech	vůz	k1gInPc6
stojí	stát	k5eAaImIp3nS
vozataj	vozataj	k1gMnSc1
a	a	k8xC
těžkooděnec	těžkooděnec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
z	z	k7c2
těžkooděnců	těžkooděnec	k1gMnPc2
na	na	k7c6
severní	severní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
předvádějí	předvádět	k5eAaImIp3nP
seskok	seskok	k1gInSc4
z	z	k7c2
vozu	vůz	k1gInSc2
za	za	k7c2
jízdy	jízda	k1gFnSc2
a	a	k8xC
naskočení	naskočení	k1gNnSc2
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
vojeskými	vojeský	k2eAgInPc7d1
oddíly	oddíl	k1gInPc7
kráčí	kráčet	k5eAaImIp3nS
průvod	průvod	k1gInSc1
občanů	občan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vybraní	vybraný	k2eAgMnPc1d1
starci	stařec	k1gMnPc1
nesou	nést	k5eAaImIp3nP
v	v	k7c6
průvodu	průvod	k1gInSc6
olivové	olivový	k2eAgFnSc2d1
ratolesti	ratolest	k1gFnSc2
<g/>
,	,	kIx,
jinoši	jinoch	k1gMnPc1
olej	olej	k1gInSc4
z	z	k7c2
posvátné	posvátný	k2eAgFnSc2d1
Athéniny	Athénin	k2eAgFnSc2d1
olivy	oliva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přítomní	přítomný	k1gMnPc1
jsou	být	k5eAaImIp3nP
též	též	k9
hráči	hráč	k1gMnPc1
na	na	k7c4
kithary	kithara	k1gFnPc4
a	a	k8xC
flétny	flétna	k1gFnPc4
a	a	k8xC
sbory	sbor	k1gInPc1
zpěváků	zpěvák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průvodu	průvod	k1gInSc6
kráčejí	kráčet	k5eAaImIp3nP
též	též	k9
cizinci	cizinec	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
nebyli	být	k5eNaImAgMnP
občany	občan	k1gMnPc4
Athén	Athéna	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
přinášejí	přinášet	k5eAaImIp3nP
bohyni	bohyně	k1gFnSc4
tradiční	tradiční	k2eAgInSc4d1
dar	dar	k1gInSc4
–	–	k?
nádoby	nádoba	k1gFnSc2
loďkovitého	loďkovitý	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
čele	čelo	k1gNnSc6
průvodu	průvod	k1gInSc2
na	na	k7c6
severní	severní	k2eAgFnSc6d1
i	i	k8xC
jižní	jižní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
je	být	k5eAaImIp3nS
hnán	hnát	k5eAaImNgInS
obětní	obětní	k2eAgInSc1d1
dobytek	dobytek	k1gInSc1
–	–	k?
hovězí	hovězí	k2eAgInSc4d1
dobytek	dobytek	k1gInSc4
pro	pro	k7c4
obětování	obětování	k1gNnSc4
Athéně	Athéna	k1gFnSc3
a	a	k8xC
i	i	k9
několik	několik	k4yIc1
ovcí	ovce	k1gFnPc2
pro	pro	k7c4
Athéninu	Athénin	k2eAgFnSc4d1
oblíbenkyni	oblíbenkyně	k1gFnSc4
Pandrosos	Pandrososa	k1gFnPc2
<g/>
,	,	kIx,
dceru	dcera	k1gFnSc4
mýtického	mýtický	k2eAgMnSc2d1
vládce	vládce	k1gMnSc2
Athén	Athéna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc1
kusy	kus	k1gInPc1
dobytka	dobytek	k1gInSc2
se	se	k3xPyFc4
honákům	honák	k1gMnPc3
vzpírají	vzpírat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
východní	východní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
můžeme	moct	k5eAaImIp1nP
sledovat	sledovat	k5eAaImF
vyvrcholení	vyvrcholení	k1gNnSc4
celého	celý	k2eAgInSc2d1
průvodu	průvod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čelo	čelo	k1gNnSc1
průvodu	průvod	k1gInSc2
již	již	k6eAd1
prošlo	projít	k5eAaPmAgNnS
Propylajemi	Propylaje	k1gFnPc7
–	–	k?
vstupní	vstupní	k2eAgFnSc7d1
branou	brána	k1gFnSc7
na	na	k7c4
Akropol	Akropol	k1gInSc4
a	a	k8xC
stanulo	stanout	k5eAaPmAgNnS
před	před	k7c7
východním	východní	k2eAgNnSc7d1
průčelím	průčelí	k1gNnSc7
Parthenónu	Parthenón	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pořadatelé	pořadatel	k1gMnPc1
z	z	k7c2
obou	dva	k4xCgFnPc2
stran	strana	k1gFnPc2
usměrňují	usměrňovat	k5eAaImIp3nP
zástup	zástup	k1gInSc4
dívek	dívka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
nesou	nést	k5eAaImIp3nP
kadidelnice	kadidelnice	k1gFnPc1
<g/>
,	,	kIx,
vonné	vonný	k2eAgFnPc1d1
látky	látka	k1gFnPc1
a	a	k8xC
nádoby	nádoba	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
čele	čelo	k1gNnSc6
kráčejí	kráčet	k5eAaImIp3nP
ty	ten	k3xDgFnPc1
dívky	dívka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
byly	být	k5eAaImAgFnP
vybrány	vybrat	k5eAaPmNgFnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
ušily	ušít	k5eAaPmAgFnP
Athéně	Athéna	k1gFnSc3
nový	nový	k2eAgInSc4d1
šat	šat	k1gInSc4
–	–	k?
peplos	peplos	k1gInSc1
s	s	k7c7
vyšívanou	vyšívaný	k2eAgFnSc7d1
gigantomachií	gigantomachie	k1gFnSc7
–	–	k?
nevýznamnější	významný	k2eNgNnSc4d2
z	z	k7c2
darů	dar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvě	dva	k4xCgFnPc4
skupiny	skupina	k1gFnPc1
starých	starý	k2eAgMnPc2d1
mužů	muž	k1gMnPc2
očekávají	očekávat	k5eAaImIp3nP
průvod	průvod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znázorňují	znázorňovat	k5eAaImIp3nP
mýtické	mýtický	k2eAgMnPc4d1
hrdiny	hrdina	k1gMnPc4
<g/>
,	,	kIx,
podle	podle	k7c2
kterých	který	k3yQgFnPc2,k3yIgFnPc2,k3yRgFnPc2
byly	být	k5eAaImAgFnP
pojmenovány	pojmenovat	k5eAaPmNgFnP
attické	attický	k2eAgFnPc1d1
fyly	fyla	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
však	však	k9
představují	představovat	k5eAaImIp3nP
athénské	athénský	k2eAgMnPc4d1
archonty	archón	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stáří	stář	k1gFnPc2
nenarušuje	narušovat	k5eNaImIp3nS
ideální	ideální	k2eAgNnSc4d1
zobrazení	zobrazení	k1gNnSc4
lidských	lidský	k2eAgNnPc2d1
těl	tělo	k1gNnPc2
<g/>
,	,	kIx,
jen	jen	k9
někteří	některý	k3yIgMnPc1
se	se	k3xPyFc4
opírají	opírat	k5eAaImIp3nP
o	o	k7c4
hůl	hůl	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Vrcholný	vrcholný	k2eAgInSc1d1
okamžik	okamžik	k1gInSc1
je	být	k5eAaImIp3nS
zachycen	zachytit	k5eAaPmNgInS
přesně	přesně	k6eAd1
uprostřed	uprostřed	k7c2
východní	východní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
vlysu	vlys	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvanáct	dvanáct	k4xCc1
bohů	bůh	k1gMnPc2
přišlo	přijít	k5eAaPmAgNnS
na	na	k7c4
slavnost	slavnost	k1gFnSc4
Athény	Athéna	k1gFnSc2
a	a	k8xC
jejího	její	k3xOp3gInSc2
lidu	lid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozděleni	rozdělen	k2eAgMnPc1d1
jsou	být	k5eAaImIp3nP
do	do	k7c2
několika	několik	k4yIc2
debatujících	debatující	k2eAgFnPc2d1
skupinek	skupinka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
severu	sever	k1gInSc2
očekávají	očekávat	k5eAaImIp3nP
průvod	průvod	k1gInSc1
Hermes	Hermes	k1gMnSc1
s	s	k7c7
Dionýsem	Dionýsos	k1gMnSc7
<g/>
,	,	kIx,
Démétér	Démétér	k1gFnSc1
a	a	k8xC
Árem	Árem	k1gInSc1
<g/>
,	,	kIx,
uprostřed	uprostřed	k6eAd1
sedí	sedit	k5eAaImIp3nS
na	na	k7c6
trůně	trůn	k1gInSc6
Zeus	Zeusa	k1gFnPc2
s	s	k7c7
Hérou	Héra	k1gFnSc7
<g/>
,	,	kIx,
o	o	k7c4
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
se	se	k3xPyFc4
opírá	opírat	k5eAaImIp3nS
Iris	iris	k1gInSc1
<g/>
,	,	kIx,
z	z	k7c2
jižní	jižní	k2eAgFnSc2d1
Eros	Eros	k1gMnSc1
držící	držící	k2eAgInSc4d1
slunečník	slunečník	k1gInSc4
nad	nad	k7c7
Afrodítou	Afrodíta	k1gFnSc7
ukazující	ukazující	k2eAgNnSc4d1
na	na	k7c4
průvod	průvod	k1gInSc4
a	a	k8xC
za	za	k7c7
nimi	on	k3xPp3gMnPc7
Artemis	Artemis	k1gFnSc1
a	a	k8xC
Apollón	Apollón	k1gMnSc1
a	a	k8xC
dokonce	dokonce	k9
i	i	k9
Poseidón	Poseidón	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
odhodil	odhodit	k5eAaPmAgMnS
rozmrzelost	rozmrzelost	k1gFnSc4
nad	nad	k7c7
prohrou	prohra	k1gFnSc7
s	s	k7c7
Athénou	Athéna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvojici	dvojice	k1gFnSc3
Dia	Dia	k1gFnSc2
a	a	k8xC
Héry	Héra	k1gFnSc2
odpovídá	odpovídat	k5eAaImIp3nS
Athéna	Athéna	k1gFnSc1
s	s	k7c7
Héfaistem	Héfaist	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zabrána	zabrána	k1gFnSc1
do	do	k7c2
rozhovoru	rozhovor	k1gInSc2
nesleduje	sledovat	k5eNaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
nejvyšší	vysoký	k2eAgMnSc1d3
náboženský	náboženský	k2eAgMnSc1d1
úředník	úředník	k1gMnSc1
obce-	obce-	k?
archón	archón	k1gMnSc1
basieus	basieus	k1gMnSc1
<g/>
,	,	kIx,
přebírá	přebírat	k5eAaImIp3nS
od	od	k7c2
dívek	dívka	k1gFnPc2
peplos	peplos	k1gInSc1
a	a	k8xC
připravuje	připravovat	k5eAaImIp3nS
jej	on	k3xPp3gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
s	s	k7c7
dívkami	dívka	k1gFnPc7
mezitím	mezitím	k6eAd1
umisťuje	umisťovat	k5eAaImIp3nS
sedátka	sedátko	k1gNnPc1
<g/>
,	,	kIx,
na	na	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
posléze	posléze	k6eAd1
oba	dva	k4xCgMnPc1
usednou	usednout	k5eAaPmIp3nP
mezi	mezi	k7c7
bohy	bůh	k1gMnPc7
jako	jako	k8xC,k8xS
rovní	roveň	k1gFnPc2
s	s	k7c7
rovnými	rovný	k2eAgFnPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připomíná	připomínat	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
bozi	bůh	k1gMnPc1
i	i	k8xC
smrtelníci	smrtelník	k1gMnPc1
jsou	být	k5eAaImIp3nP
jedné	jeden	k4xCgFnSc2
krve	krev	k1gFnSc2
a	a	k8xC
jsou	být	k5eAaImIp3nP
vázání	vázání	k1gNnSc6
poutem	pouto	k1gNnSc7
občanské	občanský	k2eAgFnSc2d1
sounáležitosti	sounáležitost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
demokratičtí	demokratický	k2eAgMnPc1d1
bohové	bůh	k1gMnPc1
a	a	k8xC
jejich	jejich	k3xOp3gNnSc1
znázornění	znázornění	k1gNnSc1
není	být	k5eNaImIp3nS
odlišné	odlišný	k2eAgNnSc1d1
od	od	k7c2
znázornění	znázornění	k1gNnSc2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jen	jen	k6eAd1
velikostí	velikost	k1gFnSc7
lidi	člověk	k1gMnPc4
předčí	předčít	k5eAaPmIp3nP,k5eAaBmIp3nP
–	–	k?
sedící	sedící	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
stejně	stejně	k6eAd1
vysocí	vysoký	k2eAgMnPc1d1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
stojící	stojící	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Jezdci	jezdec	k1gMnPc1
před	před	k7c7
nástupem	nástup	k1gInSc7
<g/>
,	,	kIx,
deska	deska	k1gFnSc1
XLII	XLII	kA
severního	severní	k2eAgInSc2d1
vlysu	vlys	k1gInSc2
</s>
<s>
Jezdci	jezdec	k1gMnSc3
<g/>
,	,	kIx,
deska	deska	k1gFnSc1
II	II	kA
západního	západní	k2eAgNnSc2d1
průčelí	průčelí	k1gNnSc2
</s>
<s>
Jezdci	jezdec	k1gMnPc1
<g/>
,	,	kIx,
desky	deska	k1gFnPc1
X	X	kA
a	a	k8xC
XI	XI	kA
jižního	jižní	k2eAgInSc2d1
vlysu	vlys	k1gInSc2
</s>
<s>
Vůz	vůz	k1gInSc1
<g/>
,	,	kIx,
deska	deska	k1gFnSc1
XXX	XXX	kA
jižního	jižní	k2eAgInSc2d1
vlysu	vlys	k1gInSc2
</s>
<s>
Obětní	obětní	k2eAgNnPc1d1
zvířata	zvíře	k1gNnPc1
<g/>
,	,	kIx,
deska	deska	k1gFnSc1
XL	XL	kA
jižního	jižní	k2eAgInSc2d1
vlysu	vlys	k1gInSc2
</s>
<s>
Předání	předání	k1gNnSc1
peplu	peplos	k1gInSc2
<g/>
,	,	kIx,
deska	deska	k1gFnSc1
V	V	kA
východního	východní	k2eAgNnSc2d1
průčelí	průčelí	k1gNnSc2
</s>
<s>
Technické	technický	k2eAgInPc1d1
a	a	k8xC
umělecké	umělecký	k2eAgInPc1d1
detaily	detail	k1gInPc1
provedení	provedení	k1gNnSc2
</s>
<s>
Celý	celý	k2eAgInSc1d1
výjev	výjev	k1gInSc1
je	být	k5eAaImIp3nS
pozoruhodně	pozoruhodně	k6eAd1
včleněn	včlenit	k5eAaPmNgInS
do	do	k7c2
celkového	celkový	k2eAgNnSc2d1
pojetí	pojetí	k1gNnSc2
chrámu	chrám	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
rozdělen	rozdělit	k5eAaPmNgInS
na	na	k7c4
všechny	všechen	k3xTgFnPc4
čtyři	čtyři	k4xCgFnPc4
stěny	stěna	k1gFnPc4
svatyně	svatyně	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začíná	začínat	k5eAaImIp3nS
na	na	k7c6
západním	západní	k2eAgNnSc6d1
průčelí	průčelí	k1gNnSc6
přípravami	příprava	k1gFnPc7
jezdců	jezdec	k1gMnPc2
k	k	k7c3
průvodu	průvod	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
pak	pak	k6eAd1
průvod	průvod	k1gInSc1
pokračuje	pokračovat	k5eAaImIp3nS
směrem	směr	k1gInSc7
k	k	k7c3
východnímu	východní	k2eAgNnSc3d1
průčelí	průčelí	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
jsou	být	k5eAaImIp3nP
zobrazeny	zobrazen	k2eAgInPc1d1
vyrovnané	vyrovnaný	k2eAgInPc1d1
oddíly	oddíl	k1gInPc1
jezdců	jezdec	k1gMnPc2
<g/>
,	,	kIx,
válečné	válečný	k2eAgInPc1d1
vozy	vůz	k1gInPc1
a	a	k8xC
svojí	svůj	k3xOyFgFnSc7
vážností	vážnost	k1gFnSc7
kontrastující	kontrastující	k2eAgNnSc4d1
procesí	procesí	k1gNnSc4
starců	stařec	k1gMnPc2
a	a	k8xC
zpěváků	zpěvák	k1gMnPc2
následované	následovaný	k2eAgFnSc2d1
skupinou	skupina	k1gFnSc7
neklidných	klidný	k2eNgNnPc2d1
obětních	obětní	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
východním	východní	k2eAgInSc6d1
štítě	štít	k1gInSc6
nad	nad	k7c7
původním	původní	k2eAgInSc7d1
vstupem	vstup	k1gInSc7
do	do	k7c2
chrámu	chrám	k1gInSc2
je	být	k5eAaImIp3nS
výjev	výjev	k1gInSc4
uzavřen	uzavřen	k2eAgInSc4d1
výjevem	výjev	k1gInSc7
vyvrcholení	vyvrcholení	k1gNnSc1
slavnosti	slavnost	k1gFnSc2
<g/>
,	,	kIx,
jemuž	jenž	k3xRgInSc3
soustředěně	soustředěně	k6eAd1
přihlížejí	přihlížet	k5eAaImIp3nP
z	z	k7c2
obou	dva	k4xCgFnPc2
stran	strana	k1gFnPc2
archonti	archón	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhovor	rozhovor	k1gInSc4
přihlížejících	přihlížející	k2eAgMnPc2d1
bohů	bůh	k1gMnPc2
připomíná	připomínat	k5eAaImIp3nS
zábavu	zábava	k1gFnSc4
jezdců	jezdec	k1gInPc2
před	před	k7c7
započetím	započetí	k1gNnSc7
průvodu	průvod	k1gInSc2
na	na	k7c6
protilehlém	protilehlý	k2eAgNnSc6d1
průčelí	průčelí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Velmi	velmi	k6eAd1
působivé	působivý	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
znázornění	znázornění	k1gNnSc1
koní	kůň	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
zobrazeni	zobrazit	k5eAaPmNgMnP
velmi	velmi	k6eAd1
lidsky	lidsky	k6eAd1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
kdyby	kdyby	kYmCp3nS
inteligence	inteligence	k1gFnSc1
a	a	k8xC
vůle	vůle	k1gFnSc1
jezdců	jezdec	k1gMnPc2
prostupovala	prostupovat	k5eAaImAgFnS
i	i	k9
do	do	k7c2
nich	on	k3xPp3gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvůrcům	tvůrce	k1gMnPc3
nešlo	jít	k5eNaImAgNnS
o	o	k7c4
zoologické	zoologický	k2eAgNnSc4d1
zobrazení	zobrazení	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
o	o	k7c4
vyjádření	vyjádření	k1gNnSc4
vztahu	vztah	k1gInSc2
člověka	člověk	k1gMnSc2
a	a	k8xC
zvířete	zvíře	k1gNnSc2
a	a	k8xC
jejich	jejich	k3xOp3gFnSc4
prospěšnost	prospěšnost	k1gFnSc4
obci	obec	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koně	kůň	k1gMnPc1
jsou	být	k5eAaImIp3nP
také	také	k9
vůči	vůči	k7c3
lidským	lidský	k2eAgFnPc3d1
postavám	postava	k1gFnPc3
menší	malý	k2eAgFnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
by	by	kYmCp3nS
tomu	ten	k3xDgInSc3
bylo	být	k5eAaImAgNnS
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
by	by	kYmCp3nP
se	se	k3xPyFc4
postavy	postava	k1gFnPc1
lidí	člověk	k1gMnPc2
proti	proti	k7c3
zvířatům	zvíře	k1gNnPc3
ztrácely	ztrácet	k5eAaImAgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Umělecké	umělecký	k2eAgNnSc1d1
vyjádření	vyjádření	k1gNnSc1
je	být	k5eAaImIp3nS
však	však	k9
natolik	natolik	k6eAd1
kvalitní	kvalitní	k2eAgMnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
tento	tento	k3xDgInSc1
detail	detail	k1gInSc1
uchází	ucházet	k5eAaImIp3nS
na	na	k7c4
první	první	k4xOgInSc4
pohled	pohled	k1gInSc4
pozornosti	pozornost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ačkoli	ačkoli	k8xS
lze	lze	k6eAd1
při	při	k7c6
bližším	blízký	k2eAgNnSc6d2
zkoumání	zkoumání	k1gNnSc6
provedení	provedení	k1gNnSc2
vlysu	vlys	k1gInSc2
z	z	k7c2
drobných	drobný	k2eAgInPc2d1
rozdílů	rozdíl	k1gInPc2
zjistit	zjistit	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
na	na	k7c6
něm	on	k3xPp3gMnSc6
podílelo	podílet	k5eAaImAgNnS
více	hodně	k6eAd2
sochařů	sochař	k1gMnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
celková	celkový	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
provedení	provedení	k1gNnSc2
daleko	daleko	k6eAd1
vyrovnanější	vyrovnaný	k2eAgMnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
u	u	k7c2
zachovalých	zachovalý	k2eAgFnPc2d1
jižních	jižní	k2eAgFnPc2d1
metop	metopa	k1gFnPc2
vlysu	vlys	k1gInSc2
dórského	dórský	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolektiv	kolektiv	k1gInSc4
sochařů	sochař	k1gMnPc2
již	již	k6eAd1
byl	být	k5eAaImAgMnS
v	v	k7c6
době	doba	k1gFnSc6
realizace	realizace	k1gFnSc2
iónského	iónský	k2eAgInSc2d1
vlysu	vlys	k1gInSc2
daleko	daleko	k6eAd1
zkušenější	zkušený	k2eAgMnPc4d2
a	a	k8xC
sehranější	sehraný	k2eAgMnPc4d2
<g/>
.	.	kIx.
</s>
<s>
Vlys	vlys	k1gInSc1
je	být	k5eAaImIp3nS
složen	složen	k2eAgInSc1d1
z	z	k7c2
kamenných	kamenný	k2eAgFnPc2d1
desek	deska	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednotlivé	jednotlivý	k2eAgInPc1d1
výjevy	výjev	k1gInPc1
jsou	být	k5eAaImIp3nP
proto	proto	k8xC
rozvrženy	rozvrhnout	k5eAaPmNgFnP
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
nebyly	být	k5eNaImAgFnP
narušeny	narušen	k2eAgFnPc1d1
spárami	spára	k1gFnPc7
mezi	mezi	k7c7
těmito	tento	k3xDgFnPc7
deskami	deska	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
hlavním	hlavní	k2eAgNnSc6d1
průčelí	průčelí	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
nejdůležitější	důležitý	k2eAgFnPc1d3
scény	scéna	k1gFnPc1
závěru	závěr	k1gInSc2
průvodu	průvod	k1gInSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
desky	deska	k1gFnPc4
proto	proto	k8xC
mnohem	mnohem	k6eAd1
delší	dlouhý	k2eAgFnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgMnSc3
se	se	k3xPyFc4
toto	tento	k3xDgNnSc1
průčelí	průčelí	k1gNnSc1
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
sedmi	sedm	k4xCc2
desek	deska	k1gFnPc2
a	a	k8xC
dvou	dva	k4xCgFnPc2
malých	malý	k2eAgFnPc2d1
postranních	postranní	k2eAgFnPc2d1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
protilehlé	protilehlý	k2eAgFnPc4d1
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
je	být	k5eAaImIp3nS
stejně	stejně	k6eAd1
dlouhé	dlouhý	k2eAgFnPc1d1
<g/>
,	,	kIx,
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
ze	z	k7c2
šestnácti	šestnáct	k4xCc2
desek	deska	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1
můžeme	moct	k5eAaImIp1nP
spatřit	spatřit	k5eAaPmF
v	v	k7c6
hloubce	hloubka	k1gFnSc6
reliéfu	reliéf	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
západní	západní	k2eAgFnSc6d1
a	a	k8xC
východní	východní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
je	být	k5eAaImIp3nS
vyšší	vysoký	k2eAgFnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
na	na	k7c6
ostatních	ostatní	k2eAgFnPc6d1
dvou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
částečně	částečně	k6eAd1
dáno	dát	k5eAaPmNgNnS
návrhem	návrh	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
figury	figura	k1gFnPc1
až	až	k9
v	v	k7c6
sedmi	sedm	k4xCc6
plánech	plán	k1gInPc6
za	za	k7c7
sebou	se	k3xPyFc7
<g/>
,	,	kIx,
nelze	lze	k6eNd1
uplatnit	uplatnit	k5eAaPmF
plastičnost	plastičnost	k1gFnSc4
a	a	k8xC
uplatňují	uplatňovat	k5eAaImIp3nP
se	se	k3xPyFc4
hlavně	hlavně	k9
obrysy	obrys	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reliéf	reliéf	k1gInSc1
je	být	k5eAaImIp3nS
také	také	k9
plošší	plochý	k2eAgNnSc1d2
při	při	k7c6
svém	svůj	k3xOyFgInSc6
spodním	spodní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
a	a	k8xC
v	v	k7c6
horní	horní	k2eAgFnSc6d1
části	část	k1gFnSc6
o	o	k7c4
něco	něco	k3yInSc4
vyšší	vysoký	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
získává	získávat	k5eAaImIp3nS
lepší	dobrý	k2eAgInSc1d2
sklon	sklon	k1gInSc1
pro	pro	k7c4
pozorovatele	pozorovatel	k1gMnPc4
v	v	k7c6
hloubce	hloubka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
starověku	starověk	k1gInSc6
se	se	k3xPyFc4
však	však	k9
reliéf	reliéf	k1gInSc1
skrýval	skrývat	k5eAaImAgInS
v	v	k7c6
pološeru	pološer	k1gInSc6wB
sloupového	sloupový	k2eAgInSc2d1
ochozu	ochoz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
některých	některý	k3yIgFnPc2
desek	deska	k1gFnPc2
na	na	k7c6
severní	severní	k2eAgFnSc6d1
a	a	k8xC
jižní	jižní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
nejsou	být	k5eNaImIp3nP
hlavy	hlava	k1gFnPc1
figur	figura	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
dosahují	dosahovat	k5eAaImIp3nP
až	až	k9
k	k	k7c3
okraji	okraj	k1gInSc3
vlysu	vlys	k1gInSc2
<g/>
,	,	kIx,
zcela	zcela	k6eAd1
začištěny	začištěn	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
patrně	patrně	k6eAd1
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
tyto	tento	k3xDgFnPc1
desky	deska	k1gFnPc1
byly	být	k5eAaImAgFnP
osazeny	osadit	k5eAaPmNgFnP
až	až	k9
po	po	k7c6
zastřešení	zastřešení	k1gNnSc6
stavby	stavba	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
438	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
a	a	k8xC
začištění	začištění	k1gNnSc1
jejich	jejich	k3xOp3gInSc2
horního	horní	k2eAgInSc2d1
okraje	okraj	k1gInSc2
proto	proto	k8xC
překážel	překážet	k5eAaImAgInS
strop	strop	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlys	vlys	k1gInSc4
tedy	tedy	k9
nebyl	být	k5eNaImAgInS
vytesán	vytesat	k5eAaPmNgInS
najednou	najednou	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Výzdoba	výzdoba	k1gFnSc1
štítů	štít	k1gInPc2
</s>
<s>
Vrcholem	vrchol	k1gInSc7
sochařské	sochařský	k2eAgFnSc2d1
výzdoby	výzdoba	k1gFnSc2
chrámu	chrám	k1gInSc2
byla	být	k5eAaImAgFnS
sousoší	sousoší	k1gNnSc1
umístěná	umístěný	k2eAgFnSc1d1
do	do	k7c2
obou	dva	k4xCgInPc2
štítů	štít	k1gInPc2
(	(	kIx(
<g/>
frontonů	fronton	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
byly	být	k5eAaImAgFnP
3,45	3,45	k4
m	m	kA
vysoké	vysoký	k2eAgFnSc2d1
a	a	k8xC
28.35	28.35	k4
m	m	kA
široké	široký	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
tvořena	tvořit	k5eAaImNgFnS
plnohodnotnými	plnohodnotný	k2eAgFnPc7d1
plastikami	plastika	k1gFnPc7
nebo	nebo	k8xC
polofigurami	polofigura	k1gFnPc7
v	v	k7c6
nadživotní	nadživotní	k2eAgFnSc6d1
velikosti	velikost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
bylo	být	k5eAaImAgNnS
soch	socha	k1gFnPc2
asi	asi	k9
čtyřicet	čtyřicet	k4xCc1
<g/>
,	,	kIx,
ale	ale	k8xC
do	do	k7c2
dnešního	dnešní	k2eAgInSc2d1
dne	den	k1gInSc2
se	se	k3xPyFc4
jich	on	k3xPp3gMnPc2
zachovalo	zachovat	k5eAaPmAgNnS
něco	něco	k6eAd1
málo	málo	k6eAd1
přes	přes	k7c4
deset	deset	k4xCc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
pouze	pouze	k6eAd1
ve	v	k7c6
zlomcích	zlomek	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sochy	socha	k1gFnPc1
byly	být	k5eAaImAgFnP
vsazeny	vsadit	k5eAaPmNgFnP
patrně	patrně	k6eAd1
až	až	k6eAd1
po	po	k7c6
Feidiově	Feidiův	k2eAgInSc6d1
odchodu	odchod	k1gInSc6
z	z	k7c2
Athén	Athéna	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
přesto	přesto	k8xC
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
některé	některý	k3yIgFnPc4
realizoval	realizovat	k5eAaBmAgMnS
již	již	k6eAd1
dříve	dříve	k6eAd2
sám	sám	k3xTgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napovídají	napovídat	k5eAaBmIp3nP
tomu	ten	k3xDgNnSc3
svojí	svůj	k3xOyFgFnSc7
kvalitou	kvalita	k1gFnSc7
zlomky	zlomek	k1gInPc4
hlavních	hlavní	k2eAgFnPc2d1
figur	figura	k1gFnPc2
západního	západní	k2eAgInSc2d1
štítu	štít	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téměř	téměř	k6eAd1
jistá	jistý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
však	však	k9
Feidiova	Feidiův	k2eAgFnSc1d1
účast	účast	k1gFnSc1
na	na	k7c4
vypracování	vypracování	k1gNnSc4
návrhu	návrh	k1gInSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
potvrzuje	potvrzovat	k5eAaImIp3nS
souvislost	souvislost	k1gFnSc1
s	s	k7c7
ostatními	ostatní	k2eAgFnPc7d1
částmi	část	k1gFnPc7
chrámové	chrámový	k2eAgFnSc2d1
výzdoby	výzdoba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Výjevy	výjev	k1gInPc1
v	v	k7c6
obou	dva	k4xCgInPc6
štítech	štít	k1gInPc6
oslavují	oslavovat	k5eAaImIp3nP
obec	obec	k1gFnSc4
Athény	Athéna	k1gFnSc2
a	a	k8xC
jejich	jejich	k3xOp3gNnSc4
patronku	patronka	k1gFnSc4
bohyni	bohyně	k1gFnSc4
Athénu	Athéna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
východním	východní	k2eAgInSc6d1
štítu	štít	k1gInSc6
bylo	být	k5eAaImAgNnS
zobrazeno	zobrazit	k5eAaPmNgNnS
zrození	zrození	k1gNnSc1
Athény	Athéna	k1gFnSc2
a	a	k8xC
v	v	k7c6
protilehlém	protilehlý	k2eAgNnSc6d1
vítězství	vítězství	k1gNnSc6
Athény	Athéna	k1gFnSc2
nad	nad	k7c7
Poseidónem	Poseidón	k1gInSc7
v	v	k7c6
jejich	jejich	k3xOp3gInSc6
sporu	spor	k1gInSc6
o	o	k7c4
Attiku	Attika	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Athéna	Athéna	k1gFnSc1
zrozená	zrozený	k2eAgFnSc1d1
z	z	k7c2
hlavy	hlava	k1gFnSc2
nejvyššího	vysoký	k2eAgInSc2d3
z	z	k7c2
bohů	bůh	k1gMnPc2
Dia	Dia	k1gFnSc2
za	za	k7c4
přítomnosti	přítomnost	k1gFnPc4
všech	všecek	k3xTgMnPc2
ostatních	ostatní	k2eAgMnPc2d1
bohů	bůh	k1gMnPc2
ztělesňuje	ztělesňovat	k5eAaImIp3nS
přesvědčení	přesvědčení	k1gNnSc1
o	o	k7c6
vzniku	vznik	k1gInSc6
a	a	k8xC
nadřazenosti	nadřazenost	k1gFnSc6
athénské	athénský	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
nad	nad	k7c7
celým	celý	k2eAgInSc7d1
řeckým	řecký	k2eAgInSc7d1
světem	svět	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
území	území	k1gNnSc1
Athén	Athéna	k1gFnPc2
–	–	k?
Attika	Attika	k1gFnSc1
je	být	k5eAaImIp3nS
natolik	natolik	k6eAd1
znamenitá	znamenitý	k2eAgFnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
o	o	k7c4
ni	on	k3xPp3gFnSc4
sami	sám	k3xTgMnPc1
bozi	bůh	k1gMnPc1
soupeří	soupeřit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
jsou	být	k5eAaImIp3nP
hlavní	hlavní	k2eAgFnPc1d1
myšlenky	myšlenka	k1gFnPc1
výzdoby	výzdoba	k1gFnSc2
ve	v	k7c6
štítech	štít	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Jelikož	jelikož	k8xS
se	se	k3xPyFc4
do	do	k7c2
dnešního	dnešní	k2eAgInSc2d1
dne	den	k1gInSc2
z	z	k7c2
obou	dva	k4xCgInPc2
štítů	štít	k1gInPc2
zachovaly	zachovat	k5eAaPmAgInP
jen	jen	k9
zlomky	zlomek	k1gInPc1
<g/>
,	,	kIx,
musíme	muset	k5eAaImIp1nP
se	se	k3xPyFc4
při	při	k7c6
jejich	jejich	k3xOp3gFnSc6
rekonstrukci	rekonstrukce	k1gFnSc6
spoléhat	spoléhat	k5eAaImF
na	na	k7c4
novověké	novověký	k2eAgFnPc4d1
kresby	kresba	k1gFnPc4
a	a	k8xC
starověké	starověký	k2eAgInPc4d1
ohlasy	ohlas	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
určité	určitý	k2eAgFnSc3d1
představě	představa	k1gFnSc3
přispívají	přispívat	k5eAaImIp3nP
i	i	k9
zachované	zachovaný	k2eAgInPc1d1
úseky	úsek	k1gInPc1
základny	základna	k1gFnSc2
štítového	štítový	k2eAgNnSc2d1
pole	pole	k1gNnSc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
byly	být	k5eAaImAgFnP
sochy	socha	k1gFnPc1
zapuštěny	zapuštěn	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výchozím	výchozí	k2eAgInSc7d1
materiálem	materiál	k1gInSc7
je	být	k5eAaImIp3nS
kresba	kresba	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1674	#num#	k4
<g/>
,	,	kIx,
tedy	tedy	k9
z	z	k7c2
doby	doba	k1gFnSc2
před	před	k7c7
výbuchem	výbuch	k1gInSc7
benátské	benátský	k2eAgFnSc2d1
bomby	bomba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zachycuje	zachycovat	k5eAaImIp3nS
spíše	spíše	k9
západní	západní	k2eAgInSc4d1
štít	štít	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Můžeme	moct	k5eAaImIp1nP
na	na	k7c6
ní	on	k3xPp3gFnSc6
však	však	k9
vidět	vidět	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
střední	střední	k2eAgFnSc1d1
část	část	k1gFnSc1
východního	východní	k2eAgInSc2d1
štítu	štít	k1gInSc2
chyběla	chybět	k5eAaImAgFnS
již	již	k6eAd1
před	před	k7c7
jmenovaným	jmenovaný	k2eAgInSc7d1
výbuchem	výbuch	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
materiálem	materiál	k1gInSc7
jsou	být	k5eAaImIp3nP
ohlasy	ohlas	k1gInPc1
v	v	k7c6
pozdějších	pozdní	k2eAgFnPc6d2
starověkých	starověký	k2eAgFnPc6d1
památkách	památka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
výjevů	výjev	k1gInPc2
ve	v	k7c6
štítech	štít	k1gInPc6
</s>
<s>
Celý	celý	k2eAgInSc1d1
výjev	výjev	k1gInSc1
východního	východní	k2eAgInSc2d1
štítu	štít	k1gInSc2
–	–	k?
Zrození	zrození	k1gNnSc1
Athény	Athéna	k1gFnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
uvozen	uvodit	k5eAaPmNgInS
dvěma	dva	k4xCgFnPc7
postavami	postava	k1gFnPc7
–	–	k?
Héliem	Hélios	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
v	v	k7c6
jižním	jižní	k2eAgInSc6d1
rohu	roh	k1gInSc6
štítu	štít	k1gInSc2
vynořoval	vynořovat	k5eAaImAgMnS
se	s	k7c7
svým	svůj	k3xOyFgInSc7
vozem	vůz	k1gInSc7
z	z	k7c2
vln	vlna	k1gFnPc2
oceánu	oceán	k1gInSc2
a	a	k8xC
Seléné	Seléná	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
v	v	k7c6
opačném	opačný	k2eAgInSc6d1
rohu	roh	k1gInSc6
do	do	k7c2
něj	on	k3xPp3gMnSc2
nořila	nořit	k5eAaImAgFnS
společně	společně	k6eAd1
se	s	k7c7
svými	svůj	k3xOyFgMnPc7
koňmi	kůň	k1gMnPc7
<g/>
,	,	kIx,
jimž	jenž	k3xRgMnPc3
nad	nad	k7c4
hladinu	hladina	k1gFnSc4
vyčnívaly	vyčnívat	k5eAaImAgFnP
jen	jen	k9
hlavy	hlava	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takovéto	takovýto	k3xDgNnSc1
rámování	rámování	k1gNnSc1
celé	celý	k2eAgFnSc2d1
události	událost	k1gFnSc2
sluncem	slunce	k1gNnSc7
a	a	k8xC
měsícem	měsíc	k1gInSc7
upomíná	upomínat	k5eAaImIp3nS
na	na	k7c4
Feidiův	Feidiův	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
návrh	návrh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobný	podobný	k2eAgInSc1d1
prvek	prvek	k1gInSc1
byl	být	k5eAaImAgInS
užit	užít	k5eAaPmNgInS
jak	jak	k6eAd1
na	na	k7c6
výzdobě	výzdoba	k1gFnSc6
štítu	štít	k1gInSc2
chryselefantinové	chryselefantinový	k2eAgFnSc2d1
sochy	socha	k1gFnSc2
Athény	Athéna	k1gFnSc2
Parthenos	Parthenosa	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
na	na	k7c6
trůně	trůn	k1gInSc6
sochy	socha	k1gFnSc2
Dia	Dia	k1gFnSc2
v	v	k7c6
Olympii	Olympia	k1gFnSc6
a	a	k8xC
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
filozofie	filozofie	k1gFnSc2
Anaxagora	Anaxagor	k1gMnSc2
z	z	k7c2
Klazomen	Klazomen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spojoval	spojovat	k5eAaImAgMnS
významnou	významný	k2eAgFnSc4d1
mýtickou	mýtický	k2eAgFnSc4d1
událost	událost	k1gFnSc4
zrození	zrození	k1gNnSc2
Athény	Athéna	k1gFnSc2
se	s	k7c7
systémem	systém	k1gInSc7
vesmíru	vesmír	k1gInSc2
a	a	k8xC
připomíná	připomínat	k5eAaImIp3nS
význam	význam	k1gInSc4
Athén	Athéna	k1gFnPc2
<g/>
,	,	kIx,
kterým	který	k3yIgNnSc7,k3yQgNnSc7,k3yRgNnSc7
je	být	k5eAaImIp3nS
patronkou	patronka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Vedle	vedle	k7c2
Seléné	Seléná	k1gFnSc2
pohodlně	pohodlně	k6eAd1
seděl	sedět	k5eAaImAgMnS
Dionýsos	Dionýsos	k1gMnSc1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
zde	zde	k6eAd1
seděly	sedět	k5eAaImAgFnP
bohyně	bohyně	k1gFnPc1
Koré	Korý	k2eAgFnPc1d1
a	a	k8xC
Démétér	Démétér	k1gFnPc1
<g/>
,	,	kIx,
k	k	k7c3
nimž	jenž	k3xRgMnPc3
přistupovala	přistupovat	k5eAaImAgFnS
<g/>
,	,	kIx,
jakoby	jakoby	k8xS
běžíc	běžet	k5eAaImSgFnS
<g/>
,	,	kIx,
poselkyně	poselkyně	k1gFnPc1
bohů	bůh	k1gMnPc2
Iris	iris	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1
část	část	k1gFnSc1
štítu	štít	k1gInSc2
je	být	k5eAaImIp3nS
ztracena	ztratit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Směrem	směr	k1gInSc7
do	do	k7c2
středu	střed	k1gInSc2
byla	být	k5eAaImAgFnS
určitě	určitě	k6eAd1
Héra	Héra	k1gFnSc1
<g/>
,	,	kIx,
Poseidón	Poseidón	k1gMnSc1
a	a	k8xC
Hermes	Hermes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Ústřední	ústřední	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
tvořily	tvořit	k5eAaImAgFnP
postavy	postava	k1gFnPc1
Dia	Dia	k1gFnSc2
<g/>
,	,	kIx,
Athény	Athéna	k1gFnSc2
a	a	k8xC
Niké	Niké	k1gFnSc2
v	v	k7c6
pořadí	pořadí	k1gNnSc6
zleva	zleva	k6eAd1
doprava	doprava	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeus	Zeusa	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
jehož	jehož	k3xOyRp3gFnSc2
hlavy	hlava	k1gFnSc2
se	se	k3xPyFc4
bohyně	bohyně	k1gFnSc1
zrodila	zrodit	k5eAaPmAgFnS
<g/>
,	,	kIx,
seděl	sedět	k5eAaImAgMnS
mírně	mírně	k6eAd1
pootočen	pootočit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
ním	on	k3xPp3gMnSc7
ve	v	k7c4
středu	středa	k1gFnSc4
stála	stát	k5eAaImAgFnS
ve	v	k7c6
vší	všecek	k3xTgFnSc6
slávě	sláva	k1gFnSc6
Athéna	Athéna	k1gFnSc1
a	a	k8xC
Niké	Niké	k1gFnSc1
ji	on	k3xPp3gFnSc4
kladla	klást	k5eAaImAgFnS
věnec	věnec	k1gInSc4
na	na	k7c4
hlavu	hlava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Výjev	výjev	k1gInSc1
pokračoval	pokračovat	k5eAaImAgInS
sochou	socha	k1gFnSc7
Héfaista	Héfaista	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
pomohl	pomoct	k5eAaPmAgMnS
rozštěpením	rozštěpení	k1gNnSc7
Diovy	Diův	k2eAgFnSc2d1
hlavy	hlava	k1gFnSc2
Athéně	Athéna	k1gFnSc3
na	na	k7c4
svět	svět	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
byli	být	k5eAaImAgMnP
sorozenci	sorozenec	k1gMnPc1
Artemis	Artemis	k1gFnSc1
a	a	k8xC
Apollón	Apollón	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedící	sedící	k2eAgFnPc4d1
postavy	postava	k1gFnPc4
jejich	jejich	k3xOp3gFnSc2
matky	matka	k1gFnSc2
Létó	Létó	k1gFnPc2
se	se	k3xPyFc4
zachovala	zachovat	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
ní	on	k3xPp3gFnSc2
byla	být	k5eAaImAgFnS
Dioné	Dioné	k2eAgFnSc1d1
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc7
se	se	k3xPyFc4
o	o	k7c4
klín	klín	k1gInSc4
opírala	opírat	k5eAaImAgFnS
dcera	dcera	k1gFnSc1
Afrodita	Afrodita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Nejlépe	dobře	k6eAd3
uchovaná	uchovaný	k2eAgFnSc1d1
socha	socha	k1gFnSc1
Dionýsa	Dionýsos	k1gMnSc2
ukazuje	ukazovat	k5eAaImIp3nS
důmyslné	důmyslný	k2eAgNnSc1d1
využití	využití	k1gNnSc1
nepříhodného	příhodný	k2eNgInSc2d1
úzkého	úzký	k2eAgInSc2d1
úseku	úsek	k1gInSc2
v	v	k7c6
rohu	roh	k1gInSc6
štítu	štít	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
postava	postava	k1gFnSc1
nevyplňuje	vyplňovat	k5eNaImIp3nS
pasivně	pasivně	k6eAd1
tento	tento	k3xDgInSc4
zužující	zužující	k2eAgInSc4d1
se	se	k3xPyFc4
prostor	prostor	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
působí	působit	k5eAaImIp3nS
velmi	velmi	k6eAd1
přirozeně	přirozeně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lehce	lehko	k6eAd1
uvolněn	uvolnit	k5eAaPmNgInS
jakoby	jakoby	k8xS
se	se	k3xPyFc4
chystal	chystat	k5eAaImAgMnS
v	v	k7c6
příštím	příští	k2eAgInSc6d1
okamžiku	okamžik	k1gInSc6
vykročit	vykročit	k5eAaPmF
ke	k	k7c3
středu	střed	k1gInSc3
štítu	štít	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atletické	atletický	k2eAgNnSc4d1
idealizované	idealizovaný	k2eAgNnSc4d1
tělo	tělo	k1gNnSc4
Dionýsa	Dionýsos	k1gMnSc2
plně	plně	k6eAd1
odpovídá	odpovídat	k5eAaImIp3nS
typu	typ	k1gInSc3
mladíků	mladík	k1gMnPc2
v	v	k7c6
ionském	ionský	k2eAgInSc6d1
vlysu	vlys	k1gInSc6
i	i	k8xC
v	v	k7c6
jižních	jižní	k2eAgFnPc6d1
metopách	metopa	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
výjev	výjev	k1gInSc4
západního	západní	k2eAgInSc2d1
štítu	štít	k1gInSc2
byl	být	k5eAaImAgInS
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
východního	východní	k2eAgNnSc2d1
<g/>
,	,	kIx,
uvozen	uvodit	k5eAaPmNgInS
rohovými	rohový	k2eAgFnPc7d1
figurami	figura	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
rámec	rámec	k1gInSc4
místní	místní	k2eAgInSc4d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
těmito	tento	k3xDgFnPc7
figurami	figura	k1gFnPc7
byly	být	k5eAaImAgFnP
nymfy	nymfa	k1gFnPc1
a	a	k8xC
nižší	nízký	k2eAgNnPc1d2
božstva	božstvo	k1gNnPc1
představující	představující	k2eAgFnSc2d1
Attické	Attický	k2eAgFnSc2d1
řeky	řeka	k1gFnSc2
a	a	k8xC
prameny	pramen	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
další	další	k2eAgMnPc1d1
postavy	postav	k1gInPc1
připomínali	připomínat	k5eAaImAgMnP
lokalizaci	lokalizace	k1gFnSc4
výjevu	výjev	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jižní	jižní	k2eAgFnSc6d1
části	část	k1gFnSc6
to	ten	k3xDgNnSc1
byli	být	k5eAaImAgMnP
představitelé	představitel	k1gMnPc1
mýtických	mýtický	k2eAgInPc2d1
Attických	Attický	k2eAgInPc2d1
rodů	rod	k1gInPc2
a	a	k8xC
v	v	k7c6
severní	severní	k2eAgFnSc6d1
členové	člen	k1gMnPc1
vladařského	vladařský	k2eAgInSc2d1
rodu	rod	k1gInSc2
Athén	Athéna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Událost	událost	k1gFnSc1
se	se	k3xPyFc4
podle	podle	k7c2
mytologie	mytologie	k1gFnSc2
odehrála	odehrát	k5eAaPmAgFnS
takto	takto	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
Poseidónem	Poseidón	k1gInSc7
a	a	k8xC
Athénou	Athéna	k1gFnSc7
vznikl	vzniknout	k5eAaPmAgInS
spor	spor	k1gInSc1
<g/>
,	,	kIx,
komu	kdo	k3yQnSc3,k3yRnSc3,k3yInSc3
připadne	připadnout	k5eAaPmIp3nS
bohaté	bohatý	k2eAgNnSc1d1
území	území	k1gNnSc1
Attiky	Attika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dohodli	dohodnout	k5eAaPmAgMnP
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gInSc1
spor	spor	k1gInSc1
rozsoudí	rozsoudit	k5eAaPmIp3nS
vládce	vládce	k1gMnPc4
této	tento	k3xDgFnSc2
země	zem	k1gFnSc2
Kekrops	Kekropsa	k1gFnPc2
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
z	z	k7c2
bohů	bůh	k1gMnPc2
přinese	přinést	k5eAaPmIp3nS
zemi	zem	k1gFnSc4
více	hodně	k6eAd2
užitku	užitek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Akropoli	Akropole	k1gFnSc6
se	se	k3xPyFc4
shromáždil	shromáždit	k5eAaPmAgInS
lid	lid	k1gInSc1
(	(	kIx(
<g/>
ve	v	k7c6
výjevu	výjev	k1gInSc6
zástupci	zástupce	k1gMnPc1
rodů	rod	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
stál	stát	k5eAaImAgInS
spíše	spíše	k9
na	na	k7c6
straně	strana	k1gFnSc6
Poseidóna	Poseidóna	k1gFnSc1
i	i	k8xC
panovnický	panovnický	k2eAgInSc1d1
rod	rod	k1gInSc1
mýtického	mýtický	k2eAgMnSc2d1
vladaře	vladař	k1gMnSc2
Kekropa	Kekrop	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgInS
spíše	spíše	k9
na	na	k7c6
straně	strana	k1gFnSc6
Athény	Athéna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poseidón	Poseidón	k1gInSc1
udeřil	udeřit	k5eAaPmAgInS
trojzubcem	trojzubec	k1gInSc7
do	do	k7c2
skály	skála	k1gFnSc2
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
vytryskl	vytrysknout	k5eAaPmAgMnS
slaný	slaný	k2eAgInSc4d1
pramen	pramen	k1gInSc4
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
Athéna	Athéna	k1gFnSc1
dala	dát	k5eAaPmAgFnS
vyrůst	vyrůst	k5eAaPmF
olivovému	olivový	k2eAgInSc3d1
stromu	strom	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kekrops	Kekrops	k1gInSc1
usoudil	usoudit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
olivovník	olivovník	k1gInSc1
je	být	k5eAaImIp3nS
prospěšnější	prospěšný	k2eAgInSc1d2
dar	dar	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
přiřkl	přiřknout	k5eAaPmAgMnS
vítězství	vítězství	k1gNnSc4
Athéně	Athéna	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
mýtu	mýtus	k1gInSc6
se	se	k3xPyFc4
odráží	odrážet	k5eAaImIp3nS
pradávný	pradávný	k2eAgInSc1d1
spor	spor	k1gInSc1
mezi	mezi	k7c7
městem	město	k1gNnSc7
Athénami	Athéna	k1gFnPc7
a	a	k8xC
Eleusínou	Eleusína	k1gFnSc7
o	o	k7c4
prvenství	prvenství	k1gNnSc4
v	v	k7c6
Attice	Attika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Soupeřící	soupeřící	k2eAgMnPc1d1
bozi	bůh	k1gMnPc1
–	–	k?
ústřední	ústřední	k2eAgFnSc2d1
postavy	postava	k1gFnSc2
<g/>
,	,	kIx,
stály	stát	k5eAaImAgFnP
proti	proti	k7c3
sobě	se	k3xPyFc3
vějířovitě	vějířovitě	k6eAd1
k	k	k7c3
ose	osa	k1gFnSc3
štítu	štít	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
se	se	k3xPyFc4
zachovaly	zachovat	k5eAaPmAgInP
jen	jen	k9
v	v	k7c6
několika	několik	k4yIc6
zlomcích	zlomek	k1gInPc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
podle	podle	k7c2
části	část	k1gFnSc2
Poseidónova	Poseidónův	k2eAgNnSc2d1
těla	tělo	k1gNnSc2
patrné	patrný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
šlo	jít	k5eAaImAgNnS
o	o	k7c4
mimořádně	mimořádně	k6eAd1
kvalitní	kvalitní	k2eAgFnSc4d1
sochařskou	sochařský	k2eAgFnSc4d1
práci	práce	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohutné	mohutný	k2eAgFnPc4d1
tělo	tělo	k1gNnSc4
boha	bůh	k1gMnSc2
stálo	stát	k5eAaImAgNnS
mírně	mírně	k6eAd1
zakloněno	zaklonit	k5eAaPmNgNnS
v	v	k7c6
zápalu	zápal	k1gInSc6
soutěže	soutěž	k1gFnSc2
a	a	k8xC
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
prsa	prso	k1gNnSc2
se	se	k3xPyFc4
dmula	dmout	k5eAaImAgFnS
jakoby	jakoby	k8xS
v	v	k7c6
předtuše	předtucha	k1gFnSc6
prohry	prohra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postava	postava	k1gFnSc1
trochu	trochu	k6eAd1
připomíná	připomínat	k5eAaImIp3nS
Siléna	Silén	k1gMnSc4
ze	z	k7c2
staršího	starý	k2eAgNnSc2d2
sousoší	sousoší	k1gNnSc2
Athéna	Athéna	k1gFnSc1
a	a	k8xC
Marsyas	Marsyas	k1gInSc1
od	od	k7c2
sochaře	sochař	k1gMnSc2
Myróna	Myrón	k1gMnSc2
a	a	k8xC
potvrzuje	potvrzovat	k5eAaImIp3nS
kontinuitu	kontinuita	k1gFnSc4
s	s	k7c7
athénskou	athénský	k2eAgFnSc7d1
sochařskou	sochařský	k2eAgFnSc7d1
tradicí	tradice	k1gFnSc7
a	a	k8xC
zároveň	zároveň	k6eAd1
jakým	jaký	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
způsobem	způsob	k1gInSc7
byla	být	k5eAaImAgFnS
zhodnocena	zhodnocen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proti	proti	k7c3
Poseidónovi	Poseidón	k1gMnSc3
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
je	být	k5eAaImIp3nS
přes	přes	k7c4
všechnu	všechen	k3xTgFnSc4
svoji	svůj	k3xOyFgFnSc4
sílu	síla	k1gFnSc4
v	v	k7c6
tomto	tento	k3xDgInSc6
souboji	souboj	k1gInSc6
bezmocný	bezmocný	k1gMnSc1
<g/>
,	,	kIx,
stála	stát	k5eAaImAgFnS
Athéna	Athéna	k1gFnSc1
ztělesňující	ztělesňující	k2eAgInSc4d1
důvtip	důvtip	k1gInSc4
a	a	k8xC
rozvahu	rozvaha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
protiklad	protiklad	k1gInSc1
určoval	určovat	k5eAaImAgInS
dynamičnost	dynamičnost	k1gFnSc4
celé	celý	k2eAgFnSc2d1
kompozice	kompozice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
obou	dva	k4xCgMnPc2
bohů	bůh	k1gMnPc2
stála	stát	k5eAaImAgFnS
dále	daleko	k6eAd2
jejich	jejich	k3xOp3gNnSc2
spřežení	spřežení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vozatajem	vozataj	k1gMnSc7
Poseidónovým	Poseidónový	k2eAgMnSc7d1
byla	být	k5eAaImAgFnS
jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
Amfitrité	Amfitritý	k2eAgFnSc2d1
a	a	k8xC
Athéniným	Athénin	k2eAgFnPc3d1
bohyně	bohyně	k1gFnSc1
vítězství	vítězství	k1gNnSc1
Niké	Niké	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
jakoby	jakoby	k8xS
předurčovala	předurčovat	k5eAaImAgFnS
výsledek	výsledek	k1gInSc4
soutěže	soutěž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postavy	postava	k1gFnPc1
těchto	tento	k3xDgFnPc2
bohyň	bohyně	k1gFnPc2
pomáhaly	pomáhat	k5eAaImAgFnP
určovat	určovat	k5eAaImF
drapérie	drapérie	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
šat	šat	k1gInSc1
Niké	Niké	k1gFnSc2
zdůrazňoval	zdůrazňovat	k5eAaImAgInS
dívčí	dívčí	k2eAgFnSc4d1
postavu	postava	k1gFnSc4
<g/>
,	,	kIx,
Amfitritin	Amfitritin	k1gInSc1
připomínal	připomínat	k5eAaImAgInS
vlhkou	vlhký	k2eAgFnSc4d1
látku	látka	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
tedy	tedy	k9
moře	moře	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc4,k3yQgNnSc4,k3yRgNnSc4
bohyně	bohyně	k1gFnSc1
reprezentuje	reprezentovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c7
Athénou	Athéna	k1gFnSc7
<g/>
,	,	kIx,
v	v	k7c6
severní	severní	k2eAgFnSc6d1
polovině	polovina	k1gFnSc6
štítu	štít	k1gInSc2
stála	stát	k5eAaImAgFnS
skupina	skupina	k1gFnSc1
vladaře	vladař	k1gMnSc2
Kekropa	Kekrop	k1gMnSc2
–	–	k?
rozhodčího	rozhodčí	k1gMnSc2
sporu	spor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
z	z	k7c2
těchto	tento	k3xDgFnPc2
postav	postava	k1gFnPc2
se	se	k3xPyFc4
zachovaly	zachovat	k5eAaPmAgInP
přímo	přímo	k6eAd1
na	na	k7c6
Parthenónu	Parthenón	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sochař	sochař	k1gMnSc1
naznačil	naznačit	k5eAaPmAgMnS
Kekropovo	Kekropův	k2eAgNnSc4d1
hadí	hadí	k2eAgNnSc4d1
tělo	tělo	k1gNnSc4
jen	jen	k9
jedním	jeden	k4xCgInSc7
obloukem	oblouk	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
němu	on	k3xPp3gInSc3
se	se	k3xPyFc4
vinula	vinout	k5eAaImAgFnS
jeho	jeho	k3xOp3gFnSc1
dcera	dcera	k1gFnSc1
Pandrosos	Pandrososa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mýtus	mýtus	k1gInSc1
o	o	k7c6
vladaři	vladař	k1gMnSc6
Kekropovi	Kekrop	k1gMnSc6
byl	být	k5eAaImAgMnS
silnou	silný	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
povědomí	povědomí	k1gNnSc2
Athéňanů	Athéňan	k1gMnPc2
o	o	k7c6
dávné	dávný	k2eAgFnSc6d1
historii	historie	k1gFnSc6
města	město	k1gNnSc2
a	a	k8xC
i	i	k9
historičtí	historický	k2eAgMnPc1d1
Athéňané	Athéňan	k1gMnPc1
se	se	k3xPyFc4
někdy	někdy	k6eAd1
nazývali	nazývat	k5eAaImAgMnP
„	„	k?
<g/>
Potomci	potomek	k1gMnPc1
Kekropovi	Kekrop	k1gMnSc6
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Východní	východní	k2eAgInSc1d1
štít	štít	k1gInSc1
<g/>
,	,	kIx,
rekonstrukce	rekonstrukce	k1gFnSc1
</s>
<s>
Západní	západní	k2eAgInSc1d1
štít	štít	k1gInSc1
<g/>
,	,	kIx,
rekonstrukce	rekonstrukce	k1gFnSc1
</s>
<s>
Detaily	detail	k1gInPc1
provedení	provedení	k1gNnSc2
</s>
<s>
Kompozice	kompozice	k1gFnSc1
štítů	štít	k1gInPc2
vykazuje	vykazovat	k5eAaImIp3nS
použití	použití	k1gNnSc1
několika	několik	k4yIc2
prvků	prvek	k1gInPc2
uplatňujících	uplatňující	k2eAgInPc2d1
se	se	k3xPyFc4
spíše	spíše	k9
v	v	k7c6
malířství	malířství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
východním	východní	k2eAgInSc6d1
štítě	štít	k1gInSc6
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
pomocí	pomocí	k7c2
figur	figura	k1gFnPc2
Hélia	hélium	k1gNnSc2
a	a	k8xC
Seléné	Seléný	k2eAgNnSc4d1
naznačené	naznačený	k2eAgNnSc4d1
vymezení	vymezení	k1gNnSc4
prostoru	prostor	k1gInSc2
mořem	moře	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povrch	povrch	k1gInSc1
bloku	blok	k1gInSc2
<g/>
,	,	kIx,
z	z	k7c2
něhož	jenž	k3xRgInSc2
se	se	k3xPyFc4
Hélios	Hélios	k1gMnSc1
vynořuje	vynořovat	k5eAaImIp3nS
<g/>
,	,	kIx,
nese	nést	k5eAaImIp3nS
dokonce	dokonce	k9
jednoduchý	jednoduchý	k2eAgInSc4d1
reliéf	reliéf	k1gInSc4
vln	vlna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
propojení	propojení	k1gNnSc1
jednotlivých	jednotlivý	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
jak	jak	k8xS,k8xC
významově	významově	k6eAd1
(	(	kIx(
<g/>
např.	např.	kA
Dionýsos	Dionýsos	k1gMnSc1
<g/>
,	,	kIx,
Koré	Korá	k1gFnPc1
a	a	k8xC
Démétér	Démétér	k1gFnPc1
<g/>
,	,	kIx,
vých	vých	k1gInSc1
<g/>
.	.	kIx.
štít	štít	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
dějově	dějově	k6eAd1
(	(	kIx(
<g/>
přibíhající	přibíhající	k2eAgFnSc1d1
Iris	iris	k1gFnSc1
<g/>
,	,	kIx,
vých	vých	k1gInSc1
<g/>
.	.	kIx.
štít	štít	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pootočení	pootočení	k1gNnSc1
postav	postava	k1gFnPc2
mimo	mimo	k7c4
přísnou	přísný	k2eAgFnSc4d1
průčelnost	průčelnost	k1gFnSc4
<g/>
,	,	kIx,
přesah	přesah	k1gInSc4
některých	některý	k3yIgFnPc2
figur	figura	k1gFnPc2
mimo	mimo	k7c4
rámec	rámec	k1gInSc4
štítu	štít	k1gInSc2
(	(	kIx(
<g/>
figura	figura	k1gFnSc1
Selenina	Selenin	k2eAgMnSc2d1
koně	kůň	k1gMnSc2
<g/>
,	,	kIx,
vých	vých	k1gInSc1
<g/>
.	.	kIx.
štít	štít	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
též	též	k9
lehké	lehký	k2eAgNnSc1d1
povysunutí	povysunutí	k1gNnSc1
rohových	rohový	k2eAgFnPc2d1
figur	figura	k1gFnPc2
vpřed	vpřed	k6eAd1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
dodává	dodávat	k5eAaImIp3nS
výjevu	výjev	k1gInSc3
dojem	dojem	k1gInSc1
vzdušnosti	vzdušnost	k1gFnSc3
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
také	také	k9
„	„	k?
<g/>
malířské	malířský	k2eAgFnSc2d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
celém	celý	k2eAgInSc6d1
výjevu	výjev	k1gInSc6
se	se	k3xPyFc4
také	také	k9
velmi	velmi	k6eAd1
umně	umně	k6eAd1
střídá	střídat	k5eAaImIp3nS
klid	klid	k1gInSc1
a	a	k8xC
pohyb	pohyb	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
Feidiás	Feidiás	k1gInSc1
využil	využít	k5eAaPmAgInS
při	při	k7c6
tvorbě	tvorba	k1gFnSc6
návrhů	návrh	k1gInPc2
obou	dva	k4xCgInPc2
štítů	štít	k1gInPc2
vzoru	vzor	k1gInSc2
současného	současný	k2eAgMnSc2d1
malíře	malíř	k1gMnSc2
Polygnota	Polygnota	k1gFnSc1
z	z	k7c2
Thasu	Thas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napovídají	napovídat	k5eAaBmIp3nP
tomu	ten	k3xDgMnSc3
zprávy	zpráva	k1gFnPc4
o	o	k7c6
něm	on	k3xPp3gNnSc6
a	a	k8xC
ohlasy	ohlas	k1gInPc4
jeho	jeho	k3xOp3gNnPc2
děl	dělo	k1gNnPc2
na	na	k7c6
keramice	keramika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzájemná	vzájemný	k2eAgFnSc1d1
dějová	dějový	k2eAgFnSc1d1
propojenost	propojenost	k1gFnSc1
je	být	k5eAaImIp3nS
pokrokem	pokrok	k1gInSc7
proti	proti	k7c3
dřívějším	dřívější	k2eAgFnPc3d1
zobrazením	zobrazení	k1gNnSc7
mytologických	mytologický	k2eAgInPc2d1
výjevů	výjev	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byly	být	k5eAaImAgFnP
jednotlivé	jednotlivý	k2eAgFnPc1d1
postavy	postava	k1gFnPc1
propojeny	propojit	k5eAaPmNgFnP
jen	jen	k6eAd1
zevně	zevně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Zlomky	zlomek	k1gInPc4
soch	socha	k1gFnPc2
z	z	k7c2
jižního	jižní	k2eAgInSc2d1
rohu	roh	k1gInSc2
východního	východní	k2eAgInSc2d1
štítu	štít	k1gInSc2
</s>
<s>
Socha	socha	k1gFnSc1
Dionýsa	Dionýsos	k1gMnSc2
</s>
<s>
Artemis	Artemis	k1gFnSc1
<g/>
,	,	kIx,
Dioné	Dioné	k1gNnSc1
a	a	k8xC
Afrodita	Afrodita	k1gFnSc1
ze	z	k7c2
severní	severní	k2eAgFnSc2d1
části	část	k1gFnSc2
východního	východní	k2eAgInSc2d1
štítu	štít	k1gInSc2
</s>
<s>
Roh	roh	k1gInSc1
východního	východní	k2eAgInSc2d1
štítu	štít	k1gInSc2
Parthenonu	Parthenon	k1gInSc2
s	s	k7c7
vsazenými	vsazený	k2eAgFnPc7d1
kopiemi	kopie	k1gFnPc7
soch	socha	k1gFnPc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.osel.cz/8320-parthenon-byl-neco-jako-fort-knox-starovekych-athen.html	http://www.osel.cz/8320-parthenon-byl-neco-jako-fort-knox-starovekych-athen.html	k1gInSc1
-	-	kIx~
Parthenón	Parthenón	k1gInSc1
byl	být	k5eAaImAgInS
něco	něco	k3yInSc4
jako	jako	k9
Fort	Fort	k?
Knox	Knox	k1gInSc1
starověkých	starověký	k2eAgFnPc2d1
Athén	Athéna	k1gFnPc2
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Boardman	Boardman	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
Parthenon	Parthenon	k1gInSc4
and	and	k?
Its	Its	k1gFnSc2
Sculptures	Sculpturesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Londýn	Londýn	k1gInSc1
<g/>
,	,	kIx,
Thames	Thames	k1gMnSc1
and	and	k?
Hudson	Hudson	k1gMnSc1
1985	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
0-500-01372-1	0-500-01372-1	k4
</s>
<s>
Buttinová	Buttinový	k2eAgFnSc1d1
<g/>
,	,	kIx,
Anne	Anne	k1gFnSc1
-	-	kIx~
Marie	Marie	k1gFnSc1
,	,	kIx,
Řecko	Řecko	k1gNnSc1
<g/>
,	,	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2002	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7106-566-8	80-7106-566-8	k4
</s>
<s>
Frel	Frel	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
,	,	kIx,
Feidias	Feidias	k1gMnSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Edice	edice	k1gFnSc2
Portréty	portrét	k1gInPc1
<g/>
,	,	kIx,
Orbis	orbis	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1964	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s>
Levi	Lev	k1gMnPc1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Svět	svět	k1gInSc1
starého	starý	k2eAgNnSc2d1
Řecka	Řecko	k1gNnSc2
<g/>
,	,	kIx,
Knižní	knižní	k2eAgInSc1d1
klub	klub	k1gInSc1
k.	k.	k?
s.	s.	k?
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1995	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-7176-214-8	80-7176-214-8	k4
</s>
<s>
Ottův	Ottův	k2eAgInSc4d1
slovník	slovník	k1gInSc4
naučný	naučný	k2eAgInSc4d1
<g/>
,	,	kIx,
čtrnáctý	čtrnáctý	k4xOgInSc4
díl	díl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydavatel	vydavatel	k1gMnSc1
a	a	k8xC
nakladatel	nakladatel	k1gMnSc1
J.	J.	kA
<g/>
Otto	Otto	k1gMnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1899	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Feidiás	Feidiás	k6eAd1
</s>
<s>
Feidiův	Feidiův	k2eAgInSc1d1
Zeus	Zeus	k1gInSc1
v	v	k7c6
Olympii	Olympia	k1gFnSc6
</s>
<s>
Kallikratés	Kallikratés	k6eAd1
</s>
<s>
Dórský	dórský	k2eAgInSc1d1
řád	řád	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Parthenón	Parthenón	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Parthenón	Parthenón	k1gInSc1
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Antika	antika	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4068909-8	4068909-8	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85098340	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
242092481	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85098340	#num#	k4
</s>
