<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
,	,	kIx,	,
známa	známo	k1gNnSc2	známo
jako	jako	k8xS	jako
Dannebrog	Dannebroga	k1gFnPc2	Dannebroga
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
červeným	červený	k2eAgInSc7d1	červený
listem	list	k1gInSc7	list
s	s	k7c7	s
bílým	bílý	k2eAgInSc7d1	bílý
skandinávským	skandinávský	k2eAgInSc7d1	skandinávský
křížem	kříž	k1gInSc7	kříž
sahajícím	sahající	k2eAgInSc7d1	sahající
až	až	k6eAd1	až
ke	k	k7c3	k
krajům	kraj	k1gInPc3	kraj
<g/>
.	.	kIx.	.
</s>
