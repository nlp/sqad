<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
,	,	kIx,	,
známa	známo	k1gNnSc2	známo
jako	jako	k8xS	jako
Dannebrog	Dannebroga	k1gFnPc2	Dannebroga
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
červeným	červený	k2eAgInSc7d1	červený
listem	list	k1gInSc7	list
s	s	k7c7	s
bílým	bílý	k2eAgInSc7d1	bílý
skandinávským	skandinávský	k2eAgInSc7d1	skandinávský
křížem	kříž	k1gInSc7	kříž
sahajícím	sahající	k2eAgInSc7d1	sahající
až	až	k6eAd1	až
ke	k	k7c3	k
krajům	kraj	k1gInPc3	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
věšení	věšení	k1gNnSc6	věšení
ve	v	k7c6	v
vertikální	vertikální	k2eAgFnSc6d1	vertikální
poloze	poloha	k1gFnSc6	poloha
je	být	k5eAaImIp3nS	být
kratší	krátký	k2eAgNnSc4d2	kratší
rameno	rameno	k1gNnSc4	rameno
skandinávského	skandinávský	k2eAgInSc2d1	skandinávský
kříže	kříž	k1gInSc2	kříž
posunuto	posunut	k2eAgNnSc1d1	posunuto
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
žerďovému	žerďový	k2eAgInSc3d1	žerďový
lemu	lem	k1gInSc3	lem
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Skandinávský	skandinávský	k2eAgInSc1d1	skandinávský
kříž	kříž	k1gInSc1	kříž
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
na	na	k7c6	na
vlajkách	vlajka	k1gFnPc6	vlajka
všech	všecek	k3xTgFnPc2	všecek
dalších	další	k2eAgFnPc2d1	další
severských	severský	k2eAgFnPc2d1	severská
zemí	zem	k1gFnPc2	zem
<g/>
:	:	kIx,	:
švédské	švédský	k2eAgFnSc2d1	švédská
<g/>
,	,	kIx,	,
finské	finský	k2eAgFnSc2d1	finská
<g/>
,	,	kIx,	,
norské	norský	k2eAgFnSc2d1	norská
a	a	k8xC	a
islandské	islandský	k2eAgFnSc2d1	islandská
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
období	období	k1gNnSc2	období
Dánska-Norska	Dánska-Norsko	k1gNnSc2	Dánska-Norsko
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
používána	používat	k5eAaImNgFnS	používat
i	i	k9	i
jako	jako	k9	jako
vlajka	vlajka	k1gFnSc1	vlajka
Norska	Norsko	k1gNnSc2	Norsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
přijalo	přijmout	k5eAaPmAgNnS	přijmout
svou	svůj	k3xOyFgFnSc4	svůj
dnešní	dnešní	k2eAgFnSc4d1	dnešní
vlajku	vlajka	k1gFnSc4	vlajka
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
<g/>
.	.	kIx.	.
</s>
<s>
Dánská	dánský	k2eAgFnSc1d1	dánská
vlajka	vlajka	k1gFnSc1	vlajka
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejstarším	starý	k2eAgInPc3d3	nejstarší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
−	−	k?	−
používala	používat	k5eAaImAgFnS	používat
se	se	k3xPyFc4	se
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1219	[number]	k4	1219
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgInSc1d3	nejstarší
státní	státní	k2eAgFnSc7d1	státní
vlajkou	vlajka	k1gFnSc7	vlajka
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
používanou	používaný	k2eAgFnSc4d1	používaná
nezávislým	závislý	k2eNgInSc7d1	nezávislý
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
dánské	dánský	k2eAgFnSc2d1	dánská
vlajky	vlajka	k1gFnSc2	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
Legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
prvním	první	k4xOgInSc6	první
objevení	objevení	k1gNnSc6	objevení
se	se	k3xPyFc4	se
dánské	dánský	k2eAgFnSc2d1	dánská
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
Dány	Dán	k1gMnPc7	Dán
velice	velice	k6eAd1	velice
populární	populární	k2eAgInSc1d1	populární
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc3	on
dánská	dánský	k2eAgFnSc1d1	dánská
vlajka	vlajka	k1gFnSc1	vlajka
spadla	spadnout	k5eAaPmAgFnS	spadnout
z	z	k7c2	z
nebe	nebe	k1gNnSc2	nebe
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1219	[number]	k4	1219
při	při	k7c6	při
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Lyndanisse	Lyndanisse	k1gFnSc2	Lyndanisse
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Estonsku	Estonsko	k1gNnSc6	Estonsko
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dánská	dánský	k2eAgNnPc4d1	dánské
vojska	vojsko	k1gNnPc4	vojsko
krále	král	k1gMnSc2	král
Waldemara	Waldemar	k1gMnSc2	Waldemar
II	II	kA	II
<g/>
.	.	kIx.	.
prohrávala	prohrávat	k5eAaImAgFnS	prohrávat
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
biskup	biskup	k1gInSc1	biskup
Andrei	Andrea	k1gFnSc3	Andrea
obrátil	obrátit	k5eAaPmAgInS	obrátit
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mu	on	k3xPp3gMnSc3	on
seslal	seslat	k5eAaPmAgMnS	seslat
právě	právě	k6eAd1	právě
tento	tento	k3xDgInSc4	tento
rudý	rudý	k2eAgInSc4d1	rudý
prapor	prapor	k1gInSc4	prapor
s	s	k7c7	s
bílým	bílý	k2eAgInSc7d1	bílý
křížem	kříž	k1gInSc7	kříž
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
posilněná	posilněný	k2eAgFnSc1d1	posilněná
podporou	podpora	k1gFnSc7	podpora
z	z	k7c2	z
míst	místo	k1gNnPc2	místo
nejvyšších	vysoký	k2eAgNnPc2d3	nejvyšší
bitvu	bitva	k1gFnSc4	bitva
dokázala	dokázat	k5eAaPmAgFnS	dokázat
vyhrát	vyhrát	k5eAaPmF	vyhrát
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
tento	tento	k3xDgInSc4	tento
prapor	prapor	k1gInSc4	prapor
nechala	nechat	k5eAaPmAgFnS	nechat
jako	jako	k8xS	jako
svůj	svůj	k3xOyFgInSc4	svůj
symbol	symbol	k1gInSc4	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
historických	historický	k2eAgInPc2d1	historický
dokumentů	dokument	k1gInPc2	dokument
tuto	tento	k3xDgFnSc4	tento
legendu	legenda	k1gFnSc4	legenda
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
nejstarší	starý	k2eAgFnSc1d3	nejstarší
vlajkou	vlajka	k1gFnSc7	vlajka
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc1d1	další
vlajky	vlajka	k1gFnPc1	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
Splitflag	Splitflag	k1gInSc4	Splitflag
a	a	k8xC	a
Orlogsflag	Orlogsflag	k1gInSc4	Orlogsflag
mají	mít	k5eAaImIp3nP	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
specifikaci	specifikace	k1gFnSc4	specifikace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
právně	právně	k6eAd1	právně
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc1	dva
odlišné	odlišný	k2eAgFnPc1d1	odlišná
vlajky	vlajka	k1gFnPc1	vlajka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Splitflag	Splitflaga	k1gFnPc2	Splitflaga
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
obdélníkovým	obdélníkový	k2eAgInSc7d1	obdélníkový
listem	list	k1gInSc7	list
zakončeným	zakončený	k2eAgInSc7d1	zakončený
dvěma	dva	k4xCgInPc7	dva
prameny	pramen	k1gInPc7	pramen
<g/>
,	,	kIx,	,
barva	barva	k1gFnSc1	barva
červeného	červený	k2eAgNnSc2d1	červené
pozadí	pozadí	k1gNnSc2	pozadí
má	mít	k5eAaImIp3nS	mít
stejný	stejný	k2eAgInSc1d1	stejný
odstín	odstín	k1gInSc1	odstín
jako	jako	k8xC	jako
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
na	na	k7c4	na
Dannebrog	Dannebrog	k1gInSc4	Dannebrog
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
užívána	užívat	k5eAaImNgFnS	užívat
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Orlogsflag	Orlogsflag	k1gInSc1	Orlogsflag
má	mít	k5eAaImIp3nS	mít
stejný	stejný	k2eAgInSc4d1	stejný
tvar	tvar	k1gInSc4	tvar
jako	jako	k8xS	jako
Splitflag	Splitflag	k1gInSc4	Splitflag
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
barva	barva	k1gFnSc1	barva
pozadí	pozadí	k1gNnSc2	pozadí
je	být	k5eAaImIp3nS	být
tmavší	tmavý	k2eAgMnSc1d2	tmavší
než	než	k8xS	než
na	na	k7c4	na
Dannebrog	Dannebrog	k1gInSc4	Dannebrog
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
užívána	užívat	k5eAaImNgFnS	užívat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Dánska	Dánsko	k1gNnSc2	Dánsko
</s>
</p>
<p>
<s>
Dánská	dánský	k2eAgFnSc1d1	dánská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Dánska	Dánsko	k1gNnSc2	Dánsko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dánská	dánský	k2eAgFnSc1d1	dánská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
