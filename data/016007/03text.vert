<s>
Gestaltismus	Gestaltismus	k1gInSc1
</s>
<s>
Je	být	k5eAaImIp3nS
navrženo	navržen	k2eAgNnSc4d1
začlenění	začlenění	k1gNnSc4
celého	celý	k2eAgInSc2d1
obsahu	obsah	k1gInSc2
článku	článek	k1gInSc2
Gestaltistická	Gestaltistický	k2eAgFnSc1d1
metodologie	metodologie	k1gFnSc1
do	do	k7c2
tohoto	tento	k3xDgInSc2
článku	článek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Odtamtud	odtamtud	k6eAd1
sem	sem	k6eAd1
pak	pak	k6eAd1
má	mít	k5eAaImIp3nS
vést	vést	k5eAaImF
přesměrování	přesměrování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
návrhu	návrh	k1gInSc3
se	se	k3xPyFc4
můžete	moct	k5eAaImIp2nP
vyjádřit	vyjádřit	k5eAaPmF
v	v	k7c6
diskusi	diskuse	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Kanizsův	Kanizsův	k2eAgInSc1d1
trojúhelník	trojúhelník	k1gInSc1
–	–	k?
tento	tento	k3xDgInSc4
optický	optický	k2eAgInSc4d1
klam	klam	k1gInSc4
názorně	názorně	k6eAd1
vysvětluje	vysvětlovat	k5eAaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
si	se	k3xPyFc3
lidské	lidský	k2eAgNnSc4d1
oko	oko	k1gNnSc4
a	a	k8xC
mysl	mysl	k1gFnSc1
subjektivně	subjektivně	k6eAd1
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
nebo	nebo	k8xC
doplňuje	doplňovat	k5eAaImIp3nS
tvary	tvar	k1gInPc4
tam	tam	k6eAd1
<g/>
,	,	kIx,
kde	kde	k6eAd1
nejsou	být	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Tvarová	tvarový	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
(	(	kIx(
<g/>
též	též	k9
gestalt	gestalt	k1gInSc1
psychologie	psychologie	k1gFnSc2
<g/>
,	,	kIx,
gestalt	gestalt	k5eAaPmF
principy	princip	k1gInPc4
nebo	nebo	k8xC
gestaltismus	gestaltismus	k1gInSc4
<g/>
)	)	kIx)
prosazuje	prosazovat	k5eAaImIp3nS
zásadu	zásada	k1gFnSc4
tzv.	tzv.	kA
celostnosti	celostnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Vystupuje	vystupovat	k5eAaImIp3nS
proti	proti	k7c3
orientaci	orientace	k1gFnSc3
psychologických	psychologický	k2eAgFnPc2d1
laboratoří	laboratoř	k1gFnPc2
na	na	k7c6
vyčleňování	vyčleňování	k1gNnSc6
jednoduchých	jednoduchý	k2eAgInPc2d1
elementů	element	k1gInPc2
prožívání	prožívání	k1gNnSc1
a	a	k8xC
chování	chování	k1gNnSc1
<g/>
,	,	kIx,
tedy	tedy	k8xC
proti	proti	k7c3
tzv.	tzv.	kA
elementové	elementový	k2eAgFnSc6d1
psychologii	psychologie	k1gFnSc6
<g/>
,	,	kIx,
hodlající	hodlající	k2eAgMnSc1d1
vyčlenit	vyčlenit	k5eAaPmF
určité	určitý	k2eAgFnPc4d1
základní	základní	k2eAgFnPc4d1
jednotky	jednotka	k1gFnPc4
(	(	kIx(
<g/>
např.	např.	kA
reflexy	reflex	k1gInPc1
<g/>
)	)	kIx)
a	a	k8xC
z	z	k7c2
nich	on	k3xPp3gMnPc2
pak	pak	k6eAd1
jaksi	jaksi	k6eAd1
skládat	skládat	k5eAaImF
větší	veliký	k2eAgInPc4d2
celky	celek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Tvarová	tvarový	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
elementy	element	k1gInPc1
jsou	být	k5eAaImIp3nP
jen	jen	k9
výsledkem	výsledek	k1gInSc7
umělé	umělý	k2eAgFnSc2d1
abstrakce	abstrakce	k1gFnSc2
a	a	k8xC
konstrukce	konstrukce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Psychologická	psychologický	k2eAgFnSc1d1
realita	realita	k1gFnSc1
se	se	k3xPyFc4
vyznačuje	vyznačovat	k5eAaImIp3nS
přirozenými	přirozený	k2eAgInPc7d1
celky	celek	k1gInPc7
fungujícími	fungující	k2eAgInPc7d1
vždy	vždy	k6eAd1
v	v	k7c6
organických	organický	k2eAgFnPc6d1
souvislostech	souvislost	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
živé	živé	k1gNnSc1
směřuje	směřovat	k5eAaImIp3nS
k	k	k7c3
tvarům	tvar	k1gInPc3
<g/>
,	,	kIx,
celkům	celek	k1gInPc3
<g/>
,	,	kIx,
formám	forma	k1gFnPc3
<g/>
,	,	kIx,
a	a	k8xC
touto	tento	k3xDgFnSc7
tendencí	tendence	k1gFnSc7
se	se	k3xPyFc4
řídí	řídit	k5eAaImIp3nS
vnímání	vnímání	k1gNnSc2
<g/>
,	,	kIx,
myšlení	myšlení	k1gNnSc2
<g/>
,	,	kIx,
chování	chování	k1gNnSc2
i	i	k8xC
usilování	usilování	k1gNnSc2
vůle	vůle	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Etymologie	etymologie	k1gFnSc1
</s>
<s>
Gestaltismus	Gestaltismus	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
z	z	k7c2
německého	německý	k2eAgInSc2d1
výrazu	výraz	k1gInSc2
Gestalt	Gestalt	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
je	být	k5eAaImIp3nS
do	do	k7c2
češtiny	čeština	k1gFnSc2
překládán	překládat	k5eAaImNgMnS
jako	jako	k9
podoba	podoba	k1gFnSc1
<g/>
,	,	kIx,
tvar	tvar	k1gInSc1
<g/>
,	,	kIx,
celek	celek	k1gInSc1
<g/>
,	,	kIx,
struktura	struktura	k1gFnSc1
<g/>
,	,	kIx,
útvar	útvar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
je	být	k5eAaImIp3nS
název	název	k1gInSc1
používán	používat	k5eAaImNgInS
v	v	k7c6
původní	původní	k2eAgFnSc6d1
německé	německý	k2eAgFnSc6d1
formě	forma	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
směr	směr	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
jako	jako	k9
reakce	reakce	k1gFnPc4
na	na	k7c4
elementarismus	elementarismus	k1gInSc4
<g/>
:	:	kIx,
kladl	klást	k5eAaImAgInS
důraz	důraz	k1gInSc1
na	na	k7c4
odlišnost	odlišnost	k1gFnSc4
kvality	kvalita	k1gFnSc2
celku	celek	k1gInSc2
jako	jako	k8xS,k8xC
pouhého	pouhý	k2eAgInSc2d1
sumáře	sumář	k1gInSc2
jednotlivých	jednotlivý	k2eAgFnPc2d1
kvalit	kvalita	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
<g/>
.	.	kIx.
„	„	k?
<g/>
Celek	celek	k1gInSc1
je	být	k5eAaImIp3nS
víc	hodně	k6eAd2
než	než	k8xS
suma	suma	k1gFnSc1
částí	část	k1gFnPc2
a	a	k8xC
celky	celek	k1gInPc4
nelze	lze	k6eNd1
vždy	vždy	k6eAd1
vykládat	vykládat	k5eAaImF
z	z	k7c2
částí	část	k1gFnPc2
<g/>
.	.	kIx.
<g/>
“	“	k?
(	(	kIx(
<g/>
Köhler	Köhler	k1gMnSc1
prováděl	provádět	k5eAaImAgMnS
pokusy	pokus	k1gInPc4
na	na	k7c6
šimpanzích	šimpanz	k1gMnPc6
v	v	k7c6
uzavřené	uzavřený	k2eAgFnSc6d1
místnosti	místnost	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgInS
také	také	k9
umístěn	umístěn	k2eAgInSc1d1
banán	banán	k1gInSc1
jako	jako	k8xC,k8xS
pozitivní	pozitivní	k2eAgFnSc1d1
odměna	odměna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Banán	banán	k1gInSc1
byl	být	k5eAaImAgInS
však	však	k9
položen	položit	k5eAaPmNgInS
dostatečně	dostatečně	k6eAd1
vysoko	vysoko	k6eAd1
<g/>
,	,	kIx,
tak	tak	k9
aby	aby	kYmCp3nS
na	na	k7c4
něj	on	k3xPp3gMnSc4
nedosáhli	dosáhnout	k5eNaPmAgMnP
jednoduchým	jednoduchý	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Představitelé	představitel	k1gMnPc1
</s>
<s>
Max	Max	k1gMnSc1
Wertheimer	Wertheimer	k1gMnSc1
(	(	kIx(
<g/>
1880	#num#	k4
<g/>
–	–	k?
<g/>
1943	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Wolfgang	Wolfgang	k1gMnSc1
Köhler	Köhler	k1gMnSc1
(	(	kIx(
<g/>
1887	#num#	k4
<g/>
–	–	k?
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kurt	kurt	k1gInSc1
Koffka	Koffka	k1gFnSc1
(	(	kIx(
<g/>
1886	#num#	k4
<g/>
–	–	k?
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Gestalt	Gestalt	k2eAgInSc4d1
zákony	zákon	k1gInPc4
</s>
<s>
Zákon	zákon	k1gInSc1
blízkosti	blízkost	k1gFnSc2
–	–	k?
tendence	tendence	k1gFnSc1
vnímat	vnímat	k5eAaImF
podobné	podobný	k2eAgInPc4d1
objekty	objekt	k1gInPc4
jako	jako	k8xS,k8xC
skupiny	skupina	k1gFnPc4
nebo	nebo	k8xC
série	série	k1gFnPc4
</s>
<s>
Zákon	zákon	k1gInSc4
podobnosti	podobnost	k1gFnSc2
–	–	k?
smíšené	smíšený	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
podobných	podobný	k2eAgInPc2d1
a	a	k8xC
odlišných	odlišný	k2eAgInPc2d1
objektů	objekt	k1gInPc2
vidíme	vidět	k5eAaImIp1nP
po	po	k7c6
skupinách	skupina	k1gFnPc6
</s>
<s>
Zákon	zákon	k1gInSc4
pokračování	pokračování	k1gNnSc2
<g/>
/	/	kIx~
<g/>
směru	směr	k1gInSc6
–	–	k?
v	v	k7c6
obrazcích	obrazec	k1gInPc6
hledáme	hledat	k5eAaImIp1nP
čáry	čára	k1gFnPc4
s	s	k7c7
nepřerušeným	přerušený	k2eNgNnSc7d1
pokračováním	pokračování	k1gNnSc7
</s>
<s>
Zákon	zákon	k1gInSc1
výstižnosti	výstižnost	k1gFnSc2
(	(	kIx(
<g/>
Prägnanz	Prägnanz	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
tendence	tendence	k1gFnSc1
vidět	vidět	k5eAaImF
nejjednodušší	jednoduchý	k2eAgInSc4d3
tvar	tvar	k1gInSc4
</s>
<s>
Zákon	zákon	k1gInSc1
dobrého	dobrý	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
–	–	k?
tendence	tendence	k1gFnSc1
doplňovat	doplňovat	k5eAaImF
obrazce	obrazec	k1gInPc4
</s>
<s>
Vnímání	vnímání	k1gNnSc1
figury	figura	k1gFnSc2
a	a	k8xC
pozadí	pozadí	k1gNnSc2
–	–	k?
schopnost	schopnost	k1gFnSc1
mysli	mysl	k1gFnSc2
zaměřit	zaměřit	k5eAaPmF
pozornost	pozornost	k1gFnSc4
na	na	k7c4
smysluplný	smysluplný	k2eAgInSc4d1
tvar	tvar	k1gInSc4
a	a	k8xC
ignorovat	ignorovat	k5eAaImF
zbytek	zbytek	k1gInSc4
</s>
<s>
Konstantnost	konstantnost	k1gFnSc1
velikosti	velikost	k1gFnSc2
–	–	k?
schopnost	schopnost	k1gFnSc1
vnímání	vnímání	k1gNnSc2
perspektivy	perspektiva	k1gFnSc2
</s>
<s>
Související	související	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Synergie	synergie	k1gFnSc1
</s>
<s>
Gestalt	Gestalt	k1gInSc1
terapie	terapie	k1gFnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Psychologie	psychologie	k1gFnSc1
Teoretické	teoretický	k2eAgFnSc2d1
psychologické	psychologický	k2eAgFnSc2d1
disciplíny	disciplína	k1gFnSc2
</s>
<s>
Základní	základní	k2eAgFnPc1d1
psychologické	psychologický	k2eAgFnPc1d1
disciplíny	disciplína	k1gFnPc1
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
(	(	kIx(
<g/>
Psychologie	psychologie	k1gFnSc1
barev	barva	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
Biologická	biologický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
(	(	kIx(
<g/>
Psychobiologie	Psychobiologie	k1gFnSc1
•	•	k?
Neuropsychologie	Neuropsychologie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Psychologie	psychologie	k1gFnSc1
osobnosti	osobnost	k1gFnSc2
•	•	k?
Vývojová	vývojový	k2eAgFnSc1d1
(	(	kIx(
<g/>
ontogenetická	ontogenetický	k2eAgFnSc1d1
<g/>
)	)	kIx)
psychologie	psychologie	k1gFnSc1
•	•	k?
Sociální	sociální	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Psychologická	psychologický	k2eAgFnSc1d1
metodologie	metodologie	k1gFnSc1
•	•	k?
Psychopatologie	psychopatologie	k1gFnSc2
•	•	k?
Dějiny	dějiny	k1gFnPc1
psychologie	psychologie	k1gFnSc2
•	•	k?
Srovnávací	srovnávací	k2eAgNnSc1d1
(	(	kIx(
<g/>
komparativní	komparativní	k2eAgFnSc1d1
<g/>
)	)	kIx)
psychologie	psychologie	k1gFnSc1
Speciální	speciální	k2eAgFnSc2d1
psychologické	psychologický	k2eAgFnSc2d1
disciplíny	disciplína	k1gFnSc2
</s>
<s>
Diferenciální	diferenciální	k2eAgFnSc1d1
(	(	kIx(
<g/>
diferenciační	diferenciační	k2eAgFnSc1d1
<g/>
)	)	kIx)
psychologie	psychologie	k1gFnSc1
•	•	k?
Biopsychologie	Biopsychologie	k1gFnSc2
•	•	k?
Psychofyziologie	psychofyziologie	k1gFnSc2
•	•	k?
Genetická	genetický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Chronopsychologie	Chronopsychologie	k1gFnSc1
•	•	k?
Psychometrie	Psychometrie	k1gFnSc1
=	=	kIx~
psychometrika	psychometrika	k1gFnSc1
•	•	k?
Patopsychologie	patopsychologie	k1gFnSc2
•	•	k?
Farmakopsychologie	Farmakopsychologie	k1gFnSc2
•	•	k?
Zoopsychologie	zoopsychologie	k1gFnSc2
•	•	k?
Kulturně	kulturně	k6eAd1
srovnávací	srovnávací	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
=	=	kIx~
mezikulturní	mezikulturní	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Geopsychologie	Geopsychologie	k1gFnSc1
•	•	k?
Psychodiagnostika	psychodiagnostika	k1gFnSc1
•	•	k?
Psycholingvistika	psycholingvistika	k1gFnSc1
•	•	k?
Psychofyzika	psychofyzika	k1gFnSc1
•	•	k?
Matematická	matematický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Psychologie	psychologie	k1gFnSc2
myšlení	myšlení	k1gNnSc2
•	•	k?
Smyslová	smyslový	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Kognitivní	kognitivní	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Percepční	percepční	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
</s>
<s>
Aplikované	aplikovaný	k2eAgFnPc1d1
(	(	kIx(
<g/>
užité	užitý	k2eAgFnPc1d1
<g/>
)	)	kIx)
psychologické	psychologický	k2eAgFnPc1d1
disciplíny	disciplína	k1gFnPc1
(	(	kIx(
<g/>
aplikovaná	aplikovaný	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Poradenská	poradenský	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Klinická	klinický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Lékařská	lékařský	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Zdravotnická	zdravotnický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Duševní	duševní	k2eAgFnSc1d1
hygiena	hygiena	k1gFnSc1
=	=	kIx~
psychohygiena	psychohygiena	k1gFnSc1
•	•	k?
Psychologie	psychologie	k1gFnSc2
zdraví	zdraví	k1gNnSc2
•	•	k?
Environmentální	environmentální	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
=	=	kIx~
ekologická	ekologický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
=	=	kIx~
psychologie	psychologie	k1gFnSc1
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
(	(	kIx(
<g/>
Psychologie	psychologie	k1gFnSc2
ochrany	ochrana	k1gFnSc2
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Ekopsychologie	Ekopsychologie	k1gFnSc2
•	•	k?
Environmentální	environmentální	k2eAgFnSc2d1
komunikace	komunikace	k1gFnSc2
•	•	k?
Psychologie	psychologie	k1gFnSc2
dítěte	dítě	k1gNnSc2
•	•	k?
Psychologie	psychologie	k1gFnSc1
mládeže	mládež	k1gFnSc2
•	•	k?
Pedagogická	pedagogický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Školní	školní	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
•	•	k?
Psychologie	psychologie	k1gFnSc2
vzdělávání	vzdělávání	k1gNnSc2
•	•	k?
Interkulturní	Interkulturní	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
•	•	k?
Psychologie	psychologie	k1gFnPc4
cestovního	cestovní	k2eAgInSc2d1
ruchu	ruch	k1gInSc2
•	•	k?
Psychologie	psychologie	k1gFnPc4
volného	volný	k2eAgInSc2d1
času	čas	k1gInSc2
•	•	k?
Psychologie	psychologie	k1gFnSc1
sportu	sport	k1gInSc2
•	•	k?
Psychologie	psychologie	k1gFnSc2
kultury	kultura	k1gFnSc2
•	•	k?
Psychologie	psychologie	k1gFnSc2
umění	umění	k1gNnSc2
•	•	k?
Bibliopsychologie	Bibliopsychologie	k1gFnSc2
•	•	k?
Psychologie	psychologie	k1gFnSc2
čtenáře	čtenář	k1gMnSc2
•	•	k?
Psychologie	psychologie	k1gFnSc2
náboženství	náboženství	k1gNnSc2
•	•	k?
Politická	politický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Právní	právní	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Policejní	policejní	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
•	•	k?
Kriminální	kriminální	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
•	•	k?
Forenzní	forenzní	k2eAgNnSc1d1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
soudní	soudní	k2eAgInSc1d1
<g/>
)	)	kIx)
psychologie	psychologie	k1gFnSc1
(	(	kIx(
<g/>
Kriminologická	kriminologický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Penitenciární	Penitenciární	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
=	=	kIx~
psychologie	psychologie	k1gFnSc1
vězeňství	vězeňství	k1gNnSc2
•	•	k?
Postpenitenciární	Postpenitenciární	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Politické	politický	k2eAgNnSc1d1
profilování	profilování	k1gNnSc1
•	•	k?
Psychologické	psychologický	k2eAgNnSc4d1
profilování	profilování	k1gNnSc4
•	•	k?
Psychologie	psychologie	k1gFnSc2
katastrof	katastrofa	k1gFnPc2
•	•	k?
Vojenská	vojenský	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Kosmická	kosmický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Psychologie	psychologie	k1gFnSc1
konfliktu	konflikt	k1gInSc2
•	•	k?
Psychologie	psychologie	k1gFnSc2
práce	práce	k1gFnSc2
a	a	k8xC
organizace	organizace	k1gFnSc2
=	=	kIx~
psychologie	psychologie	k1gFnSc1
práce	práce	k1gFnSc2
=	=	kIx~
průmyslová	průmyslový	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
Inženýrská	inženýrský	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Personální	personální	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
=	=	kIx~
poradenská	poradenský	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
práce	práce	k1gFnSc2
•	•	k?
Psychologie	psychologie	k1gFnSc2
organizace	organizace	k1gFnSc2
a	a	k8xC
řízení	řízení	k1gNnSc2
=	=	kIx~
sociální	sociální	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
práce	práce	k1gFnSc2
/	/	kIx~
<g/>
Psychologie	psychologie	k1gFnSc1
řízení	řízení	k1gNnSc2
<g/>
/	/	kIx~
•	•	k?
Psychologie	psychologie	k1gFnSc2
pracovní	pracovní	k2eAgFnSc2d1
přípravy	příprava	k1gFnSc2
•	•	k?
Ekonomická	ekonomický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Psychologie	psychologie	k1gFnSc1
dopravy	doprava	k1gFnSc2
=	=	kIx~
dopravní	dopravní	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Psychologie	psychologie	k1gFnSc1
organizace	organizace	k1gFnSc2
•	•	k?
Manažerská	manažerský	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Marketingová	marketingový	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Psychologie	psychologie	k1gFnSc1
trhu	trh	k1gInSc2
(	(	kIx(
<g/>
Psychologie	psychologie	k1gFnSc1
propagace	propagace	k1gFnSc2
•	•	k?
Psychologie	psychologie	k1gFnSc2
reklamy	reklama	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Psychologie	psychologie	k1gFnSc1
obchodu	obchod	k1gInSc2
Psychologické	psychologický	k2eAgInPc4d1
směry	směr	k1gInPc4
</s>
<s>
Behaviorismus	behaviorismus	k1gInSc1
•	•	k?
Neobehaviorismus	Neobehaviorismus	k1gInSc1
•	•	k?
Gestaltismus	Gestaltismus	k1gInSc1
=	=	kIx~
gestaltpsychologie	gestaltpsychologie	k1gFnSc1
=	=	kIx~
gestaltistická	gestaltistický	k2eAgFnSc1d1
(	(	kIx(
<g/>
tvarová	tvarový	k2eAgFnSc1d1
<g/>
,	,	kIx,
celostní	celostní	k2eAgFnSc1d1
<g/>
)	)	kIx)
psychologie	psychologie	k1gFnSc1
•	•	k?
Neogestaltismus	Neogestaltismus	k1gInSc1
•	•	k?
Psychoanalýza	psychoanalýza	k1gFnSc1
•	•	k?
Neopsychoanalýza	Neopsychoanalýza	k1gFnSc1
•	•	k?
Kognitivní	kognitivní	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Humanistická	humanistický	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Experimentální	experimentální	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Hlubinná	hlubinný	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
Psychologické	psychologický	k2eAgFnSc2d1
disciplíny	disciplína	k1gFnSc2
•	•	k?
Předvědecká	předvědecký	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Pozitivní	pozitivní	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
•	•	k?
Psycholog	psycholog	k1gMnSc1
•	•	k?
Kategorie	kategorie	k1gFnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4020725-0	4020725-0	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
9197	#num#	k4
</s>
