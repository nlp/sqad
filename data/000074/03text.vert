<s>
Lingvistika	lingvistika	k1gFnSc1	lingvistika
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
psáno	psát	k5eAaImNgNnS	psát
linguistika	linguistika	k1gFnSc1	linguistika
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
jazykověda	jazykověda	k1gFnSc1	jazykověda
je	být	k5eAaImIp3nS	být
věda	věda	k1gFnSc1	věda
zkoumající	zkoumající	k2eAgFnSc2d1	zkoumající
přirozený	přirozený	k2eAgInSc4d1	přirozený
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
ji	on	k3xPp3gFnSc4	on
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
obecnou	obecný	k2eAgFnSc4d1	obecná
lingvistiku	lingvistika	k1gFnSc4	lingvistika
<g/>
,	,	kIx,	,
aplikovanou	aplikovaný	k2eAgFnSc4d1	aplikovaná
lingvistiku	lingvistika	k1gFnSc4	lingvistika
a	a	k8xC	a
na	na	k7c4	na
jazykovědy	jazykověda	k1gFnPc4	jazykověda
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
jazyků	jazyk	k1gInPc2	jazyk
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnPc2	jejich
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ně	on	k3xPp3gInPc4	on
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
anglická	anglický	k2eAgFnSc1d1	anglická
filologie	filologie	k1gFnSc1	filologie
neboli	neboli	k8xC	neboli
anglistika	anglistika	k1gFnSc1	anglistika
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
např.	např.	kA	např.
bohemistika	bohemistika	k1gFnSc1	bohemistika
<g/>
,	,	kIx,	,
germanistika	germanistika	k1gFnSc1	germanistika
<g/>
,	,	kIx,	,
slovakistika	slovakistika	k1gFnSc1	slovakistika
<g/>
,	,	kIx,	,
lusitanistika	lusitanistika	k1gFnSc1	lusitanistika
<g/>
,	,	kIx,	,
turkologie	turkologie	k1gFnSc1	turkologie
<g/>
;	;	kIx,	;
afrikanistika	afrikanistika	k1gFnSc1	afrikanistika
apod.	apod.	kA	apod.
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gInSc4	jejich
seznam	seznam	k1gInSc4	seznam
viz	vidět	k5eAaImRp2nS	vidět
článek	článek	k1gInSc4	článek
Filologie	filologie	k1gFnPc4	filologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
filologie	filologie	k1gFnSc2	filologie
ovšem	ovšem	k9	ovšem
znamená	znamenat	k5eAaImIp3nS	znamenat
široce	široko	k6eAd1	široko
pojaté	pojatý	k2eAgNnSc4d1	pojaté
studium	studium	k1gNnSc4	studium
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
literatury	literatura	k1gFnSc2	literatura
i	i	k8xC	i
kulturních	kulturní	k2eAgFnPc2d1	kulturní
<g/>
,	,	kIx,	,
historických	historický	k2eAgFnPc2d1	historická
a	a	k8xC	a
zeměpisných	zeměpisný	k2eAgFnPc2d1	zeměpisná
specifik	specifika	k1gFnPc2	specifika
dané	daný	k2eAgFnSc2d1	daná
oblasti	oblast	k1gFnSc2	oblast
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
reálie	reálie	k1gFnSc2	reálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lingvistiku	lingvistika	k1gFnSc4	lingvistika
je	být	k5eAaImIp3nS	být
také	také	k9	také
možné	možný	k2eAgNnSc1d1	možné
chápat	chápat	k5eAaImF	chápat
jako	jako	k9	jako
soubor	soubor	k1gInSc4	soubor
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
jazykovědných	jazykovědný	k2eAgFnPc2d1	jazykovědná
disciplín	disciplína	k1gFnPc2	disciplína
(	(	kIx(	(
<g/>
především	především	k9	především
fonologie	fonologie	k1gFnSc1	fonologie
<g/>
,	,	kIx,	,
morfologie	morfologie	k1gFnSc1	morfologie
<g/>
,	,	kIx,	,
lexikologie	lexikologie	k1gFnSc1	lexikologie
<g/>
,	,	kIx,	,
syntaxe	syntaxe	k1gFnSc1	syntaxe
a	a	k8xC	a
textové	textový	k2eAgFnPc1d1	textová
lingvistiky	lingvistika	k1gFnPc1	lingvistika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jazykem	jazyk	k1gInSc7	jazyk
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
také	také	k9	také
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
disciplín	disciplína	k1gFnPc2	disciplína
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
,	,	kIx,	,
informatika	informatika	k1gFnSc1	informatika
<g/>
,	,	kIx,	,
filosofie	filosofie	k1gFnSc1	filosofie
<g/>
,	,	kIx,	,
biologie	biologie	k1gFnSc1	biologie
<g/>
,	,	kIx,	,
anatomie	anatomie	k1gFnSc1	anatomie
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
sociologie	sociologie	k1gFnSc2	sociologie
<g/>
,	,	kIx,	,
antropologie	antropologie	k1gFnSc2	antropologie
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
také	také	k9	také
vznikají	vznikat	k5eAaImIp3nP	vznikat
hraniční	hraniční	k2eAgFnPc4d1	hraniční
jazykovědné	jazykovědný	k2eAgFnPc4d1	jazykovědná
disciplíny	disciplína	k1gFnPc4	disciplína
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
psycholingvistika	psycholingvistika	k1gFnSc1	psycholingvistika
<g/>
,	,	kIx,	,
sociolingvistika	sociolingvistika	k1gFnSc1	sociolingvistika
<g/>
,	,	kIx,	,
neurolingvistika	neurolingvistika	k1gFnSc1	neurolingvistika
aj.	aj.	kA	aj.
Deskriptivní	deskriptivní	k2eAgFnSc1d1	deskriptivní
lingvistika	lingvistika	k1gFnSc1	lingvistika
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
deskriptivní	deskriptivní	k2eAgFnSc2d1	deskriptivní
lingvistiky	lingvistika	k1gFnSc2	lingvistika
je	být	k5eAaImIp3nS	být
popsat	popsat	k5eAaPmF	popsat
jazykový	jazykový	k2eAgInSc1d1	jazykový
systém	systém	k1gInSc1	systém
a	a	k8xC	a
popř.	popř.	kA	popř.
způsoby	způsob	k1gInPc1	způsob
jeho	on	k3xPp3gNnSc2	on
užívání	užívání	k1gNnSc2	užívání
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
komunikačních	komunikační	k2eAgFnPc6d1	komunikační
situacích	situace	k1gFnPc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
práce	práce	k1gFnSc2	práce
deskriptivní	deskriptivní	k2eAgFnSc2d1	deskriptivní
lingvistiky	lingvistika	k1gFnSc2	lingvistika
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
slovníky	slovník	k1gInPc1	slovník
(	(	kIx(	(
<g/>
lexikony	lexikon	k1gInPc1	lexikon
<g/>
)	)	kIx)	)
či	či	k8xC	či
mluvnice	mluvnice	k1gFnSc2	mluvnice
(	(	kIx(	(
<g/>
gramatiky	gramatika	k1gFnSc2	gramatika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
práce	práce	k1gFnSc1	práce
popisující	popisující	k2eAgFnSc4d1	popisující
slovní	slovní	k2eAgFnSc4d1	slovní
zásobu	zásoba	k1gFnSc4	zásoba
a	a	k8xC	a
strukturu	struktura	k1gFnSc4	struktura
(	(	kIx(	(
<g/>
gramatiku	gramatika	k1gFnSc4	gramatika
<g/>
)	)	kIx)	)
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Teoretická	teoretický	k2eAgFnSc1d1	teoretická
lingvistika	lingvistika	k1gFnSc1	lingvistika
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
odvětví	odvětví	k1gNnSc1	odvětví
lingvistiky	lingvistika	k1gFnSc2	lingvistika
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
obecné	obecný	k2eAgInPc1d1	obecný
principy	princip	k1gInPc1	princip
fungování	fungování	k1gNnSc2	fungování
přirozeného	přirozený	k2eAgInSc2d1	přirozený
lidského	lidský	k2eAgInSc2d1	lidský
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
často	často	k6eAd1	často
využívá	využívat	k5eAaPmIp3nS	využívat
výsledky	výsledek	k1gInPc4	výsledek
deskriptivní	deskriptivní	k2eAgFnSc2d1	deskriptivní
lingvistiky	lingvistika	k1gFnSc2	lingvistika
<g/>
.	.	kIx.	.
</s>
<s>
Teoreticko-lingvistické	teoretickoingvistický	k2eAgNnSc1d1	teoreticko-lingvistický
zkoumání	zkoumání	k1gNnSc1	zkoumání
se	se	k3xPyFc4	se
zakládá	zakládat	k5eAaImIp3nS	zakládat
na	na	k7c6	na
obecné	obecný	k2eAgFnSc6d1	obecná
vědecké	vědecký	k2eAgFnSc6d1	vědecká
metodě	metoda	k1gFnSc6	metoda
<g/>
:	:	kIx,	:
jeho	jeho	k3xOp3gInSc7	jeho
výstupem	výstup	k1gInSc7	výstup
jsou	být	k5eAaImIp3nP	být
explicitní	explicitní	k2eAgFnPc1d1	explicitní
<g/>
,	,	kIx,	,
formálně	formálně	k6eAd1	formálně
zpracované	zpracovaný	k2eAgFnPc1d1	zpracovaná
teorie	teorie	k1gFnPc1	teorie
a	a	k8xC	a
hypotézy	hypotéza	k1gFnPc1	hypotéza
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
platnost	platnost	k1gFnSc4	platnost
je	být	k5eAaImIp3nS	být
následně	následně	k6eAd1	následně
testována	testovat	k5eAaImNgFnS	testovat
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
dat	datum	k1gNnPc2	datum
z	z	k7c2	z
konkrétních	konkrétní	k2eAgInPc2d1	konkrétní
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
odvětví	odvětví	k1gNnPc1	odvětví
teoretické	teoretický	k2eAgFnSc2d1	teoretická
lingvistiky	lingvistika	k1gFnSc2	lingvistika
se	se	k3xPyFc4	se
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
liší	lišit	k5eAaImIp3nP	lišit
specifickými	specifický	k2eAgFnPc7d1	specifická
metodami	metoda	k1gFnPc7	metoda
testování	testování	k1gNnSc2	testování
<g/>
.	.	kIx.	.
</s>
<s>
Aplikovaná	aplikovaný	k2eAgFnSc1d1	aplikovaná
lingvistika	lingvistika	k1gFnSc1	lingvistika
využívá	využívat	k5eAaImIp3nS	využívat
poznatků	poznatek	k1gInPc2	poznatek
deskriptivní	deskriptivní	k2eAgFnSc1d1	deskriptivní
a	a	k8xC	a
teoretické	teoretický	k2eAgFnPc1d1	teoretická
lingvistiky	lingvistika	k1gFnPc1	lingvistika
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
oblasti	oblast	k1gFnPc4	oblast
aplikované	aplikovaný	k2eAgFnSc2d1	aplikovaná
lingvistiky	lingvistika	k1gFnSc2	lingvistika
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
jazyková	jazykový	k2eAgFnSc1d1	jazyková
terapie	terapie	k1gFnSc1	terapie
(	(	kIx(	(
<g/>
logopedie	logopedie	k1gFnSc1	logopedie
<g/>
,	,	kIx,	,
terapie	terapie	k1gFnSc1	terapie
afázie	afázie	k1gFnSc1	afázie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výuka	výuka	k1gFnSc1	výuka
cizích	cizí	k2eAgInPc2d1	cizí
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
forenzní	forenzní	k2eAgFnSc1d1	forenzní
lingvistika	lingvistika	k1gFnSc1	lingvistika
<g/>
,	,	kIx,	,
či	či	k8xC	či
manuální	manuální	k2eAgInSc4d1	manuální
a	a	k8xC	a
strojový	strojový	k2eAgInSc4d1	strojový
překlad	překlad	k1gInSc4	překlad
<g/>
.	.	kIx.	.
</s>
<s>
Preskriptivní	Preskriptivní	k2eAgFnSc1d1	Preskriptivní
lingvistika	lingvistika	k1gFnSc1	lingvistika
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc2	smysl
lingvistika	lingvistika	k1gFnSc1	lingvistika
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
nezabývá	zabývat	k5eNaImIp3nS	zabývat
přirozeným	přirozený	k2eAgInSc7d1	přirozený
lidským	lidský	k2eAgInSc7d1	lidský
jazykem	jazyk	k1gInSc7	jazyk
jako	jako	k8xS	jako
takovým	takový	k3xDgNnSc7	takový
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
tzv.	tzv.	kA	tzv.
spisovný	spisovný	k2eAgInSc4d1	spisovný
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
jazykových	jazykový	k2eAgInPc2d1	jazykový
prostředků	prostředek	k1gInPc2	prostředek
(	(	kIx(	(
<g/>
ať	ať	k8xC	ať
už	už	k6eAd1	už
mluvených	mluvený	k2eAgFnPc2d1	mluvená
či	či	k8xC	či
psaných	psaný	k2eAgFnPc2d1	psaná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
či	či	k8xC	či
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
používány	používat	k5eAaImNgInP	používat
ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
<g/>
,	,	kIx,	,
v	v	k7c6	v
oficiálních	oficiální	k2eAgInPc6d1	oficiální
sdělovacích	sdělovací	k2eAgInPc6d1	sdělovací
prostředcích	prostředek	k1gInPc6	prostředek
či	či	k8xC	či
v	v	k7c6	v
komunikačních	komunikační	k2eAgFnPc6d1	komunikační
situacích	situace	k1gFnPc6	situace
formálního	formální	k2eAgInSc2d1	formální
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Spisovný	spisovný	k2eAgInSc1d1	spisovný
jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
kodifikován	kodifikovat	k5eAaBmNgInS	kodifikovat
v	v	k7c6	v
jazykových	jazykový	k2eAgFnPc6d1	jazyková
příručkách	příručka	k1gFnPc6	příručka
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
především	především	k9	především
v	v	k7c6	v
Pravidlech	pravidlo	k1gNnPc6	pravidlo
českého	český	k2eAgInSc2d1	český
pravopisu	pravopis	k1gInSc2	pravopis
a	a	k8xC	a
slovnících	slovník	k1gInPc6	slovník
(	(	kIx(	(
<g/>
vydaných	vydaný	k2eAgInPc2d1	vydaný
ÚJČ	ÚJČ	kA	ÚJČ
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
psanou	psaný	k2eAgFnSc4d1	psaná
podobu	podoba	k1gFnSc4	podoba
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
výslovnostní	výslovnostní	k2eAgFnSc1d1	výslovnostní
norma	norma	k1gFnSc1	norma
pro	pro	k7c4	pro
mluvenou	mluvený	k2eAgFnSc4d1	mluvená
podobu	podoba	k1gFnSc4	podoba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
užívání	užívání	k1gNnSc1	užívání
však	však	k9	však
většinou	většina	k1gFnSc7	většina
není	být	k5eNaImIp3nS	být
vynuceno	vynutit	k5eAaPmNgNnS	vynutit
zákonem	zákon	k1gInSc7	zákon
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
charakter	charakter	k1gInSc4	charakter
doporučení	doporučení	k1gNnSc2	doporučení
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
přirozené	přirozený	k2eAgInPc1d1	přirozený
jazyky	jazyk	k1gInPc1	jazyk
oficiálně	oficiálně	k6eAd1	oficiálně
kodifikovanou	kodifikovaný	k2eAgFnSc4d1	kodifikovaná
podobu	podoba	k1gFnSc4	podoba
spisovného	spisovný	k2eAgInSc2d1	spisovný
jazyka	jazyk	k1gInSc2	jazyk
ani	ani	k8xC	ani
nemají	mít	k5eNaImIp3nP	mít
(	(	kIx(	(
<g/>
např.	např.	kA	např.
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
komplexní	komplexní	k2eAgInSc4d1	komplexní
systém	systém	k1gInSc4	systém
a	a	k8xC	a
skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
podsystémů	podsystém	k1gInPc2	podsystém
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
nazývaných	nazývaný	k2eAgInPc2d1	nazývaný
"	"	kIx"	"
<g/>
jazykové	jazykový	k2eAgInPc4d1	jazykový
plány	plán	k1gInPc4	plán
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
navzájem	navzájem	k6eAd1	navzájem
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
<g/>
.	.	kIx.	.
</s>
<s>
Deskriptivní	deskriptivní	k2eAgFnSc1d1	deskriptivní
a	a	k8xC	a
teoretická	teoretický	k2eAgFnSc1d1	teoretická
lingvistika	lingvistika	k1gFnSc1	lingvistika
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
toho	ten	k3xDgMnSc2	ten
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
následujících	následující	k2eAgFnPc2d1	následující
kategorií	kategorie	k1gFnPc2	kategorie
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
české	český	k2eAgFnSc2d1	Česká
lingvistické	lingvistický	k2eAgFnSc2d1	lingvistická
tradice	tradice	k1gFnSc2	tradice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lexikologie	lexikologie	k1gFnSc1	lexikologie
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
slovní	slovní	k2eAgFnSc4d1	slovní
zásobu	zásoba	k1gFnSc4	zásoba
<g/>
,	,	kIx,	,
lexikální	lexikální	k2eAgInSc4d1	lexikální
význam	význam	k1gInSc4	význam
a	a	k8xC	a
formu	forma	k1gFnSc4	forma
<g/>
,	,	kIx,	,
inherentní	inherentní	k2eAgInPc4d1	inherentní
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgNnPc7d1	jednotlivé
slovy	slovo	k1gNnPc7	slovo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
synonymie	synonymie	k1gFnSc1	synonymie
-	-	kIx~	-
dvě	dva	k4xCgFnPc1	dva
různé	různý	k2eAgFnPc1d1	různá
slovní	slovní	k2eAgFnPc1d1	slovní
formy	forma	k1gFnPc1	forma
pro	pro	k7c4	pro
tentýž	týž	k3xTgInSc4	týž
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
homonymie	homonymie	k1gFnPc4	homonymie
-	-	kIx~	-
jedna	jeden	k4xCgFnSc1	jeden
slovní	slovní	k2eAgFnSc1d1	slovní
forma	forma	k1gFnSc1	forma
pro	pro	k7c4	pro
více	hodně	k6eAd2	hodně
významů	význam	k1gInPc2	význam
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
expresivitu	expresivita	k1gFnSc4	expresivita
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vulgárnost	vulgárnost	k1gFnSc1	vulgárnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Lexikologie	lexikologie	k1gFnPc1	lexikologie
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
lexikografii	lexikografie	k1gFnSc4	lexikografie
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
tvorbu	tvorba	k1gFnSc4	tvorba
slovníků	slovník	k1gInPc2	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Fonetika	fonetika	k1gFnSc1	fonetika
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
zvukovou	zvukový	k2eAgFnSc4d1	zvuková
(	(	kIx(	(
<g/>
akustickou	akustický	k2eAgFnSc4d1	akustická
<g/>
,	,	kIx,	,
auditivní	auditivní	k2eAgFnSc4d1	auditivní
<g/>
,	,	kIx,	,
artikulační	artikulační	k2eAgFnSc4d1	artikulační
<g/>
)	)	kIx)	)
stránku	stránka	k1gFnSc4	stránka
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Fonologie	fonologie	k1gFnSc1	fonologie
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
zvukový	zvukový	k2eAgInSc4d1	zvukový
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
(	(	kIx(	(
<g/>
i	i	k9	i
<g/>
)	)	kIx)	)
inventář	inventář	k1gInSc1	inventář
zvukových	zvukový	k2eAgInPc2d1	zvukový
segmentů	segment	k1gInPc2	segment
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
fonémů	foném	k1gInPc2	foném
<g/>
)	)	kIx)	)
schopných	schopný	k2eAgMnPc2d1	schopný
rozlišit	rozlišit	k5eAaPmF	rozlišit
význam	význam	k1gInSc4	význam
větších	veliký	k2eAgInPc2d2	veliký
celků	celek	k1gInPc2	celek
<g/>
,	,	kIx,	,
pravidla	pravidlo	k1gNnSc2	pravidlo
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgNnPc2	jenž
se	se	k3xPyFc4	se
segmenty	segment	k1gInPc7	segment
chovají	chovat	k5eAaImIp3nP	chovat
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
ostatních	ostatní	k2eAgInPc2d1	ostatní
segmentů	segment	k1gInPc2	segment
(	(	kIx(	(
<g/>
např.	např.	kA	např.
asimilace	asimilace	k1gFnSc2	asimilace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
(	(	kIx(	(
<g/>
ii	ii	k?	ii
<g/>
)	)	kIx)	)
inventář	inventář	k1gInSc1	inventář
a	a	k8xC	a
pravidla	pravidlo	k1gNnPc1	pravidlo
týkající	týkající	k2eAgNnPc1d1	týkající
se	se	k3xPyFc4	se
suprasegmentálních	suprasegmentální	k2eAgFnPc2d1	suprasegmentální
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
nadsegmentálních	nadsegmentální	k2eAgFnPc2d1	nadsegmentální
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
prostředků	prostředek	k1gInPc2	prostředek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
intonace	intonace	k1gFnSc2	intonace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Morfologie	morfologie	k1gFnSc1	morfologie
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
tvarosloví	tvarosloví	k1gNnSc4	tvarosloví
<g/>
)	)	kIx)	)
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
inventář	inventář	k1gInSc4	inventář
nejmenších	malý	k2eAgInPc2d3	nejmenší
významonosných	významonosný	k2eAgInPc2d1	významonosný
či	či	k8xC	či
význam	význam	k1gInSc1	význam
modifikujících	modifikující	k2eAgInPc2d1	modifikující
jazykových	jazykový	k2eAgInPc2d1	jazykový
výrazů	výraz	k1gInPc2	výraz
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
morfémů	morfém	k1gInPc2	morfém
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
-ost	st	k1gMnSc1	-ost
<g/>
"	"	kIx"	"
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
"	"	kIx"	"
<g/>
libost	libost	k1gFnSc1	libost
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
slab-	slab-	k?	slab-
<g/>
"	"	kIx"	"
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
"	"	kIx"	"
<g/>
slabý	slabý	k2eAgMnSc1d1	slabý
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
pravidla	pravidlo	k1gNnPc1	pravidlo
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
tyto	tento	k3xDgInPc1	tento
"	"	kIx"	"
<g/>
atomické	atomický	k2eAgInPc1d1	atomický
<g/>
"	"	kIx"	"
výrazy	výraz	k1gInPc1	výraz
užívají	užívat	k5eAaImIp3nP	užívat
a	a	k8xC	a
spojují	spojovat	k5eAaImIp3nP	spojovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
jednoduchá	jednoduchý	k2eAgNnPc4d1	jednoduché
či	či	k8xC	či
složená	složený	k2eAgNnPc4d1	složené
slova	slovo	k1gNnPc4	slovo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
správně	správně	k6eAd1	správně
utvořené	utvořený	k2eAgNnSc4d1	utvořené
"	"	kIx"	"
<g/>
slabost	slabost	k1gFnSc4	slabost
<g/>
"	"	kIx"	"
vs	vs	k?	vs
<g/>
.	.	kIx.	.
neexistující	existující	k2eNgInSc4d1	neexistující
"	"	kIx"	"
<g/>
ostslab	ostslab	k1gInSc4	ostslab
<g/>
"	"	kIx"	"
či	či	k8xC	či
správně	správně	k6eAd1	správně
utvořené	utvořený	k2eAgFnPc4d1	utvořená
"	"	kIx"	"
<g/>
slabomyslný	slabomyslný	k2eAgInSc4d1	slabomyslný
<g/>
"	"	kIx"	"
vs	vs	k?	vs
<g/>
.	.	kIx.	.
neexistující	existující	k2eNgInSc4d1	neexistující
"	"	kIx"	"
<g/>
myslnoslabý	myslnoslabý	k2eAgInSc4d1	myslnoslabý
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Syntax	syntax	k1gFnSc1	syntax
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
větná	větný	k2eAgFnSc1d1	větná
skladba	skladba	k1gFnSc1	skladba
<g/>
)	)	kIx)	)
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
inventář	inventář	k1gInSc4	inventář
lexikálních	lexikální	k2eAgFnPc2d1	lexikální
a	a	k8xC	a
funkčních	funkční	k2eAgFnPc2d1	funkční
slovních	slovní	k2eAgFnPc2d1	slovní
kategorií	kategorie	k1gFnPc2	kategorie
(	(	kIx(	(
<g/>
druhů	druh	k1gInPc2	druh
<g/>
)	)	kIx)	)
a	a	k8xC	a
zejména	zejména	k9	zejména
pravidla	pravidlo	k1gNnPc1	pravidlo
jejich	jejich	k3xOp3gNnSc4	jejich
spojování	spojování	k1gNnSc4	spojování
do	do	k7c2	do
větších	veliký	k2eAgInPc2d2	veliký
celků	celek	k1gInPc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
lexikálních	lexikální	k2eAgFnPc2d1	lexikální
kategorií	kategorie	k1gFnPc2	kategorie
jsou	být	k5eAaImIp3nP	být
substantiva	substantivum	k1gNnPc1	substantivum
<g/>
,	,	kIx,	,
adjektiva	adjektivum	k1gNnSc2	adjektivum
či	či	k8xC	či
verba	verbum	k1gNnSc2	verbum
<g/>
,	,	kIx,	,
příkladem	příklad	k1gInSc7	příklad
funkčních	funkční	k2eAgFnPc2d1	funkční
kategorií	kategorie	k1gFnPc2	kategorie
jsou	být	k5eAaImIp3nP	být
zájmena	zájmeno	k1gNnPc1	zájmeno
či	či	k8xC	či
spojky	spojka	k1gFnPc1	spojka
<g/>
.	.	kIx.	.
</s>
<s>
Komplexní	komplexní	k2eAgInPc1d1	komplexní
celky	celek	k1gInPc1	celek
vytvořené	vytvořený	k2eAgInPc1d1	vytvořený
pomocí	pomocí	k7c2	pomocí
syntaktických	syntaktický	k2eAgNnPc2d1	syntaktické
pravidel	pravidlo	k1gNnPc2	pravidlo
ze	z	k7c2	z
slovních	slovní	k2eAgFnPc2d1	slovní
kategorií	kategorie	k1gFnPc2	kategorie
nazýváme	nazývat	k5eAaImIp1nP	nazývat
(	(	kIx(	(
<g/>
syntaktické	syntaktický	k2eAgFnPc1d1	syntaktická
<g/>
)	)	kIx)	)
fráze	fráze	k1gFnPc1	fráze
(	(	kIx(	(
<g/>
např.	např.	kA	např.
nominální	nominální	k2eAgInSc1d1	nominální
<g/>
/	/	kIx~	/
<g/>
substantivní	substantivní	k2eAgFnSc2d1	substantivní
fráze	fráze	k1gFnSc2	fráze
hladový	hladový	k2eAgMnSc1d1	hladový
vlk	vlk	k1gMnSc1	vlk
<g/>
,	,	kIx,	,
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
z	z	k7c2	z
adjektiva	adjektivum	k1gNnSc2	adjektivum
a	a	k8xC	a
substantiva	substantivum	k1gNnSc2	substantivum
<g/>
,	,	kIx,	,
či	či	k8xC	či
verbální	verbální	k2eAgFnSc1d1	verbální
fráze	fráze	k1gFnSc1	fráze
sníst	sníst	k5eAaPmF	sníst
červenou	červený	k2eAgFnSc4d1	červená
karkulku	karkulka	k1gFnSc4	karkulka
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
z	z	k7c2	z
verba	verbum	k1gNnSc2	verbum
a	a	k8xC	a
nominální	nominální	k2eAgFnSc2d1	nominální
fráze	fráze	k1gFnSc2	fráze
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
věty	věta	k1gFnSc2	věta
neboli	neboli	k8xC	neboli
klauze	klauze	k1gFnSc2	klauze
(	(	kIx(	(
<g/>
např.	např.	kA	např.
věta	věta	k1gFnSc1	věta
Hladový	hladový	k2eAgMnSc1d1	hladový
vlk	vlk	k1gMnSc1	vlk
snědl	snědnout	k5eAaImAgMnS	snědnout
červenou	červený	k2eAgFnSc4d1	červená
karkulku	karkulka	k1gFnSc4	karkulka
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
z	z	k7c2	z
nominální	nominální	k2eAgFnSc2d1	nominální
fráze	fráze	k1gFnSc2	fráze
a	a	k8xC	a
verbální	verbální	k2eAgFnSc2d1	verbální
fráze	fráze	k1gFnSc2	fráze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
souvětí	souvětí	k1gNnSc1	souvětí
(	(	kIx(	(
<g/>
např.	např.	kA	např.
souvětí	souvětí	k1gNnPc4	souvětí
Neviděla	vidět	k5eNaImAgFnS	vidět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
hladový	hladový	k2eAgMnSc1d1	hladový
vlk	vlk	k1gMnSc1	vlk
snědl	snědnout	k5eAaImAgMnS	snědnout
červenou	červený	k2eAgFnSc4d1	červená
karkulku	karkulka	k1gFnSc4	karkulka
<g/>
,	,	kIx,	,
vytvořené	vytvořený	k2eAgNnSc4d1	vytvořené
spojením	spojení	k1gNnSc7	spojení
verba	verbum	k1gNnSc2	verbum
a	a	k8xC	a
věty	věta	k1gFnSc2	věta
<g/>
,	,	kIx,	,
či	či	k8xC	či
souvětí	souvětí	k1gNnSc1	souvětí
Hladový	hladový	k2eAgMnSc1d1	hladový
vlk	vlk	k1gMnSc1	vlk
snědl	sníst	k5eAaPmAgMnS	sníst
červenou	červený	k2eAgFnSc4d1	červená
karkulku	karkulka	k1gFnSc4	karkulka
a	a	k8xC	a
myslivec	myslivec	k1gMnSc1	myslivec
ji	on	k3xPp3gFnSc4	on
vysvobodil	vysvobodit	k5eAaPmAgMnS	vysvobodit
<g/>
,	,	kIx,	,
vytvořené	vytvořený	k2eAgFnSc3d1	vytvořená
spojením	spojení	k1gNnSc7	spojení
dvou	dva	k4xCgFnPc2	dva
vět	věta	k1gFnPc2	věta
pomocí	pomocí	k7c2	pomocí
spojky	spojka	k1gFnSc2	spojka
a	a	k8xC	a
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Syntaktické	syntaktický	k2eAgInPc1d1	syntaktický
vztahy	vztah	k1gInPc1	vztah
<g/>
,	,	kIx,	,
např.	např.	kA	např.
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
syntaktickými	syntaktický	k2eAgFnPc7d1	syntaktická
frázemi	fráze	k1gFnPc7	fráze
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
nepřekračují	překračovat	k5eNaImIp3nP	překračovat
hranici	hranice	k1gFnSc4	hranice
souvětí	souvětí	k1gNnSc2	souvětí
<g/>
.	.	kIx.	.
</s>
<s>
Sémantika	sémantika	k1gFnSc1	sémantika
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
významem	význam	k1gInSc7	význam
jazykových	jazykový	k2eAgInPc2d1	jazykový
výrazů	výraz	k1gInPc2	výraz
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
(	(	kIx(	(
<g/>
morfémů	morfém	k1gInPc2	morfém
a	a	k8xC	a
slov	slovo	k1gNnPc2	slovo
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
složitých	složitý	k2eAgFnPc2d1	složitá
(	(	kIx(	(
<g/>
frází	fráze	k1gFnPc2	fráze
<g/>
,	,	kIx,	,
vět	věta	k1gFnPc2	věta
a	a	k8xC	a
souvětí	souvětí	k1gNnPc2	souvětí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
výrazů	výraz	k1gInPc2	výraz
je	být	k5eAaImIp3nS	být
uložen	uložit	k5eAaPmNgInS	uložit
v	v	k7c6	v
mentálním	mentální	k2eAgInSc6d1	mentální
slovníku	slovník	k1gInSc6	slovník
(	(	kIx(	(
<g/>
např.	např.	kA	např.
význam	význam	k1gInSc4	význam
slova	slovo	k1gNnSc2	slovo
vlk	vlk	k1gMnSc1	vlk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
význam	význam	k1gInSc4	význam
složitých	složitý	k2eAgInPc2d1	složitý
výrazů	výraz	k1gInPc2	výraz
lze	lze	k6eAd1	lze
zpravidla	zpravidla	k6eAd1	zpravidla
odvodit	odvodit	k5eAaPmF	odvodit
z	z	k7c2	z
významů	význam	k1gInPc2	význam
jejich	jejich	k3xOp3gFnPc2	jejich
částí	část	k1gFnPc2	část
(	(	kIx(	(
<g/>
např.	např.	kA	např.
výraz	výraz	k1gInSc1	výraz
hladový	hladový	k2eAgMnSc1d1	hladový
vlk	vlk	k1gMnSc1	vlk
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
k	k	k7c3	k
objektu	objekt	k1gInSc3	objekt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
hladový	hladový	k2eAgMnSc1d1	hladový
<g/>
"	"	kIx"	"
a	a	k8xC	a
který	který	k3yIgInSc4	který
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
vlk	vlk	k1gMnSc1	vlk
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sémantické	sémantický	k2eAgInPc1d1	sémantický
vztahy	vztah	k1gInPc1	vztah
většinou	většinou	k6eAd1	většinou
nepřekračují	překračovat	k5eNaImIp3nP	překračovat
hranici	hranice	k1gFnSc4	hranice
souvětí	souvětí	k1gNnSc2	souvětí
<g/>
.	.	kIx.	.
</s>
<s>
Pragmatika	pragmatika	k1gFnSc1	pragmatika
(	(	kIx(	(
<g/>
také	také	k9	také
pragmalingvistika	pragmalingvistika	k1gFnSc1	pragmalingvistika
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
významem	význam	k1gInSc7	význam
promluvy	promluva	k1gFnSc2	promluva
v	v	k7c6	v
konkrétní	konkrétní	k2eAgFnSc6d1	konkrétní
komunikační	komunikační	k2eAgFnSc6d1	komunikační
situaci	situace	k1gFnSc6	situace
a	a	k8xC	a
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
ostatních	ostatní	k2eAgFnPc2d1	ostatní
promluv	promluva	k1gFnPc2	promluva
<g/>
.	.	kIx.	.
</s>
<s>
Promluva	promluva	k1gFnSc1	promluva
může	moct	k5eAaImIp3nS	moct
a	a	k8xC	a
nemusí	muset	k5eNaImIp3nS	muset
mít	mít	k5eAaImF	mít
formu	forma	k1gFnSc4	forma
věty	věta	k1gFnSc2	věta
či	či	k8xC	či
souvětí	souvětí	k1gNnSc2	souvětí
(	(	kIx(	(
<g/>
např.	např.	kA	např.
slovo	slovo	k1gNnSc1	slovo
pozor	pozor	k1gInSc1	pozor
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
promluva	promluva	k1gFnSc1	promluva
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
větou	věta	k1gFnSc7	věta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Promluva	promluva	k1gFnSc1	promluva
může	moct	k5eAaImIp3nS	moct
a	a	k8xC	a
nemusí	muset	k5eNaImIp3nS	muset
mít	mít	k5eAaImF	mít
doslovný	doslovný	k2eAgInSc4d1	doslovný
(	(	kIx(	(
<g/>
sémantický	sémantický	k2eAgInSc4d1	sémantický
<g/>
)	)	kIx)	)
význam	význam	k1gInSc4	význam
(	(	kIx(	(
<g/>
např.	např.	kA	např.
význam	význam	k1gInSc4	význam
promluvy	promluva	k1gFnSc2	promluva
Bolí	bolet	k5eAaImIp3nS	bolet
mě	já	k3xPp1nSc4	já
hlava	hlava	k1gFnSc1	hlava
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
kontextu	kontext	k1gInSc6	kontext
a	a	k8xC	a
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
situaci	situace	k1gFnSc6	situace
odpovídat	odpovídat	k5eAaImF	odpovídat
sémantice	sémantika	k1gFnSc3	sémantika
věty	věta	k1gFnSc2	věta
Nechci	chtít	k5eNaImIp1nS	chtít
sex	sex	k1gInSc4	sex
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Textová	textový	k2eAgFnSc1d1	textová
lingvistika	lingvistika	k1gFnSc1	lingvistika
nebo	nebo	k8xC	nebo
také	také	k9	také
analýza	analýza	k1gFnSc1	analýza
diskurzu	diskurz	k1gInSc2	diskurz
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
pravidly	pravidlo	k1gNnPc7	pravidlo
a	a	k8xC	a
principy	princip	k1gInPc7	princip
spojování	spojování	k1gNnSc2	spojování
vět	věta	k1gFnPc2	věta
a	a	k8xC	a
souvětí	souvětí	k1gNnSc2	souvětí
ve	v	k7c4	v
větší	veliký	k2eAgInPc4d2	veliký
celky	celek	k1gInPc4	celek
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
prostředky	prostředek	k1gInPc1	prostředek
vyjadřující	vyjadřující	k2eAgInPc1d1	vyjadřující
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
textovými	textový	k2eAgInPc7d1	textový
celky	celek	k1gInPc7	celek
(	(	kIx(	(
<g/>
např.	např.	kA	např.
větami	věta	k1gFnPc7	věta
<g/>
,	,	kIx,	,
souvětími	souvětí	k1gNnPc7	souvětí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
odstavci	odstavec	k1gInPc7	odstavec
či	či	k8xC	či
kapitolami	kapitola	k1gFnPc7	kapitola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lingvistika	lingvistika	k1gFnSc1	lingvistika
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
kromě	kromě	k7c2	kromě
obecné	obecný	k2eAgFnSc2d1	obecná
lingvistiky	lingvistika	k1gFnSc2	lingvistika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
jazykem	jazyk	k1gInSc7	jazyk
jako	jako	k8xC	jako
takovým	takový	k3xDgNnSc7	takový
<g/>
,	,	kIx,	,
také	také	k9	také
potenciálně	potenciálně	k6eAd1	potenciálně
otevřenou	otevřený	k2eAgFnSc4d1	otevřená
množinu	množina	k1gFnSc4	množina
podoborů	podobor	k1gInPc2	podobor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
jazyky	jazyk	k1gInPc1	jazyk
či	či	k8xC	či
jazykové	jazykový	k2eAgFnPc1d1	jazyková
rodiny	rodina	k1gFnPc1	rodina
(	(	kIx(	(
<g/>
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
filologie	filologie	k1gFnPc1	filologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
jsou	být	k5eAaImIp3nP	být
bohemistika	bohemistika	k1gFnSc1	bohemistika
(	(	kIx(	(
<g/>
čeština	čeština	k1gFnSc1	čeština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anglistika	anglistika	k1gFnSc1	anglistika
(	(	kIx(	(
<g/>
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sinologie	sinologie	k1gFnSc1	sinologie
(	(	kIx(	(
<g/>
čínština	čínština	k1gFnSc1	čínština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hispanistika	hispanistika	k1gFnSc1	hispanistika
(	(	kIx(	(
<g/>
španělština	španělština	k1gFnSc1	španělština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slovenistika	slovenistika	k1gFnSc1	slovenistika
(	(	kIx(	(
<g/>
slovinština	slovinština	k1gFnSc1	slovinština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
slavistika	slavistika	k1gFnSc1	slavistika
(	(	kIx(	(
<g/>
slovanské	slovanský	k2eAgInPc1d1	slovanský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
bantuistika	bantuistika	k1gFnSc1	bantuistika
(	(	kIx(	(
<g/>
bantuské	bantuský	k2eAgInPc1d1	bantuský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
turkologie	turkologie	k1gFnSc1	turkologie
atd.	atd.	kA	atd.
Nutno	nutno	k6eAd1	nutno
však	však	k9	však
dodat	dodat	k5eAaPmF	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
termíny	termín	k1gInPc1	termín
zpravidla	zpravidla	k6eAd1	zpravidla
charakterizují	charakterizovat	k5eAaBmIp3nP	charakterizovat
ani	ani	k8xC	ani
ne	ne	k9	ne
tak	tak	k9	tak
podobory	podobor	k1gInPc1	podobor
lingvistiky	lingvistika	k1gFnSc2	lingvistika
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
spíš	spíš	k9	spíš
podobory	podobor	k1gInPc1	podobor
filologie	filologie	k1gFnSc2	filologie
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Bohemista	bohemista	k1gMnSc1	bohemista
<g/>
"	"	kIx"	"
může	moct	k5eAaImIp3nS	moct
proto	proto	k8xC	proto
být	být	k5eAaImF	být
i	i	k9	i
literární	literární	k2eAgMnSc1d1	literární
vědec	vědec	k1gMnSc1	vědec
či	či	k8xC	či
kulturolog	kulturolog	k1gMnSc1	kulturolog
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Historická	historický	k2eAgFnSc1d1	historická
lingvistika	lingvistika	k1gFnSc1	lingvistika
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
užívaly	užívat	k5eAaImAgFnP	užívat
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
(	(	kIx(	(
<g/>
starou	starý	k2eAgFnSc4d1	stará
češtinu	čeština	k1gFnSc4	čeština
<g/>
,	,	kIx,	,
staroslověnštinu	staroslověnština	k1gFnSc4	staroslověnština
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
vývoje	vývoj	k1gInSc2	vývoj
jazyka	jazyk	k1gInSc2	jazyk
v	v	k7c6	v
čase	čas	k1gInSc6	čas
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
:	:	kIx,	:
diachronní	diachronní	k2eAgInSc1d1	diachronní
přístup	přístup	k1gInSc1	přístup
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dialektologie	dialektologie	k1gFnSc1	dialektologie
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
geograficky	geograficky	k6eAd1	geograficky
podmíněné	podmíněný	k2eAgFnPc4d1	podmíněná
odlišnosti	odlišnost	k1gFnPc4	odlišnost
v	v	k7c6	v
jazykovém	jazykový	k2eAgInSc6d1	jazykový
systému	systém	k1gInSc6	systém
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jednoho	jeden	k4xCgInSc2	jeden
(	(	kIx(	(
<g/>
národního	národní	k2eAgInSc2d1	národní
<g/>
)	)	kIx)	)
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
česká	český	k2eAgNnPc1d1	české
nářečí	nářečí	k1gNnPc1	nářečí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
tradici	tradice	k1gFnSc4	tradice
v	v	k7c6	v
dialektologii	dialektologie	k1gFnSc6	dialektologie
má	mít	k5eAaImIp3nS	mít
studium	studium	k1gNnSc4	studium
lexikologie	lexikologie	k1gFnSc2	lexikologie
<g/>
,	,	kIx,	,
fonetiky	fonetika	k1gFnSc2	fonetika
<g/>
,	,	kIx,	,
fonologie	fonologie	k1gFnSc2	fonologie
a	a	k8xC	a
morfologie	morfologie	k1gFnSc2	morfologie
<g/>
.	.	kIx.	.
</s>
<s>
Dialektologické	dialektologický	k2eAgNnSc1d1	dialektologické
studium	studium	k1gNnSc1	studium
syntaxe	syntax	k1gFnSc2	syntax
a	a	k8xC	a
sémantiky	sémantika	k1gFnSc2	sémantika
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
mladé	mladý	k2eAgNnSc1d1	mladé
<g/>
.	.	kIx.	.
</s>
<s>
Sociolingvistika	sociolingvistika	k1gFnSc1	sociolingvistika
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
sociálně	sociálně	k6eAd1	sociálně
podmíněné	podmíněný	k2eAgFnPc4d1	podmíněná
odlišnosti	odlišnost	k1gFnPc4	odlišnost
v	v	k7c6	v
jazykovém	jazykový	k2eAgInSc6d1	jazykový
systému	systém	k1gInSc6	systém
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jednoho	jeden	k4xCgInSc2	jeden
jazyka	jazyk	k1gInSc2	jazyk
či	či	k8xC	či
nářečí	nářečí	k1gNnSc2	nářečí
(	(	kIx(	(
<g/>
např.	např.	kA	např.
slangy	slang	k1gInPc1	slang
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sociolingvistika	sociolingvistika	k1gFnSc1	sociolingvistika
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
lexikální	lexikální	k2eAgInSc4d1	lexikální
jazykový	jazykový	k2eAgInSc4d1	jazykový
plán	plán	k1gInSc4	plán
<g/>
.	.	kIx.	.
</s>
<s>
Lingvistická	lingvistický	k2eAgFnSc1d1	lingvistická
genealogie	genealogie	k1gFnSc1	genealogie
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
příbuznost	příbuznost	k1gFnSc1	příbuznost
různých	různý	k2eAgInPc2d1	různý
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
francouzština	francouzština	k1gFnSc1	francouzština
a	a	k8xC	a
rumunština	rumunština	k1gFnSc1	rumunština
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
románské	románský	k2eAgFnSc2d1	románská
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
oba	dva	k4xCgInPc1	dva
jazyky	jazyk	k1gInPc1	jazyk
se	se	k3xPyFc4	se
vyčlenily	vyčlenit	k5eAaPmAgInP	vyčlenit
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kritériem	kritérion	k1gNnSc7	kritérion
při	při	k7c6	při
určování	určování	k1gNnSc6	určování
genealogické	genealogický	k2eAgFnSc2d1	genealogická
příbuznosti	příbuznost	k1gFnSc2	příbuznost
jazyků	jazyk	k1gInPc2	jazyk
je	být	k5eAaImIp3nS	být
slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Lingvistická	lingvistický	k2eAgFnSc1d1	lingvistická
typologie	typologie	k1gFnSc1	typologie
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
jazykové	jazykový	k2eAgInPc4d1	jazykový
typy	typ	k1gInPc4	typ
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
jazykové	jazykový	k2eAgInPc1d1	jazykový
rysy	rys	k1gInPc1	rys
společné	společný	k2eAgInPc1d1	společný
více	hodně	k6eAd2	hodně
jazykům	jazyk	k1gInPc3	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Genealogicky	genealogicky	k6eAd1	genealogicky
příbuzné	příbuzný	k2eAgInPc1d1	příbuzný
jazyky	jazyk	k1gInPc1	jazyk
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
typově	typově	k6eAd1	typově
podobné	podobný	k2eAgNnSc1d1	podobné
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemusí	muset	k5eNaImIp3nS	muset
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
nutně	nutně	k6eAd1	nutně
být	být	k5eAaImF	být
(	(	kIx(	(
<g/>
např.	např.	kA	např.
bulharština	bulharština	k1gFnSc1	bulharština
je	být	k5eAaImIp3nS	být
typově	typově	k6eAd1	typově
dost	dost	k6eAd1	dost
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jde	jít	k5eAaImIp3nS	jít
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
o	o	k7c4	o
slovanský	slovanský	k2eAgInSc4d1	slovanský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
určování	určování	k1gNnSc6	určování
jazykového	jazykový	k2eAgInSc2d1	jazykový
typu	typ	k1gInSc2	typ
se	se	k3xPyFc4	se
vychází	vycházet	k5eAaImIp3nS	vycházet
především	především	k9	především
z	z	k7c2	z
morfologických	morfologický	k2eAgNnPc2d1	morfologické
a	a	k8xC	a
syntaktických	syntaktický	k2eAgNnPc2d1	syntaktické
kritérií	kritérion	k1gNnPc2	kritérion
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
a	a	k8xC	a
týž	týž	k3xTgInSc1	týž
jazyk	jazyk	k1gInSc1	jazyk
může	moct	k5eAaImIp3nS	moct
patřit	patřit	k5eAaImF	patřit
do	do	k7c2	do
více	hodně	k6eAd2	hodně
jazykových	jazykový	k2eAgInPc2d1	jazykový
typů	typ	k1gInPc2	typ
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
zvoleném	zvolený	k2eAgNnSc6d1	zvolené
kritériu	kritérion	k1gNnSc6	kritérion
a	a	k8xC	a
zvoleném	zvolený	k2eAgInSc6d1	zvolený
jazykovém	jazykový	k2eAgInSc6d1	jazykový
podsystému	podsystém	k1gInSc6	podsystém
<g/>
.	.	kIx.	.
</s>
<s>
Matematická	matematický	k2eAgFnSc1d1	matematická
lingvistika	lingvistika	k1gFnSc1	lingvistika
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
jazyk	jazyk	k1gInSc4	jazyk
pomocí	pomocí	k7c2	pomocí
matematických	matematický	k2eAgFnPc2d1	matematická
a	a	k8xC	a
statistických	statistický	k2eAgFnPc2d1	statistická
metod	metoda	k1gFnPc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Komputační	Komputační	k2eAgFnSc1d1	Komputační
(	(	kIx(	(
<g/>
počítačová	počítačový	k2eAgFnSc1d1	počítačová
<g/>
)	)	kIx)	)
lingvistika	lingvistika	k1gFnSc1	lingvistika
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
komputační	komputační	k2eAgInPc4d1	komputační
modely	model	k1gInPc4	model
jazykového	jazykový	k2eAgInSc2d1	jazykový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
jazykových	jazykový	k2eAgInPc2d1	jazykový
podsystémů	podsystém	k1gInPc2	podsystém
<g/>
,	,	kIx,	,
či	či	k8xC	či
jejich	jejich	k3xOp3gFnPc2	jejich
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Korpusová	korpusový	k2eAgFnSc1d1	korpusová
lingvistika	lingvistika	k1gFnSc1	lingvistika
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
analýzu	analýza	k1gFnSc4	analýza
jazyka	jazyk	k1gInSc2	jazyk
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
zkoumání	zkoumání	k1gNnSc2	zkoumání
obsáhlých	obsáhlý	k2eAgFnPc2d1	obsáhlá
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
elektronických	elektronický	k2eAgInPc2d1	elektronický
jazykových	jazykový	k2eAgInPc2d1	jazykový
korpusů	korpus	k1gInPc2	korpus
(	(	kIx(	(
<g/>
organizovaných	organizovaný	k2eAgInPc2d1	organizovaný
uskupení	uskupení	k1gNnSc1	uskupení
psaných	psaný	k2eAgMnPc2d1	psaný
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
mluvených	mluvený	k2eAgInPc2d1	mluvený
textů	text	k1gInPc2	text
různých	různý	k2eAgInPc2d1	různý
žánrů	žánr	k1gInPc2	žánr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jazyková	jazykový	k2eAgFnSc1d1	jazyková
akvizice	akvizice	k1gFnSc1	akvizice
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
učení	učení	k1gNnSc4	učení
a	a	k8xC	a
osvojování	osvojování	k1gNnSc4	osvojování
si	se	k3xPyFc3	se
jazyka	jazyk	k1gInSc2	jazyk
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
či	či	k8xC	či
nerodilých	rodilý	k2eNgMnPc2d1	nerodilý
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
.	.	kIx.	.
</s>
<s>
Neurolingvistika	Neurolingvistika	k1gFnSc1	Neurolingvistika
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
reprezentaci	reprezentace	k1gFnSc4	reprezentace
jazyka	jazyk	k1gInSc2	jazyk
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
a	a	k8xC	a
neurologicky	urologicky	k6eNd1	urologicky
podmíněné	podmíněný	k2eAgFnPc4d1	podmíněná
jazykové	jazykový	k2eAgFnPc4d1	jazyková
poruchy	porucha	k1gFnPc4	porucha
(	(	kIx(	(
<g/>
např.	např.	kA	např.
afázii	afázie	k1gFnSc6	afázie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teoretická	teoretický	k2eAgFnSc1d1	teoretická
lingvistika	lingvistika	k1gFnSc1	lingvistika
využívá	využívat	k5eAaImIp3nS	využívat
základní	základní	k2eAgFnSc4d1	základní
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
metodu	metoda	k1gFnSc4	metoda
-	-	kIx~	-
formuluje	formulovat	k5eAaImIp3nS	formulovat
explicitní	explicitní	k2eAgMnSc1d1	explicitní
a	a	k8xC	a
testovatelné	testovatelný	k2eAgFnPc1d1	testovatelná
teorie	teorie	k1gFnPc1	teorie
a	a	k8xC	a
hypotézy	hypotéza	k1gFnPc1	hypotéza
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
platnost	platnost	k1gFnSc1	platnost
následně	následně	k6eAd1	následně
ověřuje	ověřovat	k5eAaImIp3nS	ověřovat
analýzou	analýza	k1gFnSc7	analýza
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Analýza	analýza	k1gFnSc1	analýza
promluv	promluva	k1gFnPc2	promluva
realizovaných	realizovaný	k2eAgFnPc2d1	realizovaná
v	v	k7c6	v
neexperimentálních	experimentální	k2eNgFnPc6d1	experimentální
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
digitalizaci	digitalizace	k1gFnSc3	digitalizace
a	a	k8xC	a
automatizaci	automatizace	k1gFnSc3	automatizace
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
možné	možný	k2eAgNnSc1d1	možné
touto	tento	k3xDgFnSc7	tento
metodou	metoda	k1gFnSc7	metoda
relativně	relativně	k6eAd1	relativně
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
efektivně	efektivně	k6eAd1	efektivně
zkoumat	zkoumat	k5eAaImF	zkoumat
velmi	velmi	k6eAd1	velmi
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
jazykové	jazykový	k2eAgInPc4d1	jazykový
korpusy	korpus	k1gInPc4	korpus
(	(	kIx(	(
<g/>
ty	ten	k3xDgFnPc1	ten
mají	mít	k5eAaImIp3nP	mít
dnes	dnes	k6eAd1	dnes
řádově	řádově	k6eAd1	řádově
miliony	milion	k4xCgInPc4	milion
až	až	k8xS	až
stovky	stovka	k1gFnPc4	stovka
milionů	milion	k4xCgInPc2	milion
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
např.	např.	kA	např.
Český	český	k2eAgInSc1d1	český
národní	národní	k2eAgInSc1d1	národní
korpus	korpus	k1gInSc1	korpus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
např.	např.	kA	např.
v	v	k7c6	v
deskriptivní	deskriptivní	k2eAgFnSc6d1	deskriptivní
lingvistice	lingvistika	k1gFnSc6	lingvistika
<g/>
,	,	kIx,	,
při	při	k7c6	při
vytváření	vytváření	k1gNnSc6	vytváření
mluvnic	mluvnice	k1gFnPc2	mluvnice
a	a	k8xC	a
slovníků	slovník	k1gInPc2	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Hojně	hojně	k6eAd1	hojně
jí	on	k3xPp3gFnSc2	on
využívají	využívat	k5eAaImIp3nP	využívat
také	také	k9	také
komputační	komputační	k2eAgMnPc1d1	komputační
lingvisté	lingvista	k1gMnPc1	lingvista
a	a	k8xC	a
kvantitativní	kvantitativní	k2eAgMnPc1d1	kvantitativní
lingvisté	lingvista	k1gMnPc1	lingvista
<g/>
.	.	kIx.	.
</s>
<s>
Analýza	analýza	k1gFnSc1	analýza
promluv	promluva	k1gFnPc2	promluva
a	a	k8xC	a
reakcí	reakce	k1gFnPc2	reakce
na	na	k7c4	na
promluvy	promluva	k1gFnPc4	promluva
realizované	realizovaný	k2eAgFnPc4d1	realizovaná
v	v	k7c6	v
experimentálních	experimentální	k2eAgFnPc6d1	experimentální
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Experimentální	experimentální	k2eAgFnPc1d1	experimentální
metody	metoda	k1gFnPc1	metoda
se	se	k3xPyFc4	se
rozšířily	rozšířit	k5eAaPmAgFnP	rozšířit
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
studiu	studio	k1gNnSc3	studio
jazykové	jazykový	k2eAgFnSc2d1	jazyková
akvizice	akvizice	k1gFnSc2	akvizice
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
nelze	lze	k6eNd1	lze
data	datum	k1gNnPc4	datum
získávat	získávat	k5eAaImF	získávat
analýzou	analýza	k1gFnSc7	analýza
jazykové	jazykový	k2eAgFnSc2d1	jazyková
intuice	intuice	k1gFnSc2	intuice
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
u	u	k7c2	u
dospělých	dospělý	k2eAgMnPc2d1	dospělý
mluvčích	mluvčí	k1gMnPc2	mluvčí
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
oblasti	oblast	k1gFnPc1	oblast
lingvistiky	lingvistika	k1gFnSc2	lingvistika
<g/>
,	,	kIx,	,
např.	např.	kA	např.
neurolingvistika	neurolingvistika	k1gFnSc1	neurolingvistika
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
experimentálních	experimentální	k2eAgFnPc6d1	experimentální
metodách	metoda	k1gFnPc6	metoda
takřka	takřka	k6eAd1	takřka
závislé	závislý	k2eAgFnPc1d1	závislá
<g/>
.	.	kIx.	.
</s>
<s>
Analýza	analýza	k1gFnSc1	analýza
jazykové	jazykový	k2eAgFnSc2d1	jazyková
intuice	intuice	k1gFnSc2	intuice
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
předpokladu	předpoklad	k1gInSc6	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
mluvčí	mluvčí	k1gMnSc1	mluvčí
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
posoudit	posoudit	k5eAaPmF	posoudit
přijatelnost	přijatelnost	k1gFnSc4	přijatelnost
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
pravdivost	pravdivost	k1gFnSc4	pravdivost
vět	věta	k1gFnPc2	věta
a	a	k8xC	a
výrazů	výraz	k1gInPc2	výraz
svého	svůj	k3xOyFgInSc2	svůj
rodného	rodný	k2eAgInSc2d1	rodný
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc4	jaký
jazykový	jazykový	k2eAgInSc4d1	jazykový
plán	plán	k1gInSc4	plán
zkoumáme	zkoumat	k5eAaImIp1nP	zkoumat
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
posuzovat	posuzovat	k5eAaImF	posuzovat
přijatelnost	přijatelnost	k1gFnSc4	přijatelnost
foneticko-fonologickou	fonetickoonologický	k2eAgFnSc7d1	foneticko-fonologický
<g/>
,	,	kIx,	,
morfosyntaktickou	morfosyntaktický	k2eAgFnSc7d1	morfosyntaktický
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
gramatičnost	gramatičnost	k1gFnSc1	gramatičnost
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sémantickou	sémantický	k2eAgFnSc4d1	sémantická
<g/>
,	,	kIx,	,
či	či	k8xC	či
pragmatickou	pragmatický	k2eAgFnSc7d1	pragmatická
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
introspektivní	introspektivní	k2eAgFnSc1d1	introspektivní
metoda	metoda	k1gFnSc1	metoda
je	být	k5eAaImIp3nS	být
hojně	hojně	k6eAd1	hojně
používaná	používaný	k2eAgNnPc1d1	používané
u	u	k7c2	u
teoretických	teoretický	k2eAgMnPc2d1	teoretický
lingvistů	lingvista	k1gMnPc2	lingvista
<g/>
.	.	kIx.	.
</s>
<s>
Synchronní	synchronní	k2eAgFnSc1d1	synchronní
lingvistika	lingvistika	k1gFnSc1	lingvistika
pohlíží	pohlížet	k5eAaImIp3nS	pohlížet
na	na	k7c4	na
jazyk	jazyk	k1gInSc4	jazyk
jako	jako	k8xC	jako
na	na	k7c4	na
statický	statický	k2eAgInSc4d1	statický
systém	systém	k1gInSc4	systém
a	a	k8xC	a
abstrahuje	abstrahovat	k5eAaBmIp3nS	abstrahovat
od	od	k7c2	od
jeho	jeho	k3xOp3gFnPc2	jeho
proměn	proměna	k1gFnPc2	proměna
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Předmětem	předmět	k1gInSc7	předmět
synchronního	synchronní	k2eAgNnSc2d1	synchronní
studia	studio	k1gNnSc2	studio
může	moct	k5eAaImIp3nS	moct
tedy	tedy	k9	tedy
například	například	k6eAd1	například
být	být	k5eAaImF	být
současná	současný	k2eAgFnSc1d1	současná
čeština	čeština	k1gFnSc1	čeština
či	či	k8xC	či
čeština	čeština	k1gFnSc1	čeština
v	v	k7c6	v
době	doba	k1gFnSc6	doba
husitské	husitský	k2eAgFnSc6d1	husitská
<g/>
.	.	kIx.	.
</s>
<s>
Synchronní	synchronní	k2eAgNnSc1d1	synchronní
studium	studium	k1gNnSc1	studium
současného	současný	k2eAgInSc2d1	současný
živého	živý	k2eAgInSc2d1	živý
jazyka	jazyk	k1gInSc2	jazyk
může	moct	k5eAaImIp3nS	moct
využívat	využívat	k5eAaImF	využívat
všech	všecek	k3xTgInPc2	všecek
tří	tři	k4xCgFnPc2	tři
výše	vysoce	k6eAd2	vysoce
uvedených	uvedený	k2eAgFnPc2d1	uvedená
metod	metoda	k1gFnPc2	metoda
získávání	získávání	k1gNnSc2	získávání
lingvistických	lingvistický	k2eAgNnPc2d1	lingvistické
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Diachronní	diachronní	k2eAgFnSc1d1	diachronní
lingvistika	lingvistika	k1gFnSc1	lingvistika
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
proměny	proměna	k1gFnPc4	proměna
jazykového	jazykový	k2eAgInSc2d1	jazykový
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
jeho	jeho	k3xOp3gMnPc2	jeho
podsystémů	podsystém	k1gInPc2	podsystém
<g/>
)	)	kIx)	)
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
zpravidla	zpravidla	k6eAd1	zpravidla
o	o	k7c4	o
proměny	proměna	k1gFnPc4	proměna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
odehrají	odehrát	k5eAaPmIp3nP	odehrát
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
desítek	desítka	k1gFnPc2	desítka
až	až	k8xS	až
stovek	stovka	k1gFnPc2	stovka
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Diachronní	diachronní	k2eAgFnSc1d1	diachronní
lingvistika	lingvistika	k1gFnSc1	lingvistika
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
odkázána	odkázat	k5eAaPmNgFnS	odkázat
na	na	k7c4	na
první	první	k4xOgFnSc4	první
výše	vysoce	k6eAd2	vysoce
uvedenou	uvedený	k2eAgFnSc4d1	uvedená
metodu	metoda	k1gFnSc4	metoda
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c4	na
studium	studium	k1gNnSc4	studium
psaných	psaný	k2eAgInPc2d1	psaný
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Panchronní	Panchronní	k2eAgFnSc1d1	Panchronní
lingvistika	lingvistika	k1gFnSc1	lingvistika
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
popírá	popírat	k5eAaImIp3nS	popírat
existenci	existence	k1gFnSc4	existence
výše	výše	k1gFnSc2	výše
zmíněné	zmíněný	k2eAgFnSc2d1	zmíněná
opozice	opozice	k1gFnSc2	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Zastánci	zastánce	k1gMnPc1	zastánce
panchronního	panchronní	k2eAgInSc2d1	panchronní
přístupu	přístup	k1gInSc2	přístup
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jazykový	jazykový	k2eAgInSc1d1	jazykový
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
inherentně	inherentně	k6eAd1	inherentně
dynamický	dynamický	k2eAgMnSc1d1	dynamický
a	a	k8xC	a
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
okamžiku	okamžik	k1gInSc6	okamžik
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
potenciál	potenciál	k1gInSc1	potenciál
změny	změna	k1gFnSc2	změna
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
např.	např.	kA	např.
na	na	k7c6	na
jazykových	jazykový	k2eAgFnPc6d1	jazyková
variacích	variace	k1gFnPc6	variace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
čase	čas	k1gInSc6	čas
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
(	(	kIx(	(
<g/>
dialekty	dialekt	k1gInPc1	dialekt
<g/>
,	,	kIx,	,
funkční	funkční	k2eAgInPc1d1	funkční
styly	styl	k1gInPc1	styl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc4	co
vnímáme	vnímat	k5eAaImIp1nP	vnímat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
vývoj	vývoj	k1gInSc1	vývoj
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
zpravidla	zpravidla	k6eAd1	zpravidla
pouze	pouze	k6eAd1	pouze
převládnutí	převládnutí	k1gNnSc4	převládnutí
určité	určitý	k2eAgFnSc2d1	určitá
"	"	kIx"	"
<g/>
synchronní	synchronní	k2eAgFnSc2d1	synchronní
<g/>
"	"	kIx"	"
jazykové	jazykový	k2eAgFnSc2d1	jazyková
varianty	varianta	k1gFnSc2	varianta
nad	nad	k7c7	nad
ostatními	ostatní	k2eAgMnPc7d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
mnohotvárný	mnohotvárný	k2eAgInSc1d1	mnohotvárný
jev	jev	k1gInSc1	jev
a	a	k8xC	a
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
z	z	k7c2	z
jakého	jaký	k3yIgInSc2	jaký
úhlu	úhel	k1gInSc2	úhel
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
přistupujeme	přistupovat	k5eAaImIp1nP	přistupovat
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
lingvistiku	lingvistika	k1gFnSc4	lingvistika
zařadit	zařadit	k5eAaPmF	zařadit
do	do	k7c2	do
několika	několik	k4yIc2	několik
širších	široký	k2eAgFnPc2d2	širší
disciplín	disciplína	k1gFnPc2	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
druhy	druh	k1gInPc1	druh
lingvistiky	lingvistika	k1gFnSc2	lingvistika
si	se	k3xPyFc3	se
nekonkurují	konkurovat	k5eNaImIp3nP	konkurovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
přísně	přísně	k6eAd1	přísně
vzato	vzít	k5eAaPmNgNnS	vzít
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
jiný	jiný	k2eAgInSc1d1	jiný
jazykový	jazykový	k2eAgInSc1d1	jazykový
objekt	objekt	k1gInSc1	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
"	"	kIx"	"
<g/>
jazyky	jazyk	k1gInPc7	jazyk
<g/>
"	"	kIx"	"
a	a	k8xC	a
potažmo	potažmo	k6eAd1	potažmo
"	"	kIx"	"
<g/>
lingvistikami	lingvistika	k1gFnPc7	lingvistika
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
je	on	k3xPp3gNnSc4	on
zkoumají	zkoumat	k5eAaImIp3nP	zkoumat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
neostré	ostrý	k2eNgFnPc1d1	neostrá
<g/>
.	.	kIx.	.
</s>
<s>
Lingvistika	lingvistika	k1gFnSc1	lingvistika
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
kognitivních	kognitivní	k2eAgFnPc2d1	kognitivní
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
kognitivní	kognitivní	k2eAgFnSc1d1	kognitivní
lingvistika	lingvistika	k1gFnSc1	lingvistika
<g/>
)	)	kIx)	)
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
jazyk	jazyk	k1gInSc4	jazyk
jako	jako	k8xC	jako
součást	součást	k1gFnSc4	součást
lidského	lidský	k2eAgInSc2d1	lidský
kognitivního	kognitivní	k2eAgInSc2d1	kognitivní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
jako	jako	k9	jako
mentální	mentální	k2eAgInSc4d1	mentální
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
s	s	k7c7	s
jehož	jehož	k3xOyRp3gFnSc7	jehož
pomocí	pomoc	k1gFnPc2	pomoc
člověk	člověk	k1gMnSc1	člověk
poznává	poznávat	k5eAaImIp3nS	poznávat
<g/>
,	,	kIx,	,
interpretuje	interpretovat	k5eAaBmIp3nS	interpretovat
a	a	k8xC	a
kategorizuje	kategorizovat	k5eAaBmIp3nS	kategorizovat
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Lingvistika	lingvistika	k1gFnSc1	lingvistika
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
sémiotiky	sémiotika	k1gFnSc2	sémiotika
vnímá	vnímat	k5eAaImIp3nS	vnímat
jazyk	jazyk	k1gInSc4	jazyk
jako	jako	k8xS	jako
soubor	soubor	k1gInSc4	soubor
tzv.	tzv.	kA	tzv.
jazykových	jazykový	k2eAgInPc2d1	jazykový
znaků	znak	k1gInPc2	znak
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
lze	lze	k6eAd1	lze
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
jako	jako	k9	jako
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
myšlenkami	myšlenka	k1gFnPc7	myšlenka
či	či	k8xC	či
mentálními	mentální	k2eAgInPc7d1	mentální
koncepty	koncept	k1gInPc7	koncept
(	(	kIx(	(
<g/>
např.	např.	kA	např.
představa	představa	k1gFnSc1	představa
psa	pes	k1gMnSc2	pes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc7	jejich
formálním	formální	k2eAgNnSc7d1	formální
vyjádřením	vyjádření	k1gNnSc7	vyjádření
(	(	kIx(	(
<g/>
slovo	slovo	k1gNnSc1	slovo
pes	peso	k1gNnPc2	peso
<g/>
)	)	kIx)	)
a	a	k8xC	a
vnějším	vnější	k2eAgInSc7d1	vnější
světem	svět	k1gInSc7	svět
(	(	kIx(	(
<g/>
reálný	reálný	k2eAgMnSc1d1	reálný
pes	pes	k1gMnSc1	pes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
těchto	tento	k3xDgInPc2	tento
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
i	i	k9	i
jejich	jejich	k3xOp3gFnSc4	jejich
systémovost	systémovost	k1gFnSc4	systémovost
<g/>
,	,	kIx,	,
ustálenost	ustálenost	k1gFnSc4	ustálenost
a	a	k8xC	a
konvenčnost	konvenčnost	k1gFnSc4	konvenčnost
<g/>
,	,	kIx,	,
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
komunikaci	komunikace	k1gFnSc4	komunikace
<g/>
.	.	kIx.	.
</s>
<s>
Komunikace	komunikace	k1gFnSc1	komunikace
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
předávání	předávání	k1gNnSc4	předávání
a	a	k8xC	a
sdílení	sdílení	k1gNnSc4	sdílení
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
spjatá	spjatý	k2eAgFnSc1d1	spjatá
se	s	k7c7	s
sémiotikou	sémiotika	k1gFnSc7	sémiotika
<g/>
.	.	kIx.	.
</s>
<s>
Lingvistika	lingvistika	k1gFnSc1	lingvistika
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
úhlu	úhel	k1gInSc2	úhel
pohledu	pohled	k1gInSc2	pohled
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
příbuzná	příbuzný	k2eAgFnSc1d1	příbuzná
vědám	věda	k1gFnPc3	věda
o	o	k7c6	o
nelingvistických	lingvistický	k2eNgInPc6d1	lingvistický
znacích	znak	k1gInPc6	znak
a	a	k8xC	a
prostředcích	prostředek	k1gInPc6	prostředek
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
neverbální	verbální	k2eNgFnSc1d1	neverbální
komunikace	komunikace	k1gFnSc1	komunikace
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc1d1	různý
pomocné	pomocný	k2eAgInPc1d1	pomocný
komunikační	komunikační	k2eAgInPc1d1	komunikační
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
např.	např.	kA	např.
světelné	světelný	k2eAgInPc1d1	světelný
či	či	k8xC	či
kouřové	kouřový	k2eAgInPc1d1	kouřový
signály	signál	k1gInPc1	signál
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
znakové	znakový	k2eAgInPc1d1	znakový
systémy	systém	k1gInPc1	systém
uměle	uměle	k6eAd1	uměle
vytvářené	vytvářený	k2eAgInPc1d1	vytvářený
pro	pro	k7c4	pro
potřebu	potřeba	k1gFnSc4	potřeba
komunikace	komunikace	k1gFnSc2	komunikace
s	s	k7c7	s
počítači	počítač	k1gInPc7	počítač
a	a	k8xC	a
stroji	stroj	k1gInPc7	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Lingvistika	lingvistika	k1gFnSc1	lingvistika
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
filologie	filologie	k1gFnSc2	filologie
studuje	studovat	k5eAaImIp3nS	studovat
jazyk	jazyk	k1gInSc1	jazyk
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
člověku	člověk	k1gMnSc3	člověk
a	a	k8xC	a
všemu	všecek	k3xTgNnSc3	všecek
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
souvisí	souviset	k5eAaImIp3nS	souviset
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
tedy	tedy	k9	tedy
vzájemné	vzájemný	k2eAgInPc4d1	vzájemný
vlivy	vliv	k1gInPc4	vliv
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
především	především	k9	především
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vnímat	vnímat	k5eAaImF	vnímat
jako	jako	k9	jako
komplexní	komplexní	k2eAgInSc1d1	komplexní
jazykový	jazykový	k2eAgInSc1d1	jazykový
výraz	výraz	k1gInSc1	výraz
všech	všecek	k3xTgFnPc2	všecek
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
oblastí	oblast	k1gFnPc2	oblast
lidského	lidský	k2eAgNnSc2d1	lidské
působení	působení	k1gNnSc2	působení
<g/>
.	.	kIx.	.
</s>
