<s>
PlayStation	PlayStation	k1gInSc1
</s>
<s>
PlayStation	PlayStation	k1gInSc1
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
プ	プ	k?
<g/>
,	,	kIx,
Pureisutéšon	Pureisutéšon	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
PS	PS	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
videoherní	videoherní	k2eAgFnSc1d1
značka	značka	k1gFnSc1
pěti	pět	k4xCc2
domácích	domácí	k1gMnPc2
konzolí	konzolit	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
online	onlinout	k5eAaPmIp3nS
služby	služba	k1gFnPc4
<g/>
,	,	kIx,
ovladačů	ovladač	k1gInPc2
<g/>
,	,	kIx,
dvou	dva	k4xCgFnPc6
kapesních	kapesní	k2eAgFnPc2d1
konzolí	konzole	k1gFnPc2
a	a	k8xC
mobilního	mobilní	k2eAgInSc2d1
telefonu	telefon	k1gInSc2
společnosti	společnost	k1gFnSc2
Sony	Sony	kA
Interactive	Interactiv	k1gInSc5
Entertainment	Entertainment	k1gInSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
divizí	divize	k1gFnSc7
Sony	Sony	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc6
herní	herní	k2eAgFnSc6d1
konzole	konzola	k1gFnSc6
byla	být	k5eAaImAgFnS
uvedena	uvést	k5eAaPmNgFnS
pod	pod	k7c7
názvem	název	k1gInSc7
PlayStation	PlayStation	k1gInSc1
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
v	v	k7c6
prosinci	prosinec	k1gInSc6
1994	#num#	k4
a	a	k8xC
celosvětově	celosvětově	k6eAd1
následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bylo	být	k5eAaImAgNnS
prodáno	prodat	k5eAaPmNgNnS
více	hodně	k6eAd2
jak	jak	k8xS,k8xC
100	#num#	k4
milionů	milion	k4xCgInPc2
kusů	kus	k1gInPc2
původní	původní	k2eAgFnSc3d1
konzole	konzola	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
první	první	k4xOgNnSc4
dosažení	dosažení	k1gNnSc4
takovéto	takovýto	k3xDgFnSc2
hranice	hranice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podařilo	podařit	k5eAaPmAgNnS
se	se	k3xPyFc4
ji	on	k3xPp3gFnSc4
dosáhnout	dosáhnout	k5eAaPmF
pod	pod	k7c4
deset	deset	k4xCc4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
PlayStation	PlayStation	k1gInSc1
2	#num#	k4
<g/>
,	,	kIx,
nástupce	nástupce	k1gMnSc1
PlayStationu	PlayStation	k1gInSc2
1	#num#	k4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
uveden	uvést	k5eAaPmNgInS
na	na	k7c4
trh	trh	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
2012	#num#	k4
bylo	být	k5eAaImAgNnS
prodáno	prodat	k5eAaPmNgNnS
přes	přes	k7c4
155	#num#	k4
milionů	milion	k4xCgInPc2
kusů	kus	k1gInPc2
konzole	konzola	k1gFnSc6
a	a	k8xC
PlayStation	PlayStation	k1gInSc1
2	#num#	k4
se	se	k3xPyFc4
tak	tak	k9
stal	stát	k5eAaPmAgMnS
nejprodávanější	prodávaný	k2eAgFnSc7d3
herní	herní	k2eAgFnSc7d1
konzolí	konzole	k1gFnSc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Další	další	k2eAgFnSc6d1
herní	herní	k2eAgFnSc6d1
konzole	konzola	k1gFnSc6
byla	být	k5eAaImAgFnS
vydána	vydat	k5eAaPmNgFnS
společností	společnost	k1gFnSc7
Sony	Sony	kA
roku	rok	k1gInSc2
2006	#num#	k4
pod	pod	k7c7
názvem	název	k1gInSc7
PlayStation	PlayStation	k1gInSc4
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
března	březen	k1gInSc2
2017	#num#	k4
se	se	k3xPyFc4
prodalo	prodat	k5eAaPmAgNnS
více	hodně	k6eAd2
jak	jak	k8xS,k8xC
87,4	87,4	k4
milionu	milion	k4xCgInSc2
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Následovala	následovat	k5eAaImAgFnS
herní	herní	k2eAgFnSc1d1
konzole	konzola	k1gFnSc3
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
uvedena	uvést	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
jednoho	jeden	k4xCgInSc2
dne	den	k1gInSc2
byl	být	k5eAaImAgInS
prodán	prodat	k5eAaPmNgInS
milion	milion	k4xCgInSc1
kusů	kus	k1gInPc2
a	a	k8xC
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
nejrychleji	rychle	k6eAd3
prodávanou	prodávaný	k2eAgFnSc7d1
herní	herní	k2eAgFnSc7d1
konzolí	konzole	k1gFnSc7
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Prozatím	prozatím	k6eAd1
nejnovější	nový	k2eAgFnSc7d3
konzolí	konzole	k1gFnSc7
od	od	k7c2
Sony	Sony	kA
je	být	k5eAaImIp3nS
PlayStation	PlayStation	k1gInSc4
5	#num#	k4
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
vydána	vydat	k5eAaPmNgFnS
v	v	k7c6
průběhu	průběh	k1gInSc6
listopadu	listopad	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgFnSc7
kapesní	kapesní	k2eAgFnSc7d1
herní	herní	k2eAgFnSc7d1
konzolí	konzole	k1gFnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
PlayStation	PlayStation	k1gInSc1
Portable	portable	k1gInSc1
(	(	kIx(
<g/>
PSP	PSP	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterého	který	k3yRgNnSc2,k3yIgNnSc2,k3yQgNnSc2
se	se	k3xPyFc4
do	do	k7c2
listopadu	listopad	k1gInSc2
2013	#num#	k4
prodalo	prodat	k5eAaPmAgNnS
80	#num#	k4
milionů	milion	k4xCgInPc2
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Jeho	jeho	k3xOp3gMnSc1
nástupce	nástupce	k1gMnSc1
<g/>
,	,	kIx,
PlayStation	PlayStation	k1gInSc4
Vita	vít	k5eAaImNgFnS
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
na	na	k7c4
japonský	japonský	k2eAgInSc4d1
trh	trh	k1gInSc4
uveden	uvést	k5eAaPmNgInS
v	v	k7c6
prosinci	prosinec	k1gInSc6
2011	#num#	k4
a	a	k8xC
ve	v	k7c6
většině	většina	k1gFnSc6
dalších	další	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
v	v	k7c6
únoru	únor	k1gInSc6
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
ledna	leden	k1gInSc2
2013	#num#	k4
byly	být	k5eAaImAgFnP
prodány	prodat	k5eAaPmNgFnP
4	#num#	k4
miliony	milion	k4xCgInPc4
kusů	kus	k1gInPc2
konzole	konzola	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
PlayStation	PlayStation	k1gInSc1
TV	TV	kA
je	být	k5eAaImIp3nS
mikrokonzole	mikrokonzole	k1gFnSc1
a	a	k8xC
nepřenosná	přenosný	k2eNgFnSc1d1
varianta	varianta	k1gFnSc1
kapesní	kapesní	k2eAgFnSc1d1
konzole	konzola	k1gFnSc3
PlayStation	PlayStation	k1gInSc1
Vita	vit	k2eAgMnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Dalšími	další	k2eAgInPc7d1
vydanými	vydaný	k2eAgInPc7d1
produkty	produkt	k1gInPc7
PlayStation	PlayStation	k1gInSc1
byly	být	k5eAaImAgFnP
PSX	PSX	kA
<g/>
,	,	kIx,
DVD	DVD	kA
přehrávač	přehrávač	k1gInSc1
s	s	k7c7
plně	plně	k6eAd1
integrovanou	integrovaný	k2eAgFnSc7d1
herní	herní	k2eAgFnSc7d1
konzolí	konzole	k1gFnSc7
PlayStation	PlayStation	k1gInSc1
a	a	k8xC
PlayStation	PlayStation	k1gInSc1
2	#num#	k4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
se	se	k3xPyFc4
kvůli	kvůli	k7c3
vysoké	vysoký	k2eAgFnSc3d1
ceně	cena	k1gFnSc3
dlouho	dlouho	k6eAd1
neprodával	prodávat	k5eNaImAgInS
a	a	k8xC
nebyl	být	k5eNaImAgInS
uveden	uvést	k5eAaPmNgInS
mimo	mimo	k7c4
Japonsko	Japonsko	k1gNnSc4
<g/>
,	,	kIx,
a	a	k8xC
televize	televize	k1gFnSc1
Sony	Sony	kA
Bravia	Bravia	k1gFnSc1
s	s	k7c7
integrovaným	integrovaný	k2eAgInSc7d1
PlayStationem	PlayStation	k1gInSc7
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DualShock	DualShock	k1gInSc1
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgFnSc7d1
řadou	řada	k1gFnSc7
ovladačů	ovladač	k1gInPc2
konzolí	konzolit	k5eAaImIp3nP,k5eAaPmIp3nP
PlayStation	PlayStation	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tvořen	tvořit	k5eAaImNgInS
gamepadem	gamepad	k1gInSc7
s	s	k7c7
haptickou	haptický	k2eAgFnSc7d1
odezvou	odezva	k1gFnSc7
(	(	kIx(
<g/>
vibracemi	vibrace	k1gFnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
června	červen	k1gInSc2
2008	#num#	k4
bylo	být	k5eAaImAgNnS
prodáno	prodat	k5eAaPmNgNnS
28	#num#	k4
milionů	milion	k4xCgInPc2
kusů	kus	k1gInPc2
ovladačů	ovladač	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
novým	nový	k2eAgInSc7d1
PlayStationem	PlayStation	k1gInSc7
5	#num#	k4
přišel	přijít	k5eAaPmAgInS
nový	nový	k2eAgInSc1d1
typ	typ	k1gInSc1
ovladačů	ovladač	k1gInPc2
DualSense	DualSense	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
PlayStation	PlayStation	k1gInSc1
Network	network	k1gInSc1
je	on	k3xPp3gMnPc4
online	onlinout	k5eAaPmIp3nS
služba	služba	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
zaregistrováno	zaregistrovat	k5eAaPmNgNnS
kolem	kolem	k7c2
110	#num#	k4
milionů	milion	k4xCgInPc2
uživatelů	uživatel	k1gMnPc2
(	(	kIx(
<g/>
červen	červen	k1gInSc1
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
aktivně	aktivně	k6eAd1
ji	on	k3xPp3gFnSc4
používá	používat	k5eAaImIp3nS
94	#num#	k4
milionů	milion	k4xCgInPc2
uživatelů	uživatel	k1gMnPc2
měsíčně	měsíčně	k6eAd1
(	(	kIx(
<g/>
květen	květen	k1gInSc1
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Služba	služba	k1gFnSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
elektronického	elektronický	k2eAgInSc2d1
obchodu	obchod	k1gInSc2
PlayStation	PlayStation	k1gInSc1
Store	Stor	k1gInSc5
<g/>
,	,	kIx,
předplatného	předplatné	k1gNnSc2
PlayStation	PlayStation	k1gInSc4
Plus	plus	k6eAd1
a	a	k8xC
virtuální	virtuální	k2eAgFnPc4d1
herní	herní	k2eAgFnPc4d1
sociální	sociální	k2eAgFnPc4d1
platformy	platforma	k1gFnPc4
PlayStation	PlayStation	k1gInSc4
Home	Hom	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
v	v	k7c6
době	doba	k1gFnSc6
svého	svůj	k3xOyFgNnSc2
ukončení	ukončení	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
březnu	březen	k1gInSc6
2015	#num#	k4
<g/>
,	,	kIx,
měla	mít	k5eAaImAgFnS
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
41	#num#	k4
milionů	milion	k4xCgInPc2
uživatelů	uživatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
toclimit-	toclimit-	k?
<g/>
2	#num#	k4
.	.	kIx.
<g/>
toclevel-	toclevel-	k?
<g/>
1	#num#	k4
ul	ul	kA
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
toclimit-	toclimit-	k?
<g/>
3	#num#	k4
.	.	kIx.
<g/>
toclevel-	toclevel-	k?
<g/>
2	#num#	k4
ul	ul	kA
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
toclimit-	toclimit-	k?
<g/>
4	#num#	k4
.	.	kIx.
<g/>
toclevel-	toclevel-	k?
<g/>
3	#num#	k4
ul	ul	kA
<g/>
,	,	kIx,
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
toclimit-	toclimit-	k?
<g/>
5	#num#	k4
.	.	kIx.
<g/>
toclevel-	toclevel-	k?
<g/>
4	#num#	k4
ul	ul	kA
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
toclimit-	toclimit-	k?
<g/>
6	#num#	k4
.	.	kIx.
<g/>
toclevel-	toclevel-	k?
<g/>
5	#num#	k4
ul	ul	kA
<g/>
,	,	kIx,
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
toclimit-	toclimit-	k?
<g/>
7	#num#	k4
.	.	kIx.
<g/>
toclevel-	toclevel-	k?
<g/>
6	#num#	k4
ul	ul	kA
<g/>
{	{	kIx(
<g/>
display	displaa	k1gMnSc2
<g/>
:	:	kIx,
<g/>
none	none	k6eAd1
<g/>
}	}	kIx)
</s>
<s>
Domácí	domácí	k2eAgFnSc3d1
herní	herní	k2eAgFnSc3d1
konzole	konzola	k1gFnSc3
</s>
<s>
PlayStation	PlayStation	k1gInSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
PlayStation	PlayStation	k1gInSc1
(	(	kIx(
<g/>
herní	herní	k2eAgFnSc6d1
konzole	konzola	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Původní	původní	k2eAgInSc1d1
PlayStationRedesignovaný	PlayStationRedesignovaný	k2eAgInSc1d1
PlayStation	PlayStation	k1gInSc1
One	One	k1gFnSc2
</s>
<s>
PlayStation	PlayStation	k1gInSc1
je	být	k5eAaImIp3nS
původní	původní	k2eAgFnSc3d1
herní	herní	k2eAgFnSc3d1
konzole	konzola	k1gFnSc3
a	a	k8xC
první	první	k4xOgFnSc3
konzole	konzola	k1gFnSc3
stejnojmenné	stejnojmenný	k2eAgFnSc2d1
řady	řada	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
na	na	k7c4
japonský	japonský	k2eAgInSc4d1
trh	trh	k1gInSc4
uvedena	uvést	k5eAaPmNgNnP
3	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1994	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnPc2
byl	být	k5eAaImAgInS
i	i	k9
vývojářský	vývojářský	k2eAgInSc1d1
kit	kit	k1gInSc1
Net	Net	k1gFnSc2
Yaroze	Yaroz	k1gInSc5
<g/>
,	,	kIx,
speciální	speciální	k2eAgFnSc1d1
černá	černý	k2eAgFnSc1d1
konzole	konzola	k1gFnSc6
PlayStation	PlayStation	k1gInSc4
s	s	k7c7
nástroji	nástroj	k1gInPc7
a	a	k8xC
instrukcemi	instrukce	k1gFnPc7
pro	pro	k7c4
programování	programování	k1gNnSc4
videoher	videohra	k1gFnPc2
a	a	k8xC
aplikací	aplikace	k1gFnPc2
na	na	k7c6
PlayStationu	PlayStation	k1gInSc6
<g/>
,	,	kIx,
nadcházející	nadcházející	k2eAgFnSc6d1
konzole	konzola	k1gFnSc6
PS	PS	kA
One	One	k1gFnPc1
<g/>
,	,	kIx,
zmenšená	zmenšený	k2eAgFnSc1d1
verze	verze	k1gFnSc1
původní	původní	k2eAgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
PocketStation	PocketStation	k1gInSc1
<g/>
,	,	kIx,
kapesní	kapesní	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
funguje	fungovat	k5eAaImIp3nS
jako	jako	k9
paměťová	paměťový	k2eAgFnSc1d1
karta	karta	k1gFnSc1
a	a	k8xC
vylepšuje	vylepšovat	k5eAaImIp3nS
videohry	videohra	k1gFnSc2
PlayStationu	PlayStation	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
PlayStation	PlayStation	k1gInSc1
je	být	k5eAaImIp3nS
pátou	pátá	k1gFnSc4
generací	generace	k1gFnPc2
herních	herní	k2eAgFnPc2d1
konzolí	konzole	k1gFnPc2
a	a	k8xC
konkurentem	konkurent	k1gMnSc7
konzolí	konzolit	k5eAaPmIp3nS,k5eAaImIp3nS
Sega	Sega	k1gMnSc1
Saturn	Saturn	k1gMnSc1
a	a	k8xC
Nintendo	Nintendo	k6eAd1
64	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
prosince	prosinec	k1gInSc2
roku	rok	k1gInSc2
2003	#num#	k4
bylo	být	k5eAaImAgNnS
prodáno	prodat	k5eAaPmNgNnS
102,49	102,49	k4
milionu	milion	k4xCgInSc2
kusů	kus	k1gInPc2
konzolí	konzolit	k5eAaImIp3nS,k5eAaPmIp3nS
PlayStation	PlayStation	k1gInSc1
a	a	k8xC
PS	PS	kA
One	One	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
přičemž	přičemž	k6eAd1
PlayStation	PlayStation	k1gInSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
první	první	k4xOgFnSc7
herní	herní	k2eAgFnSc7d1
konzolí	konzole	k1gFnSc7
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
prodala	prodat	k5eAaPmAgFnS
120	#num#	k4
milionů	milion	k4xCgInPc2
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
PS	PS	kA
One	One	k1gFnSc1
</s>
<s>
PS	PS	kA
One	One	k1gFnSc1
(	(	kIx(
<g/>
stylizováno	stylizován	k2eAgNnSc1d1
jako	jako	k8xS,k8xC
PS	PS	kA
one	one	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
výrazně	výrazně	k6eAd1
menší	malý	k2eAgFnSc1d2
a	a	k8xC
redesignovaná	redesignovaný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
původní	původní	k2eAgFnSc1d1
herní	herní	k2eAgFnSc3d1
konzole	konzola	k1gFnSc3
PlayStation	PlayStation	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Byla	být	k5eAaImAgFnS
vydána	vydán	k2eAgFnSc1d1
7	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2001	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
jako	jako	k9
její	její	k3xOp3gInSc4
nástupce	nástupce	k1gMnSc1
PlayStation	PlayStation	k1gInSc4
2	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
PS	PS	kA
One	One	k1gMnSc1
předčil	předčit	k5eAaBmAgMnS,k5eAaPmAgMnS
v	v	k7c6
prodejích	prodej	k1gInPc6
v	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
roku	rok	k1gInSc2
2000	#num#	k4
všechny	všechen	k3xTgMnPc4
ostatní	ostatní	k2eAgFnSc6d1
konzole	konzola	k1gFnSc6
<g/>
,	,	kIx,
včetně	včetně	k7c2
svého	svůj	k3xOyFgMnSc2
nástupce	nástupce	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Oproti	oproti	k7c3
svému	svůj	k3xOyFgMnSc3
předchůdci	předchůdce	k1gMnSc3
obsahuje	obsahovat	k5eAaImIp3nS
konzole	konzola	k1gFnSc6
dvě	dva	k4xCgFnPc4
hlavní	hlavní	k2eAgFnPc4d1
změny	změna	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc4
je	být	k5eAaImIp3nS
kosmetická	kosmetický	k2eAgFnSc1d1
změna	změna	k1gFnSc1
konzole	konzola	k1gFnSc3
a	a	k8xC
druhou	druhý	k4xOgFnSc4
je	být	k5eAaImIp3nS
grafické	grafický	k2eAgNnSc4d1
uživatelské	uživatelský	k2eAgNnSc4d1
rozhraní	rozhraní	k1gNnSc4
(	(	kIx(
<g/>
GUI	GUI	kA
<g/>
)	)	kIx)
domovského	domovský	k2eAgNnSc2d1
menu	menu	k1gNnSc2
<g/>
,	,	kIx,
verze	verze	k1gFnSc2
GUI	GUI	kA
původně	původně	k6eAd1
použitého	použitý	k2eAgInSc2d1
na	na	k7c4
PAL	pal	k1gInSc4
konzolích	konzole	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
PlayStation	PlayStation	k1gInSc1
2	#num#	k4
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
PlayStation	PlayStation	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Původní	původní	k2eAgFnSc3d1
konzole	konzola	k1gFnSc3
PlayStation	PlayStation	k1gInSc1
2	#num#	k4
(	(	kIx(
<g/>
vlevo	vlevo	k6eAd1
<g/>
)	)	kIx)
a	a	k8xC
konzole	konzola	k1gFnSc3
PlayStation	PlayStation	k1gInSc1
2	#num#	k4
Slimline	Slimlin	k1gInSc5
s	s	k7c7
8	#num#	k4
MB	MB	kA
paměťovou	paměťový	k2eAgFnSc7d1
kartou	karta	k1gFnSc7
a	a	k8xC
ovladačem	ovladač	k1gInSc7
DualShock	DualShocko	k1gNnPc2
2	#num#	k4
(	(	kIx(
<g/>
vpravo	vpravo	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
PlayStation	PlayStation	k1gInSc4
2	#num#	k4
bylo	být	k5eAaImAgNnS
vydáno	vydat	k5eAaPmNgNnS
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
<g/>
,	,	kIx,
15	#num#	k4
měsíců	měsíc	k1gInPc2
po	po	k7c6
vydání	vydání	k1gNnSc6
herní	herní	k2eAgFnSc2d1
konzole	konzola	k1gFnSc6
Dreamcast	Dreamcast	k1gInSc4
a	a	k8xC
rok	rok	k1gInSc4
před	před	k7c7
svými	svůj	k3xOyFgMnPc7
konkurenty	konkurent	k1gMnPc7
Xbox	Xbox	k1gInSc4
a	a	k8xC
Nintendo	Nintendo	k6eAd1
GameCube	GameCub	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c6
herní	herní	k2eAgFnSc6d1
konzole	konzola	k1gFnSc6
šesté	šestý	k4xOgFnPc4
generace	generace	k1gFnPc4
podporující	podporující	k2eAgFnSc4d1
zpětnou	zpětný	k2eAgFnSc4d1
kompatibilitu	kompatibilita	k1gFnSc4
s	s	k7c7
většinou	většina	k1gFnSc7
her	hra	k1gFnPc2
původního	původní	k2eAgInSc2d1
PlayStationu	PlayStation	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
svůj	svůj	k3xOyFgMnSc1
předchůdce	předchůdce	k1gMnSc1
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
on	on	k3xPp3gMnSc1
dostal	dostat	k5eAaPmAgMnS
štíhlejší	štíhlý	k2eAgNnSc4d2
redesign	redesign	k1gNnSc4
v	v	k7c6
podobě	podoba	k1gFnSc6
nového	nový	k2eAgInSc2d1
modelu	model	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
PlayStation	PlayStation	k1gInSc1
2	#num#	k4
je	být	k5eAaImIp3nS
nejúspěšnější	úspěšný	k2eAgFnSc7d3
herní	herní	k2eAgFnSc7d1
konzolí	konzole	k1gFnSc7
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
k	k	k7c3
28	#num#	k4
<g/>
.	.	kIx.
prosinci	prosinec	k1gInSc6
2012	#num#	k4
prodala	prodat	k5eAaPmAgFnS
více	hodně	k6eAd2
jak	jak	k8xC,k8xS
155	#num#	k4
milionů	milion	k4xCgInPc2
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
29	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2005	#num#	k4
se	se	k3xPyFc4
PS2	PS2	k1gMnSc1
stal	stát	k5eAaPmAgMnS
nejrychlejší	rychlý	k2eAgFnSc7d3
herní	herní	k2eAgFnSc7d1
konzolí	konzole	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
dosáhla	dosáhnout	k5eAaPmAgFnS
100	#num#	k4
milionů	milion	k4xCgInPc2
prodaných	prodaný	k2eAgInPc2d1
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rekordu	rekord	k1gInSc2
dosáhla	dosáhnout	k5eAaPmAgFnS
po	po	k7c6
5	#num#	k4
letech	let	k1gInPc6
a	a	k8xC
9	#num#	k4
měsících	měsíc	k1gInPc6
po	po	k7c4
spuštění	spuštění	k1gNnSc4
prodeje	prodej	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgInSc1d1
PlayStation	PlayStation	k1gInSc1
dosáhl	dosáhnout	k5eAaPmAgInS
stejné	stejný	k2eAgFnPc4d1
mety	meta	k1gFnPc4
po	po	k7c4
9	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
6	#num#	k4
měsících	měsíc	k1gInPc6
od	od	k7c2
spuštění	spuštění	k1gNnSc2
prodeje	prodej	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
28	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2012	#num#	k4
byla	být	k5eAaImAgFnS
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
odeslána	odeslat	k5eAaPmNgFnS
do	do	k7c2
obchodů	obchod	k1gInPc2
poslední	poslední	k2eAgFnSc1d1
zásilka	zásilka	k1gFnSc1
konzolí	konzolit	k5eAaPmIp3nS,k5eAaImIp3nS
PlayStation	PlayStation	k1gInSc4
2	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2013	#num#	k4
bylo	být	k5eAaImAgNnS
deníkem	deník	k1gInSc7
The	The	k1gFnPc2
Guardian	Guardiana	k1gFnPc2
oznámeno	oznámit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
skončila	skončit	k5eAaPmAgFnS
celosvětová	celosvětový	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
konzolí	konzolit	k5eAaPmIp3nS,k5eAaImIp3nS
PS	PS	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
studie	studie	k1gFnPc1
ukázaly	ukázat	k5eAaPmAgFnP
<g/>
,	,	kIx,
že	že	k8xS
mnoho	mnoho	k4c1
lidí	člověk	k1gMnPc2
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
stále	stále	k6eAd1
vlastní	vlastní	k2eAgMnSc1d1
jednu	jeden	k4xCgFnSc4
<g/>
,	,	kIx,
přestože	přestože	k8xS
ji	on	k3xPp3gFnSc4
už	už	k6eAd1
nadále	nadále	k6eAd1
nepoužívají	používat	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
roku	rok	k1gInSc3
2015	#num#	k4
byl	být	k5eAaImAgInS
PlayStation	PlayStation	k1gInSc4
2	#num#	k4
ohodnocen	ohodnocen	k2eAgInSc4d1
jako	jako	k8xS,k8xC
nejprodávanější	prodávaný	k2eAgFnSc3d3
konzole	konzola	k1gFnSc3
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Model	model	k1gInSc1
Slimline	Slimlin	k1gInSc5
</s>
<s>
PlayStation	PlayStation	k1gInSc1
2	#num#	k4
Slimline	Slimlin	k1gInSc5
byl	být	k5eAaImAgInS
vydán	vydat	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
<g/>
,	,	kIx,
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
po	po	k7c6
vydání	vydání	k1gNnSc6
původního	původní	k2eAgInSc2d1
PlayStationu	PlayStation	k1gInSc2
2	#num#	k4
<g/>
,	,	kIx,
a	a	k8xC
jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
první	první	k4xOgInSc4
hlavní	hlavní	k2eAgInSc4d1
redisgenovaný	redisgenovaný	k2eAgInSc4d1
model	model	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
srovnání	srovnání	k1gNnSc6
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
předchůdcem	předchůdce	k1gMnSc7
byl	být	k5eAaImAgInS
menší	malý	k2eAgInSc1d2
<g/>
,	,	kIx,
tenčí	tenký	k2eAgInPc1d2
a	a	k8xC
tišší	tichý	k2eAgInPc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
vestavěný	vestavěný	k2eAgInSc4d1
ethernetový	ethernetový	k2eAgInSc4d1
port	port	k1gInSc4
a	a	k8xC
na	na	k7c6
některých	některý	k3yIgInPc6
trzích	trh	k1gInPc6
také	také	k9
integrovaný	integrovaný	k2eAgInSc1d1
modem	modem	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
začalo	začít	k5eAaPmAgNnS
Sony	son	k1gInPc4
dodávat	dodávat	k5eAaImF
na	na	k7c4
trh	trh	k1gInSc4
předělanou	předělaný	k2eAgFnSc4d1
verzi	verze	k1gFnSc4
modelu	model	k1gInSc2
Slimline	Slimlin	k1gInSc5
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
lehčí	lehký	k2eAgInPc4d2
a	a	k8xC
obsahovala	obsahovat	k5eAaImAgFnS
AC	AC	kA
adaptér	adaptér	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
Sony	Sony	kA
konzoli	konzoli	k6eAd1
opět	opět	k6eAd1
předělalo	předělat	k5eAaPmAgNnS
a	a	k8xC
nová	nový	k2eAgFnSc1d1
verze	verze	k1gFnSc1
Slimline	Slimlin	k1gInSc5
měla	mít	k5eAaImAgFnS
přepracované	přepracovaný	k2eAgNnSc4d1
vnitřní	vnitřní	k2eAgNnSc4d1
<g/>
,	,	kIx,
hardwarové	hardwarový	k2eAgNnSc4d1
<g/>
,	,	kIx,
prostředí	prostředí	k1gNnSc4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vedlo	vést	k5eAaImAgNnS
k	k	k7c3
celkovému	celkový	k2eAgNnSc3d1
snížení	snížení	k1gNnSc3
hmotnosti	hmotnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
vestavění	vestavění	k1gNnSc3
AC	AC	kA
adaptéru	adaptér	k1gInSc2
do	do	k7c2
vnitřku	vnitřek	k1gInSc2
konzole	konzola	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgFnSc3d1
domácí	domácí	k2eAgFnSc3d1
konzole	konzola	k1gFnSc3
</s>
<s>
PlayStation	PlayStation	k1gInSc1
3	#num#	k4
</s>
<s>
verze	verze	k1gFnSc1
<g/>
:	:	kIx,
Slim	Slim	k1gInSc1
a	a	k8xC
Super	super	k2eAgNnSc1d1
Slime	Slim	k1gInSc5
</s>
<s>
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
</s>
<s>
verze	verze	k1gFnSc1
<g/>
:	:	kIx,
Slim	Slim	k1gInSc1
a	a	k8xC
Pro	pro	k7c4
</s>
<s>
PlayStation	PlayStation	k1gInSc1
5	#num#	k4
</s>
<s>
Produkty	produkt	k1gInPc1
</s>
<s>
Kapesní	kapesní	k2eAgFnSc3d1
herní	herní	k2eAgFnSc3d1
konzole	konzola	k1gFnSc3
</s>
<s>
PocketStation	PocketStation	k1gInSc1
</s>
<s>
PlayStation	PlayStation	k1gInSc1
Portable	portable	k1gInSc1
</s>
<s>
verze	verze	k1gFnSc1
<g/>
:	:	kIx,
PSP-	PSP-	k1gFnSc1
<g/>
2000	#num#	k4
<g/>
,	,	kIx,
PSP-	PSP-	k1gFnSc1
<g/>
3000	#num#	k4
<g/>
,	,	kIx,
PSP	PSP	kA
Go	Go	k1gFnSc1
a	a	k8xC
PSP-E1000	PSP-E1000	k1gFnSc1
</s>
<s>
PlayStation	PlayStation	k1gInSc1
Vita	vit	k2eAgFnSc1d1
</s>
<s>
Další	další	k2eAgInPc1d1
systémy	systém	k1gInPc1
</s>
<s>
PSX	PSX	kA
</s>
<s>
Xperia	Xperium	k1gNnPc1
Play	play	k0
</s>
<s>
Sony	Sony	kA
Tablet	tablet	k1gInSc1
</s>
<s>
PlayStation	PlayStation	k1gInSc1
TV	TV	kA
</s>
<s>
PlayStation	PlayStation	k1gInSc1
VR	vr	k0
</s>
<s>
PlayStation	PlayStation	k1gInSc1
Classic	Classice	k1gInPc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
PlayStation	PlayStation	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Business	business	k1gInSc1
Development	Development	k1gInSc1
<g/>
/	/	kIx~
<g/>
Japan	japan	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sony	Sony	kA
Computer	computer	k1gInSc1
Entertainment	Entertainment	k1gInSc1
Inc	Inc	k1gFnPc2
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
PlayStation	PlayStation	k1gInSc1
2	#num#	k4
Breaks	Breaksa	k1gFnPc2
Record	Recorda	k1gFnPc2
as	as	k9
the	the	k?
Fastest	Fastest	k1gInSc1
Computer	computer	k1gInSc1
Entertainment	Entertainment	k1gInSc1
Platform	Platform	k1gInSc1
to	ten	k3xDgNnSc1
Reach	Reach	k1gMnSc1
Cumulative	Cumulativ	k1gInSc5
Shipment	Shipment	k1gInSc4
of	of	k?
120	#num#	k4
Million	Million	k1gInSc1
Units	Units	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sony	Sony	kA
Computer	computer	k1gInSc1
Entertainment	Entertainment	k1gInSc1
<g/>
,	,	kIx,
2015-11-30	2015-11-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Media	medium	k1gNnPc1
Create	Creat	k1gInSc5
Sales	Sales	k1gInSc1
<g/>
:	:	kIx,
Week	Week	k1gInSc1
52	#num#	k4
<g/>
,	,	kIx,
2012	#num#	k4
(	(	kIx(
<g/>
Dec	Dec	k1gFnSc1
24	#num#	k4
-	-	kIx~
Dec	Dec	k1gFnSc1
30	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
NeoGAF	NeoGAF	k1gFnPc2
<g/>
,	,	kIx,
2013-01-07	2013-01-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SIE	SIE	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Business	business	k1gInSc1
Development	Development	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sony	Sony	kA
Interactive	Interactiv	k1gInSc5
Entertainment	Entertainment	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PLAYSTATION	PLAYSTATION	kA
<g/>
®	®	k?
<g/>
3	#num#	k4
SALES	SALES	kA
REACH	REACH	kA
80	#num#	k4
MILLION	MILLION	kA
UNITS	UNITS	kA
WORLDWIDE	WORLDWIDE	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2013-12-03	2013-12-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MCWHERTOR	MCWHERTOR	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
;	;	kIx,
CARPENTER	CARPENTER	kA
<g/>
,	,	kIx,
Nicole	Nicole	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PlayStation	PlayStation	k1gInSc1
5	#num#	k4
launches	launchesa	k1gFnPc2
Nov	nova	k1gFnPc2
<g/>
.	.	kIx.
12	#num#	k4
for	forum	k1gNnPc2
$	$	kIx~
<g/>
499	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polygon	polygon	k1gInSc1
<g/>
,	,	kIx,
2020-09-16	2020-09-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
JENKINS	JENKINS	kA
<g/>
,	,	kIx,
David	David	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Xbox	Xbox	k1gInSc1
One	One	k1gFnSc2
v	v	k7c4
PlayStation	PlayStation	k1gInSc4
4	#num#	k4
<g/>
:	:	kIx,
Who	Who	k1gMnSc1
will	will	k1gMnSc1
win	win	k?
the	the	k?
next-gen	next-gen	k1gInSc1
console	console	k1gFnSc2
race	rac	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
–	–	k?
Metro	metro	k1gNnSc1
News	News	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metro	metro	k1gNnSc1
<g/>
,	,	kIx,
2013-11-27	2013-11-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
STUART	STUART	kA
<g/>
,	,	kIx,
Keith	Keith	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PlayStation	PlayStation	k1gInSc1
2	#num#	k4
manufacture	manufactur	k1gMnSc5
ends	endsit	k5eAaPmRp2nS
after	after	k1gInSc4
12	#num#	k4
years	yearsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
<g/>
,	,	kIx,
2013-01-04	2013-01-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
News	News	k1gInSc1
<g/>
:	:	kIx,
Sony	Sony	kA
announces	announces	k1gInSc4
PS	PS	kA
Vita	vit	k2eAgFnSc1d1
TV	TV	kA
microconsole	microconsole	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ComputerAndVideoGames	ComputerAndVideoGames	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2013-09-09	2013-09-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
DualShock	DualShock	k1gMnSc1
3	#num#	k4
Wireless	Wireless	k1gInSc1
Controller	Controller	k1gInSc4
available	available	k6eAd1
for	forum	k1gNnPc2
PlayStation	PlayStation	k1gInSc4
3	#num#	k4
this	this	k1gInSc1
summer	summer	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Next-	Next-	k1gFnSc1
<g/>
Gen.	gen.	kA
<g/>
biz	biz	k?
<g/>
,	,	kIx,
2008-07-01	2008-07-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BOXER	boxer	k1gMnSc1
<g/>
,	,	kIx,
Steve	Steve	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sony	Sony	kA
chief	chief	k1gMnSc1
on	on	k3xPp3gMnSc1
the	the	k?
PS4	PS4	k1gMnSc7
and	and	k?
E3	E3	k1gFnSc1
battle	battle	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
<g/>
,	,	kIx,
2013-07-03	2013-07-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
CROFT	CROFT	kA
<g/>
,	,	kIx,
Liam	Liam	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PSN	PSN	kA
Reaches	Reaches	k1gInSc4
94	#num#	k4
Million	Million	k1gInSc4
Active	Actiev	k1gFnSc2
Users	Users	k1gInSc1
<g/>
,	,	kIx,
with	with	k1gMnSc1
Over	Over	k1gMnSc1
One	One	k1gMnSc1
Third	Third	k1gMnSc1
Subscribed	Subscribed	k1gMnSc1
to	ten	k3xDgNnSc4
PlayStation	PlayStation	k1gInSc1
Plus	plus	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Push	Pusha	k1gFnPc2
<g/>
,	,	kIx,
2019-05-21	2019-05-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Sony	Sony	kA
USA	USA	kA
–	–	k?
Electronics	Electronics	k1gInSc1
<g/>
,	,	kIx,
PlayStation	PlayStation	k1gInSc1
<g/>
,	,	kIx,
Movies	Movies	k1gMnSc1
<g/>
,	,	kIx,
Music	Music	k1gMnSc1
<g/>
,	,	kIx,
Product	Product	k1gMnSc1
Support	support	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sony	Sony	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PlayStation	PlayStation	k1gInSc1
Cumulative	Cumulativ	k1gInSc5
Production	Production	k1gInSc1
Shipments	Shipments	k1gInSc1
of	of	k?
Hardware	hardware	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sony	Sony	kA
Computer	computer	k1gInSc1
Entertainment	Entertainment	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
SMITH	SMITH	kA
<g/>
,	,	kIx,
Tony	Tony	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sony	Sony	kA
PSone	PSon	k1gInSc5
sales	sales	k1gInSc1
rocket	rocket	k1gInSc1
as	as	k1gInSc4
PS	PS	kA
Two	Two	k1gFnSc7
famine	faminout	k5eAaPmIp3nS
continues	continues	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
theregister	theregister	k1gInSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
uk	uk	k?
<g/>
,	,	kIx,
2006-12-06	2006-12-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SCEE	SCEE	kA
2000	#num#	k4
<g/>
—	—	k?
<g/>
Key	Key	k1gFnSc1
Facts	Facts	k1gInSc1
and	and	k?
Figures	Figures	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sony	Sony	kA
Computer	computer	k1gInSc1
Entertainment	Entertainment	k1gInSc1
Europe	Europ	k1gInSc5
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Rick	Rick	k1gInSc1
Aristotle	Aristotle	k1gFnSc2
Munarriz	Munarriza	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS2	PS2	k1gMnSc1
on	on	k3xPp3gMnSc1
fool	foonout	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
fool	fool	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2007-02-15	2007-02-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
プ	プ	k?
<g/>
2	#num#	k4
<g/>
の	の	k?
<g/>
（	（	k?
<g/>
2012	#num#	k4
<g/>
年	年	k?
<g/>
12	#num#	k4
<g/>
月	月	k?
<g/>
28	#num#	k4
<g/>
日	日	k?
<g/>
）	）	k?
<g/>
で	で	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Famicú	Famicú	k1gFnPc2
<g/>
,	,	kIx,
2012-12-28	2012-12-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PlayStation	PlayStation	k1gInSc1
2	#num#	k4
manufacture	manufactur	k1gMnSc5
ends	endsit	k5eAaPmRp2nS
after	after	k1gInSc4
12	#num#	k4
years	yearsa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Guardian	Guardian	k1gMnSc1
<g/>
,	,	kIx,
2013-01-04	2013-01-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
GANTAYAT	GANTAYAT	kA
<g/>
,	,	kIx,
Anoop	Anoop	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PS2	PS2	k1gFnSc1
Gets	Getsa	k1gFnPc2
Lighter	Lightra	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
IGN	IGN	kA
<g/>
,	,	kIx,
2007-06-08	2007-06-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Sony	Sony	kA
Computer	computer	k1gInSc1
Entertainment	Entertainment	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PlayStation	PlayStation	k1gInSc1
<g/>
®	®	k?
<g/>
2	#num#	k4
(	(	kIx(
<g/>
SCPH-	SCPH-	k1gFnSc1
<g/>
90000	#num#	k4
SERIES	SERIES	kA
<g/>
)	)	kIx)
comes	comes	k1gMnSc1
in	in	k?
a	a	k8xC
new	new	k?
design	design	k1gInSc1
and	and	k?
in	in	k?
three	three	k1gInSc1
color	color	k1gMnSc1
variations	variations	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sony	Sony	kA
Computer	computer	k1gInSc1
Entertainment	Entertainment	k1gInSc1
<g/>
,	,	kIx,
2005	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
PlayStation	PlayStation	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
PlayStation	PlayStation	k1gInSc1
Domácí	domácí	k2eAgInSc1d1
konzole	konzola	k1gFnSc3
</s>
<s>
PlayStation	PlayStation	k1gInSc1
1	#num#	k4
•	•	k?
PlayStation	PlayStation	k1gInSc1
2	#num#	k4
•	•	k?
PlayStation	PlayStation	k1gInSc1
3	#num#	k4
•	•	k?
PlayStation	PlayStation	k1gInSc1
4	#num#	k4
•	•	k?
PlayStation	PlayStation	k1gInSc1
5	#num#	k4
Kapesní	kapesní	k2eAgFnSc6d1
konzole	konzola	k1gFnSc6
</s>
<s>
PlayStation	PlayStation	k1gInSc1
Portable	portable	k1gInSc1
•	•	k?
PlayStation	PlayStation	k1gInSc1
Vita	vit	k2eAgFnSc1d1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Videohry	videohra	k1gFnPc4
</s>
