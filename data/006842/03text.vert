<s>
Pavol	Pavol	k1gInSc1	Pavol
Blaho	blaho	k1gNnSc1	blaho
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1903	[number]	k4	1903
Skalica	Skalic	k1gInSc2	Skalic
–	–	k?	–
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1987	[number]	k4	1987
Clinton	Clinton	k1gMnSc1	Clinton
Comer	Comer	k1gMnSc1	Comer
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
a	a	k8xC	a
československý	československý	k2eAgMnSc1d1	československý
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
poválečný	poválečný	k2eAgMnSc1d1	poválečný
poslanec	poslanec	k1gMnSc1	poslanec
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
za	za	k7c4	za
Demokratickou	demokratický	k2eAgFnSc4d1	demokratická
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
předáků	předák	k1gMnPc2	předák
Strany	strana	k1gFnSc2	strana
slobody	sloboda	k1gFnSc2	sloboda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
únorovém	únorový	k2eAgInSc6d1	únorový
převratu	převrat	k1gInSc6	převrat
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
emigrace	emigrace	k1gFnSc2	emigrace
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Rady	rada	k1gFnSc2	rada
svobodného	svobodný	k2eAgNnSc2d1	svobodné
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
významný	významný	k2eAgMnSc1d1	významný
politik	politik	k1gMnSc1	politik
Pavel	Pavel	k1gMnSc1	Pavel
Blaho	blaho	k6eAd1	blaho
(	(	kIx(	(
<g/>
1867	[number]	k4	1867
<g/>
–	–	k?	–
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1938	[number]	k4	1938
byl	být	k5eAaImAgMnS	být
funkcionářem	funkcionář	k1gMnSc7	funkcionář
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
zemědělského	zemědělský	k2eAgInSc2d1	zemědělský
a	a	k8xC	a
malorolnického	malorolnický	k2eAgInSc2d1	malorolnický
lidu	lid	k1gInSc2	lid
(	(	kIx(	(
<g/>
agrární	agrární	k2eAgFnSc1d1	agrární
strana	strana	k1gFnSc1	strana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tzv.	tzv.	kA	tzv.
slovenského	slovenský	k2eAgInSc2d1	slovenský
štátu	štát	k1gInSc2	štát
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
odbojovou	odbojový	k2eAgFnSc7d1	odbojová
skupinou	skupina	k1gFnSc7	skupina
Obrana	obrana	k1gFnSc1	obrana
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
utvořil	utvořit	k5eAaPmAgInS	utvořit
vlastní	vlastní	k2eAgFnSc4d1	vlastní
odbojovou	odbojový	k2eAgFnSc4d1	odbojová
skupinu	skupina	k1gFnSc4	skupina
v	v	k7c6	v
Nitře	Nitra	k1gFnSc6	Nitra
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
odbojové	odbojový	k2eAgFnSc6d1	odbojová
organizaci	organizace	k1gFnSc6	organizace
Flora	Flor	k1gInSc2	Flor
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgInS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
přípravě	příprava	k1gFnSc6	příprava
Slovenského	slovenský	k2eAgNnSc2d1	slovenské
národního	národní	k2eAgNnSc2d1	národní
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Zasedal	zasedat	k5eAaImAgMnS	zasedat
v	v	k7c6	v
povstalecké	povstalecký	k2eAgFnSc6d1	povstalecká
SNR	SNR	kA	SNR
a	a	k8xC	a
vykonával	vykonávat	k5eAaImAgInS	vykonávat
funkci	funkce	k1gFnSc4	funkce
zástupce	zástupka	k1gFnSc3	zástupka
pověřence	pověřenka	k1gFnSc3	pověřenka
železnic	železnice	k1gFnPc2	železnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poválečném	poválečný	k2eAgNnSc6d1	poválečné
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
politického	politický	k2eAgInSc2d1	politický
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Účastnil	účastnit	k5eAaImAgMnS	účastnit
se	se	k3xPyFc4	se
v	v	k7c4	v
delegaci	delegace	k1gFnSc4	delegace
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1945	[number]	k4	1945
jednala	jednat	k5eAaImAgFnS	jednat
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Edvardem	Edvard	k1gMnSc7	Edvard
Benešem	Beneš	k1gMnSc7	Beneš
a	a	k8xC	a
řešila	řešit	k5eAaImAgFnS	řešit
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
postavení	postavení	k1gNnSc4	postavení
Slovenska	Slovensko	k1gNnSc2	Slovensko
v	v	k7c6	v
obnovené	obnovený	k2eAgFnSc6d1	obnovená
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1945-1946	[number]	k4	1945-1946
byl	být	k5eAaImAgInS	být
poslancem	poslanec	k1gMnSc7	poslanec
Prozatímního	prozatímní	k2eAgNnSc2d1	prozatímní
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
za	za	k7c4	za
Demokratickou	demokratický	k2eAgFnSc4d1	demokratická
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
1946	[number]	k4	1946
za	za	k7c4	za
Stranu	strana	k1gFnSc4	strana
slobody	sloboda	k1gFnSc2	sloboda
<g/>
.	.	kIx.	.
</s>
<s>
Poslancem	poslanec	k1gMnSc7	poslanec
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
do	do	k7c2	do
konce	konec	k1gInSc2	konec
funkčního	funkční	k2eAgNnSc2d1	funkční
období	období	k1gNnSc2	období
(	(	kIx(	(
<g/>
do	do	k7c2	do
parlamentních	parlamentní	k2eAgFnPc2d1	parlamentní
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1946	[number]	k4	1946
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
podílet	podílet	k5eAaImF	podílet
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
nové	nový	k2eAgFnSc2d1	nová
slovenské	slovenský	k2eAgFnSc2d1	slovenská
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
Křesťansko-republikánská	křesťanskoepublikánský	k2eAgFnSc1d1	křesťansko-republikánský
strana	strana	k1gFnSc1	strana
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Strana	strana	k1gFnSc1	strana
slobody	sloboda	k1gFnSc2	sloboda
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
vznik	vznik	k1gInSc1	vznik
jako	jako	k8xS	jako
potenciální	potenciální	k2eAgNnSc1d1	potenciální
oslabení	oslabení	k1gNnSc1	oslabení
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
podporovala	podporovat	k5eAaImAgFnS	podporovat
i	i	k9	i
Komunistická	komunistický	k2eAgFnSc1d1	komunistická
strana	strana	k1gFnSc1	strana
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
politická	politický	k2eAgFnSc1d1	politická
formace	formace	k1gFnSc1	formace
měla	mít	k5eAaImAgFnS	mít
mít	mít	k5eAaImF	mít
větší	veliký	k2eAgInSc4d2	veliký
katolický	katolický	k2eAgInSc4d1	katolický
akcent	akcent	k1gInSc4	akcent
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1946	[number]	k4	1946
si	se	k3xPyFc3	se
strana	strana	k1gFnSc1	strana
podala	podat	k5eAaPmAgFnS	podat
přihlášku	přihláška	k1gFnSc4	přihláška
do	do	k7c2	do
Národní	národní	k2eAgFnSc2d1	národní
fronty	fronta	k1gFnSc2	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
snažilo	snažit	k5eAaImAgNnS	snažit
odštěpeneckou	odštěpenecký	k2eAgFnSc4d1	odštěpenecká
frakci	frakce	k1gFnSc4	frakce
izolovat	izolovat	k5eAaBmF	izolovat
podpisem	podpis	k1gInSc7	podpis
takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
aprílové	aprílový	k2eAgFnSc2d1	Aprílová
(	(	kIx(	(
<g/>
dubnové	dubnový	k2eAgFnSc2d1	dubnová
<g/>
)	)	kIx)	)
dohody	dohoda	k1gFnSc2	dohoda
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
zavázalo	zavázat	k5eAaPmAgNnS	zavázat
vyjít	vyjít	k5eAaPmF	vyjít
vstříc	vstříc	k7c3	vstříc
programovým	programový	k2eAgInPc3d1	programový
a	a	k8xC	a
personálním	personální	k2eAgInPc3d1	personální
zájmům	zájem	k1gInPc3	zájem
katolického	katolický	k2eAgInSc2d1	katolický
tábora	tábor	k1gInSc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
slobody	sloboda	k1gFnSc2	sloboda
tak	tak	k6eAd1	tak
sice	sice	k8xC	sice
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezískala	získat	k5eNaPmAgFnS	získat
masový	masový	k2eAgInSc4d1	masový
ohlas	ohlas	k1gInSc4	ohlas
<g/>
.	.	kIx.	.
</s>
<s>
Pavol	Pavol	k1gInSc1	Pavol
Blaho	blaho	k6eAd1	blaho
zasedal	zasedat	k5eAaImAgInS	zasedat
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
předsednictvu	předsednictvo	k1gNnSc6	předsednictvo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
získala	získat	k5eAaPmAgFnS	získat
Strana	strana	k1gFnSc1	strana
slobody	sloboda	k1gFnSc2	sloboda
jen	jen	k9	jen
několik	několik	k4yIc1	několik
procent	procento	k1gNnPc2	procento
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1946	[number]	k4	1946
se	se	k3xPyFc4	se
Blaho	blaho	k6eAd1	blaho
stal	stát	k5eAaPmAgMnS	stát
jejím	její	k3xOp3gInSc7	její
druhým	druhý	k4xOgMnSc7	druhý
místopředsedou	místopředseda	k1gMnSc7	místopředseda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1947	[number]	k4	1947
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
pověřencem	pověřenec	k1gMnSc7	pověřenec
pošt	pošta	k1gFnPc2	pošta
(	(	kIx(	(
<g/>
v	v	k7c4	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Sboru	sbor	k1gInSc3	sbor
pověřenců	pověřenec	k1gMnPc2	pověřenec
<g/>
,	,	kIx,	,
autonomní	autonomní	k2eAgInSc1d1	autonomní
exekutivní	exekutivní	k2eAgInSc1d1	exekutivní
orgán	orgán	k1gInSc1	orgán
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
únorového	únorový	k2eAgInSc2d1	únorový
převratu	převrat	k1gInSc2	převrat
strana	strana	k1gFnSc1	strana
nezaujala	zaujmout	k5eNaPmAgFnS	zaujmout
jasné	jasný	k2eAgNnSc4d1	jasné
stanovisko	stanovisko	k1gNnSc4	stanovisko
<g/>
.	.	kIx.	.
</s>
<s>
Blaho	blaho	k6eAd1	blaho
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
stal	stát	k5eAaPmAgInS	stát
předsedou	předseda	k1gMnSc7	předseda
"	"	kIx"	"
<g/>
obrozené	obrozený	k2eAgFnSc2d1	obrozená
<g/>
"	"	kIx"	"
Strany	strana	k1gFnSc2	strana
slobody	sloboda	k1gFnSc2	sloboda
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
plně	plně	k6eAd1	plně
loajální	loajální	k2eAgMnSc1d1	loajální
k	k	k7c3	k
novému	nový	k2eAgInSc3d1	nový
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
</s>
<s>
Podpořil	podpořit	k5eAaPmAgInS	podpořit
nový	nový	k2eAgInSc1d1	nový
kurz	kurz	k1gInSc1	kurz
před	před	k7c7	před
parlamentními	parlamentní	k2eAgFnPc7d1	parlamentní
volbami	volba	k1gFnPc7	volba
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1948	[number]	k4	1948
(	(	kIx(	(
<g/>
ponechal	ponechat	k5eAaPmAgMnS	ponechat
si	se	k3xPyFc3	se
i	i	k9	i
post	post	k1gInSc4	post
pověřence	pověřenec	k1gMnSc2	pověřenec
pošt	pošta	k1gFnPc2	pošta
i	i	k8xC	i
v	v	k7c4	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Sboru	sbor	k1gInSc2	sbor
pověřenců	pověřenec	k1gMnPc2	pověřenec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
ale	ale	k8xC	ale
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
emigrace	emigrace	k1gFnSc2	emigrace
<g/>
.	.	kIx.	.
</s>
<s>
Zapojil	zapojit	k5eAaPmAgInS	zapojit
se	se	k3xPyFc4	se
do	do	k7c2	do
činnosti	činnost	k1gFnSc2	činnost
Rady	rada	k1gFnSc2	rada
svobodného	svobodný	k2eAgNnSc2d1	svobodné
Československa	Československo	k1gNnSc2	Československo
jako	jako	k8xC	jako
stoupenec	stoupenec	k1gMnSc1	stoupenec
Petra	Petr	k1gMnSc2	Petr
Zenkla	Zenkla	k1gMnSc1	Zenkla
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
organizace	organizace	k1gFnSc2	organizace
Stálá	stálý	k2eAgFnSc1d1	stálá
konference	konference	k1gFnSc1	konference
slovenských	slovenský	k2eAgMnPc2d1	slovenský
demokratických	demokratický	k2eAgMnPc2d1	demokratický
exulantů	exulant	k1gMnPc2	exulant
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
krátce	krátce	k6eAd1	krátce
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
výboru	výbor	k1gInSc2	výbor
Rady	rada	k1gFnSc2	rada
svobodného	svobodný	k2eAgNnSc2d1	svobodné
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
emigrace	emigrace	k1gFnSc2	emigrace
odešel	odejít	k5eAaPmAgMnS	odejít
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1948	[number]	k4	1948
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
příslušníků	příslušník	k1gMnPc2	příslušník
finanční	finanční	k2eAgFnSc2d1	finanční
stráže	stráž	k1gFnSc2	stráž
a	a	k8xC	a
SNB	SNB	kA	SNB
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
v	v	k7c4	v
Perth	Perth	k1gInSc4	Perth
Amboy	Amboa	k1gFnSc2	Amboa
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
v	v	k7c6	v
Clinton	Clinton	k1gMnSc1	Clinton
Comer	Comer	k1gMnSc1	Comer
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
