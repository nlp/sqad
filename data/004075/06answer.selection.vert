<s>
Obilniny	obilnina	k1gFnPc1	obilnina
jsou	být	k5eAaImIp3nP	být
rostliny	rostlina	k1gFnPc4	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
lipnicovité	lipnicovitý	k2eAgFnSc2d1	lipnicovitý
<g/>
,	,	kIx,	,
využívané	využívaný	k2eAgFnSc2d1	využívaná
<g/>
,	,	kIx,	,
šlechtěné	šlechtěný	k2eAgFnSc2d1	šlechtěná
a	a	k8xC	a
pěstované	pěstovaný	k2eAgFnSc2d1	pěstovaná
především	především	k9	především
pro	pro	k7c4	pro
svá	svůj	k3xOyFgNnPc4	svůj
semena	semeno	k1gNnPc4	semeno
–	–	k?	–
zrna	zrno	k1gNnPc4	zrno
či	či	k8xC	či
obilky	obilka	k1gFnPc4	obilka
<g/>
.	.	kIx.	.
</s>
