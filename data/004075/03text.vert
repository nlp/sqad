<s>
Obilniny	obilnina	k1gFnPc1	obilnina
jsou	být	k5eAaImIp3nP	být
rostliny	rostlina	k1gFnPc4	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
lipnicovité	lipnicovitý	k2eAgFnSc2d1	lipnicovitý
<g/>
,	,	kIx,	,
využívané	využívaný	k2eAgFnSc2d1	využívaná
<g/>
,	,	kIx,	,
šlechtěné	šlechtěný	k2eAgFnSc2d1	šlechtěná
a	a	k8xC	a
pěstované	pěstovaný	k2eAgFnSc2d1	pěstovaná
především	především	k9	především
pro	pro	k7c4	pro
svá	svůj	k3xOyFgNnPc4	svůj
semena	semeno	k1gNnPc4	semeno
–	–	k?	–
zrna	zrno	k1gNnPc4	zrno
či	či	k8xC	či
obilky	obilka	k1gFnPc4	obilka
<g/>
.	.	kIx.	.
</s>
<s>
Obilí	obilí	k1gNnSc1	obilí
<g/>
,	,	kIx,	,
obiloviny	obilovina	k1gFnPc1	obilovina
<g/>
,	,	kIx,	,
také	také	k9	také
cereálie	cereálie	k1gFnPc1	cereálie
slouží	sloužit	k5eAaImIp3nP	sloužit
především	především	k9	především
k	k	k7c3	k
lidské	lidský	k2eAgFnSc3d1	lidská
výživě	výživa	k1gFnSc3	výživa
<g/>
.	.	kIx.	.
</s>
<s>
Obilovina	obilovina	k1gFnSc1	obilovina
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
obilky	obilka	k1gFnSc2	obilka
představuje	představovat	k5eAaImIp3nS	představovat
hlavní	hlavní	k2eAgInSc1d1	hlavní
produkt	produkt	k1gInSc1	produkt
obilnin	obilnina	k1gFnPc2	obilnina
Zrna	zrno	k1gNnSc2	zrno
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
buďto	buďto	k8xC	buďto
celá	celý	k2eAgFnSc1d1	celá
(	(	kIx(	(
<g/>
rýže	rýže	k1gFnSc1	rýže
<g/>
,	,	kIx,	,
žito	žito	k1gNnSc1	žito
<g/>
,	,	kIx,	,
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
oves	oves	k1gInSc1	oves
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
broušená	broušený	k2eAgFnSc1d1	broušená
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
nebo	nebo	k8xC	nebo
úplně	úplně	k6eAd1	úplně
zbavena	zbaven	k2eAgFnSc1d1	zbavena
obalu	obal	k1gInSc2	obal
zrna	zrno	k1gNnSc2	zrno
(	(	kIx(	(
<g/>
kroupy	kroupa	k1gFnSc2	kroupa
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
rýže	rýže	k1gFnSc1	rýže
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
rozemletá	rozemletý	k2eAgFnSc1d1	rozemletá
(	(	kIx(	(
<g/>
celozrnná	celozrnný	k2eAgFnSc1d1	celozrnná
mouka	mouka	k1gFnSc1	mouka
-	-	kIx~	-
rozemleté	rozemletý	k2eAgNnSc4d1	rozemleté
celé	celý	k2eAgNnSc4d1	celé
zrno	zrno	k1gNnSc4	zrno
<g/>
,	,	kIx,	,
vločky	vločka	k1gFnPc4	vločka
(	(	kIx(	(
<g/>
nejen	nejen	k6eAd1	nejen
ovesné	ovesný	k2eAgFnPc1d1	ovesná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krupky	krupka	k1gFnSc2	krupka
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
mouka	mouka	k1gFnSc1	mouka
nebo	nebo	k8xC	nebo
krupice	krupice	k1gFnSc1	krupice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zrno	zrno	k1gNnSc1	zrno
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zejména	zejména	k9	zejména
škrob	škrob	k1gInSc4	škrob
<g/>
,	,	kIx,	,
minerály	minerál	k1gInPc4	minerál
<g/>
,	,	kIx,	,
vitamíny	vitamín	k1gInPc4	vitamín
<g/>
,	,	kIx,	,
oleje	olej	k1gInPc4	olej
a	a	k8xC	a
vlákniny	vláknina	k1gFnPc4	vláknina
<g/>
.	.	kIx.	.
</s>
<s>
Zrna	zrno	k1gNnPc1	zrno
se	se	k3xPyFc4	se
také	také	k9	také
zkrmují	zkrmovat	k5eAaImIp3nP	zkrmovat
přímo	přímo	k6eAd1	přímo
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgFnPc1d1	celá
rostliny	rostlina	k1gFnPc1	rostlina
se	se	k3xPyFc4	se
využívají	využívat	k5eAaImIp3nP	využívat
jako	jako	k8xS	jako
zelená	zelený	k2eAgFnSc1d1	zelená
píce	píce	k1gFnSc1	píce
<g/>
.	.	kIx.	.
</s>
<s>
Nadzemní	nadzemní	k2eAgFnSc1d1	nadzemní
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
silážuje	silážovat	k5eAaImIp3nS	silážovat
(	(	kIx(	(
<g/>
kukuřice	kukuřice	k1gFnSc1	kukuřice
setá	setý	k2eAgFnSc1d1	setá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
jako	jako	k9	jako
sláma	sláma	k1gFnSc1	sláma
(	(	kIx(	(
<g/>
pšenice	pšenice	k1gFnSc1	pšenice
<g/>
,	,	kIx,	,
ječmen	ječmen	k1gInSc1	ječmen
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
rohože	rohož	k1gFnPc1	rohož
<g/>
,	,	kIx,	,
košíky	košík	k1gInPc1	košík
<g/>
,	,	kIx,	,
kartáče	kartáč	k1gInPc1	kartáč
(	(	kIx(	(
<g/>
čirok	čirok	k1gInSc1	čirok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
podíl	podíl	k1gInSc1	podíl
obilovin	obilovina	k1gFnPc2	obilovina
na	na	k7c6	na
lidské	lidský	k2eAgFnSc6d1	lidská
výživě	výživa	k1gFnSc6	výživa
je	být	k5eAaImIp3nS	být
odhadován	odhadovat	k5eAaImNgInS	odhadovat
na	na	k7c4	na
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
produkce	produkce	k1gFnSc1	produkce
obilnin	obilnina	k1gFnPc2	obilnina
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
rýže	rýže	k1gFnSc2	rýže
<g/>
)	)	kIx)	)
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
téměř	téměř	k6eAd1	téměř
1	[number]	k4	1
800	[number]	k4	800
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
bylo	být	k5eAaImAgNnS	být
34	[number]	k4	34
%	%	kIx~	%
použito	použít	k5eAaPmNgNnS	použít
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
jídla	jídlo	k1gNnSc2	jídlo
<g/>
,	,	kIx,	,
42	[number]	k4	42
%	%	kIx~	%
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
krmiva	krmivo	k1gNnSc2	krmivo
<g/>
,	,	kIx,	,
16	[number]	k4	16
%	%	kIx~	%
na	na	k7c4	na
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
využití	využití	k1gNnSc4	využití
(	(	kIx(	(
<g/>
např.	např.	kA	např.
výroba	výroba	k1gFnSc1	výroba
biopaliv	biopalit	k5eAaPmDgInS	biopalit
<g/>
)	)	kIx)	)
a	a	k8xC	a
zbytek	zbytek	k1gInSc4	zbytek
uskladněn	uskladněn	k2eAgInSc4d1	uskladněn
<g/>
.	.	kIx.	.
</s>
<s>
Výzkumem	výzkum	k1gInSc7	výzkum
obilnin	obilnina	k1gFnPc2	obilnina
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
Zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
výzkumný	výzkumný	k2eAgInSc1d1	výzkumný
ústav	ústav	k1gInSc1	ústav
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Výzkumný	výzkumný	k2eAgInSc1d1	výzkumný
ústav	ústav	k1gInSc1	ústav
obilnářský	obilnářský	k2eAgInSc1d1	obilnářský
<g/>
)	)	kIx)	)
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
<g/>
.	.	kIx.	.
</s>
<s>
Obilniny	obilnina	k1gFnPc1	obilnina
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
lipnicovité	lipnicovitý	k2eAgFnSc2d1	lipnicovitý
(	(	kIx(	(
<g/>
Poaceae	Poacea	k1gFnSc2	Poacea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnPc1d1	označovaná
také	také	k6eAd1	také
jako	jako	k9	jako
trávy	tráva	k1gFnSc2	tráva
<g/>
,	,	kIx,	,
řádu	řád	k1gInSc2	řád
lipnicokvěté	lipnicokvětý	k2eAgMnPc4d1	lipnicokvětý
(	(	kIx(	(
<g/>
lipnicotvaré	lipnicotvarý	k2eAgMnPc4d1	lipnicotvarý
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Poales	Poales	k1gInSc1	Poales
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
jednoleté	jednoletý	k2eAgFnPc1d1	jednoletá
i	i	k8xC	i
víceleté	víceletý	k2eAgFnPc1d1	víceletá
byliny	bylina	k1gFnPc1	bylina
se	s	k7c7	s
svazčitým	svazčitý	k2eAgInSc7d1	svazčitý
kořenovým	kořenový	k2eAgInSc7d1	kořenový
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Stonek	stonek	k1gInSc1	stonek
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
stéblo	stéblo	k1gNnSc4	stéblo
je	být	k5eAaImIp3nS	být
složen	složen	k2eAgMnSc1d1	složen
z	z	k7c2	z
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
dutých	dutý	k2eAgInPc2d1	dutý
článků	článek	k1gInPc2	článek
(	(	kIx(	(
<g/>
internody	internoda	k1gFnSc2	internoda
<g/>
)	)	kIx)	)
a	a	k8xC	a
kolének	kolénko	k1gNnPc2	kolénko
(	(	kIx(	(
<g/>
nody	noda	k1gFnSc2	noda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
prodlužování	prodlužování	k1gNnSc3	prodlužování
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
růstu	růst	k1gInSc3	růst
celé	celý	k2eAgFnSc2d1	celá
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
druhů	druh	k1gInPc2	druh
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
několik	několik	k4yIc4	několik
odnoží	odnož	k1gFnPc2	odnož
<g/>
.	.	kIx.	.
</s>
<s>
Pochva	pochva	k1gFnSc1	pochva
listů	list	k1gInPc2	list
vyrůstající	vyrůstající	k2eAgMnPc1d1	vyrůstající
z	z	k7c2	z
kolének	kolénko	k1gNnPc2	kolénko
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
listovou	listový	k2eAgFnSc4d1	listová
čepel	čepel	k1gFnSc4	čepel
s	s	k7c7	s
rovnoběžnou	rovnoběžný	k2eAgFnSc7d1	rovnoběžná
žilnatinou	žilnatina	k1gFnSc7	žilnatina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
pochvy	pochva	k1gFnSc2	pochva
a	a	k8xC	a
čepele	čepel	k1gInSc2	čepel
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
jazýček	jazýček	k1gInSc1	jazýček
a	a	k8xC	a
čepel	čepel	k1gFnSc1	čepel
je	být	k5eAaImIp3nS	být
zakončena	zakončit	k5eAaPmNgFnS	zakončit
oušky	ouško	k1gNnPc7	ouško
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
jazýčku	jazýček	k1gInSc2	jazýček
a	a	k8xC	a
oušek	ouško	k1gNnPc2	ouško
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rozlišovacím	rozlišovací	k2eAgInSc7d1	rozlišovací
znakem	znak	k1gInSc7	znak
některých	některý	k3yIgFnPc2	některý
obilnin	obilnina	k1gFnPc2	obilnina
(	(	kIx(	(
<g/>
pšenice	pšenice	k1gFnSc1	pšenice
<g/>
,	,	kIx,	,
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
žito	žito	k1gNnSc1	žito
<g/>
,	,	kIx,	,
oves	oves	k1gInSc1	oves
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obilniny	obilnina	k1gFnPc1	obilnina
rozdělujeme	rozdělovat	k5eAaImIp1nP	rozdělovat
podle	podle	k7c2	podle
morfologických	morfologický	k2eAgFnPc2d1	morfologická
a	a	k8xC	a
fyziologických	fyziologický	k2eAgFnPc2d1	fyziologická
vlastností	vlastnost	k1gFnPc2	vlastnost
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
slzovka	slzovka	k1gFnSc1	slzovka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Coix	Coix	k1gInSc1	Coix
lacryma-jobi	lacrymaobi	k1gNnSc1	lacryma-jobi
<g/>
)	)	kIx)	)
dochan	dochan	k1gInSc1	dochan
(	(	kIx(	(
<g/>
Pennisetum	Pennisetum	k1gNnSc1	Pennisetum
<g/>
)	)	kIx)	)
kalužnice	kalužnice	k1gFnSc1	kalužnice
<g/>
,	,	kIx,	,
korakán	korakán	k2eAgInSc1d1	korakán
(	(	kIx(	(
<g/>
Eleusine	Eleusin	k1gInSc5	Eleusin
<g/>
)	)	kIx)	)
milička	milička	k1gFnSc1	milička
<g/>
,	,	kIx,	,
tef	tef	k1gInSc1	tef
(	(	kIx(	(
<g/>
Eragrostis	Eragrostis	k1gInSc1	Eragrostis
<g/>
)	)	kIx)	)
paspal	paspal	k1gInSc1	paspal
(	(	kIx(	(
<g/>
Paspalum	Paspalum	k1gInSc1	Paspalum
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
lesknice	lesknice	k1gFnSc1	lesknice
(	(	kIx(	(
<g/>
chrastice	chrastice	k1gFnSc1	chrastice
-	-	kIx~	-
Phalaris	Phalaris	k1gFnSc1	Phalaris
<g/>
)	)	kIx)	)
ježatka	ježatka	k1gFnSc1	ježatka
(	(	kIx(	(
<g/>
Echinochloa	Echinochloa	k1gFnSc1	Echinochloa
<g/>
)	)	kIx)	)
rosička	rosička	k1gFnSc1	rosička
(	(	kIx(	(
<g/>
Digitaria	Digitarium	k1gNnPc1	Digitarium
<g/>
)	)	kIx)	)
troskut	troskut	k1gInSc1	troskut
(	(	kIx(	(
<g/>
Cynodon	Cynodon	k1gNnSc1	Cynodon
<g/>
)	)	kIx)	)
Pseudoobilniny	Pseudoobilnin	k2eAgFnPc1d1	Pseudoobilnin
(	(	kIx(	(
<g/>
pseudoobiloviny	pseudoobilovina	k1gFnPc1	pseudoobilovina
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
rostliny	rostlina	k1gFnPc1	rostlina
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
čeledí	čeleď	k1gFnPc2	čeleď
než	než	k8xS	než
lipnicovité	lipnicovitý	k2eAgFnSc2d1	lipnicovitý
(	(	kIx(	(
<g/>
Poaceae	Poacea	k1gFnSc2	Poacea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
přiřazují	přiřazovat	k5eAaImIp3nP	přiřazovat
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
skupině	skupina	k1gFnSc3	skupina
díky	díky	k7c3	díky
stejnému	stejný	k2eAgNnSc3d1	stejné
hospodářskému	hospodářský	k2eAgNnSc3d1	hospodářské
využití	využití	k1gNnSc3	využití
a	a	k8xC	a
chemickému	chemický	k2eAgNnSc3d1	chemické
složení	složení	k1gNnSc3	složení
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějšími	významný	k2eAgMnPc7d3	nejvýznamnější
představiteli	představitel	k1gMnPc7	představitel
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
laskavec	laskavec	k1gInSc1	laskavec
(	(	kIx(	(
<g/>
Amaranthus	Amaranthus	k1gInSc1	Amaranthus
–	–	k?	–
Amaranthaceae	Amaranthaceae	k1gInSc1	Amaranthaceae
<g/>
)	)	kIx)	)
merlík	merlík	k1gInSc1	merlík
(	(	kIx(	(
<g/>
Chenopodium	Chenopodium	k1gNnSc1	Chenopodium
–	–	k?	–
Amaranthaceae	Amaranthacea	k1gInSc2	Amaranthacea
<g/>
)	)	kIx)	)
pohanka	pohanka	k1gFnSc1	pohanka
(	(	kIx(	(
<g/>
Fagopyrum	Fagopyrum	k1gInSc1	Fagopyrum
–	–	k?	–
Polygonaceae	Polygonaceae	k1gInSc1	Polygonaceae
<g/>
)	)	kIx)	)
Obilniny	obilnina	k1gFnPc1	obilnina
jsou	být	k5eAaImIp3nP	být
rostliny	rostlina	k1gFnPc1	rostlina
poměrně	poměrně	k6eAd1	poměrně
nenáročné	náročný	k2eNgFnPc1d1	nenáročná
<g/>
,	,	kIx,	,
rostou	růst	k5eAaImIp3nP	růst
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
podmínkách	podmínka	k1gFnPc6	podmínka
a	a	k8xC	a
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
vysoké	vysoký	k2eAgInPc1d1	vysoký
výnosy	výnos	k1gInPc1	výnos
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
plody	plod	k1gInPc1	plod
-	-	kIx~	-
obiloviny	obilovina	k1gFnPc1	obilovina
-	-	kIx~	-
lze	lze	k6eAd1	lze
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
dlouho	dlouho	k6eAd1	dlouho
skladovat	skladovat	k5eAaImF	skladovat
<g/>
,	,	kIx,	,
vytvářet	vytvářet	k5eAaImF	vytvářet
zásoby	zásoba	k1gFnPc4	zásoba
a	a	k8xC	a
obchodovat	obchodovat	k5eAaImF	obchodovat
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
hrály	hrát	k5eAaImAgFnP	hrát
obilniny	obilnina	k1gFnPc1	obilnina
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
historii	historie	k1gFnSc6	historie
velmi	velmi	k6eAd1	velmi
významnou	významný	k2eAgFnSc4d1	významná
úlohu	úloha	k1gFnSc4	úloha
jak	jak	k8xS	jak
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
od	od	k7c2	od
sběračství	sběračství	k1gNnSc2	sběračství
a	a	k8xC	a
lovectví	lovectví	k1gNnSc2	lovectví
k	k	k7c3	k
usedlému	usedlý	k2eAgNnSc3d1	usedlé
zemědělství	zemědělství	k1gNnSc3	zemědělství
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
také	také	k9	také
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
popsali	popsat	k5eAaPmAgMnP	popsat
etnologové	etnolog	k1gMnPc1	etnolog
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
Středozápadě	středozápad	k1gInSc6	středozápad
indiánské	indiánský	k2eAgInPc4d1	indiánský
kmeny	kmen	k1gInPc4	kmen
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
živily	živit	k5eAaImAgFnP	živit
sběrem	sběr	k1gInSc7	sběr
zrna	zrno	k1gNnSc2	zrno
divoce	divoce	k6eAd1	divoce
rostoucích	rostoucí	k2eAgFnPc2d1	rostoucí
travin	travina	k1gFnPc2	travina
<g/>
.	.	kIx.	.
</s>
<s>
Postupným	postupný	k2eAgNnSc7d1	postupné
šlechtěním	šlechtění	k1gNnSc7	šlechtění
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
výnosy	výnos	k1gInPc4	výnos
a	a	k8xC	a
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
současného	současný	k2eAgNnSc2d1	současné
zrání	zrání	k1gNnSc2	zrání
zrna	zrno	k1gNnSc2	zrno
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
klasickou	klasický	k2eAgFnSc4d1	klasická
sklizeň	sklizeň	k1gFnSc4	sklizeň
<g/>
.	.	kIx.	.
</s>
<s>
Skladováním	skladování	k1gNnSc7	skladování
obilovin	obilovina	k1gFnPc2	obilovina
v	v	k7c6	v
obilních	obilní	k2eAgFnPc6d1	obilní
jamách	jamách	k?	jamách
vznikaly	vznikat	k5eAaImAgFnP	vznikat
zásoby	zásoba	k1gFnPc4	zásoba
a	a	k8xC	a
přebytky	přebytek	k1gInPc4	přebytek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
pak	pak	k6eAd1	pak
umožnily	umožnit	k5eAaPmAgInP	umožnit
i	i	k8xC	i
vznik	vznik	k1gInSc1	vznik
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
řemesel	řemeslo	k1gNnPc2	řemeslo
a	a	k8xC	a
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
směňovaly	směňovat	k5eAaImAgFnP	směňovat
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
podíl	podíl	k1gInSc1	podíl
obilovin	obilovina	k1gFnPc2	obilovina
na	na	k7c6	na
celosvětové	celosvětový	k2eAgFnSc6d1	celosvětová
lidské	lidský	k2eAgFnSc6d1	lidská
výživě	výživa	k1gFnSc6	výživa
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
60	[number]	k4	60
-	-	kIx~	-
70	[number]	k4	70
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
chudých	chudý	k2eAgFnPc6d1	chudá
rozvojových	rozvojový	k2eAgFnPc6d1	rozvojová
zemích	zem	k1gFnPc6	zem
ale	ale	k8xC	ale
ještě	ještě	k6eAd1	ještě
daleko	daleko	k6eAd1	daleko
víc	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
proto	proto	k8xC	proto
také	také	k9	také
hrozí	hrozit	k5eAaImIp3nP	hrozit
choroby	choroba	k1gFnPc1	choroba
<g/>
,	,	kIx,	,
způsobené	způsobený	k2eAgNnSc1d1	způsobené
jednostrannou	jednostranný	k2eAgFnSc7d1	jednostranná
výživou	výživa	k1gFnSc7	výživa
<g/>
:	:	kIx,	:
Beri-beri	berieri	k1gNnSc4	beri-beri
–	–	k?	–
nemoc	nemoc	k1gFnSc1	nemoc
způsobená	způsobený	k2eAgFnSc1d1	způsobená
jednostrannou	jednostranný	k2eAgFnSc7d1	jednostranná
výživou	výživa	k1gFnSc7	výživa
loupanou	loupaný	k2eAgFnSc7d1	loupaná
rýží	rýže	k1gFnSc7	rýže
Pellagra	Pellagro	k1gNnSc2	Pellagro
–	–	k?	–
nemoc	nemoc	k1gFnSc1	nemoc
způsobená	způsobený	k2eAgFnSc1d1	způsobená
jednostrannou	jednostranný	k2eAgFnSc7d1	jednostranná
výživou	výživa	k1gFnSc7	výživa
kukuřicí	kukuřice	k1gFnSc7	kukuřice
České	český	k2eAgNnSc1d1	české
slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
obilí	obilí	k1gNnSc1	obilí
<g/>
"	"	kIx"	"
má	mít	k5eAaImIp3nS	mít
společný	společný	k2eAgInSc4d1	společný
původ	původ	k1gInSc4	původ
s	s	k7c7	s
ruským	ruský	k2eAgInSc7d1	ruský
"	"	kIx"	"
<g/>
obilje	obilj	k1gInPc4	obilj
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
znamená	znamenat	k5eAaImIp3nS	znamenat
zásobu	zásoba	k1gFnSc4	zásoba
<g/>
,	,	kIx,	,
hojnost	hojnost	k1gFnSc4	hojnost
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
"	"	kIx"	"
<g/>
cereálie	cereálie	k1gFnPc1	cereálie
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
latinského	latinský	k2eAgInSc2d1	latinský
původu	původ	k1gInSc2	původ
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
římské	římský	k2eAgFnSc2d1	římská
bohyně	bohyně	k1gFnSc2	bohyně
Cerery	Cerera	k1gFnSc2	Cerera
<g/>
.	.	kIx.	.
</s>
