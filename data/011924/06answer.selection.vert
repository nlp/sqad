<s>
Charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
znakem	znak	k1gInSc7	znak
všech	všecek	k3xTgInPc2	všecek
rysů	rys	k1gInPc2	rys
jsou	být	k5eAaImIp3nP	být
trojúhelníkovité	trojúhelníkovitý	k2eAgFnPc1d1	trojúhelníkovitá
uši	ucho	k1gNnPc4	ucho
s	s	k7c7	s
černými	černý	k2eAgInPc7d1	černý
chomáčky	chomáček	k1gInPc7	chomáček
chlupů	chlup	k1gInPc2	chlup
na	na	k7c6	na
konci	konec	k1gInSc6	konec
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
chvostky	chvostek	k1gInPc1	chvostek
<g/>
)	)	kIx)	)
a	a	k8xC	a
černý	černý	k2eAgInSc1d1	černý
konec	konec	k1gInSc1	konec
ocasu	ocas	k1gInSc2	ocas
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
jedinců	jedinec	k1gMnPc2	jedinec
má	mít	k5eAaImIp3nS	mít
lícní	lícní	k2eAgInPc4d1	lícní
chlupy	chlup	k1gInPc4	chlup
prodloužené	prodloužený	k2eAgInPc4d1	prodloužený
a	a	k8xC	a
utvářející	utvářející	k2eAgInPc4d1	utvářející
licousy	licousy	k1gInPc4	licousy
<g/>
.	.	kIx.	.
</s>
