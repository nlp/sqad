<p>
<s>
Calvados	Calvados	k1gInSc1	Calvados
je	být	k5eAaImIp3nS	být
francouzský	francouzský	k2eAgInSc4d1	francouzský
departement	departement	k1gInSc4	departement
ležící	ležící	k2eAgInSc4d1	ležící
v	v	k7c6	v
regionu	region	k1gInSc6	region
Normandie	Normandie	k1gFnSc2	Normandie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nejvýznamnější	významný	k2eAgNnPc1d3	nejvýznamnější
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Caen	Caen	k1gMnSc1	Caen
</s>
</p>
<p>
<s>
Hérouville-Saint-Clair	Hérouville-Saint-Clair	k1gMnSc1	Hérouville-Saint-Clair
</s>
</p>
<p>
<s>
Lisieux	Lisieux	k1gInSc1	Lisieux
</s>
</p>
<p>
<s>
Bayeux	Bayeux	k1gInSc1	Bayeux
</s>
</p>
<p>
<s>
Vire-Normandie	Vire-Normandie	k1gFnSc1	Vire-Normandie
</s>
</p>
<p>
<s>
Mondeville	Mondeville	k6eAd1	Mondeville
</s>
</p>
<p>
<s>
==	==	k?	==
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
rozdělení	rozdělení	k1gNnSc1	rozdělení
==	==	k?	==
</s>
</p>
<p>
<s>
Departement	departement	k1gInSc1	departement
Calvados	Calvadosa	k1gFnPc2	Calvadosa
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
4	[number]	k4	4
arrondissementů	arrondissement	k1gInPc2	arrondissement
<g/>
,	,	kIx,	,
25	[number]	k4	25
kantonů	kanton	k1gInPc2	kanton
a	a	k8xC	a
621	[number]	k4	621
obcí	obec	k1gFnPc2	obec
(	(	kIx(	(
<g/>
stav	stav	k1gInSc1	stav
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
procesu	proces	k1gInSc2	proces
slučování	slučování	k1gNnSc2	slučování
obcí	obec	k1gFnPc2	obec
lze	lze	k6eAd1	lze
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
obcí	obec	k1gFnPc2	obec
bude	být	k5eAaImBp3nS	být
snižovat	snižovat	k5eAaImF	snižovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
departementu	departement	k1gInSc6	departement
Calvados	Calvadosa	k1gFnPc2	Calvadosa
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Calvados	Calvadosa	k1gFnPc2	Calvadosa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Calvados	Calvadosa	k1gFnPc2	Calvadosa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
místní	místní	k2eAgFnSc2d1	místní
prefektury	prefektura	k1gFnSc2	prefektura
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
generální	generální	k2eAgFnSc2d1	generální
rady	rada	k1gFnSc2	rada
</s>
</p>
