<p>
<s>
Miklós	Miklós	k1gInSc1	Miklós
Horthy	Hortha	k1gFnSc2	Hortha
de	de	k?	de
Nagybánya	Nagybánya	k1gMnSc1	Nagybánya
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1868	[number]	k4	1868
<g/>
,	,	kIx,	,
Kenderes	Kenderes	k1gInSc1	Kenderes
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
Estoril	Estoril	k1gInSc1	Estoril
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
maďarský	maďarský	k2eAgMnSc1d1	maďarský
viceadmirál	viceadmirál	k1gMnSc1	viceadmirál
velící	velící	k2eAgFnSc2d1	velící
rakousko-uherskému	rakouskoherský	k2eAgNnSc3d1	rakousko-uherské
válečnému	válečný	k2eAgNnSc3d1	válečné
loďstvu	loďstvo	k1gNnSc3	loďstvo
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
regentem	regens	k1gMnSc7	regens
Maďarského	maďarský	k2eAgNnSc2d1	Maďarské
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
funkci	funkce	k1gFnSc3	funkce
hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
vykonával	vykonávat	k5eAaImAgInS	vykonávat
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1920	[number]	k4	1920
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc1	mládí
a	a	k8xC	a
kariéra	kariéra	k1gFnSc1	kariéra
u	u	k7c2	u
válečného	válečný	k2eAgNnSc2d1	válečné
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
==	==	k?	==
</s>
</p>
<p>
<s>
Miklós	Miklós	k1gInSc1	Miklós
Horthy	Hortha	k1gFnSc2	Hortha
pocházel	pocházet	k5eAaImAgInS	pocházet
ze	z	k7c2	z
šlechtického	šlechtický	k2eAgInSc2d1	šlechtický
kalvinistického	kalvinistický	k2eAgInSc2d1	kalvinistický
rodu	rod	k1gInSc2	rod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
cestoval	cestovat	k5eAaImAgInS	cestovat
hodně	hodně	k6eAd1	hodně
po	po	k7c6	po
světě	svět	k1gInSc6	svět
a	a	k8xC	a
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
rakousko-uherský	rakouskoherský	k2eAgMnSc1d1	rakousko-uherský
diplomat	diplomat	k1gMnSc1	diplomat
v	v	k7c6	v
Turecku	Turecko	k1gNnSc6	Turecko
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
do	do	k7c2	do
1914	[number]	k4	1914
byl	být	k5eAaImAgInS	být
osobním	osobní	k2eAgInSc7d1	osobní
tajemníkem	tajemník	k1gInSc7	tajemník
rakouského	rakouský	k2eAgMnSc2d1	rakouský
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc4	František
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
<g/>
,	,	kIx,	,
kterého	který	k3yRgNnSc2	který
si	se	k3xPyFc3	se
osobně	osobně	k6eAd1	osobně
velmi	velmi	k6eAd1	velmi
vážil	vážit	k5eAaImAgInS	vážit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
vyznamenal	vyznamenat	k5eAaPmAgInS	vyznamenat
v	v	k7c6	v
rakousko-uherském	rakouskoherský	k2eAgNnSc6d1	rakousko-uherské
válečném	válečný	k2eAgNnSc6d1	válečné
loďstvu	loďstvo	k1gNnSc6	loďstvo
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
kapitána	kapitán	k1gMnSc2	kapitán
na	na	k7c6	na
lehkém	lehký	k2eAgInSc6d1	lehký
křižníku	křižník	k1gInSc6	křižník
Novara	Novara	k1gFnSc1	Novara
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
již	již	k9	již
jako	jako	k9	jako
admirál	admirál	k1gMnSc1	admirál
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
několika	několik	k4yIc6	několik
bitvách	bitva	k1gFnPc6	bitva
podařilo	podařit	k5eAaPmAgNnS	podařit
opakovaně	opakovaně	k6eAd1	opakovaně
porazit	porazit	k5eAaPmF	porazit
italské	italský	k2eAgNnSc4d1	italské
válečné	válečný	k2eAgNnSc4d1	válečné
loďstvo	loďstvo	k1gNnSc4	loďstvo
<g/>
,	,	kIx,	,
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
v	v	k7c6	v
Otrantské	Otrantský	k2eAgFnSc6d1	Otrantský
úžině	úžina	k1gFnSc6	úžina
v	v	k7c6	v
Jaderském	jaderský	k2eAgNnSc6d1	Jaderské
moři	moře	k1gNnSc6	moře
byl	být	k5eAaImAgMnS	být
raněn	ranit	k5eAaPmNgMnS	ranit
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
válečným	válečný	k2eAgInPc3d1	válečný
úspěchům	úspěch	k1gInPc3	úspěch
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1918	[number]	k4	1918
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
vrchního	vrchní	k2eAgMnSc2d1	vrchní
velitele	velitel	k1gMnSc2	velitel
válečného	válečný	k2eAgNnSc2d1	válečné
loďstva	loďstvo	k1gNnSc2	loďstvo
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
ovšem	ovšem	k9	ovšem
zůstal	zůstat	k5eAaPmAgInS	zůstat
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
příkazem	příkaz	k1gInSc7	příkaz
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
I.	I.	kA	I.
uloženo	uložen	k2eAgNnSc1d1	uloženo
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
námořní	námořní	k2eAgFnSc2d1	námořní
flotily	flotila	k1gFnSc2	flotila
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
nově	nově	k6eAd1	nově
vzniklého	vzniklý	k2eAgInSc2d1	vzniklý
Státu	stát	k1gInSc2	stát
Slovinců	Slovinec	k1gMnPc2	Slovinec
<g/>
,	,	kIx,	,
Chorvatů	Chorvat	k1gMnPc2	Chorvat
a	a	k8xC	a
Srbů	Srb	k1gMnPc2	Srb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Meziválečné	meziválečný	k2eAgNnSc1d1	meziválečné
období	období	k1gNnSc1	období
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
==	==	k?	==
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Béla	Béla	k1gMnSc1	Béla
Kun	kuna	k1gFnPc2	kuna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
chopil	chopit	k5eAaPmAgMnS	chopit
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
ustavil	ustavit	k5eAaPmAgMnS	ustavit
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
tzv.	tzv.	kA	tzv.
Maďarskou	maďarský	k2eAgFnSc4d1	maďarská
republiku	republika	k1gFnSc4	republika
rad	rada	k1gFnPc2	rada
podle	podle	k7c2	podle
sovětského	sovětský	k2eAgInSc2d1	sovětský
vzoru	vzor	k1gInSc2	vzor
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Horthy	Hortha	k1gFnSc2	Hortha
jako	jako	k8xS	jako
uznávaný	uznávaný	k2eAgMnSc1d1	uznávaný
válečný	válečný	k2eAgMnSc1d1	válečný
hrdina	hrdina	k1gMnSc1	hrdina
pověřen	pověřit	k5eAaPmNgMnS	pověřit
kontrarevoluční	kontrarevoluční	k2eAgFnSc7d1	kontrarevoluční
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
ustavena	ustaven	k2eAgFnSc1d1	ustavena
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1919	[number]	k4	1919
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Szeged	Szeged	k1gInSc1	Szeged
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
francouzské	francouzský	k2eAgFnSc2d1	francouzská
armády	armáda	k1gFnSc2	armáda
<g/>
)	)	kIx)	)
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
,	,	kIx,	,
k	k	k7c3	k
organizaci	organizace	k1gFnSc3	organizace
a	a	k8xC	a
vedení	vedení	k1gNnSc3	vedení
Národní	národní	k2eAgFnSc2d1	národní
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
režim	režim	k1gInSc4	režim
Bély	Béla	k1gMnPc4	Béla
Kuna	kuna	k1gFnSc1	kuna
potlačit	potlačit	k5eAaPmF	potlačit
<g/>
.	.	kIx.	.
</s>
<s>
Horthyho	Horthyze	k6eAd1	Horthyze
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
nezúčastnila	zúčastnit	k5eNaPmAgFnS	zúčastnit
žádných	žádný	k3yNgInPc2	žádný
bojů	boj	k1gInPc2	boj
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
souhlasem	souhlas	k1gInSc7	souhlas
Trojdohody	Trojdohoda	k1gFnSc2	Trojdohoda
bylo	být	k5eAaImAgNnS	být
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
vojensky	vojensky	k6eAd1	vojensky
napadeno	napaden	k2eAgNnSc1d1	napadeno
Rumunskem	Rumunsko	k1gNnSc7	Rumunsko
<g/>
,	,	kIx,	,
kterému	který	k3yRgInSc3	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
režim	režim	k1gInSc4	režim
Bély	Béla	k1gMnSc2	Béla
Kuna	Kuna	k1gMnSc1	Kuna
porazit	porazit	k5eAaPmF	porazit
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
rumunská	rumunský	k2eAgFnSc1d1	rumunská
armáda	armáda	k1gFnSc1	armáda
do	do	k7c2	do
Budapešti	Budapešť	k1gFnSc2	Budapešť
a	a	k8xC	a
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
setrvala	setrvat	k5eAaPmAgFnS	setrvat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
rumunské	rumunský	k2eAgFnSc2d1	rumunská
armády	armáda	k1gFnSc2	armáda
z	z	k7c2	z
Budapešti	Budapešť	k1gFnSc2	Budapešť
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1919	[number]	k4	1919
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
města	město	k1gNnSc2	město
triumfálně	triumfálně	k6eAd1	triumfálně
Horthy	Hortha	k1gFnSc2	Hortha
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
Národní	národní	k2eAgFnSc1d1	národní
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Rumunská	rumunský	k2eAgFnSc1d1	rumunská
armáda	armáda	k1gFnSc1	armáda
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pokynu	pokyn	k1gInSc2	pokyn
Trojdohody	Trojdohoda	k1gFnSc2	Trojdohoda
opustila	opustit	k5eAaPmAgFnS	opustit
definitivně	definitivně	k6eAd1	definitivně
Maďarsko	Maďarsko	k1gNnSc4	Maďarsko
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1920	[number]	k4	1920
obnovilo	obnovit	k5eAaPmAgNnS	obnovit
maďarské	maďarský	k2eAgNnSc1d1	Maďarské
národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
měly	mít	k5eAaImAgInP	mít
výraznou	výrazný	k2eAgFnSc4d1	výrazná
převahu	převaha	k1gFnSc4	převaha
konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
síly	síla	k1gFnSc2	síla
<g/>
)	)	kIx)	)
monarchii	monarchie	k1gFnSc4	monarchie
-	-	kIx~	-
Maďarské	maďarský	k2eAgNnSc1d1	Maďarské
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
se	se	k3xPyFc4	se
nepovolat	povolat	k5eNaPmF	povolat
posledního	poslední	k2eAgMnSc2d1	poslední
rakouského	rakouský	k2eAgMnSc2d1	rakouský
císaře	císař	k1gMnSc2	císař
Karla	Karel	k1gMnSc2	Karel
I.	I.	kA	I.
(	(	kIx(	(
<g/>
což	což	k3yQnSc4	což
byl	být	k5eAaImAgMnS	být
zároveň	zároveň	k6eAd1	zároveň
současně	současně	k6eAd1	současně
poslední	poslední	k2eAgMnSc1d1	poslední
český	český	k2eAgMnSc1d1	český
-	-	kIx~	-
nikdy	nikdy	k6eAd1	nikdy
nekorunovaný	korunovaný	k2eNgMnSc1d1	nekorunovaný
-	-	kIx~	-
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
poslední	poslední	k2eAgMnSc1d1	poslední
uherský	uherský	k2eAgMnSc1d1	uherský
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
jinak	jinak	k6eAd1	jinak
na	na	k7c4	na
uherskou	uherský	k2eAgFnSc4d1	uherská
korunu	koruna	k1gFnSc4	koruna
nárok	nárok	k1gInSc1	nárok
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
něj	on	k3xPp3gNnSc2	on
byl	být	k5eAaImAgMnS	být
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
admirál	admirál	k1gMnSc1	admirál
ve	v	k7c6	v
výslužbě	výslužba	k1gFnSc6	výslužba
a	a	k8xC	a
válečný	válečný	k2eAgMnSc1d1	válečný
hrdina	hrdina	k1gMnSc1	hrdina
Miklós	Miklósa	k1gFnPc2	Miklósa
Horthy	Hortha	k1gFnSc2	Hortha
za	za	k7c4	za
tzv.	tzv.	kA	tzv.
regenta	regens	k1gMnSc4	regens
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
a	a	k8xC	a
hlavu	hlava	k1gFnSc4	hlava
státu	stát	k1gInSc2	stát
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
na	na	k7c4	na
neomezenou	omezený	k2eNgFnSc4d1	neomezená
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
a	a	k8xC	a
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1921	[number]	k4	1921
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
pokusil	pokusit	k5eAaPmAgMnS	pokusit
znovuzískat	znovuzískat	k5eAaPmF	znovuzískat
uherský	uherský	k2eAgInSc4d1	uherský
trůn	trůn	k1gInSc4	trůn
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
formou	forma	k1gFnSc7	forma
přesvědčování	přesvědčování	k1gNnSc2	přesvědčování
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
formou	forma	k1gFnSc7	forma
pochodu	pochod	k1gInSc2	pochod
na	na	k7c4	na
Budapešť	Budapešť	k1gFnSc4	Budapešť
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oba	dva	k4xCgInPc1	dva
pokusy	pokus	k1gInPc1	pokus
ovšem	ovšem	k9	ovšem
Horthy	Hortha	k1gFnPc4	Hortha
překazil	překazit	k5eAaPmAgMnS	překazit
a	a	k8xC	a
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
byli	být	k5eAaImAgMnP	být
Habsburkové	Habsburk	k1gMnPc1	Habsburk
oficiálně	oficiálně	k6eAd1	oficiálně
detronizováni	detronizován	k2eAgMnPc1d1	detronizován
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
byl	být	k5eAaImAgMnS	být
přinucen	přinucen	k2eAgMnSc1d1	přinucen
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
a	a	k8xC	a
Horthy	Horth	k1gMnPc7	Horth
zůstal	zůstat	k5eAaPmAgMnS	zůstat
regentem	regens	k1gMnSc7	regens
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
24	[number]	k4	24
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vládl	vládnout	k5eAaImAgInS	vládnout
maďarskému	maďarský	k2eAgInSc3d1	maďarský
"	"	kIx"	"
<g/>
království	království	k1gNnPc4	království
bez	bez	k7c2	bez
krále	král	k1gMnSc2	král
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Horthyho	Horthyze	k6eAd1	Horthyze
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
se	se	k3xPyFc4	se
v	v	k7c6	v
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
politice	politika	k1gFnSc6	politika
orientovalo	orientovat	k5eAaBmAgNnS	orientovat
především	především	k9	především
na	na	k7c6	na
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
volným	volný	k2eAgMnSc7d1	volný
spojencem	spojenec	k1gMnSc7	spojenec
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fall	Fall	k1gMnSc1	Fall
Grün	Grün	k1gMnSc1	Grün
<g/>
,	,	kIx,	,
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
<g/>
,	,	kIx,	,
Česko-Slovensko	Česko-Slovensko	k1gNnSc1	Česko-Slovensko
a	a	k8xC	a
Slovenský	slovenský	k2eAgInSc1d1	slovenský
stát	stát	k1gInSc1	stát
===	===	k?	===
</s>
</p>
<p>
<s>
Horthy	Horth	k1gMnPc4	Horth
také	také	k9	také
představoval	představovat	k5eAaImAgMnS	představovat
pro	pro	k7c4	pro
Hitlera	Hitler	k1gMnSc4	Hitler
významného	významný	k2eAgMnSc2d1	významný
partnera	partner	k1gMnSc2	partner
při	při	k7c6	při
rozbíjení	rozbíjení	k1gNnSc6	rozbíjení
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
po	po	k7c6	po
Mnichovské	mnichovský	k2eAgFnSc6d1	Mnichovská
dohodě	dohoda	k1gFnSc6	dohoda
Česko-Slovenska	Česko-Slovensko	k1gNnSc2	Česko-Slovensko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpnem	srpen	k1gInSc7	srpen
a	a	k8xC	a
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpnem	srpen	k1gInSc7	srpen
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
více	hodně	k6eAd2	hodně
než	než	k8xS	než
měsíc	měsíc	k1gInSc4	měsíc
před	před	k7c7	před
Mnichovskou	mnichovský	k2eAgFnSc7d1	Mnichovská
dohodou	dohoda	k1gFnSc7	dohoda
<g/>
,	,	kIx,	,
navštívila	navštívit	k5eAaPmAgFnS	navštívit
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
maďarská	maďarský	k2eAgFnSc1d1	maďarská
vládní	vládní	k2eAgFnSc1d1	vládní
delegace	delegace	k1gFnSc1	delegace
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Horthym	Horthym	k1gInSc1	Horthym
(	(	kIx(	(
<g/>
další	další	k2eAgMnPc1d1	další
členové	člen	k1gMnPc1	člen
delegace	delegace	k1gFnSc2	delegace
byli	být	k5eAaImAgMnP	být
předseda	předseda	k1gMnSc1	předseda
maďarské	maďarský	k2eAgFnSc2d1	maďarská
vlády	vláda	k1gFnSc2	vláda
Béla	Béla	k1gMnSc1	Béla
Imrédy	Imréda	k1gFnSc2	Imréda
<g/>
,	,	kIx,	,
ministr	ministr	k1gMnSc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
Kálmán	Kálmán	k2eAgMnSc1d1	Kálmán
Kánya	Kánya	k1gMnSc1	Kánya
a	a	k8xC	a
ministr	ministr	k1gMnSc1	ministr
války	válka	k1gFnSc2	válka
Jenő	Jenő	k1gMnSc1	Jenő
Rátz	Rátz	k1gMnSc1	Rátz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
návštěvy	návštěva	k1gFnPc1	návštěva
byly	být	k5eAaImAgFnP	být
rozhovory	rozhovor	k1gInPc4	rozhovor
o	o	k7c4	o
dělení	dělení	k1gNnSc4	dělení
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
podílu	podíl	k1gInSc2	podíl
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
dělbě	dělba	k1gFnSc6	dělba
<g/>
.	.	kIx.	.
</s>
<s>
Maďaři	Maďar	k1gMnPc1	Maďar
se	se	k3xPyFc4	se
domáhali	domáhat	k5eAaImAgMnP	domáhat
územních	územní	k2eAgInPc2d1	územní
zisků	zisk	k1gInPc2	zisk
na	na	k7c6	na
Československu	Československo	k1gNnSc6	Československo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zpočátku	zpočátku	k6eAd1	zpočátku
neprojevovali	projevovat	k5eNaImAgMnP	projevovat
vůli	vůle	k1gFnSc4	vůle
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
němu	on	k3xPp3gInSc3	on
vojenské	vojenský	k2eAgFnPc4d1	vojenská
akce	akce	k1gFnPc4	akce
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
ovšem	ovšem	k9	ovšem
žádal	žádat	k5eAaImAgMnS	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
bylo	být	k5eAaImAgNnS	být
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
při	při	k7c6	při
postupu	postup	k1gInSc6	postup
proti	proti	k7c3	proti
Československu	Československo	k1gNnSc3	Československo
solidární	solidární	k2eAgFnPc1d1	solidární
<g/>
,	,	kIx,	,
doslova	doslova	k6eAd1	doslova
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pokud	pokud	k8xS	pokud
chcete	chtít	k5eAaImIp2nP	chtít
společné	společný	k2eAgNnSc4d1	společné
jídlo	jídlo	k1gNnSc4	jídlo
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
byste	by	kYmCp2nP	by
se	se	k3xPyFc4	se
na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
přípravě	příprava	k1gFnSc6	příprava
také	také	k9	také
podílet	podílet	k5eAaImF	podílet
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Horthy	Horth	k1gMnPc4	Horth
Hitlerovi	Hitlerův	k2eAgMnPc1d1	Hitlerův
po	po	k7c6	po
naléhání	naléhání	k1gNnSc6	naléhání
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
účast	účast	k1gFnSc4	účast
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
při	při	k7c6	při
realizování	realizování	k1gNnSc6	realizování
plánu	plán	k1gInSc2	plán
přepadu	přepad	k1gInSc2	přepad
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Fall	Fall	k1gMnSc1	Fall
Grün	Grün	k1gMnSc1	Grün
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
<g/>
,	,	kIx,	,
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
podepsaná	podepsaný	k2eAgFnSc1d1	podepsaná
o	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
umožnila	umožnit	k5eAaPmAgFnS	umožnit
zábor	zábor	k1gInSc4	zábor
hraničních	hraniční	k2eAgNnPc2d1	hraniční
území	území	k1gNnPc2	území
Československa	Československo	k1gNnSc2	Československo
bez	bez	k7c2	bez
bojového	bojový	k2eAgNnSc2d1	bojové
nasazení	nasazení	k1gNnSc2	nasazení
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
na	na	k7c6	na
základě	základ	k1gInSc6	základ
tzv.	tzv.	kA	tzv.
první	první	k4xOgFnSc2	první
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
arbitráže	arbitráž	k1gFnSc2	arbitráž
ze	z	k7c2	z
dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1938	[number]	k4	1938
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
část	část	k1gFnSc4	část
jižního	jižní	k2eAgNnSc2d1	jižní
a	a	k8xC	a
východního	východní	k2eAgNnSc2d1	východní
Slovenska	Slovensko	k1gNnSc2	Slovensko
a	a	k8xC	a
jihozápadní	jihozápadní	k2eAgFnSc1d1	jihozápadní
část	část	k1gFnSc1	část
Podkarpatské	podkarpatský	k2eAgFnSc2d1	Podkarpatská
Rusi	Rus	k1gFnSc2	Rus
-	-	kIx~	-
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1938	[number]	k4	1938
po	po	k7c6	po
změně	změna	k1gFnSc6	změna
Ústavy	ústava	k1gFnSc2	ústava
ČSR	ČSR	kA	ČSR
přejmenované	přejmenovaný	k2eAgNnSc1d1	přejmenované
na	na	k7c4	na
autonomní	autonomní	k2eAgFnSc4d1	autonomní
Karpatskou	karpatský	k2eAgFnSc4d1	Karpatská
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
žilo	žít	k5eAaImAgNnS	žít
na	na	k7c6	na
těchto	tento	k3xDgNnPc6	tento
územích	území	k1gNnPc6	území
asi	asi	k9	asi
59	[number]	k4	59
<g/>
%	%	kIx~	%
etnických	etnický	k2eAgInPc2d1	etnický
Maďarů	maďar	k1gInPc2	maďar
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Německo-maďarská	německoaďarský	k2eAgFnSc1d1	německo-maďarský
spolupráce	spolupráce	k1gFnSc1	spolupráce
vůči	vůči	k7c3	vůči
zbytku	zbytek	k1gInSc3	zbytek
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
již	již	k6eAd1	již
<g/>
)	)	kIx)	)
Česko-Slovenska	Česko-Slovensko	k1gNnPc1	Česko-Slovensko
se	se	k3xPyFc4	se
oživila	oživit	k5eAaPmAgNnP	oživit
opět	opět	k6eAd1	opět
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Hitler	Hitler	k1gMnSc1	Hitler
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
Česko-Slovensko	Česko-Slovensko	k1gNnSc4	Česko-Slovensko
rozdělit	rozdělit	k5eAaPmF	rozdělit
a	a	k8xC	a
následně	následně	k6eAd1	následně
obsadit	obsadit	k5eAaPmF	obsadit
zbytek	zbytek	k1gInSc4	zbytek
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
ze	z	k7c2	z
Slovenska	Slovensko	k1gNnSc2	Slovensko
vytvořit	vytvořit	k5eAaPmF	vytvořit
satelitní	satelitní	k2eAgInSc1d1	satelitní
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
Horthyho	Horthy	k1gMnSc4	Horthy
k	k	k7c3	k
účasti	účast	k1gFnSc3	účast
při	při	k7c6	při
další	další	k2eAgFnSc6d1	další
dělbě	dělba	k1gFnSc6	dělba
Česko-Slovenska	Česko-Slovensko	k1gNnSc2	Česko-Slovensko
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Horthy	Hortha	k1gFnSc2	Hortha
to	ten	k3xDgNnSc4	ten
přivítal	přivítat	k5eAaPmAgMnS	přivítat
a	a	k8xC	a
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
z	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
Hitlerovi	Hitlerův	k2eAgMnPc1d1	Hitlerův
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
zkráceně	zkráceně	k6eAd1	zkráceně
s	s	k7c7	s
přeloženými	přeložený	k2eAgFnPc7d1	přeložená
citacemi	citace	k1gFnPc7	citace
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nemůžu	Nemůžu	k?	Nemůžu
ani	ani	k8xC	ani
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jsem	být	k5eAaImIp1nS	být
šťastný	šťastný	k2eAgMnSc1d1	šťastný
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
naši	náš	k3xOp1gMnPc1	náš
branci	branec	k1gMnPc1	branec
slouží	sloužit	k5eAaImIp3nP	sloužit
pouze	pouze	k6eAd1	pouze
pět	pět	k4xCc4	pět
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
do	do	k7c2	do
Česko-Slovenska	Česko-Slovensko	k1gNnSc2	Česko-Slovensko
vpadnou	vpadnout	k5eAaPmIp3nP	vpadnout
s	s	k7c7	s
dychtivým	dychtivý	k2eAgNnSc7d1	dychtivé
nadšením	nadšení	k1gNnSc7	nadšení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
již	již	k6eAd1	již
dostali	dostat	k5eAaPmAgMnP	dostat
instrukce	instrukce	k1gFnPc4	instrukce
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
mají	mít	k5eAaImIp3nP	mít
dělat	dělat	k5eAaImF	dělat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
šestnáctého	šestnáctý	k4xOgNnSc2	šestnáctý
tohoto	tento	k3xDgInSc2	tento
měsíce	měsíc	k1gInSc2	měsíc
způsobí	způsobit	k5eAaPmIp3nS	způsobit
pohraniční	pohraniční	k2eAgInSc4d1	pohraniční
incident	incident	k1gInSc4	incident
<g/>
,	,	kIx,	,
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
bude	být	k5eAaImBp3nS	být
následovat	následovat	k5eAaImF	následovat
velký	velký	k2eAgInSc4d1	velký
úder	úder	k1gInSc4	úder
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
dopis	dopis	k1gInSc4	dopis
Hitlerovi	Hitler	k1gMnSc3	Hitler
končí	končit	k5eAaImIp3nP	končit
Horthy	Hortha	k1gFnPc1	Hortha
těmito	tento	k3xDgFnPc7	tento
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nikdy	nikdy	k6eAd1	nikdy
nezapomenu	zapomenout	k5eNaPmIp1nS	zapomenout
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
důkaz	důkaz	k1gInSc4	důkaz
přátelství	přátelství	k1gNnSc2	přátelství
a	a	k8xC	a
Vaše	váš	k3xOp2gFnSc1	váš
Excelence	Excelence	k1gFnSc1	Excelence
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
spolehnout	spolehnout	k5eAaPmF	spolehnout
na	na	k7c4	na
moji	můj	k3xOp1gFnSc4	můj
neotřesitelnou	otřesitelný	k2eNgFnSc4d1	neotřesitelná
věčnou	věčný	k2eAgFnSc4d1	věčná
vděčnost	vděčnost	k1gFnSc4	vděčnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Horthyho	Horthy	k1gMnSc2	Horthy
"	"	kIx"	"
<g/>
velký	velký	k2eAgInSc1d1	velký
úder	úder	k1gInSc1	úder
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
neuskutečnil	uskutečnit	k5eNaPmAgInS	uskutečnit
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
zbytek	zbytek	k1gInSc1	zbytek
Česko-Slovenska	Česko-Slovensko	k1gNnSc2	Česko-Slovensko
Hitler	Hitler	k1gMnSc1	Hitler
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
bez	bez	k7c2	bez
bojového	bojový	k2eAgNnSc2d1	bojové
nasazení	nasazení	k1gNnSc2	nasazení
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
nebylo	být	k5eNaImAgNnS	být
tedy	tedy	k9	tedy
zapotřebí	zapotřebí	k6eAd1	zapotřebí
pomoci	pomoct	k5eAaPmF	pomoct
Horthyho	Horthy	k1gMnSc4	Horthy
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
Slovenského	slovenský	k2eAgInSc2d1	slovenský
státu	stát	k1gInSc2	stát
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
(	(	kIx(	(
<g/>
fakticky	fakticky	k6eAd1	fakticky
vynuceném	vynucený	k2eAgMnSc6d1	vynucený
Hitlerem	Hitler	k1gMnSc7	Hitler
<g/>
)	)	kIx)	)
a	a	k8xC	a
obsazení	obsazení	k1gNnSc1	obsazení
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
na	na	k7c4	na
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
Horthy	Horth	k1gInPc4	Horth
obsadil	obsadit	k5eAaPmAgInS	obsadit
a	a	k8xC	a
anektoval	anektovat	k5eAaBmAgInS	anektovat
zbytek	zbytek	k1gInSc4	zbytek
Karpatské	karpatský	k2eAgFnSc2d1	Karpatská
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Maďarské	maďarský	k2eAgNnSc1d1	Maďarské
vojsko	vojsko	k1gNnSc1	vojsko
navíc	navíc	k6eAd1	navíc
bez	bez	k7c2	bez
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
důvodu	důvod	k1gInSc2	důvod
vniklo	vniknout	k5eAaPmAgNnS	vniknout
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
německo-slovenská	německolovenský	k2eAgFnSc1d1	německo-slovenský
Ochranná	ochranný	k2eAgFnSc1d1	ochranná
smlouva	smlouva	k1gFnSc1	smlouva
z	z	k7c2	z
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
Německá	německý	k2eAgFnSc1d1	německá
říše	říš	k1gFnSc2	říš
"	"	kIx"	"
<g/>
přebírá	přebírat	k5eAaImIp3nS	přebírat
ochranu	ochrana	k1gFnSc4	ochrana
nad	nad	k7c7	nad
politickou	politický	k2eAgFnSc7d1	politická
nezávislostí	nezávislost	k1gFnSc7	nezávislost
Slovenského	slovenský	k2eAgInSc2d1	slovenský
státu	stát	k1gInSc2	stát
a	a	k8xC	a
nad	nad	k7c7	nad
nedotknutelností	nedotknutelnost	k1gFnSc7	nedotknutelnost
jeho	jeho	k3xOp3gNnSc2	jeho
území	území	k1gNnSc2	území
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
na	na	k7c4	na
Slovensko	Slovensko	k1gNnSc4	Slovensko
do	do	k7c2	do
okresů	okres	k1gInPc2	okres
Sobrance	Sobrance	k1gFnSc2	Sobrance
a	a	k8xC	a
Michalovce	Michalovce	k1gInPc1	Michalovce
(	(	kIx(	(
<g/>
části	část	k1gFnPc1	část
východního	východní	k2eAgNnSc2d1	východní
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
)	)	kIx)	)
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
je	on	k3xPp3gFnPc4	on
obsazovat	obsazovat	k5eAaImF	obsazovat
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
maďarské	maďarský	k2eAgNnSc4d1	Maďarské
letectvo	letectvo	k1gNnSc4	letectvo
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
bombardovalo	bombardovat	k5eAaImAgNnS	bombardovat
hluboko	hluboko	k6eAd1	hluboko
v	v	k7c6	v
slovenském	slovenský	k2eAgNnSc6d1	slovenské
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
letiště	letiště	k1gNnSc2	letiště
u	u	k7c2	u
Spišské	spišský	k2eAgFnSc2d1	Spišská
Nové	Nové	k2eAgFnSc2d1	Nové
Vsi	ves	k1gFnSc2	ves
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
maďarské	maďarský	k2eAgInPc1d1	maďarský
útoky	útok	k1gInPc1	útok
si	se	k3xPyFc3	se
vyžádaly	vyžádat	k5eAaPmAgInP	vyžádat
mnoho	mnoho	k4c1	mnoho
obětí	oběť	k1gFnPc2	oběť
na	na	k7c6	na
životech	život	k1gInPc6	život
i	i	k9	i
škody	škoda	k1gFnPc4	škoda
na	na	k7c6	na
majetku	majetek	k1gInSc6	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Incident	incident	k1gInSc1	incident
skončil	skončit	k5eAaPmAgInS	skončit
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
příměřím	příměří	k1gNnSc7	příměří
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
slovensko-maďarskou	slovenskoaďarský	k2eAgFnSc7d1	slovensko-maďarská
dohodou	dohoda	k1gFnSc7	dohoda
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
Slovensko	Slovensko	k1gNnSc4	Slovensko
celý	celý	k2eAgInSc1d1	celý
Sobranecký	Sobranecký	k2eAgInSc1d1	Sobranecký
okres	okres	k1gInSc1	okres
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
přitom	přitom	k6eAd1	přitom
Slovensku	Slovensko	k1gNnSc3	Slovensko
během	během	k7c2	během
incidentu	incident	k1gInSc2	incident
s	s	k7c7	s
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
neposkytl	poskytnout	k5eNaPmAgInS	poskytnout
diplomatickou	diplomatický	k2eAgFnSc4d1	diplomatická
ani	ani	k8xC	ani
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
se	se	k3xPyFc4	se
Maďarské	maďarský	k2eAgNnSc1d1	Maďarské
království	království	k1gNnSc1	království
chystalo	chystat	k5eAaImAgNnS	chystat
vojensky	vojensky	k6eAd1	vojensky
napadnout	napadnout	k5eAaPmF	napadnout
Rumunsko	Rumunsko	k1gNnSc4	Rumunsko
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
zpátky	zpátky	k6eAd1	zpátky
území	území	k1gNnSc4	území
Sedmihradska	Sedmihradsko	k1gNnSc2	Sedmihradsko
(	(	kIx(	(
<g/>
Transylvánie	Transylvánie	k1gFnSc1	Transylvánie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
před	před	k7c7	před
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
součástí	součást	k1gFnSc7	součást
Uherska	Uhersko	k1gNnSc2	Uhersko
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
zachoval	zachovat	k5eAaPmAgMnS	zachovat
jako	jako	k8xS	jako
blízký	blízký	k2eAgMnSc1d1	blízký
spojenec	spojenec	k1gMnSc1	spojenec
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
Maďarského	maďarský	k2eAgNnSc2d1	Maďarské
království	království	k1gNnSc2	království
(	(	kIx(	(
<g/>
a	a	k8xC	a
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
smyslu	smysl	k1gInSc6	smysl
jako	jako	k9	jako
patron	patron	k1gMnSc1	patron
a	a	k8xC	a
ochránce	ochránce	k1gMnSc1	ochránce
Horthyho	Horthy	k1gMnSc2	Horthy
<g/>
)	)	kIx)	)
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
polovinu	polovina	k1gFnSc4	polovina
Sedmihradska	Sedmihradsko	k1gNnSc2	Sedmihradsko
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
maďarská	maďarský	k2eAgFnSc1d1	maďarská
armáda	armáda	k1gFnSc1	armáda
byla	být	k5eAaImAgFnS	být
nucena	nutit	k5eAaImNgFnS	nutit
vystřelit	vystřelit	k5eAaPmF	vystřelit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1941	[number]	k4	1941
se	se	k3xPyFc4	se
Maďarské	maďarský	k2eAgNnSc1d1	Maďarské
království	království	k1gNnSc1	království
stalo	stát	k5eAaPmAgNnS	stát
plným	plný	k2eAgMnSc7d1	plný
členem	člen	k1gMnSc7	člen
Osy	osa	k1gFnSc2	osa
a	a	k8xC	a
podílelo	podílet	k5eAaImAgNnS	podílet
se	se	k3xPyFc4	se
vojensky	vojensky	k6eAd1	vojensky
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
Bulharskem	Bulharsko	k1gNnSc7	Bulharsko
na	na	k7c6	na
vpádu	vpád	k1gInSc6	vpád
do	do	k7c2	do
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
(	(	kIx(	(
<g/>
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
spáchal	spáchat	k5eAaPmAgInS	spáchat
maďarský	maďarský	k2eAgMnSc1d1	maďarský
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Pál	Pál	k1gFnSc1	Pál
Teleki	Telek	k1gFnSc2	Telek
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
se	se	k3xPyFc4	se
Horthy	Hortha	k1gFnSc2	Hortha
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
vést	vést	k5eAaImF	vést
tajná	tajný	k2eAgNnPc4d1	tajné
jednání	jednání	k1gNnPc4	jednání
se	s	k7c7	s
spojenci	spojenec	k1gMnPc7	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Maďarská	maďarský	k2eAgFnSc1d1	maďarská
delegace	delegace	k1gFnSc1	delegace
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vedl	vést	k5eAaImAgMnS	vést
Albert	Albert	k1gMnSc1	Albert
Szent-Györgyi	Szent-György	k1gFnSc2	Szent-György
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
několikrát	několikrát	k6eAd1	několikrát
setkala	setkat	k5eAaPmAgNnP	setkat
s	s	k7c7	s
britskými	britský	k2eAgMnPc7d1	britský
diplomaty	diplomat	k1gMnPc7	diplomat
v	v	k7c6	v
Istanbulu	Istanbul	k1gInSc6	Istanbul
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
těchto	tento	k3xDgNnPc6	tento
jednáních	jednání	k1gNnPc6	jednání
věděla	vědět	k5eAaImAgFnS	vědět
německá	německý	k2eAgFnSc1d1	německá
tajná	tajný	k2eAgFnSc1d1	tajná
služba	služba	k1gFnSc1	služba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
spojenci	spojenec	k1gMnPc1	spojenec
je	on	k3xPp3gFnPc4	on
využili	využít	k5eAaPmAgMnP	využít
spíše	spíše	k9	spíše
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
odvedli	odvést	k5eAaPmAgMnP	odvést
pozornost	pozornost	k1gFnSc4	pozornost
nacistů	nacista	k1gMnPc2	nacista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
na	na	k7c4	na
přímý	přímý	k2eAgInSc4d1	přímý
rozkaz	rozkaz	k1gInSc4	rozkaz
několika	několik	k4yIc2	několik
maďarských	maďarský	k2eAgMnPc2d1	maďarský
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
regionu	region	k1gInSc6	region
Bačka	Baček	k1gInSc2	Baček
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Vojvodiny	Vojvodina	k1gFnSc2	Vojvodina
<g/>
,	,	kIx,	,
zavražděn	zavražděn	k2eAgInSc4d1	zavražděn
velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
srbských	srbský	k2eAgMnPc2d1	srbský
a	a	k8xC	a
židovských	židovský	k2eAgMnPc2d1	židovský
civilistů	civilista	k1gMnPc2	civilista
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
těla	tělo	k1gNnSc2	tělo
posléze	posléze	k6eAd1	posléze
naházena	naházet	k5eAaBmNgFnS	naházet
do	do	k7c2	do
Dunaje	Dunaj	k1gInSc2	Dunaj
a	a	k8xC	a
Tisy	Tisa	k1gFnSc2	Tisa
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Horthy	Hortha	k1gMnSc2	Hortha
nařídil	nařídit	k5eAaPmAgMnS	nařídit
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
tohoto	tento	k3xDgInSc2	tento
zločinu	zločin	k1gInSc2	zločin
<g/>
,	,	kIx,	,
důstojníci	důstojník	k1gMnPc1	důstojník
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
do	do	k7c2	do
nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
do	do	k7c2	do
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Německo	Německo	k1gNnSc1	Německo
Maďarské	maďarský	k2eAgNnSc1d1	Maďarské
království	království	k1gNnSc1	království
okupovalo	okupovat	k5eAaBmAgNnS	okupovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byli	být	k5eAaImAgMnP	být
všichni	všechen	k3xTgMnPc1	všechen
popraveni	popraven	k2eAgMnPc1d1	popraven
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
Německo	Německo	k1gNnSc1	Německo
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
spojenci	spojenec	k1gMnPc7	spojenec
včetně	včetně	k7c2	včetně
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
v	v	k7c6	v
hluboké	hluboký	k2eAgFnSc6d1	hluboká
defenzívě	defenzíva	k1gFnSc6	defenzíva
a	a	k8xC	a
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
začala	začít	k5eAaPmAgFnS	začít
přímo	přímo	k6eAd1	přímo
ohrožovat	ohrožovat	k5eAaImF	ohrožovat
východní	východní	k2eAgFnSc4d1	východní
hranici	hranice	k1gFnSc4	hranice
Maďarského	maďarský	k2eAgNnSc2d1	Maďarské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Horthy	Hortha	k1gFnPc4	Hortha
byl	být	k5eAaImAgInS	být
pozván	pozvat	k5eAaPmNgMnS	pozvat
na	na	k7c6	na
vyjednávání	vyjednávání	k1gNnSc6	vyjednávání
do	do	k7c2	do
Klessheimu	Klessheim	k1gInSc2	Klessheim
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
Němci	Němec	k1gMnSc3	Němec
dočasně	dočasně	k6eAd1	dočasně
zajat	zajmout	k5eAaPmNgMnS	zajmout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nemohl	moct	k5eNaImAgInS	moct
vydat	vydat	k5eAaPmF	vydat
rozkaz	rozkaz	k1gInSc1	rozkaz
k	k	k7c3	k
odporu	odpor	k1gInSc3	odpor
<g/>
.	.	kIx.	.
</s>
<s>
Wehrmacht	wehrmacht	k1gFnSc4	wehrmacht
posléze	posléze	k6eAd1	posléze
obsadil	obsadit	k5eAaPmAgMnS	obsadit
Maďarsko	Maďarsko	k1gNnSc4	Maďarsko
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Operace	operace	k1gFnSc1	operace
Margarethe	Margarethe	k1gFnSc1	Margarethe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
ustavena	ustaven	k2eAgFnSc1d1	ustavena
loutková	loutkový	k2eAgFnSc1d1	loutková
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ochotně	ochotně	k6eAd1	ochotně
pomáhala	pomáhat	k5eAaImAgFnS	pomáhat
nacistům	nacista	k1gMnPc3	nacista
v	v	k7c6	v
deportaci	deportace	k1gFnSc6	deportace
Židů	Žid	k1gMnPc2	Žid
z	z	k7c2	z
Maďarského	maďarský	k2eAgNnSc2d1	Maďarské
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Horthy	Horth	k1gInPc1	Horth
tyto	tento	k3xDgFnPc4	tento
deportace	deportace	k1gFnPc4	deportace
po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
<g/>
,	,	kIx,	,
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
světových	světový	k2eAgFnPc2d1	světová
osobností	osobnost	k1gFnPc2	osobnost
reagující	reagující	k2eAgMnSc1d1	reagující
na	na	k7c4	na
svědectví	svědectví	k1gNnSc4	svědectví
dvou	dva	k4xCgMnPc2	dva
uprchlých	uprchlý	k2eAgMnPc2d1	uprchlý
vězňů	vězeň	k1gMnPc2	vězeň
<g/>
,	,	kIx,	,
počátkem	počátkem	k7c2	počátkem
července	červenec	k1gInSc2	červenec
1944	[number]	k4	1944
zastavil	zastavit	k5eAaPmAgMnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
Horthy	Hortha	k1gFnSc2	Hortha
údajně	údajně	k6eAd1	údajně
stále	stále	k6eAd1	stále
věřil	věřit	k5eAaImAgInS	věřit
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Židé	Žid	k1gMnPc1	Žid
jsou	být	k5eAaImIp3nP	být
posíláni	posílat	k5eAaImNgMnP	posílat
do	do	k7c2	do
táborů	tábor	k1gInPc2	tábor
na	na	k7c4	na
nucenou	nucený	k2eAgFnSc4d1	nucená
práci	práce	k1gFnSc4	práce
a	a	k8xC	a
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
skončení	skončení	k1gNnSc6	skončení
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
přešlo	přejít	k5eAaPmAgNnS	přejít
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
,	,	kIx,	,
odvolal	odvolat	k5eAaPmAgMnS	odvolat
Horthy	Horth	k1gMnPc4	Horth
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
sestavovat	sestavovat	k5eAaImF	sestavovat
novou	nový	k2eAgFnSc4d1	nová
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Gézy	géz	k1gMnPc4	géz
Lakatose	Lakatosa	k1gFnSc6	Lakatosa
a	a	k8xC	a
současně	současně	k6eAd1	současně
zahájil	zahájit	k5eAaPmAgMnS	zahájit
jednání	jednání	k1gNnSc4	jednání
se	s	k7c7	s
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Nacisté	nacista	k1gMnPc1	nacista
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
opět	opět	k6eAd1	opět
zakročili	zakročit	k5eAaPmAgMnP	zakročit
a	a	k8xC	a
vyslali	vyslat	k5eAaPmAgMnP	vyslat
do	do	k7c2	do
Budapešti	Budapešť	k1gFnSc2	Budapešť
nechvalně	chvalně	k6eNd1	chvalně
proslavené	proslavený	k2eAgInPc1d1	proslavený
SS	SS	kA	SS
komando	komando	k1gNnSc1	komando
vedené	vedený	k2eAgNnSc1d1	vedené
Ottou	Otta	k1gMnSc7	Otta
Skorzenym	Skorzenym	k1gInSc1	Skorzenym
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
Horthy	Horth	k1gInPc4	Horth
předem	předem	k6eAd1	předem
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
jako	jako	k9	jako
konec	konec	k1gInSc1	konec
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
uneslo	unést	k5eAaPmAgNnS	unést
toto	tento	k3xDgNnSc1	tento
komando	komando	k1gNnSc1	komando
jeho	on	k3xPp3gMnSc2	on
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
Horthy	Hortha	k1gMnSc2	Hortha
byl	být	k5eAaImAgMnS	být
donucen	donutit	k5eAaPmNgMnS	donutit
vzít	vzít	k5eAaPmF	vzít
své	svůj	k3xOyFgNnSc4	svůj
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
zpět	zpět	k6eAd1	zpět
a	a	k8xC	a
abdikovat	abdikovat	k5eAaBmF	abdikovat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
maďarského	maďarský	k2eAgMnSc2d1	maďarský
fašisty	fašista	k1gMnSc2	fašista
Ference	Ferenc	k1gMnSc2	Ferenc
Szálasiho	Szálasi	k1gMnSc2	Szálasi
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1944	[number]	k4	1944
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Maďarský	maďarský	k2eAgInSc1d1	maďarský
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
nacistický	nacistický	k2eAgInSc1d1	nacistický
loutkový	loutkový	k2eAgInSc1d1	loutkový
stát	stát	k1gInSc1	stát
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
války	válka	k1gFnSc2	válka
strávil	strávit	k5eAaPmAgInS	strávit
Horthy	Horth	k1gMnPc4	Horth
v	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
vězení	vězení	k1gNnSc6	vězení
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
a	a	k8xC	a
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1945	[number]	k4	1945
byl	být	k5eAaImAgInS	být
zatčen	zatknout	k5eAaPmNgMnS	zatknout
příslušníky	příslušník	k1gMnPc7	příslušník
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Horthy	Hortha	k1gFnPc4	Hortha
a	a	k8xC	a
holocaust	holocaust	k1gInSc4	holocaust
===	===	k?	===
</s>
</p>
<p>
<s>
Počínaje	počínaje	k7c7	počínaje
rokem	rok	k1gInSc7	rok
1938	[number]	k4	1938
přijalo	přijmout	k5eAaPmAgNnS	přijmout
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
řadu	řad	k1gInSc2	řad
protižidovských	protižidovský	k2eAgNnPc2d1	protižidovské
nařízení	nařízení	k1gNnPc2	nařízení
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
vydané	vydaný	k2eAgFnPc4d1	vydaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
omezovalo	omezovat	k5eAaImAgNnS	omezovat
podíl	podíl	k1gInSc4	podíl
pracujících	pracující	k2eAgMnPc2d1	pracující
Židů	Žid	k1gMnPc2	Žid
(	(	kIx(	(
<g/>
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
definováni	definovat	k5eAaBmNgMnP	definovat
věroučně	věroučně	k6eAd1	věroučně
-	-	kIx~	-
na	na	k7c6	na
základě	základ	k1gInSc6	základ
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
)	)	kIx)	)
v	v	k7c6	v
různých	různý	k2eAgNnPc6d1	různé
zaměstnáních	zaměstnání	k1gNnPc6	zaměstnání
<g/>
,	,	kIx,	,
ve	v	k7c6	v
veřejné	veřejný	k2eAgFnSc6d1	veřejná
správě	správa	k1gFnSc6	správa
a	a	k8xC	a
v	v	k7c6	v
obchodě	obchod	k1gInSc6	obchod
na	na	k7c4	na
dvacet	dvacet	k4xCc4	dvacet
procent	procento	k1gNnPc2	procento
a	a	k8xC	a
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
pět	pět	k4xCc4	pět
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
250	[number]	k4	250
000	[number]	k4	000
maďarských	maďarský	k2eAgMnPc2d1	maďarský
Židů	Žid	k1gMnPc2	Žid
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
Třetí	třetí	k4xOgInSc4	třetí
židovský	židovský	k2eAgInSc4d1	židovský
zákon	zákon	k1gInSc4	zákon
zakazoval	zakazovat	k5eAaImAgInS	zakazovat
smíšené	smíšený	k2eAgInPc4d1	smíšený
sňatky	sňatek	k1gInPc4	sňatek
a	a	k8xC	a
definoval	definovat	k5eAaBmAgInS	definovat
Židy	Žid	k1gMnPc4	Žid
na	na	k7c6	na
rasovém	rasový	k2eAgInSc6d1	rasový
základě	základ	k1gInSc6	základ
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
čemuž	což	k3yQnSc3	což
se	se	k3xPyFc4	se
ohradila	ohradit	k5eAaPmAgFnS	ohradit
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
masakr	masakr	k1gInSc1	masakr
maďarských	maďarský	k2eAgMnPc2d1	maďarský
Židů	Žid	k1gMnPc2	Žid
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
20	[number]	k4	20
000	[number]	k4	000
Židů	Žid	k1gMnPc2	Žid
z	z	k7c2	z
Podkarpatské	podkarpatský	k2eAgFnSc2d1	Podkarpatská
Rusi	Rus	k1gFnSc2	Rus
vyhnáno	vyhnán	k2eAgNnSc1d1	vyhnáno
na	na	k7c4	na
sovětské	sovětský	k2eAgNnSc4d1	sovětské
území	území	k1gNnSc4	území
okupované	okupovaný	k2eAgNnSc4d1	okupované
Němci	Němec	k1gMnPc7	Němec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
následně	následně	k6eAd1	následně
povražděni	povražděn	k2eAgMnPc1d1	povražděn
jednotkami	jednotka	k1gFnPc7	jednotka
SS	SS	kA	SS
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ultrapravicové	ultrapravicový	k2eAgFnPc1d1	ultrapravicová
maďarské	maďarský	k2eAgFnPc1d1	maďarská
vlády	vláda	k1gFnPc1	vláda
za	za	k7c4	za
Horthyho	Horthy	k1gMnSc4	Horthy
regentství	regentství	k1gNnSc2	regentství
a	a	k8xC	a
především	především	k9	především
po	po	k7c6	po
německé	německý	k2eAgFnSc6d1	německá
okupaci	okupace	k1gFnSc6	okupace
ustavená	ustavený	k2eAgFnSc1d1	ustavená
loutková	loutkový	k2eAgFnSc1d1	loutková
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vedl	vést	k5eAaImAgInS	vést
Döme	Döme	k1gInSc1	Döme
Sztójay	Sztójaa	k1gFnSc2	Sztójaa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
podílely	podílet	k5eAaImAgInP	podílet
na	na	k7c6	na
holocaustu	holocaust	k1gInSc6	holocaust
<g/>
.	.	kIx.	.
</s>
<s>
Hromadné	hromadný	k2eAgFnPc1d1	hromadná
deportace	deportace	k1gFnPc1	deportace
maďarských	maďarský	k2eAgMnPc2d1	maďarský
Židů	Žid	k1gMnPc2	Žid
do	do	k7c2	do
Osvětimi	Osvětim	k1gFnSc2	Osvětim
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
asi	asi	k9	asi
12	[number]	k4	12
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
denně	denně	k6eAd1	denně
začaly	začít	k5eAaPmAgFnP	začít
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1944	[number]	k4	1944
a	a	k8xC	a
skončily	skončit	k5eAaPmAgFnP	skončit
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	on	k3xPp3gInPc4	on
Horthy	Horth	k1gInPc4	Horth
zastavil	zastavit	k5eAaPmAgInS	zastavit
po	po	k7c6	po
osobních	osobní	k2eAgFnPc6d1	osobní
intervencích	intervence	k1gFnPc6	intervence
světových	světový	k2eAgMnPc2d1	světový
čelných	čelný	k2eAgMnPc2d1	čelný
představitelů	představitel	k1gMnPc2	představitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dva	dva	k4xCgMnPc1	dva
slovenští	slovenský	k2eAgMnPc1d1	slovenský
Židé	Žid	k1gMnPc1	Žid
(	(	kIx(	(
<g/>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Vrba	Vrba	k1gMnSc1	Vrba
a	a	k8xC	a
Alfréd	Alfréd	k1gMnSc1	Alfréd
Wetzler	Wetzler	k1gMnSc1	Wetzler
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
utéct	utéct	k5eAaPmF	utéct
z	z	k7c2	z
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
v	v	k7c6	v
Osvětimi	Osvětim	k1gFnSc6	Osvětim
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
válku	válka	k1gFnSc4	válka
podařilo	podařit	k5eAaPmAgNnS	podařit
pouze	pouze	k6eAd1	pouze
pěti	pět	k4xCc3	pět
lidem	člověk	k1gMnPc3	člověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
podali	podat	k5eAaPmAgMnP	podat
koncem	koncem	k7c2	koncem
dubna	duben	k1gInSc2	duben
1944	[number]	k4	1944
slovenským	slovenský	k2eAgInPc3d1	slovenský
úřadům	úřad	k1gInPc3	úřad
detailní	detailní	k2eAgMnSc1d1	detailní
svědectví	svědectví	k1gNnSc4	svědectví
o	o	k7c6	o
hromadných	hromadný	k2eAgFnPc6d1	hromadná
vraždách	vražda	k1gFnPc6	vražda
a	a	k8xC	a
genocidě	genocida	k1gFnSc3	genocida
probíhající	probíhající	k2eAgFnSc3d1	probíhající
v	v	k7c6	v
německých	německý	k2eAgInPc6d1	německý
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zpráva	zpráva	k1gFnSc1	zpráva
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
po	po	k7c6	po
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Horthy	Horth	k1gMnPc4	Horth
obdržel	obdržet	k5eAaPmAgInS	obdržet
překlad	překlad	k1gInSc1	překlad
jejich	jejich	k3xOp3gFnSc2	jejich
zprávy	zpráva	k1gFnSc2	zpráva
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
<g/>
.	.	kIx.	.
</s>
<s>
Detaily	detail	k1gInPc1	detail
zprávy	zpráva	k1gFnSc2	zpráva
byly	být	k5eAaImAgFnP	být
zveřejněny	zveřejnit	k5eAaPmNgFnP	zveřejnit
rozhlasovou	rozhlasový	k2eAgFnSc7d1	rozhlasová
stanicí	stanice	k1gFnSc7	stanice
BBC	BBC	kA	BBC
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
a	a	k8xC	a
The	The	k1gFnSc2	The
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Times	Timesa	k1gFnPc2	Timesa
je	být	k5eAaImIp3nS	být
otiskl	otisknout	k5eAaPmAgMnS	otisknout
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgFnPc1d1	přední
světové	světový	k2eAgFnPc1d1	světová
osobnosti	osobnost	k1gFnPc1	osobnost
včetně	včetně	k7c2	včetně
papeže	papež	k1gMnSc4	papež
Pia	Pius	k1gMnSc2	Pius
XII	XII	kA	XII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prezidenta	prezident	k1gMnSc4	prezident
Franklin	Franklina	k1gFnPc2	Franklina
Delano	Delana	k1gFnSc5	Delana
Roosevelta	Roosevelt	k1gMnSc2	Roosevelt
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
a	a	k8xC	a
švédského	švédský	k2eAgMnSc2d1	švédský
krále	král	k1gMnSc2	král
Gustava	Gustav	k1gMnSc2	Gustav
V.	V.	kA	V.
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
žádaly	žádat	k5eAaImAgInP	žádat
Horthyho	Horthy	k1gMnSc4	Horthy
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
deportace	deportace	k1gFnPc4	deportace
zastavil	zastavit	k5eAaPmAgMnS	zastavit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
poslal	poslat	k5eAaPmAgMnS	poslat
člen	člen	k1gMnSc1	člen
Židovského	židovský	k2eAgInSc2d1	židovský
výboru	výbor	k1gInSc2	výbor
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
telegram	telegram	k1gInSc1	telegram
žádající	žádající	k2eAgInSc1d1	žádající
po	po	k7c6	po
spojencích	spojenec	k1gMnPc6	spojenec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byli	být	k5eAaImAgMnP	být
členové	člen	k1gMnPc1	člen
maďarské	maďarský	k2eAgFnSc2d1	maďarská
vlády	vláda	k1gFnSc2	vláda
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
osobně	osobně	k6eAd1	osobně
odpovědné	odpovědný	k2eAgNnSc4d1	odpovědné
za	za	k7c4	za
masové	masový	k2eAgFnPc4d1	masová
vraždy	vražda	k1gFnPc4	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
telegram	telegram	k1gInSc1	telegram
byl	být	k5eAaImAgInS	být
Maďarskem	Maďarsko	k1gNnSc7	Maďarsko
zachycen	zachytit	k5eAaPmNgMnS	zachytit
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Döme	Döme	k1gNnSc2	Döme
Sztójay	Sztójaa	k1gFnSc2	Sztójaa
ho	on	k3xPp3gMnSc4	on
předal	předat	k5eAaPmAgInS	předat
Horthymu	Horthymum	k1gNnSc3	Horthymum
<g/>
.	.	kIx.	.
</s>
<s>
Hromadné	hromadný	k2eAgFnPc1d1	hromadná
deportace	deportace	k1gFnPc1	deportace
maďarských	maďarský	k2eAgMnPc2d1	maďarský
Židů	Žid	k1gMnPc2	Žid
byly	být	k5eAaImAgFnP	být
následně	následně	k6eAd1	následně
ukončeny	ukončen	k2eAgInPc1d1	ukončen
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
z	z	k7c2	z
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
do	do	k7c2	do
Osvětimi	Osvětim	k1gFnSc2	Osvětim
deportováno	deportovat	k5eAaBmNgNnS	deportovat
437	[number]	k4	437
000	[number]	k4	000
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
nevrátila	vrátit	k5eNaPmAgFnS	vrátit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Horthy	Horth	k1gInPc1	Horth
za	za	k7c2	za
těchto	tento	k3xDgFnPc2	tento
okolností	okolnost	k1gFnPc2	okolnost
udělal	udělat	k5eAaPmAgInS	udělat
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
co	co	k3yRnSc4	co
mohli	moct	k5eAaImAgMnP	moct
udělat	udělat	k5eAaPmF	udělat
představitelé	představitel	k1gMnPc1	představitel
spojenců	spojenec	k1gMnPc2	spojenec
nebo	nebo	k8xC	nebo
západní	západní	k2eAgNnPc4d1	západní
média	médium	k1gNnPc4	médium
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
záchraně	záchrana	k1gFnSc6	záchrana
200	[number]	k4	200
000	[number]	k4	000
Židů	Žid	k1gMnPc2	Žid
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tam	tam	k6eAd1	tam
přečkali	přečkat	k5eAaPmAgMnP	přečkat
válku	válka	k1gFnSc4	válka
až	až	k9	až
do	do	k7c2	do
osvobození	osvobození	k1gNnSc2	osvobození
Rudou	ruda	k1gFnSc7	ruda
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
svůj	svůj	k3xOyFgInSc4	svůj
velký	velký	k2eAgInSc4d1	velký
podíl	podíl	k1gInSc4	podíl
právě	právě	k6eAd1	právě
Horthyho	Horthy	k1gMnSc2	Horthy
aktivní	aktivní	k2eAgInSc4d1	aktivní
odpor	odpor	k1gInSc4	odpor
a	a	k8xC	a
vzdor	vzdor	k1gInSc4	vzdor
vůči	vůči	k7c3	vůči
německým	německý	k2eAgInPc3d1	německý
požadavkům	požadavek	k1gInPc3	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Minimálně	minimálně	k6eAd1	minimálně
stejný	stejný	k2eAgInSc4d1	stejný
podíl	podíl	k1gInSc4	podíl
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
maďarská	maďarský	k2eAgFnSc1d1	maďarská
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
Židům	Žid	k1gMnPc3	Žid
vydávala	vydávat	k5eAaPmAgFnS	vydávat
falešné	falešný	k2eAgFnPc4d1	falešná
křestní	křestní	k2eAgFnPc4d1	křestní
listy	lista	k1gFnPc4	lista
a	a	k8xC	a
občanské	občanský	k2eAgInPc4d1	občanský
průkazy	průkaz	k1gInPc4	průkaz
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
zachránit	zachránit	k5eAaPmF	zachránit
je	on	k3xPp3gInPc4	on
před	před	k7c7	před
deportacemi	deportace	k1gFnPc7	deportace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Horthy	Hortha	k1gFnSc2	Hortha
nuceně	nuceně	k6eAd1	nuceně
abdikoval	abdikovat	k5eAaBmAgInS	abdikovat
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
nacisty	nacista	k1gMnPc4	nacista
od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
držen	držet	k5eAaImNgMnS	držet
v	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
vězení	vězení	k1gNnSc6	vězení
<g/>
,	,	kIx,	,
maďarská	maďarský	k2eAgFnSc1d1	maďarská
kooperace	kooperace	k1gFnSc1	kooperace
na	na	k7c6	na
deportacích	deportace	k1gFnPc6	deportace
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
825	[number]	k4	825
000	[number]	k4	000
Židů	Žid	k1gMnPc2	Žid
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
se	se	k3xPyFc4	se
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
dožilo	dožít	k5eAaPmAgNnS	dožít
pouze	pouze	k6eAd1	pouze
260	[number]	k4	260
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
požadovala	požadovat	k5eAaImAgFnS	požadovat
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
Horthy	Hortha	k1gFnPc4	Hortha
souzen	souzen	k2eAgMnSc1d1	souzen
jako	jako	k8xC	jako
válečný	válečný	k2eAgMnSc1d1	válečný
zločinec	zločinec	k1gMnSc1	zločinec
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
spojeneckými	spojenecký	k2eAgFnPc7d1	spojenecká
státy	stát	k1gInPc4	stát
zamítnuto	zamítnout	k5eAaPmNgNnS	zamítnout
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
díky	díky	k7c3	díky
vlivu	vliv	k1gInSc3	vliv
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Stalin	Stalin	k1gMnSc1	Stalin
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
souhlasil	souhlasit	k5eAaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
především	především	k9	především
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
k	k	k7c3	k
Horthyho	Horthyha	k1gFnSc5	Horthyha
vysokému	vysoký	k2eAgInSc3d1	vysoký
věku	věk	k1gInSc3	věk
<g/>
.	.	kIx.	.
</s>
<s>
Horthy	Hortha	k1gFnPc4	Hortha
byl	být	k5eAaImAgInS	být
propuštěn	propuštěn	k2eAgInSc1d1	propuštěn
a	a	k8xC	a
v	v	k7c6	v
Norimberském	norimberský	k2eAgInSc6d1	norimberský
procesu	proces	k1gInSc6	proces
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
svědek	svědek	k1gMnSc1	svědek
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
usadil	usadit	k5eAaPmAgInS	usadit
v	v	k7c6	v
portugalském	portugalský	k2eAgInSc6d1	portugalský
Estorilu	Estoril	k1gInSc6	Estoril
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dožil	dožít	k5eAaPmAgMnS	dožít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
napsal	napsat	k5eAaBmAgMnS	napsat
vlastní	vlastní	k2eAgInPc4d1	vlastní
memoáry	memoáry	k1gInPc4	memoáry
<g/>
,	,	kIx,	,
Ein	Ein	k1gFnPc4	Ein
Leben	Lebna	k1gFnPc2	Lebna
für	für	k?	für
Ungarn	Ungarn	k1gNnSc4	Ungarn
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
A	a	k8xC	a
Life	Life	k1gFnSc1	Life
for	forum	k1gNnPc2	forum
Hungary	Hungara	k1gFnSc2	Hungara
<g/>
,	,	kIx,	,
Život	život	k1gInSc1	život
pro	pro	k7c4	pro
Maďarsko	Maďarsko	k1gNnSc4	Maďarsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
mnoho	mnoho	k4c1	mnoho
vlastních	vlastní	k2eAgFnPc2d1	vlastní
zkušeností	zkušenost	k1gFnPc2	zkušenost
a	a	k8xC	a
zážitků	zážitek	k1gInPc2	zážitek
od	od	k7c2	od
dětství	dětství	k1gNnSc2	dětství
až	až	k9	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
píše	psát	k5eAaImIp3nS	psát
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
znal	znát	k5eAaImAgMnS	znát
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
,	,	kIx,	,
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
neměl	mít	k5eNaImAgMnS	mít
důvěru	důvěra	k1gFnSc4	důvěra
<g/>
,	,	kIx,	,
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
snažil	snažit	k5eAaImAgInS	snažit
podávat	podávat	k5eAaImF	podávat
maximální	maximální	k2eAgInPc4d1	maximální
výkony	výkon	k1gInPc4	výkon
a	a	k8xC	a
jmenovat	jmenovat	k5eAaBmF	jmenovat
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
funkcionáře	funkcionář	k1gMnPc4	funkcionář
a	a	k8xC	a
úředníky	úředník	k1gMnPc4	úředník
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
také	také	k9	také
na	na	k7c4	na
necitlivé	citlivý	k2eNgNnSc4d1	necitlivé
a	a	k8xC	a
kruté	krutý	k2eAgNnSc4d1	kruté
zacházení	zacházení	k1gNnSc4	zacházení
<g/>
,	,	kIx,	,
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
Maďarsku	Maďarsko	k1gNnSc3	Maďarsko
od	od	k7c2	od
konce	konec	k1gInSc2	konec
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
od	od	k7c2	od
mnoha	mnoho	k4c2	mnoho
zemí	zem	k1gFnPc2	zem
dostalo	dostat	k5eAaPmAgNnS	dostat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
poznámek	poznámka	k1gFnPc2	poznámka
v	v	k7c6	v
psaných	psaný	k2eAgInPc6d1	psaný
memoárech	memoáry	k1gInPc6	memoáry
lze	lze	k6eAd1	lze
vyčíst	vyčíst	k5eAaPmF	vyčíst
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
zdrcený	zdrcený	k2eAgInSc1d1	zdrcený
z	z	k7c2	z
neúspěchu	neúspěch	k1gInSc2	neúspěch
Maďarského	maďarský	k2eAgNnSc2d1	Maďarské
povstání	povstání	k1gNnSc2	povstání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
poslední	poslední	k2eAgFnSc6d1	poslední
vůli	vůle	k1gFnSc6	vůle
výslovně	výslovně	k6eAd1	výslovně
žádá	žádat	k5eAaImIp3nS	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jeho	jeho	k3xOp3gInPc1	jeho
tělesné	tělesný	k2eAgInPc1d1	tělesný
ostatky	ostatek	k1gInPc1	ostatek
nebyly	být	k5eNaImAgInP	být
pohřbeny	pohřben	k2eAgInPc1d1	pohřben
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
odtamtud	odtamtud	k6eAd1	odtamtud
neodejdou	odejít	k5eNaPmIp3nP	odejít
sovětští	sovětský	k2eAgMnPc1d1	sovětský
vojáci	voják	k1gMnPc1	voják
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
dědicové	dědic	k1gMnPc1	dědic
toto	tento	k3xDgNnSc4	tento
přání	přání	k1gNnSc4	přání
splnili	splnit	k5eAaPmAgMnP	splnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
když	když	k8xS	když
sovětská	sovětský	k2eAgFnSc1d1	sovětská
armáda	armáda	k1gFnSc1	armáda
opustila	opustit	k5eAaPmAgFnS	opustit
definitivně	definitivně	k6eAd1	definitivně
své	svůj	k3xOyFgFnPc4	svůj
základny	základna	k1gFnPc4	základna
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
ostatky	ostatek	k1gInPc1	ostatek
Miklóse	Miklós	k1gMnSc2	Miklós
Horthyho	Horthy	k1gMnSc2	Horthy
převezeny	převézt	k5eAaPmNgFnP	převézt
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
a	a	k8xC	a
pohřbeny	pohřben	k2eAgFnPc1d1	pohřbena
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
Kenderes	Kenderesa	k1gFnPc2	Kenderesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
KONTLER	KONTLER	kA	KONTLER
<g/>
,	,	kIx,	,
László	László	k1gFnPc1	László
-	-	kIx~	-
Dějiny	dějiny	k1gFnPc1	dějiny
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
-	-	kIx~	-
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
ISBN	ISBN	kA	ISBN
978-80-7106-616-3	[number]	k4	978-80-7106-616-3
</s>
</p>
<p>
<s>
PRAŽÁK	Pražák	k1gMnSc1	Pražák
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
-	-	kIx~	-
Stručná	stručný	k2eAgFnSc1d1	stručná
historie	historie	k1gFnSc1	historie
států	stát	k1gInPc2	stát
MAĎARSKO	Maďarsko	k1gNnSc1	Maďarsko
-	-	kIx~	-
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Libri	Libr	k1gFnSc2	Libr
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
-	-	kIx~	-
ISBN	ISBN	kA	ISBN
80-7277-269-4	[number]	k4	80-7277-269-4
</s>
</p>
<p>
<s>
Jozef	Jozef	k1gMnSc1	Jozef
Lettrich	Lettrich	k1gMnSc1	Lettrich
<g/>
:	:	kIx,	:
Dejiny	Dejin	k1gInPc1	Dejin
novodobého	novodobý	k2eAgNnSc2d1	novodobé
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Archa	archa	k1gFnSc1	archa
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7115-045-2	[number]	k4	80-7115-045-2
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
</s>
</p>
<p>
<s>
Maďarské	maďarský	k2eAgNnSc1d1	Maďarské
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
představitelů	představitel	k1gMnPc2	představitel
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Miklós	Miklósa	k1gFnPc2	Miklósa
Horthy	Hortha	k1gFnSc2	Hortha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Diktatury	diktatura	k1gFnPc1	diktatura
v	v	k7c6	v
rukavičkách	rukavička	k1gFnPc6	rukavička
<g/>
?	?	kIx.	?
</s>
</p>
<p>
<s>
Vice-Admiral	Vice-Admirat	k5eAaPmAgInS	Vice-Admirat
Miklós	Miklós	k1gInSc1	Miklós
Horthy	Hortha	k1gFnSc2	Hortha
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
mateinfo	mateinfo	k6eAd1	mateinfo
<g/>
.	.	kIx.	.
<g/>
hu	hu	k0	hu
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
