<s>
Miklós	Miklós	k1gInSc1	Miklós
Horthy	Hortha	k1gFnSc2	Hortha
de	de	k?	de
Nagybánya	Nagybánya	k1gMnSc1	Nagybánya
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1868	[number]	k4	1868
<g/>
,	,	kIx,	,
Kenderes	Kenderes	k1gInSc1	Kenderes
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
Estoril	Estoril	k1gInSc1	Estoril
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
maďarský	maďarský	k2eAgMnSc1d1	maďarský
viceadmirál	viceadmirál	k1gMnSc1	viceadmirál
velící	velící	k2eAgFnSc2d1	velící
rakousko-uherskému	rakouskoherský	k2eAgNnSc3d1	rakousko-uherské
válečnému	válečný	k2eAgNnSc3d1	válečné
loďstvu	loďstvo	k1gNnSc3	loďstvo
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
regentem	regens	k1gMnSc7	regens
Maďarského	maďarský	k2eAgNnSc2d1	Maďarské
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
funkci	funkce	k1gFnSc3	funkce
hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
vykonával	vykonávat	k5eAaImAgInS	vykonávat
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1920	[number]	k4	1920
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
