<p>
<s>
Ribonukleová	ribonukleový	k2eAgFnSc1d1	ribonukleová
kyselina	kyselina	k1gFnSc1	kyselina
(	(	kIx(	(
<g/>
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
dříve	dříve	k6eAd2	dříve
RNK	RNK	kA	RNK
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nukleová	nukleový	k2eAgFnSc1d1	nukleová
kyselina	kyselina	k1gFnSc1	kyselina
tvořená	tvořený	k2eAgFnSc1d1	tvořená
vláknem	vlákno	k1gNnSc7	vlákno
ribonukleotidů	ribonukleotid	k1gInPc2	ribonukleotid
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
cukr	cukr	k1gInSc4	cukr
ribózu	ribóza	k1gFnSc4	ribóza
a	a	k8xC	a
nukleové	nukleový	k2eAgFnSc2d1	nukleová
báze	báze	k1gFnSc2	báze
adenin	adenin	k1gInSc1	adenin
<g/>
,	,	kIx,	,
guanin	guanin	k1gInSc1	guanin
<g/>
,	,	kIx,	,
cytosin	cytosin	k1gInSc1	cytosin
a	a	k8xC	a
uracil	uracil	k1gInSc1	uracil
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
přenos	přenos	k1gInSc4	přenos
informace	informace	k1gFnSc2	informace
z	z	k7c2	z
úrovně	úroveň	k1gFnSc2	úroveň
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
do	do	k7c2	do
proteinů	protein	k1gInPc2	protein
a	a	k8xC	a
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
virů	vir	k1gInPc2	vir
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
samotnou	samotný	k2eAgFnSc7d1	samotná
nositelkou	nositelka	k1gFnSc7	nositelka
genetické	genetický	k2eAgFnSc2d1	genetická
informace	informace	k1gFnSc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
deoxyribonukleové	deoxyribonukleový	k2eAgFnSc3d1	deoxyribonukleová
kyselině	kyselina	k1gFnSc3	kyselina
(	(	kIx(	(
<g/>
DNA	DNA	kA	DNA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
které	který	k3yQgFnSc2	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
jednak	jednak	k8xC	jednak
přítomností	přítomnost	k1gFnSc7	přítomnost
ribózy	ribóza	k1gFnSc2	ribóza
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
cukr-fosfátové	cukrosfátový	k2eAgFnSc6d1	cukr-fosfátový
kostře	kostra	k1gFnSc6	kostra
namísto	namísto	k7c2	namísto
deoxyribózy	deoxyribóza	k1gFnSc2	deoxyribóza
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
využívá	využívat	k5eAaPmIp3nS	využívat
nukleovou	nukleový	k2eAgFnSc4d1	nukleová
bázi	báze	k1gFnSc4	báze
uracil	uracila	k1gFnPc2	uracila
namísto	namísto	k7c2	namísto
thyminu	thymin	k1gInSc2	thymin
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
větší	veliký	k2eAgFnSc3d2	veliký
reaktivitě	reaktivita	k1gFnSc3	reaktivita
ribózy	ribóza	k1gFnSc2	ribóza
může	moct	k5eAaImIp3nS	moct
molekula	molekula	k1gFnSc1	molekula
RNA	RNA	kA	RNA
zaujímat	zaujímat	k5eAaImF	zaujímat
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
prostorových	prostorový	k2eAgNnPc2d1	prostorové
uspořádání	uspořádání	k1gNnPc2	uspořádání
a	a	k8xC	a
zastávat	zastávat	k5eAaImF	zastávat
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
než	než	k8xS	než
mnohem	mnohem	k6eAd1	mnohem
stabilnější	stabilní	k2eAgMnSc1d2	stabilnější
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
využívaná	využívaný	k2eAgFnSc1d1	využívaná
buňkou	buňka	k1gFnSc7	buňka
především	především	k9	především
jako	jako	k8xC	jako
úložiště	úložiště	k1gNnPc4	úložiště
genetické	genetický	k2eAgFnPc4d1	genetická
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Molekula	molekula	k1gFnSc1	molekula
RNA	RNA	kA	RNA
je	být	k5eAaImIp3nS	být
také	také	k9	také
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
DNA	dno	k1gNnSc2	dno
obvykle	obvykle	k6eAd1	obvykle
jednovláknová	jednovláknový	k2eAgFnSc1d1	jednovláknová
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
ovšem	ovšem	k9	ovšem
díky	díky	k7c3	díky
vnitřnímu	vnitřní	k2eAgNnSc3d1	vnitřní
párování	párování	k1gNnSc3	párování
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
složitější	složitý	k2eAgFnSc4d2	složitější
strukturu	struktura	k1gFnSc4	struktura
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
virů	vir	k1gInPc2	vir
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
dvouvláknová	dvouvláknový	k2eAgFnSc1d1	dvouvláknová
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RNA	RNA	kA	RNA
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
řadu	řad	k1gInSc2	řad
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
hlavní	hlavní	k2eAgMnSc1d1	hlavní
je	být	k5eAaImIp3nS	být
zajištění	zajištění	k1gNnSc1	zajištění
překladu	překlad	k1gInSc2	překlad
genetického	genetický	k2eAgInSc2d1	genetický
kódu	kód	k1gInSc2	kód
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
převod	převod	k1gInSc1	převod
informace	informace	k1gFnSc2	informace
z	z	k7c2	z
DNA	dno	k1gNnSc2	dno
do	do	k7c2	do
struktury	struktura	k1gFnSc2	struktura
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
DNA	DNA	kA	DNA
nesoucí	nesoucí	k2eAgInSc1d1	nesoucí
gen	gen	k1gInSc1	gen
je	být	k5eAaImIp3nS	být
nejprve	nejprve	k6eAd1	nejprve
přepsána	přepsán	k2eAgFnSc1d1	přepsána
(	(	kIx(	(
<g/>
procesem	proces	k1gInSc7	proces
transkripce	transkripce	k1gFnSc2	transkripce
<g/>
)	)	kIx)	)
do	do	k7c2	do
mediátorové	mediátorový	k2eAgFnSc2d1	mediátorová
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
následně	následně	k6eAd1	následně
přeložena	přeložit	k5eAaPmNgFnS	přeložit
(	(	kIx(	(
<g/>
procesem	proces	k1gInSc7	proces
translace	translace	k1gFnSc2	translace
<g/>
)	)	kIx)	)
do	do	k7c2	do
proteinů	protein	k1gInPc2	protein
tvořených	tvořený	k2eAgFnPc2d1	tvořená
řetězcem	řetězec	k1gInSc7	řetězec
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
<g/>
.	.	kIx.	.
</s>
<s>
Zařazení	zařazení	k1gNnSc1	zařazení
správné	správný	k2eAgFnSc2d1	správná
aminokyseliny	aminokyselina	k1gFnSc2	aminokyselina
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
proteinů	protein	k1gInPc2	protein
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
vazba	vazba	k1gFnSc1	vazba
transferové	transferový	k2eAgFnSc2d1	transferová
RNA	RNA	kA	RNA
na	na	k7c4	na
specifický	specifický	k2eAgInSc4d1	specifický
kodón	kodón	k1gInSc4	kodón
v	v	k7c6	v
mRNA	mRNA	k?	mRNA
pomocí	pomocí	k7c2	pomocí
párování	párování	k1gNnSc2	párování
jejich	jejich	k3xOp3gFnPc2	jejich
bází	báze	k1gFnPc2	báze
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
překlad	překlad	k1gInSc1	překlad
genetického	genetický	k2eAgInSc2d1	genetický
kódu	kód	k1gInSc2	kód
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
ribozomu	ribozom	k1gInSc6	ribozom
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
složený	složený	k2eAgMnSc1d1	složený
z	z	k7c2	z
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
rRNA	rRNA	k?	rRNA
<g/>
)	)	kIx)	)
i	i	k9	i
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
RNA	RNA	kA	RNA
v	v	k7c6	v
ribozomu	ribozom	k1gInSc6	ribozom
tvoří	tvořit	k5eAaImIp3nP	tvořit
nejen	nejen	k6eAd1	nejen
strukturní	strukturní	k2eAgFnSc4d1	strukturní
složku	složka	k1gFnSc4	složka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
i	i	k9	i
za	za	k7c4	za
syntézu	syntéza	k1gFnSc4	syntéza
peptidové	peptidový	k2eAgFnSc2d1	peptidová
vazby	vazba	k1gFnSc2	vazba
v	v	k7c6	v
nově	nově	k6eAd1	nově
vznikajícím	vznikající	k2eAgInSc6d1	vznikající
proteinu	protein	k1gInSc6	protein
<g/>
.	.	kIx.	.
</s>
<s>
Ribozom	Ribozom	k1gInSc1	Ribozom
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
významným	významný	k2eAgMnSc7d1	významný
zástupcem	zástupce	k1gMnSc7	zástupce
skupiny	skupina	k1gFnSc2	skupina
RNA	RNA	kA	RNA
s	s	k7c7	s
katalytickou	katalytický	k2eAgFnSc7d1	katalytická
aktivitou	aktivita	k1gFnSc7	aktivita
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
ribozymů	ribozym	k1gInPc2	ribozym
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
už	už	k6eAd1	už
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
rolí	role	k1gFnPc2	role
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
genetického	genetický	k2eAgInSc2d1	genetický
kódu	kód	k1gInSc2	kód
nebo	nebo	k8xC	nebo
strukturních	strukturní	k2eAgFnPc2d1	strukturní
a	a	k8xC	a
katalytických	katalytický	k2eAgFnPc2d1	katalytická
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
hraje	hrát	k5eAaImIp3nS	hrát
RNA	RNA	kA	RNA
roli	role	k1gFnSc4	role
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
dalších	další	k2eAgInPc2d1	další
buněčných	buněčný	k2eAgInPc2d1	buněčný
pochodů	pochod	k1gInPc2	pochod
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
úprava	úprava	k1gFnSc1	úprava
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
jemná	jemný	k2eAgFnSc1d1	jemná
kontrola	kontrola	k1gFnSc1	kontrola
translace	translace	k1gFnSc1	translace
pomocí	pomocí	k7c2	pomocí
RNA	RNA	kA	RNA
interference	interference	k1gFnSc2	interference
<g/>
,	,	kIx,	,
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
templát	templát	k1gInSc1	templát
pro	pro	k7c4	pro
syntézu	syntéza	k1gFnSc4	syntéza
telomer	telomera	k1gFnPc2	telomera
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
Podle	podle	k7c2	podle
hypotézy	hypotéza	k1gFnSc2	hypotéza
RNA	RNA	kA	RNA
světa	svět	k1gInSc2	svět
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
RNA	RNA	kA	RNA
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
všestrannost	všestrannost	k1gFnSc4	všestrannost
<g/>
,	,	kIx,	,
schopnost	schopnost	k1gFnSc4	schopnost
nést	nést	k5eAaImF	nést
i	i	k8xC	i
replikovat	replikovat	k5eAaImF	replikovat
genetickou	genetický	k2eAgFnSc4d1	genetická
informaci	informace	k1gFnSc4	informace
a	a	k8xC	a
syntetizovat	syntetizovat	k5eAaImF	syntetizovat
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
proteiny	protein	k1gInPc7	protein
první	první	k4xOgFnSc7	první
nukleovou	nukleový	k2eAgFnSc7d1	nukleová
kyselinou	kyselina	k1gFnSc7	kyselina
využívanou	využívaný	k2eAgFnSc7d1	využívaná
živými	živý	k2eAgInPc7d1	živý
organismy	organismus	k1gInPc7	organismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
výzkumu	výzkum	k1gInSc2	výzkum
RNA	RNA	kA	RNA
==	==	k?	==
</s>
</p>
<p>
<s>
RNA	RNA	kA	RNA
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
izolována	izolován	k2eAgFnSc1d1	izolována
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
s	s	k7c7	s
DNA	DNA	kA	DNA
F.	F.	kA	F.
Miescherem	Miescher	k1gMnSc7	Miescher
a	a	k8xC	a
nazvána	nazván	k2eAgFnSc1d1	nazvána
nuklein	nuklein	k1gInSc1	nuklein
<g/>
,	,	kIx,	,
nuklein	nuklein	k1gInSc1	nuklein
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
původně	původně	k6eAd1	původně
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
pouhý	pouhý	k2eAgInSc4d1	pouhý
buněčný	buněčný	k2eAgInSc4d1	buněčný
sklad	sklad	k1gInSc4	sklad
fosfátu	fosfát	k1gInSc2	fosfát
<g/>
.	.	kIx.	.
</s>
<s>
Existenci	existence	k1gFnSc4	existence
dvou	dva	k4xCgFnPc2	dva
různých	různý	k2eAgFnPc2d1	různá
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
naznačovaly	naznačovat	k5eAaImAgInP	naznačovat
výzkumy	výzkum	k1gInPc1	výzkum
Albrechta	Albrecht	k1gMnSc2	Albrecht
Kossela	Kossel	k1gMnSc2	Kossel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
mezi	mezi	k7c4	mezi
lety	let	k1gInPc4	let
1885	[number]	k4	1885
<g/>
–	–	k?	–
<g/>
1901	[number]	k4	1901
popsal	popsat	k5eAaPmAgMnS	popsat
všechny	všechen	k3xTgFnPc4	všechen
hlavní	hlavní	k2eAgFnPc4d1	hlavní
báze	báze	k1gFnPc4	báze
a	a	k8xC	a
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nukleové	nukleový	k2eAgFnSc2d1	nukleová
kyseliny	kyselina	k1gFnSc2	kyselina
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
cukry	cukr	k1gInPc4	cukr
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
charakterizoval	charakterizovat	k5eAaBmAgMnS	charakterizovat
jako	jako	k9	jako
pentózy	pentóza	k1gFnSc2	pentóza
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
panovala	panovat	k5eAaImAgFnS	panovat
představa	představa	k1gFnSc1	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
živočichové	živočich	k1gMnPc1	živočich
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
izolace	izolace	k1gFnSc2	izolace
z	z	k7c2	z
thymu	thym	k1gInSc2	thym
nazvána	nazvat	k5eAaPmNgFnS	nazvat
"	"	kIx"	"
<g/>
thymovou	thymový	k2eAgFnSc7d1	thymový
nukleovou	nukleový	k2eAgFnSc7d1	nukleová
kyselinou	kyselina	k1gFnSc7	kyselina
<g/>
"	"	kIx"	"
a	a	k8xC	a
nižší	nízký	k2eAgFnSc1d2	nižší
eukaryota	eukaryota	k1gFnSc1	eukaryota
a	a	k8xC	a
rostliny	rostlina	k1gFnPc1	rostlina
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
nazývána	nazývat	k5eAaImNgFnS	nazývat
"	"	kIx"	"
<g/>
kvasinkovou	kvasinkový	k2eAgFnSc7d1	kvasinková
nukleovou	nukleový	k2eAgFnSc7d1	nukleová
kyselinou	kyselina	k1gFnSc7	kyselina
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Chemickou	chemický	k2eAgFnSc4d1	chemická
strukturu	struktura	k1gFnSc4	struktura
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
upřesnil	upřesnit	k5eAaPmAgMnS	upřesnit
Phoebus	Phoebus	k1gMnSc1	Phoebus
Levene	Leven	k1gInSc5	Leven
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
zjistil	zjistit	k5eAaPmAgInS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
RNA	RNA	kA	RNA
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ribózu	ribóza	k1gFnSc4	ribóza
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
že	že	k8xS	že
DNA	DNA	kA	DNA
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
deoxyribózu	deoxyribóza	k1gFnSc4	deoxyribóza
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yRnSc2	což
vychází	vycházet	k5eAaImIp3nS	vycházet
současné	současný	k2eAgNnSc4d1	současné
pojmenování	pojmenování	k1gNnSc4	pojmenování
těchto	tento	k3xDgFnPc2	tento
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Levene	Leven	k1gMnSc5	Leven
také	také	k9	také
objevil	objevit	k5eAaPmAgInS	objevit
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
jsou	být	k5eAaImIp3nP	být
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
složky	složka	k1gFnPc1	složka
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
propojeny	propojit	k5eAaPmNgFnP	propojit
–	–	k?	–
objevil	objevit	k5eAaPmAgMnS	objevit
cukr-fosfátovou	cukrosfátový	k2eAgFnSc4d1	cukr-fosfátový
kostru	kostra	k1gFnSc4	kostra
a	a	k8xC	a
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
tzv.	tzv.	kA	tzv.
tetranukleotidovou	tetranukleotidový	k2eAgFnSc4d1	tetranukleotidový
hypotézu	hypotéza	k1gFnSc4	hypotéza
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
jsou	být	k5eAaImIp3nP	být
nukleové	nukleový	k2eAgFnPc1d1	nukleová
kyseliny	kyselina	k1gFnPc1	kyselina
tvořené	tvořený	k2eAgFnPc1d1	tvořená
pouhou	pouhý	k2eAgFnSc7d1	pouhá
čtveřicí	čtveřice	k1gFnSc7	čtveřice
nukleotidů	nukleotid	k1gInPc2	nukleotid
<g/>
.	.	kIx.	.
</s>
<s>
Levenova	Levenův	k2eAgFnSc1d1	Levenův
tetranukleotidová	tetranukleotidový	k2eAgFnSc1d1	tetranukleotidový
hypotéza	hypotéza	k1gFnSc1	hypotéza
(	(	kIx(	(
<g/>
publikována	publikován	k2eAgFnSc1d1	publikována
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
nesprávně	správně	k6eNd1	správně
naznačovala	naznačovat	k5eAaImAgFnS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nukleové	nukleový	k2eAgFnSc2d1	nukleová
kyseliny	kyselina	k1gFnSc2	kyselina
jsou	být	k5eAaImIp3nP	být
příliš	příliš	k6eAd1	příliš
jednoduché	jednoduchý	k2eAgFnPc1d1	jednoduchá
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
nést	nést	k5eAaImF	nést
genetickou	genetický	k2eAgFnSc4d1	genetická
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
a	a	k8xC	a
výzkum	výzkum	k1gInSc1	výzkum
RNA	RNA	kA	RNA
a	a	k8xC	a
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
se	se	k3xPyFc4	se
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
zastavil	zastavit	k5eAaPmAgInS	zastavit
<g/>
.	.	kIx.	.
<g/>
Role	role	k1gFnSc1	role
RNA	RNA	kA	RNA
v	v	k7c6	v
syntéze	syntéza	k1gFnSc6	syntéza
proteinů	protein	k1gInPc2	protein
byla	být	k5eAaImAgFnS	být
<g />
.	.	kIx.	.
</s>
<s>
naznačována	naznačován	k2eAgFnSc1d1	naznačována
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
množství	množství	k1gNnSc1	množství
DNA	DNA	kA	DNA
v	v	k7c6	v
nedělící	dělící	k2eNgFnSc6d1	dělící
se	se	k3xPyFc4	se
buňce	buňka	k1gFnSc3	buňka
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
konstantní	konstantní	k2eAgMnSc1d1	konstantní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
množství	množství	k1gNnSc1	množství
RNA	RNA	kA	RNA
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
u	u	k7c2	u
aktivních	aktivní	k2eAgFnPc2d1	aktivní
buněk	buňka	k1gFnPc2	buňka
a	a	k8xC	a
snižuje	snižovat	k5eAaImIp3nS	snižovat
se	se	k3xPyFc4	se
u	u	k7c2	u
neaktivních	aktivní	k2eNgInPc2d1	neaktivní
<g/>
,	,	kIx,	,
hladovějících	hladovějící	k2eAgInPc2d1	hladovějící
<g/>
;	;	kIx,	;
u	u	k7c2	u
aktivních	aktivní	k2eAgFnPc2d1	aktivní
buněk	buňka	k1gFnPc2	buňka
se	se	k3xPyFc4	se
také	také	k6eAd1	také
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
výrazná	výrazný	k2eAgNnPc4d1	výrazné
ribonukleoproteinová	ribonukleoproteinový	k2eAgNnPc4d1	ribonukleoproteinový
tělíska	tělísko	k1gNnPc4	tělísko
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
jako	jako	k8xC	jako
ribozomy	ribozom	k1gInPc7	ribozom
<g/>
.	.	kIx.	.
</s>
<s>
Existenci	existence	k1gFnSc3	existence
mRNA	mRNA	k?	mRNA
předpovídal	předpovídat	k5eAaImAgInS	předpovídat
Francis	Francis	k1gFnSc4	Francis
Crick	Cricka	k1gFnPc2	Cricka
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
centrálním	centrální	k2eAgNnSc6d1	centrální
dogmatu	dogma	k1gNnSc6	dogma
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
očekával	očekávat	k5eAaImAgMnS	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c7	mezi
DNA	DNA	kA	DNA
a	a	k8xC	a
proteiny	protein	k1gInPc1	protein
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
nějaký	nějaký	k3yIgInSc4	nějaký
prostředník	prostředník	k1gInSc4	prostředník
<g/>
.	.	kIx.	.
</s>
<s>
Crick	Crick	k6eAd1	Crick
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
dalších	další	k2eAgMnPc2d1	další
vědců	vědec	k1gMnPc2	vědec
založili	založit	k5eAaPmAgMnP	založit
RNA-tie	RNAie	k1gFnPc4	RNA-tie
club	club	k1gInSc1	club
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
klub	klub	k1gInSc1	klub
RNA	RNA	kA	RNA
kravat	kravata	k1gFnPc2	kravata
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
dal	dát	k5eAaPmAgMnS	dát
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
odhalit	odhalit	k5eAaPmF	odhalit
genetický	genetický	k2eAgInSc4d1	genetický
kód	kód	k1gInSc4	kód
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
RNA-tie	RNAie	k1gFnSc2	RNA-tie
klubu	klub	k1gInSc2	klub
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
popsali	popsat	k5eAaPmAgMnP	popsat
kodón	kodón	k1gMnSc1	kodón
<g/>
,	,	kIx,	,
teoreticky	teoreticky	k6eAd1	teoreticky
vypočetli	vypočíst	k5eAaPmAgMnP	vypočíst
<g/>
,	,	kIx,	,
že	že	k8xS	že
počet	počet	k1gInSc1	počet
nukleotidů	nukleotid	k1gInPc2	nukleotid
v	v	k7c6	v
kodónu	kodón	k1gInSc6	kodón
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
tři	tři	k4xCgFnPc4	tři
<g/>
,	,	kIx,	,
a	a	k8xC	a
postulovali	postulovat	k5eAaImAgMnP	postulovat
adaptérovou	adaptérový	k2eAgFnSc4d1	adaptérová
hypotézu	hypotéza	k1gFnSc4	hypotéza
předpovídající	předpovídající	k2eAgFnSc4d1	předpovídající
existenci	existence	k1gFnSc4	existence
tRNA	trnout	k5eAaImSgMnS	trnout
<g/>
.	.	kIx.	.
</s>
<s>
Předpovězené	Předpovězený	k2eAgFnPc1d1	Předpovězená
molekuly	molekula	k1gFnPc1	molekula
mRNA	mRNA	k?	mRNA
a	a	k8xC	a
tRNA	trnout	k5eAaImSgInS	trnout
byly	být	k5eAaImAgInP	být
později	pozdě	k6eAd2	pozdě
skutečně	skutečně	k6eAd1	skutečně
objeveny	objevit	k5eAaPmNgInP	objevit
koncem	koncem	k7c2	koncem
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
se	se	k3xPyFc4	se
už	už	k6eAd1	už
také	také	k9	také
dlouho	dlouho	k6eAd1	dlouho
předpokládáno	předpokládán	k2eAgNnSc1d1	předpokládáno
<g/>
,	,	kIx,	,
že	že	k8xS	že
syntéza	syntéza	k1gFnSc1	syntéza
proteinů	protein	k1gInPc2	protein
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
ribozomech	ribozom	k1gInPc6	ribozom
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
klíčovým	klíčový	k2eAgInSc7d1	klíčový
momentem	moment	k1gInSc7	moment
výzkumu	výzkum	k1gInSc2	výzkum
RNA	RNA	kA	RNA
byla	být	k5eAaImAgFnS	být
izolace	izolace	k1gFnSc1	izolace
enzymu	enzym	k1gInSc2	enzym
polynukleotid	polynukleotida	k1gFnPc2	polynukleotida
fosforylázy	fosforyláza	k1gFnSc2	fosforyláza
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgInSc1d1	schopen
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
podmínek	podmínka	k1gFnPc2	podmínka
syntetizovat	syntetizovat	k5eAaImF	syntetizovat
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yQnSc4	což
získal	získat	k5eAaPmAgMnS	získat
objevitel	objevitel	k1gMnSc1	objevitel
Severo	Severa	k1gMnSc5	Severa
Ochoa	Ocho	k2eAgFnSc1d1	Ocho
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
medicínu	medicína	k1gFnSc4	medicína
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc4	tento
enzym	enzym	k1gInSc4	enzym
přirozeně	přirozeně	k6eAd1	přirozeně
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
degradaci	degradace	k1gFnSc3	degradace
RNA	RNA	kA	RNA
a	a	k8xC	a
skutečná	skutečný	k2eAgFnSc1d1	skutečná
RNA	RNA	kA	RNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
byla	být	k5eAaImAgFnS	být
objevena	objevit	k5eAaPmNgFnS	objevit
až	až	k9	až
v	v	k7c6	v
době	doba	k1gFnSc6	doba
udělení	udělení	k1gNnSc2	udělení
této	tento	k3xDgFnSc2	tento
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
<g/>
,	,	kIx,	,
syntéza	syntéza	k1gFnSc1	syntéza
RNA	RNA	kA	RNA
umožnila	umožnit	k5eAaPmAgFnS	umožnit
rozluštit	rozluštit	k5eAaPmF	rozluštit
genetický	genetický	k2eAgInSc4d1	genetický
kód	kód	k1gInSc4	kód
<g/>
:	:	kIx,	:
prvním	první	k4xOgMnSc6	první
zjištěním	zjištění	k1gNnPc3	zjištění
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
RNA	RNA	kA	RNA
tvořená	tvořený	k2eAgFnSc1d1	tvořená
výhradně	výhradně	k6eAd1	výhradně
spojením	spojení	k1gNnSc7	spojení
uridinu	uridina	k1gFnSc4	uridina
(	(	kIx(	(
<g/>
polyuracil	polyuracil	k1gInSc4	polyuracil
<g/>
)	)	kIx)	)
dává	dávat	k5eAaImIp3nS	dávat
vzniknout	vzniknout	k5eAaPmF	vzniknout
polypeptidu	polypeptid	k1gInSc3	polypeptid
tvořenému	tvořený	k2eAgInSc3d1	tvořený
pouze	pouze	k6eAd1	pouze
molekulami	molekula	k1gFnPc7	molekula
fenylalaninu	fenylalanin	k2eAgMnSc3d1	fenylalanin
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
kodón	kodón	k1gMnSc1	kodón
"	"	kIx"	"
<g/>
UUU	UUU	kA	UUU
<g/>
"	"	kIx"	"
kóduje	kódovat	k5eAaBmIp3nS	kódovat
aminokyselinu	aminokyselina	k1gFnSc4	aminokyselina
fenylalanin	fenylalanina	k1gFnPc2	fenylalanina
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
genetického	genetický	k2eAgInSc2d1	genetický
kódu	kód	k1gInSc2	kód
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
rozluštěn	rozluštit	k5eAaPmNgInS	rozluštit
během	během	k7c2	během
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
a	a	k8xC	a
výzkum	výzkum	k1gInSc1	výzkum
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
oceněn	ocenit	k5eAaPmNgInS	ocenit
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
popsání	popsání	k1gNnSc6	popsání
klíčové	klíčový	k2eAgFnSc2d1	klíčová
role	role	k1gFnSc2	role
RNA	RNA	kA	RNA
v	v	k7c6	v
syntéze	syntéza	k1gFnSc6	syntéza
proteinů	protein	k1gInPc2	protein
následovala	následovat	k5eAaImAgFnS	následovat
velká	velký	k2eAgFnSc1d1	velká
vlna	vlna	k1gFnSc1	vlna
zájmu	zájem	k1gInSc2	zájem
o	o	k7c6	o
RNA	RNA	kA	RNA
související	související	k2eAgInPc4d1	související
s	s	k7c7	s
výzkumem	výzkum	k1gInSc7	výzkum
genetického	genetický	k2eAgInSc2d1	genetický
kódu	kód	k1gInSc2	kód
a	a	k8xC	a
molekuly	molekula	k1gFnSc2	molekula
RNA	RNA	kA	RNA
účastnící	účastnící	k2eAgInPc4d1	účastnící
se	se	k3xPyFc4	se
zpracování	zpracování	k1gNnPc2	zpracování
genetického	genetický	k2eAgInSc2d1	genetický
kódu	kód	k1gInSc2	kód
byly	být	k5eAaImAgInP	být
detailně	detailně	k6eAd1	detailně
zkoumány	zkoumán	k2eAgInPc1d1	zkoumán
<g/>
.	.	kIx.	.
tRNA	trnout	k5eAaImSgInS	trnout
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc7	první
osekvenovanou	osekvenovaný	k2eAgFnSc7d1	osekvenovaný
(	(	kIx(	(
<g/>
přečtenou	přečtený	k2eAgFnSc7d1	přečtená
<g/>
)	)	kIx)	)
nukleovou	nukleový	k2eAgFnSc7d1	nukleová
kyselinou	kyselina	k1gFnSc7	kyselina
a	a	k8xC	a
první	první	k4xOgFnSc7	první
nukleovou	nukleový	k2eAgFnSc7d1	nukleová
kyselinou	kyselina	k1gFnSc7	kyselina
s	s	k7c7	s
určenou	určený	k2eAgFnSc7d1	určená
krystalickou	krystalický	k2eAgFnSc7d1	krystalická
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
pokrokům	pokrok	k1gInPc3	pokrok
v	v	k7c6	v
analýze	analýza	k1gFnSc6	analýza
RNA	RNA	kA	RNA
byl	být	k5eAaImAgInS	být
také	také	k9	také
přečten	přečíst	k5eAaPmNgInS	přečíst
první	první	k4xOgInSc1	první
genom	genom	k1gInSc1	genom
–	–	k?	–
RNA	RNA	kA	RNA
genom	genom	k1gInSc1	genom
bakteriofága	bakteriofága	k1gFnSc1	bakteriofága
MS	MS	kA	MS
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
Postupně	postupně	k6eAd1	postupně
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
RNA	RNA	kA	RNA
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
zpracování	zpracování	k1gNnSc4	zpracování
genetického	genetický	k2eAgInSc2d1	genetický
kódu	kód	k1gInSc2	kód
<g/>
,	,	kIx,	,
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
Carl	Carl	k1gInSc1	Carl
Woese	Woese	k1gFnSc2	Woese
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
hypotézu	hypotéza	k1gFnSc4	hypotéza
RNA	RNA	kA	RNA
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
byly	být	k5eAaImAgFnP	být
molekuly	molekula	k1gFnPc4	molekula
RNA	RNA	kA	RNA
původní	původní	k2eAgFnSc7d1	původní
nositelkou	nositelka	k1gFnSc7	nositelka
genetické	genetický	k2eAgFnSc2d1	genetická
informace	informace	k1gFnSc2	informace
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
zajišťovaly	zajišťovat	k5eAaImAgFnP	zajišťovat
potřebné	potřebný	k2eAgFnPc1d1	potřebná
katalytické	katalytický	k2eAgFnPc1d1	katalytická
funkce	funkce	k1gFnPc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
teorie	teorie	k1gFnSc1	teorie
podpořena	podpořit	k5eAaPmNgFnS	podpořit
popsáním	popsání	k1gNnSc7	popsání
reverzní	reverzní	k2eAgFnSc2d1	reverzní
transkriptázy	transkriptáza	k1gFnSc2	transkriptáza
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
přepisu	přepis	k1gInSc3	přepis
RNA	RNA	kA	RNA
do	do	k7c2	do
DNA	DNA	kA	DNA
retrovirům	retrovir	k1gMnPc3	retrovir
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
objevem	objev	k1gInSc7	objev
katalyticky	katalyticky	k6eAd1	katalyticky
aktivních	aktivní	k2eAgInPc2d1	aktivní
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
ribozymů	ribozym	k1gMnPc2	ribozym
<g/>
:	:	kIx,	:
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
ribozomu	ribozom	k1gInSc6	ribozom
a	a	k8xC	a
RNáze	RNáza	k1gFnSc3	RNáza
P	P	kA	P
(	(	kIx(	(
<g/>
zodpovědné	zodpovědný	k2eAgNnSc1d1	zodpovědné
za	za	k7c4	za
zrání	zrání	k1gNnSc4	zrání
tRNA	trnout	k5eAaImSgInS	trnout
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
katalytickém	katalytický	k2eAgInSc6d1	katalytický
centru	centr	k1gInSc6	centr
právě	právě	k9	právě
molekula	molekula	k1gFnSc1	molekula
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
toho	ten	k3xDgNnSc2	ten
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
další	další	k2eAgInSc1d1	další
výzkum	výzkum	k1gInSc1	výzkum
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
objeveny	objevit	k5eAaPmNgInP	objevit
nekódující	kódující	k2eNgInPc1d1	nekódující
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
ribozomální	ribozomální	k2eAgFnSc1d1	ribozomální
RNA	RNA	kA	RNA
sdílená	sdílený	k2eAgFnSc1d1	sdílená
všemi	všecek	k3xTgInPc7	všecek
živými	živý	k2eAgInPc7d1	živý
organismy	organismus	k1gInPc7	organismus
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
využívána	využívat	k5eAaImNgFnS	využívat
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
evoluční	evoluční	k2eAgFnSc2d1	evoluční
příbuznosti	příbuznost	k1gFnSc2	příbuznost
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
RNA	RNA	kA	RNA
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
syntéze	syntéza	k1gFnSc6	syntéza
složitě	složitě	k6eAd1	složitě
upravována	upravován	k2eAgFnSc1d1	upravována
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
sestřihem	sestřih	k1gInSc7	sestřih
nekódujících	kódující	k2eNgInPc2d1	nekódující
intronů	intron	k1gInPc2	intron
<g/>
,	,	kIx,	,
polyadenylací	polyadenylace	k1gFnPc2	polyadenylace
nebo	nebo	k8xC	nebo
například	například	k6eAd1	například
editací	editace	k1gFnSc7	editace
a	a	k8xC	a
regulována	regulovat	k5eAaImNgFnS	regulovat
procesem	proces	k1gInSc7	proces
RNA	RNA	kA	RNA
interference	interference	k1gFnSc1	interference
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
RNA	RNA	kA	RNA
pomohl	pomoct	k5eAaPmAgInS	pomoct
odhalit	odhalit	k5eAaPmF	odhalit
podstatu	podstata	k1gFnSc4	podstata
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
klíčových	klíčový	k2eAgInPc2d1	klíčový
biologických	biologický	k2eAgInPc2d1	biologický
procesů	proces	k1gInPc2	proces
a	a	k8xC	a
za	za	k7c4	za
studium	studium	k1gNnSc4	studium
RNA	RNA	kA	RNA
byla	být	k5eAaImAgFnS	být
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
udělena	udělit	k5eAaPmNgFnS	udělit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
lidem	lid	k1gInSc7	lid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
probíhá	probíhat	k5eAaImIp3nS	probíhat
bouřlivý	bouřlivý	k2eAgInSc4d1	bouřlivý
výzkum	výzkum	k1gInSc4	výzkum
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
polích	pole	k1gNnPc6	pole
biologie	biologie	k1gFnSc2	biologie
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Bádá	bádat	k5eAaImIp3nS	bádat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
propojení	propojení	k1gNnSc2	propojení
molekul	molekula	k1gFnPc2	molekula
RNA	RNA	kA	RNA
a	a	k8xC	a
epigenetických	epigenetický	k2eAgFnPc2d1	epigenetická
regulací	regulace	k1gFnPc2	regulace
či	či	k8xC	či
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
RNA	RNA	kA	RNA
interference	interference	k1gFnSc1	interference
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
ENCODE	ENCODE	kA	ENCODE
zase	zase	k9	zase
odhalil	odhalit	k5eAaPmAgInS	odhalit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zatímco	zatímco	k8xS	zatímco
pouze	pouze	k6eAd1	pouze
asi	asi	k9	asi
1,5	[number]	k4	1,5
%	%	kIx~	%
lidského	lidský	k2eAgInSc2d1	lidský
genomu	genom	k1gInSc2	genom
přímo	přímo	k6eAd1	přímo
kóduje	kódovat	k5eAaBmIp3nS	kódovat
proteiny	protein	k1gInPc4	protein
<g/>
,	,	kIx,	,
tak	tak	k9	tak
až	až	k9	až
80	[number]	k4	80
%	%	kIx~	%
genomu	genom	k1gInSc2	genom
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přepisována	přepisovat	k5eAaImNgFnS	přepisovat
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
léky	lék	k1gInPc1	lék
využívané	využívaný	k2eAgInPc1d1	využívaný
v	v	k7c6	v
léčbě	léčba	k1gFnSc6	léčba
jsou	být	k5eAaImIp3nP	být
založeny	založit	k5eAaPmNgInP	založit
na	na	k7c4	na
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
klinického	klinický	k2eAgNnSc2d1	klinické
testování	testování	k1gNnSc2	testování
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
50	[number]	k4	50
takových	takový	k3xDgInPc2	takový
potenciální	potenciální	k2eAgMnPc1d1	potenciální
léků	lék	k1gInPc2	lék
<g/>
.	.	kIx.	.
</s>
<s>
Schválen	schválen	k2eAgInSc1d1	schválen
byl	být	k5eAaImAgInS	být
zatím	zatím	k6eAd1	zatím
pouze	pouze	k6eAd1	pouze
RNA	RNA	kA	RNA
aptamer	aptamer	k1gMnSc1	aptamer
Pegaptanib	Pegaptanib	k1gMnSc1	Pegaptanib
využívaný	využívaný	k2eAgInSc4d1	využívaný
pro	pro	k7c4	pro
léčbu	léčba	k1gFnSc4	léčba
některých	některý	k3yIgInPc2	některý
typů	typ	k1gInPc2	typ
makulární	makulární	k2eAgFnSc2d1	makulární
degenerace	degenerace	k1gFnSc2	degenerace
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
v	v	k7c6	v
EU	EU	kA	EU
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
a	a	k8xC	a
léky	lék	k1gInPc1	lék
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
antisense	antisens	k1gInSc6	antisens
RNA	RNA	kA	RNA
<g/>
:	:	kIx,	:
Fomivirsen	Fomivirsen	k1gInSc1	Fomivirsen
<g/>
,	,	kIx,	,
antivirotikum	antivirotikum	k1gNnSc1	antivirotikum
proti	proti	k7c3	proti
lidskému	lidský	k2eAgNnSc3d1	lidské
cytomegaloviru	cytomegaloviro	k1gNnSc3	cytomegaloviro
<g/>
,	,	kIx,	,
a	a	k8xC	a
Mipomersen	Mipomersen	k1gInSc1	Mipomersen
proti	proti	k7c3	proti
familiární	familiární	k2eAgFnSc3d1	familiární
hypercholesterolemii	hypercholesterolemie	k1gFnSc3	hypercholesterolemie
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
především	především	k9	především
terapeutické	terapeutický	k2eAgNnSc1d1	terapeutické
využití	využití	k1gNnSc1	využití
RNA	RNA	kA	RNA
interference	interference	k1gFnSc2	interference
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
alternativu	alternativa	k1gFnSc4	alternativa
ke	k	k7c3	k
klasické	klasický	k2eAgFnSc3d1	klasická
genové	genový	k2eAgFnSc3d1	genová
terapii	terapie	k1gFnSc3	terapie
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc1	všechen
potenciální	potenciální	k2eAgInPc1d1	potenciální
léky	lék	k1gInPc1	lék
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
jsou	být	k5eAaImIp3nP	být
ale	ale	k8xC	ale
zatím	zatím	k6eAd1	zatím
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
klinického	klinický	k2eAgNnSc2d1	klinické
testování	testování	k1gNnSc2	testování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Chemická	chemický	k2eAgFnSc1d1	chemická
struktura	struktura	k1gFnSc1	struktura
===	===	k?	===
</s>
</p>
<p>
<s>
Ribonukleová	ribonukleový	k2eAgFnSc1d1	ribonukleová
kyselina	kyselina	k1gFnSc1	kyselina
je	být	k5eAaImIp3nS	být
biopolymer	biopolymer	k1gInSc4	biopolymer
tvořený	tvořený	k2eAgInSc4d1	tvořený
ribonukleotidy	ribonukleotid	k1gInPc4	ribonukleotid
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
látky	látka	k1gFnPc4	látka
složené	složený	k2eAgFnSc2d1	složená
z	z	k7c2	z
nukleové	nukleový	k2eAgFnSc2d1	nukleová
báze	báze	k1gFnSc2	báze
(	(	kIx(	(
<g/>
adenin	adenin	k1gInSc1	adenin
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
guanin	guanin	k1gInSc1	guanin
(	(	kIx(	(
<g/>
G	G	kA	G
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cytosin	cytosin	k1gMnSc1	cytosin
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
uracil	uracil	k1gMnSc1	uracil
(	(	kIx(	(
<g/>
U	U	kA	U
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
pětiuhlíkatého	pětiuhlíkatý	k2eAgInSc2d1	pětiuhlíkatý
monosacharidu	monosacharid	k1gInSc2	monosacharid
ribózy	ribóza	k1gFnSc2	ribóza
a	a	k8xC	a
jednoho	jeden	k4xCgInSc2	jeden
zbytku	zbytek	k1gInSc2	zbytek
kyseliny	kyselina	k1gFnSc2	kyselina
fosforečné	fosforečný	k2eAgFnSc2d1	fosforečná
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
nukleová	nukleový	k2eAgFnSc1d1	nukleová
báze	báze	k1gFnSc1	báze
napojená	napojený	k2eAgFnSc1d1	napojená
na	na	k7c4	na
ribózu	ribóza	k1gFnSc4	ribóza
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ribonukleosid	ribonukleosid	k1gInSc1	ribonukleosid
<g/>
,	,	kIx,	,
až	až	k8xS	až
jeho	jeho	k3xOp3gFnSc1	jeho
fosforylovaná	fosforylovaný	k2eAgFnSc1d1	fosforylovaný
forma	forma	k1gFnSc1	forma
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
RNA	RNA	kA	RNA
je	být	k5eAaImIp3nS	být
ribonukleotid	ribonukleotid	k1gInSc1	ribonukleotid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kostře	kostra	k1gFnSc6	kostra
RNA	RNA	kA	RNA
se	se	k3xPyFc4	se
střídá	střídat	k5eAaImIp3nS	střídat
ribóza	ribóza	k1gFnSc1	ribóza
s	s	k7c7	s
fosfátovou	fosfátový	k2eAgFnSc7d1	fosfátová
skupinou	skupina	k1gFnSc7	skupina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vzájemně	vzájemně	k6eAd1	vzájemně
kovalentně	kovalentně	k6eAd1	kovalentně
propojené	propojený	k2eAgNnSc1d1	propojené
fosfodiesterovou	fosfodiesterův	k2eAgFnSc7d1	fosfodiesterův
vazbou	vazba	k1gFnSc7	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ribózu	ribóza	k1gFnSc4	ribóza
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
1	[number]	k4	1
<g/>
'	'	kIx"	'
připojeny	připojit	k5eAaPmNgFnP	připojit
glykosidickou	glykosidický	k2eAgFnSc7d1	glykosidická
vazbou	vazba	k1gFnSc7	vazba
nukleové	nukleový	k2eAgFnSc2d1	nukleová
báze	báze	k1gFnSc2	báze
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
představují	představovat	k5eAaImIp3nP	představovat
vlastní	vlastní	k2eAgFnPc4d1	vlastní
jednotky	jednotka	k1gFnPc4	jednotka
dědičnosti	dědičnost	k1gFnSc2	dědičnost
a	a	k8xC	a
právě	právě	k9	právě
pomocí	pomocí	k7c2	pomocí
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
RNA	RNA	kA	RNA
zapsána	zapsán	k2eAgFnSc1d1	zapsána
informace	informace	k1gFnSc1	informace
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
tři	tři	k4xCgFnPc4	tři
báze	báze	k1gFnPc4	báze
jsou	být	k5eAaImIp3nP	být
totožné	totožný	k2eAgInPc1d1	totožný
s	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
v	v	k7c6	v
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
thymin	thymin	k1gInSc1	thymin
přítomný	přítomný	k2eAgInSc1d1	přítomný
v	v	k7c6	v
DNA	DNA	kA	DNA
je	být	k5eAaImIp3nS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
uracilem	uracil	k1gMnSc7	uracil
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
thyminu	thymin	k1gInSc2	thymin
liší	lišit	k5eAaImIp3nP	lišit
nepřítomností	nepřítomnost	k1gFnSc7	nepřítomnost
methylové	methylový	k2eAgFnSc2d1	methylová
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
významným	významný	k2eAgInSc7d1	významný
rozdílem	rozdíl	k1gInSc7	rozdíl
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ribóza	ribóza	k1gFnSc1	ribóza
nesená	nesený	k2eAgFnSc1d1	nesená
v	v	k7c6	v
RNA	RNA	kA	RNA
má	mít	k5eAaImIp3nS	mít
oproti	oproti	k7c3	oproti
deoxyribóze	deoxyribóza	k1gFnSc3	deoxyribóza
tvořící	tvořící	k2eAgFnSc1d1	tvořící
DNA	dna	k1gFnSc1	dna
na	na	k7c6	na
pozici	pozice	k1gFnSc6	pozice
2	[number]	k4	2
<g/>
'	'	kIx"	'
hydroxylovou	hydroxylový	k2eAgFnSc7d1	hydroxylová
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
RNA	RNA	kA	RNA
je	být	k5eAaImIp3nS	být
kvůli	kvůli	k7c3	kvůli
volné	volný	k2eAgNnSc4d1	volné
2	[number]	k4	2
<g/>
'	'	kIx"	'
OH	OH	kA	OH
skupině	skupina	k1gFnSc6	skupina
výrazně	výrazně	k6eAd1	výrazně
reaktivnější	reaktivní	k2eAgFnPc1d2	reaktivnější
a	a	k8xC	a
flexibilnější	flexibilní	k2eAgFnPc1d2	flexibilnější
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
nestabilnější	stabilní	k2eNgMnSc1d2	nestabilnější
a	a	k8xC	a
v	v	k7c6	v
zásaditém	zásaditý	k2eAgNnSc6d1	zásadité
prostředí	prostředí	k1gNnSc6	prostředí
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
hydrolýze	hydrolýza	k1gFnSc3	hydrolýza
fosfodiesterové	fosfodiesterový	k2eAgFnSc2d1	fosfodiesterový
vazby	vazba	k1gFnSc2	vazba
blízkou	blízký	k2eAgFnSc7d1	blízká
-OH	-OH	k?	-OH
skupinou	skupina	k1gFnSc7	skupina
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
rozštěpení	rozštěpení	k1gNnSc4	rozštěpení
kostry	kostra	k1gFnSc2	kostra
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
RNA	RNA	kA	RNA
citlivější	citlivý	k2eAgFnSc1d2	citlivější
k	k	k7c3	k
degradaci	degradace	k1gFnSc3	degradace
řadou	řada	k1gFnSc7	řada
enzymů	enzym	k1gInPc2	enzym
<g/>
.	.	kIx.	.
</s>
<s>
Reaktivita	reaktivita	k1gFnSc1	reaktivita
RNA	RNA	kA	RNA
jí	jíst	k5eAaImIp3nS	jíst
ovšem	ovšem	k9	ovšem
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
v	v	k7c6	v
mnohých	mnohý	k2eAgInPc6d1	mnohý
případech	případ	k1gInPc6	případ
působit	působit	k5eAaImF	působit
jako	jako	k8xC	jako
katalyzátor	katalyzátor	k1gInSc4	katalyzátor
chemických	chemický	k2eAgFnPc2d1	chemická
reakcí	reakce	k1gFnPc2	reakce
–	–	k?	–
takovým	takový	k3xDgFnPc3	takový
molekulám	molekula	k1gFnPc3	molekula
RNA	RNA	kA	RNA
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
ribozymy	ribozymum	k1gNnPc7	ribozymum
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
v	v	k7c6	v
drtivé	drtivý	k2eAgFnSc6d1	drtivá
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
tzv.	tzv.	kA	tzv.
dvoušroubovici	dvoušroubovice	k1gFnSc6	dvoušroubovice
(	(	kIx(	(
<g/>
složenou	složený	k2eAgFnSc4d1	složená
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
komplementárních	komplementární	k2eAgNnPc2d1	komplementární
vláken	vlákno	k1gNnPc2	vlákno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
RNA	RNA	kA	RNA
většinou	většinou	k6eAd1	většinou
kratší	krátký	k2eAgNnPc1d2	kratší
jednoduchá	jednoduchý	k2eAgNnPc1d1	jednoduché
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
jednovláknová	jednovláknový	k2eAgNnPc1d1	jednovláknový
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
i	i	k9	i
dvouvláknová	dvouvláknový	k2eAgNnPc4d1	dvouvláknový
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
dvoušroubovici	dvoušroubovice	k1gFnSc4	dvoušroubovice
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
niž	jenž	k3xRgFnSc4	jenž
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
prostorové	prostorový	k2eAgNnSc1d1	prostorové
uspořádání	uspořádání	k1gNnSc1	uspořádání
A-formy	Aorma	k1gFnSc2	A-forma
<g/>
.	.	kIx.	.
</s>
<s>
RNA	RNA	kA	RNA
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
flexibilitě	flexibilita	k1gFnSc3	flexibilita
schopna	schopen	k2eAgFnSc1d1	schopna
vytvářet	vytvářet	k5eAaImF	vytvářet
bohaté	bohatý	k2eAgInPc4d1	bohatý
sekundární	sekundární	k2eAgInPc4d1	sekundární
a	a	k8xC	a
terciární	terciární	k2eAgFnPc4d1	terciární
struktury	struktura	k1gFnPc4	struktura
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
variabilita	variabilita	k1gFnSc1	variabilita
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
DNA	DNA	kA	DNA
výrazně	výrazně	k6eAd1	výrazně
bohatší	bohatý	k2eAgFnSc1d2	bohatší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Modifikace	modifikace	k1gFnSc2	modifikace
RNA	RNA	kA	RNA
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
RNA	RNA	kA	RNA
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
translaci	translace	k1gFnSc6	translace
vneseny	vnést	k5eAaPmNgInP	vnést
pouze	pouze	k6eAd1	pouze
čtyři	čtyři	k4xCgFnPc4	čtyři
základní	základní	k2eAgFnPc4d1	základní
báze	báze	k1gFnPc4	báze
<g/>
,	,	kIx,	,
následnou	následný	k2eAgFnSc7d1	následná
modifikací	modifikace	k1gFnSc7	modifikace
báze	báze	k1gFnSc2	báze
nebo	nebo	k8xC	nebo
ribózy	ribóza	k1gFnSc2	ribóza
ale	ale	k8xC	ale
vzniká	vznikat	k5eAaImIp3nS	vznikat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
ribonukleotidů	ribonukleotid	k1gInPc2	ribonukleotid
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
variabilitu	variabilita	k1gFnSc4	variabilita
molekul	molekula	k1gFnPc2	molekula
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
modifikovaných	modifikovaný	k2eAgInPc2d1	modifikovaný
ribonukleotidů	ribonukleotid	k1gInPc2	ribonukleotid
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejčastější	častý	k2eAgFnSc7d3	nejčastější
modifikací	modifikace	k1gFnSc7	modifikace
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
<g/>
'	'	kIx"	'
<g/>
-O-methylace	-Oethylace	k1gFnSc2	-O-methylace
ribózy	ribóza	k1gFnSc2	ribóza
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
nezbytná	nezbytný	k2eAgFnSc1d1	nezbytná
pro	pro	k7c4	pro
funkci	funkce	k1gFnSc4	funkce
ribozomu	ribozom	k1gInSc2	ribozom
a	a	k8xC	a
spliceosomu	spliceosom	k1gInSc2	spliceosom
<g/>
,	,	kIx,	,
a	a	k8xC	a
nejvíce	hodně	k6eAd3	hodně
různých	různý	k2eAgFnPc2d1	různá
modifikací	modifikace	k1gFnPc2	modifikace
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
nalézt	nalézt	k5eAaPmF	nalézt
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
tRNA	trnout	k5eAaImSgMnS	trnout
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejčastější	častý	k2eAgInPc4d3	nejčastější
příklady	příklad	k1gInPc4	příklad
nestandardních	standardní	k2eNgFnPc2d1	nestandardní
složek	složka	k1gFnPc2	složka
RNA	RNA	kA	RNA
patří	patřit	k5eAaImIp3nS	patřit
dihydrouridin	dihydrouridin	k1gInSc1	dihydrouridin
(	(	kIx(	(
<g/>
D	D	kA	D
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
destabilizační	destabilizační	k2eAgInSc1d1	destabilizační
efekt	efekt	k1gInSc1	efekt
na	na	k7c4	na
strukturu	struktura	k1gFnSc4	struktura
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
pseudouridin	pseudouridin	k1gInSc1	pseudouridin
(	(	kIx(	(
<g/>
Ψ	Ψ	k?	Ψ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
uridin	uridin	k2eAgInSc1d1	uridin
připojený	připojený	k2eAgInSc1d1	připojený
na	na	k7c4	na
ribózu	ribóza	k1gFnSc4	ribóza
neobvykle	obvykle	k6eNd1	obvykle
na	na	k7c4	na
své	své	k1gNnSc4	své
5	[number]	k4	5
<g/>
'	'	kIx"	'
pozici	pozice	k1gFnSc6	pozice
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
role	role	k1gFnSc1	role
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
stabilizace	stabilizace	k1gFnSc1	stabilizace
struktury	struktura	k1gFnSc2	struktura
RNA	RNA	kA	RNA
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
schopnosti	schopnost	k1gFnSc3	schopnost
tvořit	tvořit	k5eAaImF	tvořit
navíc	navíc	k6eAd1	navíc
další	další	k2eAgInSc4d1	další
vodíkový	vodíkový	k2eAgInSc4d1	vodíkový
můstek	můstek	k1gInSc4	můstek
a	a	k8xC	a
silnější	silný	k2eAgFnPc4d2	silnější
patrové	patrový	k2eAgFnPc4d1	patrová
interakce	interakce	k1gFnPc4	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Inosin	Inosin	k1gMnSc1	Inosin
(	(	kIx(	(
<g/>
I	I	kA	I
<g/>
)	)	kIx)	)
v	v	k7c6	v
tRNA	trnout	k5eAaImSgInS	trnout
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
i	i	k9	i
nestandardní	standardní	k2eNgMnSc1d1	nestandardní
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
kolísavé	kolísavý	k2eAgNnSc1d1	kolísavé
párování	párování	k1gNnSc1	párování
bází	báze	k1gFnPc2	báze
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaImNgInS	využívat
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
sekvence	sekvence	k1gFnSc1	sekvence
mRNA	mRNA	k?	mRNA
při	při	k7c6	při
procesu	proces	k1gInSc6	proces
editace	editace	k1gFnSc2	editace
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
RNA	RNA	kA	RNA
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
často	často	k6eAd1	často
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
5	[number]	k4	5
<g/>
-methylcytidin	ethylcytidin	k1gInSc1	-methylcytidin
(	(	kIx(	(
<g/>
m	m	kA	m
<g/>
5	[number]	k4	5
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
tRNA	trnout	k5eAaImSgInS	trnout
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
funkčnost	funkčnost	k1gFnSc4	funkčnost
a	a	k8xC	a
v	v	k7c6	v
rRNA	rRNA	k?	rRNA
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
přesnost	přesnost	k1gFnSc1	přesnost
translace	translace	k1gFnSc1	translace
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
funkce	funkce	k1gFnSc1	funkce
v	v	k7c6	v
RNA	RNA	kA	RNA
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
prozkoumaná	prozkoumaný	k2eAgFnSc1d1	prozkoumaná
(	(	kIx(	(
<g/>
naopak	naopak	k6eAd1	naopak
její	její	k3xOp3gInSc4	její
výskyt	výskyt	k1gInSc4	výskyt
v	v	k7c6	v
DNA	DNA	kA	DNA
je	být	k5eAaImIp3nS	být
významný	významný	k2eAgInSc1d1	významný
epigenetický	epigenetický	k2eAgInSc1d1	epigenetický
znak	znak	k1gInSc1	znak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
thymin	thymin	k1gInSc1	thymin
nebývá	bývat	k5eNaImIp3nS	bývat
do	do	k7c2	do
RNA	RNA	kA	RNA
zařazen	zařazen	k2eAgInSc1d1	zařazen
přímo	přímo	k6eAd1	přímo
při	při	k7c6	při
její	její	k3xOp3gFnSc6	její
syntéze	syntéza	k1gFnSc6	syntéza
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
v	v	k7c6	v
takzvaném	takzvaný	k2eAgMnSc6d1	takzvaný
TΨ	TΨ	k1gMnSc6	TΨ
rameni	rameno	k1gNnSc3	rameno
tRNA	trnout	k5eAaImSgInS	trnout
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
RNA	RNA	kA	RNA
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
tento	tento	k3xDgInSc1	tento
nukleosid	nukleosid	k1gInSc1	nukleosid
kvůli	kvůli	k7c3	kvůli
jednoznačnosti	jednoznačnost	k1gFnSc3	jednoznačnost
nazývá	nazývat	k5eAaImIp3nS	nazývat
ribothymidin	ribothymidin	k2eAgInSc1d1	ribothymidin
nebo	nebo	k8xC	nebo
5	[number]	k4	5
<g/>
-methyluridin	ethyluridin	k2eAgInSc4d1	-methyluridin
a	a	k8xC	a
ne	ne	k9	ne
thymidin	thymidin	k1gInSc1	thymidin
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
modifikací	modifikace	k1gFnSc7	modifikace
je	být	k5eAaImIp3nS	být
7	[number]	k4	7
<g/>
-methylguanosin	ethylguanosina	k1gFnPc2	-methylguanosina
(	(	kIx(	(
<g/>
m	m	kA	m
<g/>
7	[number]	k4	7
<g/>
G	G	kA	G
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nukleosid	nukleosid	k1gInSc1	nukleosid
tvořící	tvořící	k2eAgFnSc4d1	tvořící
neobvyklou	obvyklý	k2eNgFnSc4d1	neobvyklá
strukturu	struktura	k1gFnSc4	struktura
5	[number]	k4	5
<g/>
'	'	kIx"	'
čepičky	čepička	k1gFnPc1	čepička
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
chrání	chránit	k5eAaImIp3nS	chránit
5	[number]	k4	5
<g/>
'	'	kIx"	'
konec	konec	k1gInSc1	konec
eukaryotních	eukaryotní	k2eAgFnPc2d1	eukaryotní
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-methylguanosin	ethylguanosina	k1gFnPc2	-methylguanosina
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
připojen	připojit	k5eAaPmNgInS	připojit
k	k	k7c3	k
následujícímu	následující	k2eAgInSc3d1	následující
ribonukleotidu	ribonukleotid	k1gInSc3	ribonukleotid
neobvyklou	obvyklý	k2eNgFnSc4d1	neobvyklá
5	[number]	k4	5
<g/>
'	'	kIx"	'
–	–	k?	–
5	[number]	k4	5
<g/>
'	'	kIx"	'
trifosfátovou	trifosfátův	k2eAgFnSc7d1	trifosfátův
vazbou	vazba	k1gFnSc7	vazba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Strukturní	strukturní	k2eAgInPc4d1	strukturní
motivy	motiv	k1gInPc4	motiv
v	v	k7c6	v
RNA	RNA	kA	RNA
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Základní	základní	k2eAgInPc1d1	základní
strukturní	strukturní	k2eAgInPc1d1	strukturní
motivy	motiv	k1gInPc1	motiv
====	====	k?	====
</s>
</p>
<p>
<s>
Funkce	funkce	k1gFnSc1	funkce
RNA	RNA	kA	RNA
není	být	k5eNaImIp3nS	být
určena	určen	k2eAgFnSc1d1	určena
pouze	pouze	k6eAd1	pouze
pořadím	pořadí	k1gNnSc7	pořadí
jejích	její	k3xOp3gFnPc2	její
bází	báze	k1gFnPc2	báze
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
proteinů	protein	k1gInPc2	protein
i	i	k8xC	i
její	její	k3xOp3gFnSc7	její
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
RNA	RNA	kA	RNA
dokáže	dokázat	k5eAaPmIp3nS	dokázat
pomocí	pomocí	k7c2	pomocí
párování	párování	k1gNnSc2	párování
bází	báze	k1gFnPc2	báze
a	a	k8xC	a
patrových	patrový	k2eAgFnPc2d1	patrová
interakcí	interakce	k1gFnPc2	interakce
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svého	svůj	k3xOyFgInSc2	svůj
řetězce	řetězec	k1gInSc2	řetězec
vytvářet	vytvářet	k5eAaImF	vytvářet
stejné	stejný	k2eAgInPc4d1	stejný
typy	typ	k1gInPc4	typ
struktur	struktura	k1gFnPc2	struktura
jako	jako	k8xS	jako
DNA	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
flexibilitě	flexibilita	k1gFnSc3	flexibilita
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
glykosidické	glykosidický	k2eAgFnSc2d1	glykosidická
vazby	vazba	k1gFnSc2	vazba
a	a	k8xC	a
schopnosti	schopnost	k1gFnSc2	schopnost
vytvářet	vytvářet	k5eAaImF	vytvářet
další	další	k2eAgInSc4d1	další
vodíkový	vodíkový	k2eAgInSc4d1	vodíkový
můstek	můstek	k1gInSc4	můstek
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
zaujímat	zaujímat	k5eAaImF	zaujímat
i	i	k9	i
mnohem	mnohem	k6eAd1	mnohem
složitější	složitý	k2eAgNnSc4d2	složitější
uspořádání	uspořádání	k1gNnSc4	uspořádání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejčastější	častý	k2eAgInPc4d3	nejčastější
typy	typ	k1gInPc4	typ
prostorového	prostorový	k2eAgNnSc2d1	prostorové
uspořádání	uspořádání	k1gNnSc2	uspořádání
RNA	RNA	kA	RNA
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
dvoušroubovice	dvoušroubovice	k1gFnSc1	dvoušroubovice
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
RNA	RNA	kA	RNA
ovšem	ovšem	k9	ovšem
pouze	pouze	k6eAd1	pouze
tzv.	tzv.	kA	tzv.
A-formy	Aorm	k1gInPc1	A-form
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
B-formou	Borma	k1gFnSc7	B-forma
běžnou	běžný	k2eAgFnSc7d1	běžná
v	v	k7c6	v
DNA	DNA	kA	DNA
širší	široký	k2eAgFnSc1d2	širší
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
více	hodně	k6eAd2	hodně
plochý	plochý	k2eAgInSc4d1	plochý
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
hlubší	hluboký	k2eAgInSc4d2	hlubší
velký	velký	k2eAgInSc4d1	velký
žlábek	žlábek	k1gInSc4	žlábek
a	a	k8xC	a
méně	málo	k6eAd2	málo
hluboký	hluboký	k2eAgInSc1d1	hluboký
malý	malý	k2eAgInSc1d1	malý
žlábek	žlábek	k1gInSc1	žlábek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
vlásenka	vlásenka	k1gFnSc1	vlásenka
</s>
</p>
<p>
<s>
vyboulená	vyboulený	k2eAgFnSc1d1	vyboulená
smyčka	smyčka	k1gFnSc1	smyčka
vznikající	vznikající	k2eAgFnSc1d1	vznikající
ve	v	k7c6	v
dvoušroubovici	dvoušroubovice	k1gFnSc6	dvoušroubovice
při	při	k7c6	při
lokálním	lokální	k2eAgNnSc6d1	lokální
nedokonalém	dokonalý	k2eNgNnSc6d1	nedokonalé
párování	párování	k1gNnSc6	párování
bází	báze	k1gFnPc2	báze
</s>
</p>
<p>
<s>
pseudouzel	pseudouzet	k5eAaImAgInS	pseudouzet
vytvářející	vytvářející	k2eAgInSc1d1	vytvářející
několik	několik	k4yIc1	několik
interakcí	interakce	k1gFnPc2	interakce
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jedné	jeden	k4xCgFnSc2	jeden
molekuly	molekula	k1gFnSc2	molekula
a	a	k8xC	a
kissing	kissing	k1gInSc1	kissing
loops	loopsa	k1gFnPc2	loopsa
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
líbající	líbající	k2eAgFnSc2d1	líbající
se	se	k3xPyFc4	se
smyčky	smyčka	k1gFnSc2	smyčka
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
vznikající	vznikající	k2eAgInSc1d1	vznikající
mezi	mezi	k7c7	mezi
různými	různý	k2eAgFnPc7d1	různá
molekulami	molekula	k1gFnPc7	molekula
</s>
</p>
<p>
<s>
trojšroubovice	trojšroubovice	k1gFnSc1	trojšroubovice
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
do	do	k7c2	do
klasické	klasický	k2eAgFnSc2d1	klasická
dvoušroubovice	dvoušroubovice	k1gFnSc2	dvoušroubovice
připojí	připojit	k5eAaPmIp3nS	připojit
další	další	k2eAgNnSc4d1	další
vlákno	vlákno	k1gNnSc4	vlákno
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
do	do	k7c2	do
malého	malý	k2eAgInSc2d1	malý
žlábku	žlábek	k1gInSc2	žlábek
</s>
</p>
<p>
<s>
G-kvadruplex	Gvadruplex	k1gInSc1	G-kvadruplex
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
DNAHlavní	DNAHlavní	k2eAgInPc4d1	DNAHlavní
typy	typ	k1gInPc4	typ
struktur	struktura	k1gFnPc2	struktura
v	v	k7c6	v
RNA	RNA	kA	RNA
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Složitější	složitý	k2eAgFnPc1d2	složitější
struktury	struktura	k1gFnPc1	struktura
====	====	k?	====
</s>
</p>
<p>
<s>
Složitější	složitý	k2eAgInPc1d2	složitější
typy	typ	k1gInPc1	typ
struktur	struktura	k1gFnPc2	struktura
se	se	k3xPyFc4	se
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
jak	jak	k6eAd1	jak
pomocí	pomocí	k7c2	pomocí
párování	párování	k1gNnPc2	párování
bází	báze	k1gFnPc2	báze
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
částmi	část	k1gFnPc7	část
molekuly	molekula	k1gFnSc2	molekula
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k8xC	i
využíváním	využívání	k1gNnSc7	využívání
dalších	další	k2eAgFnPc2d1	další
interakcí	interakce	k1gFnPc2	interakce
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
patrových	patrový	k2eAgFnPc2d1	patrová
interakcí	interakce	k1gFnPc2	interakce
mezi	mezi	k7c7	mezi
ribonukleotidy	ribonukleotid	k1gInPc7	ribonukleotid
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
částech	část	k1gFnPc6	část
molekuly	molekula	k1gFnSc2	molekula
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
koaxiální	koaxiální	k2eAgNnSc1d1	koaxiální
skládání	skládání	k1gNnSc1	skládání
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
využívání	využívání	k1gNnSc1	využívání
kovových	kovový	k2eAgInPc2d1	kovový
iontů	ion	k1gInPc2	ion
pro	pro	k7c4	pro
stabilizaci	stabilizace	k1gFnSc4	stabilizace
řetězce	řetězec	k1gInSc2	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
RNA	RNA	kA	RNA
je	být	k5eAaImIp3nS	být
také	také	k9	také
schopná	schopný	k2eAgFnSc1d1	schopná
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
větším	veliký	k2eAgInSc6d2	veliký
rozsahu	rozsah	k1gInSc6	rozsah
než	než	k8xS	než
DNA	dno	k1gNnPc4	dno
<g/>
,	,	kIx,	,
využívat	využívat	k5eAaPmF	využívat
nestandardní	standardní	k2eNgInPc4d1	nestandardní
typy	typ	k1gInPc4	typ
párování	párování	k1gNnSc2	párování
bází	báze	k1gFnPc2	báze
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
hoogsteenovské	hoogsteenovský	k2eAgNnSc4d1	hoogsteenovský
párování	párování	k1gNnSc4	párování
nebo	nebo	k8xC	nebo
kolísavé	kolísavý	k2eAgNnSc4d1	kolísavé
párování	párování	k1gNnSc4	párování
bází	báze	k1gFnPc2	báze
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
uspořádaných	uspořádaný	k2eAgFnPc2d1	uspořádaná
oblastí	oblast	k1gFnPc2	oblast
mají	mít	k5eAaImIp3nP	mít
pro	pro	k7c4	pro
funkci	funkce	k1gFnSc4	funkce
RNA	RNA	kA	RNA
význam	význam	k1gInSc4	význam
i	i	k8xC	i
oblasti	oblast	k1gFnPc4	oblast
bez	bez	k7c2	bez
stabilní	stabilní	k2eAgFnSc2d1	stabilní
struktury	struktura	k1gFnSc2	struktura
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
neuspořádaných	uspořádaný	k2eNgFnPc2d1	neuspořádaná
oblastí	oblast	k1gFnPc2	oblast
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Neuspořádané	uspořádaný	k2eNgFnPc1d1	neuspořádaná
oblasti	oblast	k1gFnPc1	oblast
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
místem	místo	k1gNnSc7	místo
vazby	vazba	k1gFnSc2	vazba
RNA-vazebných	RNAazebný	k2eAgInPc2d1	RNA-vazebný
proteinů	protein	k1gInPc2	protein
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgInPc2d1	jiný
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
při	při	k7c6	při
vazbě	vazba	k1gFnSc6	vazba
mezi	mezi	k7c7	mezi
tRNA	trnout	k5eAaImSgInS	trnout
a	a	k8xC	a
mRNA	mRNA	k?	mRNA
<g/>
)	)	kIx)	)
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
preferovaným	preferovaný	k2eAgInSc7d1	preferovaný
cílem	cíl	k1gInSc7	cíl
RNA	RNA	kA	RNA
interference	interference	k1gFnSc1	interference
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
molekulou	molekula	k1gFnSc7	molekula
RNA	RNA	kA	RNA
se	s	k7c7	s
zjištěným	zjištěný	k2eAgNnSc7d1	zjištěné
prostorovým	prostorový	k2eAgNnSc7d1	prostorové
uspořádáním	uspořádání	k1gNnSc7	uspořádání
byla	být	k5eAaImAgFnS	být
tRNA	trnout	k5eAaImSgInS	trnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
za	za	k7c4	za
určení	určení	k1gNnSc4	určení
přesné	přesný	k2eAgFnSc2d1	přesná
prostorové	prostorový	k2eAgFnSc2d1	prostorová
struktury	struktura	k1gFnSc2	struktura
ribozomu	ribozom	k1gInSc2	ribozom
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
udělena	udělen	k2eAgFnSc1d1	udělena
Nobelova	Nobelův	k2eAgFnSc1d1	Nobelova
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Funkce	funkce	k1gFnPc4	funkce
==	==	k?	==
</s>
</p>
<p>
<s>
Ribonukleová	ribonukleový	k2eAgFnSc1d1	ribonukleová
kyselina	kyselina	k1gFnSc1	kyselina
plní	plnit	k5eAaImIp3nS	plnit
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
mnoho	mnoho	k4c4	mnoho
možných	možný	k2eAgFnPc2d1	možná
úloh	úloha	k1gFnPc2	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
například	například	k6eAd1	například
nést	nést	k5eAaImF	nést
informaci	informace	k1gFnSc4	informace
o	o	k7c6	o
stavbě	stavba	k1gFnSc6	stavba
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
v	v	k7c6	v
případě	případ	k1gInSc6	případ
mRNA	mRNA	k?	mRNA
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
RNA	RNA	kA	RNA
přítomná	přítomný	k2eAgFnSc1d1	přítomná
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
98	[number]	k4	98
%	%	kIx~	%
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
tzv.	tzv.	kA	tzv.
nekódující	kódující	k2eNgNnSc1d1	kódující
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
naprosto	naprosto	k6eAd1	naprosto
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
roli	role	k1gFnSc4	role
<g/>
.	.	kIx.	.
</s>
<s>
RNA	RNA	kA	RNA
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
bílkovinné	bílkovinný	k2eAgInPc4d1	bílkovinný
enzymy	enzym	k1gInPc4	enzym
<g/>
,	,	kIx,	,
plnit	plnit	k5eAaImF	plnit
katalytickou	katalytický	k2eAgFnSc4d1	katalytická
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
autokatalytickou	autokatalytický	k2eAgFnSc4d1	autokatalytická
<g/>
)	)	kIx)	)
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
taková	takový	k3xDgFnSc1	takový
enzymaticky	enzymaticky	k6eAd1	enzymaticky
fungující	fungující	k2eAgFnSc1d1	fungující
RNA	RNA	kA	RNA
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
ribozym	ribozym	k1gInSc1	ribozym
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
RNA	RNA	kA	RNA
viry	vira	k1gFnSc2	vira
slouží	sloužit	k5eAaImIp3nS	sloužit
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k6eAd1	už
jednovláknová	jednovláknový	k2eAgFnSc1d1	jednovláknová
nebo	nebo	k8xC	nebo
dvouvláknová	dvouvláknový	k2eAgFnSc1d1	dvouvláknová
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
uložení	uložení	k1gNnSc4	uložení
vlastní	vlastní	k2eAgNnSc4d1	vlastní
genetické	genetický	k2eAgFnPc4d1	genetická
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Čtení	čtení	k1gNnSc6	čtení
genetického	genetický	k2eAgInSc2d1	genetický
kódu	kód	k1gInSc2	kód
===	===	k?	===
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc4	informace
potřebné	potřebný	k2eAgFnPc4d1	potřebná
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
proteinů	protein	k1gInPc2	protein
živými	živý	k2eAgInPc7d1	živý
organismy	organismus	k1gInPc7	organismus
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
uloženy	uložit	k5eAaPmNgFnP	uložit
na	na	k7c4	na
DNA	dno	k1gNnPc4	dno
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
genů	gen	k1gInPc2	gen
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
kódují	kódovat	k5eAaBmIp3nP	kódovat
molekulu	molekula	k1gFnSc4	molekula
mRNA	mRNA	k?	mRNA
sloužící	sloužící	k1gMnSc1	sloužící
jako	jako	k8xS	jako
prostředník	prostředník	k1gMnSc1	prostředník
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
překladu	překlad	k1gInSc2	překlad
mRNA	mRNA	k?	mRNA
do	do	k7c2	do
proteinů	protein	k1gInPc2	protein
probíhá	probíhat	k5eAaImIp3nS	probíhat
podle	podle	k7c2	podle
souboru	soubor	k1gInSc2	soubor
pravidel	pravidlo	k1gNnPc2	pravidlo
nazývaných	nazývaný	k2eAgInPc2d1	nazývaný
genetický	genetický	k2eAgInSc4d1	genetický
kód	kód	k1gInSc4	kód
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
uváděna	uvádět	k5eAaImNgFnS	uvádět
do	do	k7c2	do
praxe	praxe	k1gFnSc2	praxe
právě	právě	k6eAd1	právě
molekulami	molekula	k1gFnPc7	molekula
RNA	RNA	kA	RNA
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Mediátorová	Mediátorový	k2eAgFnSc1d1	Mediátorový
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
z	z	k7c2	z
angl.	angl.	k?	angl.
messenger	messengra	k1gFnPc2	messengra
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přepisována	přepisovat	k5eAaImNgFnS	přepisovat
podle	podle	k7c2	podle
sekvence	sekvence	k1gFnSc2	sekvence
DNA	DNA	kA	DNA
(	(	kIx(	(
<g/>
u	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
exportována	exportovat	k5eAaBmNgFnS	exportovat
do	do	k7c2	do
cytoplazmy	cytoplazma	k1gFnSc2	cytoplazma
<g/>
)	)	kIx)	)
a	a	k8xC	a
využita	využít	k5eAaPmNgFnS	využít
pro	pro	k7c4	pro
překlad	překlad	k1gInSc4	překlad
do	do	k7c2	do
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
mRNA	mRNA	k?	mRNA
je	být	k5eAaImIp3nS	být
nejdříve	dříve	k6eAd3	dříve
přepisována	přepisován	k2eAgFnSc1d1	přepisována
jako	jako	k8xS	jako
prekurzor	prekurzor	k1gInSc4	prekurzor
(	(	kIx(	(
<g/>
hnRNA	hnRNA	k?	hnRNA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
často	často	k6eAd1	často
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nekódující	kódující	k2eNgFnPc4d1	nekódující
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
introny	intron	k1gInPc4	intron
<g/>
.	.	kIx.	.
</s>
<s>
Zralá	zralý	k2eAgFnSc1d1	zralá
mRNA	mRNA	k?	mRNA
stále	stále	k6eAd1	stále
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nepřekládané	překládaný	k2eNgFnPc4d1	nepřekládaná
oblasti	oblast	k1gFnPc4	oblast
s	s	k7c7	s
regulačními	regulační	k2eAgFnPc7d1	regulační
funkcemi	funkce	k1gFnPc7	funkce
a	a	k8xC	a
u	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
opatřena	opatřen	k2eAgFnSc1d1	opatřena
čepičkou	čepička	k1gFnSc7	čepička
a	a	k8xC	a
poly	pola	k1gFnSc2	pola
<g/>
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
koncem	konec	k1gInSc7	konec
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
dále	daleko	k6eAd2	daleko
určující	určující	k2eAgNnSc1d1	určující
její	její	k3xOp3gInSc4	její
osud	osud	k1gInSc4	osud
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
kódující	kódující	k2eAgFnSc1d1	kódující
oblast	oblast	k1gFnSc1	oblast
nese	nést	k5eAaImIp3nS	nést
kodóny	kodón	k1gInPc4	kodón
tvořené	tvořený	k2eAgInPc4d1	tvořený
nepřekrývajícími	překrývající	k2eNgFnPc7d1	nepřekrývající
se	se	k3xPyFc4	se
trojicemi	trojice	k1gFnPc7	trojice
nukleotidů	nukleotid	k1gInPc2	nukleotid
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yRgMnPc2	který
jsou	být	k5eAaImIp3nP	být
připojeny	připojen	k2eAgFnPc1d1	připojena
příslušné	příslušný	k2eAgFnPc1d1	příslušná
aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
<g/>
,	,	kIx,	,
a	a	k8xC	a
start	start	k1gInSc4	start
kodon	kodona	k1gFnPc2	kodona
a	a	k8xC	a
stop	stopa	k1gFnPc2	stopa
kodon	kodona	k1gFnPc2	kodona
určující	určující	k2eAgInSc4d1	určující
začátek	začátek	k1gInSc4	začátek
a	a	k8xC	a
konec	konec	k1gInSc4	konec
transkripce	transkripce	k1gFnSc2	transkripce
<g/>
.	.	kIx.	.
</s>
<s>
Aktivně	aktivně	k6eAd1	aktivně
překládaná	překládaný	k2eAgFnSc1d1	překládaná
mRNA	mRNA	k?	mRNA
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
pomocí	pomocí	k7c2	pomocí
proteinů	protein	k1gInPc2	protein
rozpoznávajících	rozpoznávající	k2eAgInPc2d1	rozpoznávající
poly	pola	k1gFnPc4	pola
<g/>
(	(	kIx(	(
<g/>
A	a	k9	a
<g/>
)	)	kIx)	)
konec	konec	k1gInSc4	konec
a	a	k8xC	a
čepičku	čepička	k1gFnSc4	čepička
spojena	spojit	k5eAaPmNgFnS	spojit
do	do	k7c2	do
pseudokružnice	pseudokružnice	k1gFnSc2	pseudokružnice
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
ribozomům	ribozom	k1gMnPc3	ribozom
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
translace	translace	k1gFnSc2	translace
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc1	jejich
odpadnutí	odpadnutí	k1gNnPc1	odpadnutí
rychle	rychle	k6eAd1	rychle
znovu	znovu	k6eAd1	znovu
nalézt	nalézt	k5eAaBmF	nalézt
začátek	začátek	k1gInSc4	začátek
transkripce	transkripce	k1gFnSc2	transkripce
<g/>
.	.	kIx.	.
</s>
<s>
Takovou	takový	k3xDgFnSc4	takový
mRNA	mRNA	k?	mRNA
navíc	navíc	k6eAd1	navíc
často	často	k6eAd1	často
překládá	překládat	k5eAaImIp3nS	překládat
více	hodně	k6eAd2	hodně
ribozomů	ribozom	k1gInPc2	ribozom
najednou	najednou	k6eAd1	najednou
a	a	k8xC	a
společně	společně	k6eAd1	společně
tvoří	tvořit	k5eAaImIp3nS	tvořit
útvar	útvar	k1gInSc1	útvar
zvaný	zvaný	k2eAgInSc1d1	zvaný
polyzom	polyzom	k1gInSc1	polyzom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
tRNA	trnout	k5eAaImSgMnS	trnout
(	(	kIx(	(
<g/>
transferová	transferový	k2eAgFnSc1d1	transferová
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
připojuje	připojovat	k5eAaImIp3nS	připojovat
specifickou	specifický	k2eAgFnSc4d1	specifická
aminokyselinu	aminokyselina	k1gFnSc4	aminokyselina
do	do	k7c2	do
rostoucího	rostoucí	k2eAgInSc2d1	rostoucí
polypeptidového	polypeptidový	k2eAgInSc2d1	polypeptidový
řetězce	řetězec	k1gInSc2	řetězec
při	při	k7c6	při
translaci	translace	k1gFnSc6	translace
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
adaptér	adaptér	k1gInSc1	adaptér
pro	pro	k7c4	pro
nabitou	nabitý	k2eAgFnSc4d1	nabitá
aminokyselinu	aminokyselina	k1gFnSc4	aminokyselina
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
překlad	překlad	k1gInSc1	překlad
informace	informace	k1gFnSc2	informace
z	z	k7c2	z
úrovně	úroveň	k1gFnSc2	úroveň
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
do	do	k7c2	do
úrovně	úroveň	k1gFnSc2	úroveň
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
samotné	samotný	k2eAgNnSc1d1	samotné
rozpoznání	rozpoznání	k1gNnSc1	rozpoznání
probíhá	probíhat	k5eAaImIp3nS	probíhat
podle	podle	k7c2	podle
párování	párování	k1gNnSc2	párování
bází	báze	k1gFnPc2	báze
v	v	k7c6	v
dvojici	dvojice	k1gFnSc6	dvojice
kodon-antikodon	kodonntikodona	k1gFnPc2	kodon-antikodona
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
tRNA	trnout	k5eAaImSgInS	trnout
je	být	k5eAaImIp3nS	být
průměrně	průměrně	k6eAd1	průměrně
17	[number]	k4	17
%	%	kIx~	%
bází	báze	k1gFnPc2	báze
modifikováno	modifikován	k2eAgNnSc1d1	modifikováno
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
kolísavé	kolísavý	k2eAgNnSc4d1	kolísavé
párování	párování	k1gNnSc4	párování
bází	báze	k1gFnPc2	báze
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
i	i	k9	i
jiné	jiný	k2eAgNnSc4d1	jiné
než	než	k8xS	než
klasické	klasický	k2eAgNnSc4d1	klasické
Watson-crickovské	Watsonrickovský	k2eAgNnSc4d1	Watson-crickovský
párování	párování	k1gNnSc4	párování
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
tRNA	trnout	k5eAaImSgInS	trnout
je	on	k3xPp3gMnPc4	on
proto	proto	k8xC	proto
často	často	k6eAd1	často
schopna	schopen	k2eAgFnSc1d1	schopna
rozpoznávat	rozpoznávat	k5eAaImF	rozpoznávat
více	hodně	k6eAd2	hodně
různých	různý	k2eAgInPc2d1	různý
kodónů	kodón	k1gInPc2	kodón
patřících	patřící	k2eAgFnPc2d1	patřící
jedné	jeden	k4xCgFnSc3	jeden
aminokyselině	aminokyselina	k1gFnSc3	aminokyselina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
degenerovanosti	degenerovanost	k1gFnSc2	degenerovanost
genetického	genetický	k2eAgInSc2d1	genetický
kódu	kód	k1gInSc2	kód
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
rRNA	rRNA	k?	rRNA
(	(	kIx(	(
<g/>
ribozomální	ribozomální	k2eAgFnSc1d1	ribozomální
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejčastější	častý	k2eAgFnSc7d3	nejčastější
molekulou	molekula	k1gFnSc7	molekula
RNA	RNA	kA	RNA
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
<g/>
,	,	kIx,	,
u	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
může	moct	k5eAaImIp3nS	moct
tvořit	tvořit	k5eAaImF	tvořit
95	[number]	k4	95
<g/>
–	–	k?	–
<g/>
98	[number]	k4	98
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
rRNA	rRNA	k?	rRNA
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
jednak	jednak	k8xC	jednak
svou	svůj	k3xOyFgFnSc7	svůj
stavební	stavební	k2eAgFnSc7d1	stavební
funkcí	funkce	k1gFnSc7	funkce
v	v	k7c6	v
ribozomu	ribozom	k1gInSc6	ribozom
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
právě	právě	k9	právě
rRNA	rRNA	k?	rRNA
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
rRNA	rRNA	k?	rRNA
je	být	k5eAaImIp3nS	být
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
katalytickou	katalytický	k2eAgFnSc4d1	katalytická
aktivitu	aktivita	k1gFnSc4	aktivita
ribozomu	ribozom	k1gInSc2	ribozom
<g/>
.	.	kIx.	.
</s>
<s>
Důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
ribozomu	ribozom	k1gInSc6	ribozom
katalytickou	katalytický	k2eAgFnSc4d1	katalytická
aktivitu	aktivita	k1gFnSc4	aktivita
RNA	RNA	kA	RNA
a	a	k8xC	a
ne	ne	k9	ne
protein	protein	k1gInSc1	protein
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
několik	několik	k4yIc4	několik
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
evolučního	evoluční	k2eAgNnSc2d1	evoluční
vysvětlení	vysvětlení	k1gNnSc2	vysvětlení
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
hypotézy	hypotéza	k1gFnSc2	hypotéza
RNA	RNA	kA	RNA
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
první	první	k4xOgInPc1	první
proteiny	protein	k1gInPc1	protein
syntetizovány	syntetizován	k2eAgInPc1d1	syntetizován
výhradně	výhradně	k6eAd1	výhradně
ribonukleovou	ribonukleový	k2eAgFnSc7d1	ribonukleová
kyselinou	kyselina	k1gFnSc7	kyselina
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
pomoci	pomoc	k1gFnSc2	pomoc
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
připojily	připojit	k5eAaPmAgInP	připojit
až	až	k9	až
později	pozdě	k6eAd2	pozdě
<g/>
)	)	kIx)	)
a	a	k8xC	a
rRNA	rRNA	k?	rRNA
už	už	k6eAd1	už
nebyla	být	k5eNaImAgFnS	být
nikdy	nikdy	k6eAd1	nikdy
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
<g/>
.	.	kIx.	.
</s>
<s>
Biochemicky	biochemicky	k6eAd1	biochemicky
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
fenomén	fenomén	k1gInSc1	fenomén
dá	dát	k5eAaPmIp3nS	dát
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
RNA	RNA	kA	RNA
snadno	snadno	k6eAd1	snadno
mění	měnit	k5eAaImIp3nS	měnit
konformaci	konformace	k1gFnSc4	konformace
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
tak	tak	k6eAd1	tak
nezbytný	zbytný	k2eNgInSc1d1	zbytný
pohyb	pohyb	k1gInSc1	pohyb
ribozomu	ribozom	k1gInSc2	ribozom
<g/>
;	;	kIx,	;
navíc	navíc	k6eAd1	navíc
právě	právě	k9	právě
RNA	RNA	kA	RNA
je	být	k5eAaImIp3nS	být
nejvhodnější	vhodný	k2eAgNnSc1d3	nejvhodnější
pro	pro	k7c4	pro
specifické	specifický	k2eAgNnSc4d1	specifické
rozpoznání	rozpoznání	k1gNnSc4	rozpoznání
dalších	další	k2eAgMnPc2d1	další
RNA	RNA	kA	RNA
řídících	řídící	k2eAgMnPc2d1	řídící
syntézu	syntéza	k1gFnSc4	syntéza
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
mRNA	mRNA	k?	mRNA
a	a	k8xC	a
tRNA	trnout	k5eAaImSgInS	trnout
<g/>
)	)	kIx)	)
pomocí	pomocí	k7c2	pomocí
párování	párování	k1gNnSc2	párování
bází	báze	k1gFnPc2	báze
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
interakcí	interakce	k1gFnPc2	interakce
typických	typický	k2eAgFnPc2d1	typická
pro	pro	k7c4	pro
nukleové	nukleový	k2eAgFnPc4d1	nukleová
kyseliny	kyselina	k1gFnPc4	kyselina
<g/>
.	.	kIx.	.
<g/>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgFnPc2	tento
RNA	RNA	kA	RNA
pro	pro	k7c4	pro
proteosyntézu	proteosyntéza	k1gFnSc4	proteosyntéza
nezbytných	zbytný	k2eNgMnPc2d1	zbytný
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
další	další	k2eAgInSc4d1	další
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ji	on	k3xPp3gFnSc4	on
přímo	přímo	k6eAd1	přímo
regulují	regulovat	k5eAaImIp3nP	regulovat
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
transferová-mediátorová	transferováediátorový	k2eAgFnSc1d1	transferová-mediátorový
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
tmRNA	tmRNA	k?	tmRNA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
u	u	k7c2	u
řady	řada	k1gFnSc2	řada
bakterií	bakterie	k1gFnPc2	bakterie
a	a	k8xC	a
v	v	k7c6	v
plastidech	plastid	k1gInPc6	plastid
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
záchraně	záchrana	k1gFnSc3	záchrana
"	"	kIx"	"
<g/>
zaseknutého	zaseknutý	k2eAgInSc2d1	zaseknutý
<g/>
"	"	kIx"	"
ribozomu	ribozom	k1gInSc2	ribozom
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
naváže	navázat	k5eAaPmIp3nS	navázat
jako	jako	k9	jako
tRNA	trnout	k5eAaImSgInS	trnout
a	a	k8xC	a
je	on	k3xPp3gFnPc4	on
jím	jíst	k5eAaImIp1nS	jíst
přepsána	přepsat	k5eAaPmNgFnS	přepsat
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
značky	značka	k1gFnSc2	značka
pro	pro	k7c4	pro
degradaci	degradace	k1gFnSc4	degradace
vznikajícího	vznikající	k2eAgInSc2d1	vznikající
proteinu	protein	k1gInSc2	protein
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ribozom	ribozom	k1gInSc4	ribozom
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
7SL	[number]	k4	7SL
RNA	RNA	kA	RNA
vyskytující	vyskytující	k2eAgInPc4d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c4	v
signál	signál	k1gInSc4	signál
rozpoznávající	rozpoznávající	k2eAgFnSc4d1	rozpoznávající
částici	částice	k1gFnSc4	částice
<g/>
,	,	kIx,	,
7SL	[number]	k4	7SL
RNA	RNA	kA	RNA
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
zastavení	zastavení	k1gNnSc3	zastavení
proteosyntézy	proteosyntéza	k1gFnSc2	proteosyntéza
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
tzv.	tzv.	kA	tzv.
kotranslační	kotranslační	k2eAgFnPc4d1	kotranslační
translokace	translokace	k1gFnPc4	translokace
proteinů	protein	k1gInPc2	protein
určených	určený	k2eAgFnPc2d1	určená
k	k	k7c3	k
produkci	produkce	k1gFnSc3	produkce
ven	ven	k6eAd1	ven
z	z	k7c2	z
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Regulace	regulace	k1gFnSc1	regulace
genové	genový	k2eAgFnSc2d1	genová
exprese	exprese	k1gFnSc2	exprese
===	===	k?	===
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
RNA	RNA	kA	RNA
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
čtení	čtení	k1gNnSc4	čtení
genetického	genetický	k2eAgInSc2d1	genetický
kódu	kód	k1gInSc2	kód
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
také	také	k9	také
schopná	schopný	k2eAgFnSc1d1	schopná
tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
i	i	k9	i
dále	daleko	k6eAd2	daleko
regulovat	regulovat	k5eAaImF	regulovat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
je	být	k5eAaImIp3nS	být
nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
mechanismem	mechanismus	k1gInSc7	mechanismus
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
RNA	RNA	kA	RNA
interference	interference	k1gFnSc1	interference
probíhající	probíhající	k2eAgFnSc1d1	probíhající
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
posttranskripční	posttranskripční	k2eAgFnSc2d1	posttranskripční
regulace	regulace	k1gFnSc2	regulace
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
RNA	RNA	kA	RNA
interferenci	interference	k1gFnSc4	interference
je	být	k5eAaImIp3nS	být
rozštěpena	rozštěpen	k2eAgFnSc1d1	rozštěpena
dvouvláknová	dvouvláknový	k2eAgFnSc1d1	dvouvláknová
molekula	molekula	k1gFnSc1	molekula
RNA	RNA	kA	RNA
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
jejích	její	k3xOp3gNnPc2	její
vláken	vlákno	k1gNnPc2	vlákno
je	být	k5eAaImIp3nS	být
vneseno	vnést	k5eAaPmNgNnS	vnést
do	do	k7c2	do
komplexu	komplex	k1gInSc2	komplex
RISC	RISC	kA	RISC
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
vyskytne	vyskytnout	k5eAaPmIp3nS	vyskytnout
molekula	molekula	k1gFnSc1	molekula
RNA	RNA	kA	RNA
plně	plně	k6eAd1	plně
komplementární	komplementární	k2eAgInPc4d1	komplementární
s	s	k7c7	s
vláknem	vlákno	k1gNnSc7	vlákno
RNA	RNA	kA	RNA
neseném	nesený	k2eAgInSc6d1	nesený
v	v	k7c6	v
komplexu	komplex	k1gInSc6	komplex
RISC	RISC	kA	RISC
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
molekula	molekula	k1gFnSc1	molekula
rozštěpena	rozštěpit	k5eAaPmNgFnS	rozštěpit
komplexem	komplex	k1gInSc7	komplex
RISC	RISC	kA	RISC
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
druhé	druhý	k4xOgNnSc1	druhý
vlákno	vlákno	k1gNnSc1	vlákno
komplementární	komplementární	k2eAgNnSc1d1	komplementární
pouze	pouze	k6eAd1	pouze
částečně	částečně	k6eAd1	částečně
<g/>
,	,	kIx,	,
mRNA	mRNA	k?	mRNA
není	být	k5eNaImIp3nS	být
degradována	degradován	k2eAgFnSc1d1	degradována
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
zabráněno	zabráněn	k2eAgNnSc1d1	zabráněno
její	její	k3xOp3gFnSc4	její
translaci	translace	k1gFnSc4	translace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RNA	RNA	kA	RNA
interferenci	interference	k1gFnSc4	interference
řídí	řídit	k5eAaImIp3nS	řídit
dva	dva	k4xCgInPc4	dva
hlavní	hlavní	k2eAgInPc4d1	hlavní
typy	typ	k1gInPc4	typ
RNA	RNA	kA	RNA
<g/>
:	:	kIx,	:
siRNA	siRNA	k?	siRNA
(	(	kIx(	(
<g/>
short	short	k1gInSc1	short
interfering	interfering	k1gInSc1	interfering
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
a	a	k8xC	a
miRNA	miRNA	k?	miRNA
(	(	kIx(	(
<g/>
microRNA	microRNA	k?	microRNA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
siRNA	siRNA	k?	siRNA
vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
dvouvláknové	dvouvláknový	k2eAgFnSc2d1	dvouvláknová
RNA	RNA	kA	RNA
především	především	k6eAd1	především
vnějšího	vnější	k2eAgInSc2d1	vnější
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
dsRNA	dsRNA	k?	dsRNA
viry	vira	k1gFnSc2	vira
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
do	do	k7c2	do
RISC	RISC	kA	RISC
vneseno	vnést	k5eAaPmNgNnS	vnést
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
vláken	vlákno	k1gNnPc2	vlákno
dsRNA	dsRNA	k?	dsRNA
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgFnSc2	druhý
z	z	k7c2	z
vláken	vlákna	k1gFnPc2	vlákna
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
plně	plně	k6eAd1	plně
komplementární	komplementární	k2eAgFnSc1d1	komplementární
a	a	k8xC	a
cizorodá	cizorodý	k2eAgFnSc1d1	cizorodá
dsRNA	dsRNA	k?	dsRNA
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
komplexem	komplex	k1gInSc7	komplex
RISC	RISC	kA	RISC
rozštěpena	rozštěpen	k2eAgFnSc1d1	rozštěpena
<g/>
.	.	kIx.	.
miRNA	miRNA	k?	miRNA
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
kódována	kódovat	k5eAaBmNgFnS	kódovat
buňkou	buňka	k1gFnSc7	buňka
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
samostatnými	samostatný	k2eAgInPc7d1	samostatný
geny	gen	k1gInPc7	gen
pro	pro	k7c4	pro
miRNA	miRNA	k?	miRNA
<g/>
,	,	kIx,	,
oblastmi	oblast	k1gFnPc7	oblast
uvnitř	uvnitř	k7c2	uvnitř
intronů	intron	k1gInPc2	intron
genů	gen	k1gInPc2	gen
kódujících	kódující	k2eAgInPc2d1	kódující
protein	protein	k1gInSc4	protein
nebo	nebo	k8xC	nebo
geny	gen	k1gInPc4	gen
pro	pro	k7c4	pro
jinou	jiná	k1gFnSc4	jiná
nekódující	kódující	k2eNgFnSc2d1	nekódující
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
miRNA	miRNA	k?	miRNA
reguluje	regulovat	k5eAaImIp3nS	regulovat
genovou	genový	k2eAgFnSc4d1	genová
expresi	exprese	k1gFnSc4	exprese
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
translaci	translace	k1gFnSc4	translace
těch	ten	k3xDgFnPc2	ten
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgFnPc7	který
nedokonale	dokonale	k6eNd1	dokonale
páruje	párovat	k5eAaImIp3nS	párovat
(	(	kIx(	(
<g/>
příklad	příklad	k1gInSc4	příklad
viz	vidět	k5eAaImRp2nS	vidět
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
mechanismus	mechanismus	k1gInSc1	mechanismus
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
RNA	RNA	kA	RNA
silencing	silencing	k1gInSc1	silencing
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
bioinformatických	bioinformatický	k2eAgFnPc2d1	bioinformatický
analýz	analýza	k1gFnPc2	analýza
se	se	k3xPyFc4	se
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
až	až	k9	až
60	[number]	k4	60
%	%	kIx~	%
lidských	lidský	k2eAgInPc2d1	lidský
genů	gen	k1gInPc2	gen
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
regulováno	regulovat	k5eAaImNgNnS	regulovat
pomocí	pomocí	k7c2	pomocí
miRNA	miRNA	k?	miRNA
a	a	k8xC	a
probíhá	probíhat	k5eAaImIp3nS	probíhat
intenzivní	intenzivní	k2eAgInSc4d1	intenzivní
výzkum	výzkum	k1gInSc4	výzkum
propojení	propojení	k1gNnSc2	propojení
miRNA	miRNA	k?	miRNA
a	a	k8xC	a
různých	různý	k2eAgFnPc2d1	různá
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
rakoviny	rakovina	k1gFnSc2	rakovina
<g/>
.	.	kIx.	.
<g/>
Kromě	kromě	k7c2	kromě
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
dvou	dva	k4xCgFnPc2	dva
hlavních	hlavní	k2eAgFnPc2d1	hlavní
skupin	skupina	k1gFnPc2	skupina
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
i	i	k9	i
další	další	k2eAgInPc1d1	další
typy	typ	k1gInPc1	typ
RNA	RNA	kA	RNA
řídících	řídící	k2eAgFnPc2d1	řídící
RNA	RNA	kA	RNA
interferenci	interference	k1gFnSc4	interference
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
piRNA	piRNA	k?	piRNA
hrající	hrající	k2eAgFnSc4d1	hrající
roli	role	k1gFnSc4	role
v	v	k7c6	v
obraně	obrana	k1gFnSc6	obrana
proti	proti	k7c3	proti
retrotranspozonům	retrotranspozon	k1gMnPc3	retrotranspozon
a	a	k8xC	a
siRNA	siRNA	k?	siRNA
pocházející	pocházející	k2eAgFnSc4d1	pocházející
z	z	k7c2	z
repetitivních	repetitivní	k2eAgFnPc2d1	repetitivní
sekvencí	sekvence	k1gFnPc2	sekvence
(	(	kIx(	(
<g/>
rasiRNA	rasiRNA	k?	rasiRNA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
výzkumné	výzkumný	k2eAgInPc4d1	výzkumný
nebo	nebo	k8xC	nebo
léčebné	léčebný	k2eAgInPc4d1	léčebný
účely	účel	k1gInPc4	účel
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
umělá	umělý	k2eAgFnSc1d1	umělá
shRNA	shRNA	k?	shRNA
(	(	kIx(	(
<g/>
small	small	k1gMnSc1	small
hairpin	hairpin	k1gMnSc1	hairpin
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
krátké	krátký	k2eAgFnPc1d1	krátká
molekuly	molekula	k1gFnPc1	molekula
RNA	RNA	kA	RNA
tvořící	tvořící	k2eAgFnSc4d1	tvořící
vlásenku	vlásenka	k1gFnSc4	vlásenka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
rozeznána	rozeznán	k2eAgFnSc1d1	rozeznána
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
zpracována	zpracovat	k5eAaPmNgFnS	zpracovat
aparátem	aparát	k1gInSc7	aparát
RNA	RNA	kA	RNA
interference	interference	k1gFnSc1	interference
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
prekurzory	prekurzor	k1gInPc1	prekurzor
siRNA	siRNA	k?	siRNA
nebo	nebo	k8xC	nebo
miRNA	miRNA	k?	miRNA
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
RNA	RNA	kA	RNA
interference	interference	k1gFnSc1	interference
známá	známý	k2eAgFnSc1d1	známá
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
eukaryot	eukaryota	k1gFnPc2	eukaryota
<g/>
,	,	kIx,	,
řada	řada	k1gFnSc1	řada
bakterií	bakterie	k1gFnPc2	bakterie
a	a	k8xC	a
archeí	archeí	k6eAd1	archeí
má	mít	k5eAaImIp3nS	mít
pro	pro	k7c4	pro
obranu	obrana	k1gFnSc4	obrana
proti	proti	k7c3	proti
bakteriofágům	bakteriofág	k1gInPc3	bakteriofág
a	a	k8xC	a
dalším	další	k2eAgFnPc3d1	další
parazitickým	parazitický	k2eAgFnPc3d1	parazitická
DNA	DNA	kA	DNA
elementům	element	k1gInPc3	element
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
funkční	funkční	k2eAgInSc1d1	funkční
analog	analog	k1gInSc1	analog
RNA	RNA	kA	RNA
interference	interference	k1gFnSc1	interference
zvaný	zvaný	k2eAgInSc1d1	zvaný
CRISPR	CRISPR	kA	CRISPR
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
<g/>
Regulace	regulace	k1gFnSc1	regulace
genové	genový	k2eAgFnSc2d1	genová
exprese	exprese	k1gFnSc2	exprese
pomocí	pomocí	k7c2	pomocí
RNA	RNA	kA	RNA
ale	ale	k8xC	ale
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
úrovních	úroveň	k1gFnPc6	úroveň
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ovlivňováním	ovlivňování	k1gNnSc7	ovlivňování
uspořádání	uspořádání	k1gNnSc2	uspořádání
genomu	genom	k1gInSc2	genom
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
nekódující	kódující	k2eNgFnSc1d1	nekódující
RNA	RNA	kA	RNA
nazvaná	nazvaný	k2eAgNnPc4d1	nazvané
XIST	XIST	kA	XIST
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
savců	savec	k1gMnPc2	savec
inaktivuje	inaktivovat	k5eAaBmIp3nS	inaktivovat
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
chromozomů	chromozom	k1gInPc2	chromozom
X	X	kA	X
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
obalí	obalit	k5eAaPmIp3nP	obalit
a	a	k8xC	a
zabrání	zabránit	k5eAaPmIp3nP	zabránit
aktivaci	aktivace	k1gFnSc4	aktivace
genů	gen	k1gInPc2	gen
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
tento	tento	k3xDgInSc1	tento
chromozom	chromozom	k1gInSc1	chromozom
nese	nést	k5eAaImIp3nS	nést
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Katalytická	katalytický	k2eAgFnSc1d1	katalytická
funkce	funkce	k1gFnSc1	funkce
===	===	k?	===
</s>
</p>
<p>
<s>
Katalytickou	katalytický	k2eAgFnSc4d1	katalytická
funkci	funkce	k1gFnSc4	funkce
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
živých	živý	k2eAgInPc6d1	živý
organismech	organismus	k1gInPc6	organismus
především	především	k9	především
proteinové	proteinový	k2eAgInPc1d1	proteinový
enzymy	enzym	k1gInPc1	enzym
<g/>
,	,	kIx,	,
roste	růst	k5eAaImIp3nS	růst
ale	ale	k9	ale
množství	množství	k1gNnSc1	množství
známých	známý	k2eAgFnPc2d1	známá
molekul	molekula	k1gFnPc2	molekula
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
katalytickou	katalytický	k2eAgFnSc4d1	katalytická
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
ribozymy	ribozym	k1gInPc7	ribozym
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
zástupcem	zástupce	k1gMnSc7	zástupce
ribozymů	ribozym	k1gMnPc2	ribozym
je	být	k5eAaImIp3nS	být
ribozom	ribozom	k1gInSc4	ribozom
katalyzující	katalyzující	k2eAgInSc4d1	katalyzující
peptidyltransferázovou	peptidyltransferázový	k2eAgFnSc4d1	peptidyltransferázový
reakci	reakce	k1gFnSc4	reakce
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgFnSc7d3	nejčastější
reakcí	reakce	k1gFnSc7	reakce
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
ribozymy	ribozym	k1gInPc4	ribozym
katalyzují	katalyzovat	k5eAaBmIp3nP	katalyzovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
štěpení	štěpení	k1gNnSc1	štěpení
cukr-fosfátové	cukrosfátový	k2eAgFnSc2d1	cukr-fosfátový
kostry	kostra	k1gFnSc2	kostra
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
molekule	molekula	k1gFnSc6	molekula
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
ribozymy	ribozym	k1gInPc1	ribozym
ale	ale	k8xC	ale
de	de	k?	de
facto	facto	k1gNnSc4	facto
nejsou	být	k5eNaImIp3nP	být
katalyzátory	katalyzátor	k1gInPc4	katalyzátor
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
reakce	reakce	k1gFnSc2	reakce
spotřebují	spotřebovat	k5eAaPmIp3nP	spotřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavým	zajímavý	k2eAgInSc7d1	zajímavý
příkladem	příklad	k1gInSc7	příklad
ribozymů	ribozym	k1gInPc2	ribozym
jsou	být	k5eAaImIp3nP	být
uměle	uměle	k6eAd1	uměle
připravené	připravený	k2eAgFnPc1d1	připravená
peptidyltransferázy	peptidyltransferáza	k1gFnPc1	peptidyltransferáza
schopné	schopný	k2eAgFnPc1d1	schopná
katalyzovat	katalyzovat	k5eAaBmF	katalyzovat
tvorbu	tvorba	k1gFnSc4	tvorba
vazby	vazba	k1gFnSc2	vazba
mezi	mezi	k7c7	mezi
aminokyselinami	aminokyselina	k1gFnPc7	aminokyselina
a	a	k8xC	a
umělé	umělý	k2eAgInPc4d1	umělý
ribozymy	ribozym	k1gInPc4	ribozym
schopné	schopný	k2eAgNnSc1d1	schopné
replikovat	replikovat	k5eAaImF	replikovat
jiné	jiný	k2eAgFnPc4d1	jiná
molekuly	molekula	k1gFnPc4	molekula
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
podpora	podpora	k1gFnSc1	podpora
hypotézy	hypotéza	k1gFnSc2	hypotéza
RNA	RNA	kA	RNA
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
živé	živý	k2eAgInPc4d1	živý
organismy	organismus	k1gInPc4	organismus
první	první	k4xOgFnSc1	první
nositelka	nositelka	k1gFnSc1	nositelka
genetické	genetický	k2eAgFnSc2d1	genetická
informace	informace	k1gFnSc2	informace
právě	právě	k9	právě
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
dobře	dobře	k6eAd1	dobře
prozkoumané	prozkoumaný	k2eAgInPc4d1	prozkoumaný
ribozymy	ribozym	k1gInPc4	ribozym
patří	patřit	k5eAaImIp3nS	patřit
RNáza	RNáza	k1gFnSc1	RNáza
P	P	kA	P
zodpovědná	zodpovědný	k2eAgFnSc1d1	zodpovědná
za	za	k7c4	za
zrání	zrání	k1gNnSc4	zrání
tRNA	trnout	k5eAaImSgInS	trnout
a	a	k8xC	a
sebevystřihující	sebevystřihující	k2eAgInPc4d1	sebevystřihující
introny	intron	k1gInPc4	intron
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
introny	intron	k1gMnPc7	intron
(	(	kIx(	(
<g/>
nekódující	kódující	k2eNgFnSc2d1	nekódující
oblasti	oblast	k1gFnSc2	oblast
<g/>
)	)	kIx)	)
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
tRNA	trnout	k5eAaImSgInS	trnout
nebo	nebo	k8xC	nebo
rRNA	rRNA	k?	rRNA
schopné	schopný	k2eAgNnSc1d1	schopné
vyštěpit	vyštěpit	k5eAaPmF	vyštěpit
sebe	sebe	k3xPyFc4	sebe
sama	sám	k3xTgMnSc4	sám
i	i	k9	i
bez	bez	k7c2	bez
pomoci	pomoc	k1gFnSc2	pomoc
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
</s>
<s>
Ribozymy	Ribozym	k1gInPc1	Ribozym
jsou	být	k5eAaImIp3nP	být
relativně	relativně	k6eAd1	relativně
běžné	běžný	k2eAgMnPc4d1	běžný
u	u	k7c2	u
viroidů	viroid	k1gInPc2	viroid
a	a	k8xC	a
virusoidů	virusoid	k1gInPc2	virusoid
<g/>
,	,	kIx,	,
kterým	který	k3yIgInPc3	který
ribozymy	ribozymum	k1gNnPc7	ribozymum
štěpí	štěpit	k5eAaImIp3nP	štěpit
lineární	lineární	k2eAgFnSc4d1	lineární
molekulu	molekula	k1gFnSc4	molekula
RNA	RNA	kA	RNA
nesoucí	nesoucí	k2eAgMnSc1d1	nesoucí
několik	několik	k4yIc4	několik
násobků	násobek	k1gInPc2	násobek
jejich	jejich	k3xOp3gInSc2	jejich
genomu	genom	k1gInSc2	genom
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
jejich	jejich	k3xOp3gFnSc6	jejich
replikaci	replikace	k1gFnSc6	replikace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
probíhá	probíhat	k5eAaImIp3nS	probíhat
klinické	klinický	k2eAgNnSc4d1	klinické
testování	testování	k1gNnSc4	testování
některých	některý	k3yIgInPc2	některý
umělých	umělý	k2eAgInPc2d1	umělý
ribozymů	ribozym	k1gInPc2	ribozym
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
potenciálně	potenciálně	k6eAd1	potenciálně
využitelné	využitelný	k2eAgInPc1d1	využitelný
v	v	k7c6	v
léčbě	léčba	k1gFnSc6	léčba
rakoviny	rakovina	k1gFnSc2	rakovina
nebo	nebo	k8xC	nebo
virových	virový	k2eAgNnPc2d1	virové
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
HIV	HIV	kA	HIV
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Řízení	řízení	k1gNnSc1	řízení
modifikace	modifikace	k1gFnSc2	modifikace
RNA	RNA	kA	RNA
===	===	k?	===
</s>
</p>
<p>
<s>
Význam	význam	k1gInSc1	význam
RNA	RNA	kA	RNA
pro	pro	k7c4	pro
buňku	buňka	k1gFnSc4	buňka
ilustruje	ilustrovat	k5eAaBmIp3nS	ilustrovat
také	také	k9	také
schopnost	schopnost	k1gFnSc1	schopnost
RNA	RNA	kA	RNA
modifikovat	modifikovat	k5eAaBmF	modifikovat
nebo	nebo	k8xC	nebo
řídit	řídit	k5eAaImF	řídit
modifikaci	modifikace	k1gFnSc4	modifikace
jiných	jiný	k2eAgFnPc2d1	jiná
molekul	molekula	k1gFnPc2	molekula
RNA	RNA	kA	RNA
během	běh	k1gInSc7	běh
jejich	jejich	k3xOp3gNnSc4	jejich
zrání	zrání	k1gNnSc4	zrání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
RNA	RNA	kA	RNA
zodpovědných	zodpovědný	k2eAgInPc2d1	zodpovědný
za	za	k7c2	za
modifikace	modifikace	k1gFnSc2	modifikace
RNA	RNA	kA	RNA
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
vyčlenit	vyčlenit	k5eAaPmF	vyčlenit
několik	několik	k4yIc4	několik
hlavních	hlavní	k2eAgFnPc2d1	hlavní
tříd	třída	k1gFnPc2	třída
nekódujících	kódující	k2eNgInPc2d1	nekódující
RNA	RNA	kA	RNA
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
snRNA	snRNA	k?	snRNA
(	(	kIx(	(
<g/>
small	small	k1gMnSc1	small
nuclear	nuclear	k1gMnSc1	nuclear
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
malá	malý	k2eAgFnSc1d1	malá
jaderná	jaderný	k2eAgFnSc1d1	jaderná
RNA	RNA	kA	RNA
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
skupina	skupina	k1gFnSc1	skupina
RNA	RNA	kA	RNA
vyskytující	vyskytující	k2eAgInPc4d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
eukaryotních	eukaryotní	k2eAgFnPc2d1	eukaryotní
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
sestřihu	sestřih	k1gInSc6	sestřih
prekurzorů	prekurzor	k1gMnPc2	prekurzor
mRNA	mRNA	k?	mRNA
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
jejich	jejich	k3xOp3gNnSc2	jejich
zrání	zrání	k1gNnSc2	zrání
<g/>
.	.	kIx.	.
snRNA	snRNA	k?	snRNA
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
ribonukleoproteinové	ribonukleoproteinový	k2eAgInPc4d1	ribonukleoproteinový
komplexy	komplex	k1gInPc4	komplex
zvané	zvaný	k2eAgInPc4d1	zvaný
spliceosom	spliceosom	k1gInSc4	spliceosom
složené	složený	k2eAgFnSc2d1	složená
ze	z	k7c2	z
šesti	šest	k4xCc2	šest
různých	různý	k2eAgFnPc2d1	různá
<g />
.	.	kIx.	.
</s>
<s>
molekul	molekula	k1gFnPc2	molekula
snRNA	snRNA	k?	snRNA
a	a	k8xC	a
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
150	[number]	k4	150
různých	různý	k2eAgInPc2d1	různý
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
snRNA	snRNA	k?	snRNA
v	v	k7c6	v
katalytickém	katalytický	k2eAgNnSc6d1	katalytické
místě	místo	k1gNnSc6	místo
spliceozomu	spliceozom	k1gInSc2	spliceozom
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgFnSc1d1	podobná
sebevystřihujícím	sebevystřihující	k2eAgMnPc3d1	sebevystřihující
intronům	intron	k1gInPc3	intron
typu	typ	k1gInSc2	typ
II	II	kA	II
a	a	k8xC	a
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
spliceosom	spliceosom	k1gInSc1	spliceosom
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
ribozym	ribozym	k1gInSc4	ribozym
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
má	mít	k5eAaImIp3nS	mít
katalytickou	katalytický	k2eAgFnSc4d1	katalytická
aktivitu	aktivita	k1gFnSc4	aktivita
některý	některý	k3yIgInSc1	některý
z	z	k7c2	z
proteinů	protein	k1gInPc2	protein
<g/>
.	.	kIx.	.
<g/>
snoRNA	snoRNA	k?	snoRNA
(	(	kIx(	(
<g/>
small	small	k1gMnSc1	small
nucleolar	nucleolar	k1gMnSc1	nucleolar
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
malá	malý	k2eAgFnSc1d1	malá
jadérková	jadérková	k1gFnSc1	jadérková
RNA	RNA	kA	RNA
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
jadérku	jadérko	k1gNnSc6	jadérko
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
podtyp	podtyp	k1gInSc1	podtyp
snRNA	snRNA	k?	snRNA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
methylaci	methylace	k1gFnSc4	methylace
a	a	k8xC	a
pseudouridinylaci	pseudouridinylace	k1gFnSc4	pseudouridinylace
rRNA	rRNA	k?	rRNA
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
takzvané	takzvaný	k2eAgFnSc2d1	takzvaná
H	H	kA	H
<g/>
/	/	kIx~	/
<g/>
ACA	ACA	kA	ACA
box	box	k1gInSc4	box
rodiny	rodina	k1gFnSc2	rodina
snoRNA	snoRNA	k?	snoRNA
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
RNA	RNA	kA	RNA
komponenta	komponenta	k1gFnSc1	komponenta
lidské	lidský	k2eAgFnSc2d1	lidská
telomerázy	telomeráza	k1gFnSc2	telomeráza
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
templát	templát	k1gInSc1	templát
pro	pro	k7c4	pro
syntézu	syntéza	k1gFnSc4	syntéza
DNA	DNA	kA	DNA
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
telomer	telomera	k1gFnPc2	telomera
procesem	proces	k1gInSc7	proces
reverzní	reverzní	k2eAgFnPc4d1	reverzní
transkripce	transkripce	k1gFnSc2	transkripce
<g/>
.	.	kIx.	.
<g/>
RNáza	RNáza	k1gFnSc1	RNáza
P	P	kA	P
je	být	k5eAaImIp3nS	být
ribozym	ribozym	k1gInSc1	ribozym
nezbytný	zbytný	k2eNgInSc1d1	zbytný
pro	pro	k7c4	pro
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
kroků	krok	k1gInPc2	krok
maturace	maturace	k1gFnSc2	maturace
tRNA	trnout	k5eAaImSgInS	trnout
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
ribonukleoproteinu	ribonukleoproteinout	k5eAaPmIp1nS	ribonukleoproteinout
u	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
archeí	arche	k1gFnPc2	arche
i	i	k8xC	i
eukaryot	eukaryota	k1gFnPc2	eukaryota
<g/>
,	,	kIx,	,
u	u	k7c2	u
všech	všecek	k3xTgFnPc2	všecek
skupin	skupina	k1gFnPc2	skupina
organismů	organismus	k1gInPc2	organismus
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
RNA	RNA	kA	RNA
katalyzátorem	katalyzátor	k1gInSc7	katalyzátor
štěpení	štěpení	k1gNnSc2	štěpení
<g/>
.	.	kIx.	.
<g/>
gRNA	gRNA	k?	gRNA
(	(	kIx(	(
<g/>
guide	guide	k6eAd1	guide
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
editaci	editace	k1gFnSc3	editace
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc4	proces
posttranskripční	posttranskripční	k2eAgFnSc2d1	posttranskripční
modifikace	modifikace	k1gFnSc2	modifikace
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
kinetoplastech	kinetoplast	k1gInPc6	kinetoplast
bičivek	bičivka	k1gFnPc2	bičivka
(	(	kIx(	(
<g/>
k	k	k7c3	k
zástupcům	zástupce	k1gMnPc3	zástupce
patří	patřit	k5eAaImIp3nP	patřit
například	například	k6eAd1	například
trypanozomy	trypanozoma	k1gFnSc2	trypanozoma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
typu	typ	k1gInSc6	typ
editace	editace	k1gFnSc2	editace
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
přidání	přidání	k1gNnSc3	přidání
nebo	nebo	k8xC	nebo
odstranění	odstranění	k1gNnSc3	odstranění
uracilů	uracil	k1gInPc2	uracil
z	z	k7c2	z
molekuly	molekula	k1gFnSc2	molekula
mRNA	mRNA	k?	mRNA
podle	podle	k7c2	podle
sekvence	sekvence	k1gFnSc2	sekvence
gRNA	gRNA	k?	gRNA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vazba	vazba	k1gFnSc1	vazba
malých	malý	k2eAgFnPc2d1	malá
molekul	molekula	k1gFnPc2	molekula
===	===	k?	===
</s>
</p>
<p>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
RNA	RNA	kA	RNA
tvořit	tvořit	k5eAaImF	tvořit
složité	složitý	k2eAgFnPc4d1	složitá
struktury	struktura	k1gFnPc4	struktura
jí	jíst	k5eAaImIp3nS	jíst
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
specificky	specificky	k6eAd1	specificky
vázat	vázat	k5eAaImF	vázat
jiné	jiný	k2eAgFnPc4d1	jiná
molekuly	molekula	k1gFnPc4	molekula
<g/>
,	,	kIx,	,
takové	takový	k3xDgNnSc4	takový
RNA	RNA	kA	RNA
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
aptamery	aptamera	k1gFnPc1	aptamera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
jsou	být	k5eAaImIp3nP	být
aptamery	aptamer	k1gInPc1	aptamer
součástí	součást	k1gFnPc2	součást
tzv.	tzv.	kA	tzv.
riboswitch	riboswitch	k1gMnSc1	riboswitch
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
RNA	RNA	kA	RNA
přepínač	přepínač	k1gInSc1	přepínač
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgFnPc1d1	známá
především	především	k9	především
z	z	k7c2	z
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
i	i	k9	i
u	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
kvasinek	kvasinka	k1gFnPc2	kvasinka
<g/>
.	.	kIx.	.
</s>
<s>
Riboswitch	Riboswitch	k1gInSc1	Riboswitch
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
přepínač	přepínač	k1gInSc1	přepínač
na	na	k7c6	na
molekule	molekula	k1gFnSc6	molekula
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
konformace	konformace	k1gFnSc1	konformace
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
aptameru	aptamer	k1gInSc6	aptamer
navázaná	navázaný	k2eAgFnSc1d1	navázaná
cílová	cílový	k2eAgFnSc1d1	cílová
molekula	molekula	k1gFnSc1	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
lyzinový	lyzinový	k2eAgMnSc1d1	lyzinový
riboswitch	riboswitch	k1gMnSc1	riboswitch
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
bakteriálních	bakteriální	k2eAgNnPc2d1	bakteriální
mRNA	mRNA	k?	mRNA
regulujících	regulující	k2eAgNnPc2d1	regulující
metabolismus	metabolismus	k1gInSc4	metabolismus
lyzinu	lyzino	k1gNnSc3	lyzino
<g/>
.	.	kIx.	.
</s>
<s>
Vazba	vazba	k1gFnSc1	vazba
lyzinu	lyzinout	k5eAaPmIp1nS	lyzinout
do	do	k7c2	do
struktury	struktura	k1gFnSc2	struktura
tvořící	tvořící	k2eAgInSc1d1	tvořící
riboswitch	riboswitch	k1gInSc1	riboswitch
signalizuje	signalizovat	k5eAaImIp3nS	signalizovat
dostatek	dostatek	k1gInSc4	dostatek
lyzinu	lyzin	k1gInSc2	lyzin
a	a	k8xC	a
blokuje	blokovat	k5eAaImIp3nS	blokovat
překlad	překlad	k1gInSc1	překlad
těchto	tento	k3xDgNnPc2	tento
mRNA	mRNA	k?	mRNA
<g/>
.	.	kIx.	.
</s>
<s>
Aptamery	Aptamera	k1gFnPc4	Aptamera
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
připravit	připravit	k5eAaPmF	připravit
metodou	metoda	k1gFnSc7	metoda
"	"	kIx"	"
<g/>
evoluce	evoluce	k1gFnSc1	evoluce
ve	v	k7c6	v
zkumavce	zkumavka	k1gFnSc6	zkumavka
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
SELEX	SELEX	kA	SELEX
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
z	z	k7c2	z
náhodné	náhodný	k2eAgFnSc2d1	náhodná
sady	sada	k1gFnSc2	sada
molekul	molekula	k1gFnPc2	molekula
RNA	RNA	kA	RNA
vyberou	vybrat	k5eAaPmIp3nP	vybrat
ty	ten	k3xDgFnPc1	ten
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
vázat	vázat	k5eAaImF	vázat
požadovanou	požadovaný	k2eAgFnSc4d1	požadovaná
molekulu	molekula	k1gFnSc4	molekula
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
se	se	k3xPyFc4	se
vyizolují	vyizolovat	k5eAaPmIp3nP	vyizolovat
<g/>
,	,	kIx,	,
namnoží	namnožit	k5eAaPmIp3nP	namnožit
pomocí	pomocí	k7c2	pomocí
nepřesně	přesně	k6eNd1	přesně
kopírujících	kopírující	k2eAgInPc2d1	kopírující
enzymů	enzym	k1gInPc2	enzym
a	a	k8xC	a
proces	proces	k1gInSc1	proces
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
takto	takto	k6eAd1	takto
získané	získaný	k2eAgInPc1d1	získaný
RNA	RNA	kA	RNA
aptamery	aptamer	k1gInPc1	aptamer
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
využívají	využívat	k5eAaPmIp3nP	využívat
jako	jako	k9	jako
léky	lék	k1gInPc1	lék
<g/>
.	.	kIx.	.
<g/>
Některé	některý	k3yIgInPc1	některý
ribozymy	ribozym	k1gInPc1	ribozym
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
vázat	vázat	k5eAaImF	vázat
malé	malý	k2eAgFnPc1d1	malá
molekuly	molekula	k1gFnPc1	molekula
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jim	on	k3xPp3gMnPc3	on
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k9	jako
kofaktory	kofaktor	k1gInPc1	kofaktor
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
proteinových	proteinový	k2eAgInPc2d1	proteinový
enzymů	enzym	k1gInPc2	enzym
<g/>
.	.	kIx.	.
</s>
<s>
Ribozymy	Ribozym	k1gInPc1	Ribozym
samotné	samotný	k2eAgInPc1d1	samotný
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
katalyzovat	katalyzovat	k5eAaPmF	katalyzovat
jenom	jenom	k9	jenom
relativně	relativně	k6eAd1	relativně
omezený	omezený	k2eAgInSc1d1	omezený
počet	počet	k1gInSc1	počet
chemických	chemický	k2eAgFnPc2d1	chemická
reakcí	reakce	k1gFnPc2	reakce
<g/>
,	,	kIx,	,
schopnost	schopnost	k1gFnSc1	schopnost
RNA	RNA	kA	RNA
vázat	vázat	k5eAaImF	vázat
také	také	k9	také
jiné	jiný	k2eAgFnPc4d1	jiná
molekuly	molekula	k1gFnPc4	molekula
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gFnPc4	jejich
schopnosti	schopnost	k1gFnPc4	schopnost
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dlouhé	Dlouhé	k2eAgFnSc2d1	Dlouhé
nekódující	kódující	k2eNgFnSc2d1	nekódující
RNA	RNA	kA	RNA
===	===	k?	===
</s>
</p>
<p>
<s>
Dlouhé	Dlouhá	k1gFnPc1	Dlouhá
nekódující	kódující	k2eNgFnSc2d1	nekódující
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
lncRNA	lncRNA	k?	lncRNA
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
nekódující	kódující	k2eNgFnSc4d1	nekódující
RNA	RNA	kA	RNA
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
alespoň	alespoň	k9	alespoň
200	[number]	k4	200
nukleotidů	nukleotid	k1gInPc2	nukleotid
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
ENCODE	ENCODE	kA	ENCODE
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
lidském	lidský	k2eAgInSc6d1	lidský
genomu	genom	k1gInSc6	genom
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
v	v	k7c6	v
podobném	podobný	k2eAgInSc6d1	podobný
počtu	počet	k1gInSc6	počet
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
geny	gen	k1gInPc1	gen
kódující	kódující	k2eAgInPc1d1	kódující
proteiny	protein	k1gInPc1	protein
a	a	k8xC	a
představují	představovat	k5eAaImIp3nP	představovat
nejrozšířenější	rozšířený	k2eAgInSc4d3	nejrozšířenější
typ	typ	k1gInSc4	typ
nekódující	kódující	k2eNgFnSc2d1	nekódující
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
RNA	RNA	kA	RNA
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
modifikován	modifikován	k2eAgMnSc1d1	modifikován
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
nese	nést	k5eAaImIp3nS	nést
tedy	tedy	k9	tedy
5	[number]	k4	5
<g/>
'	'	kIx"	'
čepičku	čepička	k1gFnSc4	čepička
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
polyadenylován	polyadenylován	k2eAgMnSc1d1	polyadenylován
a	a	k8xC	a
často	často	k6eAd1	často
sestřižen	sestřižen	k2eAgInSc1d1	sestřižen
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhé	Dlouhá	k1gFnPc1	Dlouhá
nekódující	kódující	k2eNgFnSc2d1	nekódující
RNA	RNA	kA	RNA
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
především	především	k9	především
v	v	k7c6	v
buněčném	buněčný	k2eAgNnSc6d1	buněčné
jádře	jádro	k1gNnSc6	jádro
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
řídí	řídit	k5eAaImIp3nS	řídit
tvorbu	tvorba	k1gFnSc4	tvorba
jaderných	jaderný	k2eAgNnPc2d1	jaderné
tělísek	tělísko	k1gNnPc2	tělísko
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
těchto	tento	k3xDgNnPc2	tento
tělísek	tělísko	k1gNnPc2	tělísko
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnPc1	jejich
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
komponenty	komponenta	k1gFnPc1	komponenta
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
koncentraci	koncentrace	k1gFnSc6	koncentrace
v	v	k7c6	v
celém	celý	k2eAgNnSc6d1	celé
jádře	jádro	k1gNnSc6	jádro
a	a	k8xC	a
shlukují	shlukovat	k5eAaImIp3nP	shlukovat
se	se	k3xPyFc4	se
až	až	k9	až
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
buňka	buňka	k1gFnSc1	buňka
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
jejich	jejich	k3xOp3gFnSc4	jejich
větší	veliký	k2eAgFnSc4d2	veliký
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
,	,	kIx,	,
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
jejich	jejich	k3xOp3gNnSc1	jejich
skládání	skládání	k1gNnSc1	skládání
většinou	většinou	k6eAd1	většinou
řídí	řídit	k5eAaImIp3nS	řídit
právě	právě	k9	právě
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
nekódující	kódující	k2eNgFnSc2d1	nekódující
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
Cajalova	Cajalův	k2eAgNnPc4d1	Cajalův
tělíska	tělísko	k1gNnPc4	tělísko
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
RNA	RNA	kA	RNA
specifické	specifický	k2eAgNnSc1d1	specifické
pro	pro	k7c4	pro
Cajalova	Cajalův	k2eAgNnPc4d1	Cajalův
tělíska	tělísko	k1gNnPc4	tělísko
(	(	kIx(	(
<g/>
scaRNA	scaRNA	k?	scaRNA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
řídí	řídit	k5eAaImIp3nP	řídit
modifikaci	modifikace	k1gFnSc4	modifikace
malých	malý	k2eAgInPc2d1	malý
jaderných	jaderný	k2eAgInPc2d1	jaderný
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
snRNA	snRNA	k?	snRNA
<g/>
)	)	kIx)	)
a	a	k8xC	a
malých	malý	k2eAgFnPc2d1	malá
jadérkových	jadérková	k1gFnPc2	jadérková
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
snoRNA	snoRNA	k?	snoRNA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
jaderné	jaderný	k2eAgFnSc2d1	jaderná
skvrny	skvrna	k1gFnSc2	skvrna
<g/>
"	"	kIx"	"
obsahující	obsahující	k2eAgInSc1d1	obsahující
<g />
.	.	kIx.	.
</s>
<s>
NEAT2	NEAT2	k4	NEAT2
řídící	řídící	k2eAgFnSc4d1	řídící
úpravu	úprava	k1gFnSc4	úprava
malých	malý	k2eAgInPc2d1	malý
jaderných	jaderný	k2eAgInPc2d1	jaderný
ribonukleoproteinů	ribonukleoprotein	k1gInPc2	ribonukleoprotein
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgInPc2	který
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
funkční	funkční	k2eAgInSc1d1	funkční
spliceosom	spliceosom	k1gInSc1	spliceosom
<g/>
,	,	kIx,	,
a	a	k8xC	a
"	"	kIx"	"
<g/>
paraspeckles	paraspeckles	k1gInSc1	paraspeckles
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
centrem	centr	k1gInSc7	centr
RNA	RNA	kA	RNA
editace	editace	k1gFnSc1	editace
adenosin	adenosin	k1gInSc1	adenosin
<g/>
→	→	k?	→
<g/>
inositol	inositola	k1gFnPc2	inositola
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vytvářeny	vytvářit	k5eAaPmNgFnP	vytvářit
kolem	kolem	k7c2	kolem
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
nekódující	kódující	k2eNgFnSc2d1	nekódující
RNA	RNA	kA	RNA
NEAT	NEAT	kA	NEAT
<g/>
1	[number]	k4	1
<g/>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
nekódující	kódující	k2eNgFnSc2d1	nekódující
RNA	RNA	kA	RNA
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
přepis	přepis	k1gInSc1	přepis
genetické	genetický	k2eAgFnSc2d1	genetická
informace	informace	k1gFnSc2	informace
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
regulují	regulovat	k5eAaImIp3nP	regulovat
epigenetické	epigenetický	k2eAgFnPc4d1	epigenetická
modifikace	modifikace	k1gFnPc4	modifikace
v	v	k7c6	v
řídících	řídící	k2eAgFnPc6d1	řídící
oblastech	oblast	k1gFnPc6	oblast
genů	gen	k1gInPc2	gen
<g/>
.	.	kIx.	.
</s>
<s>
Významná	významný	k2eAgFnSc1d1	významná
je	být	k5eAaImIp3nS	být
především	především	k9	především
interakce	interakce	k1gFnSc1	interakce
s	s	k7c7	s
komplexem	komplex	k1gInSc7	komplex
polycomb	polycomb	k1gMnSc1	polycomb
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
za	za	k7c4	za
epigenetické	epigenetický	k2eAgNnSc4d1	epigenetické
umlčení	umlčení	k1gNnSc4	umlčení
řady	řada	k1gFnSc2	řada
genů	gen	k1gInPc2	gen
pomocí	pomocí	k7c2	pomocí
methylace	methylace	k1gFnSc2	methylace
histonů	histon	k1gInPc2	histon
a	a	k8xC	a
provází	provázet	k5eAaImIp3nS	provázet
inaktivaci	inaktivace	k1gFnSc4	inaktivace
chromozomu	chromozom	k1gInSc2	chromozom
X.	X.	kA	X.
Asi	asi	k9	asi
pětina	pětina	k1gFnSc1	pětina
všech	všecek	k3xTgInPc2	všecek
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
nekódujících	kódující	k2eNgInPc2d1	nekódující
RNA	RNA	kA	RNA
interaguje	interagovat	k5eAaBmIp3nS	interagovat
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
komplexem	komplex	k1gInSc7	komplex
a	a	k8xC	a
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
zodpovědné	zodpovědný	k2eAgNnSc1d1	zodpovědné
za	za	k7c4	za
výběr	výběr	k1gInSc4	výběr
genu	gen	k1gInSc2	gen
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
přepis	přepis	k1gInSc1	přepis
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
umlčený	umlčený	k2eAgMnSc1d1	umlčený
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejlépe	dobře	k6eAd3	dobře
prostudovaných	prostudovaný	k2eAgMnPc2d1	prostudovaný
zástupců	zástupce	k1gMnPc2	zástupce
této	tento	k3xDgFnSc2	tento
třídy	třída	k1gFnSc2	třída
RNA	RNA	kA	RNA
je	být	k5eAaImIp3nS	být
XIST	XIST	kA	XIST
vyvolávající	vyvolávající	k2eAgInSc4d1	vyvolávající
u	u	k7c2	u
samic	samice	k1gFnPc2	samice
savců	savec	k1gMnPc2	savec
inaktivaci	inaktivace	k1gFnSc3	inaktivace
chromozomu	chromozom	k1gInSc2	chromozom
X	X	kA	X
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obalí	obalit	k5eAaPmIp3nP	obalit
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
chromozomů	chromozom	k1gInPc2	chromozom
X	X	kA	X
a	a	k8xC	a
zastaví	zastavit	k5eAaPmIp3nS	zastavit
čtení	čtení	k1gNnSc3	čtení
jeho	jeho	k3xOp3gFnSc2	jeho
genetické	genetický	k2eAgFnSc2d1	genetická
informace	informace	k1gFnSc2	informace
<g/>
;	;	kIx,	;
jako	jako	k8xC	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
přepisovaných	přepisovaný	k2eAgFnPc2d1	přepisovaná
oblastí	oblast	k1gFnPc2	oblast
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
okolí	okolí	k1gNnSc1	okolí
gen	gen	kA	gen
XIST	XIST	kA	XIST
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sobecká	sobecký	k2eAgFnSc1d1	sobecká
RNA	RNA	kA	RNA
===	===	k?	===
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
molekuly	molekula	k1gFnPc1	molekula
RNA	RNA	kA	RNA
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vnímat	vnímat	k5eAaImF	vnímat
jako	jako	k9	jako
molekulární	molekulární	k2eAgMnPc4d1	molekulární
parazity	parazit	k1gMnPc4	parazit
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
totiž	totiž	k9	totiž
přítomny	přítomen	k2eAgInPc1d1	přítomen
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
hostitele	hostitel	k1gMnSc2	hostitel
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
dokonce	dokonce	k9	dokonce
kódovány	kódovat	k5eAaBmNgInP	kódovat
jeho	jeho	k3xOp3gInSc7	jeho
vlastním	vlastní	k2eAgInSc7d1	vlastní
genomem	genom	k1gInSc7	genom
<g/>
,	,	kIx,	,
a	a	k8xC	a
často	často	k6eAd1	často
se	se	k3xPyFc4	se
množí	množit	k5eAaImIp3nP	množit
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
úkor	úkor	k1gInSc4	úkor
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
RNA	RNA	kA	RNA
viry	vira	k1gFnSc2	vira
a	a	k8xC	a
retroviry	retrovira	k1gFnSc2	retrovira
<g/>
,	,	kIx,	,
retrotranspozony	retrotranspozona	k1gFnSc2	retrotranspozona
a	a	k8xC	a
sebevystřihující	sebevystřihující	k2eAgInPc4d1	sebevystřihující
introny	intron	k1gInPc4	intron
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
často	často	k6eAd1	často
zabírají	zabírat	k5eAaImIp3nP	zabírat
významnou	významný	k2eAgFnSc4d1	významná
část	část	k1gFnSc4	část
hostitelského	hostitelský	k2eAgInSc2d1	hostitelský
genomu	genom	k1gInSc2	genom
<g/>
,	,	kIx,	,
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
zabírá	zabírat	k5eAaImIp3nS	zabírat
nekódující	kódující	k2eNgFnSc1d1	nekódující
DNA	dna	k1gFnSc1	dna
vniklá	vniklý	k2eAgFnSc1d1	vniklá
činností	činnost	k1gFnSc7	činnost
těchto	tento	k3xDgInPc2	tento
elementů	element	k1gInPc2	element
téměř	téměř	k6eAd1	téměř
polovinu	polovina	k1gFnSc4	polovina
jeho	on	k3xPp3gInSc2	on
genomu	genom	k1gInSc2	genom
<g/>
.	.	kIx.	.
<g/>
RNA	RNA	kA	RNA
viry	vira	k1gFnSc2	vira
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc2	smysl
jsou	být	k5eAaImIp3nP	být
zástupci	zástupce	k1gMnPc1	zástupce
virů	vir	k1gInPc2	vir
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
RNA	RNA	kA	RNA
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
nositelka	nositelka	k1gFnSc1	nositelka
genetické	genetický	k2eAgFnPc1d1	genetická
informace	informace	k1gFnPc1	informace
(	(	kIx(	(
<g/>
viry	vir	k1gInPc1	vir
způsobující	způsobující	k2eAgFnSc4d1	způsobující
chřipku	chřipka	k1gFnSc4	chřipka
<g/>
,	,	kIx,	,
klíšťovou	klíšťový	k2eAgFnSc4d1	klíšťová
encefalitidu	encefalitida	k1gFnSc4	encefalitida
<g/>
,	,	kIx,	,
SARS	SARS	kA	SARS
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Retroviry	Retrovira	k1gFnPc1	Retrovira
(	(	kIx(	(
<g/>
např.	např.	kA	např.
HIV	HIV	kA	HIV
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
genomu	genom	k1gInSc6	genom
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
využívají	využívat	k5eAaPmIp3nP	využívat
zpětný	zpětný	k2eAgInSc4d1	zpětný
přepis	přepis	k1gInSc4	přepis
RNA	RNA	kA	RNA
<g/>
→	→	k?	→
<g/>
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
svůj	svůj	k3xOyFgInSc4	svůj
genom	genom	k1gInSc4	genom
vložit	vložit	k5eAaPmF	vložit
do	do	k7c2	do
genomu	genom	k1gInSc2	genom
hostitele	hostitel	k1gMnSc2	hostitel
a	a	k8xC	a
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
tvoří	tvořit	k5eAaImIp3nP	tvořit
jejich	jejich	k3xOp3gInPc1	jejich
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
%	%	kIx~	%
genomu	genom	k1gInSc2	genom
<g/>
.	.	kIx.	.
</s>
<s>
Retroviry	Retrovira	k1gFnPc1	Retrovira
mohou	moct	k5eAaImIp3nP	moct
ztratit	ztratit	k5eAaPmF	ztratit
schopnost	schopnost	k1gFnSc4	schopnost
se	se	k3xPyFc4	se
šířit	šířit	k5eAaImF	šířit
mezi	mezi	k7c7	mezi
hostiteli	hostitel	k1gMnPc7	hostitel
a	a	k8xC	a
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
endogenními	endogenní	k2eAgInPc7d1	endogenní
retroviry	retrovir	k1gInPc7	retrovir
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgInPc1d1	lidský
endogenní	endogenní	k2eAgInPc1d1	endogenní
retroviry	retrovir	k1gInPc1	retrovir
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
neaktivní	aktivní	k2eNgFnPc1d1	neaktivní
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
schopny	schopen	k2eAgFnPc1d1	schopna
se	se	k3xPyFc4	se
šířit	šířit	k5eAaImF	šířit
pouze	pouze	k6eAd1	pouze
mezi	mezi	k7c7	mezi
pohlavními	pohlavní	k2eAgFnPc7d1	pohlavní
buňkami	buňka	k1gFnPc7	buňka
svého	svůj	k1gMnSc2	svůj
hostitele	hostitel	k1gMnSc2	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
případ	případ	k1gInSc1	případ
lidského	lidský	k2eAgInSc2d1	lidský
endogenního	endogenní	k2eAgInSc2d1	endogenní
retroviru	retrovir	k1gInSc2	retrovir
je	být	k5eAaImIp3nS	být
HERV-W	HERV-W	k1gFnSc3	HERV-W
kódující	kódující	k2eAgInSc4d1	kódující
protein	protein	k1gInSc4	protein
syncytin	syncytin	k1gInSc1	syncytin
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc1	tento
protein	protein	k1gInSc1	protein
původně	původně	k6eAd1	původně
sloužil	sloužit	k5eAaImAgInS	sloužit
viru	vira	k1gFnSc4	vira
pro	pro	k7c4	pro
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
lidský	lidský	k2eAgInSc1d1	lidský
organismus	organismus	k1gInSc1	organismus
jej	on	k3xPp3gMnSc4	on
ale	ale	k9	ale
převzal	převzít	k5eAaPmAgInS	převzít
a	a	k8xC	a
využívá	využívat	k5eAaPmIp3nS	využívat
jej	on	k3xPp3gMnSc4	on
pro	pro	k7c4	pro
fúzování	fúzování	k1gNnSc4	fúzování
buněk	buňka	k1gFnPc2	buňka
(	(	kIx(	(
<g/>
trofoblastů	trofoblast	k1gInPc2	trofoblast
<g/>
)	)	kIx)	)
v	v	k7c6	v
placentě	placenta	k1gFnSc6	placenta
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
jednodušší	jednoduchý	k2eAgInPc1d2	jednodušší
než	než	k8xS	než
endogenní	endogenní	k2eAgInPc1d1	endogenní
retroviry	retrovir	k1gInPc1	retrovir
jsou	být	k5eAaImIp3nP	být
retrotranspozony	retrotranspozon	k1gInPc4	retrotranspozon
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
úseky	úsek	k1gInPc1	úsek
DNA	dno	k1gNnPc4	dno
schopné	schopný	k2eAgFnPc1d1	schopná
se	se	k3xPyFc4	se
přepsat	přepsat	k5eAaPmF	přepsat
do	do	k7c2	do
RNA	RNA	kA	RNA
a	a	k8xC	a
pak	pak	k6eAd1	pak
zpětně	zpětně	k6eAd1	zpětně
přepsat	přepsat	k5eAaPmF	přepsat
do	do	k7c2	do
DNA	dno	k1gNnSc2	dno
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gFnPc2	jejich
činností	činnost	k1gFnPc2	činnost
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
asi	asi	k9	asi
42	[number]	k4	42
%	%	kIx~	%
lidského	lidský	k2eAgInSc2d1	lidský
genomu	genom	k1gInSc2	genom
<g/>
.	.	kIx.	.
<g/>
Geny	gen	k1gInPc1	gen
kódující	kódující	k2eAgInPc1d1	kódující
proteiny	protein	k1gInPc1	protein
často	často	k6eAd1	často
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
introny	intron	k1gInPc1	intron
<g/>
,	,	kIx,	,
nekódující	kódující	k2eNgFnPc1d1	nekódující
oblasti	oblast	k1gFnPc1	oblast
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
musí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
zrání	zrání	k1gNnSc2	zrání
mRNA	mRNA	k?	mRNA
vystřihnuty	vystřihnout	k5eAaPmNgInP	vystřihnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
vystřihnutí	vystřihnutí	k1gNnSc1	vystřihnutí
katalyzováno	katalyzován	k2eAgNnSc1d1	katalyzován
samotným	samotný	k2eAgInSc7d1	samotný
intronem	intron	k1gInSc7	intron
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
sebevystřihující	sebevystřihující	k2eAgInPc4d1	sebevystřihující
introny	intron	k1gInPc4	intron
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
kódují	kódovat	k5eAaBmIp3nP	kódovat
proteiny	protein	k1gInPc1	protein
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
jim	on	k3xPp3gMnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
šířit	šířit	k5eAaImF	šířit
se	se	k3xPyFc4	se
genomem	genom	k1gInSc7	genom
hostitele	hostitel	k1gMnSc2	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
mechanismu	mechanismus	k1gInSc2	mechanismus
sestřihu	sestřih	k1gInSc2	sestřih
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
na	na	k7c4	na
typ	typ	k1gInSc4	typ
I	I	kA	I
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
následně	následně	k6eAd1	následně
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
množení	množení	k1gNnSc4	množení
zneužívá	zneužívat	k5eAaImIp3nS	zneužívat
mechanismus	mechanismus	k1gInSc1	mechanismus
opravy	oprava	k1gFnSc2	oprava
DNA	DNA	kA	DNA
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
opraví	opravit	k5eAaPmIp3nP	opravit
<g/>
"	"	kIx"	"
úsek	úsek	k1gInSc1	úsek
hostitelské	hostitelský	k2eAgFnSc2d1	hostitelská
DNA	dno	k1gNnSc2	dno
podle	podle	k7c2	podle
své	svůj	k3xOyFgFnSc2	svůj
sekvence	sekvence	k1gFnSc2	sekvence
<g/>
;	;	kIx,	;
typ	typ	k1gInSc1	typ
II	II	kA	II
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
šíří	šířit	k5eAaImIp3nS	šířit
zpětným	zpětný	k2eAgInSc7d1	zpětný
přepisem	přepis	k1gInSc7	přepis
RNA	RNA	kA	RNA
<g/>
→	→	k?	→
<g/>
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
se	se	k3xPyFc4	se
z	z	k7c2	z
mRNA	mRNA	k?	mRNA
vystřihnout	vystřihnout	k5eAaPmF	vystřihnout
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
většinou	většinou	k6eAd1	většinou
významně	významně	k6eAd1	významně
nepoškozují	poškozovat	k5eNaImIp3nP	poškozovat
hostitele	hostitel	k1gMnSc4	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
mechanismus	mechanismus	k1gInSc1	mechanismus
vyštěpování	vyštěpování	k1gNnSc2	vyštěpování
sebevystřihujících	sebevystřihující	k2eAgInPc2d1	sebevystřihující
intronů	intron	k1gInPc2	intron
typu	typ	k1gInSc2	typ
II	II	kA	II
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc1d1	podobný
eukaryotnímu	eukaryotní	k2eAgInSc3d1	eukaryotní
spliceozomu	spliceozom	k1gInSc3	spliceozom
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
eukaryotní	eukaryotní	k2eAgFnPc1d1	eukaryotní
buňky	buňka	k1gFnPc1	buňka
získaly	získat	k5eAaPmAgFnP	získat
své	své	k1gNnSc4	své
introny	intron	k1gInPc4	intron
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
sebevystřihující	sebevystřihující	k2eAgInPc1d1	sebevystřihující
introny	intron	k1gInPc1	intron
typu	typ	k1gInSc2	typ
II	II	kA	II
rozšířily	rozšířit	k5eAaPmAgInP	rozšířit
z	z	k7c2	z
předchůdce	předchůdce	k1gMnSc2	předchůdce
mitochondrie	mitochondrie	k1gFnSc2	mitochondrie
do	do	k7c2	do
buňky	buňka	k1gFnSc2	buňka
eukaryot	eukaryota	k1gFnPc2	eukaryota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osudy	osud	k1gInPc7	osud
RNA	RNA	kA	RNA
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Syntéza	syntéza	k1gFnSc1	syntéza
RNA	RNA	kA	RNA
a	a	k8xC	a
reverzní	reverzní	k2eAgFnSc2d1	reverzní
transkripce	transkripce	k1gFnSc2	transkripce
===	===	k?	===
</s>
</p>
<p>
<s>
RNA	RNA	kA	RNA
je	být	k5eAaImIp3nS	být
syntetizovaná	syntetizovaný	k2eAgFnSc1d1	syntetizovaná
enzymy	enzym	k1gInPc4	enzym
RNA	RNA	kA	RNA
polymerázami	polymeráza	k1gFnPc7	polymeráza
(	(	kIx(	(
<g/>
RNApol	RNApol	k1gInSc1	RNApol
<g/>
,	,	kIx,	,
RNAP	RNAP	kA	RNAP
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
matrice	matrice	k1gFnSc2	matrice
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
u	u	k7c2	u
virů	vir	k1gInPc2	vir
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
RNA	RNA	kA	RNA
polymerázy	polymeráza	k1gFnPc4	polymeráza
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
transkripčních	transkripční	k2eAgInPc2d1	transkripční
faktorů	faktor	k1gInPc2	faktor
rozpoznávají	rozpoznávat	k5eAaImIp3nP	rozpoznávat
specifické	specifický	k2eAgInPc1d1	specifický
úseky	úsek	k1gInPc1	úsek
DNA	DNA	kA	DNA
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
promotory	promotor	k1gInPc1	promotor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
označují	označovat	k5eAaImIp3nP	označovat
počátek	počátek	k1gInSc4	počátek
přepisu	přepis	k1gInSc2	přepis
DNA	DNA	kA	DNA
na	na	k7c6	na
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
případě	případ	k1gInSc6	případ
prokaryot	prokaryota	k1gFnPc2	prokaryota
a	a	k8xC	a
semiautonomních	semiautonomní	k2eAgFnPc2d1	semiautonomní
organel	organela	k1gFnPc2	organela
eukaryot	eukaryota	k1gFnPc2	eukaryota
jsou	být	k5eAaImIp3nP	být
veškeré	veškerý	k3xTgInPc1	veškerý
RNA	RNA	kA	RNA
syntetizovány	syntetizovat	k5eAaImNgFnP	syntetizovat
jedinou	jediný	k2eAgFnSc7d1	jediná
RNA	RNA	kA	RNA
polymerázou	polymeráza	k1gFnSc7	polymeráza
<g/>
,	,	kIx,	,
u	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
více	hodně	k6eAd2	hodně
typů	typ	k1gInPc2	typ
<g/>
:	:	kIx,	:
RNA	RNA	kA	RNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
I	i	k9	i
syntetizující	syntetizující	k2eAgFnSc1d1	syntetizující
rRNA	rRNA	k?	rRNA
<g/>
,	,	kIx,	,
RNA	RNA	kA	RNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
II	II	kA	II
syntetizující	syntetizující	k2eAgFnSc2d1	syntetizující
prekurzor	prekurzor	k1gInSc4	prekurzor
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
většinu	většina	k1gFnSc4	většina
snRNA	snRNA	k?	snRNA
a	a	k8xC	a
miRNA	miRNA	k?	miRNA
a	a	k8xC	a
RNA	RNA	kA	RNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
III	III	kA	III
syntetizující	syntetizující	k2eAgFnSc1d1	syntetizující
tRNA	trnout	k5eAaImSgInS	trnout
a	a	k8xC	a
5S	[number]	k4	5S
rRNA	rRNA	k?	rRNA
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
těchto	tento	k3xDgFnPc2	tento
tří	tři	k4xCgFnPc2	tři
polymeráz	polymeráza	k1gFnPc2	polymeráza
společných	společný	k2eAgFnPc2d1	společná
pro	pro	k7c4	pro
eukaryota	eukaryot	k1gMnSc4	eukaryot
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
známá	známý	k2eAgFnSc1d1	známá
RNA	RNA	kA	RNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
IV	Iva	k1gFnPc2	Iva
a	a	k8xC	a
V	V	kA	V
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
syntetizují	syntetizovat	k5eAaImIp3nP	syntetizovat
siRNA	siRNA	k?	siRNA
v	v	k7c6	v
rostlinných	rostlinný	k2eAgFnPc6d1	rostlinná
buňkách	buňka	k1gFnPc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
RNA	RNA	kA	RNA
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
syntetizována	syntetizován	k2eAgFnSc1d1	syntetizována
i	i	k9	i
podle	podle	k7c2	podle
RNA	RNA	kA	RNA
templátu	templát	k1gInSc2	templát
pomocí	pomoc	k1gFnPc2	pomoc
RNA	RNA	kA	RNA
dependentních	dependentní	k2eAgFnPc2d1	dependentní
RNA	RNA	kA	RNA
polymeráz	polymeráza	k1gFnPc2	polymeráza
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
replikovat	replikovat	k5eAaImF	replikovat
viry	vir	k1gInPc4	vir
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
genom	genom	k1gInSc1	genom
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
právě	právě	k9	právě
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Syntéza	syntéza	k1gFnSc1	syntéza
RNA	RNA	kA	RNA
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
i	i	k9	i
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
replikace	replikace	k1gFnSc2	replikace
DNA	DNA	kA	DNA
–	–	k?	–
DNA	dna	k1gFnSc1	dna
polymeráza	polymeráza	k1gFnSc1	polymeráza
není	být	k5eNaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
započít	započít	k5eAaPmF	započít
syntézu	syntéza	k1gFnSc4	syntéza
nového	nový	k2eAgNnSc2d1	nové
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
speciální	speciální	k2eAgFnSc1d1	speciální
DNA	dna	k1gFnSc1	dna
dependentní	dependentní	k2eAgFnSc1d1	dependentní
RNA	RNA	kA	RNA
polymeráza	polymeráza	k1gFnSc1	polymeráza
zvaná	zvaný	k2eAgFnSc1d1	zvaná
primáza	primáza	k1gFnSc1	primáza
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
krátký	krátký	k2eAgInSc4d1	krátký
úsek	úsek	k1gInSc4	úsek
RNA	RNA	kA	RNA
komplementární	komplementární	k2eAgInPc4d1	komplementární
s	s	k7c7	s
DNA	DNA	kA	DNA
(	(	kIx(	(
<g/>
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
primer	primera	k1gFnPc2	primera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yIgInSc4	který
může	moct	k5eAaImIp3nS	moct
DNA	dna	k1gFnSc1	dna
polymeráza	polymeráza	k1gFnSc1	polymeráza
navázat	navázat	k5eAaPmF	navázat
<g/>
.	.	kIx.	.
</s>
<s>
RNA	RNA	kA	RNA
je	být	k5eAaImIp3nS	být
následně	následně	k6eAd1	následně
z	z	k7c2	z
DNA	DNA	kA	DNA
odstraněna	odstranit	k5eAaPmNgFnS	odstranit
a	a	k8xC	a
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
DNA	dna	k1gFnSc1	dna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RNA	RNA	kA	RNA
hraje	hrát	k5eAaImIp3nS	hrát
také	také	k9	také
roli	role	k1gFnSc4	role
v	v	k7c6	v
případě	případ	k1gInSc6	případ
syntézy	syntéza	k1gFnSc2	syntéza
DNA	DNA	kA	DNA
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
nazývaném	nazývaný	k2eAgInSc6d1	nazývaný
reverzní	reverzní	k2eAgFnSc4d1	reverzní
transkripce	transkripce	k1gFnPc4	transkripce
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
templát	templát	k1gInSc1	templát
<g/>
.	.	kIx.	.
</s>
<s>
Eukaryotní	Eukaryotní	k2eAgFnPc1d1	Eukaryotní
buňky	buňka	k1gFnPc1	buňka
využívají	využívat	k5eAaImIp3nP	využívat
reverzní	reverzní	k2eAgFnSc4d1	reverzní
transkripci	transkripce	k1gFnSc4	transkripce
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
svých	svůj	k3xOyFgFnPc2	svůj
telomer	telomera	k1gFnPc2	telomera
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
repetitivní	repetitivní	k2eAgFnPc1d1	repetitivní
oblasti	oblast	k1gFnPc1	oblast
chránící	chránící	k2eAgFnSc2d1	chránící
koncové	koncový	k2eAgFnSc2d1	koncová
oblasti	oblast	k1gFnSc2	oblast
chromozomů	chromozom	k1gInPc2	chromozom
<g/>
.	.	kIx.	.
</s>
<s>
Reverzní	reverzní	k2eAgFnSc4d1	reverzní
transkripci	transkripce	k1gFnSc4	transkripce
využívají	využívat	k5eAaPmIp3nP	využívat
také	také	k9	také
retroviry	retrovira	k1gFnPc1	retrovira
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
přepisují	přepisovat	k5eAaImIp3nP	přepisovat
svůj	svůj	k3xOyFgInSc4	svůj
genom	genom	k1gInSc4	genom
kódovaný	kódovaný	k2eAgInSc4d1	kódovaný
v	v	k7c4	v
RNA	RNA	kA	RNA
do	do	k7c2	do
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
integraci	integrace	k1gFnSc4	integrace
do	do	k7c2	do
hostitelského	hostitelský	k2eAgInSc2d1	hostitelský
genomu	genom	k1gInSc2	genom
<g/>
;	;	kIx,	;
virový	virový	k2eAgInSc1d1	virový
genom	genom	k1gInSc1	genom
je	být	k5eAaImIp3nS	být
následně	následně	k6eAd1	následně
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
procesem	proces	k1gInSc7	proces
transkripce	transkripce	k1gFnSc2	transkripce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Úpravy	úprava	k1gFnSc2	úprava
RNA	RNA	kA	RNA
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
lokalizace	lokalizace	k1gFnSc1	lokalizace
===	===	k?	===
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
5	[number]	k4	5
<g/>
'	'	kIx"	'
konec	konec	k1gInSc1	konec
mRNA	mRNA	k?	mRNA
u	u	k7c2	u
archeí	archeí	k1gFnSc2	archeí
není	být	k5eNaImIp3nS	být
upraven	upravit	k5eAaPmNgInS	upravit
a	a	k8xC	a
u	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
jej	on	k3xPp3gInSc4	on
chrání	chránit	k5eAaImIp3nS	chránit
pouze	pouze	k6eAd1	pouze
trifosfátová	trifosfátový	k2eAgFnSc1d1	trifosfátový
skupina	skupina	k1gFnSc1	skupina
a	a	k8xC	a
u	u	k7c2	u
obou	dva	k4xCgFnPc2	dva
skupin	skupina	k1gFnPc2	skupina
je	být	k5eAaImIp3nS	být
polyadenylace	polyadenylace	k1gFnSc1	polyadenylace
3	[number]	k4	3
<g/>
'	'	kIx"	'
konce	konec	k1gInSc2	konec
signál	signál	k1gInSc1	signál
k	k	k7c3	k
degradaci	degradace	k1gFnSc3	degradace
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
eukaryotních	eukaryotní	k2eAgInPc2d1	eukaryotní
mRNA	mRNA	k?	mRNA
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
molekul	molekula	k1gFnPc2	molekula
RNA	RNA	kA	RNA
syntetizovaných	syntetizovaný	k2eAgFnPc2d1	syntetizovaná
RNA	RNA	kA	RNA
polymerázou	polymeráza	k1gFnSc7	polymeráza
II	II	kA	II
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
řada	řada	k1gFnSc1	řada
posttranskripčních	posttranskripční	k2eAgFnPc2d1	posttranskripční
úprav	úprava	k1gFnPc2	úprava
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
většinou	většinou	k6eAd1	většinou
probíhají	probíhat	k5eAaImIp3nP	probíhat
už	už	k6eAd1	už
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
syntézy	syntéza	k1gFnSc2	syntéza
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
vlastně	vlastně	k9	vlastně
o	o	k7c4	o
úpravy	úprava	k1gFnPc4	úprava
ko-transkripční	koranskripční	k2eAgFnPc4d1	ko-transkripční
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
úpravy	úprava	k1gFnPc1	úprava
jsou	být	k5eAaImIp3nP	být
přidání	přidání	k1gNnSc4	přidání
5	[number]	k4	5
<g/>
'	'	kIx"	'
čepičky	čepička	k1gFnPc1	čepička
<g/>
,	,	kIx,	,
polyadenylace	polyadenylace	k1gFnPc1	polyadenylace
a	a	k8xC	a
splicing	splicing	k1gInSc1	splicing
<g/>
.	.	kIx.	.
</s>
<s>
Spřažení	spřažení	k1gNnSc1	spřažení
transkripce	transkripce	k1gFnSc2	transkripce
a	a	k8xC	a
úprav	úprava	k1gFnPc2	úprava
RNA	RNA	kA	RNA
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
fosforylace	fosforylace	k1gFnPc4	fosforylace
C-terminální	Cerminální	k2eAgFnSc2d1	C-terminální
domény	doména	k1gFnSc2	doména
RNA	RNA	kA	RNA
polymerázy	polymeráza	k1gFnSc2	polymeráza
II	II	kA	II
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
během	během	k7c2	během
neaktivního	aktivní	k2eNgInSc2d1	neaktivní
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
začátku	začátek	k1gInSc2	začátek
transkripce	transkripce	k1gFnSc2	transkripce
(	(	kIx(	(
<g/>
signál	signál	k1gInSc1	signál
pro	pro	k7c4	pro
připojení	připojení	k1gNnSc4	připojení
čepičky	čepička	k1gFnSc2	čepička
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
elongační	elongační	k2eAgFnSc2d1	elongační
fáze	fáze	k1gFnSc2	fáze
transkripce	transkripce	k1gFnSc2	transkripce
(	(	kIx(	(
<g/>
vyhledávání	vyhledávání	k1gNnSc1	vyhledávání
míst	místo	k1gNnPc2	místo
sestřihu	sestřih	k1gInSc2	sestřih
<g/>
)	)	kIx)	)
a	a	k8xC	a
konce	konec	k1gInSc2	konec
transkripce	transkripce	k1gFnSc2	transkripce
(	(	kIx(	(
<g/>
rozštěpení	rozštěpení	k1gNnSc2	rozštěpení
rostoucí	rostoucí	k2eAgFnSc2d1	rostoucí
mRNA	mRNA	k?	mRNA
a	a	k8xC	a
signál	signál	k1gInSc1	signál
pro	pro	k7c4	pro
polyadenylaci	polyadenylace	k1gFnSc4	polyadenylace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
chybí	chybit	k5eAaPmIp3nS	chybit
některá	některý	k3yIgFnSc1	některý
z	z	k7c2	z
potřebných	potřebný	k2eAgFnPc2d1	potřebná
modifikací	modifikace	k1gFnPc2	modifikace
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
která	který	k3yQgFnSc1	který
stále	stále	k6eAd1	stále
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
nevyštěpené	vyštěpený	k2eNgInPc4d1	vyštěpený
introny	intron	k1gInPc4	intron
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
degradována	degradovat	k5eAaBmNgFnS	degradovat
jadernými	jaderný	k2eAgFnPc7d1	jaderná
RNázami	RNáza	k1gFnPc7	RNáza
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc4	některý
typy	typ	k1gInPc4	typ
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tRNA	trnout	k5eAaImSgInS	trnout
nebo	nebo	k8xC	nebo
rRNA	rRNA	k?	rRNA
<g/>
)	)	kIx)	)
u	u	k7c2	u
všech	všecek	k3xTgFnPc2	všecek
domén	doména	k1gFnPc2	doména
života	život	k1gInSc2	život
prochází	procházet	k5eAaImIp3nP	procházet
složitým	složitý	k2eAgInSc7d1	složitý
procesem	proces	k1gInSc7	proces
"	"	kIx"	"
<g/>
zrání	zrání	k1gNnSc1	zrání
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgNnSc6	který
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gFnPc3	jejich
značným	značný	k2eAgFnPc3d1	značná
úpravám	úprava	k1gFnPc3	úprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
prokaryota	prokaryota	k1gFnSc1	prokaryota
nemají	mít	k5eNaImIp3nP	mít
jádro	jádro	k1gNnSc1	jádro
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc4	jejich
RNA	RNA	kA	RNA
je	být	k5eAaImIp3nS	být
hned	hned	k6eAd1	hned
po	po	k7c6	po
syntéze	syntéza	k1gFnSc6	syntéza
dostupná	dostupný	k2eAgFnSc1d1	dostupná
v	v	k7c6	v
cytoplasmě	cytoplasma	k1gFnSc6	cytoplasma
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
mRNA	mRNA	k?	mRNA
překládaná	překládaný	k2eAgFnSc1d1	překládaná
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
je	být	k5eAaImIp3nS	být
RNA	RNA	kA	RNA
po	po	k7c6	po
kontrole	kontrola	k1gFnSc6	kontrola
své	svůj	k3xOyFgFnSc2	svůj
kvality	kvalita	k1gFnSc2	kvalita
exportována	exportovat	k5eAaBmNgFnS	exportovat
na	na	k7c4	na
cílové	cílový	k2eAgNnSc4d1	cílové
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
za	za	k7c4	za
samotný	samotný	k2eAgInSc4d1	samotný
export	export	k1gInSc4	export
jsou	být	k5eAaImIp3nP	být
zodpovědné	zodpovědný	k2eAgInPc1d1	zodpovědný
proteiny	protein	k1gInPc1	protein
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
ribonukleoproteinové	ribonukleoproteinový	k2eAgInPc1d1	ribonukleoproteinový
komplexy	komplex	k1gInPc1	komplex
s	s	k7c7	s
přenášenou	přenášený	k2eAgFnSc7d1	přenášená
mRNA	mRNA	k?	mRNA
<g/>
.	.	kIx.	.
</s>
<s>
Cílové	cílový	k2eAgNnSc4d1	cílové
místo	místo	k1gNnSc4	místo
určují	určovat	k5eAaImIp3nP	určovat
RNA	RNA	kA	RNA
vazebné	vazebný	k2eAgInPc1d1	vazebný
proteiny	protein	k1gInPc1	protein
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
rozpoznávají	rozpoznávat	k5eAaImIp3nP	rozpoznávat
signál	signál	k1gInSc4	signál
na	na	k7c4	na
RNA	RNA	kA	RNA
směřující	směřující	k2eAgInSc4d1	směřující
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
dané	daný	k2eAgNnSc4d1	dané
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
<g/>
,	,	kIx,	,
transportovány	transportován	k2eAgFnPc1d1	transportována
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc4	tento
pomocné	pomocný	k2eAgInPc4d1	pomocný
proteiny	protein	k1gInPc4	protein
a	a	k8xC	a
RNA	RNA	kA	RNA
se	se	k3xPyFc4	se
přesunuje	přesunovat	k5eAaImIp3nS	přesunovat
navázaná	navázaný	k2eAgFnSc1d1	navázaná
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
je	být	k5eAaImIp3nS	být
nejlépe	dobře	k6eAd3	dobře
pozorovatelný	pozorovatelný	k2eAgInSc1d1	pozorovatelný
u	u	k7c2	u
velkých	velký	k2eAgFnPc2d1	velká
a	a	k8xC	a
složitých	složitý	k2eAgFnPc2d1	složitá
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
vajíčka	vajíčko	k1gNnPc1	vajíčko
<g/>
,	,	kIx,	,
raná	raný	k2eAgNnPc1d1	rané
embrya	embryo	k1gNnPc1	embryo
nebo	nebo	k8xC	nebo
neurony	neuron	k1gInPc1	neuron
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
společný	společný	k2eAgInSc1d1	společný
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
typy	typ	k1gInPc4	typ
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vazba	vazba	k1gFnSc1	vazba
na	na	k7c4	na
proteiny	protein	k1gInPc4	protein
===	===	k?	===
</s>
</p>
<p>
<s>
Prakticky	prakticky	k6eAd1	prakticky
žádná	žádný	k3yNgFnSc1	žádný
RNA	RNA	kA	RNA
se	se	k3xPyFc4	se
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
nenachází	nacházet	k5eNaImIp3nS	nacházet
volně	volně	k6eAd1	volně
<g/>
,	,	kIx,	,
RNA	RNA	kA	RNA
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
komplexu	komplex	k1gInSc6	komplex
s	s	k7c7	s
proteiny	protein	k1gInPc7	protein
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
jako	jako	k9	jako
ribonukleoprotein	ribonukleoprotein	k1gMnSc1	ribonukleoprotein
<g/>
.	.	kIx.	.
</s>
<s>
Proteiny	protein	k1gInPc1	protein
vázající	vázající	k2eAgInPc1d1	vázající
se	se	k3xPyFc4	se
na	na	k7c6	na
RNA	RNA	kA	RNA
mají	mít	k5eAaImIp3nP	mít
řadu	řada	k1gFnSc4	řada
funkcí	funkce	k1gFnPc2	funkce
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k9	už
degradaci	degradace	k1gFnSc4	degradace
RNA	RNA	kA	RNA
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
ní	on	k3xPp3gFnSc7	on
<g/>
,	,	kIx,	,
modifikují	modifikovat	k5eAaBmIp3nP	modifikovat
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
adaptér	adaptér	k1gInSc1	adaptér
pro	pro	k7c4	pro
jaderný	jaderný	k2eAgInSc4d1	jaderný
export	export	k1gInSc4	export
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
udržení	udržení	k1gNnSc1	udržení
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
pro	pro	k7c4	pro
skládání	skládání	k1gNnSc4	skládání
do	do	k7c2	do
složitějších	složitý	k2eAgInPc2d2	složitější
ribonukleoproteinových	ribonukleoproteinův	k2eAgInPc2d1	ribonukleoproteinův
komplexů	komplex	k1gInPc2	komplex
<g/>
.	.	kIx.	.
<g/>
Proteinů	protein	k1gInPc2	protein
vázajících	vázající	k2eAgInPc2d1	vázající
RNA	RNA	kA	RNA
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
RNA-recognition	RNAecognition	k1gInSc1	RNA-recognition
motif	motif	k1gInSc4	motif
(	(	kIx(	(
<g/>
motiv	motiv	k1gInSc4	motiv
rozeznávající	rozeznávající	k2eAgInSc4d1	rozeznávající
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejčastějších	častý	k2eAgInPc2d3	nejčastější
strukturních	strukturní	k2eAgInPc2d1	strukturní
motivů	motiv	k1gInPc2	motiv
v	v	k7c6	v
lidských	lidský	k2eAgInPc6d1	lidský
proteinech	protein	k1gInPc6	protein
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ribonukleoproteiny	ribonukleoproteina	k1gFnPc1	ribonukleoproteina
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
složité	složitý	k2eAgNnSc1d1	složité
samy	sám	k3xTgFnPc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
ribozom	ribozom	k1gInSc1	ribozom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
u	u	k7c2	u
eukaryotních	eukaryotní	k2eAgInPc2d1	eukaryotní
organismů	organismus	k1gInPc2	organismus
tyto	tento	k3xDgInPc1	tento
komplexy	komplex	k1gInPc1	komplex
dále	daleko	k6eAd2	daleko
shlukují	shlukovat	k5eAaImIp3nP	shlukovat
do	do	k7c2	do
vyšších	vysoký	k2eAgInPc2d2	vyšší
útvarů	útvar	k1gInPc2	útvar
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
jadérko	jadérko	k1gNnSc1	jadérko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznik	vznik	k1gInSc1	vznik
těchto	tento	k3xDgFnPc2	tento
složitějších	složitý	k2eAgFnPc2d2	složitější
struktur	struktura	k1gFnPc2	struktura
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
nezbytností	nezbytnost	k1gFnSc7	nezbytnost
fyzicky	fyzicky	k6eAd1	fyzicky
oddělit	oddělit	k5eAaPmF	oddělit
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zefektivnit	zefektivnit	k5eAaPmF	zefektivnit
různé	různý	k2eAgInPc1d1	různý
probíhající	probíhající	k2eAgInPc1d1	probíhající
pochody	pochod	k1gInPc1	pochod
u	u	k7c2	u
složitějších	složitý	k2eAgFnPc2d2	složitější
eukaryotních	eukaryotní	k2eAgFnPc2d1	eukaryotní
buněk	buňka	k1gFnPc2	buňka
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
systému	systém	k1gInSc2	systém
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
váčků	váček	k1gInPc2	váček
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
odděleny	oddělit	k5eAaPmNgInP	oddělit
membránami	membrána	k1gFnPc7	membrána
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Uskladnění	uskladnění	k1gNnSc1	uskladnění
a	a	k8xC	a
kontrola	kontrola	k1gFnSc1	kontrola
mRNA	mRNA	k?	mRNA
v	v	k7c6	v
RNA	RNA	kA	RNA
granulích	granule	k1gFnPc6	granule
===	===	k?	===
</s>
</p>
<p>
<s>
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
není	být	k5eNaImIp3nS	být
buňkou	buňka	k1gFnSc7	buňka
překládána	překládán	k2eAgFnSc1d1	překládána
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
uložena	uložit	k5eAaPmNgFnS	uložit
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
RNA	RNA	kA	RNA
granulí	granule	k1gFnPc2	granule
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
cytoplazmě	cytoplazma	k1gFnSc6	cytoplazma
eukaryotních	eukaryotní	k2eAgFnPc2d1	eukaryotní
buněk	buňka	k1gFnPc2	buňka
často	často	k6eAd1	často
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jako	jako	k9	jako
dobře	dobře	k6eAd1	dobře
rozlišitelná	rozlišitelný	k2eAgFnSc1d1	rozlišitelná
granula	granula	k1gFnSc1	granula
tvořená	tvořený	k2eAgFnSc1d1	tvořená
RNA	RNA	kA	RNA
a	a	k8xC	a
proteiny	protein	k1gInPc1	protein
<g/>
.	.	kIx.	.
</s>
<s>
RNA	RNA	kA	RNA
granule	granule	k1gFnPc1	granule
regulují	regulovat	k5eAaImIp3nP	regulovat
translaci	translace	k1gFnSc4	translace
<g/>
,	,	kIx,	,
stabilitu	stabilita	k1gFnSc4	stabilita
a	a	k8xC	a
určují	určovat	k5eAaImIp3nP	určovat
lokalizaci	lokalizace	k1gFnSc4	lokalizace
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
obsažená	obsažený	k2eAgFnSc1d1	obsažená
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
mRNA	mRNA	k?	mRNA
typicky	typicky	k6eAd1	typicky
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
ribozomální	ribozomální	k2eAgFnPc1d1	ribozomální
podjednotky	podjednotka	k1gFnPc1	podjednotka
<g/>
,	,	kIx,	,
translační	translační	k2eAgInPc1d1	translační
faktory	faktor	k1gInPc1	faktor
<g/>
,	,	kIx,	,
enzymy	enzym	k1gInPc1	enzym
zodpovědné	zodpovědný	k2eAgInPc1d1	zodpovědný
za	za	k7c4	za
degradaci	degradace	k1gFnSc4	degradace
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
komponenty	komponenta	k1gFnPc4	komponenta
RNA	RNA	kA	RNA
interference	interference	k1gFnSc2	interference
<g/>
,	,	kIx,	,
helikázy	helikáza	k1gFnSc2	helikáza
<g/>
,	,	kIx,	,
strukturní	strukturní	k2eAgInPc4d1	strukturní
proteiny	protein	k1gInPc4	protein
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
RNA-vazebné	RNAazebný	k2eAgInPc4d1	RNA-vazebný
proteiny	protein	k1gInPc4	protein
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejdůležitější	důležitý	k2eAgNnSc4d3	nejdůležitější
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Granule	granule	k1gFnSc1	granule
v	v	k7c6	v
zárodečných	zárodečný	k2eAgFnPc6d1	zárodečná
buňkách	buňka	k1gFnPc6	buňka
(	(	kIx(	(
<g/>
germ	germa	k1gFnPc2	germa
cell	cello	k1gNnPc2	cello
granules	granules	k1gInSc1	granules
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
především	především	k9	především
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
jejich	jejich	k3xOp3gInSc2	jejich
vývoje	vývoj	k1gInSc2	vývoj
a	a	k8xC	a
následně	následně	k6eAd1	následně
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
v	v	k7c6	v
oocytech	oocyt	k1gInPc6	oocyt
<g/>
.	.	kIx.	.
</s>
<s>
Nesou	nést	k5eAaImIp3nP	nést
vybrané	vybraný	k2eAgFnSc2d1	vybraná
molekuly	molekula	k1gFnSc2	molekula
mRNA	mRNA	k?	mRNA
kódující	kódující	k2eAgInPc1d1	kódující
proteiny	protein	k1gInPc1	protein
nezbytné	nezbytný	k2eAgInPc1d1	nezbytný
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
embrya	embryo	k1gNnSc2	embryo
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgInP	uložit
v	v	k7c6	v
neaktivním	aktivní	k2eNgInSc6d1	neaktivní
stavu	stav	k1gInSc6	stav
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
budou	být	k5eAaImBp3nP	být
potřeba	potřeba	k6eAd1	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
také	také	k9	také
nesou	nést	k5eAaImIp3nP	nést
aparát	aparát	k1gInSc4	aparát
RNA	RNA	kA	RNA
interference	interference	k1gFnPc1	interference
<g/>
,	,	kIx,	,
především	především	k9	především
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
piRNA	piRNA	k?	piRNA
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
inaktivují	inaktivovat	k5eAaBmIp3nP	inaktivovat
transpozony	transpozon	k1gInPc4	transpozon
a	a	k8xC	a
brání	bránit	k5eAaImIp3nS	bránit
tak	tak	k9	tak
genom	genom	k1gInSc1	genom
před	před	k7c7	před
poškozením	poškození	k1gNnSc7	poškození
<g/>
.	.	kIx.	.
<g/>
Stresová	stresový	k2eAgNnPc4d1	stresové
tělíska	tělísko	k1gNnPc4	tělísko
vznikají	vznikat	k5eAaImIp3nP	vznikat
při	při	k7c6	při
vystavení	vystavení	k1gNnSc6	vystavení
buněk	buňka	k1gFnPc2	buňka
stresu	stres	k1gInSc2	stres
a	a	k8xC	a
soustřeďují	soustřeďovat	k5eAaImIp3nP	soustřeďovat
v	v	k7c6	v
sobě	se	k3xPyFc3	se
mRNA	mRNA	k?	mRNA
kódující	kódující	k2eAgInPc4d1	kódující
běžné	běžný	k2eAgInPc4d1	běžný
buněčné	buněčný	k2eAgInPc4d1	buněčný
proteiny	protein	k1gInPc4	protein
<g/>
.	.	kIx.	.
</s>
<s>
Nesou	nést	k5eAaImIp3nP	nést
ribozomy	ribozom	k1gInPc1	ribozom
bezprostředně	bezprostředně	k6eAd1	bezprostředně
připravené	připravený	k2eAgNnSc1d1	připravené
překládat	překládat	k5eAaImF	překládat
nesenou	nesený	k2eAgFnSc4d1	nesená
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
trvání	trvání	k1gNnSc2	trvání
stresu	stres	k1gInSc2	stres
uskladněné	uskladněný	k2eAgNnSc1d1	uskladněné
v	v	k7c6	v
neaktivní	aktivní	k2eNgFnSc6d1	neaktivní
stavu	stav	k1gInSc3	stav
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
poškození	poškození	k1gNnSc3	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
fyzicky	fyzicky	k6eAd1	fyzicky
interagují	interagovat	k5eAaPmIp3nP	interagovat
s	s	k7c7	s
P-tělísky	Pělísek	k1gInPc7	P-tělísek
a	a	k8xC	a
předávají	předávat	k5eAaImIp3nP	předávat
jim	on	k3xPp3gMnPc3	on
RNA	RNA	kA	RNA
určenou	určený	k2eAgFnSc7d1	určená
k	k	k7c3	k
degradaci	degradace	k1gFnSc3	degradace
<g/>
.	.	kIx.	.
<g/>
P-tělíska	Pělíska	k1gFnSc1	P-tělíska
(	(	kIx(	(
<g/>
processing	processing	k1gInSc1	processing
bodies	bodies	k1gInSc1	bodies
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
somatických	somatický	k2eAgFnPc6d1	somatická
buňkách	buňka	k1gFnPc6	buňka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
degradace	degradace	k1gFnSc1	degradace
mRNA	mRNA	k?	mRNA
a	a	k8xC	a
zprostředkovávají	zprostředkovávat	k5eAaImIp3nP	zprostředkovávat
RNA	RNA	kA	RNA
silencing	silencing	k1gInSc1	silencing
pomocí	pomocí	k7c2	pomocí
miRNA	miRNA	k?	miRNA
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
schopnosti	schopnost	k1gFnSc3	schopnost
degradovat	degradovat	k5eAaBmF	degradovat
RNA	RNA	kA	RNA
slouží	sloužit	k5eAaImIp3nS	sloužit
také	také	k9	také
ke	k	k7c3	k
kontrole	kontrola	k1gFnSc3	kontrola
kvality	kvalita	k1gFnSc2	kvalita
buněčných	buněčný	k2eAgInPc2d1	buněčný
mRNA	mRNA	k?	mRNA
<g/>
.	.	kIx.	.
<g/>
Neuronální	neuronální	k2eAgFnSc1d1	neuronální
granule	granule	k1gFnSc1	granule
slouží	sloužit	k5eAaImIp3nS	sloužit
neuronům	neuron	k1gInPc3	neuron
pro	pro	k7c4	pro
transport	transport	k1gInSc4	transport
mRNA	mRNA	k?	mRNA
k	k	k7c3	k
jejich	jejich	k3xOp3gInPc3	jejich
axonům	axon	k1gInPc3	axon
<g/>
.	.	kIx.	.
</s>
<s>
Nesou	nést	k5eAaImIp3nP	nést
ribozomy	ribozom	k1gInPc1	ribozom
a	a	k8xC	a
faktory	faktor	k1gInPc1	faktor
pro	pro	k7c4	pro
zahájení	zahájení	k1gNnSc4	zahájení
translace	translace	k1gFnSc2	translace
<g/>
,	,	kIx,	,
té	ten	k3xDgFnSc3	ten
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
zabráněno	zabráněn	k2eAgNnSc1d1	zabráněno
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
tyto	tento	k3xDgFnPc1	tento
granule	granule	k1gFnPc1	granule
nejsou	být	k5eNaImIp3nP	být
dopraveny	dopravit	k5eAaPmNgFnP	dopravit
na	na	k7c4	na
správné	správný	k2eAgNnSc4d1	správné
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Neuronální	neuronální	k2eAgFnPc1d1	neuronální
granule	granule	k1gFnPc1	granule
hrají	hrát	k5eAaImIp3nP	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
regeneraci	regenerace	k1gFnSc6	regenerace
neuronů	neuron	k1gInPc2	neuron
a	a	k8xC	a
poruchy	porucha	k1gFnSc2	porucha
v	v	k7c6	v
transportu	transport	k1gInSc6	transport
mRNA	mRNA	k?	mRNA
jsou	být	k5eAaImIp3nP	být
spojeny	spojen	k2eAgInPc1d1	spojen
s	s	k7c7	s
neuronálními	neuronální	k2eAgFnPc7d1	neuronální
poruchami	porucha	k1gFnPc7	porucha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Degradace	degradace	k1gFnSc2	degradace
RNA	RNA	kA	RNA
===	===	k?	===
</s>
</p>
<p>
<s>
Degradace	degradace	k1gFnSc1	degradace
RNA	RNA	kA	RNA
pomocí	pomocí	k7c2	pomocí
enzymů	enzym	k1gInPc2	enzym
ribonukleáz	ribonukleáza	k1gFnPc2	ribonukleáza
(	(	kIx(	(
<g/>
RNáz	RNáz	k1gInSc1	RNáz
<g/>
)	)	kIx)	)
slouží	sloužit	k5eAaImIp3nS	sloužit
buňce	buňka	k1gFnSc3	buňka
jako	jako	k8xC	jako
regulační	regulační	k2eAgInSc4d1	regulační
a	a	k8xC	a
kontrolní	kontrolní	k2eAgInSc4d1	kontrolní
mechanismus	mechanismus	k1gInSc4	mechanismus
pro	pro	k7c4	pro
odstranění	odstranění	k1gNnSc4	odstranění
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
poškozená	poškozený	k2eAgFnSc1d1	poškozená
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nadále	nadále	k6eAd1	nadále
není	být	k5eNaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pro	pro	k7c4	pro
maturaci	maturace	k1gFnSc4	maturace
RNA	RNA	kA	RNA
a	a	k8xC	a
jako	jako	k8xC	jako
obrana	obrana	k1gFnSc1	obrana
proti	proti	k7c3	proti
RNA	RNA	kA	RNA
virům	vir	k1gInPc3	vir
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
základem	základ	k1gInSc7	základ
složitějších	složitý	k2eAgFnPc2d2	složitější
obranných	obranný	k2eAgFnPc2d1	obranná
strategií	strategie	k1gFnPc2	strategie
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
RNA	RNA	kA	RNA
interference	interference	k1gFnSc1	interference
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
proces	proces	k1gInSc1	proces
degradace	degradace	k1gFnSc2	degradace
probíhá	probíhat	k5eAaImIp3nS	probíhat
buď	buď	k8xC	buď
od	od	k7c2	od
konců	konec	k1gInPc2	konec
RNA	RNA	kA	RNA
pomocí	pomocí	k7c2	pomocí
exonukleáz	exonukleáza	k1gFnPc2	exonukleáza
(	(	kIx(	(
<g/>
štěpící	štěpící	k2eAgFnSc1d1	štěpící
od	od	k7c2	od
5	[number]	k4	5
<g/>
'	'	kIx"	'
nebo	nebo	k8xC	nebo
3	[number]	k4	3
<g/>
'	'	kIx"	'
konce	konec	k1gInSc2	konec
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pomocí	pomocí	k7c2	pomocí
endonukleáz	endonukleáza	k1gFnPc2	endonukleáza
štěpících	štěpící	k2eAgFnPc2d1	štěpící
uvnitř	uvnitř	k7c2	uvnitř
vlákna	vlákno	k1gNnSc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
existuje	existovat	k5eAaImIp3nS	existovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
RNáz	RNáza	k1gFnPc2	RNáza
s	s	k7c7	s
překrývajícími	překrývající	k2eAgFnPc7d1	překrývající
se	se	k3xPyFc4	se
aktivitami	aktivita	k1gFnPc7	aktivita
a	a	k8xC	a
každá	každý	k3xTgFnSc1	každý
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
není	být	k5eNaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
proti	proti	k7c3	proti
jejich	jejich	k3xOp3gInPc3	jejich
účinkům	účinek	k1gInPc3	účinek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
degradována	degradován	k2eAgFnSc1d1	degradována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
bakterií	bakterie	k1gFnPc2	bakterie
navazuje	navazovat	k5eAaImIp3nS	navazovat
transkripce	transkripce	k1gFnSc1	transkripce
DNA	DNA	kA	DNA
bezprostředně	bezprostředně	k6eAd1	bezprostředně
na	na	k7c4	na
translaci	translace	k1gFnSc4	translace
a	a	k8xC	a
poločas	poločas	k1gInSc4	poločas
života	život	k1gInSc2	život
mRNA	mRNA	k?	mRNA
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
sekundou	sekunda	k1gFnSc7	sekunda
až	až	k8xS	až
hodinou	hodina	k1gFnSc7	hodina
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
'	'	kIx"	'
konec	konec	k1gInSc1	konec
RNA	RNA	kA	RNA
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
chráněn	chránit	k5eAaImNgInS	chránit
trifosfátovou	trifosfátův	k2eAgFnSc7d1	trifosfátův
skupinou	skupina	k1gFnSc7	skupina
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
nukleotidu	nukleotid	k1gInSc6	nukleotid
a	a	k8xC	a
signálem	signál	k1gInSc7	signál
pro	pro	k7c4	pro
degradaci	degradace	k1gFnSc4	degradace
je	být	k5eAaImIp3nS	být
odštěpení	odštěpení	k1gNnSc2	odštěpení
dvou	dva	k4xCgInPc2	dva
fosfátových	fosfátový	k2eAgInPc2d1	fosfátový
zbytků	zbytek	k1gInPc2	zbytek
a	a	k8xC	a
polyadenylace	polyadenylace	k1gFnSc2	polyadenylace
RNA	RNA	kA	RNA
V	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
eukaryot	eukaryota	k1gFnPc2	eukaryota
je	být	k5eAaImIp3nS	být
poločas	poločas	k1gInSc1	poločas
života	život	k1gInSc2	život
mRNA	mRNA	k?	mRNA
delší	dlouhý	k2eAgInSc1d2	delší
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
minuty	minuta	k1gFnPc4	minuta
až	až	k8xS	až
dny	den	k1gInPc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Stabilita	stabilita	k1gFnSc1	stabilita
je	být	k5eAaImIp3nS	být
zajištěna	zajištěn	k2eAgFnSc1d1	zajištěna
jednak	jednak	k8xC	jednak
úpravou	úprava	k1gFnSc7	úprava
konců	konec	k1gInPc2	konec
–	–	k?	–
přidáním	přidání	k1gNnSc7	přidání
5	[number]	k4	5
<g/>
'	'	kIx"	'
čepičky	čepička	k1gFnPc1	čepička
a	a	k8xC	a
3	[number]	k4	3
<g/>
'	'	kIx"	'
polyadenylací	polyadenylace	k1gFnPc2	polyadenylace
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
vazbou	vazba	k1gFnSc7	vazba
ribonukleoproteinů	ribonukleoprotein	k1gInPc2	ribonukleoprotein
bránících	bránící	k2eAgInPc2d1	bránící
přístupu	přístup	k1gInSc3	přístup
RNáz	RNáz	k1gMnSc1	RNáz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
komplexem	komplex	k1gInSc7	komplex
zajišťujícím	zajišťující	k2eAgInSc7d1	zajišťující
degradaci	degradace	k1gFnSc4	degradace
je	být	k5eAaImIp3nS	být
eukaryotní	eukaryotní	k2eAgInSc1d1	eukaryotní
exozom	exozom	k1gInSc1	exozom
a	a	k8xC	a
u	u	k7c2	u
bakterií	bakterium	k1gNnPc2	bakterium
podobný	podobný	k2eAgMnSc1d1	podobný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jednodušší	jednoduchý	k2eAgInPc1d2	jednodušší
<g/>
,	,	kIx,	,
degradozom	degradozom	k1gInSc1	degradozom
<g/>
.	.	kIx.	.
</s>
<s>
Obsahují	obsahovat	k5eAaImIp3nP	obsahovat
typicky	typicky	k6eAd1	typicky
exo-	exo-	k?	exo-
i	i	k8xC	i
endonukleázy	endonukleáza	k1gFnSc2	endonukleáza
a	a	k8xC	a
polynukleotid	polynukleotid	k1gInSc1	polynukleotid
fosforylázu	fosforyláza	k1gFnSc4	fosforyláza
štěpící	štěpící	k2eAgInPc4d1	štěpící
krátké	krátký	k2eAgInPc4d1	krátký
fragmenty	fragment	k1gInPc4	fragment
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
činností	činnost	k1gFnSc7	činnost
endonukleáz	endonukleáza	k1gFnPc2	endonukleáza
<g/>
,	,	kIx,	,
na	na	k7c4	na
nukleotidy	nukleotid	k1gInPc4	nukleotid
<g/>
.	.	kIx.	.
</s>
<s>
Degradaci	degradace	k1gFnSc4	degradace
napomáhají	napomáhat	k5eAaBmIp3nP	napomáhat
helikázy	helikáza	k1gFnPc1	helikáza
zodpovědné	zodpovědný	k2eAgFnPc1d1	zodpovědná
za	za	k7c4	za
rozplétání	rozplétání	k1gNnSc4	rozplétání
sekundárních	sekundární	k2eAgFnPc2d1	sekundární
struktur	struktura	k1gFnPc2	struktura
na	na	k7c6	na
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Bakteriální	bakteriální	k2eAgInSc1d1	bakteriální
degradozom	degradozom	k1gInSc1	degradozom
navíc	navíc	k6eAd1	navíc
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
poly	pola	k1gFnPc4	pola
<g/>
(	(	kIx(	(
<g/>
A	a	k9	a
<g/>
)	)	kIx)	)
polymerázu	polymeráza	k1gFnSc4	polymeráza
a	a	k8xC	a
eukaryotické	eukaryotický	k2eAgInPc4d1	eukaryotický
exozomy	exozom	k1gInPc4	exozom
proteiny	protein	k1gInPc4	protein
zodpovědné	zodpovědný	k2eAgInPc4d1	zodpovědný
za	za	k7c4	za
rozpoznání	rozpoznání	k1gNnSc4	rozpoznání
destabilizujících	destabilizující	k2eAgFnPc2d1	destabilizující
oblastí	oblast	k1gFnPc2	oblast
v	v	k7c6	v
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ARE	ar	k1gInSc5	ar
elementu	element	k1gInSc2	element
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Metody	metoda	k1gFnPc1	metoda
práce	práce	k1gFnSc2	práce
s	s	k7c7	s
RNA	RNA	kA	RNA
==	==	k?	==
</s>
</p>
<p>
<s>
Práce	práce	k1gFnSc1	práce
s	s	k7c7	s
RNA	RNA	kA	RNA
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
podobná	podobný	k2eAgFnSc1d1	podobná
práci	práce	k1gFnSc3	práce
s	s	k7c7	s
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
izolaci	izolace	k1gFnSc4	izolace
RNA	RNA	kA	RNA
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
potřeba	potřeba	k1gFnSc1	potřeba
rozbití	rozbití	k1gNnSc2	rozbití
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
rozrušení	rozrušení	k1gNnSc1	rozrušení
membrány	membrána	k1gFnSc2	membrána
a	a	k8xC	a
denaturace	denaturace	k1gFnSc2	denaturace
proteinů	protein	k1gInPc2	protein
pomocí	pomocí	k7c2	pomocí
detergentů	detergent	k1gInPc2	detergent
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
izolace	izolace	k1gFnSc2	izolace
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
jednoduše	jednoduše	k6eAd1	jednoduše
inaktivovat	inaktivovat	k5eAaBmF	inaktivovat
enzymy	enzym	k1gInPc4	enzym
štěpící	štěpící	k2eAgFnSc7d1	štěpící
DNA	DNA	kA	DNA
pomocí	pomoc	k1gFnSc7	pomoc
pouhého	pouhý	k2eAgNnSc2d1	pouhé
vyvázání	vyvázání	k1gNnSc2	vyvázání
dvouvazebných	dvouvazebný	k2eAgInPc2d1	dvouvazebný
iontů	ion	k1gInPc2	ion
<g/>
,	,	kIx,	,
enzymy	enzym	k1gInPc1	enzym
štěpící	štěpící	k2eAgFnSc2d1	štěpící
RNA	RNA	kA	RNA
musí	muset	k5eAaImIp3nP	muset
drženy	držet	k5eAaImNgFnP	držet
neaktivní	aktivní	k2eNgFnSc7d1	neaktivní
pomocí	pomoc	k1gFnSc7	pomoc
chlazení	chlazení	k1gNnSc2	chlazení
nebo	nebo	k8xC	nebo
velmi	velmi	k6eAd1	velmi
účinných	účinný	k2eAgNnPc2d1	účinné
denaturačních	denaturační	k2eAgNnPc2d1	denaturační
činidel	činidlo	k1gNnPc2	činidlo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
guanidium	guanidium	k1gNnSc1	guanidium
chlorid	chlorid	k1gInSc1	chlorid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nejpoužívanější	používaný	k2eAgFnSc2d3	nejpoužívanější
metody	metoda	k1gFnSc2	metoda
izolace	izolace	k1gFnSc2	izolace
RNA	RNA	kA	RNA
využívají	využívat	k5eAaPmIp3nP	využívat
tzv.	tzv.	kA	tzv.
fenol-chloroformovou	fenolhloroformová	k1gFnSc4	fenol-chloroformová
extrakci	extrakce	k1gFnSc4	extrakce
právě	právě	k9	právě
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
guanidium	guanidium	k1gNnSc1	guanidium
chloridu	chlorid	k1gInSc2	chlorid
<g/>
.	.	kIx.	.
</s>
<s>
RNázy	RNáza	k1gFnPc1	RNáza
tvoří	tvořit	k5eAaImIp3nP	tvořit
obecně	obecně	k6eAd1	obecně
velký	velký	k2eAgInSc4d1	velký
problém	problém	k1gInSc4	problém
při	při	k7c6	při
práci	práce	k1gFnSc6	práce
s	s	k7c7	s
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
vydržet	vydržet	k5eAaPmF	vydržet
vaření	vaření	k1gNnSc4	vaření
i	i	k8xC	i
autoklávování	autoklávování	k1gNnSc4	autoklávování
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
schopnosti	schopnost	k1gFnSc3	schopnost
zpětného	zpětný	k2eAgNnSc2d1	zpětné
složení	složení	k1gNnSc2	složení
(	(	kIx(	(
<g/>
renaturace	renaturace	k1gFnSc1	renaturace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
izolaci	izolace	k1gFnSc6	izolace
celkové	celkový	k2eAgFnSc2d1	celková
buněčné	buněčný	k2eAgFnSc2d1	buněčná
RNA	RNA	kA	RNA
až	až	k6eAd1	až
98	[number]	k4	98
%	%	kIx~	%
vzorku	vzorek	k1gInSc2	vzorek
představuje	představovat	k5eAaImIp3nS	představovat
ribozomální	ribozomální	k2eAgInSc1d1	ribozomální
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
komplikuje	komplikovat	k5eAaBmIp3nS	komplikovat
analýzu	analýza	k1gFnSc4	analýza
méně	málo	k6eAd2	málo
zastoupených	zastoupený	k2eAgInPc2d1	zastoupený
typů	typ	k1gInPc2	typ
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Bohatě	bohatě	k6eAd1	bohatě
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgFnSc1d1	vyskytující
rRNA	rRNA	k?	rRNA
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
enzymaticky	enzymaticky	k6eAd1	enzymaticky
degradovat	degradovat	k5eAaBmF	degradovat
nebo	nebo	k8xC	nebo
využít	využít	k5eAaPmF	využít
další	další	k2eAgMnPc4d1	další
techniky	technik	k1gMnPc4	technik
pro	pro	k7c4	pro
její	její	k3xOp3gNnPc4	její
odstranění	odstranění	k1gNnSc4	odstranění
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
metodou	metoda	k1gFnSc7	metoda
nabohacení	nabohacení	k1gNnSc2	nabohacení
vybraného	vybraný	k2eAgInSc2d1	vybraný
podtypu	podtyp	k1gInSc2	podtyp
RNA	RNA	kA	RNA
je	být	k5eAaImIp3nS	být
izolace	izolace	k1gFnSc1	izolace
eukaryotní	eukaryotní	k2eAgFnSc2d1	eukaryotní
mRNA	mRNA	k?	mRNA
pomocí	pomocí	k7c2	pomocí
oligo	oligo	k1gNnSc4	oligo
<g/>
(	(	kIx(	(
<g/>
dT	dT	k?	dT
<g/>
)	)	kIx)	)
kotvy	kotva	k1gFnSc2	kotva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
hybridizuje	hybridizovat	k5eAaImIp3nS	hybridizovat
s	s	k7c7	s
poly	pola	k1gFnSc2	pola
<g/>
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
koncem	koncem	k7c2	koncem
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
snadno	snadno	k6eAd1	snadno
izolovat	izolovat	k5eAaBmF	izolovat
veškeré	veškerý	k3xTgFnPc4	veškerý
molekuly	molekula	k1gFnPc4	molekula
mRNA	mRNA	k?	mRNA
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
další	další	k2eAgInPc4d1	další
typy	typ	k1gInPc4	typ
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
poly	pola	k1gFnPc4	pola
<g/>
(	(	kIx(	(
<g/>
A	a	k9	a
<g/>
)	)	kIx)	)
konec	konec	k1gInSc1	konec
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
nekódující	kódující	k2eNgFnSc2d1	nekódující
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alternativní	alternativní	k2eAgInPc1d1	alternativní
způsoby	způsob	k1gInPc1	způsob
izolace	izolace	k1gFnSc2	izolace
žádané	žádaný	k2eAgFnSc2d1	žádaná
RNA	RNA	kA	RNA
ze	z	k7c2	z
směsi	směs	k1gFnSc2	směs
představuje	představovat	k5eAaImIp3nS	představovat
ultracentrifugace	ultracentrifugace	k1gFnSc1	ultracentrifugace
umožňující	umožňující	k2eAgFnSc1d1	umožňující
dělit	dělit	k5eAaImF	dělit
RNA	RNA	kA	RNA
nebo	nebo	k8xC	nebo
ribonukleoproteiny	ribonukleoprotein	k1gInPc1	ribonukleoprotein
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gFnSc2	jejich
vznášivé	vznášivý	k2eAgFnSc2d1	vznášivý
hustoty	hustota	k1gFnSc2	hustota
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
jsou	být	k5eAaImIp3nP	být
pojmenovány	pojmenován	k2eAgFnPc1d1	pojmenována
například	například	k6eAd1	například
molekuly	molekula	k1gFnPc4	molekula
rRNA	rRNA	k?	rRNA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
afinitní	afinitní	k2eAgFnSc1d1	afinitní
chromatografie	chromatografie	k1gFnSc1	chromatografie
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
imunoprecipitace	imunoprecipitace	k1gFnSc1	imunoprecipitace
proteinové	proteinový	k2eAgFnSc2d1	proteinová
složky	složka	k1gFnSc2	složka
ribonukleoproteinu	ribonukleoproteinout	k5eAaPmIp1nS	ribonukleoproteinout
a	a	k8xC	a
následná	následný	k2eAgFnSc1d1	následná
izolace	izolace	k1gFnSc1	izolace
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
analýzu	analýza	k1gFnSc4	analýza
získané	získaný	k2eAgFnSc2d1	získaná
RNA	RNA	kA	RNA
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
elektroforéza	elektroforéza	k1gFnSc1	elektroforéza
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
agarózová	agarózový	k2eAgFnSc1d1	agarózový
nebo	nebo	k8xC	nebo
polyakrylamidová	polyakrylamidový	k2eAgFnSc1d1	polyakrylamidový
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
denaturačních	denaturační	k2eAgNnPc2d1	denaturační
činidel	činidlo	k1gNnPc2	činidlo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
rozrušují	rozrušovat	k5eAaImIp3nP	rozrušovat
sekundární	sekundární	k2eAgFnSc4d1	sekundární
strukturu	struktura	k1gFnSc4	struktura
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
provedení	provedení	k1gNnSc6	provedení
elektroforézy	elektroforéza	k1gFnSc2	elektroforéza
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
detekovat	detekovat	k5eAaImF	detekovat
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
molekuly	molekula	k1gFnSc2	molekula
RNA	RNA	kA	RNA
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Northernova	Northernův	k2eAgInSc2d1	Northernův
blotu	blot	k1gInSc2	blot
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
z	z	k7c2	z
elektroforetického	elektroforetický	k2eAgInSc2d1	elektroforetický
gelu	gel	k1gInSc2	gel
přenese	přenést	k5eAaPmIp3nS	přenést
RNA	RNA	kA	RNA
na	na	k7c4	na
membránu	membrána	k1gFnSc4	membrána
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgInPc4	který
je	být	k5eAaImIp3nS	být
RNA	RNA	kA	RNA
následně	následně	k6eAd1	následně
detekována	detekován	k2eAgFnSc1d1	detekována
pomocí	pomocí	k7c2	pomocí
hybridizace	hybridizace	k1gFnSc2	hybridizace
s	s	k7c7	s
radioaktivně	radioaktivně	k6eAd1	radioaktivně
nebo	nebo	k8xC	nebo
fluorescenčně	fluorescenčně	k6eAd1	fluorescenčně
značenou	značený	k2eAgFnSc7d1	značená
nukleovou	nukleový	k2eAgFnSc7d1	nukleová
kyselinou	kyselina	k1gFnSc7	kyselina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
tzv.	tzv.	kA	tzv.
hybridizační	hybridizační	k2eAgFnSc1d1	hybridizační
sonda	sonda	k1gFnSc1	sonda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Určování	určování	k1gNnSc1	určování
RNA	RNA	kA	RNA
ovšem	ovšem	k9	ovšem
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nejčastěji	často	k6eAd3	často
probíhá	probíhat	k5eAaImIp3nS	probíhat
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
přepisu	přepis	k1gInSc2	přepis
RNA	RNA	kA	RNA
do	do	k7c2	do
DNA	DNA	kA	DNA
pomocí	pomocí	k7c2	pomocí
reverzní	reverzní	k2eAgFnSc2d1	reverzní
transkripce	transkripce	k1gFnSc2	transkripce
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vzniká	vznikat	k5eAaImIp3nS	vznikat
tzv.	tzv.	kA	tzv.
cDNA	cDNA	k?	cDNA
(	(	kIx(	(
<g/>
DNA	DNA	kA	DNA
komplementární	komplementární	k2eAgInPc1d1	komplementární
ke	k	k7c3	k
studované	studovaný	k2eAgNnSc1d1	studované
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důvodů	důvod	k1gInPc2	důvod
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
krok	krok	k1gInSc4	krok
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
vyšší	vysoký	k2eAgFnSc1d2	vyšší
stabilita	stabilita	k1gFnSc1	stabilita
DNA	dno	k1gNnSc2	dno
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
enzymy	enzym	k1gInPc1	enzym
pracující	pracující	k2eAgInPc1d1	pracující
s	s	k7c7	s
DNA	DNA	kA	DNA
jsou	být	k5eAaImIp3nP	být
efektivnější	efektivní	k2eAgFnSc1d2	efektivnější
a	a	k8xC	a
méně	málo	k6eAd2	málo
chybují	chybovat	k5eAaImIp3nP	chybovat
<g/>
.	.	kIx.	.
</s>
<s>
Získanou	získaný	k2eAgFnSc4d1	získaná
cDNA	cDNA	k?	cDNA
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
analyzovat	analyzovat	k5eAaImF	analyzovat
mnoha	mnoho	k4c7	mnoho
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
určit	určit	k5eAaPmF	určit
množství	množství	k1gNnSc4	množství
původních	původní	k2eAgFnPc2d1	původní
kopií	kopie	k1gFnPc2	kopie
RNA	RNA	kA	RNA
ve	v	k7c6	v
vzorku	vzorek	k1gInSc6	vzorek
pomocí	pomocí	k7c2	pomocí
kvantitativní	kvantitativní	k2eAgFnSc2d1	kvantitativní
PCR	PCR	kA	PCR
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
například	například	k6eAd1	například
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
kolik	kolik	k4yQc4	kolik
molekul	molekula	k1gFnPc2	molekula
mRNA	mRNA	k?	mRNA
pro	pro	k7c4	pro
daný	daný	k2eAgInSc4d1	daný
gen	gen	k1gInSc4	gen
se	se	k3xPyFc4	se
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
–	–	k?	–
jak	jak	k6eAd1	jak
silně	silně	k6eAd1	silně
je	být	k5eAaImIp3nS	být
gen	gen	k1gInSc1	gen
přepisován	přepisován	k2eAgInSc1d1	přepisován
<g/>
.	.	kIx.	.
</s>
<s>
Sbírku	sbírka	k1gFnSc4	sbírka
všech	všecek	k3xTgInPc2	všecek
získaných	získaný	k2eAgInPc2d1	získaný
cDNA	cDNA	k?	cDNA
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
představuje	představovat	k5eAaImIp3nS	představovat
soubor	soubor	k1gInSc4	soubor
všech	všecek	k3xTgInPc2	všecek
genů	gen	k1gInPc2	gen
přepisovaných	přepisovaný	k2eAgInPc2d1	přepisovaný
do	do	k7c2	do
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
transkriptom	transkriptom	k1gInSc1	transkriptom
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
analyzovat	analyzovat	k5eAaImF	analyzovat
několika	několik	k4yIc2	několik
způsoby	způsob	k1gInPc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgInSc1d1	významný
způsob	způsob	k1gInSc1	způsob
analýzy	analýza	k1gFnSc2	analýza
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
genů	gen	k1gInPc2	gen
je	být	k5eAaImIp3nS	být
DNA	dna	k1gFnSc1	dna
čip	čip	k1gInSc4	čip
využívající	využívající	k2eAgInSc1d1	využívající
nejčastěji	často	k6eAd3	často
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
různých	různý	k2eAgFnPc2d1	různá
hybridizačních	hybridizační	k2eAgFnPc2d1	hybridizační
sond	sonda	k1gFnPc2	sonda
odvozených	odvozený	k2eAgFnPc2d1	odvozená
od	od	k7c2	od
známých	známý	k2eAgInPc2d1	známý
genů	gen	k1gInPc2	gen
<g/>
,	,	kIx,	,
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
metodě	metoda	k1gFnSc6	metoda
jsou	být	k5eAaImIp3nP	být
molekuly	molekula	k1gFnPc4	molekula
cDNA	cDNA	k?	cDNA
ve	v	k7c6	v
vzorku	vzorek	k1gInSc6	vzorek
zachyceny	zachycen	k2eAgInPc1d1	zachycen
pomocí	pomocí	k7c2	pomocí
hybridizace	hybridizace	k1gFnSc2	hybridizace
k	k	k7c3	k
sondě	sonda	k1gFnSc3	sonda
umístěné	umístěný	k2eAgFnSc2d1	umístěná
na	na	k7c6	na
daném	daný	k2eAgNnSc6d1	dané
místě	místo	k1gNnSc6	místo
destičky	destička	k1gFnSc2	destička
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
čipu	čip	k1gInSc2	čip
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
vzorek	vzorek	k1gInSc1	vzorek
označen	označit	k5eAaPmNgInS	označit
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
fluorescenčně	fluorescenčně	k6eAd1	fluorescenčně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
odmytí	odmytí	k1gNnSc6	odmytí
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
signál	signál	k1gInSc1	signál
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
cDNA	cDNA	k?	cDNA
navázala	navázat	k5eAaPmAgFnS	navázat
na	na	k7c4	na
sondu	sonda	k1gFnSc4	sonda
<g/>
.	.	kIx.	.
</s>
<s>
Identifikaci	identifikace	k1gFnSc4	identifikace
nových	nový	k2eAgInPc2d1	nový
genů	gen	k1gInPc2	gen
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
například	například	k6eAd1	například
analýza	analýza	k1gFnSc1	analýza
krátkých	krátký	k2eAgInPc2d1	krátký
úseků	úsek	k1gInPc2	úsek
cDNA	cDNA	k?	cDNA
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
EST	Est	k1gMnSc1	Est
(	(	kIx(	(
<g/>
expressed	expressed	k1gMnSc1	expressed
sequence	sequence	k1gFnSc2	sequence
tag	tag	k1gInSc1	tag
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
přečteny	přečíst	k5eAaPmNgFnP	přečíst
sekvenací	sekvenace	k1gFnSc7	sekvenace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
sekvenace	sekvenace	k1gFnSc1	sekvenace
veškeré	veškerý	k3xTgNnSc4	veškerý
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
opět	opět	k6eAd1	opět
ale	ale	k9	ale
přepsané	přepsaný	k2eAgFnSc3d1	přepsaná
do	do	k7c2	do
cDNA	cDNA	k?	cDNA
<g/>
)	)	kIx)	)
přítomné	přítomný	k2eAgFnSc6d1	přítomná
buňce	buňka	k1gFnSc6	buňka
s	s	k7c7	s
použitím	použití	k1gNnSc7	použití
sekvenování	sekvenování	k1gNnSc2	sekvenování
nové	nový	k2eAgFnSc2d1	nová
generace	generace	k1gFnSc2	generace
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
RNA-Seq	RNA-Seq	k1gFnSc1	RNA-Seq
(	(	kIx(	(
<g/>
RNA	RNA	kA	RNA
Sequencing	Sequencing	k1gInSc1	Sequencing
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalších	další	k2eAgFnPc2d1	další
metod	metoda	k1gFnPc2	metoda
práce	práce	k1gFnSc2	práce
s	s	k7c7	s
RNA	RNA	kA	RNA
je	být	k5eAaImIp3nS	být
nepřeberná	přeberný	k2eNgFnSc1d1	nepřeberná
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
zajímavá	zajímavý	k2eAgFnSc1d1	zajímavá
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
fluorescenční	fluorescenční	k2eAgFnSc1d1	fluorescenční
hybridizace	hybridizace	k1gFnSc1	hybridizace
in	in	k?	in
situ	siíst	k5eAaPmIp1nS	siíst
umožňující	umožňující	k2eAgNnSc1d1	umožňující
vizualizovat	vizualizovat	k5eAaImF	vizualizovat
pozici	pozice	k1gFnSc4	pozice
dané	daný	k2eAgFnSc2d1	daná
RNA	RNA	kA	RNA
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
<g/>
,	,	kIx,	,
ribozomální	ribozomální	k2eAgInPc4d1	ribozomální
profily	profil	k1gInPc4	profil
umožňující	umožňující	k2eAgInPc4d1	umožňující
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mRNA	mRNA	k?	mRNA
jsou	být	k5eAaImIp3nP	být
buňkou	buňka	k1gFnSc7	buňka
právě	právě	k6eAd1	právě
přepisované	přepisovaný	k2eAgNnSc1d1	přepisované
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
metody	metoda	k1gFnPc1	metoda
RNA	RNA	kA	RNA
interference	interference	k1gFnSc1	interference
umožňující	umožňující	k2eAgNnSc1d1	umožňující
regulovat	regulovat	k5eAaImF	regulovat
genovou	genový	k2eAgFnSc4d1	genová
expresi	exprese	k1gFnSc4	exprese
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ZADRAŽIL	Zadražil	k1gMnSc1	Zadražil
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
.	.	kIx.	.
</s>
<s>
Ribonukleové	ribonukleový	k2eAgFnPc1d1	ribonukleová
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Věčné	věčné	k1gNnSc4	věčné
"	"	kIx"	"
<g/>
druhé	druhý	k4xOgFnSc2	druhý
<g/>
"	"	kIx"	"
mezi	mezi	k7c7	mezi
nukleovými	nukleový	k2eAgFnPc7d1	nukleová
kyselinami	kyselina	k1gFnPc7	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Živa	Živa	k1gFnSc1	Živa
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
s.	s.	k?	s.
98	[number]	k4	98
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HARTMANN	Hartmann	k1gMnSc1	Hartmann
<g/>
,	,	kIx,	,
Roland	Roland	k1gInSc1	Roland
K.	K.	kA	K.
(	(	kIx(	(
<g/>
Roland	Roland	k1gInSc1	Roland
Karl	Karla	k1gFnPc2	Karla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Handbook	handbook	k1gInSc1	handbook
of	of	k?	of
RNA	RNA	kA	RNA
biochemistry	biochemistr	k1gMnPc4	biochemistr
<g/>
.	.	kIx.	.
</s>
<s>
Weinheim	Weinheim	k1gMnSc1	Weinheim
<g/>
:	:	kIx,	:
Wiley-VCH	Wiley-VCH	k1gMnSc1	Wiley-VCH
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
527	[number]	k4	527
<g/>
-	-	kIx~	-
<g/>
30826	[number]	k4	30826
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ELLIOTT	ELLIOTT	kA	ELLIOTT
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
;	;	kIx,	;
LADOMERY	LADOMERY	kA	LADOMERY
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
<g/>
.	.	kIx.	.
</s>
<s>
Molecular	Molecular	k1gMnSc1	Molecular
biology	biolog	k1gMnPc4	biolog
of	of	k?	of
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
;	;	kIx,	;
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
928837	[number]	k4	928837
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Nukleová	nukleový	k2eAgFnSc1d1	nukleová
kyselina	kyselina	k1gFnSc1	kyselina
</s>
</p>
<p>
<s>
Molekulární	molekulární	k2eAgFnSc1d1	molekulární
biologie	biologie	k1gFnSc1	biologie
</s>
</p>
<p>
<s>
Centrální	centrální	k2eAgNnSc1d1	centrální
dogma	dogma	k1gNnSc1	dogma
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
</s>
</p>
<p>
<s>
Genetický	genetický	k2eAgInSc4d1	genetický
kód	kód	k1gInSc4	kód
</s>
</p>
<p>
<s>
RNA	RNA	kA	RNA
vazebný	vazebný	k2eAgInSc4d1	vazebný
protein	protein	k1gInSc4	protein
</s>
</p>
<p>
<s>
Ribonukleoprotein	Ribonukleoprotein	k1gMnSc1	Ribonukleoprotein
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
RNA	RNA	kA	RNA
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
http://openwetware.org/wiki/RNA	[url]	k?	http://openwetware.org/wiki/RNA
</s>
</p>
