<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Florián	Florián	k1gMnSc1	Florián
alias	alias	k9	alias
Pedro	Pedro	k1gNnSc4	Pedro
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1986	[number]	k4	1986
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
youtuber	youtuber	k1gInSc1	youtuber
<g/>
,	,	kIx,	,
let	let	k1gInSc1	let
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
player	player	k1gInSc1	player
a	a	k8xC	a
vlogger	vlogger	k1gInSc1	vlogger
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Pedro	Pedro	k6eAd1	Pedro
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Petr	Petr	k1gMnSc1	Petr
Florián	Florián	k1gMnSc1	Florián
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
a	a	k8xC	a
vyrůstal	vyrůstat	k5eAaImAgMnS	vyrůstat
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Prošel	projít	k5eAaPmAgMnS	projít
gymnáziem	gymnázium	k1gNnSc7	gymnázium
<g/>
,	,	kIx,	,
semestrem	semestr	k1gInSc7	semestr
školy	škola	k1gFnSc2	škola
vysoké	vysoká	k1gFnSc2	vysoká
a	a	k8xC	a
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
vyšší	vysoký	k2eAgFnSc4d2	vyšší
školu	škola	k1gFnSc4	škola
odbornou	odborný	k2eAgFnSc4d1	odborná
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
dělal	dělat	k5eAaImAgMnS	dělat
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
studoval	studovat	k5eAaImAgMnS	studovat
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
podnikatelem	podnikatel	k1gMnSc7	podnikatel
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
naplno	naplno	k6eAd1	naplno
věnovat	věnovat	k5eAaImF	věnovat
YouTube	YouTub	k1gInSc5	YouTub
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgMnSc1d1	ženatý
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
<g/>
,	,	kIx,	,
starší	starý	k2eAgMnPc4d2	starší
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Kryštof	Kryštof	k1gMnSc1	Kryštof
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
YouTube	YouTub	k1gInSc5	YouTub
==	==	k?	==
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Florián	Florián	k1gMnSc1	Florián
si	se	k3xPyFc3	se
svůj	svůj	k3xOyFgInSc4	svůj
kanál	kanál	k1gInSc4	kanál
PedrosGame	PedrosGam	k1gInSc5	PedrosGam
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
5	[number]	k4	5
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2012	[number]	k4	2012
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
mu	on	k3xPp3gMnSc3	on
kamarádi	kamarád	k1gMnPc1	kamarád
doporučili	doporučit	k5eAaPmAgMnP	doporučit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
to	ten	k3xDgNnSc4	ten
zkusil	zkusit	k5eAaPmAgMnS	zkusit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
nahrál	nahrát	k5eAaPmAgMnS	nahrát
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
kanál	kanál	k1gInSc4	kanál
první	první	k4xOgNnSc4	první
video	video	k1gNnSc4	video
<g/>
,	,	kIx,	,
začínal	začínat	k5eAaImAgMnS	začínat
s	s	k7c7	s
let	léto	k1gNnPc2	léto
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
play	play	k0	play
ze	z	k7c2	z
hry	hra	k1gFnSc2	hra
Minecraft	Minecrafta	k1gFnPc2	Minecrafta
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yQgFnPc3	který
se	se	k3xPyFc4	se
nejvíce	nejvíce	k6eAd1	nejvíce
proslavil	proslavit	k5eAaPmAgMnS	proslavit
<g/>
.	.	kIx.	.
</s>
<s>
Natáčel	natáčet	k5eAaImAgMnS	natáčet
dále	daleko	k6eAd2	daleko
například	například	k6eAd1	například
hry	hra	k1gFnPc1	hra
Mafia	Mafius	k1gMnSc2	Mafius
II	II	kA	II
<g/>
,	,	kIx,	,
Spore	spor	k1gInSc5	spor
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Sims	Sims	k1gInSc1	Sims
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
si	se	k3xPyFc3	se
založil	založit	k5eAaPmAgMnS	založit
dva	dva	k4xCgInPc4	dva
další	další	k2eAgInPc4d1	další
kanály	kanál	k1gInPc4	kanál
–	–	k?	–
PedrosFun	PedrosFun	k1gInSc1	PedrosFun
a	a	k8xC	a
PedrosVoice	PedrosVoice	k1gFnSc1	PedrosVoice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
PedrosFun	PedrosFun	k1gInSc4	PedrosFun
nahrává	nahrávat	k5eAaImIp3nS	nahrávat
spíše	spíše	k9	spíše
videa	video	k1gNnPc1	video
ze	z	k7c2	z
života	život	k1gInSc2	život
nebo	nebo	k8xC	nebo
o	o	k7c6	o
sportu	sport	k1gInSc6	sport
<g/>
,	,	kIx,	,
na	na	k7c6	na
PedrosVoice	PedrosVoika	k1gFnSc6	PedrosVoika
nahrává	nahrávat	k5eAaImIp3nS	nahrávat
cover	cover	k1gInSc1	cover
verze	verze	k1gFnSc2	verze
známých	známý	k2eAgFnPc2d1	známá
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
prosince	prosinec	k1gInSc2	prosinec
2017	[number]	k4	2017
překročil	překročit	k5eAaPmAgMnS	překročit
hranici	hranice	k1gFnSc4	hranice
750	[number]	k4	750
tisíc	tisíc	k4xCgInPc2	tisíc
odběratelů	odběratel	k1gMnPc2	odběratel
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
českými	český	k2eAgMnPc7d1	český
youtubery	youtuber	k1gMnPc7	youtuber
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
GEJMR	GEJMR	kA	GEJMR
nebo	nebo	k8xC	nebo
Marek	Marek	k1gMnSc1	Marek
Hubáček	Hubáček	k1gMnSc1	Hubáček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
Petr	Petr	k1gMnSc1	Petr
zanechal	zanechat	k5eAaPmAgMnS	zanechat
své	svůj	k3xOyFgFnPc4	svůj
dosavadní	dosavadní	k2eAgFnPc4d1	dosavadní
práce	práce	k1gFnPc4	práce
a	a	k8xC	a
naplno	naplno	k6eAd1	naplno
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
živit	živit	k5eAaImF	živit
YouTubem	YouTub	k1gInSc7	YouTub
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
dalšími	další	k2eAgInPc7d1	další
YouTubery	YouTuber	k1gInPc7	YouTuber
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
též	též	k9	též
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
Škoda	škoda	k1gFnSc1	škoda
Auto	auto	k1gNnSc1	auto
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
při	při	k7c6	při
testování	testování	k1gNnSc6	testování
nových	nový	k2eAgNnPc2d1	nové
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
také	také	k9	také
účastní	účastnit	k5eAaImIp3nS	účastnit
youtubového	youtubový	k2eAgInSc2d1	youtubový
festivalu	festival	k1gInSc2	festival
Utubering	Utubering	k1gInSc1	Utubering
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
Časopis	časopis	k1gInSc1	časopis
Forbes	forbes	k1gInSc1	forbes
zařadil	zařadit	k5eAaPmAgMnS	zařadit
Pedra	Pedr	k1gMnSc4	Pedr
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
na	na	k7c4	na
23	[number]	k4	23
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
mezi	mezi	k7c7	mezi
nejvlivnějšími	vlivný	k2eAgMnPc7d3	nejvlivnější
Čechy	Čech	k1gMnPc7	Čech
na	na	k7c6	na
sociálních	sociální	k2eAgFnPc6d1	sociální
sítích	síť	k1gFnPc6	síť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
veřejností	veřejnost	k1gFnSc7	veřejnost
nominován	nominovat	k5eAaBmNgInS	nominovat
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Bloger	Blogra	k1gFnPc2	Blogra
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
PedrosGame	PedrosGam	k1gInSc5	PedrosGam
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
</s>
</p>
<p>
<s>
PedrosFun	PedrosFun	k1gInSc1	PedrosFun
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
</s>
</p>
<p>
<s>
PedrosVoice	PedrosVoice	k1gFnSc1	PedrosVoice
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
</s>
</p>
