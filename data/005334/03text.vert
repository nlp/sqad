<s>
Latina	latina	k1gFnSc1	latina
(	(	kIx(	(
<g/>
lingua	lingu	k2eAgFnSc1d1	lingua
latina	latina	k1gFnSc1	latina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mrtvý	mrtvý	k2eAgInSc1d1	mrtvý
indoevropský	indoevropský	k2eAgInSc1d1	indoevropský
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
latiny	latina	k1gFnSc2	latina
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
lokalizovat	lokalizovat	k5eAaBmF	lokalizovat
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
Říma	Řím	k1gInSc2	Řím
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
době	doba	k1gFnSc6	doba
před	před	k7c7	před
7	[number]	k4	7
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
užívání	užívání	k1gNnSc1	užívání
latiny	latina	k1gFnSc2	latina
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
do	do	k7c2	do
většiny	většina	k1gFnSc2	většina
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
územní	územní	k2eAgFnSc7d1	územní
expanzí	expanze	k1gFnSc7	expanze
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
latinský	latinský	k2eAgInSc1d1	latinský
jazyk	jazyk	k1gInSc1	jazyk
šířil	šířit	k5eAaImAgInS	šířit
do	do	k7c2	do
všech	všecek	k3xTgFnPc2	všecek
římských	římský	k2eAgFnPc2d1	římská
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
hovorová	hovorový	k2eAgFnSc1d1	hovorová
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
vulgární	vulgární	k2eAgFnSc1d1	vulgární
<g/>
)	)	kIx)	)
latina	latina	k1gFnSc1	latina
na	na	k7c6	na
těchto	tento	k3xDgNnPc6	tento
územích	území	k1gNnPc6	území
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
v	v	k7c4	v
dnešní	dnešní	k2eAgInPc4d1	dnešní
románské	románský	k2eAgInPc4d1	románský
jazyky	jazyk	k1gInPc4	jazyk
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
francouzštinu	francouzština	k1gFnSc4	francouzština
<g/>
,	,	kIx,	,
italštinu	italština	k1gFnSc4	italština
<g/>
,	,	kIx,	,
španělštinu	španělština	k1gFnSc4	španělština
<g/>
,	,	kIx,	,
portugalštinu	portugalština	k1gFnSc4	portugalština
<g/>
,	,	kIx,	,
rumunštinu	rumunština	k1gFnSc4	rumunština
a	a	k8xC	a
další	další	k2eAgFnSc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Latina	latina	k1gFnSc1	latina
však	však	k9	však
silně	silně	k6eAd1	silně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
(	(	kIx(	(
<g/>
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
francouzštiny	francouzština	k1gFnSc2	francouzština
<g/>
)	)	kIx)	)
například	například	k6eAd1	například
také	také	k9	také
angličtinu	angličtina	k1gFnSc4	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Spisovná	spisovný	k2eAgFnSc1d1	spisovná
<g/>
,	,	kIx,	,
literární	literární	k2eAgFnSc1d1	literární
latina	latina	k1gFnSc1	latina
se	se	k3xPyFc4	se
ve	v	k7c6	v
vzdělaném	vzdělaný	k2eAgInSc6d1	vzdělaný
světě	svět	k1gInSc6	svět
nadále	nadále	k6eAd1	nadále
užívala	užívat	k5eAaImAgFnS	užívat
a	a	k8xC	a
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
západořímské	západořímský	k2eAgFnSc2d1	Západořímská
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jazykem	jazyk	k1gMnSc7	jazyk
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
vzdělanců	vzdělanec	k1gMnPc2	vzdělanec
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
úředně	úředně	k6eAd1	úředně
správnímu	správní	k2eAgNnSc3d1	správní
<g/>
,	,	kIx,	,
církevnímu	církevní	k2eAgNnSc3d1	církevní
a	a	k8xC	a
literárně	literárně	k6eAd1	literárně
uměleckému	umělecký	k2eAgNnSc3d1	umělecké
užívání	užívání	k1gNnSc3	užívání
si	se	k3xPyFc3	se
latina	latina	k1gFnSc1	latina
podržela	podržet	k5eAaPmAgFnS	podržet
své	svůj	k3xOyFgNnSc4	svůj
postavení	postavení	k1gNnSc4	postavení
až	až	k9	až
do	do	k7c2	do
prosazení	prosazení	k1gNnSc2	prosazení
národních	národní	k2eAgInPc2d1	národní
jazyků	jazyk	k1gInPc2	jazyk
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
renesance	renesance	k1gFnSc2	renesance
<g/>
.	.	kIx.	.
</s>
<s>
Posléze	posléze	k6eAd1	posléze
se	se	k3xPyFc4	se
latina	latina	k1gFnSc1	latina
stala	stát	k5eAaPmAgFnS	stát
mrtvým	mrtvý	k2eAgInSc7d1	mrtvý
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
však	však	k9	však
užívá	užívat	k5eAaImIp3nS	užívat
v	v	k7c6	v
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
liturgii	liturgie	k1gFnSc6	liturgie
a	a	k8xC	a
správě	správa	k1gFnSc6	správa
<g/>
,	,	kIx,	,
v	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
terminologii	terminologie	k1gFnSc6	terminologie
(	(	kIx(	(
<g/>
v	v	k7c6	v
oborech	obor	k1gInPc6	obor
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
,	,	kIx,	,
biologie	biologie	k1gFnSc2	biologie
<g/>
,	,	kIx,	,
právo	právo	k1gNnSc1	právo
<g/>
,	,	kIx,	,
filosofie	filosofie	k1gFnSc1	filosofie
<g/>
,	,	kIx,	,
filologie	filologie	k1gFnSc1	filologie
<g/>
,	,	kIx,	,
teologie	teologie	k1gFnSc1	teologie
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc1	historie
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Latina	latina	k1gFnSc1	latina
<g/>
,	,	kIx,	,
jazyk	jazyk	k1gInSc1	jazyk
používaný	používaný	k2eAgInSc1d1	používaný
po	po	k7c4	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
třídí	třídit	k5eAaImIp3nS	třídit
do	do	k7c2	do
několika	několik	k4yIc2	několik
skupin	skupina	k1gFnPc2	skupina
podle	podle	k7c2	podle
vývoje	vývoj	k1gInSc2	vývoj
a	a	k8xC	a
sféry	sféra	k1gFnSc2	sféra
použití	použití	k1gNnSc2	použití
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
skupiny	skupina	k1gFnPc1	skupina
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
často	často	k6eAd1	často
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
přesně	přesně	k6eAd1	přesně
vymezitelné	vymezitelný	k2eAgNnSc4d1	vymezitelné
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
latina	latina	k1gFnSc1	latina
–	–	k?	–
latina	latina	k1gFnSc1	latina
až	až	k6eAd1	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Texty	text	k1gInPc1	text
psané	psaný	k2eAgInPc1d1	psaný
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
formě	forma	k1gFnSc6	forma
ještě	ještě	k9	ještě
neznají	znát	k5eNaImIp3nP	znát
ustálený	ustálený	k2eAgInSc4d1	ustálený
úzus	úzus	k1gInSc4	úzus
zápisu	zápis	k1gInSc2	zápis
a	a	k8xC	a
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
archaické	archaický	k2eAgInPc1d1	archaický
tvary	tvar	k1gInPc1	tvar
a	a	k8xC	a
formy	forma	k1gFnPc1	forma
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
již	již	k6eAd1	již
nepoužívaly	používat	k5eNaImAgFnP	používat
<g/>
.	.	kIx.	.
klasická	klasický	k2eAgFnSc1d1	klasická
latina	latina	k1gFnSc1	latina
–	–	k?	–
latina	latina	k1gFnSc1	latina
starověké	starověký	k2eAgFnSc2d1	starověká
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
jazyk	jazyk	k1gInSc4	jazyk
velkých	velký	k2eAgInPc2d1	velký
"	"	kIx"	"
<g/>
klasických	klasický	k2eAgInPc2d1	klasický
<g/>
"	"	kIx"	"
antických	antický	k2eAgInPc2d1	antický
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
především	především	k9	především
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
přelomu	přelom	k1gInSc2	přelom
letopočtu	letopočet	k1gInSc2	letopočet
(	(	kIx(	(
<g/>
především	především	k9	především
Caesar	Caesar	k1gMnSc1	Caesar
a	a	k8xC	a
Cicero	Cicero	k1gMnSc1	Cicero
<g/>
,	,	kIx,	,
Ovidius	Ovidius	k1gMnSc1	Ovidius
<g/>
,	,	kIx,	,
Horatius	Horatius	k1gMnSc1	Horatius
<g/>
,	,	kIx,	,
Livius	Livius	k1gMnSc1	Livius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
bývá	bývat	k5eAaImIp3nS	bývat
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
obtížnější	obtížný	k2eAgFnSc4d2	obtížnější
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vzorem	vzor	k1gInSc7	vzor
při	při	k7c6	při
obou	dva	k4xCgFnPc6	dva
velkých	velký	k2eAgFnPc6d1	velká
reformách	reforma	k1gFnPc6	reforma
latinského	latinský	k2eAgInSc2d1	latinský
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
karolinské	karolinský	k2eAgFnPc1d1	Karolinská
a	a	k8xC	a
humanistické	humanistický	k2eAgFnPc1d1	humanistická
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vulgární	vulgární	k2eAgFnSc1d1	vulgární
latina	latina	k1gFnSc1	latina
byla	být	k5eAaImAgFnS	být
mluveným	mluvený	k2eAgNnSc7d1	mluvené
nářečím	nářečí	k1gNnSc7	nářečí
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Západořímské	západořímský	k2eAgFnPc4d1	Západořímská
říše	říš	k1gFnPc4	říš
z	z	k7c2	z
jejich	jejich	k3xOp3gNnSc2	jejich
nářečí	nářečí	k1gNnSc2	nářečí
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
románské	románský	k2eAgInPc1d1	románský
jazyky	jazyk	k1gInPc1	jazyk
<g/>
.	.	kIx.	.
středověká	středověký	k2eAgFnSc1d1	středověká
latina	latina	k1gFnSc1	latina
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
střlat	střlat	k5eAaImF	střlat
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
–	–	k?	–
latina	latina	k1gFnSc1	latina
od	od	k7c2	od
konce	konec	k1gInSc2	konec
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
až	až	k6eAd1	až
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
<g/>
)	)	kIx)	)
do	do	k7c2	do
humanismu	humanismus	k1gInSc2	humanismus
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
úžeji	úzko	k6eAd2	úzko
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
karolinské	karolinský	k2eAgFnSc2d1	Karolinská
reformace	reformace	k1gFnSc2	reformace
(	(	kIx(	(
<g/>
cca	cca	kA	cca
800	[number]	k4	800
<g/>
)	)	kIx)	)
do	do	k7c2	do
humanismu	humanismus	k1gInSc2	humanismus
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc4	počátek
tohoto	tento	k3xDgInSc2	tento
jazyka	jazyk	k1gInSc2	jazyk
se	se	k3xPyFc4	se
kladou	klást	k5eAaImIp3nP	klást
do	do	k7c2	do
posledních	poslední	k2eAgMnPc2d1	poslední
let	let	k1gInSc4	let
západořímské	západořímský	k2eAgFnSc2d1	Západořímská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
latina	latina	k1gFnSc1	latina
stala	stát	k5eAaPmAgFnS	stát
lidovým	lidový	k2eAgInSc7d1	lidový
jazykem	jazyk	k1gInSc7	jazyk
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
krajích	kraj	k1gInPc6	kraj
mimo	mimo	k7c4	mimo
rodnou	rodný	k2eAgFnSc4d1	rodná
Itálii	Itálie	k1gFnSc4	Itálie
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
zjednodušovat	zjednodušovat	k5eAaImF	zjednodušovat
(	(	kIx(	(
<g/>
omezení	omezení	k1gNnSc4	omezení
skloňování	skloňování	k1gNnSc2	skloňování
koncovkami	koncovka	k1gFnPc7	koncovka
<g/>
,	,	kIx,	,
omezení	omezení	k1gNnSc4	omezení
konjunktivů	konjunktiv	k1gInPc2	konjunktiv
<g/>
,	,	kIx,	,
nahrazování	nahrazování	k1gNnSc1	nahrazování
akuzativních	akuzativní	k2eAgFnPc2d1	akuzativní
vazeb	vazba	k1gFnPc2	vazba
vedlejšími	vedlejší	k2eAgInPc7d1	vedlejší
větami	věta	k1gFnPc7	věta
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prosazování	prosazování	k1gNnSc1	prosazování
nové	nový	k2eAgFnSc2d1	nová
vulgární	vulgární	k2eAgFnSc2d1	vulgární
či	či	k8xC	či
začínající	začínající	k2eAgFnSc2d1	začínající
středověké	středověký	k2eAgFnSc2d1	středověká
latiny	latina	k1gFnSc2	latina
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
dílech	díl	k1gInPc6	díl
Augustinových	Augustinových	k2eAgFnSc2d1	Augustinových
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
tento	tento	k3xDgMnSc1	tento
biskup	biskup	k1gMnSc1	biskup
naráží	narážet	k5eAaImIp3nS	narážet
na	na	k7c4	na
"	"	kIx"	"
<g/>
lidový	lidový	k2eAgInSc4d1	lidový
způsob	způsob	k1gInSc4	způsob
mluvy	mluva	k1gFnSc2	mluva
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
poutnice	poutnice	k1gFnSc2	poutnice
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
země	zem	k1gFnSc2	zem
Egerie	Egerie	k1gFnSc2	Egerie
(	(	kIx(	(
<g/>
Itinerarium	itinerarium	k1gNnSc1	itinerarium
Egeriae	Egeria	k1gInSc2	Egeria
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
lidovým	lidový	k2eAgInSc7d1	lidový
jazykem	jazyk	k1gInSc7	jazyk
psaný	psaný	k2eAgInSc4d1	psaný
celý	celý	k2eAgInSc4d1	celý
spis	spis	k1gInSc4	spis
<g/>
.	.	kIx.	.
</s>
<s>
Středověká	středověký	k2eAgFnSc1d1	středověká
latina	latina	k1gFnSc1	latina
doznala	doznat	k5eAaPmAgFnS	doznat
reformy	reforma	k1gFnPc4	reforma
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgMnSc2d1	veliký
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
karolinská	karolinský	k2eAgFnSc1d1	Karolinská
reforma	reforma	k1gFnSc1	reforma
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
latina	latina	k1gFnSc1	latina
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
mnohých	mnohý	k2eAgInPc6d1	mnohý
aspektech	aspekt	k1gInPc6	aspekt
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
národními	národní	k2eAgInPc7d1	národní
jazyky	jazyk	k1gInPc7	jazyk
a	a	k8xC	a
liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
částečně	částečně	k6eAd1	částečně
od	od	k7c2	od
kraje	kraj	k1gInSc2	kraj
ke	k	k7c3	k
kraji	kraj	k1gInSc3	kraj
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
lexiku	lexikon	k1gNnSc6	lexikon
<g/>
.	.	kIx.	.
humanistická	humanistický	k2eAgFnSc1d1	humanistická
latina	latina	k1gFnSc1	latina
–	–	k?	–
latina	latina	k1gFnSc1	latina
vzešlá	vzešlý	k2eAgFnSc1d1	vzešlá
z	z	k7c2	z
reformy	reforma	k1gFnSc2	reforma
za	za	k7c2	za
humanismu	humanismus	k1gInSc2	humanismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
humanismu	humanismus	k1gInSc2	humanismus
stále	stále	k6eAd1	stále
větší	veliký	k2eAgNnSc4d2	veliký
prosazování	prosazování	k1gNnSc4	prosazování
národních	národní	k2eAgInPc2d1	národní
jazyků	jazyk	k1gInPc2	jazyk
vytlačovalo	vytlačovat	k5eAaImAgNnS	vytlačovat
pomalu	pomalu	k6eAd1	pomalu
latinu	latina	k1gFnSc4	latina
z	z	k7c2	z
každodenního	každodenní	k2eAgInSc2d1	každodenní
života	život	k1gInSc2	život
<g/>
;	;	kIx,	;
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
obdiv	obdiv	k1gInSc4	obdiv
ke	k	k7c3	k
klasickým	klasický	k2eAgMnPc3d1	klasický
autorům	autor	k1gMnPc3	autor
způsobil	způsobit	k5eAaPmAgInS	způsobit
opouštění	opouštěný	k2eAgMnPc1d1	opouštěný
"	"	kIx"	"
<g/>
lidové	lidový	k2eAgFnPc4d1	lidová
<g/>
"	"	kIx"	"
mluvy	mluva	k1gFnPc4	mluva
a	a	k8xC	a
studium	studium	k1gNnSc4	studium
a	a	k8xC	a
úmyslný	úmyslný	k2eAgInSc4d1	úmyslný
návrat	návrat	k1gInSc4	návrat
ke	k	k7c3	k
klasické	klasický	k2eAgFnSc3d1	klasická
latině	latina	k1gFnSc3	latina
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
projevoval	projevovat	k5eAaImAgMnS	projevovat
především	především	k6eAd1	především
v	v	k7c6	v
syntaxi	syntax	k1gFnSc6	syntax
a	a	k8xC	a
stylistice	stylistika	k1gFnSc6	stylistika
<g/>
;	;	kIx,	;
slovní	slovní	k2eAgFnSc1d1	slovní
zásoba	zásoba	k1gFnSc1	zásoba
musela	muset	k5eAaImAgFnS	muset
naopak	naopak	k6eAd1	naopak
odpovídat	odpovídat	k5eAaImF	odpovídat
své	svůj	k3xOyFgFnSc3	svůj
době	doba	k1gFnSc3	doba
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
době	doba	k1gFnSc6	doba
humanismu	humanismus	k1gInSc2	humanismus
a	a	k8xC	a
renesance	renesance	k1gFnSc1	renesance
vzniká	vznikat	k5eAaImIp3nS	vznikat
základ	základ	k1gInSc4	základ
dnešní	dnešní	k2eAgFnSc2d1	dnešní
italštiny	italština	k1gFnSc2	italština
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
toskánským	toskánský	k2eAgNnSc7d1	toskánské
nářečím	nářečí	k1gNnSc7	nářečí
lidové	lidový	k2eAgFnSc2d1	lidová
latiny	latina	k1gFnSc2	latina
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
písemně	písemně	k6eAd1	písemně
použita	použít	k5eAaPmNgNnP	použít
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Zpěvník	zpěvník	k1gInSc1	zpěvník
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
autorem	autor	k1gMnSc7	autor
byl	být	k5eAaImAgMnS	být
slavný	slavný	k2eAgMnSc1d1	slavný
italský	italský	k2eAgMnSc1d1	italský
humanista	humanista	k1gMnSc1	humanista
Francesco	Francesco	k1gMnSc1	Francesco
Petrarca	Petrarca	k1gMnSc1	Petrarca
<g/>
.	.	kIx.	.
církevní	církevní	k2eAgFnSc1d1	církevní
latina	latina	k1gFnSc1	latina
–	–	k?	–
latina	latina	k1gFnSc1	latina
používaná	používaný	k2eAgFnSc1d1	používaná
v	v	k7c6	v
církevních	církevní	k2eAgInPc6d1	církevní
kruzích	kruh	k1gInPc6	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Latina	latina	k1gFnSc1	latina
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
jazyk	jazyk	k1gInSc1	jazyk
"	"	kIx"	"
<g/>
všech	všecek	k3xTgFnPc2	všecek
věd	věda	k1gFnPc2	věda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
snad	snad	k9	snad
nejdéle	dlouho	k6eAd3	dlouho
jako	jako	k8xS	jako
oficiální	oficiální	k2eAgInSc1d1	oficiální
jazyk	jazyk	k1gInSc1	jazyk
udržel	udržet	k5eAaPmAgInS	udržet
v	v	k7c6	v
církvi	církev	k1gFnSc6	církev
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Církevnost	církevnost	k1gFnSc1	církevnost
<g/>
"	"	kIx"	"
spočívá	spočívat	k5eAaImIp3nS	spočívat
nikoli	nikoli	k9	nikoli
v	v	k7c6	v
mluvnici	mluvnice	k1gFnSc6	mluvnice
(	(	kIx(	(
<g/>
ta	ten	k3xDgFnSc1	ten
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
vždy	vždy	k6eAd1	vždy
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
spíše	spíše	k9	spíše
v	v	k7c6	v
lexiku	lexikon	k1gNnSc6	lexikon
a	a	k8xC	a
idiomech	idiom	k1gInPc6	idiom
<g/>
.	.	kIx.	.
</s>
<s>
Zaměňování	zaměňování	k1gNnSc1	zaměňování
církevní	církevní	k2eAgFnPc1d1	církevní
latiny	latina	k1gFnPc1	latina
se	s	k7c7	s
středověkou	středověký	k2eAgFnSc7d1	středověká
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
z	z	k7c2	z
lingvistického	lingvistický	k2eAgNnSc2d1	lingvistické
hlediska	hledisko	k1gNnSc2	hledisko
nepřesné	přesný	k2eNgNnSc1d1	nepřesné
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
byl	být	k5eAaImAgInS	být
vliv	vliv	k1gInSc1	vliv
křesťanství	křesťanství	k1gNnSc2	křesťanství
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
na	na	k7c4	na
latinu	latina	k1gFnSc4	latina
značný	značný	k2eAgInSc4d1	značný
a	a	k8xC	a
pronikal	pronikat	k5eAaImAgMnS	pronikat
i	i	k9	i
daleko	daleko	k6eAd1	daleko
mimo	mimo	k7c4	mimo
sféru	sféra	k1gFnSc4	sféra
ryze	ryze	k6eAd1	ryze
náboženskou	náboženský	k2eAgFnSc7d1	náboženská
<g/>
.	.	kIx.	.
</s>
<s>
Křesťanství	křesťanství	k1gNnSc1	křesťanství
zavedlo	zavést	k5eAaPmAgNnS	zavést
do	do	k7c2	do
latiny	latina	k1gFnSc2	latina
mnoho	mnoho	k6eAd1	mnoho
nových	nový	k2eAgNnPc2d1	nové
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
přejatých	přejatý	k2eAgMnPc2d1	přejatý
většinou	většinou	k6eAd1	většinou
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
či	či	k8xC	či
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
přímo	přímo	k6eAd1	přímo
novotvary	novotvar	k1gInPc1	novotvar
<g/>
)	)	kIx)	)
již	již	k6eAd1	již
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
–	–	k?	–
baptisma	baptismum	k1gNnSc2	baptismum
<g/>
/	/	kIx~	/
<g/>
baptismum	baptismum	k1gNnSc1	baptismum
<g/>
,	,	kIx,	,
sabbatum	sabbatum	k1gNnSc1	sabbatum
<g/>
,	,	kIx,	,
trinitas	trinitas	k1gInSc1	trinitas
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
Díky	díky	k7c3	díky
vlivu	vliv	k1gInSc3	vliv
evropských	evropský	k2eAgInPc2d1	evropský
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
reálií	reálie	k1gFnPc2	reálie
také	také	k6eAd1	také
mnoho	mnoho	k4c1	mnoho
slov	slovo	k1gNnPc2	slovo
dostalo	dostat	k5eAaPmAgNnS	dostat
nový	nový	k2eAgInSc4d1	nový
význam	význam	k1gInSc4	význam
či	či	k8xC	či
nové	nový	k2eAgFnPc4d1	nová
pádové	pádový	k2eAgFnPc4d1	pádová
vazby	vazba	k1gFnPc4	vazba
(	(	kIx(	(
<g/>
confiteri	confiterit	k5eAaPmRp2nS	confiterit
alicui	alicu	k1gFnSc2	alicu
–	–	k?	–
chválit	chválit	k5eAaImF	chválit
někoho	někdo	k3yInSc4	někdo
<g/>
;	;	kIx,	;
misereri	miserer	k1gMnSc3	miserer
alicui	alicu	k1gMnSc3	alicu
namísto	namísto	k7c2	namísto
misereri	miserer	k1gFnSc2	miserer
alicuius	alicuius	k1gInSc1	alicuius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
latina	latina	k1gFnSc1	latina
již	již	k6eAd1	již
nebyla	být	k5eNaImAgFnS	být
národním	národní	k2eAgInSc7d1	národní
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mezinárodním	mezinárodní	k2eAgMnPc3d1	mezinárodní
<g/>
,	,	kIx,	,
církevním	církevní	k2eAgMnPc3d1	církevní
a	a	k8xC	a
odborným	odborný	k2eAgMnPc3d1	odborný
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
výslovnost	výslovnost	k1gFnSc1	výslovnost
latiny	latina	k1gFnSc2	latina
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
zemích	zem	k1gFnPc6	zem
odlišila	odlišit	k5eAaPmAgFnS	odlišit
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
výslovnost	výslovnost	k1gFnSc1	výslovnost
některých	některý	k3yIgFnPc2	některý
hlásek	hláska	k1gFnPc2	hláska
se	se	k3xPyFc4	se
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
výslovnosti	výslovnost	k1gFnSc2	výslovnost
nejbližší	blízký	k2eAgFnSc2d3	nejbližší
podobné	podobný	k2eAgFnPc4d1	podobná
hlásky	hláska	k1gFnSc2	hláska
vyskytující	vyskytující	k2eAgFnSc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
mluvčího	mluvčí	k1gMnSc2	mluvčí
(	(	kIx(	(
<g/>
např.	např.	kA	např.
české	český	k2eAgFnPc1d1	Česká
<g/>
,	,	kIx,	,
německé	německý	k2eAgFnSc2d1	německá
a	a	k8xC	a
anglické	anglický	k2eAgFnSc2d1	anglická
"	"	kIx"	"
<g/>
r	r	kA	r
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
otevřenost	otevřenost	k1gFnSc1	otevřenost
samohlásek	samohláska	k1gFnPc2	samohláska
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
například	například	k6eAd1	například
výslovnost	výslovnost	k1gFnSc1	výslovnost
c	c	k0	c
před	před	k7c7	před
e	e	k0	e
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
i	i	k9	i
<g/>
,	,	kIx,	,
y	y	k?	y
<g/>
,	,	kIx,	,
ae	ae	k?	ae
<g/>
,	,	kIx,	,
oe	oe	k?	oe
nebo	nebo	k8xC	nebo
eu	eu	k?	eu
(	(	kIx(	(
<g/>
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
latině	latina	k1gFnSc6	latina
se	se	k3xPyFc4	se
c	c	k0	c
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
čte	číst	k5eAaImIp3nS	číst
tradičně	tradičně	k6eAd1	tradičně
jako	jako	k8xS	jako
c	c	k0	c
<g/>
,	,	kIx,	,
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
verzích	verze	k1gFnPc6	verze
latiny	latina	k1gFnSc2	latina
častěji	často	k6eAd2	často
jako	jako	k8xS	jako
č	č	k0	č
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
g	g	kA	g
před	před	k7c7	před
uvedenými	uvedený	k2eAgFnPc7d1	uvedená
samohláskami	samohláska	k1gFnPc7	samohláska
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
verzích	verze	k1gFnPc6	verze
latiny	latina	k1gFnSc2	latina
<g />
.	.	kIx.	.
</s>
<s>
čte	číst	k5eAaImIp3nS	číst
jako	jako	k9	jako
dž	dž	k?	dž
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
české	český	k2eAgFnSc6d1	Česká
latině	latina	k1gFnSc6	latina
se	se	k3xPyFc4	se
g	g	kA	g
čte	číst	k5eAaImIp3nS	číst
vždy	vždy	k6eAd1	vždy
jako	jako	k8xS	jako
g.	g.	k?	g.
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
verzích	verze	k1gFnPc6	verze
latiny	latina	k1gFnSc2	latina
se	se	k3xPyFc4	se
s	s	k7c7	s
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
pozicích	pozice	k1gFnPc6	pozice
čte	číst	k5eAaImIp3nS	číst
jako	jako	k9	jako
z.	z.	k?	z.
O	o	k7c6	o
výslovnosti	výslovnost	k1gFnSc6	výslovnost
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
historických	historický	k2eAgFnPc6d1	historická
fázích	fáze	k1gFnPc6	fáze
se	se	k3xPyFc4	se
vedou	vést	k5eAaImIp3nP	vést
odborné	odborný	k2eAgInPc1d1	odborný
spory	spor	k1gInPc1	spor
<g/>
,	,	kIx,	,
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
původně	původně	k6eAd1	původně
bylo	být	k5eAaImAgNnS	být
písmo	písmo	k1gNnSc1	písmo
důsledněji	důsledně	k6eAd2	důsledně
hláskové	hláskový	k2eAgNnSc1d1	hláskové
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
například	například	k6eAd1	například
Cicero	Cicero	k1gMnSc1	Cicero
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
původně	původně	k6eAd1	původně
čten	číst	k5eAaImNgInS	číst
jako	jako	k8xS	jako
Kikero	Kikero	k1gNnSc1	Kikero
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
latinský	latinský	k2eAgInSc1d1	latinský
zápis	zápis	k1gInSc1	zápis
hlásek	hláska	k1gFnPc2	hláska
velmi	velmi	k6eAd1	velmi
podobá	podobat	k5eAaImIp3nS	podobat
českému	český	k2eAgMnSc3d1	český
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
odlišnostmi	odlišnost	k1gFnPc7	odlišnost
<g/>
:	:	kIx,	:
I	i	k9	i
označuje	označovat	k5eAaImIp3nS	označovat
někdy	někdy	k6eAd1	někdy
samohlásku	samohláska	k1gFnSc4	samohláska
i	i	k9	i
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
souhlásku	souhláska	k1gFnSc4	souhláska
j.	j.	k?	j.
Dvojhlásky	dvojhláska	k1gFnSc2	dvojhláska
ae	ae	k?	ae
i	i	k8xC	i
oe	oe	k?	oe
se	se	k3xPyFc4	se
čtou	číst	k5eAaImIp3nP	číst
jako	jako	k9	jako
české	český	k2eAgFnPc1d1	Česká
é.	é.	k?	é.
Jinak	jinak	k6eAd1	jinak
se	se	k3xPyFc4	se
délka	délka	k1gFnSc1	délka
samohlásek	samohláska	k1gFnPc2	samohláska
běžně	běžně	k6eAd1	běžně
nevyznačuje	vyznačovat	k5eNaImIp3nS	vyznačovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
učebnicích	učebnice	k1gFnPc6	učebnice
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nS	značit
vodorovnou	vodorovný	k2eAgFnSc7d1	vodorovná
čárkou	čárka	k1gFnSc7	čárka
nad	nad	k7c7	nad
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Slabiky	slabika	k1gFnPc1	slabika
di	di	k?	di
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
ni	on	k3xPp3gFnSc4	on
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
tvrdě	tvrdě	k6eAd1	tvrdě
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
před	před	k7c7	před
samohláskou	samohláska	k1gFnSc7	samohláska
se	se	k3xPyFc4	se
však	však	k9	však
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
zpravidla	zpravidla	k6eAd1	zpravidla
jako	jako	k8xS	jako
ci	ci	k0	ci
<g/>
.	.	kIx.	.
</s>
<s>
Písmena	písmeno	k1gNnPc4	písmeno
odpovídající	odpovídající	k2eAgFnSc2d1	odpovídající
českým	český	k2eAgMnSc7d1	český
písmenům	písmeno	k1gNnPc3	písmeno
se	s	k7c7	s
změkčovacím	změkčovací	k2eAgInSc7d1	změkčovací
háčkem	háček	k1gInSc7	háček
latina	latina	k1gFnSc1	latina
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Písmeno	písmeno	k1gNnSc1	písmeno
q	q	k?	q
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
před	před	k7c7	před
písmenem	písmeno	k1gNnSc7	písmeno
u	u	k7c2	u
<g/>
,	,	kIx,	,
seskupení	seskupení	k1gNnPc2	seskupení
qu	qu	k?	qu
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
kv	kv	k?	kv
<g/>
.	.	kIx.	.
</s>
<s>
Seskupení	seskupení	k1gNnSc1	seskupení
gu	gu	k?	gu
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
čte	číst	k5eAaImIp3nS	číst
jako	jako	k9	jako
gv	gv	k?	gv
<g/>
.	.	kIx.	.
</s>
<s>
Dvojslabičná	dvojslabičný	k2eAgNnPc1d1	dvojslabičné
slova	slovo	k1gNnPc1	slovo
mají	mít	k5eAaImIp3nP	mít
přízvuk	přízvuk	k1gInSc4	přízvuk
na	na	k7c6	na
první	první	k4xOgFnSc6	první
slabice	slabika	k1gFnSc6	slabika
<g/>
.	.	kIx.	.
</s>
<s>
Víceslabičná	víceslabičný	k2eAgNnPc1d1	víceslabičné
slova	slovo	k1gNnPc1	slovo
mají	mít	k5eAaImIp3nP	mít
přízvuk	přízvuk	k1gInSc4	přízvuk
na	na	k7c6	na
předposlední	předposlední	k2eAgFnSc6d1	předposlední
slabice	slabika	k1gFnSc6	slabika
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
slabika	slabika	k1gFnSc1	slabika
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
přirozeně	přirozeně	k6eAd1	přirozeně
nebo	nebo	k8xC	nebo
polohou	poloha	k1gFnSc7	poloha
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
na	na	k7c6	na
třetí	třetí	k4xOgFnSc6	třetí
slabice	slabika	k1gFnSc6	slabika
od	od	k7c2	od
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Přirozeně	přirozeně	k6eAd1	přirozeně
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
slabika	slabika	k1gFnSc1	slabika
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
samohlásku	samohláska	k1gFnSc4	samohláska
nebo	nebo	k8xC	nebo
dvojhlásku	dvojhláska	k1gFnSc4	dvojhláska
<g/>
.	.	kIx.	.
</s>
<s>
Slabika	slabika	k1gFnSc1	slabika
je	být	k5eAaImIp3nS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
polohou	poloha	k1gFnSc7	poloha
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
samohlásce	samohláska	k1gFnSc6	samohláska
následuje	následovat	k5eAaImIp3nS	následovat
skupina	skupina	k1gFnSc1	skupina
nejméně	málo	k6eAd3	málo
dvou	dva	k4xCgFnPc2	dva
souhlásek	souhláska	k1gFnPc2	souhláska
<g/>
.	.	kIx.	.
</s>
<s>
Přízvuk	přízvuk	k1gInSc1	přízvuk
ani	ani	k8xC	ani
délka	délka	k1gFnSc1	délka
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
v	v	k7c6	v
textu	text	k1gInSc6	text
neoznačuje	označovat	k5eNaImIp3nS	označovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
učebnicích	učebnice	k1gFnPc6	učebnice
a	a	k8xC	a
slovnících	slovník	k1gInPc6	slovník
se	se	k3xPyFc4	se
délka	délka	k1gFnSc1	délka
značí	značit	k5eAaImIp3nS	značit
vodorovnou	vodorovný	k2eAgFnSc7d1	vodorovná
čárkou	čárka	k1gFnSc7	čárka
nad	nad	k7c7	nad
písmenem	písmeno	k1gNnSc7	písmeno
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
macron	macron	k1gMnSc1	macron
-	-	kIx~	-
např.	např.	kA	např.
ā	ā	k?	ā
<g/>
;	;	kIx,	;
krátká	krátký	k2eAgFnSc1d1	krátká
slabika	slabika	k1gFnSc1	slabika
se	se	k3xPyFc4	se
značí	značit	k5eAaImIp3nS	značit
obloučkem	oblouček	k1gInSc7	oblouček
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
breve	breve	k1gNnSc1	breve
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ă	ă	k?	ă
<g/>
)	)	kIx)	)
a	a	k8xC	a
přízvuk	přízvuk	k1gInSc4	přízvuk
šikmou	šikmý	k2eAgFnSc7d1	šikmá
čárkou	čárka	k1gFnSc7	čárka
(	(	kIx(	(
<g/>
stejnou	stejný	k2eAgFnSc7d1	stejná
<g/>
,	,	kIx,	,
jaká	jaký	k3yIgFnSc1	jaký
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
dlouhých	dlouhý	k2eAgFnPc6d1	dlouhá
samohláskách	samohláska	k1gFnPc6	samohláska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Latinská	latinský	k2eAgFnSc1d1	Latinská
gramatika	gramatika	k1gFnSc1	gramatika
<g/>
.	.	kIx.	.
</s>
<s>
Latina	latina	k1gFnSc1	latina
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
češtiny	čeština	k1gFnSc2	čeština
pouze	pouze	k6eAd1	pouze
6	[number]	k4	6
pádů	pád	k1gInPc2	pád
<g/>
.	.	kIx.	.
pád	pád	k1gInSc1	pád
nominativ	nominativ	k1gInSc1	nominativ
pád	pád	k1gInSc1	pád
genitiv	genitiv	k1gInSc1	genitiv
pád	pád	k1gInSc1	pád
dativ	dativ	k1gInSc1	dativ
pád	pád	k1gInSc1	pád
akuzativ	akuzativ	k1gInSc1	akuzativ
pád	pád	k1gInSc1	pád
vokativ	vokativ	k1gInSc1	vokativ
pád	pád	k1gInSc1	pád
ablativ	ablativ	k1gInSc1	ablativ
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Latinské	latinský	k2eAgNnSc1d1	latinské
skloňování	skloňování	k1gNnSc1	skloňování
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
latinský	latinský	k2eAgInSc4d1	latinský
název	název	k1gInSc4	název
pro	pro	k7c4	pro
podstatná	podstatný	k2eAgNnPc4d1	podstatné
jména	jméno	k1gNnPc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Substantiva	substantivum	k1gNnPc1	substantivum
jsou	být	k5eAaImIp3nP	být
rozdělena	rozdělit	k5eAaPmNgNnP	rozdělit
do	do	k7c2	do
pěti	pět	k4xCc3	pět
deklinací	deklinace	k1gFnPc2	deklinace
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
zařazení	zařazení	k1gNnSc4	zařazení
do	do	k7c2	do
příslušné	příslušný	k2eAgFnSc2d1	příslušná
deklinace	deklinace	k1gFnSc2	deklinace
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
koncovka	koncovka	k1gFnSc1	koncovka
genitivu	genitiv	k1gInSc2	genitiv
singuláru	singulár	k1gInSc2	singulár
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
latinský	latinský	k2eAgInSc4d1	latinský
název	název	k1gInSc4	název
pro	pro	k7c4	pro
slovesa	sloveso	k1gNnPc4	sloveso
<g/>
.	.	kIx.	.
</s>
<s>
Časování	časování	k1gNnSc1	časování
sloves	sloveso	k1gNnPc2	sloveso
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
v	v	k7c6	v
čtyřech	čtyři	k4xCgFnPc6	čtyři
konjugacích	konjugace	k1gFnPc6	konjugace
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelná	pravidelný	k2eAgNnPc1d1	pravidelné
latinská	latinský	k2eAgNnPc1d1	latinské
slovesa	sloveso	k1gNnPc1	sloveso
jsou	být	k5eAaImIp3nP	být
rozdělena	rozdělit	k5eAaPmNgNnP	rozdělit
do	do	k7c2	do
čtyř	čtyři	k4xCgInPc2	čtyři
konjugací	konjugace	k1gFnPc2	konjugace
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
zařazení	zařazení	k1gNnSc4	zařazení
do	do	k7c2	do
příslušné	příslušný	k2eAgFnSc2d1	příslušná
konjugace	konjugace	k1gFnSc2	konjugace
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
koncovka	koncovka	k1gFnSc1	koncovka
aktivního	aktivní	k2eAgInSc2d1	aktivní
infinitivu	infinitiv	k1gInSc2	infinitiv
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
1	[number]	k4	1
<g/>
.	.	kIx.	.
konjugaci	konjugace	k1gFnSc6	konjugace
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
vzor	vzor	k1gInSc1	vzor
laudā	laudā	k?	laudā
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
chválit	chválit	k5eAaImF	chválit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
2	[number]	k4	2
<g/>
.	.	kIx.	.
konjugaci	konjugace	k1gFnSc6	konjugace
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
vzor	vzor	k1gInSc1	vzor
monē	monē	k?	monē
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
napomínat	napomínat	k5eAaImF	napomínat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
3	[number]	k4	3
<g/>
.	.	kIx.	.
konjugaci	konjugace	k1gFnSc6	konjugace
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
vzor	vzor	k1gInSc1	vzor
legere	legrat	k5eAaPmIp3nS	legrat
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
číst	číst	k5eAaImF	číst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
4	[number]	k4	4
<g/>
.	.	kIx.	.
konjugaci	konjugace	k1gFnSc6	konjugace
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
vzor	vzor	k1gInSc1	vzor
audī	audī	k?	audī
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
slyšet	slyšet	k5eAaImF	slyšet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Latin	latina	k1gFnPc2	latina
regional	regionat	k5eAaImAgMnS	regionat
pronunciation	pronunciation	k1gInSc4	pronunciation
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Vulgární	vulgární	k2eAgFnSc1d1	vulgární
latina	latina	k1gFnSc1	latina
Deponentní	deponentní	k2eAgFnSc1d1	deponentní
sloveso	sloveso	k1gNnSc4	sloveso
Seznam	seznam	k1gInSc1	seznam
jazyků	jazyk	k1gInPc2	jazyk
Antický	antický	k2eAgInSc1d1	antický
Řím	Řím	k1gInSc1	Řím
Římská	římský	k2eAgFnSc1d1	římská
říše	říše	k1gFnSc1	říše
Antika	antika	k1gFnSc1	antika
Řečtina	řečtina	k1gFnSc1	řečtina
L.	L.	kA	L.
<g/>
S.	S.	kA	S.
Okcitánština	Okcitánština	k1gFnSc1	Okcitánština
Lingvistika	lingvistika	k1gFnSc1	lingvistika
Chronogram	chronogram	k1gInSc4	chronogram
Nová	nový	k2eAgFnSc1d1	nová
latina	latina	k1gFnSc1	latina
Latinská	latinský	k2eAgFnSc1d1	Latinská
rčení	rčení	k1gNnSc4	rčení
Latinka	latinka	k1gFnSc1	latinka
Větná	větný	k2eAgFnSc1d1	větná
perioda	perioda	k1gFnSc1	perioda
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
latina	latina	k1gFnSc1	latina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Genedict	Genedict	k1gInSc1	Genedict
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
-	-	kIx~	-
genealogický	genealogický	k2eAgInSc1d1	genealogický
slovník	slovník	k1gInSc1	slovník
a	a	k8xC	a
registr	registr	k1gInSc1	registr
historických	historický	k2eAgInPc2d1	historický
pojmů	pojem	k1gInPc2	pojem
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
na	na	k7c4	na
překlad	překlad	k1gInSc4	překlad
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
latina	latina	k1gFnSc1	latina
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Téma	téma	k1gNnSc1	téma
Latinské	latinský	k2eAgInPc1d1	latinský
výroky	výrok	k1gInPc1	výrok
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Téma	téma	k1gFnSc1	téma
Latinská	latinský	k2eAgFnSc1d1	Latinská
úsloví	úsloví	k1gNnSc4	úsloví
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
