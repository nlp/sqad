<s>
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
komerční	komerční	k2eAgFnSc1d1	komerční
televizní	televizní	k2eAgFnSc1d1	televizní
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Licenci	licence	k1gFnSc4	licence
na	na	k7c4	na
vysílání	vysílání	k1gNnSc4	vysílání
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc4	první
vysílání	vysílání	k1gNnSc4	vysílání
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
až	až	k9	až
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1994	[number]	k4	1994
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
