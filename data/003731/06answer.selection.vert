<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
jako	jako	k8xC	jako
společný	společný	k2eAgInSc4d1	společný
orgán	orgán	k1gInSc4	orgán
slovenského	slovenský	k2eAgInSc2d1	slovenský
komunistického	komunistický	k2eAgInSc2d1	komunistický
i	i	k8xC	i
nekomunistického	komunistický	k2eNgInSc2d1	nekomunistický
odboje	odboj	k1gInSc2	odboj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
(	(	kIx(	(
<g/>
vánoční	vánoční	k2eAgFnSc1d1	vánoční
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
