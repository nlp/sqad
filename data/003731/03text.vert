<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národná	národný	k2eAgFnSc1d1	národná
rada	rada	k1gFnSc1	rada
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
slovenský	slovenský	k2eAgInSc1d1	slovenský
zákonodárný	zákonodárný	k2eAgInSc1d1	zákonodárný
orgán	orgán	k1gInSc1	orgán
<g/>
,	,	kIx,	,
kontinuálně	kontinuálně	k6eAd1	kontinuálně
existující	existující	k2eAgFnSc4d1	existující
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Československa	Československo	k1gNnSc2	Československo
od	od	k7c2	od
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1943	[number]	k4	1943
jako	jako	k8xS	jako
společný	společný	k2eAgInSc4d1	společný
orgán	orgán	k1gInSc4	orgán
slovenského	slovenský	k2eAgInSc2d1	slovenský
komunistického	komunistický	k2eAgInSc2d1	komunistický
i	i	k8xC	i
nekomunistického	komunistický	k2eNgInSc2d1	nekomunistický
odboje	odboj	k1gInSc2	odboj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
spolupráci	spolupráce	k1gFnSc6	spolupráce
(	(	kIx(	(
<g/>
vánoční	vánoční	k2eAgFnSc1d1	vánoční
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
SNR	SNR	kA	SNR
probíhal	probíhat	k5eAaImAgInS	probíhat
zdola	zdola	k6eAd1	zdola
<g/>
,	,	kIx,	,
paralelně	paralelně	k6eAd1	paralelně
(	(	kIx(	(
<g/>
a	a	k8xC	a
zčásti	zčásti	k6eAd1	zčásti
v	v	k7c6	v
rivalitě	rivalita	k1gFnSc6	rivalita
<g/>
)	)	kIx)	)
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
slovenskými	slovenský	k2eAgFnPc7d1	slovenská
odbojovými	odbojový	k2eAgFnPc7d1	odbojová
skupinami	skupina	k1gFnPc7	skupina
navázanými	navázaný	k2eAgFnPc7d1	navázaná
na	na	k7c4	na
zahraniční	zahraniční	k2eAgInSc4d1	zahraniční
československý	československý	k2eAgInSc4d1	československý
odboj	odboj	k1gInSc4	odboj
okolo	okolo	k7c2	okolo
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
tyto	tento	k3xDgFnPc1	tento
skupiny	skupina	k1gFnPc1	skupina
(	(	kIx(	(
<g/>
Flóra	Flóra	k1gFnSc1	Flóra
<g/>
,	,	kIx,	,
okruh	okruh	k1gInSc4	okruh
okolo	okolo	k6eAd1	okolo
Vavro	Vavro	k1gNnSc1	Vavro
Šrobára	Šrobár	k1gMnSc2	Šrobár
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
prosazovaly	prosazovat	k5eAaImAgFnP	prosazovat
v	v	k7c6	v
státoprávních	státoprávní	k2eAgFnPc6d1	státoprávní
otázkách	otázka	k1gFnPc6	otázka
víceméně	víceméně	k9	víceméně
návrat	návrat	k1gInSc4	návrat
do	do	k7c2	do
předmnichovských	předmnichovský	k2eAgInPc2d1	předmnichovský
poměrů	poměr	k1gInPc2	poměr
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
s	s	k7c7	s
jistou	jistý	k2eAgFnSc7d1	jistá
mírou	míra	k1gFnSc7	míra
decentralizace	decentralizace	k1gFnSc2	decentralizace
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
SNR	SNR	kA	SNR
prosazovala	prosazovat	k5eAaImAgFnS	prosazovat
česko-slovenský	českolovenský	k2eAgInSc4d1	česko-slovenský
vztah	vztah	k1gInSc4	vztah
jako	jako	k8xC	jako
vztah	vztah	k1gInSc4	vztah
rovného	rovný	k2eAgMnSc2d1	rovný
s	s	k7c7	s
rovným	rovný	k2eAgMnSc7d1	rovný
(	(	kIx(	(
<g/>
třebaže	třebaže	k8xS	třebaže
přesné	přesný	k2eAgInPc4d1	přesný
obrysy	obrys	k1gInPc4	obrys
státoprávního	státoprávní	k2eAgNnSc2d1	státoprávní
uspořádání	uspořádání	k1gNnSc2	uspořádání
vánoční	vánoční	k2eAgFnSc1d1	vánoční
dohoda	dohoda	k1gFnSc1	dohoda
neřešila	řešit	k5eNaImAgFnS	řešit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1943	[number]	k4	1943
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
byla	být	k5eAaImAgFnS	být
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
jen	jen	k6eAd1	jen
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
slovenských	slovenský	k2eAgFnPc2d1	slovenská
odbojových	odbojový	k2eAgFnPc2d1	odbojová
skupin	skupina	k1gFnPc2	skupina
(	(	kIx(	(
<g/>
byť	byť	k8xS	byť
významnou	významný	k2eAgFnSc4d1	významná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
faktického	faktický	k2eAgInSc2d1	faktický
vládního	vládní	k2eAgInSc2d1	vládní
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
během	během	k7c2	během
slovenského	slovenský	k2eAgNnSc2d1	slovenské
národního	národní	k2eAgNnSc2d1	národní
povstání	povstání	k1gNnSc2	povstání
od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
SNR	SNR	kA	SNR
stala	stát	k5eAaPmAgFnS	stát
zákonodárným	zákonodárný	k2eAgInSc7d1	zákonodárný
a	a	k8xC	a
výkonným	výkonný	k2eAgInSc7d1	výkonný
orgánem	orgán	k1gInSc7	orgán
na	na	k7c6	na
území	území	k1gNnSc6	území
ovládaném	ovládaný	k2eAgNnSc6d1	ovládané
povstalci	povstalec	k1gMnPc7	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
povstání	povstání	k1gNnPc2	povstání
ovšem	ovšem	k9	ovšem
SNR	SNR	kA	SNR
svedla	svést	k5eAaPmAgFnS	svést
mocenský	mocenský	k2eAgInSc4d1	mocenský
boj	boj	k1gInSc4	boj
o	o	k7c4	o
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
roli	role	k1gFnSc4	role
na	na	k7c6	na
osvobozeném	osvobozený	k2eAgNnSc6d1	osvobozené
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Banské	banský	k2eAgFnSc2d1	Banská
Bystrice	Bystrica	k1gFnSc2	Bystrica
totiž	totiž	k9	totiž
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
revolty	revolta	k1gFnSc2	revolta
přesídlil	přesídlit	k5eAaPmAgMnS	přesídlit
Vavro	Vavro	k1gNnSc4	Vavro
Šrobár	Šrobár	k1gInSc1	Šrobár
a	a	k8xC	a
ustavil	ustavit	k5eAaPmAgInS	ustavit
zde	zde	k6eAd1	zde
Revolučný	Revolučný	k2eAgInSc1d1	Revolučný
národný	národný	k2eAgInSc1d1	národný
výbor	výbor	k1gInSc1	výbor
(	(	kIx(	(
<g/>
RNV	RNV	kA	RNV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předáci	předák	k1gMnPc1	předák
SNR	SNR	kA	SNR
Gustáv	Gustáva	k1gFnPc2	Gustáva
Husák	Husák	k1gMnSc1	Husák
a	a	k8xC	a
Jozef	Jozef	k1gMnSc1	Jozef
Lettrich	Lettrich	k1gMnSc1	Lettrich
ovšem	ovšem	k9	ovšem
Šrobára	Šrobár	k1gMnSc4	Šrobár
donutili	donutit	k5eAaPmAgMnP	donutit
RNV	RNV	kA	RNV
rozpustit	rozpustit	k5eAaPmF	rozpustit
a	a	k8xC	a
včlenit	včlenit	k5eAaPmF	včlenit
se	se	k3xPyFc4	se
do	do	k7c2	do
SNR	SNR	kA	SNR
<g/>
.	.	kIx.	.
</s>
<s>
Fungoval	fungovat	k5eAaImAgMnS	fungovat
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
pak	pak	k6eAd1	pak
jako	jako	k8xS	jako
její	její	k3xOp3gMnSc1	její
předseda	předseda	k1gMnSc1	předseda
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1944	[number]	k4	1944
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
SNR	SNR	kA	SNR
z	z	k7c2	z
ilegality	ilegalita	k1gFnSc2	ilegalita
a	a	k8xC	a
představila	představit	k5eAaPmAgFnS	představit
se	se	k3xPyFc4	se
jako	jako	k9	jako
reprezentant	reprezentant	k1gMnSc1	reprezentant
státní	státní	k2eAgFnSc2d1	státní
moci	moc	k1gFnSc2	moc
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
existenci	existence	k1gFnSc3	existence
souvislého	souvislý	k2eAgInSc2d1	souvislý
povstalci	povstalec	k1gMnPc7	povstalec
kontrolovaného	kontrolovaný	k2eAgNnSc2d1	kontrolované
území	území	k1gNnSc2	území
se	se	k3xPyFc4	se
i	i	k9	i
fakticky	fakticky	k6eAd1	fakticky
ujala	ujmout	k5eAaPmAgFnS	ujmout
těchto	tento	k3xDgFnPc2	tento
pravomocí	pravomoc	k1gFnPc2	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
exekutivní	exekutivní	k2eAgFnSc4d1	exekutivní
moc	moc	k1gFnSc4	moc
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
Sbor	sbor	k1gInSc4	sbor
pověřenců	pověřenec	k1gMnPc2	pověřenec
<g/>
.	.	kIx.	.
</s>
<s>
Delegace	delegace	k1gFnSc1	delegace
SNR	SNR	kA	SNR
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
letecky	letecky	k6eAd1	letecky
přepravila	přepravit	k5eAaPmAgFnS	přepravit
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
a	a	k8xC	a
jednala	jednat	k5eAaImAgFnS	jednat
s	s	k7c7	s
Edvardem	Edvard	k1gMnSc7	Edvard
Benešem	Beneš	k1gMnSc7	Beneš
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
debaty	debata	k1gFnPc4	debata
a	a	k8xC	a
polemiky	polemika	k1gFnPc4	polemika
o	o	k7c6	o
pravomocech	pravomoc	k1gFnPc6	pravomoc
SNR	SNR	kA	SNR
a	a	k8xC	a
česko-slovenském	českolovenský	k2eAgInSc6d1	česko-slovenský
poměru	poměr	k1gInSc6	poměr
ovšem	ovšem	k9	ovšem
nebyly	být	k5eNaImAgFnP	být
dokončeny	dokončit	k5eAaPmNgFnP	dokončit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1944	[number]	k4	1944
bylo	být	k5eAaImAgNnS	být
slovenské	slovenský	k2eAgNnSc1d1	slovenské
národní	národní	k2eAgNnSc1d1	národní
povstání	povstání	k1gNnSc1	povstání
vojensky	vojensky	k6eAd1	vojensky
poraženo	porazit	k5eAaPmNgNnS	porazit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
byla	být	k5eAaImAgFnS	být
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
slovenským	slovenský	k2eAgMnSc7d1	slovenský
zákonodárným	zákonodárný	k2eAgInSc7d1	zákonodárný
sborem	sbor	k1gInSc7	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
složení	složení	k1gNnSc1	složení
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
vytvářeno	vytvářit	k5eAaPmNgNnS	vytvářit
kooptacemi	kooptace	k1gFnPc7	kooptace
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
bylo	být	k5eAaImAgNnS	být
složení	složení	k1gNnSc1	složení
SNR	SNR	kA	SNR
určeno	určit	k5eAaPmNgNnS	určit
výsledky	výsledek	k1gInPc7	výsledek
celostátních	celostátní	k2eAgFnPc2d1	celostátní
voleb	volba	k1gFnPc2	volba
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
SNR	SNR	kA	SNR
působil	působit	k5eAaImAgInS	působit
Sbor	sbor	k1gInSc1	sbor
pověřenců	pověřenec	k1gMnPc2	pověřenec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
slovenským	slovenský	k2eAgInSc7d1	slovenský
národním	národní	k2eAgInSc7d1	národní
orgánem	orgán	k1gInSc7	orgán
moci	moc	k1gFnSc2	moc
výkonné	výkonný	k2eAgFnSc2d1	výkonná
<g/>
.	.	kIx.	.
</s>
<s>
Kompetence	kompetence	k1gFnSc1	kompetence
SNR	SNR	kA	SNR
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	let	k1gInPc6	let
1945	[number]	k4	1945
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
výrazně	výrazně	k6eAd1	výrazně
proměňovaly	proměňovat	k5eAaImAgInP	proměňovat
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
trojí	trojí	k4xRgFnPc4	trojí
takzvané	takzvaný	k2eAgFnPc4d1	takzvaná
Pražské	pražský	k2eAgFnPc4d1	Pražská
dohody	dohoda	k1gFnPc4	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
poslední	poslední	k2eAgMnSc1d1	poslední
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
znamenala	znamenat	k5eAaImAgFnS	znamenat
výraznou	výrazný	k2eAgFnSc4d1	výrazná
redukci	redukce	k1gFnSc4	redukce
pravomocí	pravomoc	k1gFnPc2	pravomoc
SNR	SNR	kA	SNR
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
motivována	motivovat	k5eAaBmNgFnS	motivovat
zejména	zejména	k9	zejména
tlakem	tlak	k1gInSc7	tlak
KSČ	KSČ	kA	KSČ
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
KSS	KSS	kA	KSS
na	na	k7c4	na
omezení	omezení	k1gNnSc4	omezení
vlivu	vliv	k1gInSc2	vliv
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
díky	dík	k1gInPc7	dík
úspěchu	úspěch	k1gInSc2	úspěch
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
získala	získat	k5eAaPmAgFnS	získat
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Nekomunistické	komunistický	k2eNgFnPc1d1	nekomunistická
české	český	k2eAgFnPc1d1	Česká
strany	strana	k1gFnPc1	strana
nakonec	nakonec	k6eAd1	nakonec
po	po	k7c6	po
jistém	jistý	k2eAgNnSc6d1	jisté
váhání	váhání	k1gNnSc6	váhání
dohodu	dohoda	k1gFnSc4	dohoda
podpořily	podpořit	k5eAaPmAgInP	podpořit
<g/>
.	.	kIx.	.
</s>
<s>
Sbor	sbor	k1gInSc1	sbor
pověřenců	pověřenec	k1gMnPc2	pověřenec
jako	jako	k8xC	jako
celek	celek	k1gInSc1	celek
byl	být	k5eAaImAgInS	být
podřízen	podřízen	k2eAgInSc1d1	podřízen
československé	československý	k2eAgFnSc3d1	Československá
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
byla	být	k5eAaImAgFnS	být
ústavně	ústavně	k6eAd1	ústavně
plně	plně	k6eAd1	plně
zlegalizována	zlegalizovat	k5eAaPmNgFnS	zlegalizovat
novou	nový	k2eAgFnSc7d1	nová
Ústavou	ústava	k1gFnSc7	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
(	(	kIx(	(
<g/>
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
existovala	existovat	k5eAaImAgNnP	existovat
bez	bez	k7c2	bez
právního	právní	k2eAgInSc2d1	právní
podkladu	podklad	k1gInSc2	podklad
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
revolučním	revoluční	k2eAgInSc7d1	revoluční
orgánem	orgán	k1gInSc7	orgán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc4	její
dosavadní	dosavadní	k2eAgNnSc4d1	dosavadní
nařízení	nařízení	k1gNnSc4	nařízení
byla	být	k5eAaImAgFnS	být
ústavou	ústava	k1gFnSc7	ústava
prohlášena	prohlášen	k2eAgFnSc1d1	prohlášena
za	za	k7c4	za
zákony	zákon	k1gInPc4	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nové	nový	k2eAgFnSc2d1	nová
Ústavy	ústava	k1gFnSc2	ústava
byla	být	k5eAaImAgFnS	být
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
slovenským	slovenský	k2eAgMnSc7d1	slovenský
národním	národní	k2eAgInSc7d1	národní
zákonodárným	zákonodárný	k2eAgInSc7d1	zákonodárný
sborem	sbor	k1gInSc7	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
vlády	vláda	k1gFnSc2	vláda
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
i	i	k9	i
nadále	nadále	k6eAd1	nadále
vykonával	vykonávat	k5eAaImAgInS	vykonávat
Sbor	sbor	k1gInSc1	sbor
pověřenců	pověřenec	k1gMnPc2	pověřenec
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
mělo	mít	k5eAaImAgNnS	mít
podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
asymetrickou	asymetrický	k2eAgFnSc4d1	asymetrická
státoprávní	státoprávní	k2eAgFnSc4d1	státoprávní
strukturu	struktura	k1gFnSc4	struktura
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
existovaly	existovat	k5eAaImAgInP	existovat
slovenské	slovenský	k2eAgInPc1d1	slovenský
parlamentní	parlamentní	k2eAgInPc1d1	parlamentní
a	a	k8xC	a
vládní	vládní	k2eAgInPc1d1	vládní
orgány	orgán	k1gInPc1	orgán
bez	bez	k7c2	bez
protiváhy	protiváha	k1gFnSc2	protiváha
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
Ústava	ústava	k1gFnSc1	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
zrušila	zrušit	k5eAaPmAgFnS	zrušit
Sbor	sbor	k1gInSc4	sbor
pověřenců	pověřenec	k1gMnPc2	pověřenec
coby	coby	k?	coby
kolektivní	kolektivní	k2eAgInSc1d1	kolektivní
orgán	orgán	k1gInSc1	orgán
(	(	kIx(	(
<g/>
pověřenci	pověřenec	k1gMnPc1	pověřenec
zachováni	zachován	k2eAgMnPc1d1	zachován
<g/>
)	)	kIx)	)
a	a	k8xC	a
zásadně	zásadně	k6eAd1	zásadně
omezila	omezit	k5eAaPmAgFnS	omezit
pravomoci	pravomoc	k1gFnPc4	pravomoc
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
také	také	k9	také
vykonávala	vykonávat	k5eAaImAgFnS	vykonávat
moc	moc	k1gFnSc4	moc
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
<g/>
.	.	kIx.	.
</s>
<s>
Zachován	zachován	k2eAgInSc1d1	zachován
byl	být	k5eAaImAgInS	být
asymetrický	asymetrický	k2eAgInSc1d1	asymetrický
model	model	k1gInSc1	model
státoprávního	státoprávní	k2eAgNnSc2d1	státoprávní
uspořádání	uspořádání	k1gNnSc2	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgInP	mít
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
konat	konat	k5eAaImF	konat
volby	volba	k1gFnPc4	volba
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
politickým	politický	k2eAgFnPc3d1	politická
změnám	změna	k1gFnPc3	změna
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
přineslo	přinést	k5eAaPmAgNnS	přinést
pražské	pražský	k2eAgNnSc1d1	Pražské
jaro	jaro	k1gNnSc1	jaro
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
ovšem	ovšem	k9	ovšem
volby	volba	k1gFnPc1	volba
několikrát	několikrát	k6eAd1	několikrát
odloženy	odložit	k5eAaPmNgFnP	odložit
(	(	kIx(	(
<g/>
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
stávající	stávající	k2eAgFnSc2d1	stávající
SNR	SNR	kA	SNR
určené	určený	k2eAgFnSc2d1	určená
volbami	volba	k1gFnPc7	volba
roku	rok	k1gInSc3	rok
1964	[number]	k4	1964
bylo	být	k5eAaImAgNnS	být
prodlouženo	prodloužen	k2eAgNnSc1d1	prodlouženo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
federalizace	federalizace	k1gFnSc1	federalizace
Československa	Československo	k1gNnSc2	Československo
koncem	konec	k1gInSc7	konec
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přijat	přijmout	k5eAaPmNgInS	přijmout
Ústavní	ústavní	k2eAgInSc1d1	ústavní
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
československé	československý	k2eAgFnSc6d1	Československá
federaci	federace	k1gFnSc6	federace
<g/>
.	.	kIx.	.
</s>
<s>
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
zákonodárným	zákonodárný	k2eAgInSc7d1	zákonodárný
sborem	sbor	k1gInSc7	sbor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předchozího	předchozí	k2eAgInSc2d1	předchozí
asymetrického	asymetrický	k2eAgInSc2d1	asymetrický
modelu	model	k1gInSc2	model
<g/>
)	)	kIx)	)
nyní	nyní	k6eAd1	nyní
svůj	svůj	k3xOyFgInSc4	svůj
protějšek	protějšek	k1gInSc4	protějšek
v	v	k7c6	v
českých	český	k2eAgInPc6d1	český
republikových	republikový	k2eAgInPc6d1	republikový
orgánech	orgán	k1gInPc6	orgán
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
národní	národní	k2eAgFnSc1d1	národní
rada	rada	k1gFnSc1	rada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
řádných	řádný	k2eAgFnPc6d1	řádná
volbách	volba	k1gFnPc6	volba
SNR	SNR	kA	SNR
zvolena	zvolit	k5eAaPmNgFnS	zvolit
až	až	k6eAd1	až
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
podobě	podoba	k1gFnSc6	podoba
se	se	k3xPyFc4	se
SNR	SNR	kA	SNR
udržela	udržet	k5eAaPmAgFnS	udržet
i	i	k9	i
v	v	k7c6	v
krátkém	krátký	k2eAgNnSc6d1	krátké
období	období	k1gNnSc6	období
federace	federace	k1gFnSc2	federace
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1992	[number]	k4	1992
byla	být	k5eAaImAgFnS	být
podle	podle	k7c2	podle
Ústavy	ústava	k1gFnSc2	ústava
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
SNR	SNR	kA	SNR
přejmenována	přejmenovat	k5eAaPmNgFnS	přejmenovat
na	na	k7c4	na
Národní	národní	k2eAgFnSc4d1	národní
rada	rada	k1gMnSc1	rada
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
jako	jako	k8xS	jako
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
zákonodárný	zákonodárný	k2eAgInSc1d1	zákonodárný
sbor	sbor	k1gInSc1	sbor
chystané	chystaný	k2eAgFnSc2d1	chystaná
samostatné	samostatný	k2eAgFnSc2d1	samostatná
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
bylo	být	k5eAaImAgNnS	být
určováno	určovat	k5eAaImNgNnS	určovat
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
<g/>
:	:	kIx,	:
volby	volba	k1gFnSc2	volba
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
(	(	kIx(	(
<g/>
nepřímé	přímý	k2eNgFnSc2d1	nepřímá
<g/>
)	)	kIx)	)
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
1948	[number]	k4	1948
Volby	volba	k1gFnSc2	volba
do	do	k7c2	do
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
1954	[number]	k4	1954
Volby	volba	k1gFnSc2	volba
do	do	k7c2	do
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
1960	[number]	k4	1960
Volby	volba	k1gFnSc2	volba
do	do	k7c2	do
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
1964	[number]	k4	1964
Volby	volba	k1gFnSc2	volba
do	do	k7c2	do
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
1968	[number]	k4	1968
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
plánované	plánovaný	k2eAgNnSc1d1	plánované
<g/>
,	,	kIx,	,
nerealizované	realizovaný	k2eNgFnPc1d1	nerealizovaná
<g/>
)	)	kIx)	)
Volby	volba	k1gFnPc1	volba
do	do	k7c2	do
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
1971	[number]	k4	1971
Volby	volba	k1gFnSc2	volba
do	do	k7c2	do
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
1976	[number]	k4	1976
Volby	volba	k1gFnSc2	volba
do	do	k7c2	do
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
1981	[number]	k4	1981
Volby	volba	k1gFnSc2	volba
do	do	k7c2	do
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
1986	[number]	k4	1986
Volby	volba	k1gFnSc2	volba
do	do	k7c2	do
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
1990	[number]	k4	1990
Volby	volba	k1gFnSc2	volba
do	do	k7c2	do
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
1992	[number]	k4	1992
Vavro	Vavro	k1gNnSc4	Vavro
Šrobár	Šrobár	k1gInSc1	Šrobár
a	a	k8xC	a
Karol	Karol	k1gInSc1	Karol
Šmidke	Šmidke	k1gNnSc2	Šmidke
(	(	kIx(	(
<g/>
září	září	k1gNnSc2	září
<g />
.	.	kIx.	.
</s>
<s>
1944	[number]	k4	1944
–	–	k?	–
říjen	říjen	k1gInSc1	říjen
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
Jozef	Jozef	k1gMnSc1	Jozef
Lettrich	Lettrich	k1gMnSc1	Lettrich
a	a	k8xC	a
Karol	Karol	k1gInSc1	Karol
Šmidke	Šmidk	k1gInSc2	Šmidk
(	(	kIx(	(
<g/>
duben	duben	k1gInSc1	duben
1945	[number]	k4	1945
–	–	k?	–
září	zářit	k5eAaImIp3nS	zářit
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
Jozef	Jozef	k1gInSc1	Jozef
Lettrich	Lettricha	k1gFnPc2	Lettricha
(	(	kIx(	(
<g/>
září	září	k1gNnSc2	září
1945	[number]	k4	1945
–	–	k?	–
únor	únor	k1gInSc1	únor
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
Karol	Karol	k1gInSc1	Karol
Šmidke	Šmidk	k1gInSc2	Šmidk
(	(	kIx(	(
<g/>
únor	únor	k1gInSc1	únor
1948	[number]	k4	1948
–	–	k?	–
červenec	červenec	k1gInSc1	červenec
1950	[number]	k4	1950
<g/>
)	)	kIx)	)
František	František	k1gMnSc1	František
Kubač	Kubač	k1gMnSc1	Kubač
(	(	kIx(	(
<g/>
červenec	červenec	k1gInSc1	červenec
1950	[number]	k4	1950
–	–	k?	–
červen	červen	k1gInSc1	červen
<g />
.	.	kIx.	.
</s>
<s>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
Ľudovít	Ľudovít	k1gFnSc1	Ľudovít
Benada	Benada	k1gFnSc1	Benada
(	(	kIx(	(
<g/>
červen	červen	k1gInSc1	červen
1958	[number]	k4	1958
–	–	k?	–
červenec	červenec	k1gInSc1	červenec
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
Rudolf	Rudolf	k1gMnSc1	Rudolf
Strechaj	Strechaj	k1gMnSc1	Strechaj
(	(	kIx(	(
<g/>
červenec	červenec	k1gInSc1	červenec
1960	[number]	k4	1960
–	–	k?	–
červenec	červenec	k1gInSc1	červenec
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
Jozef	Jozef	k1gInSc1	Jozef
Lenárt	Lenárt	k1gInSc1	Lenárt
(	(	kIx(	(
<g/>
říjen	říjen	k1gInSc1	říjen
1962	[number]	k4	1962
–	–	k?	–
září	zářit	k5eAaImIp3nS	zářit
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
Michal	Michal	k1gMnSc1	Michal
Chudík	Chudík	k1gMnSc1	Chudík
(	(	kIx(	(
<g/>
září	zářit	k5eAaImIp3nS	zářit
1963	[number]	k4	1963
–	–	k?	–
březen	březen	k1gInSc1	březen
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Ondrej	Ondrej	k1gFnSc1	Ondrej
Klokoč	klokoč	k1gInSc1	klokoč
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
březen	březen	k1gInSc1	březen
1968	[number]	k4	1968
–	–	k?	–
březen	březen	k1gInSc1	březen
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Viliam	Viliam	k1gMnSc1	Viliam
Šalgovič	Šalgovič	k1gMnSc1	Šalgovič
(	(	kIx(	(
<g/>
červenec	červenec	k1gInSc1	červenec
1975	[number]	k4	1975
–	–	k?	–
listopad	listopad	k1gInSc1	listopad
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Rudolf	Rudolf	k1gMnSc1	Rudolf
Schuster	Schuster	k1gMnSc1	Schuster
(	(	kIx(	(
<g/>
listopad	listopad	k1gInSc1	listopad
1989	[number]	k4	1989
–	–	k?	–
červen	červen	k1gInSc1	červen
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
František	František	k1gMnSc1	František
Mikloško	Mikloška	k1gFnSc5	Mikloška
(	(	kIx(	(
<g/>
červen	červen	k1gInSc1	červen
1990	[number]	k4	1990
–	–	k?	–
červen	červen	k1gInSc1	červen
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Ivan	Ivan	k1gMnSc1	Ivan
Gašparovič	Gašparovič	k1gMnSc1	Gašparovič
(	(	kIx(	(
<g/>
červen	červen	k1gInSc1	červen
1992	[number]	k4	1992
–	–	k?	–
říjen	říjen	k1gInSc1	říjen
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
</s>
