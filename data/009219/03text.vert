<p>
<s>
Origin	Origin	k1gInSc1	Origin
je	být	k5eAaImIp3nS	být
digitální	digitální	k2eAgFnSc1d1	digitální
distribuce	distribuce	k1gFnSc1	distribuce
a	a	k8xC	a
správa	správa	k1gFnSc1	správa
digitálních	digitální	k2eAgNnPc2d1	digitální
práv	právo	k1gNnPc2	právo
systému	systém	k1gInSc2	systém
od	od	k7c2	od
Electronic	Electronice	k1gFnPc2	Electronice
Arts	Arts	k1gInSc1	Arts
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
uživatelům	uživatel	k1gMnPc3	uživatel
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
nákup	nákup	k1gInSc4	nákup
počítačové	počítačový	k2eAgFnSc2d1	počítačová
hry	hra	k1gFnSc2	hra
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
a	a	k8xC	a
možnost	možnost	k1gFnSc4	možnost
stáhnutí	stáhnutí	k1gNnSc2	stáhnutí
přímo	přímo	k6eAd1	přímo
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
počítače	počítač	k1gInSc2	počítač
pomocí	pomocí	k7c2	pomocí
programu	program	k1gInSc2	program
EA	EA	kA	EA
Download	Download	k1gInSc1	Download
Manager	manager	k1gMnSc1	manager
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
EA	EA	kA	EA
Downloader	Downloadra	k1gFnPc2	Downloadra
a	a	k8xC	a
EA	EA	kA	EA
Link	Link	k1gInSc1	Link
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
EA	EA	kA	EA
Download	Download	k1gInSc1	Download
Manager	manager	k1gMnSc1	manager
==	==	k?	==
</s>
</p>
<p>
<s>
EA	EA	kA	EA
Download	Download	k1gInSc1	Download
Manager	manager	k1gMnSc1	manager
je	být	k5eAaImIp3nS	být
software	software	k1gInSc4	software
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
uživatelům	uživatel	k1gMnPc3	uživatel
po	po	k7c6	po
zaplacení	zaplacení	k1gNnSc6	zaplacení
stahovat	stahovat	k5eAaImF	stahovat
originální	originální	k2eAgFnPc4d1	originální
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
datadisky	datadisek	k1gInPc4	datadisek
<g/>
,	,	kIx,	,
herní	herní	k2eAgInPc4d1	herní
bonusy	bonus	k1gInPc4	bonus
a	a	k8xC	a
patche	patch	k1gInPc4	patch
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Electronic	Electronice	k1gFnPc2	Electronice
Arts	Artsa	k1gFnPc2	Artsa
<g/>
.	.	kIx.	.
</s>
<s>
Nabízí	nabízet	k5eAaImIp3nS	nabízet
možnost	možnost	k1gFnSc4	možnost
zobrazovaní	zobrazovaný	k2eAgMnPc1d1	zobrazovaný
dostupných	dostupný	k2eAgInPc2d1	dostupný
komponentů	komponent	k1gInPc2	komponent
<g/>
.	.	kIx.	.
</s>
<s>
EA	EA	kA	EA
Download	Download	k1gInSc1	Download
Manager	manager	k1gMnSc1	manager
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
podobném	podobný	k2eAgInSc6d1	podobný
systému	systém	k1gInSc6	systém
jako	jako	k8xC	jako
Steam	Steam	k1gInSc1	Steam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
při	při	k7c6	při
instalaci	instalace	k1gFnSc6	instalace
online	onlinout	k5eAaPmIp3nS	onlinout
manager	manager	k1gMnSc1	manager
(	(	kIx(	(
<g/>
EA	EA	kA	EA
Download	Download	k1gInSc4	Download
Manager	manager	k1gMnSc1	manager
<g/>
,	,	kIx,	,
Steam	Steam	k1gInSc1	Steam
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
a	a	k8xC	a
připojení	připojení	k1gNnSc4	připojení
k	k	k7c3	k
internetu	internet	k1gInSc3	internet
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
správné	správný	k2eAgFnSc3d1	správná
funkčnosti	funkčnost	k1gFnSc3	funkčnost
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
EA	EA	kA	EA
Downloader	Downloader	k1gInSc1	Downloader
byl	být	k5eAaImAgInS	být
spuštěn	spustit	k5eAaPmNgInS	spustit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
programem	program	k1gInSc7	program
EA	EA	kA	EA
Link	Link	k1gInSc1	Link
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
přidány	přidat	k5eAaPmNgInP	přidat
trailery	trailer	k1gInPc1	trailer
<g/>
,	,	kIx,	,
dema	dema	k6eAd1	dema
a	a	k8xC	a
speciální	speciální	k2eAgInSc4d1	speciální
obsah	obsah	k1gInSc4	obsah
k	k	k7c3	k
doručovacím	doručovací	k2eAgFnPc3d1	doručovací
službám	služba	k1gFnPc3	služba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
nahrazen	nahradit	k5eAaPmNgInS	nahradit
kombinací	kombinace	k1gFnSc7	kombinace
EA	EA	kA	EA
Store	Stor	k1gInSc5	Stor
a	a	k8xC	a
EA	EA	kA	EA
Download	Download	k1gInSc1	Download
Manager	manager	k1gMnSc1	manager
se	s	k7c7	s
zkráceným	zkrácený	k2eAgInSc7d1	zkrácený
názvem	název	k1gInSc7	název
EADM	EADM	kA	EADM
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelé	uživatel	k1gMnPc1	uživatel
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
nakupují	nakupovat	k5eAaBmIp3nP	nakupovat
na	na	k7c6	na
internetových	internetový	k2eAgFnPc6d1	internetová
stránkách	stránka	k1gFnPc6	stránka
EA	EA	kA	EA
Store	Stor	k1gMnSc5	Stor
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
EADM	EADM	kA	EADM
klienta	klient	k1gMnSc4	klient
pro	pro	k7c4	pro
stažení	stažení	k1gNnSc4	stažení
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Digitální	digitální	k2eAgFnSc1d1	digitální
distribuce	distribuce	k1gFnSc1	distribuce
softwaru	software	k1gInSc2	software
byla	být	k5eAaImAgFnS	být
nejprve	nejprve	k6eAd1	nejprve
využívána	využívat	k5eAaImNgFnS	využívat
k	k	k7c3	k
doručování	doručování	k1gNnSc3	doručování
datadisku	datadisek	k1gInSc2	datadisek
Battlefield	Battlefieldo	k1gNnPc2	Battlefieldo
2	[number]	k4	2
<g/>
:	:	kIx,	:
Special	Special	k1gMnSc1	Special
Forces	Forces	k1gMnSc1	Forces
a	a	k8xC	a
následně	následně	k6eAd1	následně
pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
titulů	titul	k1gInPc2	titul
od	od	k7c2	od
EA	EA	kA	EA
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
produkce	produkce	k1gFnSc1	produkce
začala	začít	k5eAaPmAgFnS	začít
u	u	k7c2	u
titulu	titul	k1gInSc2	titul
Spore	spor	k1gInSc5	spor
Creature	Creatur	k1gMnSc5	Creatur
Creator	Creator	k1gInSc1	Creator
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zápory	zápor	k1gInPc4	zápor
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Omezení	omezení	k1gNnSc1	omezení
stahování	stahování	k1gNnSc2	stahování
===	===	k?	===
</s>
</p>
<p>
<s>
Uživatelé	uživatel	k1gMnPc1	uživatel
měli	mít	k5eAaImAgMnP	mít
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
od	od	k7c2	od
nakoupení	nakoupení	k1gNnSc2	nakoupení
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
přes	přes	k7c4	přes
EADM	EADM	kA	EADM
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
už	už	k9	už
však	však	k9	však
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
časové	časový	k2eAgNnSc1d1	časové
omezení	omezení	k1gNnSc1	omezení
zrušeno	zrušen	k2eAgNnSc1d1	zrušeno
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
pro	pro	k7c4	pro
znovustažení	znovustažení	k1gNnSc4	znovustažení
při	při	k7c6	při
přeinstalovaní	přeinstalovaný	k2eAgMnPc1d1	přeinstalovaný
PC	PC	kA	PC
apod	apod	kA	apod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
podle	podle	k7c2	podle
informací	informace	k1gFnPc2	informace
EA	EA	kA	EA
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
Váš	váš	k3xOp2gInSc4	váš
účet	účet	k1gInSc4	účet
není	být	k5eNaImIp3nS	být
aktivní	aktivní	k2eAgFnSc1d1	aktivní
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
24	[number]	k4	24
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Regionální	regionální	k2eAgFnPc1d1	regionální
ceny	cena	k1gFnPc1	cena
===	===	k?	===
</s>
</p>
<p>
<s>
Ceny	cena	k1gFnPc1	cena
EA	EA	kA	EA
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c4	na
umístění	umístění	k1gNnSc4	umístění
sídla	sídlo	k1gNnSc2	sídlo
zákazníka	zákazník	k1gMnSc2	zákazník
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
regionálních	regionální	k2eAgInPc2d1	regionální
rozdílů	rozdíl	k1gInPc2	rozdíl
a	a	k8xC	a
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
výkyvů	výkyv	k1gInPc2	výkyv
měny	měna	k1gFnSc2	měna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Změny	změna	k1gFnPc1	změna
platformy	platforma	k1gFnSc2	platforma
===	===	k?	===
</s>
</p>
<p>
<s>
Uživatelé	uživatel	k1gMnPc1	uživatel
si	se	k3xPyFc3	se
stěžovali	stěžovat	k5eAaImAgMnP	stěžovat
na	na	k7c4	na
častou	častý	k2eAgFnSc4d1	častá
změnu	změna	k1gFnSc4	změna
softwarové	softwarový	k2eAgFnSc2d1	softwarová
platformy	platforma	k1gFnSc2	platforma
(	(	kIx(	(
<g/>
EA	EA	kA	EA
Downloader	Downloader	k1gInSc1	Downloader
<g/>
,	,	kIx,	,
EA	EA	kA	EA
Link	Link	k1gInSc1	Link
<g/>
,	,	kIx,	,
EAMD	EAMD	kA	EAMD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
EA	EA	kA	EA
však	však	k9	však
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
hry	hra	k1gFnPc1	hra
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
stahovali	stahovat	k5eAaImAgMnP	stahovat
přes	přes	k7c4	přes
EA	EA	kA	EA
Link	Link	k1gInSc4	Link
je	být	k5eAaImIp3nS	být
i	i	k9	i
nadále	nadále	k6eAd1	nadále
možno	možno	k6eAd1	možno
stahovat	stahovat	k5eAaImF	stahovat
přes	přes	k7c4	přes
EA	EA	kA	EA
Download	Download	k1gInSc4	Download
Manager	manager	k1gMnSc1	manager
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
EA	EA	kA	EA
Store	Stor	k1gInSc5	Stor
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
