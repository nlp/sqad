<s>
Španělská	španělský	k2eAgFnSc1d1
hymna	hymna	k1gFnSc1
</s>
<s>
Marcha	Marcha	k1gFnSc1
Real	Real	k1gInSc1
</s>
<s>
Královský	královský	k2eAgInSc1d1
pochod	pochod	k1gInSc1
Státní	státní	k2eAgInSc1d1
znak	znak	k1gInSc1
ŠpanělskaHymna	ŠpanělskaHymn	k1gInSc2
</s>
<s>
Španělsko	Španělsko	k1gNnSc1
Španělsko	Španělsko	k1gNnSc1
Přijata	přijat	k2eAgMnSc4d1
</s>
<s>
1770	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
.	.	kIx.
<g/>
listenlist	listenlist	k1gInSc1
<g/>
{	{	kIx(
<g/>
background	background	k1gInSc1
<g/>
:	:	kIx,
<g/>
url	url	k?
<g/>
(	(	kIx(
<g/>
"	"	kIx"
<g/>
//	//	k?
<g/>
upload	upload	k1gInSc1
<g/>
.	.	kIx.
<g/>
wikimedia	wikimedium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
/	/	kIx~
<g/>
wikipedia	wikipedium	k1gNnSc2
<g/>
/	/	kIx~
<g/>
commons	commons	k1gInSc1
<g/>
/	/	kIx~
<g/>
thumb	thumb	k1gInSc1
<g/>
/	/	kIx~
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
94	#num#	k4
<g/>
/	/	kIx~
<g/>
Gnome-speakernotes	Gnome-speakernotesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
/	/	kIx~
<g/>
30	#num#	k4
<g/>
px-Gnome-speakernotes	px-Gnome-speakernotesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
.	.	kIx.
<g/>
png	png	k?
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
;	;	kIx,
<g/>
padding-left	padding-left	k1gInSc1
<g/>
:	:	kIx,
<g/>
40	#num#	k4
<g/>
px	px	k?
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
.	.	kIx.
<g/>
medialist	medialist	k1gInSc1
<g/>
{	{	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
min-height	min-height	k1gInSc1
<g/>
:	:	kIx,
<g/>
50	#num#	k4
<g/>
px	px	k?
<g/>
;	;	kIx,
<g/>
margin	margin	k1gMnSc1
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
em	em	k?
<g/>
;	;	kIx,
<g/>
background-position	background-position	k1gInSc4
<g/>
:	:	kIx,
<g/>
top	topit	k5eAaImRp2nS
left	left	k1gInSc1
<g/>
;	;	kIx,
<g/>
background-repeat	background-repeat	k1gInSc1
<g/>
:	:	kIx,
<g/>
no-repeat	no-repeat	k5eAaImF,k5eAaPmF,k5eAaBmF
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
.	.	kIx.
<g/>
medialist	medialist	k1gInSc1
ul	ul	kA
<g/>
{	{	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
list-style-type	list-style-typ	k1gInSc5
<g/>
:	:	kIx,
<g/>
none	none	k1gNnPc6
<g/>
;	;	kIx,
<g/>
list-style-image	list-style-image	k1gInSc1
<g/>
:	:	kIx,
<g/>
none	none	k1gInSc1
<g/>
;	;	kIx,
<g/>
margin	margin	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
.	.	kIx.
<g/>
medialist	medialist	k1gInSc1
ul	ul	kA
li	li	k8xS
<g/>
{	{	kIx(
<g/>
padding-bottom	padding-bottom	k1gInSc1
<g/>
:	:	kIx,
<g/>
0.5	0.5	k4
<g/>
em	em	k?
<g/>
}	}	kIx)
<g/>
.	.	kIx.
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
.	.	kIx.
<g/>
medialist	medialist	k1gInSc1
ul	ul	kA
li	li	k8xS
li	li	k8xS
<g/>
{	{	kIx(
<g/>
font-size	font-size	k1gFnSc1
<g/>
:	:	kIx,
<g/>
91	#num#	k4
<g/>
%	%	kIx~
<g/>
;	;	kIx,
<g/>
padding-bottom	padding-bottom	k1gInSc1
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
La	la	k1gNnSc4
Marcha	March	k1gMnSc2
Real	Real	k1gInSc4
</s>
<s>
Problémy	problém	k1gInPc1
s	s	k7c7
přehráváním	přehrávání	k1gNnSc7
<g/>
?	?	kIx.
</s>
<s desamb="1">
Nápověda	nápověda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Partitura	partitura	k1gFnSc1
hymny	hymna	k1gFnSc2
ve	v	k7c6
verzi	verze	k1gFnSc6
pro	pro	k7c4
klavír	klavír	k1gInSc4
<g/>
,	,	kIx,
dle	dle	k7c2
Alberta	Albert	k1gMnSc2
Betancourta	Betancourt	k1gMnSc2
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1
hymna	hymna	k1gFnSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
La	la	k1gNnSc4
Marcha	March	k1gMnSc2
Real	Real	k1gInSc4
<g/>
,	,	kIx,
česky	česky	k6eAd1
Královský	královský	k2eAgInSc4d1
pochod	pochod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
hymnou	hymna	k1gFnSc7
Španělského	španělský	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
byla	být	k5eAaImAgFnS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
několika	několik	k4yIc2
národních	národní	k2eAgFnPc2d1
hymen	hymna	k1gFnPc2
beze	beze	k7c2
slov	slovo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Španělská	španělský	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
hymna	hymna	k1gFnSc1
je	být	k5eAaImIp3nS
jednou	jeden	k4xCgFnSc7
z	z	k7c2
nejstarších	starý	k2eAgFnPc2d3
evropských	evropský	k2eAgFnPc2d1
hymen	hymna	k1gFnPc2
a	a	k8xC
její	její	k3xOp3gInSc4
původ	původ	k1gInSc4
není	být	k5eNaImIp3nS
známý	známý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
ní	on	k3xPp3gFnSc6
je	být	k5eAaImIp3nS
roku	rok	k1gInSc2
1761	#num#	k4
v	v	k7c6
knize	kniha	k1gFnSc6
Manuela	Manuel	k1gMnSc2
de	de	k?
Espinosy	Espinosa	k1gFnSc2
(	(	kIx(
<g/>
Libro	libra	k1gFnSc5
de	de	k?
Ordenanza	Ordenanz	k1gMnSc2
de	de	k?
los	los	k1gInSc1
toques	toques	k1gMnSc1
militares	militares	k1gMnSc1
de	de	k?
la	la	k1gNnSc2
Infantería	Infanterí	k2eAgFnSc1d1
Españ	Españ	k1gFnSc1
—	—	k?
Španělská	španělský	k2eAgFnSc1d1
pěchotní	pěchotní	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
troubení	troubení	k1gNnSc2
vojenské	vojenský	k2eAgFnSc2d1
trubky	trubka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Píseň	píseň	k1gFnSc1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
nazvána	nazván	k2eAgNnPc4d1
La	la	k1gNnPc4
Marcha	March	k1gMnSc2
Grenadera	Grenader	k1gMnSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
Pochod	pochod	k1gInSc1
gránátníků	gránátník	k1gMnPc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
autor	autor	k1gMnSc1
písně	píseň	k1gFnSc2
nebyl	být	k5eNaImAgInS
zjištěn	zjistit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1770	#num#	k4
<g/>
,	,	kIx,
král	král	k1gMnSc1
Carlos	Carlos	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
uznal	uznat	k5eAaPmAgMnS
píseň	píseň	k1gFnSc4
Marcha	Marcha	k1gFnSc1
Grenadera	Grenadera	k1gFnSc1
jako	jako	k8xS,k8xC
oficiální	oficiální	k2eAgInSc1d1
sváteční	sváteční	k2eAgInSc1d1
pochod	pochod	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
mohl	moct	k5eAaImAgInS
používat	používat	k5eAaImF
při	při	k7c6
veřejných	veřejný	k2eAgFnPc6d1
událostech	událost	k1gFnPc6
a	a	k8xC
ceremoniích	ceremonie	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
byla	být	k5eAaImAgFnS
vždy	vždy	k6eAd1
hrávána	hrávat	k5eAaImNgFnS
při	při	k7c6
veřejných	veřejný	k2eAgFnPc6d1
událostech	událost	k1gFnPc6
spojených	spojený	k2eAgInPc2d1
s	s	k7c7
královskou	královský	k2eAgFnSc7d1
rodinou	rodina	k1gFnSc7
<g/>
,	,	kIx,
Španělé	Španěl	k1gMnPc1
začali	začít	k5eAaPmAgMnP
píseň	píseň	k1gFnSc4
Marcha	March	k1gMnSc2
Grenadera	Grenader	k1gMnSc2
považovat	považovat	k5eAaImF
jako	jako	k8xC,k8xS
svoji	svůj	k3xOyFgFnSc4
národní	národní	k2eAgFnSc4d1
hymnu	hymna	k1gFnSc4
a	a	k8xC
začali	začít	k5eAaPmAgMnP
ji	on	k3xPp3gFnSc4
nazývat	nazývat	k5eAaImF
Marcha	March	k1gMnSc2
Real	Real	k1gInSc4
<g/>
,	,	kIx,
čili	čili	k8xC
„	„	k?
<g/>
Královský	královský	k2eAgInSc4d1
pochod	pochod	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c2
druhé	druhý	k4xOgFnSc2
španělské	španělský	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
1931	#num#	k4
<g/>
-	-	kIx~
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
La	la	k1gNnSc4
Marcha	March	k1gMnSc2
Real	Real	k1gInSc4
nahrazena	nahradit	k5eAaPmNgFnS
písní	píseň	k1gFnPc2
El	Ela	k1gFnPc2
Himno	Himno	k6eAd1
de	de	k?
Riego	Riego	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
konci	konec	k1gInSc3
občanské	občanský	k2eAgFnSc2d1
války	válka	k1gFnSc2
nicméně	nicméně	k8xC
Francisco	Francisco	k6eAd1
Franco	Franco	k6eAd1
znovu	znovu	k6eAd1
zavedl	zavést	k5eAaPmAgInS
jako	jako	k9
národní	národní	k2eAgFnSc4d1
hymnu	hymna	k1gFnSc4
píseň	píseň	k1gFnSc1
La	la	k1gNnSc4
Marcha	March	k1gMnSc2
Real	Real	k1gInSc4
<g/>
,	,	kIx,
jen	jen	k9
pod	pod	k7c7
jejím	její	k3xOp3gInSc7
starým	starý	k2eAgInSc7d1
názvem	název	k1gInSc7
La	la	k1gNnPc2
Marcha	Marcha	k1gMnSc1
Grenadera	Grenadero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Nynější	nynější	k2eAgFnSc1d1
verze	verze	k1gFnSc1
byla	být	k5eAaImAgFnS
uznána	uznat	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
po	po	k7c6
návratu	návrat	k1gInSc6
ke	k	k7c3
konstituční	konstituční	k2eAgFnSc3d1
monarchii	monarchie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
říjnu	říjen	k1gInSc6
1997	#num#	k4
byla	být	k5eAaImAgFnS
královským	královský	k2eAgInSc7d1
dekretem	dekret	k1gInSc7
upraveno	upraven	k2eAgNnSc4d1
používání	používání	k1gNnSc4
písně	píseň	k1gFnSc2
Marcha	Marcha	k1gMnSc1
Real	Real	k1gInSc1
jako	jako	k8xC,k8xS
národní	národní	k2eAgFnSc2d1
hymny	hymna	k1gFnSc2
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
Olympijskými	olympijský	k2eAgFnPc7d1
hrami	hra	k1gFnPc7
v	v	k7c6
Pekingu	Peking	k1gInSc6
2008	#num#	k4
uspořádal	uspořádat	k5eAaPmAgInS
Španělský	španělský	k2eAgInSc1d1
olympijský	olympijský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
veřejnou	veřejný	k2eAgFnSc4d1
soutěž	soutěž	k1gFnSc4
na	na	k7c4
slova	slovo	k1gNnPc4
hymny	hymna	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ji	on	k3xPp3gFnSc4
španělští	španělský	k2eAgMnPc1d1
sportovci	sportovec	k1gMnPc1
mohli	moct	k5eAaImAgMnP
také	také	k9
zpívat	zpívat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc2
provedení	provedení	k1gNnPc2
v	v	k7c6
podání	podání	k1gNnSc6
Plácida	Plácid	k1gMnSc2
Dominga	Doming	k1gMnSc2
bylo	být	k5eAaImAgNnS
naplánováno	naplánovat	k5eAaBmNgNnS
na	na	k7c4
21	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
text	text	k1gInSc1
hymny	hymna	k1gFnSc2
svým	svůj	k3xOyFgInSc7
podpisem	podpis	k1gInSc7
během	během	k7c2
kampaně	kampaň	k1gFnSc2
podpoří	podpořit	k5eAaPmIp3nS
alespoň	alespoň	k9
půl	půl	k1xP
miliónu	milión	k4xCgInSc2
španělských	španělský	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
o	o	k7c6
něm	on	k3xPp3gInSc6
hlasovat	hlasovat	k5eAaImF
španělský	španělský	k2eAgInSc4d1
parlament	parlament	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Text	text	k1gInSc1
hymny	hymna	k1gFnSc2
</s>
<s>
To	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
Marcha	Marcha	k1gFnSc1
Real	Real	k1gInSc1
dnes	dnes	k6eAd1
nemá	mít	k5eNaImIp3nS
slova	slovo	k1gNnPc1
<g/>
,	,	kIx,
neznamená	znamenat	k5eNaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
nikdy	nikdy	k6eAd1
dřív	dříve	k6eAd2
neměla	mít	k5eNaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
verze	verze	k1gFnSc1
byla	být	k5eAaImAgFnS
používána	používat	k5eAaImNgFnS
během	během	k7c2
vlády	vláda	k1gFnSc2
Alfonsa	Alfons	k1gMnSc2
XIII	XIII	kA
<g/>
.	.	kIx.
a	a	k8xC
další	další	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
používána	používat	k5eAaImNgFnS
během	během	k7c2
diktatury	diktatura	k1gFnSc2
generalissima	generalissimus	k1gMnSc2
Franca	Franca	k?
<g/>
,	,	kIx,
nicméně	nicméně	k8xC
<g/>
,	,	kIx,
žádná	žádný	k3yNgFnSc1
z	z	k7c2
nich	on	k3xPp3gInPc2
nikdy	nikdy	k6eAd1
nebyla	být	k5eNaImAgFnS
oficiální	oficiální	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Verze	verze	k1gFnSc1
používaná	používaný	k2eAgFnSc1d1
během	během	k7c2
vlády	vláda	k1gFnSc2
Alfonsa	Alfons	k1gMnSc2
XIII	XIII	kA
<g/>
.	.	kIx.
</s>
<s>
Slova	slovo	k1gNnPc4
<g/>
:	:	kIx,
Eduardo	Eduardo	k1gNnSc4
Marquina	Marquin	k2eAgInSc2d1
</s>
<s>
Marcha	Marcha	k1gFnSc1
Real	Real	k1gInSc1
<g/>
,	,	kIx,
Eduardo	Eduardo	k1gNnSc1
Marquina	Marquin	k2eAgNnSc2d1
</s>
<s>
Gloria	Gloria	k1gFnSc1
<g/>
,	,	kIx,
gloria	gloria	k1gFnSc1
<g/>
,	,	kIx,
corona	corona	k1gFnSc1
de	de	k?
la	la	k1gNnSc1
Patria	Patrium	k1gNnSc2
<g/>
,	,	kIx,
</s>
<s>
soberana	soberana	k1gFnSc1
luz	luza	k1gFnPc2
</s>
<s>
que	que	k?
es	es	k1gNnPc2
oro	oro	k?
en	en	k?
tu	tu	k6eAd1
Pendón	Pendón	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Vida	Vida	k?
<g/>
,	,	kIx,
vida	vida	k?
<g/>
,	,	kIx,
futuro	futura	k1gFnSc5
de	de	k?
la	la	k1gNnSc1
Patria	Patrium	k1gNnSc2
<g/>
,	,	kIx,
</s>
<s>
que	que	k?
en	en	k?
tus	tus	k?
ojos	ojos	k6eAd1
es	es	k1gNnSc1
</s>
<s>
abierto	abierta	k1gFnSc5
corazón	corazón	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Púrpura	Púrpura	k1gFnSc1
y	y	k?
oro	oro	k?
<g/>
:	:	kIx,
bandera	bander	k1gMnSc4
inmortal	inmortat	k5eAaPmAgMnS
<g/>
;	;	kIx,
</s>
<s>
en	en	k?
tus	tus	k?
colores	colores	k1gMnSc1
<g/>
,	,	kIx,
juntas	juntas	k1gMnSc1
<g/>
,	,	kIx,
carne	carnout	k5eAaPmIp3nS,k5eAaImIp3nS
y	y	k?
alma	alma	k1gFnSc1
están	están	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Púrpura	Púrpura	k1gFnSc1
y	y	k?
oro	oro	k?
<g/>
:	:	kIx,
querer	querer	k1gMnSc1
y	y	k?
lograr	lograr	k1gMnSc1
<g/>
;	;	kIx,
</s>
<s>
Tú	tú	k0
eres	eres	k6eAd1
<g/>
,	,	kIx,
bandera	bandero	k1gNnPc4
<g/>
,	,	kIx,
el	ela	k1gFnPc2
signo	signo	k6eAd1
del	del	k?
humano	humana	k1gFnSc5
afán	afán	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Gloria	Gloria	k1gFnSc1
<g/>
,	,	kIx,
gloria	gloria	k1gFnSc1
<g/>
,	,	kIx,
corona	corona	k1gFnSc1
de	de	k?
la	la	k1gNnSc1
Patria	Patrium	k1gNnSc2
<g/>
,	,	kIx,
</s>
<s>
soberana	soberana	k1gFnSc1
luz	luza	k1gFnPc2
</s>
<s>
que	que	k?
es	es	k1gNnPc2
oro	oro	k?
en	en	k?
tu	tu	k6eAd1
Pendón	Pendón	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Púrpura	Púrpura	k1gFnSc1
y	y	k?
oro	oro	k?
<g/>
:	:	kIx,
bandera	bander	k1gMnSc4
inmortal	inmortat	k5eAaPmAgMnS
<g/>
;	;	kIx,
</s>
<s>
en	en	k?
tus	tus	k?
colores	colores	k1gMnSc1
<g/>
,	,	kIx,
juntas	juntas	k1gMnSc1
<g/>
,	,	kIx,
carne	carnout	k5eAaPmIp3nS,k5eAaImIp3nS
y	y	k?
alma	alma	k1gFnSc1
están	están	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Sláva	Sláva	k1gFnSc1
<g/>
,	,	kIx,
sláva	sláva	k1gFnSc1
<g/>
,	,	kIx,
veličenstvo	veličenstvo	k1gNnSc1
země	zem	k1gFnSc2
</s>
<s>
svrchovanost	svrchovanost	k1gFnSc4
světla	světlo	k1gNnSc2
</s>
<s>
Vaše	váš	k3xOp2gFnSc1
standarta	standarta	k1gFnSc1
je	být	k5eAaImIp3nS
vyrobena	vyrobit	k5eAaPmNgFnS
ze	z	k7c2
zlata	zlato	k1gNnSc2
</s>
<s>
Život	život	k1gInSc1
<g/>
,	,	kIx,
život	život	k1gInSc1
<g/>
,	,	kIx,
budoucnost	budoucnost	k1gFnSc1
země	zem	k1gFnSc2
</s>
<s>
ve	v	k7c6
Vašich	váš	k3xOp2gNnPc6
očích	oko	k1gNnPc6
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
</s>
<s>
otevřené	otevřený	k2eAgNnSc1d1
srdce	srdce	k1gNnSc1
</s>
<s>
Fialová	Fialová	k1gFnSc1
a	a	k8xC
zlatá	zlatá	k1gFnSc1
<g/>
:	:	kIx,
nesmrtelný	smrtelný	k2eNgInSc1d1
prapor	prapor	k1gInSc1
<g/>
;	;	kIx,
</s>
<s>
ve	v	k7c6
Vašich	váš	k3xOp2gFnPc6
barvách	barva	k1gFnPc6
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
<g/>
,	,	kIx,
smysly	smysl	k1gInPc1
a	a	k8xC
duše	duše	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
fialová	fialový	k2eAgFnSc1d1
a	a	k8xC
zlatá	zlatý	k2eAgFnSc1d1
<g/>
:	:	kIx,
chtění	chtění	k1gNnPc2
a	a	k8xC
docílení	docílení	k1gNnPc2
<g/>
;	;	kIx,
</s>
<s>
Vy	vy	k3xPp2nPc1
jste	být	k5eAaImIp2nP
prapor	prapor	k1gInSc4
<g/>
,	,	kIx,
znak	znak	k1gInSc4
lidského	lidský	k2eAgNnSc2d1
úsilí	úsilí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Sláva	Sláva	k1gFnSc1
<g/>
,	,	kIx,
Sláva	Sláva	k1gFnSc1
<g/>
,	,	kIx,
veličenstvo	veličenstvo	k1gNnSc1
země	zem	k1gFnSc2
</s>
<s>
svrchovanost	svrchovanost	k1gFnSc4
světla	světlo	k1gNnSc2
</s>
<s>
Vaše	váš	k3xOp2gFnSc1
standarta	standarta	k1gFnSc1
je	být	k5eAaImIp3nS
vyrobena	vyrobit	k5eAaPmNgFnS
ze	z	k7c2
zlata	zlato	k1gNnSc2
</s>
<s>
Fialová	Fialová	k1gFnSc1
a	a	k8xC
zlatá	zlatá	k1gFnSc1
<g/>
:	:	kIx,
nesmrtelný	smrtelný	k2eNgInSc1d1
prapor	prapor	k1gInSc1
<g/>
;	;	kIx,
</s>
<s>
ve	v	k7c6
Vašich	váš	k3xOp2gFnPc6
barvách	barva	k1gFnPc6
<g/>
,	,	kIx,
spolu	spolu	k6eAd1
<g/>
,	,	kIx,
smysly	smysl	k1gInPc1
a	a	k8xC
duše	duše	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Verze	verze	k1gFnSc1
používaná	používaný	k2eAgFnSc1d1
během	během	k7c2
diktatury	diktatura	k1gFnSc2
generalissima	generalissimus	k1gMnSc2
Franca	Franca	k?
</s>
<s>
Slova	slovo	k1gNnPc1
<g/>
:	:	kIx,
José	Josá	k1gFnSc2
María	María	k1gMnSc1
Pemán	Pemán	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
¡	¡	k?
<g/>
Viva	Viva	k1gMnSc1
Españ	Españ	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s>
Alzad	Alzad	k6eAd1
los	los	k1gMnSc1
brazos	brazos	k1gMnSc1
<g/>
,	,	kIx,
hijos	hijos	k1gMnSc1
</s>
<s>
del	del	k?
pueblo	puebnout	k5eAaPmAgNnS
españ	españ	k1gFnPc2
<g/>
,	,	kIx,
</s>
<s>
que	que	k?
vuelve	vuelvat	k5eAaPmIp3nS
a	a	k8xC
resurgir	resurgir	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Gloria	Gloria	k1gFnSc1
a	a	k8xC
la	la	k1gNnSc1
Patria	Patrium	k1gNnSc2
que	que	k?
supo	supo	k?
seguir	seguir	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
sobre	sobr	k1gMnSc5
el	ela	k1gFnPc2
azul	azul	k1gInSc4
del	del	k?
mar	mar	k?
el	ela	k1gFnPc2
caminar	caminar	k1gMnSc1
del	del	k?
sol	sol	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
¡	¡	k?
<g/>
Triunfa	Triunf	k1gMnSc4
Españ	Españ	k1gMnSc4
<g/>
!	!	kIx.
</s>
<s>
Los	los	k1gMnSc1
yunques	yunques	k1gMnSc1
y	y	k?
las	laso	k1gNnPc2
ruedas	ruedas	k1gInSc1
</s>
<s>
cantan	cantan	k1gInSc1
al	ala	k1gFnPc2
compás	compás	k6eAd1
</s>
<s>
del	del	k?
himno	himno	k6eAd1
de	de	k?
la	la	k1gNnSc2
fe	fe	k?
<g/>
.	.	kIx.
</s>
<s>
Juntos	Juntos	k1gMnSc1
con	con	k?
ellos	ellos	k1gMnSc1
cantemos	cantemos	k1gMnSc1
de	de	k?
pie	pie	k?
</s>
<s>
la	la	k1gNnSc1
vida	vida	k?
nueva	nuev	k1gMnSc2
y	y	k?
fuerte	fuert	k1gInSc5
de	de	k?
trabajo	trabajo	k1gMnSc1
y	y	k?
paz	paz	k?
<g/>
.	.	kIx.
</s>
<s>
Ať	ať	k9
žije	žít	k5eAaImIp3nS
Španělsko	Španělsko	k1gNnSc1
<g/>
!	!	kIx.
</s>
<s>
Zvedněte	zvednout	k5eAaPmRp2nP
ruce	ruka	k1gFnPc4
<g/>
,	,	kIx,
synové	syn	k1gMnPc1
</s>
<s>
španělského	španělský	k2eAgInSc2d1
lidu	lid	k1gInSc2
<g/>
,	,	kIx,
</s>
<s>
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
znovu	znovu	k6eAd1
povstane	povstat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Sláva	Sláva	k1gFnSc1
zemi	zem	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
věděla	vědět	k5eAaImAgFnS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
následovat	následovat	k5eAaImF
</s>
<s>
přes	přes	k7c4
modré	modrý	k2eAgNnSc4d1
moře	moře	k1gNnSc4
cestu	cesta	k1gFnSc4
slunce	slunce	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Vítězství	vítězství	k1gNnSc1
Španělsku	Španělsko	k1gNnSc3
<g/>
!	!	kIx.
</s>
<s>
kovadliny	kovadlina	k1gFnPc1
a	a	k8xC
děla	dělo	k1gNnPc1
</s>
<s>
zpívají	zpívat	k5eAaImIp3nP
do	do	k7c2
taktu	takt	k1gInSc2
</s>
<s>
hymny	hymna	k1gFnPc1
víry	víra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Spolu	spolu	k6eAd1
s	s	k7c7
nimi	on	k3xPp3gMnPc7
zpívejme	zpívat	k5eAaImRp1nP
vstoje	vstoje	k6eAd1
</s>
<s>
nový	nový	k2eAgInSc4d1
a	a	k8xC
silný	silný	k2eAgInSc4d1
život	život	k1gInSc4
pro	pro	k7c4
práci	práce	k1gFnSc4
a	a	k8xC
mír	mír	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://wiadomosci.gazeta.pl/Wiadomosci/1,80269,4830773.html	http://wiadomosci.gazeta.pl/Wiadomosci/1,80269,4830773.html	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Hymny	hymnus	k1gInPc1
evropských	evropský	k2eAgInPc2d1
států	stát	k1gInPc2
Nezávislé	závislý	k2eNgInPc4d1
státy	stát	k1gInPc4
</s>
<s>
Albánie	Albánie	k1gFnSc1
•	•	k?
Andorra	Andorra	k1gFnSc1
•	•	k?
Arménie	Arménie	k1gFnSc2
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Belgie	Belgie	k1gFnSc1
•	•	k?
Bělorusko	Bělorusko	k1gNnSc1
•	•	k?
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
•	•	k?
Bulharsko	Bulharsko	k1gNnSc1
•	•	k?
Černá	černat	k5eAaImIp3nS
Hora	hora	k1gFnSc1
•	•	k?
Česko	Česko	k1gNnSc1
•	•	k?
Dánsko	Dánsko	k1gNnSc1
•	•	k?
Estonsko	Estonsko	k1gNnSc1
•	•	k?
Finsko	Finsko	k1gNnSc1
•	•	k?
Francie	Francie	k1gFnSc2
•	•	k?
Gruzie	Gruzie	k1gFnSc2
•	•	k?
Chorvatsko	Chorvatsko	k1gNnSc1
•	•	k?
Irsko	Irsko	k1gNnSc4
•	•	k?
Island	Island	k1gInSc1
•	•	k?
Itálie	Itálie	k1gFnSc2
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
Kypr	Kypr	k1gInSc1
•	•	k?
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
•	•	k?
Litva	Litva	k1gFnSc1
•	•	k?
Lotyšsko	Lotyšsko	k1gNnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Lucembursko	Lucembursko	k1gNnSc1
•	•	k?
Maďarsko	Maďarsko	k1gNnSc1
•	•	k?
Malta	Malta	k1gFnSc1
•	•	k?
Moldavsko	Moldavsko	k1gNnSc1
•	•	k?
Monako	Monako	k1gNnSc1
•	•	k?
Německo	Německo	k1gNnSc1
•	•	k?
Nizozemsko	Nizozemsko	k1gNnSc1
•	•	k?
Norsko	Norsko	k1gNnSc1
•	•	k?
Polsko	Polsko	k1gNnSc1
•	•	k?
Portugalsko	Portugalsko	k1gNnSc1
•	•	k?
Rakousko	Rakousko	k1gNnSc1
•	•	k?
Rumunsko	Rumunsko	k1gNnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Řecko	Řecko	k1gNnSc1
•	•	k?
San	San	k1gMnSc1
Marino	Marina	k1gFnSc5
•	•	k?
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
•	•	k?
Slovensko	Slovensko	k1gNnSc1
•	•	k?
Slovinsko	Slovinsko	k1gNnSc1
•	•	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Srbsko	Srbsko	k1gNnSc1
•	•	k?
Španělsko	Španělsko	k1gNnSc1
•	•	k?
Švédsko	Švédsko	k1gNnSc1
•	•	k?
Švýcarsko	Švýcarsko	k1gNnSc1
•	•	k?
Turecko	Turecko	k1gNnSc1
•	•	k?
Ukrajina	Ukrajina	k1gFnSc1
•	•	k?
Vatikán	Vatikán	k1gInSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
koloniea	koloniea	k6eAd1
zámořská	zámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Alandy	Aland	k1gInPc1
(	(	kIx(
<g/>
FIN	Fin	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Faerské	Faerský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
DNK	DNK	kA
<g/>
)	)	kIx)
•	•	k?
Gibraltar	Gibraltar	k1gInSc1
(	(	kIx(
<g/>
GBR	GBR	kA
<g/>
)	)	kIx)
•	•	k?
Grónsko	Grónsko	k1gNnSc4
(	(	kIx(
<g/>
DNK	DNK	kA
<g/>
)	)	kIx)
•	•	k?
Guernsey	Guernsea	k1gFnSc2
(	(	kIx(
<g/>
GBR	GBR	kA
<g/>
)	)	kIx)
•	•	k?
Jersey	Jersea	k1gFnSc2
(	(	kIx(
<g/>
GBR	GBR	kA
<g/>
)	)	kIx)
•	•	k?
Ostrov	ostrov	k1gInSc1
Man	Man	k1gMnSc1
(	(	kIx(
<g/>
GBR	GBR	kA
<g/>
)	)	kIx)
•	•	k?
Republika	republika	k1gFnSc1
srbská	srbský	k2eAgFnSc1d1
(	(	kIx(
<g/>
BIH	BIH	kA
<g/>
)	)	kIx)
Státy	stát	k1gInPc7
bez	bez	k7c2
plného	plný	k2eAgNnSc2d1
uznání	uznání	k1gNnSc2
</s>
<s>
Abcházie	Abcházie	k1gFnSc1
•	•	k?
Arcach	Arcach	k1gInSc1
•	•	k?
Doněcká	doněcký	k2eAgFnSc1d1
lid	lid	k1gInSc1
<g/>
.	.	kIx.
rep	rep	k?
<g/>
.	.	kIx.
•	•	k?
Jižní	jižní	k2eAgFnSc2d1
Osetie	Osetie	k1gFnSc2
•	•	k?
Kosovo	Kosův	k2eAgNnSc1d1
•	•	k?
Luhanská	Luhanský	k2eAgFnSc1d1
lid	lid	k1gInSc1
<g/>
.	.	kIx.
rep	rep	k?
<g/>
.	.	kIx.
•	•	k?
Podněstří	Podněstří	k1gFnSc2
•	•	k?
Severní	severní	k2eAgInSc1d1
Kypr	Kypr	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
|	|	kIx~
Španělsko	Španělsko	k1gNnSc1
</s>
