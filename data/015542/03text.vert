<s>
Notre-Dame-de-Lorette	Notre-Dame-de-Lorette	k5eAaPmIp2nP
(	(	kIx(
<g/>
stanice	stanice	k1gFnSc1
metra	metro	k1gNnSc2
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Notre-Dame-de-Lorette	Notre-Dame-de-Lorette	k5eAaPmIp2nP
</s>
<s>
Nástupiště	nástupiště	k1gNnSc1
ve	v	k7c6
stanici	stanice	k1gFnSc6
</s>
<s>
StátFrancie	StátFrancie	k1gFnSc1
</s>
<s>
MěstoPaříž	MěstoPaříž	k1gFnSc1
<g/>
,	,	kIx,
Issy-les-Moulineaux	Issy-les-Moulineaux	k1gInSc1
<g/>
,	,	kIx,
Saint-Denis	Saint-Denis	k1gInSc1
a	a	k8xC
Aubervilliers	Aubervilliers	k1gInSc1
</s>
<s>
Vznik	vznik	k1gInSc1
<g/>
5	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1910	#num#	k4
</s>
<s>
Linky	linka	k1gFnPc1
<g/>
12	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Zeměpisné	zeměpisný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
<g/>
48	#num#	k4
<g/>
°	°	k?
<g/>
52	#num#	k4
<g/>
′	′	k?
<g/>
34	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
2	#num#	k4
<g/>
°	°	k?
<g/>
20	#num#	k4
<g/>
′	′	k?
<g/>
19	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Notre-Dame-de-Lorette	Notre-Dame-de-Lorette	k5eAaPmIp2nP
je	on	k3xPp3gFnPc4
nepřestupní	přestupní	k2eNgFnPc4d1
stanice	stanice	k1gFnPc4
pařížského	pařížský	k2eAgNnSc2d1
metra	metro	k1gNnSc2
na	na	k7c6
lince	linka	k1gFnSc6
12	#num#	k4
v	v	k7c4
9	#num#	k4
<g/>
.	.	kIx.
obvodu	obvod	k1gInSc2
v	v	k7c6
Paříži	Paříž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
nedaleko	nedaleko	k7c2
kostela	kostel	k1gInSc2
Notre-Dame-de-Lorette	Notre-Dame-de-Lorett	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Stanice	stanice	k1gFnSc1
byla	být	k5eAaImAgFnS
otevřena	otevřen	k2eAgFnSc1d1
5	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1910	#num#	k4
jako	jako	k8xC,k8xS
součást	součást	k1gFnSc4
prvního	první	k4xOgInSc2
úseku	úsek	k1gInSc2
linky	linka	k1gFnSc2
A	A	kA
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
provozovala	provozovat	k5eAaImAgFnS
společnost	společnost	k1gFnSc1
Compagnie	Compagnie	k1gFnSc2
Nord-Sud	Nord-Suda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trať	trať	k1gFnSc1
vedla	vést	k5eAaImAgFnS
od	od	k7c2
Porte	port	k1gInSc5
de	de	k?
Versailles	Versailles	k1gFnSc4
a	a	k8xC
končila	končit	k5eAaImAgFnS
v	v	k7c6
této	tento	k3xDgFnSc6
stanici	stanice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Linka	linka	k1gFnSc1
zde	zde	k6eAd1
končila	končit	k5eAaImAgFnS
do	do	k7c2
8	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1911	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
prodloužena	prodloužit	k5eAaPmNgFnS
do	do	k7c2
stanice	stanice	k1gFnSc2
Pigalle	Pigalle	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
sloučení	sloučení	k1gNnSc6
se	s	k7c7
společností	společnost	k1gFnSc7
Compagnie	Compagnie	k1gFnSc2
du	du	k?
Métropolitain	Métropolitain	k1gMnSc1
de	de	k?
Paris	Paris	k1gMnSc1
obdržela	obdržet	k5eAaPmAgFnS
linka	linka	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1930	#num#	k4
číslo	číslo	k1gNnSc1
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Stanice	stanice	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1984	#num#	k4
renovována	renovovat	k5eAaBmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Nehoda	nehoda	k1gFnSc1
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2000	#num#	k4
došlo	dojít	k5eAaPmAgNnS
ve	v	k7c6
stanici	stanice	k1gFnSc6
k	k	k7c3
nehodě	nehoda	k1gFnSc3
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
si	se	k3xPyFc3
vyžádala	vyžádat	k5eAaPmAgFnS
24	#num#	k4
lehce	lehko	k6eAd1
zraněných	zraněný	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řidič	řidič	k1gMnSc1
vjel	vjet	k5eAaPmAgMnS
do	do	k7c2
stanice	stanice	k1gFnSc2
nepřiměřenou	přiměřený	k2eNgFnSc7d1
rychlostí	rychlost	k1gFnSc7
<g/>
,	,	kIx,
takže	takže	k8xS
došlo	dojít	k5eAaPmAgNnS
u	u	k7c2
vjezdu	vjezd	k1gInSc2
do	do	k7c2
stanice	stanice	k1gFnSc2
k	k	k7c3
vykolejení	vykolejení	k1gNnSc3
jednoho	jeden	k4xCgInSc2
vozu	vůz	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
úsek	úsek	k1gInSc1
trati	trať	k1gFnSc2
má	mít	k5eAaImIp3nS
sklon	sklon	k1gInSc4
4	#num#	k4
<g/>
%	%	kIx~
a	a	k8xC
u	u	k7c2
vjezdu	vjezd	k1gInSc2
do	do	k7c2
stanice	stanice	k1gFnSc2
2,2	2,2	k4
<g/>
%	%	kIx~
<g/>
,	,	kIx,
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
navíc	navíc	k6eAd1
kolej	kolej	k1gFnSc1
do	do	k7c2
oblouku	oblouk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgFnSc1d3
povolená	povolený	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
proto	proto	k8xC
40	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
a	a	k8xC
30	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
v	v	k7c6
zatáčce	zatáčka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
autopilot	autopilot	k1gMnSc1
nebyl	být	k5eNaImAgMnS
v	v	k7c6
provozu	provoz	k1gInSc6
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
vůz	vůz	k1gInSc1
řízen	řídit	k5eAaImNgInS
ručně	ručně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyšetřování	vyšetřování	k1gNnSc1
prokázalo	prokázat	k5eAaPmAgNnS
hlavní	hlavní	k2eAgNnSc1d1
zavinění	zavinění	k1gNnSc1
na	na	k7c6
straně	strana	k1gFnSc6
řidiče	řidič	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
měl	mít	k5eAaImAgMnS
ve	v	k7c6
zvyku	zvyk	k1gInSc6
řídit	řídit	k5eAaImF
výhradně	výhradně	k6eAd1
pomocí	pomocí	k7c2
autopilota	autopilot	k1gMnSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
ztratil	ztratit	k5eAaPmAgMnS
své	svůj	k3xOyFgInPc4
reflexy	reflex	k1gInPc4
a	a	k8xC
dále	daleko	k6eAd2
rovněž	rovněž	k9
prokázalo	prokázat	k5eAaPmAgNnS
zanedbávání	zanedbávání	k1gNnSc1
oprav	oprava	k1gFnPc2
ze	z	k7c2
strany	strana	k1gFnSc2
RATP	RATP	kA
a	a	k8xC
nedostatečnou	dostatečný	k2eNgFnSc4d1
světelnou	světelný	k2eAgFnSc4d1
kontrolu	kontrola	k1gFnSc4
rychlosti	rychlost	k1gFnSc2
na	na	k7c6
nebezpečných	bezpečný	k2eNgNnPc6d1
místech	místo	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
této	tento	k3xDgFnSc2
nehody	nehoda	k1gFnSc2
proto	proto	k8xC
RATP	RATP	kA
vyžaduje	vyžadovat	k5eAaImIp3nS
po	po	k7c6
všech	všecek	k3xTgInPc6
řidičích	řidič	k1gInPc6
pokaždé	pokaždé	k6eAd1
absolvovat	absolvovat	k5eAaPmF
minimálně	minimálně	k6eAd1
jednu	jeden	k4xCgFnSc4
jízdu	jízda	k1gFnSc4
s	s	k7c7
plným	plný	k2eAgNnSc7d1
manuálním	manuální	k2eAgNnSc7d1
řízením	řízení	k1gNnSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
neztratili	ztratit	k5eNaPmAgMnP
zručnost	zručnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
</s>
<s>
Stanice	stanice	k1gFnSc1
byla	být	k5eAaImAgFnS
pojmenována	pojmenovat	k5eAaPmNgFnS
podle	podle	k7c2
kostela	kostel	k1gInSc2
Notre-Dame-de-Lorette	Notre-Dame-de-Lorett	k1gInSc5
(	(	kIx(
<g/>
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
z	z	k7c2
Loreta	Loreto	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
v	v	k7c6
okolí	okolí	k1gNnSc6
</s>
<s>
kostel	kostel	k1gInSc1
Notre-Dame-de-Lorette	Notre-Dame-de-Lorett	k1gInSc5
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Notre-Dame-de-Lorette	Notre-Dame-de-Lorett	k1gInSc5
(	(	kIx(
<g/>
métro	métro	k1gNnSc1
de	de	k?
Paris	Paris	k1gMnSc1
<g/>
)	)	kIx)
na	na	k7c6
francouzské	francouzský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Fotografie	fotografie	k1gFnSc1
vykolejeného	vykolejený	k2eAgInSc2d1
vozu	vůz	k1gInSc2
Archivováno	archivovat	k5eAaBmNgNnS
27	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Zpráva	zpráva	k1gFnSc1
z	z	k7c2
vyšetřování	vyšetřování	k1gNnSc2
nehody	nehoda	k1gFnSc2
(	(	kIx(
<g/>
formát	formát	k1gInSc1
pdf	pdf	k?
<g/>
)	)	kIx)
Archivováno	archivovat	k5eAaBmNgNnS
27	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2005	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Notre-Dame-de-Lorette	Notre-Dame-de-Lorett	k1gInSc5
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
←	←	k?
směr	směr	k1gInSc1
Front	front	k1gInSc1
populaire	populair	k1gMnSc5
</s>
<s>
Metro	metro	k1gNnSc1
v	v	k7c6
Paříži	Paříž	k1gFnSc6
–	–	k?
Linka	linka	k1gFnSc1
12	#num#	k4
</s>
<s>
směr	směr	k1gInSc1
Mairie	Mairie	k1gFnSc2
d	d	k?
<g/>
'	'	kIx"
<g/>
Issy	Issa	k1gMnSc2
→	→	k?
</s>
<s>
Saint-Georges	Saint-Georges	k1gMnSc1
</s>
<s>
Notre-Dame-de-Lorette	Notre-Dame-de-Lorette	k5eAaPmIp2nP
</s>
<s>
Trinité	Trinitý	k2eAgInPc1d1
–	–	k?
d	d	k?
<g/>
'	'	kIx"
<g/>
Estienne	Estienn	k1gInSc5
d	d	k?
<g/>
'	'	kIx"
<g/>
Orves	Orves	k1gMnSc1
</s>
<s>
Front	front	k1gInSc1
populaire	populair	k1gMnSc5
–	–	k?
</s>
<s>
Porte	port	k1gInSc5
de	de	k?
la	la	k1gNnPc2
Chapelle	Chapelle	k1gNnPc2
–	–	k?
</s>
<s>
Marx	Marx	k1gMnSc1
Dormoy	Dormoa	k1gFnSc2
–	–	k?
</s>
<s>
Marcadet	Marcadet	k1gInSc1
–	–	k?
Poissonniers	Poissonniers	k1gInSc1
–	–	k?
</s>
<s>
Jules	Jules	k1gInSc1
Joffrin	Joffrin	k1gInSc1
–	–	k?
</s>
<s>
Lamarck	Lamarck	k1gInSc1
–	–	k?
Caulaincourt	Caulaincourt	k1gInSc1
–	–	k?
</s>
<s>
Abbesses	Abbesses	k1gInSc1
–	–	k?
</s>
<s>
Pigalle	Pigalle	k1gFnSc1
–	–	k?
</s>
<s>
Saint-Georges	Saint-Georges	k1gInSc1
–	–	k?
</s>
<s>
Notre-Dame-de-Lorette	Notre-Dame-de-Lorette	k5eAaPmIp2nP
–	–	k?
</s>
<s>
Trinité	Trinitý	k2eAgInPc1d1
–	–	k?
d	d	k?
<g/>
'	'	kIx"
<g/>
Estienne	Estienn	k1gInSc5
d	d	k?
<g/>
'	'	kIx"
<g/>
Orves	Orves	k1gMnSc1
–	–	k?
</s>
<s>
Saint-Lazare	Saint-Lazar	k1gMnSc5
–	–	k?
</s>
<s>
Madeleine	Madeleine	k1gFnSc1
–	–	k?
</s>
<s>
Concorde	Concorde	k6eAd1
–	–	k?
</s>
<s>
Assemblée	Assemblée	k6eAd1
nationale	nationale	k6eAd1
–	–	k?
</s>
<s>
Solférino	Solférino	k1gNnSc1
–	–	k?
</s>
<s>
Rue	Rue	k?
du	du	k?
Bac	bacit	k5eAaPmRp2nS
–	–	k?
</s>
<s>
Sè	Sè	k1gInSc1
–	–	k?
Babylone	Babylon	k1gInSc5
–	–	k?
</s>
<s>
Rennes	Rennes	k1gInSc1
–	–	k?
</s>
<s>
Notre-Dame-des-Champs	Notre-Dame-des-Champs	k1gInSc1
–	–	k?
</s>
<s>
Montparnasse	Montparnasse	k1gFnSc1
–	–	k?
Bienvenüe	Bienvenüe	k1gInSc1
–	–	k?
</s>
<s>
Falguiè	Falguiè	k1gMnSc5
–	–	k?
</s>
<s>
Pasteur	Pasteur	k1gMnSc1
–	–	k?
</s>
<s>
Volontaires	Volontaires	k1gInSc1
–	–	k?
</s>
<s>
Vaugirard	Vaugirard	k1gInSc1
–	–	k?
</s>
<s>
Convention	Convention	k1gInSc1
–	–	k?
</s>
<s>
Porte	port	k1gInSc5
de	de	k?
Versailles	Versailles	k1gFnSc6
–	–	k?
</s>
<s>
Corentin	Corentin	k2eAgInSc1d1
Celton	Celton	k1gInSc1
–	–	k?
</s>
<s>
Mairie	Mairie	k1gFnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Issy	Issa	k1gMnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
VIAF	VIAF	kA
<g/>
:	:	kIx,
217388216	#num#	k4
</s>
