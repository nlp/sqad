<s>
Simpsonovi	Simpson	k1gMnSc3	Simpson
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
The	The	k1gFnSc1	The
Simpsons	Simpsonsa	k1gFnPc2	Simpsonsa
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
americký	americký	k2eAgInSc4d1	americký
animovaný	animovaný	k2eAgInSc4d1	animovaný
seriál	seriál	k1gInSc4	seriál
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
Mattem	Matt	k1gInSc7	Matt
Groeningem	Groening	k1gInSc7	Groening
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
Jamese	Jamese	k1gFnSc2	Jamese
L.	L.	kA	L.
Brookse	Brookse	k1gFnSc1	Brookse
<g/>
,	,	kIx,	,
Ala	ala	k1gFnSc1	ala
Jeana	Jean	k1gMnSc2	Jean
a	a	k8xC	a
Sama	sám	k3xTgFnSc1	sám
Simona	Simona	k1gFnSc1	Simona
pro	pro	k7c4	pro
televizní	televizní	k2eAgFnSc4d1	televizní
síť	síť	k1gFnSc4	síť
FOX	fox	k1gInSc1	fox
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
satiricky	satiricky	k6eAd1	satiricky
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
životě	život	k1gInSc6	život
americké	americký	k2eAgFnSc2d1	americká
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
představované	představovaný	k2eAgFnSc2d1	představovaná
rodinkou	rodinka	k1gFnSc7	rodinka
Simpsonů	Simpson	k1gInPc2	Simpson
<g/>
,	,	kIx,	,
zahrnující	zahrnující	k2eAgFnSc1d1	zahrnující
Homera	Homera	k1gFnSc1	Homera
<g/>
,	,	kIx,	,
Marge	Marge	k1gFnSc1	Marge
<g/>
,	,	kIx,	,
Barta	Barta	k1gFnSc1	Barta
<g/>
,	,	kIx,	,
Lízu	Líza	k1gFnSc4	Líza
<g/>
,	,	kIx,	,
Maggie	Maggie	k1gFnSc1	Maggie
<g/>
,	,	kIx,	,
dědu	děda	k1gMnSc4	děda
Simpsona	Simpson	k1gMnSc4	Simpson
<g/>
,	,	kIx,	,
psa	pes	k1gMnSc4	pes
Spasitele	spasitel	k1gMnSc4	spasitel
a	a	k8xC	a
kočku	kočka	k1gFnSc4	kočka
Sněhulku	Sněhulka	k1gFnSc4	Sněhulka
<g/>
.	.	kIx.	.
</s>
<s>
Odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
ve	v	k7c6	v
fiktivním	fiktivní	k2eAgNnSc6d1	fiktivní
městě	město	k1gNnSc6	město
kdesi	kdesi	k6eAd1	kdesi
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
Springfieldu	Springfielda	k1gFnSc4	Springfielda
<g/>
.	.	kIx.	.
</s>
<s>
Paroduje	parodovat	k5eAaImIp3nS	parodovat
americkou	americký	k2eAgFnSc4d1	americká
kulturu	kultura	k1gFnSc4	kultura
<g/>
,	,	kIx,	,
společnost	společnost	k1gFnSc4	společnost
<g/>
,	,	kIx,	,
televizi	televize	k1gFnSc4	televize
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
aspektů	aspekt	k1gInPc2	aspekt
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
obecně	obecně	k6eAd1	obecně
<g/>
.	.	kIx.	.
</s>
<s>
Groening	Groening	k1gInSc1	Groening
pojmenoval	pojmenovat	k5eAaPmAgInS	pojmenovat
animovanou	animovaný	k2eAgFnSc4d1	animovaná
rodinku	rodinka	k1gFnSc4	rodinka
po	po	k7c6	po
členech	člen	k1gInPc6	člen
své	svůj	k3xOyFgFnSc2	svůj
vlastní	vlastní	k2eAgFnSc2d1	vlastní
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Bart	Bart	k2eAgInSc1d1	Bart
(	(	kIx(	(
<g/>
anagram	anagram	k1gInSc1	anagram
angl.	angl.	k?	angl.
brat	brat	k1gMnSc1	brat
–	–	k?	–
spratek	spratek	k1gMnSc1	spratek
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgMnS	mít
představovat	představovat	k5eAaImF	představovat
samotného	samotný	k2eAgMnSc4d1	samotný
Matta	Matt	k1gMnSc4	Matt
<g/>
.	.	kIx.	.
</s>
<s>
Simpsonovi	Simpsonův	k2eAgMnPc1d1	Simpsonův
jsou	být	k5eAaImIp3nP	být
nejdéle	dlouho	k6eAd3	dlouho
vysílaným	vysílaný	k2eAgInSc7d1	vysílaný
animovaným	animovaný	k2eAgInSc7d1	animovaný
seriálem	seriál	k1gInSc7	seriál
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
americké	americký	k2eAgFnSc2d1	americká
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
seriál	seriál	k1gInSc1	seriál
má	mít	k5eAaImIp3nS	mít
aktuálně	aktuálně	k6eAd1	aktuálně
618	[number]	k4	618
odvysílaných	odvysílaný	k2eAgInPc2d1	odvysílaný
dílů	díl	k1gInPc2	díl
ve	v	k7c6	v
28	[number]	k4	28
řadách	řada	k1gFnPc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
Simpsonovi	Simpsonův	k2eAgMnPc1d1	Simpsonův
na	na	k7c6	na
televizních	televizní	k2eAgFnPc6d1	televizní
obrazovkách	obrazovka	k1gFnPc6	obrazovka
objevili	objevit	k5eAaPmAgMnP	objevit
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
jako	jako	k9	jako
dvouminutový	dvouminutový	k2eAgInSc1d1	dvouminutový
skeč	skeč	k1gInSc1	skeč
v	v	k7c6	v
The	The	k1gFnPc6	The
Tracey	Tracea	k1gMnSc2	Tracea
Ullman	Ullman	k1gMnSc1	Ullman
Show	show	k1gFnSc1	show
<g/>
.	.	kIx.	.
</s>
<s>
Kresby	kresba	k1gFnSc2	kresba
skečů	skeč	k1gInPc2	skeč
byly	být	k5eAaImAgFnP	být
neumělé	umělý	k2eNgFnPc1d1	neumělá
a	a	k8xC	a
během	během	k7c2	během
tří	tři	k4xCgFnPc2	tři
řad	řada	k1gFnPc2	řada
jich	on	k3xPp3gMnPc2	on
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
48	[number]	k4	48
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
si	se	k3xPyFc3	se
Simpsonovi	Simpsonův	k2eAgMnPc1d1	Simpsonův
vysloužili	vysloužit	k5eAaPmAgMnP	vysloužit
plnohodnotný	plnohodnotný	k2eAgInSc4d1	plnohodnotný
samostatný	samostatný	k2eAgInSc4d1	samostatný
seriál	seriál	k1gInSc4	seriál
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
22	[number]	k4	22
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
první	první	k4xOgInSc1	první
díl	díl	k1gInSc1	díl
se	se	k3xPyFc4	se
vysílal	vysílat	k5eAaImAgInS	vysílat
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Simpsonovi	Simpsonův	k2eAgMnPc1d1	Simpsonův
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
se	s	k7c7	s
prvním	první	k4xOgInSc7	první
seriálem	seriál	k1gInSc7	seriál
televize	televize	k1gFnSc2	televize
Fox	fox	k1gInSc1	fox
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsadil	obsadit	k5eAaPmAgInS	obsadit
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
první	první	k4xOgFnSc6	první
třicítce	třicítka	k1gFnSc6	třicítka
nejsledovanějších	sledovaný	k2eAgInPc2d3	nejsledovanější
amerických	americký	k2eAgInPc2d1	americký
pořadů	pořad	k1gInPc2	pořad
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
už	už	k9	už
svou	svůj	k3xOyFgFnSc7	svůj
první	první	k4xOgFnSc7	první
řadou	řada	k1gFnSc7	řada
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
se	se	k3xPyFc4	se
seriál	seriál	k1gInSc1	seriál
vysílá	vysílat	k5eAaImIp3nS	vysílat
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
vysílacím	vysílací	k2eAgInSc6d1	vysílací
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
měli	mít	k5eAaImAgMnP	mít
Simpsonovi	Simpson	k1gMnSc3	Simpson
premiéru	premiér	k1gMnSc3	premiér
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1992	[number]	k4	1992
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
kanálu	kanál	k1gInSc6	kanál
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
Československé	československý	k2eAgFnSc2d1	Československá
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc4d1	základní
koncept	koncept	k1gInSc4	koncept
Simpsonových	Simpsonová	k1gFnPc2	Simpsonová
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
jejich	jejich	k3xOp3gFnSc4	jejich
tvůrce	tvůrce	k1gMnSc1	tvůrce
Matt	Matt	k1gMnSc1	Matt
Groening	Groening	k1gInSc4	Groening
během	během	k7c2	během
patnácti	patnáct	k4xCc2	patnáct
minut	minuta	k1gFnPc2	minuta
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
seděl	sedět	k5eAaImAgMnS	sedět
v	v	k7c6	v
čekárně	čekárna	k1gFnSc6	čekárna
u	u	k7c2	u
producenta	producent	k1gMnSc2	producent
Jamese	Jamese	k1gFnSc2	Jamese
L.	L.	kA	L.
Brookse	Brooks	k1gMnSc2	Brooks
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
měl	mít	k5eAaImAgInS	mít
dát	dát	k5eAaPmF	dát
námět	námět	k1gInSc4	námět
na	na	k7c4	na
animovaný	animovaný	k2eAgInSc4d1	animovaný
seriál	seriál	k1gInSc4	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Groening	Groening	k1gInSc4	Groening
původně	původně	k6eAd1	původně
uvažoval	uvažovat	k5eAaImAgMnS	uvažovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
seriál	seriál	k1gInSc1	seriál
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gInPc2	jeho
komiksových	komiksový	k2eAgInPc2d1	komiksový
stripů	strip	k1gInPc2	strip
Life	Lif	k1gInSc2	Lif
in	in	k?	in
Hell	Hella	k1gFnPc2	Hella
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
ho	on	k3xPp3gMnSc4	on
napadl	napadnout	k5eAaPmAgInS	napadnout
námět	námět	k1gInSc1	námět
na	na	k7c4	na
Simpsonovy	Simpsonův	k2eAgFnPc4d1	Simpsonova
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
na	na	k7c4	na
nefunkční	funkční	k2eNgFnSc4d1	nefunkční
rodinu	rodina	k1gFnSc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
volbě	volba	k1gFnSc6	volba
jmen	jméno	k1gNnPc2	jméno
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
seriálu	seriál	k1gInSc2	seriál
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgInS	nechat
Matt	Matt	k2eAgInSc1d1	Matt
Groening	Groening	k1gInSc1	Groening
výrazně	výrazně	k6eAd1	výrazně
inspirovat	inspirovat	k5eAaBmF	inspirovat
svou	svůj	k3xOyFgFnSc7	svůj
vlastní	vlastní	k2eAgFnSc7d1	vlastní
rodinou	rodina	k1gFnSc7	rodina
<g/>
:	:	kIx,	:
Homer	Homer	k1gInSc1	Homer
Simpson	Simpson	k1gInSc1	Simpson
–	–	k?	–
Homer	Homer	k1gInSc1	Homer
Groening	Groening	k1gInSc1	Groening
(	(	kIx(	(
<g/>
otec	otec	k1gMnSc1	otec
Matta	Matt	k1gMnSc4	Matt
Groeninga	Groening	k1gMnSc4	Groening
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
synů	syn	k1gMnPc2	syn
<g/>
)	)	kIx)	)
Marge	Marge	k1gFnSc1	Marge
Simpsonová	Simpsonová	k1gFnSc1	Simpsonová
–	–	k?	–
Margaret	Margareta	k1gFnPc2	Margareta
Groeningová	Groeningový	k2eAgFnSc1d1	Groeningový
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
simpsonovská	simpsonovský	k2eAgFnSc1d1	simpsonovská
<g />
.	.	kIx.	.
</s>
<s>
Marge	Marge	k6eAd1	Marge
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
Marjorie	Marjorie	k1gFnSc1	Marjorie
Líza	Líza	k1gFnSc1	Líza
Simpsonová	Simpsonová	k1gFnSc1	Simpsonová
–	–	k?	–
Lisa	Lisa	k1gFnSc1	Lisa
Groeningová	Groeningový	k2eAgFnSc1d1	Groeningový
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gFnSc1	jeho
sestra	sestra	k1gFnSc1	sestra
<g/>
)	)	kIx)	)
Maggie	Maggie	k1gFnSc1	Maggie
Simpsonová	Simpsonový	k2eAgFnSc1d1	Simpsonová
–	–	k?	–
Maggie	Maggie	k1gFnSc1	Maggie
Groeningová	Groeningový	k2eAgFnSc1d1	Groeningový
(	(	kIx(	(
<g/>
další	další	k2eAgFnSc1d1	další
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
sester	sestra	k1gFnPc2	sestra
<g/>
)	)	kIx)	)
Bart	Bart	k1gMnSc1	Bart
Simpson	Simpson	k1gMnSc1	Simpson
–	–	k?	–
syn	syn	k1gMnSc1	syn
(	(	kIx(	(
<g/>
anagram	anagram	k1gInSc1	anagram
slova	slovo	k1gNnSc2	slovo
brat	brat	k1gMnSc1	brat
-	-	kIx~	-
spratek	spratek	k1gMnSc1	spratek
<g/>
)	)	kIx)	)
–	–	k?	–
Bart	Bart	k1gInSc1	Bart
patrně	patrně	k6eAd1	patrně
ztvárňuje	ztvárňovat	k5eAaImIp3nS	ztvárňovat
Groeninga	Groening	k1gMnSc4	Groening
samotného	samotný	k2eAgMnSc4d1	samotný
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc4d1	základní
rodinu	rodina	k1gFnSc4	rodina
Simpsonových	Simpsonová	k1gFnPc2	Simpsonová
tvoří	tvořit	k5eAaImIp3nP	tvořit
rodiče	rodič	k1gMnPc1	rodič
Homer	Homer	k1gMnSc1	Homer
Simpson	Simpson	k1gMnSc1	Simpson
a	a	k8xC	a
Marge	Marge	k1gFnSc1	Marge
Simpsonová	Simpsonová	k1gFnSc1	Simpsonová
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc1	dítě
Bart	Bart	k1gInSc1	Bart
<g/>
,	,	kIx,	,
Líza	Líza	k1gFnSc1	Líza
a	a	k8xC	a
Maggie	Maggie	k1gFnSc1	Maggie
<g/>
,	,	kIx,	,
pes	pes	k1gMnSc1	pes
Spasitel	spasitel	k1gMnSc1	spasitel
a	a	k8xC	a
kočka	kočka	k1gFnSc1	kočka
Sněhulka	Sněhulka	k1gFnSc1	Sněhulka
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
však	však	k9	však
nešťastně	šťastně	k6eNd1	šťastně
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
robot	robot	k1gMnSc1	robot
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Líza	Líza	k1gFnSc1	Líza
si	se	k3xPyFc3	se
našla	najít	k5eAaPmAgFnS	najít
novou	nový	k2eAgFnSc4d1	nová
kočku	kočka	k1gFnSc4	kočka
Sněhulku	Sněhulka	k1gFnSc4	Sněhulka
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc3	on
nešťastně	šťastně	k6eNd1	šťastně
utopila	utopit	k5eAaPmAgFnS	utopit
v	v	k7c6	v
akváriu	akvárium	k1gNnSc6	akvárium
<g/>
,	,	kIx,	,
i	i	k9	i
čtvrtá	čtvrtý	k4xOgFnSc1	čtvrtý
Lízina	Lízin	k2eAgFnSc1d1	Lízina
kočka	kočka	k1gFnSc1	kočka
Coltrane	Coltran	k1gInSc5	Coltran
zemřela	zemřít	k5eAaPmAgFnS	zemřít
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
vyskočila	vyskočit	k5eAaPmAgFnS	vyskočit
z	z	k7c2	z
otevřeného	otevřený	k2eAgNnSc2d1	otevřené
okna	okno	k1gNnSc2	okno
<g/>
,	,	kIx,	,
až	až	k9	až
pátá	pátý	k4xOgFnSc1	pátý
kočka	kočka	k1gFnSc1	kočka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Líza	Líza	k1gFnSc1	Líza
pojmenovala	pojmenovat	k5eAaPmAgFnS	pojmenovat
opět	opět	k6eAd1	opět
Sněhulka	Sněhulka	k1gFnSc1	Sněhulka
II	II	kA	II
<g/>
.	.	kIx.	.
-	-	kIx~	-
ta	ten	k3xDgFnSc1	ten
jí	on	k3xPp3gFnSc3	on
zůstala	zůstat	k5eAaPmAgFnS	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc4d1	další
příbuzenstvo	příbuzenstvo	k1gNnSc4	příbuzenstvo
jsou	být	k5eAaImIp3nP	být
sestry	sestra	k1gFnPc1	sestra
Marge	Marg	k1gFnSc2	Marg
Patty	Patta	k1gFnSc2	Patta
a	a	k8xC	a
Selma	Selma	k1gFnSc1	Selma
Bouvierovy	Bouvierův	k2eAgFnSc2d1	Bouvierův
<g/>
,	,	kIx,	,
Homerův	Homerův	k2eAgMnSc1d1	Homerův
otec	otec	k1gMnSc1	otec
Abe	Abe	k1gMnSc1	Abe
Simpson	Simpson	k1gMnSc1	Simpson
<g/>
,	,	kIx,	,
Homerova	Homerův	k2eAgFnSc1d1	Homerova
matka	matka	k1gFnSc1	matka
Mona	Mona	k1gFnSc1	Mona
Simpsonová	Simpsonová	k1gFnSc1	Simpsonová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
útěku	útěk	k1gInSc6	útěk
před	před	k7c7	před
zákonem	zákon	k1gInSc7	zákon
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
řadě	řada	k1gFnSc6	řada
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc4	jeho
nevlastní	vlastnit	k5eNaImIp3nS	vlastnit
bratr	bratr	k1gMnSc1	bratr
Herbert	Herbert	k1gMnSc1	Herbert
Powell	Powell	k1gMnSc1	Powell
a	a	k8xC	a
nevlastní	vlastní	k2eNgFnSc1d1	nevlastní
sestra	sestra	k1gFnSc1	sestra
Abie	Abi	k1gFnSc2	Abi
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
spatřena	spatřit	k5eAaPmNgFnS	spatřit
v	v	k7c6	v
dílu	díl	k1gInSc6	díl
Obtěžoval	obtěžovat	k5eAaImAgMnS	obtěžovat
jsem	být	k5eAaImIp1nS	být
anglickou	anglický	k2eAgFnSc4d1	anglická
královnu	královna	k1gFnSc4	královna
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
The	The	k1gFnSc2	The
Regina	Regina	k1gFnSc1	Regina
Monologues	Monologues	k1gMnSc1	Monologues
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neobjevily	objevit	k5eNaPmAgFnP	objevit
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
se	se	k3xPyFc4	se
o	o	k7c6	o
nich	on	k3xPp3gMnPc6	on
mluvilo	mluvit	k5eAaImAgNnS	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
takové	takový	k3xDgFnPc4	takový
patří	patřit	k5eAaImIp3nS	patřit
Homerův	Homerův	k2eAgMnSc1d1	Homerův
bratranec	bratranec	k1gMnSc1	bratranec
Frank	Frank	k1gMnSc1	Frank
<g/>
,	,	kIx,	,
z	z	k7c2	z
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stala	stát	k5eAaPmAgFnS	stát
Františka	František	k1gMnSc4	František
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgMnS	dát
k	k	k7c3	k
nějaké	nějaký	k3yIgFnSc3	nějaký
okultní	okultní	k2eAgFnSc3d1	okultní
sektě	sekta	k1gFnSc3	sekta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
říkají	říkat	k5eAaImIp3nP	říkat
"	"	kIx"	"
<g/>
Matka	matka	k1gFnSc1	matka
Shabooboo	Shabooboo	k1gMnSc1	Shabooboo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Místa	místo	k1gNnPc1	místo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgNnPc6	který
se	se	k3xPyFc4	se
seriál	seriál	k1gInSc1	seriál
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
jsou	být	k5eAaImIp3nP	být
různorodá	různorodý	k2eAgFnSc1d1	různorodá
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
děj	děj	k1gInSc1	děj
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
fiktivním	fiktivní	k2eAgNnSc6d1	fiktivní
městě	město	k1gNnSc6	město
Springfield	Springfielda	k1gFnPc2	Springfielda
<g/>
,	,	kIx,	,
bydlišti	bydliště	k1gNnSc3	bydliště
většiny	většina	k1gFnSc2	většina
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
jak	jak	k8xC	jak
říká	říkat	k5eAaImIp3nS	říkat
sám	sám	k3xTgInSc1	sám
Matt	Matt	k2eAgInSc1d1	Matt
Groening	Groening	k1gInSc1	Groening
<g/>
:	:	kIx,	:
I	i	k9	i
když	když	k8xS	když
pocházím	pocházet	k5eAaImIp1nS	pocházet
ze	z	k7c2	z
Springfieldu	Springfield	k1gInSc2	Springfield
v	v	k7c6	v
Illinois	Illinois	k1gFnSc6	Illinois
<g/>
,	,	kIx,	,
název	název	k1gInSc1	název
Springfield	Springfielda	k1gFnPc2	Springfielda
jsem	být	k5eAaImIp1nS	být
zvolil	zvolit	k5eAaPmAgMnS	zvolit
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejčastějších	častý	k2eAgNnPc2d3	nejčastější
jmen	jméno	k1gNnPc2	jméno
měst	město	k1gNnPc2	město
a	a	k8xC	a
osad	osada	k1gFnPc2	osada
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Simpsonovi	Simpson	k1gMnSc6	Simpson
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
Matt	Matt	k2eAgInSc1d1	Matt
Groening	Groening	k1gInSc1	Groening
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
s	s	k7c7	s
celovečerní	celovečerní	k2eAgFnSc7d1	celovečerní
verzí	verze	k1gFnSc7	verze
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
Simpsonovi	Simpson	k1gMnSc6	Simpson
ve	v	k7c6	v
filmu	film	k1gInSc6	film
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Simpsons	Simpsons	k1gInSc1	Simpsons
Movie	Movie	k1gFnSc1	Movie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uvedený	uvedený	k2eAgInSc4d1	uvedený
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
charakter	charakter	k1gInSc4	charakter
dílu	díl	k1gInSc2	díl
"	"	kIx"	"
<g/>
protaženého	protažený	k2eAgInSc2d1	protažený
<g/>
"	"	kIx"	"
do	do	k7c2	do
délky	délka	k1gFnSc2	délka
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
filmu	film	k1gInSc2	film
se	se	k3xPyFc4	se
točí	točit	k5eAaImIp3nS	točit
kolem	kolem	k7c2	kolem
Homera	Homero	k1gNnSc2	Homero
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zaviní	zavinit	k5eAaPmIp3nS	zavinit
největší	veliký	k2eAgFnSc4d3	veliký
katastrofu	katastrofa	k1gFnSc4	katastrofa
okolí	okolí	k1gNnSc2	okolí
Springfieldu	Springfield	k1gInSc2	Springfield
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
pomocí	pomocí	k7c2	pomocí
2D	[number]	k4	2D
technologie	technologie	k1gFnSc2	technologie
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
mnoha	mnoho	k4c2	mnoho
3D	[number]	k4	3D
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k9	i
ve	v	k7c6	v
filmu	film	k1gInSc6	film
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
3D	[number]	k4	3D
scén	scéna	k1gFnPc2	scéna
<g/>
,	,	kIx,	,
evidentně	evidentně	k6eAd1	evidentně
renderovaných	renderovaný	k2eAgFnPc2d1	renderovaná
pomocí	pomoc	k1gFnPc2	pomoc
počítače	počítač	k1gInSc2	počítač
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
kopií	kopie	k1gFnPc2	kopie
filmu	film	k1gInSc2	film
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
ČR	ČR	kA	ČR
český	český	k2eAgInSc1d1	český
dabing	dabing	k1gInSc4	dabing
<g/>
,	,	kIx,	,
stejný	stejný	k2eAgInSc4d1	stejný
jako	jako	k9	jako
dabing	dabing	k1gInSc4	dabing
seriálu	seriál	k1gInSc2	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
pozornost	pozornost	k1gFnSc4	pozornost
stojí	stát	k5eAaImIp3nS	stát
i	i	k9	i
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
česky	česky	k6eAd1	česky
dabovaná	dabovaný	k2eAgFnSc1d1	dabovaná
verze	verze	k1gFnSc1	verze
filmu	film	k1gInSc2	film
unikla	uniknout	k5eAaPmAgFnS	uniknout
na	na	k7c4	na
peer-to-peer	peeroeer	k1gInSc4	peer-to-peer
sítě	síť	k1gFnSc2	síť
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
verze	verze	k1gFnSc1	verze
anglická	anglický	k2eAgFnSc1d1	anglická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
měli	mít	k5eAaImAgMnP	mít
Simpsonovi	Simpson	k1gMnSc3	Simpson
premiéru	premiér	k1gMnSc3	premiér
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
kanálu	kanál	k1gInSc6	kanál
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
vysílala	vysílat	k5eAaImAgFnS	vysílat
Simpsonovy	Simpsonův	k2eAgInPc4d1	Simpsonův
takřka	takřka	k6eAd1	takřka
nepřetržitě	přetržitě	k6eNd1	přetržitě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ještě	ještě	k9	ještě
seriál	seriál	k1gInSc1	seriál
vysílala	vysílat	k5eAaImAgFnS	vysílat
<g/>
,	,	kIx,	,
však	však	k9	však
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
měnila	měnit	k5eAaImAgFnS	měnit
vysílací	vysílací	k2eAgInPc4d1	vysílací
časy	čas	k1gInPc4	čas
i	i	k8xC	i
kanál	kanál	k1gInSc4	kanál
-	-	kIx~	-
Simpsonovi	Simpsonův	k2eAgMnPc1d1	Simpsonův
se	se	k3xPyFc4	se
stěhovali	stěhovat	k5eAaImAgMnP	stěhovat
z	z	k7c2	z
brzkých	brzký	k2eAgInPc2d1	brzký
odpoledních	odpolední	k2eAgInPc2d1	odpolední
do	do	k7c2	do
večerních	večerní	k2eAgFnPc2d1	večerní
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
opačně	opačně	k6eAd1	opačně
a	a	k8xC	a
také	také	k9	také
z	z	k7c2	z
ČT	ČT	kA	ČT
1	[number]	k4	1
na	na	k7c4	na
ČT	ČT	kA	ČT
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
na	na	k7c4	na
seriál	seriál	k1gInSc4	seriál
zakoupila	zakoupit	k5eAaPmAgNnP	zakoupit
vysílací	vysílací	k2eAgNnPc1d1	vysílací
práva	právo	k1gNnPc1	právo
FTV	FTV	kA	FTV
Prima	prima	k6eAd1	prima
a	a	k8xC	a
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
začala	začít	k5eAaPmAgFnS	začít
nepřetržitě	přetržitě	k6eNd1	přetržitě
reprízovat	reprízovat	k5eAaImF	reprízovat
všechny	všechen	k3xTgFnPc4	všechen
řady	řada	k1gFnPc4	řada
na	na	k7c6	na
kanálu	kanál	k1gInSc6	kanál
Prima	prima	k2eAgFnPc2d1	prima
Cool	Coola	k1gFnPc2	Coola
<g/>
.	.	kIx.	.
</s>
<s>
Denně	denně	k6eAd1	denně
se	se	k3xPyFc4	se
vysílaly	vysílat	k5eAaImAgInP	vysílat
vždy	vždy	k6eAd1	vždy
tři	tři	k4xCgInPc4	tři
díly	díl	k1gInPc4	díl
pravidelně	pravidelně	k6eAd1	pravidelně
od	od	k7c2	od
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
50	[number]	k4	50
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
od	od	k7c2	od
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2012	[number]	k4	2012
vždy	vždy	k6eAd1	vždy
o	o	k7c6	o
víkendu	víkend	k1gInSc6	víkend
také	také	k9	také
tři	tři	k4xCgInPc4	tři
díly	díl	k1gInPc4	díl
denně	denně	k6eAd1	denně
<g/>
,	,	kIx,	,
<g/>
od	od	k7c2	od
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
30	[number]	k4	30
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
vysílají	vysílat	k5eAaImIp3nP	vysílat
4	[number]	k4	4
díly	díl	k1gInPc4	díl
v	v	k7c6	v
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c6	o
víkendu	víkend	k1gInSc6	víkend
v	v	k7c6	v
17	[number]	k4	17
<g/>
:	:	kIx,	:
<g/>
55	[number]	k4	55
<g/>
.	.	kIx.	.
</s>
<s>
FTV	FTV	kA	FTV
Prima	prima	k1gFnSc1	prima
také	také	k9	také
již	již	k6eAd1	již
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
měsících	měsíc	k1gInPc6	měsíc
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
začala	začít	k5eAaPmAgFnS	začít
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
výrobou	výroba	k1gFnSc7	výroba
dabingu	dabing	k1gInSc2	dabing
pro	pro	k7c4	pro
nejnovější	nový	k2eAgInSc4d3	nejnovější
<g/>
,	,	kIx,	,
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
dosud	dosud	k6eAd1	dosud
neodvysílanou	odvysílaný	k2eNgFnSc4d1	neodvysílaná
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
řadu	řad	k1gInSc2	řad
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
měla	mít	k5eAaImAgNnP	mít
zakoupená	zakoupený	k2eAgNnPc1d1	zakoupené
vysílací	vysílací	k2eAgNnPc1d1	vysílací
práva	právo	k1gNnPc1	právo
<g/>
.	.	kIx.	.
</s>
<s>
Každým	každý	k3xTgInSc7	každý
dalším	další	k2eAgInSc7d1	další
rokem	rok	k1gInSc7	rok
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
na	na	k7c6	na
kanálu	kanál	k1gInSc6	kanál
Prima	prima	k2eAgFnPc2d1	prima
Cool	Coola	k1gFnPc2	Coola
vždy	vždy	k6eAd1	vždy
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
započne	započnout	k5eAaPmIp3nS	započnout
s	s	k7c7	s
vysíláním	vysílání	k1gNnSc7	vysílání
další	další	k2eAgFnSc2d1	další
nové	nový	k2eAgFnSc2d1	nová
navazující	navazující	k2eAgFnSc2d1	navazující
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
v	v	k7c6	v
USA	USA	kA	USA
ještě	ještě	k6eAd1	ještě
ne	ne	k9	ne
kompletně	kompletně	k6eAd1	kompletně
odvysílané	odvysílaný	k2eAgNnSc1d1	odvysílané
<g/>
,	,	kIx,	,
řadě	řada	k1gFnSc3	řada
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
se	se	k3xPyFc4	se
tak	tak	k9	tak
Česko	Česko	k1gNnSc1	Česko
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c7	mezi
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc2	první
země	zem	k1gFnSc2	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
Simpsonovi	Simpsonův	k2eAgMnPc1d1	Simpsonův
odvysíláni	odvysílat	k5eAaPmNgMnP	odvysílat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
vlastní	vlastní	k2eAgFnSc6d1	vlastní
jazykové	jazykový	k2eAgFnSc6d1	jazyková
mutaci	mutace	k1gFnSc6	mutace
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
skončila	skončit	k5eAaPmAgFnS	skončit
s	s	k7c7	s
premiérovým	premiérový	k2eAgNnSc7d1	premiérové
vysíláním	vysílání	k1gNnSc7	vysílání
Simpsonových	Simpsonová	k1gFnPc2	Simpsonová
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dokončila	dokončit	k5eAaPmAgFnS	dokončit
20	[number]	k4	20
<g/>
.	.	kIx.	.
řadu	řad	k1gInSc2	řad
a	a	k8xC	a
reprízovala	reprízovat	k5eAaImAgFnS	reprízovat
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
řadu	řad	k1gInSc2	řad
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yRgFnSc6	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ještě	ještě	k9	ještě
vlastnila	vlastnit	k5eAaImAgNnP	vlastnit
vysílací	vysílací	k2eAgNnPc1d1	vysílací
práva	právo	k1gNnPc1	právo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
20.10	[number]	k4	20.10
<g/>
.2015	.2015	k4	.2015
vysílá	vysílat	k5eAaImIp3nS	vysílat
FTV	FTV	kA	FTV
Prima	prima	k1gFnSc1	prima
prvních	první	k4xOgNnPc6	první
20	[number]	k4	20
řad	řada	k1gFnPc2	řada
(	(	kIx(	(
<g/>
až	až	k9	až
do	do	k7c2	do
epizody	epizoda	k1gFnSc2	epizoda
Hysterka	hysterka	k1gFnSc1	hysterka
Líza	Líza	k1gFnSc1	Líza
-	-	kIx~	-
poslední	poslední	k2eAgInSc1d1	poslední
díl	díl	k1gInSc1	díl
ve	v	k7c6	v
standardním	standardní	k2eAgInSc6d1	standardní
formátu	formát	k1gInSc6	formát
<g/>
)	)	kIx)	)
v	v	k7c6	v
širokoúhlé	širokoúhlý	k2eAgFnSc6d1	širokoúhlá
HD	HD	kA	HD
kvalitě	kvalita	k1gFnSc3	kvalita
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
21	[number]	k4	21
<g/>
.	.	kIx.	.
řady	řada	k1gFnSc2	řada
se	se	k3xPyFc4	se
na	na	k7c6	na
překládání	překládání	k1gNnSc6	překládání
seriálu	seriál	k1gInSc2	seriál
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
překladatelů	překladatel	k1gMnPc2	překladatel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
jej	on	k3xPp3gMnSc4	on
překládá	překládat	k5eAaImIp3nS	překládat
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Kostiha	Kostiha	k1gMnSc1	Kostiha
<g/>
.	.	kIx.	.
</s>
<s>
Režisérem	režisér	k1gMnSc7	režisér
českého	český	k2eAgNnSc2d1	české
znění	znění	k1gNnSc2	znění
Simpsonových	Simpsonová	k1gFnPc2	Simpsonová
je	být	k5eAaImIp3nS	být
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
dabingovou	dabingový	k2eAgFnSc4d1	dabingová
režii	režie	k1gFnSc4	režie
Simpsonů	Simpson	k1gInPc2	Simpson
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2000	[number]	k4	2000
a	a	k8xC	a
2013	[number]	k4	2013
cenu	cena	k1gFnSc4	cena
Františka	František	k1gMnSc4	František
Filipovského	Filipovský	k1gMnSc4	Filipovský
<g/>
.	.	kIx.	.
</s>
<s>
Slovenský	slovenský	k2eAgInSc1d1	slovenský
dabing	dabing	k1gInSc1	dabing
Simpsonových	Simpsonová	k1gFnPc2	Simpsonová
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
televizní	televizní	k2eAgFnSc1d1	televizní
stanice	stanice	k1gFnSc1	stanice
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
užívají	užívat	k5eAaImIp3nP	užívat
českou	český	k2eAgFnSc4d1	Česká
jazykovou	jazykový	k2eAgFnSc4d1	jazyková
verzi	verze	k1gFnSc4	verze
<g/>
.	.	kIx.	.
</s>
<s>
Simpsonovi	Simpsonův	k2eAgMnPc1d1	Simpsonův
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
své	svůj	k3xOyFgFnSc2	svůj
televizní	televizní	k2eAgFnSc2d1	televizní
existence	existence	k1gFnSc2	existence
získali	získat	k5eAaPmAgMnP	získat
řadu	řada	k1gFnSc4	řada
ocenění	ocenění	k1gNnSc2	ocenění
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
například	například	k6eAd1	například
Bart	Bart	k2eAgInSc1d1	Bart
Simpson	Simpson	k1gInSc1	Simpson
byl	být	k5eAaImAgInS	být
časopisem	časopis	k1gInSc7	časopis
Time	Time	k1gFnPc2	Time
vedle	vedle	k7c2	vedle
Franka	Frank	k1gMnSc2	Frank
Sinatry	Sinatr	k1gMnPc4	Sinatr
či	či	k8xC	či
Stevena	Steven	k2eAgFnSc1d1	Stevena
Spielberga	Spielberga	k1gFnSc1	Spielberga
umístěn	umístit	k5eAaPmNgInS	umístit
do	do	k7c2	do
žebříčku	žebříček	k1gInSc2	žebříček
padesáti	padesát	k4xCc2	padesát
nejvlivnějších	vlivný	k2eAgFnPc2d3	nejvlivnější
osobností	osobnost	k1gFnPc2	osobnost
v	v	k7c6	v
zábavním	zábavní	k2eAgInSc6d1	zábavní
průmyslu	průmysl	k1gInSc6	průmysl
(	(	kIx(	(
<g/>
byl	být	k5eAaImAgMnS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
fiktivní	fiktivní	k2eAgFnSc7d1	fiktivní
postavou	postava	k1gFnSc7	postava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Simpsonovi	Simpsonův	k2eAgMnPc1d1	Simpsonův
mají	mít	k5eAaImIp3nP	mít
také	také	k9	také
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
hvězdu	hvězda	k1gFnSc4	hvězda
na	na	k7c6	na
hollywoodském	hollywoodský	k2eAgInSc6d1	hollywoodský
chodníku	chodník	k1gInSc6	chodník
slávy	sláva	k1gFnSc2	sláva
a	a	k8xC	a
také	také	k9	také
překonali	překonat	k5eAaPmAgMnP	překonat
dosavadní	dosavadní	k2eAgInSc4d1	dosavadní
rekord	rekord	k1gInSc4	rekord
seriálu	seriál	k1gInSc2	seriál
Flintstoneovi	Flintstoneus	k1gMnSc6	Flintstoneus
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
dílů	díl	k1gInPc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
zpomaleně	zpomaleně	k6eAd1	zpomaleně
krokují	krokovat	k5eAaImIp3nP	krokovat
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
snímky	snímek	k1gInPc4	snímek
animace	animace	k1gFnSc2	animace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
nalézt	nalézt	k5eAaPmF	nalézt
nečekané	čekaný	k2eNgInPc4d1	nečekaný
vtípky	vtípek	k1gInPc4	vtípek
od	od	k7c2	od
tvůrců	tvůrce	k1gMnPc2	tvůrce
The	The	k1gFnSc2	The
Simpsons	Simpsons	k1gInSc1	Simpsons
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nápisy	nápis	k1gInPc4	nápis
<g/>
,	,	kIx,	,
tváře	tvář	k1gFnPc4	tvář
postav	postava	k1gFnPc2	postava
známých	známý	k2eAgFnPc2d1	známá
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
mihnou	mihnout	k5eAaPmIp3nP	mihnout
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
,	,	kIx,	,
děj	děj	k1gInSc4	děj
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rozporu	rozpor	k1gInSc6	rozpor
s	s	k7c7	s
logikou	logika	k1gFnSc7	logika
příběhu	příběh	k1gInSc2	příběh
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
i	i	k9	i
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
čeští	český	k2eAgMnPc1d1	český
dabéři	dabér	k1gMnPc1	dabér
občas	občas	k6eAd1	občas
nestihnou	stihnout	k5eNaPmIp3nP	stihnout
přeložit	přeložit	k5eAaPmF	přeložit
některé	některý	k3yIgInPc4	některý
nápisy	nápis	k1gInPc4	nápis
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
či	či	k8xC	či
na	na	k7c6	na
cedulích	cedule	k1gFnPc6	cedule
(	(	kIx(	(
<g/>
např.	např.	kA	např.
protože	protože	k8xS	protože
probíhá	probíhat	k5eAaImIp3nS	probíhat
dialog	dialog	k1gInSc4	dialog
mezi	mezi	k7c7	mezi
zásadními	zásadní	k2eAgFnPc7d1	zásadní
postavami	postava	k1gFnPc7	postava
nebo	nebo	k8xC	nebo
není	být	k5eNaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
zřetelný	zřetelný	k2eAgInSc1d1	zřetelný
kontext	kontext	k1gInSc1	kontext
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
mnohdy	mnohdy	k6eAd1	mnohdy
velmi	velmi	k6eAd1	velmi
vtipné	vtipný	k2eAgNnSc1d1	vtipné
až	až	k8xS	až
sarkastické	sarkastický	k2eAgNnSc1d1	sarkastické
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mnohých	mnohý	k2eAgInPc2d1	mnohý
dílů	díl	k1gInPc2	díl
jsou	být	k5eAaImIp3nP	být
patrné	patrný	k2eAgInPc1d1	patrný
i	i	k8xC	i
osobní	osobní	k2eAgInPc1d1	osobní
názory	názor	k1gInPc1	názor
autorů	autor	k1gMnPc2	autor
na	na	k7c4	na
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
politiku	politika	k1gFnSc4	politika
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
globální	globální	k2eAgInPc4d1	globální
problémy	problém	k1gInPc4	problém
atd.	atd.	kA	atd.
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
úvodní	úvodní	k2eAgFnSc6d1	úvodní
znělce	znělka	k1gFnSc6	znělka
většiny	většina	k1gFnSc2	většina
dílů	díl	k1gInPc2	díl
píše	psát	k5eAaImIp3nS	psát
Bart	Bart	k1gInSc1	Bart
na	na	k7c4	na
tabuli	tabule	k1gFnSc4	tabule
větu	věta	k1gFnSc4	věta
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
dostal	dostat	k5eAaPmAgMnS	dostat
za	za	k7c4	za
trest	trest	k1gInSc4	trest
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
věty	věta	k1gFnSc2	věta
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
pokaždé	pokaždé	k6eAd1	pokaždé
jiný	jiný	k2eAgMnSc1d1	jiný
(	(	kIx(	(
<g/>
např.	např.	kA	např.
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Už	už	k6eAd1	už
nikdy	nikdy	k6eAd1	nikdy
nebudu	být	k5eNaImBp1nS	být
použ	použ	k1gFnSc1	použ
<g/>
.	.	kIx.	.
zkr.	zkr.	kA	zkr.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
až	až	k9	až
na	na	k7c4	na
malé	malý	k2eAgFnPc4d1	malá
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
scéna	scéna	k1gFnSc1	scéna
s	s	k7c7	s
tabulí	tabule	k1gFnSc7	tabule
opakuje	opakovat	k5eAaImIp3nS	opakovat
pouze	pouze	k6eAd1	pouze
asi	asi	k9	asi
ve	v	k7c6	v
3	[number]	k4	3
dílech	díl	k1gInPc6	díl
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Písek	Písek	k1gInSc1	Písek
není	být	k5eNaImIp3nS	být
příchuť	příchuť	k1gFnSc4	příchuť
do	do	k7c2	do
sendviče	sendvič	k1gInSc2	sendvič
[	[	kIx(	[
<g/>
Sandwich	Sandwich	k1gInSc1	Sandwich
→	→	k?	→
Sand	Sand	k1gInSc1	Sand
=	=	kIx~	=
písek	písek	k1gInSc1	písek
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
Nejsem	být	k5eNaImIp1nS	být
drzý	drzý	k2eAgMnSc1d1	drzý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
dílů	díl	k1gInPc2	díl
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
i	i	k9	i
drobný	drobný	k2eAgInSc4d1	drobný
vtípek	vtípek	k1gInSc4	vtípek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rodina	rodina	k1gFnSc1	rodina
usedá	usedat	k5eAaImIp3nS	usedat
na	na	k7c4	na
gauč	gauč	k1gInSc4	gauč
před	před	k7c4	před
televizi	televize	k1gFnSc4	televize
a	a	k8xC	a
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
činnosti	činnost	k1gFnSc2	činnost
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
přihodí	přihodit	k5eAaPmIp3nS	přihodit
drobný	drobný	k2eAgInSc1d1	drobný
problém	problém	k1gInSc1	problém
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
scénka	scénka	k1gFnSc1	scénka
bývá	bývat	k5eAaImIp3nS	bývat
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
díl	díl	k1gInSc4	díl
jiná	jiná	k1gFnSc1	jiná
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
i	i	k9	i
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
opakuje	opakovat	k5eAaImIp3nS	opakovat
vtípek	vtípek	k1gInSc4	vtípek
z	z	k7c2	z
dříve	dříve	k6eAd2	dříve
vysílaného	vysílaný	k2eAgInSc2d1	vysílaný
dílu	díl	k1gInSc2	díl
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Např.	např.	kA	např.
gauč	gauč	k1gInSc1	gauč
je	být	k5eAaImIp3nS	být
pryč	pryč	k6eAd1	pryč
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Zprvu	zprvu	k6eAd1	zprvu
nepříliš	příliš	k6eNd1	příliš
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
a	a	k8xC	a
nápadité	nápaditý	k2eAgFnPc1d1	nápaditá
scény	scéna	k1gFnPc1	scéna
se	se	k3xPyFc4	se
v	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
řadách	řada	k1gFnPc6	řada
změnily	změnit	k5eAaPmAgFnP	změnit
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
delších	dlouhý	k2eAgFnPc2d2	delší
krátkých	krátký	k2eAgFnPc2d1	krátká
epizod	epizoda	k1gFnPc2	epizoda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
tvůrcům	tvůrce	k1gMnPc3	tvůrce
seriálu	seriál	k1gInSc2	seriál
kompenzovat	kompenzovat	k5eAaBmF	kompenzovat
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
každého	každý	k3xTgInSc2	každý
dílu	díl	k1gInSc2	díl
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
díl	díl	k1gInSc4	díl
krátký	krátký	k2eAgInSc4d1	krátký
<g/>
,	,	kIx,	,
zvolí	zvolit	k5eAaPmIp3nS	zvolit
se	se	k3xPyFc4	se
delší	dlouhý	k2eAgInSc1d2	delší
couch	couch	k1gInSc1	couch
gag	gag	k1gInSc1	gag
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
dílech	díl	k1gInPc6	díl
rozdělených	rozdělený	k2eAgInPc6d1	rozdělený
na	na	k7c4	na
3	[number]	k4	3
nezávislé	závislý	k2eNgInPc1d1	nezávislý
příběhy	příběh	k1gInPc1	příběh
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
parodie	parodie	k1gFnPc1	parodie
na	na	k7c4	na
nejrůznější	různý	k2eAgInPc4d3	nejrůznější
horory	horor	k1gInPc4	horor
(	(	kIx(	(
<g/>
Noční	noční	k2eAgFnSc1d1	noční
můra	můra	k1gFnSc1	můra
v	v	k7c6	v
Elm	Elm	k1gFnSc6	Elm
Street	Streeta	k1gFnPc2	Streeta
<g/>
,	,	kIx,	,
Osvícení	osvícení	k1gNnSc2	osvícení
<g/>
,	,	kIx,	,
Stmívání	stmívání	k1gNnSc2	stmívání
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
zemře	zemřít	k5eAaPmIp3nS	zemřít
několik	několik	k4yIc4	několik
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
i	i	k9	i
nové	nový	k2eAgFnPc1d1	nová
postavy	postava	k1gFnPc1	postava
(	(	kIx(	(
<g/>
např.	např.	kA	např.
mimozemšťané	mimozemšťan	k1gMnPc1	mimozemšťan
Kang	Kang	k1gMnSc1	Kang
a	a	k8xC	a
Kodos	Kodos	k1gMnSc1	Kodos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díly	díl	k1gInPc1	díl
obvykle	obvykle	k6eAd1	obvykle
nemají	mít	k5eNaImIp3nP	mít
s	s	k7c7	s
celkovým	celkový	k2eAgInSc7d1	celkový
dějem	děj	k1gInSc7	děj
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
(	(	kIx(	(
<g/>
kvůli	kvůli	k7c3	kvůli
poměrně	poměrně	k6eAd1	poměrně
časté	častý	k2eAgFnSc3d1	častá
smrti	smrt	k1gFnSc3	smrt
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
speciálním	speciální	k2eAgNnPc3d1	speciální
čarodějnickým	čarodějnický	k2eAgNnPc3d1	čarodějnické
dílům	dílo	k1gNnPc3	dílo
většinou	většinou	k6eAd1	většinou
autoři	autor	k1gMnPc1	autor
přidávají	přidávat	k5eAaImIp3nP	přidávat
také	také	k9	také
"	"	kIx"	"
<g/>
hrůzyplné	hrůzyplný	k2eAgInPc1d1	hrůzyplný
titulky	titulek	k1gInPc1	titulek
<g/>
"	"	kIx"	"
doplněné	doplněný	k2eAgNnSc1d1	doplněné
humorným	humorný	k2eAgInSc7d1	humorný
českým	český	k2eAgInSc7d1	český
dabingem	dabing	k1gInSc7	dabing
např.	např.	kA	např.
"	"	kIx"	"
<g/>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
znění	znění	k1gNnSc6	znění
hrůzu	hrůza	k1gFnSc4	hrůza
naháněli	nahánět	k5eAaImAgMnP	nahánět
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
bezhlavě	bezhlavě	k6eAd1	bezhlavě
účinkovali	účinkovat	k5eAaImAgMnP	účinkovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
Speciálního	speciální	k2eAgInSc2d1	speciální
čarodějnického	čarodějnický	k2eAgInSc2d1	čarodějnický
dílu	díl	k1gInSc2	díl
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
logo	logo	k1gNnSc1	logo
Gracie	Gracie	k1gFnSc2	Gracie
Film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
ozve	ozvat	k5eAaPmIp3nS	ozvat
výkřik	výkřik	k1gInSc1	výkřik
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
řady	řada	k1gFnSc2	řada
je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
novinka	novinka	k1gFnSc1	novinka
<g/>
:	:	kIx,	:
ve	v	k7c6	v
znělce	znělka	k1gFnSc6	znělka
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
billboard	billboard	k1gInSc1	billboard
s	s	k7c7	s
vtipným	vtipný	k2eAgInSc7d1	vtipný
textem	text	k1gInSc7	text
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
doplněn	doplnit	k5eAaPmNgInS	doplnit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
inovace	inovace	k1gFnSc1	inovace
úvodu	úvod	k1gInSc2	úvod
seriálu	seriál	k1gInSc2	seriál
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
díky	díky	k7c3	díky
neustálým	neustálý	k2eAgFnPc3d1	neustálá
inovacím	inovace	k1gFnPc3	inovace
animace	animace	k1gFnSc2	animace
již	již	k6eAd1	již
příliš	příliš	k6eAd1	příliš
odlišný	odlišný	k2eAgInSc1d1	odlišný
od	od	k7c2	od
hlavního	hlavní	k2eAgInSc2d1	hlavní
pořadu	pořad	k1gInSc2	pořad
<g/>
.	.	kIx.	.
</s>
<s>
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
oh	oh	k0	oh
<g/>
!	!	kIx.	!
</s>
<s>
je	být	k5eAaImIp3nS	být
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejčastěji	často	k6eAd3	často
používaných	používaný	k2eAgFnPc2d1	používaná
hlášek	hláška	k1gFnPc2	hláška
Homera	Homera	k1gFnSc1	Homera
Simpsona	Simpsona	k1gFnSc1	Simpsona
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
dabingovém	dabingový	k2eAgInSc6d1	dabingový
překladu	překlad	k1gInSc6	překlad
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
seriálu	seriál	k1gInSc2	seriál
<g/>
,	,	kIx,	,
bývává	bývávat	k5eAaImIp3nS	bývávat
tato	tento	k3xDgFnSc1	tento
fráze	fráze	k1gFnSc1	fráze
většinou	většinou	k6eAd1	většinou
překládána	překládán	k2eAgFnSc1d1	překládána
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Sakra	sakra	k0	sakra
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Kruci	Kruci	k?	Kruci
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Ne	ne	k9	ne
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Au	au	k0	au
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Ou	ou	k0	ou
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
Poprvé	poprvé	k6eAd1	poprvé
ji	on	k3xPp3gFnSc4	on
Homer	Homer	k1gMnSc1	Homer
použil	použít	k5eAaPmAgMnS	použít
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
v	v	k7c6	v
dílu	díl	k1gInSc6	díl
"	"	kIx"	"
<g/>
Boxovací	boxovací	k2eAgInSc4d1	boxovací
pytel	pytel	k1gInSc4	pytel
<g/>
"	"	kIx"	"
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
řadě	řada	k1gFnSc6	řada
Simpsonovských	Simpsonovský	k2eAgInPc2d1	Simpsonovský
skečů	skeč	k1gInPc2	skeč
v	v	k7c6	v
The	The	k1gFnPc6	The
Tracey	Tracea	k1gMnSc2	Tracea
Ullman	Ullman	k1gMnSc1	Ullman
Show	show	k1gFnSc1	show
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hláška	hláška	k1gFnSc1	hláška
si	se	k3xPyFc3	se
získala	získat	k5eAaPmAgFnS	získat
takovou	takový	k3xDgFnSc4	takový
popularitu	popularita	k1gFnSc4	popularita
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
byla	být	k5eAaImAgFnS	být
zařazena	zařadit	k5eAaPmNgFnS	zařadit
vedle	vedle	k7c2	vedle
ostatních	ostatní	k2eAgNnPc2d1	ostatní
anglických	anglický	k2eAgNnPc2d1	anglické
slov	slovo	k1gNnPc2	slovo
do	do	k7c2	do
Oficiálního	oficiální	k2eAgInSc2d1	oficiální
oxfordského	oxfordský	k2eAgInSc2d1	oxfordský
anglického	anglický	k2eAgInSc2d1	anglický
slovníku	slovník	k1gInSc2	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Simpsonovi	Simpsonův	k2eAgMnPc1d1	Simpsonův
jako	jako	k9	jako
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
značek	značka	k1gFnPc2	značka
pronikli	proniknout	k5eAaPmAgMnP	proniknout
také	také	k9	také
do	do	k7c2	do
světa	svět	k1gInSc2	svět
známé	známý	k2eAgFnSc2d1	známá
stavebnice	stavebnice	k1gFnSc2	stavebnice
LEGO	lego	k1gNnSc1	lego
a	a	k8xC	a
fanoušci	fanoušek	k1gMnPc1	fanoušek
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
mohou	moct	k5eAaImIp3nP	moct
známou	známý	k2eAgFnSc4d1	známá
rodinku	rodinka	k1gFnSc4	rodinka
pořídit	pořídit	k5eAaPmF	pořídit
i	i	k9	i
s	s	k7c7	s
jejich	jejich	k3xOp3gInSc7	jejich
domem	dům	k1gInSc7	dům
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
25	[number]	k4	25
<g/>
.	.	kIx.	.
řadě	řada	k1gFnSc6	řada
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
díl	díl	k1gInSc1	díl
Život	život	k1gInSc1	život
v	v	k7c6	v
kostce	kostka	k1gFnSc6	kostka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
prolíná	prolínat	k5eAaImIp3nS	prolínat
klasická	klasický	k2eAgFnSc1d1	klasická
"	"	kIx"	"
<g/>
groeningovská	groeningovská	k1gFnSc1	groeningovská
<g/>
"	"	kIx"	"
animace	animace	k1gFnSc1	animace
s	s	k7c7	s
Lego	lego	k1gNnSc4	lego
animací	animace	k1gFnPc2	animace
<g/>
.	.	kIx.	.
</s>
<s>
ALBERTI	ALBERTI	kA	ALBERTI
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Leaving	Leaving	k1gInSc1	Leaving
Springfield	Springfield	k1gInSc1	Springfield
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Simpsons	Simpsons	k1gInSc1	Simpsons
and	and	k?	and
the	the	k?	the
Possibility	Possibilita	k1gFnSc2	Possibilita
of	of	k?	of
Oppositional	Oppositional	k1gMnSc5	Oppositional
Culture	Cultur	k1gMnSc5	Cultur
<g/>
.	.	kIx.	.
</s>
<s>
Detroit	Detroit	k1gInSc1	Detroit
:	:	kIx,	:
Wayne	Wayn	k1gInSc5	Wayn
State	status	k1gInSc5	status
University	universita	k1gFnPc1	universita
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8143	[number]	k4	8143
<g/>
-	-	kIx~	-
<g/>
2849	[number]	k4	2849
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
CARTWRIGHT	CARTWRIGHT	kA	CARTWRIGHT
<g/>
,	,	kIx,	,
Nancy	Nancy	k1gNnSc2	Nancy
<g/>
.	.	kIx.	.
</s>
<s>
My	my	k3xPp1nPc1	my
Life	Life	k1gNnPc1	Life
as	as	k1gNnPc2	as
a	a	k8xC	a
10	[number]	k4	10
<g/>
-Year-Old	-Year-Old	k1gMnSc1	-Year-Old
Boy	boy	k1gMnSc1	boy
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
City	city	k1gNnSc1	city
:	:	kIx,	:
Hyperion	Hyperion	k1gInSc1	Hyperion
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
7868	[number]	k4	7868
<g/>
-	-	kIx~	-
<g/>
8600	[number]	k4	8600
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
RICHMOND	RICHMOND	kA	RICHMOND
<g/>
,	,	kIx,	,
Ray	Ray	k1gMnSc2	Ray
<g/>
,	,	kIx,	,
Antonia	Antonio	k1gMnSc2	Antonio
Coffman	Coffman	k1gMnSc1	Coffman
The	The	k1gMnSc1	The
Simpsons	Simpsonsa	k1gFnPc2	Simpsonsa
<g/>
:	:	kIx,	:
A	a	k9	a
Complete	Comple	k1gNnSc2	Comple
Guide	Guid	k1gInSc5	Guid
to	ten	k3xDgNnSc1	ten
Our	Our	k1gMnSc1	Our
Favorite	favorit	k1gInSc5	favorit
Family	Famil	k1gInPc4	Famil
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
City	city	k1gNnSc1	city
:	:	kIx,	:
HarperCollins	HarperCollins	k1gInSc1	HarperCollins
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
638898	[number]	k4	638898
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
TURNER	turner	k1gMnSc1	turner
<g/>
,	,	kIx,	,
Chris	Chris	k1gFnSc1	Chris
<g/>
.	.	kIx.	.
</s>
<s>
Planet	planeta	k1gFnPc2	planeta
Simpson	Simpson	k1gMnSc1	Simpson
<g/>
:	:	kIx,	:
How	How	k1gMnSc1	How
a	a	k8xC	a
Cartoon	Cartoon	k1gMnSc1	Cartoon
Masterpiece	Masterpiece	k1gMnSc1	Masterpiece
Documented	Documented	k1gMnSc1	Documented
an	an	k?	an
Era	Era	k1gMnSc1	Era
and	and	k?	and
Defined	Defined	k1gInSc1	Defined
a	a	k8xC	a
Generation	Generation	k1gInSc1	Generation
<g/>
.	.	kIx.	.
</s>
<s>
Toronto	Toronto	k1gNnSc1	Toronto
:	:	kIx,	:
Random	Random	k1gInSc1	Random
House	house	k1gNnSc1	house
Canada	Canada	k1gFnSc1	Canada
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
679	[number]	k4	679
<g/>
-	-	kIx~	-
<g/>
31318	[number]	k4	31318
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
