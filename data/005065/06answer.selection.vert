<s>
Slovem	slovem	k6eAd1	slovem
šifra	šifra	k1gFnSc1	šifra
nebo	nebo	k8xC	nebo
šifrování	šifrování	k1gNnSc1	šifrování
budeme	být	k5eAaImBp1nP	být
označovat	označovat	k5eAaImF	označovat
kryptografický	kryptografický	k2eAgInSc4d1	kryptografický
algoritmus	algoritmus	k1gInSc4	algoritmus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
převádí	převádět	k5eAaImIp3nS	převádět
čitelnou	čitelný	k2eAgFnSc4d1	čitelná
zprávu	zpráva	k1gFnSc4	zpráva
neboli	neboli	k8xC	neboli
prostý	prostý	k2eAgInSc4d1	prostý
text	text	k1gInSc4	text
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
nečitelnou	čitelný	k2eNgFnSc4d1	nečitelná
podobu	podoba	k1gFnSc4	podoba
neboli	neboli	k8xC	neboli
šifrový	šifrový	k2eAgInSc4d1	šifrový
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
