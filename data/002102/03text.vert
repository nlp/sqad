<p>
<s>
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Brünn	Brünn	k1gNnSc1	Brünn
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Bruna	Bruna	k1gMnSc1	Bruna
<g/>
,	,	kIx,	,
maďarsky	maďarsky	k6eAd1	maďarsky
Berén	Berén	k1gInSc1	Berén
<g/>
,	,	kIx,	,
v	v	k7c6	v
jidiš	jidiš	k1gNnSc6	jidiš
ב	ב	k?	ב
Brin	Brin	k1gInSc1	Brin
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
statutární	statutární	k2eAgNnSc1d1	statutární
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
i	i	k8xC	i
rozlohou	rozloha	k1gFnSc7	rozloha
druhé	druhý	k4xOgNnSc4	druhý
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
bývalé	bývalý	k2eAgNnSc4d1	bývalé
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnSc6	jehož
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
tvoří	tvořit	k5eAaImIp3nP	tvořit
samostatný	samostatný	k2eAgInSc4d1	samostatný
okres	okres	k1gInSc4	okres
Brno-město	Brnoěsta	k1gFnSc5	Brno-města
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
řek	řeka	k1gFnPc2	řeka
Svratky	Svratka	k1gFnSc2	Svratka
a	a	k8xC	a
Svitavy	Svitava	k1gFnSc2	Svitava
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
381	[number]	k4	381
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
oblasti	oblast	k1gFnSc6	oblast
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
600	[number]	k4	600
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
je	být	k5eAaImIp3nS	být
centrem	centr	k1gInSc7	centr
soudní	soudní	k2eAgFnSc2d1	soudní
moci	moc	k1gFnSc2	moc
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
sídlem	sídlo	k1gNnSc7	sídlo
jak	jak	k6eAd1	jak
Ústavního	ústavní	k2eAgInSc2d1	ústavní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
správního	správní	k2eAgInSc2d1	správní
soudu	soud	k1gInSc2	soud
i	i	k8xC	i
Nejvyššího	vysoký	k2eAgNnSc2d3	nejvyšší
státního	státní	k2eAgNnSc2d1	státní
zastupitelství	zastupitelství	k1gNnSc2	zastupitelství
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
celkově	celkově	k6eAd1	celkově
významným	významný	k2eAgNnSc7d1	významné
administrativním	administrativní	k2eAgNnSc7d1	administrativní
střediskem	středisko	k1gNnSc7	středisko
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zde	zde	k6eAd1	zde
sídlí	sídlet	k5eAaImIp3nS	sídlet
státní	státní	k2eAgInPc4d1	státní
orgány	orgán	k1gInPc4	orgán
s	s	k7c7	s
celostátní	celostátní	k2eAgFnSc7d1	celostátní
kontrolní	kontrolní	k2eAgFnSc7d1	kontrolní
působností	působnost	k1gFnSc7	působnost
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
důležité	důležitý	k2eAgFnPc4d1	důležitá
instituce	instituce	k1gFnPc4	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
například	například	k6eAd1	například
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
,	,	kIx,	,
veřejný	veřejný	k2eAgMnSc1d1	veřejný
ochránce	ochránce	k1gMnSc1	ochránce
práv	právo	k1gNnPc2	právo
nebo	nebo	k8xC	nebo
Státní	státní	k2eAgFnSc1d1	státní
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
a	a	k8xC	a
potravinářská	potravinářský	k2eAgFnSc1d1	potravinářská
inspekce	inspekce	k1gFnSc1	inspekce
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1777	[number]	k4	1777
je	být	k5eAaImIp3nS	být
Brno	Brno	k1gNnSc4	Brno
také	také	k9	také
sídlem	sídlo	k1gNnSc7	sídlo
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
brněnské	brněnský	k2eAgFnSc2d1	brněnská
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
,	,	kIx,	,
biskupským	biskupský	k2eAgInSc7d1	biskupský
chrámem	chrám	k1gInSc7	chrám
je	být	k5eAaImIp3nS	být
katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
na	na	k7c6	na
Petrově	Petrov	k1gInSc6	Petrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgNnSc7d1	významné
střediskem	středisko	k1gNnSc7	středisko
vysokého	vysoký	k2eAgNnSc2d1	vysoké
školství	školství	k1gNnSc2	školství
s	s	k7c7	s
34	[number]	k4	34
fakultami	fakulta	k1gFnPc7	fakulta
čtrnácti	čtrnáct	k4xCc2	čtrnáct
univerzit	univerzita	k1gFnPc2	univerzita
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
83	[number]	k4	83
000	[number]	k4	000
studenty	student	k1gMnPc7	student
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
zákonem	zákon	k1gInSc7	zákon
zřízeno	zřízen	k2eAgNnSc4d1	zřízeno
studio	studio	k1gNnSc4	studio
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
a	a	k8xC	a
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Brněnském	brněnský	k2eAgNnSc6d1	brněnské
výstavišti	výstaviště	k1gNnSc6	výstaviště
jsou	být	k5eAaImIp3nP	být
tradičně	tradičně	k6eAd1	tradičně
konány	konán	k2eAgFnPc1d1	konána
velké	velký	k2eAgFnPc1d1	velká
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
výstavy	výstava	k1gFnPc1	výstava
a	a	k8xC	a
veletrhy	veletrh	k1gInPc1	veletrh
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
areál	areál	k1gInSc1	areál
výstaviště	výstaviště	k1gNnSc2	výstaviště
započal	započnout	k5eAaPmAgInS	započnout
svůj	svůj	k3xOyFgInSc4	svůj
provoz	provoz	k1gInSc4	provoz
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
také	také	k9	také
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
zde	zde	k6eAd1	zde
pořádanou	pořádaný	k2eAgFnSc7d1	pořádaná
událostí	událost	k1gFnSc7	událost
je	být	k5eAaImIp3nS	být
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
strojírenský	strojírenský	k2eAgInSc1d1	strojírenský
veletrh	veletrh	k1gInSc1	veletrh
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
proslulo	proslout	k5eAaPmAgNnS	proslout
i	i	k9	i
coby	coby	k?	coby
dějiště	dějiště	k1gNnSc2	dějiště
velkých	velký	k2eAgInPc2d1	velký
motoristických	motoristický	k2eAgInPc2d1	motoristický
závodů	závod	k1gInPc2	závod
konaných	konaný	k2eAgInPc2d1	konaný
na	na	k7c6	na
blízkém	blízký	k2eAgInSc6d1	blízký
Masarykově	Masarykův	k2eAgInSc6d1	Masarykův
okruhu	okruh	k1gInSc6	okruh
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
tradice	tradice	k1gFnSc1	tradice
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k6eAd1	až
do	do	k7c2	do
třicátých	třicátý	k4xOgNnPc2	třicátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejprestižnějším	prestižní	k2eAgInPc3d3	nejprestižnější
závodům	závod	k1gInPc3	závod
patří	patřit	k5eAaImIp3nS	patřit
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc1	Prix
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
součást	součást	k1gFnSc1	součást
seriálu	seriál	k1gInSc2	seriál
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
silničních	silniční	k2eAgInPc2d1	silniční
motocyklů	motocykl	k1gInPc2	motocykl
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
také	také	k9	také
každoročně	každoročně	k6eAd1	každoročně
hostí	hostit	k5eAaImIp3nP	hostit
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
přehlídku	přehlídka	k1gFnSc4	přehlídka
ohňostrojů	ohňostroj	k1gInPc2	ohňostroj
Ignis	Ignis	k1gFnSc2	Ignis
Brunensis	Brunensis	k1gFnSc2	Brunensis
<g/>
,	,	kIx,	,
pořádanou	pořádaný	k2eAgFnSc7d1	pořádaná
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
této	tento	k3xDgFnSc2	tento
události	událost	k1gFnSc2	událost
se	se	k3xPyFc4	se
typicky	typicky	k6eAd1	typicky
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
jedním	jeden	k4xCgMnSc7	jeden
až	až	k9	až
dvěma	dva	k4xCgFnPc7	dva
sty	sto	k4xCgNnPc7	sto
tisíci	tisíc	k4xCgInPc7	tisíc
návštěvníků	návštěvník	k1gMnPc2	návštěvník
po	po	k7c4	po
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
jejího	její	k3xOp3gNnSc2	její
konání	konání	k1gNnSc2	konání
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgFnPc3d3	nejvýznamnější
dominantám	dominanta	k1gFnPc3	dominanta
města	město	k1gNnSc2	město
patří	patřit	k5eAaImIp3nP	patřit
hrad	hrad	k1gInSc4	hrad
a	a	k8xC	a
pevnost	pevnost	k1gFnSc4	pevnost
Špilberk	Špilberk	k1gInSc4	Špilberk
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
kopci	kopec	k1gInSc6	kopec
a	a	k8xC	a
katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
na	na	k7c6	na
vršku	vršek	k1gInSc6	vršek
Petrov	Petrov	k1gInSc1	Petrov
<g/>
,	,	kIx,	,
utvářející	utvářející	k2eAgNnSc1d1	utvářející
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
panorama	panorama	k1gNnSc1	panorama
města	město	k1gNnSc2	město
a	a	k8xC	a
často	často	k6eAd1	často
vyobrazovaná	vyobrazovaný	k2eAgFnSc1d1	vyobrazovaná
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gInSc1	jeho
symbol	symbol	k1gInSc1	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
dochovaným	dochovaný	k2eAgInSc7d1	dochovaný
hradem	hrad	k1gInSc7	hrad
na	na	k7c6	na
území	území	k1gNnSc6	území
Brna	Brno	k1gNnSc2	Brno
je	být	k5eAaImIp3nS	být
Veveří	veveří	k2eAgNnSc1d1	veveří
<g/>
,	,	kIx,	,
kdysi	kdysi	k6eAd1	kdysi
vybudovaný	vybudovaný	k2eAgInSc1d1	vybudovaný
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
Svratkou	Svratka	k1gFnSc7	Svratka
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
tyčící	tyčící	k2eAgMnSc1d1	tyčící
nad	nad	k7c7	nad
Brněnskou	brněnský	k2eAgFnSc7d1	brněnská
přehradou	přehrada	k1gFnSc7	přehrada
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
památkou	památka	k1gFnSc7	památka
je	být	k5eAaImIp3nS	být
funkcionalistická	funkcionalistický	k2eAgFnSc1d1	funkcionalistická
vila	vila	k1gFnSc1	vila
Tugendhat	Tugendhat	k1gFnSc1	Tugendhat
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
zapsána	zapsat	k5eAaPmNgFnS	zapsat
mezi	mezi	k7c4	mezi
Světové	světový	k2eAgNnSc4d1	světové
dědictví	dědictví	k1gNnSc4	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
městské	městský	k2eAgNnSc1d1	Městské
jádro	jádro	k1gNnSc1	jádro
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
rezervací	rezervace	k1gFnSc7	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
turisticky	turisticky	k6eAd1	turisticky
atraktivním	atraktivní	k2eAgFnPc3d1	atraktivní
lokalitám	lokalita	k1gFnPc3	lokalita
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
Chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
Moravský	moravský	k2eAgInSc1d1	moravský
kras	kras	k1gInSc1	kras
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
nejjižnější	jižní	k2eAgFnSc1d3	nejjižnější
část	část	k1gFnSc1	část
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
je	být	k5eAaImIp3nS	být
rovněž	rovněž	k9	rovněž
vinařskou	vinařský	k2eAgFnSc7d1	vinařská
obcí	obec	k1gFnSc7	obec
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Velkopavlovické	Velkopavlovický	k2eAgFnSc2d1	Velkopavlovická
vinařské	vinařský	k2eAgFnSc2d1	vinařská
podoblasti	podoblast	k1gFnSc2	podoblast
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Nynější	nynější	k2eAgInSc1d1	nynější
název	název	k1gInSc1	název
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
se	se	k3xPyFc4	se
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
od	od	k7c2	od
názvu	název	k1gInSc2	název
někdejší	někdejší	k2eAgFnSc2d1	někdejší
osady	osada	k1gFnSc2	osada
ležící	ležící	k2eAgFnSc2d1	ležící
na	na	k7c6	na
brodu	brod	k1gInSc6	brod
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Svratku	Svratka	k1gFnSc4	Svratka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
lokalitě	lokalita	k1gFnSc6	lokalita
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
starší	starý	k2eAgInSc4d2	starší
původ	původ	k1gInSc4	původ
tohoto	tento	k3xDgInSc2	tento
názvu	název	k1gInSc2	název
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasný	jasný	k2eAgInSc1d1	jasný
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInSc1d3	nejstarší
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
Brnen	Brnen	k1gInSc4	Brnen
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
ale	ale	k9	ale
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
označeními	označení	k1gNnPc7	označení
jako	jako	k8xC	jako
Brvnn	Brvnn	k1gInSc1	Brvnn
<g/>
,	,	kIx,	,
Brin	Brin	k1gNnSc1	Brin
nebo	nebo	k8xC	nebo
Brnno	Brnno	k1gNnSc1	Brnno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
latině	latina	k1gFnSc6	latina
bylo	být	k5eAaImAgNnS	být
Brno	Brno	k1gNnSc1	Brno
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xC	jako
Bruna	Bruna	k1gMnSc1	Bruna
a	a	k8xC	a
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
jako	jako	k9	jako
Brünn	Brünn	k1gMnSc1	Brünn
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1949	[number]	k4	1949
a	a	k8xC	a
1992	[number]	k4	1992
bylo	být	k5eAaImAgNnS	být
Brno	Brno	k1gNnSc1	Brno
oficiálně	oficiálně	k6eAd1	oficiálně
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
krajské	krajský	k2eAgNnSc1d1	krajské
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
neslo	nést	k5eAaImAgNnS	nést
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
Zemské	zemský	k2eAgNnSc4d1	zemské
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Brno	Brno	k1gNnSc1	Brno
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
v	v	k7c6	v
době	doba	k1gFnSc6	doba
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
Brno	Brno	k1gNnSc1	Brno
bylo	být	k5eAaImAgNnS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
již	již	k6eAd1	již
dávno	dávno	k6eAd1	dávno
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
některých	některý	k3yIgInPc6	některý
starších	starý	k2eAgInPc6d2	starší
dokumentech	dokument	k1gInPc6	dokument
(	(	kIx(	(
<g/>
mapách	mapa	k1gFnPc6	mapa
<g/>
,	,	kIx,	,
různých	různý	k2eAgFnPc6d1	různá
listinách	listina	k1gFnPc6	listina
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
Královské	královský	k2eAgNnSc1d1	královské
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Brně	Brno	k1gNnSc6	Brno
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1091	[number]	k4	1091
z	z	k7c2	z
Kosmovy	Kosmův	k2eAgFnSc2d1	Kosmova
kroniky	kronika	k1gFnSc2	kronika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
psáno	psán	k2eAgNnSc1d1	psáno
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
Vratislav	Vratislav	k1gMnSc1	Vratislav
I.	I.	kA	I.
vtrhl	vtrhnout	k5eAaPmAgMnS	vtrhnout
s	s	k7c7	s
vojskem	vojsko	k1gNnSc7	vojsko
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
a	a	k8xC	a
oblehl	oblehnout	k5eAaPmAgInS	oblehnout
hrad	hrad	k1gInSc1	hrad
Brno	Brno	k1gNnSc1	Brno
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
lehký	lehký	k2eAgInSc1d1	lehký
kulomet	kulomet	k1gInSc1	kulomet
BREN	BREN	kA	BREN
(	(	kIx(	(
<g/>
Brno	Brno	k1gNnSc1	Brno
+	+	kIx~	+
Enfield	Enfield	k1gInSc1	Enfield
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
zbraň	zbraň	k1gFnSc1	zbraň
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
používaná	používaný	k2eAgFnSc1d1	používaná
především	především	k6eAd1	především
armádou	armáda	k1gFnSc7	armáda
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
například	například	k6eAd1	například
kulinářská	kulinářský	k2eAgFnSc1d1	kulinářská
specialita	specialita	k1gFnSc1	specialita
brněnský	brněnský	k2eAgInSc1d1	brněnský
řízek	řízek	k1gInSc4	řízek
nebo	nebo	k8xC	nebo
malý	malý	k2eAgInSc4d1	malý
asteroid	asteroid	k1gInSc4	asteroid
"	"	kIx"	"
<g/>
2889	[number]	k4	2889
Brno	Brno	k1gNnSc1	Brno
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
objevený	objevený	k2eAgInSc1d1	objevený
astronomem	astronom	k1gMnSc7	astronom
Antonínem	Antonín	k1gMnSc7	Antonín
Mrkosem	Mrkos	k1gMnSc7	Mrkos
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
brněnské	brněnský	k2eAgFnSc6d1	brněnská
mluvě	mluva	k1gFnSc6	mluva
zvané	zvaný	k2eAgInPc1d1	zvaný
hantec	hantec	k1gInSc1	hantec
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
užívání	užívání	k1gNnSc1	užívání
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
běžné	běžný	k2eAgFnPc4d1	běžná
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Brno	Brno	k1gNnSc1	Brno
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
Brnisko	Brnisko	k1gNnSc1	Brnisko
nebo	nebo	k8xC	nebo
Bryncl	Bryncl	k1gInSc1	Bryncl
<g/>
,	,	kIx,	,
možné	možný	k2eAgNnSc1d1	možné
je	být	k5eAaImIp3nS	být
také	také	k9	také
označení	označení	k1gNnSc1	označení
Štatl	Štatl	k1gFnSc2	Štatl
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
tří	tři	k4xCgFnPc2	tři
nejtypičtější	typický	k2eAgFnPc1d3	nejtypičtější
mezi	mezi	k7c7	mezi
rodilými	rodilý	k2eAgMnPc7d1	rodilý
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
původně	původně	k6eAd1	původně
označuje	označovat	k5eAaImIp3nS	označovat
spíše	spíše	k9	spíše
jen	jen	k9	jen
centrum	centrum	k1gNnSc1	centrum
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Za	za	k7c2	za
přímého	přímý	k2eAgMnSc2d1	přímý
předchůdce	předchůdce	k1gMnSc2	předchůdce
Brna	Brno	k1gNnSc2	Brno
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
hradiště	hradiště	k1gNnSc1	hradiště
Staré	Staré	k2eAgInPc1d1	Staré
Zámky	zámek	k1gInPc1	zámek
na	na	k7c6	na
katastru	katastr	k1gInSc6	katastr
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Líšně	Líšeň	k1gFnSc2	Líšeň
<g/>
,	,	kIx,	,
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
hradisek	hradisko	k1gNnPc2	hradisko
Velkomoravské	velkomoravský	k2eAgFnSc2d1	Velkomoravská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
osídlení	osídlení	k1gNnSc1	osídlení
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
od	od	k7c2	od
konce	konec	k1gInSc2	konec
neolitu	neolit	k1gInSc2	neolit
až	až	k6eAd1	až
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Počátky	počátek	k1gInPc4	počátek
<g/>
,	,	kIx,	,
středověk	středověk	k1gInSc1	středověk
a	a	k8xC	a
novověk	novověk	k1gInSc1	novověk
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
nálezy	nález	k1gInPc1	nález
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
v	v	k7c6	v
brněnské	brněnský	k2eAgFnSc6d1	brněnská
oblasti	oblast	k1gFnSc6	oblast
jsou	být	k5eAaImIp3nP	být
přibližně	přibližně	k6eAd1	přibližně
z	z	k7c2	z
paleolitu	paleolit	k1gInSc2	paleolit
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Stránské	Stránské	k2eAgFnSc2d1	Stránské
skály	skála	k1gFnSc2	skála
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
doložené	doložený	k2eAgNnSc1d1	doložené
slovanské	slovanský	k2eAgNnSc1d1	slovanské
osídlení	osídlení	k1gNnSc1	osídlení
brněnské	brněnský	k2eAgFnSc2d1	brněnská
oblasti	oblast	k1gFnSc2	oblast
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kolem	kolem	k7c2	kolem
5	[number]	k4	5
<g/>
.	.	kIx.	.
až	až	k9	až
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
byly	být	k5eAaImAgInP	být
vždy	vždy	k6eAd1	vždy
pevně	pevně	k6eAd1	pevně
svázány	svázán	k2eAgInPc1d1	svázán
s	s	k7c7	s
dějinami	dějiny	k1gFnPc7	dějiny
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
údělných	údělný	k2eAgNnPc2d1	údělné
knížectví	knížectví	k1gNnPc2	knížectví
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
Brno	Brno	k1gNnSc1	Brno
zastávalo	zastávat	k5eAaImAgNnS	zastávat
mimořádně	mimořádně	k6eAd1	mimořádně
důležitou	důležitý	k2eAgFnSc4d1	důležitá
úlohu	úloha	k1gFnSc4	úloha
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ražba	ražba	k1gFnSc1	ražba
mincí	mince	k1gFnPc2	mince
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zde	zde	k6eAd1	zde
brněnská	brněnský	k2eAgNnPc1d1	brněnské
knížata	kníže	k1gNnPc1	kníže
razila	razit	k5eAaImAgNnP	razit
nejstarší	starý	k2eAgNnSc4d3	nejstarší
brněnský	brněnský	k2eAgInSc4d1	brněnský
denár	denár	k1gInSc4	denár
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Kosmově	Kosmův	k2eAgFnSc6d1	Kosmova
kronice	kronika	k1gFnSc6	kronika
roku	rok	k1gInSc2	rok
1091	[number]	k4	1091
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1243	[number]	k4	1243
byla	být	k5eAaImAgFnS	být
Brnu	Brno	k1gNnSc6	Brno
udělena	udělen	k2eAgNnPc1d1	uděleno
práva	právo	k1gNnPc1	právo
městská	městský	k2eAgFnSc1d1	městská
<g/>
,	,	kIx,	,
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
hrad	hrad	k1gInSc1	hrad
Špilberk	Špilberk	k1gInSc1	Špilberk
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1292	[number]	k4	1292
bylo	být	k5eAaImAgNnS	být
tehdejšímu	tehdejší	k2eAgMnSc3d1	tehdejší
královskému	královský	k2eAgMnSc3d1	královský
městu	město	k1gNnSc3	město
Brnu	Brno	k1gNnSc6	Brno
uděleno	udělen	k2eAgNnSc4d1	uděleno
právo	právo	k1gNnSc4	právo
volit	volit	k5eAaImF	volit
rychtáře	rychtář	k1gMnSc4	rychtář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1324	[number]	k4	1324
založila	založit	k5eAaPmAgFnS	založit
královna	královna	k1gFnSc1	královna
Eliška	Eliška	k1gFnSc1	Eliška
Rejčka	Rejčka	k1gFnSc1	Rejčka
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Brně	Brno	k1gNnSc6	Brno
kostel	kostel	k1gInSc4	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
baziliku	bazilika	k1gFnSc4	bazilika
a	a	k8xC	a
místo	místo	k1gNnSc4	místo
jejího	její	k3xOp3gInSc2	její
posledního	poslední	k2eAgInSc2d1	poslední
odpočinku	odpočinek	k1gInSc2	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
sídlila	sídlit	k5eAaImAgFnS	sídlit
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Janem	Jan	k1gMnSc7	Jan
Jindřichem	Jindřich	k1gMnSc7	Jindřich
založená	založený	k2eAgFnSc1d1	založená
moravská	moravský	k2eAgFnSc1d1	Moravská
větev	větev	k1gFnSc1	větev
Lucemburků	Lucemburk	k1gInPc2	Lucemburk
<g/>
,	,	kIx,	,
moravských	moravský	k2eAgNnPc2d1	Moravské
markrabat	markrabě	k1gNnPc2	markrabě
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
moravský	moravský	k2eAgMnSc1d1	moravský
Lucemburk	Lucemburk	k1gMnSc1	Lucemburk
Jošt	Jošt	k1gMnSc1	Jošt
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1410	[number]	k4	1410
dokonce	dokonce	k9	dokonce
zvolen	zvolit	k5eAaPmNgMnS	zvolit
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
začátkem	začátkem	k7c2	začátkem
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
ale	ale	k8xC	ale
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
Špilberku	Špilberka	k1gFnSc4	Špilberka
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Tomáše	Tomáš	k1gMnSc2	Tomáš
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
založil	založit	k5eAaPmAgMnS	založit
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Jan	Jan	k1gMnSc1	Jan
Jindřich	Jindřich	k1gMnSc1	Jindřich
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
byl	být	k5eAaImAgInS	být
markraběcí	markraběcí	k2eAgInSc1d1	markraběcí
dvůr	dvůr	k1gInSc1	dvůr
<g/>
,	,	kIx,	,
zasedal	zasedat	k5eAaImAgInS	zasedat
zde	zde	k6eAd1	zde
moravský	moravský	k2eAgInSc1d1	moravský
zemský	zemský	k2eAgInSc1d1	zemský
sněm	sněm	k1gInSc1	sněm
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
vrcholné	vrcholný	k2eAgFnPc1d1	vrcholná
politické	politický	k2eAgFnPc1d1	politická
instituce	instituce	k1gFnPc1	instituce
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
je	být	k5eAaImIp3nS	být
bývalé	bývalý	k2eAgNnSc1d1	bývalé
moravské	moravský	k2eAgNnSc1d1	Moravské
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
a	a	k8xC	a
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Olomoucí	Olomouc	k1gFnSc7	Olomouc
jejím	její	k3xOp3gNnSc7	její
historicky	historicky	k6eAd1	historicky
nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1777	[number]	k4	1777
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
brněnské	brněnský	k2eAgNnSc1d1	brněnské
biskupství	biskupství	k1gNnSc1	biskupství
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
přijel	přijet	k5eAaPmAgMnS	přijet
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
první	první	k4xOgInSc1	první
vlak	vlak	k1gInSc1	vlak
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
začal	začít	k5eAaPmAgInS	začít
provoz	provoz	k1gInSc4	provoz
parostrojní	parostrojní	k2eAgFnSc2d1	parostrojní
železnice	železnice	k1gFnSc2	železnice
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
bylo	být	k5eAaImAgNnS	být
Brno	Brno	k1gNnSc1	Brno
poprvé	poprvé	k6eAd1	poprvé
administrativně	administrativně	k6eAd1	administrativně
rozšířeno	rozšířit	k5eAaPmNgNnS	rozšířit
připojením	připojení	k1gNnSc7	připojení
20	[number]	k4	20
okolních	okolní	k2eAgNnPc2d1	okolní
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1859	[number]	k4	1859
<g/>
–	–	k?	–
<g/>
1864	[number]	k4	1864
bylo	být	k5eAaImAgNnS	být
zbořeno	zbořit	k5eAaPmNgNnS	zbořit
téměř	téměř	k6eAd1	téměř
kompletně	kompletně	k6eAd1	kompletně
celé	celý	k2eAgNnSc1d1	celé
městské	městský	k2eAgNnSc1d1	Městské
opevnění	opevnění	k1gNnSc1	opevnění
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1896	[number]	k4	1896
<g/>
–	–	k?	–
<g/>
1916	[number]	k4	1916
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
jádru	jádro	k1gNnSc6	jádro
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
asanace	asanace	k1gFnSc1	asanace
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
bylo	být	k5eAaImAgNnS	být
asi	asi	k9	asi
240	[number]	k4	240
domů	dům	k1gInPc2	dům
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
novostavbami	novostavba	k1gFnPc7	novostavba
nebo	nebo	k8xC	nebo
zcela	zcela	k6eAd1	zcela
odstraněno	odstranit	k5eAaPmNgNnS	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
poprvé	poprvé	k6eAd1	poprvé
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Česka	Česko	k1gNnSc2	Česko
vyjela	vyjet	k5eAaPmAgFnS	vyjet
do	do	k7c2	do
ulic	ulice	k1gFnPc2	ulice
města	město	k1gNnSc2	město
tramvaj	tramvaj	k1gFnSc4	tramvaj
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
tažena	tažen	k2eAgFnSc1d1	tažena
koňmi	kůň	k1gMnPc7	kůň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Významné	významný	k2eAgFnPc1d1	významná
bitvy	bitva	k1gFnPc1	bitva
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
Brno	Brno	k1gNnSc1	Brno
během	během	k7c2	během
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
pevnou	pevný	k2eAgFnSc7d1	pevná
baštou	bašta	k1gFnSc7	bašta
katolictví	katolictví	k1gNnSc2	katolictví
a	a	k8xC	a
husité	husita	k1gMnPc1	husita
město	město	k1gNnSc4	město
nezískali	získat	k5eNaPmAgMnP	získat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1467	[number]	k4	1467
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc4	Brno
obléhalo	obléhat	k5eAaImAgNnS	obléhat
hrad	hrad	k1gInSc1	hrad
Špilberk	Špilberk	k1gInSc4	Špilberk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlila	sídlit	k5eAaImAgFnS	sídlit
posádka	posádka	k1gFnSc1	posádka
(	(	kIx(	(
<g/>
husitského	husitský	k2eAgNnSc2d1	husitské
<g/>
)	)	kIx)	)
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
a	a	k8xC	a
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
Brno	Brno	k1gNnSc1	Brno
obléháno	obléhat	k5eAaImNgNnS	obléhat
roku	rok	k1gInSc2	rok
1643	[number]	k4	1643
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
roku	rok	k1gInSc2	rok
1645	[number]	k4	1645
švédskými	švédský	k2eAgNnPc7d1	švédské
vojsky	vojsko	k1gNnPc7	vojsko
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
generála	generál	k1gMnSc2	generál
Lennarta	Lennart	k1gMnSc2	Lennart
Torstensona	Torstenson	k1gMnSc2	Torstenson
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
významnou	významný	k2eAgFnSc4d1	významná
epizodu	epizoda	k1gFnSc4	epizoda
v	v	k7c6	v
třicetileté	třicetiletý	k2eAgFnSc6d1	třicetiletá
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Švédům	Švéd	k1gMnPc3	Švéd
se	se	k3xPyFc4	se
však	však	k9	však
město	město	k1gNnSc1	město
nikdy	nikdy	k6eAd1	nikdy
dobýt	dobýt	k5eAaPmF	dobýt
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
Brno	Brno	k1gNnSc1	Brno
obléháno	obléhat	k5eAaImNgNnS	obléhat
roku	rok	k1gInSc2	rok
1742	[number]	k4	1742
pruskými	pruský	k2eAgNnPc7d1	pruské
vojsky	vojsko	k1gNnPc7	vojsko
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Fridricha	Fridrich	k1gMnSc2	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Velikého	veliký	k2eAgMnSc4d1	veliký
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
rovněž	rovněž	k9	rovněž
neúspěšně	úspěšně	k6eNd1	úspěšně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1805	[number]	k4	1805
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
východně	východně	k6eAd1	východně
od	od	k7c2	od
Brna	Brno	k1gNnSc2	Brno
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Slavkova	Slavkov	k1gInSc2	Slavkov
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
též	též	k9	též
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
bitva	bitva	k1gFnSc1	bitva
tří	tři	k4xCgMnPc2	tři
císařů	císař	k1gMnPc2	císař
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
samo	sám	k3xTgNnSc1	sám
se	se	k3xPyFc4	se
do	do	k7c2	do
bitvy	bitva	k1gFnSc2	bitva
nezapojilo	zapojit	k5eNaPmAgNnS	zapojit
<g/>
,	,	kIx,	,
francouzská	francouzský	k2eAgFnSc1d1	francouzská
armáda	armáda	k1gFnSc1	armáda
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
bez	bez	k7c2	bez
boje	boj	k1gInSc2	boj
dočasně	dočasně	k6eAd1	dočasně
obsadila	obsadit	k5eAaPmAgFnS	obsadit
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
Napoleon	Napoleon	k1gMnSc1	Napoleon
I.	I.	kA	I.
zde	zde	k6eAd1	zde
několikrát	několikrát	k6eAd1	několikrát
přenocoval	přenocovat	k5eAaPmAgMnS	přenocovat
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
bylo	být	k5eAaImAgNnS	být
Brno	Brno	k1gNnSc1	Brno
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
armádou	armáda	k1gFnSc7	armáda
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
též	též	k9	též
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1809	[number]	k4	1809
a	a	k8xC	a
Napoleon	napoleon	k1gInSc1	napoleon
ve	v	k7c6	v
městě	město	k1gNnSc6	město
opět	opět	k6eAd1	opět
několik	několik	k4yIc4	několik
dní	den	k1gInPc2	den
pobyl	pobýt	k5eAaPmAgMnS	pobýt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
bylo	být	k5eAaImAgNnS	být
Brno	Brno	k1gNnSc1	Brno
osvobozeno	osvobodit	k5eAaPmNgNnS	osvobodit
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
maršála	maršál	k1gMnSc2	maršál
Rodiona	Rodion	k1gMnSc2	Rodion
Malinovského	Malinovský	k2eAgMnSc2d1	Malinovský
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
bratislavsko-brněnské	bratislavskorněnský	k2eAgFnSc2d1	bratislavsko-brněnský
operace	operace	k1gFnSc2	operace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Brno	Brno	k1gNnSc1	Brno
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Moravy	Morava	k1gFnSc2	Morava
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Před	před	k7c7	před
několika	několik	k4yIc7	několik
staletími	staletí	k1gNnPc7	staletí
bylo	být	k5eAaImAgNnS	být
Brno	Brno	k1gNnSc1	Brno
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
s	s	k7c7	s
Olomoucí	Olomouc	k1gFnSc7	Olomouc
o	o	k7c4	o
postavení	postavení	k1gNnSc4	postavení
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
spor	spor	k1gInSc4	spor
zřejmě	zřejmě	k6eAd1	zřejmě
zapříčinil	zapříčinit	k5eAaPmAgMnS	zapříčinit
kníže	kníže	k1gMnSc1	kníže
Břetislav	Břetislav	k1gMnSc1	Břetislav
I.	I.	kA	I.
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1055	[number]	k4	1055
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
Moravu	Morava	k1gFnSc4	Morava
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
navzájem	navzájem	k6eAd1	navzájem
nezávislá	závislý	k2eNgNnPc1d1	nezávislé
území	území	k1gNnPc1	území
<g/>
,	,	kIx,	,
olomoucký	olomoucký	k2eAgInSc1d1	olomoucký
a	a	k8xC	a
brněnský	brněnský	k2eAgInSc1d1	brněnský
úděl	úděl	k1gInSc1	úděl
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
i	i	k9	i
samostatný	samostatný	k2eAgInSc1d1	samostatný
znojemský	znojemský	k2eAgInSc1d1	znojemský
úděl	úděl	k1gInSc1	úděl
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
decentralizaci	decentralizace	k1gFnSc3	decentralizace
moci	moc	k1gFnSc3	moc
a	a	k8xC	a
vzniku	vznik	k1gInSc3	vznik
tří	tři	k4xCgFnPc2	tři
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
znovu	znovu	k6eAd1	znovu
dvou	dva	k4xCgMnPc6	dva
<g/>
,	,	kIx,	,
center	centrum	k1gNnPc2	centrum
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Znovusjednocení	znovusjednocení	k1gNnSc1	znovusjednocení
Moravy	Morava	k1gFnSc2	Morava
započalo	započnout	k5eAaPmAgNnS	započnout
roku	rok	k1gInSc2	rok
1182	[number]	k4	1182
z	z	k7c2	z
vůle	vůle	k1gFnSc2	vůle
římskoněmeckého	římskoněmecký	k2eAgMnSc2d1	římskoněmecký
císaře	císař	k1gMnSc2	císař
Fridricha	Fridrich	k1gMnSc2	Fridrich
I.	I.	kA	I.
Barbarossy	Barbarossa	k1gMnSc2	Barbarossa
vznikem	vznik	k1gInSc7	vznik
Markrabství	markrabství	k1gNnSc4	markrabství
moravského	moravský	k2eAgNnSc2d1	Moravské
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1348	[number]	k4	1348
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
markraběte	markrabě	k1gMnSc2	markrabě
a	a	k8xC	a
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
byl	být	k5eAaImAgInS	být
zřízen	zřídit	k5eAaPmNgInS	zřídit
moravský	moravský	k2eAgInSc1d1	moravský
zemský	zemský	k2eAgInSc1d1	zemský
soud	soud	k1gInSc1	soud
a	a	k8xC	a
moravské	moravský	k2eAgFnSc2d1	Moravská
zemské	zemský	k2eAgFnSc2d1	zemská
desky	deska	k1gFnSc2	deska
<g/>
,	,	kIx,	,
oboje	oboj	k1gFnPc1	oboj
zároveň	zároveň	k6eAd1	zároveň
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pozice	pozice	k1gFnSc1	pozice
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Moravy	Morava	k1gFnSc2	Morava
byla	být	k5eAaImAgFnS	být
stále	stále	k6eAd1	stále
nejasná	jasný	k2eNgFnSc1d1	nejasná
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
oficiálně	oficiálně	k6eAd1	oficiálně
byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
první	první	k4xOgFnSc1	první
uváděna	uváděn	k2eAgFnSc1d1	uváděna
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
totiž	totiž	k9	totiž
větší	veliký	k2eAgFnSc1d2	veliký
a	a	k8xC	a
ekonomicky	ekonomicky	k6eAd1	ekonomicky
silnější	silný	k2eAgFnSc1d2	silnější
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
sídlem	sídlo	k1gNnSc7	sídlo
biskupa	biskup	k1gMnSc2	biskup
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
výhodnější	výhodný	k2eAgFnSc4d2	výhodnější
polohu	poloha	k1gFnSc4	poloha
blíže	blízce	k6eAd2	blízce
centru	centrum	k1gNnSc3	centrum
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
Moravy	Morava	k1gFnSc2	Morava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
bylo	být	k5eAaImAgNnS	být
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
sídlem	sídlo	k1gNnSc7	sídlo
markraběte	markrabě	k1gMnSc2	markrabě
(	(	kIx(	(
<g/>
moravského	moravský	k2eAgMnSc4d1	moravský
vládce	vládce	k1gMnSc4	vládce
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
hrála	hrát	k5eAaImAgFnS	hrát
roli	role	k1gFnSc4	role
i	i	k8xC	i
blízkost	blízkost	k1gFnSc4	blízkost
rakouské	rakouský	k2eAgFnSc2d1	rakouská
metropole	metropol	k1gFnSc2	metropol
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Moravský	moravský	k2eAgInSc1d1	moravský
zemský	zemský	k2eAgInSc1d1	zemský
sněm	sněm	k1gInSc1	sněm
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
zákonodárnou	zákonodárný	k2eAgFnSc7d1	zákonodárná
institucí	instituce	k1gFnSc7	instituce
v	v	k7c6	v
markrabství	markrabství	k1gNnSc6	markrabství
<g/>
,	,	kIx,	,
zasedal	zasedat	k5eAaImAgInS	zasedat
střídavě	střídavě	k6eAd1	střídavě
v	v	k7c6	v
obou	dva	k4xCgNnPc6	dva
městech	město	k1gNnPc6	město
(	(	kIx(	(
<g/>
krátkou	krátký	k2eAgFnSc4d1	krátká
dobu	doba	k1gFnSc4	doba
i	i	k9	i
ve	v	k7c6	v
Znojmě	Znojmo	k1gNnSc6	Znojmo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
zemský	zemský	k2eAgInSc1d1	zemský
soud	soud	k1gInSc1	soud
zasedal	zasedat	k5eAaImAgInS	zasedat
střídavě	střídavě	k6eAd1	střídavě
a	a	k8xC	a
zemské	zemský	k2eAgFnSc2d1	zemská
desky	deska	k1gFnSc2	deska
byly	být	k5eAaImAgInP	být
také	také	k9	také
vedeny	vést	k5eAaImNgInP	vést
v	v	k7c6	v
obou	dva	k4xCgNnPc6	dva
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
však	však	k9	však
měla	mít	k5eAaImAgFnS	mít
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
výhodnější	výhodný	k2eAgNnSc1d2	výhodnější
postavení	postavení	k1gNnSc1	postavení
než	než	k8xS	než
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
až	až	k9	až
do	do	k7c2	do
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
roku	rok	k1gInSc2	rok
1636	[number]	k4	1636
zřízen	zřízen	k2eAgInSc1d1	zřízen
královský	královský	k2eAgInSc1d1	královský
tribunál	tribunál	k1gInSc1	tribunál
<g/>
,	,	kIx,	,
instituce	instituce	k1gFnSc1	instituce
natolik	natolik	k6eAd1	natolik
významná	významný	k2eAgFnSc1d1	významná
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Brno	Brno	k1gNnSc1	Brno
stalo	stát	k5eAaPmAgNnS	stát
de	de	k?	de
facto	fact	k2eAgNnSc1d1	facto
jediným	jediný	k2eAgNnSc7d1	jediné
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
nadlouho	nadlouho	k6eAd1	nadlouho
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
milníkem	milník	k1gInSc7	milník
sporu	spor	k1gInSc2	spor
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
až	až	k9	až
přelom	přelom	k1gInSc4	přelom
let	léto	k1gNnPc2	léto
1641	[number]	k4	1641
<g/>
–	–	k?	–
<g/>
1642	[number]	k4	1642
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1641	[number]	k4	1641
markrabě	markrabě	k1gMnSc1	markrabě
a	a	k8xC	a
císař	císař	k1gMnSc1	císař
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
nařídil	nařídit	k5eAaPmAgMnS	nařídit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
zemský	zemský	k2eAgInSc1d1	zemský
sněm	sněm	k1gInSc1	sněm
i	i	k8xC	i
soud	soud	k1gInSc1	soud
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
královským	královský	k2eAgInSc7d1	královský
tribunálem	tribunál	k1gInSc7	tribunál
a	a	k8xC	a
zemskými	zemský	k2eAgFnPc7d1	zemská
deskami	deska	k1gFnPc7	deska
trvale	trvale	k6eAd1	trvale
přemístěn	přemístit	k5eAaPmNgMnS	přemístit
z	z	k7c2	z
Olomouce	Olomouc	k1gFnSc2	Olomouc
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
bylo	být	k5eAaImAgNnS	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
střídavé	střídavý	k2eAgNnSc1d1	střídavé
zasedání	zasedání	k1gNnSc1	zasedání
zemského	zemský	k2eAgInSc2d1	zemský
sněmu	sněm	k1gInSc2	sněm
a	a	k8xC	a
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
obou	dva	k4xCgNnPc6	dva
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1642	[number]	k4	1642
se	se	k3xPyFc4	se
Olomouc	Olomouc	k1gFnSc1	Olomouc
vzdala	vzdát	k5eAaPmAgFnS	vzdát
švédskému	švédský	k2eAgInSc3d1	švédský
vojsku	vojsko	k1gNnSc3	vojsko
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
těžce	těžce	k6eAd1	těžce
upadl	upadnout	k5eAaPmAgMnS	upadnout
její	její	k3xOp3gInSc4	její
politický	politický	k2eAgInSc4d1	politický
význam	význam	k1gInSc4	význam
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
byly	být	k5eAaImAgInP	být
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
také	také	k6eAd1	také
přemístěny	přemístěn	k2eAgFnPc1d1	přemístěna
celé	celá	k1gFnPc1	celá
zemské	zemský	k2eAgFnSc2d1	zemská
desky	deska	k1gFnSc2	deska
<g/>
,	,	kIx,	,
a	a	k8xC	a
Brno	Brno	k1gNnSc1	Brno
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
definitivně	definitivně	k6eAd1	definitivně
stalo	stát	k5eAaPmAgNnS	stát
jediným	jediný	k2eAgMnSc7d1	jediný
politickým	politický	k2eAgMnSc7d1	politický
centrem	centr	k1gMnSc7	centr
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
konečné	konečný	k2eAgNnSc1d1	konečné
vyřešení	vyřešení	k1gNnSc1	vyřešení
sporů	spor	k1gInPc2	spor
o	o	k7c4	o
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Moravy	Morava	k1gFnSc2	Morava
mezi	mezi	k7c7	mezi
Brnem	Brno	k1gNnSc7	Brno
a	a	k8xC	a
Olomoucí	Olomouc	k1gFnSc7	Olomouc
náleží	náležet	k5eAaImIp3nS	náležet
až	až	k9	až
markrabímu	markrabí	k1gMnSc3	markrabí
a	a	k8xC	a
císaři	císař	k1gMnSc3	císař
Josefu	Josef	k1gMnSc3	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1782	[number]	k4	1782
přiznal	přiznat	k5eAaPmAgMnS	přiznat
nárok	nárok	k1gInSc4	nárok
na	na	k7c6	na
označení	označení	k1gNnSc6	označení
Brna	Brno	k1gNnSc2	Brno
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
trvale	trvale	k6eAd1	trvale
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
později	pozdě	k6eAd2	pozdě
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
i	i	k9	i
zemskou	zemský	k2eAgFnSc7d1	zemská
ústavou	ústava	k1gFnSc7	ústava
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
např.	např.	kA	např.
působením	působení	k1gNnPc3	působení
vrchního	vrchní	k2eAgInSc2d1	vrchní
zemského	zemský	k2eAgInSc2d1	zemský
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
působnost	působnost	k1gFnSc1	působnost
se	se	k3xPyFc4	se
ale	ale	k9	ale
nevztahovala	vztahovat	k5eNaImAgFnS	vztahovat
jen	jen	k9	jen
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c4	na
Rakouské	rakouský	k2eAgNnSc4d1	rakouské
Slezsko	Slezsko	k1gNnSc4	Slezsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brno	Brno	k1gNnSc1	Brno
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Moravy	Morava	k1gFnSc2	Morava
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
Země	země	k1gFnSc1	země
Moravská	moravský	k2eAgFnSc1d1	Moravská
sloučena	sloučit	k5eAaPmNgFnS	sloučit
se	s	k7c7	s
Zemí	zem	k1gFnSc7	zem
Slezskou	Slezská	k1gFnSc7	Slezská
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
Brno	Brno	k1gNnSc1	Brno
zemským	zemský	k2eAgNnSc7d1	zemské
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Země	zem	k1gFnSc2	zem
Moravskoslezské	moravskoslezský	k2eAgFnSc2d1	Moravskoslezská
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
vládou	vláda	k1gFnSc7	vláda
zrušena	zrušen	k2eAgFnSc1d1	zrušena
moravskoslezská	moravskoslezský	k2eAgFnSc1d1	Moravskoslezská
zemská	zemský	k2eAgFnSc1d1	zemská
samospráva	samospráva	k1gFnSc1	samospráva
a	a	k8xC	a
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
i	i	k9	i
pozice	pozice	k1gFnSc1	pozice
jejího	její	k3xOp3gNnSc2	její
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Moravské	moravský	k2eAgFnSc2d1	Moravská
zemské	zemský	k2eAgFnSc2d1	zemská
desky	deska	k1gFnSc2	deska
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
národní	národní	k2eAgFnSc7d1	národní
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
uloženy	uložit	k5eAaPmNgInP	uložit
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
zemském	zemský	k2eAgInSc6d1	zemský
archivu	archiv	k1gInSc6	archiv
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
a	a	k8xC	a
Velké	velký	k2eAgNnSc1d1	velké
Brno	Brno	k1gNnSc1	Brno
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
Brnu	Brno	k1gNnSc3	Brno
připojena	připojit	k5eAaPmNgFnS	připojit
dvě	dva	k4xCgFnPc4	dva
sousední	sousední	k2eAgInPc1d1	sousední
města	město	k1gNnSc2	město
Královo	Králův	k2eAgNnSc4d1	Královo
Pole	pole	k1gNnSc4	pole
a	a	k8xC	a
Husovice	Husovice	k1gFnPc4	Husovice
a	a	k8xC	a
21	[number]	k4	21
dalších	další	k2eAgFnPc2d1	další
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
tak	tak	k9	tak
tzv.	tzv.	kA	tzv.
Velké	velký	k2eAgNnSc1d1	velké
Brno	Brno	k1gNnSc1	Brno
s	s	k7c7	s
asi	asi	k9	asi
7	[number]	k4	7
<g/>
×	×	k?	×
větší	veliký	k2eAgFnSc7d2	veliký
rozlohou	rozloha	k1gFnSc7	rozloha
a	a	k8xC	a
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
asi	asi	k9	asi
222	[number]	k4	222
tisíc	tisíc	k4xCgInPc2	tisíc
oproti	oproti	k7c3	oproti
původním	původní	k2eAgNnSc6d1	původní
asi	asi	k9	asi
130	[number]	k4	130
tisícům	tisíc	k4xCgInPc3	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
se	se	k3xPyFc4	se
Brno	Brno	k1gNnSc1	Brno
stalo	stát	k5eAaPmAgNnS	stát
zemským	zemský	k2eAgNnSc7d1	zemské
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
země	zem	k1gFnSc2	zem
Moravské	moravský	k2eAgFnSc2d1	Moravská
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
první	první	k4xOgFnSc6	první
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
předtím	předtím	k6eAd1	předtím
bylo	být	k5eAaImAgNnS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Markrabství	markrabství	k1gNnSc2	markrabství
moravského	moravský	k2eAgNnSc2d1	Moravské
<g/>
,	,	kIx,	,
o	o	k7c4	o
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
zemským	zemský	k2eAgNnSc7d1	zemské
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
země	zem	k1gFnSc2	zem
Moravskoslezské	moravskoslezský	k2eAgFnSc2d1	Moravskoslezská
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1936	[number]	k4	1936
a	a	k8xC	a
1940	[number]	k4	1940
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
Svratce	Svratka	k1gFnSc6	Svratka
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
nad	nad	k7c7	nad
Brnem	Brno	k1gNnSc7	Brno
vodní	vodní	k2eAgFnSc2d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
přehrada	přehrada	k1gFnSc1	přehrada
(	(	kIx(	(
<g/>
zpočátku	zpočátku	k6eAd1	zpočátku
nazývána	nazýván	k2eAgFnSc1d1	nazývána
Kníničská	kníničský	k2eAgFnSc1d1	Kníničská
přehrada	přehrada	k1gFnSc1	přehrada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
bylo	být	k5eAaImAgNnS	být
Brno	Brno	k1gNnSc1	Brno
v	v	k7c6	v
předzvěsti	předzvěst	k1gFnSc6	předzvěst
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
německou	německý	k2eAgFnSc7d1	německá
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
26	[number]	k4	26
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
osvobozeno	osvobodit	k5eAaPmNgNnS	osvobodit
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
docházelo	docházet	k5eAaImAgNnS	docházet
od	od	k7c2	od
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
obdobných	obdobný	k2eAgFnPc2d1	obdobná
celostátních	celostátní	k2eAgFnPc2d1	celostátní
snah	snaha	k1gFnPc2	snaha
k	k	k7c3	k
vysídlování	vysídlování	k1gNnSc3	vysídlování
německého	německý	k2eAgNnSc2d1	německé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
komunistickém	komunistický	k2eAgInSc6d1	komunistický
převratu	převrat	k1gInSc6	převrat
z	z	k7c2	z
února	únor	k1gInSc2	únor
1948	[number]	k4	1948
upadl	upadnout	k5eAaPmAgMnS	upadnout
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
tradiční	tradiční	k2eAgInSc4d1	tradiční
politický	politický	k2eAgInSc4d1	politický
a	a	k8xC	a
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
význam	význam	k1gInSc4	význam
Brna	Brno	k1gNnSc2	Brno
jako	jako	k8xS	jako
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zákonem	zákon	k1gInSc7	zákon
o	o	k7c6	o
krajském	krajský	k2eAgNnSc6d1	krajské
zřízení	zřízení	k1gNnSc6	zřízení
přijatým	přijatý	k2eAgInSc7d1	přijatý
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
zákonem	zákon	k1gInSc7	zákon
bylo	být	k5eAaImAgNnS	být
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
zemské	zemský	k2eAgNnSc1d1	zemské
zřízení	zřízení	k1gNnSc1	zřízení
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
krajů	kraj	k1gInPc2	kraj
podřízených	podřízená	k1gFnPc2	podřízená
Praze	Praha	k1gFnSc6	Praha
(	(	kIx(	(
<g/>
s	s	k7c7	s
účinností	účinnost	k1gFnSc7	účinnost
od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krajské	krajský	k2eAgNnSc1d1	krajské
zřízení	zřízení	k1gNnSc1	zřízení
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
nerespektovalo	respektovat	k5eNaImAgNnS	respektovat
hranice	hranice	k1gFnPc4	hranice
historických	historický	k2eAgFnPc2d1	historická
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
stalo	stát	k5eAaPmAgNnS	stát
sídlem	sídlo	k1gNnSc7	sídlo
Brněnského	brněnský	k2eAgInSc2d1	brněnský
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
po	po	k7c6	po
reformě	reforma	k1gFnSc6	reforma
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
sídlem	sídlo	k1gNnSc7	sídlo
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
Brno	Brno	k1gNnSc1	Brno
postavení	postavení	k1gNnSc2	postavení
statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
národních	národní	k2eAgInPc6d1	národní
výborech	výbor	k1gInPc6	výbor
stalo	stát	k5eAaPmAgNnS	stát
znovu	znovu	k6eAd1	znovu
statutárním	statutární	k2eAgNnSc7d1	statutární
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
k	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
tohoto	tento	k3xDgInSc2	tento
statusu	status	k1gInSc2	status
došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
samosprávný	samosprávný	k2eAgInSc1d1	samosprávný
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
krajským	krajský	k2eAgNnSc7d1	krajské
městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
Brno	Brno	k1gNnSc1	Brno
stalo	stát	k5eAaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Brno	Brno	k1gNnSc1	Brno
několikrát	několikrát	k6eAd1	několikrát
rozšiřovalo	rozšiřovat	k5eAaImAgNnS	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
vzniku	vznik	k1gInSc2	vznik
Velkého	velký	k2eAgNnSc2d1	velké
Brna	Brno	k1gNnSc2	Brno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
roky	rok	k1gInPc4	rok
1944	[number]	k4	1944
(	(	kIx(	(
<g/>
připojen	připojen	k2eAgInSc1d1	připojen
městys	městys	k1gInSc1	městys
Líšeň	Líšeň	k1gFnSc1	Líšeň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
(	(	kIx(	(
<g/>
okolí	okolí	k1gNnSc6	okolí
Brněnské	brněnský	k2eAgFnSc2d1	brněnská
přehrady	přehrada	k1gFnSc2	přehrada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1960	[number]	k4	1960
(	(	kIx(	(
<g/>
čtyři	čtyři	k4xCgFnPc1	čtyři
obce	obec	k1gFnPc1	obec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
(	(	kIx(	(
<g/>
osm	osm	k4xCc4	osm
obcí	obec	k1gFnPc2	obec
<g/>
)	)	kIx)	)
a	a	k8xC	a
naposledy	naposledy	k6eAd1	naposledy
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
součástí	součást	k1gFnSc7	součást
města	město	k1gNnSc2	město
stal	stát	k5eAaPmAgInS	stát
Útěchov	Útěchov	k1gInSc1	Útěchov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
město	město	k1gNnSc1	město
strategii	strategie	k1gFnSc4	strategie
zvanou	zvaný	k2eAgFnSc4d1	zvaná
Brno	Brno	k1gNnSc4	Brno
2050	[number]	k4	2050
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
popsalo	popsat	k5eAaPmAgNnS	popsat
vize	vize	k1gFnPc4	vize
do	do	k7c2	do
budoucnosti	budoucnost	k1gFnSc2	budoucnost
ve	v	k7c6	v
třech	tři	k4xCgFnPc6	tři
oblastech	oblast	k1gFnPc6	oblast
<g/>
:	:	kIx,	:
kvalita	kvalita	k1gFnSc1	kvalita
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
městská	městský	k2eAgFnSc1d1	městská
správa	správa	k1gFnSc1	správa
a	a	k8xC	a
zdroje	zdroj	k1gInPc1	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ní	on	k3xPp3gFnSc2	on
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2050	[number]	k4	2050
žít	žít	k5eAaImF	žít
600	[number]	k4	600
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
zde	zde	k6eAd1	zde
růst	růst	k5eAaImF	růst
moderní	moderní	k2eAgFnSc1d1	moderní
architektura	architektura	k1gFnSc1	architektura
<g/>
,	,	kIx,	,
fungovat	fungovat	k5eAaImF	fungovat
spolupráce	spolupráce	k1gFnPc4	spolupráce
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
plánuje	plánovat	k5eAaImIp3nS	plánovat
se	se	k3xPyFc4	se
zavedení	zavedení	k1gNnSc4	zavedení
politiky	politika	k1gFnSc2	politika
otevřených	otevřený	k2eAgNnPc2d1	otevřené
dat	datum	k1gNnPc2	datum
či	či	k8xC	či
participativní	participativní	k2eAgFnSc2d1	participativní
správy	správa	k1gFnSc2	správa
občany	občan	k1gMnPc4	občan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dopravní	dopravní	k2eAgInPc1d1	dopravní
má	mít	k5eAaImIp3nS	mít
jít	jít	k5eAaImF	jít
o	o	k7c4	o
napojení	napojení	k1gNnSc4	napojení
města	město	k1gNnSc2	město
na	na	k7c4	na
vysokorychlostní	vysokorychlostní	k2eAgFnPc4d1	vysokorychlostní
tratě	trať	k1gFnPc4	trať
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc4	zvýšení
provozu	provoz	k1gInSc2	provoz
na	na	k7c6	na
tuřanském	tuřanský	k2eAgNnSc6d1	tuřanské
letišti	letiště	k1gNnSc6	letiště
<g/>
,	,	kIx,	,
vize	vize	k1gFnSc1	vize
dále	daleko	k6eAd2	daleko
počítá	počítat	k5eAaImIp3nS	počítat
např.	např.	kA	např.
s	s	k7c7	s
využíváním	využívání	k1gNnSc7	využívání
obnovitelných	obnovitelný	k2eAgInPc2d1	obnovitelný
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Brno	Brno	k1gNnSc1	Brno
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
řek	řeka	k1gFnPc2	řeka
Svratky	Svratka	k1gFnSc2	Svratka
a	a	k8xC	a
Svitavy	Svitava	k1gFnSc2	Svitava
a	a	k8xC	a
protékají	protékat	k5eAaImIp3nP	protékat
jím	on	k3xPp3gMnSc7	on
potoky	potok	k1gInPc4	potok
Veverka	veverek	k1gMnSc4	veverek
<g/>
,	,	kIx,	,
Ponávka	Ponávka	k1gFnSc1	Ponávka
<g/>
,	,	kIx,	,
Říčka	říčka	k1gFnSc1	říčka
a	a	k8xC	a
několik	několik	k4yIc1	několik
dalších	další	k2eAgInPc2d1	další
menších	malý	k2eAgInPc2d2	menší
toků	tok	k1gInPc2	tok
<g/>
.	.	kIx.	.
</s>
<s>
Tok	tok	k1gInSc1	tok
Svratky	Svratka	k1gFnSc2	Svratka
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
přibližně	přibližně	k6eAd1	přibližně
29	[number]	k4	29
km	km	kA	km
<g/>
,	,	kIx,	,
u	u	k7c2	u
Svitavy	Svitava	k1gFnSc2	Svitava
délka	délka	k1gFnSc1	délka
činí	činit	k5eAaImIp3nS	činit
asi	asi	k9	asi
13	[number]	k4	13
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katastru	katastr	k1gInSc6	katastr
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
přehrada	přehrada	k1gFnSc1	přehrada
a	a	k8xC	a
několik	několik	k4yIc1	několik
malých	malý	k2eAgInPc2d1	malý
rybníků	rybník	k1gInPc2	rybník
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
vodních	vodní	k2eAgFnPc2d1	vodní
ploch	plocha	k1gFnPc2	plocha
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Žebětínský	Žebětínský	k2eAgInSc1d1	Žebětínský
rybník	rybník	k1gInSc1	rybník
nebo	nebo	k8xC	nebo
nádrže	nádrž	k1gFnPc1	nádrž
v	v	k7c6	v
Mariánském	mariánský	k2eAgNnSc6d1	Mariánské
údolí	údolí	k1gNnSc6	údolí
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
stran	strana	k1gFnPc2	strana
chráněno	chránit	k5eAaImNgNnS	chránit
zalesněnými	zalesněný	k2eAgInPc7d1	zalesněný
kopci	kopec	k1gInPc7	kopec
Brněnské	brněnský	k2eAgFnSc2d1	brněnská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
pak	pak	k6eAd1	pak
začínají	začínat	k5eAaImIp3nP	začínat
nížiny	nížina	k1gFnPc1	nížina
Dyjsko-svrateckého	dyjskovratecký	k2eAgInSc2d1	dyjsko-svratecký
úvalu	úval	k1gInSc2	úval
<g/>
,	,	kIx,	,
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
190	[number]	k4	190
<g/>
–	–	k?	–
<g/>
497	[number]	k4	497
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
území	území	k1gNnSc2	území
Brna	Brno	k1gNnSc2	Brno
(	(	kIx(	(
<g/>
6379	[number]	k4	6379
ha	ha	kA	ha
<g/>
,	,	kIx,	,
28	[number]	k4	28
%	%	kIx~	%
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pokryta	pokryt	k2eAgFnSc1d1	pokryta
lesy	les	k1gInPc7	les
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
kopců	kopec	k1gInPc2	kopec
Petrov	Petrov	k1gInSc1	Petrov
a	a	k8xC	a
Špilberk	Špilberk	k1gInSc1	Špilberk
se	se	k3xPyFc4	se
na	na	k7c6	na
katastru	katastr	k1gInSc6	katastr
města	město	k1gNnSc2	město
nacházejí	nacházet	k5eAaImIp3nP	nacházet
také	také	k9	také
vrchy	vrch	k1gInPc4	vrch
Bílá	bílý	k2eAgFnSc1d1	bílá
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Červený	červený	k2eAgInSc1d1	červený
kopec	kopec	k1gInSc1	kopec
<g/>
,	,	kIx,	,
Hády	hádes	k1gInPc1	hádes
(	(	kIx(	(
<g/>
z	z	k7c2	z
části	část	k1gFnSc2	část
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kamenný	kamenný	k2eAgInSc1d1	kamenný
vrch	vrch	k1gInSc1	vrch
<g/>
,	,	kIx,	,
Kraví	kraví	k2eAgFnSc1d1	kraví
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Medlánecké	Medlánecký	k2eAgInPc1d1	Medlánecký
kopce	kopec	k1gInPc1	kopec
<g/>
,	,	kIx,	,
Mniší	mniší	k2eAgFnSc1d1	mniší
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Palackého	Palackého	k2eAgInSc1d1	Palackého
vrch	vrch	k1gInSc1	vrch
<g/>
,	,	kIx,	,
Stránská	Stránská	k1gFnSc1	Stránská
skála	skála	k1gFnSc1	skála
<g/>
,	,	kIx,	,
Strom	strom	k1gInSc1	strom
a	a	k8xC	a
Žlutý	žlutý	k2eAgInSc1d1	žlutý
kopec	kopec	k1gInSc1	kopec
<g/>
,	,	kIx,	,
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
je	být	k5eAaImIp3nS	být
Kopeček	kopeček	k1gInSc1	kopeček
(	(	kIx(	(
<g/>
479,41	[number]	k4	479,41
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
na	na	k7c6	na
katastru	katastr	k1gInSc6	katastr
města	město	k1gNnSc2	město
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
497	[number]	k4	497
m	m	kA	m
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
severně	severně	k6eAd1	severně
od	od	k7c2	od
Útěchova	Útěchův	k2eAgMnSc2d1	Útěchův
<g/>
.	.	kIx.	.
<g/>
Brno	Brno	k1gNnSc1	Brno
má	mít	k5eAaImIp3nS	mít
poměrně	poměrně	k6eAd1	poměrně
rozmanité	rozmanitý	k2eAgNnSc4d1	rozmanité
přírodní	přírodní	k2eAgNnSc4d1	přírodní
zázemí	zázemí	k1gNnSc4	zázemí
a	a	k8xC	a
mírné	mírný	k2eAgNnSc1d1	mírné
klima	klima	k1gNnSc1	klima
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
jeho	jeho	k3xOp3gInSc4	jeho
polohou	poloha	k1gFnSc7	poloha
mezi	mezi	k7c7	mezi
Českomoravskou	českomoravský	k2eAgFnSc7d1	Českomoravská
vrchovinou	vrchovina	k1gFnSc7	vrchovina
a	a	k8xC	a
nížinami	nížina	k1gFnPc7	nížina
jižní	jižní	k2eAgFnSc2d1	jižní
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
9,4	[number]	k4	9,4
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
průměrné	průměrný	k2eAgFnPc1d1	průměrná
roční	roční	k2eAgFnPc1d1	roční
srážky	srážka	k1gFnPc1	srážka
505	[number]	k4	505
mm	mm	kA	mm
<g/>
,	,	kIx,	,
průměrný	průměrný	k2eAgInSc1d1	průměrný
počet	počet	k1gInSc1	počet
dnů	den	k1gInPc2	den
se	s	k7c7	s
srážkami	srážka	k1gFnPc7	srážka
je	být	k5eAaImIp3nS	být
150	[number]	k4	150
<g/>
,	,	kIx,	,
průměrný	průměrný	k2eAgInSc1d1	průměrný
sluneční	sluneční	k2eAgInSc1d1	sluneční
svit	svit	k1gInSc1	svit
za	za	k7c4	za
rok	rok	k1gInSc4	rok
činí	činit	k5eAaImIp3nS	činit
1	[number]	k4	1
771	[number]	k4	771
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
převažující	převažující	k2eAgInSc1d1	převažující
směr	směr	k1gInSc1	směr
větru	vítr	k1gInSc2	vítr
je	být	k5eAaImIp3nS	být
severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
východu	východ	k1gInSc2	východ
na	na	k7c4	na
západ	západ	k1gInSc4	západ
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
21,5	[number]	k4	21,5
km	km	kA	km
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
zabírá	zabírat	k5eAaImIp3nS	zabírat
plochu	plocha	k1gFnSc4	plocha
230	[number]	k4	230
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
katastru	katastr	k1gInSc6	katastr
Brna	Brno	k1gNnSc2	Brno
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
část	část	k1gFnSc1	část
chráněné	chráněný	k2eAgFnSc2d1	chráněná
krajinné	krajinný	k2eAgFnSc2d1	krajinná
oblasti	oblast	k1gFnSc2	oblast
Moravský	moravský	k2eAgInSc1d1	moravský
kras	kras	k1gInSc1	kras
<g/>
,	,	kIx,	,
národní	národní	k2eAgFnSc1d1	národní
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Stránská	Stránská	k1gFnSc1	Stránská
skála	skála	k1gFnSc1	skála
<g/>
,	,	kIx,	,
národní	národní	k2eAgFnSc1d1	národní
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
Hádecká	hádecký	k2eAgFnSc1d1	Hádecká
planinka	planinka	k1gFnSc1	planinka
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgFnSc1d1	přírodní
památka	památka	k1gFnSc1	památka
Bílá	bílý	k2eAgFnSc1d1	bílá
hora	hora	k1gFnSc1	hora
nebo	nebo	k8xC	nebo
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
Bosonožský	Bosonožský	k2eAgInSc4d1	Bosonožský
hájek	hájek	k1gInSc4	hájek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
trvalému	trvalý	k2eAgInSc3d1	trvalý
pobytu	pobyt	k1gInSc3	pobyt
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
přihlášeno	přihlásit	k5eAaPmNgNnS	přihlásit
400	[number]	k4	400
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
žebříčku	žebříček	k1gInSc6	žebříček
největších	veliký	k2eAgNnPc2d3	veliký
měst	město	k1gNnPc2	město
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
řadí	řadit	k5eAaImIp3nS	řadit
na	na	k7c4	na
69	[number]	k4	69
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
stav	stav	k1gInSc1	stav
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
aglomerace	aglomerace	k1gFnSc1	aglomerace
<g/>
,	,	kIx,	,
administrativně	administrativně	k6eAd1	administrativně
vymezená	vymezený	k2eAgFnSc1d1	vymezená
dle	dle	k7c2	dle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
561	[number]	k4	561
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
hodnocení	hodnocení	k1gNnSc2	hodnocení
a	a	k8xC	a
snižování	snižování	k1gNnSc2	snižování
hluku	hluk	k1gInSc2	hluk
obcemi	obec	k1gFnPc7	obec
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Adamov	Adamov	k1gInSc1	Adamov
<g/>
,	,	kIx,	,
Bílovice	Bílovice	k1gInPc1	Bílovice
nad	nad	k7c7	nad
Svitavou	Svitava	k1gFnSc7	Svitava
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
Kuřim	Kuřim	k1gInSc1	Kuřim
<g/>
,	,	kIx,	,
Lelekovice	Lelekovice	k1gFnSc1	Lelekovice
<g/>
,	,	kIx,	,
Modřice	Modřice	k1gFnSc1	Modřice
<g/>
,	,	kIx,	,
Ostopovice	Ostopovice	k1gFnSc1	Ostopovice
<g/>
,	,	kIx,	,
Podolí	Podolí	k1gNnSc1	Podolí
<g/>
,	,	kIx,	,
Popůvky	Popůvka	k1gFnPc1	Popůvka
<g/>
,	,	kIx,	,
Rozdrojovice	Rozdrojovice	k1gFnPc1	Rozdrojovice
<g/>
,	,	kIx,	,
Řícmanice	Řícmanice	k1gFnPc1	Řícmanice
<g/>
,	,	kIx,	,
Šlapanice	Šlapanice	k1gFnPc1	Šlapanice
<g/>
,	,	kIx,	,
Troubsko	Troubsko	k1gNnSc1	Troubsko
má	mít	k5eAaImIp3nS	mít
zhruba	zhruba	k6eAd1	zhruba
450	[number]	k4	450
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotné	samotný	k2eAgFnSc6d1	samotná
brněnské	brněnský	k2eAgFnSc6d1	brněnská
metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
oblasti	oblast	k1gFnSc6	oblast
ale	ale	k8xC	ale
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
600	[number]	k4	600
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ji	on	k3xPp3gFnSc4	on
činí	činit	k5eAaImIp3nS	činit
po	po	k7c6	po
pražské	pražský	k2eAgFnSc6d1	Pražská
a	a	k8xC	a
ostravské	ostravský	k2eAgFnSc3d1	Ostravská
třetí	třetí	k4xOgFnSc7	třetí
největší	veliký	k2eAgFnSc7d3	veliký
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
Eurostatu	Eurostat	k1gInSc2	Eurostat
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
v	v	k7c6	v
širší	široký	k2eAgFnSc6d2	širší
metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
oblasti	oblast	k1gFnSc6	oblast
Brna	Brno	k1gNnSc2	Brno
730	[number]	k4	730
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
celý	celý	k2eAgInSc1d1	celý
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
kraj	kraj	k1gInSc1	kraj
má	mít	k5eAaImIp3nS	mít
asi	asi	k9	asi
1,2	[number]	k4	1,2
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
je	být	k5eAaImIp3nS	být
odvěkou	odvěký	k2eAgFnSc7d1	odvěká
křižovatkou	křižovatka	k1gFnSc7	křižovatka
obchodních	obchodní	k2eAgFnPc2d1	obchodní
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spojovala	spojovat	k5eAaImAgFnS	spojovat
severní	severní	k2eAgFnSc1d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
evropské	evropský	k2eAgFnSc2d1	Evropská
civilizace	civilizace	k1gFnSc2	civilizace
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
bylo	být	k5eAaImAgNnS	být
jediným	jediný	k2eAgNnSc7d1	jediné
právoplatným	právoplatný	k2eAgNnSc7d1	právoplatné
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
autonomní	autonomní	k2eAgFnSc2d1	autonomní
moravské	moravský	k2eAgFnSc2d1	Moravská
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
podunajského	podunajský	k2eAgInSc2d1	podunajský
regionu	region	k1gInSc2	region
historicky	historicky	k6eAd1	historicky
spjato	spjat	k2eAgNnSc1d1	spjato
s	s	k7c7	s
Vídní	Vídeň	k1gFnSc7	Vídeň
<g/>
,	,	kIx,	,
vzdálenou	vzdálený	k2eAgFnSc4d1	vzdálená
110	[number]	k4	110
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
bylo	být	k5eAaImAgNnS	být
Brno	Brno	k1gNnSc1	Brno
známé	známý	k2eAgNnSc1d1	známé
také	také	k9	také
jako	jako	k8xC	jako
významné	významný	k2eAgNnSc1d1	významné
průmyslové	průmyslový	k2eAgNnSc1d1	průmyslové
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
přezdívané	přezdívaný	k2eAgInPc1d1	přezdívaný
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
rakouský	rakouský	k2eAgInSc1d1	rakouský
Manchester	Manchester	k1gInSc1	Manchester
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Stav	stav	k1gInSc1	stav
ovzduší	ovzduší	k1gNnSc2	ovzduší
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
přesahoval	přesahovat	k5eAaImAgMnS	přesahovat
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
počet	počet	k1gInSc1	počet
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yRgFnSc6	který
smí	smět	k5eAaImIp3nS	smět
být	být	k5eAaImF	být
legálně	legálně	k6eAd1	legálně
překročen	překročit	k5eAaPmNgInS	překročit
denní	denní	k2eAgInSc1d1	denní
limit	limit	k1gInSc1	limit
pro	pro	k7c4	pro
polétavý	polétavý	k2eAgInSc4d1	polétavý
prach	prach	k1gInSc4	prach
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
mělo	mít	k5eAaImAgNnS	mít
dlouhodobé	dlouhodobý	k2eAgInPc4d1	dlouhodobý
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
překračováním	překračování	k1gNnSc7	překračování
limitů	limit	k1gInPc2	limit
pro	pro	k7c4	pro
znečištění	znečištění	k1gNnSc4	znečištění
oxidem	oxid	k1gInSc7	oxid
dusičitým	dusičitý	k2eAgInSc7d1	dusičitý
nebo	nebo	k8xC	nebo
rakovinotvorným	rakovinotvorný	k2eAgInSc7d1	rakovinotvorný
benzo-a-pyrenem	benzoyren	k1gInSc7	benzo-a-pyren
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
výrazně	výrazně	k6eAd1	výrazně
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
<g/>
,	,	kIx,	,
čísla	číslo	k1gNnSc2	číslo
koncentrací	koncentrace	k1gFnPc2	koncentrace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
naměřena	naměřit	k5eAaBmNgFnS	naměřit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nižší	nízký	k2eAgFnPc1d2	nižší
o	o	k7c4	o
desítky	desítka	k1gFnPc4	desítka
procent	procento	k1gNnPc2	procento
než	než	k8xS	než
před	před	k7c7	před
šesti	šest	k4xCc7	šest
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Brněnský	brněnský	k2eAgInSc1d1	brněnský
magistrát	magistrát	k1gInSc1	magistrát
vynakládá	vynakládat	k5eAaImIp3nS	vynakládat
na	na	k7c4	na
oblast	oblast	k1gFnSc4	oblast
kultury	kultura	k1gFnSc2	kultura
ročně	ročně	k6eAd1	ročně
kolem	kolem	k7c2	kolem
tří	tři	k4xCgFnPc2	tři
čtvrtin	čtvrtina	k1gFnPc2	čtvrtina
miliardy	miliarda	k4xCgFnSc2	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
působí	působit	k5eAaImIp3nS	působit
řada	řada	k1gFnSc1	řada
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
,	,	kIx,	,
divadel	divadlo	k1gNnPc2	divadlo
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
kulturních	kulturní	k2eAgFnPc2d1	kulturní
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
pořádá	pořádat	k5eAaImIp3nS	pořádat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
řada	řada	k1gFnSc1	řada
festivalů	festival	k1gInPc2	festival
a	a	k8xC	a
nejrůznějších	různý	k2eAgFnPc2d3	nejrůznější
kulturních	kulturní	k2eAgFnPc2d1	kulturní
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
také	také	k9	také
zřízena	zřízen	k2eAgFnSc1d1	zřízena
Hvězdárna	hvězdárna	k1gFnSc1	hvězdárna
a	a	k8xC	a
planetárium	planetárium	k1gNnSc1	planetárium
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc4	tři
botanické	botanický	k2eAgFnPc4d1	botanická
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
například	například	k6eAd1	například
Zoo	zoo	k1gNnSc2	zoo
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
svojí	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
65	[number]	k4	65
ha	ha	kA	ha
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brno	Brno	k1gNnSc1	Brno
od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
zažívá	zažívat	k5eAaImIp3nS	zažívat
kulturní	kulturní	k2eAgInSc1d1	kulturní
"	"	kIx"	"
<g/>
znovuzrození	znovuzrození	k1gNnSc1	znovuzrození
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
fasády	fasáda	k1gFnPc1	fasáda
jsou	být	k5eAaImIp3nP	být
opravovány	opravován	k2eAgFnPc1d1	opravována
<g/>
,	,	kIx,	,
různé	různý	k2eAgFnPc1d1	různá
výstavy	výstava	k1gFnPc1	výstava
<g/>
,	,	kIx,	,
přehlídky	přehlídka	k1gFnPc1	přehlídka
apod.	apod.	kA	apod.
jsou	být	k5eAaImIp3nP	být
zakládány	zakládat	k5eAaImNgFnP	zakládat
a	a	k8xC	a
rozšiřovány	rozšiřován	k2eAgFnPc1d1	rozšiřována
<g/>
,	,	kIx,	,
do	do	k7c2	do
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vrací	vracet	k5eAaImIp3nS	vracet
rytmus	rytmus	k1gInSc1	rytmus
"	"	kIx"	"
<g/>
menšího	malý	k2eAgNnSc2d2	menší
evropského	evropský	k2eAgNnSc2d1	Evropské
velkoměsta	velkoměsto	k1gNnSc2	velkoměsto
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
Brnu	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
někdy	někdy	k6eAd1	někdy
může	moct	k5eAaImIp3nS	moct
dostávat	dostávat	k5eAaImF	dostávat
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
lichotivé	lichotivý	k2eAgFnSc2d1	lichotivá
pověsti	pověst	k1gFnSc2	pověst
"	"	kIx"	"
<g/>
nudného	nudný	k2eAgNnSc2d1	nudné
maloměsta	maloměsto	k1gNnSc2	maloměsto
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
se	se	k3xPyFc4	se
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
konal	konat	k5eAaImAgInS	konat
summit	summit	k1gInSc1	summit
15	[number]	k4	15
prezidentů	prezident	k1gMnPc2	prezident
zemí	zem	k1gFnPc2	zem
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
navštívila	navštívit	k5eAaPmAgFnS	navštívit
Brno	Brno	k1gNnSc4	Brno
britská	britský	k2eAgFnSc1d1	britská
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
její	její	k3xOp3gNnSc1	její
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
princ	princ	k1gMnSc1	princ
Charles	Charles	k1gMnSc1	Charles
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
papež	papež	k1gMnSc1	papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brno	Brno	k1gNnSc1	Brno
i	i	k9	i
přes	přes	k7c4	přes
svůj	svůj	k3xOyFgInSc4	svůj
městský	městský	k2eAgInSc4d1	městský
charakter	charakter	k1gInSc4	charakter
zachovává	zachovávat	k5eAaImIp3nS	zachovávat
tradiční	tradiční	k2eAgFnSc4d1	tradiční
moravskou	moravský	k2eAgFnSc4d1	Moravská
lidovou	lidový	k2eAgFnSc4d1	lidová
kulturu	kultura	k1gFnSc4	kultura
včetně	včetně	k7c2	včetně
lidových	lidový	k2eAgFnPc2d1	lidová
slavností	slavnost	k1gFnPc2	slavnost
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yRgNnPc6	který
nechybí	chybět	k5eNaImIp3nP	chybět
moravské	moravský	k2eAgInPc1d1	moravský
kroje	kroj	k1gInPc1	kroj
<g/>
,	,	kIx,	,
moravská	moravský	k2eAgFnSc1d1	Moravská
vína	vína	k1gFnSc1	vína
(	(	kIx(	(
<g/>
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
vinařskou	vinařský	k2eAgFnSc7d1	vinařská
obcí	obec	k1gFnSc7	obec
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Velkopavlovické	Velkopavlovický	k2eAgFnSc2d1	Velkopavlovická
vinařské	vinařský	k2eAgFnSc2d1	vinařská
podoblasti	podoblast	k1gFnSc2	podoblast
s	s	k7c7	s
několika	několik	k4yIc2	několik
viničními	viniční	k2eAgFnPc7d1	viniční
tratěmi	trať	k1gFnPc7	trať
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
obnovené	obnovený	k2eAgFnSc2d1	obnovená
vinice	vinice	k1gFnSc2	vinice
na	na	k7c6	na
svahu	svah	k1gInSc6	svah
Špilberku	Špilberk	k1gInSc2	Špilberk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
moravská	moravský	k2eAgFnSc1d1	Moravská
dechová	dechový	k2eAgFnSc1d1	dechová
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
lidové	lidový	k2eAgInPc4d1	lidový
tance	tanec	k1gInPc4	tanec
a	a	k8xC	a
soutěže	soutěž	k1gFnPc4	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
menších	malý	k2eAgFnPc2d2	menší
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgInPc4	svůj
hody	hod	k1gInPc4	hod
lokálně	lokálně	k6eAd1	lokálně
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
městské	městský	k2eAgFnPc4d1	městská
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ne	ne	k9	ne
všechny	všechen	k3xTgFnPc1	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
městské	městský	k2eAgFnPc4d1	městská
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pravidelně	pravidelně	k6eAd1	pravidelně
pořádají	pořádat	k5eAaImIp3nP	pořádat
tradiční	tradiční	k2eAgInPc4d1	tradiční
hody	hod	k1gInPc4	hod
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Řečkovice	Řečkovice	k1gFnSc1	Řečkovice
a	a	k8xC	a
Mokrá	mokrý	k2eAgFnSc1d1	mokrá
Hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
Židenice	Židenice	k1gFnSc1	Židenice
<g/>
,	,	kIx,	,
Líšeň	Líšeň	k1gFnSc1	Líšeň
nebo	nebo	k8xC	nebo
Ivanovice	Ivanovice	k1gFnPc1	Ivanovice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
řada	řada	k1gFnSc1	řada
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
kamenů	kámen	k1gInPc2	kámen
zmizelých	zmizelý	k2eAgFnPc2d1	Zmizelá
<g/>
"	"	kIx"	"
-	-	kIx~	-
Stolpersteine	Stolperstein	k1gMnSc5	Stolperstein
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
chodníků	chodník	k1gInPc2	chodník
vsazeny	vsazen	k2eAgInPc4d1	vsazen
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
německým	německý	k2eAgMnSc7d1	německý
umělcem	umělec	k1gMnSc7	umělec
Gunterem	Gunter	k1gMnSc7	Gunter
Demnigem	Demnig	k1gMnSc7	Demnig
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Kulturní	kulturní	k2eAgFnPc1d1	kulturní
památky	památka	k1gFnPc1	památka
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnSc3	jeho
bohaté	bohatý	k2eAgFnSc3d1	bohatá
historii	historie	k1gFnSc3	historie
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
nalézt	nalézt	k5eAaBmF	nalézt
stovky	stovka	k1gFnPc4	stovka
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
devíti	devět	k4xCc2	devět
národních	národní	k2eAgFnPc2d1	národní
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
památky	památka	k1gFnSc2	památka
zařazené	zařazený	k2eAgFnSc2d1	zařazená
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
Světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
hlavních	hlavní	k2eAgFnPc2d1	hlavní
brněnských	brněnský	k2eAgFnPc2d1	brněnská
památek	památka	k1gFnPc2	památka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
historickém	historický	k2eAgNnSc6d1	historické
jádru	jádro	k1gNnSc6	jádro
<g/>
,	,	kIx,	,
zdejší	zdejší	k2eAgFnSc1d1	zdejší
městská	městský	k2eAgFnSc1d1	městská
památková	památkový	k2eAgFnSc1d1	památková
rezervace	rezervace	k1gFnSc1	rezervace
je	být	k5eAaImIp3nS	být
počtem	počet	k1gInSc7	počet
objektů	objekt	k1gInPc2	objekt
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
až	až	k9	až
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
odstupem	odstup	k1gInSc7	odstup
za	za	k7c7	za
Prahou	Praha	k1gFnSc7	Praha
(	(	kIx(	(
<g/>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
484	[number]	k4	484
objektů	objekt	k1gInPc2	objekt
<g/>
;	;	kIx,	;
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
1	[number]	k4	1
330	[number]	k4	330
objektů	objekt	k1gInPc2	objekt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
také	také	k9	také
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xC	jako
kolébka	kolébka	k1gFnSc1	kolébka
některých	některý	k3yIgInPc2	některý
nových	nový	k2eAgInPc2d1	nový
trendů	trend	k1gInPc2	trend
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
moderní	moderní	k2eAgFnSc2d1	moderní
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
její	její	k3xOp3gFnPc1	její
konkrétní	konkrétní	k2eAgFnPc1d1	konkrétní
podoby	podoba	k1gFnPc1	podoba
někdy	někdy	k6eAd1	někdy
nazývané	nazývaný	k2eAgFnPc1d1	nazývaná
též	též	k9	též
jako	jako	k9	jako
funkcionalismus	funkcionalismus	k1gInSc4	funkcionalismus
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
profil	profil	k1gInSc1	profil
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
a	a	k8xC	a
oceňovaný	oceňovaný	k2eAgMnSc1d1	oceňovaný
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
pomohl	pomoct	k5eAaPmAgInS	pomoct
vtisknout	vtisknout	k5eAaPmF	vtisknout
zejména	zejména	k9	zejména
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Fuchs	Fuchs	k1gMnSc1	Fuchs
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
respektovaných	respektovaný	k2eAgMnPc2d1	respektovaný
architektů	architekt	k1gMnPc2	architekt
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Podílela	podílet	k5eAaImAgFnS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
ale	ale	k9	ale
i	i	k9	i
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgMnPc2d1	další
tvůrců	tvůrce	k1gMnPc2	tvůrce
jako	jako	k9	jako
Arnošt	Arnošt	k1gMnSc1	Arnošt
Wiesner	Wiesner	k1gMnSc1	Wiesner
<g/>
,	,	kIx,	,
Emil	Emil	k1gMnSc1	Emil
Králík	Králík	k1gMnSc1	Králík
<g/>
,	,	kIx,	,
Jindřich	Jindřich	k1gMnSc1	Jindřich
Kumpošt	Kumpošt	k1gMnSc1	Kumpošt
<g/>
,	,	kIx,	,
Otto	Otto	k1gMnSc1	Otto
Eisler	Eisler	k1gMnSc1	Eisler
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
možné	možný	k2eAgNnSc1d1	možné
vidět	vidět	k5eAaImF	vidět
velmi	velmi	k6eAd1	velmi
pestrou	pestrý	k2eAgFnSc4d1	pestrá
řadu	řada	k1gFnSc4	řada
památek	památka	k1gFnPc2	památka
tohoto	tento	k3xDgInSc2	tento
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
seznam	seznam	k1gInSc1	seznam
uvádí	uvádět	k5eAaImIp3nS	uvádět
pouze	pouze	k6eAd1	pouze
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdičkou	hvězdička	k1gFnSc7	hvězdička
označené	označený	k2eAgInPc1d1	označený
objekty	objekt	k1gInPc1	objekt
jsou	být	k5eAaImIp3nP	být
národními	národní	k2eAgFnPc7d1	národní
kulturními	kulturní	k2eAgFnPc7d1	kulturní
památkami	památka	k1gFnPc7	památka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Místní	místní	k2eAgInPc4d1	místní
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
festivaly	festival	k1gInPc4	festival
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
každoročně	každoročně	k6eAd1	každoročně
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
května	květen	k1gInSc2	květen
a	a	k8xC	a
června	červen	k1gInSc2	červen
pořádán	pořádán	k2eAgInSc1d1	pořádán
festival	festival	k1gInSc1	festival
zábavy	zábava	k1gFnSc2	zábava
Brno	Brno	k1gNnSc1	Brno
–	–	k?	–
město	město	k1gNnSc1	město
uprostřed	uprostřed	k7c2	uprostřed
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
přehlídka	přehlídka	k1gFnSc1	přehlídka
a	a	k8xC	a
možnost	možnost	k1gFnSc1	možnost
jízdy	jízda	k1gFnSc2	jízda
starými	starý	k2eAgInPc7d1	starý
dopravními	dopravní	k2eAgInPc7d1	dopravní
prostředky	prostředek	k1gInPc7	prostředek
Dopravní	dopravní	k2eAgFnSc2d1	dopravní
nostalgie	nostalgie	k1gFnSc2	nostalgie
<g/>
,	,	kIx,	,
Zábava	zábava	k1gFnSc1	zábava
pod	pod	k7c7	pod
hradbami	hradba	k1gFnPc7	hradba
hradu	hrad	k1gInSc2	hrad
Špilberk	Špilberk	k1gInSc4	Špilberk
s	s	k7c7	s
koncertem	koncert	k1gInSc7	koncert
brněnské	brněnský	k2eAgFnSc2d1	brněnská
filharmonie	filharmonie	k1gFnSc2	filharmonie
<g/>
,	,	kIx,	,
lidová	lidový	k2eAgFnSc1d1	lidová
hradní	hradní	k2eAgFnSc1d1	hradní
slavnost	slavnost	k1gFnSc1	slavnost
na	na	k7c6	na
témže	týž	k3xTgInSc6	týž
hradu	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
tohoto	tento	k3xDgInSc2	tento
festivalu	festival	k1gInSc2	festival
je	být	k5eAaImIp3nS	být
i	i	k9	i
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
soutěžní	soutěžní	k2eAgFnSc1d1	soutěžní
přehlídka	přehlídka	k1gFnSc1	přehlídka
ohňostrojů	ohňostroj	k1gInPc2	ohňostroj
Ignis	Ignis	k1gFnSc1	Ignis
Brunensis	Brunensis	k1gFnSc1	Brunensis
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
"	"	kIx"	"
<g/>
Plamen	plamen	k1gInSc1	plamen
Brněnský	brněnský	k2eAgInSc1d1	brněnský
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
největší	veliký	k2eAgNnSc4d3	veliký
soutěžní	soutěžní	k2eAgNnSc4d1	soutěžní
show	show	k1gNnSc4	show
ohňostrojů	ohňostroj	k1gInPc2	ohňostroj
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
června	červen	k1gInSc2	červen
do	do	k7c2	do
září	září	k1gNnSc2	září
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
Brněnské	brněnský	k2eAgNnSc4d1	brněnské
kulturní	kulturní	k2eAgNnSc4d1	kulturní
léto	léto	k1gNnSc4	léto
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
divadelní	divadelní	k2eAgInSc4d1	divadelní
festival	festival	k1gInSc4	festival
Brněnské	brněnský	k2eAgFnSc2d1	brněnská
shakespearovské	shakespearovský	k2eAgFnSc2d1	shakespearovská
hry	hra	k1gFnSc2	hra
<g/>
;	;	kIx,	;
ukončují	ukončovat	k5eAaImIp3nP	ukončovat
je	on	k3xPp3gFnPc4	on
folklorní	folklorní	k2eAgFnPc4d1	folklorní
Brněnské	brněnský	k2eAgFnPc4d1	brněnská
lidové	lidový	k2eAgFnPc4d1	lidová
slavnosti	slavnost	k1gFnPc4	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
konají	konat	k5eAaImIp3nP	konat
Dny	dna	k1gFnPc1	dna
Brna	Brno	k1gNnSc2	Brno
–	–	k?	–
oslavy	oslava	k1gFnSc2	oslava
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
obrany	obrana	k1gFnSc2	obrana
města	město	k1gNnSc2	město
před	před	k7c7	před
Švédy	Švéd	k1gMnPc7	Švéd
roku	rok	k1gInSc2	rok
1645	[number]	k4	1645
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
součástí	součást	k1gFnSc7	součást
jsou	být	k5eAaImIp3nP	být
i	i	k8xC	i
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
dobových	dobový	k2eAgFnPc2d1	dobová
bitevních	bitevní	k2eAgFnPc2d1	bitevní
scén	scéna	k1gFnPc2	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
koná	konat	k5eAaImIp3nS	konat
svátek	svátek	k1gInSc1	svátek
balónového	balónový	k2eAgNnSc2d1	balónové
létání	létání	k1gNnSc2	létání
Baloon	Baloon	k1gNnSc1	Baloon
Jam	jam	k1gInSc1	jam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
také	také	k9	také
pořádá	pořádat	k5eAaImIp3nS	pořádat
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
festival	festival	k1gInSc1	festival
Divadelní	divadelní	k2eAgInSc1d1	divadelní
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
zahájen	zahájen	k2eAgInSc1d1	zahájen
Nocí	noc	k1gFnSc7	noc
kejklířů	kejklíř	k1gMnPc2	kejklíř
a	a	k8xC	a
vrcholící	vrcholící	k2eAgFnSc7d1	vrcholící
Slavností	slavnost	k1gFnSc7	slavnost
masek	maska	k1gFnPc2	maska
<g/>
,	,	kIx,	,
Divadelní	divadelní	k2eAgInSc1d1	divadelní
svět	svět	k1gInSc1	svět
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
podobnou	podobný	k2eAgFnSc7d1	podobná
akcí	akce	k1gFnSc7	akce
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
dva	dva	k4xCgInPc1	dva
výroční	výroční	k2eAgInPc1d1	výroční
festivaly	festival	k1gInPc1	festival
symfonické	symfonický	k2eAgFnSc2d1	symfonická
hudby	hudba	k1gFnSc2	hudba
–	–	k?	–
Mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
hudební	hudební	k2eAgInSc4d1	hudební
festival	festival	k1gInSc4	festival
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
pod	pod	k7c7	pod
tímto	tento	k3xDgNnSc7	tento
jménem	jméno	k1gNnSc7	jméno
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
a	a	k8xC	a
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
Špilberk	Špilberk	k1gInSc1	Špilberk
pod	pod	k7c7	pod
širým	širý	k2eAgNnSc7d1	širé
nebem	nebe	k1gNnSc7	nebe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
100	[number]	k4	100
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
světové	světový	k2eAgFnSc2d1	světová
premiéry	premiéra	k1gFnSc2	premiéra
opery	opera	k1gFnSc2	opera
Leoše	Leoš	k1gMnSc2	Leoš
Janáčka	Janáček	k1gMnSc2	Janáček
Její	její	k3xOp3gNnSc1	její
pastorkyňa	pastorkyňa	k1gFnSc1	pastorkyňa
v	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
divadle	divadlo	k1gNnSc6	divadlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
první	první	k4xOgInSc4	první
ročník	ročník	k1gInSc4	ročník
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
hudebního	hudební	k2eAgInSc2d1	hudební
festivalu	festival	k1gInSc2	festival
Janáček	Janáček	k1gMnSc1	Janáček
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Multižánrový	Multižánrový	k2eAgInSc1d1	Multižánrový
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
Semtex	semtex	k1gInSc1	semtex
Culture	Cultur	k1gMnSc5	Cultur
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
populární	populární	k2eAgFnSc3d1	populární
hudbě	hudba	k1gFnSc3	hudba
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
Alternativa	alternativa	k1gFnSc1	alternativa
Brno	Brno	k1gNnSc1	Brno
experimentální	experimentální	k2eAgFnSc1d1	experimentální
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
v	v	k7c6	v
Mariánském	mariánský	k2eAgNnSc6d1	Mariánské
údolí	údolí	k1gNnSc6	údolí
konaly	konat	k5eAaImAgFnP	konat
Mírové	mírový	k2eAgFnPc1d1	mírová
slavnosti	slavnost	k1gFnPc1	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Častým	častý	k2eAgNnSc7d1	časté
místem	místo	k1gNnSc7	místo
konání	konání	k1gNnSc2	konání
hudebních	hudební	k2eAgFnPc2d1	hudební
i	i	k8xC	i
jiných	jiný	k2eAgFnPc2d1	jiná
produkcí	produkce	k1gFnPc2	produkce
jsou	být	k5eAaImIp3nP	být
hala	halo	k1gNnPc1	halo
Rondo	rondo	k1gNnSc1	rondo
(	(	kIx(	(
<g/>
DRFG	DRFG	kA	DRFG
Arena	Arena	k1gFnSc1	Arena
<g/>
)	)	kIx)	)
či	či	k8xC	či
náměstí	náměstí	k1gNnSc1	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
v	v	k7c6	v
brněnském	brněnský	k2eAgNnSc6d1	brněnské
centru	centrum	k1gNnSc6	centrum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
také	také	k9	také
konal	konat	k5eAaImAgInS	konat
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
Cinema	Cinema	k1gNnSc4	Cinema
Mundi	Mund	k1gMnPc1	Mund
<g/>
,	,	kIx,	,
přehlídka	přehlídka	k1gFnSc1	přehlídka
filmů	film	k1gInPc2	film
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
ucházejí	ucházet	k5eAaImIp3nP	ucházet
či	či	k8xC	či
ucházely	ucházet	k5eAaImAgFnP	ucházet
o	o	k7c4	o
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
zde	zde	k6eAd1	zde
probíhá	probíhat	k5eAaImIp3nS	probíhat
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
s	s	k7c7	s
gay	gay	k1gMnSc1	gay
a	a	k8xC	a
lesbickou	lesbický	k2eAgFnSc7d1	lesbická
tematikou	tematika	k1gFnSc7	tematika
Mezipatra	mezipatro	k1gNnSc2	mezipatro
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Duha	duha	k1gFnSc1	duha
nad	nad	k7c7	nad
Brnem	Brno	k1gNnSc7	Brno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Muzea	muzeum	k1gNnSc2	muzeum
a	a	k8xC	a
knihovny	knihovna	k1gFnSc2	knihovna
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
muzeem	muzeum	k1gNnSc7	muzeum
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
Moravské	moravský	k2eAgNnSc1d1	Moravské
zemské	zemský	k2eAgNnSc1d1	zemské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc7	druhý
nejstarší	starý	k2eAgFnSc7d3	nejstarší
a	a	k8xC	a
nejrozsáhlejší	rozsáhlý	k2eAgFnSc7d3	nejrozsáhlejší
muzejní	muzejní	k2eAgFnSc7d1	muzejní
institucí	instituce	k1gFnSc7	instituce
v	v	k7c6	v
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
první	první	k4xOgMnSc1	první
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
Hospodářskou	hospodářský	k2eAgFnSc7d1	hospodářská
společností	společnost	k1gFnSc7	společnost
pro	pro	k7c4	pro
povznesení	povznesení	k1gNnSc4	povznesení
orby	orba	k1gFnSc2	orba
<g/>
,	,	kIx,	,
přírodovědy	přírodověda	k1gFnSc2	přírodověda
a	a	k8xC	a
vlastivědy	vlastivěda	k1gFnSc2	vlastivěda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1817	[number]	k4	1817
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
Biskupský	biskupský	k2eAgInSc1d1	biskupský
dvůr	dvůr	k1gInSc1	dvůr
na	na	k7c6	na
Zelném	zelný	k2eAgInSc6d1	zelný
trhu	trh	k1gInSc6	trh
převeden	převést	k5eAaPmNgInS	převést
církví	církev	k1gFnSc7	církev
na	na	k7c6	na
tuto	tento	k3xDgFnSc4	tento
společnost	společnost	k1gFnSc4	společnost
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
instalace	instalace	k1gFnSc1	instalace
byly	být	k5eAaImAgFnP	být
provedeny	provést	k5eAaPmNgFnP	provést
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1828	[number]	k4	1828
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
bylo	být	k5eAaImAgNnS	být
přistavěno	přistavěn	k2eAgNnSc1d1	přistavěno
tzv.	tzv.	kA	tzv.
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Elvertovo	Elvertův	k2eAgNnSc4d1	Elvertův
křídlo	křídlo	k1gNnSc4	křídlo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
dvůr	dvůr	k1gInSc4	dvůr
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
byl	být	k5eAaImAgMnS	být
zakoupen	zakoupit	k5eAaPmNgMnS	zakoupit
i	i	k8xC	i
sousední	sousední	k2eAgInSc1d1	sousední
Dietrichsteinský	Dietrichsteinský	k2eAgInSc1d1	Dietrichsteinský
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
muzeum	muzeum	k1gNnSc1	muzeum
bylo	být	k5eAaImAgNnS	být
rekonstruováno	rekonstruovat	k5eAaBmNgNnS	rekonstruovat
a	a	k8xC	a
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
uzavřeno	uzavřen	k2eAgNnSc1d1	uzavřeno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
znovuotevření	znovuotevření	k1gNnSc6	znovuotevření
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
muzeum	muzeum	k1gNnSc1	muzeum
fungovalo	fungovat	k5eAaImAgNnS	fungovat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
opět	opět	k6eAd1	opět
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
bylo	být	k5eAaImAgNnS	být
muzeum	muzeum	k1gNnSc1	muzeum
postátněno	postátnit	k5eAaPmNgNnS	postátnit
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
byly	být	k5eAaImAgFnP	být
objeveny	objeven	k2eAgFnPc1d1	objevena
části	část	k1gFnPc1	část
středověkých	středověký	k2eAgFnPc2d1	středověká
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
velkolepé	velkolepý	k2eAgFnPc1d1	velkolepá
oslavy	oslava	k1gFnPc1	oslava
150	[number]	k4	150
let	léto	k1gNnPc2	léto
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgInP	být
přikoupeny	přikoupit	k5eAaPmNgInP	přikoupit
a	a	k8xC	a
pro	pro	k7c4	pro
muzejní	muzejní	k2eAgInPc4d1	muzejní
účely	účel	k1gInPc4	účel
rekonstruovány	rekonstruovat	k5eAaBmNgFnP	rekonstruovat
další	další	k2eAgFnPc1d1	další
budovy	budova	k1gFnPc1	budova
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Palác	palác	k1gInSc4	palác
šlechtičen	šlechtična	k1gFnPc2	šlechtična
<g/>
,	,	kIx,	,
či	či	k8xC	či
nově	nově	k6eAd1	nově
postavený	postavený	k2eAgInSc4d1	postavený
pavilon	pavilon	k1gInSc4	pavilon
Anthropos	Anthroposa	k1gFnPc2	Anthroposa
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
rok	rok	k1gInSc1	rok
2018	[number]	k4	2018
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
oslav	oslava	k1gFnPc2	oslava
200	[number]	k4	200
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
založení	založení	k1gNnSc2	založení
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
dále	daleko	k6eAd2	daleko
sídlí	sídlet	k5eAaImIp3nS	sídlet
Muzeum	muzeum	k1gNnSc1	muzeum
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
na	na	k7c6	na
hradě	hrad	k1gInSc6	hrad
Špilberk	Špilberk	k1gInSc1	Špilberk
<g/>
,	,	kIx,	,
Technické	technický	k2eAgNnSc1d1	technické
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
založené	založený	k2eAgFnSc2d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
romské	romský	k2eAgFnSc2d1	romská
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
českého	český	k2eAgInSc2d1	český
a	a	k8xC	a
slovenského	slovenský	k2eAgInSc2d1	slovenský
exilu	exil	k1gInSc2	exil
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
Diecézní	diecézní	k2eAgNnSc1d1	diecézní
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
Mendelovo	Mendelův	k2eAgNnSc1d1	Mendelovo
muzeum	muzeum	k1gNnSc1	muzeum
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgNnSc4	první
celouniverzitní	celouniverzitní	k2eAgNnSc4d1	celouniverzitní
muzeum	muzeum	k1gNnSc4	muzeum
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
propaguje	propagovat	k5eAaImIp3nS	propagovat
velikána	velikán	k1gMnSc4	velikán
světové	světový	k2eAgFnSc2d1	světová
vědy	věda	k1gFnSc2	věda
Gregora	Gregor	k1gMnSc2	Gregor
Mendela	Mendel	k1gMnSc2	Mendel
<g/>
;	;	kIx,	;
původně	původně	k6eAd1	původně
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnPc2	součást
Moravského	moravský	k2eAgNnSc2d1	Moravské
zemského	zemský	k2eAgNnSc2d1	zemské
muzea	muzeum	k1gNnSc2	muzeum
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Mendelianum	Mendelianum	k1gInSc1	Mendelianum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
sídlí	sídlet	k5eAaImIp3nS	sídlet
Moravská	moravský	k2eAgFnSc1d1	Moravská
zemská	zemský	k2eAgFnSc1d1	zemská
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
,	,	kIx,	,
přední	přední	k2eAgFnSc1d1	přední
odborná	odborný	k2eAgFnSc1d1	odborná
veřejná	veřejný	k2eAgFnSc1d1	veřejná
knihovna	knihovna	k1gFnSc1	knihovna
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
s	s	k7c7	s
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
miliony	milion	k4xCgInPc4	milion
dokumentů	dokument	k1gInPc2	dokument
druhá	druhý	k4xOgFnSc1	druhý
největší	veliký	k2eAgFnSc1d3	veliký
knihovna	knihovna	k1gFnSc1	knihovna
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
městská	městský	k2eAgFnSc1d1	městská
veřejná	veřejný	k2eAgFnSc1d1	veřejná
knihovna	knihovna	k1gFnSc1	knihovna
Jiřího	Jiří	k1gMnSc2	Jiří
Mahena	Mahen	k1gMnSc2	Mahen
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
poboček	pobočka	k1gFnPc2	pobočka
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgFnPc2d1	další
menších	malý	k2eAgFnPc2d2	menší
knihoven	knihovna	k1gFnPc2	knihovna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Umělecké	umělecký	k2eAgFnPc1d1	umělecká
galerie	galerie	k1gFnPc1	galerie
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
uměleckou	umělecký	k2eAgFnSc7d1	umělecká
galerií	galerie	k1gFnSc7	galerie
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
Moravská	moravský	k2eAgFnSc1d1	Moravská
galerie	galerie	k1gFnSc1	galerie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
druhým	druhý	k4xOgMnSc7	druhý
největším	veliký	k2eAgMnSc7d3	veliký
muzeem	muzeum	k1gNnSc7	muzeum
umění	umění	k1gNnSc2	umění
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Galerie	galerie	k1gFnSc1	galerie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
uměním	umění	k1gNnSc7	umění
v	v	k7c6	v
celé	celá	k1gFnSc6	celá
jeho	jeho	k3xOp3gFnSc6	jeho
šíři	šíř	k1gFnSc6	šíř
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
tradice	tradice	k1gFnSc1	tradice
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1818	[number]	k4	1818
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
Františkovo	Františkův	k2eAgNnSc1d1	Františkovo
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1829	[number]	k4	1829
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
umělecký	umělecký	k2eAgInSc1d1	umělecký
spolek	spolek	k1gInSc1	spolek
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
nakupoval	nakupovat	k5eAaBmAgMnS	nakupovat
první	první	k4xOgNnPc4	první
umělecká	umělecký	k2eAgNnPc4d1	umělecké
díla	dílo	k1gNnPc4	dílo
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
galerii	galerie	k1gFnSc4	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
galerii	galerie	k1gFnSc3	galerie
patří	patřit	k5eAaImIp3nS	patřit
Pražákův	Pražákův	k2eAgInSc1d1	Pražákův
palác	palác	k1gInSc1	palác
<g/>
,	,	kIx,	,
Jurkovičova	Jurkovičův	k2eAgFnSc1d1	Jurkovičova
vila	vila	k1gFnSc1	vila
v	v	k7c6	v
Žabovřeskách	Žabovřesky	k1gFnPc6	Žabovřesky
a	a	k8xC	a
sbírky	sbírka	k1gFnSc2	sbírka
v	v	k7c6	v
Uměleckoprůmyslovém	uměleckoprůmyslový	k2eAgNnSc6d1	Uměleckoprůmyslové
muzeu	muzeum	k1gNnSc6	muzeum
či	či	k8xC	či
Místodržitelském	místodržitelský	k2eAgInSc6d1	místodržitelský
paláci	palác	k1gInSc6	palác
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vídeňským	vídeňský	k2eAgNnSc7d1	Vídeňské
Muzeem	muzeum	k1gNnSc7	muzeum
užitého	užitý	k2eAgNnSc2d1	užité
umění	umění	k1gNnSc2	umění
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
a	a	k8xC	a
spravuje	spravovat	k5eAaImIp3nS	spravovat
rodný	rodný	k2eAgInSc1d1	rodný
dům	dům	k1gInSc1	dům
Josefa	Josef	k1gMnSc2	Josef
Hoffmanna	Hoffmann	k1gMnSc2	Hoffmann
v	v	k7c6	v
Brtnici	Brtnice	k1gFnSc6	Brtnice
u	u	k7c2	u
Jihlavy	Jihlava	k1gFnSc2	Jihlava
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
pořádá	pořádat	k5eAaImIp3nS	pořádat
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
bienále	bienále	k1gNnSc1	bienále
grafického	grafický	k2eAgInSc2d1	grafický
designu	design	k1gInSc2	design
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
MG	mg	kA	mg
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
sbírky	sbírka	k1gFnPc4	sbírka
obrazů	obraz	k1gInPc2	obraz
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
i	i	k9	i
obrazy	obraz	k1gInPc4	obraz
Rubense	Rubense	k1gFnSc2	Rubense
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírky	sbírka	k1gFnPc4	sbírka
rukopisů	rukopis	k1gInPc2	rukopis
a	a	k8xC	a
dávných	dávný	k2eAgNnPc2d1	dávné
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
sbírky	sbírka	k1gFnPc1	sbírka
moderního	moderní	k2eAgNnSc2d1	moderní
a	a	k8xC	a
současného	současný	k2eAgNnSc2d1	současné
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Jan	Jan	k1gMnSc1	Jan
Zrzavý	zrzavý	k2eAgMnSc1d1	zrzavý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírky	sbírka	k1gFnPc1	sbírka
užitého	užitý	k2eAgNnSc2d1	užité
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Dušan	Dušan	k1gMnSc1	Dušan
Jurkovič	Jurkovič	k1gMnSc1	Jurkovič
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírky	sbírka	k1gFnPc4	sbírka
grafického	grafický	k2eAgInSc2d1	grafický
designu	design	k1gInSc2	design
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Aleš	Aleš	k1gMnSc1	Aleš
Najbrt	Najbrt	k1gInSc1	Najbrt
<g/>
,	,	kIx,	,
Alfons	Alfons	k1gMnSc1	Alfons
Mucha	Mucha	k1gMnSc1	Mucha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírku	sbírka	k1gFnSc4	sbírka
fotografií	fotografia	k1gFnPc2	fotografia
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Josef	Josef	k1gMnSc1	Josef
Sudek	sudka	k1gFnPc2	sudka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírku	sbírka	k1gFnSc4	sbírka
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
pro	pro	k7c4	pro
nevidomé	nevidomý	k1gMnPc4	nevidomý
a	a	k8xC	a
slabozraké	slabozraký	k1gMnPc4	slabozraký
a	a	k8xC	a
sbírku	sbírka	k1gFnSc4	sbírka
bibliofilií	bibliofilie	k1gFnPc2	bibliofilie
a	a	k8xC	a
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
knižních	knižní	k2eAgFnPc2d1	knižní
vazeb	vazba	k1gFnPc2	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Moravská	moravský	k2eAgFnSc1d1	Moravská
galerie	galerie	k1gFnSc1	galerie
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
se	s	k7c7	s
jmény	jméno	k1gNnPc7	jméno
jako	jako	k8xS	jako
František	František	k1gMnSc1	František
Franc	Franc	k1gMnSc1	Franc
<g/>
,	,	kIx,	,
Dagmar	Dagmar	k1gFnSc1	Dagmar
Halasová	Halasová	k1gFnSc1	Halasová
<g/>
,	,	kIx,	,
Vlasta	Vlasta	k1gFnSc1	Vlasta
Kratinová	Kratinová	k1gFnSc1	Kratinová
<g/>
,	,	kIx,	,
Albert	Albert	k1gMnSc1	Albert
Kutal	kutat	k5eAaImAgMnS	kutat
a	a	k8xC	a
August	August	k1gMnSc1	August
Prokop	Prokop	k1gMnSc1	Prokop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalšími	další	k2eAgFnPc7d1	další
galeriemi	galerie	k1gFnPc7	galerie
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
jsou	být	k5eAaImIp3nP	být
Dům	dům	k1gInSc1	dům
umění	umění	k1gNnSc2	umění
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
i	i	k9	i
Galerie	galerie	k1gFnSc1	galerie
G	G	kA	G
<g/>
99	[number]	k4	99
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
DUMB	DUMB	kA	DUMB
jsou	být	k5eAaImIp3nP	být
pořádány	pořádán	k2eAgFnPc1d1	pořádána
výstavy	výstava	k1gFnPc1	výstava
tzv.	tzv.	kA	tzv.
mladého	mladý	k2eAgNnSc2d1	mladé
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
instituce	instituce	k1gFnSc1	instituce
spolupořádá	spolupořádat	k5eAaImIp3nS	spolupořádat
Cenu	cena	k1gFnSc4	cena
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Chalupeckého	Chalupecký	k2eAgMnSc2d1	Chalupecký
pro	pro	k7c4	pro
umělce	umělec	k1gMnSc4	umělec
mladší	mladý	k2eAgFnSc2d2	mladší
než	než	k8xS	než
35	[number]	k4	35
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
DUMB	DUMB	kA	DUMB
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c4	na
Malinovského	Malinovský	k2eAgMnSc4d1	Malinovský
náměstí	náměstí	k1gNnSc6	náměstí
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
podoby	podoba	k1gFnSc2	podoba
Bohuslavem	Bohuslav	k1gMnSc7	Bohuslav
Fuchsem	Fuchs	k1gMnSc7	Fuchs
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
dostavění	dostavění	k1gNnSc2	dostavění
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
sloužila	sloužit	k5eAaImAgFnS	sloužit
pro	pro	k7c4	pro
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
jako	jako	k8xS	jako
německý	německý	k2eAgInSc1d1	německý
Künstlerhaus	Künstlerhaus	k1gInSc1	Künstlerhaus
(	(	kIx(	(
<g/>
dům	dům	k1gInSc1	dům
umění	umění	k1gNnSc2	umění
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
jako	jako	k9	jako
výstavní	výstavní	k2eAgInSc4d1	výstavní
sál	sál	k1gInSc4	sál
mladých	mladý	k2eAgMnPc2d1	mladý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
centrům	centr	k1gInPc3	centr
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
patří	patřit	k5eAaImIp3nS	patřit
galerie	galerie	k1gFnSc1	galerie
Brněnského	brněnský	k2eAgNnSc2d1	brněnské
kulturního	kulturní	k2eAgNnSc2d1	kulturní
centra	centrum	k1gNnSc2	centrum
(	(	kIx(	(
<g/>
Galerie	galerie	k1gFnSc1	galerie
mladých	mladý	k1gMnPc2	mladý
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
kabinet	kabinet	k1gInSc1	kabinet
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
U	u	k7c2	u
dobrého	dobrý	k2eAgMnSc2d1	dobrý
pastýře	pastýř	k1gMnSc2	pastýř
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Wannieck	Wannieck	k1gMnSc1	Wannieck
Gallery	Galler	k1gMnPc7	Galler
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
zrekonstruované	zrekonstruovaný	k2eAgFnSc2d1	zrekonstruovaná
Vaňkovky	Vaňkovka	k1gFnSc2	Vaňkovka
pod	pod	k7c7	pod
patronátem	patronát	k1gInSc7	patronát
Richarda	Richard	k1gMnSc2	Richard
Adama	Adam	k1gMnSc2	Adam
a	a	k8xC	a
letohrádek	letohrádek	k1gInSc4	letohrádek
Mitrovských	Mitrovský	k2eAgFnPc2d1	Mitrovská
<g/>
,	,	kIx,	,
místo	místo	k1gNnSc4	místo
konání	konání	k1gNnSc2	konání
výstav	výstava	k1gFnPc2	výstava
<g/>
,	,	kIx,	,
doprovodných	doprovodný	k2eAgFnPc2d1	doprovodná
akcí	akce	k1gFnPc2	akce
k	k	k7c3	k
veletrhům	veletrh	k1gInPc3	veletrh
a	a	k8xC	a
společenských	společenský	k2eAgFnPc2d1	společenská
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Divadla	divadlo	k1gNnSc2	divadlo
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
můžeme	moct	k5eAaImIp1nP	moct
nalézt	nalézt	k5eAaPmF	nalézt
nejstarší	starý	k2eAgFnSc4d3	nejstarší
divadelní	divadelní	k2eAgFnSc4d1	divadelní
budovu	budova	k1gFnSc4	budova
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
je	být	k5eAaImIp3nS	být
divadlo	divadlo	k1gNnSc1	divadlo
Reduta	reduta	k1gFnSc1	reduta
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
zvaném	zvaný	k2eAgNnSc6d1	zvané
Zelný	zelný	k2eAgInSc4d1	zelný
trh	trh	k1gInSc4	trh
<g/>
.	.	kIx.	.
</s>
<s>
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
tradice	tradice	k1gFnSc1	tradice
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
má	mít	k5eAaImIp3nS	mít
mít	mít	k5eAaImF	mít
kořeny	kořen	k1gInPc4	kořen
až	až	k9	až
do	do	k7c2	do
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
městské	městský	k2eAgFnSc6d1	městská
taverně	taverna	k1gFnSc6	taverna
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Redutě	reduta	k1gFnSc6	reduta
<g/>
,	,	kIx,	,
odehrávaly	odehrávat	k5eAaImAgFnP	odehrávat
občasné	občasný	k2eAgFnPc1d1	občasná
divadelní	divadelní	k2eAgFnPc1d1	divadelní
produkce	produkce	k1gFnPc1	produkce
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
již	již	k6eAd1	již
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
vlastní	vlastní	k2eAgNnSc4d1	vlastní
divadlo	divadlo	k1gNnSc4	divadlo
lóžového	lóžový	k2eAgInSc2d1	lóžový
typu	typ	k1gInSc2	typ
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
komplexu	komplex	k1gInSc6	komplex
vystavěno	vystavěn	k2eAgNnSc4d1	vystavěno
až	až	k9	až
roku	rok	k1gInSc2	rok
1733	[number]	k4	1733
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
doložené	doložená	k1gFnPc1	doložená
profesionální	profesionální	k2eAgFnPc1d1	profesionální
a	a	k8xC	a
česky	česky	k6eAd1	česky
hrané	hraný	k2eAgNnSc1d1	hrané
představení	představení	k1gNnSc1	představení
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
roku	rok	k1gInSc2	rok
1767	[number]	k4	1767
opět	opět	k6eAd1	opět
v	v	k7c6	v
Redutě	reduta	k1gFnSc6	reduta
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c6	o
představení	představení	k1gNnSc6	představení
Zamilovaný	zamilovaný	k2eAgMnSc1d1	zamilovaný
ponocný	ponocný	k1gMnSc1	ponocný
provedené	provedený	k2eAgInPc1d1	provedený
bádenskou	bádenský	k2eAgFnSc7d1	bádenská
divadelní	divadelní	k2eAgFnSc7d1	divadelní
společností	společnost	k1gFnSc7	společnost
<g/>
,	,	kIx,	,
tentýž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
zde	zde	k6eAd1	zde
koncertoval	koncertovat	k5eAaImAgMnS	koncertovat
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Amadeus	Amadeus	k1gMnSc1	Amadeus
Mozart	Mozart	k1gMnSc1	Mozart
se	se	k3xPyFc4	se
svojí	svůj	k3xOyFgFnSc7	svůj
sestrou	sestra	k1gFnSc7	sestra
Annou	Anna	k1gFnSc7	Anna
Marií	Maria	k1gFnSc7	Maria
<g/>
.	.	kIx.	.
</s>
<s>
Mozartova	Mozartův	k2eAgFnSc1d1	Mozartova
rodina	rodina	k1gFnSc1	rodina
totiž	totiž	k9	totiž
tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
trávila	trávit	k5eAaImAgFnS	trávit
Vánoce	Vánoce	k1gFnPc1	Vánoce
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
vzácnou	vzácný	k2eAgFnSc4d1	vzácná
návštěvu	návštěva	k1gFnSc4	návštěva
dodnes	dodnes	k6eAd1	dodnes
připomíná	připomínat	k5eAaImIp3nS	připomínat
soška	soška	k1gFnSc1	soška
malého	malý	k2eAgMnSc2d1	malý
Mozarta	Mozart	k1gMnSc2	Mozart
před	před	k7c7	před
Redutou	reduta	k1gFnSc7	reduta
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
nese	nést	k5eAaImIp3nS	nést
i	i	k9	i
Mozartův	Mozartův	k2eAgInSc1d1	Mozartův
sál	sál	k1gInSc1	sál
v	v	k7c6	v
Redutě	reduta	k1gFnSc6	reduta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
Brno	Brno	k1gNnSc4	Brno
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
předních	přední	k2eAgMnPc2d1	přední
představitelů	představitel	k1gMnPc2	představitel
operní	operní	k2eAgFnSc2d1	operní
<g/>
,	,	kIx,	,
činoherní	činoherní	k2eAgFnSc2d1	činoherní
a	a	k8xC	a
baletní	baletní	k2eAgFnSc2d1	baletní
scény	scéna	k1gFnSc2	scéna
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
svoji	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
této	tento	k3xDgFnSc2	tento
instituce	instituce	k1gFnSc2	instituce
patří	patřit	k5eAaImIp3nS	patřit
Mahenovo	Mahenův	k2eAgNnSc1d1	Mahenovo
divadlo	divadlo	k1gNnSc1	divadlo
postavené	postavený	k2eAgNnSc1d1	postavené
roku	rok	k1gInSc2	rok
1882	[number]	k4	1882
<g/>
,	,	kIx,	,
Janáčkovo	Janáčkův	k2eAgNnSc1d1	Janáčkovo
divadlo	divadlo	k1gNnSc1	divadlo
postavené	postavený	k2eAgNnSc1d1	postavené
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
a	a	k8xC	a
divadlo	divadlo	k1gNnSc4	divadlo
Reduta	reduta	k1gFnSc1	reduta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
onou	onen	k3xDgFnSc7	onen
nejstarší	starý	k2eAgFnSc7d3	nejstarší
divadelní	divadelní	k2eAgFnSc7d1	divadelní
budovou	budova	k1gFnSc7	budova
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Národním	národní	k2eAgNnSc7d1	národní
divadlem	divadlo	k1gNnSc7	divadlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
spjata	spjat	k2eAgFnSc1d1	spjata
také	také	k9	také
osobnost	osobnost	k1gFnSc1	osobnost
světově	světově	k6eAd1	světově
uznávaného	uznávaný	k2eAgMnSc2d1	uznávaný
skladatele	skladatel	k1gMnSc2	skladatel
Leoše	Leoš	k1gMnSc2	Leoš
Janáčka	Janáček	k1gMnSc2	Janáček
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Mahenovo	Mahenův	k2eAgNnSc1d1	Mahenovo
divadlo	divadlo	k1gNnSc1	divadlo
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
prvním	první	k4xOgInSc7	první
plně	plně	k6eAd1	plně
elektricky	elektricky	k6eAd1	elektricky
osvětleným	osvětlený	k2eAgNnSc7d1	osvětlené
divadlem	divadlo	k1gNnSc7	divadlo
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
divadla	divadlo	k1gNnSc2	divadlo
dokonce	dokonce	k9	dokonce
postavena	postaven	k2eAgFnSc1d1	postavena
malá	malý	k2eAgFnSc1d1	malá
parní	parní	k2eAgFnSc1d1	parní
elektrárna	elektrárna	k1gFnSc1	elektrárna
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
Thomas	Thomas	k1gMnSc1	Thomas
Alva	Alva	k1gMnSc1	Alva
Edison	Edison	k1gMnSc1	Edison
přijel	přijet	k5eAaPmAgMnS	přijet
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
zhlédnout	zhlédnout	k5eAaPmF	zhlédnout
svůj	svůj	k3xOyFgInSc4	svůj
výtvor	výtvor	k1gInSc4	výtvor
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
<g />
.	.	kIx.	.
</s>
<s>
představitelů	představitel	k1gMnPc2	představitel
brněnského	brněnský	k2eAgNnSc2d1	brněnské
divadla	divadlo	k1gNnSc2	divadlo
je	být	k5eAaImIp3nS	být
také	také	k9	také
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
historie	historie	k1gFnSc1	historie
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
a	a	k8xC	a
které	který	k3yRgNnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
scénu	scéna	k1gFnSc4	scéna
hudební	hudební	k2eAgFnPc1d1	hudební
a	a	k8xC	a
činoherní	činoherní	k2eAgFnPc1d1	činoherní
<g/>
,	,	kIx,	,
oboje	oboj	k1gFnPc1	oboj
velmi	velmi	k6eAd1	velmi
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
<g/>
,	,	kIx,	,
vstupenky	vstupenka	k1gFnPc1	vstupenka
bývají	bývat	k5eAaImIp3nP	bývat
brzy	brzy	k6eAd1	brzy
vyprodány	vyprodán	k2eAgFnPc1d1	vyprodána
a	a	k8xC	a
divadelní	divadelní	k2eAgNnSc1d1	divadelní
uskupení	uskupení	k1gNnSc1	uskupení
navíc	navíc	k6eAd1	navíc
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
roce	rok	k1gInSc6	rok
odehraje	odehrát	k5eAaPmIp3nS	odehrát
na	na	k7c4	na
150	[number]	k4	150
představení	představení	k1gNnPc2	představení
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
působí	působit	k5eAaImIp3nS	působit
řada	řada	k1gFnSc1	řada
divadel	divadlo	k1gNnPc2	divadlo
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
autorské	autorský	k2eAgFnPc4d1	autorská
scény	scéna	k1gFnPc4	scéna
Divadlo	divadlo	k1gNnSc1	divadlo
Bolka	Bolek	k1gMnSc2	Bolek
Polívky	Polívka	k1gMnSc2	Polívka
<g/>
,	,	kIx,	,
loutkové	loutkový	k2eAgNnSc1d1	loutkové
divadlo	divadlo	k1gNnSc1	divadlo
Radost	radost	k1gFnSc1	radost
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
Vaňkovka	Vaňkovka	k1gFnSc1	Vaňkovka
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Divadlo	divadlo	k1gNnSc1	divadlo
Polárka	Polárka	k1gFnSc1	Polárka
<g/>
,	,	kIx,	,
G	G	kA	G
Studio	studio	k1gNnSc1	studio
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
v	v	k7c6	v
7	[number]	k4	7
a	a	k8xC	a
půl	půl	k1xP	půl
(	(	kIx(	(
<g/>
Kabinet	kabinet	k1gInSc1	kabinet
múz	múza	k1gFnPc2	múza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Divadlo	divadlo	k1gNnSc1	divadlo
na	na	k7c4	na
Orlí	orlí	k2eAgFnSc4d1	orlí
(	(	kIx(	(
<g/>
hudebně-dramatická	hudebněramatický	k2eAgFnSc1d1	hudebně-dramatická
laboratoř	laboratoř	k1gFnSc1	laboratoř
JAMU	jam	k1gInSc2	jam
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
Husa	Hus	k1gMnSc2	Hus
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
<g/>
,	,	kIx,	,
HaDivadlo	HaDivadlo	k1gNnSc1	HaDivadlo
a	a	k8xC	a
Divadlo	divadlo	k1gNnSc1	divadlo
U	u	k7c2	u
stolu	stol	k1gInSc2	stol
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Centra	centrum	k1gNnSc2	centrum
experimentálního	experimentální	k2eAgNnSc2d1	experimentální
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Repertoár	repertoár	k1gInSc1	repertoár
brněnských	brněnský	k2eAgNnPc2d1	brněnské
divadel	divadlo	k1gNnPc2	divadlo
tedy	tedy	k9	tedy
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
právem	právem	k6eAd1	právem
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
relativně	relativně	k6eAd1	relativně
pestrý	pestrý	k2eAgInSc4d1	pestrý
a	a	k8xC	a
výjimkou	výjimka	k1gFnSc7	výjimka
nejsou	být	k5eNaImIp3nP	být
ani	ani	k8xC	ani
vystoupení	vystoupení	k1gNnSc4	vystoupení
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
uměleckých	umělecký	k2eAgInPc2d1	umělecký
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Shen	Shen	k1gNnSc1	Shen
Yun	Yun	k1gMnPc2	Yun
Performing	Performing	k1gInSc4	Performing
Arts	Artsa	k1gFnPc2	Artsa
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
<g/>
Brněnská	brněnský	k2eAgNnPc4d1	brněnské
divadla	divadlo	k1gNnPc4	divadlo
prošla	projít	k5eAaPmAgFnS	projít
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
vývojem	vývoj	k1gInSc7	vývoj
a	a	k8xC	a
původní	původní	k2eAgFnSc2d1	původní
scény	scéna	k1gFnSc2	scéna
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
mohou	moct	k5eAaImIp3nP	moct
od	od	k7c2	od
těch	ten	k3xDgMnPc2	ten
dnešní	dnešní	k2eAgMnPc1d1	dnešní
podstatně	podstatně	k6eAd1	podstatně
lišit	lišit	k5eAaImF	lišit
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Mahenovo	Mahenův	k2eAgNnSc1d1	Mahenovo
divadlo	divadlo	k1gNnSc1	divadlo
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
nazývalo	nazývat	k5eAaImAgNnS	nazývat
Městské	městský	k2eAgNnSc1d1	Městské
divadlo	divadlo	k1gNnSc1	divadlo
a	a	k8xC	a
hrálo	hrát	k5eAaImAgNnS	hrát
německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
součástí	součást	k1gFnPc2	součást
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
až	až	k9	až
za	za	k7c2	za
první	první	k4xOgFnSc2	první
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
s	s	k7c7	s
divadlem	divadlo	k1gNnSc7	divadlo
Reduta	reduta	k1gFnSc1	reduta
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1971	[number]	k4	1971
a	a	k8xC	a
1978	[number]	k4	1978
se	se	k3xPyFc4	se
některé	některý	k3yIgFnSc2	některý
činoherní	činoherní	k2eAgFnSc2d1	činoherní
inscenace	inscenace	k1gFnSc2	inscenace
dokonce	dokonce	k9	dokonce
odehrávány	odehráván	k2eAgInPc1d1	odehráván
na	na	k7c6	na
brněnském	brněnský	k2eAgNnSc6d1	brněnské
výstavišti	výstaviště	k1gNnSc6	výstaviště
kvůli	kvůli	k7c3	kvůli
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
Mahenova	Mahenův	k2eAgNnSc2d1	Mahenovo
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Kina	kino	k1gNnSc2	kino
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Do	do	k7c2	do
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
fungovalo	fungovat	k5eAaImAgNnS	fungovat
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
kin	kino	k1gNnPc2	kino
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
zanikla	zaniknout	k5eAaPmAgNnP	zaniknout
kina	kino	k1gNnSc2	kino
Jadran	Jadran	k1gInSc1	Jadran
<g/>
,	,	kIx,	,
Jalta	Jalta	k1gFnSc1	Jalta
<g/>
,	,	kIx,	,
Kinokavárna	kinokavárna	k1gFnSc1	kinokavárna
Boby	bob	k1gInPc4	bob
<g/>
,	,	kIx,	,
Kinokavárna	kinokavárna	k1gFnSc1	kinokavárna
Rio	Rio	k1gFnSc1	Rio
<g/>
,	,	kIx,	,
Premiéra	premiéra	k1gFnSc1	premiéra
Club	club	k1gInSc1	club
<g/>
,	,	kIx,	,
Slavia	Slavia	k1gFnSc1	Slavia
a	a	k8xC	a
Světozor	světozor	k1gInSc1	světozor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
otevření	otevření	k1gNnSc6	otevření
multikina	multikino	k1gNnSc2	multikino
v	v	k7c6	v
nákupním	nákupní	k2eAgNnSc6d1	nákupní
centru	centrum	k1gNnSc6	centrum
Olympia	Olympia	k1gFnSc1	Olympia
v	v	k7c6	v
nedalekých	daleký	k2eNgInPc6d1	nedaleký
Modřicích	Modřik	k1gInPc6	Modřik
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
zbývajícím	zbývající	k2eAgNnPc3d1	zbývající
kinům	kino	k1gNnPc3	kino
technicky	technicky	k6eAd1	technicky
lépe	dobře	k6eAd2	dobře
vybavená	vybavený	k2eAgFnSc1d1	vybavená
konkurence	konkurence	k1gFnSc1	konkurence
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
konkurence	konkurence	k1gFnSc1	konkurence
ještě	ještě	k6eAd1	ještě
narostla	narůst	k5eAaPmAgFnS	narůst
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
vznikem	vznik	k1gInSc7	vznik
dalšího	další	k2eAgNnSc2d1	další
multikina	multikino	k1gNnSc2	multikino
Velký	velký	k2eAgInSc4d1	velký
Špalíček	špalíček	k1gInSc4	špalíček
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
kin	kino	k1gNnPc2	kino
Alfa	alfa	k1gFnSc1	alfa
<g/>
,	,	kIx,	,
Beseda	beseda	k1gFnSc1	beseda
<g/>
,	,	kIx,	,
Kapitol	Kapitol	k1gInSc1	Kapitol
<g/>
,	,	kIx,	,
Morava	Morava	k1gFnSc1	Morava
<g/>
,	,	kIx,	,
Svratka	Svratka	k1gFnSc1	Svratka
a	a	k8xC	a
Svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původních	původní	k2eAgNnPc2d1	původní
menších	malý	k2eAgNnPc2d2	menší
kin	kino	k1gNnPc2	kino
tak	tak	k6eAd1	tak
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
pouze	pouze	k6eAd1	pouze
kina	kino	k1gNnSc2	kino
Art	Art	k1gFnSc2	Art
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Cihlářská	cihlářský	k2eAgFnSc1d1	Cihlářská
<g/>
)	)	kIx)	)
a	a	k8xC	a
Lucerna	lucerna	k1gFnSc1	lucerna
(	(	kIx(	(
<g/>
ulice	ulice	k1gFnSc1	ulice
Minská	minský	k2eAgFnSc1d1	Minská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kino	kino	k1gNnSc1	kino
Art	Art	k1gFnSc2	Art
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
správě	správa	k1gFnSc6	správa
Turistického	turistický	k2eAgNnSc2d1	turistické
informačního	informační	k2eAgNnSc2d1	informační
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
Lucerna	lucerna	k1gFnSc1	lucerna
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc1	druhý
nejstarší	starý	k2eAgNnSc1d3	nejstarší
stále	stále	k6eAd1	stále
hrající	hrající	k2eAgNnSc1d1	hrající
kino	kino	k1gNnSc1	kino
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
soukromém	soukromý	k2eAgNnSc6d1	soukromé
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
Provoz	provoz	k1gInSc4	provoz
kina	kino	k1gNnSc2	kino
Scala	scát	k5eAaImAgFnS	scát
(	(	kIx(	(
<g/>
Moravské	moravský	k2eAgNnSc1d1	Moravské
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
městu	město	k1gNnSc3	město
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
jako	jako	k8xS	jako
prodělečný	prodělečný	k2eAgMnSc1d1	prodělečný
zastaven	zastavit	k5eAaPmNgMnS	zastavit
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
kino	kino	k1gNnSc4	kino
provozuje	provozovat	k5eAaImIp3nS	provozovat
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
kromě	kromě	k7c2	kromě
promítání	promítání	k1gNnSc2	promítání
sál	sál	k1gInSc1	sál
využívá	využívat	k5eAaImIp3nS	využívat
i	i	k9	i
pro	pro	k7c4	pro
konference	konference	k1gFnPc4	konference
a	a	k8xC	a
výuku	výuka	k1gFnSc4	výuka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Hudební	hudební	k2eAgInPc1d1	hudební
kluby	klub	k1gInPc1	klub
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
několik	několik	k4yIc4	několik
známých	známý	k2eAgInPc2d1	známý
klubů	klub	k1gInPc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Dvoupatrové	dvoupatrový	k2eAgFnSc2d1	dvoupatrová
Mersey	Mersea	k1gFnSc2	Mersea
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
klubů	klub	k1gInPc2	klub
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Kultovním	kultovní	k2eAgNnSc7d1	kultovní
místem	místo	k1gNnSc7	místo
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
největší	veliký	k2eAgInSc1d3	veliký
brněnský	brněnský	k2eAgInSc1d1	brněnský
hudební	hudební	k2eAgInSc1d1	hudební
klub	klub	k1gInSc1	klub
Fléda	Fléda	k1gFnSc1	Fléda
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
velkými	velký	k2eAgInPc7d1	velký
koncerty	koncert	k1gInPc7	koncert
i	i	k8xC	i
pátečními	páteční	k2eAgFnPc7d1	páteční
tanečními	taneční	k2eAgFnPc7d1	taneční
akcemi	akce	k1gFnPc7	akce
<g/>
,	,	kIx,	,
dává	dávat	k5eAaImIp3nS	dávat
šanci	šance	k1gFnSc4	šance
i	i	k8xC	i
začínajícím	začínající	k2eAgFnPc3d1	začínající
českým	český	k2eAgFnPc3d1	Česká
kapelám	kapela	k1gFnPc3	kapela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
samotném	samotný	k2eAgNnSc6d1	samotné
srdci	srdce	k1gNnSc6	srdce
města	město	k1gNnSc2	město
stojí	stát	k5eAaImIp3nS	stát
legendární	legendární	k2eAgFnSc1d1	legendární
Skleněná	skleněný	k2eAgFnSc1d1	skleněná
louka	louka	k1gFnSc1	louka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
Sklenick	Sklenick	k1gMnSc1	Sklenick
nabízí	nabízet	k5eAaImIp3nS	nabízet
útočiště	útočiště	k1gNnSc2	útočiště
tanečnímu	taneční	k2eAgInSc3d1	taneční
undergroundu	underground	k1gInSc3	underground
<g/>
.	.	kIx.	.
</s>
<s>
Znovuotevřený	znovuotevřený	k2eAgInSc1d1	znovuotevřený
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Kabinet	kabinet	k1gInSc1	kabinet
múz	múza	k1gFnPc2	múza
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
útočištěm	útočiště	k1gNnSc7	útočiště
brněnských	brněnský	k2eAgInPc2d1	brněnský
hipsterů	hipster	k1gInPc2	hipster
<g/>
,	,	kIx,	,
kapel	kapela	k1gFnPc2	kapela
<g/>
,	,	kIx,	,
divadel	divadlo	k1gNnPc2	divadlo
a	a	k8xC	a
DJů	DJů	k1gFnPc2	DJů
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kolejí	kolej	k1gFnPc2	kolej
VUT	VUT	kA	VUT
pak	pak	k6eAd1	pak
stojí	stát	k5eAaImIp3nS	stát
Yacht	Yacht	k1gMnSc1	Yacht
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
proslul	proslout	k5eAaPmAgMnS	proslout
svými	svůj	k3xOyFgMnPc7	svůj
HC	HC	kA	HC
<g/>
/	/	kIx~	/
<g/>
Punkovými	punkový	k2eAgInPc7d1	punkový
koncerty	koncert	k1gInPc7	koncert
organizovanými	organizovaný	k2eAgInPc7d1	organizovaný
Loser	Loser	k1gInSc4	Loser
Crew	Crew	k1gFnSc7	Crew
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
DIY	DIY	kA	DIY
promotéry	promotér	k1gMnPc7	promotér
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
nahradily	nahradit	k5eAaPmAgInP	nahradit
spíše	spíše	k9	spíše
kluby	klub	k1gInPc1	klub
Boro	Boro	k6eAd1	Boro
(	(	kIx(	(
<g/>
zaniklý	zaniklý	k2eAgMnSc1d1	zaniklý
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Vegalité	Vegalitý	k2eAgFnSc3d1	Vegalitý
a	a	k8xC	a
klub	klub	k1gInSc1	klub
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2014	[number]	k4	2014
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Veveří	veveří	k2eAgNnSc1d1	veveří
otevřeno	otevřen	k2eAgNnSc1d1	otevřeno
Sono	Sono	k1gNnSc1	Sono
Centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Gastronomie	gastronomie	k1gFnPc4	gastronomie
a	a	k8xC	a
noční	noční	k2eAgInSc4d1	noční
život	život	k1gInSc4	život
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
stovky	stovka	k1gFnPc1	stovka
různých	různý	k2eAgFnPc2d1	různá
restaurací	restaurace	k1gFnPc2	restaurace
nabízející	nabízející	k2eAgMnPc1d1	nabízející
domácí	domácí	k1gMnPc1	domácí
i	i	k9	i
cizí	cizí	k2eAgInPc4d1	cizí
druhy	druh	k1gInPc4	druh
pohoštění	pohoštění	k1gNnSc2	pohoštění
<g/>
,	,	kIx,	,
vináren	vinárna	k1gFnPc2	vinárna
s	s	k7c7	s
moravským	moravský	k2eAgNnSc7d1	Moravské
vínem	víno	k1gNnSc7	víno
<g/>
,	,	kIx,	,
kaváren	kavárna	k1gFnPc2	kavárna
<g/>
,	,	kIx,	,
různých	různý	k2eAgInPc2d1	různý
klubů	klub	k1gInPc2	klub
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obdobích	období	k1gNnPc6	období
turistické	turistický	k2eAgFnSc2d1	turistická
a	a	k8xC	a
především	především	k6eAd1	především
festivalové	festivalový	k2eAgFnSc2d1	festivalová
sezóny	sezóna	k1gFnSc2	sezóna
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
často	často	k6eAd1	často
pulzuje	pulzovat	k5eAaImIp3nS	pulzovat
životem	život	k1gInSc7	život
a	a	k8xC	a
stává	stávat	k5eAaImIp3nS	stávat
se	se	k3xPyFc4	se
dějištěm	dějiště	k1gNnSc7	dějiště
kulturních	kulturní	k2eAgFnPc2d1	kulturní
akcí	akce	k1gFnPc2	akce
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
divadelní	divadelní	k2eAgNnPc1d1	divadelní
a	a	k8xC	a
jiná	jiný	k2eAgNnPc1d1	jiné
umělecká	umělecký	k2eAgNnPc1d1	umělecké
vystoupení	vystoupení	k1gNnPc1	vystoupení
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
<g/>
,	,	kIx,	,
přehlídky	přehlídka	k1gFnPc1	přehlídka
historických	historický	k2eAgInPc2d1	historický
vozů	vůz	k1gInPc2	vůz
<g/>
,	,	kIx,	,
lidové	lidový	k2eAgFnPc1d1	lidová
slavnosti	slavnost	k1gFnPc1	slavnost
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgNnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
mimo	mimo	k7c4	mimo
takovéto	takovýto	k3xDgFnPc4	takovýto
kulturní	kulturní	k2eAgFnSc1d1	kulturní
akce	akce	k1gFnSc1	akce
život	život	k1gInSc1	život
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
utichá	utichat	k5eAaImIp3nS	utichat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
v	v	k7c6	v
nocích	noc	k1gFnPc6	noc
před	před	k7c7	před
pracovními	pracovní	k2eAgInPc7d1	pracovní
dny	den	k1gInPc7	den
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
skupinu	skupina	k1gFnSc4	skupina
vyznavačů	vyznavač	k1gMnPc2	vyznavač
nočního	noční	k2eAgInSc2d1	noční
života	život	k1gInSc2	život
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
tvoří	tvořit	k5eAaImIp3nP	tvořit
studenti	student	k1gMnPc1	student
<g/>
,	,	kIx,	,
kterých	který	k3yIgMnPc2	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
na	na	k7c4	na
sto	sto	k4xCgNnSc4	sto
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
je	být	k5eAaImIp3nS	být
nejrušnějším	rušný	k2eAgNnSc7d3	nejrušnější
místem	místo	k1gNnSc7	místo
historického	historický	k2eAgNnSc2d1	historické
jádra	jádro	k1gNnSc2	jádro
města	město	k1gNnSc2	město
terminál	terminál	k1gInSc1	terminál
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
před	před	k7c7	před
hlavním	hlavní	k2eAgNnSc7d1	hlavní
vlakovým	vlakový	k2eAgNnSc7d1	vlakové
nádražím	nádraží	k1gNnSc7	nádraží
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
vyjíždějí	vyjíždět	k5eAaImIp3nP	vyjíždět
takzvané	takzvaný	k2eAgFnPc1d1	takzvaná
"	"	kIx"	"
<g/>
rozjezdy	rozjezd	k1gInPc7	rozjezd
<g/>
"	"	kIx"	"
noční	noční	k2eAgFnSc2d1	noční
autobusové	autobusový	k2eAgFnSc2d1	autobusová
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
několik	několik	k4yIc4	několik
non-stop	nontop	k1gInSc4	non-stop
restaurací	restaurace	k1gFnPc2	restaurace
a	a	k8xC	a
stánků	stánek	k1gInPc2	stánek
rychlého	rychlý	k2eAgNnSc2d1	rychlé
občerstvení	občerstvení	k1gNnSc2	občerstvení
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
lidé	člověk	k1gMnPc1	člověk
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
nepřetržitě	přetržitě	k6eNd1	přetržitě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
brněnským	brněnský	k2eAgMnPc3d1	brněnský
gastronomickým	gastronomický	k2eAgNnPc3d1	gastronomické
zařízením	zařízení	k1gNnPc3	zařízení
patří	patřit	k5eAaImIp3nS	patřit
dvě	dva	k4xCgFnPc4	dva
restaurace	restaurace	k1gFnPc4	restaurace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
dosud	dosud	k6eAd1	dosud
jediné	jediný	k2eAgFnPc4d1	jediná
mimopražské	mimopražský	k2eAgFnPc4d1	mimopražská
<g/>
,	,	kIx,	,
zvítězily	zvítězit	k5eAaPmAgInP	zvítězit
v	v	k7c6	v
každoročním	každoroční	k2eAgInSc6d1	každoroční
celostátním	celostátní	k2eAgInSc6d1	celostátní
výběru	výběr	k1gInSc6	výběr
Maurerův	Maurerův	k2eAgInSc4d1	Maurerův
výběr	výběr	k1gInSc4	výběr
Grand-restaurant	Grandestaurant	k1gMnSc1	Grand-restaurant
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
restaurace	restaurace	k1gFnSc1	restaurace
U	u	k7c2	u
Kastelána	kastelán	k1gMnSc2	kastelán
(	(	kIx(	(
<g/>
v	v	k7c6	v
Kotlářské	kotlářský	k2eAgFnSc6d1	Kotlářská
ulici	ulice	k1gFnSc6	ulice
<g/>
)	)	kIx)	)
a	a	k8xC	a
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
restaurace	restaurace	k1gFnSc2	restaurace
Koishi	Koish	k1gFnSc2	Koish
fish	fish	k1gMnSc1	fish
&	&	k?	&
sushi	sush	k1gFnSc2	sush
(	(	kIx(	(
<g/>
v	v	k7c6	v
Údolní	údolní	k2eAgFnSc6d1	údolní
ulici	ulice	k1gFnSc6	ulice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
Gourmet	Gourmeta	k1gFnPc2	Gourmeta
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
pořádané	pořádaný	k2eAgNnSc1d1	pořádané
brněnským	brněnský	k2eAgInSc7d1	brněnský
Turistickým	turistický	k2eAgInSc7d1	turistický
informačním	informační	k2eAgInSc7d1	informační
centrem	centr	k1gInSc7	centr
<g/>
,	,	kIx,	,
uspěly	uspět	k5eAaPmAgFnP	uspět
restaurace	restaurace	k1gFnPc1	restaurace
Simplé	Simplý	k2eAgFnPc1d1	Simplý
<g/>
,	,	kIx,	,
kavárna	kavárna	k1gFnSc1	kavárna
Coffee	Coffee	k1gFnSc1	Coffee
Fusion	Fusion	k1gInSc1	Fusion
<g/>
,	,	kIx,	,
pivnice	pivnice	k1gFnSc1	pivnice
Lokál	lokál	k1gInSc1	lokál
u	u	k7c2	u
Caipla	Caipla	k1gFnSc2	Caipla
<g/>
,	,	kIx,	,
vinárna	vinárna	k1gFnSc1	vinárna
Retro	retro	k1gNnSc2	retro
Consistorium	Consistorium	k1gNnSc1	Consistorium
<g/>
,	,	kIx,	,
cukrárna	cukrárna	k1gFnSc1	cukrárna
Raw	Raw	k1gMnSc1	Raw
with	with	k1gMnSc1	with
Love	lov	k1gInSc5	lov
a	a	k8xC	a
bar	bar	k1gInSc1	bar
Super	super	k1gInPc2	super
Panda	panda	k1gFnSc1	panda
Circus	Circus	k1gInSc1	Circus
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
dalšími	další	k2eAgMnPc7d1	další
oceněnými	oceněný	k2eAgMnPc7d1	oceněný
dále	daleko	k6eAd2	daleko
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
restaurací	restaurace	k1gFnPc2	restaurace
Koishi	Koish	k1gFnSc2	Koish
<g/>
,	,	kIx,	,
Restaurant	restaurant	k1gInSc1	restaurant
Pavillon	Pavillon	k1gInSc1	Pavillon
<g/>
,	,	kIx,	,
Borgo	Borgo	k6eAd1	Borgo
Agnese	Agnese	k1gFnSc1	Agnese
<g/>
,	,	kIx,	,
Palazzo	Palazza	k1gFnSc5	Palazza
Restaurant	restaurant	k1gInSc1	restaurant
<g/>
,	,	kIx,	,
Forhaus	Forhaus	k1gInSc1	Forhaus
<g/>
,	,	kIx,	,
Castellana	Castellana	k1gFnSc1	Castellana
trattoria	trattorium	k1gNnSc2	trattorium
<g/>
,	,	kIx,	,
La	la	k1gNnSc2	la
Bouchée	Bouché	k1gFnSc2	Bouché
a	a	k8xC	a
Retro	retro	k1gNnSc1	retro
Consistorium	Consistorium	k1gNnSc1	Consistorium
<g/>
,	,	kIx,	,
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
kaváren	kavárna	k1gFnPc2	kavárna
SKØ	SKØ	k1gFnSc1	SKØ
<g/>
,	,	kIx,	,
Kafec	Kafec	k1gInSc1	Kafec
<g/>
,	,	kIx,	,
Punkt	punkt	k1gInSc1	punkt
<g/>
.	.	kIx.	.
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Cafe	Cafus	k1gMnSc5	Cafus
Mittte	Mittt	k1gMnSc5	Mittt
<g/>
,	,	kIx,	,
Café	café	k1gNnPc7	café
Placzek	Placzek	k1gInSc1	Placzek
<g/>
,	,	kIx,	,
Kavárna	kavárna	k1gFnSc1	kavárna
Era	Era	k1gFnSc1	Era
a	a	k8xC	a
Spolek	spolek	k1gInSc1	spolek
<g/>
,	,	kIx,	,
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
pivnic	pivnice	k1gFnPc2	pivnice
Výčep	výčep	k1gInSc1	výčep
Na	na	k7c4	na
stojáka	stoják	k1gMnSc4	stoják
<g/>
,	,	kIx,	,
Zelená	zelenat	k5eAaImIp3nS	zelenat
Kočka	kočka	k1gFnSc1	kočka
Pivárium	Pivárium	k1gNnSc1	Pivárium
<g/>
,	,	kIx,	,
Ochutnávková	Ochutnávkový	k2eAgFnSc1d1	Ochutnávková
pivnice	pivnice	k1gFnSc1	pivnice
a	a	k8xC	a
Pivovarská	pivovarský	k2eAgFnSc1d1	Pivovarská
Starobrno	Starobrno	k1gNnSc1	Starobrno
(	(	kIx(	(
<g/>
zmíněny	zmíněn	k2eAgInPc1d1	zmíněn
byly	být	k5eAaImAgInP	být
i	i	k9	i
podniky	podnik	k1gInPc1	podnik
Lucky	Lucka	k1gFnSc2	Lucka
Bastard	bastard	k1gMnSc1	bastard
Beerhouse	Beerhouse	k1gFnSc1	Beerhouse
<g/>
,	,	kIx,	,
Hostinec	hostinec	k1gInSc1	hostinec
u	u	k7c2	u
Bláhovky	Bláhovka	k1gFnSc2	Bláhovka
a	a	k8xC	a
Pegas	Pegas	k1gMnSc1	Pegas
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
vináren	vinárna	k1gFnPc2	vinárna
Garage	Garage	k1gFnSc1	Garage
Wine	Wine	k1gFnSc1	Wine
<g/>
,	,	kIx,	,
Petit	petit	k1gInSc1	petit
Cru	Cru	k1gFnSc2	Cru
wine	winat	k5eAaPmIp3nS	winat
bar	bar	k1gInSc1	bar
&	&	k?	&
shop	shop	k1gInSc1	shop
<g/>
,	,	kIx,	,
Kohout	kohout	k1gInSc1	kohout
na	na	k7c6	na
víně	víno	k1gNnSc6	víno
<g/>
,	,	kIx,	,
Don	Don	k1gMnSc1	Don
Pintxos	Pintxos	k1gMnSc1	Pintxos
<g/>
,	,	kIx,	,
U	u	k7c2	u
Tří	tři	k4xCgNnPc2	tři
knížat	kníže	k1gNnPc2	kníže
<g/>
,	,	kIx,	,
Jedna	jeden	k4xCgFnSc1	jeden
báseň	báseň	k1gFnSc1	báseň
a	a	k8xC	a
Baroko	baroko	k1gNnSc1	baroko
<g/>
,	,	kIx,	,
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
cukráren	cukrárna	k1gFnPc2	cukrárna
cukrářství	cukrářství	k1gNnSc1	cukrářství
Bukovský	Bukovský	k1gMnSc1	Bukovský
<g/>
,	,	kIx,	,
Cukrářství	cukrářství	k1gNnSc1	cukrářství
Martinák	Martinák	k1gMnSc1	Martinák
a	a	k8xC	a
cukrárna	cukrárna	k1gFnSc1	cukrárna
Kolbaba	Kolbaba	k1gFnSc1	Kolbaba
a	a	k8xC	a
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
barů	bar	k1gInPc2	bar
Runway	runway	k1gInSc1	runway
<g/>
,	,	kIx,	,
Bar	bar	k1gInSc1	bar
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
Rotor	rotor	k1gInSc1	rotor
bar	bar	k1gInSc1	bar
<g/>
,	,	kIx,	,
Cubana	Cubana	k1gFnSc1	Cubana
<g/>
,	,	kIx,	,
Spirit	Spirit	k1gInSc1	Spirit
bar	bar	k1gInSc1	bar
a	a	k8xC	a
Two	Two	k1gMnSc1	Two
Faces	Faces	k1gMnSc1	Faces
coctail	coctait	k5eAaPmAgMnS	coctait
bar	bar	k1gInSc4	bar
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
známým	známý	k2eAgInPc3d1	známý
podnikům	podnik	k1gInPc3	podnik
s	s	k7c7	s
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
tradicí	tradice	k1gFnSc7	tradice
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
Stopkova	Stopkův	k2eAgFnSc1d1	Stopkova
plzeňská	plzeňský	k2eAgFnSc1d1	Plzeňská
pivnice	pivnice	k1gFnSc1	pivnice
z	z	k7c2	z
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
také	také	k9	také
Restaurant	restaurant	k1gInSc1	restaurant
Černý	Černý	k1gMnSc1	Černý
medvěd	medvěd	k1gMnSc1	medvěd
<g/>
,	,	kIx,	,
existující	existující	k2eAgMnSc1d1	existující
minimálně	minimálně	k6eAd1	minimálně
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Média	médium	k1gNnPc1	médium
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
sídlí	sídlet	k5eAaImIp3nP	sídlet
velké	velký	k2eAgFnPc4d1	velká
redakce	redakce	k1gFnPc4	redakce
nejvýznamnějších	významný	k2eAgNnPc2d3	nejvýznamnější
celostátních	celostátní	k2eAgNnPc2d1	celostátní
médií	médium	k1gNnPc2	médium
–	–	k?	–
deníků	deník	k1gInPc2	deník
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
DNES	dnes	k6eAd1	dnes
a	a	k8xC	a
Brněnský	brněnský	k2eAgInSc1d1	brněnský
deník	deník	k1gInSc1	deník
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Rovnost	rovnost	k1gFnSc1	rovnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
redakce	redakce	k1gFnPc1	redakce
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
denní	denní	k2eAgFnSc4d1	denní
výrobu	výroba	k1gFnSc4	výroba
regionálních	regionální	k2eAgFnPc2d1	regionální
zpravodajských	zpravodajský	k2eAgFnPc2d1	zpravodajská
příloh	příloha	k1gFnPc2	příloha
svých	svůj	k3xOyFgInPc2	svůj
titulů	titul	k1gInPc2	titul
<g/>
,	,	kIx,	,
a	a	k8xC	a
menší	malý	k2eAgFnSc2d2	menší
redakce	redakce	k1gFnSc2	redakce
deníků	deník	k1gInPc2	deník
Právo	právo	k1gNnSc1	právo
a	a	k8xC	a
Blesk	blesk	k1gInSc1	blesk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
brněnské	brněnský	k2eAgNnSc1d1	brněnské
studio	studio	k1gNnSc1	studio
nynější	nynější	k2eAgFnSc2d1	nynější
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
Redakce	redakce	k1gFnSc1	redakce
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
dnes	dnes	k6eAd1	dnes
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
denní	denní	k2eAgFnSc4d1	denní
výrobu	výroba	k1gFnSc4	výroba
podvečerního	podvečerní	k2eAgNnSc2d1	podvečerní
vysílání	vysílání	k1gNnSc2	vysílání
zpravodajského	zpravodajský	k2eAgInSc2d1	zpravodajský
pořadu	pořad	k1gInSc2	pořad
Události	událost	k1gFnSc2	událost
v	v	k7c6	v
regionech	region	k1gInPc6	region
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Jihomoravský	jihomoravský	k2eAgInSc1d1	jihomoravský
večerník	večerník	k1gInSc1	večerník
<g/>
)	)	kIx)	)
vysílaného	vysílaný	k2eAgInSc2d1	vysílaný
na	na	k7c6	na
televizní	televizní	k2eAgFnSc6d1	televizní
stanici	stanice	k1gFnSc6	stanice
ČT	ČT	kA	ČT
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
také	také	k9	také
redakce	redakce	k1gFnSc1	redakce
zpravodajství	zpravodajství	k1gNnSc2	zpravodajství
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
a	a	k8xC	a
malá	malý	k2eAgFnSc1d1	malá
redakce	redakce	k1gFnSc1	redakce
Prima	prima	k2eAgFnSc2d1	prima
family	famila	k1gFnSc2	famila
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
(	(	kIx(	(
<g/>
brněnské	brněnský	k2eAgNnSc1d1	brněnské
studio	studio	k1gNnSc1	studio
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
vysílá	vysílat	k5eAaImIp3nS	vysílat
z	z	k7c2	z
Brna	Brno	k1gNnSc2	Brno
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ČRo	ČRo	k1gFnSc2	ČRo
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
zpravodajská	zpravodajský	k2eAgFnSc1d1	zpravodajská
redakce	redakce	k1gFnSc1	redakce
přispívá	přispívat	k5eAaImIp3nS	přispívat
do	do	k7c2	do
zpravodajských	zpravodajský	k2eAgInPc2d1	zpravodajský
pořadů	pořad	k1gInPc2	pořad
vysílaných	vysílaný	k2eAgInPc2d1	vysílaný
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ČRo	ČRo	k1gFnSc1	ČRo
Radiožurnál	radiožurnál	k1gInSc1	radiožurnál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
sídlí	sídlet	k5eAaImIp3nS	sídlet
i	i	k9	i
početná	početný	k2eAgFnSc1d1	početná
redakce	redakce	k1gFnSc1	redakce
České	český	k2eAgFnSc2d1	Česká
tiskové	tiskový	k2eAgFnSc2d1	tisková
kanceláře	kancelář	k1gFnSc2	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
vysílá	vysílat	k5eAaImIp3nS	vysílat
denně	denně	k6eAd1	denně
od	od	k7c2	od
6	[number]	k4	6
ráno	ráno	k6eAd1	ráno
do	do	k7c2	do
půlnoci	půlnoc	k1gFnSc2	půlnoc
soukromá	soukromý	k2eAgFnSc1d1	soukromá
stanice	stanice	k1gFnSc1	stanice
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
televize	televize	k1gFnSc1	televize
(	(	kIx(	(
<g/>
B-TV	B-TV	k1gFnSc1	B-TV
<g/>
)	)	kIx)	)
dostupná	dostupný	k2eAgFnSc1d1	dostupná
v	v	k7c6	v
kabelových	kabelový	k2eAgFnPc6d1	kabelová
sítích	síť	k1gFnPc6	síť
UPC	UPC	kA	UPC
<g/>
,	,	kIx,	,
O2TV	O2TV	k1gFnSc1	O2TV
a	a	k8xC	a
NETBOX	NETBOX	kA	NETBOX
–	–	k?	–
věnuje	věnovat	k5eAaPmIp3nS	věnovat
se	se	k3xPyFc4	se
lokálnímu	lokální	k2eAgNnSc3d1	lokální
zpravodajství	zpravodajství	k1gNnSc3	zpravodajství
z	z	k7c2	z
města	město	k1gNnSc2	město
a	a	k8xC	a
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
sídlí	sídlet	k5eAaImIp3nS	sídlet
i	i	k9	i
několik	několik	k4yIc4	několik
soukromých	soukromý	k2eAgNnPc2d1	soukromé
regionálních	regionální	k2eAgNnPc2d1	regionální
rádií	rádio	k1gNnPc2	rádio
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
také	také	k9	také
vydává	vydávat	k5eAaImIp3nS	vydávat
své	svůj	k3xOyFgFnPc4	svůj
informační	informační	k2eAgFnPc4d1	informační
noviny	novina	k1gFnPc4	novina
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Brněnský	brněnský	k2eAgInSc1d1	brněnský
Metropolitan	metropolitan	k1gInSc1	metropolitan
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
měsíčník	měsíčník	k1gInSc1	měsíčník
je	být	k5eAaImIp3nS	být
zdarma	zdarma	k6eAd1	zdarma
a	a	k8xC	a
informuje	informovat	k5eAaBmIp3nS	informovat
o	o	k7c6	o
rozvoji	rozvoj	k1gInSc6	rozvoj
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kultuře	kultura	k1gFnSc3	kultura
a	a	k8xC	a
lokální	lokální	k2eAgFnSc3d1	lokální
politice	politika	k1gFnSc3	politika
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
něj	on	k3xPp3gInSc2	on
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
městské	městský	k2eAgFnPc1d1	městská
části	část	k1gFnPc1	část
obvykle	obvykle	k6eAd1	obvykle
vydávají	vydávat	k5eAaPmIp3nP	vydávat
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
informační	informační	k2eAgInPc4d1	informační
plátky	plátek	k1gInPc4	plátek
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vozech	vůz	k1gInPc6	vůz
MHD	MHD	kA	MHD
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
zdarma	zdarma	k6eAd1	zdarma
poskytován	poskytován	k2eAgInSc1d1	poskytován
měsíčník	měsíčník	k1gInSc1	měsíčník
Šalina	šalina	k1gFnSc1	šalina
vydávaný	vydávaný	k2eAgInSc1d1	vydávaný
Dopravním	dopravní	k2eAgInSc7d1	dopravní
podnikem	podnik	k1gInSc7	podnik
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
různé	různý	k2eAgFnPc1d1	různá
další	další	k2eAgFnPc1d1	další
veřejné	veřejný	k2eAgFnPc1d1	veřejná
noviny	novina	k1gFnPc1	novina
vydávají	vydávat	k5eAaImIp3nP	vydávat
také	také	k9	také
vysoké	vysoký	k2eAgFnPc4d1	vysoká
školy	škola	k1gFnPc4	škola
a	a	k8xC	a
podobné	podobný	k2eAgFnPc4d1	podobná
instituce	instituce	k1gFnPc4	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Zdarma	zdarma	k6eAd1	zdarma
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
také	také	k9	také
brněnské	brněnský	k2eAgInPc4d1	brněnský
kulturní	kulturní	k2eAgInPc4d1	kulturní
týdeníky	týdeník	k1gInPc4	týdeník
Metropolis	Metropolis	k1gFnSc2	Metropolis
a	a	k8xC	a
kulturní	kulturní	k2eAgInSc1d1	kulturní
magazín	magazín	k1gInSc1	magazín
Kult	kult	k1gInSc1	kult
oba	dva	k4xCgInPc4	dva
informující	informující	k2eAgInPc4d1	informující
o	o	k7c6	o
kulturním	kulturní	k2eAgNnSc6d1	kulturní
dění	dění	k1gNnSc6	dění
ve	v	k7c6	v
městě	město	k1gNnSc6	město
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
kultuře	kultura	k1gFnSc3	kultura
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
hudbě	hudba	k1gFnSc3	hudba
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
věnuje	věnovat	k5eAaImIp3nS	věnovat
brněnský	brněnský	k2eAgInSc1d1	brněnský
"	"	kIx"	"
<g/>
Cliché	Clichý	k2eAgNnSc1d1	Cliché
Brünn	Brünn	k1gNnSc1	Brünn
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
zdarma	zdarma	k6eAd1	zdarma
po	po	k7c6	po
kavárnách	kavárna	k1gFnPc6	kavárna
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
jsou	být	k5eAaImIp3nP	být
distribuovány	distribuován	k2eAgFnPc1d1	distribuována
také	také	k9	také
tiskoviny	tiskovina	k1gFnPc1	tiskovina
<g/>
,	,	kIx,	,
jako	jako	k9	jako
5	[number]	k4	5
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
dny	den	k1gInPc1	den
<g/>
,	,	kIx,	,
Metro	metro	k1gNnSc1	metro
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Brněnské	brněnský	k2eAgFnSc3d1	brněnská
pověsti	pověst	k1gFnSc3	pověst
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
Brna	Brno	k1gNnSc2	Brno
je	být	k5eAaImIp3nS	být
opředena	opříst	k5eAaPmNgFnS	opříst
mnohými	mnohý	k2eAgFnPc7d1	mnohá
pověstmi	pověst	k1gFnPc7	pověst
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
se	se	k3xPyFc4	se
traduje	tradovat	k5eAaImIp3nS	tradovat
pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
strašlivém	strašlivý	k2eAgInSc6d1	strašlivý
brněnském	brněnský	k2eAgInSc6d1	brněnský
draku	drak	k1gInSc6	drak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
kdysi	kdysi	k6eAd1	kdysi
ocitl	ocitnout	k5eAaPmAgMnS	ocitnout
a	a	k8xC	a
terorizoval	terorizovat	k5eAaImAgMnS	terorizovat
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
statečných	statečný	k2eAgMnPc2d1	statečný
měšťanů	měšťan	k1gMnPc2	měšťan
však	však	k9	však
lstí	lest	k1gFnSc7	lest
obávaného	obávaný	k2eAgMnSc4d1	obávaný
draka	drak	k1gMnSc4	drak
zabil	zabít	k5eAaPmAgMnS	zabít
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
dodnes	dodnes	k6eAd1	dodnes
visí	viset	k5eAaImIp3nS	viset
vycpaný	vycpaný	k2eAgInSc1d1	vycpaný
na	na	k7c6	na
Staré	Staré	k2eAgFnSc6d1	Staré
radnici	radnice	k1gFnSc6	radnice
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
připomínka	připomínka	k1gFnSc1	připomínka
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
událost	událost	k1gFnSc4	událost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
připomínka	připomínka	k1gFnSc1	připomínka
na	na	k7c4	na
vítězství	vítězství	k1gNnSc4	vítězství
při	při	k7c6	při
obléhání	obléhání	k1gNnSc6	obléhání
Brna	Brno	k1gNnSc2	Brno
švédským	švédský	k2eAgNnSc7d1	švédské
vojskem	vojsko	k1gNnSc7	vojsko
roku	rok	k1gInSc2	rok
1645	[number]	k4	1645
se	se	k3xPyFc4	se
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
zvoní	zvonit	k5eAaImIp3nS	zvonit
poledne	poledne	k1gNnSc4	poledne
v	v	k7c4	v
11	[number]	k4	11
hodin	hodina	k1gFnPc2	hodina
dopoledne	dopoledne	k6eAd1	dopoledne
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yRnSc6	což
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
pověst	pověst	k1gFnSc1	pověst
o	o	k7c4	o
zvonění	zvonění	k1gNnSc4	zvonění
na	na	k7c6	na
Petrově	Petrov	k1gInSc6	Petrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Brněnský	brněnský	k2eAgInSc1d1	brněnský
hantec	hantec	k1gInSc1	hantec
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Hantec	Hantec	k1gInSc1	Hantec
je	být	k5eAaImIp3nS	být
specifická	specifický	k2eAgFnSc1d1	specifická
<g/>
,	,	kIx,	,
výhradně	výhradně	k6eAd1	výhradně
brněnská	brněnský	k2eAgFnSc1d1	brněnská
řeč	řeč	k1gFnSc1	řeč
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
běžně	běžně	k6eAd1	běžně
používána	používat	k5eAaImNgFnS	používat
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
spíše	spíše	k9	spíše
sociálně	sociálně	k6eAd1	sociálně
slabšími	slabý	k2eAgFnPc7d2	slabší
vrstvami	vrstva	k1gFnPc7	vrstva
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
kulturních	kulturní	k2eAgFnPc2d1	kulturní
zvláštností	zvláštnost	k1gFnPc2	zvláštnost
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgInPc4	svůj
základy	základ	k1gInPc4	základ
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
češtině	čeština	k1gFnSc6	čeština
<g/>
,	,	kIx,	,
němčině	němčina	k1gFnSc6	němčina
<g/>
,	,	kIx,	,
cikánštině	cikánština	k1gFnSc6	cikánština
<g/>
,	,	kIx,	,
jidiš	jidiš	k1gNnSc6	jidiš
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
jazycích	jazyk	k1gInPc6	jazyk
a	a	k8xC	a
také	také	k6eAd1	také
místních	místní	k2eAgInPc6d1	místní
argotech	argot	k1gInPc6	argot
<g/>
.	.	kIx.	.
</s>
<s>
Hantec	Hantec	k1gInSc1	Hantec
je	být	k5eAaImIp3nS	být
natolik	natolik	k6eAd1	natolik
odlišný	odlišný	k2eAgInSc1d1	odlišný
od	od	k7c2	od
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
že	že	k8xS	že
jen	jen	k9	jen
pro	pro	k7c4	pro
pouhé	pouhý	k2eAgNnSc4d1	pouhé
porozumění	porozumění	k1gNnSc4	porozumění
slovem	slovo	k1gNnSc7	slovo
nebo	nebo	k8xC	nebo
písmem	písmo	k1gNnSc7	písmo
je	být	k5eAaImIp3nS	být
nezbytně	nezbytně	k6eAd1	nezbytně
nutná	nutný	k2eAgFnSc1d1	nutná
buďto	buďto	k8xC	buďto
jeho	jeho	k3xOp3gFnSc1	jeho
znalost	znalost	k1gFnSc1	znalost
nebo	nebo	k8xC	nebo
použití	použití	k1gNnSc1	použití
slovníku	slovník	k1gInSc2	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
není	být	k5eNaImIp3nS	být
hantec	hantec	k1gInSc1	hantec
běžně	běžně	k6eAd1	běžně
používán	používat	k5eAaImNgInS	používat
<g/>
,	,	kIx,	,
některá	některý	k3yIgNnPc4	některý
jeho	jeho	k3xOp3gNnPc4	jeho
slova	slovo	k1gNnPc4	slovo
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
dnes	dnes	k6eAd1	dnes
dobře	dobře	k6eAd1	dobře
známá	známý	k2eAgFnSc1d1	známá
a	a	k8xC	a
vesměs	vesměs	k6eAd1	vesměs
běžná	běžný	k2eAgFnSc1d1	běžná
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
výrazy	výraz	k1gInPc4	výraz
<g/>
:	:	kIx,	:
šalina	šalina	k1gFnSc1	šalina
(	(	kIx(	(
<g/>
tramvaj	tramvaj	k1gFnSc1	tramvaj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
štatl	štatnout	k5eAaPmAgMnS	štatnout
(	(	kIx(	(
<g/>
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
centrum	centrum	k1gNnSc1	centrum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
špinky	špinka	k1gFnPc1	špinka
(	(	kIx(	(
<g/>
cigarety	cigareta	k1gFnPc1	cigareta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Prágl	Prágl	k1gMnSc1	Prágl
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
škopek	škopek	k1gInSc1	škopek
(	(	kIx(	(
<g/>
pivo	pivo	k1gNnSc1	pivo
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
chálka	chálka	k1gFnSc1	chálka
(	(	kIx(	(
<g/>
jídlo	jídlo	k1gNnSc1	jídlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
peníze	peníz	k1gInPc4	peníz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
švestky	švestka	k1gFnSc2	švestka
(	(	kIx(	(
<g/>
policie	policie	k1gFnSc2	policie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokna	hokna	k1gFnSc1	hokna
(	(	kIx(	(
<g/>
práce	práce	k1gFnSc1	práce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mařka	mařka	k1gFnSc1	mařka
(	(	kIx(	(
<g/>
dívka	dívka	k1gFnSc1	dívka
<g/>
/	/	kIx~	/
<g/>
žena	žena	k1gFnSc1	žena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čórkař	čórkař	k1gMnSc1	čórkař
(	(	kIx(	(
<g/>
zloděj	zloděj	k1gMnSc1	zloděj
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kéma	kéma	k1gMnSc1	kéma
(	(	kIx(	(
<g/>
kamarád	kamarád	k1gMnSc1	kamarád
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
cajzl	cajznout	k5eAaPmAgMnS	cajznout
(	(	kIx(	(
<g/>
Pražan	Pražan	k1gMnSc1	Pražan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
slova	slovo	k1gNnSc2	slovo
cajzl	cajznout	k5eAaPmAgInS	cajznout
je	on	k3xPp3gMnPc4	on
složitější	složitý	k2eAgInSc1d2	složitější
<g/>
,	,	kIx,	,
s	s	k7c7	s
podobně	podobně	k6eAd1	podobně
znějícím	znějící	k2eAgNnSc7d1	znějící
slovem	slovo	k1gNnSc7	slovo
hajzl	hajzl	k1gMnSc1	hajzl
má	mít	k5eAaImIp3nS	mít
společného	společný	k2eAgMnSc4d1	společný
jen	jen	k8xS	jen
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Na	na	k7c6	na
katastru	katastr	k1gInSc6	katastr
Brna	Brno	k1gNnSc2	Brno
se	se	k3xPyFc4	se
částečně	částečně	k6eAd1	částečně
nachází	nacházet	k5eAaImIp3nS	nacházet
Masarykův	Masarykův	k2eAgInSc4d1	Masarykův
okruh	okruh	k1gInSc4	okruh
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
každoročně	každoročně	k6eAd1	každoročně
pořádána	pořádán	k2eAgFnSc1d1	pořádána
Velká	velký	k2eAgFnSc1d1	velká
cena	cena	k1gFnSc1	cena
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
silničních	silniční	k2eAgInPc2d1	silniční
motocyklů	motocykl	k1gInPc2	motocykl
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
velké	velký	k2eAgInPc1d1	velký
motoristické	motoristický	k2eAgInPc1d1	motoristický
závody	závod	k1gInPc1	závod
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
dostihová	dostihový	k2eAgFnSc1d1	dostihová
dráha	dráha	k1gFnSc1	dráha
Brno-Dvorska	Brno-Dvorsk	k1gInSc2	Brno-Dvorsk
<g/>
,	,	kIx,	,
sportovně	sportovně	k6eAd1	sportovně
využívané	využívaný	k2eAgNnSc4d1	využívané
letiště	letiště	k1gNnSc4	letiště
v	v	k7c6	v
Medlánkách	Medlánka	k1gFnPc6	Medlánka
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
zařízení	zařízení	k1gNnPc2	zařízení
pro	pro	k7c4	pro
vodní	vodní	k2eAgInPc4d1	vodní
sporty	sport	k1gInPc4	sport
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgNnPc2d1	další
sportovišť	sportoviště	k1gNnPc2	sportoviště
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
několik	několik	k4yIc4	několik
veřejných	veřejný	k2eAgFnPc2d1	veřejná
i	i	k8xC	i
neveřejných	veřejný	k2eNgFnPc2d1	neveřejná
fotbalových	fotbalový	k2eAgFnPc2d1	fotbalová
hřišť	hřiště	k1gNnPc2	hřiště
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgFnSc1d1	sportovní
hala	hala	k1gFnSc1	hala
Rondo	rondo	k1gNnSc4	rondo
(	(	kIx(	(
<g/>
DRFG	DRFG	kA	DRFG
Arena	Arena	k1gFnSc1	Arena
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgNnSc4d1	další
kluziště	kluziště	k1gNnSc4	kluziště
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
baseballových	baseballový	k2eAgInPc2d1	baseballový
stadionů	stadion	k1gInPc2	stadion
<g/>
,	,	kIx,	,
tělocvičen	tělocvičen	k2eAgInSc1d1	tělocvičen
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc4	několik
sportovních	sportovní	k2eAgInPc2d1	sportovní
klubů	klub	k1gInPc2	klub
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
většinou	většinou	k6eAd1	většinou
hrají	hrát	k5eAaImIp3nP	hrát
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
ligy	liga	k1gFnSc2	liga
daného	daný	k2eAgInSc2d1	daný
sportu	sport	k1gInSc2	sport
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgMnPc7d1	jiný
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
FC	FC	kA	FC
Zbrojovka	zbrojovka	k1gFnSc1	zbrojovka
Brno	Brno	k1gNnSc1	Brno
hrající	hrající	k2eAgFnSc1d1	hrající
na	na	k7c6	na
Městském	městský	k2eAgInSc6d1	městský
fotbalovém	fotbalový	k2eAgInSc6d1	fotbalový
stadionu	stadion	k1gInSc6	stadion
Srbská	srbský	k2eAgFnSc1d1	Srbská
<g/>
,	,	kIx,	,
tým	tým	k1gInSc1	tým
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
HC	HC	kA	HC
Kometa	kometa	k1gFnSc1	kometa
Brno	Brno	k1gNnSc1	Brno
hrající	hrající	k2eAgFnSc1d1	hrající
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
Rondo	rondo	k1gNnSc1	rondo
(	(	kIx(	(
<g/>
DRFG	DRFG	kA	DRFG
Arena	Arena	k1gFnSc1	Arena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
basketbalový	basketbalový	k2eAgInSc1d1	basketbalový
tým	tým	k1gInSc1	tým
Basket	basket	k1gInSc4	basket
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
ženské	ženský	k2eAgInPc1d1	ženský
basketbalové	basketbalový	k2eAgInPc1d1	basketbalový
týmy	tým	k1gInPc1	tým
Basket	basket	k1gInSc1	basket
Žabiny	Žabiny	k?	Žabiny
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
hrající	hrající	k2eAgMnSc1d1	hrající
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
hale	hala	k1gFnSc6	hala
Rosnička	rosnička	k1gFnSc1	rosnička
v	v	k7c6	v
Žabovřeskách	Žabovřesky	k1gFnPc6	Žabovřesky
<g/>
)	)	kIx)	)
a	a	k8xC	a
KP	KP	kA	KP
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc1	čtyři
baseballové	baseballový	k2eAgInPc1d1	baseballový
týmy	tým	k1gInPc1	tým
–	–	k?	–
AVG	AVG	kA	AVG
Draci	drak	k1gMnPc1	drak
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Hroši	hroch	k1gMnPc1	hroch
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
VSK	VSK	kA	VSK
Technika	technika	k1gFnSc1	technika
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
MZLU	MZLU	kA	MZLU
Express	express	k1gInSc1	express
Brno	Brno	k1gNnSc1	Brno
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
tým	tým	k1gInSc1	tým
amerického	americký	k2eAgInSc2d1	americký
fotbalu	fotbal	k1gInSc2	fotbal
Brno	Brno	k1gNnSc1	Brno
Alligators	Alligators	k1gInSc1	Alligators
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
ragbyové	ragbyový	k2eAgInPc1d1	ragbyový
kluby	klub	k1gInPc1	klub
RC	RC	kA	RC
Dragon	Dragon	k1gMnSc1	Dragon
Brno	Brno	k1gNnSc4	Brno
a	a	k8xC	a
RC	RC	kA	RC
Bystrc	Bystrc	k1gFnSc1	Bystrc
<g/>
,	,	kIx,	,
volejbalové	volejbalový	k2eAgInPc1d1	volejbalový
týmy	tým	k1gInPc1	tým
mužů	muž	k1gMnPc2	muž
(	(	kIx(	(
<g/>
Volejbal	volejbal	k1gInSc1	volejbal
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
(	(	kIx(	(
<g/>
VK	VK	kA	VK
KP	KP	kA	KP
Brno	Brno	k1gNnSc4	Brno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
florbalový	florbalový	k2eAgInSc1d1	florbalový
klub	klub	k1gInSc1	klub
Bulldogs	Bulldogsa	k1gFnPc2	Bulldogsa
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
klub	klub	k1gInSc1	klub
národní	národní	k2eAgFnSc2d1	národní
házené	házená	k1gFnSc2	házená
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
NH	NH	kA	NH
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
lakrosový	lakrosový	k2eAgInSc1d1	lakrosový
klub	klub	k1gInSc1	klub
Brno	Brno	k1gNnSc1	Brno
Ravens	Ravensa	k1gFnPc2	Ravensa
LC	LC	kA	LC
<g/>
,	,	kIx,	,
atletické	atletický	k2eAgInPc4d1	atletický
kluby	klub	k1gInPc4	klub
VSK	VSK	kA	VSK
Univerzita	univerzita	k1gFnSc1	univerzita
Brno	Brno	k1gNnSc4	Brno
(	(	kIx(	(
<g/>
muži	muž	k1gMnSc6	muž
<g/>
)	)	kIx)	)
a	a	k8xC	a
AK	AK	kA	AK
Olymp	Olymp	k1gInSc1	Olymp
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
ženy	žena	k1gFnPc1	žena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
probíhají	probíhat	k5eAaImIp3nP	probíhat
i	i	k9	i
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
sportovní	sportovní	k2eAgFnPc1d1	sportovní
soutěže	soutěž	k1gFnPc1	soutěž
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
<g/>
:	:	kIx,	:
Grand	grand	k1gMnSc1	grand
Prix	Prix	k1gInSc4	Prix
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
silničních	silniční	k2eAgInPc2d1	silniční
motocyklů	motocykl	k1gInPc2	motocykl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Evropský	evropský	k2eAgInSc1d1	evropský
pohár	pohár	k1gInSc1	pohár
v	v	k7c6	v
triatlonu	triatlon	k1gInSc6	triatlon
<g/>
,	,	kIx,	,
Mistrovství	mistrovství	k1gNnSc1	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
kadetů	kadet	k1gMnPc2	kadet
v	v	k7c6	v
baseballu	baseball	k1gInSc6	baseball
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
tenisový	tenisový	k2eAgInSc4d1	tenisový
turnaj	turnaj	k1gInSc4	turnaj
žen	žena	k1gFnPc2	žena
WTA	WTA	kA	WTA
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
cena	cena	k1gFnSc1	cena
Brna	Brno	k1gNnSc2	Brno
CDI-W	CDI-W	k1gFnSc2	CDI-W
(	(	kIx(	(
<g/>
světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
v	v	k7c6	v
drezuře	drezura	k1gFnSc6	drezura
koní	kůň	k1gMnPc2	kůň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fed	Fed	k1gFnSc1	Fed
Cup	cup	k1gInSc1	cup
v	v	k7c6	v
tenise	tenis	k1gInSc6	tenis
žen	žena	k1gFnPc2	žena
ČR	ČR	kA	ČR
<g/>
–	–	k?	–
<g/>
Španělsko	Španělsko	k1gNnSc1	Španělsko
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
Fed	Fed	k1gFnSc1	Fed
Cup	cup	k1gInSc1	cup
v	v	k7c6	v
tenise	tenis	k1gInSc6	tenis
žen	žena	k1gFnPc2	žena
ČR	ČR	kA	ČR
<g/>
–	–	k?	–
<g/>
USA	USA	kA	USA
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2009	[number]	k4	2009
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgNnPc2d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Architektura	architektura	k1gFnSc1	architektura
a	a	k8xC	a
charakter	charakter	k1gInSc1	charakter
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
obklopené	obklopený	k2eAgNnSc1d1	obklopené
řekami	řeka	k1gFnPc7	řeka
Svratkou	Svratka	k1gFnSc7	Svratka
a	a	k8xC	a
Svitavou	Svitava	k1gFnSc7	Svitava
<g/>
,	,	kIx,	,
rozkládající	rozkládající	k2eAgFnSc7d1	rozkládající
se	se	k3xPyFc4	se
pod	pod	k7c4	pod
kopci	kopec	k1gInSc6	kopec
Petrov	Petrov	k1gInSc1	Petrov
a	a	k8xC	a
Špilberk	Špilberk	k1gInSc1	Špilberk
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
formovat	formovat	k5eAaImF	formovat
již	již	k6eAd1	již
začátkem	začátkem	k7c2	začátkem
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc7	jeho
nejvýraznějšími	výrazný	k2eAgFnPc7d3	nejvýraznější
dominantami	dominanta	k1gFnPc7	dominanta
jsou	být	k5eAaImIp3nP	být
hrad	hrad	k1gInSc4	hrad
Špilberk	Špilberk	k1gInSc1	Špilberk
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k8xS	jako
královský	královský	k2eAgInSc4d1	královský
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
a	a	k8xC	a
katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
a	a	k8xC	a
Pavla	Pavel	k1gMnSc2	Pavel
obklopena	obklopit	k5eAaPmNgFnS	obklopit
domy	dům	k1gInPc1	dům
královské	královský	k2eAgFnSc2d1	královská
stoliční	stoliční	k2eAgFnSc2d1	stoliční
kapituly	kapitula	k1gFnSc2	kapitula
<g/>
,	,	kIx,	,
její	její	k3xOp3gInPc1	její
počátky	počátek	k1gInPc1	počátek
sahají	sahat	k5eAaImIp3nP	sahat
až	až	k9	až
do	do	k7c2	do
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
jádru	jádro	k1gNnSc6	jádro
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
velkých	velká	k1gFnPc2	velká
a	a	k8xC	a
bohatě	bohatě	k6eAd1	bohatě
zdobených	zdobený	k2eAgInPc2d1	zdobený
kostelů	kostel	k1gInPc2	kostel
<g/>
,	,	kIx,	,
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
,	,	kIx,	,
paláců	palác	k1gInPc2	palác
<g/>
,	,	kIx,	,
pasáží	pasáž	k1gFnPc2	pasáž
<g/>
,	,	kIx,	,
kašen	kašna	k1gFnPc2	kašna
<g/>
,	,	kIx,	,
fontán	fontána	k1gFnPc2	fontána
<g/>
,	,	kIx,	,
soch	socha	k1gFnPc2	socha
<g/>
,	,	kIx,	,
věží	věž	k1gFnPc2	věž
a	a	k8xC	a
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
různých	různý	k2eAgInPc2d1	různý
pozoruhodných	pozoruhodný	k2eAgInPc2d1	pozoruhodný
architektonických	architektonický	k2eAgInPc2d1	architektonický
detailů	detail	k1gInPc2	detail
<g/>
.	.	kIx.	.
</s>
<s>
Budovy	budova	k1gFnPc1	budova
jsou	být	k5eAaImIp3nP	být
vystavěny	vystavět	k5eAaPmNgFnP	vystavět
v	v	k7c6	v
nejrůznějších	různý	k2eAgInPc6d3	nejrůznější
architektonických	architektonický	k2eAgInPc6d1	architektonický
slozích	sloh	k1gInPc6	sloh
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
tvoří	tvořit	k5eAaImIp3nP	tvořit
uzavřené	uzavřený	k2eAgInPc4d1	uzavřený
kontinuální	kontinuální	k2eAgInPc4d1	kontinuální
bloky	blok	k1gInPc4	blok
přibližně	přibližně	k6eAd1	přibližně
stejné	stejný	k2eAgFnPc4d1	stejná
stavební	stavební	k2eAgFnPc4d1	stavební
výšky	výška	k1gFnPc4	výška
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnPc1	jejich
fasády	fasáda	k1gFnPc1	fasáda
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
ve	v	k7c6	v
dobrém	dobrý	k2eAgInSc6d1	dobrý
stavu	stav	k1gInSc6	stav
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zdobeny	zdobit	k5eAaImNgInP	zdobit
arkýři	arkýř	k1gInPc7	arkýř
<g/>
,	,	kIx,	,
terasami	terasa	k1gFnPc7	terasa
<g/>
,	,	kIx,	,
rizality	rizalit	k1gInPc7	rizalit
<g/>
,	,	kIx,	,
helmicemi	helmice	k1gFnPc7	helmice
<g/>
,	,	kIx,	,
kupolemi	kupole	k1gFnPc7	kupole
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
ulic	ulice	k1gFnPc2	ulice
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
tvořen	tvořit	k5eAaImNgInS	tvořit
kamennou	kamenný	k2eAgFnSc7d1	kamenná
dlažbou	dlažba	k1gFnSc7	dlažba
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
jádra	jádro	k1gNnSc2	jádro
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
pěší	pěší	k2eAgFnSc7d1	pěší
zónou	zóna	k1gFnSc7	zóna
s	s	k7c7	s
minimální	minimální	k2eAgFnSc7d1	minimální
dopravou	doprava	k1gFnSc7	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgInPc1d1	různý
drobné	drobný	k2eAgInPc1d1	drobný
architektonické	architektonický	k2eAgInPc1d1	architektonický
prvky	prvek	k1gInPc1	prvek
a	a	k8xC	a
nebo	nebo	k8xC	nebo
např.	např.	kA	např.
obelisk	obelisk	k1gInSc1	obelisk
a	a	k8xC	a
kolonáda	kolonáda	k1gFnSc1	kolonáda
s	s	k7c7	s
kašnou	kašna	k1gFnSc7	kašna
v	v	k7c6	v
Denisových	Denisový	k2eAgInPc6d1	Denisový
sadech	sad	k1gInPc6	sad
<g/>
,	,	kIx,	,
či	či	k8xC	či
podzemní	podzemní	k2eAgFnPc1d1	podzemní
prostory	prostora	k1gFnPc1	prostora
(	(	kIx(	(
<g/>
brněnská	brněnský	k2eAgFnSc1d1	brněnská
kostnice	kostnice	k1gFnSc1	kostnice
<g/>
,	,	kIx,	,
mumie	mumie	k1gFnSc1	mumie
v	v	k7c6	v
kapucínské	kapucínský	k2eAgFnSc6d1	Kapucínská
kryptě	krypta	k1gFnSc6	krypta
<g/>
,	,	kIx,	,
Mincmistrovský	mincmistrovský	k2eAgInSc1d1	mincmistrovský
sklep	sklep	k1gInSc1	sklep
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
zóně	zóna	k1gFnSc6	zóna
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
až	až	k9	až
tří	tři	k4xCgInPc2	tři
kilometrů	kilometr	k1gInPc2	kilometr
severně	severně	k6eAd1	severně
od	od	k7c2	od
historického	historický	k2eAgNnSc2d1	historické
jádra	jádro	k1gNnSc2	jádro
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
kompaktní	kompaktní	k2eAgFnSc1d1	kompaktní
ulicová	ulicový	k2eAgFnSc1d1	ulicová
zástavba	zástavba	k1gFnSc1	zástavba
z	z	k7c2	z
průběhu	průběh	k1gInSc2	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
s	s	k7c7	s
velkými	velký	k2eAgInPc7d1	velký
bloky	blok	k1gInPc7	blok
bohatě	bohatě	k6eAd1	bohatě
zdobených	zdobený	k2eAgMnPc2d1	zdobený
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
skupiny	skupina	k1gFnSc2	skupina
honosných	honosný	k2eAgInPc2d1	honosný
obytných	obytný	k2eAgInPc2d1	obytný
domů	dům	k1gInPc2	dům
Tivoli	Tivole	k1gFnSc4	Tivole
na	na	k7c6	na
Konečného	Konečného	k2eAgNnSc6d1	Konečného
náměstí	náměstí	k1gNnSc6	náměstí
a	a	k8xC	a
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
vil	vila	k1gFnPc2	vila
a	a	k8xC	a
rodinných	rodinný	k2eAgInPc2d1	rodinný
domů	dům	k1gInPc2	dům
včetně	včetně	k7c2	včetně
vily	vila	k1gFnSc2	vila
Tugendhat	Tugendhat	k1gFnSc2	Tugendhat
<g/>
,	,	kIx,	,
zapsané	zapsaný	k2eAgNnSc1d1	zapsané
do	do	k7c2	do
listiny	listina	k1gFnSc2	listina
světového	světový	k2eAgNnSc2d1	světové
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
zejména	zejména	k9	zejména
o	o	k7c4	o
městské	městský	k2eAgFnPc4d1	městská
čtvrti	čtvrt	k1gFnPc4	čtvrt
Veveří	veveří	k2eAgFnSc1d1	veveří
<g/>
,	,	kIx,	,
Černá	černý	k2eAgFnSc1d1	černá
Pole	pole	k1gFnSc1	pole
<g/>
,	,	kIx,	,
Královo	Králův	k2eAgNnSc1d1	Královo
Pole	pole	k1gNnSc1	pole
a	a	k8xC	a
Žabovřesky	Žabovřesky	k1gFnPc1	Žabovřesky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
okrajích	okrají	k1gNnPc6	okrají
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
sídliště	sídliště	k1gNnSc2	sídliště
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
panelových	panelový	k2eAgInPc2d1	panelový
domů	dům	k1gInPc2	dům
(	(	kIx(	(
<g/>
Lesná	lesný	k2eAgFnSc1d1	Lesná
<g/>
,	,	kIx,	,
Líšeň	Líšeň	k1gFnSc1	Líšeň
<g/>
,	,	kIx,	,
Bystrc	Bystrc	k1gFnSc1	Bystrc
<g/>
,	,	kIx,	,
Kamenný	kamenný	k2eAgInSc1d1	kamenný
Vrch	vrch	k1gInSc1	vrch
<g/>
,	,	kIx,	,
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc1	množství
modernějších	moderní	k2eAgFnPc2d2	modernější
budov	budova	k1gFnPc2	budova
a	a	k8xC	a
areálů	areál	k1gInPc2	areál
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
výšková	výškový	k2eAgFnSc1d1	výšková
budova	budova	k1gFnSc1	budova
Fakultní	fakultní	k2eAgFnSc2d1	fakultní
nemocnice	nemocnice	k1gFnSc2	nemocnice
Brno	Brno	k1gNnSc1	Brno
ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
Lískovci	Lískovec	k1gInSc6	Lískovec
nebo	nebo	k8xC	nebo
kampus	kampus	k1gInSc1	kampus
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Bohunicích	Bohunice	k1gFnPc6	Bohunice
(	(	kIx(	(
<g/>
největším	veliký	k2eAgMnSc7d3	veliký
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
)	)	kIx)	)
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnSc6	jehož
blízkosti	blízkost	k1gFnSc6	blízkost
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
obchodní	obchodní	k2eAgNnSc1d1	obchodní
centrum	centrum	k1gNnSc1	centrum
Campus	Campus	k1gMnSc1	Campus
Square	square	k1gInSc1	square
a	a	k8xC	a
Moravský	moravský	k2eAgInSc1d1	moravský
zemský	zemský	k2eAgInSc1d1	zemský
archiv	archiv	k1gInSc1	archiv
<g/>
.	.	kIx.	.
</s>
<s>
Jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
prudce	prudko	k6eAd1	prudko
se	se	k3xPyFc4	se
rozvíjející	rozvíjející	k2eAgFnSc1d1	rozvíjející
oblast	oblast	k1gFnSc1	oblast
výškových	výškový	k2eAgFnPc2d1	výšková
kancelářských	kancelářský	k2eAgFnPc2d1	kancelářská
budov	budova	k1gFnPc2	budova
podél	podél	k7c2	podél
ul	ul	kA	ul
<g/>
.	.	kIx.	.
Heršpické	heršpický	k2eAgFnPc1d1	Heršpická
(	(	kIx(	(
<g/>
M-Palác	M-Palác	k1gInSc1	M-Palác
<g/>
,	,	kIx,	,
Spielberk	Spielberk	k1gInSc1	Spielberk
Towers	Towers	k1gInSc1	Towers
<g/>
,	,	kIx,	,
AZ	AZ	kA	AZ
Tower	Tower	k1gInSc1	Tower
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výškové	výškový	k2eAgFnPc1d1	výšková
budovy	budova	k1gFnPc1	budova
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
také	také	k9	také
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
s	s	k7c7	s
Královým	Králův	k2eAgNnSc7d1	Královo
Polem	pole	k1gNnSc7	pole
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
skupinu	skupina	k1gFnSc4	skupina
výškových	výškový	k2eAgFnPc2d1	výšková
budov	budova	k1gFnPc2	budova
kancelářského	kancelářský	k2eAgInSc2d1	kancelářský
komplexu	komplex	k1gInSc2	komplex
v	v	k7c6	v
Šumavské	šumavský	k2eAgFnSc6d1	Šumavská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc4d1	další
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Králově	Králův	k2eAgNnSc6d1	Královo
Poli	pole	k1gNnSc6	pole
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
VUT	VUT	kA	VUT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
Lesné	lesný	k2eAgFnPc4d1	Lesná
a	a	k8xC	a
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Především	především	k9	především
jižně	jižně	k6eAd1	jižně
až	až	k9	až
východně	východně	k6eAd1	východně
od	od	k7c2	od
středu	střed	k1gInSc2	střed
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
opuštěných	opuštěný	k2eAgFnPc2d1	opuštěná
a	a	k8xC	a
chátrajících	chátrající	k2eAgFnPc2d1	chátrající
především	především	k6eAd1	především
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	a
areálů	areál	k1gInPc2	areál
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
brownfieldů	brownfield	k1gInPc2	brownfield
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
problém	problém	k1gInSc1	problém
je	být	k5eAaImIp3nS	být
postupně	postupně	k6eAd1	postupně
řešen	řešit	k5eAaImNgInS	řešit
<g/>
,	,	kIx,	,
a	a	k8xC	a
objekty	objekt	k1gInPc1	objekt
bývají	bývat	k5eAaImIp3nP	bývat
buď	buď	k8xC	buď
likvidovány	likvidován	k2eAgInPc1d1	likvidován
nebo	nebo	k8xC	nebo
rekonstruovány	rekonstruován	k2eAgInPc1d1	rekonstruován
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
bývalá	bývalý	k2eAgFnSc1d1	bývalá
továrna	továrna	k1gFnSc1	továrna
Vaňkovka	Vaňkovka	k1gFnSc1	Vaňkovka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
vnitřního	vnitřní	k2eAgNnSc2d1	vnitřní
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
čtvrť	čtvrtit	k5eAaImRp2nS	čtvrtit
Zábrdovice	Zábrdovice	k1gFnPc4	Zábrdovice
–	–	k?	–
ulice	ulice	k1gFnSc2	ulice
Cejl	Cejla	k1gFnPc2	Cejla
<g/>
,	,	kIx,	,
Bratislavská	bratislavský	k2eAgFnSc1d1	Bratislavská
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
můžeme	moct	k5eAaImIp1nP	moct
běžně	běžně	k6eAd1	běžně
najít	najít	k5eAaPmF	najít
zchátralé	zchátralý	k2eAgInPc4d1	zchátralý
velkoměstské	velkoměstský	k2eAgInPc4d1	velkoměstský
domy	dům	k1gInPc4	dům
<g/>
,	,	kIx,	,
špinavé	špinavý	k2eAgFnPc4d1	špinavá
ulice	ulice	k1gFnPc4	ulice
a	a	k8xC	a
zanedbaná	zanedbaný	k2eAgNnPc4d1	zanedbané
prostranství	prostranství	k1gNnPc4	prostranství
<g/>
;	;	kIx,	;
obyvatelé	obyvatel	k1gMnPc1	obyvatel
této	tento	k3xDgFnSc2	tento
části	část	k1gFnSc2	část
města	město	k1gNnSc2	město
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
Romové	Rom	k1gMnPc1	Rom
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
často	často	k6eAd1	často
byli	být	k5eAaImAgMnP	být
nastěhováni	nastěhovat	k5eAaPmNgMnP	nastěhovat
do	do	k7c2	do
domů	dům	k1gInPc2	dům
po	po	k7c6	po
vysídlených	vysídlený	k2eAgMnPc6d1	vysídlený
Němcích	Němec	k1gMnPc6	Němec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Městské	městský	k2eAgInPc1d1	městský
parky	park	k1gInPc1	park
a	a	k8xC	a
náměstí	náměstí	k1gNnSc1	náměstí
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
parků	park	k1gInPc2	park
různých	různý	k2eAgInPc2d1	různý
velikostí	velikost	k1gFnSc7	velikost
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
nejbohatší	bohatý	k2eAgNnSc4d3	nejbohatší
svým	svůj	k3xOyFgNnSc7	svůj
vybavením	vybavení	k1gNnSc7	vybavení
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
největší	veliký	k2eAgInSc1d3	veliký
patří	patřit	k5eAaImIp3nS	patřit
park	park	k1gInSc1	park
Lužánky	Lužánka	k1gFnSc2	Lužánka
<g/>
,	,	kIx,	,
otevřený	otevřený	k2eAgInSc1d1	otevřený
veřejnosti	veřejnost	k1gFnSc3	veřejnost
roku	rok	k1gInSc2	rok
1786	[number]	k4	1786
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
první	první	k4xOgInSc1	první
veřejný	veřejný	k2eAgInSc1d1	veřejný
park	park	k1gInSc1	park
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
kopcový	kopcový	k2eAgInSc1d1	kopcový
park	park	k1gInSc1	park
Špilberk	Špilberk	k1gInSc1	Špilberk
pod	pod	k7c7	pod
stejnojmenným	stejnojmenný	k2eAgInSc7d1	stejnojmenný
hradem	hrad	k1gInSc7	hrad
či	či	k8xC	či
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
lesopark	lesopark	k1gInSc4	lesopark
Wilsonův	Wilsonův	k2eAgInSc4d1	Wilsonův
les	les	k1gInSc4	les
<g/>
.	.	kIx.	.
</s>
<s>
Parky	park	k1gInPc1	park
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
vybaveny	vybavit	k5eAaPmNgInP	vybavit
množstvím	množství	k1gNnSc7	množství
soch	socha	k1gFnPc2	socha
<g/>
,	,	kIx,	,
fontán	fontána	k1gFnPc2	fontána
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
prvků	prvek	k1gInPc2	prvek
zpříjemňujících	zpříjemňující	k2eAgInPc2d1	zpříjemňující
atmosféru	atmosféra	k1gFnSc4	atmosféra
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povšimnutí	povšimnutí	k1gNnSc6	povšimnutí
stojí	stát	k5eAaImIp3nS	stát
například	například	k6eAd1	například
umělý	umělý	k2eAgInSc1d1	umělý
potůček	potůček	k1gInSc1	potůček
nebo	nebo	k8xC	nebo
dopravní	dopravní	k2eAgNnPc1d1	dopravní
hřiště	hřiště	k1gNnPc1	hřiště
v	v	k7c6	v
Lužánkách	Lužánka	k1gFnPc6	Lužánka
<g/>
,	,	kIx,	,
skalka	skalka	k1gFnSc1	skalka
a	a	k8xC	a
vyhlídkový	vyhlídkový	k2eAgInSc1d1	vyhlídkový
altán	altán	k1gInSc1	altán
v	v	k7c6	v
parku	park	k1gInSc6	park
Špilberk	Špilberk	k1gInSc1	Špilberk
<g/>
,	,	kIx,	,
fontány	fontána	k1gFnPc1	fontána
v	v	k7c6	v
parku	park	k1gInSc6	park
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
náměstí	náměstí	k1gNnSc6	náměstí
nebo	nebo	k8xC	nebo
kolonáda	kolonáda	k1gFnSc1	kolonáda
a	a	k8xC	a
obelisk	obelisk	k1gInSc1	obelisk
v	v	k7c6	v
Denisových	Denisový	k2eAgInPc6d1	Denisový
sadech	sad	k1gInPc6	sad
na	na	k7c6	na
Petrově	Petrův	k2eAgNnSc6d1	Petrovo
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vůbec	vůbec	k9	vůbec
prvním	první	k4xOgInSc7	první
veřejným	veřejný	k2eAgInSc7d1	veřejný
parkem	park	k1gInSc7	park
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
Čechách	Čechy	k1gFnPc6	Čechy
zřízeným	zřízený	k2eAgNnSc7d1	zřízené
<g />
.	.	kIx.	.
</s>
<s>
veřejnou	veřejný	k2eAgFnSc7d1	veřejná
správou	správa	k1gFnSc7	správa
<g/>
.	.	kIx.	.
<g/>
Nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
historická	historický	k2eAgFnSc1d1	historická
náměstí	náměstí	k1gNnSc1	náměstí
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
jsou	být	k5eAaImIp3nP	být
náměstí	náměstí	k1gNnSc4	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Dolní	dolní	k2eAgInSc1d1	dolní
trh	trh	k1gInSc1	trh
<g/>
)	)	kIx)	)
a	a	k8xC	a
Zelný	zelný	k2eAgInSc4d1	zelný
trh	trh	k1gInSc4	trh
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Horní	horní	k2eAgInSc1d1	horní
trh	trh	k1gInSc1	trh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
centru	centrum	k1gNnSc6	centrum
Brna	Brno	k1gNnSc2	Brno
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
nalézá	nalézat	k5eAaImIp3nS	nalézat
Dominikánské	dominikánský	k2eAgNnSc4d1	Dominikánské
náměstí	náměstí	k1gNnSc4	náměstí
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Rybí	rybí	k2eAgInSc1d1	rybí
trh	trh	k1gInSc1	trh
<g/>
)	)	kIx)	)
s	s	k7c7	s
komplexem	komplex	k1gInSc7	komplex
barokních	barokní	k2eAgFnPc2d1	barokní
budov	budova	k1gFnPc2	budova
Nové	Nové	k2eAgFnSc2d1	Nové
radnice	radnice	k1gFnSc2	radnice
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Zemského	zemský	k2eAgInSc2d1	zemský
domu	dům	k1gInSc2	dům
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Šilingrovo	Šilingrův	k2eAgNnSc1d1	Šilingrovo
náměstí	náměstí	k1gNnSc1	náměstí
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Svinský	svinský	k2eAgInSc1d1	svinský
trh	trh	k1gInSc1	trh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
bývalé	bývalý	k2eAgFnSc2d1	bývalá
brány	brána	k1gFnSc2	brána
brněnského	brněnský	k2eAgNnSc2d1	brněnské
opevnění	opevnění	k1gNnSc2	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
náměstím	náměstí	k1gNnSc7	náměstí
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
je	být	k5eAaImIp3nS	být
Moravské	moravský	k2eAgNnSc1d1	Moravské
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
vybaveno	vybavit	k5eAaPmNgNnS	vybavit
bronzovým	bronzový	k2eAgInSc7d1	bronzový
modelem	model	k1gInSc7	model
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
město	město	k1gNnSc4	město
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
době	doba	k1gFnSc6	doba
neúspěšného	úspěšný	k2eNgNnSc2d1	neúspěšné
obléhání	obléhání	k1gNnSc2	obléhání
Brna	Brno	k1gNnSc2	Brno
Švédy	švéda	k1gFnSc2	švéda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
celkově	celkově	k6eAd1	celkově
desítky	desítka	k1gFnPc4	desítka
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
,	,	kIx,	,
některá	některý	k3yIgFnSc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
svým	svůj	k3xOyFgInSc7	svůj
charakterem	charakter	k1gInSc7	charakter
připomínají	připomínat	k5eAaImIp3nP	připomínat
parky	park	k1gInPc1	park
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Slovanské	slovanský	k2eAgNnSc4d1	slovanské
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
dokola	dokola	k6eAd1	dokola
obklopené	obklopený	k2eAgNnSc1d1	obklopené
alejí	alej	k1gFnSc7	alej
<g/>
,	,	kIx,	,
Obilní	obilní	k2eAgInSc4d1	obilní
trh	trh	k1gInSc4	trh
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
i	i	k9	i
Moravské	moravský	k2eAgNnSc1d1	Moravské
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Hrady	hrad	k1gInPc1	hrad
a	a	k8xC	a
zámky	zámek	k1gInPc1	zámek
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dva	dva	k4xCgInPc4	dva
velké	velký	k2eAgInPc4d1	velký
a	a	k8xC	a
staré	starý	k2eAgInPc4d1	starý
hradní	hradní	k2eAgInPc4d1	hradní
komplexy	komplex	k1gInPc4	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
hrad	hrad	k1gInSc4	hrad
Špilberk	Špilberk	k1gInSc4	Špilberk
nad	nad	k7c7	nad
historickým	historický	k2eAgNnSc7d1	historické
jádrem	jádro	k1gNnSc7	jádro
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k8xC	jako
přední	přední	k2eAgInSc4d1	přední
královský	královský	k2eAgInSc4d1	královský
hrad	hrad	k1gInSc4	hrad
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Špilberk	Špilberk	k1gInSc1	Špilberk
byl	být	k5eAaImAgInS	být
kdysi	kdysi	k6eAd1	kdysi
trvalým	trvalý	k2eAgNnSc7d1	trvalé
sídlem	sídlo	k1gNnSc7	sídlo
moravských	moravský	k2eAgNnPc2d1	Moravské
markrabat	markrabě	k1gNnPc2	markrabě
<g/>
,	,	kIx,	,
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
vládců	vládce	k1gMnPc2	vládce
Markrabství	markrabství	k1gNnSc2	markrabství
moravského	moravský	k2eAgNnSc2d1	Moravské
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
sídlem	sídlo	k1gNnSc7	sídlo
krále	král	k1gMnSc2	král
celé	celá	k1gFnSc2	celá
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
,	,	kIx,	,
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
hrad	hrad	k1gInSc1	hrad
přestavěn	přestavět	k5eAaPmNgInS	přestavět
na	na	k7c4	na
barokní	barokní	k2eAgFnSc4d1	barokní
citadelu	citadela	k1gFnSc4	citadela
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
je	být	k5eAaImIp3nS	být
hrad	hrad	k1gInSc1	hrad
Veveří	veveří	k2eAgInSc1d1	veveří
nad	nad	k7c7	nad
Brněnskou	brněnský	k2eAgFnSc7d1	brněnská
přehradou	přehrada	k1gFnSc7	přehrada
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
založen	založit	k5eAaPmNgInS	založit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1059	[number]	k4	1059
brněnským	brněnský	k2eAgMnSc7d1	brněnský
údělným	údělný	k2eAgMnSc7d1	údělný
knížetem	kníže	k1gMnSc7	kníže
Konrádem	Konrád	k1gMnSc7	Konrád
I.	I.	kA	I.
Hrad	hrad	k1gInSc1	hrad
a	a	k8xC	a
pevnost	pevnost	k1gFnSc1	pevnost
Špilberk	Špilberk	k1gInSc1	Špilberk
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
Muzea	muzeum	k1gNnSc2	muzeum
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
se	s	k7c7	s
stálými	stálý	k2eAgFnPc7d1	stálá
expozicemi	expozice	k1gFnPc7	expozice
a	a	k8xC	a
také	také	k9	také
častým	častý	k2eAgNnSc7d1	časté
dějištěm	dějiště	k1gNnSc7	dějiště
různých	různý	k2eAgInPc2d1	různý
kulturní	kulturní	k2eAgFnSc7d1	kulturní
událostí	událost	k1gFnSc7	událost
<g/>
,	,	kIx,	,
Špilberk	Špilberk	k1gInSc1	Špilberk
je	být	k5eAaImIp3nS	být
i	i	k9	i
s	s	k7c7	s
parkem	park	k1gInSc7	park
okolo	okolo	k6eAd1	okolo
národní	národní	k2eAgFnSc1d1	národní
kulturní	kulturní	k2eAgFnSc7d1	kulturní
památkou	památka	k1gFnSc7	památka
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
hrad	hrad	k1gInSc1	hrad
Veveří	veveří	k2eAgInSc1d1	veveří
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
špatné	špatný	k2eAgFnSc6d1	špatná
údržbě	údržba	k1gFnSc6	údržba
a	a	k8xC	a
necitlivých	citlivý	k2eNgFnPc6d1	necitlivá
přestavbách	přestavba	k1gFnPc6	přestavba
za	za	k7c2	za
socialismu	socialismus	k1gInSc2	socialismus
a	a	k8xC	a
chátrání	chátrání	k1gNnSc2	chátrání
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ve	v	k7c6	v
špatném	špatný	k2eAgInSc6d1	špatný
technickém	technický	k2eAgInSc6d1	technický
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Hrad	hrad	k1gInSc1	hrad
byl	být	k5eAaImAgInS	být
zpřístupněn	zpřístupnit	k5eAaPmNgInS	zpřístupnit
veřejnosti	veřejnost	k1gFnSc3	veřejnost
teprve	teprve	k6eAd1	teprve
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
také	také	k9	také
probíhá	probíhat	k5eAaImIp3nS	probíhat
řada	řada	k1gFnSc1	řada
oprav	oprava	k1gFnPc2	oprava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
navrátit	navrátit	k5eAaPmF	navrátit
památku	památka	k1gFnSc4	památka
do	do	k7c2	do
původního	původní	k2eAgInSc2d1	původní
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
katastru	katastr	k1gInSc6	katastr
města	město	k1gNnSc2	město
lze	lze	k6eAd1	lze
také	také	k9	také
nalézt	nalézt	k5eAaPmF	nalézt
také	také	k9	také
několik	několik	k4yIc4	několik
zámků	zámek	k1gInPc2	zámek
vystavěných	vystavěný	k2eAgInPc2d1	vystavěný
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
stylech	styl	k1gInPc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
klasicistní	klasicistní	k2eAgInSc4d1	klasicistní
zámek	zámek	k1gInSc4	zámek
v	v	k7c6	v
Líšni	Líšeň	k1gFnSc6	Líšeň
přestavěný	přestavěný	k2eAgInSc4d1	přestavěný
začátkem	začátkem	k7c2	začátkem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
z	z	k7c2	z
tvrze	tvrz	k1gFnSc2	tvrz
Janem	Jan	k1gMnSc7	Jan
Kryštofem	Kryštof	k1gMnSc7	Kryštof
z	z	k7c2	z
Freynfelsu	Freynfels	k1gInSc2	Freynfels
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
barokní	barokní	k2eAgInSc4d1	barokní
chrlický	chrlický	k2eAgInSc4d1	chrlický
zámek	zámek	k1gInSc4	zámek
založený	založený	k2eAgInSc4d1	založený
olomouckými	olomoucký	k2eAgMnPc7d1	olomoucký
biskupy	biskup	k1gMnPc7	biskup
roku	rok	k1gInSc2	rok
1561	[number]	k4	1561
<g/>
,	,	kIx,	,
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
renesančně	renesančně	k6eAd1	renesančně
upravený	upravený	k2eAgInSc4d1	upravený
řečkovický	řečkovický	k2eAgInSc4d1	řečkovický
zámek	zámek	k1gInSc4	zámek
<g/>
,	,	kIx,	,
medlánecký	medlánecký	k2eAgInSc4d1	medlánecký
zámek	zámek	k1gInSc4	zámek
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc4	zámek
v	v	k7c6	v
Brněnských	brněnský	k2eAgFnPc6d1	brněnská
Ivanovicích	Ivanovice	k1gFnPc6	Ivanovice
<g/>
,	,	kIx,	,
letohrádek	letohrádek	k1gInSc1	letohrádek
Mitrovských	Mitrovský	k2eAgInPc2d1	Mitrovský
na	na	k7c6	na
Starém	starý	k2eAgNnSc6d1	staré
Brně	Brno	k1gNnSc6	Brno
či	či	k8xC	či
Bauerův	Bauerův	k2eAgInSc4d1	Bauerův
zámeček	zámeček	k1gInSc4	zámeček
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
brněnského	brněnský	k2eAgNnSc2d1	brněnské
výstaviště	výstaviště	k1gNnSc2	výstaviště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Brněnské	brněnský	k2eAgNnSc1d1	brněnské
podzemí	podzemí	k1gNnSc1	podzemí
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Především	především	k9	především
měšťanská	měšťanský	k2eAgNnPc1d1	měšťanské
sklepení	sklepení	k1gNnPc1	sklepení
tvoří	tvořit	k5eAaImIp3nP	tvořit
veřejnosti	veřejnost	k1gFnSc3	veřejnost
částečně	částečně	k6eAd1	částečně
přístupné	přístupný	k2eAgNnSc4d1	přístupné
podzemí	podzemí	k1gNnSc4	podzemí
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
sklepy	sklep	k1gInPc1	sklep
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
pod	pod	k7c7	pod
Zelným	zelný	k2eAgInSc7d1	zelný
trhem	trh	k1gInSc7	trh
<g/>
,	,	kIx,	,
Dominikánským	dominikánský	k2eAgNnSc7d1	Dominikánské
náměstím	náměstí	k1gNnSc7	náměstí
<g/>
,	,	kIx,	,
Římským	římský	k2eAgNnSc7d1	římské
náměstím	náměstí	k1gNnSc7	náměstí
<g/>
,	,	kIx,	,
Moravským	moravský	k2eAgNnSc7d1	Moravské
náměstím	náměstí	k1gNnSc7	náměstí
<g/>
,	,	kIx,	,
Petrovem	Petrov	k1gInSc7	Petrov
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
ulicí	ulice	k1gFnSc7	ulice
Orlí	orlí	k2eAgFnSc7d1	orlí
a	a	k8xC	a
Pekařskou	pekařský	k2eAgFnSc7d1	Pekařská
ulicí	ulice	k1gFnSc7	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
zpřístupněn	zpřístupnit	k5eAaPmNgInS	zpřístupnit
Mincmistrovský	mincmistrovský	k2eAgInSc1d1	mincmistrovský
sklep	sklep	k1gInSc1	sklep
(	(	kIx(	(
<g/>
sklepení	sklepení	k1gNnSc1	sklepení
zaniklého	zaniklý	k2eAgInSc2d1	zaniklý
domu	dům	k1gInSc2	dům
mincmistrů	mincmistr	k1gMnPc2	mincmistr
pod	pod	k7c7	pod
Dominikánským	dominikánský	k2eAgNnSc7d1	Dominikánské
náměstím	náměstí	k1gNnSc7	náměstí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
ruční	ruční	k2eAgFnSc1d1	ruční
ražba	ražba	k1gFnSc1	ražba
pamětního	pamětní	k2eAgInSc2d1	pamětní
brněnské	brněnský	k2eAgInPc4d1	brněnský
groše	groš	k1gInPc4	groš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
nastálo	nastálo	k6eAd1	nastálo
pro	pro	k7c4	pro
veřejnosti	veřejnost	k1gFnPc4	veřejnost
otevřeno	otevřen	k2eAgNnSc4d1	otevřeno
podzemí	podzemí	k1gNnSc4	podzemí
pod	pod	k7c7	pod
Zelným	zelný	k2eAgInSc7d1	zelný
trhem	trh	k1gInSc7	trh
a	a	k8xC	a
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2012	[number]	k4	2012
brněnská	brněnský	k2eAgFnSc1d1	brněnská
kostnice	kostnice	k1gFnSc1	kostnice
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
pod	pod	k7c7	pod
Jakubským	jakubský	k2eAgNnSc7d1	Jakubské
náměstím	náměstí	k1gNnSc7	náměstí
u	u	k7c2	u
chrámu	chrám	k1gInSc2	chrám
svatého	svatý	k2eAgMnSc2d1	svatý
Jakuba	Jakub	k1gMnSc2	Jakub
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
po	po	k7c6	po
pařížských	pařížský	k2eAgFnPc6d1	Pařížská
katakombách	katakomby	k1gFnPc6	katakomby
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc7	druhý
největší	veliký	k2eAgFnSc7d3	veliký
kostnicí	kostnice	k1gFnSc7	kostnice
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
zpřístupněn	zpřístupnit	k5eAaPmNgInS	zpřístupnit
protiatomový	protiatomový	k2eAgInSc1d1	protiatomový
kryt	kryt	k1gInSc1	kryt
10-Z	[number]	k4	10-Z
pod	pod	k7c7	pod
Špilberkem	Špilberko	k1gNnSc7	Špilberko
v	v	k7c6	v
Husově	Husův	k2eAgFnSc6d1	Husova
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známá	k1gFnPc1	známá
a	a	k8xC	a
veřejnosti	veřejnost	k1gFnPc1	veřejnost
přístupné	přístupný	k2eAgFnPc1d1	přístupná
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
kasematy	kasemata	k1gFnPc1	kasemata
na	na	k7c6	na
Špilberku	Špilberk	k1gInSc6	Špilberk
<g/>
,	,	kIx,	,
vybudované	vybudovaný	k2eAgFnSc6d1	vybudovaná
v	v	k7c6	v
době	doba	k1gFnSc6	doba
baroka	baroko	k1gNnSc2	baroko
v	v	k7c6	v
příkopech	příkop	k1gInPc6	příkop
středověkého	středověký	k2eAgInSc2d1	středověký
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
od	od	k7c2	od
konce	konec	k1gInSc2	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
do	do	k7c2	do
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
využívané	využívaný	k2eAgFnPc1d1	využívaná
jako	jako	k8xS	jako
obávaný	obávaný	k2eAgInSc1d1	obávaný
žalář	žalář	k1gInSc1	žalář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Kašny	kašna	k1gFnPc1	kašna
<g/>
,	,	kIx,	,
fontány	fontána	k1gFnPc1	fontána
a	a	k8xC	a
významné	významný	k2eAgFnPc1d1	významná
sochy	socha	k1gFnPc1	socha
a	a	k8xC	a
památníky	památník	k1gInPc1	památník
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Nejhonosnější	honosný	k2eAgFnSc7d3	nejhonosnější
brněnskou	brněnský	k2eAgFnSc7d1	brněnská
kašnou	kašna	k1gFnSc7	kašna
je	být	k5eAaImIp3nS	být
barokní	barokní	k2eAgFnSc1d1	barokní
kašna	kašna	k1gFnSc1	kašna
Parnas	Parnas	k1gInSc4	Parnas
z	z	k7c2	z
konce	konec	k1gInSc2	konec
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c6	na
Zelném	zelný	k2eAgInSc6d1	zelný
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Kašny	kašna	k1gFnPc1	kašna
či	či	k8xC	či
fontány	fontána	k1gFnPc1	fontána
můžeme	moct	k5eAaImIp1nP	moct
nalézt	nalézt	k5eAaBmF	nalézt
i	i	k9	i
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
jiných	jiný	k2eAgInPc6d1	jiný
náměstích	náměstí	k1gNnPc6	náměstí
<g/>
,	,	kIx,	,
nádvořích	nádvoří	k1gNnPc6	nádvoří
a	a	k8xC	a
parcích	park	k1gInPc6	park
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
<s>
Fontány	fontána	k1gFnPc1	fontána
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
běžným	běžný	k2eAgInSc7d1	běžný
doplňkem	doplněk	k1gInSc7	doplněk
parků	park	k1gInPc2	park
a	a	k8xC	a
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
,	,	kIx,	,
několik	několik	k4yIc1	několik
se	se	k3xPyFc4	se
jich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
v	v	k7c6	v
parku	park	k1gInSc6	park
Špilberk	Špilberk	k1gInSc1	Špilberk
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
před	před	k7c7	před
Janáčkovou	Janáčkův	k2eAgFnSc7d1	Janáčkova
akademií	akademie	k1gFnSc7	akademie
múzických	múzický	k2eAgNnPc2d1	múzické
umění	umění	k1gNnPc2	umění
a	a	k8xC	a
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Památníky	památník	k1gInPc1	památník
velmi	velmi	k6eAd1	velmi
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
a	a	k8xC	a
událostí	událost	k1gFnPc2	událost
spojených	spojený	k2eAgFnPc2d1	spojená
s	s	k7c7	s
Moravou	Morava	k1gFnSc7	Morava
a	a	k8xC	a
s	s	k7c7	s
Brnem	Brno	k1gNnSc7	Brno
jsou	být	k5eAaImIp3nP	být
vybudovány	vybudovat	k5eAaPmNgInP	vybudovat
především	především	k9	především
v	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
jádru	jádro	k1gNnSc6	jádro
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Lékařskou	lékařský	k2eAgFnSc7d1	lékařská
fakultou	fakulta	k1gFnSc7	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
stojí	stát	k5eAaImIp3nS	stát
socha	socha	k1gFnSc1	socha
Tomáše	Tomáš	k1gMnSc2	Tomáš
Garriguea	Garrigueus	k1gMnSc2	Garrigueus
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
před	před	k7c7	před
Právnickou	právnický	k2eAgFnSc7d1	právnická
fakultou	fakulta	k1gFnSc7	fakulta
stejné	stejný	k2eAgFnSc2d1	stejná
univerzity	univerzita	k1gFnSc2	univerzita
socha	socha	k1gFnSc1	socha
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
<g/>
,	,	kIx,	,
na	na	k7c6	na
Špilberku	Špilberk	k1gInSc6	Špilberk
busta	busta	k1gFnSc1	busta
brněnského	brněnský	k2eAgMnSc2d1	brněnský
hrdiny	hrdina	k1gMnSc2	hrdina
z	z	k7c2	z
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
maršála	maršál	k1gMnSc2	maršál
císařských	císařský	k2eAgNnPc2d1	císařské
vojsk	vojsko	k1gNnPc2	vojsko
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
Louise	Louis	k1gMnSc2	Louis
Raduita	Raduit	k1gMnSc2	Raduit
de	de	k?	de
Souches	Souches	k1gMnSc1	Souches
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
také	také	k9	také
socha	socha	k1gFnSc1	socha
římského	římský	k2eAgMnSc2d1	římský
krále	král	k1gMnSc2	král
Jošta	Jošt	k1gMnSc2	Jošt
a	a	k8xC	a
jeho	on	k3xPp3gMnSc2	on
otce	otec	k1gMnSc2	otec
moravského	moravský	k2eAgMnSc2d1	moravský
markraběte	markrabě	k1gMnSc2	markrabě
Jana	Jan	k1gMnSc2	Jan
Jindřicha	Jindřich	k1gMnSc2	Jindřich
na	na	k7c6	na
portálu	portál	k1gInSc6	portál
Místodržitelského	místodržitelský	k2eAgInSc2d1	místodržitelský
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
přilehlém	přilehlý	k2eAgInSc6d1	přilehlý
kostele	kostel	k1gInSc6	kostel
oba	dva	k4xCgMnPc1	dva
pohřbeni	pohřben	k2eAgMnPc1d1	pohřben
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
žije	žít	k5eAaImIp3nS	žít
necelých	celý	k2eNgInPc2d1	necelý
400	[number]	k4	400
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
Sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
domů	dům	k1gInPc2	dům
a	a	k8xC	a
bytů	byt	k1gInPc2	byt
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
posledního	poslední	k2eAgNnSc2d1	poslední
celostátního	celostátní	k2eAgNnSc2d1	celostátní
sčítání	sčítání	k1gNnSc2	sčítání
<g/>
,	,	kIx,	,
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
Brno	Brno	k1gNnSc1	Brno
k	k	k7c3	k
26	[number]	k4	26
<g/>
.	.	kIx.	.
březnu	březen	k1gInSc6	březen
2011	[number]	k4	2011
celkem	celkem	k6eAd1	celkem
385	[number]	k4	385
913	[number]	k4	913
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2019	[number]	k4	2019
žilo	žít	k5eAaImAgNnS	žít
podle	podle	k7c2	podle
Českého	český	k2eAgInSc2d1	český
statistického	statistický	k2eAgInSc2d1	statistický
úřadu	úřad	k1gInSc2	úřad
ve	v	k7c6	v
městě	město	k1gNnSc6	město
380	[number]	k4	380
681	[number]	k4	681
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
dojíždí	dojíždět	k5eAaImIp3nS	dojíždět
navíc	navíc	k6eAd1	navíc
sto	sto	k4xCgNnSc4	sto
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
za	za	k7c2	za
prací	práce	k1gFnPc2	práce
i	i	k9	i
za	za	k7c7	za
studiem	studio	k1gNnSc7	studio
<g/>
,	,	kIx,	,
reálná	reálný	k2eAgFnSc1d1	reálná
velikost	velikost	k1gFnSc1	velikost
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k6eAd1	okolo
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1389	[number]	k4	1389
mělo	mít	k5eAaImAgNnS	mít
tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
fakticky	fakticky	k6eAd1	fakticky
dnešní	dnešní	k2eAgNnSc4d1	dnešní
historické	historický	k2eAgNnSc4d1	historické
jádro	jádro	k1gNnSc4	jádro
města	město	k1gNnSc2	město
ohraničené	ohraničený	k2eAgFnPc1d1	ohraničená
hradbami	hradba	k1gFnPc7	hradba
<g/>
,	,	kIx,	,
999	[number]	k4	999
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
žilo	žít	k5eAaImAgNnS	žít
asi	asi	k9	asi
8	[number]	k4	8
400	[number]	k4	400
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
připojení	připojení	k1gNnSc6	připojení
předměstských	předměstský	k2eAgFnPc2d1	předměstská
obcí	obec	k1gFnPc2	obec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
městě	město	k1gNnSc6	město
49	[number]	k4	49
460	[number]	k4	460
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc4d3	veliký
přírůstek	přírůstek	k1gInSc4	přírůstek
zažilo	zažít	k5eAaPmAgNnS	zažít
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
Brna	Brno	k1gNnSc2	Brno
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1850	[number]	k4	1850
<g/>
–	–	k?	–
<g/>
1919	[number]	k4	1919
za	za	k7c2	za
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
zejména	zejména	k9	zejména
následným	následný	k2eAgNnSc7d1	následné
připojením	připojení	k1gNnSc7	připojení
okolních	okolní	k2eAgFnPc2d1	okolní
obcí	obec	k1gFnPc2	obec
(	(	kIx(	(
<g/>
Velké	velký	k2eAgNnSc1d1	velké
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
pak	pak	k6eAd1	pak
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
stagnoval	stagnovat	k5eAaImAgMnS	stagnovat
a	a	k8xC	a
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
opět	opět	k6eAd1	opět
rostl	růst	k5eAaImAgInS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Mírný	mírný	k2eAgInSc1d1	mírný
pokles	pokles	k1gInSc1	pokles
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
kopíroval	kopírovat	k5eAaImAgInS	kopírovat
trend	trend	k1gInSc1	trend
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
republice	republika	k1gFnSc6	republika
vlivem	vliv	k1gInSc7	vliv
suburbanizace	suburbanizace	k1gFnSc2	suburbanizace
<g/>
:	:	kIx,	:
lidé	člověk	k1gMnPc1	člověk
se	se	k3xPyFc4	se
stěhovali	stěhovat	k5eAaImAgMnP	stěhovat
za	za	k7c4	za
administrativní	administrativní	k2eAgFnPc4d1	administrativní
hranice	hranice	k1gFnPc4	hranice
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
však	však	k8xC	však
nadále	nadále	k6eAd1	nadále
pracují	pracovat	k5eAaImIp3nP	pracovat
a	a	k8xC	a
využívají	využívat	k5eAaPmIp3nP	využívat
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
Statisticky	statisticky	k6eAd1	statisticky
tak	tak	k9	tak
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
lidí	člověk	k1gMnPc2	člověk
ubývalo	ubývat	k5eAaImAgNnS	ubývat
<g/>
,	,	kIx,	,
fakticky	fakticky	k6eAd1	fakticky
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
rostl	růst	k5eAaImAgInS	růst
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
indikoval	indikovat	k5eAaBmAgMnS	indikovat
rostoucí	rostoucí	k2eAgInSc4d1	rostoucí
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnSc1	obyvatel
okresu	okres	k1gInSc2	okres
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
prstenci	prstenec	k1gInSc6	prstenec
obcí	obec	k1gFnPc2	obec
přímo	přímo	k6eAd1	přímo
sousedících	sousedící	k2eAgFnPc2d1	sousedící
s	s	k7c7	s
Brnem	Brno	k1gNnSc7	Brno
(	(	kIx(	(
<g/>
obvody	obvod	k1gInPc4	obvod
obcí	obec	k1gFnPc2	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
Kuřim	Kuřima	k1gFnPc2	Kuřima
a	a	k8xC	a
Šlapanice	Šlapanice	k1gFnSc2	Šlapanice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
začal	začít	k5eAaPmAgInS	začít
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
znovu	znovu	k6eAd1	znovu
narůstat	narůstat	k5eAaImF	narůstat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Struktura	struktura	k1gFnSc1	struktura
populace	populace	k1gFnSc2	populace
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
385	[number]	k4	385
913	[number]	k4	913
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
191	[number]	k4	191
395	[number]	k4	395
(	(	kIx(	(
<g/>
49,60	[number]	k4	49,60
%	%	kIx~	%
<g/>
)	)	kIx)	)
s	s	k7c7	s
národností	národnost	k1gFnSc7	národnost
českou	český	k2eAgFnSc7d1	Česká
<g/>
,	,	kIx,	,
72	[number]	k4	72
367	[number]	k4	367
(	(	kIx(	(
<g/>
18,75	[number]	k4	18,75
%	%	kIx~	%
<g/>
)	)	kIx)	)
moravskou	moravský	k2eAgFnSc4d1	Moravská
<g/>
,	,	kIx,	,
5	[number]	k4	5
956	[number]	k4	956
(	(	kIx(	(
<g/>
1,54	[number]	k4	1,54
%	%	kIx~	%
<g/>
)	)	kIx)	)
slovenskou	slovenský	k2eAgFnSc4d1	slovenská
<g/>
,	,	kIx,	,
3	[number]	k4	3
271	[number]	k4	271
ukrajinskou	ukrajinský	k2eAgFnSc7d1	ukrajinská
(	(	kIx(	(
<g/>
0,85	[number]	k4	0,85
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1	[number]	k4	1
487	[number]	k4	487
vietnamskou	vietnamský	k2eAgFnSc7d1	vietnamská
(	(	kIx(	(
<g/>
0,39	[number]	k4	0,39
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
464	[number]	k4	464
(	(	kIx(	(
<g/>
0,12	[number]	k4	0,12
%	%	kIx~	%
<g/>
)	)	kIx)	)
polskou	polský	k2eAgFnSc4d1	polská
<g/>
,	,	kIx,	,
ostatní	ostatní	k1gNnSc4	ostatní
národnosti	národnost	k1gFnSc2	národnost
nepřesáhly	přesáhnout	k5eNaPmAgFnP	přesáhnout
0,1	[number]	k4	0,1
%	%	kIx~	%
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
zastoupeních	zastoupení	k1gNnPc6	zastoupení
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
91	[number]	k4	91
529	[number]	k4	529
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
23,7	[number]	k4	23,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
národnost	národnost	k1gFnSc4	národnost
neuvedlo	uvést	k5eNaPmAgNnS	uvést
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sčítání	sčítání	k1gNnSc6	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
376	[number]	k4	376
172	[number]	k4	172
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
286	[number]	k4	286
120	[number]	k4	120
(	(	kIx(	(
<g/>
76,1	[number]	k4	76,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
s	s	k7c7	s
českou	český	k2eAgFnSc7d1	Česká
národností	národnost	k1gFnSc7	národnost
<g/>
,	,	kIx,	,
70	[number]	k4	70
258	[number]	k4	258
(	(	kIx(	(
<g/>
18,7	[number]	k4	18,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
moravskou	moravský	k2eAgFnSc4d1	Moravská
<g/>
,	,	kIx,	,
95	[number]	k4	95
(	(	kIx(	(
<g/>
<	<	kIx(	<
<g/>
0,03	[number]	k4	0,03
%	%	kIx~	%
<g/>
)	)	kIx)	)
slezskou	slezský	k2eAgFnSc4d1	Slezská
<g/>
,	,	kIx,	,
5	[number]	k4	5
795	[number]	k4	795
(	(	kIx(	(
<g/>
1,52	[number]	k4	1,52
%	%	kIx~	%
<g/>
)	)	kIx)	)
slovenskou	slovenský	k2eAgFnSc7d1	slovenská
a	a	k8xC	a
374	[number]	k4	374
(	(	kIx(	(
<g/>
~	~	kIx~	~
<g/>
0,1	[number]	k4	0,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
romskou	romský	k2eAgFnSc4d1	romská
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
národnostního	národnostní	k2eAgNnSc2d1	národnostní
složení	složení	k1gNnSc3	složení
město	město	k1gNnSc1	město
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
výrazný	výrazný	k2eAgInSc4d1	výrazný
pokles	pokles	k1gInSc4	pokles
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
hlásících	hlásící	k2eAgMnPc2d1	hlásící
se	se	k3xPyFc4	se
k	k	k7c3	k
české	český	k2eAgFnSc3d1	Česká
národnosti	národnost	k1gFnSc3	národnost
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
tak	tak	k6eAd1	tak
podruhé	podruhé	k6eAd1	podruhé
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
klesl	klesnout	k5eAaPmAgInS	klesnout
pod	pod	k7c4	pod
polovinu	polovina	k1gFnSc4	polovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
náboženského	náboženský	k2eAgNnSc2d1	náboženské
vyznání	vyznání	k1gNnSc2	vyznání
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
celkem	celkem	k6eAd1	celkem
134	[number]	k4	134
399	[number]	k4	399
(	(	kIx(	(
<g/>
35,7	[number]	k4	35,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
věřících	věřící	k1gMnPc2	věřící
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
111	[number]	k4	111
673	[number]	k4	673
(	(	kIx(	(
<g/>
83	[number]	k4	83
%	%	kIx~	%
<g/>
)	)	kIx)	)
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
staršího	starý	k2eAgMnSc2d2	starší
patnácti	patnáct	k4xCc2	patnáct
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
322	[number]	k4	322
164	[number]	k4	164
–	–	k?	–
85,6	[number]	k4	85,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
nejvyšší	vysoký	k2eAgNnSc4d3	nejvyšší
dosažené	dosažený	k2eAgNnSc4d1	dosažené
vzdělání	vzdělání	k1gNnSc4	vzdělání
188	[number]	k4	188
880	[number]	k4	880
(	(	kIx(	(
<g/>
58,6	[number]	k4	58,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
lidí	člověk	k1gMnPc2	člověk
středoškolské	středoškolský	k2eAgFnSc2d1	středoškolská
(	(	kIx(	(
<g/>
49,5	[number]	k4	49,5
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
s	s	k7c7	s
maturitou	maturita	k1gFnSc7	maturita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
56	[number]	k4	56
490	[number]	k4	490
(	(	kIx(	(
<g/>
17,5	[number]	k4	17,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
základní	základní	k2eAgFnSc1d1	základní
<g/>
,	,	kIx,	,
57	[number]	k4	57
758	[number]	k4	758
(	(	kIx(	(
<g/>
17,9	[number]	k4	17,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
<g/>
,	,	kIx,	,
14	[number]	k4	14
955	[number]	k4	955
(	(	kIx(	(
<g/>
4,6	[number]	k4	4,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
vyšší	vysoký	k2eAgFnPc1d2	vyšší
odborné	odborný	k2eAgFnPc1d1	odborná
a	a	k8xC	a
nástavbové	nástavbový	k2eAgFnPc1d1	nástavbová
a	a	k8xC	a
4	[number]	k4	4
081	[number]	k4	081
(	(	kIx(	(
<g/>
1,3	[number]	k4	1,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
nezjištěné	zjištěný	k2eNgFnPc1d1	nezjištěná
či	či	k8xC	či
bez	bez	k7c2	bez
formálně	formálně	k6eAd1	formálně
dosaženého	dosažený	k2eAgNnSc2d1	dosažené
vzdělání	vzdělání	k1gNnSc2	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
katastru	katastr	k1gInSc2	katastr
Brna	Brno	k1gNnSc2	Brno
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
759	[number]	k4	759
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Zdravotní	zdravotní	k2eAgFnSc1d1	zdravotní
péče	péče	k1gFnSc1	péče
a	a	k8xC	a
kriminalita	kriminalita	k1gFnSc1	kriminalita
===	===	k?	===
</s>
</p>
<p>
<s>
Brno	Brno	k1gNnSc1	Brno
má	mít	k5eAaImIp3nS	mít
relativně	relativně	k6eAd1	relativně
dobrou	dobrý	k2eAgFnSc4d1	dobrá
dostupnost	dostupnost	k1gFnSc4	dostupnost
zdravotní	zdravotní	k2eAgFnSc2d1	zdravotní
péče	péče	k1gFnSc2	péče
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
lékařů	lékař	k1gMnPc2	lékař
na	na	k7c4	na
1000	[number]	k4	1000
obyvatel	obyvatel	k1gMnPc2	obyvatel
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
8,5	[number]	k4	8,5
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
:	:	kIx,	:
4,3	[number]	k4	4,3
<g/>
;	;	kIx,	;
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
7,3	[number]	k4	7,3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
spácháno	spáchán	k2eAgNnSc4d1	spácháno
16	[number]	k4	16
042	[number]	k4	042
trestných	trestný	k2eAgInPc2d1	trestný
činů	čin	k1gInPc2	čin
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
pak	pak	k6eAd1	pak
17	[number]	k4	17
416	[number]	k4	416
trestných	trestný	k2eAgInPc2d1	trestný
činů	čin	k1gInPc2	čin
<g/>
,	,	kIx,	,
objasněnost	objasněnost	k1gFnSc1	objasněnost
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
33	[number]	k4	33
%	%	kIx~	%
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
:	:	kIx,	:
38,9	[number]	k4	38,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
trend	trend	k1gInSc4	trend
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
se	se	k3xPyFc4	se
neliší	lišit	k5eNaImIp3nS	lišit
od	od	k7c2	od
dalších	další	k2eAgNnPc2d1	další
velkých	velký	k2eAgNnPc2d1	velké
měst	město	k1gNnPc2	město
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
2,45	[number]	k4	2,45
obyvatele	obyvatel	k1gMnPc4	obyvatel
na	na	k7c4	na
byt	byt	k1gInSc4	byt
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
2,64	[number]	k4	2,64
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
Brnem	Brno	k1gNnSc7	Brno
je	být	k5eAaImIp3nS	být
vedle	vedle	k7c2	vedle
mnoha	mnoho	k4c2	mnoho
významných	významný	k2eAgFnPc2d1	významná
domácích	domácí	k2eAgFnPc2d1	domácí
osobností	osobnost	k1gFnPc2	osobnost
spjata	spjat	k2eAgFnSc1d1	spjata
i	i	k8xC	i
řada	řada	k1gFnSc1	řada
světových	světový	k2eAgFnPc2d1	světová
osobností	osobnost	k1gFnPc2	osobnost
vědy	věda	k1gFnSc2	věda
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Narodili	narodit	k5eAaPmAgMnP	narodit
se	se	k3xPyFc4	se
nebo	nebo	k8xC	nebo
působili	působit	k5eAaImAgMnP	působit
zde	zde	k6eAd1	zde
např.	např.	kA	např.
fyzik	fyzika	k1gFnPc2	fyzika
Ernst	Ernst	k1gMnSc1	Ernst
Mach	Mach	k1gMnSc1	Mach
<g/>
,	,	kIx,	,
logik	logik	k1gMnSc1	logik
Kurt	Kurt	k1gMnSc1	Kurt
Gödel	Gödel	k1gMnSc1	Gödel
<g/>
,	,	kIx,	,
genetik	genetik	k1gMnSc1	genetik
Gregor	Gregor	k1gMnSc1	Gregor
Mendel	Mendel	k1gMnSc1	Mendel
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
Mendelova	Mendelův	k2eAgFnSc1d1	Mendelova
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
všichni	všechen	k3xTgMnPc1	všechen
tři	tři	k4xCgFnPc4	tři
německojazyčného	německojazyčný	k2eAgInSc2d1	německojazyčný
původu	původ	k1gInSc2	původ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
operní	operní	k2eAgFnSc1d1	operní
pěvkyně	pěvkyně	k1gFnSc1	pěvkyně
Maria	Maria	k1gFnSc1	Maria
Jeritza	Jeritza	k1gFnSc1	Jeritza
nebo	nebo	k8xC	nebo
spisovatel	spisovatel	k1gMnSc1	spisovatel
Milan	Milan	k1gMnSc1	Milan
Kundera	Kundera	k1gFnSc1	Kundera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Městská	městský	k2eAgFnSc1d1	městská
hromadná	hromadný	k2eAgFnSc1d1	hromadná
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
tvoří	tvořit	k5eAaImIp3nP	tvořit
poměrně	poměrně	k6eAd1	poměrně
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
systém	systém	k1gInSc4	systém
sestávající	sestávající	k2eAgInSc4d1	sestávající
ze	z	k7c2	z
11	[number]	k4	11
linek	linka	k1gFnPc2	linka
tramvají	tramvaj	k1gFnPc2	tramvaj
<g/>
,	,	kIx,	,
13	[number]	k4	13
linek	linka	k1gFnPc2	linka
trolejbusů	trolejbus	k1gInPc2	trolejbus
(	(	kIx(	(
<g/>
tvořící	tvořící	k2eAgFnSc1d1	tvořící
největší	veliký	k2eAgFnSc1d3	veliký
síť	síť	k1gFnSc1	síť
trolejbusové	trolejbusový	k2eAgFnSc2d1	trolejbusová
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
a	a	k8xC	a
41	[number]	k4	41
denních	denní	k2eAgMnPc2d1	denní
a	a	k8xC	a
11	[number]	k4	11
nočních	noční	k2eAgFnPc2d1	noční
linek	linka	k1gFnPc2	linka
městských	městský	k2eAgInPc2d1	městský
autobusů	autobus	k1gInPc2	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Šaliny	šalina	k1gFnPc1	šalina
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zdejší	zdejší	k2eAgMnPc1d1	zdejší
běžně	běžně	k6eAd1	běžně
nazývají	nazývat	k5eAaImIp3nP	nazývat
tramvaje	tramvaj	k1gFnPc1	tramvaj
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
tradici	tradice	k1gFnSc4	tradice
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
vyjely	vyjet	k5eAaPmAgFnP	vyjet
do	do	k7c2	do
ulic	ulice	k1gFnPc2	ulice
města	město	k1gNnSc2	město
již	již	k9	již
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgInSc1d1	místní
MHD	MHD	kA	MHD
je	být	k5eAaImIp3nS	být
zapojena	zapojen	k2eAgFnSc1d1	zapojena
do	do	k7c2	do
Integrovaného	integrovaný	k2eAgInSc2d1	integrovaný
dopravního	dopravní	k2eAgInSc2d1	dopravní
systému	systém	k1gInSc2	systém
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
přímo	přímo	k6eAd1	přímo
dopravně	dopravně	k6eAd1	dopravně
napojuje	napojovat	k5eAaImIp3nS	napojovat
také	také	k9	také
některé	některý	k3yIgFnPc4	některý
příměstské	příměstský	k2eAgFnPc4d1	příměstská
obce	obec	k1gFnPc4	obec
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
hlavním	hlavní	k2eAgMnSc7d1	hlavní
provozovatelem	provozovatel	k1gMnSc7	provozovatel
je	být	k5eAaImIp3nS	být
Dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
také	také	k9	také
provozuje	provozovat	k5eAaImIp3nS	provozovat
rekreační	rekreační	k2eAgFnSc4d1	rekreační
lodní	lodní	k2eAgFnSc4d1	lodní
dopravu	doprava	k1gFnSc4	doprava
na	na	k7c6	na
Brněnské	brněnský	k2eAgFnSc6d1	brněnská
přehradě	přehrada	k1gFnSc6	přehrada
a	a	k8xC	a
pro	pro	k7c4	pro
zájemce	zájemce	k1gMnPc4	zájemce
turistický	turistický	k2eAgInSc4d1	turistický
minibus	minibus	k1gInSc4	minibus
poskytující	poskytující	k2eAgInSc4d1	poskytující
krátkou	krátký	k2eAgFnSc4d1	krátká
prohlídku	prohlídka	k1gFnSc4	prohlídka
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Noční	noční	k2eAgFnSc1d1	noční
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
má	mít	k5eAaImIp3nS	mít
centrální	centrální	k2eAgInSc4d1	centrální
uzel	uzel	k1gInSc4	uzel
u	u	k7c2	u
hlavního	hlavní	k2eAgNnSc2d1	hlavní
vlakového	vlakový	k2eAgNnSc2d1	vlakové
nádraží	nádraží	k1gNnSc2	nádraží
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
v	v	k7c6	v
pravidelných	pravidelný	k2eAgInPc6d1	pravidelný
intervalech	interval	k1gInPc6	interval
vyjíždí	vyjíždět	k5eAaImIp3nS	vyjíždět
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
rozjezdy	rozjezd	k1gInPc1	rozjezd
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
posílení	posílení	k1gNnSc4	posílení
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
je	být	k5eAaImIp3nS	být
také	také	k9	také
plánováno	plánován	k2eAgNnSc1d1	plánováno
vybudování	vybudování	k1gNnSc1	vybudování
podpovrchové	podpovrchový	k2eAgFnSc2d1	podpovrchová
dráhy	dráha	k1gFnSc2	dráha
typu	typ	k1gInSc2	typ
S-Bahn	S-Bahno	k1gNnPc2	S-Bahno
<g/>
.	.	kIx.	.
<g/>
Železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
zahájila	zahájit	k5eAaPmAgFnS	zahájit
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
provoz	provoz	k1gInSc4	provoz
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
díky	díky	k7c3	díky
trati	trať	k1gFnSc3	trať
Vídeň	Vídeň	k1gFnSc1	Vídeň
<g/>
–	–	k?	–
<g/>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
s	s	k7c7	s
parním	parní	k2eAgInSc7d1	parní
provozem	provoz	k1gInSc7	provoz
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
Brno	Brno	k1gNnSc1	Brno
železničním	železniční	k2eAgInSc7d1	železniční
uzlem	uzel	k1gInSc7	uzel
nadregionálního	nadregionální	k2eAgInSc2d1	nadregionální
významu	význam	k1gInSc2	význam
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
osobní	osobní	k2eAgFnSc4d1	osobní
dopravu	doprava	k1gFnSc4	doprava
zde	zde	k6eAd1	zde
slouží	sloužit	k5eAaImIp3nP	sloužit
devět	devět	k4xCc4	devět
železničních	železniční	k2eAgFnPc2d1	železniční
stanic	stanice	k1gFnPc2	stanice
a	a	k8xC	a
zastávek	zastávka	k1gFnPc2	zastávka
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
plně	plně	k6eAd1	plně
vytížené	vytížený	k2eAgNnSc4d1	vytížené
hlavní	hlavní	k2eAgNnSc4d1	hlavní
nádraží	nádraží	k1gNnSc4	nádraží
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgMnPc4d1	jiný
centrálním	centrální	k2eAgInSc7d1	centrální
uzlem	uzel	k1gInSc7	uzel
regionální	regionální	k2eAgFnSc2d1	regionální
vlakové	vlakový	k2eAgFnSc2d1	vlaková
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
denně	denně	k6eAd1	denně
využije	využít	k5eAaPmIp3nS	využít
50	[number]	k4	50
tisíc	tisíc	k4xCgInPc2	tisíc
pasažérů	pasažér	k1gMnPc2	pasažér
a	a	k8xC	a
projede	projet	k5eAaPmIp3nS	projet
jím	jíst	k5eAaImIp1nS	jíst
okolo	okolo	k7c2	okolo
500	[number]	k4	500
vlaků	vlak	k1gInPc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Stávající	stávající	k2eAgNnSc1d1	stávající
hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
je	být	k5eAaImIp3nS	být
zastaralé	zastaralý	k2eAgNnSc1d1	zastaralé
a	a	k8xC	a
kapacitně	kapacitně	k6eAd1	kapacitně
nevyhovující	vyhovující	k2eNgMnSc1d1	nevyhovující
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
nového	nový	k2eAgNnSc2d1	nové
nádraží	nádraží	k1gNnSc2	nádraží
byla	být	k5eAaImAgFnS	být
ale	ale	k8xC	ale
několikrát	několikrát	k6eAd1	několikrát
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
důvodů	důvod	k1gInPc2	důvod
odložena	odložen	k2eAgFnSc1d1	odložena
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
desetiletí	desetiletí	k1gNnSc6	desetiletí
se	se	k3xPyFc4	se
diskutuje	diskutovat	k5eAaImIp3nS	diskutovat
o	o	k7c6	o
možné	možný	k2eAgFnSc6d1	možná
změně	změna	k1gFnSc6	změna
jeho	jeho	k3xOp3gFnSc2	jeho
polohy	poloha	k1gFnSc2	poloha
a	a	k8xC	a
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
byla	být	k5eAaImAgFnS	být
vypsána	vypsán	k2eAgNnPc4d1	vypsáno
dvě	dva	k4xCgNnPc4	dva
referenda	referendum	k1gNnPc4	referendum
(	(	kIx(	(
<g/>
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2004	[number]	k4	2004
a	a	k8xC	a
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
však	však	k8xC	však
pro	pro	k7c4	pro
nízkou	nízký	k2eAgFnSc4d1	nízká
účast	účast	k1gFnSc4	účast
neplatná	platný	k2eNgFnSc1d1	neplatná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Silniční	silniční	k2eAgFnSc1d1	silniční
doprava	doprava	k1gFnSc1	doprava
dělá	dělat	k5eAaImIp3nS	dělat
z	z	k7c2	z
Brna	Brno	k1gNnSc2	Brno
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
křižovatku	křižovatka	k1gFnSc4	křižovatka
dálnic	dálnice	k1gFnPc2	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
Jižním	jižní	k2eAgInSc7d1	jižní
okrajem	okraj	k1gInSc7	okraj
města	město	k1gNnSc2	město
vedou	vést	k5eAaImIp3nP	vést
dvě	dva	k4xCgFnPc1	dva
dálnice	dálnice	k1gFnPc1	dálnice
<g/>
,	,	kIx,	,
D1	D1	k1gFnPc1	D1
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
přes	přes	k7c4	přes
Brno	Brno	k1gNnSc4	Brno
do	do	k7c2	do
Ostravy	Ostrava	k1gFnSc2	Ostrava
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
D2	D2	k1gFnSc2	D2
do	do	k7c2	do
Bratislavy	Bratislava	k1gFnSc2	Bratislava
<g/>
,	,	kIx,	,
kousek	kousek	k1gInSc4	kousek
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
Brna	Brno	k1gNnSc2	Brno
začíná	začínat	k5eAaImIp3nS	začínat
dálnice	dálnice	k1gFnSc1	dálnice
D52	D52	k1gMnSc2	D52
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Vídeň	Vídeň	k1gFnSc4	Vídeň
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
plánována	plánován	k2eAgFnSc1d1	plánována
dálnice	dálnice	k1gFnSc1	dálnice
D43	D43	k1gFnSc2	D43
severním	severní	k2eAgInSc7d1	severní
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Svitavy	Svitava	k1gFnPc4	Svitava
<g/>
.	.	kIx.	.
</s>
<s>
Brnem	Brno	k1gNnSc7	Brno
také	také	k9	také
prochází	procházet	k5eAaImIp3nS	procházet
evropské	evropský	k2eAgFnPc4d1	Evropská
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
silnice	silnice	k1gFnPc4	silnice
E	E	kA	E
<g/>
50	[number]	k4	50
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
65	[number]	k4	65
<g/>
,	,	kIx,	,
E461	E461	k1gFnSc1	E461
a	a	k8xC	a
E	E	kA	E
<g/>
462	[number]	k4	462
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
postupně	postupně	k6eAd1	postupně
budován	budovat	k5eAaImNgInS	budovat
Velký	velký	k2eAgInSc1d1	velký
městský	městský	k2eAgInSc1d1	městský
okruh	okruh	k1gInSc1	okruh
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zde	zde	k6eAd1	zde
postaveno	postavit	k5eAaPmNgNnS	postavit
několik	několik	k4yIc1	několik
silničních	silniční	k2eAgInPc2d1	silniční
tunelů	tunel	k1gInPc2	tunel
(	(	kIx(	(
<g/>
Pisárecký	pisárecký	k2eAgInSc1d1	pisárecký
<g/>
,	,	kIx,	,
Husovický	husovický	k2eAgInSc1d1	husovický
<g/>
,	,	kIx,	,
Hlinky	hlinka	k1gFnPc4	hlinka
a	a	k8xC	a
Královopolský	královopolský	k2eAgMnSc1d1	královopolský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
tunely	tunel	k1gInPc1	tunel
jsou	být	k5eAaImIp3nP	být
plánovány	plánován	k2eAgInPc1d1	plánován
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
kvůli	kvůli	k7c3	kvůli
zvýšenému	zvýšený	k2eAgInSc3d1	zvýšený
náporu	nápor	k1gInSc3	nápor
individuální	individuální	k2eAgFnSc2d1	individuální
dopravy	doprava	k1gFnSc2	doprava
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
budovány	budován	k2eAgInPc4d1	budován
parkovací	parkovací	k2eAgInPc4d1	parkovací
domy	dům	k1gInPc4	dům
a	a	k8xC	a
podzemní	podzemní	k2eAgNnSc4d1	podzemní
parkovací	parkovací	k2eAgNnSc4d1	parkovací
stání	stání	k1gNnSc4	stání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Leteckou	letecký	k2eAgFnSc4d1	letecká
dopravu	doprava	k1gFnSc4	doprava
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
dvě	dva	k4xCgNnPc1	dva
fungující	fungující	k2eAgNnPc1d1	fungující
letiště	letiště	k1gNnPc1	letiště
<g/>
,	,	kIx,	,
jedním	jeden	k4xCgMnSc7	jeden
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
veřejné	veřejný	k2eAgNnSc1d1	veřejné
letiště	letiště	k1gNnSc1	letiště
Tuřany	Tuřana	k1gFnSc2	Tuřana
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgFnSc1d1	osobní
doprava	doprava	k1gFnSc1	doprava
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
letišti	letiště	k1gNnSc6	letiště
zažívá	zažívat	k5eAaImIp3nS	zažívat
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
letech	let	k1gInPc6	let
velký	velký	k2eAgInSc4d1	velký
nárůst	nárůst	k1gInSc4	nárůst
<g/>
,	,	kIx,	,
létají	létat	k5eAaImIp3nP	létat
odtud	odtud	k6eAd1	odtud
pravidelné	pravidelný	k2eAgFnPc1d1	pravidelná
linky	linka	k1gFnPc1	linka
například	například	k6eAd1	například
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
Milána	Milán	k1gInSc2	Milán
<g/>
,	,	kIx,	,
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
letiště	letiště	k1gNnSc1	letiště
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
také	také	k9	také
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
základen	základna	k1gFnPc2	základna
letecké	letecký	k2eAgFnSc2d1	letecká
služby	služba	k1gFnSc2	služba
Policie	policie	k1gFnSc2	policie
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
je	být	k5eAaImIp3nS	být
malé	malý	k2eAgNnSc1d1	malé
vnitrostátní	vnitrostátní	k2eAgNnSc1d1	vnitrostátní
letiště	letiště	k1gNnSc1	letiště
Medlánky	Medlánka	k1gFnSc2	Medlánka
sloužící	sloužící	k2eAgNnSc1d1	sloužící
především	především	k9	především
rekreačním	rekreační	k2eAgFnPc3d1	rekreační
aktivitám	aktivita	k1gFnPc3	aktivita
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
lety	léto	k1gNnPc7	léto
balónem	balón	k1gInSc7	balón
<g/>
,	,	kIx,	,
lety	let	k1gInPc7	let
kluzáků	kluzák	k1gInPc2	kluzák
či	či	k8xC	či
pilotáž	pilotáž	k1gFnSc4	pilotáž
leteckých	letecký	k2eAgInPc2d1	letecký
RC	RC	kA	RC
modelů	model	k1gInPc2	model
<g/>
.	.	kIx.	.
<g/>
Cyklistika	cyklistika	k1gFnSc1	cyklistika
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
také	také	k9	také
díky	díky	k7c3	díky
nížinnému	nížinný	k2eAgInSc3d1	nížinný
charakteru	charakter	k1gInSc3	charakter
krajiny	krajina	k1gFnSc2	krajina
<g/>
,	,	kIx,	,
stávající	stávající	k2eAgFnSc2d1	stávající
cyklostezky	cyklostezka	k1gFnSc2	cyklostezka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
měřily	měřit	k5eAaImAgInP	měřit
celkově	celkově	k6eAd1	celkově
přibližně	přibližně	k6eAd1	přibližně
38	[number]	k4	38
km	km	kA	km
<g/>
,	,	kIx,	,
cyklostezky	cyklostezka	k1gFnPc1	cyklostezka
či	či	k8xC	či
dráhy	dráha	k1gFnPc1	dráha
pro	pro	k7c4	pro
bruslaře	bruslař	k1gMnPc4	bruslař
na	na	k7c6	na
kolečkových	kolečkový	k2eAgFnPc6d1	kolečková
bruslích	brusle	k1gFnPc6	brusle
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
postupně	postupně	k6eAd1	postupně
rozšiřovány	rozšiřován	k2eAgFnPc1d1	rozšiřována
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Brna	Brno	k1gNnSc2	Brno
vede	vést	k5eAaImIp3nS	vést
také	také	k9	také
zhruba	zhruba	k6eAd1	zhruba
130	[number]	k4	130
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
cyklostezka	cyklostezka	k1gFnSc1	cyklostezka
do	do	k7c2	do
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
partnerských	partnerský	k2eAgNnPc2d1	partnerské
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
prochází	procházet	k5eAaImIp3nS	procházet
i	i	k9	i
několik	několik	k4yIc1	několik
turistickým	turistický	k2eAgNnPc3d1	turistické
stezek	stezka	k1gFnPc2	stezka
KČT	KČT	kA	KČT
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
mnoho	mnoho	k4c1	mnoho
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
vytvořených	vytvořený	k2eAgInPc2d1	vytvořený
nebo	nebo	k8xC	nebo
vyčleněných	vyčleněný	k2eAgInPc2d1	vyčleněný
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
privatizace	privatizace	k1gFnSc2	privatizace
z	z	k7c2	z
bývalých	bývalý	k2eAgInPc2d1	bývalý
národních	národní	k2eAgInPc2d1	národní
podniků	podnik	k1gInPc2	podnik
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
i	i	k9	i
některé	některý	k3yIgFnPc4	některý
tradiční	tradiční	k2eAgFnPc4d1	tradiční
firmy	firma	k1gFnPc4	firma
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
podniky	podnik	k1gInPc1	podnik
byly	být	k5eAaImAgInP	být
restrukturalizovány	restrukturalizovat	k5eAaImNgInP	restrukturalizovat
a	a	k8xC	a
v	v	k7c6	v
konkurenčním	konkurenční	k2eAgNnSc6d1	konkurenční
prostředí	prostředí	k1gNnSc6	prostředí
se	se	k3xPyFc4	se
udržely	udržet	k5eAaPmAgFnP	udržet
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1990	[number]	k4	1990
měly	mít	k5eAaImAgInP	mít
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
velkou	velký	k2eAgFnSc4d1	velká
váhu	váha	k1gFnSc4	váha
strojírenské	strojírenský	k2eAgInPc4d1	strojírenský
podniky	podnik	k1gInPc4	podnik
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
hospodářství	hospodářství	k1gNnSc1	hospodářství
města	město	k1gNnSc2	město
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
přeorientovalo	přeorientovat	k5eAaPmAgNnS	přeorientovat
na	na	k7c4	na
lehký	lehký	k2eAgInSc4d1	lehký
průmysl	průmysl	k1gInSc4	průmysl
<g/>
,	,	kIx,	,
logistiku	logistika	k1gFnSc4	logistika
a	a	k8xC	a
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zažívají	zažívat	k5eAaImIp3nP	zažívat
tato	tento	k3xDgNnPc4	tento
odvětví	odvětví	k1gNnPc4	odvětví
nebývalý	nebývalý	k2eAgInSc4d1	nebývalý
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yRnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
na	na	k7c4	na
21	[number]	k4	21
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
Veletrhu	veletrh	k1gInSc2	veletrh
nemovitostí	nemovitost	k1gFnPc2	nemovitost
a	a	k8xC	a
investičních	investiční	k2eAgFnPc2d1	investiční
příležitostí	příležitost	k1gFnPc2	příležitost
MIPIM	MIPIM	kA	MIPIM
ve	v	k7c6	v
francouzském	francouzský	k2eAgNnSc6d1	francouzské
Cannes	Cannes	k1gNnSc6	Cannes
dostalo	dostat	k5eAaPmAgNnS	dostat
Brno	Brno	k1gNnSc1	Brno
trojí	trojit	k5eAaImIp3nS	trojit
prestižní	prestižní	k2eAgNnSc1d1	prestižní
ocenění	ocenění	k1gNnSc1	ocenění
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
Cities	Citiesa	k1gFnPc2	Citiesa
of	of	k?	of
the	the	k?	the
Future	Futur	k1gMnSc5	Futur
(	(	kIx(	(
<g/>
města	město	k1gNnSc2	město
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výrobní	výrobní	k2eAgInPc1d1	výrobní
závody	závod	k1gInPc1	závod
se	se	k3xPyFc4	se
soustřeďují	soustřeďovat	k5eAaImIp3nP	soustřeďovat
především	především	k9	především
do	do	k7c2	do
nových	nový	k2eAgFnPc2d1	nová
průmyslových	průmyslový	k2eAgFnPc2d1	průmyslová
zón	zóna	k1gFnPc2	zóna
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
okolí	okolí	k1gNnSc6	okolí
<g/>
,	,	kIx,	,
jakými	jaký	k3yIgFnPc7	jaký
jsou	být	k5eAaImIp3nP	být
Černovická	Černovický	k2eAgFnSc1d1	Černovická
terasa	terasa	k1gFnSc1	terasa
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
technologický	technologický	k2eAgInSc1d1	technologický
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna	zóna	k1gFnSc1	zóna
Modřice	Modřice	k1gFnSc1	Modřice
<g/>
,	,	kIx,	,
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
zóna	zóna	k1gFnSc1	zóna
Šlapanice	Šlapanice	k1gFnSc1	Šlapanice
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
mzda	mzda	k1gFnSc1	mzda
činila	činit	k5eAaImAgFnS	činit
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2018	[number]	k4	2018
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
35	[number]	k4	35
715	[number]	k4	715
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Zpracovatelský	zpracovatelský	k2eAgInSc1d1	zpracovatelský
průmysl	průmysl	k1gInSc1	průmysl
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgFnPc4d3	nejznámější
firmy	firma	k1gFnPc4	firma
brněnské	brněnský	k2eAgFnSc2d1	brněnská
historie	historie	k1gFnSc2	historie
patří	patřit	k5eAaImIp3nP	patřit
strojírny	strojírna	k1gFnPc1	strojírna
Zetor	zetor	k1gInSc1	zetor
(	(	kIx(	(
<g/>
světoznámý	světoznámý	k2eAgMnSc1d1	světoznámý
výrobce	výrobce	k1gMnSc1	výrobce
traktorů	traktor	k1gInPc2	traktor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zbrojovka	zbrojovka	k1gFnSc1	zbrojovka
Brno	Brno	k1gNnSc4	Brno
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
výrobce	výrobce	k1gMnSc2	výrobce
pušek	puška	k1gFnPc2	puška
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Královopolská	královopolský	k2eAgFnSc1d1	Královopolská
(	(	kIx(	(
<g/>
významná	významný	k2eAgFnSc1d1	významná
strojírna	strojírna	k1gFnSc1	strojírna
v	v	k7c6	v
Králově	Králův	k2eAgNnSc6d1	Královo
Poli	pole	k1gNnSc6	pole
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
První	první	k4xOgFnSc1	první
brněnská	brněnský	k2eAgFnSc1d1	brněnská
strojírna	strojírna	k1gFnSc1	strojírna
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
Brno	Brno	k1gNnSc1	Brno
díky	díky	k7c3	díky
vysoce	vysoce	k6eAd1	vysoce
rozvinutému	rozvinutý	k2eAgNnSc3d1	rozvinuté
textilnímu	textilní	k2eAgNnSc3d1	textilní
průmyslu	průmysl	k1gInSc2	průmysl
získalo	získat	k5eAaPmAgNnS	získat
přezdívku	přezdívka	k1gFnSc4	přezdívka
"	"	kIx"	"
<g/>
rakouský	rakouský	k2eAgMnSc1d1	rakouský
<g/>
"	"	kIx"	"
resp.	resp.	kA	resp.
"	"	kIx"	"
<g/>
moravský	moravský	k2eAgInSc1d1	moravský
Manchester	Manchester	k1gInSc1	Manchester
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Terciární	terciární	k2eAgInSc1d1	terciární
a	a	k8xC	a
kvartérní	kvartérní	k2eAgInSc1d1	kvartérní
sektor	sektor	k1gInSc1	sektor
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
Veletrhy	veletrh	k1gInPc1	veletrh
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Brněnské	brněnský	k2eAgInPc1d1	brněnský
veletrhy	veletrh	k1gInPc1	veletrh
a	a	k8xC	a
výstavy	výstava	k1gFnPc1	výstava
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgFnPc1d1	vlastní
značně	značně	k6eAd1	značně
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
a	a	k8xC	a
architektonicky	architektonicky	k6eAd1	architektonicky
okázalé	okázalý	k2eAgNnSc1d1	okázalé
Brněnské	brněnský	k2eAgNnSc1d1	brněnské
výstaviště	výstaviště	k1gNnSc1	výstaviště
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgNnSc6	jenž
se	se	k3xPyFc4	se
každoročně	každoročně	k6eAd1	každoročně
pořádá	pořádat	k5eAaImIp3nS	pořádat
řada	řada	k1gFnSc1	řada
výstav	výstava	k1gFnPc2	výstava
a	a	k8xC	a
veletrhů	veletrh	k1gInPc2	veletrh
s	s	k7c7	s
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
účastí	účast	k1gFnSc7	účast
<g/>
.	.	kIx.	.
</s>
<s>
Brněnské	brněnský	k2eAgNnSc1d1	brněnské
výstaviště	výstaviště	k1gNnSc1	výstaviště
je	být	k5eAaImIp3nS	být
svými	svůj	k3xOyFgMnPc7	svůj
0,13	[number]	k4	0,13
km2	km2	k4	km2
výstavní	výstavní	k2eAgFnSc2d1	výstavní
plochy	plocha	k1gFnSc2	plocha
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
výstavišť	výstaviště	k1gNnPc2	výstaviště
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
23	[number]	k4	23
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
činí	činit	k5eAaImIp3nS	činit
0,36	[number]	k4	0,36
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
události	událost	k1gFnPc4	událost
patří	patřit	k5eAaImIp3nS	patřit
Autosalon	autosalon	k1gInSc1	autosalon
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
strojírenský	strojírenský	k2eAgInSc1d1	strojírenský
veletrh	veletrh	k1gInSc1	veletrh
<g/>
,	,	kIx,	,
Go-Regiontour	Go-Regiontour	k1gMnSc1	Go-Regiontour
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
akce	akce	k1gFnPc4	akce
patřil	patřit	k5eAaImAgMnS	patřit
i	i	k9	i
veletrh	veletrh	k1gInSc4	veletrh
moderní	moderní	k2eAgFnSc2d1	moderní
počítačové	počítačový	k2eAgFnSc2d1	počítačová
a	a	k8xC	a
komunikační	komunikační	k2eAgFnSc2d1	komunikační
techniky	technika	k1gFnSc2	technika
Invex	Invex	k1gInSc1	Invex
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
také	také	k9	také
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
módy	móda	k1gFnSc2	móda
proslulo	proslout	k5eAaPmAgNnS	proslout
po	po	k7c6	po
Evropě	Evropa	k1gFnSc6	Evropa
pořádáním	pořádání	k1gNnSc7	pořádání
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
módního	módní	k2eAgInSc2d1	módní
veletrhu	veletrh	k1gInSc2	veletrh
Styl	styl	k1gInSc4	styl
s	s	k7c7	s
výstavou	výstava	k1gFnSc7	výstava
Kabo	Kabo	k1gMnSc1	Kabo
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
také	také	k9	také
roste	růst	k5eAaImIp3nS	růst
mnoho	mnoho	k4c4	mnoho
velkých	velký	k2eAgNnPc2d1	velké
administrativních	administrativní	k2eAgNnPc2d1	administrativní
center	centrum	k1gNnPc2	centrum
především	především	k9	především
podél	podél	k7c2	podél
ulic	ulice	k1gFnPc2	ulice
Heršpické	heršpický	k2eAgFnSc2d1	Heršpická
a	a	k8xC	a
Vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
Český	český	k2eAgInSc1d1	český
technologický	technologický	k2eAgInSc1d1	technologický
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
společný	společný	k2eAgInSc1d1	společný
projekt	projekt	k1gInSc1	projekt
britské	britský	k2eAgFnSc2d1	britská
firmy	firma	k1gFnSc2	firma
B	B	kA	B
<g/>
&	&	k?	&
<g/>
O	O	kA	O
a	a	k8xC	a
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Spielberk	Spielberk	k1gInSc1	Spielberk
Office	Office	kA	Office
Centre	centr	k1gInSc5	centr
<g/>
,	,	kIx,	,
Brno	Brno	k1gNnSc1	Brno
Business	business	k1gInSc1	business
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Axis	Axis	k1gInSc1	Axis
Office	Office	kA	Office
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
Vienna	Vienna	k1gFnSc1	Vienna
Point	pointa	k1gFnPc2	pointa
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
otevřely	otevřít	k5eAaPmAgFnP	otevřít
svoje	svůj	k3xOyFgNnSc4	svůj
pracoviště	pracoviště	k1gNnSc4	pracoviště
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
technologické	technologický	k2eAgFnSc2d1	technologická
firmy	firma	k1gFnSc2	firma
jako	jako	k8xS	jako
IBM	IBM	kA	IBM
<g/>
,	,	kIx,	,
Honeywell	Honeywell	k1gInSc1	Honeywell
<g/>
,	,	kIx,	,
Siemens	siemens	k1gInSc1	siemens
<g/>
,	,	kIx,	,
AVG	AVG	kA	AVG
Technologies	Technologies	k1gInSc1	Technologies
<g/>
,	,	kIx,	,
Cisco	Cisco	k1gNnSc1	Cisco
Systems	Systemsa	k1gFnPc2	Systemsa
<g/>
,	,	kIx,	,
AT	AT	kA	AT
<g/>
&	&	k?	&
<g/>
T	T	kA	T
<g/>
,	,	kIx,	,
SGI	SGI	kA	SGI
<g/>
,	,	kIx,	,
Red	Red	k1gFnSc1	Red
Hat	hat	k0	hat
<g/>
,	,	kIx,	,
Alstom	Alstom	k1gInSc1	Alstom
<g/>
,	,	kIx,	,
Motorola	Motorola	kA	Motorola
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Přirozeně	přirozeně	k6eAd1	přirozeně
řada	řada	k1gFnSc1	řada
těchto	tento	k3xDgFnPc2	tento
firem	firma	k1gFnPc2	firma
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
svá	svůj	k3xOyFgNnPc4	svůj
sídla	sídlo	k1gNnPc4	sídlo
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Red	Red	k1gFnSc1	Red
Hat	hat	k0	hat
in	in	k?	in
the	the	k?	the
Czech	Czech	k1gInSc1	Czech
Republic	Republice	k1gFnPc2	Republice
<g/>
,	,	kIx,	,
AVG	AVG	kA	AVG
Technologies	Technologies	k1gInSc1	Technologies
CZ	CZ	kA	CZ
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
SGI	SGI	kA	SGI
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
nebo	nebo	k8xC	nebo
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
své	svůj	k3xOyFgFnSc2	svůj
významné	významný	k2eAgFnSc2d1	významná
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
centrály	centrála	k1gFnSc2	centrála
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Honeywell	Honeywell	k1gInSc1	Honeywell
Global	globat	k5eAaImAgInS	globat
Design	design	k1gInSc1	design
Center	centrum	k1gNnPc2	centrum
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
IBM	IBM	kA	IBM
Delivery	Delivera	k1gFnPc1	Delivera
Centre	centr	k1gInSc5	centr
Central	Central	k1gMnSc1	Central
Europe	Europ	k1gInSc5	Europ
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Svá	svůj	k3xOyFgNnPc4	svůj
sídla	sídlo	k1gNnPc4	sídlo
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
i	i	k9	i
významné	významný	k2eAgFnPc4d1	významná
domácí	domácí	k2eAgFnPc4d1	domácí
firmy	firma	k1gFnPc4	firma
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
známý	známý	k2eAgMnSc1d1	známý
dopravce	dopravce	k1gMnSc1	dopravce
Student	student	k1gMnSc1	student
Agency	Agenca	k1gFnSc2	Agenca
<g/>
,	,	kIx,	,
IT	IT	kA	IT
společnost	společnost	k1gFnSc1	společnost
GyTy	GyTa	k1gFnSc2	GyTa
a	a	k8xC	a
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgNnPc2d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
začíná	začínat	k5eAaImIp3nS	začínat
také	také	k9	také
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
kvaternární	kvaternární	k2eAgInSc4d1	kvaternární
sektor	sektor	k1gInSc4	sektor
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
činnosti	činnost	k1gFnSc2	činnost
ve	v	k7c6	v
vědě	věda	k1gFnSc6	věda
<g/>
,	,	kIx,	,
výzkumu	výzkum	k1gInSc2	výzkum
a	a	k8xC	a
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
CEITEC	CEITEC	kA	CEITEC
(	(	kIx(	(
<g/>
Středoevropský	středoevropský	k2eAgInSc1d1	středoevropský
technologický	technologický	k2eAgInSc1d1	technologický
institut	institut	k1gInSc1	institut
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
International	International	k1gMnSc1	International
Clinical	Clinical	k1gMnSc1	Clinical
Research	Research	k1gMnSc1	Research
Centre	centr	k1gInSc5	centr
in	in	k?	in
Brno	Brno	k1gNnSc1	Brno
(	(	kIx(	(
<g/>
ICRC	ICRC	kA	ICRC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
AdMaS	AdMaS	k1gFnSc1	AdMaS
(	(	kIx(	(
<g/>
Advanced	Advanced	k1gInSc1	Advanced
Materials	Materials	k1gInSc1	Materials
<g/>
,	,	kIx,	,
Structures	Structures	k1gInSc1	Structures
and	and	k?	and
Technologies	Technologies	k1gInSc1	Technologies
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Cetocoen	Cetocoen	k1gInSc1	Cetocoen
(	(	kIx(	(
<g/>
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
toxických	toxický	k2eAgFnPc2d1	toxická
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
nadále	nadále	k6eAd1	nadále
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
snahy	snaha	k1gFnSc2	snaha
tyto	tento	k3xDgFnPc4	tento
aktivity	aktivita	k1gFnPc4	aktivita
podporovat	podporovat	k5eAaImF	podporovat
<g/>
,	,	kIx,	,
příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
Jihomoravské	jihomoravský	k2eAgNnSc1d1	Jihomoravské
inovační	inovační	k2eAgNnSc1d1	inovační
centrum	centrum	k1gNnSc1	centrum
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
Technologický	technologický	k2eAgInSc1d1	technologický
inkubátor	inkubátor	k1gInSc1	inkubátor
VUT	VUT	kA	VUT
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Prodejní	prodejní	k2eAgFnSc1d1	prodejní
síť	síť	k1gFnSc1	síť
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
nákupních	nákupní	k2eAgNnPc2d1	nákupní
center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
jedněmi	jeden	k4xCgFnPc7	jeden
z	z	k7c2	z
nejoblíbenějších	oblíbený	k2eAgMnPc2d3	nejoblíbenější
jsou	být	k5eAaImIp3nP	být
Galerie	galerie	k1gFnSc1	galerie
Vaňkovka	Vaňkovka	k1gFnSc1	Vaňkovka
<g/>
,	,	kIx,	,
Nákupní	nákupní	k2eAgNnSc1d1	nákupní
centrum	centrum	k1gNnSc1	centrum
Královo	Králův	k2eAgNnSc1d1	Královo
Pole	pole	k1gNnSc1	pole
a	a	k8xC	a
Olympia	Olympia	k1gFnSc1	Olympia
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
však	však	k9	však
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
leží	ležet	k5eAaImIp3nS	ležet
až	až	k9	až
za	za	k7c4	za
hranici	hranice	k1gFnSc4	hranice
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgMnPc7d3	veliký
jsou	být	k5eAaImIp3nP	být
Olympia	Olympia	k1gFnSc1	Olympia
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgInSc7	druhý
největším	veliký	k2eAgInSc7d3	veliký
obchodním	obchodní	k2eAgInSc7d1	obchodní
centrem	centr	k1gInSc7	centr
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
a	a	k8xC	a
Avion	avion	k1gInSc1	avion
Shopping	shopping	k1gInSc1	shopping
Park	park	k1gInSc1	park
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
velká	velký	k2eAgNnPc1d1	velké
nákupní	nákupní	k2eAgNnPc1d1	nákupní
centra	centrum	k1gNnPc1	centrum
jsou	být	k5eAaImIp3nP	být
otevřena	otevřít	k5eAaPmNgNnP	otevřít
non-stop	nontop	k1gInSc4	non-stop
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
již	již	k6eAd1	již
zmíněné	zmíněný	k2eAgNnSc4d1	zmíněné
Nákupní	nákupní	k2eAgNnSc4d1	nákupní
centrum	centrum	k1gNnSc4	centrum
Královo	Králův	k2eAgNnSc1d1	Královo
Pole	pole	k1gNnSc1	pole
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
běžné	běžný	k2eAgNnSc1d1	běžné
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
nákupních	nákupní	k2eAgNnPc6d1	nákupní
centrech	centrum	k1gNnPc6	centrum
jsou	být	k5eAaImIp3nP	být
pravidelně	pravidelně	k6eAd1	pravidelně
pořádány	pořádán	k2eAgFnPc1d1	pořádána
různé	různý	k2eAgFnPc1d1	různá
výstavy	výstava	k1gFnPc1	výstava
a	a	k8xC	a
kulturní	kulturní	k2eAgFnPc1d1	kulturní
akce	akce	k1gFnPc1	akce
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ve	v	k7c6	v
Vaňkovce	Vaňkovka	k1gFnSc6	Vaňkovka
<g/>
,	,	kIx,	,
Olympii	Olympia	k1gFnSc6	Olympia
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brno	Brno	k1gNnSc1	Brno
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
plošný	plošný	k2eAgInSc4d1	plošný
standard	standard	k1gInSc4	standard
maloobchodní	maloobchodní	k2eAgFnSc2d1	maloobchodní
prodejní	prodejní	k2eAgFnSc2d1	prodejní
plochy	plocha	k1gFnSc2	plocha
1,5	[number]	k4	1,5
m2	m2	k4	m2
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
stav	stav	k1gInSc1	stav
plně	plně	k6eAd1	plně
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
s	s	k7c7	s
podmínkami	podmínka	k1gFnPc7	podmínka
ve	v	k7c6	v
městech	město	k1gNnPc6	město
západní	západní	k2eAgFnSc2d1	západní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
místní	místní	k2eAgFnSc7d1	místní
sítí	síť	k1gFnSc7	síť
maloobchodních	maloobchodní	k2eAgFnPc2d1	maloobchodní
prodejen	prodejna	k1gFnPc2	prodejna
je	být	k5eAaImIp3nS	být
Brněnka	Brněnka	k1gFnSc1	Brněnka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
====	====	k?	====
Turismus	turismus	k1gInSc4	turismus
====	====	k?	====
</s>
</p>
<p>
</p>
<p>
<s>
Turismus	turismus	k1gInSc1	turismus
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
od	od	k7c2	od
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
velký	velký	k2eAgInSc4d1	velký
rozmach	rozmach	k1gInSc4	rozmach
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
výchozí	výchozí	k2eAgFnSc7d1	výchozí
turistickou	turistický	k2eAgFnSc7d1	turistická
destinací	destinace	k1gFnSc7	destinace
za	za	k7c7	za
poznáním	poznání	k1gNnSc7	poznání
přírodních	přírodní	k2eAgFnPc2d1	přírodní
a	a	k8xC	a
kulturních	kulturní	k2eAgFnPc2d1	kulturní
zajímavostí	zajímavost	k1gFnPc2	zajímavost
Brna	Brno	k1gNnSc2	Brno
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
Moravy	Morava	k1gFnSc2	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
území	území	k1gNnSc2	území
Brna	Brno	k1gNnSc2	Brno
začíná	začínat	k5eAaImIp3nS	začínat
chráněná	chráněný	k2eAgFnSc1d1	chráněná
krajinná	krajinný	k2eAgFnSc1d1	krajinná
oblast	oblast	k1gFnSc1	oblast
Moravský	moravský	k2eAgInSc1d1	moravský
kras	kras	k1gInSc1	kras
a	a	k8xC	a
na	na	k7c4	na
jih	jih	k1gInSc4	jih
se	se	k3xPyFc4	se
rozprostírají	rozprostírat	k5eAaImIp3nP	rozprostírat
vinice	vinice	k1gFnPc1	vinice
s	s	k7c7	s
vinnými	vinný	k2eAgInPc7d1	vinný
sklípky	sklípek	k1gInPc7	sklípek
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
<g/>
,	,	kIx,	,
východu	východ	k1gInSc2	východ
i	i	k8xC	i
západu	západ	k1gInSc2	západ
obklopeno	obklopit	k5eAaPmNgNnS	obklopit
zachovalými	zachovalý	k2eAgInPc7d1	zachovalý
smíšenými	smíšený	k2eAgInPc7d1	smíšený
lesy	les	k1gInPc7	les
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nabízejí	nabízet	k5eAaImIp3nP	nabízet
mnoho	mnoho	k4c4	mnoho
příležitostí	příležitost	k1gFnPc2	příležitost
pro	pro	k7c4	pro
turistiku	turistika	k1gFnSc4	turistika
a	a	k8xC	a
cykloturistiku	cykloturistika	k1gFnSc4	cykloturistika
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
několika	několik	k4yIc7	několik
městskými	městský	k2eAgFnPc7d1	městská
částmi	část	k1gFnPc7	část
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
obora	obora	k1gFnSc1	obora
Holedná	Holedný	k2eAgFnSc1d1	Holedná
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
brněnská	brněnský	k2eAgNnPc1d1	brněnské
informační	informační	k2eAgNnPc1d1	informační
centra	centrum	k1gNnPc1	centrum
přes	přes	k7c4	přes
205	[number]	k4	205
tisíc	tisíc	k4xCgInPc2	tisíc
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
83	[number]	k4	83
tisíc	tisíc	k4xCgInPc2	tisíc
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
město	město	k1gNnSc4	město
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
Rusové	Rus	k1gMnPc1	Rus
<g/>
,	,	kIx,	,
Poláci	Polák	k1gMnPc1	Polák
a	a	k8xC	a
Italové	Ital	k1gMnPc1	Ital
<g/>
,	,	kIx,	,
v	v	k7c6	v
jarním	jarní	k2eAgNnSc6d1	jarní
období	období	k1gNnSc6	období
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
Rakušané	Rakušan	k1gMnPc1	Rakušan
<g/>
,	,	kIx,	,
Španělé	Španěl	k1gMnPc1	Španěl
a	a	k8xC	a
turisté	turist	k1gMnPc1	turist
z	z	k7c2	z
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
<g/>
Počet	počet	k1gInSc1	počet
přenocování	přenocování	k1gNnSc2	přenocování
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
turistů	turist	k1gMnPc2	turist
v	v	k7c6	v
ubytovacích	ubytovací	k2eAgNnPc6d1	ubytovací
zařízeních	zařízení	k1gNnPc6	zařízení
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
překročil	překročit	k5eAaPmAgInS	překročit
778	[number]	k4	778
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vysokého	vysoký	k2eAgNnSc2d1	vysoké
zastoupení	zastoupení	k1gNnSc2	zastoupení
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
návštěvníků	návštěvník	k1gMnPc2	návštěvník
kraje	kraj	k1gInSc2	kraj
lze	lze	k6eAd1	lze
připsat	připsat	k5eAaPmF	připsat
významu	význam	k1gInSc3	význam
Brna	Brno	k1gNnSc2	Brno
jako	jako	k8xS	jako
regionální	regionální	k2eAgFnSc2d1	regionální
obchodní	obchodní	k2eAgFnSc2d1	obchodní
a	a	k8xC	a
kulturní	kulturní	k2eAgFnSc2d1	kulturní
metropole	metropol	k1gFnSc2	metropol
<g/>
,	,	kIx,	,
vytvářející	vytvářející	k2eAgInSc4d1	vytvářející
potenciál	potenciál	k1gInSc4	potenciál
pro	pro	k7c4	pro
příjezdový	příjezdový	k2eAgInSc4d1	příjezdový
cestovní	cestovní	k2eAgInSc4d1	cestovní
ruch	ruch	k1gInSc4	ruch
orientovaný	orientovaný	k2eAgInSc4d1	orientovaný
na	na	k7c4	na
veletržní	veletržní	k2eAgFnSc4d1	veletržní
a	a	k8xC	a
kongresovou	kongresový	k2eAgFnSc4d1	kongresová
turistiku	turistika	k1gFnSc4	turistika
a	a	k8xC	a
také	také	k6eAd1	také
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
kraji	kraj	k1gInSc6	kraj
vína	víno	k1gNnSc2	víno
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
kulturních	kulturní	k2eAgFnPc2d1	kulturní
<g/>
,	,	kIx,	,
historických	historický	k2eAgFnPc2d1	historická
a	a	k8xC	a
přírodních	přírodní	k2eAgFnPc2d1	přírodní
zajímavostí	zajímavost	k1gFnPc2	zajímavost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
celkem	celkem	k6eAd1	celkem
59	[number]	k4	59
hotelů	hotel	k1gInPc2	hotel
a	a	k8xC	a
23	[number]	k4	23
jiných	jiný	k2eAgFnPc2d1	jiná
hromadných	hromadný	k2eAgFnPc2d1	hromadná
ubytovacích	ubytovací	k2eAgFnPc2d1	ubytovací
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
celková	celkový	k2eAgFnSc1d1	celková
kapacita	kapacita	k1gFnSc1	kapacita
byla	být	k5eAaImAgFnS	být
9002	[number]	k4	9002
lůžek	lůžko	k1gNnPc2	lůžko
a	a	k8xC	a
4316	[number]	k4	4316
pokojů	pokoj	k1gInPc2	pokoj
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
na	na	k7c4	na
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
vzbudilo	vzbudit	k5eAaPmAgNnS	vzbudit
nečekaný	čekaný	k2eNgInSc4d1	nečekaný
zájem	zájem	k1gInSc4	zájem
odhalení	odhalení	k1gNnSc2	odhalení
hodinového	hodinový	k2eAgInSc2d1	hodinový
stroje	stroj	k1gInSc2	stroj
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
(	(	kIx(	(
<g/>
nepřesně	přesně	k6eNd1	přesně
označovaného	označovaný	k2eAgInSc2d1	označovaný
jako	jako	k8xS	jako
Brněnský	brněnský	k2eAgInSc1d1	brněnský
orloj	orloj	k1gInSc1	orloj
<g/>
)	)	kIx)	)
vyrobeného	vyrobený	k2eAgNnSc2d1	vyrobené
z	z	k7c2	z
africké	africký	k2eAgFnSc2d1	africká
žuly	žula	k1gFnSc2	žula
a	a	k8xC	a
záměrně	záměrně	k6eAd1	záměrně
tvarovaného	tvarovaný	k2eAgInSc2d1	tvarovaný
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
připomínal	připomínat	k5eAaImAgInS	připomínat
projektil	projektil	k1gInSc1	projektil
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
někdy	někdy	k6eAd1	někdy
též	též	k9	též
tvarově	tvarově	k6eAd1	tvarově
ztotožňovaného	ztotožňovaný	k2eAgNnSc2d1	ztotožňované
s	s	k7c7	s
penisem	penis	k1gInSc7	penis
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
záležitost	záležitost	k1gFnSc1	záležitost
byla	být	k5eAaImAgFnS	být
silně	silně	k6eAd1	silně
medializována	medializovat	k5eAaImNgFnS	medializovat
a	a	k8xC	a
stavba	stavba	k1gFnSc1	stavba
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
brzy	brzy	k6eAd1	brzy
stala	stát	k5eAaPmAgFnS	stát
kontroverzní	kontroverzní	k2eAgFnSc7d1	kontroverzní
a	a	k8xC	a
známou	známý	k2eAgFnSc7d1	známá
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Správa	správa	k1gFnSc1	správa
a	a	k8xC	a
členění	členění	k1gNnSc1	členění
města	město	k1gNnSc2	město
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
Brno	Brno	k1gNnSc1	Brno
je	být	k5eAaImIp3nS	být
územně	územně	k6eAd1	územně
členěné	členěný	k2eAgNnSc1d1	členěné
statutární	statutární	k2eAgNnSc1d1	statutární
město	město	k1gNnSc1	město
s	s	k7c7	s
magistrátem	magistrát	k1gInSc7	magistrát
a	a	k8xC	a
primátorem	primátor	k1gMnSc7	primátor
<g/>
,	,	kIx,	,
spravované	spravovaný	k2eAgInPc1d1	spravovaný
podle	podle	k7c2	podle
statutu	statut	k1gInSc2	statut
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
města	město	k1gNnSc2	město
a	a	k8xC	a
rady	rada	k1gFnSc2	rada
je	být	k5eAaImIp3nS	být
orgánem	orgán	k1gInSc7	orgán
města	město	k1gNnSc2	město
i	i	k8xC	i
sněm	sněm	k1gInSc1	sněm
starostů	starosta	k1gMnPc2	starosta
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc7	jehož
členy	člen	k1gMnPc7	člen
jsou	být	k5eAaImIp3nP	být
starostové	starosta	k1gMnPc1	starosta
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
brněnských	brněnský	k2eAgFnPc2d1	brněnská
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
a	a	k8xC	a
primátor	primátor	k1gMnSc1	primátor
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brno	Brno	k1gNnSc1	Brno
samo	sám	k3xTgNnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
tvoří	tvořit	k5eAaImIp3nP	tvořit
okres	okres	k1gInSc1	okres
Brno-město	Brnoěsta	k1gFnSc5	Brno-města
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
je	být	k5eAaImIp3nS	být
obcí	obec	k1gFnSc7	obec
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
působností	působnost	k1gFnSc7	působnost
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
vlastní	vlastní	k2eAgNnSc4d1	vlastní
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
senátních	senátní	k2eAgFnPc2d1	senátní
voleb	volba	k1gFnPc2	volba
je	být	k5eAaImIp3nS	být
území	území	k1gNnSc4	území
Brna	Brno	k1gNnSc2	Brno
rozděleno	rozdělen	k2eAgNnSc1d1	rozděleno
mezi	mezi	k7c7	mezi
volební	volební	k2eAgFnSc7d1	volební
obvody	obvod	k1gInPc1	obvod
55	[number]	k4	55
<g/>
,	,	kIx,	,
58	[number]	k4	58
<g/>
,	,	kIx,	,
59	[number]	k4	59
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Městské	městský	k2eAgFnSc3d1	městská
části	část	k1gFnSc3	část
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Správní	správní	k2eAgNnSc1d1	správní
členění	členění	k1gNnSc1	členění
Brna	Brno	k1gNnSc2	Brno
prošlo	projít	k5eAaPmAgNnS	projít
dramatickým	dramatický	k2eAgMnSc7d1	dramatický
a	a	k8xC	a
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
vývojem	vývoj	k1gInSc7	vývoj
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
1990	[number]	k4	1990
se	se	k3xPyFc4	se
Brno	Brno	k1gNnSc1	Brno
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
29	[number]	k4	29
samosprávných	samosprávný	k2eAgFnPc2d1	samosprávná
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgNnSc1d1	vlastní
volené	volený	k2eAgNnSc1d1	volené
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
<g/>
,	,	kIx,	,
radu	rada	k1gMnSc4	rada
a	a	k8xC	a
starostu	starosta	k1gMnSc4	starosta
<g/>
,	,	kIx,	,
znak	znak	k1gInSc4	znak
a	a	k8xC	a
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
celkově	celkově	k6eAd1	celkově
ze	z	k7c2	z
48	[number]	k4	48
katastrálních	katastrální	k2eAgNnPc2d1	katastrální
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
hranice	hranice	k1gFnPc1	hranice
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
se	se	k3xPyFc4	se
zásadně	zásadně	k6eAd1	zásadně
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
zhruba	zhruba	k6eAd1	zhruba
600	[number]	k4	600
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
MČ	MČ	kA	MČ
Brno-Ořešín	Brno-Ořešín	k1gInSc1	Brno-Ořešín
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
přibližně	přibližně	k6eAd1	přibližně
60	[number]	k4	60
tisíc	tisíc	k4xCgInSc4	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
MČ	MČ	kA	MČ
Brno-střed	Brnotřed	k1gInSc1	Brno-střed
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgFnPc4d3	veliký
městské	městský	k2eAgFnPc4d1	městská
části	část	k1gFnPc4	část
Brna	Brno	k1gNnSc2	Brno
patří	patřit	k5eAaImIp3nP	patřit
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
Brno-střed	Brnotřed	k1gMnSc1	Brno-střed
<g/>
,	,	kIx,	,
Brno-sever	Brnoever	k1gMnSc1	Brno-sever
<g/>
,	,	kIx,	,
Brno-Bystrc	Brno-Bystrc	k1gFnSc1	Brno-Bystrc
a	a	k8xC	a
Brno-Královo	Brno-Králův	k2eAgNnSc1d1	Brno-Královo
Pole	pole	k1gNnSc1	pole
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
nejmenší	malý	k2eAgFnPc1d3	nejmenší
městské	městský	k2eAgFnPc1d1	městská
části	část	k1gFnPc1	část
jsou	být	k5eAaImIp3nP	být
Brno-Kníničky	Brno-Knínička	k1gFnPc4	Brno-Knínička
<g/>
,	,	kIx,	,
Brno-Útěchov	Brno-Útěchov	k1gInSc4	Brno-Útěchov
a	a	k8xC	a
Brno-Ořešín	Brno-Ořešín	k1gInSc4	Brno-Ořešín
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
katastrální	katastrální	k2eAgFnSc2d1	katastrální
výměry	výměra	k1gFnSc2	výměra
jsou	být	k5eAaImIp3nP	být
největšími	veliký	k2eAgFnPc7d3	veliký
městskými	městský	k2eAgFnPc7d1	městská
částmi	část	k1gFnPc7	část
Brno-Bystrc	Brno-Bystrc	k1gInSc1	Brno-Bystrc
<g/>
,	,	kIx,	,
Brno-Tuřany	Brno-Tuřan	k1gMnPc4	Brno-Tuřan
<g/>
,	,	kIx,	,
Brno-Líšeň	Brno-Líšeň	k1gFnSc1	Brno-Líšeň
a	a	k8xC	a
Brno-střed	Brnotřed	k1gInSc1	Brno-střed
<g/>
,	,	kIx,	,
nejmenšími	malý	k2eAgFnPc7d3	nejmenší
pak	pak	k8xC	pak
Brno-Útěchov	Brno-Útěchov	k1gInSc4	Brno-Útěchov
a	a	k8xC	a
Brno-Nový	Brno-Nový	k2eAgInSc4d1	Brno-Nový
Lískovec	Lískovec	k1gInSc4	Lískovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Úřady	úřad	k1gInPc1	úřad
a	a	k8xC	a
instituce	instituce	k1gFnPc1	instituce
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Justiční	justiční	k2eAgMnPc4d1	justiční
===	===	k?	===
</s>
</p>
<p>
<s>
Brno	Brno	k1gNnSc1	Brno
je	být	k5eAaImIp3nS	být
centrem	centr	k1gInSc7	centr
české	český	k2eAgFnSc2d1	Česká
justice	justice	k1gFnSc2	justice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
geograficky	geograficky	k6eAd1	geograficky
oddělena	oddělit	k5eAaPmNgNnP	oddělit
od	od	k7c2	od
moci	moc	k1gFnSc2	moc
výkonné	výkonný	k2eAgFnSc2d1	výkonná
a	a	k8xC	a
zákonodárné	zákonodárný	k2eAgFnSc2d1	zákonodárná
sídlící	sídlící	k2eAgFnSc2d1	sídlící
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
Zemské	zemský	k2eAgFnSc6d1	zemská
sněmovně	sněmovna	k1gFnSc6	sněmovna
v	v	k7c6	v
Joštově	Joštův	k2eAgFnSc6d1	Joštova
ulici	ulice	k1gFnSc6	ulice
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
zde	zde	k6eAd1	zde
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
Burešova	Burešův	k2eAgFnSc1d1	Burešova
ulice	ulice	k1gFnSc1	ulice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
správní	správní	k2eAgInSc1d1	správní
soud	soud	k1gInSc1	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
Moravské	moravský	k2eAgNnSc1d1	Moravské
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
)	)	kIx)	)
a	a	k8xC	a
Nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
státní	státní	k2eAgNnSc1d1	státní
zastupitelství	zastupitelství	k1gNnSc1	zastupitelství
(	(	kIx(	(
<g/>
Jezuitská	jezuitský	k2eAgFnSc1d1	jezuitská
ulice	ulice	k1gFnSc1	ulice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Městský	městský	k2eAgInSc1d1	městský
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
má	mít	k5eAaImIp3nS	mít
působnost	působnost	k1gFnSc1	působnost
jako	jako	k8xC	jako
okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
pro	pro	k7c4	pro
celé	celý	k2eAgNnSc4d1	celé
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
okolní	okolní	k2eAgFnPc4d1	okolní
obce	obec	k1gFnPc4	obec
má	mít	k5eAaImIp3nS	mít
působnost	působnost	k1gFnSc4	působnost
Okresní	okresní	k2eAgInSc1d1	okresní
soud	soud	k1gInSc1	soud
Brno-venkov	Brnoenkov	k1gInSc1	Brno-venkov
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
soudy	soud	k1gInPc1	soud
sídlí	sídlet	k5eAaImIp3nP	sídlet
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
Justičním	justiční	k2eAgInSc6d1	justiční
areálu	areál	k1gInSc6	areál
Brno	Brno	k1gNnSc4	Brno
v	v	k7c6	v
Polní	polní	k2eAgFnSc6d1	polní
ulici	ulice	k1gFnSc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Justičním	justiční	k2eAgInSc6d1	justiční
paláci	palác	k1gInSc6	palác
v	v	k7c6	v
Rooseveltově	Rooseveltův	k2eAgFnSc6d1	Rooseveltova
ulici	ulice	k1gFnSc6	ulice
sídlí	sídlet	k5eAaImIp3nS	sídlet
Krajský	krajský	k2eAgInSc4d1	krajský
soud	soud	k1gInSc4	soud
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
s	s	k7c7	s
působností	působnost	k1gFnSc7	působnost
pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
Jihomoravský	jihomoravský	k2eAgInSc4d1	jihomoravský
kraj	kraj	k1gInSc4	kraj
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
také	také	k9	také
sídlí	sídlet	k5eAaImIp3nS	sídlet
příslušné	příslušný	k2eAgFnPc4d1	příslušná
krajské	krajský	k2eAgFnPc4d1	krajská
<g/>
,	,	kIx,	,
okresní	okresní	k2eAgFnPc4d1	okresní
a	a	k8xC	a
městské	městský	k2eAgNnSc1d1	Městské
státní	státní	k2eAgNnSc1d1	státní
zastupitelství	zastupitelství	k1gNnSc1	zastupitelství
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
náměstí	náměstí	k1gNnSc6	náměstí
navíc	navíc	k6eAd1	navíc
sídlí	sídlet	k5eAaImIp3nS	sídlet
pobočka	pobočka	k1gFnSc1	pobočka
Vrchního	vrchní	k2eAgNnSc2d1	vrchní
státního	státní	k2eAgNnSc2d1	státní
zastupitelství	zastupitelství	k1gNnSc2	zastupitelství
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Vzdělávací	vzdělávací	k2eAgMnPc4d1	vzdělávací
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
sídlí	sídlet	k5eAaImIp3nS	sídlet
celkem	celkem	k6eAd1	celkem
34	[number]	k4	34
fakult	fakulta	k1gFnPc2	fakulta
14	[number]	k4	14
různých	různý	k2eAgFnPc2d1	různá
univerzit	univerzita	k1gFnPc2	univerzita
a	a	k8xC	a
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
založeny	založit	k5eAaPmNgFnP	založit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
byla	být	k5eAaImAgFnS	být
předchůdkyně	předchůdkyně	k1gFnSc1	předchůdkyně
Vysokého	vysoký	k2eAgNnSc2d1	vysoké
učení	učení	k1gNnSc2	učení
technického	technický	k2eAgNnSc2d1	technické
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
svého	svůj	k3xOyFgInSc2	svůj
času	čas	k1gInSc2	čas
jedinou	jediný	k2eAgFnSc4d1	jediná
institucí	instituce	k1gFnPc2	instituce
poskytující	poskytující	k2eAgNnSc4d1	poskytující
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
vzdělání	vzdělání	k1gNnSc4	vzdělání
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Veterinární	veterinární	k2eAgFnSc1d1	veterinární
a	a	k8xC	a
farmaceutická	farmaceutický	k2eAgFnSc1d1	farmaceutická
univerzita	univerzita	k1gFnSc1	univerzita
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
univerzitou	univerzita	k1gFnSc7	univerzita
v	v	k7c6	v
ČR	ČR	kA	ČR
nabízející	nabízející	k2eAgInSc1d1	nabízející
studijní	studijní	k2eAgInSc1d1	studijní
program	program	k1gInSc1	program
veterinární	veterinární	k2eAgFnSc2d1	veterinární
medicíny	medicína	k1gFnSc2	medicína
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
druhou	druhý	k4xOgFnSc7	druhý
největší	veliký	k2eAgFnSc7d3	veliký
univerzitou	univerzita	k1gFnSc7	univerzita
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
největší	veliký	k2eAgFnSc7d3	veliký
univerzitou	univerzita	k1gFnSc7	univerzita
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
a	a	k8xC	a
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
také	také	k9	také
Mendelova	Mendelův	k2eAgFnSc1d1	Mendelova
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
mimo	mimo	k7c4	mimo
jiné	jiná	k1gFnPc4	jiná
nejstarším	starý	k2eAgInPc3d3	nejstarší
vysokým	vysoký	k2eAgInPc3d1	vysoký
zemědělským	zemědělský	k2eAgInPc3d1	zemědělský
a	a	k8xC	a
lesnickým	lesnický	k2eAgNnSc7d1	lesnické
učením	učení	k1gNnSc7	učení
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
i	i	k9	i
další	další	k2eAgFnPc4d1	další
veřejné	veřejný	k2eAgFnPc4d1	veřejná
vysoké	vysoký	k2eAgFnPc4d1	vysoká
školy	škola	k1gFnPc4	škola
jako	jako	k8xC	jako
například	například	k6eAd1	například
Univerzita	univerzita	k1gFnSc1	univerzita
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
jediná	jediný	k2eAgFnSc1d1	jediná
vojenská	vojenský	k2eAgFnSc1d1	vojenská
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
nebo	nebo	k8xC	nebo
Janáčkova	Janáčkův	k2eAgFnSc1d1	Janáčkova
akademie	akademie	k1gFnSc1	akademie
múzických	múzický	k2eAgNnPc2d1	múzické
umění	umění	k1gNnPc2	umění
<g/>
,	,	kIx,	,
o	o	k7c4	o
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
vznik	vznik	k1gInSc4	vznik
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
usiloval	usilovat	k5eAaImAgMnS	usilovat
známý	známý	k2eAgMnSc1d1	známý
umělec	umělec	k1gMnSc1	umělec
Leoš	Leoš	k1gMnSc1	Leoš
Janáček	Janáček	k1gMnSc1	Janáček
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
byly	být	k5eAaImAgFnP	být
zakládány	zakládat	k5eAaImNgFnP	zakládat
také	také	k9	také
soukromé	soukromý	k2eAgFnPc1d1	soukromá
vysoké	vysoký	k2eAgFnPc1d1	vysoká
školy	škola	k1gFnPc1	škola
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
dnes	dnes	k6eAd1	dnes
největší	veliký	k2eAgFnPc1d3	veliký
jsou	být	k5eAaImIp3nP	být
B.I.B.	B.I.B.	k1gFnPc1	B.I.B.
<g/>
S.	S.	kA	S.
<g/>
,	,	kIx,	,
Akademie	akademie	k1gFnSc2	akademie
STING	STING	kA	STING
či	či	k8xC	či
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
Karla	Karel	k1gMnSc2	Karel
Engliše	Engliš	k1gMnSc2	Engliš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
na	na	k7c6	na
veřejných	veřejný	k2eAgFnPc6d1	veřejná
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
studuje	studovat	k5eAaImIp3nS	studovat
asi	asi	k9	asi
84	[number]	k4	84
tisíc	tisíc	k4xCgInPc2	tisíc
studentů	student	k1gMnPc2	student
a	a	k8xC	a
přes	přes	k7c4	přes
6	[number]	k4	6
tisíc	tisíc	k4xCgInPc2	tisíc
studentů	student	k1gMnPc2	student
na	na	k7c6	na
soukromých	soukromý	k2eAgFnPc6d1	soukromá
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
<g/>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
také	také	k9	také
archeologický	archeologický	k2eAgInSc1d1	archeologický
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
biofyzikální	biofyzikální	k2eAgInSc1d1	biofyzikální
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
centrum	centrum	k1gNnSc1	centrum
výzkumu	výzkum	k1gInSc2	výzkum
globální	globální	k2eAgFnSc2d1	globální
změny	změna	k1gFnSc2	změna
<g/>
,	,	kIx,	,
psychologický	psychologický	k2eAgInSc1d1	psychologický
ústav	ústav	k1gInSc1	ústav
<g/>
,	,	kIx,	,
ústav	ústav	k1gInSc1	ústav
analytické	analytický	k2eAgFnSc2d1	analytická
chemie	chemie	k1gFnSc2	chemie
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
ústav	ústav	k1gInSc1	ústav
biologie	biologie	k1gFnSc2	biologie
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
,	,	kIx,	,
ústav	ústav	k1gInSc1	ústav
fyziky	fyzika	k1gFnSc2	fyzika
materiálů	materiál	k1gInPc2	materiál
a	a	k8xC	a
ústav	ústav	k1gInSc1	ústav
přístrojové	přístrojový	k2eAgFnSc2d1	přístrojová
techniky	technika	k1gFnSc2	technika
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
nacházelo	nacházet	k5eAaImAgNnS	nacházet
také	také	k6eAd1	také
celkem	celkem	k6eAd1	celkem
63	[number]	k4	63
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
různého	různý	k2eAgNnSc2d1	různé
zaměření	zaměření	k1gNnSc2	zaměření
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
gymnázia	gymnázium	k1gNnPc1	gymnázium
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnPc1d1	střední
průmyslové	průmyslový	k2eAgFnPc1d1	průmyslová
školy	škola	k1gFnPc1	škola
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgMnPc1	čtyři
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
zřizovány	zřizován	k2eAgFnPc1d1	zřizována
církvemi	církev	k1gFnPc7	církev
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
středních	střední	k2eAgFnPc6d1	střední
školách	škola	k1gFnPc6	škola
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
studuje	studovat	k5eAaImIp3nS	studovat
celkově	celkově	k6eAd1	celkově
přes	přes	k7c4	přes
31	[number]	k4	31
tisíc	tisíc	k4xCgInPc2	tisíc
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
polovina	polovina	k1gFnSc1	polovina
všech	všecek	k3xTgMnPc2	všecek
středoškolských	středoškolský	k2eAgMnPc2d1	středoškolský
studentů	student	k1gMnPc2	student
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
celkem	celkem	k6eAd1	celkem
12	[number]	k4	12
vyšších	vysoký	k2eAgFnPc2d2	vyšší
odborných	odborný	k2eAgFnPc2d1	odborná
škol	škola	k1gFnPc2	škola
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
3	[number]	k4	3
tisíci	tisíc	k4xCgInPc7	tisíc
studenty	student	k1gMnPc7	student
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
celkově	celkově	k6eAd1	celkově
studuje	studovat	k5eAaImIp3nS	studovat
přes	přes	k7c4	přes
120	[number]	k4	120
tisíc	tisíc	k4xCgInPc2	tisíc
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
66	[number]	k4	66
obecních	obecní	k2eAgFnPc2d1	obecní
základních	základní	k2eAgFnPc2d1	základní
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
4	[number]	k4	4
základní	základní	k2eAgFnPc1d1	základní
školy	škola	k1gFnPc1	škola
soukromé	soukromý	k2eAgFnPc1d1	soukromá
a	a	k8xC	a
1	[number]	k4	1
církevní	církevní	k2eAgInSc4d1	církevní
celkem	celkem	k6eAd1	celkem
s	s	k7c7	s
přibližně	přibližně	k6eAd1	přibližně
25	[number]	k4	25
tisíci	tisíc	k4xCgInPc7	tisíc
žáky	žák	k1gMnPc7	žák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Náboženské	náboženský	k2eAgMnPc4d1	náboženský
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
působí	působit	k5eAaImIp3nS	působit
řada	řada	k1gFnSc1	řada
církevních	církevní	k2eAgFnPc2d1	církevní
institucí	instituce	k1gFnPc2	instituce
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
možné	možný	k2eAgNnSc1d1	možné
nalézt	nalézt	k5eAaPmF	nalézt
desítky	desítka	k1gFnPc4	desítka
náboženských	náboženský	k2eAgFnPc2d1	náboženská
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
42	[number]	k4	42
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
chrámů	chrám	k1gInPc2	chrám
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jedna	jeden	k4xCgFnSc1	jeden
katedrála	katedrála	k1gFnSc1	katedrála
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
bazilika	bazilika	k1gFnSc1	bazilika
<g/>
,	,	kIx,	,
32	[number]	k4	32
kaplí	kaple	k1gFnPc2	kaple
a	a	k8xC	a
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
klášterů	klášter	k1gInPc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
brněnská	brněnský	k2eAgFnSc1d1	brněnská
mešita	mešita	k1gFnSc1	mešita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
první	první	k4xOgMnSc1	první
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
a	a	k8xC	a
synagoga	synagoga	k1gFnSc1	synagoga
Agudas	Agudasa	k1gFnPc2	Agudasa
achim	achima	k1gFnPc2	achima
<g/>
,	,	kIx,	,
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
dříve	dříve	k6eAd2	dříve
stála	stát	k5eAaImAgFnS	stát
také	také	k9	také
Velká	velký	k2eAgFnSc1d1	velká
synagoga	synagoga	k1gFnSc1	synagoga
<g/>
,	,	kIx,	,
Polský	polský	k2eAgInSc1d1	polský
templ	templ	k1gInSc1	templ
a	a	k8xC	a
Nová	nový	k2eAgFnSc1d1	nová
synagoga	synagoga	k1gFnSc1	synagoga
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
sídelním	sídelní	k2eAgNnSc7d1	sídelní
místem	místo	k1gNnSc7	místo
biskupství	biskupství	k1gNnSc1	biskupství
brněnské	brněnský	k2eAgFnSc2d1	brněnská
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
diecéze	diecéze	k1gFnSc2	diecéze
a	a	k8xC	a
církevního	církevní	k2eAgInSc2d1	církevní
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
biskupství	biskupství	k1gNnSc4	biskupství
brněnské	brněnský	k2eAgFnSc2d1	brněnská
diecéze	diecéze	k1gFnSc2	diecéze
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
brněnského	brněnský	k2eAgInSc2d1	brněnský
seniorátu	seniorát	k1gInSc2	seniorát
Českobratrské	českobratrský	k2eAgFnSc2d1	Českobratrská
církve	církev	k1gFnSc2	církev
evangelické	evangelický	k2eAgFnSc2d1	evangelická
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
usiluje	usilovat	k5eAaImIp3nS	usilovat
též	též	k9	též
o	o	k7c6	o
zřízení	zřízení	k1gNnSc6	zřízení
papežské	papežský	k2eAgFnSc2d1	Papežská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgInPc1d1	ostatní
úřady	úřad	k1gInPc1	úřad
a	a	k8xC	a
instituce	instituce	k1gFnPc1	instituce
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
celostátní	celostátní	k2eAgInPc4d1	celostátní
orgány	orgán	k1gInPc4	orgán
sídlící	sídlící	k2eAgInPc4d1	sídlící
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
patří	patřit	k5eAaImIp3nP	patřit
sídlo	sídlo	k1gNnSc4	sídlo
veřejného	veřejný	k2eAgMnSc2d1	veřejný
ochránce	ochránce	k1gMnSc2	ochránce
práv	právo	k1gNnPc2	právo
v	v	k7c6	v
Údolní	údolní	k2eAgFnSc6d1	údolní
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
soutěže	soutěž	k1gFnSc2	soutěž
na	na	k7c6	na
třídě	třída	k1gFnSc6	třída
Kapitána	kapitán	k1gMnSc2	kapitán
Jaroše	Jaroš	k1gMnSc2	Jaroš
<g/>
,	,	kIx,	,
Úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
mezinárodněprávní	mezinárodněprávní	k2eAgFnSc4d1	mezinárodněprávní
ochranu	ochrana	k1gFnSc4	ochrana
dětí	dítě	k1gFnPc2	dítě
na	na	k7c6	na
Šilingrově	Šilingrův	k2eAgNnSc6d1	Šilingrovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
matrika	matrika	k1gFnSc1	matrika
u	u	k7c2	u
úřadu	úřad	k1gInSc2	úřad
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Brno-střed	Brnotřed	k1gInSc1	Brno-střed
v	v	k7c6	v
Dominikánské	dominikánský	k2eAgFnSc6d1	Dominikánská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Odvolací	odvolací	k2eAgNnSc1d1	odvolací
finanční	finanční	k2eAgNnSc1d1	finanční
ředitelství	ředitelství	k1gNnSc1	ředitelství
na	na	k7c6	na
Masarykově	Masarykův	k2eAgFnSc6d1	Masarykova
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
Státní	státní	k2eAgFnSc1d1	státní
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
a	a	k8xC	a
potravinářská	potravinářský	k2eAgFnSc1d1	potravinářská
inspekce	inspekce	k1gFnSc1	inspekce
v	v	k7c6	v
Květné	květný	k2eAgFnSc6d1	Květná
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
Ústřední	ústřední	k2eAgInSc1d1	ústřední
kontrolní	kontrolní	k2eAgInSc1d1	kontrolní
a	a	k8xC	a
zkušební	zkušební	k2eAgInSc1d1	zkušební
ústav	ústav	k1gInSc1	ústav
zemědělský	zemědělský	k2eAgInSc1d1	zemědělský
v	v	k7c6	v
Hroznové	hroznový	k2eAgFnSc6d1	Hroznová
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
Ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
státní	státní	k2eAgFnSc4d1	státní
kontrolu	kontrola	k1gFnSc4	kontrola
veterinárních	veterinární	k2eAgMnPc2d1	veterinární
biopreparátů	biopreparát	k1gInPc2	biopreparát
a	a	k8xC	a
léčiv	léčivo	k1gNnPc2	léčivo
v	v	k7c6	v
Hudcově	Hudcův	k2eAgFnSc6d1	Hudcova
ulici	ulice	k1gFnSc6	ulice
a	a	k8xC	a
Vinařský	vinařský	k2eAgInSc1d1	vinařský
fond	fond	k1gInSc1	fond
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
krajského	krajský	k2eAgInSc2d1	krajský
úřadu	úřad	k1gInSc2	úřad
na	na	k7c6	na
Žerotínově	Žerotínův	k2eAgNnSc6d1	Žerotínovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
je	být	k5eAaImIp3nS	být
Brno	Brno	k1gNnSc4	Brno
také	také	k9	také
sídlem	sídlo	k1gNnSc7	sídlo
Úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
dohled	dohled	k1gInSc4	dohled
nad	nad	k7c7	nad
hospodařením	hospodaření	k1gNnSc7	hospodaření
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
politických	politický	k2eAgNnPc2d1	politické
hnutí	hnutí	k1gNnPc2	hnutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
sídlí	sídlet	k5eAaImIp3nS	sídlet
i	i	k9	i
profesní	profesní	k2eAgFnSc2d1	profesní
komory	komora	k1gFnSc2	komora
některých	některý	k3yIgFnPc2	některý
profesí	profes	k1gFnPc2	profes
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
Komora	komora	k1gFnSc1	komora
daňových	daňový	k2eAgMnPc2d1	daňový
poradců	poradce	k1gMnPc2	poradce
v	v	k7c6	v
Kozí	kozí	k2eAgFnSc6d1	kozí
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
Komora	komora	k1gFnSc1	komora
patentových	patentový	k2eAgMnPc2d1	patentový
zástupců	zástupce	k1gMnPc2	zástupce
v	v	k7c6	v
Gorkého	Gorkého	k2eAgFnSc6d1	Gorkého
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
Komora	komora	k1gFnSc1	komora
veterinárních	veterinární	k2eAgMnPc2d1	veterinární
lékařů	lékař	k1gMnPc2	lékař
na	na	k7c6	na
Palackého	Palackého	k2eAgFnSc6d1	Palackého
třídě	třída	k1gFnSc6	třída
a	a	k8xC	a
jediná	jediný	k2eAgFnSc1d1	jediná
pobočka	pobočka	k1gFnSc1	pobočka
České	český	k2eAgFnSc2d1	Česká
advokátní	advokátní	k2eAgFnSc2d1	advokátní
komory	komora	k1gFnSc2	komora
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
mnohé	mnohý	k2eAgInPc1d1	mnohý
regionální	regionální	k2eAgInPc1d1	regionální
úřady	úřad	k1gInPc1	úřad
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
krajským	krajský	k2eAgInSc7d1	krajský
úřadem	úřad	k1gInSc7	úřad
Jihomoravského	jihomoravský	k2eAgInSc2d1	jihomoravský
kraje	kraj	k1gInSc2	kraj
na	na	k7c6	na
Žerotínově	Žerotínův	k2eAgNnSc6d1	Žerotínovo
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
zde	zde	k6eAd1	zde
mají	mít	k5eAaImIp3nP	mít
i	i	k9	i
úřady	úřad	k1gInPc1	úřad
městských	městský	k2eAgFnPc2d1	městská
částí	část	k1gFnPc2	část
a	a	k8xC	a
krajské	krajský	k2eAgNnSc1d1	krajské
oddělení	oddělení	k1gNnSc1	oddělení
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
složek	složka	k1gFnPc2	složka
či	či	k8xC	či
dalších	další	k2eAgInPc2d1	další
úřadů	úřad	k1gInPc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
sídlí	sídlet	k5eAaImIp3nS	sídlet
i	i	k9	i
Vazební	vazební	k2eAgFnPc4d1	vazební
věznice	věznice	k1gFnPc4	věznice
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zřízena	zřízen	k2eAgFnSc1d1	zřízena
pobočka	pobočka	k1gFnSc1	pobočka
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
studio	studio	k1gNnSc1	studio
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
a	a	k8xC	a
studio	studio	k1gNnSc1	studio
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
.	.	kIx.	.
<g/>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
také	také	k9	také
sídlem	sídlo	k1gNnSc7	sídlo
celkem	celkem	k6eAd1	celkem
11	[number]	k4	11
nemocnic	nemocnice	k1gFnPc2	nemocnice
<g/>
,	,	kIx,	,
5	[number]	k4	5
odborných	odborný	k2eAgInPc2d1	odborný
léčebných	léčebný	k2eAgInPc2d1	léčebný
ústavů	ústav	k1gInPc2	ústav
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgNnPc2d1	další
zdravotnických	zdravotnický	k2eAgNnPc2d1	zdravotnické
zařízení	zařízení	k1gNnPc2	zařízení
(	(	kIx(	(
<g/>
stav	stav	k1gInSc1	stav
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Fakultní	fakultní	k2eAgFnSc2d1	fakultní
nemocnice	nemocnice	k1gFnSc2	nemocnice
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
k	k	k7c3	k
největším	veliký	k2eAgNnPc3d3	veliký
zdravotnickým	zdravotnický	k2eAgNnPc3d1	zdravotnické
zařízením	zařízení	k1gNnPc3	zařízení
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
ošetřeno	ošetřit	k5eAaPmNgNnS	ošetřit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
milión	milión	k4xCgInSc1	milión
pacientů	pacient	k1gMnPc2	pacient
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
celkově	celkově	k6eAd1	celkově
202	[number]	k4	202
ordinací	ordinace	k1gFnPc2	ordinace
praktických	praktický	k2eAgMnPc2d1	praktický
lékařů	lékař	k1gMnPc2	lékař
<g/>
,	,	kIx,	,
302	[number]	k4	302
ordinací	ordinace	k1gFnPc2	ordinace
stomatologů	stomatolog	k1gMnPc2	stomatolog
<g/>
,	,	kIx,	,
75	[number]	k4	75
ordinací	ordinace	k1gFnPc2	ordinace
dětských	dětský	k2eAgMnPc2d1	dětský
lékařů	lékař	k1gMnPc2	lékař
a	a	k8xC	a
stovky	stovka	k1gFnSc2	stovka
dalších	další	k2eAgFnPc2d1	další
ordinací	ordinace	k1gFnPc2	ordinace
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
Fakultní	fakultní	k2eAgFnSc7d1	fakultní
nemocnicí	nemocnice	k1gFnSc7	nemocnice
u	u	k7c2	u
sv.	sv.	kA	sv.
Anny	Anna	k1gFnSc2	Anna
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
zdravotnické	zdravotnický	k2eAgNnSc1d1	zdravotnické
centrum	centrum	k1gNnSc1	centrum
klinické	klinický	k2eAgFnSc2d1	klinická
medicíny	medicína	k1gFnSc2	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
klinikou	klinika	k1gFnSc7	klinika
Mayo	Mayo	k1gNnSc1	Mayo
<g/>
,	,	kIx,	,
základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
k	k	k7c3	k
otevření	otevření	k1gNnSc3	otevření
Mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
centra	centrum	k1gNnSc2	centrum
klinického	klinický	k2eAgInSc2d1	klinický
výzkumu	výzkum	k1gInSc2	výzkum
(	(	kIx(	(
<g/>
ICRC	ICRC	kA	ICRC
<g/>
)	)	kIx)	)
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Partnerskými	partnerský	k2eAgNnPc7d1	partnerské
městy	město	k1gNnPc7	město
Brna	Brno	k1gNnSc2	Brno
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Metropole	metropole	k1gFnSc1	metropole
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
300	[number]	k4	300
km	km	kA	km
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
orientační	orientační	k2eAgInSc1d1	orientační
nástroj	nástroj	k1gInSc1	nástroj
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
pouze	pouze	k6eAd1	pouze
města	město	k1gNnPc4	město
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
hlavními	hlavní	k2eAgNnPc7d1	hlavní
městy	město	k1gNnPc7	město
států	stát	k1gInPc2	stát
nebo	nebo	k8xC	nebo
zemí	zem	k1gFnPc2	zem
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
územní	územní	k2eAgFnSc2d1	územní
jednotky	jednotka	k1gFnSc2	jednotka
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
za	za	k7c2	za
metropole	metropol	k1gFnSc2	metropol
obecně	obecně	k6eAd1	obecně
v	v	k7c6	v
evropském	evropský	k2eAgInSc6d1	evropský
kontextu	kontext	k1gInSc6	kontext
považována	považován	k2eAgFnSc1d1	považována
a	a	k8xC	a
nebo	nebo	k8xC	nebo
s	s	k7c7	s
počtem	počet	k1gInSc7	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
přesahujícím	přesahující	k2eAgFnPc3d1	přesahující
300	[number]	k4	300
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
je	být	k5eAaImIp3nS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
jako	jako	k9	jako
přibližná	přibližný	k2eAgFnSc1d1	přibližná
geografická	geografický	k2eAgFnSc1d1	geografická
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
dopravní	dopravní	k2eAgFnSc4d1	dopravní
infrastrukturu	infrastruktura	k1gFnSc4	infrastruktura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
DŘÍMAL	dřímat	k5eAaImAgMnS	dřímat
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
;	;	kIx,	;
PEŠA	PEŠA	kA	PEŠA
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
I	I	kA	I
+	+	kIx~	+
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Blok	blok	k1gInSc1	blok
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
/	/	kIx~	/
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
296	[number]	k4	296
/	/	kIx~	/
380	[number]	k4	380
s.	s.	k?	s.
</s>
</p>
<p>
<s>
KUČA	KUČA	k?	KUČA
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Památky	památka	k1gFnPc1	památka
Brna	Brno	k1gNnSc2	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
183	[number]	k4	183
s.	s.	k?	s.
</s>
</p>
<p>
<s>
KUČA	KUČA	k?	KUČA
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
vývoj	vývoj	k1gInSc1	vývoj
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
předměstí	předměstí	k1gNnSc2	předměstí
a	a	k8xC	a
připojených	připojený	k2eAgFnPc2d1	připojená
vesnic	vesnice	k1gFnPc2	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Baset	Baset	k1gMnSc1	Baset
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
644	[number]	k4	644
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86223	[number]	k4	86223
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KLENOVSKÝ	KLENOVSKÝ	kA	KLENOVSKÝ
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
židovské	židovský	k2eAgFnPc1d1	židovská
:	:	kIx,	:
historie	historie	k1gFnPc1	historie
a	a	k8xC	a
památky	památka	k1gFnPc1	památka
židovského	židovský	k2eAgNnSc2d1	Židovské
osídlení	osídlení	k1gNnSc2	osídlení
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
=	=	kIx~	=
Jewish	Jewish	k1gInSc1	Jewish
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
history	histor	k1gInPc1	histor
and	and	k?	and
monuments	monuments	k1gInSc1	monuments
of	of	k?	of
the	the	k?	the
Jewish	Jewish	k1gInSc1	Jewish
settlement	settlement	k1gInSc1	settlement
in	in	k?	in
Brno	Brno	k1gNnSc1	Brno
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
upr	upr	k?	upr
<g/>
.	.	kIx.	.
a	a	k8xC	a
rozš	rozš	k1gInSc1	rozš
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
ERA	ERA	kA	ERA
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
118	[number]	k4	118
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86517	[number]	k4	86517
<g/>
-	-	kIx~	-
<g/>
44	[number]	k4	44
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SAMEK	Samek	k1gMnSc1	Samek
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
<g/>
.	.	kIx.	.
</s>
<s>
Umělecké	umělecký	k2eAgFnPc1d1	umělecká
památky	památka	k1gFnPc1	památka
Moravy	Morava	k1gFnSc2	Morava
a	a	k8xC	a
Slezska	Slezsko	k1gNnSc2	Slezsko
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
A-	A-	k?	A-
<g/>
I.	I.	kA	I.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
651	[number]	k4	651
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
474	[number]	k4	474
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Brna	Brno	k1gNnSc2	Brno
(	(	kIx(	(
<g/>
od	od	k7c2	od
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
PROCHÁZKA	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Brna	Brno	k1gNnSc2	Brno
1	[number]	k4	1
<g/>
:	:	kIx,	:
Od	od	k7c2	od
pravěku	pravěk	k1gInSc2	pravěk
k	k	k7c3	k
ranému	raný	k2eAgInSc3d1	raný
středověku	středověk	k1gInSc3	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Statutární	statutární	k2eAgNnSc1d1	statutární
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
Archiv	archiv	k1gInSc1	archiv
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
736	[number]	k4	736
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86736	[number]	k4	86736
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
JAN	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
Libor	Libor	k1gMnSc1	Libor
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Brna	Brno	k1gNnSc2	Brno
2	[number]	k4	2
<g/>
:	:	kIx,	:
Středověké	středověký	k2eAgNnSc1d1	středověké
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Statutární	statutární	k2eAgNnSc1d1	statutární
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
Archiv	archiv	k1gInSc1	archiv
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
1071	[number]	k4	1071
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86736	[number]	k4	86736
<g/>
-	-	kIx~	-
<g/>
36	[number]	k4	36
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FASORA	FASORA	kA	FASORA
<g/>
,	,	kIx,	,
Lukáš	Lukáš	k1gMnSc1	Lukáš
<g/>
;	;	kIx,	;
ŠTĚPÁNEK	Štěpánek	k1gMnSc1	Štěpánek
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Brna	Brno	k1gNnSc2	Brno
6	[number]	k4	6
<g/>
:	:	kIx,	:
Předměstské	předměstský	k2eAgFnSc2d1	předměstská
obce	obec	k1gFnSc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Statutární	statutární	k2eAgNnSc1d1	statutární
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
Archiv	archiv	k1gInSc1	archiv
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
1245	[number]	k4	1245
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86736	[number]	k4	86736
<g/>
-	-	kIx~	-
<g/>
54	[number]	k4	54
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KROUPA	Kroupa	k1gMnSc1	Kroupa
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Brna	Brno	k1gNnSc2	Brno
7	[number]	k4	7
<g/>
:	:	kIx,	:
Uměleckohistorické	uměleckohistorický	k2eAgFnSc2d1	Uměleckohistorická
památky	památka	k1gFnSc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Statutární	statutární	k2eAgNnSc1d1	statutární
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
Archiv	archiv	k1gInSc1	archiv
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
885	[number]	k4	885
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86736	[number]	k4	86736
<g/>
-	-	kIx~	-
<g/>
46	[number]	k4	46
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Brno	Brno	k1gNnSc4	Brno
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Brno	Brno	k1gNnSc4	Brno
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Brno	Brno	k1gNnSc4	Brno
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Brno	Brno	k1gNnSc4	Brno
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Brno	Brno	k1gNnSc1	Brno
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Průvodce	průvodka	k1gFnSc3	průvodka
Brno	Brno	k1gNnSc1	Brno
ve	v	k7c6	v
Wikicestách	Wikicesta	k1gFnPc6	Wikicesta
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Brno	Brno	k1gNnSc1	Brno
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Statutární	statutární	k2eAgNnSc1d1	statutární
město	město	k1gNnSc1	město
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
brno	brno	k1gNnSc1	brno
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Turistické	turistický	k2eAgNnSc1d1	turistické
informační	informační	k2eAgNnSc1d1	informační
centrum	centrum	k1gNnSc1	centrum
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
ticbrno	ticbrno	k1gNnSc1	ticbrno
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Encyklopedie	encyklopedie	k1gFnSc2	encyklopedie
dějin	dějiny	k1gFnPc2	dějiny
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
.	.	kIx.	.
<g/>
brna	brna	k1gFnSc1	brna
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Staré	Staré	k2eAgFnPc1d1	Staré
mapy	mapa	k1gFnPc1	mapa
města	město	k1gNnSc2	město
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
vilemwalter	vilemwalter	k1gInSc1	vilemwalter
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
/	/	kIx~	/
<g/>
mapy	mapa	k1gFnPc1	mapa
</s>
</p>
