<s>
Roman	Roman	k1gMnSc1	Roman
Fiala	Fiala	k1gMnSc1	Fiala
(	(	kIx(	(
<g/>
*	*	kIx~	*
3	[number]	k4	3
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1965	[number]	k4	1965
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
soudce	soudce	k1gMnSc1	soudce
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2011	[number]	k4	2011
také	také	k9	také
jeho	jeho	k3xOp3gMnSc1	jeho
místopředseda	místopředseda	k1gMnSc1	místopředseda
<g/>
.	.	kIx.	.
</s>
<s>
Odborně	odborně	k6eAd1	odborně
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
především	především	k6eAd1	především
dědickému	dědický	k2eAgNnSc3d1	dědické
právu	právo	k1gNnSc3	právo
<g/>
.	.	kIx.	.
</s>
<s>
Absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
Právnickou	právnický	k2eAgFnSc4d1	právnická
fakultu	fakulta	k1gFnSc4	fakulta
UJEP	UJEP	kA	UJEP
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
MU	MU	kA	MU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
soudce	soudce	k1gMnSc1	soudce
působí	působit	k5eAaImIp3nS	působit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejprve	nejprve	k6eAd1	nejprve
u	u	k7c2	u
Městského	městský	k2eAgInSc2d1	městský
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
u	u	k7c2	u
Krajského	krajský	k2eAgInSc2d1	krajský
soudu	soud	k1gInSc2	soud
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
byl	být	k5eAaImAgMnS	být
jeho	jeho	k3xOp3gMnSc7	jeho
místopředsedou	místopředseda	k1gMnSc7	místopředseda
<g/>
)	)	kIx)	)
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
u	u	k7c2	u
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
jako	jako	k8xC	jako
předseda	předseda	k1gMnSc1	předseda
senátu	senát	k1gInSc2	senát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
místopředsedy	místopředseda	k1gMnSc2	místopředseda
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
ho	on	k3xPp3gMnSc4	on
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc3	leden
2011	[number]	k4	2011
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
prezident	prezident	k1gMnSc1	prezident
Václav	Václav	k1gMnSc1	Václav
Klaus	Klaus	k1gMnSc1	Klaus
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
redakčních	redakční	k2eAgFnPc2d1	redakční
rad	rada	k1gFnPc2	rada
časopisů	časopis	k1gInPc2	časopis
Soudní	soudní	k2eAgFnSc1d1	soudní
judikatura	judikatura	k1gFnSc1	judikatura
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
občanského	občanský	k2eAgNnSc2d1	občanské
<g/>
,	,	kIx,	,
obchodního	obchodní	k2eAgNnSc2d1	obchodní
a	a	k8xC	a
pracovního	pracovní	k2eAgNnSc2d1	pracovní
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
Ad	ad	k7c4	ad
notam	notam	k1gInSc4	notam
<g/>
,	,	kIx,	,
také	také	k9	také
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Vědecké	vědecký	k2eAgFnSc2d1	vědecká
rady	rada	k1gFnSc2	rada
Právnické	právnický	k2eAgFnSc2d1	právnická
fakulty	fakulta	k1gFnSc2	fakulta
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Správní	správní	k2eAgFnSc2d1	správní
rady	rada	k1gFnSc2	rada
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
předsedou	předseda	k1gMnSc7	předseda
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
komise	komise	k1gFnSc2	komise
pro	pro	k7c4	pro
rekodifikaci	rekodifikace	k1gFnSc4	rekodifikace
českého	český	k2eAgInSc2d1	český
civilního	civilní	k2eAgInSc2d1	civilní
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
také	také	k9	také
ocenění	ocenění	k1gNnSc4	ocenění
Právník	právník	k1gMnSc1	právník
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Občanské	občanský	k2eAgNnSc4d1	občanské
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Roman	Roman	k1gMnSc1	Roman
Fiala	Fiala	k1gMnSc1	Fiala
FIALA	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
<g/>
.	.	kIx.	.
</s>
<s>
Přehled	přehled	k1gInSc1	přehled
judikatury	judikatura	k1gFnSc2	judikatura
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
dědických	dědický	k2eAgFnPc6d1	dědická
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
ASPI	ASPI	kA	ASPI
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
494	[number]	k4	494
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7357	[number]	k4	7357
<g/>
-	-	kIx~	-
<g/>
182	[number]	k4	182
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
FIALA	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
Roman	Roman	k1gMnSc1	Roman
<g/>
;	;	kIx,	;
DRÁPAL	Drápal	k1gMnSc1	Drápal
<g/>
,	,	kIx,	,
Ljubomír	Ljubomír	k1gMnSc1	Ljubomír
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Občanský	občanský	k2eAgInSc1d1	občanský
zákoník	zákoník	k1gInSc1	zákoník
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Dědické	dědický	k2eAgNnSc1d1	dědické
právo	právo	k1gNnSc1	právo
(	(	kIx(	(
<g/>
§	§	k?	§
1475	[number]	k4	1475
<g/>
–	–	k?	–
<g/>
1720	[number]	k4	1720
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Komentář	komentář	k1gInSc1	komentář
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
C.	C.	kA	C.
H.	H.	kA	H.
Beck	Beck	k1gMnSc1	Beck
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
648	[number]	k4	648
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Velké	velký	k2eAgInPc1d1	velký
komentáře	komentář	k1gInPc1	komentář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7400	[number]	k4	7400
<g/>
-	-	kIx~	-
<g/>
570	[number]	k4	570
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
