<p>
<s>
Hélicoptè	Hélicoptè	k?	Hélicoptè
Guimbal	Guimbal	k1gInSc1	Guimbal
je	být	k5eAaImIp3nS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
výrobce	výrobce	k1gMnSc1	výrobce
vrtulníků	vrtulník	k1gInPc2	vrtulník
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
a	a	k8xC	a
sídlí	sídlet	k5eAaImIp3nS	sídlet
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
Les	les	k1gInSc1	les
Milles	Milles	k1gInSc1	Milles
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
konstruktérem	konstruktér	k1gMnSc7	konstruktér
a	a	k8xC	a
majitelem	majitel	k1gMnSc7	majitel
je	být	k5eAaImIp3nS	být
Bruno	Bruno	k1gMnSc1	Bruno
Guimbal	Guimbal	k1gInSc1	Guimbal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vyráběné	vyráběný	k2eAgInPc1d1	vyráběný
vrtulníky	vrtulník	k1gInPc1	vrtulník
==	==	k?	==
</s>
</p>
<p>
<s>
Guimbal	Guimbal	k1gInSc1	Guimbal
Cabri	Cabr	k1gFnSc2	Cabr
G2	G2	k1gFnSc2	G2
–	–	k?	–
lehký	lehký	k2eAgInSc1d1	lehký
dvoumístný	dvoumístný	k2eAgInSc1d1	dvoumístný
pístový	pístový	k2eAgInSc1d1	pístový
vrtulník	vrtulník	k1gInSc1	vrtulník
s	s	k7c7	s
třílistým	třílistý	k2eAgInSc7d1	třílistý
hlavním	hlavní	k2eAgInSc7d1	hlavní
rotorem	rotor	k1gInSc7	rotor
a	a	k8xC	a
ocasní	ocasní	k2eAgFnSc7d1	ocasní
vrtulkou	vrtulka	k1gFnSc7	vrtulka
typu	typ	k1gInSc2	typ
fenestron	fenestron	k1gInSc1	fenestron
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Stránky	stránka	k1gFnSc2	stránka
výrobce	výrobce	k1gMnSc2	výrobce
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
České	český	k2eAgFnSc2d1	Česká
stránky	stránka	k1gFnSc2	stránka
o	o	k7c6	o
vrtulníku	vrtulník	k1gInSc6	vrtulník
</s>
</p>
