<s>
Orion	orion	k1gInSc1	orion
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Michal	Michal	k1gMnSc1	Michal
Opletal	Opletal	k1gMnSc1	Opletal
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
také	také	k9	také
Papa	papa	k1gMnSc1	papa
Vorel	Vorel	k1gMnSc1	Vorel
<g/>
,	,	kIx,	,
Miki	Mik	k1gMnPc1	Mik
<g/>
,	,	kIx,	,
Papa	papa	k1gMnSc1	papa
Ori	Ori	k1gFnSc2	Ori
<g/>
,	,	kIx,	,
Miki	Mik	k1gFnSc2	Mik
červená	červený	k2eAgFnSc1d1	červená
hlava	hlava	k1gFnSc1	hlava
<g/>
,	,	kIx,	,
Ori	Ori	k1gFnSc1	Ori
<g/>
,	,	kIx,	,
Mitch	Mitch	k1gInSc1	Mitch
Bjukenen	Bjukenen	k1gInSc1	Bjukenen
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
rapper	rapper	k1gInSc1	rapper
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
založil	založit	k5eAaPmAgMnS	založit
hip-hopovou	hipopový	k2eAgFnSc4d1	hip-hopová
skupinu	skupina	k1gFnSc4	skupina
Peneři	Pener	k1gMnPc1	Pener
strýčka	strýček	k1gMnSc2	strýček
Homeboye	Homeboy	k1gMnSc2	Homeboy
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
vystupující	vystupující	k2eAgMnPc1d1	vystupující
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
PSH	PSH	kA	PSH
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
odmítá	odmítat	k5eAaImIp3nS	odmítat
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
velkými	velký	k2eAgFnPc7d1	velká
společnostmi	společnost	k1gFnPc7	společnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
(	(	kIx(	(
<g/>
jak	jak	k8xC	jak
sám	sám	k3xTgMnSc1	sám
říká	říkat	k5eAaImIp3nS	říkat
<g/>
)	)	kIx)	)
by	by	kYmCp3nP	by
znamenaly	znamenat	k5eAaImAgInP	znamenat
ztrátu	ztráta	k1gFnSc4	ztráta
identity	identita	k1gFnSc2	identita
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
desek	deska	k1gFnPc2	deska
uvedených	uvedený	k2eAgFnPc2d1	uvedená
v	v	k7c6	v
diskografii	diskografie	k1gFnSc6	diskografie
ještě	ještě	k6eAd1	ještě
hostoval	hostovat	k5eAaImAgInS	hostovat
na	na	k7c6	na
několika	několik	k4yIc6	několik
deskách	deska	k1gFnPc6	deska
několika	několik	k4yIc2	několik
dalších	další	k2eAgFnPc2d1	další
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
dostal	dostat	k5eAaPmAgMnS	dostat
zlatého	zlatý	k2eAgMnSc4d1	zlatý
Anděla	Anděl	k1gMnSc4	Anděl
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
hiphop	hiphop	k1gInSc4	hiphop
&	&	k?	&
r	r	kA	r
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
b	b	k?	b
album	album	k1gNnSc4	album
za	za	k7c4	za
desku	deska	k1gFnSc4	deska
Teritorium	teritorium	k1gNnSc4	teritorium
II	II	kA	II
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2006	[number]	k4	2006
PSH	PSH	kA	PSH
vyšla	vyjít	k5eAaPmAgFnS	vyjít
deska	deska	k1gFnSc1	deska
Rap	rap	k1gMnSc1	rap
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
Roll	Rolla	k1gFnPc2	Rolla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
PSH	PSH	kA	PSH
vydali	vydat	k5eAaPmAgMnP	vydat
svou	svůj	k3xOyFgFnSc4	svůj
zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgFnSc4d1	poslední
desku	deska	k1gFnSc4	deska
Epilog	epilog	k1gInSc1	epilog
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
vydal	vydat	k5eAaPmAgInS	vydat
Orion	orion	k1gInSc4	orion
mixtape	mixtapat	k5eAaPmIp3nS	mixtapat
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
vůle	vůle	k1gFnSc1	vůle
zboží	zboží	k1gNnSc2	zboží
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
předzvěstí	předzvěst	k1gFnSc7	předzvěst
další	další	k2eAgFnSc2d1	další
sólové	sólový	k2eAgFnSc2d1	sólová
desky	deska	k1gFnSc2	deska
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
pro	pro	k7c4	pro
vydání	vydání	k1gNnSc4	vydání
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
demokazeta	demokazeta	k1gFnSc1	demokazeta
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
Skladeb	skladba	k1gFnPc2	skladba
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
Introň	Introň	k1gMnPc1	Introň
Introň	Introň	k1gFnPc2	Introň
II	II	kA	II
Metro	metro	k1gNnSc4	metro
Sloni	sloň	k1gFnSc2wR	sloň
Už	už	k6eAd1	už
'	'	kIx"	'
<g/>
Dou	Dou	k?	Dou
Venku	venku	k6eAd1	venku
Salám	salám	k1gInSc1	salám
Boombon	Boombona	k1gFnPc2	Boombona
Sněží	sněžit	k5eAaImIp3nS	sněžit
Bez	bez	k7c2	bez
Tripů	Trip	k1gInPc2	Trip
R.	R.	kA	R.
<g/>
M.	M.	kA	M.
<g/>
S	s	k7c7	s
Ťik	Ťik	k1gFnSc7	Ťik
<g/>
,	,	kIx,	,
Ťak	Ťak	k1gFnSc7	Ťak
<g/>
,	,	kIx,	,
Hurá	hurá	k0	hurá
Hipík	Hipík	k1gInSc1	Hipík
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Rap	rap	k1gMnSc1	rap
Feťácký	Feťácký	k2eAgMnSc1d1	Feťácký
Děti	dítě	k1gFnPc4	dítě
2	[number]	k4	2
Džony	Džon	k1gMnPc7	Džon
je	být	k5eAaImIp3nS	být
TOP	topit	k5eAaImRp2nS	topit
(	(	kIx(	(
<g/>
demokazeta	demokazeta	k1gFnSc1	demokazeta
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
skladeb	skladba	k1gFnPc2	skladba
<g/>
:	:	kIx,	:
Pohádka	pohádka	k1gFnSc1	pohádka
Not	nota	k1gFnPc2	nota
Nigga	Nigg	k1gMnSc2	Nigg
Těžkej	těžkat	k5eAaImRp2nS	těžkat
Kov	kov	k1gInSc1	kov
Konef	Konef	k1gInSc4	Konef
Líná	línat	k5eAaImIp3nS	línat
Bohatej	Bohatej	k?	Bohatej
Pan	Pan	k1gMnSc1	Pan
Gaya	Gaya	k?	Gaya
Vykydej	vykydat	k5eAaPmRp2nS	vykydat
ten	ten	k3xDgInSc4	ten
hnůj	hnůj	k1gInSc4	hnůj
Jižák	Jižák	k1gInSc1	Jižák
Metro	metro	k1gNnSc1	metro
(	(	kIx(	(
<g/>
2	[number]	k4	2
CD	CD	kA	CD
<g/>
)	)	kIx)	)
Komplet	komplet	k2eAgFnPc2d1	komplet
skladeb	skladba	k1gFnPc2	skladba
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
dvou	dva	k4xCgInPc2	dva
CD	CD	kA	CD
a	a	k8xC	a
jednoho	jeden	k4xCgInSc2	jeden
předchozího	předchozí	k2eAgInSc2d1	předchozí
CD	CD	kA	CD
skupiny	skupina	k1gFnSc2	skupina
WWW	WWW	kA	WWW
Intro	Intro	k1gNnSc1	Intro
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
La	la	k1gNnSc1	la
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
Společný	společný	k2eAgInSc4d1	společný
Zájmy	zájem	k1gInPc4	zájem
(	(	kIx(	(
<g/>
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Dano	Dana	k1gFnSc5	Dana
<g/>
)	)	kIx)	)
Zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
Dnů	den	k1gInPc2	den
(	(	kIx(	(
<g/>
feat	feata	k1gFnPc2	feata
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Čoko	Čoko	k1gMnSc1	Čoko
<g/>
)	)	kIx)	)
Ambice	ambice	k1gFnPc1	ambice
Na	na	k7c6	na
Východě	východ	k1gInSc6	východ
Dimenzio	Dimenzio	k1gMnSc1	Dimenzio
K2	K2	k1gMnSc1	K2
(	(	kIx(	(
<g/>
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Ponso	Ponsa	k1gFnSc5	Ponsa
&	&	k?	&
Azur	azur	k1gInSc1	azur
<g/>
)	)	kIx)	)
Tréning	Tréning	k1gInSc1	Tréning
Tak	tak	k6eAd1	tak
a	a	k8xC	a
Teď	teď	k6eAd1	teď
Hlasuju	hlasovat	k5eAaImIp1nS	hlasovat
Proti	proti	k7c3	proti
(	(	kIx(	(
<g/>
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Indy	Indus	k1gInPc1	Indus
&	&	k?	&
Wich	Wich	k1gInSc1	Wich
<g/>
)	)	kIx)	)
Zatím	zatím	k6eAd1	zatím
Díky	dík	k1gInPc1	dík
Úplnej	Úplnej	k?	Úplnej
Klid	klid	k1gInSc1	klid
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Čoko	Čoko	k1gMnSc1	Čoko
<g/>
)	)	kIx)	)
Autro	Autro	k1gNnSc1	Autro
Chcete	chtít	k5eAaImIp2nP	chtít
Mě	já	k3xPp1nSc4	já
Další	další	k2eAgNnSc4d1	další
Novej	Novej	k?	Novej
Den	den	k1gInSc4	den
Teritorium	teritorium	k1gNnSc1	teritorium
Chudáčci	chudáček	k1gMnPc1	chudáček
featuring	featuring	k1gInSc4	featuring
Čistychov	Čistychov	k1gInSc1	Čistychov
a	a	k8xC	a
Tina	Tina	k1gFnSc1	Tina
Skit	Skit	k1gInSc1	Skit
1	[number]	k4	1
Dokument	dokument	k1gInSc1	dokument
Orionek	Orionek	k1gInSc4	Orionek
Remix	Remix	k1gInSc4	Remix
Ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
Víno	víno	k1gNnSc1	víno
<g/>
,	,	kIx,	,
Zpěv	zpěv	k1gInSc1	zpěv
Stoupat	stoupat	k5eAaImF	stoupat
Výš	vysoce	k6eAd2	vysoce
Skit	Skit	k1gInSc4	Skit
2	[number]	k4	2
Bys	by	kYmCp2nS	by
Chtěl	chtít	k5eAaImAgMnS	chtít
Víc	hodně	k6eAd2	hodně
3000	[number]	k4	3000
Intro	Intro	k1gNnSc1	Intro
Kam	kam	k6eAd1	kam
vítr	vítr	k1gInSc1	vítr
tam	tam	k6eAd1	tam
plášť	plášť	k1gInSc1	plášť
Past	pasta	k1gFnPc2	pasta
Mamon	mamona	k1gFnPc2	mamona
Nezapomeň	zapomnět	k5eNaImRp2nS	zapomnět
Všechno	všechen	k3xTgNnSc1	všechen
pomíjivý	pomíjivý	k2eAgMnSc1d1	pomíjivý
je	být	k5eAaImIp3nS	být
Odkud	odkud	k6eAd1	odkud
jsme	být	k5eAaImIp1nP	být
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
kam	kam	k6eAd1	kam
jdem	jdem	k?	jdem
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
směřujem	směřujem	k?	směřujem
Skit	Skit	k1gInSc1	Skit
zápas	zápas	k1gInSc1	zápas
Zápas	zápas	k1gInSc1	zápas
Klony	klon	k1gInPc4	klon
Pobřežní	pobřežní	k2eAgFnSc1d1	pobřežní
hlídka	hlídka	k1gFnSc1	hlídka
Remix	Remix	k1gInSc1	Remix
Bong	bongo	k1gNnPc2	bongo
song	song	k1gInSc1	song
#	#	kIx~	#
<g/>
2	[number]	k4	2
Tréning	Tréning	k1gInSc1	Tréning
[	[	kIx(	[
<g/>
RMX	RMX	kA	RMX
<g/>
]	]	kIx)	]
Klubový	klubový	k2eAgInSc1d1	klubový
Pravidla	pravidlo	k1gNnSc2	pravidlo
[	[	kIx(	[
<g/>
RMX	RMX	kA	RMX
<g/>
]	]	kIx)	]
Teritorium	teritorium	k1gNnSc1	teritorium
Kam	kam	k6eAd1	kam
Vítr	vítr	k1gInSc1	vítr
<g/>
,	,	kIx,	,
Tam	tam	k6eAd1	tam
Plášť	plášť	k1gInSc1	plášť
[	[	kIx(	[
<g/>
RMX	RMX	kA	RMX
<g/>
]	]	kIx)	]
Chci	chtít	k5eAaImIp1nS	chtít
Slyšet	slyšet	k5eAaImF	slyšet
Svý	Svý	k?	Svý
Jméno	jméno	k1gNnSc4	jméno
Chudáčci	chudáček	k1gMnPc1	chudáček
Noční	noční	k2eAgFnSc2d1	noční
<g />
.	.	kIx.	.
</s>
<s>
Jezdec	jezdec	k1gInSc1	jezdec
Past	pasta	k1gFnPc2	pasta
Bong	bongo	k1gNnPc2	bongo
Song	song	k1gInSc1	song
[	[	kIx(	[
<g/>
RMX	RMX	kA	RMX
<g/>
]	]	kIx)	]
Bys	by	kYmCp2nS	by
Chtěl	chtít	k5eAaImAgMnS	chtít
Víc	hodně	k6eAd2	hodně
[	[	kIx(	[
<g/>
RMX	RMX	kA	RMX
<g/>
]	]	kIx)	]
Ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
Víno	víno	k1gNnSc1	víno
<g/>
,	,	kIx,	,
Zpěv	zpěv	k1gInSc1	zpěv
Mamon	mamon	k1gInSc1	mamon
[	[	kIx(	[
<g/>
RMX	RMX	kA	RMX
<g/>
]	]	kIx)	]
Nezapomeš	Nezapomeš	k1gMnSc1	Nezapomeš
Co	co	k9	co
Je	být	k5eAaImIp3nS	být
Doma	doma	k6eAd1	doma
To	to	k9	to
Se	se	k3xPyFc4	se
Počítá	počítat	k5eAaImIp3nS	počítat
Odkud	odkud	k6eAd1	odkud
Jsme	být	k5eAaImIp1nP	být
<g/>
,	,	kIx,	,
Kam	kam	k6eAd1	kam
Jdem	Jdem	k?	Jdem
A	a	k8xC	a
Kam	kam	k6eAd1	kam
Směřujem	Směřujem	k?	Směřujem
Zápas	zápas	k1gInSc1	zápas
Pobřežní	pobřežní	k1gMnSc1	pobřežní
Hlídka	hlídka	k1gFnSc1	hlídka
Všechno	všechen	k3xTgNnSc1	všechen
Pomíjivý	pomíjivý	k2eAgMnSc1d1	pomíjivý
Je	být	k5eAaImIp3nS	být
Intro	Intro	k1gNnSc4	Intro
Znova	znova	k6eAd1	znova
1985	[number]	k4	1985
Parket	parketa	k1gFnPc2	parketa
(	(	kIx(	(
<g/>
feat	feata	k1gFnPc2	feata
<g/>
.	.	kIx.	.
</s>
<s>
Čistychov	Čistychov	k1gInSc1	Čistychov
<g/>
)	)	kIx)	)
Kamufláž	kamufláž	k1gFnSc1	kamufláž
Jackpot	jackpot	k1gInSc4	jackpot
Wolfův	Wolfův	k2eAgInSc1d1	Wolfův
Revír	revír	k1gInSc1	revír
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
<g/>
)	)	kIx)	)
Klubový	klubový	k2eAgInSc1d1	klubový
Pravidla	pravidlo	k1gNnPc4	pravidlo
Nemám	mít	k5eNaImIp1nS	mít
Rád	rád	k2eAgInSc1d1	rád
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Lela	Lela	k1gFnSc1	Lela
<g/>
)	)	kIx)	)
Tf	tf	k0wR	tf
Tf	tf	k0wR	tf
(	(	kIx(	(
<g/>
Dj	Dj	k1gFnPc6	Dj
Mike	Mik	k1gInSc2	Mik
Trafik	trafika	k1gFnPc2	trafika
<g/>
)	)	kIx)	)
Rok	rok	k1gInSc1	rok
PSH	PSH	kA	PSH
Moje	můj	k3xOp1gFnSc1	můj
Řeč	řeč	k1gFnSc1	řeč
Parket	parket	k1gInSc4	parket
Remix	Remix	k1gInSc1	Remix
(	(	kIx(	(
<g/>
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Čistychov	Čistychov	k1gInSc1	Čistychov
<g/>
,	,	kIx,	,
Indy	Indus	k1gInPc1	Indus
&	&	k?	&
Supercrooo	Supercrooo	k6eAd1	Supercrooo
<g/>
)	)	kIx)	)
Neutečeš	utéct	k5eNaPmIp2nS	utéct
(	(	kIx(	(
<g/>
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
La	la	k1gNnSc1	la
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
Outro	Outro	k1gNnSc4	Outro
Zeme	Zem	k1gInSc2	Zem
Vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
After	After	k1gInSc1	After
Párty	párty	k1gFnSc7	párty
Nikdy	nikdy	k6eAd1	nikdy
nevíš	vědět	k5eNaImIp2nS	vědět
co	co	k3yQnSc4	co
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
stát	stát	k1gInSc1	stát
Slečny	slečna	k1gFnSc2	slečna
<g/>
,	,	kIx,	,
<g/>
Dívky	dívka	k1gFnSc2	dívka
Švihák	švihák	k1gMnSc1	švihák
Lázenský	Lázenský	k2eAgMnSc1d1	Lázenský
Drby	drb	k1gInPc4	drb
Lůzr	Lůzra	k1gFnPc2	Lůzra
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
Stár	stár	k2eAgInSc1d1	stár
Rumba	rumba	k1gFnSc1	rumba
Koule	koule	k1gFnPc1	koule
Funky	funk	k1gInPc1	funk
Boys	boy	k1gMnPc2	boy
Rap	rap	k1gMnSc1	rap
Biz	Biz	k1gMnSc1	Biz
Už	už	k6eAd1	už
nechci	chtít	k5eNaImIp1nS	chtít
být	být	k5eAaImF	být
sám	sám	k3xTgMnSc1	sám
Drazí	drahý	k1gMnPc1	drahý
milovníci	milovník	k1gMnPc1	milovník
hudby	hudba	k1gFnSc2	hudba
Udělejte	udělat	k5eAaPmRp2nP	udělat
bordel	bordel	k1gInSc4	bordel
Podpantoflák	Podpantoflák	k1gMnSc1	Podpantoflák
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Roman	Roman	k1gMnSc1	Roman
Holý	Holý	k1gMnSc1	Holý
Chyť	chytit	k5eAaPmRp2nS	chytit
mě	já	k3xPp1nSc4	já
jestli	jestli	k9	jestli
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
máš	mít	k5eAaImIp2nS	mít
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
Dara	Dara	k1gMnSc1	Dara
Rolins	Rolinsa	k1gFnPc2	Rolinsa
Můj	můj	k1gMnSc1	můj
rap	rap	k1gMnSc1	rap
<g/>
,	,	kIx,	,
můj	můj	k3xOp1gInSc4	můj
svět	svět	k1gInSc4	svět
Showbizu	Showbiz	k1gInSc2	Showbiz
smrad	smrad	k1gInSc4	smrad
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
Ota	Ota	k1gMnSc1	Ota
Klempíř	Klempíř	k1gMnSc1	Klempíř
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
V	V	kA	V
Prachy	prach	k1gInPc1	prach
dělaj	dělaj	k?	dělaj
člověka	člověk	k1gMnSc4	člověk
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Čistychov	Čistychov	k1gInSc1	Čistychov
Všichni	všechen	k3xTgMnPc1	všechen
jsou	být	k5eAaImIp3nP	být
už	už	k6eAd1	už
v	v	k7c6	v
německu	německ	k1gInSc6	německ
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc4	Toxxx
Wo	Wo	k1gFnSc2	Wo
ist	ist	k?	ist
meine	meinout	k5eAaImIp3nS	meinout
Heimat	Heimat	k1gInSc1	Heimat
<g/>
?	?	kIx.	?
</s>
<s>
Yes	Yes	k?	Yes
No	no	k9	no
Ok	oko	k1gNnPc2	oko
feat	feata	k1gFnPc2	feata
<g/>
.	.	kIx.	.
</s>
<s>
Matěj	Matěj	k1gMnSc1	Matěj
Ruppert	Ruppert	k1gMnSc1	Ruppert
Vim	Vim	k?	Vim
já	já	k3xPp1nSc1	já
<g/>
?	?	kIx.	?
</s>
<s>
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
Cole	cola	k1gFnSc3	cola
C	C	kA	C
<g/>
'	'	kIx"	'
<g/>
mon	mon	k?	mon
Girls	girl	k1gFnPc1	girl
feat	feata	k1gFnPc2	feata
<g/>
.	.	kIx.	.
</s>
<s>
Tereza	Tereza	k1gFnSc1	Tereza
Černochová	Černochová	k1gFnSc1	Černochová
Jako	jako	k8xS	jako
jed	jed	k1gInSc1	jed
Co	co	k9	co
bude	být	k5eAaImBp3nS	být
až	až	k6eAd1	až
nebude	být	k5eNaImBp3nS	být
rap	rap	k1gMnSc1	rap
Pravda	pravda	k9	pravda
o	o	k7c6	o
PSH	PSH	kA	PSH
V	v	k7c6	v
hajzlu	hajzl	k1gInSc6	hajzl
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Freezer	Freezer	k1gInSc1	Freezer
<g/>
)	)	kIx)	)
Vesmírnej	Vesmírnej	k?	Vesmírnej
odpad	odpad	k1gInSc1	odpad
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Donie	Donie	k1gFnSc1	Donie
Darko	Darko	k1gNnSc4	Darko
<g/>
)	)	kIx)	)
Ó	Ó	kA	Ó
jáááj	jáááj	k1gMnSc1	jáááj
(	(	kIx(	(
<g/>
prod	prod	k1gMnSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
KG	kg	kA	kg
<g/>
)	)	kIx)	)
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
je	být	k5eAaImIp3nS	být
všechno	všechen	k3xTgNnSc1	všechen
fajn	fajn	k6eAd1	fajn
feat	feat	k2eAgInSc1d1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Vladimir	Vladimir	k1gInSc1	Vladimir
518	[number]	k4	518
(	(	kIx(	(
<g/>
DJ	DJ	kA	DJ
Wich	Wich	k1gInSc1	Wich
remix	remix	k1gInSc1	remix
<g/>
)	)	kIx)	)
Chci	chtít	k5eAaImIp1nS	chtít
slyšet	slyšet	k5eAaImF	slyšet
svý	svý	k?	svý
jméno	jméno	k1gNnSc4	jméno
(	(	kIx(	(
<g/>
Rack	Rack	k1gInSc1	Rack
Rokas	Rokas	k1gInSc1	Rokas
2013	[number]	k4	2013
rmx	rmx	k?	rmx
<g/>
)	)	kIx)	)
Jedu	jet	k5eAaImIp1nS	jet
jako	jako	k9	jako
bauch	bauch	k1gMnSc1	bauch
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Abe	Abe	k?	Abe
<g/>
)	)	kIx)	)
Báječný	báječný	k2eAgMnSc1d1	báječný
klima	klima	k1gNnSc4	klima
feat	feat	k1gInSc4	feat
<g/>
.	.	kIx.	.
</s>
<s>
Nicole	Nicole	k1gFnSc1	Nicole
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
KMBL	KMBL	kA	KMBL
<g/>
)	)	kIx)	)
Zmiz	zmizet	k5eAaPmRp2nS	zmizet
feat	feat	k1gInSc4	feat
<g/>
.	.	kIx.	.
</s>
<s>
Ektor	Ektor	k1gInSc1	Ektor
<g/>
,	,	kIx,	,
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
DJ	DJ	kA	DJ
Wich	Wicha	k1gFnPc2	Wicha
<g/>
)	)	kIx)	)
Čo	Čo	k1gFnPc2	Čo
tu	tu	k6eAd1	tu
máme	mít	k5eAaImIp1nP	mít
dnes	dnes	k6eAd1	dnes
2	[number]	k4	2
(	(	kIx(	(
<g/>
prod	proda	k1gFnPc2	proda
<g/>
.	.	kIx.	.
</s>
<s>
DJ	DJ	kA	DJ
Wich	Wich	k1gInSc1	Wich
<g/>
)	)	kIx)	)
Volný	volný	k2eAgInSc1d1	volný
pád	pád	k1gInSc1	pád
feat	feat	k2eAgInSc1d1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Dara	Dara	k1gFnSc1	Dara
Rolins	Rolinsa	k1gFnPc2	Rolinsa
(	(	kIx(	(
<g/>
prod	proda	k1gFnPc2	proda
<g/>
.	.	kIx.	.
</s>
<s>
DJ	DJ	kA	DJ
Mike	Mike	k1gFnSc1	Mike
Trafik	trafika	k1gFnPc2	trafika
<g/>
)	)	kIx)	)
Paranoia	paranoia	k1gFnSc1	paranoia
feat	feata	k1gFnPc2	feata
<g/>
.	.	kIx.	.
</s>
<s>
Berezin	Berezina	k1gFnPc2	Berezina
(	(	kIx(	(
<g/>
prod	proda	k1gFnPc2	proda
<g/>
.	.	kIx.	.
</s>
<s>
Risto	Risto	k1gNnSc1	Risto
<g/>
)	)	kIx)	)
Dokument	dokument	k1gInSc1	dokument
Orionek	Orionek	k1gInSc1	Orionek
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Orion	orion	k1gInSc1	orion
<g/>
,	,	kIx,	,
DJ	DJ	kA	DJ
Wich	Wich	k1gInSc1	Wich
<g/>
)	)	kIx)	)
Gastro	Gastro	k1gNnSc1	Gastro
brekeke	brekek	k1gFnSc2	brekek
feat	feata	k1gFnPc2	feata
<g/>
.	.	kIx.	.
</s>
<s>
Dinero	Dinero	k1gNnSc1	Dinero
<g/>
,	,	kIx,	,
Hafner	Hafner	k1gInSc1	Hafner
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Beyuz	Beyuz	k1gInSc1	Beyuz
<g/>
)	)	kIx)	)
Keď	Keď	k1gMnPc2	Keď
mam	mam	k1gInSc1	mam
slovo	slovo	k1gNnSc4	slovo
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
Ego	ego	k1gNnSc1	ego
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Dash	Dash	k1gInSc1	Dash
<g/>
)	)	kIx)	)
Krysy	krysa	k1gFnPc1	krysa
feat	feat	k1gInSc4	feat
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
(	(	kIx(	(
<g/>
DJ	DJ	kA	DJ
Mike	Mike	k1gFnPc2	Mike
Trafik	trafika	k1gFnPc2	trafika
<g/>
)	)	kIx)	)
Čo	Čo	k1gMnPc1	Čo
tu	ten	k3xDgFnSc4	ten
máme	mít	k5eAaImIp1nP	mít
dnes	dnes	k6eAd1	dnes
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
La	la	k1gNnSc1	la
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
Drvivá	Drvivý	k2eAgFnSc1d1	Drvivá
Menšina	menšina	k1gFnSc1	menšina
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Hajtkovič	Hajtkovič	k1gInSc1	Hajtkovič
<g/>
)	)	kIx)	)
Vaša	Vaša	k?	Vaša
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Čistychov	Čistychov	k1gInSc1	Čistychov
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Melicher	Melichra	k1gFnPc2	Melichra
<g/>
)	)	kIx)	)
Rockin	Rockin	k1gInSc1	Rockin
at	at	k?	at
the	the	k?	the
lobby	lobby	k1gFnSc3	lobby
bar	bar	k1gInSc4	bar
feat	feat	k2eAgInSc4d1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Vladimir	Vladimir	k1gInSc1	Vladimir
518	[number]	k4	518
<g/>
,	,	kIx,	,
Monkey	Monkea	k1gFnPc1	Monkea
Business	business	k1gInSc1	business
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Roman	Roman	k1gMnSc1	Roman
Holý	Holý	k1gMnSc1	Holý
<g/>
)	)	kIx)	)
Už	už	k6eAd1	už
vieš	vieš	k1gInSc1	vieš
kto	kto	k?	kto
je	být	k5eAaImIp3nS	být
feat	feat	k1gInSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Miky	Mik	k1gMnPc4	Mik
Mora	mora	k1gFnSc1	mora
<g/>
,	,	kIx,	,
Čistychov	Čistychov	k1gInSc1	Čistychov
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Čistychov	Čistychov	k1gInSc1	Čistychov
<g/>
)	)	kIx)	)
Nemám	mít	k5eNaImIp1nS	mít
zájem	zájem	k1gInSc4	zájem
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
Vladimir	Vladimir	k1gInSc1	Vladimir
518	[number]	k4	518
(	(	kIx(	(
<g/>
prod	proda	k1gFnPc2	proda
<g/>
.	.	kIx.	.
</s>
<s>
DJ	DJ	kA	DJ
Mike	Mike	k1gFnSc1	Mike
Trafik	trafika	k1gFnPc2	trafika
<g/>
)	)	kIx)	)
Celou	celý	k2eAgFnSc4d1	celá
noc	noc	k1gFnSc4	noc
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
Mike	Mike	k1gInSc1	Mike
Spirit	Spirit	k1gInSc1	Spirit
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Grimaso	grimasa	k1gFnSc5	grimasa
<g/>
)	)	kIx)	)
Autoerotika	Autoerotik	k1gMnSc4	Autoerotik
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
<g/>
,	,	kIx,	,
Papa	papa	k1gMnSc1	papa
Fini	Fin	k1gMnPc1	Fin
(	(	kIx(	(
<g/>
prod	prod	k1gMnSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
DJ	DJ	kA	DJ
Mike	Mike	k1gFnSc1	Mike
Trafik	trafika	k1gFnPc2	trafika
<g/>
)	)	kIx)	)
Traged	Traged	k1gMnSc1	Traged
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Mike	Mike	k1gInSc1	Mike
Spirit	Spirit	k1gInSc1	Spirit
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
KMBL	KMBL	kA	KMBL
<g/>
)	)	kIx)	)
Co	co	k3yQnSc4	co
je	být	k5eAaImIp3nS	být
doma	doma	k6eAd1	doma
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
DJ	DJ	kA	DJ
Mike	Mike	k1gFnSc1	Mike
Trafik	trafika	k1gFnPc2	trafika
<g/>
)	)	kIx)	)
Kde	kde	k6eAd1	kde
domov	domov	k1gInSc1	domov
můj	můj	k3xOp1gInSc4	můj
feat	feat	k1gInSc4	feat
<g/>
.	.	kIx.	.
</s>
<s>
Hana	Hana	k1gFnSc1	Hana
Hegerová	Hegerová	k1gFnSc1	Hegerová
<g/>
,	,	kIx,	,
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Firebird	Firebird	k1gInSc1	Firebird
Phoenix	Phoenix	k1gInSc1	Phoenix
<g/>
)	)	kIx)	)
8	[number]	k4	8
promile	promile	k1gNnPc2	promile
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
DJ	DJ	kA	DJ
Wich	Wich	k1gMnSc1	Wich
<g/>
)	)	kIx)	)
Tak	tak	k9	tak
party	party	k1gFnSc4	party
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
DJ	DJ	kA	DJ
Jorgos	Jorgos	k1gMnSc1	Jorgos
<g/>
)	)	kIx)	)
Dnes	dnes	k6eAd1	dnes
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
Robo	roba	k1gFnSc5	roba
Papp	Papp	k1gInSc1	Papp
<g/>
,	,	kIx,	,
Mike	Mike	k1gInSc1	Mike
Spirit	Spirit	k1gInSc1	Spirit
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Opak	opak	k1gInSc1	opak
<g/>
)	)	kIx)	)
Party	parta	k1gFnPc1	parta
závod	závod	k1gInSc4	závod
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
Berezin	Berezina	k1gFnPc2	Berezina
(	(	kIx(	(
<g/>
prod	proda	k1gFnPc2	proda
<g/>
.	.	kIx.	.
</s>
<s>
DJ	DJ	kA	DJ
Mike	Mike	k1gFnSc1	Mike
Trafik	trafika	k1gFnPc2	trafika
<g/>
)	)	kIx)	)
Jak	jak	k8xC	jak
sa	sa	k?	sa
vieš	vieš	k5eAaPmIp2nS	vieš
hýbať	hýbatit	k5eAaPmRp2nS	hýbatit
feat	feat	k1gInSc4	feat
<g/>
.	.	kIx.	.
</s>
<s>
Mike	Mike	k1gInSc1	Mike
Spirit	Spirit	k1gInSc1	Spirit
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Grimaso	grimasa	k1gFnSc5	grimasa
<g/>
)	)	kIx)	)
Dělám	dělat	k5eAaImIp1nS	dělat
jen	jen	k9	jen
to	ten	k3xDgNnSc1	ten
co	co	k9	co
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
(	(	kIx(	(
<g/>
prod	prod	k6eAd1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Donie	Donie	k1gFnSc1	Donie
Darko	Darko	k1gNnSc4	Darko
<g/>
)	)	kIx)	)
Túžim	Túžim	k1gMnSc1	Túžim
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
</s>
<s>
Dara	Dara	k1gFnSc1	Dara
Rolins	Rolinsa	k1gFnPc2	Rolinsa
(	(	kIx(	(
<g/>
prod	proda	k1gFnPc2	proda
<g/>
.	.	kIx.	.
</s>
<s>
DJ	DJ	kA	DJ
Mike	Mike	k1gFnSc1	Mike
Trafik	trafika	k1gFnPc2	trafika
<g/>
)	)	kIx)	)
Zelený	zelený	k2eAgInSc1d1	zelený
dym	dýmit	k5eAaImRp2nS	dýmit
feat	feat	k1gMnSc1	feat
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
D	D	kA	D
<g/>
,	,	kIx,	,
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
<g/>
,	,	kIx,	,
Dan	Dan	k1gMnSc1	Dan
Bárta	Bárta	k1gMnSc1	Bárta
<g/>
,	,	kIx,	,
Ektor	Ektor	k1gMnSc1	Ektor
(	(	kIx(	(
<g/>
prod	prod	k1gMnSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Gebod	Gebod	k1gInSc1	Gebod
<g/>
)	)	kIx)	)
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
mám	mít	k5eAaImIp1nS	mít
feat	feat	k5eAaPmF	feat
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
<g/>
,	,	kIx,	,
Zdenka	Zdenka	k1gFnSc1	Zdenka
Predná	Predný	k2eAgFnSc1d1	Predná
(	(	kIx(	(
<g/>
prod	prod	k1gMnSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
DJ	DJ	kA	DJ
Mike	Mike	k1gFnSc1	Mike
Trafik	trafika	k1gFnPc2	trafika
<g/>
)	)	kIx)	)
Orion	orion	k1gInSc1	orion
-	-	kIx~	-
Noční	noční	k2eAgNnPc1d1	noční
vidění	vidění	k1gNnPc1	vidění
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Nuly	nula	k1gFnSc2	nula
(	(	kIx(	(
<g/>
prod	prod	k1gMnSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Cuffia	Cuffia	k1gFnSc1	Cuffia
<g/>
)	)	kIx)	)
Č.	Č.	kA	Č.
<g/>
C.D.	C.D.	k1gFnSc1	C.D.
+	+	kIx~	+
Vladimir	Vladimir	k1gInSc1	Vladimir
518	[number]	k4	518
(	(	kIx(	(
<g/>
prod	proda	k1gFnPc2	proda
<g/>
.	.	kIx.	.
</s>
<s>
Dj	Dj	k?	Dj
Mike	Mike	k1gInSc1	Mike
Trafik	trafika	k1gFnPc2	trafika
<g/>
)	)	kIx)	)
Kurvy	kurva	k1gFnPc1	kurva
<g/>
,	,	kIx,	,
chlast	chlast	k1gInSc1	chlast
a	a	k8xC	a
chlebíčky	chlebíček	k1gInPc1	chlebíček
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Ondryk	Ondryk	k1gMnSc1	Ondryk
<g/>
)	)	kIx)	)
Já	já	k3xPp1nSc1	já
bych	by	kYmCp1nS	by
chtěl	chtít	k5eAaImAgMnS	chtít
mít	mít	k5eAaImF	mít
swag	swag	k1gMnSc1	swag
+	+	kIx~	+
Hugo	Hugo	k1gMnSc1	Hugo
Toxxx	Toxxx	k1gInSc1	Toxxx
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Dj	Dj	k?	Dj
Enemy	Enem	k1gMnPc7	Enem
<g/>
)	)	kIx)	)
Hodně	hodně	k6eAd1	hodně
lidí	člověk	k1gMnPc2	člověk
hodně	hodně	k6eAd1	hodně
mluví	mluvit	k5eAaImIp3nS	mluvit
(	(	kIx(	(
<g/>
prod	prod	k6eAd1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Dj	Dj	k?	Dj
Wich	Wich	k1gInSc1	Wich
<g/>
)	)	kIx)	)
Neni	Neni	k?	Neni
koho	kdo	k3yQnSc4	kdo
dissit	dissit	k5eAaImF	dissit
+	+	kIx~	+
Refew	Refew	k1gFnSc1	Refew
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Donie	Donie	k1gFnSc1	Donie
Darko	Darko	k1gNnSc4	Darko
<g/>
)	)	kIx)	)
Do	do	k7c2	do
snů	sen	k1gInPc2	sen
(	(	kIx(	(
<g/>
prod	proda	k1gFnPc2	proda
<g/>
.	.	kIx.	.
</s>
<s>
Frank	Frank	k1gMnSc1	Frank
Flames	Flames	k1gMnSc1	Flames
<g/>
)	)	kIx)	)
Všechny	všechen	k3xTgMnPc4	všechen
to	ten	k3xDgNnSc1	ten
chtěj	chtěj	k6eAd1	chtěj
+	+	kIx~	+
Smack	Smack	k1gMnSc1	Smack
(	(	kIx(	(
<g/>
prod	prod	k1gMnSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Shadow	Shadow	k?	Shadow
D	D	kA	D
<g/>
)	)	kIx)	)
Imrvere	Imrver	k1gMnSc5	Imrver
Liquere	Liquer	k1gMnSc5	Liquer
+	+	kIx~	+
Ektor	Ektor	k1gMnSc1	Ektor
(	(	kIx(	(
<g/>
prod	prod	k1gMnSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Abe	Abe	k?	Abe
<g/>
)	)	kIx)	)
Zkus	zkusit	k5eAaPmRp2nS	zkusit
chodit	chodit	k5eAaImF	chodit
v	v	k7c6	v
mejch	mejch	k?	mejch
botách	bota	k1gFnPc6	bota
(	(	kIx(	(
<g/>
prod	proda	k1gFnPc2	proda
<g/>
.	.	kIx.	.
</s>
<s>
Beatbustlers	Beatbustlers	k1gInSc1	Beatbustlers
<g/>
)	)	kIx)	)
Vesmírnej	Vesmírnej	k?	Vesmírnej
Odpad	odpad	k1gInSc1	odpad
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Donie	Donie	k1gFnSc1	Donie
Darko	Darko	k1gNnSc4	Darko
<g/>
)	)	kIx)	)
Egotrip	Egotrip	k1gInSc1	Egotrip
+	+	kIx~	+
James	James	k1gInSc1	James
Cole	cola	k1gFnSc3	cola
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Jointel	Jointelit	k5eAaPmRp2nS	Jointelit
<g/>
)	)	kIx)	)
Neuč	učit	k5eNaImRp2nS	učit
Vorla	Vorel	k1gMnSc4	Vorel
lítat	lítat	k5eAaImF	lítat
+	+	kIx~	+
Refew	Refew	k1gFnSc1	Refew
(	(	kIx(	(
<g/>
prod	prod	k1gInSc1	prod
<g/>
.	.	kIx.	.
</s>
<s>
Rack	Rack	k1gMnSc1	Rack
Rokas	Rokas	k1gMnSc1	Rokas
<g/>
)	)	kIx)	)
Klip	klip	k1gInSc1	klip
-	-	kIx~	-
Teritorium	teritorium	k1gNnSc1	teritorium
ze	z	k7c2	z
sólové	sólový	k2eAgFnSc2d1	sólová
desky	deska	k1gFnSc2	deska
Teritorium	teritorium	k1gNnSc1	teritorium
I	i	k8xC	i
Klip	klip	k1gInSc1	klip
-	-	kIx~	-
Chci	chtít	k5eAaImIp1nS	chtít
slyšet	slyšet	k5eAaImF	slyšet
svý	svý	k?	svý
jméno	jméno	k1gNnSc4	jméno
Satirický	satirický	k2eAgInSc1d1	satirický
klip	klip	k1gInSc1	klip
PSH	PSH	kA	PSH
-	-	kIx~	-
H.	H.	kA	H.
<g/>
P.	P.	kA	P.
<g/>
T.	T.	kA	T.
<g/>
N.	N.	kA	N.
Mixtape	Mixtap	k1gInSc5	Mixtap
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
vůle	vůle	k1gFnSc2	vůle
zboží	zboží	k1gNnSc2	zboží
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
na	na	k7c6	na
SAAMPL	SAAMPL	kA	SAAMPL
Článek	článek	k1gInSc4	článek
o	o	k7c6	o
klipu	klip	k1gInSc6	klip
k	k	k7c3	k
tracku	tracko	k1gNnSc3	tracko
Vesmírnej	Vesmírnej	k?	Vesmírnej
odpad	odpad	k1gInSc1	odpad
z	z	k7c2	z
mixtapu	mixtap	k1gInSc2	mixtap
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
vůle	vůle	k1gFnSc1	vůle
zboží	zboží	k1gNnSc2	zboží
na	na	k7c4	na
iDnes	iDnes	k1gInSc4	iDnes
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Album	album	k1gNnSc4	album
Noční	noční	k2eAgFnSc2d1	noční
vidění	vidění	k1gNnSc4	vidění
na	na	k7c4	na
SAAMPL	SAAMPL	kA	SAAMPL
</s>
