<p>
<s>
ProFIdivadlo	ProFIdivadlo	k1gNnSc1	ProFIdivadlo
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
Fakultu	fakulta	k1gFnSc4	fakulta
informatiky	informatika	k1gFnSc2	informatika
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
studentský	studentský	k2eAgInSc4d1	studentský
divadelní	divadelní	k2eAgInSc4d1	divadelní
soubor	soubor	k1gInSc4	soubor
studentů	student	k1gMnPc2	student
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
jako	jako	k8xC	jako
zájmová	zájmový	k2eAgFnSc1d1	zájmová
činnost	činnost	k1gFnSc1	činnost
studentů	student	k1gMnPc2	student
Fakulty	fakulta	k1gFnSc2	fakulta
informatiky	informatika	k1gFnSc2	informatika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
O	o	k7c6	o
ProFIdivadle	ProFIdivadlo	k1gNnSc6	ProFIdivadlo
==	==	k?	==
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
ProFIdivadlo	ProFIdivadlo	k1gNnSc1	ProFIdivadlo
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
fénix	fénix	k1gMnSc1	fénix
<g/>
:	:	kIx,	:
na	na	k7c6	na
konci	konec	k1gInSc6	konec
každého	každý	k3xTgInSc2	každý
roku	rok	k1gInSc2	rok
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
s	s	k7c7	s
přicházejícím	přicházející	k2eAgInSc7d1	přicházející
únorem	únor	k1gInSc7	únor
znovu	znovu	k6eAd1	znovu
povstalo	povstat	k5eAaPmAgNnS	povstat
z	z	k7c2	z
popela	popel	k1gInSc2	popel
<g/>
.	.	kIx.	.
</s>
<s>
Nabere	nabrat	k5eAaPmIp3nS	nabrat
nový	nový	k2eAgInSc4d1	nový
ansámbl	ansámbl	k1gInSc4	ansámbl
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
herce	herec	k1gMnPc4	herec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
co	co	k9	co
nejvíce	nejvíce	k6eAd1	nejvíce
studentíků	studentík	k1gMnPc2	studentík
mohlo	moct	k5eAaImAgNnS	moct
vychutnat	vychutnat	k5eAaPmF	vychutnat
roli	role	k1gFnSc4	role
celebrity	celebrita	k1gFnSc2	celebrita
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Specifikem	specifikon	k1gNnSc7	specifikon
Profidivadla	Profidivadlo	k1gNnSc2	Profidivadlo
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnPc1	jeho
členové	člen	k1gMnPc1	člen
se	se	k3xPyFc4	se
každý	každý	k3xTgInSc4	každý
akademický	akademický	k2eAgInSc4d1	akademický
rok	rok	k1gInSc4	rok
obměňují	obměňovat	k5eAaImIp3nP	obměňovat
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
zúčastní	zúčastnit	k5eAaPmIp3nP	zúčastnit
konkurzu	konkurz	k1gInSc3	konkurz
a	a	k8xC	a
zapíšou	zapsat	k5eAaPmIp3nP	zapsat
si	se	k3xPyFc3	se
celouniverzitně	celouniverzitně	k6eAd1	celouniverzitně
volitelný	volitelný	k2eAgInSc4d1	volitelný
předmět	předmět	k1gInSc4	předmět
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
(	(	kIx(	(
<g/>
kurz	kurz	k1gInSc1	kurz
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Konkurz	konkurz	k1gInSc1	konkurz
je	být	k5eAaImIp3nS	být
nejen	nejen	k6eAd1	nejen
výběrovým	výběrový	k2eAgNnSc7d1	výběrové
řízením	řízení	k1gNnSc7	řízení
herců	herec	k1gMnPc2	herec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
celého	celý	k2eAgInSc2d1	celý
divadelního	divadelní	k2eAgInSc2d1	divadelní
štábu	štáb	k1gInSc2	štáb
(	(	kIx(	(
<g/>
dramaturgie	dramaturgie	k1gFnSc1	dramaturgie
<g/>
,	,	kIx,	,
inspice	inspice	k1gFnSc1	inspice
<g/>
,	,	kIx,	,
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
osvětlovači	osvětlovač	k1gMnPc1	osvětlovač
<g/>
,	,	kIx,	,
projekce	projekce	k1gFnPc1	projekce
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
scény	scéna	k1gFnSc2	scéna
<g/>
,	,	kIx,	,
rekvizity	rekvizita	k1gFnSc2	rekvizita
<g/>
,	,	kIx,	,
kostýmy	kostým	k1gInPc1	kostým
<g/>
,	,	kIx,	,
nápověda	nápověda	k1gFnSc1	nápověda
<g/>
,	,	kIx,	,
design	design	k1gInSc1	design
<g/>
,	,	kIx,	,
propagace	propagace	k1gFnSc1	propagace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Studenti	student	k1gMnPc1	student
pak	pak	k6eAd1	pak
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
jednoho	jeden	k4xCgInSc2	jeden
semestru	semestr	k1gInSc2	semestr
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
starších	starý	k2eAgInPc2d2	starší
<g/>
,	,	kIx,	,
zkušenějších	zkušený	k2eAgInPc2d2	zkušenější
členů	člen	k1gInPc2	člen
Profidivadla	Profidivadlo	k1gNnSc2	Profidivadlo
nacvičují	nacvičovat	k5eAaImIp3nP	nacvičovat
vždy	vždy	k6eAd1	vždy
novou	nový	k2eAgFnSc4d1	nová
divadelní	divadelní	k2eAgFnSc4d1	divadelní
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
akademického	akademický	k2eAgInSc2d1	akademický
dne	den	k1gInSc2	den
univerzity	univerzita	k1gFnSc2	univerzita
–	–	k?	–
Dies	Dies	k1gInSc1	Dies
Academicus	Academicus	k1gInSc1	Academicus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
týdenním	týdenní	k2eAgInSc7d1	týdenní
odstupem	odstup	k1gInSc7	odstup
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
pravidelně	pravidelně	k6eAd1	pravidelně
koná	konat	k5eAaImIp3nS	konat
repríza	repríza	k1gFnSc1	repríza
a	a	k8xC	a
derniéra	derniéra	k1gFnSc1	derniéra
na	na	k7c6	na
brněnských	brněnský	k2eAgFnPc6d1	brněnská
divadelních	divadelní	k2eAgFnPc6d1	divadelní
scénách	scéna	k1gFnPc6	scéna
(	(	kIx(	(
<g/>
Centrum	centrum	k1gNnSc1	centrum
experimentálního	experimentální	k2eAgNnSc2d1	experimentální
divadla	divadlo	k1gNnSc2	divadlo
při	při	k7c6	při
Divadle	divadlo	k1gNnSc6	divadlo
Husa	husa	k1gFnSc1	husa
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgInSc1d1	divadelní
sál	sál	k1gInSc1	sál
kulturního	kulturní	k2eAgNnSc2d1	kulturní
centra	centrum	k1gNnSc2	centrum
Starý	starý	k2eAgInSc1d1	starý
pivovar	pivovar	k1gInSc1	pivovar
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgInSc1d1	divadelní
sál	sál	k1gInSc1	sál
klubu	klub	k1gInSc2	klub
Šelepova	Šelepův	k2eAgInSc2d1	Šelepův
<g/>
,	,	kIx,	,
scéna	scéna	k1gFnSc1	scéna
Buranteatru	Buranteatr	k1gInSc2	Buranteatr
v	v	k7c6	v
multikulturním	multikulturní	k2eAgNnSc6d1	multikulturní
centru	centrum	k1gNnSc6	centrum
Scala	scát	k5eAaImAgFnS	scát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
Univerzitního	univerzitní	k2eAgNnSc2d1	univerzitní
kina	kino	k1gNnSc2	kino
Scala	scát	k5eAaImAgFnS	scát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
reprízy	repríza	k1gFnPc1	repríza
a	a	k8xC	a
derniéry	derniéra	k1gFnPc1	derniéra
uskutečňují	uskutečňovat	k5eAaImIp3nP	uskutečňovat
tam	tam	k6eAd1	tam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
založení	založení	k1gNnSc2	založení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
uvedlo	uvést	k5eAaPmAgNnS	uvést
Profidivadlo	Profidivadlo	k1gNnSc1	Profidivadlo
celkem	celkem	k6eAd1	celkem
19	[number]	k4	19
premiér	premiéra	k1gFnPc2	premiéra
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
odehrané	odehraný	k2eAgFnPc4d1	odehraná
inscenace	inscenace	k1gFnPc4	inscenace
patří	patřit	k5eAaImIp3nS	patřit
díla	dílo	k1gNnPc4	dílo
Wiliama	Wiliam	k1gMnSc2	Wiliam
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
(	(	kIx(	(
<g/>
Hamlet	Hamlet	k1gMnSc1	Hamlet
<g/>
,	,	kIx,	,
kralevic	kralevic	k1gMnSc1	kralevic
dánský	dánský	k2eAgMnSc1d1	dánský
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
E.	E.	kA	E.
A.	A.	kA	A.
Saudka	Saudek	k1gMnSc2	Saudek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Friedricha	Friedrich	k1gMnSc2	Friedrich
Dürrenmatta	Dürrenmatt	k1gMnSc2	Dürrenmatt
(	(	kIx(	(
<g/>
Fyzikové	fyzik	k1gMnPc1	fyzik
<g/>
,	,	kIx,	,
Návštěva	návštěva	k1gFnSc1	návštěva
staré	starý	k2eAgFnSc2d1	stará
dámy	dáma	k1gFnSc2	dáma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Quentina	Quentin	k2eAgFnSc1d1	Quentina
Tarantina	Tarantina	k1gFnSc1	Tarantina
(	(	kIx(	(
<g/>
Pulp	pulpa	k1gFnPc2	pulpa
Fiction	Fiction	k1gInSc1	Fiction
–	–	k?	–
adaptace	adaptace	k1gFnSc2	adaptace
Roberta	Robert	k1gMnSc2	Robert
Krále	Král	k1gMnSc2	Král
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ludvíka	Ludvík	k1gMnSc2	Ludvík
Kundery	Kundera	k1gFnSc2	Kundera
(	(	kIx(	(
<g/>
hra	hra	k1gFnSc1	hra
Labyrint	labyrint	k1gInSc1	labyrint
světa	svět	k1gInSc2	svět
a	a	k8xC	a
lusthauz	lusthauz	k1gInSc1	lusthauz
srdce	srdce	k1gNnSc2	srdce
podle	podle	k7c2	podle
předlohy	předloha	k1gFnSc2	předloha
Jana	Jan	k1gMnSc2	Jan
Amose	Amos	k1gMnSc2	Amos
Komenského	Komenský	k1gMnSc2	Komenský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ivana	Ivan	k1gMnSc2	Ivan
Klímy	Klíma	k1gMnSc2	Klíma
(	(	kIx(	(
<g/>
Cukrárna	cukrárna	k1gFnSc1	cukrárna
Myriam	Myriam	k1gInSc1	Myriam
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Martina	Martin	k1gMnSc4	Martin
Hilského	Hilský	k1gMnSc4	Hilský
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
brněnského	brněnský	k2eAgMnSc2d1	brněnský
autora	autor	k1gMnSc2	autor
Mariana	Marian	k1gMnSc2	Marian
Pally	Palla	k1gMnSc2	Palla
(	(	kIx(	(
<g/>
Sajns	Sajns	k1gInSc1	Sajns
fikšn	fikšn	k1gInSc1	fikšn
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
repretoáru	repretoár	k1gInSc2	repretoár
Profidivadla	Profidivadlo	k1gNnSc2	Profidivadlo
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
původní	původní	k2eAgFnPc4d1	původní
hry	hra	k1gFnPc4	hra
<g/>
:	:	kIx,	:
například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
uvedlo	uvést	k5eAaPmAgNnS	uvést
grotesku	groteska	k1gFnSc4	groteska
Dilema	dilema	k1gNnSc1	dilema
mentálního	mentální	k2eAgInSc2d1	mentální
dálnopisu	dálnopis	k1gInSc2	dálnopis
od	od	k7c2	od
studenta	student	k1gMnSc2	student
informatiky	informatika	k1gFnSc2	informatika
Ondřeje	Ondřej	k1gMnSc2	Ondřej
S.	S.	kA	S.
Nečase	Nečas	k1gMnSc2	Nečas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
ProFIdivadlo	ProFIdivadlo	k1gNnSc1	ProFIdivadlo
nepoužívá	používat	k5eNaImIp3nS	používat
převzatou	převzatý	k2eAgFnSc4d1	převzatá
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
a	a	k8xC	a
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
z	z	k7c2	z
odehraných	odehraný	k2eAgNnPc2d1	odehrané
představení	představení	k1gNnPc2	představení
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
originální	originální	k2eAgFnSc1d1	originální
scénická	scénický	k2eAgFnSc1d1	scénická
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
složili	složit	k5eAaPmAgMnP	složit
členové	člen	k1gMnPc1	člen
Profidivadla	Profidivadlo	k1gNnSc2	Profidivadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
ProFIdivadla	ProFIdivadlo	k1gNnSc2	ProFIdivadlo
==	==	k?	==
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
PROFIDIVADLO	PROFIDIVADLO	kA	PROFIDIVADLO
na	na	k7c6	na
Fakultě	fakulta	k1gFnSc6	fakulta
informatiky	informatika	k1gFnSc2	informatika
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
počítačové	počítačový	k2eAgFnSc2d1	počítačová
vědy	věda	k1gFnSc2	věda
snad	snad	k9	snad
nejhezčím	hezký	k2eAgInSc7d3	nejhezčí
důkazem	důkaz	k1gInSc7	důkaz
bezespornosti	bezespornost	k1gFnSc2	bezespornost
tvrzení	tvrzení	k1gNnSc2	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
informatikové	informatik	k1gMnPc1	informatik
jsou	být	k5eAaImIp3nP	být
lidé	člověk	k1gMnPc1	člověk
tvůrčí	tvůrčí	k2eAgFnSc2d1	tvůrčí
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgFnSc2d1	kulturní
a	a	k8xC	a
veskrze	veskrze	k6eAd1	veskrze
mnohorozměrní	mnohorozměrný	k2eAgMnPc1d1	mnohorozměrný
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
ProFIdivadlo	ProFIdivadlo	k1gNnSc1	ProFIdivadlo
(	(	kIx(	(
<g/>
Divadlo	divadlo	k1gNnSc1	divadlo
pro	pro	k7c4	pro
Fakultu	fakulta	k1gFnSc4	fakulta
informatiky	informatika	k1gFnSc2	informatika
<g/>
)	)	kIx)	)
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
jako	jako	k8xS	jako
zájmová	zájmový	k2eAgFnSc1d1	zájmová
činnost	činnost	k1gFnSc1	činnost
studentů	student	k1gMnPc2	student
Fakulty	fakulta	k1gFnSc2	fakulta
informatiky	informatika	k1gFnSc2	informatika
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
improvizované	improvizovaný	k2eAgNnSc1d1	improvizované
představení	představení	k1gNnSc1	představení
(	(	kIx(	(
<g/>
divadelní	divadelní	k2eAgInSc1d1	divadelní
skeč	skeč	k1gInSc1	skeč
Lambda	lambda	k1gNnSc2	lambda
Karkulka	Karkulka	k1gFnSc1	Karkulka
<g/>
)	)	kIx)	)
odehrála	odehrát	k5eAaPmAgFnS	odehrát
skupina	skupina	k1gFnSc1	skupina
nadšenců	nadšenec	k1gMnPc2	nadšenec
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
spolužáka	spolužák	k1gMnSc2	spolužák
Roberta	Robert	k1gMnSc2	Robert
Krále	Král	k1gMnSc2	Král
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
Dies	Dies	k1gInSc4	Dies
Academicus	Academicus	k1gInSc1	Academicus
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
studenti	student	k1gMnPc1	student
informatiky	informatika	k1gFnSc2	informatika
také	také	k6eAd1	také
křtili	křtít	k5eAaImAgMnP	křtít
svůj	svůj	k3xOyFgInSc4	svůj
literární	literární	k2eAgInSc4d1	literární
sborník	sborník	k1gInSc4	sborník
Tak	tak	k9	tak
pište	psát	k5eAaImRp2nP	psát
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85799	[number]	k4	85799
<g/>
-	-	kIx~	-
<g/>
36	[number]	k4	36
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
předmluvu	předmluva	k1gFnSc4	předmluva
napsal	napsat	k5eAaPmAgMnS	napsat
spisovatel	spisovatel	k1gMnSc1	spisovatel
Egon	Egon	k1gMnSc1	Egon
Bondy	bond	k1gInPc7	bond
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
Profidivadlo	Profidivadlo	k1gNnSc1	Profidivadlo
stalo	stát	k5eAaPmAgNnS	stát
předmětem	předmět	k1gInSc7	předmět
celouniverzitně	celouniverzitně	k6eAd1	celouniverzitně
volitelného	volitelný	k2eAgInSc2d1	volitelný
předmětu	předmět	k1gInSc2	předmět
Divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
koncipováno	koncipován	k2eAgNnSc1d1	koncipováno
jako	jako	k8xC	jako
praktický	praktický	k2eAgInSc1d1	praktický
seminář	seminář	k1gInSc1	seminář
komunikačních	komunikační	k2eAgFnPc2d1	komunikační
dovedností	dovednost	k1gFnPc2	dovednost
<g/>
,	,	kIx,	,
kreativity	kreativita	k1gFnSc2	kreativita
a	a	k8xC	a
týmové	týmový	k2eAgFnSc2d1	týmová
spolupráce	spolupráce	k1gFnSc2	spolupráce
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Uskutečněná	uskutečněný	k2eAgNnPc1d1	uskutečněné
představení	představení	k1gNnPc1	představení
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
uvedl	uvést	k5eAaPmAgInS	uvést
divadelní	divadelní	k2eAgInSc1d1	divadelní
soubor	soubor	k1gInSc1	soubor
ProFIdivadla	ProFIdivadlo	k1gNnSc2	ProFIdivadlo
celkem	celkem	k6eAd1	celkem
19	[number]	k4	19
premiér	premiéra	k1gFnPc2	premiéra
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Oněgin	Oněgin	k2eAgInSc1d1	Oněgin
byl	být	k5eAaImAgInS	být
Rusák	rusák	k1gInSc1	rusák
/	/	kIx~	/
<g/>
2016	[number]	k4	2016
<g/>
/	/	kIx~	/
</s>
</p>
<p>
<s>
Amatéři	amatér	k1gMnPc1	amatér
/	/	kIx~	/
<g/>
2015	[number]	k4	2015
<g/>
/	/	kIx~	/
</s>
</p>
<p>
<s>
Přelet	přelet	k1gInSc1	přelet
nad	nad	k7c7	nad
kukaččím	kukaččí	k2eAgNnSc7d1	kukaččí
hnízdem	hnízdo	k1gNnSc7	hnízdo
/	/	kIx~	/
<g/>
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
</s>
</p>
<p>
<s>
Pán	pán	k1gMnSc1	pán
much	moucha	k1gFnPc2	moucha
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
</s>
</p>
<p>
<s>
Rychlé	Rychlé	k2eAgInPc4d1	Rychlé
šípy	šíp	k1gInPc4	šíp
na	na	k7c6	na
kolenou	koleno	k1gNnPc6	koleno
aneb	aneb	k?	aneb
Dlouhé	Dlouhé	k2eAgNnSc1d1	Dlouhé
Bidlo	bidlo	k1gNnSc1	bidlo
vítězí	vítězit	k5eAaImIp3nS	vítězit
/	/	kIx~	/
<g/>
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
</s>
</p>
<p>
<s>
Pulp	pulpa	k1gFnPc2	pulpa
Fiction	Fiction	k1gInSc1	Fiction
/	/	kIx~	/
<g/>
2011	[number]	k4	2011
<g/>
/	/	kIx~	/
</s>
</p>
<p>
<s>
Romeo	Romeo	k1gMnSc1	Romeo
a	a	k8xC	a
Julian	Julian	k1gMnSc1	Julian
/	/	kIx~	/
<g/>
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
</s>
</p>
<p>
<s>
Ptákovina	ptákovina	k1gFnSc1	ptákovina
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
<g/>
/	/	kIx~	/
-neveřejné	eveřejný	k2eAgNnSc1d1	-neveřejný
představení	představení	k1gNnSc1	představení
</s>
</p>
<p>
<s>
Cukrárna	cukrárna	k1gFnSc1	cukrárna
Myriam	Myriam	k1gInSc1	Myriam
/	/	kIx~	/
<g/>
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
</s>
</p>
<p>
<s>
Návštěva	návštěva	k1gFnSc1	návštěva
staré	starý	k2eAgFnSc2d1	stará
dámy	dáma	k1gFnSc2	dáma
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
<g/>
/	/	kIx~	/
</s>
</p>
<p>
<s>
Sajns	Sajns	k1gInSc1	Sajns
fikšn	fikšn	k1gInSc1	fikšn
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
</s>
</p>
<p>
<s>
Fyzikové	fyzik	k1gMnPc1	fyzik
/	/	kIx~	/
<g/>
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
</s>
</p>
<p>
<s>
Nálevna	nálevna	k1gFnSc1	nálevna
U	u	k7c2	u
děkana	děkan	k1gMnSc2	děkan
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
<g/>
/	/	kIx~	/
</s>
</p>
<p>
<s>
Hamlet	Hamlet	k1gMnSc1	Hamlet
<g/>
,	,	kIx,	,
králevic	králevic	k1gMnSc1	králevic
dánský	dánský	k2eAgMnSc1d1	dánský
/	/	kIx~	/
<g/>
2003	[number]	k4	2003
<g/>
/	/	kIx~	/
</s>
</p>
<p>
<s>
Dilema	dilema	k1gNnSc1	dilema
mentálního	mentální	k2eAgInSc2d1	mentální
dálnopisu	dálnopis	k1gInSc2	dálnopis
/	/	kIx~	/
<g/>
2002	[number]	k4	2002
<g/>
/	/	kIx~	/
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
konference	konference	k1gFnSc1	konference
Neofeministického	Neofeministický	k2eAgNnSc2d1	Neofeministický
sdružení	sdružení	k1gNnSc2	sdružení
FI	fi	k0	fi
MU	MU	kA	MU
/	/	kIx~	/
<g/>
2001	[number]	k4	2001
<g/>
/	/	kIx~	/
</s>
</p>
<p>
<s>
Labyrint	labyrint	k1gInSc1	labyrint
světa	svět	k1gInSc2	svět
a	a	k8xC	a
lusthauz	lusthauz	k1gInSc1	lusthauz
srdce	srdce	k1gNnSc2	srdce
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
<g/>
/	/	kIx~	/
</s>
</p>
<p>
<s>
Přelet	přelet	k1gInSc1	přelet
horkovzdušného	horkovzdušný	k2eAgInSc2d1	horkovzdušný
balónu	balón	k1gInSc2	balón
nad	nad	k7c7	nad
Fakultou	fakulta	k1gFnSc7	fakulta
informatiky	informatika	k1gFnSc2	informatika
/	/	kIx~	/
<g/>
1999	[number]	k4	1999
<g/>
/	/	kIx~	/
</s>
</p>
<p>
<s>
Lambda	lambda	k1gNnSc1	lambda
Kalkulka	Kalkulka	k1gFnSc1	Kalkulka
/	/	kIx~	/
<g/>
1998	[number]	k4	1998
<g/>
/	/	kIx~	/
</s>
</p>
<p>
<s>
==	==	k?	==
Režie	režie	k1gFnSc1	režie
==	==	k?	==
</s>
</p>
<p>
<s>
Režie	režie	k1gFnSc1	režie
přináleží	přináležet	k5eAaImIp3nS	přináležet
vždy	vždy	k6eAd1	vždy
starším	starý	k2eAgMnPc3d2	starší
a	a	k8xC	a
zkušeným	zkušený	k2eAgMnPc3d1	zkušený
členům	člen	k1gMnPc3	člen
ProFIdivadla	ProFIdivadlo	k1gNnPc4	ProFIdivadlo
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
režisérem	režisér	k1gMnSc7	režisér
byl	být	k5eAaImAgMnS	být
student	student	k1gMnSc1	student
Robert	Robert	k1gMnSc1	Robert
Král	Král	k1gMnSc1	Král
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
po	po	k7c6	po
absolvovaní	absolvovaný	k2eAgMnPc1d1	absolvovaný
Fakulty	fakulta	k1gFnSc2	fakulta
informatiky	informatik	k1gMnPc7	informatik
a	a	k8xC	a
získání	získání	k1gNnSc3	získání
doktorátu	doktorát	k1gInSc2	doktorát
z	z	k7c2	z
informatiky	informatika	k1gFnSc2	informatika
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
také	také	k9	také
režii	režie	k1gFnSc4	režie
na	na	k7c4	na
FAMU	FAMU	kA	FAMU
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
režiséry	režisér	k1gMnPc7	režisér
Profidivadla	Profidivadlo	k1gNnSc2	Profidivadlo
byli	být	k5eAaImAgMnP	být
například	například	k6eAd1	například
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Filipec	Filipec	k1gMnSc1	Filipec
a	a	k8xC	a
Lukáš	Lukáš	k1gMnSc1	Lukáš
Junga	Jung	k1gMnSc2	Jung
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
Pavel	Pavel	k1gMnSc1	Pavel
Havelka	Havelka	k1gMnSc1	Havelka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hudba	hudba	k1gFnSc1	hudba
==	==	k?	==
</s>
</p>
<p>
<s>
Profidivadlo	Profidivadlo	k1gNnSc1	Profidivadlo
v	v	k7c6	v
nastudovaných	nastudovaný	k2eAgFnPc6d1	nastudovaná
inscenacích	inscenace	k1gFnPc6	inscenace
nepoužívá	používat	k5eNaImIp3nS	používat
převzaté	převzatý	k2eAgFnPc4d1	převzatá
skladby	skladba	k1gFnPc4	skladba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
vlastní	vlastní	k2eAgFnSc4d1	vlastní
scénickou	scénický	k2eAgFnSc4d1	scénická
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
hudební	hudební	k2eAgFnSc7d1	hudební
skladatele	skladatel	k1gMnSc2	skladatel
ProFIdivadla	ProFIdivadlo	k1gNnSc2	ProFIdivadlo
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Piňos	Piňos	k1gMnSc1	Piňos
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
popři	popřít	k5eAaPmRp2nS	popřít
studiu	studio	k1gNnSc6	studio
informatiky	informatika	k1gFnSc2	informatika
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
i	i	k9	i
skladbu	skladba	k1gFnSc4	skladba
na	na	k7c6	na
Hudební	hudební	k2eAgFnSc6d1	hudební
fakultě	fakulta	k1gFnSc6	fakulta
JAMU	jam	k1gInSc2	jam
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Pavel	Pavel	k1gMnSc1	Pavel
Hamřík	Hamřík	k1gMnSc1	Hamřík
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
programování	programování	k1gNnSc4	programování
a	a	k8xC	a
hudební	hudební	k2eAgFnSc4d1	hudební
kompozici	kompozice	k1gFnSc4	kompozice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Prokeš	Prokeš	k1gMnSc1	Prokeš
(	(	kIx(	(
<g/>
dramaturgie	dramaturgie	k1gFnSc1	dramaturgie
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
vysokoškolský	vysokoškolský	k2eAgMnSc1d1	vysokoškolský
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
kulturně-občanského	kulturněbčanský	k2eAgNnSc2d1	kulturně-občanský
sdružení	sdružení	k1gNnSc2	sdružení
Napříč	napříč	k6eAd1	napříč
<g/>
,	,	kIx,	,
členem	člen	k1gInSc7	člen
Obce	obec	k1gFnSc2	obec
spisovatelů	spisovatel	k1gMnPc2	spisovatel
a	a	k8xC	a
Ochranného	ochranný	k2eAgInSc2d1	ochranný
svazu	svaz	k1gInSc2	svaz
autorského	autorský	k2eAgInSc2d1	autorský
pro	pro	k7c4	pro
práva	právo	k1gNnPc4	právo
k	k	k7c3	k
dílům	dílo	k1gNnPc3	dílo
hudebním	hudební	k2eAgNnPc3d1	hudební
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
založil	založit	k5eAaPmAgInS	založit
a	a	k8xC	a
vede	vést	k5eAaImIp3nS	vést
ProFIdivadlo	ProFIdivadlo	k1gNnSc1	ProFIdivadlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
roli	role	k1gFnSc6	role
dramaturga	dramaturg	k1gMnSc4	dramaturg
a	a	k8xC	a
režiséra	režisér	k1gMnSc4	režisér
<g/>
.	.	kIx.	.
</s>
<s>
Věnuje	věnovat	k5eAaImIp3nS	věnovat
se	se	k3xPyFc4	se
vlastní	vlastní	k2eAgFnSc3d1	vlastní
literární	literární	k2eAgFnSc3d1	literární
tvorbě	tvorba	k1gFnSc3	tvorba
(	(	kIx(	(
<g/>
sbírka	sbírka	k1gFnSc1	sbírka
povídek	povídka	k1gFnPc2	povídka
Na	na	k7c6	na
sviňu	sviňu	k?	sviňu
svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
díla	dílo	k1gNnSc2	dílo
Nečítankové	čítankový	k2eNgNnSc1d1	čítankový
dětství	dětství	k1gNnSc1	dětství
<g/>
,	,	kIx,	,
Nečítankové	čítankový	k2eNgNnSc1d1	čítankový
dospívání	dospívání	k1gNnSc1	dospívání
<g/>
,	,	kIx,	,
Nebýt	být	k5eNaImF	být
stádem	stádo	k1gNnSc7	stádo
Hamletů	Hamlet	k1gMnPc2	Hamlet
a	a	k8xC	a
román	román	k1gInSc4	román
Sestup	sestup	k1gInSc1	sestup
do	do	k7c2	do
základního	základní	k2eAgInSc2d1	základní
tábora	tábor	k1gInSc2	tábor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Robert	Robert	k1gMnSc1	Robert
Král	Král	k1gMnSc1	Král
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Založil	založit	k5eAaPmAgMnS	založit
studentské	studentský	k2eAgNnSc4d1	studentské
divadelní	divadelní	k2eAgNnSc4d1	divadelní
sdružení	sdružení	k1gNnSc4	sdružení
Kandrdas	kandrdas	k1gMnSc1	kandrdas
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
informatiky	informatika	k1gFnSc2	informatika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
věnuje	věnovat	k5eAaImIp3nS	věnovat
se	se	k3xPyFc4	se
také	také	k9	také
psaní	psaní	k1gNnSc2	psaní
a	a	k8xC	a
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
filmové	filmový	k2eAgFnSc3d1	filmová
a	a	k8xC	a
scenáristické	scenáristický	k2eAgFnSc3d1	scenáristická
tvorbě	tvorba	k1gFnSc3	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
filmy	film	k1gInPc1	film
byli	být	k5eAaImAgMnP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
na	na	k7c6	na
festivalech	festival	k1gInPc6	festival
FAMU	FAMU	kA	FAMU
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
Ostrava	Ostrava	k1gFnSc1	Ostrava
Picture	Pictur	k1gInSc5	Pictur
<g/>
.	.	kIx.	.
</s>
<s>
Režíroval	režírovat	k5eAaImAgMnS	režírovat
několik	několik	k4yIc4	několik
her	hra	k1gFnPc2	hra
ProFIdivadla	ProFIdivadlo	k1gNnSc2	ProFIdivadlo
<g/>
:	:	kIx,	:
Sajns	Sajns	k1gInSc1	Sajns
Fikšn	Fikšn	k1gNnSc1	Fikšn
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dilema	dilema	k1gNnSc1	dilema
mentálního	mentální	k2eAgInSc2d1	mentální
dálnopisu	dálnopis	k1gInSc2	dálnopis
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Labyrint	labyrint	k1gInSc1	labyrint
světa	svět	k1gInSc2	svět
a	a	k8xC	a
lusthauz	lusthauz	k1gInSc1	lusthauz
srdce	srdce	k1gNnSc2	srdce
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Filipec	Filipec	k1gMnSc1	Filipec
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
ProFIdivadla	ProFIdivadlo	k1gNnSc2	ProFIdivadlo
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
učitelství	učitelství	k1gNnSc4	učitelství
matematiky	matematika	k1gFnSc2	matematika
a	a	k8xC	a
informatiky	informatika	k1gFnSc2	informatika
na	na	k7c6	na
Přírodovědecké	přírodovědecký	k2eAgFnSc6d1	Přírodovědecká
fakultě	fakulta	k1gFnSc6	fakulta
a	a	k8xC	a
Fakultě	fakulta	k1gFnSc6	fakulta
informatiky	informatika	k1gFnSc2	informatika
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Specializuje	specializovat	k5eAaBmIp3nS	specializovat
se	se	k3xPyFc4	se
na	na	k7c4	na
tvorbu	tvorba	k1gFnSc4	tvorba
webových	webový	k2eAgFnPc2d1	webová
prezentací	prezentace	k1gFnPc2	prezentace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
a	a	k8xC	a
členem	člen	k1gMnSc7	člen
kulturně	kulturně	k6eAd1	kulturně
občanského	občanský	k2eAgNnSc2d1	občanské
sdružení	sdružení	k1gNnSc2	sdružení
Napříč	napříč	k6eAd1	napříč
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ProFIdivadlo	ProFIdivadlo	k1gNnSc4	ProFIdivadlo
nastudoval	nastudovat	k5eAaBmAgMnS	nastudovat
následující	následující	k2eAgFnPc4d1	následující
hry	hra	k1gFnPc4	hra
<g/>
:	:	kIx,	:
Romeo	Romeo	k1gMnSc1	Romeo
a	a	k8xC	a
Julian	Julian	k1gMnSc1	Julian
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ptákovina	ptákovina	k1gFnSc1	ptákovina
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Cukrárna	cukrárna	k1gFnSc1	cukrárna
Myriam	Myriam	k1gInSc1	Myriam
(	(	kIx(	(
<g/>
asistent	asistent	k1gMnSc1	asistent
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Návštěva	návštěva	k1gFnSc1	návštěva
staré	starý	k2eAgFnSc2d1	stará
dámy	dáma	k1gFnSc2	dáma
(	(	kIx(	(
<g/>
role	role	k1gFnSc1	role
<g />
.	.	kIx.	.
</s>
<s>
staré	starý	k2eAgFnPc4d1	stará
dámy	dáma	k1gFnPc4	dáma
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sajns	Sajns	k1gInSc1	Sajns
fikšn	fikšn	k1gInSc1	fikšn
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
zvuk	zvuk	k1gInSc1	zvuk
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fyzikové	fyzik	k1gMnPc5	fyzik
(	(	kIx(	(
<g/>
role	role	k1gFnSc1	role
Möbia	Möbia	k1gFnSc1	Möbia
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nálevna	nálevna	k1gFnSc1	nálevna
U	u	k7c2	u
Děkana	děkan	k1gMnSc2	děkan
(	(	kIx(	(
<g/>
role	role	k1gFnSc1	role
neo	neo	k?	neo
Hamleta	Hamlet	k1gMnSc2	Hamlet
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hamlet	Hamlet	k1gMnSc1	Hamlet
<g/>
,	,	kIx,	,
králevic	králevic	k1gMnSc1	králevic
dánský	dánský	k2eAgMnSc1d1	dánský
(	(	kIx(	(
<g/>
role	role	k1gFnSc1	role
Hamleta	Hamlet	k1gMnSc2	Hamlet
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dilema	dilema	k1gNnSc1	dilema
mentálního	mentální	k2eAgInSc2d1	mentální
dálnopisu	dálnopis	k1gInSc2	dálnopis
(	(	kIx(	(
<g/>
role	role	k1gFnSc2	role
dr	dr	kA	dr
<g/>
.	.	kIx.	.
Housky	houska	k1gFnSc2	houska
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lukáš	Lukáš	k1gMnSc1	Lukáš
Junga	Jung	k1gMnSc2	Jung
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
ProFidivadla	ProFidivadlo	k1gNnSc2	ProFidivadlo
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
analytik	analytik	k1gMnSc1	analytik
a	a	k8xC	a
CRM	CRM	kA	CRM
specialista	specialista	k1gMnSc1	specialista
<g/>
.	.	kIx.	.
</s>
<s>
Režíroval	režírovat	k5eAaImAgMnS	režírovat
hry	hra	k1gFnSc2	hra
Romeo	Romeo	k1gMnSc1	Romeo
a	a	k8xC	a
Julián	Julián	k1gMnSc1	Julián
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Cukrárna	cukrárna	k1gFnSc1	cukrárna
Myriam	Myriam	k1gInSc1	Myriam
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Návštěva	návštěva	k1gFnSc1	návštěva
staré	starý	k2eAgFnSc2d1	stará
dámy	dáma	k1gFnSc2	dáma
(	(	kIx(	(
<g/>
asistent	asistent	k1gMnSc1	asistent
režie	režie	k1gFnSc1	režie
a	a	k8xC	a
role	role	k1gFnSc1	role
Jana	Jan	k1gMnSc2	Jan
Černého	Černý	k1gMnSc2	Černý
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgInS	hrát
v	v	k7c6	v
inscenacích	inscenace	k1gFnPc6	inscenace
Sajns	Sajnsa	k1gFnPc2	Sajnsa
fikšn	fikšn	k1gInSc1	fikšn
(	(	kIx(	(
<g/>
role	role	k1gFnSc1	role
kapitána	kapitán	k1gMnSc2	kapitán
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fyzikové	fyzik	k1gMnPc5	fyzik
(	(	kIx(	(
<g/>
role	role	k1gFnSc1	role
kriminálního	kriminální	k2eAgMnSc2d1	kriminální
inspektora	inspektor	k1gMnSc2	inspektor
Richarda	Richard	k1gMnSc2	Richard
Vosse	Voss	k1gMnSc2	Voss
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nálevna	nálevna	k1gFnSc1	nálevna
U	u	k7c2	u
děkana	děkan	k1gMnSc2	děkan
(	(	kIx(	(
<g/>
životní	životní	k2eAgFnSc2d1	životní
role	role	k1gFnSc2	role
Pamětníka	pamětník	k1gInSc2	pamětník
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
a	a	k8xC	a
Hamlet	Hamlet	k1gMnSc1	Hamlet
<g/>
,	,	kIx,	,
králevic	králevic	k1gMnSc1	králevic
dánský	dánský	k2eAgMnSc1d1	dánský
(	(	kIx(	(
<g/>
role	role	k1gFnSc1	role
Laerta	Laerta	k1gFnSc1	Laerta
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Havelka	Havelka	k1gMnSc1	Havelka
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
ProFIdivadla	ProFIdivadlo	k1gNnSc2	ProFIdivadlo
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
Obecnou	obecný	k2eAgFnSc4d1	obecná
teorii	teorie	k1gFnSc4	teorie
a	a	k8xC	a
dějiny	dějiny	k1gFnPc4	dějiny
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
Teorii	teorie	k1gFnSc4	teorie
interaktivních	interaktivní	k2eAgNnPc2d1	interaktivní
médií	médium	k1gNnPc2	médium
na	na	k7c6	na
Masarykově	Masarykův	k2eAgFnSc6d1	Masarykova
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Režisérsky	režisérsky	k6eAd1	režisérsky
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
hrách	hra	k1gFnPc6	hra
<g/>
:	:	kIx,	:
Oněgin	Oněgina	k1gFnPc2	Oněgina
byl	být	k5eAaImAgInS	být
Rusák	rusák	k1gInSc1	rusák
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Amatéři	amatér	k1gMnPc1	amatér
(	(	kIx(	(
<g/>
hudební	hudební	k2eAgFnSc1d1	hudební
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Přelet	přelet	k1gInSc1	přelet
nad	nad	k7c7	nad
kukaččím	kukaččí	k2eAgNnSc7d1	kukaččí
hnízdem	hnízdo	k1gNnSc7	hnízdo
(	(	kIx(	(
<g/>
asistent	asistent	k1gMnSc1	asistent
režie	režie	k1gFnSc1	režie
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
