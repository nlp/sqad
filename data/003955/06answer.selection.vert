<s>
Upír	upír	k1gMnSc1	upír
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
vampýr	vampýr	k1gMnSc1	vampýr
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mytologická	mytologický	k2eAgFnSc1d1	mytologická
bytost	bytost	k1gFnSc1	bytost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
životní	životní	k2eAgFnSc7d1	životní
esencí	esence	k1gFnSc7	esence
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
krve	krev	k1gFnSc2	krev
<g/>
)	)	kIx)	)
živých	živá	k1gFnPc2	živá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jakmile	jakmile	k8xS	jakmile
začne	začít	k5eAaPmIp3nS	začít
pít	pít	k5eAaImF	pít
krev	krev	k1gFnSc4	krev
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
přestat	přestat	k5eAaPmF	přestat
<g/>
.	.	kIx.	.
</s>
