<s>
Upír	upír	k1gMnSc1	upír
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
vampýr	vampýr	k1gMnSc1	vampýr
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
mytologická	mytologický	k2eAgFnSc1d1	mytologická
bytost	bytost	k1gFnSc1	bytost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
životní	životní	k2eAgFnSc7d1	životní
esencí	esence	k1gFnSc7	esence
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
krve	krev	k1gFnSc2	krev
<g/>
)	)	kIx)	)
živých	živá	k1gFnPc2	živá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jakmile	jakmile	k8xS	jakmile
začne	začít	k5eAaPmIp3nS	začít
pít	pít	k5eAaImF	pít
krev	krev	k1gFnSc4	krev
<g/>
,	,	kIx,	,
nemůže	moct	k5eNaImIp3nS	moct
přestat	přestat	k5eAaPmF	přestat
<g/>
.	.	kIx.	.
</s>
<s>
Přestat	přestat	k5eAaPmF	přestat
pít	pít	k5eAaImF	pít
umí	umět	k5eAaImIp3nS	umět
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
silný	silný	k2eAgInSc1d1	silný
a	a	k8xC	a
odhodlaný	odhodlaný	k2eAgMnSc1d1	odhodlaný
upír	upír	k1gMnSc1	upír
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
upír	upír	k1gMnSc1	upír
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
slovanského	slovanský	k2eAgInSc2d1	slovanský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
slovanských	slovanský	k2eAgInPc6d1	slovanský
jazycích	jazyk	k1gInPc6	jazyk
-	-	kIx~	-
bulharsky	bulharsky	k6eAd1	bulharsky
je	být	k5eAaImIp3nS	být
в	в	k?	в
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
a	a	k8xC	a
slovensky	slovensky	k6eAd1	slovensky
upír	upír	k1gMnSc1	upír
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
obměna	obměna	k1gFnSc1	obměna
vampýr	vampýr	k1gMnSc1	vampýr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polsky	polsky	k6eAd1	polsky
wampir	wampir	k1gInSc1	wampir
a	a	k8xC	a
upiór	upiór	k1gInSc1	upiór
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
у	у	k?	у
<g/>
,	,	kIx,	,
bělorusky	bělorusky	k6eAd1	bělorusky
у	у	k?	у
<g/>
,	,	kIx,	,
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
у	у	k?	у
a	a	k8xC	a
в	в	k?	в
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
praslovanštině	praslovanština	k1gFnSc6	praslovanština
bylo	být	k5eAaImAgNnS	být
*	*	kIx~	*
<g/>
ǫ	ǫ	k?	ǫ
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
dále	daleko	k6eAd2	daleko
nejasnou	jasný	k2eNgFnSc4d1	nejasná
etymologii	etymologie	k1gFnSc4	etymologie
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
vědci	vědec	k1gMnPc1	vědec
slovo	slovo	k1gNnSc4	slovo
dále	daleko	k6eAd2	daleko
spojují	spojovat	k5eAaImIp3nP	spojovat
s	s	k7c7	s
turkickým	turkický	k2eAgNnSc7d1	turkické
označením	označení	k1gNnSc7	označení
čarodějnice	čarodějnice	k1gFnSc2	čarodějnice
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
např.	např.	kA	např.
v	v	k7c6	v
tatarštině	tatarština	k1gFnSc6	tatarština
zní	znět	k5eAaImIp3nS	znět
ubyr	ubyr	k1gInSc4	ubyr
<g/>
.	.	kIx.	.
</s>
<s>
Etymolog	etymolog	k1gMnSc1	etymolog
Machek	Machek	k1gMnSc1	Machek
spojuje	spojovat	k5eAaImIp3nS	spojovat
slovo	slovo	k1gNnSc4	slovo
s	s	k7c7	s
nářečním	nářeční	k2eAgNnSc7d1	nářeční
slovenským	slovenský	k2eAgNnSc7d1	slovenské
slovesem	sloveso	k1gNnSc7	sloveso
vrepiť	vrepiť	k1gFnSc1	vrepiť
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
jeho	jeho	k3xOp3gMnPc4	jeho
hypotetickou	hypotetický	k2eAgFnSc7d1	hypotetická
přesmyčkovou	přesmyčkův	k2eAgFnSc7d1	přesmyčkův
podobou	podoba	k1gFnSc7	podoba
*	*	kIx~	*
<g/>
vъ	vъ	k?	vъ
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
překládá	překládat	k5eAaImIp3nS	překládat
jako	jako	k8xC	jako
vkousnout	vkousnout	k5eAaPmF	vkousnout
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
vsát	vsát	k5eAaPmF	vsát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
vchytit	vchytit	k5eAaPmF	vchytit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
vrazit	vrazit	k5eAaPmF	vrazit
se	se	k3xPyFc4	se
(	(	kIx(	(
<g/>
i	i	k9	i
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
ostatně	ostatně	k6eAd1	ostatně
existuje	existovat	k5eAaImIp3nS	existovat
archaické	archaický	k2eAgNnSc4d1	archaické
sloveso	sloveso	k1gNnSc4	sloveso
vpeřit	vpeřit	k5eAaPmF	vpeřit
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
prudce	prudko	k6eAd1	prudko
něco	něco	k3yInSc4	něco
někam	někam	k6eAd1	někam
vrazit	vrazit	k5eAaPmF	vrazit
<g/>
,	,	kIx,	,
vtisknout	vtisknout	k5eAaPmF	vtisknout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Machek	Machek	k6eAd1	Machek
také	také	k9	také
uvádí	uvádět	k5eAaImIp3nS	uvádět
alternativní	alternativní	k2eAgFnSc4d1	alternativní
etymologii	etymologie	k1gFnSc4	etymologie
francouzského	francouzský	k2eAgMnSc2d1	francouzský
slavisty	slavista	k1gMnSc2	slavista
Vaillanta	Vaillant	k1gMnSc2	Vaillant
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
u-pir	uir	k1gMnSc1	u-pir
překládá	překládat	k5eAaImIp3nS	překládat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
uniká	unikat	k5eAaImIp3nS	unikat
(	(	kIx(	(
<g/>
z	z	k7c2	z
hrobu	hrob	k1gInSc2	hrob
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
doložené	doložený	k2eAgNnSc1d1	doložené
použití	použití	k1gNnSc1	použití
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
staroruském	staroruský	k2eAgInSc6d1	staroruský
kolofónu	kolofón	k1gInSc6	kolofón
žaltáři	žaltář	k1gInSc6	žaltář
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
podepsán	podepsat	k5eAaPmNgInS	podepsat
žertovnou	žertovný	k2eAgFnSc7d1	žertovná
přezdívkou	přezdívka	k1gFnSc7	přezdívka
У	У	k?	У
Л	Л	k?	Л
(	(	kIx(	(
<g/>
Špatný	špatný	k2eAgMnSc1d1	špatný
upír	upír	k1gMnSc1	upír
<g/>
)	)	kIx)	)
a	a	k8xC	a
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
datován	datovat	k5eAaImNgInS	datovat
rokem	rok	k1gInSc7	rok
1047	[number]	k4	1047
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
jako	jako	k9	jako
první	první	k4xOgNnSc4	první
použití	použití	k1gNnSc4	použití
uvádí	uvádět	k5eAaImIp3nS	uvádět
ruský	ruský	k2eAgInSc1d1	ruský
protipohanský	protipohanský	k2eAgInSc1d1	protipohanský
traktát	traktát	k1gInSc1	traktát
Slovo	slovo	k1gNnSc1	slovo
svatého	svatý	k2eAgMnSc2d1	svatý
Grigorije	Grigorije	k1gMnSc2	Grigorije
(	(	kIx(	(
<g/>
С	С	k?	С
С	С	k?	С
Г	Г	k?	Г
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pohané	pohan	k1gMnPc1	pohan
uctívají	uctívat	k5eAaImIp3nP	uctívat
upíry	upír	k1gMnPc7	upír
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
spisek	spisek	k1gInSc1	spisek
pochází	pocházet	k5eAaImIp3nS	pocházet
patrně	patrně	k6eAd1	patrně
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
-	-	kIx~	-
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Obměna	obměna	k1gFnSc1	obměna
vampýr	vampýr	k1gMnSc1	vampýr
<g/>
,	,	kIx,	,
používaná	používaný	k2eAgFnSc1d1	používaná
často	často	k6eAd1	často
v	v	k7c6	v
neslovanských	slovanský	k2eNgInPc6d1	neslovanský
jazycích	jazyk	k1gInPc6	jazyk
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
vampire	vampir	k1gMnSc5	vampir
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
vampyre	vampyr	k1gMnSc5	vampyr
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Vampir	Vampir	k1gInSc1	Vampir
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
z	z	k7c2	z
nasalizované	nasalizovaný	k2eAgFnSc2d1	nasalizovaný
srbské	srbský	k2eAgFnSc2d1	Srbská
varianty	varianta	k1gFnSc2	varianta
в	в	k?	в
/	/	kIx~	/
vampir	vampir	k1gMnSc1	vampir
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
bylo	být	k5eAaImAgNnS	být
patrně	patrně	k6eAd1	patrně
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
německé	německý	k2eAgFnPc1d1	německá
Vampir	Vampir	k1gInSc4	Vampir
a	a	k8xC	a
z	z	k7c2	z
němčiny	němčina	k1gFnSc2	němčina
se	se	k3xPyFc4	se
slovo	slovo	k1gNnSc1	slovo
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
dostalo	dostat	k5eAaPmAgNnS	dostat
jinou	jiný	k2eAgFnSc7d1	jiná
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
původní	původní	k2eAgInSc1d1	původní
jazykový	jazykový	k2eAgInSc1d1	jazykový
základ	základ	k1gInSc1	základ
je	být	k5eAaImIp3nS	být
bezpochyby	bezpochyby	k6eAd1	bezpochyby
stejný	stejný	k2eAgInSc1d1	stejný
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
s	s	k7c7	s
tvrdým	tvrdé	k1gNnSc7	tvrdé
y	y	k?	y
-	-	kIx~	-
možná	možná	k9	možná
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
slova	slovo	k1gNnSc2	slovo
netopýr	netopýr	k1gMnSc1	netopýr
<g/>
.	.	kIx.	.
</s>
<s>
Postava	postava	k1gFnSc1	postava
upíra	upír	k1gMnSc2	upír
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mytologii	mytologie	k1gFnSc6	mytologie
jak	jak	k8xS	jak
východních	východní	k2eAgInPc6d1	východní
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k8xC	i
západních	západní	k2eAgInPc2d1	západní
a	a	k8xC	a
jižních	jižní	k2eAgInPc2d1	jižní
Slovanů	Slovan	k1gInPc2	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
písemných	písemný	k2eAgInPc6d1	písemný
pramenech	pramen	k1gInPc6	pramen
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pod	pod	k7c7	pod
různými	různý	k2eAgInPc7d1	různý
názvy	název	k1gInPc7	název
<g/>
:	:	kIx,	:
u	u	k7c2	u
Bulharů	Bulhar	k1gMnPc2	Bulhar
<g/>
:	:	kIx,	:
vapir	vapir	k1gMnSc1	vapir
<g/>
,	,	kIx,	,
vipir	vipir	k1gMnSc1	vipir
<g/>
,	,	kIx,	,
vъ	vъ	k?	vъ
<g/>
,	,	kIx,	,
vrapir	vrapir	k1gInSc1	vrapir
<g/>
,	,	kIx,	,
vrapirin	vrapirin	k1gInSc1	vrapirin
<g/>
,	,	kIx,	,
voper	voper	k1gMnSc1	voper
<g/>
,	,	kIx,	,
drakus	drakus	k1gMnSc1	drakus
<g/>
,	,	kIx,	,
žin	žin	k?	žin
<g/>
,	,	kIx,	,
ustrel	ustrela	k1gFnPc2	ustrela
<g/>
,	,	kIx,	,
lepir	lepira	k1gFnPc2	lepira
<g/>
,	,	kIx,	,
vlepir	vlepira	k1gFnPc2	vlepira
<g/>
,	,	kIx,	,
liter	litera	k1gFnPc2	litera
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
lemptir	lemptir	k1gMnSc1	lemptir
<g/>
,	,	kIx,	,
tenec	tenec	k1gMnSc1	tenec
<g/>
,	,	kIx,	,
grobnik	grobnik	k1gMnSc1	grobnik
<g/>
,	,	kIx,	,
brodnik	brodnik	k1gMnSc1	brodnik
<g/>
,	,	kIx,	,
plъ	plъ	k?	plъ
<g/>
,	,	kIx,	,
upir	upir	k1gInSc1	upir
<g/>
,	,	kIx,	,
samodiv	samodit	k5eAaPmDgInS	samodit
u	u	k7c2	u
Srbů	Srb	k1gMnPc2	Srb
<g/>
:	:	kIx,	:
vampir	vampir	k1gMnSc1	vampir
<g/>
,	,	kIx,	,
vukodlak	vukodlak	k1gMnSc1	vukodlak
<g/>
,	,	kIx,	,
vaper	vaper	k1gMnSc1	vaper
<g/>
,	,	kIx,	,
lampir	lampir	k1gMnSc1	lampir
<g/>
,	,	kIx,	,
lapir	lapir	k1gMnSc1	lapir
<g/>
,	,	kIx,	,
lempijer	lempijer	k1gMnSc1	lempijer
<g/>
,	,	kIx,	,
vuk	vuk	k?	vuk
<g/>
,	,	kIx,	,
vjegodonja	vjegodonja	k1gMnSc1	vjegodonja
<g/>
,	,	kIx,	,
jegodonja	jegodonja	k1gMnSc1	jegodonja
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
tenac	tenac	k6eAd1	tenac
u	u	k7c2	u
východních	východní	k2eAgInPc2d1	východní
Slovanů	Slovan	k1gInPc2	Slovan
<g/>
:	:	kIx,	:
rusky	rusky	k6eAd1	rusky
upir	upir	k1gInSc4	upir
<g/>
,	,	kIx,	,
oboroteň	oboroteň	k1gFnSc4	oboroteň
(	(	kIx(	(
<g/>
tento	tento	k3xDgInSc4	tento
výraz	výraz	k1gInSc4	výraz
označuje	označovat	k5eAaImIp3nS	označovat
jakéhokoli	jakýkoli	k3yIgMnSc2	jakýkoli
lykantropa	lykantrop	k1gMnSc2	lykantrop
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krovosos	krovosos	k1gInSc1	krovosos
<g/>
,	,	kIx,	,
bělorusky	bělorusky	k6eAd1	bělorusky
vupor	vupor	k1gInSc1	vupor
<g/>
,	,	kIx,	,
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
upir	upir	k1gMnSc1	upir
Staročeské	staročeský	k2eAgNnSc4d1	staročeské
označení	označení	k1gNnSc4	označení
upíra	upír	k1gMnSc2	upír
morous	morous	k1gMnSc1	morous
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
počátku	počátek	k1gInSc6	počátek
byl	být	k5eAaImAgMnS	být
upír	upír	k1gMnSc1	upír
blízký	blízký	k2eAgInSc1d1	blízký
noční	noční	k2eAgFnSc3d1	noční
můře	můra	k1gFnSc3	můra
<g/>
,	,	kIx,	,
zlý	zlý	k2eAgInSc4d1	zlý
přízrak	přízrak	k1gInSc4	přízrak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
sužuje	sužovat	k5eAaImIp3nS	sužovat
spáče	spáč	k1gMnPc4	spáč
a	a	k8xC	a
v	v	k7c4	v
noci	noc	k1gFnPc4	noc
jim	on	k3xPp3gMnPc3	on
odnáší	odnášet	k5eAaImIp3nS	odnášet
jejich	jejich	k3xOp3gFnSc4	jejich
životní	životní	k2eAgFnSc4d1	životní
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
mytologii	mytologie	k1gFnSc6	mytologie
Slovanů	Slovan	k1gMnPc2	Slovan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k6eAd1	též
Germánů	Germán	k1gMnPc2	Germán
se	se	k3xPyFc4	se
upírem	upír	k1gMnSc7	upír
stával	stávat	k5eAaImAgMnS	stávat
člověk	člověk	k1gMnSc1	člověk
zemřelý	zemřelý	k1gMnSc1	zemřelý
nečistým	čistý	k2eNgInSc7d1	nečistý
způsobem	způsob	k1gInSc7	způsob
(	(	kIx(	(
<g/>
sebevrah	sebevrah	k1gMnSc1	sebevrah
<g/>
,	,	kIx,	,
popravený	popravený	k2eAgMnSc1d1	popravený
zločinec	zločinec	k1gMnSc1	zločinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
vůbec	vůbec	k9	vůbec
osoba	osoba	k1gFnSc1	osoba
vylučující	vylučující	k2eAgFnSc1d1	vylučující
se	se	k3xPyFc4	se
ze	z	k7c2	z
společnosti	společnost	k1gFnSc2	společnost
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
skonavší	skonavší	k2eAgFnSc7d1	skonavší
nepřirozenou	přirozený	k2eNgFnSc7d1	nepřirozená
smrtí	smrt	k1gFnSc7	smrt
(	(	kIx(	(
<g/>
čaroděj	čaroděj	k1gMnSc1	čaroděj
užívající	užívající	k2eAgFnSc2d1	užívající
černé	černý	k2eAgFnSc2d1	černá
magie	magie	k1gFnSc2	magie
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
oběť	oběť	k1gFnSc1	oběť
čaroděje	čaroděj	k1gMnSc2	čaroděj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Upírem	upír	k1gMnSc7	upír
bylo	být	k5eAaImAgNnS	být
také	také	k9	také
dítě	dítě	k1gNnSc1	dítě
čarodějnice	čarodějnice	k1gFnSc2	čarodějnice
<g/>
,	,	kIx,	,
zplozené	zplozený	k2eAgFnSc2d1	zplozená
po	po	k7c6	po
koitu	koitus	k1gInSc6	koitus
s	s	k7c7	s
démonem	démon	k1gMnSc7	démon
<g/>
.	.	kIx.	.
</s>
<s>
Někde	někde	k6eAd1	někde
se	se	k3xPyFc4	se
věřilo	věřit	k5eAaImAgNnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mrtvola	mrtvola	k1gFnSc1	mrtvola
změnila	změnit	k5eAaPmAgFnS	změnit
v	v	k7c4	v
upíra	upír	k1gMnSc4	upír
postačí	postačit	k5eAaPmIp3nS	postačit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
přes	přes	k7c4	přes
ni	on	k3xPp3gFnSc4	on
přeskočil	přeskočit	k5eAaPmAgMnS	přeskočit
vzteklý	vzteklý	k2eAgMnSc1d1	vzteklý
pes	pes	k1gMnSc1	pes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dobách	doba	k1gFnPc6	doba
po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
křesťanství	křesťanství	k1gNnSc2	křesťanství
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
těla	tělo	k1gNnPc1	tělo
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
přestala	přestat	k5eAaPmAgFnS	přestat
spalovat	spalovat	k5eAaImF	spalovat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pohřbívat	pohřbívat	k5eAaImF	pohřbívat
staletí	staletí	k1gNnPc2	staletí
ověřenými	ověřený	k2eAgInPc7d1	ověřený
způsoby	způsob	k1gInPc7	způsob
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
také	také	k9	také
začalo	začít	k5eAaPmAgNnS	začít
věřit	věřit	k5eAaImF	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
upírem	upír	k1gMnSc7	upír
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
stát	stát	k5eAaImF	stát
osoba	osoba	k1gFnSc1	osoba
pohřbená	pohřbený	k2eAgFnSc1d1	pohřbená
bez	bez	k7c2	bez
obřadů	obřad	k1gInPc2	obřad
(	(	kIx(	(
<g/>
např.	např.	kA	např.
nepokřtěné	pokřtěný	k2eNgNnSc1d1	nepokřtěné
dítě	dítě	k1gNnSc1	dítě
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pokud	pokud	k8xS	pokud
byly	být	k5eAaImAgInP	být
tyto	tento	k3xDgInPc1	tento
obřady	obřad	k1gInPc1	obřad
provedeny	provést	k5eAaPmNgInP	provést
špatně	špatně	k6eAd1	špatně
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
osoba	osoba	k1gFnSc1	osoba
narozená	narozený	k2eAgFnSc1d1	narozená
v	v	k7c4	v
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
o	o	k7c6	o
Vánocích	Vánoce	k1gFnPc6	Vánoce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
srbských	srbský	k2eAgFnPc2d1	Srbská
tradic	tradice	k1gFnPc2	tradice
se	se	k3xPyFc4	se
upírem	upír	k1gMnSc7	upír
mohl	moct	k5eAaImAgMnS	moct
stát	stát	k5eAaImF	stát
i	i	k9	i
živý	živý	k2eAgMnSc1d1	živý
člověk	člověk	k1gMnSc1	člověk
s	s	k7c7	s
vrozenými	vrozený	k2eAgInPc7d1	vrozený
předpoklady	předpoklad	k1gInPc7	předpoklad
<g/>
.	.	kIx.	.
</s>
<s>
Upír	upír	k1gMnSc1	upír
měl	mít	k5eAaImAgMnS	mít
podobu	podoba	k1gFnSc4	podoba
člověka	člověk	k1gMnSc2	člověk
bez	bez	k7c2	bez
kostí	kost	k1gFnPc2	kost
nebo	nebo	k8xC	nebo
i	i	k9	i
bez	bez	k7c2	bez
nosu	nos	k1gInSc2	nos
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgInS	moct
mít	mít	k5eAaImF	mít
hnědé	hnědý	k2eAgNnSc4d1	hnědé
nebo	nebo	k8xC	nebo
červené	červený	k2eAgNnSc4d1	červené
oči	oko	k1gNnPc4	oko
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
hladovění	hladovění	k1gNnSc2	hladovění
měli	mít	k5eAaImAgMnP	mít
upíři	upír	k1gMnPc1	upír
oči	oko	k1gNnPc4	oko
černé	černá	k1gFnSc2	černá
<g/>
)	)	kIx)	)
a	a	k8xC	a
kovové	kovový	k2eAgInPc1d1	kovový
zuby	zub	k1gInPc1	zub
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
srstí	srst	k1gFnSc7	srst
(	(	kIx(	(
<g/>
jako	jako	k9	jako
u	u	k7c2	u
vlkodlaka	vlkodlak	k1gMnSc2	vlkodlak
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
východních	východní	k2eAgInPc2d1	východní
Slovanů	Slovan	k1gInPc2	Slovan
mohl	moct	k5eAaImAgMnS	moct
mít	mít	k5eAaImF	mít
zmijí	zmijí	k2eAgInSc4d1	zmijí
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
slepý	slepý	k2eAgMnSc1d1	slepý
nebo	nebo	k8xC	nebo
hluchý	hluchý	k2eAgMnSc1d1	hluchý
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Bulharů	Bulhar	k1gMnPc2	Bulhar
nabíral	nabírat	k5eAaImAgMnS	nabírat
někdy	někdy	k6eAd1	někdy
i	i	k9	i
zvířecí	zvířecí	k2eAgFnSc4d1	zvířecí
podobu	podoba	k1gFnSc4	podoba
<g/>
:	:	kIx,	:
rysa	rys	k1gMnSc2	rys
<g/>
,	,	kIx,	,
vlka	vlk	k1gMnSc2	vlk
<g/>
,	,	kIx,	,
myši	myš	k1gFnSc2	myš
<g/>
,	,	kIx,	,
zmije	zmije	k1gFnSc2	zmije
<g/>
,	,	kIx,	,
kozy	koza	k1gFnSc2	koza
nebo	nebo	k8xC	nebo
bílého	bílý	k2eAgMnSc4d1	bílý
koně	kůň	k1gMnSc4	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
byla	být	k5eAaImAgFnS	být
upírem	upír	k1gMnSc7	upír
(	(	kIx(	(
<g/>
potažmo	potažmo	k6eAd1	potažmo
upírkou	upírka	k1gFnSc7	upírka
<g/>
)	)	kIx)	)
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
vynikala	vynikat	k5eAaImAgFnS	vynikat
nad	nad	k7c4	nad
jiné	jiný	k2eAgNnSc4d1	jiné
svou	svůj	k3xOyFgFnSc7	svůj
neobyčejnou	obyčejný	k2eNgFnSc7d1	neobyčejná
krásou	krása	k1gFnSc7	krása
a	a	k8xC	a
výjimečností	výjimečnost	k1gFnSc7	výjimečnost
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
tvář	tvář	k1gFnSc1	tvář
byla	být	k5eAaImAgFnS	být
mrtvolně	mrtvolně	k6eAd1	mrtvolně
bledá	bledý	k2eAgFnSc1d1	bledá
<g/>
,	,	kIx,	,
vlasy	vlas	k1gInPc1	vlas
byly	být	k5eAaImAgInP	být
černé	černá	k1gFnPc4	černá
jako	jako	k8xC	jako
havraní	havraní	k2eAgNnPc4d1	havraní
křídla	křídlo	k1gNnPc4	křídlo
<g/>
,	,	kIx,	,
rty	ret	k1gInPc1	ret
rudé	rudý	k2eAgInPc1d1	rudý
jako	jako	k8xC	jako
čerstvá	čerstvý	k2eAgFnSc1d1	čerstvá
krev	krev	k1gFnSc1	krev
a	a	k8xC	a
při	při	k7c6	při
pohledu	pohled	k1gInSc6	pohled
do	do	k7c2	do
jejích	její	k3xOp3gNnPc2	její
tmavých	tmavý	k2eAgNnPc2d1	tmavé
(	(	kIx(	(
<g/>
či	či	k8xC	či
rudých	rudý	k2eAgInPc2d1	rudý
<g/>
)	)	kIx)	)
očí	oko	k1gNnPc2	oko
jsme	být	k5eAaImIp1nP	být
mohli	moct	k5eAaImAgMnP	moct
vidět	vidět	k5eAaImF	vidět
celou	celý	k2eAgFnSc4d1	celá
panenku	panenka	k1gFnSc4	panenka
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
okolní	okolní	k2eAgInSc4d1	okolní
lid	lid	k1gInSc4	lid
byl	být	k5eAaImAgMnS	být
neodhalený	odhalený	k2eNgMnSc1d1	neodhalený
upír	upír	k1gMnSc1	upír
postavou	postava	k1gFnSc7	postava
záhadnou	záhadný	k2eAgFnSc7d1	záhadná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
svému	své	k1gNnSc3	své
"	"	kIx"	"
<g/>
charismatu	charisma	k1gNnSc3	charisma
<g/>
"	"	kIx"	"
a	a	k8xC	a
atraktivitě	atraktivita	k1gFnSc3	atraktivita
velmi	velmi	k6eAd1	velmi
vyhledávanou	vyhledávaný	k2eAgFnSc4d1	vyhledávaná
<g/>
.	.	kIx.	.
</s>
<s>
Pověstné	pověstný	k2eAgFnPc1d1	pověstná
jsou	být	k5eAaImIp3nP	být
upírovy	upírův	k2eAgFnPc1d1	upírova
silné	silný	k2eAgFnPc1d1	silná
sexuální	sexuální	k2eAgFnPc1d1	sexuální
tužby	tužba	k1gFnPc1	tužba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
uspokojoval	uspokojovat	k5eAaImAgMnS	uspokojovat
<g/>
/	/	kIx~	/
<g/>
a	a	k8xC	a
se	s	k7c7	s
smrtelnými	smrtelný	k2eAgMnPc7d1	smrtelný
lidmi	člověk	k1gMnPc7	člověk
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Inkubus	Inkubus	k1gInSc1	Inkubus
a	a	k8xC	a
Sukuba	Sukuba	k1gFnSc1	Sukuba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Upíři	upír	k1gMnPc1	upír
podle	podle	k7c2	podle
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
legend	legenda	k1gFnPc2	legenda
<g/>
,	,	kIx,	,
knih	kniha	k1gFnPc2	kniha
a	a	k8xC	a
povídání	povídání	k1gNnSc1	povídání
vypadají	vypadat	k5eAaPmIp3nP	vypadat
jako	jako	k9	jako
starší	starý	k2eAgMnPc1d2	starší
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
obrovskými	obrovský	k2eAgInPc7d1	obrovský
špičáky	špičák	k1gInPc7	špičák
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
hodně	hodně	k6eAd1	hodně
nebezpeční	bezpečný	k2eNgMnPc1d1	nebezpečný
a	a	k8xC	a
drží	držet	k5eAaImIp3nP	držet
se	se	k3xPyFc4	se
naživu	naživu	k6eAd1	naživu
kousáním	kousání	k1gNnSc7	kousání
lidí	člověk	k1gMnPc2	člověk
do	do	k7c2	do
krků	krk	k1gInPc2	krk
a	a	k8xC	a
sáním	sání	k1gNnSc7	sání
jejich	jejich	k3xOp3gFnSc2	jejich
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
procesu	proces	k1gInSc6	proces
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
taktéž	taktéž	k?	taktéž
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c4	v
upíry	upír	k1gMnPc4	upír
či	či	k8xC	či
rovnou	rovnou	k6eAd1	rovnou
umírají	umírat	k5eAaImIp3nP	umírat
<g/>
.	.	kIx.	.
</s>
<s>
Upíři	upír	k1gMnPc1	upír
se	se	k3xPyFc4	se
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
možnostech	možnost	k1gFnPc6	možnost
silně	silně	k6eAd1	silně
lišili	lišit	k5eAaImAgMnP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
působnost	působnost	k1gFnSc1	působnost
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
omezena	omezit	k5eAaPmNgFnS	omezit
jen	jen	k9	jen
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
noční	noční	k2eAgFnSc4d1	noční
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
strašili	strašit	k5eAaImAgMnP	strašit
své	svůj	k3xOyFgInPc4	svůj
blízké	blízký	k2eAgInPc4d1	blízký
<g/>
,	,	kIx,	,
škodili	škodit	k5eAaImAgMnP	škodit
domácím	domácí	k2eAgNnPc3d1	domácí
zvířatům	zvíře	k1gNnPc3	zvíře
(	(	kIx(	(
<g/>
pili	pít	k5eAaImAgMnP	pít
krev	krev	k1gFnSc1	krev
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ráno	ráno	k6eAd1	ráno
znovu	znovu	k6eAd1	znovu
odcházeli	odcházet	k5eAaImAgMnP	odcházet
do	do	k7c2	do
hrobů	hrob	k1gInPc2	hrob
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
kdyby	kdyby	kYmCp3nS	kdyby
je	on	k3xPp3gMnPc4	on
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
sluneční	sluneční	k2eAgInSc1d1	sluneční
paprsek	paprsek	k1gInSc1	paprsek
<g/>
,	,	kIx,	,
rozprskli	rozprsknout	k5eAaPmAgMnP	rozprsknout
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
a	a	k8xC	a
zůstala	zůstat	k5eAaPmAgFnS	zůstat
by	by	kYmCp3nS	by
po	po	k7c6	po
nich	on	k3xPp3gFnPc6	on
jen	jen	k9	jen
krvavá	krvavý	k2eAgFnSc1d1	krvavá
skvrna	skvrna	k1gFnSc1	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgInSc1	první
příchod	příchod	k1gInSc1	příchod
upíra	upír	k1gMnSc2	upír
do	do	k7c2	do
vesnice	vesnice	k1gFnSc2	vesnice
ucítili	ucítit	k5eAaPmAgMnP	ucítit
psi	pes	k1gMnPc1	pes
(	(	kIx(	(
<g/>
začali	začít	k5eAaPmAgMnP	začít
štěkat	štěkat	k5eAaImF	štěkat
<g/>
)	)	kIx)	)
a	a	k8xC	a
dobytek	dobytek	k1gInSc4	dobytek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
plašit	plašit	k5eAaImF	plašit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
ve	v	k7c6	v
slovanské	slovanský	k2eAgFnSc6d1	Slovanská
mytologii	mytologie	k1gFnSc6	mytologie
mohl	moct	k5eAaImAgMnS	moct
upír	upír	k1gMnSc1	upír
uzavřít	uzavřít	k5eAaPmF	uzavřít
sňatek	sňatek	k1gInSc4	sňatek
s	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
mít	mít	k5eAaImF	mít
i	i	k9	i
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Upír	upír	k1gMnSc1	upír
neumírá	umírat	k5eNaImIp3nS	umírat
stářím	stáří	k1gNnSc7	stáří
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
jej	on	k3xPp3gInSc4	on
zahubit	zahubit	k5eAaPmF	zahubit
<g/>
.	.	kIx.	.
</s>
<s>
Mohl	moct	k5eAaImAgMnS	moct
shořet	shořet	k5eAaPmF	shořet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
napil	napít	k5eAaBmAgInS	napít
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
lépe	dobře	k6eAd2	dobře
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
lidskou	lidský	k2eAgFnSc7d1	lidská
krví	krev	k1gFnSc7	krev
<g/>
,	,	kIx,	,
krev	krev	k1gFnSc4	krev
zvířat	zvíře	k1gNnPc2	zvíře
sice	sice	k8xC	sice
rovněž	rovněž	k9	rovněž
poslouží	posloužit	k5eAaPmIp3nS	posloužit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oslabuje	oslabovat	k5eAaImIp3nS	oslabovat
ho	on	k3xPp3gNnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
se	se	k3xPyFc4	se
upíři	upír	k1gMnPc1	upír
neomezovali	omezovat	k5eNaImAgMnP	omezovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
působnosti	působnost	k1gFnSc6	působnost
jen	jen	k9	jen
na	na	k7c4	na
noční	noční	k2eAgFnSc4d1	noční
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Věřilo	věřit	k5eAaImAgNnS	věřit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
žijí	žít	k5eAaImIp3nP	žít
mezi	mezi	k7c7	mezi
normálními	normální	k2eAgMnPc7d1	normální
lidmi	člověk	k1gMnPc7	člověk
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
jako	jako	k8xS	jako
obchodníci	obchodník	k1gMnPc1	obchodník
nebo	nebo	k8xC	nebo
řezníci	řezník	k1gMnPc1	řezník
<g/>
)	)	kIx)	)
a	a	k8xC	a
do	do	k7c2	do
hrobu	hrob	k1gInSc2	hrob
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
uložit	uložit	k5eAaPmF	uložit
jen	jen	k9	jen
v	v	k7c4	v
určitý	určitý	k2eAgInSc4d1	určitý
den	den	k1gInSc4	den
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
.	.	kIx.	.
</s>
<s>
Někde	někde	k6eAd1	někde
se	se	k3xPyFc4	se
věřilo	věřit	k5eAaImAgNnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlivem	vliv	k1gInSc7	vliv
upírů	upír	k1gMnPc2	upír
přichází	přicházet	k5eAaImIp3nS	přicházet
hlad	hlad	k1gInSc1	hlad
<g/>
,	,	kIx,	,
špatné	špatný	k2eAgNnSc1d1	špatné
počasí	počasí	k1gNnSc1	počasí
nebo	nebo	k8xC	nebo
mor	mor	k1gInSc1	mor
<g/>
.	.	kIx.	.
</s>
<s>
Víra	víra	k1gFnSc1	víra
v	v	k7c4	v
revenanty	revenanta	k1gFnPc4	revenanta
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
doložena	doložit	k5eAaPmNgFnS	doložit
z	z	k7c2	z
písemných	písemný	k2eAgInPc2d1	písemný
pramenů	pramen	k1gInPc2	pramen
a	a	k8xC	a
etnografických	etnografický	k2eAgNnPc2d1	etnografické
pozorování	pozorování	k1gNnPc2	pozorování
<g/>
,	,	kIx,	,
se	s	k7c7	s
specifickým	specifický	k2eAgInSc7d1	specifický
způsobem	způsob	k1gInSc7	způsob
projevuje	projevovat	k5eAaImIp3nS	projevovat
i	i	k9	i
v	v	k7c6	v
archeologických	archeologický	k2eAgInPc6d1	archeologický
nálezech	nález	k1gInPc6	nález
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
obrana	obrana	k1gFnSc1	obrana
proti	proti	k7c3	proti
upírům	upír	k1gMnPc3	upír
existovalo	existovat	k5eAaImAgNnS	existovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
jsou	být	k5eAaImIp3nP	být
archeologicky	archeologicky	k6eAd1	archeologicky
doložitelné	doložitelný	k2eAgFnPc1d1	doložitelná
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
měla	mít	k5eAaImAgFnS	mít
mrtvému	mrtvý	k1gMnSc3	mrtvý
fyzicky	fyzicky	k6eAd1	fyzicky
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
opouštěl	opouštět	k5eAaImAgInS	opouštět
hrob	hrob	k1gInSc1	hrob
<g/>
:	:	kIx,	:
mrtvý	mrtvý	k1gMnSc1	mrtvý
byl	být	k5eAaImAgMnS	být
proboden	probodnout	k5eAaPmNgMnS	probodnout
kůlem	kůl	k1gInSc7	kůl
tělo	tělo	k1gNnSc4	tělo
mrtvého	mrtvý	k1gMnSc2	mrtvý
bylo	být	k5eAaImAgNnS	být
dodatečně	dodatečně	k6eAd1	dodatečně
vyhrabáno	vyhrabán	k2eAgNnSc1d1	vyhrabáno
a	a	k8xC	a
spáleno	spálen	k2eAgNnSc1d1	spáleno
mrtvola	mrtvola	k1gFnSc1	mrtvola
se	se	k3xPyFc4	se
zabalila	zabalit	k5eAaPmAgFnS	zabalit
do	do	k7c2	do
rybářské	rybářský	k2eAgFnSc2d1	rybářská
sítě	síť	k1gFnSc2	síť
mrtvý	mrtvý	k1gMnSc1	mrtvý
byl	být	k5eAaImAgMnS	být
obrácen	obrátit	k5eAaPmNgMnS	obrátit
na	na	k7c4	na
břicho	břicho	k1gNnSc4	břicho
nebo	nebo	k8xC	nebo
ležel	ležet	k5eAaImAgMnS	ležet
ve	v	k7c6	v
skrčené	skrčený	k2eAgFnSc6d1	skrčená
poloze	poloha	k1gFnSc6	poloha
mrtvý	mrtvý	k1gMnSc1	mrtvý
byl	být	k5eAaImAgMnS	být
zavalen	zavalit	k5eAaPmNgMnS	zavalit
kameny	kámen	k1gInPc7	kámen
mrtvému	mrtvý	k1gMnSc3	mrtvý
byla	být	k5eAaImAgNnP	být
zacpána	zacpán	k2eAgNnPc1d1	zacpáno
ústa	ústa	k1gNnPc1	ústa
např.	např.	kA	např.
<g />
.	.	kIx.	.
</s>
<s>
kameny	kámen	k1gInPc7	kámen
<g/>
,	,	kIx,	,
železem	železo	k1gNnSc7	železo
<g/>
,	,	kIx,	,
hlínou	hlína	k1gFnSc7	hlína
tělo	tělo	k1gNnSc4	tělo
mrtvého	mrtvý	k1gMnSc2	mrtvý
bylo	být	k5eAaImAgNnS	být
násilně	násilně	k6eAd1	násilně
porušeno	porušit	k5eAaPmNgNnS	porušit
(	(	kIx(	(
<g/>
useknuta	useknout	k5eAaPmNgFnS	useknout
hlava	hlava	k1gFnSc1	hlava
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
useknutá	useknutý	k2eAgFnSc1d1	useknutá
hlava	hlava	k1gFnSc1	hlava
mrtvého	mrtvý	k1gMnSc2	mrtvý
byla	být	k5eAaImAgFnS	být
uložena	uložit	k5eAaPmNgFnS	uložit
do	do	k7c2	do
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
hrobu	hrob	k1gInSc2	hrob
do	do	k7c2	do
hrobu	hrob	k1gInSc2	hrob
se	se	k3xPyFc4	se
střílelo	střílet	k5eAaImAgNnS	střílet
šípy	šíp	k1gInPc4	šíp
na	na	k7c6	na
hrobech	hrob	k1gInPc6	hrob
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
byly	být	k5eAaImAgFnP	být
zažíhány	zažíhat	k5eAaImNgInP	zažíhat
ohně	oheň	k1gInPc1	oheň
mrtvému	mrtvý	k1gMnSc3	mrtvý
byl	být	k5eAaImAgInS	být
sypán	sypán	k2eAgInSc1d1	sypán
do	do	k7c2	do
úst	ústa	k1gNnPc2	ústa
mák	mák	k1gInSc1	mák
Některá	některý	k3yIgNnPc1	některý
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
protivampirických	protivampirický	k2eAgNnPc2d1	protivampirický
opatření	opatření	k1gNnPc2	opatření
byla	být	k5eAaImAgFnS	být
doložena	doložit	k5eAaPmNgFnS	doložit
např.	např.	kA	např.
na	na	k7c4	na
raně	raně	k6eAd1	raně
středověkém	středověký	k2eAgNnSc6d1	středověké
pohřebišti	pohřebiště	k1gNnSc6	pohřebiště
v	v	k7c6	v
Lahovicích	Lahovice	k1gFnPc6	Lahovice
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
zkoumaném	zkoumaný	k2eAgMnSc6d1	zkoumaný
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1955-1960	[number]	k4	1955-1960
archeoložkou	archeoložka	k1gFnSc7	archeoložka
Zdeňkou	Zdeňka	k1gFnSc7	Zdeňka
Krumphanzlovou	Krumphanzlová	k1gFnSc7	Krumphanzlová
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
obtížné	obtížný	k2eAgNnSc1d1	obtížné
od	od	k7c2	od
sebe	se	k3xPyFc2	se
odlišit	odlišit	k5eAaPmF	odlišit
protivampirická	protivampirický	k2eAgNnPc4d1	protivampirický
opatření	opatření	k1gNnPc4	opatření
a	a	k8xC	a
např.	např.	kA	např.
zvlášť	zvlášť	k6eAd1	zvlášť
krutý	krutý	k2eAgInSc1d1	krutý
způsob	způsob	k1gInSc1	způsob
popravy	poprava	k1gFnSc2	poprava
nebo	nebo	k8xC	nebo
posmrtného	posmrtný	k2eAgInSc2d1	posmrtný
trestu	trest	k1gInSc2	trest
(	(	kIx(	(
<g/>
o	o	k7c6	o
důsledcích	důsledek	k1gInPc6	důsledek
činnosti	činnost	k1gFnSc2	činnost
vykradačů	vykradač	k1gMnPc2	vykradač
hrobů	hrob	k1gInPc2	hrob
nemluvě	nemluva	k1gFnSc3	nemluva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
takový	takový	k3xDgInSc1	takový
nález	nález	k1gInSc1	nález
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
nutné	nutný	k2eAgNnSc1d1	nutné
správně	správně	k6eAd1	správně
posoudit	posoudit	k5eAaPmF	posoudit
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
dobové	dobový	k2eAgFnPc4d1	dobová
a	a	k8xC	a
místní	místní	k2eAgFnPc4d1	místní
zvyklosti	zvyklost	k1gFnPc4	zvyklost
(	(	kIx(	(
<g/>
ať	ať	k9	ať
už	už	k6eAd1	už
pohřební	pohřební	k2eAgFnPc1d1	pohřební
nebo	nebo	k8xC	nebo
hrdelně-právní	hrdelněrávní	k2eAgFnPc1d1	hrdelně-právní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
písemných	písemný	k2eAgInPc2d1	písemný
popisů	popis	k1gInPc2	popis
řádění	řádění	k1gNnSc2	řádění
upíra	upír	k1gMnSc2	upír
podává	podávat	k5eAaImIp3nS	podávat
islandská	islandský	k2eAgFnSc1d1	islandská
Sága	sága	k1gFnSc1	sága
o	o	k7c6	o
Eriku	Erik	k1gMnSc6	Erik
Rudém	rudý	k1gMnSc6	rudý
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
zachycující	zachycující	k2eAgFnSc6d1	zachycující
historii	historie	k1gFnSc6	historie
osídlování	osídlování	k1gNnSc2	osídlování
Grónska	Grónsko	k1gNnSc2	Grónsko
křesťanskými	křesťanský	k2eAgMnPc7d1	křesťanský
Vikingy	Viking	k1gMnPc7	Viking
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1000	[number]	k4	1000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
epizody	epizoda	k1gFnSc2	epizoda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
během	během	k7c2	během
severské	severský	k2eAgFnSc2d1	severská
zimy	zima	k1gFnSc2	zima
<g/>
,	,	kIx,	,
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
po	po	k7c6	po
náhlé	náhlý	k2eAgFnSc6d1	náhlá
smrti	smrt	k1gFnSc6	smrt
jednoho	jeden	k4xCgMnSc2	jeden
neoblíbeného	oblíbený	k2eNgMnSc2d1	neoblíbený
muže	muž	k1gMnSc2	muž
začnou	začít	k5eAaPmIp3nP	začít
podivně	podivně	k6eAd1	podivně
umírat	umírat	k5eAaImF	umírat
i	i	k9	i
další	další	k2eAgMnPc1d1	další
členové	člen	k1gMnPc1	člen
jeho	jeho	k3xOp3gFnSc2	jeho
komunity	komunita	k1gFnSc2	komunita
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Erikova	Erikův	k2eAgMnSc2d1	Erikův
syna	syn	k1gMnSc2	syn
Thorsteina	Thorstein	k1gMnSc2	Thorstein
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
osazenstvo	osazenstvo	k1gNnSc4	osazenstvo
odloučené	odloučený	k2eAgFnSc2d1	odloučená
usedlosti	usedlost	k1gFnSc2	usedlost
vedl	vést	k5eAaImAgInS	vést
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
mrtvých	mrtvý	k1gMnPc2	mrtvý
(	(	kIx(	(
<g/>
i	i	k9	i
už	už	k6eAd1	už
pohřbených	pohřbený	k2eAgInPc2d1	pohřbený
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
znovu	znovu	k6eAd1	znovu
objevují	objevovat	k5eAaImIp3nP	objevovat
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nP	snažit
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
blízkými	blízký	k2eAgMnPc7d1	blízký
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
jim	on	k3xPp3gMnPc3	on
také	také	k9	také
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Potíže	potíž	k1gFnPc4	potíž
s	s	k7c7	s
(	(	kIx(	(
<g/>
ne	ne	k9	ne
<g/>
)	)	kIx)	)
<g/>
mrtvými	mrtvý	k1gMnPc7	mrtvý
se	se	k3xPyFc4	se
vyřeší	vyřešit	k5eAaPmIp3nS	vyřešit
až	až	k9	až
exhumací	exhumace	k1gFnSc7	exhumace
těl	tělo	k1gNnPc2	tělo
zemřelých	zemřelá	k1gFnPc2	zemřelá
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc7	jejich
opětovným	opětovný	k2eAgNnSc7d1	opětovné
uložením	uložení	k1gNnSc7	uložení
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
kostela	kostel	k1gInSc2	kostel
a	a	k8xC	a
za	za	k7c2	za
doprovodu	doprovod	k1gInSc2	doprovod
křesťanských	křesťanský	k2eAgInPc2d1	křesťanský
rituálů	rituál	k1gInPc2	rituál
-	-	kIx~	-
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
těla	tělo	k1gNnSc2	tělo
prvního	první	k4xOgMnSc2	první
zemřelého	zemřelý	k1gMnSc2	zemřelý
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
je	být	k5eAaImIp3nS	být
jakožto	jakožto	k8xS	jakožto
upír	upír	k1gMnSc1	upír
spálen	spálen	k2eAgMnSc1d1	spálen
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
upírech	upír	k1gMnPc6	upír
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
o	o	k7c6	o
pohanských	pohanský	k2eAgFnPc6d1	pohanská
zvyklostech	zvyklost	k1gFnPc6	zvyklost
<g/>
)	)	kIx)	)
přináší	přinášet	k5eAaImIp3nS	přinášet
staroruské	staroruský	k2eAgInPc4d1	staroruský
křesťanské	křesťanský	k2eAgInPc4d1	křesťanský
spisy	spis	k1gInPc4	spis
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
až	až	k9	až
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnSc1d3	nejznámější
upír	upír	k1gMnSc1	upír
Drákula	Drákula	k1gMnSc1	Drákula
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
historickou	historický	k2eAgFnSc7d1	historická
osobností	osobnost	k1gFnSc7	osobnost
krutého	krutý	k2eAgMnSc2d1	krutý
knížete	kníže	k1gMnSc2	kníže
Vlada	Vlad	k1gMnSc2	Vlad
Ţepeşe	Ţepeş	k1gMnSc2	Ţepeş
jen	jen	k9	jen
volně	volně	k6eAd1	volně
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
skutečných	skutečný	k2eAgFnPc2d1	skutečná
legend	legenda	k1gFnPc2	legenda
mají	mít	k5eAaImIp3nP	mít
k	k	k7c3	k
vampirismu	vampirismus	k1gInSc3	vampirismus
blíže	blízce	k6eAd2	blízce
ty	ten	k3xDgInPc1	ten
o	o	k7c6	o
neméně	málo	k6eNd2	málo
známé	známý	k2eAgFnSc3d1	známá
uherské	uherský	k2eAgFnSc3d1	uherská
hraběnce	hraběnka	k1gFnSc3	hraběnka
Alžbětě	Alžběta	k1gFnSc3	Alžběta
Báthoryové	Báthoryová	k1gFnSc3	Báthoryová
(	(	kIx(	(
<g/>
1560	[number]	k4	1560
–	–	k?	–
1614	[number]	k4	1614
<g/>
)	)	kIx)	)
alias	alias	k9	alias
Čachtické	čachtický	k2eAgFnSc3d1	Čachtická
paní	paní	k1gFnSc3	paní
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
ona	onen	k3xDgFnSc1	onen
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
Stokerova	Stokerův	k2eAgInSc2d1	Stokerův
Draculu	Dracul	k1gInSc2	Dracul
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1725	[number]	k4	1725
v	v	k7c6	v
Habsburské	habsburský	k2eAgFnSc6d1	habsburská
monarchii	monarchie	k1gFnSc6	monarchie
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Peter	Peter	k1gMnSc1	Peter
Plogojowitz	Plogojowitz	k1gMnSc1	Plogojowitz
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
série	série	k1gFnSc1	série
náhlých	náhlý	k2eAgFnPc2d1	náhlá
úmrtí	úmrť	k1gFnPc2	úmrť
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
podezření	podezření	k1gNnSc3	podezření
z	z	k7c2	z
vampirismu	vampirismus	k1gInSc2	vampirismus
<g/>
.	.	kIx.	.
</s>
<s>
Peter	Peter	k1gMnSc1	Peter
byl	být	k5eAaImAgMnS	být
exhumován	exhumovat	k5eAaBmNgMnS	exhumovat
<g/>
,	,	kIx,	,
prozkoumán	prozkoumat	k5eAaPmNgInS	prozkoumat
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
shledáno	shledat	k5eAaPmNgNnS	shledat
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
příznaky	příznak	k1gInPc4	příznak
vampirismu	vampirismus	k1gInSc2	vampirismus
a	a	k8xC	a
poté	poté	k6eAd1	poté
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
probodeno	probodnout	k5eAaPmNgNnS	probodnout
srdce	srdce	k1gNnSc4	srdce
kůlem	kůl	k1gInSc7	kůl
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
spálen	spálit	k5eAaPmNgMnS	spálit
<g/>
.	.	kIx.	.
</s>
<s>
Případ	případ	k1gInSc1	případ
byl	být	k5eAaImAgInS	být
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
dokumentován	dokumentován	k2eAgMnSc1d1	dokumentován
<g/>
,	,	kIx,	,
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
byla	být	k5eAaImAgFnS	být
překládána	překládán	k2eAgFnSc1d1	překládána
a	a	k8xC	a
šířena	šířen	k2eAgFnSc1d1	šířena
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
následována	následovat	k5eAaImNgFnS	následovat
dalšími	další	k2eAgInPc7d1	další
případy	případ	k1gInPc7	případ
<g/>
.	.	kIx.	.
</s>
<s>
Hysterii	hysterie	k1gFnSc4	hysterie
ukončila	ukončit	k5eAaPmAgFnS	ukončit
Marie	Marie	k1gFnSc1	Marie
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
když	když	k8xS	když
nechala	nechat	k5eAaPmAgFnS	nechat
svého	svůj	k3xOyFgMnSc4	svůj
osobního	osobní	k2eAgMnSc4d1	osobní
lékaře	lékař	k1gMnSc4	lékař
případy	případ	k1gInPc4	případ
vyšetřit	vyšetřit	k5eAaPmF	vyšetřit
a	a	k8xC	a
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
závěru	závěr	k1gInSc6	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
upíři	upír	k1gMnPc1	upír
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
,	,	kIx,	,
vydala	vydat	k5eAaPmAgFnS	vydat
zákon	zákon	k1gInSc4	zákon
zakazující	zakazující	k2eAgNnSc4d1	zakazující
otevírání	otevírání	k1gNnSc4	otevírání
hrobů	hrob	k1gInPc2	hrob
a	a	k8xC	a
zhanobování	zhanobování	k1gNnPc2	zhanobování
těl	tělo	k1gNnPc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
upíry	upír	k1gMnPc7	upír
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
dílech	díl	k1gInPc6	díl
existují	existovat	k5eAaImIp3nP	existovat
značné	značný	k2eAgInPc1d1	značný
rozdíly	rozdíl	k1gInPc1	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nemrtvé	mrtvý	k2eNgNnSc4d1	nemrtvé
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
oživená	oživený	k2eAgNnPc1d1	oživené
lidská	lidský	k2eAgNnPc1d1	lidské
těla	tělo	k1gNnPc1	tělo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
romantické	romantický	k2eAgFnSc6d1	romantická
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
ve	v	k7c6	v
filmu	film	k1gInSc6	film
se	se	k3xPyFc4	se
upírem	upír	k1gMnSc7	upír
stane	stanout	k5eAaPmIp3nS	stanout
člověk	člověk	k1gMnSc1	člověk
pouze	pouze	k6eAd1	pouze
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
kousne	kousnout	k5eAaPmIp3nS	kousnout
jiný	jiný	k2eAgMnSc1d1	jiný
upír	upír	k1gMnSc1	upír
<g/>
.	.	kIx.	.
</s>
<s>
Přeměna	přeměna	k1gFnSc1	přeměna
pak	pak	k6eAd1	pak
trvá	trvat	k5eAaImIp3nS	trvat
různě	různě	k6eAd1	různě
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
málo	málo	k4c1	málo
povedených	povedený	k2eAgFnPc2d1	povedená
přemeň	přemenit	k5eAaPmRp2nS	přemenit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mnohdy	mnohdy	k6eAd1	mnohdy
upír	upír	k1gMnSc1	upír
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
přestat	přestat	k5eAaPmF	přestat
sát	sát	k5eAaImNgInS	sát
a	a	k8xC	a
oběť	oběť	k1gFnSc1	oběť
prostě	prostě	k6eAd1	prostě
zabije	zabít	k5eAaPmIp3nS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Spousta	spousta	k1gFnSc1	spousta
upírů	upír	k1gMnPc2	upír
nechce	chtít	k5eNaImIp3nS	chtít
přeměňovat	přeměňovat	k5eAaImF	přeměňovat
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
nezbytně	nezbytně	k6eAd1	nezbytně
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novějších	nový	k2eAgNnPc6d2	novější
dílech	dílo	k1gNnPc6	dílo
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
ale	ale	k9	ale
jednat	jednat	k5eAaImF	jednat
i	i	k9	i
o	o	k7c4	o
druh	druh	k1gInSc4	druh
démonů	démon	k1gMnPc2	démon
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
u	u	k7c2	u
Sapkowského	Sapkowské	k1gNnSc2	Sapkowské
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
polodémonů	polodémon	k1gInPc2	polodémon
(	(	kIx(	(
<g/>
americký	americký	k2eAgInSc1d1	americký
seriál	seriál	k1gInSc1	seriál
Buffy	buffa	k1gFnSc2	buffa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
novějších	nový	k2eAgNnPc6d2	novější
dílech	dílo	k1gNnPc6	dílo
jsou	být	k5eAaImIp3nP	být
upíři	upír	k1gMnPc1	upír
vlastně	vlastně	k9	vlastně
jen	jen	k6eAd1	jen
jiným	jiný	k2eAgInSc7d1	jiný
živočišným	živočišný	k2eAgInSc7d1	živočišný
druhem	druh	k1gInSc7	druh
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc1	jejich
upírství	upírství	k1gNnSc1	upírství
virového	virový	k2eAgInSc2d1	virový
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
třeba	třeba	k6eAd1	třeba
americká	americký	k2eAgFnSc1d1	americká
filmová	filmový	k2eAgFnSc1d1	filmová
serie	serie	k1gFnSc1	serie
Blade	Blad	k1gInSc5	Blad
nebo	nebo	k8xC	nebo
ruská	ruský	k2eAgFnSc1d1	ruská
sága	sága	k1gFnSc1	sága
Noční	noční	k2eAgFnSc1d1	noční
hlídka	hlídka	k1gFnSc1	hlídka
<g/>
.	.	kIx.	.
</s>
<s>
Nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
jen	jen	k9	jen
prokletí	prokletý	k2eAgMnPc1d1	prokletý
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
znakem	znak	k1gInSc7	znak
upírů	upír	k1gMnPc2	upír
je	být	k5eAaImIp3nS	být
pití	pití	k1gNnSc1	pití
nebo	nebo	k8xC	nebo
spíš	spíš	k9	spíš
sání	sání	k1gNnSc1	sání
krve	krev	k1gFnSc2	krev
pomocí	pomocí	k7c2	pomocí
prodloužených	prodloužený	k2eAgInPc2d1	prodloužený
špičáků	špičák	k1gInPc2	špičák
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
duté	dutý	k2eAgInPc1d1	dutý
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jimi	on	k3xPp3gFnPc7	on
přímo	přímo	k6eAd1	přímo
sají	sát	k5eAaImIp3nP	sát
krev	krev	k1gFnSc4	krev
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jimi	on	k3xPp3gInPc7	on
jen	jen	k6eAd1	jen
prokusují	prokusovat	k5eAaImIp3nP	prokusovat
tepny	tepna	k1gFnPc4	tepna
a	a	k8xC	a
krev	krev	k1gFnSc4	krev
chlemtají	chlemtat	k5eAaImIp3nP	chlemtat
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
živí	živit	k5eAaImIp3nS	živit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
ve	v	k7c6	v
fyzickém	fyzický	k2eAgInSc6d1	fyzický
smyslu	smysl	k1gInSc6	smysl
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
využívají	využívat	k5eAaPmIp3nP	využívat
chemické	chemický	k2eAgFnPc1d1	chemická
látky	látka	k1gFnPc1	látka
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
,	,	kIx,	,
takoví	takový	k3xDgMnPc1	takový
upíři	upír	k1gMnPc1	upír
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
živit	živit	k5eAaImF	živit
i	i	k9	i
krevními	krevní	k2eAgFnPc7d1	krevní
konzervami	konzerva	k1gFnPc7	konzerva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
metafyzickém	metafyzický	k2eAgInSc6d1	metafyzický
(	(	kIx(	(
<g/>
jde	jít	k5eAaImIp3nS	jít
vlastně	vlastně	k9	vlastně
o	o	k7c4	o
vysávání	vysávání	k1gNnSc4	vysávání
životní	životní	k2eAgFnSc2d1	životní
síly	síla	k1gFnSc2	síla
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
krev	krev	k1gFnSc4	krev
lidskou	lidský	k2eAgFnSc4d1	lidská
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
lidská	lidský	k2eAgFnSc1d1	lidská
krev	krev	k1gFnSc1	krev
z	z	k7c2	z
nějakého	nějaký	k3yIgInSc2	nějaký
důvodu	důvod	k1gInSc2	důvod
lepší	dobrý	k2eAgFnSc1d2	lepší
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jedno	jeden	k4xCgNnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
upíři	upír	k1gMnPc1	upír
přijímají	přijímat	k5eAaImIp3nP	přijímat
i	i	k9	i
lidskou	lidský	k2eAgFnSc4d1	lidská
potravu	potrava	k1gFnSc4	potrava
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ji	on	k3xPp3gFnSc4	on
dokonce	dokonce	k9	dokonce
přijímat	přijímat	k5eAaImF	přijímat
musí	muset	k5eAaImIp3nS	muset
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Naprostá	naprostý	k2eAgFnSc1d1	naprostá
většina	většina	k1gFnSc1	většina
upírů	upír	k1gMnPc2	upír
je	být	k5eAaImIp3nS	být
nesmrtelných	smrtelný	k2eNgFnPc6d1	nesmrtelná
nebo	nebo	k8xC	nebo
dlouhověkých	dlouhověký	k2eAgFnPc6d1	dlouhověká
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
nestárnou	stárnout	k5eNaImIp3nP	stárnout
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jejich	jejich	k3xOp3gFnSc1	jejich
fyzická	fyzický	k2eAgFnSc1d1	fyzická
podoba	podoba	k1gFnSc1	podoba
ustrnula	ustrnout	k5eAaPmAgFnS	ustrnout
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
byli	být	k5eAaImAgMnP	být
iniciováni	iniciován	k2eAgMnPc1d1	iniciován
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c6	na
prahu	práh	k1gInSc6	práh
dospělosti	dospělost	k1gFnSc2	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
stárnou	stárnout	k5eAaImIp3nP	stárnout
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
několikrát	několikrát	k6eAd1	několikrát
pomaleji	pomale	k6eAd2	pomale
než	než	k8xS	než
člověk	člověk	k1gMnSc1	člověk
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
od	od	k7c2	od
Darrena	Darreno	k1gNnSc2	Darreno
Shana	Shan	k1gInSc2	Shan
stárnou	stárnout	k5eAaImIp3nP	stárnout
upíři	upír	k1gMnPc1	upír
desetkrát	desetkrát	k6eAd1	desetkrát
a	a	k8xC	a
poloupíři	poloupíř	k1gMnPc1	poloupíř
pětkrát	pětkrát	k6eAd1	pětkrát
pomaleji	pomale	k6eAd2	pomale
než	než	k8xS	než
lidé	člověk	k1gMnPc1	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
v	v	k7c6	v
normální	normální	k2eAgFnSc6d1	normální
lidské	lidský	k2eAgFnSc6d1	lidská
společonsti	společonst	k1gFnSc6	společonst
<g/>
,	,	kIx,	,
skrývají	skrývat	k5eAaImIp3nP	skrývat
se	se	k3xPyFc4	se
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
čas	čas	k1gInSc4	čas
se	se	k3xPyFc4	se
stěhují	stěhovat	k5eAaImIp3nP	stěhovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
svou	svůj	k3xOyFgFnSc7	svůj
neměnící	měnící	k2eNgFnSc7d1	neměnící
se	se	k3xPyFc4	se
podobou	podoba	k1gFnSc7	podoba
a	a	k8xC	a
zvyky	zvyk	k1gInPc1	zvyk
(	(	kIx(	(
<g/>
spaní	spaní	k1gNnSc1	spaní
ve	v	k7c6	v
dne	den	k1gInSc2	den
<g/>
)	)	kIx)	)
nevzbudili	vzbudit	k5eNaPmAgMnP	vzbudit
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
při	při	k7c6	při
krmení	krmení	k1gNnSc6	krmení
zabíjí	zabíjet	k5eAaImIp3nP	zabíjet
<g/>
,	,	kIx,	,
nemívají	mívat	k5eNaImIp3nP	mívat
pevný	pevný	k2eAgInSc4d1	pevný
domov	domov	k1gInSc4	domov
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
takřka	takřka	k6eAd1	takřka
všichni	všechen	k3xTgMnPc1	všechen
upíři	upír	k1gMnPc1	upír
kočovní	kočovní	k2eAgMnPc1d1	kočovní
samotáři	samotář	k1gMnPc1	samotář
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existuje	existovat	k5eAaImIp3nS	existovat
jedno	jeden	k4xCgNnSc1	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žijí	žít	k5eAaImIp3nP	žít
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
scházejí	scházet	k5eAaImIp3nP	scházet
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
častou	častý	k2eAgFnSc7d1	častá
vlastností	vlastnost	k1gFnSc7	vlastnost
upírů	upír	k1gMnPc2	upír
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
neodráží	odrážet	k5eNaImIp3nS	odrážet
v	v	k7c6	v
zrcadle	zrcadlo	k1gNnSc6	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
souvislost	souvislost	k1gFnSc4	souvislost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dříve	dříve	k6eAd2	dříve
měla	mít	k5eAaImAgFnS	mít
většina	většina	k1gFnSc1	většina
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
odraznou	odrazný	k2eAgFnSc4d1	odrazná
vrstvu	vrstva	k1gFnSc4	vrstva
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
stříbra	stříbro	k1gNnSc2	stříbro
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Benátská	benátský	k2eAgNnPc4d1	benátské
zrcadla	zrcadlo	k1gNnPc4	zrcadlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgNnSc1d1	moderní
zrcadla	zrcadlo	k1gNnPc1	zrcadlo
mají	mít	k5eAaImIp3nP	mít
odraznou	odrazný	k2eAgFnSc4d1	odrazná
plochu	plocha	k1gFnSc4	plocha
z	z	k7c2	z
hliníku	hliník	k1gInSc2	hliník
nebo	nebo	k8xC	nebo
niklu	nikl	k1gInSc2	nikl
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
běžných	běžný	k2eAgNnPc2d1	běžné
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
nebyl	být	k5eNaImAgMnS	být
upír	upír	k1gMnSc1	upír
vidět	vidět	k5eAaImF	vidět
třeba	třeba	k6eAd1	třeba
v	v	k7c6	v
objektivu	objektiv	k1gInSc6	objektiv
kamery	kamera	k1gFnSc2	kamera
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Dracula	Draculum	k1gNnSc2	Draculum
2000	[number]	k4	2000
–	–	k?	–
možná	možný	k2eAgFnSc1d1	možná
že	že	k8xS	že
její	její	k3xOp3gInSc1	její
objektiv	objektiv	k1gInSc1	objektiv
nějaké	nějaký	k3yIgNnSc4	nějaký
zrcadlo	zrcadlo	k1gNnSc4	zrcadlo
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
nedají	dát	k5eNaPmIp3nP	dát
vyfotit	vyfotit	k5eAaPmF	vyfotit
(	(	kIx(	(
<g/>
na	na	k7c6	na
celuloidovém	celuloidový	k2eAgInSc6d1	celuloidový
filmu	film	k1gInSc6	film
je	být	k5eAaImIp3nS	být
roztok	roztok	k1gInSc1	roztok
halogenidů	halogenid	k1gInPc2	halogenid
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
skoro	skoro	k6eAd1	skoro
žádný	žádný	k3yNgMnSc1	žádný
autor	autor	k1gMnSc1	autor
to	ten	k3xDgNnSc4	ten
nezmiňuje	zmiňovat	k5eNaImIp3nS	zmiňovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
Darrena	Darren	k1gMnSc2	Darren
Shana	Shan	k1gMnSc2	Shan
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vysvětleno	vysvětlit	k5eAaPmNgNnS	vysvětlit
prostě	prostě	k6eAd1	prostě
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
atomy	atom	k1gInPc4	atom
jejich	jejich	k3xOp3gNnPc2	jejich
těl	tělo	k1gNnPc2	tělo
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
těla	tělo	k1gNnSc2	tělo
lidského	lidský	k2eAgNnSc2d1	lidské
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgNnSc1d1	jiné
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
upíři	upír	k1gMnPc1	upír
vlastně	vlastně	k9	vlastně
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
paralelního	paralelní	k2eAgInSc2d1	paralelní
světa	svět	k1gInSc2	svět
nacházejícího	nacházející	k2eAgInSc2d1	nacházející
se	se	k3xPyFc4	se
za	za	k7c7	za
zrcadly	zrcadlo	k1gNnPc7	zrcadlo
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
za	za	k7c7	za
zrcadly	zrcadlo	k1gNnPc7	zrcadlo
logicky	logicky	k6eAd1	logicky
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
vysvětlení	vysvětlení	k1gNnSc1	vysvětlení
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c4	na
metafyzičnosti	metafyzičnost	k1gFnPc4	metafyzičnost
upírů	upír	k1gMnPc2	upír
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vidíme	vidět	k5eAaImIp1nP	vidět
upíra	upír	k1gMnSc4	upír
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
ne	ne	k9	ne
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
bychom	by	kYmCp1nP	by
ho	on	k3xPp3gMnSc4	on
skutečně	skutečně	k6eAd1	skutečně
viděli	vidět	k5eAaImAgMnP	vidět
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
upíři	upír	k1gMnPc1	upír
nemají	mít	k5eNaImIp3nP	mít
fyzické	fyzický	k2eAgNnSc4d1	fyzické
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
upír	upír	k1gMnSc1	upír
nám	my	k3xPp1nPc3	my
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
mysli	mysl	k1gFnSc2	mysl
a	a	k8xC	a
způsobil	způsobit	k5eAaPmAgMnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
my	my	k3xPp1nPc1	my
sami	sám	k3xTgMnPc1	sám
si	se	k3xPyFc3	se
jeho	on	k3xPp3gInSc4	on
obraz	obraz	k1gInSc4	obraz
promítáme	promítat	k5eAaImIp1nP	promítat
<g/>
.	.	kIx.	.
</s>
<s>
Skutečné	skutečný	k2eAgFnPc1d1	skutečná
věci	věc	k1gFnPc1	věc
(	(	kIx(	(
<g/>
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
stěny	stěna	k1gFnPc1	stěna
<g/>
)	)	kIx)	)
jím	jíst	k5eAaImIp1nS	jíst
pak	pak	k6eAd1	pak
procházejí	procházet	k5eAaImIp3nP	procházet
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
on	on	k3xPp3gMnSc1	on
jimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jejich	jejich	k3xOp3gFnPc4	jejich
další	další	k2eAgFnPc4d1	další
vlastnosti	vlastnost	k1gFnPc4	vlastnost
patří	patřit	k5eAaImIp3nS	patřit
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
síla	síla	k1gFnSc1	síla
a	a	k8xC	a
zlepšené	zlepšený	k2eAgInPc1d1	zlepšený
smysly	smysl	k1gInPc1	smysl
<g/>
,	,	kIx,	,
především	především	k9	především
schopnost	schopnost	k1gFnSc4	schopnost
vidět	vidět	k5eAaImF	vidět
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
upíři	upír	k1gMnPc1	upír
létají	létat	k5eAaImIp3nP	létat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
levitací	levitace	k1gFnSc7	levitace
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
netopýra	netopýr	k1gMnSc2	netopýr
<g/>
.	.	kIx.	.
</s>
<s>
Drákula	Drákula	k1gMnSc1	Drákula
chodil	chodit	k5eAaImAgMnS	chodit
či	či	k8xC	či
lezl	lézt	k5eAaImAgMnS	lézt
po	po	k7c6	po
zdech	zeď	k1gFnPc6	zeď
hlavou	hlava	k1gFnSc7	hlava
dolů	dol	k1gInPc2	dol
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
po	po	k7c6	po
stropě	strop	k1gInSc6	strop
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
zmíněného	zmíněný	k2eAgMnSc2d1	zmíněný
netopýra	netopýr	k1gMnSc2	netopýr
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
autorů	autor	k1gMnPc2	autor
mohou	moct	k5eAaImIp3nP	moct
měnit	měnit	k5eAaImF	měnit
v	v	k7c4	v
mlhu	mlha	k1gFnSc4	mlha
<g/>
,	,	kIx,	,
vlka	vlk	k1gMnSc4	vlk
<g/>
,	,	kIx,	,
hromadu	hromada	k1gFnSc4	hromada
krys	krysa	k1gFnPc2	krysa
nebo	nebo	k8xC	nebo
červů	červ	k1gMnPc2	červ
<g/>
.	.	kIx.	.
</s>
<s>
Upíři	upír	k1gMnPc1	upír
mají	mít	k5eAaImIp3nP	mít
často	často	k6eAd1	často
hypnotické	hypnotický	k2eAgFnPc1d1	hypnotická
<g/>
,	,	kIx,	,
telepatické	telepatický	k2eAgFnPc1d1	telepatická
<g/>
,	,	kIx,	,
telekinetické	telekinetický	k2eAgFnPc1d1	telekinetický
nebo	nebo	k8xC	nebo
prostě	prostě	k6eAd1	prostě
magické	magický	k2eAgFnPc4d1	magická
schopnosti	schopnost	k1gFnPc4	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
lidem	člověk	k1gMnPc3	člověk
vstupovat	vstupovat	k5eAaImF	vstupovat
do	do	k7c2	do
snů	sen	k1gInPc2	sen
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
dokáží	dokázat	k5eAaPmIp3nP	dokázat
procházet	procházet	k5eAaImF	procházet
stěnami	stěna	k1gFnPc7	stěna
<g/>
,	,	kIx,	,
cestovat	cestovat	k5eAaImF	cestovat
prostorem	prostor	k1gInSc7	prostor
i	i	k8xC	i
časem	čas	k1gInSc7	čas
<g/>
.	.	kIx.	.
</s>
<s>
Klasičtí	klasický	k2eAgMnPc1d1	klasický
upíři	upír	k1gMnPc1	upír
spí	spát	k5eAaImIp3nP	spát
přes	přes	k7c4	přes
den	den	k1gInSc4	den
v	v	k7c6	v
rakvích	rakev	k1gFnPc6	rakev
(	(	kIx(	(
<g/>
někteří	některý	k3yIgMnPc1	některý
dávají	dávat	k5eAaImIp3nP	dávat
přednost	přednost	k1gFnSc4	přednost
výrazu	výraz	k1gInSc2	výraz
sarkofág	sarkofág	k1gInSc1	sarkofág
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stokerův	Stokerův	k2eAgMnSc1d1	Stokerův
Drákula	Drákula	k1gMnSc1	Drákula
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
bednách	bedna	k1gFnPc6	bedna
(	(	kIx(	(
<g/>
nebyly	být	k5eNaImAgFnP	být
to	ten	k3xDgNnSc1	ten
rakve	rakev	k1gFnPc1	rakev
<g/>
)	)	kIx)	)
hlínu	hlína	k1gFnSc4	hlína
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
nespal	spát	k5eNaImAgMnS	spát
<g/>
,	,	kIx,	,
zemřel	zemřít	k5eAaPmAgMnS	zemřít
by	by	kYmCp3nS	by
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
spánek	spánek	k1gInSc1	spánek
<g/>
"	"	kIx"	"
ovšem	ovšem	k9	ovšem
většinou	většina	k1gFnSc7	většina
není	být	k5eNaImIp3nS	být
úplně	úplně	k6eAd1	úplně
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
upírům	upír	k1gMnPc3	upír
se	se	k3xPyFc4	se
nezdají	zdát	k5eNaImIp3nP	zdát
sny	sen	k1gInPc1	sen
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
spí	spát	k5eAaImIp3nP	spát
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jsou	být	k5eAaImIp3nP	být
zcela	zcela	k6eAd1	zcela
mrtví	mrtvý	k2eAgMnPc1d1	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Probouzení	probouzení	k1gNnSc1	probouzení
pak	pak	k6eAd1	pak
bývá	bývat	k5eAaImIp3nS	bývat
pomalé	pomalý	k2eAgNnSc1d1	pomalé
<g/>
,	,	kIx,	,
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
,	,	kIx,	,
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
i	i	k9	i
bolestivé	bolestivý	k2eAgNnSc1d1	bolestivé
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
upíři	upír	k1gMnPc1	upír
ovšem	ovšem	k9	ovšem
nespí	spát	k5eNaImIp3nP	spát
nikdy	nikdy	k6eAd1	nikdy
<g/>
.	.	kIx.	.
</s>
<s>
Pitím	pití	k1gNnSc7	pití
krve	krev	k1gFnSc2	krev
se	se	k3xPyFc4	se
upíři	upír	k1gMnPc1	upír
(	(	kIx(	(
<g/>
především	především	k9	především
ti	ten	k3xDgMnPc1	ten
nemrtví	mrtvý	k2eNgMnPc1d1	nemrtvý
<g/>
)	)	kIx)	)
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
různých	různý	k2eAgInPc2d1	různý
pramenů	pramen	k1gInPc2	pramen
se	se	k3xPyFc4	se
člověk	člověk	k1gMnSc1	člověk
změní	změnit	k5eAaPmIp3nS	změnit
v	v	k7c4	v
upíra	upír	k1gMnSc4	upír
<g/>
,	,	kIx,	,
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
upírem	upír	k1gMnSc7	upír
zcela	zcela	k6eAd1	zcela
vysán	vysán	k2eAgMnSc1d1	vysán
<g/>
,	,	kIx,	,
když	když	k8xS	když
naopak	naopak	k6eAd1	naopak
není	být	k5eNaImIp3nS	být
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
když	když	k8xS	když
sám	sám	k3xTgMnSc1	sám
pije	pít	k5eAaImIp3nS	pít
krev	krev	k1gFnSc4	krev
upíra	upír	k1gMnSc2	upír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
dílech	díl	k1gInPc6	díl
se	se	k3xPyFc4	se
ale	ale	k9	ale
upíři	upír	k1gMnPc1	upír
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
i	i	k8xC	i
běžným	běžný	k2eAgNnSc7d1	běžné
pohlavním	pohlavní	k2eAgNnSc7d1	pohlavní
rozmnožováním	rozmnožování	k1gNnSc7	rozmnožování
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
nebo	nebo	k8xC	nebo
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
-	-	kIx~	-
obě	dva	k4xCgFnPc1	dva
varianty	varianta	k1gFnPc1	varianta
se	se	k3xPyFc4	se
nevylučují	vylučovat	k5eNaImIp3nP	vylučovat
<g/>
.	.	kIx.	.
</s>
<s>
Nový	nový	k2eAgMnSc1d1	nový
upír	upír	k1gMnSc1	upír
je	být	k5eAaImIp3nS	být
učedník	učedník	k1gMnSc1	učedník
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
kdo	kdo	k3yQnSc1	kdo
ho	on	k3xPp3gMnSc4	on
zasvětil	zasvětit	k5eAaPmAgMnS	zasvětit
krví	krev	k1gFnSc7	krev
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
mistr	mistr	k1gMnSc1	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
oblíbené	oblíbený	k2eAgNnSc4d1	oblíbené
téma	téma	k1gNnSc4	téma
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pokud	pokud	k8xS	pokud
byl	být	k5eAaImAgMnS	být
upír	upír	k1gMnSc1	upír
zasvěcen	zasvěcen	k2eAgMnSc1d1	zasvěcen
nedobrovolně	dobrovolně	k6eNd1	dobrovolně
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
takových	takový	k3xDgMnPc2	takový
upírů	upír	k1gMnPc2	upír
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
odmítá	odmítat	k5eAaImIp3nS	odmítat
pít	pít	k5eAaImF	pít
lidskou	lidský	k2eAgFnSc4d1	lidská
krev	krev	k1gFnSc4	krev
a	a	k8xC	a
radši	rád	k6eAd2	rád
chtějí	chtít	k5eAaImIp3nP	chtít
zemřít	zemřít	k5eAaPmF	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Potomek	potomek	k1gMnSc1	potomek
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
upíra	upír	k1gMnSc2	upír
<g/>
,	,	kIx,	,
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
dhampír	dhampír	k1gInSc1	dhampír
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
různé	různý	k2eAgFnPc4d1	různá
kombinace	kombinace	k1gFnPc4	kombinace
upírských	upírský	k2eAgFnPc2d1	upírská
schopností	schopnost	k1gFnPc2	schopnost
a	a	k8xC	a
slabostí	slabost	k1gFnPc2	slabost
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
odolný	odolný	k2eAgInSc1d1	odolný
proti	proti	k7c3	proti
slunečnímu	sluneční	k2eAgNnSc3d1	sluneční
světlu	světlo	k1gNnSc3	světlo
(	(	kIx(	(
<g/>
ve	v	k7c6	v
světech	svět	k1gInPc6	svět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
čisté	čistý	k2eAgInPc1d1	čistý
upíry	upír	k1gMnPc7	upír
světlo	světlo	k1gNnSc4	světlo
zabíjí	zabíjet	k5eAaImIp3nP	zabíjet
<g/>
)	)	kIx)	)
-	-	kIx~	-
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
vlastnost	vlastnost	k1gFnSc4	vlastnost
jsou	být	k5eAaImIp3nP	být
upíry	upír	k1gMnPc7	upír
zváni	zvát	k5eAaImNgMnP	zvát
"	"	kIx"	"
<g/>
Denní	denní	k2eAgMnSc1d1	denní
tvor	tvor	k1gMnSc1	tvor
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Daywalker	Daywalker	k1gInSc1	Daywalker
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dhampíři	Dhampíř	k1gMnPc1	Dhampíř
se	se	k3xPyFc4	se
často	často	k6eAd1	často
stávají	stávat	k5eAaImIp3nP	stávat
ideálním	ideální	k2eAgInSc7d1	ideální
druhem	druh	k1gInSc7	druh
zabijáků	zabiják	k1gInPc2	zabiják
upírů	upír	k1gMnPc2	upír
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
bytosti	bytost	k1gFnPc4	bytost
napůl	napůl	k6eAd1	napůl
lidské	lidský	k2eAgFnPc4d1	lidská
a	a	k8xC	a
napůl	napůl	k6eAd1	napůl
upíří	upíří	k2eAgInSc1d1	upíří
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
Blade	Blad	k1gInSc5	Blad
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
své	svůj	k3xOyFgFnSc3	svůj
schopnosti	schopnost	k1gFnSc3	schopnost
získal	získat	k5eAaPmAgMnS	získat
<g/>
,	,	kIx,	,
když	když	k8xS	když
jeho	jeho	k3xOp3gFnSc4	jeho
matku	matka	k1gFnSc4	matka
kousl	kousnout	k5eAaPmAgMnS	kousnout
upír	upír	k1gMnSc1	upír
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
porodem	porod	k1gInSc7	porod
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
částečné	částečný	k2eAgFnSc3d1	částečná
přeměně	přeměna	k1gFnSc3	přeměna
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgMnSc7d3	nejstarší
upírem	upír	k1gMnSc7	upír
bývá	bývat	k5eAaImIp3nS	bývat
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
dílech	dílo	k1gNnPc6	dílo
Dracula	Draculum	k1gNnSc2	Draculum
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
existují	existovat	k5eAaImIp3nP	existovat
upíři	upír	k1gMnPc1	upír
mnohem	mnohem	k6eAd1	mnohem
déle	dlouho	k6eAd2	dlouho
–	–	k?	–
za	za	k7c4	za
nejstaršího	starý	k2eAgMnSc4d3	nejstarší
potom	potom	k6eAd1	potom
bývá	bývat	k5eAaImIp3nS	bývat
označena	označen	k2eAgFnSc1d1	označena
Adamova	Adamův	k2eAgFnSc1d1	Adamova
první	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
Lilith	Lilitha	k1gFnPc2	Lilitha
(	(	kIx(	(
<g/>
ano	ano	k9	ano
<g/>
,	,	kIx,	,
před	před	k7c7	před
Evou	Eva	k1gFnSc7	Eva
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Kain	Kain	k1gMnSc1	Kain
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
je	být	k5eAaImIp3nS	být
vůbec	vůbec	k9	vůbec
nejstarší	starý	k2eAgMnSc1d3	nejstarší
upír	upír	k1gMnSc1	upír
znám	znám	k2eAgMnSc1d1	znám
nebo	nebo	k8xC	nebo
jmenován	jmenován	k2eAgMnSc1d1	jmenován
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
míchají	míchat	k5eAaImIp3nP	míchat
upíry	upír	k1gMnPc4	upír
a	a	k8xC	a
vlkodlaky	vlkodlak	k1gMnPc4	vlkodlak
dohromady	dohromady	k6eAd1	dohromady
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
Stokerův	Stokerův	k2eAgInSc1d1	Stokerův
Dracula	Dracul	k1gMnSc4	Dracul
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
považují	považovat	k5eAaImIp3nP	považovat
lykantropy	lykantrop	k1gInPc1	lykantrop
za	za	k7c4	za
upírům	upír	k1gMnPc3	upír
podřízené	podřízená	k1gFnSc2	podřízená
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jiných	jiný	k1gMnPc2	jiný
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Underworld	Underworld	k1gInSc1	Underworld
<g/>
)	)	kIx)	)
spolu	spolu	k6eAd1	spolu
vedou	vést	k5eAaImIp3nP	vést
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Upíra	upír	k1gMnSc4	upír
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
zabít	zabít	k5eAaPmF	zabít
normálním	normální	k2eAgInSc7d1	normální
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
ať	ať	k8xC	ať
už	už	k9	už
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnPc3	jeho
regeneračním	regenerační	k2eAgFnPc3d1	regenerační
schopnostem	schopnost	k1gFnPc3	schopnost
nebo	nebo	k8xC	nebo
jakési	jakýsi	k3yIgFnSc2	jakýsi
přízračnosti	přízračnost	k1gFnSc2	přízračnost
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
upíry	upír	k1gMnPc7	upír
mohou	moct	k5eAaImIp3nP	moct
zabíjet	zabíjet	k5eAaImF	zabíjet
zase	zase	k9	zase
jen	jen	k9	jen
upíři	upír	k1gMnPc1	upír
nebo	nebo	k8xC	nebo
vlkodlaci	vlkodlak	k1gMnPc1	vlkodlak
<g/>
.	.	kIx.	.
</s>
<s>
Upíři	upír	k1gMnPc1	upír
dokonce	dokonce	k9	dokonce
nemohou	moct	k5eNaImIp3nP	moct
ani	ani	k8xC	ani
spáchat	spáchat	k5eAaPmF	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
obvyklé	obvyklý	k2eAgInPc4d1	obvyklý
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
oslabit	oslabit	k5eAaPmF	oslabit
či	či	k8xC	či
zastavit	zastavit	k5eAaPmF	zastavit
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Sluneční	sluneční	k2eAgNnSc1d1	sluneční
světlo	světlo	k1gNnSc1	světlo
-	-	kIx~	-
někdy	někdy	k6eAd1	někdy
stačí	stačit	k5eAaBmIp3nS	stačit
jakékoliv	jakýkoliv	k3yIgNnSc1	jakýkoliv
silné	silný	k2eAgNnSc1d1	silné
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
UV	UV	kA	UV
složka	složka	k1gFnSc1	složka
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
skutečně	skutečně	k6eAd1	skutečně
pouze	pouze	k6eAd1	pouze
světlo	světlo	k1gNnSc4	světlo
sluneční	sluneční	k2eAgInSc1d1	sluneční
Dřevěný	dřevěný	k2eAgInSc1d1	dřevěný
kůl	kůl	k1gInSc1	kůl
do	do	k7c2	do
srdce	srdce	k1gNnSc2	srdce
-	-	kIx~	-
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
určitý	určitý	k2eAgInSc1d1	určitý
druh	druh	k1gInSc1	druh
<g />
.	.	kIx.	.
</s>
<s>
dřeva	dřevo	k1gNnPc4	dřevo
(	(	kIx(	(
<g/>
tisové	tisový	k2eAgNnSc4d1	Tisové
<g/>
,	,	kIx,	,
osikové	osikový	k2eAgNnSc4d1	osikové
či	či	k8xC	či
dubové	dubový	k2eAgNnSc4d1	dubové
<g/>
)	)	kIx)	)
Lípa	lípa	k1gFnSc1	lípa
–	–	k?	–
svázání	svázání	k1gNnSc2	svázání
rukou	ruka	k1gFnPc2	ruka
lipovým	lipový	k2eAgNnSc7d1	lipové
lýkem	lýko	k1gNnSc7	lýko
po	po	k7c6	po
exhumaci	exhumace	k1gFnSc6	exhumace
(	(	kIx(	(
<g/>
Halič	Halič	k1gFnSc1	Halič
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posvěcený	posvěcený	k2eAgInSc1d1	posvěcený
lipový	lipový	k2eAgInSc4d1	lipový
lístek	lístek	k1gInSc4	lístek
pod	pod	k7c4	pod
jazyk	jazyk	k1gInSc4	jazyk
a	a	k8xC	a
lipová	lipový	k2eAgFnSc1d1	Lipová
hvězda	hvězda	k1gFnSc1	hvězda
na	na	k7c4	na
prsa	prsa	k1gNnPc4	prsa
(	(	kIx(	(
<g/>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
)	)	kIx)	)
Useknutí	useknutí	k1gNnSc1	useknutí
hlavy	hlava	k1gFnSc2	hlava
Tekoucí	tekoucí	k2eAgFnSc1d1	tekoucí
voda	voda	k1gFnSc1	voda
-	-	kIx~	-
nemohou	moct	k5eNaImIp3nP	moct
přejít	přejít	k5eAaPmF	přejít
přes	přes	k7c4	přes
vodní	vodní	k2eAgNnSc4d1	vodní
<g />
.	.	kIx.	.
</s>
<s>
tok	tok	k1gInSc1	tok
a	a	k8xC	a
pokud	pokud	k8xS	pokud
musí	muset	k5eAaImIp3nS	muset
přes	přes	k7c4	přes
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
cestují	cestovat	k5eAaImIp3nP	cestovat
ve	v	k7c6	v
spánku	spánek	k1gInSc6	spánek
v	v	k7c6	v
rakvích	rakev	k1gFnPc6	rakev
(	(	kIx(	(
<g/>
a	a	k8xC	a
nejlépe	dobře	k6eAd3	dobře
letadlem	letadlo	k1gNnSc7	letadlo
<g/>
)	)	kIx)	)
Náboženské	náboženský	k2eAgInPc4d1	náboženský
symboly	symbol	k1gInPc4	symbol
<g/>
:	:	kIx,	:
Kříž	kříž	k1gInSc1	kříž
Svěcená	svěcený	k2eAgFnSc1d1	svěcená
voda	voda	k1gFnSc1	voda
Hostie	hostie	k1gFnSc2	hostie
-	-	kIx~	-
nemohou	moct	k5eNaImIp3nP	moct
kolem	kolem	k7c2	kolem
ní	on	k3xPp3gFnSc2	on
projít	projít	k5eAaPmF	projít
a	a	k8xC	a
pálí	pálit	k5eAaImIp3nP	pálit
je	on	k3xPp3gFnPc4	on
Svěcené	svěcený	k2eAgFnPc4d1	svěcená
zbraně	zbraň	k1gFnPc4	zbraň
-	-	kIx~	-
pokud	pokud	k8xS	pokud
jim	on	k3xPp3gMnPc3	on
obyčejné	obyčejný	k2eAgFnPc1d1	obyčejná
zbraně	zbraň	k1gFnPc4	zbraň
neublíží	ublížit	k5eNaPmIp3nP	ublížit
<g/>
,	,	kIx,	,
svěcené	svěcený	k2eAgNnSc1d1	svěcené
je	být	k5eAaImIp3nS	být
zraňují	zraňovat	k5eAaImIp3nP	zraňovat
<g />
.	.	kIx.	.
</s>
<s>
jako	jako	k8xS	jako
člověka	člověk	k1gMnSc4	člověk
nebo	nebo	k8xC	nebo
i	i	k9	i
více	hodně	k6eAd2	hodně
Česnek	česnek	k1gInSc4	česnek
Snítka	snítka	k1gFnSc1	snítka
plané	planý	k2eAgFnSc2d1	planá
růže	růž	k1gFnSc2	růž
-	-	kIx~	-
položí	položit	k5eAaPmIp3nS	položit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
na	na	k7c4	na
rakev	rakev	k1gFnSc4	rakev
<g/>
,	,	kIx,	,
nemohou	moct	k5eNaImIp3nP	moct
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
ven	ven	k6eAd1	ven
Velké	velký	k2eAgInPc1d1	velký
kameny	kámen	k1gInPc1	kámen
položené	položená	k1gFnSc2	položená
na	na	k7c6	na
rakev	rakev	k1gFnSc1	rakev
jim	on	k3xPp3gMnPc3	on
znemožní	znemožnit	k5eAaPmIp3nS	znemožnit
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
funguje	fungovat	k5eAaImIp3nS	fungovat
i	i	k9	i
zalít	zalít	k5eAaPmF	zalít
hrob	hrob	k1gInSc4	hrob
betonem	beton	k1gInSc7	beton
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
skutečné	skutečný	k2eAgFnSc2d1	skutečná
osoby	osoba	k1gFnSc2	osoba
Beli	Bel	k1gFnSc2	Bel
Lugosiho	Lugosi	k1gMnSc2	Lugosi
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Zrcadlo	zrcadlo	k1gNnSc4	zrcadlo
-	-	kIx~	-
často	často	k6eAd1	často
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
upír	upír	k1gMnSc1	upír
neodráží	odrážet	k5eNaImIp3nS	odrážet
v	v	k7c6	v
zrcadle	zrcadlo	k1gNnSc6	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
pověra	pověra	k1gFnSc1	pověra
mohla	moct	k5eAaImAgFnS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
i	i	k9	i
jako	jako	k9	jako
důsledek	důsledek	k1gInSc1	důsledek
specifické	specifický	k2eAgFnSc2d1	specifická
psychické	psychický	k2eAgFnSc2d1	psychická
poruchy	porucha	k1gFnSc2	porucha
-	-	kIx~	-
negativní	negativní	k2eAgFnSc1d1	negativní
heautoskopie	heautoskopie	k1gFnSc1	heautoskopie
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
člověk	člověk	k1gMnSc1	člověk
nevidí	vidět	k5eNaImIp3nS	vidět
svůj	svůj	k3xOyFgInSc4	svůj
obraz	obraz	k1gInSc4	obraz
v	v	k7c6	v
zrcadle	zrcadlo	k1gNnSc6	zrcadlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
se	se	k3xPyFc4	se
upír	upír	k1gMnSc1	upír
zrcadlu	zrcadlo	k1gNnSc3	zrcadlo
dokonce	dokonce	k9	dokonce
vyhýbá	vyhýbat	k5eAaImIp3nS	vyhýbat
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
tvrzení	tvrzení	k1gNnSc1	tvrzení
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
prastaré	prastarý	k2eAgFnSc2d1	prastará
víry	víra	k1gFnSc2	víra
<g/>
,	,	kIx,	,
že	že	k8xS	že
démoni	démon	k1gMnPc1	démon
a	a	k8xC	a
temné	temný	k2eAgFnPc1d1	temná
mocnosti	mocnost	k1gFnPc1	mocnost
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
zaleknout	zaleknout	k5eAaPmF	zaleknout
vlastní	vlastní	k2eAgFnPc4d1	vlastní
ošklivosti	ošklivost	k1gFnPc4	ošklivost
a	a	k8xC	a
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
.	.	kIx.	.
</s>
<s>
Malým	malý	k2eAgFnPc3d1	malá
dětem	dítě	k1gFnPc3	dítě
proto	proto	k8xC	proto
někdy	někdy	k6eAd1	někdy
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
upíry	upír	k1gMnPc7	upír
a	a	k8xC	a
nočními	noční	k2eAgFnPc7d1	noční
můrami	můra	k1gFnPc7	můra
byla	být	k5eAaImAgFnS	být
do	do	k7c2	do
kolébek	kolébka	k1gFnPc2	kolébka
a	a	k8xC	a
postýlek	postýlka	k1gFnPc2	postýlka
vkládána	vkládán	k2eAgFnSc1d1	vkládána
malá	malá	k1gFnSc1	malá
zrcátka	zrcátko	k1gNnSc2	zrcátko
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
odhalit	odhalit	k5eAaPmF	odhalit
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterém	který	k3yRgInSc6	který
hrobě	hrob	k1gInSc6	hrob
je	být	k5eAaImIp3nS	být
uložen	uložen	k2eAgMnSc1d1	uložen
upír	upír	k1gMnSc1	upír
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
rozdělat	rozdělat	k5eAaPmF	rozdělat
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
oheň	oheň	k1gInSc4	oheň
z	z	k7c2	z
trnitých	trnitý	k2eAgFnPc2d1	trnitá
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
kouř	kouř	k1gInSc1	kouř
se	se	k3xPyFc4	se
prý	prý	k9	prý
neomylně	omylně	k6eNd1	omylně
snese	snést	k5eAaPmIp3nS	snést
na	na	k7c4	na
hrob	hrob	k1gInSc4	hrob
upíra	upír	k1gMnSc2	upír
<g/>
.	.	kIx.	.
ale	ale	k8xC	ale
vše	všechen	k3xTgNnSc4	všechen
jsou	být	k5eAaImIp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pověry	pověra	k1gFnPc1	pověra
spojené	spojený	k2eAgFnPc1d1	spojená
s	s	k7c7	s
vírou	víra	k1gFnSc7	víra
<g/>
...	...	k?	...
Žádný	žádný	k1gMnSc1	žádný
z	z	k7c2	z
uvedených	uvedený	k2eAgInPc2d1	uvedený
prostředků	prostředek	k1gInPc2	prostředek
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
"	"	kIx"	"
<g/>
spolehlivý	spolehlivý	k2eAgMnSc1d1	spolehlivý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nefunguje	fungovat	k5eNaImIp3nS	fungovat
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
literárním	literární	k2eAgInSc6d1	literární
díle	díl	k1gInSc6	díl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc4	tento
pověry	pověra	k1gFnPc4	pověra
rozšířili	rozšířit	k5eAaPmAgMnP	rozšířit
sami	sám	k3xTgMnPc1	sám
upíři	upír	k1gMnPc1	upír
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
cítili	cítit	k5eAaImAgMnP	cítit
v	v	k7c4	v
bezpečí	bezpečí	k1gNnSc4	bezpečí
a	a	k8xC	a
oni	onen	k3xDgMnPc1	onen
měli	mít	k5eAaImAgMnP	mít
volnou	volný	k2eAgFnSc4d1	volná
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Bram	brama	k1gFnPc2	brama
Stoker	Stoker	k1gInSc1	Stoker
pak	pak	k6eAd1	pak
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c2	za
upíra	upír	k1gMnSc2	upír
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
upír	upír	k1gMnSc1	upír
rozpadne	rozpadnout	k5eAaPmIp3nS	rozpadnout
na	na	k7c4	na
prach	prach	k1gInSc4	prach
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
pramenů	pramen	k1gInPc2	pramen
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
pak	pak	k6eAd1	pak
zcela	zcela	k6eAd1	zcela
zničen	zničen	k2eAgInSc1d1	zničen
–	–	k?	–
stačí	stačit	k5eAaBmIp3nS	stačit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
prach	prach	k1gInSc1	prach
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
styku	styk	k1gInSc2	styk
s	s	k7c7	s
krví	krev	k1gFnSc7	krev
a	a	k8xC	a
upír	upír	k1gMnSc1	upír
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
regeneruje	regenerovat	k5eAaBmIp3nS	regenerovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
komedii	komedie	k1gFnSc6	komedie
Káčula	Káčulum	k1gNnSc2	Káčulum
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
regeneraci	regenerace	k1gFnSc4	regenerace
zapotřebí	zapotřebí	k6eAd1	zapotřebí
speciální	speciální	k2eAgInSc4d1	speciální
rituál	rituál	k1gInSc4	rituál
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kromě	kromě	k7c2	kromě
krve	krev	k1gFnSc2	krev
zafungoval	zafungovat	k5eAaPmAgInS	zafungovat
i	i	k9	i
kečup	kečup	k1gInSc1	kečup
...	...	k?	...
Pro	pro	k7c4	pro
úplné	úplný	k2eAgNnSc4d1	úplné
zničení	zničení	k1gNnSc4	zničení
může	moct	k5eAaImIp3nS	moct
pomoci	pomoct	k5eAaPmF	pomoct
prach	prach	k1gInSc1	prach
rozprášit	rozprášit	k5eAaPmF	rozprášit
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
do	do	k7c2	do
tekoucí	tekoucí	k2eAgFnSc2d1	tekoucí
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgMnSc1d3	nejznámější
upír	upír	k1gMnSc1	upír
-	-	kIx~	-
Dracula	Dracula	k1gFnSc1	Dracula
-	-	kIx~	-
prvně	prvně	k?	prvně
se	se	k3xPyFc4	se
objevující	objevující	k2eAgMnSc1d1	objevující
ve	v	k7c6	v
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
knize	kniha	k1gFnSc6	kniha
Brama	brama	k1gFnSc1	brama
Stokera	Stokera	k1gFnSc1	Stokera
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
skutečné	skutečný	k2eAgFnSc6d1	skutečná
historické	historický	k2eAgFnSc6d1	historická
osobě	osoba	k1gFnSc6	osoba
–	–	k?	–
moldavském	moldavský	k2eAgNnSc6d1	moldavské
knížeti	kníže	k1gNnSc6wR	kníže
Vladu	Vlad	k1gInSc2	Vlad
<g/>
,	,	kIx,	,
řečeném	řečený	k2eAgNnSc6d1	řečené
Dráček	dráček	k1gMnSc1	dráček
(	(	kIx(	(
<g/>
Dracula	Dracula	k1gFnSc1	Dracula
či	či	k8xC	či
Drăculea	Drăculea	k1gFnSc1	Drăculea
<g/>
)	)	kIx)	)
a	a	k8xC	a
Naražeč	Naražeč	k1gInSc1	Naražeč
na	na	k7c4	na
kůl	kůl	k1gInSc4	kůl
resp.	resp.	kA	resp.
Napichovač	Napichovač	k1gInSc1	Napichovač
(	(	kIx(	(
<g/>
Ţepeş	Ţepeş	k1gFnSc1	Ţepeş
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
letech	let	k1gInPc6	let
1431	[number]	k4	1431
<g/>
–	–	k?	–
<g/>
1476	[number]	k4	1476
a	a	k8xC	a
proslul	proslout	k5eAaPmAgInS	proslout
svojí	svůj	k3xOyFgFnSc7	svůj
krutostí	krutost	k1gFnSc7	krutost
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
s	s	k7c7	s
Turky	Turek	k1gMnPc7	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Dracula	Dracula	k1gFnSc1	Dracula
je	být	k5eAaImIp3nS	být
zdrobnělinou	zdrobnělina	k1gFnSc7	zdrobnělina
přezdívky	přezdívka	k1gFnSc2	přezdívka
jeho	jeho	k3xOp3gMnSc2	jeho
otce	otec	k1gMnSc2	otec
-	-	kIx~	-
Drak	drak	k1gInSc1	drak
(	(	kIx(	(
<g/>
Dracul	Dracul	k1gInSc1	Dracul
<g/>
)	)	kIx)	)
-	-	kIx~	-
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
byl	být	k5eAaImAgMnS	být
označován	označovat	k5eAaImNgMnS	označovat
jako	jako	k8xC	jako
člen	člen	k1gMnSc1	člen
tzv.	tzv.	kA	tzv.
Dračího	dračí	k2eAgInSc2d1	dračí
řádu	řád	k1gInSc2	řád
císaře	císař	k1gMnSc2	císař
Zikmunda	Zikmund	k1gMnSc2	Zikmund
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
přezdívka	přezdívka	k1gFnSc1	přezdívka
Napichovač	Napichovač	k1gInSc4	Napichovač
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
oblíbeném	oblíbený	k2eAgInSc6d1	oblíbený
způsobu	způsob	k1gInSc6	způsob
popravy	poprava	k1gFnSc2	poprava
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
užíval	užívat	k5eAaImAgInS	užívat
k	k	k7c3	k
vyvolání	vyvolání	k1gNnSc3	vyvolání
strachu	strach	k1gInSc2	strach
a	a	k8xC	a
hrůzy	hrůza	k1gFnSc2	hrůza
u	u	k7c2	u
svých	svůj	k3xOyFgMnPc2	svůj
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
poselstvo	poselstvo	k1gNnSc1	poselstvo
tureckého	turecký	k2eAgMnSc2d1	turecký
sultána	sultán	k1gMnSc2	sultán
při	při	k7c6	při
audienci	audience	k1gFnSc6	audience
u	u	k7c2	u
knížete	kníže	k1gMnSc2	kníže
Draculy	Dracula	k1gFnSc2	Dracula
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
smeknout	smeknout	k5eAaPmF	smeknout
a	a	k8xC	a
vzdát	vzdát	k5eAaPmF	vzdát
mu	on	k3xPp3gMnSc3	on
čest	čest	k1gFnSc4	čest
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
kníže	kníže	k1gMnSc1	kníže
jim	on	k3xPp3gMnPc3	on
dal	dát	k5eAaPmAgMnS	dát
jejich	jejich	k3xOp3gFnSc2	jejich
pokrývky	pokrývka	k1gFnSc2	pokrývka
hlavy	hlava	k1gFnSc2	hlava
k	k	k7c3	k
hlavám	hlava	k1gFnPc3	hlava
přibít	přibít	k5eAaPmF	přibít
železnými	železný	k2eAgInPc7d1	železný
hřeby	hřeb	k1gInPc7	hřeb
a	a	k8xC	a
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
dal	dát	k5eAaPmAgMnS	dát
napíchnout	napíchnout	k5eAaPmF	napíchnout
na	na	k7c4	na
ostrý	ostrý	k2eAgInSc4d1	ostrý
kůl	kůl	k1gInSc4	kůl
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
moderních	moderní	k2eAgInPc2d1	moderní
výzkumů	výzkum	k1gInPc2	výzkum
ovšem	ovšem	k9	ovšem
Stoker	Stoker	k1gMnSc1	Stoker
o	o	k7c6	o
skutečném	skutečný	k2eAgNnSc6d1	skutečné
knížeti	kníže	k1gNnSc6wR	kníže
Vladovi	Vladův	k2eAgMnPc1d1	Vladův
mnoho	mnoho	k6eAd1	mnoho
nevěděl	vědět	k5eNaImAgMnS	vědět
a	a	k8xC	a
"	"	kIx"	"
<g/>
legendu	legenda	k1gFnSc4	legenda
<g/>
"	"	kIx"	"
sestavil	sestavit	k5eAaPmAgInS	sestavit
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
legend	legenda	k1gFnPc2	legenda
<g/>
,	,	kIx,	,
nemajících	mající	k2eNgInPc2d1	nemající
s	s	k7c7	s
Vladem	Vlad	k1gInSc7	Vlad
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgMnSc4d1	společný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rumunské	rumunský	k2eAgFnSc6d1	rumunská
tradici	tradice	k1gFnSc6	tradice
je	být	k5eAaImIp3nS	být
Vlad	Vlad	k1gInSc1	Vlad
Ţepeş	Ţepeş	k1gFnSc2	Ţepeş
pojímán	pojímat	k5eAaImNgInS	pojímat
jako	jako	k9	jako
velmi	velmi	k6eAd1	velmi
přísný	přísný	k2eAgMnSc1d1	přísný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spravedlivý	spravedlivý	k2eAgInSc1d1	spravedlivý
a	a	k8xC	a
všeobecně	všeobecně	k6eAd1	všeobecně
respektovaný	respektovaný	k2eAgMnSc1d1	respektovaný
panovník	panovník	k1gMnSc1	panovník
<g/>
,	,	kIx,	,
vynikající	vynikající	k2eAgMnSc1d1	vynikající
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
a	a	k8xC	a
statečný	statečný	k2eAgMnSc1d1	statečný
bojovník	bojovník	k1gMnSc1	bojovník
proti	proti	k7c3	proti
Turkům	Turek	k1gMnPc3	Turek
(	(	kIx(	(
<g/>
tak	tak	k8xC	tak
ho	on	k3xPp3gNnSc4	on
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
např.	např.	kA	např.
Mihai	Mihae	k1gFnSc4	Mihae
Eminescu	Eminescum	k1gNnSc3	Eminescum
v	v	k7c6	v
básni	báseň	k1gFnSc6	báseň
Třetí	třetí	k4xOgInSc4	třetí
dopis	dopis	k1gInSc4	dopis
a	a	k8xC	a
Ioan	Ioan	k1gInSc4	Ioan
Budai-Deleanu	Budai-Delean	k1gInSc2	Budai-Delean
v	v	k7c6	v
epose	epos	k1gInSc6	epos
Cikaniáda	Cikaniáda	k1gFnSc1	Cikaniáda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
rumunštině	rumunština	k1gFnSc6	rumunština
slovo	slovo	k1gNnSc1	slovo
dracul	dracula	k1gFnPc2	dracula
znamená	znamenat	k5eAaImIp3nS	znamenat
ďábel	ďábel	k1gMnSc1	ďábel
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Vladových	Vladův	k2eAgFnPc6d1	Vladova
dobách	doba	k1gFnPc6	doba
však	však	k8xC	však
tento	tento	k3xDgInSc4	tento
význam	význam	k1gInSc4	význam
nemělo	mít	k5eNaImAgNnS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Dobové	dobový	k2eAgFnPc1d1	dobová
zprávy	zpráva	k1gFnPc1	zpráva
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
ho	on	k3xPp3gMnSc4	on
líčí	líčit	k5eAaImIp3nS	líčit
jako	jako	k8xC	jako
šíleného	šílený	k2eAgMnSc4d1	šílený
tyrana	tyran	k1gMnSc4	tyran
<g/>
,	,	kIx,	,
pocházejí	pocházet	k5eAaImIp3nP	pocházet
především	především	k9	především
z	z	k7c2	z
německé	německý	k2eAgFnSc2d1	německá
a	a	k8xC	a
ruské	ruský	k2eAgFnSc2d1	ruská
oblasti	oblast	k1gFnSc2	oblast
(	(	kIx(	(
<g/>
např.	např.	kA	např.
báseň	báseň	k1gFnSc1	báseň
kronikáře	kronikář	k1gMnSc2	kronikář
Michaela	Michael	k1gMnSc2	Michael
Beheima	Beheim	k1gMnSc2	Beheim
Von	von	k1gInSc1	von
ainem	ainem	k6eAd1	ainem
wutrich	wutrich	k1gInSc1	wutrich
der	drát	k5eAaImRp2nS	drát
hies	hies	k1gInSc1	hies
Trakle	Trakl	k1gInSc5	Trakl
waida	waida	k1gFnSc1	waida
von	von	k1gInSc1	von
der	drát	k5eAaImRp2nS	drát
Walachei	Walachei	k1gNnPc1	Walachei
či	či	k8xC	či
ruské	ruský	k2eAgFnPc1d1	ruská
anonymní	anonymní	k2eAgFnPc1d1	anonymní
Skazanije	Skazanije	k1gFnPc1	Skazanije
o	o	k7c4	o
Drakule	Drakule	k1gFnPc4	Drakule
vojevodě	vojevodit	k5eAaImSgInS	vojevodit
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1481	[number]	k4	1481
<g/>
-	-	kIx~	-
<g/>
1486	[number]	k4	1486
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
německých	německý	k2eAgFnPc2d1	německá
předloh	předloha	k1gFnPc2	předloha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zřejmě	zřejmě	k6eAd1	zřejmě
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
perzekucí	perzekuce	k1gFnSc7	perzekuce
transylvánských	transylvánský	k2eAgMnPc2d1	transylvánský
Sasů	Sas	k1gMnPc2	Sas
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
Vlad	Vlad	k1gMnSc1	Vlad
podílel	podílet	k5eAaImAgMnS	podílet
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
Stoker	Stoker	k1gInSc1	Stoker
neupřesňuje	upřesňovat	k5eNaImIp3nS	upřesňovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
Dracula	Draculum	k1gNnSc2	Draculum
stal	stát	k5eAaPmAgMnS	stát
upírem	upír	k1gMnSc7	upír
<g/>
;	;	kIx,	;
v	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
zpracováních	zpracování	k1gNnPc6	zpracování
je	být	k5eAaImIp3nS	být
důvod	důvod	k1gInSc1	důvod
často	často	k6eAd1	často
náboženský	náboženský	k2eAgInSc1d1	náboženský
<g/>
:	:	kIx,	:
jednou	jednou	k6eAd1	jednou
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
Dracula	Dracul	k1gMnSc4	Dracul
na	na	k7c6	na
bitevním	bitevní	k2eAgNnSc6d1	bitevní
poli	pole	k1gNnSc6	pole
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
oklamána	oklamán	k2eAgFnSc1d1	oklamána
zprávou	zpráva	k1gFnSc7	zpráva
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
spáchala	spáchat	k5eAaPmAgFnS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Dracula	Dracula	k1gFnSc1	Dracula
poté	poté	k6eAd1	poté
proklel	proklít	k5eAaPmAgInS	proklít
Boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
věrně	věrně	k6eAd1	věrně
sloužil	sloužit	k5eAaImAgMnS	sloužit
<g/>
,	,	kIx,	,
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
upírem	upír	k1gMnSc7	upír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
zpracováních	zpracování	k1gNnPc6	zpracování
byl	být	k5eAaImAgInS	být
Dracula	Dracula	k1gFnSc1	Dracula
proklet	proklet	k2eAgMnSc1d1	proklet
knězem	kněz	k1gMnSc7	kněz
po	po	k7c6	po
vypálení	vypálení	k1gNnSc6	vypálení
kláštera	klášter	k1gInSc2	klášter
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
prokletí	prokletí	k1gNnSc1	prokletí
ho	on	k3xPp3gNnSc2	on
změnilo	změnit	k5eAaPmAgNnS	změnit
v	v	k7c4	v
upíra	upír	k1gMnSc4	upír
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
upíry	upír	k1gMnPc7	upír
byli	být	k5eAaImAgMnP	být
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
považováni	považován	k2eAgMnPc1d1	považován
především	především	k6eAd1	především
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
postižení	postižený	k2eAgMnPc1d1	postižený
nějakou	nějaký	k3yIgFnSc7	nějaký
nemocí	nemoc	k1gFnSc7	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
porfyrie	porfyrie	k1gFnSc1	porfyrie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
citlivostí	citlivost	k1gFnSc7	citlivost
na	na	k7c4	na
světlo	světlo	k1gNnSc4	světlo
<g/>
,	,	kIx,	,
obnažením	obnažení	k1gNnSc7	obnažení
dásní	dáseň	k1gFnPc2	dáseň
(	(	kIx(	(
<g/>
zvýraznění	zvýraznění	k1gNnSc1	zvýraznění
zubů	zub	k1gInPc2	zub
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krevními	krevní	k2eAgFnPc7d1	krevní
sraženinami	sraženina	k1gFnPc7	sraženina
na	na	k7c6	na
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nemoc	nemoc	k1gFnSc4	nemoc
dosti	dosti	k6eAd1	dosti
vzácnou	vzácný	k2eAgFnSc4d1	vzácná
a	a	k8xC	a
nezdá	zdát	k5eNaPmIp3nS	zdát
se	se	k3xPyFc4	se
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
základem	základ	k1gInSc7	základ
mýtu	mýtus	k1gInSc2	mýtus
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
hypotéza	hypotéza	k1gFnSc1	hypotéza
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
moderních	moderní	k2eAgFnPc6d1	moderní
představách	představa	k1gFnPc6	představa
o	o	k7c6	o
upírech	upír	k1gMnPc6	upír
a	a	k8xC	a
neodpovídá	odpovídat	k5eNaImIp3nS	odpovídat
příliš	příliš	k6eAd1	příliš
původní	původní	k2eAgFnSc4d1	původní
mytologii	mytologie	k1gFnSc4	mytologie
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc4	vliv
ovšem	ovšem	k9	ovšem
mohly	moct	k5eAaImAgFnP	moct
mít	mít	k5eAaImF	mít
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
nemoci	nemoc	k1gFnPc4	nemoc
jako	jako	k8xS	jako
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
nebo	nebo	k8xC	nebo
mor	mor	k1gInSc1	mor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
žijí	žít	k5eAaImIp3nP	žít
netopýři	netopýr	k1gMnPc1	netopýr
živící	živící	k2eAgMnPc1d1	živící
se	se	k3xPyFc4	se
krví	krev	k1gFnSc7	krev
teplokrevných	teplokrevný	k2eAgNnPc2d1	teplokrevné
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
většinou	většina	k1gFnSc7	většina
krav	kráva	k1gFnPc2	kráva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
by	by	kYmCp3nP	by
mohli	moct	k5eAaImAgMnP	moct
mít	mít	k5eAaImF	mít
souvislost	souvislost	k1gFnSc4	souvislost
s	s	k7c7	s
legendami	legenda	k1gFnPc7	legenda
o	o	k7c6	o
upírech	upír	k1gMnPc6	upír
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
české	český	k2eAgNnSc1d1	české
pojmenování	pojmenování	k1gNnSc1	pojmenování
je	být	k5eAaImIp3nS	být
upíři	upír	k1gMnPc1	upír
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
ovšem	ovšem	k9	ovšem
žádné	žádný	k3yNgInPc4	žádný
důvody	důvod	k1gInPc4	důvod
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vyskytovali	vyskytovat	k5eAaImAgMnP	vyskytovat
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
mohli	moct	k5eAaImAgMnP	moct
Evropané	Evropan	k1gMnPc1	Evropan
setkat	setkat	k5eAaPmF	setkat
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
španělští	španělský	k2eAgMnPc1d1	španělský
dobyvatelé	dobyvatel	k1gMnPc1	dobyvatel
všimli	všimnout	k5eAaPmAgMnP	všimnout
podobnosti	podobnost	k1gFnPc4	podobnost
těchto	tento	k3xDgNnPc2	tento
zvířat	zvíře	k1gNnPc2	zvíře
s	s	k7c7	s
legendami	legenda	k1gFnPc7	legenda
o	o	k7c6	o
upírech	upír	k1gMnPc6	upír
a	a	k8xC	a
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
je	on	k3xPp3gInPc4	on
podle	podle	k7c2	podle
toho	ten	k3xDgInSc2	ten
<g/>
.	.	kIx.	.
</s>
<s>
Upířími	upíří	k2eAgMnPc7d1	upíří
netopýry	netopýr	k1gMnPc7	netopýr
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
například	například	k6eAd1	například
dokument	dokument	k1gInSc4	dokument
Nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
ostrovy	ostrov	k1gInPc1	ostrov
v	v	k7c6	v
dílu	díl	k1gInSc6	díl
o	o	k7c6	o
Ďábelském	ďábelský	k2eAgInSc6d1	ďábelský
ostrově	ostrov	k1gInSc6	ostrov
<g/>
,	,	kIx,	,
nacházejícím	nacházející	k2eAgMnSc7d1	nacházející
se	se	k3xPyFc4	se
asi	asi	k9	asi
11	[number]	k4	11
km	km	kA	km
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
Guyany	Guyana	k1gFnSc2	Guyana
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Deadly	Deadly	k1gFnSc1	Deadly
Islands	Islandsa	k1gFnPc2	Islandsa
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
série	série	k1gFnSc2	série
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
"	"	kIx"	"
<g/>
Čertův	čertův	k2eAgInSc1d1	čertův
ostrov	ostrov	k1gInSc1	ostrov
<g/>
"	"	kIx"	"
-	-	kIx~	-
možná	možná	k9	možná
nesprávný	správný	k2eNgInSc4d1	nesprávný
překlad	překlad	k1gInSc4	překlad
názvu	název	k1gInSc2	název
dabingovým	dabingový	k2eAgInSc7d1	dabingový
týmem	tým	k1gInSc7	tým
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
průvodce	průvodce	k1gMnSc1	průvodce
pořadem	pořad	k1gInSc7	pořad
Dave	Dav	k1gInSc5	Dav
Salmoni	Salmon	k1gMnPc1	Salmon
pokusí	pokusit	k5eAaPmIp3nS	pokusit
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
těmi	ten	k3xDgFnPc7	ten
pověstmi	pověst	k1gFnPc7	pověst
o	o	k7c6	o
netopýrech	netopýr	k1gMnPc6	netopýr
sající	sající	k2eAgFnSc1d1	sající
krev	krev	k1gFnSc1	krev
vězňů	vězeň	k1gMnPc2	vězeň
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
vězeňské	vězeňský	k2eAgFnSc6d1	vězeňská
cele	cela	k1gFnSc6	cela
zkusí	zkusit	k5eAaPmIp3nP	zkusit
přespat	přespat	k5eAaPmF	přespat
<g/>
.	.	kIx.	.
</s>
<s>
Netopýři	netopýr	k1gMnPc1	netopýr
na	na	k7c4	na
něj	on	k3xPp3gInSc4	on
začnou	začít	k5eAaPmIp3nP	začít
létat	létat	k5eAaImF	létat
<g/>
,	,	kIx,	,
kousnou	kousnout	k5eAaPmIp3nP	kousnout
jej	on	k3xPp3gNnSc4	on
do	do	k7c2	do
nohy	noha	k1gFnSc2	noha
a	a	k8xC	a
lížou	lízat	k5eAaImIp3nP	lízat
vytékající	vytékající	k2eAgFnSc4d1	vytékající
krev	krev	k1gFnSc4	krev
<g/>
.	.	kIx.	.
</s>
<s>
Vlkodlak	vlkodlak	k1gMnSc1	vlkodlak
Noční	noční	k2eAgFnSc2d1	noční
můra	můra	k1gFnSc1	můra
Maiello	Maiello	k1gNnSc4	Maiello
<g/>
,	,	kIx,	,
Giuseppe	Giusepp	k1gMnSc5	Giusepp
<g/>
:	:	kIx,	:
Vampyrismus	vampyrismus	k1gInSc4	vampyrismus
v	v	k7c6	v
kulturních	kulturní	k2eAgFnPc6d1	kulturní
dějinách	dějiny	k1gFnPc6	dějiny
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
190	[number]	k4	190
s.	s.	k?	s.
Krumphanzlová	Krumphanzlová	k1gFnSc1	Krumphanzlová
<g/>
,	,	kIx,	,
Z.	Z.	kA	Z.
1961	[number]	k4	1961
<g/>
:	:	kIx,	:
K	k	k7c3	k
otázce	otázka	k1gFnSc3	otázka
vampyrismu	vampyrismus	k1gInSc2	vampyrismus
na	na	k7c6	na
slovanských	slovanský	k2eAgNnPc6d1	slovanské
pohřebištích	pohřebiště	k1gNnPc6	pohřebiště
<g/>
.	.	kIx.	.
</s>
<s>
Památky	památka	k1gFnPc1	památka
archeologické	archeologický	k2eAgFnPc1d1	archeologická
<g/>
,	,	kIx,	,
544	[number]	k4	544
<g/>
-	-	kIx~	-
<g/>
549	[number]	k4	549
<g/>
.	.	kIx.	.
</s>
<s>
Navrátilová	Navrátilová	k1gFnSc1	Navrátilová
<g/>
,	,	kIx,	,
A.	A.	kA	A.
1996	[number]	k4	1996
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nečistí	čistý	k2eNgMnPc1d1	nečistý
zemřelí	zemřelý	k1gMnPc1	zemřelý
<g/>
"	"	kIx"	"
v	v	k7c6	v
posmrtných	posmrtný	k2eAgInPc6d1	posmrtný
a	a	k8xC	a
pohřebních	pohřební	k2eAgInPc6d1	pohřební
rituálech	rituál	k1gInPc6	rituál
českého	český	k2eAgInSc2d1	český
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
lid	lid	k1gInSc1	lid
83	[number]	k4	83
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
-	-	kIx~	-
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
</s>
<s>
Wollmann	Wollmann	k1gMnSc1	Wollmann
<g/>
,	,	kIx,	,
F.	F.	kA	F.
1920	[number]	k4	1920
<g/>
-	-	kIx~	-
<g/>
1921	[number]	k4	1921
<g/>
:	:	kIx,	:
Vampyrické	Vampyrický	k2eAgFnSc2d1	Vampyrický
pověsti	pověst	k1gFnSc2	pověst
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
středoevropské	středoevropský	k2eAgFnSc6d1	středoevropská
<g/>
.	.	kIx.	.
</s>
<s>
Národopisný	národopisný	k2eAgMnSc1d1	národopisný
věstník	věstník	k1gMnSc1	věstník
českoslovanský	českoslovanský	k2eAgMnSc1d1	českoslovanský
14	[number]	k4	14
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
KONSTANTINOS	KONSTANTINOS	kA	KONSTANTINOS
<g/>
.	.	kIx.	.
</s>
<s>
Upíři	upír	k1gMnPc1	upír
žijí	žít	k5eAaImIp3nP	žít
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
638	[number]	k4	638
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
STEJSKAL	Stejskal	k1gMnSc1	Stejskal
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Labyrintem	labyrint	k1gInSc7	labyrint
tajemna	tajemno	k1gNnSc2	tajemno
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
54	[number]	k4	54
<g/>
-	-	kIx~	-
<g/>
55	[number]	k4	55
<g/>
.	.	kIx.	.
</s>
<s>
STEJSKAL	Stejskal	k1gMnSc1	Stejskal
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Labyrintem	labyrint	k1gInSc7	labyrint
míst	místo	k1gNnPc2	místo
klatých	klatý	k2eAgMnPc2d1	klatý
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Eminent	Eminent	k1gMnSc1	Eminent
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
73	[number]	k4	73
<g/>
-	-	kIx~	-
<g/>
74	[number]	k4	74
<g/>
.	.	kIx.	.
</s>
<s>
VONDRÁČEK	Vondráček	k1gMnSc1	Vondráček
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Fantastické	fantastický	k2eAgInPc1d1	fantastický
a	a	k8xC	a
magické	magický	k2eAgInPc1d1	magický
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
psychiatrie	psychiatrie	k1gFnSc2	psychiatrie
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
:	:	kIx,	:
Columbus	Columbus	k1gMnSc1	Columbus
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
196	[number]	k4	196
<g/>
-	-	kIx~	-
<g/>
201	[number]	k4	201
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
upír	upír	k1gMnSc1	upír
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
upír	upír	k1gMnSc1	upír
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gNnSc1	téma
Upír	upír	k1gMnSc1	upír
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Upíři	upír	k1gMnPc1	upír
<g/>
:	:	kIx,	:
od	od	k7c2	od
mrtvol	mrtvola	k1gFnPc2	mrtvola
k	k	k7c3	k
supermanům	superman	k1gMnPc3	superman
</s>
