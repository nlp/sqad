<s>
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evžen	Evžen	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc5d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evžen	Evžen	k1gMnSc1
Württemberský	Württemberský	k2eAgInSc4d1
</s>
<s>
württemberský	württemberský	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
</s>
<s>
Doba	doba	k1gFnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1795	#num#	k4
–	–	k?
23	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1797	#num#	k4
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1732	#num#	k4
</s>
<s>
Stuttgart	Stuttgart	k1gInSc1
</s>
<s>
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1797	#num#	k4
(	(	kIx(
<g/>
65	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
</s>
<s>
Hohenheim	Hohenheim	k6eAd1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
Evžen	Evžen	k1gMnSc1
</s>
<s>
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Manželka	manželka	k1gFnSc1
</s>
<s>
Bedřiška	Bedřiška	k1gFnSc1
Braniborsko-Schwedtská	Braniborsko-Schwedtský	k2eAgFnSc1d1
</s>
<s>
Dynastie	dynastie	k1gFnSc1
</s>
<s>
Württemberkové	Württemberkové	k2eAgFnSc1d1
</s>
<s>
Otec	otec	k1gMnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
</s>
<s>
Matka	matka	k1gFnSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Augusta	August	k1gMnSc2
Thurn-Taxis	Thurn-Taxis	k1gFnSc2
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evžen	Evžen	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1732	#num#	k4
<g/>
,	,	kIx,
Stuttgart	Stuttgart	k1gInSc1
–	–	k?
23	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1797	#num#	k4
<g/>
,	,	kIx,
Hohenheim	Hohenheim	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
jako	jako	k9
čtvrtý	čtvrtý	k4xOgMnSc1
syn	syn	k1gMnSc1
vévody	vévoda	k1gMnSc2
Karla	Karel	k1gMnSc2
Alexandra	Alexandr	k1gMnSc2
Württemberského	Württemberský	k2eAgMnSc2d1
a	a	k8xC
princezny	princezna	k1gFnSc2
Marie	Maria	k1gFnSc2
Augusty	Augusta	k1gMnSc2
Thurn-Taxisové	Thurn-Taxisový	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1795	#num#	k4
až	až	k9
1797	#num#	k4
vládl	vládnout	k5eAaImAgInS
jako	jako	k9
württemberský	württemberský	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Voják	voják	k1gMnSc1
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
v	v	k7c6
Sedmileté	sedmiletý	k2eAgFnSc6d1
válce	válka	k1gFnSc6
sloužil	sloužit	k5eAaImAgMnS
pruskému	pruský	k2eAgMnSc3d1
králi	král	k1gMnSc3
Fridrichovi	Fridrich	k1gMnSc3
Velikému	veliký	k2eAgMnSc3d1
<g/>
,	,	kIx,
usadil	usadit	k5eAaPmAgInS
se	se	k3xPyFc4
v	v	k7c6
roce	rok	k1gInSc6
1769	#num#	k4
v	v	k7c6
rodinné	rodinný	k2eAgFnSc6d1
enklávě	enkláva	k1gFnSc6
<g/>
,	,	kIx,
hrabství	hrabství	k1gNnSc1
Montbéliard	Montbéliard	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
byl	být	k5eAaImAgInS
také	také	k9
v	v	k7c6
březnu	březen	k1gInSc6
1786	#num#	k4
<g/>
,	,	kIx,
starším	starý	k2eAgMnSc7d2
bratrem	bratr	k1gMnSc7
Karlem	Karel	k1gMnSc7
Evženem	Evžen	k1gMnSc7
<g/>
,	,	kIx,
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
generálporučíkem	generálporučík	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1779	#num#	k4
koupil	koupit	k5eAaPmAgMnS
Fridrich	Fridrich	k1gMnSc1
hrad	hrad	k1gInSc1
a	a	k8xC
panství	panství	k1gNnSc1
Hochberg	Hochberg	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1791	#num#	k4
je	být	k5eAaImIp3nS
však	však	k9
prodal	prodat	k5eAaPmAgInS
bratrovi	bratr	k1gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
jej	on	k3xPp3gMnSc4
král	král	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Vilém	Vilém	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
jmenoval	jmenovat	k5eAaImAgMnS,k5eAaBmAgMnS
guvernérem	guvernér	k1gMnSc7
markrabství	markrabství	k1gNnSc2
Ansbach-Bayreuth	Ansbach-Bayreuth	k1gMnSc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
mu	on	k3xPp3gMnSc3
prodal	prodat	k5eAaPmAgMnS
poslední	poslední	k2eAgMnSc1d1
kníže	kníže	k1gMnSc1
oné	onen	k3xDgFnSc2
větve	větev	k1gFnSc2
rodu	rod	k1gInSc2
Hohenzollernů	Hohenzollern	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Montbéliard	Montbéliard	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1792	#num#	k4
převzala	převzít	k5eAaPmAgFnS
krátkodobá	krátkodobý	k2eAgFnSc1d1
Reuraciánská	Reuraciánský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1793	#num#	k4
bylo	být	k5eAaImAgNnS
hrabství	hrabství	k1gNnSc1
anektováno	anektovat	k5eAaBmNgNnS
Francouzskou	francouzský	k2eAgFnSc7d1
republikou	republika	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Vévoda	vévoda	k1gMnSc1
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1
starší	starý	k2eAgMnSc1d2
bratr	bratr	k1gMnSc1
měl	mít	k5eAaImAgMnS
pouze	pouze	k6eAd1
dcery	dcera	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
po	po	k7c6
smrti	smrt	k1gFnSc6
Karla	Karel	k1gMnSc2
Alexandra	Alexandr	k1gMnSc2
stal	stát	k5eAaPmAgMnS
württemberským	württemberský	k2eAgMnSc7d1
vévodou	vévoda	k1gMnSc7
jejich	jejich	k3xOp3gMnSc1
další	další	k2eAgMnSc1d1
bratr	bratr	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Evžen	Evžen	k1gMnSc1
(	(	kIx(
<g/>
1731	#num#	k4
<g/>
–	–	k?
<g/>
1795	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
ten	ten	k3xDgMnSc1
však	však	k9
o	o	k7c4
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
později	pozdě	k6eAd2
zemřel	zemřít	k5eAaPmAgMnS
a	a	k8xC
Fridrich	Fridrich	k1gMnSc1
Evžen	Evžen	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
na	na	k7c4
následující	následující	k2eAgInPc4d1
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
,	,	kIx,
než	než	k8xS
zemřel	zemřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
württemberským	württemberský	k2eAgMnSc7d1
vévodou	vévoda	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1796	#num#	k4
přijal	přijmout	k5eAaPmAgInS
pařížský	pařížský	k2eAgInSc1d1
mír	mír	k1gInSc1
s	s	k7c7
revoluční	revoluční	k2eAgFnSc7d1
Francií	Francie	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
zamítla	zamítnout	k5eAaPmAgFnS
veškeré	veškerý	k3xTgInPc4
jeho	jeho	k3xOp3gInPc4
nároky	nárok	k1gInPc4
na	na	k7c4
Montbéliard	Montbéliard	k1gInSc4
a	a	k8xC
všechna	všechen	k3xTgNnPc1
ostatní	ostatní	k2eAgNnPc1d1
území	území	k1gNnPc1
na	na	k7c6
levém	levý	k2eAgInSc6d1
břehu	břeh	k1gInSc6
Rýna	Rýn	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Manželství	manželství	k1gNnSc1
a	a	k8xC
potomci	potomek	k1gMnPc1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Evžen	Evžen	k1gMnSc1
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Bedřiškou	Bedřiška	k1gFnSc7
Žofií	Žofie	k1gFnSc7
Dorotou	Dorota	k1gFnSc7
Braniborsko-Schwedtskou	Braniborsko-Schwedtský	k2eAgFnSc7d1
<g/>
,	,	kIx,
neteří	neteř	k1gFnSc7
Fridricha	Fridrich	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velikého	veliký	k2eAgMnSc4d1
<g/>
,	,	kIx,
se	s	k7c7
kterou	který	k3yRgFnSc7,k3yQgFnSc7,k3yIgFnSc7
měl	mít	k5eAaImAgMnS
dvanáct	dvanáct	k4xCc4
potomkůː	potomkůː	k?
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
I.	I.	kA
Württemberský	Württemberský	k2eAgMnSc1d1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1754	#num#	k4
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1816	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
otcův	otcův	k2eAgMnSc1d1
nástupce	nástupce	k1gMnSc1
a	a	k8xC
později	pozdě	k6eAd2
první	první	k4xOgMnSc1
württemberský	württemberský	k2eAgMnSc1d1
král	král	k1gMnSc1
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
(	(	kIx(
<g/>
30	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1756	#num#	k4
–	–	k?
20	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1817	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
velitel	velitel	k1gMnSc1
litevské	litevský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
</s>
<s>
Evžen	Evžen	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1758	#num#	k4
–	–	k?
20	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1822	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Žofie	Žofie	k1gFnSc1
Dorota	Dorota	k1gFnSc1
Württemberská	Württemberský	k2eAgFnSc1d1
(	(	kIx(
<g/>
25	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1759	#num#	k4
–	–	k?
5	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1828	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
manželka	manželka	k1gFnSc1
cara	car	k1gMnSc2
Pavla	Pavel	k1gMnSc2
I.	I.	kA
</s>
<s>
Vilém	Vilém	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Filip	Filip	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
(	(	kIx(
<g/>
27	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1761	#num#	k4
–	–	k?
10	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1830	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
otec	otec	k1gMnSc1
Viléma	Vilém	k1gMnSc2
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
vévody	vévoda	k1gMnPc4
z	z	k7c2
Urachu	Urach	k1gInSc2
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
August	August	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1763	#num#	k4
–	–	k?
20	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1834	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
oženil	oženit	k5eAaPmAgMnS
se	se	k3xPyFc4
s	s	k7c7
Kunhutou	Kunhuta	k1gFnSc7
Metternichovou	Metternichová	k1gFnSc7
<g/>
,	,	kIx,
sestrou	sestra	k1gFnSc7
knížete	kníže	k1gMnSc2
Metternicha	Metternich	k1gMnSc2
</s>
<s>
Bedřiška	Bedřiška	k1gFnSc1
Alžběta	Alžběta	k1gFnSc1
Amálie	Amálie	k1gFnSc1
Württemberská	Württemberský	k2eAgFnSc1d1
(	(	kIx(
<g/>
27	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1765	#num#	k4
–	–	k?
24	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1785	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
manželka	manželka	k1gFnSc1
velkovévody	velkovévoda	k1gMnSc2
Petra	Petr	k1gMnSc2
Oldenburského	oldenburský	k2eAgMnSc2d1
</s>
<s>
Alžběta	Alžběta	k1gFnSc1
Vilemína	Vilemína	k1gFnSc1
Luisa	Luisa	k1gFnSc1
Württemberská	Württemberský	k2eAgFnSc1d1
(	(	kIx(
<g/>
21	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1767	#num#	k4
–	–	k?
18	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1790	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
manželka	manželka	k1gFnSc1
budoucího	budoucí	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
Františka	František	k1gMnSc2
I.	I.	kA
Rakouského	rakouský	k2eAgInSc2d1
</s>
<s>
Frederika	Frederika	k1gFnSc1
Vilemína	Vilemína	k1gFnSc1
Kateřina	Kateřina	k1gFnSc1
Württemberská	Württemberský	k2eAgFnSc1d1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1768	#num#	k4
–	–	k?
22	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1768	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Karel	Karel	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1770	#num#	k4
–	–	k?
22	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1791	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Alexandr	Alexandr	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
Karel	Karel	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1771	#num#	k4
–	–	k?
4	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1833	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
založil	založit	k5eAaPmAgInS
novou	nový	k2eAgFnSc4d1
větev	větev	k1gFnSc4
rodu	rod	k1gInSc2
</s>
<s>
Karel	Karel	k1gMnSc1
Jindřich	Jindřich	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1772	#num#	k4
–	–	k?
28	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1833	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1798	#num#	k4
se	se	k3xPyFc4
morganaticky	morganaticky	k6eAd1
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
Kristýnou	Kristýna	k1gFnSc7
Karolínou	Karolína	k1gFnSc7
Alexeï	Alexeï	k1gFnSc7
(	(	kIx(
<g/>
1779	#num#	k4
<g/>
–	–	k?
<g/>
1853	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vévoda	vévoda	k1gMnSc1
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evžen	Evžen	k1gMnSc1
zemřel	zemřít	k5eAaPmAgMnS
v	v	k7c6
Hohenheimu	Hohenheim	k1gInSc6
23	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1797	#num#	k4
ve	v	k7c6
věku	věk	k1gInSc6
65	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Předkové	předek	k1gMnPc1
</s>
<s>
Eberhard	Eberhard	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Württemberský	Württemberský	k2eAgInSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Karel	Karel	k1gMnSc1
Württembersko-Winnentalský	Württembersko-Winnentalský	k2eAgMnSc1d1
</s>
<s>
Anna	Anna	k1gFnSc1
Kateřina	Kateřina	k1gFnSc1
ze	z	k7c2
Salm-Kyrburgu	Salm-Kyrburg	k1gInSc2
</s>
<s>
Karel	Karel	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
</s>
<s>
Albrecht	Albrecht	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Braniborsko-Ansbašský	Braniborsko-Ansbašský	k2eAgInSc1d1
</s>
<s>
Eleonora	Eleonora	k1gFnSc1
Juliana	Julian	k1gMnSc2
Braniborsko-Ansbašská	Braniborsko-Ansbašský	k2eAgNnPc1d1
</s>
<s>
Žofie	Žofie	k1gFnSc1
Markéta	Markéta	k1gFnSc1
Oettingenská	Oettingenský	k2eAgFnSc1d1
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evžen	Evžen	k1gMnSc1
Württemberský	Württemberský	k2eAgInSc5d1
</s>
<s>
Evžen	Evžen	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
František	František	k1gMnSc1
Thurn-Taxis	Thurn-Taxis	k1gInSc4
</s>
<s>
Anselm	Anselm	k6eAd1
František	František	k1gMnSc1
Thurn-Taxis	Thurn-Taxis	k1gFnSc2
</s>
<s>
Anna	Anna	k1gFnSc1
Adelaida	Adelaida	k1gFnSc1
Fürstenbersko-Heiligenberská	Fürstenbersko-Heiligenberský	k2eAgFnSc1d1
</s>
<s>
Marie	Marie	k1gFnSc1
Augusta	August	k1gMnSc2
Thurn-Taxis	Thurn-Taxis	k1gFnSc2
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1
August	August	k1gMnSc1
z	z	k7c2
Lobkovic	Lobkovice	k1gInPc2
</s>
<s>
Marie	Marie	k1gFnSc1
Ludovika	Ludovika	k1gFnSc1
z	z	k7c2
Lobkovic	Lobkovice	k1gInPc2
</s>
<s>
Marie	Marie	k1gFnSc1
Anna	Anna	k1gFnSc1
Badenská	Badenský	k2eAgFnSc1d1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Frederick	Fredericka	k1gFnPc2
II	II	kA
Eugene	Eugen	k1gInSc5
<g/>
,	,	kIx,
Duke	Duke	k1gNnSc3
of	of	k?
Württemberg	Württemberg	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evžen	Evžen	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Württemberský	Württemberský	k2eAgMnSc1d1
vévoda	vévoda	k1gMnSc1
</s>
<s>
Předchůdce	předchůdce	k1gMnSc1
<g/>
:	:	kIx,
Ludvík	Ludvík	k1gMnSc1
Evžen	Evžen	k1gMnSc1
</s>
<s>
1795	#num#	k4
<g/>
–	–	k?
<g/>
1797	#num#	k4
Fridrich	Fridrich	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evžen	Evžen	k1gMnSc1
Württemberský	Württemberský	k2eAgMnSc1d1
</s>
<s>
Nástupce	nástupce	k1gMnSc1
<g/>
:	:	kIx,
Fridrich	Fridrich	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
117753416	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
7735	#num#	k4
4158	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2001028839	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
57400464	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2001028839	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
Německo	Německo	k1gNnSc1
|	|	kIx~
Novověk	novověk	k1gInSc1
|	|	kIx~
Velká	velký	k2eAgFnSc1d1
francouzská	francouzský	k2eAgFnSc1d1
revoluce	revoluce	k1gFnSc1
</s>
