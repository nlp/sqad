<p>
<s>
Jankovští	Jankovský	k2eAgMnPc1d1	Jankovský
z	z	k7c2	z
Vlašimi	Vlašim	k1gFnSc2	Vlašim
jsou	být	k5eAaImIp3nP	být
někdejší	někdejší	k2eAgInSc4d1	někdejší
rytířský	rytířský	k2eAgInSc4d1	rytířský
a	a	k8xC	a
panský	panský	k2eAgInSc4d1	panský
český	český	k2eAgInSc4d1	český
rod	rod	k1gInSc4	rod
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgInSc4	svůj
přídomek	přídomek	k1gInSc4	přídomek
odvozoval	odvozovat	k5eAaImAgMnS	odvozovat
od	od	k7c2	od
středočeského	středočeský	k2eAgNnSc2d1	Středočeské
města	město	k1gNnSc2	město
Vlašimi	Vlašim	k1gFnSc2	Vlašim
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
na	na	k7c4	na
několik	několik	k4yIc4	několik
samostatných	samostatný	k2eAgFnPc2d1	samostatná
větví	větev	k1gFnPc2	větev
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
přijaly	přijmout	k5eAaPmAgFnP	přijmout
vlastní	vlastní	k2eAgInSc4d1	vlastní
přídomek	přídomek	k1gInSc4	přídomek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Počátky	počátek	k1gInPc1	počátek
rodu	rod	k1gInSc2	rod
===	===	k?	===
</s>
</p>
<p>
<s>
Jaroš	Jaroš	k1gMnSc1	Jaroš
a	a	k8xC	a
Mařík	Mařík	k1gMnSc1	Mařík
provázeli	provázet	k5eAaImAgMnP	provázet
na	na	k7c6	na
výpravách	výprava	k1gFnPc6	výprava
na	na	k7c4	na
evropská	evropský	k2eAgNnPc4d1	Evropské
bojiště	bojiště	k1gNnPc4	bojiště
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
<g/>
,	,	kIx,	,
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
bitev	bitva	k1gFnPc2	bitva
byli	být	k5eAaImAgMnP	být
pasováni	pasován	k2eAgMnPc1d1	pasován
na	na	k7c4	na
rytíře	rytíř	k1gMnPc4	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
dalších	další	k2eAgMnPc6d1	další
pánech	pan	k1gMnPc6	pan
z	z	k7c2	z
Vlašimi	Vlašim	k1gFnSc2	Vlašim
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
o	o	k7c6	o
bratrech	bratr	k1gMnPc6	bratr
Michalovi	Michal	k1gMnSc3	Michal
<g/>
,	,	kIx,	,
Pavlovi	Pavel	k1gMnSc3	Pavel
a	a	k8xC	a
Janu	Jan	k1gMnSc3	Jan
Očkovi	Očka	k1gMnSc3	Očka
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
druhým	druhý	k4xOgMnSc7	druhý
pražským	pražský	k2eAgMnSc7d1	pražský
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
<g/>
.	.	kIx.	.
</s>
<s>
Matěj	Matěj	k1gMnSc1	Matěj
z	z	k7c2	z
Vlašimi	Vlašim	k1gFnSc2	Vlašim
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
praotce	praotec	k1gMnPc4	praotec
jankovské	jankovský	k2eAgFnSc2d1	jankovská
větve	větev	k1gFnSc2	větev
<g/>
.	.	kIx.	.
</s>
<s>
Očkův	Očkův	k2eAgMnSc1d1	Očkův
bratr	bratr	k1gMnSc1	bratr
Pavel	Pavel	k1gMnSc1	Pavel
vlastnil	vlastnit	k5eAaImAgMnS	vlastnit
hrad	hrad	k1gInSc4	hrad
Jenštejn	Jenštejn	k1gMnSc1	Jenštejn
a	a	k8xC	a
založil	založit	k5eAaPmAgMnS	založit
jenštejnskou	jenštejnský	k2eAgFnSc4d1	jenštejnský
větev	větev	k1gFnSc4	větev
<g/>
.	.	kIx.	.
</s>
<s>
Maříkův	Maříkův	k2eAgMnSc1d1	Maříkův
syn	syn	k1gMnSc1	syn
Hron	Hron	k1gMnSc1	Hron
padl	padnout	k5eAaPmAgMnS	padnout
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
králem	král	k1gMnSc7	král
Janem	Jan	k1gMnSc7	Jan
Lucemburským	lucemburský	k2eAgMnSc7d1	lucemburský
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Kresčaku	Kresčak	k1gInSc2	Kresčak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samotný	samotný	k2eAgMnSc1d1	samotný
Jan	Jan	k1gMnSc1	Jan
Očko	očko	k1gNnSc4	očko
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
tajemníkem	tajemník	k1gMnSc7	tajemník
Karla	Karel	k1gMnSc2	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
a	a	k8xC	a
poté	poté	k6eAd1	poté
olomouckým	olomoucký	k2eAgMnSc7d1	olomoucký
biskupem	biskup	k1gMnSc7	biskup
<g/>
,	,	kIx,	,
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
císaře	císař	k1gMnSc4	císař
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
po	po	k7c6	po
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Arnošta	Arnošt	k1gMnSc2	Arnošt
z	z	k7c2	z
Pardubic	Pardubice	k1gInPc2	Pardubice
jej	on	k3xPp3gInSc2	on
roku	rok	k1gInSc2	rok
1364	[number]	k4	1364
zvolili	zvolit	k5eAaPmAgMnP	zvolit
pražským	pražský	k2eAgMnSc7d1	pražský
arcibiskupem	arcibiskup	k1gMnSc7	arcibiskup
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nepřítomnosti	nepřítomnost	k1gFnSc6	nepřítomnost
panovníka	panovník	k1gMnSc2	panovník
spravoval	spravovat	k5eAaImAgMnS	spravovat
zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
poručníkem	poručník	k1gMnSc7	poručník
Václava	Václav	k1gMnSc4	Václav
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
budování	budování	k1gNnSc6	budování
roudnického	roudnický	k2eAgInSc2d1	roudnický
hradu	hrad	k1gInSc2	hrad
i	i	k8xC	i
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zdejší	zdejší	k2eAgFnSc6d1	zdejší
zámecké	zámecký	k2eAgFnSc6d1	zámecká
kapli	kaple	k1gFnSc6	kaple
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
slavný	slavný	k2eAgInSc1d1	slavný
votivní	votivní	k2eAgInSc1d1	votivní
obraz	obraz	k1gInSc1	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1378	[number]	k4	1378
jej	on	k3xPp3gInSc4	on
papež	papež	k1gMnSc1	papež
Urban	Urban	k1gMnSc1	Urban
VI	VI	kA	VI
<g/>
.	.	kIx.	.
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
kardinálem	kardinál	k1gMnSc7	kardinál
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
prvního	první	k4xOgMnSc2	první
Čecha	Čech	k1gMnSc2	Čech
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1379	[number]	k4	1379
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
postu	post	k1gInSc2	post
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
příbuzného	příbuzný	k1gMnSc2	příbuzný
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Jenštejna	Jenštejn	k1gInSc2	Jenštejn
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Moravská	moravský	k2eAgNnPc1d1	Moravské
panství	panství	k1gNnPc1	panství
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Jankovští	Jankovský	k2eAgMnPc1d1	Jankovský
prodali	prodat	k5eAaPmAgMnP	prodat
statky	statek	k1gInPc4	statek
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
usídlili	usídlit	k5eAaPmAgMnP	usídlit
se	se	k3xPyFc4	se
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Jankovský	Jankovský	k2eAgMnSc1d1	Jankovský
z	z	k7c2	z
Vlašimi	Vlašim	k1gFnSc2	Vlašim
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1405	[number]	k4	1405
členem	člen	k1gInSc7	člen
zemského	zemský	k2eAgInSc2d1	zemský
soudu	soud	k1gInSc2	soud
a	a	k8xC	a
za	za	k7c4	za
své	svůj	k3xOyFgFnPc4	svůj
služby	služba	k1gFnPc4	služba
obdržel	obdržet	k5eAaPmAgMnS	obdržet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1416	[number]	k4	1416
od	od	k7c2	od
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
zámek	zámek	k1gInSc1	zámek
Úsov	Úsovum	k1gNnPc2	Úsovum
<g/>
.	.	kIx.	.
</s>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Úsovský	Úsovský	k2eAgMnSc1d1	Úsovský
v	v	k7c6	v
letech	let	k1gInPc6	let
1516	[number]	k4	1516
<g/>
–	–	k?	–
<g/>
1520	[number]	k4	1520
působil	působit	k5eAaImAgInS	působit
jako	jako	k8xC	jako
podkomoří	podkomoří	k1gMnPc1	podkomoří
markrabství	markrabství	k1gNnSc2	markrabství
moravského	moravský	k2eAgNnSc2d1	Moravské
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1615	[number]	k4	1615
byli	být	k5eAaImAgMnP	být
povýšeni	povýšen	k2eAgMnPc1d1	povýšen
do	do	k7c2	do
panského	panský	k2eAgInSc2d1	panský
stavu	stav	k1gInSc2	stav
bratři	bratr	k1gMnPc1	bratr
Fridrich	Fridrich	k1gMnSc1	Fridrich
a	a	k8xC	a
Volf	Volf	k1gMnSc1	Volf
Zikmund	Zikmund	k1gMnSc1	Zikmund
Jankovský	Jankovský	k2eAgMnSc1d1	Jankovský
z	z	k7c2	z
Vlašimi	Vlašim	k1gFnSc2	Vlašim
z	z	k7c2	z
bítovské	bítovský	k2eAgFnSc2d1	bítovská
větve	větev	k1gFnSc2	větev
a	a	k8xC	a
bratři	bratr	k1gMnPc1	bratr
Adam	Adam	k1gMnSc1	Adam
a	a	k8xC	a
Jiřík	Jiřík	k1gMnSc1	Jiřík
Jankovský	Jankovský	k2eAgMnSc1d1	Jankovský
z	z	k7c2	z
Vlašimi	Vlašim	k1gFnSc2	Vlašim
z	z	k7c2	z
linie	linie	k1gFnSc2	linie
rešické	rešická	k1gFnSc2	rešická
<g/>
.	.	kIx.	.
</s>
<s>
Volfa	Volf	k1gMnSc4	Volf
Zikmunda	Zikmund	k1gMnSc4	Zikmund
zvolili	zvolit	k5eAaPmAgMnP	zvolit
za	za	k7c2	za
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
povstání	povstání	k1gNnSc2	povstání
dodatečně	dodatečně	k6eAd1	dodatečně
členem	člen	k1gInSc7	člen
direktorské	direktorský	k2eAgFnSc2d1	direktorský
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
se	se	k3xPyFc4	se
stačil	stačit	k5eAaBmAgInS	stačit
zodpovědět	zodpovědět	k5eAaPmF	zodpovědět
císaři	císař	k1gMnSc3	císař
Ferdinandovi	Ferdinand	k1gMnSc3	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
účasti	účast	k1gFnSc2	účast
na	na	k7c6	na
povstání	povstání	k1gNnSc6	povstání
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1622	[number]	k4	1622
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Fridrich	Fridrich	k1gMnSc1	Fridrich
bojoval	bojovat	k5eAaImAgMnS	bojovat
na	na	k7c6	na
císařově	císařův	k2eAgFnSc6d1	císařova
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
k	k	k7c3	k
bítovskému	bítovský	k2eAgNnSc3d1	bítovský
panství	panství	k1gNnSc3	panství
poté	poté	k6eAd1	poté
koupil	koupit	k5eAaPmAgMnS	koupit
i	i	k8xC	i
Jemnici	Jemnice	k1gFnSc4	Jemnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fridrichův	Fridrichův	k2eAgMnSc1d1	Fridrichův
syn	syn	k1gMnSc1	syn
Hynek	Hynek	k1gMnSc1	Hynek
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
císařským	císařský	k2eAgMnSc7d1	císařský
radou	rada	k1gMnSc7	rada
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Císařský	císařský	k2eAgMnSc1d1	císařský
komorník	komorník	k1gMnSc1	komorník
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Arnošt	Arnošt	k1gMnSc1	Arnošt
II	II	kA	II
<g/>
.	.	kIx.	.
z	z	k7c2	z
Vlašimi	Vlašim	k1gFnSc2	Vlašim
(	(	kIx(	(
<g/>
1665	[number]	k4	1665
<g/>
–	–	k?	–
<g/>
1736	[number]	k4	1736
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
studií	studio	k1gNnPc2	studio
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
přátelil	přátelit	k5eAaImAgMnS	přátelit
s	s	k7c7	s
Josefem	Josef	k1gMnSc7	Josef
I.	I.	kA	I.
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
tajným	tajný	k2eAgMnSc7d1	tajný
dvorním	dvorní	k2eAgMnSc7d1	dvorní
radou	rada	k1gMnSc7	rada
<g/>
.	.	kIx.	.
</s>
<s>
Oženil	oženit	k5eAaPmAgMnS	oženit
se	se	k3xPyFc4	se
s	s	k7c7	s
vdovou	vdova	k1gFnSc7	vdova
po	po	k7c6	po
Adamovi	Adam	k1gMnSc6	Adam
hraběti	hrabě	k1gMnSc6	hrabě
Zrinském	Zrinský	k1gMnSc6	Zrinský
<g/>
,	,	kIx,	,
Marií	Maria	k1gFnSc7	Maria
Kateřinou	Kateřina	k1gFnSc7	Kateřina
<g/>
,	,	kIx,	,
rozenou	rozený	k2eAgFnSc7d1	rozená
hraběnkou	hraběnka	k1gFnSc7	hraběnka
z	z	k7c2	z
Lamberka	Lamberka	k1gFnSc1	Lamberka
(	(	kIx(	(
<g/>
1664	[number]	k4	1664
<g/>
–	–	k?	–
<g/>
1717	[number]	k4	1717
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
sňatku	sňatek	k1gInSc3	sňatek
získal	získat	k5eAaPmAgMnS	získat
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
knihovnu	knihovna	k1gFnSc4	knihovna
rodu	rod	k1gInSc2	rod
Zrinských	Zrinský	k1gMnPc2	Zrinský
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
na	na	k7c4	na
Bítov	Bítov	k1gInSc4	Bítov
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
dcery	dcera	k1gFnPc4	dcera
provdal	provdat	k5eAaPmAgMnS	provdat
za	za	k7c4	za
členy	člen	k1gInPc4	člen
rodů	rod	k1gInPc2	rod
Daunů	Daun	k1gMnPc2	Daun
a	a	k8xC	a
Kouniců	Kounice	k1gMnPc2	Kounice
<g/>
.	.	kIx.	.
</s>
<s>
Snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
rod	rod	k1gInSc4	rod
získat	získat	k5eAaPmF	získat
titul	titul	k1gInSc4	titul
říšského	říšský	k2eAgMnSc2d1	říšský
hraběte	hrabě	k1gMnSc2	hrabě
<g/>
,	,	kIx,	,
nashromáždil	nashromáždit	k5eAaPmAgMnS	nashromáždit
mnoho	mnoho	k4c4	mnoho
důkazů	důkaz	k1gInPc2	důkaz
ze	z	k7c2	z
starých	starý	k2eAgFnPc2d1	stará
listin	listina	k1gFnPc2	listina
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
dokumentů	dokument	k1gInPc2	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Jestli	jestli	k8xS	jestli
tato	tento	k3xDgFnSc1	tento
snaha	snaha	k1gFnSc1	snaha
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
cíli	cíl	k1gInSc3	cíl
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sporné	sporný	k2eAgNnSc1d1	sporné
<g/>
,	,	kIx,	,
některá	některý	k3yIgFnSc1	některý
novější	nový	k2eAgFnSc1d2	novější
literatura	literatura	k1gFnSc1	literatura
uvádí	uvádět	k5eAaImIp3nS	uvádět
stav	stav	k1gInSc4	stav
říšských	říšský	k2eAgNnPc2d1	říšské
hrabat	hrabě	k1gNnPc2	hrabě
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
České	český	k2eAgFnSc2d1	Česká
dvorské	dvorský	k2eAgFnSc2d1	dvorská
kanceláře	kancelář	k1gFnSc2	kancelář
se	se	k3xPyFc4	se
žádné	žádný	k3yNgInPc1	žádný
důkazy	důkaz	k1gInPc1	důkaz
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
nenacházejí	nacházet	k5eNaImIp3nP	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
titul	titul	k1gInSc4	titul
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
již	již	k6eAd1	již
před	před	k7c7	před
svatbou	svatba	k1gFnSc7	svatba
s	s	k7c7	s
hraběnkou	hraběnka	k1gFnSc7	hraběnka
Zrinskou	Zrinský	k2eAgFnSc7d1	Zrinský
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
jej	on	k3xPp3gMnSc4	on
používat	používat	k5eAaImF	používat
jako	jako	k9	jako
manžel	manžel	k1gMnSc1	manžel
hraběnky	hraběnka	k1gFnSc2	hraběnka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Josefa	Josef	k1gMnSc2	Josef
I.	I.	kA	I.
ztratil	ztratit	k5eAaPmAgMnS	ztratit
vyhlídky	vyhlídka	k1gFnPc4	vyhlídka
na	na	k7c6	na
povýšení	povýšení	k1gNnSc6	povýšení
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
na	na	k7c4	na
rodnou	rodný	k2eAgFnSc4d1	rodná
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
bez	bez	k7c2	bez
mužského	mužský	k2eAgMnSc2d1	mužský
dědice	dědic	k1gMnSc2	dědic
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
bítovská	bítovský	k2eAgFnSc1d1	bítovská
větev	větev	k1gFnSc1	větev
vymřela	vymřít	k5eAaPmAgFnS	vymřít
po	po	k7c6	po
meči	meč	k1gInSc6	meč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Antonín	Antonín	k1gMnSc1	Antonín
Jankovský	Jankovský	k2eAgMnSc1d1	Jankovský
z	z	k7c2	z
Vlašimi	Vlašim	k1gFnSc2	Vlašim
(	(	kIx(	(
<g/>
1690	[number]	k4	1690
<g/>
–	–	k?	–
<g/>
1752	[number]	k4	1752
<g/>
)	)	kIx)	)
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
rešické	rešický	k2eAgFnSc2d1	rešický
linie	linie	k1gFnSc2	linie
rodu	rod	k1gInSc2	rod
a	a	k8xC	a
soudně	soudně	k6eAd1	soudně
usiloval	usilovat	k5eAaImAgMnS	usilovat
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
Bítova	Bítov	k1gInSc2	Bítov
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
větev	větev	k1gFnSc4	větev
a	a	k8xC	a
děti	dítě	k1gFnPc4	dítě
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
Jana	Jan	k1gMnSc2	Jan
Antonína	Antonín	k1gMnSc2	Antonín
(	(	kIx(	(
<g/>
1692	[number]	k4	1692
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1740	[number]	k4	1740
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Povedlo	povést	k5eAaPmAgNnS	povést
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1752	[number]	k4	1752
<g/>
,	,	kIx,	,
po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
však	však	k9	však
zemřel	zemřít	k5eAaPmAgInS	zemřít
a	a	k8xC	a
děti	dítě	k1gFnPc4	dítě
obou	dva	k4xCgMnPc2	dva
bratrů	bratr	k1gMnPc2	bratr
osiřely	osiřet	k5eAaPmAgFnP	osiřet
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1755	[number]	k4	1755
udělila	udělit	k5eAaPmAgFnS	udělit
císařovna	císařovna	k1gFnSc1	císařovna
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
bítovské	bítovský	k2eAgFnSc2d1	bítovská
panství	panství	k1gNnSc4	panství
rakouským	rakouský	k2eAgMnSc7d1	rakouský
příbuzným	příbuzný	k1gMnSc7	příbuzný
bítovské	bítovský	k2eAgFnSc2d1	bítovská
větve	větev	k1gFnSc2	větev
<g/>
,	,	kIx,	,
rodu	rod	k1gInSc2	rod
Daunů	Daun	k1gInPc2	Daun
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Americká	americký	k2eAgFnSc1d1	americká
linie	linie	k1gFnPc1	linie
==	==	k?	==
</s>
</p>
<p>
<s>
Syn	syn	k1gMnSc1	syn
Jana	Jan	k1gMnSc4	Jan
Antonína	Antonín	k1gMnSc4	Antonín
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Václav	Václav	k1gMnSc1	Václav
Jankovský	Jankovský	k2eAgMnSc1d1	Jankovský
z	z	k7c2	z
Vlašimi	Vlašim	k1gFnSc2	Vlašim
(	(	kIx(	(
<g/>
1729	[number]	k4	1729
<g/>
–	–	k?	–
<g/>
1775	[number]	k4	1775
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
vojenský	vojenský	k2eAgMnSc1d1	vojenský
vůdce	vůdce	k1gMnSc1	vůdce
během	během	k7c2	během
sedmileté	sedmiletý	k2eAgFnSc2d1	sedmiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
jeho	jeho	k3xOp3gMnPc1	jeho
synové	syn	k1gMnPc1	syn
Jan	Jan	k1gMnSc1	Jan
Jankovský	Jankovský	k2eAgMnSc1d1	Jankovský
a	a	k8xC	a
Karel	Karel	k1gMnSc1	Karel
Jankovský	Jankovský	k2eAgInSc1d1	Jankovský
von	von	k1gInSc1	von
Mayenhorst	Mayenhorst	k1gMnSc1	Mayenhorst
byli	být	k5eAaImAgMnP	být
pokračovateli	pokračovatel	k1gMnPc7	pokračovatel
rodu	rod	k1gInSc2	rod
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
se	se	k3xPyFc4	se
oženila	oženit	k5eAaPmAgFnS	oženit
s	s	k7c7	s
rodinami	rodina	k1gFnPc7	rodina
von	von	k1gInSc1	von
Raule	Raule	k1gFnPc1	Raule
a	a	k8xC	a
Kralik	Kralik	k1gMnSc1	Kralik
von	von	k1gInSc4	von
Meyrswalden	Meyrswaldna	k1gFnPc2	Meyrswaldna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
příslušníci	příslušník	k1gMnPc1	příslušník
rodu	rod	k1gInSc2	rod
odstěhovali	odstěhovat	k5eAaPmAgMnP	odstěhovat
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
John	John	k1gMnSc1	John
Jankovsky	Jankovsky	k1gMnSc1	Jankovsky
(	(	kIx(	(
<g/>
1845	[number]	k4	1845
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
rodové	rodový	k2eAgFnSc6d1	rodová
linii	linie	k1gFnSc6	linie
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
starších	starý	k2eAgFnPc2d2	starší
literatury	literatura	k1gFnSc2	literatura
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
rod	rod	k1gInSc1	rod
vymřel	vymřít	k5eAaPmAgInS	vymřít
koncem	koncem	k7c2	koncem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Erb	erb	k1gInSc4	erb
==	==	k?	==
</s>
</p>
<p>
<s>
Při	při	k7c6	při
povýšení	povýšení	k1gNnSc6	povýšení
do	do	k7c2	do
panského	panský	k2eAgInSc2d1	panský
stavu	stav	k1gInSc2	stav
dostali	dostat	k5eAaPmAgMnP	dostat
čtvercový	čtvercový	k2eAgInSc4d1	čtvercový
erb	erb	k1gInSc4	erb
s	s	k7c7	s
černou	černý	k2eAgFnSc7d1	černá
orlicí	orlice	k1gFnSc7	orlice
a	a	k8xC	a
znak	znak	k1gInSc1	znak
pánů	pan	k1gMnPc2	pan
z	z	k7c2	z
Vlašimi	Vlašim	k1gFnSc2	Vlašim
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
červenými	červený	k2eAgFnPc7d1	červená
supími	supí	k2eAgFnPc7d1	supí
hlavami	hlava	k1gFnPc7	hlava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příbuzenstvo	příbuzenstvo	k1gNnSc1	příbuzenstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Spojili	spojit	k5eAaPmAgMnP	spojit
se	se	k3xPyFc4	se
s	s	k7c7	s
pány	pan	k1gMnPc7	pan
z	z	k7c2	z
Kunštátu	Kunštát	k1gInSc2	Kunštát
<g/>
,	,	kIx,	,
Zrinskými	Zrinská	k1gFnPc7	Zrinská
<g/>
,	,	kIx,	,
Lamberky	Lamberk	k1gInPc7	Lamberk
<g/>
,	,	kIx,	,
Dauny	Daun	k1gInPc7	Daun
<g/>
,	,	kIx,	,
Šternberky	Šternberk	k1gInPc7	Šternberk
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
českými	český	k2eAgInPc7d1	český
a	a	k8xC	a
moravskými	moravský	k2eAgInPc7d1	moravský
rody	rod	k1gInPc7	rod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgInPc2d1	český
šlechtických	šlechtický	k2eAgInPc2d1	šlechtický
rodů	rod	k1gInPc2	rod
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
HALADA	HALADA	kA	HALADA
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Lexikon	lexikon	k1gInSc1	lexikon
české	český	k2eAgFnSc2d1	Česká
šlechty	šlechta	k1gFnSc2	šlechta
(	(	kIx(	(
<g/>
erby	erb	k1gInPc1	erb
<g/>
,	,	kIx,	,
fakta	faktum	k1gNnPc1	faktum
<g/>
,	,	kIx,	,
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
sídla	sídlo	k1gNnSc2	sídlo
a	a	k8xC	a
zajímavosti	zajímavost	k1gFnSc2	zajímavost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
AKROPOLIS	Akropolis	k1gFnSc1	Akropolis
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
901020	[number]	k4	901020
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Jankovští	Jankovský	k2eAgMnPc1d1	Jankovský
z	z	k7c2	z
Vlašimi	Vlašim	k1gFnSc2	Vlašim
<g/>
,	,	kIx,	,
s.	s.	k?	s.
64	[number]	k4	64
<g/>
-	-	kIx~	-
<g/>
65	[number]	k4	65
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BOLOM	BOLOM	kA	BOLOM
<g/>
,	,	kIx,	,
Sixtus	Sixtus	k1gMnSc1	Sixtus
<g/>
.	.	kIx.	.
</s>
<s>
Tajemství	tajemství	k1gNnSc1	tajemství
Jankovských	Jankovský	k2eAgInPc2d1	Jankovský
z	z	k7c2	z
Vlašimi	Vlašim	k1gFnSc2	Vlašim
a	a	k8xC	a
na	na	k7c6	na
Bítově	Bítov	k1gInSc6	Bítov
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Akcent	akcent	k1gInSc1	akcent
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7268	[number]	k4	7268
<g/>
-	-	kIx~	-
<g/>
527	[number]	k4	527
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Genealogie	genealogie	k1gFnSc1	genealogie
rodu	rod	k1gInSc2	rod
Jankovských	Jankovský	k2eAgFnPc2d1	Jankovská
z	z	k7c2	z
Vlašimi	Vlašim	k1gFnSc2	Vlašim
</s>
</p>
<p>
<s>
Z	z	k7c2	z
hradu	hrad	k1gInSc2	hrad
Bítova	Bítov	k1gInSc2	Bítov
až	až	k6eAd1	až
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
</s>
</p>
<p>
<s>
Matriky	matrika	k1gFnPc1	matrika
MZA	MZA	kA	MZA
Brno	Brno	k1gNnSc1	Brno
</s>
</p>
