<s>
Jankovští	Jankovský	k2eAgMnPc1d1	Jankovský
z	z	k7c2	z
Vlašimi	Vlašim	k1gFnSc2	Vlašim
jsou	být	k5eAaImIp3nP	být
někdejší	někdejší	k2eAgInSc4d1	někdejší
rytířský	rytířský	k2eAgInSc4d1	rytířský
a	a	k8xC	a
panský	panský	k2eAgInSc4d1	panský
český	český	k2eAgInSc4d1	český
rod	rod	k1gInSc4	rod
<g/>
,	,	kIx,	,
svůj	svůj	k3xOyFgInSc4	svůj
přídomek	přídomek	k1gInSc4	přídomek
odvozoval	odvozovat	k5eAaImAgMnS	odvozovat
od	od	k7c2	od
středočeského	středočeský	k2eAgNnSc2d1	Středočeské
města	město	k1gNnSc2	město
Vlašimi	Vlašim	k1gFnSc2	Vlašim
<g/>
.	.	kIx.	.
</s>
