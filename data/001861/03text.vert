<s>
Robert	Robert	k1gMnSc1	Robert
Grant	grant	k1gInSc1	grant
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
Rob	roba	k1gFnPc2	roba
Grant	grant	k1gInSc1	grant
<g/>
,	,	kIx,	,
narozen	narozen	k2eAgInSc1d1	narozen
v	v	k7c6	v
Salfordu	Salford	k1gInSc6	Salford
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
britský	britský	k2eAgMnSc1d1	britský
komediální	komediální	k2eAgMnSc1d1	komediální
autor	autor	k1gMnSc1	autor
a	a	k8xC	a
televizní	televizní	k2eAgMnSc1d1	televizní
producent	producent	k1gMnSc1	producent
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Dougem	Doug	k1gMnSc7	Doug
Naylorem	Naylor	k1gMnSc7	Naylor
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
scénář	scénář	k1gInSc4	scénář
kultovního	kultovní	k2eAgNnSc2d1	kultovní
sci-fi	scii	k1gNnSc2	sci-fi
seriálu	seriál	k1gInSc2	seriál
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Grant	grant	k1gInSc1	grant
a	a	k8xC	a
Naylor	Naylor	k1gMnSc1	Naylor
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
také	také	k9	také
během	během	k7c2	během
rozhlasových	rozhlasový	k2eAgInPc2d1	rozhlasový
programů	program	k1gInPc2	program
"	"	kIx"	"
<g/>
Cliché	Clichý	k2eAgNnSc4d1	Cliché
<g/>
"	"	kIx"	"
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
pokračování	pokračování	k1gNnSc1	pokračování
"	"	kIx"	"
<g/>
Son	son	k1gInSc1	son
of	of	k?	of
Cliché	Clichý	k2eAgFnPc1d1	Clichý
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
při	při	k7c6	při
televizních	televizní	k2eAgInPc6d1	televizní
pořadech	pořad	k1gInPc6	pořad
"	"	kIx"	"
<g/>
Spitting	Spitting	k1gInSc1	Spitting
Image	image	k1gInSc2	image
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
10	[number]	k4	10
Percenters	Percentersa	k1gFnPc2	Percentersa
<g/>
"	"	kIx"	"
a	a	k8xC	a
při	při	k7c6	při
různých	různý	k2eAgInPc6d1	různý
projektech	projekt	k1gInPc6	projekt
pro	pro	k7c4	pro
britského	britský	k2eAgMnSc4d1	britský
komika	komik	k1gMnSc4	komik
Jaspera	Jasper	k1gMnSc4	Jasper
Carrotta	Carrott	k1gMnSc4	Carrott
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ze	z	k7c2	z
sketche	sketch	k1gFnSc2	sketch
Dave	Dav	k1gInSc5	Dav
Hollins	Hollins	k1gInSc1	Hollins
<g/>
:	:	kIx,	:
Space	Space	k1gMnSc1	Space
Cadet	Cadet	k1gMnSc1	Cadet
z	z	k7c2	z
pořadu	pořad	k1gInSc2	pořad
"	"	kIx"	"
<g/>
Son	son	k1gInSc1	son
of	of	k?	of
Cliché	Clichý	k2eAgInPc1d1	Clichý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Rob	roba	k1gFnPc2	roba
Grant	grant	k1gInSc1	grant
se	se	k3xPyFc4	se
krátce	krátce	k6eAd1	krátce
mihl	mihnout	k5eAaPmAgMnS	mihnout
v	v	k7c6	v
epizodě	epizoda	k1gFnSc6	epizoda
"	"	kIx"	"
<g/>
Pozpátku	pozpátku	k6eAd1	pozpátku
<g/>
"	"	kIx"	"
v	v	k7c6	v
roli	role	k1gFnSc6	role
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
kouří	kouřit	k5eAaImIp3nS	kouřit
přibývající	přibývající	k2eAgFnSc4d1	přibývající
cigaretu	cigareta	k1gFnSc4	cigareta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
po	po	k7c6	po
šesti	šest	k4xCc6	šest
sériích	série	k1gFnPc6	série
Rob	roba	k1gFnPc2	roba
Grant	grant	k1gInSc4	grant
opouští	opouštět	k5eAaImIp3nS	opouštět
seriál	seriál	k1gInSc1	seriál
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
napsal	napsat	k5eAaPmAgMnS	napsat
dvě	dva	k4xCgFnPc4	dva
televizní	televizní	k2eAgFnPc4d1	televizní
série	série	k1gFnPc4	série
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Strangerers	Strangerersa	k1gFnPc2	Strangerersa
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Dark	Dark	k1gInSc1	Dark
Ages	Ages	k1gInSc1	Ages
<g/>
"	"	kIx"	"
a	a	k8xC	a
4	[number]	k4	4
romány	román	k1gInPc7	román
<g/>
.	.	kIx.	.
</s>
<s>
Červený	červený	k2eAgMnSc1d1	červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
4	[number]	k4	4
<g/>
:	:	kIx,	:
Pozpátku	pozpátku	k6eAd1	pozpátku
-	-	kIx~	-
v	v	k7c6	v
závěrečném	závěrečný	k2eAgInSc6d1	závěrečný
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
dílu	díl	k1gInSc6	díl
knižní	knižní	k2eAgFnSc2d1	knižní
série	série	k1gFnSc2	série
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
Dave	Dav	k1gInSc5	Dav
Lister	Lister	k1gMnSc1	Lister
konečně	konečně	k6eAd1	konečně
nalezl	nalézt	k5eAaBmAgMnS	nalézt
<g/>
,	,	kIx,	,
po	po	k7c6	po
čem	co	k3yRnSc6	co
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
pátral	pátrat	k5eAaImAgInS	pátrat
-	-	kIx~	-
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
má	mít	k5eAaImIp3nS	mít
to	ten	k3xDgNnSc1	ten
jeden	jeden	k4xCgInSc1	jeden
háček	háček	k1gInSc1	háček
<g/>
,	,	kIx,	,
čas	čas	k1gInSc1	čas
zde	zde	k6eAd1	zde
plyne	plynout	k5eAaImIp3nS	plynout
pozpátku	pozpátku	k6eAd1	pozpátku
<g/>
.	.	kIx.	.
</s>
<s>
Zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
tady	tady	k6eAd1	tady
čeká	čekat	k5eAaImIp3nS	čekat
velmi	velmi	k6eAd1	velmi
bizarní	bizarní	k2eAgInSc4d1	bizarní
konec	konec	k1gInSc4	konec
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
neobjeví	objevit	k5eNaPmIp3nP	objevit
kamarádi	kamarád	k1gMnPc1	kamarád
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jej	on	k3xPp3gMnSc4	on
zachránili	zachránit	k5eAaPmAgMnP	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
Kolonie	kolonie	k1gFnSc1	kolonie
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
Colony	colon	k1gNnPc7	colon
Neschopnost	neschopnost	k1gFnSc1	neschopnost
<g/>
:	:	kIx,	:
Zlo	zlo	k1gNnSc1	zlo
je	být	k5eAaImIp3nS	být
nové	nový	k2eAgNnSc1d1	nové
dobro	dobro	k1gNnSc1	dobro
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
Incompetence	Incompetence	k1gFnSc1	Incompetence
-	-	kIx~	-
humorný	humorný	k2eAgInSc1d1	humorný
román	román	k1gInSc1	román
z	z	k7c2	z
blízké	blízký	k2eAgFnSc2d1	blízká
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
Evropa	Evropa	k1gFnSc1	Evropa
sloučila	sloučit	k5eAaPmAgFnS	sloučit
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
evropských	evropský	k2eAgInPc2d1	evropský
a	a	k8xC	a
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
neschopnost	neschopnost	k1gFnSc1	neschopnost
vyžadována	vyžadován	k2eAgFnSc1d1	vyžadována
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
stopách	stopa	k1gFnPc6	stopa
naopak	naopak	k6eAd1	naopak
velmi	velmi	k6eAd1	velmi
schopného	schopný	k2eAgMnSc4d1	schopný
vraha	vrah	k1gMnSc4	vrah
se	se	k3xPyFc4	se
vydává	vydávat	k5eAaImIp3nS	vydávat
jistý	jistý	k2eAgMnSc1d1	jistý
detektiv	detektiv	k1gMnSc1	detektiv
<g/>
...	...	k?	...
Špeky	špek	k1gInPc1	špek
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
Fat	fatum	k1gNnPc2	fatum
-	-	kIx~	-
satirický	satirický	k2eAgInSc1d1	satirický
román	román	k1gInSc1	román
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
společnost	společnost	k1gFnSc1	společnost
a	a	k8xC	a
média	médium	k1gNnPc1	médium
pohlíží	pohlížet	k5eAaImIp3nP	pohlížet
na	na	k7c4	na
obezitu	obezita	k1gFnSc4	obezita
Červený	červený	k2eAgMnSc1d1	červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
1	[number]	k4	1
<g/>
:	:	kIx,	:
Nekonečno	nekonečno	k1gNnSc1	nekonečno
vítá	vítat	k5eAaImIp3nS	vítat
ohleduplné	ohleduplný	k2eAgMnPc4d1	ohleduplný
řidiče	řidič	k1gMnPc4	řidič
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
2	[number]	k4	2
<g/>
:	:	kIx,	:
Lepší	dobrý	k2eAgMnSc1d2	lepší
než	než	k8xS	než
život	život	k1gInSc1	život
Primordial	Primordial	k1gMnSc1	Primordial
Soup	Soup	k1gMnSc1	Soup
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
-	-	kIx~	-
knižní	knižní	k2eAgFnSc1d1	knižní
kolekce	kolekce	k1gFnSc1	kolekce
scénářů	scénář	k1gInPc2	scénář
TV	TV	kA	TV
Show	show	k1gNnSc1	show
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
Son	son	k1gInSc4	son
Of	Of	k1gFnSc2	Of
Soup	Soup	k1gInSc1	Soup
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
-	-	kIx~	-
další	další	k2eAgFnSc2d1	další
kolekce	kolekce	k1gFnSc2	kolekce
scénářů	scénář	k1gInPc2	scénář
Scenes	Scenes	k1gMnSc1	Scenes
From	From	k1gMnSc1	From
The	The	k1gMnSc1	The
Dwarf	Dwarf	k1gMnSc1	Dwarf
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
</s>
