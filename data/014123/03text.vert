<s>
Píšťala	píšťala	k1gFnSc1
(	(	kIx(
<g/>
zbraň	zbraň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Střelba	střelba	k1gFnSc1
z	z	k7c2
ručního	ruční	k2eAgInSc2d1
kanonu	kanon	k1gInSc2
<g/>
,	,	kIx,
rukopis	rukopis	k1gInSc1
Konrada	Konrada	k1gFnSc1
Kyesera	Kyesera	k1gFnSc1
<g/>
,	,	kIx,
1405	#num#	k4
</s>
<s>
Píšťala	píšťala	k1gFnSc1
je	být	k5eAaImIp3nS
název	název	k1gInSc4
pro	pro	k7c4
ruční	ruční	k2eAgFnSc4d1
palnou	palný	k2eAgFnSc4d1
zbraň	zbraň	k1gFnSc4
<g/>
,	,	kIx,
rozšířenou	rozšířený	k2eAgFnSc4d1
za	za	k7c2
husitských	husitský	k2eAgFnPc2d1
válek	válka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Píšťala	píšťala	k1gFnSc1
měla	mít	k5eAaImAgFnS
poměrně	poměrně	k6eAd1
krátkou	krátký	k2eAgFnSc4d1
železnou	železný	k2eAgFnSc4d1
hlaveň	hlaveň	k1gFnSc4
s	s	k7c7
tulejí	tulej	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
byla	být	k5eAaImAgFnS
vsazena	vsadit	k5eAaPmNgFnS
v	v	k7c6
dosti	dosti	k6eAd1
neohrabaném	neohrabaný	k2eAgInSc6d1,k2eNgInSc6d1
dřevěném	dřevěný	k2eAgInSc6d1
stvolu	stvol	k1gInSc6
<g/>
,	,	kIx,
jejž	jenž	k3xRgInSc4
držel	držet	k5eAaImAgMnS
střelec	střelec	k1gMnSc1
v	v	k7c6
podpaží	podpaží	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ráže	ráže	k1gFnSc1
se	se	k3xPyFc4
pohybovala	pohybovat	k5eAaImAgFnS
okolo	okolo	k7c2
15-25	15-25	k4
mm	mm	kA
s	s	k7c7
různými	různý	k2eAgInPc7d1
projektily	projektil	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používaly	používat	k5eAaImAgInP
se	se	k3xPyFc4
jak	jak	k8xS,k8xC
celistvé	celistvý	k2eAgFnPc1d1
kule	kule	k1gFnPc1
<g/>
,	,	kIx,
tak	tak	k9
kartáčové	kartáčový	k2eAgFnPc4d1
střely	střela	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
působily	působit	k5eAaImAgFnP
jako	jako	k9
brokovnice	brokovnice	k1gFnPc1
a	a	k8xC
nebo	nebo	k8xC
speciálně	speciálně	k6eAd1
tvarované	tvarovaný	k2eAgFnPc1d1
kule	kule	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
při	při	k7c6
letu	let	k1gInSc6
hvízdaly	hvízdat	k5eAaImAgFnP
a	a	k8xC
plašily	plašit	k5eAaImAgFnP
i	i	k8xC
ty	ten	k3xDgMnPc4
koně	kůň	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
byli	být	k5eAaImAgMnP
zvyklí	zvyklý	k2eAgMnPc1d1
na	na	k7c4
střelbu	střelba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlaveň	hlaveň	k1gFnSc1
neměla	mít	k5eNaImAgFnS
vývrt	vývrt	k1gInSc4
a	a	k8xC
byla	být	k5eAaImAgFnS
buď	buď	k8xC
ocelová	ocelový	k2eAgFnSc1d1
kovaná	kovaný	k2eAgFnSc1d1
<g/>
,	,	kIx,
či	či	k8xC
odlévaná	odlévaný	k2eAgFnSc1d1
z	z	k7c2
bronzu	bronz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střelný	střelný	k2eAgInSc1d1
prach	prach	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
hlavni	hlaveň	k1gFnSc6
zapaloval	zapalovat	k5eAaImAgInS
pomocí	pomocí	k7c2
doutnáku	doutnák	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostřel	dostřel	k1gInSc1
byl	být	k5eAaImAgInS
nevelký	velký	k2eNgInSc1d1
a	a	k8xC
značně	značně	k6eAd1
nepřesný	přesný	k2eNgInSc4d1
<g/>
,	,	kIx,
účinek	účinek	k1gInSc1
byl	být	k5eAaImAgInS
tedy	tedy	k9
spíše	spíše	k9
psychologický	psychologický	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
se	se	k3xPyFc4
rozšířil	rozšířit	k5eAaPmAgInS
po	po	k7c6
celé	celý	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
a	a	k8xC
má	mít	k5eAaImIp3nS
se	se	k3xPyFc4
za	za	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
z	z	k7c2
něj	on	k3xPp3gNnSc2
vyvinulo	vyvinout	k5eAaPmAgNnS
slovo	slovo	k1gNnSc1
pistole	pistol	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
píšťaly	píšťala	k1gFnSc2
se	se	k3xPyFc4
později	pozdě	k6eAd2
vyvinula	vyvinout	k5eAaPmAgFnS
hákovnice	hákovnice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
píšťala	píšťala	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
