<s>
Za	za	k7c4	za
objev	objev	k1gInSc4	objev
fototerapie	fototerapie	k1gFnSc2	fototerapie
účinné	účinný	k2eAgFnPc1d1	účinná
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
onemocnění	onemocnění	k1gNnSc4	onemocnění
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
získal	získat	k5eAaPmAgMnS	získat
dánský	dánský	k2eAgMnSc1d1	dánský
lékař	lékař	k1gMnSc1	lékař
Niels	Nielsa	k1gFnPc2	Nielsa
Ryberg	Ryberg	k1gMnSc1	Ryberg
Finsen	Finsen	k2eAgMnSc1d1	Finsen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
lékařství	lékařství	k1gNnSc4	lékařství
<g/>
.	.	kIx.	.
</s>
