<s>
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
elektrodynamika	elektrodynamika	k1gFnSc1	elektrodynamika
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
zkratka	zkratka	k1gFnSc1	zkratka
"	"	kIx"	"
<g/>
QED	QED	kA	QED
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
pohybu	pohyb	k1gInSc6	pohyb
elektrických	elektrický	k2eAgInPc2d1	elektrický
nábojů	náboj	k1gInPc2	náboj
(	(	kIx(	(
<g/>
nabitých	nabitý	k2eAgNnPc2d1	nabité
těles	těleso	k1gNnPc2	těleso
<g/>
)	)	kIx)	)
v	v	k7c6	v
obecně	obecně	k6eAd1	obecně
proměnných	proměnný	k2eAgFnPc6d1	proměnná
elektromagnetických	elektromagnetický	k2eAgFnPc6d1	elektromagnetická
polích	pole	k1gFnPc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Klasická	klasický	k2eAgFnSc1d1	klasická
elektrodynamika	elektrodynamika	k1gFnSc1	elektrodynamika
studuje	studovat	k5eAaImIp3nS	studovat
elektrodynamické	elektrodynamický	k2eAgFnPc4d1	elektrodynamická
interakce	interakce	k1gFnPc4	interakce
mezi	mezi	k7c7	mezi
makroskopickými	makroskopický	k2eAgNnPc7d1	makroskopické
tělesy	těleso	k1gNnPc7	těleso
<g/>
,	,	kIx,	,
kvantová	kvantový	k2eAgFnSc1d1	kvantová
elektrodynamika	elektrodynamika	k1gFnSc1	elektrodynamika
interakce	interakce	k1gFnSc1	interakce
mezi	mezi	k7c7	mezi
mikroobjekty	mikroobjekt	k1gInPc7	mikroobjekt
(	(	kIx(	(
<g/>
atomárních	atomární	k2eAgInPc2d1	atomární
a	a	k8xC	a
subatomárních	subatomární	k2eAgInPc2d1	subatomární
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
kvantová	kvantový	k2eAgFnSc1d1	kvantová
teorie	teorie	k1gFnSc1	teorie
elektromagnetických	elektromagnetický	k2eAgInPc2d1	elektromagnetický
procesů	proces	k1gInPc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
elektrodynamika	elektrodynamika	k1gFnSc1	elektrodynamika
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
teorie	teorie	k1gFnSc1	teorie
interakce	interakce	k1gFnSc1	interakce
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
pole	pole	k1gNnSc2	pole
a	a	k8xC	a
pole	pole	k1gNnSc2	pole
popisujícího	popisující	k2eAgInSc2d1	popisující
elektrony	elektron	k1gInPc7	elektron
a	a	k8xC	a
pozitrony	pozitron	k1gInPc7	pozitron
<g/>
.	.	kIx.	.
</s>
<s>
Interakce	interakce	k1gFnSc1	interakce
je	být	k5eAaImIp3nS	být
popsána	popsat	k5eAaPmNgFnS	popsat
v	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
elektrodynamice	elektrodynamika	k1gFnSc6	elektrodynamika
výměnou	výměna	k1gFnSc7	výměna
fotonů	foton	k1gInPc2	foton
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
důsledně	důsledně	k6eAd1	důsledně
vypracovanou	vypracovaný	k2eAgFnSc7d1	vypracovaná
kvantovou	kvantový	k2eAgFnSc7d1	kvantová
teorií	teorie	k1gFnSc7	teorie
silového	silový	k2eAgNnSc2d1	silové
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
výsledky	výsledek	k1gInPc1	výsledek
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
již	již	k6eAd1	již
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
kvantové	kvantový	k2eAgFnSc2d1	kvantová
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
postupy	postup	k1gInPc1	postup
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgInP	použít
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
dalších	další	k2eAgFnPc2d1	další
interakcí	interakce	k1gFnPc2	interakce
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Feynmanovy	Feynmanův	k2eAgInPc1d1	Feynmanův
diagramy	diagram	k1gInPc1	diagram
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
elektrodynamika	elektrodynamika	k1gFnSc1	elektrodynamika
popisuje	popisovat	k5eAaImIp3nS	popisovat
interakci	interakce	k1gFnSc4	interakce
záření	záření	k1gNnSc2	záření
s	s	k7c7	s
hmotou	hmota	k1gFnSc7	hmota
(	(	kIx(	(
<g/>
záření	záření	k1gNnSc1	záření
atomů	atom	k1gInPc2	atom
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
soustav	soustava	k1gFnPc2	soustava
<g/>
,	,	kIx,	,
fotoelektrický	fotoelektrický	k2eAgInSc1d1	fotoelektrický
jev	jev	k1gInSc1	jev
<g/>
,	,	kIx,	,
Comptonův	Comptonův	k2eAgInSc1d1	Comptonův
rozptyl	rozptyl	k1gInSc1	rozptyl
<g/>
,	,	kIx,	,
brzdné	brzdný	k2eAgNnSc1d1	brzdné
záření	záření	k1gNnSc1	záření
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
elektromagnetické	elektromagnetický	k2eAgFnSc2d1	elektromagnetická
interakce	interakce	k1gFnSc2	interakce
mezi	mezi	k7c7	mezi
nabitými	nabitý	k2eAgFnPc7d1	nabitá
elementárními	elementární	k2eAgFnPc7d1	elementární
částicemi	částice	k1gFnPc7	částice
<g/>
,	,	kIx,	,
reakce	reakce	k1gFnPc4	reakce
fotonů	foton	k1gInPc2	foton
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
teorii	teorie	k1gFnSc4	teorie
interakcí	interakce	k1gFnPc2	interakce
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
fotonů	foton	k1gInPc2	foton
jakožto	jakožto	k8xS	jakožto
kvant	kvantum	k1gNnPc2	kvantum
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Nabitým	nabitý	k2eAgFnPc3d1	nabitá
částicím	částice	k1gFnPc3	částice
(	(	kIx(	(
<g/>
např.	např.	kA	např.
elektronům	elektron	k1gInPc3	elektron
a	a	k8xC	a
pozitronům	pozitron	k1gInPc3	pozitron
<g/>
)	)	kIx)	)
přitom	přitom	k6eAd1	přitom
rovněž	rovněž	k9	rovněž
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
kvantované	kvantovaný	k2eAgNnSc4d1	kvantované
pole	pole	k1gNnSc4	pole
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
však	však	k9	však
nemá	mít	k5eNaImIp3nS	mít
klasickou	klasický	k2eAgFnSc4d1	klasická
fyzikální	fyzikální	k2eAgFnSc4d1	fyzikální
analogii	analogie	k1gFnSc4	analogie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
přesné	přesný	k2eAgFnSc3d1	přesná
shodě	shoda	k1gFnSc3	shoda
s	s	k7c7	s
experimentálními	experimentální	k2eAgNnPc7d1	experimentální
daty	datum	k1gNnPc7	datum
objasňuje	objasňovat	k5eAaImIp3nS	objasňovat
anomální	anomální	k2eAgInSc1d1	anomální
magnetický	magnetický	k2eAgInSc1d1	magnetický
moment	moment	k1gInSc1	moment
elektronu	elektron	k1gInSc2	elektron
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
malou	malý	k2eAgFnSc4d1	malá
odchylku	odchylka	k1gFnSc4	odchylka
od	od	k7c2	od
hodnoty	hodnota	k1gFnSc2	hodnota
předpovězené	předpovězená	k1gFnSc2	předpovězená
Diracovou	Diracová	k1gFnSc7	Diracová
rovnicí	rovnice	k1gFnSc7	rovnice
a	a	k8xC	a
také	také	k9	také
některé	některý	k3yIgInPc4	některý
jemné	jemný	k2eAgInPc4d1	jemný
efekty	efekt	k1gInPc4	efekt
v	v	k7c6	v
atomových	atomový	k2eAgNnPc6d1	atomové
spektrech	spektrum	k1gNnPc6	spektrum
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
kvantová	kvantový	k2eAgFnSc1d1	kvantová
mechanika	mechanika	k1gFnSc1	mechanika
není	být	k5eNaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
vystihnout	vystihnout	k5eAaPmF	vystihnout
<g/>
.	.	kIx.	.
</s>
<s>
Feynman	Feynman	k1gMnSc1	Feynman
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Phillips	Phillips	k1gInSc1	Phillips
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Quantum	Quantum	k1gNnSc1	Quantum
Electrodynamics	Electrodynamicsa	k1gFnPc2	Electrodynamicsa
<g/>
.	.	kIx.	.
</s>
<s>
Westview	Westview	k?	Westview
Press	Press	k1gInSc1	Press
<g/>
;	;	kIx,	;
New	New	k1gFnSc1	New
Ed	Ed	k1gMnSc1	Ed
edition	edition	k1gInSc1	edition
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
201	[number]	k4	201
<g/>
-	-	kIx~	-
<g/>
36075	[number]	k4	36075
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Tannoudji-Cohen	Tannoudji-Cohen	k1gInSc1	Tannoudji-Cohen
<g/>
,	,	kIx,	,
Claude	Claud	k1gMnSc5	Claud
<g/>
;	;	kIx,	;
Dupont-Roc	Dupont-Roc	k1gInSc1	Dupont-Roc
<g/>
,	,	kIx,	,
Jacques	Jacques	k1gMnSc1	Jacques
<g/>
,	,	kIx,	,
and	and	k?	and
Grynberg	Grynberg	k1gMnSc1	Grynberg
<g/>
,	,	kIx,	,
Gilbert	Gilbert	k1gMnSc1	Gilbert
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Photons	Photons	k1gInSc1	Photons
and	and	k?	and
Atoms	Atoms	k1gInSc1	Atoms
<g/>
:	:	kIx,	:
Introduction	Introduction	k1gInSc1	Introduction
to	ten	k3xDgNnSc4	ten
Quantum	Quantum	k1gNnSc4	Quantum
Electrodynamics	Electrodynamics	k1gInSc4	Electrodynamics
<g/>
.	.	kIx.	.
</s>
<s>
Wiley-Interscience	Wiley-Interscience	k1gFnSc1	Wiley-Interscience
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
471	[number]	k4	471
<g/>
-	-	kIx~	-
<g/>
18433	[number]	k4	18433
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
Broglie	Broglie	k1gFnSc1	Broglie
<g/>
,	,	kIx,	,
Louis	Louis	k1gMnSc1	Louis
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Recherches	Recherches	k1gInSc1	Recherches
sur	sur	k?	sur
la	la	k1gNnSc1	la
theorie	theorie	k1gFnSc1	theorie
des	des	k1gNnSc1	des
quanta	quant	k1gMnSc2	quant
[	[	kIx(	[
<g/>
Research	Research	k1gMnSc1	Research
on	on	k3xPp3gMnSc1	on
quantum	quantum	k1gNnSc4	quantum
theory	theora	k1gFnPc1	theora
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
France	Franc	k1gMnSc4	Franc
<g/>
:	:	kIx,	:
Wiley-Interscience	Wiley-Interscienec	k1gMnSc4	Wiley-Interscienec
<g/>
.	.	kIx.	.
</s>
<s>
Jauch	Jauch	k1gMnSc1	Jauch
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
M.	M.	kA	M.
<g/>
;	;	kIx,	;
Rohrlich	Rohrlich	k1gMnSc1	Rohrlich
<g/>
,	,	kIx,	,
F.	F.	kA	F.
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Theory	Theor	k1gInPc1	Theor
of	of	k?	of
Photons	Photons	k1gInSc1	Photons
and	and	k?	and
Electrons	Electrons	k1gInSc1	Electrons
<g/>
.	.	kIx.	.
</s>
<s>
Springer-Verlag	Springer-Verlag	k1gMnSc1	Springer-Verlag
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
387	[number]	k4	387
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7295	[number]	k4	7295
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Miller	Miller	k1gMnSc1	Miller
<g/>
,	,	kIx,	,
Arthur	Arthur	k1gMnSc1	Arthur
I.	I.	kA	I.
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Early	earl	k1gMnPc4	earl
Quantum	Quantum	k1gNnSc1	Quantum
Electrodynamics	Electrodynamics	k1gInSc1	Electrodynamics
:	:	kIx,	:
A	a	k9	a
Sourcebook	Sourcebook	k1gInSc1	Sourcebook
<g/>
.	.	kIx.	.
</s>
<s>
Cambridge	Cambridge	k1gFnSc1	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
521	[number]	k4	521
<g/>
-	-	kIx~	-
<g/>
56891	[number]	k4	56891
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Schweber	Schweber	k1gMnSc1	Schweber
<g/>
,	,	kIx,	,
Silvian	Silvian	k1gMnSc1	Silvian
<g/>
,	,	kIx,	,
S.	S.	kA	S.
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
QED	QED	kA	QED
and	and	k?	and
the	the	k?	the
Men	Men	k1gFnSc1	Men
Who	Who	k1gFnSc1	Who
Made	Made	k1gFnSc1	Made
It.	It.	k1gFnSc1	It.
Princeton	Princeton	k1gInSc4	Princeton
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
691	[number]	k4	691
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3327	[number]	k4	3327
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Schwinger	Schwinger	k1gMnSc1	Schwinger
<g/>
,	,	kIx,	,
Julian	Julian	k1gMnSc1	Julian
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Selected	Selected	k1gInSc1	Selected
Papers	Papersa	k1gFnPc2	Papersa
on	on	k3xPp3gMnSc1	on
Quantum	Quantum	k1gNnSc4	Quantum
Electrodynamics	Electrodynamics	k1gInSc4	Electrodynamics
<g/>
.	.	kIx.	.
</s>
<s>
Dover	Dover	k1gInSc1	Dover
Publications	Publications	k1gInSc1	Publications
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
486	[number]	k4	486
<g/>
-	-	kIx~	-
<g/>
60444	[number]	k4	60444
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Greiner	Greiner	k1gMnSc1	Greiner
<g/>
,	,	kIx,	,
Walter	Walter	k1gMnSc1	Walter
<g/>
;	;	kIx,	;
Bromley	Bromlea	k1gFnPc1	Bromlea
<g/>
,	,	kIx,	,
D.A.	D.A.	k1gMnSc1	D.A.
<g/>
,	,	kIx,	,
<g/>
Müller	Müller	k1gMnSc1	Müller
<g/>
,	,	kIx,	,
Berndt	Berndt	k1gMnSc1	Berndt
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gauge	Gauge	k1gFnSc1	Gauge
Theory	Theora	k1gFnSc2	Theora
of	of	k?	of
Weak	Weak	k1gInSc1	Weak
Interactions	Interactions	k1gInSc1	Interactions
<g/>
.	.	kIx.	.
</s>
<s>
Springer	Springer	k1gMnSc1	Springer
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
540	[number]	k4	540
<g/>
-	-	kIx~	-
<g/>
67672	[number]	k4	67672
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Kane	kanout	k5eAaImIp3nS	kanout
<g/>
,	,	kIx,	,
Gordon	Gordon	k1gMnSc1	Gordon
<g/>
,	,	kIx,	,
L.	L.	kA	L.
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Modern	Modern	k1gInSc1	Modern
Elementary	Elementara	k1gFnSc2	Elementara
Particle	Particle	k1gFnSc2	Particle
Physics	Physicsa	k1gFnPc2	Physicsa
<g/>
.	.	kIx.	.
</s>
<s>
Westview	Westview	k?	Westview
Press	Press	k1gInSc1	Press
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
201	[number]	k4	201
<g/>
-	-	kIx~	-
<g/>
62460	[number]	k4	62460
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
