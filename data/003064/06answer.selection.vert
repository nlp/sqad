<s>
Kvantová	kvantový	k2eAgFnSc1d1	kvantová
elektrodynamika	elektrodynamika	k1gFnSc1	elektrodynamika
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
zkratka	zkratka	k1gFnSc1	zkratka
"	"	kIx"	"
<g/>
QED	QED	kA	QED
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nauka	nauka	k1gFnSc1	nauka
o	o	k7c6	o
pohybu	pohyb	k1gInSc6	pohyb
elektrických	elektrický	k2eAgInPc2d1	elektrický
nábojů	náboj	k1gInPc2	náboj
(	(	kIx(	(
<g/>
nabitých	nabitý	k2eAgNnPc2d1	nabité
těles	těleso	k1gNnPc2	těleso
<g/>
)	)	kIx)	)
v	v	k7c6	v
obecně	obecně	k6eAd1	obecně
proměnných	proměnný	k2eAgFnPc6d1	proměnná
elektromagnetických	elektromagnetický	k2eAgFnPc6d1	elektromagnetická
polích	pole	k1gFnPc6	pole
<g/>
.	.	kIx.	.
</s>
