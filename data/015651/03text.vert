<s>
Malinska	Malinsko	k1gNnPc1
</s>
<s>
Malinska	Malinsko	k1gNnPc1
MalinskaPoloha	MalinskaPoloh	k1gMnSc2
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
45	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
26	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
31	#num#	k4
<g/>
′	′	k?
<g/>
45	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
11	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
+1	+1	k4
Stát	stát	k1gInSc1
</s>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1
Chorvatsko	Chorvatsko	k1gNnSc1
Region	region	k1gInSc1
</s>
<s>
Chorvatské	chorvatský	k2eAgNnSc1d1
Přímoří	Přímoří	k1gNnSc1
(	(	kIx(
<g/>
Dubašnica	Dubašnica	k1gFnSc1
<g/>
)	)	kIx)
Župa	župa	k1gFnSc1
</s>
<s>
Přímořsko-gorskokotarská	Přímořsko-gorskokotarský	k2eAgFnSc1d1
Opčina	Opčina	k1gFnSc1
</s>
<s>
Malinska-Dubašnica	Malinska-Dubašnica	k6eAd1
</s>
<s>
Malinska	Malinsko	k1gNnPc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
965	#num#	k4
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
Etnické	etnický	k2eAgNnSc1d1
složení	složení	k1gNnSc1
</s>
<s>
Chorvati	Chorvat	k1gMnPc1
Náboženské	náboženský	k2eAgFnSc2d1
složení	složení	k1gNnSc3
</s>
<s>
Křesťané	křesťan	k1gMnPc1
Správa	správa	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
Vesnice	vesnice	k1gFnSc1
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
+385	+385	k4
051	#num#	k4
PSČ	PSČ	kA
</s>
<s>
51	#num#	k4
511	#num#	k4
Malinska	Malinsko	k1gNnSc2
Označení	označení	k1gNnSc2
vozidel	vozidlo	k1gNnPc2
</s>
<s>
RI	RI	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Malinska	Malinsko	k1gNnPc1
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
Malinsca	Malinsc	k2eAgFnSc1d1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vesnice	vesnice	k1gFnSc1
a	a	k8xC
známé	známý	k2eAgNnSc1d1
přímořské	přímořský	k2eAgNnSc1d1
letovisko	letovisko	k1gNnSc1
v	v	k7c6
Chorvatsku	Chorvatsko	k1gNnSc6
v	v	k7c6
Přímořsko-gorskokotarské	Přímořsko-gorskokotarský	k2eAgFnSc6d1
župě	župa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
střediskem	středisko	k1gNnSc7
opčiny	opčina	k1gMnSc2
Malinska-Dubašnica	Malinska-Dubašnicus	k1gMnSc2
a	a	k8xC
jednou	jeden	k4xCgFnSc7
z	z	k7c2
mnoha	mnoho	k4c2
vesnic	vesnice	k1gFnPc2
tvořící	tvořící	k2eAgNnSc4d1
sídlo	sídlo	k1gNnSc4
Dubašnica	Dubašnic	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c6
ostrově	ostrov	k1gInSc6
Krku	Krk	k1gInSc2
<g/>
,	,	kIx,
asi	asi	k9
40	#num#	k4
km	km	kA
od	od	k7c2
Rijeky	Rijeka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
celkem	celkem	k6eAd1
965	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Sousedními	sousední	k2eAgNnPc7d1
letovisky	letovisko	k1gNnPc7
jsou	být	k5eAaImIp3nP
Milčetići	Milčetić	k1gMnPc1
a	a	k8xC
Njivice	Njivice	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Malinska	Malinsko	k1gNnSc2
na	na	k7c6
bosenské	bosenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4230157-9	4230157-9	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
248156954	#num#	k4
</s>
