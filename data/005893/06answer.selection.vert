<s>
Název	název	k1gInSc1	název
hnědý	hnědý	k2eAgMnSc1d1	hnědý
trpaslík	trpaslík	k1gMnSc1	trpaslík
zavedla	zavést	k5eAaPmAgFnS	zavést
Jill	Jill	k1gInSc4	Jill
Tarterová	Tarterová	k1gFnSc1	Tarterová
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odlišila	odlišit	k5eAaPmAgFnS	odlišit
tyto	tento	k3xDgInPc4	tento
subhvězdné	subhvězdný	k2eAgInPc4d1	subhvězdný
objekty	objekt	k1gInPc4	objekt
od	od	k7c2	od
červených	červený	k2eAgMnPc2d1	červený
trpaslíků	trpaslík	k1gMnPc2	trpaslík
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
skutečné	skutečný	k2eAgInPc1d1	skutečný
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
málo	málo	k6eAd1	málo
hmotné	hmotný	k2eAgFnPc4d1	hmotná
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
