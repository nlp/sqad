<s>
Pilot	pilot	k1gMnSc1	pilot
je	být	k5eAaImIp3nS	být
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ovládá	ovládat	k5eAaImIp3nS	ovládat
létající	létající	k2eAgInSc4d1	létající
stroj	stroj	k1gInSc4	stroj
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
letadlo	letadlo	k1gNnSc4	letadlo
<g/>
,	,	kIx,	,
vrtulník	vrtulník	k1gInSc4	vrtulník
<g/>
,	,	kIx,	,
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
loď	loď	k1gFnSc4	loď
nebo	nebo	k8xC	nebo
jiný	jiný	k2eAgInSc4d1	jiný
<g/>
,	,	kIx,	,
i	i	k8xC	i
nemotorový	motorový	k2eNgInSc1d1	nemotorový
letecký	letecký	k2eAgInSc1d1	letecký
prostředek	prostředek	k1gInSc1	prostředek
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
na	na	k7c4	na
automatické	automatický	k2eAgInPc4d1	automatický
ovládací	ovládací	k2eAgInPc4d1	ovládací
přístroje	přístroj	k1gInPc4	přístroj
<g/>
.	.	kIx.	.
</s>
