<s>
Pilot	pilot	k1gMnSc1	pilot
je	být	k5eAaImIp3nS	být
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ovládá	ovládat	k5eAaImIp3nS	ovládat
létající	létající	k2eAgInSc4d1	létající
stroj	stroj	k1gInSc4	stroj
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
letadlo	letadlo	k1gNnSc4	letadlo
<g/>
,	,	kIx,	,
vrtulník	vrtulník	k1gInSc4	vrtulník
<g/>
,	,	kIx,	,
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
loď	loď	k1gFnSc4	loď
nebo	nebo	k8xC	nebo
jiný	jiný	k2eAgInSc4d1	jiný
<g/>
,	,	kIx,	,
i	i	k8xC	i
nemotorový	motorový	k2eNgInSc1d1	nemotorový
letecký	letecký	k2eAgInSc1d1	letecký
prostředek	prostředek	k1gInSc1	prostředek
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
na	na	k7c4	na
automatické	automatický	k2eAgInPc4d1	automatický
ovládací	ovládací	k2eAgInPc4d1	ovládací
přístroje	přístroj	k1gInPc4	přístroj
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
pilota	pilot	k1gMnSc4	pilot
považován	považován	k2eAgMnSc1d1	považován
držitel	držitel	k1gMnSc1	držitel
platného	platný	k2eAgInSc2d1	platný
pilotního	pilotní	k2eAgInSc2d1	pilotní
průkazu	průkaz	k1gInSc2	průkaz
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pilotní	pilotní	k2eAgMnSc1d1	pilotní
žák	žák	k1gMnSc1	žák
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
případě	případ	k1gInSc6	případ
tato	tento	k3xDgFnSc1	tento
osoba	osoba	k1gFnSc1	osoba
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
plynulý	plynulý	k2eAgInSc4d1	plynulý
let	let	k1gInSc4	let
a	a	k8xC	a
věci	věc	k1gFnPc4	věc
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
spojené	spojený	k2eAgInPc4d1	spojený
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
pilot	pilota	k1gFnPc2	pilota
se	se	k3xPyFc4	se
vžilo	vžít	k5eAaPmAgNnS	vžít
také	také	k9	také
pro	pro	k7c4	pro
řidiče	řidič	k1gMnPc4	řidič
formulí	formule	k1gFnPc2	formule
<g/>
.	.	kIx.	.
</s>
<s>
Pilot	pilot	k1gMnSc1	pilot
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jak	jak	k6eAd1	jak
na	na	k7c4	na
profesionální	profesionální	k2eAgFnSc4d1	profesionální
<g/>
,	,	kIx,	,
tak	tak	k9	tak
na	na	k7c6	na
amatérské	amatérský	k2eAgFnSc6d1	amatérská
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
piloti	pilot	k1gMnPc1	pilot
rozdělováni	rozdělován	k2eAgMnPc1d1	rozdělován
na	na	k7c4	na
sportovní	sportovní	k2eAgMnPc4d1	sportovní
<g/>
,	,	kIx,	,
vojenské	vojenský	k2eAgFnPc1d1	vojenská
<g/>
,	,	kIx,	,
dopravní	dopravní	k2eAgFnSc1d1	dopravní
atd.	atd.	kA	atd.
Práce	práce	k1gFnSc1	práce
pilota	pilota	k1gFnSc1	pilota
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
různí	různit	k5eAaImIp3nP	různit
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc4	jaký
prostředek	prostředek	k1gInSc4	prostředek
pilotuje	pilotovat	k5eAaImIp3nS	pilotovat
<g/>
,	,	kIx,	,
základy	základ	k1gInPc1	základ
jsou	být	k5eAaImIp3nP	být
ovšem	ovšem	k9	ovšem
stejné	stejný	k2eAgFnPc1d1	stejná
<g/>
.	.	kIx.	.
</s>
<s>
Pilot	pilot	k1gMnSc1	pilot
letadla	letadlo	k1gNnSc2	letadlo
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
"	"	kIx"	"
<g/>
řidiče	řidič	k1gMnSc2	řidič
<g/>
"	"	kIx"	"
také	také	k9	také
kontrolor	kontrolor	k1gMnSc1	kontrolor
stroje	stroj	k1gInSc2	stroj
jak	jak	k8xS	jak
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
startem	start	k1gInSc7	start
i	i	k9	i
za	za	k7c2	za
letu	let	k1gInSc2	let
musí	muset	k5eAaImIp3nS	muset
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
kromě	kromě	k7c2	kromě
jiného	jiné	k1gNnSc2	jiné
průtok	průtok	k1gInSc4	průtok
paliva	palivo	k1gNnSc2	palivo
<g/>
,	,	kIx,	,
teploty	teplota	k1gFnSc2	teplota
oleje	olej	k1gInSc2	olej
<g/>
,	,	kIx,	,
skříní	skříň	k1gFnPc2	skříň
motoru	motor	k1gInSc2	motor
<g/>
,	,	kIx,	,
turbín	turbína	k1gFnPc2	turbína
<g/>
,	,	kIx,	,
hydrauliku	hydraulika	k1gFnSc4	hydraulika
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc4	rychlost
<g/>
,	,	kIx,	,
směr	směr	k1gInSc4	směr
atd.	atd.	kA	atd.
Mimo	mimo	k6eAd1	mimo
to	ten	k3xDgNnSc4	ten
musí	muset	k5eAaImIp3nP	muset
komunikovat	komunikovat	k5eAaImF	komunikovat
s	s	k7c7	s
řízením	řízení	k1gNnSc7	řízení
letového	letový	k2eAgInSc2d1	letový
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
letadlech	letadlo	k1gNnPc6	letadlo
je	být	k5eAaImIp3nS	být
veškerá	veškerý	k3xTgFnSc1	veškerý
tato	tento	k3xDgFnSc1	tento
činnost	činnost	k1gFnSc1	činnost
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
mezi	mezi	k7c4	mezi
dva	dva	k4xCgInPc4	dva
piloty	pilot	k1gInPc4	pilot
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
napomáhají	napomáhat	k5eAaImIp3nP	napomáhat
řízení	řízení	k1gNnSc4	řízení
také	také	k9	také
autopiloty	autopilot	k1gMnPc7	autopilot
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
komunikačním	komunikační	k2eAgInSc7d1	komunikační
jazykem	jazyk	k1gInSc7	jazyk
pilotů	pilot	k1gMnPc2	pilot
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
úrovni	úroveň	k1gFnSc6	úroveň
je	být	k5eAaImIp3nS	být
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Začít	začít	k5eAaPmF	začít
pilotovat	pilotovat	k5eAaImF	pilotovat
motorové	motorový	k2eAgNnSc4d1	motorové
letadlo	letadlo	k1gNnSc4	letadlo
lze	lze	k6eAd1	lze
od	od	k7c2	od
15	[number]	k4	15
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
praktického	praktický	k2eAgInSc2d1	praktický
výcviku	výcvik	k1gInSc2	výcvik
pilotního	pilotní	k2eAgMnSc2d1	pilotní
žáka	žák	k1gMnSc2	žák
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
pilotního	pilotní	k2eAgInSc2d1	pilotní
průkazu	průkaz	k1gInSc2	průkaz
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
18	[number]	k4	18
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
písemný	písemný	k2eAgInSc4d1	písemný
souhlas	souhlas	k1gInSc4	souhlas
zákonných	zákonný	k2eAgMnPc2d1	zákonný
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zájemce	zájemce	k1gMnSc1	zájemce
musí	muset	k5eAaImIp3nS	muset
zvládnout	zvládnout	k5eAaPmF	zvládnout
pilotní	pilotní	k2eAgInSc4d1	pilotní
výcvik	výcvik	k1gInSc4	výcvik
a	a	k8xC	a
nalétat	nalétat	k5eAaPmF	nalétat
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
počet	počet	k1gInSc4	počet
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
plnohodnotným	plnohodnotný	k2eAgMnSc7d1	plnohodnotný
pilotem	pilot	k1gMnSc7	pilot
<g/>
.	.	kIx.	.
</s>
<s>
Výcvik	výcvik	k1gInSc1	výcvik
bývá	bývat	k5eAaImIp3nS	bývat
časově	časově	k6eAd1	časově
i	i	k9	i
finančně	finančně	k6eAd1	finančně
náročný	náročný	k2eAgInSc1d1	náročný
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
typech	typ	k1gInPc6	typ
letounů	letoun	k1gInPc2	letoun
ULL	ULL	kA	ULL
(	(	kIx(	(
<g/>
Ultralight	Ultralight	k1gMnSc1	Ultralight
<g/>
)	)	kIx)	)
a	a	k8xC	a
GLD	GLD	kA	GLD
(	(	kIx(	(
<g/>
větroně	větroň	k1gInPc1	větroň
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
smí	smět	k5eAaImIp3nS	smět
létat	létat	k5eAaImF	létat
od	od	k7c2	od
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
