<s>
INRI	INRI	kA
</s>
<s>
Tabulka	tabulka	k1gFnSc1
na	na	k7c6
krucifixu	krucifix	k1gInSc6
s	s	k7c7
trojjazyčným	trojjazyčný	k2eAgInSc7d1
nápisem	nápis	k1gInSc7
י	י	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
P	P	kA
1	#num#	k4
<g/>
]	]	kIx)
Ι	Ι	k?
a	a	k8xC
I	i	k9
<g/>
·	·	k?
<g/>
N	N	kA
<g/>
·	·	k?
<g/>
R	R	kA
<g/>
·	·	k?
<g/>
I	i	k8xC
<g/>
·	·	k?
</s>
<s>
Nápis	nápis	k1gInSc1
INRI	INRI	kA
je	být	k5eAaImIp3nS
zkratkou	zkratka	k1gFnSc7
latinského	latinský	k2eAgInSc2d1
Iesus	Iesus	k1gInSc1
Nazarenus	Nazarenus	k1gMnSc1
Rex	Rex	k1gFnPc2
Iudaeorum	Iudaeorum	k1gInSc1
<g/>
,	,	kIx,
česky	česky	k6eAd1
Ježíš	ježit	k5eAaImIp2nS
Nazaretský	nazaretský	k2eAgMnSc1d1
<g/>
,	,	kIx,
král	král	k1gMnSc1
židovský	židovský	k2eAgMnSc1d1
a	a	k8xC
bývá	bývat	k5eAaImIp3nS
tradičně	tradičně	k6eAd1
zobrazován	zobrazovat	k5eAaImNgInS
na	na	k7c6
krucifixech	krucifix	k1gInPc6
a	a	k8xC
jiných	jiný	k2eAgNnPc6d1
zobrazeních	zobrazení	k1gNnPc6
ukřižovaného	ukřižovaný	k2eAgMnSc2d1
Ježíše	Ježíš	k1gMnSc2
Krista	Kristus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
a	a	k8xC
význam	význam	k1gInSc1
</s>
<s>
INRI	INRI	kA
na	na	k7c6
Grünewaldově	Grünewaldův	k2eAgNnSc6d1
Ukřižování	ukřižování	k1gNnSc6
</s>
<s>
Nápis	nápis	k1gInSc1
nechal	nechat	k5eAaPmAgInS
na	na	k7c4
Ježíšův	Ježíšův	k2eAgInSc4d1
kříž	kříž	k1gInSc4
umístit	umístit	k5eAaPmF
Pilát	pilát	k1gInSc1
Pontský	pontský	k2eAgInSc1d1
podle	podle	k7c2
Janova	Janův	k2eAgNnSc2d1
evangelia	evangelium	k1gNnSc2
(	(	kIx(
<g/>
v	v	k7c6
nezkráceném	zkrácený	k2eNgNnSc6d1
znění	znění	k1gNnSc6
<g/>
)	)	kIx)
ve	v	k7c6
třech	tři	k4xCgInPc6
jazycích	jazyk	k1gInPc6
–	–	k?
hebrejštině	hebrejština	k1gFnSc6
<g/>
,	,	kIx,
latině	latina	k1gFnSc6
a	a	k8xC
řečtině	řečtina	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Markovo	Markův	k2eAgNnSc1d1
evangelium	evangelium	k1gNnSc1
pouze	pouze	k6eAd1
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Ježíšovo	Ježíšův	k2eAgNnSc4d1
provinění	provinění	k1gNnPc4
označoval	označovat	k5eAaImAgInS
nápis	nápis	k1gInSc1
Král	Král	k1gMnSc1
Židů	Žid	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ježíš	Ježíš	k1gMnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
řeckém	řecký	k2eAgInSc6d1
originále	originál	k1gInSc6
označen	označit	k5eAaPmNgMnS
titulem	titul	k1gInSc7
Nazóraios	Nazóraios	k1gMnSc1
(	(	kIx(
<g/>
nazórejský	nazórejský	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
latinská	latinský	k2eAgFnSc1d1
podoba	podoba	k1gFnSc1
Nazarenus	Nazarenus	k1gInSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
jiné	jiný	k2eAgFnSc2d1
řecké	řecký	k2eAgFnSc2d1
varianty	varianta	k1gFnSc2
souběžně	souběžně	k6eAd1
používané	používaný	k2eAgInPc1d1
v	v	k7c6
Novém	nový	k2eAgInSc6d1
zákoně	zákon	k1gInSc6
Nazarénos	Nazarénos	k1gInSc1
(	(	kIx(
<g/>
nazarénský	nazarénský	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původ	původ	k1gInSc1
těchto	tento	k3xDgNnPc2
jmen	jméno	k1gNnPc2
je	být	k5eAaImIp3nS
nejistý	jistý	k2eNgInSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
obvykle	obvykle	k6eAd1
se	se	k3xPyFc4
překládají	překládat	k5eAaImIp3nP
podle	podle	k7c2
předpokládaného	předpokládaný	k2eAgInSc2d1
významu	význam	k1gInSc2
„	„	k?
<g/>
z	z	k7c2
Nazaretu	Nazaret	k1gInSc2
<g/>
“	“	k?
podle	podle	k7c2
vesnice	vesnice	k1gFnSc2
či	či	k8xC
města	město	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
Ježíš	Ježíš	k1gMnSc1
žil	žít	k5eAaImAgMnS
a	a	k8xC
vyrůstal	vyrůstat	k5eAaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Titul	titul	k1gInSc1
„	„	k?
<g/>
král	král	k1gMnSc1
židovský	židovský	k2eAgMnSc1d1
<g/>
“	“	k?
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c4
Mesiáše	Mesiáš	k1gMnSc4
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
Mašiach	Mašiach	k1gMnSc1
<g/>
,	,	kIx,
„	„	k?
<g/>
pomazaný	pomazaný	k2eAgInSc1d1
<g/>
“	“	k?
<g/>
,	,	kIx,
řecky	řecky	k6eAd1
Christos	Christos	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
židy	žid	k1gMnPc4
očekávaného	očekávaný	k2eAgMnSc2d1
krále	král	k1gMnSc2
<g/>
,	,	kIx,
za	za	k7c4
kterého	který	k3yQgMnSc4,k3yIgMnSc4,k3yRgMnSc4
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
Ježíš	Ježíš	k1gMnSc1
vydávat	vydávat	k5eAaPmF,k5eAaImF
a	a	k8xC
proto	proto	k8xC
byl	být	k5eAaImAgMnS
Pilátem	Pilát	k1gMnSc7
Pontským	pontský	k2eAgMnSc7d1
na	na	k7c4
nátlak	nátlak	k1gInSc4
davu	dav	k1gInSc2
odsouzen	odsoudit	k5eAaPmNgMnS,k5eAaImNgMnS
k	k	k7c3
ukřižování	ukřižování	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Varianty	variant	k1gInPc4
a	a	k8xC
další	další	k2eAgInPc4d1
významy	význam	k1gInPc4
</s>
<s>
Plné	plný	k2eAgNnSc1d1
znění	znění	k1gNnSc1
nápisu	nápis	k1gInSc2
na	na	k7c6
krucifixu	krucifix	k1gInSc6
v	v	k7c6
klášteře	klášter	k1gInSc6
v	v	k7c6
Gurku	Gurk	k1gInSc6
</s>
<s>
Někdy	někdy	k6eAd1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
také	také	k9
podoba	podoba	k1gFnSc1
JNRJ	JNRJ	kA
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
pozdějšího	pozdní	k2eAgInSc2d2
latinského	latinský	k2eAgInSc2d1
pravopisu	pravopis	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Východní	východní	k2eAgFnSc1d1
církev	církev	k1gFnSc1
užívá	užívat	k5eAaImIp3nS
namísto	namísto	k7c2
toho	ten	k3xDgMnSc2
písmen	písmeno	k1gNnPc2
INBI	INBI	kA
<g/>
,	,	kIx,
vycházející	vycházející	k2eAgFnSc1d1
z	z	k7c2
řecké	řecký	k2eAgFnSc2d1
podoby	podoba	k1gFnSc2
nápisu	nápis	k1gInSc2
(	(	kIx(
<g/>
Ἰ	Ἰ	k?
ὁ	ὁ	k?
Ν	Ν	k?
ὁ	ὁ	k?
β	β	k?
τ	τ	k?
Ἰ	Ἰ	k?
–	–	k?
Iésús	Iésús	k1gInSc1
ho	on	k3xPp3gInSc4
Nazóraios	Nazóraios	k1gInSc4
ho	on	k3xPp3gInSc4
basileus	basileus	k1gInSc4
tón	tón	k1gInSc1
Iúdaión	Iúdaión	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
východní	východní	k2eAgFnSc2d1
(	(	kIx(
<g/>
např.	např.	kA
pravoslavní	pravoslavný	k2eAgMnPc1d1
<g/>
)	)	kIx)
věřící	věřící	k1gMnPc1
uvádějí	uvádět	k5eAaImIp3nP
namísto	namísto	k7c2
toho	ten	k3xDgInSc2
např.	např.	kA
…	…	k?
ho	on	k3xPp3gInSc4
basileus	basileus	k1gInSc4
tú	tú	k0
kosmú	kosmú	k?
(	(	kIx(
<g/>
…	…	k?
král	král	k1gMnSc1
všehomíra	všehomír	k1gInSc2
<g/>
)	)	kIx)
anebo	anebo	k8xC
ho	on	k3xPp3gInSc4
basileus	basileus	k1gInSc4
tés	tés	k?
doxés	doxés	k1gInSc1
(	(	kIx(
<g/>
Král	Král	k1gMnSc1
slávy	sláva	k1gFnSc2
<g/>
)	)	kIx)
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ikony	ikon	k1gInPc1
vyjadřují	vyjadřovat	k5eAaImIp3nP
duchovní	duchovní	k2eAgFnSc4d1
realitu	realita	k1gFnSc4
a	a	k8xC
ne	ne	k9
fyzickou	fyzický	k2eAgFnSc4d1
realitu	realita	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Později	pozdě	k6eAd2
byl	být	k5eAaImAgMnS
zkratce	zkratka	k1gFnSc6
INRI	INRI	kA
přidán	přidán	k2eAgInSc4d1
ještě	ještě	k6eAd1
jeden	jeden	k4xCgInSc1
význam	význam	k1gInSc1
–	–	k?
In	In	k1gFnSc1
nobis	nobis	k1gFnSc4
regnat	regnat	k5eAaImF,k5eAaPmF
Iesus	Iesus	k1gInSc4
<g/>
,	,	kIx,
V	v	k7c6
nás	my	k3xPp1nPc6
kraluje	kralovat	k5eAaImIp3nS
Ježíš	Ježíš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Alchymisté	alchymista	k1gMnPc1
zkratce	zkratka	k1gFnSc6
vytvořili	vytvořit	k5eAaPmAgMnP
jiný	jiný	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
,	,	kIx,
založený	založený	k2eAgInSc4d1
na	na	k7c6
představě	představa	k1gFnSc6
živlů	živel	k1gInPc2
–	–	k?
Igne	Ign	k1gMnSc2
Natura	Natur	k1gMnSc2
Renovatur	Renovatura	k1gFnPc2
Integra	Integr	k1gMnSc2
<g/>
,	,	kIx,
Veškerá	veškerý	k3xTgFnSc1
příroda	příroda	k1gFnSc1
se	se	k3xPyFc4
obnovuje	obnovovat	k5eAaImIp3nS
ohněm	oheň	k1gInSc7
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Z	z	k7c2
hebrejského	hebrejský	k2eAgInSc2d1
י	י	k?
מ	מ	k?
מ	מ	k?
ה	ה	k?
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Jan	Jan	k1gMnSc1
19	#num#	k4
<g/>
,	,	kIx,
19	#num#	k4
<g/>
–	–	k?
<g/>
20	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Mk	Mk	k1gFnSc1
15	#num#	k4
<g/>
,	,	kIx,
26	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Novotný	Novotný	k1gMnSc1
<g/>
,	,	kIx,
Adolf	Adolf	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biblický	biblický	k2eAgInSc4d1
slovník	slovník	k1gInSc4
1	#num#	k4
A	a	k8xC
<g/>
–	–	k?
<g/>
R	R	kA
Praha	Praha	k1gFnSc1
1956	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
479	#num#	k4
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc4
Nazaretský	nazaretský	k2eAgMnSc1d1
<g/>
↑	↑	k?
INRI	INRI	kA
na	na	k7c4
Zkratky	zkratka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Svatý	svatý	k2eAgMnSc1d1
Kříž	Kříž	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
INRI	INRI	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
J.	J.	kA
N.	N.	kA
R.	R.	kA
J.	J.	kA
v	v	k7c6
Ottově	Ottův	k2eAgInSc6d1
slovníku	slovník	k1gInSc6
naučném	naučný	k2eAgInSc6d1
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
</s>
