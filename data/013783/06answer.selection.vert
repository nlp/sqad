<s>
Nápis	nápis	k1gInSc1
INRI	INRI	kA
je	být	k5eAaImIp3nS
zkratkou	zkratka	k1gFnSc7
latinského	latinský	k2eAgInSc2d1
Iesus	Iesus	k1gInSc1
Nazarenus	Nazarenus	k1gMnSc1
Rex	Rex	k1gFnPc2
Iudaeorum	Iudaeorum	k1gInSc1
<g/>
,	,	kIx,
česky	česky	k6eAd1
Ježíš	ježit	k5eAaImIp2nS
Nazaretský	nazaretský	k2eAgMnSc1d1
<g/>
,	,	kIx,
král	král	k1gMnSc1
židovský	židovský	k2eAgMnSc1d1
a	a	k8xC
bývá	bývat	k5eAaImIp3nS
tradičně	tradičně	k6eAd1
zobrazován	zobrazovat	k5eAaImNgInS
na	na	k7c6
krucifixech	krucifix	k1gInPc6
a	a	k8xC
jiných	jiný	k2eAgNnPc6d1
zobrazeních	zobrazení	k1gNnPc6
ukřižovaného	ukřižovaný	k2eAgMnSc2d1
Ježíše	Ježíš	k1gMnSc2
Krista	Kristus	k1gMnSc2
<g/>
.	.	kIx.
</s>