<p>
<s>
Kruh	kruh	k1gInSc1	kruh
je	být	k5eAaImIp3nS	být
rovinný	rovinný	k2eAgInSc1d1	rovinný
geometrický	geometrický	k2eAgInSc1d1	geometrický
útvar	útvar	k1gInSc1	útvar
<g/>
,	,	kIx,	,
omezený	omezený	k2eAgInSc4d1	omezený
kružnicí	kružnice	k1gFnSc7	kružnice
<g/>
.	.	kIx.	.
</s>
<s>
Kruh	kruh	k1gInSc1	kruh
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
svým	svůj	k3xOyFgInSc7	svůj
středem	střed	k1gInSc7	střed
S	s	k7c7	s
a	a	k8xC	a
poloměrem	poloměr	k1gInSc7	poloměr
r	r	kA	r
<g/>
:	:	kIx,	:
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgInPc2	všecek
bodů	bod	k1gInPc2	bod
roviny	rovina	k1gFnSc2	rovina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
od	od	k7c2	od
středu	střed	k1gInSc2	střed
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
menší	malý	k2eAgFnSc2d2	menší
nebo	nebo	k8xC	nebo
rovnou	rovnou	k6eAd1	rovnou
poloměru	poloměr	k1gInSc2	poloměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgInPc1d1	základní
vzorce	vzorec	k1gInPc1	vzorec
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Pro	pro	k7c4	pro
poloměr	poloměr	k1gInSc4	poloměr
===	===	k?	===
</s>
</p>
<p>
<s>
Obvod	obvod	k1gInSc1	obvod
o	o	k7c6	o
kruhu	kruh	k1gInSc6	kruh
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
vzorcem	vzorec	k1gInSc7	vzorec
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
o	o	k0	o
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
r	r	kA	r
<g/>
\	\	kIx~	\
<g/>
,,	,,	k?	,,
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
π	π	k?	π
označuje	označovat	k5eAaImIp3nS	označovat
číslo	číslo	k1gNnSc4	číslo
pí	pí	k1gNnSc2	pí
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
plocha	plocha	k1gFnSc1	plocha
S	s	k7c7	s
vzorcem	vzorec	k1gInSc7	vzorec
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
r	r	kA	r
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Pro	pro	k7c4	pro
průměr	průměr	k1gInSc4	průměr
===	===	k?	===
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
bychom	by	kYmCp1nP	by
uvažovali	uvažovat	k5eAaImAgMnP	uvažovat
poloměr	poloměr	k1gInSc4	poloměr
(	(	kIx(	(
<g/>
rádius	rádius	k1gInSc1	rádius
<g/>
)	)	kIx)	)
r	r	kA	r
jako	jako	k8xC	jako
polovinu	polovina	k1gFnSc4	polovina
průměru	průměr	k1gInSc2	průměr
d	d	k?	d
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
dosadili	dosadit	k5eAaPmAgMnP	dosadit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
r	r	kA	r
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
r	r	kA	r
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
tak	tak	k9	tak
by	by	kYmCp3nP	by
vzorce	vzorec	k1gInPc1	vzorec
vypadaly	vypadat	k5eAaPmAgInP	vypadat
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
obvod	obvod	k1gInSc1	obvod
o	o	k7c6	o
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
o	o	k7c6	o
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
o	o	k0	o
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
d	d	k?	d
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
d	d	k?	d
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
takto	takto	k6eAd1	takto
pro	pro	k7c4	pro
plochu	plocha	k1gFnSc4	plocha
S	s	k7c7	s
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
π	π	k?	π
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
4	[number]	k4	4
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
pi	pi	k0	pi
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgInPc1d1	další
pojmy	pojem	k1gInPc1	pojem
==	==	k?	==
</s>
</p>
<p>
<s>
Část	část	k1gFnSc1	část
kruhu	kruh	k1gInSc2	kruh
vymezená	vymezený	k2eAgFnSc1d1	vymezená
dvěma	dva	k4xCgInPc7	dva
průvodiči	průvodič	k1gInPc7	průvodič
je	být	k5eAaImIp3nS	být
kruhová	kruhový	k2eAgFnSc1d1	kruhová
výseč	výseč	k1gFnSc1	výseč
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
kruhu	kruh	k1gInSc2	kruh
omezená	omezený	k2eAgFnSc1d1	omezená
sečnou	sečna	k1gFnSc7	sečna
je	být	k5eAaImIp3nS	být
kruhová	kruhový	k2eAgFnSc1d1	kruhová
úseč	úseč	k1gFnSc1	úseč
<g/>
.	.	kIx.	.
</s>
<s>
Plocha	plocha	k1gFnSc1	plocha
vymezená	vymezený	k2eAgFnSc1d1	vymezená
dvěma	dva	k4xCgFnPc7	dva
soustřednými	soustředný	k2eAgFnPc7d1	soustředná
kružnicemi	kružnice	k1gFnPc7	kružnice
o	o	k7c6	o
nestejném	stejný	k2eNgInSc6d1	nestejný
poloměru	poloměr	k1gInSc6	poloměr
je	být	k5eAaImIp3nS	být
mezikruží	mezikruží	k1gNnSc1	mezikruží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kvadratura	kvadratura	k1gFnSc1	kvadratura
kruhu	kruh	k1gInSc2	kruh
==	==	k?	==
</s>
</p>
<p>
<s>
Kvadratura	kvadratura	k1gFnSc1	kvadratura
kruhu	kruh	k1gInSc2	kruh
je	být	k5eAaImIp3nS	být
konstrukční	konstrukční	k2eAgFnSc1d1	konstrukční
úloha	úloha	k1gFnSc1	úloha
<g/>
:	:	kIx,	:
sestrojit	sestrojit	k5eAaPmF	sestrojit
k	k	k7c3	k
danému	daný	k2eAgInSc3d1	daný
kruhu	kruh	k1gInSc3	kruh
čtverec	čtverec	k1gInSc4	čtverec
o	o	k7c6	o
stejném	stejný	k2eAgInSc6d1	stejný
obsahu	obsah	k1gInSc6	obsah
pouze	pouze	k6eAd1	pouze
pomocí	pomocí	k7c2	pomocí
pravítka	pravítko	k1gNnSc2	pravítko
a	a	k8xC	a
kružítka	kružítko	k1gNnSc2	kružítko
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
úloha	úloha	k1gFnSc1	úloha
obecně	obecně	k6eAd1	obecně
nemá	mít	k5eNaImIp3nS	mít
řešení	řešení	k1gNnSc4	řešení
<g/>
,	,	kIx,	,
přibližná	přibližný	k2eAgNnPc1d1	přibližné
řešení	řešení	k1gNnPc1	řešení
byla	být	k5eAaImAgNnP	být
ovšem	ovšem	k9	ovšem
známa	znám	k2eAgNnPc1d1	známo
už	už	k6eAd1	už
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
Tarského	Tarského	k2eAgInSc4d1	Tarského
problém	problém	k1gInSc4	problém
kvadratury	kvadratura	k1gFnSc2	kvadratura
kruhu	kruh	k1gInSc2	kruh
je	být	k5eAaImIp3nS	být
úloha	úloha	k1gFnSc1	úloha
rozdělit	rozdělit	k5eAaPmF	rozdělit
daný	daný	k2eAgInSc4d1	daný
kruh	kruh	k1gInSc4	kruh
na	na	k7c4	na
konečně	konečně	k6eAd1	konečně
mnoho	mnoho	k4c4	mnoho
kousků	kousek	k1gInPc2	kousek
a	a	k8xC	a
složit	složit	k5eAaPmF	složit
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
kousků	kousek	k1gInPc2	kousek
čtverec	čtverec	k1gInSc4	čtverec
o	o	k7c6	o
stejném	stejný	k2eAgInSc6d1	stejný
obsahu	obsah	k1gInSc6	obsah
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
použitím	použití	k1gNnSc7	použití
axiomu	axiom	k1gInSc2	axiom
výběru	výběr	k1gInSc2	výběr
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
úloha	úloha	k1gFnSc1	úloha
řešitelná	řešitelný	k2eAgFnSc1d1	řešitelná
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
nikoliv	nikoliv	k9	nikoliv
prakticky	prakticky	k6eAd1	prakticky
<g/>
.	.	kIx.	.
</s>
<s>
Kousky	kousek	k1gInPc1	kousek
jsou	být	k5eAaImIp3nP	být
neměřitelné	měřitelný	k2eNgFnPc1d1	neměřitelná
množiny	množina	k1gFnPc1	množina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
nelze	lze	k6eNd1	lze
realizovat	realizovat	k5eAaBmF	realizovat
hmotou	hmota	k1gFnSc7	hmota
složenou	složený	k2eAgFnSc7d1	složená
z	z	k7c2	z
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
řešení	řešení	k1gNnSc1	řešení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nalezl	nalézt	k5eAaBmAgInS	nalézt
Laczkovich	Laczkovich	k1gInSc1	Laczkovich
<g/>
,	,	kIx,	,
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
10	[number]	k4	10
</s>
</p>
<p>
</p>
<p>
<s>
50	[number]	k4	50
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
10	[number]	k4	10
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
50	[number]	k4	50
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
kousků	kousek	k1gInPc2	kousek
<g/>
.	.	kIx.	.
<g/>
Třírozměrné	třírozměrný	k2eAgInPc4d1	třírozměrný
tvary	tvar	k1gInPc4	tvar
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
průsečíky	průsečík	k1gInPc4	průsečík
s	s	k7c7	s
některými	některý	k3yIgFnPc7	některý
rovinami	rovina	k1gFnPc7	rovina
dávají	dávat	k5eAaImIp3nP	dávat
kruhy	kruh	k1gInPc7	kruh
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
koule	koule	k1gFnPc4	koule
<g/>
,	,	kIx,	,
sféroidy	sféroid	k1gInPc4	sféroid
<g/>
,	,	kIx,	,
válce	válec	k1gInPc4	válec
a	a	k8xC	a
kužely	kužel	k1gInPc4	kužel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Kružnice	kružnice	k1gFnSc1	kružnice
</s>
</p>
<p>
<s>
Mezikruží	mezikruží	k1gNnSc1	mezikruží
</s>
</p>
<p>
<s>
Kvadratura	kvadratura	k1gFnSc1	kvadratura
kruhu	kruh	k1gInSc2	kruh
</s>
</p>
<p>
<s>
Malfattiho	Malfattize	k6eAd1	Malfattize
kruhy	kruh	k1gInPc1	kruh
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kruh	kruh	k1gInSc1	kruh
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgFnSc1d1	lokální
šablona	šablona	k1gFnSc1	šablona
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
kategorii	kategorie	k1gFnSc4	kategorie
Commons	Commonsa	k1gFnPc2	Commonsa
než	než	k8xS	než
přiřazená	přiřazený	k2eAgFnSc1d1	přiřazená
položka	položka	k1gFnSc1	položka
Wikidat	Wikidat	k1gFnSc2	Wikidat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Lokální	lokální	k2eAgInSc1d1	lokální
odkaz	odkaz	k1gInSc1	odkaz
<g/>
:	:	kIx,	:
Circle	Circle	k1gFnPc1	Circle
geometry	geometr	k1gInPc1	geometr
</s>
</p>
<p>
<s>
Wikidata	Wikidata	k1gFnSc1	Wikidata
<g/>
:	:	kIx,	:
Discs	Discs	k1gInSc1	Discs
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kruh	kruh	k1gInSc1	kruh
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Vzorce	vzorec	k1gInPc1	vzorec
pro	pro	k7c4	pro
kruh	kruh	k1gInSc4	kruh
a	a	k8xC	a
kružnici	kružnice	k1gFnSc4	kružnice
na	na	k7c4	na
Geometry	geometr	k1gMnPc4	geometr
Atlas	Atlas	k1gInSc4	Atlas
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Interaktivní	interaktivní	k2eAgInPc1d1	interaktivní
applety	applet	k1gInPc1	applet
Java	Jav	k1gInSc2	Jav
Vlastnosti	vlastnost	k1gFnSc2	vlastnost
a	a	k8xC	a
jednoduché	jednoduchý	k2eAgFnSc2d1	jednoduchá
konstrukce	konstrukce	k1gFnSc2	konstrukce
kruhu	kruh	k1gInSc2	kruh
a	a	k8xC	a
kružnice	kružnice	k1gFnSc2	kružnice
<g/>
.	.	kIx.	.
</s>
</p>
