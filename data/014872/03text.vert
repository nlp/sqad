<s>
Svátost	svátost	k1gFnSc1
</s>
<s>
Rogier	Rogier	k1gInSc1
van	vana	k1gFnPc2
der	drát	k5eAaImRp2nS
Weyden	Weydno	k1gNnPc2
<g/>
,	,	kIx,
Sedmero	Sedmero	k1gNnSc1
svátostí	svátost	k1gFnPc2
(	(	kIx(
<g/>
mezi	mezi	k7c7
1445	#num#	k4
a	a	k8xC
1450	#num#	k4
<g/>
,	,	kIx,
Koninklijk	Koninklijk	k1gInSc1
Museum	museum	k1gNnSc1
voor	voor	k1gInSc1
Schone	Schon	k1gInSc5
Kunsten	Kunsten	k2eAgMnSc1d1
Antwerpen	Antwerpen	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Svátost	svátost	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
lat.	lat.	k?
pojmu	pojmout	k5eAaPmIp1nS
sacramentum	sacramentum	k1gNnSc4
–	–	k?
„	„	k?
<g/>
posvátná	posvátný	k2eAgFnSc1d1
věc	věc	k1gFnSc1
<g/>
,	,	kIx,
posvátný	posvátný	k2eAgInSc1d1
úkon	úkon	k1gInSc1
<g/>
,	,	kIx,
přísaha	přísaha	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
je	být	k5eAaImIp3nS
ekvivalentem	ekvivalent	k1gInSc7
staršího	starší	k1gMnSc2
řec.	řec.	k?
μ	μ	k?
mystérion	mystérion	k1gInSc1
–	–	k?
„	„	k?
<g/>
tajemství	tajemství	k1gNnSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
nebo	nebo	k8xC
v	v	k7c6
rámci	rámec	k1gInSc6
pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
svatá	svatý	k2eAgFnSc1d1
Tajina	tajina	k1gFnSc1
je	být	k5eAaImIp3nS
důležitý	důležitý	k2eAgInSc4d1
křesťanský	křesťanský	k2eAgInSc4d1
obřad	obřad	k1gInSc4
<g/>
,	,	kIx,
často	často	k6eAd1
takový	takový	k3xDgInSc4
<g/>
,	,	kIx,
jejž	jenž	k3xRgInSc4
podle	podle	k7c2
názoru	názor	k1gInSc2
teologů	teolog	k1gMnPc2
ustanovil	ustanovit	k5eAaPmAgMnS
Ježíš	Ježíš	k1gMnSc1
Kristus	Kristus	k1gMnSc1
a	a	k8xC
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
prostřednictvím	prostřednictvím	k7c2
viditelného	viditelný	k2eAgInSc2d1
symbolu	symbol	k1gInSc2
zprostředkuje	zprostředkovat	k5eAaPmIp3nS
neviditelnou	viditelný	k2eNgFnSc4d1
Boží	boží	k2eAgFnSc4d1
milost	milost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Křesťanské	křesťanský	k2eAgFnSc2d1
církve	církev	k1gFnSc2
nejsou	být	k5eNaImIp3nP
ohledně	ohledně	k7c2
pohledu	pohled	k1gInSc2
na	na	k7c6
svátosti	svátost	k1gFnSc6
a	a	k8xC
jejich	jejich	k3xOp3gInSc1
počet	počet	k1gInSc1
jednotné	jednotný	k2eAgFnSc2d1
<g/>
;	;	kIx,
svátostí	svátost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
uznávají	uznávat	k5eAaImIp3nP
katolické	katolický	k2eAgFnSc2d1
církve	církev	k1gFnSc2
<g/>
,	,	kIx,
pravoslaví	pravoslaví	k1gNnSc1
a	a	k8xC
církev	církev	k1gFnSc1
československá	československý	k2eAgFnSc1d1
husitská	husitský	k2eAgFnSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
sedm	sedm	k4xCc1
<g/>
:	:	kIx,
</s>
<s>
Křest	křest	k1gInSc1
</s>
<s>
Biřmování	biřmování	k1gNnSc1
</s>
<s>
Eucharistie	eucharistie	k1gFnSc1
</s>
<s>
Svátost	svátost	k1gFnSc1
smíření	smíření	k1gNnSc2
</s>
<s>
Svátost	svátost	k1gFnSc1
nemocných	nemocný	k1gMnPc2
</s>
<s>
Manželství	manželství	k1gNnSc1
</s>
<s>
Kněžství	kněžství	k1gNnSc1
(	(	kIx(
<g/>
svěcení	svěcení	k1gNnSc1
kněžstva	kněžstvo	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
Katolické	katolický	k2eAgNnSc1d1
pojetí	pojetí	k1gNnSc1
</s>
<s>
Katolickou	katolický	k2eAgFnSc4d1
nauku	nauka	k1gFnSc4
o	o	k7c6
svátostech	svátost	k1gFnPc6
poprvé	poprvé	k6eAd1
definoval	definovat	k5eAaBmAgInS
Tridentský	tridentský	k2eAgInSc1d1
koncil	koncil	k1gInSc1
v	v	k7c6
průběhu	průběh	k1gInSc6
protestantské	protestantský	k2eAgFnSc2d1
reformace	reformace	k1gFnSc2
a	a	k8xC
většina	většina	k1gFnSc1
katolických	katolický	k2eAgNnPc2d1
stanovisek	stanovisko	k1gNnPc2
ohledně	ohledně	k7c2
svátostí	svátost	k1gFnPc2
vychází	vycházet	k5eAaImIp3nS
právě	právě	k9
z	z	k7c2
tohoto	tento	k3xDgInSc2
koncilu	koncil	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zjednodušeně	zjednodušeně	k6eAd1
by	by	kYmCp3nS
se	se	k3xPyFc4
dalo	dát	k5eAaPmAgNnS
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
svátosti	svátost	k1gFnPc1
jsou	být	k5eAaImIp3nP
viditelná	viditelný	k2eAgNnPc4d1
znamení	znamení	k1gNnPc4
neviditelné	viditelný	k2eNgFnSc2d1
Boží	boží	k2eAgFnSc2d1
blízkosti	blízkost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Katolické	katolický	k2eAgFnPc4d1
církve	církev	k1gFnPc4
učí	učit	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
svátosti	svátost	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
ustanovil	ustanovit	k5eAaPmAgMnS
Ježíš	Ježíš	k1gMnSc1
Kristus	Kristus	k1gMnSc1
a	a	k8xC
svěřil	svěřit	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
církvi	církev	k1gFnSc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
věřícím	věřící	k1gMnPc3
mohl	moct	k5eAaImAgInS
udělovat	udělovat	k5eAaImF
božský	božský	k2eAgInSc4d1
život	život	k1gInSc4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
účinným	účinný	k2eAgNnSc7d1
znamením	znamení	k1gNnSc7
milosti	milost	k1gFnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
nejen	nejen	k6eAd1
pouhými	pouhý	k2eAgInPc7d1
symboly	symbol	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svátosti	svátost	k1gFnPc1
jsou	být	k5eAaImIp3nP
podle	podle	k7c2
katolického	katolický	k2eAgNnSc2d1
pojetí	pojetí	k1gNnSc2
součástí	součást	k1gFnPc2
liturgie	liturgie	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
slaví	slavit	k5eAaImIp3nS
církev	církev	k1gFnSc1
jako	jako	k8xC,k8xS
kněžské	kněžský	k2eAgNnSc1d1
společenství	společenství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duch	duch	k1gMnSc1
Svatý	svatý	k1gMnSc1
skrze	skrze	k?
církev	církev	k1gFnSc1
připravuje	připravovat	k5eAaImIp3nS
člověka	člověk	k1gMnSc4
na	na	k7c4
přijetí	přijetí	k1gNnSc4
svátosti	svátost	k1gFnSc2
Božím	božit	k5eAaImIp1nS
slovem	slovo	k1gNnSc7
a	a	k8xC
vírou	víra	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
toto	tento	k3xDgNnSc4
slovo	slovo	k1gNnSc4
přijímá	přijímat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Ježíšova	Ježíšův	k2eAgNnPc4d1
slova	slovo	k1gNnPc4
a	a	k8xC
jeho	jeho	k3xOp3gInPc1
skutky	skutek	k1gInPc1
přinesly	přinést	k5eAaPmAgInP
spásu	spása	k1gFnSc4
všem	všecek	k3xTgMnPc3
lidem	člověk	k1gMnPc3
v	v	k7c6
moci	moc	k1gFnSc6
velikonočního	velikonoční	k2eAgNnSc2d1
tajemství	tajemství	k1gNnSc2
Ježíšovy	Ježíšův	k2eAgFnSc2d1
smrti	smrt	k1gFnSc2
<g/>
,	,	kIx,
vzkříšení	vzkříšení	k1gNnSc2
a	a	k8xC
nanebevstoupení	nanebevstoupení	k1gNnSc2
<g/>
;	;	kIx,
tento	tento	k3xDgInSc1
dar	dar	k1gInSc1
vlastního	vlastní	k2eAgInSc2d1
života	život	k1gInSc2
církvi	církev	k1gFnSc3
Ježíš	ježit	k5eAaImIp2nS
dále	daleko	k6eAd2
nabízí	nabízet	k5eAaImIp3nS
též	též	k9
prostřednictvím	prostřednictví	k1gNnSc7
svátostí	svátost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svátosti	svátost	k1gFnPc1
jsou	být	k5eAaImIp3nP
tedy	tedy	k8xC
„	„	k?
<g/>
pro	pro	k7c4
církev	církev	k1gFnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
protože	protože	k8xS
ji	on	k3xPp3gFnSc4
budují	budovat	k5eAaImIp3nP
<g/>
,	,	kIx,
a	a	k8xC
jsou	být	k5eAaImIp3nP
„	„	k?
<g/>
z	z	k7c2
církve	církev	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
ní	on	k3xPp3gFnSc6
působí	působit	k5eAaImIp3nS
díky	díky	k7c3
působení	působení	k1gNnSc3
Ducha	duch	k1gMnSc2
Svatého	svatý	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tři	tři	k4xCgMnPc1
ze	z	k7c2
svátostí	svátost	k1gFnPc2
–	–	k?
křest	křest	k1gInSc1
<g/>
,	,	kIx,
biřmování	biřmování	k1gNnSc1
<g/>
,	,	kIx,
kněžství	kněžství	k1gNnSc1
–	–	k?
pak	pak	k6eAd1
kromě	kromě	k7c2
milosti	milost	k1gFnSc2
do	do	k7c2
člověka	člověk	k1gMnSc2
vtiskují	vtiskovat	k5eAaImIp3nP
znamení	znamení	k1gNnSc4
<g/>
,	,	kIx,
skrze	skrze	k?
něž	jenž	k3xRgNnSc4
je	být	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
připodobněn	připodobněn	k2eAgMnSc1d1
Kristu	Krista	k1gFnSc4
a	a	k8xC
církvi	církev	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgNnPc1
znamení	znamení	k1gNnPc1
jsou	být	k5eAaImIp3nP
nezrušitelná	zrušitelný	k2eNgNnPc1d1
(	(	kIx(
<g/>
srov.	srov.	kA
KKC	KKC	kA
1121	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
člověk	člověk	k1gMnSc1
je	on	k3xPp3gNnPc4
může	moct	k5eAaImIp3nS
přijmout	přijmout	k5eAaPmF
pouze	pouze	k6eAd1
jednou	jeden	k4xCgFnSc7
<g/>
.	.	kIx.
</s>
<s>
Svátosti	svátost	k1gFnPc1
podle	podle	k7c2
katolíků	katolík	k1gMnPc2
udělují	udělovat	k5eAaImIp3nP
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
<g/>
-li	-li	k?
důstojně	důstojně	k6eAd1
slaveny	slavit	k5eAaImNgFnP
ve	v	k7c6
víře	víra	k1gFnSc6
<g/>
,	,	kIx,
milost	milost	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
znamením	znamení	k1gNnSc7
(	(	kIx(
<g/>
srov.	srov.	kA
Tridentský	tridentský	k2eAgInSc1d1
koncil	koncil	k1gInSc1
<g/>
,	,	kIx,
DS	DS	kA
1605	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
účinek	účinek	k1gInSc4
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
nich	on	k3xPp3gFnPc6
působí	působit	k5eAaImIp3nS
sám	sám	k3xTgMnSc1
Kristus	Kristus	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
protože	protože	k8xS
Bůh	bůh	k1gMnSc1
Otec	otec	k1gMnSc1
vždy	vždy	k6eAd1
vyslyší	vyslyšet	k5eAaPmIp3nS
modlitbu	modlitba	k1gFnSc4
svého	svůj	k3xOyFgMnSc2
Syna	syn	k1gMnSc2
<g/>
,	,	kIx,
svátosti	svátost	k1gFnSc2
působí	působit	k5eAaImIp3nS
prostým	prostý	k2eAgNnSc7d1
udělením	udělení	k1gNnSc7
<g/>
,	,	kIx,
tzv.	tzv.	kA
ex	ex	k6eAd1
opere	oprat	k5eAaPmIp3nS
operato	operat	k2eAgNnSc1d1
<g/>
,	,	kIx,
tedy	tedy	k8xC
z	z	k7c2
moci	moc	k1gFnSc2
Kristova	Kristův	k2eAgNnSc2d1
spásného	spásný	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
vykonaného	vykonaný	k2eAgNnSc2d1
jednou	jednou	k6eAd1
provždy	provždy	k6eAd1
(	(	kIx(
<g/>
KKC	KKC	kA
1128	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svátost	svátost	k1gFnSc1
se	se	k3xPyFc4
tudíž	tudíž	k8xC
podle	podle	k7c2
katolického	katolický	k2eAgInSc2d1
pohledu	pohled	k1gInSc2
neuskutečňuje	uskutečňovat	k5eNaImIp3nS
spravedlností	spravedlnost	k1gFnSc7
člověka	člověk	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
Boží	boží	k2eAgFnSc7d1
mocí	moc	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plody	plod	k1gInPc4
svátosti	svátost	k1gFnSc2
však	však	k9
závisejí	záviset	k5eAaImIp3nP
též	též	k9
na	na	k7c6
víře	víra	k1gFnSc6
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
je	on	k3xPp3gMnPc4
přijímá	přijímat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
říká	říkat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
svátosti	svátost	k1gFnPc1
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
věřící	věřící	k1gFnPc4
nutné	nutný	k2eAgFnPc1d1
ke	k	k7c3
spáse	spása	k1gFnSc3
(	(	kIx(
<g/>
srov.	srov.	kA
Tridentský	tridentský	k2eAgInSc1d1
koncil	koncil	k1gInSc1
<g/>
,	,	kIx,
DS	DS	kA
1604	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
svátostmi	svátost	k1gFnPc7
se	se	k3xPyFc4
člověku	člověk	k1gMnSc6
takto	takto	k6eAd1
dostává	dostávat	k5eAaImIp3nS
milosti	milost	k1gFnPc4
Ducha	duch	k1gMnSc2
Svatého	svatý	k1gMnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
člověku	člověk	k1gMnSc3
daruje	darovat	k5eAaPmIp3nS
Ježíš	Ježíš	k1gMnSc1
Kristus	Kristus	k1gMnSc1
<g/>
,	,	kIx,
člověk	člověk	k1gMnSc1
je	být	k5eAaImIp3nS
prostřednictvím	prostřednictvím	k7c2
svátostí	svátost	k1gFnPc2
též	též	k9
připodobněn	připodobněn	k2eAgInSc4d1
Božímu	boží	k2eAgMnSc3d1
Synu	syn	k1gMnSc3
<g/>
,	,	kIx,
Ježíši	Ježíš	k1gMnSc3
Kristu	Krista	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klasickou	klasický	k2eAgFnSc4d1
definici	definice	k1gFnSc4
svátosti	svátost	k1gFnSc2
nabízí	nabízet	k5eAaImIp3nS
sv.	sv.	kA
Tomáš	Tomáš	k1gMnSc1
Akvinský	Akvinský	k2eAgMnSc1d1
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
Teologické	teologický	k2eAgFnSc6d1
sumě	suma	k1gFnSc6
(	(	kIx(
<g/>
III	III	kA
<g/>
,60	,60	k4
<g/>
,3	,3	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
„	„	k?
<g/>
Svátost	svátost	k1gFnSc1
je	on	k3xPp3gNnSc4
znamení	znamení	k1gNnSc4
připomínající	připomínající	k2eAgFnSc4d1
minulost	minulost	k1gFnSc4
<g/>
,	,	kIx,
totiž	totiž	k9
utrpení	utrpení	k1gNnSc4
Páně	páně	k2eAgFnSc2d1
<g/>
;	;	kIx,
je	být	k5eAaImIp3nS
znamením	znamení	k1gNnSc7
poukazujícím	poukazující	k2eAgNnSc7d1
na	na	k7c4
plody	plod	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
v	v	k7c4
nás	my	k3xPp1nPc4
působí	působit	k5eAaImIp3nS
jeho	jeho	k3xOp3gNnSc1
utrpení	utrpení	k1gNnSc1
<g/>
,	,	kIx,
totiž	totiž	k9
milost	milost	k1gFnSc1
<g/>
;	;	kIx,
je	být	k5eAaImIp3nS
prorockým	prorocký	k2eAgNnSc7d1
znamením	znamení	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
ohlašuje	ohlašovat	k5eAaImIp3nS
předem	předem	k6eAd1
budoucí	budoucí	k2eAgFnSc4d1
slávu	sláva	k1gFnSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
</s>
<s>
Katolíci	katolík	k1gMnPc1
tím	ten	k3xDgNnSc7
nezpochybňují	zpochybňovat	k5eNaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
existují	existovat	k5eAaImIp3nP
ještě	ještě	k6eAd1
další	další	k2eAgInPc1d1
prostředky	prostředek	k1gInPc1
Boží	boží	k2eAgFnSc2d1
milosti	milost	k1gFnSc2
<g/>
;	;	kIx,
římskokatolická	římskokatolický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
má	mít	k5eAaImIp3nS
kromě	kromě	k7c2
svátostí	svátost	k1gFnPc2
ještě	ještě	k9
tzv.	tzv.	kA
svátostiny	svátostina	k1gFnSc2
–	–	k?
bohoslužebné	bohoslužebný	k2eAgInPc4d1
úkony	úkon	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
nemají	mít	k5eNaImIp3nP
svůj	svůj	k3xOyFgInSc4
původ	původ	k1gInSc4
v	v	k7c6
Novém	nový	k2eAgInSc6d1
zákoně	zákon	k1gInSc6
a	a	k8xC
v	v	k7c6
ustanovení	ustanovení	k1gNnSc6
Kristem	Kristus	k1gMnSc7
<g/>
,	,	kIx,
avšak	avšak	k8xC
též	též	k9
podle	podle	k7c2
ní	on	k3xPp3gFnSc2
působí	působit	k5eAaImIp3nS
milost	milost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c7
ně	on	k3xPp3gNnSc4
například	například	k6eAd1
žehnání	žehnání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Církev	církev	k1gFnSc1
jako	jako	k8xC,k8xS
svátost	svátost	k1gFnSc1
</s>
<s>
Protože	protože	k8xS
slovo	slovo	k1gNnSc4
svátost	svátost	k1gFnSc1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
viditelné	viditelný	k2eAgNnSc4d1
znamení	znamení	k1gNnSc4
skryté	skrytý	k2eAgFnSc2d1
skutečnosti	skutečnost	k1gFnSc2
spásy	spása	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
sám	sám	k3xTgMnSc1
Ježíš	Ježíš	k1gMnSc1
Kristus	Kristus	k1gMnSc1
v	v	k7c6
katolickém	katolický	k2eAgInSc6d1
pohledu	pohled	k1gInSc6
sám	sám	k3xTgMnSc1
coby	coby	k?
tajemství	tajemství	k1gNnSc1
spásy	spása	k1gFnSc2
svátostí	svátost	k1gFnSc7
(	(	kIx(
<g/>
teologové	teolog	k1gMnPc1
jej	on	k3xPp3gMnSc4
označují	označovat	k5eAaImIp3nP
jako	jako	k8xS,k8xC
prasvátost	prasvátost	k1gFnSc1
<g/>
,	,	kIx,
něm.	něm.	k?
Ursakrament	Ursakrament	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sedmero	Sedmero	k1gNnSc1
svátostí	svátost	k1gFnPc2
je	být	k5eAaImIp3nS
pak	pak	k9
nástrojem	nástroj	k1gInSc7
této	tento	k3xDgFnSc2
spásy	spása	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
šíří	šířit	k5eAaImIp3nS
Duch	duch	k1gMnSc1
Svatý	svatý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kristus	Kristus	k1gMnSc1
sám	sám	k3xTgMnSc1
je	být	k5eAaImIp3nS
hlavou	hlava	k1gFnSc7
svého	svůj	k3xOyFgNnSc2
těla	tělo	k1gNnSc2
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
jest	být	k5eAaImIp3nS
církve	církev	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
tak	tak	k6eAd1
sděluje	sdělovat	k5eAaImIp3nS
neviditelnou	viditelný	k2eNgFnSc4d1
milost	milost	k1gFnSc4
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
je	být	k5eAaImIp3nS
znamením	znamení	k1gNnSc7
(	(	kIx(
<g/>
tj.	tj.	kA
Krista	Kristus	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
smyslu	smysl	k1gInSc6
je	být	k5eAaImIp3nS
analogicky	analogicky	k6eAd1
svátostí	svátost	k1gFnSc7
též	též	k9
církev	církev	k1gFnSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
odborně	odborně	k6eAd1
se	se	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
jako	jako	k9
základní	základní	k2eAgFnSc1d1
svátost	svátost	k1gFnSc1
<g/>
,	,	kIx,
něm.	něm.	k?
Grundsakrament	Grundsakrament	k1gMnSc1
(	(	kIx(
<g/>
srov.	srov.	kA
Druhý	druhý	k4xOgInSc4
vatikánský	vatikánský	k2eAgInSc4d1
koncil	koncil	k1gInSc4
<g/>
,	,	kIx,
konstituce	konstituce	k1gFnPc4
Lumen	lumen	k1gInSc4
gentium	gentium	k1gNnSc1
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
je	být	k5eAaImIp3nS
církev	církev	k1gFnSc1
prostředkem	prostředek	k1gInSc7
<g/>
,	,	kIx,
jímž	jenž	k3xRgInSc7
Kristus	Kristus	k1gMnSc1
zjevuje	zjevovat	k5eAaImIp3nS
a	a	k8xC
uskutečňuje	uskutečňovat	k5eAaImIp3nS
tajemství	tajemství	k1gNnSc1
Boží	boží	k2eAgFnSc2d1
lásky	láska	k1gFnSc2
vůči	vůči	k7c3
lidem	člověk	k1gMnPc3
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
církev	církev	k1gFnSc4
podle	podle	k7c2
Druhého	druhý	k4xOgInSc2
vatikánského	vatikánský	k2eAgInSc2d1
koncilu	koncil	k1gInSc2
„	„	k?
<g/>
všeobecnou	všeobecný	k2eAgFnSc7d1
svátostí	svátost	k1gFnSc7
spásy	spása	k1gFnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
srov.	srov.	kA
Lumen	lumen	k1gInSc1
gentium	gentium	k1gNnSc1
48	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Protestantské	protestantský	k2eAgNnSc1d1
a	a	k8xC
husitské	husitský	k2eAgNnSc1d1
pojetí	pojetí	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Marburské	Marburský	k2eAgInPc4d1
náboženské	náboženský	k2eAgInPc4d1
rozhovory	rozhovor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Většina	většina	k1gFnSc1
protestantů	protestant	k1gMnPc2
věří	věřit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
svátosti	svátost	k1gFnPc1
jsou	být	k5eAaImIp3nP
„	„	k?
<g/>
vnějším	vnější	k2eAgNnSc7d1
znamením	znamení	k1gNnSc7
vnitřní	vnitřní	k2eAgFnSc2d1
milosti	milost	k1gFnSc2
<g/>
“	“	k?
anebo	anebo	k8xC
symbolem	symbol	k1gInSc7
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
událo	udát	k5eAaPmAgNnS
neviditelně	viditelně	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
svátost	svátost	k1gFnSc4
však	však	k9
považují	považovat	k5eAaImIp3nP
pouze	pouze	k6eAd1
křest	křest	k1gInSc4
a	a	k8xC
Večeři	večeře	k1gFnSc4
Páně	páně	k2eAgFnSc4d1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
odpovídá	odpovídat	k5eAaImIp3nS
pojmu	pojem	k1gInSc3
eucharistie	eucharistie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbylých	zbylý	k2eAgFnPc2d1
pět	pět	k4xCc1
svátostí	svátost	k1gFnPc2
nepovažují	považovat	k5eNaImIp3nP
za	za	k7c4
ustanovené	ustanovený	k2eAgInPc4d1
v	v	k7c6
Novém	nový	k2eAgInSc6d1
zákoně	zákon	k1gInSc6
<g/>
,	,	kIx,
avšak	avšak	k8xC
mnoho	mnoho	k4c1
protestantských	protestantský	k2eAgFnPc2d1
církví	církev	k1gFnPc2
má	mít	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
svatební	svatební	k2eAgInSc4d1
obřad	obřad	k1gInSc4
a	a	k8xC
ordinované	ordinovaný	k2eAgMnPc4d1
služebníky	služebník	k1gMnPc4
a	a	k8xC
některé	některý	k3yIgFnPc4
církve	církev	k1gFnPc4
anglikánského	anglikánský	k2eAgNnSc2d1
společenství	společenství	k1gNnSc2
–	–	k?
anglokatolíci	anglokatolík	k1gMnPc1
–	–	k?
přijímají	přijímat	k5eAaImIp3nP
všech	všecek	k3xTgFnPc2
sedm	sedm	k4xCc1
svátostí	svátost	k1gFnPc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
je	on	k3xPp3gInPc4
chápou	chápat	k5eAaImIp3nP
katolíci	katolík	k1gMnPc1
<g/>
,	,	kIx,
ačkoli	ačkoli	k8xS
se	se	k3xPyFc4
sami	sám	k3xTgMnPc1
považují	považovat	k5eAaImIp3nP
za	za	k7c7
protestanty	protestant	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
českém	český	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
si	se	k3xPyFc3
též	též	k9
Církev	církev	k1gFnSc1
československá	československý	k2eAgFnSc1d1
husitská	husitský	k2eAgFnSc1d1
ponechala	ponechat	k5eAaPmAgFnS
systém	systém	k1gInSc1
sedmi	sedm	k4xCc2
svátostí	svátost	k1gFnPc2
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
reformační	reformační	k2eAgFnSc4d1
církev	církev	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svátost	svátost	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
této	tento	k3xDgFnSc6
církvi	církev	k1gFnSc6
definována	definován	k2eAgFnSc1d1
jako	jako	k8xC,k8xS
„	„	k?
<g/>
jednání	jednání	k1gNnPc2
<g/>
,	,	kIx,
jímž	jenž	k3xRgInSc7
se	se	k3xPyFc4
obecenství	obecenství	k1gNnSc1
věřících	věřící	k1gMnPc2
v	v	k7c6
Duchu	duch	k1gMnSc6
svatém	svatý	k1gMnSc6
činně	činně	k6eAd1
podílí	podílet	k5eAaImIp3nS
na	na	k7c6
milosti	milost	k1gFnSc6
Božího	boží	k2eAgNnSc2d1
Slova	slovo	k1gNnSc2
přítomného	přítomný	k2eAgInSc2d1
skrze	skrze	k?
slyšené	slyšený	k2eAgNnSc4d1
svědectví	svědectví	k1gNnSc4
Písma	písmo	k1gNnSc2
svatého	svatý	k1gMnSc2
a	a	k8xC
svátostné	svátostný	k2eAgInPc4d1
úkony	úkon	k1gInPc4
<g/>
“	“	k?
(	(	kIx(
<g/>
srov.	srov.	kA
Základy	základ	k1gInPc1
víry	víra	k1gFnSc2
Církve	církev	k1gFnSc2
československé	československý	k2eAgFnSc2d1
husitské	husitský	k2eAgFnSc2d1
<g/>
,	,	kIx,
čl	čl	kA
<g/>
.	.	kIx.
307	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Pravoslavné	pravoslavný	k2eAgNnSc1d1
pojetí	pojetí	k1gNnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Pravoslaví	pravoslaví	k1gNnSc2
<g/>
#	#	kIx~
<g/>
Svaté	svatý	k2eAgFnSc2d1
tajiny	tajina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pravoslavní	pravoslavný	k2eAgMnPc1d1
sice	sice	k8xC
uznávají	uznávat	k5eAaImIp3nP
symbolický	symbolický	k2eAgInSc4d1
počet	počet	k1gInSc4
sedmi	sedm	k4xCc2
svatých	svatý	k2eAgFnPc2d1
Tajin	tajina	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
označují	označovat	k5eAaImIp3nP
tento	tento	k3xDgInSc4
soubor	soubor	k1gInSc4
za	za	k7c2
„	„	k?
<g/>
hlavní	hlavní	k2eAgFnSc2d1
svaté	svatý	k2eAgFnSc2d1
Tajiny	tajina	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
protože	protože	k8xS
počítají	počítat	k5eAaImIp3nP
svatých	svatý	k2eAgFnPc2d1
Tajin	tajina	k1gFnPc2
více	hodně	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
svatou	svatý	k2eAgFnSc4d1
Tajinu	tajina	k1gFnSc4
je	být	k5eAaImIp3nS
považováno	považován	k2eAgNnSc1d1
například	například	k6eAd1
velké	velký	k2eAgNnSc1d1
svěcení	svěcení	k1gNnSc1
vody	voda	k1gFnSc2
na	na	k7c4
svátek	svátek	k1gInSc4
Zjevení	zjevení	k1gNnSc1
Páně	páně	k2eAgFnPc1d1
(	(	kIx(
<g/>
Theofanie	Theofanie	k1gFnPc1
<g/>
,	,	kIx,
Bogojavlenije	Bogojavlenije	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
stejně	stejně	k6eAd1
tak	tak	k9
i	i	k9
mnišské	mnišský	k2eAgNnSc4d1
postřižení	postřižení	k1gNnSc4
–	–	k?
velká	velká	k1gFnSc1
schima	schima	k1gFnSc1
–	–	k?
<g/>
,	,	kIx,
při	při	k7c6
kterém	který	k3yIgInSc6,k3yQgInSc6,k3yRgInSc6
jsou	být	k5eAaImIp3nP
odpouštěny	odpouštět	k5eAaImNgInP
všechny	všechen	k3xTgInPc1
dosavadní	dosavadní	k2eAgInPc1d1
hříchy	hřích	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Obřad	obřad	k1gInSc1
svaté	svatý	k2eAgFnSc2d1
Tajiny	tajina	k1gFnSc2
vykonává	vykonávat	k5eAaImIp3nS
kněz	kněz	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
duchovně	duchovně	k6eAd1
je	být	k5eAaImIp3nS
dílem	díl	k1gInSc7
Svatého	svatý	k2eAgMnSc2d1
Ducha	duch	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
požehnání	požehnání	k1gNnSc2
a	a	k8xC
posvěcení	posvěcení	k1gNnSc1
nějaké	nějaký	k3yIgFnSc2
věci	věc	k1gFnSc2
se	se	k3xPyFc4
svatá	svatý	k2eAgFnSc1d1
Tajina	tajina	k1gFnSc1
odlišuje	odlišovat	k5eAaImIp3nS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
jakýmsi	jakýsi	k3yIgNnSc7
hlubším	hluboký	k2eAgNnSc7d2
působením	působení	k1gNnSc7
Svatého	svatý	k2eAgMnSc2d1
Ducha	duch	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
ve	v	k7c6
svatých	svatý	k2eAgFnPc6d1
Tajinách	tajina	k1gFnPc6
sestupuje	sestupovat	k5eAaImIp3nS
a	a	k8xC
očišťuje	očišťovat	k5eAaImIp3nS
lidské	lidský	k2eAgNnSc4d1
nitro	nitro	k1gNnSc4
<g/>
,	,	kIx,
obnovuje	obnovovat	k5eAaImIp3nS
člověka	člověk	k1gMnSc4
<g/>
,	,	kIx,
proměňuje	proměňovat	k5eAaImIp3nS
svaté	svatý	k2eAgInPc4d1
Dary	dar	k1gInPc4
<g/>
,	,	kIx,
ustanovuje	ustanovovat	k5eAaImIp3nS
a	a	k8xC
uschopňuje	uschopňovat	k5eAaImIp3nS
člověka	člověk	k1gMnSc2
k	k	k7c3
nějaké	nějaký	k3yIgFnSc3
církevní	církevní	k2eAgFnSc3d1
službě	služba	k1gFnSc3
apod.	apod.	kA
Svatými	svatý	k2eAgFnPc7d1
Tajinami	tajina	k1gFnPc7
je	být	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
sjednocován	sjednocován	k2eAgMnSc1d1
s	s	k7c7
Bohem	bůh	k1gMnSc7
a	a	k8xC
nemůže	moct	k5eNaImIp3nS
být	být	k5eAaImF
bez	bez	k7c2
nich	on	k3xPp3gInPc2
spasen	spasen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Pro	pro	k7c4
pravoslavné	pravoslavný	k2eAgMnPc4d1
křesťany	křesťan	k1gMnPc4
je	být	k5eAaImIp3nS
„	„	k?
<g/>
svátost	svátost	k1gFnSc1
<g/>
“	“	k?
západním	západní	k2eAgInSc7d1
termínem	termín	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
definovat	definovat	k5eAaBmF
cosi	cosi	k3yInSc4
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
definovat	definovat	k5eAaBmF
nelze	lze	k6eNd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Namísto	namísto	k7c2
něj	on	k3xPp3gNnSc2
se	se	k3xPyFc4
tedy	tedy	k9
používá	používat	k5eAaImIp3nS
pojmu	pojem	k1gInSc3
„	„	k?
<g/>
tajemství	tajemství	k1gNnSc2
<g/>
“	“	k?
(	(	kIx(
<g/>
svatá	svatý	k2eAgFnSc1d1
Tajina	tajina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jímž	jenž	k3xRgInSc7
se	se	k3xPyFc4
vyjadřuje	vyjadřovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
svátost	svátost	k1gFnSc1
<g/>
“	“	k?
je	být	k5eAaImIp3nS
cosi	cosi	k3yInSc1
<g/>
,	,	kIx,
čemu	co	k3yRnSc3,k3yInSc3,k3yQnSc3
nelze	lze	k6eNd1
porozumět	porozumět	k5eAaPmF
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
přesahuje	přesahovat	k5eAaImIp3nS
lidské	lidský	k2eAgNnSc4d1
chápání	chápání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bůh	bůh	k1gMnSc1
se	se	k3xPyFc4
nás	my	k3xPp1nPc2
dotýká	dotýkat	k5eAaImIp3nS
skrze	skrze	k?
hmotné	hmotný	k2eAgFnPc4d1
věci	věc	k1gFnPc4
<g/>
,	,	kIx,
např.	např.	kA
vodu	voda	k1gFnSc4
<g/>
,	,	kIx,
víno	víno	k1gNnSc4
<g/>
,	,	kIx,
chléb	chléb	k1gInSc4
<g/>
,	,	kIx,
olej	olej	k1gInSc4
<g/>
,	,	kIx,
kadidlo	kadidlo	k1gNnSc4
<g/>
,	,	kIx,
svíce	svíce	k1gFnPc4
<g/>
,	,	kIx,
oltář	oltář	k1gInSc4
či	či	k8xC
ikony	ikona	k1gFnPc4
<g/>
,	,	kIx,
a	a	k8xC
způsob	způsob	k1gInSc1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
toho	ten	k3xDgMnSc4
dosahuje	dosahovat	k5eAaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
tajemstvím	tajemství	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
nejhlubší	hluboký	k2eAgNnSc4d3
tajemství	tajemství	k1gNnSc4
považují	považovat	k5eAaImIp3nP
pravoslavní	pravoslavný	k2eAgMnPc1d1
eucharistii	eucharistie	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Bůh	bůh	k1gMnSc1
sestupuje	sestupovat	k5eAaImIp3nS
na	na	k7c4
předložené	předložený	k2eAgInPc4d1
dary	dar	k1gInPc4
<g/>
,	,	kIx,
tajemně	tajemně	k6eAd1
proměňuje	proměňovat	k5eAaImIp3nS
materii	materie	k1gFnSc4
chleba	chléb	k1gInSc2
a	a	k8xC
vína	víno	k1gNnSc2
a	a	k8xC
při	při	k7c6
svatém	svatý	k2eAgNnSc6d1
přijímání	přijímání	k1gNnSc6
pak	pak	k6eAd1
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
bezprostřednímu	bezprostřední	k2eAgNnSc3d1
společenství	společenství	k1gNnSc3
s	s	k7c7
Bohem	bůh	k1gMnSc7
<g/>
,	,	kIx,
takže	takže	k8xS
se	se	k3xPyFc4
krev	krev	k1gFnSc1
člověka	člověk	k1gMnSc2
spojuje	spojovat	k5eAaImIp3nS
s	s	k7c7
krví	krev	k1gFnSc7
Kristovou	Kristův	k2eAgFnSc7d1
a	a	k8xC
tělo	tělo	k1gNnSc4
lidské	lidský	k2eAgNnSc4d1
s	s	k7c7
tělem	tělo	k1gNnSc7
Krista	Kristus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1
rozdíly	rozdíl	k1gInPc1
v	v	k7c6
římskokatolickém	římskokatolický	k2eAgNnSc6d1
a	a	k8xC
pravoslavném	pravoslavný	k2eAgNnSc6d1
pojetí	pojetí	k1gNnSc6
</s>
<s>
Hlavní	hlavní	k2eAgFnPc1d1
odlišnosti	odlišnost	k1gFnPc1
římskokatolického	římskokatolický	k2eAgNnSc2d1
učení	učení	k1gNnSc2
o	o	k7c6
svátostech	svátost	k1gFnPc6
a	a	k8xC
učením	učení	k1gNnSc7
pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
jsou	být	k5eAaImIp3nP
následující	následující	k2eAgFnPc1d1
<g/>
:	:	kIx,
</s>
<s>
rozdílné	rozdílný	k2eAgNnSc1d1
pojímání	pojímání	k1gNnSc1
úlohy	úloha	k1gFnSc2
kněze	kněz	k1gMnSc2
a	a	k8xC
působení	působení	k1gNnSc1
Ducha	duch	k1gMnSc2
Svatého	svatý	k2eAgMnSc2d1
v	v	k7c6
rámci	rámec	k1gInSc6
konání	konání	k1gNnSc2
svátostí	svátost	k1gFnPc2
(	(	kIx(
<g/>
Tajin	tajina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
je	být	k5eAaImIp3nS
křest	křest	k1gInSc1
nebo	nebo	k8xC
modlitba	modlitba	k1gFnSc1
rozhřešení	rozhřešení	k1gNnSc2
při	při	k7c6
zpovědi	zpověď	k1gFnSc6
<g/>
:	:	kIx,
zatímco	zatímco	k8xS
v	v	k7c6
římskokatolické	římskokatolický	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
je	být	k5eAaImIp3nS
stavěn	stavit	k5eAaImNgInS
do	do	k7c2
popředí	popředí	k1gNnSc2
kněz	kněz	k1gMnSc1
–	–	k?
viz	vidět	k5eAaImRp2nS
např.	např.	kA
křestní	křestní	k2eAgFnSc4d1
formuli	formule	k1gFnSc4
svátosti	svátost	k1gFnSc2
křtu	křest	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
římský	římský	k2eAgMnSc1d1
kněz	kněz	k1gMnSc1
říká	říkat	k5eAaImIp3nS
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
„	„	k?
<g/>
Já	já	k3xPp1nSc1
tě	ty	k3xPp2nSc4
křtím	křtít	k5eAaImIp1nS
ve	v	k7c6
jménu	jméno	k1gNnSc6
Otce	otec	k1gMnSc2
i	i	k8xC
Syna	syn	k1gMnSc2
i	i	k8xC
svatého	svatý	k2eAgMnSc2d1
Ducha	duch	k1gMnSc2
<g/>
…	…	k?
<g/>
“	“	k?
–	–	k?
<g/>
,	,	kIx,
v	v	k7c6
pravoslaví	pravoslaví	k1gNnSc6
je	být	k5eAaImIp3nS
brán	brán	k2eAgInSc1d1
zřetel	zřetel	k1gInSc1
především	především	k9
na	na	k7c4
působení	působení	k1gNnSc4
Ducha	duch	k1gMnSc2
Svatého	svatý	k1gMnSc2
a	a	k8xC
kněz	kněz	k1gMnSc1
je	být	k5eAaImIp3nS
vnímán	vnímat	k5eAaImNgInS
pouze	pouze	k6eAd1
jako	jako	k8xC,k8xS
služebník	služebník	k1gMnSc1
tohoto	tento	k3xDgNnSc2
duchovního	duchovní	k2eAgNnSc2d1
díla	dílo	k1gNnSc2
–	–	k?
při	při	k7c6
pravoslavném	pravoslavný	k2eAgInSc6d1
křtu	křest	k1gInSc6
kněz	kněz	k1gMnSc1
říká	říkat	k5eAaImIp3nS
<g/>
:	:	kIx,
„	„	k?
<g/>
Křtí	křtít	k5eAaImIp3nS
se	se	k3xPyFc4
služebník	služebník	k1gMnSc1
Boží	božit	k5eAaImIp3nS
ve	v	k7c6
jménu	jméno	k1gNnSc6
otce	otec	k1gMnSc2
i	i	k8xC
Syna	syn	k1gMnSc2
i	i	k8xC
Svatého	svatý	k2eAgMnSc2d1
Ducha	duch	k1gMnSc2
<g/>
…	…	k?
<g/>
“	“	k?
</s>
<s>
odlišnosti	odlišnost	k1gFnPc1
v	v	k7c6
učení	učení	k1gNnSc6
o	o	k7c6
posvěcení	posvěcení	k1gNnSc6
svátostí	svátost	k1gFnPc2
(	(	kIx(
<g/>
Tajin	tajina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
pravoslavní	pravoslavný	k2eAgMnPc1d1
vyznávají	vyznávat	k5eAaImIp3nP
učení	učený	k2eAgMnPc1d1
o	o	k7c6
„	„	k?
<g/>
posvěcení	posvěcení	k1gNnSc6
nestvořenými	stvořený	k2eNgFnPc7d1
energiemi	energie	k1gFnPc7
Ducha	duch	k1gMnSc2
Svatého	svatý	k1gMnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
římská	římský	k2eAgFnSc1d1
církev	církev	k1gFnSc1
odmítá	odmítat	k5eAaImIp3nS
</s>
<s>
odlišnosti	odlišnost	k1gFnPc1
týkající	týkající	k2eAgFnPc1d1
se	se	k3xPyFc4
osob	osoba	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
mohou	moct	k5eAaImIp3nP
vykonávat	vykonávat	k5eAaImF
svátosti	svátost	k1gFnPc4
(	(	kIx(
<g/>
Tajiny	tajina	k1gFnPc4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
křest	křest	k1gInSc1
<g/>
:	:	kIx,
dle	dle	k7c2
římskokatolického	římskokatolický	k2eAgInSc2d1
katechismu	katechismus	k1gInSc2
vás	vy	k3xPp2nPc4
může	moct	k5eAaImIp3nS
v	v	k7c6
nouzi	nouze	k1gFnSc6
pokřtít	pokřtít	k5eAaPmF
každý	každý	k3xTgMnSc1
člověk	člověk	k1gMnSc1
<g/>
,	,	kIx,
i	i	k8xC
příslušník	příslušník	k1gMnSc1
jiného	jiný	k2eAgNnSc2d1
náboženství	náboženství	k1gNnSc2
nebo	nebo	k8xC
nevěřící	nevěřící	k1gFnSc2
<g/>
;	;	kIx,
v	v	k7c6
pravoslaví	pravoslaví	k1gNnSc6
je	být	k5eAaImIp3nS
něco	něco	k3yInSc1
takového	takový	k3xDgNnSc2
vyloučeno	vyloučen	k2eAgNnSc1d1
<g/>
,	,	kIx,
pokřtít	pokřtít	k5eAaPmF
může	moct	k5eAaImIp3nS
i	i	k9
laik	laik	k1gMnSc1
(	(	kIx(
<g/>
nekněz	nekněz	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
vždy	vždy	k6eAd1
to	ten	k3xDgNnSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
zbožný	zbožný	k2eAgMnSc1d1
pravoslavný	pravoslavný	k2eAgMnSc1d1
křesťan	křesťan	k1gMnSc1
</s>
<s>
biřmování	biřmování	k1gNnSc1
<g/>
/	/	kIx~
<g/>
myropomazání	myropomazání	k1gNnSc1
<g/>
:	:	kIx,
v	v	k7c6
římské	římský	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
jej	on	k3xPp3gMnSc4
může	moct	k5eAaImIp3nS
konat	konat	k5eAaImF
pouze	pouze	k6eAd1
biskup	biskup	k1gMnSc1
<g/>
;	;	kIx,
v	v	k7c6
pravoslavné	pravoslavný	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
jej	on	k3xPp3gNnSc4
vykonává	vykonávat	k5eAaImIp3nS
každý	každý	k3xTgMnSc1
kněz	kněz	k1gMnSc1
ihned	ihned	k6eAd1
po	po	k7c6
křtu	křest	k1gInSc6
<g/>
,	,	kIx,
používá	používat	k5eAaImIp3nS
ovšem	ovšem	k9
svaté	svatý	k2eAgNnSc1d1
myro	myro	k1gNnSc1
(	(	kIx(
<g/>
křižmo	křižmo	k1gNnSc1
<g/>
)	)	kIx)
posvěcené	posvěcený	k2eAgInPc1d1
biskupem	biskup	k1gMnSc7
</s>
<s>
rozdílnosti	rozdílnost	k1gFnPc1
ve	v	k7c6
vysluhování	vysluhování	k1gNnSc6
eucharistie	eucharistie	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
v	v	k7c6
pravoslavné	pravoslavný	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
se	se	k3xPyFc4
podává	podávat	k5eAaImIp3nS
zásadně	zásadně	k6eAd1
pod	pod	k7c7
obojí	oboj	k1gFnSc7
způsobou	způsoba	k1gFnSc7
<g/>
,	,	kIx,
nikdy	nikdy	k6eAd1
se	se	k3xPyFc4
nepodává	podávat	k5eNaImIp3nS
pouze	pouze	k6eAd1
proměněný	proměněný	k2eAgInSc1d1
chléb	chléb	k1gInSc1
<g/>
,	,	kIx,
kdežto	kdežto	k8xS
v	v	k7c6
římskokatolické	římskokatolický	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
se	se	k3xPyFc4
věřícím	věřící	k1gMnPc3
podávají	podávat	k5eAaImIp3nP
samotné	samotný	k2eAgFnPc4d1
hostie	hostie	k1gFnPc4
</s>
<s>
v	v	k7c6
pravoslaví	pravoslaví	k1gNnSc6
se	se	k3xPyFc4
nepodává	podávat	k5eNaImIp3nS
samotná	samotný	k2eAgFnSc1d1
Kristova	Kristův	k2eAgFnSc1d1
krev	krev	k1gFnSc1
<g/>
,	,	kIx,
leč	leč	k8xC,k8xS
krev	krev	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
už	už	k6eAd1
byla	být	k5eAaImAgFnS
sjednocena	sjednotit	k5eAaPmNgFnS
s	s	k7c7
Tělem	tělo	k1gNnSc7
Kristovým	Kristův	k2eAgNnSc7d1
</s>
<s>
v	v	k7c6
římskokatolické	římskokatolický	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
se	se	k3xPyFc4
podává	podávat	k5eAaImIp3nS
nekvašený	kvašený	k2eNgInSc1d1
chléb	chléb	k1gInSc1
–	–	k?
oplatka	oplatka	k1gFnSc1
čili	čili	k8xC
hostie	hostie	k1gFnSc1
–	–	k?
a	a	k8xC
většinou	většinou	k6eAd1
bílé	bílý	k2eAgNnSc1d1
víno	víno	k1gNnSc1
<g/>
,	,	kIx,
v	v	k7c6
pravoslavné	pravoslavný	k2eAgFnSc3d1
církvi	církev	k1gFnSc3
jedině	jedině	k6eAd1
kvašený	kvašený	k2eAgInSc4d1
chléb	chléb	k1gInSc4
a	a	k8xC
většinou	většinou	k6eAd1
červené	červený	k2eAgNnSc4d1
víno	víno	k1gNnSc4
</s>
<s>
odlišnost	odlišnost	k1gFnSc1
v	v	k7c6
chápání	chápání	k1gNnSc6
epiklese	epiklese	k1gFnSc2
(	(	kIx(
<g/>
vzývání	vzývání	k1gNnSc4
Svatého	svatý	k2eAgMnSc2d1
Ducha	duch	k1gMnSc2
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
proměnění	proměnění	k1gNnSc6
chleba	chléb	k1gInSc2
a	a	k8xC
vína	víno	k1gNnSc2
na	na	k7c4
tělo	tělo	k1gNnSc4
a	a	k8xC
krev	krev	k1gFnSc4
Kristovu	Kristův	k2eAgFnSc4d1
</s>
<s>
rozdíly	rozdíl	k1gInPc1
v	v	k7c6
konání	konání	k1gNnSc6
konkrétních	konkrétní	k2eAgFnPc2d1
svátostí	svátost	k1gFnPc2
(	(	kIx(
<g/>
Tajin	tajina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
způsob	způsob	k1gInSc1
křtu	křest	k1gInSc2
<g/>
:	:	kIx,
v	v	k7c6
pravoslaví	pravoslaví	k1gNnSc6
by	by	kYmCp3nS
se	se	k3xPyFc4
křest	křest	k1gInSc1
správně	správně	k6eAd1
měl	mít	k5eAaImAgInS
konat	konat	k5eAaImF
ponořením	ponoření	k1gNnSc7
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
kropením	kropení	k1gNnSc7
jako	jako	k8xC,k8xS
v	v	k7c6
případě	případ	k1gInSc6
členů	člen	k1gMnPc2
římskokatolické	římskokatolický	k2eAgFnSc2d1
církve	církev	k1gFnSc2
</s>
<s>
způsob	způsob	k1gInSc1
svěcení	svěcení	k1gNnSc2
kněžstva	kněžstvo	k1gNnSc2
</s>
<s>
způsob	způsob	k1gInSc1
sňatku	sňatek	k1gInSc2
a	a	k8xC
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
pojetí	pojetí	k1gNnSc2
<g/>
:	:	kIx,
v	v	k7c6
římskokatolické	římskokatolický	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
si	se	k3xPyFc3
snoubenci	snoubenec	k1gMnPc1
udělují	udělovat	k5eAaImIp3nP
svátost	svátost	k1gFnSc4
navzájem	navzájem	k6eAd1
<g/>
,	,	kIx,
skládají	skládat	k5eAaImIp3nP
manželský	manželský	k2eAgInSc4d1
slib	slib	k1gInSc4
<g/>
;	;	kIx,
pravoslavné	pravoslavný	k2eAgNnSc4d1
učení	učení	k1gNnSc4
nic	nic	k3yNnSc1
takového	takový	k3xDgMnSc4
nezná	znát	k5eNaImIp3nS,k5eAaImIp3nS
</s>
<s>
právní	právní	k2eAgFnPc1d1
otázky	otázka	k1gFnPc1
týkající	týkající	k2eAgFnPc1d1
se	se	k3xPyFc4
manželství	manželství	k1gNnSc1
a	a	k8xC
otázka	otázka	k1gFnSc1
případných	případný	k2eAgNnPc2d1
dalších	další	k2eAgNnPc2d1
manželství	manželství	k1gNnPc2
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Milost	milost	k1gFnSc1
</s>
<s>
Svátostina	svátostina	k1gFnSc1
</s>
<s>
Svěcení	svěcení	k1gNnSc1
</s>
<s>
Zpovědní	zpovědní	k2eAgNnSc1d1
tajemství	tajemství	k1gNnSc1
</s>
<s>
Svátosti	svátost	k1gFnPc1
v	v	k7c6
mormonismu	mormonismus	k1gInSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
svátost	svátost	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
svátost	svátost	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Katechismus	katechismus	k1gInSc1
katolické	katolický	k2eAgFnSc2d1
církve	církev	k1gFnSc2
</s>
<s>
Liturgický	liturgický	k2eAgInSc1d1
život	život	k1gInSc1
pravoslavné	pravoslavný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Svátosti	svátost	k1gFnPc1
</s>
<s>
Křest	křest	k1gInSc1
•	•	k?
Biřmování	biřmování	k1gNnSc2
•	•	k?
Eucharistie	eucharistie	k1gFnSc1
•	•	k?
Svátost	svátost	k1gFnSc1
smíření	smíření	k1gNnSc2
•	•	k?
Svátost	svátost	k1gFnSc1
nemocných	nemocná	k1gFnPc2
•	•	k?
Kněžství	kněžství	k1gNnSc4
•	•	k?
Manželství	manželství	k1gNnSc2
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4051342-7	4051342-7	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
7843	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85116252	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85116252	#num#	k4
</s>
