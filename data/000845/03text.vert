<s>
Phar	Phar	k1gInSc1	Phar
Lap	lap	k1gInSc1	lap
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
-	-	kIx~	-
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
proslulý	proslulý	k2eAgMnSc1d1	proslulý
dostihový	dostihový	k2eAgMnSc1d1	dostihový
kůň	kůň	k1gMnSc1	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
ryzáka	ryzák	k1gMnSc4	ryzák
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
předkem	předek	k1gMnSc7	předek
byl	být	k5eAaImAgMnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Eclipse	Eclipse	k1gFnSc2	Eclipse
<g/>
.	.	kIx.	.
</s>
<s>
Trenér	trenér	k1gMnSc1	trenér
Harry	Harra	k1gFnSc2	Harra
Telford	Telford	k1gMnSc1	Telford
jej	on	k3xPp3gMnSc4	on
přivezl	přivézt	k5eAaPmAgMnS	přivézt
roku	rok	k1gInSc2	rok
1928	[number]	k4	1928
z	z	k7c2	z
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
za	za	k7c4	za
pouhých	pouhý	k2eAgFnPc2d1	pouhá
160	[number]	k4	160
guineí	guine	k1gFnPc2	guine
<g/>
.	.	kIx.	.
</s>
<s>
Majiteli	majitel	k1gMnSc3	majitel
se	se	k3xPyFc4	se
kůň	kůň	k1gMnSc1	kůň
nelíbil	líbit	k5eNaImAgMnS	líbit
a	a	k8xC	a
chtěl	chtít	k5eAaImAgMnS	chtít
ho	on	k3xPp3gInSc4	on
obratem	obratem	k6eAd1	obratem
prodat	prodat	k5eAaPmF	prodat
<g/>
.	.	kIx.	.
</s>
<s>
Trenér	trenér	k1gMnSc1	trenér
mu	on	k3xPp3gMnSc3	on
tedy	tedy	k9	tedy
zaplatil	zaplatit	k5eAaPmAgInS	zaplatit
polovinu	polovina	k1gFnSc4	polovina
kupní	kupní	k2eAgFnSc2d1	kupní
částky	částka	k1gFnSc2	částka
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
jej	on	k3xPp3gMnSc4	on
cvičit	cvičit	k5eAaImF	cvičit
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
neuměl	umět	k5eNaImAgMnS	umět
na	na	k7c6	na
trénincích	trénink	k1gInPc6	trénink
cválat	cválat	k5eAaImF	cválat
<g/>
,	,	kIx,	,
naučil	naučit	k5eAaPmAgMnS	naučit
ho	on	k3xPp3gNnSc4	on
to	ten	k3xDgNnSc4	ten
H.	H.	kA	H.
Telford	Telford	k1gInSc4	Telford
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
skoro	skoro	k6eAd1	skoro
strhal	strhat	k5eAaPmAgMnS	strhat
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
se	se	k3xPyFc4	se
Harrymu	Harrym	k1gInSc2	Harrym
i	i	k8xC	i
koni	kůň	k1gMnPc1	kůň
smáli	smát	k5eAaImAgMnP	smát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeden	jeden	k4xCgMnSc1	jeden
pán	pán	k1gMnSc1	pán
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ho	on	k3xPp3gMnSc4	on
viděl	vidět	k5eAaImAgMnS	vidět
běžet	běžet	k5eAaImF	běžet
<g/>
,	,	kIx,	,
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
jméno	jméno	k1gNnSc4	jméno
"	"	kIx"	"
<g/>
Blesk	blesk	k1gInSc1	blesk
<g/>
"	"	kIx"	"
-	-	kIx~	-
Far	fara	k1gFnPc2	fara
Lap	lap	k1gInSc1	lap
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Harry	Harr	k1gMnPc4	Harr
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
na	na	k7c4	na
7	[number]	k4	7
písmen	písmeno	k1gNnPc2	písmeno
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
poslední	poslední	k2eAgMnPc1d1	poslední
vítězové	vítěz	k1gMnPc1	vítěz
v	v	k7c6	v
Melbourne	Melbourne	k1gNnSc6	Melbourne
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
vymyšleno	vymyslet	k5eAaPmNgNnS	vymyslet
jméno	jméno	k1gNnSc4	jméno
Phar	Phara	k1gFnPc2	Phara
Lap	lap	k1gInSc1	lap
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
vedený	vedený	k2eAgInSc1d1	vedený
výcvik	výcvik	k1gInSc1	výcvik
vyústil	vyústit	k5eAaPmAgInS	vyústit
ve	v	k7c4	v
skvělé	skvělý	k2eAgInPc4d1	skvělý
výsledky	výsledek	k1gInPc4	výsledek
na	na	k7c6	na
travnaté	travnatý	k2eAgFnSc6d1	travnatá
dráze	dráha	k1gFnSc6	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
51	[number]	k4	51
dostihů	dostih	k1gInPc2	dostih
jich	on	k3xPp3gInPc2	on
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
37	[number]	k4	37
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
sérii	série	k1gFnSc4	série
33	[number]	k4	33
vítězství	vítězství	k1gNnSc2	vítězství
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
platný	platný	k2eAgInSc4d1	platný
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Phar	Phar	k1gInSc1	Phar
Lap	lap	k1gInSc1	lap
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
proslavil	proslavit	k5eAaPmAgMnS	proslavit
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
i	i	k9	i
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
před	před	k7c7	před
jedním	jeden	k4xCgInSc7	jeden
důležitým	důležitý	k2eAgInSc7d1	důležitý
závodem	závod	k1gInSc7	závod
spáchán	spáchán	k2eAgInSc4d1	spáchán
atentát	atentát	k1gInSc4	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Kulka	kulka	k1gFnSc1	kulka
ho	on	k3xPp3gMnSc4	on
však	však	k9	však
minula	minout	k5eAaImAgFnS	minout
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
zanedlouho	zanedlouho	k6eAd1	zanedlouho
po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
si	se	k3xPyFc3	se
jeho	on	k3xPp3gInSc4	on
majitel	majitel	k1gMnSc1	majitel
nepřál	přát	k5eNaImAgMnS	přát
pitvu	pitva	k1gFnSc4	pitva
<g/>
,	,	kIx,	,
hovořilo	hovořit	k5eAaImAgNnS	hovořit
se	se	k3xPyFc4	se
o	o	k7c6	o
kolice	kolika	k1gFnSc6	kolika
<g/>
.	.	kIx.	.
</s>
<s>
Objevily	objevit	k5eAaPmAgInP	objevit
se	se	k3xPyFc4	se
i	i	k9	i
fámy	fáma	k1gFnPc1	fáma
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
Phar	Phar	k1gInSc1	Phar
Lap	lap	k1gInSc1	lap
otráven	otráven	k2eAgInSc1d1	otráven
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
nenáviděn	nenávidět	k5eAaImNgMnS	nenávidět
soupeři	soupeř	k1gMnPc7	soupeř
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
potvridlo	potvridlo	k1gNnSc4	potvridlo
při	při	k7c6	při
pitvě	pitva	k1gFnSc6	pitva
provedené	provedený	k2eAgFnPc4d1	provedená
asi	asi	k9	asi
20	[number]	k4	20
let	léto	k1gNnPc2	léto
po	po	k7c6	po
Phar	Phar	k1gMnSc1	Phar
Lapově	Lapův	k2eAgFnSc3d1	Lapův
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Proto	proto	k8xC	proto
mu	on	k3xPp3gMnSc3	on
pořadatelé	pořadatel	k1gMnPc1	pořadatel
dávali	dávat	k5eAaImAgMnP	dávat
do	do	k7c2	do
sedla	sedlo	k1gNnSc2	sedlo
závaží	závaží	k1gNnSc2	závaží
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
dostih	dostih	k1gInSc1	dostih
podle	podle	k7c2	podle
nich	on	k3xPp3gInPc2	on
spravedlivý	spravedlivý	k2eAgInSc1d1	spravedlivý
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
handicapován	handicapovat	k5eAaBmNgInS	handicapovat
zátěží	zátěž	k1gFnSc7	zátěž
10	[number]	k4	10
až	až	k9	až
20	[number]	k4	20
kg	kg	kA	kg
<g/>
,	,	kIx,	,
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgMnSc1	tento
kůň	kůň	k1gMnSc1	kůň
měl	mít	k5eAaImAgMnS	mít
2	[number]	k4	2
<g/>
x	x	k?	x
větší	veliký	k2eAgNnSc4d2	veliký
srdce	srdce	k1gNnSc4	srdce
než	než	k8xS	než
ostatní	ostatní	k2eAgMnPc4d1	ostatní
koně	kůň	k1gMnPc4	kůň
(	(	kIx(	(
<g/>
vážilo	vážit	k5eAaImAgNnS	vážit
6,2	[number]	k4	6,2
kg	kg	kA	kg
<g/>
,	,	kIx,	,
normální	normální	k2eAgFnSc1d1	normální
váha	váha	k1gFnSc1	váha
srdce	srdce	k1gNnSc2	srdce
se	se	k3xPyFc4	se
u	u	k7c2	u
koní	kůň	k1gMnPc2	kůň
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
3	[number]	k4	3
a	a	k8xC	a
4	[number]	k4	4
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgMnSc3	ten
mohl	moct	k5eAaImAgMnS	moct
běžet	běžet	k5eAaImF	běžet
rychleji	rychle	k6eAd2	rychle
-	-	kIx~	-
měl	mít	k5eAaImAgMnS	mít
lépe	dobře	k6eAd2	dobře
okysličenou	okysličený	k2eAgFnSc4d1	okysličená
krev	krev	k1gFnSc4	krev
<g/>
.	.	kIx.	.
</s>
<s>
Hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
rodokmenu	rodokmen	k1gInSc3	rodokmen
ho	on	k3xPp3gInSc4	on
H.	H.	kA	H.
Telford	Telford	k1gInSc4	Telford
koupil	koupit	k5eAaPmAgInS	koupit
a	a	k8xC	a
dával	dávat	k5eAaImAgInS	dávat
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
naděje	nadát	k5eAaBmIp3nS	nadát
<g/>
.	.	kIx.	.
</s>
<s>
Obsahoval	obsahovat	k5eAaImAgInS	obsahovat
slavného	slavný	k2eAgMnSc4d1	slavný
vítěze	vítěz	k1gMnSc4	vítěz
Melbourne	Melbourne	k1gNnSc2	Melbourne
Cup	cup	k1gInSc4	cup
-	-	kIx~	-
Carbine	Carbin	k1gMnSc5	Carbin
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
<g/>
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
prapra	prapra	k1gMnSc1	prapra
<g/>
...	...	k?	...
<g/>
potomkem	potomek	k1gMnSc7	potomek
Eclipse	Eclips	k1gMnSc2	Eclips
<g/>
,	,	kIx,	,
<g/>
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
při	při	k7c6	při
zatmění	zatmění	k1gNnSc6	zatmění
a	a	k8xC	a
také	také	k9	také
měl	mít	k5eAaImAgInS	mít
dvakrát	dvakrát	k6eAd1	dvakrát
větší	veliký	k2eAgNnSc4d2	veliký
srdce	srdce	k1gNnSc4	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Australský	australský	k2eAgMnSc1d1	australský
režisér	režisér	k1gMnSc1	režisér
Simon	Simon	k1gMnSc1	Simon
Wincer	Wincer	k1gMnSc1	Wincer
o	o	k7c6	o
něm	on	k3xPp3gNnSc6	on
natočil	natočit	k5eAaBmAgMnS	natočit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
životopisný	životopisný	k2eAgInSc1d1	životopisný
film	film	k1gInSc1	film
Phar	Phar	k1gMnSc1	Phar
Lap	lap	k1gInSc1	lap
<g/>
.	.	kIx.	.
</s>
