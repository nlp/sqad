<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xS	jako
nástroj	nástroj	k1gInSc1	nástroj
genocidy	genocida	k1gFnSc2	genocida
v	v	k7c6	v
plynových	plynový	k2eAgFnPc6d1	plynová
komorách	komora	k1gFnPc6	komora
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
především	především	k9	především
v	v	k7c6	v
táborech	tábor	k1gInPc6	tábor
Auschwitz-Birkenau	Auschwitz-Birkenaus	k1gInSc2	Auschwitz-Birkenaus
a	a	k8xC	a
Majdanek	Majdanka	k1gFnPc2	Majdanka
<g/>
.	.	kIx.	.
</s>
