<s>
Klasické	klasický	k2eAgNnSc1d1	klasické
podmiňování	podmiňování	k1gNnSc1	podmiňování
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
psy	pes	k1gMnPc7	pes
přímo	přímo	k6eAd1	přímo
spjato	spjat	k2eAgNnSc1d1	spjato
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
výzkumům	výzkum	k1gInPc3	výzkum
ruského	ruský	k2eAgMnSc4d1	ruský
fyziologa	fyziolog	k1gMnSc4	fyziolog
a	a	k8xC	a
nositele	nositel	k1gMnSc4	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
<g/>
,	,	kIx,	,
I.	I.	kA	I.
P.	P.	kA	P.
Pavlova	Pavlův	k2eAgInSc2d1	Pavlův
<g/>
.	.	kIx.	.
</s>
