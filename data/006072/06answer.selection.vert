<s>
Helium	helium	k1gNnSc1	helium
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
He	he	k0	he
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Helium	helium	k1gNnSc4	helium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
plynný	plynný	k2eAgInSc1d1	plynný
chemický	chemický	k2eAgInSc1d1	chemický
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgFnSc1d1	patřící
mezi	mezi	k7c4	mezi
vzácné	vzácný	k2eAgInPc4d1	vzácný
plyny	plyn	k1gInPc4	plyn
a	a	k8xC	a
tvořící	tvořící	k2eAgFnPc4d1	tvořící
druhou	druhý	k4xOgFnSc4	druhý
nejvíce	hodně	k6eAd3	hodně
zastoupenou	zastoupený	k2eAgFnSc4d1	zastoupená
složku	složka	k1gFnSc4	složka
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
