<s>
Středoevropský	středoevropský	k2eAgInSc1d1
čas	čas	k1gInSc1
<g/>
,	,	kIx,
zkratka	zkratka	k1gFnSc1
SEČ	seč	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Central	Central	k1gFnSc1
European	Europeana	k1gFnPc2
Time	Time	k1gFnPc2
<g/>
,	,	kIx,
CET	ceta	k1gFnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
pásmový	pásmový	k2eAgInSc1d1
čas	čas	k1gInSc1
platný	platný	k2eAgInSc1d1
pro	pro	k7c4
střední	střední	k2eAgFnSc4d1
Evropu	Evropa	k1gFnSc4
<g/>
,	,	kIx,
časové	časový	k2eAgNnSc4d1
pásmo	pásmo	k1gNnSc4
od	od	k7c2
15	#num#	k4
<g/>
°	°	k?
východní	východní	k2eAgFnSc2d1
délky	délka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>