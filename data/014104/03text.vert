<s>
Středoevropský	středoevropský	k2eAgInSc1d1
čas	čas	k1gInSc1
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1
<g/>
,	,	kIx,
západoevropský	západoevropský	k2eAgInSc1d1
čas	čas	k1gInSc1
-	-	kIx~
ZEČ	ZEČ	kA
(	(	kIx(
<g/>
WET	WET	kA
<g/>
/	/	kIx~
<g/>
UTC	UTC	kA
<g/>
)	)	kIx)
<g/>
Červená	červený	k2eAgFnSc1d1
<g/>
,	,	kIx,
středoevropský	středoevropský	k2eAgInSc1d1
čas	čas	k1gInSc1
-	-	kIx~
SEČ	SEČ	kA
(	(	kIx(
<g/>
CET	ceta	k1gFnPc2
<g/>
/	/	kIx~
<g/>
UTC	UTC	kA
<g/>
+	+	kIx~
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
Žlutá	žlutat	k5eAaImIp3nS
<g/>
,	,	kIx,
východoevropský	východoevropský	k2eAgInSc1d1
čas	čas	k1gInSc1
-	-	kIx~
VEČ	VEČ	kA
(	(	kIx(
<g/>
EET	EET	kA
<g/>
/	/	kIx~
<g/>
UTC	UTC	kA
<g/>
+	+	kIx~
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
Zelená	Zelená	k1gFnSc1
<g/>
,	,	kIx,
východoevropský	východoevropský	k2eAgInSc4d1
čas	čas	k1gInSc4
-	-	kIx~
(	(	kIx(
<g/>
FET	FET	kA
<g/>
/	/	kIx~
<g/>
UTC	UTC	kA
<g/>
+	+	kIx~
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Středoevropský	středoevropský	k2eAgInSc1d1
čas	čas	k1gInSc1
<g/>
,	,	kIx,
zkratka	zkratka	k1gFnSc1
SEČ	seč	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Central	Central	k1gFnSc1
European	Europeana	k1gFnPc2
Time	Time	k1gFnPc2
<g/>
,	,	kIx,
CET	ceta	k1gFnPc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
pásmový	pásmový	k2eAgInSc1d1
čas	čas	k1gInSc1
platný	platný	k2eAgInSc1d1
pro	pro	k7c4
střední	střední	k2eAgFnSc4d1
Evropu	Evropa	k1gFnSc4
<g/>
,	,	kIx,
časové	časový	k2eAgNnSc4d1
pásmo	pásmo	k1gNnSc4
od	od	k7c2
15	#num#	k4
<g/>
°	°	k?
východní	východní	k2eAgFnSc2d1
délky	délka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
SEČ	SEČ	kA
=	=	kIx~
UTC	UTC	kA
+	+	kIx~
1	#num#	k4
hod	hod	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Středoevropský	středoevropský	k2eAgInSc1d1
čas	čas	k1gInSc1
platí	platit	k5eAaImIp3nS
ve	v	k7c6
většině	většina	k1gFnSc6
evropských	evropský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
:	:	kIx,
kromě	kromě	k7c2
Česka	Česko	k1gNnSc2
a	a	k8xC
Slovenska	Slovensko	k1gNnSc2
také	také	k9
v	v	k7c6
Německu	Německo	k1gNnSc6
<g/>
,	,	kIx,
Polsku	Polsko	k1gNnSc6
<g/>
,	,	kIx,
Maďarsku	Maďarsko	k1gNnSc6
<g/>
,	,	kIx,
Rakousku	Rakousko	k1gNnSc6
<g/>
,	,	kIx,
Švýcarsku	Švýcarsko	k1gNnSc6
<g/>
,	,	kIx,
Nizozemsku	Nizozemsko	k1gNnSc6
<g/>
,	,	kIx,
Francii	Francie	k1gFnSc6
<g/>
,	,	kIx,
Itálii	Itálie	k1gFnSc6
<g/>
,	,	kIx,
Španělsku	Španělsko	k1gNnSc6
<g/>
,	,	kIx,
Norsku	Norsko	k1gNnSc6
<g/>
,	,	kIx,
Švédsku	Švédsko	k1gNnSc6
a	a	k8xC
v	v	k7c6
dalších	další	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
jarním	jarní	k2eAgNnSc6d1
a	a	k8xC
letním	letní	k2eAgNnSc6d1
období	období	k1gNnSc6
je	být	k5eAaImIp3nS
ve	v	k7c6
většině	většina	k1gFnSc6
států	stát	k1gInPc2
Evropy	Evropa	k1gFnSc2
zaváděn	zaváděn	k2eAgInSc4d1
středoevropský	středoevropský	k2eAgInSc4d1
letní	letní	k2eAgInSc4d1
čas	čas	k1gInSc4
(	(	kIx(
<g/>
SELČ	SELČ	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Rakousko-uherské	rakousko-uherský	k2eAgFnPc1d1
železnice	železnice	k1gFnPc1
a	a	k8xC
pošty	pošta	k1gFnPc1
přijaly	přijmout	k5eAaPmAgFnP
SEČ	seč	k6eAd1
1	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1891	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1968	#num#	k4
a	a	k8xC
1971	#num#	k4
byl	být	k5eAaImAgInS
středoevropský	středoevropský	k2eAgInSc1d1
čas	čas	k1gInSc1
pokusně	pokusně	k6eAd1
zaveden	zavést	k5eAaPmNgInS
také	také	k9
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Británii	Británie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Středoevropský	středoevropský	k2eAgInSc1d1
letní	letní	k2eAgInSc1d1
čas	čas	k1gInSc1
</s>
<s>
Zimní	zimní	k2eAgInSc1d1
čas	čas	k1gInSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Universum	universum	k1gNnSc1
<g/>
,	,	kIx,
všeobecná	všeobecný	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
Euromedia	Euromedium	k1gNnPc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
646	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
207	#num#	k4
<g/>
-	-	kIx~
<g/>
1071	#num#	k4
<g/>
-	-	kIx~
<g/>
X.	X.	kA
S.	S.	kA
148	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Universum	universum	k1gNnSc1
<g/>
,	,	kIx,
všeobecná	všeobecný	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
díl	díl	k1gInSc1
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Odeon	odeon	k1gInSc1
<g/>
,	,	kIx,
Euromedia	Euromedium	k1gNnPc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
681	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
207	#num#	k4
<g/>
-	-	kIx~
<g/>
1062	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
170	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Evropské	evropský	k2eAgFnSc2d1
země	zem	k1gFnSc2
používající	používající	k2eAgInSc4d1
středoevropský	středoevropský	k2eAgInSc4d1
čas	čas	k1gInSc4
(	(	kIx(
<g/>
CET	ceta	k1gFnPc2
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
