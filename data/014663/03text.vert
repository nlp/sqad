<s>
Kontinent	kontinent	k1gInSc1
</s>
<s>
Mapa	mapa	k1gFnSc1
světa	svět	k1gInSc2
zobrazující	zobrazující	k2eAgFnSc1d1
6	#num#	k4
barevně	barevna	k1gFnSc3
odlišených	odlišený	k2eAgInPc2d1
kontinentů	kontinent	k1gInPc2
</s>
<s>
Světadíl	světadíl	k1gInSc1
nebo	nebo	k8xC
kontinent	kontinent	k1gInSc1
(	(	kIx(
<g/>
z	z	k7c2
latinského	latinský	k2eAgInSc2d1
contineō	contineō	k1gInSc2
<g/>
,	,	kIx,
-ē	-ē	kA
<g/>
,	,	kIx,
což	což	k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
držím	držet	k5eAaImIp1nS
pohromadě	pohromadě	k6eAd1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
velkou	velký	k2eAgFnSc7d1
souvislou	souvislý	k2eAgFnSc7d1
pevninskou	pevninský	k2eAgFnSc7d1
masou	masa	k1gFnSc7
na	na	k7c6
povrchu	povrch	k1gInSc6
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozlišuje	rozlišovat	k5eAaImIp3nS
se	se	k3xPyFc4
maximálně	maximálně	k6eAd1
sedm	sedm	k4xCc4
kontinentů	kontinent	k1gInPc2
(	(	kIx(
<g/>
podle	podle	k7c2
rozlohy	rozloha	k1gFnSc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Asie	Asie	k1gFnSc1
<g/>
,	,	kIx,
Afrika	Afrika	k1gFnSc1
<g/>
,	,	kIx,
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
<g/>
,	,	kIx,
Antarktida	Antarktida	k1gFnSc1
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc1
a	a	k8xC
Evropa	Evropa	k1gFnSc1
<g/>
,	,	kIx,
podle	podle	k7c2
jiného	jiný	k2eAgNnSc2d1
dělení	dělení	k1gNnSc2
se	se	k3xPyFc4
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
jeden	jeden	k4xCgInSc4
kontinent	kontinent	k1gInSc4
Amerika	Amerika	k1gFnSc1
a	a	k8xC
<g/>
/	/	kIx~
<g/>
nebo	nebo	k8xC
Eurasie	Eurasie	k1gFnSc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
Eurafrasie	Eurafrasie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
zeměpisná	zeměpisný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
rozlišuje	rozlišovat	k5eAaImIp3nS
5	#num#	k4
kontinentů	kontinent	k1gInPc2
a	a	k8xC
7	#num#	k4
světadílů	světadíl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Jelikož	jelikož	k8xS
neexistuje	existovat	k5eNaImIp3nS
žádný	žádný	k3yNgInSc4
standard	standard	k1gInSc4
pro	pro	k7c4
definici	definice	k1gFnSc4
kontinentu	kontinent	k1gInSc2
<g/>
,	,	kIx,
různé	různý	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
a	a	k8xC
vědní	vědní	k2eAgInPc4d1
obory	obor	k1gInPc4
mají	mít	k5eAaImIp3nP
odlišné	odlišný	k2eAgInPc4d1
seznamy	seznam	k1gInPc4
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
považují	považovat	k5eAaImIp3nP
za	za	k7c4
kontinent	kontinent	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všeobecně	všeobecně	k6eAd1
uznané	uznaný	k2eAgFnPc4d1
charakteristiky	charakteristika	k1gFnPc4
kontinentu	kontinent	k1gInSc2
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
musí	muset	k5eAaImIp3nP
jít	jít	k5eAaImF
o	o	k7c4
rozlohou	rozloha	k1gFnSc7
velké	velký	k2eAgNnSc4d1
území	území	k1gNnSc4
<g/>
,	,	kIx,
musí	muset	k5eAaImIp3nP
sestávat	sestávat	k5eAaImF
z	z	k7c2
neponořené	ponořený	k2eNgFnSc2d1
pevniny	pevnina	k1gFnSc2
a	a	k8xC
musí	muset	k5eAaImIp3nS
mít	mít	k5eAaImF
geologicky	geologicky	k6eAd1
význačné	význačný	k2eAgFnPc4d1
hranice	hranice	k1gFnPc4
(	(	kIx(
<g/>
v	v	k7c4
úvahu	úvaha	k1gFnSc4
se	se	k3xPyFc4
můžou	můžou	k?
kromě	kromě	k7c2
moří	moře	k1gNnPc2
či	či	k8xC
hor	hora	k1gFnPc2
brát	brát	k5eAaImF
např.	např.	kA
i	i	k8xC
litosférické	litosférický	k2eAgFnSc2d1
desky	deska	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
podle	podle	k7c2
některých	některý	k3yIgInPc2
existuje	existovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
4	#num#	k4
nebo	nebo	k8xC
5	#num#	k4
kontinentů	kontinent	k1gInPc2
(	(	kIx(
<g/>
například	například	k6eAd1
OSN	OSN	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
světě	svět	k1gInSc6
se	se	k3xPyFc4
nejčastěji	často	k6eAd3
uvádí	uvádět	k5eAaImIp3nS
7	#num#	k4
(	(	kIx(
<g/>
západoevropský	západoevropský	k2eAgInSc1d1
svět	svět	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
resp.	resp.	kA
6	#num#	k4
(	(	kIx(
<g/>
východoevropský	východoevropský	k2eAgInSc4d1
svět	svět	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Dvěma	dva	k4xCgFnPc7
nejčastějšími	častý	k2eAgFnPc7d3
neshodami	neshoda	k1gFnPc7
v	v	k7c6
udávání	udávání	k1gNnSc6
počtu	počet	k1gInSc2
kontinentů	kontinent	k1gInPc2
jsou	být	k5eAaImIp3nP
otázky	otázka	k1gFnPc4
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
<g/>
-li	-li	k?
se	se	k3xPyFc4
Evropa	Evropa	k1gFnSc1
a	a	k8xC
Asie	Asie	k1gFnSc1
počítat	počítat	k5eAaImF
odděleně	odděleně	k6eAd1
nebo	nebo	k8xC
dohromady	dohromady	k6eAd1
jako	jako	k9
Eurasie	Eurasie	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
mají	mít	k5eAaImIp3nP
<g/>
-li	-li	k?
Severní	severní	k2eAgFnSc1d1
a	a	k8xC
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
být	být	k5eAaImF
považovány	považován	k2eAgFnPc1d1
za	za	k7c4
dva	dva	k4xCgInPc4
kontinenty	kontinent	k1gInPc4
nebo	nebo	k8xC
za	za	k7c4
jediný	jediný	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Několik	několik	k4yIc1
geografů	geograf	k1gMnPc2
<g/>
[	[	kIx(
<g/>
kterých	který	k3yRgMnPc2,k3yQgMnPc2,k3yIgMnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
také	také	k9
navrhlo	navrhnout	k5eAaPmAgNnS
seskupit	seskupit	k5eAaPmF
Evropu	Evropa	k1gFnSc4
<g/>
,	,	kIx,
Asii	Asie	k1gFnSc4
a	a	k8xC
Afriku	Afrika	k1gFnSc4
do	do	k7c2
jediného	jediný	k2eAgInSc2d1
kontinentu	kontinent	k1gInSc2
<g/>
,	,	kIx,
Eurafrasie	Eurafrasie	k1gFnSc1
(	(	kIx(
<g/>
v	v	k7c6
geopolitice	geopolitika	k1gFnSc6
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
rovněž	rovněž	k6eAd1
říká	říkat	k5eAaImIp3nS
světový	světový	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
laiky	laik	k1gMnPc4
se	se	k3xPyFc4
situace	situace	k1gFnSc1
stává	stávat	k5eAaImIp3nS
ještě	ještě	k9
více	hodně	k6eAd2
matoucí	matoucí	k2eAgMnSc1d1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
případě	případ	k1gInSc6
Ameriky	Amerika	k1gFnSc2
se	se	k3xPyFc4
paralelně	paralelně	k6eAd1
používá	používat	k5eAaImIp3nS
výrazů	výraz	k1gInPc2
Latinská	latinský	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
a	a	k8xC
zejména	zejména	k9
Střední	střední	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
sice	sice	k8xC
označují	označovat	k5eAaImIp3nP
jisté	jistý	k2eAgFnPc4d1
části	část	k1gFnPc4
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
ne	ne	k9
však	však	k9
nějaký	nějaký	k3yIgInSc4
kontinent	kontinent	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgFnSc1d1
zeměpisná	zeměpisný	k2eAgFnSc1d1
škola	škola	k1gFnSc1
spory	spor	k1gInPc1
v	v	k7c6
počtu	počet	k1gInSc6
kontinentů	kontinent	k1gInPc2
většinou	většinou	k6eAd1
řeší	řešit	k5eAaImIp3nS
vydělením	vydělení	k1gNnSc7
dvou	dva	k4xCgInPc2
různých	různý	k2eAgInPc2d1
termínů	termín	k1gInPc2
<g/>
:	:	kIx,
kontinent	kontinent	k1gInSc4
a	a	k8xC
světadíl	světadíl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
za	za	k7c4
kontinent	kontinent	k1gInSc4
(	(	kIx(
<g/>
5	#num#	k4
<g/>
×	×	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
považována	považován	k2eAgFnSc1d1
souvislá	souvislý	k2eAgFnSc1d1
část	část	k1gFnSc1
souše	souš	k1gFnSc2
obklopená	obklopený	k2eAgNnPc4d1
světovým	světový	k2eAgInSc7d1
oceánem	oceán	k1gInSc7
a	a	k8xC
tvořená	tvořený	k2eAgNnPc4d1
pevninským	pevninský	k2eAgInSc7d1
typem	typ	k1gInSc7
zemské	zemský	k2eAgFnSc2d1
kůry	kůra	k1gFnSc2
(	(	kIx(
<g/>
kontinentální	kontinentální	k2eAgFnSc1d1
kůra	kůra	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
světadíl	světadíl	k1gInSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
×	×	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vnímán	vnímat	k5eAaImNgInS
jako	jako	k8xC,k8xS
oblast	oblast	k1gFnSc1
se	s	k7c7
samostatným	samostatný	k2eAgInSc7d1
geografickým	geografický	k2eAgInSc7d1
a	a	k8xC
historickým	historický	k2eAgInSc7d1
vývojem	vývoj	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
světa	svět	k1gInSc2
na	na	k7c4
7	#num#	k4
kontinentů	kontinent	k1gInPc2
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
vyučuje	vyučovat	k5eAaImIp3nS
v	v	k7c6
západní	západní	k2eAgFnSc6d1
a	a	k8xC
střední	střední	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
a	a	k8xC
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
rozdělení	rozdělení	k1gNnSc1
na	na	k7c4
6	#num#	k4
kontinentů	kontinent	k1gInPc2
(	(	kIx(
<g/>
spojená	spojený	k2eAgFnSc1d1
Eurasie	Eurasie	k1gFnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
také	také	k9
vyučuje	vyučovat	k5eAaImIp3nS
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
a	a	k8xC
dál	daleko	k6eAd2
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
Evropě	Evropa	k1gFnSc6
a	a	k8xC
Jižní	jižní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
či	či	k8xC
v	v	k7c6
Asii	Asie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šest	šest	k4xCc1
je	být	k5eAaImIp3nS
také	také	k6eAd1
počet	počet	k1gInSc1
kontinentů	kontinent	k1gInPc2
používaných	používaný	k2eAgInPc2d1
ve	v	k7c6
vědeckém	vědecký	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Kontinenty	kontinent	k1gInPc1
jsou	být	k5eAaImIp3nP
občas	občas	k6eAd1
dál	daleko	k6eAd2
pomyslně	pomyslně	k6eAd1
děleny	dělen	k2eAgInPc4d1
na	na	k7c4
subkontinenty	subkontinent	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Definice	definice	k1gFnSc1
tohoto	tento	k3xDgInSc2
termínu	termín	k1gInSc2
je	být	k5eAaImIp3nS
ještě	ještě	k6eAd1
nepřesnější	přesný	k2eNgFnSc1d2
než	než	k8xS
definice	definice	k1gFnSc1
kontinentu	kontinent	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
češtině	čeština	k1gFnSc6
se	se	k3xPyFc4
tímto	tento	k3xDgInSc7
termínem	termín	k1gInSc7
obvykle	obvykle	k6eAd1
označuje	označovat	k5eAaImIp3nS
Indie	Indie	k1gFnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
indický	indický	k2eAgInSc4d1
subkontinent	subkontinent	k1gInSc4
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ostrovy	ostrov	k1gInPc1
jsou	být	k5eAaImIp3nP
zpravidla	zpravidla	k6eAd1
„	„	k?
<g/>
přiřazeny	přiřadit	k5eAaPmNgFnP
<g/>
“	“	k?
ke	k	k7c3
kontinentu	kontinent	k1gInSc3
<g/>
,	,	kIx,
kterému	který	k3yQgNnSc3,k3yIgNnSc3,k3yRgNnSc3
jsou	být	k5eAaImIp3nP
nejblíž	blízce	k6eAd3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Austrálie	Austrálie	k1gFnPc1
a	a	k8xC
ostrovy	ostrov	k1gInPc1
v	v	k7c6
Tichém	tichý	k2eAgInSc6d1
oceánu	oceán	k1gInSc6
jsou	být	k5eAaImIp3nP
někdy	někdy	k6eAd1
souhrnně	souhrnně	k6eAd1
označovány	označovat	k5eAaImNgInP
jako	jako	k8xS,k8xC
Australasie	Australasie	k1gFnPc1
nebo	nebo	k8xC
Oceánie	Oceánie	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
Systémy	systém	k1gInPc1
dělení	dělení	k1gNnSc2
kontinentů	kontinent	k1gInPc2
</s>
<s>
Kontinenty	kontinent	k1gInPc1
</s>
<s>
SvětadílyMapa	SvětadílyMapa	k1gFnSc1
světa	svět	k1gInSc2
s	s	k7c7
barevně	barevně	k6eAd1
odlišenými	odlišený	k2eAgInPc7d1
kontinenty	kontinent	k1gInPc7
</s>
<s>
7	#num#	k4
kontinentů	kontinent	k1gInPc2
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Antarktida	Antarktida	k1gFnSc1
</s>
<s>
Afrika	Afrika	k1gFnSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
</s>
<s>
Asie	Asie	k1gFnSc1
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
6	#num#	k4
kontinentů	kontinent	k1gInPc2
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Antarktida	Antarktida	k1gFnSc1
</s>
<s>
Afrika	Afrika	k1gFnSc1
</s>
<s>
Eurasie	Eurasie	k1gFnSc1
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
6	#num#	k4
kontinentů	kontinent	k1gInPc2
</s>
<s>
Amerika	Amerika	k1gFnSc1
</s>
<s>
Antarktida	Antarktida	k1gFnSc1
</s>
<s>
Afrika	Afrika	k1gFnSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
</s>
<s>
Asie	Asie	k1gFnSc1
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
5	#num#	k4
kontinentů	kontinent	k1gInPc2
</s>
<s>
Amerika	Amerika	k1gFnSc1
</s>
<s>
Antarktida	Antarktida	k1gFnSc1
</s>
<s>
Afrika	Afrika	k1gFnSc1
</s>
<s>
Eurasie	Eurasie	k1gFnSc1
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
4	#num#	k4
kontinenty	kontinent	k1gInPc5
</s>
<s>
Amerika	Amerika	k1gFnSc1
</s>
<s>
Antarktida	Antarktida	k1gFnSc1
</s>
<s>
Eurafrasie	Eurafrasie	k1gFnSc1
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
Rozdělení	rozdělení	k1gNnSc1
kontinentů	kontinent	k1gInPc2
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
také	také	k9
v	v	k7c6
čase	čas	k1gInSc6
díky	díky	k7c3
deskové	deskový	k2eAgFnSc3d1
tektonice	tektonika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
geologických	geologický	k2eAgInPc6d1
časových	časový	k2eAgInPc6d1
měřítcích	měřítek	k1gInPc6
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
přirozenému	přirozený	k2eAgNnSc3d1
rozdělování	rozdělování	k1gNnSc3
a	a	k8xC
spojování	spojování	k1gNnSc3
kontinentů	kontinent	k1gInPc2
přesunem	přesun	k1gInSc7
<g/>
,	,	kIx,
zlomy	zlom	k1gInPc7
a	a	k8xC
srážkami	srážka	k1gFnPc7
jednotlivých	jednotlivý	k2eAgFnPc2d1
pevninských	pevninský	k2eAgFnPc2d1
litosférických	litosférický	k2eAgFnPc2d1
desek	deska	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
části	část	k1gFnSc2
kontinentů	kontinent	k1gInPc2
dokonce	dokonce	k9
nově	nově	k6eAd1
vznikají	vznikat	k5eAaImIp3nP
z	z	k7c2
oceánského	oceánský	k2eAgNnSc2d1
dna	dno	k1gNnSc2
a	a	k8xC
některé	některý	k3yIgFnPc1
naopak	naopak	k6eAd1
subdukcí	subdukce	k1gFnSc7
kontinentálních	kontinentální	k2eAgFnPc2d1
desek	deska	k1gFnPc2
úplně	úplně	k6eAd1
zanikají	zanikat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Minimálně	minimálně	k6eAd1
dvakrát	dvakrát	k6eAd1
již	již	k6eAd1
byly	být	k5eAaImAgFnP
všechny	všechen	k3xTgFnPc1
(	(	kIx(
<g/>
nebo	nebo	k8xC
naprostá	naprostý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
<g/>
)	)	kIx)
části	část	k1gFnPc1
pevniny	pevnina	k1gFnSc2
spojené	spojený	k2eAgFnSc6d1
do	do	k7c2
tzv.	tzv.	kA
superkontinentů	superkontinent	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
jistou	jistý	k2eAgFnSc7d1
mírou	míra	k1gFnSc7
spolehlivosti	spolehlivost	k1gFnSc2
lze	lze	k6eAd1
tento	tento	k3xDgInSc4
vývoj	vývoj	k1gInSc4
podle	podle	k7c2
geologického	geologický	k2eAgInSc2d1
záznamu	záznam	k1gInSc2
vysledovat	vysledovat	k5eAaPmF,k5eAaImF
až	až	k9
k	k	k7c3
superkontinentu	superkontinent	k1gInSc3
zvanému	zvaný	k2eAgInSc3d1
Rodinie	Rodinie	k1gFnPc4
před	před	k7c7
cca	cca	kA
800	#num#	k4
až	až	k8xS
1100	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
,	,	kIx,
třebaže	třebaže	k8xS
jsou	být	k5eAaImIp3nP
i	i	k9
doklady	doklad	k1gInPc1
starších	starý	k2eAgFnPc2d2
hornin	hornina	k1gFnPc2
<g/>
,	,	kIx,
svědčících	svědčící	k2eAgFnPc2d1
o	o	k7c6
dřívějších	dřívější	k2eAgInPc6d1
kontinentech	kontinent	k1gInPc6
(	(	kIx(
<g/>
v	v	k7c6
jejich	jejich	k3xOp3gFnSc6
existenci	existence	k1gFnSc6
<g/>
,	,	kIx,
součástech	součást	k1gFnPc6
a	a	k8xC
poloze	poloha	k1gFnSc6
se	se	k3xPyFc4
však	však	k9
výsledky	výsledek	k1gInPc1
studií	studie	k1gFnPc2
rozcházejí	rozcházet	k5eAaImIp3nP
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c4
550	#num#	k4
až	až	k8xS
600	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
došlo	dojít	k5eAaPmAgNnS
také	také	k6eAd1
k	k	k7c3
významnému	významný	k2eAgNnSc3d1
přiblížení	přiblížení	k1gNnSc3
kontinentů	kontinent	k1gInPc2
vzniklých	vzniklý	k2eAgFnPc2d1
postupným	postupný	k2eAgInSc7d1
rozpadem	rozpad	k1gInSc7
Rodinie	Rodinie	k1gFnSc2
a	a	k8xC
možná	možná	k9
i	i	k9
k	k	k7c3
jejich	jejich	k3xOp3gNnSc3
dočasnému	dočasný	k2eAgNnSc3d1
spojení	spojení	k1gNnSc3
do	do	k7c2
superkontinentu	superkontinent	k1gInSc2
Pannotie	Pannotie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatím	zatím	k6eAd1
posledním	poslední	k2eAgMnSc7d1
superkontinentem	superkontinent	k1gMnSc7
<g/>
,	,	kIx,
mnohem	mnohem	k6eAd1
spolehlivěji	spolehlivě	k6eAd2
doloženým	doložený	k2eAgNnSc7d1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
před	před	k7c7
200	#num#	k4
až	až	k8xS
300	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
Pangea	Pangea	k1gFnSc1
<g/>
,	,	kIx,
z	z	k7c2
ní	on	k3xPp3gFnSc2
rozpadem	rozpad	k1gInSc7
vznikly	vzniknout	k5eAaPmAgInP
Laurasie	Laurasie	k1gFnSc2
(	(	kIx(
<g/>
dala	dát	k5eAaPmAgFnS
vznik	vznik	k1gInSc4
Severní	severní	k2eAgFnSc2d1
Americe	Amerika	k1gFnSc3
a	a	k8xC
velké	velký	k2eAgFnSc3d1
části	část	k1gFnSc3
Eurasie	Eurasie	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
Gondwana	Gondwana	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
níž	jenž	k3xRgFnSc2
pocházejí	pocházet	k5eAaImIp3nP
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
<g/>
,	,	kIx,
Afrika	Afrika	k1gFnSc1
<g/>
,	,	kIx,
Antarktida	Antarktida	k1gFnSc1
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc1
a	a	k8xC
Indie	Indie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Základní	základní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
o	o	k7c6
kontinentech	kontinent	k1gInPc6
</s>
<s>
Kontinent	kontinent	k1gInSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
(	(	kIx(
<g/>
km²	km²	k?
<g/>
)	)	kIx)
</s>
<s>
Populace	populace	k1gFnSc1
<g/>
(	(	kIx(
<g/>
odhad	odhad	k1gInSc1
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
%	%	kIx~
celkovésvětové	celkovésvětový	k2eAgFnSc2d1
populace	populace	k1gFnSc2
</s>
<s>
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
<g/>
(	(	kIx(
<g/>
ob.	ob.	k?
<g/>
/	/	kIx~
<g/>
km²	km²	k?
<g/>
)	)	kIx)
</s>
<s>
Asie	Asie	k1gFnSc1
</s>
<s>
44	#num#	k4
603	#num#	k4
8534	#num#	k4
522	#num#	k4
000	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
59,5	59,5	k4
%	%	kIx~
<g/>
101,4	101,4	k4
</s>
<s>
Amerika	Amerika	k1gFnSc1
</s>
<s>
42	#num#	k4
549	#num#	k4
0001	#num#	k4
002	#num#	k4
000	#num#	k4
00013	#num#	k4
%	%	kIx~
<g/>
23,5	23,5	k4
</s>
<s>
Afrika	Afrika	k1gFnSc1
</s>
<s>
31	#num#	k4
370	#num#	k4
0001	#num#	k4
276	#num#	k4
000	#num#	k4
00017	#num#	k4
%	%	kIx~
<g/>
40,7	40,7	k4
</s>
<s>
Antarktida	Antarktida	k1gFnSc1
</s>
<s>
13	#num#	k4
720	#num#	k4
0001	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0,00002	0,00002	k4
%	%	kIx~
<g/>
0,00007	0,00007	k4
</s>
<s>
Evropa	Evropa	k1gFnSc1
</s>
<s>
10	#num#	k4
180	#num#	k4
000740	#num#	k4
000	#num#	k4
00010	#num#	k4
%	%	kIx~
<g/>
72,7	72,7	k4
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
a	a	k8xC
Oceánie	Oceánie	k1gFnSc1
</s>
<s>
8	#num#	k4
500	#num#	k4
00041	#num#	k4
000	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0,5	0,5	k4
%	%	kIx~
<g/>
4,8	4,8	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
VILÍMEK	Vilímek	k1gMnSc1
<g/>
,	,	kIx,
Vít	Vít	k1gMnSc1
<g/>
;	;	kIx,
ET	ET	kA
AL	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zeměpisný	zeměpisný	k2eAgInSc1d1
slovníček	slovníček	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
NČGS	NČGS	kA
<g/>
,	,	kIx,
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
901942	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
55	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Geologie	geologie	k1gFnSc1
</s>
<s>
Desková	deskový	k2eAgFnSc1d1
tektonika	tektonika	k1gFnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
států	stát	k1gInPc2
světa	svět	k1gInSc2
podle	podle	k7c2
kontinentů	kontinent	k1gInPc2
</s>
<s>
Bájné	bájný	k2eAgInPc1d1
kontinenty	kontinent	k1gInPc1
a	a	k8xC
světy	svět	k1gInPc1
</s>
<s>
Atlantida	Atlantida	k1gFnSc1
</s>
<s>
Mu	on	k3xPp3gNnSc3
(	(	kIx(
<g/>
ztracený	ztracený	k2eAgInSc4d1
kontinent	kontinent	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Lemurie	Lemurie	k1gFnSc1
</s>
<s>
Kumari	Kumari	k6eAd1
Kandam	Kandam	k1gInSc1
</s>
<s>
První	první	k4xOgInPc1
kontinenty	kontinent	k1gInPc1
</s>
<s>
Pangea	Pangea	k1gFnSc1
</s>
<s>
Laurasie	Laurasie	k1gFnSc1
a	a	k8xC
Gondwana	Gondwana	k1gFnSc1
</s>
<s>
Ponořené	ponořený	k2eAgInPc1d1
kontinenty	kontinent	k1gInPc1
</s>
<s>
Zélandie	Zélandie	k1gFnSc1
</s>
<s>
Kergueleny	Kerguelen	k2eAgFnPc1d1
Plateau	Platea	k1gMnSc3
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
kontinent	kontinent	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
kontinent	kontinent	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Wikidata	Wikide	k1gNnPc1
používají	používat	k5eAaImIp3nP
Kontinent	kontinent	k1gInSc4
jako	jako	k8xC,k8xS
vlastnost	vlastnost	k1gFnSc4
P30	P30	k1gFnSc2
(	(	kIx(
<g/>
použití	použití	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Tektonické	tektonický	k2eAgFnSc2d1
desky	deska	k1gFnSc2
Velké	velký	k2eAgFnSc2d1
</s>
<s>
Africká	africký	k2eAgFnSc1d1
•	•	k?
Antarktická	antarktický	k2eAgFnSc1d1
•	•	k?
Australská	australský	k2eAgFnSc1d1
•	•	k?
Eurasijská	Eurasijský	k2eAgFnSc1d1
•	•	k?
Indická	indický	k2eAgFnSc1d1
•	•	k?
Jihoamerická	jihoamerický	k2eAgFnSc1d1
•	•	k?
Pacifická	pacifický	k2eAgFnSc1d1
•	•	k?
Severoamerická	severoamerický	k2eAgFnSc1d1
Malé	Malé	k2eAgFnSc1d1
</s>
<s>
Arabská	arabský	k2eAgFnSc1d1
•	•	k?
Filipínská	filipínský	k2eAgFnSc1d1
•	•	k?
Jan	Jan	k1gMnSc1
Mayen	Mayen	k2eAgMnSc1d1
•	•	k?
Juan	Juan	k1gMnSc1
de	de	k?
Fuca	Fuca	k1gFnSc1
•	•	k?
Karibská	karibský	k2eAgFnSc1d1
•	•	k?
Kokosová	kokosový	k2eAgFnSc1d1
•	•	k?
Nazca	Nazc	k2eAgFnSc1d1
•	•	k?
Scotia	Scotia	k1gFnSc1
</s>
<s>
Světadíly	světadíl	k1gInPc1
Jednotlivé	jednotlivý	k2eAgInPc1d1
kontinenty	kontinent	k1gInPc1
</s>
<s>
Afrika	Afrika	k1gFnSc1
•	•	k?
Antarktida	Antarktida	k1gFnSc1
•	•	k?
Asie	Asie	k1gFnSc2
•	•	k?
Austrálie	Austrálie	k1gFnSc2
•	•	k?
Evropa	Evropa	k1gFnSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
Superkontinenty	Superkontinenta	k1gFnSc2
</s>
<s>
Eurafrasie	Eurafrasie	k1gFnSc1
•	•	k?
Amerika	Amerika	k1gFnSc1
•	•	k?
Eurasie	Eurasie	k1gFnSc2
•	•	k?
Oceánie	Oceánie	k1gFnSc2
Geologické	geologický	k2eAgFnSc2d1
superkontinenty	superkontinenta	k1gFnSc2
</s>
<s>
Gondwana	Gondwana	k1gFnSc1
•	•	k?
Kenorland	Kenorland	k1gInSc1
•	•	k?
Kolumbie	Kolumbie	k1gFnSc2
•	•	k?
Laurasie	Laurasie	k1gFnSc2
•	•	k?
Pangea	Pangea	k1gFnSc1
•	•	k?
Pannotie	Pannotie	k1gFnSc1
•	•	k?
Protogondwana	Protogondwana	k1gFnSc1
•	•	k?
Protolaurasie	Protolaurasie	k1gFnSc2
•	•	k?
Rodinie	Rodinie	k1gFnSc2
•	•	k?
Vaalbara	Vaalbar	k1gMnSc2
Historické	historický	k2eAgInPc1d1
kontinenty	kontinent	k1gInPc1
</s>
<s>
Arktida	Arktida	k1gFnSc1
•	•	k?
Asiamerika	Asiamerik	k1gMnSc2
•	•	k?
Atlantika	Atlantik	k1gMnSc2
•	•	k?
Avalonie	Avalonie	k1gFnSc2
•	•	k?
Baltika	Baltika	k1gFnSc1
•	•	k?
Euramerika	Euramerika	k1gFnSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Čína	Čína	k1gFnSc1
•	•	k?
Kalahari	Kalahar	k1gFnSc2
•	•	k?
Kazachstánie	Kazachstánie	k1gFnSc2
•	•	k?
Kongo	Kongo	k1gNnSc1
•	•	k?
Laurentie	Laurentie	k1gFnSc2
•	•	k?
Severní	severní	k2eAgFnSc1d1
Čína	Čína	k1gFnSc1
•	•	k?
Sibiř	Sibiř	k1gFnSc1
•	•	k?
Ur	Ur	k1gInSc1
</s>
<s>
Pohyb	pohyb	k1gInSc1
světadílů	světadíl	k1gInPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnPc2
desek	deska	k1gFnPc2
</s>
<s>
1100	#num#	k4
<g/>
–	–	k?
<g/>
750	#num#	k4
<g/>
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
zpět	zpět	k6eAd1
<g/>
600	#num#	k4
<g/>
–	–	k?
<g/>
5502000	#num#	k4
</s>
<s>
Světadíly	světadíl	k1gInPc1
<g/>
:	:	kIx,
<g/>
↗	↗	k?
<g/>
Arábie	Arábie	k1gFnSc1
</s>
<s>
↗	↗	k?
<g/>
Madagaskar	Madagaskar	k1gInSc1
</s>
<s>
↗	↗	k?
<g/>
Indie	Indie	k1gFnSc1
</s>
<s>
↗	↗	k?
<g/>
Kongo	Kongo	k1gNnSc1
<g/>
↓	↓	k?
<g/>
↗	↗	k?
<g/>
Afrika	Afrika	k1gFnSc1
<g/>
→	→	k?
<g/>
Afrika	Afrika	k1gFnSc1
</s>
<s>
↗	↗	k?
<g/>
Patagonie	Patagonie	k1gFnSc1
<g/>
↓	↓	k?
<g/>
↗	↗	k?
<g/>
Sibiř	Sibiř	k1gFnSc1
<g/>
↓	↓	k?
<g/>
↗	↗	k?
<g/>
Atlantika	Atlantikum	k1gNnSc2
<g/>
→	→	k?
<g/>
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Atlantika	Atlantika	k1gFnSc1
<g/>
↘	↘	k?
<g/>
↗	↗	k?
<g/>
Západní	západní	k2eAgFnSc2d1
Arábie	Arábie	k1gFnSc2
<g/>
↓	↓	k?
<g/>
↗	↗	k?
<g/>
Baltika	Baltikum	k1gNnSc2
<g/>
↘	↘	k?
<g/>
↗	↗	k?
<g/>
Austrálie	Austrálie	k1gFnSc2
</s>
<s>
Ur	Ur	k1gInSc1
<g/>
→	→	k?
<g/>
Rodinie	Rodinie	k1gFnSc2
<g/>
→	→	k?
<g/>
Východní	východní	k2eAgFnSc1d1
Gondwana	Gondwana	k1gFnSc1
<g/>
→	→	k?
<g/>
Protogondwana	Protogondwana	k1gFnSc1
<g/>
→	→	k?
<g/>
Pannotie	Pannotie	k1gFnSc2
<g/>
→	→	k?
<g/>
Laurentie	Laurentie	k1gFnSc2
<g/>
→	→	k?
<g/>
Euramerika	Euramerika	k1gFnSc1
(	(	kIx(
<g/>
Laurussie	Laurussie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
→	→	k?
<g/>
Pangea	Pangea	k1gFnSc1
<g/>
→	→	k?
<g/>
Gondwana	Gondwana	k1gFnSc1
<g/>
→	→	k?
<g/>
Antarktida	Antarktida	k1gFnSc1
<g/>
→	→	k?
<g/>
Antarktida	Antarktida	k1gFnSc1
</s>
<s>
Arktida	Arktida	k1gFnSc1
<g/>
→	→	k?
<g/>
Nena	Nena	k1gFnSc1
<g/>
↗	↗	k?
<g/>
↘	↘	k?
<g/>
Západní	západní	k2eAgFnSc1d1
Gondwana	Gondwana	k1gFnSc1
<g/>
→	→	k?
<g/>
Protolaurasie	Protolaurasie	k1gFnSc2
<g/>
↗	↗	k?
<g/>
↘	↘	k?
<g/>
Gondwana	Gondwana	k1gFnSc1
<g/>
↗	↗	k?
<g/>
↘	↘	k?
<g/>
Laurasie	Laurasie	k1gFnSc2
<g/>
→	→	k?
<g/>
Laurentie	Laurentie	k1gFnSc2
<g/>
→	→	k?
<g/>
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Baltika	Baltika	k1gFnSc1
<g/>
↗	↗	k?
<g/>
↘	↘	k?
<g/>
Baltika	Baltikum	k1gNnSc2
<g/>
↗	↗	k?
<g/>
↓	↓	k?
<g/>
Avalonie	Avalonie	k1gFnSc2
<g/>
↘	↘	k?
<g/>
Eurasie	Eurasie	k1gFnSc2
</s>
<s>
↘	↘	k?
<g/>
Laurentie	Laurentie	k1gFnSc1
<g/>
↗	↗	k?
<g/>
↓	↓	k?
<g/>
Severní	severní	k2eAgFnSc1d1
Čína	Čína	k1gFnSc1
</s>
<s>
↘	↘	k?
<g/>
Sibiř	Sibiř	k1gFnSc1
<g/>
↗	↗	k?
<g/>
↓	↓	k?
<g/>
Jižní	jižní	k2eAgFnSc1d1
Čína	Čína	k1gFnSc1
</s>
<s>
Oceány	oceán	k1gInPc1
<g/>
:	:	kIx,
<g/>
MiroviaPrototethys	MiroviaPrototethys	k1gInSc1
<g/>
,	,	kIx,
PaleotethysPanthalassa	PaleotethysPanthalassa	k1gFnSc1
<g/>
↘	↘	k?
<g/>
Tethys	Tethys	k1gInSc1
</s>
<s>
svislé	svislý	k2eAgFnPc1d1
šipky	šipka	k1gFnPc1
<g/>
:	:	kIx,
rozdělení	rozdělení	k1gNnSc1
a	a	k8xC
spojení	spojení	k1gNnSc1
•	•	k?
vodorovné	vodorovný	k2eAgFnSc2d1
a	a	k8xC
šikmé	šikmý	k2eAgFnSc2d1
šipky	šipka	k1gFnSc2
<g/>
:	:	kIx,
postupné	postupný	k2eAgNnSc1d1
připojování	připojování	k1gNnSc1
a	a	k8xC
oddělování	oddělování	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4165153-4	4165153-4	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85031556	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85031556	#num#	k4
</s>
