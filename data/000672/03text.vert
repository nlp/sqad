<s>
Freddie	Freddie	k1gFnSc1	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
<g/>
,	,	kIx,	,
rodným	rodný	k2eAgNnSc7d1	rodné
jménem	jméno	k1gNnSc7	jméno
Farrokh	Farrokha	k1gFnPc2	Farrokha
Bulsara	Bulsar	k1gMnSc2	Bulsar
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
Stone	ston	k1gInSc5	ston
Town	Town	k1gInSc1	Town
<g/>
,	,	kIx,	,
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
-	-	kIx~	-
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
britský	britský	k2eAgMnSc1d1	britský
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
rockové	rockový	k2eAgFnSc2d1	rocková
skupiny	skupina	k1gFnSc2	skupina
Queen	Quena	k1gFnPc2	Quena
a	a	k8xC	a
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
zpěváků	zpěvák	k1gMnPc2	zpěvák
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
proslulý	proslulý	k2eAgMnSc1d1	proslulý
byl	být	k5eAaImAgInS	být
i	i	k9	i
svým	svůj	k3xOyFgNnSc7	svůj
charizmatem	charizma	k1gNnSc7	charizma
a	a	k8xC	a
extravagantností	extravagantnost	k1gFnSc7	extravagantnost
při	při	k7c6	při
vystupování	vystupování	k1gNnSc6	vystupování
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
smrt	smrt	k1gFnSc1	smrt
byla	být	k5eAaImAgFnS	být
zapříčiněna	zapříčinit	k5eAaPmNgFnS	zapříčinit
AIDS	AIDS	kA	AIDS
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k9	jako
Farrokh	Farrokh	k1gInSc1	Farrokh
Bulsara	Bulsar	k1gMnSc2	Bulsar
ve	v	k7c6	v
Stone	ston	k1gInSc5	ston
Town	Town	k1gInSc1	Town
na	na	k7c6	na
východoafrickém	východoafrický	k2eAgInSc6d1	východoafrický
ostrově	ostrov	k1gInSc6	ostrov
Zanzibar	Zanzibar	k1gInSc1	Zanzibar
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
Bomi	Bom	k1gFnSc2	Bom
a	a	k8xC	a
Jer	jer	k1gNnSc2	jer
byli	být	k5eAaImAgMnP	být
Pársové	Párs	k1gMnPc1	Párs
z	z	k7c2	z
indického	indický	k2eAgInSc2d1	indický
státu	stát	k1gInSc2	stát
Gudžarát	Gudžarát	k1gInSc1	Gudžarát
<g/>
;	;	kIx,	;
otec	otec	k1gMnSc1	otec
Bomi	Bomi	k1gNnSc4	Bomi
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
Zanzibaru	Zanzibar	k1gInSc6	Zanzibar
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
koloniální	koloniální	k2eAgFnSc6d1	koloniální
správě	správa	k1gFnSc6	správa
jako	jako	k8xC	jako
soudní	soudní	k2eAgMnSc1d1	soudní
úředník	úředník	k1gMnSc1	úředník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
začal	začít	k5eAaPmAgInS	začít
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
zanzibarskou	zanzibarský	k2eAgFnSc4d1	zanzibarská
misijní	misijní	k2eAgFnSc4d1	misijní
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
byl	být	k5eAaImAgInS	být
poslán	poslat	k5eAaPmNgMnS	poslat
do	do	k7c2	do
Bombaje	Bombaj	k1gFnSc2	Bombaj
(	(	kIx(	(
<g/>
Indie	Indie	k1gFnSc2	Indie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
internátní	internátní	k2eAgFnSc6d1	internátní
škole	škola	k1gFnSc6	škola
St.	st.	kA	st.
Peter	Peter	k1gMnSc1	Peter
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
v	v	k7c6	v
Panchgani	Panchgaň	k1gFnSc6	Panchgaň
<g/>
.	.	kIx.	.
</s>
<s>
Opustil	opustit	k5eAaPmAgMnS	opustit
tak	tak	k9	tak
svého	svůj	k3xOyFgMnSc4	svůj
otce	otec	k1gMnSc4	otec
<g/>
,	,	kIx,	,
mladší	mladý	k2eAgFnSc4d2	mladší
sestru	sestra	k1gFnSc4	sestra
Kashmiru	Kashmir	k1gInSc2	Kashmir
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
svou	svůj	k3xOyFgFnSc4	svůj
matku	matka	k1gFnSc4	matka
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
měl	mít	k5eAaImAgMnS	mít
velmi	velmi	k6eAd1	velmi
silný	silný	k2eAgInSc4d1	silný
vztah	vztah	k1gInSc4	vztah
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
St	St	kA	St
Peter	Peter	k1gMnSc1	Peter
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
dostalo	dostat	k5eAaPmAgNnS	dostat
klasické	klasický	k2eAgFnSc2d1	klasická
britské	britský	k2eAgFnSc2d1	britská
výchovy	výchova	k1gFnSc2	výchova
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
chodit	chodit	k5eAaImF	chodit
na	na	k7c4	na
hodiny	hodina	k1gFnPc4	hodina
klavíru	klavír	k1gInSc2	klavír
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
pozitivně	pozitivně	k6eAd1	pozitivně
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
jeho	jeho	k3xOp3gFnSc4	jeho
budoucí	budoucí	k2eAgFnSc4d1	budoucí
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
kamarády	kamarád	k1gMnPc7	kamarád
založil	založit	k5eAaPmAgMnS	založit
svoji	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
hudební	hudební	k2eAgFnSc4d1	hudební
skupinu	skupina	k1gFnSc4	skupina
-	-	kIx~	-
The	The	k1gFnSc4	The
Hectics	Hecticsa	k1gFnPc2	Hecticsa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c4	na
klavír	klavír	k1gInSc4	klavír
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
právě	právě	k9	právě
zde	zde	k6eAd1	zde
získal	získat	k5eAaPmAgMnS	získat
přezdívku	přezdívka	k1gFnSc4	přezdívka
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterou	který	k3yRgFnSc7	který
ho	on	k3xPp3gMnSc4	on
bude	být	k5eAaImBp3nS	být
znát	znát	k5eAaImF	znát
celý	celý	k2eAgInSc4d1	celý
svět	svět	k1gInSc4	svět
-	-	kIx~	-
Freddie	Freddie	k1gFnSc2	Freddie
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
líbila	líbit	k5eAaImAgFnS	líbit
jemu	on	k3xPp3gNnSc3	on
i	i	k9	i
rodičům	rodič	k1gMnPc3	rodič
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc1	jeho
skutečné	skutečný	k2eAgNnSc1d1	skutečné
jméno	jméno	k1gNnSc1	jméno
upadlo	upadnout	k5eAaPmAgNnS	upadnout
v	v	k7c6	v
zapomnění	zapomnění	k1gNnSc6	zapomnění
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1963	[number]	k4	1963
školu	škola	k1gFnSc4	škola
z	z	k7c2	z
neznámých	známý	k2eNgInPc2d1	neznámý
důvodů	důvod	k1gInPc2	důvod
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
domů	domů	k6eAd1	domů
a	a	k8xC	a
dokončil	dokončit	k5eAaPmAgMnS	dokončit
své	svůj	k3xOyFgNnSc4	svůj
vzdělání	vzdělání	k1gNnSc4	vzdělání
v	v	k7c6	v
klášterní	klášterní	k2eAgFnSc6d1	klášterní
škole	škola	k1gFnSc6	škola
St	St	kA	St
Joseph	Joseph	k1gInSc1	Joseph
<g/>
'	'	kIx"	'
<g/>
s.	s.	k?	s.
Po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
na	na	k7c6	na
Zanzibaru	Zanzibar	k1gInSc6	Zanzibar
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
nevedlo	vést	k5eNaImAgNnS	vést
-	-	kIx~	-
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
se	se	k3xPyFc4	se
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
předmětech	předmět	k1gInPc6	předmět
kromě	kromě	k7c2	kromě
jednoho	jeden	k4xCgInSc2	jeden
<g/>
:	:	kIx,	:
umění	umění	k1gNnPc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
rozhodnutí	rozhodnutí	k1gNnSc3	rozhodnutí
přihlásit	přihlásit	k5eAaPmF	přihlásit
se	se	k3xPyFc4	se
na	na	k7c4	na
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1966	[number]	k4	1966
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
na	na	k7c4	na
londýnskou	londýnský	k2eAgFnSc4d1	londýnská
Ealling	Ealling	k1gInSc1	Ealling
College	Colleg	k1gMnSc2	Colleg
of	of	k?	of
Art	Art	k1gMnSc2	Art
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
mu	on	k3xPp3gMnSc3	on
dvacet	dvacet	k4xCc1	dvacet
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
řádným	řádný	k2eAgMnSc7d1	řádný
studentem	student	k1gMnSc7	student
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
vše	všechen	k3xTgNnSc1	všechen
uprostřed	uprostřed	k7c2	uprostřed
rušných	rušný	k2eAgNnPc2d1	rušné
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
samozvanou	samozvaný	k2eAgFnSc7d1	samozvaná
Mekkou	Mekka	k1gFnSc7	Mekka
kultury	kultura	k1gFnSc2	kultura
pro	pro	k7c4	pro
mladé	mladé	k1gNnSc4	mladé
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
nemohl	moct	k5eNaImAgInS	moct
vybrat	vybrat	k5eAaPmF	vybrat
město	město	k1gNnSc4	město
více	hodně	k6eAd2	hodně
vzrušující	vzrušující	k2eAgFnPc1d1	vzrušující
<g/>
.	.	kIx.	.
</s>
<s>
Ealling	Ealling	k1gInSc1	Ealling
College	Colleg	k1gFnSc2	Colleg
of	of	k?	of
Art	Art	k1gFnSc1	Art
se	se	k3xPyFc4	se
ukazála	ukazát	k5eAaPmAgFnS	ukazát
být	být	k5eAaImF	být
dobrou	dobrý	k2eAgFnSc7d1	dobrá
volbou	volba	k1gFnSc7	volba
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
dřívější	dřívější	k2eAgMnPc4d1	dřívější
studenty	student	k1gMnPc4	student
patřili	patřit	k5eAaImAgMnP	patřit
Pete	Pet	k1gInPc4	Pet
Townshend	Townshenda	k1gFnPc2	Townshenda
z	z	k7c2	z
The	The	k1gFnSc2	The
Who	Who	k1gFnSc2	Who
i	i	k8xC	i
Ronnie	Ronnie	k1gFnSc2	Ronnie
Wood	Woodo	k1gNnPc2	Woodo
z	z	k7c2	z
Faces	Facesa	k1gFnPc2	Facesa
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgInSc1d2	pozdější
člen	člen	k1gInSc1	člen
Rolling	Rolling	k1gInSc1	Rolling
Stones	Stones	k1gInSc1	Stones
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
se	se	k3xPyFc4	se
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
s	s	k7c7	s
Timem	Tim	k1gMnSc7	Tim
Staffelem	Staffel	k1gMnSc7	Staffel
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
tráví	trávit	k5eAaImIp3nS	trávit
většinu	většina	k1gFnSc4	většina
svých	svůj	k3xOyFgNnPc2	svůj
studentských	studentský	k2eAgNnPc2d1	studentské
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Tim	Tim	k?	Tim
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
už	už	k6eAd1	už
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
poloprofesionální	poloprofesionální	k2eAgFnSc6d1	poloprofesionální
kapele	kapela	k1gFnSc6	kapela
s	s	k7c7	s
Brianem	Brian	k1gMnSc7	Brian
Mayem	May	k1gMnSc7	May
a	a	k8xC	a
Rogerem	Roger	k1gMnSc7	Roger
Taylorem	Taylor	k1gMnSc7	Taylor
<g/>
.	.	kIx.	.
</s>
<s>
Smile	smil	k1gInSc5	smil
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
kapela	kapela	k1gFnSc1	kapela
začala	začít	k5eAaPmAgFnS	začít
nazývat	nazývat	k5eAaImF	nazývat
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
kytaristou	kytarista	k1gMnSc7	kytarista
Brianem	Brian	k1gMnSc7	Brian
Mayem	May	k1gMnSc7	May
a	a	k8xC	a
s	s	k7c7	s
adeptem	adept	k1gMnSc7	adept
zubního	zubní	k2eAgNnSc2d1	zubní
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
,	,	kIx,	,
bubeníkem	bubeník	k1gMnSc7	bubeník
Rogerem	Roger	k1gMnSc7	Roger
Taylorem	Taylor	k1gMnSc7	Taylor
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
dva	dva	k4xCgMnPc1	dva
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
s	s	k7c7	s
Timem	Tim	k1gMnSc7	Tim
Staffelem	Staffel	k1gMnSc7	Staffel
<g/>
,	,	kIx,	,
založili	založit	k5eAaPmAgMnP	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
kapelu	kapela	k1gFnSc4	kapela
Smile	smil	k1gInSc5	smil
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
slibně	slibně	k6eAd1	slibně
rozjetém	rozjetý	k2eAgInSc6d1	rozjetý
začátku	začátek	k1gInSc6	začátek
však	však	k9	však
kapela	kapela	k1gFnSc1	kapela
začala	začít	k5eAaPmAgFnS	začít
trošku	trošku	k6eAd1	trošku
krachovat	krachovat	k5eAaBmF	krachovat
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
jim	on	k3xPp3gFnPc3	on
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
by	by	kYmCp3nS	by
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
nemohl	moct	k5eNaImAgMnS	moct
účinkovat	účinkovat	k5eAaImF	účinkovat
<g/>
,	,	kIx,	,
oni	onen	k3xDgMnPc1	onen
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
souhlasili	souhlasit	k5eAaImAgMnP	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Jedinému	jediný	k2eAgMnSc3d1	jediný
Timu	Tim	k1gMnSc3	Tim
Staffelovi	Staffel	k1gMnSc3	Staffel
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc1	ten
příliš	příliš	k6eAd1	příliš
nezamlouvalo	zamlouvat	k5eNaImAgNnS	zamlouvat
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
opustil	opustit	k5eAaPmAgMnS	opustit
kapelu	kapela	k1gFnSc4	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Roger	Roger	k1gMnSc1	Roger
a	a	k8xC	a
Brian	Brian	k1gMnSc1	Brian
navrhovali	navrhovat	k5eAaImAgMnP	navrhovat
jako	jako	k9	jako
název	název	k1gInSc4	název
kapely	kapela	k1gFnSc2	kapela
Grand	grand	k1gMnSc1	grand
Dance	Danka	k1gFnSc3	Danka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
on	on	k3xPp3gMnSc1	on
si	se	k3xPyFc3	se
prosadil	prosadit	k5eAaPmAgMnS	prosadit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
jmenovat	jmenovat	k5eAaBmF	jmenovat
Queen	Queen	k1gInSc4	Queen
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
malé	malý	k2eAgInPc4d1	malý
protesty	protest	k1gInPc4	protest
nakonec	nakonec	k6eAd1	nakonec
souhlasili	souhlasit	k5eAaImAgMnP	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Potřebovali	potřebovat	k5eAaImAgMnP	potřebovat
ještě	ještě	k9	ještě
jednoho	jeden	k4xCgMnSc4	jeden
člena	člen	k1gMnSc4	člen
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
po	po	k7c6	po
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
hledání	hledání	k1gNnSc6	hledání
baskytarista	baskytarista	k1gMnSc1	baskytarista
John	John	k1gMnSc1	John
Deacon	Deacon	k1gMnSc1	Deacon
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
první	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
Queen	Quena	k1gFnPc2	Quena
nezaznamenalo	zaznamenat	k5eNaPmAgNnS	zaznamenat
zdaleka	zdaleka	k6eAd1	zdaleka
úspěch	úspěch	k1gInSc4	úspěch
v	v	k7c6	v
měřítku	měřítko	k1gNnSc6	měřítko
s	s	k7c7	s
ostatními	ostatní	k2eAgFnPc7d1	ostatní
tehdejšími	tehdejší	k2eAgFnPc7d1	tehdejší
kapelami	kapela	k1gFnPc7	kapela
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
novou	nový	k2eAgFnSc4d1	nová
kapelu	kapela	k1gFnSc4	kapela
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
slibný	slibný	k2eAgInSc1d1	slibný
začátek	začátek	k1gInSc1	začátek
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
album	album	k1gNnSc1	album
Queen	Quena	k1gFnPc2	Quena
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c4	na
24	[number]	k4	24
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgFnSc2	druhý
LP	LP	kA	LP
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
úspěšnější	úspěšný	k2eAgNnSc1d2	úspěšnější
-	-	kIx~	-
stalo	stát	k5eAaPmAgNnS	stát
se	s	k7c7	s
5	[number]	k4	5
<g/>
.	.	kIx.	.
nejprodávanějším	prodávaný	k2eAgNnSc7d3	nejprodávanější
albem	album	k1gNnSc7	album
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
další	další	k2eAgNnSc1d1	další
album	album	k1gNnSc1	album
(	(	kIx(	(
<g/>
Sheer	Sheer	k1gInSc1	Sheer
Heart	Heart	k1gInSc1	Heart
Attack	Attack	k1gInSc1	Attack
-	-	kIx~	-
listopad	listopad	k1gInSc1	listopad
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
dokonce	dokonce	k9	dokonce
druhé	druhý	k4xOgFnPc1	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1975	[number]	k4	1975
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
LP	LP	kA	LP
A	A	kA	A
Night	Night	k1gMnSc1	Night
at	at	k?	at
the	the	k?	the
Opera	opera	k1gFnSc1	opera
se	s	k7c7	s
skladbou	skladba	k1gFnSc7	skladba
Bohemian	bohemian	k1gInSc1	bohemian
Rhapsody	Rhapsoda	k1gFnPc1	Rhapsoda
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgNnSc4	který
sbory	sbor	k1gInPc1	sbor
byly	být	k5eAaImAgInP	být
nahrány	nahrát	k5eAaPmNgInP	nahrát
postupným	postupný	k2eAgNnSc7d1	postupné
navršením	navršení	k1gNnSc7	navršení
180	[number]	k4	180
hlasů	hlas	k1gInPc2	hlas
<g/>
,	,	kIx,	,
použitá	použitý	k2eAgFnSc1d1	použitá
kytara	kytara	k1gFnSc1	kytara
potom	potom	k6eAd1	potom
působila	působit	k5eAaImAgFnS	působit
dojmem	dojem	k1gInSc7	dojem
orchestrálního	orchestrální	k2eAgInSc2d1	orchestrální
doprovodu	doprovod	k1gInSc2	doprovod
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
10	[number]	k4	10
dnů	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
singl	singl	k1gInSc1	singl
Bohemian	bohemian	k1gInSc4	bohemian
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
<g/>
,	,	kIx,	,
jejž	jenž	k3xRgInSc4	jenž
složil	složit	k5eAaPmAgMnS	složit
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgInS	dostat
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
britské	britský	k2eAgFnSc2d1	britská
hitparády	hitparáda	k1gFnSc2	hitparáda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
vedl	vést	k5eAaImAgMnS	vést
celých	celý	k2eAgNnPc2d1	celé
9	[number]	k4	9
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Podruhé	podruhé	k6eAd1	podruhé
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
výtěžek	výtěžek	k1gInSc1	výtěžek
z	z	k7c2	z
prodeje	prodej	k1gInSc2	prodej
byl	být	k5eAaImAgInS	být
věnován	věnovat	k5eAaPmNgInS	věnovat
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
léčení	léčení	k1gNnSc2	léčení
nemoci	nemoc	k1gFnSc2	nemoc
AIDS	AIDS	kA	AIDS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bohemian	bohemian	k1gInSc1	bohemian
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
velmi	velmi	k6eAd1	velmi
živá	živý	k2eAgFnSc1d1	živá
skladba	skladba	k1gFnSc1	skladba
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yQnSc6	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
prvenství	prvenství	k1gNnSc1	prvenství
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
anketách	anketa	k1gFnPc6	anketa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
si	se	k3xPyFc3	se
přestal	přestat	k5eAaPmAgInS	přestat
extravagantně	extravagantně	k6eAd1	extravagantně
lakovat	lakovat	k5eAaImF	lakovat
nehty	nehet	k1gInPc4	nehet
na	na	k7c4	na
černo	černo	k1gNnSc4	černo
<g/>
,	,	kIx,	,
oblékat	oblékat	k5eAaImF	oblékat
se	se	k3xPyFc4	se
do	do	k7c2	do
saténu	satén	k1gInSc2	satén
<g/>
,	,	kIx,	,
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
si	se	k3xPyFc3	se
narůst	narůst	k5eAaPmF	narůst
knírek	knírek	k1gInSc4	knírek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dál	daleko	k6eAd2	daleko
střídal	střídat	k5eAaImAgMnS	střídat
extravagantní	extravagantní	k2eAgNnSc4d1	extravagantní
oblečení	oblečení	k1gNnSc4	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1980	[number]	k4	1980
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
LP	LP	kA	LP
The	The	k1gFnSc1	The
Game	game	k1gInSc1	game
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nejprodávanějším	prodávaný	k2eAgNnSc7d3	nejprodávanější
albem	album	k1gNnSc7	album
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1985	[number]	k4	1985
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
dvacetiminutové	dvacetiminutový	k2eAgNnSc1d1	dvacetiminutové
vystoupení	vystoupení	k1gNnSc1	vystoupení
Queen	Quena	k1gFnPc2	Quena
na	na	k7c6	na
londýnském	londýnský	k2eAgInSc6d1	londýnský
koncertě	koncert	k1gInSc6	koncert
Live	Liv	k1gFnSc2	Liv
Aid	Aida	k1gFnPc2	Aida
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
přenášelo	přenášet	k5eAaImAgNnS	přenášet
12	[number]	k4	12
družic	družice	k1gFnPc2	družice
pro	pro	k7c4	pro
1,6	[number]	k4	1,6
miliardy	miliarda	k4xCgFnSc2	miliarda
televizních	televizní	k2eAgMnPc2d1	televizní
diváků	divák	k1gMnPc2	divák
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
i	i	k9	i
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gFnSc2	McCartnea
<g/>
,	,	kIx,	,
Eric	Eric	k1gFnSc1	Eric
Clapton	Clapton	k1gInSc1	Clapton
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Bowie	Bowie	k1gFnSc1	Bowie
<g/>
,	,	kIx,	,
Madonna	Madonna	k1gFnSc1	Madonna
<g/>
,	,	kIx,	,
U	u	k7c2	u
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Who	Who	k1gMnSc1	Who
<g/>
,	,	kIx,	,
Sting	Sting	k1gMnSc1	Sting
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
hvězdy	hvězda	k1gFnPc1	hvězda
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
hudebního	hudební	k2eAgNnSc2d1	hudební
nebe	nebe	k1gNnSc2	nebe
<g/>
.	.	kIx.	.
1984	[number]	k4	1984
vyšla	vyjít	k5eAaPmAgFnS	vyjít
na	na	k7c6	na
singlu	singl	k1gInSc6	singl
jeho	jeho	k3xOp3gFnSc1	jeho
první	první	k4xOgFnSc1	první
sólová	sólový	k2eAgFnSc1d1	sólová
nahrávka	nahrávka	k1gFnSc1	nahrávka
Love	lov	k1gInSc5	lov
Kills	Kills	k1gInSc4	Kills
a	a	k8xC	a
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
i	i	k9	i
první	první	k4xOgNnSc4	první
sólové	sólový	k2eAgNnSc4d1	sólové
debutové	debutový	k2eAgNnSc4d1	debutové
album	album	k1gNnSc4	album
Mr	Mr	k1gFnSc2	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bad	Bad	k?	Bad
Guy	Guy	k1gMnSc1	Guy
<g/>
.	.	kIx.	.
1987	[number]	k4	1987
vyšel	vyjít	k5eAaPmAgInS	vyjít
singl	singl	k1gInSc1	singl
se	s	k7c7	s
španělskou	španělský	k2eAgFnSc7d1	španělská
operní	operní	k2eAgFnSc7d1	operní
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Montserrat	Montserrat	k1gMnSc1	Montserrat
Caballé	Caball	k1gMnPc1	Caball
Barcelona	Barcelona	k1gFnSc1	Barcelona
a	a	k8xC	a
1988	[number]	k4	1988
pak	pak	k6eAd1	pak
i	i	k9	i
společné	společný	k2eAgNnSc4d1	společné
stejnojmenné	stejnojmenný	k2eAgNnSc4d1	stejnojmenné
album	album	k1gNnSc4	album
duetů	duet	k1gInPc2	duet
<g/>
,	,	kIx,	,
přinášející	přinášející	k2eAgInPc1d1	přinášející
spojení	spojení	k1gNnSc4	spojení
rocku	rock	k1gInSc2	rock
a	a	k8xC	a
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
veřejně	veřejně	k6eAd1	veřejně
spekulovat	spekulovat	k5eAaImF	spekulovat
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
špatném	špatný	k2eAgInSc6d1	špatný
zdravotním	zdravotní	k2eAgInSc6d1	zdravotní
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
fyzicky	fyzicky	k6eAd1	fyzicky
i	i	k9	i
psychicky	psychicky	k6eAd1	psychicky
unaven	unavit	k5eAaPmNgMnS	unavit
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
ještě	ještě	k9	ještě
natočil	natočit	k5eAaBmAgMnS	natočit
album	album	k1gNnSc4	album
Innuendo	Innuendo	k6eAd1	Innuendo
(	(	kIx(	(
<g/>
únor	únor	k1gInSc1	únor
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1991	[number]	k4	1991
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
trpí	trpět	k5eAaImIp3nP	trpět
AIDS	AIDS	kA	AIDS
a	a	k8xC	a
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1991	[number]	k4	1991
kolem	kolem	k7c2	kolem
19	[number]	k4	19
<g/>
.	.	kIx.	.
hodiny	hodina	k1gFnSc2	hodina
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
shodou	shoda	k1gFnSc7	shoda
okolností	okolnost	k1gFnPc2	okolnost
zemřel	zemřít	k5eAaPmAgMnS	zemřít
také	také	k9	také
bubeník	bubeník	k1gMnSc1	bubeník
skupiny	skupina	k1gFnSc2	skupina
Kiss	Kiss	k1gInSc1	Kiss
Eric	Eric	k1gInSc1	Eric
Carr	Carr	k1gInSc1	Carr
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
vyšel	vyjít	k5eAaPmAgInS	vyjít
singl	singl	k1gInSc1	singl
Bohemian	bohemian	k1gInSc4	bohemian
Rhapsody	Rhapsoda	k1gFnSc2	Rhapsoda
jako	jako	k9	jako
hold	hold	k1gInSc1	hold
zemřelému	zemřelý	k1gMnSc3	zemřelý
příteli	přítel	k1gMnSc3	přítel
a	a	k8xC	a
dostal	dostat	k5eAaPmAgInS	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
první	první	k4xOgFnSc4	první
příčku	příčka	k1gFnSc4	příčka
britské	britský	k2eAgFnSc2d1	britská
hitparády	hitparáda	k1gFnSc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1991	[number]	k4	1991
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
pohřeb	pohřeb	k1gInSc1	pohřeb
jen	jen	k9	jen
pro	pro	k7c4	pro
nejbližší	blízký	k2eAgMnPc4d3	nejbližší
přátele	přítel	k1gMnPc4	přítel
v	v	k7c6	v
krematoriu	krematorium	k1gNnSc6	krematorium
West	West	k1gMnSc1	West
London	London	k1gMnSc1	London
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1992	[number]	k4	1992
se	se	k3xPyFc4	se
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
ve	v	k7c4	v
Wembley	Wemble	k1gMnPc4	Wemble
konal	konat	k5eAaImAgInS	konat
ceremoniál	ceremoniál	k1gInSc1	ceremoniál
pro	pro	k7c4	pro
uctění	uctění	k1gNnSc4	uctění
jeho	jeho	k3xOp3gFnSc2	jeho
památky	památka	k1gFnSc2	památka
pro	pro	k7c4	pro
více	hodně	k6eAd2	hodně
než	než	k8xS	než
miliardu	miliarda	k4xCgFnSc4	miliarda
televizních	televizní	k2eAgMnPc2d1	televizní
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Zpopelněn	zpopelněn	k2eAgMnSc1d1	zpopelněn
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Kensal	Kensal	k1gFnSc6	Kensal
Green	Grena	k1gFnPc2	Grena
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
se	se	k3xPyFc4	se
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
rozptýlen	rozptýlit	k5eAaPmNgInS	rozptýlit
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
Ženevského	ženevský	k2eAgNnSc2d1	Ženevské
jezera	jezero	k1gNnSc2	jezero
ve	v	k7c6	v
švýcarském	švýcarský	k2eAgNnSc6d1	švýcarské
Montreux	Montreux	k1gInSc1	Montreux
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
vztyčena	vztyčen	k2eAgFnSc1d1	vztyčena
jeho	jeho	k3xOp3gFnSc1	jeho
třímetrová	třímetrový	k2eAgFnSc1d1	třímetrová
socha	socha	k1gFnSc1	socha
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
autorkou	autorka	k1gFnSc7	autorka
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
sochařka	sochařka	k1gFnSc1	sochařka
Irena	Irena	k1gFnSc1	Irena
Sedlecká	sedlecký	k2eAgFnSc1d1	Sedlecká
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
poslední	poslední	k2eAgNnSc1d1	poslední
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
čtveřice	čtveřice	k1gFnSc1	čtveřice
Made	Made	k1gNnSc1	Made
in	in	k?	in
Heaven	Heavna	k1gFnPc2	Heavna
s	s	k7c7	s
několika	několik	k4yIc7	několik
hlasovými	hlasový	k2eAgInPc7d1	hlasový
záznamy	záznam	k1gInPc7	záznam
natočenými	natočený	k2eAgInPc7d1	natočený
před	před	k7c7	před
jeho	jeho	k3xOp3gFnSc7	jeho
smrtí	smrt	k1gFnSc7	smrt
a	a	k8xC	a
předělávkami	předělávka	k1gFnPc7	předělávka
některých	některý	k3yIgFnPc2	některý
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
vydaných	vydaný	k2eAgFnPc2d1	vydaná
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nepůvodní	původní	k2eNgFnSc6d1	nepůvodní
sestavě	sestava	k1gFnSc6	sestava
pak	pak	k6eAd1	pak
Queen	Queen	k2eAgMnSc1d1	Queen
se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Paulem	Paul	k1gMnSc7	Paul
Rodgersem	Rodgers	k1gMnSc7	Rodgers
vyrazili	vyrazit	k5eAaPmAgMnP	vyrazit
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
už	už	k9	už
bez	bez	k7c2	bez
Freddieho	Freddie	k1gMnSc2	Freddie
a	a	k8xC	a
bez	bez	k7c2	bez
Johna	John	k1gMnSc2	John
Deacona	Deacon	k1gMnSc2	Deacon
<g/>
.	.	kIx.	.
</s>
<s>
Mr	Mr	k?	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bad	Bad	k?	Bad
Guy	Guy	k1gMnSc1	Guy
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Barcelona	Barcelona	k1gFnSc1	Barcelona
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
s	s	k7c7	s
Montserrat	Montserrat	k1gInSc1	Montserrat
Caballé	Caballý	k2eAgFnSc2d1	Caballý
The	The	k1gFnSc2	The
Album	album	k1gNnSc1	album
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
výběr	výběr	k1gInSc1	výběr
obsahující	obsahující	k2eAgInPc1d1	obsahující
remixy	remix	k1gInPc1	remix
některých	některý	k3yIgFnPc2	některý
skladeb	skladba	k1gFnPc2	skladba
The	The	k1gFnPc2	The
Great	Great	k2eAgInSc1d1	Great
Pretender	Pretender	k1gInSc1	Pretender
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
obdoba	obdoba	k1gFnSc1	obdoba
The	The	k1gFnSc2	The
Album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
jednu	jeden	k4xCgFnSc4	jeden
skladbu	skladba	k1gFnSc4	skladba
<g />
.	.	kIx.	.
</s>
<s>
navíc	navíc	k6eAd1	navíc
a	a	k8xC	a
odlišné	odlišný	k2eAgInPc1d1	odlišný
remixy	remix	k1gInPc1	remix
některých	některý	k3yIgFnPc2	některý
skladeb	skladba	k1gFnPc2	skladba
Freddie	Freddie	k1gFnSc2	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
The	The	k1gFnSc1	The
Solo	Solo	k1gMnSc1	Solo
Collection	Collection	k1gInSc1	Collection
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
box	box	k1gInSc1	box
obsahující	obsahující	k2eAgInSc1d1	obsahující
10	[number]	k4	10
CD	CD	kA	CD
s	s	k7c7	s
alby	album	k1gNnPc7	album
Mr	Mr	k1gMnSc2	Mr
<g/>
.	.	kIx.	.
<g/>
Bad	Bad	k1gMnSc1	Bad
Guy	Guy	k1gMnSc1	Guy
<g/>
,	,	kIx,	,
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Great	Great	k2eAgInSc4d1	Great
Pretender	Pretender	k1gInSc4	Pretender
<g/>
,	,	kIx,	,
všemi	všecek	k3xTgInPc7	všecek
vydanými	vydaný	k2eAgInPc7d1	vydaný
singly	singl	k1gInPc7	singl
<g/>
,	,	kIx,	,
několika	několik	k4yIc7	několik
demo	demo	k2eAgFnPc7d1	demo
nahrávkami	nahrávka	k1gFnPc7	nahrávka
a	a	k8xC	a
raritami	rarita	k1gFnPc7	rarita
Freddie	Freddie	k1gFnSc2	Freddie
Mercury	Mercur	k1gInPc4	Mercur
<g />
.	.	kIx.	.
</s>
<s>
Solo	Solo	k1gNnSc1	Solo
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
3	[number]	k4	3
CD	CD	kA	CD
obsahující	obsahující	k2eAgFnSc1d1	obsahující
alba	alba	k1gFnSc1	alba
Mr	Mr	k1gMnSc1	Mr
<g/>
.	.	kIx.	.
<g/>
Bad	Bad	k1gMnSc1	Bad
Guy	Guy	k1gMnSc1	Guy
<g/>
,	,	kIx,	,
Barcelona	Barcelona	k1gFnSc1	Barcelona
a	a	k8xC	a
třetí	třetí	k4xOgInSc4	třetí
bonusový	bonusový	k2eAgInSc4d1	bonusový
disk	disk	k1gInSc4	disk
se	s	k7c7	s
singly	singl	k1gInPc7	singl
a	a	k8xC	a
remixy	remix	k1gInPc1	remix
The	The	k1gMnSc2	The
Very	Vera	k1gMnSc2	Vera
Best	Best	k1gMnSc1	Best
Of	Of	k1gMnSc2	Of
Freddie	Freddie	k1gFnSc2	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
Solo	Solo	k1gMnSc1	Solo
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
výběr	výběr	k1gInSc1	výběr
největších	veliký	k2eAgInPc2d3	veliký
hitů	hit	k1gInPc2	hit
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgInSc4d1	obsahující
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
disku	disk	k1gInSc6	disk
nové	nový	k2eAgInPc4d1	nový
remixy	remix	k1gInPc4	remix
<g />
.	.	kIx.	.
</s>
<s>
některých	některý	k3yIgFnPc2	některý
skladeb	skladba	k1gFnPc2	skladba
Barcelona	Barcelona	k1gFnSc1	Barcelona
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
s	s	k7c7	s
Montserrat	Montserrat	k1gInSc1	Montserrat
Caballé	Caball	k1gMnPc1	Caball
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
natočený	natočený	k2eAgInSc1d1	natočený
hudební	hudební	k2eAgInSc1d1	hudební
doprovod	doprovod	k1gInSc1	doprovod
s	s	k7c7	s
Pražským	pražský	k2eAgInSc7d1	pražský
filharmonickým	filharmonický	k2eAgInSc7d1	filharmonický
orchestrem	orchestr	k1gInSc7	orchestr
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Freddie	Freddie	k1gFnSc2	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Freddie	Freddie	k1gFnSc2	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Queen	Queen	k2eAgInSc4d1	Queen
Archives	Archives	k1gInSc4	Archives
-	-	kIx~	-
interview	interview	k1gNnSc4	interview
s	s	k7c7	s
Freddiem	Freddium	k1gNnSc7	Freddium
Mercurym	Mercurym	k1gInSc1	Mercurym
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Voice	Voice	k1gFnSc1	Voice
<g/>
:	:	kIx,	:
Freddie	Freddie	k1gFnSc1	Freddie
Mercury	Mercura	k1gFnSc2	Mercura
-	-	kIx~	-
podrobné	podrobný	k2eAgInPc1d1	podrobný
detaily	detail	k1gInPc1	detail
o	o	k7c6	o
Mercuryho	Mercury	k1gMnSc2	Mercury
hlase	hlas	k1gInSc6	hlas
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
skupiny	skupina	k1gFnSc2	skupina
Queen	Quena	k1gFnPc2	Quena
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
Fan	Fana	k1gFnPc2	Fana
klub	klub	k1gInSc1	klub
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Český	český	k2eAgInSc1d1	český
web	web	k1gInSc1	web
a	a	k8xC	a
fan	fana	k1gFnPc2	fana
club	club	k1gInSc1	club
Queen	Queno	k1gNnPc2	Queno
</s>
