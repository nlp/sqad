<s>
Zajíc	Zajíc	k1gMnSc1	Zajíc
(	(	kIx(	(
<g/>
Lepus	Lepus	k1gMnSc1	Lepus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
savců	savec	k1gMnPc2	savec
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
zajícovití	zajícovitý	k2eAgMnPc1d1	zajícovitý
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
podobný	podobný	k2eAgMnSc1d1	podobný
králík	králík	k1gMnSc1	králík
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
býložravce	býložravec	k1gMnPc4	býložravec
schopné	schopný	k2eAgMnPc4d1	schopný
velmi	velmi	k6eAd1	velmi
rychlého	rychlý	k2eAgInSc2d1	rychlý
běhu	běh	k1gInSc2	běh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
žijící	žijící	k2eAgMnSc1d1	žijící
zajíc	zajíc	k1gMnSc1	zajíc
polní	polní	k2eAgMnSc1d1	polní
(	(	kIx(	(
<g/>
Lepus	Lepus	k1gMnSc1	Lepus
europeus	europeus	k1gMnSc1	europeus
<g/>
)	)	kIx)	)
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vyvinout	vyvinout	k5eAaPmF	vyvinout
rychlost	rychlost	k1gFnSc4	rychlost
až	až	k9	až
70	[number]	k4	70
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	kA	h
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zajíc	Zajíc	k1gMnSc1	Zajíc
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
zajíc	zajíc	k1gMnSc1	zajíc
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Taxon	taxon	k1gInSc1	taxon
Lepus	Lepus	k1gInSc1	Lepus
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
Zajíc	Zajíc	k1gMnSc1	Zajíc
na	na	k7c4	na
biolib	biolib	k1gInSc4	biolib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
