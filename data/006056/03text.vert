<s>
Vývar	vývar	k1gInSc1	vývar
(	(	kIx(	(
<g/>
též	též	k9	též
bujón	bujón	k1gInSc1	bujón
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kuchyňská	kuchyňský	k2eAgFnSc1d1	kuchyňská
surovina	surovina	k1gFnSc1	surovina
připravená	připravený	k2eAgFnSc1d1	připravená
vyvařením	vyvaření	k1gNnSc7	vyvaření
masa	maso	k1gNnSc2	maso
(	(	kIx(	(
<g/>
kostí	kost	k1gFnPc2	kost
<g/>
)	)	kIx)	)
či	či	k8xC	či
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
obojího	obojí	k4xRgMnSc2	obojí
<g/>
,	,	kIx,	,
používaná	používaný	k2eAgFnSc1d1	používaná
např.	např.	kA	např.
při	při	k7c6	při
vaření	vaření	k1gNnSc6	vaření
polévek	polévka	k1gFnPc2	polévka
a	a	k8xC	a
omáček	omáčka	k1gFnPc2	omáčka
<g/>
.	.	kIx.	.
</s>
<s>
Těmito	tento	k3xDgInPc7	tento
názvy	název	k1gInPc7	název
se	se	k3xPyFc4	se
také	také	k6eAd1	také
označují	označovat	k5eAaImIp3nP	označovat
polévky	polévka	k1gFnPc4	polévka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
vývar	vývar	k1gInSc1	vývar
tvoří	tvořit	k5eAaImIp3nS	tvořit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
surovinu	surovina	k1gFnSc4	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Dobrý	dobrý	k2eAgInSc4d1	dobrý
vývar	vývar	k1gInSc4	vývar
hospodyňky	hospodyňka	k1gFnPc1	hospodyňka
považovaly	považovat	k5eAaImAgFnP	považovat
a	a	k8xC	a
i	i	k9	i
dnes	dnes	k6eAd1	dnes
mnohdy	mnohdy	k6eAd1	mnohdy
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
vrchol	vrchol	k1gInSc4	vrchol
kuchařského	kuchařský	k2eAgNnSc2d1	kuchařské
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Vývar	vývar	k1gInSc1	vývar
byl	být	k5eAaImAgInS	být
též	též	k9	též
považován	považován	k2eAgMnSc1d1	považován
všeobecně	všeobecně	k6eAd1	všeobecně
jako	jako	k8xC	jako
posilující	posilující	k2eAgInSc1d1	posilující
nebo	nebo	k8xC	nebo
i	i	k9	i
léčivý	léčivý	k2eAgInSc4d1	léčivý
prostředek	prostředek	k1gInSc4	prostředek
<g/>
,	,	kIx,	,
maminkám	maminka	k1gFnPc3	maminka
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
dítě	dítě	k1gNnSc1	dítě
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nosil	nosit	k5eAaImAgInS	nosit
silný	silný	k2eAgInSc1d1	silný
slepičí	slepičí	k2eAgInSc1d1	slepičí
vývar	vývar	k1gInSc1	vývar
s	s	k7c7	s
masem	maso	k1gNnSc7	maso
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
brzy	brzy	k6eAd1	brzy
zesílily	zesílit	k5eAaPmAgInP	zesílit
<g/>
.	.	kIx.	.
</s>
<s>
Vývar	vývar	k1gInSc1	vývar
byl	být	k5eAaImAgInS	být
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
užíván	užíván	k2eAgInSc1d1	užíván
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
prostředků	prostředek	k1gInPc2	prostředek
na	na	k7c4	na
zmírnění	zmírnění	k1gNnSc4	zmírnění
kocoviny	kocovina	k1gFnSc2	kocovina
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
vyprošťovací	vyprošťovací	k2eAgFnSc1d1	vyprošťovací
polévka	polévka	k1gFnSc1	polévka
<g/>
)	)	kIx)	)
</s>
<s>
Pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
vývaru	vývar	k1gInSc2	vývar
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
části	část	k1gFnPc4	část
masa	maso	k1gNnSc2	maso
s	s	k7c7	s
kostí	kost	k1gFnSc7	kost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
hovězí	hovězí	k2eAgInPc4d1	hovězí
přední	přední	k2eAgInPc4d1	přední
<g/>
,	,	kIx,	,
kuřecí	kuřecí	k2eAgInPc4d1	kuřecí
hřbety	hřbet	k1gInPc4	hřbet
<g/>
,	,	kIx,	,
rybí	rybí	k2eAgFnPc4d1	rybí
hlavy	hlava	k1gFnPc4	hlava
ap.	ap.	kA	ap.
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
omezené	omezený	k2eAgNnSc1d1	omezené
použití	použití	k1gNnSc1	použití
pro	pro	k7c4	pro
přímou	přímý	k2eAgFnSc4d1	přímá
přípravu	příprava	k1gFnSc4	příprava
masného	masný	k2eAgNnSc2d1	masné
jídla	jídlo	k1gNnSc2	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Suroviny	surovina	k1gFnPc1	surovina
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
vkládají	vkládat	k5eAaImIp3nP	vkládat
do	do	k7c2	do
studené	studený	k2eAgFnSc2d1	studená
<g/>
,	,	kIx,	,
málo	málo	k6eAd1	málo
či	či	k8xC	či
vůbec	vůbec	k9	vůbec
solené	solený	k2eAgFnPc1d1	solená
vody	voda	k1gFnPc1	voda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
chuťové	chuťový	k2eAgFnPc1d1	chuťová
látky	látka	k1gFnPc1	látka
lépe	dobře	k6eAd2	dobře
vyvařily	vyvařit	k5eAaPmAgFnP	vyvařit
<g/>
.	.	kIx.	.
</s>
<s>
Kvalitní	kvalitní	k2eAgInSc1d1	kvalitní
masový	masový	k2eAgInSc1d1	masový
vývar	vývar	k1gInSc1	vývar
se	se	k3xPyFc4	se
vaří	vařit	k5eAaImIp3nS	vařit
i	i	k9	i
několik	několik	k4yIc1	několik
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
zelenina	zelenina	k1gFnSc1	zelenina
se	se	k3xPyFc4	se
přidává	přidávat	k5eAaImIp3nS	přidávat
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyvaření	vyvaření	k1gNnSc6	vyvaření
se	se	k3xPyFc4	se
suroviny	surovina	k1gFnPc1	surovina
vyjmou	vyjmout	k5eAaPmIp3nP	vyjmout
a	a	k8xC	a
vývar	vývar	k1gInSc4	vývar
přecedí	přecedit	k5eAaPmIp3nS	přecedit
<g/>
.	.	kIx.	.
</s>
<s>
Čistší	čistý	k2eAgInSc1d2	čistší
masový	masový	k2eAgInSc1d1	masový
vývar	vývar	k1gInSc1	vývar
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
cezením	cezení	k1gNnSc7	cezení
přes	přes	k7c4	přes
plátýnko	plátýnko	k1gNnSc4	plátýnko
(	(	kIx(	(
<g/>
gázu	gáz	k1gInSc2	gáz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
čištěním	čištění	k1gNnSc7	čištění
pomocí	pomocí	k7c2	pomocí
vaječného	vaječný	k2eAgInSc2d1	vaječný
bílku	bílek	k1gInSc2	bílek
<g/>
.	.	kIx.	.
</s>
<s>
Hotový	hotový	k2eAgInSc1d1	hotový
vývar	vývar	k1gInSc1	vývar
lze	lze	k6eAd1	lze
dobře	dobře	k6eAd1	dobře
konzervovat	konzervovat	k5eAaBmF	konzervovat
zamražením	zamražení	k1gNnSc7	zamražení
<g/>
.	.	kIx.	.
</s>
<s>
Vývarem	vývar	k1gInSc7	vývar
či	či	k8xC	či
bujónem	bujón	k1gInSc7	bujón
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
označuje	označovat	k5eAaImIp3nS	označovat
i	i	k9	i
kuchyňský	kuchyňský	k2eAgInSc4d1	kuchyňský
polotovar	polotovar	k1gInSc4	polotovar
v	v	k7c6	v
pevné	pevný	k2eAgFnSc6d1	pevná
či	či	k8xC	či
tekuté	tekutý	k2eAgFnSc6d1	tekutá
formě	forma	k1gFnSc6	forma
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
bílkovin	bílkovina	k1gFnPc2	bílkovina
rostlinného	rostlinný	k2eAgInSc2d1	rostlinný
nebo	nebo	k8xC	nebo
živočišného	živočišný	k2eAgInSc2d1	živočišný
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
zpracovávají	zpracovávat	k5eAaImIp3nP	zpracovávat
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
přes	přes	k7c4	přes
100	[number]	k4	100
°	°	k?	°
<g/>
C.	C.	kA	C.
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
procesu	proces	k1gInSc6	proces
se	se	k3xPyFc4	se
všechny	všechen	k3xTgFnPc1	všechen
bílkoviny	bílkovina	k1gFnPc1	bílkovina
rozštěpí	rozštěpit	k5eAaPmIp3nP	rozštěpit
na	na	k7c4	na
aminokyseliny	aminokyselina	k1gFnPc4	aminokyselina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
reakce	reakce	k1gFnSc2	reakce
se	se	k3xPyFc4	se
přebytek	přebytek	k1gInSc1	přebytek
kyseliny	kyselina	k1gFnSc2	kyselina
neutralizuje	neutralizovat	k5eAaBmIp3nS	neutralizovat
sodou	soda	k1gFnSc7	soda
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
polévkové	polévkový	k2eAgInPc4d1	polévkový
bujóny	bujón	k1gInPc4	bujón
značné	značný	k2eAgNnSc4d1	značné
množství	množství	k1gNnSc4	množství
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zvýraznění	zvýraznění	k1gNnSc4	zvýraznění
chutí	chuť	k1gFnPc2	chuť
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
výrobku	výrobek	k1gInSc6	výrobek
minimum	minimum	k1gNnSc1	minimum
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
používá	používat	k5eAaImIp3nS	používat
glutamát	glutamát	k1gInSc1	glutamát
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výrobek	výrobek	k1gInSc1	výrobek
se	se	k3xPyFc4	se
vhodí	vhodit	k5eAaPmIp3nS	vhodit
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
povaří	povařit	k5eAaPmIp3nS	povařit
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
přidají	přidat	k5eAaPmIp3nP	přidat
další	další	k2eAgFnPc1d1	další
ingredience	ingredience	k1gFnPc1	ingredience
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vývar	vývar	k1gInSc1	vývar
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Silný	silný	k2eAgInSc1d1	silný
domácí	domácí	k2eAgInSc1d1	domácí
kuřecí	kuřecí	k2eAgInSc1d1	kuřecí
vývar	vývar	k1gInSc1	vývar
jako	jako	k8xS	jako
od	od	k7c2	od
babičky	babička	k1gFnSc2	babička
táhněte	táhnout	k5eAaImRp2nP	táhnout
alespoň	alespoň	k9	alespoň
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
Zahoďte	zahodit	k5eAaPmRp2nP	zahodit
bujón	bujón	k1gInSc4	bujón
v	v	k7c6	v
kostce	kostka	k1gFnSc6	kostka
a	a	k8xC	a
připravte	připravit	k5eAaPmRp2nP	připravit
si	se	k3xPyFc3	se
vlastní	vlastní	k2eAgInSc4d1	vlastní
domácí	domácí	k2eAgInSc4d1	domácí
vývar	vývar	k1gInSc4	vývar
<g/>
!	!	kIx.	!
</s>
<s>
Polévka	polévka	k1gFnSc1	polévka
je	být	k5eAaImIp3nS	být
grunt	grunt	k1gInSc4	grunt
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
připravit	připravit	k5eAaPmF	připravit
consommé	consommý	k2eAgNnSc4d1	consommé
<g/>
?	?	kIx.	?
</s>
<s>
Kuřecí	kuřecí	k2eAgInSc1d1	kuřecí
vývar	vývar	k1gInSc1	vývar
s	s	k7c7	s
domácími	domácí	k2eAgFnPc7d1	domácí
nudlemi	nudle	k1gFnPc7	nudle
Peklo	péct	k5eAaImAgNnS	péct
na	na	k7c6	na
talíři	talíř	k1gInSc6	talíř
<g/>
:	:	kIx,	:
Kuřecí	kuřecí	k2eAgInPc1d1	kuřecí
bujóny	bujón	k1gInPc1	bujón
(	(	kIx(	(
<g/>
video	video	k1gNnSc1	video
<g/>
)	)	kIx)	)
1	[number]	k4	1
kuře	kuře	k1gNnSc1	kuře
na	na	k7c4	na
5	[number]	k4	5
tisíc	tisíc	k4xCgInSc4	tisíc
litrů	litr	k1gInPc2	litr
polévky	polévka	k1gFnSc2	polévka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
vaří	vařit	k5eAaImIp3nP	vařit
výrobci	výrobce	k1gMnPc1	výrobce
</s>
