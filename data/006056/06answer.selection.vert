<s>
Vývar	vývar	k1gInSc1	vývar
(	(	kIx(	(
<g/>
též	též	k9	též
bujón	bujón	k1gInSc1	bujón
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kuchyňská	kuchyňský	k2eAgFnSc1d1	kuchyňská
surovina	surovina	k1gFnSc1	surovina
připravená	připravený	k2eAgFnSc1d1	připravená
vyvařením	vyvaření	k1gNnSc7	vyvaření
masa	maso	k1gNnSc2	maso
(	(	kIx(	(
<g/>
kostí	kost	k1gFnPc2	kost
<g/>
)	)	kIx)	)
či	či	k8xC	či
zeleniny	zelenina	k1gFnSc2	zelenina
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
obojího	obojí	k4xRgMnSc2	obojí
<g/>
,	,	kIx,	,
používaná	používaný	k2eAgFnSc1d1	používaná
např.	např.	kA	např.
při	při	k7c6	při
vaření	vaření	k1gNnSc6	vaření
polévek	polévka	k1gFnPc2	polévka
a	a	k8xC	a
omáček	omáčka	k1gFnPc2	omáčka
<g/>
.	.	kIx.	.
</s>
