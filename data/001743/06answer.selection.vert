<s>
Cthulhu	Cthulha	k1gFnSc4	Cthulha
je	být	k5eAaImIp3nS	být
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
obrovská	obrovský	k2eAgFnSc1d1	obrovská
nestvůra	nestvůra	k1gFnSc1	nestvůra
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
bájných	bájný	k2eAgMnPc2d1	bájný
Prastarých	prastarý	k2eAgMnPc2d1	prastarý
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgFnSc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
dílech	díl	k1gInPc6	díl
H.	H.	kA	H.
P.	P.	kA	P.
Lovecrafta	Lovecrafta	k1gMnSc1	Lovecrafta
a	a	k8xC	a
některých	některý	k3yIgMnPc2	některý
jeho	jeho	k3xOp3gMnPc2	jeho
epigonů	epigon	k1gMnPc2	epigon
<g/>
.	.	kIx.	.
</s>
