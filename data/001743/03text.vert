<s>
Cthulhu	Cthulha	k1gFnSc4	Cthulha
je	být	k5eAaImIp3nS	být
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
obrovská	obrovský	k2eAgFnSc1d1	obrovská
nestvůra	nestvůra	k1gFnSc1	nestvůra
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
bájných	bájný	k2eAgMnPc2d1	bájný
Prastarých	prastarý	k2eAgMnPc2d1	prastarý
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgFnSc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
dílech	díl	k1gInPc6	díl
H.	H.	kA	H.
P.	P.	kA	P.
Lovecrafta	Lovecrafta	k1gMnSc1	Lovecrafta
a	a	k8xC	a
některých	některý	k3yIgMnPc2	některý
jeho	jeho	k3xOp3gMnPc2	jeho
epigonů	epigon	k1gMnPc2	epigon
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
zmiňována	zmiňován	k2eAgFnSc1d1	zmiňována
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
nepopsatelně	popsatelně	k6eNd1	popsatelně
děsivý	děsivý	k2eAgInSc4d1	děsivý
vzhled	vzhled	k1gInSc4	vzhled
<g/>
,	,	kIx,	,
obrovské	obrovský	k2eAgInPc4d1	obrovský
rozměry	rozměr	k1gInPc4	rozměr
a	a	k8xC	a
hrůzu	hrůza	k1gFnSc4	hrůza
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
budí	budit	k5eAaImIp3nP	budit
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
zmiňována	zmiňovat	k5eAaImNgFnS	zmiňovat
především	především	k6eAd1	především
v	v	k7c4	v
anglickojazyčné	anglickojazyčný	k2eAgFnPc4d1	anglickojazyčná
sci-fi	scii	k1gFnPc4	sci-fi
a	a	k8xC	a
fantasy	fantas	k1gInPc4	fantas
jako	jako	k8xC	jako
něco	něco	k3yInSc1	něco
extrémně	extrémně	k6eAd1	extrémně
děsivého	děsivý	k2eAgNnSc2d1	děsivé
či	či	k8xC	či
jako	jako	k9	jako
extrémní	extrémní	k2eAgNnSc4d1	extrémní
zlo	zlo	k1gNnSc4	zlo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dle	dle	k7c2	dle
Lovecrafta	Lovecraft	k1gInSc2	Lovecraft
"	"	kIx"	"
<g/>
nezní	znět	k5eNaImIp3nS	znět
jako	jako	k8xS	jako
nic	nic	k3yNnSc1	nic
z	z	k7c2	z
lidské	lidský	k2eAgFnSc2d1	lidská
řeči	řeč	k1gFnSc2	řeč
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
variant	varianta	k1gFnPc2	varianta
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
výslovnost	výslovnost	k1gFnSc1	výslovnost
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasná	jasný	k2eAgFnSc1d1	jasná
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
nejasnost	nejasnost	k1gFnSc1	nejasnost
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
autorským	autorský	k2eAgInSc7d1	autorský
záměrem	záměr	k1gInSc7	záměr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Lovecraft	Lovecraft	k1gMnSc1	Lovecraft
podal	podat	k5eAaPmAgMnS	podat
několik	několik	k4yIc4	několik
variant	varianta	k1gFnPc2	varianta
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
nejlogičtější	logický	k2eAgMnSc1d3	nejlogičtější
se	se	k3xPyFc4	se
jeví	jevit	k5eAaImIp3nS	jevit
anglický	anglický	k2eAgInSc1d1	anglický
přepis	přepis	k1gInSc1	přepis
Khlû	Khlû	k1gFnSc2	Khlû
<g/>
'	'	kIx"	'
<g/>
hloo	hloo	k6eAd1	hloo
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
nejspíš	nejspíš	k9	nejspíš
[	[	kIx(	[
<g/>
chlúl-hlú	chlúllú	k?	chlúl-hlú
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
komunitou	komunita	k1gFnSc7	komunita
fanoušků	fanoušek	k1gMnPc2	fanoušek
se	se	k3xPyFc4	se
ustálila	ustálit	k5eAaPmAgFnS	ustálit
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
kathúlú	kathúlú	k?	kathúlú
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
/	/	kIx~	/
<g/>
kə	kə	k?	kə
<g/>
/	/	kIx~	/
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
známá	známý	k2eAgFnSc1d1	známá
počítačová	počítačový	k2eAgFnSc1d1	počítačová
hra	hra	k1gFnSc1	hra
Call	Calla	k1gFnPc2	Calla
of	of	k?	of
Cthulhu	Cthulh	k1gInSc2	Cthulh
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
českými	český	k2eAgMnPc7d1	český
fanoušky	fanoušek	k1gMnPc7	fanoušek
však	však	k8xC	však
je	být	k5eAaImIp3nS	být
obvyklá	obvyklý	k2eAgFnSc1d1	obvyklá
verze	verze	k1gFnSc1	verze
výslovnosti	výslovnost	k1gFnSc2	výslovnost
kthulhu	kthulh	k1gInSc2	kthulh
<g/>
.	.	kIx.	.
</s>
<s>
Lovecraft	Lovecraft	k1gMnSc1	Lovecraft
sám	sám	k3xTgMnSc1	sám
označil	označit	k5eAaPmAgMnS	označit
jméno	jméno	k1gNnSc4	jméno
Cthulhu	Cthulh	k1gInSc2	Cthulh
za	za	k7c4	za
nevyslovitelné	vyslovitelný	k2eNgInPc1d1	nevyslovitelný
lidskou	lidský	k2eAgFnSc7d1	lidská
rasou	rasa	k1gFnSc7	rasa
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
jménu	jméno	k1gNnSc3	jméno
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
přidávána	přidáván	k2eAgNnPc4d1	přidáváno
epiteta	epiteton	k1gNnPc4	epiteton
jako	jako	k8xS	jako
Veliký	veliký	k2eAgInSc4d1	veliký
(	(	kIx(	(
<g/>
Great	Great	k1gInSc4	Great
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
(	(	kIx(	(
<g/>
Dead	Dead	k1gInSc1	Dead
<g/>
)	)	kIx)	)
či	či	k8xC	či
Děsivý	děsivý	k2eAgInSc1d1	děsivý
(	(	kIx(	(
<g/>
Dread	Dread	k1gInSc1	Dread
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
vyskytl	vyskytnout	k5eAaPmAgMnS	vyskytnout
v	v	k7c6	v
Lovecraftově	Lovecraftův	k2eAgFnSc6d1	Lovecraftova
povídce	povídka	k1gFnSc6	povídka
Volání	volání	k1gNnSc2	volání
Cthulhu	Cthulh	k1gInSc2	Cthulh
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Call	Calla	k1gFnPc2	Calla
of	of	k?	of
Cthulhu	Cthulh	k1gInSc2	Cthulh
<g/>
,	,	kIx,	,
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
zmiňována	zmiňovat	k5eAaImNgFnS	zmiňovat
i	i	k9	i
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
dílech	dílo	k1gNnPc6	dílo
téhož	týž	k3xTgMnSc2	týž
autora	autor	k1gMnSc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Nejnověji	nově	k6eAd3	nově
se	se	k3xPyFc4	se
Cthulhu	Cthulh	k1gInSc3	Cthulh
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
South	South	k1gMnSc1	South
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
v	v	k7c4	v
jedenácté	jedenáctý	k4xOgInPc4	jedenáctý
až	až	k9	až
třinácté	třináctý	k4xOgInPc4	třináctý
epizodě	epizoda	k1gFnSc6	epizoda
čtrnácté	čtrnáctý	k4xOgFnSc2	čtrnáctý
sezóny	sezóna	k1gFnSc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Magicka	Magicka	k1gFnSc1	Magicka
(	(	kIx(	(
<g/>
Magika	magika	k1gFnSc1	magika
nebo	nebo	k8xC	nebo
Medžika	Medžika	k1gFnSc1	Medžika
<g/>
)	)	kIx)	)
a	a	k8xC	a
Terraria	Terrarium	k1gNnSc2	Terrarium
<g/>
.	.	kIx.	.
</s>
<s>
Cthulhu	Cthulha	k1gFnSc4	Cthulha
je	být	k5eAaImIp3nS	být
zmiňován	zmiňován	k2eAgInSc1d1	zmiňován
jako	jako	k8xC	jako
nepopsatelný	popsatelný	k2eNgInSc1d1	nepopsatelný
<g/>
,	,	kIx,	,
určité	určitý	k2eAgInPc1d1	určitý
popisy	popis	k1gInPc1	popis
odkazují	odkazovat	k5eAaImIp3nP	odkazovat
především	především	k9	především
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
sochám	sochat	k5eAaImIp1nS	sochat
(	(	kIx(	(
<g/>
Cthulhu	Cthulha	k1gFnSc4	Cthulha
byl	být	k5eAaImAgInS	být
uctíván	uctívat	k5eAaImNgInS	uctívat
temnými	temný	k2eAgInPc7d1	temný
kulty	kult	k1gInPc7	kult
degenerovaných	degenerovaný	k2eAgInPc2d1	degenerovaný
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
odlehlých	odlehlý	k2eAgFnPc6d1	odlehlá
oblastech	oblast	k1gFnPc6	oblast
světa	svět	k1gInSc2	svět
-	-	kIx~	-
centrum	centrum	k1gNnSc1	centrum
kultu	kult	k1gInSc2	kult
je	být	k5eAaImIp3nS	být
kdesi	kdesi	k6eAd1	kdesi
v	v	k7c6	v
Arábii	Arábie	k1gFnSc6	Arábie
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nP	řídit
ho	on	k3xPp3gNnSc4	on
nesmrtelní	smrtelní	k2eNgMnPc1d1	nesmrtelní
vůdci	vůdce	k1gMnPc1	vůdce
z	z	k7c2	z
čínských	čínský	k2eAgFnPc2d1	čínská
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
odnože	odnož	k1gFnPc4	odnož
i	i	k9	i
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
a	a	k8xC	a
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
popsán	popsat	k5eAaPmNgInS	popsat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
současně	současně	k6eAd1	současně
drak	drak	k1gMnSc1	drak
<g/>
,	,	kIx,	,
hlavonožec	hlavonožec	k1gMnSc1	hlavonožec
a	a	k8xC	a
karikatura	karikatura	k1gFnSc1	karikatura
člověka	člověk	k1gMnSc2	člověk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zelený	zelený	k2eAgInSc1d1	zelený
<g/>
,	,	kIx,	,
vlhký	vlhký	k2eAgInSc1d1	vlhký
a	a	k8xC	a
okřídlený	okřídlený	k2eAgInSc1d1	okřídlený
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
stvořen	stvořit	k5eAaPmNgMnS	stvořit
z	z	k7c2	z
masa	maso	k1gNnSc2	maso
a	a	k8xC	a
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
velikostí	velikost	k1gFnPc2	velikost
údajně	údajně	k6eAd1	údajně
připomíná	připomínat	k5eAaImIp3nS	připomínat
horu	hora	k1gFnSc4	hora
<g/>
.	.	kIx.	.
</s>
<s>
Nechutným	chutný	k2eNgInPc3d1	nechutný
obřadům	obřad	k1gInPc3	obřad
jeho	jeho	k3xOp3gFnPc6	jeho
uctívání	uctívání	k1gNnSc6	uctívání
dominuje	dominovat	k5eAaImIp3nS	dominovat
mantra	mantra	k1gFnSc1	mantra
"	"	kIx"	"
<g/>
Ph	Ph	kA	Ph
<g/>
'	'	kIx"	'
<g/>
nglui	ngluit	k5eAaImRp2nS	ngluit
mglw	mglw	k?	mglw
<g/>
'	'	kIx"	'
<g/>
nafh	nafh	k1gInSc1	nafh
Cthulhu	Cthulh	k1gInSc2	Cthulh
R	R	kA	R
<g/>
'	'	kIx"	'
<g/>
lyeh	lyeh	k1gMnSc1	lyeh
wgah	wgah	k1gMnSc1	wgah
<g/>
'	'	kIx"	'
<g/>
nagl	nagl	k1gMnSc1	nagl
fhtagn	fhtagn	k1gMnSc1	fhtagn
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
údajně	údajně	k6eAd1	údajně
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
Ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
domě	dům	k1gInSc6	dům
v	v	k7c6	v
R	R	kA	R
<g/>
'	'	kIx"	'
<g/>
lyehu	lyeh	k1gInSc6	lyeh
mrtvý	mrtvý	k2eAgInSc1d1	mrtvý
Cthulhu	Cthulha	k1gFnSc4	Cthulha
čeká	čekat	k5eAaImIp3nS	čekat
a	a	k8xC	a
sní	snít	k5eAaImIp3nS	snít
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
zkracuje	zkracovat	k5eAaImIp3nS	zkracovat
na	na	k7c4	na
"	"	kIx"	"
<g/>
Cthulhu	Cthulha	k1gFnSc4	Cthulha
fhtagn	fhtagna	k1gFnPc2	fhtagna
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
vykládáno	vykládat	k5eAaImNgNnS	vykládat
na	na	k7c4	na
"	"	kIx"	"
<g/>
Cthulhu	Cthulha	k1gFnSc4	Cthulha
čeká	čekat	k5eAaImIp3nS	čekat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
R	R	kA	R
<g/>
'	'	kIx"	'
<g/>
lyeh	lyeha	k1gFnPc2	lyeha
je	být	k5eAaImIp3nS	být
obrovské	obrovský	k2eAgNnSc1d1	obrovské
<g/>
,	,	kIx,	,
nepřirozené	přirozený	k2eNgNnSc1d1	nepřirozené
město	město	k1gNnSc1	město
pod	pod	k7c7	pod
mořskou	mořský	k2eAgFnSc7d1	mořská
hladinou	hladina	k1gFnSc7	hladina
<g/>
,	,	kIx,	,
potopené	potopený	k2eAgInPc4d1	potopený
kdysi	kdysi	k6eAd1	kdysi
v	v	k7c6	v
jižním	jižní	k2eAgInSc6d1	jižní
Pacifiku	Pacifik	k1gInSc6	Pacifik
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
architektura	architektura	k1gFnSc1	architektura
je	být	k5eAaImIp3nS	být
groteskní	groteskní	k2eAgFnSc1d1	groteskní
<g/>
,	,	kIx,	,
zbudovaná	zbudovaný	k2eAgFnSc1d1	zbudovaná
podle	podle	k7c2	podle
principů	princip	k1gInPc2	princip
neeuklidovské	euklidovský	k2eNgFnSc2d1	neeuklidovská
geometrie	geometrie	k1gFnSc2	geometrie
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
náhodném	náhodný	k2eAgNnSc6d1	náhodné
objevení	objevení	k1gNnSc6	objevení
tvoří	tvořit	k5eAaImIp3nS	tvořit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
dějových	dějový	k2eAgFnPc2d1	dějová
linií	linie	k1gFnPc2	linie
povídky	povídka	k1gFnSc2	povídka
Volání	volání	k1gNnSc1	volání
Cthulhu	Cthulh	k1gInSc2	Cthulh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Cthulhu	Cthulh	k1gInSc2	Cthulh
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
