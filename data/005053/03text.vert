<s>
Trojská	trojský	k2eAgFnSc1d1	Trojská
válka	válka	k1gFnSc1	válka
je	být	k5eAaImIp3nS	být
řecká	řecký	k2eAgFnSc1d1	řecká
báje	báje	k1gFnSc1	báje
o	o	k7c6	o
dobývání	dobývání	k1gNnSc6	dobývání
města	město	k1gNnSc2	město
Tróje	Trója	k1gFnSc2	Trója
řeckými	řecký	k2eAgMnPc7d1	řecký
bojovníky	bojovník	k1gMnPc7	bojovník
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	on	k3xPp3gMnPc4	on
vylíčil	vylíčit	k5eAaPmAgMnS	vylíčit
básník	básník	k1gMnSc1	básník
Homér	Homér	k1gMnSc1	Homér
v	v	k7c6	v
eposu	epos	k1gInSc6	epos
Ilias	Ilias	k1gFnSc1	Ilias
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
válce	válka	k1gFnSc3	válka
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1200	[number]	k4	1200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
<s>
Trója	Trója	k1gFnSc1	Trója
je	být	k5eAaImIp3nS	být
legendární	legendární	k2eAgNnSc4d1	legendární
město	město	k1gNnSc4	město
ležící	ležící	k2eAgNnSc4d1	ležící
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
,	,	kIx,	,
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Dardanel	Dardanely	k1gFnPc2	Dardanely
pod	pod	k7c7	pod
horou	hora	k1gFnSc7	hora
Ida	ido	k1gNnSc2	ido
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
však	však	k9	však
spíše	spíše	k9	spíše
hledí	hledět	k5eAaImIp3nS	hledět
jako	jako	k9	jako
na	na	k7c4	na
archeologickou	archeologický	k2eAgFnSc4d1	archeologická
lokalitu	lokalita	k1gFnSc4	lokalita
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
považovanou	považovaný	k2eAgFnSc7d1	považovaná
za	za	k7c4	za
dějiště	dějiště	k1gNnSc4	dějiště
tzv.	tzv.	kA	tzv.
trojské	trojský	k2eAgFnSc2d1	Trojská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Téma	téma	k1gNnSc1	téma
trojské	trojský	k2eAgFnSc2d1	Trojská
války	válka	k1gFnSc2	válka
je	být	k5eAaImIp3nS	být
až	až	k9	až
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
zastřené	zastřený	k2eAgFnSc2d1	zastřená
velikým	veliký	k2eAgNnSc7d1	veliké
tajemstvím	tajemství	k1gNnSc7	tajemství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
vedou	vést	k5eAaImIp3nP	vést
spory	spor	k1gInPc1	spor
nejen	nejen	k6eAd1	nejen
o	o	k7c4	o
její	její	k3xOp3gNnSc4	její
datování	datování	k1gNnSc4	datování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokonce	dokonce	k9	dokonce
i	i	k9	i
o	o	k7c4	o
její	její	k3xOp3gFnSc4	její
existenci	existence	k1gFnSc4	existence
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarším	starý	k2eAgInSc7d3	nejstarší
zdrojem	zdroj	k1gInSc7	zdroj
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
události	událost	k1gFnSc6	událost
je	být	k5eAaImIp3nS	být
Ilias	Ilias	k1gFnSc1	Ilias
a	a	k8xC	a
Odyssea	Odyssea	k1gFnSc1	Odyssea
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
dílo	dílo	k1gNnSc1	dílo
slepého	slepý	k2eAgMnSc2d1	slepý
řeckého	řecký	k2eAgMnSc2d1	řecký
básníka	básník	k1gMnSc2	básník
Homéra	Homér	k1gMnSc2	Homér
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
žil	žít	k5eAaImAgMnS	žít
někdy	někdy	k6eAd1	někdy
kolem	kolem	k7c2	kolem
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
tento	tento	k3xDgMnSc1	tento
básník	básník	k1gMnSc1	básník
opravdu	opravdu	k6eAd1	opravdu
žil	žít	k5eAaImAgMnS	žít
a	a	k8xC	a
jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
opravdu	opravdu	k6eAd1	opravdu
autorem	autor	k1gMnSc7	autor
tohoto	tento	k3xDgInSc2	tento
epického	epický	k2eAgInSc2d1	epický
skvostu	skvost	k1gInSc2	skvost
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
vedou	vést	k5eAaImIp3nP	vést
spory	spor	k1gInPc1	spor
už	už	k6eAd1	už
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
dílo	dílo	k1gNnSc4	dílo
potom	potom	k6eAd1	potom
přepracoval	přepracovat	k5eAaPmAgMnS	přepracovat
a	a	k8xC	a
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
římský	římský	k2eAgMnSc1d1	římský
básník	básník	k1gMnSc1	básník
Vergilius	Vergilius	k1gMnSc1	Vergilius
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
Aeneis	Aeneis	k1gFnSc1	Aeneis
a	a	k8xC	a
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgMnPc2d1	další
autorů	autor	k1gMnPc2	autor
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
lákaly	lákat	k5eAaImAgFnP	lákat
dálky	dálka	k1gFnPc1	dálka
dávno	dávno	k6eAd1	dávno
zaprášených	zaprášený	k2eAgFnPc2d1	zaprášená
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
Tróji	Trója	k1gFnSc6	Trója
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
i	i	k9	i
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
chetitských	chetitský	k2eAgInPc6d1	chetitský
textech	text	k1gInPc6	text
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
objevil	objevit	k5eAaPmAgMnS	objevit
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
badatel	badatel	k1gMnSc1	badatel
Emil	Emil	k1gMnSc1	Emil
Forrer	Forrer	k1gMnSc1	Forrer
spojitosti	spojitost	k1gFnSc3	spojitost
mezi	mezi	k7c7	mezi
místními	místní	k2eAgInPc7d1	místní
názvy	název	k1gInPc7	název
Wilusa	Wilus	k1gMnSc2	Wilus
a	a	k8xC	a
Ilium	Ilium	k1gInSc4	Ilium
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jmény	jméno	k1gNnPc7	jméno
Alaksandus	Alaksandus	k1gInSc1	Alaksandus
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Wilusy	Wilus	k1gInPc4	Wilus
<g/>
,	,	kIx,	,
a	a	k8xC	a
Alexandros	Alexandrosa	k1gFnPc2	Alexandrosa
(	(	kIx(	(
<g/>
Paris	Paris	k1gMnSc1	Paris
<g/>
)	)	kIx)	)
z	z	k7c2	z
Tróje	Trója	k1gFnSc2	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
domněnky	domněnka	k1gFnPc4	domněnka
většina	většina	k1gFnSc1	většina
badatelů	badatel	k1gMnPc2	badatel
zpochybnila	zpochybnit	k5eAaPmAgFnS	zpochybnit
či	či	k8xC	či
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
za	za	k7c4	za
neprokazatelné	prokazatelný	k2eNgNnSc4d1	neprokazatelné
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nové	nový	k2eAgInPc4d1	nový
objevy	objev	k1gInPc4	objev
jim	on	k3xPp3gInPc3	on
dnes	dnes	k6eAd1	dnes
přikládají	přikládat	k5eAaImIp3nP	přikládat
už	už	k6eAd1	už
větší	veliký	k2eAgFnSc4d2	veliký
váhu	váha	k1gFnSc4	váha
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jednoho	jeden	k4xCgInSc2	jeden
příběhu	příběh	k1gInSc2	příběh
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
města	město	k1gNnSc2	město
přišli	přijít	k5eAaPmAgMnP	přijít
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
Kréťané	Kréťan	k1gMnPc1	Kréťan
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
královského	královský	k2eAgMnSc2d1	královský
syna	syn	k1gMnSc2	syn
Skamandra	Skamandr	k1gMnSc2	Skamandr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
potomci	potomek	k1gMnPc1	potomek
pak	pak	k6eAd1	pak
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
území	území	k1gNnSc6	území
zakládali	zakládat	k5eAaImAgMnP	zakládat
nové	nový	k2eAgFnPc4d1	nová
vesnice	vesnice	k1gFnPc4	vesnice
a	a	k8xC	a
chrámy	chrám	k1gInPc4	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
město	město	k1gNnSc1	město
Trója	Trója	k1gFnSc1	Trója
nechal	nechat	k5eAaPmAgInS	nechat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vystavět	vystavět	k5eAaPmF	vystavět
jistý	jistý	k2eAgMnSc1d1	jistý
Illos	Illos	k1gMnSc1	Illos
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Tyrhénského	tyrhénský	k2eAgMnSc2d1	tyrhénský
Dardana	Dardan	k1gMnSc2	Dardan
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
něho	on	k3xPp3gNnSc2	on
se	se	k3xPyFc4	se
také	také	k9	také
město	město	k1gNnSc1	město
nazývalo	nazývat	k5eAaImAgNnS	nazývat
Ilium	Ilium	k1gInSc4	Ilium
či	či	k8xC	či
Ílion	Ílion	k1gInSc4	Ílion
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jisté	jistý	k2eAgNnSc1d1	jisté
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
trójský	trójský	k2eAgInSc1d1	trójský
pahorek	pahorek	k1gInSc1	pahorek
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
zvaný	zvaný	k2eAgMnSc1d1	zvaný
Hissarlik	Hissarlik	k1gMnSc1	Hissarlik
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
obydlen	obydlet	k5eAaPmNgInS	obydlet
už	už	k6eAd1	už
někdy	někdy	k6eAd1	někdy
ve	v	k7c6	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Město	město	k1gNnSc1	město
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
prošlo	projít	k5eAaPmAgNnS	projít
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
vývojem	vývoj	k1gInSc7	vývoj
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
dnešní	dnešní	k2eAgFnPc1d1	dnešní
archeologické	archeologický	k2eAgFnPc1d1	archeologická
vykopávky	vykopávka	k1gFnPc1	vykopávka
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
nejstarší	starý	k2eAgNnSc1d3	nejstarší
město	město	k1gNnSc1	město
mělo	mít	k5eAaImAgNnS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
bylo	být	k5eAaImAgNnS	být
obehnáno	obehnat	k5eAaPmNgNnS	obehnat
hradbami	hradba	k1gFnPc7	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vrstva	vrstva	k1gFnSc1	vrstva
již	již	k6eAd1	již
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
dokonce	dokonce	k9	dokonce
znalost	znalost	k1gFnSc1	znalost
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
domy	dům	k1gInPc1	dům
byly	být	k5eAaImAgInP	být
budovány	budovat	k5eAaImNgInP	budovat
na	na	k7c6	na
kamenné	kamenný	k2eAgFnSc6d1	kamenná
podezdívce	podezdívka	k1gFnSc6	podezdívka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozkvětu	rozkvět	k1gInSc3	rozkvět
obchodu	obchod	k1gInSc2	obchod
zde	zde	k6eAd1	zde
došlo	dojít	k5eAaPmAgNnS	dojít
díky	díky	k7c3	díky
výhodné	výhodný	k2eAgFnSc3d1	výhodná
pozici	pozice	k1gFnSc3	pozice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožňovala	umožňovat	k5eAaImAgFnS	umožňovat
dohled	dohled	k1gInSc4	dohled
nad	nad	k7c7	nad
úžinou	úžina	k1gFnSc7	úžina
Dardanely	Dardanely	k1gFnPc1	Dardanely
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
musela	muset	k5eAaImAgFnS	muset
projet	projet	k5eAaPmF	projet
každá	každý	k3xTgFnSc1	každý
obchodní	obchodní	k2eAgFnSc1d1	obchodní
loď	loď	k1gFnSc1	loď
směřující	směřující	k2eAgFnSc1d1	směřující
do	do	k7c2	do
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
pak	pak	k6eAd1	pak
navazuje	navazovat	k5eAaImIp3nS	navazovat
další	další	k2eAgFnSc1d1	další
vrstva	vrstva	k1gFnSc1	vrstva
z	z	k7c2	z
let	léto	k1gNnPc2	léto
2400	[number]	k4	2400
<g/>
-	-	kIx~	-
<g/>
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
hradby	hradba	k1gFnPc1	hradba
byly	být	k5eAaImAgFnP	být
postaveny	postavit	k5eAaPmNgFnP	postavit
z	z	k7c2	z
kamení	kamení	k1gNnSc2	kamení
a	a	k8xC	a
cihel	cihla	k1gFnPc2	cihla
<g/>
.	.	kIx.	.
</s>
<s>
Nalezená	nalezený	k2eAgFnSc1d1	nalezená
keramika	keramika	k1gFnSc1	keramika
již	již	k6eAd1	již
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
znalost	znalost	k1gFnSc1	znalost
hrnčířského	hrnčířský	k2eAgInSc2d1	hrnčířský
kruhu	kruh	k1gInSc2	kruh
a	a	k8xC	a
vypalovacích	vypalovací	k2eAgFnPc2d1	vypalovací
pecí	pec	k1gFnPc2	pec
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
vrstva	vrstva	k1gFnSc1	vrstva
byla	být	k5eAaImAgFnS	být
Schliemannem	Schliemanno	k1gNnSc7	Schliemanno
mylně	mylně	k6eAd1	mylně
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
Homérovu	Homérův	k2eAgFnSc4d1	Homérova
Tróju	Trója	k1gFnSc4	Trója
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
bohaté	bohatý	k2eAgInPc1d1	bohatý
nálezy	nález	k1gInPc1	nález
nazvány	nazván	k2eAgInPc1d1	nazván
"	"	kIx"	"
<g/>
Priamovým	Priamův	k2eAgInSc7d1	Priamův
pokladem	poklad	k1gInSc7	poklad
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
omyl	omyl	k1gInSc1	omyl
však	však	k9	však
prokázaly	prokázat	k5eAaPmAgInP	prokázat
pozdější	pozdní	k2eAgInPc1d2	pozdější
archeologické	archeologický	k2eAgInPc1d1	archeologický
výzkumy	výzkum	k1gInPc1	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
byla	být	k5eAaImAgFnS	být
Trója	Trója	k1gFnSc1	Trója
II	II	kA	II
<g/>
.	.	kIx.	.
vypálena	vypálen	k2eAgFnSc1d1	vypálena
<g/>
,	,	kIx,	,
snad	snad	k9	snad
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
stěhováním	stěhování	k1gNnSc7	stěhování
maloasijských	maloasijský	k2eAgInPc2d1	maloasijský
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
spáleništi	spáleniště	k1gNnSc6	spáleniště
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
vrstvy	vrstva	k1gFnPc1	vrstva
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
V.	V.	kA	V.
<g/>
,	,	kIx,	,
neopevněné	opevněný	k2eNgFnPc4d1	neopevněná
a	a	k8xC	a
mnohem	mnohem	k6eAd1	mnohem
chudší	chudý	k2eAgInPc1d2	chudší
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
nálezy	nález	k1gInPc1	nález
prozrazují	prozrazovat	k5eAaImIp3nP	prozrazovat
styk	styk	k1gInSc4	styk
s	s	k7c7	s
řeckou	řecký	k2eAgFnSc7d1	řecká
pevninou	pevnina	k1gFnSc7	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Vrstva	vrstva	k1gFnSc1	vrstva
VI	VI	kA	VI
<g/>
.	.	kIx.	.
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
hradby	hradba	k1gFnPc4	hradba
podle	podle	k7c2	podle
mykénského	mykénský	k2eAgInSc2d1	mykénský
vzoru	vzor	k1gInSc2	vzor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
památek	památka	k1gFnPc2	památka
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
velmi	velmi	k6eAd1	velmi
málo	málo	k4c1	málo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byly	být	k5eAaImAgFnP	být
zničeny	zničit	k5eAaPmNgFnP	zničit
při	při	k7c6	při
pozdějším	pozdní	k2eAgNnSc6d2	pozdější
přestavování	přestavování	k1gNnSc6	přestavování
<g/>
.	.	kIx.	.
</s>
<s>
Nebyly	být	k5eNaImAgInP	být
zde	zde	k6eAd1	zde
nalezeny	nalezen	k2eAgInPc1d1	nalezen
ani	ani	k8xC	ani
žádné	žádný	k3yNgInPc1	žádný
tělesné	tělesný	k2eAgInPc1d1	tělesný
ostatky	ostatek	k1gInPc1	ostatek
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
zničeno	zničit	k5eAaPmNgNnS	zničit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1300	[number]	k4	1300
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zemětřesením	zemětřesení	k1gNnSc7	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Vrstva	vrstva	k1gFnSc1	vrstva
zvaná	zvaný	k2eAgFnSc1d1	zvaná
VIIa	VIIa	k1gFnSc1	VIIa
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
základě	základ	k1gInSc6	základ
stylu	styl	k1gInSc2	styl
keramiky	keramika	k1gFnSc2	keramika
zasazena	zasadit	k5eAaPmNgFnS	zasadit
do	do	k7c2	do
období	období	k1gNnSc2	období
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nejpravděpodobnějším	pravděpodobný	k2eAgMnSc7d3	nejpravděpodobnější
kandidátem	kandidát	k1gMnSc7	kandidát
na	na	k7c4	na
Homérskou	homérský	k2eAgFnSc4d1	homérská
Tróju	Trója	k1gFnSc4	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
také	také	k9	také
určité	určitý	k2eAgInPc4d1	určitý
znaky	znak	k1gInPc4	znak
válečného	válečný	k2eAgNnSc2d1	válečné
dobývání	dobývání	k1gNnSc2	dobývání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
vrstvou	vrstva	k1gFnSc7	vrstva
popela	popel	k1gInSc2	popel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
korespondovat	korespondovat	k5eAaImF	korespondovat
s	s	k7c7	s
řáděním	řádění	k1gNnSc7	řádění
achájských	achájský	k2eAgMnPc2d1	achájský
bojovníků	bojovník	k1gMnPc2	bojovník
po	po	k7c4	po
dobytí	dobytí	k1gNnSc4	dobytí
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
však	však	k9	však
byly	být	k5eAaImAgFnP	být
odkryty	odkrýt	k5eAaPmNgFnP	odkrýt
pouze	pouze	k6eAd1	pouze
malé	malý	k2eAgFnPc1d1	malá
části	část	k1gFnPc1	část
této	tento	k3xDgFnSc2	tento
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
i	i	k9	i
málo	málo	k4c1	málo
nálezů	nález	k1gInPc2	nález
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
zkázou	zkáza	k1gFnSc7	zkáza
města	město	k1gNnSc2	město
nebyli	být	k5eNaImAgMnP	být
řečtí	řecký	k2eAgMnPc1d1	řecký
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přírodní	přírodní	k2eAgFnSc1d1	přírodní
pohroma	pohroma	k1gFnSc1	pohroma
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
půl	půl	k1xP	půl
století	století	k1gNnSc2	století
ležel	ležet	k5eAaImAgMnS	ležet
Hissarlik	Hissarlik	k1gMnSc1	Hissarlik
zpustošen	zpustošit	k5eAaPmNgMnS	zpustošit
<g/>
,	,	kIx,	,
stopy	stopa	k1gFnPc1	stopa
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
jen	jen	k9	jen
po	po	k7c6	po
putujících	putující	k2eAgInPc6d1	putující
kmenech	kmen	k1gInPc6	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Osmé	osmý	k4xOgNnSc1	osmý
a	a	k8xC	a
deváté	devátý	k4xOgNnSc1	devátý
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
na	na	k7c6	na
troskách	troska	k1gFnPc6	troska
několika	několik	k4yIc2	několik
jejich	jejich	k3xOp3gMnPc2	jejich
předchůdců	předchůdce	k1gMnPc2	předchůdce
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
už	už	k6eAd1	už
k	k	k7c3	k
době	doba	k1gFnSc3	doba
helénistické	helénistický	k2eAgFnSc3d1	helénistická
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
významným	významný	k2eAgInSc7d1	významný
obchodním	obchodní	k2eAgInSc7d1	obchodní
centrem	centr	k1gInSc7	centr
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgNnP	být
ustanovena	ustanoven	k2eAgNnPc1d1	ustanoveno
Konstantinopol	Konstantinopol	k1gInSc1	Konstantinopol
jako	jako	k8xC	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
východní	východní	k2eAgFnSc2d1	východní
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
éře	éra	k1gFnSc6	éra
byzantské	byzantský	k2eAgFnSc6d1	byzantská
pak	pak	k6eAd1	pak
postupně	postupně	k6eAd1	postupně
upadalo	upadat	k5eAaImAgNnS	upadat
<g/>
,	,	kIx,	,
až	až	k9	až
nakonec	nakonec	k6eAd1	nakonec
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
moderní	moderní	k2eAgFnSc2d1	moderní
historické	historický	k2eAgFnSc2d1	historická
kritiky	kritika	k1gFnSc2	kritika
byl	být	k5eAaImAgInS	být
příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
trojské	trojský	k2eAgFnSc6d1	Trojská
válce	válka	k1gFnSc6	válka
zatlačen	zatlačen	k2eAgInSc4d1	zatlačen
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
pouhých	pouhý	k2eAgFnPc2d1	pouhá
legend	legenda	k1gFnPc2	legenda
<g/>
.	.	kIx.	.
</s>
<s>
Historikové	historik	k1gMnPc1	historik
přestali	přestat	k5eAaPmAgMnP	přestat
věřit	věřit	k5eAaImF	věřit
v	v	k7c4	v
jeho	jeho	k3xOp3gFnSc4	jeho
pravdivost	pravdivost	k1gFnSc4	pravdivost
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
sedmdesátých	sedmdesátý	k4xOgInPc2	sedmdesátý
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
názor	názor	k1gInSc1	názor
opět	opět	k6eAd1	opět
změnil	změnit	k5eAaPmAgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Nadšený	nadšený	k2eAgMnSc1d1	nadšený
německý	německý	k2eAgMnSc1d1	německý
archeolog	archeolog	k1gMnSc1	archeolog
Heinrich	Heinrich	k1gMnSc1	Heinrich
Schliemann	Schliemann	k1gMnSc1	Schliemann
odkryl	odkrýt	k5eAaPmAgMnS	odkrýt
tajemství	tajemství	k1gNnSc4	tajemství
ukryté	ukrytý	k2eAgNnSc4d1	ukryté
pod	pod	k7c7	pod
povrchem	povrch	k1gInSc7	povrch
pahorku	pahorek	k1gInSc2	pahorek
Hissarlik	Hissarlika	k1gFnPc2	Hissarlika
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
zde	zde	k6eAd1	zde
ruiny	ruina	k1gFnPc1	ruina
několika	několik	k4yIc2	několik
starověkých	starověký	k2eAgNnPc2d1	starověké
měst	město	k1gNnPc2	město
a	a	k8xC	a
prozkoumal	prozkoumat	k5eAaPmAgInS	prozkoumat
pět	pět	k4xCc4	pět
hrobů	hrob	k1gInPc2	hrob
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
místo	místo	k1gNnSc4	místo
posledního	poslední	k2eAgInSc2d1	poslední
odpočinku	odpočinek	k1gInSc2	odpočinek
trojského	trojský	k2eAgMnSc2d1	trojský
Priama	Priamos	k1gMnSc2	Priamos
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc2	jeho
potomků	potomek	k1gMnPc2	potomek
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
vrstvu	vrstva	k1gFnSc4	vrstva
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
Homérovu	Homérův	k2eAgFnSc4d1	Homérova
Tróju	Trója	k1gFnSc4	Trója
a	a	k8xC	a
tato	tento	k3xDgFnSc1	tento
jeho	jeho	k3xOp3gFnSc1	jeho
identifikace	identifikace	k1gFnSc1	identifikace
byla	být	k5eAaImAgFnS	být
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
přijímána	přijímán	k2eAgFnSc1d1	přijímána
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Schliemannovi	Schliemann	k1gMnSc6	Schliemann
bylo	být	k5eAaImAgNnS	být
naleziště	naleziště	k1gNnSc1	naleziště
odkryto	odkrýt	k5eAaPmNgNnS	odkrýt
týmem	tým	k1gInSc7	tým
archeologů	archeolog	k1gMnPc2	archeolog
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
W.	W.	kA	W.
Dörpfelda	Dörpfeldo	k1gNnSc2	Dörpfeldo
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
/	/	kIx~	/
<g/>
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
a	a	k8xC	a
Carla	Carl	k1gMnSc2	Carl
Blegena	Blegen	k1gMnSc2	Blegen
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
-	-	kIx~	-
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
vykopávky	vykopávka	k1gFnPc1	vykopávka
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
stálo	stát	k5eAaImAgNnS	stát
nejméně	málo	k6eAd3	málo
devět	devět	k4xCc4	devět
různých	různý	k2eAgNnPc2d1	různé
starověkých	starověký	k2eAgNnPc2d1	starověké
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
významným	významný	k2eAgMnSc7d1	významný
archeologem	archeolog	k1gMnSc7	archeolog
zabývajícím	zabývající	k2eAgMnSc7d1	zabývající
se	se	k3xPyFc4	se
otázkou	otázka	k1gFnSc7	otázka
trojské	trojský	k2eAgFnSc2d1	Trojská
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgMnS	být
profesor	profesor	k1gMnSc1	profesor
Manfred	Manfred	k1gMnSc1	Manfred
Korfmann	Korfmann	k1gMnSc1	Korfmann
s	s	k7c7	s
týmem	tým	k1gInSc7	tým
z	z	k7c2	z
Tübingenské	Tübingenský	k2eAgFnSc2d1	Tübingenská
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
Univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Cincinnati	Cincinnati	k1gFnSc6	Cincinnati
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
průzkum	průzkum	k1gInSc1	průzkum
odkryl	odkrýt	k5eAaPmAgInS	odkrýt
v	v	k7c6	v
polích	pole	k1gNnPc6	pole
pod	pod	k7c7	pod
pevností	pevnost	k1gFnSc7	pevnost
hluboký	hluboký	k2eAgInSc1d1	hluboký
příkop	příkop	k1gInSc1	příkop
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
leželo	ležet	k5eAaImAgNnS	ležet
mnoho	mnoho	k4c1	mnoho
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
bronzové	bronzový	k2eAgFnSc2d1	bronzová
<g/>
.	.	kIx.	.
</s>
<s>
Možným	možný	k2eAgNnSc7d1	možné
svědectvím	svědectví	k1gNnSc7	svědectví
o	o	k7c6	o
průběhu	průběh	k1gInSc6	průběh
bitvy	bitva	k1gFnSc2	bitva
by	by	kYmCp3nP	by
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
nalezené	nalezený	k2eAgInPc4d1	nalezený
hroty	hrot	k1gInPc4	hrot
šípů	šíp	k1gInPc2	šíp
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Přesný	přesný	k2eAgInSc1d1	přesný
letopočet	letopočet	k1gInSc1	letopočet
konání	konání	k1gNnSc2	konání
války	válka	k1gFnSc2	válka
o	o	k7c4	o
Tróju	Trója	k1gFnSc4	Trója
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
jen	jen	k9	jen
stěží	stěží	k6eAd1	stěží
určit	určit	k5eAaPmF	určit
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
u	u	k7c2	u
starověkých	starověký	k2eAgMnPc2d1	starověký
historiků	historik	k1gMnPc2	historik
se	se	k3xPyFc4	se
názory	názor	k1gInPc7	názor
rozcházejí	rozcházet	k5eAaImIp3nP	rozcházet
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
výpočtů	výpočet	k1gInPc2	výpočet
Eratosthena	Eratostheno	k1gNnSc2	Eratostheno
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
pádu	pád	k1gInSc3	pád
města	město	k1gNnSc2	město
přesně	přesně	k6eAd1	přesně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1184	[number]	k4	1184
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
.	.	kIx.	.
</s>
<s>
Hérodotos	Hérodotos	k1gMnSc1	Hérodotos
zase	zase	k9	zase
uvádí	uvádět	k5eAaImIp3nS	uvádět
letopočet	letopočet	k1gInSc4	letopočet
1250	[number]	k4	1250
<g/>
.	.	kIx.	.
</s>
<s>
Homér	Homér	k1gMnSc1	Homér
klade	klást	k5eAaImIp3nS	klást
trojskou	trojský	k2eAgFnSc4d1	Trojská
válku	válka	k1gFnSc4	válka
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
mykénské	mykénský	k2eAgFnSc2d1	mykénská
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
že	že	k8xS	že
hranicí	hranice	k1gFnSc7	hranice
je	být	k5eAaImIp3nS	být
konec	konec	k1gInSc1	konec
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
padla	padnout	k5eAaImAgFnS	padnout
mykénská	mykénský	k2eAgFnSc1d1	mykénská
kultura	kultura	k1gFnSc1	kultura
pod	pod	k7c7	pod
náporem	nápor	k1gInSc7	nápor
Dórů	Dór	k1gMnPc2	Dór
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dvanáctého	dvanáctý	k4xOgNnSc2	dvanáctý
století	století	k1gNnSc2	století
však	však	k9	však
museli	muset	k5eAaImAgMnP	muset
Řekové	Řek	k1gMnPc1	Řek
odolávat	odolávat	k5eAaImF	odolávat
jejich	jejich	k3xOp3gInPc3	jejich
nájezdům	nájezd	k1gInPc3	nájezd
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
dost	dost	k6eAd1	dost
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1	nepravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
vydávali	vydávat	k5eAaImAgMnP	vydávat
na	na	k7c4	na
dobyvačné	dobyvačný	k2eAgFnPc4d1	dobyvačná
cesty	cesta	k1gFnPc4	cesta
do	do	k7c2	do
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgMnPc4	který
se	se	k3xPyFc4	se
strhávají	strhávat	k5eAaImIp3nP	strhávat
pře	pře	k1gFnPc1	pře
o	o	k7c6	o
konání	konání	k1gNnSc6	konání
trojské	trojský	k2eAgFnSc2d1	Trojská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžněji	běžně	k6eAd3	běžně
udávaným	udávaný	k2eAgNnSc7d1	udávané
rozmezím	rozmezí	k1gNnSc7	rozmezí
je	být	k5eAaImIp3nS	být
1250	[number]	k4	1250
–	–	k?	–
1200	[number]	k4	1200
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
i	i	k9	i
starší	starý	k2eAgInPc1d2	starší
letopočty	letopočet	k1gInPc1	letopočet
<g/>
,	,	kIx,	,
dokonce	dokonce	k9	dokonce
i	i	k9	i
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
nasnadě	nasnadě	k6eAd1	nasnadě
souvislost	souvislost	k1gFnSc4	souvislost
se	s	k7c7	s
stěhováním	stěhování	k1gNnSc7	stěhování
"	"	kIx"	"
mořských	mořský	k2eAgInPc2d1	mořský
národů	národ	k1gInPc2	národ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Skutečná	skutečný	k2eAgFnSc1d1	skutečná
příčina	příčina	k1gFnSc1	příčina
boje	boj	k1gInSc2	boj
o	o	k7c4	o
město	město	k1gNnSc4	město
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
dnes	dnes	k6eAd1	dnes
už	už	k6eAd1	už
nedá	dát	k5eNaPmIp3nS	dát
přesně	přesně	k6eAd1	přesně
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
různých	různý	k2eAgInPc2d1	různý
názorů	názor	k1gInPc2	názor
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejrealističtější	realistický	k2eAgFnSc1d3	nejrealističtější
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
trojská	trojský	k2eAgFnSc1d1	Trojská
válka	válka	k1gFnSc1	válka
byla	být	k5eAaImAgFnS	být
pouze	pouze	k6eAd1	pouze
válkou	válka	k1gFnSc7	válka
obchodní	obchodní	k2eAgFnSc7d1	obchodní
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Trója	Trója	k1gFnSc1	Trója
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
rozkvětu	rozkvět	k1gInSc6	rozkvět
byla	být	k5eAaImAgFnS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
významných	významný	k2eAgNnPc2d1	významné
měst	město	k1gNnPc2	město
stojících	stojící	k2eAgNnPc2d1	stojící
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
obchodní	obchodní	k2eAgFnSc6d1	obchodní
cestě	cesta	k1gFnSc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Ovládala	ovládat	k5eAaImAgFnS	ovládat
cenný	cenný	k2eAgInSc4d1	cenný
obchod	obchod	k1gInSc4	obchod
se	s	k7c7	s
zlatem	zlato	k1gNnSc7	zlato
<g/>
,	,	kIx,	,
stříbrem	stříbro	k1gNnSc7	stříbro
<g/>
,	,	kIx,	,
dřevem	dřevo	k1gNnSc7	dřevo
<g/>
,	,	kIx,	,
konopím	konopí	k1gNnSc7	konopí
<g/>
,	,	kIx,	,
rybami	ryba	k1gFnPc7	ryba
<g/>
,	,	kIx,	,
olejem	olej	k1gInSc7	olej
či	či	k8xC	či
čínským	čínský	k2eAgInSc7d1	čínský
nefritem	nefrit	k1gInSc7	nefrit
a	a	k8xC	a
její	její	k3xOp3gNnPc4	její
dobytí	dobytí	k1gNnPc4	dobytí
řeckým	řecký	k2eAgNnSc7d1	řecké
národům	národ	k1gInPc3	národ
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
vybudovat	vybudovat	k5eAaPmF	vybudovat
své	svůj	k3xOyFgFnPc4	svůj
kolonie	kolonie	k1gFnPc4	kolonie
podél	podél	k7c2	podél
oné	onen	k3xDgFnSc2	onen
obchodní	obchodní	k2eAgFnSc2d1	obchodní
stezky	stezka	k1gFnSc2	stezka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nebylo	být	k5eNaImAgNnS	být
těžké	těžký	k2eAgNnSc1d1	těžké
rychle	rychle	k6eAd1	rychle
zbohatnout	zbohatnout	k5eAaPmF	zbohatnout
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
verze	verze	k1gFnSc1	verze
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
válka	válka	k1gFnSc1	válka
byla	být	k5eAaImAgFnS	být
pouze	pouze	k6eAd1	pouze
vyhrocením	vyhrocení	k1gNnSc7	vyhrocení
dlouhotrvajícího	dlouhotrvající	k2eAgNnSc2d1	dlouhotrvající
nepřátelství	nepřátelství	k1gNnSc2	nepřátelství
mezi	mezi	k7c7	mezi
řeckými	řecký	k2eAgInPc7d1	řecký
Lokry	Lokr	k1gInPc7	Lokr
a	a	k8xC	a
Priamem	Priamos	k1gMnSc7	Priamos
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
sporu	spor	k1gInSc2	spor
byl	být	k5eAaImAgInS	být
únos	únos	k1gInSc1	únos
Priamovy	Priamův	k2eAgFnSc2d1	Priamova
sestry	sestra	k1gFnSc2	sestra
jistým	jistý	k2eAgMnSc7d1	jistý
Aiakovcem	Aiakovec	k1gMnSc7	Aiakovec
Telamónem	Telamón	k1gMnSc7	Telamón
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
i	i	k9	i
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
trojských	trojský	k2eAgFnPc2d1	Trojská
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
hrálo	hrát	k5eAaImAgNnS	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
pozdějším	pozdní	k2eAgInSc6d2	pozdější
vývoji	vývoj	k1gInSc6	vývoj
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
podle	podle	k7c2	podle
věštby	věštba	k1gFnSc2	věštba
boha	bůh	k1gMnSc4	bůh
Apollóna	Apollón	k1gMnSc4	Apollón
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
právě	právě	k9	právě
místo	místo	k1gNnSc1	místo
postavené	postavený	k2eAgNnSc1d1	postavené
Aiakem	Aiaek	k1gMnSc7	Aiaek
prolomeno	prolomit	k5eAaPmNgNnS	prolomit
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
západní	západní	k2eAgFnSc1d1	západní
zeď	zeď	k1gFnSc1	zeď
a	a	k8xC	a
právě	právě	k9	právě
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
Dörpfeldovy	Dörpfeldův	k2eAgFnPc4d1	Dörpfeldův
vykopávky	vykopávka	k1gFnPc4	vykopávka
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
neznámých	známý	k2eNgInPc2d1	neznámý
důvodů	důvod	k1gInPc2	důvod
nejslabší	slabý	k2eAgInSc1d3	nejslabší
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
Lokrů	Lokr	k1gInPc2	Lokr
pocházel	pocházet	k5eAaImAgMnS	pocházet
i	i	k9	i
velký	velký	k2eAgMnSc1d1	velký
Achilleův	Achilleův	k2eAgMnSc1d1	Achilleův
přítel	přítel	k1gMnSc1	přítel
Patroklos	Patroklos	k1gMnSc1	Patroklos
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
starých	starý	k2eAgFnPc2d1	stará
řeckých	řecký	k2eAgFnPc2d1	řecká
pověstí	pověst	k1gFnPc2	pověst
byl	být	k5eAaImAgInS	být
však	však	k9	však
příčinou	příčina	k1gFnSc7	příčina
trojské	trojský	k2eAgFnSc2d1	Trojská
války	válka	k1gFnSc2	válka
osobní	osobní	k2eAgInSc4d1	osobní
spor	spor	k1gInSc4	spor
<g/>
.	.	kIx.	.
</s>
<s>
Básník	básník	k1gMnSc1	básník
Homér	Homér	k1gMnSc1	Homér
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
eposu	epos	k1gInSc6	epos
Ílias	Íliasa	k1gFnPc2	Íliasa
o	o	k7c6	o
konkrétním	konkrétní	k2eAgInSc6d1	konkrétní
původu	původ	k1gInSc6	původ
tohoto	tento	k3xDgInSc2	tento
sporu	spor	k1gInSc2	spor
nezmiňuje	zmiňovat	k5eNaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
několikrát	několikrát	k6eAd1	několikrát
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
děje	děj	k1gInSc2	děj
naráží	narážet	k5eAaImIp3nS	narážet
na	na	k7c4	na
krásnou	krásný	k2eAgFnSc4d1	krásná
Helenu	Helena	k1gFnSc4	Helena
a	a	k8xC	a
pohledného	pohledný	k2eAgMnSc4d1	pohledný
Parida	Paris	k1gMnSc4	Paris
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bájích	báj	k1gFnPc6	báj
je	být	k5eAaImIp3nS	být
Helena	Helena	k1gFnSc1	Helena
líčena	líčen	k2eAgFnSc1d1	líčena
jako	jako	k8xC	jako
překrásná	překrásný	k2eAgFnSc1d1	překrásná
dcera	dcera	k1gFnSc1	dcera
spartského	spartský	k2eAgMnSc2d1	spartský
krále	král	k1gMnSc2	král
Tyndarea	Tyndareus	k1gMnSc2	Tyndareus
<g/>
,	,	kIx,	,
za	za	k7c2	za
níž	jenž	k3xRgFnSc2	jenž
proudily	proudit	k5eAaImAgInP	proudit
davy	dav	k1gInPc1	dav
nápadníků	nápadník	k1gMnPc2	nápadník
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
mladí	mladý	k2eAgMnPc1d1	mladý
<g/>
,	,	kIx,	,
krásní	krásnit	k5eAaImIp3nP	krásnit
<g/>
,	,	kIx,	,
mocní	mocnit	k5eAaImIp3nP	mocnit
i	i	k9	i
odvážní	odvážný	k2eAgMnPc1d1	odvážný
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
volba	volba	k1gFnSc1	volba
však	však	k9	však
padla	padnout	k5eAaPmAgFnS	padnout
na	na	k7c4	na
nejbohatšího	bohatý	k2eAgInSc2d3	nejbohatší
z	z	k7c2	z
Achájců	Achájce	k1gMnPc2	Achájce
<g/>
,	,	kIx,	,
budoucího	budoucí	k2eAgMnSc2d1	budoucí
krále	král	k1gMnSc2	král
Sparty	Sparta	k1gFnSc2	Sparta
-	-	kIx~	-
Meneláa	Meneláa	k1gMnSc1	Meneláa
<g/>
.	.	kIx.	.
</s>
<s>
Paris	Paris	k1gMnSc1	Paris
byl	být	k5eAaImAgMnS	být
zase	zase	k9	zase
ztraceným	ztracený	k2eAgMnSc7d1	ztracený
synem	syn	k1gMnSc7	syn
trojského	trojský	k2eAgMnSc4d1	trojský
Priama	Priamos	k1gMnSc4	Priamos
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
ženy	žena	k1gFnPc1	žena
Hekabé	Hekabý	k2eAgFnPc1d1	Hekabý
<g/>
.	.	kIx.	.
</s>
<s>
Chlapec	chlapec	k1gMnSc1	chlapec
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
okamžitě	okamžitě	k6eAd1	okamžitě
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
zavražděn	zavražděn	k2eAgMnSc1d1	zavražděn
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
věštci	věštec	k1gMnPc1	věštec
(	(	kIx(	(
<g/>
Sibyla	Sibyla	k1gFnSc1	Sibyla
<g/>
)	)	kIx)	)
předpověděli	předpovědět	k5eAaPmAgMnP	předpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
zkázou	zkáza	k1gFnSc7	zkáza
nejen	nejen	k6eAd1	nejen
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
celé	celý	k2eAgFnSc2d1	celá
Tróje	Trója	k1gFnSc2	Trója
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Priamos	Priamos	k1gMnSc1	Priamos
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
ho	on	k3xPp3gMnSc4	on
nechal	nechat	k5eAaPmAgMnS	nechat
pouze	pouze	k6eAd1	pouze
odvézt	odvézt	k5eAaPmF	odvézt
z	z	k7c2	z
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
ujal	ujmout	k5eAaPmAgMnS	ujmout
správce	správce	k1gMnSc1	správce
stád	stádo	k1gNnPc2	stádo
Agelaos	Agelaos	k1gInSc4	Agelaos
a	a	k8xC	a
chlapce	chlapec	k1gMnPc4	chlapec
vychoval	vychovat	k5eAaPmAgInS	vychovat
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jednou	k6eAd1	jednou
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
pře	pře	k1gFnSc1	pře
mezi	mezi	k7c7	mezi
bohyněmi	bohyně	k1gFnPc7	bohyně
Hérou	Héra	k1gFnSc7	Héra
<g/>
,	,	kIx,	,
Afrodítou	Afrodíta	k1gFnSc7	Afrodíta
a	a	k8xC	a
Athénou	Athéna	k1gFnSc7	Athéna
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
nejkrásnější	krásný	k2eAgFnSc1d3	nejkrásnější
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Spor	spor	k1gInSc1	spor
o	o	k7c4	o
zlaté	zlatý	k2eAgNnSc4d1	Zlaté
jablko	jablko	k1gNnSc4	jablko
věnované	věnovaný	k2eAgFnPc1d1	věnovaná
bohyní	bohyně	k1gFnSc7	bohyně
sváru	svár	k1gInSc2	svár
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Té	ten	k3xDgFnSc2	ten
nejkrásnější	krásný	k2eAgFnSc2d3	nejkrásnější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
Souhlasily	souhlasit	k5eAaImAgFnP	souhlasit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nechají	nechat	k5eAaPmIp3nP	nechat
Parida	Paris	k1gMnSc4	Paris
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
jejich	jejich	k3xOp3gInSc4	jejich
spor	spor	k1gInSc4	spor
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
mu	on	k3xPp3gMnSc3	on
nabízela	nabízet	k5eAaImAgFnS	nabízet
mnoho	mnoho	k4c4	mnoho
darů	dar	k1gInPc2	dar
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
ji	on	k3xPp3gFnSc4	on
prohlásí	prohlásit	k5eAaPmIp3nS	prohlásit
za	za	k7c4	za
nejhezčí	hezký	k2eAgNnSc4d3	nejhezčí
<g/>
.	.	kIx.	.
</s>
<s>
Héra	Héra	k1gFnSc1	Héra
mu	on	k3xPp3gMnSc3	on
nabízela	nabízet	k5eAaImAgFnS	nabízet
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
Evropou	Evropa	k1gFnSc7	Evropa
i	i	k8xC	i
Asií	Asie	k1gFnSc7	Asie
a	a	k8xC	a
Athéna	Athéna	k1gFnSc1	Athéna
moudrost	moudrost	k1gFnSc1	moudrost
a	a	k8xC	a
válečné	válečný	k2eAgNnSc1d1	válečné
umění	umění	k1gNnSc1	umění
<g/>
.	.	kIx.	.
</s>
<s>
Afrodíté	Afrodíta	k1gMnPc1	Afrodíta
mu	on	k3xPp3gInSc3	on
však	však	k9	však
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
dar	dar	k1gInSc4	dar
nejcennější	cenný	k2eAgInSc4d3	nejcennější
<g/>
,	,	kIx,	,
lásku	láska	k1gFnSc4	láska
–	–	k?	–
zapřisáhla	zapřisáhnout	k5eAaPmAgFnS	zapřisáhnout
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
postará	postarat	k5eAaPmIp3nS	postarat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
spartská	spartský	k2eAgFnSc1d1	spartská
Helena	Helena	k1gFnSc1	Helena
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gMnSc3	jeho
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
se	se	k3xPyFc4	se
Paris	Paris	k1gMnSc1	Paris
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
pro	pro	k7c4	pro
vítězství	vítězství	k1gNnSc4	vítězství
Afrodíty	Afrodíta	k1gFnSc2	Afrodíta
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k9	jako
Paridův	Paridův	k2eAgInSc4d1	Paridův
soud	soud	k1gInSc4	soud
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
námětem	námět	k1gInSc7	námět
mnoha	mnoho	k4c2	mnoho
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
konečně	konečně	k9	konečně
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
Helenou	Helena	k1gFnSc7	Helena
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
něho	on	k3xPp3gNnSc2	on
opravdu	opravdu	k6eAd1	opravdu
ihned	ihned	k6eAd1	ihned
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
a	a	k8xC	a
odešla	odejít	k5eAaPmAgFnS	odejít
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
do	do	k7c2	do
Tróje	Trója	k1gFnSc2	Trója
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
velice	velice	k6eAd1	velice
rozhněvala	rozhněvat	k5eAaPmAgNnP	rozhněvat
Meneláa	Meneláum	k1gNnPc1	Meneláum
<g/>
.	.	kIx.	.
</s>
<s>
Meneláos	Meneláos	k1gMnSc1	Meneláos
pak	pak	k6eAd1	pak
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Agamemnonem	Agamemnon	k1gMnSc7	Agamemnon
svolali	svolat	k5eAaPmAgMnP	svolat
hotovost	hotovost	k1gFnSc4	hotovost
a	a	k8xC	a
vedli	vést	k5eAaImAgMnP	vést
vojsko	vojsko	k1gNnSc4	vojsko
proti	proti	k7c3	proti
Tróji	Trója	k1gFnSc3	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
boje	boj	k1gInSc2	boj
za	za	k7c4	za
čest	čest	k1gFnSc4	čest
řeckého	řecký	k2eAgMnSc2d1	řecký
velmože	velmož	k1gMnSc2	velmož
se	se	k3xPyFc4	se
vydalo	vydat	k5eAaPmAgNnS	vydat
podle	podle	k7c2	podle
Homérova	Homérův	k2eAgNnSc2d1	Homérovo
líčení	líčení	k1gNnSc2	líčení
1144	[number]	k4	1144
lodí	loď	k1gFnPc2	loď
s	s	k7c7	s
vojáky	voják	k1gMnPc7	voják
z	z	k7c2	z
Athén	Athéna	k1gFnPc2	Athéna
<g/>
,	,	kIx,	,
Sparty	Sparta	k1gFnSc2	Sparta
<g/>
,	,	kIx,	,
Mykén	Mykény	k1gFnPc2	Mykény
<g/>
,	,	kIx,	,
Thessalie	Thessalie	k1gFnSc2	Thessalie
<g/>
,	,	kIx,	,
Pylu	pyl	k1gInSc2	pyl
<g/>
,	,	kIx,	,
Arkadie	Arkadie	k1gFnSc2	Arkadie
<g/>
,	,	kIx,	,
Salamíny	Salamína	k1gFnSc2	Salamína
<g/>
,	,	kIx,	,
Ithaky	Ithaka	k1gFnSc2	Ithaka
<g/>
,	,	kIx,	,
Kréty	Kréta	k1gFnSc2	Kréta
<g/>
,	,	kIx,	,
Rhodu	Rhodos	k1gInSc2	Rhodos
<g/>
,	,	kIx,	,
Élidy	Élida	k1gFnSc2	Élida
<g/>
,	,	kIx,	,
Bojótie	Bojótie	k1gFnSc2	Bojótie
<g/>
,	,	kIx,	,
Euboie	Euboie	k1gFnSc2	Euboie
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgInPc2d1	další
městských	městský	k2eAgInPc2d1	městský
států	stát	k1gInPc2	stát
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
koutů	kout	k1gInPc2	kout
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
vůdců	vůdce	k1gMnPc2	vůdce
těchto	tento	k3xDgInPc2	tento
států	stát	k1gInPc2	stát
totiž	totiž	k9	totiž
byla	být	k5eAaImAgFnS	být
dřívějšími	dřívější	k2eAgMnPc7d1	dřívější
Heleninými	Helenin	k2eAgMnPc7d1	Helenin
nápadníky	nápadník	k1gMnPc7	nápadník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
na	na	k7c6	na
rozčtvrceném	rozčtvrcený	k2eAgMnSc6d1	rozčtvrcený
obětním	obětní	k2eAgMnSc6d1	obětní
koni	kůň	k1gMnSc6	kůň
přislíbili	přislíbit	k5eAaPmAgMnP	přislíbit
věrnost	věrnost	k1gFnSc1	věrnost
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
koho	kdo	k3yRnSc4	kdo
si	se	k3xPyFc3	se
Helena	Helena	k1gFnSc1	Helena
vyvolí	vyvolit	k5eAaPmIp3nS	vyvolit
za	za	k7c4	za
chotě	choť	k1gMnSc4	choť
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
války	válka	k1gFnSc2	válka
tedy	tedy	k9	tedy
kráčel	kráčet	k5eAaImAgMnS	kráčet
i	i	k9	i
Odysseus	Odysseus	k1gMnSc1	Odysseus
z	z	k7c2	z
Ithaky	Ithaka	k1gFnSc2	Ithaka
<g/>
,	,	kIx,	,
Velký	velký	k2eAgInSc1d1	velký
Aiás	Aiás	k1gInSc1	Aiás
ze	z	k7c2	z
Salamíny	Salamína	k1gFnSc2	Salamína
<g/>
,	,	kIx,	,
Patroklos	Patroklos	k1gMnSc1	Patroklos
<g/>
,	,	kIx,	,
Diomédes	Diomédes	k1gMnSc1	Diomédes
(	(	kIx(	(
<g/>
král	král	k1gMnSc1	král
v	v	k7c6	v
Argu	Argos	k1gInSc6	Argos
<g/>
)	)	kIx)	)
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
hrdinové	hrdina	k1gMnPc1	hrdina
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
u	u	k7c2	u
Tróje	Trója	k1gFnSc2	Trója
vyznamenali	vyznamenat	k5eAaPmAgMnP	vyznamenat
<g/>
.	.	kIx.	.
</s>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
rolí	role	k1gFnPc2	role
hrál	hrát	k5eAaImAgMnS	hrát
také	také	k9	také
bájný	bájný	k2eAgMnSc1d1	bájný
Achilleus	Achilleus	k1gMnSc1	Achilleus
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
krále	král	k1gMnSc2	král
Pélea	Péleum	k1gNnSc2	Péleum
a	a	k8xC	a
mořské	mořský	k2eAgFnSc2d1	mořská
bohyně	bohyně	k1gFnSc2	bohyně
Thetis	Thetis	k1gFnSc2	Thetis
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
jehož	jehož	k3xOyRp3gFnSc2	jehož
pomoci	pomoc	k1gFnSc2	pomoc
by	by	kYmCp3nS	by
prý	prý	k9	prý
Trója	Trója	k1gFnSc1	Trója
nemohla	moct	k5eNaImAgFnS	moct
být	být	k5eAaImF	být
dobyta	dobyt	k2eAgFnSc1d1	dobyta
<g/>
.	.	kIx.	.
</s>
<s>
Věštba	věštba	k1gFnSc1	věštba
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
předpověděla	předpovědět	k5eAaPmAgFnS	předpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jednou	jednou	k6eAd1	jednou
k	k	k7c3	k
Tróje	Trója	k1gFnSc2	Trója
vydá	vydat	k5eAaPmIp3nS	vydat
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
nenavrátí	navrátit	k5eNaPmIp3nP	navrátit
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Thetis	Thetis	k1gFnSc1	Thetis
měla	mít	k5eAaImAgFnS	mít
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
–	–	k?	–
buď	buď	k8xC	buď
se	se	k3xPyFc4	se
Achilleus	Achilleus	k1gMnSc1	Achilleus
stane	stanout	k5eAaPmIp3nS	stanout
slavným	slavný	k2eAgMnSc7d1	slavný
a	a	k8xC	a
mladý	mladý	k1gMnSc1	mladý
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
prožije	prožít	k5eAaPmIp3nS	prožít
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
život	život	k1gInSc4	život
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
slávy	sláva	k1gFnSc2	sláva
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
matka	matka	k1gFnSc1	matka
však	však	k9	však
nechtěla	chtít	k5eNaImAgFnS	chtít
přijít	přijít	k5eAaPmF	přijít
o	o	k7c4	o
svého	svůj	k3xOyFgMnSc4	svůj
milovaného	milovaný	k1gMnSc4	milovaný
syna	syn	k1gMnSc4	syn
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
Achillea	Achilleus	k1gMnSc4	Achilleus
skrýt	skrýt	k5eAaPmF	skrýt
v	v	k7c6	v
dívčích	dívčí	k2eAgInPc6d1	dívčí
šatech	šat	k1gInPc6	šat
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
skyrského	skyrský	k2eAgMnSc2d1	skyrský
krále	král	k1gMnSc2	král
Lykoméda	Lykoméd	k1gMnSc2	Lykoméd
<g/>
.	.	kIx.	.
</s>
<s>
Moudrý	moudrý	k2eAgInSc1d1	moudrý
Odysseus	Odysseus	k1gInSc1	Odysseus
však	však	k9	však
brzy	brzy	k6eAd1	brzy
prohlédl	prohlédnout	k5eAaPmAgMnS	prohlédnout
jeho	jeho	k3xOp3gNnSc4	jeho
převlečení	převlečení	k1gNnSc4	převlečení
a	a	k8xC	a
Achilleus	Achilleus	k1gMnSc1	Achilleus
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
povede	povést	k5eAaPmIp3nS	povést
svůj	svůj	k3xOyFgInSc4	svůj
kmen	kmen	k1gInSc4	kmen
Myrmidonů	Myrmidon	k1gMnPc2	Myrmidon
k	k	k7c3	k
Tróji	Trója	k1gFnSc3	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Lodě	loď	k1gFnPc1	loď
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
shromáždily	shromáždit	k5eAaPmAgInP	shromáždit
v	v	k7c6	v
přístavu	přístav	k1gInSc6	přístav
Aulidě	Aulida	k1gFnSc6	Aulida
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
společně	společně	k6eAd1	společně
vypluly	vyplout	k5eAaPmAgFnP	vyplout
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Tróji	Trója	k1gFnSc3	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
dorazily	dorazit	k5eAaPmAgFnP	dorazit
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
vytáhla	vytáhnout	k5eAaPmAgFnS	vytáhnout
posádka	posádka	k1gFnSc1	posádka
lodě	loď	k1gFnSc2	loď
na	na	k7c4	na
břeh	břeh	k1gInSc4	břeh
a	a	k8xC	a
zbudovala	zbudovat	k5eAaPmAgFnS	zbudovat
si	se	k3xPyFc3	se
kolem	kolem	k7c2	kolem
nich	on	k3xPp3gMnPc2	on
stanové	stanový	k2eAgNnSc4d1	stanové
tábořiště	tábořiště	k1gNnSc4	tábořiště
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
obehnala	obehnat	k5eAaPmAgFnS	obehnat
zdí	zeď	k1gFnSc7	zeď
a	a	k8xC	a
hlubokým	hluboký	k2eAgInSc7d1	hluboký
příkopem	příkop	k1gInSc7	příkop
<g/>
.	.	kIx.	.
</s>
<s>
Devět	devět	k4xCc1	devět
dlouhých	dlouhý	k2eAgNnPc2d1	dlouhé
let	léto	k1gNnPc2	léto
obléhali	obléhat	k5eAaImAgMnP	obléhat
Řekové	Řek	k1gMnPc1	Řek
Tróju	Trója	k1gFnSc4	Trója
<g/>
,	,	kIx,	,
plenili	plenit	k5eAaImAgMnP	plenit
okolní	okolní	k2eAgFnPc4d1	okolní
vesnice	vesnice	k1gFnPc4	vesnice
a	a	k8xC	a
zmocňovali	zmocňovat	k5eAaImAgMnP	zmocňovat
se	se	k3xPyFc4	se
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
dobýt	dobýt	k5eAaPmF	dobýt
město	město	k1gNnSc1	město
jim	on	k3xPp3gMnPc3	on
prozatím	prozatím	k6eAd1	prozatím
nebylo	být	k5eNaImAgNnS	být
souzeno	souzen	k2eAgNnSc1d1	souzeno
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
věštba	věštba	k1gFnSc1	věštba
zaručovala	zaručovat	k5eAaImAgFnS	zaručovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Trója	Trója	k1gFnSc1	Trója
nepadne	padnout	k5eNaPmIp3nS	padnout
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
Priamův	Priamův	k2eAgMnSc1d1	Priamův
syn	syn	k1gMnSc1	syn
Troilos	Troilos	k1gMnSc1	Troilos
nedožije	dožít	k5eNaPmIp3nS	dožít
dvaceti	dvacet	k4xCc2	dvacet
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Vojska	vojsko	k1gNnPc1	vojsko
měla	mít	k5eAaImAgNnP	mít
také	také	k9	také
poměrně	poměrně	k6eAd1	poměrně
vyrovnané	vyrovnaný	k2eAgFnPc1d1	vyrovnaná
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Tróje	Trója	k1gFnSc2	Trója
stálo	stát	k5eAaImAgNnS	stát
mnoho	mnoho	k4c1	mnoho
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
nejvýraznější	výrazný	k2eAgFnSc4d3	nejvýraznější
úlohu	úloha	k1gFnSc4	úloha
hráli	hrát	k5eAaImAgMnP	hrát
hlavně	hlavně	k9	hlavně
Lykijci	Lykijce	k1gMnPc1	Lykijce
<g/>
,	,	kIx,	,
Mýsové	Mýsus	k1gMnPc5	Mýsus
<g/>
,	,	kIx,	,
Kikoni	Kikon	k1gMnPc5	Kikon
<g/>
,	,	kIx,	,
Dardani	Dardan	k1gMnPc5	Dardan
<g/>
,	,	kIx,	,
Maioni	Maion	k1gMnPc5	Maion
<g/>
,	,	kIx,	,
Frygové	Fryg	k1gMnPc5	Fryg
<g/>
,	,	kIx,	,
Kárové	kárový	k2eAgFnSc3d1	Kárová
<g/>
,	,	kIx,	,
Pelasgové	Pelasgový	k2eAgFnSc3d1	Pelasgový
a	a	k8xC	a
Halisgóni	Halisgóň	k1gMnSc3	Halisgóň
<g/>
.	.	kIx.	.
</s>
<s>
Trojští	trojský	k2eAgMnPc1d1	trojský
měli	mít	k5eAaImAgMnP	mít
také	také	k9	také
velikou	veliký	k2eAgFnSc4d1	veliká
výhodu	výhoda	k1gFnSc4	výhoda
v	v	k7c6	v
městském	městský	k2eAgNnSc6d1	Městské
opevnění	opevnění	k1gNnSc6	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
přiblížil	přiblížit	k5eAaPmAgInS	přiblížit
konec	konec	k1gInSc1	konec
devátého	devátý	k4xOgInSc2	devátý
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
přestali	přestat	k5eAaPmAgMnP	přestat
Řekové	Řek	k1gMnPc1	Řek
pustošit	pustošit	k5eAaImF	pustošit
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
soustředili	soustředit	k5eAaPmAgMnP	soustředit
své	svůj	k3xOyFgFnPc4	svůj
síly	síla	k1gFnPc4	síla
na	na	k7c6	na
trojské	trojský	k2eAgFnSc6d1	Trojská
pláni	pláň	k1gFnSc6	pláň
pod	pod	k7c7	pod
městskými	městský	k2eAgFnPc7d1	městská
hradbami	hradba	k1gFnPc7	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Desátý	desátý	k4xOgInSc4	desátý
rok	rok	k1gInSc4	rok
trojské	trojský	k2eAgFnSc2d1	Trojská
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
sporem	spor	k1gInSc7	spor
o	o	k7c4	o
dceru	dcera	k1gFnSc4	dcera
Apollónova	Apollónův	k2eAgMnSc2d1	Apollónův
kněze	kněz	k1gMnSc2	kněz
Chrýsa	Chrýs	k1gMnSc2	Chrýs
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
unesl	unést	k5eAaPmAgMnS	unést
velitel	velitel	k1gMnSc1	velitel
Řeků	Řek	k1gMnPc2	Řek
<g/>
,	,	kIx,	,
mykénský	mykénský	k2eAgMnSc1d1	mykénský
král	král	k1gMnSc1	král
Agamemnón	Agamemnón	k1gMnSc1	Agamemnón
<g/>
.	.	kIx.	.
</s>
<s>
Apollón	Apollón	k1gMnSc1	Apollón
z	z	k7c2	z
hněvu	hněv	k1gInSc2	hněv
seslal	seslat	k5eAaPmAgMnS	seslat
na	na	k7c4	na
achájský	achájský	k2eAgInSc4d1	achájský
tábor	tábor	k1gInSc4	tábor
mor	mora	k1gFnPc2	mora
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
deset	deset	k4xCc4	deset
dnů	den	k1gInPc2	den
sužovala	sužovat	k5eAaImAgFnS	sužovat
choroba	choroba	k1gFnSc1	choroba
vojáky	voják	k1gMnPc4	voják
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
Agamemnón	Agamemnón	k1gMnSc1	Agamemnón
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
naléhání	naléhání	k1gNnSc4	naléhání
Chrýsovu	Chrýsův	k2eAgFnSc4d1	Chrýsův
dceru	dcera	k1gFnSc4	dcera
vrátit	vrátit	k5eAaPmF	vrátit
otci	otec	k1gMnSc3	otec
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
náhradu	náhrada	k1gFnSc4	náhrada
si	se	k3xPyFc3	se
však	však	k9	však
vyvolil	vyvolit	k5eAaPmAgMnS	vyvolit
Achilleovu	Achilleův	k2eAgFnSc4d1	Achilleova
milenku	milenka	k1gFnSc4	milenka
Bríseovnu	Bríseovna	k1gFnSc4	Bríseovna
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
vyvolal	vyvolat	k5eAaPmAgMnS	vyvolat
velkou	velký	k2eAgFnSc4d1	velká
nevoli	nevole	k1gFnSc4	nevole
a	a	k8xC	a
nepřátelství	nepřátelství	k1gNnSc4	nepřátelství
<g/>
.	.	kIx.	.
</s>
<s>
Achilles	Achilles	k1gMnSc1	Achilles
tedy	tedy	k9	tedy
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
za	za	k7c4	za
Agamemnóna	Agamemnón	k1gMnSc4	Agamemnón
bojovat	bojovat	k5eAaImF	bojovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
i	i	k9	i
jeho	jeho	k3xOp3gMnPc1	jeho
Myrmidoni	Myrmidon	k1gMnPc1	Myrmidon
z	z	k7c2	z
Thesálie	Thesálie	k1gFnSc2	Thesálie
<g/>
.	.	kIx.	.
</s>
<s>
Agamemnón	Agamemnón	k1gMnSc1	Agamemnón
pak	pak	k6eAd1	pak
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c4	o
Achillovu	Achillův	k2eAgFnSc4d1	Achillova
pomoc	pomoc	k1gFnSc4	pomoc
nestojí	stát	k5eNaImIp3nS	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
do	do	k7c2	do
boje	boj	k1gInSc2	boj
bez	bez	k7c2	bez
něj	on	k3xPp3gInSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
stranami	strana	k1gFnPc7	strana
byl	být	k5eAaImAgInS	být
celý	celý	k2eAgInSc1d1	celý
protkán	protkán	k2eAgInSc1d1	protkán
intrikami	intrika	k1gFnPc7	intrika
bohů	bůh	k1gMnPc2	bůh
a	a	k8xC	a
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
také	také	k9	také
probíhal	probíhat	k5eAaImAgMnS	probíhat
<g/>
.	.	kIx.	.
</s>
<s>
Ochránci	ochránce	k1gMnPc1	ochránce
řeckých	řecký	k2eAgNnPc2d1	řecké
vojsk	vojsko	k1gNnPc2	vojsko
byli	být	k5eAaImAgMnP	být
například	například	k6eAd1	například
mořský	mořský	k2eAgInSc4d1	mořský
Poseidón	Poseidón	k1gInSc4	Poseidón
<g/>
,	,	kIx,	,
Héra	Héra	k1gFnSc1	Héra
<g/>
,	,	kIx,	,
Pallas	Pallas	k1gMnSc1	Pallas
Athéna	Athéna	k1gFnSc1	Athéna
nebo	nebo	k8xC	nebo
Achilleova	Achilleův	k2eAgFnSc1d1	Achilleova
matka	matka	k1gFnSc1	matka
bohyně	bohyně	k1gFnSc1	bohyně
Thetis	Thetis	k1gFnSc1	Thetis
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
trójské	trójský	k2eAgFnSc6d1	Trójská
stál	stát	k5eAaImAgInS	stát
sám	sám	k3xTgInSc1	sám
mocný	mocný	k2eAgInSc1d1	mocný
Zeus	Zeus	k1gInSc1	Zeus
<g/>
,	,	kIx,	,
válečník	válečník	k1gMnSc1	válečník
Áres	Áres	k1gMnSc1	Áres
či	či	k8xC	či
stříbrnoluký	stříbrnoluký	k2eAgMnSc1d1	stříbrnoluký
Apollón	Apollón	k1gMnSc1	Apollón
<g/>
.	.	kIx.	.
</s>
<s>
Bohové	bůh	k1gMnPc1	bůh
vyvolali	vyvolat	k5eAaPmAgMnP	vyvolat
i	i	k9	i
první	první	k4xOgFnSc4	první
bitvu	bitva	k1gFnSc4	bitva
desátého	desátý	k4xOgInSc2	desátý
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Zeus	Zeus	k6eAd1	Zeus
seslal	seslat	k5eAaPmAgMnS	seslat
k	k	k7c3	k
Agamemnónovi	Agamemnón	k1gMnSc3	Agamemnón
sen	sen	k1gInSc4	sen
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
ho	on	k3xPp3gMnSc4	on
pobízel	pobízet	k5eAaImAgInS	pobízet
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
<g/>
.	.	kIx.	.
</s>
<s>
Vůdce	vůdce	k1gMnSc1	vůdce
tedy	tedy	k9	tedy
uposlechl	uposlechnout	k5eAaPmAgMnS	uposlechnout
jeho	jeho	k3xOp3gFnPc4	jeho
rady	rada	k1gFnPc4	rada
a	a	k8xC	a
vehnal	vehnat	k5eAaPmAgInS	vehnat
muže	muž	k1gMnPc4	muž
do	do	k7c2	do
nelítostného	lítostný	k2eNgInSc2d1	nelítostný
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Vojska	vojsko	k1gNnPc1	vojsko
se	se	k3xPyFc4	se
však	však	k9	však
zatím	zatím	k6eAd1	zatím
nestřetla	střetnout	k5eNaPmAgFnS	střetnout
<g/>
,	,	kIx,	,
poněvadž	poněvadž	k8xS	poněvadž
Paris	Paris	k1gMnSc1	Paris
<g/>
,	,	kIx,	,
spatřiv	spatřit	k5eAaPmDgInS	spatřit
blížící	blížící	k2eAgNnSc4d1	blížící
se	se	k3xPyFc4	se
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
utkat	utkat	k5eAaPmF	utkat
s	s	k7c7	s
Meneláem	Meneláum	k1gNnSc7	Meneláum
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
tedy	tedy	k9	tedy
uzavřeno	uzavřen	k2eAgNnSc1d1	uzavřeno
příměří	příměří	k1gNnSc1	příměří
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
muži	muž	k1gMnPc1	muž
se	se	k3xPyFc4	se
pustili	pustit	k5eAaPmAgMnP	pustit
do	do	k7c2	do
boje	boj	k1gInSc2	boj
na	na	k7c4	na
život	život	k1gInSc4	život
a	a	k8xC	a
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
však	však	k9	však
do	do	k7c2	do
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
bohyně	bohyně	k1gFnSc1	bohyně
Afrodíté	Afrodítý	k2eAgFnSc2d1	Afrodítý
a	a	k8xC	a
prohrávajícího	prohrávající	k2eAgMnSc2d1	prohrávající
Parida	Paris	k1gMnSc2	Paris
zachránila	zachránit	k5eAaPmAgFnS	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
Trojský	trojský	k2eAgMnSc1d1	trojský
bojovník	bojovník	k1gMnSc1	bojovník
Pandaros	Pandarosa	k1gFnPc2	Pandarosa
pak	pak	k6eAd1	pak
porušil	porušit	k5eAaPmAgMnS	porušit
příměří	příměří	k1gNnSc4	příměří
a	a	k8xC	a
zranil	zranit	k5eAaPmAgMnS	zranit
Meneláa	Meneláa	k1gMnSc1	Meneláa
šípem	šíp	k1gInSc7	šíp
<g/>
.	.	kIx.	.
</s>
<s>
Strhla	strhnout	k5eAaPmAgFnS	strhnout
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
bitva	bitva	k1gFnSc1	bitva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
však	však	k9	však
neměla	mít	k5eNaImAgFnS	mít
vítěze	vítěz	k1gMnSc4	vítěz
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k8xS	tak
se	se	k3xPyFc4	se
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
dohodly	dohodnout	k5eAaPmAgFnP	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
druhý	druhý	k4xOgInSc1	druhý
den	den	k1gInSc1	den
bude	být	k5eAaImBp3nS	být
boj	boj	k1gInSc1	boj
pozastaven	pozastavit	k5eAaPmNgInS	pozastavit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohli	moct	k5eAaImAgMnP	moct
řádně	řádně	k6eAd1	řádně
pohřbít	pohřbít	k5eAaPmF	pohřbít
své	svůj	k3xOyFgMnPc4	svůj
mrtvé	mrtvý	k1gMnPc4	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
bojových	bojový	k2eAgInPc6d1	bojový
dnech	den	k1gInPc6	den
se	se	k3xPyFc4	se
úspěchy	úspěch	k1gInPc1	úspěch
střídaly	střídat	k5eAaImAgInP	střídat
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
Trójanům	Trójan	k1gMnPc3	Trójan
zatlačit	zatlačit	k5eAaPmF	zatlačit
Acháje	Acháj	k1gFnPc4	Acháj
až	až	k6eAd1	až
k	k	k7c3	k
hradbám	hradba	k1gFnPc3	hradba
tábořiště	tábořiště	k1gNnSc2	tábořiště
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
však	však	k9	však
dále	daleko	k6eAd2	daleko
nedostali	dostat	k5eNaPmAgMnP	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
pak	pak	k6eAd1	pak
byli	být	k5eAaImAgMnP	být
dalším	další	k2eAgInSc7d1	další
útokem	útok	k1gInSc7	útok
Řeků	Řek	k1gMnPc2	Řek
vytlačeni	vytlačen	k2eAgMnPc1d1	vytlačen
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
městu	město	k1gNnSc3	město
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
však	však	k9	však
Agamemnón	Agamemnón	k1gMnSc1	Agamemnón
zraněn	zraněn	k2eAgMnSc1d1	zraněn
a	a	k8xC	a
morálka	morálka	k1gFnSc1	morálka
řeckého	řecký	k2eAgNnSc2d1	řecké
vojska	vojsko	k1gNnSc2	vojsko
značně	značně	k6eAd1	značně
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
<g/>
.	.	kIx.	.
</s>
<s>
Trójané	Trójan	k1gMnPc1	Trójan
toho	ten	k3xDgMnSc4	ten
využili	využít	k5eAaPmAgMnP	využít
a	a	k8xC	a
postoupili	postoupit	k5eAaPmAgMnP	postoupit
až	až	k6eAd1	až
k	k	k7c3	k
táboru	tábor	k1gInSc3	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Trojský	trojský	k2eAgMnSc1d1	trojský
vůdce	vůdce	k1gMnSc1	vůdce
Hektór	Hektór	k1gMnSc1	Hektór
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
krále	král	k1gMnSc2	král
Priama	Priamos	k1gMnSc2	Priamos
<g/>
,	,	kIx,	,
poručil	poručit	k5eAaPmAgMnS	poručit
pěší	pěší	k2eAgInSc4d1	pěší
postup	postup	k1gInSc4	postup
přes	přes	k7c4	přes
příkop	příkop	k1gInSc4	příkop
a	a	k8xC	a
po	po	k7c6	po
delším	dlouhý	k2eAgNnSc6d2	delší
obléhání	obléhání	k1gNnSc6	obléhání
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
tak	tak	k9	tak
podařilo	podařit	k5eAaPmAgNnS	podařit
proniknout	proniknout	k5eAaPmF	proniknout
až	až	k9	až
k	k	k7c3	k
řeckým	řecký	k2eAgFnPc3d1	řecká
lodím	loď	k1gFnPc3	loď
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
Patroklos	Patroklos	k1gInSc1	Patroklos
<g/>
,	,	kIx,	,
nejvěrnější	věrný	k2eAgInSc1d3	nejvěrnější
Achilleův	Achilleův	k2eAgInSc4d1	Achilleův
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
zašel	zajít	k5eAaPmAgMnS	zajít
za	za	k7c7	za
Achilleem	Achilleus	k1gMnSc7	Achilleus
a	a	k8xC	a
požádal	požádat	k5eAaPmAgMnS	požádat
ho	on	k3xPp3gMnSc4	on
o	o	k7c4	o
povolení	povolení	k1gNnSc4	povolení
vytáhnout	vytáhnout	k5eAaPmF	vytáhnout
do	do	k7c2	do
bitvy	bitva	k1gFnSc2	bitva
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
zbroji	zbroj	k1gFnSc6	zbroj
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
Achilles	Achilles	k1gMnSc1	Achilles
dal	dát	k5eAaPmAgMnS	dát
svoje	svůj	k3xOyFgNnSc4	svůj
svolení	svolení	k1gNnSc4	svolení
<g/>
,	,	kIx,	,
vyšel	vyjít	k5eAaPmAgInS	vyjít
Patroklos	Patroklos	k1gInSc1	Patroklos
s	s	k7c7	s
vojáky	voják	k1gMnPc7	voják
do	do	k7c2	do
boje	boj	k1gInSc2	boj
<g/>
.	.	kIx.	.
</s>
<s>
Vyděšení	vyděšený	k2eAgMnPc1d1	vyděšený
Trójané	Trójan	k1gMnPc1	Trójan
si	se	k3xPyFc3	se
mysleli	myslet	k5eAaImAgMnP	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
proti	proti	k7c3	proti
nim	on	k3xPp3gMnPc3	on
stojí	stát	k5eAaImIp3nS	stát
sám	sám	k3xTgMnSc1	sám
Achilles	Achilles	k1gMnSc1	Achilles
<g/>
,	,	kIx,	,
a	a	k8xC	a
otočili	otočit	k5eAaPmAgMnP	otočit
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
k	k	k7c3	k
útěku	útěk	k1gInSc3	útěk
<g/>
.	.	kIx.	.
</s>
<s>
Hektór	Hektór	k1gInSc1	Hektór
však	však	k9	však
brzy	brzy	k6eAd1	brzy
poznal	poznat	k5eAaPmAgMnS	poznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
za	za	k7c7	za
nádhernou	nádherný	k2eAgFnSc7d1	nádherná
Achillovou	Achillův	k2eAgFnSc7d1	Achillova
zbrojí	zbroj	k1gFnSc7	zbroj
skrývá	skrývat	k5eAaImIp3nS	skrývat
slabší	slabý	k2eAgInSc4d2	slabší
Patroklos	Patroklos	k1gInSc4	Patroklos
<g/>
,	,	kIx,	,
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
ho	on	k3xPp3gInSc4	on
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
zabil	zabít	k5eAaPmAgMnS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Achilles	Achilles	k1gMnSc1	Achilles
jeho	jeho	k3xOp3gFnSc4	jeho
smrt	smrt	k1gFnSc4	smrt
nesl	nést	k5eAaImAgMnS	nést
těžce	těžce	k6eAd1	těžce
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
Trójanům	Trójan	k1gMnPc3	Trójan
krutě	krutě	k6eAd1	krutě
pomstít	pomstít	k5eAaPmF	pomstít
<g/>
.	.	kIx.	.
</s>
<s>
Bohové	bůh	k1gMnPc1	bůh
mu	on	k3xPp3gMnSc3	on
ukovali	ukovat	k5eAaPmAgMnP	ukovat
novou	nový	k2eAgFnSc4d1	nová
zbroj	zbroj	k1gFnSc4	zbroj
a	a	k8xC	a
hrdina	hrdina	k1gMnSc1	hrdina
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
na	na	k7c4	na
bitevní	bitevní	k2eAgNnSc4d1	bitevní
pole	pole	k1gNnSc4	pole
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgInSc1	sám
pobil	pobít	k5eAaPmAgInS	pobít
mnoho	mnoho	k4c4	mnoho
trojských	trojský	k2eAgFnPc2d1	Trojská
<g/>
,	,	kIx,	,
zahnal	zahnat	k5eAaPmAgMnS	zahnat
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
městu	město	k1gNnSc3	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
utkal	utkat	k5eAaPmAgMnS	utkat
s	s	k7c7	s
Hektórem	Hektór	k1gInSc7	Hektór
v	v	k7c6	v
souboji	souboj	k1gInSc6	souboj
muže	muž	k1gMnSc2	muž
proti	proti	k7c3	proti
muži	muž	k1gMnSc3	muž
<g/>
.	.	kIx.	.
</s>
<s>
Souboj	souboj	k1gInSc1	souboj
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
velmi	velmi	k6eAd1	velmi
vyrovnaný	vyrovnaný	k2eAgInSc1d1	vyrovnaný
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Achilles	Achilles	k1gMnSc1	Achilles
byl	být	k5eAaImAgMnS	být
poháněn	poháněn	k2eAgInSc4d1	poháněn
vztekem	vztek	k1gInSc7	vztek
a	a	k8xC	a
touhou	touha	k1gFnSc7	touha
pomstít	pomstít	k5eAaPmF	pomstít
přítele	přítel	k1gMnSc4	přítel
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
Hektóra	Hektóra	k1gMnSc1	Hektóra
snadno	snadno	k6eAd1	snadno
porazil	porazit	k5eAaPmAgMnS	porazit
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc4	jeho
mrtvolu	mrtvola	k1gFnSc4	mrtvola
zneuctil	zneuctít	k5eAaPmAgMnS	zneuctít
a	a	k8xC	a
za	za	k7c7	za
vozem	vůz	k1gInSc7	vůz
ji	on	k3xPp3gFnSc4	on
odtáhl	odtáhnout	k5eAaPmAgMnS	odtáhnout
do	do	k7c2	do
řeckého	řecký	k2eAgNnSc2d1	řecké
tábořiště	tábořiště	k1gNnSc2	tábořiště
<g/>
.	.	kIx.	.
</s>
<s>
Achillea	Achilleus	k1gMnSc4	Achilleus
však	však	k9	však
čekala	čekat	k5eAaImAgFnS	čekat
brzká	brzký	k2eAgFnSc1d1	brzká
smrt	smrt	k1gFnSc1	smrt
(	(	kIx(	(
<g/>
kterou	který	k3yQgFnSc4	který
už	už	k6eAd1	už
ovšem	ovšem	k9	ovšem
Homérův	Homérův	k2eAgInSc4d1	Homérův
epos	epos	k1gInSc4	epos
nezachycuje	zachycovat	k5eNaImIp3nS	zachycovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
zatlačit	zatlačit	k5eAaPmF	zatlačit
Trójany	Trójan	k1gMnPc4	Trójan
až	až	k9	až
k	k	k7c3	k
městu	město	k1gNnSc3	město
<g/>
,	,	kIx,	,
dospěl	dochvít	k5eAaPmAgMnS	dochvít
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
svojí	svojit	k5eAaImIp3nS	svojit
nedlouhé	dlouhý	k2eNgNnSc1d1	nedlouhé
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
slavné	slavný	k2eAgFnSc2d1	slavná
životní	životní	k2eAgFnSc2d1	životní
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Paridův	Paridův	k2eAgInSc1d1	Paridův
šíp	šíp	k1gInSc1	šíp
<g/>
,	,	kIx,	,
řízený	řízený	k2eAgInSc1d1	řízený
samotným	samotný	k2eAgMnSc7d1	samotný
Apollónem	Apollón	k1gMnSc7	Apollón
<g/>
,	,	kIx,	,
jej	on	k3xPp3gMnSc4	on
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
do	do	k7c2	do
paty	pata	k1gFnSc2	pata
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
jediným	jediný	k2eAgNnSc7d1	jediné
zranitelným	zranitelný	k2eAgNnSc7d1	zranitelné
místem	místo	k1gNnSc7	místo
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
těle	tělo	k1gNnSc6	tělo
<g/>
,	,	kIx,	,
a	a	k8xC	a
Achilles	Achilles	k1gMnSc1	Achilles
pak	pak	k6eAd1	pak
v	v	k7c6	v
mukách	muka	k1gFnPc6	muka
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
bitevním	bitevní	k2eAgNnSc6d1	bitevní
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
okamžiky	okamžik	k1gInPc1	okamžik
města	město	k1gNnSc2	město
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
dobytí	dobytí	k1gNnSc4	dobytí
už	už	k6eAd1	už
Homér	Homér	k1gMnSc1	Homér
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
Iliadě	Iliada	k1gFnSc6	Iliada
nepopisuje	popisovat	k5eNaImIp3nS	popisovat
<g/>
.	.	kIx.	.
</s>
<s>
Básnické	básnický	k2eAgNnSc1d1	básnické
líčení	líčení	k1gNnSc1	líčení
Trojského	trojský	k2eAgMnSc2d1	trojský
koně	kůň	k1gMnSc2	kůň
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
až	až	k9	až
ve	v	k7c6	v
Vergiliově	Vergiliův	k2eAgFnSc6d1	Vergiliova
Aeneis	Aeneis	k1gFnSc1	Aeneis
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pověstí	pověst	k1gFnPc2	pověst
přinesla	přinést	k5eAaPmAgFnS	přinést
zkázu	zkáza	k1gFnSc4	zkáza
města	město	k1gNnSc2	město
lest	lest	k1gFnSc1	lest
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vnukla	vnuknout	k5eAaPmAgFnS	vnuknout
bohyně	bohyně	k1gFnSc1	bohyně
Pallas	Pallas	k1gMnSc1	Pallas
Athéna	Athéna	k1gFnSc1	Athéna
jistému	jistý	k2eAgMnSc3d1	jistý
Prylidovi	Prylid	k1gMnSc3	Prylid
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
postavili	postavit	k5eAaPmAgMnP	postavit
z	z	k7c2	z
lodních	lodní	k2eAgMnPc2d1	lodní
trupů	trup	k1gMnPc2	trup
obrovského	obrovský	k2eAgMnSc2d1	obrovský
dutého	dutý	k2eAgMnSc2d1	dutý
dřevěného	dřevěný	k2eAgMnSc2d1	dřevěný
koně	kůň	k1gMnSc2	kůň
a	a	k8xC	a
zasvětili	zasvětit	k5eAaPmAgMnP	zasvětit
ho	on	k3xPp3gMnSc4	on
Athéně	Athéna	k1gFnSc6	Athéna
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
sklidili	sklidit	k5eAaPmAgMnP	sklidit
tábořiště	tábořiště	k1gNnSc4	tábořiště
a	a	k8xC	a
odpluli	odplout	k5eAaPmAgMnP	odplout
s	s	k7c7	s
loděmi	loď	k1gFnPc7	loď
do	do	k7c2	do
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nebyly	být	k5eNaImAgFnP	být
z	z	k7c2	z
břehu	břeh	k1gInSc2	břeh
vidět	vidět	k5eAaImF	vidět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
jen	jen	k9	jen
několik	několik	k4yIc1	několik
vojáků	voják	k1gMnPc2	voják
(	(	kIx(	(
<g/>
jejich	jejich	k3xOp3gNnSc4	jejich
množství	množství	k1gNnSc4	množství
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
výkladech	výklad	k1gInPc6	výklad
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nejpravděpodobnější	pravděpodobný	k2eAgMnSc1d3	nejpravděpodobnější
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
20-30	[number]	k4	20-30
mužů	muž	k1gMnPc2	muž
<g/>
)	)	kIx)	)
skrytých	skrytý	k2eAgInPc2d1	skrytý
v	v	k7c6	v
útrobách	útroba	k1gFnPc6	útroba
dřevěného	dřevěný	k2eAgMnSc2d1	dřevěný
koně	kůň	k1gMnSc2	kůň
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
zvěd	zvěd	k1gMnSc1	zvěd
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
zapálit	zapálit	k5eAaPmF	zapálit
pochodeň	pochodeň	k1gFnSc4	pochodeň
na	na	k7c6	na
znamení	znamení	k1gNnSc6	znamení
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
koně	kůň	k1gMnSc4	kůň
dostat	dostat	k5eAaPmF	dostat
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Bázliví	bázlivý	k2eAgMnPc1d1	bázlivý
Trójané	Trójan	k1gMnPc1	Trójan
pak	pak	k6eAd1	pak
v	v	k7c6	v
radosti	radost	k1gFnSc6	radost
z	z	k7c2	z
vítězství	vítězství	k1gNnSc2	vítězství
vtáhli	vtáhnout	k5eAaPmAgMnP	vtáhnout
koně	kůň	k1gMnPc1	kůň
za	za	k7c2	za
hradby	hradba	k1gFnSc2	hradba
<g/>
,	,	kIx,	,
považujíce	považovat	k5eAaImSgMnP	považovat
ho	on	k3xPp3gMnSc4	on
za	za	k7c4	za
dar	dar	k1gInSc4	dar
na	na	k7c4	na
usmířenou	usmířená	k1gFnSc4	usmířená
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bouřlivých	bouřlivý	k2eAgFnPc6d1	bouřlivá
oslavách	oslava	k1gFnPc6	oslava
město	město	k1gNnSc1	město
tvrdě	tvrdě	k6eAd1	tvrdě
usnulo	usnout	k5eAaPmAgNnS	usnout
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Achájci	Achájce	k1gMnPc1	Achájce
vylezli	vylézt	k5eAaPmAgMnP	vylézt
padacími	padací	k2eAgFnPc7d1	padací
dveřmi	dveře	k1gFnPc7	dveře
v	v	k7c6	v
koňském	koňský	k2eAgInSc6d1	koňský
břiše	břich	k1gInSc6	břich
ven	ven	k6eAd1	ven
<g/>
,	,	kIx,	,
dali	dát	k5eAaPmAgMnP	dát
znamení	znamení	k1gNnSc4	znamení
lodím	lodit	k5eAaImIp1nS	lodit
a	a	k8xC	a
otevřeli	otevřít	k5eAaPmAgMnP	otevřít
městské	městský	k2eAgFnPc4d1	městská
brány	brána	k1gFnPc4	brána
pro	pro	k7c4	pro
spojence	spojenec	k1gMnPc4	spojenec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
znovu	znovu	k6eAd1	znovu
vylodili	vylodit	k5eAaPmAgMnP	vylodit
<g/>
.	.	kIx.	.
</s>
<s>
Společnými	společný	k2eAgFnPc7d1	společná
silami	síla	k1gFnPc7	síla
pak	pak	k8xC	pak
rozespalé	rozespalý	k2eAgNnSc4d1	rozespalé
město	město	k1gNnSc4	město
pobili	pobít	k5eAaPmAgMnP	pobít
a	a	k8xC	a
zapálili	zapálit	k5eAaPmAgMnP	zapálit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
brát	brát	k5eAaImF	brát
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
<g/>
,	,	kIx,	,
že	že	k8xS	že
Homér	Homér	k1gMnSc1	Homér
byl	být	k5eAaImAgMnS	být
především	především	k9	především
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
stál	stát	k5eAaImAgMnS	stát
i	i	k9	i
svým	svůj	k3xOyFgInSc7	svůj
způsobem	způsob	k1gInSc7	způsob
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
dějepisectví	dějepisectví	k1gNnSc2	dějepisectví
svými	svůj	k3xOyFgMnPc7	svůj
záznamy	záznam	k1gInPc1	záznam
historických	historický	k2eAgInPc2d1	historický
dějů	děj	k1gInPc2	děj
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
s	s	k7c7	s
mytologickým	mytologický	k2eAgInSc7d1	mytologický
nátěrem	nátěr	k1gInSc7	nátěr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
díle	díl	k1gInSc6	díl
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
mnoho	mnoho	k4c4	mnoho
podstatných	podstatný	k2eAgInPc2d1	podstatný
detailů	detail	k1gInPc2	detail
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nám	my	k3xPp1nPc3	my
pomohou	pomoct	k5eAaPmIp3nP	pomoct
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
některé	některý	k3yIgFnPc4	některý
skutečnosti	skutečnost	k1gFnPc4	skutečnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
o	o	k7c6	o
přesnosti	přesnost	k1gFnSc6	přesnost
jeho	jeho	k3xOp3gFnPc2	jeho
výpovědí	výpověď	k1gFnPc2	výpověď
svědčí	svědčit	k5eAaImIp3nS	svědčit
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
dle	dle	k7c2	dle
jeho	on	k3xPp3gInSc2	on
eposu	epos	k1gInSc2	epos
Schliemann	Schliemann	k1gMnSc1	Schliemann
přesně	přesně	k6eAd1	přesně
lokalizoval	lokalizovat	k5eAaBmAgMnS	lokalizovat
Tróju	Trója	k1gFnSc4	Trója
<g/>
.	.	kIx.	.
</s>
<s>
Homér	Homér	k1gMnSc1	Homér
nebyl	být	k5eNaImAgMnS	být
současníkem	současník	k1gMnSc7	současník
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
o	o	k7c6	o
nichž	jenž	k3xRgInPc6	jenž
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
příběhy	příběh	k1gInPc4	příběh
o	o	k7c6	o
trojské	trojský	k2eAgFnSc6d1	Trojská
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
tradované	tradovaný	k2eAgFnPc1d1	tradovaná
pouze	pouze	k6eAd1	pouze
ústně	ústně	k6eAd1	ústně
<g/>
)	)	kIx)	)
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
při	při	k7c6	při
každé	každý	k3xTgFnSc6	každý
nové	nový	k2eAgFnSc6d1	nová
interpretaci	interpretace	k1gFnSc6	interpretace
rozšířeny	rozšířen	k2eAgMnPc4d1	rozšířen
a	a	k8xC	a
mistrovský	mistrovský	k2eAgMnSc1d1	mistrovský
básník	básník	k1gMnSc1	básník
jejich	jejich	k3xOp3gFnSc4	jejich
podobu	podoba	k1gFnSc4	podoba
ještě	ještě	k6eAd1	ještě
učinil	učinit	k5eAaImAgInS	učinit
košatější	košatý	k2eAgInSc1d2	košatější
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
pouze	pouze	k6eAd1	pouze
konstatovat	konstatovat	k5eAaBmF	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
fakta	faktum	k1gNnPc4	faktum
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc4	který
Homér	Homér	k1gMnSc1	Homér
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
těžko	těžko	k6eAd1	těžko
oddělitelná	oddělitelný	k2eAgFnSc1d1	oddělitelná
od	od	k7c2	od
mýtického	mýtický	k2eAgInSc2d1	mýtický
podkladu	podklad	k1gInSc2	podklad
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
díla	dílo	k1gNnSc2	dílo
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
Homérově	Homérův	k2eAgNnSc6d1	Homérovo
úžasném	úžasný	k2eAgNnSc6d1	úžasné
básnickém	básnický	k2eAgNnSc6d1	básnické
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
velice	velice	k6eAd1	velice
obratně	obratně	k6eAd1	obratně
líčí	líčit	k5eAaImIp3nS	líčit
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
příběhy	příběh	k1gInPc4	příběh
zúčastněných	zúčastněný	k2eAgFnPc2d1	zúčastněná
a	a	k8xC	a
události	událost	k1gFnPc1	událost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
době	doba	k1gFnSc6	doba
hýbaly	hýbat	k5eAaImAgInP	hýbat
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
<s>
Homér	Homér	k1gMnSc1	Homér
<g/>
.	.	kIx.	.
</s>
<s>
Ílias	Ílias	k1gInSc1	Ílias
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
GRAVES	GRAVES	kA	GRAVES
<g/>
.	.	kIx.	.
</s>
<s>
Řecké	řecký	k2eAgInPc1d1	řecký
mýty	mýtus	k1gInPc1	mýtus
II	II	kA	II
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Alain	Alain	k1gMnSc1	Alain
QUESNEL	QUESNEL	kA	QUESNEL
<g/>
.	.	kIx.	.
</s>
<s>
Mýty	mýtus	k1gInPc1	mýtus
a	a	k8xC	a
legendy	legenda	k1gFnPc1	legenda
<g/>
:	:	kIx,	:
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
Galie	Galie	k1gFnSc1	Galie
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
GEMINI	GEMINI	kA	GEMINI
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Margaret	Margareta	k1gFnPc2	Margareta
OLIPHANT	OLIPHANT	kA	OLIPHANT
<g/>
.	.	kIx.	.
</s>
<s>
Atlas	Atlas	k1gInSc1	Atlas
starověkého	starověký	k2eAgInSc2d1	starověký
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
GEMINI	GEMINI	kA	GEMINI
1993	[number]	k4	1993
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Trojská	trojský	k2eAgFnSc1d1	Trojská
válka	válka	k1gFnSc1	válka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Dílo	dílo	k1gNnSc1	dílo
Ilias	Ilias	k1gFnSc1	Ilias
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
Dílo	dílo	k1gNnSc1	dílo
Sepsání	sepsání	k1gNnSc1	sepsání
války	válka	k1gFnSc2	válka
peloponnesské	peloponnesská	k1gFnSc2	peloponnesská
<g/>
/	/	kIx~	/
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
Dílo	dílo	k1gNnSc1	dílo
Sepsání	sepsání	k1gNnSc1	sepsání
války	válka	k1gFnSc2	válka
peloponnesské	peloponnesská	k1gFnSc2	peloponnesská
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
Dílo	dílo	k1gNnSc1	dílo
Sepsání	sepsání	k1gNnSc1	sepsání
války	válka	k1gFnSc2	válka
peloponnesské	peloponnesská	k1gFnSc2	peloponnesská
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
