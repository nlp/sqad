<s>
Trojská	trojský	k2eAgFnSc1d1	Trojská
válka	válka	k1gFnSc1	válka
je	být	k5eAaImIp3nS	být
řecká	řecký	k2eAgFnSc1d1	řecká
báje	báje	k1gFnSc1	báje
o	o	k7c6	o
dobývání	dobývání	k1gNnSc6	dobývání
města	město	k1gNnSc2	město
Tróje	Trója	k1gFnSc2	Trója
řeckými	řecký	k2eAgMnPc7d1	řecký
bojovníky	bojovník	k1gMnPc7	bojovník
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	on	k3xPp3gMnPc4	on
vylíčil	vylíčit	k5eAaPmAgMnS	vylíčit
básník	básník	k1gMnSc1	básník
Homér	Homér	k1gMnSc1	Homér
v	v	k7c6	v
eposu	epos	k1gInSc6	epos
Ilias	Ilias	k1gFnSc1	Ilias
<g/>
.	.	kIx.	.
</s>
