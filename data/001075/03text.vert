<s>
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Ekvádorská	ekvádorský	k2eAgFnSc1d1	ekvádorská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
Ecuador	Ecuador	k1gInSc1	Ecuador
nebo	nebo	k8xC	nebo
República	República	k1gFnSc1	República
del	del	k?	del
Ecuador	Ecuador	k1gInSc1	Ecuador
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaImF	stát
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnPc2	jeho
pobřeží	pobřeží	k1gNnPc2	pobřeží
omýváno	omývat	k5eAaImNgNnS	omývat
vodami	voda	k1gFnPc7	voda
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
sousedy	soused	k1gMnPc7	soused
jsou	být	k5eAaImIp3nP	být
Kolumbie	Kolumbie	k1gFnPc1	Kolumbie
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
Peru	Peru	k1gNnSc6	Peru
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
Galapágy	Galapágy	k1gFnPc1	Galapágy
<g/>
.	.	kIx.	.
</s>
<s>
Španělské	španělský	k2eAgNnSc1d1	španělské
slovo	slovo	k1gNnSc1	slovo
Ecuador	Ecuador	k1gInSc1	Ecuador
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
rovník	rovník	k1gInSc1	rovník
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
vodorovných	vodorovný	k2eAgInPc2d1	vodorovný
pruhů	pruh	k1gInPc2	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Vrchní	vrchní	k2eAgInSc1d1	vrchní
pruh	pruh	k1gInSc1	pruh
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
na	na	k7c6	na
vrchní	vrchní	k2eAgFnSc6d1	vrchní
polovině	polovina	k1gFnSc6	polovina
vlajky	vlajka	k1gFnSc2	vlajka
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
barvu	barva	k1gFnSc4	barva
žlutou	žlutý	k2eAgFnSc4d1	žlutá
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
spodní	spodní	k2eAgFnSc4d1	spodní
polovinu	polovina	k1gFnSc4	polovina
vlajky	vlajka	k1gFnSc2	vlajka
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
barva	barva	k1gFnSc1	barva
modrá	modrý	k2eAgFnSc1d1	modrá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
.	.	kIx.	.
</s>
<s>
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
symbolizují	symbolizovat	k5eAaImIp3nP	symbolizovat
vlajku	vlajka	k1gFnSc4	vlajka
Španělskou	španělský	k2eAgFnSc4d1	španělská
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Španělsko	Španělsko	k1gNnSc1	Španělsko
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
kolonizovalo	kolonizovat	k5eAaBmAgNnS	kolonizovat
<g/>
.	.	kIx.	.
</s>
<s>
Modrá	modrý	k2eAgFnSc1d1	modrá
má	mít	k5eAaImIp3nS	mít
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
oceán	oceán	k1gInSc1	oceán
kterým	který	k3yQgInSc7	který
je	být	k5eAaImIp3nS	být
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
a	a	k8xC	a
Španělsko	Španělsko	k1gNnSc1	Španělsko
odděleno	oddělit	k5eAaPmNgNnS	oddělit
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
modré	modrý	k2eAgFnSc2d1	modrá
části	část	k1gFnSc2	část
a	a	k8xC	a
kouskem	kousek	k1gInSc7	kousek
do	do	k7c2	do
žluté	žlutý	k2eAgFnSc2d1	žlutá
části	část	k1gFnSc2	část
pak	pak	k6eAd1	pak
ještě	ještě	k6eAd1	ještě
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
ekvádorský	ekvádorský	k2eAgInSc1d1	ekvádorský
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
obrázek	obrázek	k1gInSc4	obrázek
znázornující	znázornující	k2eAgInSc4d1	znázornující
čistou	čistý	k2eAgFnSc4d1	čistá
krajinu	krajina	k1gFnSc4	krajina
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
obrázku	obrázek	k1gInSc2	obrázek
jsou	být	k5eAaImIp3nP	být
větvičky	větvička	k1gFnPc1	větvička
a	a	k8xC	a
na	na	k7c6	na
obrázku	obrázek	k1gInSc6	obrázek
sedí	sedit	k5eAaImIp3nS	sedit
kondor	kondor	k1gMnSc1	kondor
<g/>
.	.	kIx.	.
</s>
<s>
Ekvádorská	ekvádorský	k2eAgFnSc1d1	ekvádorská
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
podobná	podobný	k2eAgFnSc1d1	podobná
vlajce	vlajka	k1gFnSc3	vlajka
kolumbijské	kolumbijský	k2eAgFnSc3d1	kolumbijská
a	a	k8xC	a
venezuelské	venezuelský	k2eAgFnSc3d1	venezuelská
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
<g/>
.	.	kIx.	.
</s>
<s>
Indiánský	indiánský	k2eAgInSc1d1	indiánský
stát	stát	k1gInSc1	stát
na	na	k7c6	na
území	území	k1gNnSc6	území
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
poražen	porazit	k5eAaPmNgMnS	porazit
vpádem	vpád	k1gInSc7	vpád
říše	říš	k1gFnSc2	říš
Inků	Ink	k1gMnPc2	Ink
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
centrum	centrum	k1gNnSc1	centrum
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Peru	Peru	k1gNnSc2	Peru
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k9	již
roku	rok	k1gInSc2	rok
1534	[number]	k4	1534
byl	být	k5eAaImAgInS	být
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
dobyt	dobyt	k2eAgInSc1d1	dobyt
Španěly	Španěl	k1gMnPc7	Španěl
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
indiány	indián	k1gMnPc4	indián
podmanili	podmanit	k5eAaPmAgMnP	podmanit
na	na	k7c4	na
300	[number]	k4	300
let	léto	k1gNnPc2	léto
a	a	k8xC	a
Ekvádor	Ekvádor	k1gInSc4	Ekvádor
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
součástí	součást	k1gFnSc7	součást
místokrálovství	místokrálovství	k1gNnSc2	místokrálovství
Peru	Peru	k1gNnSc2	Peru
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1717	[number]	k4	1717
byl	být	k5eAaImAgInS	být
připojen	připojit	k5eAaPmNgInS	připojit
k	k	k7c3	k
místokrálovství	místokrálovství	k1gNnSc3	místokrálovství
Nová	nový	k2eAgFnSc1d1	nová
Granada	Granada	k1gFnSc1	Granada
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1822	[number]	k4	1822
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
byl	být	k5eAaImAgInS	být
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
součástí	součást	k1gFnPc2	součást
Velké	velký	k2eAgFnSc2d1	velká
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
stal	stát	k5eAaPmAgInS	stát
samostatný	samostatný	k2eAgInSc1d1	samostatný
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
zažil	zažít	k5eAaPmAgMnS	zažít
Ekvádor	Ekvádor	k1gInSc4	Ekvádor
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Eloye	Eloy	k1gFnSc2	Eloy
Alfaroa	Alfaro	k1gInSc2	Alfaro
tzv.	tzv.	kA	tzv.
liberální	liberální	k2eAgFnSc4d1	liberální
revoluci	revoluce	k1gFnSc4	revoluce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
snížila	snížit	k5eAaPmAgFnS	snížit
vliv	vliv	k1gInSc4	vliv
církve	církev	k1gFnSc2	církev
a	a	k8xC	a
konzervativních	konzervativní	k2eAgMnPc2d1	konzervativní
velkovlastníků	velkovlastník	k1gMnPc2	velkovlastník
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
projevila	projevit	k5eAaPmAgFnS	projevit
politická	politický	k2eAgFnSc1d1	politická
destabilizace	destabilizace	k1gFnSc1	destabilizace
a	a	k8xC	a
časté	častý	k2eAgNnSc1d1	časté
střídání	střídání	k1gNnSc1	střídání
civilních	civilní	k2eAgFnPc2d1	civilní
a	a	k8xC	a
vojenských	vojenský	k2eAgFnPc2d1	vojenská
vlád	vláda	k1gFnPc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1940	[number]	k4	1940
až	až	k9	až
1942	[number]	k4	1942
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
neúspěšná	úspěšný	k2eNgFnSc1d1	neúspěšná
válka	válka	k1gFnSc1	válka
s	s	k7c7	s
Peru	Peru	k1gNnSc7	Peru
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc3	který
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
ztratil	ztratit	k5eAaPmAgInS	ztratit
část	část	k1gFnSc4	část
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
(	(	kIx(	(
<g/>
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
sousedil	sousedit	k5eAaImAgMnS	sousedit
s	s	k7c7	s
Brazílií	Brazílie	k1gFnSc7	Brazílie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1950	[number]	k4	1950
a	a	k8xC	a
1970	[number]	k4	1970
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
několik	několik	k4yIc1	několik
státních	státní	k2eAgInPc2d1	státní
převratů	převrat	k1gInPc2	převrat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
armádní	armádní	k2eAgFnSc1d1	armádní
junta	junta	k1gFnSc1	junta
provedla	provést	k5eAaPmAgFnS	provést
puč	puč	k1gInSc4	puč
vlády	vláda	k1gFnSc2	vláda
Velasca	Velascus	k1gMnSc2	Velascus
Ibarry	Ibarra	k1gMnSc2	Ibarra
a	a	k8xC	a
započala	započnout	k5eAaPmAgFnS	započnout
diktatura	diktatura	k1gFnSc1	diktatura
Guilermo	Guilerma	k1gFnSc5	Guilerma
Rodrígeze	Rodrígeze	k1gFnSc2	Rodrígeze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
byl	být	k5eAaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
civilní	civilní	k2eAgInSc1d1	civilní
demokratický	demokratický	k2eAgInSc1d1	demokratický
režim	režim	k1gInSc1	režim
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
zažil	zažít	k5eAaPmAgInS	zažít
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
krizi	krize	k1gFnSc4	krize
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
otevření	otevření	k1gNnSc1	otevření
se	se	k3xPyFc4	se
mezinárodnímu	mezinárodní	k2eAgInSc3d1	mezinárodní
obchodu	obchod	k1gInSc3	obchod
<g/>
,	,	kIx,	,
vzestupy	vzestup	k1gInPc4	vzestup
a	a	k8xC	a
poklesy	pokles	k1gInPc4	pokles
vlivu	vliv	k1gInSc2	vliv
církve	církev	k1gFnSc2	církev
<g/>
,	,	kIx,	,
opozice	opozice	k1gFnSc2	opozice
a	a	k8xC	a
poměr	poměr	k1gInSc1	poměr
sil	síla	k1gFnPc2	síla
mezi	mezi	k7c7	mezi
kapitány	kapitán	k1gMnPc7	kapitán
tamního	tamní	k2eAgInSc2d1	tamní
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
zástupci	zástupce	k1gMnPc1	zástupce
původního	původní	k2eAgNnSc2d1	původní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
událostí	událost	k1gFnPc2	událost
z	z	k7c2	z
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
současného	současný	k2eAgMnSc2d1	současný
prezidenta	prezident	k1gMnSc2	prezident
<g/>
,	,	kIx,	,
Rafaela	Rafael	k1gMnSc2	Rafael
Correy	Correa	k1gMnSc2	Correa
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
připomenout	připomenout	k5eAaPmF	připomenout
revoltu	revolta	k1gFnSc4	revolta
policistů	policista	k1gMnPc2	policista
z	z	k7c2	z
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
na	na	k7c4	na
týden	týden	k1gInSc4	týden
v	v	k7c6	v
chaosu	chaos	k1gInSc6	chaos
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
<g/>
.	.	kIx.	.
</s>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
různorodá	různorodý	k2eAgFnSc1d1	různorodá
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
částí	část	k1gFnPc2	část
(	(	kIx(	(
<g/>
západní	západní	k2eAgFnSc2d1	západní
<g/>
,	,	kIx,	,
centrální	centrální	k2eAgFnSc2d1	centrální
Andy	Anda	k1gFnSc2	Anda
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
výrazně	výrazně	k6eAd1	výrazně
liší	lišit	k5eAaImIp3nS	lišit
klimatem	klima	k1gNnSc7	klima
<g/>
,	,	kIx,	,
biotypem	biotyp	k1gInSc7	biotyp
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zvyky	zvyk	k1gInPc4	zvyk
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
24	[number]	k4	24
provincií	provincie	k1gFnPc2	provincie
a	a	k8xC	a
dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
3	[number]	k4	3
odlišné	odlišný	k2eAgInPc4d1	odlišný
geoklimatické	geoklimatický	k2eAgInPc4d1	geoklimatický
celky	celek	k1gInPc4	celek
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Pobřežní	pobřežní	k2eAgInSc1d1	pobřežní
pás	pás	k1gInSc1	pás
(	(	kIx(	(
<g/>
Costa	Costa	k1gFnSc1	Costa
<g/>
)	)	kIx)	)
-	-	kIx~	-
nížinný	nížinný	k2eAgMnSc1d1	nížinný
<g/>
,	,	kIx,	,
s	s	k7c7	s
deštnými	deštný	k2eAgInPc7d1	deštný
pralesy	prales	k1gInPc7	prales
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	s	k7c7	s
savanami	savana	k1gFnPc7	savana
<g/>
;	;	kIx,	;
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Horský	horský	k2eAgInSc1d1	horský
pás	pás	k1gInSc1	pás
And	Anda	k1gFnPc2	Anda
(	(	kIx(	(
<g/>
Sierra	Sierra	k1gFnSc1	Sierra
<g/>
)	)	kIx)	)
-	-	kIx~	-
pásma	pásmo	k1gNnPc1	pásmo
Cordillera	Cordiller	k1gMnSc2	Cordiller
Occidental	Occidental	k1gMnSc1	Occidental
a	a	k8xC	a
Cordillera	Cordiller	k1gMnSc2	Cordiller
Oriental	Oriental	k1gMnSc1	Oriental
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
pánve	pánev	k1gFnPc1	pánev
a	a	k8xC	a
náhorní	náhorní	k2eAgFnPc1d1	náhorní
plošiny	plošina	k1gFnPc1	plošina
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
2200	[number]	k4	2200
<g/>
-	-	kIx~	-
<g/>
2900	[number]	k4	2900
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Sopečné	sopečný	k2eAgInPc4d1	sopečný
vrcholy	vrchol	k1gInPc4	vrchol
<g/>
:	:	kIx,	:
vyhaslé	vyhaslý	k2eAgFnPc4d1	vyhaslá
Chimborazo	Chimboraza	k1gFnSc5	Chimboraza
(	(	kIx(	(
<g/>
6310	[number]	k4	6310
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
-	-	kIx~	-
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
pohoří	pohoří	k1gNnSc6	pohoří
činné	činný	k2eAgFnSc2d1	činná
sopky	sopka	k1gFnSc2	sopka
(	(	kIx(	(
<g/>
Cotopaxi	Cotopaxe	k1gFnSc4	Cotopaxe
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
aktivních	aktivní	k2eAgFnPc2d1	aktivní
sopek	sopka	k1gFnPc2	sopka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
-	-	kIx~	-
5897	[number]	k4	5897
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rostlinstvo	rostlinstvo	k1gNnSc1	rostlinstvo
uspořádáno	uspořádat	k5eAaPmNgNnS	uspořádat
do	do	k7c2	do
výškových	výškový	k2eAgInPc2d1	výškový
stupňů	stupeň	k1gInPc2	stupeň
<g/>
;	;	kIx,	;
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgInSc1d1	východní
pás	pás	k1gInSc1	pás
(	(	kIx(	(
<g/>
Oriente	Orient	k1gInSc5	Orient
<g/>
)	)	kIx)	)
-	-	kIx~	-
okraj	okraj	k1gInSc4	okraj
Amazonské	amazonský	k2eAgFnSc2d1	Amazonská
nížiny	nížina	k1gFnSc2	nížina
(	(	kIx(	(
<g/>
deštný	deštný	k2eAgInSc1d1	deštný
prales	prales	k1gInSc1	prales
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
35	[number]	k4	35
%	%	kIx~	%
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
lesními	lesní	k2eAgInPc7d1	lesní
porosty	porost	k1gInPc7	porost
<g/>
,	,	kIx,	,
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
půda	půda	k1gFnSc1	půda
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
28	[number]	k4	28
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
zde	zde	k6eAd1	zde
11	[number]	k4	11
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
(	(	kIx(	(
<g/>
stav	stav	k1gInSc1	stav
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Politika	politika	k1gFnSc1	politika
se	se	k3xPyFc4	se
v	v	k7c6	v
Ekvádoru	Ekvádor	k1gInSc6	Ekvádor
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
systému	systém	k1gInSc2	systém
zastupujícího	zastupující	k2eAgInSc2d1	zastupující
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
Ekvádoru	Ekvádor	k1gInSc6	Ekvádor
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
i	i	k9	i
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
pluralitní	pluralitní	k2eAgInSc4d1	pluralitní
systém	systém	k1gInSc4	systém
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Výkonnou	výkonný	k2eAgFnSc4d1	výkonná
moc	moc	k1gFnSc4	moc
představuje	představovat	k5eAaImIp3nS	představovat
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Legislativní	legislativní	k2eAgFnSc1d1	legislativní
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
svěřena	svěřen	k2eAgFnSc1d1	svěřena
vládě	vláda	k1gFnSc3	vláda
i	i	k8xC	i
Národnímu	národní	k2eAgInSc3d1	národní
kongresu	kongres	k1gInSc3	kongres
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
<g/>
.	.	kIx.	.
</s>
<s>
Soudnictví	soudnictví	k1gNnSc1	soudnictví
je	být	k5eAaImIp3nS	být
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
na	na	k7c6	na
výkonné	výkonný	k2eAgFnSc6d1	výkonná
moci	moc	k1gFnSc6	moc
a	a	k8xC	a
zákonodárného	zákonodárný	k2eAgInSc2d1	zákonodárný
sboru	sbor	k1gInSc2	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
často	často	k6eAd1	často
klade	klást	k5eAaImIp3nS	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
multilaterální	multilaterální	k2eAgInSc4d1	multilaterální
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
mezinárodním	mezinárodní	k2eAgFnPc3d1	mezinárodní
otázkám	otázka	k1gFnPc3	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Sandro	Sandra	k1gFnSc5	Sandra
Sarang	Sarang	k1gMnSc1	Sarang
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
(	(	kIx(	(
<g/>
a	a	k8xC	a
většiny	většina	k1gFnSc2	většina
jejich	jejich	k3xOp3gFnPc2	jejich
specializovaných	specializovaný	k2eAgFnPc2d1	specializovaná
agentur	agentura	k1gFnPc2	agentura
<g/>
)	)	kIx)	)
a	a	k8xC	a
členem	člen	k1gMnSc7	člen
mnoha	mnoho	k4c2	mnoho
regionálních	regionální	k2eAgNnPc2d1	regionální
uskupení	uskupení	k1gNnPc2	uskupení
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
organizací	organizace	k1gFnPc2	organizace
jako	jako	k8xS	jako
skupina	skupina	k1gFnSc1	skupina
z	z	k7c2	z
Ria	Ria	k1gFnSc2	Ria
<g/>
,	,	kIx,	,
Latinskoamerický	latinskoamerický	k2eAgInSc1d1	latinskoamerický
ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
Latinsko-americké	latinskomerický	k2eAgFnSc2d1	latinsko-americká
energetické	energetický	k2eAgFnSc2d1	energetická
organizace	organizace	k1gFnSc2	organizace
<g/>
,	,	kIx,	,
Latinskoamerické	latinskoamerický	k2eAgNnSc1d1	latinskoamerické
integrační	integrační	k2eAgNnSc1d1	integrační
sdružení	sdružení	k1gNnSc1	sdružení
a	a	k8xC	a
Andský	andský	k2eAgInSc1d1	andský
pakt	pakt	k1gInSc1	pakt
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
komunistickým	komunistický	k2eAgNnSc7d1	komunistické
Československem	Československo	k1gNnSc7	Československo
byly	být	k5eAaImAgFnP	být
2	[number]	k4	2
<g/>
×	×	k?	×
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
narušeny	narušen	k2eAgInPc1d1	narušen
vyhoštěním	vyhoštění	k1gNnSc7	vyhoštění
československých	československý	k2eAgInPc2d1	československý
diplomatů	diplomat	k1gInPc2	diplomat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
ukončení	ukončení	k1gNnSc2	ukončení
diplomatických	diplomatický	k2eAgInPc2d1	diplomatický
vztahů	vztah	k1gInPc2	vztah
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
s	s	k7c7	s
ukončením	ukončení	k1gNnSc7	ukončení
diplomatických	diplomatický	k2eAgInPc2d1	diplomatický
vztahů	vztah	k1gInPc2	vztah
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
diplomatické	diplomatický	k2eAgInPc1d1	diplomatický
vztahy	vztah	k1gInPc1	vztah
navázány	navázán	k2eAgInPc1d1	navázán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ČR	ČR	kA	ČR
v	v	k7c6	v
Quitu	Quit	k1gInSc6	Quit
nemá	mít	k5eNaImIp3nS	mít
velvyslanectví	velvyslanectví	k1gNnSc1	velvyslanectví
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
měly	mít	k5eAaImAgInP	mít
na	na	k7c4	na
pobřeží	pobřeží	k1gNnSc4	pobřeží
země	zem	k1gFnSc2	zem
leteckou	letecký	k2eAgFnSc4d1	letecká
základnu	základna	k1gFnSc4	základna
Eloy	Eloa	k1gFnSc2	Eloa
Alfaro	Alfara	k1gFnSc5	Alfara
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
současný	současný	k2eAgMnSc1d1	současný
prezident	prezident	k1gMnSc1	prezident
ji	on	k3xPp3gFnSc4	on
7	[number]	k4	7
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2009	[number]	k4	2009
nařídil	nařídit	k5eAaPmAgMnS	nařídit
zrušit	zrušit	k5eAaPmF	zrušit
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
afér	aféra	k1gFnPc2	aféra
WikiLeaks	WikiLeaksa	k1gFnPc2	WikiLeaksa
označil	označit	k5eAaPmAgInS	označit
americkou	americký	k2eAgFnSc4d1	americká
ambasadorku	ambasadorka	k1gFnSc4	ambasadorka
Heather	Heathra	k1gFnPc2	Heathra
Hodges	Hodgesa	k1gFnPc2	Hodgesa
persona	persona	k1gFnSc1	persona
non	non	k?	non
grata	grata	k1gFnSc1	grata
<g/>
.	.	kIx.	.
</s>
<s>
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
poskytl	poskytnout	k5eAaPmAgInS	poskytnout
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
velvyslanectví	velvyslanectví	k1gNnSc6	velvyslanectví
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
azyl	azyl	k1gInSc4	azyl
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
WikiLeaks	WikiLeaks	k1gInSc4	WikiLeaks
Julianu	Julian	k1gMnSc3	Julian
Assangemu	Assangem	k1gMnSc3	Assangem
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejnapjatější	napjatý	k2eAgInPc4d3	nejnapjatější
zahraniční	zahraniční	k2eAgInPc4d1	zahraniční
vztahy	vztah	k1gInPc4	vztah
má	mít	k5eAaImIp3nS	mít
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
s	s	k7c7	s
Kolumbií	Kolumbie	k1gFnSc7	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2008	[number]	k4	2008
dokonce	dokonce	k9	dokonce
propukla	propuknout	k5eAaPmAgFnS	propuknout
"	"	kIx"	"
<g/>
andská	andský	k2eAgFnSc1d1	andská
diplomatická	diplomatický	k2eAgFnSc1d1	diplomatická
krize	krize	k1gFnSc1	krize
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
v	v	k7c6	v
hraničních	hraniční	k2eAgFnPc6d1	hraniční
oblastech	oblast	k1gFnPc6	oblast
obou	dva	k4xCgInPc2	dva
zemí	zem	k1gFnPc2	zem
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
střetnutím	střetnutí	k1gNnSc7	střetnutí
kolumbijských	kolumbijský	k2eAgFnPc2d1	kolumbijská
armádních	armádní	k2eAgFnPc2d1	armádní
jednotek	jednotka	k1gFnPc2	jednotka
a	a	k8xC	a
tamních	tamní	k2eAgFnPc2d1	tamní
guerrilových	guerrilův	k2eAgFnPc2d1	guerrilův
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
24	[number]	k4	24
provincií	provincie	k1gFnPc2	provincie
(	(	kIx(	(
<g/>
provincia	provincia	k1gFnSc1	provincia
<g/>
,	,	kIx,	,
množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
provincias	provinciasa	k1gFnPc2	provinciasa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
<g/>
:	:	kIx,	:
Provincie	provincie	k1gFnSc1	provincie
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
199	[number]	k4	199
kantonů	kanton	k1gInPc2	kanton
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
problémem	problém	k1gInSc7	problém
současného	současný	k2eAgInSc2d1	současný
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
je	být	k5eAaImIp3nS	být
chudoba	chudoba	k1gFnSc1	chudoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
prostupuje	prostupovat	k5eAaImIp3nS	prostupovat
celou	celá	k1gFnSc4	celá
zemí	zem	k1gFnPc2	zem
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
řadil	řadit	k5eAaImAgMnS	řadit
mezi	mezi	k7c4	mezi
chudé	chudý	k2eAgInPc4d1	chudý
státy	stát	k1gInPc4	stát
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
a	a	k8xC	a
lepší	dobrý	k2eAgFnSc3d2	lepší
situaci	situace	k1gFnSc3	situace
nepřineslo	přinést	k5eNaPmAgNnS	přinést
ani	ani	k8xC	ani
nalezení	nalezení	k1gNnSc1	nalezení
ropy	ropa	k1gFnSc2	ropa
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
sice	sice	k8xC	sice
přineslo	přinést	k5eAaPmAgNnS	přinést
částečný	částečný	k2eAgInSc4d1	částečný
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
růst	růst	k1gInSc4	růst
a	a	k8xC	a
zlepšení	zlepšení	k1gNnSc4	zlepšení
situace	situace	k1gFnSc2	situace
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
celkově	celkově	k6eAd1	celkově
země	zem	k1gFnPc1	zem
výrazně	výrazně	k6eAd1	výrazně
hospodářsky	hospodářsky	k6eAd1	hospodářsky
neposílila	posílit	k5eNaPmAgFnS	posílit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Ekvádor	Ekvádor	k1gInSc4	Ekvádor
měl	mít	k5eAaImAgInS	mít
katastrofální	katastrofální	k2eAgInSc4d1	katastrofální
dopad	dopad	k1gInSc4	dopad
průběh	průběh	k1gInSc4	průběh
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
recese	recese	k1gFnSc2	recese
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
jako	jako	k9	jako
na	na	k7c4	na
většinu	většina	k1gFnSc4	většina
zemí	zem	k1gFnPc2	zem
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
inflace	inflace	k1gFnSc2	inflace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
76	[number]	k4	76
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
dekádě	dekáda	k1gFnSc6	dekáda
se	se	k3xPyFc4	se
však	však	k9	však
podařilo	podařit	k5eAaPmAgNnS	podařit
vládě	vláda	k1gFnSc3	vláda
situaci	situace	k1gFnSc3	situace
částečně	částečně	k6eAd1	částečně
zlepšit	zlepšit	k5eAaPmF	zlepšit
a	a	k8xC	a
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1995	[number]	k4	1995
a	a	k8xC	a
1997	[number]	k4	1997
se	se	k3xPyFc4	se
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
hodnot	hodnota	k1gFnPc2	hodnota
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
ohromnému	ohromný	k2eAgNnSc3d1	ohromné
zadlužení	zadlužení	k1gNnSc3	zadlužení
země	zem	k1gFnSc2	zem
v	v	k7c6	v
cizině	cizina	k1gFnSc6	cizina
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
prognóz	prognóza	k1gFnPc2	prognóza
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
zásoby	zásoba	k1gFnPc4	zásoba
ropy	ropa	k1gFnSc2	ropa
vyčerpány	vyčerpat	k5eAaPmNgFnP	vyčerpat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ještě	ještě	k6eAd1	ještě
přitížílo	přitížínout	k5eAaPmAgNnS	přitížínout
slabému	slabý	k2eAgNnSc3d1	slabé
hospodářství	hospodářství	k1gNnSc3	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nezlepšuje	zlepšovat	k5eNaImIp3nS	zlepšovat
ani	ani	k8xC	ani
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
1	[number]	k4	1
%	%	kIx~	%
bohatých	bohatý	k2eAgMnPc2d1	bohatý
statkářů	statkář	k1gMnPc2	statkář
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
přes	přes	k7c4	přes
40	[number]	k4	40
%	%	kIx~	%
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
rodinných	rodinný	k2eAgFnPc2d1	rodinná
farem	farma	k1gFnPc2	farma
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgFnPc1d2	menší
než	než	k8xS	než
5	[number]	k4	5
ha	ha	kA	ha
-	-	kIx~	-
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
příliš	příliš	k6eAd1	příliš
malé	malý	k2eAgFnPc4d1	malá
farmy	farma	k1gFnPc4	farma
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
uživit	uživit	k5eAaPmF	uživit
ani	ani	k8xC	ani
farmářské	farmářský	k2eAgFnPc4d1	farmářská
rodiny	rodina	k1gFnPc4	rodina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zapříčiňuje	zapříčiňovat	k5eAaImIp3nS	zapříčiňovat
odliv	odliv	k1gInSc1	odliv
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
do	do	k7c2	do
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Vidina	vidina	k1gFnSc1	vidina
snazšího	snadný	k2eAgNnSc2d2	snazší
bohatství	bohatství	k1gNnSc2	bohatství
však	však	k9	však
často	často	k6eAd1	často
končí	končit	k5eAaImIp3nS	končit
v	v	k7c6	v
příměstských	příměstský	k2eAgInPc6d1	příměstský
slumech	slum	k1gInPc6	slum
například	například	k6eAd1	například
v	v	k7c6	v
Guayaquilu	Guayaquil	k1gInSc6	Guayaquil
<g/>
.	.	kIx.	.
</s>
<s>
Tíživou	tíživý	k2eAgFnSc4d1	tíživá
situaci	situace	k1gFnSc4	situace
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
umocňuje	umocňovat	k5eAaImIp3nS	umocňovat
silná	silný	k2eAgFnSc1d1	silná
půdní	půdní	k2eAgFnSc1d1	půdní
eroze	eroze	k1gFnSc1	eroze
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
And	Anda	k1gFnPc2	Anda
a	a	k8xC	a
celkově	celkově	k6eAd1	celkově
těžké	těžký	k2eAgFnPc4d1	těžká
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
zde	zde	k6eAd1	zde
panují	panovat	k5eAaImIp3nP	panovat
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
vývozními	vývozní	k2eAgInPc7d1	vývozní
artikly	artikl	k1gInPc7	artikl
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
jsou	být	k5eAaImIp3nP	být
ropa	ropa	k1gFnSc1	ropa
<g/>
,	,	kIx,	,
kakao	kakao	k1gNnSc1	kakao
<g/>
,	,	kIx,	,
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
květiny	květina	k1gFnPc1	květina
<g/>
,	,	kIx,	,
balza	balza	k1gFnSc1	balza
<g/>
,	,	kIx,	,
banány	banán	k1gInPc1	banán
a	a	k8xC	a
garnáti	garnát	k1gMnPc1	garnát
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgNnPc4d1	hlavní
hospodářská	hospodářský	k2eAgNnPc4d1	hospodářské
odvětví	odvětví	k1gNnPc4	odvětví
patří	patřit	k5eAaImIp3nP	patřit
zemědělství	zemědělství	k1gNnPc1	zemědělství
<g/>
,	,	kIx,	,
těžba	těžba	k1gFnSc1	těžba
a	a	k8xC	a
rafinace	rafinace	k1gFnSc1	rafinace
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
cementu	cement	k1gInSc2	cement
<g/>
,	,	kIx,	,
petrochemie	petrochemie	k1gFnSc2	petrochemie
a	a	k8xC	a
potravinářství	potravinářství	k1gNnSc2	potravinářství
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
třetiny	třetina	k1gFnSc2	třetina
populace	populace	k1gFnSc2	populace
jsou	být	k5eAaImIp3nP	být
čistokrevní	čistokrevný	k2eAgMnPc1d1	čistokrevný
indiáni	indián	k1gMnPc1	indián
z	z	k7c2	z
kmenů	kmen	k1gInPc2	kmen
Chívarů	Chívar	k1gInPc2	Chívar
a	a	k8xC	a
Kečuů	Keču	k1gInPc2	Keču
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
k	k	k7c3	k
indiánskému	indiánský	k2eAgInSc3d1	indiánský
původu	původ	k1gInSc3	původ
přihlásilo	přihlásit	k5eAaPmAgNnS	přihlásit
7	[number]	k4	7
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
procento	procento	k1gNnSc1	procento
lidí	člověk	k1gMnPc2	člověk
domorodého	domorodý	k2eAgInSc2d1	domorodý
původu	původ	k1gInSc2	původ
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
jim	on	k3xPp3gMnPc3	on
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
velké	velký	k2eAgNnSc4d1	velké
území	území	k1gNnSc4	území
na	na	k7c6	na
východě	východ	k1gInSc6	východ
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
indiáni	indián	k1gMnPc1	indián
mají	mít	k5eAaImIp3nP	mít
stále	stále	k6eAd1	stále
malý	malý	k2eAgInSc4d1	malý
kapitál	kapitál	k1gInSc4	kapitál
a	a	k8xC	a
malou	malý	k2eAgFnSc4d1	malá
politickou	politický	k2eAgFnSc4d1	politická
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Kojenecká	kojenecký	k2eAgFnSc1d1	kojenecká
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
u	u	k7c2	u
indiánů	indián	k1gMnPc2	indián
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
9	[number]	k4	9
%	%	kIx~	%
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
průměrná	průměrný	k2eAgFnSc1d1	průměrná
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
okolo	okolo	k7c2	okolo
50	[number]	k4	50
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
kmeny	kmen	k1gInPc1	kmen
žijí	žít	k5eAaImIp3nP	žít
jako	jako	k9	jako
kočovníci	kočovník	k1gMnPc1	kočovník
v	v	k7c6	v
hloubi	hloub	k1gFnSc6	hloub
ekvádorského	ekvádorský	k2eAgInSc2d1	ekvádorský
deštného	deštný	k2eAgInSc2d1	deštný
pralesa	prales	k1gInSc2	prales
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
si	se	k3xPyFc3	se
vždy	vždy	k6eAd1	vždy
vymýtí	vymýtit	k5eAaPmIp3nP	vymýtit
kus	kus	k1gInSc4	kus
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
postaví	postavit	k5eAaPmIp3nS	postavit
vesnici	vesnice	k1gFnSc4	vesnice
z	z	k7c2	z
chaloupek	chaloupka	k1gFnPc2	chaloupka
s	s	k7c7	s
doškovou	doškový	k2eAgFnSc7d1	došková
střechou	střecha	k1gFnSc7	střecha
<g/>
,	,	kIx,	,
vypěstují	vypěstovat	k5eAaPmIp3nP	vypěstovat
něco	něco	k3yInSc4	něco
málo	málo	k6eAd1	málo
plodin	plodina	k1gFnPc2	plodina
a	a	k8xC	a
po	po	k7c6	po
žních	žeň	k1gFnPc6	žeň
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
přesunou	přesunout	k5eAaPmIp3nP	přesunout
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
loví	lovit	k5eAaImIp3nP	lovit
zvěř	zvěř	k1gFnSc4	zvěř
a	a	k8xC	a
rybaří	rybařit	k5eAaImIp3nS	rybařit
<g/>
.	.	kIx.	.
</s>
<s>
Chívarové	Chívarová	k1gFnPc1	Chívarová
v	v	k7c6	v
Amazonii	Amazonie	k1gFnSc6	Amazonie
prosluli	proslout	k5eAaPmAgMnP	proslout
jako	jako	k9	jako
lovci	lovec	k1gMnPc1	lovec
lebek	lebka	k1gFnPc2	lebka
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
indiánů	indián	k1gMnPc2	indián
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
Ekvádoru	Ekvádor	k1gInSc6	Ekvádor
<g/>
;	;	kIx,	;
jeden	jeden	k4xCgInSc1	jeden
kmen	kmen	k1gInSc1	kmen
<g/>
,	,	kIx,	,
Coloradové	Coloradový	k2eAgFnPc1d1	Coloradový
<g/>
,	,	kIx,	,
však	však	k9	však
obývá	obývat	k5eAaImIp3nS	obývat
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
jedinými	jediný	k2eAgMnPc7d1	jediný
obyvateli	obyvatel	k1gMnPc7	obyvatel
deštného	deštný	k2eAgInSc2d1	deštný
pralesa	prales	k1gInSc2	prales
<g/>
,	,	kIx,	,
žijícími	žijící	k2eAgFnPc7d1	žijící
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
And	Anda	k1gFnPc2	Anda
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
indiánské	indiánský	k2eAgInPc1d1	indiánský
kmeny	kmen	k1gInPc1	kmen
najdeme	najít	k5eAaPmIp1nP	najít
vysoko	vysoko	k6eAd1	vysoko
v	v	k7c6	v
Andách	Anda	k1gFnPc6	Anda
<g/>
;	;	kIx,	;
obdělávají	obdělávat	k5eAaImIp3nP	obdělávat
tam	tam	k6eAd1	tam
malá	malý	k2eAgNnPc4d1	malé
políčka	políčko	k1gNnPc4	políčko
a	a	k8xC	a
prodávají	prodávat	k5eAaImIp3nP	prodávat
řemeslné	řemeslný	k2eAgInPc4d1	řemeslný
výrobky	výrobek	k1gInPc4	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Předkové	předek	k1gMnPc1	předek
těchto	tento	k3xDgInPc2	tento
kmenů	kmen	k1gInPc2	kmen
tvořili	tvořit	k5eAaImAgMnP	tvořit
součást	součást	k1gFnSc4	součást
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
incké	incký	k2eAgFnSc2d1	incká
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
během	během	k7c2	během
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
z	z	k7c2	z
Peru	Peru	k1gNnSc2	Peru
na	na	k7c4	na
sever	sever	k1gInSc4	sever
a	a	k8xC	a
kromě	kromě	k7c2	kromě
centrálních	centrální	k2eAgFnPc2d1	centrální
And	Anda	k1gFnPc2	Anda
pokryla	pokrýt	k5eAaPmAgFnS	pokrýt
i	i	k9	i
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
severních	severní	k2eAgFnPc2d1	severní
And	Anda	k1gFnPc2	Anda
<g/>
.	.	kIx.	.
</s>
<s>
Španělé	Španěl	k1gMnPc1	Španěl
si	se	k3xPyFc3	se
Inky	Inka	k1gFnPc4	Inka
podrobili	podrobit	k5eAaPmAgMnP	podrobit
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
přinesli	přinést	k5eAaPmAgMnP	přinést
na	na	k7c4	na
dobytá	dobytý	k2eAgNnPc4d1	dobyté
území	území	k1gNnSc4	území
feudální	feudální	k2eAgInSc1d1	feudální
systém	systém	k1gInSc1	systém
zvaný	zvaný	k2eAgInSc1d1	zvaný
encomienda	encomienda	k1gFnSc1	encomienda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
si	se	k3xPyFc3	se
dovezli	dovézt	k5eAaPmAgMnP	dovézt
otroky	otrok	k1gMnPc7	otrok
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
pak	pak	k6eAd1	pak
pracovali	pracovat	k5eAaImAgMnP	pracovat
na	na	k7c6	na
cukrových	cukrový	k2eAgFnPc6d1	cukrová
plantážích	plantáž	k1gFnPc6	plantáž
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
mestice	mestic	k1gMnSc4	mestic
smíšeného	smíšený	k2eAgInSc2d1	smíšený
původu	původ	k1gInSc2	původ
-	-	kIx~	-
míšenci	míšenec	k1gMnPc1	míšenec
bělochů	běloch	k1gMnPc2	běloch
a	a	k8xC	a
indiánů	indián	k1gMnPc2	indián
tvoří	tvořit	k5eAaImIp3nS	tvořit
71.9	[number]	k4	71.9
<g/>
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
7.4	[number]	k4	7.4
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
Montubiové	Montubiový	k2eAgFnPc1d1	Montubiový
<g/>
,	,	kIx,	,
domorodé	domorodý	k2eAgNnSc1d1	domorodé
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
z	z	k7c2	z
pobřeží	pobřeží	k1gNnSc2	pobřeží
-	-	kIx~	-
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
samostatná	samostatný	k2eAgFnSc1d1	samostatná
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
7.2	[number]	k4	7.2
<g/>
%	%	kIx~	%
Afroekvádorci	Afroekvádorek	k1gMnPc1	Afroekvádorek
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
%	%	kIx~	%
indiáni	indián	k1gMnPc1	indián
a	a	k8xC	a
6.1	[number]	k4	6.1
<g/>
%	%	kIx~	%
běloši	běloch	k1gMnPc1	běloch
převážně	převážně	k6eAd1	převážně
španělského	španělský	k2eAgInSc2d1	španělský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
k	k	k7c3	k
indiánskému	indiánský	k2eAgInSc3d1	indiánský
původu	původ	k1gInSc3	původ
hlásilo	hlásit	k5eAaImAgNnS	hlásit
přibližně	přibližně	k6eAd1	přibližně
40	[number]	k4	40
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
40	[number]	k4	40
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
považovalo	považovat	k5eAaImAgNnS	považovat
za	za	k7c4	za
mestice	mestic	k1gMnPc4	mestic
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
%	%	kIx~	%
za	za	k7c4	za
bělochy	běloch	k1gMnPc4	běloch
a	a	k8xC	a
5	[number]	k4	5
<g/>
%	%	kIx~	%
za	za	k7c4	za
černochy	černoch	k1gMnPc4	černoch
<g/>
.	.	kIx.	.
</s>
<s>
Úředními	úřední	k2eAgMnPc7d1	úřední
jazyky	jazyk	k1gMnPc7	jazyk
jsou	být	k5eAaImIp3nP	být
španělština	španělština	k1gFnSc1	španělština
<g/>
,	,	kIx,	,
kečujština	kečujština	k1gFnSc1	kečujština
a	a	k8xC	a
šuárština	šuárština	k1gFnSc1	šuárština
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
se	se	k3xPyFc4	se
však	však	k9	však
používá	používat	k5eAaImIp3nS	používat
španělština	španělština	k1gFnSc1	španělština
<g/>
.	.	kIx.	.
</s>
<s>
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
se	se	k3xPyFc4	se
zbavil	zbavit	k5eAaPmAgInS	zbavit
závislosti	závislost	k1gFnSc2	závislost
na	na	k7c6	na
Španělsku	Španělsko	k1gNnSc6	Španělsko
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
lidé	člověk	k1gMnPc1	člověk
španělského	španělský	k2eAgInSc2d1	španělský
původu	původ	k1gInSc2	původ
však	však	k9	však
zůstali	zůstat	k5eAaPmAgMnP	zůstat
tou	ten	k3xDgFnSc7	ten
nejbohatší	bohatý	k2eAgFnSc7d3	nejbohatší
a	a	k8xC	a
nejvlivnější	vlivný	k2eAgFnSc7d3	nejvlivnější
skupinou	skupina	k1gFnSc7	skupina
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
mnoho	mnoho	k4c1	mnoho
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
chudobě	chudoba	k1gFnSc6	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
Novodobé	novodobý	k2eAgFnPc1d1	novodobá
dějiny	dějiny	k1gFnPc1	dějiny
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
politickou	politický	k2eAgFnSc7d1	politická
nestabilitou	nestabilita	k1gFnSc7	nestabilita
<g/>
;	;	kIx,	;
došlo	dojít	k5eAaPmAgNnS	dojít
zde	zde	k6eAd1	zde
k	k	k7c3	k
celé	celý	k2eAgFnSc3d1	celá
řadě	řada	k1gFnSc3	řada
vojenských	vojenský	k2eAgInPc2d1	vojenský
převratů	převrat	k1gInPc2	převrat
a	a	k8xC	a
bouřlivých	bouřlivý	k2eAgNnPc2d1	bouřlivé
povstání	povstání	k1gNnPc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
je	být	k5eAaImIp3nS	být
vláda	vláda	k1gFnSc1	vláda
volena	volit	k5eAaImNgFnS	volit
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
cestou	cesta	k1gFnSc7	cesta
<g/>
,	,	kIx,	,
problém	problém	k1gInSc1	problém
chudoby	chudoba	k1gFnSc2	chudoba
však	však	k9	však
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nevyřešen	vyřešen	k2eNgInSc1d1	nevyřešen
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
68	[number]	k4	68
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
římskokatolického	římskokatolický	k2eAgNnSc2d1	římskokatolické
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
víru	vír	k1gInSc2	vír
praktikuje	praktikovat	k5eAaImIp3nS	praktikovat
a	a	k8xC	a
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
mše	mše	k1gFnSc2	mše
pravidelně	pravidelně	k6eAd1	pravidelně
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
venkovských	venkovský	k2eAgFnPc6d1	venkovská
oblastech	oblast	k1gFnPc6	oblast
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
jsou	být	k5eAaImIp3nP	být
domorodná	domorodný	k2eAgFnSc1d1	domorodný
víra	víra	k1gFnSc1	víra
a	a	k8xC	a
křesťanství	křesťanství	k1gNnSc1	křesťanství
jsou	být	k5eAaImIp3nP	být
někdy	někdy	k6eAd1	někdy
syncretcké	syncretcký	k2eAgFnPc1d1	syncretcký
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
jiné	jiný	k2eAgFnSc6d1	jiná
latinskoamerické	latinskoamerický	k2eAgFnSc6d1	latinskoamerická
zemi	zem	k1gFnSc6	zem
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
protestantismus	protestantismus	k1gInSc1	protestantismus
masivní	masivní	k2eAgInSc4d1	masivní
růst	růst	k1gInSc4	růst
<g/>
,	,	kIx,	,
speciálně	speciálně	k6eAd1	speciálně
v	v	k7c6	v
chudých	chudý	k2eAgFnPc6d1	chudá
venkovských	venkovský	k2eAgFnPc6d1	venkovská
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgNnSc1d1	jiné
seskupení	seskupení	k1gNnSc1	seskupení
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Svědkové	svědek	k1gMnPc1	svědek
Jehovovi	Jehovův	k2eAgMnPc1d1	Jehovův
a	a	k8xC	a
Mormoni	mormon	k1gMnPc1	mormon
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
počty	počet	k1gInPc4	počet
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
malá	malý	k2eAgFnSc1d1	malá
muslimská	muslimský	k2eAgFnSc1d1	muslimská
menšina	menšina	k1gFnSc1	menšina
čítající	čítající	k2eAgFnSc1d1	čítající
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Židovská	židovský	k2eAgFnSc1d1	židovská
komunita	komunita	k1gFnSc1	komunita
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
něco	něco	k3yInSc4	něco
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1	[number]	k4	1
000	[number]	k4	000
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
německého	německý	k2eAgInSc2d1	německý
a	a	k8xC	a
italského	italský	k2eAgInSc2d1	italský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Převládající	převládající	k2eAgFnSc1d1	převládající
kultura	kultura	k1gFnSc1	kultura
v	v	k7c6	v
Ekvádoru	Ekvádor	k1gInSc6	Ekvádor
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
ekvádorskou	ekvádorský	k2eAgFnSc7d1	ekvádorská
většinou	většina	k1gFnSc7	většina
mesticů	mestic	k1gMnPc2	mestic
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc4	jejich
předky	předek	k1gInPc4	předek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
směsí	směs	k1gFnSc7	směs
evropské	evropský	k2eAgFnSc2d1	Evropská
a	a	k8xC	a
jihoamerické	jihoamerický	k2eAgFnSc2d1	jihoamerická
kultury	kultura	k1gFnSc2	kultura
s	s	k7c7	s
některými	některý	k3yIgInPc7	některý
africkými	africký	k2eAgInPc7d1	africký
prvky	prvek	k1gInPc7	prvek
zděděnými	zděděný	k2eAgInPc7d1	zděděný
po	po	k7c6	po
zotročených	zotročený	k2eAgMnPc6d1	zotročený
předcích	předek	k1gMnPc6	předek
<g/>
.	.	kIx.	.
</s>
<s>
Ekvádorské	ekvádorský	k2eAgFnPc1d1	ekvádorská
domorodé	domorodý	k2eAgFnPc1d1	domorodá
komunity	komunita	k1gFnPc1	komunita
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
víceméně	víceméně	k9	víceméně
integrované	integrovaný	k2eAgNnSc1d1	integrované
do	do	k7c2	do
těchto	tento	k3xDgFnPc2	tento
mainstreamových	mainstreamový	k2eAgFnPc2d1	mainstreamová
kultur	kultura	k1gFnPc2	kultura
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
vzdálenější	vzdálený	k2eAgFnPc1d2	vzdálenější
domorodé	domorodý	k2eAgFnPc1d1	domorodá
komunity	komunita	k1gFnPc1	komunita
Amazonské	amazonský	k2eAgFnSc2d1	Amazonská
kotliny	kotlina	k1gFnSc2	kotlina
<g/>
.	.	kIx.	.
</s>
<s>
Nejpopulárnějším	populární	k2eAgInSc7d3	nejpopulárnější
sportem	sport	k1gInSc7	sport
v	v	k7c6	v
Ekvádoru	Ekvádor	k1gInSc6	Ekvádor
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
jihoamerických	jihoamerický	k2eAgFnPc2d1	jihoamerická
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
fotbal	fotbal	k1gInSc1	fotbal
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
nejznámějších	známý	k2eAgInPc2d3	nejznámější
týmů	tým	k1gInPc2	tým
<g/>
:	:	kIx,	:
Barcelona	Barcelona	k1gFnSc1	Barcelona
SC	SC	kA	SC
a	a	k8xC	a
FC	FC	kA	FC
Emelec	Emelec	k1gInSc1	Emelec
z	z	k7c2	z
Guayaquilu	Guayaquil	k1gInSc2	Guayaquil
<g/>
,	,	kIx,	,
Liga	liga	k1gFnSc1	liga
Deportiva	Deportivum	k1gNnSc2	Deportivum
Universitaria	Universitarium	k1gNnSc2	Universitarium
de	de	k?	de
Quito	Quito	k1gNnSc1	Quito
a	a	k8xC	a
El	Ela	k1gFnPc2	Ela
Nacional	Nacional	k1gFnPc2	Nacional
(	(	kIx(	(
<g/>
ekvádorské	ekvádorský	k2eAgFnPc1d1	ekvádorská
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
<g/>
)	)	kIx)	)
z	z	k7c2	z
Quita	Quit	k1gInSc2	Quit
<g/>
,	,	kIx,	,
Olmedo	Olmedo	k1gNnSc4	Olmedo
z	z	k7c2	z
Riobamba	Riobamb	k1gMnSc2	Riobamb
a	a	k8xC	a
Deportivo	Deportiva	k1gFnSc5	Deportiva
Cuenca	Cuenc	k2eAgNnPc4d1	Cuenc
z	z	k7c2	z
Cuenca	Cuenc	k1gInSc2	Cuenc
<g/>
.	.	kIx.	.
</s>
<s>
Projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
značný	značný	k2eAgInSc1d1	značný
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
tenis	tenis	k1gInSc4	tenis
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
vyšší	vysoký	k2eAgFnSc6d2	vyšší
vrstvě	vrstva	k1gFnSc6	vrstva
ekvádorské	ekvádorský	k2eAgFnSc2d1	ekvádorská
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
několik	několik	k4yIc1	několik
ekvádorských	ekvádorský	k2eAgMnPc2d1	ekvádorský
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
hráčů	hráč	k1gMnPc2	hráč
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
věhlasu	věhlas	k1gInSc2	věhlas
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
jmen	jméno	k1gNnPc2	jméno
jako	jako	k8xC	jako
Francisco	Francisco	k6eAd1	Francisco
Segura	Segura	k1gFnSc1	Segura
či	či	k8xC	či
Andrés	Andrés	k1gInSc1	Andrés
Gomez	Gomeza	k1gFnPc2	Gomeza
a	a	k8xC	a
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
Nicolas	Nicolas	k1gInSc4	Nicolas
Lapentti	Lapentti	k1gNnSc3	Lapentti
<g/>
.	.	kIx.	.
</s>
<s>
Basketbal	basketbal	k1gInSc1	basketbal
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
velkou	velký	k2eAgFnSc4d1	velká
poluplaritu	poluplarita	k1gFnSc4	poluplarita
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mezi	mezi	k7c4	mezi
ekvádorské	ekvádorský	k2eAgFnPc4d1	ekvádorská
speciality	specialita	k1gFnPc4	specialita
patří	patřit	k5eAaImIp3nP	patřit
Ecuavolley	Ecuavolley	k1gInPc1	Ecuavolley
<g/>
,	,	kIx,	,
variace	variace	k1gFnPc1	variace
volejbalu	volejbal	k1gInSc2	volejbal
pro	pro	k7c4	pro
tři	tři	k4xCgMnPc4	tři
hráče	hráč	k1gMnPc4	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Býčí	býčí	k2eAgInPc1d1	býčí
zápasy	zápas	k1gInPc1	zápas
se	se	k3xPyFc4	se
praktikují	praktikovat	k5eAaImIp3nP	praktikovat
na	na	k7c6	na
profesionální	profesionální	k2eAgFnSc6d1	profesionální
úrovni	úroveň	k1gFnSc6	úroveň
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
Quitu	Quit	k1gInSc6	Quit
během	během	k7c2	během
výročních	výroční	k2eAgFnPc2d1	výroční
slavností	slavnost	k1gFnPc2	slavnost
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
založení	založení	k1gNnSc2	založení
města	město	k1gNnSc2	město
Španěly	Španěl	k1gMnPc4	Španěl
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
nekrvavá	krvavý	k2eNgFnSc1d1	nekrvavá
forma	forma	k1gFnSc1	forma
tohoto	tento	k3xDgInSc2	tento
sportu	sport	k1gInSc2	sport
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
rodeos	rodeos	k1gInSc4	rodeos
montubios	montubiosa	k1gFnPc2	montubiosa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
praktikuje	praktikovat	k5eAaImIp3nS	praktikovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
venkovských	venkovský	k2eAgFnPc6d1	venkovská
oblastech	oblast	k1gFnPc6	oblast
během	během	k7c2	během
místních	místní	k2eAgFnPc2d1	místní
slavností	slavnost	k1gFnPc2	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Kuchyně	kuchyně	k1gFnSc1	kuchyně
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Ekvádoru	Ekvádor	k1gInSc6	Ekvádor
velmi	velmi	k6eAd1	velmi
různorodá	různorodý	k2eAgFnSc1d1	různorodá
<g/>
;	;	kIx,	;
mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
podmínkami	podmínka	k1gFnPc7	podmínka
pro	pro	k7c4	pro
zemědělství	zemědělství	k1gNnSc4	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Cuy	Cuy	k?	Cuy
(	(	kIx(	(
<g/>
pečené	pečený	k2eAgNnSc1d1	pečené
morče	morče	k1gNnSc1	morče
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
populární	populární	k2eAgFnSc1d1	populární
v	v	k7c6	v
horských	horský	k2eAgFnPc6d1	horská
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
celá	celý	k2eAgFnSc1d1	celá
paleta	paleta	k1gFnSc1	paleta
čerstvého	čerstvý	k2eAgNnSc2d1	čerstvé
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
polohách	poloha	k1gFnPc6	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Mořské	mořský	k2eAgInPc1d1	mořský
plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
populární	populární	k2eAgInPc1d1	populární
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
corvina	corvina	k1gFnSc1	corvina
a	a	k8xC	a
garnáti	garnát	k1gMnPc1	garnát
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
krevety	kreveta	k1gFnPc1	kreveta
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgFnPc1d1	populární
a	a	k8xC	a
čerstvé	čerstvý	k2eAgFnPc1d1	čerstvá
<g/>
.	.	kIx.	.
</s>
<s>
Strava	strava	k1gFnSc1	strava
nabízena	nabízen	k2eAgFnSc1d1	nabízena
na	na	k7c6	na
ulicích	ulice	k1gFnPc6	ulice
v	v	k7c6	v
Ekvádoru	Ekvádor	k1gInSc6	Ekvádor
jsou	být	k5eAaImIp3nP	být
brambory	brambor	k1gInPc1	brambor
s	s	k7c7	s
opékáním	opékání	k1gNnSc7	opékání
vepřovým	vepřový	k2eAgNnSc7d1	vepřové
masem	maso	k1gNnSc7	maso
<g/>
.	.	kIx.	.
</s>
<s>
Fanesca	Fanesca	k6eAd1	Fanesca
je	být	k5eAaImIp3nS	být
jídlo	jídlo	k1gNnSc1	jídlo
známé	známý	k2eAgNnSc1d1	známé
v	v	k7c6	v
Ekvádoru	Ekvádor	k1gInSc6	Ekvádor
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
čočková	čočkový	k2eAgFnSc1d1	čočková
polévka	polévka	k1gFnSc1	polévka
se	s	k7c7	s
sušenou	sušený	k2eAgFnSc7d1	sušená
treskou	treska	k1gFnSc7	treska
<g/>
,	,	kIx,	,
mlékem	mléko	k1gNnSc7	mléko
a	a	k8xC	a
četnými	četný	k2eAgInPc7d1	četný
druhy	druh	k1gInPc7	druh
fazolí	fazole	k1gFnPc2	fazole
<g/>
,	,	kIx,	,
např.	např.	kA	např.
zelenou	zelený	k2eAgFnSc4d1	zelená
fazolemi	fazole	k1gFnPc7	fazole
<g/>
,	,	kIx,	,
lima	limo	k1gNnSc2	limo
fazolemi	fazole	k1gFnPc7	fazole
a	a	k8xC	a
chochos	chochos	k1gInSc1	chochos
<g/>
.	.	kIx.	.
</s>
