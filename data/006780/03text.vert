<s>
Nicholas	Nicholas	k1gMnSc1	Nicholas
Jerry	Jerra	k1gFnSc2	Jerra
"	"	kIx"	"
<g/>
Nick	Nick	k1gMnSc1	Nick
<g/>
"	"	kIx"	"
Jonas	Jonas	k1gMnSc1	Jonas
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
Dallas	Dallas	k1gInSc1	Dallas
<g/>
,	,	kIx,	,
Texas	Texas	k1gInSc1	Texas
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
<g/>
,	,	kIx,	,
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
pianista	pianista	k1gMnSc1	pianista
<g/>
,	,	kIx,	,
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
bubeník	bubeník	k1gMnSc1	bubeník
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
známý	známý	k1gMnSc1	známý
především	především	k6eAd1	především
jako	jako	k9	jako
člen	člen	k1gMnSc1	člen
Jonas	Jonasa	k1gFnPc2	Jonasa
Brothers	Brothersa	k1gFnPc2	Brothersa
<g/>
,	,	kIx,	,
pop-rockové	popockový	k2eAgFnSc2d1	pop-rocková
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
založil	založit	k5eAaPmAgMnS	založit
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
bratry	bratr	k1gMnPc7	bratr
Joem	Joemo	k1gNnPc2	Joemo
a	a	k8xC	a
Kevinem	Kevin	k1gInSc7	Kevin
Jonasovými	Jonasův	k2eAgInPc7d1	Jonasův
<g/>
.	.	kIx.	.
</s>
<s>
Jonas	Jonas	k1gInSc1	Jonas
Brothers	Brothers	k1gInSc1	Brothers
začali	začít	k5eAaPmAgMnP	začít
původně	původně	k6eAd1	původně
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gFnSc1	jeho
sólová	sólový	k2eAgFnSc1d1	sólová
kariéra	kariéra	k1gFnSc1	kariéra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hudebním	hudební	k2eAgMnPc3d1	hudební
producentům	producent	k1gMnPc3	producent
se	se	k3xPyFc4	se
líbilo	líbit	k5eAaImAgNnS	líbit
<g/>
,	,	kIx,	,
když	když	k8xS	když
jeho	jeho	k3xOp3gMnPc1	jeho
bratři	bratr	k1gMnPc1	bratr
zpívali	zpívat	k5eAaImAgMnP	zpívat
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Disney	Disnea	k1gFnSc2	Disnea
channelu	channel	k1gInSc2	channel
JONAS	JONAS	kA	JONAS
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
JONAS	JONAS	kA	JONAS
LA	la	k1gNnSc1	la
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
Nick	Nick	k1gMnSc1	Nick
Lucas	Lucas	k1gMnSc1	Lucas
po	po	k7c6	po
boku	bok	k1gInSc6	bok
svých	svůj	k3xOyFgMnPc2	svůj
bratrů	bratr	k1gMnPc2	bratr
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
DCOMu	DCOMus	k1gInSc6	DCOMus
Camp	camp	k1gInSc1	camp
Rock	rock	k1gInSc1	rock
i	i	k8xC	i
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
pokračování	pokračování	k1gNnSc6	pokračování
Camp	camp	k1gInSc4	camp
Rock	rock	k1gInSc1	rock
2	[number]	k4	2
<g/>
:	:	kIx,	:
Velký	velký	k2eAgInSc1d1	velký
koncert	koncert	k1gInSc1	koncert
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgMnS	hrát
postavu	postava	k1gFnSc4	postava
jménem	jméno	k1gNnSc7	jméno
Nate	Nat	k1gMnSc2	Nat
Gray	Graa	k1gMnSc2	Graa
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
skupinu	skupina	k1gFnSc4	skupina
Nick	Nick	k1gMnSc1	Nick
Jonas	Jonas	k1gMnSc1	Jonas
&	&	k?	&
The	The	k1gFnSc1	The
Administration	Administration	k1gInSc1	Administration
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
první	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Aktuálně	aktuálně	k6eAd1	aktuálně
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Kingdom	Kingdom	k1gInSc1	Kingdom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
moderoval	moderovat	k5eAaBmAgInS	moderovat
28	[number]	k4	28
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
předvání	předvání	k1gNnSc1	předvání
cen	cena	k1gFnPc2	cena
Kids	Kidsa	k1gFnPc2	Kidsa
<g/>
'	'	kIx"	'
Choice	Choice	k1gFnSc1	Choice
Awards	Awardsa	k1gFnPc2	Awardsa
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
obsazen	obsadit	k5eAaPmNgInS	obsadit
do	do	k7c2	do
seriálu	seriál	k1gInSc2	seriál
stanice	stanice	k1gFnSc2	stanice
FOX	fox	k1gInSc1	fox
Scream	Scream	k1gInSc1	Scream
Queens	Queens	k1gInSc1	Queens
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Dallasu	Dallas	k1gInSc6	Dallas
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
jsou	být	k5eAaImIp3nP	být
Denise	Denisa	k1gFnSc3	Denisa
(	(	kIx(	(
<g/>
rozená	rozený	k2eAgFnSc1d1	rozená
Miller	Miller	k1gMnSc1	Miller
<g/>
)	)	kIx)	)
a	a	k8xC	a
Paul	Paul	k1gMnSc1	Paul
Kevin	Kevin	k1gMnSc1	Kevin
Jonas	Jonas	k1gMnSc1	Jonas
"	"	kIx"	"
<g/>
starší	starší	k1gMnSc1	starší
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vyrostl	vyrůst	k5eAaPmAgMnS	vyrůst
ve	v	k7c6	v
Wyckoffu	Wyckoff	k1gInSc6	Wyckoff
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
Yersey	Yersea	k1gFnSc2	Yersea
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vzděláván	vzdělávat	k5eAaImNgInS	vzdělávat
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
irské	irský	k2eAgNnSc1d1	irské
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gMnSc1	jeho
dědeček	dědeček	k1gMnSc1	dědeček
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
<g/>
)	)	kIx)	)
Cherokee	Cheroke	k1gMnPc4	Cheroke
<g/>
,	,	kIx,	,
italské	italský	k2eAgMnPc4d1	italský
a	a	k8xC	a
německé	německý	k2eAgMnPc4d1	německý
předky	předek	k1gMnPc4	předek
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgMnPc4	dva
starší	starý	k2eAgMnPc4d2	starší
bratry	bratr	k1gMnPc4	bratr
Kevina	Kevina	k1gMnSc1	Kevina
(	(	kIx(	(
<g/>
narozený	narozený	k2eAgMnSc1d1	narozený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
a	a	k8xC	a
Joeho	Joe	k1gMnSc2	Joe
(	(	kIx(	(
<g/>
narozený	narozený	k2eAgMnSc1d1	narozený
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
a	a	k8xC	a
mladšího	mladý	k2eAgMnSc2d2	mladší
bratra	bratr	k1gMnSc2	bratr
Frankieho	Frankie	k1gMnSc2	Frankie
(	(	kIx(	(
<g/>
narozený	narozený	k2eAgMnSc1d1	narozený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jonasova	Jonasův	k2eAgFnSc1d1	Jonasova
kariéra	kariéra	k1gFnSc1	kariéra
začala	začít	k5eAaPmAgFnS	začít
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
6	[number]	k4	6
letech	léto	k1gNnPc6	léto
objeven	objevit	k5eAaPmNgInS	objevit
v	v	k7c6	v
holičství	holičství	k1gNnSc6	holičství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
začal	začít	k5eAaPmAgInS	začít
hrát	hrát	k5eAaImF	hrát
na	na	k7c4	na
Broadway	Broadwaa	k1gFnPc4	Broadwaa
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
ve	v	k7c6	v
hrách	hra	k1gFnPc6	hra
Christmas	Christmasa	k1gFnPc2	Christmasa
Carol	Carol	k1gInSc1	Carol
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
Tiny	Tina	k1gFnPc1	Tina
Tim	Tim	k?	Tim
a	a	k8xC	a
Scrooge	Scrooge	k1gNnPc2	Scrooge
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Annie	Annie	k1gFnSc1	Annie
Get	Get	k1gMnSc1	Get
Your	Your	k1gMnSc1	Your
Gun	Gun	k1gMnSc1	Gun
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
Little	Little	k1gFnSc1	Little
Jake	Jak	k1gFnSc2	Jak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kráska	kráska	k1gFnSc1	kráska
a	a	k8xC	a
Zvíře	zvíře	k1gNnSc1	zvíře
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
<g/>
jako	jako	k8xC	jako
Chip	Chip	k1gInSc1	Chip
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Les	les	k1gInSc1	les
Misérables	Misérables	k1gInSc1	Misérables
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Gavroche	Gavroche	k1gInSc1	Gavroche
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c4	v
Sounds	Sounds	k1gInSc4	Sounds
of	of	k?	of
music	musice	k1gFnPc2	musice
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
Kurt	Kurt	k1gMnSc1	Kurt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
napsal	napsat	k5eAaBmAgMnS	napsat
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
otcem	otec	k1gMnSc7	otec
písničku	písnička	k1gFnSc4	písnička
"	"	kIx"	"
<g/>
Joy	Joy	k1gFnSc4	Joy
to	ten	k3xDgNnSc1	ten
the	the	k?	the
World	Worldo	k1gNnPc2	Worldo
(	(	kIx(	(
<g/>
A	a	k8xC	a
Christmas	Christmas	k1gMnSc1	Christmas
Prayer	Prayer	k1gMnSc1	Prayer
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2003	[number]	k4	2003
INO	INO	kA	INO
Records	Records	k1gInSc1	Records
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
demo	demo	k2eAgFnPc4d1	demo
písničky	písnička	k1gFnPc4	písnička
a	a	k8xC	a
zveřejnili	zveřejnit	k5eAaPmAgMnP	zveřejnit
jí	on	k3xPp3gFnSc3	on
a	a	k8xC	a
písnička	písnička	k1gFnSc1	písnička
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
ihned	ihned	k6eAd1	ihned
velice	velice	k6eAd1	velice
oblíbenou	oblíbený	k2eAgFnSc4d1	oblíbená
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2004	[number]	k4	2004
nahrávací	nahrávací	k2eAgFnSc4d1	nahrávací
společnost	společnost	k1gFnSc4	společnost
Columbia	Columbia	k1gFnSc1	Columbia
Records	Recordsa	k1gFnPc2	Recordsa
objevila	objevit	k5eAaPmAgFnS	objevit
Nicka	nicka	k1gFnSc1	nicka
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
podepsal	podepsat	k5eAaPmAgMnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
jak	jak	k8xS	jak
s	s	k7c7	s
INO	INO	kA	INO
Recorsd	Recorsd	k1gInSc1	Recorsd
tak	tak	k6eAd1	tak
s	s	k7c7	s
Columbia	Columbia	k1gFnSc1	Columbia
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
měl	mít	k5eAaImAgMnS	mít
vydat	vydat	k5eAaPmF	vydat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
Nicholas	Nicholasa	k1gFnPc2	Nicholasa
Jonas	Jonasa	k1gFnPc2	Jonasa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vydání	vydání	k1gNnSc1	vydání
bylo	být	k5eAaImAgNnS	být
posunuto	posunout	k5eAaPmNgNnS	posunout
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
jen	jen	k9	jen
limitovaně	limitovaně	k6eAd1	limitovaně
<g/>
.	.	kIx.	.
</s>
<s>
Nick	Nick	k6eAd1	Nick
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
bratry	bratr	k1gMnPc7	bratr
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
písničkách	písnička	k1gFnPc6	písnička
pro	pro	k7c4	pro
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
slyšel	slyšet	k5eAaImAgMnS	slyšet
nový	nový	k2eAgMnSc1d1	nový
prezident	prezident	k1gMnSc1	prezident
Columbia	Columbia	k1gFnSc1	Columbia
Records	Records	k1gInSc1	Records
Steve	Steve	k1gMnSc1	Steve
Greenberg	Greenberg	k1gMnSc1	Greenberg
nahrávky	nahrávka	k1gFnSc2	nahrávka
bratrů	bratr	k1gMnPc2	bratr
a	a	k8xC	a
album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
nelíbilo	líbit	k5eNaImAgNnS	líbit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
líbil	líbit	k5eAaImAgMnS	líbit
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
Nickův	Nickův	k2eAgInSc4d1	Nickův
hlas	hlas	k1gInSc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Jonas	Jonas	k1gMnSc1	Jonas
Brothers	Brothers	k1gInSc1	Brothers
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
poslechnutí	poslechnutí	k1gNnSc6	poslechnutí
písničky	písnička	k1gFnSc2	písnička
"	"	kIx"	"
<g/>
Please	Pleasa	k1gFnSc3	Pleasa
Be	Be	k1gFnSc2	Be
Mine	minout	k5eAaImIp3nS	minout
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
nahrávací	nahrávací	k2eAgFnSc1d1	nahrávací
společnost	společnost	k1gFnSc1	společnost
Daylight	Daylight	k1gMnSc1	Daylight
<g/>
/	/	kIx~	/
<g/>
Columbia	Columbia	k1gFnSc1	Columbia
Records	Recordsa	k1gFnPc2	Recordsa
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
podepsat	podepsat	k5eAaPmF	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
bratry	bratr	k1gMnPc7	bratr
jako	jako	k8xS	jako
tříčlennou	tříčlenný	k2eAgFnSc4d1	tříčlenná
skupinu	skupina	k1gFnSc4	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Kluci	kluk	k1gMnPc1	kluk
plánovali	plánovat	k5eAaImAgMnP	plánovat
nazvat	nazvat	k5eAaBmF	nazvat
svojí	svůj	k3xOyFgFnSc7	svůj
kapelu	kapela	k1gFnSc4	kapela
"	"	kIx"	"
<g/>
Sons	Sons	k1gInSc1	Sons
of	of	k?	of
Jonas	Jonas	k1gInSc1	Jonas
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
ji	on	k3xPp3gFnSc4	on
nazvali	nazvat	k5eAaPmAgMnP	nazvat
"	"	kIx"	"
<g/>
Jonas	Jonas	k1gInSc1	Jonas
Brothers	Brothers	k1gInSc1	Brothers
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
It	It	k1gFnSc2	It
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
About	About	k1gInSc1	About
Time	Time	k1gNnSc2	Time
bylo	být	k5eAaImAgNnS	být
vydané	vydaný	k2eAgNnSc4d1	vydané
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
s	s	k7c7	s
omezeným	omezený	k2eAgInSc7d1	omezený
počtem	počet	k1gInSc7	počet
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
odejít	odejít	k5eAaPmF	odejít
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
moc	moc	k6eAd1	moc
nepodporovala	podporovat	k5eNaImAgFnS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
skupina	skupina	k1gFnSc1	skupina
podepsala	podepsat	k5eAaPmAgFnS	podepsat
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Hollywood	Hollywood	k1gInSc1	Hollywood
Records	Records	k1gInSc1	Records
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
druhé	druhý	k4xOgNnSc4	druhý
album	album	k1gNnSc4	album
Jonas	Jonasa	k1gFnPc2	Jonasa
Brothers	Brothersa	k1gFnPc2	Brothersa
vydali	vydat	k5eAaPmAgMnP	vydat
7	[number]	k4	7
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2007	[number]	k4	2007
a	a	k8xC	a
umístilo	umístit	k5eAaPmAgNnS	umístit
se	se	k3xPyFc4	se
na	na	k7c6	na
pátém	pátý	k4xOgNnSc6	pátý
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
žebříčku	žebříčko	k1gNnSc6	žebříčko
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
týdnu	týden	k1gInSc6	týden
prodeje	prodej	k1gInSc2	prodej
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2007	[number]	k4	2007
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
jako	jako	k9	jako
hosté	host	k1gMnPc1	host
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
seriálu	seriál	k1gInSc6	seriál
Hannah	Hannah	k1gMnSc1	Hannah
Montana	Montana	k1gFnSc1	Montana
<g/>
.	.	kIx.	.
</s>
<s>
Epizodu	epizoda	k1gFnSc4	epizoda
sledovalo	sledovat	k5eAaImAgNnS	sledovat
téměř	téměř	k6eAd1	téměř
10.7	[number]	k4	10.7
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
nejsledovanější	sledovaný	k2eAgFnSc7d3	nejsledovanější
epizodou	epizoda	k1gFnSc7	epizoda
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
seriálu	seriál	k1gInSc2	seriál
Disney	Disnea	k1gFnSc2	Disnea
Channelu	Channel	k1gInSc2	Channel
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
Jonas	Jonasa	k1gFnPc2	Jonasa
Brothers	Brothersa	k1gFnPc2	Brothersa
žijí	žít	k5eAaImIp3nP	žít
svůj	svůj	k3xOyFgInSc4	svůj
sen	sen	k1gInSc4	sen
měl	mít	k5eAaImAgMnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
na	na	k7c4	na
Disney	Disney	k1gInPc4	Disney
Channel	Channel	k1gInSc1	Channel
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Bratři	bratr	k1gMnPc1	bratr
Jonasové	Jonasové	k2eAgMnPc1d1	Jonasové
si	se	k3xPyFc3	se
zahráli	zahrát	k5eAaPmAgMnP	zahrát
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Camp	camp	k1gInSc1	camp
Rock	rock	k1gInSc1	rock
a	a	k8xC	a
ve	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
sequelu	sequel	k1gInSc6	sequel
Camp	camp	k1gInSc1	camp
rock	rock	k1gInSc1	rock
2	[number]	k4	2
<g/>
:	:	kIx,	:
Velký	velký	k2eAgInSc1d1	velký
koncert	koncert	k1gInSc1	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgNnSc1	třetí
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
A	a	k8xC	a
Little	Little	k1gFnSc1	Little
Bit	bit	k1gInSc1	bit
Longer	Longer	k1gInSc1	Longer
bylo	být	k5eAaImAgNnS	být
vydané	vydaný	k2eAgNnSc4d1	vydané
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
a	a	k8xC	a
prodalo	prodat	k5eAaPmAgNnS	prodat
se	se	k3xPyFc4	se
ho	on	k3xPp3gNnSc2	on
přes	přes	k7c4	přes
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
kopií	kopie	k1gFnPc2	kopie
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
měl	mít	k5eAaImAgMnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
další	další	k2eAgInSc4d1	další
seriál	seriál	k1gInSc4	seriál
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
bratři	bratr	k1gMnPc1	bratr
hráli	hrát	k5eAaImAgMnP	hrát
hlavní	hlavní	k2eAgFnPc4d1	hlavní
role	role	k1gFnPc4	role
Jonas	Jonasa	k1gFnPc2	Jonasa
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
série	série	k1gFnSc1	série
seriálu	seriál	k1gInSc2	seriál
byla	být	k5eAaImAgFnS	být
přejmenovaná	přejmenovaný	k2eAgFnSc1d1	přejmenovaná
na	na	k7c4	na
Jonas	Jonas	k1gInSc4	Jonas
L.A.	L.A.	k1gFnSc2	L.A.
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
Lines	Lines	k1gMnSc1	Lines
<g/>
,	,	kIx,	,
Vines	Vines	k1gMnSc1	Vines
and	and	k?	and
Trying	Trying	k1gInSc1	Trying
Times	Timesa	k1gFnPc2	Timesa
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
ve	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
kapelu	kapela	k1gFnSc4	kapela
Nick	Nick	k1gMnSc1	Nick
Jonas	Jonas	k1gMnSc1	Jonas
&	&	k?	&
the	the	k?	the
Administration	Administration	k1gInSc4	Administration
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2010	[number]	k4	2010
vydali	vydat	k5eAaPmAgMnP	vydat
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
Who	Who	k1gFnPc2	Who
I	i	k8xC	i
Am	Am	k1gFnPc2	Am
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
Nick	Nick	k1gMnSc1	Nick
debutoval	debutovat	k5eAaBmAgMnS	debutovat
v	v	k7c6	v
Londýnském	londýnský	k2eAgInSc6d1	londýnský
West	West	k1gInSc1	West
End	End	k1gFnSc4	End
v	v	k7c6	v
muzikálu	muzikál	k1gInSc6	muzikál
Les	les	k1gInSc1	les
Misérables	Misérables	k1gInSc1	Misérables
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tentokrát	tentokrát	k6eAd1	tentokrát
jako	jako	k8xC	jako
Marius	Marius	k1gInSc4	Marius
Pontmercy	Pontmerca	k1gFnSc2	Pontmerca
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
hrát	hrát	k5eAaImF	hrát
tuto	tento	k3xDgFnSc4	tento
roli	role	k1gFnSc4	role
pouze	pouze	k6eAd1	pouze
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
tří	tři	k4xCgNnPc2	tři
týdnu	týden	k1gInSc6	týden
<g/>
,	,	kIx,	,
<g/>
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
doba	doba	k1gFnSc1	doba
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
posunout	posunout	k5eAaPmF	posunout
až	až	k9	až
do	do	k7c2	do
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
změnám	změna	k1gFnPc3	změna
v	v	k7c4	v
tour	tour	k1gInSc4	tour
Jonas	Jonas	k1gInSc4	Jonas
Brothers	Brothersa	k1gFnPc2	Brothersa
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
<g/>
,	,	kIx,	,
uspořádaném	uspořádaný	k2eAgInSc6d1	uspořádaný
na	na	k7c6	na
počest	počest	k1gFnSc6	počest
25	[number]	k4	25
let	léto	k1gNnPc2	léto
hraní	hranit	k5eAaImIp3nS	hranit
Les	les	k1gInSc4	les
Misérables	Misérablesa	k1gFnPc2	Misérablesa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
znovu	znovu	k6eAd1	znovu
zahrál	zahrát	k5eAaPmAgMnS	zahrát
roli	role	k1gFnSc4	role
Mariuse	Mariuse	k1gFnSc2	Mariuse
Pontmercyho	Pontmercy	k1gMnSc2	Pontmercy
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Smash	Smasha	k1gFnPc2	Smasha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgInS	zahrát
roli	role	k1gFnSc4	role
mladého	mladý	k2eAgMnSc2d1	mladý
hudebníka	hudebník	k1gMnSc2	hudebník
Lyleho	Lyle	k1gMnSc2	Lyle
Westa	West	k1gMnSc2	West
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
do	do	k7c2	do
července	červenec	k1gInSc2	červenec
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
hrál	hrát	k5eAaImAgMnS	hrát
J.	J.	kA	J.
Pierreponta	Pierreponta	k1gMnSc1	Pierreponta
Finche	Finch	k1gInSc2	Finch
v	v	k7c6	v
muzikálu	muzikál	k1gInSc6	muzikál
"	"	kIx"	"
<g/>
How	How	k1gFnSc4	How
to	ten	k3xDgNnSc1	ten
Succeed	Succeed	k1gInSc1	Succeed
in	in	k?	in
Business	business	k1gInSc1	business
Without	Without	k1gMnSc1	Without
Really	Realla	k1gFnSc2	Realla
Trying	Trying	k1gInSc1	Trying
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
soundtrackové	soundtrackový	k2eAgNnSc4d1	soundtrackový
album	album	k1gNnSc4	album
Nick	Nicka	k1gFnPc2	Nicka
nahrál	nahrát	k5eAaBmAgInS	nahrát
pět	pět	k4xCc4	pět
písniček	písnička	k1gFnPc2	písnička
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Nick	Nick	k1gMnSc1	Nick
objeví	objevit	k5eAaPmIp3nS	objevit
ve	v	k7c6	v
thrillerovém	thrillerový	k2eAgInSc6d1	thrillerový
filmu	film	k1gInSc6	film
Carful	Carfula	k1gFnPc2	Carfula
What	What	k1gMnSc1	What
You	You	k1gMnSc1	You
Wish	Wish	k1gMnSc1	Wish
For	forum	k1gNnPc2	forum
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
zahraje	zahrát	k5eAaPmIp3nS	zahrát
roli	role	k1gFnSc4	role
Nate	Nat	k1gMnSc2	Nat
Hendersona	Henderson	k1gMnSc2	Henderson
v	v	k7c6	v
dramatickém	dramatický	k2eAgInSc6d1	dramatický
seriálu	seriál	k1gInSc6	seriál
Kingdom	Kingdom	k1gInSc1	Kingdom
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
vydal	vydat	k5eAaPmAgMnS	vydat
svůj	svůj	k3xOyFgInSc4	svůj
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Chains	Chains	k1gInSc4	Chains
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Jealouse	Jealouse	k1gFnPc1	Jealouse
<g/>
"	"	kIx"	"
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
společnosti	společnost	k1gFnSc2	společnost
Island	Island	k1gInSc1	Island
Recrods	Recrods	k1gInSc1	Recrods
vydal	vydat	k5eAaPmAgInS	vydat
své	svůj	k3xOyFgNnSc4	svůj
druhé	druhý	k4xOgNnSc4	druhý
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
jako	jako	k8xC	jako
sólo	sólo	k1gNnSc4	sólo
umělec	umělec	k1gMnSc1	umělec
nazvané	nazvaný	k2eAgFnPc1d1	nazvaná
Nick	Nick	k1gInSc4	Nick
Jonas	Jonasa	k1gFnPc2	Jonasa
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
umístilo	umístit	k5eAaPmAgNnS	umístit
na	na	k7c6	na
šestém	šestý	k4xOgInSc6	šestý
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
žebříčku	žebříčko	k1gNnSc6	žebříčko
Billboard	billboard	k1gInSc1	billboard
200	[number]	k4	200
a	a	k8xC	a
za	za	k7c4	za
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
prodalo	prodat	k5eAaPmAgNnS	prodat
37	[number]	k4	37
000	[number]	k4	000
kopií	kopie	k1gFnPc2	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
hostující	hostující	k2eAgMnSc1d1	hostující
mentor	mentor	k1gMnSc1	mentor
zpěvačky	zpěvačka	k1gFnSc2	zpěvačka
Christiny	Christin	k2eAgInPc4d1	Christin
Aguilery	Aguiler	k1gInPc4	Aguiler
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
sérii	série	k1gFnSc4	série
americké	americký	k2eAgFnSc2d1	americká
talentové	talentový	k2eAgFnSc2d1	talentová
show	show	k1gFnSc2	show
The	The	k1gMnSc1	The
Voice	Voice	k1gMnSc1	Voice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
získal	získat	k5eAaPmAgMnS	získat
roli	role	k1gFnSc4	role
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
hororovém	hororový	k2eAgInSc6d1	hororový
<g/>
/	/	kIx~	/
<g/>
komediálním	komediální	k2eAgInSc6d1	komediální
seriálu	seriál	k1gInSc6	seriál
Scream	Scream	k1gInSc1	Scream
Queens	Queens	k1gInSc1	Queens
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
předskokanem	předskokan	k1gMnSc7	předskokan
Kelly	Kella	k1gFnSc2	Kella
Clarkson	Clarksona	k1gFnPc2	Clarksona
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
turné	turné	k1gNnSc6	turné
Piece	pieca	k1gFnSc3	pieca
by	by	kYmCp3nS	by
Piece	pieca	k1gFnSc3	pieca
Tour	Toura	k1gFnPc2	Toura
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
začíná	začínat	k5eAaImIp3nS	začínat
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
13	[number]	k4	13
letech	léto	k1gNnPc6	léto
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
diagnostikována	diagnostikován	k2eAgFnSc1d1	diagnostikována
cukrovka	cukrovka	k1gFnSc1	cukrovka
I.	I.	kA	I.
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
se	se	k3xPyFc4	se
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
Bayer	Bayer	k1gMnSc1	Bayer
Diabetes	diabetes	k1gInSc1	diabetes
Care	car	k1gMnSc5	car
a	a	k8xC	a
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
mladým	mladý	k2eAgMnPc3d1	mladý
diabetikům	diabetik	k1gMnPc3	diabetik
žít	žít	k5eAaImF	žít
normální	normální	k2eAgInSc4d1	normální
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Miley	Milea	k1gFnSc2	Milea
Cyrusová	Cyrusový	k2eAgFnSc1d1	Cyrusová
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
Jonasem	Jonas	k1gInSc7	Jonas
chodila	chodit	k5eAaImAgFnS	chodit
od	od	k7c2	od
června	červen	k1gInSc2	červen
2006	[number]	k4	2006
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2008	[number]	k4	2008
se	se	k3xPyFc4	se
spekulovalo	spekulovat	k5eAaImAgNnS	spekulovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
chodí	chodit	k5eAaImIp3nP	chodit
s	s	k7c7	s
herečkou	herečka	k1gFnSc7	herečka
a	a	k8xC	a
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Selenou	selený	k2eAgFnSc7d1	selený
Gomezovou	Gomezová	k1gFnSc7	Gomezová
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yRgFnSc4	který
si	se	k3xPyFc3	se
byli	být	k5eAaImAgMnP	být
velice	velice	k6eAd1	velice
blízcí	blízký	k2eAgMnPc1d1	blízký
i	i	k9	i
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
o	o	k7c6	o
možném	možný	k2eAgInSc6d1	možný
vztahu	vztah	k1gInSc6	vztah
se	s	k7c7	s
zpěvačkou	zpěvačka	k1gFnSc7	zpěvačka
Deltou	delta	k1gFnSc7	delta
Goodrem	Goodr	k1gMnSc7	Goodr
.	.	kIx.	.
<g/>
Od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
chodil	chodit	k5eAaImAgInS	chodit
s	s	k7c7	s
Miss	miss	k1gFnSc7	miss
Universe	Universe	k1gFnSc1	Universe
Olivia	Olivia	k1gFnSc1	Olivia
Culpo	Culpa	k1gFnSc5	Culpa
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
-	-	kIx~	-
LOS	los	k1gMnSc1	los
Premios	Premios	k1gMnSc1	Premios
MTV	MTV	kA	MTV
Lationoamérica	Lationoamérica	k1gMnSc1	Lationoamérica
(	(	kIx(	(
<g/>
Nejlépe	dobře	k6eAd3	dobře
oblékaný	oblékaný	k2eAgMnSc1d1	oblékaný
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
-	-	kIx~	-
Young	Young	k1gInSc1	Young
Hollywood	Hollywood	k1gInSc1	Hollywood
Awards	Awards	k1gInSc1	Awards
(	(	kIx(	(
<g/>
Umělec	umělec	k1gMnSc1	umělec
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
2011	[number]	k4	2011
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
DRLC	DRLC	kA	DRLC
(	(	kIx(	(
<g/>
Dream	Dream	k1gInSc1	Dream
Award	Award	k1gInSc1	Award
<g/>
)	)	kIx)	)
2012	[number]	k4	2012
-	-	kIx~	-
Broadway	Broadwaa	k1gFnSc2	Broadwaa
Beacon	Beacon	k1gInSc1	Beacon
Awards	Awards	k1gInSc1	Awards
(	(	kIx(	(
<g/>
Přínos	přínos	k1gInSc1	přínos
divadelní	divadelní	k2eAgInSc1d1	divadelní
komunitě	komunita	k1gFnSc3	komunita
<g/>
)	)	kIx)	)
2013	[number]	k4	2013
-	-	kIx~	-
Teen	Teen	k1gMnSc1	Teen
Choice	Choice	k1gFnSc2	Choice
Award	Award	k1gMnSc1	Award
(	(	kIx(	(
<g/>
Acevue	Acevue	k1gFnSc1	Acevue
Inspirující	inspirující	k2eAgNnSc1d1	inspirující
ocenění	ocenění	k1gNnSc1	ocenění
<g/>
)	)	kIx)	)
2014	[number]	k4	2014
-	-	kIx~	-
Young	Young	k1gInSc1	Young
Hollywood	Hollywood	k1gInSc1	Hollywood
Awards	Awards	k1gInSc4	Awards
(	(	kIx(	(
<g/>
Nejúspěšnější	úspěšný	k2eAgMnSc1d3	nejúspěšnější
crossover	crossover	k1gMnSc1	crossover
umělec	umělec	k1gMnSc1	umělec
<g/>
)	)	kIx)	)
2015	[number]	k4	2015
-	-	kIx~	-
Nickelodeon	Nickelodeon	k1gNnSc1	Nickelodeon
Kids	Kidsa	k1gFnPc2	Kidsa
<g/>
'	'	kIx"	'
<g />
.	.	kIx.	.
</s>
<s>
Choice	Choice	k1gFnSc1	Choice
Awards	Awards	k1gInSc1	Awards
(	(	kIx(	(
<g/>
Nejoblíbenější	oblíbený	k2eAgMnSc1d3	nejoblíbenější
zpěvák	zpěvák	k1gMnSc1	zpěvák
<g/>
)	)	kIx)	)
Nicholas	Nicholas	k1gMnSc1	Nicholas
Jonas	Jonas	k1gMnSc1	Jonas
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Nick	Nick	k1gMnSc1	Nick
Jonas	Jonas	k1gMnSc1	Jonas
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Last	Last	k1gMnSc1	Last
Year	Year	k1gMnSc1	Year
Was	Was	k1gMnSc1	Was
Complicated	Complicated	k1gMnSc1	Complicated
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
<g/>
:	:	kIx,	:
Who	Who	k1gMnSc1	Who
I	i	k8xC	i
Am	Am	k1gMnSc1	Am
Tour	Tour	k1gMnSc1	Tour
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Nick	Nick	k1gInSc4	Nick
Jonas	Jonasa	k1gFnPc2	Jonasa
2011	[number]	k4	2011
Tour	Toura	k1gFnPc2	Toura
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Nick	Nicko	k1gNnPc2	Nicko
Jonas	Jonasa	k1gFnPc2	Jonasa
Live	Live	k1gFnPc2	Live
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Předzpěvák	předzpěvák	k1gMnSc1	předzpěvák
<g/>
:	:	kIx,	:
Piece	pieca	k1gFnSc6	pieca
by	by	kYmCp3nS	by
Piece	pieca	k1gFnSc3	pieca
Tour	Tour	k1gInSc1	Tour
(	(	kIx(	(
<g/>
Kelly	Kell	k1gInPc1	Kell
Clarkson	Clarkson	k1gNnSc1	Clarkson
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Speciální	speciální	k2eAgMnSc1d1	speciální
host	host	k1gMnSc1	host
<g/>
:	:	kIx,	:
A	a	k9	a
Special	Special	k1gInSc1	Special
Night	Nighta	k1gFnPc2	Nighta
with	witha	k1gFnPc2	witha
Demi	Demi	k1gNnSc2	Demi
Lovato	Lovat	k2eAgNnSc1d1	Lovato
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
Neon	neon	k1gInSc1	neon
Lights	Lights	k1gInSc1	Lights
Tour	Tour	k1gInSc1	Tour
(	(	kIx(	(
<g/>
Demi	Demi	k1gNnSc1	Demi
Lovato	Lovat	k2eAgNnSc1d1	Lovato
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Jingle	Jingle	k1gInSc4	Jingle
Bell	bell	k1gInSc1	bell
Tour	Tour	k1gInSc1	Tour
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
Nickelodeon	Nickelodeon	k1gInSc1	Nickelodeon
Kids	Kids	k1gInSc1	Kids
<g/>
'	'	kIx"	'
Choice	Choice	k1gFnSc1	Choice
Awards	Awardsa	k1gFnPc2	Awardsa
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Tvůrce	tvůrce	k1gMnSc1	tvůrce
<g/>
/	/	kIx~	/
<g/>
Režisér	režisér	k1gMnSc1	režisér
<g/>
:	:	kIx,	:
Neon	neon	k1gInSc1	neon
Lights	Lights	k1gInSc1	Lights
Tour	Tour	k1gInSc1	Tour
(	(	kIx(	(
<g/>
Demi	Demi	k1gNnSc1	Demi
Lovato	Lovat	k2eAgNnSc1d1	Lovato
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
