<p>
<s>
Labrador	Labrador	k1gInSc1	Labrador
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Labrador	Labrador	k1gInSc1	Labrador
Peninsula	Peninsulum	k1gNnSc2	Peninsulum
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
Péninsule	Péninsule	k1gFnSc1	Péninsule
du	du	k?	du
Labrador	Labrador	k1gInSc1	Labrador
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
poloostrov	poloostrov	k1gInSc4	poloostrov
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Kanady	Kanada	k1gFnSc2	Kanada
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
1	[number]	k4	1
400	[number]	k4	400
000	[number]	k4	000
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
obklopen	obklopit	k5eAaPmNgInS	obklopit
Hudsonovým	Hudsonův	k2eAgInSc7d1	Hudsonův
zálivem	záliv	k1gInSc7	záliv
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
<g/>
,	,	kIx,	,
Hudsonovým	Hudsonův	k2eAgInSc7d1	Hudsonův
průlivem	průliv	k1gInSc7	průliv
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
<g/>
,	,	kIx,	,
Labradorským	labradorský	k2eAgNnSc7d1	labradorský
mořem	moře	k1gNnSc7	moře
z	z	k7c2	z
východu	východ	k1gInSc2	východ
a	a	k8xC	a
zálivem	záliv	k1gInSc7	záliv
sv.	sv.	kA	sv.
Vavřince	Vavřinec	k1gMnSc4	Vavřinec
z	z	k7c2	z
jihovýchodu	jihovýchod	k1gInSc2	jihovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
poloostrova	poloostrov	k1gInSc2	poloostrov
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
portugalského	portugalský	k2eAgMnSc2d1	portugalský
mořeplavce	mořeplavec	k1gMnSc2	mořeplavec
Joã	Joã	k1gMnSc2	Joã
Fernandese	Fernandese	k1gFnSc2	Fernandese
Lavradora	Lavrador	k1gMnSc2	Lavrador
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
počátkem	počátkem	k7c2	počátkem
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
poloostrov	poloostrov	k1gInSc1	poloostrov
Labrador	labrador	k1gMnSc1	labrador
objevil	objevit	k5eAaPmAgMnS	objevit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
poloostrova	poloostrov	k1gInSc2	poloostrov
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
pevninskou	pevninský	k2eAgFnSc7d1	pevninská
částí	část	k1gFnSc7	část
provincie	provincie	k1gFnSc2	provincie
Newfoundland	Newfoundland	k1gInSc1	Newfoundland
a	a	k8xC	a
Labrador	Labrador	k1gInSc1	Labrador
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
poloostrova	poloostrov	k1gInSc2	poloostrov
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
provincii	provincie	k1gFnSc3	provincie
Québec	Québec	k1gInSc4	Québec
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
150	[number]	k4	150
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Původními	původní	k2eAgMnPc7d1	původní
obyvateli	obyvatel	k1gMnPc7	obyvatel
poloostrova	poloostrov	k1gInSc2	poloostrov
jsou	být	k5eAaImIp3nP	být
indiáni	indián	k1gMnPc1	indián
z	z	k7c2	z
kmenů	kmen	k1gInPc2	kmen
Montagnis	Montagnis	k1gFnSc2	Montagnis
<g/>
,	,	kIx,	,
Naskapi	Naskap	k1gFnSc2	Naskap
a	a	k8xC	a
Krí	Krí	k1gFnSc2	Krí
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
též	též	k9	též
Inuité	Inuitý	k2eAgNnSc1d1	Inuité
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geologie	geologie	k1gFnSc2	geologie
==	==	k?	==
</s>
</p>
<p>
<s>
Labrador	Labrador	k1gInSc1	Labrador
je	být	k5eAaImIp3nS	být
částí	část	k1gFnSc7	část
kanadského	kanadský	k2eAgInSc2d1	kanadský
štítu	štít	k1gInSc2	štít
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
rulami	rula	k1gFnPc7	rula
a	a	k8xC	a
granity	granit	k1gInPc7	granit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
mořské	mořský	k2eAgFnSc2d1	mořská
hladiny	hladina	k1gFnSc2	hladina
z	z	k7c2	z
Hudsonova	Hudsonův	k2eAgInSc2d1	Hudsonův
zálivu	záliv	k1gInSc2	záliv
se	se	k3xPyFc4	se
ve	v	k7c6	v
východním	východní	k2eAgInSc6d1	východní
směru	směr	k1gInSc6	směr
zvedá	zvedat	k5eAaImIp3nS	zvedat
až	až	k9	až
do	do	k7c2	do
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
1652	[number]	k4	1652
m.	m.	k?	m.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Labrador	Labrador	k1gInSc1	Labrador
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Labrador	Labrador	k1gInSc1	Labrador
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
