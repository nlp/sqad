<s>
Vogue	Vogue	k6eAd1	Vogue
je	být	k5eAaImIp3nS	být
časopis	časopis	k1gInSc1	časopis
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c4	na
módu	móda	k1gFnSc4	móda
<g/>
,	,	kIx,	,
design	design	k1gInSc4	design
a	a	k8xC	a
životní	životní	k2eAgInSc4d1	životní
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
ve	v	k7c6	v
dvaceti	dvacet	k4xCc6	dvacet
třech	tři	k4xCgInPc6	tři
zemích	zem	k1gFnPc6	zem
každý	každý	k3xTgInSc4	každý
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
redakci	redakce	k1gFnSc6	redakce
se	se	k3xPyFc4	se
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
světoznámých	světoznámý	k2eAgMnPc2d1	světoznámý
módních	módní	k2eAgMnPc2d1	módní
fotografů	fotograf	k1gMnPc2	fotograf
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
magazínu	magazín	k1gInSc6	magazín
The	The	k1gFnSc2	The
New	New	k1gMnPc2	New
York	York	k1gInSc1	York
Times	Timesa	k1gFnPc2	Timesa
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2006	[number]	k4	2006
popsala	popsat	k5eAaPmAgFnS	popsat
Vogue	Vogue	k1gInSc4	Vogue
literární	literární	k2eAgFnSc1d1	literární
kritička	kritička	k1gFnSc1	kritička
Caroline	Carolin	k1gInSc5	Carolin
Weber	weber	k1gInSc1	weber
jako	jako	k9	jako
"	"	kIx"	"
<g/>
nejvlivnější	vlivný	k2eAgInSc4d3	nejvlivnější
módní	módní	k2eAgInSc4d1	módní
časopis	časopis	k1gInSc4	časopis
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Vogue	Vogue	k1gInSc1	Vogue
publikuje	publikovat	k5eAaBmIp3nS	publikovat
také	také	k9	také
články	článek	k1gInPc4	článek
o	o	k7c6	o
umění	umění	k1gNnSc6	umění
<g/>
,	,	kIx,	,
kultuře	kultura	k1gFnSc6	kultura
a	a	k8xC	a
politice	politika	k1gFnSc6	politika
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
stálých	stálý	k2eAgFnPc2d1	stálá
modelek	modelka	k1gFnPc2	modelka
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
známých	známý	k2eAgFnPc2d1	známá
celebrit	celebrita	k1gFnPc2	celebrita
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
je	být	k5eAaImIp3nS	být
časopis	časopis	k1gInSc1	časopis
také	také	k9	také
kritizován	kritizován	k2eAgMnSc1d1	kritizován
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
vychází	vycházet	k5eAaImIp3nS	vycházet
ve	v	k7c6	v
23	[number]	k4	23
zemích	zem	k1gFnPc6	zem
<g/>
:	:	kIx,	:
Argentina	Argentina	k1gFnSc1	Argentina
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Řecko	Řecko	k1gNnSc1	Řecko
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
<g/>
,	,	kIx,	,
Korea	Korea	k1gFnSc1	Korea
<g/>
,	,	kIx,	,
Mexico	Mexico	k1gNnSc1	Mexico
<g/>
,	,	kIx,	,
Peru	Peru	k1gNnSc1	Peru
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Vogue	Vogue	k1gInSc1	Vogue
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
jako	jako	k9	jako
týdeník	týdeník	k1gInSc4	týdeník
Arthurem	Arthur	k1gMnSc7	Arthur
Baldwinem	Baldwin	k1gMnSc7	Baldwin
Turnurem	Turnur	k1gMnSc7	Turnur
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
převzal	převzít	k5eAaPmAgMnS	převzít
společnost	společnost	k1gFnSc4	společnost
Condé	Condý	k2eAgFnSc2d1	Condý
Nast	Nast	k1gInSc4	Nast
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
změna	změna	k1gFnSc1	změna
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
Nast	Nast	k1gMnSc1	Nast
udělal	udělat	k5eAaPmAgMnS	udělat
<g/>
,	,	kIx,	,
že	že	k8xS	že
změnil	změnit	k5eAaPmAgInS	změnit
periodu	perioda	k1gFnSc4	perioda
výtisků	výtisk	k1gInPc2	výtisk
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
místo	místo	k7c2	místo
jednoho	jeden	k4xCgInSc2	jeden
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
začal	začít	k5eAaPmAgMnS	začít
vydávat	vydávat	k5eAaImF	vydávat
i	i	k9	i
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
postupně	postupně	k6eAd1	postupně
byla	být	k5eAaImAgFnS	být
dobyta	dobyt	k2eAgFnSc1d1	dobyta
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
začala	začít	k5eAaPmAgFnS	začít
dělat	dělat	k5eAaImF	dělat
šéfredaktorku	šéfredaktorka	k1gFnSc4	šéfredaktorka
Diana	Diana	k1gFnSc1	Diana
Vreeland	Vreelanda	k1gFnPc2	Vreelanda
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
dávat	dávat	k5eAaImF	dávat
práci	práce	k1gFnSc4	práce
takovým	takový	k3xDgFnPc3	takový
celebritám	celebrita	k1gFnPc3	celebrita
-	-	kIx~	-
modelkám	modelka	k1gFnPc3	modelka
jako	jako	k8xC	jako
byli	být	k5eAaImAgMnP	být
například	například	k6eAd1	například
Suzy	Suza	k1gFnPc4	Suza
Parker	Parkra	k1gFnPc2	Parkra
<g/>
,	,	kIx,	,
Twiggy	Twigga	k1gFnSc2	Twigga
<g/>
,	,	kIx,	,
Penelope	Penelop	k1gMnSc5	Penelop
Tree	Treus	k1gMnSc5	Treus
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
je	být	k5eAaImIp3nS	být
šéfredaktorkou	šéfredaktorka	k1gFnSc7	šéfredaktorka
americké	americký	k2eAgFnSc2d1	americká
verze	verze	k1gFnSc2	verze
Anna	Anna	k1gFnSc1	Anna
Wintourová	Wintourový	k2eAgFnSc1d1	Wintourová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
redakci	redakce	k1gFnSc6	redakce
se	se	k3xPyFc4	se
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
světoznámých	světoznámý	k2eAgMnPc2d1	světoznámý
módních	módní	k2eAgMnPc2d1	módní
a	a	k8xC	a
reportážních	reportážní	k2eAgMnPc2d1	reportážní
fotografů	fotograf	k1gMnPc2	fotograf
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
André	André	k1gMnSc1	André
Kertész	Kertész	k1gMnSc1	Kertész
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
Doisneau	Doisneaus	k1gInSc2	Doisneaus
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
Steichen	Steichen	k1gInSc1	Steichen
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
Avedon	Avedon	k1gMnSc1	Avedon
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Klein	Klein	k1gMnSc1	Klein
nebo	nebo	k8xC	nebo
Edwin	Edwin	k1gMnSc1	Edwin
Smith	Smith	k1gMnSc1	Smith
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
1913-1921	[number]	k4	1913-1921
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
fotografů	fotograf	k1gMnPc2	fotograf
Adolf	Adolf	k1gMnSc1	Adolf
de	de	k?	de
Meyer	Meyer	k1gMnSc1	Meyer
<g/>
.	.	kIx.	.
</s>
