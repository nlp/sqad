<p>
<s>
Commerce	Commerka	k1gFnSc3	Commerka
Square	square	k1gInSc1	square
je	být	k5eAaImIp3nS	být
komplex	komplex	k1gInSc4	komplex
dvou	dva	k4xCgInPc2	dva
stejných	stejný	k2eAgInPc2d1	stejný
kancelářských	kancelářský	k2eAgInPc2d1	kancelářský
mrakodrapů	mrakodrap	k1gInPc2	mrakodrap
v	v	k7c6	v
pensylvánském	pensylvánský	k2eAgNnSc6d1	pensylvánské
městě	město	k1gNnSc6	město
Filadelfie	Filadelfie	k1gFnSc2	Filadelfie
<g/>
.	.	kIx.	.
</s>
<s>
Budovy	budova	k1gFnPc1	budova
nesou	nést	k5eAaImIp3nP	nést
název	název	k1gInSc4	název
One	One	k1gFnSc2	One
Commerce	Commerka	k1gFnSc6	Commerka
Square	square	k1gInSc4	square
a	a	k8xC	a
Two	Two	k1gFnSc3	Two
Commerce	Commerka	k1gFnSc3	Commerka
Square	square	k1gInSc4	square
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
mají	mít	k5eAaImIp3nP	mít
41	[number]	k4	41
pater	patro	k1gNnPc2	patro
a	a	k8xC	a
výšku	výška	k1gFnSc4	výška
172	[number]	k4	172
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
navrženy	navrhnout	k5eAaPmNgFnP	navrhnout
architektonickou	architektonický	k2eAgFnSc7d1	architektonická
firmou	firma	k1gFnSc7	firma
IM	IM	kA	IM
Pei	Pei	k1gMnSc1	Pei
&	&	k?	&
Partners	Partners	k1gInSc1	Partners
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgMnSc1d1	dnešní
Pei	Pei	k1gMnSc1	Pei
Cobb	Cobb	k1gMnSc1	Cobb
Freed	Freed	k1gMnSc1	Freed
&	&	k?	&
Partners	Partners	k1gInSc1	Partners
<g/>
)	)	kIx)	)
a	a	k8xC	a
výstavba	výstavba	k1gFnSc1	výstavba
1	[number]	k4	1
<g/>
.	.	kIx.	.
věže	věž	k1gFnSc2	věž
probíhala	probíhat	k5eAaImAgNnP	probíhat
v	v	k7c6	v
letech	let	k1gInPc6	let
1985	[number]	k4	1985
<g/>
–	–	k?	–
<g/>
1987	[number]	k4	1987
a	a	k8xC	a
druhé	druhý	k4xOgFnSc2	druhý
věže	věž	k1gFnSc2	věž
v	v	k7c6	v
letech	let	k1gInPc6	let
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
komplex	komplex	k1gInSc1	komplex
disponuje	disponovat	k5eAaBmIp3nS	disponovat
pordlahovou	pordlahový	k2eAgFnSc7d1	pordlahový
plochou	plocha	k1gFnSc7	plocha
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
asi	asi	k9	asi
200	[number]	k4	200
000	[number]	k4	000
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
přibližně	přibližně	k6eAd1	přibližně
170	[number]	k4	170
000	[number]	k4	000
m	m	kA	m
<g/>
2	[number]	k4	2
zabírají	zabírat	k5eAaImIp3nP	zabírat
kancelářské	kancelářský	k2eAgFnPc1d1	kancelářská
prostory	prostora	k1gFnPc1	prostora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
budov	budova	k1gFnPc2	budova
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
</s>
</p>
