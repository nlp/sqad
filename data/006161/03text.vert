<s>
John	John	k1gMnSc1	John
Galsworthy	Galswortha	k1gFnSc2	Galswortha
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1867	[number]	k4	1867
<g/>
,	,	kIx,	,
Kingston	Kingston	k1gInSc1	Kingston
<g/>
,	,	kIx,	,
Surrey	Surrey	k1gInPc1	Surrey
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
anglický	anglický	k2eAgMnSc1d1	anglický
prozaik	prozaik	k1gMnSc1	prozaik
a	a	k8xC	a
dramatik	dramatik	k1gMnSc1	dramatik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
<s>
Galsworthy	Galsworth	k1gInPc1	Galsworth
pocházel	pocházet	k5eAaImAgMnS	pocházet
ze	z	k7c2	z
zámožné	zámožný	k2eAgFnSc2d1	zámožná
právnické	právnický	k2eAgFnSc2d1	právnická
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
studoval	studovat	k5eAaImAgMnS	studovat
námořní	námořní	k2eAgNnSc4d1	námořní
právo	právo	k1gNnSc4	právo
na	na	k7c6	na
nejlepších	dobrý	k2eAgFnPc6d3	nejlepší
anglických	anglický	k2eAgFnPc6d1	anglická
školách	škola	k1gFnPc6	škola
v	v	k7c6	v
Harrow	Harrow	k1gFnSc6	Harrow
a	a	k8xC	a
Oxfordu	Oxford	k1gInSc6	Oxford
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
získal	získat	k5eAaPmAgInS	získat
doktorát	doktorát	k1gInSc1	doktorát
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
advokátní	advokátní	k2eAgFnSc4d1	advokátní
praxi	praxe	k1gFnSc4	praxe
však	však	k9	však
neprovozoval	provozovat	k5eNaImAgInS	provozovat
a	a	k8xC	a
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
s	s	k7c7	s
podporovou	podporový	k2eAgFnSc4d1	podporová
otcových	otcův	k2eAgInPc2d1	otcův
peněz	peníze	k1gInPc2	peníze
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
cestoval	cestovat	k5eAaImAgMnS	cestovat
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1893	[number]	k4	1893
se	se	k3xPyFc4	se
na	na	k7c6	na
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
po	po	k7c6	po
jižních	jižní	k2eAgNnPc6d1	jižní
mořích	moře	k1gNnPc6	moře
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Josephem	Joseph	k1gInSc7	Joseph
Conradem	Conrad	k1gInSc7	Conrad
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k6eAd1	ještě
námořním	námořní	k2eAgMnSc7d1	námořní
důstojníkem	důstojník	k1gMnSc7	důstojník
<g/>
,	,	kIx,	,
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
budoucí	budoucí	k2eAgMnPc1d1	budoucí
spisovatelé	spisovatel	k1gMnPc1	spisovatel
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
důvěrnými	důvěrný	k2eAgMnPc7d1	důvěrný
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
setkání	setkání	k1gNnSc4	setkání
Galsworthyho	Galsworthy	k1gMnSc2	Galsworthy
definitivně	definitivně	k6eAd1	definitivně
přesvědčilo	přesvědčit	k5eAaPmAgNnS	přesvědčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vzdal	vzdát	k5eAaPmAgMnS	vzdát
právnické	právnický	k2eAgFnSc2d1	právnická
kariéry	kariéra	k1gFnSc2	kariéra
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgInS	věnovat
literatuře	literatura	k1gFnSc3	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
citového	citový	k2eAgInSc2d1	citový
vývoje	vývoj	k1gInSc2	vývoj
prudce	prudko	k6eAd1	prudko
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
láska	láska	k1gFnSc1	láska
k	k	k7c3	k
manželce	manželka	k1gFnSc3	manželka
vlastního	vlastní	k2eAgMnSc2d1	vlastní
bratrance	bratranec	k1gMnSc2	bratranec
Adě	Adě	k1gMnSc2	Adě
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ho	on	k3xPp3gMnSc4	on
přivedla	přivést	k5eAaPmAgFnS	přivést
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
nejen	nejen	k6eAd1	nejen
s	s	k7c7	s
příbuzenstvem	příbuzenstvo	k1gNnSc7	příbuzenstvo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
pokryteckou	pokrytecký	k2eAgFnSc7d1	pokrytecká
tzv.	tzv.	kA	tzv.
vysokou	vysoká	k1gFnSc7	vysoká
společností	společnost	k1gFnSc7	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Práci	práce	k1gFnSc4	práce
advokáta	advokát	k1gMnSc2	advokát
se	se	k3xPyFc4	se
Galsworthy	Galswortha	k1gFnSc2	Galswortha
nevěnoval	věnovat	k5eNaImAgInS	věnovat
ani	ani	k8xC	ani
po	po	k7c6	po
otcově	otcův	k2eAgFnSc6d1	otcova
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
rodinný	rodinný	k2eAgInSc4d1	rodinný
majetek	majetek	k1gInSc4	majetek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
celý	celý	k2eAgInSc1d1	celý
zdědil	zdědit	k5eAaPmAgInS	zdědit
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
umožnil	umožnit	k5eAaPmAgInS	umožnit
vést	vést	k5eAaImF	vést
finančně	finančně	k6eAd1	finančně
nezávislý	závislý	k2eNgInSc1d1	nezávislý
život	život	k1gInSc1	život
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
po	po	k7c6	po
letech	léto	k1gNnPc6	léto
útrap	útrapa	k1gFnPc2	útrapa
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
Ada	Ada	kA	Ada
rozvodu	rozvod	k1gInSc2	rozvod
<g/>
,	,	kIx,	,
mohl	moct	k5eAaImAgMnS	moct
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
Galsworthy	Galswortha	k1gFnSc2	Galswortha
roku	rok	k1gInSc2	rok
1905	[number]	k4	1905
oženit	oženit	k5eAaPmF	oženit
<g/>
,	,	kIx,	,
a	a	k8xC	a
toto	tento	k3xDgNnSc1	tento
manželství	manželství	k1gNnSc1	manželství
vydrželo	vydržet	k5eAaPmAgNnS	vydržet
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
jeho	on	k3xPp3gInSc4	on
další	další	k2eAgInSc4d1	další
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
desetiletích	desetiletí	k1gNnPc6	desetiletí
se	se	k3xPyFc4	se
Galsworthy	Galswortha	k1gFnSc2	Galswortha
věnoval	věnovat	k5eAaImAgInS	věnovat
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
literatuře	literatura	k1gFnSc3	literatura
a	a	k8xC	a
postupem	postupem	k7c2	postupem
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
úspěšným	úspěšný	k2eAgMnSc7d1	úspěšný
a	a	k8xC	a
váženým	vážený	k2eAgMnSc7d1	vážený
autorem	autor	k1gMnSc7	autor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
organizace	organizace	k1gFnSc2	organizace
spisovatelů	spisovatel	k1gMnPc2	spisovatel
PEN	PEN	kA	PEN
klub	klub	k1gInSc4	klub
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
jejím	její	k3xOp3gInPc3	její
prvním	první	k4xOgMnSc7	první
předsedou	předseda	k1gMnSc7	předseda
(	(	kIx(	(
<g/>
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
vykonával	vykonávat	k5eAaImAgMnS	vykonávat
až	až	k9	až
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
spisovatel	spisovatel	k1gMnSc1	spisovatel
debutoval	debutovat	k5eAaBmAgMnS	debutovat
Galsworthy	Galswortha	k1gFnPc4	Galswortha
roku	rok	k1gInSc2	rok
1897	[number]	k4	1897
povídkovým	povídkový	k2eAgInSc7d1	povídkový
souborem	soubor	k1gInSc7	soubor
Ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
stran	strana	k1gFnPc2	strana
světa	svět	k1gInSc2	svět
a	a	k8xC	a
brzy	brzy	k6eAd1	brzy
nato	nato	k6eAd1	nato
vydal	vydat	k5eAaPmAgMnS	vydat
své	svůj	k3xOyFgInPc4	svůj
první	první	k4xOgInPc4	první
dva	dva	k4xCgInPc4	dva
nepříliš	příliš	k6eNd1	příliš
úspěšné	úspěšný	k2eAgInPc4d1	úspěšný
romány	román	k1gInPc4	román
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
třetí	třetí	k4xOgInSc1	třetí
román	román	k1gInSc1	román
Ostrov	ostrov	k1gInSc1	ostrov
pokrytců	pokrytec	k1gMnPc2	pokrytec
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
Galsworthy	Galsworth	k1gInPc4	Galsworth
satiricky	satiricky	k6eAd1	satiricky
zobrazil	zobrazit	k5eAaPmAgInS	zobrazit
život	život	k1gInSc1	život
anglické	anglický	k2eAgFnSc2d1	anglická
vysoké	vysoký	k2eAgFnSc2d1	vysoká
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
přežívající	přežívající	k2eAgInSc1d1	přežívající
kastovní	kastovní	k2eAgInSc1d1	kastovní
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
přinesl	přinést	k5eAaPmAgInS	přinést
výraznější	výrazný	k2eAgInSc4d2	výraznější
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Povzbuzen	povzbuzen	k2eAgInSc1d1	povzbuzen
úspěchem	úspěch	k1gInSc7	úspěch
díla	dílo	k1gNnPc4	dílo
vydal	vydat	k5eAaPmAgMnS	vydat
pak	pak	k6eAd1	pak
Galsworthy	Galsworth	k1gInPc4	Galsworth
během	během	k7c2	během
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
dalších	další	k2eAgMnPc2d1	další
pěti	pět	k4xCc2	pět
románů	román	k1gInPc2	román
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
zkoumal	zkoumat	k5eAaImAgInS	zkoumat
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
vrstvy	vrstva	k1gFnPc4	vrstva
vysoké	vysoký	k2eAgFnSc3d1	vysoká
anglické	anglický	k2eAgFnSc3d1	anglická
společnosti	společnost	k1gFnSc3	společnost
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gInPc2	jejich
charakteristických	charakteristický	k2eAgInPc2d1	charakteristický
sociálních	sociální	k2eAgInPc2d1	sociální
rysů	rys	k1gInPc2	rys
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vlastníkovi	vlastník	k1gMnSc6	vlastník
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prvním	první	k4xOgInSc6	první
díle	díl	k1gInSc6	díl
jeho	jeho	k3xOp3gFnSc2	jeho
pozdější	pozdní	k2eAgFnSc2d2	pozdější
románové	románový	k2eAgFnSc2d1	románová
trilogie	trilogie	k1gFnSc2	trilogie
Sága	ságo	k1gNnSc2	ságo
rodu	rod	k1gInSc2	rod
Forsytů	Forsyt	k1gInPc2	Forsyt
to	ten	k3xDgNnSc1	ten
byli	být	k5eAaImAgMnP	být
velkoobchodníci	velkoobchodník	k1gMnPc1	velkoobchodník
<g/>
,	,	kIx,	,
advokáti	advokát	k1gMnPc1	advokát
a	a	k8xC	a
finančníci	finančník	k1gMnPc1	finančník
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Venkovském	venkovský	k2eAgNnSc6d1	venkovské
sídle	sídlo	k1gNnSc6	sídlo
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
statkářské	statkářský	k2eAgNnSc1d1	statkářské
panstvo	panstvo	k1gNnSc1	panstvo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Bratrství	bratrství	k1gNnSc6	bratrství
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
umělecká	umělecký	k2eAgFnSc1d1	umělecká
inteligence	inteligence	k1gFnSc1	inteligence
<g/>
,	,	kIx,	,
v	v	k7c6	v
Patriciji	patricij	k1gMnSc6	patricij
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
vysoká	vysoký	k2eAgFnSc1d1	vysoká
šlechta	šlechta	k1gFnSc1	šlechta
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
Tmavém	tmavý	k2eAgInSc6d1	tmavý
květu	květ	k1gInSc6	květ
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
opět	opět	k6eAd1	opět
umělci	umělec	k1gMnPc1	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
vydal	vydat	k5eAaPmAgMnS	vydat
Galsworthy	Galsworth	k1gInPc4	Galsworth
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
díly	díl	k1gInPc4	díl
své	svůj	k3xOyFgFnSc2	svůj
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
forsytovské	forsytovský	k2eAgFnSc2d1	forsytovský
kroniky	kronika	k1gFnSc2	kronika
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
román	román	k1gInSc4	román
V	v	k7c6	v
pasti	past	k1gFnSc6	past
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
pak	pak	k6eAd1	pak
román	román	k1gInSc4	román
K	k	k7c3	k
pronajmutí	pronajmutí	k1gNnSc3	pronajmutí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
třetího	třetí	k4xOgInSc2	třetí
dílu	díl	k1gInSc2	díl
se	se	k3xPyFc4	se
Galsworthy	Galsworth	k1gInPc1	Galsworth
takřka	takřka	k6eAd1	takřka
okamžitě	okamžitě	k6eAd1	okamžitě
zařadil	zařadit	k5eAaPmAgMnS	zařadit
mezi	mezi	k7c4	mezi
nejuznávanější	uznávaný	k2eAgMnPc4d3	nejuznávanější
anglicky	anglicky	k6eAd1	anglicky
píšící	píšící	k2eAgMnPc4d1	píšící
autory	autor	k1gMnPc4	autor
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
své	svůj	k3xOyFgFnSc2	svůj
trilogie	trilogie	k1gFnSc2	trilogie
<g/>
,	,	kIx,	,
prvně	prvně	k?	prvně
souhrnně	souhrnně	k6eAd1	souhrnně
vydané	vydaný	k2eAgNnSc4d1	vydané
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Sága	ságo	k1gNnSc2	ságo
rodu	rod	k1gInSc2	rod
Forsytů	Forsyt	k1gInPc2	Forsyt
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
Galsworthy	Galswortha	k1gFnSc2	Galswortha
navázal	navázat	k5eAaPmAgInS	navázat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1924	[number]	k4	1924
<g/>
–	–	k?	–
<g/>
1928	[number]	k4	1928
trilogií	trilogie	k1gFnPc2	trilogie
další	další	k2eAgFnSc4d1	další
<g/>
,	,	kIx,	,
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Moderní	moderní	k2eAgFnSc4d1	moderní
komedie	komedie	k1gFnPc4	komedie
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
rozsáhlé	rozsáhlý	k2eAgNnSc4d1	rozsáhlé
vyprávění	vyprávění	k1gNnSc4	vyprávění
na	na	k7c4	na
forsytovské	forsytovský	k2eAgNnSc4d1	forsytovský
téma	téma	k1gNnSc4	téma
pak	pak	k6eAd1	pak
zakončil	zakončit	k5eAaPmAgInS	zakončit
trilogií	trilogie	k1gFnSc7	trilogie
Konec	konec	k1gInSc1	konec
kapitoly	kapitola	k1gFnSc2	kapitola
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1931	[number]	k4	1931
<g/>
–	–	k?	–
<g/>
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
tvorbě	tvorba	k1gFnSc6	tvorba
spojil	spojit	k5eAaPmAgMnS	spojit
Galsworthy	Galswortha	k1gFnPc4	Galswortha
lehce	lehko	k6eAd1	lehko
sžíravou	sžíravý	k2eAgFnSc4d1	sžíravá
a	a	k8xC	a
sociálně	sociálně	k6eAd1	sociálně
laděnou	laděný	k2eAgFnSc4d1	laděná
kritiku	kritika	k1gFnSc4	kritika
života	život	k1gInSc2	život
anglické	anglický	k2eAgFnSc2d1	anglická
aristokracie	aristokracie	k1gFnSc2	aristokracie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pod	pod	k7c7	pod
maskou	maska	k1gFnSc7	maska
kultivovanosti	kultivovanost	k1gFnSc2	kultivovanost
projevuje	projevovat	k5eAaImIp3nS	projevovat
své	svůj	k3xOyFgNnSc4	svůj
přízemní	přízemní	k2eAgNnSc4d1	přízemní
sobectví	sobectví	k1gNnSc4	sobectví
a	a	k8xC	a
touhu	touha	k1gFnSc4	touha
po	po	k7c6	po
hromadění	hromadění	k1gNnSc6	hromadění
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
s	s	k7c7	s
poněkud	poněkud	k6eAd1	poněkud
abstraktní	abstraktní	k2eAgFnSc7d1	abstraktní
etickou	etický	k2eAgFnSc7d1	etická
touhou	touha	k1gFnSc7	touha
po	po	k7c6	po
větší	veliký	k2eAgFnSc6d2	veliký
sociální	sociální	k2eAgFnSc6d1	sociální
spravedlnosti	spravedlnost	k1gFnSc6	spravedlnost
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
základě	základ	k1gInSc6	základ
naznačoval	naznačovat	k5eAaImAgMnS	naznačovat
možnosti	možnost	k1gFnPc4	možnost
morální	morální	k2eAgFnSc2d1	morální
reformy	reforma	k1gFnSc2	reforma
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
se	se	k3xPyFc4	se
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
díle	díl	k1gInSc6	díl
s	s	k7c7	s
přibývajícím	přibývající	k2eAgInSc7d1	přibývající
roky	rok	k1gInPc4	rok
projevilo	projevit	k5eAaPmAgNnS	projevit
postupné	postupný	k2eAgNnSc1d1	postupné
slábnutí	slábnutí	k1gNnSc1	slábnutí
ironie	ironie	k1gFnSc2	ironie
a	a	k8xC	a
kritiky	kritika	k1gFnSc2	kritika
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
jej	on	k3xPp3gMnSc4	on
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
velkého	velký	k2eAgMnSc4d1	velký
vyprávěče	vyprávěč	k1gMnSc4	vyprávěč
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přenesl	přenést	k5eAaPmAgMnS	přenést
tradice	tradice	k1gFnPc4	tradice
anglického	anglický	k2eAgInSc2d1	anglický
kritického	kritický	k2eAgInSc2d1	kritický
realismu	realismus	k1gInSc2	realismus
do	do	k7c2	do
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
životní	životní	k2eAgNnSc1d1	životní
dílo	dílo	k1gNnSc1	dílo
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
oceněno	ocenit	k5eAaPmNgNnS	ocenit
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
za	za	k7c4	za
"	"	kIx"	"
<g/>
za	za	k7c4	za
vynikající	vynikající	k2eAgNnSc4d1	vynikající
vypravěčské	vypravěčský	k2eAgNnSc4d1	vypravěčské
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
stupně	stupeň	k1gInSc2	stupeň
dokonalosti	dokonalost	k1gFnSc2	dokonalost
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Sága	ságo	k1gNnSc2	ságo
rodu	rod	k1gInSc2	rod
Forsytů	Forsyt	k1gInPc2	Forsyt
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
citace	citace	k1gFnPc1	citace
z	z	k7c2	z
odůvodnění	odůvodnění	k1gNnSc2	odůvodnění
Švédské	švédský	k2eAgFnSc2d1	švédská
akademie	akademie	k1gFnSc2	akademie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
From	From	k1gInSc1	From
the	the	k?	the
Four	Four	k1gInSc1	Four
Winds	Winds	k1gInSc1	Winds
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
Ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
stran	strana	k1gFnPc2	strana
světa	svět	k1gInSc2	svět
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc4	soubor
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
Jocelyn	Jocelyna	k1gFnPc2	Jocelyna
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
Villa	Villa	k1gMnSc1	Villa
Rubein	Rubein	k1gMnSc1	Rubein
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
Vila	vila	k1gFnSc1	vila
Rubein	Rubein	k1gMnSc1	Rubein
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Island	Island	k1gInSc1	Island
Pharisees	Pharisees	k1gInSc1	Pharisees
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
Ostrov	ostrov	k1gInSc1	ostrov
pokrytců	pokrytec	k1gMnPc2	pokrytec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
Galsworthy	Galsworth	k1gInPc4	Galsworth
satiricky	satiricky	k6eAd1	satiricky
zobrazil	zobrazit	k5eAaPmAgInS	zobrazit
život	život	k1gInSc1	život
anglické	anglický	k2eAgFnSc2d1	anglická
vysoké	vysoký	k2eAgFnSc2d1	vysoká
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
přežívající	přežívající	k2eAgInSc1d1	přežívající
kastovní	kastovní	k2eAgInSc1d1	kastovní
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
ničí	ničit	k5eAaImIp3nS	ničit
štěstí	štěstí	k1gNnSc4	štěstí
citově	citově	k6eAd1	citově
a	a	k8xC	a
tvořivě	tvořivě	k6eAd1	tvořivě
založených	založený	k2eAgMnPc2d1	založený
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
knihy	kniha	k1gFnSc2	kniha
Richard	Richard	k1gMnSc1	Richard
Shelton	Shelton	k1gInSc4	Shelton
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgMnSc1d1	typický
představitel	představitel	k1gMnSc1	představitel
anglické	anglický	k2eAgFnSc2d1	anglická
privilegované	privilegovaný	k2eAgFnSc2d1	privilegovaná
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zasnouben	zasnouben	k2eAgMnSc1d1	zasnouben
s	s	k7c7	s
dívkou	dívka	k1gFnSc7	dívka
ze	z	k7c2	z
zámožné	zámožný	k2eAgFnSc2d1	zámožná
aristokratické	aristokratický	k2eAgFnSc2d1	aristokratická
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
celý	celý	k2eAgInSc1d1	celý
děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
několika	několik	k4yIc2	několik
měsíců	měsíc	k1gInPc2	měsíc
jejich	jejich	k3xOp3gFnPc2	jejich
zásnub	zásnuba	k1gFnPc2	zásnuba
<g/>
.	.	kIx.	.
</s>
<s>
Shelton	Shelton	k1gInSc1	Shelton
náhodou	náhodou	k6eAd1	náhodou
pozná	poznat	k5eAaPmIp3nS	poznat
poměry	poměr	k1gInPc4	poměr
panující	panující	k2eAgInPc4d1	panující
v	v	k7c6	v
chudinských	chudinský	k2eAgFnPc6d1	chudinská
čtvrtích	čtvrt	k1gFnPc6	čtvrt
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
mu	on	k3xPp3gMnSc3	on
otevře	otevřít	k5eAaPmIp3nS	otevřít
oči	oko	k1gNnPc4	oko
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
uvědomí	uvědomit	k5eAaPmIp3nS	uvědomit
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
prohnilost	prohnilost	k1gFnSc4	prohnilost
a	a	k8xC	a
faleš	faleš	k1gFnSc4	faleš
"	"	kIx"	"
<g/>
úctyhodné	úctyhodný	k2eAgFnPc1d1	úctyhodná
<g/>
"	"	kIx"	"
společnosti	společnost	k1gFnPc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
rozpor	rozpor	k1gInSc1	rozpor
vede	vést	k5eAaImIp3nS	vést
až	až	k9	až
k	k	k7c3	k
zrušení	zrušení	k1gNnSc3	zrušení
jeho	jeho	k3xOp3gNnPc2	jeho
zasnoubení	zasnoubení	k1gNnPc2	zasnoubení
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Shelton	Shelton	k1gInSc1	Shelton
zvítězí	zvítězit	k5eAaPmIp3nS	zvítězit
nad	nad	k7c7	nad
svým	svůj	k3xOyFgInSc7	svůj
sklonem	sklon	k1gInSc7	sklon
ke	k	k7c3	k
kompromismu	kompromismus	k1gInSc3	kompromismus
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Salvation	Salvation	k1gInSc1	Salvation
of	of	k?	of
a	a	k8xC	a
Forsyte	Forsyt	k1gInSc5	Forsyt
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
,	,	kIx,	,
Forsytovo	Forsytův	k2eAgNnSc1d1	Forsytův
nanebevzetí	nanebevzetí	k1gNnSc1	nanebevzetí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
Galsworthy	Galswortha	k1gFnSc2	Galswortha
připravuje	připravovat	k5eAaImIp3nS	připravovat
na	na	k7c4	na
Ságu	sága	k1gFnSc4	sága
rodu	rod	k1gInSc2	rod
Forsytů	Forsyt	k1gInPc2	Forsyt
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Silver	Silver	k1gInSc1	Silver
Box	box	k1gInSc1	box
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
Stříbrné	stříbrný	k2eAgNnSc1d1	stříbrné
pouzdro	pouzdro	k1gNnSc1	pouzdro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
zobrazující	zobrazující	k2eAgFnSc4d1	zobrazující
britskou	britský	k2eAgFnSc4d1	britská
justici	justice	k1gFnSc4	justice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
jiný	jiný	k2eAgInSc4d1	jiný
loket	loket	k1gInSc4	loket
pro	pro	k7c4	pro
bohatce	bohatec	k1gMnPc4	bohatec
a	a	k8xC	a
jiný	jiný	k2eAgInSc1d1	jiný
pro	pro	k7c4	pro
chudáky	chudák	k1gMnPc4	chudák
<g/>
.	.	kIx.	.
</s>
<s>
Strife	Strif	k1gMnSc5	Strif
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
Svár	svár	k1gInSc1	svár
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
odehrávající	odehrávající	k2eAgFnSc1d1	odehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
dramatickém	dramatický	k2eAgNnSc6d1	dramatické
období	období	k1gNnSc6	období
stávky	stávka	k1gFnSc2	stávka
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Man	Man	k1gMnSc1	Man
of	of	k?	of
Property	Propert	k1gMnPc7	Propert
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
Vlastník	vlastník	k1gMnSc1	vlastník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
také	také	k9	také
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Bohatec	bohatec	k1gMnSc1	bohatec
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
mnohých	mnohý	k2eAgInPc2d1	mnohý
názorů	názor	k1gInPc2	názor
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
autorův	autorův	k2eAgInSc4d1	autorův
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
Galsworthyho	Galsworthy	k1gMnSc2	Galsworthy
trilogie	trilogie	k1gFnSc2	trilogie
Sága	ságo	k1gNnSc2	ságo
rodu	rod	k1gInSc2	rod
Forsytů	Forsyt	k1gInPc2	Forsyt
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Country	country	k2eAgNnSc1d1	country
House	house	k1gNnSc1	house
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
,	,	kIx,	,
Venkovské	venkovský	k2eAgNnSc1d1	venkovské
sídlo	sídlo	k1gNnSc1	sídlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
analýza	analýza	k1gFnSc1	analýza
pokrytectví	pokrytectví	k1gNnSc2	pokrytectví
a	a	k8xC	a
konzervatizmu	konzervatizmus	k1gInSc2	konzervatizmus
anglických	anglický	k2eAgFnPc2d1	anglická
vyšších	vysoký	k2eAgFnPc2d2	vyšší
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
reprezentovaných	reprezentovaný	k2eAgFnPc2d1	reprezentovaná
v	v	k7c6	v
románě	román	k1gInSc6	román
především	především	k9	především
rodinou	rodina	k1gFnSc7	rodina
venkovského	venkovský	k2eAgMnSc4d1	venkovský
šlechtice	šlechtic	k1gMnSc4	šlechtic
Horáce	Horác	k1gMnSc4	Horác
Peudyce	Peudyce	k1gMnSc4	Peudyce
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc4	konflikt
jeho	on	k3xPp3gMnSc2	on
syna	syn	k1gMnSc2	syn
Jiřího	Jiří	k1gMnSc2	Jiří
s	s	k7c7	s
rodinnými	rodinný	k2eAgFnPc7d1	rodinná
tradicemi	tradice	k1gFnPc7	tradice
je	být	k5eAaImIp3nS	být
však	však	k9	však
vyřešen	vyřešit	k5eAaPmNgInS	vyřešit
smírně	smírně	k6eAd1	smírně
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Commentary	Commentara	k1gFnPc1	Commentara
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
Komentář	komentář	k1gInSc1	komentář
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Joy	Joy	k1gMnSc1	Joy
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
Fraternity	fraternita	k1gFnSc2	fraternita
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
,	,	kIx,	,
Bratrství	bratrství	k1gNnSc1	bratrství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
odehrávající	odehrávající	k2eAgInSc4d1	odehrávající
se	se	k3xPyFc4	se
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
londýnské	londýnský	k2eAgFnSc2d1	londýnská
umělecké	umělecký	k2eAgFnSc2d1	umělecká
inteligence	inteligence	k1gFnSc2	inteligence
<g/>
,	,	kIx,	,
Justice	justice	k1gFnSc2	justice
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
Spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pochmurná	pochmurný	k2eAgFnSc1d1	pochmurná
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
svou	svůj	k3xOyFgFnSc7	svůj
scénou	scéna	k1gFnSc7	scéna
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
nervově	nervově	k6eAd1	nervově
zhroucený	zhroucený	k2eAgMnSc1d1	zhroucený
vězeň	vězeň	k1gMnSc1	vězeň
nelidsky	lidsky	k6eNd1	lidsky
trpí	trpět	k5eAaImIp3nS	trpět
v	v	k7c6	v
samovazbě	samovazba	k1gFnSc6	samovazba
<g/>
,	,	kIx,	,
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
reformu	reforma	k1gFnSc4	reforma
anglického	anglický	k2eAgInSc2d1	anglický
vězeňského	vězeňský	k2eAgInSc2d1	vězeňský
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Patrician	Patrician	k1gInSc1	Patrician
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
Patricij	patricij	k1gMnSc1	patricij
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
nejvyšších	vysoký	k2eAgInPc2d3	Nejvyšší
kruhů	kruh	k1gInPc2	kruh
anglické	anglický	k2eAgFnSc2d1	anglická
šlechty	šlechta	k1gFnSc2	šlechta
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
první	první	k4xOgFnSc7	první
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
líčící	líčící	k2eAgFnSc4d1	líčící
mileneckou	milenecký	k2eAgFnSc4d1	milenecká
dvojici	dvojice	k1gFnSc4	dvojice
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
láska	láska	k1gFnSc1	láska
ztroskotává	ztroskotávat	k5eAaImIp3nS	ztroskotávat
na	na	k7c6	na
tvrdých	tvrdý	k2eAgInPc6d1	tvrdý
zákonech	zákon	k1gInPc6	zákon
této	tento	k3xDgFnSc2	tento
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgInPc4d1	vlastní
pojmy	pojem	k1gInPc4	pojem
cti	čest	k1gFnSc2	čest
a	a	k8xC	a
neřesti	neřest	k1gFnSc2	neřest
a	a	k8xC	a
která	který	k3yIgFnSc1	který
za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
okolností	okolnost	k1gFnPc2	okolnost
nedovoluje	dovolovat	k5eNaImIp3nS	dovolovat
spojit	spojit	k5eAaPmF	spojit
společenské	společenský	k2eAgNnSc4d1	společenské
postavení	postavení	k1gNnSc4	postavení
a	a	k8xC	a
osobní	osobní	k2eAgNnSc4d1	osobní
štěstí	štěstí	k1gNnSc4	štěstí
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Inn	Inn	k1gFnSc1	Inn
of	of	k?	of
Tranquillity	Tranquillita	k1gFnSc2	Tranquillita
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
Hostinec	hostinec	k1gInSc1	hostinec
klidu	klid	k1gInSc2	klid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
<g/>
,	,	kIx,	,
The	The	k1gFnPc1	The
Dark	Dark	k1gMnSc1	Dark
Flower	Flower	k1gMnSc1	Flower
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
Tmavý	tmavý	k2eAgInSc4d1	tmavý
květ	květ	k1gInSc4	květ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
čtyřportrét	čtyřportrét	k1gMnSc1	čtyřportrét
sochaře	sochař	k1gMnSc2	sochař
v	v	k7c6	v
mezních	mezní	k2eAgFnPc6d1	mezní
situacích	situace	k1gFnPc6	situace
jeho	jeho	k3xOp3gInSc2	jeho
tvůrčího	tvůrčí	k2eAgInSc2d1	tvůrčí
zápasu	zápas	k1gInSc2	zápas
<g/>
.	.	kIx.	.
</s>
<s>
Láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
symbolizovaná	symbolizovaný	k2eAgNnPc4d1	symbolizované
tmavým	tmavý	k2eAgInSc7d1	tmavý
květem	květ	k1gInSc7	květ
a	a	k8xC	a
představující	představující	k2eAgInSc1d1	představující
vztah	vztah	k1gInSc1	vztah
mladého	mladý	k2eAgMnSc2d1	mladý
umělce	umělec	k1gMnSc2	umělec
k	k	k7c3	k
ženě	žena	k1gFnSc3	žena
jeho	jeho	k3xOp3gMnSc2	jeho
profesora	profesor	k1gMnSc2	profesor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
vstupním	vstupní	k2eAgInSc7d1	vstupní
milostným	milostný	k2eAgInSc7d1	milostný
příběhem	příběh	k1gInSc7	příběh
<g/>
,	,	kIx,	,
zažehujícím	zažehující	k2eAgInSc7d1	zažehující
několik	několik	k4yIc4	několik
milostných	milostný	k2eAgInPc2d1	milostný
vztahů	vztah	k1gInPc2	vztah
<g/>
,	,	kIx,	,
provázejících	provázející	k2eAgInPc2d1	provázející
umělce	umělec	k1gMnPc4	umělec
jako	jako	k9	jako
tvůrčí	tvůrčí	k2eAgFnSc2d1	tvůrčí
inspirace	inspirace	k1gFnSc2	inspirace
během	během	k7c2	během
celého	celý	k2eAgInSc2d1	celý
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Milostné	milostný	k2eAgInPc1d1	milostný
příběhy	příběh	k1gInPc1	příběh
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
jen	jen	k9	jen
zajímavým	zajímavý	k2eAgInSc7d1	zajímavý
epickým	epický	k2eAgInSc7d1	epický
prvkem	prvek	k1gInSc7	prvek
románové	románový	k2eAgFnSc2d1	románová
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
slouží	sloužit	k5eAaImIp3nS	sloužit
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
zobrazení	zobrazení	k1gNnSc4	zobrazení
složitosti	složitost	k1gFnSc2	složitost
lidských	lidský	k2eAgInPc2d1	lidský
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Freelands	Freelands	k1gInSc1	Freelands
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
Rod	rod	k1gInSc1	rod
Freelandů	Freeland	k1gInPc2	Freeland
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Apple	Apple	kA	Apple
Tree	Tree	k1gFnSc1	Tree
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
Jabloň	jabloň	k1gFnSc1	jabloň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novela	novela	k1gFnSc1	novela
<g/>
,	,	kIx,	,
Beyond	Beyond	k1gInSc1	Beyond
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
Věčnost	věčnost	k1gFnSc1	věčnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
jako	jako	k9	jako
Silnější	silný	k2eAgFnSc1d2	silnější
než	než	k8xS	než
smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
Five	Five	k1gFnSc1	Five
Tales	Tales	k1gMnSc1	Tales
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
,	,	kIx,	,
Pět	pět	k4xCc1	pět
povídek	povídka	k1gFnPc2	povídka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povídkový	povídkový	k2eAgInSc1d1	povídkový
soubor	soubor	k1gInSc1	soubor
obsahující	obsahující	k2eAgInSc1d1	obsahující
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
povídku	povídka	k1gFnSc4	povídka
Indian	Indiana	k1gFnPc2	Indiana
Summer	Summer	k1gInSc4	Summer
of	of	k?	of
a	a	k8xC	a
Forsyte	Forsyt	k1gInSc5	Forsyt
(	(	kIx(	(
<g/>
Forsytovo	Forsytův	k2eAgNnSc4d1	Forsytův
babí	babí	k2eAgNnSc4d1	babí
léto	léto	k1gNnSc4	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
autor	autor	k1gMnSc1	autor
zařadil	zařadit	k5eAaPmAgMnS	zařadit
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
jako	jako	k8xC	jako
první	první	k4xOgFnSc4	první
mezihru	mezihra	k1gFnSc4	mezihra
do	do	k7c2	do
Ságy	sága	k1gFnSc2	sága
rody	rod	k1gInPc4	rod
Forsytů	Forsyt	k1gInPc2	Forsyt
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Saint	Saint	k1gInSc1	Saint
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Progress	Progressa	k1gFnPc2	Progressa
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
Světec	světec	k1gMnSc1	světec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novela	novela	k1gFnSc1	novela
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Skin	skin	k1gMnSc1	skin
Game	game	k1gInSc1	game
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
Podfuk	Podfuk	k?	Podfuk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c4	na
nepřátelství	nepřátelství	k1gNnSc4	nepřátelství
dvou	dva	k4xCgFnPc2	dva
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
Chancery	Chancera	k1gFnPc1	Chancera
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
V	v	k7c6	v
pasti	past	k1gFnSc6	past
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
V	v	k7c6	v
osidlech	osidlo	k1gNnPc6	osidlo
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
díl	díl	k1gInSc4	díl
Galsworthyho	Galsworthy	k1gMnSc2	Galsworthy
trilogie	trilogie	k1gFnSc2	trilogie
Sága	ságo	k1gNnSc2	ságo
rodu	rod	k1gInSc2	rod
Forsytů	Forsyt	k1gInPc2	Forsyt
<g/>
.	.	kIx.	.
</s>
<s>
Awakening	Awakening	k1gInSc1	Awakening
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
Probuzení	probuzení	k1gNnSc1	probuzení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
autor	autor	k1gMnSc1	autor
zařadil	zařadit	k5eAaPmAgMnS	zařadit
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
jako	jako	k8xS	jako
druhou	druhý	k4xOgFnSc4	druhý
mezihru	mezihra	k1gFnSc4	mezihra
do	do	k7c2	do
Ságy	sága	k1gFnSc2	sága
rody	rod	k1gInPc4	rod
Forsytů	Forsyt	k1gInPc2	Forsyt
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
Let	let	k1gInSc1	let
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
K	k	k7c3	k
pronajmutí	pronajmutí	k1gNnSc3	pronajmutí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgInSc4	třetí
díl	díl	k1gInSc4	díl
Galsworthyho	Galsworthy	k1gMnSc2	Galsworthy
trilogie	trilogie	k1gFnSc2	trilogie
Sága	ságo	k1gNnSc2	ságo
rodu	rod	k1gInSc2	rod
Forsytů	Forsyt	k1gInPc2	Forsyt
<g/>
.	.	kIx.	.
</s>
<s>
Loyalties	Loyalties	k1gInSc1	Loyalties
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
Oddanost	oddanost	k1gFnSc1	oddanost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Forsyte	Forsyt	k1gInSc5	Forsyt
Saga	Sagum	k1gNnPc1	Sagum
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
Sága	sága	k1gFnSc1	sága
rodu	rod	k1gInSc2	rod
Forsytů	Forsyt	k1gInPc2	Forsyt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc4d1	souborné
vydání	vydání	k1gNnSc4	vydání
trilogie	trilogie	k1gFnSc2	trilogie
líčící	líčící	k2eAgInPc1d1	líčící
osudy	osud	k1gInPc1	osud
tří	tři	k4xCgFnPc2	tři
generací	generace	k1gFnPc2	generace
patricijského	patricijský	k2eAgInSc2d1	patricijský
rodu	rod	k1gInSc2	rod
Forsytů	Forsyt	k1gInPc2	Forsyt
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
až	až	k9	až
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
dvacátých	dvacátý	k4xOgNnPc2	dvacátý
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Old	Olda	k1gFnPc2	Olda
English	Englisha	k1gFnPc2	Englisha
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
Starý	starý	k2eAgMnSc1d1	starý
Angličan	Angličan	k1gMnSc1	Angličan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc4	soubor
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
Caravan	Caravan	k1gMnSc1	Caravan
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
Karavana	karavana	k1gFnSc1	karavana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc4	soubor
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
Escape	Escap	k1gMnSc5	Escap
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
Útěk	útěk	k1gInSc1	útěk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
<g/>
,	,	kIx,	,
Verses	Verses	k1gMnSc1	Verses
New	New	k1gFnSc2	New
<g />
.	.	kIx.	.
</s>
<s>
and	and	k?	and
Old	Olda	k1gFnPc2	Olda
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgInPc1d1	Nové
a	a	k8xC	a
staré	starý	k2eAgInPc1d1	starý
verše	verš	k1gInPc1	verš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sbírka	sbírka	k1gFnSc1	sbírka
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
A	a	k9	a
Modern	Modern	k1gInSc1	Modern
Comedy	Comeda	k1gMnSc2	Comeda
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
Moderní	moderní	k2eAgFnSc1d1	moderní
komedie	komedie	k1gFnSc1	komedie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
Galsworthyho	Galsworthy	k1gMnSc2	Galsworthy
románová	románový	k2eAgFnSc1d1	románová
trilogie	trilogie	k1gFnSc1	trilogie
o	o	k7c6	o
Forsytech	Forsyt	k1gInPc6	Forsyt
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
autor	autor	k1gMnSc1	autor
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
líčení	líčení	k1gNnSc6	líčení
osudů	osud	k1gInPc2	osud
třetí	třetí	k4xOgFnSc2	třetí
a	a	k8xC	a
čtvrté	čtvrtý	k4xOgFnSc2	čtvrtý
generace	generace	k1gFnSc2	generace
rodu	rod	k1gInSc2	rod
v	v	k7c6	v
letech	let	k1gInPc6	let
1922	[number]	k4	1922
<g/>
–	–	k?	–
<g/>
1926	[number]	k4	1926
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
románů	román	k1gInPc2	román
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
autor	autor	k1gMnSc1	autor
při	při	k7c6	při
jejím	její	k3xOp3gNnSc6	její
prvním	první	k4xOgNnSc6	první
souhrnnem	souhrnn	k1gMnSc7	souhrnn
vydání	vydání	k1gNnSc2	vydání
zařadil	zařadit	k5eAaPmAgMnS	zařadit
ještě	ještě	k9	ještě
dvě	dva	k4xCgFnPc4	dva
mezihry	mezihra	k1gFnPc4	mezihra
<g/>
.	.	kIx.	.
</s>
<s>
Názvy	název	k1gInPc1	název
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
části	část	k1gFnPc4	část
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
The	The	k1gMnSc5	The
White	Whit	k1gMnSc5	Whit
Monkey	Monkea	k1gMnSc2	Monkea
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
Bílá	bílý	k2eAgFnSc1d1	bílá
opice	opice	k1gFnSc1	opice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
A	a	k9	a
Silent	Silent	k1gInSc1	Silent
Wooing	Wooing	k1gInSc1	Wooing
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
Tiché	Tiché	k2eAgNnSc1d1	Tiché
vábení	vábení	k1gNnSc1	vábení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
mezihra	mezihra	k1gFnSc1	mezihra
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Silver	Silver	k1gMnSc1	Silver
Spoon	Spoon	k1gMnSc1	Spoon
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
lžička	lžička	k1gFnSc1	lžička
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
Passers	Passers	k1gInSc1	Passers
By	by	k9	by
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
Kolemjdoucí	kolemjdoucí	k1gMnSc1	kolemjdoucí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
povídka	povídka	k1gFnSc1	povídka
<g/>
,	,	kIx,	,
mezihra	mezihra	k1gFnSc1	mezihra
<g/>
,	,	kIx,	,
Swan	Swan	k1gInSc1	Swan
Song	song	k1gInSc1	song
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
Labutí	labutí	k2eAgFnSc1d1	labutí
píseň	píseň	k1gFnSc1	píseň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
také	také	k9	také
jako	jako	k9	jako
Labutí	labutí	k2eAgInSc4d1	labutí
zpěv	zpěv	k1gInSc4	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Chance	Chanec	k1gInPc1	Chanec
on	on	k3xPp3gMnSc1	on
Forsyte	Forsyt	k1gInSc5	Forsyt
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
Na	na	k7c6	na
burze	burza	k1gFnSc6	burza
Forsytů	Forsyt	k1gInPc2	Forsyt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc4	soubor
povídek	povídka	k1gFnPc2	povídka
s	s	k7c7	s
forsytovskou	forsytovský	k2eAgFnSc7d1	forsytovský
tematikou	tematika	k1gFnSc7	tematika
<g/>
.	.	kIx.	.
</s>
<s>
Two	Two	k?	Two
Essays	Essays	k1gInSc1	Essays
on	on	k3xPp3gInSc1	on
Conrad	Conrad	k1gInSc1	Conrad
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
Dvě	dva	k4xCgFnPc1	dva
eseje	esej	k1gFnPc1	esej
o	o	k7c6	o
Conradovi	Conrada	k1gMnSc6	Conrada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
eseje	esej	k1gInPc1	esej
<g/>
,	,	kIx,	,
End	End	k1gFnPc1	End
of	of	k?	of
the	the	k?	the
Chapter	Chapter	k1gInSc1	Chapter
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
Konec	konec	k1gInSc1	konec
kapitoly	kapitola	k1gFnSc2	kapitola
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
též	též	k9	též
jako	jako	k9	jako
Poslední	poslední	k2eAgFnSc1d1	poslední
kapitola	kapitola	k1gFnSc1	kapitola
<g/>
,	,	kIx,	,
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
Galsworthyho	Galsworthy	k1gMnSc2	Galsworthy
románová	románový	k2eAgFnSc1d1	románová
trilogie	trilogie	k1gFnSc1	trilogie
týkající	týkající	k2eAgFnSc1d1	týkající
se	se	k3xPyFc4	se
rodu	rod	k1gInSc2	rod
Forsytů	Forsyt	k1gInPc2	Forsyt
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
vydaná	vydaný	k2eAgFnSc1d1	vydaná
souhrnně	souhrnně	k6eAd1	souhrnně
až	až	k9	až
po	po	k7c6	po
autorově	autorův	k2eAgFnSc6d1	autorova
smrti	smrt	k1gFnSc6	smrt
<g/>
,	,	kIx,	,
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
románů	román	k1gInPc2	román
Maid	Maida	k1gFnPc2	Maida
In	In	k1gFnSc2	In
Waiting	Waiting	k1gInSc1	Waiting
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
Společnice	společnice	k1gFnSc1	společnice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
prvně	prvně	k?	prvně
jako	jako	k8xS	jako
Sestra	sestra	k1gFnSc1	sestra
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jako	jako	k8xC	jako
Než	než	k8xS	než
osud	osud	k1gInSc1	osud
zavolá	zavolat	k5eAaPmIp3nS	zavolat
nebo	nebo	k8xC	nebo
jako	jako	k8xS	jako
Dychtivé	dychtivý	k2eAgNnSc1d1	dychtivé
mládí	mládí	k1gNnSc1	mládí
<g/>
,	,	kIx,	,
Flowering	Flowering	k1gInSc1	Flowering
Wilderness	Wilderness	k1gInSc1	Wilderness
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
Kvetoucí	kvetoucí	k2eAgFnSc1d1	kvetoucí
pustina	pustina	k1gFnSc1	pustina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Over	Over	k1gMnSc1	Over
the	the	k?	the
River	River	k1gMnSc1	River
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
Napříč	napříč	k7c7	napříč
proudem	proud	k1gInSc7	proud
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Collected	Collected	k1gInSc1	Collected
Poems	Poems	k1gInSc1	Poems
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
Sebrané	sebraný	k2eAgFnPc4d1	sebraná
básně	báseň	k1gFnPc4	báseň
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
posmrtně	posmrtně	k6eAd1	posmrtně
vydaný	vydaný	k2eAgInSc4d1	vydaný
soubor	soubor	k1gInSc4	soubor
básní	báseň	k1gFnPc2	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Justice	justice	k1gFnSc1	justice
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
Spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Maurice	Maurika	k1gFnSc6	Maurika
Elvey	Elve	k1gMnPc4	Elve
<g/>
,	,	kIx,	,
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Skin	skin	k1gMnSc1	skin
Game	game	k1gInSc1	game
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
Podfuk	Podfuk	k?	Podfuk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
režie	režie	k1gFnPc1	režie
B.	B.	kA	B.
E.	E.	kA	E.
Doxat-Pratt	Doxat-Pratt	k1gInSc1	Doxat-Pratt
<g/>
,	,	kIx,	,
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
The	The	k1gMnSc1	The
Stranger	Stranger	k1gMnSc1	Stranger
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
Cizinec	cizinec	k1gMnSc1	cizinec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Joseph	Josepha	k1gFnPc2	Josepha
Henabery	Henabera	k1gFnSc2	Henabera
<g/>
,	,	kIx,	,
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
The	The	k1gMnSc5	The
White	Whit	k1gMnSc5	Whit
Monkey	Monkea	k1gMnSc2	Monkea
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
Bílá	bílý	k2eAgFnSc1d1	bílá
opice	opice	k1gFnSc1	opice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Phil	Phil	k1gMnSc1	Phil
Rosen	rosen	k2eAgMnSc1d1	rosen
<g/>
,	,	kIx,	,
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Old	Olda	k1gFnPc2	Olda
English	Englisha	k1gFnPc2	Englisha
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
Starý	starý	k2eAgMnSc1d1	starý
Angličan	Angličan	k1gMnSc1	Angličan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Alfred	Alfred	k1gMnSc1	Alfred
E.	E.	kA	E.
Green	Green	k1gInSc1	Green
<g/>
,	,	kIx,	,
Escape	Escap	k1gMnSc5	Escap
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
Útěk	útěk	k1gInSc1	útěk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Basil	Basil	k1gMnSc1	Basil
Dean	Dean	k1gMnSc1	Dean
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Skin	skin	k1gMnSc1	skin
Game	game	k1gInSc1	game
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Podfuk	Podfuk	k?	Podfuk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Alfred	Alfred	k1gMnSc1	Alfred
Hitchcock	Hitchcock	k1gMnSc1	Hitchcock
<g/>
,	,	kIx,	,
Loyalties	Loyalties	k1gMnSc1	Loyalties
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
Oddanost	oddanost	k1gFnSc1	oddanost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Basil	Basil	k1gMnSc1	Basil
Dean	Dean	k1gMnSc1	Dean
a	a	k8xC	a
Thorold	Thorold	k1gMnSc1	Thorold
Dickinson	Dickinson	k1gMnSc1	Dickinson
<g/>
,	,	kIx,	,
̈	̈	k?	̈
One	One	k1gFnSc1	One
More	mor	k1gInSc5	mor
River	River	k1gMnSc1	River
(	(	kIx(	(
<g/>
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
Napříč	napříč	k7c7	napříč
proudem	proud	k1gInSc7	proud
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
James	James	k1gMnSc1	James
Whale	Whale	k1gInSc1	Whale
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Silver	Silver	k1gMnSc1	Silver
Box	box	k1gInSc1	box
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
Stříbrné	stříbrný	k2eAgNnSc1d1	stříbrné
pouzdro	pouzdro	k1gNnSc1	pouzdro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
21	[number]	k4	21
Days	Daysa	k1gFnPc2	Daysa
(	(	kIx(	(
<g/>
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
Dvacet	dvacet	k4xCc4	dvacet
jedna	jeden	k4xCgFnSc1	jeden
dní	den	k1gInPc2	den
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Basil	Basil	k1gMnSc1	Basil
<g />
.	.	kIx.	.
</s>
<s>
Dean	Dean	k1gMnSc1	Dean
<g/>
,	,	kIx,	,
Escape	Escap	k1gMnSc5	Escap
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
Útěk	útěk	k1gInSc1	útěk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Joseph	Joseph	k1gMnSc1	Joseph
L.	L.	kA	L.
Mankiewicz	Mankiewicz	k1gMnSc1	Mankiewicz
<g/>
,	,	kIx,	,
That	That	k1gMnSc1	That
Forsyte	Forsyt	k1gInSc5	Forsyt
Woman	Woman	k1gMnSc1	Woman
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
,	,	kIx,	,
Forsytova	Forsytův	k2eAgFnSc1d1	Forsytův
žena	žena	k1gFnSc1	žena
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Compton	Compton	k1gInSc1	Compton
Bennett	Bennett	k1gInSc1	Bennett
<g/>
,	,	kIx,	,
Letzten	Letztno	k1gNnPc2	Letztno
werden	werdno	k1gNnPc2	werdno
die	die	k?	die
Ersten	Erstno	k1gNnPc2	Erstno
<g />
.	.	kIx.	.
</s>
<s>
sein	sein	k1gNnSc1	sein
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
Poslední	poslední	k2eAgFnSc1d1	poslední
bude	být	k5eAaImBp3nS	být
první	první	k4xOgFnSc6	první
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Rolf	Rolf	k1gMnSc1	Rolf
Hansen	Hansen	k1gInSc1	Hansen
<g/>
,	,	kIx,	,
Strife	Strif	k1gMnSc5	Strif
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
Svár	svár	k1gInSc1	svár
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Peter	Peter	k1gMnSc1	Peter
Graham	Graham	k1gMnSc1	Graham
Scott	Scott	k1gMnSc1	Scott
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Forsyte	Forsyt	k1gInSc5	Forsyt
Saga	Sagum	k1gNnPc4	Sagum
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
Sága	sága	k1gFnSc1	sága
rodu	rod	k1gInSc2	rod
Forsytů	Forsyt	k1gInPc2	Forsyt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
James	James	k1gMnSc1	James
Cellan	Cellan	k1gMnSc1	Cellan
Jones	Jones	k1gMnSc1	Jones
a	a	k8xC	a
David	David	k1gMnSc1	David
Giles	Giles	k1gMnSc1	Giles
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Skin	skin	k1gMnSc1	skin
Game	game	k1gInSc1	game
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
Podfuk	Podfuk	k?	Podfuk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Peter	Peter	k1gMnSc1	Peter
Wood	Wood	k1gMnSc1	Wood
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Strife	Strif	k1gMnSc5	Strif
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
Svár	svár	k1gInSc1	svár
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Michael	Michael	k1gMnSc1	Michael
Darlow	Darlow	k1gMnSc1	Darlow
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Loyalties	Loyalties	k1gInSc1	Loyalties
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
Oddanost	oddanost	k1gFnSc1	oddanost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Rudolph	Rudolph	k1gMnSc1	Rudolph
Cartier	Cartier	k1gInSc1	Cartier
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Anglijskij	Anglijskij	k1gFnSc1	Anglijskij
vals	valsa	k1gFnPc2	valsa
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
Anglický	anglický	k2eAgInSc1d1	anglický
valčík	valčík	k1gInSc1	valčík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Gitis	Gitis	k1gFnSc2	Gitis
Luksas	Luksasa	k1gFnPc2	Luksasa
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
Joy	Joy	k1gFnSc2	Joy
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
Summer	Summer	k1gInSc1	Summer
Story	story	k1gFnPc2	story
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
Letní	letní	k2eAgInSc1d1	letní
příběh	příběh	k1gInSc1	příběh
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Piers	Piersa	k1gFnPc2	Piersa
Haggard	Haggarda	k1gFnPc2	Haggarda
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
novely	novela	k1gFnSc2	novela
Jabloň	jabloň	k1gFnSc1	jabloň
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
a	a	k8xC	a
poslední	poslední	k2eAgMnSc1d1	poslední
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
Poslední	poslední	k2eAgFnSc1d1	poslední
bude	být	k5eAaImBp3nS	být
první	první	k4xOgFnSc6	první
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Dušan	Dušan	k1gMnSc1	Dušan
Klein	Klein	k1gMnSc1	Klein
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Forsyte	Forsyt	k1gInSc5	Forsyt
Saga	Sagum	k1gNnPc1	Sagum
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
Sága	sága	k1gFnSc1	sága
rodu	rod	k1gInSc2	rod
Forsytů	Forsyt	k1gInPc2	Forsyt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Christopher	Christophra	k1gFnPc2	Christophra
Menaul	Menaula	k1gFnPc2	Menaula
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
Dave	Dav	k1gInSc5	Dav
Moore	Moor	k1gInSc5	Moor
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Forsyte	Forsyt	k1gInSc5	Forsyt
Saga	Sag	k2eAgNnPc1d1	Sag
<g/>
:	:	kIx,	:
To	to	k9	to
Let	let	k1gInSc1	let
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
Sága	sága	k1gFnSc1	sága
rodu	rod	k1gInSc2	rod
Forsytů	Forsyt	k1gInPc2	Forsyt
<g/>
:	:	kIx,	:
K	k	k7c3	k
pronajmutí	pronajmutí	k1gNnSc3	pronajmutí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Andy	Anda	k1gFnSc2	Anda
Wilson	Wilson	k1gInSc1	Wilson
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
(	(	kIx(	(
<g/>
pokračování	pokračování	k1gNnSc1	pokračování
přecházejícího	přecházející	k2eAgInSc2d1	přecházející
<g/>
)	)	kIx)	)
Vybrané	vybraný	k2eAgNnSc1d1	vybrané
<g />
.	.	kIx.	.
</s>
<s>
novelly	novella	k1gFnPc1	novella
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1915	[number]	k4	1915
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Karel	Karel	k1gMnSc1	Karel
Mašek	Mašek	k1gMnSc1	Mašek
<g/>
,	,	kIx,	,
Bratrství	bratrství	k1gNnSc1	bratrství
<g/>
,	,	kIx,	,
Laichter	Laichter	k1gInSc1	Laichter
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Franta	Franta	k1gMnSc1	Franta
<g/>
,	,	kIx,	,
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
Patricij	patricij	k1gMnSc1	patricij
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Minařík	Minařík	k1gMnSc1	Minařík
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Čeněk	Čeněk	k1gMnSc1	Čeněk
Syrový	syrový	k2eAgMnSc1d1	syrový
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Sága	sága	k1gFnSc1	sága
rodu	rod	k1gInSc2	rod
Forsytů	Forsyt	k1gInPc2	Forsyt
I.	I.	kA	I.
<g/>
-III	-III	k?	-III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Aventinum	Aventinum	k1gNnSc1	Aventinum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Bohumila	Bohumila	k1gFnSc1	Bohumila
Kubertová-Zátková	Kubertová-Zátkový	k2eAgFnSc1d1	Kubertová-Zátkový
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
1931	[number]	k4	1931
a	a	k8xC	a
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
1938	[number]	k4	1938
a	a	k8xC	a
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
</s>
<s>
Útěk	útěk	k1gInSc1	útěk
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
Kuncíř	Kuncíř	k1gMnSc1	Kuncíř
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1927	[number]	k4	1927
<g/>
,	,	kIx,	,
Tmavý	tmavý	k2eAgInSc1d1	tmavý
květ	květ	k1gInSc1	květ
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Elsie	Elsie	k1gFnSc1	Elsie
Havlasová	Havlasový	k2eAgFnSc1d1	Havlasový
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1931	[number]	k4	1931
a	a	k8xC	a
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
komedie	komedie	k1gFnSc1	komedie
I.	I.	kA	I.
<g/>
-III	-III	k?	-III
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Sága	sága	k1gFnSc1	sága
rodiny	rodina	k1gFnSc2	rodina
Forsytů	Forsyt	k1gInPc2	Forsyt
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
-VI	-VI	k?	-VI
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Aventinum	Aventinum	k1gNnSc1	Aventinum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Bohumila	Bohumila	k1gFnSc1	Bohumila
Kubertová-Zátková	Kubertová-Zátkový	k2eAgFnSc1d1	Kubertová-Zátkový
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1931	[number]	k4	1931
a	a	k8xC	a
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1935	[number]	k4	1935
a	a	k8xC	a
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
pokrytců	pokrytec	k1gMnPc2	pokrytec
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Anna	Anna	k1gFnSc1	Anna
Kučerová	Kučerová	k1gFnSc1	Kučerová
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
SNKLU	SNKLU	kA	SNKLU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
Silnější	silný	k2eAgFnSc1d2	silnější
než	než	k8xS	než
smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jan	Jan	k1gMnSc1	Jan
Čep	Čep	k1gMnSc1	Čep
a	a	k8xC	a
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Rovenský	Rovenský	k2eAgMnSc1d1	Rovenský
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
a	a	k8xC	a
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Svár	svár	k1gInSc1	svár
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Synek	Synek	k1gMnSc1	Synek
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Staša	Staša	k1gFnSc1	Staša
Jílovská	jílovský	k2eAgFnSc1d1	Jílovská
<g/>
,	,	kIx,	,
Komentář	komentář	k1gInSc1	komentář
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
Srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgInS	přeložit
F.	F.	kA	F.
Zieris	Zieris	k1gFnSc2	Zieris
a	a	k8xC	a
Robert	Robert	k1gMnSc1	Robert
Titěra	titěra	k1gMnSc1	titěra
<g/>
,	,	kIx,	,
Na	na	k7c6	na
burse	bursa	k1gFnSc6	bursa
Forsytů	Forsyt	k1gInPc2	Forsyt
<g/>
,	,	kIx,	,
Aventinum	Aventinum	k1gNnSc1	Aventinum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Bohumila	Bohumila	k1gFnSc1	Bohumila
Kubertová-Zátková	Kubertová-Zátkový	k2eAgFnSc1d1	Kubertová-Zátkový
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
Freelandů	Freeland	k1gInPc2	Freeland
<g/>
,	,	kIx,	,
Kvasnička	Kvasnička	k1gMnSc1	Kvasnička
a	a	k8xC	a
Hampl	Hampl	k1gMnSc1	Hampl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
L.	L.	kA	L.
Vymětal	Vymětal	k1gMnSc1	Vymětal
<g/>
,	,	kIx,	,
Světec	světec	k1gMnSc1	světec
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Robert	Robert	k1gMnSc1	Robert
Tintěra	tintěra	k1gMnSc1	tintěra
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Sestra	sestra	k1gFnSc1	sestra
<g/>
,	,	kIx,	,
Aventinum	Aventinum	k1gNnSc1	Aventinum
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Bohumila	Bohumila	k1gFnSc1	Bohumila
Kubertová-Zátková	Kubertová-Zátkový	k2eAgFnSc1d1	Kubertová-Zátkový
<g/>
,	,	kIx,	,
Stařec	stařec	k1gMnSc1	stařec
a	a	k8xC	a
smrt	smrt	k1gFnSc1	smrt
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
Venkovské	venkovský	k2eAgNnSc1d1	venkovské
sídlo	sídlo	k1gNnSc1	sídlo
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Květa	Květa	k1gFnSc1	Květa
Milcová	Milcová	k1gFnSc1	Milcová
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Vzpomínky	vzpomínka	k1gFnPc1	vzpomínka
<g/>
,	,	kIx,	,
L.	L.	kA	L.
Mazáč	Mazáč	k1gInSc1	Mazáč
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Anna	Anna	k1gFnSc1	Anna
Kučerová	Kučerová	k1gFnSc1	Kučerová
<g/>
,	,	kIx,	,
Kvetoucí	kvetoucí	k2eAgFnSc1d1	kvetoucí
pustina	pustina	k1gFnSc1	pustina
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Marie	Marie	k1gFnSc1	Marie
Vlasáková	Vlasáková	k1gFnSc1	Vlasáková
<g/>
,	,	kIx,	,
Napříč	napříč	k6eAd1	napříč
proudem	proud	k1gInSc7	proud
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Jarmila	Jarmila	k1gFnSc1	Jarmila
Kurelová	Kurelová	k1gFnSc1	Kurelová
<g/>
,	,	kIx,	,
Vila	vila	k1gFnSc1	vila
Rubein	Rubein	k1gMnSc1	Rubein
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jan	Jan	k1gMnSc1	Jan
Filip	Filip	k1gMnSc1	Filip
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
kapitola	kapitola	k1gFnSc1	kapitola
I.	I.	kA	I.
<g/>
-III	-III	k?	-III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
přeložily	přeložit	k5eAaPmAgFnP	přeložit
B.	B.	kA	B.
Kubertová-Zátková	Kubertová-Zátkový	k2eAgFnSc1d1	Kubertová-Zátkový
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Vlasáková	Vlasáková	k1gFnSc1	Vlasáková
a	a	k8xC	a
Jarmila	Jarmila	k1gFnSc1	Jarmila
Kurelová	Kurelová	k1gFnSc1	Kurelová
(	(	kIx(	(
<g/>
první	první	k4xOgInSc1	první
díl	díl	k1gInSc1	díl
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Než	než	k8xS	než
osud	osud	k1gInSc1	osud
zavolá	zavolat	k5eAaPmIp3nS	zavolat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Allegro	allegro	k1gNnSc1	allegro
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1992	[number]	k4	1992
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Konec	konec	k1gInSc1	konec
kapitoly	kapitola	k1gFnSc2	kapitola
(	(	kIx(	(
<g/>
první	první	k4xOgInSc1	první
díl	díl	k1gInSc1	díl
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Dychtivé	dychtivý	k2eAgNnSc1d1	dychtivé
mládí	mládí	k1gNnSc1	mládí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
karta	karta	k1gFnSc1	karta
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Josef	Josef	k1gMnSc1	Josef
Hobza	Hobza	k1gMnSc1	Hobza
<g/>
,	,	kIx,	,
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
Jabloň	jabloň	k1gFnSc1	jabloň
<g/>
,	,	kIx,	,
Nakladatelské	nakladatelský	k2eAgNnSc1d1	nakladatelské
družstvo	družstvo	k1gNnSc1	družstvo
Máje	máje	k1gFnSc1	máje
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Ludmila	Ludmila	k1gFnSc1	Ludmila
Vočadlová	Vočadlový	k2eAgFnSc1d1	Vočadlová
<g/>
,	,	kIx,	,
Sága	sága	k1gFnSc1	sága
rodu	rod	k1gInSc2	rod
Forsytů	Forsyt	k1gInPc2	Forsyt
I.	I.	kA	I.
<g/>
-III	-III	k?	-III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Urbánek	Urbánek	k1gMnSc1	Urbánek
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
1971	[number]	k4	1971
a	a	k8xC	a
1991	[number]	k4	1991
a	a	k8xC	a
BB	BB	kA	BB
art	art	k?	art
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
a	a	k8xC	a
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
komedie	komedie	k1gFnSc1	komedie
I.	I.	kA	I.
<g/>
-III	-III	k?	-III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Urbánek	Urbánek	k1gMnSc1	Urbánek
<g/>
,	,	kIx,	,
Patricij	patricij	k1gMnSc1	patricij
<g/>
,	,	kIx,	,
Práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1969	[number]	k4	1969
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Věra	Věra	k1gFnSc1	Věra
Kopalová	Kopalová	k1gFnSc1	Kopalová
<g/>
,	,	kIx,	,
Tmavý	tmavý	k2eAgInSc1d1	tmavý
květ	květ	k1gInSc1	květ
<g/>
,	,	kIx,	,
Svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jan	Jan	k1gMnSc1	Jan
Caha	Caha	k1gMnSc1	Caha
<g/>
,	,	kIx,	,
Forsytovo	Forsytův	k2eAgNnSc1d1	Forsytův
nanebevzetí	nanebevzetí	k1gNnSc1	nanebevzetí
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Hana	Hana	k1gFnSc1	Hana
Žantovská	Žantovský	k2eAgFnSc1d1	Žantovská
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
Garamond	garamond	k1gInSc1	garamond
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
povídky	povídka	k1gFnPc1	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
John	John	k1gMnSc1	John
Galsworthy	Galswortha	k1gFnPc4	Galswortha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
John	John	k1gMnSc1	John
Galsworthy	Galswortha	k1gFnSc2	Galswortha
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
John	John	k1gMnSc1	John
Galsworthy	Galswortha	k1gFnSc2	Galswortha
Plné	plný	k2eAgInPc4d1	plný
texty	text	k1gInPc4	text
děl	dít	k5eAaBmAgMnS	dít
autora	autor	k1gMnSc2	autor
John	John	k1gMnSc1	John
Galsworthy	Galswortha	k1gMnSc2	Galswortha
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
Autor	autor	k1gMnSc1	autor
John	John	k1gMnSc1	John
Galsworthy	Galswortha	k1gFnSc2	Galswortha
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Osoba	osoba	k1gFnSc1	osoba
John	John	k1gMnSc1	John
Galsworthy	Galswortha	k1gFnSc2	Galswortha
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Nobel	Nobel	k1gMnSc1	Nobel
Prize	Prize	k1gFnSc2	Prize
bio	bio	k?	bio
<g/>
,	,	kIx,	,
http://nobelprize.org/nobel_prizes/literature/laureates/1932/galsworthy-bio.html	[url]	k1gMnSc1	http://nobelprize.org/nobel_prizes/literature/laureates/1932/galsworthy-bio.html
–	–	k?	–
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
http://www.kirjasto.sci.fi/johngals.htm	[url]	k6eAd1	http://www.kirjasto.sci.fi/johngals.htm
–	–	k?	–
anglicky	anglicky	k6eAd1	anglicky
John	John	k1gMnSc1	John
Galsworthy	Galswortha	k1gFnSc2	Galswortha
<g/>
:	:	kIx,	:
free	freat	k5eAaPmIp3nS	freat
web	web	k1gInSc4	web
books	booksa	k1gFnPc2	booksa
<g/>
,	,	kIx,	,
online	onlinout	k5eAaPmIp3nS	onlinout
–	–	k?	–
anglicky	anglicky	k6eAd1	anglicky
</s>
