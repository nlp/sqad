<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
(	(	kIx(
<g/>
Libčany	Libčan	k1gMnPc7
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
(	(	kIx(
<g/>
Libčany	Libčan	k1gMnPc4
<g/>
)	)	kIx)
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
(	(	kIx(
<g/>
Libčany	Libčan	k1gMnPc7
<g/>
)	)	kIx)
<g/>
Místo	místo	k6eAd1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Královéhradecký	královéhradecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
Obec	obec	k1gFnSc1
</s>
<s>
Libčany	Libčan	k1gMnPc4
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
11	#num#	k4
<g/>
′	′	k?
<g/>
30,77	30,77	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
41	#num#	k4
<g/>
′	′	k?
<g/>
42,22	42,22	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Církev	církev	k1gFnSc1
</s>
<s>
římskokatolická	římskokatolický	k2eAgFnSc1d1
Diecéze	diecéze	k1gFnSc1
</s>
<s>
Královéhradecká	královéhradecký	k2eAgFnSc1d1
Farnost	farnost	k1gFnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Libčany	Libčan	k1gMnPc7
Architektonický	architektonický	k2eAgInSc4d1
popis	popis	k1gInSc4
Stavební	stavební	k2eAgInSc1d1
sloh	sloh	k1gInSc1
</s>
<s>
románský	románský	k2eAgMnSc1d1
Specifikace	specifikace	k1gFnSc2
Stavební	stavební	k2eAgInSc1d1
materiál	materiál	k1gInSc1
</s>
<s>
kámen	kámen	k1gInSc1
Další	další	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Kód	kód	k1gInSc4
památky	památka	k1gFnSc2
</s>
<s>
25587	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
637	#num#	k4
(	(	kIx(
<g/>
Pk	Pk	k1gFnSc1
<g/>
•	•	k?
<g/>
MIS	mísa	k1gFnPc2
<g/>
•	•	k?
<g/>
Sez	Sez	k1gMnSc1
<g/>
•	•	k?
<g/>
Obr	obr	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
se	se	k3xPyFc4
nalézá	nalézat	k5eAaImIp3nS
na	na	k7c4
návrší	návrší	k1gNnPc4
na	na	k7c6
okraji	okraj	k1gInSc6
obce	obec	k1gFnSc2
Libčany	Libčany	k1gMnPc7
v	v	k7c6
okrese	okres	k1gInSc6
Hradec	Hradec	k1gInSc1
Králové	Králové	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Románský	románský	k2eAgInSc1d1
kostel	kostel	k1gInSc1
spolu	spolu	k6eAd1
s	s	k7c7
areálem	areál	k1gInSc7
přilehlého	přilehlý	k2eAgInSc2d1
hřbitova	hřbitov	k1gInSc2
a	a	k8xC
zvonice	zvonice	k1gFnSc1
tvoří	tvořit	k5eAaImIp3nS
dominantu	dominanta	k1gFnSc4
obce	obec	k1gFnSc2
a	a	k8xC
jejího	její	k3xOp3gNnSc2
okolí	okolí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Románský	románský	k2eAgInSc1d1
kostel	kostel	k1gInSc1
je	být	k5eAaImIp3nS
chráněn	chránit	k5eAaImNgInS
jako	jako	k8xS,k8xC
kulturní	kulturní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgInSc1d1
památkový	památkový	k2eAgInSc1d1
ústav	ústav	k1gInSc1
tento	tento	k3xDgInSc4
kostel	kostel	k1gInSc4
uvádí	uvádět	k5eAaImIp3nS
v	v	k7c6
katalogu	katalog	k1gInSc6
památek	památka	k1gFnPc2
pod	pod	k7c7
rejstříkovým	rejstříkový	k2eAgNnSc7d1
číslem	číslo	k1gNnSc7
ÚSKP	ÚSKP	kA
25587	#num#	k4
<g/>
/	/	kIx~
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
637	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
v	v	k7c6
Libčanech	Libčan	k1gMnPc6
je	on	k3xPp3gMnPc4
románský	románský	k2eAgInSc1d1
bezvěžový	bezvěžový	k2eAgInSc1d1
tribunový	tribunový	k2eAgInSc1d1
kostel	kostel	k1gInSc1
s	s	k7c7
apsidou	apsida	k1gFnSc7
pocházející	pocházející	k2eAgInSc1d1
počátku	počátek	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostel	kostel	k1gInSc1
byl	být	k5eAaImAgInS
na	na	k7c6
přelomu	přelom	k1gInSc6
15	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
přestavěn	přestavěn	k2eAgInSc1d1
goticky	goticky	k6eAd1
<g/>
,	,	kIx,
následně	následně	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1608	#num#	k4
renesančně	renesančně	k6eAd1
a	a	k8xC
posléze	posléze	k6eAd1
barokně	barokně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
počátku	počátek	k1gInSc2
kostela	kostel	k1gInSc2
se	se	k3xPyFc4
dochovalo	dochovat	k5eAaPmAgNnS
románské	románský	k2eAgNnSc1d1
zdivo	zdivo	k1gNnSc1
<g/>
,	,	kIx,
ústupkový	ústupkový	k2eAgInSc1d1
portál	portál	k1gInSc1
s	s	k7c7
tympanonem	tympanon	k1gInSc7
a	a	k8xC
sloupy	sloup	k1gInPc1
tribuny	tribuna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
znázorněno	znázornit	k5eAaPmNgNnS
freskou	freska	k1gFnSc7
od	od	k7c2
Josefa	Josef	k1gMnSc2
Kramolína	Kramolín	k1gMnSc2
či	či	k8xC
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
žáka	žák	k1gMnSc2
zasazenou	zasazený	k2eAgFnSc4d1
v	v	k7c6
nice	nika	k1gFnSc6
a	a	k8xC
obklopenou	obklopený	k2eAgFnSc7d1
iluzivními	iluzivní	k2eAgInPc7d1
sloupy	sloup	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
jižního	jižní	k2eAgInSc2d1
pilíře	pilíř	k1gInSc2
triumfálního	triumfální	k2eAgInSc2d1
oblouku	oblouk	k1gInSc2
je	být	k5eAaImIp3nS
vložen	vložit	k5eAaPmNgInS
epitaf	epitaf	k1gInSc1
hraběte	hrabě	k1gMnSc2
Jana	Jan	k1gMnSc2
Petra	Petr	k1gMnSc2
Straky	Straka	k1gMnSc2
z	z	k7c2
Nedabylic	Nedabylice	k1gFnPc2
(	(	kIx(
<g/>
†	†	k?
1720	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konzole	konzola	k1gFnSc6
vystupující	vystupující	k2eAgFnSc6d1
ze	z	k7c2
zdi	zeď	k1gFnSc2
je	být	k5eAaImIp3nS
položen	položit	k5eAaPmNgInS
sarkofág	sarkofág	k1gInSc1
s	s	k7c7
rodovým	rodový	k2eAgInSc7d1
erbem	erb	k1gInSc7
opřeným	opřený	k2eAgInSc7d1
o	o	k7c4
konzolu	konzola	k1gFnSc4
s	s	k7c7
dvěma	dva	k4xCgInPc7
orlími	orlí	k2eAgInPc7d1
pařáty	pařát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
sarkofág	sarkofág	k1gInSc4
se	se	k3xPyFc4
levou	levý	k2eAgFnSc7d1
rukou	ruka	k1gFnSc7
opírá	opírat	k5eAaImIp3nS
polopostava	polopostava	k1gFnSc1
hraběte	hrabě	k1gMnSc2
Straky	Straka	k1gMnSc2
oděného	oděný	k2eAgMnSc2d1
ve	v	k7c6
zbroji	zbroj	k1gFnSc6
s	s	k7c7
dlouhou	dlouhý	k2eAgFnSc7d1
parukou	paruka	k1gFnSc7
na	na	k7c6
hlavě	hlava	k1gFnSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
pravou	pravý	k2eAgFnSc7d1
rukou	ruka	k1gFnSc7
ovinutou	ovinutý	k2eAgFnSc7d1
pláštěm	plášť	k1gInSc7
kyne	kynout	k5eAaImIp3nS
směrem	směr	k1gInSc7
k	k	k7c3
oltáři	oltář	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c7
jeho	jeho	k3xOp3gNnPc7
zády	záda	k1gNnPc7
klečí	klečet	k5eAaImIp3nS
dítě	dítě	k1gNnSc4
s	s	k7c7
přesýpacími	přesýpací	k2eAgFnPc7d1
hodinami	hodina	k1gFnPc7
a	a	k8xC
lebkou	lebka	k1gFnSc7
v	v	k7c6
rukou	ruka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nad	nad	k7c7
hrabětem	hrabě	k1gMnSc7
rozprostírá	rozprostírat	k5eAaImIp3nS
okřídlený	okřídlený	k2eAgMnSc1d1
stařec	stařec	k1gMnSc1
Chronos	Chronos	k1gMnSc1
zvlněnou	zvlněný	k2eAgFnSc4d1
drapérii	drapérie	k1gFnSc4
s	s	k7c7
latinsky	latinsky	k6eAd1
psaným	psaný	k2eAgInSc7d1
epitafem	epitaf	k1gInSc7
ze	z	k7c2
zlaceného	zlacený	k2eAgNnSc2d1
písma	písmo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Naproti	naproti	k7c3
epitafu	epitaf	k1gInSc3
je	být	k5eAaImIp3nS
na	na	k7c6
severním	severní	k2eAgInSc6d1
pilíři	pilíř	k1gInSc6
vítězného	vítězný	k2eAgInSc2d1
oblouku	oblouk	k1gInSc2
umístěna	umístit	k5eAaPmNgFnS
dřevěná	dřevěný	k2eAgFnSc1d1
kazatelna	kazatelna	k1gFnSc1
se	s	k7c7
stříškou	stříška	k1gFnSc7
zakončenou	zakončený	k2eAgFnSc7d1
sochou	socha	k1gFnSc7
Hospodina	Hospodin	k1gMnSc2
s	s	k7c7
knihou	kniha	k1gFnSc7
v	v	k7c6
ruce	ruka	k1gFnSc6
z	z	k7c2
první	první	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
kostela	kostel	k1gInSc2
je	být	k5eAaImIp3nS
dále	daleko	k6eAd2
barokní	barokní	k2eAgFnSc1d1
márnice	márnice	k1gFnSc1
<g/>
,	,	kIx,
novodobá	novodobý	k2eAgFnSc1d1
zvonice	zvonice	k1gFnSc1
a	a	k8xC
barokní	barokní	k2eAgFnSc1d1
fara	fara	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Areál	areál	k1gInSc1
je	být	k5eAaImIp3nS
obklopen	obklopit	k5eAaPmNgInS
ohradní	ohradní	k2eAgFnSc7d1
zdí	zeď	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
v	v	k7c6
Libčanech	Libčan	k1gMnPc6
</s>
<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
v	v	k7c6
Libčanech	Libčan	k1gMnPc6
</s>
<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
v	v	k7c6
Libčanech	Libčan	k1gMnPc6
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Umělecké	umělecký	k2eAgFnPc1d1
památky	památka	k1gFnPc1
Čech	Čechy	k1gFnPc2
I.	I.	kA
-	-	kIx~
IV	IV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Emanuel	Emanuel	k1gMnSc1
POCHE	POCHE	kA
(	(	kIx(
<g/>
ed	ed	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
1977	#num#	k4
<g/>
,	,	kIx,
strana	strana	k1gFnSc1
833	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc6
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
kostel	kostel	k1gInSc1
na	na	k7c6
památkovém	památkový	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
</s>
<s>
kostel	kostel	k1gInSc1
na	na	k7c4
hrady	hrad	k1gInPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
kostel	kostel	k1gInSc1
na	na	k7c6
turistika	turistika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20081216011	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
157591548	#num#	k4
</s>
