<s>
Queens	Queens	k6eAd1	Queens
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
obvodů	obvod	k1gInPc2	obvod
města	město	k1gNnSc2	město
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
okres	okres	k1gInSc1	okres
amerického	americký	k2eAgInSc2d1	americký
spolkového	spolkový	k2eAgInSc2d1	spolkový
státu	stát	k1gInSc2	stát
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Rozlohou	rozloha	k1gFnSc7	rozloha
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dvě	dva	k4xCgNnPc1	dva
letiště	letiště	k1gNnPc1	letiště
<g/>
:	:	kIx,	:
LaGuardia	LaGuardium	k1gNnPc4	LaGuardium
v	v	k7c6	v
části	část	k1gFnSc6	část
Flushing	Flushing	k1gInSc1	Flushing
a	a	k8xC	a
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Johna	John	k1gMnSc2	John
F.	F.	kA	F.
Kennedyho	Kennedy	k1gMnSc2	Kennedy
(	(	kIx(	(
<g/>
známé	známý	k2eAgFnSc2d1	známá
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
JFK	JFK	kA	JFK
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
činí	činit	k5eAaImIp3nS	činit
přibližně	přibližně	k6eAd1	přibližně
2,2	[number]	k4	2,2
miliónu	milión	k4xCgInSc2	milión
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Polovina	polovina	k1gFnSc1	polovina
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
zemi	zem	k1gFnSc6	zem
než	než	k8xS	než
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Národním	národní	k2eAgNnSc6d1	národní
tenisovém	tenisový	k2eAgNnSc6d1	tenisové
centru	centrum	k1gNnSc6	centrum
Billie	Billie	k1gFnSc2	Billie
Jean	Jean	k1gMnSc1	Jean
Kingové	Kingový	k2eAgInPc4d1	Kingový
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
září	zářit	k5eAaImIp3nS	zářit
pořádán	pořádán	k2eAgInSc1d1	pořádán
tenisový	tenisový	k2eAgInSc1d1	tenisový
grandslam	grandslam	k1gInSc1	grandslam
US	US	kA	US
Open	Open	k1gInSc1	Open
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
známý	známý	k2eAgInSc1d1	známý
raper	raper	k1gInSc1	raper
Curtis	Curtis	k1gFnSc1	Curtis
50	[number]	k4	50
Cent	cent	k1gInSc1	cent
Jackson	Jackson	k1gInSc4	Jackson
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
obvodu	obvod	k1gInSc2	obvod
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
Nieuw	Nieuw	k1gFnSc1	Nieuw
Amsterdam	Amsterdam	k1gInSc1	Amsterdam
(	(	kIx(	(
<g/>
Nový	nový	k2eAgInSc1d1	nový
Amsterodam	Amsterodam	k1gInSc1	Amsterodam
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
odebrán	odebrat	k5eAaPmNgInS	odebrat
Holandsku	Holandsko	k1gNnSc6	Holandsko
a	a	k8xC	a
přejmenován	přejmenován	k2eAgMnSc1d1	přejmenován
na	na	k7c6	na
New	New	k1gFnSc6	New
York	York	k1gInSc1	York
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gNnSc3	jeho
okolí	okolí	k1gNnSc3	okolí
pak	pak	k6eAd1	pak
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
vlády	vláda	k1gFnSc2	vláda
manželky	manželka	k1gFnSc2	manželka
anglického	anglický	k2eAgMnSc4d1	anglický
krále	král	k1gMnSc4	král
Karla	Karel	k1gMnSc2	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Kateřiny	Kateřina	k1gFnPc1	Kateřina
z	z	k7c2	z
Braganzy	Braganza	k1gFnSc2	Braganza
dostalo	dostat	k5eAaPmAgNnS	dostat
jméno	jméno	k1gNnSc1	jméno
Queens	Queens	k1gInSc1	Queens
(	(	kIx(	(
<g/>
královny	královna	k1gFnSc2	královna
n.	n.	k?	n.
královnino	královnin	k2eAgNnSc1d1	královnino
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Queens	Queens	k6eAd1	Queens
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Long	Longa	k1gFnPc2	Longa
Islandu	Island	k1gInSc2	Island
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
několik	několik	k4yIc4	několik
menších	malý	k2eAgInPc2d2	menší
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Jamaica	Jamaica	k1gFnSc6	Jamaica
Bay	Bay	k1gFnSc2	Bay
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrť	čtvrť	k1gFnSc1	čtvrť
má	mít	k5eAaImIp3nS	mít
celkovou	celkový	k2eAgFnSc4d1	celková
rozlohu	rozloha	k1gFnSc4	rozloha
462	[number]	k4	462
km2	km2	k4	km2
<g/>
,	,	kIx,	,
283	[number]	k4	283
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
souš	souš	k1gFnSc1	souš
a	a	k8xC	a
38,7	[number]	k4	38,7
<g/>
%	%	kIx~	%
činí	činit	k5eAaImIp3nS	činit
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
drží	držet	k5eAaImIp3nS	držet
většinu	většina	k1gFnSc4	většina
veřejných	veřejný	k2eAgInPc2d1	veřejný
úřadů	úřad	k1gInPc2	úřad
<g/>
.	.	kIx.	.
63	[number]	k4	63
procent	procento	k1gNnPc2	procento
registrovaných	registrovaný	k2eAgMnPc2d1	registrovaný
voličů	volič	k1gMnPc2	volič
v	v	k7c6	v
Queensu	Queens	k1gInSc6	Queens
jsou	být	k5eAaImIp3nP	být
demokraté	demokrat	k1gMnPc1	demokrat
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnPc4d1	místní
stranické	stranický	k2eAgFnPc4d1	stranická
platformy	platforma	k1gFnPc4	platforma
se	se	k3xPyFc4	se
soustředí	soustředit	k5eAaPmIp3nP	soustředit
na	na	k7c4	na
dostupné	dostupný	k2eAgNnSc4d1	dostupné
bydlení	bydlení	k1gNnSc4	bydlení
<g/>
,	,	kIx,	,
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
a	a	k8xC	a
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
kontroverzní	kontroverzní	k2eAgFnPc4d1	kontroverzní
politické	politický	k2eAgFnPc4d1	politická
otázky	otázka	k1gFnPc4	otázka
v	v	k7c6	v
Queensu	Queens	k1gInSc6	Queens
patří	patřit	k5eAaImIp3nS	patřit
rozvoj	rozvoj	k1gInSc1	rozvoj
<g/>
,	,	kIx,	,
hluk	hluk	k1gInSc1	hluk
a	a	k8xC	a
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
bydlení	bydlení	k1gNnSc4	bydlení
<g/>
.	.	kIx.	.
</s>
<s>
Queens	Queens	k1gInSc1	Queens
nehlasoval	hlasovat	k5eNaImAgInS	hlasovat
pro	pro	k7c4	pro
republikánského	republikánský	k2eAgMnSc4d1	republikánský
kandidáta	kandidát	k1gMnSc4	kandidát
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zdejší	zdejší	k2eAgMnPc1d1	zdejší
voliči	volič	k1gMnPc1	volič
vybrali	vybrat	k5eAaPmAgMnP	vybrat
Richarda	Richard	k1gMnSc4	Richard
Nixona	Nixon	k1gMnSc4	Nixon
nad	nad	k7c7	nad
Georgem	Georg	k1gMnSc7	Georg
McGovernem	McGovern	k1gInSc7	McGovern
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgFnSc1d1	zdejší
ekonomika	ekonomika	k1gFnSc1	ekonomika
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
cestovním	cestovní	k2eAgInSc6d1	cestovní
ruchu	ruch	k1gInSc6	ruch
<g/>
,	,	kIx,	,
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
newyorská	newyorský	k2eAgFnSc1d1	newyorská
metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
oblast	oblast	k1gFnSc1	oblast
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgNnPc4	tři
letiště	letiště	k1gNnPc4	letiště
<g/>
,	,	kIx,	,
vzdušný	vzdušný	k2eAgInSc4d1	vzdušný
prostor	prostor	k1gInSc4	prostor
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
nejrušnější	rušný	k2eAgFnSc7d3	nejrušnější
a	a	k8xC	a
nejvíce	hodně	k6eAd3	hodně
regulované	regulovaný	k2eAgFnPc1d1	regulovaná
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Johna	John	k1gMnSc2	John
F.	F.	kA	F.
Kennedyho	Kennedy	k1gMnSc2	Kennedy
podél	podél	k7c2	podél
Jamaica	Jamaica	k1gFnSc2	Jamaica
Bay	Bay	k1gFnSc2	Bay
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejrušnější	rušný	k2eAgNnSc1d3	nejrušnější
letiště	letiště	k1gNnSc1	letiště
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
cestování	cestování	k1gNnSc2	cestování
<g/>
.	.	kIx.	.
</s>
<s>
Regionální	regionální	k2eAgNnSc1d1	regionální
Letiště	letiště	k1gNnSc1	letiště
La	la	k1gNnSc2	la
Guardia	Guardia	k?	Guardia
na	na	k7c4	na
East	East	k2eAgInSc4d1	East
River	River	k1gInSc4	River
většinou	většinou	k6eAd1	většinou
obsluhuje	obsluhovat	k5eAaImIp3nS	obsluhovat
lety	léto	k1gNnPc7	léto
z	z	k7c2	z
východu	východ	k1gInSc2	východ
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
tvořili	tvořit	k5eAaImAgMnP	tvořit
běloši	běloch	k1gMnPc1	běloch
39,7	[number]	k4	39,7
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc1	populace
(	(	kIx(	(
<g/>
nehispánští	hispánský	k2eNgMnPc1d1	hispánský
běloši	běloch	k1gMnPc1	běloch
27,6	[number]	k4	27,6
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
podíl	podíl	k1gInSc1	podíl
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
činil	činit	k5eAaImAgInS	činit
91,5	[number]	k4	91,5
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
19,1	[number]	k4	19,1
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
Asiaté	Asiat	k1gMnPc1	Asiat
22,9	[number]	k4	22,9
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
populace	populace	k1gFnSc2	populace
jiné	jiný	k2eAgFnSc2d1	jiná
rasy	rasa	k1gFnSc2	rasa
12,9	[number]	k4	12,9
<g/>
%	%	kIx~	%
a	a	k8xC	a
smíšené	smíšený	k2eAgMnPc4d1	smíšený
(	(	kIx(	(
<g/>
dvou	dva	k4xCgFnPc2	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	rasa	k1gFnPc2	rasa
<g/>
)	)	kIx)	)
4,5	[number]	k4	4,5
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
27,5	[number]	k4	27,5
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
čtvrti	čtvrt	k1gFnSc2	čtvrt
Queens	Queensa	k1gFnPc2	Queensa
bylo	být	k5eAaImAgNnS	být
hispánského	hispánský	k2eAgInSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinoamerického	latinoamerický	k2eAgInSc2d1	latinoamerický
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
rasy	rasa	k1gFnSc2	rasa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
46,6	[number]	k4	46,6
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
narodilo	narodit	k5eAaPmAgNnS	narodit
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
zemi	zem	k1gFnSc6	zem
než	než	k8xS	než
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
44,2	[number]	k4	44,2
<g/>
%	%	kIx~	%
populace	populace	k1gFnPc1	populace
starší	starý	k2eAgFnPc1d2	starší
5	[number]	k4	5
let	léto	k1gNnPc2	léto
mluvit	mluvit	k5eAaImF	mluvit
doma	doma	k6eAd1	doma
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
23,8	[number]	k4	23,8
<g/>
%	%	kIx~	%
španělsky	španělsky	k6eAd1	španělsky
<g/>
,	,	kIx,	,
16,8	[number]	k4	16,8
<g/>
%	%	kIx~	%
jiným	jiný	k2eAgInSc7d1	jiný
Indoevropským	indoevropský	k2eAgInSc7d1	indoevropský
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgNnPc2d1	další
13,5	[number]	k4	13,5
<g/>
%	%	kIx~	%
mluví	mluvit	k5eAaImIp3nS	mluvit
doma	doma	k6eAd1	doma
některým	některý	k3yIgFnPc3	některý
z	z	k7c2	z
asijských	asijský	k2eAgInPc2d1	asijský
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
2	[number]	k4	2
229	[number]	k4	229
379	[number]	k4	379
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
tam	tam	k6eAd1	tam
782	[number]	k4	782
664	[number]	k4	664
domácností	domácnost	k1gFnPc2	domácnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
537	[number]	k4	537
690	[number]	k4	690
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
byla	být	k5eAaImAgFnS	být
7	[number]	k4	7
879,6	[number]	k4	879,6
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
tam	tam	k6eAd1	tam
817	[number]	k4	817
250	[number]	k4	250
bytových	bytový	k2eAgFnPc2d1	bytová
jednotek	jednotka	k1gFnPc2	jednotka
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
hustotou	hustota	k1gFnSc7	hustota
2	[number]	k4	2
888,5	[number]	k4	888,5
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadu	odhad	k1gInSc2	odhad
Úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
sčítání	sčítání	k1gNnSc4	sčítání
lidu	lid	k1gInSc2	lid
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
na	na	k7c4	na
2	[number]	k4	2
293	[number]	k4	293
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
příjem	příjem	k1gInSc1	příjem
pro	pro	k7c4	pro
domácnost	domácnost	k1gFnSc4	domácnost
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
činí	činit	k5eAaImIp3nS	činit
374	[number]	k4	374
39	[number]	k4	39
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
průměrný	průměrný	k2eAgInSc1d1	průměrný
příjem	příjem	k1gInSc1	příjem
pro	pro	k7c4	pro
rodinu	rodina	k1gFnSc4	rodina
činil	činit	k5eAaImAgMnS	činit
42	[number]	k4	42
608	[number]	k4	608
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
měli	mít	k5eAaImAgMnP	mít
průměrný	průměrný	k2eAgInSc4d1	průměrný
příjem	příjem	k1gInSc4	příjem
30	[number]	k4	30
576	[number]	k4	576
dolarů	dolar	k1gInPc2	dolar
oproti	oproti	k7c3	oproti
26	[number]	k4	26
628	[number]	k4	628
dolarům	dolar	k1gInPc3	dolar
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Příjem	příjem	k1gInSc1	příjem
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
činil	činit	k5eAaImAgMnS	činit
19	[number]	k4	19
222	[number]	k4	222
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Queensu	Queens	k1gInSc6	Queens
vydělává	vydělávat	k5eAaImIp3nS	vydělávat
černošská	černošský	k2eAgFnSc1d1	černošská
populace	populace	k1gFnSc1	populace
průměrně	průměrně	k6eAd1	průměrně
více	hodně	k6eAd2	hodně
než	než	k8xS	než
populace	populace	k1gFnSc1	populace
bělošská	bělošský	k2eAgFnSc1d1	bělošská
<g/>
.	.	kIx.	.
</s>
<s>
Queens	Queens	k1gInSc1	Queens
má	mít	k5eAaImIp3nS	mít
zásadní	zásadní	k2eAgInSc1d1	zásadní
význam	význam	k1gInSc1	význam
v	v	k7c6	v
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
a	a	k8xC	a
mezistátní	mezistátní	k2eAgFnSc3d1	mezistátní
letecké	letecký	k2eAgFnSc3d1	letecká
dopravě	doprava	k1gFnSc3	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
dvě	dva	k4xCgFnPc4	dva
ze	z	k7c2	z
tří	tři	k4xCgNnPc2	tři
Newyorských	newyorský	k2eAgNnPc2d1	newyorské
hlavních	hlavní	k2eAgNnPc2d1	hlavní
letišť	letiště	k1gNnPc2	letiště
<g/>
,	,	kIx,	,
letiště	letiště	k1gNnSc2	letiště
LaGuardia	LaGuardium	k1gNnSc2	LaGuardium
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
v	v	k7c6	v
severním	severní	k2eAgInSc6d1	severní
Queensu	Queens	k1gInSc6	Queens
při	při	k7c6	při
řece	řeka	k1gFnSc6	řeka
East	East	k2eAgInSc1d1	East
River	River	k1gInSc1	River
a	a	k8xC	a
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Johna	John	k1gMnSc2	John
F.	F.	kA	F.
Kennedyho	Kennedy	k1gMnSc2	Kennedy
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Jamaica	Jamaica	k1gFnSc2	Jamaica
Bay	Bay	k1gFnSc2	Bay
<g/>
.	.	kIx.	.
</s>
<s>
Nadzemní	nadzemní	k2eAgFnSc1d1	nadzemní
povrchová	povrchový	k2eAgFnSc1d1	povrchová
dráha	dráha	k1gFnSc1	dráha
JFK	JFK	kA	JFK
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
železniční	železniční	k2eAgNnSc1d1	železniční
spojení	spojení	k1gNnSc1	spojení
mezi	mezi	k7c7	mezi
letištěm	letiště	k1gNnSc7	letiště
JFK	JFK	kA	JFK
a	a	k8xC	a
místní	místní	k2eAgFnSc7d1	místní
železniční	železniční	k2eAgFnSc7d1	železniční
tratí	trať	k1gFnSc7	trať
<g/>
.	.	kIx.	.
</s>
<s>
Queens	Queens	k1gInSc1	Queens
je	být	k5eAaImIp3nS	být
připojen	připojit	k5eAaPmNgInS	připojit
k	k	k7c3	k
Bronxu	Bronx	k1gInSc3	Bronx
čtyřmi	čtyři	k4xCgInPc7	čtyři
mosty	most	k1gInPc7	most
<g/>
:	:	kIx,	:
Bronx	Bronx	k1gInSc1	Bronx
Whitestone	Whiteston	k1gInSc5	Whiteston
Bridge	Bridge	k1gNnPc3	Bridge
<g/>
,	,	kIx,	,
Throgs	Throgs	k1gInSc4	Throgs
Neck	Necka	k1gFnPc2	Necka
Bridge	Bridg	k1gInSc2	Bridg
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
F.	F.	kA	F.
Kennedy	Kenneda	k1gMnSc2	Kenneda
Bridge	Bridg	k1gMnSc2	Bridg
a	a	k8xC	a
Hell	Hell	k1gInSc4	Hell
Gate	Gate	k1gNnSc2	Gate
Bridge	Bridg	k1gFnSc2	Bridg
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Manhattanu	Manhattan	k1gInSc3	Manhattan
je	být	k5eAaImIp3nS	být
připojen	připojit	k5eAaPmNgInS	připojit
dvěma	dva	k4xCgInPc7	dva
mosty	most	k1gInPc7	most
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
tunelem	tunel	k1gInSc7	tunel
<g/>
:	:	kIx,	:
Robert	Robert	k1gMnSc1	Robert
F.	F.	kA	F.
Kennedy	Kenneda	k1gMnSc2	Kenneda
Bridge	Bridg	k1gMnSc2	Bridg
<g/>
,	,	kIx,	,
Queensboro	Queensbora	k1gFnSc5	Queensbora
Bridge	Bridge	k1gNnPc2	Bridge
a	a	k8xC	a
Queens	Queensa	k1gFnPc2	Queensa
Midtown	Midtown	k1gMnSc1	Midtown
Tunnel	Tunnel	k1gMnSc1	Tunnel
<g/>
.	.	kIx.	.
</s>
<s>
Vede	vést	k5eAaImIp3nS	vést
zde	zde	k6eAd1	zde
také	také	k9	také
most	most	k1gInSc4	most
na	na	k7c4	na
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
Island	Island	k1gInSc1	Island
-	-	kIx~	-
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
Island	Island	k1gInSc4	Island
Bridge	Bridg	k1gInSc2	Bridg
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Queens	Queensa	k1gFnPc2	Queensa
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Queens	Queensa	k1gFnPc2	Queensa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Informace	informace	k1gFnSc1	informace
o	o	k7c4	o
Queensu	Queensa	k1gFnSc4	Queensa
</s>
