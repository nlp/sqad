<s>
Queens	Queens	k6eAd1	Queens
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
obvodů	obvod	k1gInPc2	obvod
města	město	k1gNnSc2	město
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
okres	okres	k1gInSc1	okres
amerického	americký	k2eAgInSc2d1	americký
spolkového	spolkový	k2eAgInSc2d1	spolkový
státu	stát	k1gInSc2	stát
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Rozlohou	rozloha	k1gFnSc7	rozloha
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dvě	dva	k4xCgNnPc1	dva
letiště	letiště	k1gNnPc1	letiště
<g/>
:	:	kIx,	:
LaGuardia	LaGuardium	k1gNnPc4	LaGuardium
v	v	k7c6	v
části	část	k1gFnSc6	část
Flushing	Flushing	k1gInSc1	Flushing
a	a	k8xC	a
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Johna	John	k1gMnSc2	John
F.	F.	kA	F.
Kennedyho	Kennedy	k1gMnSc2	Kennedy
(	(	kIx(	(
<g/>
známé	známý	k2eAgFnSc2d1	známá
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
JFK	JFK	kA	JFK
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
