<s>
Grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Graphical	Graphical	k1gFnSc1	Graphical
User	usrat	k5eAaPmRp2nS	usrat
Interface	interface	k1gInSc1	interface
<g/>
,	,	kIx,	,
známe	znát	k5eAaImIp1nP	znát
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
GUI	GUI	kA	GUI
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
ovládat	ovládat	k5eAaImF	ovládat
počítač	počítač	k1gInSc1	počítač
pomocí	pomocí	k7c2	pomocí
interaktivních	interaktivní	k2eAgInPc2d1	interaktivní
grafických	grafický	k2eAgInPc2d1	grafický
ovládacích	ovládací	k2eAgInPc2d1	ovládací
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
