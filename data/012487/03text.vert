<p>
<s>
Grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Graphical	Graphical	k1gFnSc1	Graphical
User	usrat	k5eAaPmRp2nS	usrat
Interface	interface	k1gInSc1	interface
<g/>
,	,	kIx,	,
známe	znát	k5eAaImIp1nP	znát
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
GUI	GUI	kA	GUI
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
ovládat	ovládat	k5eAaImF	ovládat
počítač	počítač	k1gInSc1	počítač
pomocí	pomocí	k7c2	pomocí
interaktivních	interaktivní	k2eAgInPc2d1	interaktivní
grafických	grafický	k2eAgInPc2d1	grafický
ovládacích	ovládací	k2eAgInPc2d1	ovládací
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
monitoru	monitor	k1gInSc6	monitor
počítače	počítač	k1gInSc2	počítač
jsou	být	k5eAaImIp3nP	být
zobrazena	zobrazit	k5eAaPmNgNnP	zobrazit
okna	okno	k1gNnPc1	okno
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgMnPc6	který
programy	program	k1gInPc4	program
zobrazují	zobrazovat	k5eAaImIp3nP	zobrazovat
svůj	svůj	k3xOyFgInSc4	svůj
výstup	výstup	k1gInSc4	výstup
<g/>
.	.	kIx.	.
</s>
<s>
Uživatel	uživatel	k1gMnSc1	uživatel
používá	používat	k5eAaImIp3nS	používat
klávesnici	klávesnice	k1gFnSc4	klávesnice
<g/>
,	,	kIx,	,
myš	myš	k1gFnSc4	myš
a	a	k8xC	a
grafické	grafický	k2eAgInPc4d1	grafický
vstupní	vstupní	k2eAgInPc4d1	vstupní
prvky	prvek	k1gInPc4	prvek
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
menu	menu	k1gNnSc4	menu
<g/>
,	,	kIx,	,
ikony	ikon	k1gInPc1	ikon
<g/>
,	,	kIx,	,
tlačítka	tlačítko	k1gNnPc1	tlačítko
<g/>
,	,	kIx,	,
posuvníky	posuvník	k1gInPc1	posuvník
<g/>
,	,	kIx,	,
formuláře	formulář	k1gInPc1	formulář
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GUI	GUI	kA	GUI
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
v	v	k7c6	v
počítačích	počítač	k1gInPc6	počítač
<g/>
,	,	kIx,	,
přenosných	přenosný	k2eAgNnPc2d1	přenosné
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
přehrávače	přehrávač	k1gInPc1	přehrávač
MP	MP	kA	MP
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
přenosné	přenosný	k2eAgInPc4d1	přenosný
přehrávače	přehrávač	k1gInPc4	přehrávač
médií	médium	k1gNnPc2	médium
a	a	k8xC	a
herní	herní	k2eAgNnPc4d1	herní
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
,	,	kIx,	,
domácích	domácí	k1gFnPc2	domácí
spotřebiče	spotřebič	k1gInSc2	spotřebič
<g/>
,	,	kIx,	,
kancelářské	kancelářský	k2eAgNnSc4d1	kancelářské
a	a	k8xC	a
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
vybavení	vybavení	k1gNnSc4	vybavení
<g/>
,	,	kIx,	,
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
GUI	GUI	kA	GUI
představuje	představovat	k5eAaImIp3nS	představovat
informace	informace	k1gFnPc4	informace
a	a	k8xC	a
akce	akce	k1gFnPc4	akce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
uživatele	uživatel	k1gMnPc4	uživatel
zobrazována	zobrazován	k2eAgFnSc1d1	zobrazována
pomocí	pomoc	k1gFnSc7	pomoc
grafických	grafický	k2eAgFnPc2d1	grafická
ikon	ikona	k1gFnPc2	ikona
a	a	k8xC	a
vizuálních	vizuální	k2eAgInPc2d1	vizuální
indikátorů	indikátor	k1gInPc2	indikátor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
rozdíl	rozdíl	k1gInSc4	rozdíl
oproti	oproti	k7c3	oproti
textovým	textový	k2eAgFnPc3d1	textová
rozhraní	rozhraní	k1gNnSc6	rozhraní
(	(	kIx(	(
<g/>
CLI	clít	k5eAaImRp2nS	clít
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
textové	textový	k2eAgFnPc4d1	textová
navigace	navigace	k1gFnPc4	navigace
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnPc1	akce
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
prováděny	provádět	k5eAaImNgFnP	provádět
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
přímé	přímý	k2eAgFnSc2d1	přímá
manipulace	manipulace	k1gFnSc2	manipulace
s	s	k7c7	s
grafickými	grafický	k2eAgInPc7d1	grafický
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
GUI	GUI	kA	GUI
je	být	k5eAaImIp3nS	být
omezena	omezit	k5eAaPmNgFnS	omezit
na	na	k7c4	na
rozsah	rozsah	k1gInSc4	rozsah
dvojrozměrných	dvojrozměrný	k2eAgFnPc2d1	dvojrozměrná
obrazovek	obrazovka	k1gFnPc2	obrazovka
se	s	k7c7	s
schopností	schopnost	k1gFnSc7	schopnost
popsat	popsat	k5eAaPmF	popsat
generické	generický	k2eAgFnPc4d1	generická
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelskému	uživatelský	k2eAgNnSc3d1	Uživatelské
rozhraní	rozhraní	k1gNnSc3	rozhraní
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
zejména	zejména	k9	zejména
výzkum	výzkum	k1gInSc1	výzkum
počítačové	počítačový	k2eAgFnSc2d1	počítačová
vědy	věda	k1gFnSc2	věda
na	na	k7c4	na
PARC	PARC	kA	PARC
(	(	kIx(	(
<g/>
Palo	Pala	k1gMnSc5	Pala
Alto	Alta	k1gMnSc5	Alta
Research	Resear	k1gFnPc6	Resear
Center	centrum	k1gNnPc2	centrum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
GUI	GUI	kA	GUI
není	být	k5eNaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
aplikován	aplikován	k2eAgMnSc1d1	aplikován
na	na	k7c4	na
jiné	jiný	k2eAgInPc4d1	jiný
typy	typ	k1gInPc4	typ
rozhraní	rozhraní	k1gNnPc2	rozhraní
s	s	k7c7	s
nízkým	nízký	k2eAgNnSc7d1	nízké
rozlišením	rozlišení	k1gNnSc7	rozlišení
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nejsou	být	k5eNaImIp3nP	být
generické	generický	k2eAgInPc1d1	generický
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
videohry	videohra	k1gFnPc1	videohra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc1	přednost
termínu	termín	k1gInSc2	termín
HUD	HUD	kA	HUD
(	(	kIx(	(
<g/>
Head-Up-Display	Head-Up-Displaa	k1gMnSc2	Head-Up-Displaa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc4	první
grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
(	(	kIx(	(
<g/>
WIMP	WIMP	kA	WIMP
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
vyvinuto	vyvinout	k5eAaPmNgNnS	vyvinout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
ve	v	k7c6	v
vývojových	vývojový	k2eAgFnPc6d1	vývojová
laboratořích	laboratoř	k1gFnPc6	laboratoř
společnosti	společnost	k1gFnSc2	společnost
Xerox	Xerox	kA	Xerox
<g/>
.	.	kIx.	.
</s>
<s>
Oblibu	obliba	k1gFnSc4	obliba
mezi	mezi	k7c7	mezi
uživateli	uživatel	k1gMnPc7	uživatel
získalo	získat	k5eAaPmAgNnS	získat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
počítači	počítač	k1gInPc7	počítač
Mac	Mac	kA	Mac
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
v	v	k7c6	v
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
</s>
</p>
<p>
<s>
===	===	k?	===
Předchůdci	předchůdce	k1gMnSc3	předchůdce
GUI	GUI	kA	GUI
===	===	k?	===
</s>
</p>
<p>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
předchůdců	předchůdce	k1gMnPc2	předchůdce
GUI	GUI	kA	GUI
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
v	v	k7c4	v
Stanford	Stanford	k1gInSc4	Stanford
Research	Researcha	k1gFnPc2	Researcha
Institute	institut	k1gInSc5	institut
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
vedl	vést	k5eAaImAgMnS	vést
Douglas	Douglas	k1gMnSc1	Douglas
Engelbart	Engelbarta	k1gFnPc2	Engelbarta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
byly	být	k5eAaImAgInP	být
odkazy	odkaz	k1gInPc1	odkaz
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgInPc7	který
se	se	k3xPyFc4	se
manipulovalo	manipulovat	k5eAaImAgNnS	manipulovat
pomocí	pomocí	k7c2	pomocí
myši	myš	k1gFnSc2	myš
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
malého	malý	k2eAgNnSc2d1	malé
rozlišení	rozlišení	k1gNnSc2	rozlišení
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
obrazovek	obrazovka	k1gFnPc2	obrazovka
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
koncept	koncept	k1gInSc1	koncept
odkazů	odkaz	k1gInPc2	odkaz
byl	být	k5eAaImAgInS	být
dále	daleko	k6eAd2	daleko
vylepšován	vylepšovat	k5eAaImNgInS	vylepšovat
a	a	k8xC	a
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
pracovníky	pracovník	k1gMnPc4	pracovník
Xerox	Xerox	kA	Xerox
PARC	PARC	kA	PARC
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Alanem	Alan	k1gMnSc7	Alan
Kayem	Kay	k1gMnSc7	Kay
<g/>
.	.	kIx.	.
</s>
<s>
GUI	GUI	kA	GUI
bylo	být	k5eAaImAgNnS	být
primární	primární	k2eAgNnSc1d1	primární
rozhraní	rozhraní	k1gNnSc1	rozhraní
pro	pro	k7c4	pro
počítače	počítač	k1gInPc4	počítač
Xerox	Xerox	kA	Xerox
Alto	Alto	k6eAd1	Alto
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
moderních	moderní	k2eAgInPc2d1	moderní
a	a	k8xC	a
univerzálních	univerzální	k2eAgInPc2d1	univerzální
GUI	GUI	kA	GUI
bylo	být	k5eAaImAgNnS	být
odvozeno	odvozen	k2eAgNnSc1d1	odvozeno
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ivan	Ivan	k1gMnSc1	Ivan
Sutherland	Sutherland	k1gInSc4	Sutherland
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
systém	systém	k1gInSc1	systém
"	"	kIx"	"
<g/>
Sketchpad	Sketchpad	k1gInSc1	Sketchpad
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Používalo	používat	k5eAaImAgNnS	používat
se	se	k3xPyFc4	se
pero	pero	k1gNnSc1	pero
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	a
manipulaci	manipulace	k1gFnSc4	manipulace
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
v	v	k7c6	v
technických	technický	k2eAgInPc6d1	technický
výkresech	výkres	k1gInPc6	výkres
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
PARC	PARC	kA	PARC
===	===	k?	===
</s>
</p>
<p>
<s>
Uživatelské	uživatelský	k2eAgNnSc1d1	Uživatelské
rozhraní	rozhraní	k1gNnSc1	rozhraní
PARC	PARC	kA	PARC
se	se	k3xPyFc4	se
skládalo	skládat	k5eAaImAgNnS	skládat
z	z	k7c2	z
grafických	grafický	k2eAgMnPc2d1	grafický
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
oken	okno	k1gNnPc2	okno
<g/>
,	,	kIx,	,
nabídek	nabídka	k1gFnPc2	nabídka
(	(	kIx(	(
<g/>
menu	menu	k1gNnSc2	menu
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
radio	radio	k1gNnSc1	radio
<g/>
"	"	kIx"	"
polí	pole	k1gFnPc2	pole
<g/>
,	,	kIx,	,
zatrhávacích	zatrhávací	k2eAgNnPc2d1	zatrhávací
tlačítek	tlačítko	k1gNnPc2	tlačítko
a	a	k8xC	a
ikon	ikona	k1gFnPc2	ikona
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozhraní	rozhraní	k1gNnSc1	rozhraní
začalo	začít	k5eAaPmAgNnS	začít
používat	používat	k5eAaImF	používat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
klávesnicí	klávesnice	k1gFnSc7	klávesnice
také	také	k9	také
polohovací	polohovací	k2eAgNnPc4d1	polohovací
zařízení	zařízení	k1gNnPc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
aspekty	aspekt	k1gInPc1	aspekt
byly	být	k5eAaImAgInP	být
zdůrazněny	zdůraznit	k5eAaPmNgInP	zdůraznit
používáním	používání	k1gNnSc7	používání
alternativního	alternativní	k2eAgInSc2d1	alternativní
názvu	název	k1gInSc2	název
WIMP	WIMP	kA	WIMP
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
zkratkou	zkratka	k1gFnSc7	zkratka
pro	pro	k7c4	pro
názvy	název	k1gInPc4	název
windows	windowsa	k1gFnPc2	windowsa
(	(	kIx(	(
<g/>
okna	okno	k1gNnSc2	okno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
icons	icons	k1gInSc1	icons
(	(	kIx(	(
<g/>
ikony	ikona	k1gFnSc2	ikona
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
menus	menus	k1gInSc1	menus
(	(	kIx(	(
<g/>
nabídky	nabídka	k1gFnSc2	nabídka
<g/>
)	)	kIx)	)
a	a	k8xC	a
pointing	pointing	k1gInSc1	pointing
device	device	k1gInSc2	device
(	(	kIx(	(
<g/>
polohovací	polohovací	k2eAgNnPc1d1	polohovací
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Překotný	překotný	k2eAgInSc4d1	překotný
vývoj	vývoj	k1gInSc4	vývoj
===	===	k?	===
</s>
</p>
<p>
<s>
Následovatel	následovatel	k1gMnSc1	následovatel
PARCu	PARCus	k1gInSc2	PARCus
<g/>
,	,	kIx,	,
počítač	počítač	k1gInSc1	počítač
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
prvně	prvně	k?	prvně
GUI	GUI	kA	GUI
centralizovaně	centralizovaně	k6eAd1	centralizovaně
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Xerox	Xerox	kA	Xerox
8010	[number]	k4	8010
Star	Star	kA	Star
Information	Information	k1gInSc4	Information
system	syst	k1gInSc7	syst
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgFnP	následovat
počítače	počítač	k1gInPc4	počítač
Apple	Apple	kA	Apple
Lisa	Lisa	k1gFnSc1	Lisa
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Apple	Apple	kA	Apple
Macintosh	Macintosh	kA	Macintosh
128K	[number]	k4	128K
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Atari	Atar	k1gMnPc1	Atar
ST	St	kA	St
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
Commodore	Commodor	k1gInSc5	Commodor
Amiga	Amig	k1gMnSc2	Amig
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rané	raný	k2eAgInPc1d1	raný
GUI	GUI	kA	GUI
příkazy	příkaz	k1gInPc1	příkaz
<g/>
,	,	kIx,	,
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
IBM	IBM	kA	IBM
Common	Common	k1gInSc1	Common
User	usrat	k5eAaPmRp2nS	usrat
Access	Accessa	k1gFnPc2	Accessa
<g/>
,	,	kIx,	,
používaly	používat	k5eAaImAgFnP	používat
různé	různý	k2eAgFnPc1d1	různá
příkazové	příkazový	k2eAgFnPc1d1	příkazová
sekvence	sekvence	k1gFnPc1	sekvence
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
programy	program	k1gInPc4	program
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
klávesa	klávesa	k1gFnSc1	klávesa
F3	F3	k1gFnSc1	F3
aktivovala	aktivovat	k5eAaBmAgFnS	aktivovat
nápovědu	nápověda	k1gFnSc4	nápověda
v	v	k7c6	v
programu	program	k1gInSc6	program
WordPerfect	Wordperfect	kA	Wordperfect
<g/>
.	.	kIx.	.
</s>
<s>
Nabídky	nabídka	k1gFnPc1	nabídka
(	(	kIx(	(
<g/>
menu	menu	k1gNnSc1	menu
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
přístupné	přístupný	k2eAgInPc1d1	přístupný
pomocí	pomocí	k7c2	pomocí
různých	různý	k2eAgFnPc2d1	různá
kláves	klávesa	k1gFnPc2	klávesa
(	(	kIx(	(
<g/>
control	control	k1gInSc1	control
v	v	k7c4	v
WordStar	WordStar	k1gInSc4	WordStar
<g/>
,	,	kIx,	,
Alt	alt	k1gInSc4	alt
nebo	nebo	k8xC	nebo
F10	F10	k1gFnSc4	F10
v	v	k7c6	v
programech	program	k1gInPc6	program
společnosti	společnost	k1gFnSc2	společnost
Microsoft	Microsoft	kA	Microsoft
<g/>
,	,	kIx,	,
pomocí	pomoc	k1gFnSc7	pomoc
"	"	kIx"	"
<g/>
/	/	kIx~	/
<g/>
"	"	kIx"	"
v	v	k7c6	v
Lotusu	Lotus	k1gInSc6	Lotus
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
F9	F9	k1gFnSc1	F9
v	v	k7c4	v
Norton	Norton	k1gInSc4	Norton
Commanderu	Commander	k1gInSc2	Commander
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
těmto	tento	k3xDgInPc3	tento
programovým	programový	k2eAgInPc3d1	programový
rozdílům	rozdíl	k1gInPc3	rozdíl
byly	být	k5eAaImAgFnP	být
vyráběny	vyráběn	k2eAgFnPc1d1	vyráběna
plastové	plastový	k2eAgFnPc1d1	plastová
nebo	nebo	k8xC	nebo
dřevěné	dřevěný	k2eAgFnPc1d1	dřevěná
masky	maska	k1gFnPc1	maska
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
na	na	k7c6	na
plochách	plocha	k1gFnPc6	plocha
kolem	kolem	k7c2	kolem
kláves	klávesa	k1gFnPc2	klávesa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
byly	být	k5eAaImAgFnP	být
napsány	napsán	k2eAgFnPc1d1	napsána
funkce	funkce	k1gFnPc1	funkce
platné	platný	k2eAgFnPc1d1	platná
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
programy	program	k1gInPc4	program
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Post-WIMP	Post-WIMP	k1gFnSc3	Post-WIMP
rozhraní	rozhraní	k1gNnSc2	rozhraní
===	===	k?	===
</s>
</p>
<p>
<s>
Aplikace	aplikace	k1gFnPc1	aplikace
na	na	k7c6	na
menších	malý	k2eAgNnPc6d2	menší
přenosných	přenosný	k2eAgNnPc6d1	přenosné
zařízeních	zařízení	k1gNnPc6	zařízení
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
chytré	chytrý	k2eAgInPc1d1	chytrý
mobilní	mobilní	k2eAgInPc1d1	mobilní
telefony	telefon	k1gInPc1	telefon
nebo	nebo	k8xC	nebo
PDA	PDA	kA	PDA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgNnSc4	který
WIMP	WIMP	kA	WIMP
není	být	k5eNaImIp3nS	být
uzpůsoben	uzpůsobit	k5eAaPmNgInS	uzpůsobit
nejlépe	dobře	k6eAd3	dobře
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
novější	nový	k2eAgFnPc1d2	novější
techniky	technika	k1gFnPc1	technika
interakce	interakce	k1gFnSc2	interakce
s	s	k7c7	s
uživatelem	uživatel	k1gMnSc7	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
techniky	technika	k1gFnPc1	technika
jsou	být	k5eAaImIp3nP	být
obecně	obecně	k6eAd1	obecně
nazývány	nazývat	k5eAaImNgFnP	nazývat
Post-WIMP	Post-WIMP	k1gFnPc1	Post-WIMP
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
už	už	k6eAd1	už
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
začala	začít	k5eAaPmAgNnP	začít
objevovat	objevovat	k5eAaImF	objevovat
zařízení	zařízení	k1gNnPc1	zařízení
založená	založený	k2eAgNnPc1d1	založené
na	na	k7c6	na
ovládání	ovládání	k1gNnSc6	ovládání
dotyky	dotyk	k1gInPc7	dotyk
(	(	kIx(	(
<g/>
Android	android	k1gInSc1	android
nebo	nebo	k8xC	nebo
iPhone	iPhon	k1gMnSc5	iPhon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnPc1	jejichž
uživatelská	uživatelský	k2eAgNnPc1d1	Uživatelské
rozhraní	rozhraní	k1gNnPc1	rozhraní
jsou	být	k5eAaImIp3nP	být
Post-WIMP	Post-WIMP	k1gMnPc4	Post-WIMP
<g/>
.	.	kIx.	.
</s>
<s>
Uživatelé	uživatel	k1gMnPc1	uživatel
používají	používat	k5eAaImIp3nP	používat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jeden	jeden	k4xCgInSc4	jeden
prst	prst	k1gInSc4	prst
pro	pro	k7c4	pro
interakci	interakce	k1gFnSc4	interakce
se	s	k7c7	s
zařízením	zařízení	k1gNnSc7	zařízení
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
displeje	displej	k1gInSc2	displej
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jim	on	k3xPp3gMnPc3	on
dovolí	dovolit	k5eAaPmIp3nS	dovolit
provádět	provádět	k5eAaImF	provádět
akce	akce	k1gFnSc1	akce
typu	typ	k1gInSc2	typ
přibližování	přibližování	k1gNnSc2	přibližování
(	(	kIx(	(
<g/>
pomocí	pomocí	k7c2	pomocí
sbíhání	sbíhání	k1gNnSc2	sbíhání
dvou	dva	k4xCgInPc2	dva
prstů	prst	k1gInPc2	prst
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
rotace	rotace	k1gFnSc1	rotace
s	s	k7c7	s
objekty	objekt	k1gInPc7	objekt
zobrazenými	zobrazený	k2eAgInPc7d1	zobrazený
na	na	k7c6	na
displeji	displej	k1gInSc6	displej
(	(	kIx(	(
<g/>
kroužení	kroužení	k1gNnSc6	kroužení
dvěma	dva	k4xCgInPc7	dva
prsty	prst	k1gInPc7	prst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
podotknout	podotknout	k5eAaPmF	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
takové	takový	k3xDgFnPc1	takový
akce	akce	k1gFnPc1	akce
neprovedly	provést	k5eNaPmAgFnP	provést
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
myši	myš	k1gFnSc2	myš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Porovnání	porovnání	k1gNnSc1	porovnání
s	s	k7c7	s
příkazovým	příkazový	k2eAgInSc7d1	příkazový
řádkem	řádek	k1gInSc7	řádek
(	(	kIx(	(
<g/>
CLI	clít	k5eAaImRp2nS	clít
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
GUI	GUI	kA	GUI
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jako	jako	k8xC	jako
potřeba	potřeba	k6eAd1	potřeba
nahradit	nahradit	k5eAaPmF	nahradit
rozhraní	rozhraní	k1gNnSc4	rozhraní
příkazového	příkazový	k2eAgInSc2d1	příkazový
řádku	řádek	k1gInSc2	řádek
(	(	kIx(	(
<g/>
Command	Command	k1gInSc1	Command
Line	linout	k5eAaImIp3nS	linout
Interface	interface	k1gInSc4	interface
<g/>
)	)	kIx)	)
něčím	něco	k3yInSc7	něco
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
rychleji	rychle	k6eAd2	rychle
naučili	naučit	k5eAaPmAgMnP	naučit
a	a	k8xC	a
všechny	všechen	k3xTgInPc4	všechen
příkazy	příkaz	k1gInPc4	příkaz
nemuseli	muset	k5eNaImAgMnP	muset
psát	psát	k5eAaImF	psát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CLI	clít	k5eAaImRp2nS	clít
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
velkou	velký	k2eAgFnSc4d1	velká
efektivitu	efektivita	k1gFnSc4	efektivita
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
uživatel	uživatel	k1gMnSc1	uživatel
naučí	naučit	k5eAaPmIp3nS	naučit
příkazy	příkaz	k1gInPc4	příkaz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naučení	naučení	k1gNnPc4	naučení
zabere	zabrat	k5eAaPmIp3nS	zabrat
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
věcí	věc	k1gFnSc7	věc
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
používání	používání	k1gNnSc1	používání
příkazového	příkazový	k2eAgInSc2d1	příkazový
řádku	řádek	k1gInSc2	řádek
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pomalé	pomalý	k2eAgNnSc1d1	pomalé
<g/>
,	,	kIx,	,
když	když	k8xS	když
uživatel	uživatel	k1gMnSc1	uživatel
zadává	zadávat	k5eAaImIp3nS	zadávat
příkazy	příkaz	k1gInPc4	příkaz
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
parametry	parametr	k1gInPc7	parametr
s	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
cesty	cesta	k1gFnSc2	cesta
k	k	k7c3	k
souborům	soubor	k1gInPc3	soubor
na	na	k7c6	na
disku	disk	k1gInSc6	disk
<g/>
.	.	kIx.	.
</s>
<s>
WIMP	WIMP	kA	WIMP
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
různé	různý	k2eAgFnPc4d1	různá
tlačítka	tlačítko	k1gNnPc4	tlačítko
apod.	apod.	kA	apod.
reprezentující	reprezentující	k2eAgInPc4d1	reprezentující
rozličné	rozličný	k2eAgInPc4d1	rozličný
systémové	systémový	k2eAgInPc4d1	systémový
příkazy	příkaz	k1gInPc4	příkaz
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
několik	několik	k4yIc4	několik
kliknutí	kliknutí	k1gNnPc2	kliknutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
GUI	GUI	kA	GUI
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
horší	horší	k1gNnSc4	horší
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
některá	některý	k3yIgNnPc1	některý
nastavení	nastavení	k1gNnPc1	nastavení
jsou	být	k5eAaImIp3nP	být
příliš	příliš	k6eAd1	příliš
hluboko	hluboko	k6eAd1	hluboko
v	v	k7c6	v
systému	systém	k1gInSc6	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ale	ale	k9	ale
doklikání	doklikání	k1gNnSc1	doklikání
ve	v	k7c6	v
WIMP	WIMP	kA	WIMP
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
snadnější	snadný	k2eAgMnSc1d2	snazší
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
příkazových	příkazový	k2eAgInPc6d1	příkazový
řádcích	řádek	k1gInPc6	řádek
neplatí	platit	k5eNaImIp3nP	platit
všechny	všechen	k3xTgInPc1	všechen
příkazy	příkaz	k1gInPc1	příkaz
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
adresáře	adresář	k1gInPc4	adresář
nebo	nebo	k8xC	nebo
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
nutné	nutný	k2eAgNnSc1d1	nutné
přeskakovat	přeskakovat	k5eAaImF	přeskakovat
z	z	k7c2	z
adresáře	adresář	k1gInSc2	adresář
do	do	k7c2	do
adresáře	adresář	k1gInSc2	adresář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
moderních	moderní	k2eAgInPc2d1	moderní
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
jak	jak	k6eAd1	jak
WIMP	WIMP	kA	WIMP
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k8xC	i
CLI	clít	k5eAaImRp2nS	clít
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
GUI	GUI	kA	GUI
získává	získávat	k5eAaImIp3nS	získávat
u	u	k7c2	u
běžných	běžný	k2eAgMnPc2d1	běžný
uživatelů	uživatel	k1gMnPc2	uživatel
větší	veliký	k2eAgFnSc4d2	veliký
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aplikace	aplikace	k1gFnPc1	aplikace
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
obě	dva	k4xCgNnPc1	dva
rozhraní	rozhraní	k1gNnPc1	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgFnSc1	první
bylo	být	k5eAaImAgNnS	být
CLI	clít	k5eAaImRp2nS	clít
<g/>
,	,	kIx,	,
a	a	k8xC	a
GUI	GUI	kA	GUI
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
jejím	její	k3xOp3gInSc7	její
obalem	obal	k1gInSc7	obal
<g/>
,	,	kIx,	,
zjednodušujícím	zjednodušující	k2eAgInSc7d1	zjednodušující
některé	některý	k3yIgFnPc4	některý
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vývojáři	vývojář	k1gMnPc1	vývojář
mohli	moct	k5eAaImAgMnP	moct
vyzkoušet	vyzkoušet	k5eAaPmF	vyzkoušet
funkčnost	funkčnost	k1gFnSc4	funkčnost
programu	program	k1gInSc2	program
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
obtěžovali	obtěžovat	k5eAaImAgMnP	obtěžovat
s	s	k7c7	s
vývojem	vývoj	k1gInSc7	vývoj
grafického	grafický	k2eAgNnSc2d1	grafické
rozhraní	rozhraní	k1gNnSc2	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
povětšinou	povětšinou	k6eAd1	povětšinou
v	v	k7c6	v
operačních	operační	k2eAgInPc6d1	operační
systémech	systém	k1gInPc6	systém
typu	typ	k1gInSc2	typ
UNIX	UNIX	kA	UNIX
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
to	ten	k3xDgNnSc1	ten
uživatelům	uživatel	k1gMnPc3	uživatel
používat	používat	k5eAaImF	používat
programy	program	k1gInPc4	program
jako	jako	k8xS	jako
automatizované	automatizovaný	k2eAgInPc4d1	automatizovaný
skripty	skript	k1gInPc4	skript
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
pouze	pouze	k6eAd1	pouze
přítomným	přítomný	k2eAgMnSc7d1	přítomný
uživatelem	uživatel	k1gMnSc7	uživatel
jednorázová	jednorázový	k2eAgNnPc1d1	jednorázové
spouštění	spouštění	k1gNnPc1	spouštění
přes	přes	k7c4	přes
GUI	GUI	kA	GUI
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
3D	[number]	k4	3D
uživatelská	uživatelský	k2eAgFnSc1d1	Uživatelská
rozhraní	rozhraní	k1gNnSc3	rozhraní
u	u	k7c2	u
PC	PC	kA	PC
==	==	k?	==
</s>
</p>
<p>
<s>
Označení	označení	k1gNnSc1	označení
3D	[number]	k4	3D
není	být	k5eNaImIp3nS	být
přesné	přesný	k2eAgNnSc1d1	přesné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
počítačové	počítačový	k2eAgFnPc1d1	počítačová
obrazovky	obrazovka	k1gFnPc1	obrazovka
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
pouze	pouze	k6eAd1	pouze
dvoudimenzionální	dvoudimenzionální	k2eAgFnSc1d1	dvoudimenzionální
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
<g/>
,	,	kIx,	,
různá	různý	k2eAgNnPc4d1	různé
grafická	grafický	k2eAgNnPc4d1	grafické
prostředí	prostředí	k1gNnPc4	prostředí
používají	používat	k5eAaImIp3nP	používat
tři	tři	k4xCgInPc4	tři
rozměry	rozměr	k1gInPc4	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Výšku	výška	k1gFnSc4	výška
a	a	k8xC	a
šířku	šířka	k1gFnSc4	šířka
doplňují	doplňovat	k5eAaImIp3nP	doplňovat
vrstvením	vrstvení	k1gNnSc7	vrstvení
nebo	nebo	k8xC	nebo
stohováním	stohování	k1gNnSc7	stohování
objektů	objekt	k1gInPc2	objekt
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
to	ten	k3xDgNnSc1	ten
doprovázeno	doprovázen	k2eAgNnSc1d1	doprovázeno
průhledností	průhlednost	k1gFnSc7	průhlednost
objektů	objekt	k1gInPc2	objekt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
3D	[number]	k4	3D
našlo	najít	k5eAaPmAgNnS	najít
své	svůj	k3xOyFgNnSc1	svůj
uplatnění	uplatnění	k1gNnSc1	uplatnění
ve	v	k7c6	v
filmové	filmový	k2eAgFnSc6d1	filmová
produkci	produkce	k1gFnSc6	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmostí	samozřejmost	k1gFnSc7	samozřejmost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
narůstá	narůstat	k5eAaImIp3nS	narůstat
složitost	složitost	k1gFnSc1	složitost
výpočtu	výpočet	k1gInSc2	výpočet
3D	[number]	k4	3D
animací	animace	k1gFnPc2	animace
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
také	také	k9	také
narůstat	narůstat	k5eAaImF	narůstat
výkon	výkon	k1gInSc4	výkon
hardwaru	hardware	k1gInSc2	hardware
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
výpočty	výpočet	k1gInPc4	výpočet
probíhají	probíhat	k5eAaImIp3nP	probíhat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jiná	jiný	k2eAgNnPc1d1	jiné
uživatelská	uživatelský	k2eAgNnPc1d1	Uživatelské
rozhraní	rozhraní	k1gNnPc1	rozhraní
==	==	k?	==
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
grafických	grafický	k2eAgMnPc2d1	grafický
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
jiná	jiný	k2eAgNnPc4d1	jiné
uživatelská	uživatelský	k2eAgNnPc4d1	Uživatelské
rozhraní	rozhraní	k1gNnPc4	rozhraní
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
textové	textový	k2eAgNnSc4d1	textové
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
(	(	kIx(	(
<g/>
s	s	k7c7	s
menu	menu	k1gNnSc7	menu
<g/>
,	,	kIx,	,
tlačítky	tlačítko	k1gNnPc7	tlačítko
a	a	k8xC	a
myší	myš	k1gFnSc7	myš
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
příkazový	příkazový	k2eAgInSc4d1	příkazový
řádek	řádek	k1gInSc4	řádek
(	(	kIx(	(
<g/>
příkazy	příkaz	k1gInPc1	příkaz
se	se	k3xPyFc4	se
zadávají	zadávat	k5eAaImIp3nP	zadávat
jejich	jejich	k3xOp3gNnSc7	jejich
zapsáním	zapsání	k1gNnSc7	zapsání
pomocí	pomocí	k7c2	pomocí
klávesnice	klávesnice	k1gFnSc2	klávesnice
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
braillský	braillský	k2eAgInSc1d1	braillský
řádek	řádek	k1gInSc1	řádek
</s>
</p>
<p>
<s>
hlasová	hlasový	k2eAgNnPc1d1	hlasové
rozhraní	rozhraní	k1gNnPc1	rozhraní
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Ovládací	ovládací	k2eAgInSc1d1	ovládací
prvek	prvek	k1gInSc1	prvek
(	(	kIx(	(
<g/>
widget	widget	k1gInSc1	widget
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
X	X	kA	X
Window	Window	k1gFnSc1	Window
System	Syst	k1gInSc7	Syst
–	–	k?	–
grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
prostředí	prostředí	k1gNnSc4	prostředí
pro	pro	k7c4	pro
unixové	unixový	k2eAgInPc4d1	unixový
systémy	systém	k1gInPc4	systém
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
grafické	grafický	k2eAgNnSc4d1	grafické
uživatelské	uživatelský	k2eAgNnSc4d1	Uživatelské
rozhraní	rozhraní	k1gNnSc4	rozhraní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seriál	seriál	k1gInSc1	seriál
Jak	jak	k8xS	jak
na	na	k7c4	na
GUI	GUI	kA	GUI
s	s	k7c7	s
wxPythonem	wxPython	k1gInSc7	wxPython
</s>
</p>
<p>
<s>
In	In	k?	In
the	the	k?	the
Beginning	Beginning	k1gInSc1	Beginning
was	was	k?	was
the	the	k?	the
Command	Command	k1gInSc1	Command
Line	linout	k5eAaImIp3nS	linout
–	–	k?	–
stať	stať	k1gFnSc1	stať
Neala	Neala	k1gFnSc1	Neala
Stephensona	Stephensona	k1gFnSc1	Stephensona
o	o	k7c6	o
operačních	operační	k2eAgInPc6d1	operační
systémech	systém	k1gInPc6	systém
a	a	k8xC	a
GUI	GUI	kA	GUI
obsahující	obsahující	k2eAgFnSc4d1	obsahující
řadu	řada	k1gFnSc4	řada
barvitých	barvitý	k2eAgFnPc2d1	barvitá
metafor	metafora	k1gFnPc2	metafora
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
zazipovaný	zazipovaný	k2eAgInSc4d1	zazipovaný
<g/>
.	.	kIx.	.
</s>
<s>
Online	Onlinout	k5eAaPmIp3nS	Onlinout
mirror	mirror	k1gInSc1	mirror
</s>
</p>
<p>
<s>
Nové	Nové	k2eAgNnSc1d1	Nové
grafické	grafický	k2eAgNnSc1d1	grafické
rozhraní	rozhraní	k1gNnSc1	rozhraní
aplikace	aplikace	k1gFnSc2	aplikace
Scratch	Scratcha	k1gFnPc2	Scratcha
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
3.0	[number]	k4	3.0
</s>
</p>
