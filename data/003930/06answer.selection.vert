<s>
Ochrana	ochrana	k1gFnSc1	ochrana
přírody	příroda	k1gFnSc2	příroda
se	se	k3xPyFc4	se
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
soustřeďuje	soustřeďovat	k5eAaImIp3nS	soustřeďovat
zejména	zejména	k9	zejména
do	do	k7c2	do
23	[number]	k4	23
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
<g/>
:	:	kIx,	:
Babiogórski	Babiogórske	k1gFnSc4	Babiogórske
<g/>
,	,	kIx,	,
Białowieski	Białowieski	k1gNnPc4	Białowieski
<g/>
,	,	kIx,	,
Biebrzański	Biebrzańsk	k1gFnPc4	Biebrzańsk
<g/>
,	,	kIx,	,
Bieszczadzki	Bieszczadzk	k1gFnPc4	Bieszczadzk
<g/>
,	,	kIx,	,
Bory	bor	k1gInPc4	bor
Tucholskie	Tucholskie	k1gFnSc2	Tucholskie
<g/>
,	,	kIx,	,
Drawieński	Drawieński	k1gNnSc2	Drawieński
<g/>
,	,	kIx,	,
Gorczański	Gorczański	k1gNnSc2	Gorczański
<g/>
,	,	kIx,	,
Gór	Gór	k1gMnSc1	Gór
Stołowych	Stołowych	k1gMnSc1	Stołowych
<g/>
,	,	kIx,	,
Kampinoski	Kampinosk	k1gMnPc5	Kampinosk
<g/>
,	,	kIx,	,
Karkonoski	Karkonosk	k1gMnPc5	Karkonosk
<g/>
,	,	kIx,	,
Magurski	Magursk	k1gMnPc5	Magursk
<g/>
,	,	kIx,	,
Narwiański	Narwiańsk	k1gMnPc5	Narwiańsk
<g/>
,	,	kIx,	,
Ojcowski	Ojcowsk	k1gMnPc5	Ojcowsk
<g/>
,	,	kIx,	,
Pieniński	Pienińsk	k1gMnPc5	Pienińsk
<g/>
,	,	kIx,	,
Poleski	Polesk	k1gMnPc5	Polesk
<g/>
,	,	kIx,	,
Roztoczański	Roztoczańsk	k1gMnPc5	Roztoczańsk
<g/>
,	,	kIx,	,
Słowiński	Słowińsk	k1gMnPc5	Słowińsk
<g/>
,	,	kIx,	,
Swiętokrzyski	Swiętokrzysk	k1gMnPc5	Swiętokrzysk
<g/>
,	,	kIx,	,
Tatrzański	Tatrzańsk	k1gMnPc5	Tatrzańsk
<g/>
,	,	kIx,	,
Ujście	Ujście	k1gFnSc2	Ujście
Warty	Warta	k1gFnSc2	Warta
<g/>
,	,	kIx,	,
Wielkopolski	Wielkopolsk	k1gFnSc2	Wielkopolsk
<g/>
,	,	kIx,	,
Wigierski	Wigiersk	k1gFnSc2	Wigiersk
<g/>
,	,	kIx,	,
Woliński	Wolińsk	k1gFnPc4	Wolińsk
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
