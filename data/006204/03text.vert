<s>
Alfredo	Alfredo	k1gNnSc1	Alfredo
James	James	k1gInSc1	James
Pacino	Pacino	k1gNnSc1	Pacino
(	(	kIx(	(
<g/>
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1940	[number]	k4	1940
New	New	k1gFnPc2	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
filmový	filmový	k2eAgMnSc1d1	filmový
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
příležitostně	příležitostně	k6eAd1	příležitostně
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
také	také	k9	také
režii	režie	k1gFnSc4	režie
<g/>
.	.	kIx.	.
</s>
<s>
Al	ala	k1gFnPc2	ala
Pacino	Pacino	k1gNnSc1	Pacino
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
americkým	americký	k2eAgMnPc3d1	americký
hercům	herec	k1gMnPc3	herec
<g/>
,	,	kIx,	,
ztvárňuje	ztvárňovat	k5eAaImIp3nS	ztvárňovat
především	především	k9	především
typ	typ	k1gInSc1	typ
antihrdiny	antihrdina	k1gMnSc2	antihrdina
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
jej	on	k3xPp3gMnSc4	on
nejčastěji	často	k6eAd3	často
dabuje	dabovat	k5eAaBmIp3nS	dabovat
Jiří	Jiří	k1gMnSc1	Jiří
Prager	Prager	k1gMnSc1	Prager
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Harlemu	Harlem	k1gInSc6	Harlem
<g/>
,	,	kIx,	,
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k9	když
mu	on	k3xPp3gMnSc3	on
byly	být	k5eAaImAgFnP	být
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnPc1	jeho
rodiče	rodič	k1gMnPc1	rodič
se	se	k3xPyFc4	se
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
<g/>
.	.	kIx.	.
</s>
<s>
Přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
se	se	k3xPyFc4	se
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
do	do	k7c2	do
Bronxu	Bronx	k1gInSc2	Bronx
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žili	žít	k5eAaImAgMnP	žít
jeho	jeho	k3xOp3gMnPc4	jeho
prarodiče	prarodič	k1gMnPc4	prarodič
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
přišli	přijít	k5eAaPmAgMnP	přijít
z	z	k7c2	z
Corleone	Corleon	k1gInSc5	Corleon
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
High	High	k1gInSc1	High
School	School	k1gInSc1	School
of	of	k?	of
the	the	k?	the
Performing	Performing	k1gInSc1	Performing
Arts	Arts	k1gInSc1	Arts
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
herectví	herectví	k1gNnSc4	herectví
v	v	k7c6	v
newyorské	newyorský	k2eAgFnSc6d1	newyorská
herecké	herecký	k2eAgFnSc6d1	herecká
škole	škola	k1gFnSc6	škola
Actors	Actors	k1gInSc1	Actors
<g/>
'	'	kIx"	'
Studio	studio	k1gNnSc1	studio
Lee	Lea	k1gFnSc3	Lea
Strasberga	Strasberga	k1gFnSc1	Strasberga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1982	[number]	k4	1982
–	–	k?	–
1984	[number]	k4	1984
v	v	k7c6	v
Actors	Actors	k1gInSc1	Actors
Studiu	studio	k1gNnSc3	studio
působil	působit	k5eAaImAgInS	působit
jako	jako	k9	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
uměleckých	umělecký	k2eAgMnPc2d1	umělecký
ředitelů	ředitel	k1gMnPc2	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
celoživotní	celoživotní	k2eAgFnSc7d1	celoživotní
láskou	láska	k1gFnSc7	láska
je	být	k5eAaImIp3nS	být
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velkým	velký	k2eAgMnSc7d1	velký
fanouškem	fanoušek	k1gMnSc7	fanoušek
všech	všecek	k3xTgFnPc2	všecek
her	hra	k1gFnPc2	hra
od	od	k7c2	od
Williama	William	k1gMnSc2	William
Shakespeara	Shakespeare	k1gMnSc2	Shakespeare
<g/>
.	.	kIx.	.
</s>
<s>
Al	ala	k1gFnPc2	ala
Pacino	Pacino	k1gNnSc1	Pacino
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neoženil	oženit	k5eNaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc4	jeho
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Tarrantové	Tarrant	k1gMnPc1	Tarrant
<g/>
,	,	kIx,	,
narodila	narodit	k5eAaPmAgFnS	narodit
dcera	dcera	k1gFnSc1	dcera
Julia	Julius	k1gMnSc2	Julius
Marie	Maria	k1gFnSc2	Maria
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2001	[number]	k4	2001
jeho	jeho	k3xOp3gFnSc4	jeho
dlouholetá	dlouholetý	k2eAgFnSc1d1	dlouholetá
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
Beverly	Beverla	k1gFnSc2	Beverla
D	D	kA	D
<g/>
'	'	kIx"	'
<g/>
Angelová	Angelová	k1gFnSc1	Angelová
porodila	porodit	k5eAaPmAgFnS	porodit
dvojčata	dvojče	k1gNnPc1	dvojče
Antona	Anton	k1gMnSc2	Anton
Jamese	Jamese	k1gFnSc2	Jamese
a	a	k8xC	a
Olivii	Olivie	k1gFnSc4	Olivie
Rose	Ros	k1gMnSc2	Ros
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
dílo	dílo	k1gNnSc4	dílo
byl	být	k5eAaImAgInS	být
vyznamenán	vyznamenat	k5eAaPmNgInS	vyznamenat
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
Zlatým	zlatý	k2eAgInSc7d1	zlatý
lvem	lev	k1gInSc7	lev
na	na	k7c6	na
Mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
1994	[number]	k4	1994
a	a	k8xC	a
na	na	k7c6	na
Mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
San	San	k1gMnSc6	San
Sebastianu	Sebastian	k1gMnSc6	Sebastian
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
cenou	cena	k1gFnSc7	cena
AFI	AFI	kA	AFI
Lifetime	Lifetim	k1gInSc5	Lifetim
Achievement	Achievement	k1gInSc4	Achievement
Award	Award	k1gInSc4	Award
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
filmové	filmový	k2eAgNnSc4d1	filmové
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
divadelní	divadelní	k2eAgFnSc4d1	divadelní
kariéru	kariéra	k1gFnSc4	kariéra
zahájil	zahájit	k5eAaPmAgMnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
získal	získat	k5eAaPmAgMnS	získat
cenu	cena	k1gFnSc4	cena
Obie	Obi	k1gFnSc2	Obi
za	za	k7c4	za
roli	role	k1gFnSc4	role
v	v	k7c6	v
divadelním	divadelní	k2eAgNnSc6d1	divadelní
představení	představení	k1gNnSc6	představení
Indiani	Indian	k1gMnPc1	Indian
chtějí	chtít	k5eAaImIp3nP	chtít
Bronx	Bronx	k1gInSc4	Bronx
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
debutoval	debutovat	k5eAaBmAgMnS	debutovat
na	na	k7c6	na
Brodwayi	Brodway	k1gFnSc6	Brodway
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Nosí	nosit	k5eAaImIp3nS	nosit
tygr	tygr	k1gMnSc1	tygr
kravatu	kravata	k1gFnSc4	kravata
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
byl	být	k5eAaImAgMnS	být
oceněn	oceněn	k2eAgMnSc1d1	oceněn
cenami	cena	k1gFnPc7	cena
Tony	Tony	k1gFnSc7	Tony
a	a	k8xC	a
Theatre	Theatr	k1gInSc5	Theatr
World	Worlda	k1gFnPc2	Worlda
Award	Awardo	k1gNnPc2	Awardo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
menší	malý	k2eAgFnSc4d2	menší
filmovou	filmový	k2eAgFnSc4d1	filmová
roli	role	k1gFnSc4	role
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1969	[number]	k4	1969
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
Natálie	Natálie	k1gFnSc1	Natálie
(	(	kIx(	(
<g/>
Me	Me	k1gFnSc1	Me
<g/>
,	,	kIx,	,
Natalie	Natalie	k1gFnSc1	Natalie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
režiséra	režisér	k1gMnSc2	režisér
Francise	Francise	k1gFnSc2	Francise
F.	F.	kA	F.
Copollu	Copolla	k1gMnSc4	Copolla
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
jeho	jeho	k3xOp3gNnSc4	jeho
role	role	k1gFnSc1	role
narkomana	narkoman	k1gMnSc2	narkoman
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Panika	panika	k1gFnSc1	panika
v	v	k7c6	v
Needle	Needle	k1gFnPc6	Needle
Parku	park	k1gInSc2	park
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Panic	panic	k1gMnSc1	panic
in	in	k?	in
Needle	Needle	k1gFnSc1	Needle
Park	park	k1gInSc1	park
<g/>
)	)	kIx)	)
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
mu	on	k3xPp3gMnSc3	on
roli	role	k1gFnSc4	role
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
novém	nový	k2eAgInSc6d1	nový
filmu	film	k1gInSc6	film
Kmotr	kmotr	k1gMnSc1	kmotr
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Godfather	Godfathra	k1gFnPc2	Godfathra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
role	role	k1gFnSc1	role
Michaela	Michael	k1gMnSc2	Michael
Corleoneho	Corleone	k1gMnSc2	Corleone
v	v	k7c6	v
mafiánské	mafiánský	k2eAgFnSc6d1	mafiánská
sáze	sága	k1gFnSc6	sága
podle	podle	k7c2	podle
románu	román	k1gInSc2	román
Maria	Maria	k1gFnSc1	Maria
Puza	Puza	k1gFnSc1	Puza
ho	on	k3xPp3gInSc4	on
proslavila	proslavit	k5eAaPmAgFnS	proslavit
a	a	k8xC	a
otevřela	otevřít	k5eAaPmAgFnS	otevřít
mu	on	k3xPp3gMnSc3	on
dveře	dveře	k1gFnPc4	dveře
k	k	k7c3	k
další	další	k2eAgFnSc3d1	další
kariéře	kariéra	k1gFnSc3	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
i	i	k9	i
jeho	jeho	k3xOp3gNnSc1	jeho
pokračování	pokračování	k1gNnSc1	pokračování
je	být	k5eAaImIp3nS	být
mnohými	mnohý	k2eAgInPc7d1	mnohý
filmovými	filmový	k2eAgInPc7d1	filmový
žebřičky	žebřička	k1gFnPc1	žebřička
řazen	řadit	k5eAaImNgMnS	řadit
mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
filmy	film	k1gInPc4	film
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
roli	role	k1gFnSc4	role
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
,	,	kIx,	,
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
a	a	k8xC	a
ceny	cena	k1gFnPc4	cena
BAFTA	BAFTA	kA	BAFTA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgInS	zahrát
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Gena	Genus	k1gMnSc2	Genus
Hackmana	Hackman	k1gMnSc2	Hackman
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Strašák	strašák	k1gMnSc1	strašák
(	(	kIx(	(
<g/>
Scarecrow	Scarecrow	k1gMnSc1	Scarecrow
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
kriminálním	kriminální	k2eAgNnSc6d1	kriminální
dramatu	drama	k1gNnSc6	drama
Serpico	Serpico	k1gMnSc1	Serpico
<g/>
,	,	kIx,	,
za	za	k7c4	za
který	který	k3yQgInSc4	který
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
rok	rok	k1gInSc4	rok
si	se	k3xPyFc3	se
zopakoval	zopakovat	k5eAaPmAgMnS	zopakovat
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
Michaela	Michael	k1gMnSc2	Michael
Corleoneho	Corleone	k1gMnSc2	Corleone
v	v	k7c6	v
pokračování	pokračování	k1gNnSc6	pokračování
Kmotr	kmotr	k1gMnSc1	kmotr
II	II	kA	II
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Godfather	Godfathra	k1gFnPc2	Godfathra
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c7	za
kterou	který	k3yQgFnSc7	který
byl	být	k5eAaImAgMnS	být
oceněn	oceněn	k2eAgInSc4d1	oceněn
cenou	cena	k1gFnSc7	cena
BAFTA	BAFTA	kA	BAFTA
a	a	k8xC	a
potřetí	potřetí	k4xO	potřetí
byl	být	k5eAaImAgInS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
i	i	k8xC	i
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
ještě	ještě	k6eAd1	ještě
stihl	stihnout	k5eAaPmAgInS	stihnout
natočit	natočit	k5eAaBmF	natočit
krimi	krimi	k1gNnSc7	krimi
drama	drama	k1gNnSc4	drama
Psí	psí	k1gNnSc2	psí
odpoledne	odpoledne	k1gNnSc2	odpoledne
(	(	kIx(	(
<g/>
Dog	doga	k1gFnPc2	doga
Day	Day	k1gFnPc2	Day
Afternoon	Afternoon	k1gInSc1	Afternoon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
bankovního	bankovní	k2eAgMnSc4d1	bankovní
lupiče	lupič	k1gMnSc4	lupič
Sonnyho	Sonny	k1gMnSc4	Sonny
<g/>
,	,	kIx,	,
za	za	k7c4	za
který	který	k3yQgInSc4	který
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
i	i	k8xC	i
Zlatý	zlatý	k2eAgInSc4d1	zlatý
Glóbus	glóbus	k1gInSc4	glóbus
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
cenu	cena	k1gFnSc4	cena
BAFTA	BAFTA	kA	BAFTA
<g/>
,	,	kIx,	,
drama	drama	k1gNnSc4	drama
Sydneyho	Sydney	k1gMnSc2	Sydney
Pollacka	Pollacko	k1gNnSc2	Pollacko
Bobby	Bobba	k1gMnSc2	Bobba
Deerfield	Deerfieldo	k1gNnPc2	Deerfieldo
<g/>
,	,	kIx,	,
a	a	k8xC	a
justiční	justiční	k2eAgInSc1d1	justiční
thriller	thriller	k1gInSc1	thriller
...	...	k?	...
A	a	k9	a
spravedlivost	spravedlivost	k1gFnSc4	spravedlivost
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
(	(	kIx(	(
<g/>
...	...	k?	...
And	Anda	k1gFnPc2	Anda
Justice	justice	k1gFnSc1	justice
for	forum	k1gNnPc2	forum
All	All	k1gFnSc2	All
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztělesnil	ztělesnit	k5eAaPmAgInS	ztělesnit
postavu	postava	k1gFnSc4	postava
Arthura	Arthur	k1gMnSc2	Arthur
Kirklanda	Kirklando	k1gNnSc2	Kirklando
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
musí	muset	k5eAaImIp3nS	muset
vybrat	vybrat	k5eAaPmF	vybrat
mezi	mezi	k7c7	mezi
kariérou	kariéra	k1gFnSc7	kariéra
nebo	nebo	k8xC	nebo
vlastním	vlastní	k2eAgNnSc7d1	vlastní
svědomím	svědomí	k1gNnSc7	svědomí
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
neproměnil	proměnit	k5eNaPmAgMnS	proměnit
už	už	k6eAd1	už
svou	svůj	k3xOyFgFnSc4	svůj
pátou	pátý	k4xOgFnSc4	pátý
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
Al	ala	k1gFnPc2	ala
Pacino	Pacino	k1gNnSc4	Pacino
natočil	natočit	k5eAaBmAgInS	natočit
krimi	krimi	k1gFnSc4	krimi
thriller	thriller	k1gInSc1	thriller
Na	na	k7c6	na
lovu	lov	k1gInSc6	lov
(	(	kIx(	(
<g/>
Cruising	Cruising	k1gInSc1	Cruising
<g/>
)	)	kIx)	)
a	a	k8xC	a
rodinné	rodinný	k2eAgNnSc1d1	rodinné
drama	drama	k1gNnSc1	drama
Autor	autor	k1gMnSc1	autor
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
Autor	autor	k1gMnSc1	autor
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Author	Authora	k1gFnPc2	Authora
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
Author	Author	k1gMnSc1	Author
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výraznou	výrazný	k2eAgFnSc4d1	výrazná
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
nezapomenutelnou	zapomenutelný	k2eNgFnSc4d1	nezapomenutelná
roli	role	k1gFnSc4	role
mu	on	k3xPp3gNnSc3	on
přinesla	přinést	k5eAaPmAgFnS	přinést
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
Brianem	Brian	k1gInSc7	Brian
de	de	k?	de
Palmou	palma	k1gFnSc7	palma
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgNnSc7	jenž
natočil	natočit	k5eAaBmAgMnS	natočit
výborné	výborný	k2eAgNnSc4d1	výborné
krimi	krimi	k1gNnSc4	krimi
drama	drama	k1gFnSc1	drama
Zjizvená	zjizvený	k2eAgFnSc1d1	zjizvená
tvář	tvář	k1gFnSc1	tvář
(	(	kIx(	(
<g/>
Scarface	Scarface	k1gFnSc1	Scarface
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Pacino	Pacino	k1gNnSc4	Pacino
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
postavu	postav	k1gInSc3	postav
arogantního	arogantní	k2eAgMnSc2d1	arogantní
drogového	drogový	k2eAgMnSc2d1	drogový
krále	král	k1gMnSc2	král
Tonyho	Tony	k1gMnSc2	Tony
Montanu	Montana	k1gFnSc4	Montana
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
roli	role	k1gFnSc4	role
získal	získat	k5eAaPmAgMnS	získat
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válečném	válečný	k2eAgInSc6d1	válečný
filmu	film	k1gInSc6	film
Revoluce	revoluce	k1gFnSc1	revoluce
(	(	kIx(	(
<g/>
Revolution	Revolution	k1gInSc1	Revolution
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nebyl	být	k5eNaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
nadšením	nadšení	k1gNnSc7	nadšení
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Al	ala	k1gFnPc2	ala
Pacino	Pacino	k1gNnSc4	Pacino
na	na	k7c4	na
pár	pár	k4xCyI	pár
let	léto	k1gNnPc2	léto
stáhl	stáhnout	k5eAaPmAgInS	stáhnout
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
a	a	k8xC	a
věnoval	věnovat	k5eAaImAgMnS	věnovat
se	se	k3xPyFc4	se
naplno	naplno	k6eAd1	naplno
své	svůj	k3xOyFgFnSc3	svůj
vášni	vášeň	k1gFnSc3	vášeň
<g/>
,	,	kIx,	,
divadlu	divadlo	k1gNnSc3	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Přijal	přijmout	k5eAaPmAgMnS	přijmout
role	role	k1gFnPc4	role
jak	jak	k8xC	jak
v	v	k7c6	v
inscenacích	inscenace	k1gFnPc6	inscenace
na	na	k7c6	na
Brodwayi	Brodway	k1gFnSc6	Brodway
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
na	na	k7c6	na
mimobroadwayských	mimobroadwayský	k2eAgFnPc6d1	mimobroadwayský
a	a	k8xC	a
londýnských	londýnský	k2eAgFnPc6d1	londýnská
scénách	scéna	k1gFnPc6	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
své	svůj	k3xOyFgFnPc4	svůj
postavy	postava	k1gFnPc4	postava
v	v	k7c6	v
divadelních	divadelní	k2eAgFnPc6d1	divadelní
hrách	hra	k1gFnPc6	hra
Americký	americký	k2eAgMnSc1d1	americký
bizon	bizon	k1gMnSc1	bizon
a	a	k8xC	a
Julius	Julius	k1gMnSc1	Julius
Caesar	Caesar	k1gMnSc1	Caesar
sklidil	sklidit	k5eAaPmAgMnS	sklidit
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
filmovým	filmový	k2eAgFnPc3d1	filmová
rolím	role	k1gFnPc3	role
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgInS	zahrát
detektiva	detektiv	k1gMnSc4	detektiv
Franka	Frank	k1gMnSc4	Frank
Kellera	Keller	k1gMnSc4	Keller
v	v	k7c6	v
krimi	krimi	k1gFnSc6	krimi
thrilleru	thriller	k1gInSc2	thriller
Moře	moře	k1gNnSc2	moře
lásky	láska	k1gFnSc2	láska
(	(	kIx(	(
<g/>
Sea	Sea	k1gFnSc1	Sea
of	of	k?	of
Love	lov	k1gInSc5	lov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterého	který	k3yRgMnSc4	který
získal	získat	k5eAaPmAgMnS	získat
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
dramatu	drama	k1gNnSc6	drama
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Al	ala	k1gFnPc2	ala
Pacino	Pacino	k1gNnSc4	Pacino
objevil	objevit	k5eAaPmAgInS	objevit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Dick	Dicka	k1gFnPc2	Dicka
Tracy	Traca	k1gFnSc2	Traca
<g/>
,	,	kIx,	,
za	za	k7c4	za
který	který	k3yRgInSc4	který
získal	získat	k5eAaPmAgMnS	získat
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
,	,	kIx,	,
a	a	k8xC	a
završil	završit	k5eAaPmAgInS	završit
trilogii	trilogie	k1gFnSc4	trilogie
o	o	k7c6	o
mafiánské	mafiánský	k2eAgFnSc6d1	mafiánská
rodině	rodina	k1gFnSc6	rodina
ve	v	k7c6	v
snímku	snímek	k1gInSc6	snímek
Kmotr	kmotr	k1gMnSc1	kmotr
III	III	kA	III
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Godfather	Godfathra	k1gFnPc2	Godfathra
III	III	kA	III
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgInS	zahrát
s	s	k7c7	s
Michelle	Michelle	k1gNnSc7	Michelle
Pfeifferovou	Pfeifferová	k1gFnSc4	Pfeifferová
v	v	k7c6	v
romantické	romantický	k2eAgFnSc6d1	romantická
komedii	komedie	k1gFnSc6	komedie
Frankie	Frankie	k1gFnSc2	Frankie
a	a	k8xC	a
Johnny	Johnna	k1gFnSc2	Johnna
<g/>
.	.	kIx.	.
</s>
<s>
Osudovým	osudový	k2eAgInSc7d1	osudový
rokem	rok	k1gInSc7	rok
pro	pro	k7c4	pro
Al	ala	k1gFnPc2	ala
Pacina	Pacina	k1gMnSc1	Pacina
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
rok	rok	k1gInSc4	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
po	po	k7c6	po
boku	bok	k1gInSc6	bok
hereckých	herecký	k2eAgNnPc2d1	herecké
es	es	k1gNnPc2	es
jako	jako	k8xS	jako
Jack	Jack	k1gMnSc1	Jack
Lemmon	Lemmon	k1gMnSc1	Lemmon
<g/>
,	,	kIx,	,
Ed	Ed	k1gMnSc1	Ed
Harris	Harris	k1gFnSc2	Harris
nebo	nebo	k8xC	nebo
Kevin	Kevin	k1gMnSc1	Kevin
Spacey	Spacea	k1gFnSc2	Spacea
v	v	k7c6	v
adaptaci	adaptace	k1gFnSc6	adaptace
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
o	o	k7c4	o
vypsání	vypsání	k1gNnSc4	vypsání
soutěže	soutěž	k1gFnSc2	soutěž
jednou	jednou	k6eAd1	jednou
realitní	realitní	k2eAgFnSc2d1	realitní
kanceláří	kancelář	k1gFnPc2	kancelář
Konkurenti	konkurent	k1gMnPc1	konkurent
(	(	kIx(	(
<g/>
Glengarry	Glengarr	k1gInPc1	Glengarr
Glen	Glena	k1gFnPc2	Glena
Ross	Rossa	k1gFnPc2	Rossa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Vůně	vůně	k1gFnSc2	vůně
ženy	žena	k1gFnSc2	žena
(	(	kIx(	(
<g/>
Scent	Scent	k1gInSc1	Scent
of	of	k?	of
a	a	k8xC	a
Woman	Woman	k1gMnSc1	Woman
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
postavu	postava	k1gFnSc4	postava
slepého	slepý	k2eAgMnSc2d1	slepý
podplukovníka	podplukovník	k1gMnSc2	podplukovník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
si	se	k3xPyFc3	se
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
znovu	znovu	k6eAd1	znovu
začíná	začínat	k5eAaImIp3nS	začínat
uvědomovat	uvědomovat	k5eAaImF	uvědomovat
krásu	krása	k1gFnSc4	krása
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Nominace	nominace	k1gFnSc1	nominace
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
i	i	k8xC	i
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
za	za	k7c4	za
film	film	k1gInSc4	film
Konkurenti	konkurent	k1gMnPc1	konkurent
sice	sice	k8xC	sice
neproměnil	proměnit	k5eNaPmAgMnS	proměnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
šesti	šest	k4xCc6	šest
nominacích	nominace	k1gFnPc6	nominace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vyšly	vyjít	k5eAaPmAgFnP	vyjít
naprázdno	naprázdno	k6eAd1	naprázdno
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
roli	role	k1gFnSc4	role
oceněn	ocenit	k5eAaPmNgInS	ocenit
Oscarem	Oscar	k1gInSc7	Oscar
i	i	k8xC	i
Zlatým	zlatý	k2eAgInSc7d1	zlatý
glóbem	glóbus	k1gInSc7	glóbus
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
Al	ala	k1gFnPc2	ala
Pacinovi	Pacin	k1gMnSc3	Pacin
podařilo	podařit	k5eAaPmAgNnS	podařit
natočit	natočit	k5eAaBmF	natočit
více	hodně	k6eAd2	hodně
oceňovaných	oceňovaný	k2eAgInPc2d1	oceňovaný
a	a	k8xC	a
kvalitních	kvalitní	k2eAgInPc2d1	kvalitní
snímků	snímek	k1gInPc2	snímek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
Carlitova	Carlitův	k2eAgFnSc1d1	Carlitova
cesta	cesta	k1gFnSc1	cesta
(	(	kIx(	(
<g/>
Carlito	Carlit	k2eAgNnSc1d1	Carlito
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Way	Way	k1gFnSc7	Way
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
sešel	sejít	k5eAaPmAgMnS	sejít
s	s	k7c7	s
Brianem	Brian	k1gInSc7	Brian
de	de	k?	de
Palmou	palma	k1gFnSc7	palma
a	a	k8xC	a
exceloval	excelovat	k5eAaImAgInS	excelovat
v	v	k7c6	v
roli	role	k1gFnSc6	role
portorikánského	portorikánský	k2eAgMnSc4d1	portorikánský
zločince	zločinec	k1gMnSc4	zločinec
Carlita	Carlit	k2eAgMnSc4d1	Carlit
Briganteho	Brigante	k1gMnSc4	Brigante
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
kritikou	kritika	k1gFnSc7	kritika
i	i	k8xC	i
diváky	divák	k1gMnPc7	divák
velmi	velmi	k6eAd1	velmi
pozitivně	pozitivně	k6eAd1	pozitivně
přijatý	přijatý	k2eAgInSc4d1	přijatý
film	film	k1gInSc4	film
bylo	být	k5eAaImAgNnS	být
krimidrama	krimidrama	k1gNnSc1	krimidrama
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
Nelítostný	lítostný	k2eNgInSc1d1	nelítostný
souboj	souboj	k1gInSc1	souboj
(	(	kIx(	(
<g/>
Heat	Heat	k1gInSc1	Heat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
opět	opět	k6eAd1	opět
zahrál	zahrát	k5eAaPmAgMnS	zahrát
s	s	k7c7	s
Robertem	Robert	k1gMnSc7	Robert
De	De	k?	De
Nirem	Nir	k1gMnSc7	Nir
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
natočil	natočit	k5eAaBmAgInS	natočit
další	další	k2eAgInPc4d1	další
výtečné	výtečný	k2eAgInPc4d1	výtečný
filmy	film	k1gInPc4	film
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
své	svůj	k3xOyFgFnPc4	svůj
nesporné	sporný	k2eNgFnPc4d1	nesporná
herecké	herecký	k2eAgFnPc4d1	herecká
kvality	kvalita	k1gFnPc4	kvalita
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
thriller	thriller	k1gInSc4	thriller
Ďáblův	ďáblův	k2eAgMnSc1d1	ďáblův
advokát	advokát	k1gMnSc1	advokát
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Devil	Devil	k1gMnSc1	Devil
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Advocate	Advocat	k1gMnSc5	Advocat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stvořil	stvořit	k5eAaPmAgMnS	stvořit
samotného	samotný	k2eAgMnSc4d1	samotný
Satana	Satan	k1gMnSc4	Satan
<g/>
,	,	kIx,	,
a	a	k8xC	a
krimidrama	krimidrama	k1gFnSc1	krimidrama
Krycí	krycí	k2eAgNnSc4d1	krycí
jméno	jméno	k1gNnSc4	jméno
Donnie	Donnie	k1gFnSc2	Donnie
Brasco	Brasco	k1gNnSc1	Brasco
(	(	kIx(	(
<g/>
Donnie	Donnie	k1gFnSc1	Donnie
Brasco	Brasco	k1gMnSc1	Brasco
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
skutečných	skutečný	k2eAgFnPc2d1	skutečná
událostí	událost	k1gFnPc2	událost
a	a	k8xC	a
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
příběh	příběh	k1gInSc4	příběh
o	o	k7c6	o
newyorské	newyorský	k2eAgFnSc6d1	newyorská
mafii	mafie	k1gFnSc6	mafie
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
uveden	uveden	k2eAgInSc1d1	uveden
snímek	snímek	k1gInSc1	snímek
Vítězové	vítěz	k1gMnPc1	vítěz
a	a	k8xC	a
poražení	poražený	k1gMnPc1	poražený
(	(	kIx(	(
<g/>
Any	Any	k1gMnPc1	Any
Given	Givna	k1gFnPc2	Givna
Sunday	Sundaa	k1gFnSc2	Sundaa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztělesnil	ztělesnit	k5eAaPmAgInS	ztělesnit
roli	role	k1gFnSc4	role
trenéra	trenér	k1gMnSc2	trenér
Tonyho	Tony	k1gMnSc2	Tony
Damata	Damat	k1gMnSc2	Damat
<g/>
,	,	kIx,	,
a	a	k8xC	a
thriller	thriller	k1gInSc1	thriller
Insider	insider	k1gMnSc1	insider
<g/>
:	:	kIx,	:
Muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
viděl	vidět	k5eAaImAgMnS	vidět
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k6eAd1	mnoho
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
nekalých	kalý	k2eNgFnPc6d1	nekalá
praktikách	praktika	k1gFnPc6	praktika
v	v	k7c6	v
tabákovém	tabákový	k2eAgInSc6d1	tabákový
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
tyto	tento	k3xDgInPc1	tento
filmy	film	k1gInPc1	film
se	se	k3xPyFc4	se
můžou	můžou	k?	můžou
pyšnit	pyšnit	k5eAaImF	pyšnit
nadprůměrným	nadprůměrný	k2eAgNnSc7d1	nadprůměrné
hodnocením	hodnocení	k1gNnSc7	hodnocení
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
diváků	divák	k1gMnPc2	divák
i	i	k8xC	i
kritiků	kritik	k1gMnPc2	kritik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
tisíciletí	tisíciletí	k1gNnSc6	tisíciletí
se	se	k3xPyFc4	se
Al	ala	k1gFnPc2	ala
Pacino	Pacino	k1gNnSc4	Pacino
blýskl	blýsknout	k5eAaPmAgInS	blýsknout
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Insomnie	insomnie	k1gFnSc2	insomnie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgInS	zahrát
veterána	veterán	k1gMnSc4	veterán
losangelské	losangelský	k2eAgFnSc2d1	losangelská
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
thrilleru	thriller	k1gInSc2	thriller
o	o	k7c4	o
CIA	CIA	kA	CIA
Test	test	k1gMnSc1	test
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Recruit	Recruit	k1gMnSc1	Recruit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
adaptaci	adaptace	k1gFnSc6	adaptace
klasické	klasický	k2eAgFnSc2d1	klasická
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
Williama	William	k1gMnSc4	William
Shakespeara	Shakespeare	k1gMnSc4	Shakespeare
<g/>
,	,	kIx,	,
Kupec	kupec	k1gMnSc1	kupec
benátský	benátský	k2eAgMnSc1d1	benátský
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Merchant	Merchant	k1gMnSc1	Merchant
of	of	k?	of
Venice	Venice	k1gFnSc2	Venice
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
dramatu	drama	k1gNnSc6	drama
Maximální	maximální	k2eAgInSc4d1	maximální
limit	limit	k1gInSc4	limit
(	(	kIx(	(
<g/>
Two	Two	k1gFnPc2	Two
for	forum	k1gNnPc2	forum
the	the	k?	the
Money	Monea	k1gFnSc2	Monea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztvárnil	ztvárnit	k5eAaPmAgInS	ztvárnit
postavu	postava	k1gFnSc4	postava
mocného	mocný	k2eAgMnSc2d1	mocný
šéfa	šéf	k1gMnSc2	šéf
obrovské	obrovský	k2eAgFnSc2d1	obrovská
národní	národní	k2eAgFnSc2d1	národní
korporace	korporace	k1gFnSc2	korporace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dává	dávat	k5eAaImIp3nS	dávat
tipy	tip	k1gInPc4	tip
na	na	k7c4	na
sportovní	sportovní	k2eAgFnPc4d1	sportovní
sázky	sázka	k1gFnPc4	sázka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
Al	ala	k1gFnPc2	ala
Pacino	Pacino	k1gNnSc4	Pacino
objevil	objevit	k5eAaPmAgInS	objevit
po	po	k7c6	po
boku	bok	k1gInSc6	bok
hvězd	hvězda	k1gFnPc2	hvězda
Meryl	Meryl	k1gInSc4	Meryl
Streepové	Streepová	k1gFnSc2	Streepová
a	a	k8xC	a
Emmy	Emma	k1gFnSc2	Emma
Thompsonové	Thompsonová	k1gFnSc2	Thompsonová
v	v	k7c6	v
pozoruhodné	pozoruhodný	k2eAgFnSc6d1	pozoruhodná
a	a	k8xC	a
strhující	strhující	k2eAgFnSc6d1	strhující
šestidílné	šestidílný	k2eAgFnSc6d1	šestidílná
televizní	televizní	k2eAgFnSc6d1	televizní
minisérii	minisérie	k1gFnSc6	minisérie
HBO	HBO	kA	HBO
Andělé	anděl	k1gMnPc1	anděl
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
(	(	kIx(	(
<g/>
Angels	Angels	k1gInSc1	Angels
in	in	k?	in
America	Americ	k1gInSc2	Americ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
oceněna	ocenit	k5eAaPmNgFnS	ocenit
pěti	pět	k4xCc7	pět
Zlatými	zlatý	k2eAgInPc7d1	zlatý
glóby	glóbus	k1gInPc7	glóbus
a	a	k8xC	a
jedenácti	jedenáct	k4xCc2	jedenáct
cenami	cena	k1gFnPc7	cena
Emmy	Emma	k1gFnSc2	Emma
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
je	být	k5eAaImIp3nS	být
natočen	natočit	k5eAaBmNgInS	natočit
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
hry	hra	k1gFnSc2	hra
oceněné	oceněný	k2eAgNnSc1d1	oceněné
Pulitzerovou	Pulitzerův	k2eAgFnSc7d1	Pulitzerova
cenou	cena	k1gFnSc7	cena
<g/>
,	,	kIx,	,
pohrává	pohrávat	k5eAaImIp3nS	pohrávat
si	se	k3xPyFc3	se
s	s	k7c7	s
realitou	realita	k1gFnSc7	realita
a	a	k8xC	a
řeší	řešit	k5eAaImIp3nP	řešit
témata	téma	k1gNnPc1	téma
jako	jako	k8xC	jako
homosexualita	homosexualita	k1gFnSc1	homosexualita
<g/>
,	,	kIx,	,
AIDS	aids	k1gInSc1	aids
a	a	k8xC	a
rakovina	rakovina	k1gFnSc1	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
filmovým	filmový	k2eAgFnPc3d1	filmová
hvězdám	hvězda	k1gFnPc3	hvězda
Georgi	Georgi	k1gNnSc2	Georgi
Clooneymu	Clooneym	k1gInSc2	Clooneym
<g/>
,	,	kIx,	,
Bradu	brada	k1gFnSc4	brada
Pittovi	Pitt	k1gMnSc3	Pitt
a	a	k8xC	a
Mattu	Matt	k1gMnSc3	Matt
Damonovi	Damon	k1gMnSc3	Damon
a	a	k8xC	a
zahrál	zahrát	k5eAaPmAgMnS	zahrát
si	se	k3xPyFc3	se
ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
pokračování	pokračování	k1gNnSc6	pokračování
filmu	film	k1gInSc2	film
Dannyho	Danny	k1gMnSc2	Danny
parťáci	parťák	k1gMnPc1	parťák
(	(	kIx(	(
<g/>
Ocean	Ocean	k1gMnSc1	Ocean
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Thirteen	Thirtena	k1gFnPc2	Thirtena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
jsme	být	k5eAaImIp1nP	být
mohli	moct	k5eAaImAgMnP	moct
Al	ala	k1gFnPc2	ala
Pacina	Pacin	k1gMnSc4	Pacin
vidět	vidět	k5eAaImF	vidět
v	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
krimi	krimi	k1gNnSc6	krimi
thrilleru	thriller	k1gInSc2	thriller
88	[number]	k4	88
minut	minuta	k1gFnPc2	minuta
(	(	kIx(	(
<g/>
88	[number]	k4	88
Minutes	Minutesa	k1gFnPc2	Minutesa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
vysokoškolského	vysokoškolský	k2eAgMnSc4d1	vysokoškolský
profesora	profesor	k1gMnSc4	profesor
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jacka	Jacek	k1gMnSc2	Jacek
Gramma	Gramm	k1gMnSc2	Gramm
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zároveň	zároveň	k6eAd1	zároveň
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
forenzní	forenzní	k2eAgMnSc1d1	forenzní
psychiatr	psychiatr	k1gMnSc1	psychiatr
FBI	FBI	kA	FBI
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
přišel	přijít	k5eAaPmAgInS	přijít
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
snímek	snímka	k1gFnPc2	snímka
Righteous	Righteous	k1gMnSc1	Righteous
Kill	Kill	k1gMnSc1	Kill
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
potřetí	potřetí	k4xO	potřetí
sešel	sejít	k5eAaPmAgMnS	sejít
s	s	k7c7	s
Robertem	Robert	k1gMnSc7	Robert
De	De	k?	De
Nirem	Nir	k1gMnSc7	Nir
<g/>
.	.	kIx.	.
</s>
<s>
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
Natálie	Natálie	k1gFnSc1	Natálie
/	/	kIx~	/
Me	Me	k1gFnSc1	Me
<g/>
,	,	kIx,	,
Natalie	Natalie	k1gFnSc1	Natalie
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Panika	panika	k1gFnSc1	panika
v	v	k7c6	v
Needle	Needle	k1gFnPc6	Needle
Parku	park	k1gInSc2	park
/	/	kIx~	/
Panic	panic	k1gMnSc1	panic
in	in	k?	in
Needle	Needle	k1gFnSc1	Needle
Park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
Kmotr	kmotr	k1gMnSc1	kmotr
/	/	kIx~	/
The	The	k1gFnSc1	The
Godfather	Godfathra	k1gFnPc2	Godfathra
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Strašák	strašák	k1gMnSc1	strašák
/	/	kIx~	/
Scarecrow	Scarecrow	k1gMnSc1	Scarecrow
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Serpico	Serpico	k6eAd1	Serpico
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
The	The	k1gFnSc1	The
Godfather	Godfathra	k1gFnPc2	Godfathra
<g/>
:	:	kIx,	:
Part	part	k1gInSc1	part
II	II	kA	II
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
Psí	psí	k2eAgNnSc1d1	psí
odpoledne	odpoledne	k1gNnSc1	odpoledne
/	/	kIx~	/
Dog	doga	k1gFnPc2	doga
Day	Day	k1gFnPc2	Day
Afternoon	Afternoon	k1gInSc1	Afternoon
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Bobby	Bobba	k1gFnSc2	Bobba
Deerfield	Deerfield	k1gInSc1	Deerfield
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
...	...	k?	...
<g/>
a	a	k8xC	a
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
/	/	kIx~	/
...	...	k?	...
<g/>
And	Anda	k1gFnPc2	Anda
Justice	justice	k1gFnSc1	justice
for	forum	k1gNnPc2	forum
All	All	k1gFnSc2	All
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
Cruising	Cruising	k1gInSc1	Cruising
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
Author	Authora	k1gFnPc2	Authora
<g/>
!	!	kIx.	!
</s>
<s>
Author	Author	k1gMnSc1	Author
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
Scarface	Scarface	k1gFnSc2	Scarface
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
Revolution	Revolution	k1gInSc1	Revolution
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Local	Local	k1gMnSc1	Local
Stigmatic	Stigmatice	k1gFnPc2	Stigmatice
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
režie	režie	k1gFnSc1	režie
a	a	k8xC	a
produkce	produkce	k1gFnSc1	produkce
Sea	Sea	k1gFnSc2	Sea
of	of	k?	of
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Dick	Dick	k1gInSc1	Dick
Tracy	Traca	k1gFnSc2	Traca
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Godfather	Godfathra	k1gFnPc2	Godfathra
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Part	part	k1gInSc1	part
III	III	kA	III
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Madonna	Madonna	k1gFnSc1	Madonna
<g/>
:	:	kIx,	:
Truth	Truth	k1gInSc1	Truth
or	or	k?	or
Dare	dar	k1gInSc5	dar
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Frankie	Frankie	k1gFnSc1	Frankie
and	and	k?	and
Johnny	Johnna	k1gFnSc2	Johnna
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Glengarry	Glengarra	k1gFnSc2	Glengarra
Glen	Glen	k1gMnSc1	Glen
Ross	Ross	k1gInSc1	Ross
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Vůně	vůně	k1gFnPc1	vůně
ženy	žena	k1gFnPc1	žena
/	/	kIx~	/
Scent	Scent	k1gInSc1	Scent
of	of	k?	of
a	a	k8xC	a
Woman	Woman	k1gMnSc1	Woman
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Carlito	Carlit	k2eAgNnSc1d1	Carlito
<g/>
'	'	kIx"	'
<g/>
<g />
.	.	kIx.	.
</s>
<s>
s	s	k7c7	s
Way	Way	k1gFnSc7	Way
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Jonas	Jonas	k1gInSc1	Jonas
in	in	k?	in
the	the	k?	the
Desert	desert	k1gInSc1	desert
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Two	Two	k1gFnSc1	Two
Bits	Bitsa	k1gFnPc2	Bitsa
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Heat	Heata	k1gFnPc2	Heata
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
City	city	k1gNnSc1	city
Hall	Halla	k1gFnPc2	Halla
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
Looking	Looking	k1gInSc1	Looking
for	forum	k1gNnPc2	forum
Richard	Richarda	k1gFnPc2	Richarda
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
režie	režie	k1gFnSc1	režie
a	a	k8xC	a
produkce	produkce	k1gFnSc1	produkce
<g />
.	.	kIx.	.
</s>
<s>
Pitch	Pitch	k1gInSc1	Pitch
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Krycí	krycí	k2eAgNnSc1d1	krycí
jméno	jméno	k1gNnSc1	jméno
Donnie	Donnie	k1gFnSc2	Donnie
Brasco	Brasco	k1gNnSc1	Brasco
/	/	kIx~	/
Donnie	Donnie	k1gFnSc1	Donnie
Brasco	Brasco	k1gMnSc1	Brasco
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Devil	Devil	k1gMnSc1	Devil
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Advocate	Advocat	k1gInSc5	Advocat
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Insider	insider	k1gMnSc1	insider
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Any	Any	k1gFnSc6	Any
Given	Givna	k1gFnPc2	Givna
Sunday	Sundaa	k1gFnSc2	Sundaa
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Chinese	Chinese	k1gFnSc1	Chinese
Coffee	Coffee	k1gFnSc1	Coffee
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
režie	režie	k1gFnSc1	režie
Insomnia	Insomnium	k1gNnSc2	Insomnium
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
S	s	k7c7	s
<g/>
1	[number]	k4	1
<g/>
m	m	kA	m
<g/>
0	[number]	k4	0
<g/>
ne	ne	k9	ne
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
People	People	k1gFnSc6	People
I	i	k8xC	i
Know	Know	k1gFnSc6	Know
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Recruit	Recruit	k1gMnSc1	Recruit
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Gigli	Gigle	k1gFnSc6	Gigle
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Merchant	Merchant	k1gMnSc1	Merchant
of	of	k?	of
<g />
.	.	kIx.	.
</s>
<s>
Venice	Venice	k1gFnSc1	Venice
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Two	Two	k1gFnPc2	Two
for	forum	k1gNnPc2	forum
the	the	k?	the
Money	Monea	k1gFnSc2	Monea
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
88	[number]	k4	88
Minutes	Minutesa	k1gFnPc2	Minutesa
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Dannyho	Danny	k1gMnSc2	Danny
Parťáci	parťák	k1gMnPc1	parťák
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
Ocean	Ocean	k1gInSc1	Ocean
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Thirteen	Thirtena	k1gFnPc2	Thirtena
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Jack	Jack	k1gMnSc1	Jack
and	and	k?	and
Jill	Jill	k1gMnSc1	Jill
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Oscar	Oscar	k1gInSc1	Oscar
1972	[number]	k4	1972
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
Nominace	nominace	k1gFnSc1	nominace
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
za	za	k7c4	za
film	film	k1gInSc4	film
Kmotr	kmotr	k1gMnSc1	kmotr
1973	[number]	k4	1973
–	–	k?	–
Nominace	nominace	k1gFnSc2	nominace
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
za	za	k7c4	za
film	film	k1gInSc4	film
Serpico	Serpico	k6eAd1	Serpico
1974	[number]	k4	1974
–	–	k?	–
Nominace	nominace	k1gFnSc2	nominace
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
za	za	k7c4	za
film	film	k1gInSc4	film
Kmotr	kmotr	k1gMnSc1	kmotr
II	II	kA	II
1975	[number]	k4	1975
–	–	k?	–
Nominace	nominace	k1gFnSc2	nominace
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
za	za	k7c4	za
film	film	k1gInSc4	film
Psí	psí	k2eAgNnSc1d1	psí
odpoledne	odpoledne	k1gNnSc1	odpoledne
1979	[number]	k4	1979
–	–	k?	–
Nominace	nominace	k1gFnSc2	nominace
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
za	za	k7c4	za
film	film	k1gInSc4	film
...	...	k?	...
A	a	k9	a
spravedlivost	spravedlivost	k1gFnSc1	spravedlivost
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
1990	[number]	k4	1990
–	–	k?	–
Nominace	nominace	k1gFnSc2	nominace
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
za	za	k7c4	za
film	film	k1gInSc4	film
Dick	Dick	k1gMnSc1	Dick
Tracy	Traca	k1gFnSc2	Traca
1992	[number]	k4	1992
–	–	k?	–
Nominace	nominace	k1gFnSc2	nominace
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
<g />
.	.	kIx.	.
</s>
<s>
za	za	k7c4	za
film	film	k1gInSc4	film
Konkurenti	konkurent	k1gMnPc1	konkurent
1992	[number]	k4	1992
–	–	k?	–
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
za	za	k7c4	za
film	film	k1gInSc4	film
Vůně	vůně	k1gFnSc2	vůně
ženy	žena	k1gFnSc2	žena
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
1973	[number]	k4	1973
–	–	k?	–
Nominace	nominace	k1gFnSc2	nominace
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
dramatu	drama	k1gNnSc6	drama
za	za	k7c4	za
film	film	k1gInSc4	film
Kmotr	kmotr	k1gMnSc1	kmotr
1974	[number]	k4	1974
–	–	k?	–
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
dramatu	drama	k1gNnSc6	drama
za	za	k7c4	za
film	film	k1gInSc4	film
Serpico	Serpico	k6eAd1	Serpico
1975	[number]	k4	1975
–	–	k?	–
Nominace	nominace	k1gFnSc2	nominace
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
dramatu	drama	k1gNnSc6	drama
za	za	k7c4	za
film	film	k1gInSc4	film
Kmotr	kmotr	k1gMnSc1	kmotr
II	II	kA	II
1976	[number]	k4	1976
–	–	k?	–
Nominace	nominace	k1gFnSc2	nominace
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
dramatu	drama	k1gNnSc6	drama
za	za	k7c4	za
film	film	k1gInSc4	film
Psí	psí	k2eAgNnSc1d1	psí
odpoledne	odpoledne	k1gNnSc1	odpoledne
1978	[number]	k4	1978
–	–	k?	–
Nominace	nominace	k1gFnSc2	nominace
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
dramatu	drama	k1gNnSc6	drama
za	za	k7c4	za
film	film	k1gInSc4	film
Bobby	Bobba	k1gFnSc2	Bobba
Deerfield	Deerfield	k1gInSc1	Deerfield
1980	[number]	k4	1980
–	–	k?	–
Nominace	nominace	k1gFnSc2	nominace
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
dramatu	drama	k1gNnSc6	drama
za	za	k7c4	za
film	film	k1gInSc4	film
...	...	k?	...
A	a	k9	a
spravedlivost	spravedlivost	k1gFnSc1	spravedlivost
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
1983	[number]	k4	1983
–	–	k?	–
Nominace	nominace	k1gFnSc2	nominace
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
komedii	komedie	k1gFnSc6	komedie
nebo	nebo	k8xC	nebo
muzikálu	muzikál	k1gInSc6	muzikál
za	za	k7c4	za
film	film	k1gInSc4	film
Autor	autor	k1gMnSc1	autor
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
Autor	autor	k1gMnSc1	autor
<g/>
!	!	kIx.	!
</s>
<s>
1984	[number]	k4	1984
–	–	k?	–
Nominace	nominace	k1gFnSc2	nominace
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
dramatu	drama	k1gNnSc6	drama
za	za	k7c4	za
film	film	k1gInSc4	film
Zjizvená	zjizvený	k2eAgFnSc1d1	zjizvená
tvář	tvář	k1gFnSc1	tvář
1990	[number]	k4	1990
–	–	k?	–
Nominace	nominace	k1gFnSc2	nominace
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
dramatu	drama	k1gNnSc6	drama
za	za	k7c4	za
film	film	k1gInSc4	film
Moře	moře	k1gNnSc2	moře
lásky	láska	k1gFnSc2	láska
1991	[number]	k4	1991
–	–	k?	–
Nominace	nominace	k1gFnSc2	nominace
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
za	za	k7c4	za
film	film	k1gInSc4	film
Dick	Dick	k1gMnSc1	Dick
Tracy	Traca	k1gFnSc2	Traca
1991	[number]	k4	1991
–	–	k?	–
Nominace	nominace	k1gFnSc2	nominace
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
<g />
.	.	kIx.	.
</s>
<s>
výkon	výkon	k1gInSc1	výkon
v	v	k7c6	v
dramatu	drama	k1gNnSc6	drama
za	za	k7c4	za
film	film	k1gInSc4	film
Kmotr	kmotr	k1gMnSc1	kmotr
III	III	kA	III
1993	[number]	k4	1993
–	–	k?	–
Nominace	nominace	k1gFnSc2	nominace
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
ve	v	k7c6	v
vedlejší	vedlejší	k2eAgFnSc6d1	vedlejší
roli	role	k1gFnSc6	role
za	za	k7c4	za
film	film	k1gInSc4	film
Konkurenti	konkurent	k1gMnPc1	konkurent
1993	[number]	k4	1993
–	–	k?	–
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
dramatu	drama	k1gNnSc6	drama
za	za	k7c4	za
film	film	k1gInSc4	film
Vůně	vůně	k1gFnSc2	vůně
ženy	žena	k1gFnSc2	žena
2001	[number]	k4	2001
–	–	k?	–
Cecil	Cecil	k1gMnSc1	Cecil
B.	B.	kA	B.
DeMille	DeMille	k1gFnSc1	DeMille
Award	Award	k1gInSc1	Award
2004	[number]	k4	2004
–	–	k?	–
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
mužský	mužský	k2eAgInSc4d1	mužský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
mini	mini	k2eAgFnSc6d1	mini
sérii	série	k1gFnSc6	série
nebo	nebo	k8xC	nebo
TV	TV	kA	TV
seriálu	seriál	k1gInSc2	seriál
za	za	k7c4	za
seriál	seriál	k1gInSc4	seriál
Andělé	anděl	k1gMnPc1	anděl
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
Emmy	Emma	k1gFnSc2	Emma
2004	[number]	k4	2004
–	–	k?	–
Vynikající	vynikající	k2eAgFnSc2d1	vynikající
hlavní	hlavní	k2eAgFnSc2d1	hlavní
role	role	k1gFnSc2	role
v	v	k7c6	v
mini	mini	k2eAgFnSc6d1	mini
sérii	série	k1gFnSc6	série
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
filmu	film	k1gInSc6	film
za	za	k7c4	za
seriál	seriál	k1gInSc4	seriál
Andělé	anděl	k1gMnPc1	anděl
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Al	ala	k1gFnPc2	ala
Pacino	Pacino	k1gNnSc4	Pacino
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopédia	Encyklopédium	k1gNnPc1	Encyklopédium
filmu	film	k1gInSc2	film
<g/>
,	,	kIx,	,
Obzor	obzor	k1gInSc1	obzor
1993	[number]	k4	1993
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Al	ala	k1gFnPc2	ala
Pacino	Pacino	k1gNnSc4	Pacino
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Al	ala	k1gFnPc2	ala
Pacino	Pacino	k1gNnSc4	Pacino
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Al	ala	k1gFnPc2	ala
Pacino	Pacino	k1gNnSc4	Pacino
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Al	ala	k1gFnPc2	ala
Pacino	Pacino	k1gNnSc4	Pacino
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Stránky	stránka	k1gFnPc1	stránka
fanoušků	fanoušek	k1gMnPc2	fanoušek
</s>
