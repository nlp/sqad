<s>
Vikariát	vikariát	k1gInSc4
S.	S.	kA
Marco	Marco	k1gNnSc1
-	-	kIx~
Castello	Castello	k1gNnSc1
</s>
<s>
Vikariát	vikariát	k1gInSc1
S.	S.	kA
Marco	Marco	k6eAd1
-	-	kIx~
CastelloDiecéze	CastelloDiecéza	k1gFnSc3
</s>
<s>
Patriarchát	patriarchát	k1gInSc1
benátský	benátský	k2eAgInSc1d1
Provincie	provincie	k1gFnSc2
</s>
<s>
Triveneto	Triveneto	k1gNnSc1
Okrskový	okrskový	k2eAgMnSc1d1
vikář	vikář	k1gMnSc1
</s>
<s>
Mons	Mons	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Narciso	Narcisa	k1gFnSc5
Belfiore	Belfior	k1gInSc5
<g/>
,	,	kIx,
S.	S.	kA
<g/>
D.B.	D.B.	k1gFnSc4
Další	další	k2eAgInSc1d1
úřad	úřad	k1gInSc1
vikáře	vikář	k1gMnSc2
</s>
<s>
Farář	farář	k1gMnSc1
ve	v	k7c6
farnostech	farnost	k1gFnPc6
</s>
<s>
Vikariát	vikariát	k1gInSc4
S.	S.	kA
Marco	Marco	k1gNnSc1
-	-	kIx~
Castello	Castello	k1gNnSc1
je	být	k5eAaImIp3nS
územní	územní	k2eAgFnSc1d1
část	část	k1gFnSc1
benátského	benátský	k2eAgInSc2d1
patriarchátu	patriarchát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvoří	tvořit	k5eAaImIp3nS
ji	on	k3xPp3gFnSc4
14	#num#	k4
římskokatolických	římskokatolický	k2eAgFnPc2d1
farností	farnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Farnosti	farnost	k1gFnPc1
vikariátu	vikariát	k1gInSc2
</s>
<s>
Označení	označení	k1gNnSc1
položek	položka	k1gFnPc2
</s>
<s>
FarnostSprávceDalší	FarnostSprávceDalší	k2eAgMnSc1d1
duchovní	duchovní	k1gMnSc1
ve	v	k7c4
farnostiFarní	farnostiFarní	k2eAgInSc4d1
kostel	kostel	k1gInSc4
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Sv.	sv.	kA
Petra	Petra	k1gFnSc1
v	v	k7c4
Castello	Castello	k1gNnSc4
(	(	kIx(
<g/>
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
<g/>
Mons	Mons	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Narciso	Narcisa	k1gFnSc5
Belfiore	Belfior	k1gInSc5
<g/>
,	,	kIx,
S.	S.	kA
<g/>
D.B.	D.B.	k1gFnSc2
<g/>
farářAlessandro	farářAlessandro	k6eAd1
Zaramella	Zaramello	k1gNnSc2
<g/>
,	,	kIx,
S.	S.	kA
<g/>
D.B.	D.B.	k1gFnSc4
<g/>
farní	farní	k2eAgFnSc2d1
vikářSvatého	vikářSvatý	k2eAgMnSc2d1
Petra	Petr	k1gMnSc2
apoštola	apoštol	k1gMnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Sv.	sv.	kA
Josefa	Josefa	k1gFnSc1
v	v	k7c4
Castello	Castello	k1gNnSc4
(	(	kIx(
<g/>
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
<g/>
Mons	Mons	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Narciso	Narcisa	k1gFnSc5
Belfiore	Belfior	k1gInSc5
<g/>
,	,	kIx,
S.	S.	kA
<g/>
D.B.	D.B.	k1gFnSc2
<g/>
farářMassimo	farářMassimo	k6eAd1
Schibotto	Schibotto	k1gNnSc1
<g/>
,	,	kIx,
S.	S.	kA
<g/>
D.B.	D.B.	k1gMnPc1
<g/>
farní	farní	k2eAgMnPc1d1
vikářSvatého	vikářSvatý	k2eAgInSc2d1
Josefa	Josef	k1gMnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Sv.	sv.	kA
Františka	Františka	k1gFnSc1
z	z	k7c2
Pauly	Paula	k1gFnSc2
(	(	kIx(
<g/>
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
<g/>
Mons	Mons	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Narciso	Narcisa	k1gFnSc5
Belfiore	Belfior	k1gInSc5
<g/>
,	,	kIx,
S.	S.	kA
<g/>
D.B.	D.B.	k1gFnSc1
<g/>
farářFilippo	farářFilippa	k1gFnSc5
Chiaffoni	Chiaffoň	k1gFnSc3
<g/>
,	,	kIx,
S.	S.	kA
<g/>
D.B.	D.B.	k1gFnSc4
<g/>
farní	farní	k2eAgFnSc2d1
vikářSvatého	vikářSvatý	k2eAgMnSc4d1
Františka	František	k1gMnSc4
z	z	k7c2
Pauly	Paula	k1gFnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Sv.	sv.	kA
Martina	Martina	k1gFnSc1
v	v	k7c4
Castello	Castello	k1gNnSc4
(	(	kIx(
<g/>
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
<g/>
Augusto	Augusta	k1gMnSc5
ManentefarářSvatého	ManentefarářSvatý	k2eAgMnSc2d1
Martina	Martin	k1gMnSc2
biskupa	biskup	k1gMnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Sv.	sv.	kA
Františka	František	k1gMnSc2
della	dell	k1gMnSc2
Vigna	Vign	k1gMnSc2
(	(	kIx(
<g/>
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
<g/>
Adriano	Adriana	k1gFnSc5
Campesato	Campesat	k2eAgNnSc1d1
<g/>
,	,	kIx,
O.	O.	kA
<g/>
F.	F.	kA
<g/>
M.	M.	kA
<g/>
farářSebastiano	farářSebastiana	k1gFnSc5
Simonitto	Simonitto	k1gNnSc1
<g/>
,	,	kIx,
O.	O.	kA
<g/>
F.	F.	kA
<g/>
M.	M.	kA
<g/>
farní	farní	k2eAgInSc4d1
vikářSvatého	vikářSvatý	k2eAgMnSc4d1
Františka	František	k1gMnSc4
z	z	k7c2
Assisi	Assis	k1gMnSc6
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Bragora	Bragora	k1gFnSc1
(	(	kIx(
<g/>
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
<g/>
Giovanni	Giovann	k1gMnPc1
FavarettofarářSvatého	FavarettofarářSvatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Křtitele	křtitel	k1gMnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Sv.	sv.	kA
Štěpána	Štěpána	k1gFnSc1
(	(	kIx(
<g/>
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
<g/>
Mons	Mons	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gianni	Gianeň	k1gFnSc6
BernardifarářSvatého	BernardifarářSvatý	k2eAgMnSc2d1
Štěpána	Štěpán	k1gMnSc2
prvomučedníka	prvomučedník	k1gMnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Sv.	sv.	kA
Lukáše	Lukáš	k1gMnSc2
(	(	kIx(
<g/>
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
<g/>
Mons	Mons	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marino	Marina	k1gFnSc5
Gallinafarní	Gallinafarný	k2eAgMnPc1d1
administrátorSvatého	administrátorSvatý	k2eAgMnSc2d1
Lukáše	Lukáš	k1gMnSc2
evangelisty	evangelista	k1gMnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Sv.	sv.	kA
Salvátora	Salvátor	k1gMnSc2
(	(	kIx(
<g/>
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
<g/>
Nejsvětějšího	nejsvětější	k2eAgMnSc4d1
Salvátora	Salvátor	k1gMnSc4
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Sv.	sv.	kA
Zachariáše	Zachariáš	k1gMnSc2
(	(	kIx(
<g/>
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
<g/>
Mons	Mons	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Carlo	Carlo	k1gNnSc1
Senofarní	Senofarní	k2eAgFnSc2d1
administrátorSvatých	administrátorSvatá	k1gFnPc6
Zachariáše	Zachariáš	k1gMnSc2
a	a	k8xC
Atanáše	Atanáš	k1gMnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Santa	Santa	k1gFnSc1
Maria	Maria	k1gFnSc1
Formosa	Formosa	k1gFnSc1
(	(	kIx(
<g/>
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
<g/>
Piotr	Piotr	k1gMnSc1
Mikulskifarní	Mikulskifarní	k2eAgNnSc1d1
administrátorOčišťování	administrátorOčišťování	k1gNnSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Sv.	sv.	kA
Jana	Jana	k1gFnSc1
a	a	k8xC
Pavla	Pavla	k1gFnSc1
(	(	kIx(
<g/>
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
<g/>
Angelo	Angela	k1gFnSc5
Preda	Preda	k1gMnSc1
<g/>
,	,	kIx,
O.	O.	kA
<g/>
P.	P.	kA
<g/>
farářSvatých	farářSvatá	k1gFnPc2
Jana	Jana	k1gFnSc1
a	a	k8xC
Pavla	Pavla	k1gFnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Sv.	sv.	kA
Heleny	Helena	k1gFnSc2
(	(	kIx(
<g/>
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
<g/>
Carlo	Carlo	k1gNnSc1
M.	M.	kA
Serpelloni	Serpelloň	k1gFnSc6
<g/>
,	,	kIx,
O.S.	O.S.	k1gFnSc6
<g/>
M.	M.	kA
<g/>
farářSvaté	farářSvatý	k2eAgFnSc2d1
Heleny	Helena	k1gFnSc2
císařovny	císařovna	k1gFnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Sv.	sv.	kA
Mojžíše	Mojžíš	k1gMnSc2
(	(	kIx(
<g/>
Benátky	Benátky	k1gFnPc1
<g/>
)	)	kIx)
<g/>
Mons	Mons	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Giuseppe	Giusepp	k1gInSc5
Camilottofarní	Camilottofarný	k2eAgMnPc1d1
administrátorRoberto	administrátorRoberta	k1gFnSc5
Donadonifarní	Donadonifarný	k2eAgMnPc1d1
vikářSvatého	vikářSvatý	k2eAgInSc2d1
Mojžíše	Mojžíš	k1gMnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Vikariát	vikariát	k1gInSc4
S.	S.	kA
Marco	Marco	k1gNnSc1
-	-	kIx~
Castello	Castello	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
</s>
