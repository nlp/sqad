<p>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
volejbale	volejbal	k1gInSc6	volejbal
mužů	muž	k1gMnPc2	muž
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
ve	v	k7c6	v
dnech	den	k1gInPc6	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
v	v	k7c6	v
NDR	NDR	kA	NDR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Turnaje	turnaj	k1gInSc2	turnaj
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
12	[number]	k4	12
týmů	tým	k1gInPc2	tým
<g/>
,	,	kIx,	,
rozdělených	rozdělený	k2eAgInPc2d1	rozdělený
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
čtyřčlenných	čtyřčlenný	k2eAgFnPc2d1	čtyřčlenná
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnPc4	první
dvě	dva	k4xCgNnPc4	dva
družstva	družstvo	k1gNnPc4	družstvo
postoupila	postoupit	k5eAaPmAgFnS	postoupit
do	do	k7c2	do
finálové	finálový	k2eAgFnSc2d1	finálová
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
týmy	tým	k1gInPc1	tým
na	na	k7c6	na
třetím	třetí	k4xOgMnSc6	třetí
a	a	k8xC	a
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
místě	místo	k1gNnSc6	místo
hráli	hrát	k5eAaImAgMnP	hrát
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
o	o	k7c4	o
sedmé	sedmý	k4xOgNnSc4	sedmý
až	až	k8xS	až
dvanácté	dvanáctý	k4xOgNnSc4	dvanáctý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Mistrem	mistr	k1gMnSc7	mistr
Evropy	Evropa	k1gFnSc2	Evropa
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
volejbalisté	volejbalista	k1gMnPc1	volejbalista
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kvalifikace	kvalifikace	k1gFnSc1	kvalifikace
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledky	výsledek	k1gInPc1	výsledek
a	a	k8xC	a
tabulky	tabulka	k1gFnPc1	tabulka
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
A	a	k9	a
(	(	kIx(	(
<g/>
Erfurt	Erfurt	k1gInSc1	Erfurt
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
B	B	kA	B
(	(	kIx(	(
<g/>
Suhl	Suhl	k1gMnSc1	Suhl
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
K	K	kA	K
<g/>
=	=	kIx~	=
Zápas	zápas	k1gInSc1	zápas
Polsko	Polsko	k1gNnSc1	Polsko
-	-	kIx~	-
Francie	Francie	k1gFnSc2	Francie
skončil	skončit	k5eAaPmAgInS	skončit
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
13	[number]	k4	13
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
zjištěný	zjištěný	k2eAgInSc4d1	zjištěný
doping	doping	k1gInSc4	doping
francouzského	francouzský	k2eAgMnSc2d1	francouzský
hráče	hráč	k1gMnSc2	hráč
Clevenauxe	Clevenauxe	k1gFnSc2	Clevenauxe
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
výsledek	výsledek	k1gInSc1	výsledek
zápasu	zápas	k1gInSc2	zápas
kontumován	kontumovat	k5eAaImNgInS	kontumovat
na	na	k7c4	na
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
C	C	kA	C
(	(	kIx(	(
<g/>
Berlín	Berlín	k1gInSc1	Berlín
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Finálová	finálový	k2eAgFnSc1d1	finálová
skupina	skupina	k1gFnSc1	skupina
(	(	kIx(	(
<g/>
Berlín	Berlín	k1gInSc1	Berlín
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Skupina	skupina	k1gFnSc1	skupina
o	o	k7c4	o
7	[number]	k4	7
<g/>
.	.	kIx.	.
-	-	kIx~	-
12	[number]	k4	12
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
(	(	kIx(	(
<g/>
Suhl	Suhl	k1gInSc1	Suhl
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Konečné	Konečné	k2eAgNnSc1d1	Konečné
pořadí	pořadí	k1gNnSc1	pořadí
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
CEV	CEV	kA	CEV
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Sportovní	sportovní	k2eAgFnPc1d1	sportovní
statistiky	statistika	k1gFnPc1	statistika
na	na	k7c4	na
todor	todor	k1gInSc4	todor
<g/>
66	[number]	k4	66
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Rudé	rudý	k2eAgNnSc1d1	Rudé
právo	právo	k1gNnSc1	právo
</s>
</p>
