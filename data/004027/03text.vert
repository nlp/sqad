<s>
Čas	čas	k1gInSc1	čas
a	a	k8xC	a
doba	doba	k1gFnSc1	doba
jsou	být	k5eAaImIp3nP	být
jedny	jeden	k4xCgFnPc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
veličin	veličina	k1gFnPc2	veličina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
bývá	bývat	k5eAaImIp3nS	bývat
užitečné	užitečný	k2eAgNnSc1d1	užitečné
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
čas	čas	k1gInSc1	čas
(	(	kIx(	(
<g/>
čas	čas	k1gInSc1	čas
jedné	jeden	k4xCgFnSc2	jeden
události	událost	k1gFnSc2	událost
<g/>
)	)	kIx)	)
určuje	určovat	k5eAaImIp3nS	určovat
okamžik	okamžik	k1gInSc4	okamžik
události	událost	k1gFnSc2	událost
na	na	k7c6	na
časové	časový	k2eAgFnSc6d1	časová
ose	osa	k1gFnSc6	osa
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
první	první	k4xOgFnSc3	první
souřadnici	souřadnice	k1gFnSc3	souřadnice
časoprostoru	časoprostor	k1gInSc2	časoprostor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
doba	doba	k1gFnSc1	doba
určuje	určovat	k5eAaImIp3nS	určovat
časovou	časový	k2eAgFnSc4d1	časová
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
událostmi	událost	k1gFnPc7	událost
<g/>
,	,	kIx,	,
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
časy	čas	k1gInPc7	čas
dvou	dva	k4xCgNnPc2	dva
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozlišení	rozlišení	k1gNnSc1	rozlišení
není	být	k5eNaImIp3nS	být
absolutní	absolutní	k2eAgNnSc1d1	absolutní
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
jako	jako	k8xC	jako
souřadnice	souřadnice	k1gFnSc1	souřadnice
je	být	k5eAaImIp3nS	být
také	také	k9	také
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
dvou	dva	k4xCgInPc2	dva
okamžiků	okamžik	k1gInPc2	okamžik
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
uvedeného	uvedený	k2eAgInSc2d1	uvedený
<g/>
,	,	kIx,	,
zkoumaného	zkoumaný	k2eAgInSc2d1	zkoumaný
<g/>
,	,	kIx,	,
určovaného	určovaný	k2eAgInSc2d1	určovaný
okamžiku	okamžik	k1gInSc2	okamžik
a	a	k8xC	a
nulového	nulový	k2eAgInSc2d1	nulový
okamžiku	okamžik	k1gInSc2	okamžik
počátku	počátek	k1gInSc2	počátek
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
zamlčeného	zamlčený	k2eAgNnSc2d1	zamlčené
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
řeči	řeč	k1gFnSc6	řeč
jsou	být	k5eAaImIp3nP	být
tato	tento	k3xDgNnPc4	tento
dvě	dva	k4xCgNnPc4	dva
slova	slovo	k1gNnPc4	slovo
plně	plně	k6eAd1	plně
synonyma	synonymum	k1gNnSc2	synonymum
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
i	i	k8xC	i
doba	doba	k1gFnSc1	doba
mají	mít	k5eAaImIp3nP	mít
zásadní	zásadní	k2eAgInSc4d1	zásadní
význam	význam	k1gInSc4	význam
pro	pro	k7c4	pro
lidský	lidský	k2eAgInSc4d1	lidský
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
povahy	povaha	k1gFnSc2	povaha
věci	věc	k1gFnSc2	věc
časově	časově	k6eAd1	časově
omezený	omezený	k2eAgInSc1d1	omezený
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
nemám	mít	k5eNaImIp1nS	mít
čas	čas	k1gInSc1	čas
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
doba	doba	k1gFnSc1	doba
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
organizaci	organizace	k1gFnSc4	organizace
lidské	lidský	k2eAgFnSc2d1	lidská
společnosti	společnost	k1gFnSc2	společnost
včetně	včetně	k7c2	včetně
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
byl	být	k5eAaImAgInS	být
vždy	vždy	k6eAd1	vždy
důležité	důležitý	k2eAgNnSc4d1	důležité
téma	téma	k1gNnSc4	téma
pro	pro	k7c4	pro
spisovatele	spisovatel	k1gMnPc4	spisovatel
<g/>
,	,	kIx,	,
umělce	umělec	k1gMnPc4	umělec
a	a	k8xC	a
filosofy	filosof	k1gMnPc4	filosof
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
také	také	k9	také
definovat	definovat	k5eAaBmF	definovat
jako	jako	k9	jako
neprostorové	prostorový	k2eNgNnSc4d1	neprostorové
lineární	lineární	k2eAgNnSc4d1	lineární
kontinuum	kontinuum	k1gNnSc4	kontinuum
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
události	událost	k1gFnPc1	událost
stávají	stávat	k5eAaImIp3nP	stávat
ve	v	k7c6	v
zjevně	zjevně	k6eAd1	zjevně
nevratném	vratný	k2eNgNnSc6d1	nevratné
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
takový	takový	k3xDgInSc1	takový
je	být	k5eAaImIp3nS	být
podstatnou	podstatný	k2eAgFnSc7d1	podstatná
složkou	složka	k1gFnSc7	složka
struktury	struktura	k1gFnSc2	struktura
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
,	,	kIx,	,
až	až	k8xS	až
nemožné	možný	k2eNgNnSc1d1	nemožné
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
čas	čas	k1gInSc4	čas
nějak	nějak	k6eAd1	nějak
představit	představit	k5eAaPmF	představit
<g/>
.	.	kIx.	.
</s>
<s>
Pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
pochopení	pochopení	k1gNnSc4	pochopení
času	čas	k1gInSc2	čas
byly	být	k5eAaImAgInP	být
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
především	především	k6eAd1	především
doménou	doména	k1gFnSc7	doména
filosofů	filosof	k1gMnPc2	filosof
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
i	i	k9	i
přírodovědců	přírodovědec	k1gMnPc2	přírodovědec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
povahu	povaha	k1gFnSc4	povaha
a	a	k8xC	a
smysl	smysl	k1gInSc4	smysl
času	čas	k1gInSc2	čas
existuje	existovat	k5eAaImIp3nS	existovat
množství	množství	k1gNnSc4	množství
silně	silně	k6eAd1	silně
odlišných	odlišný	k2eAgInPc2d1	odlišný
náhledů	náhled	k1gInPc2	náhled
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
obtížné	obtížný	k2eAgNnSc1d1	obtížné
nabídnout	nabídnout	k5eAaPmF	nabídnout
jeho	jeho	k3xOp3gFnSc4	jeho
nekontroverzní	kontroverzní	k2eNgFnSc4d1	nekontroverzní
a	a	k8xC	a
jasnou	jasný	k2eAgFnSc4d1	jasná
definici	definice	k1gFnSc4	definice
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
pojmem	pojem	k1gInSc7	pojem
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
šipka	šipka	k1gFnSc1	šipka
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
smysl	smysl	k1gInSc4	smysl
(	(	kIx(	(
<g/>
směr	směr	k1gInSc1	směr
<g/>
)	)	kIx)	)
plynutí	plynutí	k1gNnSc1	plynutí
času	čas	k1gInSc2	čas
a	a	k8xC	a
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
směru	směr	k1gInSc2	směr
rozpínání	rozpínání	k1gNnSc2	rozpínání
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
také	také	k9	také
měří	měřit	k5eAaImIp3nS	měřit
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
počítáním	počítání	k1gNnSc7	počítání
pravidelně	pravidelně	k6eAd1	pravidelně
se	se	k3xPyFc4	se
opakujících	opakující	k2eAgInPc2d1	opakující
pohybů	pohyb	k1gInPc2	pohyb
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Slunce	slunce	k1gNnSc2	slunce
nebo	nebo	k8xC	nebo
kyvadla	kyvadlo	k1gNnSc2	kyvadlo
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc4d1	základní
myšlenku	myšlenka	k1gFnSc4	myšlenka
tohoto	tento	k3xDgNnSc2	tento
měření	měření	k1gNnSc2	měření
využil	využít	k5eAaPmAgMnS	využít
Aristotelés	Aristotelés	k1gInSc4	Aristotelés
k	k	k7c3	k
definici	definice	k1gFnSc3	definice
<g/>
:	:	kIx,	:
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Měření	měření	k1gNnSc2	měření
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
všechna	všechen	k3xTgNnPc1	všechen
jiná	jiný	k2eAgNnPc1d1	jiné
meření	meření	k1gNnPc1	meření
je	být	k5eAaImIp3nS	být
stanovování	stanovování	k1gNnSc1	stanovování
času	čas	k1gInSc2	čas
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
srovnávání	srovnávání	k1gNnSc6	srovnávání
s	s	k7c7	s
jednotkou	jednotka	k1gFnSc7	jednotka
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
času	čas	k1gInSc2	čas
s	s	k7c7	s
dobou	doba	k1gFnSc7	doba
opakovaného	opakovaný	k2eAgInSc2d1	opakovaný
děje	děj	k1gInSc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
Podmínkou	podmínka	k1gFnSc7	podmínka
měření	měření	k1gNnSc2	měření
je	být	k5eAaImIp3nS	být
stanovení	stanovení	k1gNnSc4	stanovení
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
jednotek	jednotka	k1gFnPc2	jednotka
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
rozdělení	rozdělení	k1gNnSc1	rozdělení
času	čas	k1gInSc2	čas
na	na	k7c4	na
vhodné	vhodný	k2eAgInPc4d1	vhodný
stupně	stupeň	k1gInPc4	stupeň
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
nutně	nutně	k6eAd1	nutně
podle	podle	k7c2	podle
přírodních	přírodní	k2eAgInPc2d1	přírodní
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
měření	měření	k1gNnSc4	měření
času	čas	k1gInSc2	čas
a	a	k8xC	a
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
o	o	k7c4	o
časomíru	časomíra	k1gFnSc4	časomíra
se	se	k3xPyFc4	se
lidé	člověk	k1gMnPc1	člověk
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
již	již	k9	již
tisíciletí	tisíciletí	k1gNnSc4	tisíciletí
počítáním	počítání	k1gNnSc7	počítání
(	(	kIx(	(
<g/>
pravidelných	pravidelný	k2eAgInPc2d1	pravidelný
<g/>
)	)	kIx)	)
pohybů	pohyb	k1gInPc2	pohyb
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tradičně	tradičně	k6eAd1	tradičně
na	na	k7c6	na
více	hodně	k6eAd2	hodně
úrovních	úroveň	k1gFnPc6	úroveň
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
<g/>
:	:	kIx,	:
pro	pro	k7c4	pro
delší	dlouhý	k2eAgInPc4d2	delší
intervaly	interval	k1gInPc4	interval
–	–	k?	–
ode	ode	k7c2	ode
dne	den	k1gInSc2	den
datování	datování	k1gNnSc2	datování
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
uspořádání	uspořádání	k1gNnSc2	uspořádání
těchto	tento	k3xDgInPc2	tento
jevů	jev	k1gInPc2	jev
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
kalendář	kalendář	k1gInSc1	kalendář
a	a	k8xC	a
jevy	jev	k1gInPc1	jev
a	a	k8xC	a
jednotky	jednotka	k1gFnPc1	jednotka
bývají	bývat	k5eAaImIp3nP	bývat
nazývány	nazývat	k5eAaImNgFnP	nazývat
jako	jako	k8xC	jako
kalendářní	kalendářní	k2eAgFnSc1d1	kalendářní
<g/>
.	.	kIx.	.
pro	pro	k7c4	pro
kratší	krátký	k2eAgInPc4d2	kratší
intervaly	interval	k1gInPc4	interval
–	–	k?	–
počítáním	počítání	k1gNnSc7	počítání
rychlejších	rychlý	k2eAgInPc2d2	rychlejší
pravidelných	pravidelný	k2eAgInPc2d1	pravidelný
jevů	jev	k1gInPc2	jev
na	na	k7c6	na
jevech	jev	k1gInPc6	jev
menšího	malý	k2eAgNnSc2d2	menší
měřítka	měřítko	k1gNnSc2	měřítko
na	na	k7c6	na
hodinách	hodina	k1gFnPc6	hodina
–	–	k?	–
slunečních	sluneční	k2eAgNnPc2d1	sluneční
<g/>
,	,	kIx,	,
objemových	objemový	k2eAgFnPc2d1	objemová
(	(	kIx(	(
<g/>
vodních	vodní	k2eAgFnPc2d1	vodní
<g/>
,	,	kIx,	,
přesýpacích	přesýpací	k2eAgFnPc2d1	přesýpací
<g/>
)	)	kIx)	)
a	a	k8xC	a
kyvadlových	kyvadlový	k2eAgInPc2d1	kyvadlový
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
kratší	krátký	k2eAgInPc1d2	kratší
jevy	jev	k1gInPc1	jev
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc1	jejich
měření	měření	k1gNnPc1	měření
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
čas	čas	k1gInSc1	čas
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
významu	význam	k1gInSc6	význam
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
tyto	tento	k3xDgFnPc1	tento
úrovně	úroveň	k1gFnPc1	úroveň
předvádí	předvádět	k5eAaImIp3nP	předvádět
např.	např.	kA	např.
pražský	pražský	k2eAgInSc1d1	pražský
staroměstský	staroměstský	k2eAgInSc1d1	staroměstský
orloj	orloj	k1gInSc1	orloj
s	s	k7c7	s
horním	horní	k2eAgInSc7d1	horní
ciferníkem	ciferník	k1gInSc7	ciferník
hodinovým	hodinový	k2eAgInSc7d1	hodinový
a	a	k8xC	a
dolním	dolní	k2eAgInSc7d1	dolní
kalendářovým	kalendářový	k2eAgInSc7d1	kalendářový
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
delší	dlouhý	k2eAgFnPc1d2	delší
jednotky	jednotka	k1gFnPc1	jednotka
času	čas	k1gInSc2	čas
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
rovněž	rovněž	k9	rovněž
z	z	k7c2	z
pohybu	pohyb	k1gInSc2	pohyb
kyvadla	kyvadlo	k1gNnSc2	kyvadlo
a	a	k8xC	a
ne	ne	k9	ne
z	z	k7c2	z
astronomických	astronomický	k2eAgInPc2d1	astronomický
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Měřením	měření	k1gNnSc7	měření
času	čas	k1gInSc2	čas
a	a	k8xC	a
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
hlavně	hlavně	k9	hlavně
vědci	vědec	k1gMnPc1	vědec
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
úkolů	úkol	k1gInPc2	úkol
fyziky	fyzika	k1gFnSc2	fyzika
a	a	k8xC	a
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
)	)	kIx)	)
a	a	k8xC	a
technici	technik	k1gMnPc1	technik
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kalendář	kalendář	k1gInSc1	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
dělení	dělení	k1gNnSc2	dělení
času	čas	k1gInSc2	čas
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
sledováním	sledování	k1gNnSc7	sledování
ročních	roční	k2eAgNnPc2d1	roční
období	období	k1gNnPc2	období
a	a	k8xC	a
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
vývoje	vývoj	k1gInPc1	vývoj
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
dnů	den	k1gInPc2	den
a	a	k8xC	a
částí	část	k1gFnPc2	část
dnů	den	k1gInPc2	den
(	(	kIx(	(
<g/>
noc	noc	k1gFnSc4	noc
<g/>
,	,	kIx,	,
světlý	světlý	k2eAgInSc4d1	světlý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
rozbřesk	rozbřesk	k1gInSc4	rozbřesk
(	(	kIx(	(
<g/>
východ	východ	k1gInSc4	východ
slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
svítání	svítání	k1gNnSc2	svítání
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ráno	ráno	k1gNnSc4	ráno
<g/>
,	,	kIx,	,
poledne	poledne	k1gNnSc4	poledne
<g/>
,	,	kIx,	,
dopoledne	dopoledne	k1gNnSc4	dopoledne
<g/>
,	,	kIx,	,
odpoledne	odpoledne	k1gNnSc4	odpoledne
<g/>
,	,	kIx,	,
západ	západ	k1gInSc1	západ
slunce	slunce	k1gNnSc1	slunce
(	(	kIx(	(
<g/>
stmívání	stmívání	k1gNnSc1	stmívání
<g/>
,	,	kIx,	,
soumrak	soumrak	k1gInSc1	soumrak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
večer	večer	k6eAd1	večer
<g/>
)	)	kIx)	)
sledováním	sledování	k1gNnSc7	sledování
astronomických	astronomický	k2eAgInPc2d1	astronomický
jevů	jev	k1gInPc2	jev
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
zdánlivého	zdánlivý	k2eAgInSc2d1	zdánlivý
oběhu	oběh	k1gInSc2	oběh
slunce	slunce	k1gNnSc2	slunce
a	a	k8xC	a
změny	změna	k1gFnSc2	změna
tvaru	tvar	k1gInSc2	tvar
osvětlené	osvětlený	k2eAgFnPc4d1	osvětlená
viditelné	viditelný	k2eAgFnPc4d1	viditelná
části	část	k1gFnPc4	část
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
kamenné	kamenný	k2eAgFnSc2d1	kamenná
(	(	kIx(	(
<g/>
neolitu	neolit	k1gInSc2	neolit
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
stavby	stavba	k1gFnPc1	stavba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
sloužily	sloužit	k5eAaImAgFnP	sloužit
ke	k	k7c3	k
stanovení	stanovení	k1gNnSc3	stanovení
slunovratu	slunovrat	k1gInSc2	slunovrat
a	a	k8xC	a
rovnodennosti	rovnodennost	k1gFnSc2	rovnodennost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Stonehenge	Stoneheng	k1gFnSc2	Stoneheng
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
pokročilejších	pokročilý	k2eAgInPc6d2	pokročilejší
způsobech	způsob	k1gInPc6	způsob
kalendářního	kalendářní	k2eAgNnSc2d1	kalendářní
měření	měření	k1gNnSc2	měření
patrně	patrně	k6eAd1	patrně
svědčí	svědčit	k5eAaImIp3nS	svědčit
nedávno	nedávno	k6eAd1	nedávno
nalezený	nalezený	k2eAgInSc1d1	nalezený
disk	disk	k1gInSc1	disk
z	z	k7c2	z
Nebry	Nebra	k1gFnSc2	Nebra
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
zdánlivý	zdánlivý	k2eAgInSc1d1	zdánlivý
roční	roční	k2eAgInSc1d1	roční
pohyb	pohyb	k1gInSc1	pohyb
některých	některý	k3yIgFnPc2	některý
hvězd	hvězda	k1gFnPc2	hvězda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Siria	Siria	k1gFnSc1	Siria
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
užíval	užívat	k5eAaImAgInS	užívat
ke	k	k7c3	k
stanovení	stanovení	k1gNnSc3	stanovení
správného	správný	k2eAgInSc2d1	správný
okamžiku	okamžik	k1gInSc2	okamžik
pro	pro	k7c4	pro
polní	polní	k2eAgFnPc4d1	polní
práce	práce	k1gFnPc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
doby	doba	k1gFnSc2	doba
kvantifikují	kvantifikovat	k5eAaBmIp3nP	kvantifikovat
trvání	trvání	k1gNnPc1	trvání
dějů	děj	k1gInPc2	děj
a	a	k8xC	a
intervalů	interval	k1gInPc2	interval
mezi	mezi	k7c7	mezi
událostmi	událost	k1gFnPc7	událost
proto	proto	k8xC	proto
vycházely	vycházet	k5eAaImAgFnP	vycházet
z	z	k7c2	z
dějů	děj	k1gInPc2	děj
vyvolaných	vyvolaný	k2eAgFnPc2d1	vyvolaná
pravidelnými	pravidelný	k2eAgInPc7d1	pravidelný
pohyby	pohyb	k1gInPc7	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgInSc7d3	nejvýznamnější
takovým	takový	k3xDgInSc7	takový
dějem	děj	k1gInSc7	děj
je	být	k5eAaImIp3nS	být
jistě	jistě	k9	jistě
stmívání	stmívání	k1gNnSc1	stmívání
a	a	k8xC	a
svítání	svítání	k1gNnSc1	svítání
<g/>
,	,	kIx,	,
střídání	střídání	k1gNnSc1	střídání
světlého	světlý	k2eAgInSc2d1	světlý
dne	den	k1gInSc2	den
a	a	k8xC	a
noci	noc	k1gFnSc2	noc
a	a	k8xC	a
roční	roční	k2eAgNnSc1d1	roční
střídání	střídání	k1gNnSc1	střídání
částí	část	k1gFnPc2	část
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
sloužily	sloužit	k5eAaImAgInP	sloužit
jako	jako	k9	jako
standardy	standard	k1gInPc1	standard
pohyb	pohyb	k1gInSc4	pohyb
Slunce	slunce	k1gNnSc1	slunce
po	po	k7c6	po
obloze	obloha	k1gFnSc6	obloha
<g/>
,	,	kIx,	,
fáze	fáze	k1gFnSc1	fáze
Měsíce	měsíc	k1gInSc2	měsíc
a	a	k8xC	a
kmit	kmit	k1gInSc4	kmit
kyvadla	kyvadlo	k1gNnSc2	kyvadlo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
ustálily	ustálit	k5eAaPmAgFnP	ustálit
jednotky	jednotka	k1gFnPc1	jednotka
nakonec	nakonec	k6eAd1	nakonec
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
bez	bez	k7c2	bez
přímé	přímý	k2eAgFnSc2d1	přímá
vazby	vazba	k1gFnSc2	vazba
na	na	k7c4	na
astronomické	astronomický	k2eAgInPc4d1	astronomický
jevy	jev	k1gInPc4	jev
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
upravují	upravovat	k5eAaImIp3nP	upravovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zmenšil	zmenšit	k5eAaPmAgInS	zmenšit
rozdíl	rozdíl	k1gInSc1	rozdíl
od	od	k7c2	od
astronomických	astronomický	k2eAgInPc2d1	astronomický
jevů	jev	k1gInPc2	jev
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hodiny	hodina	k1gFnSc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
mechanické	mechanický	k2eAgFnPc1d1	mechanická
hodiny	hodina	k1gFnPc1	hodina
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
nejistých	jistý	k2eNgFnPc2d1	nejistá
zpráv	zpráva	k1gFnPc2	zpráva
objevily	objevit	k5eAaPmAgFnP	objevit
snad	snad	k9	snad
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
spolehlivé	spolehlivý	k2eAgFnPc1d1	spolehlivá
zprávy	zpráva	k1gFnPc1	zpráva
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
až	až	k9	až
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
z	z	k7c2	z
anglických	anglický	k2eAgInPc2d1	anglický
a	a	k8xC	a
francouzských	francouzský	k2eAgInPc2d1	francouzský
klášterů	klášter	k1gInPc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Mechanické	mechanický	k2eAgFnPc1d1	mechanická
hodiny	hodina	k1gFnPc1	hodina
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
)	)	kIx)	)
oscilátoru	oscilátor	k1gInSc2	oscilátor
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
)	)	kIx)	)
zdroje	zdroj	k1gInSc2	zdroj
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
3	[number]	k4	3
<g/>
)	)	kIx)	)
počítacího	počítací	k2eAgMnSc2d1	počítací
a	a	k8xC	a
indikačního	indikační	k2eAgNnSc2d1	indikační
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
hodiny	hodina	k1gFnPc1	hodina
užívaly	užívat	k5eAaImAgFnP	užívat
jako	jako	k9	jako
oscilátor	oscilátor	k1gInSc4	oscilátor
poměrně	poměrně	k6eAd1	poměrně
nepřesný	přesný	k2eNgInSc4d1	nepřesný
lihýř	lihýř	k1gInSc4	lihýř
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
zdroj	zdroj	k1gInSc1	zdroj
energie	energie	k1gFnSc2	energie
závaží	závaží	k1gNnSc2	závaží
a	a	k8xC	a
měly	mít	k5eAaImAgFnP	mít
i	i	k9	i
bicí	bicí	k2eAgNnSc4d1	bicí
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
přenosné	přenosný	k2eAgFnPc1d1	přenosná
a	a	k8xC	a
kapesní	kapesní	k2eAgFnPc1d1	kapesní
hodiny	hodina	k1gFnPc1	hodina
s	s	k7c7	s
pružinou	pružina	k1gFnSc7	pružina
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
však	však	k9	však
málo	málo	k6eAd1	málo
přesné	přesný	k2eAgNnSc1d1	přesné
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pokusech	pokus	k1gInPc6	pokus
s	s	k7c7	s
volným	volný	k2eAgInSc7d1	volný
pádem	pád	k1gInSc7	pád
měřil	měřit	k5eAaImAgInS	měřit
snad	snad	k9	snad
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galile	k1gMnPc1	Galile
dobu	doba	k1gFnSc4	doba
počítáním	počítání	k1gNnSc7	počítání
srdečního	srdeční	k2eAgInSc2d1	srdeční
tepu	tep	k1gInSc2	tep
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
smrtí	smrt	k1gFnSc7	smrt
zkonstruoval	zkonstruovat	k5eAaPmAgInS	zkonstruovat
velmi	velmi	k6eAd1	velmi
důmyslné	důmyslný	k2eAgFnPc4d1	důmyslná
hodiny	hodina	k1gFnPc4	hodina
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
kyvadla	kyvadlo	k1gNnSc2	kyvadlo
jako	jako	k8xS	jako
oscilátoru	oscilátor	k1gInSc2	oscilátor
(	(	kIx(	(
<g/>
prvku	prvek	k1gInSc2	prvek
určujícího	určující	k2eAgInSc2d1	určující
rychlost	rychlost	k1gFnSc1	rychlost
chodu	chod	k1gInSc2	chod
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kyvadlové	kyvadlový	k2eAgFnPc1d1	kyvadlová
hodiny	hodina	k1gFnPc1	hodina
však	však	k9	však
poprvé	poprvé	k6eAd1	poprvé
realizoval	realizovat	k5eAaBmAgMnS	realizovat
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1657	[number]	k4	1657
holandský	holandský	k2eAgMnSc1d1	holandský
fyzik	fyzik	k1gMnSc1	fyzik
Christiaan	Christiaana	k1gFnPc2	Christiaana
Huygens	Huygens	k1gInSc1	Huygens
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
také	také	k9	také
o	o	k7c4	o
něco	něco	k3yInSc4	něco
později	pozdě	k6eAd2	pozdě
vybavil	vybavit	k5eAaPmAgMnS	vybavit
lihýř	lihýř	k1gFnSc4	lihýř
pružinou	pružina	k1gFnSc7	pružina
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
nepokoj	nepokoj	k1gInSc4	nepokoj
<g/>
,	,	kIx,	,
přesnější	přesný	k2eAgInSc4d2	přesnější
oscilátor	oscilátor	k1gInSc4	oscilátor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
hodil	hodit	k5eAaImAgInS	hodit
i	i	k9	i
do	do	k7c2	do
přenosných	přenosný	k2eAgMnPc2d1	přenosný
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
malých	malý	k2eAgFnPc2d1	malá
hodinek	hodinka	k1gFnPc2	hodinka
<g/>
.	.	kIx.	.
</s>
<s>
Přesnost	přesnost	k1gFnSc1	přesnost
mechanických	mechanický	k2eAgFnPc2d1	mechanická
hodin	hodina	k1gFnPc2	hodina
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
zvyšovala	zvyšovat	k5eAaImAgFnS	zvyšovat
a	a	k8xC	a
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
změřit	změřit	k5eAaPmF	změřit
nerovnoměrnosti	nerovnoměrnost	k1gFnPc4	nerovnoměrnost
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
zdánlivý	zdánlivý	k2eAgInSc4d1	zdánlivý
pohyb	pohyb	k1gInSc4	pohyb
Slunce	slunce	k1gNnSc2	slunce
jako	jako	k8xS	jako
časový	časový	k2eAgInSc1d1	časový
normál	normál	k1gInSc1	normál
nahrazen	nahradit	k5eAaPmNgInS	nahradit
mechanickými	mechanický	k2eAgInPc7d1	mechanický
oscilátory	oscilátor	k1gInPc7	oscilátor
a	a	k8xC	a
hodinami	hodina	k1gFnPc7	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
používat	používat	k5eAaImF	používat
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
pohony	pohon	k1gInPc4	pohon
a	a	k8xC	a
oscilátory	oscilátor	k1gInPc4	oscilátor
<g/>
.	.	kIx.	.
</s>
<s>
Nejrozšířenější	rozšířený	k2eAgInPc1d3	nejrozšířenější
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
hodiny	hodina	k1gFnPc4	hodina
s	s	k7c7	s
elektrickým	elektrický	k2eAgInSc7d1	elektrický
pohonem	pohon	k1gInSc7	pohon
a	a	k8xC	a
piezoelektrickým	piezoelektrický	k2eAgInSc7d1	piezoelektrický
či	či	k8xC	či
křemenným	křemenný	k2eAgInSc7d1	křemenný
(	(	kIx(	(
<g/>
quartzovým	quartzův	k2eAgInSc7d1	quartzův
<g/>
)	)	kIx)	)
oscilátorem	oscilátor	k1gInSc7	oscilátor
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
má	mít	k5eAaImIp3nS	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
přesnost	přesnost	k1gFnSc4	přesnost
<g/>
,	,	kIx,	,
nízké	nízký	k2eAgInPc4d1	nízký
výrobní	výrobní	k2eAgInPc4d1	výrobní
náklady	náklad	k1gInPc4	náklad
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
propojuje	propojovat	k5eAaImIp3nS	propojovat
s	s	k7c7	s
elektronickými	elektronický	k2eAgInPc7d1	elektronický
obvody	obvod	k1gInPc7	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nejpřesnější	přesný	k2eAgNnPc4d3	nejpřesnější
měření	měření	k1gNnPc4	měření
dob	doba	k1gFnPc2	doba
se	se	k3xPyFc4	se
užívají	užívat	k5eAaImIp3nP	užívat
atomové	atomový	k2eAgFnSc2d1	atomová
hodiny	hodina	k1gFnSc2	hodina
<g/>
,	,	kIx,	,
využívajících	využívající	k2eAgInPc2d1	využívající
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
frekvence	frekvence	k1gFnSc1	frekvence
pravidelných	pravidelný	k2eAgInPc2d1	pravidelný
kmitů	kmit	k1gInPc2	kmit
při	při	k7c6	při
stavovém	stavový	k2eAgInSc6d1	stavový
přechodu	přechod	k1gInSc6	přechod
atomu	atom	k1gInSc2	atom
cesia	cesium	k1gNnSc2	cesium
<g/>
.	.	kIx.	.
</s>
<s>
Nejpřesnější	přesný	k2eAgInSc1d3	nejpřesnější
světový	světový	k2eAgInSc1d1	světový
čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
určuje	určovat	k5eAaImIp3nS	určovat
statistickým	statistický	k2eAgInSc7d1	statistický
průměrem	průměr	k1gInSc7	průměr
několika	několik	k4yIc2	několik
set	set	k1gInSc4	set
césiových	césiový	k2eAgFnPc2d1	césiový
hodin	hodina	k1gFnPc2	hodina
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nepřesnost	nepřesnost	k1gFnSc1	nepřesnost
(	(	kIx(	(
<g/>
lépe	dobře	k6eAd2	dobře
nerovnoměrnost	nerovnoměrnost	k1gFnSc1	nerovnoměrnost
čili	čili	k8xC	čili
variace	variace	k1gFnSc1	variace
chodu	chod	k1gInSc2	chod
<g/>
)	)	kIx)	)
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
činila	činit	k5eAaImAgFnS	činit
u	u	k7c2	u
prvních	první	k4xOgFnPc2	první
lihýřových	lihýřův	k2eAgFnPc2d1	lihýřův
hodin	hodina	k1gFnPc2	hodina
asi	asi	k9	asi
100	[number]	k4	100
s	s	k7c7	s
<g/>
/	/	kIx~	/
<g/>
den	den	k1gInSc1	den
(	(	kIx(	(
<g/>
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
u	u	k7c2	u
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
kyvadlových	kyvadlový	k2eAgFnPc2d1	kyvadlová
hodin	hodina	k1gFnPc2	hodina
snížila	snížit	k5eAaPmAgFnS	snížit
na	na	k7c4	na
sekundu	sekunda	k1gFnSc4	sekunda
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
u	u	k7c2	u
křemenných	křemenný	k2eAgFnPc2d1	křemenná
hodin	hodina	k1gFnPc2	hodina
na	na	k7c4	na
sekundu	sekunda	k1gFnSc4	sekunda
za	za	k7c4	za
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
a	a	k8xC	a
u	u	k7c2	u
césiových	césiový	k2eAgFnPc2d1	césiový
hodin	hodina	k1gFnPc2	hodina
na	na	k7c4	na
sekundu	sekunda	k1gFnSc4	sekunda
za	za	k7c4	za
třicet	třicet	k4xCc4	třicet
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
1	[number]	k4	1
:	:	kIx,	:
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
15	[number]	k4	15
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
r.	r.	kA	r.
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
rekordní	rekordní	k2eAgFnSc1d1	rekordní
dosažená	dosažený	k2eAgFnSc1d1	dosažená
přesnost	přesnost	k1gFnSc1	přesnost
měření	měření	k1gNnSc2	měření
doby	doba	k1gFnSc2	doba
1	[number]	k4	1
sekunda	sekunda	k1gFnSc1	sekunda
za	za	k7c4	za
32	[number]	k4	32
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvojnásobek	dvojnásobek	k1gInSc4	dvojnásobek
věku	věk	k1gInSc2	věk
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
relativní	relativní	k2eAgFnSc1d1	relativní
přesnost	přesnost	k1gFnSc1	přesnost
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
staletí	staletí	k1gNnPc2	staletí
od	od	k7c2	od
vynálezu	vynález	k1gInSc2	vynález
hodin	hodina	k1gFnPc2	hodina
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
přesnost	přesnost	k1gFnSc1	přesnost
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
o	o	k7c4	o
16	[number]	k4	16
desetinných	desetinný	k2eAgInPc2d1	desetinný
řádů	řád	k1gInPc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
kmitočtu	kmitočet	k1gInSc2	kmitočet
patří	patřit	k5eAaImIp3nS	patřit
dnes	dnes	k6eAd1	dnes
k	k	k7c3	k
nejpřesnějším	přesný	k2eAgNnPc3d3	nejpřesnější
měřením	měření	k1gNnPc3	měření
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
(	(	kIx(	(
<g/>
r.	r.	kA	r.
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
znám	znám	k2eAgInSc1d1	znám
princip	princip	k1gInSc1	princip
tzv.	tzv.	kA	tzv.
jaderných	jaderný	k2eAgFnPc2d1	jaderná
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
založených	založený	k2eAgFnPc2d1	založená
na	na	k7c6	na
magnetickém	magnetický	k2eAgInSc6d1	magnetický
dipólovém	dipólový	k2eAgInSc6d1	dipólový
přechodu	přechod	k1gInSc6	přechod
mezi	mezi	k7c7	mezi
energetickými	energetický	k2eAgInPc7d1	energetický
stavy	stav	k1gInPc7	stav
jádra	jádro	k1gNnSc2	jádro
iontu	ion	k1gInSc3	ion
thoria	thorium	k1gNnSc2	thorium
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
umožňoval	umožňovat	k5eAaImAgInS	umožňovat
dosažení	dosažení	k1gNnSc4	dosažení
nepřesnosti	nepřesnost	k1gFnSc2	nepřesnost
pouhé	pouhý	k2eAgInPc1d1	pouhý
1	[number]	k4	1
s	s	k7c7	s
za	za	k7c2	za
200	[number]	k4	200
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
je	být	k5eAaImIp3nS	být
společné	společný	k2eAgNnSc4d1	společné
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
několik	několik	k4yIc4	několik
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
pojmů	pojem	k1gInPc2	pojem
-	-	kIx~	-
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	a
veličin	veličina	k1gFnPc2	veličina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
<g/>
:	:	kIx,	:
okamžik	okamžik	k1gInSc4	okamžik
<g/>
:	:	kIx,	:
bod	bod	k1gInSc4	bod
na	na	k7c6	na
časové	časový	k2eAgFnSc6d1	časová
ose	osa	k1gFnSc6	osa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
čas	čas	k1gInSc1	čas
daného	daný	k2eAgInSc2d1	daný
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
veličinou	veličina	k1gFnSc7	veličina
protenzivní	protenzivní	k2eAgFnSc7d1	protenzivní
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
okamžitá	okamžitý	k2eAgFnSc1d1	okamžitá
hodnota	hodnota	k1gFnSc1	hodnota
(	(	kIx(	(
<g/>
datum	datum	k1gNnSc1	datum
<g/>
,	,	kIx,	,
časový	časový	k2eAgInSc4d1	časový
údaj	údaj	k1gInSc4	údaj
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
jako	jako	k9	jako
doba	doba	k1gFnSc1	doba
trvání	trvání	k1gNnSc2	trvání
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
od	od	k7c2	od
dohodnutého	dohodnutý	k2eAgInSc2d1	dohodnutý
počátečního	počáteční	k2eAgInSc2d1	počáteční
okamžiku	okamžik	k1gInSc2	okamžik
k	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
okamžiku	okamžik	k1gInSc3	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
poloze	poloha	k1gFnSc3	poloha
<g/>
;	;	kIx,	;
datum	datum	k1gInSc1	datum
<g/>
,	,	kIx,	,
časový	časový	k2eAgInSc1d1	časový
údaj	údaj	k1gInSc1	údaj
<g/>
:	:	kIx,	:
značka	značka	k1gFnSc1	značka
přiřazená	přiřazený	k2eAgFnSc1d1	přiřazená
okamžiku	okamžik	k1gInSc2	okamžik
pomocí	pomocí	k7c2	pomocí
uvedené	uvedený	k2eAgFnSc2d1	uvedená
časové	časový	k2eAgFnSc2d1	časová
stupnice	stupnice	k1gFnSc2	stupnice
<g/>
;	;	kIx,	;
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
souřadnicím	souřadnice	k1gFnPc3	souřadnice
polohy	poloha	k1gFnSc2	poloha
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
souřadném	souřadný	k2eAgInSc6d1	souřadný
systému	systém	k1gInSc6	systém
<g/>
;	;	kIx,	;
doba	doba	k1gFnSc1	doba
trvání	trvání	k1gNnSc2	trvání
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
spojité	spojitý	k2eAgFnPc4d1	spojitá
časové	časový	k2eAgFnPc4d1	časová
stupnice	stupnice	k1gFnPc4	stupnice
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
rozsah	rozsah	k1gInSc1	rozsah
časového	časový	k2eAgInSc2d1	časový
intervalu	interval	k1gInSc2	interval
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
části	část	k1gFnPc1	část
časové	časový	k2eAgFnSc2d1	časová
osy	osa	k1gFnSc2	osa
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
okamžiky	okamžik	k1gInPc7	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
veličina	veličina	k1gFnSc1	veličina
extenzivní	extenzivní	k2eAgFnSc1d1	extenzivní
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
<g/>
,	,	kIx,	,
doba	doba	k1gFnSc1	doba
jsou	být	k5eAaImIp3nP	být
základní	základní	k2eAgFnSc7d1	základní
veličinou	veličina	k1gFnSc7	veličina
všech	všecek	k3xTgFnPc2	všecek
běžně	běžně	k6eAd1	běžně
používaných	používaný	k2eAgFnPc2d1	používaná
soustav	soustava	k1gFnPc2	soustava
veličin	veličina	k1gFnPc2	veličina
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
i	i	k9	i
soustavy	soustava	k1gFnSc2	soustava
SI	si	k1gNnSc2	si
<g/>
.	.	kIx.	.
</s>
<s>
Doporučená	doporučený	k2eAgFnSc1d1	Doporučená
značka	značka	k1gFnSc1	značka
veličiny	veličina	k1gFnSc2	veličina
(	(	kIx(	(
<g/>
doby	doba	k1gFnSc2	doba
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
t	t	k?	t
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
time	time	k1gInSc1	time
<g/>
,	,	kIx,	,
lat.	lat.	k?	lat.
tempus	tempus	k1gInSc1	tempus
<g/>
)	)	kIx)	)
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
ve	v	k7c6	v
fyzikální	fyzikální	k2eAgFnSc6d1	fyzikální
literatuře	literatura	k1gFnSc6	literatura
takto	takto	k6eAd1	takto
značí	značit	k5eAaImIp3nS	značit
i	i	k9	i
čas	čas	k1gInSc4	čas
daného	daný	k2eAgInSc2d1	daný
okamžiku	okamžik	k1gInSc2	okamžik
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
s	s	k7c7	s
identifikačním	identifikační	k2eAgInSc7d1	identifikační
indexem	index	k1gInSc7	index
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
doby	doba	k1gFnSc2	doba
trvání	trvání	k1gNnSc2	trvání
používá	používat	k5eAaImIp3nS	používat
značka	značka	k1gFnSc1	značka
Δ	Δ	k?	Δ
nebo	nebo	k8xC	nebo
zápis	zápis	k1gInSc1	zápis
pomocí	pomocí	k7c2	pomocí
rozdílu	rozdíl	k1gInSc2	rozdíl
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
např.	např.	kA	např.
t	t	k?	t
−	−	k?	−
t	t	k?	t
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Doporučený	doporučený	k2eAgInSc1d1	doporučený
zápis	zápis	k1gInSc1	zápis
data	datum	k1gNnSc2	datum
a	a	k8xC	a
časového	časový	k2eAgInSc2d1	časový
údaje	údaj	k1gInSc2	údaj
pro	pro	k7c4	pro
vědecké	vědecký	k2eAgInPc4d1	vědecký
a	a	k8xC	a
technické	technický	k2eAgInPc4d1	technický
účely	účel	k1gInPc4	účel
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
2014	[number]	k4	2014
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
T	T	kA	T
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
<g/>
:	:	kIx,	:
<g/>
10,33	[number]	k4	10,33
pro	pro	k7c4	pro
14	[number]	k4	14
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
v	v	k7c6	v
9	[number]	k4	9
h	h	k?	h
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
25	[number]	k4	25
min	mina	k1gFnPc2	mina
a	a	k8xC	a
10,33	[number]	k4	10,33
sekundy	sekunda	k1gFnSc2	sekunda
V	v	k7c6	v
běžných	běžný	k2eAgFnPc6d1	běžná
písemnostech	písemnost	k1gFnPc6	písemnost
se	se	k3xPyFc4	se
za	za	k7c4	za
správný	správný	k2eAgInSc4d1	správný
považuje	považovat	k5eAaImIp3nS	považovat
i	i	k8xC	i
vzestupný	vzestupný	k2eAgInSc1d1	vzestupný
zápis	zápis	k1gInSc1	zápis
pouhého	pouhý	k2eAgNnSc2d1	pouhé
data	datum	k1gNnSc2	datum
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
nebo	nebo	k8xC	nebo
14.08	[number]	k4	14.08
<g/>
.2014	.2014	k4	.2014
<g/>
)	)	kIx)	)
a	a	k8xC	a
zápis	zápis	k1gInSc1	zápis
časového	časový	k2eAgInSc2d1	časový
údaje	údaj	k1gInSc2	údaj
zaokrouhleného	zaokrouhlený	k2eAgMnSc4d1	zaokrouhlený
na	na	k7c4	na
minuty	minuta	k1gFnPc4	minuta
s	s	k7c7	s
rozdělující	rozdělující	k2eAgFnSc7d1	rozdělující
tečkou	tečka	k1gFnSc7	tečka
a	a	k8xC	a
bez	bez	k7c2	bez
nuly	nula	k1gFnSc2	nula
u	u	k7c2	u
jednomístných	jednomístný	k2eAgFnPc2d1	jednomístná
hodin	hodina	k1gFnPc2	hodina
(	(	kIx(	(
<g/>
9.25	[number]	k4	9.25
či	či	k8xC	či
9.25	[number]	k4	9.25
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
jednotkou	jednotka	k1gFnSc7	jednotka
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
doby	doba	k1gFnSc2	doba
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	si	k1gNnPc2	si
sekunda	sekunda	k1gFnSc1	sekunda
(	(	kIx(	(
<g/>
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
značka	značka	k1gFnSc1	značka
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k9	jako
doba	doba	k1gFnSc1	doba
trvání	trvání	k1gNnSc2	trvání
9	[number]	k4	9
192	[number]	k4	192
631	[number]	k4	631
770	[number]	k4	770
period	perioda	k1gFnPc2	perioda
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
přechodu	přechod	k1gInSc6	přechod
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
hladinami	hladina	k1gFnPc7	hladina
velmi	velmi	k6eAd1	velmi
jemné	jemný	k2eAgFnPc4d1	jemná
struktury	struktura	k1gFnPc4	struktura
základního	základní	k2eAgInSc2d1	základní
stavu	stav	k1gInSc2	stav
atomu	atom	k1gInSc2	atom
cesia	cesium	k1gNnSc2	cesium
133	[number]	k4	133
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
definice	definice	k1gFnSc1	definice
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
cesiový	cesiový	k2eAgInSc4d1	cesiový
atom	atom	k1gInSc4	atom
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
absolutní	absolutní	k2eAgFnSc2d1	absolutní
nuly	nula	k1gFnSc2	nula
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
vlastní	vlastní	k2eAgInSc4d1	vlastní
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
hovorovém	hovorový	k2eAgInSc6d1	hovorový
jazyce	jazyk	k1gInSc6	jazyk
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
této	tento	k3xDgFnSc2	tento
jednotky	jednotka	k1gFnSc2	jednotka
používá	používat	k5eAaImIp3nS	používat
výraz	výraz	k1gInSc4	výraz
vteřina	vteřina	k1gFnSc1	vteřina
<g/>
,	,	kIx,	,
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
to	ten	k3xDgNnSc1	ten
však	však	k9	však
není	být	k5eNaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
kvůli	kvůli	k7c3	kvůli
jednoznačnosti	jednoznačnost	k1gFnSc3	jednoznačnost
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
komise	komise	k1gFnSc1	komise
pro	pro	k7c4	pro
váhy	váha	k1gFnPc4	váha
a	a	k8xC	a
míry	míra	k1gFnSc2	míra
CIMP	CIMP	kA	CIMP
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
používat	používat	k5eAaImF	používat
v	v	k7c6	v
SI	si	k1gNnSc6	si
souběžně	souběžně	k6eAd1	souběžně
se	s	k7c7	s
základní	základní	k2eAgFnSc7d1	základní
jednotkou	jednotka	k1gFnSc7	jednotka
sekunda	sekunda	k1gFnSc1	sekunda
a	a	k8xC	a
jejími	její	k3xOp3gInPc7	její
dekadickými	dekadický	k2eAgInPc7d1	dekadický
násobky	násobek	k1gInPc7	násobek
a	a	k8xC	a
díly	díl	k1gInPc7	díl
<g/>
,	,	kIx,	,
s	s	k7c7	s
názvy	název	k1gInPc7	název
odvozenými	odvozený	k2eAgFnPc7d1	odvozená
standardními	standardní	k2eAgFnPc7d1	standardní
předponami	předpona	k1gFnPc7	předpona
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
milisekundou	milisekunda	k1gFnSc7	milisekunda
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
ms	ms	k?	ms
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mikrosekundou	mikrosekunda	k1gFnSc7	mikrosekunda
(	(	kIx(	(
<g/>
μ	μ	k?	μ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nanosekundou	nanosekunda	k1gFnSc7	nanosekunda
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
ns	ns	k?	ns
<g/>
)	)	kIx)	)
a	a	k8xC	a
pikosekundou	pikosekunda	k1gFnSc7	pikosekunda
(	(	kIx(	(
<g/>
ps	ps	k0	ps
<g/>
)	)	kIx)	)
)	)	kIx)	)
také	také	k9	také
následující	následující	k2eAgFnPc1d1	následující
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
:	:	kIx,	:
minuta	minuta	k1gFnSc1	minuta
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
min	mina	k1gFnPc2	mina
<g/>
,	,	kIx,	,
1	[number]	k4	1
min	mina	k1gFnPc2	mina
=	=	kIx~	=
60	[number]	k4	60
s	s	k7c7	s
hodina	hodina	k1gFnSc1	hodina
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
h	h	k?	h
<g/>
,	,	kIx,	,
1	[number]	k4	1
h	h	k?	h
=	=	kIx~	=
60	[number]	k4	60
min	mina	k1gFnPc2	mina
=	=	kIx~	=
3600	[number]	k4	3600
s	s	k7c7	s
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
d	d	k?	d
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
1	[number]	k4	1
d	d	k?	d
=	=	kIx~	=
24	[number]	k4	24
h	h	k?	h
=	=	kIx~	=
86	[number]	k4	86
400	[number]	k4	400
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Swatch	Swatch	k1gMnSc1	Swatch
<g/>
)	)	kIx)	)
beat	beat	k1gInSc1	beat
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
překládaná	překládaný	k2eAgFnSc1d1	překládaná
jako	jako	k8xS	jako
takt	takt	k1gInSc1	takt
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
zavináč	zavináč	k1gInSc1	zavináč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1	[number]	k4	1
beat	beat	k1gInSc1	beat
=	=	kIx~	=
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
1000	[number]	k4	1000
dne	den	k1gInSc2	den
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
zavedená	zavedený	k2eAgFnSc1d1	zavedená
pro	pro	k7c4	pro
udávání	udávání	k1gNnSc4	udávání
tzv.	tzv.	kA	tzv.
internetového	internetový	k2eAgInSc2d1	internetový
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
pro	pro	k7c4	pro
dobu	doba	k1gFnSc4	doba
trvání	trvání	k1gNnSc2	trvání
děje	děj	k1gInSc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
Nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
mimosoustavovou	mimosoustavový	k2eAgFnSc4d1	mimosoustavový
jednotku	jednotka	k1gFnSc4	jednotka
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
akceptovanou	akceptovaný	k2eAgFnSc4d1	akceptovaná
v	v	k7c6	v
příručce	příručka	k1gFnSc6	příručka
SI	si	k1gNnSc2	si
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnPc1d2	veliký
mimosoustavové	mimosoustavový	k2eAgFnPc1d1	mimosoustavový
jednotky	jednotka	k1gFnPc1	jednotka
než	než	k8xS	než
den	den	k1gInSc4	den
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
např.	např.	kA	např.
v	v	k7c6	v
kalendáři	kalendář	k1gInSc6	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
však	však	k9	však
již	již	k6eAd1	již
definovány	definovat	k5eAaBmNgFnP	definovat
jednoznačně	jednoznačně	k6eAd1	jednoznačně
<g/>
:	:	kIx,	:
kalendářní	kalendářní	k2eAgInSc4d1	kalendářní
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
přechodu	přechod	k1gInSc3	přechod
na	na	k7c4	na
letní	letní	k2eAgInSc4d1	letní
čas	čas	k1gInSc4	čas
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
velikost	velikost	k1gFnSc1	velikost
může	moct	k5eAaImIp3nS	moct
lišit	lišit	k5eAaImF	lišit
o	o	k7c4	o
±	±	k?	±
1	[number]	k4	1
hodinu	hodina	k1gFnSc4	hodina
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
korekci	korekce	k1gFnSc3	korekce
koordinovaného	koordinovaný	k2eAgInSc2d1	koordinovaný
světového	světový	k2eAgInSc2d1	světový
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
UTC	UTC	kA	UTC
<g/>
)	)	kIx)	)
na	na	k7c4	na
reálnou	reálný	k2eAgFnSc4d1	reálná
rotaci	rotace	k1gFnSc4	rotace
Země	zem	k1gFnSc2	zem
o	o	k7c6	o
±	±	k?	±
1	[number]	k4	1
přestupnou	přestupný	k2eAgFnSc4d1	přestupná
sekundu	sekunda	k1gFnSc4	sekunda
týden	týden	k1gInSc4	týden
je	být	k5eAaImIp3nS	být
<g />
.	.	kIx.	.
</s>
<s>
7	[number]	k4	7
kalendářních	kalendářní	k2eAgInPc2d1	kalendářní
dní	den	k1gInPc2	den
měsíc	měsíc	k1gInSc1	měsíc
je	být	k5eAaImIp3nS	být
28	[number]	k4	28
až	až	k9	až
31	[number]	k4	31
kalendářních	kalendářní	k2eAgInPc2d1	kalendářní
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
kalendářní	kalendářní	k2eAgInSc4d1	kalendářní
<g/>
)	)	kIx)	)
rok	rok	k1gInSc4	rok
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
a	a	k8xC	a
nebo	nebo	k8xC	nebo
též	též	k9	též
r	r	kA	r
nebo	nebo	k8xC	nebo
y	y	k?	y
<g/>
,	,	kIx,	,
yr	yr	k?	yr
z	z	k7c2	z
anglického	anglický	k2eAgMnSc2d1	anglický
year	year	k1gInSc1	year
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
365	[number]	k4	365
dní	den	k1gInPc2	den
(	(	kIx(	(
<g/>
366	[number]	k4	366
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
rok	rok	k1gInSc4	rok
přestupný	přestupný	k2eAgInSc4d1	přestupný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
den	dna	k1gFnPc2	dna
a	a	k8xC	a
rok	rok	k1gInSc1	rok
jsou	být	k5eAaImIp3nP	být
odvozeny	odvodit	k5eAaPmNgFnP	odvodit
z	z	k7c2	z
astronomických	astronomický	k2eAgFnPc2d1	astronomická
časových	časový	k2eAgFnPc2d1	časová
charakteristik	charakteristika	k1gFnPc2	charakteristika
otáčení	otáčení	k1gNnSc2	otáčení
Země	zem	k1gFnPc4	zem
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
a	a	k8xC	a
jejího	její	k3xOp3gInSc2	její
oběhu	oběh	k1gInSc2	oběh
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
astronomové	astronom	k1gMnPc1	astronom
proto	proto	k8xC	proto
od	od	k7c2	od
kalendářního	kalendářní	k2eAgInSc2d1	kalendářní
dne	den	k1gInSc2	den
a	a	k8xC	a
roku	rok	k1gInSc2	rok
důsledně	důsledně	k6eAd1	důsledně
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
přesně	přesně	k6eAd1	přesně
definované	definovaný	k2eAgInPc1d1	definovaný
pojmy	pojem	k1gInPc1	pojem
pravý	pravý	k2eAgInSc1d1	pravý
sluneční	sluneční	k2eAgInSc4d1	sluneční
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
střední	střední	k2eAgInSc4d1	střední
sluneční	sluneční	k2eAgInSc4d1	sluneční
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
tropický	tropický	k2eAgInSc4d1	tropický
rok	rok	k1gInSc4	rok
a	a	k8xC	a
siderický	siderický	k2eAgInSc4d1	siderický
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
některé	některý	k3yIgFnPc4	některý
přírodní	přírodní	k2eAgFnPc4d1	přírodní
vědy	věda	k1gFnPc4	věda
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgMnPc4d1	zabývající
se	se	k3xPyFc4	se
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
časovými	časový	k2eAgNnPc7d1	časové
obdobími	období	k1gNnPc7	období
(	(	kIx(	(
<g/>
astrofyzika	astrofyzika	k1gFnSc1	astrofyzika
<g/>
,	,	kIx,	,
kosmologie	kosmologie	k1gFnSc1	kosmologie
<g/>
,	,	kIx,	,
geologie	geologie	k1gFnSc1	geologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
však	však	k9	však
potřebují	potřebovat	k5eAaImIp3nP	potřebovat
větší	veliký	k2eAgFnPc4d2	veliký
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
exaktně	exaktně	k6eAd1	exaktně
definované	definovaný	k2eAgFnPc1d1	definovaná
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
proto	proto	k8xC	proto
jednotku	jednotka	k1gFnSc4	jednotka
definovanou	definovaný	k2eAgFnSc4d1	definovaná
jako	jako	k8xC	jako
přesný	přesný	k2eAgInSc4d1	přesný
násobek	násobek	k1gInSc4	násobek
sekundy	sekunda	k1gFnSc2	sekunda
<g/>
:	:	kIx,	:
rok	rok	k1gInSc1	rok
(	(	kIx(	(
<g/>
annus	annus	k1gInSc1	annus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
a	a	k8xC	a
<g/>
,	,	kIx,	,
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
různých	různý	k2eAgFnPc6d1	různá
verzích	verze	k1gFnPc6	verze
–	–	k?	–
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
1	[number]	k4	1
a	a	k8xC	a
=	=	kIx~	=
31	[number]	k4	31
556	[number]	k4	556
926	[number]	k4	926
s	s	k7c7	s
nebo	nebo	k8xC	nebo
1	[number]	k4	1
a	a	k8xC	a
=	=	kIx~	=
31	[number]	k4	31
600	[number]	k4	600
000	[number]	k4	000
s	s	k7c7	s
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
geologii	geologie	k1gFnSc6	geologie
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
též	též	k9	též
v	v	k7c6	v
jaderné	jaderný	k2eAgFnSc6d1	jaderná
fyzice	fyzika	k1gFnSc6	fyzika
pro	pro	k7c4	pro
pomalu	pomalu	k6eAd1	pomalu
se	se	k3xPyFc4	se
rozpadající	rozpadající	k2eAgInPc1d1	rozpadající
atomy	atom	k1gInPc1	atom
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
střední	střední	k2eAgFnPc4d1	střední
<g/>
)	)	kIx)	)
juliánský	juliánský	k2eAgInSc4d1	juliánský
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
aj	aj	kA	aj
nebo	nebo	k8xC	nebo
též	též	k9	též
pouze	pouze	k6eAd1	pouze
a	a	k8xC	a
<g/>
,	,	kIx,	,
1	[number]	k4	1
aj	aj	kA	aj
=	=	kIx~	=
365,25	[number]	k4	365,25
dne	den	k1gInSc2	den
=	=	kIx~	=
31	[number]	k4	31
557	[number]	k4	557
600	[number]	k4	600
s	s	k7c7	s
(	(	kIx(	(
<g/>
v	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
a	a	k8xC	a
astrofyzice	astrofyzika	k1gFnSc6	astrofyzika
-	-	kIx~	-
dle	dle	k7c2	dle
IAU	IAU	kA	IAU
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
z	z	k7c2	z
jejich	jejich	k3xOp3gInPc2	jejich
násobků	násobek	k1gInPc2	násobek
nejčastěji	často	k6eAd3	často
1	[number]	k4	1
Ma	Ma	k1gFnPc2	Ma
=	=	kIx~	=
106	[number]	k4	106
a	a	k8xC	a
1	[number]	k4	1
Ga	Ga	k1gFnPc2	Ga
=	=	kIx~	=
109	[number]	k4	109
a.	a.	k?	a.
Naopak	naopak	k6eAd1	naopak
mimosoustavovou	mimosoustavový	k2eAgFnSc7d1	mimosoustavový
jednotkou	jednotka	k1gFnSc7	jednotka
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
sekunda	sekunda	k1gFnSc1	sekunda
je	být	k5eAaImIp3nS	být
Planckův	Planckův	k2eAgInSc4d1	Planckův
čas	čas	k1gInSc4	čas
(	(	kIx(	(
<g/>
jakožto	jakožto	k8xS	jakožto
jednotka	jednotka	k1gFnSc1	jednotka
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
doby	doba	k1gFnSc2	doba
trvání	trvání	k1gNnSc2	trvání
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	s	k7c7	s
"	"	kIx"	"
<g/>
doba	doba	k1gFnSc1	doba
<g/>
"	"	kIx"	"
v	v	k7c6	v
názvu	název	k1gInSc6	název
neužívá	užívat	k5eNaImIp3nS	užívat
<g/>
;	;	kIx,	;
v	v	k7c6	v
kosmologii	kosmologie	k1gFnSc6	kosmologie
používaný	používaný	k2eAgInSc1d1	používaný
i	i	k8xC	i
pro	pro	k7c4	pro
čas	čas	k1gInSc4	čas
okamžiku	okamžik	k1gInSc2	okamžik
po	po	k7c6	po
velkém	velký	k2eAgInSc6d1	velký
třesku	třesk	k1gInSc6	třesk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
značený	značený	k2eAgInSc1d1	značený
tP	tP	k?	tP
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
obdoby	obdoba	k1gFnSc2	obdoba
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
soustavách	soustava	k1gFnPc6	soustava
přirozených	přirozený	k2eAgFnPc2d1	přirozená
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
stanovené	stanovený	k2eAgFnPc1d1	stanovená
jednotky	jednotka	k1gFnPc1	jednotka
závisejí	záviset	k5eAaImIp3nP	záviset
na	na	k7c4	na
znalosti	znalost	k1gFnPc4	znalost
hodnot	hodnota	k1gFnPc2	hodnota
univerzálních	univerzální	k2eAgFnPc2d1	univerzální
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
konstant	konstanta	k1gFnPc2	konstanta
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
velikost	velikost	k1gFnSc1	velikost
je	být	k5eAaImIp3nS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
experimentálně	experimentálně	k6eAd1	experimentálně
<g/>
.	.	kIx.	.
</s>
<s>
Planckův	Planckův	k2eAgInSc1d1	Planckův
čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
v	v	k7c6	v
teoretické	teoretický	k2eAgFnSc6d1	teoretická
fyzice	fyzika	k1gFnSc6	fyzika
a	a	k8xC	a
kosmologii	kosmologie	k1gFnSc6	kosmologie
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
malou	malý	k2eAgFnSc4d1	malá
přesnost	přesnost	k1gFnSc4	přesnost
není	být	k5eNaImIp3nS	být
však	však	k9	však
v	v	k7c6	v
metrologii	metrologie	k1gFnSc6	metrologie
použitelný	použitelný	k2eAgInSc1d1	použitelný
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
současné	současný	k2eAgFnSc2d1	současná
adjustace	adjustace	k1gFnSc2	adjustace
konstant	konstanta	k1gFnPc2	konstanta
je	být	k5eAaImIp3nS	být
hodnota	hodnota	k1gFnSc1	hodnota
této	tento	k3xDgFnSc2	tento
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
:	:	kIx,	:
tP	tP	k?	tP
=	=	kIx~	=
5,391	[number]	k4	5,391
16	[number]	k4	16
<g/>
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
)	)	kIx)	)
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
44	[number]	k4	44
s.	s.	k?	s.
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
úřad	úřad	k1gInSc1	úřad
pro	pro	k7c4	pro
míry	míra	k1gFnPc4	míra
a	a	k8xC	a
váhy	váha	k1gFnPc4	váha
(	(	kIx(	(
<g/>
BIMP	BIMP	kA	BIMP
<g/>
)	)	kIx)	)
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
přirozené	přirozený	k2eAgFnPc4d1	přirozená
jednotky	jednotka	k1gFnPc4	jednotka
času	čas	k1gInSc2	čas
mnohem	mnohem	k6eAd1	mnohem
přesněji	přesně	k6eAd2	přesně
stanovené	stanovený	k2eAgInPc1d1	stanovený
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s hack="1">
proto	proto	k8xC	proto
pro	pro	k7c4	pro
metrologické	metrologický	k2eAgInPc4d1	metrologický
účely	účel	k1gInPc4	účel
vhodnější	vhodný	k2eAgMnSc1d2	vhodnější
<g/>
)	)	kIx)	)
konstanty	konstanta	k1gFnPc1	konstanta
(	(	kIx(	(
<g/>
ve	v	k7c6	v
vztazích	vztah	k1gInPc6	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ħ	ħ	k?	ħ
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc4	hbar
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
redukovaná	redukovaný	k2eAgFnSc1d1	redukovaná
Planckova	Planckův	k2eAgFnSc1d1	Planckova
konstanta	konstanta	k1gFnSc1	konstanta
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}}	}}	k?	}}
hmotnost	hmotnost	k1gFnSc1	hmotnost
elektronu	elektron	k1gInSc2	elektron
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
}	}	kIx)	}
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
alpha	alpha	k1gFnSc1	alpha
}	}	kIx)	}
konstanta	konstanta	k1gFnSc1	konstanta
jemné	jemný	k2eAgFnSc2d1	jemná
struktury	struktura	k1gFnSc2	struktura
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
přirozená	přirozený	k2eAgFnSc1d1	přirozená
jednotka	jednotka	k1gFnSc1	jednotka
času	čas	k1gInSc2	čas
<g/>
"	"	kIx"	"
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ħ	ħ	k?	ħ
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
tfrac	tfrac	k6eAd1	tfrac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
s	s	k7c7	s
aktuální	aktuální	k2eAgFnSc7d1	aktuální
hodnotou	hodnota	k1gFnSc7	hodnota
1,288	[number]	k4	1,288
088	[number]	k4	088
667	[number]	k4	667
12	[number]	k4	12
<g/>
(	(	kIx(	(
<g/>
58	[number]	k4	58
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
21	[number]	k4	21
s	s	k7c7	s
"	"	kIx"	"
<g/>
atomová	atomový	k2eAgFnSc1d1	atomová
jednotka	jednotka	k1gFnSc1	jednotka
času	čas	k1gInSc2	čas
<g/>
"	"	kIx"	"
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ħ	ħ	k?	ħ
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tfrac	tfrac	k6eAd1	tfrac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hbar	hbar	k1gInSc1	hbar
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
c	c	k0	c
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
s	s	k7c7	s
aktuální	aktuální	k2eAgFnSc7d1	aktuální
hodnotou	hodnota	k1gFnSc7	hodnota
2,418	[number]	k4	2,418
884	[number]	k4	884
326	[number]	k4	326
509	[number]	k4	509
<g/>
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
)	)	kIx)	)
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
17	[number]	k4	17
s	s	k7c7	s
(	(	kIx(	(
<g/>
jednotka	jednotka	k1gFnSc1	jednotka
času	čas	k1gInSc2	čas
Hartreeovy	Hartreeův	k2eAgMnPc4d1	Hartreeův
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Bohrovy	Bohrův	k2eAgFnPc1d1	Bohrova
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
soustavy	soustava	k1gFnPc1	soustava
atomových	atomový	k2eAgFnPc2d1	atomová
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
charakter	charakter	k1gInSc1	charakter
doby	doba	k1gFnSc2	doba
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc1	několik
dalších	další	k2eAgFnPc2d1	další
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
veličin	veličina	k1gFnPc2	veličina
<g/>
.	.	kIx.	.
</s>
<s>
Nejpoužívanější	používaný	k2eAgMnPc1d3	nejpoužívanější
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
perioda	perioda	k1gFnSc1	perioda
<g/>
,	,	kIx,	,
doporučená	doporučený	k2eAgFnSc1d1	Doporučená
značka	značka	k1gFnSc1	značka
T	T	kA	T
<g/>
,	,	kIx,	,
udávající	udávající	k2eAgInSc1d1	udávající
nejkratší	krátký	k2eAgInSc4d3	nejkratší
časový	časový	k2eAgInSc4d1	časový
interval	interval	k1gInSc4	interval
opakování	opakování	k1gNnSc4	opakování
periodického	periodický	k2eAgInSc2d1	periodický
děje	děj	k1gInSc2	děj
<g/>
,	,	kIx,	,
poločas	poločas	k1gInSc1	poločas
přeměny	přeměna	k1gFnSc2	přeměna
<g/>
,	,	kIx,	,
doporučená	doporučený	k2eAgFnSc1d1	Doporučená
značka	značka	k1gFnSc1	značka
T1⁄	T1⁄	k1gFnSc1	T1⁄
<g/>
,	,	kIx,	,
a	a	k8xC	a
střední	střední	k2eAgFnSc1d1	střední
doba	doba	k1gFnSc1	doba
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
doporučená	doporučený	k2eAgFnSc1d1	Doporučená
značka	značka	k1gFnSc1	značka
τ	τ	k?	τ
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
používané	používaný	k2eAgInPc1d1	používaný
v	v	k7c6	v
jaderné	jaderný	k2eAgFnSc6d1	jaderná
fyzice	fyzika	k1gFnSc6	fyzika
jako	jako	k8xC	jako
charakteristiky	charakteristika	k1gFnSc2	charakteristika
nestabilních	stabilní	k2eNgInPc2d1	nestabilní
atomů	atom	k1gInPc2	atom
a	a	k8xC	a
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Zápis	zápis	k1gInSc4	zápis
času	čas	k1gInSc2	čas
stanovují	stanovovat	k5eAaImIp3nP	stanovovat
české	český	k2eAgFnPc1d1	Česká
i	i	k8xC	i
mezinárodní	mezinárodní	k2eAgFnPc1d1	mezinárodní
normy	norma	k1gFnPc1	norma
<g/>
.	.	kIx.	.
</s>
<s>
Hodiny	hodina	k1gFnPc1	hodina
a	a	k8xC	a
minuty	minuta	k1gFnPc1	minuta
se	se	k3xPyFc4	se
standardně	standardně	k6eAd1	standardně
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
extended	extended	k1gMnSc1	extended
form	form	k1gMnSc1	form
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
dvojtečkou	dvojtečka	k1gFnSc7	dvojtečka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
35	[number]	k4	35
<g/>
)	)	kIx)	)
-	-	kIx~	-
většinou	většina	k1gFnSc7	většina
ve	v	k7c6	v
vědeckých	vědecký	k2eAgInPc6d1	vědecký
a	a	k8xC	a
technických	technický	k2eAgInPc6d1	technický
oborech	obor	k1gInPc6	obor
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
počítače	počítač	k1gInSc2	počítač
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jako	jako	k9	jako
desetinná	desetinný	k2eAgFnSc1d1	desetinná
značka	značka	k1gFnSc1	značka
používá	používat	k5eAaImIp3nS	používat
tečka	tečka	k1gFnSc1	tečka
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
nejednoznačnostem	nejednoznačnost	k1gFnPc3	nejednoznačnost
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
pravidla	pravidlo	k1gNnPc1	pravidlo
českého	český	k2eAgInSc2d1	český
pravopisu	pravopis	k1gInSc2	pravopis
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
i	i	k8xC	i
slovenského	slovenský	k2eAgInSc2d1	slovenský
<g/>
)	)	kIx)	)
uvádějí	uvádět	k5eAaImIp3nP	uvádět
(	(	kIx(	(
<g/>
v	v	k7c6	v
jistých	jistý	k2eAgInPc6d1	jistý
případech	případ	k1gInPc6	případ
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
oddělovač	oddělovač	k1gInSc1	oddělovač
tečku	tečka	k1gFnSc4	tečka
(	(	kIx(	(
<g/>
např.	např.	kA	např.
12.35	[number]	k4	12.35
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
používá	používat	k5eAaImIp3nS	používat
spíše	spíše	k9	spíše
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
typografii	typografia	k1gFnSc6	typografia
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
den	den	k1gInSc4	den
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
pomlčkou	pomlčka	k1gFnSc7	pomlčka
<g/>
,	,	kIx,	,
od	od	k7c2	od
hodiny	hodina	k1gFnSc2	hodina
písmenem	písmeno	k1gNnSc7	písmeno
T	T	kA	T
<g/>
,	,	kIx,	,
např.	např.	kA	např.
1982	[number]	k4	1982
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
<g/>
T	T	kA	T
<g/>
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
<g />
.	.	kIx.	.
</s>
<s>
0	[number]	k4	0
<g/>
0	[number]	k4	0
v	v	k7c4	v
poledne	poledne	k1gNnSc4	poledne
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
eventuální	eventuální	k2eAgInPc1d1	eventuální
zlomky	zlomek	k1gInPc1	zlomek
sekundy	sekunda	k1gFnSc2	sekunda
se	se	k3xPyFc4	se
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
desetinnou	desetinný	k2eAgFnSc7d1	desetinná
značkou	značka	k1gFnSc7	značka
(	(	kIx(	(
<g/>
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
tedy	tedy	k9	tedy
čárkou	čárka	k1gFnSc7	čárka
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1982	[number]	k4	1982
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
<g/>
T	T	kA	T
<g/>
12	[number]	k4	12
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0,000	[number]	k4	0,000
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
Brennan	Brennan	k1gInSc1	Brennan
<g/>
,	,	kIx,	,
Kameny	kámen	k1gInPc1	kámen
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1997	[number]	k4	1997
M.	M.	kA	M.
Hajn	Hajn	k1gMnSc1	Hajn
<g/>
,	,	kIx,	,
Základy	základ	k1gInPc1	základ
jemné	jemný	k2eAgFnSc2d1	jemná
mechaniky	mechanika	k1gFnSc2	mechanika
a	a	k8xC	a
hodinářství	hodinářství	k1gNnSc2	hodinářství
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1953	[number]	k4	1953
S.	S.	kA	S.
Hawking	Hawking	k1gInSc1	Hawking
<g/>
,	,	kIx,	,
Stručná	stručný	k2eAgFnSc1d1	stručná
historie	historie	k1gFnSc1	historie
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
S.	S.	kA	S.
Michal	Michal	k1gMnSc1	Michal
<g/>
,	,	kIx,	,
Hodiny	hodina	k1gFnPc1	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1980	[number]	k4	1980
J.	J.	kA	J.
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
,	,	kIx,	,
Čas	čas	k1gInSc1	čas
a	a	k8xC	a
rytmus	rytmus	k1gInSc1	rytmus
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
N.	N.	kA	N.
Máčová	Máčová	k1gFnSc1	Máčová
<g/>
,	,	kIx,	,
Čas	čas	k1gInSc1	čas
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
forma	forma	k1gFnSc1	forma
2012	[number]	k4	2012
Hodiny	hodina	k1gFnPc4	hodina
Universal	Universal	k1gFnSc2	Universal
Time	Tim	k1gInSc2	Tim
(	(	kIx(	(
<g/>
UT	UT	kA	UT
-	-	kIx~	-
univerzální	univerzální	k2eAgInSc4d1	univerzální
čas	čas	k1gInSc4	čas
<g/>
)	)	kIx)	)
Time	Time	k1gFnSc1	Time
management	management	k1gInSc4	management
Geologický	geologický	k2eAgInSc1d1	geologický
čas	čas	k1gInSc1	čas
Rok	rok	k1gInSc1	rok
<g/>
,	,	kIx,	,
měsíc	měsíc	k1gInSc1	měsíc
<g/>
,	,	kIx,	,
den	den	k1gInSc1	den
Kalendář	kalendář	k1gInSc1	kalendář
<g/>
,	,	kIx,	,
Letopočet	letopočet	k1gInSc1	letopočet
Čas	čas	k1gInSc1	čas
(	(	kIx(	(
<g/>
filosofie	filosofie	k1gFnSc1	filosofie
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
čas	čas	k1gInSc1	čas
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
čas	čas	k1gInSc1	čas
<g />
.	.	kIx.	.
</s>
<s>
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Téma	téma	k1gNnSc1	téma
čas	čas	k1gInSc1	čas
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Vše	všechen	k3xTgNnSc1	všechen
o	o	k7c6	o
času	čas	k1gInSc6	čas
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Aktuální	aktuální	k2eAgInSc4d1	aktuální
středoevropský	středoevropský	k2eAgInSc4d1	středoevropský
čas	čas	k1gInSc4	čas
(	(	kIx(	(
<g/>
atomové	atomový	k2eAgFnPc4d1	atomová
hodiny	hodina	k1gFnPc4	hodina
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Aktuální	aktuální	k2eAgInSc4d1	aktuální
středoevropský	středoevropský	k2eAgInSc4d1	středoevropský
čas	čas	k1gInSc4	čas
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Různé	různý	k2eAgInPc1d1	různý
systémy	systém	k1gInPc1	systém
měření	měření	k1gNnSc2	měření
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
mimo	mimo	k7c4	mimo
SI	si	k1gNnSc4	si
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Přestupná	přestupný	k2eAgFnSc1d1	přestupná
sekunda	sekunda	k1gFnSc1	sekunda
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
World	World	k1gMnSc1	World
Clock	Clock	k1gMnSc1	Clock
-	-	kIx~	-
Time	Time	k1gFnSc1	Time
Zones	Zones	k1gMnSc1	Zones
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
UTC	UTC	kA	UTC
<g/>
/	/	kIx~	/
<g/>
TAI	TAI	kA	TAI
Timeserver	Timeserver	k1gInSc1	Timeserver
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Interactive	Interactiv	k1gInSc5	Interactiv
Map	mapa	k1gFnPc2	mapa
of	of	k?	of
World	World	k1gMnSc1	World
Time	Tim	k1gMnSc2	Tim
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
GMT	GMT	kA	GMT
and	and	k?	and
all	all	k?	all
<g />
.	.	kIx.	.
</s>
<s>
other	other	k1gMnSc1	other
timezones	timezones	k1gMnSc1	timezones
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
TimeTicker	TimeTicker	k1gInSc1	TimeTicker
and	and	k?	and
the	the	k?	the
time	time	k6eAd1	time
tickers	tickers	k6eAd1	tickers
<g/>
...	...	k?	...
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Official	Official	k1gInSc1	Official
US	US	kA	US
time	tim	k1gInPc1	tim
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
A	a	k9	a
walk	walk	k6eAd1	walk
through	through	k1gMnSc1	through
Time	Tim	k1gFnSc2	Tim
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Time	Time	k1gFnSc1	Time
Travel	Travel	k1gInSc1	Travel
and	and	k?	and
Multi-Dimensionality	Multi-Dimensionalita	k1gFnSc2	Multi-Dimensionalita
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Time	Time	k1gFnSc1	Time
and	and	k?	and
classical	classicat	k5eAaPmAgMnS	classicat
and	and	k?	and
quantum	quantum	k1gNnSc4	quantum
mechanics	mechanics	k6eAd1	mechanics
<g/>
:	:	kIx,	:
Indeterminacy	Indeterminacy	k1gInPc1	Indeterminacy
vs	vs	k?	vs
<g/>
.	.	kIx.	.
discontinuity	discontinuita	k1gMnSc2	discontinuita
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Time	Time	k1gFnSc1	Time
as	as	k1gInSc1	as
a	a	k8xC	a
universal	universat	k5eAaImAgInS	universat
consequence	consequenec	k1gMnSc4	consequenec
of	of	k?	of
quanta	quant	k1gMnSc4	quant
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Theories	Theories	k1gInSc1	Theories
With	With	k1gMnSc1	With
Problems	Problems	k1gInSc1	Problems
<g/>
:	:	kIx,	:
What	What	k1gInSc1	What
Is	Is	k1gMnSc2	Is
Time	Tim	k1gMnSc2	Tim
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Exploring	Exploring	k1gInSc1	Exploring
the	the	k?	the
Nature	Natur	k1gMnSc5	Natur
of	of	k?	of
Time	Time	k1gFnPc7	Time
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Sean	Sean	k1gMnSc1	Sean
Carroll	Carroll	k1gMnSc1	Carroll
on	on	k3xPp3gMnSc1	on
the	the	k?	the
arrow	arrow	k?	arrow
of	of	k?	of
time	time	k1gInSc1	time
(	(	kIx(	(
<g/>
Part	part	k1gInSc1	part
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
origin	origin	k1gMnSc1	origin
of	of	k?	of
the	the	k?	the
universe	universe	k1gFnSc2	universe
and	and	k?	and
the	the	k?	the
arrow	arrow	k?	arrow
of	of	k?	of
time	time	k1gInSc1	time
<g/>
,	,	kIx,	,
Sean	Sean	k1gNnSc1	Sean
Carroll	Carrolla	k1gFnPc2	Carrolla
<g/>
,	,	kIx,	,
video	video	k1gNnSc4	video
z	z	k7c2	z
přednášky	přednáška	k1gFnSc2	přednáška
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
CHAST	CHAST	kA	CHAST
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
Templeton	Templeton	k1gInSc1	Templeton
<g/>
,	,	kIx,	,
Faculty	Facult	k1gInPc1	Facult
of	of	k?	of
science	scienec	k1gInSc2	scienec
<g/>
,	,	kIx,	,
University	universita	k1gFnPc1	universita
of	of	k?	of
Sydney	Sydney	k1gNnSc1	Sydney
<g/>
,	,	kIx,	,
listopad	listopad	k1gInSc1	listopad
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
TED	Ted	k1gMnSc1	Ted
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Stránka	stránka	k1gFnSc1	stránka
Antiquarian	Antiquariany	k1gInPc2	Antiquariany
Horological	Horological	k1gMnSc2	Horological
Society	societa	k1gFnSc2	societa
GB	GB	kA	GB
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Stránka	stránka	k1gFnSc1	stránka
Chronometrophilia	Chronometrophilium	k1gNnSc2	Chronometrophilium
CH	Ch	kA	Ch
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Stránka	stránka	k1gFnSc1	stránka
Deutsche	Deutsche	k1gNnSc2	Deutsche
Gesellschaft	Gesellschaftum	k1gNnPc2	Gesellschaftum
für	für	k?	für
Chronometrie	chronometrie	k1gFnSc2	chronometrie
D	D	kA	D
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
)	)	kIx)	)
Stránka	stránka	k1gFnSc1	stránka
Association	Association	k1gInSc4	Association
Francaise	Francaise	k1gFnSc2	Francaise
des	des	k1gNnSc2	des
Amateurs	Amateursa	k1gFnPc2	Amateursa
d	d	k?	d
<g/>
́	́	k?	́
<g/>
Horlogerie	Horlogerie	k1gFnSc2	Horlogerie
Ancienne	Ancienn	k1gInSc5	Ancienn
F	F	kA	F
</s>
