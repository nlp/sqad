<s>
Čas	čas	k1gInSc1	čas
a	a	k8xC	a
doba	doba	k1gFnSc1	doba
jsou	být	k5eAaImIp3nP	být
jedny	jeden	k4xCgFnPc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
veličin	veličina	k1gFnPc2	veličina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
bývá	bývat	k5eAaImIp3nS	bývat
užitečné	užitečný	k2eAgNnSc1d1	užitečné
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
čas	čas	k1gInSc1	čas
(	(	kIx(	(
<g/>
čas	čas	k1gInSc1	čas
jedné	jeden	k4xCgFnSc2	jeden
události	událost	k1gFnSc2	událost
<g/>
)	)	kIx)	)
určuje	určovat	k5eAaImIp3nS	určovat
okamžik	okamžik	k1gInSc4	okamžik
události	událost	k1gFnSc2	událost
na	na	k7c6	na
časové	časový	k2eAgFnSc6d1	časová
ose	osa	k1gFnSc6	osa
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
první	první	k4xOgFnSc3	první
souřadnici	souřadnice	k1gFnSc3	souřadnice
časoprostoru	časoprostor	k1gInSc2	časoprostor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
doba	doba	k1gFnSc1	doba
určuje	určovat	k5eAaImIp3nS	určovat
časovou	časový	k2eAgFnSc4d1	časová
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
událostmi	událost	k1gFnPc7	událost
<g/>
,	,	kIx,	,
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
časy	čas	k1gInPc7	čas
dvou	dva	k4xCgNnPc2	dva
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
