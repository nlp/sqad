<s>
Organizace	organizace	k1gFnSc1	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
United	United	k1gInSc1	United
Nations	Nationsa	k1gFnPc2	Nationsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
UN	UN	kA	UN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gMnPc7	jejíž
členy	člen	k1gMnPc7	člen
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc1	všechen
státy	stát	k1gInPc1	stát
světa	svět	k1gInSc2	svět
–	–	k?	–
k	k	k7c3	k
březnu	březen	k1gInSc3	březen
2016	[number]	k4	2016
má	mít	k5eAaImIp3nS	mít
193	[number]	k4	193
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
OSN	OSN	kA	OSN
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1945	[number]	k4	1945
v	v	k7c6	v
San	San	k1gFnSc6	San
Franciscu	Franciscus	k1gInSc2	Franciscus
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
na	na	k7c6	na
základě	základ	k1gInSc6	základ
přijetí	přijetí	k1gNnSc2	přijetí
Charty	charta	k1gFnSc2	charta
OSN	OSN	kA	OSN
50	[number]	k4	50
státy	stát	k1gInPc7	stát
včetně	včetně	k7c2	včetně
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
ČSR	ČSR	kA	ČSR
a	a	k8xC	a
významné	významný	k2eAgFnSc2d1	významná
podpory	podpora	k1gFnSc2	podpora
Rockefellerova	Rockefellerův	k2eAgInSc2d1	Rockefellerův
fondu	fond	k1gInSc2	fond
<g/>
.	.	kIx.	.
</s>
<s>
Nahradila	nahradit	k5eAaPmAgFnS	nahradit
Společnost	společnost	k1gFnSc1	společnost
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jako	jako	k9	jako
garant	garant	k1gMnSc1	garant
kolektivní	kolektivní	k2eAgFnSc2d1	kolektivní
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
a	a	k8xC	a
mírového	mírový	k2eAgNnSc2d1	Mírové
řešení	řešení	k1gNnSc2	řešení
konfliktů	konflikt	k1gInPc2	konflikt
neobstála	obstát	k5eNaPmAgFnS	obstát
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
Valné	valný	k2eAgNnSc1d1	Valné
shromáždění	shromáždění	k1gNnSc1	shromáždění
OSN	OSN	kA	OSN
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1946	[number]	k4	1946
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
OSN	OSN	kA	OSN
je	být	k5eAaImIp3nS	být
zachování	zachování	k1gNnSc4	zachování
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
a	a	k8xC	a
zajištění	zajištění	k1gNnSc2	zajištění
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Členství	členství	k1gNnSc1	členství
v	v	k7c6	v
OSN	OSN	kA	OSN
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
principu	princip	k1gInSc6	princip
suverénní	suverénní	k2eAgFnSc2d1	suverénní
rovnosti	rovnost	k1gFnSc2	rovnost
<g/>
,	,	kIx,	,
státy	stát	k1gInPc1	stát
mají	mít	k5eAaImIp3nP	mít
svá	svůj	k3xOyFgNnPc4	svůj
zastoupení	zastoupení	k1gNnPc4	zastoupení
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
stálé	stálý	k2eAgFnSc2d1	stálá
mise	mise	k1gFnSc2	mise
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
sídle	sídlo	k1gNnSc6	sídlo
OSN	OSN	kA	OSN
New	New	k1gFnSc1	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k6eAd1	také
např.	např.	kA	např.
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
členský	členský	k2eAgInSc1d1	členský
stát	stát	k1gInSc1	stát
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgMnPc4	svůj
zástupce	zástupce	k1gMnPc4	zástupce
ve	v	k7c6	v
Valném	valný	k2eAgNnSc6d1	Valné
shromáždění	shromáždění	k1gNnSc6	shromáždění
a	a	k8xC	a
disponuje	disponovat	k5eAaBmIp3nS	disponovat
jedním	jeden	k4xCgMnSc7	jeden
stejně	stejně	k6eAd1	stejně
platným	platný	k2eAgInSc7d1	platný
hlasem	hlas	k1gInSc7	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Výkonným	výkonný	k2eAgInSc7d1	výkonný
orgánem	orgán	k1gInSc7	orgán
je	být	k5eAaImIp3nS	být
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
přísluší	příslušet	k5eAaImIp3nP	příslušet
základní	základní	k2eAgFnSc4d1	základní
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
za	za	k7c4	za
udržení	udržení	k1gNnSc4	udržení
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
míru	mír	k1gInSc2	mír
a	a	k8xC	a
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gFnSc2	jejíž
rezoluce	rezoluce	k1gFnSc2	rezoluce
jsou	být	k5eAaImIp3nP	být
právně	právně	k6eAd1	právně
závazné	závazný	k2eAgFnPc1d1	závazná
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
hlavními	hlavní	k2eAgInPc7d1	hlavní
orgány	orgán	k1gInPc7	orgán
OSN	OSN	kA	OSN
jsou	být	k5eAaImIp3nP	být
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
a	a	k8xC	a
sociální	sociální	k2eAgFnSc1d1	sociální
rada	rada	k1gFnSc1	rada
OSN	OSN	kA	OSN
-	-	kIx~	-
ECOSOC	ECOSOC	kA	ECOSOC
<g/>
,	,	kIx,	,
Poručenská	poručenský	k2eAgFnSc1d1	Poručenská
rada	rada	k1gFnSc1	rada
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
soudní	soudní	k2eAgInSc1d1	soudní
dvůr	dvůr	k1gInSc1	dvůr
Sekretariát	sekretariát	k1gInSc1	sekretariát
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
OSN	OSN	kA	OSN
dostala	dostat	k5eAaPmAgFnS	dostat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
dříve	dříve	k6eAd2	dříve
stejnou	stejný	k2eAgFnSc4d1	stejná
cenu	cena	k1gFnSc4	cena
dostaly	dostat	k5eAaPmAgInP	dostat
Úřad	úřad	k1gInSc1	úřad
Vysokého	vysoký	k2eAgMnSc2d1	vysoký
komisaře	komisař	k1gMnSc2	komisař
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
uprchlíky	uprchlík	k1gMnPc4	uprchlík
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
a	a	k8xC	a
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dětský	dětský	k2eAgInSc1d1	dětský
fond	fond	k1gInSc1	fond
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mírové	mírový	k2eAgFnPc1d1	mírová
síly	síla	k1gFnPc1	síla
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
(	(	kIx(	(
<g/>
RB	RB	kA	RB
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
15	[number]	k4	15
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Charta	charta	k1gFnSc1	charta
OSN	OSN	kA	OSN
určuje	určovat	k5eAaImIp3nS	určovat
5	[number]	k4	5
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jako	jako	k9	jako
stálé	stálý	k2eAgNnSc1d1	stálé
<g/>
,	,	kIx,	,
Valné	valný	k2eAgNnSc1d1	Valné
shromáždění	shromáždění	k1gNnSc1	shromáždění
volí	volit	k5eAaImIp3nS	volit
10	[number]	k4	10
dalších	další	k1gNnPc2	další
za	za	k7c4	za
členy	člen	k1gInPc4	člen
nestálé	stálý	k2eNgInPc4d1	nestálý
na	na	k7c4	na
dvouleté	dvouletý	k2eAgNnSc4d1	dvouleté
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Stálými	stálý	k2eAgMnPc7d1	stálý
členy	člen	k1gMnPc7	člen
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
.	.	kIx.	.
10	[number]	k4	10
nestálých	stálý	k2eNgInPc2d1	nestálý
členů	člen	k1gInPc2	člen
(	(	kIx(	(
<g/>
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
rok	rok	k1gInSc4	rok
ukončení	ukončení	k1gNnSc2	ukončení
mandátu	mandát	k1gInSc2	mandát
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Angola	Angola	k1gFnSc1	Angola
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Egypt	Egypt	k1gInSc1	Egypt
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Malajsie	Malajsie	k1gFnSc1	Malajsie
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Senegal	Senegal	k1gInSc1	Senegal
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Uruguay	Uruguay	k1gFnSc1	Uruguay
(	(	kIx(	(
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Venezuela	Venezuela	k1gFnSc1	Venezuela
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Česko	Česko	k1gNnSc1	Česko
bylo	být	k5eAaImAgNnS	být
členem	člen	k1gInSc7	člen
rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
naposledy	naposledy	k6eAd1	naposledy
v	v	k7c6	v
letech	let	k1gInPc6	let
1994	[number]	k4	1994
–	–	k?	–
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
hostila	hostit	k5eAaImAgFnS	hostit
57	[number]	k4	57
<g/>
.	.	kIx.	.
zasedání	zasedání	k1gNnSc2	zasedání
Valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
,	,	kIx,	,
kterému	který	k3yQgMnSc3	který
předsedal	předsedat	k5eAaImAgMnS	předsedat
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
předseda	předseda	k1gMnSc1	předseda
Valného	valný	k2eAgNnSc2d1	Valné
shromáždění	shromáždění	k1gNnSc2	shromáždění
OSN	OSN	kA	OSN
Jan	Jan	k1gMnSc1	Jan
Kavan	Kavan	k1gMnSc1	Kavan
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
je	být	k5eAaImIp3nS	být
také	také	k9	také
předsednictví	předsednictví	k1gNnSc1	předsednictví
Hospodářské	hospodářský	k2eAgNnSc1d1	hospodářské
a	a	k8xC	a
sociální	sociální	k2eAgFnSc3d1	sociální
radě	rada	k1gFnSc3	rada
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
a	a	k8xC	a
sociální	sociální	k2eAgFnSc1d1	sociální
rada	rada	k1gFnSc1	rada
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
a	a	k8xC	a
sociální	sociální	k2eAgFnSc1d1	sociální
rada	rada	k1gFnSc1	rada
(	(	kIx(	(
<g/>
ECOSOC	ECOSOC	kA	ECOSOC
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
54	[number]	k4	54
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
volených	volený	k2eAgMnPc2d1	volený
na	na	k7c4	na
tříleté	tříletý	k2eAgNnSc4d1	tříleté
období	období	k1gNnSc4	období
Valným	valný	k2eAgNnSc7d1	Valné
shromážděním	shromáždění	k1gNnSc7	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
členského	členský	k2eAgInSc2d1	členský
státu	stát	k1gInSc2	stát
končí	končit	k5eAaImIp3nS	končit
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
posledního	poslední	k2eAgInSc2d1	poslední
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Poručenská	poručenský	k2eAgFnSc1d1	Poručenská
rada	rada	k1gFnSc1	rada
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Poručenská	poručenský	k2eAgFnSc1d1	Poručenská
rada	rada	k1gFnSc1	rada
má	mít	k5eAaImIp3nS	mít
5	[number]	k4	5
členů	člen	k1gInPc2	člen
<g/>
:	:	kIx,	:
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
formálně	formálně	k6eAd1	formálně
ukončila	ukončit	k5eAaPmAgFnS	ukončit
činnost	činnost	k1gFnSc1	činnost
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nezávislým	závislý	k2eNgInSc7d1	nezávislý
státem	stát	k1gInSc7	stát
a	a	k8xC	a
členem	člen	k1gInSc7	člen
OSN	OSN	kA	OSN
stalo	stát	k5eAaPmAgNnS	stát
Palau	Palaus	k1gInSc2	Palaus
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgNnSc1d1	poslední
území	území	k1gNnSc1	území
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Rezolucí	rezoluce	k1gFnSc7	rezoluce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
ten	ten	k3xDgInSc4	ten
den	den	k1gInSc4	den
přijata	přijmout	k5eAaPmNgFnS	přijmout
<g/>
,	,	kIx,	,
pozměnila	pozměnit	k5eAaPmAgFnS	pozměnit
Rada	rada	k1gFnSc1	rada
svůj	svůj	k3xOyFgInSc4	svůj
jednací	jednací	k2eAgInSc4d1	jednací
řád	řád	k1gInSc4	řád
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nebude	být	k5eNaImBp3nS	být
scházet	scházet	k5eAaImF	scházet
každoročně	každoročně	k6eAd1	každoročně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
situace	situace	k1gFnSc1	situace
vyžádá	vyžádat	k5eAaPmIp3nS	vyžádat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
svolání	svolání	k1gNnSc6	svolání
může	moct	k5eAaImIp3nS	moct
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
předseda	předseda	k1gMnSc1	předseda
nebo	nebo	k8xC	nebo
pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
vyžádá	vyžádat	k5eAaPmIp3nS	vyžádat
většina	většina	k1gFnSc1	většina
členů	člen	k1gMnPc2	člen
Poručenské	poručenský	k2eAgFnSc2d1	Poručenská
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
VS	VS	kA	VS
nebo	nebo	k8xC	nebo
RB	RB	kA	RB
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
soudní	soudní	k2eAgInSc1d1	soudní
dvůr	dvůr	k1gInSc1	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
soudní	soudní	k2eAgInSc1d1	soudní
dvůr	dvůr	k1gInSc1	dvůr
má	mít	k5eAaImIp3nS	mít
sídlo	sídlo	k1gNnSc4	sídlo
v	v	k7c6	v
Haagu	Haag	k1gInSc6	Haag
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
15	[number]	k4	15
členů	člen	k1gInPc2	člen
volených	volený	k2eAgInPc2d1	volený
Valným	valný	k2eAgNnSc7d1	Valné
shromážděním	shromáždění	k1gNnSc7	shromáždění
a	a	k8xC	a
Radou	rada	k1gFnSc7	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
<s>
Soudci	soudce	k1gMnPc1	soudce
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
na	na	k7c4	na
9	[number]	k4	9
<g/>
leté	letý	k2eAgNnSc4d1	leté
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
končí	končit	k5eAaImIp3nS	končit
dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
za	za	k7c7	za
jmény	jméno	k1gNnPc7	jméno
soudců	soudce	k1gMnPc2	soudce
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
volbě	volba	k1gFnSc6	volba
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
zohledněna	zohledněn	k2eAgFnSc1d1	zohledněna
zásada	zásada	k1gFnSc1	zásada
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
zastoupeny	zastoupit	k5eAaPmNgInP	zastoupit
všechny	všechen	k3xTgInPc1	všechen
regiony	region	k1gInPc1	region
světa	svět	k1gInSc2	svět
a	a	k8xC	a
také	také	k9	také
jednotlivé	jednotlivý	k2eAgFnSc2d1	jednotlivá
právní	právní	k2eAgFnSc2d1	právní
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Soud	soud	k1gInSc1	soud
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
statutem	statut	k1gInSc7	statut
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Chartou	charta	k1gFnSc7	charta
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
soudního	soudní	k2eAgInSc2d1	soudní
dvora	dvůr	k1gInSc2	dvůr
jsou	být	k5eAaImIp3nP	být
závazná	závazný	k2eAgNnPc1d1	závazné
a	a	k8xC	a
definitivní	definitivní	k2eAgNnPc1d1	definitivní
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
projednávat	projednávat	k5eAaImF	projednávat
spory	spor	k1gInPc4	spor
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
předloží	předložit	k5eAaPmIp3nS	předložit
sporové	sporový	k2eAgFnPc4d1	sporový
strany	strana	k1gFnPc4	strana
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
spory	spor	k1gInPc1	spor
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
přiznaly	přiznat	k5eAaPmAgFnP	přiznat
jeho	jeho	k3xOp3gFnSc4	jeho
generální	generální	k2eAgFnSc4d1	generální
příslušnost	příslušnost	k1gFnSc4	příslušnost
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgNnSc1d1	současné
složení	složení	k1gNnSc1	složení
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
soudního	soudní	k2eAgInSc2d1	soudní
dvora	dvůr	k1gInSc2	dvůr
(	(	kIx(	(
<g/>
k	k	k7c3	k
červnu	červen	k1gInSc3	červen
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Ronny	Ronna	k1gFnSc2	Ronna
Abraham	Abraham	k1gMnSc1	Abraham
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Abdulqawi	Abdulqaw	k1gMnPc1	Abdulqaw
Ahmed	Ahmed	k1gMnSc1	Ahmed
Yusuf	Yusuf	k1gMnSc1	Yusuf
<g/>
,	,	kIx,	,
Somálsko	Somálsko	k1gNnSc1	Somálsko
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Christopher	Christophra	k1gFnPc2	Christophra
Greenwood	Greenwood	k1gInSc1	Greenwood
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Xue	Xue	k1gMnSc1	Xue
Hanqin	Hanqin	k1gMnSc1	Hanqin
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Hisashi	Hisashi	k1gNnSc1	Hisashi
Owada	Owada	k1gFnSc1	Owada
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Peter	Peter	k1gMnSc1	Peter
Tomka	Tomek	k1gMnSc2	Tomek
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Joan	Joan	k1gInSc1	Joan
E.	E.	kA	E.
Donoghue	Donoghue	k1gInSc1	Donoghue
<g/>
,	,	kIx,	,
USA	USA	kA	USA
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Mohamed	Mohamed	k1gMnSc1	Mohamed
Bennouna	Bennoun	k1gMnSc2	Bennoun
<g/>
,	,	kIx,	,
Maroko	Maroko	k1gNnSc1	Maroko
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Antônio	Antônia	k1gMnSc5	Antônia
Augusto	Augusta	k1gMnSc5	Augusta
Cançado	Cançada	k1gFnSc5	Cançada
Trindade	Trindad	k1gInSc5	Trindad
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Giorgio	Giorgio	k1gMnSc1	Giorgio
Gaja	Gaja	k1gMnSc1	Gaja
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Julia	Julius	k1gMnSc2	Julius
Sebutinde	Sebutind	k1gInSc5	Sebutind
<g/>
,	,	kIx,	,
Uganda	Uganda	k1gFnSc1	Uganda
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Dalveer	Dalveer	k1gInSc1	Dalveer
Bhandari	Bhandar	k1gFnSc2	Bhandar
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc2	Indie
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Patrick	Patrick	k1gMnSc1	Patrick
Lipton	Lipton	k1gInSc4	Lipton
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
,	,	kIx,	,
Jamajka	Jamajka	k1gFnSc1	Jamajka
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
James	James	k1gMnSc1	James
Richard	Richard	k1gMnSc1	Richard
Crawford	Crawford	k1gMnSc1	Crawford
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
Kirill	Kirill	k1gMnSc1	Kirill
Gevorgian	Gevorgian	k1gMnSc1	Gevorgian
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
soudního	soudní	k2eAgInSc2d1	soudní
dvora	dvůr	k1gInSc2	dvůr
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
soudce	soudce	k1gMnSc2	soudce
Ronny	Ronna	k1gMnSc2	Ronna
Abraham	Abraham	k1gMnSc1	Abraham
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
a	a	k8xC	a
místopředsedou	místopředseda	k1gMnSc7	místopředseda
soudce	soudce	k1gMnSc2	soudce
Abdulqawi	Abdulqawi	k1gNnSc2	Abdulqawi
Ahmed	Ahmed	k1gMnSc1	Ahmed
Yusuf	Yusuf	k1gMnSc1	Yusuf
(	(	kIx(	(
<g/>
Somálsko	Somálsko	k1gNnSc1	Somálsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Udržitelný	udržitelný	k2eAgInSc4d1	udržitelný
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
OSN	OSN	kA	OSN
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
činitelem	činitel	k1gInSc7	činitel
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
udržitelného	udržitelný	k2eAgInSc2d1	udržitelný
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Rozvojové	rozvojový	k2eAgInPc4d1	rozvojový
cíle	cíl	k1gInPc4	cíl
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
<g/>
.	.	kIx.	.
</s>
<s>
Cíle	Cíla	k1gFnSc3	Cíla
OSN	OSN	kA	OSN
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
:	:	kIx,	:
snížit	snížit	k5eAaPmF	snížit
chudobu	chudoba	k1gFnSc4	chudoba
a	a	k8xC	a
sociální	sociální	k2eAgNnSc4d1	sociální
vyloučení	vyloučení	k1gNnSc4	vyloučení
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
univerzálního	univerzální	k2eAgNnSc2d1	univerzální
primárního	primární	k2eAgNnSc2d1	primární
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
prosazovat	prosazovat	k5eAaImF	prosazovat
rovnost	rovnost	k1gFnSc4	rovnost
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
poskytovat	poskytovat	k5eAaImF	poskytovat
ženám	žena	k1gFnPc3	žena
více	hodně	k6eAd2	hodně
možností	možnost	k1gFnSc7	možnost
prosadit	prosadit	k5eAaPmF	prosadit
se	se	k3xPyFc4	se
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
snížit	snížit	k5eAaPmF	snížit
dětskou	dětský	k2eAgFnSc4d1	dětská
úmrtnost	úmrtnost	k1gFnSc4	úmrtnost
zlepšit	zlepšit	k5eAaPmF	zlepšit
zdraví	zdraví	k1gNnSc4	zdraví
matek	matka	k1gFnPc2	matka
boj	boj	k1gInSc4	boj
s	s	k7c7	s
HIV	HIV	kA	HIV
<g/>
/	/	kIx~	/
<g/>
AIDS	AIDS	kA	AIDS
<g/>
,	,	kIx,	,
malárií	malárie	k1gFnSc7	malárie
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
nemocemi	nemoc	k1gFnPc7	nemoc
zajistit	zajistit	k5eAaPmF	zajistit
<g />
.	.	kIx.	.
</s>
<s>
environmentální	environmentální	k2eAgFnSc1d1	environmentální
udržitelnost	udržitelnost	k1gFnSc1	udržitelnost
vybudovat	vybudovat	k5eAaPmF	vybudovat
globální	globální	k2eAgNnSc4d1	globální
partnerství	partnerství	k1gNnSc4	partnerství
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
udržovat	udržovat	k5eAaImF	udržovat
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
mír	mír	k1gInSc4	mír
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
OSN	OSN	kA	OSN
má	mít	k5eAaImIp3nS	mít
řadu	řada	k1gFnSc4	řada
odborných	odborný	k2eAgFnPc2d1	odborná
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
<g/>
:	:	kIx,	:
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
organizace	organizace	k1gFnSc1	organizace
práce	práce	k1gFnSc2	práce
(	(	kIx(	(
<g/>
ILO	ILO	kA	ILO
<g/>
)	)	kIx)	)
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
civilní	civilní	k2eAgNnSc4d1	civilní
letectví	letectví	k1gNnSc4	letectví
(	(	kIx(	(
<g/>
ICAO	ICAO	kA	ICAO
<g/>
)	)	kIx)	)
Organizace	organizace	k1gFnSc2	organizace
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
,	,	kIx,	,
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
kulturu	kultura	k1gFnSc4	kultura
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
UNESCO	Unesco	k1gNnSc1	Unesco
<g/>
)	)	kIx)	)
Organizace	organizace	k1gFnSc1	organizace
pro	pro	k7c4	pro
výživu	výživa	k1gFnSc4	výživa
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc4	zemědělství
(	(	kIx(	(
<g/>
FAO	FAO	kA	FAO
<g/>
)	)	kIx)	)
Světová	světový	k2eAgFnSc1d1	světová
meteorologická	meteorologický	k2eAgFnSc1d1	meteorologická
organizace	organizace	k1gFnSc1	organizace
(	(	kIx(	(
<g/>
WMO	WMO	kA	WMO
<g/>
)	)	kIx)	)
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
(	(	kIx(	(
<g/>
WHO	WHO	kA	WHO
<g/>
)	)	kIx)	)
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
měnový	měnový	k2eAgInSc1d1	měnový
fond	fond	k1gInSc1	fond
(	(	kIx(	(
<g/>
MMF	MMF	kA	MMF
<g/>
)	)	kIx)	)
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
atomovou	atomový	k2eAgFnSc4d1	atomová
energii	energie	k1gFnSc4	energie
(	(	kIx(	(
<g/>
MAAE	MAAE	kA	MAAE
<g/>
)	)	kIx)	)
Světová	světový	k2eAgFnSc1d1	světová
banka	banka	k1gFnSc1	banka
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
WB	WB	kA	WB
<g/>
)	)	kIx)	)
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
banka	banka	k1gFnSc1	banka
pro	pro	k7c4	pro
obnovu	obnova	k1gFnSc4	obnova
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
(	(	kIx(	(
<g/>
IBRD	IBRD	kA	IBRD
<g/>
)	)	kIx)	)
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
asociace	asociace	k1gFnSc1	asociace
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
(	(	kIx(	(
<g/>
IDA	ido	k1gNnSc2	ido
<g/>
)	)	kIx)	)
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
fond	fond	k1gInSc1	fond
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
zemědělství	zemědělství	k1gNnSc2	zemědělství
(	(	kIx(	(
<g/>
IFAD	IFAD	kA	IFAD
<g/>
)	)	kIx)	)
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
finanční	finanční	k2eAgFnSc1d1	finanční
korporace	korporace	k1gFnSc1	korporace
(	(	kIx(	(
<g/>
IFC	IFC	kA	IFC
<g/>
)	)	kIx)	)
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
námořní	námořní	k2eAgFnSc2d1	námořní
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
IMO	IMO	kA	IMO
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
výzkumný	výzkumný	k2eAgInSc1d1	výzkumný
a	a	k8xC	a
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
ústav	ústav	k1gInSc1	ústav
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
žen	žena	k1gFnPc2	žena
(	(	kIx(	(
<g/>
INSTRAW	INSTRAW	kA	INSTRAW
<g/>
)	)	kIx)	)
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
telekomunikační	telekomunikační	k2eAgFnSc2d1	telekomunikační
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
ITU	ITU	kA	ITU
<g/>
)	)	kIx)	)
Centrum	centrum	k1gNnSc1	centrum
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
obydlí	obydlí	k1gNnPc4	obydlí
(	(	kIx(	(
<g/>
Habitat	Habitat	k1gFnSc1	Habitat
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
UNCHS	UNCHS	kA	UNCHS
<g/>
)	)	kIx)	)
Konference	konference	k1gFnSc1	konference
OSN	OSN	kA	OSN
o	o	k7c6	o
obchodu	obchod	k1gInSc6	obchod
a	a	k8xC	a
rozvoji	rozvoj	k1gInSc6	rozvoj
(	(	kIx(	(
<g/>
UNCTAD	UNCTAD	kA	UNCTAD
<g/>
)	)	kIx)	)
Pozorovatelská	pozorovatelský	k2eAgFnSc1d1	pozorovatelská
<g />
.	.	kIx.	.
</s>
<s>
mise	mise	k1gFnSc1	mise
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
uvolňování	uvolňování	k1gNnSc4	uvolňování
napětí	napětí	k1gNnSc2	napětí
(	(	kIx(	(
<g/>
UNDOF	UNDOF	kA	UNDOF
<g/>
)	)	kIx)	)
Rozvojový	rozvojový	k2eAgInSc1d1	rozvojový
program	program	k1gInSc1	program
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
UNDP	UNDP	kA	UNDP
<g/>
)	)	kIx)	)
Program	program	k1gInSc1	program
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
(	(	kIx(	(
<g/>
UNEP	UNEP	kA	UNEP
<g/>
)	)	kIx)	)
Ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
OSN	OSN	kA	OSN
na	na	k7c6	na
Kypru	Kypr	k1gInSc6	Kypr
(	(	kIx(	(
<g/>
UNFICYP	UNFICYP	kA	UNFICYP
<g/>
)	)	kIx)	)
Populační	populační	k2eAgInSc1d1	populační
fond	fond	k1gInSc1	fond
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
UNFPA	UNFPA	kA	UNFPA
<g/>
)	)	kIx)	)
Mise	mise	k1gFnSc1	mise
OSN	OSN	kA	OSN
v	v	k7c4	v
Kosovu	Kosův	k2eAgFnSc4d1	Kosova
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
UNMIK	UNMIK	kA	UNMIK
<g/>
)	)	kIx)	)
Mise	mise	k1gFnSc1	mise
dobrovolné	dobrovolný	k2eAgFnSc2d1	dobrovolná
pomoci	pomoc	k1gFnSc2	pomoc
OSN	OSN	kA	OSN
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
a	a	k8xC	a
Pákistánu	Pákistán	k1gInSc6	Pákistán
(	(	kIx(	(
<g/>
UNGOMAP	UNGOMAP	kA	UNGOMAP
<g/>
)	)	kIx)	)
Vysoký	vysoký	k2eAgMnSc1d1	vysoký
komisař	komisař	k1gMnSc1	komisař
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
pro	pro	k7c4	pro
uprchlíky	uprchlík	k1gMnPc4	uprchlík
(	(	kIx(	(
<g/>
UNHCR	UNHCR	kA	UNHCR
<g/>
)	)	kIx)	)
Dětský	dětský	k2eAgInSc1d1	dětský
fond	fond	k1gInSc1	fond
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
UNICEF	UNICEF	kA	UNICEF
<g/>
)	)	kIx)	)
Organizace	organizace	k1gFnSc2	organizace
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
rozvoj	rozvoj	k1gInSc4	rozvoj
(	(	kIx(	(
<g/>
UNIDO	UNIDO	kA	UNIDO
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Prozatímní	prozatímní	k2eAgFnPc1d1	prozatímní
jednotky	jednotka	k1gFnPc1	jednotka
OSN	OSN	kA	OSN
v	v	k7c6	v
Libanonu	Libanon	k1gInSc6	Libanon
(	(	kIx(	(
<g/>
UNIFIL	UNIFIL	kA	UNIFIL
<g/>
)	)	kIx)	)
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
pozorovatelská	pozorovatelský	k2eAgFnSc1d1	pozorovatelská
skupina	skupina	k1gFnSc1	skupina
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
Írán	Írán	k1gInSc4	Írán
a	a	k8xC	a
Irák	Irák	k1gInSc1	Irák
(	(	kIx(	(
<g/>
UNIIMOG	UNIIMOG	kA	UNIIMOG
<g/>
)	)	kIx)	)
Institut	institut	k1gInSc1	institut
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
výcvik	výcvik	k1gInSc4	výcvik
a	a	k8xC	a
výzkum	výzkum	k1gInSc4	výzkum
(	(	kIx(	(
<g/>
UNITAR	UNITAR	kA	UNITAR
<g/>
)	)	kIx)	)
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
pozorovatelská	pozorovatelský	k2eAgFnSc1d1	pozorovatelská
skupina	skupina	k1gFnSc1	skupina
OSN	OSN	kA	OSN
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
a	a	k8xC	a
Pákistánu	Pákistán	k1gInSc6	Pákistán
(	(	kIx(	(
<g/>
UNMOGIP	UNMOGIP	kA	UNMOGIP
<g/>
)	)	kIx)	)
Pozorovatelská	pozorovatelský	k2eAgFnSc1d1	pozorovatelská
skupina	skupina	k1gFnSc1	skupina
OSN	OSN	kA	OSN
<g />
.	.	kIx.	.
</s>
<s>
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
(	(	kIx(	(
<g/>
UNOGCA	UNOGCA	kA	UNOGCA
<g/>
)	)	kIx)	)
Pozorovatelská	pozorovatelský	k2eAgFnSc1d1	pozorovatelská
skupina	skupina	k1gFnSc1	skupina
OSN	OSN	kA	OSN
v	v	k7c6	v
Libanonu	Libanon	k1gInSc6	Libanon
(	(	kIx(	(
<g/>
UNOGIL	UNOGIL	kA	UNOGIL
<g/>
)	)	kIx)	)
Pozorovatelská	pozorovatelský	k2eAgFnSc1d1	pozorovatelská
mise	mise	k1gFnSc1	mise
OSN	OSN	kA	OSN
v	v	k7c6	v
Ugandě	Uganda	k1gFnSc6	Uganda
a	a	k8xC	a
Rwandě	Rwanda	k1gFnSc6	Rwanda
(	(	kIx(	(
<g/>
UNOMUR	UNOMUR	kA	UNOMUR
<g/>
)	)	kIx)	)
Ochranné	ochranný	k2eAgFnSc2d1	ochranná
a	a	k8xC	a
stabilizační	stabilizační	k2eAgFnSc2d1	stabilizační
síly	síla	k1gFnSc2	síla
OSN	OSN	kA	OSN
v	v	k7c6	v
bývalé	bývalý	k2eAgFnSc6d1	bývalá
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
(	(	kIx(	(
<g/>
UNPROFOR	UNPROFOR	kA	UNPROFOR
<g/>
,	,	kIx,	,
IFOR	IFOR	kA	IFOR
<g/>
,	,	kIx,	,
SFOR	SFOR	kA	SFOR
<g/>
,	,	kIx,	,
KFOR	KFOR	kA	KFOR
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Úřad	úřad	k1gInSc1	úřad
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
palestinské	palestinský	k2eAgMnPc4d1	palestinský
uprchlíky	uprchlík	k1gMnPc4	uprchlík
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
(	(	kIx(	(
<g/>
UNRWA	UNRWA	kA	UNRWA
<g/>
)	)	kIx)	)
Dozorčí	dozorčí	k2eAgFnSc2d1	dozorčí
organizace	organizace	k1gFnSc2	organizace
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
dodržování	dodržování	k1gNnSc4	dodržování
příměří	příměří	k1gNnSc2	příměří
(	(	kIx(	(
<g/>
UNTSO	UNTSO	kA	UNTSO
<g/>
)	)	kIx)	)
Univerzita	univerzita	k1gFnSc1	univerzita
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
UNU	UNU	kA	UNU
<g/>
)	)	kIx)	)
Světová	světový	k2eAgFnSc1d1	světová
poštovní	poštovní	k2eAgFnSc1d1	poštovní
unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g/>
UPU	UPU	kA	UPU
<g/>
)	)	kIx)	)
Rada	rada	k1gFnSc1	rada
pro	pro	k7c4	pro
světovou	světový	k2eAgFnSc4d1	světová
výživu	výživa	k1gFnSc4	výživa
(	(	kIx(	(
<g/>
WFC	WFC	kA	WFC
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Rozvojový	rozvojový	k2eAgInSc1d1	rozvojový
fond	fond	k1gInSc1	fond
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
(	(	kIx(	(
<g/>
UNIFEM	UNIFEM	kA	UNIFEM
<g/>
)	)	kIx)	)
Světový	světový	k2eAgInSc1d1	světový
potravinový	potravinový	k2eAgInSc1d1	potravinový
program	program	k1gInSc1	program
(	(	kIx(	(
<g/>
WFP	WFP	kA	WFP
<g/>
)	)	kIx)	)
Světová	světový	k2eAgFnSc1d1	světová
organizace	organizace	k1gFnSc1	organizace
duševního	duševní	k2eAgNnSc2d1	duševní
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
(	(	kIx(	(
<g/>
WIPO	WIPO	kA	WIPO
<g/>
)	)	kIx)	)
Švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
poštovní	poštovní	k2eAgFnSc1d1	poštovní
správa	správa	k1gFnSc1	správa
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
Evropského	evropský	k2eAgInSc2d1	evropský
úřadu	úřad	k1gInSc2	úřad
OSN	OSN	kA	OSN
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
opatřila	opatřit	k5eAaPmAgFnS	opatřit
část	část	k1gFnSc1	část
emisí	emise	k1gFnPc2	emise
švýcarských	švýcarský	k2eAgFnPc2d1	švýcarská
poštovních	poštovní	k2eAgFnPc2d1	poštovní
známek	známka	k1gFnPc2	známka
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
přetiskem	přetisk	k1gInSc7	přetisk
NATIONS	NATIONS	kA	NATIONS
UNIES	UNIES	kA	UNIES
OFFICE	Office	kA	Office
EUROPÉEN	EUROPÉEN	kA	EUROPÉEN
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
vydala	vydat	k5eAaPmAgFnS	vydat
speciální	speciální	k2eAgFnPc4d1	speciální
emise	emise	k1gFnPc4	emise
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
nikoli	nikoli	k9	nikoli
přetisk	přetisk	k1gInSc1	přetisk
<g/>
)	)	kIx)	)
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
NATIONS	NATIONS	kA	NATIONS
UNIÉS	UNIÉS	kA	UNIÉS
a	a	k8xC	a
HELVETIA	Helvetia	k1gFnSc1	Helvetia
<g/>
,	,	kIx,	,
v	v	k7c6	v
švýcarské	švýcarský	k2eAgFnSc6d1	švýcarská
měně	měna	k1gFnSc6	měna
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
další	další	k2eAgInPc4d1	další
4	[number]	k4	4
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
byly	být	k5eAaImAgFnP	být
vydávány	vydávat	k5eAaImNgFnP	vydávat
první	první	k4xOgFnPc1	první
emise	emise	k1gFnPc1	emise
známek	známka	k1gFnPc2	známka
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
i	i	k8xC	i
nadále	nadále	k6eAd1	nadále
se	s	k7c7	s
švýcarskou	švýcarský	k2eAgFnSc7d1	švýcarská
měnou	měna	k1gFnSc7	měna
<g/>
.	.	kIx.	.
</s>
<s>
Model	model	k1gInSc1	model
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
středoškolské	středoškolský	k2eAgMnPc4d1	středoškolský
a	a	k8xC	a
vysokoškolské	vysokoškolský	k2eAgMnPc4d1	vysokoškolský
studenty	student	k1gMnPc4	student
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
vzdělávacím	vzdělávací	k2eAgInSc7d1	vzdělávací
projektem	projekt	k1gInSc7	projekt
Asociace	asociace	k1gFnSc2	asociace
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
otázky	otázka	k1gFnPc4	otázka
(	(	kIx(	(
<g/>
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Pražského	pražský	k2eAgInSc2d1	pražský
studentského	studentský	k2eAgInSc2d1	studentský
summitu	summit	k1gInSc2	summit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
každoročně	každoročně	k6eAd1	každoročně
od	od	k7c2	od
září	září	k1gNnSc2	září
do	do	k7c2	do
března	březen	k1gInSc2	březen
na	na	k7c6	na
Vysoké	vysoký	k2eAgFnSc6d1	vysoká
škole	škola	k1gFnSc6	škola
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgInPc1d1	podobný
modely	model	k1gInPc1	model
jsou	být	k5eAaImIp3nP	být
vytvářeny	vytvářit	k5eAaPmNgInP	vytvářit
i	i	k9	i
na	na	k7c6	na
regionální	regionální	k2eAgFnSc6d1	regionální
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Debatní	debatní	k2eAgFnSc1d1	debatní
liga	liga	k1gFnSc1	liga
Hodonín	Hodonín	k1gInSc1	Hodonín
<g/>
,	,	kIx,	,
Plzeňská	plzeňský	k2eAgFnSc1d1	Plzeňská
diplomatická	diplomatický	k2eAgFnSc1d1	diplomatická
simulace	simulace	k1gFnSc1	simulace
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
léta	léto	k1gNnSc2	léto
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
předsedá	předsedat	k5eAaImIp3nS	předsedat
Radě	rada	k1gFnSc3	rada
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
blízkovýchodní	blízkovýchodní	k2eAgFnSc2d1	blízkovýchodní
monarchie	monarchie	k1gFnSc2	monarchie
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
za	za	k7c4	za
hrubé	hrubý	k2eAgNnSc4d1	hrubé
porušování	porušování	k1gNnSc4	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
je	být	k5eAaImIp3nS	být
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
za	za	k7c4	za
exkluzivní	exkluzivní	k2eAgInSc4d1	exkluzivní
klub	klub	k1gInSc4	klub
stálých	stálý	k2eAgInPc2d1	stálý
členů	člen	k1gInPc2	člen
a	a	k8xC	a
právo	právo	k1gNnSc4	právo
veta	veto	k1gNnSc2	veto
<g/>
.	.	kIx.	.
</s>
<s>
Charta	charta	k1gFnSc1	charta
Spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
dává	dávat	k5eAaImIp3nS	dávat
Radě	rada	k1gFnSc3	rada
legislativní	legislativní	k2eAgFnSc1d1	legislativní
<g/>
,	,	kIx,	,
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
i	i	k8xC	i
soudní	soudní	k2eAgFnSc4d1	soudní
pravomoc	pravomoc	k1gFnSc4	pravomoc
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
není	být	k5eNaImIp3nS	být
OSN	OSN	kA	OSN
s	s	k7c7	s
to	ten	k3xDgNnSc4	ten
zabránit	zabránit	k5eAaPmF	zabránit
konfliktům	konflikt	k1gInPc3	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
skandály	skandál	k1gInPc4	skandál
kolem	kolem	k7c2	kolem
programu	program	k1gInSc2	program
Ropa	ropa	k1gFnSc1	ropa
za	za	k7c2	za
potraviny	potravina	k1gFnSc2	potravina
<g/>
,	,	kIx,	,
dětské	dětský	k2eAgFnSc2d1	dětská
prostituce	prostituce	k1gFnSc2	prostituce
či	či	k8xC	či
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
obtěžování	obtěžování	k1gNnSc2	obtěžování
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
jsou	být	k5eAaImIp3nP	být
organizace	organizace	k1gFnPc1	organizace
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Mezivládní	mezivládní	k2eAgInSc1d1	mezivládní
panel	panel	k1gInSc1	panel
pro	pro	k7c4	pro
změny	změna	k1gFnPc4	změna
klimatu	klima	k1gNnSc2	klima
(	(	kIx(	(
<g/>
IPCC	IPCC	kA	IPCC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
údajně	údajně	k6eAd1	údajně
zpolitizované	zpolitizovaný	k2eAgFnPc1d1	zpolitizovaná
<g/>
.	.	kIx.	.
</s>
