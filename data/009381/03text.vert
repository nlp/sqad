<p>
<s>
Ropa	ropa	k1gFnSc1	ropa
je	být	k5eAaImIp3nS	být
obec	obec	k1gFnSc4	obec
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
okresu	okres	k1gInSc6	okres
Gorlice	Gorlice	k1gFnSc2	Gorlice
<g/>
,	,	kIx,	,
Malopolském	malopolský	k2eAgNnSc6d1	Malopolské
vojvodství	vojvodství	k1gNnSc6	vojvodství
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
přibližně	přibližně	k6eAd1	přibližně
11	[number]	k4	11
kilometrů	kilometr	k1gInPc2	kilometr
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Gorlice	Gorlice	k1gFnSc2	Gorlice
a	a	k8xC	a
95	[number]	k4	95
kilometrů	kilometr	k1gInPc2	kilometr
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Krakova	Krakov	k1gInSc2	Krakov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
gminy	gmina	k1gFnSc2	gmina
Ropa	ropa	k1gFnSc1	ropa
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
mimo	mimo	k7c4	mimo
střediskové	střediskový	k2eAgFnPc4d1	středisková
obce	obec	k1gFnPc4	obec
tvoří	tvořit	k5eAaImIp3nP	tvořit
vesnice	vesnice	k1gFnSc1	vesnice
Klimkówka	Klimkówka	k1gFnSc1	Klimkówka
a	a	k8xC	a
Łosie	Łosie	k1gFnSc1	Łosie
<g/>
.	.	kIx.	.
</s>
<s>
Bydlí	bydlet	k5eAaImIp3nS	bydlet
tu	tu	k6eAd1	tu
okolo	okolo	k7c2	okolo
3700	[number]	k4	3700
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
