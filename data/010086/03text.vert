<p>
<s>
Prstová	prstový	k2eAgFnSc1d1	prstová
abeceda	abeceda	k1gFnSc1	abeceda
je	být	k5eAaImIp3nS	být
komunikační	komunikační	k2eAgInSc4d1	komunikační
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
užívají	užívat	k5eAaImIp3nP	užívat
zejména	zejména	k9	zejména
neslyšící	slyšící	k2eNgFnPc1d1	neslyšící
osoby	osoba	k1gFnPc1	osoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prstová	prstový	k2eAgFnSc1d1	prstová
abeceda	abeceda	k1gFnSc1	abeceda
využívá	využívat	k5eAaPmIp3nS	využívat
formalizovaných	formalizovaný	k2eAgNnPc2d1	formalizované
a	a	k8xC	a
ustálených	ustálený	k2eAgNnPc2d1	ustálené
postavení	postavení	k1gNnPc2	postavení
prstů	prst	k1gInPc2	prst
a	a	k8xC	a
dlaně	dlaň	k1gFnSc2	dlaň
jedné	jeden	k4xCgFnSc2	jeden
ruky	ruka	k1gFnSc2	ruka
nebo	nebo	k8xC	nebo
prstů	prst	k1gInPc2	prst
a	a	k8xC	a
dlaní	dlaň	k1gFnPc2	dlaň
obou	dva	k4xCgFnPc2	dva
rukou	ruka	k1gFnPc2	ruka
k	k	k7c3	k
zobrazování	zobrazování	k1gNnSc3	zobrazování
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
písmen	písmeno	k1gNnPc2	písmeno
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Prstová	prstový	k2eAgFnSc1d1	prstová
abeceda	abeceda	k1gFnSc1	abeceda
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaPmNgFnS	využívat
zejména	zejména	k9	zejména
k	k	k7c3	k
odhláskování	odhláskování	k1gNnSc3	odhláskování
cizích	cizí	k2eAgNnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
odborných	odborný	k2eAgInPc2d1	odborný
termínů	termín	k1gInPc2	termín
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
dalších	další	k2eAgInPc2d1	další
pojmů	pojem	k1gInPc2	pojem
<g/>
.	.	kIx.	.
</s>
<s>
Prstová	prstový	k2eAgFnSc1d1	prstová
abeceda	abeceda	k1gFnSc1	abeceda
v	v	k7c6	v
taktilní	taktilní	k2eAgFnSc6d1	taktilní
(	(	kIx(	(
<g/>
hmatové	hmatový	k2eAgFnSc6d1	hmatová
<g/>
)	)	kIx)	)
formě	forma	k1gFnSc6	forma
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
využívána	využívat	k5eAaPmNgFnS	využívat
jako	jako	k8xC	jako
komunikační	komunikační	k2eAgInSc4d1	komunikační
systém	systém	k1gInSc4	systém
hluchoslepých	hluchoslepý	k2eAgFnPc2d1	hluchoslepá
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prstové	prstový	k2eAgFnSc2d1	prstová
abecedy	abeceda	k1gFnSc2	abeceda
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
jazyky	jazyk	k1gInPc4	jazyk
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgInP	používat
různé	různý	k2eAgInPc1d1	různý
systémy	systém	k1gInPc1	systém
prstové	prstový	k2eAgFnSc2d1	prstová
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
latinku	latinka	k1gFnSc4	latinka
jsou	být	k5eAaImIp3nP	být
častěji	často	k6eAd2	často
používány	používán	k2eAgInPc4d1	používán
prsty	prst	k1gInPc4	prst
jedné	jeden	k4xCgFnSc2	jeden
ruky	ruka	k1gFnSc2	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Praváci	pravák	k1gMnPc1	pravák
přitom	přitom	k6eAd1	přitom
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
písmena	písmeno	k1gNnPc1	písmeno
pravou	pravý	k2eAgFnSc4d1	pravá
ruku	ruka	k1gFnSc4	ruka
<g/>
,	,	kIx,	,
leváci	levák	k1gMnPc1	levák
levou	levý	k2eAgFnSc4d1	levá
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
popsán	popsat	k5eAaPmNgInS	popsat
španělskými	španělský	k2eAgInPc7d1	španělský
mnichy	mnich	k1gInPc7	mnich
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
použit	použít	k5eAaPmNgInS	použít
v	v	k7c6	v
pařížské	pařížský	k2eAgFnSc6d1	Pařížská
škole	škola	k1gFnSc6	škola
hluchých	hluchý	k2eAgFnPc6d1	hluchá
v	v	k7c4	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
se	se	k3xPyFc4	se
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
jazyky	jazyk	k1gInPc4	jazyk
existují	existovat	k5eAaImIp3nP	existovat
odchylky	odchylka	k1gFnPc1	odchylka
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
Spojené	spojený	k2eAgNnSc4d1	spojené
království	království	k1gNnSc4	království
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc4	Austrálie
a	a	k8xC	a
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
používána	používán	k2eAgFnSc1d1	používána
prstová	prstový	k2eAgFnSc1d1	prstová
abeceda	abeceda	k1gFnSc1	abeceda
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
BANZSL	BANZSL	kA	BANZSL
(	(	kIx(	(
<g/>
British	British	k1gMnSc1	British
<g/>
,	,	kIx,	,
Australian	Australian	k1gMnSc1	Australian
and	and	k?	and
New	New	k1gMnSc4	New
Zealand	Zealand	k1gInSc1	Zealand
Sign	signum	k1gNnPc2	signum
Language	language	k1gFnSc2	language
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přepis	přepis	k1gInSc1	přepis
francouzské	francouzský	k2eAgFnSc2d1	francouzská
abecedy	abeceda	k1gFnSc2	abeceda
je	být	k5eAaImIp3nS	být
označován	označován	k2eAgMnSc1d1	označován
LSF	LSF	kA	LSF
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
langue	langue	k1gFnSc2	langue
des	des	k1gNnPc2	des
signes	signes	k1gMnSc1	signes
française	française	k1gFnSc2	française
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
řadě	řada	k1gFnSc6	řada
zemí	zem	k1gFnPc2	zem
jsou	být	k5eAaImIp3nP	být
používány	používán	k2eAgInPc1d1	používán
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
jednotlivá	jednotlivý	k2eAgNnPc1d1	jednotlivé
písmena	písmeno	k1gNnPc1	písmeno
tvořena	tvořit	k5eAaImNgNnP	tvořit
prsty	prst	k1gInPc1	prst
obou	dva	k4xCgFnPc2	dva
rukou	ruka	k1gFnPc2	ruka
(	(	kIx(	(
<g/>
dominantní	dominantní	k2eAgFnSc2d1	dominantní
ruky	ruka	k1gFnSc2	ruka
a	a	k8xC	a
podřízené	podřízený	k2eAgFnSc2d1	podřízená
ruky	ruka	k1gFnSc2	ruka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prstové	prstový	k2eAgFnPc1d1	prstová
abecedy	abeceda	k1gFnPc1	abeceda
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgFnP	používat
i	i	k9	i
pro	pro	k7c4	pro
jiná	jiný	k2eAgNnPc4d1	jiné
písma	písmo	k1gNnPc4	písmo
<g/>
,	,	kIx,	,
než	než	k8xS	než
latinku	latinka	k1gFnSc4	latinka
(	(	kIx(	(
<g/>
řečtina	řečtina	k1gFnSc1	řečtina
<g/>
,	,	kIx,	,
ruština	ruština	k1gFnSc1	ruština
<g/>
,	,	kIx,	,
arabština	arabština	k1gFnSc1	arabština
<g/>
,	,	kIx,	,
japonština	japonština	k1gFnSc1	japonština
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prstová	prstový	k2eAgFnSc1d1	prstová
abeceda	abeceda	k1gFnSc1	abeceda
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
==	==	k?	==
</s>
</p>
<p>
<s>
Užívání	užívání	k1gNnSc1	užívání
prstové	prstový	k2eAgFnSc2d1	prstová
abecedy	abeceda	k1gFnSc2	abeceda
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
upravuje	upravovat	k5eAaImIp3nS	upravovat
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
155	[number]	k4	155
<g/>
/	/	kIx~	/
<g/>
1998	[number]	k4	1998
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
komunikačních	komunikační	k2eAgInPc6d1	komunikační
systémech	systém	k1gInPc6	systém
neslyšících	slyšící	k2eNgMnPc2d1	neslyšící
a	a	k8xC	a
hluchoněmých	hluchoněmý	k2eAgFnPc2d1	hluchoněmá
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Fingerspelling	Fingerspelling	k1gInSc4	Fingerspelling
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
,	,	kIx,	,
Д	Д	k?	Д
na	na	k7c6	na
ruské	ruský	k2eAgFnSc6d1	ruská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Alphabet	Alphabet	k1gInSc1	Alphabet
dactylologique	dactylologiquat	k5eAaPmIp3nS	dactylologiquat
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Znaková	znakový	k2eAgFnSc1d1	znaková
řeč	řeč	k1gFnSc1	řeč
</s>
</p>
<p>
<s>
Znakový	znakový	k2eAgInSc1d1	znakový
jazyk	jazyk	k1gInSc1	jazyk
</s>
</p>
