<p>
<s>
Kanton	Kanton	k1gInSc1	Kanton
Tinchebray	Tinchebraa	k1gFnSc2	Tinchebraa
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
Canton	Canton	k1gInSc1	Canton
de	de	k?	de
Tinchebray	Tinchebraa	k1gMnSc2	Tinchebraa
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
francouzský	francouzský	k2eAgInSc1d1	francouzský
kanton	kanton	k1gInSc1	kanton
v	v	k7c6	v
departementu	departement	k1gInSc6	departement
Orne	Orn	k1gFnSc2	Orn
v	v	k7c6	v
regionu	region	k1gInSc6	region
Dolní	dolní	k2eAgFnSc2d1	dolní
Normandie	Normandie	k1gFnSc2	Normandie
<g/>
.	.	kIx.	.
</s>
<s>
Tvořilo	tvořit	k5eAaImAgNnS	tvořit
ho	on	k3xPp3gNnSc2	on
15	[number]	k4	15
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Zrušen	zrušen	k2eAgMnSc1d1	zrušen
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
reformě	reforma	k1gFnSc6	reforma
kantonů	kanton	k1gInPc2	kanton
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obce	obec	k1gFnSc2	obec
kantonu	kanton	k1gInSc2	kanton
==	==	k?	==
</s>
</p>
<p>
<s>
Beauchê	Beauchê	k?	Beauchê
</s>
</p>
<p>
<s>
Chanu	Chanout	k5eAaImIp1nS	Chanout
</s>
</p>
<p>
<s>
Clairefougè	Clairefougè	k?	Clairefougè
</s>
</p>
<p>
<s>
Frê	Frê	k?	Frê
</s>
</p>
<p>
<s>
Larchamp	Larchamp	k1gMnSc1	Larchamp
</s>
</p>
<p>
<s>
Le	Le	k?	Le
Ménil-Ciboult	Ménil-Ciboult	k1gInSc1	Ménil-Ciboult
</s>
</p>
<p>
<s>
Moncy	Monca	k1gFnPc1	Monca
</s>
</p>
<p>
<s>
Montsecret	Montsecret	k1gMnSc1	Montsecret
</s>
</p>
<p>
<s>
Saint-Christophe-de-Chaulieu	Saint-Christophee-Chaulieu	k6eAd1	Saint-Christophe-de-Chaulieu
</s>
</p>
<p>
<s>
Saint-Cornier-des-Landes	Saint-Cornieres-Landes	k1gMnSc1	Saint-Cornier-des-Landes
</s>
</p>
<p>
<s>
Saint-Jean-des-Bois	Saint-Jeanes-Bois	k1gFnSc1	Saint-Jean-des-Bois
</s>
</p>
<p>
<s>
Saint-Pierre-d	Saint-Pierre	k1gMnSc1	Saint-Pierre-d
<g/>
'	'	kIx"	'
<g/>
Entremont	Entremont	k1gMnSc1	Entremont
</s>
</p>
<p>
<s>
Saint-Quentin-les-Chardonnets	Saint-Quentines-Chardonnets	k6eAd1	Saint-Quentin-les-Chardonnets
</s>
</p>
<p>
<s>
Tinchebray	Tinchebraa	k1gFnPc1	Tinchebraa
</s>
</p>
<p>
<s>
Yvrandes	Yvrandes	k1gMnSc1	Yvrandes
</s>
</p>
