<p>
<s>
Hedda	Hedda	k1gFnSc1	Hedda
je	být	k5eAaImIp3nS	být
filmová	filmový	k2eAgFnSc1d1	filmová
adaptace	adaptace	k1gFnSc1	adaptace
hry	hra	k1gFnSc2	hra
Heda	Heda	k1gFnSc1	Heda
Gablerová	Gablerová	k1gFnSc1	Gablerová
norského	norský	k2eAgMnSc2d1	norský
dramatika	dramatik	k1gMnSc2	dramatik
Henrika	Henrik	k1gMnSc2	Henrik
Ibsena	Ibsen	k1gMnSc2	Ibsen
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
rolích	role	k1gFnPc6	role
se	se	k3xPyFc4	se
představili	představit	k5eAaPmAgMnP	představit
Peter	Peter	k1gMnSc1	Peter
Eyre	Eyre	k1gInSc4	Eyre
<g/>
,	,	kIx,	,
Glenda	Glenda	k1gFnSc1	Glenda
Jacksonová	Jacksonová	k1gFnSc1	Jacksonová
a	a	k8xC	a
Patrick	Patrick	k1gMnSc1	Patrick
Stewart	Stewart	k1gMnSc1	Stewart
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc7	jeho
režisérem	režisér	k1gMnSc7	režisér
byl	být	k5eAaImAgMnS	být
Trevor	Trevor	k1gMnSc1	Trevor
Nunn	Nunn	k1gMnSc1	Nunn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
první	první	k4xOgFnSc4	první
(	(	kIx(	(
<g/>
a	a	k8xC	a
doposud	doposud	k6eAd1	doposud
jediné	jediný	k2eAgNnSc1d1	jediné
<g/>
)	)	kIx)	)
filmové	filmový	k2eAgNnSc1d1	filmové
zpracování	zpracování	k1gNnSc1	zpracování
této	tento	k3xDgFnSc2	tento
hry	hra	k1gFnSc2	hra
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byla	být	k5eAaImAgFnS	být
hra	hra	k1gFnSc1	hra
již	již	k6eAd1	již
několikrát	několikrát	k6eAd1	několikrát
zfilmována	zfilmován	k2eAgFnSc1d1	zfilmována
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
vždy	vždy	k6eAd1	vždy
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Glenda	Glenda	k1gFnSc1	Glenda
Jackson	Jacksona	k1gFnPc2	Jacksona
byla	být	k5eAaImAgFnS	být
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
ženský	ženský	k2eAgInSc4d1	ženský
herecký	herecký	k2eAgInSc4d1	herecký
výkon	výkon	k1gInSc4	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
také	také	k9	také
představen	představit	k5eAaPmNgInS	představit
na	na	k7c6	na
Filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Cannes	Cannes	k1gNnSc6	Cannes
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nebyl	být	k5eNaImAgMnS	být
zapsán	zapsat	k5eAaPmNgMnS	zapsat
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
soutěže	soutěž	k1gFnSc2	soutěž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Hedda	Hedda	k1gFnSc1	Hedda
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
