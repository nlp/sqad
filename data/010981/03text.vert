<p>
<s>
Corno	Corno	k6eAd1	Corno
Grande	grand	k1gMnSc5	grand
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Apeninského	apeninský	k2eAgInSc2d1	apeninský
poloostrova	poloostrov	k1gInSc2	poloostrov
a	a	k8xC	a
horského	horský	k2eAgInSc2d1	horský
systému	systém	k1gInSc2	systém
Apenin	Apeniny	k1gFnPc2	Apeniny
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
Apenin	Apeniny	k1gFnPc2	Apeniny
(	(	kIx(	(
<g/>
ve	v	k7c6	v
Středních	střední	k2eAgFnPc6d1	střední
Apeninách	Apeniny	k1gFnPc6	Apeniny
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
masivu	masiv	k1gInSc6	masiv
Gran	Grana	k1gFnPc2	Grana
Sasso	Sassa	k1gFnSc5	Sassa
<g/>
,	,	kIx,	,
v	v	k7c6	v
Abruzzu	Abruzz	k1gInSc6	Abruzz
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jeho	jeho	k3xOp3gInSc7	jeho
vrcholem	vrchol	k1gInSc7	vrchol
leží	ležet	k5eAaImIp3nS	ležet
nejjižnější	jižní	k2eAgInSc1d3	nejjižnější
ledovec	ledovec	k1gInSc1	ledovec
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
Ghiacciaio	Ghiacciaio	k6eAd1	Ghiacciaio
del	del	k?	del
Calderone	Calderon	k1gInSc5	Calderon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Corno	Corno	k6eAd1	Corno
Grande	grand	k1gMnSc5	grand
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Národního	národní	k2eAgInSc2d1	národní
parku	park	k1gInSc2	park
Gran	Gran	k1gMnSc1	Gran
Sasso	Sassa	k1gFnSc5	Sassa
e	e	k0	e
Monti	Monti	k1gNnSc2	Monti
della	della	k1gMnSc1	della
Laga	Laga	k1gMnSc1	Laga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Název	název	k1gInSc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
italštiny	italština	k1gFnSc2	italština
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
velký	velký	k2eAgInSc4d1	velký
roh	roh	k1gInSc4	roh
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prvovýstup	prvovýstup	k1gInSc4	prvovýstup
==	==	k?	==
</s>
</p>
<p>
<s>
Prvním	první	k4xOgMnSc7	první
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
na	na	k7c4	na
horu	hora	k1gFnSc4	hora
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
historických	historický	k2eAgInPc2d1	historický
pramenů	pramen	k1gInPc2	pramen
Francesco	Francesco	k6eAd1	Francesco
De	De	k?	De
Marchi	March	k1gMnSc3	March
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1573	[number]	k4	1573
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Corno	Corno	k6eAd1	Corno
Grande	grand	k1gMnSc5	grand
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
