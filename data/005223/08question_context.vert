<s>
Všechny	všechen	k3xTgFnPc1	všechen
duchovní	duchovní	k2eAgFnPc1d1	duchovní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
materiální	materiální	k2eAgInPc1d1	materiální
úspěchy	úspěch	k1gInPc1	úspěch
Mayů	May	k1gMnPc2	May
se	se	k3xPyFc4	se
zakládaly	zakládat	k5eAaImAgFnP	zakládat
na	na	k7c4	na
originální	originální	k2eAgInPc4d1	originální
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
jednoduché	jednoduchý	k2eAgFnSc6d1	jednoduchá
číselné	číselný	k2eAgFnSc6d1	číselná
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc4	její
základ	základ	k1gInSc4	základ
tvořilo	tvořit	k5eAaImAgNnS	tvořit
dvacet	dvacet	k4xCc1	dvacet
číslic	číslice	k1gFnPc2	číslice
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
nuly	nula	k1gFnSc2	nula
(	(	kIx(	(
<g/>
starověké	starověký	k2eAgFnSc2d1	starověká
kultury	kultura	k1gFnSc2	kultura
nulu	nula	k1gFnSc4	nula
většinou	většinou	k6eAd1	většinou
neznaly	znát	k5eNaImAgFnP	znát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>

