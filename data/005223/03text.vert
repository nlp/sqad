<s>
Pojmem	pojem	k1gInSc7	pojem
Mayská	mayský	k2eAgFnSc1d1	mayská
civilizace	civilizace	k1gFnSc1	civilizace
(	(	kIx(	(
<g/>
cca	cca	kA	cca
2500	[number]	k4	2500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
1520	[number]	k4	1520
<g/>
/	/	kIx~	/
<g/>
1697	[number]	k4	1697
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rozumí	rozumět	k5eAaImIp3nS	rozumět
území	území	k1gNnSc1	území
tzv.	tzv.	kA	tzv.
Mayské	mayský	k2eAgFnSc2d1	mayská
říše	říš	k1gFnSc2	říš
(	(	kIx(	(
<g/>
spíše	spíše	k9	spíše
konfederace	konfederace	k1gFnSc1	konfederace
městských	městský	k2eAgInPc2d1	městský
států	stát	k1gInPc2	stát
<g/>
)	)	kIx)	)
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
oblasti	oblast	k1gFnPc4	oblast
osídlené	osídlený	k2eAgFnPc4d1	osídlená
početnými	početný	k2eAgInPc7d1	početný
mayskými	mayský	k2eAgInPc7d1	mayský
národy	národ	k1gInPc7	národ
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
rozprostíraly	rozprostírat	k5eAaImAgInP	rozprostírat
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
<g />
.	.	kIx.	.
</s>
<s>
poloostrově	poloostrov	k1gInSc6	poloostrov
Yucatán	Yucatán	k2eAgMnSc1d1	Yucatán
a	a	k8xC	a
protilehlém	protilehlý	k2eAgNnSc6d1	protilehlé
území	území	k1gNnSc6	území
až	až	k9	až
po	po	k7c4	po
Tichý	tichý	k2eAgInSc4d1	tichý
oceán	oceán	k1gInSc4	oceán
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
přesněji	přesně	k6eAd2	přesně
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Guatemale	Guatemala	k1gFnSc6	Guatemala
<g/>
,	,	kIx,	,
celém	celý	k2eAgInSc6d1	celý
dnešním	dnešní	k2eAgInSc6d1	dnešní
Belize	Beliz	k1gInSc6	Beliz
<g/>
,	,	kIx,	,
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
Hondurasu	Honduras	k1gInSc6	Honduras
<g/>
,	,	kIx,	,
v	v	k7c6	v
mexických	mexický	k2eAgInPc6d1	mexický
státech	stát	k1gInPc6	stát
Yucatán	Yucatán	k2eAgMnSc1d1	Yucatán
<g/>
,	,	kIx,	,
Quintana	Quintana	k1gFnSc1	Quintana
Roo	Roo	k1gFnSc1	Roo
<g/>
,	,	kIx,	,
Campeche	Campeche	k1gFnSc1	Campeche
<g/>
,	,	kIx,	,
Chiapas	Chiapas	k1gInSc1	Chiapas
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
mexického	mexický	k2eAgInSc2d1	mexický
státu	stát	k1gInSc2	stát
Tabasco	Tabasco	k1gMnSc1	Tabasco
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Mezoameriky	Mezoamerika	k1gFnSc2	Mezoamerika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
více	hodně	k6eAd2	hodně
důvodů	důvod	k1gInPc2	důvod
sa	sa	k?	sa
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
<g/>
/	/	kIx~	/
<g/>
čtyři	čtyři	k4xCgMnPc4	čtyři
velmi	velmi	k6eAd1	velmi
odlišné	odlišný	k2eAgFnPc4d1	odlišná
oblasti	oblast	k1gFnPc4	oblast
(	(	kIx(	(
<g/>
od	od	k7c2	od
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
severní	severní	k2eAgFnPc1d1	severní
nížiny	nížina	k1gFnPc1	nížina
<g/>
:	:	kIx,	:
severní	severní	k2eAgInSc4d1	severní
poloostrov	poloostrov	k1gInSc4	poloostrov
Yucatán	Yucatán	k1gMnSc1	Yucatán
jižní	jižní	k2eAgFnSc2d1	jižní
nížiny	nížina	k1gFnSc2	nížina
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc1	oblast
tropického	tropický	k2eAgInSc2d1	tropický
pralesa	prales	k1gInSc2	prales
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Chiapas	Chiapas	k1gInSc1	Chiapas
(	(	kIx(	(
<g/>
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Petén	Petén	k1gInSc1	Petén
(	(	kIx(	(
<g/>
region	region	k1gInSc1	region
v	v	k7c6	v
Guatemale	Guatemala	k1gFnSc6	Guatemala
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Belize	Belize	k1gFnSc1	Belize
a	a	k8xC	a
severozápadní	severozápadní	k2eAgInSc1d1	severozápadní
Honduras	Honduras	k1gInSc1	Honduras
vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
:	:	kIx,	:
na	na	k7c6	na
tichomořském	tichomořský	k2eAgNnSc6d1	Tichomořské
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
;	;	kIx,	;
často	často	k6eAd1	často
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gNnPc2	on
vyčleňuje	vyčleňovat	k5eAaImIp3nS	vyčleňovat
území	území	k1gNnSc4	území
lesnaté	lesnatý	k2eAgFnSc2d1	lesnatá
nížiny	nížina	k1gFnSc2	nížina
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
tichomořském	tichomořský	k2eAgNnSc6d1	Tichomořské
pobřeží	pobřeží	k1gNnSc6	pobřeží
Podrobnější	podrobný	k2eAgFnSc2d2	podrobnější
informace	informace	k1gFnSc2	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Mayů	May	k1gMnPc2	May
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
Mayů	May	k1gMnPc2	May
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
dějin	dějiny	k1gFnPc2	dějiny
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
značně	značně	k6eAd1	značně
změnil	změnit	k5eAaPmAgInS	změnit
<g/>
,	,	kIx,	,
co	co	k9	co
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gNnSc1	jejich
komplikované	komplikovaný	k2eAgNnSc1d1	komplikované
písmo	písmo	k1gNnSc1	písmo
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
daří	dařit	k5eAaImIp3nS	dařit
rozluštit	rozluštit	k5eAaPmF	rozluštit
až	až	k9	až
od	od	k7c2	od
posledních	poslední	k2eAgNnPc2d1	poslední
desetiletí	desetiletí	k1gNnPc2	desetiletí
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
až	až	k9	až
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
jsme	být	k5eAaImIp1nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
rozumět	rozumět	k5eAaImF	rozumět
jejich	jejich	k3xOp3gInPc3	jejich
textům	text	k1gInPc3	text
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
bylo	být	k5eAaImAgNnS	být
rozluštěno	rozluštit	k5eAaPmNgNnS	rozluštit
asi	asi	k9	asi
85	[number]	k4	85
%	%	kIx~	%
mayských	mayský	k2eAgInPc2d1	mayský
hieroglyfů	hieroglyf	k1gInPc2	hieroglyf
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc4	dějiny
Mayů	May	k1gMnPc2	May
dělíme	dělit	k5eAaImIp1nP	dělit
na	na	k7c6	na
období	období	k1gNnSc6	období
<g/>
:	:	kIx,	:
archaické	archaický	k2eAgFnSc2d1	archaická
(	(	kIx(	(
<g/>
7000	[number]	k4	7000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
2000	[number]	k4	2000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předklasické	předklasický	k2eAgFnSc2d1	předklasická
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
250	[number]	k4	250
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
klasické	klasický	k2eAgFnSc2d1	klasická
(	(	kIx(	(
<g/>
250	[number]	k4	250
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
900	[number]	k4	900
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
poklasické	poklasický	k2eAgFnSc2d1	poklasický
(	(	kIx(	(
<g/>
900	[number]	k4	900
<g/>
–	–	k?	–
<g/>
1521	[number]	k4	1521
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
prvních	první	k4xOgInPc6	první
Mayích	May	k1gMnPc6	May
můžeme	moct	k5eAaImIp1nP	moct
podle	podle	k7c2	podle
posledního	poslední	k2eAgInSc2d1	poslední
výzkumu	výzkum	k1gInSc2	výzkum
mluvit	mluvit	k5eAaImF	mluvit
už	už	k6eAd1	už
asi	asi	k9	asi
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2500	[number]	k4	2500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
přibližně	přibližně	k6eAd1	přibližně
s	s	k7c7	s
výskytem	výskyt	k1gInSc7	výskyt
prvního	první	k4xOgNnSc2	první
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
nálezů	nález	k1gInPc2	nález
ale	ale	k8xC	ale
pochází	pocházet	k5eAaImIp3nS	pocházet
až	až	k9	až
z	z	k7c2	z
období	období	k1gNnSc2	období
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1500	[number]	k4	1500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Mayové	Mayová	k1gFnSc2	Mayová
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
celých	celá	k1gFnPc2	celá
svých	svůj	k3xOyFgFnPc2	svůj
dějin	dějiny	k1gFnPc2	dějiny
pěstovali	pěstovat	k5eAaImAgMnP	pěstovat
zejména	zejména	k9	zejména
kukuřici	kukuřice	k1gFnSc4	kukuřice
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
ve	v	k7c6	v
středním	střední	k2eAgNnSc6d1	střední
předklasickém	předklasický	k2eAgNnSc6d1	předklasický
období	období	k1gNnSc6	období
převzali	převzít	k5eAaPmAgMnP	převzít
Mayové	May	k1gMnPc1	May
od	od	k7c2	od
Olméků	Olmék	k1gInPc2	Olmék
systém	systém	k1gInSc1	systém
hierarchické	hierarchický	k2eAgFnSc2d1	hierarchická
společnosti	společnost	k1gFnSc2	společnost
vedené	vedený	k2eAgFnSc2d1	vedená
králi	král	k1gMnSc3	král
a	a	k8xC	a
šlechtici	šlechtic	k1gMnSc3	šlechtic
<g/>
.	.	kIx.	.
</s>
<s>
Nížinní	nížinný	k2eAgMnPc1d1	nížinný
Mayové	May	k1gMnPc1	May
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
kmenové	kmenový	k2eAgFnPc4d1	kmenová
konfederace	konfederace	k1gFnPc4	konfederace
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Mayové	May	k1gMnPc1	May
ve	v	k7c6	v
vysočinách	vysočina	k1gFnPc6	vysočina
se	se	k3xPyFc4	se
podrobovali	podrobovat	k5eAaImAgMnP	podrobovat
nadřízeným	nadřízený	k2eAgMnPc3d1	nadřízený
králům	král	k1gMnPc3	král
nebo	nebo	k8xC	nebo
náčelníkům	náčelník	k1gMnPc3	náčelník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
se	se	k3xPyFc4	se
králové	král	k1gMnPc1	král
objevili	objevit	k5eAaPmAgMnP	objevit
až	až	k9	až
od	od	k7c2	od
pozdního	pozdní	k2eAgNnSc2d1	pozdní
předklasického	předklasický	k2eAgNnSc2d1	předklasický
období	období	k1gNnSc2	období
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
však	však	k9	však
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
100	[number]	k4	100
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
nazýval	nazývat	k5eAaImAgInS	nazývat
ahaus	ahaus	k1gInSc1	ahaus
(	(	kIx(	(
<g/>
velkokrál	velkokrál	k1gInSc1	velkokrál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
mnohé	mnohé	k1gNnSc4	mnohé
jiné	jiný	k2eAgFnSc2d1	jiná
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
mayská	mayský	k2eAgFnSc1d1	mayská
společnost	společnost	k1gFnSc1	společnost
nikdy	nikdy	k6eAd1	nikdy
netvořila	tvořit	k5eNaImAgFnS	tvořit
jednotnou	jednotný	k2eAgFnSc4d1	jednotná
říši	říše	k1gFnSc4	říše
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
klasickém	klasický	k2eAgNnSc6d1	klasické
období	období	k1gNnSc6	období
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
měla	mít	k5eAaImAgFnS	mít
blízko	blízko	k1gNnSc4	blízko
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
existovala	existovat	k5eAaImAgFnS	existovat
dvě	dva	k4xCgNnPc4	dva
velká	velký	k2eAgNnPc4d1	velké
království	království	k1gNnPc4	království
–	–	k?	–
Tikalské	Tikalský	k2eAgFnSc6d1	Tikalský
a	a	k8xC	a
Calakmulské	Calakmulský	k2eAgFnSc6d1	Calakmulský
<g/>
.	.	kIx.	.
</s>
<s>
Mayská	mayský	k2eAgFnSc1d1	mayská
nížinná	nížinný	k2eAgFnSc1d1	nížinná
civilizace	civilizace	k1gFnSc1	civilizace
klasického	klasický	k2eAgNnSc2d1	klasické
období	období	k1gNnSc2	období
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
ohledu	ohled	k1gInSc6	ohled
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
kulturní	kulturní	k2eAgFnSc2d1	kulturní
úrovně	úroveň	k1gFnSc2	úroveň
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
známých	známý	k2eAgFnPc2d1	známá
kultur	kultura	k1gFnPc2	kultura
předkolumbovské	předkolumbovský	k2eAgFnSc2d1	předkolumbovská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Budovala	budovat	k5eAaImAgNnP	budovat
se	se	k3xPyFc4	se
města	město	k1gNnPc1	město
s	s	k7c7	s
nádvořími	nádvoří	k1gNnPc7	nádvoří
na	na	k7c4	na
náboženské	náboženský	k2eAgInPc4d1	náboženský
obřady	obřad	k1gInPc4	obřad
<g/>
,	,	kIx,	,
chrámy	chrám	k1gInPc4	chrám
<g/>
,	,	kIx,	,
paláci	palác	k1gInSc6	palác
<g/>
,	,	kIx,	,
či	či	k8xC	či
hřišti	hřiště	k1gNnSc3	hřiště
na	na	k7c4	na
míčové	míčový	k2eAgFnPc4d1	Míčová
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
chápalo	chápat	k5eAaImAgNnS	chápat
spíše	spíše	k9	spíše
jako	jako	k9	jako
kultové	kultový	k2eAgNnSc1d1	kultový
středisko	středisko	k1gNnSc1	středisko
než	než	k8xS	než
jako	jako	k9	jako
místo	místo	k1gNnSc4	místo
na	na	k7c4	na
bydlení	bydlení	k1gNnSc4	bydlení
<g/>
.	.	kIx.	.
</s>
<s>
Mayské	mayský	k2eAgInPc1d1	mayský
chrámy	chrám	k1gInPc1	chrám
<g/>
,	,	kIx,	,
stavěné	stavěný	k2eAgFnPc4d1	stavěná
jako	jako	k8xS	jako
mohutné	mohutný	k2eAgFnPc4d1	mohutná
stupňovité	stupňovitý	k2eAgFnPc4d1	stupňovitá
kamenné	kamenný	k2eAgFnPc4d1	kamenná
stavby	stavba	k1gFnPc4	stavba
<g/>
,	,	kIx,	,
bývají	bývat	k5eAaImIp3nP	bývat
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
podobnost	podobnost	k1gFnSc4	podobnost
s	s	k7c7	s
egyptskými	egyptský	k2eAgFnPc7d1	egyptská
pyramidami	pyramida	k1gFnPc7	pyramida
nazývány	nazýván	k2eAgFnPc1d1	nazývána
chrámové	chrámový	k2eAgFnPc1d1	chrámová
pyramidy	pyramida	k1gFnPc1	pyramida
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
náboženské	náboženský	k2eAgInPc4d1	náboženský
obřady	obřad	k1gInPc4	obřad
se	se	k3xPyFc4	se
využívaly	využívat	k5eAaImAgFnP	využívat
i	i	k9	i
jeskyně	jeskyně	k1gFnPc1	jeskyně
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgNnPc1d1	sloužící
jako	jako	k8xC	jako
poutní	poutní	k2eAgNnPc1d1	poutní
místa	místo	k1gNnPc1	místo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgInPc6	který
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgNnP	mít
uskutečňovat	uskutečňovat	k5eAaImF	uskutečňovat
střetnutí	střetnutí	k1gNnPc1	střetnutí
s	s	k7c7	s
duchy	duch	k1gMnPc7	duch
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgFnPc1d1	lidská
oběti	oběť	k1gFnPc1	oběť
nebyly	být	k5eNaImAgFnP	být
zřídkavé	zřídkavý	k2eAgFnPc1d1	zřídkavá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klasickém	klasický	k2eAgNnSc6d1	klasické
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
mayská	mayský	k2eAgFnSc1d1	mayská
společnost	společnost	k1gFnSc1	společnost
členila	členit	k5eAaImAgFnS	členit
na	na	k7c4	na
vládnoucí	vládnoucí	k2eAgFnSc4d1	vládnoucí
vrstvu	vrstva	k1gFnSc4	vrstva
(	(	kIx(	(
<g/>
vládce	vládce	k1gMnPc4	vládce
<g/>
,	,	kIx,	,
šlechtu	šlechta	k1gFnSc4	šlechta
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc4	jejich
rodinu	rodina	k1gFnSc4	rodina
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
družinou	družina	k1gFnSc7	družina
a	a	k8xC	a
dvořany	dvořan	k1gMnPc7	dvořan
<g/>
)	)	kIx)	)
a	a	k8xC	a
kněze	kněz	k1gMnSc2	kněz
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc4d1	střední
vrstvu	vrstva	k1gFnSc4	vrstva
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
dnes	dnes	k6eAd1	dnes
–	–	k?	–
specialisté	specialista	k1gMnPc1	specialista
<g/>
,	,	kIx,	,
řemeslníci	řemeslník	k1gMnPc1	řemeslník
<g/>
,	,	kIx,	,
manažeři	manažer	k1gMnPc1	manažer
a	a	k8xC	a
úředníci	úředník	k1gMnPc1	úředník
<g/>
;	;	kIx,	;
vlivní	vlivný	k2eAgMnPc1d1	vlivný
lidé	člověk	k1gMnPc1	člověk
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
vrstvy	vrstva	k1gFnSc2	vrstva
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
i	i	k9	i
do	do	k7c2	do
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bojovníky	bojovník	k1gMnPc4	bojovník
<g/>
,	,	kIx,	,
otroky	otrok	k1gMnPc4	otrok
(	(	kIx(	(
<g/>
výlučně	výlučně	k6eAd1	výlučně
zajatí	zajatý	k2eAgMnPc1d1	zajatý
nepřátelští	přátelský	k2eNgMnPc1d1	nepřátelský
vojáci	voják	k1gMnPc1	voják
<g/>
)	)	kIx)	)
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Společenské	společenský	k2eAgFnPc1d1	společenská
vrstvy	vrstva	k1gFnPc1	vrstva
se	se	k3xPyFc4	se
podobaly	podobat	k5eAaImAgFnP	podobat
odděleným	oddělený	k2eAgFnPc3d1	oddělená
kastám	kasta	k1gFnPc3	kasta
<g/>
.	.	kIx.	.
</s>
<s>
Kněží	kněz	k1gMnPc1	kněz
byli	být	k5eAaImAgMnP	být
často	často	k6eAd1	často
současně	současně	k6eAd1	současně
vládci	vládce	k1gMnPc1	vládce
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
základ	základ	k1gInSc4	základ
tvořilo	tvořit	k5eAaImAgNnS	tvořit
zemědělství	zemědělství	k1gNnSc1	zemědělství
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pěstování	pěstování	k1gNnSc4	pěstování
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
platidlo	platidlo	k1gNnSc1	platidlo
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgInP	používat
kakaové	kakaový	k2eAgInPc1d1	kakaový
boby	bob	k1gInPc1	bob
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
imperiální	imperiální	k2eAgFnSc7d1	imperiální
kulturou	kultura	k1gFnSc7	kultura
Inků	Ink	k1gMnPc2	Ink
si	se	k3xPyFc3	se
vyspělá	vyspělý	k2eAgFnSc1d1	vyspělá
mayská	mayský	k2eAgNnPc4d1	mayské
zaslouží	zasloužit	k5eAaPmIp3nS	zasloužit
označení	označení	k1gNnPc4	označení
intelektuální	intelektuální	k2eAgNnPc4d1	intelektuální
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
přinejmenším	přinejmenším	k6eAd1	přinejmenším
písmo	písmo	k1gNnSc4	písmo
a	a	k8xC	a
kalendář	kalendář	k1gInSc4	kalendář
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zdědili	zdědit	k5eAaPmAgMnP	zdědit
od	od	k7c2	od
Olméků	Olmék	k1gInPc2	Olmék
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
už	už	k6eAd1	už
jen	jen	k9	jen
stojí	stát	k5eAaImIp3nS	stát
jejich	jejich	k3xOp3gInPc4	jejich
základní	základní	k2eAgInPc4d1	základní
intelektuální	intelektuální	k2eAgInPc4d1	intelektuální
výdobytky	výdobytek	k1gInPc4	výdobytek
<g/>
:	:	kIx,	:
Jejich	jejich	k3xOp3gNnSc1	jejich
písmo	písmo	k1gNnSc1	písmo
bylo	být	k5eAaImAgNnS	být
jediné	jediný	k2eAgNnSc1d1	jediné
opravdové	opravdový	k2eAgNnSc1d1	opravdové
písmo	písmo	k1gNnSc1	písmo
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
předkolumbovské	předkolumbovský	k2eAgFnSc6d1	předkolumbovská
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
bylo	být	k5eAaImAgNnS	být
schopné	schopný	k2eAgNnSc1d1	schopné
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
slova	slovo	k1gNnPc4	slovo
a	a	k8xC	a
rozličné	rozličný	k2eAgInPc4d1	rozličný
zvuky	zvuk	k1gInPc4	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Zachovalo	zachovat	k5eAaPmAgNnS	zachovat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
na	na	k7c6	na
stélách	stéla	k1gFnPc6	stéla
popisujících	popisující	k2eAgFnPc2d1	popisující
občanské	občanský	k2eAgFnSc2d1	občanská
události	událost	k1gFnSc2	událost
a	a	k8xC	a
zaznamenávajících	zaznamenávající	k2eAgFnPc2d1	zaznamenávající
kalendářní	kalendářní	k2eAgInPc4d1	kalendářní
a	a	k8xC	a
astronomické	astronomický	k2eAgInPc4d1	astronomický
údaje	údaj	k1gInPc4	údaj
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
mayských	mayský	k2eAgFnPc2d1	mayská
knih	kniha	k1gFnPc2	kniha
přežily	přežít	k5eAaPmAgFnP	přežít
španělskou	španělský	k2eAgFnSc4d1	španělská
conquistu	conquista	k1gFnSc4	conquista
jen	jen	k9	jen
čtyři	čtyři	k4xCgNnPc4	čtyři
tzv.	tzv.	kA	tzv.
kodexy	kodex	k1gInPc7	kodex
<g/>
.	.	kIx.	.
</s>
<s>
Mayský	mayský	k2eAgInSc4d1	mayský
kalendář	kalendář	k1gInSc4	kalendář
(	(	kIx(	(
<g/>
lépe	dobře	k6eAd2	dobře
řečeno	říct	k5eAaPmNgNnS	říct
více	hodně	k6eAd2	hodně
paralelních	paralelní	k2eAgInPc2d1	paralelní
kalendářů	kalendář	k1gInPc2	kalendář
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
nejsložitějším	složitý	k2eAgMnPc3d3	nejsložitější
(	(	kIx(	(
<g/>
přinejmenším	přinejmenším	k6eAd1	přinejmenším
<g/>
)	)	kIx)	)
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
předkolumbovské	předkolumbovský	k2eAgFnSc6d1	předkolumbovská
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Mayský	mayský	k2eAgInSc1d1	mayský
kalendář	kalendář	k1gInSc1	kalendář
byl	být	k5eAaImAgInS	být
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
skutečné	skutečný	k2eAgFnSc3d1	skutečná
délce	délka	k1gFnSc3	délka
roku	rok	k1gInSc2	rok
přesnější	přesný	k2eAgFnSc1d2	přesnější
než	než	k8xS	než
náš	náš	k3xOp1gInSc1	náš
dnešní	dnešní	k2eAgInSc1d1	dnešní
kalendář	kalendář	k1gInSc1	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
kalendářní	kalendářní	k2eAgInPc1d1	kalendářní
výpočty	výpočet	k1gInPc1	výpočet
měly	mít	k5eAaImAgInP	mít
dokonce	dokonce	k9	dokonce
speciální	speciální	k2eAgInSc4d1	speciální
název	název	k1gInSc4	název
(	(	kIx(	(
<g/>
alautun	alautun	k1gInSc4	alautun
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
období	období	k1gNnSc4	období
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
63	[number]	k4	63
081	[number]	k4	081
429	[number]	k4	429
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
matematika	matematika	k1gFnSc1	matematika
používala	používat	k5eAaImAgFnS	používat
nulu	nula	k1gFnSc4	nula
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
neznali	znát	k5eNaImAgMnP	znát
Římané	Říman	k1gMnPc1	Říman
ani	ani	k8xC	ani
Řekové	Řek	k1gMnPc1	Řek
<g/>
.	.	kIx.	.
</s>
<s>
Astronomie	astronomie	k1gFnSc1	astronomie
byla	být	k5eAaImAgFnS	být
taktéž	taktéž	k?	taktéž
velmi	velmi	k6eAd1	velmi
vyspělá	vyspělý	k2eAgFnSc1d1	vyspělá
<g/>
.	.	kIx.	.
</s>
<s>
Mayští	mayský	k2eAgMnPc1d1	mayský
kněží	kněz	k1gMnPc1	kněz
velmi	velmi	k6eAd1	velmi
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
kulatá	kulatý	k2eAgFnSc1d1	kulatá
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
tisíc	tisíc	k4xCgInSc4	tisíc
let	let	k1gInSc4	let
před	před	k7c7	před
Koperníkem	Koperník	k1gInSc7	Koperník
<g/>
.	.	kIx.	.
</s>
<s>
Analyzovali	analyzovat	k5eAaImAgMnP	analyzovat
dráhy	dráha	k1gFnPc4	dráha
měsíců	měsíc	k1gInPc2	měsíc
Jupitera	Jupiter	k1gMnSc2	Jupiter
tisíc	tisíc	k4xCgInSc4	tisíc
let	let	k1gInSc4	let
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
vůbec	vůbec	k9	vůbec
objevil	objevit	k5eAaPmAgMnS	objevit
Galileo	Galilea	k1gFnSc5	Galilea
Galilei	Galile	k1gMnPc5	Galile
<g/>
.	.	kIx.	.
</s>
<s>
Znali	znát	k5eAaImAgMnP	znát
cykly	cyklus	k1gInPc4	cyklus
oběhu	oběh	k1gInSc2	oběh
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
Venuše	Venuše	k1gFnSc2	Venuše
<g/>
,	,	kIx,	,
spočítali	spočítat	k5eAaPmAgMnP	spočítat
tropický	tropický	k2eAgInSc4d1	tropický
a	a	k8xC	a
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Stavěli	stavět	k5eAaImAgMnP	stavět
dálkové	dálkový	k2eAgFnPc4d1	dálková
cesty	cesta	k1gFnPc4	cesta
přes	přes	k7c4	přes
džungli	džungle	k1gFnSc4	džungle
<g/>
,	,	kIx,	,
vysušovali	vysušovat	k5eAaImAgMnP	vysušovat
močály	močál	k1gInPc4	močál
<g/>
,	,	kIx,	,
stavěli	stavět	k5eAaImAgMnP	stavět
podzemní	podzemní	k2eAgFnPc4d1	podzemní
nádrže	nádrž	k1gFnPc4	nádrž
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
především	především	k9	především
umělecky	umělecky	k6eAd1	umělecky
hodnotné	hodnotný	k2eAgFnSc2d1	hodnotná
pyramidy	pyramida	k1gFnSc2	pyramida
<g/>
,	,	kIx,	,
paláce	palác	k1gInSc2	palác
a	a	k8xC	a
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Egypta	Egypt	k1gInSc2	Egypt
<g/>
,	,	kIx,	,
starověké	starověký	k2eAgFnSc2d1	starověká
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Řecka	Řecko	k1gNnSc2	Řecko
či	či	k8xC	či
Mezopotámie	Mezopotámie	k1gFnPc1	Mezopotámie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vznikaly	vznikat	k5eAaImAgFnP	vznikat
na	na	k7c6	na
územích	území	k1gNnPc6	území
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nS	dařit
zemědělským	zemědělský	k2eAgFnPc3d1	zemědělská
plodinám	plodina	k1gFnPc3	plodina
(	(	kIx(	(
<g/>
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
s	s	k7c7	s
příznivým	příznivý	k2eAgNnSc7d1	příznivé
klimatem	klima	k1gNnSc7	klima
<g/>
,	,	kIx,	,
s	s	k7c7	s
vhodnými	vhodný	k2eAgInPc7d1	vhodný
vodohospodářskými	vodohospodářský	k2eAgInPc7d1	vodohospodářský
poměry	poměr	k1gInPc7	poměr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Yucatán	Yucatán	k2eAgInSc1d1	Yucatán
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
opakem	opak	k1gInSc7	opak
podobného	podobný	k2eAgInSc2d1	podobný
ideálu	ideál	k1gInSc2	ideál
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
pozoruhodné	pozoruhodný	k2eAgNnSc1d1	pozoruhodné
<g/>
,	,	kIx,	,
že	že	k8xS	že
civilizace	civilizace	k1gFnSc1	civilizace
s	s	k7c7	s
natolik	natolik	k6eAd1	natolik
primitivními	primitivní	k2eAgInPc7d1	primitivní
materiály	materiál	k1gInPc7	materiál
<g/>
,	,	kIx,	,
hospodářskými	hospodářský	k2eAgInPc7d1	hospodářský
a	a	k8xC	a
ekonomickými	ekonomický	k2eAgInPc7d1	ekonomický
základy	základ	k1gInPc7	základ
v	v	k7c6	v
klimaticky	klimaticky	k6eAd1	klimaticky
nevhodné	vhodný	k2eNgFnSc6d1	nevhodná
oblasti	oblast	k1gFnSc6	oblast
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
natolik	natolik	k6eAd1	natolik
výjimečných	výjimečný	k2eAgInPc2d1	výjimečný
intelektuálních	intelektuální	k2eAgInPc2d1	intelektuální
úspěchů	úspěch	k1gInPc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
mayská	mayský	k2eAgFnSc1d1	mayská
kultura	kultura	k1gFnSc1	kultura
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
po	po	k7c6	po
roku	rok	k1gInSc6	rok
800	[number]	k4	800
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
postupně	postupně	k6eAd1	postupně
došlo	dojít	k5eAaPmAgNnS	dojít
z	z	k7c2	z
dodnes	dodnes	k6eAd1	dodnes
sporných	sporný	k2eAgFnPc2d1	sporná
příčin	příčina	k1gFnPc2	příčina
k	k	k7c3	k
opuštění	opuštění	k1gNnSc3	opuštění
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
nížinách	nížina	k1gFnPc6	nížina
<g/>
,	,	kIx,	,
čím	co	k3yInSc7	co
tzv.	tzv.	kA	tzv.
klasické	klasický	k2eAgNnSc1d1	klasické
období	období	k1gNnSc1	období
skončilo	skončit	k5eAaPmAgNnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Mayové	May	k1gMnPc1	May
jižních	jižní	k2eAgFnPc2d1	jižní
nížin	nížina	k1gFnPc2	nížina
se	se	k3xPyFc4	se
změnili	změnit	k5eAaPmAgMnP	změnit
na	na	k7c4	na
primitivní	primitivní	k2eAgMnPc4d1	primitivní
pěstitele	pěstitel	k1gMnPc4	pěstitel
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
,	,	kIx,	,
Mayové	Mayové	k2eAgFnPc2d1	Mayové
severních	severní	k2eAgFnPc2d1	severní
nížin	nížina	k1gFnPc2	nížina
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
pod	pod	k7c4	pod
vliv	vliv	k1gInSc4	vliv
Toltéků	Tolték	k1gInPc2	Tolték
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
např.	např.	kA	např.
vzrůst	vzrůst	k5eAaPmF	vzrůst
obětování	obětování	k1gNnSc4	obětování
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
rozpadli	rozpadnout	k5eAaPmAgMnP	rozpadnout
na	na	k7c4	na
hodně	hodně	k6eAd1	hodně
(	(	kIx(	(
<g/>
ne	ne	k9	ne
nevyspělých	vyspělý	k2eNgInPc2d1	nevyspělý
<g/>
)	)	kIx)	)
samostatných	samostatný	k2eAgInPc2d1	samostatný
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Mayové	Mayová	k1gFnPc1	Mayová
ve	v	k7c6	v
vysočinách	vysočina	k1gFnPc6	vysočina
se	se	k3xPyFc4	se
změnili	změnit	k5eAaPmAgMnP	změnit
v	v	k7c4	v
agresivní	agresivní	k2eAgMnPc4d1	agresivní
bojovníky	bojovník	k1gMnPc4	bojovník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takové	takový	k3xDgFnSc6	takový
situaci	situace	k1gFnSc6	situace
přišli	přijít	k5eAaPmAgMnP	přijít
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1520	[number]	k4	1520
první	první	k4xOgMnPc1	první
španělští	španělský	k2eAgMnPc1d1	španělský
dobyvatelé	dobyvatel	k1gMnPc1	dobyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1550	[number]	k4	1550
dobyli	dobýt	k5eAaPmAgMnP	dobýt
většinu	většina	k1gFnSc4	většina
mayského	mayský	k2eAgNnSc2d1	mayské
území	území	k1gNnSc2	území
(	(	kIx(	(
<g/>
Petén	Petén	k1gInSc4	Petén
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1697	[number]	k4	1697
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dobývání	dobývání	k1gNnSc1	dobývání
jim	on	k3xPp3gFnPc3	on
trvalo	trvat	k5eAaImAgNnS	trvat
poměrně	poměrně	k6eAd1	poměrně
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zejména	zejména	k9	zejména
severní	severní	k2eAgFnSc2d1	severní
Mayové	Mayová	k1gFnSc2	Mayová
byli	být	k5eAaImAgMnP	být
ještě	ještě	k6eAd1	ještě
stále	stále	k6eAd1	stále
velmi	velmi	k6eAd1	velmi
vyspělí	vyspělý	k2eAgMnPc1d1	vyspělý
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Mayská	mayský	k2eAgFnSc1d1	mayská
mytologie	mytologie	k1gFnSc1	mytologie
<g/>
,	,	kIx,	,
Popol	Popol	k1gInSc1	Popol
Vuh	Vuh	k1gFnSc2	Vuh
a	a	k8xC	a
Mayská	mayský	k2eAgFnSc1d1	mayská
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Mayové	May	k1gMnPc1	May
žili	žít	k5eAaImAgMnP	žít
v	v	k7c6	v
území	území	k1gNnSc6	území
bohatém	bohatý	k2eAgNnSc6d1	bohaté
na	na	k7c6	na
vegetaci	vegetace	k1gFnSc6	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
růstu	růst	k1gInSc2	růst
potravy	potrava	k1gFnSc2	potrava
představuje	představovat	k5eAaImIp3nS	představovat
proces	proces	k1gInSc1	proces
obnovení	obnovení	k1gNnSc2	obnovení
světa	svět	k1gInSc2	svět
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
při	při	k7c6	při
zacházení	zacházení	k1gNnSc6	zacházení
s	s	k7c7	s
rostlinami	rostlina	k1gFnPc7	rostlina
dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
zacházení	zacházení	k1gNnSc3	zacházení
se	s	k7c7	s
silami	síla	k1gFnPc7	síla
a	a	k8xC	a
mocemi	moc	k1gFnPc7	moc
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vegetaci	vegetace	k1gFnSc4	vegetace
a	a	k8xC	a
symboliku	symbolika	k1gFnSc4	symbolika
spojenou	spojený	k2eAgFnSc4d1	spojená
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
hrají	hrát	k5eAaImIp3nP	hrát
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
bohové	bůh	k1gMnPc1	bůh
Itzamna	Itzamno	k1gNnSc2	Itzamno
<g/>
,	,	kIx,	,
Ah	ah	k0	ah
K	k	k7c3	k
<g/>
'	'	kIx"	'
<g/>
in	in	k?	in
<g/>
,	,	kIx,	,
Hunab	Hunab	k1gInSc1	Hunab
Ku	k	k7c3	k
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
chápána	chápat	k5eAaImNgFnS	chápat
dvěma	dva	k4xCgInPc7	dva
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
jako	jako	k8xC	jako
čtverec	čtverec	k1gInSc1	čtverec
pole	pole	k1gNnSc2	pole
kukuřice	kukuřice	k1gFnSc2	kukuřice
nebo	nebo	k8xC	nebo
kruh	kruh	k1gInSc1	kruh
jako	jako	k8xC	jako
želva	želva	k1gFnSc1	želva
plovoucí	plovoucí	k2eAgFnSc1d1	plovoucí
na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
svět	svět	k1gInSc1	svět
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
na	na	k7c4	na
4	[number]	k4	4
světové	světový	k2eAgFnSc2d1	světová
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Vertikálně	vertikálně	k6eAd1	vertikálně
je	být	k5eAaImIp3nS	být
země	země	k1gFnSc1	země
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
3	[number]	k4	3
patra	patro	k1gNnSc2	patro
a	a	k8xC	a
13	[number]	k4	13
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
knihách	kniha	k1gFnPc6	kniha
jsou	být	k5eAaImIp3nP	být
zmíněny	zmínit	k5eAaPmNgInP	zmínit
mýty	mýtus	k1gInPc1	mýtus
o	o	k7c6	o
konci	konec	k1gInSc6	konec
světa	svět	k1gInSc2	svět
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
následné	následný	k2eAgFnSc3d1	následná
obnově	obnova	k1gFnSc3	obnova
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
mayské	mayský	k2eAgNnSc1d1	mayské
náboženství	náboženství	k1gNnSc1	náboženství
pracovalo	pracovat	k5eAaImAgNnS	pracovat
s	s	k7c7	s
cyklickým	cyklický	k2eAgInSc7d1	cyklický
časem	čas	k1gInSc7	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
čas	čas	k1gInSc1	čas
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
kruzích	kruh	k1gInPc6	kruh
a	a	k8xC	a
ne	ne	k9	ne
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
do	do	k7c2	do
konce	konec	k1gInSc2	konec
<g/>
.	.	kIx.	.
</s>
<s>
Stvoření	stvoření	k1gNnSc1	stvoření
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
neustálým	neustálý	k2eAgInSc7d1	neustálý
procesem	proces	k1gInSc7	proces
zasévání	zasévání	k1gNnSc2	zasévání
a	a	k8xC	a
úsvitu	úsvit	k1gInSc2	úsvit
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Strom	strom	k1gInSc1	strom
světa	svět	k1gInSc2	svět
spojuje	spojovat	k5eAaImIp3nS	spojovat
tři	tři	k4xCgNnPc4	tři
patra	patro	k1gNnPc4	patro
světa	svět	k1gInSc2	svět
–	–	k?	–
podsvětí	podsvětí	k1gNnSc1	podsvětí
<g/>
,	,	kIx,	,
prostřední	prostřední	k2eAgInSc4d1	prostřední
svět	svět	k1gInSc4	svět
a	a	k8xC	a
nebe	nebe	k1gNnSc4	nebe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podsvětí	podsvětí	k1gNnSc6	podsvětí
měl	mít	k5eAaImAgInS	mít
kořeny	kořen	k1gInPc4	kořen
<g/>
,	,	kIx,	,
v	v	k7c6	v
prostředním	prostřední	k2eAgInSc6d1	prostřední
světě	svět	k1gInSc6	svět
kmen	kmen	k1gInSc1	kmen
a	a	k8xC	a
v	v	k7c6	v
nebesích	nebesa	k1gNnPc6	nebesa
korunu	koruna	k1gFnSc4	koruna
s	s	k7c7	s
větvemi	větev	k1gFnPc7	větev
<g/>
.	.	kIx.	.
</s>
<s>
Představuje	představovat	k5eAaImIp3nS	představovat
vesmírnou	vesmírný	k2eAgFnSc4d1	vesmírná
osu	osa	k1gFnSc4	osa
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
pohybjí	pohybjet	k5eAaImIp3nP	pohybjet
duše	duše	k1gFnPc1	duše
zemřelých	zemřelý	k1gMnPc2	zemřelý
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
také	také	k9	také
vyznamným	vyznamný	k2eAgInSc7d1	vyznamný
symbolem	symbol	k1gInSc7	symbol
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
symbolem	symbol	k1gInSc7	symbol
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
vertikálnosti	vertikálnost	k1gFnSc2	vertikálnost
<g/>
,	,	kIx,	,
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
mužské	mužský	k2eAgFnSc2d1	mužská
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
věku	věk	k1gInSc2	věk
a	a	k8xC	a
obnovy	obnova	k1gFnSc2	obnova
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
posvátnou	posvátný	k2eAgFnSc7d1	posvátná
energií	energie	k1gFnSc7	energie
zajišťující	zajišťující	k2eAgFnSc4d1	zajišťující
obnovu	obnova	k1gFnSc4	obnova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
individuu	individuum	k1gNnSc6	individuum
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
duší	duše	k1gFnPc2	duše
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
stín	stín	k1gInSc4	stín
<g/>
,	,	kIx,	,
dech	dech	k1gInSc4	dech
<g/>
,	,	kIx,	,
krev	krev	k1gFnSc4	krev
a	a	k8xC	a
kost	kost	k1gFnSc4	kost
<g/>
.	.	kIx.	.
</s>
<s>
Ztráta	ztráta	k1gFnSc1	ztráta
duše	duše	k1gFnSc2	duše
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
specifické	specifický	k2eAgFnSc3d1	specifická
nemoci	nemoc	k1gFnSc3	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
existují	existovat	k5eAaImIp3nP	existovat
esence	esence	k1gFnPc1	esence
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
zvířata	zvíře	k1gNnPc4	zvíře
nebo	nebo	k8xC	nebo
přírodní	přírodní	k2eAgFnPc4d1	přírodní
události	událost	k1gFnPc4	událost
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
danou	daný	k2eAgFnSc7d1	daná
osobou	osoba	k1gFnSc7	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
ho	on	k3xPp3gMnSc4	on
chrání	chránit	k5eAaImIp3nS	chránit
<g/>
.	.	kIx.	.
</s>
<s>
Vladař	vladař	k1gMnSc1	vladař
(	(	kIx(	(
<g/>
Mah	Mah	k?	Mah
K	K	kA	K
<g/>
'	'	kIx"	'
<g/>
in	in	k?	in
nebo	nebo	k8xC	nebo
Ahau	Ahaus	k1gInSc2	Ahaus
<g/>
)	)	kIx)	)
představuje	představovat	k5eAaImIp3nS	představovat
střed	střed	k1gInSc4	střed
všeho	všecek	k3xTgNnSc2	všecek
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
i	i	k8xC	i
na	na	k7c6	na
nebesích	nebesa	k1gNnPc6	nebesa
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgInS	mít
neomezenou	omezený	k2eNgFnSc4d1	neomezená
moc	moc	k1gFnSc4	moc
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
(	(	kIx(	(
<g/>
náboženské	náboženský	k2eAgInPc4d1	náboženský
obřady	obřad	k1gInPc4	obřad
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc4	obchod
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
řemesla	řemeslo	k1gNnSc2	řemeslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spojuje	spojovat	k5eAaImIp3nS	spojovat
pozemský	pozemský	k2eAgInSc4d1	pozemský
svět	svět	k1gInSc4	svět
s	s	k7c7	s
nadpřirozeným	nadpřirozený	k2eAgInSc7d1	nadpřirozený
světem	svět	k1gInSc7	svět
bohů	bůh	k1gMnPc2	bůh
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
komunikaci	komunikace	k1gFnSc4	komunikace
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Nosil	nosit	k5eAaImAgInS	nosit
zdobené	zdobený	k2eAgInPc4d1	zdobený
pásy	pás	k1gInPc4	pás
<g/>
,	,	kIx,	,
náprsenky	náprsenka	k1gFnPc4	náprsenka
<g/>
,	,	kIx,	,
nákolenky	nákolenek	k1gInPc4	nákolenek
<g/>
,	,	kIx,	,
náramky	náramek	k1gInPc4	náramek
a	a	k8xC	a
obrovské	obrovský	k2eAgFnPc4d1	obrovská
čelenky	čelenka	k1gFnPc4	čelenka
zoomorfních	zoomorfní	k2eAgInPc2d1	zoomorfní
tvarů	tvar	k1gInPc2	tvar
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
symbolizovaly	symbolizovat	k5eAaImAgFnP	symbolizovat
božstva	božstvo	k1gNnSc2	božstvo
<g/>
.	.	kIx.	.
</s>
<s>
Předci	předek	k1gMnPc1	předek
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
přítomnou	přítomný	k2eAgFnSc7d1	přítomná
silou	síla	k1gFnSc7	síla
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
jsou	být	k5eAaImIp3nP	být
přiřazeni	přiřadit	k5eAaPmNgMnP	přiřadit
k	k	k7c3	k
specifickým	specifický	k2eAgNnPc3d1	specifické
místům	místo	k1gNnPc3	místo
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
horám	hora	k1gFnPc3	hora
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
zde	zde	k6eAd1	zde
také	také	k9	také
kult	kult	k1gInSc1	kult
hrdinů	hrdina	k1gMnPc2	hrdina
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yIgFnPc3	který
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
modlit	modlit	k5eAaImF	modlit
a	a	k8xC	a
které	který	k3yQgFnPc4	který
lze	lze	k6eAd1	lze
uctívat	uctívat	k5eAaImF	uctívat
<g/>
.	.	kIx.	.
</s>
<s>
Mayské	mayský	k2eAgNnSc1d1	mayské
náboženství	náboženství	k1gNnSc1	náboženství
je	být	k5eAaImIp3nS	být
polyteistické	polyteistický	k2eAgNnSc1d1	polyteistické
<g/>
.	.	kIx.	.
</s>
<s>
Bohové	bůh	k1gMnPc1	bůh
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
spojeni	spojit	k5eAaPmNgMnP	spojit
s	s	k7c7	s
přírodními	přírodní	k2eAgInPc7d1	přírodní
úkazy	úkaz	k1gInPc7	úkaz
<g/>
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
zde	zde	k6eAd1	zde
bohy	bůh	k1gMnPc7	bůh
slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
plodin	plodina	k1gFnPc2	plodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
bohy	bůh	k1gMnPc4	bůh
smrti	smrt	k1gFnSc2	smrt
<g/>
,	,	kIx,	,
manželství	manželství	k1gNnSc2	manželství
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
nadpřirozené	nadpřirozený	k2eAgFnPc1d1	nadpřirozená
bytosti	bytost	k1gFnPc1	bytost
jsou	být	k5eAaImIp3nP	být
zvířecí	zvířecí	k2eAgMnPc1d1	zvířecí
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Zvířecí	zvířecí	k2eAgMnPc1d1	zvířecí
lidé	člověk	k1gMnPc1	člověk
jsou	být	k5eAaImIp3nP	být
bytosti	bytost	k1gFnSc3	bytost
zastávající	zastávající	k2eAgFnSc4d1	zastávající
určitou	určitý	k2eAgFnSc4d1	určitá
sociální	sociální	k2eAgFnSc4d1	sociální
roli	role	k1gFnSc4	role
(	(	kIx(	(
<g/>
léčitelé	léčitel	k1gMnPc1	léčitel
<g/>
,	,	kIx,	,
soudci	soudce	k1gMnPc1	soudce
<g/>
,	,	kIx,	,
spisovatelé	spisovatel	k1gMnPc1	spisovatel
<g/>
,	,	kIx,	,
hudebníci	hudebník	k1gMnPc1	hudebník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
zde	zde	k6eAd1	zde
také	také	k9	také
různé	různý	k2eAgFnPc1d1	různá
škodolibé	škodolibý	k2eAgFnPc1d1	škodolibá
příšery	příšera	k1gFnPc1	příšera
a	a	k8xC	a
démoni	démon	k1gMnPc1	démon
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
tvorové	tvor	k1gMnPc1	tvor
pomocníci	pomocník	k1gMnPc1	pomocník
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
trpaslíci	trpaslík	k1gMnPc1	trpaslík
a	a	k8xC	a
golemové	golem	k1gMnPc1	golem
<g/>
.	.	kIx.	.
</s>
<s>
Kosmický	kosmický	k2eAgInSc1d1	kosmický
řád	řád	k1gInSc1	řád
prostupuje	prostupovat	k5eAaImIp3nS	prostupovat
všechny	všechen	k3xTgFnPc4	všechen
roviny	rovina	k1gFnPc4	rovina
a	a	k8xC	a
dimenze	dimenze	k1gFnSc2	dimenze
a	a	k8xC	a
světa	svět	k1gInSc2	svět
a	a	k8xC	a
pro	pro	k7c4	pro
zachování	zachování	k1gNnSc4	zachování
společnosti	společnost	k1gFnSc2	společnost
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
její	její	k3xOp3gNnSc4	její
připodobnění	připodobnění	k1gNnSc4	připodobnění
ke	k	k7c3	k
kosmickému	kosmický	k2eAgInSc3d1	kosmický
řádu	řád	k1gInSc3	řád
<g/>
.	.	kIx.	.
</s>
<s>
Rituály	rituál	k1gInPc1	rituál
probíhaly	probíhat	k5eAaImAgInP	probíhat
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
spletitém	spletitý	k2eAgInSc6d1	spletitý
systému	systém	k1gInSc6	systém
kalendářů	kalendář	k1gInPc2	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Města	město	k1gNnPc1	město
byla	být	k5eAaImAgNnP	být
stavěna	stavit	k5eAaImNgNnP	stavit
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
kosmickými	kosmický	k2eAgNnPc7d1	kosmické
tělesy	těleso	k1gNnPc7	těleso
a	a	k8xC	a
jevy	jev	k1gInPc7	jev
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
3	[number]	k4	3
budovy	budova	k1gFnPc4	budova
umístěné	umístěný	k2eAgFnPc1d1	umístěná
vůči	vůči	k7c3	vůči
jedné	jeden	k4xCgFnSc2	jeden
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ukazovaly	ukazovat	k5eAaImAgInP	ukazovat
roční	roční	k2eAgFnSc4d1	roční
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
slunce	slunce	k1gNnSc1	slunce
vycházelo	vycházet	k5eAaImAgNnS	vycházet
nad	nad	k7c7	nad
první	první	k4xOgFnSc7	první
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
letní	letní	k2eAgInSc4d1	letní
slunovrat	slunovrat	k1gInSc4	slunovrat
a	a	k8xC	a
pokud	pokud	k8xS	pokud
vycházelo	vycházet	k5eAaImAgNnS	vycházet
nad	nad	k7c7	nad
třetí	třetí	k4xOgFnSc7	třetí
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
zimní	zimní	k2eAgInSc4d1	zimní
slunovrat	slunovrat	k1gInSc4	slunovrat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
vycházelo	vycházet	k5eAaImAgNnS	vycházet
nad	nad	k7c4	nad
prostřední	prostřední	k2eAgFnSc4d1	prostřední
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
rovnodennost	rovnodennost	k1gFnSc1	rovnodennost
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
mayském	mayský	k2eAgNnSc6d1	mayské
náboženství	náboženství	k1gNnSc6	náboženství
Xibalba	Xibalba	k1gFnSc1	Xibalba
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
podsvětí	podsvětí	k1gNnSc1	podsvětí
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
to	ten	k3xDgNnSc1	ten
ucelený	ucelený	k2eAgInSc1d1	ucelený
koncept	koncept	k1gInSc1	koncept
posmrtného	posmrtný	k2eAgInSc2d1	posmrtný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
společenství	společenství	k1gNnPc1	společenství
věřila	věřit	k5eAaImAgNnP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
doprovázejí	doprovázet	k5eAaImIp3nP	doprovázet
svého	svůj	k3xOyFgMnSc4	svůj
mrtvého	mrtvý	k2eAgMnSc4d1	mrtvý
krále	král	k1gMnSc4	král
do	do	k7c2	do
Xibalby	Xibalba	k1gFnSc2	Xibalba
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
prochází	procházet	k5eAaImIp3nS	procházet
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
vladaři	vladař	k1gMnPc1	vladař
dostávají	dostávat	k5eAaImIp3nP	dostávat
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
čelí	čelit	k5eAaImIp3nS	čelit
množství	množství	k1gNnSc1	množství
nástrah	nástraha	k1gFnPc2	nástraha
–	–	k?	–
pokud	pokud	k8xS	pokud
je	on	k3xPp3gMnPc4	on
překonají	překonat	k5eAaPmIp3nP	překonat
<g/>
,	,	kIx,	,
dostanou	dostat	k5eAaPmIp3nP	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
nebesa	nebesa	k1gNnPc4	nebesa
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
převzali	převzít	k5eAaPmAgMnP	převzít
model	model	k1gInSc4	model
pekla	peklo	k1gNnSc2	peklo
a	a	k8xC	a
nebe	nebe	k1gNnSc2	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
proměny	proměna	k1gFnSc2	proměna
v	v	k7c4	v
zvíře	zvíře	k1gNnSc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
podobný	podobný	k2eAgInSc4d1	podobný
princip	princip	k1gInSc4	princip
jako	jako	k8xS	jako
u	u	k7c2	u
vlkodlaků	vlkodlak	k1gMnPc2	vlkodlak
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
mayů	may	k1gMnPc2	may
s	s	k7c7	s
bohy	bůh	k1gMnPc7	bůh
funguje	fungovat	k5eAaImIp3nS	fungovat
na	na	k7c6	na
principu	princip	k1gInSc6	princip
vzájemnosti	vzájemnost	k1gFnSc2	vzájemnost
–	–	k?	–
bohové	bůh	k1gMnPc1	bůh
pečují	pečovat	k5eAaImIp3nP	pečovat
o	o	k7c4	o
lidi	člověk	k1gMnPc4	člověk
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
o	o	k7c4	o
ně	on	k3xPp3gFnPc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Bohové	bůh	k1gMnPc1	bůh
svoji	svůj	k3xOyFgFnSc4	svůj
přízeň	přízeň	k1gFnSc4	přízeň
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
pomocí	pomocí	k7c2	pomocí
úrody	úroda	k1gFnSc2	úroda
<g/>
,	,	kIx,	,
deště	dešť	k1gInSc2	dešť
nebo	nebo	k8xC	nebo
slunečního	sluneční	k2eAgInSc2d1	sluneční
svitu	svit	k1gInSc2	svit
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
o	o	k7c4	o
ně	on	k3xPp3gFnPc4	on
pečují	pečovat	k5eAaImIp3nP	pečovat
pomocí	pomocí	k7c2	pomocí
uctívání	uctívání	k1gNnSc2	uctívání
bohů	bůh	k1gMnPc2	bůh
a	a	k8xC	a
obnovování	obnovování	k1gNnSc1	obnovování
jejich	jejich	k3xOp3gFnSc2	jejich
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
dělo	dít	k5eAaImAgNnS	dít
pomocí	pomoc	k1gFnSc7	pomoc
různých	různý	k2eAgInPc2d1	různý
obřadů	obřad	k1gInPc2	obřad
<g/>
,	,	kIx,	,
např.	např.	kA	např.
obřadů	obřad	k1gInPc2	obřad
prolévání	prolévání	k1gNnSc4	prolévání
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Mayská	mayský	k2eAgFnSc1d1	mayská
krajina	krajina	k1gFnSc1	krajina
je	být	k5eAaImIp3nS	být
rituální	rituální	k2eAgFnSc7d1	rituální
topografií	topografie	k1gFnSc7	topografie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
památky	památka	k1gFnPc1	památka
jako	jako	k8xC	jako
hory	hora	k1gFnPc1	hora
<g/>
,	,	kIx,	,
studny	studna	k1gFnPc1	studna
a	a	k8xC	a
jeskyně	jeskyně	k1gFnPc1	jeskyně
přiděleny	přidělen	k2eAgFnPc1d1	přidělena
specifickým	specifický	k2eAgMnPc3d1	specifický
předkům	předek	k1gMnPc3	předek
a	a	k8xC	a
božstvům	božstvo	k1gNnPc3	božstvo
<g/>
.	.	kIx.	.
</s>
<s>
Dary	dar	k1gInPc1	dar
slouží	sloužit	k5eAaImIp3nP	sloužit
pro	pro	k7c4	pro
obnovu	obnova	k1gFnSc4	obnova
vztahu	vztah	k1gInSc2	vztah
s	s	k7c7	s
druhým	druhý	k4xOgInSc7	druhý
světem	svět	k1gInSc7	svět
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
množství	množství	k1gNnSc1	množství
<g/>
,	,	kIx,	,
příprava	příprava	k1gFnSc1	příprava
a	a	k8xC	a
aranžmá	aranžmá	k1gNnSc1	aranžmá
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
striktními	striktní	k2eAgNnPc7d1	striktní
pravidly	pravidlo	k1gNnPc7	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
nabízené	nabízený	k2eAgInPc1d1	nabízený
byli	být	k5eAaImAgMnP	být
kukuřičné	kukuřičný	k2eAgInPc4d1	kukuřičný
chleby	chléb	k1gInPc4	chléb
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnPc4	kukuřice
<g/>
,	,	kIx,	,
nápoje	nápoj	k1gInPc4	nápoj
z	z	k7c2	z
kakaa	kakao	k1gNnSc2	kakao
<g/>
,	,	kIx,	,
med	med	k1gInSc1	med
<g/>
,	,	kIx,	,
květiny	květina	k1gFnPc1	květina
<g/>
,	,	kIx,	,
gumové	gumový	k2eAgFnPc1d1	gumová
figury	figura	k1gFnPc1	figura
a	a	k8xC	a
cigarety	cigareta	k1gFnPc1	cigareta
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
dary	dar	k1gInPc4	dar
také	také	k9	také
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
skladovány	skladovat	k5eAaImNgFnP	skladovat
a	a	k8xC	a
zakopány	zakopat	k5eAaPmNgFnP	zakopat
pod	pod	k7c7	pod
architekturou	architektura	k1gFnSc7	architektura
–	–	k?	–
podlahami	podlaha	k1gFnPc7	podlaha
<g/>
,	,	kIx,	,
pámátníky	pámátník	k1gMnPc7	pámátník
a	a	k8xC	a
oltáři	oltář	k1gInSc3	oltář
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
se	se	k3xPyFc4	se
obětují	obětovat	k5eAaBmIp3nP	obětovat
krocani	krocan	k1gMnPc1	krocan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
Španělů	Španěl	k1gMnPc2	Španěl
se	se	k3xPyFc4	se
obětovali	obětovat	k5eAaBmAgMnP	obětovat
jeleni	jelen	k1gMnPc1	jelen
<g/>
,	,	kIx,	,
psi	pes	k1gMnPc1	pes
<g/>
,	,	kIx,	,
ryby	ryba	k1gFnPc1	ryba
a	a	k8xC	a
na	na	k7c6	na
zvláštní	zvláštní	k2eAgFnSc6d1	zvláštní
příležitosti	příležitost	k1gFnSc6	příležitost
(	(	kIx(	(
<g/>
vstup	vstup	k1gInSc1	vstup
nového	nový	k2eAgMnSc2d1	nový
panovníka	panovník	k1gMnSc2	panovník
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
sucho	sucho	k1gNnSc1	sucho
<g/>
,	,	kIx,	,
závažná	závažný	k2eAgFnSc1d1	závažná
nemoc	nemoc	k1gFnSc1	nemoc
panovníka	panovník	k1gMnSc2	panovník
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
obětovali	obětovat	k5eAaBmAgMnP	obětovat
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
dospělí	dospělí	k1gMnPc1	dospělí
i	i	k8xC	i
děti	dítě	k1gFnPc1	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Kanibalismus	kanibalismus	k1gInSc1	kanibalismus
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
vzácný	vzácný	k2eAgMnSc1d1	vzácný
<g/>
.	.	kIx.	.
</s>
<s>
Rituální	rituální	k2eAgNnSc1d1	rituální
prolévání	prolévání	k1gNnSc1	prolévání
krve	krev	k1gFnSc2	krev
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
každodenních	každodenní	k2eAgInPc2d1	každodenní
veřejných	veřejný	k2eAgInPc2d1	veřejný
rituálů	rituál	k1gInPc2	rituál
a	a	k8xC	a
hrálo	hrát	k5eAaImAgNnS	hrát
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
roli	role	k1gFnSc4	role
v	v	k7c6	v
obětní	obětní	k2eAgFnSc6d1	obětní
činnosti	činnost	k1gFnSc6	činnost
vyšších	vysoký	k2eAgFnPc2d2	vyšší
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
doprovázelo	doprovázet	k5eAaImAgNnS	doprovázet
svěcení	svěcení	k1gNnSc1	svěcení
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
zrození	zrození	k1gNnSc1	zrození
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
svatební	svatební	k2eAgInPc4d1	svatební
obřady	obřad	k1gInPc4	obřad
<g/>
,	,	kIx,	,
veškeré	veškerý	k3xTgNnSc4	veškerý
politické	politický	k2eAgNnSc4d1	politické
dění	dění	k1gNnSc4	dění
<g/>
,	,	kIx,	,
kalendářní	kalendářní	k2eAgInSc4d1	kalendářní
mezníky	mezník	k1gInPc4	mezník
a	a	k8xC	a
rituály	rituál	k1gInPc4	rituál
životního	životní	k2eAgInSc2d1	životní
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Provádělo	provádět	k5eAaImAgNnS	provádět
se	se	k3xPyFc4	se
krvácením	krvácení	k1gNnSc7	krvácení
z	z	k7c2	z
stehna	stehno	k1gNnSc2	stehno
<g/>
,	,	kIx,	,
jazyka	jazyk	k1gInSc2	jazyk
nebo	nebo	k8xC	nebo
genitálií	genitálie	k1gFnPc2	genitálie
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
typem	typ	k1gInSc7	typ
prolévání	prolévání	k1gNnSc2	prolévání
krve	krev	k1gFnSc2	krev
byly	být	k5eAaImAgInP	být
obřady	obřad	k1gInPc1	obřad
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
vladařem	vladař	k1gMnSc7	vladař
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
při	při	k7c6	při
nástupu	nástup	k1gInSc6	nástup
vladaře	vladař	k1gMnSc2	vladař
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
se	s	k7c7	s
prolitím	prolití	k1gNnSc7	prolití
krve	krev	k1gFnSc2	krev
otevřela	otevřít	k5eAaPmAgFnS	otevřít
cesta	cesta	k1gFnSc1	cesta
nadpřirozeným	nadpřirozený	k2eAgFnPc3d1	nadpřirozená
silám	síla	k1gFnPc3	síla
a	a	k8xC	a
předkům	předek	k1gMnPc3	předek
do	do	k7c2	do
světa	svět	k1gInSc2	svět
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
případem	případ	k1gInSc7	případ
bylo	být	k5eAaImAgNnS	být
krvácení	krvácení	k1gNnSc1	krvácení
z	z	k7c2	z
genitálií	genitálie	k1gFnPc2	genitálie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
panovník	panovník	k1gMnSc1	panovník
snažil	snažit	k5eAaImAgMnS	snažit
navodit	navodit	k5eAaPmF	navodit
vize	vize	k1gFnPc4	vize
a	a	k8xC	a
spojit	spojit	k5eAaPmF	spojit
se	se	k3xPyFc4	se
s	s	k7c7	s
bohy	bůh	k1gMnPc7	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
spojení	spojení	k1gNnSc4	spojení
mužské	mužský	k2eAgFnSc2d1	mužská
podstaty	podstata	k1gFnSc2	podstata
se	s	k7c7	s
ženskou	ženská	k1gFnSc7	ženská
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vladař	vladař	k1gMnSc1	vladař
stával	stávat	k5eAaImAgMnS	stávat
matkou	matka	k1gFnSc7	matka
opatrovatelkou	opatrovatelka	k1gFnSc7	opatrovatelka
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgFnPc1d1	tradiční
Mayové	Mayová	k1gFnPc1	Mayová
mšli	mšli	k6eAd1	mšli
své	svůj	k3xOyFgMnPc4	svůj
vlastní	vlastní	k2eAgMnPc4d1	vlastní
náboženské	náboženský	k2eAgMnPc4d1	náboženský
funkcionáře	funkcionář	k1gMnPc4	funkcionář
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
hierarchicky	hierarchicky	k6eAd1	hierarchicky
organizované	organizovaný	k2eAgFnSc2d1	organizovaná
a	a	k8xC	a
pověřené	pověřený	k2eAgFnSc2d1	pověřená
povinností	povinnost	k1gFnSc7	povinnost
modlit	modlit	k5eAaImF	modlit
se	se	k3xPyFc4	se
a	a	k8xC	a
obětovat	obětovat	k5eAaBmF	obětovat
za	za	k7c4	za
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
místní	místní	k2eAgFnPc4d1	místní
skupiny	skupina	k1gFnPc4	skupina
nebo	nebo	k8xC	nebo
celou	celý	k2eAgFnSc4d1	celá
komunitu	komunita	k1gFnSc4	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Důležitější	důležitý	k2eAgInPc4d2	Důležitější
rituály	rituál	k1gInPc4	rituál
předcházela	předcházet	k5eAaImAgFnS	předcházet
očista	očista	k1gFnSc1	očista
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
půstem	půst	k1gInSc7	půst
<g/>
,	,	kIx,	,
sexuální	sexuální	k2eAgFnSc7d1	sexuální
abstinencí	abstinence	k1gFnSc7	abstinence
nebo	nebo	k8xC	nebo
koupáním	koupání	k1gNnSc7	koupání
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
náboženského	náboženský	k2eAgInSc2d1	náboženský
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
také	také	k9	také
pouť	pouť	k1gFnSc1	pouť
a	a	k8xC	a
hostina	hostina	k1gFnSc1	hostina
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
děla	dít	k5eAaBmAgFnS	dít
dnes	dnes	k6eAd1	dnes
i	i	k9	i
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byla	být	k5eAaImAgFnS	být
sponzorována	sponzorovat	k5eAaImNgFnS	sponzorovat
bohatými	bohatý	k2eAgInPc7d1	bohatý
a	a	k8xC	a
sloužily	sloužit	k5eAaImAgFnP	sloužit
k	k	k7c3	k
redistribuci	redistribuce	k1gFnSc3	redistribuce
jídla	jídlo	k1gNnSc2	jídlo
a	a	k8xC	a
pití	pití	k1gNnSc2	pití
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
upevňuje	upevňovat	k5eAaImIp3nS	upevňovat
komunitu	komunita	k1gFnSc4	komunita
jak	jak	k6eAd1	jak
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
a	a	k8xC	a
bohy	bůh	k1gMnPc7	bůh
<g/>
,	,	kIx,	,
tak	tak	k9	tak
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
navzájem	navzájem	k6eAd1	navzájem
<g/>
.	.	kIx.	.
</s>
<s>
Životní	životní	k2eAgInSc1d1	životní
cyklus	cyklus	k1gInSc1	cyklus
–	–	k?	–
rituály	rituál	k1gInPc1	rituál
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
narozením	narození	k1gNnSc7	narození
<g/>
,	,	kIx,	,
dospíváním	dospívání	k1gNnSc7	dospívání
<g/>
,	,	kIx,	,
svatbou	svatba	k1gFnSc7	svatba
a	a	k8xC	a
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Zdraví	zdravit	k5eAaImIp3nP	zdravit
–	–	k?	–
rituály	rituál	k1gInPc1	rituál
soustředěné	soustředěný	k2eAgInPc1d1	soustředěný
na	na	k7c4	na
léčení	léčení	k1gNnSc4	léčení
pomocí	pomocí	k7c2	pomocí
navrácení	navrácení	k1gNnSc2	navrácení
ztracené	ztracený	k2eAgFnSc2d1	ztracená
duše	duše	k1gFnSc2	duše
nebo	nebo	k8xC	nebo
části	část	k1gFnSc2	část
duše	duše	k1gFnSc2	duše
zpátky	zpátky	k6eAd1	zpátky
do	do	k7c2	do
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Počasí	počasí	k1gNnSc1	počasí
–	–	k?	–
rituály	rituál	k1gInPc4	rituál
přivolání	přivolání	k1gNnSc2	přivolání
deště	dešť	k1gInPc4	dešť
<g/>
,	,	kIx,	,
uvěznění	uvěznění	k1gNnSc1	uvěznění
větru	vítr	k1gInSc2	vítr
nebo	nebo	k8xC	nebo
zapečetění	zapečetění	k1gNnSc2	zapečetění
zimy	zima	k1gFnSc2	zima
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
–	–	k?	–
rituály	rituál	k1gInPc1	rituál
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
zasetím	zasetí	k1gNnSc7	zasetí
a	a	k8xC	a
sklizením	sklizení	k1gNnSc7	sklizení
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
příležitostmi	příležitost	k1gFnPc7	příležitost
byli	být	k5eAaImAgMnP	být
oslavy	oslava	k1gFnPc4	oslava
lovu	lov	k1gInSc2	lov
<g/>
,	,	kIx,	,
získání	získání	k1gNnSc4	získání
teritoria	teritorium	k1gNnSc2	teritorium
<g/>
,	,	kIx,	,
události	událost	k1gFnPc4	událost
spojené	spojený	k2eAgFnPc4d1	spojená
s	s	k7c7	s
vládcem	vládce	k1gMnSc7	vládce
a	a	k8xC	a
uctívání	uctívání	k1gNnSc3	uctívání
předků	předek	k1gInPc2	předek
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
zdroje	zdroj	k1gInPc1	zdroj
sloužily	sloužit	k5eAaImAgInP	sloužit
3	[number]	k4	3
autentické	autentický	k2eAgFnPc4d1	autentická
knihy	kniha	k1gFnPc4	kniha
hieroglyfů	hieroglyf	k1gInPc2	hieroglyf
<g/>
:	:	kIx,	:
Drážďanský	drážďanský	k2eAgInSc1d1	drážďanský
kodex	kodex	k1gInSc1	kodex
<g/>
,	,	kIx,	,
Madridský	madridský	k2eAgInSc1d1	madridský
kodex	kodex	k1gInSc1	kodex
a	a	k8xC	a
Pařížský	pařížský	k2eAgInSc1d1	pařížský
kodex	kodex	k1gInSc1	kodex
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
máme	mít	k5eAaImIp1nP	mít
zdroje	zdroj	k1gInPc4	zdroj
od	od	k7c2	od
samotných	samotný	k2eAgMnPc2d1	samotný
kolonizátorů	kolonizátor	k1gMnPc2	kolonizátor
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
jsou	být	k5eAaImIp3nP	být
Popol	Popol	k1gInSc1	Popol
Vuh	Vuh	k1gMnPc1	Vuh
<g/>
,	,	kIx,	,
Knihy	kniha	k1gFnPc1	kniha
Chilama	Chilama	k1gNnSc1	Chilama
Balama	Balama	k?	Balama
a	a	k8xC	a
Letopisy	letopis	k1gInPc7	letopis
Cakchiquelů	Cakchiquel	k1gMnPc2	Cakchiquel
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
zdrojů	zdroj	k1gInPc2	zdroj
tvoří	tvořit	k5eAaImIp3nP	tvořit
vykopávky	vykopávka	k1gFnPc4	vykopávka
či	či	k8xC	či
antropologické	antropologický	k2eAgFnPc4d1	antropologická
studie	studie	k1gFnPc4	studie
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
duchovní	duchovní	k2eAgFnPc1d1	duchovní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
materiální	materiální	k2eAgInPc1d1	materiální
úspěchy	úspěch	k1gInPc1	úspěch
Mayů	May	k1gMnPc2	May
se	se	k3xPyFc4	se
zakládaly	zakládat	k5eAaImAgFnP	zakládat
na	na	k7c4	na
originální	originální	k2eAgInPc4d1	originální
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
jednoduché	jednoduchý	k2eAgFnSc6d1	jednoduchá
číselné	číselný	k2eAgFnSc6d1	číselná
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc4	její
základ	základ	k1gInSc4	základ
tvořilo	tvořit	k5eAaImAgNnS	tvořit
dvacet	dvacet	k4xCc1	dvacet
číslic	číslice	k1gFnPc2	číslice
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
nuly	nula	k1gFnSc2	nula
(	(	kIx(	(
<g/>
starověké	starověký	k2eAgFnSc2d1	starověká
kultury	kultura	k1gFnSc2	kultura
nulu	nula	k1gFnSc4	nula
většinou	většinou	k6eAd1	většinou
neznaly	znát	k5eNaImAgFnP	znát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Znalosti	znalost	k1gFnPc1	znalost
nuly	nula	k1gFnSc2	nula
a	a	k8xC	a
matematických	matematický	k2eAgFnPc2d1	matematická
operací	operace	k1gFnPc2	operace
s	s	k7c7	s
ní	on	k3xPp3gFnSc6	on
spojených	spojený	k2eAgInPc2d1	spojený
se	se	k3xPyFc4	se
připisuje	připisovat	k5eAaImIp3nS	připisovat
velká	velký	k2eAgFnSc1d1	velká
důležitost	důležitost	k1gFnSc1	důležitost
<g/>
.	.	kIx.	.
</s>
<s>
Ukazují	ukazovat	k5eAaImIp3nP	ukazovat
na	na	k7c4	na
neobvyklou	obvyklý	k2eNgFnSc4d1	neobvyklá
intelektuální	intelektuální	k2eAgFnSc4d1	intelektuální
vyspělost	vyspělost	k1gFnSc4	vyspělost
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Praktičnost	praktičnost	k1gFnSc1	praktičnost
a	a	k8xC	a
využitelnost	využitelnost	k1gFnSc1	využitelnost
mayského	mayský	k2eAgInSc2d1	mayský
způsobu	způsob	k1gInSc2	způsob
zápisu	zápis	k1gInSc2	zápis
čísel	číslo	k1gNnPc2	číslo
je	být	k5eAaImIp3nS	být
porovnatelná	porovnatelný	k2eAgFnSc1d1	porovnatelná
s	s	k7c7	s
arabskou	arabský	k2eAgFnSc7d1	arabská
soustavou	soustava	k1gFnSc7	soustava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
ve	v	k7c6	v
světe	svět	k1gInSc5	svět
nejvíce	hodně	k6eAd3	hodně
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
<g/>
.	.	kIx.	.
</s>
<s>
Mayově	Mayův	k2eAgFnSc3d1	Mayova
však	však	k9	však
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
překonali	překonat	k5eAaPmAgMnP	překonat
sami	sám	k3xTgMnPc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Používali	používat	k5eAaImAgMnP	používat
totiž	totiž	k9	totiž
i	i	k9	i
druhý	druhý	k4xOgInSc1	druhý
<g/>
,	,	kIx,	,
paralelní	paralelní	k2eAgInSc1d1	paralelní
matematický	matematický	k2eAgInSc1d1	matematický
aparát	aparát	k1gInSc1	aparát
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
deseti	deset	k4xCc6	deset
základních	základní	k2eAgFnPc6d1	základní
číslicích	číslice	k1gFnPc6	číslice
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
praktické	praktický	k2eAgInPc4d1	praktický
účely	účel	k1gInPc4	účel
se	se	k3xPyFc4	se
však	však	k9	však
více	hodně	k6eAd2	hodně
využívala	využívat	k5eAaPmAgFnS	využívat
dvacítková	dvacítkový	k2eAgFnSc1d1	dvacítková
soustava	soustava	k1gFnSc1	soustava
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgFnPc4	který
se	se	k3xPyFc4	se
zakládal	zakládat	k5eAaImAgMnS	zakládat
i	i	k9	i
mayský	mayský	k2eAgInSc4d1	mayský
kalendář	kalendář	k1gInSc4	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Mayský	mayský	k2eAgInSc4d1	mayský
kalendář	kalendář	k1gInSc4	kalendář
<g/>
.	.	kIx.	.
</s>
<s>
Gilbert	Gilbert	k1gMnSc1	Gilbert
<g/>
,	,	kIx,	,
A.	A.	kA	A.
G.	G.	kA	G.
<g/>
,	,	kIx,	,
Cotterell	Cotterell	k1gMnSc1	Cotterell
<g/>
,	,	kIx,	,
M.	M.	kA	M.
M	M	kA	M
<g/>
:	:	kIx,	:
Mayská	mayský	k2eAgNnPc1d1	mayské
proroctví	proroctví	k1gNnPc1	proroctví
<g/>
.	.	kIx.	.
</s>
<s>
Odkrývání	odkrývání	k1gNnSc1	odkrývání
tajemství	tajemství	k1gNnSc2	tajemství
ztracené	ztracený	k2eAgFnSc2d1	ztracená
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Pragma	Pragma	k1gFnSc1	Pragma
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1997	[number]	k4	1997
Tůma	Tůma	k1gMnSc1	Tůma
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
:	:	kIx,	:
Mayská	mayský	k2eAgNnPc4d1	mayské
proroctví	proroctví	k1gNnPc4	proroctví
<g/>
,	,	kIx,	,
in	in	k?	in
<g/>
:	:	kIx,	:
Západočeský	západočeský	k2eAgInSc1d1	západočeský
archeoastronautický	archeoastronautický	k2eAgInSc1d1	archeoastronautický
zpravodaj	zpravodaj	k1gInSc1	zpravodaj
<g/>
,	,	kIx,	,
č.	č.	k?	č.
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
1997	[number]	k4	1997
Mayská	mayský	k2eAgFnSc1d1	mayská
civilizácia	civilizácia	k1gFnSc1	civilizácia
-	-	kIx~	-
ríša	ríš	k2eAgFnSc1d1	ríš
slnka	slnka	k1gFnSc1	slnka
<g/>
,	,	kIx,	,
in	in	k?	in
<g/>
:	:	kIx,	:
Národná	Národný	k2eAgFnSc1d1	Národná
Obroda	obroda	k1gFnSc1	obroda
13	[number]	k4	13
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
1999	[number]	k4	1999
STINGL	STINGL	kA	STINGL
<g/>
,	,	kIx,	,
Miloslav	Miloslav	k1gMnSc1	Miloslav
<g/>
:	:	kIx,	:
Tajomstvá	Tajomstvá	k1gFnSc1	Tajomstvá
indiánskych	indiánskych	k1gMnSc1	indiánskych
pyramíd	pyramíd	k1gMnSc1	pyramíd
Carrasco	Carrasco	k1gMnSc1	Carrasco
<g/>
,	,	kIx,	,
Davíd	Davíd	k1gMnSc1	Davíd
<g/>
:	:	kIx,	:
Náboženství	náboženství	k1gNnSc1	náboženství
Mezoameriky	Mezoamerika	k1gFnSc2	Mezoamerika
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Mayská	mayský	k2eAgNnPc4d1	mayské
civilizácia	civilizácium	k1gNnPc4	civilizácium
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Maya	Maya	k?	Maya
religion	religion	k1gInSc1	religion
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mayská	mayský	k2eAgFnSc1d1	mayská
civilizace	civilizace	k1gFnSc1	civilizace
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Mayský	mayský	k2eAgInSc4d1	mayský
svět	svět	k1gInSc4	svět
času	čas	k1gInSc2	čas
a	a	k8xC	a
čísel	číslo	k1gNnPc2	číslo
</s>
