<p>
<s>
Potápění	potápění	k1gNnSc1	potápění
je	být	k5eAaImIp3nS	být
činnost	činnost	k1gFnSc4	činnost
člověka	člověk	k1gMnSc2	člověk
pod	pod	k7c7	pod
vodní	vodní	k2eAgFnSc7d1	vodní
hladinou	hladina	k1gFnSc7	hladina
s	s	k7c7	s
dýchacím	dýchací	k2eAgInSc7d1	dýchací
přístrojem	přístroj	k1gInSc7	přístroj
nebo	nebo	k8xC	nebo
bez	bez	k7c2	bez
něj	on	k3xPp3gNnSc2	on
(	(	kIx(	(
<g/>
šnorchlování	šnorchlování	k1gNnSc2	šnorchlování
<g/>
,	,	kIx,	,
freediving	freediving	k1gInSc1	freediving
a	a	k8xC	a
scuba	scuba	k1gFnSc1	scuba
diving	diving	k1gInSc1	diving
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Potápění	potápěný	k2eAgMnPc1d1	potápěný
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
také	také	k9	také
na	na	k7c4	na
rekreační	rekreační	k2eAgNnSc4d1	rekreační
a	a	k8xC	a
profesionální	profesionální	k2eAgNnSc4d1	profesionální
(	(	kIx(	(
<g/>
komerční	komerční	k2eAgNnSc4d1	komerční
potápění	potápění	k1gNnSc4	potápění
a	a	k8xC	a
policejní	policejní	k2eAgMnPc1d1	policejní
potápěči	potápěč	k1gMnPc1	potápěč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Potápějící	potápějící	k2eAgFnSc1d1	potápějící
se	se	k3xPyFc4	se
osoba	osoba	k1gFnSc1	osoba
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
potápěč	potápěč	k1gInSc1	potápěč
(	(	kIx(	(
<g/>
hovorově	hovorově	k6eAd1	hovorově
i	i	k9	i
potapěč	potapěč	k1gMnSc1	potapěč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
potápění	potápění	k1gNnSc2	potápění
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgFnPc1d3	nejstarší
nespolehlivé	spolehlivý	k2eNgFnPc1d1	nespolehlivá
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
potápění	potápění	k1gNnSc6	potápění
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
Asyrské	asyrský	k2eAgFnSc2d1	Asyrská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Archeologické	archeologický	k2eAgInPc1d1	archeologický
nálezy	nález	k1gInPc1	nález
šperků	šperk	k1gInPc2	šperk
z	z	k7c2	z
perel	perla	k1gFnPc2	perla
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c4	o
jeho	jeho	k3xOp3gNnSc4	jeho
provozování	provozování	k1gNnSc4	provozování
zhruba	zhruba	k6eAd1	zhruba
2300	[number]	k4	2300
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Spolehlivější	spolehlivý	k2eAgMnPc1d2	spolehlivější
údaje	údaj	k1gInPc1	údaj
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
starověkého	starověký	k2eAgNnSc2d1	starověké
Řecka	Řecko	k1gNnSc2	Řecko
u	u	k7c2	u
Hérodota	Hérodot	k1gMnSc2	Hérodot
a	a	k8xC	a
také	také	k9	také
u	u	k7c2	u
Aristotela	Aristoteles	k1gMnSc2	Aristoteles
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
popsal	popsat	k5eAaPmAgMnS	popsat
i	i	k9	i
užití	užití	k1gNnSc4	užití
potápěčského	potápěčský	k2eAgInSc2d1	potápěčský
zvonu	zvon	k1gInSc2	zvon
<g/>
.	.	kIx.	.
</s>
<s>
Potápěči	potápěč	k1gMnPc1	potápěč
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
záznamů	záznam	k1gInPc2	záznam
zúčastňovali	zúčastňovat	k5eAaImAgMnP	zúčastňovat
mnoha	mnoho	k4c2	mnoho
starověkých	starověký	k2eAgFnPc2d1	starověká
bitev	bitva	k1gFnPc2	bitva
na	na	k7c6	na
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
==	==	k?	==
</s>
</p>
<p>
<s>
sportovní	sportovní	k2eAgNnPc1d1	sportovní
šnorchlování	šnorchlování	k1gNnPc1	šnorchlování
(	(	kIx(	(
<g/>
snorkeling	snorkeling	k1gInSc1	snorkeling
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
freediving	freediving	k1gInSc1	freediving
(	(	kIx(	(
<g/>
volné	volný	k2eAgNnSc1d1	volné
potápění	potápění	k1gNnSc1	potápění
na	na	k7c4	na
nádech	nádech	k1gInSc4	nádech
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
potápění	potápění	k1gNnSc1	potápění
s	s	k7c7	s
přístrojem	přístroj	k1gInSc7	přístroj
(	(	kIx(	(
<g/>
scuba	scuba	k1gFnSc1	scuba
diving	diving	k1gInSc1	diving
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
rekreační	rekreační	k2eAgFnSc1d1	rekreační
</s>
</p>
<p>
<s>
vrakové	vrakový	k2eAgNnSc1d1	vrakové
</s>
</p>
<p>
<s>
hloubkové	hloubkový	k2eAgInPc1d1	hloubkový
(	(	kIx(	(
<g/>
max	max	kA	max
<g/>
.	.	kIx.	.
40	[number]	k4	40
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
navigační	navigační	k2eAgFnSc1d1	navigační
</s>
</p>
<p>
<s>
vyhledavání	vyhledavánět	k5eAaImIp3nP	vyhledavánět
a	a	k8xC	a
vyzvedávaní	vyzvedávaný	k2eAgMnPc1d1	vyzvedávaný
předmětů	předmět	k1gInPc2	předmět
</s>
</p>
<p>
<s>
fotografovaní	fotografovaný	k2eAgMnPc1d1	fotografovaný
a	a	k8xC	a
filmování	filmování	k1gNnSc1	filmování
</s>
</p>
<p>
<s>
potápění	potápění	k1gNnSc1	potápění
se	s	k7c7	s
skútrem	skútr	k1gInSc7	skútr
(	(	kIx(	(
<g/>
DPV	DPV	kA	DPV
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
potápění	potápění	k1gNnSc1	potápění
s	s	k7c7	s
obohaceným	obohacený	k2eAgInSc7d1	obohacený
vzduchem	vzduch	k1gInSc7	vzduch
(	(	kIx(	(
<g/>
enrich	enrich	k1gInSc1	enrich
air	air	k?	air
Nitrox	Nitrox	k1gInSc1	Nitrox
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
technické	technický	k2eAgNnSc1d1	technické
</s>
</p>
<p>
<s>
hloubkové	hloubkový	k2eAgNnSc1d1	hloubkové
</s>
</p>
<p>
<s>
jeskynní	jeskynní	k2eAgFnSc1d1	jeskynní
</s>
</p>
<p>
<s>
+	+	kIx~	+
vše	všechen	k3xTgNnSc1	všechen
jako	jako	k9	jako
u	u	k7c2	u
rekreačního	rekreační	k2eAgNnSc2d1	rekreační
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
hloubkách	hloubka	k1gFnPc6	hloubka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
závodní	závodní	k2eAgNnSc1d1	závodní
potápění	potápění	k1gNnSc1	potápění
–	–	k?	–
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
discipliny	disciplina	k1gFnPc4	disciplina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
plavání	plavání	k1gNnSc1	plavání
s	s	k7c7	s
ploutvemi	ploutev	k1gFnPc7	ploutev
</s>
</p>
<p>
<s>
orientační	orientační	k2eAgNnSc1d1	orientační
potápění	potápění	k1gNnSc1	potápění
</s>
</p>
<p>
<s>
podvodní	podvodní	k2eAgNnSc1d1	podvodní
rugby	rugby	k1gNnSc1	rugby
</s>
</p>
<p>
<s>
podvodní	podvodní	k2eAgInSc4d1	podvodní
hokej	hokej	k1gInSc4	hokej
</s>
</p>
<p>
<s>
lov	lov	k1gInSc1	lov
na	na	k7c4	na
nádech	nádech	k1gInSc4	nádech
</s>
</p>
<p>
<s>
podvodní	podvodní	k2eAgFnSc1d1	podvodní
střelba	střelba	k1gFnSc1	střelba
na	na	k7c4	na
terč	terč	k1gInSc4	terč
</s>
</p>
<p>
<s>
apnoe	apnoe	k1gNnSc1	apnoe
(	(	kIx(	(
<g/>
potápění	potápění	k1gNnSc1	potápění
na	na	k7c4	na
nádech	nádech	k1gInSc4	nádech
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
podvodní	podvodní	k2eAgNnSc1d1	podvodní
fotografování	fotografování	k1gNnSc1	fotografování
a	a	k8xC	a
filmování	filmování	k1gNnSc1	filmování
</s>
</p>
<p>
<s>
==	==	k?	==
Výukové	výukový	k2eAgInPc1d1	výukový
systémy	systém	k1gInPc1	systém
==	==	k?	==
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
též	též	k9	též
několik	několik	k4yIc4	několik
výukových	výukový	k2eAgInPc2d1	výukový
systémů	systém	k1gInPc2	systém
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
ANDI	ANDI	kA	ANDI
–	–	k?	–
American	American	k1gInSc1	American
Nitrox	Nitrox	k1gInSc1	Nitrox
Divers	Divers	k1gInSc1	Divers
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ANDI	ANDI	kA	ANDI
<g/>
.	.	kIx.	.
<g/>
CZ	CZ	kA	CZ
<g/>
,	,	kIx,	,
ANDI-EUROPE	ANDI-EUROPE	k1gFnSc1	ANDI-EUROPE
<g/>
.	.	kIx.	.
<g/>
EU	EU	kA	EU
</s>
</p>
<p>
<s>
ČPŠ	ČPŠ	kA	ČPŠ
–	–	k?	–
Česká	český	k2eAgFnSc1d1	Česká
potápěčská	potápěčský	k2eAgFnSc1d1	potápěčská
škola	škola	k1gFnSc1	škola
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
CMAS	CMAS	kA	CMAS
–	–	k?	–
Confédération	Confédération	k1gInSc1	Confédération
Mondiale	Mondiala	k1gFnSc6	Mondiala
des	des	k1gNnSc1	des
Activités	Activitésa	k1gFnPc2	Activitésa
Subaquatiques	Subaquatiquesa	k1gFnPc2	Subaquatiquesa
CMAS	CMAS	kA	CMAS
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
UDI	UDI	kA	UDI
–	–	k?	–
United	United	k1gInSc1	United
Diving	Diving	k1gInSc1	Diving
Instructors	Instructors	k1gInSc4	Instructors
UDI	UDI	kA	UDI
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
SSI	SSI	kA	SSI
–	–	k?	–
Scuba	Scuba	k1gFnSc1	Scuba
Schools	Schools	k1gInSc1	Schools
International	International	k1gMnSc1	International
SSI	SSI	kA	SSI
<g/>
.	.	kIx.	.
<g/>
hr	hr	k6eAd1	hr
</s>
</p>
<p>
<s>
PADI	PADI	kA	PADI
–	–	k?	–
Professional	Professional	k1gFnSc1	Professional
Association	Association	k1gInSc1	Association
of	of	k?	of
Diving	Diving	k1gInSc1	Diving
Instructors	Instructors	k1gInSc1	Instructors
PADI	PADI	kA	PADI
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
IANTD	IANTD	kA	IANTD
–	–	k?	–
International	International	k1gFnSc1	International
Association	Association	k1gInSc1	Association
of	of	k?	of
Nitrox	Nitrox	k1gInSc1	Nitrox
and	and	k?	and
Technical	Technical	k1gFnSc2	Technical
Divers	Diversa	k1gFnPc2	Diversa
</s>
</p>
<p>
<s>
TDI	TDI	kA	TDI
<g/>
/	/	kIx~	/
<g/>
SDI	SDI	kA	SDI
–	–	k?	–
Technical	Technical	k1gFnSc1	Technical
Diving	Diving	k1gInSc1	Diving
International	International	k1gFnSc1	International
<g/>
/	/	kIx~	/
<g/>
Scuba	Scuba	k1gFnSc1	Scuba
Diving	Diving	k1gInSc1	Diving
International	International	k1gFnSc1	International
TDISDI	TDISDI	kA	TDISDI
</s>
</p>
<p>
<s>
NAUI	NAUI	kA	NAUI
–	–	k?	–
National	National	k1gFnSc1	National
Association	Association	k1gInSc1	Association
of	of	k?	of
Underwater	Underwater	k1gInSc1	Underwater
Instructors	Instructors	k1gInSc1	Instructors
NAUI	NAUI	kA	NAUI
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
GUE-	GUE-	k?	GUE-
Global	globat	k5eAaImAgInS	globat
Underwater	Underwater	k1gInSc1	Underwater
Explorers	Explorers	k1gInSc4	Explorers
GUE	GUE	kA	GUE
</s>
</p>
<p>
<s>
EDS	EDS	kA	EDS
–	–	k?	–
European	European	k1gInSc1	European
Diving	Diving	k1gInSc1	Diving
School	Schoola	k1gFnPc2	Schoola
EDSDalší	EDSDalší	k2eAgFnSc2d1	EDSDalší
výukové	výukový	k2eAgFnSc2d1	výuková
školy	škola	k1gFnSc2	škola
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
rozšířené	rozšířený	k2eAgFnSc6d1	rozšířená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Terminologie	terminologie	k1gFnSc2	terminologie
==	==	k?	==
</s>
</p>
<p>
<s>
ABC	ABC	kA	ABC
–	–	k?	–
ploutve	ploutev	k1gFnSc2	ploutev
+	+	kIx~	+
maska	maska	k1gFnSc1	maska
+	+	kIx~	+
šnorchl	šnorchl	k1gInSc1	šnorchl
</s>
</p>
<p>
<s>
Plicní	plicní	k2eAgFnSc1d1	plicní
automatika	automatika	k1gFnSc1	automatika
–	–	k?	–
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
primárního	primární	k2eAgInSc2d1	primární
a	a	k8xC	a
sekundárního	sekundární	k2eAgInSc2d1	sekundární
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Primární	primární	k2eAgNnSc1d1	primární
je	být	k5eAaImIp3nS	být
připojen	připojit	k5eAaPmNgInS	připojit
na	na	k7c4	na
láhev	láhev	k1gFnSc4	láhev
závitem	závit	k1gInSc7	závit
-DIN	-DIN	k?	-DIN
systém	systém	k1gInSc1	systém
nebo	nebo	k8xC	nebo
třmenem	třmen	k1gInSc7	třmen
-YOKE	-YOKE	k?	-YOKE
systém	systém	k1gInSc1	systém
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
výstupy	výstup	k1gInPc4	výstup
označené	označený	k2eAgInPc4d1	označený
jako	jako	k9	jako
vysokotlak	vysokotlak	k1gInSc4	vysokotlak
a	a	k8xC	a
středotlak	středotlak	k1gInSc4	středotlak
<g/>
.	.	kIx.	.
<g/>
Vysokotlak	Vysokotlak	k1gInSc1	Vysokotlak
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
jakási	jakýsi	k3yIgFnSc1	jakýsi
průchodka	průchodka	k1gFnSc1	průchodka
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
lahve	lahev	k1gFnSc2	lahev
a	a	k8xC	a
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
výstupu	výstup	k1gInSc2	výstup
se	se	k3xPyFc4	se
připojuje	připojovat	k5eAaImIp3nS	připojovat
manometr	manometr	k1gInSc1	manometr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Středotlak	Středotlak	k6eAd1	Středotlak
reguluje	regulovat	k5eAaImIp3nS	regulovat
tlak	tlak	k1gInSc1	tlak
z	z	k7c2	z
lahve	lahev	k1gFnSc2	lahev
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
vždy	vždy	k6eAd1	vždy
o	o	k7c6	o
10	[number]	k4	10
bar	bar	k1gInSc1	bar
větší	veliký	k2eAgInSc1d2	veliký
než	než	k8xS	než
okolní	okolní	k2eAgInSc1d1	okolní
tlak	tlak	k1gInSc1	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
středotlaku	středotlak	k1gInSc2	středotlak
patří	patřit	k5eAaImIp3nS	patřit
sekundární	sekundární	k2eAgInSc1d1	sekundární
stupeň	stupeň	k1gInSc1	stupeň
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
dýchá	dýchat	k5eAaImIp3nS	dýchat
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
na	na	k7c4	na
středotlak	středotlak	k1gInSc4	středotlak
připojuje	připojovat	k5eAaImIp3nS	připojovat
BCD	BCD	kA	BCD
či	či	k8xC	či
suchý	suchý	k2eAgInSc4d1	suchý
oblek	oblek	k1gInSc4	oblek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Primární	primární	k2eAgInSc1d1	primární
stupeň	stupeň	k1gInSc1	stupeň
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
podle	podle	k7c2	podle
techniky	technika	k1gFnSc2	technika
regulace	regulace	k1gFnSc2	regulace
průtoku	průtok	k1gInSc2	průtok
plynu	plyn	k1gInSc2	plyn
dvojího	dvojí	k4xRgInSc2	dvojí
typu	typ	k1gInSc2	typ
<g/>
:	:	kIx,	:
membránový	membránový	k2eAgInSc1d1	membránový
a	a	k8xC	a
pístový	pístový	k2eAgInSc1d1	pístový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sekundární	sekundární	k2eAgInSc1d1	sekundární
stupeň	stupeň	k1gInSc1	stupeň
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
"	"	kIx"	"
<g/>
ústenkový	ústenkový	k2eAgMnSc1d1	ústenkový
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
drží	držet	k5eAaImIp3nS	držet
pouze	pouze	k6eAd1	pouze
zuby	zub	k1gInPc4	zub
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
snadno	snadno	k6eAd1	snadno
vyjmout	vyjmout	k5eAaPmF	vyjmout
z	z	k7c2	z
úst	ústa	k1gNnPc2	ústa
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
při	při	k7c6	při
pomoci	pomoc	k1gFnSc6	pomoc
druhému	druhý	k4xOgInSc3	druhý
potápěči	potápěč	k1gInSc3	potápěč
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
potápěč	potápěč	k1gMnSc1	potápěč
nemá	mít	k5eNaImIp3nS	mít
záložní	záložní	k2eAgFnSc4d1	záložní
masku	maska	k1gFnSc4	maska
<g/>
)	)	kIx)	)
a	a	k8xC	a
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
integrovaný	integrovaný	k2eAgInSc1d1	integrovaný
<g/>
"	"	kIx"	"
do	do	k7c2	do
celoobličejové	celoobličejový	k2eAgFnSc2d1	celoobličejová
masky	maska	k1gFnSc2	maska
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
především	především	k9	především
pro	pro	k7c4	pro
práce	práce	k1gFnPc4	práce
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poměrně	poměrně	k6eAd1	poměrně
těžko	těžko	k6eAd1	těžko
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BCD	BCD	kA	BCD
(	(	kIx(	(
<g/>
Buoyancy	Buoyancy	k1gInPc1	Buoyancy
Control	Controla	k1gFnPc2	Controla
Device	device	k1gInSc2	device
<g/>
)	)	kIx)	)
–	–	k?	–
kompenzátor	kompenzátor	k1gInSc1	kompenzátor
vztlaku	vztlak	k1gInSc2	vztlak
plní	plnit	k5eAaImIp3nS	plnit
potápěč	potápěč	k1gInSc4	potápěč
vzduchem	vzduch	k1gInSc7	vzduch
z	z	k7c2	z
lahve	lahev	k1gFnSc2	lahev
dle	dle	k7c2	dle
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
;	;	kIx,	;
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
udržovat	udržovat	k5eAaImF	udržovat
potápěče	potápěč	k1gMnPc4	potápěč
ve	v	k7c6	v
vyvážené	vyvážený	k2eAgFnSc6d1	vyvážená
poloze	poloha	k1gFnSc6	poloha
(	(	kIx(	(
<g/>
potápěč	potápěč	k1gInSc1	potápěč
bez	bez	k7c2	bez
pohybu	pohyb	k1gInSc2	pohyb
neklesá	klesat	k5eNaImIp3nS	klesat
ani	ani	k8xC	ani
nestoupá	stoupat	k5eNaImIp3nS	stoupat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
BCD	BCD	kA	BCD
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
žaket	žaket	k1gInSc1	žaket
nebo	nebo	k8xC	nebo
křídlo	křídlo	k1gNnSc1	křídlo
<g/>
,	,	kIx,	,
za	za	k7c2	za
BCD	BCD	kA	BCD
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
i	i	k9	i
suchý	suchý	k2eAgInSc4d1	suchý
oblek	oblek	k1gInSc4	oblek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doing	Doing	k1gMnSc1	Doing
It	It	k1gMnSc1	It
Right	Right	k1gMnSc1	Right
(	(	kIx(	(
<g/>
DIR	DIR	kA	DIR
<g/>
)	)	kIx)	)
–	–	k?	–
je	být	k5eAaImIp3nS	být
potápěčský	potápěčský	k2eAgInSc1d1	potápěčský
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
si	se	k3xPyFc3	se
klade	klást	k5eAaImIp3nS	klást
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
maximalizovat	maximalizovat	k5eAaBmF	maximalizovat
zábavu	zábava	k1gFnSc4	zábava
při	při	k7c6	při
potápění	potápění	k1gNnSc6	potápění
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
minimalizovat	minimalizovat	k5eAaBmF	minimalizovat
námahu	námaha	k1gFnSc4	námaha
a	a	k8xC	a
riziko	riziko	k1gNnSc4	riziko
<g/>
.	.	kIx.	.
</s>
<s>
DIR	DIR	kA	DIR
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
praktikován	praktikovat	k5eAaImNgInS	praktikovat
potápěči	potápěč	k1gInPc7	potápěč
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
od	od	k7c2	od
mělkých	mělký	k2eAgInPc2d1	mělký
korálových	korálový	k2eAgInPc2d1	korálový
útesů	útes	k1gInPc2	útes
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
hluboké	hluboký	k2eAgInPc4d1	hluboký
vraky	vrak	k1gInPc4	vrak
v	v	k7c6	v
oceánech	oceán	k1gInPc6	oceán
až	až	k9	až
po	po	k7c4	po
extrémní	extrémní	k2eAgFnPc4d1	extrémní
penetrace	penetrace	k1gFnPc4	penetrace
v	v	k7c6	v
jeskynních	jeskynní	k2eAgInPc6d1	jeskynní
systémech	systém	k1gInPc6	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kyslík	kyslík	k1gInSc1	kyslík
–	–	k?	–
jako	jako	k8xS	jako
dýchací	dýchací	k2eAgFnSc1d1	dýchací
směs	směs	k1gFnSc1	směs
je	být	k5eAaImIp3nS	být
používána	používat	k5eAaImNgFnS	používat
jen	jen	k9	jen
při	při	k7c6	při
dekompresních	dekompresní	k2eAgFnPc6d1	dekompresní
zastávkách	zastávka	k1gFnPc6	zastávka
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
hloubkách	hloubka	k1gFnPc6	hloubka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
vyšším	vysoký	k2eAgInSc6d2	vyšší
parciálním	parciální	k2eAgInSc6d1	parciální
tlaku	tlak	k1gInSc6	tlak
toxický	toxický	k2eAgInSc1d1	toxický
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
dýchací	dýchací	k2eAgInSc1d1	dýchací
plyn	plyn	k1gInSc1	plyn
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
vzduch	vzduch	k1gInSc1	vzduch
zbavený	zbavený	k2eAgInSc1d1	zbavený
vlhkosti	vlhkost	k1gFnSc2	vlhkost
a	a	k8xC	a
oleje	olej	k1gInSc2	olej
<g/>
,	,	kIx,	,
Nitrox	Nitrox	k1gInSc1	Nitrox
nebo	nebo	k8xC	nebo
Trimix	Trimix	k1gInSc1	Trimix
</s>
</p>
<p>
<s>
Tlakové	tlakový	k2eAgFnPc1d1	tlaková
lahve	lahev	k1gFnPc1	lahev
–	–	k?	–
Ocelové	ocelový	k2eAgFnPc1d1	ocelová
<g/>
,	,	kIx,	,
duralové	duralový	k2eAgFnPc1d1	duralová
nebo	nebo	k8xC	nebo
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
kevlarové	kevlarový	k2eAgFnSc2d1	kevlarová
či	či	k8xC	či
uhlíkové	uhlíkový	k2eAgFnSc2d1	uhlíková
lahve	lahev	k1gFnSc2	lahev
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc1d1	obsahující
dýchací	dýchací	k2eAgFnSc1d1	dýchací
směs	směs	k1gFnSc1	směs
<g/>
.	.	kIx.	.
<g/>
Laická	laický	k2eAgFnSc1d1	laická
veřejnost	veřejnost	k1gFnSc1	veřejnost
většinou	většina	k1gFnSc7	většina
používá	používat	k5eAaImIp3nS	používat
termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
Kyslíkové	kyslíkový	k2eAgFnSc2d1	kyslíková
bomby	bomba	k1gFnSc2	bomba
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
velmi	velmi	k6eAd1	velmi
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
mylný	mylný	k2eAgInSc1d1	mylný
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
potapěči	potapěč	k1gMnPc1	potapěč
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
dýchají	dýchat	k5eAaImIp3nP	dýchat
kyslík	kyslík	k1gInSc4	kyslík
a	a	k8xC	a
tvar	tvar	k1gInSc4	tvar
lahví	lahev	k1gFnPc2	lahev
připomíná	připomínat	k5eAaImIp3nS	připomínat
tvar	tvar	k1gInSc1	tvar
leteckých	letecký	k2eAgFnPc2d1	letecká
pum	puma	k1gFnPc2	puma
<g/>
.	.	kIx.	.
</s>
<s>
Sportovní	sportovní	k2eAgFnPc1d1	sportovní
a	a	k8xC	a
profesionální	profesionální	k2eAgMnPc1d1	profesionální
potápěči	potápěč	k1gMnPc1	potápěč
naopak	naopak	k6eAd1	naopak
většinou	většinou	k6eAd1	většinou
používají	používat	k5eAaImIp3nP	používat
slangový	slangový	k2eAgInSc4d1	slangový
výraz	výraz	k1gInSc4	výraz
"	"	kIx"	"
<g/>
Flašky	flaška	k1gFnSc2	flaška
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Maska	maska	k1gFnSc1	maska
–	–	k?	–
kryje	krýt	k5eAaImIp3nS	krýt
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
nos	nos	k1gInSc4	nos
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
brýlí	brýle	k1gFnPc2	brýle
<g/>
)	)	kIx)	)
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
stisknout	stisknout	k5eAaPmF	stisknout
nos	nos	k1gInSc4	nos
pro	pro	k7c4	pro
provedení	provedení	k1gNnSc4	provedení
vyrovnání	vyrovnání	k1gNnSc1	vyrovnání
tlaku	tlak	k1gInSc2	tlak
ve	v	k7c6	v
středoušní	středoušní	k2eAgFnSc6d1	středoušní
dutině	dutina	k1gFnSc6	dutina
(	(	kIx(	(
<g/>
valsavův	valsavův	k2eAgInSc1d1	valsavův
manévr	manévr	k1gInSc1	manévr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
maska	maska	k1gFnSc1	maska
kryje	krýt	k5eAaImIp3nS	krýt
i	i	k9	i
ústa	ústa	k1gNnPc4	ústa
<g/>
,	,	kIx,	,
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
celoobličejová	celoobličejový	k2eAgFnSc1d1	celoobličejová
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
integrovaná	integrovaný	k2eAgFnSc1d1	integrovaná
plicní	plicní	k2eAgFnSc1d1	plicní
automatika	automatika	k1gFnSc1	automatika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
regulátor	regulátor	k1gInSc1	regulátor
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
Plicní	plicní	k2eAgNnSc1d1	plicní
automatika	automatika	k1gFnSc1	automatika
</s>
</p>
<p>
<s>
Octopus	Octopus	k1gInSc1	Octopus
–	–	k?	–
uspořádání	uspořádání	k1gNnSc3	uspořádání
prvého	prvý	k4xOgInSc2	prvý
stupně	stupeň	k1gInSc2	stupeň
plicní	plicní	k2eAgFnSc2d1	plicní
automatiky	automatika	k1gFnSc2	automatika
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
vychází	vycházet	k5eAaImIp3nS	vycházet
několik	několik	k4yIc4	několik
hadic	hadice	k1gFnPc2	hadice
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yIgMnPc4	který
je	být	k5eAaImIp3nS	být
napojen	napojen	k2eAgInSc1d1	napojen
manometr	manometr	k1gInSc1	manometr
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
stupeň	stupeň	k1gInSc1	stupeň
plicní	plicní	k2eAgFnSc2d1	plicní
automatiky	automatika	k1gFnSc2	automatika
<g/>
,	,	kIx,	,
záložní	záložní	k2eAgFnSc1d1	záložní
(	(	kIx(	(
<g/>
záchranná	záchranný	k2eAgFnSc1d1	záchranná
<g/>
)	)	kIx)	)
plicní	plicní	k2eAgFnSc1d1	plicní
automatika	automatika	k1gFnSc1	automatika
<g/>
,	,	kIx,	,
napojení	napojení	k1gNnSc1	napojení
kompenzátoru	kompenzátor	k1gInSc2	kompenzátor
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
i	i	k8xC	i
výstup	výstup	k1gInSc4	výstup
pro	pro	k7c4	pro
zařízení	zařízení	k1gNnSc4	zařízení
(	(	kIx(	(
<g/>
plnění	plnění	k1gNnSc4	plnění
výtažných	výtažný	k2eAgInPc2d1	výtažný
vaků	vak	k1gInPc2	vak
<g/>
,	,	kIx,	,
archeologické	archeologický	k2eAgInPc4d1	archeologický
vysavače	vysavač	k1gInPc4	vysavač
<g/>
,	,	kIx,	,
ruční	ruční	k2eAgNnSc4d1	ruční
nářadí	nářadí	k1gNnSc4	nářadí
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Finimetr	finimetr	k1gInSc1	finimetr
–	–	k?	–
manometr	manometr	k1gInSc4	manometr
zobrazující	zobrazující	k2eAgInSc4d1	zobrazující
tlak	tlak	k1gInSc4	tlak
v	v	k7c6	v
láhvi	láhev	k1gFnSc6	láhev
</s>
</p>
<p>
<s>
==	==	k?	==
Potápění	potápění	k1gNnPc1	potápění
žen	žena	k1gFnPc2	žena
==	==	k?	==
</s>
</p>
<p>
<s>
Účast	účast	k1gFnSc1	účast
žen	žena	k1gFnPc2	žena
v	v	k7c6	v
potápěčských	potápěčský	k2eAgFnPc6d1	potápěčská
aktivitách	aktivita	k1gFnPc6	aktivita
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
extenzivně	extenzivně	k6eAd1	extenzivně
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Početně	početně	k6eAd1	početně
největší	veliký	k2eAgFnSc4d3	veliký
skupinu	skupina	k1gFnSc4	skupina
potápějících	potápějící	k2eAgFnPc2d1	potápějící
se	se	k3xPyFc4	se
žen	žena	k1gFnPc2	žena
tvoří	tvořit	k5eAaImIp3nS	tvořit
amatérské	amatérský	k2eAgNnSc1d1	amatérské
<g/>
,	,	kIx,	,
rekreační	rekreační	k2eAgInPc1d1	rekreační
potápěčky	potápěček	k1gInPc1	potápěček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
měřítku	měřítko	k1gNnSc6	měřítko
jsou	být	k5eAaImIp3nP	být
držitelkami	držitelka	k1gFnPc7	držitelka
rekreačních	rekreační	k2eAgFnPc2d1	rekreační
potápěčských	potápěčský	k2eAgFnPc2d1	potápěčská
licencí	licence	k1gFnPc2	licence
různých	různý	k2eAgInPc2d1	různý
světových	světový	k2eAgInPc2d1	světový
výcvikových	výcvikový	k2eAgInPc2d1	výcvikový
systémů	systém	k1gInPc2	systém
tisíce	tisíc	k4xCgInSc2	tisíc
osob	osoba	k1gFnPc2	osoba
ženského	ženský	k2eAgNnSc2d1	ženské
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Výrobci	výrobce	k1gMnPc1	výrobce
potápěčské	potápěčský	k2eAgFnSc2d1	potápěčská
techniky	technika	k1gFnSc2	technika
uvádějí	uvádět	k5eAaImIp3nP	uvádět
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
nové	nový	k2eAgFnSc2d1	nová
kolekce	kolekce	k1gFnSc2	kolekce
výzbroje	výzbroj	k1gFnSc2	výzbroj
pro	pro	k7c4	pro
rekreační	rekreační	k2eAgNnSc4d1	rekreační
potápění	potápění	k1gNnSc4	potápění
určené	určený	k2eAgNnSc4d1	určené
ergonomicky	ergonomicky	k6eAd1	ergonomicky
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
i	i	k9	i
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
módního	módní	k2eAgInSc2d1	módní
designu	design	k1gInSc2	design
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
úspěšně	úspěšně	k6eAd1	úspěšně
působí	působit	k5eAaImIp3nP	působit
jako	jako	k9	jako
instruktorky	instruktorka	k1gFnPc1	instruktorka
potápění	potápění	k1gNnSc2	potápění
<g/>
,	,	kIx,	,
či	či	k8xC	či
potápěčské	potápěčský	k2eAgFnPc1d1	potápěčská
průvodkyně	průvodkyně	k1gFnPc1	průvodkyně
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
potápěčských	potápěčský	k2eAgFnPc6d1	potápěčská
lokalitách	lokalita	k1gFnPc6	lokalita
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
žen	žena	k1gFnPc2	žena
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
náročnému	náročný	k2eAgInSc3d1	náročný
technickému	technický	k2eAgInSc3d1	technický
<g/>
,	,	kIx,	,
hloubkovému	hloubkový	k2eAgInSc3d1	hloubkový
<g/>
,	,	kIx,	,
či	či	k8xC	či
objevnému	objevný	k2eAgNnSc3d1	objevné
potápění	potápění	k1gNnSc3	potápění
v	v	k7c6	v
lodních	lodní	k2eAgInPc6d1	lodní
vracích	vrak	k1gInPc6	vrak
či	či	k8xC	či
zatopených	zatopený	k2eAgInPc6d1	zatopený
jeskynních	jeskynní	k2eAgInPc6d1	jeskynní
systémech	systém	k1gInPc6	systém
<g/>
,	,	kIx,	,
světové	světový	k2eAgFnSc2d1	světová
šampiónky	šampiónka	k1gFnSc2	šampiónka
v	v	k7c6	v
potápění	potápění	k1gNnSc6	potápění
na	na	k7c4	na
nádech	nádech	k1gInSc4	nádech
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
extrémních	extrémní	k2eAgFnPc2d1	extrémní
hloubek	hloubka	k1gFnPc2	hloubka
<g/>
.	.	kIx.	.
</s>
<s>
Ženy-Potápěčky	Ženy-Potápěčky	k6eAd1	Ženy-Potápěčky
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
i	i	k9	i
v	v	k7c6	v
komerčním	komerční	k2eAgInSc6d1	komerční
(	(	kIx(	(
<g/>
pracovním	pracovní	k2eAgMnSc6d1	pracovní
<g/>
)	)	kIx)	)
potápění	potápění	k1gNnSc6	potápění
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
žena	žena	k1gFnSc1	žena
úspěšně	úspěšně	k6eAd1	úspěšně
absolvovala	absolvovat	k5eAaPmAgFnS	absolvovat
mimořádně	mimořádně	k6eAd1	mimořádně
náročný	náročný	k2eAgInSc4d1	náročný
výcvik	výcvik	k1gInSc4	výcvik
potápěče	potápěč	k1gInSc2	potápěč
amerického	americký	k2eAgNnSc2d1	americké
válečného	válečný	k2eAgNnSc2d1	válečné
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
(	(	kIx(	(
<g/>
US	US	kA	US
NAVY	NAVY	k?	NAVY
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
US	US	kA	US
Navy	Navy	k?	Navy
Diving	Diving	k1gInSc1	Diving
School	Schoola	k1gFnPc2	Schoola
vycvičených	vycvičený	k2eAgFnPc2d1	vycvičená
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
dalších	další	k2eAgFnPc2d1	další
následovnic	následovnice	k1gFnPc2	následovnice
<g/>
.	.	kIx.	.
</s>
<s>
Potápějící	potápějící	k2eAgFnPc1d1	potápějící
se	se	k3xPyFc4	se
ženy	žena	k1gFnPc1	žena
tak	tak	k9	tak
tvoří	tvořit	k5eAaImIp3nP	tvořit
velmi	velmi	k6eAd1	velmi
významnou	významný	k2eAgFnSc4d1	významná
skupinu	skupina	k1gFnSc4	skupina
hyperbaricky	hyperbaricky	k6eAd1	hyperbaricky
exponovaných	exponovaný	k2eAgFnPc2d1	exponovaná
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
svá	svůj	k3xOyFgNnPc4	svůj
inherentní	inherentní	k2eAgNnPc4d1	inherentní
somatická	somatický	k2eAgNnPc4d1	somatické
i	i	k8xC	i
funkční	funkční	k2eAgNnPc4d1	funkční
specifika	specifikon	k1gNnPc4	specifikon
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
zvláštní	zvláštní	k2eAgInPc4d1	zvláštní
medicínské	medicínský	k2eAgInPc4d1	medicínský
přístupy	přístup	k1gInPc4	přístup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Morfologické	morfologický	k2eAgFnPc4d1	morfologická
a	a	k8xC	a
funkční	funkční	k2eAgFnPc4d1	funkční
zvláštnosti	zvláštnost	k1gFnPc4	zvláštnost
ženského	ženský	k2eAgInSc2d1	ženský
organismu	organismus	k1gInSc2	organismus
===	===	k?	===
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
zvláštnosti	zvláštnost	k1gFnPc1	zvláštnost
ve	v	k7c6	v
stavbě	stavba	k1gFnSc6	stavba
a	a	k8xC	a
funkcích	funkce	k1gFnPc6	funkce
ženského	ženský	k2eAgInSc2d1	ženský
organismu	organismus	k1gInSc2	organismus
oproti	oproti	k7c3	oproti
mužům	muž	k1gMnPc3	muž
jsou	být	k5eAaImIp3nP	být
determinovány	determinován	k2eAgFnPc1d1	determinována
ontogenezí	ontogeneze	k1gFnSc7	ontogeneze
i	i	k8xC	i
individuálním	individuální	k2eAgInSc7d1	individuální
genetickým	genetický	k2eAgInSc7d1	genetický
vývojem	vývoj	k1gInSc7	vývoj
<g/>
,	,	kIx,	,
žádným	žádný	k3yNgInSc7	žádný
závažnějším	závažný	k2eAgInSc7d2	závažnější
způsobem	způsob	k1gInSc7	způsob
však	však	k9	však
osobu	osoba	k1gFnSc4	osoba
ženského	ženský	k2eAgNnSc2d1	ženské
pohlaví	pohlaví	k1gNnSc2	pohlaví
nediskriminují	diskriminovat	k5eNaBmIp3nP	diskriminovat
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
její	její	k3xOp3gFnSc3	její
eventuální	eventuální	k2eAgFnSc3d1	eventuální
potápěčské	potápěčský	k2eAgFnSc3d1	potápěčská
aktivitě	aktivita	k1gFnSc3	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Anatomická	anatomický	k2eAgFnSc1d1	anatomická
a	a	k8xC	a
funkční	funkční	k2eAgFnSc1d1	funkční
specifika	specifika	k1gFnSc1	specifika
ženského	ženský	k2eAgInSc2d1	ženský
organismu	organismus	k1gInSc2	organismus
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
jistým	jistý	k2eAgInSc7d1	jistý
způsobem	způsob	k1gInSc7	způsob
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
její	její	k3xOp3gFnSc4	její
potápěčskou	potápěčský	k2eAgFnSc4d1	potápěčská
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
potápěč	potápěč	k1gMnSc1	potápěč
<g/>
,	,	kIx,	,
ženu	žena	k1gFnSc4	žena
nevyjímaje	nevyjímaje	k7c4	nevyjímaje
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
před	před	k7c7	před
sestupem	sestup	k1gInSc7	sestup
pod	pod	k7c4	pod
hladinu	hladina	k1gFnSc4	hladina
vybavit	vybavit	k5eAaPmF	vybavit
komplexní	komplexní	k2eAgFnSc7d1	komplexní
potápěčskou	potápěčský	k2eAgFnSc7d1	potápěčská
výstrojí	výstroj	k1gFnSc7	výstroj
nezbytnou	zbytný	k2eNgFnSc7d1	zbytný
pro	pro	k7c4	pro
přežití	přežití	k1gNnSc4	přežití
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
(	(	kIx(	(
<g/>
life-support	lifeupport	k1gInSc1	life-support
system	syst	k1gInSc7	syst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Potápěčský	potápěčský	k2eAgInSc1d1	potápěčský
dýchací	dýchací	k2eAgInSc1d1	dýchací
přístroj	přístroj	k1gInSc1	přístroj
kromě	kromě	k7c2	kromě
nevýhody	nevýhoda	k1gFnSc2	nevýhoda
značné	značný	k2eAgFnSc2d1	značná
hmotnosti	hmotnost	k1gFnSc2	hmotnost
uživateli	uživatel	k1gMnPc7	uživatel
ve	v	k7c6	v
vzpřímené	vzpřímený	k2eAgFnSc6d1	vzpřímená
poloze	poloha	k1gFnSc6	poloha
zásadně	zásadně	k6eAd1	zásadně
změní	změnit	k5eAaPmIp3nS	změnit
těžiště	těžiště	k1gNnSc1	těžiště
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Skeletální	Skeletální	k2eAgInSc1d1	Skeletální
systém	systém	k1gInSc1	systém
ženy	žena	k1gFnSc2	žena
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
gracilností	gracilnost	k1gFnPc2	gracilnost
<g/>
,	,	kIx,	,
klouby	kloub	k1gInPc1	kloub
mají	mít	k5eAaImIp3nP	mít
menší	malý	k2eAgFnPc4d2	menší
kontaktní	kontaktní	k2eAgFnPc4d1	kontaktní
artikulární	artikulární	k2eAgFnPc4d1	artikulární
plošky	ploška	k1gFnPc4	ploška
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
anatomickému	anatomický	k2eAgNnSc3d1	anatomické
uspořádání	uspořádání	k1gNnSc3	uspořádání
ženské	ženský	k2eAgFnSc2d1	ženská
pánve	pánev	k1gFnSc2	pánev
a	a	k8xC	a
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
souvisejícím	související	k2eAgNnSc7d1	související
osovým	osový	k2eAgNnSc7d1	osové
postavením	postavení	k1gNnSc7	postavení
dolních	dolní	k2eAgFnPc2d1	dolní
končetin	končetina	k1gFnPc2	končetina
dochází	docházet	k5eAaImIp3nS	docházet
tak	tak	k6eAd1	tak
u	u	k7c2	u
ženy	žena	k1gFnSc2	žena
k	k	k7c3	k
přirozenému	přirozený	k2eAgNnSc3d1	přirozené
snížení	snížení	k1gNnSc3	snížení
polohy	poloha	k1gFnSc2	poloha
těžiště	těžiště	k1gNnSc2	těžiště
těla	tělo	k1gNnSc2	tělo
s	s	k7c7	s
mírným	mírný	k2eAgNnSc7d1	mírné
zhoršeným	zhoršený	k2eAgNnSc7d1	zhoršené
posturální	posturální	k2eAgMnPc1d1	posturální
stability	stabilita	k1gFnSc2	stabilita
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
projevit	projevit	k5eAaPmF	projevit
především	především	k9	především
při	při	k7c6	při
přenášení	přenášení	k1gNnSc6	přenášení
zátěže	zátěž	k1gFnSc2	zátěž
na	na	k7c4	na
zádech	zádech	k1gInSc4	zádech
(	(	kIx(	(
<g/>
dýchací	dýchací	k2eAgInSc4d1	dýchací
přístroj	přístroj	k1gInSc4	přístroj
<g/>
)	)	kIx)	)
v	v	k7c6	v
bipedálním	bipedální	k2eAgInSc6d1	bipedální
postoji	postoj	k1gInSc6	postoj
na	na	k7c6	na
nestabilní	stabilní	k2eNgFnSc6d1	nestabilní
podložce	podložka	k1gFnSc6	podložka
(	(	kIx(	(
<g/>
paluba	paluba	k1gFnSc1	paluba
lodě	loď	k1gFnSc2	loď
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potápění	potápění	k1gNnSc1	potápění
v	v	k7c6	v
ČR	ČR	kA	ČR
==	==	k?	==
</s>
</p>
<p>
<s>
Potápění	potápění	k1gNnSc1	potápění
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
i	i	k9	i
v	v	k7c6	v
ČR	ČR	kA	ČR
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
každoročně	každoročně	k6eAd1	každoročně
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
několik	několik	k4yIc1	několik
nových	nový	k2eAgNnPc2d1	nové
potápěčských	potápěčský	k2eAgNnPc2d1	potápěčské
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
aktuálně	aktuálně	k6eAd1	aktuálně
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
ČR	ČR	kA	ČR
přes	přes	k7c4	přes
200	[number]	k4	200
potápěčských	potápěčský	k2eAgNnPc2d1	potápěčské
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potápění	potápění	k1gNnPc4	potápění
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
==	==	k?	==
</s>
</p>
<p>
<s>
Nejznámější	známý	k2eAgFnPc1d3	nejznámější
potápěčské	potápěčský	k2eAgFnPc1d1	potápěčská
lokality	lokalita	k1gFnPc1	lokalita
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Egypt-Marsa	Egypt-Marsa	k1gFnSc1	Egypt-Marsa
Alam	Alam	k1gInSc1	Alam
<g/>
,	,	kIx,	,
Šarm	šarm	k1gInSc1	šarm
aš	aš	k?	aš
Šajch	šajch	k1gMnSc1	šajch
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc1	Austrálie
</s>
</p>
<p>
<s>
Velký	velký	k2eAgInSc1d1	velký
bariérový	bariérový	k2eAgInSc1d1	bariérový
útes	útes	k1gInSc1	útes
(	(	kIx(	(
<g/>
Great	Great	k2eAgMnSc1d1	Great
Barrier	Barrier	k1gMnSc1	Barrier
Reef	Reef	k1gMnSc1	Reef
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
,	,	kIx,	,
Maria	Maria	k1gFnSc1	Maria
la	la	k1gNnSc2	la
Gorda	Gordo	k1gNnSc2	Gordo
</s>
</p>
<p>
<s>
Maledivy	Maledivy	k1gFnPc1	Maledivy
</s>
</p>
<p>
<s>
Belize	Belize	k6eAd1	Belize
</s>
</p>
<p>
<s>
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
</s>
</p>
<p>
<s>
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
Conzumel	Conzumel	k1gInSc1	Conzumel
</s>
</p>
<p>
<s>
Turks	Turks	k1gInSc1	Turks
a	a	k8xC	a
Caicos	Caicos	k1gInSc1	Caicos
</s>
</p>
<p>
<s>
Maui	Maui	k1gNnSc1	Maui
<g/>
,	,	kIx,	,
Havaj	Havaj	k1gFnSc1	Havaj
</s>
</p>
<p>
<s>
Malajsie	Malajsie	k1gFnSc1	Malajsie
<g/>
,	,	kIx,	,
Borneo	Borneo	k1gNnSc1	Borneo
–	–	k?	–
Sipadan	Sipadan	k1gMnSc1	Sipadan
</s>
</p>
<p>
<s>
Karibik	Karibik	k1gMnSc1	Karibik
<g/>
,	,	kIx,	,
Saint	Saint	k1gMnSc1	Saint
Croix	Croix	k1gInSc1	Croix
</s>
</p>
<p>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
Durban	Durban	k1gInSc1	Durban
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
SCHINCK	SCHINCK	kA	SCHINCK
<g/>
,	,	kIx,	,
Andrea	Andrea	k1gFnSc1	Andrea
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
SCHINCK	SCHINCK	kA	SCHINCK
<g/>
.	.	kIx.	.
</s>
<s>
Potápění	potápění	k1gNnSc1	potápění
<g/>
:	:	kIx,	:
výstroj	výstroj	k1gInSc1	výstroj
<g/>
,	,	kIx,	,
rizika	riziko	k1gNnPc1	riziko
<g/>
,	,	kIx,	,
potápěčské	potápěčský	k2eAgInPc1d1	potápěčský
kurzy	kurz	k1gInPc1	kurz
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Čestlice	Čestlice	k1gFnSc1	Čestlice
<g/>
:	:	kIx,	:
Rebo	Rebo	k1gNnSc1	Rebo
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
223	[number]	k4	223
s.	s.	k?	s.
Teorie	teorie	k1gFnSc1	teorie
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7234	[number]	k4	7234
<g/>
-	-	kIx~	-
<g/>
704	[number]	k4	704
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g/>
LINDER	LINDER	kA	LINDER
<g/>
,	,	kIx,	,
Nikolay	Nikolaa	k1gFnSc2	Nikolaa
a	a	k8xC	a
Phil	Phil	k1gMnSc1	Phil
SIMHA	SIMHA	kA	SIMHA
<g/>
.	.	kIx.	.
</s>
<s>
Freediving	Freediving	k1gInSc1	Freediving
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
vydání	vydání	k1gNnSc1	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
IFP	IFP	kA	IFP
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
128	[number]	k4	128
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87383	[number]	k4	87383
<g/>
-	-	kIx~	-
<g/>
45	[number]	k4	45
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
ČERMÁK	Čermák	k1gMnSc1	Čermák
<g/>
,	,	kIx,	,
Marek	Marek	k1gMnSc1	Marek
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
potápění	potápění	k1gNnSc2	potápění
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgInPc4d1	dostupný
z	z	k7c2	z
<g/>
:	:	kIx,	:
https://is.muni.cz/th/73987/fsps_b_b1/Bakalarska_prace_-_Historie_potapeni.pdf.	[url]	k4	https://is.muni.cz/th/73987/fsps_b_b1/Bakalarska_prace_-_Historie_potapeni.pdf.
Bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
<g/>
.	.	kIx.	.
</s>
<s>
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucí	vedoucí	k1gMnSc1	vedoucí
práce	práce	k1gFnSc2	práce
PaedDr	PaedDr	kA	PaedDr
<g/>
.	.	kIx.	.
Vladimír	Vladimír	k1gMnSc1	Vladimír
Hansgut	Hansgut	k1gMnSc1	Hansgut
<g/>
,	,	kIx,	,
Ph	Ph	kA	Ph
<g/>
.	.	kIx.	.
<g/>
D.	D.	kA	D.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
názvy	název	k1gInPc1	název
==	==	k?	==
</s>
</p>
<p>
<s>
Potápka	potápka	k1gFnSc1	potápka
</s>
</p>
<p>
<s>
Potápníkovití	Potápníkovitý	k2eAgMnPc1d1	Potápníkovitý
</s>
</p>
<p>
<s>
Lovci	lovec	k1gMnPc1	lovec
perel	perla	k1gFnPc2	perla
</s>
</p>
<p>
<s>
Scuba	Scuba	k1gFnSc1	Scuba
</s>
</p>
<p>
<s>
Skafandr	skafandr	k1gInSc1	skafandr
</s>
</p>
<p>
<s>
Neoprén	Neoprén	k1gInSc1	Neoprén
</s>
</p>
<p>
<s>
Batyskaf	batyskaf	k1gInSc1	batyskaf
</s>
</p>
<p>
<s>
Utonutí	utonutí	k1gNnSc1	utonutí
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
potápění	potápění	k1gNnSc2	potápění
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Svaz	svaz	k1gInSc1	svaz
potápěčů	potápěč	k1gMnPc2	potápěč
ČR	ČR	kA	ČR
</s>
</p>
<p>
<s>
TV	TV	kA	TV
DIVE	div	k1gInSc5	div
–	–	k?	–
potápěčská	potápěčský	k2eAgFnSc1d1	potápěčská
televize	televize	k1gFnSc1	televize
</s>
</p>
<p>
<s>
iDive	iDivat	k5eAaPmIp3nS	iDivat
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
web	web	k1gInSc1	web
věnovaný	věnovaný	k2eAgInSc1d1	věnovaný
potápění	potápění	k1gNnSc4	potápění
<g/>
,	,	kIx,	,
články	článek	k1gInPc4	článek
<g/>
,	,	kIx,	,
fotogalerie	fotogalerie	k1gFnPc4	fotogalerie
<g/>
,	,	kIx,	,
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
chystaných	chystaný	k2eAgFnPc6d1	chystaná
akcích	akce	k1gFnPc6	akce
</s>
</p>
<p>
<s>
BUDDYmag	BUDDYmag	k1gInSc1	BUDDYmag
–	–	k?	–
dvouměsíčník	dvouměsíčník	k1gInSc1	dvouměsíčník
o	o	k7c4	o
potápění	potápění	k1gNnSc4	potápění
</s>
</p>
<p>
<s>
stranypotapecske	stranypotapecske	k1gFnSc1	stranypotapecske
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
výpis	výpis	k1gInSc1	výpis
všech	všecek	k3xTgFnPc2	všecek
potápěčských	potápěčský	k2eAgFnPc2d1	potápěčská
lokalit	lokalita	k1gFnPc2	lokalita
v	v	k7c6	v
ČR	ČR	kA	ČR
</s>
</p>
