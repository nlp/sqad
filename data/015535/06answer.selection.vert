<s>
Rakouské	rakouský	k2eAgNnSc1d1
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Vídeň	Vídeň	k1gFnSc1
se	se	k3xPyFc4
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1954	#num#	k4
administrativně	administrativně	k6eAd1
člení	členit	k5eAaImIp3nS
na	na	k7c4
23	#num#	k4
samosprávných	samosprávný	k2eAgFnPc2d1
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
tradičně	tradičně	k6eAd1
označovaných	označovaný	k2eAgFnPc2d1
jako	jako	k8xS,k8xC
vídeňské	vídeňský	k2eAgInPc1d1
městské	městský	k2eAgInPc1d1
okresy	okres	k1gInPc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Wiener	Wiener	k1gMnSc1
Gemeindebezirke	Gemeindebezirke	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>