<s>
Vídeňské	vídeňský	k2eAgInPc1d1
městské	městský	k2eAgInPc1d1
okresy	okres	k1gInPc1
</s>
<s>
Vídeňské	vídeňský	k2eAgInPc1d1
městské	městský	k2eAgInPc1d1
okresy	okres	k1gInPc1
</s>
<s>
Uliční	uliční	k2eAgInSc1d1
štít	štít	k1gInSc1
na	na	k7c6
vídeňském	vídeňský	k2eAgNnSc6d1
Schrödingerově	Schrödingerův	k2eAgNnSc6d1
náměstí	náměstí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Řadová	řadový	k2eAgFnSc1d1
číslovka	číslovka	k1gFnSc1
„	„	k?
<g/>
22	#num#	k4
<g/>
.	.	kIx.
<g/>
“	“	k?
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
nacházíme	nacházet	k5eAaImIp1nP
ve	v	k7c6
22	#num#	k4
<g/>
.	.	kIx.
okrese	okres	k1gInSc6
</s>
<s>
Rakouské	rakouský	k2eAgNnSc1d1
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Vídeň	Vídeň	k1gFnSc1
se	se	k3xPyFc4
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1954	#num#	k4
administrativně	administrativně	k6eAd1
člení	členit	k5eAaImIp3nS
na	na	k7c4
23	#num#	k4
samosprávných	samosprávný	k2eAgFnPc2d1
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
tradičně	tradičně	k6eAd1
označovaných	označovaný	k2eAgFnPc2d1
jako	jako	k8xS,k8xC
vídeňské	vídeňský	k2eAgInPc1d1
městské	městský	k2eAgInPc1d1
okresy	okres	k1gInPc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Wiener	Wiener	k1gMnSc1
Gemeindebezirke	Gemeindebezirke	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc4
z	z	k7c2
těchto	tento	k3xDgInPc2
okresů	okres	k1gInPc2
má	mít	k5eAaImIp3nS
nejen	nejen	k6eAd1
svoje	svůj	k3xOyFgNnSc4
číslo	číslo	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
název	název	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
odvozen	odvodit	k5eAaPmNgInS
od	od	k7c2
nejdůležitější	důležitý	k2eAgFnSc2d3
čtvrti	čtvrt	k1gFnSc2
v	v	k7c6
příslušném	příslušný	k2eAgInSc6d1
okrese	okres	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vídeňané	Vídeňan	k1gMnPc1
jednotlivé	jednotlivý	k2eAgInPc1d1
okresy	okres	k1gInPc1
označují	označovat	k5eAaImIp3nP
buď	buď	k8xC
jejich	jejich	k3xOp3gInPc1
názvy	název	k1gInPc1
nebo	nebo	k8xC
jejich	jejich	k3xOp3gNnPc7
čísly	číslo	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čísla	číslo	k1gNnPc1
okresů	okres	k1gInPc2
jsou	být	k5eAaImIp3nP
ve	v	k7c6
Vídni	Vídeň	k1gFnSc6
uvedena	uveden	k2eAgFnSc1d1
na	na	k7c6
každém	každý	k3xTgInSc6
uličním	uliční	k2eAgInSc6d1
štítě	štít	k1gInSc6
před	před	k7c7
názvem	název	k1gInSc7
ulice	ulice	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
„	„	k?
<g/>
17	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Pezzlgasse	Pezzlgasse	k1gFnSc1
<g/>
”	”	k?
<g/>
)	)	kIx)
a	a	k8xC
tvoří	tvořit	k5eAaImIp3nP
i	i	k9
2	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
3	#num#	k4
<g/>
.	.	kIx.
číslici	číslice	k1gFnSc6
ve	v	k7c6
vídeňských	vídeňský	k2eAgInPc2d1
PSČ	PSČ	kA
(	(	kIx(
<g/>
„	„	k?
<g/>
1010	#num#	k4
<g/>
”	”	k?
pro	pro	k7c4
1	#num#	k4
<g/>
.	.	kIx.
okres	okres	k1gInSc1
<g/>
,	,	kIx,
či	či	k8xC
„	„	k?
<g/>
1220	#num#	k4
<g/>
”	”	k?
pro	pro	k7c4
22	#num#	k4
<g/>
.	.	kIx.
okres	okres	k1gInSc1
<g/>
;	;	kIx,
ve	v	k7c6
23	#num#	k4
<g/>
.	.	kIx.
okrese	okres	k1gInSc6
platí	platit	k5eAaImIp3nP
jisté	jistý	k2eAgFnPc4d1
výjimky	výjimka	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
městských	městský	k2eAgInPc2d1
okresů	okres	k1gInPc2
</s>
<s>
Č.	Č.	kA
</s>
<s>
Městský	městský	k2eAgInSc1d1
okres	okres	k1gInSc1
</s>
<s>
Znak	znak	k1gInSc1
</s>
<s>
Katastrální	katastrální	k2eAgNnSc1d1
území	území	k1gNnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
v	v	k7c6
ha	ha	kA
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1
<g/>
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
01	#num#	k4
</s>
<s>
Vnitřní	vnitřní	k2eAgNnSc1d1
Město	město	k1gNnSc1
</s>
<s>
Vnitřní	vnitřní	k2eAgNnSc1d1
Město	město	k1gNnSc1
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
287.000	287.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
287	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
16268.00000016	16268.00000016	k4
268	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
5668.0000005	5668.0000005	k4
668	#num#	k4
</s>
<s>
02	#num#	k4
</s>
<s>
Leopoldstadt	Leopoldstadt	k1gMnSc1
</s>
<s>
Jägerzeile	Jägerzeile	k6eAd1
Leopoldstadt	Leopoldstadt	k2eAgInSc1d1
Zwischenbrücken	Zwischenbrücken	k1gInSc1
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1924.0000001	1924.0000001	k4
924	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
96866.00000096	96866.00000096	k4
866	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
5036.0000005	5036.0000005	k4
036	#num#	k4
</s>
<s>
03	#num#	k4
</s>
<s>
Landstraße	Landstraße	k6eAd1
</s>
<s>
Landstraße	Landstraße	k6eAd1
Erdberg	Erdberg	k1gMnSc1
Weißgerberviertel	Weißgerberviertel	k1gMnSc1
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
740.000	740.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
740	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
85508.00000085	85508.00000085	k4
508	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
11558.00000011	11558.00000011	k4
558	#num#	k4
</s>
<s>
04	#num#	k4
</s>
<s>
Wieden	Wiedna	k1gFnPc2
</s>
<s>
Hungelbrunn	Hungelbrunn	k1gNnSc1
Schaumburgergrund	Schaumburgergrunda	k1gFnPc2
Wieden	Wiedna	k1gFnPc2
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
178.000	178.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
178	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
30989.00000030	30989.00000030	k4
989	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
17459.00000017	17459.00000017	k4
459	#num#	k4
</s>
<s>
05	#num#	k4
</s>
<s>
Margareten	Margareten	k2eAgInSc1d1
</s>
<s>
Hundsturm	Hundsturm	k1gInSc1
Laurenzergrund	Laurenzergrunda	k1gFnPc2
Margareten	Margareten	k2eAgMnSc1d1
MatzleinsdorfNikolsdorf	MatzleinsdorfNikolsdorf	k1gMnSc1
Reinprechtsdorf	Reinprechtsdorf	k1gMnSc1
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
201.000	201.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
201	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
53071.00000053	53071.00000053	k4
071	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
26390.00000026	26390.00000026	k4
390	#num#	k4
</s>
<s>
06	#num#	k4
</s>
<s>
Mariahilf	Mariahilf	k1gMnSc1
</s>
<s>
Gumpendorf	Gumpendorf	k1gMnSc1
Laimgrube	Laimgrub	k1gInSc5
Magdalenengrund	Magdalenengrunda	k1gFnPc2
Mariahilf	Mariahilf	k1gMnSc1
Windmühle	Windmühle	k1gMnSc1
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
145.000	145.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
145	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
30117.00000030	30117.00000030	k4
117	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
20727.00000020	20727.00000020	k4
727	#num#	k4
</s>
<s>
07	#num#	k4
</s>
<s>
Neubau	Neubau	k6eAd1
</s>
<s>
Altlerchenfeld	Altlerchenfeld	k1gMnSc1
Neubau	Neubaus	k1gInSc2
Sankt	Sankt	k1gInSc1
Ulrich	Ulrich	k1gMnSc1
Schottenfeld	Schottenfeld	k1gMnSc1
Spittelberg	Spittelberg	k1gMnSc1
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
161.000	161.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
161	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
30309.00000030	30309.00000030	k4
309	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
18884.00000018	18884.00000018	k4
884	#num#	k4
</s>
<s>
08	#num#	k4
</s>
<s>
Josefstadt	Josefstadt	k1gMnSc1
</s>
<s>
Alservorstadt	Alservorstadt	k2eAgMnSc1d1
Altlerchenfeld	Altlerchenfeld	k1gMnSc1
Breitenfeld	Breitenfeld	k1gMnSc1
Josefstadt	Josefstadt	k1gMnSc1
Strozzigrund	Strozzigrund	k1gMnSc1
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
109.000	109.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
109	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
23930.00000023	23930.00000023	k4
930	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
21954.00000021	21954.00000021	k4
954	#num#	k4
</s>
<s>
09	#num#	k4
</s>
<s>
Alsergrund	Alsergrund	k1gMnSc1
</s>
<s>
Alservorstadt	Alservorstadt	k2eAgMnSc1d1
Althangrund	Althangrund	k1gMnSc1
Himmelpfortgrund	Himmelpfortgrund	k1gMnSc1
Lichtental	Lichtental	k1gMnSc1
Michelbeuern	Michelbeuern	k1gMnSc1
Rossau	Rossaus	k1gInSc2
Thurygrund	Thurygrund	k1gMnSc1
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
297.000	297.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
297	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
39968.00000039	39968.00000039	k4
968	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
13471.00000013	13471.00000013	k4
471	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
Favoriten	Favoriten	k2eAgInSc1d1
</s>
<s>
Favoriten	Favoriten	k2eAgMnSc1d1
Inzersdorf-Stadt	Inzersdorf-Stadt	k1gMnSc1
Oberlaa	Oberlaum	k1gNnSc2
Rothneusiedl	Rothneusiedl	k1gMnSc1
Unterlaa	Unterlaa	k1gMnSc1
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3182.0000003	3182.0000003	k4
182	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
182595.000000182	182595.000000182	k4
595	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
5738.0000005	5738.0000005	k4
738	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
Simmering	Simmering	k1gInSc1
</s>
<s>
Albern	Albern	k1gMnSc1
Kaiserebersdorf	Kaiserebersdorf	k1gMnSc1
Simmering	Simmering	k1gInSc4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2326.0000002	2326.0000002	k4
326	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
92274.00000092	92274.00000092	k4
274	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3968.0000003	3968.0000003	k4
968	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
Meidling	Meidling	k1gInSc1
</s>
<s>
Altmannsdorf	Altmannsdorf	k1gInSc1
Gaudenzdorf	Gaudenzdorf	k1gInSc1
Hetzendorf	Hetzendorf	k1gInSc1
Obermeidling	Obermeidling	k1gInSc1
Untermeidling	Untermeidling	k1gInSc4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
810.000	810.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
810	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
89616.00000089	89616.00000089	k4
616	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
11060.00000011	11060.00000011	k4
060	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
Hietzing	Hietzing	k1gInSc1
</s>
<s>
Hietzing	Hietzing	k1gInSc1
Unter-	Unter-	k1gFnSc2
<g/>
St.	st.	kA
<g/>
-Veit	-Veit	k1gMnSc1
Ober-	Ober-	k1gMnSc2
<g/>
St.	st.	kA
<g/>
-Veit	-Veit	k2eAgInSc1d1
Hacking	Hacking	k1gInSc1
Lainz	Lainza	k1gFnPc2
Speising	Speising	k1gInSc1
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3772.0000003	3772.0000003	k4
772	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
50831.00000050	50831.00000050	k4
831	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1348.0000001	1348.0000001	k4
348	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
Penzing	Penzing	k1gInSc1
</s>
<s>
Baumgarten	Baumgarten	k2eAgInSc4d1
Breitensee	Breitensee	k1gInSc4
Hadersdorf-Weidlingau	Hadersdorf-Weidlingaus	k1gInSc2
Hütteldorf	Hütteldorf	k1gInSc1
Penzing	Penzing	k1gInSc1
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3376.0000003	3376.0000003	k4
376	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
86248.00000086	86248.00000086	k4
248	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2555.0000002	2555.0000002	k4
555	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
Rudolfsheim-Fünfhaus	Rudolfsheim-Fünfhaus	k1gMnSc1
</s>
<s>
Rudolfsheim	Rudolfsheim	k1gMnSc1
Fünfhaus	Fünfhaus	k1gMnSc1
Sechshaus	Sechshaus	k1gMnSc1
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
392.000	392.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
392	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
73527.00000073	73527.00000073	k4
527	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
18738.00000018	18738.00000018	k4
738	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
Ottakring	Ottakring	k1gInSc1
</s>
<s>
Neulerchenfeld	Neulerchenfeld	k6eAd1
Ottakring	Ottakring	k1gInSc1
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
867.000	867.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
867	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
97565.00000097	97565.00000097	k4
565	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
11248.00000011	11248.00000011	k4
248	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
Hernals	Hernals	k6eAd1
</s>
<s>
Hernals	Hernals	k6eAd1
Dornbach	Dornbach	k1gMnSc1
Neuwaldegg	Neuwaldegg	k1gMnSc1
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1139.0000001	1139.0000001	k4
139	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
53489.00000053	53489.00000053	k4
489	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
4696.0000004	4696.0000004	k4
696	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
Währing	Währing	k1gInSc1
</s>
<s>
Gersthof	Gersthof	k1gInSc1
Pötzleinsdorf	Pötzleinsdorf	k1gInSc1
Währing	Währing	k1gInSc1
Weinhaus	Weinhaus	k1gInSc4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
635.000	635.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
635	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
48162.00000048	48162.00000048	k4
162	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
7588.0000007	7588.0000007	k4
588	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
Döbling	Döbling	k1gInSc1
</s>
<s>
Grinzing	Grinzing	k1gInSc1
Heiligenstadt	Heiligenstadt	k1gMnSc1
Josefsdorf	Josefsdorf	k1gMnSc1
Kahlenbergerdorf	Kahlenbergerdorf	k1gMnSc1
Neustift	Neustift	k1gMnSc1
am	am	k?
Walde	Wald	k1gInSc5
Nussdorf	Nussdorf	k1gInSc4
Oberdöbling	Oberdöbling	k1gInSc1
Salmannsdorf	Salmannsdorf	k1gInSc1
Sievering	Sievering	k1gInSc1
Unterdöbling	Unterdöbling	k1gInSc1
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2494.0000002	2494.0000002	k4
494	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
68892.00000068	68892.00000068	k4
892	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2762.0000002	2762.0000002	k4
762	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
Brigittenau	Brigittenau	k6eAd1
</s>
<s>
Brigittenau	Brigittenau	k6eAd1
Zwischenbrücken	Zwischenbrücken	k2eAgInSc1d1
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
571.000	571.000	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
571	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
83977.00000083	83977.00000083	k4
977	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
14707.00000014	14707.00000014	k4
707	#num#	k4
</s>
<s>
21	#num#	k4
</s>
<s>
Floridsdorf	Floridsdorf	k1gMnSc1
</s>
<s>
Donaufeld	Donaufeld	k1gMnSc1
Floridsdorf	Floridsdorf	k1gMnSc1
Großjedlersdorf	Großjedlersdorf	k1gMnSc1
Jedlesee	Jedlesee	k1gInSc4
Leopoldau	Leopoldaus	k1gInSc2
Stammersdorf	Stammersdorf	k1gMnSc1
Strebersdorf	Strebersdorf	k1gMnSc1
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
4444.0000004	4444.0000004	k4
444	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
146516.000000146	146516.000000146	k4
516	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3297.0000003	3297.0000003	k4
297	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
Donaustadt	Donaustadt	k1gMnSc1
</s>
<s>
Aspern	Aspern	k1gMnSc1
Breitenlee	Breitenle	k1gFnSc2
Essling	Essling	k1gInSc1
Hirschstetten	Hirschstetten	k2eAgInSc1d1
Kagran	Kagran	k1gInSc4
Kaisermühlen	Kaisermühlen	k2eAgInSc4d1
Stadlau	Stadla	k2eAgFnSc4d1
Süßenbrunn	Süßenbrunna	k1gFnPc2
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
10231.00000010	10231.00000010	k4
231	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
165265.000000165	165265.000000165	k4
265	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1615.0000001	1615.0000001	k4
615	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
Liesing	Liesing	k1gInSc1
</s>
<s>
Atzgersdorf	Atzgersdorf	k1gMnSc1
Erlaa	Erlaa	k1gMnSc1
Inzersdorf	Inzersdorf	k1gMnSc1
Kalksburg	Kalksburg	k1gMnSc1
Liesing	Liesing	k1gInSc4
Mauer	Mauer	k1gInSc1
Rodaun	Rodaun	k1gInSc1
Siebenhirten	Siebenhirten	k2eAgInSc1d1
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
3207.0000003	3207.0000003	k4
207	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
95263.00000095	95263.00000095	k4
263	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
2971.0000002	2971.0000002	k4
971	#num#	k4
</s>
<s>
Vídeň	Vídeň	k1gFnSc1
</s>
<s>
—	—	k?
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
41487.00000041	41487.00000041	k4
487	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
1741246.0000001	1741246.0000001	k4
741	#num#	k4
246	#num#	k4
</s>
<s>
&	&	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
4197.0000004	4197.0000004	k4
197	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Stadtgebiet	Stadtgebiet	k1gInSc1
nach	nach	k1gInSc1
Nutzungsklassen	Nutzungsklassen	k2eAgInSc1d1
und	und	k?
Gemeindebezirken	Gemeindebezirken	k2eAgInSc1d1
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Abgerufen	Abgerufen	k1gInSc1
am	am	k?
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Februar	Februar	k1gInSc1
2014	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bevölkerung	Bevölkerung	k1gInSc1
nach	nach	k1gInSc1
Bezirken	Bezirken	k2eAgInSc1d1
2005	#num#	k4
bis	bis	k?
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Abgerufen	Abgerufen	k1gInSc1
am	am	k?
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Februar	Februar	k1gInSc1
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
vídeňský	vídeňský	k2eAgInSc4d1
městský	městský	k2eAgInSc4d1
okres	okres	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Územní	územní	k2eAgInSc1d1
plán	plán	k1gInSc1
Vídně	Vídeň	k1gFnSc2
</s>
