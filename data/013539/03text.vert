<s>
Sham	Sham	k6eAd1
69	#num#	k4
</s>
<s>
Sham	Sham	k6eAd1
69	#num#	k4
Zpěvák	Zpěvák	k1gMnSc1
Jimmy	Jimm	k1gInPc1
PurseyZákladní	PurseyZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Původ	původ	k1gInSc1
</s>
<s>
Hersham	Hersham	k1gInSc1
<g/>
,	,	kIx,
Surrey	Surrey	k1gInPc1
<g/>
,	,	kIx,
Anglie	Anglie	k1gFnSc1
Žánry	žánr	k1gInPc1
</s>
<s>
punk	punk	k1gInSc1
rock	rock	k1gInSc1
Aktivní	aktivní	k2eAgInPc1d1
roky	rok	k1gInPc1
</s>
<s>
1976	#num#	k4
<g/>
–	–	k?
<g/>
19801987	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
Web	web	k1gInSc1
</s>
<s>
www.sham69.comwww.sham69.org	www.sham69.comwww.sham69.org	k1gInSc4
Současní	současný	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Jimmy	Jimma	k1gFnPc1
PurseyDave	PurseyDav	k1gInSc5
ParsonsDave	ParsonsDav	k1gInSc5
TregunnaMark	TregunnaMark	k1gInSc1
Cain	Cain	k1gInSc4
Dřívější	dřívější	k2eAgMnPc1d1
členové	člen	k1gMnPc1
</s>
<s>
Mat	mat	k2eAgInSc4d1
SargentAl	SargentAl	k1gInSc4
CampbellBilly	CampbellBilla	k1gFnSc2
BostickAlby	BostickAlba	k1gFnPc1
MaskellNeil	MaskellNeil	k1gInSc4
HarrisRicky	HarrisRicko	k1gNnPc7
GoldsteinAndy	GoldsteinAnda	k1gFnSc2
PrinceTim	PrinceTi	k1gNnSc7
VIan	VIana	k1gFnPc2
Whitewood	Whitewood	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sham	Sham	k1gInSc1
69	#num#	k4
je	být	k5eAaImIp3nS
britská	britský	k2eAgFnSc1d1
punk	punk	k1gInSc1
rocková	rockový	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
<g/>
,	,	kIx,
založená	založený	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Frontmanem	frontman	k1gInSc7
skupiny	skupina	k1gFnSc2
je	být	k5eAaImIp3nS
Jimmy	Jimmy	k1gMnPc1
Pursey	Pursey	k1gMnPc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
skupinu	skupina	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1976	#num#	k4
spoluzaložil	spoluzaložit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgInPc7d1
zakládajícími	zakládající	k2eAgInPc7d1
členy	člen	k1gInPc7
byli	být	k5eAaImAgMnP
Neil	Neil	k1gMnSc1
Harris	Harris	k1gFnSc2
<g/>
,	,	kIx,
Johnny	Johnna	k1gFnPc1
Goodfornothing	Goodfornothing	k1gInSc1
<g/>
,	,	kIx,
Albie	Albie	k1gFnSc1
Slider	Slidra	k1gFnPc2
s	s	k7c7
Billy	Bill	k1gMnPc7
Bostik	Bostik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
singl	singl	k1gInSc1
nazvaný	nazvaný	k2eAgInSc1d1
„	„	k?
<g/>
I	i	k9
Don	Don	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
Wanna	Wanen	k2eAgFnSc1d1
<g/>
“	“	k?
skupina	skupina	k1gFnSc1
vydala	vydat	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
a	a	k8xC
jeho	jeho	k3xOp3gMnSc7
producentem	producent	k1gMnSc7
byl	být	k5eAaImAgMnS
velšský	velšský	k2eAgMnSc1d1
hudebník	hudebník	k1gMnSc1
John	John	k1gMnSc1
Cale	Cale	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
album	album	k1gNnSc4
nazvané	nazvaný	k2eAgInPc1d1
Tell	Tell	k1gInSc1
Us	Us	k1gFnSc2
the	the	k?
Truth	Trutha	k1gFnPc2
následovalo	následovat	k5eAaImAgNnS
následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skupina	skupina	k1gFnSc1
se	se	k3xPyFc4
rozpadla	rozpadnout	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
jejímu	její	k3xOp3gNnSc3
obnovení	obnovení	k1gNnSc3
a	a	k8xC
po	po	k7c6
několika	několik	k4yIc6
výměnách	výměna	k1gFnPc6
členů	člen	k1gMnPc2
skupina	skupina	k1gFnSc1
existuje	existovat	k5eAaImIp3nS
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Sestavy	sestava	k1gFnPc1
a	a	k8xC
diskografie	diskografie	k1gFnPc1
</s>
<s>
Sestavy	sestava	k1gFnPc1
Sham	Shama	k1gFnPc2
69	#num#	k4
</s>
<s>
1976	#num#	k4
</s>
<s>
Jimmy	Jimm	k1gInPc1
Pursey	Pursea	k1gFnSc2
–	–	k?
zpěv	zpěv	k1gInSc1
</s>
<s>
Neil	Neil	k1gMnSc1
Harris	Harris	k1gFnSc2
–	–	k?
sólová	sólový	k2eAgFnSc1d1
kytara	kytara	k1gFnSc1
</s>
<s>
Johnny	Johnna	k1gFnPc1
Goodfornothing	Goodfornothing	k1gInSc1
–	–	k?
rytmická	rytmický	k2eAgFnSc1d1
kytara	kytara	k1gFnSc1
</s>
<s>
Albie	Albie	k1gFnSc1
Slider	Slider	k1gMnSc1
(	(	kIx(
<g/>
Albert	Albert	k1gMnSc1
Maskell	Maskell	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
baskytara	baskytara	k1gFnSc1
</s>
<s>
Billy	Bill	k1gMnPc4
Bostik	Bostika	k1gFnPc2
–	–	k?
bicí	bicí	k2eAgFnSc2d1
</s>
<s>
1977	#num#	k4
</s>
<s>
Jimmy	Jimm	k1gInPc1
Pursey	Pursea	k1gFnSc2
-	-	kIx~
zpěv	zpěv	k1gInSc1
</s>
<s>
Dave	Dav	k1gInSc5
Parsons	Parsons	k1gInSc1
–	–	k?
kytara	kytara	k1gFnSc1
</s>
<s>
Albie	Albie	k1gFnSc1
Slider	Slider	k1gInSc1
–	–	k?
baskytara	baskytara	k1gFnSc1
</s>
<s>
Mark	Mark	k1gMnSc1
Cain	Cain	k1gMnSc1
–	–	k?
bicí	bicí	k2eAgNnSc1d1
</s>
<s>
1977-1979	1977-1979	k4
</s>
<s>
Jimmy	Jimm	k1gInPc1
Pursey	Pursea	k1gFnSc2
-	-	kIx~
zpěv	zpěv	k1gInSc1
</s>
<s>
Dave	Dav	k1gInSc5
Parsons	Parsons	k1gInSc1
–	–	k?
kytara	kytara	k1gFnSc1
</s>
<s>
Dave	Dav	k1gInSc5
Tregunna	Tregunen	k2eAgFnSc1d1
–	–	k?
baskytara	baskytara	k1gFnSc1
</s>
<s>
Mark	Mark	k1gMnSc1
Cain	Cain	k1gMnSc1
–	–	k?
bicí	bicí	k2eAgNnSc1d1
</s>
<s>
1979-1980	1979-1980	k4
</s>
<s>
Jimmy	Jimm	k1gInPc1
Pursey	Pursea	k1gFnSc2
-	-	kIx~
zpěv	zpěv	k1gInSc1
</s>
<s>
Dave	Dav	k1gInSc5
Parsons	Parsons	k1gInSc1
–	–	k?
kytara	kytara	k1gFnSc1
</s>
<s>
Dave	Dav	k1gInSc5
Tregunna	Tregunen	k2eAgFnSc1d1
–	–	k?
baskytara	baskytara	k1gFnSc1
</s>
<s>
Ricky	Ricky	k6eAd1
Goldstein	Goldstein	k2eAgInSc1d1
–	–	k?
bicí	bicí	k2eAgInSc1d1
</s>
<s>
1987-1988	1987-1988	k4
</s>
<s>
Jimmy	Jimm	k1gInPc1
Pursey	Pursea	k1gFnSc2
-	-	kIx~
zpěv	zpěv	k1gInSc1
</s>
<s>
Dave	Dav	k1gInSc5
Parsons	Parsons	k1gInSc1
–	–	k?
kytara	kytara	k1gFnSc1
</s>
<s>
Ian	Ian	k?
Whitewood	Whitewood	k1gInSc1
–	–	k?
bicí	bicí	k2eAgInSc1d1
</s>
<s>
1988-1991	1988-1991	k4
</s>
<s>
Jimmy	Jimm	k1gInPc1
Pursey	Pursea	k1gFnSc2
-	-	kIx~
zpěv	zpěv	k1gInSc1
</s>
<s>
Dave	Dav	k1gInSc5
Parsons	Parsons	k1gInSc1
–	–	k?
kytara	kytara	k1gFnSc1
</s>
<s>
Andy	Anda	k1gFnSc2
Prince	princa	k1gFnSc3
–	–	k?
baskytara	baskytara	k1gFnSc1
</s>
<s>
Ian	Ian	k?
Whitewood	Whitewood	k1gInSc1
–	–	k?
bicí	bicí	k2eAgInSc1d1
</s>
<s>
1991-2001	1991-2001	k4
</s>
<s>
Jimmy	Jimm	k1gInPc1
Pursey	Pursea	k1gFnSc2
-	-	kIx~
zpěv	zpěv	k1gInSc1
</s>
<s>
Dave	Dav	k1gInSc5
Parsons	Parsons	k1gInSc1
–	–	k?
kytara	kytara	k1gFnSc1
</s>
<s>
Mat	mat	k2eAgInSc1d1
Sargent	Sargent	k1gInSc1
–	–	k?
baskytara	baskytara	k1gFnSc1
</s>
<s>
Ian	Ian	k?
Whitewood	Whitewood	k1gInSc1
–	–	k?
bicí	bicí	k2eAgInSc1d1
</s>
<s>
2001-2007	2001-2007	k4
</s>
<s>
Jimmy	Jimm	k1gInPc1
Pursey	Pursea	k1gFnSc2
-	-	kIx~
zpěv	zpěv	k1gInSc1
</s>
<s>
Dave	Dav	k1gInSc5
Parsons	Parsons	k1gInSc1
–	–	k?
kytara	kytara	k1gFnSc1
</s>
<s>
Mat	mat	k2eAgInSc1d1
Sargent	Sargent	k1gInSc1
–	–	k?
baskytara	baskytara	k1gFnSc1
</s>
<s>
Ian	Ian	k?
Whitewood	Whitewood	k1gInSc1
–	–	k?
bicí	bicí	k2eAgInSc1d1
</s>
<s>
2007-2009	2007-2009	k4
</s>
<s>
Tim	Tim	k?
V	V	kA
-	-	kIx~
zpěv	zpěv	k1gInSc1
</s>
<s>
Dave	Dav	k1gInSc5
Parsons	Parsons	k1gInSc1
–	–	k?
kytara	kytara	k1gFnSc1
</s>
<s>
Rob	roba	k1gFnPc2
„	„	k?
<g/>
Zee	Zee	k1gMnSc1
<g/>
“	“	k?
Jefferson	Jefferson	k1gMnSc1
–	–	k?
baskytara	baskytara	k1gFnSc1
</s>
<s>
Al	ala	k1gFnPc2
Campbell	Campbell	k1gMnSc1
–	–	k?
baskytara	baskytara	k1gFnSc1
</s>
<s>
Ian	Ian	k?
Whitewood	Whitewood	k1gInSc1
–	–	k?
bicí	bicí	k2eAgInSc1d1
</s>
<s>
2007-2011	2007-2011	k4
</s>
<s>
Tim	Tim	k?
V	V	kA
-	-	kIx~
zpěv	zpěv	k1gInSc1
</s>
<s>
Dave	Dav	k1gInSc5
Parsons	Parsons	k1gInSc1
–	–	k?
kytara	kytara	k1gFnSc1
</s>
<s>
Al	ala	k1gFnPc2
Campbell	Campbell	k1gMnSc1
–	–	k?
baskytara	baskytara	k1gFnSc1
</s>
<s>
Ian	Ian	k?
Whitewood	Whitewood	k1gInSc1
–	–	k?
bicí	bicí	k2eAgInSc1d1
</s>
<s>
2011	#num#	k4
<g/>
-dosud	-dosud	k1gInSc1
</s>
<s>
Tim	Tim	k?
V	V	kA
-	-	kIx~
zpěv	zpěv	k1gInSc1
</s>
<s>
Dave	Dav	k1gInSc5
Parsons	Parsons	k1gInSc1
–	–	k?
kytara	kytara	k1gFnSc1
</s>
<s>
Dave	Dav	k1gInSc5
Tregunna	Tregunen	k2eAgFnSc1d1
–	–	k?
baskytara	baskytara	k1gFnSc1
</s>
<s>
Mark	Mark	k1gMnSc1
Cain	Cain	k1gMnSc1
–	–	k?
bicí	bicí	k2eAgMnSc1d1
</s>
<s>
Studiová	studiový	k2eAgNnPc1d1
alba	album	k1gNnPc1
</s>
<s>
Tell	Tell	k1gMnSc1
Us	Us	k1gMnSc1
the	the	k?
Truth	Truth	k1gMnSc1
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
That	That	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Life	Life	k1gFnSc7
(	(	kIx(
<g/>
1978	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Adventures	Adventures	k1gInSc1
of	of	k?
the	the	k?
Hersham	Hersham	k1gInSc4
Boys	boy	k1gMnPc2
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Game	game	k1gInSc1
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Volunteer	Volunteer	k1gInSc1
(	(	kIx(
<g/>
1988	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Information	Information	k1gInSc1
Libre	Libr	k1gInSc5
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Soapy	Soap	k1gInPc1
Water	Watra	k1gFnPc2
and	and	k?
Mister	mister	k1gMnSc1
Marmalade	Marmalad	k1gInSc5
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
A	a	k9
Files	Files	k1gMnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Direct	Direct	k2eAgInSc1d1
Action	Action	k1gInSc1
<g/>
:	:	kIx,
Day	Day	k1gFnSc1
21	#num#	k4
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hollywood	Hollywood	k1gInSc1
Hero	Hero	k1gNnSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Western	Western	kA
Culture	Cultur	k1gMnSc5
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Who	Who	k?
Killed	Killed	k1gMnSc1
Joe	Joe	k1gFnSc2
Public	publicum	k1gNnPc2
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Their	Their	k1gMnSc1
Finest	Finest	k1gMnSc1
Hour	Hour	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
It	It	k?
<g/>
'	'	kIx"
<g/>
ll	ll	k?
End	End	k1gMnSc1
in	in	k?
Tears	Tears	k1gInSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Koncertní	koncertní	k2eAgNnPc1d1
alba	album	k1gNnPc1
</s>
<s>
Live	Live	k1gFnSc1
and	and	k?
Loud	louda	k1gFnPc2
<g/>
!!	!!	k?
(	(	kIx(
<g/>
1987	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Complete	Comple	k1gNnSc2
Sham	Shama	k1gFnPc2
69	#num#	k4
Live	Live	k1gFnPc2
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Live	Live	k1gFnSc1
at	at	k?
the	the	k?
Roxy	Roxy	k1gInPc1
Club	club	k1gInSc1
(	(	kIx(
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Live	Live	k1gFnSc1
in	in	k?
Italy	Ital	k1gMnPc7
(	(	kIx(
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Live	Live	k1gFnSc1
at	at	k?
CBGB	CBGB	kA
<g/>
'	'	kIx"
<g/>
s	s	k7c7
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kompilace	kompilace	k1gFnSc1
</s>
<s>
The	The	k?
First	First	k1gInSc1
<g/>
,	,	kIx,
the	the	k?
Best	Best	k1gMnSc1
and	and	k?
the	the	k?
Last	Last	k1gInSc1
(	(	kIx(
<g/>
1980	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kings	Kings	k1gInSc1
&	&	k?
Queens	Queens	k1gInSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lords	Lords	k6eAd1
of	of	k?
Oi	Oi	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Punk	punk	k1gInSc1
Singles	Singles	k1gMnSc1
Collection	Collection	k1gInSc1
<g/>
:	:	kIx,
1977-1980	1977-1980	k4
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Laced	Laced	k1gMnSc1
Up	Up	k1gFnSc2
Boots	Boots	k1gInSc1
and	and	k?
Corduroys	Corduroys	k1gInSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Teenage	Teenage	k1gFnSc1
Kicks	Kicksa	k1gFnPc2
(	(	kIx(
<g/>
4	#num#	k4
April	April	k1gInSc1
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Original	Original	k1gFnPc4
Punk	punk	k1gInSc4
Album	album	k1gNnSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Punk	punk	k1gInSc1
77	#num#	k4
<g/>
/	/	kIx~
<g/>
2007	#num#	k4
30	#num#	k4
<g/>
th	th	k?
Anniversary	Anniversara	k1gFnSc2
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
If	If	k?
The	The	k1gFnSc1
Kids	Kids	k1gInSc1
Are	ar	k1gInSc5
United	United	k1gMnSc1
-	-	kIx~
The	The	k1gMnSc1
Very	Vera	k1gFnSc2
Best	Best	k1gMnSc1
of	of	k?
Sham	Sham	k1gMnSc1
69	#num#	k4
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
The	The	k?
Complete	Comple	k1gNnSc2
Collection	Collection	k1gInSc1
<g/>
:	:	kIx,
3	#num#	k4
<g/>
-disc	-disc	k1gInSc1
<g/>
'	'	kIx"
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
STRONG	STRONG	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
C.	C.	kA
The	The	k1gMnSc1
Great	Great	k2eAgInSc1d1
Rock	rock	k1gInSc1
Discography	Discographa	k1gFnSc2
<g/>
.	.	kIx.
5	#num#	k4
<g/>
th	th	k?
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Edinburgh	Edinburgh	k1gInSc1
<g/>
:	:	kIx,
Mojo	Mojo	k1gNnSc1
Books	Booksa	k1gFnPc2
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
1	#num#	k4
<g/>
-	-	kIx~
<g/>
84195	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
869	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
book	booko	k1gNnPc2
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Sham	Sham	k6eAd1
69	#num#	k4
</s>
<s>
Jimmy	Jimm	k1gInPc1
Pursey	Pursea	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Sham	Sha	k1gNnSc7
69	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
osd	osd	k?
<g/>
2010549400	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
10275636-3	10275636-3	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2238	#num#	k4
0751	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
98048761	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
122576574	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
98048761	#num#	k4
</s>
