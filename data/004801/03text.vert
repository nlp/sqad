<s>
Anime	Animat	k5eAaPmIp3nS	Animat
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
ア	ア	k?	ア
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
a	a	k8xC	a
seriálů	seriál	k1gInPc2	seriál
produkovaných	produkovaný	k2eAgInPc2d1	produkovaný
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
<g/>
.	.	kIx.	.
</s>
<s>
Anime	Aniit	k5eAaImRp1nP	Aniit
je	být	k5eAaImIp3nS	být
zvykem	zvyk	k1gInSc7	zvyk
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
TV	TV	kA	TV
seriály	seriál	k1gInPc1	seriál
<g/>
,	,	kIx,	,
OVA	OVA	kA	OVA
(	(	kIx(	(
<g/>
Original	Original	k1gMnSc6	Original
Video	video	k1gNnSc1	video
Animation	Animation	k1gInSc1	Animation
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ONA	onen	k3xDgFnSc1	onen
(	(	kIx(	(
<g/>
Original	Original	k1gFnSc1	Original
Net	Net	k1gMnSc1	Net
Animation	Animation	k1gInSc1	Animation
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
speciály	speciál	k1gInPc1	speciál
<g/>
,	,	kIx,	,
filmy	film	k1gInPc1	film
a	a	k8xC	a
hudební	hudební	k2eAgNnPc1d1	hudební
videa	video	k1gNnPc1	video
<g/>
.	.	kIx.	.
</s>
<s>
Anime	Animat	k5eAaPmIp3nS	Animat
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
také	také	k9	také
zdaleka	zdaleka	k6eAd1	zdaleka
nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
<g/>
,	,	kIx,	,
a	a	k8xC	a
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
stylem	styl	k1gInSc7	styl
kresby	kresba	k1gFnSc2	kresba
postav	postav	k1gInSc1	postav
a	a	k8xC	a
pozadí	pozadí	k1gNnSc1	pozadí
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
anime	animat	k5eAaPmIp3nS	animat
se	se	k3xPyFc4	se
však	však	k9	však
počítá	počítat	k5eAaImIp3nS	počítat
i	i	k9	i
loutková	loutkový	k2eAgFnSc1d1	loutková
nebo	nebo	k8xC	nebo
3D	[number]	k4	3D
animace	animace	k1gFnPc4	animace
<g/>
.	.	kIx.	.
</s>
<s>
Anime	Animat	k5eAaPmIp3nS	Animat
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
širokou	široký	k2eAgFnSc4d1	široká
škálu	škála	k1gFnSc4	škála
žánrů	žánr	k1gInPc2	žánr
<g/>
,	,	kIx,	,
od	od	k7c2	od
akčních	akční	k2eAgInPc2d1	akční
seriálů	seriál	k1gInPc2	seriál
přes	přes	k7c4	přes
detektivky	detektivka	k1gFnPc4	detektivka
až	až	k9	až
po	po	k7c4	po
psychologická	psychologický	k2eAgNnPc4d1	psychologické
dramata	drama	k1gNnPc4	drama
<g/>
.	.	kIx.	.
</s>
<s>
Anime	Animat	k5eAaPmIp3nS	Animat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
vysílá	vysílat	k5eAaImIp3nS	vysílat
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
<g/>
,	,	kIx,	,
distribuuje	distribuovat	k5eAaBmIp3nS	distribuovat
na	na	k7c6	na
DVD	DVD	kA	DVD
nebo	nebo	k8xC	nebo
publikuje	publikovat	k5eAaBmIp3nS	publikovat
jako	jako	k9	jako
videohry	videohra	k1gFnPc4	videohra
<g/>
.	.	kIx.	.
</s>
<s>
Anime	Animat	k5eAaPmIp3nS	Animat
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
nebo	nebo	k8xC	nebo
přímo	přímo	k6eAd1	přímo
inspirováno	inspirovat	k5eAaBmNgNnS	inspirovat
japonskými	japonský	k2eAgInPc7d1	japonský
komiksy	komiks	k1gInPc7	komiks
manga	mango	k1gNnSc2	mango
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
občas	občas	k6eAd1	občas
naopak	naopak	k6eAd1	naopak
takové	takový	k3xDgInPc4	takový
komiksy	komiks	k1gInPc4	komiks
inspiruje	inspirovat	k5eAaBmIp3nS	inspirovat
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
bývá	bývat	k5eAaImIp3nS	bývat
anime	animat	k5eAaPmIp3nS	animat
také	také	k9	také
adaptováno	adaptovat	k5eAaBmNgNnS	adaptovat
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
hraných	hraný	k2eAgInPc2d1	hraný
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Anime	Animat	k5eAaPmIp3nS	Animat
produkuje	produkovat	k5eAaImIp3nS	produkovat
řada	řada	k1gFnSc1	řada
japonských	japonský	k2eAgFnPc2d1	japonská
animátorských	animátorský	k2eAgFnPc2d1	animátorská
studií	studie	k1gFnPc2	studie
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgNnSc4d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
studio	studio	k1gNnSc4	studio
Ghibli	Ghibli	k1gFnSc2	Ghibli
nebo	nebo	k8xC	nebo
Gainax	Gainax	k1gInSc1	Gainax
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
představitele	představitel	k1gMnPc4	představitel
anime	animat	k5eAaPmIp3nS	animat
určeného	určený	k2eAgInSc2d1	určený
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
seriál	seriál	k1gInSc1	seriál
Pokémon	Pokémona	k1gFnPc2	Pokémona
nebo	nebo	k8xC	nebo
film	film	k1gInSc1	film
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
fantazie	fantazie	k1gFnSc2	fantazie
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
západní	západní	k2eAgFnSc2d1	západní
kultury	kultura	k1gFnSc2	kultura
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
jevit	jevit	k5eAaImF	jevit
jako	jako	k9	jako
překvapivé	překvapivý	k2eAgNnSc1d1	překvapivé
<g/>
,	,	kIx,	,
že	že	k8xS	že
řada	řada	k1gFnSc1	řada
japonských	japonský	k2eAgInPc2d1	japonský
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
je	být	k5eAaImIp3nS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
spíše	spíše	k9	spíše
na	na	k7c4	na
dospělé	dospělý	k2eAgMnPc4d1	dospělý
diváky	divák	k1gMnPc4	divák
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
fanoušci	fanoušek	k1gMnPc1	fanoušek
tohoto	tento	k3xDgInSc2	tento
žánru	žánr	k1gInSc2	žánr
pokládají	pokládat	k5eAaImIp3nP	pokládat
za	za	k7c4	za
reprezentativní	reprezentativní	k2eAgInPc4d1	reprezentativní
např.	např.	kA	např.
tituly	titul	k1gInPc4	titul
jako	jako	k8xC	jako
Hrob	hrob	k1gInSc1	hrob
světlušek	světluška	k1gFnPc2	světluška
<g/>
,	,	kIx,	,
Ghost	Ghost	k1gFnSc1	Ghost
in	in	k?	in
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
<g/>
,	,	kIx,	,
Neon	neon	k1gInSc1	neon
Genesis	Genesis	k1gFnSc1	Genesis
Evangelion	Evangelion	k1gInSc1	Evangelion
<g/>
,	,	kIx,	,
Princezna	princezna	k1gFnSc1	princezna
Mononoke	Mononoke	k1gFnSc1	Mononoke
<g/>
,	,	kIx,	,
Vampire	Vampir	k1gInSc5	Vampir
Hunter	Huntra	k1gFnPc2	Huntra
D	D	kA	D
nebo	nebo	k8xC	nebo
Akira	Akiro	k1gNnSc2	Akiro
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
anime	animat	k5eAaPmIp3nS	animat
začala	začít	k5eAaPmAgFnS	začít
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
japonští	japonský	k2eAgMnPc1d1	japonský
filmoví	filmový	k2eAgMnPc1d1	filmový
tvůrci	tvůrce	k1gMnPc1	tvůrce
experimentovali	experimentovat	k5eAaImAgMnP	experimentovat
s	s	k7c7	s
animačními	animační	k2eAgFnPc7d1	animační
technikami	technika	k1gFnPc7	technika
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
vyvíjely	vyvíjet	k5eAaImAgFnP	vyvíjet
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
a	a	k8xC	a
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Animace	animace	k1gFnSc1	animace
se	se	k3xPyFc4	se
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
stala	stát	k5eAaPmAgFnS	stát
populární	populární	k2eAgFnSc1d1	populární
jako	jako	k8xS	jako
alternativní	alternativní	k2eAgFnSc1d1	alternativní
forma	forma	k1gFnSc1	forma
dějového	dějový	k2eAgNnSc2d1	dějové
vyprávění	vyprávění	k1gNnSc2	vyprávění
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
málo	málo	k6eAd1	málo
rozvinutým	rozvinutý	k2eAgInSc7d1	rozvinutý
japonským	japonský	k2eAgInSc7d1	japonský
hraným	hraný	k2eAgInSc7d1	hraný
filmem	film	k1gInSc7	film
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
například	například	k6eAd1	například
Americe	Amerika	k1gFnSc3	Amerika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
hrané	hraný	k2eAgInPc4d1	hraný
pořady	pořad	k1gInPc1	pořad
a	a	k8xC	a
filmy	film	k1gInPc1	film
štědře	štědro	k6eAd1	štědro
dotované	dotovaný	k2eAgInPc1d1	dotovaný
<g/>
,	,	kIx,	,
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
je	být	k5eAaImIp3nS	být
hraný	hraný	k2eAgInSc1d1	hraný
filmový	filmový	k2eAgInSc1d1	filmový
průmysl	průmysl	k1gInSc1	průmysl
malý	malý	k2eAgInSc1d1	malý
a	a	k8xC	a
tak	tak	k9	tak
trpí	trpět	k5eAaImIp3nP	trpět
nízkými	nízký	k2eAgFnPc7d1	nízká
státními	státní	k2eAgFnPc7d1	státní
dotacemi	dotace	k1gFnPc7	dotace
<g/>
,	,	kIx,	,
umístěním	umístění	k1gNnSc7	umístění
i	i	k8xC	i
omezením	omezení	k1gNnSc7	omezení
počtu	počet	k1gInSc2	počet
herců	herc	k1gInPc2	herc
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
také	také	k9	také
obtížně	obtížně	k6eAd1	obtížně
uchycuje	uchycovat	k5eAaImIp3nS	uchycovat
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
nebo	nebo	k8xC	nebo
americkém	americký	k2eAgInSc6d1	americký
trhu	trh	k1gInSc6	trh
díky	díky	k7c3	díky
nedostatku	nedostatek	k1gInSc3	nedostatek
účinkujících	účinkující	k1gMnPc2	účinkující
západního	západní	k2eAgNnSc2d1	západní
vzezření	vzezření	k1gNnSc2	vzezření
<g/>
.	.	kIx.	.
</s>
<s>
Zato	zato	k6eAd1	zato
různorodé	různorodý	k2eAgNnSc1d1	různorodé
použití	použití	k1gNnSc1	použití
animace	animace	k1gFnSc2	animace
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
tvůrcům	tvůrce	k1gMnPc3	tvůrce
vytvářet	vytvářet	k5eAaImF	vytvářet
postavy	postava	k1gFnPc4	postava
a	a	k8xC	a
prostředí	prostředí	k1gNnPc4	prostředí
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
nevypadají	vypadat	k5eNaPmIp3nP	vypadat
japonsky	japonsky	k6eAd1	japonsky
a	a	k8xC	a
tak	tak	k6eAd1	tak
jsou	být	k5eAaImIp3nP	být
západnímu	západní	k2eAgInSc3d1	západní
světu	svět	k1gInSc3	svět
blíže	blíž	k1gFnSc2	blíž
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
popularity	popularita	k1gFnSc2	popularita
manga	mango	k1gNnSc2	mango
komiksů	komiks	k1gInPc2	komiks
–	–	k?	–
které	který	k3yRgInPc4	který
často	často	k6eAd1	často
později	pozdě	k6eAd2	pozdě
získávaly	získávat	k5eAaImAgFnP	získávat
též	též	k9	též
anime	animat	k5eAaPmIp3nS	animat
podobu	podoba	k1gFnSc4	podoba
–	–	k?	–
obzvláště	obzvláště	k6eAd1	obzvláště
pak	pak	k6eAd1	pak
mang	mango	k1gNnPc2	mango
Osamy	Osama	k1gFnSc2	Osama
Tezuka	Tezuk	k1gMnSc2	Tezuk
<g/>
,	,	kIx,	,
nazývaným	nazývaný	k2eAgInSc7d1	nazývaný
"	"	kIx"	"
<g/>
legenda	legenda	k1gFnSc1	legenda
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
bůh	bůh	k1gMnSc1	bůh
mangy	mango	k1gNnPc7	mango
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
výsledek	výsledek	k1gInSc1	výsledek
práce	práce	k1gFnSc2	práce
Tezuky	Tezuk	k1gMnPc7	Tezuk
a	a	k8xC	a
dalších	další	k2eAgNnPc6d1	další
průkomníku	průkomník	k1gMnSc6	průkomník
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
přineslo	přinést	k5eAaPmAgNnS	přinést
anime	animat	k5eAaPmIp3nS	animat
do	do	k7c2	do
současného	současný	k2eAgNnSc2d1	současné
umění	umění	k1gNnSc2	umění
plnohodnotné	plnohodnotný	k2eAgInPc4d1	plnohodnotný
žánry	žánr	k1gInPc4	žánr
a	a	k8xC	a
některé	některý	k3yIgInPc4	některý
typické	typický	k2eAgInPc4d1	typický
rysy	rys	k1gInPc4	rys
<g/>
.	.	kIx.	.
</s>
<s>
Kupříkladu	kupříkladu	k6eAd1	kupříkladu
žánr	žánr	k1gInSc4	žánr
o	o	k7c6	o
gigantických	gigantický	k2eAgInPc6d1	gigantický
robotech	robot	k1gInPc6	robot
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
Japonsko	Japonsko	k1gNnSc4	Japonsko
známý	známý	k1gMnSc1	známý
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
mecha	mecha	k1gFnSc1	mecha
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
utvořil	utvořit	k5eAaPmAgMnS	utvořit
pod	pod	k7c7	pod
Tezukou	Tezuka	k1gFnSc7	Tezuka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
vyvíjen	vyvíjet	k5eAaImNgInS	vyvíjet
pod	pod	k7c7	pod
Go	Go	k1gMnPc7	Go
Nagaim	Nagaim	k1gMnSc1	Nagaim
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jinými	jiný	k1gMnPc7	jiný
a	a	k8xC	a
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
desetiletí	desetiletí	k1gNnSc2	desetiletí
zcela	zcela	k6eAd1	zcela
změněn	změněn	k2eAgInSc1d1	změněn
Jošijukim	Jošijukim	k1gInSc1	Jošijukim
Tominem	Tomin	k1gInSc7	Tomin
<g/>
.	.	kIx.	.
</s>
<s>
Mecha	Mecha	k1gFnSc1	Mecha
anime	animat	k5eAaPmIp3nS	animat
Gundam	Gundam	k1gInSc1	Gundam
nebo	nebo	k8xC	nebo
Macross	Macross	k1gInSc1	Macross
se	se	k3xPyFc4	se
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
rychle	rychle	k6eAd1	rychle
stalo	stát	k5eAaPmAgNnS	stát
klasikami	klasika	k1gFnPc7	klasika
žánru	žánr	k1gInSc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
samém	samý	k3xTgInSc6	samý
desetiletí	desetiletí	k1gNnSc6	desetiletí
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
anime	animat	k5eAaPmIp3nS	animat
přijato	přijmout	k5eAaPmNgNnS	přijmout
hlavním	hlavní	k2eAgInSc7d1	hlavní
filmovým	filmový	k2eAgInSc7d1	filmový
proudem	proud	k1gInSc7	proud
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
tak	tak	k6eAd1	tak
zažilo	zažít	k5eAaPmAgNnS	zažít
boom	boom	k1gInSc4	boom
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
titulů	titul	k1gInPc2	titul
(	(	kIx(	(
<g/>
mimoto	mimoto	k6eAd1	mimoto
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
se	se	k3xPyFc4	se
manga	mango	k1gNnSc2	mango
těší	těšit	k5eAaImIp3nS	těšit
větší	veliký	k2eAgFnSc1d2	veliký
publicitě	publicita	k1gFnSc3	publicita
než	než	k8xS	než
anime	animat	k5eAaPmIp3nS	animat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
se	se	k3xPyFc4	se
anime	animat	k5eAaPmIp3nS	animat
zvýšeně	zvýšeně	k6eAd1	zvýšeně
rozšiřovalo	rozšiřovat	k5eAaImAgNnS	rozšiřovat
na	na	k7c6	na
zámořských	zámořský	k2eAgInPc6d1	zámořský
trzích	trh	k1gInPc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Japonský	japonský	k2eAgInSc1d1	japonský
výraz	výraz	k1gInSc1	výraz
pro	pro	k7c4	pro
animaci	animace	k1gFnSc4	animace
<g/>
,	,	kIx,	,
psaný	psaný	k2eAgInSc4d1	psaný
v	v	k7c4	v
katakaně	katakaně	k6eAd1	katakaně
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ア	ア	k?	ア
(	(	kIx(	(
<g/>
animéšon	animéšon	k1gMnSc1	animéšon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
přímý	přímý	k2eAgInSc1d1	přímý
přepis	přepis	k1gInSc1	přepis
a	a	k8xC	a
převzaté	převzatý	k2eAgNnSc1d1	převzaté
slovo	slovo	k1gNnSc1	slovo
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
gairaigo	gairaigo	k1gNnSc4	gairaigo
<g/>
)	)	kIx)	)
anglického	anglický	k2eAgInSc2d1	anglický
termínu	termín	k1gInSc2	termín
animation	animation	k1gInSc1	animation
<g/>
.	.	kIx.	.
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1	japonská
zkratka	zkratka	k1gFnSc1	zkratka
tohoto	tento	k3xDgInSc2	tento
termínu	termín	k1gInSc2	termín
je	být	k5eAaImIp3nS	být
ア	ア	k?	ア
(	(	kIx(	(
<g/>
anime	animat	k5eAaPmIp3nS	animat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
výrazy	výraz	k1gInPc1	výraz
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
japonštině	japonština	k1gFnSc6	japonština
rovnocenné	rovnocenný	k2eAgFnSc6d1	rovnocenná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zkrácený	zkrácený	k2eAgInSc1d1	zkrácený
tvar	tvar	k1gInSc1	tvar
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
užíván	užíván	k2eAgInSc1d1	užíván
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc4	slovo
anime	animat	k5eAaPmIp3nS	animat
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
podstatné	podstatný	k2eAgNnSc1d1	podstatné
jméno	jméno	k1gNnSc1	jméno
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Díváš	dívat	k5eAaImIp2nS	dívat
se	se	k3xPyFc4	se
také	také	k9	také
na	na	k7c4	na
anime	anim	k1gInSc5	anim
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
přídavné	přídavný	k2eAgNnSc4d1	přídavné
jméno	jméno	k1gNnSc4	jméno
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Anime	Anim	k1gMnSc5	Anim
Guyver	Guyver	k1gMnSc1	Guyver
je	být	k5eAaImIp3nS	být
odlišný	odlišný	k2eAgMnSc1d1	odlišný
od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
filmového	filmový	k2eAgNnSc2d1	filmové
Guyvera	Guyvero	k1gNnSc2	Guyvero
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
je	být	k5eAaImIp3nS	být
nesklonné	sklonný	k2eNgNnSc1d1	nesklonné
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
tvar	tvar	k1gInSc1	tvar
množného	množný	k2eAgNnSc2d1	množné
čísla	číslo	k1gNnSc2	číslo
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Kolik	kolik	k9	kolik
už	už	k6eAd1	už
máš	mít	k5eAaImIp2nS	mít
anime	animat	k5eAaPmIp3nS	animat
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	O	kA	O
anime	animat	k5eAaPmIp3nS	animat
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
mluví	mluvit	k5eAaImIp3nS	mluvit
jako	jako	k9	jako
o	o	k7c4	o
japanimaci	japanimace	k1gFnSc4	japanimace
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
toto	tento	k3xDgNnSc1	tento
označení	označení	k1gNnSc1	označení
se	se	k3xPyFc4	se
již	již	k6eAd1	již
přestalo	přestat	k5eAaPmAgNnS	přestat
používat	používat	k5eAaImF	používat
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
japanimace	japanimace	k1gFnSc2	japanimace
zažilo	zažít	k5eAaPmAgNnS	zažít
svůj	svůj	k3xOyFgInSc4	svůj
vrchol	vrchol	k1gInSc4	vrchol
během	během	k7c2	během
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
<g/>
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
běžně	běžně	k6eAd1	běžně
používáno	používat	k5eAaImNgNnS	používat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
a	a	k8xC	a
druhé	druhý	k4xOgFnSc3	druhý
vlně	vlna	k1gFnSc3	vlna
anime	animat	k5eAaPmIp3nS	animat
příznivců	příznivec	k1gMnPc2	příznivec
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zlatá	zlatý	k2eAgFnSc1d1	zlatá
éra	éra	k1gFnSc1	éra
skončila	skončit	k5eAaPmAgFnS	skončit
před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
polovinou	polovina	k1gFnSc7	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
<g/>
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
vnímán	vnímat	k5eAaImNgInS	vnímat
spíše	spíše	k9	spíše
v	v	k7c6	v
nostalgickém	nostalgický	k2eAgInSc6d1	nostalgický
kontextu	kontext	k1gInSc6	kontext
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
pak	pak	k6eAd1	pak
označuje	označovat	k5eAaImIp3nS	označovat
domácí	domácí	k2eAgFnSc4d1	domácí
animovanou	animovaný	k2eAgFnSc4d1	animovaná
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
anime	animat	k5eAaPmIp3nS	animat
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
animéšon	animéšon	k1gInSc1	animéšon
<g/>
,	,	kIx,	,
popisuje	popisovat	k5eAaImIp3nS	popisovat
všechny	všechen	k3xTgInPc4	všechen
možné	možný	k2eAgInPc4d1	možný
druhy	druh	k1gInPc4	druh
animace	animace	k1gFnSc2	animace
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
japanimace	japanimace	k1gFnSc1	japanimace
využívána	využíván	k2eAgFnSc1d1	využívána
k	k	k7c3	k
odlišování	odlišování	k1gNnSc3	odlišování
japonských	japonský	k2eAgNnPc2d1	Japonské
děl	dělo	k1gNnPc2	dělo
od	od	k7c2	od
tvorby	tvorba	k1gFnSc2	tvorba
zbytku	zbytek	k1gInSc2	zbytek
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
poslední	poslední	k2eAgFnSc7d1	poslední
dobou	doba	k1gFnSc7	doba
slovo	slovo	k1gNnSc4	slovo
anime	animat	k5eAaPmIp3nS	animat
často	často	k6eAd1	často
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
též	též	k9	též
pojem	pojem	k1gInSc1	pojem
manga	mango	k1gNnSc2	mango
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zvyk	zvyk	k1gInSc1	zvyk
může	moct	k5eAaImIp3nS	moct
pramenit	pramenit	k5eAaImF	pramenit
z	z	k7c2	z
japonského	japonský	k2eAgNnSc2d1	Japonské
užití	užití	k1gNnSc2	užití
<g/>
:	:	kIx,	:
V	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
nabývá	nabývat	k5eAaImIp3nS	nabývat
manga	mango	k1gNnPc4	mango
významu	význam	k1gInSc2	význam
animace	animace	k1gFnSc2	animace
i	i	k8xC	i
komiksu	komiks	k1gInSc2	komiks
(	(	kIx(	(
<g/>
ačkoli	ačkoli	k8xS	ačkoli
použití	použití	k1gNnSc4	použití
slova	slovo	k1gNnSc2	slovo
manga	mango	k1gNnSc2	mango
pro	pro	k7c4	pro
animaci	animace	k1gFnSc4	animace
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
příznakem	příznak	k1gInSc7	příznak
'	'	kIx"	'
<g/>
nefanouška	nefanoušek	k1gMnSc4	nefanoušek
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
českými	český	k2eAgMnPc7d1	český
fanoušky	fanoušek	k1gMnPc7	fanoušek
má	mít	k5eAaImIp3nS	mít
obvykle	obvykle	k6eAd1	obvykle
manga	mango	k1gNnPc4	mango
přísnější	přísný	k2eAgInSc4d2	přísnější
význam	význam	k1gInSc4	význam
"	"	kIx"	"
<g/>
japonského	japonský	k2eAgInSc2d1	japonský
komiksu	komiks	k1gInSc2	komiks
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Anime	Animat	k5eAaPmIp3nS	Animat
je	on	k3xPp3gMnPc4	on
širokou	široký	k2eAgFnSc7d1	široká
paletou	paleta	k1gFnSc7	paleta
uměleckých	umělecký	k2eAgInPc2d1	umělecký
stylů	styl	k1gInPc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Liší	lišit	k5eAaImIp3nP	lišit
se	se	k3xPyFc4	se
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
od	od	k7c2	od
umělce	umělec	k1gMnSc2	umělec
k	k	k7c3	k
umělci	umělec	k1gMnSc3	umělec
nebo	nebo	k8xC	nebo
studio	studio	k1gNnSc1	studio
od	od	k7c2	od
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc7	jejich
typickými	typický	k2eAgInPc7d1	typický
rysy	rys	k1gInPc7	rys
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
velké	velký	k2eAgNnSc4d1	velké
<g/>
,	,	kIx,	,
kulaté	kulatý	k2eAgNnSc4d1	kulaté
<g/>
,	,	kIx,	,
roztomilé	roztomilý	k2eAgNnSc4d1	roztomilé
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
propracované	propracovaný	k2eAgFnPc4d1	propracovaná
hudební	hudební	k2eAgFnSc4d1	hudební
kulisy	kulisa	k1gFnPc4	kulisa
a	a	k8xC	a
postavy	postav	k1gInPc4	postav
stylizované	stylizovaný	k2eAgInPc4d1	stylizovaný
v	v	k7c6	v
rozmanitých	rozmanitý	k2eAgFnPc6d1	rozmanitá
scénách	scéna	k1gFnPc6	scéna
a	a	k8xC	a
příbězích	příběh	k1gInPc6	příběh
<g/>
,	,	kIx,	,
zaměřené	zaměřený	k2eAgFnPc1d1	zaměřená
na	na	k7c4	na
širší	široký	k2eAgNnSc4d2	širší
publikum	publikum	k1gNnSc4	publikum
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
anime	animat	k5eAaPmIp3nS	animat
existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
žánrových	žánrový	k2eAgFnPc2d1	žánrová
podkategorií	podkategorie	k1gFnPc2	podkategorie
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
náplní	náplň	k1gFnSc7	náplň
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
akce	akce	k1gFnSc1	akce
<g/>
,	,	kIx,	,
dobrodružství	dobrodružství	k1gNnPc1	dobrodružství
<g/>
,	,	kIx,	,
příběhy	příběh	k1gInPc1	příběh
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
komedie	komedie	k1gFnSc1	komedie
<g/>
,	,	kIx,	,
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
erotika	erotika	k1gFnSc1	erotika
(	(	kIx(	(
<g/>
hentai	hentai	k1gNnSc1	hentai
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fantasy	fantas	k1gInPc1	fantas
<g/>
,	,	kIx,	,
magie	magie	k1gFnPc1	magie
<g/>
,	,	kIx,	,
horor	horor	k1gInSc1	horor
či	či	k8xC	či
sci-fi	scii	k1gFnSc1	sci-fi
anime	animat	k5eAaPmIp3nS	animat
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
anime	animat	k5eAaPmIp3nS	animat
je	být	k5eAaImIp3nS	být
kombinací	kombinace	k1gFnSc7	kombinace
několika	několik	k4yIc2	několik
žánrů	žánr	k1gInPc2	žánr
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
témat	téma	k1gNnPc2	téma
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
těžké	těžký	k2eAgNnSc1d1	těžké
přiřadit	přiřadit	k5eAaPmF	přiřadit
titul	titul	k1gInSc4	titul
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
jednomu	jeden	k4xCgInSc3	jeden
žánru	žánr	k1gInSc3	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Pořad	pořad	k1gInSc1	pořad
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
jevit	jevit	k5eAaImF	jevit
jako	jako	k8xS	jako
dílo	dílo	k1gNnSc4	dílo
s	s	k7c7	s
jednoduchou	jednoduchý	k2eAgFnSc7d1	jednoduchá
zápletkou	zápletka	k1gFnSc7	zápletka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
daleko	daleko	k6eAd1	daleko
hlubší	hluboký	k2eAgFnSc1d2	hlubší
dějová	dějový	k2eAgFnSc1d1	dějová
linie	linie	k1gFnSc1	linie
nebo	nebo	k8xC	nebo
vývoj	vývoj	k1gInSc1	vývoj
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
u	u	k7c2	u
akčních	akční	k2eAgFnPc2d1	akční
anime	animat	k5eAaPmIp3nS	animat
není	být	k5eNaImIp3nS	být
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
dotýkají	dotýkat	k5eAaImIp3nP	dotýkat
takových	takový	k3xDgNnPc2	takový
témat	téma	k1gNnPc2	téma
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
romance	romance	k1gFnSc1	romance
<g/>
,	,	kIx,	,
humor	humor	k1gInSc1	humor
či	či	k8xC	či
společenská	společenský	k2eAgFnSc1d1	společenská
kritika	kritika	k1gFnSc1	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
některé	některý	k3yIgFnPc1	některý
romance	romance	k1gFnPc1	romance
jdou	jít	k5eAaImIp3nP	jít
ruku	ruka	k1gFnSc4	ruka
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
s	s	k7c7	s
akčními	akční	k2eAgInPc7d1	akční
prvky	prvek	k1gInPc7	prvek
nebo	nebo	k8xC	nebo
násilím	násilí	k1gNnSc7	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
anime	animat	k5eAaPmIp3nS	animat
směry	směr	k1gInPc4	směr
<g/>
:	:	kIx,	:
Šónen	Šónna	k1gFnPc2	Šónna
česky	česky	k6eAd1	česky
'	'	kIx"	'
<g/>
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
šónen	šónen	k1gInSc1	šónen
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
žánru	žánr	k1gInSc3	žánr
seinen	seinen	k1gInSc1	seinen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
na	na	k7c4	na
mladší	mladý	k2eAgInPc4d2	mladší
ročníky	ročník	k1gInPc4	ročník
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
akční	akční	k2eAgMnSc1d1	akční
anime	animat	k5eAaPmIp3nS	animat
<g/>
.	.	kIx.	.
například	například	k6eAd1	například
Naruto	Narut	k2eAgNnSc1d1	Naruto
<g/>
,	,	kIx,	,
Bleach	Bleach	k1gMnSc1	Bleach
<g/>
,	,	kIx,	,
Dragon	Dragon	k1gMnSc1	Dragon
Ball	Ball	k1gMnSc1	Ball
<g/>
,	,	kIx,	,
One	One	k1gFnPc1	One
Piece	pieca	k1gFnSc3	pieca
<g/>
,	,	kIx,	,
Fullmetal	Fullmetal	k1gFnSc1	Fullmetal
Alchemist	Alchemist	k1gMnSc1	Alchemist
Šódžo	Šódžo	k1gMnSc1	Šódžo
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
'	'	kIx"	'
<g/>
mladá	mladý	k2eAgFnSc1d1	mladá
slečna	slečna	k1gFnSc1	slečna
<g/>
'	'	kIx"	'
nebo	nebo	k8xC	nebo
'	'	kIx"	'
<g/>
holčička	holčička	k1gFnSc1	holčička
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Žánr	žánr	k1gInSc1	žánr
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
dívky	dívka	k1gFnPc4	dívka
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
romantické	romantický	k2eAgNnSc4d1	romantické
anime	animat	k5eAaPmIp3nS	animat
<g/>
.	.	kIx.	.
například	například	k6eAd1	například
Bišódžo	Bišódžo	k6eAd1	Bišódžo
senši	sensat	k5eAaPmIp1nSwK	sensat
Sailor	Sailor	k1gMnSc1	Sailor
Moon	Moon	k1gMnSc1	Moon
<g/>
,	,	kIx,	,
Candy	Canda	k1gMnSc2	Canda
Candy	Canda	k1gMnSc2	Canda
<g/>
,	,	kIx,	,
Ouran	Ouran	k1gInSc1	Ouran
High	Higha	k1gFnPc2	Higha
School	School	k1gInSc1	School
Host	host	k1gMnSc1	host
Club	club	k1gInSc1	club
<g/>
,	,	kIx,	,
Versailles	Versailles	k1gFnSc1	Versailles
no	no	k9	no
Bara	Bara	k1gFnSc1	Bara
<g/>
,	,	kIx,	,
Kimi	Kimi	k1gNnSc1	Kimi
ni	on	k3xPp3gFnSc4	on
todoke	todoke	k1gFnSc4	todoke
Mahó	Mahó	k1gMnSc1	Mahó
šódžo	šódžo	k1gMnSc1	šódžo
Podkategorie	Podkategorie	k1gFnSc2	Podkategorie
šódžo	šódžo	k1gMnSc1	šódžo
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
náplní	náplň	k1gFnSc7	náplň
jsou	být	k5eAaImIp3nP	být
příběhy	příběh	k1gInPc4	příběh
o	o	k7c6	o
magicky	magicky	k6eAd1	magicky
nadaných	nadaný	k2eAgFnPc6d1	nadaná
dívkách	dívka	k1gFnPc6	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Bišódžo	Bišódžo	k6eAd1	Bišódžo
senši	sensat	k5eAaPmIp1nSwK	sensat
Sailor	Sailor	k1gMnSc1	Sailor
Moon	Moon	k1gMnSc1	Moon
<g/>
,	,	kIx,	,
Sally	Sall	k1gInPc1	Sall
<g/>
,	,	kIx,	,
malá	malý	k2eAgFnSc1d1	malá
čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
<g/>
,	,	kIx,	,
Princess	Princess	k1gInSc1	Princess
Tutu	tut	k1gInSc2	tut
<g/>
,	,	kIx,	,
Mahó	Mahó	k1gFnSc1	Mahó
šódžo	šódžo	k1gNnSc1	šódžo
Madoka	Madoek	k1gMnSc2	Madoek
Magica	Magicus	k1gMnSc2	Magicus
Mahó	Mahó	k1gMnSc2	Mahó
šónen	šónen	k2eAgInSc1d1	šónen
Mužský	mužský	k2eAgInSc1d1	mužský
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
mahó	mahó	k?	mahó
šódžo	šódžo	k6eAd1	šódžo
<g/>
.	.	kIx.	.
například	například	k6eAd1	například
D.	D.	kA	D.
<g/>
N.	N.	kA	N.
Angel	angel	k1gMnSc1	angel
<g/>
,	,	kIx,	,
Binan	Binan	k1gMnSc1	Binan
kókó	kókó	k?	kókó
čikjú	čikjú	k?	čikjú
bóei-bu	bóeiat	k5eAaPmIp1nS	bóei-bat
LOVE	lov	k1gInSc5	lov
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
Ao	Ao	k1gMnSc1	Ao
no	no	k9	no
Exorcist	Exorcist	k1gInSc4	Exorcist
Džosei	Džose	k1gFnSc2	Džose
Japonský	japonský	k2eAgInSc4d1	japonský
výraz	výraz	k1gInSc4	výraz
pro	pro	k7c4	pro
'	'	kIx"	'
<g/>
mladou	mladý	k2eAgFnSc4d1	mladá
ženu	žena	k1gFnSc4	žena
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Anime	Animat	k5eAaPmIp3nS	Animat
zaměřené	zaměřený	k2eAgNnSc4d1	zaměřené
na	na	k7c4	na
mladé	mladý	k2eAgFnPc4d1	mladá
ženy	žena	k1gFnPc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvzácnějších	vzácný	k2eAgMnPc2d3	nejvzácnější
typů	typ	k1gInPc2	typ
anime	animat	k5eAaPmIp3nS	animat
<g/>
.	.	kIx.	.
například	například	k6eAd1	například
Nana	Nana	k1gFnSc1	Nana
<g/>
,	,	kIx,	,
Paradise	Paradise	k1gFnSc1	Paradise
Kiss	Kiss	k1gInSc4	Kiss
Ečči	Ečč	k1gFnSc2	Ečč
Odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
japonské	japonský	k2eAgFnSc2d1	japonská
výslovnosti	výslovnost	k1gFnSc2	výslovnost
písmena	písmeno	k1gNnSc2	písmeno
'	'	kIx"	'
<g/>
H	H	kA	H
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
počátku	počátek	k1gInSc2	počátek
slova	slovo	k1gNnSc2	slovo
'	'	kIx"	'
<g/>
hentai	hentai	k6eAd1	hentai
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
znamená	znamenat	k5eAaImIp3nS	znamenat
něco	něco	k3yInSc4	něco
jako	jako	k9	jako
'	'	kIx"	'
<g/>
obscénní	obscénní	k2eAgFnSc1d1	obscénní
sexualita	sexualita	k1gFnSc1	sexualita
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
lehký	lehký	k2eAgInSc1d1	lehký
sexuální	sexuální	k2eAgInSc1d1	sexuální
humor	humor	k1gInSc1	humor
a	a	k8xC	a
fanservis	fanservis	k1gInSc1	fanservis
<g/>
.	.	kIx.	.
například	například	k6eAd1	například
To	to	k9	to
LOVE-Ru	LOVE-Ra	k1gFnSc4	LOVE-Ra
<g/>
,	,	kIx,	,
No	no	k9	no
Game	game	k1gInSc1	game
No	no	k9	no
Life	Life	k1gInSc1	Life
<g/>
,	,	kIx,	,
Kill	Kill	k1gMnSc1	Kill
la	la	k1gNnSc2	la
Kill	Kill	k1gMnSc1	Kill
<g/>
,	,	kIx,	,
High	High	k1gMnSc1	High
School	Schoola	k1gFnPc2	Schoola
DxD	DxD	k1gFnPc2	DxD
<g/>
,	,	kIx,	,
Golden	Goldna	k1gFnPc2	Goldna
Boy	boa	k1gFnSc2	boa
<g/>
,	,	kIx,	,
Highschool	Highschool	k1gInSc1	Highschool
of	of	k?	of
the	the	k?	the
Dead	Dead	k1gInSc1	Dead
Hentai	Henta	k1gFnSc2	Henta
Hentai	Henta	k1gFnSc2	Henta
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
anime	animat	k5eAaPmIp3nS	animat
s	s	k7c7	s
erotickou	erotický	k2eAgFnSc7d1	erotická
nebo	nebo	k8xC	nebo
pornografickou	pornografický	k2eAgFnSc7d1	pornografická
tematikou	tematika	k1gFnSc7	tematika
<g/>
.	.	kIx.	.
například	například	k6eAd1	například
Oni	onen	k3xDgMnPc1	onen
čiči	čiči	k0	čiči
<g/>
,	,	kIx,	,
Cream	Cream	k1gInSc1	Cream
Lemon	lemon	k1gInSc1	lemon
<g/>
,	,	kIx,	,
Bible	bible	k1gFnSc1	bible
Black	Black	k1gMnSc1	Black
<g/>
,	,	kIx,	,
Kanodžo	Kanodžo	k1gMnSc1	Kanodžo
×	×	k?	×
kanodžo	kanodžo	k1gMnSc1	kanodžo
×	×	k?	×
kanodžo	kanodžo	k1gMnSc1	kanodžo
Kodomo	Kodoma	k1gFnSc5	Kodoma
Japonský	japonský	k2eAgMnSc1d1	japonský
výraz	výraz	k1gInSc4	výraz
pro	pro	k7c4	pro
'	'	kIx"	'
<g/>
dítě	dítě	k1gNnSc4	dítě
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Určeno	určit	k5eAaPmNgNnS	určit
pro	pro	k7c4	pro
malé	malý	k2eAgFnPc4d1	malá
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
například	například	k6eAd1	například
Hello	Hello	k1gNnSc1	Hello
Kitty	Kitta	k1gFnSc2	Kitta
<g/>
,	,	kIx,	,
Keropi	Keropi	k1gNnSc2	Keropi
<g/>
,	,	kIx,	,
Panda-Z	Panda-Z	k1gMnSc1	Panda-Z
The	The	k1gFnSc2	The
Robonimation	Robonimation	k1gInSc1	Robonimation
<g/>
,	,	kIx,	,
Kodomo	Kodoma	k1gFnSc5	Kodoma
no	no	k9	no
omoča	omoča	k1gMnSc1	omoča
<g/>
,	,	kIx,	,
Doraemon	Doraemon	k1gMnSc1	Doraemon
Moe	Moe	k1gMnSc1	Moe
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
postavy	postava	k1gFnPc4	postava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vypadají	vypadat	k5eAaImIp3nP	vypadat
mladší	mladý	k2eAgInPc1d2	mladší
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gInSc4	jejich
skutečný	skutečný	k2eAgInSc4d1	skutečný
věk	věk	k1gInSc4	věk
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
extrémně	extrémně	k6eAd1	extrémně
roztomilé	roztomilý	k2eAgFnPc1d1	roztomilá
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
dobou	doba	k1gFnSc7	doba
anime	animat	k5eAaPmIp3nS	animat
zažívá	zažívat	k5eAaImIp3nS	zažívat
trend	trend	k1gInSc1	trend
moe	moe	k?	moe
<g/>
.	.	kIx.	.
například	například	k6eAd1	například
Akazukin	Akazukin	k1gInSc1	Akazukin
čača	čača	k1gFnSc1	čača
<g/>
,	,	kIx,	,
Ičigo	Ičigo	k6eAd1	Ičigo
mašimaro	mašimara	k1gFnSc5	mašimara
<g/>
,	,	kIx,	,
Mahó	Mahó	k1gMnSc1	Mahó
šódžo	šódžo	k1gMnSc1	šódžo
Lyrical	Lyrical	k1gMnSc1	Lyrical
Nanoha	Nanoha	k1gMnSc1	Nanoha
<g/>
,	,	kIx,	,
Gakuen	Gakuen	k1gInSc1	Gakuen
Utopia	Utopium	k1gNnSc2	Utopium
Manabi	Manab	k1gFnSc2	Manab
Straight	Straighta	k1gFnPc2	Straighta
<g/>
!	!	kIx.	!
</s>
<s>
Mecha	Mecha	k1gFnSc1	Mecha
Anime	Anim	k1gInSc5	Anim
s	s	k7c7	s
gigantickými	gigantický	k2eAgMnPc7d1	gigantický
roboty	robot	k1gMnPc7	robot
<g/>
.	.	kIx.	.
například	například	k6eAd1	například
série	série	k1gFnSc1	série
Mobile	mobile	k1gNnSc2	mobile
Suit	suita	k1gFnPc2	suita
Gundam	Gundam	k1gInSc1	Gundam
<g/>
,	,	kIx,	,
Neon	neon	k1gInSc1	neon
Genesis	Genesis	k1gFnSc1	Genesis
Evangelion	Evangelion	k1gInSc1	Evangelion
<g/>
,	,	kIx,	,
Code	Code	k1gInSc1	Code
Geass	Geass	k1gInSc1	Geass
<g/>
:	:	kIx,	:
Hangjaku	Hangjak	k1gInSc2	Hangjak
no	no	k9	no
Lelouch	Lelouch	k1gInSc1	Lelouch
Seinen	Seinen	k2eAgInSc1d1	Seinen
Anime	Anim	k1gInSc5	Anim
pro	pro	k7c4	pro
dospívající	dospívající	k2eAgMnPc4d1	dospívající
i	i	k8xC	i
dospělé	dospělý	k2eAgMnPc4d1	dospělý
muže	muž	k1gMnPc4	muž
<g/>
.	.	kIx.	.
například	například	k6eAd1	například
Mušiši	Mušiš	k1gMnPc1	Mušiš
<g/>
,	,	kIx,	,
Berserk	berserk	k1gMnSc1	berserk
<g/>
,	,	kIx,	,
Seihó	Seihó	k1gMnSc1	Seihó
bukjó	bukjó	k?	bukjó
Outlaw	Outlaw	k1gFnSc2	Outlaw
Star	Star	kA	Star
<g/>
,	,	kIx,	,
Kovboj	kovboj	k1gMnSc1	kovboj
Bebop	bebop	k1gInSc1	bebop
<g/>
,	,	kIx,	,
Ping	pingo	k1gNnPc2	pingo
Pong	Pong	k1gInSc1	Pong
the	the	k?	the
Animation	Animation	k1gInSc1	Animation
<g/>
,	,	kIx,	,
Hellsing	Hellsing	k1gInSc1	Hellsing
Ultimate	Ultimat	k1gInSc5	Ultimat
<g/>
,	,	kIx,	,
Baccano	Baccana	k1gFnSc5	Baccana
<g/>
!	!	kIx.	!
</s>
<s>
Sentai	Sentai	k1gNnSc1	Sentai
<g/>
/	/	kIx~	/
<g/>
Super	super	k2eAgNnSc1d1	super
sentai	sentai	k1gNnSc1	sentai
Doslova	doslova	k6eAd1	doslova
"	"	kIx"	"
<g/>
bojující	bojující	k2eAgInSc1d1	bojující
tým	tým	k1gInSc1	tým
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
jakékoliv	jakýkoliv	k3yIgNnSc1	jakýkoliv
dílo	dílo	k1gNnSc1	dílo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
tým	tým	k1gInSc1	tým
superhrdinů	superhrdin	k1gInPc2	superhrdin
<g/>
.	.	kIx.	.
například	například	k6eAd1	například
Cyborg	Cyborg	k1gInSc1	Cyborg
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
Gekiganger	Gekiganger	k1gInSc1	Gekiganger
3	[number]	k4	3
<g/>
:	:	kIx,	:
Nekkecu	Nekkecus	k1gInSc2	Nekkecus
daikessen	daikessen	k1gInSc1	daikessen
<g/>
!!	!!	k?	!!
<g/>
,	,	kIx,	,
Samurai	Samurae	k1gFnSc4	Samurae
Flamenco	flamenco	k1gNnSc1	flamenco
Dementia	Dementium	k1gNnSc2	Dementium
Anime	Anim	k1gInSc5	Anim
s	s	k7c7	s
nepředvídatelným	předvídatelný	k2eNgInSc7d1	nepředvídatelný
zvratem	zvrat	k1gInSc7	zvrat
v	v	k7c6	v
ději	děj	k1gInSc6	děj
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
také	také	k9	také
označení	označení	k1gNnSc1	označení
experimentálních	experimentální	k2eAgMnPc2d1	experimentální
anime	animat	k5eAaPmIp3nS	animat
<g/>
.	.	kIx.	.
například	například	k6eAd1	například
Neon	neon	k1gInSc4	neon
Genesis	Genesis	k1gFnSc1	Genesis
Evangelion	Evangelion	k1gInSc1	Evangelion
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
End	End	k1gFnSc2	End
of	of	k?	of
Evangelion	Evangelion	k1gInSc1	Evangelion
<g/>
,	,	kIx,	,
FLCL	FLCL	kA	FLCL
<g/>
,	,	kIx,	,
Serial	Serial	k1gInSc1	Serial
Experiment	experiment	k1gInSc1	experiment
Lain	Lain	k1gInSc1	Lain
<g/>
,	,	kIx,	,
Perfect	Perfect	k2eAgInSc1d1	Perfect
Blue	Blue	k1gInSc1	Blue
<g/>
,	,	kIx,	,
Nami	Nami	k1gNnSc1	Nami
<g/>
,	,	kIx,	,
Ucu	Ucu	k1gFnSc1	Ucu
musume	musum	k1gInSc5	musum
Sajuri	Sajur	k1gFnSc6	Sajur
Šónen-ai	Šónen	k1gFnSc6	Šónen-a
<g/>
/	/	kIx~	/
<g/>
Yaoi	Yao	k1gInSc6	Yao
Japonský	japonský	k2eAgInSc4d1	japonský
výraz	výraz	k1gInSc4	výraz
pro	pro	k7c4	pro
'	'	kIx"	'
<g/>
chlapeckou	chlapecký	k2eAgFnSc4d1	chlapecká
lásku	láska	k1gFnSc4	láska
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Anime	Aniet	k5eAaPmRp1nP	Aniet
je	být	k5eAaImIp3nS	být
zaměřeno	zaměřit	k5eAaPmNgNnS	zaměřit
na	na	k7c4	na
sex	sex	k1gInSc4	sex
(	(	kIx(	(
<g/>
jaoi	jaoi	k6eAd1	jaoi
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
na	na	k7c4	na
vztah	vztah	k1gInSc4	vztah
(	(	kIx(	(
<g/>
šónen-ai	šónene	k1gFnSc4	šónen-ae
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
muži	muž	k1gMnPc7	muž
<g/>
/	/	kIx~	/
<g/>
chlapci	chlapec	k1gMnPc7	chlapec
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
šónen-ai	šóneni	k6eAd1	šónen-ai
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
vyřazován	vyřazovat	k5eAaImNgInS	vyřazovat
z	z	k7c2	z
japonského	japonský	k2eAgInSc2d1	japonský
jazyka	jazyk	k1gInSc2	jazyk
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gInSc3	jeho
postrannímu	postranní	k2eAgInSc3d1	postranní
významu	význam	k1gInSc3	význam
–	–	k?	–
efebofilie	efebofilie	k1gFnPc4	efebofilie
–	–	k?	–
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nahrazován	nahrazovat	k5eAaImNgInS	nahrazovat
slovní	slovní	k2eAgFnSc7d1	slovní
vazbou	vazba	k1gFnSc7	vazba
Boy	boa	k1gFnSc2	boa
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
BL	BL	kA	BL
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
chlapecká	chlapecký	k2eAgFnSc1d1	chlapecká
láska	láska	k1gFnSc1	láska
<g/>
.	.	kIx.	.
například	například	k6eAd1	například
Loveless	Loveless	k1gInSc1	Loveless
<g/>
,	,	kIx,	,
Gravitation	Gravitation	k1gInSc1	Gravitation
<g/>
,	,	kIx,	,
Zecuai	Zecuai	k1gNnSc1	Zecuai
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Ai	Ai	k1gMnSc3	Ai
no	no	k9	no
kusabi	kusabi	k6eAd1	kusabi
<g/>
,	,	kIx,	,
Fudžimi	Fudži	k1gFnPc7	Fudži
2	[number]	k4	2
<g/>
-čóme	-čómat	k5eAaPmIp3nS	-čómat
kokjogakudan	kokjogakudan	k1gMnSc1	kokjogakudan
<g/>
,	,	kIx,	,
Keiraku	Keirak	k1gMnSc3	Keirak
no	no	k9	no
hóteišiki	hóteišik	k1gMnSc3	hóteišik
Level-C	Level-C	k1gMnSc3	Level-C
<g/>
,	,	kIx,	,
Suki	Suk	k1gMnSc3	Suk
na	na	k7c4	na
mono	mono	k2eAgFnPc4d1	mono
wa	wa	k?	wa
suki	suk	k1gFnSc2	suk
dakara	dakar	k1gMnSc2	dakar
šó	šó	k?	šó
ga	ga	k?	ga
nai	nai	k?	nai
<g/>
!!	!!	k?	!!
<g/>
,	,	kIx,	,
Junjou	Junjá	k1gFnSc4	Junjá
Romantica	Romanticum	k1gNnSc2	Romanticum
<g/>
,	,	kIx,	,
Sekaiichi	Sekaiich	k1gFnSc2	Sekaiich
Hatsukoi	Hatsuko	k1gFnSc2	Hatsuko
Šódžo-ai	Šódžo	k1gFnSc2	Šódžo-a
<g/>
/	/	kIx~	/
<g/>
juri	juri	k6eAd1	juri
Tento	tento	k3xDgInSc4	tento
typ	typ	k1gInSc4	typ
mangy	mango	k1gNnPc7	mango
má	mít	k5eAaImIp3nS	mít
podobnou	podobný	k2eAgFnSc4d1	podobná
náplň	náplň	k1gFnSc4	náplň
jako	jako	k8xS	jako
<g />
.	.	kIx.	.
</s>
<s>
jaoi	jaoi	k1gNnSc1	jaoi
<g/>
/	/	kIx~	/
<g/>
šónen-ai	šóneni	k1gNnSc1	šónen-ai
<g/>
,	,	kIx,	,
jen	jen	k6eAd1	jen
je	být	k5eAaImIp3nS	být
zaměřen	zaměřit	k5eAaPmNgInS	zaměřit
na	na	k7c4	na
vztah	vztah	k1gInSc4	vztah
dvou	dva	k4xCgFnPc2	dva
dívek	dívka	k1gFnPc2	dívka
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
většinou	většinou	k6eAd1	většinou
spíše	spíše	k9	spíše
na	na	k7c6	na
platonické	platonický	k2eAgFnSc6d1	platonická
úrovni	úroveň	k1gFnSc6	úroveň
(	(	kIx(	(
<g/>
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
šódžo-ai	šódžo	k1gFnSc6	šódžo-a
<g/>
)	)	kIx)	)
než	než	k8xS	než
na	na	k7c4	na
sexuální	sexuální	k2eAgFnSc4d1	sexuální
(	(	kIx(	(
<g/>
juri	jure	k1gFnSc4	jure
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
například	například	k6eAd1	například
Šódžo	Šódžo	k6eAd1	Šódžo
kakumei	kakumei	k6eAd1	kakumei
Utena	Utena	k1gFnSc1	Utena
<g/>
,	,	kIx,	,
Maria-sama	Mariaama	k1gFnSc1	Maria-sama
ga	ga	k?	ga
miteru	mitrat	k5eAaPmIp1nS	mitrat
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Kašimaši	Kašimaš	k1gMnPc5	Kašimaš
<g/>
:	:	kIx,	:
Girl	girl	k1gFnSc1	girl
Meets	Meets	k1gInSc1	Meets
Girl	girl	k1gFnSc1	girl
<g/>
,	,	kIx,	,
Juri	Juri	k1gNnSc1	Juri
kuma	kum	k1gMnSc2	kum
araši	arasat	k5eAaPmIp1nSwK	arasat
<g/>
,	,	kIx,	,
Juru	jura	k1gFnSc4	jura
juri	juri	k1gNnSc2	juri
Animace	animace	k1gFnSc2	animace
Komiks	komiks	k1gInSc1	komiks
Manga	mango	k1gNnSc2	mango
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
anime	animat	k5eAaPmIp3nS	animat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
České	český	k2eAgInPc1d1	český
odkazy	odkaz	k1gInPc1	odkaz
Shirai	Shira	k1gFnSc2	Shira
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
:	:	kIx,	:
Novinkový	novinkový	k2eAgInSc1d1	novinkový
portál	portál	k1gInSc1	portál
zabývající	zabývající	k2eAgInSc1d1	zabývající
se	se	k3xPyFc4	se
popularizací	popularizace	k1gFnSc7	popularizace
anime	animat	k5eAaPmIp3nS	animat
a	a	k8xC	a
mangy	mango	k1gNnPc7	mango
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Manga	mango	k1gNnPc4	mango
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
:	:	kIx,	:
Recenze	recenze	k1gFnSc1	recenze
<g/>
,	,	kIx,	,
japonská	japonský	k2eAgFnSc1d1	japonská
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
japonština	japonština	k1gFnSc1	japonština
<g/>
,	,	kIx,	,
fan	fana	k1gFnPc2	fana
art	art	k?	art
REANIMATED	REANIMATED	kA	REANIMATED
<g/>
:	:	kIx,	:
Český	český	k2eAgMnSc1d1	český
e-zine	einout	k5eAaPmIp3nS	e-zinout
o	o	k7c6	o
anime	animat	k5eAaPmIp3nS	animat
seriálech	seriál	k1gInPc6	seriál
a	a	k8xC	a
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
manga	mango	k1gNnPc4	mango
and	and	k?	and
japonské	japonský	k2eAgFnSc3d1	japonská
kultuře	kultura	k1gFnSc3	kultura
FOXAXE	FOXAXE	kA	FOXAXE
<g/>
:	:	kIx,	:
okolo	okolo	k7c2	okolo
30	[number]	k4	30
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
recenzí	recenze	k1gFnPc2	recenze
anime	animat	k5eAaPmIp3nS	animat
<g/>
,	,	kIx,	,
novinky	novinka	k1gFnPc4	novinka
Akihabara	Akihabara	k1gFnSc1	Akihabara
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
:	:	kIx,	:
Československá	československý	k2eAgFnSc1d1	Československá
portál	portál	k1gInSc1	portál
do	do	k7c2	do
světa	svět	k1gInSc2	svět
japonské	japonský	k2eAgFnSc2d1	japonská
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
největší	veliký	k2eAgFnSc4d3	veliký
ČS	čs	kA	čs
databázi	databáze	k1gFnSc4	databáze
anime	animat	k5eAaPmIp3nS	animat
<g/>
,	,	kIx,	,
mangy	mango	k1gNnPc7	mango
<g/>
,	,	kIx,	,
doramy	doram	k1gInPc7	doram
<g/>
,	,	kIx,	,
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
japonské	japonský	k2eAgFnSc2d1	japonská
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Cizojazyčné	cizojazyčný	k2eAgInPc1d1	cizojazyčný
odkazy	odkaz	k1gInPc1	odkaz
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
AniDB	AniDB	k1gFnSc1	AniDB
<g/>
:	:	kIx,	:
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
anime	animat	k5eAaPmIp3nS	animat
databáze	databáze	k1gFnSc1	databáze
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Myanimelist	Myanimelist	k1gInSc1	Myanimelist
<g/>
:	:	kIx,	:
Nejpoužívanější	používaný	k2eAgFnSc1d3	nejpoužívanější
anime	animat	k5eAaPmIp3nS	animat
databáze	databáze	k1gFnSc1	databáze
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Animenfo	Animenfo	k6eAd1	Animenfo
<g/>
:	:	kIx,	:
Server	server	k1gInSc1	server
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
anime	animat	k5eAaPmIp3nS	animat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rozsáhlou	rozsáhlý	k2eAgFnSc7d1	rozsáhlá
databází	databáze	k1gFnSc7	databáze
anime	animat	k5eAaPmIp3nS	animat
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Anime	Anim	k1gInSc5	Anim
News	News	k1gInSc4	News
Network	network	k1gInSc1	network
<g/>
:	:	kIx,	:
Rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
anime	animat	k5eAaPmIp3nS	animat
databáze	databáze	k1gFnSc1	databáze
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
T.H.E.	T.H.E.	k1gMnPc1	T.H.E.
<g/>
M.	M.	kA	M.
Anime	Anim	k1gInSc5	Anim
Reviews	Reviews	k1gInSc1	Reviews
<g/>
:	:	kIx,	:
Recenze	recenze	k1gFnSc1	recenze
anime	animat	k5eAaPmIp3nS	animat
seriálů	seriál	k1gInPc2	seriál
a	a	k8xC	a
filmů	film	k1gInPc2	film
<g/>
.	.	kIx.	.
</s>
