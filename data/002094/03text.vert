<s>
San	San	k?	San
Diego	Diego	k6eAd1	Diego
je	být	k5eAaImIp3nS	být
přímořské	přímořský	k2eAgNnSc1d1	přímořské
město	město	k1gNnSc1	město
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
státě	stát	k1gInSc6	stát
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
jihozápadním	jihozápadní	k2eAgNnSc6d1	jihozápadní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
tu	tu	k6eAd1	tu
žilo	žít	k5eAaImAgNnS	žít
přes	přes	k7c4	přes
1,3	[number]	k4	1,3
miliónu	milión	k4xCgInSc2	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
druhé	druhý	k4xOgNnSc1	druhý
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
a	a	k8xC	a
osmé	osmý	k4xOgNnSc1	osmý
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
severně	severně	k6eAd1	severně
od	od	k7c2	od
státní	státní	k2eAgFnSc2d1	státní
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Mexikem	Mexiko	k1gNnSc7	Mexiko
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
přímo	přímo	k6eAd1	přímo
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
mexickým	mexický	k2eAgNnSc7d1	mexické
městem	město	k1gNnSc7	město
Tijuana	Tijuana	k1gFnSc1	Tijuana
<g/>
.	.	kIx.	.
</s>
<s>
San	San	k?	San
Diego	Diego	k6eAd1	Diego
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
amerického	americký	k2eAgNnSc2d1	americké
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
přímořské	přímořský	k2eAgNnSc1d1	přímořské
podnebí	podnebí	k1gNnSc1	podnebí
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
množství	množství	k1gNnSc1	množství
pláží	pláž	k1gFnPc2	pláž
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
je	být	k5eAaImIp3nS	být
18	[number]	k4	18
°	°	k?	°
<g/>
C.	C.	kA	C.
Hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
příjmů	příjem	k1gInPc2	příjem
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
turismus	turismus	k1gInSc1	turismus
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc1	obchod
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
biotechnologie	biotechnologie	k1gFnPc1	biotechnologie
<g/>
,	,	kIx,	,
počítačové	počítačový	k2eAgFnPc1d1	počítačová
vědy	věda	k1gFnPc1	věda
a	a	k8xC	a
elektronika	elektronika	k1gFnSc1	elektronika
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
San	San	k1gMnSc2	San
Diega	Dieg	k1gMnSc2	Dieg
bylo	být	k5eAaImAgNnS	být
nalezeno	nalézt	k5eAaBmNgNnS	nalézt
prehistorické	prehistorický	k2eAgNnSc1d1	prehistorické
sídliště	sídliště	k1gNnSc1	sídliště
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
vůbec	vůbec	k9	vůbec
nejstarším	starý	k2eAgInPc3d3	nejstarší
na	na	k7c6	na
americkém	americký	k2eAgInSc6d1	americký
kontinentě	kontinent	k1gInSc6	kontinent
(	(	kIx(	(
<g/>
Salton	Salton	k1gInSc1	Salton
Sea	Sea	k1gFnSc2	Sea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
předkolumbovské	předkolumbovský	k2eAgFnSc2d1	předkolumbovská
byla	být	k5eAaImAgFnS	být
oblast	oblast	k1gFnSc1	oblast
osídlena	osídlit	k5eAaPmNgFnS	osídlit
především	především	k9	především
lidmi	člověk	k1gMnPc7	člověk
mluvícími	mluvící	k2eAgInPc7d1	mluvící
jazyky	jazyk	k1gInPc7	jazyk
Hokan	Hokany	k1gInPc2	Hokany
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
Evropané	Evropan	k1gMnPc1	Evropan
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
vylodili	vylodit	k5eAaPmAgMnP	vylodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1542	[number]	k4	1542
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1602	[number]	k4	1602
zde	zde	k6eAd1	zde
přistává	přistávat	k5eAaImIp3nS	přistávat
španělský	španělský	k2eAgMnSc1d1	španělský
conquistador	conquistador	k1gMnSc1	conquistador
Sebastian	Sebastian	k1gMnSc1	Sebastian
Viscaino	Viscaino	k1gNnSc4	Viscaino
a	a	k8xC	a
pojmenovává	pojmenovávat	k5eAaImIp3nS	pojmenovávat
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
San	San	k1gMnSc1	San
Diego	Diego	k1gMnSc1	Diego
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
založena	založen	k2eAgFnSc1d1	založena
misijní	misijní	k2eAgFnSc1d1	misijní
stanice	stanice	k1gFnSc1	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Objevení	objevení	k1gNnSc1	objevení
nalezišť	naleziště	k1gNnPc2	naleziště
zlata	zlato	k1gNnSc2	zlato
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
nastartovalo	nastartovat	k5eAaPmAgNnS	nastartovat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
horečku	horečka	k1gFnSc4	horečka
a	a	k8xC	a
do	do	k7c2	do
města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
železnice	železnice	k1gFnSc1	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
pokusy	pokus	k1gInPc4	pokus
o	o	k7c4	o
modernizaci	modernizace	k1gFnSc4	modernizace
města	město	k1gNnSc2	město
během	během	k7c2	během
výstavy	výstava	k1gFnSc2	výstava
Panamá-California	Panamá-Californium	k1gNnSc2	Panamá-Californium
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
-	-	kIx~	-
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
a	a	k8xC	a
výstavby	výstavba	k1gFnSc2	výstavba
leteckých	letecký	k2eAgInPc2d1	letecký
závodů	závod	k1gInPc2	závod
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
san	san	k?	san
Diego	Diego	k1gNnSc1	Diego
až	až	k9	až
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
relativně	relativně	k6eAd1	relativně
provinčním	provinční	k2eAgNnSc7d1	provinční
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
napadení	napadení	k1gNnSc6	napadení
Pearl	Pearl	k1gInSc4	Pearl
Harboru	Harbor	k1gInSc2	Harbor
byl	být	k5eAaImAgMnS	být
do	do	k7c2	do
San	San	k1gMnSc2	San
Diega	Dieg	k1gMnSc2	Dieg
přesunut	přesunut	k2eAgInSc1d1	přesunut
vrchní	vrchní	k2eAgInSc1d1	vrchní
štáb	štáb	k1gInSc1	štáb
Americké	americký	k2eAgFnSc2d1	americká
pacifické	pacifický	k2eAgFnSc2d1	Pacifická
flotily	flotila	k1gFnSc2	flotila
(	(	kIx(	(
<g/>
US	US	kA	US
Pacific	Pacific	k1gMnSc1	Pacific
Fleet	Fleet	k1gMnSc1	Fleet
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
rozmachu	rozmach	k1gInSc3	rozmach
místní	místní	k2eAgFnSc2d1	místní
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Gaslamp	Gaslamp	k1gInSc1	Gaslamp
Quarter	quarter	k1gInSc1	quarter
(	(	kIx(	(
<g/>
Čtvrť	čtvrť	k1gFnSc1	čtvrť
plynových	plynový	k2eAgFnPc2d1	plynová
lamp	lampa	k1gFnPc2	lampa
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
jediná	jediný	k2eAgFnSc1d1	jediná
zachovalá	zachovalý	k2eAgFnSc1d1	zachovalá
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
demolici	demolice	k1gFnSc6	demolice
a	a	k8xC	a
přestavbě	přestavba	k1gFnSc6	přestavba
této	tento	k3xDgFnSc2	tento
čtvrti	čtvrt	k1gFnSc2	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
tomuto	tento	k3xDgInSc3	tento
postupu	postup	k1gInSc3	postup
se	se	k3xPyFc4	se
zvedla	zvednout	k5eAaPmAgFnS	zvednout
místní	místní	k2eAgFnSc1d1	místní
vlna	vlna	k1gFnSc1	vlna
rozhořčení	rozhořčení	k1gNnSc2	rozhořčení
a	a	k8xC	a
historický	historický	k2eAgInSc1d1	historický
Gaslamp	Gaslamp	k1gInSc1	Gaslamp
Quarter	quarter	k1gInSc1	quarter
byl	být	k5eAaImAgInS	být
zachován	zachovat	k5eAaPmNgInS	zachovat
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc2	jeho
jedinečného	jedinečný	k2eAgInSc2d1	jedinečný
charakteru	charakter	k1gInSc2	charakter
využito	využit	k2eAgNnSc1d1	využito
pro	pro	k7c4	pro
přitažení	přitažení	k1gNnSc4	přitažení
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
zde	zde	k6eAd1	zde
najdeme	najít	k5eAaPmIp1nP	najít
bary	bar	k1gInPc1	bar
<g/>
,	,	kIx,	,
kluby	klub	k1gInPc1	klub
<g/>
,	,	kIx,	,
hostely	hostel	k1gInPc1	hostel
<g/>
.	.	kIx.	.
</s>
<s>
Balboa	Balboa	k6eAd1	Balboa
Park	park	k1gInSc1	park
<g/>
:	:	kIx,	:
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
populární	populární	k2eAgInSc1d1	populární
místní	místní	k2eAgInSc1d1	místní
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
pojmenovaný	pojmenovaný	k2eAgInSc1d1	pojmenovaný
po	po	k7c6	po
španělském	španělský	k2eAgInSc6d1	španělský
conquistadorovi	conquistador	k1gMnSc3	conquistador
Vascovi	Vasec	k1gMnSc3	Vasec
de	de	k?	de
Balboa	Balboa	k1gMnSc1	Balboa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
-	-	kIx~	-
1916	[number]	k4	1916
místem	místo	k1gNnSc7	místo
konání	konání	k1gNnSc2	konání
výstavy	výstava	k1gFnSc2	výstava
Panamá-California	Panamá-Californium	k1gNnSc2	Panamá-Californium
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
výstavy	výstava	k1gFnSc2	výstava
Pacific-California	Pacific-Californium	k1gNnSc2	Pacific-Californium
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
těchto	tento	k3xDgFnPc2	tento
výstav	výstava	k1gFnPc2	výstava
zde	zde	k6eAd1	zde
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výstavbě	výstavba	k1gFnSc3	výstavba
mnoha	mnoho	k4c2	mnoho
romantických	romantický	k2eAgFnPc2d1	romantická
budov	budova	k1gFnPc2	budova
v	v	k7c6	v
pseudokoloniálním	pseudokoloniální	k2eAgInSc6d1	pseudokoloniální
španělském	španělský	k2eAgInSc6d1	španělský
stylu	styl	k1gInSc6	styl
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Casa	Casa	k1gMnSc1	Casa
de	de	k?	de
Balboa	Balboa	k1gFnSc1	Balboa
nebo	nebo	k8xC	nebo
Muzeum	muzeum	k1gNnSc1	muzeum
člověka	člověk	k1gMnSc2	člověk
<g/>
)	)	kIx)	)
a	a	k8xC	a
různých	různý	k2eAgInPc6d1	různý
stylech	styl	k1gInPc6	styl
exotických	exotický	k2eAgInPc6d1	exotický
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vliv	vliv	k1gInSc4	vliv
architektury	architektura	k1gFnSc2	architektura
starého	starý	k2eAgNnSc2d1	staré
Mali	Mali	k1gNnSc2	Mali
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgInPc4	tento
domy	dům	k1gInPc4	dům
domovem	domov	k1gInSc7	domov
různých	různý	k2eAgNnPc2d1	různé
muzeí	muzeum	k1gNnPc2	muzeum
(	(	kIx(	(
<g/>
San	San	k1gFnSc1	San
Diego	Diego	k6eAd1	Diego
Museum	museum	k1gNnSc4	museum
of	of	k?	of
Arts	Arts	k1gInSc1	Arts
<g/>
,	,	kIx,	,
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Man	mana	k1gFnPc2	mana
<g/>
,	,	kIx,	,
Timken	Timkna	k1gFnPc2	Timkna
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Arts	Artsa	k1gFnPc2	Artsa
<g/>
,	,	kIx,	,
Natural	Natural	k?	Natural
History	Histor	k1gInPc1	Histor
Museum	museum	k1gNnSc4	museum
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	daleko	k6eNd1	daleko
odsud	odsud	k6eAd1	odsud
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
San	San	k1gMnSc1	San
Diego	Diego	k1gMnSc1	Diego
Zoo	zoo	k1gFnSc1	zoo
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
vůbec	vůbec	k9	vůbec
nejzajímavějších	zajímavý	k2eAgFnPc2d3	nejzajímavější
zoo	zoo	k1gFnPc2	zoo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Starají	starat	k5eAaImIp3nP	starat
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
o	o	k7c4	o
mnoho	mnoho	k4c4	mnoho
unikátních	unikátní	k2eAgInPc2d1	unikátní
druhů	druh	k1gInPc2	druh
(	(	kIx(	(
<g/>
např.	např.	kA	např.
panda	panda	k1gFnSc1	panda
velká	velká	k1gFnSc1	velká
<g/>
)	)	kIx)	)
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
vedení	vedení	k1gNnSc1	vedení
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
velmi	velmi	k6eAd1	velmi
citlivě	citlivě	k6eAd1	citlivě
vytvářet	vytvářet	k5eAaImF	vytvářet
výběhy	výběh	k1gInPc4	výběh
a	a	k8xC	a
interiéry	interiér	k1gInPc4	interiér
<g/>
.	.	kIx.	.
</s>
<s>
Atrakci	atrakce	k1gFnSc4	atrakce
Balboa	Balboum	k1gNnSc2	Balboum
Parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
také	také	k9	také
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
vesnice	vesnice	k1gFnSc1	vesnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mají	mít	k5eAaImIp3nP	mít
svůj	svůj	k3xOyFgInSc4	svůj
domeček	domeček	k1gInSc4	domeček
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc1	všechen
vyspělé	vyspělý	k2eAgInPc1d1	vyspělý
státy	stát	k1gInPc1	stát
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
je	být	k5eAaImIp3nS	být
i	i	k9	i
společný	společný	k2eAgInSc4d1	společný
dům	dům	k1gInSc4	dům
Česka	Česko	k1gNnSc2	Česko
a	a	k8xC	a
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
jsou	být	k5eAaImIp3nP	být
i	i	k8xC	i
ochutnávky	ochutnávka	k1gFnPc1	ochutnávka
tradičních	tradiční	k2eAgFnPc2d1	tradiční
pochoutek	pochoutka	k1gFnPc2	pochoutka
příslušných	příslušný	k2eAgInPc2d1	příslušný
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Dominantou	dominanta	k1gFnSc7	dominanta
Parku	park	k1gInSc2	park
je	být	k5eAaImIp3nS	být
také	také	k9	také
botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
"	"	kIx"	"
<g/>
Balboa	Balboa	k1gFnSc1	Balboa
Park	park	k1gInSc1	park
<g/>
"	"	kIx"	"
jež	jenž	k3xRgFnSc1	jenž
již	již	k9	již
zvenku	zvenku	k6eAd1	zvenku
působí	působit	k5eAaImIp3nS	působit
velice	velice	k6eAd1	velice
impozantně	impozantně	k6eAd1	impozantně
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
pak	pak	k6eAd1	pak
teprve	teprve	k6eAd1	teprve
uvnitř	uvnitř	k6eAd1	uvnitř
<g/>
....	....	k?	....
Celý	celý	k2eAgInSc1d1	celý
park	park	k1gInSc1	park
se	se	k3xPyFc4	se
rozprostírá	rozprostírat	k5eAaImIp3nS	rozprostírat
na	na	k7c6	na
obrovské	obrovský	k2eAgFnSc6d1	obrovská
ploše	plocha	k1gFnSc6	plocha
a	a	k8xC	a
na	na	k7c4	na
prohlídku	prohlídka	k1gFnSc4	prohlídka
parku	park	k1gInSc2	park
nepostačí	postačit	k5eNaPmIp3nS	postačit
jediný	jediný	k2eAgInSc4d1	jediný
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
řada	řada	k1gFnSc1	řada
oblíbených	oblíbený	k2eAgFnPc2d1	oblíbená
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
divadlo	divadlo	k1gNnSc1	divadlo
"	"	kIx"	"
<g/>
Old	Olda	k1gFnPc2	Olda
Globe	globus	k1gInSc5	globus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
název	název	k1gInSc1	název
napovídá	napovídat	k5eAaBmIp3nS	napovídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
repertoáru	repertoár	k1gInSc6	repertoár
nebude	být	k5eNaImBp3nS	být
chybět	chybět	k5eAaImF	chybět
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
<g/>
.	.	kIx.	.
</s>
<s>
Embarcadero	Embarcadero	k1gNnSc1	Embarcadero
<g/>
:	:	kIx,	:
přístav	přístav	k1gInSc1	přístav
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
změněn	změnit	k5eAaPmNgMnS	změnit
v	v	k7c4	v
promenádu	promenáda	k1gFnSc4	promenáda
podél	podél	k7c2	podél
mořského	mořský	k2eAgInSc2d1	mořský
břehu	břeh	k1gInSc2	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Najdeme	najít	k5eAaPmIp1nP	najít
zde	zde	k6eAd1	zde
mořské	mořský	k2eAgNnSc4d1	mořské
muzeum	muzeum	k1gNnSc4	muzeum
a	a	k8xC	a
supermoderní	supermoderní	k2eAgNnSc4d1	supermoderní
Konferenční	konferenční	k2eAgNnSc4d1	konferenční
centrum	centrum	k1gNnSc4	centrum
(	(	kIx(	(
<g/>
Convention	Convention	k1gInSc4	Convention
Center	centrum	k1gNnPc2	centrum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Little	Littlat	k5eAaPmIp3nS	Littlat
Italy	Ital	k1gMnPc4	Ital
<g/>
:	:	kIx,	:
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
osídlena	osídlen	k2eAgFnSc1d1	osídlena
italskými	italský	k2eAgMnPc7d1	italský
rybáři	rybář	k1gMnPc7	rybář
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
středomořský	středomořský	k2eAgInSc4d1	středomořský
charakter	charakter	k1gInSc4	charakter
si	se	k3xPyFc3	se
zachovala	zachovat	k5eAaPmAgFnS	zachovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
mnoha	mnoho	k4c2	mnoho
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
,	,	kIx,	,
barů	bar	k1gInPc2	bar
a	a	k8xC	a
pizzerií	pizzerie	k1gFnPc2	pizzerie
<g/>
.	.	kIx.	.
</s>
<s>
Stará	starý	k2eAgFnSc1d1	stará
architektura	architektura	k1gFnSc1	architektura
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
snoubí	snoubit	k5eAaImIp3nP	snoubit
s	s	k7c7	s
moderní	moderní	k2eAgFnSc7d1	moderní
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Coronado	Coronada	k1gFnSc5	Coronada
Bridge	Bridgus	k1gMnSc5	Bridgus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
obrovsky	obrovsky	k6eAd1	obrovsky
most	most	k1gInSc1	most
nad	nad	k7c7	nad
zálivem	záliv	k1gInSc7	záliv
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
spojuje	spojovat	k5eAaImIp3nS	spojovat
San	San	k1gMnSc1	San
Diego	Diego	k1gMnSc1	Diego
a	a	k8xC	a
Coronado	Coronada	k1gFnSc5	Coronada
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
jen	jen	k9	jen
pro	pro	k7c4	pro
automobilovou	automobilový	k2eAgFnSc4d1	automobilová
dopravu	doprava	k1gFnSc4	doprava
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
osvětlený	osvětlený	k2eAgInSc1d1	osvětlený
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
nádherně	nádherně	k6eAd1	nádherně
<g/>
.	.	kIx.	.
</s>
<s>
Nechybí	chybit	k5eNaPmIp3nS	chybit
ani	ani	k8xC	ani
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
pohlednici	pohlednice	k1gFnSc6	pohlednice
z	z	k7c2	z
města	město	k1gNnSc2	město
San	San	k1gMnSc1	San
Diego	Diego	k1gMnSc1	Diego
<g/>
.	.	kIx.	.
</s>
<s>
Hotel	hotel	k1gInSc1	hotel
"	"	kIx"	"
<g/>
Del	Del	k1gFnSc1	Del
Coronado	Coronada	k1gFnSc5	Coronada
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
<g/>
,	,	kIx,	,
nejslavnějších	slavný	k2eAgInPc2d3	nejslavnější
a	a	k8xC	a
nejmalebnějších	malebný	k2eAgInPc2d3	nejmalebnější
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
a	a	k8xC	a
na	na	k7c6	na
pláži	pláž	k1gFnSc6	pláž
"	"	kIx"	"
<g/>
Coronado	Coronada	k1gFnSc5	Coronada
Beach	Bea	k1gFnPc6	Bea
<g/>
'	'	kIx"	'
se	se	k3xPyFc4	se
natáčel	natáčet	k5eAaImAgInS	natáčet
film	film	k1gInSc1	film
"	"	kIx"	"
<g/>
Někdo	někdo	k3yInSc1	někdo
to	ten	k3xDgNnSc4	ten
rád	rád	k6eAd1	rád
horké	horký	k2eAgNnSc1d1	horké
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Upomínky	upomínka	k1gFnPc4	upomínka
na	na	k7c4	na
Marilyn	Marilyn	k1gFnSc4	Marilyn
Monroe	Monro	k1gInSc2	Monro
nacházíme	nacházet	k5eAaImIp1nP	nacházet
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
hotelu	hotel	k1gInSc2	hotel
stále	stále	k6eAd1	stále
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
1,381,069	[number]	k4	1,381,069
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
58,9	[number]	k4	58,9
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
6,7	[number]	k4	6,7
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,6	[number]	k4	0,6
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
15,9	[number]	k4	15,9
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,5	[number]	k4	0,5
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
12,3	[number]	k4	12,3
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
5,1	[number]	k4	5,1
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
28,8	[number]	k4	28,8
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Veřejnou	veřejný	k2eAgFnSc4d1	veřejná
dopravu	doprava	k1gFnSc4	doprava
využívají	využívat	k5eAaPmIp3nP	využívat
méně	málo	k6eAd2	málo
než	než	k8xS	než
3	[number]	k4	3
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
San	San	k1gMnSc2	San
Diega	Dieg	k1gMnSc2	Dieg
<g/>
.	.	kIx.	.
</s>
<s>
Dopravu	doprava	k1gFnSc4	doprava
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
je	být	k5eAaImIp3nS	být
zajišťována	zajišťovat	k5eAaImNgFnS	zajišťovat
autobusy	autobus	k1gInPc7	autobus
<g/>
,	,	kIx,	,
rychlodrážními	rychlodrážní	k2eAgFnPc7d1	rychlodrážní
tramvajemi	tramvaj	k1gFnPc7	tramvaj
a	a	k8xC	a
příměstskými	příměstský	k2eAgInPc7d1	příměstský
vlaky	vlak	k1gInPc7	vlak
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
zaměstnavatelem	zaměstnavatel	k1gMnSc7	zaměstnavatel
ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
Námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
s	s	k7c7	s
55	[number]	k4	55
300	[number]	k4	300
zaměstnanci	zaměstnanec	k1gMnPc7	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Alcalá	Alcalý	k2eAgFnSc1d1	Alcalá
de	de	k?	de
Henares	Henares	k1gInSc1	Henares
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
Campinas	Campinasa	k1gFnPc2	Campinasa
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc2	Brazílie
Cavite	Cavit	k1gInSc5	Cavit
City	city	k1gNnPc4	city
<g/>
,	,	kIx,	,
Filipíny	Filipíny	k1gFnPc4	Filipíny
Džalálábád	Džalálábáda	k1gFnPc2	Džalálábáda
<g/>
,	,	kIx,	,
Afghánistán	Afghánistán	k1gInSc1	Afghánistán
Edinburgh	Edinburgh	k1gInSc1	Edinburgh
<g/>
,	,	kIx,	,
Skotsko	Skotsko	k1gNnSc1	Skotsko
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
Jeonju	Jeonju	k1gFnSc1	Jeonju
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
Jokohama	Jokohama	k1gFnSc1	Jokohama
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
León	Leóna	k1gFnPc2	Leóna
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
Perth	Pertha	k1gFnPc2	Pertha
<g/>
,	,	kIx,	,
Austrálie	Austrálie	k1gFnSc2	Austrálie
Tema	Tema	k?	Tema
<g/>
,	,	kIx,	,
Ghana	Ghana	k1gFnSc1	Ghana
Tchaj-čung	Tchaj-čung	k1gInSc1	Tchaj-čung
<g/>
,	,	kIx,	,
Tchaj-wan	Tchajan	k1gInSc1	Tchaj-wan
Varšava	Varšava	k1gFnSc1	Varšava
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Vladivostok	Vladivostok	k1gInSc1	Vladivostok
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
Jen-tchaj	Jenchaj	k1gFnSc1	Jen-tchaj
<g/>
,	,	kIx,	,
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
</s>
