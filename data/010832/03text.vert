<p>
<s>
Mikroregion	mikroregion	k1gInSc1	mikroregion
Údlicko	Údlicko	k1gNnSc1	Údlicko
je	být	k5eAaImIp3nS	být
zájmové	zájmový	k2eAgNnSc1d1	zájmové
sdružení	sdružení	k1gNnSc1	sdružení
právnických	právnický	k2eAgFnPc2d1	právnická
osob	osoba	k1gFnPc2	osoba
v	v	k7c6	v
okresu	okres	k1gInSc6	okres
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
sídlem	sídlo	k1gNnSc7	sídlo
jsou	být	k5eAaImIp3nP	být
Údlice	Údlice	k1gInPc1	Údlice
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc7	jeho
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
koordinace	koordinace	k1gFnSc1	koordinace
obecních	obecní	k2eAgInPc2d1	obecní
územních	územní	k2eAgInPc2d1	územní
plánů	plán	k1gInPc2	plán
a	a	k8xC	a
územní	územní	k2eAgNnSc1d1	územní
plánování	plánování	k1gNnSc1	plánování
<g/>
,	,	kIx,	,
slaďování	slaďování	k1gNnSc1	slaďování
zájmů	zájem	k1gInPc2	zájem
a	a	k8xC	a
činností	činnost	k1gFnPc2	činnost
<g/>
,	,	kIx,	,
koordinace	koordinace	k1gFnSc1	koordinace
významných	významný	k2eAgFnPc2d1	významná
investičních	investiční	k2eAgFnPc2d1	investiční
akcí	akce	k1gFnPc2	akce
<g/>
,	,	kIx,	,
vytváření	vytváření	k1gNnSc1	vytváření
<g/>
,	,	kIx,	,
zmnožování	zmnožování	k1gNnSc1	zmnožování
a	a	k8xC	a
samospráva	samospráva	k1gFnSc1	samospráva
společného	společný	k2eAgInSc2d1	společný
majetku	majetek	k1gInSc2	majetek
sdružení	sdružení	k1gNnSc2	sdružení
<g/>
.	.	kIx.	.
</s>
<s>
Sdružuje	sdružovat	k5eAaImIp3nS	sdružovat
celkem	celkem	k6eAd1	celkem
5	[number]	k4	5
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
1	[number]	k4	1
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obce	obec	k1gFnPc1	obec
sdružené	sdružený	k2eAgFnPc1d1	sdružená
v	v	k7c6	v
mikroregionu	mikroregion	k1gInSc6	mikroregion
==	==	k?	==
</s>
</p>
<p>
<s>
Údlice	Údlice	k1gFnSc1	Údlice
</s>
</p>
<p>
<s>
Nezabylice	Nezabylice	k1gFnSc1	Nezabylice
</s>
</p>
<p>
<s>
Bílence	Bílence	k1gFnSc1	Bílence
</s>
</p>
<p>
<s>
Hrušovany	Hrušovany	k1gInPc1	Hrušovany
</s>
</p>
<p>
<s>
Všehrdy	Všehrdy	k6eAd1	Všehrdy
</s>
</p>
<p>
<s>
ZŠ	ZŠ	kA	ZŠ
Údlice	Údlice	k1gFnSc1	Údlice
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Mikroregion	mikroregion	k1gInSc4	mikroregion
Údlicko	Údlicko	k1gNnSc1	Údlicko
na	na	k7c6	na
Regionálním	regionální	k2eAgInSc6d1	regionální
informačním	informační	k2eAgInSc6d1	informační
servisu	servis	k1gInSc6	servis
</s>
</p>
<p>
<s>
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
</s>
</p>
