<p>
<s>
Morče	morče	k1gNnSc1	morče
domácí	domácí	k2eAgMnSc1d1	domácí
je	být	k5eAaImIp3nS	být
domestikovaná	domestikovaný	k2eAgFnSc1d1	domestikovaná
forma	forma	k1gFnSc1	forma
jihoamerického	jihoamerický	k2eAgMnSc2d1	jihoamerický
hlodavce	hlodavec	k1gMnSc2	hlodavec
morčete	morče	k1gNnSc2	morče
divokého	divoký	k2eAgInSc2d1	divoký
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
oblíbené	oblíbený	k2eAgMnPc4d1	oblíbený
domácí	domácí	k2eAgMnPc4d1	domácí
mazlíčky	mazlíček	k1gMnPc4	mazlíček
<g/>
.	.	kIx.	.
</s>
<s>
Morčata	morče	k1gNnPc1	morče
většinou	většinou	k6eAd1	většinou
nekoušou	kousat	k5eNaImIp3nP	kousat
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
vhodná	vhodný	k2eAgNnPc1d1	vhodné
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Morče	morče	k1gNnSc1	morče
domácí	domácí	k2eAgNnSc1d1	domácí
(	(	kIx(	(
<g/>
Cavia	Cavia	k1gFnSc1	Cavia
aperea	aperea	k1gMnSc1	aperea
porcellus	porcellus	k1gMnSc1	porcellus
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
divokého	divoký	k2eAgNnSc2d1	divoké
morčete	morče	k1gNnSc2	morče
(	(	kIx(	(
<g/>
Cavia	Cavia	k1gFnSc1	Cavia
aperea	aperea	k1gMnSc1	aperea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
dávno	dávno	k6eAd1	dávno
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Jižní	jižní	k2eAgFnSc4d1	jižní
Ameriku	Amerika	k1gFnSc4	Amerika
Španělé	Španěl	k1gMnPc1	Španěl
<g/>
,	,	kIx,	,
chovali	chovat	k5eAaImAgMnP	chovat
morčata	morče	k1gNnPc4	morče
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
a	a	k8xC	a
kožešinu	kožešina	k1gFnSc4	kožešina
Močikové	Močik	k1gMnPc1	Močik
<g/>
,	,	kIx,	,
Čimúové	Čimúus	k1gMnPc1	Čimúus
a	a	k8xC	a
Inkové	Ink	k1gMnPc1	Ink
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Peru	Peru	k1gNnSc2	Peru
a	a	k8xC	a
Chile	Chile	k1gNnSc2	Chile
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
chovala	chovat	k5eAaImAgFnS	chovat
také	také	k9	také
jako	jako	k9	jako
mazlíčci	mazlíček	k1gMnPc1	mazlíček
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Morčata	morče	k1gNnPc1	morče
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
také	také	k9	také
jako	jako	k9	jako
obětní	obětní	k2eAgNnPc1d1	obětní
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
,	,	kIx,	,
kněží	kněz	k1gMnPc2	kněz
z	z	k7c2	z
jejich	jejich	k3xOp3gFnPc2	jejich
vnitřností	vnitřnost	k1gFnPc2	vnitřnost
věštili	věštit	k5eAaImAgMnP	věštit
budoucnost	budoucnost	k1gFnSc4	budoucnost
a	a	k8xC	a
ukládali	ukládat	k5eAaImAgMnP	ukládat
je	on	k3xPp3gMnPc4	on
jako	jako	k8xS	jako
obětinu	obětina	k1gFnSc4	obětina
do	do	k7c2	do
hrobů	hrob	k1gInPc2	hrob
zemřelých	zemřelý	k1gMnPc2	zemřelý
<g/>
.	.	kIx.	.
</s>
<s>
Svědectvím	svědectví	k1gNnSc7	svědectví
této	tento	k3xDgFnSc2	tento
skutečnosti	skutečnost	k1gFnSc2	skutečnost
jsou	být	k5eAaImIp3nP	být
mumifikovaná	mumifikovaný	k2eAgNnPc1d1	mumifikované
morčata	morče	k1gNnPc1	morče
nalezená	nalezený	k2eAgNnPc1d1	nalezené
ve	v	k7c6	v
vykopávkách	vykopávka	k1gFnPc6	vykopávka
inckých	incký	k2eAgFnPc2d1	incká
sídlišť	sídliště	k1gNnPc2	sídliště
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
mnohé	mnohý	k2eAgInPc1d1	mnohý
barevné	barevný	k2eAgInPc1d1	barevný
rázy	ráz	k1gInPc1	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
výzkumy	výzkum	k1gInPc1	výzkum
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
černá	černý	k2eAgNnPc4d1	černé
morčata	morče	k1gNnPc4	morče
byla	být	k5eAaImAgFnS	být
(	(	kIx(	(
<g/>
nejspíše	nejspíše	k9	nejspíše
z	z	k7c2	z
pověrčivosti	pověrčivost	k1gFnSc2	pověrčivost
<g/>
)	)	kIx)	)
hned	hned	k6eAd1	hned
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
usmrcována	usmrcovat	k5eAaImNgFnS	usmrcovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kečujštině	kečujština	k1gFnSc6	kečujština
(	(	kIx(	(
<g/>
jazyce	jazyk	k1gInSc6	jazyk
Inků	Ink	k1gMnPc2	Ink
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
morčata	morče	k1gNnPc1	morče
nazývají	nazývat	k5eAaImIp3nP	nazývat
cui	cui	k?	cui
cui	cui	k?	cui
podle	podle	k7c2	podle
typického	typický	k2eAgInSc2d1	typický
hlasového	hlasový	k2eAgInSc2d1	hlasový
projevu	projev	k1gInSc2	projev
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
setkání	setkání	k1gNnSc4	setkání
s	s	k7c7	s
ochočenými	ochočený	k2eAgNnPc7d1	ochočené
morčaty	morče	k1gNnPc7	morče
u	u	k7c2	u
jihoamerických	jihoamerický	k2eAgMnPc2d1	jihoamerický
indiánů	indián	k1gMnPc2	indián
Kryštof	Kryštof	k1gMnSc1	Kryštof
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
a	a	k8xC	a
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
tato	tento	k3xDgNnPc4	tento
zvířata	zvíře	k1gNnPc4	zvíře
popsal	popsat	k5eAaPmAgMnS	popsat
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
o	o	k7c6	o
přírodě	příroda	k1gFnSc6	příroda
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
slavný	slavný	k2eAgMnSc1d1	slavný
renesanční	renesanční	k2eAgMnSc1d1	renesanční
přírodovědec	přírodovědec	k1gMnSc1	přírodovědec
Conrad	Conrada	k1gFnPc2	Conrada
Gessner	Gessnra	k1gFnPc2	Gessnra
<g/>
.	.	kIx.	.
<g/>
Morčata	morče	k1gNnPc1	morče
byla	být	k5eAaImAgNnP	být
dovezena	dovézt	k5eAaPmNgNnP	dovézt
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
kolem	kolem	k7c2	kolem
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
jako	jako	k9	jako
domácí	domácí	k2eAgMnPc1d1	domácí
mazlíčci	mazlíček	k1gMnPc1	mazlíček
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
chovat	chovat	k5eAaImF	chovat
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
si	se	k3xPyFc3	se
je	on	k3xPp3gFnPc4	on
mohli	moct	k5eAaImAgMnP	moct
dovolit	dovolit	k5eAaPmF	dovolit
jen	jen	k9	jen
bohatí	bohatý	k2eAgMnPc1d1	bohatý
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
byly	být	k5eAaImAgInP	být
také	také	k9	také
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pořádány	pořádat	k5eAaImNgInP	pořádat
i	i	k9	i
první	první	k4xOgFnSc2	první
morčecí	morčecí	k2eAgFnSc2d1	morčecí
výstavy	výstava	k1gFnSc2	výstava
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
přišla	přijít	k5eAaPmAgFnS	přijít
vlna	vlna	k1gFnSc1	vlna
obliby	obliba	k1gFnSc2	obliba
morčat	morče	k1gNnPc2	morče
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
španělském	španělský	k2eAgInSc6d1	španělský
vstupu	vstup	k1gInSc6	vstup
do	do	k7c2	do
nového	nový	k2eAgInSc2d1	nový
světa	svět	k1gInSc2	svět
si	se	k3xPyFc3	se
morče	morče	k1gNnSc1	morče
brzy	brzy	k6eAd1	brzy
našlo	najít	k5eAaPmAgNnS	najít
svou	svůj	k3xOyFgFnSc4	svůj
cestu	cesta	k1gFnSc4	cesta
na	na	k7c4	na
evropský	evropský	k2eAgInSc4d1	evropský
kontinent	kontinent	k1gInSc4	kontinent
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
okamžitě	okamžitě	k6eAd1	okamžitě
stalo	stát	k5eAaPmAgNnS	stát
populárním	populární	k2eAgMnSc7d1	populární
jako	jako	k8xS	jako
domácí	domácí	k1gMnSc1	domácí
mazlíček	mazlíček	k1gMnSc1	mazlíček
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
I.	I.	kA	I.
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
morče	morče	k1gNnSc4	morče
jako	jako	k8xC	jako
domácího	domácí	k2eAgMnSc4d1	domácí
mazlíčka	mazlíček	k1gMnSc4	mazlíček
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mohlo	moct	k5eAaImAgNnS	moct
přispět	přispět	k5eAaPmF	přispět
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
popularitě	popularita	k1gFnSc3	popularita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
různých	různý	k2eAgNnPc2d1	různé
plemen	plemeno	k1gNnPc2	plemeno
morčat	morče	k1gNnPc2	morče
<g/>
,	,	kIx,	,
lišících	lišící	k2eAgMnPc2d1	lišící
se	se	k3xPyFc4	se
barvou	barva	k1gFnSc7	barva
<g/>
,	,	kIx,	,
délkou	délka	k1gFnSc7	délka
či	či	k8xC	či
strukturou	struktura	k1gFnSc7	struktura
srsti	srst	k1gFnSc2	srst
<g/>
,	,	kIx,	,
barvou	barva	k1gFnSc7	barva
očí	oko	k1gNnPc2	oko
a	a	k8xC	a
rozetami	rozeta	k1gFnPc7	rozeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
domácího	domácí	k2eAgInSc2d1	domácí
chovu	chov	k1gInSc2	chov
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
morčata	morče	k1gNnPc1	morče
hmotnosti	hmotnost	k1gFnSc2	hmotnost
800	[number]	k4	800
až	až	k9	až
1250	[number]	k4	1250
g	g	kA	g
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
vážit	vážit	k5eAaImF	vážit
až	až	k9	až
1800	[number]	k4	1800
g	g	kA	g
při	při	k7c6	při
délce	délka	k1gFnSc6	délka
těla	tělo	k1gNnSc2	tělo
20	[number]	k4	20
až	až	k9	až
36	[number]	k4	36
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Páření	páření	k1gNnSc1	páření
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
říje	říje	k1gFnSc2	říje
trvající	trvající	k2eAgInSc4d1	trvající
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
březost	březost	k1gFnSc1	březost
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
58	[number]	k4	58
až	až	k9	až
75	[number]	k4	75
dní	den	k1gInPc2	den
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
68	[number]	k4	68
dní	den	k1gInPc2	den
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
vrhu	vrh	k1gInSc6	vrh
(	(	kIx(	(
<g/>
ročně	ročně	k6eAd1	ročně
až	až	k9	až
pět	pět	k4xCc1	pět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
raději	rád	k6eAd2	rád
jen	jen	k9	jen
dva	dva	k4xCgMnPc4	dva
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
rodí	rodit	k5eAaImIp3nS	rodit
jedno	jeden	k4xCgNnSc1	jeden
až	až	k8xS	až
šest	šest	k4xCc1	šest
mláďat	mládě	k1gNnPc2	mládě
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
porodní	porodní	k2eAgFnSc7d1	porodní
váhou	váha	k1gFnSc7	váha
90	[number]	k4	90
g.	g.	k?	g.
Ty	ten	k3xDgFnPc1	ten
samice	samice	k1gFnPc1	samice
odstaví	odstavit	k5eAaPmIp3nP	odstavit
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
4-6	[number]	k4	4-6
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
dospělosti	dospělost	k1gFnSc2	dospělost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
samci	samec	k1gMnPc1	samec
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
osmi	osm	k4xCc2	osm
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
samice	samice	k1gFnPc1	samice
deseti	deset	k4xCc2	deset
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
bývá	bývat	k5eAaImIp3nS	bývat
čtyři	čtyři	k4xCgInPc1	čtyři
až	až	k6eAd1	až
deset	deset	k4xCc1	deset
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k9	až
čtrnácti	čtrnáct	k4xCc2	čtrnáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stavba	stavba	k1gFnSc1	stavba
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
všech	všecek	k3xTgNnPc2	všecek
morčat	morče	k1gNnPc2	morče
stejná	stejný	k2eAgFnSc1d1	stejná
–	–	k?	–
široká	široký	k2eAgFnSc1d1	široká
oblá	oblý	k2eAgFnSc1d1	oblá
hlava	hlava	k1gFnSc1	hlava
<g/>
,	,	kIx,	,
krátký	krátký	k2eAgInSc4d1	krátký
krk	krk	k1gInSc4	krk
<g/>
,	,	kIx,	,
ramena	rameno	k1gNnPc4	rameno
a	a	k8xC	a
předhrudí	předhrudit	k5eAaPmIp3nS	předhrudit
široké	široký	k2eAgNnSc1d1	široké
<g/>
,	,	kIx,	,
ne	ne	k9	ne
moc	moc	k6eAd1	moc
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
široký	široký	k2eAgInSc4d1	široký
hřbet	hřbet	k1gInSc4	hřbet
<g/>
.	.	kIx.	.
</s>
<s>
Zadek	zadek	k1gInSc1	zadek
zaoblený	zaoblený	k2eAgInSc1d1	zaoblený
bez	bez	k7c2	bez
ocasu	ocas	k1gInSc2	ocas
<g/>
,	,	kIx,	,
končetiny	končetina	k1gFnSc2	končetina
krátké	krátká	k1gFnSc2	krátká
rovné	rovný	k2eAgFnSc2d1	rovná
–	–	k?	–
přední	přední	k2eAgFnSc4d1	přední
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
prsty	prst	k1gInPc7	prst
<g/>
,	,	kIx,	,
zadní	zadní	k2eAgFnPc4d1	zadní
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
<g/>
.	.	kIx.	.
</s>
<s>
Prospívající	prospívající	k2eAgMnSc1d1	prospívající
jedinec	jedinec	k1gMnSc1	jedinec
má	mít	k5eAaImIp3nS	mít
celé	celý	k2eAgNnSc4d1	celé
tělo	tělo	k1gNnSc4	tělo
dobře	dobře	k6eAd1	dobře
osvalené	osvalený	k2eAgFnPc1d1	osvalená
<g/>
.	.	kIx.	.
</s>
<s>
Chrup	chrup	k1gInSc1	chrup
morčete	morče	k1gNnSc2	morče
tvoří	tvořit	k5eAaImIp3nS	tvořit
dvacet	dvacet	k4xCc1	dvacet
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc4	čtyři
řezáky	řezák	k1gInPc4	řezák
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc4	čtyři
třenové	třenový	k2eAgInPc4d1	třenový
zuby	zub	k1gInPc4	zub
a	a	k8xC	a
dvanáct	dvanáct	k4xCc4	dvanáct
stoliček	stolička	k1gFnPc2	stolička
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chov	chov	k1gInSc1	chov
morčat	morče	k1gNnPc2	morče
==	==	k?	==
</s>
</p>
<p>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
při	při	k7c6	při
chovu	chov	k1gInSc3	chov
jiných	jiný	k2eAgMnPc2d1	jiný
domácích	domácí	k2eAgMnPc2d1	domácí
mazlíčků	mazlíček	k1gMnPc2	mazlíček
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zajistit	zajistit	k5eAaPmF	zajistit
zvířeti	zvíře	k1gNnSc3	zvíře
vhodné	vhodný	k2eAgFnSc2d1	vhodná
podmínky	podmínka	k1gFnSc2	podmínka
a	a	k8xC	a
věnovat	věnovat	k5eAaPmF	věnovat
mu	on	k3xPp3gMnSc3	on
dostatek	dostatek	k1gInSc4	dostatek
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Morčata	morče	k1gNnPc1	morče
jsou	být	k5eAaImIp3nP	být
zvířata	zvíře	k1gNnPc4	zvíře
společenská	společenský	k2eAgNnPc4d1	společenské
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
samice	samice	k1gFnSc1	samice
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
lze	lze	k6eAd1	lze
chovat	chovat	k5eAaImF	chovat
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Chovat	chovat	k5eAaImF	chovat
dva	dva	k4xCgInPc4	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
samečků	sameček	k1gMnPc2	sameček
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
často	často	k6eAd1	často
kolem	kolem	k7c2	kolem
půl	půl	k1xP	půl
roku	rok	k1gInSc2	rok
věku	věk	k1gInSc2	věk
začínají	začínat	k5eAaImIp3nP	začínat
prát	prát	k5eAaImF	prát
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
když	když	k8xS	když
spolu	spolu	k6eAd1	spolu
žijí	žít	k5eAaImIp3nP	žít
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
jsou	být	k5eAaImIp3nP	být
případy	případ	k1gInPc4	případ
<g/>
,	,	kIx,	,
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
jich	on	k3xPp3gMnPc2	on
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
samečci	sameček	k1gMnPc1	sameček
neperou	prát	k5eNaImIp3nP	prát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vhodné	vhodný	k2eAgNnSc4d1	vhodné
obydlí	obydlí	k1gNnSc4	obydlí
pro	pro	k7c4	pro
jedno	jeden	k4xCgNnSc4	jeden
morče	morče	k1gNnSc4	morče
je	být	k5eAaImIp3nS	být
klec	klec	k1gFnSc1	klec
o	o	k7c6	o
ploše	plocha	k1gFnSc6	plocha
minimálně	minimálně	k6eAd1	minimálně
80	[number]	k4	80
×	×	k?	×
50	[number]	k4	50
cm	cm	kA	cm
a	a	k8xC	a
výšce	výška	k1gFnSc6	výška
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
umístěna	umístit	k5eAaPmNgFnS	umístit
ve	v	k7c6	v
světlém	světlý	k2eAgInSc6d1	světlý
nepřetopeném	přetopený	k2eNgInSc6d1	přetopený
pokoji	pokoj	k1gInSc6	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
dbát	dbát	k5eAaImF	dbát
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
pokoji	pokoj	k1gInSc6	pokoj
nebyl	být	k5eNaImAgInS	být
průvan	průvan	k1gInSc1	průvan
(	(	kIx(	(
<g/>
na	na	k7c4	na
ten	ten	k3xDgInSc4	ten
jsou	být	k5eAaImIp3nP	být
morčata	morče	k1gNnPc1	morče
náchylná	náchylný	k2eAgNnPc1d1	náchylné
<g/>
)	)	kIx)	)
a	a	k8xC	a
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
ani	ani	k8xC	ani
zdroje	zdroj	k1gInPc1	zdroj
hluku	hluk	k1gInSc2	hluk
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
chovatelů	chovatel	k1gMnPc2	chovatel
tento	tento	k3xDgInSc1	tento
faktor	faktor	k1gInSc1	faktor
nezpůsobuje	způsobovat	k5eNaImIp3nS	způsobovat
žádné	žádný	k3yNgFnPc4	žádný
komplikace	komplikace	k1gFnPc4	komplikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Umístění	umístění	k1gNnSc1	umístění
chovu	chov	k1gInSc2	chov
===	===	k?	===
</s>
</p>
<p>
<s>
Klec	klec	k1gFnSc1	klec
–	–	k?	–
asi	asi	k9	asi
nejčastější	častý	k2eAgNnSc4d3	nejčastější
používané	používaný	k2eAgNnSc4d1	používané
obydlí	obydlí	k1gNnSc4	obydlí
pro	pro	k7c4	pro
morčata	morče	k1gNnPc4	morče
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
koupit	koupit	k5eAaPmF	koupit
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
zverimexu	zverimex	k1gInSc6	zverimex
<g/>
,	,	kIx,	,
momentálně	momentálně	k6eAd1	momentálně
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
i	i	k8xC	i
velká	velký	k2eAgFnSc1d1	velká
nabídka	nabídka	k1gFnSc1	nabídka
co	co	k3yRnSc4	co
do	do	k7c2	do
barev	barva	k1gFnPc2	barva
a	a	k8xC	a
velikostí	velikost	k1gFnPc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
výhody	výhoda	k1gFnPc4	výhoda
patří	patřit	k5eAaImIp3nS	patřit
snadná	snadný	k2eAgFnSc1d1	snadná
přenosnost	přenosnost	k1gFnSc1	přenosnost
<g/>
,	,	kIx,	,
vzdušnost	vzdušnost	k1gFnSc1	vzdušnost
a	a	k8xC	a
jednoduché	jednoduchý	k2eAgNnSc1d1	jednoduché
čištění	čištění	k1gNnSc1	čištění
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
cena	cena	k1gFnSc1	cena
<g/>
,	,	kIx,	,
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
průvanu	průvan	k1gInSc2	průvan
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
morčata	morče	k1gNnPc4	morče
je	být	k5eAaImIp3nS	být
průvan	průvan	k1gInSc1	průvan
nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc1	možnost
broušení	broušení	k1gNnSc2	broušení
zubů	zub	k1gInPc2	zub
o	o	k7c4	o
kovové	kovový	k2eAgFnPc4d1	kovová
mříže	mříž	k1gFnPc4	mříž
klece	klec	k1gFnSc2	klec
<g/>
,	,	kIx,	,
špatný	špatný	k2eAgInSc4d1	špatný
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
morčetem	morče	k1gNnSc7	morče
a	a	k8xC	a
také	také	k9	také
občasné	občasný	k2eAgNnSc1d1	občasné
vyhazování	vyhazování	k1gNnSc1	vyhazování
podestýlky	podestýlka	k1gFnSc2	podestýlka
z	z	k7c2	z
klece	klec	k1gFnSc2	klec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plastbox	Plastbox	k1gInSc1	Plastbox
–	–	k?	–
Na	na	k7c4	na
morče	morče	k1gNnSc4	morče
netáhne	táhnout	k5eNaImIp3nS	táhnout
a	a	k8xC	a
nevypadává	vypadávat	k5eNaImIp3nS	vypadávat
ven	ven	k6eAd1	ven
podestýlka	podestýlka	k1gFnSc1	podestýlka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
plastboxu	plastbox	k1gInSc6	plastbox
je	být	k5eAaImIp3nS	být
ztížena	ztížen	k2eAgFnSc1d1	ztížena
ventilace	ventilace	k1gFnSc1	ventilace
(	(	kIx(	(
<g/>
mřížka	mřížka	k1gFnSc1	mřížka
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
svrchu	svrchu	k6eAd1	svrchu
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
častěji	často	k6eAd2	často
čistit	čistit	k5eAaImF	čistit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
nebyla	být	k5eNaImAgFnS	být
příliš	příliš	k6eAd1	příliš
vysoká	vysoký	k2eAgFnSc1d1	vysoká
vlhkost	vlhkost	k1gFnSc1	vlhkost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Terárium	terárium	k1gNnSc1	terárium
–	–	k?	–
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
nejvhodnější	vhodný	k2eAgNnSc1d3	nejvhodnější
<g/>
,	,	kIx,	,
s	s	k7c7	s
morčetem	morče	k1gNnSc7	morče
jste	být	k5eAaImIp2nP	být
mnohem	mnohem	k6eAd1	mnohem
více	hodně	k6eAd2	hodně
v	v	k7c6	v
kontaktu	kontakt	k1gInSc6	kontakt
<g/>
,	,	kIx,	,
morče	morče	k1gNnSc1	morče
je	být	k5eAaImIp3nS	být
prokazatelně	prokazatelně	k6eAd1	prokazatelně
mnohem	mnohem	k6eAd1	mnohem
spokojenější	spokojený	k2eAgMnPc1d2	spokojenější
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doma	doma	k6eAd1	doma
nebo	nebo	k8xC	nebo
na	na	k7c4	na
zakázku	zakázka	k1gFnSc4	zakázka
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
ubikace	ubikace	k1gFnSc1	ubikace
–	–	k?	–
často	často	k6eAd1	často
využívané	využívaný	k2eAgFnPc4d1	využívaná
chovateli	chovatel	k1gMnPc7	chovatel
<g/>
,	,	kIx,	,
ubytování	ubytování	k1gNnSc1	ubytování
obvykle	obvykle	k6eAd1	obvykle
vypadá	vypadat	k5eAaPmIp3nS	vypadat
jako	jako	k9	jako
skříň	skříň	k1gFnSc1	skříň
z	z	k7c2	z
lamina	lamino	k1gNnSc2	lamino
s	s	k7c7	s
šuplíky	šuplíky	k?	šuplíky
z	z	k7c2	z
plexi	plexe	k1gFnSc4	plexe
nebo	nebo	k8xC	nebo
skla	sklo	k1gNnPc4	sklo
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
jen	jen	k9	jen
se	s	k7c7	s
sklem	sklo	k1gNnSc7	sklo
vysokým	vysoký	k2eAgNnSc7d1	vysoké
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
cm	cm	kA	cm
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
stěně	stěna	k1gFnSc6	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
ubikace	ubikace	k1gFnPc1	ubikace
se	se	k3xPyFc4	se
lehce	lehko	k6eAd1	lehko
čistí	čistit	k5eAaImIp3nS	čistit
<g/>
,	,	kIx,	,
s	s	k7c7	s
morčaty	morče	k1gNnPc7	morče
se	se	k3xPyFc4	se
jednoduše	jednoduše	k6eAd1	jednoduše
manipuluje	manipulovat	k5eAaImIp3nS	manipulovat
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
i	i	k9	i
lépe	dobře	k6eAd2	dobře
ochočují	ochočovat	k5eAaImIp3nP	ochočovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
nejsou	být	k5eNaImIp3nP	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
průvanu	průvan	k1gInSc6	průvan
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
toto	tento	k3xDgNnSc1	tento
ubytování	ubytování	k1gNnSc1	ubytování
skýtá	skýtat	k5eAaImIp3nS	skýtat
možnost	možnost	k1gFnSc4	možnost
variací	variace	k1gFnPc2	variace
a	a	k8xC	a
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
vhodně	vhodně	k6eAd1	vhodně
zakomponovat	zakomponovat	k5eAaPmF	zakomponovat
do	do	k7c2	do
vybavení	vybavení	k1gNnSc2	vybavení
bytu	byt	k1gInSc2	byt
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nevýhody	nevýhoda	k1gFnPc4	nevýhoda
patří	patřit	k5eAaImIp3nS	patřit
váha	váha	k1gFnSc1	váha
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yQnSc3	což
se	se	k3xPyFc4	se
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
sestavou	sestava	k1gFnSc7	sestava
špatně	špatně	k6eAd1	špatně
manipuluje	manipulovat	k5eAaImIp3nS	manipulovat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
lze	lze	k6eAd1	lze
částečně	částečně	k6eAd1	částečně
řešit	řešit	k5eAaImF	řešit
např.	např.	kA	např.
kolečky	koleček	k1gInPc7	koleček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Venkovní	venkovní	k2eAgInSc1d1	venkovní
výběh	výběh	k1gInSc1	výběh
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
,	,	kIx,	,
oplocený	oplocený	k2eAgInSc1d1	oplocený
kousek	kousek	k1gInSc1	kousek
travnatého	travnatý	k2eAgInSc2d1	travnatý
plácku	plácek	k1gInSc2	plácek
(	(	kIx(	(
<g/>
chráněný	chráněný	k2eAgMnSc1d1	chráněný
i	i	k8xC	i
shora	shora	k6eAd1	shora
před	před	k7c7	před
útoky	útok	k1gInPc7	útok
dravců	dravec	k1gMnPc2	dravec
<g/>
,	,	kIx,	,
psů	pes	k1gMnPc2	pes
a	a	k8xC	a
koček	kočka	k1gFnPc2	kočka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Morčata	morče	k1gNnPc1	morče
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
stále	stále	k6eAd1	stále
na	na	k7c6	na
čerstvém	čerstvý	k2eAgInSc6d1	čerstvý
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
třeba	třeba	k6eAd1	třeba
dávat	dávat	k5eAaImF	dávat
pozor	pozor	k1gInSc4	pozor
na	na	k7c4	na
přehřátí	přehřátí	k1gNnSc4	přehřátí
od	od	k7c2	od
slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zajistit	zajistit	k5eAaPmF	zajistit
i	i	k9	i
chladnější	chladný	k2eAgNnSc4d2	chladnější
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
morčata	morče	k1gNnPc1	morče
měla	mít	k5eAaImAgNnP	mít
kam	kam	k6eAd1	kam
schovat	schovat	k5eAaPmF	schovat
<g/>
.	.	kIx.	.
<g/>
Jako	jako	k8xS	jako
podestýlku	podestýlka	k1gFnSc4	podestýlka
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
použít	použít	k5eAaPmF	použít
hobliny	hoblina	k1gFnPc4	hoblina
(	(	kIx(	(
<g/>
piliny	pilina	k1gFnPc4	pilina
jsou	být	k5eAaImIp3nP	být
příliš	příliš	k6eAd1	příliš
jemné	jemný	k2eAgFnPc1d1	jemná
<g/>
,	,	kIx,	,
dráždí	dráždit	k5eAaImIp3nS	dráždit
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
vnikají	vnikat	k5eAaImIp3nP	vnikat
do	do	k7c2	do
nozder	nozdra	k1gFnPc2	nozdra
<g/>
;	;	kIx,	;
sláma	sláma	k1gFnSc1	sláma
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
poranění	poranění	k1gNnSc2	poranění
očí	oko	k1gNnPc2	oko
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
morčat	morče	k1gNnPc2	morče
přirozeně	přirozeně	k6eAd1	přirozeně
vystouplé	vystouplý	k2eAgInPc1d1	vystouplý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
možnostmi	možnost	k1gFnPc7	možnost
jsou	být	k5eAaImIp3nP	být
dřevěné	dřevěný	k2eAgFnPc4d1	dřevěná
pelety	peleta	k1gFnPc4	peleta
<g/>
,	,	kIx,	,
dělají	dělat	k5eAaImIp3nP	dělat
se	se	k3xPyFc4	se
i	i	k9	i
slaměné	slaměný	k2eAgInPc1d1	slaměný
či	či	k8xC	či
konopné	konopný	k2eAgInPc1d1	konopný
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
konopných	konopný	k2eAgFnPc2d1	konopná
pelet	peleta	k1gFnPc2	peleta
hrozí	hrozit	k5eAaImIp3nS	hrozit
výskyt	výskyt	k1gInSc1	výskyt
parazitů	parazit	k1gMnPc2	parazit
(	(	kIx(	(
<g/>
lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
odstranit	odstranit	k5eAaPmF	odstranit
vystříkáním	vystříkání	k1gNnSc7	vystříkání
některým	některý	k3yIgFnPc3	některý
z	z	k7c2	z
relevantních	relevantní	k2eAgInPc2d1	relevantní
přípravků	přípravek	k1gInPc2	přípravek
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Arpalitem	Arpalit	k1gInSc7	Arpalit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vhodná	vhodný	k2eAgFnSc1d1	vhodná
je	být	k5eAaImIp3nS	být
také	také	k9	také
kukuřičná	kukuřičný	k2eAgFnSc1d1	kukuřičná
drť	drť	k1gFnSc1	drť
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
moč	moč	k1gFnSc1	moč
vsákne	vsáknout	k5eAaPmIp3nS	vsáknout
a	a	k8xC	a
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
tak	tak	k6eAd1	tak
zápachu	zápach	k1gInSc2	zápach
<g/>
,	,	kIx,	,
někdo	někdo	k3yInSc1	někdo
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
kočkolity	kočkolit	k1gInPc4	kočkolit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hrozí	hrozit	k5eAaImIp3nS	hrozit
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
otravy	otrava	k1gFnSc2	otrava
při	při	k7c6	při
požití	požití	k1gNnSc6	požití
<g/>
.	.	kIx.	.
</s>
<s>
Seno	seno	k1gNnSc1	seno
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
jako	jako	k8xC	jako
potrava	potrava	k1gFnSc1	potrava
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
však	však	k9	však
jako	jako	k9	jako
podestýlka	podestýlka	k1gFnSc1	podestýlka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nasákne	nasáknout	k5eAaPmIp3nS	nasáknout
močí	moč	k1gFnSc7	moč
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
začne	začít	k5eAaPmIp3nS	začít
zapáchat	zapáchat	k5eAaImF	zapáchat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
kontrolovat	kontrolovat	k5eAaImF	kontrolovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
u	u	k7c2	u
morčete	morče	k1gNnSc2	morče
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
dostatečnému	dostatečný	k2eAgNnSc3d1	dostatečné
obrušování	obrušování	k1gNnSc3	obrušování
zubů	zub	k1gInPc2	zub
a	a	k8xC	a
nepřerůstají	přerůstat	k5eNaImIp3nP	přerůstat
<g/>
-li	i	k?	-li
mu	on	k3xPp3gMnSc3	on
drápky	drápek	k1gInPc1	drápek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
že	že	k8xS	že
ano	ano	k9	ano
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
pravidelně	pravidelně	k6eAd1	pravidelně
zastřihávat	zastřihávat	k5eAaImF	zastřihávat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
činnost	činnost	k1gFnSc4	činnost
netroufne	troufnout	k5eNaPmIp3nS	troufnout
sám	sám	k3xTgMnSc1	sám
chovatel	chovatel	k1gMnSc1	chovatel
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
navštívit	navštívit	k5eAaPmF	navštívit
zvěrolékaře	zvěrolékař	k1gMnPc4	zvěrolékař
<g/>
.	.	kIx.	.
<g/>
Každé	každý	k3xTgNnSc1	každý
morče	morče	k1gNnSc1	morče
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
mít	mít	k5eAaImF	mít
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
bydlení	bydlení	k1gNnSc2	bydlení
domeček	domeček	k1gInSc1	domeček
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
koupit	koupit	k5eAaPmF	koupit
ve	v	k7c6	v
zverimexu	zverimex	k1gInSc6	zverimex
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ho	on	k3xPp3gMnSc4	on
vyrobit	vyrobit	k5eAaPmF	vyrobit
i	i	k9	i
doma	doma	k6eAd1	doma
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
domečku	domeček	k1gInSc2	domeček
morče	morče	k1gNnSc1	morče
nemá	mít	k5eNaImIp3nS	mít
soukromí	soukromí	k1gNnSc2	soukromí
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
se	se	k3xPyFc4	se
kam	kam	k6eAd1	kam
schovat	schovat	k5eAaPmF	schovat
<g/>
.	.	kIx.	.
</s>
<s>
Napaječka	Napaječka	k1gFnSc1	Napaječka
nejlépe	dobře	k6eAd3	dobře
s	s	k7c7	s
kuličkovým	kuličkový	k2eAgInSc7d1	kuličkový
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
jesličky	jesličky	k1gFnPc1	jesličky
na	na	k7c4	na
seno	seno	k1gNnSc4	seno
a	a	k8xC	a
trávu	tráva	k1gFnSc4	tráva
<g/>
,	,	kIx,	,
různé	různý	k2eAgFnPc4d1	různá
hračky	hračka	k1gFnPc4	hračka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Morče	morče	k1gNnSc4	morče
je	být	k5eAaImIp3nS	být
koprofág	koprofág	k1gMnSc1	koprofág
<g/>
,	,	kIx,	,
požírá	požírat	k5eAaImIp3nS	požírat
vlastní	vlastní	k2eAgInPc4d1	vlastní
výkaly	výkal	k1gInPc4	výkal
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
jsou	být	k5eAaImIp3nP	být
zdrojem	zdroj	k1gInSc7	zdroj
vitaminů	vitamin	k1gInPc2	vitamin
<g/>
,	,	kIx,	,
vlákniny	vláknina	k1gFnSc2	vláknina
a	a	k8xC	a
symbiotických	symbiotický	k2eAgFnPc2d1	symbiotická
<g/>
,	,	kIx,	,
ve	v	k7c6	v
střevě	střevo	k1gNnSc6	střevo
žijících	žijící	k2eAgFnPc2d1	žijící
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Potrava	potrava	k1gFnSc1	potrava
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
potrava	potrava	k1gFnSc1	potrava
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
pestrá	pestrý	k2eAgFnSc1d1	pestrá
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
domácích	domácí	k2eAgFnPc6d1	domácí
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vhodné	vhodný	k2eAgNnSc1d1	vhodné
je	být	k5eAaImIp3nS	být
krmit	krmit	k5eAaImF	krmit
morče	morče	k1gNnSc4	morče
pravidelně	pravidelně	k6eAd1	pravidelně
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
krmí	krmit	k5eAaImIp3nS	krmit
dvakrát	dvakrát	k6eAd1	dvakrát
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
složkou	složka	k1gFnSc7	složka
potravy	potrava	k1gFnSc2	potrava
je	být	k5eAaImIp3nS	být
seno	seno	k1gNnSc4	seno
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
hlavní	hlavní	k2eAgInSc1d1	hlavní
zdroj	zdroj	k1gInSc1	zdroj
vlákniny	vláknina	k1gFnSc2	vláknina
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
morče	morče	k1gNnSc4	morče
velmi	velmi	k6eAd1	velmi
důležitá	důležitý	k2eAgNnPc1d1	důležité
<g/>
)	)	kIx)	)
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
si	se	k3xPyFc3	se
morče	morče	k1gNnSc4	morče
neustálým	neustálý	k2eAgNnSc7d1	neustálé
přežvykováním	přežvykování	k1gNnSc7	přežvykování
stébel	stéblo	k1gNnPc2	stéblo
obrušuje	obrušovat	k5eAaImIp3nS	obrušovat
stoličky	stolička	k1gFnPc4	stolička
<g/>
.	.	kIx.	.
</s>
<s>
Morče	morče	k1gNnSc4	morče
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
musí	muset	k5eAaImIp3nS	muset
mít	mít	k5eAaImF	mít
přístup	přístup	k1gInSc4	přístup
neustále	neustále	k6eAd1	neustále
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
tuky	tuk	k1gInPc4	tuk
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
krmení	krmení	k1gNnSc6	krmení
senem	seno	k1gNnSc7	seno
musí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
morče	morče	k1gNnSc4	morče
také	také	k9	také
neustálý	neustálý	k2eAgInSc4d1	neustálý
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
pitné	pitný	k2eAgFnSc3d1	pitná
vodě	voda	k1gFnSc3	voda
<g/>
.	.	kIx.	.
</s>
<s>
Seno	seno	k1gNnSc1	seno
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
dostatečně	dostatečně	k6eAd1	dostatečně
hrubé	hrubý	k2eAgNnSc4d1	hrubé
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
sečené	sečený	k2eAgFnSc2d1	sečená
kosou	kosa	k1gFnSc7	kosa
(	(	kIx(	(
<g/>
domácí	domácí	k2eAgFnSc7d1	domácí
sekačkou	sekačka	k1gFnSc7	sekačka
není	být	k5eNaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
moc	moc	k6eAd1	moc
jemné	jemný	k2eAgInPc4d1	jemný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
(	(	kIx(	(
<g/>
suché	suchý	k2eAgFnPc1d1	suchá
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgFnPc1d1	zelená
a	a	k8xC	a
bezprašné	bezprašný	k2eAgFnPc1d1	bezprašná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvhodnější	vhodný	k2eAgFnSc1d3	nejvhodnější
je	být	k5eAaImIp3nS	být
luční	luční	k2eAgNnSc4d1	luční
nebo	nebo	k8xC	nebo
horské	horský	k2eAgNnSc4d1	horské
seno	seno	k1gNnSc4	seno
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
lučních	luční	k2eAgFnPc2d1	luční
trav	tráva	k1gFnPc2	tráva
a	a	k8xC	a
bylin	bylina	k1gFnPc2	bylina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc7d1	další
důležitou	důležitý	k2eAgFnSc7d1	důležitá
základní	základní	k2eAgFnSc7d1	základní
složkou	složka	k1gFnSc7	složka
potravy	potrava	k1gFnSc2	potrava
je	být	k5eAaImIp3nS	být
zelenina	zelenina	k1gFnSc1	zelenina
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
morče	morče	k1gNnSc1	morče
kousek	kousek	k1gInSc4	kousek
čerstvé	čerstvý	k2eAgFnSc2d1	čerstvá
zeleniny	zelenina	k1gFnSc2	zelenina
dostat	dostat	k5eAaPmF	dostat
<g/>
.	.	kIx.	.
</s>
<s>
Čerstvá	čerstvý	k2eAgFnSc1d1	čerstvá
potrava	potrava	k1gFnSc1	potrava
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
podávaná	podávaný	k2eAgFnSc1d1	podávaná
příliš	příliš	k6eAd1	příliš
studená	studený	k2eAgFnSc1d1	studená
rovnou	rovnou	k6eAd1	rovnou
z	z	k7c2	z
ledničky	lednička	k1gFnSc2	lednička
<g/>
.	.	kIx.	.
</s>
<s>
Morčeti	morče	k1gNnSc3	morče
můžete	moct	k5eAaImIp2nP	moct
nabídnout	nabídnout	k5eAaPmF	nabídnout
téměř	téměř	k6eAd1	téměř
jakýkoli	jakýkoli	k3yIgInSc4	jakýkoli
druh	druh	k1gInSc4	druh
zeleniny	zelenina	k1gFnSc2	zelenina
či	či	k8xC	či
ovoce	ovoce	k1gNnSc2	ovoce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
mrkev	mrkev	k1gFnSc1	mrkev
<g/>
,	,	kIx,	,
celer	celer	k1gInSc1	celer
<g/>
,	,	kIx,	,
petržel	petržel	k1gFnSc1	petržel
<g/>
,	,	kIx,	,
papriky	paprika	k1gFnPc1	paprika
všech	všecek	k3xTgFnPc2	všecek
barev	barva	k1gFnPc2	barva
(	(	kIx(	(
<g/>
ne	ne	k9	ne
pálivé	pálivý	k2eAgFnPc1d1	pálivá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
červenou	červený	k2eAgFnSc4d1	červená
řepu	řepa	k1gFnSc4	řepa
<g/>
,	,	kIx,	,
čerstvý	čerstvý	k2eAgInSc1d1	čerstvý
fenykl	fenykl	k1gInSc1	fenykl
<g/>
,	,	kIx,	,
rajče	rajče	k1gNnSc1	rajče
<g/>
,	,	kIx,	,
saláty	salát	k1gInPc1	salát
<g/>
,	,	kIx,	,
mátu	máta	k1gFnSc4	máta
<g/>
,	,	kIx,	,
ovesné	ovesný	k2eAgFnPc4d1	ovesná
vločky	vločka	k1gFnPc4	vločka
<g/>
,	,	kIx,	,
různé	různý	k2eAgFnPc4d1	různá
zeleninové	zeleninový	k2eAgFnPc4d1	zeleninová
natě	nať	k1gFnPc4	nať
<g/>
,	,	kIx,	,
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
salátovou	salátový	k2eAgFnSc4d1	salátová
okurku	okurka	k1gFnSc4	okurka
(	(	kIx(	(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
příliš	příliš	k6eAd1	příliš
vody	voda	k1gFnPc4	voda
a	a	k8xC	a
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
průjem	průjem	k1gInSc1	průjem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
květák	květák	k1gInSc4	květák
<g/>
,	,	kIx,	,
kapustu	kapusta	k1gFnSc4	kapusta
<g/>
,	,	kIx,	,
kedluben	kedluben	k1gInSc4	kedluben
<g/>
,	,	kIx,	,
brokolici	brokolice	k1gFnSc4	brokolice
<g/>
,	,	kIx,	,
zelí	zelí	k1gNnSc4	zelí
(	(	kIx(	(
<g/>
nadýmá	nadýmat	k5eAaPmIp3nS	nadýmat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zátěží	zátěž	k1gFnSc7	zátěž
při	při	k7c6	při
trávení	trávení	k1gNnSc6	trávení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nevhodné	vhodný	k2eNgFnPc1d1	nevhodná
jsou	být	k5eAaImIp3nP	být
brambory	brambora	k1gFnPc1	brambora
<g/>
,	,	kIx,	,
pórek	pórek	k1gInSc1	pórek
<g/>
,	,	kIx,	,
cibule	cibule	k1gFnSc1	cibule
a	a	k8xC	a
česnek	česnek	k1gInSc1	česnek
<g/>
!	!	kIx.	!
</s>
</p>
<p>
<s>
Ovoce	ovoce	k1gNnSc1	ovoce
jen	jen	k9	jen
občas	občas	k6eAd1	občas
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
(	(	kIx(	(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
hodně	hodně	k6eAd1	hodně
cukrů	cukr	k1gInPc2	cukr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
např.	např.	kA	např.
jablko	jablko	k1gNnSc1	jablko
<g/>
,	,	kIx,	,
hruška	hruška	k1gFnSc1	hruška
<g/>
,	,	kIx,	,
hroznové	hroznový	k2eAgNnSc1d1	hroznové
víno	víno	k1gNnSc1	víno
<g/>
,	,	kIx,	,
mandarinka	mandarinka	k1gFnSc1	mandarinka
<g/>
,	,	kIx,	,
meloun	meloun	k1gInSc1	meloun
(	(	kIx(	(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
moc	moc	k1gFnSc1	moc
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ananas	ananas	k1gInSc1	ananas
<g/>
,	,	kIx,	,
jahody	jahoda	k1gFnPc1	jahoda
<g/>
,	,	kIx,	,
třešně	třešeň	k1gFnPc1	třešeň
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Čerstvá	čerstvý	k2eAgFnSc1d1	čerstvá
tráva	tráva	k1gFnSc1	tráva
je	být	k5eAaImIp3nS	být
další	další	k2eAgNnPc4d1	další
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc7d1	důležitá
složkou	složka	k1gFnSc7	složka
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teplém	teplý	k2eAgNnSc6d1	teplé
období	období	k1gNnSc6	období
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgNnP	mít
morčata	morče	k1gNnPc1	morče
dostávat	dostávat	k5eAaImF	dostávat
trávu	tráva	k1gFnSc4	tráva
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
v	v	k7c6	v
přiměřeném	přiměřený	k2eAgNnSc6d1	přiměřené
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nenahrazuje	nahrazovat	k5eNaImIp3nS	nahrazovat
seno	seno	k1gNnSc4	seno
<g/>
!	!	kIx.	!
</s>
<s>
Je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
důležitým	důležitý	k2eAgInSc7d1	důležitý
zdrojem	zdroj	k1gInSc7	zdroj
vitamínů	vitamín	k1gInPc2	vitamín
<g/>
.	.	kIx.	.
</s>
<s>
Trávu	tráva	k1gFnSc4	tráva
nesmíte	smět	k5eNaImIp2nP	smět
podávat	podávat	k5eAaImF	podávat
zapařenou	zapařený	k2eAgFnSc4d1	zapařená
a	a	k8xC	a
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
chemicky	chemicky	k6eAd1	chemicky
ošetřena	ošetřit	k5eAaPmNgFnS	ošetřit
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
radost	radost	k1gFnSc1	radost
jim	on	k3xPp3gMnPc3	on
uděláte	udělat	k5eAaPmIp2nP	udělat
pampeliškovými	pampeliškový	k2eAgInPc7d1	pampeliškový
listy	list	k1gInPc7	list
či	či	k8xC	či
květy	květ	k1gInPc7	květ
sedmikrásek	sedmikráska	k1gFnPc2	sedmikráska
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
můžete	moct	k5eAaImIp2nP	moct
podávat	podávat	k5eAaImF	podávat
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Dávejte	dávat	k5eAaImRp2nP	dávat
však	však	k9	však
pozor	pozor	k1gInSc4	pozor
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
trháte	trhat	k5eAaImIp2nP	trhat
<g/>
!	!	kIx.	!
</s>
<s>
Ne	ne	k9	ne
všechny	všechen	k3xTgFnPc1	všechen
rostliny	rostlina	k1gFnPc1	rostlina
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
morčata	morče	k1gNnPc4	morče
jedlé	jedlý	k2eAgFnPc1d1	jedlá
<g/>
.	.	kIx.	.
</s>
<s>
Nesbírejte	sbírat	k5eNaImRp2nP	sbírat
nic	nic	k6eAd1	nic
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
neznáte	znát	k5eNaImIp2nP	znát
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jedovatých	jedovatý	k2eAgFnPc2d1	jedovatá
rostlin	rostlina	k1gFnPc2	rostlina
si	se	k3xPyFc3	se
jmenujme	jmenovat	k5eAaImRp1nP	jmenovat
např.	např.	kA	např.
pryskyřník	pryskyřník	k1gInSc4	pryskyřník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Morčeti	morče	k1gNnSc3	morče
neustále	neustále	k6eAd1	neustále
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
zuby	zub	k1gInPc1	zub
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mělo	mít	k5eAaImAgNnS	mít
seno	seno	k1gNnSc4	seno
neustále	neustále	k6eAd1	neustále
k	k	k7c3	k
disposici	disposice	k1gFnSc3	disposice
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
podávat	podávat	k5eAaImF	podávat
větvičky	větvička	k1gFnPc4	větvička
ovocných	ovocný	k2eAgInPc2d1	ovocný
stromů	strom	k1gInPc2	strom
na	na	k7c4	na
ohlodávání	ohlodávání	k1gNnSc4	ohlodávání
<g/>
,	,	kIx,	,
sušené	sušený	k2eAgFnPc1d1	sušená
bylinky	bylinka	k1gFnPc1	bylinka
i	i	k9	i
se	s	k7c7	s
stonky	stonek	k1gInPc7	stonek
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
obrušování	obrušování	k1gNnSc2	obrušování
zubů	zub	k1gInPc2	zub
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
kvalitní	kvalitní	k2eAgNnSc4d1	kvalitní
seno	seno	k1gNnSc4	seno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tekutiny	tekutina	k1gFnPc1	tekutina
morčatům	morče	k1gNnPc3	morče
nesmějí	smát	k5eNaImIp3nP	smát
chybět	chybět	k5eAaImF	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
vpraví	vpravit	k5eAaPmIp3nP	vpravit
při	při	k7c6	při
konzumaci	konzumace	k1gFnSc6	konzumace
čerstvé	čerstvý	k2eAgFnSc2d1	čerstvá
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vždy	vždy	k6eAd1	vždy
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc4d1	důležitý
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
pitné	pitný	k2eAgFnSc3d1	pitná
vodě	voda	k1gFnSc3	voda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
mít	mít	k5eAaImF	mít
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
napáječce	napáječka	k1gFnSc6	napáječka
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
misce	miska	k1gFnSc6	miska
dochází	docházet	k5eAaImIp3nS	docházet
často	často	k6eAd1	často
ke	k	k7c3	k
znečištění	znečištění	k1gNnSc3	znečištění
výkaly	výkal	k1gInPc7	výkal
<g/>
.	.	kIx.	.
</s>
<s>
Neměla	mít	k5eNaImAgFnS	mít
by	by	kYmCp3nS	by
být	být	k5eAaImF	být
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
kohoutku	kohoutek	k1gInSc2	kohoutek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
převařená	převařený	k2eAgFnSc1d1	převařená
nebo	nebo	k8xC	nebo
odstátá	odstátý	k2eAgFnSc1d1	odstátá
<g/>
.	.	kIx.	.
</s>
<s>
Příležitostně	příležitostně	k6eAd1	příležitostně
můžete	moct	k5eAaImIp2nP	moct
morčeti	morče	k1gNnSc6	morče
nabídnout	nabídnout	k5eAaPmF	nabídnout
čaj	čaj	k1gInSc1	čaj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Granule	granule	k1gFnPc4	granule
speciální	speciální	k2eAgFnPc4d1	speciální
pro	pro	k7c4	pro
morčata	morče	k1gNnPc4	morče
jsou	být	k5eAaImIp3nP	být
vyráběné	vyráběný	k2eAgInPc1d1	vyráběný
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
morče	morče	k1gNnSc1	morče
mělo	mít	k5eAaImAgNnS	mít
dostatek	dostatek	k1gInSc4	dostatek
vitamínů	vitamín	k1gInPc2	vitamín
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
nejvhodnější	vhodný	k2eAgNnSc1d3	nejvhodnější
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
přidaný	přidaný	k2eAgInSc1d1	přidaný
vitamín	vitamín	k1gInSc1	vitamín
C	C	kA	C
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
si	se	k3xPyFc3	se
morče	morče	k1gNnSc1	morče
neumí	umět	k5eNaImIp3nS	umět
vyrobit	vyrobit	k5eAaPmF	vyrobit
samo	sám	k3xTgNnSc1	sám
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nevhodné	vhodný	k2eNgFnPc1d1	nevhodná
jsou	být	k5eAaImIp3nP	být
směsi	směs	k1gFnPc1	směs
se	s	k7c7	s
zrním	zrní	k1gNnSc7	zrní
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dostání	dostání	k1gNnSc3	dostání
v	v	k7c6	v
Pet-Shopech	Pet-Shop	k1gMnPc6	Pet-Shop
(	(	kIx(	(
<g/>
zverimexech	zverimex	k1gInPc6	zverimex
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
směsi	směs	k1gFnPc1	směs
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
nevhodné	vhodný	k2eNgNnSc4d1	nevhodné
zrní	zrní	k1gNnSc4	zrní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
třeba	třeba	k6eAd1	třeba
i	i	k9	i
slunečnicová	slunečnicový	k2eAgNnPc1d1	slunečnicové
semena	semeno	k1gNnPc1	semeno
<g/>
,	,	kIx,	,
ořechy	ořech	k1gInPc1	ořech
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
nevhodné	vhodný	k2eNgFnPc1d1	nevhodná
složky	složka	k1gFnPc1	složka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
příliš	příliš	k6eAd1	příliš
mnoho	mnoho	k4c4	mnoho
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
až	až	k9	až
k	k	k7c3	k
obezitě	obezita	k1gFnSc3	obezita
morčete	morče	k1gNnSc2	morče
a	a	k8xC	a
trávicím	trávicí	k2eAgInPc3d1	trávicí
problémům	problém	k1gInPc3	problém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejpotřebnějším	potřební	k2eAgInSc7d3	potřební
vitamínem	vitamín	k1gInSc7	vitamín
pro	pro	k7c4	pro
morčata	morče	k1gNnPc4	morče
je	být	k5eAaImIp3nS	být
vitamín	vitamín	k1gInSc1	vitamín
C.	C.	kA	C.
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
člověk	člověk	k1gMnSc1	člověk
si	se	k3xPyFc3	se
jej	on	k3xPp3gNnSc4	on
morče	morče	k1gNnSc4	morče
neumí	umět	k5eNaImIp3nS	umět
samo	sám	k3xTgNnSc1	sám
vyrobit	vyrobit	k5eAaPmF	vyrobit
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jej	on	k3xPp3gMnSc4	on
musí	muset	k5eAaImIp3nS	muset
získávat	získávat	k5eAaImF	získávat
z	z	k7c2	z
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgInPc4d1	nutný
vitamíny	vitamín	k1gInPc4	vitamín
dodávat	dodávat	k5eAaImF	dodávat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
různých	různý	k2eAgFnPc2d1	různá
kapek	kapka	k1gFnPc2	kapka
<g/>
,	,	kIx,	,
prášku	prášek	k1gInSc2	prášek
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
době	doba	k1gFnSc6	doba
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
morče	morče	k1gNnSc1	morče
cestuje	cestovat	k5eAaImIp3nS	cestovat
v	v	k7c6	v
přepravce	přepravka	k1gFnSc6	přepravka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
těsno	těsno	k1gNnSc1	těsno
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nS	by
mít	mít	k5eAaImF	mít
stále	stále	k6eAd1	stále
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
seno	seno	k1gNnSc1	seno
<g/>
,	,	kIx,	,
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
zeleninu	zelenina	k1gFnSc4	zelenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
krmivo	krmivo	k1gNnSc1	krmivo
===	===	k?	===
</s>
</p>
<p>
<s>
Směsi	směs	k1gFnPc1	směs
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
sušené	sušený	k2eAgNnSc4d1	sušené
mléko	mléko	k1gNnSc4	mléko
<g/>
,	,	kIx,	,
živočišné	živočišný	k2eAgFnPc4d1	živočišná
moučky	moučka	k1gFnPc4	moučka
<g/>
,	,	kIx,	,
luštěniny	luštěnina	k1gFnPc4	luštěnina
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
semínek	semínko	k1gNnPc2	semínko
(	(	kIx(	(
<g/>
např.	např.	kA	např.
slunečnice	slunečnice	k1gFnSc1	slunečnice
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pamlsky	pamlska	k1gFnPc1	pamlska
pro	pro	k7c4	pro
hlodavce	hlodavec	k1gMnSc4	hlodavec
obsahující	obsahující	k2eAgNnSc1d1	obsahující
mléko	mléko	k1gNnSc1	mléko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyčinky	tyčinka	k1gFnPc1	tyčinka
lepené	lepený	k2eAgFnPc1d1	lepená
vejci	vejce	k1gNnPc7	vejce
nebo	nebo	k8xC	nebo
medem	med	k1gInSc7	med
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ořechy	ořech	k1gInPc1	ořech
(	(	kIx(	(
<g/>
příliš	příliš	k6eAd1	příliš
tučné	tučný	k2eAgInPc1d1	tučný
<g/>
,	,	kIx,	,
poškozující	poškozující	k2eAgInPc1d1	poškozující
orgány	orgán	k1gInPc1	orgán
<g/>
;	;	kIx,	;
na	na	k7c4	na
obezitu	obezita	k1gFnSc4	obezita
může	moct	k5eAaImIp3nS	moct
morče	morče	k1gNnSc4	morče
i	i	k9	i
zemřít	zemřít	k5eAaPmF	zemřít
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
sušené	sušený	k2eAgNnSc1d1	sušené
pečivo	pečivo	k1gNnSc1	pečivo
(	(	kIx(	(
<g/>
morče	morče	k1gNnSc1	morče
si	se	k3xPyFc3	se
zuby	zub	k1gInPc1	zub
nebrousí	brousit	k5eNaImIp3nP	brousit
(	(	kIx(	(
<g/>
v	v	k7c6	v
puse	pusa	k1gFnSc6	pusa
se	se	k3xPyFc4	se
hned	hned	k6eAd1	hned
rozmočí	rozmočit	k5eAaPmIp3nS	rozmočit
na	na	k7c4	na
kaši	kaše	k1gFnSc4	kaše
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tučné	tučný	k2eAgNnSc4d1	tučné
a	a	k8xC	a
špatně	špatně	k6eAd1	špatně
stravitelné	stravitelný	k2eAgNnSc1d1	stravitelné
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
solné	solný	k2eAgInPc1d1	solný
a	a	k8xC	a
minerální	minerální	k2eAgInPc1d1	minerální
kameny	kámen	k1gInPc1	kámen
(	(	kIx(	(
<g/>
hrozí	hrozit	k5eAaImIp3nS	hrozit
tvorba	tvorba	k1gFnSc1	tvorba
močových	močový	k2eAgInPc2d1	močový
kamenů	kámen	k1gInPc2	kámen
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Komunikace	komunikace	k1gFnSc1	komunikace
==	==	k?	==
</s>
</p>
<p>
<s>
Morčata	morče	k1gNnPc1	morče
žila	žít	k5eAaImAgNnP	žít
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
zvířata	zvíře	k1gNnPc4	zvíře
se	s	k7c7	s
silně	silně	k6eAd1	silně
vyvinutým	vyvinutý	k2eAgInSc7d1	vyvinutý
sociálním	sociální	k2eAgInSc7d1	sociální
smyslem	smysl	k1gInSc7	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Morčata	morče	k1gNnPc1	morče
ovládají	ovládat	k5eAaImIp3nP	ovládat
pestrou	pestrý	k2eAgFnSc4d1	pestrá
škálu	škála	k1gFnSc4	škála
zvuků	zvuk	k1gInPc2	zvuk
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
prostředků	prostředek	k1gInPc2	prostředek
k	k	k7c3	k
vyjádření	vyjádření	k1gNnSc3	vyjádření
svých	svůj	k3xOyFgInPc2	svůj
pocitů	pocit	k1gInPc2	pocit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kvíkání	kvíkání	k1gNnSc1	kvíkání
–	–	k?	–
morče	morče	k1gNnSc1	morče
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
něco	něco	k3yInSc4	něco
chuť	chuť	k1gFnSc1	chuť
<g/>
;	;	kIx,	;
kvíkají	kvíkat	k5eAaImIp3nP	kvíkat
také	také	k9	také
mláďata	mládě	k1gNnPc1	mládě
dožadující	dožadující	k2eAgNnPc1d1	dožadující
se	se	k3xPyFc4	se
matky	matka	k1gFnPc4	matka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bublání	bublání	k1gNnSc1	bublání
–	–	k?	–
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
spokojenost	spokojenost	k1gFnSc1	spokojenost
<g/>
,	,	kIx,	,
klid	klid	k1gInSc1	klid
<g/>
;	;	kIx,	;
bublají	bublat	k5eAaImIp3nP	bublat
i	i	k9	i
samice	samice	k1gFnPc1	samice
v	v	k7c6	v
říji	říje	k1gFnSc6	říje
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cvakání	cvakání	k1gNnSc1	cvakání
zubů	zub	k1gInPc2	zub
-	-	kIx~	-
značí	značit	k5eAaImIp3nS	značit
hrozbu	hrozba	k1gFnSc4	hrozba
<g/>
,	,	kIx,	,
strach	strach	k1gInSc4	strach
<g/>
,	,	kIx,	,
vztek	vztek	k1gInSc4	vztek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zpívání	zpívání	k1gNnSc1	zpívání
–	–	k?	–
je	být	k5eAaImIp3nS	být
ojedinělé	ojedinělý	k2eAgNnSc1d1	ojedinělé
<g/>
,	,	kIx,	,
přesně	přesně	k6eAd1	přesně
se	se	k3xPyFc4	se
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
<g/>
;	;	kIx,	;
existují	existovat	k5eAaImIp3nP	existovat
domněnky	domněnka	k1gFnPc1	domněnka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
signál	signál	k1gInSc1	signál
stresové	stresový	k2eAgFnSc2d1	stresová
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc2d1	sociální
tlak	tlak	k1gInSc4	tlak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Účelové	účelový	k2eAgNnSc1d1	účelové
pískání	pískání	k1gNnSc1	pískání
–	–	k?	–
morčata	morče	k1gNnPc1	morče
se	se	k3xPyFc4	se
dorozumívají	dorozumívat	k5eAaImIp3nP	dorozumívat
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
tak	tak	k6eAd1	tak
jako	jako	k9	jako
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rumba	rumba	k1gFnSc1	rumba
–	–	k?	–
tanec	tanec	k1gInSc1	tanec
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
projevuje	projevovat	k5eAaImIp3nS	projevovat
samec	samec	k1gInSc1	samec
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
samici	samice	k1gFnSc4	samice
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
nějaké	nějaký	k3yIgNnSc4	nějaký
dění	dění	k1gNnSc4	dění
narušující	narušující	k2eAgInSc1d1	narušující
jeho	jeho	k3xOp3gInSc1	jeho
běžný	běžný	k2eAgInSc1d1	běžný
den	den	k1gInSc1	den
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
při	při	k7c6	při
rumbě	rumba	k1gFnSc6	rumba
pomalu	pomalu	k6eAd1	pomalu
<g/>
,	,	kIx,	,
natažen	natažen	k2eAgInSc1d1	natažen
a	a	k8xC	a
vydává	vydávat	k5eAaImIp3nS	vydávat
jakési	jakýsi	k3yIgNnSc1	jakýsi
mručení	mručení	k1gNnSc1	mručení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
zívání-	zívání-	k?	zívání-
výraz	výraz	k1gInSc1	výraz
spokojenosti	spokojenost	k1gFnSc2	spokojenost
či	či	k8xC	či
únavyUši	únavyUše	k1gFnSc4	únavyUše
morčete	morče	k1gNnSc2	morče
dokáží	dokázat	k5eAaPmIp3nP	dokázat
vnímat	vnímat	k5eAaImF	vnímat
ultrazvuk	ultrazvuk	k1gInSc4	ultrazvuk
až	až	k9	až
k	k	k7c3	k
hranici	hranice	k1gFnSc3	hranice
100	[number]	k4	100
kHz	khz	kA	khz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Čistokrevná	čistokrevný	k2eAgNnPc1d1	čistokrevné
morčata	morče	k1gNnPc1	morče
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
státech	stát	k1gInPc6	stát
jsou	být	k5eAaImIp3nP	být
zřízené	zřízený	k2eAgInPc1d1	zřízený
kluby	klub	k1gInPc1	klub
morčat	morče	k1gNnPc2	morče
s	s	k7c7	s
rodokmenem	rodokmen	k1gInSc7	rodokmen
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nP	řídit
určitými	určitý	k2eAgInPc7d1	určitý
standardy	standard	k1gInPc7	standard
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
není	být	k5eNaImIp3nS	být
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Pořádá	pořádat	k5eAaImIp3nS	pořádat
mnoho	mnoho	k4c1	mnoho
výstav	výstava	k1gFnPc2	výstava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
navštívit	navštívit	k5eAaPmF	navštívit
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
a	a	k8xC	a
termíny	termín	k1gInPc1	termín
výstav	výstava	k1gFnPc2	výstava
naleznete	nalézt	k5eAaBmIp2nP	nalézt
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Abyste	aby	kYmCp2nP	aby
mohli	moct	k5eAaImAgMnP	moct
odchovávat	odchovávat	k5eAaImF	odchovávat
morčata	morče	k1gNnPc4	morče
s	s	k7c7	s
průkazem	průkaz	k1gInSc7	průkaz
původu	původ	k1gInSc2	původ
(	(	kIx(	(
<g/>
PP	PP	kA	PP
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
musíte	muset	k5eAaImIp2nP	muset
se	se	k3xPyFc4	se
nejdříve	dříve	k6eAd3	dříve
stát	stát	k5eAaImF	stát
členy	člen	k1gInPc4	člen
Českého	český	k2eAgInSc2d1	český
svazu	svaz	k1gInSc2	svaz
chovatelů	chovatel	k1gMnPc2	chovatel
u	u	k7c2	u
libovolné	libovolný	k2eAgFnSc2d1	libovolná
základní	základní	k2eAgFnSc2d1	základní
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
bydliště	bydliště	k1gNnSc2	bydliště
či	či	k8xC	či
chovatelů	chovatel	k1gMnPc2	chovatel
morčat	morče	k1gNnPc2	morče
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zaregistrovat	zaregistrovat	k5eAaPmF	zaregistrovat
si	se	k3xPyFc3	se
vlastní	vlastní	k2eAgFnSc4d1	vlastní
chovatelskou	chovatelský	k2eAgFnSc4d1	chovatelská
stanici	stanice	k1gFnSc4	stanice
(	(	kIx(	(
<g/>
CHS	CHS	kA	CHS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vystavovaná	vystavovaný	k2eAgNnPc1d1	vystavované
morčata	morče	k1gNnPc1	morče
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
do	do	k7c2	do
tří	tři	k4xCgInPc2	tři
standardů	standard	k1gInPc2	standard
–	–	k?	–
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C.	C.	kA	C.
Morče	morče	k1gNnSc1	morče
lze	lze	k6eAd1	lze
registrovat	registrovat	k5eAaBmF	registrovat
od	od	k7c2	od
500	[number]	k4	500
g	g	kA	g
a	a	k8xC	a
výš	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
na	na	k7c6	na
věku	věk	k1gInSc6	věk
nezáleží	záležet	k5eNaImIp3nS	záležet
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nS	by
ovšem	ovšem	k9	ovšem
mít	mít	k5eAaImF	mít
vyvinuté	vyvinutý	k2eAgInPc4d1	vyvinutý
všechny	všechen	k3xTgInPc4	všechen
znaky	znak	k1gInPc4	znak
svého	svůj	k3xOyFgNnSc2	svůj
plemene	plemeno	k1gNnSc2	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Vystavovat	vystavovat	k5eAaImF	vystavovat
můžete	moct	k5eAaImIp2nP	moct
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
baby	baba	k1gFnSc2	baba
<g/>
,	,	kIx,	,
junior	junior	k1gMnSc1	junior
nebo	nebo	k8xC	nebo
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průkaz	průkaz	k1gInSc1	průkaz
původu	původ	k1gInSc2	původ
se	se	k3xPyFc4	se
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
jedincům	jedinec	k1gMnPc3	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
standardu	standard	k1gInSc2	standard
svého	svůj	k3xOyFgNnSc2	svůj
plemene	plemeno	k1gNnSc2	plemeno
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
absolvuje	absolvovat	k5eAaPmIp3nS	absolvovat
registrace	registrace	k1gFnSc1	registrace
čili	čili	k8xC	čili
uchovnění	uchovnění	k1gNnSc1	uchovnění
<g/>
.	.	kIx.	.
</s>
<s>
Mláďatům	mládě	k1gNnPc3	mládě
se	se	k3xPyFc4	se
vystavuje	vystavovat	k5eAaImIp3nS	vystavovat
tento	tento	k3xDgInSc4	tento
průkaz	průkaz	k1gInSc4	průkaz
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
mají	mít	k5eAaImIp3nP	mít
alespoň	alespoň	k9	alespoň
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
rodičů	rodič	k1gMnPc2	rodič
registrovaného	registrovaný	k2eAgMnSc2d1	registrovaný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Plemena	plemeno	k1gNnSc2	plemeno
morčat	morče	k1gNnPc2	morče
==	==	k?	==
</s>
</p>
<p>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
dělení	dělení	k1gNnSc4	dělení
morčat	morče	k1gNnPc2	morče
podle	podle	k7c2	podle
struktury	struktura	k1gFnSc2	struktura
srsti	srst	k1gFnSc2	srst
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
i	i	k9	i
některá	některý	k3yIgNnPc1	některý
neuznaná	uznaný	k2eNgNnPc1d1	neuznané
plemena	plemeno	k1gNnPc1	plemeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Saténové	saténový	k2eAgNnSc1d1	saténové
morče	morče	k1gNnSc1	morče
===	===	k?	===
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
morče	morče	k1gNnSc1	morče
bylo	být	k5eAaImAgNnS	být
vyšlechtěno	vyšlechtit	k5eAaPmNgNnS	vyšlechtit
v	v	k7c6	v
USA	USA	kA	USA
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
srst	srst	k1gFnSc1	srst
má	mít	k5eAaImIp3nS	mít
saténový	saténový	k2eAgInSc4d1	saténový
lesk	lesk	k1gInSc4	lesk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jemnější	jemný	k2eAgMnSc1d2	jemnější
než	než	k8xS	než
u	u	k7c2	u
běžných	běžný	k2eAgNnPc2d1	běžné
hladkosrstých	hladkosrstý	k2eAgNnPc2d1	hladkosrsté
morčat	morče	k1gNnPc2	morče
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
méně	málo	k6eAd2	málo
podsady	podsada	k1gFnSc2	podsada
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristický	charakteristický	k2eAgMnSc1d1	charakteristický
je	být	k5eAaImIp3nS	být
výrazný	výrazný	k2eAgInSc1d1	výrazný
lesk	lesk	k1gInSc1	lesk
(	(	kIx(	(
<g/>
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
se	se	k3xPyFc4	se
až	až	k9	až
ke	k	k7c3	k
kořínkům	kořínek	k1gInPc3	kořínek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
nejrůznějších	různý	k2eAgFnPc6d3	nejrůznější
barvách	barva	k1gFnPc6	barva
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
červené	červená	k1gFnSc6	červená
<g/>
,	,	kIx,	,
zlaté	zlatý	k2eAgFnPc1d1	zlatá
<g/>
,	,	kIx,	,
krémové	krémový	k2eAgFnPc1d1	krémová
<g/>
,	,	kIx,	,
bílé	bílý	k2eAgFnPc1d1	bílá
a	a	k8xC	a
aguti	aguť	k1gFnPc1	aguť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
barev	barva	k1gFnPc2	barva
zatím	zatím	k6eAd1	zatím
nebylo	být	k5eNaImAgNnS	být
uznáno	uznat	k5eAaPmNgNnS	uznat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
saténových	saténový	k2eAgNnPc2d1	saténové
morčat	morče	k1gNnPc2	morče
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc2	jejich
kříženců	kříženec	k1gMnPc2	kříženec
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
choroba	choroba	k1gFnSc1	choroba
fibrózní	fibrózní	k2eAgFnSc2d1	fibrózní
osteodystrofie	osteodystrofie	k1gFnSc2	osteodystrofie
<g/>
.	.	kIx.	.
</s>
<s>
Spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
poruše	porucha	k1gFnSc6	porucha
metabolismu	metabolismus	k1gInSc2	metabolismus
vápníku	vápník	k1gInSc2	vápník
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nP	docházet
k	k	k7c3	k
vyplavování	vyplavování	k1gNnSc3	vyplavování
vápníku	vápník	k1gInSc2	vápník
z	z	k7c2	z
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
nahrazení	nahrazení	k1gNnSc2	nahrazení
kostní	kostní	k2eAgFnSc2d1	kostní
hmoty	hmota	k1gFnSc2	hmota
měkkou	měkký	k2eAgFnSc7d1	měkká
vazivovou	vazivový	k2eAgFnSc7d1	vazivová
tkání	tkáň	k1gFnSc7	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Měkké	měkký	k2eAgFnPc1d1	měkká
kosti	kost	k1gFnPc1	kost
pak	pak	k6eAd1	pak
působí	působit	k5eAaImIp3nP	působit
morčeti	morče	k1gNnSc6	morče
bolest	bolest	k1gFnSc4	bolest
při	při	k7c6	při
přijímání	přijímání	k1gNnSc6	přijímání
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
bolestivé	bolestivý	k2eAgNnSc4d1	bolestivé
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Diagnózu	diagnóza	k1gFnSc4	diagnóza
lze	lze	k6eAd1	lze
určit	určit	k5eAaPmF	určit
jednoznačně	jednoznačně	k6eAd1	jednoznačně
pouze	pouze	k6eAd1	pouze
rentgenovým	rentgenový	k2eAgInSc7d1	rentgenový
snímkem	snímek	k1gInSc7	snímek
lebky	lebka	k1gFnSc2	lebka
a	a	k8xC	a
dlouhých	dlouhý	k2eAgFnPc2d1	dlouhá
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
nemoc	nemoc	k1gFnSc1	nemoc
je	být	k5eAaImIp3nS	být
léčitelná	léčitelný	k2eAgFnSc1d1	léčitelná
<g/>
,	,	kIx,	,
prognóza	prognóza	k1gFnSc1	prognóza
však	však	k9	však
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
stupni	stupeň	k1gInSc6	stupeň
postižení	postižení	k1gNnSc2	postižení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základní	základní	k2eAgInPc1d1	základní
příznaky	příznak	k1gInPc1	příznak
osteodystrofie	osteodystrofie	k1gFnSc2	osteodystrofie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
pohybem	pohyb	k1gInSc7	pohyb
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
příjmem	příjem	k1gInSc7	příjem
potravy	potrava	k1gFnSc2	potrava
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
postupný	postupný	k2eAgInSc1d1	postupný
úbytek	úbytek	k1gInSc1	úbytek
váhy	váha	k1gFnSc2	váha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Morče	morče	k1gNnSc1	morče
na	na	k7c4	na
maso	maso	k1gNnSc4	maso
==	==	k?	==
</s>
</p>
<p>
<s>
Morče	morče	k1gNnSc1	morče
bylo	být	k5eAaImAgNnS	být
původními	původní	k2eAgMnPc7d1	původní
obyvateli	obyvatel	k1gMnPc7	obyvatel
Ameriky	Amerika	k1gFnSc2	Amerika
chováno	chován	k2eAgNnSc4d1	chováno
na	na	k7c4	na
maso	maso	k1gNnSc4	maso
a	a	k8xC	a
i	i	k9	i
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
maso	maso	k1gNnSc1	maso
morčat	morče	k1gNnPc2	morče
běžnou	běžný	k2eAgFnSc7d1	běžná
součástí	součást	k1gFnSc7	součást
jídelníčku	jídelníček	k1gInSc2	jídelníček
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
mezi	mezi	k7c7	mezi
obyvateli	obyvatel	k1gMnPc7	obyvatel
Peru	Peru	k1gNnSc2	Peru
či	či	k8xC	či
Ekvádoru	Ekvádor	k1gInSc2	Ekvádor
<g/>
.	.	kIx.	.
</s>
<s>
Peruánci	Peruánec	k1gMnPc1	Peruánec
například	například	k6eAd1	například
ročně	ročně	k6eAd1	ročně
zkonzumují	zkonzumovat	k5eAaPmIp3nP	zkonzumovat
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
65	[number]	k4	65
milionů	milion	k4xCgInPc2	milion
morčat	morče	k1gNnPc2	morče
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Morče	morče	k1gNnSc1	morče
jako	jako	k8xC	jako
laboratorní	laboratorní	k2eAgNnSc1d1	laboratorní
zvíře	zvíře	k1gNnSc1	zvíře
==	==	k?	==
</s>
</p>
<p>
<s>
Morče	morče	k1gNnSc1	morče
domácí	domácí	k2eAgMnSc1d1	domácí
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
podle	podle	k7c2	podle
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
207	[number]	k4	207
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
mezi	mezi	k7c7	mezi
druhy	druh	k1gInPc7	druh
laboratorních	laboratorní	k2eAgFnPc2d1	laboratorní
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
laboratorním	laboratorní	k2eAgInPc3d1	laboratorní
pokusům	pokus	k1gInPc3	pokus
ve	v	k7c6	v
farmakologii	farmakologie	k1gFnSc6	farmakologie
<g/>
,	,	kIx,	,
mikrobiologii	mikrobiologie	k1gFnSc6	mikrobiologie
<g/>
,	,	kIx,	,
imunologii	imunologie	k1gFnSc6	imunologie
<g/>
,	,	kIx,	,
parazitologii	parazitologie	k1gFnSc6	parazitologie
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
biotechnologických	biotechnologický	k2eAgInPc6d1	biotechnologický
oborech	obor	k1gInPc6	obor
<g/>
.	.	kIx.	.
</s>
<s>
Chov	chov	k1gInSc1	chov
v	v	k7c6	v
laboratorních	laboratorní	k2eAgFnPc6d1	laboratorní
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
veškeré	veškerý	k3xTgInPc1	veškerý
pokusy	pokus	k1gInPc1	pokus
na	na	k7c6	na
morčatech	morče	k1gNnPc6	morče
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
zákonem	zákon	k1gInSc7	zákon
č.	č.	k?	č.
246	[number]	k4	246
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
zvířat	zvíře	k1gNnPc2	zvíře
proti	proti	k7c3	proti
týrání	týrání	k1gNnSc3	týrání
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
zákona	zákon	k1gInSc2	zákon
77	[number]	k4	77
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
vyhlášky	vyhláška	k1gFnSc2	vyhláška
č.	č.	k?	č.
207	[number]	k4	207
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
Sb	sb	kA	sb
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
souvisejících	související	k2eAgInPc2d1	související
předpisů	předpis	k1gInPc2	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zmiňované	zmiňovaný	k2eAgFnSc6d1	zmiňovaná
vyhlášce	vyhláška	k1gFnSc6	vyhláška
jsou	být	k5eAaImIp3nP	být
stanoveny	stanovit	k5eAaPmNgInP	stanovit
limity	limit	k1gInPc1	limit
pro	pro	k7c4	pro
minimální	minimální	k2eAgFnSc4d1	minimální
chovnou	chovný	k2eAgFnSc4d1	chovná
plochu	plocha	k1gFnSc4	plocha
pro	pro	k7c4	pro
jedno	jeden	k4xCgNnSc4	jeden
zvíře	zvíře	k1gNnSc4	zvíře
během	během	k7c2	během
pokusu	pokus	k1gInSc2	pokus
i	i	k9	i
mimo	mimo	k7c4	mimo
něj	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
chovnou	chovný	k2eAgFnSc4d1	chovná
plochu	plocha	k1gFnSc4	plocha
pro	pro	k7c4	pro
matku	matka	k1gFnSc4	matka
s	s	k7c7	s
mláďaty	mládě	k1gNnPc7	mládě
<g/>
,	,	kIx,	,
limity	limit	k1gInPc7	limit
pro	pro	k7c4	pro
teplotu	teplota	k1gFnSc4	teplota
a	a	k8xC	a
vlhkost	vlhkost	k1gFnSc4	vlhkost
<g/>
,	,	kIx,	,
limity	limit	k1gInPc4	limit
pro	pro	k7c4	pro
navykací	navykací	k2eAgFnSc4d1	navykací
fázi	fáze	k1gFnSc4	fáze
před	před	k7c7	před
samotným	samotný	k2eAgInSc7d1	samotný
experimentem	experiment	k1gInSc7	experiment
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
morče	morče	k1gNnSc4	morče
domácí	domácí	k2eAgMnSc1d1	domácí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Morče	morče	k1gNnSc4	morče
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
morče	morče	k1gNnSc4	morče
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Morče	morče	k1gNnSc1	morče
domácí	domácí	k2eAgNnSc1d1	domácí
-	-	kIx~	-
výživa	výživa	k1gFnSc1	výživa
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnSc1d1	základní
organizace	organizace	k1gFnSc1	organizace
chovatelů	chovatel	k1gMnPc2	chovatel
morčat	morče	k1gNnPc2	morče
a	a	k8xC	a
jiných	jiný	k2eAgMnPc2d1	jiný
drobných	drobný	k2eAgMnPc2d1	drobný
hlodavců	hlodavec	k1gMnPc2	hlodavec
</s>
</p>
