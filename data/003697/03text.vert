<s>
Metro	metro	k1gNnSc1	metro
v	v	k7c6	v
Dubaji	Dubaj	k1gFnSc6	Dubaj
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
م	م	k?	م
د	د	k?	د
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vznikající	vznikající	k2eAgFnSc4d1	vznikající
síť	síť	k1gFnSc4	síť
automatického	automatický	k2eAgNnSc2d1	automatické
metra	metro	k1gNnSc2	metro
v	v	k7c6	v
největším	veliký	k2eAgNnSc6d3	veliký
městě	město	k1gNnSc6	město
Spojených	spojený	k2eAgInPc2d1	spojený
arabských	arabský	k2eAgInPc2d1	arabský
emirátů	emirát	k1gInPc2	emirát
<g/>
,	,	kIx,	,
v	v	k7c6	v
Dubaji	Dubaj	k1gFnSc6	Dubaj
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
úsek	úsek	k1gInSc4	úsek
první	první	k4xOgFnSc2	první
Červené	Červené	k2eAgFnSc2d1	Červené
linky	linka	k1gFnSc2	linka
(	(	kIx(	(
<g/>
Red	Red	k1gMnSc1	Red
Line	linout	k5eAaImIp3nS	linout
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
úsek	úsek	k1gInSc1	úsek
Zelené	Zelené	k2eAgFnSc2d1	Zelené
linky	linka	k1gFnSc2	linka
(	(	kIx(	(
<g/>
Green	Green	k1gInSc1	Green
Line	linout	k5eAaImIp3nS	linout
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
je	být	k5eAaImIp3nS	být
plánována	plánován	k2eAgFnSc1d1	plánována
výstavba	výstavba	k1gFnSc1	výstavba
čtyř	čtyři	k4xCgFnPc2	čtyři
linek	linka	k1gFnPc2	linka
metra	metro	k1gNnSc2	metro
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
délce	délka	k1gFnSc6	délka
318	[number]	k4	318
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historickém	historický	k2eAgNnSc6d1	historické
centru	centrum	k1gNnSc6	centrum
Dubaje	Dubaj	k1gInSc2	Dubaj
je	být	k5eAaImIp3nS	být
metro	metro	k1gNnSc1	metro
vedeno	vést	k5eAaImNgNnS	vést
v	v	k7c6	v
tunelech	tunel	k1gInPc6	tunel
<g/>
,	,	kIx,	,
jinde	jinde	k6eAd1	jinde
po	po	k7c6	po
viaduktech	viadukt	k1gInPc6	viadukt
<g/>
.	.	kIx.	.
</s>
<s>
Provozovatelem	provozovatel	k1gMnSc7	provozovatel
sítě	síť	k1gFnSc2	síť
je	být	k5eAaImIp3nS	být
městský	městský	k2eAgMnSc1d1	městský
dopravce	dopravce	k1gMnSc1	dopravce
RTA	RTA	kA	RTA
(	(	kIx(	(
<g/>
Dubai	Duba	k1gInSc6	Duba
Roads	Roads	k1gInSc4	Roads
&	&	k?	&
Transport	transport	k1gInSc1	transport
Authority	Authorita	k1gFnSc2	Authorita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
plánována	plánovat	k5eAaImNgFnS	plánovat
počátkem	počátkem	k7c2	počátkem
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výběrovém	výběrový	k2eAgNnSc6d1	výběrové
řízení	řízení	k1gNnSc6	řízení
na	na	k7c6	na
naplánovaní	naplánovaný	k2eAgMnPc1d1	naplánovaný
<g/>
,	,	kIx,	,
postavení	postavení	k1gNnSc2	postavení
a	a	k8xC	a
15	[number]	k4	15
<g/>
letou	letý	k2eAgFnSc4d1	letá
údržbu	údržba	k1gFnSc4	údržba
metra	metro	k1gNnSc2	metro
zvítězilo	zvítězit	k5eAaPmAgNnS	zvítězit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2005	[number]	k4	2005
konsorcium	konsorcium	k1gNnSc4	konsorcium
Dubai	Duba	k1gFnSc2	Duba
Rail	Raila	k1gFnPc2	Raila
Link	Linka	k1gFnPc2	Linka
japonských	japonský	k2eAgFnPc2d1	japonská
a	a	k8xC	a
tureckých	turecký	k2eAgFnPc2d1	turecká
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kontrakt	kontrakt	k1gInSc1	kontrakt
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
dvě	dva	k4xCgFnPc4	dva
linky	linka	k1gFnPc4	linka
metra	metro	k1gNnSc2	metro
s	s	k7c7	s
53	[number]	k4	53
stanicemi	stanice	k1gFnPc7	stanice
a	a	k8xC	a
celkovou	celkový	k2eAgFnSc7d1	celková
délkou	délka	k1gFnSc7	délka
69	[number]	k4	69
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgFnPc1d1	stavební
práce	práce	k1gFnPc1	práce
začaly	začít	k5eAaPmAgFnP	začít
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
projekt	projekt	k1gInSc1	projekt
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
skluzu	skluz	k1gInSc2	skluz
a	a	k8xC	a
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2009	[number]	k4	2009
otevřen	otevřít	k5eAaPmNgInS	otevřít
první	první	k4xOgInSc1	první
úsek	úsek	k1gInSc1	úsek
červené	červený	k2eAgFnSc2d1	červená
linky	linka	k1gFnSc2	linka
jen	jen	k9	jen
s	s	k7c7	s
10	[number]	k4	10
stanicemi	stanice	k1gFnPc7	stanice
(	(	kIx(	(
<g/>
dalšími	další	k2eAgFnPc7d1	další
15	[number]	k4	15
metro	metro	k1gNnSc1	metro
jen	jen	k6eAd1	jen
projíždí	projíždět	k5eAaImIp3nS	projíždět
<g/>
)	)	kIx)	)
místo	místo	k7c2	místo
plánovaných	plánovaný	k2eAgNnPc2d1	plánované
29	[number]	k4	29
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
Červené	Červené	k2eAgFnSc2d1	Červené
linky	linka	k1gFnSc2	linka
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
50	[number]	k4	50
km	km	kA	km
a	a	k8xC	a
jízdní	jízdní	k2eAgFnSc1d1	jízdní
doba	doba	k1gFnSc1	doba
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
konečné	konečná	k1gFnSc2	konečná
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
asi	asi	k9	asi
2,5	[number]	k4	2,5
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc3	druhý
<g/>
,	,	kIx,	,
Zelené	Zelené	k2eAgFnSc3d1	Zelené
lince	linka	k1gFnSc3	linka
(	(	kIx(	(
<g/>
20	[number]	k4	20
km	km	kA	km
<g/>
,	,	kIx,	,
22	[number]	k4	22
stanic	stanice	k1gFnPc2	stanice
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
provoz	provoz	k1gInSc1	provoz
zahájen	zahájit	k5eAaPmNgInS	zahájit
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
plánovány	plánovat	k5eAaImNgFnP	plánovat
i	i	k9	i
další	další	k2eAgFnPc1d1	další
linky	linka	k1gFnPc1	linka
<g/>
,	,	kIx,	,
Fialová	Fialová	k1gFnSc1	Fialová
(	(	kIx(	(
<g/>
49	[number]	k4	49
km	km	kA	km
<g/>
,	,	kIx,	,
8	[number]	k4	8
stanic	stanice	k1gFnPc2	stanice
<g/>
)	)	kIx)	)
a	a	k8xC	a
Modrá	modrý	k2eAgFnSc1d1	modrá
(	(	kIx(	(
<g/>
47	[number]	k4	47
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Dubaji	Dubaj	k1gFnSc6	Dubaj
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
rozvojového	rozvojový	k2eAgInSc2d1	rozvojový
plánu	plán	k1gInSc2	plán
RTA	RTA	kA	RTA
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
4	[number]	k4	4
linek	linka	k1gFnPc2	linka
metra	metro	k1gNnSc2	metro
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
délce	délka	k1gFnSc6	délka
318	[number]	k4	318
km	km	kA	km
také	také	k9	také
7	[number]	k4	7
tramvajových	tramvajový	k2eAgFnPc2d1	tramvajová
linek	linka	k1gFnPc2	linka
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
délce	délka	k1gFnSc6	délka
270	[number]	k4	270
km	km	kA	km
<g/>
,	,	kIx,	,
90	[number]	k4	90
tras	trasa	k1gFnPc2	trasa
autobusů	autobus	k1gInPc2	autobus
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
2500	[number]	k4	2500
km	km	kA	km
a	a	k8xC	a
5	[number]	k4	5
nových	nový	k2eAgFnPc2d1	nová
vodních	vodní	k2eAgFnPc2d1	vodní
cest	cesta	k1gFnPc2	cesta
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
210	[number]	k4	210
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Záměrem	záměr	k1gInSc7	záměr
je	být	k5eAaImIp3nS	být
zvýšit	zvýšit	k5eAaPmF	zvýšit
podíl	podíl	k1gInSc4	podíl
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
Dubaji	Dubaj	k1gInSc6	Dubaj
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
z	z	k7c2	z
původních	původní	k2eAgInPc2d1	původní
6	[number]	k4	6
%	%	kIx~	%
na	na	k7c4	na
30	[number]	k4	30
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
až	až	k9	až
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
v	v	k7c4	v
Dubaji	Dubaje	k1gFnSc4	Dubaje
konat	konat	k5eAaImF	konat
59	[number]	k4	59
<g/>
.	.	kIx.	.
kongres	kongres	k1gInSc1	kongres
UITP	UITP	kA	UITP
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
výstavou	výstava	k1gFnSc7	výstava
o	o	k7c6	o
městské	městský	k2eAgFnSc6d1	městská
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Metro	metro	k1gNnSc1	metro
je	být	k5eAaImIp3nS	být
automatické	automatický	k2eAgNnSc1d1	automatické
<g/>
,	,	kIx,	,
soupravy	souprava	k1gFnPc1	souprava
nemají	mít	k5eNaImIp3nP	mít
strojvedoucího	strojvedoucí	k1gMnSc4	strojvedoucí
<g/>
.	.	kIx.	.
</s>
<s>
Napájeny	napájen	k2eAgFnPc1d1	napájena
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
třetí	třetí	k4xOgFnSc2	třetí
kolejnice	kolejnice	k1gFnSc2	kolejnice
<g/>
.	.	kIx.	.
</s>
<s>
Provozovány	provozovat	k5eAaImNgFnP	provozovat
jsou	být	k5eAaImIp3nP	být
pětivozové	pětivozový	k2eAgFnPc1d1	pětivozový
jednotky	jednotka	k1gFnPc1	jednotka
od	od	k7c2	od
japonského	japonský	k2eAgMnSc2d1	japonský
výrobce	výrobce	k1gMnSc2	výrobce
Kinki	Kinki	k1gNnSc2	Kinki
Sharyo	Sharyo	k1gMnSc1	Sharyo
<g/>
,	,	kIx,	,
kterých	který	k3yIgNnPc2	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
sítě	síť	k1gFnSc2	síť
dodáno	dodat	k5eAaPmNgNnS	dodat
87	[number]	k4	87
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
mají	mít	k5eAaImIp3nP	mít
kapacitu	kapacita	k1gFnSc4	kapacita
643	[number]	k4	643
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
tříd	třída	k1gFnPc2	třída
–	–	k?	–
luxusní	luxusní	k2eAgNnSc1d1	luxusní
(	(	kIx(	(
<g/>
Gold	Gold	k1gInSc1	Gold
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ženské	ženská	k1gFnPc1	ženská
<g/>
+	+	kIx~	+
<g/>
dětské	dětský	k2eAgFnPc1d1	dětská
a	a	k8xC	a
ekonomické	ekonomický	k2eAgFnPc1d1	ekonomická
(	(	kIx(	(
<g/>
Silver	Silver	k1gInSc1	Silver
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgNnPc1d1	velké
okna	okno	k1gNnPc1	okno
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
rozhled	rozhled	k1gInSc4	rozhled
do	do	k7c2	do
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Vlaky	vlak	k1gInPc1	vlak
mají	mít	k5eAaImIp3nP	mít
maximální	maximální	k2eAgFnSc4d1	maximální
rychlost	rychlost	k1gFnSc4	rychlost
90	[number]	k4	90
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
Interval	interval	k1gInSc1	interval
na	na	k7c6	na
Červené	Červené	k2eAgFnSc6d1	Červené
lince	linka	k1gFnSc6	linka
je	být	k5eAaImIp3nS	být
desetiminutový	desetiminutový	k2eAgMnSc1d1	desetiminutový
<g/>
,	,	kIx,	,
u	u	k7c2	u
stanic	stanice	k1gFnPc2	stanice
jsou	být	k5eAaImIp3nP	být
terminály	terminál	k1gInPc4	terminál
návazné	návazný	k2eAgFnSc2d1	návazná
autobusové	autobusový	k2eAgFnSc2d1	autobusová
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
parkoviště	parkoviště	k1gNnSc2	parkoviště
P	P	kA	P
<g/>
+	+	kIx~	+
<g/>
R	R	kA	R
a	a	k8xC	a
stanoviště	stanoviště	k1gNnSc2	stanoviště
taxislužby	taxislužba	k1gFnSc2	taxislužba
<g/>
.	.	kIx.	.
</s>
<s>
Dekorativní	dekorativní	k2eAgNnSc1d1	dekorativní
osvětlení	osvětlení	k1gNnSc1	osvětlení
v	v	k7c6	v
přestupní	přestupní	k2eAgFnSc6d1	přestupní
stanici	stanice	k1gFnSc6	stanice
mezi	mezi	k7c7	mezi
zelenou	zelená	k1gFnSc7	zelená
a	a	k8xC	a
červenou	červený	k2eAgFnSc7d1	červená
linkou	linka	k1gFnSc7	linka
Burjuman	Burjuman	k1gMnSc1	Burjuman
a	a	k8xC	a
ve	v	k7c6	v
stanici	stanice	k1gFnSc6	stanice
Al	ala	k1gFnPc2	ala
Rigga	Rigga	k1gFnSc1	Rigga
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
česká	český	k2eAgFnSc1d1	Česká
firma	firma	k1gFnSc1	firma
Lasvit	Lasvit	k1gInSc1	Lasvit
<g/>
.	.	kIx.	.
</s>
<s>
Pasažéři	pasažér	k1gMnPc1	pasažér
si	se	k3xPyFc3	se
mohou	moct	k5eAaImIp3nP	moct
vybrat	vybrat	k5eAaPmF	vybrat
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
třídami	třída	k1gFnPc7	třída
-	-	kIx~	-
zlatá	zlatý	k2eAgFnSc1d1	zlatá
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc4	první
<g/>
)	)	kIx)	)
a	a	k8xC	a
stříbrná	stříbrný	k2eAgFnSc1d1	stříbrná
(	(	kIx(	(
<g/>
druhá	druhý	k4xOgFnSc1	druhý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
metru	metro	k1gNnSc6	metro
je	být	k5eAaImIp3nS	být
vyhrazený	vyhrazený	k2eAgInSc4d1	vyhrazený
také	také	k9	také
vagón	vagón	k1gInSc4	vagón
pro	pro	k7c4	pro
ženy	žena	k1gFnPc4	žena
a	a	k8xC	a
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
stanice	stanice	k1gFnPc1	stanice
i	i	k8xC	i
vozy	vůz	k1gInPc1	vůz
metra	metro	k1gNnSc2	metro
jsou	být	k5eAaImIp3nP	být
klimatizované	klimatizovaný	k2eAgFnPc1d1	klimatizovaná
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
máte	mít	k5eAaImIp2nP	mít
místní	místní	k2eAgNnSc4d1	místní
telefonní	telefonní	k2eAgNnSc4d1	telefonní
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
připojíte	připojit	k5eAaPmIp2nP	připojit
se	se	k3xPyFc4	se
ve	v	k7c6	v
stanicích	stanice	k1gFnPc6	stanice
a	a	k8xC	a
často	často	k6eAd1	často
i	i	k9	i
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
k	k	k7c3	k
Wi-Fi	Wi-F	k1gFnSc3	Wi-F
síti	síť	k1gFnSc3	síť
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Metro	metro	k1gNnSc1	metro
v	v	k7c6	v
Dubaji	Dubaj	k1gFnSc6	Dubaj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Metro	metro	k1gNnSc1	metro
v	v	k7c6	v
Dubaji	Dubaj	k1gFnSc6	Dubaj
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
RTA	RTA	kA	RTA
Portal	Portal	k1gMnSc1	Portal
<g/>
,	,	kIx,	,
Roads	Roads	k1gInSc1	Roads
&	&	k?	&
Transport	transport	k1gInSc1	transport
Authority	Authorita	k1gFnSc2	Authorita
<g/>
,	,	kIx,	,
Government	Government	k1gMnSc1	Government
of	of	k?	of
Dubai	Duba	k1gFnSc2	Duba
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Informace	informace	k1gFnPc1	informace
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
UrbanRail	UrbanRail	k1gInSc1	UrbanRail
<g/>
.	.	kIx.	.
<g/>
net	net	k?	net
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
mapy	mapa	k1gFnSc2	mapa
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
UITP	UITP	kA	UITP
vítá	vítat	k5eAaImIp3nS	vítat
první	první	k4xOgFnSc4	první
linku	linka	k1gFnSc4	linka
metra	metro	k1gNnSc2	metro
v	v	k7c6	v
Dubaji	Dubaj	k1gFnSc6	Dubaj
<g/>
,	,	kIx,	,
místě	místo	k1gNnSc6	místo
příštího	příští	k2eAgInSc2d1	příští
kongresu	kongres	k1gInSc2	kongres
UITP	UITP	kA	UITP
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
BUSportál	BUSportál	k1gInSc1	BUSportál
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
Skúška	Skúška	k1gFnSc1	Skúška
metra	metro	k1gNnSc2	metro
v	v	k7c6	v
Dubaji	Dubaj	k1gFnSc6	Dubaj
<g/>
,	,	kIx,	,
BUSportál	BUSportál	k1gInSc1	BUSportál
<g/>
.	.	kIx.	.
<g/>
sk	sk	k?	sk
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
(	(	kIx(	(
<g/>
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
)	)	kIx)	)
Fantastické	fantastický	k2eAgInPc1d1	fantastický
lustry	lustr	k1gInPc1	lustr
v	v	k7c6	v
unikátním	unikátní	k2eAgNnSc6d1	unikátní
metru	metro	k1gNnSc6	metro
v	v	k7c6	v
Dubaji	Dubaj	k1gFnSc6	Dubaj
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
Češi	Čech	k1gMnPc1	Čech
<g/>
,	,	kIx,	,
iDnes	iDnes	k1gMnSc1	iDnes
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
</s>
