<s>
Visuté	visutý	k2eAgFnPc1d1	visutá
zahrady	zahrada	k1gFnPc1	zahrada
Semiramidiny	Semiramidin	k2eAgFnPc1d1	Semiramidin
(	(	kIx(	(
<g/>
také	také	k6eAd1	také
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xS	jako
Visuté	visutý	k2eAgFnPc1d1	visutá
zahrady	zahrada	k1gFnPc1	zahrada
babylónské	babylónský	k2eAgFnPc1d1	Babylónská
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
zaniklá	zaniklý	k2eAgFnSc1d1	zaniklá
starověká	starověký	k2eAgFnSc1d1	starověká
památka	památka	k1gFnSc1	památka
v	v	k7c6	v
Babylóně	Babylón	k1gInSc6	Babylón
řazená	řazený	k2eAgFnSc1d1	řazená
mezi	mezi	k7c4	mezi
sedm	sedm	k4xCc4	sedm
divů	div	k1gInPc2	div
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Postavit	postavit	k5eAaPmF	postavit
je	být	k5eAaImIp3nS	být
nechala	nechat	k5eAaPmAgFnS	nechat
dle	dle	k7c2	dle
legendy	legenda	k1gFnSc2	legenda
královna	královna	k1gFnSc1	královna
Semiramis	Semiramis	k1gFnSc1	Semiramis
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
je	být	k5eAaImIp3nS	být
však	však	k9	však
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
až	až	k9	až
král	král	k1gMnSc1	král
Nebukadnesar	Nebukadnesar	k1gMnSc1	Nebukadnesar
II	II	kA	II
<g/>
.	.	kIx.	.
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
manželku	manželka	k1gFnSc4	manželka
Amytis	Amytis	k1gFnSc2	Amytis
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
byly	být	k5eAaImAgInP	být
vybudovány	vybudovat	k5eAaPmNgInP	vybudovat
pro	pro	k7c4	pro
královnu	královna	k1gFnSc4	královna
Amytis	Amytis	k1gFnSc2	Amytis
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
v	v	k7c6	v
zelené	zelený	k2eAgFnSc6d1	zelená
Médii	Médie	k1gFnSc6	Médie
a	a	k8xC	a
proto	proto	k8xC	proto
Nabukadnesar	Nabukadnesar	k1gMnSc1	Nabukadnesar
II	II	kA	II
<g/>
.	.	kIx.	.
přikázal	přikázat	k5eAaPmAgInS	přikázat
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
600	[number]	k4	600
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
postavit	postavit	k5eAaPmF	postavit
visuté	visutý	k2eAgFnPc4d1	visutá
zahrady	zahrada	k1gFnPc4	zahrada
na	na	k7c6	na
terasách	terasa	k1gFnPc6	terasa
paláce	palác	k1gInSc2	palác
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
potěšil	potěšit	k5eAaPmAgInS	potěšit
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
jejich	jejich	k3xOp3gFnSc6	jejich
existenci	existence	k1gFnSc6	existence
však	však	k9	však
neexistují	existovat	k5eNaImIp3nP	existovat
nezvratné	zvratný	k2eNgInPc4d1	nezvratný
důkazy	důkaz	k1gInPc4	důkaz
<g/>
.	.	kIx.	.
</s>
<s>
Zahrady	zahrada	k1gFnPc4	zahrada
popisovali	popisovat	k5eAaImAgMnP	popisovat
zejména	zejména	k6eAd1	zejména
starořečtí	starořecký	k2eAgMnPc1d1	starořecký
historikové	historik	k1gMnPc1	historik
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
Strabón	Strabón	k1gInSc1	Strabón
a	a	k8xC	a
Diodóros	Diodórosa	k1gFnPc2	Diodórosa
Sicilský	sicilský	k2eAgMnSc1d1	sicilský
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zmíněných	zmíněný	k2eAgInPc2d1	zmíněný
popisů	popis	k1gInPc2	popis
byly	být	k5eAaImAgFnP	být
zahrady	zahrada	k1gFnPc1	zahrada
zavlažovány	zavlažován	k2eAgInPc4d1	zavlažován
prameny	pramen	k1gInPc4	pramen
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
stékaly	stékat	k5eAaImAgFnP	stékat
dolů	dolů	k6eAd1	dolů
po	po	k7c6	po
hradbách	hradba	k1gFnPc6	hradba
a	a	k8xC	a
zavlažovaly	zavlažovat	k5eAaImAgFnP	zavlažovat
tak	tak	k9	tak
všechny	všechen	k3xTgFnPc1	všechen
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
div	div	k1gInSc1	div
světa	svět	k1gInSc2	svět
–	–	k?	–
okouzlující	okouzlující	k2eAgFnSc2d1	okouzlující
visuté	visutý	k2eAgFnSc2d1	visutá
zahrady	zahrada	k1gFnSc2	zahrada
královny	královna	k1gFnSc2	královna
Semiramis	Semiramis	k1gFnSc1	Semiramis
v	v	k7c6	v
Babylóně	Babylón	k1gInSc6	Babylón
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
svou	svůj	k3xOyFgFnSc7	svůj
krásou	krása	k1gFnSc7	krása
předčily	předčít	k5eAaPmAgInP	předčít
monumentálnost	monumentálnost	k1gFnSc4	monumentálnost
babylónských	babylónský	k2eAgFnPc2d1	Babylónská
hradeb	hradba	k1gFnPc2	hradba
<g/>
,	,	kIx,	,
zikkuratu	zikkurat	k1gInSc2	zikkurat
i	i	k8xC	i
Mardukovy	Mardukův	k2eAgFnSc2d1	Mardukova
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Historik	historik	k1gMnSc1	historik
Diodóros	Diodórosa	k1gFnPc2	Diodórosa
Sicilský	sicilský	k2eAgMnSc1d1	sicilský
(	(	kIx(	(
<g/>
50	[number]	k4	50
<g/>
př	př	kA	př
<g/>
.	.	kIx.	.
<g/>
n.	n.	k?	n.
<g/>
l.	l.	k?	l.
<g/>
)	)	kIx)	)
píše	psát	k5eAaImIp3nS	psát
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Visuté	visutý	k2eAgFnPc1d1	visutá
zahrady	zahrada	k1gFnPc1	zahrada
Semiramidiny	Semiramidin	k2eAgFnPc1d1	Semiramidin
nebyly	být	k5eNaImAgFnP	být
vystavěny	vystavěn	k2eAgFnPc1d1	vystavěna
Semiramis	Semiramis	k1gFnSc1	Semiramis
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
založila	založit	k5eAaPmAgFnS	založit
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pozdějším	pozdní	k2eAgMnSc7d2	pozdější
vládcem	vládce	k1gMnSc7	vládce
nazývaným	nazývaný	k2eAgFnPc3d1	nazývaná
Kýros	Kýros	k1gInSc4	Kýros
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kurtizáně	kurtizána	k1gFnSc3	kurtizána
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
Persie	Persie	k1gFnSc2	Persie
<g/>
.	.	kIx.	.
</s>
<s>
Vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
louky	louka	k1gFnPc4	louka
na	na	k7c6	na
vrcholcích	vrcholek	k1gInPc6	vrcholek
hor	hora	k1gFnPc2	hora
<g/>
,	,	kIx,	,
požadovaných	požadovaný	k2eAgFnPc2d1	požadovaná
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Zhotovovala	zhotovovat	k5eAaImAgFnS	zhotovovat
umělé	umělý	k2eAgFnPc4d1	umělá
výsadby	výsadba	k1gFnPc4	výsadba
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
napodobila	napodobit	k5eAaPmAgFnS	napodobit
Perskou	perský	k2eAgFnSc4d1	perská
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
400	[number]	k4	400
stop	stopa	k1gFnPc2	stopa
čtverečných	čtverečný	k2eAgFnPc2d1	čtverečná
velká	velký	k2eAgFnSc1d1	velká
a	a	k8xC	a
stoupala	stoupat	k5eAaImAgFnS	stoupat
až	až	k9	až
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
hory	hora	k1gFnSc2	hora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
budova	budova	k1gFnSc1	budova
a	a	k8xC	a
otevřené	otevřený	k2eAgFnPc1d1	otevřená
místnosti	místnost	k1gFnPc1	místnost
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
divadla	divadlo	k1gNnSc2	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
schodiště	schodiště	k1gNnSc2	schodiště
byly	být	k5eAaImAgFnP	být
postaveny	postavit	k5eAaPmNgInP	postavit
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
oblouky	oblouk	k1gInPc1	oblouk
<g/>
,	,	kIx,	,
mírně	mírně	k6eAd1	mírně
vystoupavě	vystoupavě	k6eAd1	vystoupavě
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
podepírají	podepírat	k5eAaImIp3nP	podepírat
celou	celý	k2eAgFnSc4d1	celá
zahradu	zahrada	k1gFnSc4	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
oblouk	oblouk	k1gInSc1	oblouk
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
leží	ležet	k5eAaImIp3nS	ležet
celá	celý	k2eAgFnSc1d1	celá
plocha	plocha	k1gFnSc1	plocha
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
50	[number]	k4	50
loktů	loket	k1gInPc2	loket
vysoký	vysoký	k2eAgInSc1d1	vysoký
<g/>
.	.	kIx.	.
</s>
<s>
Zahrada	zahrada	k1gFnSc1	zahrada
samotná	samotný	k2eAgFnSc1d1	samotná
byla	být	k5eAaImAgFnS	být
obklopena	obklopit	k5eAaPmNgFnS	obklopit
cimbuřím	cimbuří	k1gNnSc7	cimbuří
a	a	k8xC	a
valy	val	k1gInPc7	val
<g/>
.	.	kIx.	.
</s>
<s>
Stěny	stěna	k1gFnPc1	stěna
byly	být	k5eAaImAgFnP	být
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
silné	silný	k2eAgFnPc4d1	silná
<g/>
,	,	kIx,	,
postavené	postavený	k2eAgFnPc4d1	postavená
s	s	k7c7	s
nemalou	malý	k2eNgFnSc7d1	nemalá
péčí	péče	k1gFnSc7	péče
a	a	k8xC	a
náklady	náklad	k1gInPc7	náklad
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
silné	silný	k2eAgFnPc1d1	silná
22	[number]	k4	22
stop	stopa	k1gFnPc2	stopa
<g/>
,	,	kIx,	,
a	a	k8xC	a
každý	každý	k3xTgInSc1	každý
pískovcový	pískovcový	k2eAgInSc1d1	pískovcový
kámen	kámen	k1gInSc1	kámen
je	být	k5eAaImIp3nS	být
10	[number]	k4	10
stop	stop	k1gInSc1	stop
široký	široký	k2eAgInSc1d1	široký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
několika	několik	k4yIc6	několik
patrech	patro	k1gNnPc6	patro
této	tento	k3xDgFnSc6	tento
stavby	stavba	k1gFnPc1	stavba
byly	být	k5eAaImAgFnP	být
položeny	položen	k2eAgInPc4d1	položen
trámy	trám	k1gInPc4	trám
a	a	k8xC	a
velké	velký	k2eAgInPc4d1	velký
kameny	kámen	k1gInPc4	kámen
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc1	každý
16	[number]	k4	16
stop	stop	k1gInSc1	stop
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
a	a	k8xC	a
čtyři	čtyři	k4xCgMnPc4	čtyři
široký	široký	k2eAgInSc4d1	široký
<g/>
.	.	kIx.	.
</s>
<s>
Střecha	střecha	k1gFnSc1	střecha
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
vším	všecek	k3xTgNnSc7	všecek
byla	být	k5eAaImAgFnS	být
nejprve	nejprve	k6eAd1	nejprve
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
rákosím	rákosí	k1gNnSc7	rákosí
zalitým	zalitý	k2eAgNnSc7d1	zalité
množstvím	množství	k1gNnSc7	množství
síry	síra	k1gFnSc2	síra
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
asfaltu	asfalt	k1gInSc2	asfalt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
položeny	položen	k2eAgFnPc1d1	položena
dvojitě	dvojitě	k6eAd1	dvojitě
dlaždice	dlaždice	k1gFnPc4	dlaždice
spojené	spojený	k2eAgFnPc4d1	spojená
tvrdou	tvrdý	k2eAgFnSc7d1	tvrdá
a	a	k8xC	a
odolnou	odolný	k2eAgFnSc7d1	odolná
maltou	malta	k1gFnSc7	malta
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
vším	všecek	k3xTgNnSc7	všecek
byly	být	k5eAaImAgInP	být
položeny	položen	k2eAgInPc1d1	položen
pláty	plát	k1gInPc1	plát
olova	olovo	k1gNnSc2	olovo
uložené	uložený	k2eAgNnSc4d1	uložené
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
odváděly	odvádět	k5eAaImAgFnP	odvádět
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
také	také	k9	také
aby	aby	kYmCp3nP	aby
nehnily	hnít	k5eNaImAgInP	hnít
základy	základ	k1gInPc1	základ
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vším	všecek	k3xTgNnSc7	všecek
byla	být	k5eAaImAgFnS	být
položena	položen	k2eAgFnSc1d1	položena
zemina	zemina	k1gFnSc1	zemina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
hloubku	hloubka	k1gFnSc4	hloubka
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
největších	veliký	k2eAgInPc2d3	veliký
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
položena	položit	k5eAaPmNgFnS	položit
půda	půda	k1gFnSc1	půda
rovná	rovný	k2eAgFnSc1d1	rovná
a	a	k8xC	a
hladká	hladký	k2eAgFnSc1d1	hladká
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgNnP	být
osázena	osázet	k5eAaPmNgNnP	osázet
všemi	všecek	k3xTgInPc7	všecek
druhy	druh	k1gInPc4	druh
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jak	jak	k6eAd1	jak
pro	pro	k7c4	pro
krásu	krása	k1gFnSc4	krása
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
pro	pro	k7c4	pro
velikost	velikost	k1gFnSc4	velikost
může	moct	k5eAaImIp3nS	moct
potěšit	potěšit	k5eAaPmF	potěšit
diváky	divák	k1gMnPc4	divák
<g/>
.	.	kIx.	.
</s>
<s>
Oblouky	oblouk	k1gInPc1	oblouk
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
stály	stát	k5eAaImAgInP	stát
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
mnoho	mnoho	k4c4	mnoho
majestátních	majestátní	k2eAgFnPc2d1	majestátní
prostor	prostora	k1gFnPc2	prostora
všeho	všecek	k3xTgInSc2	všecek
druhu	druh	k1gInSc2	druh
a	a	k8xC	a
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
tam	tam	k6eAd1	tam
stroje	stroj	k1gInPc1	stroj
které	který	k3yQgFnPc1	který
přiváděly	přivádět	k5eAaImAgFnP	přivádět
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
vody	voda	k1gFnSc2	voda
z	z	k7c2	z
řeky	řeka	k1gFnSc2	řeka
Eufrat	Eufrat	k1gInSc1	Eufrat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
potrubí	potrubí	k1gNnSc2	potrubí
<g/>
,	,	kIx,	,
tak	tak	k9	tak
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
skryto	skryt	k2eAgNnSc1d1	skryto
před	před	k7c7	před
diváky	divák	k1gMnPc7	divák
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
potrubí	potrubí	k1gNnSc1	potrubí
vodu	voda	k1gFnSc4	voda
vyvádělo	vyvádět	k5eAaImAgNnS	vyvádět
stavbou	stavba	k1gFnSc7	stavba
až	až	k9	až
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
zahrady	zahrada	k1gFnSc2	zahrada
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Toto	tento	k3xDgNnSc4	tento
svědectví	svědectví	k1gNnSc4	svědectví
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
nalezenému	nalezený	k2eAgInSc3d1	nalezený
objektu	objekt	k1gInSc3	objekt
možná	možná	k9	možná
nebylo	být	k5eNaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
přímém	přímý	k2eAgNnSc6d1	přímé
pozorování	pozorování	k1gNnSc6	pozorování
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
bylo	být	k5eAaImAgNnS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
svá	svůj	k3xOyFgNnPc4	svůj
díla	dílo	k1gNnPc4	dílo
použil	použít	k5eAaPmAgMnS	použít
Diodóros	Diodórosa	k1gFnPc2	Diodórosa
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
starších	starý	k2eAgInPc2d2	starší
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Filóna	Filón	k1gMnSc2	Filón
se	se	k3xPyFc4	se
visutými	visutý	k2eAgFnPc7d1	visutá
zahradami	zahrada	k1gFnPc7	zahrada
nazývaly	nazývat	k5eAaImAgFnP	nazývat
takové	takový	k3xDgFnPc1	takový
zahrady	zahrada	k1gFnPc1	zahrada
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
rostliny	rostlina	k1gFnPc1	rostlina
se	se	k3xPyFc4	se
zdvihaly	zdvihat	k5eAaImAgFnP	zdvihat
vysoko	vysoko	k6eAd1	vysoko
do	do	k7c2	do
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Nacházely	nacházet	k5eAaImAgInP	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
kamenné	kamenný	k2eAgInPc1d1	kamenný
sloupy	sloup	k1gInPc1	sloup
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nesly	nést	k5eAaImAgInP	nést
celou	celý	k2eAgFnSc4d1	celá
tíhu	tíha	k1gFnSc4	tíha
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
zavodňovací	zavodňovací	k2eAgInPc1d1	zavodňovací
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
palmové	palmový	k2eAgInPc1d1	palmový
trámy	trám	k1gInPc1	trám
a	a	k8xC	a
především	především	k6eAd1	především
nejrůznější	různý	k2eAgNnSc4d3	nejrůznější
rostlinstvo	rostlinstvo	k1gNnSc4	rostlinstvo
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
krása	krása	k1gFnSc1	krása
předčila	předčit	k5eAaBmAgFnS	předčit
všechny	všechen	k3xTgFnPc4	všechen
známé	známý	k2eAgFnPc4d1	známá
stavby	stavba	k1gFnPc4	stavba
Babylónu	babylón	k1gInSc2	babylón
a	a	k8xC	a
právem	právem	k6eAd1	právem
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
Strabónem	Strabón	k1gInSc7	Strabón
popisována	popisovat	k5eAaImNgFnS	popisovat
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
divů	div	k1gInPc2	div
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
ji	on	k3xPp3gFnSc4	on
popisoval	popisovat	k5eAaImAgMnS	popisovat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
,,	,,	k?	,,
<g/>
Zahrada	zahrada	k1gFnSc1	zahrada
tvoří	tvořit	k5eAaImIp3nS	tvořit
čtverec	čtverec	k1gInSc1	čtverec
<g/>
,	,	kIx,	,
drží	držet	k5eAaImIp3nS	držet
na	na	k7c6	na
klenbách	klenba	k1gFnPc6	klenba
spočívajících	spočívající	k2eAgFnPc2d1	spočívající
z	z	k7c2	z
kvádrů	kvádr	k1gInPc2	kvádr
<g/>
,	,	kIx,	,
postavených	postavený	k2eAgFnPc2d1	postavená
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
jako	jako	k9	jako
kostky	kostka	k1gFnPc1	kostka
<g/>
.	.	kIx.	.
</s>
<s>
Podstavce	podstavec	k1gInPc1	podstavec
jsou	být	k5eAaImIp3nP	být
vyplněny	vyplnit	k5eAaPmNgInP	vyplnit
hlínou	hlína	k1gFnSc7	hlína
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
mohou	moct	k5eAaImIp3nP	moct
růst	růst	k5eAaImF	růst
i	i	k9	i
největší	veliký	k2eAgInPc4d3	veliký
stromy	strom	k1gInPc4	strom
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
plošina	plošina	k1gFnSc1	plošina
má	mít	k5eAaImIp3nS	mít
stupňovité	stupňovitý	k2eAgInPc4d1	stupňovitý
terasy	teras	k1gInPc4	teras
a	a	k8xC	a
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
jsou	být	k5eAaImIp3nP	být
spirálová	spirálový	k2eAgNnPc4d1	spirálové
čerpadla	čerpadlo	k1gNnPc4	čerpadlo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
čerpají	čerpat	k5eAaImIp3nP	čerpat
vodu	voda	k1gFnSc4	voda
z	z	k7c2	z
Eufratu	Eufrat	k1gInSc2	Eufrat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Nejen	nejen	k6eAd1	nejen
pro	pro	k7c4	pro
krásu	krása	k1gFnSc4	krása
však	však	k9	však
byly	být	k5eAaImAgFnP	být
visuté	visutý	k2eAgFnPc1d1	visutá
zahrady	zahrada	k1gFnPc1	zahrada
Semiramidiny	Semiramidin	k2eAgFnPc1d1	Semiramidin
označeny	označit	k5eAaPmNgInP	označit
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
divů	div	k1gInPc2	div
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zasloužila	zasloužit	k5eAaPmAgFnS	zasloužit
se	se	k3xPyFc4	se
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
také	také	k9	také
originalita	originalita	k1gFnSc1	originalita
myšlenky	myšlenka	k1gFnSc2	myšlenka
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
důmyslnost	důmyslnost	k1gFnSc1	důmyslnost
čerpacího	čerpací	k2eAgNnSc2d1	čerpací
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
už	už	k6eAd1	už
dnes	dnes	k6eAd1	dnes
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
skvost	skvost	k1gInSc4	skvost
nemůžeme	moct	k5eNaImIp1nP	moct
podívat	podívat	k5eAaImF	podívat
<g/>
,	,	kIx,	,
zůstaly	zůstat	k5eAaPmAgInP	zůstat
nám	my	k3xPp1nPc3	my
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
novodobých	novodobý	k2eAgMnPc2d1	novodobý
odborníků	odborník	k1gMnPc2	odborník
a	a	k8xC	a
malířů	malíř	k1gMnPc2	malíř
<g/>
.	.	kIx.	.
</s>
<s>
Archeolog	archeolog	k1gMnSc1	archeolog
Robert	Robert	k1gMnSc1	Robert
Koldewey	Koldewea	k1gFnSc2	Koldewea
<g/>
,	,	kIx,	,
objevitel	objevitel	k1gMnSc1	objevitel
Babylonu	babylon	k1gInSc2	babylon
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
vykopal	vykopat	k5eAaPmAgInS	vykopat
zbytky	zbytek	k1gInPc4	zbytek
kamenné	kamenný	k2eAgFnSc2d1	kamenná
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
mnohdy	mnohdy	k6eAd1	mnohdy
s	s	k7c7	s
visutými	visutý	k2eAgFnPc7d1	visutá
zahradami	zahrada	k1gFnPc7	zahrada
ztotožňována	ztotožňovat	k5eAaImNgFnS	ztotožňovat
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
pověstem	pověst	k1gFnPc3	pověst
o	o	k7c6	o
Semiramis	Semiramis	k1gFnSc1	Semiramis
tedy	tedy	k8xC	tedy
byly	být	k5eAaImAgFnP	být
i	i	k9	i
zmíněné	zmíněný	k2eAgFnPc4d1	zmíněná
visuté	visutý	k2eAgFnPc4d1	visutá
zahrady	zahrada	k1gFnPc4	zahrada
považovány	považován	k2eAgFnPc4d1	považována
za	za	k7c4	za
pohádku	pohádka	k1gFnSc4	pohádka
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
Robert	Robert	k1gMnSc1	Robert
Koldewey	Koldewea	k1gFnSc2	Koldewea
je	on	k3xPp3gNnSc4	on
našel	najít	k5eAaPmAgMnS	najít
<g/>
!	!	kIx.	!
</s>
<s>
Na	na	k7c6	na
pahorku	pahorek	k1gInSc6	pahorek
Karsu	Kars	k1gInSc2	Kars
našel	najít	k5eAaPmAgMnS	najít
jednoho	jeden	k4xCgInSc2	jeden
dne	den	k1gInSc2	den
podivné	podivný	k2eAgFnSc2d1	podivná
klenby	klenba	k1gFnSc2	klenba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
nebylo	být	k5eNaImAgNnS	být
nic	nic	k3yNnSc1	nic
neobyčejného	obyčejný	k2eNgNnSc2d1	neobyčejné
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
první	první	k4xOgFnSc2	první
klenby	klenba	k1gFnSc2	klenba
nalezené	nalezený	k2eAgFnSc2d1	nalezená
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
<g/>
.	.	kIx.	.
</s>
<s>
Přikázal	přikázat	k5eAaPmAgMnS	přikázat
kopat	kopat	k5eAaImF	kopat
dělníkům	dělník	k1gMnPc3	dělník
dál	daleko	k6eAd2	daleko
a	a	k8xC	a
objevoval	objevovat	k5eAaImAgInS	objevovat
více	hodně	k6eAd2	hodně
zajímavých	zajímavý	k2eAgFnPc2d1	zajímavá
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
sloupy	sloup	k1gInPc1	sloup
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
–	–	k?	–
kámen	kámen	k1gInSc1	kámen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvzácnějších	vzácný	k2eAgInPc2d3	nejvzácnější
stavebních	stavební	k2eAgInPc2d1	stavební
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
studnu	studna	k1gFnSc4	studna
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yRgFnPc4	který
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
mohl	moct	k5eAaImAgMnS	moct
domnívat	domnívat	k5eAaImF	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
věží	věž	k1gFnSc7	věž
nějaké	nějaký	k3yIgFnSc2	nějaký
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
vykopávkách	vykopávka	k1gFnPc6	vykopávka
se	se	k3xPyFc4	se
prokázalo	prokázat	k5eAaPmAgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
opravdu	opravdu	k6eAd1	opravdu
o	o	k7c6	o
studni	studna	k1gFnSc6	studna
s	s	k7c7	s
trojitou	trojitý	k2eAgFnSc7d1	trojitá
spirálovitou	spirálovitý	k2eAgFnSc7d1	spirálovitá
šachtou	šachta	k1gFnSc7	šachta
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
byl	být	k5eAaImAgMnS	být
Koldewey	Koldewea	k1gFnPc4	Koldewea
i	i	k9	i
znalcem	znalec	k1gMnSc7	znalec
kanalizací	kanalizace	k1gFnPc2	kanalizace
<g/>
,	,	kIx,	,
okamžitě	okamžitě	k6eAd1	okamžitě
ho	on	k3xPp3gNnSc4	on
napadla	napadnout	k5eAaPmAgFnS	napadnout
bláznivá	bláznivý	k2eAgFnSc1d1	bláznivá
myšlenka	myšlenka	k1gFnSc1	myšlenka
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
proto	proto	k8xC	proto
hledat	hledat	k5eAaImF	hledat
informace	informace	k1gFnPc4	informace
v	v	k7c6	v
dílech	dílo	k1gNnPc6	dílo
Diodóra	Diodóra	k1gFnSc1	Diodóra
<g/>
,	,	kIx,	,
Strabóna	Strabóna	k1gFnSc1	Strabóna
a	a	k8xC	a
Béróssa	Béróssa	k1gFnSc1	Béróssa
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
kamene	kámen	k1gInSc2	kámen
se	se	k3xPyFc4	se
použilo	použít	k5eAaPmAgNnS	použít
při	při	k7c6	při
stavbách	stavba	k1gFnPc6	stavba
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
Babylónu	Babylón	k1gInSc6	Babylón
jen	jen	k9	jen
dvakrát	dvakrát	k6eAd1	dvakrát
–	–	k?	–
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
straně	strana	k1gFnSc6	strana
Kasru	Kasr	k1gInSc2	Kasr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
už	už	k6eAd1	už
ho	on	k3xPp3gMnSc4	on
objevil	objevit	k5eAaPmAgInS	objevit
a	a	k8xC	a
na	na	k7c6	na
visutých	visutý	k2eAgFnPc6d1	visutá
zahradách	zahrada	k1gFnPc6	zahrada
Semiramidiných	Semiramidin	k2eAgFnPc6d1	Semiramidin
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
také	také	k9	také
prostudoval	prostudovat	k5eAaPmAgMnS	prostudovat
Strabónův	Strabónův	k2eAgInSc4d1	Strabónův
popis	popis	k1gInSc4	popis
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
shodoval	shodovat	k5eAaImAgInS	shodovat
s	s	k7c7	s
průřezem	průřez	k1gInSc7	průřez
nalezeného	nalezený	k2eAgInSc2d1	nalezený
komplexu	komplex	k1gInSc2	komplex
u	u	k7c2	u
Karsu	Kars	k1gInSc2	Kars
<g/>
.	.	kIx.	.
</s>
<s>
Sedm	sedm	k4xCc1	sedm
divů	div	k1gInPc2	div
světa	svět	k1gInSc2	svět
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Visuté	visutý	k2eAgFnSc2d1	visutá
zahrady	zahrada	k1gFnSc2	zahrada
Semiramidiny	Semiramidin	k2eAgFnPc4d1	Semiramidin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Článek	článek	k1gInSc1	článek
na	na	k7c6	na
serveru	server	k1gInSc6	server
Antika	antika	k1gFnSc1	antika
</s>
