<s>
Golčův	Golčův	k2eAgInSc1d1	Golčův
Jeníkov	Jeníkov	k1gInSc1	Jeníkov
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Goltsch-Jenikau	Goltsch-Jenikaus	k1gInSc3	Goltsch-Jenikaus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
v	v	k7c6	v
Kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
25	[number]	k4	25
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Havlíčkova	Havlíčkův	k2eAgInSc2d1	Havlíčkův
Brodu	Brod	k1gInSc2	Brod
<g/>
.	.	kIx.	.
</s>
