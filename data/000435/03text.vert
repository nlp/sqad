<s>
Golčův	Golčův	k2eAgInSc1d1	Golčův
Jeníkov	Jeníkov	k1gInSc1	Jeníkov
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Goltsch-Jenikau	Goltsch-Jenikaus	k1gInSc3	Goltsch-Jenikaus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Havlíčkův	Havlíčkův	k2eAgInSc1d1	Havlíčkův
Brod	Brod	k1gInSc1	Brod
v	v	k7c6	v
Kraji	kraj	k1gInSc6	kraj
Vysočina	vysočina	k1gFnSc1	vysočina
<g/>
,	,	kIx,	,
25	[number]	k4	25
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Havlíčkova	Havlíčkův	k2eAgInSc2d1	Havlíčkův
Brodu	Brod	k1gInSc2	Brod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
přes	přes	k7c4	přes
2	[number]	k4	2
600	[number]	k4	600
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
protékají	protékat	k5eAaImIp3nP	protékat
potoky	potok	k1gInPc4	potok
Váhanka	Váhanka	k1gFnSc1	Váhanka
a	a	k8xC	a
Výrovka	výrovka	k1gFnSc1	výrovka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
levostrannými	levostranný	k2eAgInPc7d1	levostranný
přítoky	přítok	k1gInPc7	přítok
říčky	říčka	k1gFnSc2	říčka
Hostačovky	Hostačovka	k1gFnSc2	Hostačovka
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
obec	obec	k1gFnSc1	obec
písemně	písemně	k6eAd1	písemně
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
kronikář	kronikář	k1gMnSc1	kronikář
Jarloch	Jarloch	k1gMnSc1	Jarloch
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1461	[number]	k4	1461
<g/>
-	-	kIx~	-
<g/>
1580	[number]	k4	1580
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
Slavatů	Slavat	k1gInPc2	Slavat
z	z	k7c2	z
Chlumu	chlum	k1gInSc2	chlum
a	a	k8xC	a
Košumberka	Košumberka	k1gFnSc1	Košumberka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1636	[number]	k4	1636
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
císaře	císař	k1gMnSc2	císař
obec	obec	k1gFnSc1	obec
darem	dar	k1gInSc7	dar
generál	generál	k1gMnSc1	generál
Martin	Martin	k1gMnSc1	Martin
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Goltz	Goltz	k1gMnSc1	Goltz
(	(	kIx(	(
<g/>
†	†	k?	†
<g/>
1653	[number]	k4	1653
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1673	[number]	k4	1673
byl	být	k5eAaImAgInS	být
Jeníkov	Jeníkov	k1gInSc1	Jeníkov
v	v	k7c6	v
majetku	majetek	k1gInSc6	majetek
Barbory	Barbora	k1gFnSc2	Barbora
Eusebie	Eusebie	k1gFnSc2	Eusebie
hraběnky	hraběnka	k1gFnSc2	hraběnka
ze	z	k7c2	z
Žďáru	Žďár	k1gInSc2	Žďár
<g/>
,	,	kIx,	,
podruhé	podruhé	k6eAd1	podruhé
provdané	provdaný	k2eAgInPc1d1	provdaný
za	za	k7c2	za
hraběte	hrabě	k1gMnSc2	hrabě
Karla	Karel	k1gMnSc2	Karel
Leopolda	Leopold	k1gMnSc2	Leopold
Caretto	Caretto	k1gNnSc1	Caretto
Millesimo	Millesima	k1gFnSc5	Millesima
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
Golčova	Golčův	k2eAgNnSc2d1	Golčův
Jeníkova	Jeníkův	k2eAgNnPc1d1	Jeníkovo
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
vesnice	vesnice	k1gFnSc2	vesnice
Kobylí	kobylí	k2eAgFnSc1d1	kobylí
Hlava	hlava	k1gFnSc1	hlava
Nasavrky	Nasavrka	k1gFnSc2	Nasavrka
Římovice	Římovice	k1gFnSc1	Římovice
Sirákovice	Sirákovice	k1gFnSc1	Sirákovice
Stupárovice	Stupárovice	k1gFnSc1	Stupárovice
Vrtěšice	Vrtěšice	k1gFnSc1	Vrtěšice
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
Golčův	Golčův	k2eAgInSc1d1	Golčův
Jeníkov	Jeníkov	k1gInSc4	Jeníkov
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Golčově	Golčův	k2eAgFnSc6d1	Golčova
Jeníkově	Jeníkův	k2eAgFnSc6d1	Jeníkova
<g/>
.	.	kIx.	.
</s>
<s>
Děkanský	děkanský	k2eAgInSc1d1	děkanský
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Františka	František	k1gMnSc2	František
Serafínského	serafínský	k2eAgInSc2d1	serafínský
je	být	k5eAaImIp3nS	být
empírová	empírový	k2eAgFnSc1d1	empírová
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1827	[number]	k4	1827
<g/>
-	-	kIx~	-
<g/>
1829	[number]	k4	1829
Kostel	kostel	k1gInSc1	kostel
svaté	svatý	k2eAgFnSc2d1	svatá
Markéty	Markéta	k1gFnSc2	Markéta
postaven	postavit	k5eAaPmNgInS	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
staršího	starý	k2eAgInSc2d2	starší
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
jsou	být	k5eAaImIp3nP	být
náhrobní	náhrobní	k2eAgInPc1d1	náhrobní
kameny	kámen	k1gInPc1	kámen
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
nejstarší	starý	k2eAgMnSc1d3	nejstarší
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Golčova	Golčův	k2eAgFnSc1d1	Golčova
tvrz	tvrz	k1gFnSc1	tvrz
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
sídlo	sídlo	k1gNnSc1	sídlo
galerie	galerie	k1gFnSc2	galerie
Loreta	Loreto	k1gNnSc2	Loreto
-	-	kIx~	-
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1650	[number]	k4	1650
-	-	kIx~	-
1653	[number]	k4	1653
<g/>
.	.	kIx.	.
</s>
<s>
Založil	založit	k5eAaPmAgMnS	založit
ji	on	k3xPp3gFnSc4	on
Martin	Martin	k1gMnSc1	Martin
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
svobodný	svobodný	k2eAgMnSc1d1	svobodný
pán	pán	k1gMnSc1	pán
z	z	k7c2	z
Golče	Golč	k1gFnSc2	Golč
jako	jako	k8xS	jako
dík	dík	k1gInSc1	dík
za	za	k7c4	za
ukončení	ukončení	k1gNnSc4	ukončení
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Poutní	poutní	k2eAgNnSc1d1	poutní
místo	místo	k1gNnSc1	místo
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
roku	rok	k1gInSc2	rok
1827	[number]	k4	1827
<g/>
,	,	kIx,	,
dochovala	dochovat	k5eAaPmAgFnS	dochovat
se	se	k3xPyFc4	se
část	část	k1gFnSc1	část
obvodové	obvodový	k2eAgFnSc2d1	obvodová
zdi	zeď	k1gFnSc2	zeď
ambitu	ambit	k1gInSc2	ambit
a	a	k8xC	a
Loretánská	loretánský	k2eAgFnSc1d1	Loretánská
kaple	kaple	k1gFnSc1	kaple
u	u	k7c2	u
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Františka	František	k1gMnSc2	František
<g/>
.	.	kIx.	.
</s>
<s>
Madoně	Madona	k1gFnSc3	Madona
Jeníkovské	Jeníkovský	k2eAgFnSc2d1	Jeníkovská
z	z	k7c2	z
Lorety	loreta	k1gFnSc2	loreta
byla	být	k5eAaImAgFnS	být
zasvěcena	zasvěcen	k2eAgFnSc1d1	zasvěcena
14	[number]	k4	14
<g/>
.	.	kIx.	.
kaple	kaple	k1gFnSc2	kaple
Svaté	svatý	k2eAgFnSc2d1	svatá
cesty	cesta	k1gFnSc2	cesta
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Staré	Stará	k1gFnSc2	Stará
Boleslavi	Boleslaev	k1gFnSc3	Boleslaev
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
roku	rok	k1gInSc2	rok
1674	[number]	k4	1674
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
donátorem	donátor	k1gMnSc7	donátor
byl	být	k5eAaImAgMnS	být
Karel	Karel	k1gMnSc1	Karel
Leopold	Leopold	k1gMnSc1	Leopold
<g/>
,	,	kIx,	,
hrabě	hrabě	k1gMnSc1	hrabě
Carreto	Carreta	k1gFnSc5	Carreta
Millesimo	Millesima	k1gFnSc5	Millesima
<g/>
.	.	kIx.	.
</s>
<s>
Děkanství	děkanství	k1gNnSc1	děkanství
Věž	věž	k1gFnSc4	věž
se	s	k7c7	s
zvonicí	zvonice	k1gFnSc7	zvonice
Zámecký	zámecký	k2eAgInSc4d1	zámecký
areál	areál	k1gInSc4	areál
Radnice	radnice	k1gFnSc1	radnice
Dům	dům	k1gInSc1	dům
Viktorin	Viktorin	k1gInSc1	Viktorin
-	-	kIx~	-
budova	budova	k1gFnSc1	budova
bývalého	bývalý	k2eAgInSc2d1	bývalý
hotelu	hotel	k1gInSc2	hotel
Černý	Černý	k1gMnSc1	Černý
orel	orel	k1gMnSc1	orel
Budova	budova	k1gFnSc1	budova
bývalé	bývalý	k2eAgFnPc4d1	bývalá
vrchnostenské	vrchnostenský	k2eAgFnPc4d1	vrchnostenská
hospody	hospody	k?	hospody
Zlaté	zlatý	k2eAgFnSc6d1	zlatá
slunce	slunka	k1gFnSc6	slunka
Budova	budova	k1gFnSc1	budova
Staré	Staré	k2eAgFnSc2d1	Staré
pošty	pošta	k1gFnSc2	pošta
Synagoga	synagoga	k1gFnSc1	synagoga
je	být	k5eAaImIp3nS	být
novorománská	novorománský	k2eAgFnSc1d1	novorománská
stavba	stavba	k1gFnSc1	stavba
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1871	[number]	k4	1871
<g/>
-	-	kIx~	-
<g/>
1873	[number]	k4	1873
Židovský	židovský	k2eAgInSc1d1	židovský
hřbitov	hřbitov	k1gInSc1	hřbitov
má	mít	k5eAaImIp3nS	mít
asi	asi	k9	asi
1	[number]	k4	1
500	[number]	k4	500
náhrobních	náhrobní	k2eAgInPc2d1	náhrobní
kamenů	kámen	k1gInPc2	kámen
Ota	Ota	k1gMnSc1	Ota
Hora	Hora	k1gMnSc1	Hora
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
Jarmila	Jarmila	k1gFnSc1	Jarmila
Kratochvílová	Kratochvílová	k1gFnSc1	Kratochvílová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
atletka	atletka	k1gFnSc1	atletka
Karel	Karel	k1gMnSc1	Karel
Pavlík	Pavlík	k1gMnSc1	Pavlík
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
-	-	kIx~	-
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
Jiří	Jiří	k1gMnSc1	Jiří
Sedláček	Sedláček	k1gMnSc1	Sedláček
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odborník	odborník	k1gMnSc1	odborník
na	na	k7c4	na
parní	parní	k2eAgFnPc4d1	parní
lokomotivy	lokomotiva	k1gFnPc4	lokomotiva
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Golčův	Golčův	k2eAgInSc4d1	Golčův
Jeníkov	Jeníkov	k1gInSc4	Jeníkov
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
města	město	k1gNnSc2	město
</s>
