<s>
Frazeologie	frazeologie	k1gFnSc1	frazeologie
je	být	k5eAaImIp3nS	být
lingvistická	lingvistický	k2eAgFnSc1d1	lingvistická
lexikologická	lexikologický	k2eAgFnSc1d1	lexikologická
disciplína	disciplína	k1gFnSc1	disciplína
o	o	k7c6	o
ustálených	ustálený	k2eAgNnPc6d1	ustálené
slovních	slovní	k2eAgNnPc6d1	slovní
spojeních	spojení	k1gNnPc6	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
jednotkou	jednotka	k1gFnSc7	jednotka
je	být	k5eAaImIp3nS	být
frazém	frazém	k1gInSc1	frazém
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
obrazné	obrazný	k2eAgNnSc4d1	obrazné
<g/>
,	,	kIx,	,
ustálené	ustálený	k2eAgNnSc4d1	ustálené
víceslovné	víceslovný	k2eAgNnSc4d1	víceslovné
pojmenování	pojmenování	k1gNnSc4	pojmenování
(	(	kIx(	(
<g/>
např.	např.	kA	např.
"	"	kIx"	"
<g/>
mít	mít	k5eAaImF	mít
namále	namále	k6eAd1	namále
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
být	být	k5eAaImF	být
páté	pátý	k4xOgNnSc4	pátý
kolo	kolo	k1gNnSc4	kolo
u	u	k7c2	u
vozu	vůz	k1gInSc2	vůz
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
přístupu	přístup	k1gInSc2	přístup
se	se	k3xPyFc4	se
frazeologie	frazeologie	k1gFnSc1	frazeologie
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
frazeologii	frazeologie	k1gFnSc4	frazeologie
historickou	historický	k2eAgFnSc4d1	historická
<g/>
,	,	kIx,	,
porovnávací	porovnávací	k2eAgFnSc4d1	porovnávací
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
Frazeologismy	frazeologismus	k1gInPc1	frazeologismus
lze	lze	k6eAd1	lze
také	také	k9	také
dělit	dělit	k5eAaImF	dělit
podle	podle	k7c2	podle
kritérií	kritérion	k1gNnPc2	kritérion
formálních	formální	k2eAgInPc2d1	formální
a	a	k8xC	a
sémantických	sémantický	k2eAgInPc2d1	sémantický
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
způsoby	způsob	k1gInPc4	způsob
dělení	dělení	k1gNnSc2	dělení
<g/>
.	.	kIx.	.
</s>
<s>
Idiomatika	idiomatika	k1gFnSc1	idiomatika
</s>
