<s>
Základní	základní	k2eAgFnSc7d1	základní
jednotkou	jednotka	k1gFnSc7	jednotka
je	být	k5eAaImIp3nS	být
frazém	frazém	k1gInSc1	frazém
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
obrazné	obrazný	k2eAgNnSc4d1	obrazné
<g/>
,	,	kIx,	,
ustálené	ustálený	k2eAgNnSc4d1	ustálené
víceslovné	víceslovný	k2eAgNnSc4d1	víceslovné
pojmenování	pojmenování	k1gNnSc4	pojmenování
(	(	kIx(	(
<g/>
např.	např.	kA	např.
"	"	kIx"	"
<g/>
mít	mít	k5eAaImF	mít
namále	namále	k6eAd1	namále
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
být	být	k5eAaImF	být
páté	pátý	k4xOgNnSc4	pátý
kolo	kolo	k1gNnSc4	kolo
u	u	k7c2	u
vozu	vůz	k1gInSc2	vůz
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
