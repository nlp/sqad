<s>
Velká	velký	k2eAgFnSc1d1
Skrovnice	Skrovnice	k1gFnSc1
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Skrovnice	Skrovnice	k1gFnSc1
Kaple	kaple	k1gFnSc2
na	na	k7c6
hřbitově	hřbitov	k1gInSc6
ve	v	k7c6
Velké	velký	k2eAgFnSc6d1
Skrovnici	Skrovnice	k1gFnSc6
</s>
<s>
znak	znak	k1gInSc1
Lokalita	lokalita	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
obec	obec	k1gFnSc1
LAU	LAU	kA
2	#num#	k4
(	(	kIx(
<g/>
obec	obec	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CZ0534	CZ0534	k4
581101	#num#	k4
Pověřená	pověřený	k2eAgFnSc1d1
obec	obec	k1gFnSc1
a	a	k8xC
obec	obec	k1gFnSc1
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
</s>
<s>
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
Okres	okres	k1gInSc1
(	(	kIx(
<g/>
LAU	LAU	kA
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
534	#num#	k4
<g/>
)	)	kIx)
Kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
NUTS	NUTS	kA
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pardubický	pardubický	k2eAgMnSc1d1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
53	#num#	k4
<g/>
)	)	kIx)
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Čechy	Čechy	k1gFnPc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
1	#num#	k4
<g/>
′	′	k?
<g/>
50	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
°	°	k?
<g/>
19	#num#	k4
<g/>
′	′	k?
<g/>
1	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
298	#num#	k4
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
5,75	5,75	k4
km²	km²	k?
Katastrální	katastrální	k2eAgNnSc4d1
území	území	k1gNnSc4
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Skrovnice	Skrovnice	k1gFnSc1
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
415	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
PSČ	PSČ	kA
</s>
<s>
562	#num#	k4
01	#num#	k4
Počet	počet	k1gInSc1
částí	část	k1gFnPc2
obce	obec	k1gFnSc2
</s>
<s>
2	#num#	k4
Počet	počet	k1gInSc1
k.	k.	k?
ú.	ú.	k?
</s>
<s>
1	#num#	k4
Počet	počet	k1gInSc1
ZSJ	ZSJ	kA
</s>
<s>
2	#num#	k4
Kontakt	kontakt	k1gInSc1
Adresa	adresa	k1gFnSc1
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Skrovnice	Skrovnice	k1gFnSc1
30562	#num#	k4
01	#num#	k4
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
1	#num#	k4
obec@velkaskrovnice.cz	obec@velkaskrovnice.cza	k1gFnPc2
Starosta	Starosta	k1gMnSc1
</s>
<s>
Ing.	ing.	kA
Pavel	Pavel	k1gMnSc1
Chalupa	Chalupa	k1gMnSc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
:	:	kIx,
www.velkaskrovnice.cz	www.velkaskrovnice.cz	k1gInSc1
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Skrovnice	Skrovnice	k1gFnSc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Obec	obec	k1gFnSc1
Velká	velká	k1gFnSc1
Skrovnice	Skrovnice	k1gFnSc1
(	(	kIx(
<g/>
německy	německy	k6eAd1
Groß	Groß	k1gMnSc1
Skraunitz	Skraunitz	k1gMnSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
okrese	okres	k1gInSc6
Ústí	ústí	k1gNnSc2
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
v	v	k7c6
Pardubickém	pardubický	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
298	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
obci	obec	k1gFnSc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
[	[	kIx(
<g/>
1349	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Části	část	k1gFnPc1
obce	obec	k1gFnSc2
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Skrovnice	Skrovnice	k1gFnSc1
</s>
<s>
Malá	malý	k2eAgFnSc1d1
Skrovnice	Skrovnice	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
:	:	kIx,
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
obcích	obec	k1gFnPc6
-	-	kIx~
k	k	k7c3
1.1	1.1	k4
<g/>
.2020	.2020	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Velká	velký	k2eAgFnSc1d1
Skrovnice	Skrovnice	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Skrovnice	Skrovnice	k1gFnSc1
v	v	k7c6
Registru	registrum	k1gNnSc6
územní	územní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
<g/>
,	,	kIx,
adres	adresa	k1gFnPc2
a	a	k8xC
nemovitostí	nemovitost	k1gFnPc2
(	(	kIx(
<g/>
RÚIAN	RÚIAN	kA
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Části	část	k1gFnSc2
obce	obec	k1gFnSc2
Velká	velký	k2eAgFnSc1d1
Skrovnice	Skrovnice	k1gFnSc1
Části	část	k1gFnSc2
obce	obec	k1gFnSc2
</s>
<s>
Malá	malý	k2eAgFnSc1d1
SkrovniceVelká	SkrovniceVelký	k2eAgFnSc1d1
Skrovnice	Skrovnice	k1gFnSc1
</s>
<s>
Města	město	k1gNnPc1
<g/>
,	,	kIx,
městyse	městys	k1gInPc1
a	a	k8xC
obce	obec	k1gFnPc1
okresu	okres	k1gInSc2
Ústí	ústit	k5eAaImIp3nP
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
</s>
<s>
Albrechtice	Albrechtice	k1gFnPc1
•	•	k?
Anenská	anenský	k2eAgFnSc1d1
Studánka	studánka	k1gFnSc1
•	•	k?
Běstovice	Běstovice	k1gFnSc2
•	•	k?
Bošín	Bošín	k1gInSc1
•	•	k?
Brandýs	Brandýs	k1gInSc1
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
•	•	k?
Bučina	bučina	k1gFnSc1
•	•	k?
Bystřec	Bystřec	k1gInSc1
•	•	k?
Cotkytle	Cotkytle	k1gFnSc2
•	•	k?
Čenkovice	Čenkovice	k1gFnSc2
•	•	k?
Červená	červený	k2eAgFnSc1d1
Voda	voda	k1gFnSc1
•	•	k?
Česká	český	k2eAgFnSc1d1
Rybná	rybný	k2eAgFnSc1d1
•	•	k?
Česká	český	k2eAgFnSc1d1
Třebová	Třebová	k1gFnSc1
•	•	k?
České	český	k2eAgFnSc2d1
Heřmanice	Heřmanice	k1gFnSc2
•	•	k?
České	český	k2eAgFnSc2d1
Libchavy	Libchava	k1gFnSc2
•	•	k?
České	český	k2eAgFnSc2d1
Petrovice	Petrovice	k1gFnSc2
•	•	k?
Damníkov	Damníkov	k1gInSc1
•	•	k?
Dlouhá	Dlouhá	k1gFnSc1
Třebová	Třebová	k1gFnSc1
•	•	k?
Dlouhoňovice	Dlouhoňovice	k1gFnSc2
•	•	k?
Dobříkov	Dobříkov	k1gInSc1
•	•	k?
Dolní	dolní	k2eAgFnSc1d1
Čermná	Čermný	k2eAgFnSc1d1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Dolní	dolní	k2eAgFnSc1d1
Dobrouč	Dobrouč	k1gFnSc1
•	•	k?
Dolní	dolní	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
•	•	k?
Džbánov	Džbánov	k1gInSc1
•	•	k?
Hejnice	Hejnice	k1gFnSc2
•	•	k?
Helvíkovice	Helvíkovice	k1gFnSc2
•	•	k?
Hnátnice	Hnátnice	k1gFnSc2
•	•	k?
Horní	horní	k2eAgFnSc1d1
Čermná	Čermný	k2eAgFnSc1d1
•	•	k?
Horní	horní	k2eAgFnSc1d1
Heřmanice	Heřmanice	k1gFnSc1
•	•	k?
Horní	horní	k2eAgInSc1d1
Třešňovec	třešňovec	k1gInSc1
•	•	k?
Hrádek	hrádek	k1gInSc1
•	•	k?
Hrušová	hrušový	k2eAgFnSc1d1
•	•	k?
Choceň	Choceň	k1gFnSc1
•	•	k?
Jablonné	Jablonné	k1gNnSc1
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
•	•	k?
Jamné	Jamná	k1gFnSc2
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
•	•	k?
Javorník	Javorník	k1gInSc4
•	•	k?
Jehnědí	Jehnědý	k2eAgMnPc1d1
•	•	k?
Kameničná	Kameničný	k2eAgFnSc1d1
•	•	k?
Klášterec	Klášterec	k1gInSc1
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
•	•	k?
Koldín	Koldín	k1gMnSc1
•	•	k?
Kosořín	Kosořín	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Králíky	Králík	k1gMnPc4
•	•	k?
Krasíkov	Krasíkov	k1gInSc1
•	•	k?
Kunvald	Kunvald	k1gInSc1
•	•	k?
Lanškroun	Lanškroun	k1gInSc1
•	•	k?
Leština	Leština	k1gFnSc1
•	•	k?
Letohrad	letohrad	k1gInSc1
•	•	k?
Libecina	Libecina	k1gFnSc1
•	•	k?
Libchavy	Libchava	k1gFnSc2
•	•	k?
Lichkov	Lichkov	k1gInSc1
•	•	k?
Líšnice	Líšnice	k1gFnSc2
•	•	k?
Lubník	Lubník	k1gMnSc1
•	•	k?
Lukavice	Lukavice	k1gFnSc2
•	•	k?
Luková	Lukový	k2eAgFnSc1d1
•	•	k?
Mistrovice	Mistrovice	k1gFnSc1
•	•	k?
Mladkov	Mladkov	k1gInSc1
•	•	k?
Mostek	mostek	k1gInSc1
•	•	k?
Nasavrky	Nasavrka	k1gFnSc2
•	•	k?
Nekoř	kořit	k5eNaImRp2nS
•	•	k?
Nové	Nové	k2eAgInPc1d1
Hrady	hrad	k1gInPc1
•	•	k?
Orlické	orlický	k2eAgNnSc4d1
Podhůří	Podhůří	k1gNnSc4
•	•	k?
Orličky	orlička	k1gFnSc2
•	•	k?
Ostrov	ostrov	k1gInSc1
•	•	k?
Oucmanice	Oucmanice	k1gFnSc2
•	•	k?
Pastviny	pastvina	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Petrovice	Petrovice	k1gFnSc2
•	•	k?
Písečná	písečný	k2eAgFnSc1d1
•	•	k?
Plchovice	Plchovice	k1gFnSc1
•	•	k?
Podlesí	Podlesí	k1gNnSc2
•	•	k?
Přívrat	přívrat	k1gInSc1
•	•	k?
Pustina	pustina	k1gFnSc1
•	•	k?
Radhošť	Radhošť	k1gInSc1
•	•	k?
Rudoltice	Rudoltice	k1gFnSc1
•	•	k?
Rybník	rybník	k1gInSc4
•	•	k?
Řepníky	řepník	k1gInPc4
•	•	k?
Řetová	Řetová	k1gFnSc1
•	•	k?
Řetůvka	Řetůvka	k1gFnSc1
•	•	k?
Sázava	Sázava	k1gFnSc1
•	•	k?
Seč	seč	k6eAd1
•	•	k?
Semanín	Semanín	k1gInSc1
•	•	k?
Skořenice	Skořenice	k1gFnSc2
•	•	k?
Slatina	slatina	k1gFnSc1
•	•	k?
Sobkovice	Sobkovice	k1gFnSc2
•	•	k?
Sopotnice	Sopotnice	k1gFnSc2
•	•	k?
Sruby	srub	k1gInPc4
•	•	k?
Stradouň	Stradouň	k1gFnSc1
•	•	k?
Strážná	strážná	k1gFnSc1
•	•	k?
Studené	Studená	k1gFnSc2
•	•	k?
Sudislav	Sudislav	k1gMnSc1
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Sudslava	Sudslava	k1gFnSc1
•	•	k?
Svatý	svatý	k2eAgMnSc1d1
Jiří	Jiří	k1gMnSc1
•	•	k?
Šedivec	šedivec	k1gMnSc1
•	•	k?
Tatenice	Tatenice	k1gFnSc2
•	•	k?
Těchonín	Těchonín	k1gMnSc1
•	•	k?
Tisová	tisový	k2eAgFnSc1d1
•	•	k?
Trpík	Trpík	k1gInSc1
•	•	k?
Třebovice	Třebovice	k1gFnSc2
•	•	k?
Týnišťko	Týnišťko	k1gNnSc4
•	•	k?
Újezd	Újezd	k1gInSc1
u	u	k7c2
Chocně	Choceň	k1gFnSc2
•	•	k?
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
•	•	k?
Velká	velký	k2eAgFnSc1d1
Skrovnice	Skrovnice	k1gFnSc1
•	•	k?
Verměřovice	Verměřovice	k1gFnSc2
•	•	k?
Vinary	Vinara	k1gFnSc2
•	•	k?
Voděrady	Voděrada	k1gFnSc2
•	•	k?
Vraclav	Vraclav	k1gFnSc2
•	•	k?
Vračovice-Orlov	Vračovice-Orlov	k1gInSc1
•	•	k?
Výprachtice	Výprachtice	k1gFnSc2
•	•	k?
Vysoké	vysoký	k2eAgNnSc1d1
Mýto	mýto	k1gNnSc1
•	•	k?
Zádolí	Zádole	k1gFnPc2
•	•	k?
Záchlumí	Záchlumí	k1gNnSc4
•	•	k?
Zálší	Zálší	k1gNnSc2
•	•	k?
Zámrsk	Zámrsk	k1gInSc1
•	•	k?
Zářecká	zářecký	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
•	•	k?
Žamberk	Žamberk	k1gInSc1
•	•	k?
Žampach	Žampach	k1gInSc1
•	•	k?
Žichlínek	Žichlínek	k1gInSc1
legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
město	město	k1gNnSc1
<g/>
,	,	kIx,
městys	městys	k1gInSc1
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
131257	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
1969155411323508940009	#num#	k4
</s>
