<p>
<s>
Bylina	bylina	k1gFnSc1	bylina
je	být	k5eAaImIp3nS	být
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
nedřevnatějící	dřevnatějící	k2eNgInSc4d1	dřevnatějící
nadzemní	nadzemní	k2eAgInSc4d1	nadzemní
stonek	stonek	k1gInSc4	stonek
<g/>
.	.	kIx.	.
</s>
<s>
Bylinný	bylinný	k2eAgInSc1d1	bylinný
typ	typ	k1gInSc1	typ
růstu	růst	k1gInSc2	růst
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
rostlinné	rostlinný	k2eAgFnSc6d1	rostlinná
říší	říš	k1gFnSc7	říš
mnohokrát	mnohokrát	k6eAd1	mnohokrát
nezávisle	závisle	k6eNd1	závisle
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
čeledí	čeleď	k1gFnPc2	čeleď
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgMnPc4	svůj
bylinné	bylinný	k2eAgMnPc4d1	bylinný
zástupce	zástupce	k1gMnPc4	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
o	o	k7c4	o
lodyhu	lodyha	k1gFnSc4	lodyha
nesoucí	nesoucí	k2eAgInPc4d1	nesoucí
listy	list	k1gInPc4	list
<g/>
,	,	kIx,	,
neolistěný	olistěný	k2eNgInSc4d1	olistěný
stvol	stvol	k1gInSc4	stvol
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
u	u	k7c2	u
trav	tráva	k1gFnPc2	tráva
stéblo	stéblo	k1gNnSc4	stéblo
<g/>
.	.	kIx.	.
</s>
<s>
Bylinný	bylinný	k2eAgInSc1d1	bylinný
stonek	stonek	k1gInSc1	stonek
bývá	bývat	k5eAaImIp3nS	bývat
různého	různý	k2eAgInSc2d1	různý
tvaru	tvar	k1gInSc2	tvar
na	na	k7c6	na
průřezu	průřez	k1gInSc6	průřez
<g/>
:	:	kIx,	:
válcovitý	válcovitý	k2eAgMnSc1d1	válcovitý
<g/>
,	,	kIx,	,
čtyřhranný	čtyřhranný	k2eAgMnSc1d1	čtyřhranný
<g/>
,	,	kIx,	,
trojhranný	trojhranný	k2eAgMnSc1d1	trojhranný
<g/>
,	,	kIx,	,
zploštělý	zploštělý	k2eAgMnSc1d1	zploštělý
<g/>
,	,	kIx,	,
rýhovaný	rýhovaný	k2eAgMnSc1d1	rýhovaný
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
růstu	růst	k1gInSc2	růst
přímý	přímý	k2eAgInSc1d1	přímý
<g/>
,	,	kIx,	,
vystoupavý	vystoupavý	k2eAgInSc1d1	vystoupavý
<g/>
,	,	kIx,	,
poléhavý	poléhavý	k2eAgInSc1d1	poléhavý
<g/>
,	,	kIx,	,
plazivý	plazivý	k2eAgInSc1d1	plazivý
<g/>
,	,	kIx,	,
popínavý	popínavý	k2eAgInSc1d1	popínavý
(	(	kIx(	(
<g/>
bylinné	bylinný	k2eAgFnPc1d1	bylinná
liány	liána	k1gFnPc1	liána
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byliny	bylina	k1gFnPc1	bylina
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
různou	různý	k2eAgFnSc4d1	různá
velikost	velikost	k1gFnSc4	velikost
od	od	k7c2	od
nejmenších	malý	k2eAgFnPc2d3	nejmenší
rostlin	rostlina	k1gFnPc2	rostlina
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
drobnička	drobnička	k1gFnSc1	drobnička
<g/>
,	,	kIx,	,
Wolffia	Wolffia	k1gFnSc1	Wolffia
<g/>
)	)	kIx)	)
po	po	k7c4	po
obří	obří	k2eAgFnPc4d1	obří
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejmohutnější	mohutný	k2eAgFnPc4d3	nejmohutnější
byliny	bylina	k1gFnPc4	bylina
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
banánovníku	banánovník	k1gInSc2	banánovník
<g/>
.	.	kIx.	.
</s>
<s>
Banánovník	banánovník	k1gInSc1	banánovník
textilní	textilní	k2eAgInSc1d1	textilní
(	(	kIx(	(
<g/>
Musa	Musa	k1gFnSc1	Musa
textilis	textilis	k1gFnSc2	textilis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pocházející	pocházející	k2eAgMnPc1d1	pocházející
z	z	k7c2	z
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
výšky	výška	k1gFnSc2	výška
až	až	k9	až
8	[number]	k4	8
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Banánovníky	banánovník	k1gInPc7	banánovník
mají	mít	k5eAaImIp3nP	mít
tzv.	tzv.	kA	tzv.
nepravý	pravý	k2eNgInSc4d1	nepravý
kmen	kmen	k1gInSc4	kmen
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
celý	celý	k2eAgInSc1d1	celý
bylinný	bylinný	k2eAgInSc1d1	bylinný
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
bázemi	báze	k1gFnPc7	báze
řapíků	řapík	k1gInPc2	řapík
listů	list	k1gInPc2	list
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
KUBÁT	Kubát	k1gMnSc1	Kubát
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
botanika	botanika	k1gFnSc1	botanika
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
:	:	kIx,	:
UNIVERZITA	univerzita	k1gFnSc1	univerzita
JANA	Jan	k1gMnSc2	Jan
EVANGELISTY	evangelista	k1gMnSc2	evangelista
PURKYNĚ	PURKYNĚ	kA	PURKYNĚ
PŘÍRODOVĚDECKÁ	přírodovědecký	k2eAgFnSc1d1	Přírodovědecká
FAKULTA	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
katedra	katedra	k1gFnSc1	katedra
biologie	biologie	k1gFnSc1	biologie
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
bylina	bylina	k1gFnSc1	bylina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
bylina	bylina	k1gFnSc1	bylina
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Pěstování	pěstování	k1gNnSc1	pěstování
bylinek	bylinka	k1gFnPc2	bylinka
v	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
prostředí	prostředí	k1gNnSc6	prostředí
</s>
</p>
