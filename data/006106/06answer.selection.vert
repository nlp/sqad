<s>
Egyptský	egyptský	k2eAgMnSc1d1	egyptský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Nadžíb	Nadžíb	k1gMnSc1	Nadžíb
Mahfúz	Mahfúz	k1gMnSc1	Mahfúz
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
arabským	arabský	k2eAgMnSc7d1	arabský
spisovatelem	spisovatel	k1gMnSc7	spisovatel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgMnS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
literaturu	literatura	k1gFnSc4	literatura
<g/>
.	.	kIx.	.
</s>
