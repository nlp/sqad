<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
Kirche	Kirche	k1gNnSc1
Mariä	Mariä	k1gFnSc2
Himmelfahrt	Himmelfahrta	k1gFnPc2
<g/>
)	)	kIx)
v	v	k7c6
saském	saský	k2eAgInSc6d1
Leutersdorfu	Leutersdorf	k1gInSc6
je	být	k5eAaImIp3nS
novogotický	novogotický	k2eAgInSc1d1
farní	farní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
dokončený	dokončený	k2eAgInSc1d1
roku	rok	k1gInSc2
1862	#num#	k4
<g/>
.	.	kIx.
</s>