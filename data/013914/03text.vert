<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
(	(	kIx(
<g/>
Leutersdorf	Leutersdorf	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Mariev	Mariva	k1gFnPc2
Leutersdorfu	Leutersdorf	k1gInSc2
Boční	boční	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
kostel	kostel	k1gInSc4
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
Místo	místo	k6eAd1
Stát	stát	k1gInSc1
</s>
<s>
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc4
Spolková	spolkový	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Sasko	Sasko	k1gNnSc4
Zemský	zemský	k2eAgInSc1d1
okres	okres	k1gInSc1
</s>
<s>
Zhořelec	Zhořelec	k1gInSc1
Obec	obec	k1gFnSc1
</s>
<s>
Leutersdorf	Leutersdorf	k1gInSc1
Lokalita	lokalita	k1gFnSc1
</s>
<s>
Neuleutersdorf	Neuleutersdorf	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
57	#num#	k4
<g/>
′	′	k?
<g/>
8,87	8,87	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
38	#num#	k4
<g/>
′	′	k?
<g/>
26,22	26,22	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Církev	církev	k1gFnSc1
</s>
<s>
římskokatolická	římskokatolický	k2eAgFnSc1d1
Provincie	provincie	k1gFnSc1
</s>
<s>
berlínská	berlínský	k2eAgFnSc1d1
Diecéze	diecéze	k1gFnSc1
</s>
<s>
drážďansko-míšeňská	drážďansko-míšeňský	k2eAgFnSc1d1
Děkanát	děkanát	k1gInSc1
</s>
<s>
Budyšín	Budyšín	k1gInSc1
Farnost	farnost	k1gFnSc1
</s>
<s>
Leutersdorf	Leutersdorf	k1gInSc1
Status	status	k1gInSc1
</s>
<s>
farní	farní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
Užívání	užívání	k1gNnSc1
</s>
<s>
pravidelné	pravidelný	k2eAgNnSc1d1
Datum	datum	k1gNnSc1
posvěcení	posvěcení	k1gNnSc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1862	#num#	k4
Světitel	světitel	k1gMnSc1
</s>
<s>
Ludwig	Ludwig	k1gMnSc1
Forwerk	Forwerk	k1gInSc4
Architektonický	architektonický	k2eAgInSc4d1
popis	popis	k1gInSc4
Architekt	architekt	k1gMnSc1
</s>
<s>
Carl	Carl	k1gMnSc1
August	August	k1gMnSc1
Schramm	Schramm	k1gMnSc1
Stavební	stavební	k2eAgInSc4d1
sloh	sloh	k1gInSc4
</s>
<s>
novogotika	novogotika	k1gFnSc1
Typ	typ	k1gInSc1
stavby	stavba	k1gFnSc2
</s>
<s>
kostel	kostel	k1gInSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1860	#num#	k4
<g/>
–	–	k?
<g/>
1862	#num#	k4
Specifikace	specifikace	k1gFnSc1
Umístění	umístění	k1gNnSc2
oltáře	oltář	k1gInSc2
</s>
<s>
severovýchod	severovýchod	k1gInSc1
Stavební	stavební	k2eAgInSc4d1
materiál	materiál	k1gInSc4
</s>
<s>
zdivo	zdivo	k1gNnSc1
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Aloys-Scholze-Straße	Aloys-Scholze-Straße	k6eAd1
402794	#num#	k4
Leutersdorf	Leutersdorf	k1gInSc4
Ulice	ulice	k1gFnSc2
</s>
<s>
Aloys-Scholze-Straße	Aloys-Scholze-Straße	k6eAd1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
(	(	kIx(
<g/>
německy	německy	k6eAd1
Kirche	Kirche	k1gNnSc1
Mariä	Mariä	k1gFnSc2
Himmelfahrt	Himmelfahrta	k1gFnPc2
<g/>
)	)	kIx)
v	v	k7c6
saském	saský	k2eAgInSc6d1
Leutersdorfu	Leutersdorf	k1gInSc6
je	být	k5eAaImIp3nS
novogotický	novogotický	k2eAgInSc1d1
farní	farní	k2eAgInSc1d1
kostel	kostel	k1gInSc1
dokončený	dokončený	k2eAgInSc1d1
roku	rok	k1gInSc2
1862	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Pozemek	pozemek	k1gInSc1
pro	pro	k7c4
výstavbu	výstavba	k1gFnSc4
katolického	katolický	k2eAgInSc2d1
kostela	kostel	k1gInSc2
v	v	k7c6
leutersdorfské	leutersdorfský	k2eAgFnSc6d1
části	část	k1gFnSc6
Neuleutersdorf	Neuleutersdorf	k1gInSc1
získal	získat	k5eAaPmAgInS
roku	rok	k1gInSc2
1851	#num#	k4
biskup	biskup	k1gMnSc1
Joseph	Joseph	k1gMnSc1
Dittrich	Dittrich	k1gMnSc1
(	(	kIx(
<g/>
1794	#num#	k4
<g/>
–	–	k?
<g/>
1853	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Položení	položení	k1gNnSc3
základního	základní	k2eAgInSc2d1
kamene	kámen	k1gInSc2
se	se	k3xPyFc4
uskutečnilo	uskutečnit	k5eAaPmAgNnS
29	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1860	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výstavba	výstavba	k1gFnSc1
trvala	trvat	k5eAaImAgFnS
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
a	a	k8xC
nový	nový	k2eAgInSc1d1
svatostánek	svatostánek	k1gInSc1
byl	být	k5eAaImAgInS
vysvěcen	vysvětit	k5eAaPmNgInS
5	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1862	#num#	k4
biskupem	biskup	k1gMnSc7
Ludwigem	Ludwig	k1gMnSc7
Forwerkem	Forwerek	k1gMnSc7
(	(	kIx(
<g/>
1816	#num#	k4
<g/>
–	–	k?
<g/>
1875	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plány	plán	k1gInPc7
zhotovil	zhotovit	k5eAaPmAgMnS
žitavský	žitavský	k2eAgMnSc1d1
architekt	architekt	k1gMnSc1
Carl	Carl	k1gMnSc1
August	August	k1gMnSc1
Schramm	Schramm	k1gMnSc1
(	(	kIx(
<g/>
1807	#num#	k4
<g/>
–	–	k?
<g/>
1869	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
farářem	farář	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Anton	Anton	k1gMnSc1
Müller	Müller	k1gMnSc1
ze	z	k7c2
severočeského	severočeský	k2eAgInSc2d1
Varnsdorfu	Varnsdorf	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1878	#num#	k4
získal	získat	k5eAaPmAgInS
kostel	kostel	k1gInSc1
první	první	k4xOgInPc4
varhany	varhany	k1gInPc4
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1910	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
elektrifikaci	elektrifikace	k1gFnSc3
a	a	k8xC
roku	rok	k1gInSc3
1925	#num#	k4
bylo	být	k5eAaImAgNnS
vybudováno	vybudovat	k5eAaPmNgNnS
plynové	plynový	k2eAgNnSc1d1
topení	topení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1923	#num#	k4
a	a	k8xC
1927	#num#	k4
obdržel	obdržet	k5eAaPmAgInS
kostel	kostel	k1gInSc4
nové	nový	k2eAgInPc1d1
zvony	zvon	k1gInPc1
jako	jako	k8xS,k8xC
náhradu	náhrada	k1gFnSc4
za	za	k7c4
zvony	zvon	k1gInPc4
zrekvírované	zrekvírovaný	k2eAgInPc4d1
během	během	k7c2
první	první	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místní	místní	k2eAgFnSc1d1
farář	farář	k1gMnSc1
Aloys	Aloys	k1gInSc4
Scholze	Scholze	k1gFnSc2
(	(	kIx(
<g/>
1893	#num#	k4
<g/>
–	–	k?
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
odpůrce	odpůrce	k1gMnSc1
nacismu	nacismus	k1gInSc2
<g/>
,	,	kIx,
zahynul	zahynout	k5eAaPmAgMnS
roku	rok	k1gInSc2
1942	#num#	k4
v	v	k7c6
koncentračním	koncentrační	k2eAgInSc6d1
táboře	tábor	k1gInSc6
Dachau	Dachaus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvony	zvon	k1gInPc1
zrekvírované	zrekvírovaný	k2eAgInPc1d1
během	během	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
byly	být	k5eAaImAgFnP
nahrazeny	nahradit	k5eAaPmNgFnP
novými	nový	k2eAgFnPc7d1
roku	rok	k1gInSc2
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkové	celkový	k2eAgFnPc4d1
rekonstrukce	rekonstrukce	k1gFnPc4
se	se	k3xPyFc4
stavba	stavba	k1gFnSc1
dočkala	dočkat	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1962	#num#	k4
u	u	k7c2
příležitosti	příležitost	k1gFnSc2
stého	stý	k4xOgNnSc2
výročí	výročí	k1gNnPc2
dokončení	dokončení	k1gNnSc2
<g/>
,	,	kIx,
další	další	k2eAgFnPc1d1
následovaly	následovat	k5eAaImAgFnP
roku	rok	k1gInSc2
1997	#num#	k4
a	a	k8xC
v	v	k7c6
letech	let	k1gInPc6
2007	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kostel	kostel	k1gInSc1
je	být	k5eAaImIp3nS
chráněn	chránit	k5eAaImNgInS
jako	jako	k8xS,k8xC
kulturní	kulturní	k2eAgFnSc1d1
památka	památka	k1gFnSc1
pod	pod	k7c7
číslem	číslo	k1gNnSc7
09272089	#num#	k4
a	a	k8xC
je	být	k5eAaImIp3nS
využíván	využívat	k5eAaImNgInS,k5eAaPmNgInS
k	k	k7c3
pravidelným	pravidelný	k2eAgFnPc3d1
bohoslužbám	bohoslužba	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
farním	farní	k2eAgInSc7d1
kostelem	kostel	k1gInSc7
Římskokatolické	římskokatolický	k2eAgFnSc2d1
farnosti	farnost	k1gFnSc2
Leutersdorf	Leutersdorf	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Jednolodní	jednolodní	k2eAgInSc1d1
neorientovaný	orientovaný	k2eNgInSc1d1
kostel	kostel	k1gInSc1
je	být	k5eAaImIp3nS
postavený	postavený	k2eAgInSc1d1
v	v	k7c6
novogotickém	novogotický	k2eAgInSc6d1
slohu	sloh	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průčelí	průčelí	k1gNnPc4
dominuje	dominovat	k5eAaImIp3nS
věž	věž	k1gFnSc1
s	s	k7c7
portálem	portál	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Střecha	střecha	k1gFnSc1
pokrytá	pokrytý	k2eAgFnSc1d1
pálenými	pálený	k2eAgFnPc7d1
taškami	taška	k1gFnPc7
nese	nést	k5eAaImIp3nS
barevné	barevný	k2eAgInPc1d1
vzory	vzor	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
oltáři	oltář	k1gInSc6
je	být	k5eAaImIp3nS
umístěn	umístěn	k2eAgInSc1d1
obraz	obraz	k1gInSc1
s	s	k7c7
motivem	motiv	k1gInSc7
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Varhany	varhany	k1gInPc7
se	se	k3xPyFc4
17	#num#	k4
rejstříky	rejstřík	k1gInPc7
zhotovil	zhotovit	k5eAaPmAgMnS
budyšínský	budyšínský	k2eAgMnSc1d1
varhanář	varhanář	k1gMnSc1
Leopold	Leopold	k1gMnSc1
Kohl	Kohl	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Okolí	okolí	k1gNnSc1
kostela	kostel	k1gInSc2
</s>
<s>
Kolem	kolem	k7c2
kostela	kostel	k1gInSc2
se	se	k3xPyFc4
rozprostírá	rozprostírat	k5eAaImIp3nS
hřbitov	hřbitov	k1gInSc1
vystavěný	vystavěný	k2eAgInSc1d1
spolu	spolu	k6eAd1
s	s	k7c7
kostelem	kostel	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Západně	západně	k6eAd1
od	od	k7c2
kostela	kostel	k1gInSc2
stojí	stát	k5eAaImIp3nS
památkově	památkově	k6eAd1
chráněná	chráněný	k2eAgFnSc1d1
fara	fara	k1gFnSc1
s	s	k7c7
pamětní	pamětní	k2eAgFnSc7d1
deskou	deska	k1gFnSc7
připomínající	připomínající	k2eAgMnPc4d1
faráře	farář	k1gMnPc4
Aloyse	Aloyse	k1gFnSc2
Scholze	Scholze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc6
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
v	v	k7c6
Leutersdorfu	Leutersdorf	k1gInSc6
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
farnosti	farnost	k1gFnSc2
</s>
<s>
150	#num#	k4
Jahre	Jahr	k1gInSc5
katholische	katholische	k1gInSc1
Kirche	Kirch	k1gInPc1
"	"	kIx"
<g/>
Mariä	Mariä	k1gFnSc1
Himmelfahrt	Himmelfahrta	k1gFnPc2
<g/>
"	"	kIx"
in	in	k?
Leutersdorf	Leutersdorf	k1gInSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
