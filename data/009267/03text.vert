<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Sýrie	Sýrie	k1gFnSc2	Sýrie
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
<g/>
:	:	kIx,	:
červený	červený	k2eAgInSc4d1	červený
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc4d1	bílý
a	a	k8xC	a
černý	černý	k2eAgInSc4d1	černý
a	a	k8xC	a
v	v	k7c6	v
bílém	bílý	k2eAgInSc6d1	bílý
pruhu	pruh	k1gInSc6	pruh
dvě	dva	k4xCgFnPc1	dva
zelené	zelená	k1gFnPc1	zelená
pěticípé	pěticípý	k2eAgFnPc1d1	pěticípá
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
boje	boj	k1gInSc2	boj
a	a	k8xC	a
obětí	oběť	k1gFnPc2	oběť
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
znamená	znamenat	k5eAaImIp3nS	znamenat
mír	mír	k1gInSc4	mír
<g/>
,	,	kIx,	,
černá	černat	k5eAaImIp3nS	černat
chmurnou	chmurný	k2eAgFnSc4d1	chmurná
koloniální	koloniální	k2eAgFnSc4d1	koloniální
minulost	minulost	k1gFnSc4	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
zelené	zelený	k2eAgFnPc1d1	zelená
hvězdy	hvězda	k1gFnPc1	hvězda
představovaly	představovat	k5eAaImAgFnP	představovat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
vzniku	vznik	k1gInSc2	vznik
vlajky	vlajka	k1gFnSc2	vlajka
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
dva	dva	k4xCgInPc4	dva
členy	člen	k1gInPc4	člen
(	(	kIx(	(
<g/>
Sýrii	Sýrie	k1gFnSc3	Sýrie
a	a	k8xC	a
Egypt	Egypt	k1gInSc1	Egypt
<g/>
)	)	kIx)	)
Sjednocené	sjednocený	k2eAgFnSc2d1	sjednocená
arabské	arabský	k2eAgFnSc2d1	arabská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Sýrie	Sýrie	k1gFnSc2	Sýrie
bylo	být	k5eAaImAgNnS	být
osídleno	osídlit	k5eAaPmNgNnS	osídlit
již	již	k6eAd1	již
asi	asi	k9	asi
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
tak	tak	k6eAd1	tak
k	k	k7c3	k
nejstarším	starý	k2eAgFnPc3d3	nejstarší
kulturním	kulturní	k2eAgFnPc3d1	kulturní
oblastem	oblast	k1gFnPc3	oblast
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2500	[number]	k4	2500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zde	zde	k6eAd1	zde
existovala	existovat	k5eAaImAgFnS	existovat
civilizace	civilizace	k1gFnSc1	civilizace
s	s	k7c7	s
centrem	centrum	k1gNnSc7	centrum
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Ebla	Ebl	k1gInSc2	Ebl
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
na	na	k7c6	na
dávné	dávný	k2eAgFnSc6d1	dávná
cestě	cesta	k1gFnSc6	cesta
mezi	mezi	k7c7	mezi
Mezopotámií	Mezopotámie	k1gFnSc7	Mezopotámie
a	a	k8xC	a
Středozemním	středozemní	k2eAgNnSc7d1	středozemní
mořem	moře	k1gNnSc7	moře
často	často	k6eAd1	často
pod	pod	k7c7	pod
nadvládou	nadvláda	k1gFnSc7	nadvláda
cizích	cizí	k2eAgFnPc2d1	cizí
mocností	mocnost	k1gFnPc2	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vystřídala	vystřídat	k5eAaPmAgFnS	vystřídat
Chetitská	chetitský	k2eAgFnSc1d1	Chetitská
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
Asýrie	Asýrie	k1gFnSc1	Asýrie
<g/>
,	,	kIx,	,
Babylonie	Babylonie	k1gFnSc1	Babylonie
<g/>
,	,	kIx,	,
Persie	Persie	k1gFnSc1	Persie
<g/>
,	,	kIx,	,
Arabové	Arab	k1gMnPc1	Arab
<g/>
,	,	kIx,	,
Alexandr	Alexandr	k1gMnSc1	Alexandr
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
,	,	kIx,	,
Římané	Říman	k1gMnPc1	Říman
<g/>
,	,	kIx,	,
Byzantská	byzantský	k2eAgFnSc1d1	byzantská
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
Fátimovský	fátimovský	k2eAgInSc1d1	fátimovský
chalífát	chalífát	k1gInSc1	chalífát
<g/>
,	,	kIx,	,
Seldžucká	Seldžucký	k2eAgFnSc1d1	Seldžucká
říše	říše	k1gFnSc1	říše
a	a	k8xC	a
Mamlúcký	mamlúcký	k2eAgInSc1d1	mamlúcký
sultanát	sultanát	k1gInSc1	sultanát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1516	[number]	k4	1516
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
připojeno	připojit	k5eAaPmNgNnS	připojit
k	k	k7c3	k
Osmanské	osmanský	k2eAgFnSc3d1	Osmanská
říši	říš	k1gFnSc3	říš
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
vlajky	vlajka	k1gFnPc1	vlajka
byly	být	k5eAaImAgFnP	být
prvními	první	k4xOgFnPc7	první
státními	státní	k2eAgFnPc7d1	státní
vlajkami	vlajka	k1gFnPc7	vlajka
vyvěšovanými	vyvěšovaný	k2eAgFnPc7d1	vyvěšovaná
na	na	k7c4	na
území	území	k1gNnSc4	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1844	[number]	k4	1844
byly	být	k5eAaImAgFnP	být
vlajky	vlajka	k1gFnPc1	vlajka
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
tvořeny	tvořen	k2eAgMnPc4d1	tvořen
červeným	červený	k2eAgInSc7d1	červený
listem	list	k1gInSc7	list
s	s	k7c7	s
bílým	bílý	k2eAgInSc7d1	bílý
půlměsícem	půlměsíc	k1gInSc7	půlměsíc
a	a	k8xC	a
bílou	bílý	k2eAgFnSc7d1	bílá
osmicípou	osmicípý	k2eAgFnSc7d1	osmicípá
hvězdou	hvězda	k1gFnSc7	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Známa	znám	k2eAgFnSc1d1	známa
je	být	k5eAaImIp3nS	být
i	i	k9	i
varianta	varianta	k1gFnSc1	varianta
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
půlměsíci	půlměsíc	k1gInPc7	půlměsíc
(	(	kIx(	(
<g/>
nejsou	být	k5eNaImIp3nP	být
obrázky	obrázek	k1gInPc4	obrázek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1844	[number]	k4	1844
byla	být	k5eAaImAgFnS	být
hvězda	hvězda	k1gFnSc1	hvězda
změněna	změnit	k5eAaPmNgFnS	změnit
na	na	k7c4	na
pěticípou	pěticípý	k2eAgFnSc4d1	pěticípá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
I.	I.	kA	I.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byla	být	k5eAaImAgFnS	být
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1918	[number]	k4	1918
Sýrie	Sýrie	k1gFnSc2	Sýrie
obsazena	obsadit	k5eAaPmNgFnS	obsadit
hášimovským	hášimovský	k2eAgNnSc7d1	hášimovské
vojskem	vojsko	k1gNnSc7	vojsko
z	z	k7c2	z
Hidžázu	Hidžáz	k1gInSc2	Hidžáz
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hidžázskou	hidžázský	k2eAgFnSc4d1	hidžázský
vlajku	vlajka	k1gFnSc4	vlajka
tvořily	tvořit	k5eAaImAgInP	tvořit
tři	tři	k4xCgInPc1	tři
vodorovné	vodorovný	k2eAgInPc1d1	vodorovný
pruhy	pruh	k1gInPc1	pruh
(	(	kIx(	(
<g/>
černý	černý	k2eAgInSc1d1	černý
<g/>
,	,	kIx,	,
zelený	zelený	k2eAgInSc1d1	zelený
a	a	k8xC	a
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
)	)	kIx)	)
a	a	k8xC	a
červený	červený	k2eAgInSc4d1	červený
klín	klín	k1gInSc4	klín
sahající	sahající	k2eAgInPc4d1	sahající
do	do	k7c2	do
třetiny	třetina	k1gFnSc2	třetina
délky	délka	k1gFnSc2	délka
listu	list	k1gInSc2	list
<g/>
.	.	kIx.	.
</s>
<s>
Vlajku	vlajka	k1gFnSc4	vlajka
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
uváděn	uváděn	k2eAgInSc1d1	uváděn
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
i	i	k9	i
poměr	poměr	k1gInSc1	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
v	v	k7c6	v
panarabských	panarabský	k2eAgFnPc6d1	panarabská
barvách	barva	k1gFnPc6	barva
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
hidžázský	hidžázský	k2eAgMnSc1d1	hidžázský
král	král	k1gMnSc1	král
Husajn	Husajn	k1gMnSc1	Husajn
ibn	ibn	k?	ibn
Alí	Alí	k1gMnSc1	Alí
al-Hášimí	al-Hášimit	k5eAaPmIp3nS	al-Hášimit
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
tuto	tento	k3xDgFnSc4	tento
vlajku	vlajka	k1gFnSc4	vlajka
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
pracovník	pracovník	k1gMnSc1	pracovník
britského	britský	k2eAgNnSc2d1	Britské
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
Sir	sir	k1gMnSc1	sir
Mark	Mark	k1gMnSc1	Mark
Sykes	Sykes	k1gMnSc1	Sykes
a	a	k8xC	a
král	král	k1gMnSc1	král
Husajn	Husajn	k1gMnSc1	Husajn
pouze	pouze	k6eAd1	pouze
změnil	změnit	k5eAaPmAgMnS	změnit
odstín	odstín	k1gInSc4	odstín
červeného	červený	k2eAgInSc2d1	červený
klínu	klín	k1gInSc2	klín
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
symbolizovaly	symbolizovat	k5eAaImAgFnP	symbolizovat
historicky	historicky	k6eAd1	historicky
významné	významný	k2eAgFnPc1d1	významná
arabské	arabský	k2eAgFnPc1d1	arabská
dynastie	dynastie	k1gFnPc1	dynastie
<g/>
:	:	kIx,	:
černá	černý	k2eAgFnSc1d1	černá
Abbásovce	Abbásovec	k1gInPc4	Abbásovec
<g/>
,	,	kIx,	,
zelená	zelená	k1gFnSc1	zelená
Fátimovce	Fátimovec	k1gInSc2	Fátimovec
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
Umajjovce	Umajjovka	k1gFnSc3	Umajjovka
a	a	k8xC	a
červená	červená	k1gFnSc1	červená
Hášimovce	Hášimovec	k1gInSc2	Hášimovec
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
obsadila	obsadit	k5eAaPmAgFnS	obsadit
syrské	syrský	k2eAgNnSc4d1	syrské
pobřeží	pobřeží	k1gNnSc4	pobřeží
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
protifrancouzský	protifrancouzský	k2eAgInSc1d1	protifrancouzský
odboj	odboj	k1gInSc1	odboj
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1920	[number]	k4	1920
bylo	být	k5eAaImAgNnS	být
vyhlášeno	vyhlásit	k5eAaPmNgNnS	vyhlásit
Syrské	syrský	k2eAgNnSc1d1	syrské
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
nárokující	nárokující	k2eAgFnSc1d1	nárokující
si	se	k3xPyFc3	se
území	území	k1gNnSc4	území
Velké	velký	k2eAgFnSc2d1	velká
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlajkou	vlajka	k1gFnSc7	vlajka
zůstala	zůstat	k5eAaPmAgFnS	zůstat
hidžázská	hidžázský	k2eAgFnSc1d1	hidžázský
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
mnohých	mnohý	k2eAgInPc2d1	mnohý
zdrojů	zdroj	k1gInPc2	zdroj
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
poměr	poměr	k1gInSc1	poměr
stran	strana	k1gFnPc2	strana
upraven	upravit	k5eAaPmNgInS	upravit
na	na	k7c4	na
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
a	a	k8xC	a
do	do	k7c2	do
červeného	červený	k2eAgInSc2d1	červený
klínu	klín	k1gInSc2	klín
přibyla	přibýt	k5eAaPmAgFnS	přibýt
bílá	bílý	k2eAgFnSc1d1	bílá
sedmicípá	sedmicípý	k2eAgFnSc1d1	sedmicípá
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
<g/>
Již	již	k6eAd1	již
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
bylo	být	k5eAaImAgNnS	být
syrské	syrský	k2eAgNnSc1d1	syrské
vojsko	vojsko	k1gNnSc1	vojsko
poraženo	poražen	k2eAgNnSc1d1	poraženo
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Damašek	Damašek	k1gInSc4	Damašek
bylo	být	k5eAaImAgNnS	být
obsazeno	obsadit	k5eAaPmNgNnS	obsadit
francouzským	francouzský	k2eAgNnSc7d1	francouzské
vojskem	vojsko	k1gNnSc7	vojsko
a	a	k8xC	a
král	král	k1gMnSc1	král
Fajsal	Fajsal	k1gFnSc2	Fajsal
I.	I.	kA	I.
byl	být	k5eAaImAgInS	být
přinucen	přinucen	k2eAgInSc1d1	přinucen
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
Iráku	Irák	k1gInSc2	Irák
(	(	kIx(	(
<g/>
zajímavostí	zajímavost	k1gFnPc2	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
letech	let	k1gInPc6	let
1921	[number]	k4	1921
<g/>
–	–	k?	–
<g/>
1933	[number]	k4	1933
i	i	k8xC	i
tamním	tamní	k2eAgMnSc7d1	tamní
králem	král	k1gMnSc7	král
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
užívat	užívat	k5eAaImF	užívat
nová	nový	k2eAgFnSc1d1	nová
vlajka	vlajka	k1gFnSc1	vlajka
francouzského	francouzský	k2eAgNnSc2d1	francouzské
mandátního	mandátní	k2eAgNnSc2d1	mandátní
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
světle	světle	k6eAd1	světle
modrým	modrý	k2eAgInSc7d1	modrý
listem	list	k1gInSc7	list
s	s	k7c7	s
uprostřed	uprostřed	k6eAd1	uprostřed
umístěným	umístěný	k2eAgInSc7d1	umístěný
bílým	bílý	k2eAgInSc7d1	bílý
půlměsícem	půlměsíc	k1gInSc7	půlměsíc
s	s	k7c7	s
cípy	cíp	k1gInPc7	cíp
směřujícími	směřující	k2eAgInPc7d1	směřující
k	k	k7c3	k
vlajícímu	vlající	k2eAgInSc3d1	vlající
okraji	okraj	k1gInSc3	okraj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
malém	malý	k2eAgInSc6d1	malý
kantonu	kanton	k1gInSc6	kanton
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
francouzská	francouzský	k2eAgFnSc1d1	francouzská
trikolora	trikolora	k1gFnSc1	trikolora
<g/>
.1	.1	k4	.1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
získala	získat	k5eAaPmAgFnS	získat
Francie	Francie	k1gFnSc1	Francie
mandát	mandát	k1gInSc4	mandát
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
nad	nad	k7c7	nad
územím	území	k1gNnSc7	území
Sýrie	Sýrie	k1gFnSc2	Sýrie
a	a	k8xC	a
Libanonu	Libanon	k1gInSc2	Libanon
<g/>
.	.	kIx.	.
</s>
<s>
Vlajky	vlajka	k1gFnPc1	vlajka
autonomních	autonomní	k2eAgInPc2d1	autonomní
států	stát	k1gInPc2	stát
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
níže	nízce	k6eAd2	nízce
<g/>
.28	.28	k4	.28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1922	[number]	k4	1922
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Syrská	syrský	k2eAgFnSc1d1	Syrská
federace	federace	k1gFnSc1	federace
<g/>
.	.	kIx.	.
</s>
<s>
Jejími	její	k3xOp3gInPc7	její
členy	člen	k1gInPc7	člen
byly	být	k5eAaImAgFnP	být
autonomní	autonomní	k2eAgInPc4d1	autonomní
státy	stát	k1gInPc4	stát
Aleppo	Aleppa	k1gFnSc5	Aleppa
<g/>
,	,	kIx,	,
Damašek	Damašek	k1gInSc4	Damašek
a	a	k8xC	a
autonomní	autonomní	k2eAgNnSc4d1	autonomní
Alavitské	Alavitský	k2eAgNnSc4d1	Alavitský
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc7d1	státní
vlajkou	vlajka	k1gFnSc7	vlajka
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
list	list	k1gInSc1	list
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
<g/>
:	:	kIx,	:
zeleným	zelený	k2eAgInSc7d1	zelený
<g/>
,	,	kIx,	,
bílým	bílý	k2eAgInSc7d1	bílý
a	a	k8xC	a
zeleným	zelený	k2eAgInSc7d1	zelený
<g/>
,	,	kIx,	,
s	s	k7c7	s
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
vlajkou	vlajka	k1gFnSc7	vlajka
v	v	k7c6	v
kantonu	kanton	k1gInSc6	kanton
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
již	již	k6eAd1	již
zabírající	zabírající	k2eAgInPc4d1	zabírající
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
4	[number]	k4	4
listu	list	k1gInSc2	list
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
federace	federace	k1gFnSc1	federace
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
a	a	k8xC	a
vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1925	[number]	k4	1925
státní	státní	k2eAgFnSc7d1	státní
vlajkou	vlajka	k1gFnSc7	vlajka
Syrského	syrský	k2eAgInSc2d1	syrský
státu	stát	k1gInSc2	stát
zahrnující	zahrnující	k2eAgNnSc1d1	zahrnující
území	území	k1gNnSc1	území
Aleppa	Alepp	k1gMnSc2	Alepp
a	a	k8xC	a
Damašku	Damašek	k1gInSc2	Damašek
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
se	se	k3xPyFc4	se
francouzský	francouzský	k2eAgInSc1d1	francouzský
kanton	kanton	k1gInSc1	kanton
změnil	změnit	k5eAaPmAgInS	změnit
na	na	k7c4	na
menší	malý	k2eAgNnSc4d2	menší
karé	karé	k1gNnSc4	karé
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
plochy	plocha	k1gFnPc4	plocha
listu	list	k1gInSc2	list
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1930	[number]	k4	1930
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
pod	pod	k7c7	pod
francouzským	francouzský	k2eAgInSc7d1	francouzský
mandátem	mandát	k1gInSc7	mandát
<g/>
,	,	kIx,	,
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
Syrská	syrský	k2eAgFnSc1d1	Syrská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
měla	mít	k5eAaImAgFnS	mít
tři	tři	k4xCgInPc4	tři
pruhy	pruh	k1gInPc4	pruh
<g/>
:	:	kIx,	:
zelený	zelený	k2eAgInSc4d1	zelený
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc4d1	bílý
a	a	k8xC	a
černý	černý	k2eAgInSc4d1	černý
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
bílého	bílý	k2eAgInSc2d1	bílý
pruhu	pruh	k1gInSc2	pruh
byly	být	k5eAaImAgFnP	být
tři	tři	k4xCgFnPc1	tři
červené	červený	k2eAgFnPc1d1	červená
pěticípé	pěticípý	k2eAgFnPc1d1	pěticípá
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
byly	být	k5eAaImAgFnP	být
poprvé	poprvé	k6eAd1	poprvé
vztyčena	vztyčen	k2eAgFnSc1d1	vztyčena
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1932	[number]	k4	1932
v	v	k7c6	v
Aleppu	Alepp	k1gInSc6	Alepp
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
však	však	k9	však
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
až	až	k6eAd1	až
11	[number]	k4	11
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1932	[number]	k4	1932
a	a	k8xC	a
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
poprvé	poprvé	k6eAd1	poprvé
vztyčena	vztyčen	k2eAgFnSc1d1	vztyčena
v	v	k7c6	v
Damašku	Damašek	k1gInSc6	Damašek
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
hvězdy	hvězda	k1gFnPc1	hvězda
symbolizovaly	symbolizovat	k5eAaImAgFnP	symbolizovat
administrativní	administrativní	k2eAgFnPc1d1	administrativní
jednotky	jednotka	k1gFnPc1	jednotka
(	(	kIx(	(
<g/>
vilájety	vilájet	k2eAgFnPc1d1	vilájet
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Damašek	Damašek	k1gInSc1	Damašek
<g/>
,	,	kIx,	,
Aleppo	Aleppa	k1gFnSc5	Aleppa
a	a	k8xC	a
Dajr	Dajr	k1gMnSc1	Dajr
az-Zór	az-Zór	k1gMnSc1	az-Zór
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgFnP	následovat
události	událost	k1gFnPc1	událost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
názvy	název	k1gInPc4	název
státu	stát	k1gInSc2	stát
či	či	k8xC	či
jeho	jeho	k3xOp3gInSc1	jeho
statut	statut	k1gInSc1	statut
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1939	[number]	k4	1939
–	–	k?	–
změna	změna	k1gFnSc1	změna
názvu	název	k1gInSc2	název
na	na	k7c4	na
Stát	stát	k1gInSc4	stát
Sýrie	Sýrie	k1gFnSc2	Sýrie
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1940	[number]	k4	1940
–	–	k?	–
pod	pod	k7c7	pod
koloniální	koloniální	k2eAgFnSc7d1	koloniální
správou	správa	k1gFnSc7	správa
vichistické	vichistický	k2eAgFnSc2d1	vichistická
Francie	Francie	k1gFnSc2	Francie
</s>
</p>
<p>
<s>
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1941	[number]	k4	1941
–	–	k?	–
obsazena	obsadit	k5eAaPmNgFnS	obsadit
společným	společný	k2eAgNnSc7d1	společné
britským	britský	k2eAgNnSc7d1	Britské
a	a	k8xC	a
francouzským	francouzský	k2eAgNnSc7d1	francouzské
vojskem	vojsko	k1gNnSc7	vojsko
</s>
</p>
<p>
<s>
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1941	[number]	k4	1941
–	–	k?	–
nominální	nominální	k2eAgNnSc4d1	nominální
vyhlášení	vyhlášení	k1gNnSc4	vyhlášení
nezávisle	závisle	k6eNd1	závisle
Syrské	syrský	k2eAgFnSc2d1	Syrská
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1941	[number]	k4	1941
–	–	k?	–
zrušen	zrušen	k2eAgInSc1d1	zrušen
francouzský	francouzský	k2eAgInSc1d1	francouzský
mandát	mandát	k1gInSc1	mandát
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1944	[number]	k4	1944
–	–	k?	–
deklarována	deklarován	k2eAgFnSc1d1	deklarována
nezávislost	nezávislost	k1gFnSc1	nezávislost
na	na	k7c6	na
Francii	Francie	k1gFnSc6	Francie
</s>
</p>
<p>
<s>
1945	[number]	k4	1945
–	–	k?	–
pokus	pokus	k1gInSc1	pokus
Francie	Francie	k1gFnSc2	Francie
o	o	k7c6	o
obnovení	obnovení	k1gNnSc6	obnovení
koloniální	koloniální	k2eAgFnSc2d1	koloniální
závislosti	závislost	k1gFnSc2	závislost
</s>
</p>
<p>
<s>
1946	[number]	k4	1946
–	–	k?	–
odchod	odchod	k1gInSc1	odchod
britských	britský	k2eAgInPc2d1	britský
a	a	k8xC	a
francouzských	francouzský	k2eAgInPc2d1	francouzský
vojsk	vojsko	k1gNnPc2	vojsko
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1946	[number]	k4	1946
–	–	k?	–
nezávislost	nezávislost	k1gFnSc4	nezávislost
Sýrie	Sýrie	k1gFnSc2	Sýrie
de	de	k?	de
factoPo	factoPo	k1gMnSc1	factoPo
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
však	však	k9	však
byla	být	k5eAaImAgFnS	být
zachována	zachovat	k5eAaPmNgFnS	zachovat
vlajka	vlajka	k1gFnSc1	vlajka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1932.1	[number]	k4	1932.1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1958	[number]	k4	1958
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
spojením	spojení	k1gNnSc7	spojení
Egypta	Egypt	k1gInSc2	Egypt
a	a	k8xC	a
Sýrie	Sýrie	k1gFnSc1	Sýrie
Sjednocená	sjednocený	k2eAgFnSc1d1	sjednocená
arabská	arabský	k2eAgFnSc1d1	arabská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
byla	být	k5eAaImAgFnS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
vlajka	vlajka	k1gFnSc1	vlajka
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
egyptské	egyptský	k2eAgFnSc2d1	egyptská
vlajky	vlajka	k1gFnSc2	vlajka
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
<g/>
:	:	kIx,	:
červeným	červený	k2eAgInSc7d1	červený
<g/>
,	,	kIx,	,
bílým	bílý	k2eAgInSc7d1	bílý
a	a	k8xC	a
černým	černý	k2eAgMnSc7d1	černý
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
bílého	bílý	k2eAgInSc2d1	bílý
pruhu	pruh	k1gInSc2	pruh
byly	být	k5eAaImAgFnP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
dvě	dva	k4xCgFnPc1	dva
zelené	zelený	k2eAgFnPc1d1	zelená
pěticípé	pěticípý	k2eAgFnPc1d1	pěticípá
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
symbolizující	symbolizující	k2eAgInPc1d1	symbolizující
oba	dva	k4xCgInPc1	dva
členy	člen	k1gInPc1	člen
federace	federace	k1gFnSc2	federace
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1958	[number]	k4	1958
–	–	k?	–
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
existovala	existovat	k5eAaImAgFnS	existovat
(	(	kIx(	(
<g/>
spíše	spíše	k9	spíše
na	na	k7c6	na
papíře	papír	k1gInSc6	papír
<g/>
)	)	kIx)	)
i	i	k8xC	i
konfederace	konfederace	k1gFnSc2	konfederace
Sjednocené	sjednocený	k2eAgInPc1d1	sjednocený
arabské	arabský	k2eAgInPc1d1	arabský
státy	stát	k1gInPc1	stát
(	(	kIx(	(
<g/>
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
Sýrie	Sýrie	k1gFnSc1	Sýrie
a	a	k8xC	a
Jemen	Jemen	k1gInSc1	Jemen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1961	[number]	k4	1961
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
Syrská	syrský	k2eAgFnSc1d1	Syrská
arabská	arabský	k2eAgFnSc1d1	arabská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
federace	federace	k1gFnSc2	federace
byly	být	k5eAaImAgInP	být
následně	následně	k6eAd1	následně
znovupřijaty	znovupřijat	k2eAgInPc4d1	znovupřijat
státní	státní	k2eAgInPc4d1	státní
symboly	symbol	k1gInPc4	symbol
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1932.8	[number]	k4	1932.8
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1963	[number]	k4	1963
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
převratu	převrat	k1gInSc6	převrat
strany	strana	k1gFnSc2	strana
Baas	Baasa	k1gFnPc2	Baasa
zavedena	zavést	k5eAaPmNgFnS	zavést
nová	nový	k2eAgFnSc1d1	nová
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
měla	mít	k5eAaImAgFnS	mít
znovu	znovu	k6eAd1	znovu
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
egyptského	egyptský	k2eAgInSc2d1	egyptský
vzoru	vzor	k1gInSc2	vzor
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc4	tři
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
<g/>
:	:	kIx,	:
červený	červený	k2eAgInSc4d1	červený
<g/>
,	,	kIx,	,
bílý	bílý	k2eAgInSc4d1	bílý
a	a	k8xC	a
černý	černý	k2eAgInSc4d1	černý
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
bílého	bílý	k2eAgInSc2d1	bílý
pruhu	pruh	k1gInSc2	pruh
byly	být	k5eAaImAgFnP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
tři	tři	k4xCgFnPc1	tři
zelené	zelený	k2eAgFnPc1d1	zelená
pěticípé	pěticípý	k2eAgFnPc1d1	pěticípá
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
symbolizovaly	symbolizovat	k5eAaImAgFnP	symbolizovat
Egypt	Egypt	k1gInSc4	Egypt
<g/>
,	,	kIx,	,
Irák	Irák	k1gInSc4	Irák
a	a	k8xC	a
Sýrii	Sýrie	k1gFnSc4	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Černá	černý	k2eAgFnSc1d1	černá
barva	barva	k1gFnSc1	barva
symbolizovala	symbolizovat	k5eAaImAgFnS	symbolizovat
statečnost	statečnost	k1gFnSc4	statečnost
v	v	k7c6	v
boji	boj	k1gInSc6	boj
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
šlechetnost	šlechetnost	k1gFnSc1	šlechetnost
<g/>
,	,	kIx,	,
černá	černý	k2eAgFnSc1d1	černá
historická	historický	k2eAgFnSc1d1	historická
vítězství	vítězství	k1gNnSc1	vítězství
muslimů	muslim	k1gMnPc2	muslim
a	a	k8xC	a
zelená	zelený	k2eAgFnSc1d1	zelená
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgFnSc1d1	tradiční
barva	barva	k1gFnSc1	barva
islámu	islám	k1gInSc2	islám
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
federativnímu	federativní	k2eAgNnSc3d1	federativní
spojení	spojení	k1gNnSc3	spojení
sice	sice	k8xC	sice
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
vlajku	vlajka	k1gFnSc4	vlajka
si	se	k3xPyFc3	se
však	však	k9	však
Sýrie	Sýrie	k1gFnSc1	Sýrie
ponechala	ponechat	k5eAaPmAgFnS	ponechat
<g/>
.	.	kIx.	.
</s>
<s>
Irák	Irák	k1gInSc1	Irák
užíval	užívat	k5eAaImAgInS	užívat
tuto	tento	k3xDgFnSc4	tento
vlajku	vlajka	k1gFnSc4	vlajka
v	v	k7c6	v
letech	let	k1gInPc6	let
1963	[number]	k4	1963
<g/>
–	–	k?	–
<g/>
1991.2	[number]	k4	1991.2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1971	[number]	k4	1971
byla	být	k5eAaImAgFnS	být
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
Federace	federace	k1gFnSc1	federace
arabských	arabský	k2eAgFnPc2d1	arabská
republik	republika	k1gFnPc2	republika
(	(	kIx(	(
<g/>
Egypt	Egypt	k1gInSc1	Egypt
<g/>
,	,	kIx,	,
Libye	Libye	k1gFnSc1	Libye
a	a	k8xC	a
Sýrie	Sýrie	k1gFnSc1	Sýrie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1972	[number]	k4	1972
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
společná	společný	k2eAgFnSc1d1	společná
vlajka	vlajka	k1gFnSc1	vlajka
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
tvořená	tvořený	k2eAgFnSc1d1	tvořená
červenými	červený	k2eAgInPc7d1	červený
<g/>
,	,	kIx,	,
bílými	bílý	k2eAgInPc7d1	bílý
a	a	k8xC	a
černými	černý	k2eAgInPc7d1	černý
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
se	s	k7c7	s
znakem	znak	k1gInSc7	znak
federace	federace	k1gFnSc2	federace
uprostřed	uprostřed	k7c2	uprostřed
<g/>
.	.	kIx.	.
</s>
<s>
Znakem	znak	k1gInSc7	znak
byl	být	k5eAaImAgMnS	být
stylizovaný	stylizovaný	k2eAgMnSc1d1	stylizovaný
zlatý	zlatý	k2eAgMnSc1d1	zlatý
sokol	sokol	k1gMnSc1	sokol
<g/>
,	,	kIx,	,
heraldicky	heraldicky	k6eAd1	heraldicky
hledící	hledící	k2eAgInPc1d1	hledící
vlevo	vlevo	k6eAd1	vlevo
<g/>
.	.	kIx.	.
</s>
<s>
Sokol	Sokol	k1gMnSc1	Sokol
(	(	kIx(	(
<g/>
symbol	symbol	k1gInSc1	symbol
kmene	kmen	k1gInSc2	kmen
Kurajšovců	Kurajšovec	k1gMnPc2	Kurajšovec
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
pocházel	pocházet	k5eAaImAgMnS	pocházet
prorok	prorok	k1gMnSc1	prorok
Mohamed	Mohamed	k1gMnSc1	Mohamed
<g/>
)	)	kIx)	)
měl	mít	k5eAaImAgInS	mít
na	na	k7c6	na
hrudi	hruď	k1gFnSc6	hruď
prázdný	prázdný	k2eAgInSc1d1	prázdný
žlutý	žlutý	k2eAgInSc1d1	žlutý
štít	štít	k1gInSc1	štít
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
držel	držet	k5eAaImAgInS	držet
v	v	k7c6	v
pařátech	pařát	k1gInPc6	pařát
stuhu	stuha	k1gFnSc4	stuha
s	s	k7c7	s
arabským	arabský	k2eAgInSc7d1	arabský
textem	text	k1gInSc7	text
Federace	federace	k1gFnSc2	federace
arabských	arabský	k2eAgFnPc2d1	arabská
republik	republika	k1gFnPc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
stuhou	stuha	k1gFnSc7	stuha
měla	mít	k5eAaImAgFnS	mít
egyptská	egyptský	k2eAgFnSc1d1	egyptská
a	a	k8xC	a
libyjská	libyjský	k2eAgFnSc1d1	Libyjská
vlajka	vlajka	k1gFnSc1	vlajka
ještě	ještě	k6eAd1	ještě
název	název	k1gInSc1	název
svého	svůj	k3xOyFgInSc2	svůj
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
syrské	syrský	k2eAgInPc4d1	syrský
název	název	k1gInSc4	název
státu	stát	k1gInSc2	stát
chyběl	chybět	k5eAaImAgInS	chybět
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
symbolizovala	symbolizovat	k5eAaImAgFnS	symbolizovat
oběti	oběť	k1gFnPc4	oběť
v	v	k7c6	v
boji	boj	k1gInSc6	boj
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
bílá	bílý	k2eAgFnSc1d1	bílá
mír	mír	k1gInSc4	mír
a	a	k8xC	a
černá	černat	k5eAaImIp3nS	černat
chmurnou	chmurný	k2eAgFnSc4d1	chmurná
koloniální	koloniální	k2eAgFnSc4d1	koloniální
minulost	minulost	k1gFnSc4	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Sýrie	Sýrie	k1gFnSc1	Sýrie
si	se	k3xPyFc3	se
vlajku	vlajka	k1gFnSc4	vlajka
ponechala	ponechat	k5eAaPmAgFnS	ponechat
i	i	k9	i
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
federace	federace	k1gFnSc2	federace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
<g/>
Počátkem	počátkem	k7c2	počátkem
března	březen	k1gInSc2	březen
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
zahájil	zahájit	k5eAaPmAgInS	zahájit
na	na	k7c4	na
popud	popud	k1gInSc4	popud
syrského	syrský	k2eAgMnSc2d1	syrský
prezidenta	prezident	k1gMnSc2	prezident
Háfize	Háfize	k1gFnSc2	Háfize
Asada	Asado	k1gNnSc2	Asado
syrský	syrský	k2eAgInSc1d1	syrský
parlament	parlament	k1gInSc1	parlament
debatu	debata	k1gFnSc4	debata
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
bylo	být	k5eAaImAgNnS	být
navázání	navázání	k1gNnSc1	navázání
diplomatických	diplomatický	k2eAgInPc2d1	diplomatický
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
Egyptem	Egypt	k1gInSc7	Egypt
a	a	k8xC	a
Izraelem	Izrael	k1gInSc7	Izrael
(	(	kIx(	(
<g/>
Dohody	dohoda	k1gFnPc1	dohoda
z	z	k7c2	z
Camp	camp	k1gInSc4	camp
Davidu	David	k1gMnSc3	David
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1980	[number]	k4	1980
schválilo	schválit	k5eAaPmAgNnS	schválit
Lidové	lidový	k2eAgNnSc1d1	lidové
shromáždění	shromáždění	k1gNnSc1	shromáždění
Syrské	syrský	k2eAgFnSc2d1	Syrská
arabské	arabský	k2eAgFnSc2d1	arabská
republiky	republika	k1gFnSc2	republika
staronovou	staronový	k2eAgFnSc4d1	staronová
vlajku	vlajka	k1gFnSc4	vlajka
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1958	[number]	k4	1958
<g/>
–	–	k?	–
<g/>
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
platná	platný	k2eAgFnSc1d1	platná
dodnes	dodnes	k6eAd1	dodnes
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
funkci	funkce	k1gFnSc4	funkce
vlaky	vlak	k1gInPc1	vlak
státní	státní	k2eAgInPc1d1	státní
<g/>
,	,	kIx,	,
národní	národní	k2eAgInPc1d1	národní
<g/>
,	,	kIx,	,
válečné	válečný	k2eAgInPc1d1	válečný
i	i	k8xC	i
obchodní	obchodní	k2eAgInPc1d1	obchodní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
symbolem	symbol	k1gInSc7	symbol
odporu	odpor	k1gInSc2	odpor
proti	proti	k7c3	proti
stávající	stávající	k2eAgFnSc3d1	stávající
vládě	vláda	k1gFnSc3	vláda
stala	stát	k5eAaPmAgFnS	stát
vlajka	vlajka	k1gFnSc1	vlajka
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1932	[number]	k4	1932
<g/>
–	–	k?	–
<g/>
1958	[number]	k4	1958
a	a	k8xC	a
1961	[number]	k4	1961
<g/>
–	–	k?	–
<g/>
1963	[number]	k4	1963
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vlajky	vlajka	k1gFnPc4	vlajka
autonomních	autonomní	k2eAgInPc2d1	autonomní
států	stát	k1gInPc2	stát
za	za	k7c2	za
francouzského	francouzský	k2eAgInSc2d1	francouzský
mandátu	mandát	k1gInSc2	mandát
===	===	k?	===
</s>
</p>
<p>
<s>
Taktikou	taktika	k1gFnSc7	taktika
správy	správa	k1gFnSc2	správa
francouzského	francouzský	k2eAgInSc2d1	francouzský
mandátu	mandát	k1gInSc2	mandát
Sýrie	Sýrie	k1gFnSc2	Sýrie
a	a	k8xC	a
Libanonu	Libanon	k1gInSc2	Libanon
bylo	být	k5eAaImAgNnS	být
rozdělení	rozdělení	k1gNnSc1	rozdělení
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
podrobení	podrobení	k1gNnSc2	podrobení
a	a	k8xC	a
upevnění	upevnění	k1gNnSc2	upevnění
nadvlády	nadvláda	k1gFnSc2	nadvláda
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
proto	proto	k6eAd1	proto
čtyři	čtyři	k4xCgInPc4	čtyři
loutkové	loutkový	k2eAgInPc4d1	loutkový
<g/>
,	,	kIx,	,
autonomní	autonomní	k2eAgInPc4d1	autonomní
státy	stát	k1gInPc4	stát
<g/>
:	:	kIx,	:
Velký	velký	k2eAgInSc4d1	velký
Libanon	Libanon	k1gInSc4	Libanon
<g/>
,	,	kIx,	,
Aleppo	Aleppa	k1gFnSc5	Aleppa
<g/>
,	,	kIx,	,
Damašek	Damašek	k1gInSc4	Damašek
a	a	k8xC	a
Alavitské	Alavitský	k2eAgNnSc4d1	Alavitský
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1920	[number]	k4	1920
–	–	k?	–
Vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
autonomního	autonomní	k2eAgNnSc2d1	autonomní
Alavitského	Alavitský	k2eAgNnSc2d1	Alavitský
území	území	k1gNnSc2	území
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1920	[number]	k4	1920
–	–	k?	–
Vyhlášení	vyhlášení	k1gNnSc1	vyhlášení
autonomního	autonomní	k2eAgInSc2d1	autonomní
státu	stát	k1gInSc2	stát
Aleppo	Aleppa	k1gFnSc5	Aleppa
a	a	k8xC	a
autonomního	autonomní	k2eAgInSc2d1	autonomní
státu	stát	k1gInSc2	stát
Damašek	Damašek	k1gInSc1	Damašek
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1921	[number]	k4	1921
–	–	k?	–
Vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
autonomního	autonomní	k2eAgNnSc2d1	autonomní
území	území	k1gNnSc2	území
Soueida	Soueid	k1gMnSc2	Soueid
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1922	[number]	k4	1922
–	–	k?	–
Autonomní	autonomní	k2eAgNnSc4d1	autonomní
území	území	k1gNnSc4	území
Soueida	Soueid	k1gMnSc2	Soueid
bylo	být	k5eAaImAgNnS	být
prohlášeno	prohlásit	k5eAaPmNgNnS	prohlásit
autonomním	autonomní	k2eAgInSc7d1	autonomní
státem	stát	k1gInSc7	stát
Soueida	Soueid	k1gMnSc2	Soueid
</s>
</p>
<p>
<s>
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1922	[number]	k4	1922
–	–	k?	–
Autonomní	autonomní	k2eAgInSc4d1	autonomní
stát	stát	k1gInSc4	stát
Aleppo	Aleppa	k1gFnSc5	Aleppa
a	a	k8xC	a
autonomní	autonomní	k2eAgNnSc1d1	autonomní
stát	stát	k5eAaPmF	stát
Damašek	Damašek	k1gInSc4	Damašek
byly	být	k5eAaImAgInP	být
začleněny	začleněn	k2eAgInPc1d1	začleněn
do	do	k7c2	do
Syrské	syrský	k2eAgFnSc2d1	Syrská
federace	federace	k1gFnSc2	federace
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1922	[number]	k4	1922
–	–	k?	–
Autonomní	autonomní	k2eAgNnSc1d1	autonomní
Alavitské	Alavitský	k2eAgNnSc1d1	Alavitský
území	území	k1gNnSc1	území
bylo	být	k5eAaImAgNnS	být
začleněno	začlenit	k5eAaPmNgNnS	začlenit
do	do	k7c2	do
Syrské	syrský	k2eAgFnSc2d1	Syrská
federace	federace	k1gFnSc2	federace
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1924	[number]	k4	1924
–	–	k?	–
Sloučení	sloučení	k1gNnSc2	sloučení
Aleppa	Alepp	k1gMnSc2	Alepp
s	s	k7c7	s
Damaškem	Damašek	k1gInSc7	Damašek
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1925	[number]	k4	1925
–	–	k?	–
Aleppo	Aleppa	k1gFnSc5	Aleppa
s	s	k7c7	s
Damaškem	Damašek	k1gInSc7	Damašek
součástí	součást	k1gFnPc2	součást
Syrského	syrský	k2eAgInSc2d1	syrský
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
Alavitské	Alavitský	k2eAgNnSc1d1	Alavitský
území	území	k1gNnSc1	území
bylo	být	k5eAaImAgNnS	být
prohlášeno	prohlásit	k5eAaPmNgNnS	prohlásit
Alavitským	Alavitský	k2eAgInSc7d1	Alavitský
státem	stát	k1gInSc7	stát
(	(	kIx(	(
<g/>
nestalo	stát	k5eNaPmAgNnS	stát
se	se	k3xPyFc4	se
však	však	k9	však
součástí	součást	k1gFnSc7	součást
Syrského	syrský	k2eAgInSc2d1	syrský
státu	stát	k1gInSc2	stát
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1927	[number]	k4	1927
–	–	k?	–
Autonomní	autonomní	k2eAgInSc1d1	autonomní
stát	stát	k1gInSc1	stát
Soueida	Soueid	k1gMnSc2	Soueid
byl	být	k5eAaImAgInS	být
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c4	na
Džebel	Džebel	k1gInSc4	Džebel
Drúz	drúza	k1gFnPc2	drúza
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1930	[number]	k4	1930
–	–	k?	–
Alavitský	Alavitský	k2eAgInSc1d1	Alavitský
stát	stát	k1gInSc1	stát
byl	být	k5eAaImAgInS	být
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c4	na
stát	stát	k1gInSc4	stát
Latakie	Latakie	k1gFnSc2	Latakie
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1936	[number]	k4	1936
–	–	k?	–
Autonomní	autonomní	k2eAgInSc1d1	autonomní
stát	stát	k1gInSc1	stát
Džebel	Džebela	k1gFnPc2	Džebela
Drúz	drúza	k1gFnPc2	drúza
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
součástí	součást	k1gFnSc7	součást
Sýrie	Sýrie	k1gFnSc2	Sýrie
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1936	[number]	k4	1936
–	–	k?	–
Stát	stát	k1gInSc1	stát
Latakie	Latakie	k1gFnSc2	Latakie
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
součástí	součást	k1gFnSc7	součást
Sýrie	Sýrie	k1gFnSc2	Sýrie
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Sýrie	Sýrie	k1gFnSc2	Sýrie
</s>
</p>
<p>
<s>
Syrská	syrský	k2eAgFnSc1d1	Syrská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Syrská	syrský	k2eAgFnSc1d1	Syrská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
