<s>
Eric	Eric	k1gInSc1	Eric
Patrick	Patrick	k1gInSc1	Patrick
Clapton	Clapton	k1gInSc4	Clapton
CBE	CBE	kA	CBE
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1945	[number]	k4	1945
v	v	k7c4	v
Ripley	Ripley	k1gInPc4	Ripley
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přezdívaný	přezdívaný	k2eAgInSc1d1	přezdívaný
Slowhand	Slowhand	k1gInSc1	Slowhand
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
pomalá	pomalý	k2eAgFnSc1d1	pomalá
ruka	ruka	k1gFnSc1	ruka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
britský	britský	k2eAgMnSc1d1	britský
kytarista	kytarista	k1gMnSc1	kytarista
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
světových	světový	k2eAgMnPc2d1	světový
bluesových	bluesový	k2eAgMnPc2d1	bluesový
kytaristů	kytarista	k1gMnPc2	kytarista
<g/>
.	.	kIx.	.
</s>
<s>
Eric	Eric	k6eAd1	Eric
Clapton	Clapton	k1gInSc1	Clapton
byl	být	k5eAaImAgInS	být
vychováván	vychovávat	k5eAaImNgInS	vychovávat
svými	svůj	k3xOyFgMnPc7	svůj
prarodiči	prarodič	k1gMnPc7	prarodič
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yIgInPc6	který
si	se	k3xPyFc3	se
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gMnPc7	jeho
pravými	pravý	k2eAgMnPc7d1	pravý
rodiči	rodič	k1gMnPc7	rodič
a	a	k8xC	a
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
matka	matka	k1gFnSc1	matka
jen	jen	k9	jen
sestrou	sestra	k1gFnSc7	sestra
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
kanadský	kanadský	k2eAgMnSc1d1	kanadský
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
muzikant	muzikant	k1gMnSc1	muzikant
Edward	Edward	k1gMnSc1	Edward
Fryer	Fryer	k1gMnSc1	Fryer
<g/>
.	.	kIx.	.
</s>
<s>
Clapton	Clapton	k1gInSc1	Clapton
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
nikdy	nikdy	k6eAd1	nikdy
nesetkal	setkat	k5eNaPmAgMnS	setkat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
napsal	napsat	k5eAaPmAgMnS	napsat
o	o	k7c6	o
něm	on	k3xPp3gMnSc6	on
alespoň	alespoň	k9	alespoň
písničku	písnička	k1gFnSc4	písnička
My	my	k3xPp1nPc1	my
father	fathra	k1gFnPc2	fathra
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
eyes	eyes	k1gInSc1	eyes
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
na	na	k7c6	na
albu	album	k1gNnSc6	album
Pilgrim	Pilgrima	k1gFnPc2	Pilgrima
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
rodinným	rodinný	k2eAgInPc3d1	rodinný
poměrům	poměr	k1gInPc3	poměr
se	se	k3xPyFc4	se
z	z	k7c2	z
Claptona	Clapton	k1gMnSc2	Clapton
stal	stát	k5eAaPmAgMnS	stát
introvert	introvert	k1gMnSc1	introvert
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dlouho	dlouho	k6eAd1	dlouho
hledal	hledat	k5eAaImAgMnS	hledat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
identitu	identita	k1gFnSc4	identita
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
uzavíral	uzavírat	k5eAaPmAgInS	uzavírat
do	do	k7c2	do
světa	svět	k1gInSc2	svět
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
patnácti	patnáct	k4xCc6	patnáct
letech	léto	k1gNnPc6	léto
ho	on	k3xPp3gInSc2	on
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
syrové	syrový	k2eAgFnPc1d1	syrová
černošské	černošský	k2eAgFnPc1d1	černošská
bluesové	bluesový	k2eAgFnPc1d1	bluesová
nahrávky	nahrávka	k1gFnPc1	nahrávka
Muddy	Mudda	k1gFnSc2	Mudda
Waterse	Waterse	k1gFnSc1	Waterse
<g/>
,	,	kIx,	,
Chucka	Chucka	k1gFnSc1	Chucka
Berryho	Berry	k1gMnSc2	Berry
a	a	k8xC	a
dalších	další	k2eAgMnPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Prarodiče	prarodič	k1gMnPc1	prarodič
mu	on	k3xPp3gMnSc3	on
koupili	koupit	k5eAaPmAgMnP	koupit
první	první	k4xOgFnSc4	první
kytaru	kytara	k1gFnSc4	kytara
a	a	k8xC	a
už	už	k6eAd1	už
během	během	k7c2	během
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
umělecké	umělecký	k2eAgFnSc6d1	umělecká
škole	škola	k1gFnSc6	škola
si	se	k3xPyFc3	se
vydělával	vydělávat	k5eAaImAgMnS	vydělávat
hraním	hranit	k5eAaImIp1nS	hranit
po	po	k7c6	po
hospodách	hospodách	k?	hospodách
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
založil	založit	k5eAaPmAgMnS	založit
první	první	k4xOgFnSc4	první
skupinu	skupina	k1gFnSc4	skupina
Roosters	Roostersa	k1gFnPc2	Roostersa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hráli	hrát	k5eAaImAgMnP	hrát
například	například	k6eAd1	například
pozdější	pozdní	k2eAgMnPc1d2	pozdější
členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
Manfred	Manfred	k1gMnSc1	Manfred
Mann	Mann	k1gMnSc1	Mann
Paul	Paul	k1gMnSc1	Paul
Jones	Jones	k1gMnSc1	Jones
<g/>
,	,	kIx,	,
Tom	Tom	k1gMnSc1	Tom
McGuinness	McGuinness	k1gInSc1	McGuinness
nebo	nebo	k8xC	nebo
Brian	Brian	k1gMnSc1	Brian
Jones	Jones	k1gMnSc1	Jones
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
později	pozdě	k6eAd2	pozdě
přešel	přejít	k5eAaPmAgMnS	přejít
k	k	k7c3	k
Rolling	Rolling	k1gInSc4	Rolling
Stones	Stonesa	k1gFnPc2	Stonesa
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
neměla	mít	k5eNaImAgFnS	mít
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
trvání	trvání	k1gNnSc2	trvání
a	a	k8xC	a
Clapton	Clapton	k1gInSc1	Clapton
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
přešel	přejít	k5eAaPmAgInS	přejít
k	k	k7c3	k
The	The	k1gFnSc3	The
Yardbirds	Yardbirdsa	k1gFnPc2	Yardbirdsa
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
nahráli	nahrát	k5eAaPmAgMnP	nahrát
tři	tři	k4xCgNnPc4	tři
alba	album	k1gNnPc4	album
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
Clapton	Clapton	k1gInSc4	Clapton
odešel	odejít	k5eAaPmAgMnS	odejít
a	a	k8xC	a
přidal	přidat	k5eAaPmAgMnS	přidat
se	se	k3xPyFc4	se
k	k	k7c3	k
Johnu	John	k1gMnSc3	John
Mayallovi	Mayall	k1gMnSc3	Mayall
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc3	jeho
The	The	k1gMnPc3	The
Bluesbreakers	Bluesbreakersa	k1gFnPc2	Bluesbreakersa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
naplno	naplno	k6eAd1	naplno
uplatnil	uplatnit	k5eAaPmAgMnS	uplatnit
umění	umění	k1gNnSc4	umění
bluesového	bluesový	k2eAgNnSc2d1	bluesové
hraní	hraní	k1gNnSc2	hraní
a	a	k8xC	a
získal	získat	k5eAaPmAgInS	získat
si	se	k3xPyFc3	se
tam	tam	k6eAd1	tam
značnou	značný	k2eAgFnSc4d1	značná
popularitu	popularita	k1gFnSc4	popularita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
vydání	vydání	k1gNnSc4	vydání
velmi	velmi	k6eAd1	velmi
úspěšného	úspěšný	k2eAgNnSc2d1	úspěšné
alba	album	k1gNnSc2	album
Bluesbreakers	Bluesbreakersa	k1gFnPc2	Bluesbreakersa
with	witha	k1gFnPc2	witha
Eric	Eric	k1gFnSc1	Eric
Clapton	Clapton	k1gInSc1	Clapton
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
jeho	on	k3xPp3gInSc4	on
styl	styl	k1gInSc4	styl
hraní	hranit	k5eAaImIp3nP	hranit
začali	začít	k5eAaPmAgMnP	začít
kopírovat	kopírovat	k5eAaImF	kopírovat
i	i	k9	i
ostatní	ostatní	k2eAgMnPc1d1	ostatní
hudebníci	hudebník	k1gMnPc1	hudebník
<g/>
.	.	kIx.	.
</s>
<s>
Publikum	publikum	k1gNnSc1	publikum
uchvátil	uchvátit	k5eAaPmAgInS	uchvátit
výrazný	výrazný	k2eAgInSc1d1	výrazný
a	a	k8xC	a
osobitý	osobitý	k2eAgInSc1d1	osobitý
hráčský	hráčský	k2eAgInSc1d1	hráčský
styl	styl	k1gInSc1	styl
odposlouchaný	odposlouchaný	k2eAgInSc1d1	odposlouchaný
od	od	k7c2	od
černošských	černošský	k2eAgMnPc2d1	černošský
kytaristů	kytarista	k1gMnPc2	kytarista
<g/>
,	,	kIx,	,
doplněný	doplněný	k2eAgInSc1d1	doplněný
o	o	k7c4	o
vlastní	vlastní	k2eAgInSc4d1	vlastní
zvuk	zvuk	k1gInSc4	zvuk
spočívající	spočívající	k2eAgFnSc4d1	spočívající
v	v	k7c6	v
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
"	"	kIx"	"
<g/>
houslových	houslový	k2eAgInPc6d1	houslový
<g/>
"	"	kIx"	"
tónech	tón	k1gInPc6	tón
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
nimž	jenž	k3xRgFnPc3	jenž
získal	získat	k5eAaPmAgMnS	získat
svou	svůj	k3xOyFgFnSc4	svůj
přezdívku	přezdívka	k1gFnSc4	přezdívka
slowhand	slowhanda	k1gFnPc2	slowhanda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
skupině	skupina	k1gFnSc6	skupina
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
hudebníky	hudebník	k1gMnPc7	hudebník
Jackem	Jacek	k1gMnSc7	Jacek
Brucem	Bruce	k1gMnSc7	Bruce
a	a	k8xC	a
Gingerem	Ginger	k1gMnSc7	Ginger
Bakerem	Baker	k1gMnSc7	Baker
s	s	k7c7	s
kterými	který	k3yQgFnPc7	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
založil	založit	k5eAaPmAgMnS	založit
skupinu	skupina	k1gFnSc4	skupina
The	The	k1gFnSc2	The
Cream	Cream	k1gInSc1	Cream
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterou	který	k3yIgFnSc7	který
přišel	přijít	k5eAaPmAgMnS	přijít
velký	velký	k2eAgInSc4d1	velký
komerční	komerční	k2eAgInSc4d1	komerční
úspěch	úspěch	k1gInSc4	úspěch
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
hvězdou	hvězda	k1gFnSc7	hvězda
první	první	k4xOgFnSc2	první
velikosti	velikost	k1gFnSc2	velikost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
začaly	začít	k5eAaPmAgInP	začít
Claptonovy	Claptonův	k2eAgInPc1d1	Claptonův
psychické	psychický	k2eAgInPc1d1	psychický
problémy	problém	k1gInPc1	problém
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
záchvaty	záchvat	k1gInPc1	záchvat
malomyslnosti	malomyslnost	k1gFnSc2	malomyslnost
a	a	k8xC	a
malou	malý	k2eAgFnSc4d1	malá
sebedůvěru	sebedůvěra	k1gFnSc4	sebedůvěra
řešil	řešit	k5eAaImAgMnS	řešit
alkoholem	alkohol	k1gInSc7	alkohol
a	a	k8xC	a
drogami	droga	k1gFnPc7	droga
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
a	a	k8xC	a
Clapton	Clapton	k1gInSc1	Clapton
hrál	hrát	k5eAaImAgInS	hrát
se	s	k7c7	s
skupinami	skupina	k1gFnPc7	skupina
Blind	Blind	k1gMnSc1	Blind
Faith	Faith	k1gMnSc1	Faith
(	(	kIx(	(
<g/>
hned	hned	k6eAd1	hned
po	po	k7c6	po
první	první	k4xOgFnSc6	první
vydané	vydaný	k2eAgFnSc6d1	vydaná
desce	deska	k1gFnSc6	deska
skončil	skončit	k5eAaPmAgMnS	skončit
tento	tento	k3xDgInSc4	tento
projekt	projekt	k1gInSc4	projekt
fiaskem	fiasko	k1gNnSc7	fiasko
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
rhythm	rhythm	k1gMnSc1	rhythm
<g/>
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
bluesovém	bluesový	k2eAgNnSc6d1	bluesové
duu	duo	k1gNnSc6	duo
Delaney	Delanea	k1gFnSc2	Delanea
&	&	k?	&
Bonnie	Bonnie	k1gFnSc1	Bonnie
<g/>
.	.	kIx.	.
</s>
<s>
Hostoval	hostovat	k5eAaImAgMnS	hostovat
na	na	k7c6	na
deskách	deska	k1gFnPc6	deska
svých	svůj	k3xOyFgMnPc2	svůj
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Harrisonem	Harrison	k1gMnSc7	Harrison
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
mj.	mj.	kA	mj.
členem	člen	k1gMnSc7	člen
doprovodné	doprovodný	k2eAgFnSc2d1	doprovodná
Lennonovy	Lennonův	k2eAgFnSc2d1	Lennonova
skupiny	skupina	k1gFnSc2	skupina
Plastic	Plastice	k1gFnPc2	Plastice
Ono	onen	k3xDgNnSc1	onen
Band	band	k1gInSc1	band
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
sólovou	sólový	k2eAgFnSc4d1	sólová
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Derek	Derek	k1gMnSc1	Derek
působil	působit	k5eAaImAgMnS	působit
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Derek	Derky	k1gFnPc2	Derky
and	and	k?	and
the	the	k?	the
Dominos	Dominos	k1gMnSc1	Dominos
se	se	k3xPyFc4	se
kterou	který	k3yRgFnSc7	který
nahrál	nahrát	k5eAaPmAgMnS	nahrát
například	například	k6eAd1	například
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
nejslavnějších	slavný	k2eAgFnPc2d3	nejslavnější
písní	píseň	k1gFnPc2	píseň
Layla	Laylo	k1gNnSc2	Laylo
věnovanou	věnovaný	k2eAgFnSc4d1	věnovaná
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
manželce	manželka	k1gFnSc3	manželka
Georgea	Georgeum	k1gNnSc2	Georgeum
Harrisona	Harrison	k1gMnSc2	Harrison
Pattie	Pattie	k1gFnSc2	Pattie
Boydové	Boydová	k1gFnSc2	Boydová
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sestava	sestava	k1gFnSc1	sestava
Derek	Derky	k1gFnPc2	Derky
and	and	k?	and
the	the	k?	the
Dominos	Dominosa	k1gFnPc2	Dominosa
sice	sice	k8xC	sice
byla	být	k5eAaImAgFnS	být
celkem	celkem	k6eAd1	celkem
úspěšná	úspěšný	k2eAgFnSc1d1	úspěšná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Clapton	Clapton	k1gInSc1	Clapton
propadl	propadnout	k5eAaPmAgInS	propadnout
depresím	deprese	k1gFnPc3	deprese
a	a	k8xC	a
několikaleté	několikaletý	k2eAgFnSc6d1	několikaletá
drogové	drogový	k2eAgFnSc6d1	drogová
a	a	k8xC	a
alkoholové	alkoholový	k2eAgFnSc6d1	alkoholová
závislosti	závislost	k1gFnSc6	závislost
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
sotva	sotva	k6eAd1	sotva
udržel	udržet	k5eAaPmAgInS	udržet
na	na	k7c6	na
nohou	noha	k1gFnPc6	noha
<g/>
,	,	kIx,	,
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
hrál	hrát	k5eAaImAgMnS	hrát
jako	jako	k9	jako
o	o	k7c4	o
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
zmizel	zmizet	k5eAaPmAgMnS	zmizet
z	z	k7c2	z
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
zvolnil	zvolnit	k5eAaPmAgMnS	zvolnit
pracovní	pracovní	k2eAgNnSc4d1	pracovní
tempo	tempo	k1gNnSc4	tempo
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
nahrávat	nahrávat	k5eAaImF	nahrávat
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
<g/>
,	,	kIx,	,
příjemné	příjemný	k2eAgFnPc4d1	příjemná
skladby	skladba	k1gFnPc4	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Vydal	vydat	k5eAaPmAgMnS	vydat
několik	několik	k4yIc1	několik
hudebně	hudebně	k6eAd1	hudebně
zajímavých	zajímavý	k2eAgNnPc2d1	zajímavé
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
experimentoval	experimentovat	k5eAaImAgMnS	experimentovat
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
popové	popový	k2eAgFnSc2d1	popová
hudby	hudba	k1gFnSc2	hudba
i	i	k9	i
country	country	k2eAgMnSc7d1	country
<g/>
.	.	kIx.	.
</s>
<s>
Clapton	Clapton	k1gInSc1	Clapton
byl	být	k5eAaImAgInS	být
téměř	téměř	k6eAd1	téměř
celou	celý	k2eAgFnSc4d1	celá
svou	svůj	k3xOyFgFnSc4	svůj
kariéru	kariéra	k1gFnSc4	kariéra
prezentován	prezentován	k2eAgMnSc1d1	prezentován
jako	jako	k8xS	jako
představitel	představitel	k1gMnSc1	představitel
bílého	bílý	k2eAgNnSc2d1	bílé
blues	blues	k1gNnSc2	blues
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
i	i	k8xC	i
hodně	hodně	k6eAd1	hodně
z	z	k7c2	z
muzikanta	muzikant	k1gMnSc2	muzikant
schopného	schopný	k2eAgInSc2d1	schopný
dělat	dělat	k5eAaImF	dělat
pop	pop	k1gMnSc1	pop
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
šesti	šest	k4xCc2	šest
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
za	za	k7c4	za
projekt	projekt	k1gInSc4	projekt
MTV	MTV	kA	MTV
Unplugged	Unplugged	k1gInSc1	Unplugged
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Eric	Eric	k6eAd1	Eric
Clapton	Clapton	k1gInSc1	Clapton
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
kytaristů	kytarista	k1gMnPc2	kytarista
<g/>
,	,	kIx,	,
jež	jenž	k3xRgMnPc4	jenž
proslavili	proslavit	k5eAaPmAgMnP	proslavit
kytary	kytara	k1gFnPc4	kytara
značky	značka	k1gFnSc2	značka
Fender	Fendra	k1gFnPc2	Fendra
Stratocaster	Stratocaster	k1gMnSc1	Stratocaster
Poslední	poslední	k2eAgNnSc4d1	poslední
album	album	k1gNnSc4	album
Old	Olda	k1gFnPc2	Olda
Sock	Socka	k1gFnPc2	Socka
vydal	vydat	k5eAaPmAgInS	vydat
Clapton	Clapton	k1gInSc1	Clapton
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
odehrál	odehrát	k5eAaPmAgInS	odehrát
Eric	Eric	k1gInSc1	Eric
Clapton	Clapton	k1gInSc1	Clapton
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
koncert	koncert	k1gInSc4	koncert
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
koncert	koncert	k1gInSc1	koncert
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
19	[number]	k4	19
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
Claptonovou	Claptonový	k2eAgFnSc7d1	Claptonový
manželkou	manželka	k1gFnSc7	manželka
byla	být	k5eAaImAgFnS	být
Pattie	Pattie	k1gFnSc1	Pattie
Boydová	Boydová	k1gFnSc1	Boydová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
odešla	odejít	k5eAaPmAgFnS	odejít
během	během	k7c2	během
drogové	drogový	k2eAgFnSc2d1	drogová
závislosti	závislost	k1gFnSc2	závislost
svého	svůj	k3xOyFgMnSc2	svůj
prvního	první	k4xOgMnSc2	první
muže	muž	k1gMnSc2	muž
George	George	k1gNnSc2	George
Harrisona	Harrison	k1gMnSc2	Harrison
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Oddáni	oddán	k2eAgMnPc1d1	oddán
byli	být	k5eAaImAgMnP	být
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1979	[number]	k4	1979
<g/>
,	,	kIx,	,
rozvedeni	rozveden	k2eAgMnPc1d1	rozveden
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
potkal	potkat	k5eAaPmAgInS	potkat
Clapton	Clapton	k1gInSc1	Clapton
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
pětadvacetiletou	pětadvacetiletý	k2eAgFnSc4d1	pětadvacetiletá
umělkyni	umělkyně	k1gFnSc4	umělkyně
Meliu	Melium	k1gNnSc3	Melium
McEneryovou	McEneryový	k2eAgFnSc7d1	McEneryový
<g/>
,	,	kIx,	,
když	když	k8xS	když
pracoval	pracovat	k5eAaImAgMnS	pracovat
na	na	k7c6	na
albu	album	k1gNnSc6	album
s	s	k7c7	s
B.	B.	kA	B.
B.	B.	kA	B.
Kingem	King	k1gInSc7	King
<g/>
.	.	kIx.	.
</s>
<s>
Svatbu	svatba	k1gFnSc4	svatba
měli	mít	k5eAaImAgMnP	mít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
v	v	k7c6	v
Claptonově	Claptonův	k2eAgNnSc6d1	Claptonovo
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
Ripley	Riplea	k1gFnSc2	Riplea
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
spolu	spolu	k6eAd1	spolu
tři	tři	k4xCgFnPc4	tři
dcery	dcera	k1gFnPc4	dcera
<g/>
:	:	kIx,	:
Julii	Julie	k1gFnSc4	Julie
Rose	Ros	k1gMnSc2	Ros
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ellu	Ella	k1gMnSc4	Ella
May	May	k1gMnSc1	May
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
a	a	k8xC	a
Sophii	Sophie	k1gFnSc4	Sophie
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
dceru	dcera	k1gFnSc4	dcera
Ruth	Ruth	k1gFnSc2	Ruth
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
Clapton	Clapton	k1gInSc1	Clapton
s	s	k7c7	s
bývalou	bývalý	k2eAgFnSc7d1	bývalá
přítelkyní	přítelkyně	k1gFnSc7	přítelkyně
Ivoone	Ivoon	k1gInSc5	Ivoon
Kelleyovou	Kelleyový	k2eAgFnSc7d1	Kelleyový
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1986	[number]	k4	1986
se	se	k3xPyFc4	se
Claptonovi	Clapton	k1gMnSc6	Clapton
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
Conor	Conor	k1gMnSc1	Conor
(	(	kIx(	(
<g/>
matkou	matka	k1gFnSc7	matka
byla	být	k5eAaImAgFnS	být
italská	italský	k2eAgFnSc1d1	italská
modelka	modelka	k1gFnSc1	modelka
Lory	Lory	k1gFnSc1	Lory
Del	Del	k1gFnSc1	Del
Santová	Santová	k1gFnSc1	Santová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hudebníkův	hudebníkův	k2eAgInSc4d1	hudebníkův
život	život	k1gInSc4	život
ovšem	ovšem	k9	ovšem
postihla	postihnout	k5eAaPmAgFnS	postihnout
tragédie	tragédie	k1gFnSc1	tragédie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ho	on	k3xPp3gMnSc4	on
velmi	velmi	k6eAd1	velmi
zranila	zranit	k5eAaPmAgFnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
syn	syn	k1gMnSc1	syn
zabil	zabít	k5eAaPmAgMnS	zabít
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
z	z	k7c2	z
53	[number]	k4	53
<g/>
.	.	kIx.	.
patra	patro	k1gNnSc2	patro
newyorského	newyorský	k2eAgInSc2d1	newyorský
mrakodrapu	mrakodrap	k1gInSc2	mrakodrap
<g/>
.	.	kIx.	.
</s>
<s>
Clapton	Clapton	k1gInSc1	Clapton
se	se	k3xPyFc4	se
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
do	do	k7c2	do
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
tragédii	tragédie	k1gFnSc6	tragédie
napsal	napsat	k5eAaPmAgMnS	napsat
asi	asi	k9	asi
svou	svůj	k3xOyFgFnSc4	svůj
nejznámější	známý	k2eAgFnSc4d3	nejznámější
baladu	balada	k1gFnSc4	balada
"	"	kIx"	"
<g/>
Tears	Tears	k1gInSc1	Tears
in	in	k?	in
Heaven	Heavna	k1gFnPc2	Heavna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
prodal	prodat	k5eAaPmAgInS	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
70	[number]	k4	70
kytar	kytara	k1gFnPc2	kytara
a	a	k8xC	a
zesilovačů	zesilovač	k1gInPc2	zesilovač
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2012	[number]	k4	2012
prodal	prodat	k5eAaPmAgInS	prodat
obraz	obraz	k1gInSc1	obraz
Gerharda	Gerhard	k1gMnSc2	Gerhard
Richtera	Richter	k1gMnSc2	Richter
v	v	k7c4	v
Sotheby	Sotheb	k1gInPc4	Sotheb
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
za	za	k7c2	za
34,2	[number]	k4	34,2
milióny	milión	k4xCgInPc4	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Five	Fivat	k5eAaPmIp3nS	Fivat
Live	Live	k1gFnSc4	Live
Yardbirds	Yardbirdsa	k1gFnPc2	Yardbirdsa
(	(	kIx(	(
<g/>
živá	živý	k2eAgFnSc1d1	živá
nahrávka	nahrávka	k1gFnSc1	nahrávka
<g/>
,	,	kIx,	,
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
For	forum	k1gNnPc2	forum
Your	Youra	k1gFnPc2	Youra
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
Having	Having	k1gInSc1	Having
a	a	k8xC	a
Rave	Rave	k1gFnSc1	Rave
Up	Up	k1gFnSc1	Up
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
Blues	blues	k1gNnSc1	blues
Anytime	Anytim	k1gInSc5	Anytim
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
Bluesbreakers	Bluesbreakers	k1gInSc1	Bluesbreakers
with	with	k1gInSc1	with
Eric	Eric	k1gInSc1	Eric
Clapton	Clapton	k1gInSc1	Clapton
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
What	What	k1gInSc1	What
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
Shakin	Shakin	k1gInSc1	Shakin
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
kompilace	kompilace	k1gFnSc1	kompilace
<g/>
,	,	kIx,	,
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
Fresh	Fresh	k1gInSc1	Fresh
Cream	Cream	k1gInSc1	Cream
(	(	kIx(	(
<g/>
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
Disraeli	Disrael	k1gInSc6	Disrael
Gears	Gearsa	k1gFnPc2	Gearsa
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
Wheels	Wheels	k1gInSc1	Wheels
of	of	k?	of
Fire	Fire	k1gInSc1	Fire
(	(	kIx(	(
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Goodbye	Goodby	k1gInSc2	Goodby
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Live	Liv	k1gInSc2	Liv
Cream	Cream	k1gInSc1	Cream
(	(	kIx(	(
<g/>
živá	živý	k2eAgFnSc1d1	živá
nahrávka	nahrávka	k1gFnSc1	nahrávka
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Live	Live	k6eAd1	Live
Cream	Cream	k1gInSc1	Cream
Volume	volum	k1gInSc5	volum
II	II	kA	II
(	(	kIx(	(
<g/>
živá	živý	k2eAgFnSc1d1	živá
nahrávka	nahrávka	k1gFnSc1	nahrávka
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Heavy	Heava	k1gFnSc2	Heava
Cream	Cream	k1gInSc1	Cream
(	(	kIx(	(
<g/>
kompilace	kompilace	k1gFnSc1	kompilace
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Strange	Strange	k1gFnSc1	Strange
Brew	Brew	k1gFnSc1	Brew
(	(	kIx(	(
<g/>
kompilace	kompilace	k1gFnSc1	kompilace
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
The	The	k1gFnPc1	The
Very	Very	k1gInPc1	Very
Best	Best	k2eAgInSc1d1	Best
of	of	k?	of
Cream	Cream	k1gInSc1	Cream
(	(	kIx(	(
<g/>
kompilace	kompilace	k1gFnSc1	kompilace
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Those	Those	k1gFnSc1	Those
Were	Wer	k1gFnSc2	Wer
the	the	k?	the
Days	Days	k1gInSc1	Days
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g />
.	.	kIx.	.
</s>
<s>
<g/>
th	th	k?	th
Century	Centura	k1gFnPc1	Centura
Masters	Mastersa	k1gFnPc2	Mastersa
(	(	kIx(	(
<g/>
kompilace	kompilace	k1gFnSc1	kompilace
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Cream	Cream	k1gInSc4	Cream
<g/>
:	:	kIx,	:
The	The	k1gMnSc3	The
BBC	BBC	kA	BBC
Sessions	Sessions	k1gInSc1	Sessions
(	(	kIx(	(
<g/>
kompilace	kompilace	k1gFnSc1	kompilace
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Cream	Cream	k1gInSc1	Cream
Gold	Gold	k1gInSc1	Gold
(	(	kIx(	(
<g/>
kompilace	kompilace	k1gFnSc1	kompilace
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Royal	Royal	k1gMnSc1	Royal
Albert	Albert	k1gMnSc1	Albert
Hall	Hall	k1gMnSc1	Hall
London	London	k1gMnSc1	London
May	May	k1gMnSc1	May
2-3-5-6	[number]	k4	2-3-5-6
2005	[number]	k4	2005
(	(	kIx(	(
<g/>
živá	živý	k2eAgFnSc1d1	živá
nahrávka	nahrávka	k1gFnSc1	nahrávka
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Blind	Blind	k1gMnSc1	Blind
Faith	Faith	k1gMnSc1	Faith
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
On	on	k3xPp3gMnSc1	on
Tour	Tour	k1gMnSc1	Tour
with	witha	k1gFnPc2	witha
Eric	Eric	k1gInSc4	Eric
Clapton	Clapton	k1gInSc1	Clapton
(	(	kIx(	(
<g/>
živá	živý	k2eAgFnSc1d1	živá
nahrávka	nahrávka	k1gFnSc1	nahrávka
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Layla	Layla	k1gMnSc1	Layla
and	and	k?	and
Other	Other	k1gMnSc1	Other
Assorted	Assorted	k1gMnSc1	Assorted
Love	lov	k1gInSc5	lov
Songs	Songsa	k1gFnPc2	Songsa
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
In	In	k1gMnSc1	In
Concert	Concert	k1gMnSc1	Concert
(	(	kIx(	(
<g/>
živá	živý	k2eAgFnSc1d1	živá
nahrávka	nahrávka	k1gFnSc1	nahrávka
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Layla	Layla	k1gMnSc1	Layla
Sessions	Sessions	k1gInSc1	Sessions
<g/>
:	:	kIx,	:
20	[number]	k4	20
<g/>
th	th	k?	th
Anniversary	Anniversar	k1gInPc1	Anniversar
Edition	Edition	k1gInSc1	Edition
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Live	Liv	k1gInSc2	Liv
at	at	k?	at
<g />
.	.	kIx.	.
</s>
<s>
the	the	k?	the
Fillmore	Fillmor	k1gMnSc5	Fillmor
(	(	kIx(	(
<g/>
živá	živý	k2eAgFnSc1d1	živá
nahrávka	nahrávka	k1gFnSc1	nahrávka
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Eric	Eric	k1gInSc1	Eric
Clapton	Clapton	k1gInSc1	Clapton
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
461	[number]	k4	461
Ocean	Ocean	k1gMnSc1	Ocean
Boulevard	Boulevard	k1gMnSc1	Boulevard
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
There	Ther	k1gInSc5	Ther
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
One	One	k1gFnSc7	One
in	in	k?	in
Every	Evera	k1gFnSc2	Evera
Crowd	Crowd	k1gMnSc1	Crowd
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
No	no	k9	no
Reason	Reason	k1gInSc1	Reason
to	ten	k3xDgNnSc1	ten
Cry	Cry	k1gFnPc1	Cry
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
Slowhand	Slowhanda	k1gFnPc2	Slowhanda
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
Backless	Backlessa	k1gFnPc2	Backlessa
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
Another	Anothra	k1gFnPc2	Anothra
Ticket	Ticket	k1gInSc1	Ticket
<g/>
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
Money	Monea	k1gFnSc2	Monea
and	and	k?	and
Cigarettes	Cigarettes	k1gMnSc1	Cigarettes
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
Behind	Behind	k1gInSc1	Behind
the	the	k?	the
Sun	Sun	kA	Sun
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
August	August	k1gMnSc1	August
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Journeyman	Journeyman	k1gMnSc1	Journeyman
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
From	From	k1gMnSc1	From
the	the	k?	the
Cradle	Cradle	k1gMnSc1	Cradle
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Pilgrim	Pilgrima	k1gFnPc2	Pilgrima
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Reptile	reptil	k1gMnSc5	reptil
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Me	Me	k1gMnSc1	Me
and	and	k?	and
Mr	Mr	k1gMnSc1	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Johnson	Johnson	k1gMnSc1	Johnson
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Sessions	Sessionsa	k1gFnPc2	Sessionsa
for	forum	k1gNnPc2	forum
Robert	Robert	k1gMnSc1	Robert
J	J	kA	J
(	(	kIx(	(
<g/>
CD	CD	kA	CD
+	+	kIx~	+
DVD	DVD	kA	DVD
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Back	Back	k1gInSc1	Back
Home	Hom	k1gInSc2	Hom
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Road	Road	k1gMnSc1	Road
To	ten	k3xDgNnSc4	ten
Escondido	Escondida	k1gFnSc5	Escondida
-	-	kIx~	-
with	with	k1gInSc1	with
J.J.	J.J.	k1gMnSc2	J.J.
Cale	Cal	k1gMnSc2	Cal
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Clapton	Clapton	k1gInSc1	Clapton
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Eric	Eric	k1gInSc1	Eric
Clapton	Clapton	k1gInSc1	Clapton
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s>
Rainbow	Rainbow	k?	Rainbow
Concert	Concert	k1gInSc1	Concert
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
E.C.	E.C.	k1gFnSc1	E.C.
Was	Was	k1gFnSc1	Was
Here	Here	k1gFnSc1	Here
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
Just	just	k6eAd1	just
One	One	k1gMnSc1	One
Night	Night	k1gMnSc1	Night
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
24	[number]	k4	24
Nights	Nightsa	k1gFnPc2	Nightsa
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Unplugged	Unplugged	k1gInSc1	Unplugged
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Blues	blues	k1gFnSc2	blues
Concert	Concert	k1gMnSc1	Concert
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Crossroads	Crossroads	k1gInSc1	Crossroads
2	[number]	k4	2
<g/>
:	:	kIx,	:
Live	Liv	k1gInSc2	Liv
in	in	k?	in
<g />
.	.	kIx.	.
</s>
<s>
the	the	k?	the
Seventies	Seventies	k1gInSc1	Seventies
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
One	One	k1gFnSc1	One
More	mor	k1gInSc5	mor
Car	car	k1gMnSc1	car
<g/>
,	,	kIx,	,
One	One	k1gMnSc1	One
More	mor	k1gInSc5	mor
Rider	Rider	k1gMnSc1	Rider
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
After	After	k1gMnSc1	After
Midnight	Midnight	k1gMnSc1	Midnight
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
History	Histor	k1gInPc1	Histor
of	of	k?	of
Eric	Eric	k1gInSc1	Eric
Clapton	Clapton	k1gInSc1	Clapton
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Eric	Eric	k1gFnSc1	Eric
Clapton	Clapton	k1gInSc1	Clapton
at	at	k?	at
His	his	k1gNnSc1	his
Best	Best	k1gMnSc1	Best
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
Clapton	Clapton	k1gInSc1	Clapton
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Time	Time	k1gFnSc1	Time
Pieces	Pieces	k1gMnSc1	Pieces
<g/>
:	:	kIx,	:
Best	Best	k1gMnSc1	Best
Of	Of	k1gFnSc2	Of
Eric	Eric	k1gInSc1	Eric
Clapton	Clapton	k1gInSc1	Clapton
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
Backtrackin	Backtrackin	k1gInSc1	Backtrackin
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
Too	Too	k1gFnSc6	Too
Much	moucha	k1gFnPc2	moucha
Monkey	Monkea	k1gFnSc2	Monkea
Business	business	k1gInSc1	business
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Cream	Cream	k1gInSc1	Cream
of	of	k?	of
Eric	Eric	k1gInSc1	Eric
Clapton	Clapton	k1gInSc1	Clapton
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Crossroads	Crossroadsa	k1gFnPc2	Crossroadsa
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Story	story	k1gFnSc1	story
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
Stages	Stagesa	k1gFnPc2	Stagesa
(	(	kIx(	(
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Cream	Cream	k1gInSc1	Cream
of	of	k?	of
Clapton	Clapton	k1gInSc1	Clapton
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Strictly	Strictly	k1gFnSc1	Strictly
The	The	k1gFnSc1	The
Blues	blues	k1gFnSc1	blues
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Blues	blues	k1gFnSc1	blues
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Clapton	Clapton	k1gInSc1	Clapton
Chronicles	Chronicles	k1gMnSc1	Chronicles
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
Eric	Eric	k1gInSc1	Eric
Clapton	Clapton	k1gInSc1	Clapton
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Complete	Comple	k1gNnSc2	Comple
<g />
.	.	kIx.	.
</s>
<s>
Clapton	Clapton	k1gInSc1	Clapton
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
White	Whit	k1gInSc5	Whit
Album	album	k1gNnSc1	album
(	(	kIx(	(
<g/>
i	i	k9	i
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
The	The	k1gMnSc2	The
Beatles	Beatles	k1gFnPc2	Beatles
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Wonderwall	Wonderwall	k1gMnSc1	Wonderwall
Music	Music	k1gMnSc1	Music
(	(	kIx(	(
<g/>
hudba	hudba	k1gFnSc1	hudba
k	k	k7c3	k
filmu	film	k1gInSc3	film
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
All	All	k1gFnSc1	All
Things	Things	k1gInSc4	Things
Must	Must	k2eAgInSc1d1	Must
Pass	Pass	k1gInSc1	Pass
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Concert	Concert	k1gInSc4	Concert
for	forum	k1gNnPc2	forum
Bangla	Bangl	k1gMnSc2	Bangl
Desh	Desha	k1gFnPc2	Desha
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
živá	živý	k2eAgFnSc1d1	živá
nahrávka	nahrávka	k1gFnSc1	nahrávka
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
Dark	Dark	k1gInSc1	Dark
Horse	Horse	k1gFnSc2	Horse
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
George	Georg	k1gMnSc4	Georg
Harrison	Harrison	k1gMnSc1	Harrison
(	(	kIx(	(
<g/>
kompilace	kompilace	k1gFnSc1	kompilace
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
George	George	k1gFnSc1	George
Harrison	Harrison	k1gMnSc1	Harrison
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
Cloud	Cloud	k1gInSc1	Cloud
Nine	Nin	k1gInSc2	Nin
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Best	Best	k1gInSc1	Best
of	of	k?	of
Dark	Dark	k1gInSc1	Dark
Horse	Horse	k1gFnSc1	Horse
1976-1989	[number]	k4	1976-1989
(	(	kIx(	(
<g/>
kompilace	kompilace	k1gFnSc1	kompilace
1989	[number]	k4	1989
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Live	Live	k1gFnSc3	Live
in	in	k?	in
Japan	japan	k1gInSc1	japan
(	(	kIx(	(
<g/>
živá	živý	k2eAgFnSc1d1	živá
nahrávka	nahrávka	k1gFnSc1	nahrávka
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Concert	Concert	k1gInSc1	Concert
for	forum	k1gNnPc2	forum
George	Georg	k1gFnSc2	Georg
(	(	kIx(	(
<g/>
živá	živý	k2eAgFnSc1d1	živá
nahrávka	nahrávka	k1gFnSc1	nahrávka
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
The	The	k1gFnSc6	The
Dark	Darka	k1gFnPc2	Darka
Horse	Horse	k1gFnSc1	Horse
Years	Years	k1gInSc1	Years
1976-1992	[number]	k4	1976-1992
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Lumpy	lump	k1gMnPc7	lump
Gravy	Grava	k1gFnSc2	Grava
(	(	kIx(	(
<g/>
s	s	k7c7	s
Frankem	Frank	k1gMnSc7	Frank
Zappou	Zappa	k1gMnSc7	Zappa
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Live	Liv	k1gInSc2	Liv
Peace	Peace	k1gFnSc1	Peace
in	in	k?	in
Toronto	Toronto	k1gNnSc1	Toronto
<g />
.	.	kIx.	.
</s>
<s>
1969	[number]	k4	1969
(	(	kIx(	(
<g/>
živá	živý	k2eAgFnSc1d1	živá
nahrávka	nahrávka	k1gFnSc1	nahrávka
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Plastic	Plastice	k1gFnPc2	Plastice
Ono	onen	k3xDgNnSc1	onen
Band	band	k1gInSc1	band
<g/>
,	,	kIx,	,
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Stephen	Stephen	k2eAgInSc1d1	Stephen
Stills	Stills	k1gInSc1	Stills
(	(	kIx(	(
<g/>
se	s	k7c7	s
Stephenem	Stephen	k1gMnSc7	Stephen
Stillsem	Stills	k1gMnSc7	Stills
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Leon	Leona	k1gFnPc2	Leona
Russell	Russell	k1gInSc1	Russell
(	(	kIx(	(
<g/>
s	s	k7c7	s
Leonem	Leo	k1gMnSc7	Leo
Russellem	Russell	k1gMnSc7	Russell
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Last	Last	k1gMnSc1	Last
Waltz	waltz	k1gInSc1	waltz
(	(	kIx(	(
<g/>
živá	živý	k2eAgFnSc1d1	živá
nahrávka	nahrávka	k1gFnSc1	nahrávka
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
The	The	k1gMnSc2	The
Band	banda	k1gFnPc2	banda
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
Rough	Rough	k1gInSc1	Rough
Mix	mix	k1gInSc1	mix
(	(	kIx(	(
<g/>
s	s	k7c7	s
Petem	Pet	k1gMnSc7	Pet
Townshendem	Townshend	k1gMnSc7	Townshend
and	and	k?	and
Ronnie	Ronnie	k1gFnSc1	Ronnie
Lanelem	Lanel	k1gInSc7	Lanel
<g/>
,	,	kIx,	,
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Pros	prosa	k1gFnPc2	prosa
and	and	k?	and
Cons	Cons	k1gInSc1	Cons
of	of	k?	of
Hitch	Hitch	k1gInSc1	Hitch
Hiking	Hiking	k1gInSc1	Hiking
(	(	kIx(	(
<g/>
s	s	k7c7	s
Rogerem	Roger	k1gMnSc7	Roger
Watersem	Waters	k1gMnSc7	Waters
solo	solo	k6eAd1	solo
debut	debut	k1gInSc4	debut
<g/>
,	,	kIx,	,
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
Live	Liv	k1gInSc2	Liv
at	at	k?	at
Montreux	Montreux	k1gInSc1	Montreux
1986	[number]	k4	1986
(	(	kIx(	(
<g/>
živá	živý	k2eAgFnSc1d1	živá
nahrávka	nahrávka	k1gFnSc1	nahrávka
s	s	k7c7	s
Otisem	Otis	k1gMnSc7	Otis
Rushem	Rush	k1gInSc7	Rush
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
přáteli	přítel	k1gMnPc7	přítel
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
Persona	persona	k1gFnSc1	persona
(	(	kIx(	(
<g/>
s	s	k7c7	s
Lionou	Lioný	k2eAgFnSc7d1	Lioný
Boydovou	Boydová	k1gFnSc7	Boydová
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
The	The	k1gMnSc1	The
One	One	k1gMnSc1	One
(	(	kIx(	(
<g/>
s	s	k7c7	s
Eltonem	Elton	k1gMnSc7	Elton
Johnem	John	k1gMnSc7	John
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Ten	ten	k3xDgInSc1	ten
Summoner	Summoner	k1gInSc1	Summoner
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Tales	Talesa	k1gFnPc2	Talesa
(	(	kIx(	(
<g/>
se	s	k7c7	s
Stingem	Sting	k1gInSc7	Sting
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
Retail	Retail	k1gInSc1	Retail
Therapy	Therapa	k1gFnSc2	Therapa
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
se	s	k7c7	s
Simonem	Simon	k1gMnSc7	Simon
Climiem	Climius	k1gMnSc7	Climius
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Supernatural	Supernatural	k1gFnSc2	Supernatural
(	(	kIx(	(
<g/>
s	s	k7c7	s
Carlosem	Carlos	k1gInSc7	Carlos
Santanou	Santaný	k2eAgFnSc4d1	Santaný
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Riding	Riding	k1gInSc1	Riding
with	with	k1gMnSc1	with
the	the	k?	the
King	King	k1gMnSc1	King
(	(	kIx(	(
<g/>
s	s	k7c7	s
B.	B.	kA	B.
B.	B.	kA	B.
Kingem	King	k1gMnSc7	King
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Ringo	Ringo	k1gMnSc1	Ringo
Rama	Rama	k1gMnSc1	Rama
(	(	kIx(	(
<g/>
s	s	k7c7	s
Ringo	Ringo	k1gMnSc1	Ringo
Starrem	Starr	k1gMnSc7	Starr
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Africa	Afric	k1gInSc2	Afric
Unite	Unit	k1gInSc5	Unit
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Singles	Singles	k1gInSc1	Singles
Collection	Collection	k1gInSc1	Collection
(	(	kIx(	(
<g/>
s	s	k7c7	s
Bobem	Bob	k1gMnSc7	Bob
Marleym	Marleym	k1gInSc4	Marleym
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Edge	Edg	k1gInSc2	Edg
of	of	k?	of
Darkness	Darkness	k1gInSc1	Darkness
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
Lethal	Lethal	k1gMnSc1	Lethal
Weapon	Weapon	k1gMnSc1	Weapon
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
Homeboy	Homeboa	k1gFnSc2	Homeboa
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
Lethal	Lethal	k1gMnSc1	Lethal
Weapon	Weapon	k1gMnSc1	Weapon
2	[number]	k4	2
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Rush	Rusha	k1gFnPc2	Rusha
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
Lethal	Lethal	k1gMnSc1	Lethal
Weapon	Weapon	k1gMnSc1	Weapon
3	[number]	k4	3
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
Phenomenon	Phenomenona	k1gFnPc2	Phenomenona
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Story	story	k1gFnSc2	story
of	of	k?	of
Us	Us	k1gMnSc1	Us
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
