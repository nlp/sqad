<s>
Drahokam	drahokam	k1gInSc1
či	či	k8xC
drahý	drahý	k2eAgInSc1d1
kámen	kámen	k1gInSc1
(	(	kIx(
<g/>
z	z	k7c2
latiny	latina	k1gFnSc2
Lapis	lapis	k1gInSc1
preciosus	preciosus	k1gInSc1
<g/>
,	,	kIx,
angl.	angl.	kA
Precious	Precious	k1gInSc1
stone	ston	k1gInSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
minerál	minerál	k1gInSc1
(	(	kIx(
<g/>
nerost	nerost	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ceněný	ceněný	k2eAgMnSc1d1
pro	pro	k7c4
své	svůj	k3xOyFgFnPc4
výjimečné	výjimečný	k2eAgFnPc4d1
fyzikální	fyzikální	k2eAgFnPc4d1
a	a	k8xC
estetické	estetický	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
a	a	k8xC
používaný	používaný	k2eAgInSc4d1
k	k	k7c3
ozdobným	ozdobný	k2eAgInPc3d1
účelům	účel	k1gInPc3
<g/>
,	,	kIx,
k	k	k7c3
reprezentaci	reprezentace	k1gFnSc3
anebo	anebo	k8xC
k	k	k7c3
tezauraci	tezaurace	k1gFnSc3
majetku	majetek	k1gInSc2
<g/>
.	.	kIx.
</s>