<s>
Drahokam	drahokam	k1gInSc1
</s>
<s>
Diamanty	diamant	k1gInPc1
s	s	k7c7
briliantovým	briliantový	k2eAgInSc7d1
brusem	brus	k1gInSc7
</s>
<s>
Safír	safír	k1gInSc1
Logan	Logana	k1gFnPc2
<g/>
,	,	kIx,
16	#num#	k4
karátový	karátový	k2eAgInSc1d1
<g/>
,	,	kIx,
v	v	k7c6
broži	brož	k1gFnSc6
s	s	k7c7
věnečkemn	věnečkemn	k1gMnSc1
diamantů	diamant	k1gInPc2
s	s	k7c7
briliantovým	briliantový	k2eAgInSc7d1
brusem	brus	k1gInSc7
<g/>
;	;	kIx,
Přírodovědecké	přírodovědecký	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
Washington	Washington	k1gInSc1
</s>
<s>
Španělský	španělský	k2eAgInSc1d1
šperk	šperk	k1gInSc1
s	s	k7c7
kolumbijskými	kolumbijský	k2eAgInPc7d1
smaragdy	smaragd	k1gInPc7
</s>
<s>
Výběr	výběr	k1gInSc1
běžných	běžný	k2eAgInPc2d1
šperkových	šperkův	k2eAgInPc2d1
kamenů	kámen	k1gInPc2
podle	podle	k7c2
barvy	barva	k1gFnSc2
</s>
<s>
Drahokam	drahokam	k1gInSc1
či	či	k8xC
drahý	drahý	k2eAgInSc1d1
kámen	kámen	k1gInSc1
(	(	kIx(
<g/>
z	z	k7c2
latiny	latina	k1gFnSc2
Lapis	lapis	k1gInSc1
preciosus	preciosus	k1gInSc1
<g/>
,	,	kIx,
angl.	angl.	kA
Precious	Precious	k1gInSc1
stone	ston	k1gInSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
minerál	minerál	k1gInSc1
(	(	kIx(
<g/>
nerost	nerost	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ceněný	ceněný	k2eAgMnSc1d1
pro	pro	k7c4
své	svůj	k3xOyFgFnPc4
výjimečné	výjimečný	k2eAgFnPc4d1
fyzikální	fyzikální	k2eAgFnPc4d1
a	a	k8xC
estetické	estetický	k2eAgFnPc4d1
vlastnosti	vlastnost	k1gFnPc4
a	a	k8xC
používaný	používaný	k2eAgInSc4d1
k	k	k7c3
ozdobným	ozdobný	k2eAgInPc3d1
účelům	účel	k1gInPc3
<g/>
,	,	kIx,
k	k	k7c3
reprezentaci	reprezentace	k1gFnSc3
anebo	anebo	k8xC
k	k	k7c3
tezauraci	tezaurace	k1gFnSc3
majetku	majetek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Termíny	termín	k1gInPc1
</s>
<s>
Synonyma	synonymum	k1gNnPc4
pro	pro	k7c4
drahokam	drahokam	k1gInSc4
<g/>
,	,	kIx,
užívaná	užívaný	k2eAgFnSc1d1
ve	v	k7c6
šperkařství	šperkařství	k1gNnSc6
a	a	k8xC
glyptice	glyptika	k1gFnSc6
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
termíny	termín	k1gInPc1
polodrahokam	polodrahokam	k1gInSc1
(	(	kIx(
<g/>
angl.	angl.	k?
<g/>
semiprecious	semiprecious	k1gInSc1
stone	ston	k1gInSc5
<g/>
)	)	kIx)
nebo	nebo	k8xC
dekorativní	dekorativní	k2eAgInSc4d1
kámen	kámen	k1gInSc4
(	(	kIx(
<g/>
decorative	decorativ	k1gInSc5
stone	ston	k1gInSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
drahokamům	drahokam	k1gInPc3
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
také	také	k9
vulkanické	vulkanický	k2eAgNnSc1d1
sklo	sklo	k1gNnSc1
(	(	kIx(
<g/>
vltavín	vltavín	k1gInSc1
<g/>
,	,	kIx,
jet	jet	k5eAaImNgMnS
<g/>
)	)	kIx)
a	a	k8xC
organické	organický	k2eAgInPc4d1
materiály	materiál	k1gInPc4
jako	jako	k8xC,k8xS
jantar	jantar	k1gInSc4
<g/>
,	,	kIx,
mořský	mořský	k2eAgInSc4d1
korál	korál	k1gInSc4
nebo	nebo	k8xC
perly	perla	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
a	a	k8xC
výskyt	výskyt	k1gInSc1
</s>
<s>
Podle	podle	k7c2
geologického	geologický	k2eAgInSc2d1
původu	původ	k1gInSc2
se	se	k3xPyFc4
drahokamy	drahokam	k1gInPc7
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
ostatní	ostatní	k2eAgInPc4d1
minerály	minerál	k1gInPc4
<g/>
,	,	kIx,
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
magmatické	magmatický	k2eAgNnSc4d1
<g/>
,	,	kIx,
metamorfované	metamorfovaný	k2eAgNnSc4d1
a	a	k8xC
sedimentované	sedimentovaný	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Naleziště	naleziště	k1gNnSc1
</s>
<s>
Primárním	primární	k2eAgNnSc7d1
nalezištěm	naleziště	k1gNnSc7
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
matečná	matečný	k2eAgFnSc1d1
hornina	hornina	k1gFnSc1
<g/>
,	,	kIx,
z	z	k7c2
níž	jenž	k3xRgFnSc2
se	se	k3xPyFc4
drahokamy	drahokam	k1gInPc7
těží	těžet	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Povrchová	povrchový	k2eAgFnSc1d1
těžba	těžba	k1gFnSc1
se	se	k3xPyFc4
provádí	provádět	k5eAaImIp3nS
kopáním	kopání	k1gNnSc7
krumpáčem	krumpáč	k1gInSc7
(	(	kIx(
<g/>
například	například	k6eAd1
pyropy	pyrop	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hloubková	hloubkový	k2eAgFnSc1d1
těžba	těžba	k1gFnSc1
probíhá	probíhat	k5eAaImIp3nS
v	v	k7c6
kopaných	kopaná	k1gFnPc6
a	a	k8xC
pažením	pažení	k1gNnSc7
zpevněných	zpevněný	k2eAgInPc2d1
šachtách	šachta	k1gFnPc6
nebo	nebo	k8xC
ve	v	k7c6
štolách	štola	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
sekundárních	sekundární	k2eAgNnPc6d1
nalezištích	naleziště	k1gNnPc6
(	(	kIx(
<g/>
říční	říční	k2eAgInPc1d1
a	a	k8xC
mořské	mořský	k2eAgInPc1d1
náplavy	náplav	k1gInPc1
<g/>
,	,	kIx,
pouštní	pouštní	k2eAgInPc1d1
písky	písek	k1gInPc1
<g/>
,	,	kIx,
zvětralé	zvětralý	k2eAgFnPc1d1
horniny	hornina	k1gFnPc1
<g/>
)	)	kIx)
se	s	k7c7
drahokamy	drahokam	k1gInPc7
získávají	získávat	k5eAaImIp3nP
povrchovým	povrchový	k2eAgInSc7d1
sběrem	sběr	k1gInSc7
<g/>
,	,	kIx,
roztloukáním	roztloukání	k1gNnSc7
kladivem	kladivo	k1gNnSc7
nebo	nebo	k8xC
plavením	plavení	k1gNnSc7
v	v	k7c6
sítech	síto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s>
Hodnotící	hodnotící	k2eAgNnPc1d1
kritéria	kritérion	k1gNnPc1
drahokamů	drahokam	k1gInPc2
</s>
<s>
Podle	podle	k7c2
tradiční	tradiční	k2eAgFnSc2d1
definice	definice	k1gFnSc2
musí	muset	k5eAaImIp3nS
drahokam	drahokam	k1gInSc4
být	být	k5eAaImF
varietou	varieta	k1gFnSc7
minerálu	minerál	k1gInSc2
s	s	k7c7
výjimečnými	výjimečný	k2eAgFnPc7d1
vlastnostmi	vlastnost	k1gFnPc7
<g/>
:	:	kIx,
tvrdostí	tvrdost	k1gFnSc7
(	(	kIx(
<g/>
v	v	k7c6
desetistupňové	desetistupňový	k2eAgFnSc6d1
Mohsově	Mohsův	k2eAgFnSc6d1
stupnici	stupnice	k1gFnSc6
tvrdosti	tvrdost	k1gFnSc2
zaujímá	zaujímat	k5eAaImIp3nS
horní	horní	k2eAgFnSc4d1
polovinu	polovina	k1gFnSc4
<g/>
,	,	kIx,
od	od	k7c2
5	#num#	k4
<g/>
.	.	kIx.
do	do	k7c2
10	#num#	k4
<g/>
.	.	kIx.
stupně	stupeň	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
průhledný	průhledný	k2eAgInSc1d1
či	či	k8xC
průsvitný	průsvitný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Kritéria	kritérion	k1gNnPc1
hodnocení	hodnocení	k1gNnSc2
se	se	k3xPyFc4
původně	původně	k6eAd1
uplatňovala	uplatňovat	k5eAaImAgFnS
pro	pro	k7c4
diamant	diamant	k1gInSc4
<g/>
,	,	kIx,
gemologové	gemolog	k1gMnPc1
je	on	k3xPp3gMnPc4
užívají	užívat	k5eAaImIp3nP
také	také	k9
pro	pro	k7c4
další	další	k2eAgInPc4d1
drahokamy	drahokam	k1gInPc4
<g/>
:	:	kIx,
označují	označovat	k5eAaImIp3nP
se	se	k3xPyFc4
"	"	kIx"
<g/>
čtyři	čtyři	k4xCgMnPc1
cé	cé	k?
<g/>
"	"	kIx"
podle	podle	k7c2
anglického	anglický	k2eAgNnSc2d1
názvosloví	názvosloví	k1gNnSc2
<g/>
:	:	kIx,
Carat	Carat	k1gInSc1
=	=	kIx~
hmotnost	hmotnost	k1gFnSc1
v	v	k7c6
metrických	metrický	k2eAgInPc6d1
karátech	karát	k1gInPc6
<g/>
,	,	kIx,
Colour	Colour	k1gMnSc1
=	=	kIx~
barva	barva	k1gFnSc1
<g/>
,	,	kIx,
tj.	tj.	kA
zabarvení	zabarvení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
diamantů	diamant	k1gInPc2
není	být	k5eNaImIp3nS
žádoucí	žádoucí	k2eAgFnSc1d1
sytá	sytý	k2eAgFnSc1d1
barevnost	barevnost	k1gFnSc1
<g/>
,	,	kIx,
bezbarvý	bezbarvý	k2eAgInSc1d1
(	(	kIx(
<g/>
bílý	bílý	k2eAgInSc1d1
<g/>
)	)	kIx)
diamant	diamant	k1gInSc1
nejlépe	dobře	k6eAd3
odráží	odrážet	k5eAaImIp3nS
světlo	světlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
tomu	ten	k3xDgInSc3
u	u	k7c2
jiných	jiný	k2eAgInPc2d1
drahokamů	drahokam	k1gInPc2
se	se	k3xPyFc4
sytost	sytost	k1gFnSc1
zbarvení	zbarvení	k1gNnPc2
oceňuje	oceňovat	k5eAaImIp3nS
(	(	kIx(
<g/>
zejména	zejména	k9
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
vzácná	vzácný	k2eAgFnSc1d1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
u	u	k7c2
smaragdu	smaragd	k1gInSc2
nebo	nebo	k8xC
safíru	safír	k1gInSc2
<g/>
)	)	kIx)
<g/>
;	;	kIx,
Clarity	Clarita	k1gFnPc1
=	=	kIx~
vnitřní	vnitřní	k2eAgFnSc1d1
čistota	čistota	k1gFnSc1
<g/>
,	,	kIx,
tj.	tj.	kA
minimální	minimální	k2eAgFnPc4d1
poruchy	porucha	k1gFnPc4
jako	jako	k8xS,k8xC
jehlice	jehlice	k1gFnPc4
<g/>
,	,	kIx,
pérka	pérko	k1gNnPc4
<g/>
,	,	kIx,
praskliny	prasklina	k1gFnPc1
či	či	k8xC
krystalky	krystalka	k1gFnPc1
jiných	jiný	k2eAgInPc2d1
drahokamů	drahokam	k1gInPc2
<g/>
;	;	kIx,
Cut	Cut	k1gFnSc1
=	=	kIx~
brus	brus	k1gInSc1
<g/>
,	,	kIx,
tj.	tj.	kA
počet	počet	k1gInSc1
a	a	k8xC
přesnost	přesnost	k1gFnSc1
plošek	ploška	k1gFnPc2
(	(	kIx(
<g/>
facet	facet	k1gInSc1
<g/>
)	)	kIx)
výbrusu	výbrus	k1gInSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
zapříčiňuje	zapříčiňovat	k5eAaImIp3nS
dokonalost	dokonalost	k1gFnSc1
odrazu	odraz	k1gInSc2
světla	světlo	k1gNnSc2
<g/>
,	,	kIx,
měřenou	měřený	k2eAgFnSc4d1
indexem	index	k1gInSc7
lomu	lom	k1gInSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
lesk	lesk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
soudobém	soudobý	k2eAgNnSc6d1
šperkařství	šperkařství	k1gNnSc6
a	a	k8xC
glyptice	glyptika	k1gFnSc6
se	se	k3xPyFc4
k	k	k7c3
drahokamům	drahokam	k1gInPc3
řadí	řadit	k5eAaImIp3nP
také	také	k9
polodrahokamy	polodrahokam	k1gInPc1
a	a	k8xC
dekorativní	dekorativní	k2eAgInPc1d1
kameny	kámen	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
mají	mít	k5eAaImIp3nP
v	v	k7c6
Mohsově	Mohsův	k2eAgFnSc6d1
stupnici	stupnice	k1gFnSc6
číslo	číslo	k1gNnSc1
2	#num#	k4
<g/>
.	.	kIx.
až	až	k9
5	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
jsou	být	k5eAaImIp3nP
neprůsvitné	průsvitný	k2eNgFnPc1d1
(	(	kIx(
<g/>
opakní	opakní	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
malachit	malachit	k1gInSc1
<g/>
,	,	kIx,
tyrkys	tyrkys	k1gInSc1
<g/>
,	,	kIx,
alabastr	alabastr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
vzácnost	vzácnost	k1gFnSc1
v	v	k7c6
přírodě	příroda	k1gFnSc6
</s>
<s>
Opracování	opracování	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
)	)	kIx)
štípání	štípání	k1gNnSc1
podle	podle	k7c2
krystalické	krystalický	k2eAgFnSc2d1
mřížky	mřížka	k1gFnSc2
<g/>
;	;	kIx,
2	#num#	k4
<g/>
)	)	kIx)
leštění	leštění	k1gNnSc1
a	a	k8xC
tvarování	tvarování	k1gNnSc1
<g/>
:	:	kIx,
leštěný	leštěný	k2eAgInSc1d1
valounek	valounek	k1gInSc1
drahokamu	drahokam	k1gInSc2
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
kabošon	kabošon	k1gInSc1
(	(	kIx(
<g/>
z	z	k7c2
franc	franc	k1gFnSc1
<g/>
.	.	kIx.
cabochon	cabochon	k1gInSc1
<g/>
,	,	kIx,
německy	německy	k6eAd1
mugl	mugnout	k5eAaPmAgMnS
<g/>
)	)	kIx)
<g/>
;	;	kIx,
strojní	strojní	k2eAgNnSc1d1
leštění	leštění	k1gNnSc1
se	se	k3xPyFc4
provádí	provádět	k5eAaImIp3nS
ve	v	k7c6
vibrační	vibrační	k2eAgFnSc6d1
<g/>
,	,	kIx,
odstředivé	odstředivý	k2eAgFnSc6d1
omílací	omílací	k2eAgFnSc3d1
nebo	nebo	k8xC
rotační	rotační	k2eAgFnSc3d1
leštičce	leštička	k1gFnSc3
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
tradiční	tradiční	k2eAgNnSc1d1
bylo	být	k5eAaImAgNnS
otloukání	otloukání	k1gNnSc1
a	a	k8xC
ohlazování	ohlazování	k1gNnSc1
kaménků	kamének	k1gInPc2
o	o	k7c4
sebe	sebe	k3xPyFc4
navzájem	navzájem	k6eAd1
(	(	kIx(
<g/>
tzv.	tzv.	kA
tromlování	tromlování	k1gNnSc4
v	v	k7c6
koženém	kožený	k2eAgInSc6d1
pytli	pytel	k1gInSc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
3	#num#	k4
<g/>
)	)	kIx)
broušení	broušení	k1gNnPc4
do	do	k7c2
symetrických	symetrický	k2eAgInPc2d1
tvarů	tvar	k1gInPc2
s	s	k7c7
ploškami	ploška	k1gFnPc7
(	(	kIx(
<g/>
facetami	faceta	k1gFnPc7
<g/>
)	)	kIx)
a	a	k8xC
hranami	hrana	k1gFnPc7
<g/>
:	:	kIx,
podle	podle	k7c2
počtu	počet	k1gInSc2
facet	faceta	k1gFnPc2
se	se	k3xPyFc4
brusy	brus	k1gInPc1
nazývají	nazývat	k5eAaImIp3nP
<g/>
:	:	kIx,
tabulkovec	tabulkovec	k1gInSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
má	mít	k5eAaImIp3nS
po	po	k7c6
stranách	strana	k1gFnPc6
tabulky	tabulka	k1gFnSc2
facety	faceta	k1gFnSc2
<g/>
,	,	kIx,
speciální	speciální	k2eAgInSc1d1
typ	typ	k1gInSc1
tabulkovce	tabulkovec	k1gInSc2
je	být	k5eAaImIp3nS
smaragdový	smaragdový	k2eAgInSc1d1
brus	brus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
polštářový	polštářový	k2eAgInSc1d1
brus	brus	k1gInSc1
<g/>
,	,	kIx,
kapka	kapka	k1gFnSc1
<g/>
,	,	kIx,
bageta	bageta	k1gFnSc1
či	či	k8xC
ovísek	ovísek	k1gInSc1
<g/>
,	,	kIx,
routa	routa	k1gFnSc1
<g/>
,	,	kIx,
markýz	markýz	k1gMnSc1
<g/>
(	(	kIx(
<g/>
a	a	k8xC
<g/>
)	)	kIx)
<g/>
,	,	kIx,
srdčitý	srdčitý	k2eAgMnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
další	další	k2eAgMnSc1d1
<g/>
;	;	kIx,
nejdokonalejší	dokonalý	k2eAgMnSc1d3
<g />
.	.	kIx.
</s>
<s hack="1">
je	být	k5eAaImIp3nS
briliantový	briliantový	k2eAgInSc1d1
brus	brus	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
u	u	k7c2
drahokamů	drahokam	k1gInPc2
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
u	u	k7c2
skla	sklo	k1gNnSc2
<g/>
.	.	kIx.
4	#num#	k4
<g/>
)	)	kIx)
řezání	řezání	k1gNnSc2
<g/>
:	:	kIx,
u	u	k7c2
drahokamů	drahokam	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
ve	v	k7c6
velkých	velký	k2eAgInPc6d1
formátech	formát	k1gInPc6
<g/>
;	;	kIx,
řezáním	řezání	k1gNnSc7
nádob	nádoba	k1gFnPc2
z	z	k7c2
drahokamů	drahokam	k1gInPc2
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
glyptika	glyptika	k1gFnSc1
<g/>
;	;	kIx,
5	#num#	k4
<g/>
)	)	kIx)
rytí	rytí	k1gNnSc6
<g/>
:	:	kIx,
rytím	rytí	k1gNnSc7
reliéfů	reliéf	k1gInPc2
<g/>
,	,	kIx,
symbolů	symbol	k1gInPc2
či	či	k8xC
písmen	písmeno	k1gNnPc2
se	se	k3xPyFc4
zdobí	zdobit	k5eAaImIp3nP
drobné	drobný	k2eAgInPc1d1
drahokamy	drahokam	k1gInPc1
<g/>
,	,	kIx,
užívané	užívaný	k2eAgInPc1d1
ve	v	k7c6
šperku	šperk	k1gInSc6
<g/>
,	,	kIx,
zejména	zejména	k9
do	do	k7c2
pečetních	pečetní	k2eAgInPc2d1
prstenů	prsten	k1gInPc2
<g/>
,	,	kIx,
nazývají	nazývat	k5eAaImIp3nP
se	se	k3xPyFc4
gemy	gem	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Cena	cena	k1gFnSc1
</s>
<s>
Hodnota	hodnota	k1gFnSc1
drahokamu	drahokam	k1gInSc2
se	se	k3xPyFc4
nemusí	muset	k5eNaImIp3nS
rovnat	rovnat	k5eAaImF
jeho	jeho	k3xOp3gFnSc3
tržní	tržní	k2eAgFnSc3d1
ceně	cena	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Závisí	záviset	k5eAaImIp3nS
především	především	k9
na	na	k7c6
výše	vysoce	k6eAd2
popsaných	popsaný	k2eAgNnPc6d1
kritériích	kritérion	k1gNnPc6
kvality	kvalita	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
dále	daleko	k6eAd2
na	na	k7c6
vzácnosti	vzácnost	k1gFnSc6
výskytu	výskyt	k1gInSc2
v	v	k7c6
přírodě	příroda	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cenu	cena	k1gFnSc4
drahokamů	drahokam	k1gInPc2
může	moct	k5eAaImIp3nS
ovlivňovat	ovlivňovat	k5eAaImF
také	také	k9
existence	existence	k1gFnSc1
jejich	jejich	k3xOp3gFnPc2
syntetických	syntetický	k2eAgFnPc2d1
napodobenin	napodobenina	k1gFnPc2
(	(	kIx(
<g/>
místo	místo	k7c2
diamantu	diamant	k1gInSc2
je	být	k5eAaImIp3nS
v	v	k7c6
posledních	poslední	k2eAgNnPc6d1
desetiletích	desetiletí	k1gNnPc6
oblíbená	oblíbený	k2eAgFnSc1d1
průmyslově	průmyslově	k6eAd1
vyráběná	vyráběný	k2eAgFnSc1d1
cubic	cubice	k1gInPc2
zirconia	zirconium	k1gNnSc2
neboli	neboli	k8xC
zirkonia	zirkonium	k1gNnSc2
<g/>
,	,	kIx,
od	od	k7c2
počátku	počátek	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
často	často	k6eAd1
užívá	užívat	k5eAaImIp3nS
syntetický	syntetický	k2eAgInSc4d1
rubín	rubín	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
současnosti	současnost	k1gFnSc6
také	také	k9
safír	safír	k1gInSc4
<g/>
,	,	kIx,
akvamarín	akvamarín	k1gInSc4
a	a	k8xC
všechny	všechen	k3xTgFnPc1
další	další	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cena	cena	k1gFnSc1
drahokamu	drahokam	k1gInSc2
ve	v	k7c6
šperku	šperk	k1gInSc6
závisí	záviset	k5eAaImIp3nS
také	také	k9
na	na	k7c6
momentální	momentální	k2eAgFnSc6d1
módě	móda	k1gFnSc6
<g/>
:	:	kIx,
v	v	k7c6
posledních	poslední	k2eAgNnPc6d1
letech	léto	k1gNnPc6
se	se	k3xPyFc4
více	hodně	k6eAd2
cení	cenit	k5eAaImIp3nS
velikost	velikost	k1gFnSc4
drahokamů	drahokam	k1gInPc2
na	na	k7c4
úkor	úkor	k1gInSc4
klenotnického	klenotnický	k2eAgNnSc2d1
zpracování	zpracování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Výběr	výběr	k1gInSc1
drahokamů	drahokam	k1gInPc2
podle	podle	k7c2
tvrdosti	tvrdost	k1gFnSc2
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
diamant	diamant	k1gInSc1
–	–	k?
často	často	k6eAd1
bezbarvý	bezbarvý	k2eAgMnSc1d1
nebo	nebo	k8xC
žlutý	žlutý	k2eAgMnSc1d1
<g/>
,	,	kIx,
existuje	existovat	k5eAaImIp3nS
však	však	k9
ve	v	k7c6
všech	všecek	k3xTgFnPc6
barevných	barevný	k2eAgFnPc6d1
varietách	varieta	k1gFnPc6
<g/>
;	;	kIx,
JAR	jar	k1gFnSc1
<g/>
,	,	kIx,
střední	střední	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
<g/>
,	,	kIx,
Sibiř	Sibiř	k1gFnSc1
,	,	kIx,
Brazílie	Brazílie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
drahokamových	drahokamový	k2eAgFnPc2d1
odrůd	odrůda	k1gFnPc2
korundu	korund	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
safír	safír	k1gInSc1
<g/>
;	;	kIx,
modrý	modrý	k2eAgInSc1d1
<g/>
;	;	kIx,
Barma	Barma	k1gFnSc1
<g/>
,	,	kIx,
Srí	Srí	k1gFnSc1
Lanka	lanko	k1gNnSc2
<g/>
;	;	kIx,
oranžové	oranžový	k2eAgFnPc1d1
a	a	k8xC
žluté	žlutý	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
levnější	levný	k2eAgFnPc1d2
<g/>
,	,	kIx,
oranžové	oranžový	k2eAgFnPc1d1
padparádža	padparádža	k6eAd1
naopak	naopak	k6eAd1
velmi	velmi	k6eAd1
ceněné	ceněný	k2eAgNnSc1d1
<g/>
;	;	kIx,
bezbarvý	bezbarvý	k2eAgMnSc1d1
je	být	k5eAaImIp3nS
leukosafír	leukosafír	k1gInSc1
</s>
<s>
rubín	rubín	k1gInSc1
(	(	kIx(
<g/>
červený	červený	k2eAgInSc1d1
<g/>
;	;	kIx,
Barma	Barma	k1gFnSc1
<g/>
,	,	kIx,
Srí	Srí	k1gFnPc1
Lanka	lanko	k1gNnSc2
cení	cenit	k5eAaImIp3nP
se	se	k3xPyFc4
mnohem	mnohem	k6eAd1
výše	vysoce	k6eAd2
než	než	k8xS
z	z	k7c2
Afriky	Afrika	k1gFnSc2
<g/>
,	,	kIx,
Vietnamu	Vietnam	k1gInSc2
<g/>
,	,	kIx,
Kambodži	Kambodža	k1gFnSc6
a	a	k8xC
Thajska	Thajsko	k1gNnSc2
<g/>
,	,	kIx,
Afghánistánu	Afghánistán	k1gInSc2
a	a	k8xC
tu	tu	k6eAd1
a	a	k8xC
tam	tam	k6eAd1
Kašmíru	Kašmír	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
topaz	topaz	k1gInSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
spinel	spinel	k1gInSc4
(	(	kIx(
<g/>
různých	různý	k2eAgFnPc2d1
barev	barva	k1gFnPc2
<g/>
,	,	kIx,
např.	např.	kA
<g/>
:	:	kIx,
fialová	fialový	k2eAgFnSc1d1
<g/>
,	,	kIx,
modrá	modrý	k2eAgFnSc1d1
<g/>
,	,	kIx,
bílá	bílý	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
;	;	kIx,
<g/>
Tanzanie	Tanzanie	k1gFnSc1
<g/>
,	,	kIx,
Srí	Srí	k1gFnSc1
Lanka	lanko	k1gNnSc2
<g/>
,	,	kIx,
Tádžikistán	Tádžikistán	k1gInSc1
<g/>
,	,	kIx,
Barma	Barma	k1gFnSc1
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
;	;	kIx,
často	často	k6eAd1
v	v	k7c6
korunovačních	korunovační	k2eAgInPc6d1
klenotech	klenot	k1gInPc6
<g/>
)	)	kIx)
<g/>
;	;	kIx,
mísí	mísit	k5eAaImIp3nP
se	se	k3xPyFc4
s	s	k7c7
dalšími	další	k2eAgInPc7d1
drahokamy	drahokam	k1gInPc7
<g/>
:	:	kIx,
balasrubín	balasrubín	k1gMnSc1
<g/>
,	,	kIx,
rubínspinel	rubínspinel	k1gMnSc1
<g/>
,	,	kIx,
safírspinel	safírspinel	k1gMnSc1
<g/>
,	,	kIx,
ametystspinel	ametystspinel	k1gMnSc1
<g/>
,	,	kIx,
almandinspinel	almandinspinel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
7,5	7,5	k4
až	až	k6eAd1
8	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
berylu	beryl	k1gInSc2
(	(	kIx(
<g/>
odrůdy	odrůda	k1gFnSc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
smaragd	smaragd	k1gInSc1
(	(	kIx(
<g/>
nejcennější	cenný	k2eAgMnSc1d3
sytě	sytě	k6eAd1
zelený	zelený	k2eAgInSc1d1
<g/>
;	;	kIx,
Kolumbie	Kolumbie	k1gFnSc1
<g/>
,	,	kIx,
Pákistán	Pákistán	k1gInSc1
<g/>
,	,	kIx,
Egypt	Egypt	k1gInSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
-	-	kIx~
Ural	Ural	k1gInSc1
<g/>
,	,	kIx,
Afrika	Afrika	k1gFnSc1
</s>
<s>
akvamarín	akvamarín	k1gInSc1
<g/>
,	,	kIx,
drahocenný	drahocenný	k2eAgInSc1d1
zelenomodré	zelenomodrý	k2eAgMnPc4d1
až	až	k9
blankytně	blankytně	k6eAd1
modré	modrý	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
<g/>
,	,	kIx,
</s>
<s>
heliodor	heliodor	k1gInSc1
zlatožlutý	zlatožlutý	k2eAgInSc1d1
<g/>
,	,	kIx,
</s>
<s>
morganit	morganit	k1gInSc1
červený	červený	k2eAgInSc1d1
a	a	k8xC
růžový	růžový	k2eAgInSc1d1
<g/>
,	,	kIx,
vorobjevit	vorobjevit	k1gInSc1
</s>
<s>
7	#num#	k4
až	až	k6eAd1
7,5	7,5	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
granátů	granát	k1gInPc2
<g/>
:	:	kIx,
pyrop	pyrop	k1gInSc1
<g/>
,	,	kIx,
almandin	almandin	k1gInSc1
<g/>
,	,	kIx,
pyralmandin	pyralmandin	k2eAgInSc1d1
a	a	k8xC
další	další	k2eAgInSc1d1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
skupina	skupina	k1gFnSc1
křemenu	křemen	k1gInSc3
<g/>
:	:	kIx,
</s>
<s>
ametyst	ametyst	k1gInSc1
<g/>
;	;	kIx,
nejvalitnější	valitný	k2eAgInSc1d3
brazilský	brazilský	k2eAgInSc1d1
je	být	k5eAaImIp3nS
sytě	sytě	k6eAd1
fialový	fialový	k2eAgInSc1d1
a	a	k8xC
průhledný	průhledný	k2eAgInSc1d1
<g/>
,	,	kIx,
český	český	k2eAgInSc1d1
je	být	k5eAaImIp3nS
méně	málo	k6eAd2
cenný	cenný	k2eAgInSc1d1
-zakalený	-zakalený	k2eAgInSc1d1
</s>
<s>
Křišťál	křišťál	k1gInSc1
<g/>
;	;	kIx,
bezbarvý	bezbarvý	k2eAgInSc1d1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
se	se	k3xPyFc4
pro	pro	k7c4
odlišení	odlišení	k1gNnSc4
od	od	k7c2
křišťálového	křišťálový	k2eAgNnSc2d1
skla	sklo	k1gNnSc2
nazývá	nazývat	k5eAaImIp3nS
horský	horský	k2eAgInSc4d1
křišťál	křišťál	k1gInSc4
</s>
<s>
záhněda	záhněda	k1gFnSc1
<g/>
;	;	kIx,
zabarvený	zabarvený	k2eAgMnSc1d1
do	do	k7c2
hněda	hnědo	k1gNnSc2
nebo	nebo	k8xC
kouřový	kouřový	k2eAgMnSc1d1
<g/>
;	;	kIx,
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
až	až	k9
7	#num#	k4
<g/>
.	.	kIx.
chalcedon	chalcedon	k1gInSc1
<g/>
:	:	kIx,
mléčně	mléčně	k6eAd1
zakalený	zakalený	k2eAgInSc1d1
</s>
<s>
achát	achát	k1gInSc1
</s>
<s>
onyx	onyx	k1gInSc1
<g/>
,	,	kIx,
sardonyx	sardonyx	k1gInSc1
proužkovaný	proužkovaný	k2eAgInSc1d1
</s>
<s>
chryzopras	chryzopras	k1gInSc1
zelený	zelený	k2eAgInSc1d1
</s>
<s>
jaspis	jaspis	k1gInSc4
sytě	sytě	k6eAd1
zbarvený	zbarvený	k2eAgInSc4d1
<g/>
,	,	kIx,
neprůsvitný	průsvitný	k2eNgInSc4d1
</s>
<s>
5,5	5,5	k4
až	až	k6eAd1
6,5	6,5	k4
<g/>
.	.	kIx.
drahý	drahý	k2eAgInSc1d1
opál	opál	k1gInSc1
(	(	kIx(
<g/>
pestrobarevný	pestrobarevný	k2eAgMnSc1d1
<g/>
;	;	kIx,
Mexiko	Mexiko	k1gNnSc1
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc1
<g/>
,	,	kIx,
Slovensko	Slovensko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
seznam	seznam	k1gInSc1
drahokamů	drahokam	k1gInPc2
</s>
<s>
polodrahokam	polodrahokam	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Ďuďa	Ďuďa	k1gMnSc1
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
Rejl	Rejl	k1gMnSc1
<g/>
:	:	kIx,
Svět	svět	k1gInSc1
drahých	drahý	k2eAgInPc2d1
kamenů	kámen	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Granit	granit	k1gInSc1
Praha	Praha	k1gFnSc1
2002	#num#	k4
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Ďuďa	Ďuďa	k1gMnSc1
<g/>
,	,	kIx,
Luboš	Luboš	k1gMnSc1
Rejl	Rejla	k1gFnPc2
<g/>
:	:	kIx,
Drahé	drahý	k2eAgInPc1d1
kameny	kámen	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aventinum	Aventinum	k1gInSc1
Praha	Praha	k1gFnSc1
2001	#num#	k4
(	(	kIx(
<g/>
s	s	k7c7
CD	CD	kA
ROMem	Rom	k1gMnSc7
k	k	k7c3
určování	určování	k1gNnSc3
kamenů	kámen	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
drahokam	drahokam	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
drahokam	drahokam	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4052949-6	4052949-6	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
4803	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85053712	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85053712	#num#	k4
</s>
