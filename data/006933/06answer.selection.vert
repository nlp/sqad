<s>
Groucho	Groucho	k6eAd1	Groucho
Marx	Marx	k1gMnSc1	Marx
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Julius	Julius	k1gMnSc1	Julius
Henry	Henry	k1gMnSc1	Henry
Marx	Marx	k1gMnSc1	Marx
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
komik	komik	k1gMnSc1	komik
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
komiků	komik	k1gMnPc2	komik
amerického	americký	k2eAgInSc2d1	americký
zvukového	zvukový	k2eAgInSc2d1	zvukový
filmu	film	k1gInSc2	film
30	[number]	k4	30
<g/>
.	.	kIx.	.
a	a	k8xC	a
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
spolutvořil	spolutvořit	k5eAaImAgInS	spolutvořit
komediální	komediální	k2eAgFnSc4d1	komediální
skupinu	skupina	k1gFnSc4	skupina
Bratři	bratr	k1gMnPc1	bratr
Marxové	Marxové	k2eAgNnSc2d1	Marxové
<g/>
.	.	kIx.	.
</s>
