<s>
Jaké	jaký	k3yIgNnSc1	jaký
je	být	k5eAaImIp3nS	být
vlastní	vlastní	k2eAgNnSc4d1	vlastní
jméno	jméno	k1gNnSc4	jméno
Groucho	Groucha	k1gFnSc5	Groucha
Marxe	Marx	k1gMnSc4	Marx
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
komiků	komik	k1gMnPc2	komik
amerického	americký	k2eAgInSc2d1	americký
zvukového	zvukový	k2eAgInSc2d1	zvukový
filmu	film	k1gInSc2	film
30	[number]	k4	30
<g/>
.	.	kIx.	.
a	a	k8xC	a
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
spolutvořil	spolutvořit	k5eAaImAgInS	spolutvořit
komediální	komediální	k2eAgFnSc4d1	komediální
skupinu	skupina	k1gFnSc4	skupina
Bratři	bratr	k1gMnPc1	bratr
Marxové	Marxové	k2eAgNnSc2d1	Marxové
<g/>
?	?	kIx.	?
</s>
