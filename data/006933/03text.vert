<s>
Groucho	Groucho	k6eAd1	Groucho
Marx	Marx	k1gMnSc1	Marx
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Julius	Julius	k1gMnSc1	Julius
Henry	Henry	k1gMnSc1	Henry
Marx	Marx	k1gMnSc1	Marx
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1890	[number]	k4	1890
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1977	[number]	k4	1977
<g/>
,	,	kIx,	,
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
komik	komik	k1gMnSc1	komik
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
komiků	komik	k1gMnPc2	komik
amerického	americký	k2eAgInSc2d1	americký
zvukového	zvukový	k2eAgInSc2d1	zvukový
filmu	film	k1gInSc2	film
30	[number]	k4	30
<g/>
.	.	kIx.	.
a	a	k8xC	a
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
spolutvořil	spolutvořit	k5eAaImAgInS	spolutvořit
komediální	komediální	k2eAgFnSc4d1	komediální
skupinu	skupina	k1gFnSc4	skupina
Bratři	bratr	k1gMnPc1	bratr
Marxové	Marxové	k2eAgNnSc2d1	Marxové
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
znám	znát	k5eAaImIp1nS	znát
svými	svůj	k3xOyFgFnPc7	svůj
ironickými	ironický	k2eAgFnPc7d1	ironická
<g/>
,	,	kIx,	,
sebeironickými	sebeironický	k2eAgFnPc7d1	sebeironická
a	a	k8xC	a
parodickými	parodický	k2eAgFnPc7d1	parodická
vtipnými	vtipný	k2eAgFnPc7d1	vtipná
poznámkami	poznámka	k1gFnPc7	poznámka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
karikoval	karikovat	k5eAaImAgInS	karikovat
americkou	americký	k2eAgFnSc4d1	americká
maloměšťáckou	maloměšťácký	k2eAgFnSc4d1	maloměšťácká
podnikatelskou	podnikatelský	k2eAgFnSc4d1	podnikatelská
třídu	třída	k1gFnSc4	třída
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
své	svůj	k3xOyFgFnPc4	svůj
celoživotní	celoživotní	k2eAgFnPc4d1	celoživotní
zásluhy	zásluha	k1gFnPc4	zásluha
o	o	k7c4	o
americký	americký	k2eAgInSc4d1	americký
film	film	k1gInSc4	film
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
Oscarem	Oscar	k1gMnSc7	Oscar
<g/>
.	.	kIx.	.
1933	[number]	k4	1933
Kachní	kachní	k2eAgFnSc1d1	kachní
polévka	polévka	k1gFnSc1	polévka
1935	[number]	k4	1935
Noc	noc	k1gFnSc1	noc
v	v	k7c6	v
opeře	opera	k1gFnSc6	opera
1937	[number]	k4	1937
Kobylkáři	kobylkář	k1gMnPc1	kobylkář
1946	[number]	k4	1946
Noc	noc	k1gFnSc4	noc
v	v	k7c6	v
Casablance	Casablanca	k1gFnSc6	Casablanca
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Groucho	Groucha	k1gFnSc5	Groucha
Marx	Marx	k1gMnSc1	Marx
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Groucho	Groucha	k1gFnSc5	Groucha
Marx	Marx	k1gMnSc1	Marx
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
