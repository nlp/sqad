<s>
Dinosaury	dinosaurus	k1gMnPc4	dinosaurus
jako	jako	k8xC	jako
předky	předek	k1gMnPc4	předek
ptáků	pták	k1gMnPc2	pták
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
již	již	k6eAd1	již
Carl	Carl	k1gMnSc1	Carl
Gegenbaur	Gegenbaur	k1gMnSc1	Gegenbaur
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
a	a	k8xC	a
jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
nejstarší	starý	k2eAgFnSc4d3	nejstarší
přesně	přesně	k6eAd1	přesně
zformulovanou	zformulovaný	k2eAgFnSc4d1	zformulovaná
hypotézu	hypotéza	k1gFnSc4	hypotéza
o	o	k7c6	o
původu	původ	k1gInSc6	původ
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
