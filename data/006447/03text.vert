<s>
Liga	liga	k1gFnSc1	liga
výjimečných	výjimečný	k2eAgMnPc2d1	výjimečný
(	(	kIx(	(
<g/>
v	v	k7c6	v
anglickém	anglický	k2eAgInSc6d1	anglický
originále	originál	k1gInSc6	originál
The	The	k1gMnSc1	The
League	League	k1gNnSc2	League
of	of	k?	of
Extraordinary	Extraordinar	k1gInPc1	Extraordinar
Gentlemen	Gentlemen	k1gInSc1	Gentlemen
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
komiksová	komiksový	k2eAgFnSc1d1	komiksová
série	série	k1gFnSc1	série
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
scenáristou	scenárista	k1gMnSc7	scenárista
Alanem	Alan	k1gMnSc7	Alan
Moorem	Moor	k1gMnSc7	Moor
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
byla	být	k5eAaImAgFnS	být
vydávaná	vydávaný	k2eAgFnSc1d1	vydávaná
vydavatelstvím	vydavatelství	k1gNnPc3	vydavatelství
WildStorm	WildStorm	k1gInSc1	WildStorm
(	(	kIx(	(
<g/>
součást	součást	k1gFnSc1	součást
DC	DC	kA	DC
Comics	comics	k1gInSc1	comics
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
ji	on	k3xPp3gFnSc4	on
vydává	vydávat	k5eAaImIp3nS	vydávat
vydavatelství	vydavatelství	k1gNnSc1	vydavatelství
Top	topit	k5eAaImRp2nS	topit
Shelf	Shelf	k1gMnSc1	Shelf
Productions	Productions	k1gInSc1	Productions
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
postavami	postava	k1gFnPc7	postava
jsou	být	k5eAaImIp3nP	být
superhrdinové	superhrdinový	k2eAgInPc1d1	superhrdinový
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
–	–	k?	–
původně	původně	k6eAd1	původně
postavy	postava	k1gFnPc1	postava
klasické	klasický	k2eAgFnSc2d1	klasická
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
této	tento	k3xDgFnSc2	tento
komiksové	komiksový	k2eAgFnSc2d1	komiksová
série	série	k1gFnSc2	série
upraveny	upravit	k5eAaPmNgInP	upravit
<g/>
.	.	kIx.	.
</s>
<s>
Vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
zde	zde	k6eAd1	zde
např.	např.	kA	např.
Mina	mina	k1gFnSc1	mina
Murrayová	Murrayová	k1gFnSc1	Murrayová
z	z	k7c2	z
Drákuly	Drákula	k1gMnSc2	Drákula
<g/>
,	,	kIx,	,
Allan	Allan	k1gMnSc1	Allan
Quatermain	Quatermain	k1gMnSc1	Quatermain
z	z	k7c2	z
Dolů	dol	k1gInPc2	dol
krále	král	k1gMnSc4	král
Šalamouna	Šalamoun	k1gMnSc4	Šalamoun
<g/>
,	,	kIx,	,
kapitán	kapitán	k1gMnSc1	kapitán
Nemo	Nemo	k6eAd1	Nemo
z	z	k7c2	z
Dvaceti	dvacet	k4xCc2	dvacet
tisíc	tisíc	k4xCgInPc2	tisíc
mil	míle	k1gFnPc2	míle
pod	pod	k7c7	pod
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
doktor	doktor	k1gMnSc1	doktor
Henry	Henry	k1gMnSc1	Henry
Jekyll	Jekyll	k1gMnSc1	Jekyll
z	z	k7c2	z
Podivného	podivný	k2eAgInSc2d1	podivný
případu	případ	k1gInSc2	případ
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Jekylla	Jekyll	k1gMnSc2	Jekyll
a	a	k8xC	a
pana	pan	k1gMnSc2	pan
Hyda	Hydus	k1gMnSc2	Hydus
či	či	k8xC	či
Hawley	Hawlea	k1gMnSc2	Hawlea
Griffin	Griffina	k1gFnPc2	Griffina
z	z	k7c2	z
Neviditelného	viditelný	k2eNgNnSc2d1	neviditelné
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
první	první	k4xOgFnSc2	první
řady	řada	k1gFnSc2	řada
byl	být	k5eAaImAgInS	být
zasazen	zasadit	k5eAaPmNgInS	zasadit
do	do	k7c2	do
viktoriánského	viktoriánský	k2eAgNnSc2d1	viktoriánské
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
příběhy	příběh	k1gInPc1	příběh
dalších	další	k2eAgInPc2d1	další
komiksů	komiks	k1gInPc2	komiks
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
i	i	k9	i
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
komiksové	komiksový	k2eAgFnSc2d1	komiksová
série	série	k1gFnSc2	série
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
natočen	natočen	k2eAgInSc1d1	natočen
film	film	k1gInSc1	film
Liga	liga	k1gFnSc1	liga
výjimečných	výjimečný	k2eAgNnPc2d1	výjimečné
<g/>
.	.	kIx.	.
</s>
