<s>
Hvězda	Hvězda	k1gMnSc1	Hvězda
nebo	nebo	k8xC	nebo
zastarale	zastarale	k6eAd1	zastarale
stálice	stálice	k1gFnSc1	stálice
je	být	k5eAaImIp3nS	být
plazmové	plazmový	k2eAgNnSc1d1	plazmové
(	(	kIx(	(
<g/>
plynné	plynný	k2eAgNnSc1d1	plynné
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
kulovité	kulovitý	k2eAgNnSc1d1	kulovité
těleso	těleso	k1gNnSc1	těleso
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
zdroj	zdroj	k1gInSc4	zdroj
viditelného	viditelný	k2eAgNnSc2d1	viditelné
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
drží	držet	k5eAaImIp3nS	držet
ho	on	k3xPp3gMnSc4	on
pohromadě	pohromadě	k6eAd1	pohromadě
jeho	jeho	k3xOp3gFnSc1	jeho
vlastní	vlastní	k2eAgFnSc1d1	vlastní
gravitace	gravitace	k1gFnSc1	gravitace
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
0,08	[number]	k4	0,08
až	až	k8xS	až
300	[number]	k4	300
hmotností	hmotnost	k1gFnSc7	hmotnost
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
hvězdách	hvězda	k1gFnPc6	hvězda
je	být	k5eAaImIp3nS	být
soustředěna	soustředit	k5eAaPmNgFnS	soustředit
většina	většina	k1gFnSc1	většina
viditelné	viditelný	k2eAgFnSc2d1	viditelná
hmoty	hmota	k1gFnSc2	hmota
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližší	blízký	k2eAgFnSc7d3	nejbližší
hvězdou	hvězda	k1gFnSc7	hvězda
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
je	být	k5eAaImIp3nS	být
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
většiny	většina	k1gFnSc2	většina
energie	energie	k1gFnSc2	energie
naší	náš	k3xOp1gFnSc2	náš
planety	planeta	k1gFnSc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vhodných	vhodný	k2eAgFnPc6d1	vhodná
atmosférických	atmosférický	k2eAgFnPc6d1	atmosférická
podmínkách	podmínka	k1gFnPc6	podmínka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
viditelné	viditelný	k2eAgFnSc2d1	viditelná
i	i	k8xC	i
jiné	jiný	k2eAgFnSc2d1	jiná
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
obrovským	obrovský	k2eAgFnPc3d1	obrovská
vzdálenostem	vzdálenost	k1gFnPc3	vzdálenost
vypadají	vypadat	k5eAaImIp3nP	vypadat
jako	jako	k8xS	jako
množství	množství	k1gNnSc4	množství
nehybných	hybný	k2eNgInPc2d1	nehybný
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
blikajících	blikající	k2eAgInPc2d1	blikající
světelných	světelný	k2eAgInPc2d1	světelný
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
pojmem	pojem	k1gInSc7	pojem
hvězda	hvězda	k1gFnSc1	hvězda
se	se	k3xPyFc4	se
ve	v	k7c6	v
starém	staré	k1gNnSc6	staré
chápání	chápání	k1gNnSc2	chápání
myslel	myslet	k5eAaImAgInS	myslet
téměř	téměř	k6eAd1	téměř
každý	každý	k3xTgInSc1	každý
objekt	objekt	k1gInSc1	objekt
na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
jako	jako	k9	jako
planeta	planeta	k1gFnSc1	planeta
<g/>
,	,	kIx,	,
kometa	kometa	k1gFnSc1	kometa
atd.	atd.	kA	atd.
kromě	kromě	k7c2	kromě
Měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
astronomickém	astronomický	k2eAgInSc6d1	astronomický
významu	význam	k1gInSc6	význam
jsou	být	k5eAaImIp3nP	být
hvězdy	hvězda	k1gFnSc2	hvězda
ty	ten	k3xDgInPc1	ten
kosmické	kosmický	k2eAgInPc1d1	kosmický
kulovité	kulovitý	k2eAgInPc1d1	kulovitý
objekty	objekt	k1gInPc1	objekt
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
vlastní	vlastní	k2eAgInSc4d1	vlastní
zdroj	zdroj	k1gInSc4	zdroj
viditelného	viditelný	k2eAgNnSc2d1	viditelné
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
<g/>
,	,	kIx,	,
přeneseně	přeneseně	k6eAd1	přeneseně
zvané	zvaný	k2eAgInPc1d1	zvaný
"	"	kIx"	"
<g/>
život	život	k1gInSc1	život
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
tohoto	tento	k3xDgNnSc2	tento
záření	záření	k1gNnSc2	záření
hvězd	hvězda	k1gFnPc2	hvězda
termonukleární	termonukleární	k2eAgFnSc2d1	termonukleární
fúze	fúze	k1gFnSc2	fúze
vodíku	vodík	k1gInSc2	vodík
na	na	k7c4	na
helium	helium	k1gNnSc4	helium
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
vnitřkem	vnitřek	k1gInSc7	vnitřek
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vyzářena	vyzářit	k5eAaPmNgFnS	vyzářit
do	do	k7c2	do
vnějšího	vnější	k2eAgInSc2d1	vnější
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyčerpala	vyčerpat	k5eAaPmAgFnS	vyčerpat
zásoby	zásoba	k1gFnPc4	zásoba
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
dostatečně	dostatečně	k6eAd1	dostatečně
hmotná	hmotný	k2eAgFnSc1d1	hmotná
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
ve	v	k7c6	v
hvězdě	hvězda	k1gFnSc6	hvězda
chemické	chemický	k2eAgInPc4d1	chemický
prvky	prvek	k1gInPc4	prvek
těžší	těžký	k2eAgInPc4d2	těžší
než	než	k8xS	než
helium	helium	k1gNnSc4	helium
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
koncem	konec	k1gInSc7	konec
života	život	k1gInSc2	život
mohou	moct	k5eAaImIp3nP	moct
hvězdy	hvězda	k1gFnPc4	hvězda
obsahovat	obsahovat	k5eAaImF	obsahovat
degenerovanou	degenerovaný	k2eAgFnSc4d1	degenerovaná
hmotu	hmota	k1gFnSc4	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
zjišťují	zjišťovat	k5eAaImIp3nP	zjišťovat
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
,	,	kIx,	,
věk	věk	k1gInSc4	věk
<g/>
,	,	kIx,	,
metalicitu	metalicita	k1gFnSc4	metalicita
(	(	kIx(	(
<g/>
chemické	chemický	k2eAgNnSc1d1	chemické
složení	složení	k1gNnSc1	složení
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnohé	mnohý	k2eAgFnPc4d1	mnohá
další	další	k2eAgFnPc4d1	další
vlastnosti	vlastnost	k1gFnPc4	vlastnost
pomocí	pomocí	k7c2	pomocí
pozorování	pozorování	k1gNnSc2	pozorování
pohybu	pohyb	k1gInSc2	pohyb
hvězdy	hvězda	k1gFnSc2	hvězda
vesmírem	vesmír	k1gInSc7	vesmír
<g/>
,	,	kIx,	,
svítivosti	svítivost	k1gFnSc2	svítivost
a	a	k8xC	a
analýzou	analýza	k1gFnSc7	analýza
jejího	její	k3xOp3gNnSc2	její
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Graf	graf	k1gInSc1	graf
porovnávající	porovnávající	k2eAgFnSc4d1	porovnávající
teplotu	teplota	k1gFnSc4	teplota
hvězd	hvězda	k1gFnPc2	hvězda
s	s	k7c7	s
jejich	jejich	k3xOp3gFnSc7	jejich
svítivostí	svítivost	k1gFnSc7	svítivost
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xC	jako
Hertzsprungův-Russellův	Hertzsprungův-Russellův	k2eAgInSc1d1	Hertzsprungův-Russellův
diagram	diagram	k1gInSc1	diagram
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
zjistit	zjistit	k5eAaPmF	zjistit
věk	věk	k1gInSc4	věk
a	a	k8xC	a
stav	stav	k1gInSc4	stav
vývoje	vývoj	k1gInSc2	vývoj
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	hvězda	k1gFnSc1	hvězda
začíná	začínat	k5eAaImIp3nS	začínat
jako	jako	k9	jako
kolabující	kolabující	k2eAgInSc4d1	kolabující
mrak	mrak	k1gInSc4	mrak
materiálu	materiál	k1gInSc2	materiál
složený	složený	k2eAgInSc4d1	složený
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
hélia	hélium	k1gNnSc2	hélium
a	a	k8xC	a
stopových	stopový	k2eAgNnPc2d1	stopové
množství	množství	k1gNnPc2	množství
těžších	těžký	k2eAgInPc2d2	těžší
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
jádro	jádro	k1gNnSc1	jádro
hvězdy	hvězda	k1gFnSc2	hvězda
dostatečné	dostatečný	k2eAgFnSc2d1	dostatečná
hustoty	hustota	k1gFnSc2	hustota
<g/>
,	,	kIx,	,
vodík	vodík	k1gInSc1	vodík
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
termonukleární	termonukleární	k2eAgFnSc7d1	termonukleární
fúzí	fúze	k1gFnSc7	fúze
přeměňovat	přeměňovat	k5eAaImF	přeměňovat
na	na	k7c4	na
helium	helium	k1gNnSc4	helium
a	a	k8xC	a
vyzařovat	vyzařovat	k5eAaImF	vyzařovat
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc1	přenos
energie	energie	k1gFnSc2	energie
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
jádra	jádro	k1gNnSc2	jádro
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
hvězdy	hvězda	k1gFnSc2	hvězda
je	být	k5eAaImIp3nS	být
kombinací	kombinace	k1gFnSc7	kombinace
procesů	proces	k1gInPc2	proces
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
konvekce	konvekce	k1gFnSc2	konvekce
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vzniklý	vzniklý	k2eAgInSc1d1	vzniklý
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
tlak	tlak	k1gInSc1	tlak
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
hvězda	hvězda	k1gFnSc1	hvězda
zkolabovala	zkolabovat	k5eAaPmAgFnS	zkolabovat
pod	pod	k7c7	pod
vlastní	vlastní	k2eAgFnSc7d1	vlastní
gravitací	gravitace	k1gFnSc7	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
větší	veliký	k2eAgFnSc7d2	veliký
než	než	k8xS	než
0,4	[number]	k4	0,4
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
po	po	k7c6	po
vyčerpání	vyčerpání	k1gNnSc6	vyčerpání
vodíku	vodík	k1gInSc2	vodík
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
expandují	expandovat	k5eAaImIp3nP	expandovat
a	a	k8xC	a
stávají	stávat	k5eAaImIp3nP	stávat
se	se	k3xPyFc4	se
červeným	červený	k2eAgMnSc7d1	červený
obrem	obr	k1gMnSc7	obr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
vznikají	vznikat	k5eAaImIp3nP	vznikat
fúzí	fúze	k1gFnSc7	fúze
těžší	těžký	k2eAgInPc1d2	těžší
prvky	prvek	k1gInPc1	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
hvězda	hvězda	k1gFnSc1	hvězda
vyvine	vyvinout	k5eAaPmIp3nS	vyvinout
do	do	k7c2	do
degenerovaného	degenerovaný	k2eAgInSc2d1	degenerovaný
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
její	její	k3xOp3gFnSc2	její
hmoty	hmota	k1gFnSc2	hmota
rozptýlena	rozptýlit	k5eAaPmNgFnS	rozptýlit
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
jako	jako	k8xC	jako
mezihvězdná	mezihvězdný	k2eAgFnSc1d1	mezihvězdná
hmota	hmota	k1gFnSc1	hmota
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
později	pozdě	k6eAd2	pozdě
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
nová	nový	k2eAgFnSc1d1	nová
generace	generace	k1gFnSc1	generace
hvězd	hvězda	k1gFnPc2	hvězda
s	s	k7c7	s
vyšším	vysoký	k2eAgInSc7d2	vyšší
podílem	podíl	k1gInSc7	podíl
těžších	těžký	k2eAgInPc2d2	těžší
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
hvězdy	hvězda	k1gFnSc2	hvězda
se	se	k3xPyFc4	se
změní	změnit	k5eAaPmIp3nS	změnit
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c4	na
její	její	k3xOp3gFnPc4	její
původní	původní	k2eAgFnPc4d1	původní
hmostnosti	hmostnost	k1gFnPc4	hmostnost
<g/>
,	,	kIx,	,
na	na	k7c4	na
bílého	bílý	k2eAgMnSc4d1	bílý
trpaslíka	trpaslík	k1gMnSc4	trpaslík
<g/>
,	,	kIx,	,
neutronovou	neutronový	k2eAgFnSc4d1	neutronová
hvězdu	hvězda	k1gFnSc4	hvězda
nebo	nebo	k8xC	nebo
černou	černý	k2eAgFnSc4d1	černá
díru	díra	k1gFnSc4	díra
<g/>
.	.	kIx.	.
</s>
<s>
Systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
či	či	k8xC	či
více	hodně	k6eAd2	hodně
gravitačně	gravitačně	k6eAd1	gravitačně
svázaných	svázaný	k2eAgFnPc2d1	svázaná
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k9	jako
dvounásobné	dvounásobný	k2eAgInPc1d1	dvounásobný
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
vícenásobné	vícenásobný	k2eAgNnSc1d1	vícenásobné
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
obíhají	obíhat	k5eAaImIp3nP	obíhat
příliš	příliš	k6eAd1	příliš
blízko	blízko	k6eAd1	blízko
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc7	jejich
vzájemné	vzájemný	k2eAgNnSc1d1	vzájemné
gravitační	gravitační	k2eAgNnSc1d1	gravitační
působení	působení	k1gNnSc1	působení
může	moct	k5eAaImIp3nS	moct
výrazně	výrazně	k6eAd1	výrazně
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
jejich	jejich	k3xOp3gInSc4	jejich
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
tvoří	tvořit	k5eAaImIp3nP	tvořit
část	část	k1gFnSc4	část
mnohem	mnohem	k6eAd1	mnohem
větších	veliký	k2eAgFnPc2d2	veliký
gravitačních	gravitační	k2eAgFnPc2d1	gravitační
struktur	struktura	k1gFnPc2	struktura
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
hvězdokupy	hvězdokupa	k1gFnPc1	hvězdokupa
nebo	nebo	k8xC	nebo
galaxie	galaxie	k1gFnPc1	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
se	se	k3xPyFc4	se
na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
vůči	vůči	k7c3	vůči
sobě	se	k3xPyFc3	se
navzájem	navzájem	k6eAd1	navzájem
zdánlivě	zdánlivě	k6eAd1	zdánlivě
nepohybují	pohybovat	k5eNaImIp3nP	pohybovat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
nazývaly	nazývat	k5eAaImAgFnP	nazývat
stálice	stálice	k1gFnPc1	stálice
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
bludic	bludice	k1gFnPc2	bludice
(	(	kIx(	(
<g/>
planet	planeta	k1gFnPc2	planeta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
obrovskou	obrovský	k2eAgFnSc7d1	obrovská
rychlostí	rychlost	k1gFnSc7	rychlost
až	až	k9	až
několik	několik	k4yIc1	několik
set	sto	k4xCgNnPc2	sto
kilometrů	kilometr	k1gInPc2	kilometr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
obrovské	obrovský	k2eAgFnSc3d1	obrovská
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
se	se	k3xPyFc4	se
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
pozorovatelné	pozorovatelný	k2eAgFnSc2d1	pozorovatelná
změny	změna	k1gFnSc2	změna
v	v	k7c6	v
polohách	poloha	k1gFnPc6	poloha
hvězd	hvězda	k1gFnPc2	hvězda
projeví	projevit	k5eAaPmIp3nS	projevit
až	až	k9	až
po	po	k7c6	po
staletích	staletí	k1gNnPc6	staletí
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
tisíciletích	tisíciletí	k1gNnPc6	tisíciletí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
hvězdy	hvězda	k1gFnSc2	hvězda
utvářejí	utvářet	k5eAaImIp3nP	utvářet
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
velmi	velmi	k6eAd1	velmi
<g/>
)	)	kIx)	)
výrazné	výrazný	k2eAgFnPc4d1	výrazná
konfigurace	konfigurace	k1gFnPc4	konfigurace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
této	tento	k3xDgFnSc2	tento
zdánlivé	zdánlivý	k2eAgFnSc2d1	zdánlivá
nehybnosti	nehybnost	k1gFnSc2	nehybnost
přetrvávají	přetrvávat	k5eAaImIp3nP	přetrvávat
"	"	kIx"	"
<g/>
beze	beze	k7c2	beze
změny	změna	k1gFnSc2	změna
<g/>
"	"	kIx"	"
po	po	k7c4	po
mnoho	mnoho	k4c4	mnoho
lidských	lidský	k2eAgFnPc2d1	lidská
generací	generace	k1gFnPc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Starověcí	starověký	k2eAgMnPc1d1	starověký
astronomové	astronom	k1gMnPc1	astronom
sdružili	sdružit	k5eAaPmAgMnP	sdružit
takové	takový	k3xDgFnPc4	takový
hvězdy	hvězda	k1gFnPc4	hvězda
do	do	k7c2	do
obrazců	obrazec	k1gInPc2	obrazec
tvořících	tvořící	k2eAgInPc2d1	tvořící
základy	základ	k1gInPc4	základ
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
a	a	k8xC	a
asterismů	asterismus	k1gInPc2	asterismus
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
také	také	k9	také
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
nejjasnější	jasný	k2eAgFnPc4d3	nejjasnější
hvězdy	hvězda	k1gFnPc4	hvězda
a	a	k8xC	a
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
katalogy	katalog	k1gInPc4	katalog
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c7	mezi
nejpočetnější	početní	k2eAgFnSc7d3	nejpočetnější
a	a	k8xC	a
nejsnáze	snadno	k6eAd3	snadno
<g/>
,	,	kIx,	,
i	i	k9	i
bez	bez	k7c2	bez
optických	optický	k2eAgInPc2d1	optický
přístrojů	přístroj	k1gInPc2	přístroj
pozorovatelné	pozorovatelný	k2eAgInPc4d1	pozorovatelný
vesmírné	vesmírný	k2eAgInPc4d1	vesmírný
objekty	objekt	k1gInPc4	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
ostatních	ostatní	k2eAgNnPc2d1	ostatní
těles	těleso	k1gNnPc2	těleso
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
vidíme	vidět	k5eAaImIp1nP	vidět
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
odrážejí	odrážet	k5eAaImIp3nP	odrážet
světlo	světlo	k1gNnSc4	světlo
hvězd	hvězda	k1gFnPc2	hvězda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
planety	planeta	k1gFnSc2	planeta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
buzeny	budit	k5eAaImNgFnP	budit
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
záření	záření	k1gNnSc3	záření
zářením	záření	k1gNnSc7	záření
hvězd	hvězda	k1gFnPc2	hvězda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
emisní	emisní	k2eAgFnPc4d1	emisní
mlhoviny	mlhovina	k1gFnPc4	mlhovina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
jsou	být	k5eAaImIp3nP	být
nejspíše	nejspíše	k9	nejspíše
v	v	k7c6	v
naprosté	naprostý	k2eAgFnSc6d1	naprostá
většině	většina	k1gFnSc6	většina
centry	centr	k1gInPc1	centr
planetárních	planetární	k2eAgFnPc2d1	planetární
soustav	soustava	k1gFnPc2	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Odvětví	odvětvit	k5eAaPmIp3nS	odvětvit
astronomie	astronomie	k1gFnSc1	astronomie
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
hvězdami	hvězda	k1gFnPc7	hvězda
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
stelární	stelární	k2eAgFnSc1d1	stelární
astronomie	astronomie	k1gFnSc1	astronomie
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
vlastnosti	vlastnost	k1gFnPc1	vlastnost
hvězd	hvězda	k1gFnPc2	hvězda
dají	dát	k5eAaPmIp3nP	dát
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
SI	si	k1gNnSc2	si
nebo	nebo	k8xC	nebo
CGS	CGS	kA	CGS
<g/>
,	,	kIx,	,
mnohem	mnohem	k6eAd1	mnohem
častěji	často	k6eAd2	často
se	se	k3xPyFc4	se
při	při	k7c6	při
udávání	udávání	k1gNnSc6	udávání
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
svítivosti	svítivost	k1gFnSc2	svítivost
a	a	k8xC	a
poloměru	poloměr	k1gInSc2	poloměr
používají	používat	k5eAaImIp3nP	používat
solární	solární	k2eAgFnPc1d1	solární
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
založené	založený	k2eAgFnPc1d1	založená
na	na	k7c6	na
vlastnostech	vlastnost	k1gFnPc6	vlastnost
k	k	k7c3	k
Zemi	zem	k1gFnSc3	zem
nejbližší	blízký	k2eAgFnSc2d3	nejbližší
hvězdy	hvězda	k1gFnSc2	hvězda
-	-	kIx~	-
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
vzdáleno	vzdálit	k5eAaPmNgNnS	vzdálit
přibližně	přibližně	k6eAd1	přibližně
150	[number]	k4	150
milionů	milion	k4xCgInPc2	milion
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
jednotky	jednotka	k1gFnPc1	jednotka
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
astronomickým	astronomický	k2eAgInSc7d1	astronomický
symbolem	symbol	k1gInSc7	symbol
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
M	M	kA	M
:	:	kIx,	:
⊙	⊙	k?	⊙
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
M_	M_	k1gMnPc6	M_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
odot	odot	k5eAaPmF	odot
}}	}}	k?	}}
(	(	kIx(	(
<g/>
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
velkým	velký	k2eAgNnSc7d1	velké
S	s	k7c7	s
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dolního	dolní	k2eAgInSc2d1	dolní
indexu	index	k1gInSc2	index
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
parametrem	parametr	k1gInSc7	parametr
je	být	k5eAaImIp3nS	být
hmotnost	hmotnost	k1gFnSc1	hmotnost
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
M	M	kA	M
<g/>
,	,	kIx,	,
rozměr	rozměr	k1gInSc1	rozměr
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnostní	hmotnostní	k2eAgNnSc1d1	hmotnostní
rozmezí	rozmezí	k1gNnSc1	rozmezí
hvězd	hvězda	k1gFnPc2	hvězda
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
0,08	[number]	k4	0,08
MS	MS	kA	MS
do	do	k7c2	do
cca	cca	kA	cca
150	[number]	k4	150
MS.	MS.	k1gMnPc2	MS.
Podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
parametru	parametr	k1gInSc2	parametr
lze	lze	k6eAd1	lze
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
zjistit	zjistit	k5eAaPmF	zjistit
délku	délka	k1gFnSc4	délka
života	život	k1gInSc2	život
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
Seznam	seznam	k1gInSc4	seznam
nejtěžších	těžký	k2eAgFnPc2d3	nejtěžší
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
Slunce	slunce	k1gNnSc2	slunce
<g/>
:	:	kIx,	:
M	M	kA	M
<g/>
⊙	⊙	k?	⊙
=	=	kIx~	=
1.989	[number]	k4	1.989
<g/>
1	[number]	k4	1
<g/>
×	×	k?	×
<g/>
1030	[number]	k4	1030
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
velikost	velikost	k1gFnSc1	velikost
<g/>
,	,	kIx,	,
též	též	k9	též
relativní	relativní	k2eAgFnSc1d1	relativní
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
velikost	velikost	k1gFnSc1	velikost
či	či	k8xC	či
magnituda	magnituda	k1gFnSc1	magnituda
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
mag	mag	k?	mag
či	či	k8xC	či
m	m	kA	m
<g/>
)	)	kIx)	)
-	-	kIx~	-
Nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
velikost	velikost	k1gFnSc4	velikost
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
spíše	spíše	k9	spíše
jakási	jakýsi	k3yIgFnSc1	jakýsi
jasnost	jasnost	k1gFnSc1	jasnost
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
Seznam	seznam	k1gInSc4	seznam
nejjasnějších	jasný	k2eAgFnPc2d3	nejjasnější
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Absolutní	absolutní	k2eAgFnSc1d1	absolutní
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
velikost	velikost	k1gFnSc1	velikost
<g/>
,	,	kIx,	,
též	též	k9	též
absolutní	absolutní	k2eAgFnSc1d1	absolutní
magnituda	magnituda	k1gFnSc1	magnituda
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
M	M	kA	M
<g/>
)	)	kIx)	)
-	-	kIx~	-
Není	být	k5eNaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c4	na
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
relativní	relativní	k2eAgFnSc2d1	relativní
magnitudy	magnituda	k1gFnSc2	magnituda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
magnituda	magnituda	k1gFnSc1	magnituda
<g/>
,	,	kIx,	,
pozorovatelná	pozorovatelný	k2eAgFnSc1d1	pozorovatelná
10	[number]	k4	10
pc	pc	k?	pc
od	od	k7c2	od
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Zářivý	zářivý	k2eAgInSc4d1	zářivý
výkon	výkon	k1gInSc4	výkon
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
nesprávně	správně	k6eNd1	správně
"	"	kIx"	"
<g/>
svítivost	svítivost	k1gFnSc1	svítivost
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
L	L	kA	L
<g/>
,	,	kIx,	,
rozměr	rozměr	k1gInSc1	rozměr
W	W	kA	W
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
LS	LS	kA	LS
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
⊙	⊙	k?	⊙
=	=	kIx~	=
3.827	[number]	k4	3.827
<g/>
×	×	k?	×
<g/>
1026	[number]	k4	1026
wattů	watt	k1gInPc2	watt
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
viz	vidět	k5eAaImRp2nS	vidět
Seznam	seznam	k1gInSc1	seznam
nejzářivějších	zářivý	k2eAgFnPc2d3	nejzářivější
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Povrchová	povrchový	k2eAgFnSc1d1	povrchová
teplota	teplota	k1gFnSc1	teplota
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
T	T	kA	T
<g/>
,	,	kIx,	,
jednotka	jednotka	k1gFnSc1	jednotka
K	K	kA	K
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
souvisí	souviset	k5eAaImIp3nS	souviset
dominantní	dominantní	k2eAgFnSc1d1	dominantní
barva	barva	k1gFnSc1	barva
vyzařovaného	vyzařovaný	k2eAgNnSc2d1	vyzařované
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
Spektrální	spektrální	k2eAgFnSc4d1	spektrální
klasifikace	klasifikace	k1gFnSc1	klasifikace
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
většinou	většina	k1gFnSc7	většina
r	r	kA	r
<g/>
,	,	kIx,	,
jednotka	jednotka	k1gFnSc1	jednotka
ly	ly	k?	ly
nebo	nebo	k8xC	nebo
pc	pc	k?	pc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
místo	místo	k7c2	místo
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
paralaxa	paralaxa	k1gFnSc1	paralaxa
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
π	π	k?	π
<g/>
,	,	kIx,	,
jednotka	jednotka	k1gFnSc1	jednotka
0,001	[number]	k4	0,001
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poloměr	poloměr	k1gInSc1	poloměr
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
r	r	kA	r
<g/>
,	,	kIx,	,
jednotka	jednotka	k1gFnSc1	jednotka
kilometr	kilometr	k1gInSc1	kilometr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poloměr	poloměr	k1gInSc1	poloměr
Slunce	slunce	k1gNnSc2	slunce
<g/>
:	:	kIx,	:
R	R	kA	R
<g/>
⊙	⊙	k?	⊙
=	=	kIx~	=
6.960	[number]	k4	6.960
<g/>
×	×	k?	×
<g/>
105	[number]	k4	105
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
ostatních	ostatní	k2eAgFnPc2d1	ostatní
hvězd	hvězda	k1gFnPc2	hvězda
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
jsou	být	k5eAaImIp3nP	být
obrovské	obrovský	k2eAgFnPc1d1	obrovská
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
praktické	praktický	k2eAgNnSc1d1	praktické
vyjadřovat	vyjadřovat	k5eAaImF	vyjadřovat
je	on	k3xPp3gMnPc4	on
v	v	k7c6	v
kilometrech	kilometr	k1gInPc6	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
vzdálenosti	vzdálenost	k1gFnPc1	vzdálenost
uvádějí	uvádět	k5eAaImIp3nP	uvádět
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
jednotek	jednotka	k1gFnPc2	jednotka
času	čas	k1gInSc2	čas
<g/>
,	,	kIx,	,
za	za	k7c4	za
který	který	k3yIgInSc4	který
přiletí	přiletět	k5eAaPmIp3nS	přiletět
světlo	světlo	k1gNnSc1	světlo
z	z	k7c2	z
hvězd	hvězda	k1gFnPc2	hvězda
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
je	být	k5eAaImIp3nS	být
Slunce	slunce	k1gNnSc1	slunce
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
vzdáleno	vzdálit	k5eAaPmNgNnS	vzdálit
asi	asi	k9	asi
8,5	[number]	k4	8,5
světelných	světelný	k2eAgFnPc2d1	světelná
minut	minuta	k1gFnPc2	minuta
(	(	kIx(	(
<g/>
přesněji	přesně	k6eAd2	přesně
499	[number]	k4	499
sekund	sekunda	k1gFnPc2	sekunda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
nejbližší	blízký	k2eAgFnSc7d3	nejbližší
hvězdou	hvězda	k1gFnSc7	hvězda
je	být	k5eAaImIp3nS	být
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
,	,	kIx,	,
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
4,3	[number]	k4	4,3
světelných	světelný	k2eAgNnPc2d1	světelné
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
používanou	používaný	k2eAgFnSc7d1	používaná
jednotkou	jednotka	k1gFnSc7	jednotka
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
hvězd	hvězda	k1gFnPc2	hvězda
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
parsek	parsek	k1gInSc1	parsek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
asi	asi	k9	asi
3,26	[number]	k4	3,26
světelného	světelný	k2eAgInSc2d1	světelný
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
hvězd	hvězda	k1gFnPc2	hvězda
od	od	k7c2	od
sebe	se	k3xPyFc2	se
v	v	k7c6	v
pozorovatelném	pozorovatelný	k2eAgInSc6d1	pozorovatelný
vesmíru	vesmír	k1gInSc6	vesmír
je	být	k5eAaImIp3nS	být
různá	různý	k2eAgFnSc1d1	různá
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
od	od	k7c2	od
několika	několik	k4yIc2	několik
světelných	světelný	k2eAgFnPc2d1	světelná
hodin	hodina	k1gFnPc2	hodina
až	až	k9	až
po	po	k7c4	po
miliardy	miliarda	k4xCgFnPc4	miliarda
světelných	světelný	k2eAgFnPc2d1	světelná
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
mají	mít	k5eAaImIp3nP	mít
různé	různý	k2eAgFnPc4d1	různá
fyzikální	fyzikální	k2eAgFnPc4d1	fyzikální
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
hranicích	hranice	k1gFnPc6	hranice
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Energii	energie	k1gFnSc4	energie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
hvězdy	hvězda	k1gFnPc1	hvězda
produkují	produkovat	k5eAaImIp3nP	produkovat
jako	jako	k9	jako
následek	následek	k1gInSc4	následek
jaderné	jaderný	k2eAgFnSc2d1	jaderná
fúze	fúze	k1gFnSc2	fúze
<g/>
,	,	kIx,	,
vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
do	do	k7c2	do
vesmíru	vesmír	k1gInSc2	vesmír
buď	buď	k8xC	buď
jako	jako	k9	jako
elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
nebo	nebo	k8xC	nebo
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
vyzářené	vyzářený	k2eAgFnPc1d1	vyzářená
částice	částice	k1gFnPc1	částice
tvoří	tvořit	k5eAaImIp3nP	tvořit
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
vítr	vítr	k1gInSc4	vítr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
proudí	proudit	k5eAaImIp3nS	proudit
z	z	k7c2	z
vnějších	vnější	k2eAgFnPc2d1	vnější
vrstev	vrstva	k1gFnPc2	vrstva
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
volných	volný	k2eAgInPc2d1	volný
protonů	proton	k1gInPc2	proton
a	a	k8xC	a
elektricky	elektricky	k6eAd1	elektricky
nabitých	nabitý	k2eAgNnPc2d1	nabité
alfa	alfa	k1gNnPc2	alfa
a	a	k8xC	a
beta	beta	k1gNnSc1	beta
částic	částice	k1gFnPc2	částice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
hvězdy	hvězda	k1gFnSc2	hvězda
vzniká	vznikat	k5eAaImIp3nS	vznikat
i	i	k9	i
stálý	stálý	k2eAgInSc1d1	stálý
proud	proud	k1gInSc1	proud
neutrin	neutrino	k1gNnPc2	neutrino
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
hvězdy	hvězda	k1gFnSc2	hvězda
je	být	k5eAaImIp3nS	být
dána	dán	k2eAgFnSc1d1	dána
tou	ten	k3xDgFnSc7	ten
frekvencí	frekvence	k1gFnSc7	frekvence
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
hvězda	hvězda	k1gFnSc1	hvězda
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
nejintenzivněji	intenzivně	k6eAd3	intenzivně
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
frekvence	frekvence	k1gFnSc1	frekvence
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
teplotě	teplota	k1gFnSc6	teplota
vnějších	vnější	k2eAgFnPc2d1	vnější
vrstev	vrstva	k1gFnPc2	vrstva
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
viditelného	viditelný	k2eAgNnSc2d1	viditelné
světla	světlo	k1gNnSc2	světlo
vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
hvězdy	hvězda	k1gFnPc1	hvězda
i	i	k8xC	i
jiné	jiný	k2eAgFnPc1d1	jiná
formy	forma	k1gFnPc1	forma
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
lidské	lidský	k2eAgNnSc4d1	lidské
oko	oko	k1gNnSc4	oko
neviditelné	viditelný	k2eNgNnSc4d1	neviditelné
<g/>
.	.	kIx.	.
</s>
<s>
Elektromagnetické	elektromagnetický	k2eAgNnSc1d1	elektromagnetické
záření	záření	k1gNnSc1	záření
hvězd	hvězda	k1gFnPc2	hvězda
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
celé	celý	k2eAgNnSc4d1	celé
elektromagnetické	elektromagnetický	k2eAgNnSc4d1	elektromagnetické
spektrum	spektrum	k1gNnSc4	spektrum
<g/>
,	,	kIx,	,
od	od	k7c2	od
nejdelších	dlouhý	k2eAgFnPc2d3	nejdelší
vlnových	vlnový	k2eAgFnPc2d1	vlnová
délek	délka	k1gFnPc2	délka
rádiových	rádiový	k2eAgFnPc2d1	rádiová
vln	vlna	k1gFnPc2	vlna
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
infračervené	infračervený	k2eAgNnSc4d1	infračervené
záření	záření	k1gNnSc4	záření
<g/>
,	,	kIx,	,
viditelné	viditelný	k2eAgNnSc1d1	viditelné
světlo	světlo	k1gNnSc1	světlo
<g/>
,	,	kIx,	,
ultrafialové	ultrafialový	k2eAgNnSc1d1	ultrafialové
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
po	po	k7c6	po
nejkratší	krátký	k2eAgFnSc6d3	nejkratší
rentgenové	rentgenový	k2eAgFnSc6d1	rentgenová
a	a	k8xC	a
gama	gama	k1gNnSc6	gama
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
frekvence	frekvence	k1gFnPc1	frekvence
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
poznávat	poznávat	k5eAaImF	poznávat
fyziku	fyzika	k1gFnSc4	fyzika
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Astronomové	astronom	k1gMnPc1	astronom
dokáží	dokázat	k5eAaPmIp3nP	dokázat
pomocí	pomocí	k7c2	pomocí
spektra	spektrum	k1gNnSc2	spektrum
hvězdy	hvězda	k1gFnSc2	hvězda
určit	určit	k5eAaPmF	určit
její	její	k3xOp3gFnSc4	její
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
teplotu	teplota	k1gFnSc4	teplota
<g/>
,	,	kIx,	,
metalicitu	metalicita	k1gFnSc4	metalicita
a	a	k8xC	a
rychlost	rychlost	k1gFnSc4	rychlost
rotace	rotace	k1gFnSc2	rotace
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
určit	určit	k5eAaPmF	určit
i	i	k9	i
svítivost	svítivost	k1gFnSc4	svítivost
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
hvězdných	hvězdný	k2eAgInPc2d1	hvězdný
modelů	model	k1gInPc2	model
lze	lze	k6eAd1	lze
odhadnout	odhadnout	k5eAaPmF	odhadnout
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
,	,	kIx,	,
poloměr	poloměr	k1gInSc4	poloměr
<g/>
,	,	kIx,	,
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
gravitaci	gravitace	k1gFnSc4	gravitace
a	a	k8xC	a
dobu	doba	k1gFnSc4	doba
rotace	rotace	k1gFnSc2	rotace
<g/>
.	.	kIx.	.
</s>
<s>
Zakřivení	zakřivení	k1gNnSc1	zakřivení
okolí	okolí	k1gNnSc2	okolí
hvězdy	hvězda	k1gFnSc2	hvězda
její	její	k3xOp3gFnSc7	její
gravitací	gravitace	k1gFnSc7	gravitace
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
k	k	k7c3	k
určení	určení	k1gNnSc3	určení
hmotnosti	hmotnost	k1gFnSc2	hmotnost
samostatných	samostatný	k2eAgFnPc2d1	samostatná
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgInPc2	tento
parametrů	parametr	k1gInPc2	parametr
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
odhadnout	odhadnout	k5eAaPmF	odhadnout
i	i	k9	i
věk	věk	k1gInSc4	věk
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Svítivost	svítivost	k1gFnSc1	svítivost
hvězdy	hvězda	k1gFnSc2	hvězda
představuje	představovat	k5eAaImIp3nS	představovat
množství	množství	k1gNnSc1	množství
vyzářené	vyzářený	k2eAgFnSc2d1	vyzářená
energie	energie	k1gFnSc2	energie
za	za	k7c4	za
jednotku	jednotka	k1gFnSc4	jednotka
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
povrchové	povrchový	k2eAgFnSc6d1	povrchová
teplotě	teplota	k1gFnSc6	teplota
a	a	k8xC	a
poloměru	poloměr	k1gInSc6	poloměr
<g/>
.	.	kIx.	.
</s>
<s>
Udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
v	v	k7c6	v
jednotkách	jednotka	k1gFnPc6	jednotka
výkonu	výkon	k1gInSc2	výkon
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
většinou	většina	k1gFnSc7	většina
nevyzařují	vyzařovat	k5eNaImIp3nP	vyzařovat
energii	energie	k1gFnSc4	energie
rovnoměrně	rovnoměrně	k6eAd1	rovnoměrně
celým	celý	k2eAgInSc7d1	celý
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
rychle	rychle	k6eAd1	rychle
rotující	rotující	k2eAgFnSc1d1	rotující
hvězda	hvězda	k1gFnSc1	hvězda
Vega	Veg	k1gInSc2	Veg
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgInSc1d2	veliký
energetický	energetický	k2eAgInSc1d1	energetický
tok	tok	k1gInSc1	tok
na	na	k7c6	na
pólech	pól	k1gInPc6	pól
než	než	k8xS	než
podél	podél	k7c2	podél
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Oblasti	oblast	k1gFnPc1	oblast
povrchu	povrch	k1gInSc2	povrch
s	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
a	a	k8xC	a
svítivostí	svítivost	k1gFnSc7	svítivost
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
průměrné	průměrný	k2eAgFnPc1d1	průměrná
hodnoty	hodnota	k1gFnPc1	hodnota
hvězdy	hvězda	k1gFnSc2	hvězda
jsou	být	k5eAaImIp3nP	být
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xS	jako
hvězdné	hvězdný	k2eAgFnPc1d1	hvězdná
skvrny	skvrna	k1gFnPc1	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
mají	mít	k5eAaImIp3nP	mít
malé	malý	k2eAgFnPc1d1	malá
hvězdy	hvězda	k1gFnPc1	hvězda
jako	jako	k8xC	jako
Slunce	slunce	k1gNnSc1	slunce
na	na	k7c6	na
disku	disk	k1gInSc6	disk
jen	jen	k6eAd1	jen
nevýrazné	výrazný	k2eNgFnSc2d1	nevýrazná
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
skvrny	skvrna	k1gFnSc2	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnPc1d2	veliký
<g/>
,	,	kIx,	,
obří	obří	k2eAgFnPc1d1	obří
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
výraznější	výrazný	k2eAgFnPc4d2	výraznější
skvrny	skvrna	k1gFnPc4	skvrna
a	a	k8xC	a
také	také	k9	také
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
nich	on	k3xPp3gFnPc6	on
pozorovatelné	pozorovatelný	k2eAgNnSc1d1	pozorovatelné
výrazné	výrazný	k2eAgNnSc1d1	výrazné
okrajové	okrajový	k2eAgNnSc1d1	okrajové
ztemnění	ztemnění	k1gNnSc1	ztemnění
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
jejich	jejich	k3xOp3gInSc4	jejich
jas	jas	k1gInSc4	jas
klesá	klesat	k5eAaImIp3nS	klesat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
okrajům	okraj	k1gInPc3	okraj
hvězdného	hvězdný	k2eAgInSc2d1	hvězdný
disku	disk	k1gInSc2	disk
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
objevu	objev	k1gInSc2	objev
spektroskopie	spektroskopie	k1gFnSc2	spektroskopie
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
nevědělo	vědět	k5eNaImAgNnS	vědět
<g/>
,	,	kIx,	,
z	z	k7c2	z
čeho	co	k3yQnSc2	co
se	se	k3xPyFc4	se
hvězdy	hvězda	k1gFnPc1	hvězda
skládají	skládat	k5eAaImIp3nP	skládat
<g/>
.	.	kIx.	.
</s>
<s>
Gustavu	Gustav	k1gMnSc3	Gustav
Robertu	Robert	k1gMnSc3	Robert
Kirchhoffovi	Kirchhoff	k1gMnSc3	Kirchhoff
se	se	k3xPyFc4	se
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
podařilo	podařit	k5eAaPmAgNnS	podařit
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jistá	jistý	k2eAgFnSc1d1	jistá
tmavá	tmavý	k2eAgFnSc1d1	tmavá
čára	čára	k1gFnSc1	čára
ve	v	k7c6	v
slunečním	sluneční	k2eAgNnSc6d1	sluneční
spektru	spektrum	k1gNnSc6	spektrum
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
rozžhaveným	rozžhavený	k2eAgInSc7d1	rozžhavený
sodíkem	sodík	k1gInSc7	sodík
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgFnSc1	první
indicie	indicie	k1gFnSc1	indicie
objevu	objev	k1gInSc2	objev
<g/>
,	,	kIx,	,
že	že	k8xS	že
hvězdy	hvězda	k1gFnPc1	hvězda
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
ze	z	k7c2	z
stejných	stejný	k2eAgInPc2d1	stejný
chemických	chemický	k2eAgInPc2d1	chemický
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
tělesa	těleso	k1gNnPc1	těleso
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
však	však	k9	však
zároveň	zároveň	k6eAd1	zároveň
vše	všechen	k3xTgNnSc1	všechen
napovídalo	napovídat	k5eAaBmAgNnS	napovídat
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hvězdy	hvězda	k1gFnPc1	hvězda
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
horké	horký	k2eAgInPc1d1	horký
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
prvky	prvek	k1gInPc1	prvek
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
většinou	většinou	k6eAd1	většinou
volně	volně	k6eAd1	volně
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
nejsou	být	k5eNaImIp3nP	být
vázány	vázat	k5eAaImNgFnP	vázat
v	v	k7c6	v
četných	četný	k2eAgFnPc6d1	četná
chemických	chemický	k2eAgFnPc6d1	chemická
sloučeninách	sloučenina	k1gFnPc6	sloučenina
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc4	ten
známe	znát	k5eAaImIp1nP	znát
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
nejchladnější	chladný	k2eAgFnPc1d3	nejchladnější
hvězdy	hvězda	k1gFnPc1	hvězda
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
povrchu	povrch	k1gInSc6	povrch
některé	některý	k3yIgFnSc2	některý
jednoduché	jednoduchý	k2eAgFnSc2d1	jednoduchá
chemické	chemický	k2eAgFnSc2d1	chemická
sloučeniny	sloučenina	k1gFnSc2	sloučenina
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
TiO	TiO	k1gFnSc1	TiO
<g/>
,	,	kIx,	,
CH	Ch	kA	Ch
a	a	k8xC	a
CN	CN	kA	CN
(	(	kIx(	(
<g/>
na	na	k7c6	na
Slunci	slunce	k1gNnSc6	slunce
např.	např.	kA	např.
OH	OH	kA	OH
<g/>
,	,	kIx,	,
MgH	MgH	k1gMnSc1	MgH
<g/>
,	,	kIx,	,
SiH	SiH	k1gMnSc1	SiH
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vysoké	vysoký	k2eAgFnSc2d1	vysoká
teploty	teplota	k1gFnSc2	teplota
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
atomů	atom	k1gInPc2	atom
také	také	k9	také
ionizovaných	ionizovaný	k2eAgInPc2d1	ionizovaný
<g/>
.	.	kIx.	.
</s>
<s>
Směs	směs	k1gFnSc1	směs
volných	volný	k2eAgInPc2d1	volný
elektricky	elektricky	k6eAd1	elektricky
nabitých	nabitý	k2eAgFnPc2d1	nabitá
částic	částice	k1gFnPc2	částice
(	(	kIx(	(
<g/>
iontů	ion	k1gInPc2	ion
<g/>
)	)	kIx)	)
a	a	k8xC	a
neutrálních	neutrální	k2eAgFnPc2d1	neutrální
částic	částice	k1gFnPc2	částice
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
plazma	plazma	k1gFnSc1	plazma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jádrech	jádro	k1gNnPc6	jádro
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
a	a	k8xC	a
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
minimálně	minimálně	k6eAd1	minimálně
7	[number]	k4	7
milionů	milion	k4xCgInPc2	milion
stupňů	stupeň	k1gInPc2	stupeň
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
existence	existence	k1gFnSc1	existence
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
chemické	chemický	k2eAgFnSc2d1	chemická
sloučeniny	sloučenina	k1gFnSc2	sloučenina
nemožná	možný	k2eNgFnSc1d1	nemožná
<g/>
.	.	kIx.	.
</s>
<s>
Hmota	hmota	k1gFnSc1	hmota
hvězd	hvězda	k1gFnPc2	hvězda
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jádra	jádro	k1gNnSc2	jádro
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
atomových	atomový	k2eAgNnPc2d1	atomové
jader	jádro	k1gNnPc2	jádro
a	a	k8xC	a
volných	volný	k2eAgInPc2d1	volný
leptonů	lepton	k1gInPc2	lepton
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
závěrečná	závěrečný	k2eAgNnPc1d1	závěrečné
stadia	stadion	k1gNnPc1	stadion
hvězd	hvězda	k1gFnPc2	hvězda
nejsou	být	k5eNaImIp3nP	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
plazmatu	plazma	k1gNnSc2	plazma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
degenerovaného	degenerovaný	k2eAgInSc2d1	degenerovaný
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
prvky	prvek	k1gInPc1	prvek
se	se	k3xPyFc4	se
ve	v	k7c6	v
spektru	spektrum	k1gNnSc6	spektrum
hvězdy	hvězda	k1gFnSc2	hvězda
projevují	projevovat	k5eAaImIp3nP	projevovat
jako	jako	k9	jako
čáry	čára	k1gFnPc1	čára
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jejich	jejich	k3xOp3gNnSc2	jejich
měření	měření	k1gNnSc2	měření
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
plazmových	plazmový	k2eAgFnPc6d1	plazmová
hvězdách	hvězda	k1gFnPc6	hvězda
vznikajících	vznikající	k2eAgFnPc6d1	vznikající
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
Galaxii	galaxie	k1gFnSc6	galaxie
nejzastoupenějším	zastoupený	k2eAgInSc7d3	nejzastoupenější
chemickým	chemický	k2eAgInSc7d1	chemický
prvkem	prvek	k1gInSc7	prvek
vodík	vodík	k1gInSc1	vodík
(	(	kIx(	(
<g/>
71	[number]	k4	71
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
následuje	následovat	k5eAaImIp3nS	následovat
helium	helium	k1gNnSc1	helium
(	(	kIx(	(
<g/>
27	[number]	k4	27
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
prvky	prvek	k1gInPc1	prvek
tvoří	tvořit	k5eAaImIp3nP	tvořit
oproti	oproti	k7c3	oproti
vodíku	vodík	k1gInSc3	vodík
a	a	k8xC	a
héliu	hélium	k1gNnSc6	hélium
jen	jen	k9	jen
nepatrnou	nepatrný	k2eAgFnSc4d1	nepatrná
příměs	příměs	k1gFnSc4	příměs
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
množství	množství	k1gNnSc4	množství
není	být	k5eNaImIp3nS	být
u	u	k7c2	u
všech	všecek	k3xTgFnPc2	všecek
hvězd	hvězda	k1gFnPc2	hvězda
stejné	stejná	k1gFnSc2	stejná
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
těžkých	těžký	k2eAgInPc2d1	těžký
prvků	prvek	k1gInPc2	prvek
se	se	k3xPyFc4	se
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
obsahu	obsah	k1gInSc2	obsah
železa	železo	k1gNnSc2	železo
v	v	k7c6	v
hvězdné	hvězdný	k2eAgFnSc6d1	hvězdná
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
železo	železo	k1gNnSc1	železo
je	být	k5eAaImIp3nS	být
běžný	běžný	k2eAgInSc4d1	běžný
prvek	prvek	k1gInSc4	prvek
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
absorpční	absorpční	k2eAgInSc1d1	absorpční
(	(	kIx(	(
<g/>
tmavé	tmavý	k2eAgFnPc1d1	tmavá
<g/>
)	)	kIx)	)
čáry	čára	k1gFnPc1	čára
se	se	k3xPyFc4	se
měří	měřit	k5eAaImIp3nP	měřit
relativně	relativně	k6eAd1	relativně
snadno	snadno	k6eAd1	snadno
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
molekulární	molekulární	k2eAgFnSc1d1	molekulární
mračna	mračna	k1gFnSc1	mračna
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
vznikají	vznikat	k5eAaImIp3nP	vznikat
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
obohacují	obohacovat	k5eAaImIp3nP	obohacovat
o	o	k7c4	o
těžší	těžký	k2eAgInPc4d2	těžší
prvky	prvek	k1gInPc4	prvek
z	z	k7c2	z
výbuchů	výbuch	k1gInPc2	výbuch
supernov	supernova	k1gFnPc2	supernova
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
chemické	chemický	k2eAgNnSc4d1	chemické
složení	složení	k1gNnSc4	složení
použít	použít	k5eAaPmF	použít
i	i	k9	i
na	na	k7c6	na
odvození	odvození	k1gNnSc6	odvození
věku	věk	k1gInSc2	věk
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jakou	jaký	k3yQgFnSc4	jaký
generaci	generace	k1gFnSc4	generace
hvězd	hvězda	k1gFnPc2	hvězda
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
vesmíru	vesmír	k1gInSc2	vesmír
hvězda	hvězda	k1gFnSc1	hvězda
představuje	představovat	k5eAaImIp3nS	představovat
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnPc1d2	starší
hvězdy	hvězda	k1gFnPc1	hvězda
mají	mít	k5eAaImIp3nP	mít
menší	malý	k2eAgNnSc4d2	menší
zastoupení	zastoupení	k1gNnSc4	zastoupení
těžších	těžký	k2eAgInPc2d2	těžší
chemických	chemický	k2eAgInPc2d1	chemický
prvků	prvek	k1gInPc2	prvek
než	než	k8xS	než
mladší	mladý	k2eAgMnSc1d2	mladší
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
těžkých	těžký	k2eAgInPc2d1	těžký
prvků	prvek	k1gInPc2	prvek
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
naznačovat	naznačovat	k5eAaImF	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hvězda	hvězda	k1gFnSc1	hvězda
má	mít	k5eAaImIp3nS	mít
planetární	planetární	k2eAgInSc4d1	planetární
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgNnSc1d1	chemické
složení	složení	k1gNnSc1	složení
hvězd	hvězda	k1gFnPc2	hvězda
se	se	k3xPyFc4	se
časem	časem	k6eAd1	časem
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
termonukleárních	termonukleární	k2eAgFnPc2d1	termonukleární
reakcí	reakce	k1gFnPc2	reakce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mění	měnit	k5eAaImIp3nP	měnit
prvky	prvek	k1gInPc4	prvek
na	na	k7c4	na
jiné	jiný	k2eAgInPc4d1	jiný
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
HE	he	k0	he
1327-2326	[number]	k4	1327-2326
je	být	k5eAaImIp3nS	být
hvězdou	hvězda	k1gFnSc7	hvězda
s	s	k7c7	s
nejnižším	nízký	k2eAgInSc7d3	nejnižší
odměřeným	odměřený	k2eAgInSc7d1	odměřený
obsahem	obsah	k1gInSc7	obsah
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
200	[number]	k4	200
000	[number]	k4	000
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
Slunci	slunce	k1gNnSc6	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
μ	μ	k?	μ
Leonis	Leonis	k1gInSc1	Leonis
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
téměř	téměř	k6eAd1	téměř
dvojnásobek	dvojnásobek	k1gInSc4	dvojnásobek
železa	železo	k1gNnSc2	železo
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
se	s	k7c7	s
Sluncem	slunce	k1gNnSc7	slunce
a	a	k8xC	a
hvězda	hvězda	k1gFnSc1	hvězda
14	[number]	k4	14
Herculis	Herculis	k1gFnPc2	Herculis
s	s	k7c7	s
planetárním	planetární	k2eAgInSc7d1	planetární
systémem	systém	k1gInSc7	systém
ho	on	k3xPp3gNnSc2	on
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
až	až	k9	až
trojnásobek	trojnásobek	k1gInSc4	trojnásobek
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
hvězdy	hvězda	k1gFnPc1	hvězda
se	s	k7c7	s
zvláštním	zvláštní	k2eAgNnSc7d1	zvláštní
chemickým	chemický	k2eAgNnSc7d1	chemické
složením	složení	k1gNnSc7	složení
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
vyšší	vysoký	k2eAgNnSc4d2	vyšší
množství	množství	k1gNnSc4	množství
některých	některý	k3yIgInPc2	některý
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
chromu	chromat	k5eAaImIp1nS	chromat
a	a	k8xC	a
přechodných	přechodný	k2eAgInPc2d1	přechodný
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
charakteristikou	charakteristika	k1gFnSc7	charakteristika
hvězd	hvězda	k1gFnPc2	hvězda
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc4	jejich
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
určuje	určovat	k5eAaImIp3nS	určovat
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc7d1	střední
hodnotou	hodnota	k1gFnSc7	hodnota
hmotnosti	hmotnost	k1gFnSc2	hmotnost
hvězd	hvězda	k1gFnPc2	hvězda
je	být	k5eAaImIp3nS	být
polovina	polovina	k1gFnSc1	polovina
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
mladším	mladý	k2eAgInSc6d2	mladší
vesmíru	vesmír	k1gInSc6	vesmír
vznikaly	vznikat	k5eAaImAgFnP	vznikat
hmotnější	hmotný	k2eAgFnPc1d2	hmotnější
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
než	než	k8xS	než
pozorujeme	pozorovat	k5eAaImIp1nP	pozorovat
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgFnPc1d1	současná
hmotnosti	hmotnost	k1gFnPc1	hmotnost
pozorovaných	pozorovaný	k2eAgFnPc2d1	pozorovaná
hvězd	hvězda	k1gFnPc2	hvězda
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
tzv.	tzv.	kA	tzv.
Bethe-Salpeterovou	Bethe-Salpeterův	k2eAgFnSc7d1	Bethe-Salpeterův
rovnicí	rovnice	k1gFnSc7	rovnice
pojmenovanou	pojmenovaný	k2eAgFnSc7d1	pojmenovaná
po	po	k7c6	po
astrofyzikovi	astrofyzik	k1gMnSc6	astrofyzik
Edwinu	Edwin	k1gMnSc6	Edwin
Salpeterovi	Salpeter	k1gMnSc6	Salpeter
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
zformuloval	zformulovat	k5eAaPmAgMnS	zformulovat
<g/>
.	.	kIx.	.
</s>
<s>
Rovnice	rovnice	k1gFnSc1	rovnice
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hvězdy	hvězda	k1gFnPc1	hvězda
s	s	k7c7	s
nízkou	nízký	k2eAgFnSc7d1	nízká
hmotností	hmotnost	k1gFnSc7	hmotnost
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
početnější	početní	k2eAgFnPc1d2	početnější
<g/>
,	,	kIx,	,
než	než	k8xS	než
hvězdy	hvězda	k1gFnPc1	hvězda
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
s	s	k7c7	s
nízkou	nízký	k2eAgFnSc7d1	nízká
hmotností	hmotnost	k1gFnSc7	hmotnost
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
za	za	k7c2	za
současných	současný	k2eAgFnPc2d1	současná
podmínek	podmínka	k1gFnPc2	podmínka
v	v	k7c6	v
galaxiích	galaxie	k1gFnPc6	galaxie
snadněji	snadno	k6eAd2	snadno
formují	formovat	k5eAaImIp3nP	formovat
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
hmotnějším	hmotný	k2eAgFnPc3d2	hmotnější
hvězdám	hvězda	k1gFnPc3	hvězda
také	také	k9	také
delší	dlouhý	k2eAgFnPc4d2	delší
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
termojaderné	termojaderný	k2eAgFnPc1d1	termojaderná
reakce	reakce	k1gFnPc1	reakce
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
probíhají	probíhat	k5eAaImIp3nP	probíhat
méně	málo	k6eAd2	málo
intenzivně	intenzivně	k6eAd1	intenzivně
a	a	k8xC	a
jaderné	jaderný	k2eAgNnSc1d1	jaderné
palivo	palivo	k1gNnSc1	palivo
jim	on	k3xPp3gMnPc3	on
tedy	tedy	k9	tedy
déle	dlouho	k6eAd2	dlouho
vydrží	vydržet	k5eAaPmIp3nS	vydržet
<g/>
.	.	kIx.	.
</s>
<s>
Určit	určit	k5eAaPmF	určit
hmotnost	hmotnost	k1gFnSc4	hmotnost
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
ta	ten	k3xDgFnSc1	ten
není	být	k5eNaImIp3nS	být
složkou	složka	k1gFnSc7	složka
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
náročné	náročný	k2eAgNnSc1d1	náročné
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
metod	metoda	k1gFnPc2	metoda
je	být	k5eAaImIp3nS	být
analýza	analýza	k1gFnSc1	analýza
jejího	její	k3xOp3gNnSc2	její
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
další	další	k2eAgNnSc1d1	další
měření	měření	k1gNnSc1	měření
svítivosti	svítivost	k1gFnSc2	svítivost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
přímo	přímo	k6eAd1	přímo
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
hmotnosti	hmotnost	k1gFnSc6	hmotnost
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
dvojhvězdy	dvojhvězda	k1gFnSc2	dvojhvězda
astronomové	astronom	k1gMnPc1	astronom
určí	určit	k5eAaPmIp3nP	určit
její	její	k3xOp3gFnSc4	její
hmotnost	hmotnost	k1gFnSc4	hmotnost
pozorováním	pozorování	k1gNnSc7	pozorování
vzájemného	vzájemný	k2eAgInSc2d1	vzájemný
oběhu	oběh	k1gInSc2	oběh
složek	složka	k1gFnPc2	složka
pomocí	pomocí	k7c2	pomocí
Keplerových	Keplerův	k2eAgInPc2d1	Keplerův
a	a	k8xC	a
Newtonových	Newtonových	k2eAgInPc2d1	Newtonových
zákonů	zákon	k1gInPc2	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
hmoty	hmota	k1gFnSc2	hmota
tvořící	tvořící	k2eAgFnSc2d1	tvořící
hvězdy	hvězda	k1gFnSc2	hvězda
je	být	k5eAaImIp3nS	být
fyzikálními	fyzikální	k2eAgInPc7d1	fyzikální
zákony	zákon	k1gInPc7	zákon
omezené	omezený	k2eAgFnSc2d1	omezená
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nízké	nízký	k2eAgFnSc6d1	nízká
metalicitě	metalicita	k1gFnSc6	metalicita
mají	mít	k5eAaImIp3nP	mít
nejmenší	malý	k2eAgFnPc1d3	nejmenší
hvězdy	hvězda	k1gFnPc1	hvězda
mají	mít	k5eAaImIp3nP	mít
asi	asi	k9	asi
8,3	[number]	k4	8,3
%	%	kIx~	%
hmotnosti	hmotnost	k1gFnSc3	hmotnost
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
87	[number]	k4	87
<g/>
násobek	násobek	k1gInSc4	násobek
hmotnosti	hmotnost	k1gFnSc2	hmotnost
nejhmotnější	hmotný	k2eAgFnSc2d3	nejhmotnější
planety	planeta	k1gFnSc2	planeta
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
-	-	kIx~	-
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Teoretické	teoretický	k2eAgNnSc1d1	teoretické
minimum	minimum	k1gNnSc1	minimum
hmotnosti	hmotnost	k1gFnSc2	hmotnost
hvězdy	hvězda	k1gFnSc2	hvězda
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
metalicitou	metalicita	k1gFnSc7	metalicita
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc7	jaký
má	mít	k5eAaImIp3nS	mít
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
75	[number]	k4	75
<g/>
násobek	násobek	k1gInSc1	násobek
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Jupiteru	Jupiter	k1gInSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
s	s	k7c7	s
menší	malý	k2eAgFnSc7d2	menší
hmotností	hmotnost	k1gFnSc7	hmotnost
než	než	k8xS	než
tento	tento	k3xDgInSc4	tento
limit	limit	k1gInSc4	limit
nemohou	moct	k5eNaImIp3nP	moct
existovat	existovat	k5eAaImF	existovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
v	v	k7c6	v
jejich	jejich	k3xOp3gNnSc6	jejich
jádru	jádro	k1gNnSc6	jádro
by	by	kYmCp3nS	by
byly	být	k5eAaImAgFnP	být
příliš	příliš	k6eAd1	příliš
nízké	nízký	k2eAgFnPc1d1	nízká
na	na	k7c4	na
zapálení	zapálení	k1gNnSc4	zapálení
fúzních	fúzní	k2eAgFnPc2d1	fúzní
reakcí	reakce	k1gFnPc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Tělesa	těleso	k1gNnPc1	těleso
přibližující	přibližující	k2eAgNnPc1d1	přibližující
se	se	k3xPyFc4	se
ke	k	k7c3	k
spodnímu	spodní	k2eAgInSc3d1	spodní
limitu	limit	k1gInSc2	limit
této	tento	k3xDgFnSc2	tento
hmotnosti	hmotnost	k1gFnSc2	hmotnost
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
hnědí	hnědý	k2eAgMnPc1d1	hnědý
trpaslíci	trpaslík	k1gMnPc1	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenší	malý	k2eAgFnSc1d3	nejmenší
známá	známý	k2eAgFnSc1d1	známá
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ještě	ještě	k6eAd1	ještě
spaluje	spalovat	k5eAaImIp3nS	spalovat
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
vodík	vodík	k1gInSc4	vodík
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
AB	AB	kA	AB
Doradus	Doradus	k1gInSc4	Doradus
C	C	kA	C
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
93	[number]	k4	93
<g/>
násobku	násobek	k1gInSc2	násobek
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Jupitera	Jupiter	k1gMnSc2	Jupiter
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
horním	horní	k2eAgInSc6d1	horní
hmotnostním	hmotnostní	k2eAgInSc6d1	hmotnostní
limitu	limit	k1gInSc6	limit
se	se	k3xPyFc4	se
však	však	k9	však
teoretici	teoretik	k1gMnPc1	teoretik
neumějí	umět	k5eNaImIp3nP	umět
sjednotit	sjednotit	k5eAaPmF	sjednotit
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
odhadů	odhad	k1gInPc2	odhad
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
100	[number]	k4	100
<g/>
-	-	kIx~	-
<g/>
120	[number]	k4	120
hmotností	hmotnost	k1gFnSc7	hmotnost
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
větší	veliký	k2eAgFnSc4d2	veliký
hvězdu	hvězda	k1gFnSc4	hvězda
by	by	kYmCp3nS	by
silný	silný	k2eAgInSc1d1	silný
tlak	tlak	k1gInSc1	tlak
záření	záření	k1gNnSc2	záření
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
nitru	nitro	k1gNnSc6	nitro
roztrhal	roztrhat	k5eAaPmAgInS	roztrhat
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
hlavní	hlavní	k2eAgFnPc4d1	hlavní
posloupnosti	posloupnost	k1gFnPc4	posloupnost
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgNnSc3	tento
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
i	i	k9	i
pozorování	pozorování	k1gNnSc4	pozorování
-	-	kIx~	-
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
pozorována	pozorován	k2eAgFnSc1d1	pozorována
"	"	kIx"	"
<g/>
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
"	"	kIx"	"
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
,	,	kIx,	,
podrobnější	podrobný	k2eAgInSc1d2	podrobnější
rozbor	rozbor	k1gInSc1	rozbor
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
minimálně	minimálně	k6eAd1	minimálně
o	o	k7c4	o
dvojhvězdu	dvojhvězda	k1gFnSc4	dvojhvězda
nebo	nebo	k8xC	nebo
hvězdokupu	hvězdokupa	k1gFnSc4	hvězdokupa
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgInPc1d1	jiný
odhady	odhad	k1gInPc1	odhad
horního	horní	k2eAgInSc2d1	horní
limitu	limit	k1gInSc2	limit
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c4	o
130	[number]	k4	130
<g/>
-	-	kIx~	-
<g/>
170	[number]	k4	170
hmotnostech	hmotnost	k1gFnPc6	hmotnost
Slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zkoumání	zkoumání	k1gNnSc2	zkoumání
hvězdokupy	hvězdokupa	k1gFnSc2	hvězdokupa
Arches	Arches	k1gMnSc1	Arches
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
150	[number]	k4	150
<g/>
násobek	násobek	k1gInSc1	násobek
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
představuje	představovat	k5eAaImIp3nS	představovat
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
éře	éra	k1gFnSc6	éra
vesmíru	vesmír	k1gInSc2	vesmír
horní	horní	k2eAgFnSc4d1	horní
hranici	hranice	k1gFnSc4	hranice
hmotnosti	hmotnost	k1gFnSc2	hmotnost
hvězd	hvězda	k1gFnPc2	hvězda
při	při	k7c6	při
jejich	jejich	k3xOp3gInSc6	jejich
vzniku	vznik	k1gInSc6	vznik
z	z	k7c2	z
mlhoviny	mlhovina	k1gFnSc2	mlhovina
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
stelárníci	stelárník	k1gMnPc1	stelárník
však	však	k9	však
nevylučují	vylučovat	k5eNaImIp3nP	vylučovat
ani	ani	k8xC	ani
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
1000	[number]	k4	1000
<g/>
krát	krát	k6eAd1	krát
hmotnější	hmotný	k2eAgFnSc1d2	hmotnější
než	než	k8xS	než
Slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Nejhmotnější	hmotný	k2eAgFnPc1d3	nejhmotnější
hvězdy	hvězda	k1gFnPc1	hvězda
jsou	být	k5eAaImIp3nP	být
nadobry	nadobra	k1gFnPc4	nadobra
spektrálních	spektrální	k2eAgInPc2d1	spektrální
typů	typ	k1gInPc2	typ
O2	O2	k1gFnSc2	O2
a	a	k8xC	a
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
extrémně	extrémně	k6eAd1	extrémně
hmotné	hmotný	k2eAgFnPc4d1	hmotná
hvězdy	hvězda	k1gFnPc4	hvězda
je	být	k5eAaImIp3nS	být
hvězda	hvězda	k1gFnSc1	hvězda
Eta	Eta	k1gFnSc1	Eta
Carinae	Carinae	k1gFnSc1	Carinae
<g/>
.	.	kIx.	.
</s>
<s>
Eta	Eta	k?	Eta
Carinae	Carinae	k1gFnSc1	Carinae
váží	vážit	k5eAaImIp3nS	vážit
100	[number]	k4	100
<g/>
-	-	kIx~	-
<g/>
150	[number]	k4	150
<g/>
krát	krát	k6eAd1	krát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
Slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
délka	délka	k1gFnSc1	délka
jejího	její	k3xOp3gInSc2	její
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
několik	několik	k4yIc4	několik
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	hvězda	k1gFnSc1	hvězda
R	R	kA	R
<g/>
136	[number]	k4	136
<g/>
a	a	k8xC	a
<g/>
1	[number]	k4	1
ve	v	k7c6	v
hvězdokupě	hvězdokupa	k1gFnSc6	hvězdokupa
RMC	RMC	kA	RMC
136	[number]	k4	136
<g/>
a	a	k8xC	a
(	(	kIx(	(
<g/>
modrý	modrý	k2eAgMnSc1d1	modrý
veleobr	veleobr	k1gMnSc1	veleobr
a	a	k8xC	a
nejtěžší	těžký	k2eAgFnSc1d3	nejtěžší
známá	známý	k2eAgFnSc1d1	známá
hvězda	hvězda	k1gFnSc1	hvězda
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
)	)	kIx)	)
však	však	k9	však
váží	vážit	k5eAaImIp3nS	vážit
podle	podle	k7c2	podle
měření	měření	k1gNnPc2	měření
265	[number]	k4	265
<g/>
krát	krát	k6eAd1	krát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
Slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
těžší	těžký	k2eAgFnSc2d2	těžší
než	než	k8xS	než
150	[number]	k4	150
<g/>
násobek	násobek	k1gInSc1	násobek
hmotnosti	hmotnost	k1gFnSc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
vznikají	vznikat	k5eAaImIp3nP	vznikat
podle	podle	k7c2	podle
studie	studie	k1gFnPc4	studie
kolizemi	kolize	k1gFnPc7	kolize
a	a	k8xC	a
splynutím	splynutí	k1gNnSc7	splynutí
těžkých	těžký	k2eAgFnPc2d1	těžká
hvězd	hvězda	k1gFnPc2	hvězda
v	v	k7c6	v
těsném	těsný	k2eAgInSc6d1	těsný
systému	systém	k1gInSc6	systém
dvou	dva	k4xCgFnPc2	dva
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
měla	mít	k5eAaImAgFnS	mít
méně	málo	k6eAd2	málo
než	než	k8xS	než
150	[number]	k4	150
hmotností	hmotnost	k1gFnSc7	hmotnost
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
po	po	k7c6	po
Velkém	velký	k2eAgInSc6d1	velký
třesku	třesk	k1gInSc6	třesk
<g/>
,	,	kIx,	,
však	však	k9	však
mohly	moct	k5eAaImAgFnP	moct
mít	mít	k5eAaImF	mít
podle	podle	k7c2	podle
výpočtů	výpočet	k1gInPc2	výpočet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
300	[number]	k4	300
hmotností	hmotnost	k1gFnSc7	hmotnost
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hustota	hustota	k1gFnSc1	hustota
hmoty	hmota	k1gFnSc2	hmota
ve	v	k7c6	v
hvězdách	hvězda	k1gFnPc6	hvězda
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
000	[number]	k4	000
000	[number]	k4	000
(	(	kIx(	(
<g/>
červení	červený	k2eAgMnPc1d1	červený
nadobři	nadobr	k1gMnPc1	nadobr
<g/>
)	)	kIx)	)
až	až	k6eAd1	až
do	do	k7c2	do
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
gramů	gram	k1gInPc2	gram
(	(	kIx(	(
<g/>
jedné	jeden	k4xCgFnSc2	jeden
tuny	tuna	k1gFnSc2	tuna
<g/>
)	)	kIx)	)
na	na	k7c4	na
cm3	cm3	k4	cm3
(	(	kIx(	(
<g/>
bílý	bílý	k1gMnSc1	bílý
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Objekty	objekt	k1gInPc1	objekt
jako	jako	k8xC	jako
neutronové	neutronový	k2eAgFnPc1d1	neutronová
hvězdy	hvězda	k1gFnPc1	hvězda
a	a	k8xC	a
kvarkové	kvarkový	k2eAgFnPc1d1	kvarková
hvězdy	hvězda	k1gFnPc1	hvězda
jsou	být	k5eAaImIp3nP	být
ještě	ještě	k9	ještě
podstatně	podstatně	k6eAd1	podstatně
hmotnější	hmotný	k2eAgNnSc1d2	hmotnější
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
hustota	hustota	k1gFnSc1	hustota
hmoty	hmota	k1gFnSc2	hmota
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
na	na	k7c4	na
cm3	cm3	k4	cm3
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
hustota	hustota	k1gFnSc1	hustota
plynů	plyn	k1gInPc2	plyn
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
nitra	nitro	k1gNnSc2	nitro
hvězdy	hvězda	k1gFnSc2	hvězda
rychle	rychle	k6eAd1	rychle
narůstá	narůstat	k5eAaImIp3nS	narůstat
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
Slunce	slunce	k1gNnSc2	slunce
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc4	všechen
hvězdy	hvězda	k1gFnPc4	hvězda
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
kvůli	kvůli	k7c3	kvůli
obrovským	obrovský	k2eAgFnPc3d1	obrovská
vzdálenostem	vzdálenost	k1gFnPc3	vzdálenost
viditelné	viditelný	k2eAgFnSc2d1	viditelná
jen	jen	k8xS	jen
jako	jako	k9	jako
mihotavé	mihotavý	k2eAgInPc1d1	mihotavý
světelné	světelný	k2eAgInPc1d1	světelný
body	bod	k1gInPc1	bod
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
také	také	k9	také
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
blízko	blízko	k6eAd1	blízko
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
ji	on	k3xPp3gFnSc4	on
viděli	vidět	k5eAaImAgMnP	vidět
jako	jako	k8xC	jako
disk	disk	k1gInSc4	disk
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdou	hvězda	k1gFnSc7	hvězda
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
zdánlivou	zdánlivý	k2eAgFnSc7d1	zdánlivá
velikostí	velikost	k1gFnSc7	velikost
po	po	k7c6	po
Slunci	slunce	k1gNnSc6	slunce
je	být	k5eAaImIp3nS	být
R	R	kA	R
Doradus	Doradus	k1gInSc1	Doradus
s	s	k7c7	s
úhlovým	úhlový	k2eAgInSc7d1	úhlový
průměrem	průměr	k1gInSc7	průměr
pouhých	pouhý	k2eAgNnPc2d1	pouhé
0,057	[number]	k4	0,057
úhlové	úhlový	k2eAgFnSc2d1	úhlová
vteřiny	vteřina	k1gFnSc2	vteřina
<g/>
.	.	kIx.	.
</s>
<s>
Disky	disk	k1gInPc1	disk
většiny	většina	k1gFnSc2	většina
hvězd	hvězda	k1gFnPc2	hvězda
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgFnPc1d1	malá
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
daly	dát	k5eAaPmAgFnP	dát
přímo	přímo	k6eAd1	přímo
pozorovat	pozorovat	k5eAaImF	pozorovat
dnešními	dnešní	k2eAgInPc7d1	dnešní
pozemskými	pozemský	k2eAgInPc7d1	pozemský
teleskopy	teleskop	k1gInPc7	teleskop
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
obrázků	obrázek	k1gInPc2	obrázek
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
interferometry	interferometr	k1gInPc1	interferometr
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
technikou	technika	k1gFnSc7	technika
měření	měření	k1gNnSc2	měření
úhlové	úhlový	k2eAgFnSc2d1	úhlová
velikosti	velikost	k1gFnSc2	velikost
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
zákryt	zákryt	k1gInSc4	zákryt
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
lze	lze	k6eAd1	lze
úhlovou	úhlový	k2eAgFnSc4d1	úhlová
velikost	velikost	k1gFnSc4	velikost
vypočítat	vypočítat	k5eAaPmF	vypočítat
z	z	k7c2	z
přesných	přesný	k2eAgFnPc2d1	přesná
měření	měření	k1gNnPc2	měření
změny	změna	k1gFnSc2	změna
jasu	jas	k1gInSc2	jas
hvězdy	hvězda	k1gFnSc2	hvězda
při	při	k7c6	při
zákrytu	zákryt	k1gInSc6	zákryt
Měsícem	měsíc	k1gInSc7	měsíc
či	či	k8xC	či
jiným	jiný	k2eAgNnSc7d1	jiné
tělesem	těleso	k1gNnSc7	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Rozsah	rozsah	k1gInSc1	rozsah
velikostí	velikost	k1gFnPc2	velikost
hvězd	hvězda	k1gFnPc2	hvězda
je	být	k5eAaImIp3nS	být
obrovský	obrovský	k2eAgInSc1d1	obrovský
<g/>
.	.	kIx.	.
</s>
<s>
Kolísá	kolísat	k5eAaImIp3nS	kolísat
v	v	k7c6	v
rozhraní	rozhraní	k1gNnSc6	rozhraní
od	od	k7c2	od
velikosti	velikost	k1gFnSc2	velikost
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
45	[number]	k4	45
km	km	kA	km
u	u	k7c2	u
neutronových	neutronový	k2eAgFnPc2d1	neutronová
hvězd	hvězda	k1gFnPc2	hvězda
až	až	k9	až
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
stonásobku	stonásobek	k1gInSc2	stonásobek
průměru	průměr	k1gInSc2	průměr
Slunce	slunce	k1gNnSc2	slunce
u	u	k7c2	u
nadobrů	nadobr	k1gInPc2	nadobr
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Betelgeuze	Betelgeuze	k1gFnSc1	Betelgeuze
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Orionu	orion	k1gInSc2	orion
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
650	[number]	k4	650
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
průměr	průměr	k1gInSc4	průměr
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
asi	asi	k9	asi
900	[number]	k4	900
000	[number]	k4	000
000	[number]	k4	000
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poloměry	poloměr	k1gInPc1	poloměr
hvězd	hvězda	k1gFnPc2	hvězda
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
až	až	k9	až
3000	[number]	k4	3000
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
poloměr	poloměr	k1gInSc4	poloměr
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	s	k7c7	s
vzrůstajícím	vzrůstající	k2eAgInSc7d1	vzrůstající
průměrem	průměr	k1gInSc7	průměr
hvězdy	hvězda	k1gFnSc2	hvězda
klesá	klesat	k5eAaImIp3nS	klesat
její	její	k3xOp3gFnSc1	její
hustota	hustota	k1gFnSc1	hustota
<g/>
.	.	kIx.	.
</s>
<s>
Věk	věk	k1gInSc1	věk
většiny	většina	k1gFnSc2	většina
hvězd	hvězda	k1gFnPc2	hvězda
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
objevenou	objevený	k2eAgFnSc7d1	objevená
hvězdou	hvězda	k1gFnSc7	hvězda
je	být	k5eAaImIp3nS	být
HE	he	k0	he
1523	[number]	k4	1523
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
901	[number]	k4	901
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
stáří	stáří	k1gNnSc1	stáří
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
13,2	[number]	k4	13,2
miliardy	miliarda	k4xCgFnPc4	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Čím	čí	k3xOyRgNnSc7	čí
je	být	k5eAaImIp3nS	být
hvězda	hvězda	k1gFnSc1	hvězda
těžší	těžký	k2eAgFnSc1d2	těžší
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
má	mít	k5eAaImIp3nS	mít
kratší	krátký	k2eAgFnSc4d2	kratší
životnost	životnost	k1gFnSc4	životnost
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
jádrech	jádro	k1gNnPc6	jádro
těžkých	těžký	k2eAgFnPc2d1	těžká
hvězd	hvězda	k1gFnPc2	hvězda
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgInSc4d2	veliký
tlak	tlak	k1gInSc4	tlak
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
rychlejší	rychlý	k2eAgNnSc1d2	rychlejší
spalování	spalování	k1gNnSc1	spalování
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Nejtěžší	těžký	k2eAgFnPc1d3	nejtěžší
hvězdy	hvězda	k1gFnPc1	hvězda
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
jen	jen	k9	jen
pár	pár	k4xCyI	pár
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
nejlehčí	lehký	k2eAgMnSc1d3	nejlehčí
spalují	spalovat	k5eAaImIp3nP	spalovat
své	svůj	k3xOyFgNnSc4	svůj
palivo	palivo	k1gNnSc4	palivo
pomaličku	pomaličku	k6eAd1	pomaličku
a	a	k8xC	a
vydrží	vydržet	k5eAaPmIp3nS	vydržet
jim	on	k3xPp3gMnPc3	on
na	na	k7c6	na
desítky	desítka	k1gFnPc1	desítka
až	až	k8xS	až
stovky	stovka	k1gFnPc1	stovka
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Proměnná	proměnný	k2eAgFnSc1d1	proměnná
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Žádná	žádný	k3yNgFnSc1	žádný
hvězda	hvězda	k1gFnSc1	hvězda
nezáří	zářit	k5eNaImIp3nS	zářit
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
až	až	k9	až
po	po	k7c4	po
zánik	zánik	k1gInSc4	zánik
konstantně	konstantně	k6eAd1	konstantně
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
však	však	k9	však
mění	měnit	k5eAaImIp3nP	měnit
svou	svůj	k3xOyFgFnSc4	svůj
jasnost	jasnost	k1gFnSc4	jasnost
rychle	rychle	k6eAd1	rychle
(	(	kIx(	(
<g/>
řádově	řádově	k6eAd1	řádově
během	během	k7c2	během
hodin	hodina	k1gFnPc2	hodina
až	až	k8xS	až
desetiletí	desetiletí	k1gNnPc2	desetiletí
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
o	o	k7c4	o
výrazné	výrazný	k2eAgFnPc4d1	výrazná
hodnoty	hodnota	k1gFnPc4	hodnota
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
proměnné	proměnná	k1gFnPc1	proměnná
<g/>
.	.	kIx.	.
</s>
<s>
Příčina	příčina	k1gFnSc1	příčina
proměnnosti	proměnnost	k1gFnSc2	proměnnost
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
různých	různý	k2eAgFnPc2d1	různá
hvězd	hvězda	k1gFnPc2	hvězda
různá	různý	k2eAgFnSc1d1	různá
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
buď	buď	k8xC	buď
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gMnPc4	on
zakrývá	zakrývat	k5eAaImIp3nS	zakrývat
temnější	temný	k2eAgInSc1d2	temnější
objekt	objekt	k1gInSc1	objekt
(	(	kIx(	(
<g/>
zákrytové	zákrytový	k2eAgFnPc1d1	zákrytová
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
má	můj	k3xOp1gFnSc1	můj
proměnlivost	proměnlivost	k1gFnSc1	proměnlivost
fyzikální	fyzikální	k2eAgFnSc4d1	fyzikální
příčinu	příčina	k1gFnSc4	příčina
od	od	k7c2	od
samotné	samotný	k2eAgFnSc2d1	samotná
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pulsující	pulsující	k2eAgFnSc2d1	pulsující
hvězdy	hvězda	k1gFnSc2	hvězda
mění	měnit	k5eAaImIp3nS	měnit
svůj	svůj	k3xOyFgInSc4	svůj
průměr	průměr	k1gInSc4	průměr
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
rozpětí	rozpětí	k1gNnSc6	rozpětí
a	a	k8xC	a
časovém	časový	k2eAgInSc6d1	časový
úseku	úsek	k1gInSc6	úsek
<g/>
.	.	kIx.	.
</s>
<s>
Eruptivní	eruptivní	k2eAgFnPc1d1	eruptivní
proměnné	proměnný	k2eAgFnPc1d1	proměnná
hvězdy	hvězda	k1gFnPc1	hvězda
procházejí	procházet	k5eAaImIp3nP	procházet
náhlým	náhlý	k2eAgInSc7d1	náhlý
nárůstem	nárůst	k1gInSc7	nárůst
svítivosti	svítivost	k1gFnSc2	svítivost
následkem	následkem	k7c2	následkem
erupcí	erupce	k1gFnPc2	erupce
a	a	k8xC	a
výronů	výron	k1gInPc2	výron
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
protohvězdy	protohvězda	k1gFnSc2	protohvězda
nebo	nebo	k8xC	nebo
Wolf-Rayetovy	Wolf-Rayetův	k2eAgFnSc2d1	Wolf-Rayetova
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Kataklyzmatické	kataklyzmatický	k2eAgFnPc1d1	kataklyzmatická
(	(	kIx(	(
<g/>
explozivní	explozivní	k2eAgFnPc1d1	explozivní
<g/>
)	)	kIx)	)
proměnné	proměnný	k2eAgFnPc1d1	proměnná
hvězdy	hvězda	k1gFnPc1	hvězda
procházejí	procházet	k5eAaImIp3nP	procházet
dramatickými	dramatický	k2eAgFnPc7d1	dramatická
změnami	změna	k1gFnPc7	změna
svých	svůj	k3xOyFgFnPc2	svůj
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
novy	nov	k1gInPc4	nov
a	a	k8xC	a
supernovy	supernova	k1gFnPc4	supernova
<g/>
.	.	kIx.	.
</s>
<s>
Expandující	expandující	k2eAgFnPc1d1	expandující
hvězdy	hvězda	k1gFnPc1	hvězda
mění	měnit	k5eAaImIp3nP	měnit
svůj	svůj	k3xOyFgInSc4	svůj
průměr	průměr	k1gInSc4	průměr
náhle	náhle	k6eAd1	náhle
obrovskými	obrovský	k2eAgInPc7d1	obrovský
výbuchy	výbuch	k1gInPc7	výbuch
(	(	kIx(	(
<g/>
supernovy	supernova	k1gFnPc4	supernova
při	při	k7c6	při
výbuších	výbuch	k1gInPc6	výbuch
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
svou	svůj	k3xOyFgFnSc4	svůj
jasnost	jasnost	k1gFnSc4	jasnost
až	až	k9	až
100	[number]	k4	100
<g/>
milionkrát	milionkrát	k6eAd1	milionkrát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
změn	změna	k1gFnPc2	změna
jasností	jasnost	k1gFnPc2	jasnost
však	však	k9	však
nebývá	bývat	k5eNaImIp3nS	bývat
tak	tak	k6eAd1	tak
dramatická	dramatický	k2eAgFnSc1d1	dramatická
<g/>
,	,	kIx,	,
mnohé	mnohý	k2eAgFnPc1d1	mnohá
změny	změna	k1gFnPc1	změna
jsou	být	k5eAaImIp3nP	být
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
nezachytitelné	zachytitelný	k2eNgFnSc2d1	nezachytitelná
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
mají	mít	k5eAaImIp3nP	mít
větší	veliký	k2eAgInPc4d2	veliký
sklony	sklon	k1gInPc4	sklon
k	k	k7c3	k
fyzikálním	fyzikální	k2eAgFnPc3d1	fyzikální
změnám	změna	k1gFnPc3	změna
jasnosti	jasnost	k1gFnSc2	jasnost
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
(	(	kIx(	(
<g/>
hvězdy	hvězda	k1gFnSc2	hvězda
typu	typ	k1gInSc2	typ
T	T	kA	T
Tauri	Taur	k1gFnSc2	Taur
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
konci	konec	k1gInSc6	konec
(	(	kIx(	(
<g/>
Cefeida	Cefeida	k1gFnSc1	Cefeida
<g/>
,	,	kIx,	,
Miridy	Mirida	k1gFnPc1	Mirida
<g/>
,	,	kIx,	,
supernovy	supernova	k1gFnPc1	supernova
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
svého	své	k1gNnSc2	své
vývoje	vývoj	k1gInSc2	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
hvězdy	hvězda	k1gFnPc1	hvězda
zase	zase	k9	zase
mírně	mírně	k6eAd1	mírně
mění	měnit	k5eAaImIp3nS	měnit
svou	svůj	k3xOyFgFnSc4	svůj
jasnost	jasnost	k1gFnSc4	jasnost
kvůli	kvůli	k7c3	kvůli
extrémním	extrémní	k2eAgFnPc3d1	extrémní
skvrnám	skvrna	k1gFnPc3	skvrna
na	na	k7c6	na
svých	svůj	k3xOyFgInPc6	svůj
površích	povrch	k1gInPc6	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Rotace	rotace	k1gFnSc1	rotace
hvězdy	hvězda	k1gFnSc2	hvězda
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
zjistit	zjistit	k5eAaPmF	zjistit
pomocí	pomocí	k7c2	pomocí
spektroskopických	spektroskopický	k2eAgNnPc2d1	spektroskopické
měření	měření	k1gNnPc2	měření
nebo	nebo	k8xC	nebo
přesněji	přesně	k6eAd2	přesně
sledováním	sledování	k1gNnSc7	sledování
rotace	rotace	k1gFnSc2	rotace
hvězdných	hvězdný	k2eAgFnPc2d1	hvězdná
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgFnPc1d1	mladá
hvězdy	hvězda	k1gFnPc1	hvězda
rotují	rotovat	k5eAaImIp3nP	rotovat
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc1	rychlost
rotace	rotace	k1gFnSc1	rotace
na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
vyšší	vysoký	k2eAgFnSc2d2	vyšší
než	než	k8xS	než
100	[number]	k4	100
km	km	kA	km
/	/	kIx~	/
s.	s.	k?	s.
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
případech	případ	k1gInPc6	případ
odstředivá	odstředivý	k2eAgFnSc1d1	odstředivá
síla	síla	k1gFnSc1	síla
na	na	k7c6	na
rovníku	rovník	k1gInSc6	rovník
silně	silně	k6eAd1	silně
vydouvá	vydouvat	k5eAaImIp3nS	vydouvat
hmotu	hmota	k1gFnSc4	hmota
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Rotační	rotační	k2eAgFnSc1d1	rotační
rychlost	rychlost	k1gFnSc1	rychlost
hvězdy	hvězda	k1gFnSc2	hvězda
typu	typ	k1gInSc2	typ
B	B	kA	B
<g/>
,	,	kIx,	,
Achernar	Achernar	k1gInSc1	Achernar
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
225	[number]	k4	225
km	km	kA	km
/	/	kIx~	/
s	s	k7c7	s
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gInSc4	její
rovníkový	rovníkový	k2eAgInSc4d1	rovníkový
poloměr	poloměr	k1gInSc4	poloměr
o	o	k7c4	o
50	[number]	k4	50
%	%	kIx~	%
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
polární	polární	k2eAgInSc4d1	polární
poloměr	poloměr	k1gInSc4	poloměr
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
hodnoty	hodnota	k1gFnPc1	hodnota
rychlosti	rychlost	k1gFnSc2	rychlost
rotace	rotace	k1gFnSc1	rotace
jsou	být	k5eAaImIp3nP	být
těsně	těsně	k6eAd1	těsně
pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
300	[number]	k4	300
km	km	kA	km
/	/	kIx~	/
s	s	k7c7	s
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yRgFnSc4	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
hvězda	hvězda	k1gFnSc1	hvězda
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
se	se	k3xPyFc4	se
otočí	otočit	k5eAaPmIp3nS	otočit
kolem	kolem	k7c2	kolem
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
rychlostí	rychlost	k1gFnSc7	rychlost
1,994	[number]	k4	1,994
km	km	kA	km
/	/	kIx~	/
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
25	[number]	k4	25
<g/>
-	-	kIx~	-
<g/>
35	[number]	k4	35
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Magnetické	magnetický	k2eAgNnSc4d1	magnetické
pole	pole	k1gNnSc4	pole
a	a	k8xC	a
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
vítr	vítr	k1gInSc4	vítr
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
významné	významný	k2eAgNnSc4d1	významné
zpomalení	zpomalení	k1gNnSc4	zpomalení
rotace	rotace	k1gFnSc2	rotace
hvězd	hvězda	k1gFnPc2	hvězda
během	během	k7c2	během
jejich	jejich	k3xOp3gInSc2	jejich
vývoje	vývoj	k1gInSc2	vývoj
na	na	k7c6	na
hlavní	hlavní	k2eAgFnSc6d1	hlavní
posloupnosti	posloupnost	k1gFnSc6	posloupnost
<g/>
.	.	kIx.	.
</s>
<s>
Degenerované	degenerovaný	k2eAgFnPc1d1	degenerovaná
hvězdy	hvězda	k1gFnPc1	hvězda
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
do	do	k7c2	do
stavu	stav	k1gInSc2	stav
kompaktní	kompaktní	k2eAgFnSc2d1	kompaktní
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
vysoké	vysoký	k2eAgFnSc2d1	vysoká
rotační	rotační	k2eAgFnSc2d1	rotační
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
rychlosti	rychlost	k1gFnPc1	rychlost
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
nízké	nízký	k2eAgInPc1d1	nízký
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
rychlostmi	rychlost	k1gFnPc7	rychlost
předpokládanými	předpokládaný	k2eAgFnPc7d1	předpokládaná
podle	podle	k7c2	podle
zachování	zachování	k1gNnSc2	zachování
momentu	moment	k1gInSc2	moment
hybnosti	hybnost	k1gFnSc2	hybnost
-	-	kIx~	-
tendence	tendence	k1gFnSc1	tendence
rotujícího	rotující	k2eAgNnSc2d1	rotující
tělesa	těleso	k1gNnSc2	těleso
vyrovnávat	vyrovnávat	k5eAaImF	vyrovnávat
zmenšení	zmenšení	k1gNnSc4	zmenšení
velikosti	velikost	k1gFnSc2	velikost
zrychlením	zrychlení	k1gNnSc7	zrychlení
rotace	rotace	k1gFnSc1	rotace
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
momentu	moment	k1gInSc2	moment
hybnosti	hybnost	k1gFnSc2	hybnost
hvězdy	hvězda	k1gFnSc2	hvězda
se	se	k3xPyFc4	se
ztratí	ztratit	k5eAaPmIp3nS	ztratit
následkem	následkem	k7c2	následkem
ztráty	ztráta	k1gFnSc2	ztráta
hmotnosti	hmotnost	k1gFnSc2	hmotnost
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
hvězdného	hvězdný	k2eAgInSc2d1	hvězdný
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
pulsary	pulsar	k1gInPc1	pulsar
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
vysoké	vysoký	k2eAgFnSc2d1	vysoká
rychlosti	rychlost	k1gFnSc2	rychlost
rotace	rotace	k1gFnSc2	rotace
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Krabího	krabí	k2eAgInSc2d1	krabí
pulsaru	pulsar	k1gInSc2	pulsar
<g/>
,	,	kIx,	,
30	[number]	k4	30
otáček	otáčka	k1gFnPc2	otáčka
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Povrchová	povrchový	k2eAgFnSc1d1	povrchová
teplota	teplota	k1gFnSc1	teplota
hvězd	hvězda	k1gFnPc2	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
produkce	produkce	k1gFnSc2	produkce
energie	energie	k1gFnSc2	energie
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
dána	dát	k5eAaPmNgFnS	dát
efektivní	efektivní	k2eAgFnSc7d1	efektivní
teplotou	teplota	k1gFnSc7	teplota
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
teplotu	teplota	k1gFnSc4	teplota
ideálního	ideální	k2eAgNnSc2d1	ideální
černého	černý	k2eAgNnSc2d1	černé
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
energii	energie	k1gFnSc4	energie
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
svítivostí	svítivost	k1gFnSc7	svítivost
povrchu	povrch	k1gInSc2	povrch
jako	jako	k8xC	jako
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Efektivní	efektivní	k2eAgFnSc1d1	efektivní
teplota	teplota	k1gFnSc1	teplota
není	být	k5eNaImIp3nS	být
reprezentativní	reprezentativní	k2eAgFnSc1d1	reprezentativní
hodnota	hodnota	k1gFnSc1	hodnota
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
teplota	teplota	k1gFnSc1	teplota
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
jádra	jádro	k1gNnSc2	jádro
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
hvězdy	hvězda	k1gFnSc2	hvězda
je	být	k5eAaImIp3nS	být
několik	několik	k4yIc1	několik
milionů	milion	k4xCgInPc2	milion
kelvinů	kelvin	k1gInPc2	kelvin
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
hvězdy	hvězda	k1gFnSc2	hvězda
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
proces	proces	k1gInSc1	proces
ionizace	ionizace	k1gFnSc2	ionizace
rozličných	rozličný	k2eAgInPc2d1	rozličný
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
výsledkem	výsledek	k1gInSc7	výsledek
toho	ten	k3xDgInSc2	ten
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
absorpční	absorpční	k2eAgInPc1d1	absorpční
čáry	čár	k1gInPc1	čár
ve	v	k7c6	v
spektru	spektrum	k1gNnSc6	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Povrchová	povrchový	k2eAgFnSc1d1	povrchová
teplota	teplota	k1gFnSc1	teplota
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
absolutní	absolutní	k2eAgFnSc1d1	absolutní
magnituda	magnituda	k1gFnSc1	magnituda
a	a	k8xC	a
absorpční	absorpční	k2eAgFnPc1d1	absorpční
vlastnosti	vlastnost	k1gFnPc1	vlastnost
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
klasifikaci	klasifikace	k1gFnSc4	klasifikace
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
hvězdy	hvězda	k1gFnPc1	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
povrchové	povrchový	k2eAgFnPc4d1	povrchová
teploty	teplota	k1gFnPc4	teplota
50	[number]	k4	50
000	[number]	k4	000
K.	K.	kA	K.
Menší	malý	k2eAgFnSc2d2	menší
hvězdy	hvězda	k1gFnSc2	hvězda
jako	jako	k8xC	jako
Slunce	slunce	k1gNnSc2	slunce
mají	mít	k5eAaImIp3nP	mít
povrchové	povrchový	k2eAgFnPc1d1	povrchová
teploty	teplota	k1gFnPc1	teplota
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
K.	K.	kA	K.
Nejnižší	nízký	k2eAgFnSc2d3	nejnižší
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
3	[number]	k4	3
600	[number]	k4	600
K	K	kA	K
<g/>
,	,	kIx,	,
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
červení	červený	k2eAgMnPc1d1	červený
obři	obr	k1gMnPc1	obr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
obrovskému	obrovský	k2eAgInSc3d1	obrovský
povrchu	povrch	k1gInSc3	povrch
mají	mít	k5eAaImIp3nP	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
svítivost	svítivost	k1gFnSc4	svítivost
<g/>
.	.	kIx.	.
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
hvězdy	hvězda	k1gFnSc2	hvězda
vzniká	vznikat	k5eAaImIp3nS	vznikat
uvnitř	uvnitř	k7c2	uvnitř
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
probíhá	probíhat	k5eAaImIp3nS	probíhat
konvekční	konvekční	k2eAgFnSc1d1	konvekční
cirkulace	cirkulace	k1gFnSc1	cirkulace
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pohyb	pohyb	k1gInSc1	pohyb
horkého	horký	k2eAgMnSc2d1	horký
<g/>
,	,	kIx,	,
vodivého	vodivý	k2eAgNnSc2d1	vodivé
plazmatu	plazma	k1gNnSc2	plazma
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
dynamo	dynamo	k1gNnSc1	dynamo
<g/>
,	,	kIx,	,
generuje	generovat	k5eAaImIp3nS	generovat
magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
přesahující	přesahující	k2eAgFnSc4d1	přesahující
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Síla	síla	k1gFnSc1	síla
magnetického	magnetický	k2eAgNnSc2d1	magnetické
pole	pole	k1gNnSc2	pole
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
a	a	k8xC	a
složením	složení	k1gNnSc7	složení
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
magnetické	magnetický	k2eAgFnSc2d1	magnetická
aktivity	aktivita	k1gFnSc2	aktivita
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
rychlosti	rychlost	k1gFnSc6	rychlost
rotace	rotace	k1gFnSc2	rotace
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
povrchová	povrchový	k2eAgFnSc1d1	povrchová
aktivita	aktivita	k1gFnSc1	aktivita
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
hvězdné	hvězdný	k2eAgFnPc4d1	hvězdná
skvrny	skvrna	k1gFnPc4	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdné	hvězdný	k2eAgFnPc1d1	hvězdná
skvrny	skvrna	k1gFnPc1	skvrna
jsou	být	k5eAaImIp3nP	být
oblasti	oblast	k1gFnPc4	oblast
se	s	k7c7	s
silným	silný	k2eAgNnSc7d1	silné
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
polem	pole	k1gNnSc7	pole
a	a	k8xC	a
teplotou	teplota	k1gFnSc7	teplota
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
normální	normální	k2eAgFnSc1d1	normální
povrchová	povrchový	k2eAgFnSc1d1	povrchová
teplota	teplota	k1gFnSc1	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Koronální	koronální	k2eAgFnPc1d1	koronální
smyčky	smyčka	k1gFnPc1	smyčka
jsou	být	k5eAaImIp3nP	být
vypouklá	vypouklý	k2eAgFnSc1d1	vypouklá
magnetická	magnetický	k2eAgFnSc1d1	magnetická
pole	pole	k1gFnSc1	pole
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
aktivních	aktivní	k2eAgFnPc2d1	aktivní
oblastí	oblast	k1gFnPc2	oblast
-	-	kIx~	-
míst	místo	k1gNnPc2	místo
s	s	k7c7	s
výraznými	výrazný	k2eAgInPc7d1	výrazný
jevy	jev	k1gInPc7	jev
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
hvězdy	hvězda	k1gFnSc2	hvězda
-	-	kIx~	-
do	do	k7c2	do
vysoké	vysoký	k2eAgFnSc2d1	vysoká
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
koróny	koróna	k1gFnSc2	koróna
<g/>
.	.	kIx.	.
</s>
<s>
Erupce	erupce	k1gFnPc4	erupce
jsou	být	k5eAaImIp3nP	být
výtrysky	výtrysk	k1gInPc1	výtrysk
vysoce	vysoce	k6eAd1	vysoce
energetických	energetický	k2eAgFnPc2d1	energetická
částic	částice	k1gFnPc2	částice
vyzářených	vyzářený	k2eAgFnPc2d1	vyzářená
toutéž	týž	k3xTgFnSc7	týž
magnetickou	magnetický	k2eAgFnSc7d1	magnetická
aktivitou	aktivita	k1gFnSc7	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladé	k1gNnSc1	mladé
<g/>
,	,	kIx,	,
rychle	rychle	k6eAd1	rychle
rotující	rotující	k2eAgFnPc1d1	rotující
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
obvykle	obvykle	k6eAd1	obvykle
vyšší	vysoký	k2eAgFnSc4d2	vyšší
úroveň	úroveň	k1gFnSc4	úroveň
povrchové	povrchový	k2eAgFnSc2d1	povrchová
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Magnetické	magnetický	k2eAgNnSc1d1	magnetické
pole	pole	k1gNnSc1	pole
může	moct	k5eAaImIp3nS	moct
působit	působit	k5eAaImF	působit
na	na	k7c4	na
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
vítr	vítr	k1gInSc4	vítr
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
zpomalit	zpomalit	k5eAaPmF	zpomalit
rotaci	rotace	k1gFnSc4	rotace
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
starší	starý	k2eAgFnPc1d2	starší
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
rotují	rotovat	k5eAaImIp3nP	rotovat
mnohem	mnohem	k6eAd1	mnohem
pomaleji	pomale	k6eAd2	pomale
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
nízkou	nízký	k2eAgFnSc4d1	nízká
povrchovou	povrchový	k2eAgFnSc4d1	povrchová
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Úroveň	úroveň	k1gFnSc4	úroveň
aktivity	aktivita	k1gFnSc2	aktivita
starších	starý	k2eAgFnPc2d2	starší
hvězd	hvězda	k1gFnPc2	hvězda
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
cyklicky	cyklicky	k6eAd1	cyklicky
mění	měnit	k5eAaImIp3nS	měnit
a	a	k8xC	a
na	na	k7c4	na
určité	určitý	k2eAgNnSc4d1	určité
období	období	k1gNnSc4	období
může	moct	k5eAaImIp3nS	moct
zcela	zcela	k6eAd1	zcela
ustát	ustát	k5eAaPmF	ustát
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřek	vnitřek	k1gInSc1	vnitřek
stabilní	stabilní	k2eAgFnSc2d1	stabilní
hvězdy	hvězda	k1gFnSc2	hvězda
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
hydrostatické	hydrostatický	k2eAgFnSc2d1	hydrostatická
rovnováhy	rovnováha	k1gFnSc2	rovnováha
<g/>
:	:	kIx,	:
síly	síla	k1gFnSc2	síla
působící	působící	k2eAgFnPc1d1	působící
na	na	k7c4	na
vybraný	vybraný	k2eAgInSc4d1	vybraný
malý	malý	k2eAgInSc4d1	malý
objem	objem	k1gInSc4	objem
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
přesně	přesně	k6eAd1	přesně
vyrovnávají	vyrovnávat	k5eAaImIp3nP	vyrovnávat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tyto	tento	k3xDgFnPc4	tento
síly	síla	k1gFnPc4	síla
patří	patřit	k5eAaImIp3nS	patřit
gravitační	gravitační	k2eAgFnSc1d1	gravitační
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
neustále	neustále	k6eAd1	neustále
stlačuje	stlačovat	k5eAaImIp3nS	stlačovat
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
,	,	kIx,	,
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
způsobený	způsobený	k2eAgInSc1d1	způsobený
vznikající	vznikající	k2eAgFnSc7d1	vznikající
energií	energie	k1gFnSc7	energie
následkem	následkem	k7c2	následkem
fúze	fúze	k1gFnSc2	fúze
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
působí	působit	k5eAaImIp3nS	působit
směrem	směr	k1gInSc7	směr
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Tlakový	tlakový	k2eAgInSc1d1	tlakový
gradient	gradient	k1gInSc1	gradient
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
teplotním	teplotní	k2eAgInSc7d1	teplotní
gradientem	gradient	k1gInSc7	gradient
plazmatu	plazma	k1gNnSc2	plazma
<g/>
:	:	kIx,	:
vnější	vnější	k2eAgFnPc1d1	vnější
části	část	k1gFnPc1	část
hvězdy	hvězda	k1gFnSc2	hvězda
jsou	být	k5eAaImIp3nP	být
chladnější	chladný	k2eAgFnPc1d2	chladnější
než	než	k8xS	než
jádro	jádro	k1gNnSc1	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
jádra	jádro	k1gNnSc2	jádro
hvězdy	hvězda	k1gFnSc2	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
nebo	nebo	k8xC	nebo
obrů	obr	k1gMnPc2	obr
je	být	k5eAaImIp3nS	být
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
107	[number]	k4	107
K.	K.	kA	K.
Výsledná	výsledný	k2eAgFnSc1d1	výsledná
teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
hvězdy	hvězda	k1gFnSc2	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
spalující	spalující	k2eAgInSc4d1	spalující
vodík	vodík	k1gInSc4	vodík
jsou	být	k5eAaImIp3nP	být
dostatečné	dostatečný	k2eAgInPc1d1	dostatečný
k	k	k7c3	k
udržení	udržení	k1gNnSc3	udržení
jaderné	jaderný	k2eAgFnSc2d1	jaderná
fúze	fúze	k1gFnSc2	fúze
a	a	k8xC	a
produkují	produkovat	k5eAaImIp3nP	produkovat
dostatek	dostatek	k1gInSc4	dostatek
energie	energie	k1gFnSc2	energie
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zabránily	zabránit	k5eAaPmAgFnP	zabránit
dalšímu	další	k2eAgInSc3d1	další
kolapsu	kolaps	k1gInSc3	kolaps
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
nitru	nitro	k1gNnSc6	nitro
velmi	velmi	k6eAd1	velmi
podobnou	podobný	k2eAgFnSc4d1	podobná
stavbu	stavba	k1gFnSc4	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
teplotách	teplota	k1gFnPc6	teplota
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgInPc6	který
závisí	záviset	k5eAaImIp3nS	záviset
i	i	k9	i
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc1	jaký
typ	typ	k1gInSc1	typ
jaderné	jaderný	k2eAgFnSc2d1	jaderná
reakce	reakce	k1gFnSc2	reakce
v	v	k7c6	v
hvězdě	hvězda	k1gFnSc6	hvězda
probíhá	probíhat	k5eAaImIp3nS	probíhat
<g/>
.	.	kIx.	.
</s>
<s>
Vrstvy	vrstva	k1gFnPc1	vrstva
hvězdy	hvězda	k1gFnSc2	hvězda
směrem	směr	k1gInSc7	směr
zevnitř	zevnitř	k6eAd1	zevnitř
ven	ven	k6eAd1	ven
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Jádro	jádro	k1gNnSc1	jádro
-	-	kIx~	-
nejžhavější	žhavý	k2eAgFnSc1d3	nejžhavější
a	a	k8xC	a
nejhustší	hustý	k2eAgFnSc1d3	nejhustší
část	část	k1gFnSc1	část
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Jádra	jádro	k1gNnPc1	jádro
jsou	být	k5eAaImIp3nP	být
zdroje	zdroj	k1gInPc4	zdroj
energie	energie	k1gFnSc2	energie
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
přenáší	přenášet	k5eAaImIp3nS	přenášet
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
do	do	k7c2	do
okolního	okolní	k2eAgNnSc2d1	okolní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
jaderné	jaderný	k2eAgFnSc2d1	jaderná
fúze	fúze	k1gFnSc2	fúze
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
se	se	k3xPyFc4	se
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
energie	energie	k1gFnSc1	energie
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
gama	gama	k1gNnSc1	gama
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
fotony	foton	k1gInPc1	foton
interagují	interagovat	k5eAaBmIp3nP	interagovat
s	s	k7c7	s
okolním	okolní	k2eAgNnSc7d1	okolní
plazmatem	plazma	k1gNnSc7	plazma
a	a	k8xC	a
tak	tak	k9	tak
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
energii	energie	k1gFnSc4	energie
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
spalují	spalovat	k5eAaImIp3nP	spalovat
vodík	vodík	k1gInSc4	vodík
na	na	k7c4	na
helium	helium	k1gNnSc4	helium
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
podíl	podíl	k1gInSc4	podíl
hélia	hélium	k1gNnSc2	hélium
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
hydrostatické	hydrostatický	k2eAgFnSc2d1	hydrostatická
rovnováhy	rovnováha	k1gFnSc2	rovnováha
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
jádro	jádro	k1gNnSc1	jádro
stabilní	stabilní	k2eAgFnSc2d1	stabilní
hvězdy	hvězda	k1gFnSc2	hvězda
i	i	k8xC	i
energetické	energetický	k2eAgFnSc2d1	energetická
rovnováhy	rovnováha	k1gFnSc2	rovnováha
-	-	kIx~	-
tepelné	tepelný	k2eAgFnSc2d1	tepelná
rovnováhy	rovnováha	k1gFnSc2	rovnováha
<g/>
.	.	kIx.	.
</s>
<s>
Vrstva	vrstva	k1gFnSc1	vrstva
v	v	k7c6	v
zářivé	zářivý	k2eAgFnSc6d1	zářivá
rovnováze	rovnováha	k1gFnSc6	rovnováha
-	-	kIx~	-
velmi	velmi	k6eAd1	velmi
silná	silný	k2eAgFnSc1d1	silná
vrstva	vrstva	k1gFnSc1	vrstva
plazmatu	plazma	k1gNnSc2	plazma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
jádro	jádro	k1gNnSc4	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
také	také	k9	také
radiační	radiační	k2eAgFnSc1d1	radiační
zóna	zóna	k1gFnSc1	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
oblast	oblast	k1gFnSc1	oblast
uvnitř	uvnitř	k7c2	uvnitř
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
je	být	k5eAaImIp3nS	být
záření	záření	k1gNnSc1	záření
dostatečně	dostatečně	k6eAd1	dostatečně
efektivní	efektivní	k2eAgMnSc1d1	efektivní
k	k	k7c3	k
udržení	udržení	k1gNnSc3	udržení
toku	tok	k1gInSc2	tok
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Fotony	foton	k1gInPc1	foton
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
<g/>
,	,	kIx,	,
procházejí	procházet	k5eAaImIp3nP	procházet
touto	tento	k3xDgFnSc7	tento
vrstvou	vrstva	k1gFnSc7	vrstva
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
vlnová	vlnový	k2eAgFnSc1d1	vlnová
délka	délka	k1gFnSc1	délka
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
velké	velký	k2eAgFnSc3d1	velká
hustotě	hustota	k1gFnSc3	hustota
prostředí	prostředí	k1gNnSc2	prostředí
je	být	k5eAaImIp3nS	být
foton	foton	k1gInSc1	foton
neustále	neustále	k6eAd1	neustále
pohlcován	pohlcován	k2eAgInSc1d1	pohlcován
a	a	k8xC	a
vyzařován	vyzařován	k2eAgInSc1d1	vyzařován
okolní	okolní	k2eAgFnSc7d1	okolní
hmotou	hmota	k1gFnSc7	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Konvektivní	Konvektivní	k2eAgFnSc1d1	Konvektivní
zóna	zóna	k1gFnSc1	zóna
-	-	kIx~	-
ještě	ještě	k9	ještě
chladnější	chladný	k2eAgFnSc1d2	chladnější
vrstva	vrstva	k1gFnSc1	vrstva
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
energie	energie	k1gFnSc1	energie
přenáší	přenášet	k5eAaImIp3nS	přenášet
prouděním	proudění	k1gNnSc7	proudění
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholky	vrcholek	k1gInPc1	vrcholek
sestupných	sestupný	k2eAgInPc2d1	sestupný
a	a	k8xC	a
vzestupných	vzestupný	k2eAgInPc2d1	vzestupný
proudů	proud	k1gInPc2	proud
můžeme	moct	k5eAaImIp1nP	moct
vidět	vidět	k5eAaImF	vidět
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
hvězdy	hvězda	k1gFnSc2	hvězda
jako	jako	k8xS	jako
útvary	útvar	k1gInPc4	útvar
zvané	zvaný	k2eAgFnSc2d1	zvaná
granule	granule	k1gFnSc2	granule
<g/>
.	.	kIx.	.
</s>
<s>
Fotosféra	fotosféra	k1gFnSc1	fotosféra
-	-	kIx~	-
viditelný	viditelný	k2eAgInSc1d1	viditelný
(	(	kIx(	(
<g/>
ne	ne	k9	ne
však	však	k9	však
pevný	pevný	k2eAgInSc4d1	pevný
<g/>
)	)	kIx)	)
povrch	povrch	k1gInSc4	povrch
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
vrstvě	vrstva	k1gFnSc6	vrstva
se	se	k3xPyFc4	se
plazma	plazma	k1gNnSc1	plazma
stává	stávat	k5eAaImIp3nS	stávat
průhledné	průhledný	k2eAgNnSc1d1	průhledné
pro	pro	k7c4	pro
fotony	foton	k1gInPc4	foton
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
vygenerovaná	vygenerovaný	k2eAgFnSc1d1	vygenerovaná
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
se	se	k3xPyFc4	se
odsud	odsud	k6eAd1	odsud
může	moct	k5eAaImIp3nS	moct
volně	volně	k6eAd1	volně
šířit	šířit	k5eAaImF	šířit
do	do	k7c2	do
okolního	okolní	k2eAgInSc2d1	okolní
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejchladnější	chladný	k2eAgFnSc1d3	nejchladnější
část	část	k1gFnSc1	část
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
při	při	k7c6	při
velmi	velmi	k6eAd1	velmi
chladných	chladný	k2eAgFnPc6d1	chladná
hvězdách	hvězda	k1gFnPc6	hvězda
nebo	nebo	k8xC	nebo
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
hvězdných	hvězdný	k2eAgFnPc2d1	hvězdná
skvrn	skvrna	k1gFnPc2	skvrna
(	(	kIx(	(
<g/>
slunečních	sluneční	k2eAgFnPc2d1	sluneční
skvrn	skvrna	k1gFnPc2	skvrna
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
dokonce	dokonce	k9	dokonce
udrží	udržet	k5eAaPmIp3nP	udržet
chemické	chemický	k2eAgFnPc1d1	chemická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
fotosférou	fotosféra	k1gFnSc7	fotosféra
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
atmosféra	atmosféra	k1gFnSc1	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Chromosféra	chromosféra	k1gFnSc1	chromosféra
-	-	kIx~	-
spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
atmosféry	atmosféra	k1gFnSc2	atmosféra
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
chromosféře	chromosféra	k1gFnSc6	chromosféra
opět	opět	k6eAd1	opět
začíná	začínat	k5eAaImIp3nS	začínat
stoupat	stoupat	k5eAaImF	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Koróna	koróna	k1gFnSc1	koróna
-	-	kIx~	-
nejsvrchnější	svrchní	k2eAgFnSc1d3	nejsvrchnější
<g/>
,	,	kIx,	,
nejžhavější	žhavý	k2eAgFnSc1d3	nejžhavější
a	a	k8xC	a
nejméně	málo	k6eAd3	málo
hustá	hustý	k2eAgFnSc1d1	hustá
vnější	vnější	k2eAgFnSc1d1	vnější
atmosféra	atmosféra	k1gFnSc1	atmosféra
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozplývá	rozplývat	k5eAaImIp3nS	rozplývat
do	do	k7c2	do
mezihvězdného	mezihvězdný	k2eAgInSc2d1	mezihvězdný
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
fotosféry	fotosféra	k1gFnSc2	fotosféra
teplota	teplota	k1gFnSc1	teplota
hvězdy	hvězda	k1gFnSc2	hvězda
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
jádra	jádro	k1gNnSc2	jádro
k	k	k7c3	k
povrchu	povrch	k1gInSc3	povrch
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgFnPc6d1	další
vrstvách	vrstva	k1gFnPc6	vrstva
opět	opět	k6eAd1	opět
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Příčiny	příčina	k1gFnPc1	příčina
tohoto	tento	k3xDgInSc2	tento
jevu	jev	k1gInSc2	jev
nejsou	být	k5eNaImIp3nP	být
dosud	dosud	k6eAd1	dosud
plně	plně	k6eAd1	plně
objasněny	objasněn	k2eAgFnPc1d1	objasněna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
těleso	těleso	k1gNnSc1	těleso
dalo	dát	k5eAaPmAgNnS	dát
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
jako	jako	k9	jako
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
nitru	nitro	k1gNnSc6	nitro
probíhat	probíhat	k5eAaImF	probíhat
termojaderné	termojaderný	k2eAgFnPc4d1	termojaderná
reakce	reakce	k1gFnPc4	reakce
nebo	nebo	k8xC	nebo
muselo	muset	k5eAaImAgNnS	muset
fází	fáze	k1gFnPc2	fáze
fúzních	fúzní	k2eAgFnPc2d1	fúzní
reakcí	reakce	k1gFnPc2	reakce
projít	projít	k5eAaPmF	projít
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Termojaderná	termojaderný	k2eAgFnSc1d1	termojaderná
reakce	reakce	k1gFnSc1	reakce
je	být	k5eAaImIp3nS	být
reakce	reakce	k1gFnPc4	reakce
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
jádra	jádro	k1gNnPc4	jádro
atomů	atom	k1gInPc2	atom
lehkých	lehký	k2eAgInPc2d1	lehký
prvků	prvek	k1gInPc2	prvek
sloučí	sloučit	k5eAaPmIp3nP	sloučit
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
těžšího	těžký	k2eAgInSc2d2	těžší
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
jádra	jádro	k1gNnPc1	jádro
atomů	atom	k1gInPc2	atom
jsou	být	k5eAaImIp3nP	být
kladně	kladně	k6eAd1	kladně
nabitá	nabitý	k2eAgFnSc1d1	nabitá
a	a	k8xC	a
navzájem	navzájem	k6eAd1	navzájem
se	se	k3xPyFc4	se
silně	silně	k6eAd1	silně
odpuzují	odpuzovat	k5eAaImIp3nP	odpuzovat
<g/>
,	,	kIx,	,
ke	k	k7c3	k
spuštění	spuštění	k1gNnSc3	spuštění
termojaderné	termojaderný	k2eAgFnSc2d1	termojaderná
reakce	reakce	k1gFnSc2	reakce
je	být	k5eAaImIp3nS	být
potřebná	potřebný	k2eAgFnSc1d1	potřebná
velmi	velmi	k6eAd1	velmi
vysoká	vysoký	k2eAgFnSc1d1	vysoká
teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tyto	tento	k3xDgFnPc1	tento
odpudivé	odpudivý	k2eAgFnPc1d1	odpudivá
síly	síla	k1gFnPc1	síla
překonají	překonat	k5eAaPmIp3nP	překonat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
velké	velký	k2eAgFnSc2d1	velká
většiny	většina	k1gFnSc2	většina
hvězd	hvězda	k1gFnPc2	hvězda
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
hlavní	hlavní	k2eAgFnPc4d1	hlavní
posloupnosti	posloupnost	k1gFnPc4	posloupnost
<g/>
)	)	kIx)	)
vstupují	vstupovat	k5eAaImIp3nP	vstupovat
do	do	k7c2	do
reakce	reakce	k1gFnSc2	reakce
jádra	jádro	k1gNnSc2	jádro
nejlehčího	lehký	k2eAgInSc2d3	nejlehčí
chemického	chemický	k2eAgInSc2d1	chemický
prvku	prvek	k1gInSc2	prvek
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
výsledným	výsledný	k2eAgInSc7d1	výsledný
produktem	produkt	k1gInSc7	produkt
je	být	k5eAaImIp3nS	být
helium	helium	k1gNnSc1	helium
<g/>
.	.	kIx.	.
</s>
<s>
Přeměna	přeměna	k1gFnSc1	přeměna
lehkého	lehký	k2eAgInSc2d1	lehký
vodíku	vodík	k1gInSc2	vodík
na	na	k7c4	na
helium	helium	k1gNnSc4	helium
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
dvěma	dva	k4xCgInPc7	dva
odlišnými	odlišný	k2eAgInPc7d1	odlišný
způsoby	způsob	k1gInPc7	způsob
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
proton-protonovým	protonrotonový	k2eAgInSc7d1	proton-protonový
cyklem	cyklus	k1gInSc7	cyklus
nebo	nebo	k8xC	nebo
uhlík-dusík-kyslíkovým	uhlíkusíkyslíkův	k2eAgInSc7d1	uhlík-dusík-kyslíkův
cyklem	cyklus	k1gInSc7	cyklus
(	(	kIx(	(
<g/>
nazývaným	nazývaný	k2eAgMnSc7d1	nazývaný
také	také	k9	také
CNO	CNO	kA	CNO
cyklus	cyklus	k1gInSc1	cyklus
podle	podle	k7c2	podle
chemických	chemický	k2eAgFnPc2d1	chemická
značek	značka	k1gFnPc2	značka
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
účastní	účastnit	k5eAaImIp3nS	účastnit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
cyklů	cyklus	k1gInPc2	cyklus
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
hvězdy	hvězda	k1gFnSc2	hvězda
převládá	převládat	k5eAaImIp3nS	převládat
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
hlavně	hlavně	k9	hlavně
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
16	[number]	k4	16
milionů	milion	k4xCgInPc2	milion
kelvinů	kelvin	k1gInPc2	kelvin
je	být	k5eAaImIp3nS	být
dominantní	dominantní	k2eAgInSc1d1	dominantní
proton-protonový	protonrotonový	k2eAgInSc1d1	proton-protonový
cyklus	cyklus	k1gInSc1	cyklus
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
touto	tento	k3xDgFnSc7	tento
hranicí	hranice	k1gFnSc7	hranice
převládá	převládat	k5eAaImIp3nS	převládat
CNO	CNO	kA	CNO
cyklus	cyklus	k1gInSc1	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
fungování	fungování	k1gNnSc4	fungování
CNO	CNO	kA	CNO
cyklu	cyklus	k1gInSc2	cyklus
je	být	k5eAaImIp3nS	být
nezbytná	zbytný	k2eNgFnSc1d1	zbytný
také	také	k9	také
přítomnost	přítomnost	k1gFnSc1	přítomnost
těchto	tento	k3xDgInPc2	tento
tří	tři	k4xCgInPc2	tři
prvků	prvek	k1gInPc2	prvek
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Čistá	čistý	k2eAgFnSc1d1	čistá
váha	váha	k1gFnSc1	váha
nově	nově	k6eAd1	nově
vzniklého	vzniklý	k2eAgNnSc2d1	vzniklé
atomového	atomový	k2eAgNnSc2d1	atomové
jádra	jádro	k1gNnSc2	jádro
v	v	k7c6	v
termojaderné	termojaderný	k2eAgFnSc6d1	termojaderná
reakci	reakce	k1gFnSc6	reakce
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgMnSc1d2	menší
než	než	k8xS	než
součet	součet	k1gInSc1	součet
hmotností	hmotnost	k1gFnPc2	hmotnost
původních	původní	k2eAgFnPc2d1	původní
jader	jádro	k1gNnPc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
obou	dva	k4xCgInPc6	dva
cyklech	cyklus	k1gInPc6	cyklus
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
140	[number]	k4	140
hmoty	hmota	k1gFnSc2	hmota
přemění	přeměnit	k5eAaPmIp3nP	přeměnit
na	na	k7c4	na
čistou	čistý	k2eAgFnSc4d1	čistá
energii	energie	k1gFnSc4	energie
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
Einsteinovou	Einsteinův	k2eAgFnSc7d1	Einsteinova
rovnicí	rovnice	k1gFnSc7	rovnice
E	E	kA	E
=	=	kIx~	=
mc2	mc2	k4	mc2
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
fúze	fúze	k1gFnSc2	fúze
vodíku	vodík	k1gInSc2	vodík
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
citlivý	citlivý	k2eAgInSc1d1	citlivý
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
i	i	k9	i
mírné	mírný	k2eAgNnSc1d1	mírné
zvýšení	zvýšení	k1gNnSc1	zvýšení
teploty	teplota	k1gFnSc2	teplota
jádra	jádro	k1gNnSc2	jádro
způsobí	způsobit	k5eAaPmIp3nS	způsobit
značný	značný	k2eAgInSc4d1	značný
nárůst	nárůst	k1gInSc4	nárůst
v	v	k7c6	v
rychlosti	rychlost	k1gFnSc6	rychlost
fúze	fúze	k1gFnSc2	fúze
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
teploty	teplota	k1gFnPc4	teplota
v	v	k7c6	v
jádrech	jádro	k1gNnPc6	jádro
hvězd	hvězda	k1gFnPc2	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
od	od	k7c2	od
4	[number]	k4	4
milionů	milion	k4xCgInPc2	milion
kelvinů	kelvin	k1gInPc2	kelvin
pro	pro	k7c4	pro
malé	malý	k2eAgFnPc4d1	malá
hvězdy	hvězda	k1gFnPc4	hvězda
třídy	třída	k1gFnSc2	třída
M	M	kA	M
po	po	k7c4	po
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
kelvinů	kelvin	k1gInPc2	kelvin
při	při	k7c6	při
těžkých	těžký	k2eAgFnPc6d1	těžká
hvězdách	hvězda	k1gFnPc6	hvězda
třídy	třída	k1gFnSc2	třída
O.	O.	kA	O.
Ve	v	k7c6	v
Slunci	slunce	k1gNnSc6	slunce
<g/>
,	,	kIx,	,
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
kelvinů	kelvin	k1gInPc2	kelvin
<g/>
,	,	kIx,	,
probíhá	probíhat	k5eAaImIp3nS	probíhat
fúze	fúze	k1gFnSc1	fúze
vodíku	vodík	k1gInSc2	vodík
proton-protonovým	protonrotonový	k2eAgInSc7d1	proton-protonový
cyklem	cyklus	k1gInSc7	cyklus
<g/>
:	:	kIx,	:
41H	[number]	k4	41H
→	→	k?	→
22H	[number]	k4	22H
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
<g/>
e	e	k0	e
<g/>
+	+	kIx~	+
+	+	kIx~	+
2	[number]	k4	2
<g/>
ν	ν	k?	ν
(	(	kIx(	(
<g/>
4,0	[number]	k4	4,0
MeV	MeV	k1gFnPc2	MeV
+	+	kIx~	+
1,0	[number]	k4	1,0
MeV	MeV	k1gFnPc2	MeV
<g/>
)	)	kIx)	)
21H	[number]	k4	21H
+	+	kIx~	+
22H	[number]	k4	22H
→	→	k?	→
23	[number]	k4	23
<g/>
He	he	k0	he
+	+	kIx~	+
2	[number]	k4	2
<g/>
γ	γ	k?	γ
(	(	kIx(	(
<g/>
5,5	[number]	k4	5,5
MeV	MeV	k1gFnPc2	MeV
<g/>
)	)	kIx)	)
23	[number]	k4	23
<g/>
He	he	k0	he
→	→	k?	→
4	[number]	k4	4
<g/>
He	he	k0	he
+	+	kIx~	+
21H	[number]	k4	21H
(	(	kIx(	(
<g/>
12,9	[number]	k4	12,9
MeV	MeV	k1gFnPc2	MeV
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Sumárum	sumárum	k9	sumárum
těchto	tento	k3xDgFnPc2	tento
reakcí	reakce	k1gFnPc2	reakce
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
41H	[number]	k4	41H
→	→	k?	→
4	[number]	k4	4
<g/>
He	he	k0	he
+	+	kIx~	+
2	[number]	k4	2
<g/>
e	e	k0	e
<g/>
+	+	kIx~	+
+	+	kIx~	+
2	[number]	k4	2
<g/>
γ	γ	k?	γ
+	+	kIx~	+
2	[number]	k4	2
<g/>
ν	ν	k?	ν
(	(	kIx(	(
<g/>
26,7	[number]	k4	26,7
MeV	MeV	k1gFnPc2	MeV
<g/>
)	)	kIx)	)
kde	kde	k6eAd1	kde
e	e	k0	e
<g/>
+	+	kIx~	+
je	být	k5eAaImIp3nS	být
pozitron	pozitron	k1gInSc1	pozitron
<g/>
,	,	kIx,	,
γ	γ	k?	γ
je	být	k5eAaImIp3nS	být
foton	foton	k1gInSc1	foton
gama	gama	k1gNnSc2	gama
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
ν	ν	k?	ν
je	být	k5eAaImIp3nS	být
neutrino	neutrino	k1gNnSc1	neutrino
a	a	k8xC	a
H	H	kA	H
a	a	k8xC	a
He	he	k0	he
jsou	být	k5eAaImIp3nP	být
izotopy	izotop	k1gInPc1	izotop
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
hélia	hélium	k1gNnSc2	hélium
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
uvolněná	uvolněný	k2eAgFnSc1d1	uvolněná
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
reakci	reakce	k1gFnSc6	reakce
je	být	k5eAaImIp3nS	být
řádově	řádově	k6eAd1	řádově
v	v	k7c6	v
megaelektronvoltech	megaelektronvolt	k1gInPc6	megaelektronvolt
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
maličké	maličký	k2eAgNnSc1d1	maličké
množství	množství	k1gNnSc1	množství
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
však	však	k9	však
neustále	neustále	k6eAd1	neustále
probíhá	probíhat	k5eAaImIp3nS	probíhat
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
těchto	tento	k3xDgFnPc2	tento
reakcí	reakce	k1gFnPc2	reakce
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc1	množství
energie	energie	k1gFnSc2	energie
je	být	k5eAaImIp3nS	být
dostatečné	dostatečný	k2eAgNnSc1d1	dostatečné
k	k	k7c3	k
udržení	udržení	k1gNnSc3	udržení
výstupu	výstup	k1gInSc2	výstup
záření	záření	k1gNnSc2	záření
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jádrech	jádro	k1gNnPc6	jádro
s	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
K	K	kA	K
a	a	k8xC	a
hmotností	hmotnost	k1gFnSc7	hmotnost
0,5	[number]	k4	0,5
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
hmotností	hmotnost	k1gFnSc7	hmotnost
Slunce	slunce	k1gNnSc2	slunce
může	moct	k5eAaImIp3nS	moct
helium	helium	k1gNnSc4	helium
vytvářet	vytvářet	k5eAaImF	vytvářet
uhlík	uhlík	k1gInSc4	uhlík
v	v	k7c4	v
3	[number]	k4	3
<g/>
-alfa	lf	k1gMnSc4	-alf
reakci	reakce	k1gFnSc6	reakce
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jako	jako	k9	jako
pomocný	pomocný	k2eAgInSc1d1	pomocný
krok	krok	k1gInSc1	krok
využívá	využívat	k5eAaImIp3nS	využívat
beryllium	beryllium	k1gNnSc4	beryllium
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
He	he	k0	he
+	+	kIx~	+
4	[number]	k4	4
<g/>
He	he	k0	he
+	+	kIx~	+
92	[number]	k4	92
keV	keV	k?	keV
→	→	k?	→
8	[number]	k4	8
<g/>
*	*	kIx~	*
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Be	Be	k1gFnSc1	Be
4	[number]	k4	4
<g/>
He	he	k0	he
+	+	kIx~	+
8	[number]	k4	8
<g/>
*	*	kIx~	*
<g/>
Be	Be	k1gFnSc2	Be
+	+	kIx~	+
67	[number]	k4	67
keV	keV	k?	keV
→	→	k?	→
12	[number]	k4	12
<g/>
*	*	kIx~	*
<g/>
C	C	kA	C
12	[number]	k4	12
<g/>
*	*	kIx~	*
<g/>
C	C	kA	C
→	→	k?	→
12C	[number]	k4	12C
+	+	kIx~	+
γ	γ	k?	γ
+	+	kIx~	+
7,4	[number]	k4	7,4
MeV	MeV	k1gFnSc4	MeV
Celková	celkový	k2eAgFnSc1d1	celková
reakce	reakce	k1gFnSc1	reakce
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
34	[number]	k4	34
<g/>
He	he	k0	he
→	→	k?	→
12C	[number]	k4	12C
+	+	kIx~	+
γ	γ	k?	γ
+	+	kIx~	+
7,2	[number]	k4	7,2
MeV	MeV	k1gMnSc2	MeV
Hvězdy	Hvězda	k1gMnSc2	Hvězda
spalují	spalovat	k5eAaImIp3nP	spalovat
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
jádrech	jádro	k1gNnPc6	jádro
i	i	k8xC	i
jiné	jiný	k2eAgNnSc1d1	jiné
chemické	chemický	k2eAgInPc1d1	chemický
prvky	prvek	k1gInPc1	prvek
než	než	k8xS	než
vodík	vodík	k1gInSc1	vodík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
protohvězdách	protohvězda	k1gFnPc6	protohvězda
během	během	k7c2	během
jejich	jejich	k3xOp3gInSc2	jejich
vzniku	vznik	k1gInSc2	vznik
postupně	postupně	k6eAd1	postupně
se	s	k7c7	s
vzrůstající	vzrůstající	k2eAgFnSc7d1	vzrůstající
teplotou	teplota	k1gFnSc7	teplota
a	a	k8xC	a
tlakem	tlak	k1gInSc7	tlak
dochází	docházet	k5eAaImIp3nS	docházet
nejprve	nejprve	k6eAd1	nejprve
ke	k	k7c3	k
spalování	spalování	k1gNnSc3	spalování
těžkého	těžký	k2eAgInSc2d1	těžký
vodíku	vodík	k1gInSc2	vodík
(	(	kIx(	(
<g/>
deuteria	deuterium	k1gNnSc2	deuterium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lithia	lithium	k1gNnSc2	lithium
<g/>
,	,	kIx,	,
berylia	berylium	k1gNnSc2	berylium
a	a	k8xC	a
bóru	bór	k1gInSc2	bór
s	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
<g/>
,	,	kIx,	,
než	než	k8xS	než
dojde	dojít	k5eAaPmIp3nS	dojít
na	na	k7c6	na
spalování	spalování	k1gNnSc6	spalování
čistého	čistý	k2eAgInSc2d1	čistý
lehkého	lehký	k2eAgInSc2d1	lehký
vodíku	vodík	k1gInSc2	vodík
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
nejvíce	hodně	k6eAd3	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Výsledným	výsledný	k2eAgInSc7d1	výsledný
produktem	produkt	k1gInSc7	produkt
všech	všecek	k3xTgFnPc2	všecek
těchto	tento	k3xDgFnPc2	tento
reakcí	reakce	k1gFnPc2	reakce
je	být	k5eAaImIp3nS	být
helium	helium	k1gNnSc1	helium
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
starších	starý	k2eAgFnPc6d2	starší
hvězdách	hvězda	k1gFnPc6	hvězda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
blízko	blízko	k7c2	blízko
svého	svůj	k3xOyFgInSc2	svůj
zániku	zánik	k1gInSc2	zánik
<g/>
,	,	kIx,	,
však	však	k9	však
nastává	nastávat	k5eAaImIp3nS	nastávat
spalování	spalování	k1gNnSc1	spalování
hélia	hélium	k1gNnSc2	hélium
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
výsledkem	výsledek	k1gInSc7	výsledek
jsou	být	k5eAaImIp3nP	být
jiné	jiný	k2eAgInPc1d1	jiný
produkty	produkt	k1gInPc1	produkt
(	(	kIx(	(
<g/>
různé	různý	k2eAgInPc4d1	různý
chemické	chemický	k2eAgInPc4d1	chemický
prvky	prvek	k1gInPc4	prvek
až	až	k9	až
po	po	k7c4	po
železo	železo	k1gNnSc4	železo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
spalováním	spalování	k1gNnSc7	spalování
křemíku	křemík	k1gInSc2	křemík
-	-	kIx~	-
uhlík	uhlík	k1gInSc1	uhlík
s	s	k7c7	s
héliem	hélium	k1gNnSc7	hélium
na	na	k7c4	na
kyslík	kyslík	k1gInSc4	kyslík
<g/>
,	,	kIx,	,
kyslík	kyslík	k1gInSc4	kyslík
na	na	k7c4	na
neon	neon	k1gInSc4	neon
<g/>
,	,	kIx,	,
neon	neon	k1gInSc4	neon
na	na	k7c4	na
hořčík	hořčík	k1gInSc4	hořčík
<g/>
,	,	kIx,	,
hořčík	hořčík	k1gInSc4	hořčík
na	na	k7c4	na
křemík	křemík	k1gInSc4	křemík
a	a	k8xC	a
křemík	křemík	k1gInSc4	křemík
na	na	k7c4	na
železo	železo	k1gNnSc4	železo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spalování	spalování	k1gNnSc1	spalování
může	moct	k5eAaImIp3nS	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
dál	daleko	k6eAd2	daleko
jen	jen	k9	jen
pomocí	pomocí	k7c2	pomocí
procesu	proces	k1gInSc2	proces
spotřebovávajícího	spotřebovávající	k2eAgInSc2d1	spotřebovávající
teplo	teplo	k6eAd1	teplo
a	a	k8xC	a
dodatečnou	dodatečný	k2eAgFnSc4d1	dodatečná
energii	energie	k1gFnSc4	energie
může	moct	k5eAaImIp3nS	moct
dodat	dodat	k5eAaPmF	dodat
pouze	pouze	k6eAd1	pouze
gravitační	gravitační	k2eAgInSc4d1	gravitační
kolaps	kolaps	k1gInSc4	kolaps
<g/>
.	.	kIx.	.
</s>
<s>
Prvky	prvek	k1gInPc4	prvek
těžší	těžký	k2eAgInPc4d2	těžší
než	než	k8xS	než
železo	železo	k1gNnSc4	železo
proto	proto	k8xC	proto
vznikají	vznikat	k5eAaImIp3nP	vznikat
jen	jen	k9	jen
v	v	k7c6	v
supernovách	supernova	k1gFnPc6	supernova
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
druhy	druh	k1gInPc1	druh
reakcí	reakce	k1gFnPc2	reakce
mají	mít	k5eAaImIp3nP	mít
velký	velký	k2eAgInSc4d1	velký
význam	význam	k1gInSc4	význam
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
vzniku	vznik	k1gInSc3	vznik
života	život	k1gInSc2	život
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
a	a	k8xC	a
terestrických	terestrický	k2eAgFnPc2d1	terestrická
planet	planeta	k1gFnPc2	planeta
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jádra	jádro	k1gNnPc1	jádro
starších	starý	k2eAgFnPc2d2	starší
hvězd	hvězda	k1gFnPc2	hvězda
jsou	být	k5eAaImIp3nP	být
jediným	jediný	k2eAgNnSc7d1	jediné
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
tyto	tento	k3xDgInPc1	tento
chemické	chemický	k2eAgInPc1d1	chemický
prvky	prvek	k1gInPc1	prvek
vznikají	vznikat	k5eAaImIp3nP	vznikat
(	(	kIx(	(
<g/>
nepočítaje	nepočítaje	k7c4	nepočítaje
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
produktem	produkt	k1gInSc7	produkt
samovolného	samovolný	k2eAgInSc2d1	samovolný
rozpadu	rozpad	k1gInSc2	rozpad
těžších	těžký	k2eAgNnPc2d2	těžší
jader	jádro	k1gNnPc2	jádro
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
níže	nízce	k6eAd2	nízce
popisuje	popisovat	k5eAaImIp3nS	popisovat
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
za	za	k7c4	za
který	který	k3yQgInSc4	který
hvězda	hvězda	k1gFnSc1	hvězda
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
20	[number]	k4	20
hmotností	hmotnost	k1gFnPc2	hmotnost
Slunce	slunce	k1gNnSc2	slunce
spotřebuje	spotřebovat	k5eAaPmIp3nS	spotřebovat
veškeré	veškerý	k3xTgNnSc4	veškerý
jaderné	jaderný	k2eAgNnSc4d1	jaderné
palivo	palivo	k1gNnSc4	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
hvězda	hvězda	k1gFnSc1	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
třídy	třída	k1gFnSc2	třída
bude	být	k5eAaImBp3nS	být
62	[number]	k4	62
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
krát	krát	k6eAd1	krát
svítivější	svítivý	k2eAgFnSc1d2	svítivější
než	než	k8xS	než
Slunce	slunce	k1gNnSc1	slunce
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
průměr	průměr	k1gInSc1	průměr
bude	být	k5eAaImBp3nS	být
8	[number]	k4	8
<g/>
krát	krát	k6eAd1	krát
větší	veliký	k2eAgInSc4d2	veliký
než	než	k8xS	než
průměr	průměr	k1gInSc4	průměr
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgFnPc1d1	mladá
hvězdy	hvězda	k1gFnPc1	hvězda
předtím	předtím	k6eAd1	předtím
<g/>
,	,	kIx,	,
než	než	k8xS	než
dosáhnou	dosáhnout	k5eAaPmIp3nP	dosáhnout
hlavní	hlavní	k2eAgFnPc4d1	hlavní
posloupnosti	posloupnost	k1gFnPc4	posloupnost
<g/>
,	,	kIx,	,
získávají	získávat	k5eAaImIp3nP	získávat
energii	energie	k1gFnSc4	energie
gravitační	gravitační	k2eAgFnSc7d1	gravitační
kontrakcí	kontrakce	k1gFnSc7	kontrakce
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
některé	některý	k3yIgFnPc1	některý
velké	velký	k2eAgFnPc1d1	velká
planety	planeta	k1gFnPc1	planeta
nebo	nebo	k8xC	nebo
hnědí	hnědý	k2eAgMnPc1d1	hnědý
trpaslíci	trpaslík	k1gMnPc1	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Gravitační	gravitační	k2eAgFnSc1d1	gravitační
kontrakce	kontrakce	k1gFnSc1	kontrakce
umožní	umožnit	k5eAaPmIp3nS	umožnit
vznikající	vznikající	k2eAgFnSc1d1	vznikající
hvězdě	hvězda	k1gFnSc3	hvězda
zvýšit	zvýšit	k5eAaPmF	zvýšit
teplotu	teplota	k1gFnSc4	teplota
a	a	k8xC	a
tlak	tlak	k1gInSc4	tlak
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
nitru	nitro	k1gNnSc6	nitro
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
spustily	spustit	k5eAaPmAgFnP	spustit
termojaderné	termojaderný	k2eAgFnPc1d1	termojaderná
reakce	reakce	k1gFnPc1	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Staré	Staré	k2eAgFnPc1d1	Staré
hvězdy	hvězda	k1gFnPc1	hvězda
po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
fáze	fáze	k1gFnSc2	fáze
jaderných	jaderný	k2eAgFnPc2d1	jaderná
reakcí	reakce	k1gFnPc2	reakce
mohou	moct	k5eAaImIp3nP	moct
svítit	svítit	k5eAaImF	svítit
z	z	k7c2	z
nazářených	nazářený	k2eAgFnPc2d1	nazářený
zásob	zásoba	k1gFnPc2	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
(	(	kIx(	(
<g/>
nedospělá	dospělý	k2eNgFnSc1d1	nedospělá
i	i	k8xC	i
stará	starý	k2eAgFnSc1d1	stará
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
)	)	kIx)	)
však	však	k9	však
tyto	tento	k3xDgFnPc1	tento
hvězdy	hvězda	k1gFnPc1	hvězda
ve	v	k7c6	v
viditelném	viditelný	k2eAgNnSc6d1	viditelné
spektru	spektrum	k1gNnSc6	spektrum
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
jen	jen	k9	jen
malý	malý	k2eAgInSc4d1	malý
zlomek	zlomek	k1gInSc4	zlomek
zářivého	zářivý	k2eAgInSc2d1	zářivý
výkonu	výkon	k1gInSc2	výkon
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
mají	mít	k5eAaImIp3nP	mít
hvězdy	hvězda	k1gFnPc1	hvězda
s	s	k7c7	s
probíhajícími	probíhající	k2eAgInPc7d1	probíhající
termonukleární	termonukleární	k2eAgFnPc1d1	termonukleární
reakcemi	reakce	k1gFnPc7	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vznik	vznik	k1gInSc1	vznik
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
vznikají	vznikat	k5eAaImIp3nP	vznikat
z	z	k7c2	z
původně	původně	k6eAd1	původně
chladných	chladný	k2eAgFnPc2d1	chladná
<g/>
,	,	kIx,	,
řídkých	řídký	k2eAgFnPc2d1	řídká
a	a	k8xC	a
studených	studený	k2eAgFnPc2d1	studená
mračen	mračna	k1gFnPc2	mračna
mezihvězdné	mezihvězdný	k2eAgFnSc2d1	mezihvězdná
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
těchto	tento	k3xDgFnPc2	tento
mračen	mračna	k1gFnPc2	mračna
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
než	než	k8xS	než
hustota	hustota	k1gFnSc1	hustota
mezihvězdného	mezihvězdný	k2eAgNnSc2d1	mezihvězdné
média	médium	k1gNnSc2	médium
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
hustota	hustota	k1gFnSc1	hustota
uvnitř	uvnitř	k7c2	uvnitř
vakuové	vakuový	k2eAgFnSc2d1	vakuová
komory	komora	k1gFnSc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
oblasti	oblast	k1gFnPc1	oblast
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
molekulární	molekulární	k2eAgFnSc1d1	molekulární
mračna	mračna	k1gFnSc1	mračna
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
tvořena	tvořit	k5eAaImNgFnS	tvořit
vodíkem	vodík	k1gInSc7	vodík
s	s	k7c7	s
příměsí	příměs	k1gFnSc7	příměs
~	~	kIx~	~
<g/>
23	[number]	k4	23
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
%	%	kIx~	%
helia	helium	k1gNnSc2	helium
a	a	k8xC	a
malým	malý	k2eAgNnSc7d1	malé
procentem	procento	k1gNnSc7	procento
těžších	těžký	k2eAgInPc2d2	těžší
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
takové	takový	k3xDgFnSc2	takový
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
vznikají	vznikat	k5eAaImIp3nP	vznikat
nové	nový	k2eAgFnPc1d1	nová
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mlhovina	mlhovina	k1gFnSc1	mlhovina
v	v	k7c6	v
Orionu	orion	k1gInSc6	orion
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
molekulárních	molekulární	k2eAgFnPc2d1	molekulární
mračen	mračna	k1gFnPc2	mračna
zde	zde	k6eAd1	zde
vznikají	vznikat	k5eAaImIp3nP	vznikat
obrovské	obrovský	k2eAgFnPc4d1	obrovská
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
osvětlují	osvětlovat	k5eAaImIp3nP	osvětlovat
tato	tento	k3xDgNnPc4	tento
mračna	mračno	k1gNnPc4	mračno
a	a	k8xC	a
také	také	k9	také
ionizují	ionizovat	k5eAaBmIp3nP	ionizovat
vodík	vodík	k1gInSc4	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
vznikají	vznikat	k5eAaImIp3nP	vznikat
svítící	svítící	k2eAgFnPc1d1	svítící
mlhoviny	mlhovina	k1gFnPc1	mlhovina
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
oblasti	oblast	k1gFnSc2	oblast
H	H	kA	H
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
mračna	mračno	k1gNnPc1	mračno
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
ramenech	rameno	k1gNnPc6	rameno
spirálních	spirální	k2eAgFnPc2d1	spirální
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
,	,	kIx,	,
v	v	k7c6	v
čočkových	čočkový	k2eAgFnPc6d1	čočková
a	a	k8xC	a
nepravidelných	pravidelný	k2eNgFnPc6d1	nepravidelná
galaxiích	galaxie	k1gFnPc6	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
je	být	k5eAaImIp3nS	být
proto	proto	k6eAd1	proto
tvorba	tvorba	k1gFnSc1	tvorba
hvězd	hvězda	k1gFnPc2	hvězda
nejčastější	častý	k2eAgFnSc1d3	nejčastější
<g/>
.	.	kIx.	.
</s>
<s>
Chladné	chladný	k2eAgNnSc1d1	chladné
<g/>
,	,	kIx,	,
prachoplynné	prachoplynný	k2eAgNnSc1d1	prachoplynný
mračno	mračno	k1gNnSc1	mračno
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
většinou	většina	k1gFnSc7	většina
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
nějakého	nějaký	k3yIgInSc2	nějaký
vnějšího	vnější	k2eAgInSc2d1	vnější
faktoru	faktor	k1gInSc2	faktor
(	(	kIx(	(
<g/>
výbuch	výbuch	k1gInSc1	výbuch
supernovy	supernova	k1gFnSc2	supernova
<g/>
,	,	kIx,	,
srážka	srážka	k1gFnSc1	srážka
s	s	k7c7	s
jiným	jiný	k2eAgNnSc7d1	jiné
mračnem	mračno	k1gNnSc7	mračno
<g/>
,	,	kIx,	,
srážka	srážka	k1gFnSc1	srážka
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
)	)	kIx)	)
smršťovat	smršťovat	k5eAaImF	smršťovat
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
oblast	oblast	k1gFnSc1	oblast
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
dostatečné	dostatečný	k2eAgFnPc4d1	dostatečná
hustoty	hustota	k1gFnPc4	hustota
hmoty	hmota	k1gFnSc2	hmota
a	a	k8xC	a
splní	splnit	k5eAaPmIp3nS	splnit
Jeansovo	Jeansův	k2eAgNnSc1d1	Jeansovo
kritérium	kritérium	k1gNnSc1	kritérium
nestability	nestabilita	k1gFnSc2	nestabilita
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
kolabovat	kolabovat	k5eAaImF	kolabovat
pod	pod	k7c7	pod
vlastní	vlastní	k2eAgFnSc7d1	vlastní
gravitací	gravitace	k1gFnSc7	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
kolapsu	kolaps	k1gInSc2	kolaps
mraku	mrak	k1gInSc2	mrak
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
shluky	shluk	k1gInPc1	shluk
hustšího	hustý	k2eAgInSc2d2	hustší
prachu	prach	k1gInSc2	prach
a	a	k8xC	a
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Bokovy	Bokovy	k?	Bokovy
globule	globule	k1gFnSc1	globule
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
kolapsu	kolaps	k1gInSc2	kolaps
globulí	globule	k1gFnPc2	globule
a	a	k8xC	a
růstu	růst	k1gInSc2	růst
hustoty	hustota	k1gFnSc2	hustota
se	se	k3xPyFc4	se
gravitační	gravitační	k2eAgFnSc1d1	gravitační
energie	energie	k1gFnSc1	energie
přeměňuje	přeměňovat	k5eAaImIp3nS	přeměňovat
na	na	k7c4	na
teplo	teplo	k1gNnSc4	teplo
a	a	k8xC	a
teplota	teplota	k1gFnSc1	teplota
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
nárůstem	nárůst	k1gInSc7	nárůst
teploty	teplota	k1gFnSc2	teplota
stoupá	stoupat	k5eAaImIp3nS	stoupat
také	také	k9	také
rychlost	rychlost	k1gFnSc1	rychlost
rotace	rotace	k1gFnSc2	rotace
mraku	mrak	k1gInSc2	mrak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mračnu	mračno	k1gNnSc6	mračno
se	se	k3xPyFc4	se
začínají	začínat	k5eAaImIp3nP	začínat
tvořit	tvořit	k5eAaImF	tvořit
hustší	hustý	k2eAgFnPc4d2	hustší
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
zárodky	zárodek	k1gInPc4	zárodek
samotných	samotný	k2eAgFnPc2d1	samotná
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
zárodky	zárodek	k1gInPc1	zárodek
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
až	až	k9	až
deset	deset	k4xCc4	deset
tisíc	tisíc	k4xCgInPc2	tisíc
slunečních	sluneční	k2eAgFnPc2d1	sluneční
hmotností	hmotnost	k1gFnPc2	hmotnost
dále	daleko	k6eAd2	daleko
kolabují	kolabovat	k5eAaImIp3nP	kolabovat
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
začíná	začínat	k5eAaImIp3nS	začínat
volnému	volný	k2eAgInSc3d1	volný
gravitačnímu	gravitační	k2eAgInSc3d1	gravitační
hroucení	hroucení	k1gNnPc2	hroucení
bránit	bránit	k5eAaImF	bránit
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
tlak	tlak	k1gInSc4	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mrak	mrak	k1gInSc1	mrak
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
zhruba	zhruba	k6eAd1	zhruba
stabilního	stabilní	k2eAgInSc2d1	stabilní
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
jádro	jádro	k1gNnSc1	jádro
tzv.	tzv.	kA	tzv.
protohvězdy	protohvězda	k1gFnSc2	protohvězda
<g/>
.	.	kIx.	.
</s>
<s>
Protohvězdy	Protohvězd	k1gInPc1	Protohvězd
jsou	být	k5eAaImIp3nP	být
bouřlivé	bouřlivý	k2eAgInPc1d1	bouřlivý
<g/>
,	,	kIx,	,
svítící	svítící	k2eAgInPc1d1	svítící
<g/>
,	,	kIx,	,
nestabilní	stabilní	k2eNgInPc1d1	nestabilní
objekty	objekt	k1gInPc1	objekt
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
nadále	nadále	k6eAd1	nadále
scvrkávají	scvrkávat	k5eAaImIp3nP	scvrkávat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
hvězdy	hvězda	k1gFnPc1	hvězda
před	před	k7c7	před
hlavní	hlavní	k2eAgFnSc7d1	hlavní
posloupností	posloupnost	k1gFnSc7	posloupnost
často	často	k6eAd1	často
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
disk	disk	k1gInSc4	disk
prachu	prach	k1gInSc2	prach
a	a	k8xC	a
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
protoplanetární	protoplanetární	k2eAgInSc1d1	protoplanetární
disk	disk	k1gInSc1	disk
<g/>
,	,	kIx,	,
a	a	k8xC	a
zdrojem	zdroj	k1gInSc7	zdroj
jejich	jejich	k3xOp3gNnSc2	jejich
vyzařování	vyzařování	k1gNnSc2	vyzařování
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
uvolňování	uvolňování	k1gNnSc1	uvolňování
gravitační	gravitační	k2eAgFnSc2d1	gravitační
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
gravitačního	gravitační	k2eAgInSc2d1	gravitační
kolapsu	kolaps	k1gInSc2	kolaps
trvá	trvat	k5eAaImIp3nS	trvat
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Mladé	mladý	k2eAgFnPc1d1	mladá
hvězdy	hvězda	k1gFnPc1	hvězda
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
méně	málo	k6eAd2	málo
než	než	k8xS	než
2	[number]	k4	2
M	M	kA	M
<g/>
⊙	⊙	k?	⊙
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
hvězdy	hvězda	k1gFnPc1	hvězda
T	T	kA	T
Tauri	Taur	k1gInPc7	Taur
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
hmotností	hmotnost	k1gFnSc7	hmotnost
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
Herbig	Herbig	k1gMnSc1	Herbig
Ae	Ae	k1gMnSc1	Ae
/	/	kIx~	/
Be	Be	k1gMnSc1	Be
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
mladé	mladý	k2eAgFnPc1d1	mladá
hvězdy	hvězda	k1gFnPc1	hvězda
vyzařují	vyzařovat	k5eAaImIp3nP	vyzařovat
podél	podél	k7c2	podél
své	svůj	k3xOyFgFnSc2	svůj
osy	osa	k1gFnSc2	osa
rotace	rotace	k1gFnSc2	rotace
proudy	proud	k1gInPc1	proud
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
snížit	snížit	k5eAaPmF	snížit
moment	moment	k1gInSc4	moment
hybnosti	hybnost	k1gFnSc2	hybnost
vznikající	vznikající	k2eAgFnSc2d1	vznikající
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
malých	malý	k2eAgFnPc2d1	malá
mlhovinovitých	mlhovinovitý	k2eAgFnPc2d1	mlhovinovitý
oblastí	oblast	k1gFnPc2	oblast
známých	známý	k2eAgFnPc2d1	známá
jako	jako	k8xC	jako
Herbigovy	Herbigův	k2eAgInPc1d1	Herbigův
<g/>
-	-	kIx~	-
<g/>
Harovy	Harův	k2eAgInPc1d1	Harův
objekty	objekt	k1gInPc1	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
proudy	proud	k1gInPc1	proud
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
se	s	k7c7	s
zářením	záření	k1gNnSc7	záření
blízkých	blízký	k2eAgFnPc2d1	blízká
masivních	masivní	k2eAgFnPc2d1	masivní
hvězd	hvězda	k1gFnPc2	hvězda
mohou	moct	k5eAaImIp3nP	moct
rozehnat	rozehnat	k5eAaPmF	rozehnat
okolní	okolní	k2eAgInSc4d1	okolní
mrak	mrak	k1gInSc4	mrak
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
hvězda	hvězda	k1gFnSc1	hvězda
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
protohvězdy	protohvězda	k1gFnSc2	protohvězda
vzrostou	vzrůst	k5eAaPmIp3nP	vzrůst
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zapálí	zapálit	k5eAaPmIp3nP	zapálit
termojaderné	termojaderný	k2eAgFnPc1d1	termojaderná
reakce	reakce	k1gFnPc1	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Gravitační	gravitační	k2eAgFnSc1d1	gravitační
síla	síla	k1gFnSc1	síla
se	se	k3xPyFc4	se
vyrovná	vyrovnat	k5eAaBmIp3nS	vyrovnat
s	s	k7c7	s
tlakem	tlak	k1gInSc7	tlak
záření	záření	k1gNnSc2	záření
přicházejícího	přicházející	k2eAgNnSc2d1	přicházející
z	z	k7c2	z
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
hvězda	hvězda	k1gFnSc1	hvězda
se	se	k3xPyFc4	se
přestane	přestat	k5eAaPmIp3nS	přestat
dále	daleko	k6eAd2	daleko
zmenšovat	zmenšovat	k5eAaImF	zmenšovat
a	a	k8xC	a
usadí	usadit	k5eAaPmIp3nP	usadit
se	se	k3xPyFc4	se
na	na	k7c6	na
hlavní	hlavní	k2eAgFnSc6d1	hlavní
posloupnosti	posloupnost	k1gFnSc6	posloupnost
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stráví	strávit	k5eAaPmIp3nS	strávit
až	až	k9	až
90	[number]	k4	90
%	%	kIx~	%
svého	své	k1gNnSc2	své
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vznikaly	vznikat	k5eAaImAgFnP	vznikat
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
dost	dost	k6eAd1	dost
odlišné	odlišný	k2eAgInPc1d1	odlišný
od	od	k7c2	od
současných	současný	k2eAgInPc2d1	současný
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
nesmírně	smírně	k6eNd1	smírně
hmotné	hmotný	k2eAgInPc4d1	hmotný
a	a	k8xC	a
zářivé	zářivý	k2eAgInPc4d1	zářivý
objekty	objekt	k1gInPc4	objekt
s	s	k7c7	s
hmotnostmi	hmotnost	k1gFnPc7	hmotnost
nejméně	málo	k6eAd3	málo
15	[number]	k4	15
hmotností	hmotnost	k1gFnSc7	hmotnost
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
hvězdy	hvězda	k1gFnPc4	hvězda
také	také	k6eAd1	také
neobsahovaly	obsahovat	k5eNaImAgInP	obsahovat
prvky	prvek	k1gInPc1	prvek
těžší	těžký	k2eAgNnSc4d2	těžší
než	než	k8xS	než
helium	helium	k1gNnSc4	helium
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tyto	tento	k3xDgInPc1	tento
prvky	prvek	k1gInPc1	prvek
ještě	ještě	k6eAd1	ještě
neexistovaly	existovat	k5eNaImAgInP	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
životnost	životnost	k1gFnSc1	životnost
však	však	k9	však
byla	být	k5eAaImAgFnS	být
krátká	krátká	k1gFnSc1	krátká
<g/>
,	,	kIx,	,
necelý	celý	k2eNgInSc4d1	necelý
milion	milion	k4xCgInSc4	milion
let	let	k1gInSc4	let
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vývoj	vývoj	k1gInSc1	vývoj
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
života	život	k1gInSc2	život
hvězdy	hvězda	k1gFnSc2	hvězda
T	T	kA	T
Tauri	Taur	k1gMnPc1	Taur
následují	následovat	k5eAaImIp3nP	následovat
Hajašiho	Hajaši	k1gMnSc4	Hajaši
stopu	stop	k1gInSc2	stop
-	-	kIx~	-
zmenšují	zmenšovat	k5eAaImIp3nP	zmenšovat
se	se	k3xPyFc4	se
a	a	k8xC	a
klesá	klesat	k5eAaImIp3nS	klesat
jejich	jejich	k3xOp3gFnSc1	jejich
svítivost	svítivost	k1gFnSc1	svítivost
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
zhruba	zhruba	k6eAd1	zhruba
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
.	.	kIx.	.
</s>
<s>
Lehčí	lehčit	k5eAaImIp3nP	lehčit
T	T	kA	T
Tauri	Taure	k1gFnSc4	Taure
hvězdy	hvězda	k1gFnSc2	hvězda
přecházejí	přecházet	k5eAaImIp3nP	přecházet
do	do	k7c2	do
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
těžké	těžký	k2eAgFnPc1d1	těžká
hvězdy	hvězda	k1gFnPc1	hvězda
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
Henyeyho	Henyey	k1gMnSc4	Henyey
stopou	stopa	k1gFnSc7	stopa
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
dlouho	dlouho	k6eAd1	dlouho
hvězda	hvězda	k1gFnSc1	hvězda
setrvá	setrvat	k5eAaPmIp3nS	setrvat
v	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
stabilní	stabilní	k2eAgFnSc6d1	stabilní
fázi	fáze	k1gFnSc6	fáze
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
,	,	kIx,	,
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
počáteční	počáteční	k2eAgFnSc6d1	počáteční
hmotnosti	hmotnost	k1gFnSc6	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnější	hmotný	k2eAgFnPc1d2	hmotnější
hvězdy	hvězda	k1gFnPc1	hvězda
paradoxně	paradoxně	k6eAd1	paradoxně
žijí	žít	k5eAaImIp3nP	žít
kratčeji	krátce	k6eAd2	krátce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
termojaderné	termojaderný	k2eAgFnSc2d1	termojaderná
reakce	reakce	k1gFnSc2	reakce
v	v	k7c6	v
jejich	jejich	k3xOp3gNnPc6	jejich
jádrech	jádro	k1gNnPc6	jádro
probíhají	probíhat	k5eAaImIp3nP	probíhat
mnohem	mnohem	k6eAd1	mnohem
bouřlivěji	bouřlivě	k6eAd2	bouřlivě
než	než	k8xS	než
v	v	k7c6	v
málo	málo	k6eAd1	málo
hmotných	hmotný	k2eAgFnPc6d1	hmotná
hvězdách	hvězda	k1gFnPc6	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
hvězdy	hvězda	k1gFnSc2	hvězda
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
Slunce	slunce	k1gNnSc2	slunce
trvá	trvat	k5eAaImIp3nS	trvat
celé	celý	k2eAgFnSc2d1	celá
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
mnohem	mnohem	k6eAd1	mnohem
hmotnějších	hmotný	k2eAgMnPc2d2	hmotnější
obrů	obr	k1gMnPc2	obr
a	a	k8xC	a
nadobrů	nadobr	k1gMnPc2	nadobr
jen	jen	k9	jen
miliony	milion	k4xCgInPc4	milion
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
jen	jen	k6eAd1	jen
statisíce	statisíce	k1gInPc4	statisíce
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
podíl	podíl	k1gInSc1	podíl
hélia	hélium	k1gNnSc2	hélium
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc1	rychlost
jaderné	jaderný	k2eAgFnSc2d1	jaderná
fúze	fúze	k1gFnSc2	fúze
pomalu	pomalu	k6eAd1	pomalu
narůstá	narůstat	k5eAaImIp3nS	narůstat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
teplotou	teplota	k1gFnSc7	teplota
a	a	k8xC	a
svítivostí	svítivost	k1gFnSc7	svítivost
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Slunce	slunce	k1gNnSc1	slunce
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
hlavní	hlavní	k2eAgFnSc4d1	hlavní
posloupnost	posloupnost	k1gFnSc4	posloupnost
před	před	k7c7	před
4,6	[number]	k4	4,6
miliardami	miliarda	k4xCgFnPc7	miliarda
(	(	kIx(	(
<g/>
4.6	[number]	k4	4.6
<g/>
×	×	k?	×
<g/>
109	[number]	k4	109
<g/>
)	)	kIx)	)
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
svítivost	svítivost	k1gFnSc1	svítivost
do	do	k7c2	do
dnešní	dnešní	k2eAgFnSc2d1	dnešní
doby	doba	k1gFnSc2	doba
podle	podle	k7c2	podle
výpočtů	výpočet	k1gInPc2	výpočet
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
o	o	k7c4	o
40	[number]	k4	40
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
většinu	většina	k1gFnSc4	většina
hvězd	hvězda	k1gFnPc2	hvězda
je	být	k5eAaImIp3nS	být
množství	množství	k1gNnSc1	množství
hmoty	hmota	k1gFnSc2	hmota
ztracené	ztracený	k2eAgFnSc6d1	ztracená
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
hvězdného	hvězdný	k2eAgInSc2d1	hvězdný
větru	vítr	k1gInSc2	vítr
zanedbatelné	zanedbatelný	k2eAgNnSc1d1	zanedbatelné
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
hmotnosti	hmotnost	k1gFnSc3	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
ztratí	ztratit	k5eAaPmIp3nS	ztratit
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
hmotností	hmotnost	k1gFnSc7	hmotnost
Slunce	slunce	k1gNnSc2	slunce
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
nebo	nebo	k8xC	nebo
0,01	[number]	k4	0,01
%	%	kIx~	%
své	svůj	k3xOyFgFnSc2	svůj
celkové	celkový	k2eAgFnSc2d1	celková
hmotnosti	hmotnost	k1gFnSc2	hmotnost
během	během	k7c2	během
celého	celý	k2eAgInSc2d1	celý
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
těžké	těžký	k2eAgFnPc1d1	těžká
hvězdy	hvězda	k1gFnPc1	hvězda
však	však	k9	však
mohou	moct	k5eAaImIp3nP	moct
ztratit	ztratit	k5eAaPmF	ztratit
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
až	až	k9	až
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
hmotností	hmotnost	k1gFnSc7	hmotnost
Slunce	slunce	k1gNnSc2	slunce
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
výrazně	výrazně	k6eAd1	výrazně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
jejich	jejich	k3xOp3gInSc4	jejich
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
M	M	kA	M
<g/>
⊙	⊙	k?	⊙
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
během	během	k7c2	během
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
ztratit	ztratit	k5eAaPmF	ztratit
až	až	k9	až
polovinu	polovina	k1gFnSc4	polovina
celkové	celkový	k2eAgFnSc2d1	celková
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
hvězda	hvězda	k1gFnSc1	hvězda
stráví	strávit	k5eAaPmIp3nS	strávit
na	na	k7c4	na
hlavní	hlavní	k2eAgFnPc4d1	hlavní
posloupnosti	posloupnost	k1gFnPc4	posloupnost
<g/>
,	,	kIx,	,
závisí	záviset	k5eAaImIp3nS	záviset
především	především	k9	především
na	na	k7c4	na
množství	množství	k1gNnSc4	množství
paliva	palivo	k1gNnSc2	palivo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
hvězda	hvězda	k1gFnSc1	hvězda
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
a	a	k8xC	a
rychlosti	rychlost	k1gFnSc3	rychlost
fúze	fúze	k1gFnSc2	fúze
<g/>
,	,	kIx,	,
spalování	spalování	k1gNnSc1	spalování
toho	ten	k3xDgNnSc2	ten
paliva	palivo	k1gNnSc2	palivo
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
původní	původní	k2eAgFnSc2d1	původní
hmotnosti	hmotnost	k1gFnSc2	hmotnost
a	a	k8xC	a
svítivosti	svítivost	k1gFnSc2	svítivost
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Odhadovaná	odhadovaný	k2eAgFnSc1d1	odhadovaná
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
Slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
miliard	miliarda	k4xCgFnPc2	miliarda
(	(	kIx(	(
<g/>
1010	[number]	k4	1010
<g/>
)	)	kIx)	)
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Těžké	těžký	k2eAgFnPc1d1	těžká
hvězdy	hvězda	k1gFnPc1	hvězda
spotřebovávají	spotřebovávat	k5eAaImIp3nP	spotřebovávat
palivo	palivo	k1gNnSc4	palivo
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
krátký	krátký	k2eAgInSc1d1	krátký
<g/>
.	.	kIx.	.
</s>
<s>
Lehké	Lehké	k2eAgFnPc1d1	Lehké
hvězdy	hvězda	k1gFnPc1	hvězda
naopak	naopak	k6eAd1	naopak
utrácejí	utrácet	k5eAaImIp3nP	utrácet
palivo	palivo	k1gNnSc4	palivo
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
lehčí	lehký	k2eAgFnSc2d2	lehčí
než	než	k8xS	než
0,25	[number]	k4	0,25
M	M	kA	M
<g/>
⊙	⊙	k?	⊙
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
červení	červenit	k5eAaImIp3nP	červenit
trpaslíci	trpaslík	k1gMnPc1	trpaslík
<g/>
,	,	kIx,	,
dokáží	dokázat	k5eAaPmIp3nP	dokázat
na	na	k7c4	na
fúzi	fúze	k1gFnSc4	fúze
využít	využít	k5eAaPmF	využít
téměř	téměř	k6eAd1	téměř
veškerou	veškerý	k3xTgFnSc4	veškerý
svou	svůj	k3xOyFgFnSc4	svůj
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
hvězdy	hvězda	k1gFnPc1	hvězda
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
~	~	kIx~	~
1	[number]	k4	1
M	M	kA	M
<g/>
⊙	⊙	k?	⊙
využijí	využít	k5eAaPmIp3nP	využít
jako	jako	k9	jako
palivo	palivo	k1gNnSc4	palivo
pouze	pouze	k6eAd1	pouze
10	[number]	k4	10
%	%	kIx~	%
své	svůj	k3xOyFgFnSc2	svůj
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Kombinace	kombinace	k1gFnSc1	kombinace
nízké	nízký	k2eAgFnSc2d1	nízká
spotřeby	spotřeba	k1gFnSc2	spotřeba
a	a	k8xC	a
relativně	relativně	k6eAd1	relativně
velkých	velký	k2eAgFnPc2d1	velká
použitelných	použitelný	k2eAgFnPc2d1	použitelná
zásob	zásoba	k1gFnPc2	zásoba
paliva	palivo	k1gNnSc2	palivo
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
podle	podle	k7c2	podle
výpočtů	výpočet	k1gInPc2	výpočet
hvězdám	hvězda	k1gFnPc3	hvězda
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
~	~	kIx~	~
0,25	[number]	k4	0,25
M	M	kA	M
<g/>
⊙	⊙	k?	⊙
existovat	existovat	k5eAaImF	existovat
zhruba	zhruba	k6eAd1	zhruba
bilion	bilion	k4xCgInSc4	bilion
(	(	kIx(	(
<g/>
1012	[number]	k4	1012
<g/>
,	,	kIx,	,
)	)	kIx)	)
let	léto	k1gNnPc2	léto
a	a	k8xC	a
nejlehčím	lehký	k2eAgFnPc3d3	nejlehčí
hvězdám	hvězda	k1gFnPc3	hvězda
spalujícím	spalující	k2eAgFnPc3d1	spalující
vodík	vodík	k1gInSc4	vodík
(	(	kIx(	(
<g/>
0,08	[number]	k4	0,08
M	M	kA	M
<g/>
⊙	⊙	k?	⊙
<g/>
)	)	kIx)	)
dokonce	dokonce	k9	dokonce
12	[number]	k4	12
bilionů	bilion	k4xCgInPc2	bilion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
protože	protože	k8xS	protože
životnost	životnost	k1gFnSc1	životnost
takových	takový	k3xDgFnPc2	takový
hvězd	hvězda	k1gFnPc2	hvězda
je	být	k5eAaImIp3nS	být
delší	dlouhý	k2eAgInSc4d2	delší
než	než	k8xS	než
současný	současný	k2eAgInSc4d1	současný
odhadovaný	odhadovaný	k2eAgInSc4d1	odhadovaný
věk	věk	k1gInSc4	věk
vesmíru	vesmír	k1gInSc2	vesmír
(	(	kIx(	(
<g/>
13,8	[number]	k4	13,8
miliardy	miliarda	k4xCgFnSc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ještě	ještě	k9	ještě
žádné	žádný	k3yNgFnPc4	žádný
hvězdy	hvězda	k1gFnPc4	hvězda
lehčí	lehký	k2eAgMnSc1d2	lehčí
než	než	k8xS	než
0,85	[number]	k4	0,85
M	M	kA	M
<g/>
⊙	⊙	k?	⊙
neopustily	opustit	k5eNaPmAgInP	opustit
hlavní	hlavní	k2eAgFnSc4d1	hlavní
posloupnost	posloupnost	k1gFnSc4	posloupnost
<g/>
.	.	kIx.	.
</s>
<s>
Prvky	prvek	k1gInPc4	prvek
těžší	těžký	k2eAgInPc4d2	těžší
než	než	k8xS	než
hélium	hélium	k1gNnSc4	hélium
hrají	hrát	k5eAaImIp3nP	hrát
vedle	vedle	k7c2	vedle
hmotnosti	hmotnost	k1gFnSc2	hmotnost
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
astronomii	astronomie	k1gFnSc6	astronomie
se	se	k3xPyFc4	se
všechny	všechen	k3xTgInPc1	všechen
prvky	prvek	k1gInPc1	prvek
těžší	těžký	k2eAgFnSc2d2	těžší
než	než	k8xS	než
helium	helium	k1gNnSc4	helium
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
kovy	kov	k1gInPc4	kov
(	(	kIx(	(
<g/>
ang	ang	k?	ang
<g/>
.	.	kIx.	.
metal	metal	k1gInSc1	metal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
koncentrace	koncentrace	k1gFnSc1	koncentrace
těchto	tento	k3xDgInPc2	tento
prvků	prvek	k1gInPc2	prvek
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
metalicita	metalicita	k1gFnSc1	metalicita
<g/>
.	.	kIx.	.
</s>
<s>
Metalicita	Metalicita	k1gFnSc1	Metalicita
může	moct	k5eAaImIp3nS	moct
ovlivnit	ovlivnit	k5eAaPmF	ovlivnit
délku	délka	k1gFnSc4	délka
spalování	spalování	k1gNnSc2	spalování
paliva	palivo	k1gNnSc2	palivo
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
vznik	vznik	k1gInSc4	vznik
magnetických	magnetický	k2eAgFnPc2d1	magnetická
polí	pole	k1gFnPc2	pole
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
i	i	k9	i
na	na	k7c4	na
intenzitu	intenzita	k1gFnSc4	intenzita
hvězdného	hvězdný	k2eAgInSc2d1	hvězdný
větru	vítr	k1gInSc2	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starší	k1gMnSc1	starší
hvězdy	hvězda	k1gFnSc2	hvězda
populace	populace	k1gFnSc2	populace
II	II	kA	II
mají	mít	k5eAaImIp3nP	mít
kvůli	kvůli	k7c3	kvůli
složení	složení	k1gNnSc3	složení
molekulárního	molekulární	k2eAgNnSc2d1	molekulární
mračna	mračno	k1gNnSc2	mračno
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
<g/>
,	,	kIx,	,
podstatně	podstatně	k6eAd1	podstatně
nižší	nízký	k2eAgFnSc4d2	nižší
metalicitu	metalicita	k1gFnSc4	metalicita
než	než	k8xS	než
mladší	mladý	k2eAgFnPc4d2	mladší
hvězdy	hvězda	k1gFnPc4	hvězda
populace	populace	k1gFnSc2	populace
I.	I.	kA	I.
Postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
obsah	obsah	k1gInSc1	obsah
těžších	těžký	k2eAgInPc2d2	těžší
prvků	prvek	k1gInPc2	prvek
v	v	k7c6	v
mezihvězdných	mezihvězdný	k2eAgNnPc6d1	mezihvězdné
mračnech	mračno	k1gNnPc6	mračno
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
umírající	umírající	k2eAgFnSc2d1	umírající
hvězdy	hvězda	k1gFnSc2	hvězda
rozptýlí	rozptýlit	k5eAaPmIp3nS	rozptýlit
tyto	tento	k3xDgInPc4	tento
prvky	prvek	k1gInPc4	prvek
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Nejpočetnější	početní	k2eAgFnPc1d3	nejpočetnější
hvězdy	hvězda	k1gFnPc1	hvězda
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
<g/>
,	,	kIx,	,
červení	červenit	k5eAaImIp3nP	červenit
trpaslíci	trpaslík	k1gMnPc1	trpaslík
<g/>
,	,	kIx,	,
zanikají	zanikat	k5eAaImIp3nP	zanikat
nenápadně	nápadně	k6eNd1	nápadně
-	-	kIx~	-
po	po	k7c6	po
vyhoření	vyhoření	k1gNnSc6	vyhoření
veškerého	veškerý	k3xTgNnSc2	veškerý
paliva	palivo	k1gNnSc2	palivo
pozvolna	pozvolna	k6eAd1	pozvolna
chladnou	chladnout	k5eAaImIp3nP	chladnout
až	až	k9	až
nakonec	nakonec	k6eAd1	nakonec
úplně	úplně	k6eAd1	úplně
zhasnou	zhasnout	k5eAaPmIp3nP	zhasnout
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
alespoň	alespoň	k9	alespoň
0,4	[number]	k4	0,4
M	M	kA	M
<g/>
⊙	⊙	k?	⊙
se	se	k3xPyFc4	se
po	po	k7c4	po
vyčerpání	vyčerpání	k1gNnSc4	vyčerpání
zásob	zásoba	k1gFnPc2	zásoba
vodíku	vodík	k1gInSc2	vodík
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
nafouknou	nafouknout	k5eAaPmIp3nP	nafouknout
a	a	k8xC	a
ochladí	ochladit	k5eAaPmIp3nP	ochladit
(	(	kIx(	(
<g/>
z	z	k7c2	z
hvězdy	hvězda	k1gFnSc2	hvězda
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
tepla	tepnout	k5eAaPmAgFnS	tepnout
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
čtverečního	čtvereční	k2eAgInSc2d1	čtvereční
metru	metr	k1gInSc2	metr
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
zvětšeného	zvětšený	k2eAgNnSc2d1	zvětšené
nafouknutím	nafouknutí	k1gNnSc7	nafouknutí
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
-	-	kIx~	-
povrch	povrch	k6eAd1wR	povrch
tedy	tedy	k9	tedy
chladne	chladnout	k5eAaImIp3nS	chladnout
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
jádro	jádro	k1gNnSc1	jádro
se	se	k3xPyFc4	se
zahřálo	zahřát	k5eAaPmAgNnS	zahřát
a	a	k8xC	a
svítivost	svítivost	k1gFnSc4	svítivost
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
červený	červený	k2eAgMnSc1d1	červený
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
za	za	k7c4	za
5	[number]	k4	5
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Slunce	slunce	k1gNnSc1	slunce
vstoupí	vstoupit	k5eAaPmIp3nS	vstoupit
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
fáze	fáze	k1gFnSc2	fáze
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gInSc1	jeho
poloměr	poloměr	k1gInSc1	poloměr
zvětší	zvětšit	k5eAaPmIp3nS	zvětšit
na	na	k7c4	na
zhruba	zhruba	k6eAd1	zhruba
1	[number]	k4	1
AU	au	k0	au
(	(	kIx(	(
<g/>
150	[number]	k4	150
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
představuje	představovat	k5eAaImIp3nS	představovat
250	[number]	k4	250
<g/>
násobný	násobný	k2eAgInSc1d1	násobný
nárůst	nárůst	k1gInSc1	nárůst
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
obra	obr	k1gMnSc2	obr
ztratí	ztratit	k5eAaPmIp3nS	ztratit
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
%	%	kIx~	%
své	svůj	k3xOyFgFnSc2	svůj
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červených	červený	k2eAgMnPc6d1	červený
obrech	obr	k1gMnPc6	obr
do	do	k7c2	do
2,25	[number]	k4	2,25
M	M	kA	M
<g/>
⊙	⊙	k?	⊙
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
spalování	spalování	k1gNnSc1	spalování
vodíku	vodík	k1gInSc2	vodík
ve	v	k7c6	v
vrstvě	vrstva	k1gFnSc6	vrstva
obklopující	obklopující	k2eAgNnSc4d1	obklopující
jádro	jádro	k1gNnSc4	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
vzroste	vzrůst	k5eAaPmIp3nS	vzrůst
teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
začne	začít	k5eAaPmIp3nS	začít
fúze	fúze	k1gFnSc1	fúze
hélia	hélium	k1gNnSc2	hélium
<g/>
.	.	kIx.	.
</s>
<s>
Průměr	průměr	k1gInSc1	průměr
hvězdy	hvězda	k1gFnSc2	hvězda
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
postupně	postupně	k6eAd1	postupně
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
a	a	k8xC	a
povrchová	povrchový	k2eAgFnSc1d1	povrchová
teplota	teplota	k1gFnSc1	teplota
stoupá	stoupat	k5eAaImIp3nS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
větších	veliký	k2eAgNnPc2d2	veliký
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
hmotnějších	hmotný	k2eAgFnPc2d2	hmotnější
hvězd	hvězda	k1gFnPc2	hvězda
jádro	jádro	k1gNnSc1	jádro
přejde	přejít	k5eAaPmIp3nS	přejít
přímo	přímo	k6eAd1	přímo
ze	z	k7c2	z
spalování	spalování	k1gNnSc2	spalování
vodíku	vodík	k1gInSc2	vodík
na	na	k7c6	na
spalování	spalování	k1gNnSc6	spalování
hélia	hélium	k1gNnSc2	hélium
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
spotřebování	spotřebování	k1gNnSc6	spotřebování
hélia	hélium	k1gNnSc2	hélium
v	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
fúze	fúze	k1gFnSc1	fúze
ve	v	k7c6	v
vrstvě	vrstva	k1gFnSc6	vrstva
z	z	k7c2	z
uhlíku	uhlík	k1gInSc2	uhlík
a	a	k8xC	a
kyslíku	kyslík	k1gInSc2	kyslík
kolem	kolem	k7c2	kolem
horkého	horký	k2eAgNnSc2d1	horké
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	Hvězda	k1gMnSc1	Hvězda
pak	pak	k6eAd1	pak
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
cestou	cesta	k1gFnSc7	cesta
podobnou	podobný	k2eAgFnSc7d1	podobná
s	s	k7c7	s
původní	původní	k2eAgFnSc7d1	původní
fází	fáze	k1gFnSc7	fáze
červeného	červený	k2eAgMnSc4d1	červený
obra	obr	k1gMnSc4	obr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
povrchovou	povrchový	k2eAgFnSc7d1	povrchová
teplotou	teplota	k1gFnSc7	teplota
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
postupným	postupný	k2eAgNnSc7d1	postupné
zmenšováním	zmenšování	k1gNnSc7	zmenšování
hvězdného	hvězdný	k2eAgNnSc2d1	Hvězdné
jádra	jádro	k1gNnSc2	jádro
narůstá	narůstat	k5eAaImIp3nS	narůstat
intenzita	intenzita	k1gFnSc1	intenzita
záření	záření	k1gNnSc2	záření
z	z	k7c2	z
jeho	jeho	k3xOp3gInSc2	jeho
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
takový	takový	k3xDgInSc4	takový
tlak	tlak	k1gInSc4	tlak
záření	záření	k1gNnSc2	záření
na	na	k7c4	na
vnější	vnější	k2eAgFnPc4d1	vnější
vrstvy	vrstva	k1gFnPc4	vrstva
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	on	k3xPp3gMnPc4	on
doslova	doslova	k6eAd1	doslova
odhodí	odhodit	k5eAaPmIp3nP	odhodit
a	a	k8xC	a
vytvoří	vytvořit	k5eAaPmIp3nP	vytvořit
planetární	planetární	k2eAgFnSc4d1	planetární
mlhovinu	mlhovina	k1gFnSc4	mlhovina
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
jádro	jádro	k1gNnSc1	jádro
po	po	k7c6	po
odmrštění	odmrštění	k1gNnSc6	odmrštění
vnější	vnější	k2eAgFnSc2d1	vnější
atmosféry	atmosféra	k1gFnSc2	atmosféra
hmotnost	hmotnost	k1gFnSc4	hmotnost
menší	malý	k2eAgFnSc4d2	menší
než	než	k8xS	než
1,4	[number]	k4	1,4
M	M	kA	M
<g/>
⊙	⊙	k?	⊙
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
smrští	smrštit	k5eAaPmIp3nS	smrštit
na	na	k7c4	na
poměrně	poměrně	k6eAd1	poměrně
malý	malý	k2eAgInSc4d1	malý
objekt	objekt	k1gInSc4	objekt
velký	velký	k2eAgInSc4d1	velký
přibližně	přibližně	k6eAd1	přibližně
jako	jako	k9	jako
Země	zem	k1gFnSc2	zem
-	-	kIx~	-
bílý	bílý	k1gMnSc1	bílý
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
těžký	těžký	k2eAgInSc1d1	těžký
pro	pro	k7c4	pro
další	další	k2eAgNnSc4d1	další
stlačování	stlačování	k1gNnSc4	stlačování
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
žhavý	žhavý	k2eAgMnSc1d1	žhavý
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
chladnout	chladnout	k5eAaImF	chladnout
více	hodně	k6eAd2	hodně
než	než	k8xS	než
13	[number]	k4	13
miliard	miliarda	k4xCgFnPc2	miliarda
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejstarší	starý	k2eAgFnPc1d3	nejstarší
takto	takto	k6eAd1	takto
zkolabované	zkolabovaný	k2eAgFnPc1d1	zkolabovaná
hvězdy	hvězda	k1gFnPc1	hvězda
ještě	ještě	k9	ještě
zdaleka	zdaleka	k6eAd1	zdaleka
nevychladly	vychladnout	k5eNaPmAgFnP	vychladnout
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
hmotnost	hmotnost	k1gFnSc4	hmotnost
od	od	k7c2	od
0,1	[number]	k4	0,1
až	až	k9	až
1,4	[number]	k4	1,4
Sluncí	slunce	k1gNnPc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
cm	cm	kA	cm
<g/>
3	[number]	k4	3
má	mít	k5eAaImIp3nS	mít
tak	tak	k9	tak
hmotnost	hmotnost	k1gFnSc1	hmotnost
cca	cca	kA	cca
1	[number]	k4	1
tunu	tuna	k1gFnSc4	tuna
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
miliónkrát	miliónkrát	k6eAd1	miliónkrát
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
hustota	hustota	k1gFnSc1	hustota
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgMnSc1d1	bílý
trpaslík	trpaslík	k1gMnSc1	trpaslík
postupně	postupně	k6eAd1	postupně
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
<g/>
,	,	kIx,	,
vybledne	vyblednout	k5eAaPmIp3nS	vyblednout
na	na	k7c4	na
černého	černý	k2eAgMnSc4d1	černý
trpaslíka	trpaslík	k1gMnSc4	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
těžké	těžký	k2eAgFnPc1d1	těžká
hvězdy	hvězda	k1gFnPc1	hvězda
s	s	k7c7	s
hmotností	hmotnost	k1gFnSc7	hmotnost
více	hodně	k6eAd2	hodně
než	než	k8xS	než
9	[number]	k4	9
M	M	kA	M
<g/>
⊙	⊙	k?	⊙
během	během	k7c2	během
fáze	fáze	k1gFnSc2	fáze
hoření	hoření	k2eAgNnPc1d1	hoření
hélia	hélium	k1gNnPc1	hélium
expandují	expandovat	k5eAaImIp3nP	expandovat
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
červeného	červený	k2eAgMnSc4d1	červený
veleobra	veleobr	k1gMnSc4	veleobr
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
vyčerpají	vyčerpat	k5eAaPmIp3nP	vyčerpat
palivo	palivo	k1gNnSc4	palivo
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
<g/>
,	,	kIx,	,
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
ve	v	k7c6	v
spalování	spalování	k1gNnSc6	spalování
těžších	těžký	k2eAgInPc2d2	těžší
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
se	se	k3xPyFc4	se
zmenšuje	zmenšovat	k5eAaImIp3nS	zmenšovat
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
teplota	teplota	k1gFnSc1	teplota
a	a	k8xC	a
tlak	tlak	k1gInSc1	tlak
nejsou	být	k5eNaImIp3nP	být
dostatečné	dostatečný	k2eAgFnPc1d1	dostatečná
k	k	k7c3	k
fúzi	fúze	k1gFnSc3	fúze
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
dalšími	další	k2eAgFnPc7d1	další
fázemi	fáze	k1gFnPc7	fáze
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgInPc6	který
je	být	k5eAaImIp3nS	být
palivem	palivo	k1gNnSc7	palivo
nejprve	nejprve	k6eAd1	nejprve
neon	neon	k1gInSc4	neon
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
kyslík	kyslík	k1gInSc1	kyslík
a	a	k8xC	a
křemík	křemík	k1gInSc1	křemík
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
koncem	konec	k1gInSc7	konec
života	život	k1gInSc2	život
fúze	fúze	k1gFnSc2	fúze
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
v	v	k7c6	v
sérii	série	k1gFnSc6	série
vrstev	vrstva	k1gFnPc2	vrstva
podobných	podobný	k2eAgFnPc2d1	podobná
cibuli	cibule	k1gFnSc4	cibule
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
vrstva	vrstva	k1gFnSc1	vrstva
spaluje	spalovat	k5eAaImIp3nS	spalovat
jiný	jiný	k2eAgInSc4d1	jiný
prvek	prvek	k1gInSc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
fáze	fáze	k1gFnSc1	fáze
nastává	nastávat	k5eAaImIp3nS	nastávat
<g/>
,	,	kIx,	,
když	když	k8xS	když
hvězda	hvězda	k1gFnSc1	hvězda
začne	začít	k5eAaPmIp3nS	začít
produkovat	produkovat	k5eAaImF	produkovat
železo	železo	k1gNnSc4	železo
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
jádro	jádro	k1gNnSc1	jádro
železa	železo	k1gNnSc2	železo
je	být	k5eAaImIp3nS	být
vázáno	vázat	k5eAaImNgNnS	vázat
pevněji	pevně	k6eAd2	pevně
než	než	k8xS	než
jiná	jiný	k2eAgNnPc1d1	jiné
<g/>
,	,	kIx,	,
těžší	těžký	k2eAgNnPc1d2	těžší
jádra	jádro	k1gNnPc1	jádro
<g/>
,	,	kIx,	,
fúze	fúze	k1gFnSc1	fúze
železa	železo	k1gNnSc2	železo
nevytváří	vytvářit	k5eNaPmIp3nS	vytvářit
žádnou	žádný	k3yNgFnSc4	žádný
energii	energie	k1gFnSc4	energie
a	a	k8xC	a
proces	proces	k1gInSc4	proces
naopak	naopak	k6eAd1	naopak
energii	energie	k1gFnSc4	energie
spotřebovává	spotřebovávat	k5eAaImIp3nS	spotřebovávat
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
stejného	stejný	k2eAgInSc2d1	stejný
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
energie	energie	k1gFnSc1	energie
nedá	dát	k5eNaPmIp3nS	dát
získat	získat	k5eAaPmF	získat
ani	ani	k9	ani
štěpením	štěpení	k1gNnSc7	štěpení
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
relativně	relativně	k6eAd1	relativně
starých	starý	k2eAgFnPc6d1	stará
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
těžkých	těžký	k2eAgFnPc6d1	těžká
hvězdách	hvězda	k1gFnPc6	hvězda
se	se	k3xPyFc4	se
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
naakumuluje	naakumulovat	k5eAaPmIp3nS	naakumulovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
nereaktivního	reaktivní	k2eNgNnSc2d1	nereaktivní
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Těžké	těžký	k2eAgInPc1d1	těžký
prvky	prvek	k1gInPc1	prvek
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
hvězdách	hvězda	k1gFnPc6	hvězda
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
dostat	dostat	k5eAaPmF	dostat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
objekt	objekt	k1gInSc1	objekt
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xS	jako
Wolfova-Rayetova	Wolfova-Rayetův	k2eAgFnSc1d1	Wolfova-Rayetův
hvězda	hvězda	k1gFnSc1	hvězda
s	s	k7c7	s
hustým	hustý	k2eAgInSc7d1	hustý
hvězdným	hvězdný	k2eAgInSc7d1	hvězdný
větrem	vítr	k1gInSc7	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
železné	železný	k2eAgNnSc1d1	železné
jádro	jádro	k1gNnSc1	jádro
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
hmotnosti	hmotnost	k1gFnSc3	hmotnost
>	>	kIx)	>
1,4	[number]	k4	1,4
M	M	kA	M
<g/>
⊙	⊙	k?	⊙
<g/>
,	,	kIx,	,
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
již	již	k6eAd1	již
více	hodně	k6eAd2	hodně
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
gravitaci	gravitace	k1gFnSc4	gravitace
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
náhle	náhle	k6eAd1	náhle
kolabuje	kolabovat	k5eAaImIp3nS	kolabovat
<g/>
,	,	kIx,	,
elektrony	elektron	k1gInPc1	elektron
se	se	k3xPyFc4	se
kombinují	kombinovat	k5eAaImIp3nP	kombinovat
s	s	k7c7	s
protony	proton	k1gInPc7	proton
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
neutrony	neutron	k1gInPc1	neutron
<g/>
,	,	kIx,	,
neutrina	neutrino	k1gNnPc1	neutrino
a	a	k8xC	a
gama	gama	k1gNnSc1	gama
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Nárazová	nárazový	k2eAgFnSc1d1	nárazová
vlna	vlna	k1gFnSc1	vlna
vyvolaná	vyvolaný	k2eAgFnSc1d1	vyvolaná
kolapsem	kolaps	k1gInSc7	kolaps
způsobí	způsobit	k5eAaPmIp3nS	způsobit
výbuch	výbuch	k1gInSc1	výbuch
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
exploze	exploze	k1gFnPc1	exploze
supernovy	supernova	k1gFnSc2	supernova
<g/>
.	.	kIx.	.
</s>
<s>
Supernovy	supernova	k1gFnPc1	supernova
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
krátký	krátký	k2eAgInSc4d1	krátký
okamžik	okamžik	k1gInSc4	okamžik
prosvítí	prosvítit	k5eAaPmIp3nS	prosvítit
celou	celý	k2eAgFnSc4d1	celá
vlastní	vlastní	k2eAgFnSc4d1	vlastní
galaxii	galaxie	k1gFnSc4	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
vyskytly	vyskytnout	k5eAaPmAgInP	vyskytnout
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
Galaxii	galaxie	k1gFnSc6	galaxie
<g/>
,	,	kIx,	,
daly	dát	k5eAaPmAgFnP	dát
se	se	k3xPyFc4	se
pozorovat	pozorovat	k5eAaImF	pozorovat
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
mohutnější	mohutný	k2eAgFnPc1d2	mohutnější
jsou	být	k5eAaImIp3nP	být
exploze	exploze	k1gFnPc1	exploze
hypernov	hypernov	k1gInSc4	hypernov
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
hmoty	hmota	k1gFnSc2	hmota
hvězdy	hvězda	k1gFnSc2	hvězda
je	být	k5eAaImIp3nS	být
rozmetána	rozmetat	k5eAaImNgFnS	rozmetat
výbuchem	výbuch	k1gInSc7	výbuch
supernovy	supernova	k1gFnSc2	supernova
(	(	kIx(	(
<g/>
tak	tak	k6eAd1	tak
vznikají	vznikat	k5eAaImIp3nP	vznikat
mlhoviny	mlhovina	k1gFnPc1	mlhovina
jako	jako	k8xC	jako
např.	např.	kA	např.
Krabí	krabit	k5eAaImIp3nS	krabit
mlhovina	mlhovina	k1gFnSc1	mlhovina
<g/>
)	)	kIx)	)
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
zůstane	zůstat	k5eAaPmIp3nS	zůstat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
neutronová	neutronový	k2eAgFnSc1d1	neutronová
hvězda	hvězda	k1gFnSc1	hvězda
(	(	kIx(	(
<g/>
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
projevuje	projevovat	k5eAaImIp3nS	projevovat
jako	jako	k9	jako
pulsar	pulsar	k1gInSc1	pulsar
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
případě	případ	k1gInSc6	případ
největších	veliký	k2eAgFnPc2d3	veliký
hvězd	hvězda	k1gFnPc2	hvězda
(	(	kIx(	(
<g/>
dostatečně	dostatečně	k6eAd1	dostatečně
velkých	velká	k1gFnPc2	velká
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zanechaly	zanechat	k5eAaPmAgFnP	zanechat
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
zůstatek	zůstatek	k1gInSc4	zůstatek
těžší	těžký	k2eAgInSc4d2	těžší
než	než	k8xS	než
~	~	kIx~	~
4	[number]	k4	4
M	M	kA	M
<g/>
⊙	⊙	k?	⊙
<g/>
)	)	kIx)	)
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
černá	černý	k2eAgFnSc1d1	černá
díra	díra	k1gFnSc1	díra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neutronové	neutronový	k2eAgFnSc6d1	neutronová
hvězdě	hvězda	k1gFnSc6	hvězda
je	být	k5eAaImIp3nS	být
hmota	hmota	k1gFnSc1	hmota
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
známém	známý	k2eAgInSc6d1	známý
jako	jako	k8xS	jako
degenerovaná	degenerovaný	k2eAgFnSc1d1	degenerovaná
neutronová	neutronový	k2eAgFnSc1d1	neutronová
hmota	hmota	k1gFnSc1	hmota
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
ještě	ještě	k9	ještě
exotičtější	exotický	k2eAgFnSc1d2	exotičtější
forma	forma	k1gFnSc1	forma
hmoty	hmota	k1gFnSc2	hmota
tzv.	tzv.	kA	tzv.
QCD	QCD	kA	QCD
hmota	hmota	k1gFnSc1	hmota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
je	být	k5eAaImIp3nS	být
hmota	hmota	k1gFnSc1	hmota
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
kterému	který	k3yIgInSc3	který
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nerozumíme	rozumět	k5eNaImIp1nP	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Hmota	hmota	k1gFnSc1	hmota
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
hvězdou	hvězda	k1gFnSc7	hvězda
vyvržena	vyvržen	k2eAgFnSc1d1	vyvržena
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
planetární	planetární	k2eAgFnSc2d1	planetární
mlhoviny	mlhovina	k1gFnSc2	mlhovina
nebo	nebo	k8xC	nebo
zbytků	zbytek	k1gInPc2	zbytek
po	po	k7c6	po
výbuchu	výbuch	k1gInSc6	výbuch
supernovy	supernova	k1gFnSc2	supernova
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
rozpíná	rozpínat	k5eAaImIp3nS	rozpínat
<g/>
,	,	kIx,	,
mísí	mísit	k5eAaImIp3nP	mísit
se	se	k3xPyFc4	se
s	s	k7c7	s
mezihvězdnou	mezihvězdný	k2eAgFnSc7d1	mezihvězdná
hmotou	hmota	k1gFnSc7	hmota
a	a	k8xC	a
vrací	vracet	k5eAaImIp3nS	vracet
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
do	do	k7c2	do
oběhu	oběh	k1gInSc6	oběh
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
za	za	k7c4	za
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
mohou	moct	k5eAaImIp3nP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
nové	nový	k2eAgFnPc4d1	nová
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Odvržené	odvržený	k2eAgFnPc1d1	odvržená
vnější	vnější	k2eAgFnPc1d1	vnější
vrstvy	vrstva	k1gFnPc1	vrstva
umírajících	umírající	k2eAgFnPc2d1	umírající
hvězd	hvězda	k1gFnPc2	hvězda
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
těžké	těžký	k2eAgInPc1d1	těžký
prvky	prvek	k1gInPc1	prvek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
po	po	k7c4	po
zrecyklování	zrecyklování	k1gNnSc4	zrecyklování
další	další	k2eAgFnSc7d1	další
generací	generace	k1gFnSc7	generace
hvězd	hvězda	k1gFnPc2	hvězda
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
vznik	vznik	k1gInSc4	vznik
kamenných	kamenný	k2eAgFnPc2d1	kamenná
planet	planeta	k1gFnPc2	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Modely	model	k1gInPc1	model
života	život	k1gInSc2	život
hvězd	hvězda	k1gFnPc2	hvězda
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
teoriemi	teorie	k1gFnPc7	teorie
-	-	kIx~	-
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velké	velký	k2eAgFnSc3d1	velká
délce	délka	k1gFnSc3	délka
života	život	k1gInSc2	život
i	i	k8xC	i
těch	ten	k3xDgFnPc2	ten
nejkratších	krátký	k2eAgFnPc2d3	nejkratší
žijících	žijící	k2eAgFnPc2d1	žijící
hvězd	hvězda	k1gFnPc2	hvězda
lidstvo	lidstvo	k1gNnSc1	lidstvo
ještě	ještě	k6eAd1	ještě
nemělo	mít	k5eNaImAgNnS	mít
možnost	možnost	k1gFnSc4	možnost
sledovat	sledovat	k5eAaImF	sledovat
nějakou	nějaký	k3yIgFnSc4	nějaký
hvězdu	hvězda	k1gFnSc4	hvězda
od	od	k7c2	od
jejího	její	k3xOp3gInSc2	její
vzniku	vznik	k1gInSc2	vznik
až	až	k9	až
po	po	k7c4	po
zánik	zánik	k1gInSc4	zánik
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
modely	model	k1gInPc1	model
vycházejí	vycházet	k5eAaImIp3nP	vycházet
z	z	k7c2	z
pozorování	pozorování	k1gNnPc2	pozorování
hvězd	hvězda	k1gFnPc2	hvězda
s	s	k7c7	s
odlišnými	odlišný	k2eAgFnPc7d1	odlišná
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
-	-	kIx~	-
hvězd	hvězda	k1gFnPc2	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
,	,	kIx,	,
obrů	obr	k1gMnPc2	obr
<g/>
,	,	kIx,	,
bílých	bílý	k2eAgMnPc2d1	bílý
trpalíků	trpalík	k1gMnPc2	trpalík
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
časového	časový	k2eAgNnSc2d1	časové
období	období	k1gNnSc2	období
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
postupným	postupný	k2eAgFnPc3d1	postupná
proměnám	proměna	k1gFnPc3	proměna
hvězd	hvězda	k1gFnPc2	hvězda
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
pozorované	pozorovaný	k2eAgFnSc2d1	pozorovaná
formy	forma	k1gFnSc2	forma
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
samostatných	samostatný	k2eAgFnPc2d1	samostatná
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
vícenásobné	vícenásobný	k2eAgInPc1d1	vícenásobný
hvězdné	hvězdný	k2eAgInPc1d1	hvězdný
systémy	systém	k1gInPc1	systém
tvořené	tvořený	k2eAgFnSc2d1	tvořená
dvěma	dva	k4xCgFnPc7	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
gravitačně	gravitačně	k6eAd1	gravitačně
svázanými	svázaný	k2eAgFnPc7d1	svázaná
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
obíhají	obíhat	k5eAaImIp3nP	obíhat
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějším	častý	k2eAgInSc7d3	nejčastější
příkladem	příklad	k1gInSc7	příklad
vícehvězdného	vícehvězdný	k2eAgInSc2d1	vícehvězdný
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
dvojhvězda	dvojhvězda	k1gFnSc1	dvojhvězda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
systémy	systém	k1gInPc1	systém
tří	tři	k4xCgFnPc2	tři
a	a	k8xC	a
více	hodně	k6eAd2	hodně
hvězd	hvězda	k1gFnPc2	hvězda
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
běžné	běžný	k2eAgFnPc1d1	běžná
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc1	takový
vícehvězdné	vícehvězdný	k2eAgInPc1d1	vícehvězdný
systémy	systém	k1gInPc1	systém
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
stability	stabilita	k1gFnSc2	stabilita
oběžných	oběžný	k2eAgFnPc2d1	oběžná
drah	draha	k1gFnPc2	draha
hierarchicky	hierarchicky	k6eAd1	hierarchicky
organizované	organizovaný	k2eAgInPc4d1	organizovaný
soubory	soubor	k1gInPc4	soubor
vzájemně	vzájemně	k6eAd1	vzájemně
se	se	k3xPyFc4	se
obíhajících	obíhající	k2eAgFnPc2d1	obíhající
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
větší	veliký	k2eAgFnPc4d2	veliký
skupiny	skupina	k1gFnPc4	skupina
tzv.	tzv.	kA	tzv.
hvězdokupy	hvězdokupa	k1gFnSc2	hvězdokupa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
molekulárních	molekulární	k2eAgNnPc6d1	molekulární
mračnech	mračno	k1gNnPc6	mračno
vznikají	vznikat	k5eAaImIp3nP	vznikat
hvězdy	hvězda	k1gFnPc1	hvězda
ve	v	k7c6	v
skupinách	skupina	k1gFnPc6	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
minimálně	minimálně	k6eAd1	minimálně
po	po	k7c4	po
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
gravitačně	gravitačně	k6eAd1	gravitačně
vázány	vázat	k5eAaImNgFnP	vázat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Slunce	slunce	k1gNnSc2	slunce
vznikají	vznikat	k5eAaImIp3nP	vznikat
hvězdy	hvězda	k1gFnPc1	hvězda
v	v	k7c6	v
útvarech	útvar	k1gInPc6	útvar
zvaných	zvaný	k2eAgFnPc2d1	zvaná
otevřené	otevřený	k2eAgFnPc4d1	otevřená
hvězdokupy	hvězdokupa	k1gFnSc2	hvězdokupa
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
poměrně	poměrně	k6eAd1	poměrně
volné	volný	k2eAgFnPc1d1	volná
skupiny	skupina	k1gFnPc1	skupina
desítek	desítka	k1gFnPc2	desítka
až	až	k8xS	až
stovek	stovka	k1gFnPc2	stovka
mladých	mladý	k2eAgFnPc2d1	mladá
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
časem	časem	k6eAd1	časem
rozpadají	rozpadat	k5eAaPmIp3nP	rozpadat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
Galaxii	galaxie	k1gFnSc6	galaxie
vznikaly	vznikat	k5eAaImAgFnP	vznikat
hvězdy	hvězda	k1gFnPc1	hvězda
i	i	k9	i
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
hustších	hustý	k2eAgInPc6d2	hustší
a	a	k8xC	a
kompaktnějších	kompaktní	k2eAgInPc6d2	kompaktnější
útvarech	útvar	k1gInPc6	útvar
obsahujících	obsahující	k2eAgInPc6d1	obsahující
až	až	k6eAd1	až
miliony	milion	k4xCgInPc4	milion
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
seskupení	seskupení	k1gNnPc1	seskupení
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
kulové	kulový	k2eAgFnPc1d1	kulová
hvězdokupy	hvězdokupa	k1gFnPc1	hvězdokupa
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
však	však	k9	však
kulové	kulový	k2eAgFnPc1d1	kulová
hvězdokupy	hvězdokupa	k1gFnPc1	hvězdokupa
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
Galaxii	galaxie	k1gFnSc6	galaxie
již	již	k6eAd1	již
nevznikají	vznikat	k5eNaImIp3nP	vznikat
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
v	v	k7c6	v
kulových	kulový	k2eAgFnPc6d1	kulová
hvězdokupách	hvězdokupa	k1gFnPc6	hvězdokupa
jsou	být	k5eAaImIp3nP	být
silněji	silně	k6eAd2	silně
gravitačně	gravitačně	k6eAd1	gravitačně
vázány	vázat	k5eAaImNgFnP	vázat
než	než	k8xS	než
členové	člen	k1gMnPc1	člen
otevřených	otevřený	k2eAgFnPc2d1	otevřená
hvězdokup	hvězdokupa	k1gFnPc2	hvězdokupa
a	a	k8xC	a
často	často	k6eAd1	často
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
spolu	spolu	k6eAd1	spolu
až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
zániku	zánik	k1gInSc2	zánik
<g/>
.	.	kIx.	.
</s>
<s>
Prostorově	prostorově	k6eAd1	prostorově
ohraničená	ohraničený	k2eAgFnSc1d1	ohraničená
skupina	skupina	k1gFnSc1	skupina
určitého	určitý	k2eAgInSc2d1	určitý
typu	typ	k1gInSc2	typ
hvězd	hvězda	k1gFnPc2	hvězda
společného	společný	k2eAgInSc2d1	společný
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
volnější	volný	k2eAgFnSc1d2	volnější
než	než	k8xS	než
otevřená	otevřený	k2eAgFnSc1d1	otevřená
hvězdokupa	hvězdokupa	k1gFnSc1	hvězdokupa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
asociace	asociace	k1gFnSc1	asociace
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
hvězdokup	hvězdokupa	k1gFnPc2	hvězdokupa
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
již	již	k6eAd1	již
ve	v	k7c6	v
hvězdokupách	hvězdokupa	k1gFnPc6	hvězdokupa
nenacházejí	nacházet	k5eNaImIp3nP	nacházet
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
mnohem	mnohem	k6eAd1	mnohem
bližší	blízký	k2eAgFnPc1d2	bližší
a	a	k8xC	a
stabilnější	stabilní	k2eAgFnPc1d2	stabilnější
konfigurace	konfigurace	k1gFnPc1	konfigurace
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastějším	častý	k2eAgInSc7d3	nejčastější
případem	případ	k1gInSc7	případ
je	být	k5eAaImIp3nS	být
dvojhvězda	dvojhvězda	k1gFnSc1	dvojhvězda
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dvě	dva	k4xCgFnPc1	dva
přibližně	přibližně	k6eAd1	přibližně
stejně	stejně	k6eAd1	stejně
staré	starý	k2eAgFnPc1d1	stará
hvězdy	hvězda	k1gFnPc1	hvězda
obíhají	obíhat	k5eAaImIp3nP	obíhat
kolem	kolem	k7c2	kolem
společného	společný	k2eAgNnSc2d1	společné
těžiště	těžiště	k1gNnSc2	těžiště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedna	jeden	k4xCgFnSc1	jeden
složka	složka	k1gFnSc1	složka
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
hmotnější	hmotný	k2eAgFnSc1d2	hmotnější
než	než	k8xS	než
druhá	druhý	k4xOgFnSc1	druhý
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
těžiště	těžiště	k1gNnSc1	těžiště
soustavy	soustava	k1gFnSc2	soustava
uvnitř	uvnitř	k7c2	uvnitř
hmotnější	hmotný	k2eAgFnSc2d2	hmotnější
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
méně	málo	k6eAd2	málo
hmotná	hmotný	k2eAgFnSc1d1	hmotná
složka	složka	k1gFnSc1	složka
obíhá	obíhat	k5eAaImIp3nS	obíhat
kolem	kolem	k7c2	kolem
ní	on	k3xPp3gFnSc2	on
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
planety	planeta	k1gFnPc1	planeta
obíhají	obíhat	k5eAaImIp3nP	obíhat
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
vidíme	vidět	k5eAaImIp1nP	vidět
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
takovou	takový	k3xDgFnSc4	takový
dvojici	dvojice	k1gFnSc4	dvojice
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
dvojhvězdy	dvojhvězda	k1gFnPc1	dvojhvězda
lze	lze	k6eAd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
již	již	k6eAd1	již
malými	malý	k2eAgInPc7d1	malý
dalekohledy	dalekohled	k1gInPc7	dalekohled
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc1	některý
jen	jen	k9	jen
mohutnějšími	mohutný	k2eAgInPc7d2	mohutnější
přístroji	přístroj	k1gInPc7	přístroj
a	a	k8xC	a
některé	některý	k3yIgInPc4	některý
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
tak	tak	k6eAd1	tak
blízko	blízko	k6eAd1	blízko
<g/>
,	,	kIx,	,
že	že	k8xS	že
ani	ani	k8xC	ani
při	při	k7c6	při
nejvyšším	vysoký	k2eAgNnSc6d3	nejvyšší
dostupném	dostupný	k2eAgNnSc6d1	dostupné
rozlišení	rozlišení	k1gNnSc6	rozlišení
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
nepodaří	podařit	k5eNaPmIp3nS	podařit
pozorovat	pozorovat	k5eAaImF	pozorovat
jako	jako	k9	jako
dvě	dva	k4xCgFnPc4	dva
oddělené	oddělený	k2eAgFnPc4d1	oddělená
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
takzvané	takzvaný	k2eAgFnPc1d1	takzvaná
spektroskopické	spektroskopický	k2eAgFnPc1d1	spektroskopická
dvojhvězdy	dvojhvězda	k1gFnPc1	dvojhvězda
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
obíhají	obíhat	k5eAaImIp3nP	obíhat
společné	společný	k2eAgNnSc4d1	společné
těžiště	těžiště	k1gNnSc4	těžiště
tři	tři	k4xCgFnPc1	tři
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
trojhvězdě	trojhvězda	k1gFnSc6	trojhvězda
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
tvoří	tvořit	k5eAaImIp3nP	tvořit
trojhvězdí	trojhvězdí	k1gNnSc4	trojhvězdí
formaci	formace	k1gFnSc3	formace
centrální	centrální	k2eAgFnSc2d1	centrální
dvojice	dvojice	k1gFnSc2	dvojice
hmotnostně	hmotnostně	k6eAd1	hmotnostně
víceméně	víceméně	k9	víceméně
vyrovnaných	vyrovnaný	k2eAgFnPc2d1	vyrovnaná
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
obíhají	obíhat	k5eAaImIp3nP	obíhat
společné	společný	k2eAgNnSc4d1	společné
těžiště	těžiště	k1gNnSc4	těžiště
a	a	k8xC	a
kolem	kolem	k7c2	kolem
nich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc6d2	veliký
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
obíhá	obíhat	k5eAaImIp3nS	obíhat
méně	málo	k6eAd2	málo
hmotná	hmotný	k2eAgFnSc1d1	hmotná
třetí	třetí	k4xOgFnSc1	třetí
složka	složka	k1gFnSc1	složka
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
systém	systém	k1gInSc1	systém
utváří	utvářit	k5eAaPmIp3nS	utvářit
například	například	k6eAd1	například
Slunci	slunce	k1gNnSc3	slunce
nejbližší	blízký	k2eAgFnSc1d3	nejbližší
soustava	soustava	k1gFnSc1	soustava
Alfa	alfa	k1gFnSc1	alfa
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
společné	společný	k2eAgNnSc4d1	společné
těžiště	těžiště	k1gNnSc4	těžiště
obíhají	obíhat	k5eAaImIp3nP	obíhat
čtyři	čtyři	k4xCgFnPc1	čtyři
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
mluvíme	mluvit	k5eAaImIp1nP	mluvit
o	o	k7c6	o
čtyřhvězdě	čtyřhvězda	k1gFnSc6	čtyřhvězda
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
čtyřhvězdy	čtyřhvězda	k1gFnSc2	čtyřhvězda
je	být	k5eAaImIp3nS	být
Epsilon	epsilon	k1gNnSc1	epsilon
Lyrae	Lyra	k1gFnSc2	Lyra
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
dvě	dva	k4xCgFnPc4	dva
dvojice	dvojice	k1gFnPc4	dvojice
hvězd	hvězda	k1gFnPc2	hvězda
obíhající	obíhající	k2eAgFnPc1d1	obíhající
kolem	kolo	k1gNnSc7	kolo
společného	společný	k2eAgNnSc2d1	společné
těžiště	těžiště	k1gNnSc2	těžiště
<g/>
.	.	kIx.	.
</s>
<s>
Gravitačně	gravitačně	k6eAd1	gravitačně
těsně	těsně	k6eAd1	těsně
vázaných	vázaný	k2eAgFnPc2d1	vázaná
hvězd	hvězda	k1gFnPc2	hvězda
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
více	hodně	k6eAd2	hodně
než	než	k8xS	než
čtyři	čtyři	k4xCgInPc4	čtyři
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdné	hvězdný	k2eAgInPc1d1	hvězdný
systémy	systém	k1gInPc1	systém
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvěma	dva	k4xCgFnPc7	dva
složkami	složka	k1gFnPc7	složka
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
souhrnným	souhrnný	k2eAgInSc7d1	souhrnný
názvem	název	k1gInSc7	název
vícenásobné	vícenásobný	k2eAgFnSc2d1	vícenásobná
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Známou	známý	k2eAgFnSc7d1	známá
vícenásobnou	vícenásobný	k2eAgFnSc7d1	vícenásobná
hvězdou	hvězda	k1gFnSc7	hvězda
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Trapéz	trapéz	k1gInSc4	trapéz
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Orion	orion	k1gInSc1	orion
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
převažoval	převažovat	k5eAaImAgInS	převažovat
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
hvězd	hvězda	k1gFnPc2	hvězda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
gravitačně	gravitačně	k6eAd1	gravitačně
svázaných	svázaný	k2eAgInPc6d1	svázaný
vícenásobných	vícenásobný	k2eAgInPc6d1	vícenásobný
systémech	systém	k1gInPc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
pravda	pravda	k1gFnSc1	pravda
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
při	při	k7c6	při
obrovských	obrovský	k2eAgFnPc6d1	obrovská
hvězdách	hvězda	k1gFnPc6	hvězda
třídy	třída	k1gFnSc2	třída
O	O	kA	O
a	a	k8xC	a
B	B	kA	B
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
až	až	k9	až
80	[number]	k4	80
%	%	kIx~	%
hvězd	hvězda	k1gFnPc2	hvězda
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
takových	takový	k3xDgInPc6	takový
systémech	systém	k1gInPc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
samostatných	samostatný	k2eAgFnPc2d1	samostatná
hvězd	hvězda	k1gFnPc2	hvězda
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
s	s	k7c7	s
klesající	klesající	k2eAgFnSc7d1	klesající
hmotností	hmotnost	k1gFnSc7	hmotnost
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
255	[number]	k4	255
červených	červený	k2eAgMnPc2d1	červený
trpaslíků	trpaslík	k1gMnPc2	trpaslík
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
existence	existence	k1gFnSc1	existence
hvězdného	hvězdný	k2eAgMnSc2d1	hvězdný
společníka	společník	k1gMnSc2	společník
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
protože	protože	k8xS	protože
85	[number]	k4	85
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
hvězd	hvězda	k1gFnPc2	hvězda
jsou	být	k5eAaImIp3nP	být
právě	právě	k9	právě
červení	červený	k2eAgMnPc1d1	červený
trpaslíci	trpaslík	k1gMnPc1	trpaslík
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
hvězd	hvězda	k1gFnPc2	hvězda
v	v	k7c6	v
Galaxii	galaxie	k1gFnSc6	galaxie
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
samostatná	samostatný	k2eAgFnSc1d1	samostatná
od	od	k7c2	od
narození	narození	k1gNnSc2	narození
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgFnPc1	všechen
hvězdy	hvězda	k1gFnPc1	hvězda
tvoří	tvořit	k5eAaImIp3nP	tvořit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
mezihvězdnou	mezihvězdný	k2eAgFnSc7d1	mezihvězdná
hmotou	hmota	k1gFnSc7	hmota
a	a	k8xC	a
obrovskými	obrovský	k2eAgNnPc7d1	obrovské
množstvími	množství	k1gNnPc7	množství
temné	temný	k2eAgFnPc1d1	temná
hmoty	hmota	k1gFnPc1	hmota
gigantické	gigantický	k2eAgInPc4d1	gigantický
kompaktní	kompaktní	k2eAgInPc4d1	kompaktní
systémy	systém	k1gInPc4	systém
-	-	kIx~	-
galaxie	galaxie	k1gFnPc4	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	hvězda	k1gFnSc1	hvězda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
nějaké	nějaký	k3yIgFnSc2	nějaký
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
,	,	kIx,	,
obíhá	obíhat	k5eAaImIp3nS	obíhat
kolem	kolem	k7c2	kolem
jejího	její	k3xOp3gNnSc2	její
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
hvězdy	hvězda	k1gFnPc1	hvězda
zdánlivě	zdánlivě	k6eAd1	zdánlivě
vytržené	vytržený	k2eAgFnPc1d1	vytržená
z	z	k7c2	z
galaxií	galaxie	k1gFnPc2	galaxie
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
při	při	k7c6	při
vzájemných	vzájemný	k2eAgFnPc6d1	vzájemná
kolizích	kolize	k1gFnPc6	kolize
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
)	)	kIx)	)
zřejmě	zřejmě	k6eAd1	zřejmě
spadají	spadat	k5eAaImIp3nP	spadat
pod	pod	k7c4	pod
gravitační	gravitační	k2eAgInSc4d1	gravitační
vliv	vliv	k1gInSc4	vliv
nějaké	nějaký	k3yIgFnSc2	nějaký
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
hvězdy	hvězda	k1gFnPc1	hvězda
viditelné	viditelný	k2eAgFnPc1d1	viditelná
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
a	a	k8xC	a
menšími	malý	k2eAgInPc7d2	menší
dalekohledy	dalekohled	k1gInPc7	dalekohled
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
naší	náš	k3xOp1gFnSc2	náš
Galaxie	galaxie	k1gFnSc2	galaxie
-	-	kIx~	-
Mléčné	mléčný	k2eAgFnSc2d1	mléčná
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
galaxie	galaxie	k1gFnSc1	galaxie
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
stovky	stovka	k1gFnPc4	stovka
miliard	miliarda	k4xCgFnPc2	miliarda
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
pozorovatelném	pozorovatelný	k2eAgInSc6d1	pozorovatelný
vesmíru	vesmír	k1gInSc6	vesmír
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
miliard	miliarda	k4xCgFnPc2	miliarda
(	(	kIx(	(
<g/>
1011	[number]	k4	1011
<g/>
)	)	kIx)	)
galaxií	galaxie	k1gFnPc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Odhad	odhad	k1gInSc1	odhad
počtu	počet	k1gInSc2	počet
hvězd	hvězda	k1gFnPc2	hvězda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
viditelném	viditelný	k2eAgInSc6d1	viditelný
vesmíru	vesmír	k1gInSc6	vesmír
existuje	existovat	k5eAaImIp3nS	existovat
300	[number]	k4	300
triliard	triliarda	k4xCgFnPc2	triliarda
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
×	×	k?	×
<g/>
1023	[number]	k4	1023
<g/>
,	,	kIx,	,
)	)	kIx)	)
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvojhvězdách	dvojhvězda	k1gFnPc6	dvojhvězda
a	a	k8xC	a
vícenásobných	vícenásobný	k2eAgFnPc6d1	vícenásobná
hvězdách	hvězda	k1gFnPc6	hvězda
jsou	být	k5eAaImIp3nP	být
vzdálenosti	vzdálenost	k1gFnPc1	vzdálenost
mezi	mezi	k7c7	mezi
jejich	jejich	k3xOp3gFnPc7	jejich
složkami	složka	k1gFnPc7	složka
relativně	relativně	k6eAd1	relativně
malé	malý	k2eAgMnPc4d1	malý
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
srovnatelné	srovnatelný	k2eAgInPc1d1	srovnatelný
se	s	k7c7	s
vzdálenostmi	vzdálenost	k1gFnPc7	vzdálenost
planet	planeta	k1gFnPc2	planeta
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
o	o	k7c4	o
něco	něco	k3yInSc4	něco
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnPc1d2	veliký
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
,	,	kIx,	,
jaké	jaký	k3yRgFnPc1	jaký
mají	mít	k5eAaImIp3nP	mít
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
nesouvisející	související	k2eNgInPc1d1	nesouvisející
vícehvězdné	vícehvězdný	k2eAgInPc1d1	vícehvězdný
systémy	systém	k1gInPc1	systém
nebo	nebo	k8xC	nebo
osamělé	osamělý	k2eAgFnPc1d1	osamělá
hvězdy	hvězda	k1gFnPc1	hvězda
typu	typ	k1gInSc2	typ
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližší	blízký	k2eAgFnSc7d3	nejbližší
hvězdou	hvězda	k1gFnSc7	hvězda
ke	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
je	být	k5eAaImIp3nS	být
Proxima	Proxima	k1gFnSc1	Proxima
Centauri	Centauri	k1gNnSc2	Centauri
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
39,9	[number]	k4	39,9
bilionu	bilion	k4xCgInSc2	bilion
km	km	kA	km
nebo	nebo	k8xC	nebo
4,2	[number]	k4	4,2
světelného	světelný	k2eAgInSc2d1	světelný
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Trvalo	trvat	k5eAaImAgNnS	trvat
by	by	k9	by
150	[number]	k4	150
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
než	než	k8xS	než
bychom	by	kYmCp1nP	by
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
dostali	dostat	k5eAaPmAgMnP	dostat
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
kosmická	kosmický	k2eAgFnSc1d1	kosmická
stanice	stanice	k1gFnSc1	stanice
ISS	ISS	kA	ISS
obíhá	obíhat	k5eAaImIp3nS	obíhat
kolem	kolem	k7c2	kolem
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
8	[number]	k4	8
km	km	kA	km
/	/	kIx~	/
s	s	k7c7	s
=	=	kIx~	=
27	[number]	k4	27
500	[number]	k4	500
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgFnPc1	takový
vzdálenosti	vzdálenost	k1gFnPc1	vzdálenost
jsou	být	k5eAaImIp3nP	být
běžné	běžný	k2eAgFnPc1d1	běžná
pro	pro	k7c4	pro
vnitřek	vnitřek	k1gInSc4	vnitřek
galaktického	galaktický	k2eAgInSc2d1	galaktický
disku	disk	k1gInSc2	disk
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
poměrně	poměrně	k6eAd1	poměrně
velkým	velký	k2eAgFnPc3d1	velká
vzdálenostem	vzdálenost	k1gFnPc3	vzdálenost
mezi	mezi	k7c7	mezi
hvězdami	hvězda	k1gFnPc7	hvězda
nejsou	být	k5eNaImIp3nP	být
vzájemné	vzájemný	k2eAgFnPc1d1	vzájemná
srážky	srážka	k1gFnPc1	srážka
hvězd	hvězda	k1gFnPc2	hvězda
časté	častý	k2eAgFnPc1d1	častá
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
sobě	se	k3xPyFc3	se
mnohem	mnohem	k6eAd1	mnohem
blíže	blízce	k6eAd2	blízce
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
galaxií	galaxie	k1gFnPc2	galaxie
nebo	nebo	k8xC	nebo
v	v	k7c6	v
kulových	kulový	k2eAgFnPc6d1	kulová
hvězdokupách	hvězdokupa	k1gFnPc6	hvězdokupa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
více	hodně	k6eAd2	hodně
kolizí	kolize	k1gFnPc2	kolize
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
mnohem	mnohem	k6eAd1	mnohem
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
galaktickém	galaktický	k2eAgNnSc6d1	Galaktické
halo	halo	k1gNnSc1	halo
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
hvězdy	hvězda	k1gFnSc2	hvězda
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
může	moct	k5eAaImIp3nS	moct
poskytnout	poskytnout	k5eAaPmF	poskytnout
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
původu	původ	k1gInSc6	původ
a	a	k8xC	a
stáří	stáří	k1gNnSc6	stáří
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
o	o	k7c6	o
její	její	k3xOp3gFnSc6	její
struktuře	struktura	k1gFnSc6	struktura
a	a	k8xC	a
vývoji	vývoj	k1gInSc6	vývoj
jejího	její	k3xOp3gNnSc2	její
galaktického	galaktický	k2eAgNnSc2d1	Galaktické
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc4	pohyb
hvězdy	hvězda	k1gFnSc2	hvězda
popisují	popisovat	k5eAaImIp3nP	popisovat
dvě	dva	k4xCgFnPc1	dva
složky	složka	k1gFnPc1	složka
<g/>
:	:	kIx,	:
radiální	radiální	k2eAgFnSc1d1	radiální
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc4	rychlost
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
k	k	k7c3	k
nebo	nebo	k8xC	nebo
od	od	k7c2	od
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
a	a	k8xC	a
úhlový	úhlový	k2eAgInSc1d1	úhlový
pohyb	pohyb	k1gInSc1	pohyb
po	po	k7c6	po
nebeské	nebeský	k2eAgFnSc6d1	nebeská
sféře	sféra	k1gFnSc6	sféra
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
také	také	k9	také
vlastní	vlastní	k2eAgInSc4d1	vlastní
pohyb	pohyb	k1gInSc4	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Radiální	radiální	k2eAgFnSc1d1	radiální
rychlost	rychlost	k1gFnSc1	rychlost
se	se	k3xPyFc4	se
určuje	určovat	k5eAaImIp3nS	určovat
pomocí	pomocí	k7c2	pomocí
měření	měření	k1gNnSc2	měření
dopplerovského	dopplerovský	k2eAgInSc2d1	dopplerovský
posunu	posun	k1gInSc2	posun
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
čar	čára	k1gFnPc2	čára
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jsou	být	k5eAaImIp3nP	být
posunuty	posunout	k5eAaPmNgFnP	posunout
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
modrému	modrý	k2eAgInSc3d1	modrý
konci	konec	k1gInSc3	konec
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
hvězda	hvězda	k1gFnSc1	hvězda
se	se	k3xPyFc4	se
k	k	k7c3	k
nám	my	k3xPp1nPc3	my
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
k	k	k7c3	k
červenému	červený	k2eAgInSc3d1	červený
konci	konec	k1gInSc3	konec
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
hvězda	hvězda	k1gFnSc1	hvězda
se	se	k3xPyFc4	se
od	od	k7c2	od
nás	my	k3xPp1nPc2	my
vzdaluje	vzdalovat	k5eAaImIp3nS	vzdalovat
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
rychlost	rychlost	k1gFnSc1	rychlost
se	se	k3xPyFc4	se
udává	udávat	k5eAaImIp3nS	udávat
v	v	k7c6	v
kilometrech	kilometr	k1gInPc6	kilometr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
(	(	kIx(	(
<g/>
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInSc1d1	vlastní
pohyb	pohyb	k1gInSc1	pohyb
hvězdy	hvězda	k1gFnSc2	hvězda
se	se	k3xPyFc4	se
určuje	určovat	k5eAaImIp3nS	určovat
pomocí	pomocí	k7c2	pomocí
přesných	přesný	k2eAgNnPc2d1	přesné
astrometrických	astrometrický	k2eAgNnPc2d1	astrometrický
měření	měření	k1gNnPc2	měření
a	a	k8xC	a
udává	udávat	k5eAaImIp3nS	udávat
se	se	k3xPyFc4	se
v	v	k7c6	v
tisícinách	tisícina	k1gFnPc6	tisícina
úhlové	úhlový	k2eAgFnSc2d1	úhlová
vteřiny	vteřina	k1gFnSc2	vteřina
(	(	kIx(	(
<g/>
mas	masa	k1gFnPc2	masa
<g/>
)	)	kIx)	)
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastní	k2eAgInSc1d1	vlastní
pohyb	pohyb	k1gInSc1	pohyb
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
paralaxy	paralaxa	k1gFnSc2	paralaxa
přeměnit	přeměnit	k5eAaPmF	přeměnit
na	na	k7c4	na
jednotky	jednotka	k1gFnPc4	jednotka
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
hodnotou	hodnota	k1gFnSc7	hodnota
vlastního	vlastní	k2eAgInSc2d1	vlastní
pohybu	pohyb	k1gInSc2	pohyb
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
relativně	relativně	k6eAd1	relativně
blízko	blízko	k7c2	blízko
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
parametrů	parametr	k1gInPc2	parametr
lze	lze	k6eAd1	lze
vypočítat	vypočítat	k5eAaPmF	vypočítat
rychlost	rychlost	k1gFnSc4	rychlost
hvězdy	hvězda	k1gFnSc2	hvězda
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
Slunci	slunce	k1gNnSc3	slunce
nebo	nebo	k8xC	nebo
při	při	k7c6	při
jejím	její	k3xOp3gInSc6	její
pohybu	pohyb	k1gInSc6	pohyb
v	v	k7c6	v
galaxii	galaxie	k1gFnSc6	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Pozorování	pozorování	k1gNnSc4	pozorování
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
obecně	obecně	k6eAd1	obecně
mají	mít	k5eAaImIp3nP	mít
hvězdy	hvězda	k1gFnPc1	hvězda
populace	populace	k1gFnSc2	populace
I	i	k8xC	i
nižší	nízký	k2eAgFnSc2d2	nižší
rychlosti	rychlost	k1gFnSc2	rychlost
než	než	k8xS	než
starší	starý	k2eAgFnSc2d2	starší
hvězdy	hvězda	k1gFnSc2	hvězda
populace	populace	k1gFnSc2	populace
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
mají	mít	k5eAaImIp3nP	mít
eliptické	eliptický	k2eAgFnPc4d1	eliptická
oběžné	oběžný	k2eAgFnPc4d1	oběžná
dráhy	dráha	k1gFnPc4	dráha
nakloněné	nakloněný	k2eAgFnPc1d1	nakloněná
k	k	k7c3	k
rovině	rovina	k1gFnSc3	rovina
galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Porovnání	porovnání	k1gNnSc1	porovnání
pohybů	pohyb	k1gInPc2	pohyb
blízkých	blízký	k2eAgFnPc2d1	blízká
hvězd	hvězda	k1gFnPc2	hvězda
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
objevům	objev	k1gInPc3	objev
hvězdných	hvězdný	k2eAgFnPc2d1	hvězdná
asociací	asociace	k1gFnPc2	asociace
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
skupiny	skupina	k1gFnPc1	skupina
hvězd	hvězda	k1gFnPc2	hvězda
se	s	k7c7	s
společným	společný	k2eAgNnSc7d1	společné
místem	místo	k1gNnSc7	místo
původu	původ	k1gInSc2	původ
v	v	k7c6	v
jediném	jediný	k2eAgNnSc6d1	jediné
obrovském	obrovský	k2eAgNnSc6d1	obrovské
molekulárním	molekulární	k2eAgNnSc6d1	molekulární
mračnu	mračno	k1gNnSc6	mračno
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Spektrální	spektrální	k2eAgFnSc2d1	spektrální
klasifikace	klasifikace	k1gFnSc2	klasifikace
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
hvězdách	hvězda	k1gFnPc6	hvězda
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc1	jejich
světlo	světlo	k1gNnSc1	světlo
rozložené	rozložený	k2eAgNnSc1d1	rozložené
do	do	k7c2	do
spektra	spektrum	k1gNnSc2	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Charakter	charakter	k1gInSc1	charakter
spektra	spektrum	k1gNnSc2	spektrum
hvězdy	hvězda	k1gFnSc2	hvězda
určuje	určovat	k5eAaImIp3nS	určovat
především	především	k9	především
teplota	teplota	k1gFnSc1	teplota
atmosféry	atmosféra	k1gFnSc2	atmosféra
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgInSc1d1	současný
systém	systém	k1gInSc1	systém
klasifikace	klasifikace	k1gFnSc2	klasifikace
hvězd	hvězda	k1gFnPc2	hvězda
má	mít	k5eAaImIp3nS	mít
původ	původ	k1gInSc4	původ
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
hvězdy	hvězda	k1gFnPc1	hvězda
klasifikovaly	klasifikovat	k5eAaImAgFnP	klasifikovat
od	od	k7c2	od
A	A	kA	A
po	po	k7c6	po
Q	Q	kA	Q
na	na	k7c6	na
základě	základ	k1gInSc6	základ
síly	síla	k1gFnSc2	síla
čar	čára	k1gFnPc2	čára
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
nebylo	být	k5eNaImAgNnS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
hlavním	hlavní	k2eAgInSc7d1	hlavní
faktorem	faktor	k1gInSc7	faktor
ovlivňujícím	ovlivňující	k2eAgInSc7d1	ovlivňující
sílu	síla	k1gFnSc4	síla
této	tento	k3xDgFnSc2	tento
čáry	čára	k1gFnSc2	čára
je	být	k5eAaImIp3nS	být
teplota	teplota	k1gFnSc1	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Čára	čára	k1gFnSc1	čára
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
maxima	maxima	k1gFnSc1	maxima
při	při	k7c6	při
9	[number]	k4	9
000	[number]	k4	000
K	K	kA	K
a	a	k8xC	a
slábne	slábnout	k5eAaImIp3nS	slábnout
při	při	k7c6	při
vyšších	vysoký	k2eAgFnPc6d2	vyšší
i	i	k8xC	i
nižších	nízký	k2eAgFnPc6d2	nižší
teplotách	teplota	k1gFnPc6	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
seřazení	seřazení	k1gNnSc6	seřazení
klasifikace	klasifikace	k1gFnSc2	klasifikace
podle	podle	k7c2	podle
teploty	teplota	k1gFnSc2	teplota
už	už	k6eAd1	už
připomínala	připomínat	k5eAaImAgFnS	připomínat
současné	současný	k2eAgNnSc4d1	současné
schéma	schéma	k1gNnSc4	schéma
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
spektra	spektrum	k1gNnSc2	spektrum
čili	čili	k8xC	čili
teploty	teplota	k1gFnSc2	teplota
dělíme	dělit	k5eAaImIp1nP	dělit
hvězdy	hvězda	k1gFnPc1	hvězda
do	do	k7c2	do
osmi	osm	k4xCc2	osm
hlavních	hlavní	k2eAgFnPc2d1	hlavní
tříd	třída	k1gFnPc2	třída
(	(	kIx(	(
<g/>
W	W	kA	W
<g/>
,	,	kIx,	,
O	O	kA	O
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
,	,	kIx,	,
F	F	kA	F
<g/>
,	,	kIx,	,
G	G	kA	G
<g/>
,	,	kIx,	,
K	K	kA	K
a	a	k8xC	a
M	M	kA	M
<g/>
)	)	kIx)	)
a	a	k8xC	a
5	[number]	k4	5
vzácných	vzácný	k2eAgFnPc2d1	vzácná
tříd	třída	k1gFnPc2	třída
(	(	kIx(	(
<g/>
Q	Q	kA	Q
<g/>
,	,	kIx,	,
R	R	kA	R
<g/>
,	,	kIx,	,
N	N	kA	N
<g/>
,	,	kIx,	,
S	s	k7c7	s
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
této	tento	k3xDgFnSc2	tento
klasifikace	klasifikace	k1gFnSc2	klasifikace
rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
hvězdy	hvězda	k1gFnPc4	hvězda
raného	raný	k2eAgInSc2d1	raný
spektrálního	spektrální	k2eAgInSc2d1	spektrální
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
)	)	kIx)	)
a	a	k8xC	a
hvězdy	hvězda	k1gFnPc4	hvězda
pozdního	pozdní	k2eAgInSc2d1	pozdní
spektrálního	spektrální	k2eAgInSc2d1	spektrální
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
G	G	kA	G
<g/>
,	,	kIx,	,
K	K	kA	K
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
S	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	O	kA	O
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
horké	horký	k2eAgFnPc4d1	horká
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
postupně	postupně	k6eAd1	postupně
klesá	klesat	k5eAaImIp3nS	klesat
až	až	k9	až
po	po	k7c6	po
M	M	kA	M
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
takové	takový	k3xDgFnPc4	takový
chladné	chladný	k2eAgFnPc4d1	chladná
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
atmosférách	atmosféra	k1gFnPc6	atmosféra
mohou	moct	k5eAaImIp3nP	moct
vznikat	vznikat	k5eAaImF	vznikat
molekuly	molekula	k1gFnPc1	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
netradičních	tradiční	k2eNgInPc2d1	netradiční
spektrálních	spektrální	k2eAgInPc2d1	spektrální
typů	typ	k1gInPc2	typ
má	mít	k5eAaImIp3nS	mít
speciální	speciální	k2eAgFnSc4d1	speciální
klasifikaci	klasifikace	k1gFnSc4	klasifikace
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
nejběžnější	běžný	k2eAgMnPc4d3	Nejběžnější
patří	patřit	k5eAaImIp3nP	patřit
L	L	kA	L
a	a	k8xC	a
T	T	kA	T
<g/>
,	,	kIx,	,
označující	označující	k2eAgInPc1d1	označující
po	po	k7c6	po
pořadí	pořadí	k1gNnSc6	pořadí
nejchladnější	chladný	k2eAgFnSc2d3	nejchladnější
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
hnědé	hnědý	k2eAgMnPc4d1	hnědý
trpaslíky	trpaslík	k1gMnPc4	trpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
písmeno	písmeno	k1gNnSc1	písmeno
má	mít	k5eAaImIp3nS	mít
10	[number]	k4	10
podkategorií	podkategorie	k1gFnPc2	podkategorie
<g/>
,	,	kIx,	,
očíslovaných	očíslovaný	k2eAgInPc2d1	očíslovaný
od	od	k7c2	od
0	[number]	k4	0
po	po	k7c4	po
9	[number]	k4	9
s	s	k7c7	s
postupně	postupně	k6eAd1	postupně
klesající	klesající	k2eAgFnSc7d1	klesající
teplotou	teplota	k1gFnSc7	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
selhává	selhávat	k5eAaImIp3nS	selhávat
při	při	k7c6	při
extrémních	extrémní	k2eAgFnPc6d1	extrémní
teplotách	teplota	k1gFnPc6	teplota
<g/>
:	:	kIx,	:
hvězdy	hvězda	k1gFnPc1	hvězda
třídy	třída	k1gFnSc2	třída
O0	O0	k1gFnSc1	O0
a	a	k8xC	a
O1	O1	k1gFnSc1	O1
nemusí	muset	k5eNaImIp3nS	muset
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Bílí	bílý	k2eAgMnPc1d1	bílý
trpaslíci	trpaslík	k1gMnPc1	trpaslík
mají	mít	k5eAaImIp3nP	mít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
třídu	třída	k1gFnSc4	třída
označenou	označený	k2eAgFnSc4d1	označená
písmenem	písmeno	k1gNnSc7	písmeno
D.	D.	kA	D.
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c6	na
DA	DA	kA	DA
<g/>
,	,	kIx,	,
DB	db	kA	db
<g/>
,	,	kIx,	,
DC	DC	kA	DC
<g/>
,	,	kIx,	,
DO	do	k7c2	do
<g/>
,	,	kIx,	,
DZ	DZ	kA	DZ
<g/>
,	,	kIx,	,
a	a	k8xC	a
DQ	DQ	kA	DQ
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
převažujících	převažující	k2eAgFnPc2d1	převažující
čar	čára	k1gFnPc2	čára
ve	v	k7c6	v
spektru	spektrum	k1gNnSc6	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tím	ten	k3xDgNnSc7	ten
následuje	následovat	k5eAaImIp3nS	následovat
číselná	číselný	k2eAgFnSc1d1	číselná
hodnota	hodnota	k1gFnSc1	hodnota
označující	označující	k2eAgFnSc4d1	označující
teplotu	teplota	k1gFnSc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
zakreslené	zakreslený	k2eAgFnPc1d1	zakreslená
do	do	k7c2	do
grafu	graf	k1gInSc2	graf
podle	podle	k7c2	podle
spektrální	spektrální	k2eAgFnSc2d1	spektrální
třídy	třída	k1gFnSc2	třída
a	a	k8xC	a
absolutní	absolutní	k2eAgFnSc2d1	absolutní
velikosti	velikost	k1gFnSc2	velikost
(	(	kIx(	(
<g/>
svítivosti	svítivost	k1gFnSc2	svítivost
<g/>
)	)	kIx)	)
dávají	dávat	k5eAaImIp3nP	dávat
tzv.	tzv.	kA	tzv.
Hertzsprungův-Russellův	Hertzsprungův-Russellův	k2eAgInSc4d1	Hertzsprungův-Russellův
diagram	diagram	k1gInSc4	diagram
<g/>
.	.	kIx.	.
</s>
<s>
Hertzsprungův-Russellův	Hertzsprungův-Russellův	k2eAgInSc1d1	Hertzsprungův-Russellův
diagram	diagram	k1gInSc1	diagram
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hvězdy	hvězda	k1gFnPc1	hvězda
se	se	k3xPyFc4	se
seskupují	seskupovat	k5eAaImIp3nP	seskupovat
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
větví	větev	k1gFnPc2	větev
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavní	hlavní	k2eAgFnPc1d1	hlavní
větve	větev	k1gFnPc1	větev
a	a	k8xC	a
větve	větev	k1gFnPc1	větev
obrů	obr	k1gMnPc2	obr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
diagramu	diagram	k1gInSc6	diagram
je	být	k5eAaImIp3nS	být
zřetelné	zřetelný	k2eAgNnSc1d1	zřetelné
dělení	dělení	k1gNnSc1	dělení
hvězd	hvězda	k1gFnPc2	hvězda
na	na	k7c6	na
<g/>
:	:	kIx,	:
nadobrů	nadobr	k1gMnPc2	nadobr
<g/>
,	,	kIx,	,
jasných	jasný	k2eAgMnPc2d1	jasný
obrů	obr	k1gMnPc2	obr
<g/>
,	,	kIx,	,
obrů	obr	k1gMnPc2	obr
<g/>
,	,	kIx,	,
podobrů	podobr	k1gMnPc2	podobr
<g/>
,	,	kIx,	,
hvězd	hvězda	k1gFnPc2	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
(	(	kIx(	(
<g/>
nejpočetnější	početní	k2eAgFnSc1d3	nejpočetnější
skupina	skupina	k1gFnSc1	skupina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
trpaslíků	trpaslík	k1gMnPc2	trpaslík
a	a	k8xC	a
podtrpaslíků	podtrpaslík	k1gMnPc2	podtrpaslík
<g/>
.	.	kIx.	.
</s>
<s>
Hertzsprungův-Russellův	Hertzsprungův-Russellův	k2eAgInSc1d1	Hertzsprungův-Russellův
diagram	diagram	k1gInSc1	diagram
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
i	i	k9	i
vývojovou	vývojový	k2eAgFnSc4d1	vývojová
cestu	cesta	k1gFnSc4	cesta
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Svítivost	svítivost	k1gFnSc1	svítivost
nejenže	nejenže	k6eAd1	nejenže
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
poloměru	poloměr	k1gInSc3	poloměr
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
i	i	k9	i
na	na	k7c4	na
jejich	jejich	k3xOp3gFnPc4	jejich
spektrální	spektrální	k2eAgFnPc4d1	spektrální
čáry	čára	k1gFnPc4	čára
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
hvězd	hvězda	k1gFnPc2	hvězda
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c4	na
hlavní	hlavní	k2eAgFnPc4d1	hlavní
posloupnosti	posloupnost	k1gFnPc4	posloupnost
tvořené	tvořený	k2eAgFnPc1d1	tvořená
hvězdami	hvězda	k1gFnPc7	hvězda
spalujícími	spalující	k2eAgFnPc7d1	spalující
vodík	vodík	k1gInSc4	vodík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
grafu	graf	k1gInSc6	graf
zobrazujícím	zobrazující	k2eAgInSc6d1	zobrazující
jejich	jejich	k3xOp3gNnSc2	jejich
absolutní	absolutní	k2eAgFnSc4d1	absolutní
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
velikost	velikost	k1gFnSc4	velikost
a	a	k8xC	a
spektrální	spektrální	k2eAgInSc1d1	spektrální
typ	typ	k1gInSc1	typ
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
úzkém	úzký	k2eAgInSc6d1	úzký
pásu	pás	k1gInSc6	pás
podél	podél	k6eAd1	podél
mírně	mírně	k6eAd1	mírně
zakřivené	zakřivený	k2eAgFnPc1d1	zakřivená
linie	linie	k1gFnPc1	linie
směřující	směřující	k2eAgFnPc1d1	směřující
z	z	k7c2	z
levého	levý	k2eAgInSc2d1	levý
horního	horní	k2eAgInSc2d1	horní
do	do	k7c2	do
pravého	pravý	k2eAgInSc2d1	pravý
spodního	spodní	k2eAgInSc2d1	spodní
rohu	roh	k1gInSc2	roh
diagramu	diagram	k1gInSc2	diagram
<g/>
.	.	kIx.	.
</s>
<s>
Slunce	slunce	k1gNnSc1	slunce
je	být	k5eAaImIp3nS	být
hvězdou	hvězda	k1gFnSc7	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
typu	typ	k1gInSc2	typ
G	G	kA	G
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
žlutou	žlutý	k2eAgFnSc7d1	žlutá
hvězdou	hvězda	k1gFnSc7	hvězda
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
teplotou	teplota	k1gFnSc7	teplota
a	a	k8xC	a
běžnou	běžný	k2eAgFnSc7d1	běžná
velikostí	velikost	k1gFnSc7	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Přídavné	přídavný	k2eAgNnSc1d1	přídavné
kategorizování	kategorizování	k1gNnSc1	kategorizování
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
malého	malý	k2eAgNnSc2d1	malé
písmene	písmeno	k1gNnSc2	písmeno
za	za	k7c7	za
spektrálním	spektrální	k2eAgInSc7d1	spektrální
typem	typ	k1gInSc7	typ
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
speciální	speciální	k2eAgFnSc2d1	speciální
vlastnosti	vlastnost	k1gFnSc2	vlastnost
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
např.	např.	kA	např.
e	e	k0	e
znamená	znamenat	k5eAaImIp3nS	znamenat
přítomnost	přítomnost	k1gFnSc4	přítomnost
emisní	emisní	k2eAgFnSc2d1	emisní
čáry	čára	k1gFnSc2	čára
<g/>
,	,	kIx,	,
m	m	kA	m
znamená	znamenat	k5eAaImIp3nS	znamenat
neobvyklou	obvyklý	k2eNgFnSc4d1	neobvyklá
úroveň	úroveň	k1gFnSc4	úroveň
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
var	var	k1gInSc1	var
znamená	znamenat	k5eAaImIp3nS	znamenat
proměnnou	proměnný	k2eAgFnSc4d1	proměnná
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
.	.	kIx.	.
plazmové	plazmový	k2eAgFnPc1d1	plazmová
hvězdy	hvězda	k1gFnPc1	hvězda
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgNnPc6	který
probíhají	probíhat	k5eAaImIp3nP	probíhat
termojaderné	termojaderný	k2eAgFnPc4d1	termojaderná
reakce	reakce	k1gFnPc4	reakce
a	a	k8xC	a
průběžně	průběžně	k6eAd1	průběžně
tak	tak	k9	tak
vzniká	vznikat	k5eAaImIp3nS	vznikat
nové	nový	k2eAgNnSc4d1	nové
vlastní	vlastní	k2eAgNnSc4d1	vlastní
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
sem	sem	k6eAd1	sem
obři	obr	k1gMnPc1	obr
<g/>
,	,	kIx,	,
veleobři	veleobr	k1gMnPc1	veleobr
a	a	k8xC	a
hvězdy	hvězda	k1gFnPc1	hvězda
hlavní	hlavní	k2eAgFnSc2d1	hlavní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
hvězdy	hvězda	k1gFnPc1	hvězda
viditelné	viditelný	k2eAgFnPc1d1	viditelná
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
jsou	být	k5eAaImIp3nP	být
plazmové	plazmový	k2eAgFnPc1d1	plazmová
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
.	.	kIx.	.
degenerované	degenerovaný	k2eAgFnPc1d1	degenerovaná
hvězdy	hvězda	k1gFnPc1	hvězda
-	-	kIx~	-
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
již	již	k6eAd1	již
neprobíhají	probíhat	k5eNaImIp3nP	probíhat
termojaderné	termojaderný	k2eAgFnPc4d1	termojaderná
reakce	reakce	k1gFnPc4	reakce
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
hmota	hmota	k1gFnSc1	hmota
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
degenerovaném	degenerovaný	k2eAgInSc6d1	degenerovaný
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nP	patřit
sem	sem	k6eAd1	sem
bílí	bílý	k2eAgMnPc1d1	bílý
trpaslíci	trpaslík	k1gMnPc1	trpaslík
<g/>
,	,	kIx,	,
neutronové	neutronový	k2eAgFnPc1d1	neutronová
hvězdy	hvězda	k1gFnPc1	hvězda
a	a	k8xC	a
černé	černý	k2eAgFnPc1d1	černá
díry	díra	k1gFnPc1	díra
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
hvězdy	hvězda	k1gFnPc1	hvězda
září	zářit	k5eAaImIp3nP	zářit
pouze	pouze	k6eAd1	pouze
ze	z	k7c2	z
zásob	zásoba	k1gFnPc2	zásoba
energie	energie	k1gFnSc2	energie
nastřádaných	nastřádaný	k2eAgFnPc2d1	nastřádaná
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
tepla	teplo	k1gNnSc2	teplo
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
nezáří	zářit	k5eNaImIp3nP	zářit
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
některých	některý	k3yIgFnPc2	některý
hvězd	hvězda	k1gFnPc2	hvězda
byla	být	k5eAaImAgFnS	být
dokázána	dokázán	k2eAgFnSc1d1	dokázána
existence	existence	k1gFnSc1	existence
temných	temný	k2eAgMnPc2d1	temný
průvodců	průvodce	k1gMnPc2	průvodce
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
nedají	dát	k5eNaPmIp3nP	dát
pozorovat	pozorovat	k5eAaImF	pozorovat
dalekohledy	dalekohled	k1gInPc4	dalekohled
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
ani	ani	k8xC	ani
spektroskopickými	spektroskopický	k2eAgFnPc7d1	spektroskopická
dvojhvězdami	dvojhvězda	k1gFnPc7	dvojhvězda
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
tito	tento	k3xDgMnPc1	tento
neviditelní	viditelný	k2eNgMnPc1d1	Neviditelný
společníci	společník	k1gMnPc1	společník
gravitačně	gravitačně	k6eAd1	gravitačně
působí	působit	k5eAaImIp3nP	působit
na	na	k7c4	na
hvězdu	hvězda	k1gFnSc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
jimi	on	k3xPp3gNnPc7	on
být	být	k5eAaImF	být
bývalé	bývalý	k2eAgFnPc4d1	bývalá
plazmové	plazmový	k2eAgFnPc4d1	plazmová
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
malými	malý	k2eAgFnPc7d1	malá
degenerovanými	degenerovaný	k2eAgFnPc7d1	degenerovaná
hvězdami	hvězda	k1gFnPc7	hvězda
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
nepozorovatelné	pozorovatelný	k2eNgFnPc1d1	nepozorovatelná
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
bílé	bílý	k2eAgMnPc4d1	bílý
trpaslíky	trpaslík	k1gMnPc4	trpaslík
<g/>
,	,	kIx,	,
neutronové	neutronový	k2eAgMnPc4d1	neutronový
hvězdy	hvězda	k1gFnSc2	hvězda
nebo	nebo	k8xC	nebo
černé	černý	k2eAgFnSc2d1	černá
díry	díra	k1gFnSc2	díra
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
hvězdy	hvězda	k1gFnPc1	hvězda
mají	mít	k5eAaImIp3nP	mít
zase	zase	k9	zase
mnohem	mnohem	k6eAd1	mnohem
méně	málo	k6eAd2	málo
hmotné	hmotný	k2eAgMnPc4d1	hmotný
společníky	společník	k1gMnPc4	společník
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
hnědí	hnědý	k2eAgMnPc1d1	hnědý
trpaslíci	trpaslík	k1gMnPc1	trpaslík
nebo	nebo	k8xC	nebo
planety	planeta	k1gFnPc1	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	stoletý	k2eAgMnPc1d1	stoletý
astronomové	astronom	k1gMnPc1	astronom
nevěděli	vědět	k5eNaImAgMnP	vědět
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
i	i	k9	i
jiné	jiný	k2eAgFnPc1d1	jiná
hvězdy	hvězda	k1gFnPc1	hvězda
mají	mít	k5eAaImIp3nP	mít
planety	planeta	k1gFnPc4	planeta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
desetiletí	desetiletí	k1gNnSc6	desetiletí
se	se	k3xPyFc4	se
však	však	k9	však
začalo	začít	k5eAaPmAgNnS	začít
objevovat	objevovat	k5eAaImF	objevovat
množství	množství	k1gNnSc1	množství
planet	planeta	k1gFnPc2	planeta
obíhajících	obíhající	k2eAgFnPc2d1	obíhající
kolem	kolem	k7c2	kolem
jiných	jiný	k2eAgFnPc2d1	jiná
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jich	on	k3xPp3gMnPc2	on
známe	znát	k5eAaImIp1nP	znát
již	již	k6eAd1	již
více	hodně	k6eAd2	hodně
než	než	k8xS	než
900	[number]	k4	900
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
dávali	dávat	k5eAaImAgMnP	dávat
lidé	člověk	k1gMnPc1	člověk
hvězdám	hvězda	k1gFnPc3	hvězda
různá	různý	k2eAgNnPc4d1	různé
jména	jméno	k1gNnPc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
s	s	k7c7	s
některými	některý	k3yIgNnPc7	některý
souhvězdími	souhvězdí	k1gNnPc7	souhvězdí
a	a	k8xC	a
samotným	samotný	k2eAgNnSc7d1	samotné
Sluncem	slunce	k1gNnSc7	slunce
<g/>
,	,	kIx,	,
i	i	k9	i
s	s	k7c7	s
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
hvězdami	hvězda	k1gFnPc7	hvězda
se	se	k3xPyFc4	se
spojovala	spojovat	k5eAaImAgFnS	spojovat
mytologie	mytologie	k1gFnSc1	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Jména	jméno	k1gNnPc1	jméno
hvězd	hvězda	k1gFnPc2	hvězda
popisovala	popisovat	k5eAaImAgNnP	popisovat
jejich	jejich	k3xOp3gNnSc6	jejich
vzhled	vzhled	k1gInSc4	vzhled
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Rutilicus	Rutilicus	k1gInSc1	Rutilicus
-	-	kIx~	-
nažloutlý	nažloutlý	k2eAgMnSc1d1	nažloutlý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
(	(	kIx(	(
<g/>
Phacd	Phacd	k1gInSc1	Phacd
-	-	kIx~	-
stehno	stehno	k1gNnSc1	stehno
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnSc4	jejich
roli	role	k1gFnSc4	role
v	v	k7c4	v
mytologii	mytologie	k1gFnSc4	mytologie
(	(	kIx(	(
<g/>
Alcyone	Alcyon	k1gMnSc5	Alcyon
-	-	kIx~	-
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
mýtických	mýtický	k2eAgFnPc2d1	mýtická
Plejád	Plejáda	k1gFnPc2	Plejáda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
používaná	používaný	k2eAgNnPc1d1	používané
jména	jméno	k1gNnPc1	jméno
pocházejí	pocházet	k5eAaImIp3nP	pocházet
většinou	většina	k1gFnSc7	většina
ze	z	k7c2	z
staroarabštiny	staroarabština	k1gFnSc2	staroarabština
<g/>
,	,	kIx,	,
řečtiny	řečtina	k1gFnSc2	řečtina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
každá	každý	k3xTgFnSc1	každý
šestá	šestý	k4xOgFnSc1	šestý
hvězda	hvězda	k1gFnSc1	hvězda
viditelná	viditelný	k2eAgFnSc1d1	viditelná
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgNnSc4	svůj
vlastní	vlastní	k2eAgNnSc4d1	vlastní
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jen	jen	k9	jen
kolem	kolem	k7c2	kolem
100	[number]	k4	100
jmen	jméno	k1gNnPc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Novodobé	novodobý	k2eAgNnSc1d1	novodobé
pojmenování	pojmenování	k1gNnSc1	pojmenování
hvězd	hvězda	k1gFnPc2	hvězda
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
je	být	k5eAaImIp3nS	být
vzácné	vzácný	k2eAgNnSc1d1	vzácné
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
moderních	moderní	k2eAgInPc2d1	moderní
názvů	název	k1gInPc2	název
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Regor	Regor	k1gInSc4	Regor
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
několik	několik	k4yIc1	několik
hvězd	hvězda	k1gFnPc2	hvězda
nese	nést	k5eAaImIp3nS	nést
jména	jméno	k1gNnSc2	jméno
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Barnardova	Barnardův	k2eAgFnSc1d1	Barnardova
šipka	šipka	k1gFnSc1	šipka
či	či	k8xC	či
Sualocin	Sualocin	k1gInSc1	Sualocin
a	a	k8xC	a
Rotanev	Rotanev	k1gFnSc1	Rotanev
(	(	kIx(	(
<g/>
α	α	k?	α
a	a	k8xC	a
β	β	k?	β
Delfína	Delfín	k1gMnSc2	Delfín
které	který	k3yRgMnPc4	který
přečtené	přečtený	k2eAgMnPc4d1	přečtený
pozpátku	pozpátku	k6eAd1	pozpátku
dají	dát	k5eAaPmIp3nP	dát
jméno	jméno	k1gNnSc4	jméno
Nicolaus	Nicolaus	k1gMnSc1	Nicolaus
Venator	Venator	k1gMnSc1	Venator
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
latinskou	latinský	k2eAgFnSc4d1	Latinská
podobu	podoba	k1gFnSc4	podoba
jména	jméno	k1gNnSc2	jméno
asistenta	asistent	k1gMnSc2	asistent
astronoma	astronom	k1gMnSc2	astronom
Giuseppe	Giusepp	k1gMnSc5	Giusepp
Piazziho	Piazziha	k1gMnSc5	Piazziha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Koncept	koncept	k1gInSc1	koncept
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
existoval	existovat	k5eAaImAgInS	existovat
už	už	k6eAd1	už
během	během	k7c2	během
Babylonské	babylonský	k2eAgFnSc2d1	Babylonská
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Starověcí	starověký	k2eAgMnPc1d1	starověký
astronomové	astronom	k1gMnPc1	astronom
si	se	k3xPyFc3	se
všimli	všimnout	k5eAaPmAgMnP	všimnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
hvězdy	hvězda	k1gFnPc1	hvězda
tvoří	tvořit	k5eAaImIp3nP	tvořit
výrazné	výrazný	k2eAgInPc4d1	výrazný
obrazce	obrazec	k1gInPc4	obrazec
a	a	k8xC	a
spojili	spojit	k5eAaPmAgMnP	spojit
je	on	k3xPp3gInPc4	on
s	s	k7c7	s
přírodními	přírodní	k2eAgInPc7d1	přírodní
úkazy	úkaz	k1gInPc7	úkaz
a	a	k8xC	a
mytologií	mytologie	k1gFnSc7	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Dvanáct	dvanáct	k4xCc1	dvanáct
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
formací	formace	k1gFnPc2	formace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
leží	ležet	k5eAaImIp3nP	ležet
podél	podél	k7c2	podél
roviny	rovina	k1gFnSc2	rovina
ekliptiky	ekliptika	k1gFnSc2	ekliptika
<g/>
,	,	kIx,	,
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
základ	základ	k1gInSc4	základ
astrologie	astrologie	k1gFnSc2	astrologie
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
objevem	objev	k1gInSc7	objev
dalekohledu	dalekohled	k1gInSc2	dalekohled
se	se	k3xPyFc4	se
zvyšovalo	zvyšovat	k5eAaImAgNnS	zvyšovat
množství	množství	k1gNnSc1	množství
pozorovatelných	pozorovatelný	k2eAgFnPc2d1	pozorovatelná
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
vytvářet	vytvářet	k5eAaImF	vytvářet
a	a	k8xC	a
používat	používat	k5eAaImF	používat
různé	různý	k2eAgInPc4d1	různý
hvězdné	hvězdný	k2eAgInPc4d1	hvězdný
katalogy	katalog	k1gInPc4	katalog
<g/>
.	.	kIx.	.
</s>
<s>
Autorem	autor	k1gMnSc7	autor
prvního	první	k4xOgInSc2	první
hvězdného	hvězdný	k2eAgInSc2d1	hvězdný
katalogu	katalog	k1gInSc2	katalog
byl	být	k5eAaImAgInS	být
Hipparchos	Hipparchos	k1gMnSc1	Hipparchos
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zavedl	zavést	k5eAaPmAgInS	zavést
i	i	k9	i
hvězdnou	hvězdný	k2eAgFnSc4d1	hvězdná
velikost	velikost	k1gFnSc4	velikost
(	(	kIx(	(
<g/>
magnitudu	magnituda	k1gFnSc4	magnituda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1600	[number]	k4	1600
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
názvy	název	k1gInPc1	název
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
používat	používat	k5eAaImF	používat
k	k	k7c3	k
pojmenování	pojmenování	k1gNnSc3	pojmenování
nebeských	nebeský	k2eAgNnPc2d1	nebeské
těles	těleso	k1gNnPc2	těleso
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
oblasti	oblast	k1gFnSc6	oblast
oblohy	obloha	k1gFnSc2	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nejjasnější	jasný	k2eAgFnPc4d3	nejjasnější
hvězdy	hvězda	k1gFnPc4	hvězda
oblohy	obloha	k1gFnSc2	obloha
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
používá	používat	k5eAaImIp3nS	používat
Bayerovo	Bayerův	k2eAgNnSc1d1	Bayerovo
značení	značení	k1gNnSc1	značení
<g/>
.	.	kIx.	.
</s>
<s>
Sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
písmene	písmeno	k1gNnSc2	písmeno
za	za	k7c7	za
kterým	který	k3yQgNnSc7	který
následuje	následovat	k5eAaImIp3nS	následovat
genitiv	genitiv	k1gInSc4	genitiv
latinského	latinský	k2eAgInSc2d1	latinský
názvu	název	k1gInSc2	název
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
hvězda	hvězda	k1gFnSc1	hvězda
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
jsou	být	k5eAaImIp3nP	být
označeny	označit	k5eAaPmNgFnP	označit
od	od	k7c2	od
alfy	alfa	k1gFnSc2	alfa
až	až	k9	až
po	po	k7c4	po
omegu	omega	k1gFnSc4	omega
obvykle	obvykle	k6eAd1	obvykle
(	(	kIx(	(
<g/>
ne	ne	k9	ne
však	však	k9	však
vždy	vždy	k6eAd1	vždy
<g/>
)	)	kIx)	)
od	od	k7c2	od
nejjasnější	jasný	k2eAgFnSc2d3	nejjasnější
(	(	kIx(	(
<g/>
alfa	alfa	k1gNnSc7	alfa
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
nejslabší	slabý	k2eAgNnSc4d3	nejslabší
(	(	kIx(	(
<g/>
omega	omega	k1gNnSc4	omega
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
jasná	jasný	k2eAgFnSc1d1	jasná
hvězda	hvězda	k1gFnSc1	hvězda
viditelná	viditelný	k2eAgFnSc1d1	viditelná
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
nemá	mít	k5eNaImIp3nS	mít
jméno	jméno	k1gNnSc4	jméno
ani	ani	k8xC	ani
Bayerovo	Bayerův	k2eAgNnSc4d1	Bayerovo
značení	značení	k1gNnSc4	značení
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
Flamsteedovo	Flamsteedův	k2eAgNnSc4d1	Flamsteedovo
značení	značení	k1gNnSc4	značení
<g/>
.	.	kIx.	.
</s>
<s>
Použil	použít	k5eAaPmAgMnS	použít
jej	on	k3xPp3gMnSc4	on
John	John	k1gMnSc1	John
Flamsteed	Flamsteed	k1gMnSc1	Flamsteed
v	v	k7c6	v
katalogu	katalog	k1gInSc6	katalog
hvězd	hvězda	k1gFnPc2	hvězda
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Historia	Historium	k1gNnSc2	Historium
coelestis	coelestis	k1gFnSc2	coelestis
Britannica	Britannic	k1gInSc2	Britannic
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
podobné	podobný	k2eAgNnSc1d1	podobné
Bayerovu	Bayerův	k2eAgFnSc4d1	Bayerova
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
,	,	kIx,	,
že	že	k8xS	že
namísto	namísto	k7c2	namísto
písmena	písmeno	k1gNnSc2	písmeno
řecké	řecký	k2eAgFnSc2d1	řecká
abecedy	abeceda	k1gFnSc2	abeceda
je	být	k5eAaImIp3nS	být
arabská	arabský	k2eAgFnSc1d1	arabská
číslice	číslice	k1gFnSc1	číslice
<g/>
.	.	kIx.	.
</s>
<s>
Kritériem	kritérion	k1gNnSc7	kritérion
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yIgMnSc2	který
jsou	být	k5eAaImIp3nP	být
hvězdy	hvězda	k1gFnPc1	hvězda
s	s	k7c7	s
Flamsteedovym	Flamsteedovym	k1gInSc1	Flamsteedovym
označením	označení	k1gNnSc7	označení
seřazeny	seřadit	k5eAaPmNgFnP	seřadit
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
jasnost	jasnost	k1gFnSc1	jasnost
ale	ale	k8xC	ale
rektascenze	rektascenze	k1gFnSc1	rektascenze
<g/>
.	.	kIx.	.
</s>
<s>
Slabší	slabý	k2eAgFnSc2d2	slabší
hvězdy	hvězda	k1gFnSc2	hvězda
nepozorovatelné	pozorovatelný	k2eNgFnSc2d1	nepozorovatelná
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
mají	mít	k5eAaImIp3nP	mít
přidělená	přidělený	k2eAgNnPc4d1	přidělené
čísla	číslo	k1gNnPc4	číslo
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
katalozích	katalog	k1gInPc6	katalog
např.	např.	kA	např.
SAO	SAO	kA	SAO
<g/>
,	,	kIx,	,
HD	HD	kA	HD
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Čísla	číslo	k1gNnPc1	číslo
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
katalozích	katalog	k1gInPc6	katalog
mají	mít	k5eAaImIp3nP	mít
sice	sice	k8xC	sice
přiděleny	přidělen	k2eAgFnPc1d1	přidělena
všechny	všechen	k3xTgFnPc1	všechen
hvězdy	hvězda	k1gFnPc1	hvězda
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
limitní	limitní	k2eAgFnSc2d1	limitní
magnitudy	magnituda	k1gFnSc2	magnituda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jasných	jasný	k2eAgFnPc2d1	jasná
hvězd	hvězda	k1gFnPc2	hvězda
se	se	k3xPyFc4	se
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
většinou	většinou	k6eAd1	většinou
používá	používat	k5eAaImIp3nS	používat
vlastní	vlastní	k2eAgNnSc4d1	vlastní
jméno	jméno	k1gNnSc4	jméno
či	či	k8xC	či
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
výše	vysoce	k6eAd2	vysoce
uvedených	uvedený	k2eAgNnPc2d1	uvedené
označení	označení	k1gNnPc2	označení
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
princip	princip	k1gInSc1	princip
označení	označení	k1gNnSc2	označení
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
hvězdy	hvězda	k1gFnPc4	hvězda
ve	v	k7c6	v
vícenásobných	vícenásobný	k2eAgInPc6d1	vícenásobný
systémech	systém	k1gInPc6	systém
<g/>
.	.	kIx.	.
</s>
<s>
Nejhmotnější	hmotný	k2eAgFnSc1d3	nejhmotnější
složka	složka	k1gFnSc1	složka
systému	systém	k1gInSc2	systém
má	mít	k5eAaImIp3nS	mít
za	za	k7c7	za
svým	svůj	k3xOyFgNnSc7	svůj
katalogovým	katalogový	k2eAgNnSc7d1	Katalogové
označením	označení	k1gNnSc7	označení
písmeno	písmeno	k1gNnSc1	písmeno
A	A	kA	A
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
hmotná	hmotný	k2eAgFnSc1d1	hmotná
B	B	kA	B
a	a	k8xC	a
tak	tak	k6eAd1	tak
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Alfa	alfa	k1gFnSc1	alfa
Centauri	Centaur	k1gFnSc2	Centaur
A	a	k9	a
je	být	k5eAaImIp3nS	být
nejhmotnější	hmotný	k2eAgFnSc1d3	nejhmotnější
hvězda	hvězda	k1gFnSc1	hvězda
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
trojhvězdného	trojhvězdný	k2eAgInSc2d1	trojhvězdný
<g/>
)	)	kIx)	)
Alfa	alfa	k1gFnSc1	alfa
Centauri	Centaur	k1gFnSc2	Centaur
<g/>
,	,	kIx,	,
Gliese	Gliese	k1gFnSc1	Gliese
165	[number]	k4	165
B	B	kA	B
je	být	k5eAaImIp3nS	být
druhá	druhý	k4xOgFnSc1	druhý
nejhmotnější	hmotný	k2eAgFnSc1d3	nejhmotnější
hvězda	hvězda	k1gFnSc1	hvězda
systému	systém	k1gInSc2	systém
Gliese	Gliese	k1gFnSc2	Gliese
165	[number]	k4	165
<g/>
,	,	kIx,	,
Polárka	Polárka	k1gFnSc1	Polárka
C	C	kA	C
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc1	třetí
nejhmotnější	hmotný	k2eAgFnSc1d3	nejhmotnější
hvězda	hvězda	k1gFnSc1	hvězda
systému	systém	k1gInSc2	systém
Polárka	Polárka	k1gFnSc1	Polárka
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc4d1	jediná
mezinárodně	mezinárodně	k6eAd1	mezinárodně
uznávanou	uznávaný	k2eAgFnSc4d1	uznávaná
autoritou	autorita	k1gFnSc7	autorita
pro	pro	k7c4	pro
pojmenovávání	pojmenovávání	k1gNnPc4	pojmenovávání
nebeských	nebeský	k2eAgNnPc2d1	nebeské
těles	těleso	k1gNnPc2	těleso
je	být	k5eAaImIp3nS	být
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
astronomická	astronomický	k2eAgFnSc1d1	astronomická
unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g/>
IAU	IAU	kA	IAU
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
soukromých	soukromý	k2eAgFnPc2d1	soukromá
společností	společnost	k1gFnPc2	společnost
přesto	přesto	k8xC	přesto
prodává	prodávat	k5eAaImIp3nS	prodávat
jména	jméno	k1gNnPc4	jméno
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
IAU	IAU	kA	IAU
se	se	k3xPyFc4	se
distancuje	distancovat	k5eAaBmIp3nS	distancovat
od	od	k7c2	od
těchto	tento	k3xDgFnPc2	tento
komerčních	komerční	k2eAgFnPc2d1	komerční
praktik	praktika	k1gFnPc2	praktika
a	a	k8xC	a
takto	takto	k6eAd1	takto
přidělená	přidělený	k2eAgNnPc4d1	přidělené
jména	jméno	k1gNnPc4	jméno
neuznává	uznávat	k5eNaImIp3nS	uznávat
ani	ani	k8xC	ani
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jasné	jasný	k2eAgFnPc4d1	jasná
noci	noc	k1gFnPc4	noc
můžeme	moct	k5eAaImIp1nP	moct
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
vidět	vidět	k5eAaImF	vidět
při	při	k7c6	při
ideálním	ideální	k2eAgInSc6d1	ideální
rovném	rovný	k2eAgInSc6d1	rovný
horizontu	horizont	k1gInSc6	horizont
asi	asi	k9	asi
3	[number]	k4	3
000	[number]	k4	000
-	-	kIx~	-
5	[number]	k4	5
000	[number]	k4	000
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
malým	malý	k2eAgInSc7d1	malý
dalekohledem	dalekohled	k1gInSc7	dalekohled
jich	on	k3xPp3gFnPc2	on
uvidíme	uvidět	k5eAaPmIp1nP	uvidět
mnohem	mnohem	k6eAd1	mnohem
víc	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
hvězdy	hvězda	k1gFnPc1	hvězda
viditelné	viditelný	k2eAgFnSc2d1	viditelná
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
nebo	nebo	k8xC	nebo
malým	malý	k2eAgInSc7d1	malý
dalekohledem	dalekohled	k1gInSc7	dalekohled
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
naší	náš	k3xOp1gFnSc2	náš
Galaxie	galaxie	k1gFnSc2	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
samostatné	samostatný	k2eAgFnPc1d1	samostatná
hvězdy	hvězda	k1gFnPc1	hvězda
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
cizích	cizí	k2eAgFnPc6d1	cizí
galaxiích	galaxie	k1gFnPc6	galaxie
pozorovány	pozorován	k2eAgInPc1d1	pozorován
až	až	k9	až
ve	v	k7c4	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Polohu	poloha	k1gFnSc4	poloha
hvězd	hvězda	k1gFnPc2	hvězda
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
určujeme	určovat	k5eAaImIp1nP	určovat
souřadnicemi	souřadnice	k1gFnPc7	souřadnice
<g/>
.	.	kIx.	.
</s>
<s>
Vzhled	vzhled	k1gInSc1	vzhled
hvězdy	hvězda	k1gFnSc2	hvězda
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
bodový	bodový	k2eAgInSc1d1	bodový
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
největších	veliký	k2eAgInPc6d3	veliký
dalekohledech	dalekohled	k1gInPc6	dalekohled
(	(	kIx(	(
<g/>
výjimkou	výjimka	k1gFnSc7	výjimka
se	se	k3xPyFc4	se
v	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
minulosti	minulost	k1gFnSc6	minulost
staly	stát	k5eAaPmAgFnP	stát
hvězdy	hvězda	k1gFnPc1	hvězda
Betelgeuze	Betelgeuze	k1gFnSc1	Betelgeuze
a	a	k8xC	a
Mira	Mira	k1gFnSc1	Mira
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přechodem	přechod	k1gInSc7	přechod
světla	světlo	k1gNnSc2	světlo
hvězdy	hvězda	k1gFnSc2	hvězda
atmosférou	atmosféra	k1gFnSc7	atmosféra
Země	zem	k1gFnSc2	zem
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
scintilaci	scintilace	k1gFnSc3	scintilace
(	(	kIx(	(
<g/>
třpyt	třpyt	k1gInSc1	třpyt
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
refrakci	refrakce	k1gFnSc3	refrakce
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
scintilaci	scintilace	k1gFnSc3	scintilace
můžeme	moct	k5eAaImIp1nP	moct
bezpečně	bezpečně	k6eAd1	bezpečně
rozeznat	rozeznat	k5eAaPmF	rozeznat
hvězdy	hvězda	k1gFnPc4	hvězda
od	od	k7c2	od
planet	planeta	k1gFnPc2	planeta
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
ke	k	k7c3	k
scintilaci	scintilace	k1gFnSc3	scintilace
nedochází	docházet	k5eNaImIp3nS	docházet
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
barva	barva	k1gFnSc1	barva
hvězdy	hvězda	k1gFnSc2	hvězda
vypovídá	vypovídat	k5eAaPmIp3nS	vypovídat
o	o	k7c6	o
její	její	k3xOp3gFnSc6	její
teplotě	teplota	k1gFnSc6	teplota
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
její	její	k3xOp3gInSc1	její
odstín	odstín	k1gInSc1	odstín
způsoben	způsobit	k5eAaPmNgInS	způsobit
nějakým	nějaký	k3yIgInSc7	nějaký
optickým	optický	k2eAgInSc7d1	optický
klamem	klam	k1gInSc7	klam
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
právě	právě	k9	právě
atmosférou	atmosféra	k1gFnSc7	atmosféra
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
lidským	lidský	k2eAgNnSc7d1	lidské
okem	oke	k1gNnSc7	oke
<g/>
,	,	kIx,	,
barevnou	barevný	k2eAgFnSc7d1	barevná
vadou	vada	k1gFnSc7	vada
čočky	čočka	k1gFnSc2	čočka
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Poblikávání	poblikávání	k1gNnSc1	poblikávání
hvězd	hvězda	k1gFnPc2	hvězda
však	však	k9	však
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
jen	jen	k9	jen
atmosférou	atmosféra	k1gFnSc7	atmosféra
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
proměnné	proměnný	k2eAgFnPc4d1	proměnná
hvězdy	hvězda	k1gFnPc4	hvězda
či	či	k8xC	či
zákrytové	zákrytový	k2eAgFnPc4d1	zákrytová
dvouhvězdy	dvouhvězda	k1gFnPc4	dvouhvězda
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
jasnost	jasnost	k1gFnSc1	jasnost
s	s	k7c7	s
časem	čas	k1gInSc7	čas
mění	měnit	k5eAaImIp3nS	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
mají	mít	k5eAaImIp3nP	mít
různou	různý	k2eAgFnSc4d1	různá
jasnost	jasnost	k1gFnSc4	jasnost
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
zářivosti	zářivost	k1gFnSc6	zářivost
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc2	její
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
na	na	k7c6	na
vlivu	vliv	k1gInSc6	vliv
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
kterou	který	k3yRgFnSc4	který
její	její	k3xOp3gNnSc4	její
světlo	světlo	k1gNnSc4	světlo
prochází	procházet	k5eAaImIp3nS	procházet
<g/>
.	.	kIx.	.
</s>
<s>
Různá	různý	k2eAgFnSc1d1	různá
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
hvězd	hvězda	k1gFnPc2	hvězda
od	od	k7c2	od
nás	my	k3xPp1nPc2	my
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
hvězdy	hvězda	k1gFnPc1	hvězda
jsou	být	k5eAaImIp3nP	být
při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
jasnější	jasný	k2eAgMnSc1d2	jasnější
(	(	kIx(	(
<g/>
Vega	Vega	k1gMnSc1	Vega
<g/>
,	,	kIx,	,
Sirius	Sirius	k1gMnSc1	Sirius
<g/>
,	,	kIx,	,
Toliman	Toliman	k1gMnSc1	Toliman
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
než	než	k8xS	než
jiné	jiný	k2eAgFnPc1d1	jiná
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
mnohem	mnohem	k6eAd1	mnohem
zářivější	zářivý	k2eAgFnPc1d2	zářivější
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
(	(	kIx(	(
<g/>
Rigel	Rigel	k1gInSc1	Rigel
<g/>
,	,	kIx,	,
Antares	Antares	k1gInSc1	Antares
<g/>
,	,	kIx,	,
Polárka	Polárka	k1gFnSc1	Polárka
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jasnost	jasnost	k1gFnSc4	jasnost
hvězd	hvězda	k1gFnPc2	hvězda
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
určujeme	určovat	k5eAaImIp1nP	určovat
tzv.	tzv.	kA	tzv.
vizuálními	vizuální	k2eAgFnPc7d1	vizuální
magnitudami	magnituda	k1gFnPc7	magnituda
<g/>
.	.	kIx.	.
</s>
<s>
Čím	co	k3yQnSc7	co
má	mít	k5eAaImIp3nS	mít
vizuální	vizuální	k2eAgFnSc1d1	vizuální
magnituda	magnituda	k1gFnSc1	magnituda
menší	malý	k2eAgNnSc4d2	menší
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
hvězda	hvězda	k1gFnSc1	hvězda
jasnější	jasný	k2eAgFnSc1d2	jasnější
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
čím	co	k3yQnSc7	co
vyšší	vysoký	k2eAgNnSc1d2	vyšší
číslo	číslo	k1gNnSc1	číslo
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
nižší	nízký	k2eAgFnSc1d2	nižší
jas	jas	k1gInSc1	jas
<g/>
.	.	kIx.	.
</s>
<s>
Magnitudy	Magnitudy	k6eAd1	Magnitudy
nejjasnějších	jasný	k2eAgFnPc2d3	nejjasnější
hvězd	hvězda	k1gFnPc2	hvězda
mají	mít	k5eAaImIp3nP	mít
negativní	negativní	k2eAgFnPc4d1	negativní
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Nejjasnější	jasný	k2eAgFnSc1d3	nejjasnější
hvězda	hvězda	k1gFnSc1	hvězda
Sírius	Sírius	k1gInSc1	Sírius
má	mít	k5eAaImIp3nS	mít
magnitudu	magnitudu	k6eAd1	magnitudu
-1,43	-1,43	k4	-1,43
<g/>
,	,	kIx,	,
Vega	Vega	k1gFnSc1	Vega
0,03	[number]	k4	0,03
<g/>
,	,	kIx,	,
Polárka	Polárka	k1gFnSc1	Polárka
2,13	[number]	k4	2,13
a	a	k8xC	a
Slunce	slunce	k1gNnSc1	slunce
-26,7	-26,7	k4	-26,7
<g/>
.	.	kIx.	.
</s>
<s>
Nejslabší	slabý	k2eAgFnPc1d3	nejslabší
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
při	při	k7c6	při
dobrých	dobrý	k2eAgFnPc6d1	dobrá
pozorovacích	pozorovací	k2eAgFnPc6d1	pozorovací
podmínkách	podmínka	k1gFnPc6	podmínka
ještě	ještě	k6eAd1	ještě
vidíme	vidět	k5eAaImIp1nP	vidět
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
magnitudu	magnituda	k1gFnSc4	magnituda
6	[number]	k4	6
a	a	k8xC	a
nejslabší	slabý	k2eAgFnPc1d3	nejslabší
hvězdy	hvězda	k1gFnPc1	hvězda
zachycené	zachycený	k2eAgInPc4d1	zachycený
dalekohledy	dalekohled	k1gInPc4	dalekohled
na	na	k7c6	na
fotografických	fotografický	k2eAgFnPc6d1	fotografická
deskách	deska	k1gFnPc6	deska
mají	mít	k5eAaImIp3nP	mít
magnitudu	magnitudu	k6eAd1	magnitudu
až	až	k9	až
do	do	k7c2	do
30	[number]	k4	30
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
nejslabších	slabý	k2eAgFnPc2d3	nejslabší
pozorovatelných	pozorovatelný	k2eAgFnPc2d1	pozorovatelná
hvězd	hvězda	k1gFnPc2	hvězda
se	s	k7c7	s
zdokonalováním	zdokonalování	k1gNnSc7	zdokonalování
pozorovací	pozorovací	k2eAgFnSc2d1	pozorovací
techniky	technika	k1gFnSc2	technika
neustále	neustále	k6eAd1	neustále
posouvá	posouvat	k5eAaImIp3nS	posouvat
k	k	k7c3	k
vyšším	vysoký	k2eAgFnPc3d2	vyšší
magnitudám	magnituda	k1gFnPc3	magnituda
<g/>
.	.	kIx.	.
</s>
<s>
Skutečná	skutečný	k2eAgFnSc1d1	skutečná
nebo	nebo	k8xC	nebo
absolutní	absolutní	k2eAgFnSc1d1	absolutní
magnituda	magnituda	k1gFnSc1	magnituda
hvězdy	hvězda	k1gFnSc2	hvězda
přímo	přímo	k6eAd1	přímo
souvisí	souviset	k5eAaImIp3nS	souviset
se	se	k3xPyFc4	se
svítivostí	svítivost	k1gFnSc7	svítivost
hvězdy	hvězda	k1gFnPc1	hvězda
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
zdánlivou	zdánlivý	k2eAgFnSc4d1	zdánlivá
magnitudu	magnituda	k1gFnSc4	magnituda
hvězdy	hvězda	k1gFnSc2	hvězda
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
10	[number]	k4	10
parseků	parsec	k1gInPc2	parsec
(	(	kIx(	(
<g/>
32,6	[number]	k4	32,6
svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Stupnice	stupnice	k1gFnSc1	stupnice
obou	dva	k4xCgFnPc2	dva
magnitud	magnituda	k1gFnPc2	magnituda
<g/>
,	,	kIx,	,
zdánlivé	zdánlivý	k2eAgNnSc1d1	zdánlivé
a	a	k8xC	a
absolutní	absolutní	k2eAgNnSc1d1	absolutní
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
logaritmickými	logaritmický	k2eAgFnPc7d1	logaritmická
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
:	:	kIx,	:
rozdíl	rozdíl	k1gInSc1	rozdíl
magnitudy	magnituda	k1gFnSc2	magnituda
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
celé	celý	k2eAgNnSc4d1	celé
číslo	číslo	k1gNnSc4	číslo
představuje	představovat	k5eAaImIp3nS	představovat
přibližně	přibližně	k6eAd1	přibližně
2,5	[number]	k4	2,5
<g/>
násobný	násobný	k2eAgMnSc1d1	násobný
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
odmocnina	odmocnina	k1gFnSc1	odmocnina
ze	z	k7c2	z
100	[number]	k4	100
nebo	nebo	k8xC	nebo
cca	cca	kA	cca
2,512	[number]	k4	2,512
<g/>
)	)	kIx)	)
rozdíl	rozdíl	k1gInSc1	rozdíl
ve	v	k7c6	v
svítivosti	svítivost	k1gFnSc6	svítivost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hvězda	hvězda	k1gFnSc1	hvězda
první	první	k4xOgFnSc2	první
magnitudy	magnituda	k1gFnSc2	magnituda
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
1,00	[number]	k4	1,00
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
2,5	[number]	k4	2,5
<g/>
krát	krát	k6eAd1	krát
jasnější	jasný	k2eAgFnSc1d2	jasnější
než	než	k8xS	než
hvězda	hvězda	k1gFnSc1	hvězda
druhé	druhý	k4xOgFnSc2	druhý
magnitudy	magnituda	k1gFnSc2	magnituda
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
2,00	[number]	k4	2,00
<g/>
)	)	kIx)	)
a	a	k8xC	a
100	[number]	k4	100
<g/>
krát	krát	k6eAd1	krát
jasnější	jasný	k2eAgFnSc1d2	jasnější
než	než	k8xS	než
hvězda	hvězda	k1gFnSc1	hvězda
šesté	šestý	k4xOgFnSc2	šestý
magnitudy	magnituda	k1gFnSc2	magnituda
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
6,00	[number]	k4	6,00
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
v	v	k7c6	v
jasnosti	jasnost	k1gFnSc6	jasnost
(	(	kIx(	(
<g/>
Δ	Δ	k?	Δ
<g/>
)	)	kIx)	)
dvou	dva	k4xCgFnPc2	dva
hvězd	hvězda	k1gFnPc2	hvězda
vypočítáme	vypočítat	k5eAaPmIp1nP	vypočítat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
magnitudy	magnituda	k1gFnSc2	magnituda
méně	málo	k6eAd2	málo
jasné	jasný	k2eAgFnPc4d1	jasná
hvězdy	hvězda	k1gFnPc4	hvězda
(	(	kIx(	(
<g/>
mf	mf	k?	mf
<g/>
)	)	kIx)	)
odečteme	odečíst	k5eAaPmIp1nP	odečíst
magnitudu	magnitudu	k6eAd1	magnitudu
jasnější	jasný	k2eAgFnPc1d2	jasnější
hvězdy	hvězda	k1gFnPc1	hvězda
(	(	kIx(	(
<g/>
mb	mb	k?	mb
<g/>
)	)	kIx)	)
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
rozdíl	rozdíl	k1gInSc1	rozdíl
se	se	k3xPyFc4	se
použije	použít	k5eAaPmIp3nS	použít
jako	jako	k9	jako
exponent	exponent	k1gInSc1	exponent
základního	základní	k2eAgNnSc2d1	základní
čísla	číslo	k1gNnSc2	číslo
2,512	[number]	k4	2,512
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Δ	Δ	k?	Δ
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
m_	m_	k?	m_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
}	}	kIx)	}
}	}	kIx)	}
<g/>
-m_	_	k?	-m_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}	}	kIx)	}
}}	}}	k?	}}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2.512	[number]	k4	2.512
:	:	kIx,	:
Δ	Δ	k?	Δ
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
Δ	Δ	k?	Δ
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2.512	[number]	k4	2.512
<g/>
^	^	kIx~	^
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
{	{	kIx(	{
<g/>
m	m	kA	m
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
{	{	kIx(	{
<g/>
L	L	kA	L
<g/>
}}	}}	k?	}}
:	:	kIx,	:
Absolutní	absolutní	k2eAgFnSc1d1	absolutní
magnituda	magnituda	k1gFnSc1	magnituda
hvězdy	hvězda	k1gFnSc2	hvězda
(	(	kIx(	(
<g/>
M	M	kA	M
<g/>
)	)	kIx)	)
a	a	k8xC	a
zdánlivá	zdánlivý	k2eAgFnSc1d1	zdánlivá
magnituda	magnituda	k1gFnSc1	magnituda
(	(	kIx(	(
<g/>
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
svítivosti	svítivost	k1gFnSc3	svítivost
a	a	k8xC	a
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
nejsou	být	k5eNaImIp3nP	být
ekvivalentní	ekvivalentní	k2eAgFnPc1d1	ekvivalentní
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Sírius	Sírius	k1gInSc1	Sírius
se	s	k7c7	s
zdánlivou	zdánlivý	k2eAgFnSc7d1	zdánlivá
magnitudou	magnitudý	k2eAgFnSc7d1	magnitudý
-1,44	-1,44	k4	-1,44
má	mít	k5eAaImIp3nS	mít
absolutní	absolutní	k2eAgFnSc4d1	absolutní
magnitudu	magnituda	k1gFnSc4	magnituda
1,41	[number]	k4	1,41
<g/>
.	.	kIx.	.
</s>
<s>
Zdánlivá	zdánlivý	k2eAgFnSc1d1	zdánlivá
magnituda	magnituda	k1gFnSc1	magnituda
Slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
-26,7	-26,7	k4	-26,7
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
absolutní	absolutní	k2eAgFnSc1d1	absolutní
magnituda	magnituda	k1gFnSc1	magnituda
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
4,83	[number]	k4	4,83
<g/>
.	.	kIx.	.
</s>
<s>
Sírius	Sírius	k1gInSc1	Sírius
<g/>
,	,	kIx,	,
nejjasnější	jasný	k2eAgFnSc1d3	nejjasnější
hvězda	hvězda	k1gFnSc1	hvězda
na	na	k7c6	na
naší	náš	k3xOp1gFnSc6	náš
obloze	obloha	k1gFnSc6	obloha
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
23	[number]	k4	23
<g/>
krát	krát	k6eAd1	krát
jasnější	jasný	k2eAgFnSc1d2	jasnější
než	než	k8xS	než
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Canopus	Canopus	k1gMnSc1	Canopus
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
nejjasnější	jasný	k2eAgFnSc1d3	nejjasnější
hvězda	hvězda	k1gFnSc1	hvězda
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
,	,	kIx,	,
s	s	k7c7	s
absolutní	absolutní	k2eAgInSc1d1	absolutní
magnitudu	magnitud	k1gInSc2	magnitud
-5,53	-5,53	k4	-5,53
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
14	[number]	k4	14
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
krát	krát	k6eAd1	krát
jasnější	jasný	k2eAgFnSc1d2	jasnější
než	než	k8xS	než
Slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Canopus	Canopus	k1gInSc1	Canopus
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
jasnější	jasný	k2eAgFnSc1d2	jasnější
než	než	k8xS	než
Sirius	Sirius	k1gInSc1	Sirius
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Sírius	Sírius	k1gInSc1	Sírius
zdánlivě	zdánlivě	k6eAd1	zdánlivě
jasnější	jasný	k2eAgInSc1d2	jasnější
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Sírius	Sírius	k1gInSc1	Sírius
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
8,6	[number]	k4	8,6
světelného	světelný	k2eAgInSc2d1	světelný
roku	rok	k1gInSc2	rok
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
Canopus	Canopus	k1gInSc1	Canopus
mnohem	mnohem	k6eAd1	mnohem
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
~	~	kIx~	~
310	[number]	k4	310
ly	ly	k?	ly
<g/>
.	.	kIx.	.
</s>
<s>
Nejjasnější	jasný	k2eAgFnSc1d3	nejjasnější
známá	známý	k2eAgFnSc1d1	známá
hvězda	hvězda	k1gFnSc1	hvězda
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
absolutní	absolutní	k2eAgFnSc7d1	absolutní
magnitudou	magnitudý	k2eAgFnSc7d1	magnitudý
-14,2	-14,2	k4	-14,2
je	být	k5eAaImIp3nS	být
LBV	LBV	kA	LBV
1806	[number]	k4	1806
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
hvězda	hvězda	k1gFnSc1	hvězda
je	být	k5eAaImIp3nS	být
přinejmenším	přinejmenším	k6eAd1	přinejmenším
5	[number]	k4	5
000	[number]	k4	000
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
krát	krát	k6eAd1	krát
jasnější	jasný	k2eAgFnSc1d2	jasnější
než	než	k8xS	než
Slunce	slunce	k1gNnSc1	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Nejméně	málo	k6eAd3	málo
zářivé	zářivý	k2eAgFnPc1d1	zářivá
hvězdy	hvězda	k1gFnPc1	hvězda
<g/>
,	,	kIx,	,
jaké	jaký	k3yQgFnPc1	jaký
jsou	být	k5eAaImIp3nP	být
momentálně	momentálně	k6eAd1	momentálně
známy	znám	k2eAgFnPc1d1	známa
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
ve	v	k7c6	v
hvězdokupě	hvězdokupa	k1gFnSc6	hvězdokupa
NGC	NGC	kA	NGC
6397	[number]	k4	6397
<g/>
.	.	kIx.	.
</s>
<s>
Nejmatnější	matný	k2eAgMnSc1d3	matný
červený	červený	k2eAgMnSc1d1	červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
ve	v	k7c6	v
hvězdokupě	hvězdokupa	k1gFnSc6	hvězdokupa
má	mít	k5eAaImIp3nS	mít
magnitudu	magnitudu	k6eAd1	magnitudu
26	[number]	k4	26
až	až	k9	až
28	[number]	k4	28
<g/>
.	.	kIx.	.
</s>
<s>
Jas	jas	k1gInSc1	jas
těchto	tento	k3xDgFnPc2	tento
hvězd	hvězda	k1gFnPc2	hvězda
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
přirovnat	přirovnat	k5eAaPmF	přirovnat
ke	k	k7c3	k
svíčce	svíčka	k1gFnSc3	svíčka
na	na	k7c6	na
narozeninovém	narozeninový	k2eAgInSc6d1	narozeninový
dortu	dort	k1gInSc6	dort
na	na	k7c6	na
Měsíci	měsíc	k1gInSc6	měsíc
pozorovaném	pozorovaný	k2eAgInSc6d1	pozorovaný
ze	z	k7c2	z
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
historického	historický	k2eAgInSc2d1	historický
pohledu	pohled	k1gInSc2	pohled
byly	být	k5eAaImAgFnP	být
hvězdy	hvězda	k1gFnPc1	hvězda
důležité	důležitý	k2eAgFnPc1d1	důležitá
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
civilizacích	civilizace	k1gFnPc6	civilizace
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
jako	jako	k8xC	jako
součásti	součást	k1gFnPc1	součást
náboženských	náboženský	k2eAgFnPc2d1	náboženská
praktik	praktika	k1gFnPc2	praktika
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
též	též	k9	též
používaly	používat	k5eAaImAgInP	používat
k	k	k7c3	k
navigaci	navigace	k1gFnSc3	navigace
a	a	k8xC	a
orientaci	orientace	k1gFnSc3	orientace
na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
starověkých	starověký	k2eAgMnPc2d1	starověký
astronomů	astronom	k1gMnPc2	astronom
věřilo	věřit	k5eAaImAgNnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
hvězdy	hvězda	k1gFnPc1	hvězda
na	na	k7c6	na
nebeské	nebeský	k2eAgFnSc6d1	nebeská
sféře	sféra	k1gFnSc6	sféra
umístěny	umístit	k5eAaPmNgInP	umístit
trvale	trvale	k6eAd1	trvale
a	a	k8xC	a
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
jinak	jinak	k6eAd1	jinak
neměnné	měnný	k2eNgFnPc1d1	neměnná
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zvyklostí	zvyklost	k1gFnPc2	zvyklost
astronomové	astronom	k1gMnPc1	astronom
seskupili	seskupit	k5eAaPmAgMnP	seskupit
hvězdy	hvězda	k1gFnPc4	hvězda
do	do	k7c2	do
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
a	a	k8xC	a
používali	používat	k5eAaImAgMnP	používat
je	být	k5eAaImIp3nS	být
ke	k	k7c3	k
sledování	sledování	k1gNnSc3	sledování
pohybů	pohyb	k1gInPc2	pohyb
planet	planeta	k1gFnPc2	planeta
a	a	k8xC	a
odvození	odvození	k1gNnSc1	odvození
polohy	poloha	k1gFnSc2	poloha
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Pohyb	pohyb	k1gInSc1	pohyb
Slunce	slunce	k1gNnSc2	slunce
vůči	vůči	k7c3	vůči
hvězdnému	hvězdný	k2eAgNnSc3d1	Hvězdné
pozadí	pozadí	k1gNnSc3	pozadí
(	(	kIx(	(
<g/>
a	a	k8xC	a
horizontu	horizont	k1gInSc2	horizont
<g/>
)	)	kIx)	)
posloužil	posloužit	k5eAaPmAgMnS	posloužit
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
kalendáře	kalendář	k1gInSc2	kalendář
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pak	pak	k6eAd1	pak
našel	najít	k5eAaPmAgInS	najít
využití	využití	k1gNnSc4	využití
hlavně	hlavně	k6eAd1	hlavně
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
.	.	kIx.	.
</s>
<s>
Gregoriánský	gregoriánský	k2eAgInSc4d1	gregoriánský
kalendář	kalendář	k1gInSc4	kalendář
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
používaný	používaný	k2eAgMnSc1d1	používaný
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sluneční	sluneční	k2eAgInSc1d1	sluneční
kalendář	kalendář	k1gInSc1	kalendář
založený	založený	k2eAgInSc1d1	založený
na	na	k7c6	na
úhlu	úhel	k1gInSc6	úhel
osy	osa	k1gFnSc2	osa
otáčení	otáčení	k1gNnSc2	otáčení
Země	zem	k1gFnSc2	zem
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
nejbližší	blízký	k2eAgFnSc3d3	nejbližší
hvězdě	hvězda	k1gFnSc3	hvězda
-	-	kIx~	-
Slunci	slunce	k1gNnSc3	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnSc1d3	nejstarší
přesně	přesně	k6eAd1	přesně
datovaný	datovaný	k2eAgInSc1d1	datovaný
popis	popis	k1gInSc1	popis
hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
oblohy	obloha	k1gFnSc2	obloha
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
starověké	starověký	k2eAgFnSc2d1	starověká
egyptské	egyptský	k2eAgFnSc2d1	egyptská
astronomie	astronomie	k1gFnSc2	astronomie
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1534	[number]	k4	1534
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Nejraněji	raně	k6eAd3	raně
známé	známý	k2eAgFnPc1d1	známá
hvězdy	hvězda	k1gFnPc1	hvězda
z	z	k7c2	z
katalogu	katalog	k1gInSc2	katalog
byly	být	k5eAaImAgFnP	být
sestaveny	sestavit	k5eAaPmNgInP	sestavit
dávnými	dávný	k2eAgMnPc7d1	dávný
babylonskými	babylonský	k2eAgMnPc7d1	babylonský
astronomy	astronom	k1gMnPc7	astronom
Mezopotámie	Mezopotámie	k1gFnSc2	Mezopotámie
na	na	k7c6	na
konci	konec	k1gInSc6	konec
druhého	druhý	k4xOgNnSc2	druhý
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
před	před	k7c7	před
naším	náš	k3xOp1gInSc7	náš
letopočtem	letopočet	k1gInSc7	letopočet
<g/>
,	,	kIx,	,
během	běh	k1gInSc7	běh
období	období	k1gNnSc2	období
Kassitů	Kassit	k1gInPc2	Kassit
(	(	kIx(	(
<g/>
cca	cca	kA	cca
1531	[number]	k4	1531
<g/>
-	-	kIx~	-
<g/>
1155	[number]	k4	1155
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
hvězdný	hvězdný	k2eAgInSc4d1	hvězdný
katalog	katalog	k1gInSc4	katalog
řecké	řecký	k2eAgFnSc2d1	řecká
astronomie	astronomie	k1gFnSc2	astronomie
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
Aristyllus	Aristyllus	k1gInSc4	Aristyllus
asi	asi	k9	asi
300	[number]	k4	300
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
Timochara	Timochar	k1gMnSc2	Timochar
<g/>
.	.	kIx.	.
</s>
<s>
Hipparchův	Hipparchův	k2eAgInSc4d1	Hipparchův
katalog	katalog	k1gInSc4	katalog
hvězd	hvězda	k1gFnPc2	hvězda
z	z	k7c2	z
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
1	[number]	k4	1
020	[number]	k4	020
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
pro	pro	k7c4	pro
sestavení	sestavení	k1gNnSc4	sestavení
Ptolemaiova	Ptolemaiův	k2eAgInSc2d1	Ptolemaiův
hvězdného	hvězdný	k2eAgInSc2d1	hvězdný
katalogu	katalog	k1gInSc2	katalog
<g/>
.	.	kIx.	.
</s>
<s>
Hipparchos	Hipparchos	k1gInSc1	Hipparchos
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
svým	svůj	k3xOyFgInSc7	svůj
objevem	objev	k1gInSc7	objev
první	první	k4xOgFnSc2	první
zaznamenané	zaznamenaný	k2eAgFnSc2d1	zaznamenaná
novy	nova	k1gFnSc2	nova
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
a	a	k8xC	a
jmen	jméno	k1gNnPc2	jméno
hvězd	hvězda	k1gFnPc2	hvězda
pocházejících	pocházející	k2eAgFnPc2d1	pocházející
z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
astronomie	astronomie	k1gFnSc2	astronomie
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
zdánlivé	zdánlivý	k2eAgFnSc3d1	zdánlivá
neměnnosti	neměnnost	k1gFnSc3	neměnnost
nebes	nebesa	k1gNnPc2	nebesa
si	se	k3xPyFc3	se
byli	být	k5eAaImAgMnP	být
čínští	čínský	k2eAgMnPc1d1	čínský
astronomové	astronom	k1gMnPc1	astronom
vědomi	vědom	k2eAgMnPc1d1	vědom
toho	ten	k3xDgInSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
čas	čas	k1gInSc1	čas
od	od	k7c2	od
času	čas	k1gInSc2	čas
mohou	moct	k5eAaImIp3nP	moct
objevit	objevit	k5eAaPmF	objevit
nové	nový	k2eAgFnPc4d1	nová
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
185	[number]	k4	185
n.	n.	k?	n.
l.	l.	k?	l.
byli	být	k5eAaImAgMnP	být
první	první	k4xOgMnPc1	první
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
pozorovali	pozorovat	k5eAaImAgMnP	pozorovat
a	a	k8xC	a
psali	psát	k5eAaImAgMnP	psát
o	o	k7c6	o
supernově	supernova	k1gFnSc6	supernova
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
známé	známý	k2eAgFnSc2d1	známá
jako	jako	k8xC	jako
SN	SN	kA	SN
185	[number]	k4	185
<g/>
.	.	kIx.	.
</s>
<s>
Nejjasnější	jasný	k2eAgFnSc1d3	nejjasnější
hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
událost	událost	k1gFnSc1	událost
v	v	k7c6	v
zaznamenané	zaznamenaný	k2eAgFnSc6d1	zaznamenaná
historii	historie	k1gFnSc6	historie
byla	být	k5eAaImAgFnS	být
supernova	supernova	k1gFnSc1	supernova
SN	SN	kA	SN
1006	[number]	k4	1006
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
r.	r.	kA	r.
1006	[number]	k4	1006
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaPmAgMnS	napsat
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
egyptský	egyptský	k2eAgMnSc1d1	egyptský
astronom	astronom	k1gMnSc1	astronom
Abú	abú	k1gMnSc1	abú
Hasan	Hasan	k1gMnSc1	Hasan
Alí	Alí	k1gMnSc1	Alí
ibn	ibn	k?	ibn
Ridwan	Ridwan	k1gMnSc1	Ridwan
al-Misrí	al-Misrí	k2eAgMnSc1d1	al-Misrí
a	a	k8xC	a
několik	několik	k4yIc1	několik
čínských	čínský	k2eAgMnPc2d1	čínský
astronomů	astronom	k1gMnPc2	astronom
<g/>
.	.	kIx.	.
</s>
<s>
Supernova	supernova	k1gFnSc1	supernova
SN	SN	kA	SN
1054	[number]	k4	1054
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
způsobila	způsobit	k5eAaPmAgFnS	způsobit
zrod	zrod	k1gInSc4	zrod
Krabí	krabí	k2eAgFnSc2d1	krabí
mlhoviny	mlhovina	k1gFnSc2	mlhovina
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
čínskými	čínský	k2eAgInPc7d1	čínský
a	a	k8xC	a
islámskými	islámský	k2eAgMnPc7d1	islámský
astronomy	astronom	k1gMnPc7	astronom
<g/>
.	.	kIx.	.
</s>
<s>
Středověcí	středověký	k2eAgMnPc1d1	středověký
islámští	islámský	k2eAgMnPc1d1	islámský
astronomové	astronom	k1gMnPc1	astronom
dali	dát	k5eAaPmAgMnP	dát
mnoha	mnoho	k4c3	mnoho
hvězdám	hvězda	k1gFnPc3	hvězda
svá	svůj	k3xOyFgNnPc4	svůj
arabská	arabský	k2eAgNnPc4d1	arabské
jména	jméno	k1gNnPc4	jméno
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
a	a	k8xC	a
vymysleli	vymyslet	k5eAaPmAgMnP	vymyslet
mnoho	mnoho	k4c4	mnoho
astronomických	astronomický	k2eAgInPc2d1	astronomický
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
pomocí	pomoc	k1gFnSc7	pomoc
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vypočítat	vypočítat	k5eAaPmF	vypočítat
polohu	poloha	k1gFnSc4	poloha
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Postavili	postavit	k5eAaPmAgMnP	postavit
první	první	k4xOgFnPc4	první
velké	velká	k1gFnPc4	velká
výzkumné	výzkumný	k2eAgFnSc2d1	výzkumná
observatoře	observatoř	k1gFnSc2	observatoř
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
sepsání	sepsání	k1gNnSc2	sepsání
hvězdných	hvězdný	k2eAgInPc2d1	hvězdný
katalogů	katalog	k1gInPc2	katalog
Zij	Zij	k1gFnSc2	Zij
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gFnPc4	on
i	i	k9	i
Kniha	kniha	k1gFnSc1	kniha
stálic	stálice	k1gFnPc2	stálice
(	(	kIx(	(
<g/>
964	[number]	k4	964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
napsaná	napsaný	k2eAgFnSc1d1	napsaná
perským	perský	k2eAgMnSc7d1	perský
astronomem	astronom	k1gMnSc7	astronom
Abdurrahmánem	Abdurrahmán	k1gMnSc7	Abdurrahmán
ibn	ibn	k?	ibn
Umar	Umar	k1gInSc1	Umar
as-Súfím	as-Súfet	k5eAaImIp1nS	as-Súfet
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
sledoval	sledovat	k5eAaImAgMnS	sledovat
počet	počet	k1gInSc4	počet
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
hvězdokup	hvězdokupa	k1gFnPc2	hvězdokupa
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Omicron	Omicron	k1gMnSc1	Omicron
Velorum	Velorum	k1gInSc1	Velorum
a	a	k8xC	a
Collinder	Collinder	k1gInSc1	Collinder
399	[number]	k4	399
<g/>
)	)	kIx)	)
a	a	k8xC	a
galaxií	galaxie	k1gFnPc2	galaxie
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
galaxie	galaxie	k1gFnSc2	galaxie
v	v	k7c6	v
Andromedě	Andromed	k1gInSc6	Andromed
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Ahmada	Ahmada	k1gFnSc1	Ahmada
Zahoora	Zahoora	k1gFnSc1	Zahoora
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
perský	perský	k2eAgMnSc1d1	perský
učenec	učenec	k1gMnSc1	učenec
Aliboron	Aliboron	k1gMnSc1	Aliboron
popsal	popsat	k5eAaPmAgMnS	popsat
galaxie	galaxie	k1gFnPc4	galaxie
jako	jako	k8xS	jako
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
fragmentů	fragment	k1gInPc2	fragment
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
vlastnosti	vlastnost	k1gFnPc4	vlastnost
mlhavých	mlhavý	k2eAgFnPc2d1	mlhavá
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Josepa	Josep	k1gMnSc2	Josep
Puigeho	Puige	k1gMnSc4	Puige
andaluský	andaluský	k2eAgMnSc1d1	andaluský
astronom	astronom	k1gMnSc1	astronom
Ibn	Ibn	k1gMnSc1	Ibn
Bádždža	Bádždža	k1gMnSc1	Bádždža
Abú	abú	k1gMnSc1	abú
Bakr	Bakr	k1gMnSc1	Bakr
Muhammad	Muhammad	k1gInSc4	Muhammad
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
hypotézu	hypotéza	k1gFnSc4	hypotéza
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Mléčná	mléčný	k2eAgFnSc1d1	mléčná
dráha	dráha	k1gFnSc1	dráha
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
jakoby	jakoby	k8xS	jakoby
navzájem	navzájem	k6eAd1	navzájem
dotýkaly	dotýkat	k5eAaImAgFnP	dotýkat
a	a	k8xC	a
vypadají	vypadat	k5eAaImIp3nP	vypadat
jako	jako	k9	jako
souvislý	souvislý	k2eAgInSc4d1	souvislý
obraz	obraz	k1gInSc4	obraz
jen	jen	k9	jen
díky	díky	k7c3	díky
vlivu	vliv	k1gInSc3	vliv
refrakce	refrakce	k1gFnSc2	refrakce
sublunárního	sublunární	k2eAgInSc2d1	sublunární
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
evropští	evropský	k2eAgMnPc1d1	evropský
astronomové	astronom	k1gMnPc1	astronom
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
Tycho	Tyc	k1gMnSc4	Tyc
Brahe	Brah	k1gMnSc4	Brah
<g/>
,	,	kIx,	,
identifikovali	identifikovat	k5eAaBmAgMnP	identifikovat
nové	nový	k2eAgFnPc4d1	nová
hvězdy	hvězda	k1gFnPc4	hvězda
na	na	k7c6	na
noční	noční	k2eAgFnSc6d1	noční
obloze	obloha	k1gFnSc6	obloha
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
novy	nova	k1gFnSc2	nova
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
naznačovalo	naznačovat	k5eAaImAgNnS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebesa	nebesa	k1gNnPc1	nebesa
nejsou	být	k5eNaImIp3nP	být
neměnná	neměnný	k2eAgNnPc1d1	neměnné
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1584	[number]	k4	1584
Giordano	Giordana	k1gFnSc5	Giordana
Bruno	Bruno	k1gMnSc1	Bruno
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
hvězdy	hvězda	k1gFnPc1	hvězda
to	ten	k3xDgNnSc4	ten
samé	samý	k3xTgFnSc6	samý
<g/>
,	,	kIx,	,
co	co	k9	co
Slunce	slunce	k1gNnSc1	slunce
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
rovněž	rovněž	k9	rovněž
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
své	svůj	k3xOyFgFnPc4	svůj
planety	planeta	k1gFnPc4	planeta
na	na	k7c6	na
oběžných	oběžný	k2eAgFnPc6d1	oběžná
drahách	draha	k1gFnPc6	draha
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ty	ten	k3xDgMnPc4	ten
dokonce	dokonce	k9	dokonce
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
podobné	podobný	k2eAgFnSc3d1	podobná
Zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
myšlenka	myšlenka	k1gFnSc1	myšlenka
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
vyřknuta	vyřknout	k5eAaPmNgFnS	vyřknout
starověkými	starověký	k2eAgFnPc7d1	starověká
řeckých	řecký	k2eAgMnPc2d1	řecký
filozofy	filozof	k1gMnPc7	filozof
Démokritem	Démokritos	k1gMnSc7	Démokritos
a	a	k8xC	a
Epikúrem	Epikúr	k1gInSc7	Epikúr
a	a	k8xC	a
středověkými	středověký	k2eAgMnPc7d1	středověký
islámskými	islámský	k2eAgMnPc7d1	islámský
kosmology	kosmolog	k1gMnPc7	kosmolog
jako	jako	k8xC	jako
Fakhr	Fakhr	k1gMnSc1	Fakhr
ad-Din	ad-Din	k1gMnSc1	ad-Din
al-Razi	al-Raze	k1gFnSc4	al-Raze
<g/>
.	.	kIx.	.
</s>
<s>
William	William	k1gInSc1	William
Herschel	Herschel	k1gInSc1	Herschel
byl	být	k5eAaImAgMnS	být
astronom	astronom	k1gMnSc1	astronom
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgMnS	zajímat
mj.	mj.	kA	mj.
i	i	k9	i
o	o	k7c4	o
rozdělení	rozdělení	k1gNnSc4	rozdělení
hvězd	hvězda	k1gFnPc2	hvězda
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1780	[number]	k4	1780
vykonával	vykonávat	k5eAaImAgInS	vykonávat
řadu	řada	k1gFnSc4	řada
měření	měření	k1gNnSc2	měření
na	na	k7c6	na
600	[number]	k4	600
různých	různý	k2eAgInPc6d1	různý
místech	místo	k1gNnPc6	místo
a	a	k8xC	a
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
spočítal	spočítat	k5eAaPmAgMnS	spočítat
pozorované	pozorovaný	k2eAgFnPc4d1	pozorovaná
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
vyvodil	vyvodit	k5eAaPmAgMnS	vyvodit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
se	se	k3xPyFc4	se
neustále	neustále	k6eAd1	neustále
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
jedné	jeden	k4xCgFnSc3	jeden
straně	strana	k1gFnSc3	strana
oblohy	obloha	k1gFnSc2	obloha
<g/>
,	,	kIx,	,
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
jádra	jádro	k1gNnSc2	jádro
Mléčné	mléčný	k2eAgFnSc2d1	mléčná
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Herschel	Herschel	k1gMnSc1	Herschel
tuto	tento	k3xDgFnSc4	tento
studii	studie	k1gFnSc4	studie
opakoval	opakovat	k5eAaImAgMnS	opakovat
na	na	k7c6	na
Jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
a	a	k8xC	a
našel	najít	k5eAaPmAgMnS	najít
totožný	totožný	k2eAgInSc4d1	totožný
nárůst	nárůst	k1gInSc4	nárůst
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
směru	směr	k1gInSc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
dalších	další	k2eAgInPc2d1	další
úspěchů	úspěch	k1gInPc2	úspěch
je	být	k5eAaImIp3nS	být
William	William	k1gInSc1	William
Herschel	Herschela	k1gFnPc2	Herschela
také	také	k9	také
znám	znát	k5eAaImIp1nS	znát
svým	svůj	k3xOyFgInSc7	svůj
objevem	objev	k1gInSc7	objev
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgFnPc1	některý
hvězdy	hvězda	k1gFnPc1	hvězda
neleží	ležet	k5eNaImIp3nP	ležet
těsně	těsně	k6eAd1	těsně
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
jen	jen	k6eAd1	jen
z	z	k7c2	z
pohledu	pohled	k1gInSc2	pohled
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
<g/>
,	,	kIx,	,
zdánlivě	zdánlivě	k6eAd1	zdánlivě
tak	tak	k6eAd1	tak
tvoříc	tvořit	k5eAaImSgFnS	tvořit
vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
pár	pár	k4xCyI	pár
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
optická	optický	k2eAgFnSc1d1	optická
dvojhvězda	dvojhvězda	k1gFnSc1	dvojhvězda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
hvězdy	hvězda	k1gFnPc1	hvězda
jsou	být	k5eAaImIp3nP	být
společníky	společník	k1gMnPc4	společník
fyzickými	fyzický	k2eAgFnPc7d1	fyzická
a	a	k8xC	a
skutečně	skutečně	k6eAd1	skutečně
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
tvoříc	tvořit	k5eAaImSgFnS	tvořit
tak	tak	k9	tak
systémy	systém	k1gInPc1	systém
dvojhvězd	dvojhvězda	k1gFnPc2	dvojhvězda
<g/>
.	.	kIx.	.
</s>
<s>
Věda	věda	k1gFnSc1	věda
o	o	k7c6	o
spektroskopii	spektroskopie	k1gFnSc6	spektroskopie
hvězd	hvězda	k1gFnPc2	hvězda
byla	být	k5eAaImAgFnS	být
propagována	propagovat	k5eAaImNgFnS	propagovat
Josephem	Joseph	k1gInSc7	Joseph
von	von	k1gInSc4	von
Fraunhoferem	Fraunhofero	k1gNnSc7	Fraunhofero
a	a	k8xC	a
Angelo	Angela	k1gFnSc5	Angela
Secchim	Secchim	k1gInSc4	Secchim
<g/>
.	.	kIx.	.
</s>
<s>
Porovnáním	porovnání	k1gNnSc7	porovnání
spekter	spektrum	k1gNnPc2	spektrum
hvězd	hvězda	k1gFnPc2	hvězda
Siria	Siria	k1gFnSc1	Siria
a	a	k8xC	a
Slunce	slunce	k1gNnSc4	slunce
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
síle	síla	k1gFnSc6	síla
a	a	k8xC	a
počtu	počet	k1gInSc2	počet
jejich	jejich	k3xOp3gFnPc2	jejich
absorpčních	absorpční	k2eAgFnPc2d1	absorpční
čar	čára	k1gFnPc2	čára
-	-	kIx~	-
tmavých	tmavý	k2eAgFnPc2d1	tmavá
linek	linka	k1gFnPc2	linka
-	-	kIx~	-
byly	být	k5eAaImAgInP	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
absorpcí	absorpce	k1gFnSc7	absorpce
určitých	určitý	k2eAgFnPc2d1	určitá
frekvencí	frekvence	k1gFnSc7	frekvence
atmosférou	atmosféra	k1gFnSc7	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1865	[number]	k4	1865
začal	začít	k5eAaPmAgInS	začít
Secchi	Secch	k1gFnSc2	Secch
třídit	třídit	k5eAaImF	třídit
hvězdy	hvězda	k1gFnPc4	hvězda
do	do	k7c2	do
spektrálních	spektrální	k2eAgInPc2d1	spektrální
typů	typ	k1gInPc2	typ
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
moderní	moderní	k2eAgFnSc4d1	moderní
verzi	verze	k1gFnSc4	verze
klasifikačního	klasifikační	k2eAgNnSc2d1	klasifikační
schématu	schéma	k1gNnSc2	schéma
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
až	až	k8xS	až
Annie	Annie	k1gFnSc2	Annie
Jump	Jump	k1gMnSc1	Jump
Cannonová	Cannonová	k1gFnSc1	Cannonová
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
přímé	přímý	k2eAgNnSc1d1	přímé
měření	měření	k1gNnSc1	měření
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
roku	rok	k1gInSc2	rok
1838	[number]	k4	1838
Friedrichem	Friedrich	k1gMnSc7	Friedrich
Wilhelmem	Wilhelm	k1gMnSc7	Wilhelm
Besselem	Bessel	k1gMnSc7	Bessel
pomocí	pomocí	k7c2	pomocí
techniky	technika	k1gFnSc2	technika
paralaxy	paralaxa	k1gFnSc2	paralaxa
na	na	k7c6	na
hvězdě	hvězda	k1gFnSc6	hvězda
61	[number]	k4	61
Cygni	Cygeň	k1gFnSc6	Cygeň
(	(	kIx(	(
<g/>
11,4	[number]	k4	11,4
svět	svět	k1gInSc1	svět
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
daleko	daleko	k6eAd1	daleko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měření	měření	k1gNnSc1	měření
paralaxou	paralaxa	k1gFnSc7	paralaxa
prokázala	prokázat	k5eAaPmAgFnS	prokázat
naprostou	naprostý	k2eAgFnSc4d1	naprostá
oddělenost	oddělenost	k1gFnSc4	oddělenost
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
pozorování	pozorování	k1gNnSc2	pozorování
dvojhvězd	dvojhvězda	k1gFnPc2	dvojhvězda
rostl	růst	k5eAaImAgInS	růst
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1834	[number]	k4	1834
Bessel	Bessel	k1gMnSc1	Bessel
vypozoroval	vypozorovat	k5eAaPmAgMnS	vypozorovat
změny	změna	k1gFnPc4	změna
od	od	k7c2	od
předpokládaného	předpokládaný	k2eAgInSc2d1	předpokládaný
pohybu	pohyb	k1gInSc2	pohyb
hvězdy	hvězda	k1gFnSc2	hvězda
Sirius	Sirius	k1gMnSc1	Sirius
a	a	k8xC	a
odvodil	odvodit	k5eAaPmAgMnS	odvodit
tak	tak	k6eAd1	tak
skrytého	skrytý	k2eAgMnSc4d1	skrytý
společníka	společník	k1gMnSc4	společník
<g/>
.	.	kIx.	.
</s>
<s>
Edward	Edward	k1gMnSc1	Edward
Charles	Charles	k1gMnSc1	Charles
Pickering	Pickering	k1gInSc4	Pickering
objevil	objevit	k5eAaPmAgMnS	objevit
první	první	k4xOgInSc4	první
spektroskopický	spektroskopický	k2eAgInSc4d1	spektroskopický
binární	binární	k2eAgInSc4d1	binární
systém	systém	k1gInSc4	systém
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
když	když	k8xS	když
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
periodické	periodický	k2eAgNnSc4d1	periodické
rozštěpení	rozštěpení	k1gNnSc4	rozštěpení
spektrálních	spektrální	k2eAgFnPc2d1	spektrální
čar	čára	k1gFnPc2	čára
hvězdy	hvězda	k1gFnSc2	hvězda
Mizar	Mizara	k1gFnPc2	Mizara
se	s	k7c7	s
104	[number]	k4	104
<g/>
denní	denní	k2eAgFnSc7d1	denní
periodou	perioda	k1gFnSc7	perioda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvních	první	k4xOgNnPc2	první
desetiletí	desetiletí	k1gNnPc2	desetiletí
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc6	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vyhotovení	vyhotovení	k1gNnSc3	vyhotovení
důležitých	důležitý	k2eAgFnPc2d1	důležitá
teoretických	teoretický	k2eAgFnPc2d1	teoretická
prací	práce	k1gFnPc2	práce
o	o	k7c6	o
fyzické	fyzický	k2eAgFnSc6d1	fyzická
struktuře	struktura	k1gFnSc6	struktura
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
HR	hr	k2eAgInSc1d1	hr
diagram	diagram	k1gInSc1	diagram
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dopomohl	dopomoct	k5eAaPmAgInS	dopomoct
ještě	ještě	k6eAd1	ještě
většímu	veliký	k2eAgNnSc3d2	veliký
astrofyzikálnímu	astrofyzikální	k2eAgNnSc3d1	astrofyzikální
studiu	studio	k1gNnSc3	studio
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
vyvinuté	vyvinutý	k2eAgInPc1d1	vyvinutý
modely	model	k1gInPc1	model
úspěšně	úspěšně	k6eAd1	úspěšně
vysvětlovaly	vysvětlovat	k5eAaImAgInP	vysvětlovat
vnitřek	vnitřek	k1gInSc4	vnitřek
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Cecilia	Cecilia	k1gFnSc1	Cecilia
Helena	Helena	k1gFnSc1	Helena
Payne	Payn	k1gInSc5	Payn
Gaposchkinová	Gaposchkinový	k2eAgFnSc1d1	Gaposchkinový
jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
disertační	disertační	k2eAgFnSc6d1	disertační
práci	práce	k1gFnSc6	práce
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
vyslovila	vyslovit	k5eAaPmAgFnS	vyslovit
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
hvězdy	hvězda	k1gFnPc1	hvězda
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgFnP	složit
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
vodíku	vodík	k1gInSc2	vodík
a	a	k8xC	a
helia	helium	k1gNnSc2	helium
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
pokrokům	pokrok	k1gInPc3	pokrok
v	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
fyzice	fyzika	k1gFnSc6	fyzika
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
lépe	dobře	k6eAd2	dobře
pochopit	pochopit	k5eAaPmF	pochopit
spektra	spektrum	k1gNnPc4	spektrum
hvězd	hvězda	k1gFnPc2	hvězda
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tak	tak	k9	tak
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
stanovit	stanovit	k5eAaPmF	stanovit
chemické	chemický	k2eAgNnSc1d1	chemické
složení	složení	k1gNnSc1	složení
jejich	jejich	k3xOp3gFnPc2	jejich
atmosfér	atmosféra	k1gFnPc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Anaximandros	Anaximandrosa	k1gFnPc2	Anaximandrosa
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
látka	látka	k1gFnSc1	látka
plodící	plodící	k2eAgFnSc1d1	plodící
od	od	k7c2	od
věčnosti	věčnost	k1gFnSc2	věčnost
teplo	teplo	k1gNnSc4	teplo
a	a	k8xC	a
chlad	chlad	k1gInSc4	chlad
při	při	k7c6	při
vzniku	vznik	k1gInSc6	vznik
tohoto	tento	k3xDgInSc2	tento
světa	svět	k1gInSc2	svět
oddělila	oddělit	k5eAaPmAgFnS	oddělit
a	a	k8xC	a
že	že	k8xS	že
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
kolem	kolem	k7c2	kolem
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
zem	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
vyrostla	vyrůst	k5eAaPmAgFnS	vyrůst
jakási	jakýsi	k3yIgFnSc1	jakýsi
ohnivá	ohnivý	k2eAgFnSc1d1	ohnivá
koule	koule	k1gFnSc1	koule
jako	jako	k8xS	jako
kůra	kůra	k1gFnSc1	kůra
kolem	kolem	k7c2	kolem
stromu	strom	k1gInSc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
tato	tento	k3xDgFnSc1	tento
koule	koule	k1gFnSc1	koule
roztrhla	roztrhnout	k5eAaPmAgFnS	roztrhnout
a	a	k8xC	a
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
do	do	k7c2	do
různých	různý	k2eAgFnPc2d1	různá
prstencovou	prstencový	k2eAgFnSc7d1	prstencová
pásů	pás	k1gInPc2	pás
<g/>
,	,	kIx,	,
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
Slunce	slunce	k1gNnSc4	slunce
<g/>
,	,	kIx,	,
Měsíc	měsíc	k1gInSc4	měsíc
a	a	k8xC	a
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dějin	dějiny	k1gFnPc2	dějiny
výzkumu	výzkum	k1gInSc2	výzkum
hvězd	hvězda	k1gFnPc2	hvězda
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
myslích	mysl	k1gFnPc6	mysl
vědců	vědec	k1gMnPc2	vědec
i	i	k8xC	i
autorů	autor	k1gMnPc2	autor
sci-fi	scii	k1gNnSc2	sci-fi
množství	množství	k1gNnSc2	množství
hypotetických	hypotetický	k2eAgFnPc2d1	hypotetická
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgInPc4d3	nejznámější
příklady	příklad	k1gInPc4	příklad
patří	patřit	k5eAaImIp3nS	patřit
hypotetická	hypotetický	k2eAgFnSc1d1	hypotetická
hvězda	hvězda	k1gFnSc1	hvězda
smrti	smrt	k1gFnSc2	smrt
Nemesis	Nemesis	k1gFnSc2	Nemesis
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
koncept	koncept	k1gInSc1	koncept
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
Vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
jej	on	k3xPp3gNnSc4	on
paleontologové	paleontolog	k1gMnPc1	paleontolog
David	David	k1gMnSc1	David
M.	M.	kA	M.
Raup	Raup	k1gMnSc1	Raup
a	a	k8xC	a
John	John	k1gMnSc1	John
Sepkoski	Sepkosk	k1gFnSc2	Sepkosk
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
domnělou	domnělý	k2eAgFnSc4d1	domnělá
periodicitu	periodicita	k1gFnSc4	periodicita
ve	v	k7c6	v
vymírání	vymírání	k1gNnSc6	vymírání
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Hvězda	hvězda	k1gFnSc1	hvězda
(	(	kIx(	(
<g/>
zřejmě	zřejmě	k6eAd1	zřejmě
hnědý	hnědý	k2eAgInSc1d1	hnědý
nebo	nebo	k8xC	nebo
červený	červený	k2eAgMnSc1d1	červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
jen	jen	k9	jen
asi	asi	k9	asi
1,5	[number]	k4	1,5
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
světelné	světelný	k2eAgInPc4d1	světelný
roky	rok	k1gInPc4	rok
od	od	k7c2	od
našeho	náš	k3xOp1gNnSc2	náš
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
její	její	k3xOp3gFnSc1	její
existence	existence	k1gFnSc1	existence
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
reálnou	reálný	k2eAgFnSc4d1	reálná
<g/>
.	.	kIx.	.
</s>
