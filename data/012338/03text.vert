<p>
<s>
Zedníček	Zedníček	k1gMnSc1	Zedníček
skalní	skalní	k2eAgMnSc1d1	skalní
(	(	kIx(	(
<g/>
Tichodroma	Tichodroma	k1gNnSc1	Tichodroma
muraria	murarium	k1gNnSc2	murarium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
menšího	malý	k2eAgMnSc2d2	menší
horského	horský	k2eAgMnSc2d1	horský
ptáka	pták	k1gMnSc2	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
brhlíkovitých	brhlíkovitý	k2eAgInPc2d1	brhlíkovitý
<g/>
,	,	kIx,	,
o	o	k7c4	o
něco	něco	k3yInSc4	něco
větší	veliký	k2eAgMnSc1d2	veliký
než	než	k8xS	než
vrabec	vrabec	k1gMnSc1	vrabec
domácí	domácí	k1gMnSc1	domácí
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
západní	západní	k2eAgFnSc2d1	západní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
Tatrách	Tatra	k1gFnPc6	Tatra
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
nejsevernější	severní	k2eAgNnSc1d3	nejsevernější
hnízdiště	hnízdiště	k1gNnSc1	hnízdiště
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
pták	pták	k1gMnSc1	pták
stěhovavý	stěhovavý	k2eAgMnSc1d1	stěhovavý
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
dobu	doba	k1gFnSc4	doba
hnízdění	hnízdění	k1gNnSc2	hnízdění
se	se	k3xPyFc4	se
vydává	vydávat	k5eAaImIp3nS	vydávat
jen	jen	k9	jen
na	na	k7c4	na
krátké	krátký	k2eAgFnPc4d1	krátká
cesty	cesta	k1gFnPc4	cesta
do	do	k7c2	do
nižších	nízký	k2eAgFnPc2d2	nižší
horských	horský	k2eAgFnPc2d1	horská
poloh	poloha	k1gFnPc2	poloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
stanovištěm	stanoviště	k1gNnSc7	stanoviště
jsou	být	k5eAaImIp3nP	být
skalní	skalní	k2eAgFnPc4d1	skalní
stěny	stěna	k1gFnPc4	stěna
a	a	k8xC	a
suťová	suťový	k2eAgNnPc4d1	suťové
pole	pole	k1gNnPc4	pole
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
rozsedlinách	rozsedlina	k1gFnPc6	rozsedlina
a	a	k8xC	a
dutinách	dutina	k1gFnPc6	dutina
mezi	mezi	k7c7	mezi
balvany	balvan	k1gMnPc7	balvan
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nP	živit
se	s	k7c7	s
hmyzem	hmyz	k1gInSc7	hmyz
a	a	k8xC	a
pavouky	pavouk	k1gMnPc7	pavouk
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
svým	svůj	k3xOyFgInSc7	svůj
dlouhým	dlouhý	k2eAgInSc7d1	dlouhý
zobákem	zobák	k1gInSc7	zobák
vytahuje	vytahovat	k5eAaImIp3nS	vytahovat
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
úkrytů	úkryt	k1gInPc2	úkryt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
dva	dva	k4xCgInPc1	dva
poddruhy	poddruh	k1gInPc1	poddruh
zedníčka	zedníček	k1gMnSc2	zedníček
skalního	skalní	k2eAgMnSc2d1	skalní
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
poddruhu	poddruh	k1gInSc2	poddruh
T.	T.	kA	T.
muraria	murarium	k1gNnSc2	murarium
muraria	murarium	k1gNnSc2	murarium
(	(	kIx(	(
<g/>
Evropa	Evropa	k1gFnSc1	Evropa
a	a	k8xC	a
Turecko	Turecko	k1gNnSc1	Turecko
až	až	k9	až
po	po	k7c4	po
Kavkaz	Kavkaz	k1gInSc4	Kavkaz
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
zjištěn	zjistit	k5eAaPmNgInS	zjistit
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
výskyt	výskyt	k1gInSc1	výskyt
poddruhu	poddruh	k1gInSc2	poddruh
T.	T.	kA	T.
muraria	murarium	k1gNnSc2	murarium
nepalensis	nepalensis	k1gInSc1	nepalensis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
jen	jen	k9	jen
nepatrně	nepatrně	k6eAd1	nepatrně
liší	lišit	k5eAaImIp3nP	lišit
temnější	temný	k2eAgFnSc7d2	temnější
barvou	barva	k1gFnSc7	barva
peří	peří	k1gNnSc2	peří
a	a	k8xC	a
výskytem	výskyt	k1gInSc7	výskyt
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
horských	horský	k2eAgFnPc6d1	horská
polohách	poloha	k1gFnPc6	poloha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zedníček	Zedníček	k1gMnSc1	Zedníček
skalní	skalní	k2eAgMnSc1d1	skalní
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
však	však	k9	však
kriticky	kriticky	k6eAd1	kriticky
ohroženým	ohrožený	k2eAgInSc7d1	ohrožený
druhem	druh	k1gInSc7	druh
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Husitské	husitský	k2eAgFnSc6d1	husitská
trilogii	trilogie	k1gFnSc6	trilogie
Andrzeje	Andrzeje	k1gMnSc2	Andrzeje
Sapkowského	Sapkowský	k1gMnSc2	Sapkowský
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
čaroděj	čaroděj	k1gMnSc1	čaroděj
Birkart	Birkarta	k1gFnPc2	Birkarta
Grellenort	Grellenort	k1gInSc1	Grellenort
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
autora	autor	k1gMnSc2	autor
proměňuje	proměňovat	k5eAaImIp3nS	proměňovat
právě	právě	k9	právě
v	v	k7c4	v
tohoto	tento	k3xDgMnSc4	tento
ptáka	pták	k1gMnSc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
Pták	pták	k1gMnSc1	pták
<g/>
,	,	kIx,	,
popsaný	popsaný	k2eAgMnSc1d1	popsaný
v	v	k7c6	v
románu	román	k1gInSc6	román
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
sice	sice	k8xC	sice
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
zobák	zobák	k1gInSc1	zobák
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
tmavý	tmavý	k2eAgMnSc1d1	tmavý
a	a	k8xC	a
dost	dost	k6eAd1	dost
velký	velký	k2eAgInSc1d1	velký
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
připomíná	připomínat	k5eAaImIp3nS	připomínat
spíš	spíš	k9	spíš
kavku	kavka	k1gFnSc4	kavka
nebo	nebo	k8xC	nebo
kavče	kavče	k1gNnSc4	kavče
červenozobé	červenozobý	k2eAgNnSc4d1	červenozobé
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Pomurnik	Pomurnik	k1gInSc1	Pomurnik
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
zedníček	zedníček	k1gMnSc1	zedníček
skalní	skalní	k2eAgMnSc1d1	skalní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
zedníček	zedníček	k1gMnSc1	zedníček
skalní	skalní	k2eAgMnSc1d1	skalní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
zedníček	zedníček	k1gMnSc1	zedníček
skalní	skalní	k2eAgMnSc1d1	skalní
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Tichodroma	Tichodromum	k1gNnSc2	Tichodromum
muraria	murarium	k1gNnSc2	murarium
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
</s>
</p>
<p>
<s>
obrázky	obrázek	k1gInPc1	obrázek
zedníčka	zedníček	k1gMnSc2	zedníček
skalního	skalní	k2eAgNnSc2d1	skalní
</s>
</p>
