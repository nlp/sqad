<s>
DNA	DNA	kA	DNA
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
pro	pro	k7c4	pro
život	život	k1gInSc4	život
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc7d1	důležitá
látkou	látka	k1gFnSc7	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
struktuře	struktura	k1gFnSc6	struktura
kóduje	kódovat	k5eAaBmIp3nS	kódovat
a	a	k8xC	a
buňkám	buňka	k1gFnPc3	buňka
zadává	zadávat	k5eAaImIp3nS	zadávat
jejich	jejich	k3xOp3gInSc4	jejich
program	program	k1gInSc4	program
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
předurčuje	předurčovat	k5eAaImIp3nS	předurčovat
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
vlastnosti	vlastnost	k1gFnPc4	vlastnost
celého	celý	k2eAgInSc2d1	celý
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
