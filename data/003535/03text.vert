<s>
Deoxyribonukleová	deoxyribonukleový	k2eAgFnSc1d1	deoxyribonukleová
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
označovaná	označovaný	k2eAgFnSc1d1	označovaná
DNA	dna	k1gFnSc1	dna
(	(	kIx(	(
<g/>
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
deoxyribonucleic	deoxyribonucleice	k1gInPc2	deoxyribonucleice
acid	acido	k1gNnPc2	acido
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
zřídka	zřídka	k6eAd1	zřídka
i	i	k9	i
DNK	DNK	kA	DNK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nukleová	nukleový	k2eAgFnSc1d1	nukleová
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
,	,	kIx,	,
nositelka	nositelka	k1gFnSc1	nositelka
genetické	genetický	k2eAgFnSc2d1	genetická
informace	informace	k1gFnSc2	informace
všech	všecek	k3xTgInPc2	všecek
organismů	organismus	k1gInPc2	organismus
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
některých	některý	k3yIgMnPc2	některý
nebuněčných	buněčný	k2eNgMnPc2d1	buněčný
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgInPc2	jenž
hraje	hrát	k5eAaImIp3nS	hrát
tuto	tento	k3xDgFnSc4	tento
úlohu	úloha	k1gFnSc4	úloha
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
např.	např.	kA	např.
RNA	RNA	kA	RNA
viry	vir	k1gInPc1	vir
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
DNA	DNA	kA	DNA
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
pro	pro	k7c4	pro
život	život	k1gInSc4	život
velmi	velmi	k6eAd1	velmi
důležitou	důležitý	k2eAgFnSc7d1	důležitá
látkou	látka	k1gFnSc7	látka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
struktuře	struktura	k1gFnSc6	struktura
kóduje	kódovat	k5eAaBmIp3nS	kódovat
a	a	k8xC	a
buňkám	buňka	k1gFnPc3	buňka
zadává	zadávat	k5eAaImIp3nS	zadávat
jejich	jejich	k3xOp3gInSc4	jejich
program	program	k1gInSc4	program
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
předurčuje	předurčovat	k5eAaImIp3nS	předurčovat
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
vlastnosti	vlastnost	k1gFnPc4	vlastnost
celého	celý	k2eAgInSc2d1	celý
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
eukaryotických	eukaryotický	k2eAgInPc2d1	eukaryotický
organizmů	organizmus	k1gInPc2	organizmus
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
např.	např.	kA	např.
rostliny	rostlina	k1gFnSc2	rostlina
a	a	k8xC	a
živočichové	živočich	k1gMnPc1	živočich
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
DNA	dna	k1gFnSc1	dna
hlavní	hlavní	k2eAgFnSc1d1	hlavní
složkou	složka	k1gFnSc7	složka
chromatinu	chromatin	k1gInSc2	chromatin
<g/>
,	,	kIx,	,
směsi	směs	k1gFnSc2	směs
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
a	a	k8xC	a
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
uložena	uložit	k5eAaPmNgFnS	uložit
zejména	zejména	k9	zejména
uvnitř	uvnitř	k7c2	uvnitř
buněčného	buněčný	k2eAgNnSc2d1	buněčné
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
u	u	k7c2	u
prokaryot	prokaryota	k1gFnPc2	prokaryota
(	(	kIx(	(
<g/>
např.	např.	kA	např.
bakterie	bakterie	k1gFnSc2	bakterie
a	a	k8xC	a
archea	archeum	k1gNnSc2	archeum
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
DNA	dna	k1gFnSc1	dna
nachází	nacházet	k5eAaImIp3nS	nacházet
volně	volně	k6eAd1	volně
v	v	k7c6	v
cytoplazmě	cytoplazma	k1gFnSc6	cytoplazma
<g/>
.	.	kIx.	.
</s>
<s>
DNA	DNA	kA	DNA
je	být	k5eAaImIp3nS	být
biologická	biologický	k2eAgFnSc1d1	biologická
makromolekula	makromolekula	k1gFnSc1	makromolekula
–	–	k?	–
polymer	polymer	k1gInSc1	polymer
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
řetězce	řetězec	k1gInSc2	řetězec
nukleotidů	nukleotid	k1gInPc2	nukleotid
<g/>
.	.	kIx.	.
</s>
<s>
Nukleotidy	nukleotid	k1gInPc1	nukleotid
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
složeny	složit	k5eAaPmNgInP	složit
z	z	k7c2	z
cukru	cukr	k1gInSc2	cukr
deoxyribózy	deoxyribóza	k1gFnSc2	deoxyribóza
<g/>
,	,	kIx,	,
fosfátové	fosfátový	k2eAgFnSc2d1	fosfátová
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
nukleových	nukleový	k2eAgFnPc2d1	nukleová
bází	báze	k1gFnPc2	báze
<g/>
.	.	kIx.	.
</s>
<s>
Informační	informační	k2eAgFnSc4d1	informační
funkci	funkce	k1gFnSc4	funkce
mají	mít	k5eAaImIp3nP	mít
právě	právě	k9	právě
báze	báze	k1gFnSc2	báze
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
adenin	adenin	k1gInSc4	adenin
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
guanin	guanin	k1gInSc1	guanin
(	(	kIx(	(
<g/>
G	G	kA	G
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cytosin	cytosin	k1gMnSc1	cytosin
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
thymin	thymin	k1gInSc1	thymin
(	(	kIx(	(
<g/>
T	T	kA	T
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
dvě	dva	k4xCgFnPc1	dva
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
puriny	purin	k1gInPc4	purin
<g/>
,	,	kIx,	,
zbylé	zbylý	k2eAgInPc4d1	zbylý
mezi	mezi	k7c7	mezi
tzv.	tzv.	kA	tzv.
pyrimidiny	pyrimidin	k1gInPc7	pyrimidin
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
vlákna	vlákna	k1gFnSc1	vlákna
DNA	DNA	kA	DNA
se	se	k3xPyFc4	se
často	často	k6eAd1	často
spojují	spojovat	k5eAaImIp3nP	spojovat
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
dvoušroubovici	dvoušroubovice	k1gFnSc4	dvoušroubovice
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
tvar	tvar	k1gInSc4	tvar
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
slavný	slavný	k2eAgMnSc1d1	slavný
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
kulturní	kulturní	k2eAgFnSc7d1	kulturní
ikonou	ikona	k1gFnSc7	ikona
moderní	moderní	k2eAgFnSc2d1	moderní
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Dvoušroubovici	Dvoušroubovice	k1gFnSc4	Dvoušroubovice
DNA	dno	k1gNnSc2	dno
tvoří	tvořit	k5eAaImIp3nS	tvořit
dvě	dva	k4xCgFnPc4	dva
navzájem	navzájem	k6eAd1	navzájem
spletené	spletený	k2eAgFnSc2d1	spletená
šroubovice	šroubovice	k1gFnSc2	šroubovice
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
mířící	mířící	k2eAgInSc4d1	mířící
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
antiparalelní	antiparalelní	k2eAgFnPc1d1	antiparalelní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
protilehlými	protilehlý	k2eAgFnPc7d1	protilehlá
bázemi	báze	k1gFnPc7	báze
obou	dva	k4xCgInPc2	dva
vláken	vlákno	k1gNnPc2	vlákno
se	se	k3xPyFc4	se
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
vodíkové	vodíkový	k2eAgInPc1d1	vodíkový
můstky	můstek	k1gInPc1	můstek
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
tři	tři	k4xCgFnPc4	tři
mezi	mezi	k7c7	mezi
guaninem	guanin	k1gInSc7	guanin
a	a	k8xC	a
cytosinem	cytosin	k1gInSc7	cytosin
nebo	nebo	k8xC	nebo
dva	dva	k4xCgInPc4	dva
mezi	mezi	k7c7	mezi
adeninem	adenin	k1gInSc7	adenin
a	a	k8xC	a
thyminem	thymin	k1gInSc7	thymin
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
způsoby	způsob	k1gInPc4	způsob
uspořádání	uspořádání	k1gNnSc2	uspořádání
řetězců	řetězec	k1gInPc2	řetězec
<g/>
,	,	kIx,	,
vymykající	vymykající	k2eAgFnSc6d1	vymykající
se	se	k3xPyFc4	se
tradiční	tradiční	k2eAgFnSc3d1	tradiční
představě	představa	k1gFnSc3	představa
dvoušroubovice	dvoušroubovice	k1gFnSc2	dvoušroubovice
<g/>
.	.	kIx.	.
</s>
<s>
Deoxyribonukleová	deoxyribonukleový	k2eAgFnSc1d1	deoxyribonukleová
kyselina	kyselina	k1gFnSc1	kyselina
je	být	k5eAaImIp3nS	být
středem	střed	k1gInSc7	střed
zájmu	zájem	k1gInSc2	zájem
vědců	vědec	k1gMnPc2	vědec
nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
biologických	biologický	k2eAgInPc2d1	biologický
oborů	obor	k1gInPc2	obor
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
vyvinuty	vyvinout	k5eAaPmNgFnP	vyvinout
promyšlené	promyšlený	k2eAgFnPc1d1	promyšlená
techniky	technika	k1gFnPc1	technika
její	její	k3xOp3gFnSc2	její
izolace	izolace	k1gFnSc2	izolace
<g/>
,	,	kIx,	,
separace	separace	k1gFnSc2	separace
<g/>
,	,	kIx,	,
barvení	barvení	k1gNnSc2	barvení
<g/>
,	,	kIx,	,
sekvenování	sekvenování	k1gNnSc2	sekvenování
<g/>
,	,	kIx,	,
umělé	umělý	k2eAgFnSc2d1	umělá
syntézy	syntéza	k1gFnSc2	syntéza
a	a	k8xC	a
manipulace	manipulace	k1gFnSc2	manipulace
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
pomocí	pomoc	k1gFnSc7	pomoc
metod	metoda	k1gFnPc2	metoda
genového	genový	k2eAgNnSc2d1	genové
inženýrství	inženýrství	k1gNnSc2	inženýrství
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
postupy	postup	k1gInPc1	postup
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgInPc1d1	důležitý
i	i	k9	i
pro	pro	k7c4	pro
lékaře	lékař	k1gMnPc4	lékař
<g/>
,	,	kIx,	,
kriminalisty	kriminalista	k1gMnPc4	kriminalista
či	či	k8xC	či
evoluční	evoluční	k2eAgMnPc4d1	evoluční
biology	biolog	k1gMnPc4	biolog
–	–	k?	–
DNA	dna	k1gFnSc1	dna
je	být	k5eAaImIp3nS	být
zásadním	zásadní	k2eAgInSc7d1	zásadní
materiálem	materiál	k1gInSc7	materiál
v	v	k7c6	v
diagnostice	diagnostika	k1gFnSc6	diagnostika
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
,	,	kIx,	,
testech	test	k1gInPc6	test
otcovství	otcovství	k1gNnSc4	otcovství
<g/>
,	,	kIx,	,
při	při	k7c6	při
vyšetřování	vyšetřování	k1gNnSc6	vyšetřování
zločinů	zločin	k1gInPc2	zločin
<g/>
,	,	kIx,	,
přípravě	příprava	k1gFnSc3	příprava
plodin	plodina	k1gFnPc2	plodina
s	s	k7c7	s
novými	nový	k2eAgFnPc7d1	nová
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
či	či	k8xC	či
třeba	třeba	k6eAd1	třeba
hledání	hledání	k1gNnSc1	hledání
příbuzenských	příbuzenský	k2eAgInPc2d1	příbuzenský
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
organismy	organismus	k1gInPc7	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
dějiny	dějiny	k1gFnPc1	dějiny
objevu	objev	k1gInSc2	objev
a	a	k8xC	a
výzkumu	výzkum	k1gInSc2	výzkum
DNA	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Deoxyribonukleová	deoxyribonukleový	k2eAgFnSc1d1	deoxyribonukleová
kyselina	kyselina	k1gFnSc1	kyselina
byla	být	k5eAaImAgFnS	být
popsána	popsán	k2eAgFnSc1d1	popsána
roku	rok	k1gInSc2	rok
1869	[number]	k4	1869
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
lékař	lékař	k1gMnSc1	lékař
Friedrich	Friedrich	k1gMnSc1	Friedrich
Miescher	Mieschra	k1gFnPc2	Mieschra
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
složení	složení	k1gNnSc3	složení
hnisu	hnis	k1gInSc2	hnis
z	z	k7c2	z
nemocničních	nemocniční	k2eAgInPc2d1	nemocniční
obvazů	obvaz	k1gInPc2	obvaz
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jader	jádro	k1gNnPc2	jádro
bílých	bílý	k2eAgFnPc2d1	bílá
krvinek	krvinka	k1gFnPc2	krvinka
přítomných	přítomný	k2eAgFnPc2d1	přítomná
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
hnisu	hnis	k1gInSc6	hnis
získal	získat	k5eAaPmAgMnS	získat
jisté	jistý	k2eAgNnSc4d1	jisté
množství	množství	k1gNnSc4	množství
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
souhrnně	souhrnně	k6eAd1	souhrnně
nazýval	nazývat	k5eAaImAgMnS	nazývat
nuklein	nuklein	k1gMnSc1	nuklein
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Phoebus	Phoebus	k1gInSc1	Phoebus
Levene	Leven	k1gInSc5	Leven
rozpoznal	rozpoznat	k5eAaPmAgMnS	rozpoznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
DNA	DNA	kA	DNA
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
cukrů	cukr	k1gInPc2	cukr
<g/>
,	,	kIx,	,
fosfátů	fosfát	k1gInPc2	fosfát
a	a	k8xC	a
bází	báze	k1gFnPc2	báze
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
funkci	funkce	k1gFnSc6	funkce
DNA	dno	k1gNnPc4	dno
toho	ten	k3xDgNnSc2	ten
dlouho	dlouho	k6eAd1	dlouho
nebylo	být	k5eNaImAgNnS	být
moc	moc	k6eAd1	moc
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
důkaz	důkaz	k1gInSc1	důkaz
o	o	k7c4	o
roli	role	k1gFnSc4	role
DNA	DNA	kA	DNA
v	v	k7c6	v
přenosu	přenos	k1gInSc6	přenos
genetické	genetický	k2eAgFnPc4d1	genetická
informace	informace	k1gFnPc4	informace
přinesl	přinést	k5eAaPmAgInS	přinést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
Averyho-MacLeodův-McCartyho	Averyho-MacLeodův-McCarty	k1gMnSc2	Averyho-MacLeodův-McCarty
experiment	experiment	k1gInSc4	experiment
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
provedli	provést	k5eAaPmAgMnP	provést
Oswald	Oswald	k1gInSc4	Oswald
Avery	Avera	k1gFnSc2	Avera
společně	společně	k6eAd1	společně
s	s	k7c7	s
Colinem	Colin	k1gMnSc7	Colin
MacLeodem	MacLeod	k1gMnSc7	MacLeod
a	a	k8xC	a
Maclynem	Maclyn	k1gInSc7	Maclyn
McCartym	McCartym	k1gInSc1	McCartym
<g/>
.	.	kIx.	.
</s>
<s>
Sérií	série	k1gFnSc7	série
pokusů	pokus	k1gInPc2	pokus
s	s	k7c7	s
transformací	transformace	k1gFnSc7	transformace
pneumokoků	pneumokok	k1gInPc2	pneumokok
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
DNA	DNA	kA	DNA
je	být	k5eAaImIp3nS	být
genetickým	genetický	k2eAgInSc7d1	genetický
materiálem	materiál	k1gInSc7	materiál
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
důkaz	důkaz	k1gInSc1	důkaz
přinesl	přinést	k5eAaPmAgInS	přinést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
Hersheyho-Chaseové	Hersheyho-Chaseové	k2eAgInSc1d1	Hersheyho-Chaseové
experiment	experiment	k1gInSc1	experiment
<g/>
.	.	kIx.	.
</s>
<s>
Patrně	patrně	k6eAd1	patrně
nejslavnějším	slavný	k2eAgInSc7d3	nejslavnější
milníkem	milník	k1gInSc7	milník
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
DNA	dno	k1gNnSc2	dno
bylo	být	k5eAaImAgNnS	být
odhalení	odhalení	k1gNnSc4	odhalení
její	její	k3xOp3gFnSc2	její
trojrozměrné	trojrozměrný	k2eAgFnSc2d1	trojrozměrná
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Správný	správný	k2eAgInSc4d1	správný
dvoušroubovicový	dvoušroubovicový	k2eAgInSc4d1	dvoušroubovicový
model	model	k1gInSc4	model
poprvé	poprvé	k6eAd1	poprvé
představili	představit	k5eAaPmAgMnP	představit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Nature	Natur	k1gMnSc5	Natur
James	James	k1gMnSc1	James
D.	D.	kA	D.
Watson	Watson	k1gMnSc1	Watson
a	a	k8xC	a
Francis	Francis	k1gFnSc1	Francis
Crick	Cricko	k1gNnPc2	Cricko
<g/>
,	,	kIx,	,
pozdější	pozdní	k2eAgMnPc1d2	pozdější
laureáti	laureát	k1gMnPc1	laureát
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Vycházeli	vycházet	k5eAaImAgMnP	vycházet
přitom	přitom	k6eAd1	přitom
z	z	k7c2	z
rentgenové	rentgenový	k2eAgFnSc2d1	rentgenová
difrakční	difrakční	k2eAgFnSc2d1	difrakční
analýzy	analýza	k1gFnSc2	analýza
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
o	o	k7c4	o
rok	rok	k1gInSc4	rok
dříve	dříve	k6eAd2	dříve
provedli	provést	k5eAaPmAgMnP	provést
Rosalind	Rosalinda	k1gFnPc2	Rosalinda
Franklinová	Franklinový	k2eAgFnSc1d1	Franklinová
a	a	k8xC	a
Raymond	Raymond	k1gInSc1	Raymond
Gosling	Gosling	k1gInSc1	Gosling
a	a	k8xC	a
publikovali	publikovat	k5eAaBmAgMnP	publikovat
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
čísle	číslo	k1gNnSc6	číslo
Nature	Natur	k1gInSc5	Natur
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
článek	článek	k1gInSc1	článek
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
vydání	vydání	k1gNnSc6	vydání
předložil	předložit	k5eAaPmAgMnS	předložit
i	i	k8xC	i
Maurice	Maurika	k1gFnSc3	Maurika
Wilkins	Wilkinsa	k1gFnPc2	Wilkinsa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
předložil	předložit	k5eAaPmAgMnS	předložit
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
slavný	slavný	k2eAgInSc1d1	slavný
Crick	Crick	k1gInSc1	Crick
sérii	série	k1gFnSc4	série
pravidel	pravidlo	k1gNnPc2	pravidlo
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
centrální	centrální	k2eAgNnSc4d1	centrální
dogma	dogma	k1gNnSc4	dogma
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
a	a	k8xC	a
popisují	popisovat	k5eAaImIp3nP	popisovat
vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
RNA	RNA	kA	RNA
a	a	k8xC	a
proteiny	protein	k1gInPc1	protein
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
slavný	slavný	k2eAgInSc1d1	slavný
Meselsonův-Stahlův	Meselsonův-Stahlův	k2eAgInSc1d1	Meselsonův-Stahlův
experiment	experiment	k1gInSc1	experiment
umožnil	umožnit	k5eAaPmAgInS	umožnit
poznat	poznat	k5eAaPmF	poznat
způsob	způsob	k1gInSc1	způsob
replikace	replikace	k1gFnSc2	replikace
DNA	DNA	kA	DNA
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Genetický	genetický	k2eAgInSc4d1	genetický
kód	kód	k1gInSc4	kód
rozluštili	rozluštit	k5eAaPmAgMnP	rozluštit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
Har	Har	k1gMnSc1	Har
Gobind	Gobind	k1gMnSc1	Gobind
Khorana	Khorana	k1gFnSc1	Khorana
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
W.	W.	kA	W.
Holley	Holle	k1gMnPc7	Holle
a	a	k8xC	a
Marshall	Marshall	k1gMnSc1	Marshall
Warren	Warrna	k1gFnPc2	Warrna
Nirenberg	Nirenberg	k1gMnSc1	Nirenberg
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
DNA	DNA	kA	DNA
a	a	k8xC	a
RNA	RNA	kA	RNA
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
společnou	společný	k2eAgFnSc7d1	společná
vlastností	vlastnost	k1gFnSc7	vlastnost
všech	všecek	k3xTgInPc2	všecek
známých	známý	k2eAgInPc2d1	známý
pozemských	pozemský	k2eAgInPc2d1	pozemský
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Veškerý	veškerý	k3xTgInSc1	veškerý
život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c4	na
koexistenci	koexistence	k1gFnSc4	koexistence
těchto	tento	k3xDgFnPc2	tento
nukleových	nukleový	k2eAgFnPc2d1	nukleová
kyselin	kyselina	k1gFnPc2	kyselina
s	s	k7c7	s
bílkovinami	bílkovina	k1gFnPc7	bílkovina
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
DNA	DNA	kA	DNA
a	a	k8xC	a
bílkovinami	bílkovina	k1gFnPc7	bílkovina
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
hypotéz	hypotéza	k1gFnPc2	hypotéza
nejprve	nejprve	k6eAd1	nejprve
existovaly	existovat	k5eAaImAgFnP	existovat
bílkoviny	bílkovina	k1gFnPc1	bílkovina
a	a	k8xC	a
až	až	k9	až
následně	následně	k6eAd1	následně
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
nukleové	nukleový	k2eAgFnPc1d1	nukleová
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nejvíce	hodně	k6eAd3	hodně
příznivců	příznivec	k1gMnPc2	příznivec
má	mít	k5eAaImIp3nS	mít
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
představa	představa	k1gFnSc1	představa
<g/>
,	,	kIx,	,
že	že	k8xS	že
prapůvodní	prapůvodní	k2eAgFnSc7d1	prapůvodní
látkou	látka	k1gFnSc7	látka
byla	být	k5eAaImAgFnS	být
nukleová	nukleový	k2eAgFnSc1d1	nukleová
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
biologické	biologický	k2eAgFnPc4d1	biologická
evoluce	evoluce	k1gFnPc4	evoluce
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
teorie	teorie	k1gFnSc2	teorie
RNA	RNA	kA	RNA
světa	svět	k1gInSc2	svět
však	však	k9	však
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
hrála	hrát	k5eAaImAgFnS	hrát
nejprve	nejprve	k6eAd1	nejprve
spíše	spíše	k9	spíše
RNA	RNA	kA	RNA
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
posléze	posléze	k6eAd1	posléze
přejala	přejmout	k5eAaPmAgFnS	přejmout
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Doklady	doklad	k1gInPc1	doklad
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
takových	takový	k3xDgFnPc2	takový
hypotéz	hypotéza	k1gFnPc2	hypotéza
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
vždy	vždy	k6eAd1	vždy
nepřímé	přímý	k2eNgNnSc1d1	nepřímé
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nejsou	být	k5eNaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
dostatečně	dostatečně	k6eAd1	dostatečně
staré	starý	k2eAgMnPc4d1	starý
vzorky	vzorek	k1gInPc4	vzorek
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
již	již	k6eAd1	již
před	před	k7c7	před
několika	několik	k4yIc7	několik
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
už	už	k6eAd1	už
po	po	k7c6	po
několika	několik	k4yIc6	několik
desítkách	desítka	k1gFnPc6	desítka
tisíců	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
klesá	klesat	k5eAaImIp3nS	klesat
množství	množství	k1gNnSc1	množství
DNA	dno	k1gNnSc2	dno
na	na	k7c4	na
setinu	setina	k1gFnSc4	setina
původního	původní	k2eAgInSc2d1	původní
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Nature	Natur	k1gMnSc5	Natur
z	z	k7c2	z
let	léto	k1gNnPc2	léto
2000	[number]	k4	2000
a	a	k8xC	a
2002	[number]	k4	2002
nicméně	nicméně	k8xC	nicméně
popisují	popisovat	k5eAaImIp3nP	popisovat
nález	nález	k1gInSc4	nález
až	až	k9	až
450	[number]	k4	450
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
starých	starý	k2eAgInPc2d1	starý
vzorků	vzorek	k1gInPc2	vzorek
bakteriální	bakteriální	k2eAgFnSc1d1	bakteriální
DNA	dna	k1gFnSc1	dna
uchovaných	uchovaný	k2eAgMnPc2d1	uchovaný
v	v	k7c6	v
solných	solný	k2eAgInPc6d1	solný
krystalech	krystal	k1gInPc6	krystal
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgMnPc2d1	další
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
nebo	nebo	k8xC	nebo
méně	málo	k6eAd2	málo
spolehlivých	spolehlivý	k2eAgFnPc2d1	spolehlivá
studií	studie	k1gFnPc2	studie
<g/>
.	.	kIx.	.
</s>
<s>
Stavbu	stavba	k1gFnSc4	stavba
DNA	dno	k1gNnSc2	dno
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
zkoumat	zkoumat	k5eAaImF	zkoumat
na	na	k7c6	na
několika	několik	k4yIc6	několik
úrovních	úroveň	k1gFnPc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Pořadí	pořadí	k1gNnSc1	pořadí
nukleotidů	nukleotid	k1gInPc2	nukleotid
v	v	k7c6	v
lineárním	lineární	k2eAgNnSc6d1	lineární
dvouvlákně	dvouvlákno	k1gNnSc6	dvouvlákno
je	být	k5eAaImIp3nS	být
záležitostí	záležitost	k1gFnSc7	záležitost
tzv.	tzv.	kA	tzv.
primární	primární	k2eAgFnSc2d1	primární
struktury	struktura	k1gFnSc2	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Stáčení	stáčení	k1gNnSc1	stáčení
vlákna	vlákno	k1gNnSc2	vlákno
do	do	k7c2	do
dvoušroubovice	dvoušroubovice	k1gFnSc2	dvoušroubovice
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
sekundární	sekundární	k2eAgFnSc1d1	sekundární
struktura	struktura	k1gFnSc1	struktura
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
pod	pod	k7c7	pod
tzv.	tzv.	kA	tzv.
terciární	terciární	k2eAgFnSc7d1	terciární
strukturou	struktura	k1gFnSc7	struktura
se	se	k3xPyFc4	se
rozumí	rozumět	k5eAaImIp3nS	rozumět
obvykle	obvykle	k6eAd1	obvykle
nadšroubovicové	nadšroubovicový	k2eAgNnSc1d1	nadšroubovicový
vinutí	vinutí	k1gNnSc1	vinutí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
kondenzaci	kondenzace	k1gFnSc4	kondenzace
DNA	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
nukleová	nukleový	k2eAgFnSc1d1	nukleová
kyselina	kyselina	k1gFnSc1	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
DNA	DNA	kA	DNA
vlastně	vlastně	k9	vlastně
není	být	k5eNaImIp3nS	být
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
než	než	k8xS	než
velmi	velmi	k6eAd1	velmi
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
lineární	lineární	k2eAgInSc1d1	lineární
řetězec	řetězec	k1gInSc1	řetězec
nukleotidů	nukleotid	k1gInPc2	nukleotid
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
uvnitř	uvnitř	k7c2	uvnitř
každého	každý	k3xTgInSc2	každý
virionu	virion	k1gInSc2	virion
planých	planý	k2eAgFnPc2d1	planá
neštovic	neštovice	k1gFnPc2	neštovice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
DNA	DNA	kA	DNA
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
193	[number]	k4	193
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
,	,	kIx,	,
kruhová	kruhový	k2eAgFnSc1d1	kruhová
DNA	dna	k1gFnSc1	dna
u	u	k7c2	u
Escherichia	Escherichium	k1gNnSc2	Escherichium
coli	coli	k6eAd1	coli
má	mít	k5eAaImIp3nS	mít
délku	délka	k1gFnSc4	délka
1	[number]	k4	1
600	[number]	k4	600
mikrometrů	mikrometr	k1gInPc2	mikrometr
(	(	kIx(	(
<g/>
1,6	[number]	k4	1,6
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lidský	lidský	k2eAgInSc1d1	lidský
genom	genom	k1gInSc1	genom
je	být	k5eAaImIp3nS	být
rozložen	rozložit	k5eAaPmNgInS	rozložit
do	do	k7c2	do
23	[number]	k4	23
lineárních	lineární	k2eAgFnPc2d1	lineární
molekul	molekula	k1gFnPc2	molekula
DNA	DNA	kA	DNA
(	(	kIx(	(
<g/>
v	v	k7c6	v
haploidním	haploidní	k2eAgInSc6d1	haploidní
stavu	stav	k1gInSc6	stav
<g/>
)	)	kIx)	)
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
délce	délka	k1gFnSc6	délka
1	[number]	k4	1
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Nukleotid	nukleotid	k1gInSc1	nukleotid
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
stavební	stavební	k2eAgFnSc7d1	stavební
jednotkou	jednotka	k1gFnSc7	jednotka
všech	všecek	k3xTgFnPc2	všecek
molekul	molekula	k1gFnPc2	molekula
DNA	dno	k1gNnSc2	dno
<g/>
;	;	kIx,	;
existují	existovat	k5eAaImIp3nP	existovat
přitom	přitom	k6eAd1	přitom
čtyři	čtyři	k4xCgInPc4	čtyři
základní	základní	k2eAgInPc4d1	základní
typy	typ	k1gInPc4	typ
nukleotidů	nukleotid	k1gInPc2	nukleotid
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
DNA	DNA	kA	DNA
přirozeně	přirozeně	k6eAd1	přirozeně
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
čtyři	čtyři	k4xCgInPc1	čtyři
nukleotidy	nukleotid	k1gInPc1	nukleotid
(	(	kIx(	(
<g/>
dATP	dATP	k?	dATP
<g/>
,	,	kIx,	,
dGTP	dGTP	k?	dGTP
<g/>
,	,	kIx,	,
dCTP	dCTP	k?	dCTP
<g/>
,	,	kIx,	,
dTTP	dTTP	k?	dTTP
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
liší	lišit	k5eAaImIp3nS	lišit
typem	typ	k1gInSc7	typ
přivěšené	přivěšený	k2eAgFnSc2d1	přivěšená
nukleové	nukleový	k2eAgFnSc2d1	nukleová
báze	báze	k1gFnSc2	báze
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
především	především	k9	především
adenin	adenin	k1gInSc1	adenin
<g/>
,	,	kIx,	,
guanin	guanin	k1gInSc1	guanin
<g/>
,	,	kIx,	,
cytosin	cytosin	k1gInSc1	cytosin
či	či	k8xC	či
thymin	thymin	k1gInSc1	thymin
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgInSc1	každý
nukleotid	nukleotid	k1gInSc1	nukleotid
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgFnPc4	tři
důležité	důležitý	k2eAgFnPc4d1	důležitá
stavební	stavební	k2eAgFnPc4d1	stavební
součásti	součást	k1gFnPc4	součást
<g/>
:	:	kIx,	:
deoxyribóza	deoxyribóza	k1gFnSc1	deoxyribóza
–	–	k?	–
pětiuhlíkový	pětiuhlíkový	k2eAgInSc4d1	pětiuhlíkový
cukr	cukr	k1gInSc4	cukr
(	(	kIx(	(
<g/>
pentóza	pentóza	k1gFnSc1	pentóza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
DNA	DNA	kA	DNA
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
cyklické	cyklický	k2eAgFnSc6d1	cyklická
furanózové	furanózový	k2eAgFnSc6d1	furanózový
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
uhlíky	uhlík	k1gInPc1	uhlík
se	se	k3xPyFc4	se
po	po	k7c6	po
směru	směr	k1gInSc6	směr
pohybu	pohyb	k1gInSc2	pohyb
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
označují	označovat	k5eAaImIp3nP	označovat
1	[number]	k4	1
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
'	'	kIx"	'
a	a	k8xC	a
5	[number]	k4	5
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
na	na	k7c4	na
1	[number]	k4	1
<g/>
'	'	kIx"	'
uhlíku	uhlík	k1gInSc2	uhlík
je	být	k5eAaImIp3nS	být
navěšena	navěšen	k2eAgFnSc1d1	navěšena
nukleová	nukleový	k2eAgFnSc1d1	nukleová
báze	báze	k1gFnSc1	báze
<g/>
,	,	kIx,	,
na	na	k7c4	na
3	[number]	k4	3
<g/>
'	'	kIx"	'
a	a	k8xC	a
5	[number]	k4	5
<g/>
'	'	kIx"	'
uhlíku	uhlík	k1gInSc2	uhlík
jsou	být	k5eAaImIp3nP	být
přes	přes	k7c4	přes
OH	OH	kA	OH
skupinu	skupina	k1gFnSc4	skupina
připevněny	připevněn	k2eAgFnPc1d1	připevněna
fosfátové	fosfátový	k2eAgFnPc1d1	fosfátová
skupiny	skupina	k1gFnPc1	skupina
<g/>
;	;	kIx,	;
fosfát	fosfát	k1gInSc1	fosfát
–	–	k?	–
vazebný	vazebný	k2eAgInSc1d1	vazebný
zbytek	zbytek	k1gInSc1	zbytek
kyseliny	kyselina	k1gFnSc2	kyselina
ortofosforečné	ortofosforečný	k2eAgFnSc2d1	ortofosforečná
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
navázán	navázat	k5eAaPmNgInS	navázat
na	na	k7c4	na
5	[number]	k4	5
<g/>
'	'	kIx"	'
uhlíku	uhlík	k1gInSc2	uhlík
každého	každý	k3xTgInSc2	každý
nukleotidu	nukleotid	k1gInSc2	nukleotid
<g/>
.	.	kIx.	.
</s>
<s>
Záporný	záporný	k2eAgInSc1d1	záporný
náboj	náboj	k1gInSc1	náboj
na	na	k7c6	na
fosforečnanu	fosforečnan	k1gInSc6	fosforečnan
je	být	k5eAaImIp3nS	být
důvodem	důvod	k1gInSc7	důvod
celkového	celkový	k2eAgInSc2d1	celkový
negativního	negativní	k2eAgInSc2d1	negativní
náboje	náboj	k1gInSc2	náboj
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Fosfátová	fosfátový	k2eAgFnSc1d1	fosfátová
skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
můstek	můstek	k1gInSc1	můstek
propojující	propojující	k2eAgInSc1d1	propojující
5	[number]	k4	5
<g/>
'	'	kIx"	'
uhlík	uhlík	k1gInSc1	uhlík
každé	každý	k3xTgFnSc2	každý
deoxyribózy	deoxyribóza	k1gFnSc2	deoxyribóza
s	s	k7c7	s
3	[number]	k4	3
<g/>
'	'	kIx"	'
uhlíkem	uhlík	k1gInSc7	uhlík
předchozí	předchozí	k2eAgFnSc2d1	předchozí
deoxyribózy	deoxyribóza	k1gFnSc2	deoxyribóza
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
cukr-fosfátová	cukrosfátový	k2eAgFnSc1d1	cukr-fosfátový
kostra	kostra	k1gFnSc1	kostra
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
nukleová	nukleový	k2eAgFnSc1d1	nukleová
báze	báze	k1gFnSc1	báze
–	–	k?	–
dusíkatá	dusíkatý	k2eAgFnSc1d1	dusíkatá
heterocyklická	heterocyklický	k2eAgFnSc1d1	heterocyklická
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
DNA	DNA	kA	DNA
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
kombinacích	kombinace	k1gFnPc6	kombinace
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
především	především	k9	především
čtyři	čtyři	k4xCgFnPc4	čtyři
základní	základní	k2eAgFnPc4d1	základní
nukleové	nukleový	k2eAgFnPc4d1	nukleová
báze	báze	k1gFnPc4	báze
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc4	dva
purinové	purinový	k2eAgFnPc4d1	purinová
(	(	kIx(	(
<g/>
adenin	adenin	k1gInSc4	adenin
[	[	kIx(	[
<g/>
A	a	k9	a
<g/>
]	]	kIx)	]
a	a	k8xC	a
guanin	guanin	k1gInSc1	guanin
[	[	kIx(	[
<g/>
G	G	kA	G
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
pyrimidinové	pyrimidinový	k2eAgFnPc4d1	pyrimidinová
(	(	kIx(	(
<g/>
thymin	thymin	k1gInSc4	thymin
[	[	kIx(	[
<g/>
T	T	kA	T
<g/>
]	]	kIx)	]
a	a	k8xC	a
cytosin	cytosin	k1gMnSc1	cytosin
[	[	kIx(	[
<g/>
C	C	kA	C
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
čtyř	čtyři	k4xCgFnPc2	čtyři
nukleových	nukleový	k2eAgFnPc2d1	nukleová
bází	báze	k1gFnPc2	báze
je	být	k5eAaImIp3nS	být
připojena	připojit	k5eAaPmNgFnS	připojit
na	na	k7c4	na
1	[number]	k4	1
<g/>
'	'	kIx"	'
uhlíku	uhlík	k1gInSc2	uhlík
deoxyribózy	deoxyribóza	k1gFnSc2	deoxyribóza
pomocí	pomocí	k7c2	pomocí
N-glykosidové	Nlykosidový	k2eAgFnSc2d1	N-glykosidový
vazby	vazba	k1gFnSc2	vazba
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
existence	existence	k1gFnSc1	existence
čtyř	čtyři	k4xCgFnPc2	čtyři
nukleových	nukleový	k2eAgFnPc2d1	nukleová
bází	báze	k1gFnPc2	báze
je	být	k5eAaImIp3nS	být
zásadní	zásadní	k2eAgInSc1d1	zásadní
pro	pro	k7c4	pro
informační	informační	k2eAgFnPc4d1	informační
vlastnosti	vlastnost	k1gFnPc4	vlastnost
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
také	také	k9	také
schopnost	schopnost	k1gFnSc4	schopnost
nukleových	nukleový	k2eAgFnPc2d1	nukleová
bází	báze	k1gFnPc2	báze
vytvářet	vytvářet	k5eAaImF	vytvářet
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
vodíkových	vodíkový	k2eAgInPc2d1	vodíkový
můstků	můstek	k1gInPc2	můstek
<g/>
.	.	kIx.	.
</s>
<s>
Primární	primární	k2eAgFnSc1d1	primární
struktura	struktura	k1gFnSc1	struktura
DNA	dno	k1gNnSc2	dno
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
znázornit	znázornit	k5eAaPmF	znázornit
jako	jako	k9	jako
lineární	lineární	k2eAgFnSc1d1	lineární
řada	řada	k1gFnSc1	řada
nukleotidů	nukleotid	k1gInPc2	nukleotid
nebo	nebo	k8xC	nebo
třeba	třeba	k6eAd1	třeba
jako	jako	k9	jako
řada	řada	k1gFnSc1	řada
písmen	písmeno	k1gNnPc2	písmeno
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
dusíkatým	dusíkatý	k2eAgFnPc3d1	dusíkatá
bázím	báze	k1gFnPc3	báze
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
nukleotidech	nukleotid	k1gInPc6	nukleotid
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
že	že	k8xS	že
DNA	dna	k1gFnSc1	dna
je	být	k5eAaImIp3nS	být
směrovaná	směrovaný	k2eAgFnSc1d1	směrovaná
(	(	kIx(	(
<g/>
direkcionalizovaná	direkcionalizovaný	k2eAgFnSc1d1	direkcionalizovaný
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
dají	dát	k5eAaPmIp3nP	dát
se	se	k3xPyFc4	se
jednoznačně	jednoznačně	k6eAd1	jednoznačně
odlišit	odlišit	k5eAaPmF	odlišit
oba	dva	k4xCgInPc4	dva
konce	konec	k1gInPc4	konec
<g/>
.	.	kIx.	.
</s>
<s>
Směr	směr	k1gInSc1	směr
vláken	vlákna	k1gFnPc2	vlákna
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
právě	právě	k9	právě
podle	podle	k7c2	podle
orientace	orientace	k1gFnSc2	orientace
deoxyribózy	deoxyribóza	k1gFnSc2	deoxyribóza
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
<g/>
:	:	kIx,	:
směr	směr	k1gInSc1	směr
3	[number]	k4	3
<g/>
'	'	kIx"	'
<g/>
→	→	k?	→
<g/>
5	[number]	k4	5
<g/>
'	'	kIx"	'
a	a	k8xC	a
opačný	opačný	k2eAgInSc1d1	opačný
směr	směr	k1gInSc1	směr
5	[number]	k4	5
<g/>
'	'	kIx"	'
<g/>
→	→	k?	→
<g/>
3	[number]	k4	3
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
konvence	konvence	k1gFnSc2	konvence
se	se	k3xPyFc4	se
pořadí	pořadí	k1gNnSc1	pořadí
nukleotidů	nukleotid	k1gInPc2	nukleotid
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
směrem	směr	k1gInSc7	směr
5	[number]	k4	5
<g/>
'	'	kIx"	'
<g/>
→	→	k?	→
<g/>
3	[number]	k4	3
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
např.	např.	kA	např.
TACGGACGGG	TACGGACGGG	kA	TACGGACGGG
AGAAGCGCGC	AGAAGCGCGC	kA	AGAAGCGCGC
GGGCGGGCCG	GGGCGGGCCG	kA	GGGCGGGCCG
je	být	k5eAaImIp3nS	být
prvních	první	k4xOgNnPc6	první
30	[number]	k4	30
z	z	k7c2	z
3675	[number]	k4	3675
nukleotidů	nukleotid	k1gInPc2	nukleotid
tvořících	tvořící	k2eAgFnPc2d1	tvořící
přepisovanou	přepisovaný	k2eAgFnSc4d1	přepisovaná
část	část	k1gFnSc4	část
genu	gen	k1gInSc2	gen
pro	pro	k7c4	pro
lidský	lidský	k2eAgInSc4d1	lidský
alfa-tubulin	alfaubulin	k2eAgInSc4d1	alfa-tubulin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
existenci	existence	k1gFnSc6	existence
bakterií	bakterium	k1gNnPc2	bakterium
GFAJ-	GFAJ-	k1gFnSc1	GFAJ-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
údajně	údajně	k6eAd1	údajně
ve	v	k7c4	v
své	svůj	k3xOyFgMnPc4	svůj
DNA	dna	k1gFnSc1	dna
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
místo	místo	k1gNnSc4	místo
fosfátových	fosfátový	k2eAgFnPc2d1	fosfátová
skupin	skupina	k1gFnPc2	skupina
arseničnany	arseničnana	k1gFnSc2	arseničnana
<g/>
.	.	kIx.	.
</s>
<s>
Hypotéza	hypotéza	k1gFnSc1	hypotéza
byla	být	k5eAaImAgFnS	být
definitivně	definitivně	k6eAd1	definitivně
vyvrácena	vyvrátit	k5eAaPmNgFnS	vyvrátit
v	v	k7c6	v
r.	r.	kA	r.
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
párování	párování	k1gNnSc2	párování
bází	báze	k1gFnPc2	báze
<g/>
.	.	kIx.	.
</s>
<s>
Deoxyribonukleová	deoxyribonukleový	k2eAgFnSc1d1	deoxyribonukleová
kyselina	kyselina	k1gFnSc1	kyselina
(	(	kIx(	(
<g/>
DNA	DNA	kA	DNA
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
existovat	existovat	k5eAaImF	existovat
jako	jako	k9	jako
samostatná	samostatný	k2eAgFnSc1d1	samostatná
jednovláknová	jednovláknový	k2eAgFnSc1d1	jednovláknová
molekula	molekula	k1gFnSc1	molekula
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
ssDNA	ssDNA	k?	ssDNA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
vícevláknové	vícevláknový	k2eAgFnPc4d1	vícevláknová
struktury	struktura	k1gFnPc4	struktura
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
složené	složený	k2eAgFnSc2d1	složená
z	z	k7c2	z
několika	několik	k4yIc2	několik
řetězců	řetězec	k1gInPc2	řetězec
spojených	spojený	k2eAgInPc2d1	spojený
vodíkovými	vodíkový	k2eAgInPc7d1	vodíkový
můstky	můstek	k1gInPc7	můstek
<g/>
.	.	kIx.	.
</s>
<s>
Vodíkové	vodíkový	k2eAgInPc1d1	vodíkový
můstky	můstek	k1gInPc1	můstek
jsou	být	k5eAaImIp3nP	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
typů	typ	k1gInPc2	typ
poměrně	poměrně	k6eAd1	poměrně
slabých	slabý	k2eAgFnPc2d1	slabá
vazebných	vazebný	k2eAgFnPc2d1	vazebná
interakcí	interakce	k1gFnPc2	interakce
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
či	či	k8xC	či
více	hodně	k6eAd2	hodně
vlákny	vlákna	k1gFnPc4	vlákna
DNA	DNA	kA	DNA
jich	on	k3xPp3gMnPc2	on
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
<g/>
;	;	kIx,	;
výsledná	výsledný	k2eAgFnSc1d1	výsledná
vícevláknová	vícevláknový	k2eAgFnSc1d1	vícevláknová
struktura	struktura	k1gFnSc1	struktura
tak	tak	k9	tak
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
.	.	kIx.	.
</s>
<s>
Typickou	typický	k2eAgFnSc7d1	typická
formou	forma	k1gFnSc7	forma
takového	takový	k3xDgNnSc2	takový
vícevláknového	vícevláknový	k2eAgNnSc2d1	vícevláknový
uspořádání	uspořádání	k1gNnSc2	uspořádání
DNA	dno	k1gNnSc2	dno
je	být	k5eAaImIp3nS	být
dvoušroubovice	dvoušroubovice	k1gFnSc1	dvoušroubovice
<g/>
,	,	kIx,	,
notoricky	notoricky	k6eAd1	notoricky
známá	známý	k2eAgFnSc1d1	známá
molekula	molekula	k1gFnSc1	molekula
DNA	DNA	kA	DNA
(	(	kIx(	(
<g/>
připomínající	připomínající	k2eAgInSc4d1	připomínající
"	"	kIx"	"
<g/>
stočený	stočený	k2eAgInSc4d1	stočený
žebřík	žebřík	k1gInSc4	žebřík
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
tvořená	tvořený	k2eAgFnSc1d1	tvořená
dvěma	dva	k4xCgInPc7	dva
lineárními	lineární	k2eAgInPc7d1	lineární
řetězci	řetězec	k1gInPc7	řetězec
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
pravidelná	pravidelný	k2eAgFnSc1d1	pravidelná
struktura	struktura	k1gFnSc1	struktura
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
vodíkových	vodíkový	k2eAgInPc2d1	vodíkový
můstků	můstek	k1gInPc2	můstek
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
žádoucí	žádoucí	k2eAgMnSc1d1	žádoucí
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vedle	vedle	k7c2	vedle
sebe	se	k3xPyFc2	se
"	"	kIx"	"
<g/>
v	v	k7c6	v
příčli	příčel	k1gInSc6	příčel
žebříku	žebřík	k1gInSc2	žebřík
<g/>
"	"	kIx"	"
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
vždy	vždy	k6eAd1	vždy
určité	určitý	k2eAgFnSc2d1	určitá
nukleové	nukleový	k2eAgFnSc2d1	nukleová
báze	báze	k1gFnSc2	báze
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
spolu	spolu	k6eAd1	spolu
ve	v	k7c6	v
správném	správný	k2eAgNnSc6d1	správné
prostorovém	prostorový	k2eAgNnSc6d1	prostorové
uspořádání	uspořádání	k1gNnSc6	uspořádání
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
několik	několik	k4yIc4	několik
vodíkových	vodíkový	k2eAgInPc2d1	vodíkový
můstků	můstek	k1gInPc2	můstek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
typickém	typický	k2eAgInSc6d1	typický
případě	případ	k1gInSc6	případ
(	(	kIx(	(
<g/>
ne	ne	k9	ne
však	však	k9	však
vždy	vždy	k6eAd1	vždy
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nukleové	nukleový	k2eAgFnSc2d1	nukleová
báze	báze	k1gFnSc2	báze
spojují	spojovat	k5eAaImIp3nP	spojovat
navzájem	navzájem	k6eAd1	navzájem
s	s	k7c7	s
odpovídající	odpovídající	k2eAgFnSc7d1	odpovídající
bází	báze	k1gFnSc7	báze
podle	podle	k7c2	podle
jednoduchého	jednoduchý	k2eAgInSc2d1	jednoduchý
klíče	klíč	k1gInSc2	klíč
<g/>
:	:	kIx,	:
A	a	k8xC	a
se	se	k3xPyFc4	se
páruje	párovat	k5eAaImIp3nS	párovat
s	s	k7c7	s
T	T	kA	T
(	(	kIx(	(
<g/>
vzájemně	vzájemně	k6eAd1	vzájemně
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
dvěma	dva	k4xCgFnPc7	dva
vodíkovými	vodíkový	k2eAgFnPc7d1	vodíková
vazbami	vazba	k1gFnPc7	vazba
<g/>
)	)	kIx)	)
G	G	kA	G
se	se	k3xPyFc4	se
páruje	párovat	k5eAaImIp3nS	párovat
s	s	k7c7	s
C	C	kA	C
(	(	kIx(	(
<g/>
vzájemně	vzájemně	k6eAd1	vzájemně
jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgFnP	spojit
třemi	tři	k4xCgFnPc7	tři
vodíkovými	vodíkový	k2eAgFnPc7d1	vodíková
vazbami	vazba	k1gFnPc7	vazba
<g/>
)	)	kIx)	)
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
komplementaritu	komplementarita	k1gFnSc4	komplementarita
bází	báze	k1gFnPc2	báze
<g/>
,	,	kIx,	,
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vychází	vycházet	k5eAaImIp3nS	vycházet
vzájemná	vzájemný	k2eAgFnSc1d1	vzájemná
komplementarita	komplementarita	k1gFnSc1	komplementarita
obou	dva	k4xCgFnPc2	dva
vláken	vlákna	k1gFnPc2	vlákna
DNA	dno	k1gNnSc2	dno
<g/>
.	.	kIx.	.
</s>
<s>
Vždy	vždy	k6eAd1	vždy
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
určité	určitý	k2eAgFnSc6d1	určitá
pozici	pozice	k1gFnSc6	pozice
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
jeden	jeden	k4xCgInSc4	jeden
nukleotid	nukleotid	k1gInSc4	nukleotid
z	z	k7c2	z
dvojice	dvojice	k1gFnSc2	dvojice
a	a	k8xC	a
v	v	k7c6	v
protějším	protější	k2eAgNnSc6d1	protější
vlákně	vlákno	k1gNnSc6	vlákno
druhý	druhý	k4xOgMnSc1	druhý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
z	z	k7c2	z
vláken	vlákno	k1gNnPc2	vlákno
tatáž	týž	k3xTgFnSc1	týž
informace	informace	k1gFnSc1	informace
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
vláken	vlákno	k1gNnPc2	vlákno
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
negativem	negativum	k1gNnSc7	negativum
<g/>
"	"	kIx"	"
vlákna	vlákno	k1gNnPc1	vlákno
druhého	druhý	k4xOgMnSc4	druhý
–	–	k?	–
podle	podle	k7c2	podle
jednoho	jeden	k4xCgNnSc2	jeden
vlákna	vlákno	k1gNnSc2	vlákno
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
přiřazením	přiřazení	k1gNnSc7	přiřazení
komplementárních	komplementární	k2eAgFnPc2d1	komplementární
bází	báze	k1gFnPc2	báze
vytvořit	vytvořit	k5eAaPmF	vytvořit
vlákno	vlákno	k1gNnSc4	vlákno
druhé	druhý	k4xOgFnSc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Poměr	poměr	k1gInSc1	poměr
AT	AT	kA	AT
a	a	k8xC	a
GC	GC	kA	GC
párů	pár	k1gInPc2	pár
v	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
DNA	dno	k1gNnSc2	dno
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
různý	různý	k2eAgMnSc1d1	různý
<g/>
:	:	kIx,	:
tzv.	tzv.	kA	tzv.
obsah	obsah	k1gInSc1	obsah
GC	GC	kA	GC
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
u	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
od	od	k7c2	od
25	[number]	k4	25
do	do	k7c2	do
75	[number]	k4	75
%	%	kIx~	%
<g/>
,	,	kIx,	,
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
39	[number]	k4	39
<g/>
–	–	k?	–
<g/>
46	[number]	k4	46
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
možností	možnost	k1gFnPc2	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
pomocí	pomocí	k7c2	pomocí
vodíkových	vodíkový	k2eAgInPc2d1	vodíkový
můstků	můstek	k1gInPc2	můstek
spárovat	spárovat	k5eAaImF	spárovat
báze	báze	k1gFnPc4	báze
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
atomů	atom	k1gInPc2	atom
schopných	schopný	k2eAgMnPc2d1	schopný
podílet	podílet	k5eAaImF	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
vodíkových	vodíkový	k2eAgFnPc2d1	vodíková
vazeb	vazba	k1gFnPc2	vazba
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
molekulách	molekula	k1gFnPc6	molekula
purinů	purin	k1gInPc2	purin
i	i	k9	i
pyrimidinů	pyrimidin	k1gInPc2	pyrimidin
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
<g/>
.	.	kIx.	.
</s>
<s>
Samostatnou	samostatný	k2eAgFnSc7d1	samostatná
kapitolou	kapitola	k1gFnSc7	kapitola
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
hoogsteenovské	hoogsteenovský	k2eAgNnSc1d1	hoogsteenovský
párování	párování	k1gNnSc1	párování
pojmenované	pojmenovaný	k2eAgNnSc1d1	pojmenované
podle	podle	k7c2	podle
Karsta	Karst	k1gMnSc2	Karst
Hoogsteena	Hoogsteen	k1gMnSc2	Hoogsteen
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
jako	jako	k8xC	jako
první	první	k4xOgFnSc7	první
popsal	popsat	k5eAaPmAgMnS	popsat
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
možností	možnost	k1gFnSc7	možnost
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
wobble	wobble	k6eAd1	wobble
párování	párování	k1gNnSc1	párování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
úsporné	úsporný	k2eAgNnSc1d1	úsporné
rozeznávání	rozeznávání	k1gNnSc1	rozeznávání
kodonů	kodon	k1gMnPc2	kodon
pomocí	pomocí	k7c2	pomocí
tRNA	trnout	k5eAaImSgInS	trnout
molekul	molekula	k1gFnPc2	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
wobble	wobble	k6eAd1	wobble
párování	párování	k1gNnPc2	párování
může	moct	k5eAaImIp3nS	moct
například	například	k6eAd1	například
guanin	guanin	k1gInSc4	guanin
vytvářet	vytvářet	k5eAaImF	vytvářet
vazbu	vazba	k1gFnSc4	vazba
s	s	k7c7	s
uracilem	uracil	k1gInSc7	uracil
<g/>
;	;	kIx,	;
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
rekrutován	rekrutován	k2eAgMnSc1d1	rekrutován
inosin	inosin	k1gMnSc1	inosin
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
obecné	obecný	k2eAgFnSc2d1	obecná
vazebné	vazebný	k2eAgFnSc2d1	vazebná
schopnosti	schopnost	k1gFnSc2	schopnost
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
vázat	vázat	k5eAaImF	vázat
se	se	k3xPyFc4	se
na	na	k7c4	na
C	C	kA	C
<g/>
,	,	kIx,	,
A	A	kA	A
a	a	k8xC	a
U.	U.	kA	U.
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
dvoušroubovice	dvoušroubovice	k1gFnSc2	dvoušroubovice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
drtivém	drtivý	k2eAgNnSc6d1	drtivé
procentu	procento	k1gNnSc6	procento
případů	případ	k1gInPc2	případ
se	s	k7c7	s
DNA	DNA	kA	DNA
za	za	k7c2	za
běžných	běžný	k2eAgFnPc2d1	běžná
podmínek	podmínka	k1gFnPc2	podmínka
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
pravotočivé	pravotočivý	k2eAgFnSc2d1	pravotočivá
dvoušroubovice	dvoušroubovice	k1gFnSc2	dvoušroubovice
<g/>
.	.	kIx.	.
</s>
<s>
Dvoušroubovice	Dvoušroubovice	k1gFnSc1	Dvoušroubovice
DNA	dno	k1gNnSc2	dno
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
dvěma	dva	k4xCgInPc7	dva
vlákny	vlákno	k1gNnPc7	vlákno
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
obtáčí	obtáčet	k5eAaImIp3nP	obtáčet
kolem	kolem	k7c2	kolem
společné	společný	k2eAgFnSc2d1	společná
osy	osa	k1gFnSc2	osa
a	a	k8xC	a
interagují	interagovat	k5eAaImIp3nP	interagovat
spolu	spolu	k6eAd1	spolu
<g/>
.	.	kIx.	.
</s>
<s>
Vlákna	vlákna	k1gFnSc1	vlákna
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
antiparalelní	antiparalelní	k2eAgMnSc1d1	antiparalelní
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
směřují	směřovat	k5eAaImIp3nP	směřovat
opačnými	opačný	k2eAgInPc7d1	opačný
směry	směr	k1gInPc7	směr
–	–	k?	–
zatímco	zatímco	k8xS	zatímco
jedno	jeden	k4xCgNnSc1	jeden
vlákno	vlákno	k1gNnSc1	vlákno
můžeme	moct	k5eAaImIp1nP	moct
jedním	jeden	k4xCgInSc7	jeden
směrem	směr	k1gInSc7	směr
popsat	popsat	k5eAaPmF	popsat
jako	jako	k9	jako
5	[number]	k4	5
<g/>
'	'	kIx"	'
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc1	druhý
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
směru	směr	k1gInSc6	směr
3	[number]	k4	3
<g/>
'	'	kIx"	'
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Čísla	číslo	k1gNnPc4	číslo
3	[number]	k4	3
<g/>
'	'	kIx"	'
a	a	k8xC	a
5	[number]	k4	5
<g/>
'	'	kIx"	'
označují	označovat	k5eAaImIp3nP	označovat
čísla	číslo	k1gNnPc1	číslo
uhlíku	uhlík	k1gInSc2	uhlík
na	na	k7c6	na
deoxyribóze	deoxyribóza	k1gFnSc6	deoxyribóza
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
upínají	upínat	k5eAaImIp3nP	upínat
fosfátové	fosfátový	k2eAgFnPc1d1	fosfátová
skupiny	skupina	k1gFnPc1	skupina
v	v	k7c6	v
cukr-fosfátové	cukrosfátový	k2eAgFnSc6d1	cukr-fosfátový
kostře	kostra	k1gFnSc6	kostra
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
bázemi	báze	k1gFnPc7	báze
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jednoho	jeden	k4xCgInSc2	jeden
"	"	kIx"	"
<g/>
patra	patro	k1gNnSc2	patro
<g/>
"	"	kIx"	"
dvoušroubovice	dvoušroubovice	k1gFnPc1	dvoušroubovice
platí	platit	k5eAaImIp3nP	platit
pravidla	pravidlo	k1gNnPc1	pravidlo
Watson-Crickovské	Watson-Crickovský	k2eAgFnSc2d1	Watson-Crickovský
komplementarity	komplementarita	k1gFnSc2	komplementarita
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
tzv.	tzv.	kA	tzv.
helikálních	helikální	k2eAgFnPc2d1	helikální
forem	forma	k1gFnPc2	forma
(	(	kIx(	(
<g/>
konformací	konformace	k1gFnPc2	konformace
<g/>
)	)	kIx)	)
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
parametrů	parametr	k1gInPc2	parametr
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
Watson-Crickovská	Watson-Crickovský	k2eAgFnSc1d1	Watson-Crickovský
pravotočivá	pravotočivý	k2eAgFnSc1d1	pravotočivá
dvoušroubovice	dvoušroubovice	k1gFnSc1	dvoušroubovice
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
B-DNA	B-DNA	k1gFnSc1	B-DNA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nicméně	nicméně	k8xC	nicméně
zcela	zcela	k6eAd1	zcela
převažující	převažující	k2eAgFnPc1d1	převažující
a	a	k8xC	a
ostatní	ostatní	k2eAgFnPc1d1	ostatní
formy	forma	k1gFnPc1	forma
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
pravotočivá	pravotočivý	k2eAgFnSc1d1	pravotočivá
A-DNA	A-DNA	k1gFnSc1	A-DNA
a	a	k8xC	a
levotočivá	levotočivý	k2eAgFnSc1d1	levotočivá
Z-DNA	Z-DNA	k1gFnSc1	Z-DNA
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
mohou	moct	k5eAaImIp3nP	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
i	i	k9	i
v	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
živé	živý	k2eAgFnSc2d1	živá
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
spíše	spíše	k9	spíše
vzácně	vzácně	k6eAd1	vzácně
a	a	k8xC	a
jen	jen	k9	jen
za	za	k7c2	za
specifických	specifický	k2eAgFnPc2d1	specifická
okolností	okolnost	k1gFnPc2	okolnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obecném	obecný	k2eAgNnSc6d1	obecné
povědomí	povědomí	k1gNnSc6	povědomí
DNA	DNA	kA	DNA
tvoří	tvořit	k5eAaImIp3nS	tvořit
dvoušroubovici	dvoušroubovice	k1gFnSc4	dvoušroubovice
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
jiné	jiný	k2eAgInPc4d1	jiný
způsoby	způsob	k1gInPc4	způsob
uspořádání	uspořádání	k1gNnSc2	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
(	(	kIx(	(
<g/>
in	in	k?	in
vivo	vivo	k6eAd1	vivo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgInPc1d1	jiný
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
laboratorní	laboratorní	k2eAgFnSc4d1	laboratorní
záležitost	záležitost	k1gFnSc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
neobvyklých	obvyklý	k2eNgNnPc2d1	neobvyklé
párovacích	párovací	k2eAgNnPc2d1	párovací
míst	místo	k1gNnPc2	místo
na	na	k7c6	na
molekulách	molekula	k1gFnPc6	molekula
bází	báze	k1gFnPc2	báze
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
případ	případ	k1gInSc1	případ
tzv.	tzv.	kA	tzv.
G-kvartetů	Gvartet	k1gMnPc2	G-kvartet
<g/>
,	,	kIx,	,
čtyřvláknových	čtyřvláknový	k2eAgMnPc2d1	čtyřvláknový
úseků	úsek	k1gInPc2	úsek
DNA	DNA	kA	DNA
v	v	k7c6	v
telomerických	telomerický	k2eAgFnPc6d1	telomerický
oblastech	oblast	k1gFnPc6	oblast
chromozomů	chromozom	k1gInPc2	chromozom
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
do	do	k7c2	do
kruhu	kruh	k1gInSc2	kruh
párují	párovat	k5eAaImIp3nP	párovat
čtyři	čtyři	k4xCgFnPc1	čtyři
guaninové	guaninový	k2eAgFnPc1d1	guaninový
báze	báze	k1gFnPc1	báze
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
trojšroubovice	trojšroubovice	k1gFnSc1	trojšroubovice
DNA	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
možná	možná	k9	možná
dočasně	dočasně	k6eAd1	dočasně
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
tzv.	tzv.	kA	tzv.
crossing-overu	crossingver	k1gInSc6	crossing-over
<g/>
;	;	kIx,	;
laboratorně	laboratorně	k6eAd1	laboratorně
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
trojvláknová	trojvláknový	k2eAgFnSc1d1	trojvláknový
struktura	struktura	k1gFnSc1	struktura
připravena	připravit	k5eAaPmNgFnS	připravit
např.	např.	kA	např.
z	z	k7c2	z
vláken	vlákno	k1gNnPc2	vlákno
poly	pola	k1gFnSc2	pola
<g/>
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
)	)	kIx)	)
a	a	k8xC	a
polydeoxy	polydeox	k1gInPc1	polydeox
<g/>
(	(	kIx(	(
<g/>
U	u	k7c2	u
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
DNA	DNA	kA	DNA
se	se	k3xPyFc4	se
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
větvit	větvit	k5eAaImF	větvit
a	a	k8xC	a
vznikají	vznikat	k5eAaImIp3nP	vznikat
např.	např.	kA	např.
třívláknová	třívláknový	k2eAgNnPc4d1	třívláknový
či	či	k8xC	či
čtyřvláknová	čtyřvláknový	k2eAgNnPc4d1	čtyřvláknový
spojení	spojení	k1gNnPc4	spojení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
dvoušroubovicová	dvoušroubovicový	k2eAgFnSc1d1	dvoušroubovicová
DNA	dna	k1gFnSc1	dna
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
svém	svůj	k3xOyFgInSc6	svůj
konci	konec	k1gInSc6	konec
lokálně	lokálně	k6eAd1	lokálně
denaturuje	denaturovat	k5eAaBmIp3nS	denaturovat
a	a	k8xC	a
na	na	k7c4	na
uvolněné	uvolněný	k2eAgInPc4d1	uvolněný
konce	konec	k1gInPc4	konec
se	se	k3xPyFc4	se
připojí	připojit	k5eAaPmIp3nS	připojit
třetí	třetí	k4xOgInSc1	třetí
řetězec	řetězec	k1gInSc1	řetězec
–	–	k?	–
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
buňky	buňka	k1gFnSc2	buňka
by	by	kYmCp3nS	by
tato	tento	k3xDgFnSc1	tento
struktura	struktura	k1gFnSc1	struktura
mohla	moct	k5eAaImAgFnS	moct
vznikat	vznikat	k5eAaImF	vznikat
při	při	k7c6	při
crossing-overu	crossingver	k1gInSc6	crossing-over
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
replikaci	replikace	k1gFnSc3	replikace
v	v	k7c6	v
jednom	jeden	k4xCgMnSc6	jeden
z	z	k7c2	z
genomů	genom	k1gInPc2	genom
<g/>
.	.	kIx.	.
</s>
<s>
Jindy	jindy	k6eAd1	jindy
takto	takto	k6eAd1	takto
vlastně	vlastně	k9	vlastně
denaturují	denaturovat	k5eAaBmIp3nP	denaturovat
dvě	dva	k4xCgFnPc4	dva
dvoušroubovice	dvoušroubovice	k1gFnPc4	dvoušroubovice
a	a	k8xC	a
vzájemně	vzájemně	k6eAd1	vzájemně
se	se	k3xPyFc4	se
komplementárně	komplementárně	k6eAd1	komplementárně
přiloží	přiložit	k5eAaPmIp3nP	přiložit
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
čtyřvláknové	čtyřvláknový	k2eAgNnSc1d1	čtyřvláknový
spojení	spojení	k1gNnSc1	spojení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
crossing-overu	crossingver	k1gInSc2	crossing-over
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
známý	známý	k2eAgInSc4d1	známý
Hollidayův	Hollidayův	k2eAgInSc4d1	Hollidayův
spoj	spoj	k1gInSc4	spoj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
výměnu	výměna	k1gFnSc4	výměna
homologních	homologní	k2eAgNnPc2d1	homologní
vláken	vlákno	k1gNnPc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
replikaci	replikace	k1gFnSc6	replikace
DNA	DNA	kA	DNA
či	či	k8xC	či
při	při	k7c6	při
opravě	oprava	k1gFnSc6	oprava
DNA	dno	k1gNnSc2	dno
mohou	moct	k5eAaImIp3nP	moct
větvení	větvení	k1gNnPc4	větvení
vznikat	vznikat	k5eAaImF	vznikat
také	také	k6eAd1	také
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
nicméně	nicméně	k8xC	nicméně
vznikají	vznikat	k5eAaImIp3nP	vznikat
ještě	ještě	k6eAd1	ještě
mnohem	mnohem	k6eAd1	mnohem
fantastičtější	fantastický	k2eAgFnPc4d2	fantastičtější
prostorové	prostorový	k2eAgFnPc4d1	prostorová
struktury	struktura	k1gFnPc4	struktura
DNA	DNA	kA	DNA
–	–	k?	–
byly	být	k5eAaImAgInP	být
vyrobeny	vyrobit	k5eAaPmNgInP	vyrobit
např.	např.	kA	např.
krychle	krychle	k1gFnSc1	krychle
či	či	k8xC	či
osmistěn	osmistěn	k1gInSc1	osmistěn
složené	složený	k2eAgFnSc2d1	složená
celé	celá	k1gFnSc2	celá
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
DNA	dno	k1gNnSc2	dno
molekul	molekula	k1gFnPc2	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
syntetické	syntetický	k2eAgFnPc1d1	syntetická
struktury	struktura	k1gFnPc1	struktura
DNA	dno	k1gNnSc2	dno
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
zájmu	zájem	k1gInSc2	zájem
DNA	dna	k1gFnSc1	dna
nanotechnologů	nanotechnolog	k1gMnPc2	nanotechnolog
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
kondenzace	kondenzace	k1gFnSc2	kondenzace
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Genom	genom	k1gInSc1	genom
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
souhrn	souhrn	k1gInSc1	souhrn
DNA	DNA	kA	DNA
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
pouhou	pouhý	k2eAgFnSc7d1	pouhá
změtí	změt	k1gFnSc7	změt
dvoušroubovicové	dvoušroubovicový	k2eAgFnSc6d1	dvoušroubovicová
DNA	DNA	kA	DNA
–	–	k?	–
na	na	k7c6	na
vyšších	vysoký	k2eAgFnPc6d2	vyšší
úrovních	úroveň	k1gFnPc6	úroveň
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
komplikované	komplikovaný	k2eAgNnSc4d1	komplikované
vinutí	vinutí	k1gNnSc4	vinutí
a	a	k8xC	a
četné	četný	k2eAgFnPc4d1	četná
interakce	interakce	k1gFnPc4	interakce
s	s	k7c7	s
buněčnými	buněčný	k2eAgFnPc7d1	buněčná
bílkovinami	bílkovina	k1gFnPc7	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
struktury	struktura	k1gFnPc1	struktura
také	také	k9	také
nesou	nést	k5eAaImIp3nP	nést
genetickou	genetický	k2eAgFnSc4d1	genetická
informaci	informace	k1gFnSc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
typické	typický	k2eAgNnSc1d1	typické
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
nadšroubovicové	nadšroubovicový	k2eAgNnSc1d1	nadšroubovicový
vinutí	vinutí	k1gNnSc1	vinutí
(	(	kIx(	(
<g/>
supercoiling	supercoiling	k1gInSc1	supercoiling
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
dodatečné	dodatečný	k2eAgNnSc1d1	dodatečné
šroubovicové	šroubovicový	k2eAgNnSc1d1	šroubovicové
vinutí	vinutí	k1gNnSc1	vinutí
již	již	k6eAd1	již
existující	existující	k2eAgFnPc4d1	existující
dvoušroubovice	dvoušroubovice	k1gFnPc4	dvoušroubovice
<g/>
.	.	kIx.	.
</s>
<s>
Nadšroubovicové	Nadšroubovicový	k2eAgNnSc1d1	Nadšroubovicový
vinutí	vinutí	k1gNnSc1	vinutí
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
představit	představit	k5eAaPmF	představit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
držíme	držet	k5eAaImIp1nP	držet
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
ruce	ruka	k1gFnSc6	ruka
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
obou	dva	k4xCgInPc2	dva
konců	konec	k1gInPc2	konec
provázku	provázek	k1gInSc2	provázek
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
konci	konec	k1gInSc6	konec
provázek	provázek	k1gInSc4	provázek
kroutíme	kroutit	k5eAaImIp1nP	kroutit
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
napětí	napětí	k1gNnSc1	napětí
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
(	(	kIx(	(
<g/>
relaxuje	relaxovat	k5eAaBmIp3nS	relaxovat
<g/>
)	)	kIx)	)
jen	jen	k9	jen
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
uvolníme	uvolnit	k5eAaPmIp1nP	uvolnit
jednu	jeden	k4xCgFnSc4	jeden
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Dvoušroubovice	Dvoušroubovice	k1gFnSc1	Dvoušroubovice
je	být	k5eAaImIp3nS	být
však	však	k9	však
stočená	stočený	k2eAgFnSc1d1	stočená
již	již	k6eAd1	již
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
relaxovaném	relaxovaný	k2eAgInSc6d1	relaxovaný
stavu	stav	k1gInSc6	stav
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
otáčka	otáčka	k1gFnSc1	otáčka
každých	každý	k3xTgInPc2	každý
cca	cca	kA	cca
10	[number]	k4	10
párů	pár	k1gInPc2	pár
bází	báze	k1gFnPc2	báze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
můžeme	moct	k5eAaImIp1nP	moct
rozlišit	rozlišit	k5eAaPmF	rozlišit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
nadšroubovice	nadšroubovice	k1gFnSc1	nadšroubovice
vine	vinout	k5eAaImIp3nS	vinout
stejným	stejný	k2eAgInSc7d1	stejný
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
dvoušroubovice	dvoušroubovice	k1gFnSc1	dvoušroubovice
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
supercoiling	supercoiling	k1gInSc1	supercoiling
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
směrem	směr	k1gInSc7	směr
opačným	opačný	k2eAgInSc7d1	opačný
(	(	kIx(	(
<g/>
negativní	negativní	k2eAgInSc4d1	negativní
supercoiling	supercoiling	k1gInSc4	supercoiling
<g/>
,	,	kIx,	,
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
DNA	DNA	kA	DNA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nadšroubovicové	Nadšroubovicový	k2eAgNnSc1d1	Nadšroubovicový
vinutí	vinutí	k1gNnSc1	vinutí
má	mít	k5eAaImIp3nS	mít
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
důležitých	důležitý	k2eAgFnPc2d1	důležitá
funkcí	funkce	k1gFnPc2	funkce
a	a	k8xC	a
regulačních	regulační	k2eAgFnPc2d1	regulační
rolí	role	k1gFnPc2	role
<g/>
;	;	kIx,	;
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
anomálii	anomálie	k1gFnSc4	anomálie
ve	v	k7c6	v
struktuře	struktura	k1gFnSc6	struktura
<g/>
.	.	kIx.	.
</s>
<s>
DNA	DNA	kA	DNA
se	se	k3xPyFc4	se
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
dále	daleko	k6eAd2	daleko
organizuje	organizovat	k5eAaBmIp3nS	organizovat
do	do	k7c2	do
mikroskopicky	mikroskopicky	k6eAd1	mikroskopicky
pozorovatelných	pozorovatelný	k2eAgInPc2d1	pozorovatelný
útvarů	útvar	k1gInPc2	útvar
známých	známá	k1gFnPc2	známá
jako	jako	k8xS	jako
chromozomy	chromozom	k1gInPc4	chromozom
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
bakterií	bakterie	k1gFnPc2	bakterie
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
systém	systém	k1gInSc1	systém
kondenzace	kondenzace	k1gFnSc2	kondenzace
DNA	DNA	kA	DNA
do	do	k7c2	do
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
jediného	jediný	k2eAgInSc2d1	jediný
<g/>
)	)	kIx)	)
chromozomu	chromozom	k1gInSc2	chromozom
poněkud	poněkud	k6eAd1	poněkud
méně	málo	k6eAd2	málo
propracovaný	propracovaný	k2eAgInSc1d1	propracovaný
a	a	k8xC	a
např.	např.	kA	např.
u	u	k7c2	u
Escherichia	Escherichium	k1gNnSc2	Escherichium
coli	col	k1gFnSc2	col
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
několik	několik	k4yIc1	několik
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
udržovat	udržovat	k5eAaImF	udržovat
nadšroubovicové	nadšroubovicový	k2eAgNnSc4d1	nadšroubovicový
vinutí	vinutí	k1gNnSc4	vinutí
a	a	k8xC	a
vytvářet	vytvářet	k5eAaImF	vytvářet
ostré	ostrý	k2eAgInPc4d1	ostrý
ohyby	ohyb	k1gInPc4	ohyb
vlákna	vlákno	k1gNnSc2	vlákno
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Eukaryotické	Eukaryotický	k2eAgInPc1d1	Eukaryotický
organismy	organismus	k1gInPc1	organismus
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
komplikovaně	komplikovaně	k6eAd1	komplikovaně
sbalenou	sbalený	k2eAgFnSc4d1	sbalená
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Souvisí	souviset	k5eAaImIp3nS	souviset
to	ten	k3xDgNnSc1	ten
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
jejich	jejich	k3xOp3gNnSc2	jejich
DNA	dno	k1gNnSc2	dno
–	–	k?	–
např.	např.	kA	např.
lidský	lidský	k2eAgInSc1d1	lidský
genom	genom	k1gInSc1	genom
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
dva	dva	k4xCgInPc4	dva
metry	metr	k1gInPc4	metr
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
buněčné	buněčný	k2eAgNnSc1d1	buněčné
jádro	jádro	k1gNnSc1	jádro
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
délku	délka	k1gFnSc4	délka
několik	několik	k4yIc4	několik
mikrometrů	mikrometr	k1gInPc2	mikrometr
<g/>
.	.	kIx.	.
</s>
<s>
Dvouvlákno	Dvouvlákno	k1gNnSc1	Dvouvlákno
DNA	dno	k1gNnSc2	dno
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
nabaluje	nabalovat	k5eAaImIp3nS	nabalovat
na	na	k7c4	na
bazické	bazický	k2eAgInPc4d1	bazický
proteiny	protein	k1gInPc4	protein
známé	známý	k2eAgInPc4d1	známý
jako	jako	k8xS	jako
histony	histon	k1gInPc1	histon
<g/>
;	;	kIx,	;
DNA	DNA	kA	DNA
nabalená	nabalený	k2eAgFnSc1d1	nabalená
na	na	k7c4	na
osm	osm	k4xCc4	osm
histonů	histon	k1gInPc2	histon
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
nukleozom	nukleozom	k1gInSc1	nukleozom
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
úrovni	úroveň	k1gFnSc6	úroveň
DNA	dno	k1gNnSc2	dno
vypadá	vypadat	k5eAaImIp3nS	vypadat
jako	jako	k9	jako
řada	řada	k1gFnSc1	řada
korálků	korálek	k1gInPc2	korálek
(	(	kIx(	(
<g/>
nukleozomů	nukleozom	k1gInPc2	nukleozom
<g/>
)	)	kIx)	)
na	na	k7c6	na
provázku	provázek	k1gInSc6	provázek
(	(	kIx(	(
<g/>
DNA	DNA	kA	DNA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
korálky	korálek	k1gInPc1	korálek
se	se	k3xPyFc4	se
však	však	k9	však
obvykle	obvykle	k6eAd1	obvykle
ještě	ještě	k6eAd1	ještě
stáčí	stáčet	k5eAaImIp3nS	stáčet
do	do	k7c2	do
30	[number]	k4	30
nanometrů	nanometr	k1gInPc2	nanometr
tlusté	tlustý	k2eAgFnSc2d1	tlustá
šroubovice	šroubovice	k1gFnSc2	šroubovice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
chromozomů	chromozom	k1gInPc2	chromozom
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
ještě	ještě	k6eAd1	ještě
vyšší	vysoký	k2eAgFnPc4d2	vyšší
úrovně	úroveň	k1gFnPc4	úroveň
sbalení	sbalení	k1gNnSc2	sbalení
DNA	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
méně	málo	k6eAd2	málo
prostudované	prostudovaný	k2eAgFnPc1d1	prostudovaná
a	a	k8xC	a
vznikají	vznikat	k5eAaImIp3nP	vznikat
jen	jen	k9	jen
v	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
fázích	fáze	k1gFnPc6	fáze
buněčného	buněčný	k2eAgInSc2d1	buněčný
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
</s>
<s>
DNA	DNA	kA	DNA
je	být	k5eAaImIp3nS	být
polymerní	polymerní	k2eAgFnSc7d1	polymerní
sloučeninou	sloučenina	k1gFnSc7	sloučenina
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
molární	molární	k2eAgFnSc7d1	molární
hmotností	hmotnost	k1gFnSc7	hmotnost
<g/>
.	.	kIx.	.
</s>
<s>
Molární	molární	k2eAgFnSc1d1	molární
hmotnost	hmotnost	k1gFnSc1	hmotnost
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
délce	délka	k1gFnSc6	délka
DNA	dno	k1gNnSc2	dno
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
každým	každý	k3xTgInSc7	každý
nukleotidem	nukleotid	k1gInSc7	nukleotid
stoupá	stoupat	k5eAaImIp3nS	stoupat
molární	molární	k2eAgFnSc4d1	molární
hmotnost	hmotnost	k1gFnSc4	hmotnost
o	o	k7c4	o
330	[number]	k4	330
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
mol	mol	k1gMnSc1	mol
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
dvouvláknové	dvouvláknový	k2eAgFnSc2d1	dvouvláknová
DNA	dno	k1gNnPc4	dno
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
pár	pár	k1gInSc4	pár
bází	báze	k1gFnSc7	báze
připadá	připadat	k5eAaPmIp3nS	připadat
asi	asi	k9	asi
650	[number]	k4	650
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
mol	mol	k1gMnSc1	mol
<g/>
.	.	kIx.	.
</s>
<s>
Deoxyribonukleová	deoxyribonukleový	k2eAgFnSc1d1	deoxyribonukleová
kyselina	kyselina	k1gFnSc1	kyselina
je	být	k5eAaImIp3nS	být
záporně	záporně	k6eAd1	záporně
nabitá	nabitý	k2eAgFnSc1d1	nabitá
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
fosforečnanovým	fosforečnanův	k2eAgFnPc3d1	fosforečnanův
skupinám	skupina	k1gFnPc3	skupina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
polárního	polární	k2eAgInSc2d1	polární
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
ethanolu	ethanol	k1gInSc6	ethanol
se	se	k3xPyFc4	se
sráží	srážet	k5eAaImIp3nS	srážet
(	(	kIx(	(
<g/>
neboť	neboť	k8xC	neboť
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
vyvázání	vyvázání	k1gNnSc3	vyvázání
záporných	záporný	k2eAgInPc2d1	záporný
nábojů	náboj	k1gInPc2	náboj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vysrážení	vysrážení	k1gNnSc6	vysrážení
má	mít	k5eAaImIp3nS	mít
DNA	DNA	kA	DNA
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Izolovaná	izolovaný	k2eAgFnSc1d1	izolovaná
DNA	dna	k1gFnSc1	dna
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
dvoušroubovicové	dvoušroubovicový	k2eAgNnSc4d1	dvoušroubovicový
uspořádání	uspořádání	k1gNnSc4	uspořádání
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
však	však	k9	však
možné	možný	k2eAgNnSc1d1	možné
rozrušit	rozrušit	k5eAaPmF	rozrušit
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
denaturace	denaturace	k1gFnSc2	denaturace
<g/>
.	.	kIx.	.
</s>
<s>
Typicky	typicky	k6eAd1	typicky
se	se	k3xPyFc4	se
denaturace	denaturace	k1gFnSc1	denaturace
provádí	provádět	k5eAaImIp3nS	provádět
zvýšením	zvýšení	k1gNnSc7	zvýšení
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
denaturaci	denaturace	k1gFnSc4	denaturace
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
i	i	k9	i
nízká	nízký	k2eAgFnSc1d1	nízká
iontová	iontový	k2eAgFnSc1d1	iontová
síla	síla	k1gFnSc1	síla
roztoku	roztok	k1gInSc2	roztok
nebo	nebo	k8xC	nebo
silně	silně	k6eAd1	silně
zásadité	zásaditý	k2eAgNnSc4d1	zásadité
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
kyselé	kyselý	k2eAgNnSc1d1	kyselé
prostředí	prostředí	k1gNnSc1	prostředí
není	být	k5eNaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
hydrolýze	hydrolýza	k1gFnSc3	hydrolýza
glykosidových	glykosidový	k2eAgFnPc2d1	glykosidový
vazeb	vazba	k1gFnPc2	vazba
mezi	mezi	k7c7	mezi
cukrem	cukr	k1gInSc7	cukr
a	a	k8xC	a
bází	báze	k1gFnSc7	báze
<g/>
.	.	kIx.	.
</s>
<s>
DNA	DNA	kA	DNA
absorbuje	absorbovat	k5eAaBmIp3nS	absorbovat
v	v	k7c6	v
UV	UV	kA	UV
oblasti	oblast	k1gFnPc4	oblast
s	s	k7c7	s
absorpčním	absorpční	k2eAgNnSc7d1	absorpční
maximem	maximum	k1gNnSc7	maximum
při	při	k7c6	při
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
260	[number]	k4	260
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
denaturaci	denaturace	k1gFnSc6	denaturace
DNA	dno	k1gNnSc2	dno
se	se	k3xPyFc4	se
absorbance	absorbance	k1gFnSc1	absorbance
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
–	–	k?	–
tomuto	tento	k3xDgInSc3	tento
jevu	jev	k1gInSc3	jev
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
hyperchromní	hyperchromní	k2eAgInSc1d1	hyperchromní
efekt	efekt	k1gInSc1	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dáno	dát	k5eAaPmNgNnS	dát
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
absorpci	absorpce	k1gFnSc6	absorpce
se	se	k3xPyFc4	se
v	v	k7c6	v
největší	veliký	k2eAgFnSc6d3	veliký
míře	míra	k1gFnSc6	míra
podílejí	podílet	k5eAaImIp3nP	podílet
báze	báze	k1gFnPc1	báze
DNA	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
dsDNA	dsDNA	k?	dsDNA
"	"	kIx"	"
<g/>
schované	schovaná	k1gFnSc2	schovaná
<g/>
"	"	kIx"	"
uvnitř	uvnitř	k7c2	uvnitř
dvoušroubovice	dvoušroubovice	k1gFnSc2	dvoušroubovice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
denaturaci	denaturace	k1gFnSc6	denaturace
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
"	"	kIx"	"
<g/>
obnažení	obnažení	k1gNnSc3	obnažení
<g/>
"	"	kIx"	"
bází	báze	k1gFnPc2	báze
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tak	tak	k9	tak
mohou	moct	k5eAaImIp3nP	moct
lépe	dobře	k6eAd2	dobře
absorbovat	absorbovat	k5eAaBmF	absorbovat
UV	UV	kA	UV
zážení	záženit	k5eAaPmIp3nS	záženit
<g/>
.	.	kIx.	.
</s>
<s>
Poločas	poločas	k1gInSc1	poločas
rozpadu	rozpad	k1gInSc2	rozpad
DNA	DNA	kA	DNA
činí	činit	k5eAaImIp3nS	činit
dle	dle	k7c2	dle
studia	studio	k1gNnSc2	studio
kosterních	kosterní	k2eAgInPc2d1	kosterní
nálezů	nález	k1gInPc2	nález
asi	asi	k9	asi
521	[number]	k4	521
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
DNA	DNA	kA	DNA
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
stabilní	stabilní	k2eAgFnSc4d1	stabilní
molekulu	molekula	k1gFnSc4	molekula
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vynikne	vyniknout	k5eAaPmIp3nS	vyniknout
zejména	zejména	k9	zejména
při	při	k7c6	při
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
RNA	RNA	kA	RNA
jakožto	jakožto	k8xS	jakožto
druhou	druhý	k4xOgFnSc7	druhý
významnou	významný	k2eAgFnSc7d1	významná
nukleovou	nukleový	k2eAgFnSc7d1	nukleová
kyselinou	kyselina	k1gFnSc7	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
molekule	molekula	k1gFnSc6	molekula
DNA	dno	k1gNnSc2	dno
není	být	k5eNaImIp3nS	být
na	na	k7c4	na
2	[number]	k4	2
<g/>
'	'	kIx"	'
uhlíku	uhlík	k1gInSc2	uhlík
OH	OH	kA	OH
skupina	skupina	k1gFnSc1	skupina
–	–	k?	–
u	u	k7c2	u
RNA	RNA	kA	RNA
tam	tam	k6eAd1	tam
tato	tento	k3xDgFnSc1	tento
reaktivní	reaktivní	k2eAgFnSc1d1	reaktivní
skupina	skupina	k1gFnSc1	skupina
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
nižší	nízký	k2eAgFnSc4d2	nižší
stabilitu	stabilita	k1gFnSc4	stabilita
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
DNA	DNA	kA	DNA
se	se	k3xPyFc4	se
v	v	k7c6	v
laboratoři	laboratoř	k1gFnSc6	laboratoř
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
skladuje	skladovat	k5eAaImIp3nS	skladovat
při	při	k7c6	při
−	−	k?	−
<g/>
°	°	k?	°
nebo	nebo	k8xC	nebo
−	−	k?	−
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vydrží	vydržet	k5eAaPmIp3nS	vydržet
i	i	k9	i
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
4	[number]	k4	4
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
TE	TE	kA	TE
pufru	pufr	k1gInSc2	pufr
vydrží	vydržet	k5eAaPmIp3nS	vydržet
několik	několik	k4yIc1	několik
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
mnoho	mnoho	k4c1	mnoho
různých	různý	k2eAgFnPc2d1	různá
metod	metoda	k1gFnPc2	metoda
k	k	k7c3	k
uchování	uchování	k1gNnSc3	uchování
DNA	dno	k1gNnSc2	dno
na	na	k7c4	na
delší	dlouhý	k2eAgInSc4d2	delší
čas	čas	k1gInSc4	čas
(	(	kIx(	(
<g/>
zmrazení	zmrazení	k1gNnSc3	zmrazení
vzorků	vzorek	k1gInPc2	vzorek
tekutým	tekutý	k2eAgInSc7d1	tekutý
dusíkem	dusík	k1gInSc7	dusík
<g/>
,	,	kIx,	,
FTA	FTA	kA	FTA
karty	karta	k1gFnSc2	karta
<g/>
,	,	kIx,	,
plastové	plastový	k2eAgFnSc2d1	plastová
mikrozkumavky	mikrozkumavka	k1gFnSc2	mikrozkumavka
<g/>
,	,	kIx,	,
uchování	uchování	k1gNnSc2	uchování
pomocí	pomocí	k7c2	pomocí
chitosanu	chitosan	k1gInSc2	chitosan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
těl	tělo	k1gNnPc2	tělo
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
však	však	k9	však
DNA	dna	k1gFnSc1	dna
musí	muset	k5eAaImIp3nS	muset
snášet	snášet	k5eAaImF	snášet
i	i	k9	i
poměrně	poměrně	k6eAd1	poměrně
vysoké	vysoký	k2eAgFnPc4d1	vysoká
teploty	teplota	k1gFnPc4	teplota
<g/>
,	,	kIx,	,
a	a	k8xC	a
přesto	přesto	k8xC	přesto
vydrží	vydržet	k5eAaPmIp3nS	vydržet
<g/>
.	.	kIx.	.
</s>
<s>
Krajním	krajní	k2eAgInSc7d1	krajní
případem	případ	k1gInSc7	případ
jsou	být	k5eAaImIp3nP	být
hypertermofilní	hypertermofilní	k2eAgInPc1d1	hypertermofilní
organismy	organismus	k1gInPc1	organismus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
žijí	žít	k5eAaImIp3nP	žít
i	i	k9	i
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
kolem	kolem	k7c2	kolem
100	[number]	k4	100
°	°	k?	°
<g/>
C.	C.	kA	C.
Jejich	jejich	k3xOp3gNnPc4	jejich
DNA	dno	k1gNnPc4	dno
čelí	čelit	k5eAaImIp3nS	čelit
jak	jak	k6eAd1	jak
riziku	riziko	k1gNnSc3	riziko
denaturace	denaturace	k1gFnSc2	denaturace
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k8xC	i
termodegradaci	termodegradace	k1gFnSc3	termodegradace
(	(	kIx(	(
<g/>
rozpadu	rozpad	k1gInSc3	rozpad
pevných	pevný	k2eAgFnPc2d1	pevná
chemických	chemický	k2eAgFnPc2d1	chemická
vazeb	vazba	k1gFnPc2	vazba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
žijí	žít	k5eAaImIp3nP	žít
a	a	k8xC	a
mimo	mimo	k6eAd1	mimo
opravných	opravný	k2eAgInPc2d1	opravný
mechanismů	mechanismus	k1gInPc2	mechanismus
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
zřejmě	zřejmě	k6eAd1	zřejmě
přispívá	přispívat	k5eAaImIp3nS	přispívat
i	i	k9	i
nadšroubovicové	nadšroubovicový	k2eAgNnSc1d1	nadšroubovicový
vinutí	vinutí	k1gNnSc1	vinutí
a	a	k8xC	a
také	také	k9	také
optimální	optimální	k2eAgNnSc4d1	optimální
iontové	iontový	k2eAgNnSc4d1	iontové
složení	složení	k1gNnSc4	složení
cytoplazmy	cytoplazma	k1gFnSc2	cytoplazma
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
DNA	DNA	kA	DNA
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
dále	daleko	k6eAd2	daleko
typické	typický	k2eAgFnPc1d1	typická
i	i	k9	i
některé	některý	k3yIgFnPc1	některý
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
od	od	k7c2	od
běžných	běžný	k2eAgFnPc2d1	běžná
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
možné	možný	k2eAgNnSc1d1	možné
replikovat	replikovat	k5eAaImF	replikovat
DNA	dno	k1gNnPc4	dno
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vytvářet	vytvářet	k5eAaImF	vytvářet
její	její	k3xOp3gFnPc4	její
kopie	kopie	k1gFnPc4	kopie
<g/>
.	.	kIx.	.
</s>
<s>
Víceméně	víceméně	k9	víceméně
každé	každý	k3xTgNnSc1	každý
buněčné	buněčný	k2eAgNnSc1d1	buněčné
dělení	dělení	k1gNnSc1	dělení
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
zmnožení	zmnožení	k1gNnSc4	zmnožení
genetické	genetický	k2eAgFnSc2d1	genetická
informace	informace	k1gFnSc2	informace
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jí	on	k3xPp3gFnSc3	on
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
buňce	buňka	k1gFnSc6	buňka
bylo	být	k5eAaImAgNnS	být
stále	stále	k6eAd1	stále
konstantní	konstantní	k2eAgNnSc1d1	konstantní
množství	množství	k1gNnSc1	množství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
procesu	proces	k1gInSc2	proces
se	se	k3xPyFc4	se
oddělí	oddělit	k5eAaPmIp3nP	oddělit
řetězce	řetězec	k1gInPc1	řetězec
mateřské	mateřský	k2eAgInPc1d1	mateřský
DNA	DNA	kA	DNA
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
slouží	sloužit	k5eAaImIp3nP	sloužit
jako	jako	k8xS	jako
návod	návod	k1gInSc1	návod
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
templát	templát	k1gInSc1	templát
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
druhých	druhý	k4xOgFnPc2	druhý
vláken	vlákna	k1gFnPc2	vlákna
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
obou	dva	k4xCgFnPc2	dva
nově	nově	k6eAd1	nově
vznikajících	vznikající	k2eAgFnPc2d1	vznikající
dvoušroubovic	dvoušroubovice	k1gFnPc2	dvoušroubovice
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
následně	následně	k6eAd1	následně
napůl	napůl	k6eAd1	napůl
tvořeny	tvořit	k5eAaImNgFnP	tvořit
původní	původní	k2eAgFnPc1d1	původní
DNA	dna	k1gFnSc1	dna
a	a	k8xC	a
napůl	napůl	k6eAd1	napůl
nově	nově	k6eAd1	nově
dosyntetizované	dosyntetizovaný	k2eAgNnSc4d1	dosyntetizovaný
–	–	k?	–
celý	celý	k2eAgInSc1d1	celý
proces	proces	k1gInSc1	proces
je	být	k5eAaImIp3nS	být
semikonzervativní	semikonzervativní	k2eAgInSc1d1	semikonzervativní
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
zajímavým	zajímavý	k2eAgFnPc3d1	zajímavá
vlastnostem	vlastnost	k1gFnPc3	vlastnost
DNA	dno	k1gNnSc2	dno
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
patří	patřit	k5eAaImIp3nS	patřit
možnost	možnost	k1gFnSc1	možnost
opravovat	opravovat	k5eAaImF	opravovat
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ještě	ještě	k6eAd1	ještě
dále	daleko	k6eAd2	daleko
vylepšuje	vylepšovat	k5eAaImIp3nS	vylepšovat
(	(	kIx(	(
<g/>
už	už	k9	už
tak	tak	k9	tak
poměrně	poměrně	k6eAd1	poměrně
precizní	precizní	k2eAgMnSc1d1	precizní
<g/>
)	)	kIx)	)
přenos	přenos	k1gInSc1	přenos
genetické	genetický	k2eAgFnSc2d1	genetická
informace	informace	k1gFnSc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nS	by
možno	možno	k6eAd1	možno
najít	najít	k5eAaPmF	najít
množství	množství	k1gNnSc4	množství
dalších	další	k2eAgFnPc2d1	další
pozoruhodných	pozoruhodný	k2eAgFnPc2d1	pozoruhodná
vlastností	vlastnost	k1gFnPc2	vlastnost
DNA	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
vesměs	vesměs	k6eAd1	vesměs
probíhajících	probíhající	k2eAgFnPc2d1	probíhající
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
speciálních	speciální	k2eAgInPc2d1	speciální
enzymů	enzym	k1gInPc2	enzym
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
genom	genom	k1gInSc1	genom
<g/>
,	,	kIx,	,
sekvence	sekvence	k1gFnPc1	sekvence
DNA	DNA	kA	DNA
a	a	k8xC	a
genetický	genetický	k2eAgInSc4d1	genetický
kód	kód	k1gInSc4	kód
<g/>
.	.	kIx.	.
</s>
<s>
DNA	DNA	kA	DNA
je	být	k5eAaImIp3nS	být
nositelkou	nositelka	k1gFnSc7	nositelka
genetické	genetický	k2eAgFnSc2d1	genetická
informace	informace	k1gFnSc2	informace
všech	všecek	k3xTgInPc2	všecek
živých	živý	k2eAgInPc2d1	živý
organismů	organismus	k1gInPc2	organismus
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
mnoha	mnoho	k4c2	mnoho
virů	vir	k1gInPc2	vir
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
DNA	DNA	kA	DNA
je	být	k5eAaImIp3nS	být
zapsána	zapsán	k2eAgFnSc1d1	zapsána
sekvence	sekvence	k1gFnSc1	sekvence
všech	všecek	k3xTgFnPc2	všecek
bílkovin	bílkovina	k1gFnPc2	bílkovina
a	a	k8xC	a
přeneseně	přeneseně	k6eAd1	přeneseně
je	být	k5eAaImIp3nS	být
genetickou	genetický	k2eAgFnSc7d1	genetická
informací	informace	k1gFnSc7	informace
podmíněna	podmíněn	k2eAgFnSc1d1	podmíněna
existence	existence	k1gFnSc1	existence
všech	všecek	k3xTgFnPc2	všecek
biomolekul	biomolekula	k1gFnPc2	biomolekula
a	a	k8xC	a
buněčných	buněčný	k2eAgFnPc2d1	buněčná
struktur	struktura	k1gFnPc2	struktura
(	(	kIx(	(
<g/>
k	k	k7c3	k
jejichž	jejichž	k3xOyRp3gFnSc3	jejichž
tvorbě	tvorba	k1gFnSc3	tvorba
jsou	být	k5eAaImIp3nP	být
potřeba	potřeba	k6eAd1	potřeba
bílkoviny	bílkovina	k1gFnPc4	bílkovina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Schopnost	schopnost	k1gFnSc1	schopnost
ukládat	ukládat	k5eAaImF	ukládat
a	a	k8xC	a
přenášet	přenášet	k5eAaImF	přenášet
genetickou	genetický	k2eAgFnSc4d1	genetická
informaci	informace	k1gFnSc4	informace
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
fundamentálních	fundamentální	k2eAgFnPc2d1	fundamentální
vlastností	vlastnost	k1gFnPc2	vlastnost
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
DNA	dno	k1gNnSc2	dno
buňky	buňka	k1gFnSc2	buňka
vydrží	vydržet	k5eAaPmIp3nS	vydržet
žít	žít	k5eAaImF	žít
jen	jen	k9	jen
omezenou	omezený	k2eAgFnSc4d1	omezená
dobu	doba	k1gFnSc4	doba
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
lidské	lidský	k2eAgFnPc4d1	lidská
červené	červený	k2eAgFnPc4d1	červená
krvinky	krvinka	k1gFnPc4	krvinka
při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
zrání	zrání	k1gNnSc6	zrání
vyvrhují	vyvrhovat	k5eAaImIp3nP	vyvrhovat
jádro	jádro	k1gNnSc4	jádro
<g/>
,	,	kIx,	,
a	a	k8xC	a
protože	protože	k8xS	protože
pak	pak	k6eAd1	pak
nejsou	být	k5eNaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
vyrábět	vyrábět	k5eAaImF	vyrábět
nové	nový	k2eAgFnPc4d1	nová
bílkoviny	bílkovina	k1gFnPc4	bílkovina
a	a	k8xC	a
udržovat	udržovat	k5eAaImF	udržovat
buňku	buňka	k1gFnSc4	buňka
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
několika	několik	k4yIc6	několik
měsících	měsíc	k1gInPc6	měsíc
poškozeny	poškozen	k2eAgFnPc1d1	poškozena
a	a	k8xC	a
musí	muset	k5eAaImIp3nP	muset
se	se	k3xPyFc4	se
z	z	k7c2	z
oběhu	oběh	k1gInSc2	oběh
odstraňovat	odstraňovat	k5eAaImF	odstraňovat
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
viry	vir	k1gInPc1	vir
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
schopné	schopný	k2eAgNnSc1d1	schopné
uchovávat	uchovávat	k5eAaImF	uchovávat
svůj	svůj	k3xOyFgInSc4	svůj
genetický	genetický	k2eAgInSc4d1	genetický
materiál	materiál	k1gInSc4	materiál
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
RNA	RNA	kA	RNA
viry	vir	k1gInPc1	vir
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenže	jenže	k8xC	jenže
RNA	RNA	kA	RNA
genomy	genom	k1gInPc1	genom
nepodléhají	podléhat	k5eNaImIp3nP	podléhat
opravným	opravný	k2eAgInPc3d1	opravný
mechanismům	mechanismus	k1gInPc3	mechanismus
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
mutují	mutovat	k5eAaImIp3nP	mutovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
mají	mít	k5eAaImIp3nP	mít
limitovanou	limitovaný	k2eAgFnSc4d1	limitovaná
velikost	velikost	k1gFnSc4	velikost
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jak	jak	k8xC	jak
ho	on	k3xPp3gMnSc4	on
známe	znát	k5eAaImIp1nP	znát
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
závislý	závislý	k2eAgMnSc1d1	závislý
na	na	k7c4	na
DNA	dno	k1gNnPc4	dno
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétní	konkrétní	k2eAgNnSc1d1	konkrétní
uložení	uložení	k1gNnSc1	uložení
DNA	DNA	kA	DNA
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
příslušnosti	příslušnost	k1gFnSc6	příslušnost
organismu	organismus	k1gInSc2	organismus
k	k	k7c3	k
jedné	jeden	k4xCgFnSc3	jeden
z	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
základních	základní	k2eAgFnPc2d1	základní
skupin	skupina	k1gFnPc2	skupina
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
a	a	k8xC	a
archea	archea	k1gFnSc1	archea
(	(	kIx(	(
<g/>
souhrnně	souhrnně	k6eAd1	souhrnně
"	"	kIx"	"
<g/>
prokaryota	prokaryota	k1gFnSc1	prokaryota
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
DNA	DNA	kA	DNA
obvykle	obvykle	k6eAd1	obvykle
uloženu	uložen	k2eAgFnSc4d1	uložena
volně	volně	k6eAd1	volně
v	v	k7c6	v
cytoplazmě	cytoplazma	k1gFnSc6	cytoplazma
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
vzniká	vznikat	k5eAaImIp3nS	vznikat
pouze	pouze	k6eAd1	pouze
jistá	jistý	k2eAgFnSc1d1	jistá
jaderná	jaderný	k2eAgFnSc1d1	jaderná
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
nukleoid	nukleoid	k1gInSc1	nukleoid
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
to	ten	k3xDgNnSc1	ten
řada	řada	k1gFnSc1	řada
bakterií	bakterie	k1gFnPc2	bakterie
vlastní	vlastní	k2eAgFnSc2d1	vlastní
i	i	k8xC	i
malé	malý	k2eAgFnSc2d1	malá
kruhové	kruhový	k2eAgFnSc2d1	kruhová
molekuly	molekula	k1gFnSc2	molekula
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
plazmidy	plazmida	k1gFnPc1	plazmida
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
horizontální	horizontální	k2eAgFnSc4d1	horizontální
výměnu	výměna	k1gFnSc4	výměna
genetické	genetický	k2eAgFnPc4d1	genetická
informace	informace	k1gFnPc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgInPc1d1	zbylý
organismy	organismus	k1gInPc1	organismus
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
např.	např.	kA	např.
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
,	,	kIx,	,
živočichové	živočich	k1gMnPc1	živočich
či	či	k8xC	či
prvoci	prvok	k1gMnPc1	prvok
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
DNA	dna	k1gFnSc1	dna
uloženu	uložen	k2eAgFnSc4d1	uložena
především	především	k9	především
v	v	k7c6	v
buněčné	buněčný	k2eAgFnSc6d1	buněčná
jádře	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
však	však	k9	však
se	se	k3xPyFc4	se
DNA	dna	k1gFnSc1	dna
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
eukaryotických	eukaryotický	k2eAgFnPc6d1	eukaryotická
organelách	organela	k1gFnPc6	organela
<g/>
,	,	kIx,	,
jmenovitě	jmenovitě	k6eAd1	jmenovitě
v	v	k7c6	v
mitochondriích	mitochondrie	k1gFnPc6	mitochondrie
a	a	k8xC	a
v	v	k7c6	v
plastidech	plastid	k1gInPc6	plastid
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
buňka	buňka	k1gFnSc1	buňka
vlastní	vlastní	k2eAgFnSc1d1	vlastní
(	(	kIx(	(
<g/>
jev	jev	k1gInSc1	jev
zvaný	zvaný	k2eAgInSc1d1	zvaný
mimojaderná	mimojaderný	k2eAgFnSc1d1	mimojaderná
dědičnost	dědičnost	k1gFnSc1	dědičnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnSc1	informace
nesená	nesený	k2eAgFnSc1d1	nesená
sekvencí	sekvence	k1gFnSc7	sekvence
nukleotidů	nukleotid	k1gInPc2	nukleotid
v	v	k7c6	v
DNA	DNA	kA	DNA
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
genetická	genetický	k2eAgFnSc1d1	genetická
informace	informace	k1gFnSc1	informace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
nukleotidové	nukleotidový	k2eAgFnSc6d1	nukleotidová
pozici	pozice	k1gFnSc6	pozice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
bází	báze	k1gFnPc2	báze
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
G	G	kA	G
či	či	k8xC	či
T	T	kA	T
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sekvence	sekvence	k1gFnSc1	sekvence
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
n	n	k0	n
může	moct	k5eAaImIp3nS	moct
nabývat	nabývat	k5eAaImF	nabývat
4	[number]	k4	4
<g/>
n	n	k0	n
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
DNA	DNA	kA	DNA
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
pouhých	pouhý	k2eAgInPc2d1	pouhý
10	[number]	k4	10
nukleotidů	nukleotid	k1gInPc2	nukleotid
existuje	existovat	k5eAaImIp3nS	existovat
tedy	tedy	k9	tedy
teoreticky	teoreticky	k6eAd1	teoreticky
410	[number]	k4	410
=	=	kIx~	=
1	[number]	k4	1
048	[number]	k4	048
576	[number]	k4	576
kombinací	kombinace	k1gFnPc2	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
Lidský	lidský	k2eAgInSc1d1	lidský
genom	genom	k1gInSc1	genom
(	(	kIx(	(
<g/>
souhrn	souhrn	k1gInSc1	souhrn
lidské	lidský	k2eAgFnSc2d1	lidská
jaderné	jaderný	k2eAgFnSc2d1	jaderná
DNA	DNA	kA	DNA
<g/>
)	)	kIx)	)
přitom	přitom	k6eAd1	přitom
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
3,1	[number]	k4	3,1
miliardy	miliarda	k4xCgFnSc2	miliarda
(	(	kIx(	(
<g/>
párů	pár	k1gInPc2	pár
<g/>
)	)	kIx)	)
bází	báze	k1gFnPc2	báze
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
informační	informační	k2eAgFnSc1d1	informační
hodnota	hodnota	k1gFnSc1	hodnota
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
v	v	k7c6	v
genomu	genom	k1gInSc6	genom
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sídlí	sídlet	k5eAaImIp3nS	sídlet
tzv.	tzv.	kA	tzv.
geny	gen	k1gInPc1	gen
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
zaznamenávají	zaznamenávat	k5eAaImIp3nP	zaznamenávat
informaci	informace	k1gFnSc4	informace
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
RNA	RNA	kA	RNA
a	a	k8xC	a
potažmo	potažmo	k6eAd1	potažmo
i	i	k8xC	i
všech	všecek	k3xTgFnPc2	všecek
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
bílkovin	bílkovina	k1gFnPc2	bílkovina
je	být	k5eAaImIp3nS	být
zašifrována	zašifrovat	k5eAaPmNgFnS	zašifrovat
pomocí	pomocí	k7c2	pomocí
třípísmenného	třípísmenný	k2eAgInSc2d1	třípísmenný
kódu	kód	k1gInSc2	kód
známého	známý	k2eAgInSc2d1	známý
jako	jako	k8xS	jako
genetický	genetický	k2eAgInSc4d1	genetický
kód	kód	k1gInSc4	kód
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgFnSc3	každý
trojici	trojice	k1gFnSc3	trojice
bází	báze	k1gFnPc2	báze
v	v	k7c6	v
DNA	DNA	kA	DNA
totiž	totiž	k9	totiž
u	u	k7c2	u
protein-kódujících	proteinódující	k2eAgInPc2d1	protein-kódující
genů	gen	k1gInPc2	gen
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
určitá	určitý	k2eAgFnSc1d1	určitá
aminokyselina	aminokyselina	k1gFnSc1	aminokyselina
<g/>
.	.	kIx.	.
</s>
<s>
Aminokyseliny	aminokyselina	k1gFnPc1	aminokyselina
jsou	být	k5eAaImIp3nP	být
základní	základní	k2eAgInPc4d1	základní
stavební	stavební	k2eAgInPc4d1	stavební
kameny	kámen	k1gInPc4	kámen
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
genetická	genetický	k2eAgFnSc1d1	genetická
informace	informace	k1gFnSc1	informace
jakýmsi	jakýsi	k3yIgInSc7	jakýsi
návodem	návod	k1gInSc7	návod
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Genetická	genetický	k2eAgFnSc1d1	genetická
informace	informace	k1gFnSc1	informace
je	být	k5eAaImIp3nS	být
uplatňována	uplatňovat	k5eAaImNgFnS	uplatňovat
podle	podle	k7c2	podle
tzv.	tzv.	kA	tzv.
centrálního	centrální	k2eAgNnSc2d1	centrální
dogmatu	dogma	k1gNnSc2	dogma
molekulární	molekulární	k2eAgFnSc2d1	molekulární
biologie	biologie	k1gFnSc2	biologie
<g/>
.	.	kIx.	.
</s>
<s>
DNA	DNA	kA	DNA
je	být	k5eAaImIp3nS	být
nejprve	nejprve	k6eAd1	nejprve
přepisována	přepisován	k2eAgFnSc1d1	přepisována
v	v	k7c6	v
RNA	RNA	kA	RNA
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
tzv.	tzv.	kA	tzv.
messenger	messengra	k1gFnPc2	messengra
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
RNA	RNA	kA	RNA
použita	použít	k5eAaPmNgFnS	použít
jako	jako	k8xS	jako
vzor	vzor	k1gInSc1	vzor
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
zmíněný	zmíněný	k2eAgInSc1d1	zmíněný
krok	krok	k1gInSc1	krok
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
transkripce	transkripce	k1gFnSc1	transkripce
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc1	druhý
translace	translace	k1gFnPc4	translace
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
genomu	genom	k1gInSc2	genom
mnoha	mnoho	k4c2	mnoho
organismů	organismus	k1gInPc2	organismus
však	však	k9	však
není	být	k5eNaImIp3nS	být
součástí	součást	k1gFnSc7	součást
žádného	žádný	k3yNgInSc2	žádný
genu	gen	k1gInSc2	gen
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
nepřepisuje	přepisovat	k5eNaImIp3nS	přepisovat
v	v	k7c6	v
RNA	RNA	kA	RNA
<g/>
.	.	kIx.	.
</s>
<s>
Role	role	k1gFnSc1	role
této	tento	k3xDgFnSc2	tento
tzv.	tzv.	kA	tzv.
nekódující	kódující	k2eNgFnSc1d1	nekódující
DNA	dna	k1gFnSc1	dna
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
neznámá	známý	k2eNgFnSc1d1	neznámá
<g/>
;	;	kIx,	;
někdy	někdy	k6eAd1	někdy
však	však	k9	však
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
regulovat	regulovat	k5eAaImF	regulovat
spouštění	spouštění	k1gNnSc4	spouštění
a	a	k8xC	a
vypínání	vypínání	k1gNnSc4	vypínání
okolních	okolní	k2eAgInPc2d1	okolní
genů	gen	k1gInPc2	gen
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
nekódující	kódující	k2eNgFnSc1d1	nekódující
DNA	dna	k1gFnSc1	dna
dle	dle	k7c2	dle
současné	současný	k2eAgFnSc2d1	současná
úrovně	úroveň	k1gFnSc2	úroveň
znalostí	znalost	k1gFnPc2	znalost
nemá	mít	k5eNaImIp3nS	mít
žádnou	žádný	k3yNgFnSc4	žádný
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
prostě	prostě	k9	prostě
jako	jako	k9	jako
junk	junk	k6eAd1	junk
(	(	kIx(	(
<g/>
odpadní	odpadní	k2eAgNnPc4d1	odpadní
<g/>
)	)	kIx)	)
DNA	dno	k1gNnPc4	dno
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
této	tento	k3xDgFnSc2	tento
odpadní	odpadní	k2eAgFnSc2d1	odpadní
DNA	DNA	kA	DNA
však	však	k9	však
podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
projektu	projekt	k1gInSc2	projekt
ENCODE	ENCODE	kA	ENCODE
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
kóduje	kódovat	k5eAaBmIp3nS	kódovat
různé	různý	k2eAgNnSc1d1	různé
krátké	krátké	k1gNnSc1	krátké
regulační	regulační	k2eAgFnSc2d1	regulační
RNA	RNA	kA	RNA
<g/>
;	;	kIx,	;
celkem	celkem	k6eAd1	celkem
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
%	%	kIx~	%
genomu	genom	k1gInSc2	genom
má	mít	k5eAaImIp3nS	mít
díky	díky	k7c3	díky
těmto	tento	k3xDgInPc3	tento
RNA	RNA	kA	RNA
významnou	významný	k2eAgFnSc4d1	významná
regulační	regulační	k2eAgFnSc4d1	regulační
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těsném	těsný	k2eAgNnSc6d1	těsné
okolí	okolí	k1gNnSc6	okolí
těchto	tento	k3xDgFnPc2	tento
regulačních	regulační	k2eAgFnPc2d1	regulační
sekvencí	sekvence	k1gFnPc2	sekvence
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
podle	podle	k7c2	podle
ENCODE	ENCODE	kA	ENCODE
celkem	celkem	k6eAd1	celkem
nachází	nacházet	k5eAaImIp3nS	nacházet
až	až	k9	až
95	[number]	k4	95
%	%	kIx~	%
lidského	lidský	k2eAgInSc2d1	lidský
genomu	genom	k1gInSc2	genom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
případů	případ	k1gInPc2	případ
je	být	k5eAaImIp3nS	být
žádoucí	žádoucí	k2eAgMnSc1d1	žádoucí
izolovat	izolovat	k5eAaBmF	izolovat
z	z	k7c2	z
buněk	buňka	k1gFnPc2	buňka
či	či	k8xC	či
z	z	k7c2	z
virových	virový	k2eAgFnPc2d1	virová
partikulí	partikule	k1gFnPc2	partikule
jejich	jejich	k3xOp3gNnSc4	jejich
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
samozřejmě	samozřejmě	k6eAd1	samozřejmě
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
metod	metoda	k1gFnPc2	metoda
extrakce	extrakce	k1gFnSc1	extrakce
DNA	dna	k1gFnSc1	dna
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
u	u	k7c2	u
všech	všecek	k3xTgFnPc2	všecek
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
získat	získat	k5eAaPmF	získat
dostatečné	dostatečný	k2eAgNnSc4d1	dostatečné
množství	množství	k1gNnSc4	množství
biologického	biologický	k2eAgInSc2d1	biologický
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
uvolnit	uvolnit	k5eAaPmF	uvolnit
DNA	dno	k1gNnPc4	dno
a	a	k8xC	a
oddělit	oddělit	k5eAaPmF	oddělit
ji	on	k3xPp3gFnSc4	on
z	z	k7c2	z
nadmolekulárních	nadmolekulární	k2eAgFnPc2d1	nadmolekulární
struktur	struktura	k1gFnPc2	struktura
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
vzorek	vzorek	k1gInSc4	vzorek
přečistit	přečistit	k5eAaPmF	přečistit
a	a	k8xC	a
případně	případně	k6eAd1	případně
zahustit	zahustit	k5eAaPmF	zahustit
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
krokem	krok	k1gInSc7	krok
je	být	k5eAaImIp3nS	být
uvolnění	uvolnění	k1gNnSc1	uvolnění
DNA	dno	k1gNnSc2	dno
z	z	k7c2	z
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
u	u	k7c2	u
živočišných	živočišný	k2eAgFnPc2d1	živočišná
buněk	buňka	k1gFnPc2	buňka
provádí	provádět	k5eAaImIp3nS	provádět
pomocí	pomocí	k7c2	pomocí
detergentů	detergent	k1gInPc2	detergent
(	(	kIx(	(
<g/>
povrchově	povrchově	k6eAd1	povrchově
aktivních	aktivní	k2eAgFnPc2d1	aktivní
čisticích	čisticí	k2eAgFnPc2d1	čisticí
látek	látka	k1gFnPc2	látka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
rozrušují	rozrušovat	k5eAaImIp3nP	rozrušovat
membrány	membrána	k1gFnPc4	membrána
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
buněk	buňka	k1gFnPc2	buňka
s	s	k7c7	s
buněčnou	buněčný	k2eAgFnSc7d1	buněčná
stěnou	stěna	k1gFnSc7	stěna
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
komplikovanější	komplikovaný	k2eAgFnSc1d2	komplikovanější
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
nasadit	nasadit	k5eAaPmF	nasadit
třeba	třeba	k6eAd1	třeba
lysozymy	lysozymum	k1gNnPc7	lysozymum
(	(	kIx(	(
<g/>
na	na	k7c4	na
bakteriální	bakteriální	k2eAgFnSc4d1	bakteriální
buněčnou	buněčný	k2eAgFnSc4d1	buněčná
stěnu	stěna	k1gFnSc4	stěna
<g/>
)	)	kIx)	)
či	či	k8xC	či
mechanickou	mechanický	k2eAgFnSc4d1	mechanická
degradaci	degradace	k1gFnSc4	degradace
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
přečišťování	přečišťování	k1gNnSc1	přečišťování
buněčných	buněčný	k2eAgInPc2d1	buněčný
extraktů	extrakt	k1gInPc2	extrakt
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
se	se	k3xPyFc4	se
zbavit	zbavit	k5eAaPmF	zbavit
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
představují	představovat	k5eAaImIp3nP	představovat
hlavní	hlavní	k2eAgFnSc4d1	hlavní
kontaminaci	kontaminace	k1gFnSc4	kontaminace
vzorků	vzorek	k1gInPc2	vzorek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
proteázy	proteáza	k1gFnPc4	proteáza
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mnohdy	mnohdy	k6eAd1	mnohdy
se	se	k3xPyFc4	se
proteiny	protein	k1gInPc1	protein
sráží	srážet	k5eAaImIp3nP	srážet
fenolem	fenol	k1gInSc7	fenol
a	a	k8xC	a
chloroformem	chloroform	k1gInSc7	chloroform
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
nukleové	nukleový	k2eAgFnSc2d1	nukleová
kyseliny	kyselina	k1gFnSc2	kyselina
zůstanou	zůstat	k5eAaPmIp3nP	zůstat
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
je	on	k3xPp3gInPc4	on
pak	pak	k6eAd1	pak
vysrážet	vysrážet	k5eAaPmF	vysrážet
třeba	třeba	k6eAd1	třeba
ethanolem	ethanol	k1gInSc7	ethanol
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
izolaci	izolace	k1gFnSc6	izolace
DNA	dno	k1gNnSc2	dno
následuje	následovat	k5eAaImIp3nS	následovat
často	často	k6eAd1	často
separace	separace	k1gFnSc1	separace
(	(	kIx(	(
<g/>
oddělení	oddělení	k1gNnSc1	oddělení
<g/>
)	)	kIx)	)
požadovaných	požadovaný	k2eAgInPc2d1	požadovaný
druhů	druh	k1gInPc2	druh
molekul	molekula	k1gFnPc2	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
žádoucí	žádoucí	k2eAgNnSc1d1	žádoucí
oddělení	oddělení	k1gNnSc1	oddělení
třeba	třeba	k6eAd1	třeba
plazmidů	plazmid	k1gMnPc2	plazmid
od	od	k7c2	od
genomové	genomový	k2eAgFnSc2d1	genomová
DNA	DNA	kA	DNA
bakterií	bakterie	k1gFnPc2	bakterie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
dělá	dělat	k5eAaImIp3nS	dělat
poměrně	poměrně	k6eAd1	poměrně
jednoduše	jednoduše	k6eAd1	jednoduše
centrifugací	centrifugace	k1gFnPc2	centrifugace
při	při	k7c6	při
vhodně	vhodně	k6eAd1	vhodně
nastavených	nastavený	k2eAgInPc6d1	nastavený
parametrech	parametr	k1gInPc6	parametr
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
pomocí	pomocí	k7c2	pomocí
denaturace	denaturace	k1gFnSc2	denaturace
a	a	k8xC	a
následné	následný	k2eAgFnSc2d1	následná
renaturace	renaturace	k1gFnSc2	renaturace
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jemnější	jemný	k2eAgNnSc4d2	jemnější
rozdělování	rozdělování	k1gNnSc4	rozdělování
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
i	i	k9	i
podle	podle	k7c2	podle
topologie	topologie	k1gFnSc2	topologie
DNA	DNA	kA	DNA
se	se	k3xPyFc4	se
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
elektroforéza	elektroforéza	k1gFnSc1	elektroforéza
na	na	k7c6	na
agarózovém	agarózový	k2eAgMnSc6d1	agarózový
(	(	kIx(	(
<g/>
či	či	k8xC	či
v	v	k7c6	v
případě	případ	k1gInSc6	případ
velmi	velmi	k6eAd1	velmi
malých	malý	k2eAgFnPc2d1	malá
molekul	molekula	k1gFnPc2	molekula
na	na	k7c6	na
polyakrylamidovém	polyakrylamidový	k2eAgInSc6d1	polyakrylamidový
<g/>
)	)	kIx)	)
gelu	gel	k1gInSc6	gel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
extrémně	extrémně	k6eAd1	extrémně
velkých	velký	k2eAgInPc2d1	velký
fragmentů	fragment	k1gInPc2	fragment
DNA	DNA	kA	DNA
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
tzv.	tzv.	kA	tzv.
pulzní	pulzní	k2eAgFnSc1d1	pulzní
gelová	gelový	k2eAgFnSc1d1	gelová
elektroforéza	elektroforéza	k1gFnSc1	elektroforéza
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
gelu	gel	k1gInSc2	gel
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
následně	následně	k6eAd1	následně
DNA	dno	k1gNnPc4	dno
převést	převést	k5eAaPmF	převést
na	na	k7c4	na
nitrocelulózovou	nitrocelulózový	k2eAgFnSc4d1	nitrocelulózová
membránu	membrána	k1gFnSc4	membrána
pomocí	pomocí	k7c2	pomocí
tzv.	tzv.	kA	tzv.
Southernova	Southernov	k1gInSc2	Southernov
přenosu	přenos	k1gInSc2	přenos
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
metodou	metoda	k1gFnSc7	metoda
dělení	dělení	k1gNnSc2	dělení
DNA	dno	k1gNnSc2	dno
je	být	k5eAaImIp3nS	být
centrifugace	centrifugace	k1gFnSc1	centrifugace
v	v	k7c6	v
hustotním	hustotní	k2eAgInSc6d1	hustotní
gradientu	gradient	k1gInSc6	gradient
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
gradientu	gradient	k1gInSc6	gradient
chloridu	chlorid	k1gInSc2	chlorid
cesného	cesný	k2eAgInSc2d1	cesný
–	–	k?	–
tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
zejména	zejména	k9	zejména
fragmenty	fragment	k1gInPc1	fragment
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
zastoupením	zastoupení	k1gNnSc7	zastoupení
bází	báze	k1gFnPc2	báze
(	(	kIx(	(
<g/>
obsahem	obsah	k1gInSc7	obsah
GC	GC	kA	GC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
nespočet	nespočet	k1gInSc1	nespočet
způsobů	způsob	k1gInPc2	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
obarvit	obarvit	k5eAaPmF	obarvit
DNA	dno	k1gNnPc4	dno
–	–	k?	–
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k8xC	jak
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
DNA	dna	k1gFnSc1	dna
izolovanou	izolovaný	k2eAgFnSc4d1	izolovaná
v	v	k7c6	v
laboratorním	laboratorní	k2eAgNnSc6d1	laboratorní
skle	sklo	k1gNnSc6	sklo
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
často	často	k6eAd1	často
v	v	k7c6	v
laboratořích	laboratoř	k1gFnPc6	laboratoř
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgFnPc4d1	nutná
např.	např.	kA	např.
v	v	k7c6	v
elektroforetickém	elektroforetický	k2eAgInSc6d1	elektroforetický
gelu	gel	k1gInSc6	gel
či	či	k8xC	či
přímo	přímo	k6eAd1	přímo
ve	v	k7c6	v
fixované	fixovaný	k2eAgFnSc6d1	fixovaná
buňce	buňka	k1gFnSc6	buňka
zvýraznit	zvýraznit	k5eAaPmF	zvýraznit
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
známým	známý	k2eAgInPc3d1	známý
takovým	takový	k3xDgNnPc3	takový
barvivům	barvivo	k1gNnPc3	barvivo
patří	patřit	k5eAaImIp3nS	patřit
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
logické	logický	k2eAgFnSc2d1	logická
následnosti	následnost	k1gFnSc2	následnost
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
SYBR	SYBR	kA	SYBR
Green	Green	k1gInSc1	Green
<g/>
,	,	kIx,	,
YOYO-	YOYO-	k1gFnSc1	YOYO-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
TOTO-	TOTO-	k1gFnSc1	TOTO-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
TO-PRO	TO-PRO	k1gFnSc1	TO-PRO
<g/>
,	,	kIx,	,
SYTOX	SYTOX	kA	SYTOX
Green	Green	k1gInSc1	Green
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
klasický	klasický	k2eAgInSc4d1	klasický
ethidiumbromid	ethidiumbromid	k1gInSc4	ethidiumbromid
a	a	k8xC	a
propidiumjodid	propidiumjodid	k1gInSc4	propidiumjodid
<g/>
,	,	kIx,	,
akridinová	akridinový	k2eAgFnSc1d1	akridinový
oranž	oranž	k1gFnSc1	oranž
<g/>
,	,	kIx,	,
různá	různý	k2eAgFnSc1d1	různá
Hoechst	Hoechst	k1gFnSc1	Hoechst
barviva	barvivo	k1gNnSc2	barvivo
či	či	k8xC	či
třeba	třeba	k6eAd1	třeba
DAPI	DAPI	kA	DAPI
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
velmi	velmi	k6eAd1	velmi
specifickým	specifický	k2eAgFnPc3d1	specifická
barvícím	barvící	k2eAgFnPc3d1	barvící
metodám	metoda	k1gFnPc3	metoda
patří	patřit	k5eAaImIp3nS	patřit
fluorescenční	fluorescenční	k2eAgFnSc1d1	fluorescenční
in	in	k?	in
situ	siíst	k5eAaPmIp1nS	siíst
hybridizace	hybridizace	k1gFnSc1	hybridizace
(	(	kIx(	(
<g/>
FISH	FISH	kA	FISH
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
navázání	navázání	k1gNnSc4	navázání
fluorescenčních	fluorescenční	k2eAgFnPc2d1	fluorescenční
sond	sonda	k1gFnPc2	sonda
na	na	k7c4	na
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
sekvenci	sekvence	k1gFnSc4	sekvence
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Sekvenování	Sekvenování	k1gNnSc1	Sekvenování
DNA	dno	k1gNnSc2	dno
je	být	k5eAaImIp3nS	být
souhrnný	souhrnný	k2eAgInSc1d1	souhrnný
termín	termín	k1gInSc1	termín
pro	pro	k7c4	pro
biochemické	biochemický	k2eAgFnPc4d1	biochemická
metody	metoda	k1gFnPc4	metoda
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
se	se	k3xPyFc4	se
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
pořadí	pořadí	k1gNnSc1	pořadí
nukleových	nukleový	k2eAgFnPc2d1	nukleová
bází	báze	k1gFnPc2	báze
v	v	k7c6	v
sekvencích	sekvence	k1gFnPc6	sekvence
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
pořadí	pořadí	k1gNnSc1	pořadí
bází	báze	k1gFnPc2	báze
je	být	k5eAaImIp3nS	být
princip	princip	k1gInSc1	princip
zakódování	zakódování	k1gNnSc2	zakódování
genetické	genetický	k2eAgFnPc4d1	genetická
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
zájmu	zájem	k1gInSc2	zájem
biologů	biolog	k1gMnPc2	biolog
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
a	a	k8xC	a
po	po	k7c4	po
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
převažující	převažující	k2eAgFnSc7d1	převažující
metodou	metoda	k1gFnSc7	metoda
bylo	být	k5eAaImAgNnS	být
tzv.	tzv.	kA	tzv.
Sangerovo	Sangerův	k2eAgNnSc1d1	Sangerovo
sekvenování	sekvenování	k1gNnSc1	sekvenování
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
využívá	využívat	k5eAaImIp3nS	využívat
speciálně	speciálně	k6eAd1	speciálně
chemicky	chemicky	k6eAd1	chemicky
upravených	upravený	k2eAgInPc2d1	upravený
nukleotidů	nukleotid	k1gInPc2	nukleotid
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
pomocí	pomocí	k7c2	pomocí
DNA	dno	k1gNnSc2	dno
polymerázy	polymeráza	k1gFnSc2	polymeráza
zařazovány	zařazován	k2eAgFnPc4d1	zařazována
s	s	k7c7	s
určitou	určitý	k2eAgFnSc7d1	určitá
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
do	do	k7c2	do
prodlužující	prodlužující	k2eAgFnSc2d1	prodlužující
se	s	k7c7	s
DNA	DNA	kA	DNA
–	–	k?	–
tím	ten	k3xDgNnSc7	ten
blokují	blokovat	k5eAaImIp3nP	blokovat
další	další	k2eAgFnSc4d1	další
polymeraci	polymerace	k1gFnSc4	polymerace
a	a	k8xC	a
výsledný	výsledný	k2eAgInSc1d1	výsledný
produkt	produkt	k1gInSc1	produkt
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
detekovat	detekovat	k5eAaImF	detekovat
pomocí	pomocí	k7c2	pomocí
elektroforézy	elektroforéza	k1gFnSc2	elektroforéza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
snahou	snaha	k1gFnSc7	snaha
zrychlit	zrychlit	k5eAaPmF	zrychlit
a	a	k8xC	a
zlevnit	zlevnit	k5eAaPmF	zlevnit
sekvenovací	sekvenovací	k2eAgInSc4d1	sekvenovací
proces	proces	k1gInSc4	proces
byla	být	k5eAaImAgFnS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
sekvenačních	sekvenační	k2eAgFnPc2d1	sekvenační
metod	metoda	k1gFnPc2	metoda
nové	nový	k2eAgFnSc2d1	nová
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těm	ten	k3xDgInPc3	ten
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
pyrosekvenování	pyrosekvenování	k1gNnSc1	pyrosekvenování
a	a	k8xC	a
příbuzné	příbuzný	k2eAgFnPc1d1	příbuzná
metody	metoda	k1gFnPc1	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
Zhang	Zhanga	k1gFnPc2	Zhanga
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
uvádí	uvádět	k5eAaImIp3nS	uvádět
pět	pět	k4xCc4	pět
moderních	moderní	k2eAgFnPc2d1	moderní
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
komerčně	komerčně	k6eAd1	komerčně
dostupné	dostupný	k2eAgInPc1d1	dostupný
<g/>
:	:	kIx,	:
Roche	Roch	k1gInPc1	Roch
GS-FLX	GS-FLX	k1gFnSc1	GS-FLX
454	[number]	k4	454
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
454	[number]	k4	454
sekvenování	sekvenování	k1gNnSc2	sekvenování
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Illumina	Illumin	k2eAgFnSc1d1	Illumina
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Solexa	Solexa	k1gFnSc1	Solexa
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ABI	ABI	kA	ABI
SOLiD	solid	k1gInSc1	solid
<g/>
,	,	kIx,	,
Polonator	Polonator	k1gInSc1	Polonator
G	G	kA	G
<g/>
.007	.007	k4	.007
a	a	k8xC	a
Helicos	Helicos	k1gMnSc1	Helicos
HeliScope	HeliScop	k1gMnSc5	HeliScop
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
postupů	postup	k1gInPc2	postup
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
si	se	k3xPyFc3	se
připravit	připravit	k5eAaPmF	připravit
či	či	k8xC	či
namnožit	namnožit	k5eAaPmF	namnožit
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
molekulu	molekula	k1gFnSc4	molekula
DNA	DNA	kA	DNA
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
možností	možnost	k1gFnPc2	možnost
je	být	k5eAaImIp3nS	být
chemická	chemický	k2eAgFnSc1d1	chemická
syntéza	syntéza	k1gFnSc1	syntéza
DNA	dno	k1gNnSc2	dno
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
sestavování	sestavování	k1gNnSc3	sestavování
krátkých	krátký	k2eAgInPc2d1	krátký
oligonukleotidů	oligonukleotid	k1gInPc2	oligonukleotid
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
postupným	postupný	k2eAgNnSc7d1	postupné
řazením	řazení	k1gNnSc7	řazení
nukleotidů	nukleotid	k1gInPc2	nukleotid
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
typickém	typický	k2eAgInSc6d1	typický
případě	případ	k1gInSc6	případ
však	však	k9	však
již	již	k6eAd1	již
je	být	k5eAaImIp3nS	být
určité	určitý	k2eAgNnSc4d1	určité
množství	množství	k1gNnSc4	množství
DNA	DNA	kA	DNA
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
žádoucí	žádoucí	k2eAgNnSc1d1	žádoucí
ho	on	k3xPp3gNnSc4	on
pouze	pouze	k6eAd1	pouze
zmnožit	zmnožit	k5eAaPmF	zmnožit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
všechny	všechen	k3xTgFnPc1	všechen
kopie	kopie	k1gFnPc1	kopie
měly	mít	k5eAaImAgFnP	mít
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
totožnou	totožný	k2eAgFnSc4d1	totožná
sekvenci	sekvence	k1gFnSc4	sekvence
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
často	často	k6eAd1	často
dělá	dělat	k5eAaImIp3nS	dělat
buď	buď	k8xC	buď
pomocí	pomocí	k7c2	pomocí
klonování	klonování	k1gNnSc2	klonování
DNA	DNA	kA	DNA
nebo	nebo	k8xC	nebo
metodou	metoda	k1gFnSc7	metoda
polymerázové	polymerázový	k2eAgFnSc2d1	polymerázová
řetězové	řetězový	k2eAgFnSc2d1	řetězová
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Vědecký	vědecký	k2eAgInSc1d1	vědecký
pokrok	pokrok	k1gInSc1	pokrok
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
genetiky	genetika	k1gFnSc2	genetika
způsobil	způsobit	k5eAaPmAgMnS	způsobit
boom	boom	k1gInSc4	boom
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
lékařské	lékařský	k2eAgFnSc2d1	lékařská
diagnostiky	diagnostika	k1gFnSc2	diagnostika
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
bakteriologii	bakteriologie	k1gFnSc6	bakteriologie
<g/>
,	,	kIx,	,
virologii	virologie	k1gFnSc6	virologie
a	a	k8xC	a
parazitologii	parazitologie	k1gFnSc6	parazitologie
se	se	k3xPyFc4	se
uplatnily	uplatnit	k5eAaPmAgFnP	uplatnit
metody	metoda	k1gFnPc1	metoda
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
v	v	k7c6	v
napadené	napadený	k2eAgFnSc6d1	napadená
tkáni	tkáň	k1gFnSc6	tkáň
detekovat	detekovat	k5eAaImF	detekovat
DNA	dna	k1gFnSc1	dna
pocházející	pocházející	k2eAgFnSc1d1	pocházející
z	z	k7c2	z
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
tuto	tento	k3xDgFnSc4	tento
tkáň	tkáň	k1gFnSc4	tkáň
napadly	napadnout	k5eAaPmAgInP	napadnout
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
dělá	dělat	k5eAaImIp3nS	dělat
buď	buď	k8xC	buď
pomocí	pomocí	k7c2	pomocí
různých	různý	k2eAgNnPc2d1	různé
DNA	dno	k1gNnSc2	dno
prób	próba	k1gFnPc2	próba
schopných	schopný	k2eAgFnPc2d1	schopná
se	se	k3xPyFc4	se
specificky	specificky	k6eAd1	specificky
vázat	vázat	k5eAaImF	vázat
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
sekvenci	sekvence	k1gFnSc4	sekvence
typickou	typický	k2eAgFnSc4d1	typická
pro	pro	k7c4	pro
daného	daný	k2eAgMnSc4d1	daný
parazita	parazit	k1gMnSc4	parazit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
např.	např.	kA	např.
cestou	cesta	k1gFnSc7	cesta
namnožení	namnožení	k1gNnSc2	namnožení
DNA	DNA	kA	DNA
pomocí	pomocí	k7c2	pomocí
polymerázové	polymerázový	k2eAgFnSc2d1	polymerázová
řetězové	řetězový	k2eAgFnSc2d1	řetězová
reakce	reakce	k1gFnSc2	reakce
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
sekvenováním	sekvenování	k1gNnSc7	sekvenování
–	–	k?	–
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
získat	získat	k5eAaPmF	získat
sekvenci	sekvence	k1gFnSc4	sekvence
DNA	dno	k1gNnSc2	dno
patogenních	patogenní	k2eAgInPc2d1	patogenní
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
mikrobiologové	mikrobiolog	k1gMnPc1	mikrobiolog
srovnají	srovnat	k5eAaPmIp3nP	srovnat
s	s	k7c7	s
databázemi	databáze	k1gFnPc7	databáze
patogenních	patogenní	k2eAgMnPc2d1	patogenní
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
pokročilé	pokročilý	k2eAgFnPc1d1	pokročilá
molekulární	molekulární	k2eAgFnPc1d1	molekulární
metody	metoda	k1gFnPc1	metoda
se	se	k3xPyFc4	se
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
např.	např.	kA	např.
při	při	k7c6	při
identifikaci	identifikace	k1gFnSc6	identifikace
těžko	těžko	k6eAd1	těžko
kultivovatelných	kultivovatelný	k2eAgFnPc2d1	kultivovatelný
bakterií	bakterie	k1gFnPc2	bakterie
či	či	k8xC	či
při	při	k7c6	při
určování	určování	k1gNnSc6	určování
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
virových	virový	k2eAgNnPc2d1	virové
či	či	k8xC	či
parazitárních	parazitární	k2eAgNnPc2d1	parazitární
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
diagnostické	diagnostický	k2eAgFnSc2d1	diagnostická
práce	práce	k1gFnSc2	práce
je	být	k5eAaImIp3nS	být
však	však	k9	však
i	i	k9	i
studium	studium	k1gNnSc4	studium
lidské	lidský	k2eAgFnSc2d1	lidská
DNA	DNA	kA	DNA
–	–	k?	–
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
v	v	k7c6	v
rakovinné	rakovinný	k2eAgFnSc6d1	rakovinná
terapii	terapie	k1gFnSc6	terapie
či	či	k8xC	či
při	při	k7c6	při
diagnostice	diagnostika	k1gFnSc6	diagnostika
některých	některý	k3yIgNnPc2	některý
genetických	genetický	k2eAgNnPc2d1	genetické
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
již	již	k6eAd1	již
molekulární	molekulární	k2eAgFnPc1d1	molekulární
metody	metoda	k1gFnPc1	metoda
našly	najít	k5eAaPmAgFnP	najít
v	v	k7c6	v
prenatální	prenatální	k2eAgFnSc6d1	prenatální
diagnostice	diagnostika	k1gFnSc6	diagnostika
chorob	choroba	k1gFnPc2	choroba
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ze	z	k7c2	z
vzorku	vzorek	k1gInSc2	vzorek
plodové	plodový	k2eAgFnSc2d1	plodová
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
testy	test	k1gInPc1	test
se	se	k3xPyFc4	se
rutinně	rutinně	k6eAd1	rutinně
provádí	provádět	k5eAaImIp3nS	provádět
z	z	k7c2	z
kapky	kapka	k1gFnSc2	kapka
krve	krev	k1gFnSc2	krev
novorozenců	novorozenec	k1gMnPc2	novorozenec
<g/>
.	.	kIx.	.
</s>
<s>
Testy	test	k1gInPc1	test
DNA	DNA	kA	DNA
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
genetického	genetický	k2eAgNnSc2d1	genetické
poradenství	poradenství	k1gNnSc2	poradenství
však	však	k9	však
dnes	dnes	k6eAd1	dnes
mohou	moct	k5eAaImIp3nP	moct
pomoci	pomoct	k5eAaPmF	pomoct
i	i	k8xC	i
párům	pár	k1gInPc3	pár
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
teprve	teprve	k6eAd1	teprve
dítě	dítě	k1gNnSc4	dítě
plánují	plánovat	k5eAaImIp3nP	plánovat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
vhodné	vhodný	k2eAgNnSc1d1	vhodné
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
v	v	k7c6	v
rodinné	rodinný	k2eAgFnSc6d1	rodinná
historii	historie	k1gFnSc6	historie
nějaké	nějaký	k3yIgNnSc1	nějaký
genetické	genetický	k2eAgNnSc1d1	genetické
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
genetické	genetický	k2eAgInPc1d1	genetický
testy	test	k1gInPc1	test
dostupné	dostupný	k2eAgInPc1d1	dostupný
všem	všecek	k3xTgMnPc3	všecek
zájemcům	zájemce	k1gMnPc3	zájemce
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
zjistit	zjistit	k5eAaPmF	zjistit
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
informací	informace	k1gFnPc2	informace
od	od	k7c2	od
těch	ten	k3xDgInPc2	ten
zřejmých	zřejmý	k2eAgInPc2d1	zřejmý
(	(	kIx(	(
<g/>
barva	barva	k1gFnSc1	barva
očí	oko	k1gNnPc2	oko
<g/>
)	)	kIx)	)
přes	přes	k7c4	přes
různé	různý	k2eAgFnPc4d1	různá
zajímavosti	zajímavost	k1gFnPc4	zajímavost
(	(	kIx(	(
<g/>
atletické	atletický	k2eAgFnPc4d1	atletická
vlohy	vloha	k1gFnPc4	vloha
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
vážné	vážný	k2eAgInPc4d1	vážný
údaje	údaj	k1gInPc4	údaj
(	(	kIx(	(
<g/>
náchylnost	náchylnost	k1gFnSc1	náchylnost
k	k	k7c3	k
rakovině	rakovina	k1gFnSc3	rakovina
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
genetická	genetický	k2eAgFnSc1d1	genetická
daktyloskopie	daktyloskopie	k1gFnSc1	daktyloskopie
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
oblasti	oblast	k1gFnPc1	oblast
např.	např.	kA	např.
lidské	lidský	k2eAgFnSc2d1	lidská
jaderné	jaderný	k2eAgFnSc2d1	jaderná
DNA	DNA	kA	DNA
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
proměnlivé	proměnlivý	k2eAgNnSc1d1	proměnlivé
a	a	k8xC	a
člověk	člověk	k1gMnSc1	člověk
od	od	k7c2	od
člověka	člověk	k1gMnSc2	člověk
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
téměř	téměř	k6eAd1	téměř
vždy	vždy	k6eAd1	vždy
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
DNA	DNA	kA	DNA
v	v	k7c6	v
kriminalistice	kriminalistika	k1gFnSc6	kriminalistika
a	a	k8xC	a
v	v	k7c6	v
forenzních	forenzní	k2eAgFnPc6d1	forenzní
vědách	věda	k1gFnPc6	věda
neocenitelným	ocenitelný	k2eNgInSc7d1	neocenitelný
zdrojem	zdroj	k1gInSc7	zdroj
informací	informace	k1gFnPc2	informace
<g/>
.	.	kIx.	.
</s>
<s>
Repetitivní	repetitivní	k2eAgFnSc1d1	repetitivní
sekvence	sekvence	k1gFnSc1	sekvence
známé	známý	k2eAgFnSc2d1	známá
jako	jako	k8xS	jako
VNTR	VNTR	kA	VNTR
či	či	k8xC	či
STR	str	kA	str
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
ty	ten	k3xDgInPc4	ten
nejčastěji	často	k6eAd3	často
studované	studovaný	k2eAgInPc4d1	studovaný
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
VNTR	VNTR	kA	VNTR
repetic	repetice	k1gFnPc2	repetice
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
relativně	relativně	k6eAd1	relativně
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
zejména	zejména	k9	zejména
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
máme	mít	k5eAaImIp1nP	mít
<g/>
-li	i	k?	-li
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
vzorek	vzorek	k1gInSc4	vzorek
krve	krev	k1gFnSc2	krev
(	(	kIx(	(
<g/>
např.	např.	kA	např.
u	u	k7c2	u
testů	test	k1gInPc2	test
otcovství	otcovství	k1gNnSc2	otcovství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
testují	testovat	k5eAaImIp3nP	testovat
metodou	metoda	k1gFnSc7	metoda
RFLP	RFLP	kA	RFLP
(	(	kIx(	(
<g/>
jenž	jenž	k3xRgMnSc1	jenž
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
polymorfismus	polymorfismus	k1gInSc4	polymorfismus
délky	délka	k1gFnSc2	délka
restrikčních	restrikční	k2eAgInPc2d1	restrikční
fragmentů	fragment	k1gInPc2	fragment
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kriminalistice	kriminalistika	k1gFnSc6	kriminalistika
našly	najít	k5eAaPmAgFnP	najít
větší	veliký	k2eAgNnSc4d2	veliký
využití	využití	k1gNnSc4	využití
tzv.	tzv.	kA	tzv.
STR	str	kA	str
(	(	kIx(	(
<g/>
čili	čili	k8xC	čili
~	~	kIx~	~
<g/>
mikrosatelity	mikrosatelita	k1gFnSc2	mikrosatelita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
dvě	dva	k4xCgFnPc1	dva
osoby	osoba	k1gFnPc1	osoba
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
jednu	jeden	k4xCgFnSc4	jeden
STR	str	kA	str
oblast	oblast	k1gFnSc4	oblast
shodnou	shodný	k2eAgFnSc7d1	shodná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
danou	daný	k2eAgFnSc4d1	daná
variantu	varianta	k1gFnSc4	varianta
např.	např.	kA	např.
1	[number]	k4	1
:	:	kIx,	:
83	[number]	k4	83
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
nebylo	být	k5eNaImAgNnS	být
příliš	příliš	k6eAd1	příliš
přesvědčivé	přesvědčivý	k2eAgNnSc1d1	přesvědčivé
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
obvykle	obvykle	k6eAd1	obvykle
13	[number]	k4	13
markerů	marker	k1gInPc2	marker
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
vyhodnocují	vyhodnocovat	k5eAaImIp3nP	vyhodnocovat
zvlášť	zvlášť	k6eAd1	zvlášť
a	a	k8xC	a
vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
výsledek	výsledek	k1gInSc1	výsledek
důvěryhodnost	důvěryhodnost	k1gFnSc1	důvěryhodnost
testu	test	k1gInSc2	test
mnohonásobně	mnohonásobně	k6eAd1	mnohonásobně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
použití	použití	k1gNnSc1	použití
DNA	DNA	kA	DNA
v	v	k7c6	v
kriminalistice	kriminalistika	k1gFnSc6	kriminalistika
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1986	[number]	k4	1986
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
soudního	soudní	k2eAgNnSc2d1	soudní
řízení	řízení	k1gNnSc2	řízení
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Testování	testování	k1gNnSc1	testování
STR	str	kA	str
oblastí	oblast	k1gFnPc2	oblast
se	se	k3xPyFc4	se
však	však	k9	však
dnes	dnes	k6eAd1	dnes
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
i	i	k9	i
v	v	k7c6	v
určování	určování	k1gNnSc6	určování
otcovství	otcovství	k1gNnSc2	otcovství
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
genetické	genetický	k2eAgFnSc2d1	genetická
inženýrství	inženýrství	k1gNnSc3	inženýrství
a	a	k8xC	a
geneticky	geneticky	k6eAd1	geneticky
modifikovaný	modifikovaný	k2eAgInSc4d1	modifikovaný
organismus	organismus	k1gInSc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
lidstvo	lidstvo	k1gNnSc1	lidstvo
schopné	schopný	k2eAgNnSc1d1	schopné
provádět	provádět	k5eAaImF	provádět
cílené	cílený	k2eAgFnPc4d1	cílená
změny	změna	k1gFnPc4	změna
v	v	k7c6	v
genetické	genetický	k2eAgFnSc6d1	genetická
informaci	informace	k1gFnSc6	informace
(	(	kIx(	(
<g/>
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
nukleotidů	nukleotid	k1gInPc2	nukleotid
v	v	k7c6	v
DNA	DNA	kA	DNA
<g/>
)	)	kIx)	)
a	a	k8xC	a
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
tím	ten	k3xDgNnSc7	ten
některé	některý	k3yIgFnPc1	některý
vlastnosti	vlastnost	k1gFnPc1	vlastnost
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
tzv.	tzv.	kA	tzv.
genetické	genetický	k2eAgFnPc4d1	genetická
modifikace	modifikace	k1gFnPc4	modifikace
způsobily	způsobit	k5eAaPmAgInP	způsobit
revoluci	revoluce	k1gFnSc4	revoluce
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
biotechnologických	biotechnologický	k2eAgNnPc2d1	Biotechnologické
odvětví	odvětví	k1gNnPc2	odvětví
a	a	k8xC	a
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
např.	např.	kA	např.
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
produkci	produkce	k1gFnSc4	produkce
hormonů	hormon	k1gInPc2	hormon
<g/>
,	,	kIx,	,
srážecích	srážecí	k2eAgInPc2d1	srážecí
faktorů	faktor	k1gInPc2	faktor
pro	pro	k7c4	pro
hemofiliky	hemofilik	k1gMnPc4	hemofilik
<g/>
,	,	kIx,	,
enzymů	enzym	k1gInPc2	enzym
užívaných	užívaný	k2eAgInPc2d1	užívaný
v	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
vakcín	vakcína	k1gFnPc2	vakcína
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
genetického	genetický	k2eAgNnSc2d1	genetické
inženýrství	inženýrství	k1gNnSc2	inženýrství
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
různé	různý	k2eAgFnPc1d1	různá
transgenní	transgenní	k2eAgFnPc1d1	transgenní
plodiny	plodina	k1gFnPc1	plodina
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ty	ty	k3xPp2nSc5	ty
odolné	odolný	k2eAgFnPc1d1	odolná
k	k	k7c3	k
herbicidům	herbicid	k1gInPc3	herbicid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
geneticky	geneticky	k6eAd1	geneticky
modifikovaných	modifikovaný	k2eAgFnPc2d1	modifikovaná
plodin	plodina	k1gFnPc2	plodina
povolena	povolit	k5eAaPmNgFnS	povolit
pouze	pouze	k6eAd1	pouze
Bt	Bt	k1gFnSc1	Bt
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nese	nést	k5eAaImIp3nS	nést
gen	gen	k1gInSc4	gen
cry	cry	k?	cry
pocházející	pocházející	k2eAgFnSc2d1	pocházející
z	z	k7c2	z
půdní	půdní	k2eAgFnSc2d1	půdní
bakterie	bakterie	k1gFnSc2	bakterie
Bacillus	Bacillus	k1gMnSc1	Bacillus
thuringiensis	thuringiensis	k1gFnSc1	thuringiensis
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
gen	gen	k1gInSc1	gen
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
rostlina	rostlina	k1gFnSc1	rostlina
pro	pro	k7c4	pro
své	svůj	k3xOyFgMnPc4	svůj
hmyzí	hmyzí	k2eAgMnPc4d1	hmyzí
škůdce	škůdce	k1gMnPc4	škůdce
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
biologická	biologický	k2eAgFnSc1d1	biologická
systematika	systematika	k1gFnSc1	systematika
a	a	k8xC	a
fylogenetika	fylogenetika	k1gFnSc1	fylogenetika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
se	se	k3xPyFc4	se
studium	studium	k1gNnSc1	studium
sekvencí	sekvence	k1gFnPc2	sekvence
DNA	DNA	kA	DNA
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
v	v	k7c6	v
třídění	třídění	k1gNnSc6	třídění
organismů	organismus	k1gInPc2	organismus
podle	podle	k7c2	podle
jejich	jejich	k3xOp3gFnSc2	jejich
příbuznosti	příbuznost	k1gFnSc2	příbuznost
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
biologie	biologie	k1gFnSc2	biologie
známém	známý	k2eAgNnSc6d1	známé
jako	jako	k9	jako
fylogenetika	fylogenetik	k1gMnSc2	fylogenetik
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
krůčků	krůček	k1gInPc2	krůček
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
oboru	obor	k1gInSc6	obor
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
studie	studie	k1gFnSc1	studie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
srovnávala	srovnávat	k5eAaImAgFnS	srovnávat
sekvenci	sekvence	k1gFnSc4	sekvence
genu	gen	k1gInSc2	gen
pro	pro	k7c4	pro
cytochrom	cytochrom	k1gInSc4	cytochrom
c	c	k0	c
u	u	k7c2	u
různých	různý	k2eAgInPc2d1	různý
organismů	organismus	k1gInPc2	organismus
<g/>
:	:	kIx,	:
výsledky	výsledek	k1gInPc1	výsledek
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
intuitivní	intuitivní	k2eAgNnSc1d1	intuitivní
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
šimpanz	šimpanz	k1gMnSc1	šimpanz
má	mít	k5eAaImIp3nS	mít
sekvenci	sekvence	k1gFnSc4	sekvence
tohoto	tento	k3xDgInSc2	tento
genu	gen	k1gInSc2	gen
s	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
zcela	zcela	k6eAd1	zcela
shodnou	shodnout	k5eAaPmIp3nP	shodnout
a	a	k8xC	a
makak	makak	k1gMnSc1	makak
rhesus	rhesus	k1gMnSc1	rhesus
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
pouze	pouze	k6eAd1	pouze
jedinou	jediný	k2eAgFnSc7d1	jediná
nukleotidovou	nukleotidový	k2eAgFnSc7d1	nukleotidová
záměnou	záměna	k1gFnSc7	záměna
<g/>
,	,	kIx,	,
psí	psí	k2eAgInSc4d1	psí
gen	gen	k1gInSc4	gen
pro	pro	k7c4	pro
cytochrom	cytochrom	k1gInSc4	cytochrom
už	už	k6eAd1	už
se	se	k3xPyFc4	se
od	od	k7c2	od
lidského	lidský	k2eAgInSc2d1	lidský
genu	gen	k1gInSc2	gen
liší	lišit	k5eAaImIp3nS	lišit
na	na	k7c6	na
13	[number]	k4	13
místech	místo	k1gNnPc6	místo
a	a	k8xC	a
kvasinkový	kvasinkový	k2eAgInSc4d1	kvasinkový
gen	gen	k1gInSc4	gen
dokonce	dokonce	k9	dokonce
na	na	k7c6	na
56	[number]	k4	56
pozicích	pozice	k1gFnPc6	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgFnPc2	tento
informací	informace	k1gFnPc2	informace
si	se	k3xPyFc3	se
lze	lze	k6eAd1	lze
udělat	udělat	k5eAaPmF	udělat
obrázek	obrázek	k1gInSc4	obrázek
o	o	k7c6	o
příbuzenských	příbuzenský	k2eAgInPc6d1	příbuzenský
vztazích	vztah	k1gInPc6	vztah
mezi	mezi	k7c7	mezi
organismy	organismus	k1gInPc7	organismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
rozmachem	rozmach	k1gInSc7	rozmach
sekvenování	sekvenování	k1gNnSc2	sekvenování
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
obrovské	obrovský	k2eAgNnSc1d1	obrovské
množství	množství	k1gNnSc1	množství
sekvencí	sekvence	k1gFnPc2	sekvence
DNA	dno	k1gNnSc2	dno
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
organismů	organismus	k1gInPc2	organismus
a	a	k8xC	a
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
analýze	analýza	k1gFnSc3	analýza
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
různé	různý	k2eAgInPc1d1	různý
sofistikované	sofistikovaný	k2eAgInPc1d1	sofistikovaný
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
metoda	metoda	k1gFnSc1	metoda
parsimonie	parsimonie	k1gFnSc1	parsimonie
nebo	nebo	k8xC	nebo
metoda	metoda	k1gFnSc1	metoda
maximální	maximální	k2eAgFnSc2d1	maximální
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
i	i	k9	i
odhadnout	odhadnout	k5eAaPmF	odhadnout
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
dělí	dělit	k5eAaImIp3nS	dělit
v	v	k7c6	v
evoluční	evoluční	k2eAgFnSc6d1	evoluční
historii	historie	k1gFnSc6	historie
libovolné	libovolný	k2eAgInPc4d1	libovolný
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc4	druh
–	–	k?	–
metoda	metoda	k1gFnSc1	metoda
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
užívaná	užívaný	k2eAgFnSc1d1	užívaná
opět	opět	k6eAd1	opět
pracuje	pracovat	k5eAaImIp3nS	pracovat
se	s	k7c7	s
sekvencemi	sekvence	k1gFnPc7	sekvence
DNA	DNA	kA	DNA
a	a	k8xC	a
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
molekulární	molekulární	k2eAgFnPc1d1	molekulární
hodiny	hodina	k1gFnPc1	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
fylogenetických	fylogenetický	k2eAgInPc2d1	fylogenetický
přístupů	přístup	k1gInPc2	přístup
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
odpovídat	odpovídat	k5eAaImF	odpovídat
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgFnPc2d1	další
otázek	otázka	k1gFnPc2	otázka
<g/>
,	,	kIx,	,
namátkou	namátkou	k6eAd1	namátkou
"	"	kIx"	"
<g/>
jaký	jaký	k3yIgInSc4	jaký
vztah	vztah	k1gInSc4	vztah
mají	mít	k5eAaImIp3nP	mít
neandertálci	neandertálec	k1gMnPc1	neandertálec
k	k	k7c3	k
dnešním	dnešní	k2eAgMnPc3d1	dnešní
lidem	člověk	k1gMnPc3	člověk
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgMnPc7d1	jednotlivý
nemocnými	nemocný	k1gMnPc7	nemocný
šíří	šířit	k5eAaImIp3nS	šířit
virus	virus	k1gInSc1	virus
HIV	HIV	kA	HIV
<g/>
"	"	kIx"	"
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
studie	studie	k1gFnPc1	studie
o	o	k7c6	o
údajném	údajný	k2eAgNnSc6d1	údajné
izolování	izolování	k1gNnSc6	izolování
sekvencí	sekvence	k1gFnPc2	sekvence
DNA	dno	k1gNnSc2	dno
pravěkých	pravěký	k2eAgInPc2d1	pravěký
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
přinesl	přinést	k5eAaPmAgMnS	přinést
tomuto	tento	k3xDgInSc3	tento
oboru	obor	k1gInSc3	obor
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
paleogenetika	paleogenetika	k1gFnSc1	paleogenetika
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
slávu	sláva	k1gFnSc4	sláva
román	román	k1gInSc4	román
a	a	k8xC	a
následný	následný	k2eAgInSc4d1	následný
film	film	k1gInSc4	film
Jurský	jurský	k2eAgInSc1d1	jurský
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
byli	být	k5eAaImAgMnP	být
naklonováni	naklonovat	k5eAaPmNgMnP	naklonovat
druhohorní	druhohorní	k2eAgMnPc1d1	druhohorní
dinosauři	dinosaurus	k1gMnPc1	dinosaurus
<g/>
.	.	kIx.	.
</s>
<s>
Dinosauří	dinosauří	k2eAgInPc4d1	dinosauří
proteiny	protein	k1gInPc4	protein
a	a	k8xC	a
měkké	měkký	k2eAgFnPc4d1	měkká
tkáně	tkáň	k1gFnPc4	tkáň
byly	být	k5eAaImAgFnP	být
skutečně	skutečně	k6eAd1	skutečně
získány	získat	k5eAaPmNgFnP	získat
např.	např.	kA	např.
Mary	Mary	k1gFnSc2	Mary
H.	H.	kA	H.
Schweitzerovou	Schweitzerová	k1gFnSc7	Schweitzerová
a	a	k8xC	a
jejím	její	k3xOp3gInSc7	její
týmem	tým	k1gInSc7	tým
<g/>
,	,	kIx,	,
pravěká	pravěký	k2eAgFnSc1d1	pravěká
DNA	dna	k1gFnSc1	dna
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
ale	ale	k8xC	ale
nejspíš	nejspíš	k9	nejspíš
nikdy	nikdy	k6eAd1	nikdy
získána	získán	k2eAgFnSc1d1	získána
nebude	být	k5eNaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Nepotvrdily	potvrdit	k5eNaPmAgInP	potvrdit
se	se	k3xPyFc4	se
ani	ani	k9	ani
domněnky	domněnka	k1gFnPc1	domněnka
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
DNA	DNA	kA	DNA
ze	z	k7c2	z
120	[number]	k4	120
-	-	kIx~	-
135	[number]	k4	135
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
starého	starý	k2eAgInSc2d1	starý
libanonského	libanonský	k2eAgInSc2d1	libanonský
jantaru	jantar	k1gInSc2	jantar
entomologem	entomolog	k1gMnSc7	entomolog
Georgem	Georg	k1gMnSc7	Georg
Poinarem	Poinar	k1gMnSc7	Poinar
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
