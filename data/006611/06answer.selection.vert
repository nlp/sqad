<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
nabízí	nabízet	k5eAaImIp3nS	nabízet
prenatální	prenatální	k2eAgInSc4d1	prenatální
neinvazivní	invazivní	k2eNgInSc4d1	neinvazivní
test	test	k1gInSc4	test
Downova	Downův	k2eAgInSc2d1	Downův
syndromu	syndrom	k1gInSc2	syndrom
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
častých	častý	k2eAgFnPc2d1	častá
chromozomálních	chromozomální	k2eAgFnPc2d1	chromozomální
vad	vada	k1gFnPc2	vada
(	(	kIx(	(
<g/>
trizomie	trizomie	k1gFnSc1	trizomie
chromozomů	chromozom	k1gInPc2	chromozom
13	[number]	k4	13
a	a	k8xC	a
18	[number]	k4	18
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
zkratky	zkratka	k1gFnSc2	zkratka
NIPD	NIPD	kA	NIPD
<g/>
,	,	kIx,	,
NIPT	NIPT	kA	NIPT
<g/>
))	))	k?	))
další	další	k2eAgFnSc2d1	další
americké	americký	k2eAgFnSc2d1	americká
společnosti	společnost	k1gFnSc2	společnost
(	(	kIx(	(
<g/>
Ariosa	arioso	k1gNnSc2	arioso
<g/>
,	,	kIx,	,
Verinata	Verinata	k1gFnSc1	Verinata
<g/>
,	,	kIx,	,
Natera	Natera	k1gFnSc1	Natera
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
