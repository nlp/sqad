<s>
Zemská	zemský	k2eAgFnSc1d1	zemská
kůra	kůra	k1gFnSc1	kůra
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
svrchní	svrchní	k2eAgFnPc4d1	svrchní
vrstvy	vrstva	k1gFnPc4	vrstva
planety	planeta	k1gFnSc2	planeta
Země	zem	k1gFnSc2	zem
složený	složený	k2eAgMnSc1d1	složený
z	z	k7c2	z
utuhlých	utuhlý	k2eAgFnPc2d1	utuhlá
hornin	hornina	k1gFnPc2	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
přinejmenším	přinejmenším	k6eAd1	přinejmenším
před	před	k7c7	před
4,6	[number]	k4	4,6
miliardami	miliarda	k4xCgFnPc7	miliarda
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
do	do	k7c2	do
150	[number]	k4	150
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zemské	zemský	k2eAgNnSc1d1	zemské
těleso	těleso	k1gNnSc1	těleso
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
soustředných	soustředný	k2eAgFnPc2d1	soustředná
sfér	sféra	k1gFnPc2	sféra
<g/>
:	:	kIx,	:
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
zemského	zemský	k2eAgInSc2d1	zemský
pláště	plášť	k1gInSc2	plášť
a	a	k8xC	a
zemského	zemský	k2eAgNnSc2d1	zemské
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Hranice	hranice	k1gFnSc1	hranice
mezi	mezi	k7c7	mezi
sférami	sféra	k1gFnPc7	sféra
tvoří	tvořit	k5eAaImIp3nP	tvořit
plochy	plocha	k1gFnSc2	plocha
nespojitosti	nespojitost	k1gFnSc2	nespojitost
(	(	kIx(	(
<g/>
diskontinuity	diskontinuita	k1gFnSc2	diskontinuita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
plochy	plocha	k1gFnPc1	plocha
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
náhlé	náhlý	k2eAgFnPc1d1	náhlá
změny	změna	k1gFnPc1	změna
chemických	chemický	k2eAgFnPc2d1	chemická
a	a	k8xC	a
fyzikálních	fyzikální	k2eAgFnPc2d1	fyzikální
vlastností	vlastnost	k1gFnPc2	vlastnost
látek	látka	k1gFnPc2	látka
uvnitř	uvnitř	k7c2	uvnitř
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Kůru	kůra	k1gFnSc4	kůra
od	od	k7c2	od
pláště	plášť	k1gInSc2	plášť
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
tzv.	tzv.	kA	tzv.
Mohorovičičova	Mohorovičičův	k2eAgFnSc1d1	Mohorovičičova
plocha	plocha	k1gFnSc1	plocha
diskontinuity	diskontinuita	k1gFnSc2	diskontinuita
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
odrážejí	odrážet	k5eAaImIp3nP	odrážet
a	a	k8xC	a
lomí	lomit	k5eAaImIp3nP	lomit
zemětřesné	zemětřesný	k2eAgFnPc1d1	zemětřesná
vlny	vlna	k1gFnPc1	vlna
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
kůře	kůra	k1gFnSc6	kůra
existují	existovat	k5eAaImIp3nP	existovat
další	další	k2eAgFnPc1d1	další
podružné	podružný	k2eAgFnPc1d1	podružná
plochy	plocha	k1gFnPc1	plocha
nespojitosti	nespojitost	k1gFnSc2	nespojitost
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
průběh	průběh	k1gInSc1	průběh
však	však	k9	však
není	být	k5eNaImIp3nS	být
souvislý	souvislý	k2eAgInSc1d1	souvislý
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgMnPc1d3	nejdůležitější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
Conradova	Conradův	k2eAgFnSc1d1	Conradova
plocha	plocha	k1gFnSc1	plocha
<g/>
,	,	kIx,	,
považovaná	považovaný	k2eAgFnSc1d1	považovaná
za	za	k7c4	za
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
svrchní	svrchní	k2eAgFnSc7d1	svrchní
(	(	kIx(	(
<g/>
granitovou	granitový	k2eAgFnSc7d1	granitová
<g/>
)	)	kIx)	)
a	a	k8xC	a
spodní	spodní	k2eAgFnSc7d1	spodní
(	(	kIx(	(
<g/>
bazaltovou	bazaltový	k2eAgFnSc7d1	bazaltová
<g/>
)	)	kIx)	)
vrstvou	vrstva	k1gFnSc7	vrstva
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
</s>
<s>
Zemská	zemský	k2eAgFnSc1d1	zemská
kůra	kůra	k1gFnSc1	kůra
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
SIAL	sial	k1gInSc1	sial
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
žulovou	žulový	k2eAgFnSc7d1	Žulová
(	(	kIx(	(
<g/>
granitovou	granitový	k2eAgFnSc7d1	granitová
<g/>
)	)	kIx)	)
a	a	k8xC	a
čedičovou	čedičový	k2eAgFnSc7d1	čedičová
(	(	kIx(	(
<g/>
bazaltovou	bazaltový	k2eAgFnSc7d1	bazaltová
<g/>
)	)	kIx)	)
vrstvou	vrstva	k1gFnSc7	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
mocnost	mocnost	k1gFnSc1	mocnost
(	(	kIx(	(
<g/>
tloušťka	tloušťka	k1gFnSc1	tloušťka
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
5	[number]	k4	5
km	km	kA	km
do	do	k7c2	do
70	[number]	k4	70
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Nejsilnější	silný	k2eAgMnSc1d3	nejsilnější
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
kontinentech	kontinent	k1gInPc6	kontinent
pod	pod	k7c7	pod
pohořími	pohoří	k1gNnPc7	pohoří
<g/>
,	,	kIx,	,
nejtenčí	tenký	k2eAgFnSc7d3	nejtenčí
(	(	kIx(	(
<g/>
5	[number]	k4	5
až	až	k9	až
10	[number]	k4	10
km	km	kA	km
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
oceány	oceán	k1gInPc7	oceán
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
chybí	chybit	k5eAaPmIp3nS	chybit
žulová	žulový	k2eAgFnSc1d1	Žulová
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
masívu	masív	k1gInSc6	masív
se	se	k3xPyFc4	se
mocnost	mocnost	k1gFnSc1	mocnost
kůry	kůra	k1gFnSc2	kůra
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
35	[number]	k4	35
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
minerály	minerál	k1gInPc7	minerál
kůry	kůra	k1gFnSc2	kůra
jsou	být	k5eAaImIp3nP	být
živce	živec	k1gInPc1	živec
<g/>
,	,	kIx,	,
křemen	křemen	k1gInSc1	křemen
a	a	k8xC	a
slídy	slída	k1gFnPc1	slída
(	(	kIx(	(
<g/>
muskovit	muskovit	k1gInSc1	muskovit
<g/>
,	,	kIx,	,
biotit	biotit	k1gInSc1	biotit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Názvy	název	k1gInPc1	název
granitová	granitový	k2eAgNnPc1d1	granitové
a	a	k8xC	a
čedičová	čedičový	k2eAgNnPc1d1	čedičové
neznamenají	znamenat	k5eNaImIp3nP	znamenat
petrografické	petrografický	k2eAgNnSc4d1	petrografické
složení	složení	k1gNnSc4	složení
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
subjektivní	subjektivní	k2eAgNnSc4d1	subjektivní
označení	označení	k1gNnSc4	označení
vrstvy	vrstva	k1gFnSc2	vrstva
názvem	název	k1gInSc7	název
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nejvíce	nejvíce	k6eAd1	nejvíce
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
známým	známý	k2eAgFnPc3d1	známá
fyzikálním	fyzikální	k2eAgFnPc3d1	fyzikální
vlastnostem	vlastnost	k1gFnPc3	vlastnost
horniny	hornina	k1gFnSc2	hornina
skládajících	skládající	k2eAgFnPc2d1	skládající
tyto	tento	k3xDgFnPc4	tento
vrstvy	vrstva	k1gFnPc4	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
pevninské	pevninský	k2eAgFnSc2d1	pevninská
kůry	kůra	k1gFnSc2	kůra
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
pestřejší	pestrý	k2eAgFnSc1d2	pestřejší
následkem	následkem	k7c2	následkem
složitějšího	složitý	k2eAgInSc2d2	složitější
vývoje	vývoj	k1gInSc2	vývoj
pevnin	pevnina	k1gFnPc2	pevnina
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vývoje	vývoj	k1gInSc2	vývoj
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
do	do	k7c2	do
kůry	kůra	k1gFnSc2	kůra
z	z	k7c2	z
pláště	plášť	k1gInSc2	plášť
přesunuly	přesunout	k5eAaPmAgInP	přesunout
lehce	lehko	k6eAd1	lehko
tavitelné	tavitelný	k2eAgInPc1d1	tavitelný
a	a	k8xC	a
specificky	specificky	k6eAd1	specificky
lehčí	lehký	k2eAgFnPc4d2	lehčí
složky	složka	k1gFnPc4	složka
<g/>
,	,	kIx,	,
především	především	k9	především
sloučeniny	sloučenina	k1gFnPc1	sloučenina
křemíku	křemík	k1gInSc2	křemík
<g/>
,	,	kIx,	,
draslíku	draslík	k1gInSc2	draslík
<g/>
,	,	kIx,	,
hliníku	hliník	k1gInSc2	hliník
<g/>
,	,	kIx,	,
vápníku	vápník	k1gInSc2	vápník
<g/>
,	,	kIx,	,
sodíku	sodík	k1gInSc2	sodík
a	a	k8xC	a
pod	pod	k7c7	pod
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
hustota	hustota	k1gFnSc1	hustota
kůry	kůra	k1gFnSc2	kůra
činí	činit	k5eAaImIp3nS	činit
2,85	[number]	k4	2,85
g	g	kA	g
<g/>
/	/	kIx~	/
<g/>
cm3	cm3	k4	cm3
<g/>
.	.	kIx.	.
</s>
<s>
Zemskou	zemský	k2eAgFnSc4d1	zemská
kůru	kůra	k1gFnSc4	kůra
tvoří	tvořit	k5eAaImIp3nP	tvořit
různé	různý	k2eAgFnPc1d1	různá
horniny	hornina	k1gFnPc1	hornina
složené	složený	k2eAgFnPc1d1	složená
z	z	k7c2	z
nerostů	nerost	k1gInPc2	nerost
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
nich	on	k3xPp3gFnPc2	on
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
obsaženy	obsáhnout	k5eAaPmNgFnP	obsáhnout
i	i	k9	i
plyny	plyn	k1gInPc1	plyn
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
s	s	k7c7	s
různým	různý	k2eAgNnSc7d1	různé
množstvím	množství	k1gNnSc7	množství
rozpuštěných	rozpuštěný	k2eAgFnPc2d1	rozpuštěná
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc1	všechen
chemické	chemický	k2eAgInPc1d1	chemický
prvky	prvek	k1gInPc1	prvek
<g/>
.	.	kIx.	.
kontinentální	kontinentální	k2eAgFnSc1d1	kontinentální
kůra	kůra	k1gFnSc1	kůra
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
3	[number]	k4	3
vrstvy	vrstva	k1gFnSc2	vrstva
a	a	k8xC	a
proměnlivou	proměnlivý	k2eAgFnSc4d1	proměnlivá
tloušťku	tloušťka	k1gFnSc4	tloušťka
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
kontinenty	kontinent	k1gInPc4	kontinent
<g/>
,	,	kIx,	,
šelfy	šelf	k1gInPc4	šelf
<g/>
,	,	kIx,	,
kontinentální	kontinentální	k2eAgInPc4d1	kontinentální
svahy	svah	k1gInPc4	svah
oceánská	oceánský	k2eAgFnSc1d1	oceánská
kůra	kůra	k1gFnSc1	kůra
-	-	kIx~	-
dvouvrstvá	dvouvrstvá	k1gFnSc1	dvouvrstvá
<g/>
,	,	kIx,	,
tenčí	tenčí	k1gNnPc1	tenčí
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgNnPc1d1	tvořící
dna	dno	k1gNnPc1	dno
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
pánve	pánev	k1gFnPc1	pánev
<g/>
,	,	kIx,	,
příkopy	příkop	k1gInPc1	příkop
přechodná	přechodný	k2eAgNnPc4d1	přechodné
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
nejvzácnější	vzácný	k2eAgInPc1d3	nejvzácnější
<g/>
,	,	kIx,	,
třívrstvá	třívrstvat	k5eAaImIp3nS	třívrstvat
(	(	kIx(	(
<g/>
dno	dno	k1gNnSc1	dno
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
)	)	kIx)	)
SiO	SiO	k1gFnSc1	SiO
<g/>
2	[number]	k4	2
-	-	kIx~	-
69	[number]	k4	69
%	%	kIx~	%
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
-	-	kIx~	-
14	[number]	k4	14
%	%	kIx~	%
Fe	Fe	k1gFnSc2	Fe
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
+	+	kIx~	+
FeO	FeO	k1gFnSc2	FeO
-	-	kIx~	-
4	[number]	k4	4
%	%	kIx~	%
Ostatní	ostatní	k1gNnSc4	ostatní
5	[number]	k4	5
%	%	kIx~	%
SiO	SiO	k1gFnSc1	SiO
<g/>
2	[number]	k4	2
-	-	kIx~	-
48	[number]	k4	48
%	%	kIx~	%
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
-	-	kIx~	-
15	[number]	k4	15
%	%	kIx~	%
CaO	CaO	k1gFnSc2	CaO
-	-	kIx~	-
11	[number]	k4	11
%	%	kIx~	%
Fe	Fe	k1gFnSc2	Fe
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
+	+	kIx~	+
FeO	FeO	k1gFnSc2	FeO
-	-	kIx~	-
11	[number]	k4	11
%	%	kIx~	%
MgO	MgO	k1gFnSc2	MgO
-	-	kIx~	-
9	[number]	k4	9
%	%	kIx~	%
Ostatní	ostatní	k1gNnSc4	ostatní
6	[number]	k4	6
%	%	kIx~	%
</s>
