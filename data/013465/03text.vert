<p>
<s>
Georg	Georg	k1gMnSc1	Georg
Stumme	Stumme	k1gMnSc1	Stumme
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1886	[number]	k4	1886
<g/>
,	,	kIx,	,
Halberstadt	Halberstadt	k1gInSc1	Halberstadt
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
–	–	k?	–
24	[number]	k4	24
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1942	[number]	k4	1942
El	Ela	k1gFnPc2	Ela
Alamein	Alameina	k1gFnPc2	Alameina
<g/>
,	,	kIx,	,
Egypt	Egypt	k1gInSc1	Egypt
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
vyznamenaný	vyznamenaný	k2eAgMnSc1d1	vyznamenaný
veterán	veterán	k1gMnSc1	veterán
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
a	a	k8xC	a
pozdější	pozdní	k2eAgMnSc1d2	pozdější
generál	generál	k1gMnSc1	generál
Wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
generál	generál	k1gMnSc1	generál
tankových	tankový	k2eAgFnPc2d1	tanková
jednotek	jednotka	k1gFnPc2	jednotka
(	(	kIx(	(
<g/>
General	General	k1gFnSc1	General
der	drát	k5eAaImRp2nS	drát
Panzertruppe	Panzertrupp	k1gMnSc5	Panzertrupp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
byl	být	k5eAaImAgInS	být
držitelem	držitel	k1gMnSc7	držitel
mnoha	mnoho	k4c2	mnoho
vojenských	vojenský	k2eAgNnPc2d1	vojenské
vyznamenání	vyznamenání	k1gNnPc2	vyznamenání
včetně	včetně	k7c2	včetně
Rytířského	rytířský	k2eAgInSc2d1	rytířský
kříže	kříž	k1gInSc2	kříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mládí	mládí	k1gNnSc6	mládí
a	a	k8xC	a
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
29	[number]	k4	29
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
1886	[number]	k4	1886
v	v	k7c6	v
anhaltském	anhaltský	k2eAgNnSc6d1	anhaltský
městě	město	k1gNnSc6	město
Halberstadt	Halberstadta	k1gFnPc2	Halberstadta
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1906	[number]	k4	1906
dokončil	dokončit	k5eAaPmAgInS	dokončit
úspěšně	úspěšně	k6eAd1	úspěšně
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
královské	královský	k2eAgFnSc6d1	královská
pruské	pruský	k2eAgFnSc6d1	pruská
vojenské	vojenský	k2eAgFnSc6d1	vojenská
akademii	akademie	k1gFnSc6	akademie
a	a	k8xC	a
jako	jako	k8xC	jako
kadet-čekatel	kadet-čekatel	k1gMnSc1	kadet-čekatel
(	(	kIx(	(
<g/>
Fahnenjunker	Fahnenjunker	k1gMnSc1	Fahnenjunker
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
zařazen	zařadit	k5eAaPmNgMnS	zařadit
ke	k	k7c3	k
57	[number]	k4	57
<g/>
.	.	kIx.	.
hornoslezskému	hornoslezský	k2eAgInSc3d1	hornoslezský
polnímu	polní	k2eAgInSc3d1	polní
dělostřeleckému	dělostřelecký	k2eAgInSc3d1	dělostřelecký
pluku	pluk	k1gInSc3	pluk
(	(	kIx(	(
<g/>
Oberschlesischen	Oberschlesischen	k2eAgInSc1d1	Oberschlesischen
Feld-Artillerie-Regiment	Feld-Artillerie-Regiment	k1gInSc1	Feld-Artillerie-Regiment
Nr	Nr	k1gFnSc2	Nr
<g/>
.	.	kIx.	.
57	[number]	k4	57
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
srpna	srpen	k1gInSc2	srpen
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělen	k2eAgFnSc1d1	udělena
hodnost	hodnost	k1gFnSc1	hodnost
poručíka	poručík	k1gMnSc2	poručík
(	(	kIx(	(
<g/>
Leutnant	Leutnant	k1gInSc1	Leutnant
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
adjutant	adjutant	k1gMnSc1	adjutant
praporu	prapor	k1gInSc2	prapor
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
byl	být	k5eAaImAgMnS	být
raněn	ranit	k5eAaPmNgMnS	ranit
na	na	k7c6	na
frontě	fronta	k1gFnSc6	fronta
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
převelen	převelet	k5eAaPmNgInS	převelet
do	do	k7c2	do
týlu	týl	k1gInSc2	týl
a	a	k8xC	a
následně	následně	k6eAd1	následně
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
výcvik	výcvik	k1gInSc1	výcvik
generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
již	již	k6eAd1	již
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
kapitána	kapitán	k1gMnSc4	kapitán
(	(	kIx(	(
<g/>
Hauptmann	Hauptmann	k1gMnSc1	Hauptmann
<g/>
)	)	kIx)	)
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
generálním	generální	k2eAgInSc6d1	generální
štábu	štáb	k1gInSc6	štáb
<g/>
,	,	kIx,	,
než	než	k8xS	než
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
náčelníka	náčelník	k1gMnSc2	náčelník
štábu	štáb	k1gInSc2	štáb
u	u	k7c2	u
33	[number]	k4	33
<g/>
.	.	kIx.	.
divize	divize	k1gFnSc2	divize
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
během	během	k7c2	během
války	válka	k1gFnSc2	válka
si	se	k3xPyFc3	se
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
oba	dva	k4xCgInPc4	dva
stupně	stupeň	k1gInPc4	stupeň
pruského	pruský	k2eAgInSc2d1	pruský
železného	železný	k2eAgInSc2d1	železný
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
Sasko-Meiningenský	Sasko-Meiningenský	k2eAgInSc1d1	Sasko-Meiningenský
čestný	čestný	k2eAgInSc1d1	čestný
kříž	kříž	k1gInSc1	kříž
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
a	a	k8xC	a
Odznak	odznak	k1gInSc4	odznak
za	za	k7c4	za
zranění	zranění	k1gNnSc4	zranění
v	v	k7c6	v
černém	černé	k1gNnSc6	černé
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Meziválečné	meziválečný	k2eAgNnSc4d1	meziválečné
období	období	k1gNnSc4	období
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
mladému	mladý	k1gMnSc3	mladý
kapitánovi	kapitán	k1gMnSc3	kapitán
Stumme	Stumme	k1gMnSc3	Stumme
dostalo	dostat	k5eAaPmAgNnS	dostat
příležitosti	příležitost	k1gFnPc4	příležitost
zůstat	zůstat	k5eAaPmF	zůstat
v	v	k7c6	v
nově	nově	k6eAd1	nově
vytvořené	vytvořený	k2eAgFnSc3d1	vytvořená
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
zredukované	zredukovaný	k2eAgFnSc3d1	zredukovaná
armádě	armáda	k1gFnSc3	armáda
–	–	k?	–
Reichswehru	Reichswehra	k1gFnSc4	Reichswehra
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
zařazen	zařadit	k5eAaPmNgMnS	zařadit
ke	k	k7c3	k
10	[number]	k4	10
<g/>
.	.	kIx.	.
pruskému	pruský	k2eAgInSc3d1	pruský
jízdnímu	jízdní	k2eAgInSc3d1	jízdní
pluku	pluk	k1gInSc3	pluk
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Preuß	Preuß	k1gFnSc1	Preuß
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
Reiter-Regiment	Reiter-Regiment	k1gInSc1	Reiter-Regiment
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sloužil	sloužit	k5eAaImAgInS	sloužit
u	u	k7c2	u
plukovního	plukovní	k2eAgInSc2d1	plukovní
štábu	štáb	k1gInSc2	štáb
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Züllichau	Züllichaus	k1gInSc2	Züllichaus
(	(	kIx(	(
<g/>
Sulechów	Sulechów	k1gFnSc1	Sulechów
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
byl	být	k5eAaImAgInS	být
převelen	převelet	k5eAaPmNgInS	převelet
do	do	k7c2	do
štábu	štáb	k1gInSc2	štáb
3	[number]	k4	3
<g/>
.	.	kIx.	.
jízdní	jízdní	k2eAgFnSc2d1	jízdní
divize	divize	k1gFnSc2	divize
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
generálporučíka	generálporučík	k1gMnSc2	generálporučík
Paula	Paul	k1gMnSc2	Paul
Haseho	Hase	k1gMnSc2	Hase
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
byl	být	k5eAaImAgMnS	být
také	také	k9	také
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
říjnu	říjen	k1gInSc3	říjen
roku	rok	k1gInSc2	rok
1926	[number]	k4	1926
povýšen	povýšit	k5eAaPmNgInS	povýšit
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
majora	major	k1gMnSc2	major
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
působení	působení	k1gNnSc1	působení
u	u	k7c2	u
štábu	štáb	k1gInSc2	štáb
2	[number]	k4	2
<g/>
.	.	kIx.	.
pruského	pruský	k2eAgInSc2d1	pruský
jízdního	jízdní	k2eAgInSc2d1	jízdní
pluku	pluk	k1gInSc2	pluk
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Preuß	Preuß	k1gFnSc1	Preuß
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
Reiter-Regiment	Reiter-Regiment	k1gInSc1	Reiter-Regiment
<g/>
)	)	kIx)	)
v	v	k7c6	v
Osterode	Osterod	k1gMnSc5	Osterod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgInS	dočkat
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
únoru	únor	k1gInSc3	únor
1931	[number]	k4	1931
povýšení	povýšení	k1gNnSc2	povýšení
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
podplukovníka	podplukovník	k1gMnSc2	podplukovník
(	(	kIx(	(
<g/>
Oberstleutnant	Oberstleutnant	k1gInSc1	Oberstleutnant
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
říjnu	říjen	k1gInSc3	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
nahradil	nahradit	k5eAaPmAgInS	nahradit
plukovníka	plukovník	k1gMnSc4	plukovník
Georga	Georg	k1gMnSc2	Georg
Lisacka	Lisacko	k1gNnSc2	Lisacko
jako	jako	k8xC	jako
velitele	velitel	k1gMnSc2	velitel
1	[number]	k4	1
<g/>
.	.	kIx.	.
pruského	pruský	k2eAgInSc2d1	pruský
jízdního	jízdní	k2eAgInSc2d1	jízdní
pluku	pluk	k1gInSc2	pluk
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Preuß	Preuß	k1gFnSc1	Preuß
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
Reiter-Regiment	Reiter-Regiment	k1gInSc1	Reiter-Regiment
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zanedlouho	zanedlouho	k6eAd1	zanedlouho
následovalo	následovat	k5eAaImAgNnS	následovat
povýšení	povýšení	k1gNnSc1	povýšení
na	na	k7c4	na
plukovníka	plukovník	k1gMnSc4	plukovník
a	a	k8xC	a
přesně	přesně	k6eAd1	přesně
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
říjnu	říjen	k1gInSc3	říjen
1934	[number]	k4	1934
byl	být	k5eAaImAgInS	být
Stumme	Stumme	k1gFnSc2	Stumme
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
velitele	velitel	k1gMnSc2	velitel
1	[number]	k4	1
<g/>
.	.	kIx.	.
pruského	pruský	k2eAgInSc2d1	pruský
jízdního	jízdní	k2eAgInSc2d1	jízdní
pluku	pluk	k1gInSc2	pluk
nahrazen	nahradit	k5eAaPmNgInS	nahradit
plukovníkem	plukovník	k1gMnSc7	plukovník
Kurtem	Kurt	k1gMnSc7	Kurt
Wuthenowem	Wuthenow	k1gMnSc7	Wuthenow
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
novým	nový	k2eAgInSc7d1	nový
služebním	služební	k2eAgInSc7d1	služební
postem	post	k1gInSc7	post
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
stalo	stát	k5eAaPmAgNnS	stát
Velitelství	velitelství	k1gNnSc4	velitelství
kavalerie	kavalerie	k1gFnSc2	kavalerie
Štětín	štětín	k1gMnSc1	štětín
(	(	kIx(	(
<g/>
Kavallerie-Kommando	Kavallerie-Kommanda	k1gFnSc5	Kavallerie-Kommanda
Stettin	Stettin	k2eAgMnSc1d1	Stettin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
jízdní	jízdní	k2eAgFnSc4d1	jízdní
brigádu	brigáda	k1gFnSc4	brigáda
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Reiter-Brigade	Reiter-Brigást	k5eAaPmIp3nS	Reiter-Brigást
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
Stumme	Stumme	k1gFnSc4	Stumme
převelen	převelet	k5eAaPmNgMnS	převelet
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zde	zde	k6eAd1	zde
převzal	převzít	k5eAaPmAgInS	převzít
funkci	funkce	k1gFnSc4	funkce
oblastního	oblastní	k2eAgMnSc2d1	oblastní
vyššího	vysoký	k2eAgMnSc2d2	vyšší
velitele	velitel	k1gMnSc2	velitel
jezdectva	jezdectvo	k1gNnSc2	jezdectvo
(	(	kIx(	(
<g/>
Höherer	Höherra	k1gFnPc2	Höherra
Kavallerieoffizier	Kavallerieoffiziero	k1gNnPc2	Kavallerieoffiziero
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
o	o	k7c4	o
pár	pár	k4xCyI	pár
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
dočkal	dočkat	k5eAaPmAgMnS	dočkat
generálských	generálský	k2eAgFnPc2d1	generálská
výložek	výložka	k1gFnPc2	výložka
<g/>
.	.	kIx.	.
</s>
<s>
Stumme	Stumit	k5eAaPmRp1nP	Stumit
byl	být	k5eAaImAgInS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
generálmajora	generálmajor	k1gMnSc4	generálmajor
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpnu	srpen	k1gInSc6	srpen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
počátku	počátek	k1gInSc3	počátek
dubna	duben	k1gInSc2	duben
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
povýšen	povýšit	k5eAaPmNgInS	povýšit
do	do	k7c2	do
hodnosti	hodnost	k1gFnSc2	hodnost
generálporučíka	generálporučík	k1gMnSc2	generálporučík
a	a	k8xC	a
k	k	k7c3	k
počátku	počátek	k1gInSc3	počátek
listopadu	listopad	k1gInSc2	listopad
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
mu	on	k3xPp3gNnSc3	on
bylo	být	k5eAaImAgNnS	být
svěřeno	svěřit	k5eAaPmNgNnS	svěřit
velení	velení	k1gNnSc1	velení
nově	nově	k6eAd1	nově
zformované	zformovaný	k2eAgNnSc4d1	zformované
2	[number]	k4	2
<g/>
.	.	kIx.	.
lehké	lehký	k2eAgFnSc2d1	lehká
divize	divize	k1gFnSc2	divize
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Leichte-Division	Leichte-Division	k1gInSc1	Leichte-Division
<g/>
)	)	kIx)	)
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Gera	Gera	k1gMnSc1	Gera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
2	[number]	k4	2
<g/>
.	.	kIx.	.
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Této	tento	k3xDgFnSc3	tento
divizi	divize	k1gFnSc3	divize
velel	velet	k5eAaImAgInS	velet
i	i	k9	i
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
Polsko	Polsko	k1gNnSc4	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
velícím	velící	k2eAgMnSc7d1	velící
generálem	generál	k1gMnSc7	generál
XXXX	XXXX	kA	XXXX
<g/>
.	.	kIx.	.
</s>
<s>
Armeekorpsu	Armeekorps	k1gMnSc3	Armeekorps
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
velel	velet	k5eAaImAgInS	velet
při	při	k7c6	při
válečných	válečný	k2eAgFnPc6d1	válečná
operacích	operace	k1gFnPc6	operace
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
při	při	k7c6	při
balkánském	balkánský	k2eAgNnSc6d1	balkánské
tažení	tažení	k1gNnSc6	tažení
a	a	k8xC	a
válce	válka	k1gFnSc6	válka
proti	proti	k7c3	proti
Světskému	světský	k2eAgInSc3d1	světský
svazu	svaz	k1gInSc3	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
jednotka	jednotka	k1gFnSc1	jednotka
motorizována	motorizován	k2eAgFnSc1d1	motorizována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
operaci	operace	k1gFnSc6	operace
Barbarossa	Barbarossa	k1gMnSc1	Barbarossa
Stummemu	Stummema	k1gFnSc4	Stummema
velel	velet	k5eAaImAgMnS	velet
polní	polní	k2eAgMnSc1d1	polní
maršál	maršál	k1gMnSc1	maršál
Fedor	Fedor	k1gMnSc1	Fedor
von	von	k1gInSc4	von
Bock	Bocka	k1gFnPc2	Bocka
<g/>
.	.	kIx.	.
</s>
<s>
Stumme	Stumit	k5eAaPmRp1nP	Stumit
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgMnPc7	svůj
vojáky	voják	k1gMnPc7	voják
obsadil	obsadit	k5eAaPmAgMnS	obsadit
Možajsk	Možajsk	k1gInSc4	Možajsk
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c4	na
operaci	operace	k1gFnSc4	operace
Blau	Blaus	k1gInSc2	Blaus
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
výsledkem	výsledek	k1gInSc7	výsledek
byl	být	k5eAaImAgInS	být
postup	postup	k1gInSc4	postup
Paulusovy	Paulusův	k2eAgFnSc2d1	Paulusova
6	[number]	k4	6
<g/>
.	.	kIx.	.
armády	armáda	k1gFnSc2	armáda
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
reorganizovaným	reorganizovaný	k2eAgInSc7d1	reorganizovaný
XXXX	XXXX	kA	XXXX
<g/>
.	.	kIx.	.
sborem	sborem	k6eAd1	sborem
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1942	[number]	k4	1942
byly	být	k5eAaImAgInP	být
některé	některý	k3yIgInPc1	některý
německé	německý	k2eAgInPc1d1	německý
plány	plán	k1gInPc1	plán
zachyceny	zachycen	k2eAgMnPc4d1	zachycen
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
obvinil	obvinit	k5eAaPmAgMnS	obvinit
Stummeho	Stumme	k1gMnSc4	Stumme
a	a	k8xC	a
nařídil	nařídit	k5eAaPmAgMnS	nařídit
jej	on	k3xPp3gMnSc4	on
postavit	postavit	k5eAaPmF	postavit
před	před	k7c4	před
vojenský	vojenský	k2eAgInSc4d1	vojenský
soud	soud	k1gInSc4	soud
<g/>
.	.	kIx.	.
</s>
<s>
Velení	velení	k1gNnSc1	velení
byl	být	k5eAaImAgMnS	být
zbaven	zbaven	k2eAgMnSc1d1	zbaven
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
shledán	shledat	k5eAaPmNgInS	shledat
vinným	vinný	k1gMnSc7	vinný
a	a	k8xC	a
odsouzen	odsouzet	k5eAaImNgInS	odsouzet
k	k	k7c3	k
pěti	pět	k4xCc3	pět
letům	let	k1gInPc3	let
vězení	vězení	k1gNnSc2	vězení
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Bock	Bock	k1gMnSc1	Bock
zařídil	zařídit	k5eAaPmAgMnS	zařídit
jeho	jeho	k3xOp3gNnSc4	jeho
propuštění	propuštění	k1gNnSc4	propuštění
<g/>
.	.	kIx.	.
</s>
<s>
Ulrich	Ulrich	k1gMnSc1	Ulrich
von	von	k1gInSc4	von
Hassell	Hassella	k1gFnPc2	Hassella
to	ten	k3xDgNnSc4	ten
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
deníku	deník	k1gInSc6	deník
nazval	nazvat	k5eAaBmAgMnS	nazvat
"	"	kIx"	"
<g/>
groteskní	groteskní	k2eAgFnSc7d1	groteskní
hrou	hra	k1gFnSc7	hra
s	s	k7c7	s
cínovými	cínový	k2eAgMnPc7d1	cínový
vojáčky	vojáček	k1gMnPc7	vojáček
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Hitler	Hitler	k1gMnSc1	Hitler
hraje	hrát	k5eAaImIp3nS	hrát
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
generály	generál	k1gMnPc7	generál
<g/>
"	"	kIx"	"
a	a	k8xC	a
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Stumme	Stumme	k1gMnSc1	Stumme
<g/>
,	,	kIx,	,
velící	velící	k2eAgMnSc1d1	velící
generál	generál	k1gMnSc1	generál
tankových	tankový	k2eAgFnPc2d1	tanková
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
odsouzen	odsoudit	k5eAaPmNgMnS	odsoudit
k	k	k7c3	k
pěti	pět	k4xCc3	pět
letům	léto	k1gNnPc3	léto
[	[	kIx(	[
<g/>
za	za	k7c2	za
akce	akce	k1gFnSc2	akce
divizního	divizní	k2eAgMnSc2d1	divizní
štábního	štábní	k2eAgMnSc2d1	štábní
důstojníka	důstojník	k1gMnSc2	důstojník
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Okamžitě	okamžitě	k6eAd1	okamžitě
byl	být	k5eAaImAgMnS	být
omilostněn	omilostnit	k5eAaPmNgMnS	omilostnit
<g/>
,	,	kIx,	,
od	od	k7c2	od
Göringa	Göring	k1gMnSc2	Göring
obdržel	obdržet	k5eAaPmAgMnS	obdržet
příslib	příslib	k1gInSc4	příslib
nové	nový	k2eAgFnSc2d1	nová
velitelské	velitelský	k2eAgFnSc2d1	velitelská
funkce	funkce	k1gFnSc2	funkce
a	a	k8xC	a
teď	teď	k6eAd1	teď
je	být	k5eAaImIp3nS	být
poslán	poslat	k5eAaPmNgMnS	poslat
do	do	k7c2	do
Afriky	Afrika	k1gFnSc2	Afrika
náhradou	náhrada	k1gFnSc7	náhrada
za	za	k7c2	za
Rommela	Rommel	k1gMnSc2	Rommel
<g/>
.	.	kIx.	.
</s>
<s>
Nevojácká	vojácký	k2eNgFnSc1d1	vojácký
<g/>
,	,	kIx,	,
nepruská	pruský	k2eNgFnSc1d1	pruský
fraška	fraška	k1gFnSc1	fraška
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Stumme	Stumme	k1gMnPc3	Stumme
přibyl	přibýt	k5eAaPmAgInS	přibýt
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
k	k	k7c3	k
Afrikakorpsu	Afrikakorps	k1gInSc3	Afrikakorps
v	v	k7c6	v
září	září	k1gNnSc6	září
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
musel	muset	k5eAaImAgMnS	muset
čelit	čelit	k5eAaImF	čelit
Britům	Brit	k1gMnPc3	Brit
u	u	k7c2	u
El	Ela	k1gFnPc2	Ela
Alameinu	Alamein	k1gInSc2	Alamein
<g/>
.	.	kIx.	.
</s>
<s>
Erwin	Erwin	k1gMnSc1	Erwin
Rommel	Rommel	k1gMnSc1	Rommel
byl	být	k5eAaImAgMnS	být
kvůli	kvůli	k7c3	kvůli
nemoci	nemoc	k1gFnSc3	nemoc
a	a	k8xC	a
vyčerpání	vyčerpání	k1gNnSc6	vyčerpání
uvolněn	uvolnit	k5eAaPmNgInS	uvolnit
z	z	k7c2	z
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Stumme	Stumit	k5eAaPmRp1nP	Stumit
přijel	přijet	k5eAaPmAgInS	přijet
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
,	,	kIx,	,
jen	jen	k9	jen
pár	pár	k4xCyI	pár
dní	den	k1gInPc2	den
před	před	k7c7	před
Rommelovým	Rommelův	k2eAgInSc7d1	Rommelův
odjezdem	odjezd	k1gInSc7	odjezd
<g/>
.	.	kIx.	.
</s>
<s>
Převzal	převzít	k5eAaPmAgInS	převzít
velení	velení	k1gNnSc4	velení
Tankové	tankový	k2eAgFnSc2d1	tanková
armády	armáda	k1gFnSc2	armáda
Afrika	Afrika	k1gFnSc1	Afrika
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
smíšených	smíšený	k2eAgFnPc2d1	smíšená
italských	italský	k2eAgFnPc2d1	italská
a	a	k8xC	a
německých	německý	k2eAgFnPc2d1	německá
sil	síla	k1gFnPc2	síla
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
von	von	k1gInSc4	von
Thoma	Thomum	k1gNnSc2	Thomum
nahradil	nahradit	k5eAaPmAgMnS	nahradit
zraněného	zraněný	k2eAgMnSc4d1	zraněný
Walthera	Walther	k1gMnSc4	Walther
Nehringa	Nehring	k1gMnSc4	Nehring
ve	v	k7c6	v
velení	velení	k1gNnSc6	velení
Afrikakorpsu	Afrikakorps	k1gInSc2	Afrikakorps
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
El	Ela	k1gFnPc2	Ela
Alameinu	Alamein	k1gInSc2	Alamein
a	a	k8xC	a
smrt	smrt	k1gFnSc4	smrt
===	===	k?	===
</s>
</p>
<p>
<s>
Stumme	Stumit	k5eAaPmRp1nP	Stumit
"	"	kIx"	"
<g/>
s	s	k7c7	s
důvěrou	důvěra	k1gFnSc7	důvěra
prováděl	provádět	k5eAaImAgInS	provádět
plán	plán	k1gInSc1	plán
vypracovaný	vypracovaný	k2eAgInSc1d1	vypracovaný
Rommelem	Rommel	k1gMnSc7	Rommel
<g/>
"	"	kIx"	"
pro	pro	k7c4	pro
případ	případ	k1gInSc4	případ
očekávaného	očekávaný	k2eAgInSc2d1	očekávaný
útoku	útok	k1gInSc2	útok
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zpráv	zpráva	k1gFnPc2	zpráva
nadřízeným	nadřízená	k1gFnPc3	nadřízená
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebyl	být	k5eNaImAgMnS	být
příliš	příliš	k6eAd1	příliš
optimistický	optimistický	k2eAgMnSc1d1	optimistický
a	a	k8xC	a
shodoval	shodovat	k5eAaImAgMnS	shodovat
se	se	k3xPyFc4	se
s	s	k7c7	s
Rommelem	Rommel	k1gInSc7	Rommel
<g/>
,	,	kIx,	,
že	že	k8xS	že
jediná	jediný	k2eAgFnSc1d1	jediná
vyhlídka	vyhlídka	k1gFnSc1	vyhlídka
na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
zadrží	zadržet	k5eAaPmIp3nS	zadržet
nepřátelské	přátelský	k2eNgInPc4d1	nepřátelský
útoky	útok	k1gInPc4	útok
<g/>
;	;	kIx,	;
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
však	však	k9	však
neměl	mít	k5eNaImAgMnS	mít
dostatečné	dostatečný	k2eAgFnPc4d1	dostatečná
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
šest	šest	k4xCc4	šest
týdnů	týden	k1gInPc2	týden
od	od	k7c2	od
jeho	jeho	k3xOp3gInSc2	jeho
příjezdu	příjezd	k1gInSc2	příjezd
zahájili	zahájit	k5eAaPmAgMnP	zahájit
Britové	Brit	k1gMnPc1	Brit
masivní	masivní	k2eAgNnPc4d1	masivní
ostřelování	ostřelování	k1gNnPc4	ostřelování
<g/>
.	.	kIx.	.
</s>
<s>
Stumme	Stumit	k5eAaPmRp1nP	Stumit
zakázal	zakázat	k5eAaPmAgInS	zakázat
užití	užití	k1gNnSc4	užití
dělostřelecké	dělostřelecký	k2eAgFnSc2d1	dělostřelecká
munice	munice	k1gFnSc2	munice
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
britské	britský	k2eAgInPc4d1	britský
nástupní	nástupní	k2eAgInPc4d1	nástupní
prostory	prostor	k1gInPc4	prostor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
nepřítel	nepřítel	k1gMnSc1	nepřítel
nejzranitelnější	zranitelný	k2eAgMnSc1d3	nejzranitelnější
<g/>
,	,	kIx,	,
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
přednost	přednost	k1gFnSc4	přednost
držet	držet	k5eAaImF	držet
omezené	omezený	k2eAgInPc4d1	omezený
prostředky	prostředek	k1gInPc4	prostředek
v	v	k7c6	v
rezervě	rezerva	k1gFnSc6	rezerva
<g/>
.	.	kIx.	.
</s>
<s>
Reinhard	Reinhard	k1gMnSc1	Reinhard
Stumpf	Stumpf	k1gMnSc1	Stumpf
to	ten	k3xDgNnSc4	ten
popsal	popsat	k5eAaPmAgMnS	popsat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
hrubou	hrubý	k2eAgFnSc4d1	hrubá
chybu	chyba	k1gFnSc4	chyba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
Britům	Brit	k1gMnPc3	Brit
umožnila	umožnit	k5eAaPmAgFnS	umožnit
v	v	k7c6	v
relativním	relativní	k2eAgInSc6d1	relativní
poklidu	poklid	k1gInSc6	poklid
zformovat	zformovat	k5eAaPmF	zformovat
útok	útok	k1gInSc4	útok
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Rommela	Rommel	k1gMnSc2	Rommel
<g/>
,	,	kIx,	,
Stumme	Stumme	k1gFnSc7	Stumme
se	se	k3xPyFc4	se
přesouval	přesouvat	k5eAaImAgInS	přesouvat
bez	bez	k7c2	bez
ochranného	ochranný	k2eAgInSc2d1	ochranný
doprovodu	doprovod	k1gInSc2	doprovod
a	a	k8xC	a
bez	bez	k7c2	bez
radiového	radiový	k2eAgInSc2d1	radiový
vozu	vůz	k1gInSc2	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Stumme	Stumit	k5eAaPmRp1nP	Stumit
a	a	k8xC	a
velitel	velitel	k1gMnSc1	velitel
spojařů	spojař	k1gMnPc2	spojař
plukovník	plukovník	k1gMnSc1	plukovník
Andreas	Andreas	k1gMnSc1	Andreas
Büchting	Büchting	k1gInSc4	Büchting
se	s	k7c7	s
24	[number]	k4	24
října	říjen	k1gInSc2	říjen
přesunovali	přesunovat	k5eAaImAgMnP	přesunovat
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
obhlédli	obhlédnout	k5eAaPmAgMnP	obhlédnout
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
velitelskému	velitelský	k2eAgNnSc3d1	velitelské
stanovišti	stanoviště	k1gNnSc3	stanoviště
se	se	k3xPyFc4	se
automobil	automobil	k1gInSc1	automobil
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
pod	pod	k7c7	pod
útokem	útok	k1gInSc7	útok
v	v	k7c6	v
otevřené	otevřený	k2eAgFnSc6d1	otevřená
krajině	krajina	k1gFnSc6	krajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Büchting	Büchting	k1gInSc1	Büchting
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
mrtev	mrtev	k2eAgMnSc1d1	mrtev
po	po	k7c6	po
zásahu	zásah	k1gInSc6	zásah
do	do	k7c2	do
hlavy	hlava	k1gFnSc2	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Stumme	Stumit	k5eAaPmRp1nP	Stumit
vyskočil	vyskočit	k5eAaPmAgInS	vyskočit
a	a	k8xC	a
držel	držet	k5eAaImAgInS	držet
se	se	k3xPyFc4	se
kraje	kraj	k1gInSc2	kraj
auta	auto	k1gNnSc2	auto
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
řidič	řidič	k1gMnSc1	řidič
nezmizí	zmizet	k5eNaPmIp3nS	zmizet
z	z	k7c2	z
dostřelu	dostřel	k1gInSc2	dostřel
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
den	den	k1gInSc1	den
byl	být	k5eAaImAgInS	být
bez	bez	k7c2	bez
zjevných	zjevný	k2eAgNnPc2d1	zjevné
zranění	zranění	k1gNnPc2	zranění
nalezen	nalézt	k5eAaBmNgInS	nalézt
mrtev	mrtev	k2eAgInSc1d1	mrtev
vedle	vedle	k7c2	vedle
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Vědělo	vědět	k5eAaImAgNnS	vědět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
vysoký	vysoký	k2eAgInSc4d1	vysoký
krevní	krevní	k2eAgInSc4d1	krevní
tlak	tlak	k1gInSc4	tlak
a	a	k8xC	a
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
srdeční	srdeční	k2eAgInSc4d1	srdeční
infarkt	infarkt	k1gInSc4	infarkt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
velitele	velitel	k1gMnSc2	velitel
tankové	tankový	k2eAgFnSc2d1	tanková
armády	armáda	k1gFnSc2	armáda
Afrika	Afrika	k1gFnSc1	Afrika
jej	on	k3xPp3gNnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
navrátivší	navrátivší	k2eAgMnSc1d1	navrátivší
se	se	k3xPyFc4	se
Erwin	Erwin	k1gMnSc1	Erwin
Rommel	Rommel	k1gMnSc1	Rommel
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Afrika	Afrika	k1gFnSc1	Afrika
Korpsu	Korps	k1gInSc2	Korps
velel	velet	k5eAaImAgMnS	velet
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
von	von	k1gInSc4	von
Thoma	Thomum	k1gNnSc2	Thomum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Povýšení	povýšení	k1gNnSc1	povýšení
a	a	k8xC	a
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Data	datum	k1gNnSc2	datum
povýšení	povýšení	k1gNnSc2	povýšení
===	===	k?	===
</s>
</p>
<p>
<s>
Fahnenjunker	Fahnenjunker	k1gInSc1	Fahnenjunker
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1906	[number]	k4	1906
</s>
</p>
<p>
<s>
Leutnant	Leutnant	k1gInSc1	Leutnant
–	–	k?	–
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1907	[number]	k4	1907
</s>
</p>
<p>
<s>
Oberleutnant	Oberleutnant	k1gMnSc1	Oberleutnant
–	–	k?	–
?	?	kIx.	?
</s>
</p>
<p>
<s>
Hauptmann	Hauptmann	k1gInSc1	Hauptmann
–	–	k?	–
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1915	[number]	k4	1915
</s>
</p>
<p>
<s>
Major	major	k1gMnSc1	major
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1926	[number]	k4	1926
</s>
</p>
<p>
<s>
Oberstleutnant	Oberstleutnant	k1gInSc1	Oberstleutnant
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1931	[number]	k4	1931
</s>
</p>
<p>
<s>
Oberst	Oberst	k?	Oberst
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1933	[number]	k4	1933
</s>
</p>
<p>
<s>
Generalmajor	Generalmajor	k1gInSc1	Generalmajor
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1936	[number]	k4	1936
</s>
</p>
<p>
<s>
Generalleutnant	Generalleutnant	k1gInSc1	Generalleutnant
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1938	[number]	k4	1938
</s>
</p>
<p>
<s>
General	Generat	k5eAaPmAgMnS	Generat
der	drát	k5eAaImRp2nS	drát
Kavallerie	Kavallerie	k1gFnSc2	Kavallerie
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1940	[number]	k4	1940
</s>
</p>
<p>
<s>
Přejmenován	přejmenován	k2eAgMnSc1d1	přejmenován
na	na	k7c6	na
General	General	k1gFnSc6	General
der	drát	k5eAaImRp2nS	drát
Panzertruppe	Panzertrupp	k1gInSc5	Panzertrupp
–	–	k?	–
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1941	[number]	k4	1941
</s>
</p>
<p>
<s>
===	===	k?	===
Vyznamenání	vyznamenání	k1gNnSc2	vyznamenání
===	===	k?	===
</s>
</p>
<p>
<s>
Rytířský	rytířský	k2eAgInSc1d1	rytířský
kříž	kříž	k1gInSc1	kříž
železného	železný	k2eAgInSc2d1	železný
kříže	kříž	k1gInSc2	kříž
–	–	k?	–
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1940	[number]	k4	1940
</s>
</p>
<p>
<s>
Spona	spona	k1gFnSc1	spona
k	k	k7c3	k
pruskému	pruský	k2eAgInSc3d1	pruský
železnému	železný	k2eAgInSc3d1	železný
kříži	kříž	k1gInSc3	kříž
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
</s>
</p>
<p>
<s>
Spona	spona	k1gFnSc1	spona
k	k	k7c3	k
pruskému	pruský	k2eAgInSc3d1	pruský
železnému	železný	k2eAgInSc3d1	železný
kříži	kříž	k1gInSc3	kříž
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
</s>
</p>
<p>
<s>
Pruský	pruský	k2eAgInSc1d1	pruský
železný	železný	k2eAgInSc1d1	železný
kříž	kříž	k1gInSc1	kříž
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pruský	pruský	k2eAgInSc1d1	pruský
železný	železný	k2eAgInSc1d1	železný
kříž	kříž	k1gInSc1	kříž
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnPc1	třída
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vévodský	vévodský	k2eAgInSc1d1	vévodský
Sasko-Meiningenský	Sasko-Meiningenský	k2eAgInSc1d1	Sasko-Meiningenský
čestný	čestný	k2eAgInSc1d1	čestný
kříž	kříž	k1gInSc1	kříž
za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Odznak	odznak	k1gInSc1	odznak
za	za	k7c4	za
zranění	zranění	k1gNnSc4	zranění
v	v	k7c6	v
černém	černé	k1gNnSc6	černé
(	(	kIx(	(
<g/>
první	první	k4xOgFnSc1	první
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Baltský	baltský	k2eAgInSc1d1	baltský
kříž	kříž	k1gInSc1	kříž
Baltický	baltický	k2eAgInSc1d1	baltický
kříž	kříž	k1gInSc1	kříž
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
</s>
</p>
<p>
<s>
Baltický	baltický	k2eAgInSc1d1	baltický
kříž	kříž	k1gInSc1	kříž
II	II	kA	II
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
</s>
</p>
<p>
<s>
Kříž	Kříž	k1gMnSc1	Kříž
cti	čest	k1gFnSc2	čest
</s>
</p>
<p>
<s>
|	|	kIx~	|
|	|	kIx~	|
|	|	kIx~	|
Služební	služební	k2eAgNnSc1d1	služební
vyznamenání	vyznamenání	k1gNnSc1	vyznamenání
wehrmachtu	wehrmacht	k1gInSc2	wehrmacht
od	od	k7c2	od
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
až	až	k9	až
do	do	k7c2	do
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Životopis	životopis	k1gInSc1	životopis
v	v	k7c6	v
datech	datum	k1gNnPc6	datum
</s>
</p>
<p>
<s>
životopis	životopis	k1gInSc1	životopis
v	v	k7c6	v
datech	datum	k1gNnPc6	datum
</s>
</p>
