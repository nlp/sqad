<p>
<s>
Jarní	jarní	k2eAgInSc1d1	jarní
bod	bod	k1gInSc1	bod
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
astronomii	astronomie	k1gFnSc4	astronomie
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
průsečíků	průsečík	k1gInPc2	průsečík
ekliptiky	ekliptika	k1gFnSc2	ekliptika
se	se	k3xPyFc4	se
nebeským	nebeský	k2eAgInSc7d1	nebeský
rovníkem	rovník	k1gInSc7	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
bod	bod	k1gInSc4	bod
na	na	k7c6	na
nebeské	nebeský	k2eAgFnSc6d1	nebeská
sféře	sféra	k1gFnSc6	sféra
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
Slunce	slunce	k1gNnSc2	slunce
v	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
jarní	jarní	k2eAgFnSc2d1	jarní
rovnodennosti	rovnodennost	k1gFnSc2	rovnodennost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jarní	jarní	k2eAgInSc1d1	jarní
bod	bod	k1gInSc1	bod
je	být	k5eAaImIp3nS	být
počátečním	počáteční	k2eAgInSc7d1	počáteční
bodem	bod	k1gInSc7	bod
pro	pro	k7c4	pro
rovníkové	rovníkový	k2eAgFnPc4d1	Rovníková
souřadnice	souřadnice	k1gFnPc4	souřadnice
používané	používaný	k2eAgFnPc4d1	používaná
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
polohy	poloha	k1gFnSc2	poloha
těles	těleso	k1gNnPc2	těleso
na	na	k7c6	na
nebi	nebe	k1gNnSc6	nebe
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
rovníkové	rovníkový	k2eAgFnPc1d1	Rovníková
souřadnice	souřadnice	k1gFnPc1	souřadnice
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
α	α	k?	α
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
°	°	k?	°
(	(	kIx(	(
<g/>
rektascenze	rektascenze	k1gFnSc1	rektascenze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
δ	δ	k?	δ
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
°	°	k?	°
(	(	kIx(	(
<g/>
deklinace	deklinace	k1gFnSc1	deklinace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Ryb	Ryby	k1gFnPc2	Ryby
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
znakem	znak	k1gInSc7	znak
souhvězdí	souhvězdí	k1gNnPc2	souhvězdí
Berana	Beran	k1gMnSc2	Beran
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
dvěma	dva	k4xCgInPc7	dva
tisíci	tisíc	k4xCgInPc7	tisíc
lety	léto	k1gNnPc7	léto
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
nacházel	nacházet	k5eAaImAgMnS	nacházet
v	v	k7c6	v
souhvězdí	souhvězdí	k1gNnSc6	souhvězdí
Berana	Beran	k1gMnSc2	Beran
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
precese	precese	k1gFnSc2	precese
zemské	zemský	k2eAgFnSc2d1	zemská
osy	osa	k1gFnSc2	osa
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
souhvězdí	souhvězdí	k1gNnSc2	souhvězdí
Ryb	Ryby	k1gFnPc2	Ryby
<g/>
,	,	kIx,	,
a	a	k8xC	a
takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
bude	být	k5eAaImBp3nS	být
posouvat	posouvat	k5eAaImF	posouvat
i	i	k9	i
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protějškem	protějšek	k1gInSc7	protějšek
je	být	k5eAaImIp3nS	být
podzimní	podzimní	k2eAgInSc4d1	podzimní
bod	bod	k1gInSc4	bod
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
se	se	k3xPyFc4	se
Slunce	slunce	k1gNnSc1	slunce
nachází	nacházet	k5eAaImIp3nS	nacházet
o	o	k7c6	o
podzimní	podzimní	k2eAgFnSc6d1	podzimní
rovnodennosti	rovnodennost	k1gFnSc6	rovnodennost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Rovnodennost	rovnodennost	k1gFnSc1	rovnodennost
</s>
</p>
<p>
<s>
Nebeská	nebeský	k2eAgFnSc1d1	nebeská
sféra	sféra	k1gFnSc1	sféra
</s>
</p>
<p>
<s>
Slunovrat	slunovrat	k1gInSc1	slunovrat
</s>
</p>
