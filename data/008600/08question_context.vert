<s>
Třicetiletá	třicetiletý	k2eAgFnSc1d1
válka	válka	k1gFnSc1
(	(	kIx(
<g/>
1618	[number]	k4
<g/>
–	–	k?
<g/>
1648	[number]	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
evropský	evropský	k2eAgInSc1d1
ozbrojený	ozbrojený	k2eAgInSc1d1
konflikt	konflikt	k1gInSc1
známý	známý	k2eAgInSc1d1
především	především	k9
jako	jako	k9
vyvrcholení	vyvrcholení	k1gNnSc1
sporů	spor	k1gInPc2
mezi	mezi	k7c7
římskokatolickou	římskokatolický	k2eAgFnSc7d1
církví	církev	k1gFnSc7
a	a	k8xC
zastánci	zastánce	k1gMnPc1
vyznání	vyznání	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
po	po	k7c4
reformaci	reformace	k1gFnSc4
v	v	k7c6
16	[number]	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
kalvinismem	kalvinismus	k1gInSc7
a	a	k8xC
luteránstvím	luteránství	k1gNnSc7
<g/>
.	.	kIx.
</s>