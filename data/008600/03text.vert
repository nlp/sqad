<p>
<s>
Třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
1618	[number]	k4	1618
<g/>
–	–	k?	–
<g/>
1648	[number]	k4	1648
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
evropský	evropský	k2eAgInSc1d1	evropský
ozbrojený	ozbrojený	k2eAgInSc1d1	ozbrojený
konflikt	konflikt	k1gInSc1	konflikt
známý	známý	k2eAgInSc1d1	známý
především	především	k9	především
jako	jako	k9	jako
vyvrcholení	vyvrcholení	k1gNnSc1	vyvrcholení
sporů	spor	k1gInPc2	spor
mezi	mezi	k7c7	mezi
římskokatolickou	římskokatolický	k2eAgFnSc7d1	Římskokatolická
církví	církev	k1gFnSc7	církev
a	a	k8xC	a
zastánci	zastánce	k1gMnPc1	zastánce
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
po	po	k7c4	po
reformaci	reformace	k1gFnSc4	reformace
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
kalvinismem	kalvinismus	k1gInSc7	kalvinismus
a	a	k8xC	a
luteránstvím	luteránství	k1gNnSc7	luteránství
<g/>
.	.	kIx.	.
</s>
<s>
Neméně	málo	k6eNd2	málo
důležitou	důležitý	k2eAgFnSc7d1	důležitá
příčinou	příčina	k1gFnSc7	příčina
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
také	také	k9	také
boj	boj	k1gInSc1	boj
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
o	o	k7c4	o
politickou	politický	k2eAgFnSc4d1	politická
nadvládu	nadvláda	k1gFnSc4	nadvláda
<g/>
.	.	kIx.	.
<g/>
Vlastní	vlastní	k2eAgFnSc4d1	vlastní
válku	válka	k1gFnSc4	válka
započala	započnout	k5eAaPmAgFnS	započnout
revoluce	revoluce	k1gFnSc1	revoluce
stavů	stav	k1gInPc2	stav
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
proti	proti	k7c3	proti
panovníkovi	panovník	k1gMnSc3	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
mocenský	mocenský	k2eAgInSc1d1	mocenský
souboj	souboj	k1gInSc1	souboj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
již	již	k9	již
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
probíhal	probíhat	k5eAaImAgInS	probíhat
mezi	mezi	k7c7	mezi
Nizozemskými	nizozemský	k2eAgFnPc7d1	nizozemská
provinciemi	provincie	k1gFnPc7	provincie
a	a	k8xC	a
Španělskem	Španělsko	k1gNnSc7	Španělsko
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xS	jako
nizozemská	nizozemský	k2eAgFnSc1d1	nizozemská
revoluce	revoluce	k1gFnSc1	revoluce
<g/>
,	,	kIx,	,
také	také	k9	také
výrazně	výrazně	k6eAd1	výrazně
ovlivňoval	ovlivňovat	k5eAaImAgMnS	ovlivňovat
boje	boj	k1gInSc2	boj
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
konečně	konečně	k6eAd1	konečně
vstup	vstup	k1gInSc4	vstup
katolické	katolický	k2eAgFnSc2d1	katolická
Francie	Francie	k1gFnSc2	Francie
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
protihabsburské	protihabsburský	k2eAgFnSc2d1	protihabsburská
koalice	koalice	k1gFnSc2	koalice
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
eliminace	eliminace	k1gFnSc2	eliminace
přílišné	přílišný	k2eAgFnSc2d1	přílišná
moci	moc	k1gFnSc2	moc
Habsburků	Habsburk	k1gMnPc2	Habsburk
podtrhl	podtrhnout	k5eAaPmAgMnS	podtrhnout
mocenské	mocenský	k2eAgInPc4d1	mocenský
zájmy	zájem	k1gInPc4	zájem
na	na	k7c4	na
pozadí	pozadí	k1gNnSc4	pozadí
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInPc1	první
roky	rok	k1gInPc1	rok
války	válka	k1gFnSc2	válka
probíhaly	probíhat	k5eAaImAgInP	probíhat
především	především	k6eAd1	především
na	na	k7c6	na
území	území	k1gNnSc6	území
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
a	a	k8xC	a
Rakouského	rakouský	k2eAgNnSc2d1	rakouské
arcivévodství	arcivévodství	k1gNnSc2	arcivévodství
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
totiž	totiž	k9	totiž
revoluce	revoluce	k1gFnPc1	revoluce
proti	proti	k7c3	proti
Habsburkům	Habsburk	k1gMnPc3	Habsburk
vypukly	vypuknout	k5eAaPmAgFnP	vypuknout
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
na	na	k7c4	na
většinu	většina	k1gFnSc4	většina
dalších	další	k2eAgNnPc2d1	další
let	léto	k1gNnPc2	léto
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
boje	boj	k1gInPc1	boj
přemístily	přemístit	k5eAaPmAgInP	přemístit
mimo	mimo	k7c4	mimo
tato	tento	k3xDgNnPc4	tento
území	území	k1gNnPc4	území
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
katolíkům	katolík	k1gMnPc3	katolík
podařilo	podařit	k5eAaPmAgNnS	podařit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
bitev	bitva	k1gFnPc2	bitva
vyhrát	vyhrát	k5eAaPmF	vyhrát
a	a	k8xC	a
přesunout	přesunout	k5eAaPmF	přesunout
boje	boj	k1gInPc4	boj
na	na	k7c4	na
zbytek	zbytek	k1gInSc4	zbytek
území	území	k1gNnSc2	území
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnPc4d1	římská
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
ovládali	ovládat	k5eAaImAgMnP	ovládat
protestanti	protestant	k1gMnPc1	protestant
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vleklá	vleklý	k2eAgFnSc1d1	vleklá
a	a	k8xC	a
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
válka	válka	k1gFnSc1	válka
způsobila	způsobit	k5eAaPmAgFnS	způsobit
značný	značný	k2eAgInSc4d1	značný
úbytek	úbytek	k1gInSc4	úbytek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
na	na	k7c6	na
zasažených	zasažený	k2eAgNnPc6d1	zasažené
územích	území	k1gNnPc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
o	o	k7c4	o
třicet	třicet	k4xCc4	třicet
procent	procento	k1gNnPc2	procento
<g/>
,	,	kIx,	,
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
až	až	k9	až
o	o	k7c4	o
padesát	padesát	k4xCc4	padesát
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
byly	být	k5eAaImAgFnP	být
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
války	válka	k1gFnSc2	válka
před	před	k7c7	před
největšími	veliký	k2eAgFnPc7d3	veliký
válečnými	válečný	k2eAgFnPc7d1	válečná
útrapami	útrapa	k1gFnPc7	útrapa
uchráněny	uchráněn	k2eAgFnPc1d1	uchráněna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
i	i	k9	i
zde	zde	k6eAd1	zde
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
třetinu	třetina	k1gFnSc4	třetina
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
uzavřením	uzavření	k1gNnSc7	uzavření
vestfálského	vestfálský	k2eAgInSc2d1	vestfálský
míru	mír	k1gInSc2	mír
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1648	[number]	k4	1648
<g/>
;	;	kIx,	;
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
míru	mír	k1gInSc2	mír
nejvíce	nejvíce	k6eAd1	nejvíce
získaly	získat	k5eAaPmAgInP	získat
protestantské	protestantský	k2eAgInPc1d1	protestantský
státy	stát	k1gInPc1	stát
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příčiny	příčina	k1gFnSc2	příčina
války	válka	k1gFnSc2	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
uznání	uznání	k1gNnSc6	uznání
augšpurského	augšpurský	k2eAgInSc2d1	augšpurský
míru	mír	k1gInSc2	mír
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1555	[number]	k4	1555
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
upevňování	upevňování	k1gNnSc3	upevňování
katolicismu	katolicismus	k1gInSc2	katolicismus
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
na	na	k7c6	na
základě	základ	k1gInSc6	základ
páteřní	páteřní	k2eAgFnSc2d1	páteřní
myšlenky	myšlenka	k1gFnSc2	myšlenka
augšpurského	augšpurský	k2eAgInSc2d1	augšpurský
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
Cuius	Cuius	k1gMnSc1	Cuius
regio	regio	k1gMnSc1	regio
<g/>
,	,	kIx,	,
eius	eiusit	k5eAaPmRp2nS	eiusit
religio	religio	k1gMnSc1	religio
(	(	kIx(	(
<g/>
Koho	kdo	k3yRnSc4	kdo
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
toho	ten	k3xDgNnSc2	ten
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Veliký	veliký	k2eAgInSc1d1	veliký
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
pokatoličťování	pokatoličťování	k1gNnSc6	pokatoličťování
měl	mít	k5eAaImAgInS	mít
řeholní	řeholní	k2eAgInSc1d1	řeholní
řád	řád	k1gInSc1	řád
jezuitů	jezuita	k1gMnPc2	jezuita
<g/>
,	,	kIx,	,
potvrzený	potvrzený	k2eAgInSc1d1	potvrzený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1540	[number]	k4	1540
bulou	bula	k1gFnSc7	bula
Regimini	Regimin	k2eAgMnPc1d1	Regimin
militantis	militantis	k1gFnPc4	militantis
ecclesiae	ecclesia	k1gMnSc2	ecclesia
papeže	papež	k1gMnSc2	papež
Pavla	Pavel	k1gMnSc2	Pavel
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
funkce	funkce	k1gFnSc1	funkce
řádu	řád	k1gInSc2	řád
byla	být	k5eAaImAgFnS	být
misijní	misijní	k2eAgFnSc1d1	misijní
činnost	činnost	k1gFnSc1	činnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
pomáhat	pomáhat	k5eAaImF	pomáhat
najít	najít	k5eAaPmF	najít
protestantům	protestant	k1gMnPc3	protestant
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
římskokatolické	římskokatolický	k2eAgFnSc3d1	Římskokatolická
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
<s>
Řád	řád	k1gInSc1	řád
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgInS	angažovat
především	především	k9	především
ve	v	k7c6	v
školství	školství	k1gNnSc6	školství
<g/>
,	,	kIx,	,
vědách	věda	k1gFnPc6	věda
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc6	umění
a	a	k8xC	a
kultuře	kultura	k1gFnSc6	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
ho	on	k3xPp3gInSc4	on
pozval	pozvat	k5eAaPmAgMnS	pozvat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1556	[number]	k4	1556
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
I.	I.	kA	I.
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
pomohl	pomoct	k5eAaPmAgMnS	pomoct
proti	proti	k7c3	proti
sílící	sílící	k2eAgFnSc3d1	sílící
vlně	vlna	k1gFnSc3	vlna
protestantské	protestantský	k2eAgFnSc2d1	protestantská
reformace	reformace	k1gFnSc2	reformace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Ferdinandově	Ferdinandův	k2eAgFnSc6d1	Ferdinandova
smrti	smrt	k1gFnSc6	smrt
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
méně	málo	k6eAd2	málo
nábožensky	nábožensky	k6eAd1	nábožensky
vyhraněný	vyhraněný	k2eAgMnSc1d1	vyhraněný
král	král	k1gMnSc1	král
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
českým	český	k2eAgMnPc3d1	český
stavům	stav	k1gInPc3	stav
slíbil	slíbit	k5eAaPmAgMnS	slíbit
náboženské	náboženský	k2eAgFnSc2d1	náboženská
svobody	svoboda	k1gFnSc2	svoboda
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
České	český	k2eAgFnSc2d1	Česká
konfese	konfese	k1gFnSc2	konfese
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
to	ten	k3xDgNnSc1	ten
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
nástupnické	nástupnický	k2eAgNnSc1d1	nástupnické
právo	právo	k1gNnSc1	právo
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gMnSc4	jeho
syna	syn	k1gMnSc4	syn
Rudolfa	Rudolf	k1gMnSc4	Rudolf
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
jako	jako	k9	jako
panovník	panovník	k1gMnSc1	panovník
slabý	slabý	k2eAgMnSc1d1	slabý
a	a	k8xC	a
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
rozsáhlé	rozsáhlý	k2eAgFnSc2d1	rozsáhlá
náboženské	náboženský	k2eAgFnSc2d1	náboženská
svobody	svoboda	k1gFnSc2	svoboda
pro	pro	k7c4	pro
české	český	k2eAgInPc4d1	český
stavy	stav	k1gInPc4	stav
v	v	k7c6	v
Rudolfově	Rudolfův	k2eAgInSc6d1	Rudolfův
majestátu	majestát	k1gInSc6	majestát
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
habsburský	habsburský	k2eAgInSc1d1	habsburský
ho	on	k3xPp3gMnSc4	on
pak	pak	k6eAd1	pak
záhy	záhy	k6eAd1	záhy
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
za	za	k7c2	za
nesvéprávného	svéprávný	k2eNgMnSc2d1	nesvéprávný
a	a	k8xC	a
sesadil	sesadit	k5eAaPmAgMnS	sesadit
ho	on	k3xPp3gNnSc4	on
z	z	k7c2	z
českého	český	k2eAgInSc2d1	český
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Matyáš	Matyáš	k1gMnSc1	Matyáš
Habsburský	habsburský	k2eAgMnSc1d1	habsburský
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
stabilizovat	stabilizovat	k5eAaBmF	stabilizovat
moc	moc	k6eAd1	moc
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stavové	stavový	k2eAgFnPc1d1	stavová
se	se	k3xPyFc4	se
svých	svůj	k3xOyFgNnPc2	svůj
dříve	dříve	k6eAd2	dříve
stvrzených	stvrzený	k2eAgNnPc2d1	stvrzené
práv	právo	k1gNnPc2	právo
nevzdali	vzdát	k5eNaPmAgMnP	vzdát
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
a	a	k8xC	a
panovníkem	panovník	k1gMnSc7	panovník
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
jistá	jistý	k2eAgFnSc1d1	jistá
nevraživost	nevraživost	k1gFnSc1	nevraživost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1617	[number]	k4	1617
stárnoucí	stárnoucí	k2eAgMnPc1d1	stárnoucí
císař	císař	k1gMnSc1	císař
Matyáš	Matyáš	k1gMnSc1	Matyáš
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
české	český	k2eAgInPc4d1	český
stavy	stav	k1gInPc4	stav
k	k	k7c3	k
volbě	volba	k1gFnSc3	volba
svého	svůj	k3xOyFgMnSc2	svůj
bratrance	bratranec	k1gMnSc2	bratranec
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Štýrského	štýrský	k2eAgMnSc2d1	štýrský
(	(	kIx(	(
<g/>
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgNnSc4d1	české
vládl	vládnout	k5eAaImAgMnS	vládnout
jako	jako	k8xC	jako
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	Tom	k1gMnSc3	Tom
napomohl	napomoct	k5eAaPmAgMnS	napomoct
také	také	k6eAd1	také
Jáchym	Jáchym	k1gMnSc1	Jáchym
Ondřej	Ondřej	k1gMnSc1	Ondřej
Šlik	šlika	k1gFnPc2	šlika
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgInS	nechat
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
podpořil	podpořit	k5eAaPmAgMnS	podpořit
korunovaci	korunovace	k1gFnSc3	korunovace
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
měl	mít	k5eAaImAgInS	mít
původně	původně	k6eAd1	původně
pronést	pronést	k5eAaPmF	pronést
řeč	řeč	k1gFnSc4	řeč
podporující	podporující	k2eAgInSc1d1	podporující
názor	názor	k1gInSc1	názor
stavů	stav	k1gInPc2	stav
o	o	k7c6	o
volitelnosti	volitelnost	k1gFnSc6	volitelnost
králů	král	k1gMnPc2	král
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
byl	být	k5eAaImAgMnS	být
zastáncem	zastánce	k1gMnSc7	zastánce
důsledné	důsledný	k2eAgFnSc2d1	důsledná
rekatolizační	rekatolizační	k2eAgFnSc2d1	rekatolizační
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
české	český	k2eAgInPc1d1	český
stavy	stav	k1gInPc1	stav
nijak	nijak	k6eAd1	nijak
neupokojilo	upokojit	k5eNaPmAgNnS	upokojit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
většinu	většina	k1gFnSc4	většina
stížností	stížnost	k1gFnPc2	stížnost
podávali	podávat	k5eAaImAgMnP	podávat
především	především	k9	především
na	na	k7c4	na
ustanovenou	ustanovený	k2eAgFnSc4d1	ustanovená
místodržitelskou	místodržitelský	k2eAgFnSc4d1	místodržitelská
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Spory	spor	k1gInPc1	spor
o	o	k7c4	o
kompetence	kompetence	k1gFnPc4	kompetence
stavů	stav	k1gInPc2	stav
a	a	k8xC	a
vlády	vláda	k1gFnSc2	vláda
vyvrcholily	vyvrcholit	k5eAaPmAgFnP	vyvrcholit
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1618	[number]	k4	1618
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
z	z	k7c2	z
oken	okno	k1gNnPc2	okno
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
při	při	k7c6	při
pražské	pražský	k2eAgFnSc6d1	Pražská
defenestraci	defenestrace	k1gFnSc6	defenestrace
vyhozeni	vyhozen	k2eAgMnPc1d1	vyhozen
dva	dva	k4xCgMnPc1	dva
místodržící	místodržící	k1gMnPc1	místodržící
Vilém	Vilém	k1gMnSc1	Vilém
Slavata	Slavata	k1gFnSc1	Slavata
z	z	k7c2	z
Chlumu	chlum	k1gInSc2	chlum
a	a	k8xC	a
Košumberka	Košumberka	k1gFnSc1	Košumberka
a	a	k8xC	a
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Bořita	Bořita	k1gMnSc1	Bořita
z	z	k7c2	z
Martinic	Martinice	k1gFnPc2	Martinice
i	i	k8xC	i
jejich	jejich	k3xOp3gFnSc4	jejich
sekretář	sekretář	k1gMnSc1	sekretář
Fabricius	Fabricius	k1gMnSc1	Fabricius
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
obviňováni	obviňovat	k5eAaImNgMnP	obviňovat
ze	z	k7c2	z
špatného	špatný	k2eAgNnSc2d1	špatné
zastupování	zastupování	k1gNnSc2	zastupování
Čechů	Čech	k1gMnPc2	Čech
u	u	k7c2	u
dvora	dvůr	k1gInSc2	dvůr
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mimo	mimo	k7c4	mimo
země	zem	k1gFnPc4	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
===	===	k?	===
</s>
</p>
<p>
<s>
I	i	k9	i
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
byla	být	k5eAaImAgFnS	být
vnitropolitická	vnitropolitický	k2eAgFnSc1d1	vnitropolitická
situace	situace	k1gFnSc1	situace
napjatá	napjatý	k2eAgFnSc1d1	napjatá
a	a	k8xC	a
protestantské	protestantský	k2eAgInPc1d1	protestantský
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1608	[number]	k4	1608
spojily	spojit	k5eAaPmAgFnP	spojit
do	do	k7c2	do
svazku	svazek	k1gInSc2	svazek
Protestantské	protestantský	k2eAgFnSc2d1	protestantská
unie	unie	k1gFnSc2	unie
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Fridricha	Fridrich	k1gMnSc2	Fridrich
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Falckého	falcký	k2eAgInSc2d1	falcký
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
katolické	katolický	k2eAgInPc1d1	katolický
státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgInSc3	ten
sloučily	sloučit	k5eAaPmAgFnP	sloučit
do	do	k7c2	do
Katolické	katolický	k2eAgFnSc2d1	katolická
ligy	liga	k1gFnSc2	liga
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Maxmiliánem	Maxmilián	k1gMnSc7	Maxmilián
I.	I.	kA	I.
Bavorským	bavorský	k2eAgFnPc3d1	bavorská
<g/>
.	.	kIx.	.
</s>
<s>
Notná	notný	k2eAgFnSc1d1	notná
část	část	k1gFnSc1	část
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
patřící	patřící	k2eAgFnSc1d1	patřící
pod	pod	k7c4	pod
španělskou	španělský	k2eAgFnSc4d1	španělská
vládu	vláda	k1gFnSc4	vláda
byla	být	k5eAaImAgFnS	být
již	již	k6eAd1	již
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
se	s	k7c7	s
Španělskem	Španělsko	k1gNnSc7	Španělsko
ve	v	k7c6	v
válečném	válečný	k2eAgInSc6d1	válečný
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
územními	územní	k2eAgInPc7d1	územní
celky	celek	k1gInPc7	celek
bylo	být	k5eAaImAgNnS	být
sepsáno	sepsán	k2eAgNnSc1d1	sepsáno
dvanáctileté	dvanáctiletý	k2eAgNnSc1d1	dvanáctileté
příměří	příměří	k1gNnSc1	příměří
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
neznamenalo	znamenat	k5eNaImAgNnS	znamenat
mír	mír	k1gInSc4	mír
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sever	sever	k1gInSc1	sever
svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
byl	být	k5eAaImAgInS	být
také	také	k9	také
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
luteránů	luterán	k1gMnPc2	luterán
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
sousedních	sousední	k2eAgFnPc2d1	sousední
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
Dánsko-Norska	Dánsko-Norsko	k1gNnSc2	Dánsko-Norsko
a	a	k8xC	a
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Dánsko-Norsko	Dánsko-Norsko	k6eAd1	Dánsko-Norsko
ovládalo	ovládat	k5eAaImAgNnS	ovládat
holštýnské	holštýnský	k2eAgNnSc1d1	holštýnské
vévodství	vévodství	k1gNnSc1	vévodství
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
kdyby	kdyby	kYmCp3nS	kdyby
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
politických	politický	k2eAgInPc2d1	politický
poměrů	poměr	k1gInPc2	poměr
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
panství	panství	k1gNnSc4	panství
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
jistě	jistě	k9	jistě
mělo	mít	k5eAaImAgNnS	mít
neblahý	blahý	k2eNgInSc4d1	neblahý
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
tomu	ten	k3xDgNnSc3	ten
Švédsko	Švédsko	k1gNnSc1	Švédsko
přímo	přímo	k6eAd1	přímo
neovládalo	ovládat	k5eNaImAgNnS	ovládat
žádné	žádný	k3yNgNnSc1	žádný
území	území	k1gNnSc1	území
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gMnSc1	jeho
panovník	panovník	k1gMnSc1	panovník
Gustav	Gustav	k1gMnSc1	Gustav
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Albrecht	Albrecht	k1gMnSc1	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejno	k1gNnSc2	Valdštejno
<g/>
,	,	kIx,	,
snil	snít	k5eAaImAgInS	snít
o	o	k7c6	o
přinejmenším	přinejmenším	k6eAd1	přinejmenším
evropské	evropský	k2eAgFnSc6d1	Evropská
mocnosti	mocnost	k1gFnSc6	mocnost
ovládané	ovládaný	k2eAgFnSc6d1	ovládaná
jednou	jednou	k6eAd1	jednou
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
sen	sen	k1gInSc1	sen
opět	opět	k6eAd1	opět
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
konfliktu	konflikt	k1gInSc6	konflikt
zmařen	zmařen	k2eAgMnSc1d1	zmařen
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
uskutečněn	uskutečněn	k2eAgInSc1d1	uskutečněn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ani	ani	k8xC	ani
Francie	Francie	k1gFnSc1	Francie
nebyla	být	k5eNaImAgFnS	být
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
spokojena	spokojen	k2eAgFnSc1d1	spokojena
s	s	k7c7	s
územním	územní	k2eAgNnSc7d1	územní
uspořádáním	uspořádání	k1gNnSc7	uspořádání
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Francouzští	francouzský	k2eAgMnPc1d1	francouzský
panovníci	panovník	k1gMnPc1	panovník
viděli	vidět	k5eAaImAgMnP	vidět
velký	velký	k2eAgInSc4d1	velký
problém	problém	k1gInSc4	problém
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byli	být	k5eAaImAgMnP	být
obklopeni	obklopit	k5eAaPmNgMnP	obklopit
državami	država	k1gFnPc7	država
Habsburků	Habsburk	k1gMnPc2	Habsburk
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nebylo	být	k5eNaImAgNnS	být
nijak	nijak	k6eAd1	nijak
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Francie	Francie	k1gFnSc1	Francie
vložila	vložit	k5eAaPmAgFnS	vložit
do	do	k7c2	do
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Zprvu	zprvu	k6eAd1	zprvu
pouze	pouze	k6eAd1	pouze
peněžním	peněžní	k2eAgNnSc7d1	peněžní
přispěním	přispění	k1gNnSc7	přispění
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
vypořádala	vypořádat	k5eAaPmAgFnS	vypořádat
s	s	k7c7	s
vnitropolitickými	vnitropolitický	k2eAgInPc7d1	vnitropolitický
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
i	i	k9	i
podporou	podpora	k1gFnSc7	podpora
armádní	armádní	k2eAgFnSc7d1	armádní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fáze	fáze	k1gFnSc2	fáze
války	válka	k1gFnSc2	válka
==	==	k?	==
</s>
</p>
<p>
<s>
Třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
čtyři	čtyři	k4xCgFnPc4	čtyři
části	část	k1gFnPc4	část
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
česko-falcká	českoalcký	k2eAgFnSc1d1	česko-falcký
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
probíhala	probíhat	k5eAaImAgFnS	probíhat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1618	[number]	k4	1618
až	až	k9	až
1623	[number]	k4	1623
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
období	období	k1gNnSc4	období
války	válka	k1gFnSc2	válka
české	český	k2eAgFnSc2d1	Česká
(	(	kIx(	(
<g/>
1618	[number]	k4	1618
<g/>
–	–	k?	–
<g/>
1620	[number]	k4	1620
<g/>
)	)	kIx)	)
a	a	k8xC	a
falcké	falcký	k2eAgNnSc1d1	falcké
(	(	kIx(	(
<g/>
1620	[number]	k4	1620
<g/>
–	–	k?	–
<g/>
1623	[number]	k4	1623
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
odboj	odboj	k1gInSc4	odboj
českých	český	k2eAgInPc2d1	český
stavů	stav	k1gInPc2	stav
<g/>
,	,	kIx,	,
vládu	vláda	k1gFnSc4	vláda
Fridricha	Fridrich	k1gMnSc2	Fridrich
Falckého	falcký	k2eAgMnSc2d1	falcký
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
jeho	on	k3xPp3gInSc4	on
boj	boj	k1gInSc4	boj
s	s	k7c7	s
Nizozemím	Nizozemí	k1gNnSc7	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
fázi	fáze	k1gFnSc3	fáze
řadí	řadit	k5eAaImIp3nS	řadit
i	i	k9	i
období	období	k1gNnSc1	období
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1623	[number]	k4	1623
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
tehdy	tehdy	k6eAd1	tehdy
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
zavládl	zavládnout	k5eAaPmAgInS	zavládnout
klid	klid	k1gInSc1	klid
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
<g/>
Druhé	druhý	k4xOgFnSc2	druhý
části	část	k1gFnSc2	část
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
válka	válka	k1gFnSc1	válka
dánská	dánský	k2eAgFnSc1d1	dánská
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
dánsko-dolnosaská	dánskoolnosaský	k2eAgFnSc1d1	dánsko-dolnosaský
<g/>
;	;	kIx,	;
probíhala	probíhat	k5eAaImAgFnS	probíhat
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1625	[number]	k4	1625
<g/>
–	–	k?	–
<g/>
1629	[number]	k4	1629
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
vstupem	vstup	k1gInSc7	vstup
Dánského	dánský	k2eAgNnSc2d1	dánské
a	a	k8xC	a
Norského	norský	k2eAgNnSc2d1	norské
království	království	k1gNnSc2	království
do	do	k7c2	do
války	válka	k1gFnSc2	válka
a	a	k8xC	a
nepříliš	příliš	k6eNd1	příliš
úspěšným	úspěšný	k2eAgNnSc7d1	úspěšné
válečným	válečný	k2eAgNnSc7d1	válečné
úsilím	úsilí	k1gNnSc7	úsilí
Kristiána	Kristián	k1gMnSc2	Kristián
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
<g/>
Třetí	třetí	k4xOgFnSc1	třetí
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
švédskou	švédský	k2eAgFnSc7d1	švédská
<g/>
,	,	kIx,	,
ohraničuje	ohraničovat	k5eAaImIp3nS	ohraničovat
vstup	vstup	k1gInSc4	vstup
Gustava	Gustav	k1gMnSc2	Gustav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Adolfa	Adolf	k1gMnSc4	Adolf
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1630	[number]	k4	1630
a	a	k8xC	a
sepsání	sepsání	k1gNnSc4	sepsání
pražského	pražský	k2eAgInSc2d1	pražský
míru	mír	k1gInSc2	mír
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1635	[number]	k4	1635
<g/>
.	.	kIx.	.
<g/>
Poslední	poslední	k2eAgFnSc1d1	poslední
část	část	k1gFnSc1	část
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
švédsko-francouzskou	švédskorancouzský	k2eAgFnSc7d1	švédsko-francouzská
či	či	k8xC	či
francouzsko-švédskou	francouzsko-švédský	k2eAgFnSc7d1	francouzsko-švédský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
výrazné	výrazný	k2eAgFnSc3d1	výrazná
účasti	účast	k1gFnSc3	účast
Francie	Francie	k1gFnSc2	Francie
často	často	k6eAd1	často
pouze	pouze	k6eAd1	pouze
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
aktivním	aktivní	k2eAgInSc7d1	aktivní
vstupem	vstup	k1gInSc7	vstup
Francie	Francie	k1gFnSc2	Francie
do	do	k7c2	do
války	válka	k1gFnSc2	válka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1635	[number]	k4	1635
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
podepsáním	podepsání	k1gNnSc7	podepsání
vestfálského	vestfálský	k2eAgInSc2d1	vestfálský
míru	mír	k1gInSc2	mír
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1648	[number]	k4	1648
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Česko-falcká	českoalcký	k2eAgFnSc1d1	česko-falcký
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
1618	[number]	k4	1618
<g/>
–	–	k?	–
<g/>
1623	[number]	k4	1623
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Pražská	pražský	k2eAgFnSc1d1	Pražská
defenestrace	defenestrace	k1gFnSc1	defenestrace
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
oficiálním	oficiální	k2eAgInSc7d1	oficiální
začátkem	začátek	k1gInSc7	začátek
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Okamžitě	okamžitě	k6eAd1	okamžitě
po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
události	událost	k1gFnSc6	událost
byla	být	k5eAaImAgFnS	být
zvolena	zvolit	k5eAaPmNgFnS	zvolit
prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
vláda	vláda	k1gFnSc1	vláda
třiceti	třicet	k4xCc2	třicet
direktorů	direktor	k1gMnPc2	direktor
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nahradila	nahradit	k5eAaPmAgFnS	nahradit
správu	správa	k1gFnSc4	správa
pěti	pět	k4xCc2	pět
místodržících	místodržící	k1gMnPc2	místodržící
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
stavovských	stavovský	k2eAgMnPc2d1	stavovský
předáků	předák	k1gMnPc2	předák
Jindřich	Jindřich	k1gMnSc1	Jindřich
Matyáš	Matyáš	k1gMnSc1	Matyáš
Thurn	Thurn	k1gMnSc1	Thurn
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
generálem	generál	k1gMnSc7	generál
stavovské	stavovský	k2eAgFnSc2d1	stavovská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
stavové	stavový	k2eAgNnSc1d1	stavové
tuto	tento	k3xDgFnSc4	tento
akci	akce	k1gFnSc4	akce
obhajovali	obhajovat	k5eAaImAgMnP	obhajovat
jako	jako	k8xC	jako
nápravu	náprava	k1gFnSc4	náprava
chyb	chyba	k1gFnPc2	chyba
královských	královský	k2eAgMnPc2d1	královský
správců	správce	k1gMnPc2	správce
<g/>
,	,	kIx,	,
ne	ne	k9	ne
jako	jako	k9	jako
vzpouru	vzpoura	k1gFnSc4	vzpoura
proti	proti	k7c3	proti
panovníkovi	panovník	k1gMnSc3	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
hrabě	hrabě	k1gMnSc1	hrabě
Thurn	Thurn	k1gMnSc1	Thurn
s	s	k7c7	s
armádou	armáda	k1gFnSc7	armáda
čítající	čítající	k2eAgFnSc4d1	čítající
3	[number]	k4	3
000	[number]	k4	000
pěších	pěší	k1gMnPc2	pěší
a	a	k8xC	a
na	na	k7c4	na
1	[number]	k4	1
100	[number]	k4	100
jízdních	jízdní	k2eAgInPc2d1	jízdní
pokusil	pokusit	k5eAaPmAgMnS	pokusit
dobýt	dobýt	k5eAaPmF	dobýt
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
a	a	k8xC	a
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
,	,	kIx,	,
prohabsburská	prohabsburský	k2eAgNnPc1d1	prohabsburský
města	město	k1gNnPc1	město
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obě	dva	k4xCgFnPc1	dva
byla	být	k5eAaImAgNnP	být
dobře	dobře	k6eAd1	dobře
opevněna	opevněn	k2eAgNnPc1d1	opevněno
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
podnikla	podniknout	k5eAaPmAgFnS	podniknout
první	první	k4xOgFnSc4	první
část	část	k1gFnSc4	část
císařské	císařský	k2eAgFnSc2d1	císařská
armády	armáda	k1gFnSc2	armáda
přechod	přechod	k1gInSc1	přechod
českých	český	k2eAgFnPc2d1	Česká
hranic	hranice	k1gFnPc2	hranice
u	u	k7c2	u
Nové	Nové	k2eAgFnSc2d1	Nové
Bystřice	Bystřice	k1gFnSc2	Bystřice
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
přešla	přejít	k5eAaPmAgFnS	přejít
přes	přes	k7c4	přes
neutrální	neutrální	k2eAgFnSc4d1	neutrální
Moravu	Morava	k1gFnSc4	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
císařské	císařský	k2eAgFnPc1d1	císařská
armády	armáda	k1gFnPc1	armáda
byly	být	k5eAaImAgFnP	být
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
slezských	slezský	k2eAgInPc2d1	slezský
stavů	stav	k1gInPc2	stav
Čechy	Čechy	k1gFnPc1	Čechy
poraženy	poražen	k2eAgFnPc1d1	poražena
<g/>
.	.	kIx.	.
<g/>
Původně	původně	k6eAd1	původně
bylo	být	k5eAaImAgNnS	být
české	český	k2eAgNnSc1d1	české
povstání	povstání	k1gNnSc1	povstání
záležitostí	záležitost	k1gFnPc2	záležitost
pouze	pouze	k6eAd1	pouze
panského	panský	k2eAgInSc2d1	panský
stavu	stav	k1gInSc2	stav
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Když	když	k8xS	když
později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
nákladné	nákladný	k2eAgNnSc1d1	nákladné
vedení	vedení	k1gNnSc1	vedení
války	válka	k1gFnSc2	válka
sami	sám	k3xTgMnPc1	sám
nezvládnou	zvládnout	k5eNaPmIp3nP	zvládnout
<g/>
,	,	kIx,	,
přesvědčili	přesvědčit	k5eAaPmAgMnP	přesvědčit
měšťany	měšťan	k1gMnPc7	měšťan
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jejich	jejich	k3xOp3gInSc4	jejich
odboj	odboj	k1gInSc4	odboj
finančně	finančně	k6eAd1	finančně
podporovali	podporovat	k5eAaImAgMnP	podporovat
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
stavové	stavový	k2eAgNnSc4d1	stavové
zprvu	zprvu	k6eAd1	zprvu
obávali	obávat	k5eAaImAgMnP	obávat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nezvrhl	zvrhnout	k5eNaPmAgInS	zvrhnout
v	v	k7c4	v
nekontrolovatelnou	kontrolovatelný	k2eNgFnSc4d1	nekontrolovatelná
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
nechtěli	chtít	k5eNaImAgMnP	chtít
dělit	dělit	k5eAaImF	dělit
o	o	k7c4	o
případné	případný	k2eAgInPc4d1	případný
úspěchy	úspěch	k1gInPc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
tedy	tedy	k9	tedy
bylo	být	k5eAaImAgNnS	být
nemyslitelné	myslitelný	k2eNgNnSc1d1	nemyslitelné
jednat	jednat	k5eAaImF	jednat
s	s	k7c7	s
nižšími	nízký	k2eAgInPc7d2	nižší
stavy	stav	k1gInPc7	stav
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
později	pozdě	k6eAd2	pozdě
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
normální	normální	k2eAgMnSc1d1	normální
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
měšťané	měšťan	k1gMnPc1	měšťan
účastnili	účastnit	k5eAaImAgMnP	účastnit
i	i	k9	i
diplomatických	diplomatický	k2eAgFnPc2d1	diplomatická
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Financování	financování	k1gNnSc1	financování
stavovské	stavovský	k2eAgFnSc2d1	stavovská
armády	armáda	k1gFnSc2	armáda
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
provázela	provázet	k5eAaImAgFnS	provázet
nechuť	nechuť	k1gFnSc1	nechuť
šlechty	šlechta	k1gFnSc2	šlechta
jakkoli	jakkoli	k6eAd1	jakkoli
ji	on	k3xPp3gFnSc4	on
podporovat	podporovat	k5eAaImF	podporovat
z	z	k7c2	z
vlastních	vlastní	k2eAgFnPc2d1	vlastní
rezerv	rezerva	k1gFnPc2	rezerva
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
solventnost	solventnost	k1gFnSc1	solventnost
během	během	k7c2	během
války	válka	k1gFnSc2	válka
nijak	nijak	k6eAd1	nijak
rapidně	rapidně	k6eAd1	rapidně
nezmenšovala	zmenšovat	k5eNaImAgFnS	zmenšovat
a	a	k8xC	a
dokázala	dokázat	k5eAaPmAgFnS	dokázat
pokrýt	pokrýt	k5eAaPmF	pokrýt
o	o	k7c4	o
něco	něco	k3yInSc4	něco
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
polovinu	polovina	k1gFnSc4	polovina
reálných	reálný	k2eAgInPc2d1	reálný
výdajů	výdaj	k1gInPc2	výdaj
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
nebylo	být	k5eNaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
seskupení	seskupení	k1gNnSc1	seskupení
zemí	zem	k1gFnPc2	zem
spadajících	spadající	k2eAgFnPc2d1	spadající
pod	pod	k7c4	pod
Korunu	koruna	k1gFnSc4	koruna
českou	český	k2eAgFnSc4d1	Česká
bylo	být	k5eAaImAgNnS	být
vcelku	vcelku	k6eAd1	vcelku
volné	volný	k2eAgNnSc1d1	volné
<g/>
,	,	kIx,	,
zachovávali	zachovávat	k5eAaImAgMnP	zachovávat
si	se	k3xPyFc3	se
stavové	stavový	k2eAgInPc4d1	stavový
z	z	k7c2	z
Moravského	moravský	k2eAgNnSc2d1	Moravské
markrabství	markrabství	k1gNnSc2	markrabství
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
sporu	spor	k1gInSc6	spor
neutralitu	neutralita	k1gFnSc4	neutralita
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
spíše	spíše	k9	spíše
klonili	klonit	k5eAaImAgMnP	klonit
na	na	k7c4	na
císařskou	císařský	k2eAgFnSc4d1	císařská
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
vůdcem	vůdce	k1gMnSc7	vůdce
moravských	moravský	k2eAgInPc2d1	moravský
stavů	stav	k1gInPc2	stav
byl	být	k5eAaImAgMnS	být
šlechtic	šlechtic	k1gMnSc1	šlechtic
Karel	Karel	k1gMnSc1	Karel
starší	starší	k1gMnSc1	starší
ze	z	k7c2	z
Žerotína	Žerotín	k1gInSc2	Žerotín
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vzpouru	vzpoura	k1gFnSc4	vzpoura
Čechů	Čech	k1gMnPc2	Čech
nepodporoval	podporovat	k5eNaImAgInS	podporovat
kvůli	kvůli	k7c3	kvůli
malé	malý	k2eAgFnSc3d1	malá
zahraniční	zahraniční	k2eAgFnSc3d1	zahraniční
podpoře	podpora	k1gFnSc3	podpora
<g/>
.	.	kIx.	.
<g/>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
roku	rok	k1gInSc2	rok
1619	[number]	k4	1619
se	se	k3xPyFc4	se
Matyášovi	Matyáš	k1gMnSc3	Matyáš
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
svého	svůj	k3xOyFgMnSc4	svůj
příbuzného	příbuzný	k1gMnSc4	příbuzný
na	na	k7c6	na
španělském	španělský	k2eAgInSc6d1	španělský
trůně	trůn	k1gInSc6	trůn
<g/>
,	,	kIx,	,
Filipa	Filip	k1gMnSc2	Filip
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
pomoc	pomoc	k1gFnSc4	pomoc
proti	proti	k7c3	proti
vzbouřencům	vzbouřenec	k1gMnPc3	vzbouřenec
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgInSc3	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
roku	rok	k1gInSc2	rok
1619	[number]	k4	1619
k	k	k7c3	k
českému	český	k2eAgInSc3d1	český
odboji	odboj	k1gInSc3	odboj
přidali	přidat	k5eAaPmAgMnP	přidat
i	i	k9	i
stavové	stavový	k2eAgNnSc4d1	stavové
z	z	k7c2	z
Moravy	Morava	k1gFnSc2	Morava
<g/>
;	;	kIx,	;
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
generál	generál	k1gMnSc1	generál
hrabě	hrabě	k1gMnSc1	hrabě
Thurn	Thurn	k1gMnSc1	Thurn
české	český	k2eAgFnSc2d1	Česká
stavovské	stavovský	k2eAgFnSc2d1	stavovská
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
přiměl	přimět	k5eAaPmAgInS	přimět
protestantskou	protestantský	k2eAgFnSc4d1	protestantská
šlechtu	šlechta	k1gFnSc4	šlechta
k	k	k7c3	k
převratu	převrat	k1gInSc3	převrat
<g/>
.	.	kIx.	.
</s>
<s>
Moravským	moravský	k2eAgMnSc7d1	moravský
zemským	zemský	k2eAgMnSc7d1	zemský
hejtmanem	hejtman	k1gMnSc7	hejtman
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
stal	stát	k5eAaPmAgMnS	stát
Ladislav	Ladislav	k1gMnSc1	Ladislav
Velen	velen	k2eAgMnSc1d1	velen
ze	z	k7c2	z
Žerotína	Žerotín	k1gInSc2	Žerotín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Stavovská	stavovský	k2eAgFnSc1d1	stavovská
armáda	armáda	k1gFnSc1	armáda
zprvu	zprvu	k6eAd1	zprvu
slavila	slavit	k5eAaImAgFnS	slavit
úspěchy	úspěch	k1gInPc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
velké	velký	k2eAgNnSc4d1	velké
vítězství	vítězství	k1gNnSc4	vítězství
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
generál	generál	k1gMnSc1	generál
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
Petr	Petr	k1gMnSc1	Petr
Arnošt	Arnošt	k1gMnSc1	Arnošt
Mansfeld	Mansfeld	k1gMnSc1	Mansfeld
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
dobýt	dobýt	k5eAaPmF	dobýt
Plzeň	Plzeň	k1gFnSc4	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
nezastavila	zastavit	k5eNaPmAgFnS	zastavit
a	a	k8xC	a
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
hraběte	hrabě	k1gMnSc2	hrabě
Thurna	Thurna	k1gFnSc1	Thurna
přitáhla	přitáhnout	k5eAaPmAgFnS	přitáhnout
až	až	k9	až
k	k	k7c3	k
Vídni	Vídeň	k1gFnSc3	Vídeň
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
málem	málem	k6eAd1	málem
dobyla	dobýt	k5eAaPmAgFnS	dobýt
<g/>
,	,	kIx,	,
musela	muset	k5eAaImAgFnS	muset
se	se	k3xPyFc4	se
ale	ale	k9	ale
stáhnout	stáhnout	k5eAaPmF	stáhnout
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
přišla	přijít	k5eAaPmAgFnS	přijít
porážka	porážka	k1gFnSc1	porážka
protestantských	protestantský	k2eAgNnPc2d1	protestantské
vojsk	vojsko	k1gNnPc2	vojsko
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
hraběte	hrabě	k1gMnSc2	hrabě
Mansfelda	Mansfeld	k1gMnSc2	Mansfeld
od	od	k7c2	od
odvetné	odvetný	k2eAgFnSc2d1	odvetná
armády	armáda	k1gFnSc2	armáda
vedené	vedený	k2eAgFnSc2d1	vedená
polním	polní	k2eAgMnSc7d1	polní
maršálem	maršál	k1gMnSc7	maršál
Buquoyem	Buquoy	k1gMnSc7	Buquoy
u	u	k7c2	u
Záblatí	Záblatí	k1gNnSc2	Záblatí
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
porážce	porážka	k1gFnSc6	porážka
byl	být	k5eAaImAgMnS	být
hrabě	hrabě	k1gMnSc1	hrabě
Thurn	Thurn	k1gMnSc1	Thurn
odříznut	odříznout	k5eAaPmNgMnS	odříznout
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
podporovatelů	podporovatel	k1gMnPc2	podporovatel
rebelie	rebelie	k1gFnSc2	rebelie
<g/>
,	,	kIx,	,
Savojsko	Savojsko	k1gNnSc1	Savojsko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
posílalo	posílat	k5eAaImAgNnS	posílat
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
jisté	jistý	k2eAgFnSc2d1	jistá
peněžní	peněžní	k2eAgFnPc4d1	peněžní
částky	částka	k1gFnPc4	částka
<g/>
,	,	kIx,	,
také	také	k9	také
vojáky	voják	k1gMnPc4	voják
na	na	k7c6	na
obsazení	obsazení	k1gNnSc6	obsazení
pevností	pevnost	k1gFnPc2	pevnost
v	v	k7c6	v
Porýní	Porýní	k1gNnSc6	Porýní
a	a	k8xC	a
Mansfelda	Mansfelda	k1gFnSc1	Mansfelda
samotného	samotný	k2eAgInSc2d1	samotný
s	s	k7c7	s
2	[number]	k4	2
000	[number]	k4	000
vojáky	voják	k1gMnPc7	voják
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
na	na	k7c6	na
podpoře	podpora	k1gFnSc6	podpora
prakticky	prakticky	k6eAd1	prakticky
přestalo	přestat	k5eAaPmAgNnS	přestat
podílet	podílet	k5eAaImF	podílet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Posléze	posléze	k6eAd1	posléze
došlo	dojít	k5eAaPmAgNnS	dojít
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1619	[number]	k4	1619
k	k	k7c3	k
sesazení	sesazení	k1gNnSc3	sesazení
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
z	z	k7c2	z
českého	český	k2eAgInSc2d1	český
trůnu	trůn	k1gInSc2	trůn
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
o	o	k7c4	o
devět	devět	k4xCc4	devět
dní	den	k1gInPc2	den
později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
zvolen	zvolit	k5eAaPmNgInS	zvolit
a	a	k8xC	a
korunován	korunovat	k5eAaBmNgInS	korunovat
za	za	k7c4	za
římského	římský	k2eAgMnSc4d1	římský
císaře	císař	k1gMnSc4	císař
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
zvolení	zvolení	k1gNnSc4	zvolení
Fridricha	Fridrich	k1gMnSc2	Fridrich
Falckého	falcký	k2eAgMnSc2d1	falcký
za	za	k7c2	za
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
jmenování	jmenování	k1gNnSc3	jmenování
nového	nový	k2eAgMnSc2d1	nový
stavovského	stavovský	k2eAgMnSc2d1	stavovský
vrchního	vrchní	k2eAgMnSc2d1	vrchní
velitele	velitel	k1gMnSc2	velitel
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
;	;	kIx,	;
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jím	on	k3xPp3gMnSc7	on
Kristián	Kristián	k1gMnSc1	Kristián
I.	I.	kA	I.
Anhaltský	Anhaltský	k2eAgInSc5d1	Anhaltský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Zimní	zimní	k2eAgMnSc1d1	zimní
král	král	k1gMnSc1	král
====	====	k?	====
</s>
</p>
<p>
<s>
Stavové	stavový	k2eAgInPc1d1	stavový
si	se	k3xPyFc3	se
vybrali	vybrat	k5eAaPmAgMnP	vybrat
svého	svůj	k3xOyFgMnSc4	svůj
kandidáta	kandidát	k1gMnSc4	kandidát
na	na	k7c4	na
českého	český	k2eAgMnSc4d1	český
krále	král	k1gMnSc4	král
již	již	k6eAd1	již
za	za	k7c4	za
panování	panování	k1gNnSc4	panování
bezdětného	bezdětný	k2eAgMnSc2d1	bezdětný
Rudolfa	Rudolf	k1gMnSc2	Rudolf
a	a	k8xC	a
snažili	snažit	k5eAaImAgMnP	snažit
se	se	k3xPyFc4	se
prosadit	prosadit	k5eAaPmF	prosadit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
Albrechta	Albrecht	k1gMnSc2	Albrecht
Jana	Jan	k1gMnSc2	Jan
Smiřického	smiřický	k2eAgMnSc2d1	smiřický
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
však	však	k9	však
záhy	záhy	k6eAd1	záhy
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Jindřich	Jindřich	k1gMnSc1	Jindřich
Jiří	Jiří	k1gMnSc1	Jiří
nepřipadal	připadat	k5eNaImAgMnS	připadat
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byl	být	k5eAaImAgInS	být
slabomyslný	slabomyslný	k2eAgInSc1d1	slabomyslný
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
objevili	objevit	k5eAaPmAgMnP	objevit
tři	tři	k4xCgMnPc1	tři
kandidáti	kandidát	k1gMnPc1	kandidát
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
<g/>
:	:	kIx,	:
Karel	Karel	k1gMnSc1	Karel
Emanuel	Emanuel	k1gMnSc1	Emanuel
Savojský	savojský	k2eAgMnSc1d1	savojský
<g/>
,	,	kIx,	,
luterán	luterán	k1gMnSc1	luterán
Jan	Jan	k1gMnSc1	Jan
Jiří	Jiří	k1gMnSc1	Jiří
Saský	saský	k2eAgMnSc1d1	saský
a	a	k8xC	a
kalvinista	kalvinista	k1gMnSc1	kalvinista
Fridrich	Fridrich	k1gMnSc1	Fridrich
Falcký	falcký	k2eAgMnSc1d1	falcký
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Emanuel	Emanuel	k1gMnSc1	Emanuel
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Savojska	Savojsko	k1gNnSc2	Savojsko
a	a	k8xC	a
s	s	k7c7	s
Benátskou	benátský	k2eAgFnSc7d1	Benátská
republikou	republika	k1gFnSc7	republika
podporoval	podporovat	k5eAaImAgInS	podporovat
české	český	k2eAgNnSc4d1	české
povstání	povstání	k1gNnSc4	povstání
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gFnPc4	jeho
šance	šance	k1gFnPc4	šance
na	na	k7c4	na
zvolení	zvolení	k1gNnSc4	zvolení
byly	být	k5eAaImAgFnP	být
mizivé	mizivý	k2eAgFnPc1d1	mizivá
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gInPc3	jeho
velkým	velký	k2eAgInPc3d1	velký
dluhům	dluh	k1gInPc3	dluh
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Jiří	Jiří	k1gMnSc1	Jiří
měl	mít	k5eAaImAgMnS	mít
velkou	velký	k2eAgFnSc4d1	velká
podporu	podpora	k1gFnSc4	podpora
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
luterán	luterán	k1gMnSc1	luterán
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
kandidát	kandidát	k1gMnSc1	kandidát
byl	být	k5eAaImAgMnS	být
sice	sice	k8xC	sice
kalvinista	kalvinista	k1gMnSc1	kalvinista
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
poněkud	poněkud	k6eAd1	poněkud
odlišoval	odlišovat	k5eAaImAgInS	odlišovat
od	od	k7c2	od
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
majoritního	majoritní	k2eAgInSc2d1	majoritní
českého	český	k2eAgInSc2d1	český
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mluvilo	mluvit	k5eAaImAgNnS	mluvit
pro	pro	k7c4	pro
něho	on	k3xPp3gInSc4	on
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
dynastie	dynastie	k1gFnSc2	dynastie
Wittelsbachů	Wittelsbach	k1gMnPc2	Wittelsbach
<g/>
,	,	kIx,	,
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
rodů	rod	k1gInPc2	rod
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
vládce	vládce	k1gMnSc4	vládce
historicky	historicky	k6eAd1	historicky
velmi	velmi	k6eAd1	velmi
významného	významný	k2eAgNnSc2d1	významné
území	území	k1gNnSc2	území
v	v	k7c6	v
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
otci	otec	k1gMnSc6	otec
zdědil	zdědit	k5eAaPmAgInS	zdědit
dokonce	dokonce	k9	dokonce
vůdcovství	vůdcovství	k1gNnSc4	vůdcovství
v	v	k7c6	v
Protestantské	protestantský	k2eAgFnSc6d1	protestantská
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
výhodou	výhoda	k1gFnSc7	výhoda
pro	pro	k7c4	pro
něho	on	k3xPp3gInSc4	on
byla	být	k5eAaImAgFnS	být
blízká	blízký	k2eAgFnSc1d1	blízká
vazba	vazba	k1gFnSc1	vazba
na	na	k7c4	na
anglického	anglický	k2eAgMnSc4d1	anglický
krále	král	k1gMnSc4	král
Jakuba	Jakub	k1gMnSc2	Jakub
I.	I.	kA	I.
<g/>
,	,	kIx,	,
s	s	k7c7	s
jehož	jehož	k3xOyRp3gFnSc7	jehož
dcerou	dcera	k1gFnSc7	dcera
Alžbětou	Alžběta	k1gFnSc7	Alžběta
byl	být	k5eAaImAgInS	být
sezdán	sezdán	k2eAgMnSc1d1	sezdán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
vlastní	vlastní	k2eAgFnSc6d1	vlastní
volbě	volba	k1gFnSc6	volba
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Fridrich	Fridrich	k1gMnSc1	Fridrich
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
výše	výše	k1gFnSc2	výše
popsaných	popsaný	k2eAgFnPc2d1	popsaná
výhod	výhoda	k1gFnPc2	výhoda
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
využít	využít	k5eAaPmF	využít
a	a	k8xC	a
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
se	se	k3xPyFc4	se
slova	slovo	k1gNnPc1	slovo
skeptiků	skeptik	k1gMnPc2	skeptik
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
ho	on	k3xPp3gMnSc4	on
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1619	[number]	k4	1619
označovali	označovat	k5eAaImAgMnP	označovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
zimního	zimní	k2eAgMnSc4d1	zimní
krále	král	k1gMnSc4	král
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1620	[number]	k4	1620
musel	muset	k5eAaImAgInS	muset
země	zem	k1gFnSc2	zem
</s>
</p>
<p>
<s>
Koruny	koruna	k1gFnPc4	koruna
české	český	k2eAgFnSc2d1	Česká
opustit	opustit	k5eAaPmF	opustit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
byl	být	k5eAaImAgMnS	být
rozhořčen	rozhořčit	k5eAaPmNgMnS	rozhořčit
stavovskou	stavovský	k2eAgFnSc7d1	stavovská
vzpourou	vzpoura	k1gFnSc7	vzpoura
<g/>
,	,	kIx,	,
a	a	k8xC	a
zatímco	zatímco	k8xS	zatímco
stavové	stavový	k2eAgFnPc1d1	stavová
leželi	ležet	k5eAaImAgMnP	ležet
v	v	k7c6	v
Horních	horní	k2eAgInPc6d1	horní
Rakousích	Rakousy	k1gInPc6	Rakousy
a	a	k8xC	a
váhali	váhat	k5eAaImAgMnP	váhat
s	s	k7c7	s
útokem	útok	k1gInSc7	útok
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
18	[number]	k4	18
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
za	za	k7c4	za
spojence	spojenec	k1gMnSc4	spojenec
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
I.	I.	kA	I.
za	za	k7c4	za
Bavorsko	Bavorsko	k1gNnSc4	Bavorsko
a	a	k8xC	a
Katolickou	katolický	k2eAgFnSc4d1	katolická
ligu	liga	k1gFnSc4	liga
<g/>
.	.	kIx.	.
</s>
<s>
Španělé	Španěl	k1gMnPc1	Španěl
z	z	k7c2	z
Bruselu	Brusel	k1gInSc2	Brusel
vyslali	vyslat	k5eAaPmAgMnP	vyslat
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
spojila	spojit	k5eAaPmAgFnS	spojit
s	s	k7c7	s
císařskou	císařský	k2eAgFnSc7d1	císařská
a	a	k8xC	a
vojskem	vojsko	k1gNnSc7	vojsko
Katolické	katolický	k2eAgFnSc2d1	katolická
ligy	liga	k1gFnSc2	liga
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Karla	Karel	k1gMnSc2	Karel
Buquoye	Buquoy	k1gMnSc2	Buquoy
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
španělský	španělský	k2eAgMnSc1d1	španělský
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
při	při	k7c6	při
vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
dvoře	dvůr	k1gInSc6	dvůr
–	–	k?	–
don	don	k1gMnSc1	don
Íñ	Íñ	k1gMnSc1	Íñ
Vélez	Vélez	k1gMnSc1	Vélez
de	de	k?	de
Guevara	Guevara	k1gFnSc1	Guevara
–	–	k?	–
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1620	[number]	k4	1620
protestantského	protestantský	k2eAgMnSc2d1	protestantský
saského	saský	k2eAgMnSc2d1	saský
kurfiřta	kurfiřt	k1gMnSc2	kurfiřt
Jana	Jan	k1gMnSc2	Jan
Jiřího	Jiří	k1gMnSc2	Jiří
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
pomohl	pomoct	k5eAaPmAgMnS	pomoct
katolickým	katolický	k2eAgFnPc3d1	katolická
silám	síla	k1gFnPc3	síla
<g/>
,	,	kIx,	,
především	především	k9	především
zásahem	zásah	k1gInSc7	zásah
jeho	on	k3xPp3gNnSc2	on
vojska	vojsko	k1gNnSc2	vojsko
v	v	k7c6	v
Lužicích	Lužice	k1gFnPc6	Lužice
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Jiří	Jiří	k1gMnSc1	Jiří
Saský	saský	k2eAgMnSc1d1	saský
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
zahájil	zahájit	k5eAaPmAgMnS	zahájit
útok	útok	k1gInSc4	útok
proti	proti	k7c3	proti
narušiteli	narušitel	k1gMnSc3	narušitel
míru	míra	k1gFnSc4	míra
ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
říši	říš	k1gFnSc6	říš
<g/>
,	,	kIx,	,
Fridrichu	Fridrich	k1gMnSc3	Fridrich
Falckému	falcký	k2eAgMnSc3d1	falcký
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
němu	on	k3xPp3gInSc3	on
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
slezský	slezský	k2eAgMnSc1d1	slezský
velitel	velitel	k1gMnSc1	velitel
Jan	Jan	k1gMnSc1	Jan
Jiří	Jiří	k1gMnSc1	Jiří
Krnovský	krnovský	k2eAgMnSc1d1	krnovský
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
katolickým	katolický	k2eAgMnPc3d1	katolický
vojskům	vojsko	k1gNnPc3	vojsko
vyslal	vyslat	k5eAaPmAgMnS	vyslat
polský	polský	k2eAgMnSc1d1	polský
král	král	k1gMnSc1	král
Zikmund	Zikmund	k1gMnSc1	Zikmund
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Vasa	Vasa	k1gFnSc1	Vasa
lisovčíky	lisovčík	k1gInPc1	lisovčík
–	–	k?	–
vojáky	voják	k1gMnPc4	voják
kozáckého	kozácký	k2eAgInSc2d1	kozácký
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
při	při	k7c6	při
nájezdu	nájezd	k1gInSc6	nájezd
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
drancovali	drancovat	k5eAaImAgMnP	drancovat
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
na	na	k7c4	na
co	co	k3yInSc4	co
přišli	přijít	k5eAaPmAgMnP	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
město	město	k1gNnSc1	město
Holešov	Holešov	k1gInSc4	Holešov
bylo	být	k5eAaImAgNnS	být
zachráněno	zachránit	k5eAaPmNgNnS	zachránit
zásahem	zásah	k1gInSc7	zásah
kněze	kněz	k1gMnSc2	kněz
Jana	Jan	k1gMnSc2	Jan
Sarkandera	Sarkander	k1gMnSc2	Sarkander
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
"	"	kIx"	"
<g/>
pozvání	pozvání	k1gNnSc4	pozvání
<g/>
"	"	kIx"	"
lisovčíků	lisovčík	k1gMnPc2	lisovčík
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
Praze	Praha	k1gFnSc3	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
přes	přes	k7c4	přes
relativně	relativně	k6eAd1	relativně
dobré	dobrý	k2eAgNnSc4d1	dobré
postavení	postavení	k1gNnSc4	postavení
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
vojska	vojsko	k1gNnSc2	vojsko
oproti	oproti	k7c3	oproti
invazi	invaze	k1gFnSc3	invaze
císařské	císařský	k2eAgFnSc2d1	císařská
armády	armáda	k1gFnSc2	armáda
při	při	k7c6	při
jednom	jeden	k4xCgInSc6	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
střetů	střet	k1gInPc2	střet
<g/>
,	,	kIx,	,
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
8	[number]	k4	8
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1620	[number]	k4	1620
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
stavovské	stavovský	k2eAgFnPc1d1	stavovská
síly	síla	k1gFnPc1	síla
po	po	k7c6	po
jedné	jeden	k4xCgFnSc3	jeden
až	až	k9	až
dvou	dva	k4xCgFnPc6	dva
hodinách	hodina	k1gFnPc6	hodina
boje	boj	k1gInSc2	boj
poraženy	poražen	k2eAgFnPc1d1	poražena
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
zpráva	zpráva	k1gFnSc1	zpráva
donesla	donést	k5eAaPmAgFnS	donést
Fridrichu	Fridrich	k1gMnSc3	Fridrich
Falckému	falcký	k2eAgMnSc3d1	falcký
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zrovna	zrovna	k6eAd1	zrovna
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
snahy	snaha	k1gFnSc2	snaha
město	město	k1gNnSc1	město
hájit	hájit	k5eAaImF	hájit
utekl	utéct	k5eAaPmAgMnS	utéct
z	z	k7c2	z
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
do	do	k7c2	do
slezské	slezský	k2eAgFnSc2d1	Slezská
Vratislavi	Vratislav	k1gFnSc2	Vratislav
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Thurn	Thurn	k1gMnSc1	Thurn
chce	chtít	k5eAaImIp3nS	chtít
vzdát	vzdát	k5eAaPmF	vzdát
císaři	císař	k1gMnSc3	císař
<g/>
,	,	kIx,	,
opustil	opustit	k5eAaPmAgMnS	opustit
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1620	[number]	k4	1620
země	zem	k1gFnSc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
nadobro	nadobro	k6eAd1	nadobro
<g/>
.	.	kIx.	.
</s>
<s>
Thurn	Thurn	k1gMnSc1	Thurn
nakonec	nakonec	k6eAd1	nakonec
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
úmyslu	úmysl	k1gInSc2	úmysl
upustil	upustit	k5eAaPmAgMnS	upustit
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
není	být	k5eNaImIp3nS	být
míru	míra	k1gFnSc4	míra
nakloněn	nakloněn	k2eAgMnSc1d1	nakloněn
<g/>
.	.	kIx.	.
</s>
<s>
Stavovské	stavovský	k2eAgNnSc1d1	Stavovské
povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
definitivně	definitivně	k6eAd1	definitivně
skončilo	skončit	k5eAaPmAgNnS	skončit
popravou	poprava	k1gFnSc7	poprava
dvaceti	dvacet	k4xCc6	dvacet
sedmi	sedm	k4xCc2	sedm
českých	český	k2eAgMnPc2d1	český
pánů	pan	k1gMnPc2	pan
a	a	k8xC	a
veškerý	veškerý	k3xTgInSc4	veškerý
další	další	k2eAgInSc4d1	další
odboj	odboj	k1gInSc4	odboj
probíhal	probíhat	k5eAaImAgInS	probíhat
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
muselo	muset	k5eAaImAgNnS	muset
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
hlásících	hlásící	k2eAgMnPc2d1	hlásící
se	se	k3xPyFc4	se
k	k	k7c3	k
jinému	jiný	k2eAgNnSc3d1	jiné
než	než	k8xS	než
katolickému	katolický	k2eAgNnSc3d1	katolické
vyznání	vyznání	k1gNnSc3	vyznání
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
<g/>
,	,	kIx,	,
některým	některý	k3yIgMnPc3	některý
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
podařilo	podařit	k5eAaPmAgNnS	podařit
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
při	při	k7c6	při
vpádu	vpád	k1gInSc6	vpád
protestantů	protestant	k1gMnPc2	protestant
do	do	k7c2	do
Českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
nikdy	nikdy	k6eAd1	nikdy
netrvala	trvat	k5eNaImAgFnS	trvat
dlouho	dlouho	k6eAd1	dlouho
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Český	český	k2eAgInSc4d1	český
odboj	odboj	k1gInSc4	odboj
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
====	====	k?	====
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Německa	Německo	k1gNnSc2	Německo
se	se	k3xPyFc4	se
za	za	k7c4	za
Fridrichovy	Fridrichův	k2eAgInPc4d1	Fridrichův
zájmy	zájem	k1gInPc4	zájem
postavili	postavit	k5eAaPmAgMnP	postavit
především	především	k9	především
Jan	Jan	k1gMnSc1	Jan
Jiří	Jiří	k1gMnSc1	Jiří
Krnovský	krnovský	k2eAgMnSc1d1	krnovský
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Fridrich	Fridrich	k1gMnSc1	Fridrich
Bádensko-Durlašský	Bádensko-Durlašský	k2eAgMnSc1d1	Bádensko-Durlašský
a	a	k8xC	a
Kristián	Kristián	k1gMnSc1	Kristián
Brunšvický	brunšvický	k2eAgMnSc1d1	brunšvický
<g/>
.	.	kIx.	.
</s>
<s>
Španělský	španělský	k2eAgMnSc1d1	španělský
generál	generál	k1gMnSc1	generál
Ambrosio	Ambrosio	k1gMnSc1	Ambrosio
Spinola	Spinola	k1gFnSc1	Spinola
však	však	k8xC	však
vytlačil	vytlačit	k5eAaPmAgMnS	vytlačit
vojska	vojsko	k1gNnPc4	vojsko
generála	generál	k1gMnSc2	generál
Mansfelda	Mansfeld	k1gMnSc2	Mansfeld
z	z	k7c2	z
Horní	horní	k2eAgFnSc2d1	horní
Falce	Falc	k1gFnSc2	Falc
a	a	k8xC	a
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgFnPc6d1	další
porážkách	porážka	k1gFnPc6	porážka
protestantských	protestantský	k2eAgNnPc2d1	protestantské
vojsk	vojsko	k1gNnPc2	vojsko
byla	být	k5eAaImAgFnS	být
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1621	[number]	k4	1621
sepsána	sepsán	k2eAgFnSc1d1	sepsána
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
základě	základ	k1gInSc6	základ
byla	být	k5eAaImAgFnS	být
Protestantská	protestantský	k2eAgFnSc1d1	protestantská
unie	unie	k1gFnSc1	unie
rozpuštěna	rozpuštěn	k2eAgFnSc1d1	rozpuštěna
.	.	kIx.	.
</s>
<s>
Spinola	Spinola	k1gFnSc1	Spinola
postupoval	postupovat	k5eAaImAgInS	postupovat
dále	daleko	k6eAd2	daleko
k	k	k7c3	k
Jülichu	Jülich	k1gInSc3	Jülich
a	a	k8xC	a
dobyl	dobýt	k5eAaPmAgMnS	dobýt
ho	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Vůdce	vůdce	k1gMnSc1	vůdce
Katolické	katolický	k2eAgFnSc2d1	katolická
ligy	liga	k1gFnSc2	liga
Jan	Jan	k1gMnSc1	Jan
Tilly	Tilla	k1gMnSc2	Tilla
mezitím	mezitím	k6eAd1	mezitím
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1622	[number]	k4	1622
dobyl	dobýt	k5eAaPmAgInS	dobýt
důležitá	důležitý	k2eAgNnPc4d1	důležité
města	město	k1gNnPc4	město
Mannheim	Mannheim	k1gInSc1	Mannheim
a	a	k8xC	a
Heidelberg	Heidelberg	k1gInSc1	Heidelberg
<g/>
.	.	kIx.	.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
přenechal	přenechat	k5eAaPmAgMnS	přenechat
katolíkům	katolík	k1gMnPc3	katolík
Falc	Falc	k1gFnSc4	Falc
a	a	k8xC	a
s	s	k7c7	s
Mansfeldem	Mansfeld	k1gMnSc7	Mansfeld
a	a	k8xC	a
Kristiánem	Kristián	k1gMnSc7	Kristián
se	se	k3xPyFc4	se
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
do	do	k7c2	do
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Kristián	Kristián	k1gMnSc1	Kristián
odjel	odjet	k5eAaPmAgMnS	odjet
pomáhat	pomáhat	k5eAaImF	pomáhat
příbuzným	příbuzný	k1gMnSc7	příbuzný
do	do	k7c2	do
Dolního	dolní	k2eAgNnSc2d1	dolní
Saska	Sasko	k1gNnSc2	Sasko
proti	proti	k7c3	proti
Tillymu	Tillymum	k1gNnSc3	Tillymum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgInS	být
potlačen	potlačen	k2eAgInSc1d1	potlačen
protestantský	protestantský	k2eAgInSc1d1	protestantský
odpor	odpor	k1gInSc1	odpor
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
jižním	jižní	k2eAgNnSc6d1	jižní
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Tilly	Till	k1gInPc1	Till
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
porazil	porazit	k5eAaPmAgInS	porazit
Kristiána	Kristián	k1gMnSc4	Kristián
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
zjištění	zjištění	k1gNnSc6	zjištění
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
bez	bez	k7c2	bez
Mansfeldovy	Mansfeldův	k2eAgFnSc2d1	Mansfeldova
pomoci	pomoc	k1gFnSc2	pomoc
neobejde	obejde	k6eNd1	obejde
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgInS	začít
stahovat	stahovat	k5eAaImF	stahovat
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Kristián	Kristián	k1gMnSc1	Kristián
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
jen	jen	k9	jen
několik	několik	k4yIc4	několik
kilometrů	kilometr	k1gInPc2	kilometr
od	od	k7c2	od
hranic	hranice	k1gFnPc2	hranice
spřáteleného	spřátelený	k2eAgNnSc2d1	spřátelené
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
čtyři	čtyři	k4xCgFnPc4	čtyři
pětiny	pětina	k1gFnPc4	pětina
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
Tilly	Tilla	k1gMnSc2	Tilla
si	se	k3xPyFc3	se
podrobil	podrobit	k5eAaPmAgMnS	podrobit
vzpurné	vzpurný	k2eAgNnSc4d1	vzpurné
Dolní	dolní	k2eAgNnSc4d1	dolní
Sasko	Sasko	k1gNnSc4	Sasko
a	a	k8xC	a
Vestfálsko	Vestfálsko	k1gNnSc4	Vestfálsko
<g/>
.	.	kIx.	.
</s>
<s>
Bavorský	bavorský	k2eAgMnSc1d1	bavorský
vévoda	vévoda	k1gMnSc1	vévoda
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
I.	I.	kA	I.
obdržel	obdržet	k5eAaPmAgMnS	obdržet
jako	jako	k8xS	jako
odměnu	odměna	k1gFnSc4	odměna
za	za	k7c4	za
své	svůj	k3xOyFgFnPc4	svůj
služby	služba	k1gFnPc4	služba
Horní	horní	k2eAgFnSc4d1	horní
Falc	Falc	k1gFnSc4	Falc
a	a	k8xC	a
hodnost	hodnost	k1gFnSc4	hodnost
kurfiřta	kurfiřt	k1gMnSc2	kurfiřt
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
vyobcovaného	vyobcovaný	k2eAgMnSc2d1	vyobcovaný
bratrance	bratranec	k1gMnSc2	bratranec
Fridricha	Fridrich	k1gMnSc2	Fridrich
Falckého	falcký	k2eAgMnSc2d1	falcký
(	(	kIx(	(
<g/>
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
falckého	falcký	k2eAgMnSc2d1	falcký
kurfiřta	kurfiřt	k1gMnSc2	kurfiřt
jako	jako	k9	jako
Fridrich	Fridrich	k1gMnSc1	Fridrich
V.	V.	kA	V.
<g/>
)	)	kIx)	)
zbaveného	zbavený	k2eAgInSc2d1	zbavený
této	tento	k3xDgFnSc6	tento
funkce	funkce	k1gFnPc4	funkce
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1623	[number]	k4	1623
<g/>
.	.	kIx.	.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1	Fridrich
bez	bez	k7c2	bez
území	území	k1gNnSc2	území
a	a	k8xC	a
titulu	titul	k1gInSc2	titul
začal	začít	k5eAaPmAgInS	začít
cestovat	cestovat	k5eAaImF	cestovat
a	a	k8xC	a
přesvědčovat	přesvědčovat	k5eAaImF	přesvědčovat
protestantské	protestantský	k2eAgFnPc4d1	protestantská
země	zem	k1gFnPc4	zem
pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
věc	věc	k1gFnSc4	věc
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
Nizozemí	Nizozemí	k1gNnSc1	Nizozemí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bojovalo	bojovat	k5eAaImAgNnS	bojovat
proti	proti	k7c3	proti
Španělům	Španěl	k1gMnPc3	Španěl
v	v	k7c6	v
osmdesátileté	osmdesátiletý	k2eAgFnSc6d1	osmdesátiletá
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Dánsko	Dánsko	k1gNnSc1	Dánsko
a	a	k8xC	a
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
skončila	skončit	k5eAaPmAgFnS	skončit
část	část	k1gFnSc1	část
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
probíhala	probíhat	k5eAaImAgFnS	probíhat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
.	.	kIx.	.
</s>
<s>
Regionální	regionální	k2eAgInSc1d1	regionální
konflikt	konflikt	k1gInSc1	konflikt
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dánská	dánský	k2eAgFnSc1d1	dánská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
1625	[number]	k4	1625
<g/>
–	–	k?	–
<g/>
1629	[number]	k4	1629
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1623	[number]	k4	1623
až	až	k9	až
1625	[number]	k4	1625
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
relativní	relativní	k2eAgInSc4d1	relativní
klid	klid	k1gInSc4	klid
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ale	ale	k9	ale
nepřetrval	přetrvat	k5eNaPmAgInS	přetrvat
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
1624	[number]	k4	1624
byla	být	k5eAaImAgNnP	být
zahájena	zahájit	k5eAaPmNgNnP	zahájit
jednání	jednání	k1gNnPc1	jednání
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
získat	získat	k5eAaPmF	získat
dánské	dánský	k2eAgFnSc3d1	dánská
a	a	k8xC	a
anglické	anglický	k2eAgFnSc3d1	anglická
spojence	spojenka	k1gFnSc3	spojenka
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
roku	rok	k1gInSc2	rok
1625	[number]	k4	1625
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
dohoda	dohoda	k1gFnSc1	dohoda
zakládající	zakládající	k2eAgFnSc4d1	zakládající
Haagskou	haagský	k2eAgFnSc4d1	Haagská
koalici	koalice	k1gFnSc4	koalice
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
spojily	spojit	k5eAaPmAgFnP	spojit
Nizozemí	Nizozemí	k1gNnSc4	Nizozemí
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
,	,	kIx,	,
Dánsko	Dánsko	k1gNnSc1	Dánsko
a	a	k8xC	a
severní	severní	k2eAgInPc1d1	severní
státy	stát	k1gInPc1	stát
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
proti	proti	k7c3	proti
Habsburkům	Habsburk	k1gMnPc3	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Nizozemí	Nizozemí	k1gNnSc1	Nizozemí
se	se	k3xPyFc4	se
války	válka	k1gFnSc2	válka
účastnilo	účastnit	k5eAaImAgNnS	účastnit
již	již	k6eAd1	již
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
protihabsburské	protihabsburský	k2eAgFnSc6d1	protihabsburská
<g/>
.	.	kIx.	.
</s>
<s>
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
král	král	k1gMnSc1	král
Jakub	Jakub	k1gMnSc1	Jakub
I.	I.	kA	I.
Stuart	Stuart	k1gInSc1	Stuart
<g/>
,	,	kIx,	,
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
přesvědčoval	přesvědčovat	k5eAaImAgMnS	přesvědčovat
svého	svůj	k3xOyFgMnSc4	svůj
zetě	zeť	k1gMnSc4	zeť
Fridricha	Fridrich	k1gMnSc4	Fridrich
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
těchto	tento	k3xDgInPc2	tento
bojů	boj	k1gInPc2	boj
nezúčastnil	zúčastnit	k5eNaPmAgMnS	zúčastnit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
jeho	jeho	k3xOp3gMnSc1	jeho
následník	následník	k1gMnSc1	následník
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
Stuart	Stuart	k1gInSc1	Stuart
nechal	nechat	k5eAaPmAgInS	nechat
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
a	a	k8xC	a
do	do	k7c2	do
války	válka	k1gFnSc2	válka
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
<g/>
.	.	kIx.	.
</s>
<s>
Kristián	Kristián	k1gMnSc1	Kristián
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Dánský	dánský	k2eAgMnSc1d1	dánský
byl	být	k5eAaImAgInS	být
luterán	luterán	k1gMnSc1	luterán
a	a	k8xC	a
obával	obávat	k5eAaImAgMnS	obávat
se	se	k3xPyFc4	se
sílící	sílící	k2eAgFnSc2d1	sílící
moci	moc	k1gFnSc2	moc
katolíků	katolík	k1gMnPc2	katolík
v	v	k7c6	v
sousední	sousední	k2eAgFnSc6d1	sousední
Svaté	svatý	k2eAgFnSc6d1	svatá
říši	říš	k1gFnSc6	říš
římské	římský	k2eAgFnPc4d1	římská
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
měl	mít	k5eAaImAgInS	mít
sám	sám	k3xTgInSc1	sám
rodinné	rodinný	k2eAgFnPc1d1	rodinná
vazby	vazba	k1gFnPc1	vazba
–	–	k?	–
byl	být	k5eAaImAgInS	být
vévodou	vévoda	k1gMnSc7	vévoda
holštýnským	holštýnský	k2eAgMnSc7d1	holštýnský
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
choť	choť	k1gFnSc1	choť
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
domu	dům	k1gInSc2	dům
vládců	vládce	k1gMnPc2	vládce
Braniborského	braniborský	k2eAgNnSc2d1	braniborské
markrabství	markrabství	k1gNnSc2	markrabství
a	a	k8xC	a
první	první	k4xOgMnSc1	první
syn	syn	k1gMnSc1	syn
byl	být	k5eAaImAgMnS	být
biskupem	biskup	k1gMnSc7	biskup
brémským	brémský	k2eAgMnSc7d1	brémský
<g/>
.	.	kIx.	.
</s>
<s>
Pomoc	pomoc	k1gFnSc1	pomoc
také	také	k9	také
přislíbila	přislíbit	k5eAaPmAgFnS	přislíbit
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Právě	právě	k9	právě
Kristián	Kristián	k1gMnSc1	Kristián
IV	IV	kA	IV
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
vojsk	vojsko	k1gNnPc2	vojsko
Haagské	haagský	k2eAgFnSc2d1	Haagská
koalice	koalice	k1gFnSc2	koalice
a	a	k8xC	a
také	také	k6eAd1	také
vůdcem	vůdce	k1gMnSc7	vůdce
sousedního	sousední	k2eAgNnSc2d1	sousední
luteránského	luteránský	k2eAgNnSc2d1	luteránské
Dolního	dolní	k2eAgNnSc2d1	dolní
Saska	Sasko	k1gNnSc2	Sasko
<g/>
.	.	kIx.	.
</s>
<s>
Volba	volba	k1gFnSc1	volba
Kristiána	Kristián	k1gMnSc2	Kristián
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
armády	armáda	k1gFnSc2	armáda
byla	být	k5eAaImAgFnS	být
sice	sice	k8xC	sice
poněkud	poněkud	k6eAd1	poněkud
překvapující	překvapující	k2eAgFnSc1d1	překvapující
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Dánsko	Dánsko	k1gNnSc1	Dánsko
až	až	k9	až
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
chvíle	chvíle	k1gFnSc2	chvíle
války	válka	k1gFnSc2	válka
prakticky	prakticky	k6eAd1	prakticky
nezúčastnilo	zúčastnit	k5eNaPmAgNnS	zúčastnit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
Kristián	Kristián	k1gMnSc1	Kristián
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
dosahoval	dosahovat	k5eAaImAgInS	dosahovat
znamenitých	znamenitý	k2eAgInPc2d1	znamenitý
výsledků	výsledek	k1gInPc2	výsledek
v	v	k7c6	v
reformách	reforma	k1gFnPc6	reforma
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
ekonomiky	ekonomika	k1gFnSc2	ekonomika
a	a	k8xC	a
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
tam	tam	k6eAd1	tam
nastalo	nastat	k5eAaPmAgNnS	nastat
období	období	k1gNnSc1	období
prosperity	prosperita	k1gFnSc2	prosperita
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
neexistovala	existovat	k5eNaImAgFnS	existovat
již	již	k6eAd1	již
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
dalším	další	k2eAgInSc6d1	další
evropském	evropský	k2eAgInSc6d1	evropský
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
výše	výše	k1gFnSc2	výše
zmíněných	zmíněný	k2eAgInPc2d1	zmíněný
států	stát	k1gInPc2	stát
sestavil	sestavit	k5eAaPmAgMnS	sestavit
Kristián	Kristián	k1gMnSc1	Kristián
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
tvořenou	tvořený	k2eAgFnSc4d1	tvořená
20	[number]	k4	20
000	[number]	k4	000
žoldnéři	žoldnéř	k1gMnPc7	žoldnéř
a	a	k8xC	a
15	[number]	k4	15
000	[number]	k4	000
národními	národní	k2eAgMnPc7d1	národní
dobrovolníky	dobrovolník	k1gMnPc7	dobrovolník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vstup	vstup	k1gInSc1	vstup
Albrechta	Albrecht	k1gMnSc2	Albrecht
a	a	k8xC	a
Kristiána	Kristián	k1gMnSc2	Kristián
do	do	k7c2	do
války	válka	k1gFnSc2	válka
====	====	k?	====
</s>
</p>
<p>
<s>
Císař	Císař	k1gMnSc1	Císař
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
proti	proti	k7c3	proti
koaliční	koaliční	k2eAgFnSc3d1	koaliční
armádě	armáda	k1gFnSc3	armáda
najal	najmout	k5eAaPmAgMnS	najmout
vojenského	vojenský	k2eAgMnSc4d1	vojenský
podnikatele	podnikatel	k1gMnSc4	podnikatel
Albrechta	Albrecht	k1gMnSc4	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejno	k1gNnSc2	Valdštejno
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
původně	původně	k6eAd1	původně
český	český	k2eAgMnSc1d1	český
protestantský	protestantský	k2eAgMnSc1d1	protestantský
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
ale	ale	k9	ale
po	po	k7c6	po
několika	několik	k4yIc6	několik
neshodách	neshoda	k1gFnPc6	neshoda
s	s	k7c7	s
českými	český	k2eAgInPc7d1	český
stavy	stav	k1gInPc7	stav
přidal	přidat	k5eAaPmAgInS	přidat
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
císaře	císař	k1gMnSc2	císař
i	i	k8xC	i
s	s	k7c7	s
odcizenou	odcizený	k2eAgFnSc7d1	odcizená
stavovskou	stavovský	k2eAgFnSc7d1	stavovská
pokladnou	pokladna	k1gFnSc7	pokladna
<g/>
,	,	kIx,	,
po	po	k7c6	po
staroměstské	staroměstský	k2eAgFnSc6d1	Staroměstská
exekuci	exekuce	k1gFnSc6	exekuce
se	se	k3xPyFc4	se
výrazně	výrazně	k6eAd1	výrazně
obohatil	obohatit	k5eAaPmAgMnS	obohatit
na	na	k7c6	na
konfiskátech	konfiskát	k1gInPc6	konfiskát
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejbohatších	bohatý	k2eAgMnPc2d3	nejbohatší
šlechticů	šlechtic	k1gMnPc2	šlechtic
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
Říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Bohatství	bohatství	k1gNnSc1	bohatství
mu	on	k3xPp3gMnSc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
vytvořit	vytvořit	k5eAaPmF	vytvořit
armádu	armáda	k1gFnSc4	armáda
z	z	k7c2	z
vlastních	vlastní	k2eAgInPc2d1	vlastní
zdrojů	zdroj	k1gInPc2	zdroj
čítající	čítající	k2eAgInSc4d1	čítající
od	od	k7c2	od
30	[number]	k4	30
000	[number]	k4	000
do	do	k7c2	do
100	[number]	k4	100
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
Valdštejn	Valdštejn	k1gMnSc1	Valdštejn
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
Ferdinandovi	Ferdinand	k1gMnSc3	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
za	za	k7c4	za
právo	právo	k1gNnSc4	právo
na	na	k7c6	na
získávání	získávání	k1gNnSc6	získávání
válečné	válečný	k2eAgFnSc2d1	válečná
kořisti	kořist	k1gFnSc2	kořist
z	z	k7c2	z
dobytých	dobytý	k2eAgNnPc2d1	dobyté
území	území	k1gNnPc2	území
<g/>
.	.	kIx.	.
<g/>
Kristián	Kristián	k1gMnSc1	Kristián
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
o	o	k7c6	o
Valdštejnově	Valdštejnův	k2eAgNnSc6d1	Valdštejnovo
válečném	válečný	k2eAgNnSc6d1	válečné
angažmá	angažmá	k1gNnSc6	angažmá
věděl	vědět	k5eAaImAgMnS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Sice	sice	k8xC	sice
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
postoupit	postoupit	k5eAaPmF	postoupit
přes	přes	k7c4	přes
Moravu	Morava	k1gFnSc4	Morava
a	a	k8xC	a
Slezsko	Slezsko	k1gNnSc4	Slezsko
až	až	k9	až
na	na	k7c6	na
území	území	k1gNnSc6	území
Uher	Uhry	k1gFnPc2	Uhry
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
sedmihradskému	sedmihradský	k2eAgMnSc3d1	sedmihradský
knížeti	kníže	k1gMnSc3	kníže
Gabrielu	Gabriel	k1gMnSc3	Gabriel
Betlenovi	Betlen	k1gMnSc3	Betlen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
musel	muset	k5eAaImAgMnS	muset
stáhnout	stáhnout	k5eAaPmF	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1626	[number]	k4	1626
byl	být	k5eAaImAgMnS	být
protestantský	protestantský	k2eAgMnSc1d1	protestantský
generál	generál	k1gMnSc1	generál
Mansfeld	Mansfeld	k1gMnSc1	Mansfeld
poražen	poražen	k2eAgMnSc1d1	poražen
Valdštejnem	Valdštejn	k1gMnSc7	Valdštejn
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Desavy	Desava	k1gFnSc2	Desava
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
Kristián	Kristián	k1gMnSc1	Kristián
byl	být	k5eAaImAgMnS	být
poražen	poražen	k2eAgInSc4d1	poražen
Tillym	Tillym	k1gInSc4	Tillym
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Lutteru	Lutter	k1gInSc2	Lutter
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
Kristiánově	Kristiánův	k2eAgFnSc3d1	Kristiánova
smůle	smůla	k1gFnSc3	smůla
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dvě	dva	k4xCgFnPc1	dva
největší	veliký	k2eAgFnPc1d3	veliký
mocnosti	mocnost	k1gFnPc1	mocnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
přislíbily	přislíbit	k5eAaPmAgFnP	přislíbit
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
nebyly	být	k5eNaImAgInP	být
na	na	k7c4	na
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
války	válka	k1gFnSc2	válka
připraveny	připravit	k5eAaPmNgFnP	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Anglie	Anglie	k1gFnSc1	Anglie
byla	být	k5eAaImAgFnS	být
vnitřně	vnitřně	k6eAd1	vnitřně
rozdrobená	rozdrobený	k2eAgFnSc1d1	rozdrobená
a	a	k8xC	a
slabá	slabý	k2eAgFnSc1d1	slabá
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
se	se	k3xPyFc4	se
zmítala	zmítat	k5eAaImAgFnS	zmítat
v	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
a	a	k8xC	a
Švédsko	Švédsko	k1gNnSc1	Švédsko
bojovalo	bojovat	k5eAaImAgNnS	bojovat
s	s	k7c7	s
Republikou	republika	k1gFnSc7	republika
obou	dva	k4xCgInPc2	dva
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
ranou	rána	k1gFnSc7	rána
pro	pro	k7c4	pro
koaliční	koaliční	k2eAgNnPc4d1	koaliční
vojska	vojsko	k1gNnPc4	vojsko
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1626	[number]	k4	1626
náhle	náhle	k6eAd1	náhle
v	v	k7c6	v
Sarajevu	Sarajevo	k1gNnSc6	Sarajevo
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Mansfeld	Mansfeld	k1gMnSc1	Mansfeld
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Benátek	Benátky	k1gFnPc2	Benátky
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yIgInPc7	který
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
dohodnout	dohodnout	k5eAaPmF	dohodnout
na	na	k7c6	na
obnovení	obnovení	k1gNnSc6	obnovení
bojů	boj	k1gInPc2	boj
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgNnPc2	tento
významných	významný	k2eAgNnPc2d1	významné
vítězství	vítězství	k1gNnPc2	vítězství
si	se	k3xPyFc3	se
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
mohl	moct	k5eAaImAgInS	moct
dovolit	dovolit	k5eAaPmF	dovolit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1627	[number]	k4	1627
oktrojovat	oktrojovat	k5eAaBmF	oktrojovat
Obnovené	obnovený	k2eAgNnSc4d1	obnovené
zřízení	zřízení	k1gNnSc4	zřízení
zemské	zemský	k2eAgNnSc4d1	zemské
pro	pro	k7c4	pro
země	zem	k1gFnPc4	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
změnilo	změnit	k5eAaPmAgNnS	změnit
stavovskou	stavovský	k2eAgFnSc4d1	stavovská
vládu	vláda	k1gFnSc4	vláda
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
zemích	zem	k1gFnPc6	zem
na	na	k7c4	na
vládu	vláda	k1gFnSc4	vláda
absolutistickou	absolutistický	k2eAgFnSc4d1	absolutistická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vytlačení	vytlačení	k1gNnSc1	vytlačení
Dánů	Dán	k1gMnPc2	Dán
zpět	zpět	k6eAd1	zpět
====	====	k?	====
</s>
</p>
<p>
<s>
Valdštejn	Valdštejn	k1gInSc1	Valdštejn
postupoval	postupovat	k5eAaImAgInS	postupovat
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
sever	sever	k1gInSc4	sever
a	a	k8xC	a
podrobil	podrobit	k5eAaPmAgMnS	podrobit
si	se	k3xPyFc3	se
Meklenbursko	Meklenbursko	k1gNnSc4	Meklenbursko
<g/>
,	,	kIx,	,
Pomořansko	Pomořansko	k1gNnSc4	Pomořansko
a	a	k8xC	a
také	také	k9	také
samotný	samotný	k2eAgInSc1d1	samotný
poloostrov	poloostrov	k1gInSc1	poloostrov
Jutsko	Jutsko	k1gNnSc1	Jutsko
<g/>
.	.	kIx.	.
</s>
<s>
Nedokázal	dokázat	k5eNaPmAgMnS	dokázat
však	však	k9	však
již	již	k6eAd1	již
dobýt	dobýt	k5eAaPmF	dobýt
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Dánska	Dánsko	k1gNnSc2	Dánsko
<g/>
,	,	kIx,	,
Kodaň	Kodaň	k1gFnSc1	Kodaň
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Sjæ	Sjæ	k1gFnSc2	Sjæ
<g/>
.	.	kIx.	.
</s>
<s>
Valdštejnovi	Valdštejnův	k2eAgMnPc1d1	Valdštejnův
chyběla	chybět	k5eAaImAgFnS	chybět
námořní	námořní	k2eAgFnSc1d1	námořní
flotila	flotila	k1gFnSc1	flotila
a	a	k8xC	a
žádné	žádný	k3yNgNnSc1	žádný
z	z	k7c2	z
hanzovních	hanzovní	k2eAgNnPc2d1	hanzovní
měst	město	k1gNnPc2	město
nebylo	být	k5eNaImAgNnS	být
ochotno	ochoten	k2eAgNnSc1d1	ochotno
umožnit	umožnit	k5eAaPmF	umožnit
výstavbu	výstavba	k1gFnSc4	výstavba
císařské	císařský	k2eAgFnSc2d1	císařská
flotily	flotila	k1gFnSc2	flotila
v	v	k7c6	v
Baltském	baltský	k2eAgNnSc6d1	Baltské
moři	moře	k1gNnSc6	moře
a	a	k8xC	a
pro	pro	k7c4	pro
španělskou	španělský	k2eAgFnSc4d1	španělská
flotilu	flotila	k1gFnSc4	flotila
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
ochotna	ochoten	k2eAgFnSc1d1	ochotna
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
příliš	příliš	k6eAd1	příliš
riskantní	riskantní	k2eAgInSc4d1	riskantní
Jutský	jutský	k2eAgInSc4d1	jutský
poloostrov	poloostrov	k1gInSc4	poloostrov
obeplout	obeplout	k5eAaPmF	obeplout
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
Valdštejn	Valdštejn	k1gMnSc1	Valdštejn
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
propojení	propojení	k1gNnSc4	propojení
Severního	severní	k2eAgNnSc2d1	severní
a	a	k8xC	a
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
příkopem	příkop	k1gInSc7	příkop
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
ale	ale	k9	ale
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
nad	nad	k7c4	nad
tehdejší	tehdejší	k2eAgFnPc4d1	tehdejší
možnosti	možnost	k1gFnPc4	možnost
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
pokusil	pokusit	k5eAaPmAgMnS	pokusit
dobýt	dobýt	k5eAaPmF	dobýt
přístavní	přístavní	k2eAgNnSc4d1	přístavní
město	město	k1gNnSc4	město
Stralsund	Stralsunda	k1gFnPc2	Stralsunda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
mělo	mít	k5eAaImAgNnS	mít
dostatečné	dostatečný	k2eAgNnSc1d1	dostatečné
vybavení	vybavení	k1gNnSc1	vybavení
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
loďstva	loďstvo	k1gNnSc2	loďstvo
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
ale	ale	k8xC	ale
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
snahy	snaha	k1gFnSc2	snaha
upustil	upustit	k5eAaPmAgMnS	upustit
<g/>
,	,	kIx,	,
možná	možná	k9	možná
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
ztráty	ztráta	k1gFnPc1	ztráta
při	při	k7c6	při
obléhání	obléhání	k1gNnSc6	obléhání
byly	být	k5eAaImAgInP	být
vyšší	vysoký	k2eAgInPc1d2	vyšší
než	než	k8xS	než
předpokládané	předpokládaný	k2eAgInPc1d1	předpokládaný
zisky	zisk	k1gInPc1	zisk
ze	z	k7c2	z
zbytku	zbytek	k1gInSc2	zbytek
Dánska	Dánsko	k1gNnSc2	Dánsko
a	a	k8xC	a
spokojil	spokojit	k5eAaPmAgMnS	spokojit
se	se	k3xPyFc4	se
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kristiána	Kristián	k1gMnSc4	Kristián
přiměl	přimět	k5eAaPmAgMnS	přimět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1629	[number]	k4	1629
podepsat	podepsat	k5eAaPmF	podepsat
lübecký	lübecký	k2eAgInSc4d1	lübecký
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
Kristián	Kristián	k1gMnSc1	Kristián
zavázal	zavázat	k5eAaPmAgMnS	zavázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebude	být	k5eNaImBp3nS	být
podporovat	podporovat	k5eAaImF	podporovat
severoněmecké	severoněmecký	k2eAgInPc4d1	severoněmecký
protestantské	protestantský	k2eAgInPc4d1	protestantský
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
ponechána	ponechán	k2eAgFnSc1d1	ponechána
vláda	vláda	k1gFnSc1	vláda
nad	nad	k7c7	nad
Dánskem	Dánsko	k1gNnSc7	Dánsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Císaře	Císař	k1gMnSc2	Císař
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
také	také	k9	také
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1629	[number]	k4	1629
přesvědčila	přesvědčit	k5eAaPmAgFnS	přesvědčit
Katolická	katolický	k2eAgFnSc1d1	katolická
liga	liga	k1gFnSc1	liga
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
podepsal	podepsat	k5eAaPmAgMnS	podepsat
restituční	restituční	k2eAgInSc4d1	restituční
edikt	edikt	k1gInSc4	edikt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zrušil	zrušit	k5eAaPmAgInS	zrušit
sekularizaci	sekularizace	k1gFnSc4	sekularizace
církevního	církevní	k2eAgInSc2d1	církevní
majetku	majetek	k1gInSc2	majetek
provedenou	provedený	k2eAgFnSc7d1	provedená
protestanty	protestant	k1gMnPc7	protestant
<g/>
,	,	kIx,	,
především	především	k9	především
luterány	luterán	k1gMnPc4	luterán
<g/>
,	,	kIx,	,
před	před	k7c7	před
augsburským	augsburský	k2eAgInSc7d1	augsburský
mírem	mír	k1gInSc7	mír
a	a	k8xC	a
týkal	týkat	k5eAaImAgMnS	týkat
se	se	k3xPyFc4	se
dvou	dva	k4xCgNnPc2	dva
arcibiskupství	arcibiskupství	k1gNnPc2	arcibiskupství
<g/>
,	,	kIx,	,
šestnácti	šestnáct	k4xCc2	šestnáct
biskupství	biskupství	k1gNnPc2	biskupství
a	a	k8xC	a
stovek	stovka	k1gFnPc2	stovka
klášterů	klášter	k1gInPc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
zemřel	zemřít	k5eAaPmAgMnS	zemřít
Gabriel	Gabriel	k1gMnSc1	Gabriel
Betlen	Betlna	k1gFnPc2	Betlna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
Valdštejnovi	Valdštejn	k1gMnSc3	Valdštejn
dokázalo	dokázat	k5eAaPmAgNnS	dokázat
ubránit	ubránit	k5eAaPmF	ubránit
pouze	pouze	k6eAd1	pouze
město	město	k1gNnSc4	město
Stralsund	Stralsunda	k1gFnPc2	Stralsunda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Švédská	švédský	k2eAgFnSc1d1	švédská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
1630	[number]	k4	1630
<g/>
–	–	k?	–
<g/>
1635	[number]	k4	1635
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Valdštejn	Valdštejn	k1gInSc1	Valdštejn
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
získává	získávat	k5eAaImIp3nS	získávat
velkou	velký	k2eAgFnSc4d1	velká
moc	moc	k1gFnSc4	moc
v	v	k7c6	v
Pobaltí	Pobaltí	k1gNnSc6	Pobaltí
<g/>
,	,	kIx,	,
varoval	varovat	k5eAaImAgMnS	varovat
císaře	císař	k1gMnSc4	císař
Ferdinanda	Ferdinand	k1gMnSc4	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
před	před	k7c7	před
vydáním	vydání	k1gNnSc7	vydání
restitučního	restituční	k2eAgInSc2d1	restituční
ediktu	edikt	k1gInSc2	edikt
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgMnS	obávat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
pobouřená	pobouřený	k2eAgNnPc1d1	pobouřené
protestantská	protestantský	k2eAgNnPc1d1	protestantské
knížata	kníže	k1gNnPc1	kníže
nespojila	spojit	k5eNaPmAgNnP	spojit
se	se	k3xPyFc4	se
švédským	švédský	k2eAgMnSc7d1	švédský
králem	král	k1gMnSc7	král
Gustavem	Gustav	k1gMnSc7	Gustav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Adolfem	Adolf	k1gMnSc7	Adolf
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dvoře	dvůr	k1gInSc6	dvůr
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
se	se	k3xPyFc4	se
ale	ale	k9	ale
Valdštejnovi	Valdštejnův	k2eAgMnPc1d1	Valdštejnův
nepřátelé	nepřítel	k1gMnPc1	nepřítel
snažili	snažit	k5eAaImAgMnP	snažit
císaře	císař	k1gMnSc4	císař
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
o	o	k7c4	o
nebezpečnosti	nebezpečnost	k1gFnPc4	nebezpečnost
veliké	veliký	k2eAgFnSc3d1	veliká
moci	moc	k1gFnSc3	moc
císařského	císařský	k2eAgMnSc2d1	císařský
vrchního	vrchní	k2eAgMnSc2d1	vrchní
velitele	velitel	k1gMnSc2	velitel
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kurfiřtském	kurfiřtský	k2eAgInSc6d1	kurfiřtský
sněmu	sněm	k1gInSc6	sněm
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1630	[number]	k4	1630
byl	být	k5eAaImAgMnS	být
Valdštejn	Valdštejn	k1gMnSc1	Valdštejn
zbaven	zbavit	k5eAaPmNgMnS	zbavit
velení	velení	k1gNnSc3	velení
císařské	císařský	k2eAgFnSc2d1	císařská
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
z	z	k7c2	z
jeho	jeho	k3xOp3gMnPc2	jeho
žoldnéřů	žoldnéř	k1gMnPc2	žoldnéř
byla	být	k5eAaImAgFnS	být
propuštěna	propustit	k5eAaPmNgFnS	propustit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
největší	veliký	k2eAgMnSc1d3	veliký
kritik	kritik	k1gMnSc1	kritik
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
zde	zde	k6eAd1	zde
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
především	především	k9	především
bavorský	bavorský	k2eAgMnSc1d1	bavorský
kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vzestup	vzestup	k1gInSc1	vzestup
Valdštejnovy	Valdštejnův	k2eAgFnSc2d1	Valdštejnova
moci	moc	k1gFnSc2	moc
nelibě	libě	k6eNd1	libě
nesl	nést	k5eAaImAgMnS	nést
<g/>
.	.	kIx.	.
</s>
<s>
Valdštejn	Valdštejn	k1gInSc1	Valdštejn
tuto	tento	k3xDgFnSc4	tento
zprávu	zpráva	k1gFnSc4	zpráva
přijal	přijmout	k5eAaPmAgMnS	přijmout
rezignovaně	rezignovaně	k6eAd1	rezignovaně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vstup	vstup	k1gInSc1	vstup
Gustava	Gustav	k1gMnSc2	Gustav
Adolfa	Adolf	k1gMnSc2	Adolf
do	do	k7c2	do
války	válka	k1gFnSc2	válka
====	====	k?	====
</s>
</p>
<p>
<s>
Ještě	ještě	k6eAd1	ještě
roku	rok	k1gInSc2	rok
1630	[number]	k4	1630
se	se	k3xPyFc4	se
Gustav	Gustav	k1gMnSc1	Gustav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
skutečně	skutečně	k6eAd1	skutečně
přidal	přidat	k5eAaPmAgMnS	přidat
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
protestantů	protestant	k1gMnPc2	protestant
a	a	k8xC	a
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
armádou	armáda	k1gFnSc7	armáda
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
okolo	okolo	k7c2	okolo
13	[number]	k4	13
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
se	se	k3xPyFc4	se
vylodil	vylodit	k5eAaPmAgInS	vylodit
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Usedom	Usedom	k1gInSc1	Usedom
(	(	kIx(	(
<g/>
Uznojem	Uznoj	k1gInSc7	Uznoj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
vycvičená	vycvičený	k2eAgFnSc1d1	vycvičená
švédská	švédský	k2eAgFnSc1d1	švédská
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
z	z	k7c2	z
nezvykle	zvykle	k6eNd1	zvykle
vysokého	vysoký	k2eAgInSc2d1	vysoký
počtu	počet	k1gInSc2	počet
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
a	a	k8xC	a
sedláků	sedlák	k1gMnPc2	sedlák
loajálních	loajální	k2eAgMnPc2d1	loajální
králi	král	k1gMnSc6	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krom	krom	k7c2	krom
spojenectví	spojenectví	k1gNnSc2	spojenectví
s	s	k7c7	s
říšskými	říšský	k2eAgNnPc7d1	říšské
knížaty	kníže	k1gNnPc7	kníže
získal	získat	k5eAaPmAgMnS	získat
Gustav	Gustav	k1gMnSc1	Gustav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
finanční	finanční	k2eAgFnSc4d1	finanční
pomoc	pomoc	k1gFnSc4	pomoc
také	také	k9	také
od	od	k7c2	od
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zavázala	zavázat	k5eAaPmAgFnS	zavázat
ve	v	k7c6	v
smlouvě	smlouva	k1gFnSc6	smlouva
z	z	k7c2	z
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1631	[number]	k4	1631
platit	platit	k5eAaImF	platit
mu	on	k3xPp3gMnSc3	on
400	[number]	k4	400
000	[number]	k4	000
říšských	říšský	k2eAgInPc2d1	říšský
tolarů	tolar	k1gInPc2	tolar
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
vytvořit	vytvořit	k5eAaPmF	vytvořit
a	a	k8xC	a
vydržovat	vydržovat	k5eAaImF	vydržovat
armádu	armáda	k1gFnSc4	armáda
čítající	čítající	k2eAgFnSc4d1	čítající
36	[number]	k4	36
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
smlouvu	smlouva	k1gFnSc4	smlouva
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
první	první	k4xOgMnSc1	první
ministr	ministr	k1gMnSc1	ministr
francouzského	francouzský	k2eAgMnSc2d1	francouzský
krále	král	k1gMnSc2	král
Ludvíka	Ludvík	k1gMnSc2	Ludvík
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kardinál	kardinál	k1gMnSc1	kardinál
Richelieu	Richelieu	k1gMnSc1	Richelieu
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
pomoc	pomoc	k1gFnSc4	pomoc
se	se	k3xPyFc4	se
mohl	moct	k5eAaImAgMnS	moct
Gustav	Gustav	k1gMnSc1	Gustav
II	II	kA	II
<g/>
.	.	kIx.	.
spolehnout	spolehnout	k5eAaPmF	spolehnout
od	od	k7c2	od
Nizozemských	nizozemský	k2eAgFnPc2d1	nizozemská
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1631	[number]	k4	1631
byla	být	k5eAaImAgFnS	být
sepsána	sepsán	k2eAgFnSc1d1	sepsána
tajná	tajný	k2eAgFnSc1d1	tajná
fontainebleuská	fontainebleuský	k2eAgFnSc1d1	fontainebleuský
smlouva	smlouva	k1gFnSc1	smlouva
mezi	mezi	k7c7	mezi
Bavorskem	Bavorsko	k1gNnSc7	Bavorsko
<g/>
,	,	kIx,	,
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Švédskem	Švédsko	k1gNnSc7	Švédsko
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
ale	ale	k9	ale
vzápětí	vzápětí	k6eAd1	vzápětí
znehodnocena	znehodnotit	k5eAaPmNgFnS	znehodnotit
útokem	útok	k1gInSc7	útok
Gustava	Gustav	k1gMnSc2	Gustav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Adolfa	Adolf	k1gMnSc2	Adolf
na	na	k7c4	na
Bavorsko	Bavorsko	k1gNnSc4	Bavorsko
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýraznějším	výrazný	k2eAgMnSc7d3	nejvýraznější
spojencem	spojenec	k1gMnSc7	spojenec
na	na	k7c6	na
území	území	k1gNnSc6	území
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Sasko	Sasko	k1gNnSc1	Sasko
<g/>
.	.	kIx.	.
</s>
<s>
Současný	současný	k2eAgMnSc1d1	současný
velitel	velitel	k1gMnSc1	velitel
císařských	císařský	k2eAgFnPc2d1	císařská
armád	armáda	k1gFnPc2	armáda
<g/>
,	,	kIx,	,
Tilly	Tilla	k1gFnSc2	Tilla
<g/>
,	,	kIx,	,
po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
zvolení	zvolení	k1gNnSc6	zvolení
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
vojsk	vojsko	k1gNnPc2	vojsko
dobyl	dobýt	k5eAaPmAgMnS	dobýt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1631	[number]	k4	1631
Magdeburg	Magdeburg	k1gInSc1	Magdeburg
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
přinejmenším	přinejmenším	k6eAd1	přinejmenším
20	[number]	k4	20
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
Magdeburská	magdeburský	k2eAgFnSc1d1	Magdeburská
svatba	svatba	k1gFnSc1	svatba
<g/>
"	"	kIx"	"
zařadila	zařadit	k5eAaPmAgFnS	zařadit
mezi	mezi	k7c4	mezi
nejkrvavější	krvavý	k2eAgInPc4d3	nejkrvavější
masakry	masakr	k1gInPc4	masakr
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Saský	saský	k2eAgMnSc1d1	saský
kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
Jan	Jan	k1gMnSc1	Jan
Jiří	Jiří	k1gMnSc1	Jiří
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgMnS	obávat
Tillyho	Tilly	k1gMnSc4	Tilly
útoku	útok	k1gInSc2	útok
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
spojil	spojit	k5eAaPmAgMnS	spojit
s	s	k7c7	s
Gustavem	Gustav	k1gMnSc7	Gustav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Adolfem	Adolf	k1gMnSc7	Adolf
a	a	k8xC	a
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
armádu	armáda	k1gFnSc4	armáda
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
42	[number]	k4	42
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
švédsko-saské	švédskoaský	k2eAgNnSc1d1	švédsko-saský
vojsko	vojsko	k1gNnSc1	vojsko
se	se	k3xPyFc4	se
s	s	k7c7	s
Tillym	Tillym	k1gInSc1	Tillym
střetlo	střetnout	k5eAaPmAgNnS	střetnout
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Breitenfeldu	Breitenfeld	k1gInSc2	Breitenfeld
(	(	kIx(	(
<g/>
nedaleko	nedaleko	k7c2	nedaleko
Lipska	Lipsko	k1gNnSc2	Lipsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
po	po	k7c6	po
pěti	pět	k4xCc6	pět
hodinách	hodina	k1gFnPc6	hodina
císařské	císařský	k2eAgInPc4d1	císařský
pluky	pluk	k1gInPc4	pluk
udolalo	udolat	k5eAaPmAgNnS	udolat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
vítězství	vítězství	k1gNnSc6	vítězství
si	se	k3xPyFc3	se
vybudoval	vybudovat	k5eAaPmAgMnS	vybudovat
Gustav	Gustav	k1gMnSc1	Gustav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
hlavní	hlavní	k2eAgInSc4d1	hlavní
stan	stan	k1gInSc4	stan
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
po	po	k7c4	po
Tillyho	Tilly	k1gMnSc4	Tilly
neúspěchu	neúspěch	k1gInSc2	neúspěch
znovu	znovu	k6eAd1	znovu
povolal	povolat	k5eAaPmAgMnS	povolat
Valdštejna	Valdštejn	k1gMnSc2	Valdštejn
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
tentokrát	tentokrát	k6eAd1	tentokrát
Valdštejn	Valdštejn	k1gMnSc1	Valdštejn
namítl	namítnout	k5eAaPmAgMnS	namítnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nemocný	nemocný	k2eAgMnSc1d1	nemocný
a	a	k8xC	a
již	již	k6eAd1	již
se	se	k3xPyFc4	se
nechce	chtít	k5eNaImIp3nS	chtít
boje	boj	k1gInSc2	boj
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Saský	saský	k2eAgInSc4d1	saský
vpád	vpád	k1gInSc4	vpád
do	do	k7c2	do
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
====	====	k?	====
</s>
</p>
<p>
<s>
Po	po	k7c6	po
výhrách	výhra	k1gFnPc6	výhra
nad	nad	k7c7	nad
císařskou	císařský	k2eAgFnSc7d1	císařská
armádou	armáda	k1gFnSc7	armáda
vtrhla	vtrhnout	k5eAaPmAgFnS	vtrhnout
švédská	švédský	k2eAgFnSc1d1	švédská
a	a	k8xC	a
saská	saský	k2eAgFnSc1d1	saská
armáda	armáda	k1gFnSc1	armáda
do	do	k7c2	do
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
a	a	k8xC	a
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
začala	začít	k5eAaPmAgFnS	začít
rabovat	rabovat	k5eAaImF	rabovat
pražské	pražský	k2eAgInPc4d1	pražský
domy	dům	k1gInPc4	dům
<g/>
;	;	kIx,	;
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
nimi	on	k3xPp3gFnPc7	on
se	se	k3xPyFc4	se
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
vraceli	vracet	k5eAaImAgMnP	vracet
i	i	k9	i
někteří	některý	k3yIgMnPc1	některý
čeští	český	k2eAgMnPc1d1	český
exulanti	exulant	k1gMnPc1	exulant
(	(	kIx(	(
<g/>
především	především	k9	především
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
šlechty	šlechta	k1gFnSc2	šlechta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
začali	začít	k5eAaPmAgMnP	začít
domáhat	domáhat	k5eAaImF	domáhat
restituce	restituce	k1gFnPc4	restituce
svých	svůj	k3xOyFgInPc2	svůj
majetků	majetek	k1gInPc2	majetek
zabavených	zabavený	k2eAgInPc2d1	zabavený
po	po	k7c6	po
Bílé	bílý	k2eAgFnSc6d1	bílá
hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
Valdštejn	Valdštejn	k1gMnSc1	Valdštejn
<g/>
,	,	kIx,	,
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
Ferdinandovi	Ferdinand	k1gMnSc3	Ferdinand
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jenom	jenom	k9	jenom
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
měsíce	měsíc	k1gInPc4	měsíc
<g/>
,	,	kIx,	,
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1631	[number]	k4	1631
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
dubna	duben	k1gInSc2	duben
1632	[number]	k4	1632
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
tetanus	tetanus	k1gInSc4	tetanus
vrchní	vrchní	k2eAgMnSc1d1	vrchní
velitel	velitel	k1gMnSc1	velitel
ligistické	ligistický	k2eAgFnSc2d1	ligistická
armády	armáda	k1gFnSc2	armáda
Jan	Jan	k1gMnSc1	Jan
Tserclaes	Tserclaes	k1gMnSc1	Tserclaes
Tilly	Tilla	k1gFnSc2	Tilla
a	a	k8xC	a
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
novou	nový	k2eAgFnSc7d1	nová
armádou	armáda	k1gFnSc7	armáda
Valdštejn	Valdštejn	k1gInSc1	Valdštejn
přes	přes	k7c4	přes
Znojmo	Znojmo	k1gNnSc4	Znojmo
do	do	k7c2	do
západních	západní	k2eAgFnPc2d1	západní
Čech	Čechy	k1gFnPc2	Čechy
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odřízl	odříznout	k5eAaPmAgMnS	odříznout
Gustava	Gustav	k1gMnSc4	Gustav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Adolfa	Adolf	k1gMnSc4	Adolf
od	od	k7c2	od
zásobování	zásobování	k1gNnSc2	zásobování
<g/>
.	.	kIx.	.
</s>
<s>
Gustav	Gustav	k1gMnSc1	Gustav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
střetl	střetnout	k5eAaPmAgMnS	střetnout
s	s	k7c7	s
Maxmiliánem	Maxmilián	k1gMnSc7	Maxmilián
u	u	k7c2	u
Donauwörthu	Donauwörth	k1gInSc2	Donauwörth
a	a	k8xC	a
porazil	porazit	k5eAaPmAgMnS	porazit
ho	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
Mnichov	Mnichov	k1gInSc4	Mnichov
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
posléze	posléze	k6eAd1	posléze
Gustavovými	Gustavův	k2eAgInPc7d1	Gustavův
prapory	prapor	k1gInPc7	prapor
zpustošen	zpustošit	k5eAaPmNgInS	zpustošit
<g/>
.	.	kIx.	.
</s>
<s>
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
musel	muset	k5eAaImAgMnS	muset
opustit	opustit	k5eAaPmF	opustit
Bavorsko	Bavorsko	k1gNnSc4	Bavorsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Valdštejn	Valdštejn	k1gMnSc1	Valdštejn
začal	začít	k5eAaPmAgMnS	začít
jednat	jednat	k5eAaImF	jednat
se	s	k7c7	s
saským	saský	k2eAgMnSc7d1	saský
polním	polní	k2eAgMnSc7d1	polní
maršálkem	maršálek	k1gMnSc7	maršálek
<g/>
,	,	kIx,	,
Janem	Jan	k1gMnSc7	Jan
Jiřím	Jiří	k1gMnSc7	Jiří
z	z	k7c2	z
Arnimu	Arnim	k1gInSc2	Arnim
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
slíbil	slíbit	k5eAaPmAgInS	slíbit
zrušení	zrušení	k1gNnSc4	zrušení
restitučního	restituční	k2eAgInSc2d1	restituční
ediktu	edikt	k1gInSc2	edikt
a	a	k8xC	a
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
toleranci	tolerance	k1gFnSc4	tolerance
v	v	k7c6	v
Říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Arnim	Arnim	k1gMnSc1	Arnim
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
Valdštejnovu	Valdštejnův	k2eAgFnSc4d1	Valdštejnova
nabídku	nabídka	k1gFnSc4	nabídka
a	a	k8xC	a
obrátil	obrátit	k5eAaPmAgMnS	obrátit
se	se	k3xPyFc4	se
s	s	k7c7	s
prosbou	prosba	k1gFnSc7	prosba
o	o	k7c4	o
pomoc	pomoc	k1gFnSc4	pomoc
na	na	k7c4	na
švédského	švédský	k2eAgMnSc4d1	švédský
krále	král	k1gMnSc4	král
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
správně	správně	k6eAd1	správně
soudil	soudit	k5eAaImAgMnS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Valdštejn	Valdštejn	k1gNnSc1	Valdštejn
po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
odmítnutí	odmítnutí	k1gNnSc6	odmítnutí
zaútočí	zaútočit	k5eAaPmIp3nS	zaútočit
na	na	k7c4	na
saská	saský	k2eAgNnPc4d1	Saské
vojska	vojsko	k1gNnPc4	vojsko
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Valdštejn	Valdštejn	k1gMnSc1	Valdštejn
skutečně	skutečně	k6eAd1	skutečně
vtrhl	vtrhnout	k5eAaPmAgMnS	vtrhnout
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
obsadil	obsadit	k5eAaPmAgMnS	obsadit
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
Prahu	práh	k1gInSc2	práh
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
Prahy	Praha	k1gFnSc2	Praha
museli	muset	k5eAaImAgMnP	muset
čeští	český	k2eAgMnPc1d1	český
stavové	stavový	k2eAgNnSc4d1	stavové
společně	společně	k6eAd1	společně
se	s	k7c7	s
saskou	saský	k2eAgFnSc7d1	saská
armádou	armáda	k1gFnSc7	armáda
Čechy	Čech	k1gMnPc4	Čech
opustit	opustit	k5eAaPmF	opustit
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
konfiskacím	konfiskace	k1gFnPc3	konfiskace
<g/>
.	.	kIx.	.
<g/>
Gustav	Gustav	k1gMnSc1	Gustav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
rozdělit	rozdělit	k5eAaPmF	rozdělit
svoji	svůj	k3xOyFgFnSc4	svůj
armádu	armáda	k1gFnSc4	armáda
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
část	část	k1gFnSc1	část
mužů	muž	k1gMnPc2	muž
zůstala	zůstat	k5eAaPmAgFnS	zůstat
v	v	k7c6	v
Bavorsku	Bavorsko	k1gNnSc6	Bavorsko
a	a	k8xC	a
Gustav	Gustav	k1gMnSc1	Gustav
sám	sám	k3xTgMnSc1	sám
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
18	[number]	k4	18
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
se	se	k3xPyFc4	se
vypravil	vypravit	k5eAaPmAgInS	vypravit
do	do	k7c2	do
Norimberka	Norimberk	k1gInSc2	Norimberk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
mínil	mínit	k5eAaImAgMnS	mínit
spojit	spojit	k5eAaPmF	spojit
s	s	k7c7	s
armádou	armáda	k1gFnSc7	armáda
švédského	švédský	k2eAgMnSc2d1	švédský
kancléře	kancléř	k1gMnSc2	kancléř
Axela	Axela	k1gFnSc1	Axela
Oxenstierna	Oxenstierna	k1gFnSc1	Oxenstierna
<g/>
,	,	kIx,	,
čítající	čítající	k2eAgFnSc1d1	čítající
okolo	okolo	k7c2	okolo
30	[number]	k4	30
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
Valdštejnovi	Valdštejn	k1gMnSc3	Valdštejn
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
táhl	táhnout	k5eAaImAgMnS	táhnout
do	do	k7c2	do
Saska	Sasko	k1gNnSc2	Sasko
<g/>
.	.	kIx.	.
</s>
<s>
Gustavovi	Gustav	k1gMnSc3	Gustav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Adolfovi	Adolf	k1gMnSc3	Adolf
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
nedařilo	dařit	k5eNaImAgNnS	dařit
zastihnout	zastihnout	k5eAaPmF	zastihnout
Valdštejna	Valdštejn	k1gMnSc4	Valdštejn
a	a	k8xC	a
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
mu	on	k3xPp3gInSc3	on
až	až	k9	až
jedna	jeden	k4xCgFnSc1	jeden
třetina	třetina	k1gFnSc1	třetina
vojska	vojsko	k1gNnSc2	vojsko
zemřela	zemřít	k5eAaPmAgFnS	zemřít
a	a	k8xC	a
dezertovala	dezertovat	k5eAaBmAgFnS	dezertovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Lützenu	Lützen	k2eAgFnSc4d1	Lützen
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
již	již	k6eAd1	již
začínala	začínat	k5eAaImAgFnS	začínat
tuhá	tuhý	k2eAgFnSc1d1	tuhá
zima	zima	k1gFnSc1	zima
<g/>
,	,	kIx,	,
Valdštejn	Valdštejn	k1gInSc1	Valdštejn
již	již	k6eAd1	již
nepředpokládal	předpokládat	k5eNaImAgInS	předpokládat
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
útok	útok	k1gInSc4	útok
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
začal	začít	k5eAaPmAgMnS	začít
stahovat	stahovat	k5eAaImF	stahovat
svoji	svůj	k3xOyFgFnSc4	svůj
armádu	armáda	k1gFnSc4	armáda
na	na	k7c4	na
přezimování	přezimování	k1gNnSc4	přezimování
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Lipska	Lipsko	k1gNnSc2	Lipsko
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgMnSc4	ten
však	však	k9	však
chtěl	chtít	k5eAaImAgMnS	chtít
využít	využít	k5eAaPmF	využít
Gustav	Gustav	k1gMnSc1	Gustav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
a	a	k8xC	a
táhl	táhnout	k5eAaImAgMnS	táhnout
na	na	k7c4	na
oslabenou	oslabený	k2eAgFnSc4d1	oslabená
Valdštejnovu	Valdštejnův	k2eAgFnSc4d1	Valdštejnova
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
tahu	tah	k1gInSc6	tah
se	se	k3xPyFc4	se
ale	ale	k9	ale
Valdštejn	Valdštejn	k1gMnSc1	Valdštejn
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
v	v	k7c4	v
Lützenu	Lützen	k2eAgFnSc4d1	Lützen
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
poslal	poslat	k5eAaPmAgMnS	poslat
pro	pro	k7c4	pro
zbytek	zbytek	k1gInSc4	zbytek
armády	armáda	k1gFnSc2	armáda
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Gottfrieda	Gottfried	k1gMnSc2	Gottfried
Heinricha	Heinrich	k1gMnSc2	Heinrich
Pappenheima	Pappenheim	k1gMnSc2	Pappenheim
<g/>
,	,	kIx,	,
o	o	k7c4	o
kterou	který	k3yQgFnSc4	který
byl	být	k5eAaImAgInS	být
oslaben	oslaben	k2eAgInSc1d1	oslaben
<g/>
.	.	kIx.	.
</s>
<s>
Pappenheim	Pappenheim	k1gInSc1	Pappenheim
dorazil	dorazit	k5eAaPmAgInS	dorazit
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1632	[number]	k4	1632
ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
do	do	k7c2	do
prvních	první	k4xOgInPc2	první
bojů	boj	k1gInPc2	boj
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Lützenu	Lützen	k2eAgFnSc4d1	Lützen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
bitvě	bitva	k1gFnSc6	bitva
padlo	padnout	k5eAaImAgNnS	padnout
mnoho	mnoho	k4c1	mnoho
mužů	muž	k1gMnPc2	muž
z	z	k7c2	z
Valdštejnovy	Valdštejnův	k2eAgFnSc2d1	Valdštejnova
armády	armáda	k1gFnSc2	armáda
včetně	včetně	k7c2	včetně
Pappenheima	Pappenheimum	k1gNnSc2	Pappenheimum
a	a	k8xC	a
Valdštejn	Valdštejn	k1gMnSc1	Valdštejn
sám	sám	k3xTgMnSc1	sám
byl	být	k5eAaImAgMnS	být
zraněn	zranit	k5eAaPmNgMnS	zranit
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
ale	ale	k9	ale
nemluví	mluvit	k5eNaImIp3nS	mluvit
o	o	k7c6	o
švédském	švédský	k2eAgNnSc6d1	švédské
vítězství	vítězství	k1gNnSc6	vítězství
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
bitvě	bitva	k1gFnSc6	bitva
padl	padnout	k5eAaPmAgMnS	padnout
i	i	k9	i
sám	sám	k3xTgMnSc1	sám
Gustav	Gustav	k1gMnSc1	Gustav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mělo	mít	k5eAaImAgNnS	mít
neblahý	blahý	k2eNgInSc4d1	neblahý
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
morálku	morálka	k1gFnSc4	morálka
švédské	švédský	k2eAgFnSc2d1	švédská
armády	armáda	k1gFnSc2	armáda
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
bojích	boj	k1gInPc6	boj
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Valdštejn	Valdštejn	k1gInSc1	Valdštejn
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
konání	konání	k1gNnSc4	konání
u	u	k7c2	u
Lützenu	Lützen	k2eAgFnSc4d1	Lützen
dostal	dostat	k5eAaPmAgMnS	dostat
pochvalu	pochvala	k1gFnSc4	pochvala
od	od	k7c2	od
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
za	za	k7c4	za
nezodpovědné	zodpovědný	k2eNgNnSc4d1	nezodpovědné
chování	chování	k1gNnSc4	chování
zbytku	zbytek	k1gInSc2	zbytek
císařské	císařský	k2eAgFnSc2d1	císařská
armády	armáda	k1gFnSc2	armáda
padly	padnout	k5eAaImAgFnP	padnout
i	i	k9	i
rozsudky	rozsudek	k1gInPc4	rozsudek
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
převzala	převzít	k5eAaPmAgFnS	převzít
moc	moc	k6eAd1	moc
říšská	říšský	k2eAgFnSc1d1	říšská
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zastupovala	zastupovat	k5eAaImAgFnS	zastupovat
Gustavovu	Gustavův	k2eAgFnSc4d1	Gustavova
nezletilou	zletilý	k2eNgFnSc4d1	nezletilá
dceru	dcera	k1gFnSc4	dcera
Kristýnu	Kristýna	k1gFnSc4	Kristýna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
prohrách	prohra	k1gFnPc6	prohra
bylo	být	k5eAaImAgNnS	být
švédské	švédský	k2eAgNnSc1d1	švédské
vojsko	vojsko	k1gNnSc1	vojsko
staženo	stáhnout	k5eAaPmNgNnS	stáhnout
a	a	k8xC	a
nahrazeno	nahradit	k5eAaPmNgNnS	nahradit
žoldáckým	žoldácký	k2eAgNnSc7d1	žoldácké
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Švédský	švédský	k2eAgMnSc1d1	švédský
kancléř	kancléř	k1gMnSc1	kancléř
Oxenstierna	Oxenstierna	k1gFnSc1	Oxenstierna
vyjednával	vyjednávat	k5eAaImAgMnS	vyjednávat
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
německými	německý	k2eAgMnPc7d1	německý
knížaty	kníže	k1gMnPc7wR	kníže
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Matyáš	Matyáš	k1gMnSc1	Matyáš
Thurn	Thurn	k1gMnSc1	Thurn
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
českými	český	k2eAgInPc7d1	český
stavy	stav	k1gInPc7	stav
začal	začít	k5eAaPmAgMnS	začít
jednat	jednat	k5eAaImF	jednat
s	s	k7c7	s
Valdštejnem	Valdštejno	k1gNnSc7	Valdštejno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Valdštejn	Valdštejn	k1gNnSc1	Valdštejn
ale	ale	k8xC	ale
ani	ani	k8xC	ani
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
návrhů	návrh	k1gInPc2	návrh
neodpověděl	odpovědět	k5eNaPmAgMnS	odpovědět
a	a	k8xC	a
veškerá	veškerý	k3xTgNnPc4	veškerý
jednání	jednání	k1gNnPc4	jednání
před	před	k7c7	před
císařským	císařský	k2eAgInSc7d1	císařský
dvorem	dvůr	k1gInSc7	dvůr
popřel	popřít	k5eAaPmAgMnS	popřít
<g/>
.	.	kIx.	.
</s>
<s>
Valdštejn	Valdštejn	k1gMnSc1	Valdštejn
nicméně	nicméně	k8xC	nicméně
začal	začít	k5eAaPmAgMnS	začít
opět	opět	k6eAd1	opět
vyjednávat	vyjednávat	k5eAaImF	vyjednávat
se	s	k7c7	s
saskou	saský	k2eAgFnSc7d1	saská
armádou	armáda	k1gFnSc7	armáda
o	o	k7c6	o
společné	společný	k2eAgFnSc6d1	společná
intervenci	intervence	k1gFnSc6	intervence
proti	proti	k7c3	proti
notně	notně	k6eAd1	notně
oslabeným	oslabený	k2eAgNnPc3d1	oslabené
žoldáckým	žoldácký	k2eAgNnPc3d1	žoldácké
švédským	švédský	k2eAgNnPc3d1	švédské
vojskům	vojsko	k1gNnPc3	vojsko
ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
závažnosti	závažnost	k1gFnSc3	závažnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
zvláštnosti	zvláštnost	k1gFnPc4	zvláštnost
návrhu	návrh	k1gInSc2	návrh
vyslal	vyslat	k5eAaPmAgMnS	vyslat
Arnim	Arnim	k1gInSc4	Arnim
za	za	k7c7	za
Valdštejnem	Valdštejno	k1gNnSc7	Valdštejno
svého	svůj	k3xOyFgMnSc2	svůj
maršála	maršál	k1gMnSc2	maršál
Františka	František	k1gMnSc2	František
Albrechta	Albrecht	k1gMnSc2	Albrecht
z	z	k7c2	z
Lauenbergu	Lauenberg	k1gInSc2	Lauenberg
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Valdštejn	Valdštejn	k1gMnSc1	Valdštejn
chce	chtít	k5eAaImIp3nS	chtít
vyhnat	vyhnat	k5eAaPmF	vyhnat
nejen	nejen	k6eAd1	nejen
Švédy	švéda	k1gFnPc4	švéda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
Francouze	Francouz	k1gMnPc4	Francouz
a	a	k8xC	a
Španěly	Španěl	k1gMnPc4	Španěl
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
výsledkem	výsledek	k1gInSc7	výsledek
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
jednání	jednání	k1gNnSc2	jednání
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Arnim	Arnim	k1gInSc1	Arnim
odtáhl	odtáhnout	k5eAaPmAgInS	odtáhnout
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
armádou	armáda	k1gFnSc7	armáda
ze	z	k7c2	z
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
poskytlo	poskytnout	k5eAaPmAgNnS	poskytnout
Valdštejnovi	Valdštejnův	k2eAgMnPc1d1	Valdštejnův
příležitost	příležitost	k1gFnSc4	příležitost
k	k	k7c3	k
obsazení	obsazení	k1gNnSc3	obsazení
Stínavy	Stínava	k1gFnSc2	Stínava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
čekala	čekat	k5eAaImAgFnS	čekat
švédská	švédský	k2eAgFnSc1d1	švédská
armáda	armáda	k1gFnSc1	armáda
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
okolo	okolo	k7c2	okolo
6	[number]	k4	6
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Thurna	Thurno	k1gNnSc2	Thurno
<g/>
.	.	kIx.	.
</s>
<s>
Valdštejn	Valdštejn	k1gInSc1	Valdštejn
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
silnější	silný	k2eAgFnSc3d2	silnější
armádě	armáda	k1gFnSc3	armáda
čítající	čítající	k2eAgFnSc1d1	čítající
30	[number]	k4	30
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
rychlému	rychlý	k2eAgNnSc3d1	rychlé
obklíčení	obklíčení	k1gNnSc3	obklíčení
záhy	záhy	k6eAd1	záhy
přinutil	přinutit	k5eAaPmAgInS	přinutit
Thurna	Thurno	k1gNnPc4	Thurno
ke	k	k7c3	k
kapitulaci	kapitulace	k1gFnSc3	kapitulace
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
Thurnových	Thurnův	k2eAgMnPc2d1	Thurnův
žoldnéřů	žoldnéř	k1gMnPc2	žoldnéř
přešla	přejít	k5eAaPmAgFnS	přejít
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
získal	získat	k5eAaPmAgInS	získat
všechny	všechen	k3xTgFnPc4	všechen
pevnosti	pevnost	k1gFnPc4	pevnost
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
jako	jako	k8xS	jako
kompenzaci	kompenzace	k1gFnSc6	kompenzace
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
Thurn	Thurn	k1gInSc1	Thurn
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
švédských	švédský	k2eAgMnPc2d1	švédský
důstojníků	důstojník	k1gMnPc2	důstojník
byla	být	k5eAaImAgFnS	být
propuštěna	propustit	k5eAaPmNgFnS	propustit
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
o	o	k7c6	o
vítězství	vítězství	k1gNnSc6	vítězství
informoval	informovat	k5eAaBmAgMnS	informovat
císaře	císař	k1gMnPc4	císař
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
kontaktoval	kontaktovat	k5eAaImAgMnS	kontaktovat
Lauenberga	Lauenberg	k1gMnSc4	Lauenberg
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
opět	opět	k6eAd1	opět
jeho	jeho	k3xOp3gFnSc4	jeho
nabídku	nabídka	k1gFnSc4	nabídka
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Pád	Pád	k1gInSc1	Pád
Řezna	Řezno	k1gNnSc2	Řezno
a	a	k8xC	a
Valdštejnova	Valdštejnův	k2eAgFnSc1d1	Valdštejnova
smrt	smrt	k1gFnSc1	smrt
====	====	k?	====
</s>
</p>
<p>
<s>
Valdštejn	Valdštejn	k1gMnSc1	Valdštejn
uspořádal	uspořádat	k5eAaPmAgMnS	uspořádat
tažení	tažení	k1gNnSc4	tažení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
Čechy	Čechy	k1gFnPc4	Čechy
chránil	chránit	k5eAaImAgMnS	chránit
před	před	k7c7	před
vpádem	vpád	k1gInSc7	vpád
švédských	švédský	k2eAgNnPc2d1	švédské
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
minul	minout	k5eAaImAgMnS	minout
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Švédskému	švédský	k2eAgMnSc3d1	švédský
vrchnímu	vrchní	k1gMnSc3	vrchní
veliteli	velitel	k1gMnSc3	velitel
Bernardu	Bernard	k1gMnSc3	Bernard
Sasko-Výmarskému	saskoýmarský	k2eAgMnSc3d1	sasko-výmarský
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
14	[number]	k4	14
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1633	[number]	k4	1633
vzdalo	vzdát	k5eAaPmAgNnS	vzdát
Řezno	Řezno	k1gNnSc1	Řezno
a	a	k8xC	a
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
žádal	žádat	k5eAaImAgMnS	žádat
o	o	k7c4	o
okamžitou	okamžitý	k2eAgFnSc4d1	okamžitá
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Valdštejn	Valdštejn	k1gMnSc1	Valdštejn
už	už	k6eAd1	už
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
roku	rok	k1gInSc2	rok
nechtěl	chtít	k5eNaImAgMnS	chtít
cestovat	cestovat	k5eAaImF	cestovat
<g/>
,	,	kIx,	,
také	také	k9	také
kvůli	kvůli	k7c3	kvůli
svému	svůj	k3xOyFgInSc3	svůj
horšícímu	horšící	k2eAgInSc3d1	horšící
se	se	k3xPyFc4	se
zdravotnímu	zdravotní	k2eAgInSc3d1	zdravotní
stavu	stav	k1gInSc3	stav
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
přemluvit	přemluvit	k5eAaPmF	přemluvit
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tažení	tažení	k1gNnSc6	tažení
jel	jet	k5eAaImAgInS	jet
přes	přes	k7c4	přes
Plzeň	Plzeň	k1gFnSc4	Plzeň
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
opět	opět	k6eAd1	opět
stěžoval	stěžovat	k5eAaImAgMnS	stěžovat
na	na	k7c4	na
svoje	svůj	k3xOyFgNnSc4	svůj
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
císařský	císařský	k2eAgMnSc1d1	císařský
poradce	poradce	k1gMnSc1	poradce
Trautmannsdorf	Trautmannsdorf	k1gMnSc1	Trautmannsdorf
ho	on	k3xPp3gMnSc4	on
znovu	znovu	k6eAd1	znovu
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
o	o	k7c6	o
nutnosti	nutnost	k1gFnSc6	nutnost
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Valdštejnovi	Valdštejn	k1gMnSc3	Valdštejn
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
překonat	překonat	k5eAaPmF	překonat
Dunaj	Dunaj	k1gInSc4	Dunaj
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
do	do	k7c2	do
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Bavorský	bavorský	k2eAgMnSc1d1	bavorský
měl	mít	k5eAaImAgInS	mít
pochybnost	pochybnost	k1gFnSc4	pochybnost
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
za	za	k7c7	za
jeho	jeho	k3xOp3gInSc7	jeho
neúspěchem	neúspěch	k1gInSc7	neúspěch
stojí	stát	k5eAaImIp3nS	stát
skutečně	skutečně	k6eAd1	skutečně
jen	jen	k9	jen
špatné	špatný	k2eAgNnSc4d1	špatné
počasí	počasí	k1gNnSc4	počasí
a	a	k8xC	a
Valdštejnovo	Valdštejnův	k2eAgNnSc4d1	Valdštejnovo
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
na	na	k7c6	na
vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
dvoře	dvůr	k1gInSc6	dvůr
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
zvedla	zvednout	k5eAaPmAgFnS	zvednout
vlna	vlna	k1gFnSc1	vlna
odporu	odpor	k1gInSc2	odpor
proti	proti	k7c3	proti
Valdštejnovi	Valdštejn	k1gMnSc3	Valdštejn
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
1634	[number]	k4	1634
byl	být	k5eAaImAgMnS	být
Valdštejn	Valdštejn	k1gMnSc1	Valdštejn
zbaven	zbavit	k5eAaPmNgMnS	zbavit
vrchního	vrchní	k1gMnSc4	vrchní
velení	velení	k1gNnSc2	velení
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
zrádce	zrádce	k1gMnSc4	zrádce
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
tajně	tajně	k6eAd1	tajně
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
titul	titul	k1gInSc4	titul
získal	získat	k5eAaPmAgMnS	získat
Matyáš	Matyáš	k1gMnSc1	Matyáš
Gallas	Gallas	k1gMnSc1	Gallas
<g/>
.	.	kIx.	.
</s>
<s>
Valdštejn	Valdštejn	k1gMnSc1	Valdštejn
se	se	k3xPyFc4	se
přesunul	přesunout	k5eAaPmAgMnS	přesunout
přezimovat	přezimovat	k5eAaBmF	přezimovat
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
své	svůj	k3xOyFgFnSc2	svůj
armády	armáda	k1gFnSc2	armáda
do	do	k7c2	do
Chebu	Cheb	k1gInSc2	Cheb
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
císařův	císařův	k2eAgInSc4d1	císařův
pokyn	pokyn	k1gInSc4	pokyn
zavražděn	zavražděn	k2eAgInSc4d1	zavražděn
svými	svůj	k3xOyFgMnPc7	svůj
důstojníky	důstojník	k1gMnPc7	důstojník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Valdštejna	Valdštejno	k1gNnSc2	Valdštejno
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
císařské	císařský	k2eAgFnSc3d1	císařská
armádě	armáda	k1gFnSc3	armáda
dobýt	dobýt	k5eAaPmF	dobýt
zpět	zpět	k6eAd1	zpět
Řezno	Řezno	k1gNnSc4	Řezno
a	a	k8xC	a
Donauwörth	Donauwörth	k1gInSc4	Donauwörth
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
spojila	spojit	k5eAaPmAgFnS	spojit
s	s	k7c7	s
armádou	armáda	k1gFnSc7	armáda
španělskou	španělský	k2eAgFnSc7d1	španělská
a	a	k8xC	a
zbylé	zbylý	k2eAgInPc4d1	zbylý
švédské	švédský	k2eAgInPc4d1	švédský
oddíly	oddíl	k1gInPc4	oddíl
porazila	porazit	k5eAaPmAgFnS	porazit
v	v	k7c4	v
září	září	k1gNnSc4	září
1634	[number]	k4	1634
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Nördlingenu	Nördlingen	k1gInSc2	Nördlingen
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
takovémto	takovýto	k3xDgInSc6	takovýto
vývoji	vývoj	k1gInSc6	vývoj
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
saský	saský	k2eAgMnSc1d1	saský
kurfiřt	kurfiřt	k1gMnSc1	kurfiřt
Jan	Jan	k1gMnSc1	Jan
Jiří	Jiří	k1gMnSc1	Jiří
spojenectví	spojenectví	k1gNnPc2	spojenectví
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
bylo	být	k5eAaImAgNnS	být
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1635	[number]	k4	1635
spojenectví	spojenectví	k1gNnSc4	spojenectví
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
zaštítil	zaštítit	k5eAaPmAgInS	zaštítit
pražský	pražský	k2eAgInSc1d1	pražský
mír	mír	k1gInSc1	mír
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
přestoupení	přestoupení	k1gNnSc4	přestoupení
získali	získat	k5eAaPmAgMnP	získat
Sasové	Sas	k1gMnPc1	Sas
obě	dva	k4xCgFnPc4	dva
Lužice	Lužice	k1gFnPc4	Lužice
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
byla	být	k5eAaImAgFnS	být
pozastavena	pozastavit	k5eAaPmNgFnS	pozastavit
platnost	platnost	k1gFnSc1	platnost
restitučního	restituční	k2eAgInSc2d1	restituční
ediktu	edikt	k1gInSc2	edikt
na	na	k7c4	na
40	[number]	k4	40
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Švédsko-francouzská	švédskorancouzský	k2eAgFnSc1d1	švédsko-francouzská
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
1635	[number]	k4	1635
<g/>
–	–	k?	–
<g/>
1648	[number]	k4	1648
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Ač	ač	k8xS	ač
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
převládalo	převládat	k5eAaImAgNnS	převládat
římskokatolické	římskokatolický	k2eAgNnSc1d1	římskokatolické
vyznání	vyznání	k1gNnSc1	vyznání
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
podporovala	podporovat	k5eAaImAgFnS	podporovat
protestantskou	protestantský	k2eAgFnSc4d1	protestantská
stranu	strana	k1gFnSc4	strana
finančně	finančně	k6eAd1	finančně
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
obávala	obávat	k5eAaImAgFnS	obávat
velké	velká	k1gFnSc3	velká
mocí	moc	k1gFnPc2	moc
Habsburků	Habsburk	k1gMnPc2	Habsburk
<g/>
,	,	kIx,	,
především	především	k9	především
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
francouzských	francouzský	k2eAgFnPc2d1	francouzská
východních	východní	k2eAgFnPc2d1	východní
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvních	první	k4xOgNnPc2	první
let	léto	k1gNnPc2	léto
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
probíhala	probíhat	k5eAaImAgNnP	probíhat
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
hugenoty	hugenot	k1gMnPc7	hugenot
a	a	k8xC	a
katolickým	katolický	k2eAgMnSc7d1	katolický
králem	král	k1gMnSc7	král
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
jeho	jeho	k3xOp3gNnSc1	jeho
prvním	první	k4xOgMnSc7	první
ministrem	ministr	k1gMnSc7	ministr
<g/>
,	,	kIx,	,
kardinálem	kardinál	k1gMnSc7	kardinál
Richelieu	Richelieu	k1gMnSc7	Richelieu
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
Francie	Francie	k1gFnSc1	Francie
vojensky	vojensky	k6eAd1	vojensky
nezapojila	zapojit	k5eNaPmAgFnS	zapojit
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
,	,	kIx,	,
když	když	k8xS	když
padl	padnout	k5eAaPmAgInS	padnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1628	[number]	k4	1628
přístav	přístav	k1gInSc1	přístav
La	la	k1gNnSc2	la
Rochelle	Rochelle	k1gFnSc2	Rochelle
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vojenský	vojenský	k2eAgInSc1d1	vojenský
vstup	vstup	k1gInSc1	vstup
Francie	Francie	k1gFnSc2	Francie
do	do	k7c2	do
války	válka	k1gFnSc2	válka
====	====	k?	====
</s>
</p>
<p>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1628	[number]	k4	1628
se	se	k3xPyFc4	se
Francie	Francie	k1gFnSc1	Francie
spojila	spojit	k5eAaPmAgFnS	spojit
s	s	k7c7	s
Nizozemskými	nizozemský	k2eAgFnPc7d1	nizozemská
provinciemi	provincie	k1gFnPc7	provincie
a	a	k8xC	a
Švédskem	Švédsko	k1gNnSc7	Švédsko
a	a	k8xC	a
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
do	do	k7c2	do
války	válka	k1gFnSc2	válka
proti	proti	k7c3	proti
Španělsku	Španělsko	k1gNnSc3	Španělsko
(	(	kIx(	(
<g/>
tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
bojů	boj	k1gInPc2	boj
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
francouzsko-španělská	francouzsko-španělský	k2eAgFnSc1d1	francouzsko-španělská
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
Španělsko	Španělsko	k1gNnSc1	Španělsko
reagovalo	reagovat	k5eAaBmAgNnS	reagovat
vpádem	vpád	k1gInSc7	vpád
na	na	k7c4	na
území	území	k1gNnSc4	území
Francie	Francie	k1gFnSc2	Francie
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
císařského	císařský	k2eAgMnSc2d1	císařský
generála	generál	k1gMnSc2	generál
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Werthu	Werth	k1gInSc2	Werth
a	a	k8xC	a
kardinála	kardinál	k1gMnSc2	kardinál
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
<g/>
.	.	kIx.	.
</s>
<s>
Španělé	Španěl	k1gMnPc1	Španěl
se	se	k3xPyFc4	se
dostali	dostat	k5eAaPmAgMnP	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1636	[number]	k4	1636
do	do	k7c2	do
provincií	provincie	k1gFnPc2	provincie
Champagne	Champagn	k1gInSc5	Champagn
a	a	k8xC	a
Burgundy	Burgund	k1gMnPc4	Burgund
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
které	který	k3yIgInPc4	který
postoupili	postoupit	k5eAaPmAgMnP	postoupit
až	až	k6eAd1	až
k	k	k7c3	k
Paříži	Paříž	k1gFnSc3	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Odsud	odsud	k6eAd1	odsud
byli	být	k5eAaImAgMnP	být
vyhnáni	vyhnat	k5eAaPmNgMnP	vyhnat
Bernardem	Bernard	k1gMnSc7	Bernard
Sasko-Výmarským	saskoýmarský	k2eAgMnSc7d1	sasko-výmarský
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
porazil	porazit	k5eAaPmAgMnS	porazit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Compiè	Compiè	k1gFnSc2	Compiè
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1637	[number]	k4	1637
zemřel	zemřít	k5eAaPmAgMnS	zemřít
císař	císař	k1gMnSc1	císař
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Protihabsburské	protihabsburský	k2eAgFnPc1d1	protihabsburská
síly	síla	k1gFnPc1	síla
obsadily	obsadit	k5eAaPmAgFnP	obsadit
řadu	řada	k1gFnSc4	řada
pevností	pevnost	k1gFnSc7	pevnost
ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
říši	říš	k1gFnSc6	říš
u	u	k7c2	u
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
ztrátou	ztráta	k1gFnSc7	ztráta
byla	být	k5eAaImAgFnS	být
pevnost	pevnost	k1gFnSc1	pevnost
Breisach	Breisacha	k1gFnPc2	Breisacha
dobytá	dobytý	k2eAgFnSc1d1	dobytá
Francouzi	Francouz	k1gMnSc3	Francouz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1638	[number]	k4	1638
<g/>
,	,	kIx,	,
kudy	kudy	k6eAd1	kudy
mohli	moct	k5eAaImAgMnP	moct
dříve	dříve	k6eAd2	dříve
Španělé	Španěl	k1gMnPc1	Španěl
posílat	posílat	k5eAaImF	posílat
své	svůj	k3xOyFgFnPc4	svůj
posily	posila	k1gFnPc4	posila
z	z	k7c2	z
italských	italský	k2eAgFnPc2d1	italská
provincií	provincie	k1gFnPc2	provincie
do	do	k7c2	do
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
<g/>
.	.	kIx.	.
</s>
<s>
Španělské	španělský	k2eAgNnSc1d1	španělské
loďstvo	loďstvo	k1gNnSc1	loďstvo
také	také	k9	také
utrpělo	utrpět	k5eAaPmAgNnS	utrpět
porážku	porážka	k1gFnSc4	porážka
od	od	k7c2	od
Nizozemců	Nizozemec	k1gMnPc2	Nizozemec
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
téměř	téměř	k6eAd1	téměř
znemožnilo	znemožnit	k5eAaPmAgNnS	znemožnit
přesouvání	přesouvání	k1gNnSc1	přesouvání
španělských	španělský	k2eAgFnPc2d1	španělská
posil	posila	k1gFnPc2	posila
do	do	k7c2	do
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Oslabení	oslabení	k1gNnSc1	oslabení
Španělska	Španělsko	k1gNnSc2	Španělsko
využily	využít	k5eAaPmAgFnP	využít
Katalánie	Katalánie	k1gFnSc1	Katalánie
a	a	k8xC	a
Portugalské	portugalský	k2eAgNnSc1d1	portugalské
království	království	k1gNnSc1	království
a	a	k8xC	a
vzbouřily	vzbouřit	k5eAaPmAgInP	vzbouřit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Katalánií	Katalánie	k1gFnSc7	Katalánie
začal	začít	k5eAaPmAgMnS	začít
jednat	jednat	k5eAaImF	jednat
kardinál	kardinál	k1gMnSc1	kardinál
Richelieu	Richelieu	k1gMnSc1	Richelieu
a	a	k8xC	a
nabízel	nabízet	k5eAaImAgMnS	nabízet
<g/>
,	,	kIx,	,
že	že	k8xS	že
dosadí	dosadit	k5eAaPmIp3nS	dosadit
Ludvíka	Ludvík	k1gMnSc4	Ludvík
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
na	na	k7c4	na
její	její	k3xOp3gInSc4	její
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1640	[number]	k4	1640
od	od	k7c2	od
Španělska	Španělsko	k1gNnSc2	Španělsko
odtrhlo	odtrhnout	k5eAaPmAgNnS	odtrhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Smrt	smrt	k1gFnSc1	smrt
Richelieua	Richelieua	k1gFnSc1	Richelieua
a	a	k8xC	a
bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Rocroi	Rocro	k1gFnSc2	Rocro
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1639	[number]	k4	1639
byla	být	k5eAaImAgFnS	být
císařská	císařský	k2eAgFnSc1d1	císařská
armáda	armáda	k1gFnSc1	armáda
zatlačena	zatlačen	k2eAgFnSc1d1	zatlačena
až	až	k9	až
k	k	k7c3	k
Drážďanům	Drážďany	k1gInPc3	Drážďany
armádou	armáda	k1gFnSc7	armáda
švédskou	švédský	k2eAgFnSc7d1	švédská
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
se	se	k3xPyFc4	se
Švédskem	Švédsko	k1gNnSc7	Švédsko
začaly	začít	k5eAaPmAgFnP	začít
připravovat	připravovat	k5eAaImF	připravovat
mírovou	mírový	k2eAgFnSc4d1	mírová
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yRgFnPc4	který
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
stvrzeno	stvrzen	k2eAgNnSc1d1	stvrzeno
jejich	jejich	k3xOp3gNnSc4	jejich
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1642	[number]	k4	1642
zemřel	zemřít	k5eAaPmAgMnS	zemřít
kardinál	kardinál	k1gMnSc1	kardinál
Richelieu	Richelieu	k1gMnSc1	Richelieu
a	a	k8xC	a
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
prvního	první	k4xOgMnSc2	první
ministra	ministr	k1gMnSc2	ministr
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
kardinál	kardinál	k1gMnSc1	kardinál
Mazarin	Mazarin	k1gInSc4	Mazarin
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
zemřel	zemřít	k5eAaPmAgMnS	zemřít
také	také	k9	také
sám	sám	k3xTgMnSc1	sám
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
a	a	k8xC	a
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Ludvík	Ludvík	k1gMnSc1	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
zemřel	zemřít	k5eAaPmAgMnS	zemřít
i	i	k9	i
nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
švédský	švédský	k2eAgMnSc1d1	švédský
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
,	,	kIx,	,
Johan	Johan	k1gMnSc1	Johan
Banér	Banér	k1gMnSc1	Banér
<g/>
,	,	kIx,	,
a	a	k8xC	a
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
švédského	švédský	k2eAgNnSc2d1	švédské
vojska	vojsko	k1gNnSc2	vojsko
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
Lennart	Lennart	k1gInSc1	Lennart
Torstenson	Torstenson	k1gInSc1	Torstenson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1643	[number]	k4	1643
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozhodující	rozhodující	k2eAgFnSc3d1	rozhodující
bitvě	bitva	k1gFnSc3	bitva
španělských	španělský	k2eAgInPc2d1	španělský
a	a	k8xC	a
francouzských	francouzský	k2eAgNnPc2d1	francouzské
vojsk	vojsko	k1gNnPc2	vojsko
u	u	k7c2	u
Rocroi	Rocro	k1gFnSc2	Rocro
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mladičký	mladičký	k2eAgMnSc1d1	mladičký
Ludvík	Ludvík	k1gMnSc1	Ludvík
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Bourbon	bourbon	k1gInSc1	bourbon
<g/>
,	,	kIx,	,
vévoda	vévoda	k1gMnSc1	vévoda
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Enghien	Enghien	k1gInSc1	Enghien
porazil	porazit	k5eAaPmAgInS	porazit
španělsko-císařskou	španělskoísařský	k2eAgFnSc4d1	španělsko-císařský
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
složenou	složený	k2eAgFnSc4d1	složená
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
tradičních	tradiční	k2eAgFnPc2d1	tradiční
tercií	tercie	k1gFnPc2	tercie
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mu	on	k3xPp3gMnSc3	on
vyneslo	vynést	k5eAaPmAgNnS	vynést
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
slávu	sláva	k1gFnSc4	sláva
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
porážka	porážka	k1gFnSc1	porážka
také	také	k9	také
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
počátku	počátek	k1gInSc2	počátek
mírových	mírový	k2eAgNnPc2d1	Mírové
jednání	jednání	k1gNnPc2	jednání
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
započata	započat	k2eAgFnSc1d1	započata
roku	rok	k1gInSc2	rok
1644	[number]	k4	1644
v	v	k7c6	v
Münsteru	Münster	k1gInSc6	Münster
a	a	k8xC	a
Osnabrücku	Osnabrück	k1gInSc6	Osnabrück
<g/>
.	.	kIx.	.
</s>
<s>
Jednání	jednání	k1gNnSc4	jednání
nakonec	nakonec	k6eAd1	nakonec
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
podepsání	podepsání	k1gNnSc3	podepsání
vestfálského	vestfálský	k2eAgInSc2d1	vestfálský
míru	mír	k1gInSc2	mír
roku	rok	k1gInSc2	rok
1648	[number]	k4	1648
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nový	nový	k2eAgMnSc1d1	nový
velitel	velitel	k1gMnSc1	velitel
švédského	švédský	k2eAgNnSc2d1	švédské
vojska	vojsko	k1gNnSc2	vojsko
Torstenson	Torstenson	k1gNnSc1	Torstenson
zahájil	zahájit	k5eAaPmAgMnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1642	[number]	k4	1642
invazi	invaze	k1gFnSc6	invaze
přes	přes	k7c4	přes
Slezsko	Slezsko	k1gNnSc4	Slezsko
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
během	během	k7c2	během
které	který	k3yQgFnSc2	který
obsadil	obsadit	k5eAaPmAgMnS	obsadit
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Breitenfeldu	Breitenfeld	k1gInSc2	Breitenfeld
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1643	[number]	k4	1643
k	k	k7c3	k
Praze	Praha	k1gFnSc3	Praha
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
švédské	švédský	k2eAgNnSc1d1	švédské
vojsko	vojsko	k1gNnSc1	vojsko
odvoláno	odvolat	k5eAaPmNgNnS	odvolat
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1645	[number]	k4	1645
se	se	k3xPyFc4	se
střetla	střetnout	k5eAaPmAgFnS	střetnout
švédská	švédský	k2eAgFnSc1d1	švédská
a	a	k8xC	a
císařská	císařský	k2eAgNnPc1d1	císařské
vojska	vojsko	k1gNnPc1	vojsko
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Jankova	Jankův	k2eAgNnSc2d1	Jankovo
<g/>
,	,	kIx,	,
v	v	k7c4	v
které	který	k3yQgNnSc4	který
Torstenson	Torstenson	k1gNnSc4	Torstenson
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Tohoto	tento	k3xDgNnSc2	tento
vítězství	vítězství	k1gNnSc2	vítězství
chtěl	chtít	k5eAaImAgMnS	chtít
využít	využít	k5eAaPmF	využít
a	a	k8xC	a
napadnout	napadnout	k5eAaPmF	napadnout
Vídeň	Vídeň	k1gFnSc4	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tažení	tažení	k1gNnSc6	tažení
museli	muset	k5eAaImAgMnP	muset
Švédové	Švéd	k1gMnPc1	Švéd
překonat	překonat	k5eAaPmF	překonat
Brno	Brno	k1gNnSc4	Brno
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
však	však	k9	však
nepodařilo	podařit	k5eNaPmAgNnS	podařit
a	a	k8xC	a
od	od	k7c2	od
obléhaného	obléhaný	k2eAgNnSc2d1	obléhané
Brna	Brno	k1gNnSc2	Brno
se	se	k3xPyFc4	se
museli	muset	k5eAaImAgMnP	muset
stáhnout	stáhnout	k5eAaPmF	stáhnout
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
vrchního	vrchní	k2eAgMnSc2d1	vrchní
velitele	velitel	k1gMnSc2	velitel
švédských	švédský	k2eAgFnPc2d1	švédská
armád	armáda	k1gFnPc2	armáda
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Torstensona	Torstenson	k1gMnSc2	Torstenson
Carl	Carl	k1gMnSc1	Carl
Gustav	Gustav	k1gMnSc1	Gustav
Wrangel	Wrangel	k1gMnSc1	Wrangel
<g/>
.	.	kIx.	.
</s>
<s>
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
III	III	kA	III
<g/>
.	.	kIx.	.
nakonec	nakonec	k6eAd1	nakonec
podepsal	podepsat	k5eAaPmAgMnS	podepsat
separátní	separátní	k2eAgInSc4d1	separátní
mír	mír	k1gInSc4	mír
se	s	k7c7	s
sedmihradským	sedmihradský	k2eAgMnSc7d1	sedmihradský
knížetem	kníže	k1gMnSc7	kníže
Jiřím	Jiří	k1gMnSc7	Jiří
I.	I.	kA	I.
Rákoczym	Rákoczym	k1gInSc1	Rákoczym
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Švédům	Švéd	k1gMnPc3	Švéd
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
oddělili	oddělit	k5eAaPmAgMnP	oddělit
od	od	k7c2	od
hlavního	hlavní	k2eAgInSc2d1	hlavní
proudu	proud	k1gInSc2	proud
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
generála	generál	k1gMnSc2	generál
Hanse	Hans	k1gMnSc2	Hans
Christoffa	Christoff	k1gMnSc2	Christoff
Königsmarcka	Königsmarcka	k1gFnSc1	Königsmarcka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1648	[number]	k4	1648
nečekaným	čekaný	k2eNgInSc7d1	nečekaný
útokem	útok	k1gInSc7	útok
dobýt	dobýt	k5eAaPmF	dobýt
města	město	k1gNnPc4	město
Pražská	pražský	k2eAgNnPc4d1	Pražské
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
Vltavy	Vltava	k1gFnSc2	Vltava
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
přepadu	přepad	k1gInSc6	přepad
byly	být	k5eAaImAgFnP	být
uloupeny	uloupen	k2eAgFnPc1d1	uloupena
cenné	cenný	k2eAgFnPc1d1	cenná
umělecké	umělecký	k2eAgFnPc1d1	umělecká
sbírky	sbírka	k1gFnPc1	sbírka
z	z	k7c2	z
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
a	a	k8xC	a
do	do	k7c2	do
zajetí	zajetí	k1gNnSc2	zajetí
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
mnoho	mnoho	k4c1	mnoho
urozených	urozený	k2eAgMnPc2d1	urozený
pánů	pan	k1gMnPc2	pan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
museli	muset	k5eAaImAgMnP	muset
zaplatit	zaplatit	k5eAaPmF	zaplatit
vysoké	vysoký	k2eAgNnSc4d1	vysoké
výkupné	výkupné	k1gNnSc4	výkupné
<g/>
.	.	kIx.	.
</s>
<s>
Vniknutí	vniknutí	k1gNnSc1	vniknutí
Švédů	Švéd	k1gMnPc2	Švéd
na	na	k7c4	na
pravý	pravý	k2eAgInSc4d1	pravý
břeh	břeh	k1gInSc4	břeh
zabránili	zabránit	k5eAaPmAgMnP	zabránit
Pražané	Pražan	k1gMnPc1	Pražan
a	a	k8xC	a
studenti	student	k1gMnPc1	student
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
zahradili	zahradit	k5eAaPmAgMnP	zahradit
Karlův	Karlův	k2eAgInSc4d1	Karlův
most	most	k1gInSc4	most
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
bojovalo	bojovat	k5eAaImAgNnS	bojovat
až	až	k9	až
několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
uzavření	uzavření	k1gNnSc6	uzavření
vestfálského	vestfálský	k2eAgInSc2d1	vestfálský
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
podepsaly	podepsat	k5eAaPmAgFnP	podepsat
mírovou	mírový	k2eAgFnSc4d1	mírová
dohodu	dohoda	k1gFnSc4	dohoda
a	a	k8xC	a
Švédové	Švéd	k1gMnPc1	Švéd
se	se	k3xPyFc4	se
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ukončení	ukončení	k1gNnPc4	ukončení
a	a	k8xC	a
následky	následek	k1gInPc4	následek
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vestfálský	vestfálský	k2eAgInSc4d1	vestfálský
mír	mír	k1gInSc4	mír
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
politický	politický	k2eAgInSc4d1	politický
dopad	dopad	k1gInSc4	dopad
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1644	[number]	k4	1644
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
městech	město	k1gNnPc6	město
Osnabrück	Osnabrücka	k1gFnPc2	Osnabrücka
a	a	k8xC	a
Münster	Münstra	k1gFnPc2	Münstra
ve	v	k7c6	v
Vestfálsku	Vestfálsko	k1gNnSc6	Vestfálsko
(	(	kIx(	(
<g/>
severozápadní	severozápadní	k2eAgNnSc1d1	severozápadní
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
zahájena	zahájen	k2eAgNnPc4d1	zahájeno
mírová	mírový	k2eAgNnPc4d1	Mírové
jednání	jednání	k1gNnPc4	jednání
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
dohoda	dohoda	k1gFnSc1	dohoda
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1648	[number]	k4	1648
nazývá	nazývat	k5eAaImIp3nS	nazývat
vestfálský	vestfálský	k2eAgInSc4d1	vestfálský
mír	mír	k1gInSc4	mír
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
řešil	řešit	k5eAaImAgInS	řešit
tento	tento	k3xDgInSc1	tento
mír	mír	k1gInSc1	mír
také	také	k6eAd1	také
osmdesátiletou	osmdesátiletý	k2eAgFnSc4d1	osmdesátiletá
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
spor	spor	k1gInSc4	spor
mezi	mezi	k7c7	mezi
Španělskem	Španělsko	k1gNnSc7	Španělsko
a	a	k8xC	a
Nizozemskými	nizozemský	k2eAgFnPc7d1	nizozemská
provinciemi	provincie	k1gFnPc7	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
mezi	mezi	k7c7	mezi
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Španělskem	Španělsko	k1gNnSc7	Španělsko
probíhaly	probíhat	k5eAaImAgFnP	probíhat
po	po	k7c6	po
vestfálském	vestfálský	k2eAgInSc6d1	vestfálský
míru	mír	k1gInSc6	mír
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1659	[number]	k4	1659
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
sepsán	sepsat	k5eAaPmNgInS	sepsat
pyrenejský	pyrenejský	k2eAgInSc1d1	pyrenejský
mír	mír	k1gInSc1	mír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vestfálský	vestfálský	k2eAgInSc1d1	vestfálský
mír	mír	k1gInSc1	mír
znamenal	znamenat	k5eAaImAgInS	znamenat
příklon	příklon	k1gInSc4	příklon
k	k	k7c3	k
absolutismu	absolutismus	k1gInSc3	absolutismus
a	a	k8xC	a
také	také	k9	také
striktní	striktní	k2eAgNnSc4d1	striktní
dodržování	dodržování	k1gNnSc4	dodržování
augšpurského	augšpurský	k2eAgNnSc2d1	augšpurské
určování	určování	k1gNnSc2	určování
náboženství	náboženství	k1gNnSc2	náboženství
podle	podle	k7c2	podle
panovníka	panovník	k1gMnSc2	panovník
(	(	kIx(	(
<g/>
cuius	cuius	k1gMnSc1	cuius
regio	regio	k1gMnSc1	regio
<g/>
,	,	kIx,	,
eius	eius	k6eAd1	eius
religio	religio	k6eAd1	religio
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
praktikováno	praktikován	k2eAgNnSc1d1	praktikováno
s	s	k7c7	s
určitou	určitý	k2eAgFnSc7d1	určitá
benevolencí	benevolence	k1gFnSc7	benevolence
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
nový	nový	k2eAgInSc4d1	nový
umělecký	umělecký	k2eAgInSc4d1	umělecký
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
baroko	baroko	k1gNnSc4	baroko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vytlačilo	vytlačit	k5eAaPmAgNnS	vytlačit
renesanci	renesance	k1gFnSc4	renesance
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
nástrojem	nástroj	k1gInSc7	nástroj
protireformace	protireformace	k1gFnSc2	protireformace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemích	zem	k1gFnPc6	zem
České	český	k2eAgFnSc2d1	Česká
koruny	koruna	k1gFnSc2	koruna
došlo	dojít	k5eAaPmAgNnS	dojít
během	během	k7c2	během
války	válka	k1gFnSc2	válka
a	a	k8xC	a
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
k	k	k7c3	k
"	"	kIx"	"
<g/>
době	doba	k1gFnSc3	doba
temna	temno	k1gNnSc2	temno
<g/>
"	"	kIx"	"
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
regionu	region	k1gInSc6	region
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
mnoho	mnoho	k4c1	mnoho
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
jich	on	k3xPp3gMnPc2	on
muselo	muset	k5eAaImAgNnS	muset
odejít	odejít	k5eAaPmF	odejít
kvůli	kvůli	k7c3	kvůli
náboženskému	náboženský	k2eAgNnSc3d1	náboženské
přesvědčení	přesvědčení	k1gNnSc3	přesvědčení
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
odešli	odejít	k5eAaPmAgMnP	odejít
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Amos	Amos	k1gMnSc1	Amos
Komenský	Komenský	k1gMnSc1	Komenský
<g/>
.	.	kIx.	.
</s>
<s>
Třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
byla	být	k5eAaImAgFnS	být
také	také	k9	také
posledním	poslední	k2eAgInSc7d1	poslední
velkým	velký	k2eAgInSc7d1	velký
náboženským	náboženský	k2eAgInSc7d1	náboženský
konfliktem	konflikt	k1gInSc7	konflikt
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
neproběhla	proběhnout	k5eNaPmAgFnS	proběhnout
takto	takto	k6eAd1	takto
komplexní	komplexní	k2eAgFnSc1d1	komplexní
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
odvolávala	odvolávat	k5eAaImAgFnS	odvolávat
na	na	k7c6	na
náboženství	náboženství	k1gNnSc3	náboženství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ztráty	ztráta	k1gFnPc1	ztráta
na	na	k7c6	na
životech	život	k1gInPc6	život
a	a	k8xC	a
šíření	šíření	k1gNnSc1	šíření
nemocí	nemoc	k1gFnPc2	nemoc
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
německých	německý	k2eAgFnPc6d1	německá
zemích	zem	k1gFnPc6	zem
válka	válka	k1gFnSc1	válka
způsobila	způsobit	k5eAaPmAgFnS	způsobit
úbytek	úbytek	k1gInSc4	úbytek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
o	o	k7c4	o
třicet	třicet	k4xCc4	třicet
procent	procento	k1gNnPc2	procento
<g/>
,	,	kIx,	,
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
až	až	k9	až
o	o	k7c4	o
padesát	padesát	k4xCc4	padesát
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Braniborska	Braniborsko	k1gNnSc2	Braniborsko
klesl	klesnout	k5eAaPmAgInS	klesnout
počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
a	a	k8xC	a
v	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Pomořansku	Pomořansko	k1gNnSc6	Pomořansko
a	a	k8xC	a
na	na	k7c6	na
dalších	další	k2eAgNnPc6d1	další
místech	místo	k1gNnPc6	místo
až	až	k6eAd1	až
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
třetiny	třetina	k1gFnPc4	třetina
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejvíce	hodně	k6eAd3	hodně
poškozených	poškozený	k2eAgNnPc2d1	poškozené
měst	město	k1gNnPc2	město
byl	být	k5eAaImAgMnS	být
Magdeburg	Magdeburg	k1gInSc4	Magdeburg
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
během	během	k7c2	během
války	válka	k1gFnSc2	válka
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
25	[number]	k4	25
000	[number]	k4	000
na	na	k7c4	na
2	[number]	k4	2
464	[number]	k4	464
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Labe	Labe	k1gNnSc2	Labe
bylo	být	k5eAaImAgNnS	být
opuštěno	opuštěn	k2eAgNnSc1d1	opuštěno
nebo	nebo	k8xC	nebo
pobořeno	pobořen	k2eAgNnSc1d1	pobořeno
87	[number]	k4	87
%	%	kIx~	%
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgFnP	být
České	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
války	válka	k1gFnSc2	válka
uchráněny	uchráněn	k2eAgInPc1d1	uchráněn
před	před	k7c7	před
největšími	veliký	k2eAgFnPc7d3	veliký
válečnými	válečný	k2eAgFnPc7d1	válečná
útrapami	útrapa	k1gFnPc7	útrapa
<g/>
,	,	kIx,	,
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
zde	zde	k6eAd1	zde
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
třetinu	třetina	k1gFnSc4	třetina
a	a	k8xC	a
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
pustých	pustý	k2eAgNnPc2d1	pusté
20	[number]	k4	20
%	%	kIx~	%
usedlostí	usedlost	k1gFnPc2	usedlost
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
22	[number]	k4	22
%	%	kIx~	%
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
Některým	některý	k3yIgMnPc3	některý
oblastem	oblast	k1gFnPc3	oblast
se	se	k3xPyFc4	se
válka	válka	k1gFnSc1	válka
téměř	téměř	k6eAd1	téměř
vyhnula	vyhnout	k5eAaPmAgFnS	vyhnout
<g/>
,	,	kIx,	,
v	v	k7c6	v
Dolním	dolní	k2eAgNnSc6d1	dolní
Sasku	Sasko	k1gNnSc6	Sasko
klesl	klesnout	k5eAaPmAgInS	klesnout
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
deset	deset	k4xCc4	deset
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
<g/>
Zpustošenou	zpustošený	k2eAgFnSc4d1	zpustošená
zemi	zem	k1gFnSc4	zem
popsal	popsat	k5eAaPmAgMnS	popsat
Petr	Petr	k1gMnSc1	Petr
Lehnstein	Lehnstein	k1gMnSc1	Lehnstein
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Člověk	člověk	k1gMnSc1	člověk
jde	jít	k5eAaImIp3nS	jít
až	až	k9	až
deset	deset	k4xCc4	deset
mil	míle	k1gFnPc2	míle
a	a	k8xC	a
nepotká	potkat	k5eNaPmIp3nS	potkat
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
kus	kus	k1gInSc4	kus
dobytka	dobytek	k1gInSc2	dobytek
<g/>
,	,	kIx,	,
neuvidí	uvidět	k5eNaPmIp3nP	uvidět
ani	ani	k8xC	ani
špačka	špaček	k1gMnSc4	špaček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
najde	najít	k5eAaPmIp3nS	najít
starého	starý	k1gMnSc4	starý
muže	muž	k1gMnSc4	muž
<g/>
,	,	kIx,	,
dítě	dítě	k1gNnSc1	dítě
nebo	nebo	k8xC	nebo
dvě	dva	k4xCgFnPc1	dva
staré	starý	k2eAgFnPc1d1	stará
ženy	žena	k1gFnPc1	žena
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
vesnicích	vesnice	k1gFnPc6	vesnice
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
domech	dům	k1gInPc6	dům
plno	plno	k6eAd1	plno
mrtvol	mrtvola	k1gFnPc2	mrtvola
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc1	dítě
i	i	k8xC	i
čeleď	čeleď	k1gFnSc1	čeleď
<g/>
,	,	kIx,	,
vepři	vepř	k1gMnPc1	vepř
<g/>
,	,	kIx,	,
krávy	kráva	k1gFnPc1	kráva
a	a	k8xC	a
voli	vůl	k1gMnPc1	vůl
leží	ležet	k5eAaImIp3nP	ležet
vedle	vedle	k7c2	vedle
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
zardoušeni	zardoušen	k2eAgMnPc1d1	zardoušen
hladem	hlad	k1gInSc7	hlad
a	a	k8xC	a
morem	mor	k1gInSc7	mor
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
plni	pln	k2eAgMnPc1d1	pln
larv	larv	k1gMnSc1	larv
a	a	k8xC	a
červů	červ	k1gMnPc2	červ
<g/>
.	.	kIx.	.
</s>
<s>
Požírají	požírat	k5eAaImIp3nP	požírat
je	on	k3xPp3gNnSc4	on
vlci	vlk	k1gMnPc1	vlk
<g/>
,	,	kIx,	,
lišky	liška	k1gFnPc1	liška
<g/>
,	,	kIx,	,
psi	pes	k1gMnPc1	pes
<g/>
,	,	kIx,	,
krkavci	krkavec	k1gMnPc1	krkavec
<g/>
,	,	kIx,	,
vrány	vrána	k1gFnPc1	vrána
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
poněvadž	poněvadž	k8xS	poněvadž
nebylo	být	k5eNaImAgNnS	být
nikoho	nikdo	k3yNnSc4	nikdo
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
by	by	kYmCp3nS	by
je	on	k3xPp3gFnPc4	on
pochoval	pochovat	k5eAaPmAgMnS	pochovat
<g/>
,	,	kIx,	,
politoval	politovat	k5eAaPmAgMnS	politovat
a	a	k8xC	a
oplakal	oplakat	k5eAaPmAgMnS	oplakat
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
V	v	k7c6	v
dnešním	dnešní	k2eAgNnSc6d1	dnešní
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
okolních	okolní	k2eAgFnPc6d1	okolní
zemích	zem	k1gFnPc6	zem
během	během	k7c2	během
války	válka	k1gFnSc2	válka
a	a	k8xC	a
po	po	k7c6	po
ní	on	k3xPp3gFnSc6	on
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
propuknutí	propuknutí	k1gNnSc3	propuknutí
mnoha	mnoho	k4c3	mnoho
epidemií	epidemie	k1gFnPc2	epidemie
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
během	během	k7c2	během
válek	válka	k1gFnPc2	válka
výjimečný	výjimečný	k2eAgInSc4d1	výjimečný
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velkému	velký	k2eAgNnSc3d1	velké
mísení	mísení	k1gNnSc3	mísení
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vojáci	voják	k1gMnPc1	voják
přesouvali	přesouvat	k5eAaImAgMnP	přesouvat
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
pozice	pozice	k1gFnSc2	pozice
do	do	k7c2	do
další	další	k2eAgFnSc2d1	další
a	a	k8xC	a
na	na	k7c4	na
jiné	jiný	k2eAgFnPc4d1	jiná
fronty	fronta	k1gFnPc4	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
také	také	k9	také
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
velkému	velký	k2eAgNnSc3d1	velké
přesidlování	přesidlování	k1gNnSc3	přesidlování
utečenců	utečenec	k1gMnPc2	utečenec
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
vše	všechen	k3xTgNnSc1	všechen
dohromady	dohromady	k6eAd1	dohromady
poskytovalo	poskytovat	k5eAaImAgNnS	poskytovat
podmínky	podmínka	k1gFnPc4	podmínka
pro	pro	k7c4	pro
šíření	šíření	k1gNnSc4	šíření
epidemií	epidemie	k1gFnPc2	epidemie
<g/>
.	.	kIx.	.
</s>
<s>
Dobové	dobový	k2eAgFnPc1d1	dobová
kroniky	kronika	k1gFnPc1	kronika
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
o	o	k7c6	o
lokálních	lokální	k2eAgFnPc6d1	lokální
epidemiích	epidemie	k1gFnPc6	epidemie
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
když	když	k8xS	když
se	se	k3xPyFc4	se
střetly	střetnout	k5eAaPmAgFnP	střetnout
dánské	dánský	k2eAgFnPc1d1	dánská
a	a	k8xC	a
císařské	císařský	k2eAgFnPc1d1	císařská
armády	armáda	k1gFnPc1	armáda
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Saska	Sasko	k1gNnSc2	Sasko
a	a	k8xC	a
Durynska	Durynsko	k1gNnSc2	Durynsko
v	v	k7c6	v
letech	let	k1gInPc6	let
1625	[number]	k4	1625
<g/>
–	–	k?	–
<g/>
26	[number]	k4	26
<g/>
,	,	kIx,	,
mluví	mluvit	k5eAaImIp3nS	mluvit
záznamy	záznam	k1gInPc4	záznam
z	z	k7c2	z
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
o	o	k7c6	o
rapidním	rapidní	k2eAgInSc6d1	rapidní
nárůstu	nárůst	k1gInSc6	nárůst
nemocnosti	nemocnost	k1gFnSc2	nemocnost
<g/>
.	.	kIx.	.
</s>
<s>
Kroniky	kronika	k1gFnPc1	kronika
a	a	k8xC	a
dobové	dobový	k2eAgInPc1d1	dobový
záznamy	záznam	k1gInPc1	záznam
podávají	podávat	k5eAaImIp3nP	podávat
informace	informace	k1gFnPc4	informace
o	o	k7c4	o
takzvané	takzvaný	k2eAgFnPc4d1	takzvaná
"	"	kIx"	"
<g/>
uherské	uherský	k2eAgFnPc4d1	uherská
nemoci	nemoc	k1gFnPc4	nemoc
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
pravá	pravý	k2eAgFnSc1d1	pravá
podstata	podstata	k1gFnSc1	podstata
není	být	k5eNaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
uváděna	uvádět	k5eAaImNgFnS	uvádět
jako	jako	k9	jako
úplavice	úplavice	k1gFnSc1	úplavice
nebo	nebo	k8xC	nebo
též	též	k9	též
syfilis	syfilis	k1gFnSc1	syfilis
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
se	se	k3xPyFc4	se
také	také	k9	také
objevují	objevovat	k5eAaImIp3nP	objevovat
epidemie	epidemie	k1gFnPc1	epidemie
tyfu	tyf	k1gInSc2	tyf
<g/>
,	,	kIx,	,
moru	mor	k1gInSc2	mor
nebo	nebo	k8xC	nebo
kurdějí	kurděje	k1gFnPc2	kurděje
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Český	český	k2eAgMnSc1d1	český
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
historik	historik	k1gMnSc1	historik
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Balbín	Balbín	k1gMnSc1	Balbín
napsal	napsat	k5eAaPmAgMnS	napsat
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Spatříš	spatřit	k5eAaPmIp2nS	spatřit
mnohdy	mnohdy	k6eAd1	mnohdy
na	na	k7c6	na
opuštěném	opuštěný	k2eAgNnSc6d1	opuštěné
místě	místo	k1gNnSc6	místo
trčeti	trčet	k5eAaImF	trčet
bránu	brána	k1gFnSc4	brána
<g/>
,	,	kIx,	,
sloup	sloup	k1gInSc4	sloup
<g/>
,	,	kIx,	,
ohromné	ohromný	k2eAgFnPc4d1	ohromná
trosky	troska	k1gFnPc4	troska
veliké	veliký	k2eAgFnPc4d1	veliká
budovy	budova	k1gFnPc4	budova
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
zbývá	zbývat	k5eAaImIp3nS	zbývat
ze	z	k7c2	z
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
vypínal	vypínat	k5eAaImAgMnS	vypínat
<g/>
.	.	kIx.	.
</s>
<s>
Jinde	jinde	k6eAd1	jinde
strmí	strmit	k5eAaImIp3nS	strmit
osamělá	osamělý	k2eAgFnSc1d1	osamělá
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgMnSc1d1	jediný
to	ten	k3xDgNnSc1	ten
zbytek	zbytek	k1gInSc1	zbytek
vesnice	vesnice	k1gFnSc2	vesnice
či	či	k8xC	či
tvrze	tvrz	k1gFnSc2	tvrz
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
pohltily	pohltit	k5eAaPmAgInP	pohltit
plameny	plamen	k1gInPc1	plamen
<g/>
.	.	kIx.	.
</s>
<s>
Nebylo	být	k5eNaImAgNnS	být
by	by	kYmCp3nS	by
možná	možná	k9	možná
uvést	uvést	k5eAaPmF	uvést
města	město	k1gNnPc4	město
<g/>
,	,	kIx,	,
ba	ba	k9	ba
ani	ani	k8xC	ani
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
nebyl	být	k5eNaImAgInS	být
vypálen	vypálit	k5eAaPmNgInS	vypálit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Země	zem	k1gFnSc2	zem
Evropy	Evropa	k1gFnSc2	Evropa
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
,	,	kIx,	,
Irsko	Irsko	k1gNnSc1	Irsko
a	a	k8xC	a
Skotsko	Skotsko	k1gNnSc1	Skotsko
<g/>
:	:	kIx,	:
Relativně	relativně	k6eAd1	relativně
válkou	válka	k1gFnSc7	válka
nedotčené	dotčený	k2eNgFnSc2d1	nedotčená
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterých	který	k3yIgFnPc6	který
ještě	ještě	k6eAd1	ještě
během	během	k7c2	během
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
válka	válka	k1gFnSc1	válka
občanská	občanský	k2eAgFnSc1d1	občanská
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vyústila	vyústit	k5eAaPmAgFnS	vyústit
roku	rok	k1gInSc2	rok
1651	[number]	k4	1651
ve	v	k7c4	v
vládu	vláda	k1gFnSc4	vláda
Olivera	Oliver	k1gMnSc2	Oliver
Cromwella	Cromwell	k1gMnSc2	Cromwell
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
:	:	kIx,	:
Vyšly	vyjít	k5eAaPmAgFnP	vyjít
z	z	k7c2	z
války	válka	k1gFnSc2	válka
menší	malý	k2eAgFnSc2d2	menší
o	o	k7c6	o
území	území	k1gNnSc6	území
Horní	horní	k2eAgFnSc1d1	horní
a	a	k8xC	a
Dolní	dolní	k2eAgFnSc1d1	dolní
Lužice	Lužice	k1gFnSc1	Lužice
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
připojeno	připojit	k5eAaPmNgNnS	připojit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1635	[number]	k4	1635
k	k	k7c3	k
Sasku	Sasko	k1gNnSc3	Sasko
jako	jako	k8xS	jako
vypořádání	vypořádání	k1gNnSc4	vypořádání
mezi	mezi	k7c7	mezi
kurfiřtem	kurfiřt	k1gMnSc7	kurfiřt
a	a	k8xC	a
císařem	císař	k1gMnSc7	císař
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Válečnými	válečný	k2eAgFnPc7d1	válečná
útrapami	útrapa	k1gFnPc7	útrapa
byla	být	k5eAaImAgFnS	být
nejvíce	hodně	k6eAd3	hodně
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
stavů	stav	k1gInPc2	stav
postižena	postižen	k2eAgNnPc4d1	postiženo
města	město	k1gNnPc4	město
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
musela	muset	k5eAaImAgFnS	muset
odvádět	odvádět	k5eAaImF	odvádět
nejvíce	hodně	k6eAd3	hodně
daní	daň	k1gFnPc2	daň
přímo	přímo	k6eAd1	přímo
i	i	k9	i
nepřímo	přímo	k6eNd1	přímo
<g/>
,	,	kIx,	,
těmito	tento	k3xDgFnPc7	tento
útrapami	útrapa	k1gFnPc7	útrapa
byly	být	k5eAaImAgInP	být
znovu	znovu	k6eAd1	znovu
utuženy	utužen	k2eAgInPc4d1	utužen
cechy	cech	k1gInPc4	cech
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
zpomalen	zpomalen	k2eAgInSc4d1	zpomalen
městský	městský	k2eAgInSc4d1	městský
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
vývoj	vývoj	k1gInSc4	vývoj
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zaznamenaly	zaznamenat	k5eAaPmAgFnP	zaznamenat
konec	konec	k1gInSc4	konec
nezávislosti	nezávislost	k1gFnSc2	nezávislost
stavovského	stavovský	k2eAgNnSc2d1	Stavovské
soustátí	soustátí	k1gNnSc2	soustátí
a	a	k8xC	a
vznik	vznik	k1gInSc1	vznik
dědičného	dědičný	k2eAgNnSc2d1	dědičné
království	království	k1gNnSc2	království
v	v	k7c6	v
držení	držení	k1gNnSc6	držení
Habsburků	Habsburk	k1gMnPc2	Habsburk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
si	se	k3xPyFc3	se
tyto	tento	k3xDgFnPc4	tento
země	zem	k1gFnPc4	zem
udrželi	udržet	k5eAaPmAgMnP	udržet
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nizozemské	nizozemský	k2eAgFnPc4d1	nizozemská
provincie	provincie	k1gFnPc4	provincie
<g/>
:	:	kIx,	:
Po	po	k7c6	po
osmdesáti	osmdesát	k4xCc6	osmdesát
letech	léto	k1gNnPc6	léto
bojů	boj	k1gInPc2	boj
se	s	k7c7	s
Španělskem	Španělsko	k1gNnSc7	Španělsko
jim	on	k3xPp3gInPc3	on
byla	být	k5eAaImAgFnS	být
uznána	uznán	k2eAgFnSc1d1	uznána
oficiálně	oficiálně	k6eAd1	oficiálně
nezávislost	nezávislost	k1gFnSc4	nezávislost
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Říše	říš	k1gFnSc2	říš
a	a	k8xC	a
Španělska	Španělsko	k1gNnSc2	Španělsko
(	(	kIx(	(
<g/>
fakticky	fakticky	k6eAd1	fakticky
byla	být	k5eAaImAgFnS	být
uznána	uznat	k5eAaPmNgFnS	uznat
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1609	[number]	k4	1609
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
sjednáno	sjednán	k2eAgNnSc4d1	sjednáno
dvanáctileté	dvanáctiletý	k2eAgNnSc4d1	dvanáctileté
příměří	příměří	k1gNnSc4	příměří
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svatá	svatý	k2eAgFnSc1d1	svatá
říše	říše	k1gFnSc1	říše
římská	římský	k2eAgFnSc1d1	římská
<g/>
:	:	kIx,	:
Statem	status	k1gInSc7	status
quo	quo	k?	quo
byl	být	k5eAaImAgInS	být
vestfálským	vestfálský	k2eAgInSc7d1	vestfálský
mírem	mír	k1gInSc7	mír
určen	určen	k2eAgInSc1d1	určen
rok	rok	k1gInSc1	rok
1624	[number]	k4	1624
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
svazek	svazek	k1gInSc4	svazek
nezávislých	závislý	k2eNgInPc2d1	nezávislý
států	stát	k1gInPc2	stát
s	s	k7c7	s
formální	formální	k2eAgFnSc7d1	formální
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
–	–	k?	–
císařem	císař	k1gMnSc7	císař
<g/>
,	,	kIx,	,
voleným	volený	k2eAgMnSc7d1	volený
pro	pro	k7c4	pro
příště	příště	k6eAd1	příště
už	už	k6eAd1	už
jen	jen	k9	jen
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Habsburků	Habsburk	k1gMnPc2	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Habsburkové	Habsburk	k1gMnPc1	Habsburk
si	se	k3xPyFc3	se
ale	ale	k9	ale
podrželi	podržet	k5eAaPmAgMnP	podržet
absolutní	absolutní	k2eAgFnSc4d1	absolutní
moc	moc	k1gFnSc4	moc
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
dědičných	dědičný	k2eAgFnPc6d1	dědičná
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
země	země	k1gFnSc1	země
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
Rakouské	rakouský	k2eAgFnSc2d1	rakouská
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
–	–	k?	–
rekatolizace	rekatolizace	k1gFnSc1	rekatolizace
zde	zde	k6eAd1	zde
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
a	a	k8xC	a
konfiskace	konfiskace	k1gFnSc1	konfiskace
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
udrželi	udržet	k5eAaPmAgMnP	udržet
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
museli	muset	k5eAaImAgMnP	muset
tolerovat	tolerovat	k5eAaImF	tolerovat
protestantskou	protestantský	k2eAgFnSc4d1	protestantská
víru	víra	k1gFnSc4	víra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
:	:	kIx,	:
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1640	[number]	k4	1640
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
odtrhlo	odtrhnout	k5eAaPmAgNnS	odtrhnout
Portugalské	portugalský	k2eAgNnSc4d1	portugalské
království	království	k1gNnSc4	království
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
také	také	k9	také
velice	velice	k6eAd1	velice
urychlila	urychlit	k5eAaPmAgFnS	urychlit
úpadek	úpadek	k1gInSc4	úpadek
této	tento	k3xDgFnSc2	tento
mocnosti	mocnost	k1gFnSc2	mocnost
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
završen	završit	k5eAaPmNgInS	završit
vymřením	vymření	k1gNnSc7	vymření
habsburské	habsburský	k2eAgFnSc2d1	habsburská
větve	větev	k1gFnSc2	větev
španělských	španělský	k2eAgMnPc2d1	španělský
panovníků	panovník	k1gMnPc2	panovník
roku	rok	k1gInSc2	rok
1700	[number]	k4	1700
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
:	:	kIx,	:
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
války	válka	k1gFnSc2	válka
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
bojovalo	bojovat	k5eAaImAgNnS	bojovat
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
výrazným	výrazný	k2eAgInSc7d1	výrazný
úspěchem	úspěch	k1gInSc7	úspěch
také	také	k9	také
s	s	k7c7	s
Dánskem	Dánsko	k1gNnSc7	Dánsko
o	o	k7c4	o
dominium	dominium	k1gNnSc4	dominium
maris	maris	k1gFnSc2	maris
Baltici	Baltice	k1gFnSc4	Baltice
(	(	kIx(	(
<g/>
panství	panství	k1gNnSc4	panství
nad	nad	k7c7	nad
Baltským	baltský	k2eAgNnSc7d1	Baltské
mořem	moře	k1gNnSc7	moře
<g/>
)	)	kIx)	)
a	a	k8xC	a
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
uhájit	uhájit	k5eAaPmF	uhájit
výsadní	výsadní	k2eAgNnSc4d1	výsadní
postavení	postavení	k1gNnSc4	postavení
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Baltu	Balt	k1gInSc2	Balt
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
udělalo	udělat	k5eAaPmAgNnS	udělat
novou	nový	k2eAgFnSc4d1	nová
evropskou	evropský	k2eAgFnSc4d1	Evropská
velmoc	velmoc	k1gFnSc4	velmoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Švýcarská	švýcarský	k2eAgFnSc1d1	švýcarská
konfederace	konfederace	k1gFnSc1	konfederace
<g/>
:	:	kIx,	:
Definitivně	definitivně	k6eAd1	definitivně
byla	být	k5eAaImAgFnS	být
uznána	uznán	k2eAgFnSc1d1	uznána
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Věčná	věčný	k2eAgFnSc1d1	věčná
neutralita	neutralita	k1gFnSc1	neutralita
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
uznána	uznán	k2eAgFnSc1d1	uznána
všemi	všecek	k3xTgFnPc7	všecek
evropskými	evropský	k2eAgFnPc7d1	Evropská
mocnostmi	mocnost	k1gFnPc7	mocnost
a	a	k8xC	a
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
udrželo	udržet	k5eAaPmAgNnS	udržet
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsob	způsob	k1gInSc1	způsob
boje	boj	k1gInSc2	boj
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
válce	válka	k1gFnSc6	válka
bojovali	bojovat	k5eAaImAgMnP	bojovat
především	především	k9	především
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
najímáni	najímat	k5eAaImNgMnP	najímat
za	za	k7c4	za
žold	žold	k1gInSc4	žold
(	(	kIx(	(
<g/>
převážná	převážný	k2eAgFnSc1d1	převážná
většina	většina	k1gFnSc1	většina
všech	všecek	k3xTgMnPc2	všecek
vojáků	voják	k1gMnPc2	voják
byli	být	k5eAaImAgMnP	být
právě	právě	k9	právě
žoldnéři	žoldnér	k1gMnPc1	žoldnér
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
jich	on	k3xPp3gMnPc2	on
poskytovaly	poskytovat	k5eAaImAgInP	poskytovat
země	zem	k1gFnSc2	zem
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Hesensko	Hesensko	k1gNnSc1	Hesensko
a	a	k8xC	a
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Španělé	Španěl	k1gMnPc1	Španěl
získávali	získávat	k5eAaImAgMnP	získávat
vojáky	voják	k1gMnPc4	voják
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
evropských	evropský	k2eAgFnPc2d1	Evropská
držav	država	k1gFnPc2	država
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
krom	krom	k7c2	krom
vlastního	vlastní	k2eAgNnSc2d1	vlastní
Španělska	Španělsko	k1gNnSc2	Španělsko
ještě	ještě	k9	ještě
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Valonska	Valonsko	k1gNnSc2	Valonsko
a	a	k8xC	a
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvýraznější	výrazný	k2eAgFnSc7d3	nejvýraznější
bojovou	bojový	k2eAgFnSc7d1	bojová
taktikou	taktika	k1gFnSc7	taktika
se	se	k3xPyFc4	se
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
války	válka	k1gFnSc2	válka
zapojilo	zapojit	k5eAaPmAgNnS	zapojit
hned	hned	k6eAd1	hned
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
Španělsko	Španělsko	k1gNnSc4	Španělsko
s	s	k7c7	s
bojovými	bojový	k2eAgFnPc7d1	bojová
formacemi	formace	k1gFnPc7	formace
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
se	se	k3xPyFc4	se
říkalo	říkat	k5eAaImAgNnS	říkat
španělský	španělský	k2eAgInSc4d1	španělský
čtverec	čtverec	k1gInSc4	čtverec
nebo	nebo	k8xC	nebo
tercie	tercie	k1gFnPc4	tercie
<g/>
.	.	kIx.	.
</s>
<s>
Boj	boj	k1gInSc1	boj
v	v	k7c6	v
tercii	tercie	k1gFnSc6	tercie
byla	být	k5eAaImAgFnS	být
taktika	taktika	k1gFnSc1	taktika
známá	známý	k2eAgFnSc1d1	známá
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
pocházela	pocházet	k5eAaImAgFnS	pocházet
zřejmě	zřejmě	k6eAd1	zřejmě
ze	z	k7c2	z
Švýcarska	Švýcarsko	k1gNnSc2	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
této	tento	k3xDgFnSc2	tento
formace	formace	k1gFnSc2	formace
<g/>
,	,	kIx,	,
čítající	čítající	k2eAgInSc4d1	čítající
do	do	k7c2	do
3	[number]	k4	3
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
pikenýři	pikenýř	k1gMnPc1	pikenýř
uspořádáni	uspořádán	k2eAgMnPc1d1	uspořádán
ve	v	k7c6	v
falanze	falanga	k1gFnSc6	falanga
a	a	k8xC	a
okolo	okolo	k7c2	okolo
nich	on	k3xPp3gMnPc2	on
byli	být	k5eAaImAgMnP	být
rozestavěni	rozestavěn	k2eAgMnPc1d1	rozestavěn
mušketýři	mušketýr	k1gMnPc1	mušketýr
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
formace	formace	k1gFnPc1	formace
byly	být	k5eAaImAgFnP	být
známé	známý	k2eAgFnPc1d1	známá
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
vysoké	vysoký	k2eAgFnSc3d1	vysoká
morálce	morálka	k1gFnSc3	morálka
a	a	k8xC	a
hloubce	hloubka	k1gFnSc3	hloubka
řad	řada	k1gFnPc2	řada
<g/>
,	,	kIx,	,
svojí	svůj	k3xOyFgFnSc7	svůj
neporazitelností	neporazitelnost	k1gFnSc7	neporazitelnost
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Rocroi	Rocro	k1gFnSc2	Rocro
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
Španělé	Španěl	k1gMnPc1	Španěl
poraženi	porazit	k5eAaPmNgMnP	porazit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jízda	jízda	k1gFnSc1	jízda
používala	používat	k5eAaImAgFnS	používat
obvykle	obvykle	k6eAd1	obvykle
taktiku	taktika	k1gFnSc4	taktika
"	"	kIx"	"
<g/>
karakola	karakola	k1gFnSc1	karakola
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
taktika	taktika	k1gFnSc1	taktika
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
skupina	skupina	k1gFnSc1	skupina
jezdců	jezdec	k1gMnPc2	jezdec
(	(	kIx(	(
<g/>
kyrysníci	kyrysník	k1gMnPc1	kyrysník
<g/>
,	,	kIx,	,
či	či	k8xC	či
arkebuzíři	arkebuzír	k1gMnPc1	arkebuzír
<g/>
)	)	kIx)	)
přijela	přijet	k5eAaPmAgFnS	přijet
na	na	k7c4	na
dostřel	dostřel	k1gInSc4	dostřel
od	od	k7c2	od
nepřítele	nepřítel	k1gMnSc2	nepřítel
(	(	kIx(	(
<g/>
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
kroků	krok	k1gInPc2	krok
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zastavila	zastavit	k5eAaPmAgNnP	zastavit
se	se	k3xPyFc4	se
a	a	k8xC	a
první	první	k4xOgFnSc1	první
řada	řada	k1gFnSc1	řada
vypálila	vypálit	k5eAaPmAgFnS	vypálit
salvu	salva	k1gFnSc4	salva
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
vzdálila	vzdálit	k5eAaPmAgFnS	vzdálit
nabít	nabít	k5eAaPmF	nabít
a	a	k8xC	a
mezitím	mezitím	k6eAd1	mezitím
mohla	moct	k5eAaImAgFnS	moct
střílet	střílet	k5eAaImF	střílet
druhá	druhý	k4xOgFnSc1	druhý
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc1	třetí
řada	řada	k1gFnSc1	řada
atd.	atd.	kA	atd.
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
ale	ale	k8xC	ale
probíhaly	probíhat	k5eAaImAgFnP	probíhat
diskuze	diskuze	k1gFnPc1	diskuze
o	o	k7c6	o
neúčinnosti	neúčinnost	k1gFnSc6	neúčinnost
palných	palný	k2eAgFnPc2d1	palná
zbraní	zbraň	k1gFnPc2	zbraň
používaných	používaný	k2eAgFnPc2d1	používaná
jízdou	jízda	k1gFnSc7	jízda
<g/>
.	.	kIx.	.
<g/>
Císařská	císařský	k2eAgFnSc1d1	císařská
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
spoléhala	spoléhat	k5eAaImAgFnS	spoléhat
především	především	k9	především
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
armádu	armáda	k1gFnSc4	armáda
žoldnéřů	žoldnéř	k1gMnPc2	žoldnéř
<g/>
.	.	kIx.	.
</s>
<s>
Takovouto	takovýto	k3xDgFnSc4	takovýto
armádu	armáda	k1gFnSc4	armáda
také	také	k9	také
využívali	využívat	k5eAaPmAgMnP	využívat
i	i	k9	i
protestanští	protestanský	k2eAgMnPc1d1	protestanský
vojáci	voják	k1gMnPc1	voják
ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
výstavby	výstavba	k1gFnSc2	výstavba
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
žoldnéřský	žoldnéřský	k2eAgInSc1d1	žoldnéřský
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgInS	mít
nevýhodu	nevýhoda	k1gFnSc4	nevýhoda
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vlastní	vlastní	k2eAgMnPc1d1	vlastní
vojáci	voják	k1gMnPc1	voják
nebyli	být	k5eNaImAgMnP	být
profesionálně	profesionálně	k6eAd1	profesionálně
cvičeni	cvičit	k5eAaImNgMnP	cvičit
a	a	k8xC	a
nebyli	být	k5eNaImAgMnP	být
disciplinovaní	disciplinovaný	k2eAgMnPc1d1	disciplinovaný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Švédský	švédský	k2eAgMnSc1d1	švédský
král	král	k1gMnSc1	král
Gustav	Gustav	k1gMnSc1	Gustav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
přinesl	přinést	k5eAaPmAgMnS	přinést
do	do	k7c2	do
evropských	evropský	k2eAgFnPc2d1	Evropská
taktik	taktika	k1gFnPc2	taktika
mnoho	mnoho	k6eAd1	mnoho
nového	nový	k2eAgMnSc2d1	nový
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
vůbec	vůbec	k9	vůbec
mohl	moct	k5eAaImAgInS	moct
postavit	postavit	k5eAaPmF	postavit
schopnou	schopný	k2eAgFnSc4d1	schopná
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
zavedl	zavést	k5eAaPmAgInS	zavést
v	v	k7c6	v
početně	početně	k6eAd1	početně
slabém	slabý	k2eAgNnSc6d1	slabé
Švédsku	Švédsko	k1gNnSc6	Švédsko
systém	systém	k1gInSc1	systém
odvodů	odvod	k1gInPc2	odvod
–	–	k?	–
každý	každý	k3xTgMnSc1	každý
desátý	desátý	k4xOgMnSc1	desátý
muž	muž	k1gMnSc1	muž
ze	z	k7c2	z
statku	statek	k1gInSc2	statek
měl	mít	k5eAaImAgInS	mít
jít	jít	k5eAaImF	jít
do	do	k7c2	do
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
zbývajících	zbývající	k2eAgNnPc2d1	zbývající
devět	devět	k4xCc4	devět
jej	on	k3xPp3gNnSc4	on
mělo	mít	k5eAaImAgNnS	mít
živit	živit	k5eAaImF	živit
a	a	k8xC	a
strojit	strojit	k5eAaImF	strojit
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgInSc4	první
prvek	prvek	k1gInSc4	prvek
profesionalizace	profesionalizace	k1gFnSc2	profesionalizace
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
kladl	klást	k5eAaImAgInS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
rychlejší	rychlý	k2eAgFnSc4d2	rychlejší
a	a	k8xC	a
účinnější	účinný	k2eAgFnSc4d2	účinnější
střelbu	střelba	k1gFnSc4	střelba
<g/>
.	.	kIx.	.
</s>
<s>
Mušketýry	mušketýr	k1gMnPc4	mušketýr
Gustav	Gustav	k1gMnSc1	Gustav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
stavěl	stavět	k5eAaImAgMnS	stavět
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
šesti	šest	k4xCc2	šest
řad	řada	k1gFnPc2	řada
za	za	k7c7	za
sebou	se	k3xPyFc7	se
(	(	kIx(	(
<g/>
oproti	oproti	k7c3	oproti
Nizozemcům	Nizozemec	k1gMnPc3	Nizozemec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
bojovali	bojovat	k5eAaImAgMnP	bojovat
v	v	k7c6	v
deseti	deset	k4xCc6	deset
řadách	řada	k1gFnPc6	řada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
tyto	tento	k3xDgFnPc4	tento
řady	řada	k1gFnPc4	řada
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
a	a	k8xC	a
docílil	docílit	k5eAaPmAgMnS	docílit
tak	tak	k9	tak
střelby	střelba	k1gFnPc4	střelba
z	z	k7c2	z
více	hodně	k6eAd2	hodně
mušket	mušketa	k1gFnPc2	mušketa
naráz	naráz	k6eAd1	naráz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jezdectvo	jezdectvo	k1gNnSc1	jezdectvo
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
využití	využití	k1gNnSc3	využití
dobře	dobře	k6eAd1	dobře
cvičených	cvičený	k2eAgMnPc2d1	cvičený
mušketýrů	mušketýr	k1gMnPc2	mušketýr
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
začali	začít	k5eAaPmAgMnP	začít
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
používat	používat	k5eAaImF	používat
střelbu	střelba	k1gFnSc4	střelba
salvou	salva	k1gFnSc7	salva
(	(	kIx(	(
<g/>
až	až	k6eAd1	až
tři	tři	k4xCgFnPc4	tři
řady	řada	k1gFnPc1	řada
za	za	k7c7	za
sebou	se	k3xPyFc7	se
vystřelily	vystřelit	k5eAaPmAgInP	vystřelit
na	na	k7c4	na
blížící	blížící	k2eAgNnSc4d1	blížící
se	se	k3xPyFc4	se
jezdce	jezdec	k1gInSc2	jezdec
naráz	naráz	k6eAd1	naráz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
taktice	taktika	k1gFnSc6	taktika
karakola	karakola	k1gFnSc1	karakola
velmi	velmi	k6eAd1	velmi
trpělo	trpět	k5eAaImAgNnS	trpět
střelbou	střelba	k1gFnSc7	střelba
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
Gustav	Gustav	k1gMnSc1	Gustav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Adolf	Adolf	k1gMnSc1	Adolf
nařídil	nařídit	k5eAaPmAgMnS	nařídit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jízda	jízda	k1gFnSc1	jízda
útočila	útočit	k5eAaImAgFnS	útočit
s	s	k7c7	s
tasenými	tasený	k2eAgInPc7d1	tasený
palaši	palaš	k1gInPc7	palaš
a	a	k8xC	a
ne	ne	k9	ne
se	s	k7c7	s
střelnými	střelný	k2eAgFnPc7d1	střelná
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
.	.	kIx.	.
<g/>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
malé	malý	k2eAgFnSc3d1	malá
účinnosti	účinnost	k1gFnSc3	účinnost
mušket	mušketa	k1gFnPc2	mušketa
a	a	k8xC	a
arkebuz	arkebuza	k1gFnPc2	arkebuza
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
nosili	nosit	k5eAaImAgMnP	nosit
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
především	především	k9	především
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
co	co	k9	co
jezdili	jezdit	k5eAaImAgMnP	jezdit
na	na	k7c6	na
koních	kůň	k1gMnPc6	kůň
<g/>
,	,	kIx,	,
plátovou	plátový	k2eAgFnSc4d1	plátová
zbroj	zbroj	k1gFnSc4	zbroj
nebo	nebo	k8xC	nebo
kyrys	kyrys	k1gInSc4	kyrys
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
je	on	k3xPp3gNnSc4	on
dokázaly	dokázat	k5eAaPmAgInP	dokázat
bez	bez	k7c2	bez
problému	problém	k1gInSc2	problém
ochránit	ochránit	k5eAaPmF	ochránit
před	před	k7c7	před
nepřátelskou	přátelský	k2eNgFnSc7d1	nepřátelská
palbou	palba	k1gFnSc7	palba
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
účinnost	účinnost	k1gFnSc1	účinnost
střelby	střelba	k1gFnSc2	střelba
natolik	natolik	k6eAd1	natolik
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zbroje	zbroj	k1gFnPc1	zbroj
staly	stát	k5eAaPmAgFnP	stát
nepoužitelnými	použitelný	k2eNgFnPc7d1	nepoužitelná
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
tedy	tedy	k9	tedy
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
vymizení	vymizení	k1gNnSc3	vymizení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bitvy	bitva	k1gFnPc1	bitva
probíhaly	probíhat	k5eAaImAgFnP	probíhat
pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
krvavě	krvavě	k6eAd1	krvavě
<g/>
,	,	kIx,	,
v	v	k7c6	v
neschůdném	schůdný	k2eNgInSc6d1	neschůdný
terénu	terén	k1gInSc6	terén
mohly	moct	k5eAaImAgFnP	moct
armády	armáda	k1gFnPc1	armáda
chodit	chodit	k5eAaImF	chodit
kolem	kolem	k6eAd1	kolem
sebe	sebe	k3xPyFc4	sebe
několik	několik	k4yIc1	několik
dní	den	k1gInPc2	den
bez	bez	k7c2	bez
jakéhokoli	jakýkoli	k3yIgInSc2	jakýkoli
střetu	střet	k1gInSc2	střet
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
samozřejmě	samozřejmě	k6eAd1	samozřejmě
krom	krom	k7c2	krom
vyčerpání	vyčerpání	k1gNnSc2	vyčerpání
vojáků	voják	k1gMnPc2	voják
mělo	mít	k5eAaImAgNnS	mít
neblahý	blahý	k2eNgInSc4d1	neblahý
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
okolní	okolní	k2eAgFnPc4d1	okolní
vesnice	vesnice	k1gFnPc4	vesnice
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc4	jejich
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
problém	problém	k1gInSc1	problém
byl	být	k5eAaImAgInS	být
se	s	k7c7	s
zajatci	zajatec	k1gMnPc7	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
absenci	absence	k1gFnSc3	absence
jakýchkoli	jakýkoli	k3yIgFnPc2	jakýkoli
úmluv	úmluva	k1gFnPc2	úmluva
o	o	k7c6	o
válečných	válečný	k2eAgFnPc6d1	válečná
zajatcích	zajatec	k1gMnPc6	zajatec
byli	být	k5eAaImAgMnP	být
zajatci	zajatec	k1gMnPc1	zajatec
(	(	kIx(	(
<g/>
nejenom	nejenom	k6eAd1	nejenom
váleční	váleční	k2eAgMnPc1d1	váleční
v	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
smyslu	smysl	k1gInSc6	smysl
ale	ale	k8xC	ale
i	i	k8xC	i
civilisté	civilista	k1gMnPc1	civilista
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
za	za	k7c4	za
ně	on	k3xPp3gMnPc4	on
nemohla	moct	k5eNaImAgFnS	moct
být	být	k5eAaImF	být
vyplacena	vyplacen	k2eAgFnSc1d1	vyplacena
odměna	odměna	k1gFnSc1	odměna
<g/>
,	,	kIx,	,
nezřídka	nezřídka	k6eAd1	nezřídka
popravováni	popravován	k2eAgMnPc1d1	popravován
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
výjimečný	výjimečný	k2eAgInSc4d1	výjimečný
případ	případ	k1gInSc4	případ
byla	být	k5eAaImAgFnS	být
vražda	vražda	k1gFnSc1	vražda
císařského	císařský	k2eAgMnSc2d1	císařský
generála	generál	k1gMnSc2	generál
Karla	Karel	k1gMnSc2	Karel
Bonaventury	Bonaventura	k1gFnSc2	Bonaventura
Buquoye	Buquoy	k1gMnSc2	Buquoy
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
ostatních	ostatní	k2eAgFnPc2d1	ostatní
nezúčastněných	zúčastněný	k2eNgFnPc2d1	nezúčastněná
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
konfliktů	konflikt	k1gInPc2	konflikt
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
==	==	k?	==
</s>
</p>
<p>
<s>
Třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
nezaměstnávala	zaměstnávat	k5eNaImAgFnS	zaměstnávat
zdaleka	zdaleka	k6eAd1	zdaleka
celou	celý	k2eAgFnSc4d1	celá
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
prvé	prvý	k4xOgFnPc4	prvý
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
konflikt	konflikt	k1gInSc1	konflikt
mezi	mezi	k7c7	mezi
Španělskem	Španělsko	k1gNnSc7	Španělsko
a	a	k8xC	a
Nizozemskem	Nizozemsko	k1gNnSc7	Nizozemsko
<g/>
,	,	kIx,	,
osmdesátiletá	osmdesátiletý	k2eAgFnSc1d1	osmdesátiletá
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
po	po	k7c6	po
uplynutí	uplynutí	k1gNnSc6	uplynutí
příměří	příměří	k1gNnSc2	příměří
znovu	znovu	k6eAd1	znovu
rozhořel	rozhořet	k5eAaPmAgInS	rozhořet
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
účastnily	účastnit	k5eAaImAgInP	účastnit
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgFnP	být
činné	činný	k2eAgInPc1d1	činný
ve	v	k7c6	v
třicetileté	třicetiletý	k2eAgFnSc6d1	třicetiletá
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
splývají	splývat	k5eAaImIp3nP	splývat
tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
konflikty	konflikt	k1gInPc1	konflikt
a	a	k8xC	a
končí	končit	k5eAaImIp3nP	končit
stejným	stejný	k2eAgInSc7d1	stejný
mírem	mír	k1gInSc7	mír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
země	zem	k1gFnPc1	zem
se	se	k3xPyFc4	se
války	válek	k1gInPc7	válek
nezúčastnily	zúčastnit	k5eNaPmAgFnP	zúčastnit
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
téměř	téměř	k6eAd1	téměř
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1	ruské
carství	carství	k1gNnSc1	carství
se	se	k3xPyFc4	se
nezapojilo	zapojit	k5eNaPmAgNnS	zapojit
vojensky	vojensky	k6eAd1	vojensky
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
podporovalo	podporovat	k5eAaImAgNnS	podporovat
protihabsburskou	protihabsburský	k2eAgFnSc4d1	protihabsburská
koalici	koalice	k1gFnSc4	koalice
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
dodáváním	dodávání	k1gNnSc7	dodávání
obilí	obilí	k1gNnSc1	obilí
Anglii	Anglie	k1gFnSc3	Anglie
a	a	k8xC	a
Spojeným	spojený	k2eAgFnPc3d1	spojená
provinciím	provincie	k1gFnPc3	provincie
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Dánskému	dánský	k2eAgNnSc3d1	dánské
a	a	k8xC	a
Norskému	norský	k2eAgNnSc3d1	norské
království	království	k1gNnSc3	království
a	a	k8xC	a
Švédsku	Švédsko	k1gNnSc3	Švédsko
a	a	k8xC	a
po	po	k7c6	po
lübeckém	lübecký	k2eAgInSc6d1	lübecký
míru	mír	k1gInSc6	mír
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1629	[number]	k4	1629
již	již	k6eAd1	již
jen	jen	k8xS	jen
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
vyváželo	vyvážet	k5eAaImAgNnS	vyvážet
také	také	k9	také
ledek	ledek	k1gInSc4	ledek
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
zemí	zem	k1gFnPc2	zem
spolčeným	spolčený	k2eAgInSc7d1	spolčený
proti	proti	k7c3	proti
Habsburkům	Habsburk	k1gMnPc3	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
Ruska	Rusko	k1gNnSc2	Rusko
je	být	k5eAaImIp3nS	být
také	také	k9	také
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1632	[number]	k4	1632
<g/>
–	–	k?	–
<g/>
1634	[number]	k4	1634
účastnilo	účastnit	k5eAaImAgNnS	účastnit
konfliktu	konflikt	k1gInSc3	konflikt
s	s	k7c7	s
katolickou	katolický	k2eAgFnSc7d1	katolická
Republikou	republika	k1gFnSc7	republika
obou	dva	k4xCgInPc2	dva
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
konflikt	konflikt	k1gInSc1	konflikt
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
smolenskou	smolenský	k2eAgFnSc4d1	smolenská
válku	válka	k1gFnSc4	válka
<g/>
,	,	kIx,	,
sice	sice	k8xC	sice
Rusko	Rusko	k1gNnSc1	Rusko
prohrálo	prohrát	k5eAaPmAgNnS	prohrát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
tak	tak	k9	tak
Švédům	Švéd	k1gMnPc3	Švéd
bojovat	bojovat	k5eAaImF	bojovat
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nP	by
byli	být	k5eAaImAgMnP	být
napadeni	napadnout	k5eAaPmNgMnP	napadnout
polskými	polský	k2eAgFnPc7d1	polská
armádami	armáda	k1gFnPc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
ruskou	ruský	k2eAgFnSc4d1	ruská
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
"	"	kIx"	"
<g/>
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
"	"	kIx"	"
obvykle	obvykle	k6eAd1	obvykle
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
především	především	k9	především
ruští	ruský	k2eAgMnPc1d1	ruský
historici	historik	k1gMnPc1	historik
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
o	o	k7c6	o
její	její	k3xOp3gFnSc6	její
skutečném	skutečný	k2eAgInSc6d1	skutečný
významu	význam	k1gInSc6	význam
mohou	moct	k5eAaImIp3nP	moct
panovat	panovat	k5eAaImF	panovat
určité	určitý	k2eAgFnPc4d1	určitá
pochybnosti	pochybnost	k1gFnPc4	pochybnost
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
Republika	republika	k1gFnSc1	republika
obou	dva	k4xCgInPc2	dva
národů	národ	k1gInPc2	národ
bojovala	bojovat	k5eAaImAgFnS	bojovat
například	například	k6eAd1	například
také	také	k9	také
s	s	k7c7	s
Osmanskou	osmanský	k2eAgFnSc7d1	Osmanská
říší	říš	k1gFnSc7	říš
a	a	k8xC	a
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
víceméně	víceméně	k9	víceméně
pouze	pouze	k6eAd1	pouze
vysláním	vyslání	k1gNnSc7	vyslání
kozáckých	kozácký	k2eAgInPc2d1	kozácký
lisovčíků	lisovčík	k1gInPc2	lisovčík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgInSc1d1	další
lokální	lokální	k2eAgInSc1d1	lokální
konflikt	konflikt	k1gInSc1	konflikt
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
stoupenci	stoupenec	k1gMnPc7	stoupenec
protihabsburského	protihabsburský	k2eAgInSc2d1	protihabsburský
tábora	tábor	k1gInSc2	tábor
<g/>
,	,	kIx,	,
Dánsko-Norským	dánskoorský	k2eAgNnSc7d1	dánsko-norský
královstvím	království	k1gNnSc7	království
a	a	k8xC	a
Švédskem	Švédsko	k1gNnSc7	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Dánsko	Dánsko	k1gNnSc4	Dánsko
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
obklopovalo	obklopovat	k5eAaImAgNnS	obklopovat
Švédsko	Švédsko	k1gNnSc1	Švédsko
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
<g/>
,	,	kIx,	,
západu	západ	k1gInSc2	západ
a	a	k8xC	a
severu	sever	k1gInSc2	sever
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
již	již	k9	již
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
znepokojovalo	znepokojovat	k5eAaImAgNnS	znepokojovat
švédské	švédský	k2eAgMnPc4d1	švédský
panovníky	panovník	k1gMnPc4	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Švédové	Švéd	k1gMnPc1	Švéd
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
již	již	k6eAd1	již
dobyli	dobýt	k5eAaPmAgMnP	dobýt
mnoho	mnoho	k4c4	mnoho
úspěchů	úspěch	k1gInPc2	úspěch
v	v	k7c6	v
třicetileté	třicetiletý	k2eAgFnSc6d1	třicetiletá
válce	válka	k1gFnSc6	válka
stáhli	stáhnout	k5eAaPmAgMnP	stáhnout
roku	rok	k1gInSc2	rok
1643	[number]	k4	1643
Lennarta	Lennart	k1gMnSc2	Lennart
Torstensona	Torstenson	k1gMnSc2	Torstenson
ze	z	k7c2	z
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
Dánsku	Dánsko	k1gNnSc6	Dánsko
válku	válek	k1gInSc3	válek
<g/>
.	.	kIx.	.
</s>
<s>
Protihabsburský	protihabsburský	k2eAgInSc1d1	protihabsburský
tábor	tábor	k1gInSc1	tábor
tak	tak	k6eAd1	tak
ztratil	ztratit	k5eAaPmAgInS	ztratit
dva	dva	k4xCgMnPc4	dva
spojence	spojenec	k1gMnPc4	spojenec
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
samotné	samotný	k2eAgNnSc1d1	samotné
Dánsko	Dánsko	k1gNnSc1	Dánsko
již	již	k6eAd1	již
války	válka	k1gFnSc2	válka
příliš	příliš	k6eAd1	příliš
neúčastnilo	účastnit	k5eNaImAgNnS	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
torstensonská	torstensonský	k2eAgFnSc1d1	torstensonský
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
probíhalo	probíhat	k5eAaImAgNnS	probíhat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1645	[number]	k4	1645
<g/>
,	,	kIx,	,
skončila	skončit	k5eAaPmAgFnS	skončit
pro	pro	k7c4	pro
Dánsko	Dánsko	k1gNnSc4	Dánsko
nepříznivě	příznivě	k6eNd1	příznivě
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
ukončení	ukončení	k1gNnSc6	ukončení
se	se	k3xPyFc4	se
postaral	postarat	k5eAaPmAgMnS	postarat
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
i	i	k9	i
kardinál	kardinál	k1gMnSc1	kardinál
Mazarin	Mazarin	k1gInSc1	Mazarin
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
Švédy	švéda	k1gFnSc2	švéda
k	k	k7c3	k
dalšímu	další	k2eAgInSc3d1	další
boji	boj	k1gInSc3	boj
proti	proti	k7c3	proti
Habsburkům	Habsburk	k1gMnPc3	Habsburk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tato	tento	k3xDgFnSc1	tento
válka	válka	k1gFnSc1	válka
neměla	mít	k5eNaImAgFnS	mít
výrazný	výrazný	k2eAgInSc4d1	výrazný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
pozice	pozice	k1gFnPc4	pozice
protihabsburské	protihabsburský	k2eAgFnSc2d1	protihabsburská
koalice	koalice	k1gFnSc2	koalice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
umělecké	umělecký	k2eAgFnSc6d1	umělecká
literatuře	literatura	k1gFnSc6	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
otřásla	otřást	k5eAaPmAgFnS	otřást
společností	společnost	k1gFnSc7	společnost
celé	celý	k2eAgFnSc2d1	celá
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
vliv	vliv	k1gInSc1	vliv
byl	být	k5eAaImAgInS	být
patrný	patrný	k2eAgInSc1d1	patrný
především	především	k9	především
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
literatuře	literatura	k1gFnSc6	literatura
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
díla	dílo	k1gNnSc2	dílo
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Hans	Hans	k1gMnSc1	Hans
Jakob	Jakob	k1gMnSc1	Jakob
Christoffel	Christoffel	k1gMnSc1	Christoffel
von	von	k1gInSc4	von
Grimmelshausen	Grimmelshausen	k2eAgInSc4d1	Grimmelshausen
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
poměrně	poměrně	k6eAd1	poměrně
realisticky	realisticky	k6eAd1	realisticky
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
tato	tento	k3xDgNnPc4	tento
díla	dílo	k1gNnPc4	dílo
patří	patřit	k5eAaImIp3nS	patřit
Simplicius	Simplicius	k1gMnSc1	Simplicius
Simplicissimus	Simplicissimus	k1gMnSc1	Simplicissimus
<g/>
,	,	kIx,	,
Divous	divousit	k5eAaImRp2nS	divousit
Skočdopole	Skočdopole	k1gFnSc2	Skočdopole
<g/>
,	,	kIx,	,
Poběhlice	poběhlice	k1gFnSc2	poběhlice
Kuráž	Kuráž	k?	Kuráž
<g/>
.	.	kIx.	.
</s>
<s>
Básníci	básník	k1gMnPc1	básník
reagovali	reagovat	k5eAaBmAgMnP	reagovat
na	na	k7c4	na
obecnou	obecný	k2eAgFnSc4d1	obecná
skleslost	skleslost	k1gFnSc4	skleslost
příklonem	příklon	k1gInSc7	příklon
k	k	k7c3	k
mysticismu	mysticismus	k1gInSc3	mysticismus
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Angela	angel	k1gMnSc2	angel
Silesia	Silesius	k1gMnSc2	Silesius
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
války	válka	k1gFnSc2	válka
působila	působit	k5eAaImAgFnS	působit
také	také	k9	také
slezská	slezský	k2eAgFnSc1d1	Slezská
básnická	básnický	k2eAgFnSc1d1	básnická
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
zakladatel	zakladatel	k1gMnSc1	zakladatel
slezské	slezský	k2eAgFnSc2d1	Slezská
školy	škola	k1gFnSc2	škola
Martin	Martin	k1gMnSc1	Martin
Opitz	Opitz	k1gMnSc1	Opitz
vyzýval	vyzývat	k5eAaImAgMnS	vyzývat
v	v	k7c6	v
Knize	kniha	k1gFnSc6	kniha
o	o	k7c6	o
německé	německý	k2eAgFnSc6d1	německá
poezii	poezie	k1gFnSc6	poezie
k	k	k7c3	k
popisování	popisování	k1gNnSc3	popisování
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jak	jak	k8xC	jak
by	by	kYmCp3nP	by
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
<g/>
,	,	kIx,	,
ne	ne	k9	ne
tak	tak	k9	tak
jsou	být	k5eAaImIp3nP	být
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Písně	píseň	k1gFnSc2	píseň
útěchy	útěcha	k1gFnSc2	útěcha
o	o	k7c6	o
válečných	válečný	k2eAgFnPc6d1	válečná
útrapách	útrapa	k1gFnPc6	útrapa
popisuje	popisovat	k5eAaImIp3nS	popisovat
válečné	válečný	k2eAgInPc4d1	válečný
útrapy	útrap	k1gInPc4	útrap
bez	bez	k7c2	bez
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
idealizace	idealizace	k1gFnSc2	idealizace
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
ideálem	ideál	k1gInSc7	ideál
bylo	být	k5eAaImAgNnS	být
národní	národní	k2eAgNnSc1d1	národní
spojení	spojení	k1gNnSc1	spojení
a	a	k8xC	a
mír	mír	k1gInSc1	mír
<g/>
.	.	kIx.	.
</s>
<s>
Opitze	Opitze	k6eAd1	Opitze
následovali	následovat	k5eAaImAgMnP	následovat
ve	v	k7c6	v
slezské	slezský	k2eAgFnSc6d1	Slezská
škole	škola	k1gFnSc6	škola
např.	např.	kA	např.
Georg	Georg	k1gInSc1	Georg
Rodolf	Rodolf	k1gInSc1	Rodolf
Weckherlin	Weckherlin	k1gInSc1	Weckherlin
nebo	nebo	k8xC	nebo
Johann	Johann	k1gInSc1	Johann
von	von	k1gInSc1	von
Rist	Rista	k1gFnPc2	Rista
<g/>
,	,	kIx,	,
ti	ten	k3xDgMnPc1	ten
si	se	k3xPyFc3	se
vybrali	vybrat	k5eAaPmAgMnP	vybrat
cestu	cesta	k1gFnSc4	cesta
satiry	satira	k1gFnSc2	satira
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
poměrů	poměr	k1gInPc2	poměr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kritice	kritika	k1gFnSc6	kritika
a	a	k8xC	a
satiričnosti	satiričnost	k1gFnSc6	satiričnost
doby	doba	k1gFnSc2	doba
a	a	k8xC	a
především	především	k9	především
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
vrstvy	vrstva	k1gFnSc2	vrstva
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
Johann	Johann	k1gMnSc1	Johann
Michael	Michael	k1gMnSc1	Michael
Moscherosch	Moscherosch	k1gMnSc1	Moscherosch
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
románu	román	k1gInSc6	román
Podivné	podivný	k2eAgNnSc1d1	podivné
a	a	k8xC	a
pravdivé	pravdivý	k2eAgNnSc1d1	pravdivé
zjevení	zjevení	k1gNnSc1	zjevení
Philandera	Philander	k1gMnSc2	Philander
ze	z	k7c2	z
Sittewaldu	Sittewald	k1gInSc2	Sittewald
(	(	kIx(	(
<g/>
1640	[number]	k4	1640
<g/>
–	–	k?	–
<g/>
1643	[number]	k4	1643
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
na	na	k7c6	na
příběhu	příběh	k1gInSc6	příběh
hrdiny	hrdina	k1gMnSc2	hrdina
velmi	velmi	k6eAd1	velmi
obsáhle	obsáhle	k6eAd1	obsáhle
popisuje	popisovat	k5eAaImIp3nS	popisovat
život	život	k1gInSc4	život
během	během	k7c2	během
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Satirické	satirický	k2eAgFnPc4d1	satirická
básně	báseň	k1gFnPc4	báseň
potom	potom	k6eAd1	potom
psal	psát	k5eAaImAgMnS	psát
Friedrich	Friedrich	k1gMnSc1	Friedrich
von	von	k1gInSc4	von
Logau	Logaus	k1gInSc2	Logaus
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
Opitzův	Opitzův	k2eAgMnSc1d1	Opitzův
žák	žák	k1gMnSc1	žák
Paul	Paul	k1gMnSc1	Paul
Fleming	Fleming	k1gInSc1	Fleming
(	(	kIx(	(
<g/>
1609	[number]	k4	1609
<g/>
–	–	k?	–
<g/>
1640	[number]	k4	1640
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lyrický	lyrický	k2eAgMnSc1d1	lyrický
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
vlastenec	vlastenec	k1gMnSc1	vlastenec
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
obrací	obracet	k5eAaImIp3nS	obracet
k	k	k7c3	k
vášni	vášeň	k1gFnSc3	vášeň
<g/>
,	,	kIx,	,
milostnému	milostný	k2eAgNnSc3d1	milostné
vzplanutí	vzplanutí	k1gNnSc3	vzplanutí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pomíjivosti	pomíjivost	k1gFnSc2	pomíjivost
vezdejšího	vezdejší	k2eAgInSc2d1	vezdejší
světa	svět	k1gInSc2	svět
a	a	k8xC	a
utrpení	utrpení	k1gNnSc2	utrpení
otčiny	otčina	k1gFnSc2	otčina
v	v	k7c6	v
Panu	Pan	k1gMnSc6	Pan
Olearovi	Olear	k1gMnSc6	Olear
(	(	kIx(	(
<g/>
1636	[number]	k4	1636
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Válku	válka	k1gFnSc4	válka
jako	jako	k9	jako
tříštící	tříštící	k2eAgInSc1d1	tříštící
a	a	k8xC	a
plenící	plenící	k2eAgInSc1d1	plenící
faktor	faktor	k1gInSc1	faktor
popisuje	popisovat	k5eAaImIp3nS	popisovat
v	v	k7c6	v
Ódě	óda	k1gFnSc6	óda
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
rok	rok	k1gInSc4	rok
1633	[number]	k4	1633
<g/>
.	.	kIx.	.
<g/>
I	i	k9	i
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
přímý	přímý	k2eAgInSc4d1	přímý
vliv	vliv	k1gInSc4	vliv
války	válka	k1gFnSc2	válka
pominul	pominout	k5eAaPmAgMnS	pominout
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
autoři	autor	k1gMnPc1	autor
několikrát	několikrát	k6eAd1	několikrát
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
tématu	téma	k1gNnSc3	téma
vrátili	vrátit	k5eAaPmAgMnP	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Defoe	Defo	k1gInSc2	Defo
ve	v	k7c6	v
Vzpomínkách	vzpomínka	k1gFnPc6	vzpomínka
kavalíra	kavalír	k1gMnSc2	kavalír
(	(	kIx(	(
<g/>
1720	[number]	k4	1720
<g/>
)	)	kIx)	)
popsal	popsat	k5eAaPmAgMnS	popsat
příběh	příběh	k1gInSc4	příběh
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
a	a	k8xC	a
anglické	anglický	k2eAgFnSc2d1	anglická
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vrátil	vrátit	k5eAaPmAgInS	vrátit
další	další	k2eAgMnSc1d1	další
Němec	Němec	k1gMnSc1	Němec
Bertolt	Bertolt	k1gMnSc1	Bertolt
Brecht	Brecht	k1gMnSc1	Brecht
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
použil	použít	k5eAaPmAgMnS	použít
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
Matku	matka	k1gFnSc4	matka
Kuráž	Kuráž	k?	Kuráž
a	a	k8xC	a
její	její	k3xOp3gFnPc1	její
děti	dítě	k1gFnPc1	dítě
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
historické	historický	k2eAgInPc4d1	historický
motivy	motiv	k1gInPc4	motiv
od	od	k7c2	od
Grimmelshausena	Grimmelshausen	k2eAgNnPc4d1	Grimmelshausen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
potom	potom	k6eAd1	potom
život	život	k1gInSc1	život
Albrechta	Albrecht	k1gMnSc2	Albrecht
z	z	k7c2	z
Valdštejna	Valdštejn	k1gInSc2	Valdštejn
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
díla	dílo	k1gNnSc2	dílo
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Durycha	Durych	k1gMnSc2	Durych
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
patrno	patrn	k2eAgNnSc1d1	patrno
v	v	k7c6	v
Bloudění	bloudění	k1gNnSc6	bloudění
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
Rekviem	rekviem	k1gNnSc6	rekviem
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
též	též	k9	též
stojí	stát	k5eAaImIp3nS	stát
František	František	k1gMnSc1	František
Kubka	Kubka	k1gMnSc1	Kubka
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
dvěma	dva	k4xCgInPc7	dva
historickými	historický	k2eAgInPc7d1	historický
romány	román	k1gInPc7	román
Říkali	říkat	k5eAaImAgMnP	říkat
mu	on	k3xPp3gMnSc3	on
Ječmínek	ječmínek	k1gInSc4	ječmínek
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
a	a	k8xC	a
Ječmínkův	Ječmínkův	k2eAgInSc1d1	Ječmínkův
návrat	návrat	k1gInSc1	návrat
(	(	kIx(	(
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
popisujícími	popisující	k2eAgInPc7d1	popisující
život	život	k1gInSc4	život
mladého	mladý	k2eAgMnSc2d1	mladý
moravského	moravský	k2eAgMnSc2d1	moravský
rytíře	rytíř	k1gMnSc2	rytíř
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
u	u	k7c2	u
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Fridricha	Fridrich	k1gMnSc2	Fridrich
a	a	k8xC	a
který	který	k3yIgInSc1	který
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
prožívá	prožívat	k5eAaImIp3nS	prožívat
útrapy	útrapa	k1gFnPc4	útrapa
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ABELINUS	ABELINUS	kA	ABELINUS
<g/>
,	,	kIx,	,
Johann	Johann	k1gMnSc1	Johann
Philipp	Philipp	k1gMnSc1	Philipp
<g/>
.	.	kIx.	.
</s>
<s>
Theatrum	Theatrum	k1gNnSc4	Theatrum
Europaeum	Europaeum	k1gNnSc1	Europaeum
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
<g/>
:	:	kIx,	:
Matthäus	Matthäus	k1gMnSc1	Matthäus
Merian	Merian	k1gMnSc1	Merian
<g/>
,	,	kIx,	,
1618	[number]	k4	1618
<g/>
–	–	k?	–
<g/>
1718	[number]	k4	1718
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
němčina	němčina	k1gFnSc1	němčina
<g/>
,	,	kIx,	,
latina	latina	k1gFnSc1	latina
<g/>
)	)	kIx)	)
Rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
dobové	dobový	k2eAgNnSc1d1	dobové
dílo	dílo	k1gNnSc1	dílo
zabývající	zabývající	k2eAgMnSc1d1	zabývající
se	se	k3xPyFc4	se
především	především	k6eAd1	především
třicetiletou	třicetiletý	k2eAgFnSc7d1	třicetiletá
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
doplněno	doplněn	k2eAgNnSc1d1	doplněno
mědirytinami	mědirytina	k1gFnPc7	mědirytina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BALCÁREK	Balcárek	k1gMnSc1	Balcárek
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
víru	vír	k1gInSc6	vír
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
:	:	kIx,	:
politikové	politik	k1gMnPc1	politik
<g/>
,	,	kIx,	,
kondotiéři	kondotiér	k1gMnPc1	kondotiér
<g/>
,	,	kIx,	,
rebelové	rebel	k1gMnPc1	rebel
a	a	k8xC	a
mučedníci	mučedník	k1gMnPc1	mučedník
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
:	:	kIx,	:
Veduta	veduta	k1gFnSc1	veduta
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
510	[number]	k4	510
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86829	[number]	k4	86829
<g/>
-	-	kIx~	-
<g/>
61	[number]	k4	61
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČORNEJOVÁ	ČORNEJOVÁ	kA	ČORNEJOVÁ
<g/>
,	,	kIx,	,
Ivana	Ivana	k1gFnSc1	Ivana
<g/>
;	;	kIx,	;
MIKULEC	Mikulec	k1gMnSc1	Mikulec
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
VLNAS	VLNAS	kA	VLNAS
<g/>
,	,	kIx,	,
Vít	Vít	k1gMnSc1	Vít
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
dějiny	dějiny	k1gFnPc1	dějiny
zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
1618	[number]	k4	1618
<g/>
-	-	kIx~	-
<g/>
1683	[number]	k4	1683
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
711	[number]	k4	711
s.	s.	k?	s.
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
32	[number]	k4	32
<g/>
]	]	kIx)	]
s.	s.	k?	s.
barev	barva	k1gFnPc2	barva
<g/>
.	.	kIx.	.
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
příl	příl	k1gMnSc1	příl
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
947	[number]	k4	947
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DRŠKA	drška	k1gFnSc1	drška
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
;	;	kIx,	;
STELLNER	STELLNER	kA	STELLNER
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
;	;	kIx,	;
SKŘIVAN	Skřivan	k1gMnSc1	Skřivan
<g/>
,	,	kIx,	,
Aleš	Aleš	k1gMnSc1	Aleš
<g/>
.	.	kIx.	.
</s>
<s>
Kapitoly	kapitola	k1gFnPc4	kapitola
z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
evropské	evropský	k2eAgFnSc2d1	Evropská
politiky	politika	k1gFnSc2	politika
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1648	[number]	k4	1648
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Institut	institut	k1gInSc1	institut
pro	pro	k7c4	pro
středoevropskou	středoevropský	k2eAgFnSc4d1	středoevropská
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
politiku	politika	k1gFnSc4	politika
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
205	[number]	k4	205
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85241	[number]	k4	85241
<g/>
-	-	kIx~	-
<g/>
87	[number]	k4	87
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ENGLUND	ENGLUND	kA	ENGLUND
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
<g/>
.	.	kIx.	.
</s>
<s>
Nepokojná	pokojný	k2eNgNnPc1d1	nepokojné
léta	léto	k1gNnPc1	léto
<g/>
:	:	kIx,	:
historie	historie	k1gFnSc2	historie
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
NLN	NLN	kA	NLN
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
679	[number]	k4	679
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
355	[number]	k4	355
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
FUKALA	Fukal	k1gMnSc4	Fukal
<g/>
,	,	kIx,	,
Radek	Radek	k1gMnSc1	Radek
<g/>
.	.	kIx.	.
</s>
<s>
Sen	sen	k1gInSc1	sen
o	o	k7c6	o
odplatě	odplata	k1gFnSc6	odplata
<g/>
:	:	kIx,	:
dramata	drama	k1gNnPc1	drama
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Epocha	epocha	k1gFnSc1	epocha
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
389	[number]	k4	389
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86328	[number]	k4	86328
<g/>
-	-	kIx~	-
<g/>
84	[number]	k4	84
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FUKALA	Fukal	k1gMnSc4	Fukal
<g/>
,	,	kIx,	,
Radek	Radek	k1gMnSc1	Radek
<g/>
.	.	kIx.	.
</s>
<s>
Třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
všeobecný	všeobecný	k2eAgInSc4d1	všeobecný
evropský	evropský	k2eAgInSc4d1	evropský
konflikt	konflikt	k1gInSc4	konflikt
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
:	:	kIx,	:
otázky	otázka	k1gFnPc4	otázka
<g/>
,	,	kIx,	,
úvahy	úvaha	k1gFnPc4	úvaha
a	a	k8xC	a
problémy	problém	k1gInPc4	problém
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
:	:	kIx,	:
Veduta	veduta	k1gFnSc1	veduta
<g/>
,	,	kIx,	,
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
193	[number]	k4	193
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86829	[number]	k4	86829
<g/>
-	-	kIx~	-
<g/>
81	[number]	k4	81
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KLUČINA	klučina	k1gMnSc1	klučina
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
<g/>
:	:	kIx,	:
obraz	obraz	k1gInSc1	obraz
doby	doba	k1gFnSc2	doba
1618	[number]	k4	1618
<g/>
-	-	kIx~	-
<g/>
1648	[number]	k4	1648
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
409	[number]	k4	409
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
663	[number]	k4	663
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MUNCK	MUNCK	kA	MUNCK
<g/>
,	,	kIx,	,
Thomas	Thomas	k1gMnSc1	Thomas
<g/>
.	.	kIx.	.
</s>
<s>
Evropa	Evropa	k1gFnSc1	Evropa
sedmnáctého	sedmnáctý	k4xOgInSc2	sedmnáctý
století	století	k1gNnSc2	století
<g/>
:	:	kIx,	:
1598	[number]	k4	1598
<g/>
-	-	kIx~	-
<g/>
1700	[number]	k4	1700
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
468	[number]	k4	468
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
508	[number]	k4	508
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PERNES	PERNES	kA	PERNES
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
FUČÍK	Fučík	k1gMnSc1	Fučík
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
;	;	kIx,	;
HAVEL	Havel	k1gMnSc1	Havel
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
císařským	císařský	k2eAgInSc7d1	císařský
praporem	prapor	k1gInSc7	prapor
<g/>
:	:	kIx,	:
historie	historie	k1gFnSc1	historie
habsburské	habsburský	k2eAgFnSc2d1	habsburská
armády	armáda	k1gFnSc2	armáda
1526	[number]	k4	1526
<g/>
-	-	kIx~	-
<g/>
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
18	[number]	k4	18
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Elka	Elka	k1gFnSc1	Elka
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
555	[number]	k4	555
s.	s.	k?	s.
(	(	kIx(	(
<g/>
Militaria	Militarium	k1gNnSc2	Militarium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902745	[number]	k4	902745
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PETRÁŇ	Petráň	k1gMnSc1	Petráň
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Staroměstská	staroměstský	k2eAgFnSc1d1	Staroměstská
exekuce	exekuce	k1gFnSc1	exekuce
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
autorem	autor	k1gMnSc7	autor
dopl	dopnout	k5eAaPmAgMnS	dopnout
<g/>
.	.	kIx.	.
a	a	k8xC	a
přeprac	přeprac	k1gFnSc1	přeprac
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
318	[number]	k4	318
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86695	[number]	k4	86695
<g/>
-	-	kIx~	-
<g/>
44	[number]	k4	44
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
POLIŠENSKÝ	POLIŠENSKÝ	kA	POLIŠENSKÝ
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
evropské	evropský	k2eAgFnPc1d1	Evropská
krize	krize	k1gFnPc1	krize
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
284	[number]	k4	284
s.	s.	k?	s.
</s>
</p>
<p>
<s>
PRÁŠEK	Prášek	k1gMnSc1	Prášek
<g/>
,	,	kIx,	,
Justin	Justin	k1gMnSc1	Justin
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
exulanti	exulant	k1gMnPc1	exulant
ve	v	k7c6	v
vojskách	vojsko	k1gNnPc6	vojsko
války	válka	k1gFnSc2	válka
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
<g/>
.	.	kIx.	.
</s>
<s>
Československý	československý	k2eAgMnSc1d1	československý
legionář	legionář	k1gMnSc1	legionář
<g/>
.	.	kIx.	.
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
</s>
<s>
IV	IV	kA	IV
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
,	,	kIx,	,
10	[number]	k4	10
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
REZEK	Rezek	k1gMnSc1	Rezek
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Saského	saský	k2eAgInSc2d1	saský
vpádu	vpád	k1gInSc2	vpád
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
(	(	kIx(	(
<g/>
1631	[number]	k4	1631
<g/>
-	-	kIx~	-
<g/>
1632	[number]	k4	1632
<g/>
)	)	kIx)	)
a	a	k8xC	a
návrat	návrat	k1gInSc4	návrat
emigrace	emigrace	k1gFnSc2	emigrace
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
I.L.	I.L.	k1gFnSc1	I.L.
Kober	kobra	k1gFnPc2	kobra
<g/>
,	,	kIx,	,
1889	[number]	k4	1889
<g/>
.	.	kIx.	.
173	[number]	k4	173
s.	s.	k?	s.
</s>
</p>
<p>
<s>
SKUTIL	skutit	k5eAaPmAgMnS	skutit
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
,	,	kIx,	,
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Morava	Morava	k1gFnSc1	Morava
a	a	k8xC	a
Brno	Brno	k1gNnSc1	Brno
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
konference	konference	k1gFnSc1	konference
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
.	.	kIx.	.
a	a	k8xC	a
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1995	[number]	k4	1995
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Societas	Societas	k1gInSc1	Societas
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
287	[number]	k4	287
s.	s.	k?	s.
</s>
</p>
<p>
<s>
ŠINDELÁŘ	Šindelář	k1gMnSc1	Šindelář
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
<g/>
.	.	kIx.	.
</s>
<s>
Vestfálský	vestfálský	k2eAgInSc1d1	vestfálský
mír	mír	k1gInSc1	mír
a	a	k8xC	a
česká	český	k2eAgFnSc1d1	Česká
otázka	otázka	k1gFnSc1	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
378	[number]	k4	378
s.	s.	k?	s.
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
bibliografii	bibliografie	k1gFnSc4	bibliografie
<g/>
,	,	kIx,	,
bibliografické	bibliografický	k2eAgInPc4d1	bibliografický
odkazy	odkaz	k1gInPc4	odkaz
a	a	k8xC	a
rejstřík	rejstřík	k1gInSc1	rejstřík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TAPIÉ	TAPIÉ	kA	TAPIÉ
<g/>
,	,	kIx,	,
Victor	Victor	k1gMnSc1	Victor
Lucien	Lucien	k1gInSc1	Lucien
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
Hora	hora	k1gFnSc1	hora
a	a	k8xC	a
francouzská	francouzský	k2eAgFnSc1d1	francouzská
politika	politika	k1gFnSc1	politika
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kalista	Kalista	k1gMnSc1	Kalista
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
499	[number]	k4	499
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
bitev	bitva	k1gFnPc2	bitva
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
osobností	osobnost	k1gFnPc2	osobnost
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
</s>
</p>
<p>
<s>
Události	událost	k1gFnPc1	událost
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
</s>
</p>
<p>
<s>
Hakapelité	Hakapelitý	k2eAgNnSc1d1	Hakapelitý
</s>
</p>
<p>
<s>
České	český	k2eAgNnSc1d1	české
stavovské	stavovský	k2eAgNnSc1d1	Stavovské
povstání	povstání	k1gNnSc1	povstání
</s>
</p>
<p>
<s>
Pražská	pražský	k2eAgFnSc1d1	Pražská
defenestrace	defenestrace	k1gFnSc1	defenestrace
(	(	kIx(	(
<g/>
1618	[number]	k4	1618
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Staroměstská	staroměstský	k2eAgFnSc1d1	Staroměstská
exekuce	exekuce	k1gFnSc1	exekuce
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Dreißigjähriger	Dreißigjährigra	k1gFnPc2	Dreißigjährigra
Krieg	Kriega	k1gFnPc2	Kriega
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
ve	v	k7c6	v
Vlastenském	vlastenský	k2eAgInSc6d1	vlastenský
slovníku	slovník	k1gInSc6	slovník
historickém	historický	k2eAgInSc6d1	historický
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
</s>
</p>
<p>
<s>
Téma	téma	k1gFnSc1	téma
Třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
o	o	k7c6	o
Švédsku	Švédsko	k1gNnSc6	Švédsko
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Ztráty	ztráta	k1gFnPc1	ztráta
na	na	k7c6	na
životech	život	k1gInPc6	život
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
během	během	k7c2	během
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
v	v	k7c6	v
procentech	procento	k1gNnPc6	procento
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Thirty	Thirta	k1gFnSc2	Thirta
Years	Yearsa	k1gFnPc2	Yearsa
War	War	k1gMnSc1	War
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Thirty	Thirta	k1gFnSc2	Thirta
Years	Yearsa	k1gFnPc2	Yearsa
War	War	k1gMnSc1	War
–	–	k?	–
článek	článek	k1gInSc1	článek
na	na	k7c6	na
Katolické	katolický	k2eAgFnSc6d1	katolická
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
</s>
</p>
