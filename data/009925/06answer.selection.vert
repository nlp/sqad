<s>
Oxid	oxid	k1gInSc1	oxid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
(	(	kIx(	(
<g/>
chemický	chemický	k2eAgInSc1d1	chemický
vzorec	vzorec	k1gInSc1	vzorec
CaO	CaO	k1gFnSc2	CaO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
triviálními	triviální	k2eAgInPc7d1	triviální
názvy	název	k1gInPc7	název
pálené	pálená	k1gFnSc2	pálená
vápno	vápno	k1gNnSc4	vápno
nebo	nebo	k8xC	nebo
též	též	k9	též
nehašené	hašený	k2eNgNnSc4d1	nehašené
vápno	vápno	k1gNnSc4	vápno
je	být	k5eAaImIp3nS	být
široce	široko	k6eAd1	široko
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
běžně	běžně	k6eAd1	běžně
používaná	používaný	k2eAgFnSc1d1	používaná
chemická	chemický	k2eAgFnSc1d1	chemická
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
.	.	kIx.	.
</s>
