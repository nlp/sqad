<p>
<s>
Oxid	oxid	k1gInSc1	oxid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
(	(	kIx(	(
<g/>
chemický	chemický	k2eAgInSc1d1	chemický
vzorec	vzorec	k1gInSc1	vzorec
CaO	CaO	k1gFnSc2	CaO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
triviálními	triviální	k2eAgInPc7d1	triviální
názvy	název	k1gInPc7	název
pálené	pálená	k1gFnSc2	pálená
vápno	vápno	k1gNnSc4	vápno
nebo	nebo	k8xC	nebo
též	též	k9	též
nehašené	hašený	k2eNgNnSc4d1	nehašené
vápno	vápno	k1gNnSc4	vápno
je	být	k5eAaImIp3nS	být
široce	široko	k6eAd1	široko
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
běžně	běžně	k6eAd1	běžně
používaná	používaný	k2eAgFnSc1d1	používaná
chemická	chemický	k2eAgFnSc1d1	chemická
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
žíravá	žíravý	k2eAgFnSc1d1	žíravá
a	a	k8xC	a
alkalická	alkalický	k2eAgFnSc1d1	alkalická
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Komerčně	komerčně	k6eAd1	komerčně
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
oxid	oxid	k1gInSc1	oxid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
také	také	k9	také
často	často	k6eAd1	často
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
příměsi	příměs	k1gFnSc3	příměs
oxidu	oxid	k1gInSc2	oxid
hořečnatého	hořečnatý	k2eAgInSc2d1	hořečnatý
<g/>
,	,	kIx,	,
oxidu	oxid	k1gInSc2	oxid
křemičitého	křemičitý	k2eAgInSc2d1	křemičitý
a	a	k8xC	a
malá	malý	k2eAgNnPc4d1	malé
množství	množství	k1gNnSc4	množství
oxidu	oxid	k1gInSc2	oxid
hlinitého	hlinitý	k2eAgInSc2d1	hlinitý
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
železitého	železitý	k2eAgInSc2d1	železitý
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
Oxid	oxid	k1gInSc1	oxid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
vyráběn	vyrábět	k5eAaImNgInS	vyrábět
tepelným	tepelný	k2eAgInSc7d1	tepelný
rozkladem	rozklad	k1gInSc7	rozklad
hornin	hornina	k1gFnPc2	hornina
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
vápenec	vápenec	k1gInSc1	vápenec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
uhličitan	uhličitan	k1gInSc4	uhličitan
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
(	(	kIx(	(
<g/>
CaCO	CaCO	k1gFnSc1	CaCO
<g/>
3	[number]	k4	3
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
minerálů	minerál	k1gInPc2	minerál
kalcitu	kalcit	k1gInSc2	kalcit
a	a	k8xC	a
aragonitu	aragonit	k1gInSc2	aragonit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozklad	rozklad	k1gInSc1	rozklad
je	být	k5eAaImIp3nS	být
prováděn	provádět	k5eAaImNgInS	provádět
zahříváním	zahřívání	k1gNnSc7	zahřívání
jemně	jemně	k6eAd1	jemně
mleté	mletý	k2eAgFnSc2d1	mletá
horniny	hornina	k1gFnSc2	hornina
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
přesahující	přesahující	k2eAgFnSc4d1	přesahující
825	[number]	k4	825
°	°	k?	°
<g/>
C.	C.	kA	C.
Tento	tento	k3xDgInSc4	tento
proces	proces	k1gInSc4	proces
je	být	k5eAaImIp3nS	být
nazýván	nazývat	k5eAaImNgMnS	nazývat
kalcinace	kalcinace	k1gFnSc2	kalcinace
nebo	nebo	k8xC	nebo
též	též	k9	též
pálení	pálení	k1gNnSc2	pálení
vápna	vápno	k1gNnSc2	vápno
<g/>
.	.	kIx.	.
</s>
<s>
Uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
se	se	k3xPyFc4	se
při	při	k7c6	při
něm	on	k3xPp3gMnSc6	on
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
(	(	kIx(	(
<g/>
CO	co	k9	co
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
uhličitan	uhličitan	k1gInSc1	uhličitan
se	se	k3xPyFc4	se
přeměňuje	přeměňovat	k5eAaImIp3nS	přeměňovat
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
vápenatý	vápenatý	k2eAgInSc4d1	vápenatý
(	(	kIx(	(
<g/>
CaO	CaO	k1gFnSc4	CaO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
palivo	palivo	k1gNnSc1	palivo
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
nejčastěji	často	k6eAd3	často
hnědé	hnědý	k2eAgNnSc4d1	hnědé
uhlí	uhlí	k1gNnSc4	uhlí
<g/>
,	,	kIx,	,
koks	koks	k1gInSc1	koks
<g/>
,	,	kIx,	,
černé	černý	k2eAgNnSc1d1	černé
uhlí	uhlí	k1gNnSc1	uhlí
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
antracit	antracit	k1gInSc1	antracit
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
jako	jako	k9	jako
palivo	palivo	k1gNnSc1	palivo
používalo	používat	k5eAaImAgNnS	používat
též	též	k9	též
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
je	být	k5eAaImIp3nS	být
reverzibilní	reverzibilní	k2eAgMnSc1d1	reverzibilní
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
od	od	k7c2	od
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
vypálené	vypálený	k2eAgNnSc1d1	vypálené
vápno	vápno	k1gNnSc1	vápno
ochlazeno	ochlazen	k2eAgNnSc1d1	ochlazeno
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
vstřebávat	vstřebávat	k5eAaImF	vstřebávat
okolní	okolní	k2eAgInSc4d1	okolní
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
po	po	k7c6	po
čase	čas	k1gInSc6	čas
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
změní	změnit	k5eAaPmIp3nS	změnit
na	na	k7c4	na
původní	původní	k2eAgInSc4d1	původní
uhličitan	uhličitan	k1gInSc4	uhličitan
<g/>
.	.	kIx.	.
</s>
<s>
Pálení	pálení	k1gNnSc1	pálení
vápna	vápno	k1gNnSc2	vápno
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
první	první	k4xOgInPc4	první
chemické	chemický	k2eAgInPc4d1	chemický
procesy	proces	k1gInPc4	proces
objevené	objevený	k2eAgInPc4d1	objevený
člověkem	člověk	k1gMnSc7	člověk
již	již	k6eAd1	již
v	v	k7c6	v
pravěku	pravěk	k1gInSc6	pravěk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
hydratované	hydratovaný	k2eAgNnSc1d1	hydratované
nebo	nebo	k8xC	nebo
též	též	k9	též
triviálně	triviálně	k6eAd1	triviálně
hašené	hašený	k2eAgNnSc4d1	hašené
vápno	vápno	k1gNnSc4	vápno
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
hydroxidu	hydroxid	k1gInSc2	hydroxid
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
Ca	ca	kA	ca
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
název	název	k1gInSc1	název
tohoto	tento	k3xDgInSc2	tento
minerálu	minerál	k1gInSc2	minerál
je	být	k5eAaImIp3nS	být
portlandit	portlandit	k5eAaPmF	portlandit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaPmNgInS	využívat
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
malty	malta	k1gFnSc2	malta
a	a	k8xC	a
sádry	sádra	k1gFnSc2	sádra
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
tvrdosti	tvrdost	k1gFnSc2	tvrdost
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
hašeného	hašený	k2eAgNnSc2d1	hašené
vápna	vápno	k1gNnSc2	vápno
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
oxid	oxid	k1gInSc1	oxid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
zásaditý	zásaditý	k2eAgInSc1d1	zásaditý
anhydrid	anhydrid	k1gInSc1	anhydrid
a	a	k8xC	a
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
reaguje	reagovat	k5eAaBmIp3nS	reagovat
velmi	velmi	k6eAd1	velmi
živě	živě	k6eAd1	živě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Historicky	historicky	k6eAd1	historicky
nejstarší	starý	k2eAgNnSc1d3	nejstarší
využití	využití	k1gNnSc1	využití
vápna	vápno	k1gNnSc2	vápno
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
–	–	k?	–
v	v	k7c6	v
omítkách	omítka	k1gFnPc6	omítka
je	on	k3xPp3gInPc4	on
používali	používat	k5eAaImAgMnP	používat
již	jenž	k3xRgMnPc1	jenž
starověcí	starověký	k2eAgMnPc1d1	starověký
Římané	Říman	k1gMnPc1	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nedostatku	nedostatek	k1gInSc6	nedostatek
vápence	vápenec	k1gInPc4	vápenec
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
dokonce	dokonce	k9	dokonce
někdy	někdy	k6eAd1	někdy
byly	být	k5eAaImAgFnP	být
rozbíjeny	rozbíjen	k2eAgFnPc1d1	rozbíjena
mramorové	mramorový	k2eAgFnPc1d1	mramorová
sochy	socha	k1gFnPc1	socha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oxid	oxid	k1gInSc1	oxid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
je	být	k5eAaImIp3nS	být
též	též	k9	též
používán	používat	k5eAaImNgInS	používat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
skla	sklo	k1gNnSc2	sklo
a	a	k8xC	a
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
schopnosti	schopnost	k1gFnSc3	schopnost
reagovat	reagovat	k5eAaBmF	reagovat
s	s	k7c7	s
křemičitany	křemičitan	k1gInPc7	křemičitan
je	být	k5eAaImIp3nS	být
používán	používán	k2eAgInSc1d1	používán
v	v	k7c6	v
moderních	moderní	k2eAgInPc6d1	moderní
postupech	postup	k1gInPc6	postup
výroby	výroba	k1gFnSc2	výroba
ocelí	ocel	k1gFnPc2	ocel
a	a	k8xC	a
hořčíkových	hořčíkový	k2eAgInPc2d1	hořčíkový
<g/>
,	,	kIx,	,
hliníkových	hliníkový	k2eAgInPc2d1	hliníkový
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
neželezných	železný	k2eNgInPc2d1	neželezný
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Napomáhá	napomáhat	k5eAaBmIp3nS	napomáhat
vyplavování	vyplavování	k1gNnSc1	vyplavování
nečistot	nečistota	k1gFnPc2	nečistota
do	do	k7c2	do
strusky	struska	k1gFnSc2	struska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
také	také	k9	také
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xC	jako
přísada	přísada	k1gFnSc1	přísada
pro	pro	k7c4	pro
úpravu	úprava	k1gFnSc4	úprava
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Snižuje	snižovat	k5eAaImIp3nS	snižovat
její	její	k3xOp3gNnSc1	její
kyselost	kyselost	k1gFnSc1	kyselost
<g/>
,	,	kIx,	,
změkčuje	změkčovat	k5eAaImIp3nS	změkčovat
ji	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
flokulant	flokulant	k1gMnSc1	flokulant
(	(	kIx(	(
<g/>
sbaluje	sbalovat	k5eAaImIp3nS	sbalovat
koloidní	koloidní	k2eAgFnPc4d1	koloidní
nečistoty	nečistota	k1gFnPc4	nečistota
<g/>
)	)	kIx)	)
a	a	k8xC	a
napomáhá	napomáhat	k5eAaBmIp3nS	napomáhat
odstraňování	odstraňování	k1gNnSc4	odstraňování
fosfátů	fosfát	k1gInPc2	fosfát
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
nečistot	nečistota	k1gFnPc2	nečistota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
papírnictví	papírnictví	k1gNnSc6	papírnictví
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
rozpouštět	rozpouštět	k5eAaImF	rozpouštět
lignin	lignin	k1gInSc4	lignin
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
koagulant	koagulant	k1gInSc1	koagulant
a	a	k8xC	a
bělidlo	bělidlo	k1gNnSc1	bělidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
a	a	k8xC	a
lesnictví	lesnictví	k1gNnSc6	lesnictví
snižuje	snižovat	k5eAaImIp3nS	snižovat
kyselost	kyselost	k1gFnSc1	kyselost
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
též	též	k9	též
používán	používat	k5eAaImNgInS	používat
jako	jako	k8xC	jako
účinná	účinný	k2eAgFnSc1d1	účinná
složka	složka	k1gFnSc1	složka
při	při	k7c6	při
čistění	čistění	k1gNnSc6	čistění
a	a	k8xC	a
odsiřování	odsiřování	k1gNnSc6	odsiřování
plynných	plynný	k2eAgFnPc2d1	plynná
zplodin	zplodina	k1gFnPc2	zplodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tradičně	tradičně	k6eAd1	tradičně
bývá	bývat	k5eAaImIp3nS	bývat
používán	používat	k5eAaImNgInS	používat
při	při	k7c6	při
pohřbívání	pohřbívání	k1gNnSc6	pohřbívání
mrtvých	mrtvý	k2eAgNnPc2d1	mrtvé
těl	tělo	k1gNnPc2	tělo
do	do	k7c2	do
otevřených	otevřený	k2eAgInPc2d1	otevřený
hrobů	hrob	k1gInPc2	hrob
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
období	období	k1gNnSc6	období
epidemií	epidemie	k1gFnPc2	epidemie
například	například	k6eAd1	například
moru	mora	k1gFnSc4	mora
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
dezinfekce	dezinfekce	k1gFnSc2	dezinfekce
a	a	k8xC	a
zamezení	zamezení	k1gNnSc4	zamezení
zápachu	zápach	k1gInSc2	zápach
rozkladu	rozklad	k1gInSc2	rozklad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
forenzních	forenzní	k2eAgFnPc6d1	forenzní
vědách	věda	k1gFnPc6	věda
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
k	k	k7c3	k
detekci	detekce	k1gFnSc3	detekce
otisků	otisk	k1gInPc2	otisk
prstů	prst	k1gInPc2	prst
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
dehydratační	dehydratační	k2eAgNnSc1d1	dehydratační
činidlo	činidlo	k1gNnSc1	činidlo
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
čištění	čištění	k1gNnSc3	čištění
kyseliny	kyselina	k1gFnSc2	kyselina
citronové	citronový	k2eAgFnSc2d1	citronová
<g/>
,	,	kIx,	,
glukosy	glukosa	k1gFnSc2	glukosa
<g/>
,	,	kIx,	,
barviv	barvivo	k1gNnPc2	barvivo
a	a	k8xC	a
jako	jako	k9	jako
pohlcovač	pohlcovač	k1gInSc1	pohlcovač
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
používán	používat	k5eAaImNgInS	používat
v	v	k7c6	v
hrnčířství	hrnčířství	k1gNnSc6	hrnčířství
<g/>
,	,	kIx,	,
malířství	malířství	k1gNnSc6	malířství
a	a	k8xC	a
potravinářství	potravinářství	k1gNnSc6	potravinářství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Energie	energie	k1gFnSc1	energie
uvolněná	uvolněný	k2eAgFnSc1d1	uvolněná
reakcí	reakce	k1gFnSc7	reakce
oxidu	oxid	k1gInSc2	oxid
vápenatého	vápenatý	k2eAgMnSc4d1	vápenatý
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
bývá	bývat	k5eAaImIp3nS	bývat
používaná	používaný	k2eAgFnSc1d1	používaná
jako	jako	k8xC	jako
zdroj	zdroj	k1gInSc1	zdroj
tepla	teplo	k1gNnSc2	teplo
k	k	k7c3	k
ohřevu	ohřev	k1gInSc3	ohřev
speciálních	speciální	k2eAgFnPc2d1	speciální
samoohřevných	samoohřevný	k2eAgFnPc2d1	samoohřevný
konzerv	konzerva	k1gFnPc2	konzerva
jídla	jídlo	k1gNnSc2	jídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roční	roční	k2eAgFnSc1d1	roční
světová	světový	k2eAgFnSc1d1	světová
produkce	produkce	k1gFnSc1	produkce
oxidu	oxid	k1gInSc2	oxid
vápenatého	vápenatý	k2eAgInSc2d1	vápenatý
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
130	[number]	k4	130
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnPc1d3	veliký
producenti	producent	k1gMnPc1	producent
<g/>
,	,	kIx,	,
USA	USA	kA	USA
a	a	k8xC	a
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
vyprodukují	vyprodukovat	k5eAaPmIp3nP	vyprodukovat
každý	každý	k3xTgInSc4	každý
přibližně	přibližně	k6eAd1	přibližně
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Využití	využití	k1gNnSc1	využití
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
===	===	k?	===
</s>
</p>
<p>
<s>
Vzdušné	vzdušný	k2eAgNnSc4d1	vzdušné
vápno	vápno	k1gNnSc4	vápno
</s>
</p>
<p>
<s>
Dle	dle	k7c2	dle
jakosti	jakost	k1gFnSc2	jakost
surovin	surovina	k1gFnPc2	surovina
lze	lze	k6eAd1	lze
vyrobit	vyrobit	k5eAaPmF	vyrobit
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc4	druh
vzdušného	vzdušný	k2eAgNnSc2d1	vzdušné
vápna	vápno	k1gNnSc2	vápno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhy	druh	k1gInPc1	druh
vzdušného	vzdušný	k2eAgNnSc2d1	vzdušné
vápna	vápno	k1gNnSc2	vápno
bíléhoPozn	bíléhoPozn	k1gNnSc4	bíléhoPozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
Hodnoty	hodnota	k1gFnPc1	hodnota
uvedené	uvedený	k2eAgFnPc4d1	uvedená
v	v	k7c6	v
hmotnostních	hmotnostní	k2eAgInPc6d1	hmotnostní
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nehašené	hašený	k2eNgNnSc4d1	nehašené
vápno	vápno	k1gNnSc4	vápno
platí	platit	k5eAaImIp3nS	platit
přímo	přímo	k6eAd1	přímo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nehašeného	hašený	k2eNgNnSc2d1	nehašené
vápna	vápno	k1gNnSc2	vápno
se	se	k3xPyFc4	se
vápenné	vápenný	k2eAgFnPc1d1	vápenná
kaše	kaše	k1gFnPc1	kaše
po	po	k7c6	po
odpočtu	odpočet	k1gInSc6	odpočet
volné	volný	k2eAgFnSc2d1	volná
a	a	k8xC	a
vázané	vázaný	k2eAgFnSc2d1	vázaná
vody	voda	k1gFnSc2	voda
</s>
</p>
<p>
<s>
2	[number]	k4	2
Vyhoví	vyhovit	k5eAaPmIp3nS	vyhovit
<g/>
-li	i	k?	-li
zkoušce	zkouška	k1gFnSc3	zkouška
objemové	objemový	k2eAgFnSc2d1	objemová
stálosti	stálost	k1gFnSc2	stálost
dle	dle	k7c2	dle
ČSN	ČSN	kA	ČSN
EN	EN	kA	EN
459	[number]	k4	459
–	–	k?	–
2	[number]	k4	2
<g/>
,	,	kIx,	,
připouští	připouštět	k5eAaImIp3nS	připouštět
se	se	k3xPyFc4	se
až	až	k9	až
7	[number]	k4	7
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
Druhy	druh	k1gInPc1	druh
vzdušného	vzdušný	k2eAgNnSc2d1	vzdušné
vápna	vápno	k1gNnSc2	vápno
dolomitickéhoPozn	dolomitickéhoPozn	k1gNnSc4	dolomitickéhoPozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
Hodnoty	hodnota	k1gFnPc1	hodnota
uvedené	uvedený	k2eAgFnPc4d1	uvedená
v	v	k7c6	v
hmotnostních	hmotnostní	k2eAgInPc6d1	hmotnostní
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nehašené	hašený	k2eNgNnSc4d1	nehašené
vápno	vápno	k1gNnSc4	vápno
platí	platit	k5eAaImIp3nS	platit
přímo	přímo	k6eAd1	přímo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nehašeného	hašený	k2eNgNnSc2d1	nehašené
vápna	vápno	k1gNnSc2	vápno
se	se	k3xPyFc4	se
vápenné	vápenný	k2eAgFnPc1d1	vápenná
kaše	kaše	k1gFnPc1	kaše
po	po	k7c6	po
odpočtu	odpočet	k1gInSc6	odpočet
volné	volný	k2eAgFnSc2d1	volná
a	a	k8xC	a
vázané	vázaný	k2eAgFnSc2d1	vázaná
vody	voda	k1gFnSc2	voda
</s>
</p>
<p>
<s>
2	[number]	k4	2
Vyhoví	vyhovit	k5eAaPmIp3nS	vyhovit
<g/>
-li	i	k?	-li
zkoušce	zkouška	k1gFnSc3	zkouška
objemové	objemový	k2eAgFnSc2d1	objemová
stálosti	stálost	k1gFnSc2	stálost
dle	dle	k7c2	dle
ČSN	ČSN	kA	ČSN
EN	EN	kA	EN
459	[number]	k4	459
–	–	k?	–
2	[number]	k4	2
<g/>
,	,	kIx,	,
připouští	připouštět	k5eAaImIp3nS	připouštět
se	se	k3xPyFc4	se
až	až	k9	až
7	[number]	k4	7
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
VOHLÍDAL	VOHLÍDAL	kA	VOHLÍDAL
<g/>
,	,	kIx,	,
JIŘÍ	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
ŠTULÍK	štulík	k1gInSc1	štulík
<g/>
,	,	kIx,	,
KAREL	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
JULÁK	JULÁK	kA	JULÁK
<g/>
,	,	kIx,	,
ALOIS	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgFnPc1d1	chemická
a	a	k8xC	a
analytické	analytický	k2eAgFnPc1d1	analytická
tabulky	tabulka	k1gFnPc1	tabulka
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Grada	Grada	k1gFnSc1	Grada
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7169	[number]	k4	7169
<g/>
-	-	kIx~	-
<g/>
855	[number]	k4	855
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
