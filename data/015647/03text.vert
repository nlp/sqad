<s>
Galloway	Gallowaa	k1gFnPc1
Atlantic	Atlantice	k1gFnPc2
</s>
<s>
Galloway	Galloway	k1gInPc4
Atlantic	Atlantice	k1gFnPc2
byl	být	k5eAaImAgInS
letecký	letecký	k2eAgInSc1d1
motor	motor	k1gInSc1
vyvinutý	vyvinutý	k2eAgInSc1d1
u	u	k7c2
firmy	firma	k1gFnSc2
Galloway	Gallowaa	k1gFnPc1
Engineering	Engineering	k1gInSc4
Company	Compana	k1gFnSc2
na	na	k7c6
sklonku	sklonek	k1gInSc6
I.	I.	kA
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konstrukce	konstrukce	k1gFnSc1
motoru	motor	k1gInSc2
–	–	k?
vidlicového	vidlicový	k2eAgInSc2d1
dvanáctiválce	dvanáctiválec	k1gInSc2
–	–	k?
vycházela	vycházet	k5eAaImAgFnS
z	z	k7c2
řadového	řadový	k2eAgInSc2d1
šestiválce	šestiválec	k1gInSc2
Galloway	Gallowaa	k1gFnSc2
Adriatic	Adriatice	k1gFnPc2
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yRgInSc7,k3yIgInSc7,k3yQgInSc7
má	mít	k5eAaImIp3nS
shodné	shodný	k2eAgNnSc1d1
vrtání	vrtání	k1gNnSc1
válců	válec	k1gInPc2
a	a	k8xC
používá	používat	k5eAaImIp3nS
některých	některý	k3yIgMnPc2
konstrukčních	konstrukční	k2eAgMnPc2d1
celků	celek	k1gInPc2
svého	svůj	k3xOyFgMnSc2
předchůdce	předchůdce	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konstrukce	konstrukce	k1gFnSc1
klikového	klikový	k2eAgInSc2d1
mechanismu	mechanismus	k1gInSc2
je	být	k5eAaImIp3nS
s	s	k7c7
hlavní	hlavní	k2eAgFnSc7d1
a	a	k8xC
zavěšenou	zavěšený	k2eAgFnSc7d1
(	(	kIx(
<g/>
vedlejší	vedlejší	k2eAgFnSc7d1
<g/>
)	)	kIx)
ojnicí	ojnice	k1gFnSc7
<g/>
,	,	kIx,
což	což	k3yQnSc4,k3yRnSc4
bylo	být	k5eAaImAgNnS
uspořádání	uspořádání	k1gNnSc1
použité	použitý	k2eAgNnSc1d1
i	i	k9
u	u	k7c2
mnoha	mnoho	k4c2
jiných	jiný	k2eAgInPc2d1
motorů	motor	k1gInPc2
–	–	k?
mj.	mj.	kA
lze	lze	k6eAd1
uvést	uvést	k5eAaPmF
motory	motor	k1gInPc4
Hispano-Suiza	Hispano-Suiz	k1gMnSc2
HS-	HS-	k1gMnSc2
<g/>
12	#num#	k4
<g/>
Y	Y	kA
<g/>
,	,	kIx,
BMW	BMW	kA
VI	VI	kA
(	(	kIx(
<g/>
vzniklý	vzniklý	k2eAgMnSc1d1
obdobně	obdobně	k6eAd1
jako	jako	k8xC,k8xS
Galloway	Gallowaa	k1gFnPc1
Atlantic	Atlantice	k1gFnPc2
„	„	k?
<g/>
zdvojením	zdvojení	k1gNnPc3
<g/>
“	“	k?
řadového	řadový	k2eAgInSc2d1
šestiválce	šestiválec	k1gInSc2
BMW	BMW	kA
IV	IV	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Klimov	Klimov	k1gInSc1
M-	M-	k1gFnSc1
<g/>
105	#num#	k4
<g/>
,	,	kIx,
Mikulin	Mikulin	k2eAgMnSc1d1
M-35	M-35	k1gMnSc1
či	či	k8xC
sovětský	sovětský	k2eAgInSc1d1
tankový	tankový	k2eAgInSc1d1
vznětový	vznětový	k2eAgInSc1d1
motor	motor	k1gInSc1
V-2	V-2	k1gFnSc2
(	(	kIx(
<g/>
rusky	rusky	k6eAd1
<g/>
:	:	kIx,
В	В	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
čep	čep	k1gInSc1
na	na	k7c6
kterém	který	k3yIgInSc6,k3yRgInSc6,k3yQgInSc6
je	být	k5eAaImIp3nS
zavěšena	zavěšen	k2eAgFnSc1d1
vedlejší	vedlejší	k2eAgFnSc1d1
ojnice	ojnice	k1gFnSc1
není	být	k5eNaImIp3nS
od	od	k7c2
osy	osa	k1gFnSc2
hlavní	hlavní	k2eAgFnSc2d1
ojnice	ojnice	k1gFnSc2
odchýlen	odchýlit	k5eAaPmNgInS
pod	pod	k7c7
stejným	stejný	k2eAgInSc7d1
úhlem	úhel	k1gInSc7
jaký	jaký	k9
svírají	svírat	k5eAaImIp3nP
řady	řada	k1gFnPc1
válců	válec	k1gInPc2
<g/>
,	,	kIx,
pak	pak	k6eAd1
písty	píst	k1gInPc4
levého	levý	k2eAgInSc2d1
a	a	k8xC
pravého	pravý	k2eAgInSc2d1
bloku	blok	k1gInSc2
mají	mít	k5eAaImIp3nP
rozdílný	rozdílný	k2eAgInSc4d1
zdvih	zdvih	k1gInSc4
(	(	kIx(
<g/>
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
případ	případ	k1gInSc4
jak	jak	k8xC,k8xS
motoru	motor	k1gInSc2
Atlantic	Atlantice	k1gFnPc2
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
BMW	BMW	kA
VI	VI	kA
<g/>
,	,	kIx,
motorů	motor	k1gInPc2
Mikulin	Mikulin	k2eAgMnSc1d1
či	či	k8xC
tankového	tankový	k2eAgMnSc2d1
V-	V-	k1gMnSc2
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Celkem	celkem	k6eAd1
bylo	být	k5eAaImAgNnS
postaveno	postavit	k5eAaPmNgNnS
pouhých	pouhý	k2eAgInPc2d1
72	#num#	k4
motorů	motor	k1gInPc2
Galloway	Gallowaa	k1gFnSc2
Atlantic	Atlantice	k1gFnPc2
<g/>
,	,	kIx,
výhradně	výhradně	k6eAd1
dodaných	dodaný	k2eAgInPc2d1
firmou	firma	k1gFnSc7
Galloway	Gallowaa	k1gFnSc2
Engineering	Engineering	k1gInSc1
Co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ltd	ltd	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
z	z	k7c2
osmi	osm	k4xCc2
set	sto	k4xCgNnPc2
objednaných	objednaný	k2eAgInPc2d1
motorů	motor	k1gInPc2
–	–	k?
200	#num#	k4
měla	mít	k5eAaImAgFnS
dodat	dodat	k5eAaPmF
firma	firma	k1gFnSc1
Galloway	Gallowaa	k1gFnSc2
Engineering	Engineering	k1gInSc1
<g/>
,	,	kIx,
dalších	další	k2eAgMnPc2d1
600	#num#	k4
měla	mít	k5eAaImAgFnS
v	v	k7c4
licenci	licence	k1gFnSc4
vyrobit	vyrobit	k5eAaPmF
strojírna	strojírna	k1gFnSc1
Arrol-Johnston	Arrol-Johnston	k1gInSc4
Limited	limited	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
poháněly	pohánět	k5eAaImAgInP
letouny	letoun	k1gInPc1
de	de	k?
Havilland	Havilland	k1gInSc1
D.H	D.H	k1gFnSc1
<g/>
.15	.15	k4
Gazelle	Gazelle	k1gNnPc2
a	a	k8xC
Handley	Handle	k2eAgFnPc4d1
Page	Pag	k1gFnPc4
V	v	k7c6
<g/>
/	/	kIx~
<g/>
1500	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Technická	technický	k2eAgNnPc1d1
data	datum	k1gNnPc1
</s>
<s>
Typ	typ	k1gInSc1
<g/>
:	:	kIx,
čtyřdobý	čtyřdobý	k2eAgInSc1d1
zážehový	zážehový	k2eAgInSc1d1
vodou	voda	k1gFnSc7
chlazený	chlazený	k2eAgInSc1d1
vidlicový	vidlicový	k2eAgInSc1d1
dvanáctiválec	dvanáctiválec	k1gInSc1
s	s	k7c7
rozevřením	rozevření	k1gNnSc7
řad	řada	k1gFnPc2
válců	válec	k1gInPc2
60	#num#	k4
stupňů	stupeň	k1gInPc2
<g/>
,	,	kIx,
s	s	k7c7
přímým	přímý	k2eAgInSc7d1
náhonem	náhon	k1gInSc7
na	na	k7c4
tažnou	tažný	k2eAgFnSc4d1
pravotočivou	pravotočivý	k2eAgFnSc4d1
vrtuli	vrtule	k1gFnSc4
(	(	kIx(
<g/>
nebo	nebo	k8xC
také	také	k9
na	na	k7c4
tlačnou	tlačný	k2eAgFnSc4d1
levotočivou	levotočivý	k2eAgFnSc4d1
vrtuli	vrtule	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Vrtání	vrtání	k1gNnSc1
válce	válec	k1gInSc2
<g/>
:	:	kIx,
145	#num#	k4
mm	mm	kA
</s>
<s>
Zdvih	zdvih	k1gInSc1
pístu	píst	k1gInSc2
<g/>
:	:	kIx,
190	#num#	k4
mm	mm	kA
(	(	kIx(
<g/>
hlavní	hlavní	k2eAgFnSc2d1
ojnice	ojnice	k1gFnSc2
<g/>
)	)	kIx)
a	a	k8xC
202	#num#	k4
mm	mm	kA
(	(	kIx(
<g/>
zavěšená	zavěšený	k2eAgFnSc1d1
ojnice	ojnice	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Celková	celkový	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
pístů	píst	k1gInPc2
<g/>
:	:	kIx,
1981	#num#	k4
cm²	cm²	k?
</s>
<s>
Zdvihový	zdvihový	k2eAgInSc1d1
objem	objem	k1gInSc1
motoru	motor	k1gInSc2
<g/>
:	:	kIx,
38	#num#	k4
838	#num#	k4
cm³	cm³	k?
</s>
<s>
Kompresní	kompresní	k2eAgInSc1d1
poměr	poměr	k1gInSc1
<g/>
:	:	kIx,
4,90	4,90	k4
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1
suchého	suchý	k2eAgInSc2d1
motoru	motor	k1gInSc2
(	(	kIx(
<g/>
tj.	tj.	kA
bez	bez	k7c2
provozních	provozní	k2eAgFnPc2d1
náplní	náplň	k1gFnPc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
548,8	548,8	k4
kg	kg	kA
</s>
<s>
Výkony	výkon	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
vzletový	vzletový	k2eAgMnSc1d1
<g/>
:	:	kIx,
500	#num#	k4
hp	hp	k?
(	(	kIx(
<g/>
373	#num#	k4
kW	kW	kA
<g/>
)	)	kIx)
při	při	k7c6
1500	#num#	k4
ot	ot	k1gMnSc1
<g/>
/	/	kIx~
<g/>
min	min	kA
</s>
<s>
maximální	maximální	k2eAgFnSc1d1
<g/>
:	:	kIx,
510	#num#	k4
hp	hp	k?
(	(	kIx(
<g/>
380	#num#	k4
kW	kW	kA
<g/>
)	)	kIx)
při	při	k7c6
1600	#num#	k4
ot	ot	k1gMnSc1
<g/>
/	/	kIx~
<g/>
min	min	kA
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Prameny	pramen	k1gInPc1
</s>
<s>
Alec	Alec	k1gInSc1
S.	S.	kA
C.	C.	kA
Lumsden	Lumsdna	k1gFnPc2
<g/>
,	,	kIx,
MRAeS	MRAeS	k1gFnSc1
<g/>
,	,	kIx,
British	British	k1gInSc1
Piston	piston	k1gInSc1
Aero-Engines	Aero-Engines	k1gInSc1
<g/>
,	,	kIx,
ISBN	ISBN	kA
1-85310-294-6	1-85310-294-6	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
B2545	B2545	k1gMnSc1
Aero	aero	k1gNnSc4
engine	enginout	k5eAaPmIp3nS
<g/>
,	,	kIx,
Galloway	Galloway	k1gInPc4
Atlantic	Atlantice	k1gFnPc2
<g/>
,	,	kIx,
The	The	k1gMnSc1
Galloway	Gallowaa	k1gFnSc2
Engineering	Engineering	k1gInSc1
Co	co	k8xS
Ltd	ltd	kA
<g/>
,	,	kIx,
Dumfries	Dumfries	k1gInSc1
<g/>
,	,	kIx,
Scotland	Scotland	k1gInSc1
<g/>
,	,	kIx,
1918	#num#	k4
<g/>
.	.	kIx.
-	-	kIx~
Powerhouse	Powerhouse	k1gFnSc2
Museum	museum	k1gNnSc1
Collection	Collection	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
</s>
