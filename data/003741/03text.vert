<s>
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
zrada	zrada	k1gFnSc1	zrada
či	či	k8xC	či
Mnichovský	mnichovský	k2eAgInSc1d1	mnichovský
diktát	diktát	k1gInSc1	diktát
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
dohoda	dohoda	k1gFnSc1	dohoda
mezi	mezi	k7c7	mezi
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
,	,	kIx,	,
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
o	o	k7c6	o
odstoupení	odstoupení	k1gNnSc6	odstoupení
pohraničních	pohraniční	k2eAgNnPc2d1	pohraniční
území	území	k1gNnPc2	území
Československa	Československo	k1gNnSc2	Československo
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
dojednána	dojednat	k5eAaPmNgFnS	dojednat
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1938	[number]	k4	1938
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
(	(	kIx(	(
<g/>
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
jazykových	jazykový	k2eAgFnPc6d1	jazyková
verzích	verze	k1gFnPc6	verze
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgFnS	být
podepsána	podepsat	k5eAaPmNgFnS	podepsat
po	po	k7c6	po
půlnoci	půlnoc	k1gFnSc6	půlnoc
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
čtyř	čtyři	k4xCgFnPc2	čtyři
zemí	zem	k1gFnPc2	zem
–	–	k?	–
Neville	Neville	k1gInSc1	Neville
Chamberlain	Chamberlain	k1gInSc1	Chamberlain
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Édouard	Édouard	k1gMnSc1	Édouard
Daladier	Daladier	k1gMnSc1	Daladier
(	(	kIx(	(
<g/>
Francie	Francie	k1gFnSc1	Francie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
a	a	k8xC	a
Benito	Benit	k2eAgNnSc1d1	Benito
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
(	(	kIx(	(
<g/>
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
)	)	kIx)	)
–	–	k?	–
se	se	k3xPyFc4	se
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Československo	Československo	k1gNnSc1	Československo
musí	muset	k5eAaImIp3nS	muset
do	do	k7c2	do
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
postoupit	postoupit	k5eAaPmF	postoupit
pohraniční	pohraniční	k2eAgNnSc4d1	pohraniční
území	území	k1gNnSc4	území
obývané	obývaný	k2eAgNnSc4d1	obývané
Němci	Němec	k1gMnPc7	Němec
(	(	kIx(	(
<g/>
Sudety	Sudety	k1gInPc4	Sudety
<g/>
)	)	kIx)	)
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Zástupci	zástupce	k1gMnPc1	zástupce
československé	československý	k2eAgFnSc2d1	Československá
strany	strana	k1gFnSc2	strana
byli	být	k5eAaImAgMnP	být
přítomni	přítomen	k2eAgMnPc1d1	přítomen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
jednání	jednání	k1gNnSc3	jednání
samotnému	samotný	k2eAgNnSc3d1	samotné
nebyli	být	k5eNaImAgMnP	být
přizváni	přizvat	k5eAaPmNgMnP	přizvat
<g/>
.	.	kIx.	.
</s>
<s>
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
byla	být	k5eAaImAgFnS	být
završením	završení	k1gNnSc7	završení
činnosti	činnost	k1gFnSc2	činnost
Sudetoněmecké	sudetoněmecký	k2eAgFnSc2d1	Sudetoněmecká
strany	strana	k1gFnSc2	strana
Konráda	Konrád	k1gMnSc2	Konrád
Henleina	Henlein	k1gMnSc2	Henlein
a	a	k8xC	a
vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
snah	snaha	k1gFnPc2	snaha
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
rozbít	rozbít	k5eAaPmF	rozbít
demokratické	demokratický	k2eAgNnSc4d1	demokratické
Československo	Československo	k1gNnSc4	Československo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
postupných	postupný	k2eAgInPc2d1	postupný
cílů	cíl	k1gInPc2	cíl
k	k	k7c3	k
ovládnutí	ovládnutí	k1gNnSc3	ovládnutí
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
je	být	k5eAaImIp3nS	být
příkladem	příklad	k1gInSc7	příklad
politiky	politika	k1gFnSc2	politika
ústupků	ústupek	k1gInPc2	ústupek
<g/>
,	,	kIx,	,
appeasementu	appeasement	k1gInSc2	appeasement
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Matěje	Matěj	k1gMnSc2	Matěj
Spurného	Spurný	k1gMnSc2	Spurný
<g/>
,	,	kIx,	,
Jana	Jan	k1gMnSc2	Jan
Urbana	Urban	k1gMnSc2	Urban
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
Cermana	Cerman	k1gMnSc2	Cerman
a	a	k8xC	a
Jana	Jan	k1gMnSc4	Jan
Tesaře	Tesař	k1gMnSc4	Tesař
byla	být	k5eAaImAgFnS	být
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
završením	završení	k1gNnSc7	završení
politických	politický	k2eAgFnPc2d1	politická
a	a	k8xC	a
diplomatických	diplomatický	k2eAgFnPc2d1	diplomatická
selhání	selhání	k1gNnSc4	selhání
československé	československý	k2eAgFnSc2d1	Československá
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Obdobné	obdobný	k2eAgNnSc1d1	obdobné
stanovisko	stanovisko	k1gNnSc1	stanovisko
zastává	zastávat	k5eAaImIp3nS	zastávat
sudetoněmecká	sudetoněmecký	k2eAgFnSc1d1	Sudetoněmecká
interpretace	interpretace	k1gFnSc1	interpretace
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yQgFnSc2	který
nese	nést	k5eAaImIp3nS	nést
československá	československý	k2eAgFnSc1d1	Československá
politika	politika	k1gFnSc1	politika
sama	sám	k3xTgFnSc1	sám
odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
za	za	k7c4	za
zničení	zničení	k1gNnSc4	zničení
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
mezinárodněprávní	mezinárodněprávní	k2eAgFnSc2d1	mezinárodněprávní
teze	teze	k1gFnSc2	teze
Hermanna	Hermann	k1gMnSc2	Hermann
Raschhofera	Raschhofer	k1gMnSc2	Raschhofer
<g/>
,	,	kIx,	,
uveřejněné	uveřejněný	k2eAgInPc1d1	uveřejněný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
pouze	pouze	k6eAd1	pouze
prováděcí	prováděcí	k2eAgFnSc7d1	prováděcí
dohodou	dohoda	k1gFnSc7	dohoda
zásady	zásada	k1gFnSc2	zásada
odstoupení	odstoupení	k1gNnSc2	odstoupení
Sudet	Sudety	k1gFnPc2	Sudety
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
československá	československý	k2eAgFnSc1d1	Československá
vláda	vláda	k1gFnSc1	vláda
údajně	údajně	k6eAd1	údajně
přijala	přijmout	k5eAaPmAgFnS	přijmout
již	již	k6eAd1	již
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tudíž	tudíž	k8xC	tudíž
platná	platný	k2eAgFnSc1d1	platná
<g/>
.	.	kIx.	.
</s>
<s>
Stejné	stejný	k2eAgNnSc4d1	stejné
tvrzení	tvrzení	k1gNnSc4	tvrzení
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
sborník	sborník	k1gInSc1	sborník
prací	práce	k1gFnSc7	práce
sudetoněmeckých	sudetoněmecký	k2eAgMnPc2d1	sudetoněmecký
historiků	historik	k1gMnPc2	historik
a	a	k8xC	a
politiků	politik	k1gMnPc2	politik
"	"	kIx"	"
<g/>
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
a	a	k8xC	a
osud	osud	k1gInSc1	osud
sudetských	sudetský	k2eAgMnPc2d1	sudetský
Němců	Němec	k1gMnPc2	Němec
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
vydaný	vydaný	k2eAgInSc1d1	vydaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1925	[number]	k4	1925
bylo	být	k5eAaImAgNnS	být
Československo	Československo	k1gNnSc1	Československo
vázáno	vázat	k5eAaImNgNnS	vázat
podle	podle	k7c2	podle
Locarnských	Locarnská	k1gFnPc2	Locarnská
dohod	dohoda	k1gFnPc2	dohoda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zavázalo	zavázat	k5eAaPmAgNnS	zavázat
vzájemné	vzájemný	k2eAgInPc4d1	vzájemný
spory	spor	k1gInPc4	spor
řešit	řešit	k5eAaImF	řešit
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
arbitrážních	arbitrážní	k2eAgFnPc2d1	arbitrážní
smluv	smlouva	k1gFnPc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Zásadním	zásadní	k2eAgInSc7d1	zásadní
bodem	bod	k1gInSc7	bod
Locarnských	Locarnský	k2eAgFnPc2d1	Locarnská
dohod	dohoda	k1gFnPc2	dohoda
byl	být	k5eAaImAgInS	být
bezpečnostní	bezpečnostní	k2eAgInSc1d1	bezpečnostní
charakter	charakter	k1gInSc1	charakter
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
záruk	záruka	k1gFnPc2	záruka
německých	německý	k2eAgFnPc2d1	německá
západních	západní	k2eAgFnPc2d1	západní
hranic	hranice	k1gFnPc2	hranice
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Rýnského	rýnský	k2eAgInSc2d1	rýnský
garančního	garanční	k2eAgInSc2d1	garanční
paktu	pakt	k1gInSc2	pakt
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
východní	východní	k2eAgFnSc1d1	východní
hranice	hranice	k1gFnSc1	hranice
Německa	Německo	k1gNnSc2	Německo
smluvně	smluvně	k6eAd1	smluvně
zaručeny	zaručen	k2eAgFnPc1d1	zaručena
nebyly	být	k5eNaImAgFnP	být
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
zde	zde	k6eAd1	zde
existovala	existovat	k5eAaImAgFnS	existovat
možnost	možnost	k1gFnSc4	možnost
jejich	jejich	k3xOp3gFnSc2	jejich
revize	revize	k1gFnSc2	revize
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
nacisté	nacista	k1gMnPc1	nacista
chopili	chopit	k5eAaPmAgMnP	chopit
moci	moct	k5eAaImF	moct
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
Československo	Československo	k1gNnSc1	Československo
ve	v	k7c6	v
vážném	vážný	k2eAgNnSc6d1	vážné
nebezpečí	nebezpečí	k1gNnSc6	nebezpečí
stran	strana	k1gFnPc2	strana
německé	německý	k2eAgFnSc2d1	německá
expanze	expanze	k1gFnSc2	expanze
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
své	svůj	k3xOyFgInPc4	svůj
motivy	motiv	k1gInPc4	motiv
politické	politický	k2eAgInPc4d1	politický
(	(	kIx(	(
<g/>
revizionismus	revizionismus	k1gInSc4	revizionismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ideologické	ideologický	k2eAgInPc1d1	ideologický
(	(	kIx(	(
<g/>
nároky	nárok	k1gInPc1	nárok
na	na	k7c4	na
bývalá	bývalý	k2eAgNnPc4d1	bývalé
území	území	k1gNnPc4	území
německé	německý	k2eAgFnSc2d1	německá
říše	říš	k1gFnSc2	říš
a	a	k8xC	a
expanze	expanze	k1gFnSc2	expanze
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
strategické	strategický	k2eAgInPc1d1	strategický
(	(	kIx(	(
<g/>
průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
potenciál	potenciál	k1gInSc1	potenciál
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
poloha	poloha	k1gFnSc1	poloha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgNnSc3	tento
nebezpečí	nebezpečí	k1gNnSc3	nebezpečí
se	se	k3xPyFc4	se
Československo	Československo	k1gNnSc1	Československo
snažilo	snažit	k5eAaImAgNnS	snažit
čelit	čelit	k5eAaImF	čelit
jak	jak	k6eAd1	jak
budováním	budování	k1gNnSc7	budování
silné	silný	k2eAgFnSc2d1	silná
moderní	moderní	k2eAgFnSc2d1	moderní
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
pohraničních	pohraniční	k2eAgNnPc2d1	pohraniční
opevnění	opevnění	k1gNnPc2	opevnění
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
posilováním	posilování	k1gNnSc7	posilování
spojeneckých	spojenecký	k2eAgInPc2d1	spojenecký
svazků	svazek	k1gInPc2	svazek
nově	nově	k6eAd1	nově
i	i	k9	i
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgMnSc1d1	hlavní
spojenci	spojenec	k1gMnPc1	spojenec
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
neměli	mít	k5eNaImAgMnP	mít
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
další	další	k2eAgInSc4d1	další
krvavý	krvavý	k2eAgInSc4d1	krvavý
konflikt	konflikt	k1gInSc4	konflikt
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Podstatnou	podstatný	k2eAgFnSc7d1	podstatná
slabinou	slabina	k1gFnSc7	slabina
byla	být	k5eAaImAgFnS	být
<g/>
,	,	kIx,	,
neurčitá	určitý	k2eNgFnSc1d1	neurčitá
formulace	formulace	k1gFnSc1	formulace
Spojenecké	spojenecký	k2eAgFnSc2d1	spojenecká
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
navržena	navržen	k2eAgFnSc1d1	navržena
československou	československý	k2eAgFnSc7d1	Československá
diplomacií	diplomacie	k1gFnSc7	diplomacie
<g/>
,	,	kIx,	,
vázaní	vázaný	k2eAgMnPc1d1	vázaný
pomoci	pomoc	k1gFnSc2	pomoc
SSSR	SSSR	kA	SSSR
na	na	k7c4	na
kroky	krok	k1gInPc4	krok
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
nedostatečné	dostatečný	k2eNgInPc4d1	nedostatečný
kroky	krok	k1gInPc4	krok
československé	československý	k2eAgFnSc2d1	Československá
diplomacie	diplomacie	k1gFnSc2	diplomacie
u	u	k7c2	u
dalších	další	k2eAgFnPc2d1	další
velmocí	velmoc	k1gFnPc2	velmoc
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
zaručovaly	zaručovat	k5eAaImAgFnP	zaručovat
neměnnou	neměnný	k2eAgFnSc4d1	neměnná
podobu	podoba	k1gFnSc4	podoba
východních	východní	k2eAgFnPc2d1	východní
hranic	hranice	k1gFnPc2	hranice
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Německé	německý	k2eAgInPc4d1	německý
nároky	nárok	k1gInPc4	nárok
vůči	vůči	k7c3	vůči
Československu	Československo	k1gNnSc3	Československo
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
před	před	k7c7	před
světovou	světový	k2eAgFnSc7d1	světová
veřejností	veřejnost	k1gFnSc7	veřejnost
vysvětlovány	vysvětlován	k2eAgFnPc1d1	vysvětlována
jako	jako	k8xC	jako
částečná	částečný	k2eAgFnSc1d1	částečná
náprava	náprava	k1gFnSc1	náprava
Versailleského	versailleský	k2eAgInSc2d1	versailleský
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nastavil	nastavit	k5eAaPmAgInS	nastavit
Německu	Německo	k1gNnSc6	Německo
příliš	příliš	k6eAd1	příliš
tvrdé	tvrdý	k2eAgFnSc2d1	tvrdá
podmínky	podmínka	k1gFnSc2	podmínka
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
naplnění	naplnění	k1gNnSc4	naplnění
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
sebeurčení	sebeurčení	k1gNnSc4	sebeurčení
pro	pro	k7c4	pro
etnické	etnický	k2eAgMnPc4d1	etnický
Němce	Němec	k1gMnPc4	Němec
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
toho	ten	k3xDgNnSc2	ten
všeho	všecek	k3xTgNnSc2	všecek
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
velmoci	velmoc	k1gFnPc1	velmoc
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejichž	jejichž	k3xOyRp3gFnSc4	jejichž
pomoc	pomoc	k1gFnSc4	pomoc
při	při	k7c6	při
obraně	obrana	k1gFnSc6	obrana
proti	proti	k7c3	proti
německé	německý	k2eAgFnSc3d1	německá
agresi	agrese	k1gFnSc3	agrese
československá	československý	k2eAgFnSc1d1	Československá
vláda	vláda	k1gFnSc1	vláda
nejvíce	hodně	k6eAd3	hodně
spoléhala	spoléhat	k5eAaImAgFnS	spoléhat
<g/>
,	,	kIx,	,
měly	mít	k5eAaImAgFnP	mít
snahu	snaha	k1gFnSc4	snaha
se	se	k3xPyFc4	se
s	s	k7c7	s
nacisty	nacista	k1gMnPc7	nacista
dohodnout	dohodnout	k5eAaPmF	dohodnout
a	a	k8xC	a
předejít	předejít	k5eAaPmF	předejít
tak	tak	k6eAd1	tak
scénáři	scénář	k1gInPc7	scénář
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vedl	vést	k5eAaImAgInS	vést
k	k	k7c3	k
první	první	k4xOgFnSc6	první
světová	světový	k2eAgFnSc1d1	světová
válce	válka	k1gFnSc3	válka
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
obětování	obětování	k1gNnSc2	obětování
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Československo-sovětská	československoovětský	k2eAgFnSc1d1	československo-sovětská
smlouva	smlouva	k1gFnSc1	smlouva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
byla	být	k5eAaImAgFnS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
smlouvou	smlouva	k1gFnSc7	smlouva
vymezující	vymezující	k2eAgFnSc4d1	vymezující
vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
pomoc	pomoc	k1gFnSc4	pomoc
v	v	k7c6	v
případě	případ	k1gInSc6	případ
útoku	útok	k1gInSc2	útok
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
byl	být	k5eAaImAgInS	být
sice	sice	k8xC	sice
smluvně	smluvně	k6eAd1	smluvně
zavázán	zavázán	k2eAgMnSc1d1	zavázán
bránit	bránit	k5eAaImF	bránit
Československo	Československo	k1gNnSc4	Československo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
zapojí	zapojit	k5eAaPmIp3nS	zapojit
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
mělo	mít	k5eAaImAgNnS	mít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
podepsanou	podepsaný	k2eAgFnSc4d1	podepsaná
spojeneckou	spojenecký	k2eAgFnSc4d1	spojenecká
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
smluvní	smluvní	k2eAgFnPc1d1	smluvní
strany	strana	k1gFnPc1	strana
zavázaly	zavázat	k5eAaPmAgFnP	zavázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ohrožení	ohrožení	k1gNnPc2	ohrožení
společných	společný	k2eAgInPc2d1	společný
zájmů	zájem	k1gInPc2	zájem
se	se	k3xPyFc4	se
shodnou	shodnout	k5eAaPmIp3nP	shodnout
na	na	k7c6	na
opatřeních	opatření	k1gNnPc6	opatření
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
je	on	k3xPp3gNnSc4	on
měla	mít	k5eAaImAgFnS	mít
chránit	chránit	k5eAaImF	chránit
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
ovšem	ovšem	k9	ovšem
postrádal	postrádat	k5eAaImAgInS	postrádat
ustanovení	ustanovení	k1gNnSc4	ustanovení
o	o	k7c6	o
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
vojenské	vojenský	k2eAgFnSc6d1	vojenská
pomoci	pomoc	k1gFnSc6	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Signatáři	signatář	k1gMnPc1	signatář
měli	mít	k5eAaImAgMnP	mít
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
volnost	volnost	k1gFnSc4	volnost
a	a	k8xC	a
suverenitu	suverenita	k1gFnSc4	suverenita
rozhodování	rozhodování	k1gNnSc2	rozhodování
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
neurčitost	neurčitost	k1gFnSc4	neurčitost
vnesla	vnést	k5eAaPmAgFnS	vnést
do	do	k7c2	do
dokumentu	dokument	k1gInSc2	dokument
československá	československý	k2eAgFnSc1d1	Československá
diplomacie	diplomacie	k1gFnSc1	diplomacie
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1925	[number]	k4	1925
Československo	Československo	k1gNnSc1	Československo
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
Garanční	garanční	k2eAgFnSc4d1	garanční
smlouvu	smlouva	k1gFnSc4	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
dokument	dokument	k1gInSc1	dokument
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Locarnských	Locarnský	k2eAgFnPc2d1	Locarnská
dohod	dohoda	k1gFnPc2	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Určoval	určovat	k5eAaImAgMnS	určovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Československo	Československo	k1gNnSc4	Československo
si	se	k3xPyFc3	se
poskytnou	poskytnout	k5eAaPmIp3nP	poskytnout
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
stanou	stanout	k5eAaPmIp3nP	stanout
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
cílem	cíl	k1gInSc7	cíl
nevyprovokované	vyprovokovaný	k2eNgFnSc2d1	nevyprovokovaná
agrese	agrese	k1gFnSc2	agrese
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
za	za	k7c4	za
situace	situace	k1gFnPc4	situace
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Rada	rada	k1gFnSc1	rada
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
SN	SN	kA	SN
<g/>
)	)	kIx)	)
nevyjádří	vyjádřit	k5eNaPmIp3nS	vyjádřit
jednomyslně	jednomyslně	k6eAd1	jednomyslně
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
označení	označení	k1gNnPc2	označení
agresora	agresor	k1gMnSc2	agresor
<g/>
.	.	kIx.	.
</s>
<s>
Jednomyslnost	jednomyslnost	k1gFnSc1	jednomyslnost
členy	člen	k1gMnPc4	člen
SN	SN	kA	SN
opravňovala	opravňovat	k5eAaImAgFnS	opravňovat
pomáhat	pomáhat	k5eAaImF	pomáhat
vojensky	vojensky	k6eAd1	vojensky
napadené	napadený	k2eAgFnSc3d1	napadená
zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
jen	jen	k9	jen
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
eventuálním	eventuální	k2eAgInSc7d1	eventuální
francouzským	francouzský	k2eAgInSc7d1	francouzský
postupem	postup	k1gInSc7	postup
přes	přes	k7c4	přes
Rýn	Rýn	k1gInSc4	Rýn
budou	být	k5eAaImBp3nP	být
souhlasit	souhlasit	k5eAaImF	souhlasit
ostatní	ostatní	k2eAgMnPc1d1	ostatní
signatáři	signatář	k1gMnPc1	signatář
Rýnského	rýnský	k2eAgInSc2d1	rýnský
bezpečnostního	bezpečnostní	k2eAgInSc2d1	bezpečnostní
paktu	pakt	k1gInSc2	pakt
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
bylo	být	k5eAaImAgNnS	být
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
Německo	Německo	k1gNnSc4	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Garanční	garanční	k2eAgFnSc1d1	garanční
smlouva	smlouva	k1gFnSc1	smlouva
neumožňovala	umožňovat	k5eNaImAgFnS	umožňovat
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
expanzí	expanze	k1gFnSc7	expanze
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
dohoda	dohoda	k1gFnSc1	dohoda
(	(	kIx(	(
<g/>
vojensko-politické	vojenskoolitický	k2eAgNnSc1d1	vojensko-politické
spojenectví	spojenectví	k1gNnSc1	spojenectví
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
složené	složený	k2eAgNnSc4d1	složené
z	z	k7c2	z
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
a	a	k8xC	a
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
proti	proti	k7c3	proti
Rakousku	Rakousko	k1gNnSc3	Rakousko
a	a	k8xC	a
Maďarsku	Maďarsko	k1gNnSc3	Maďarsko
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
proti	proti	k7c3	proti
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Locarnských	Locarnský	k2eAgFnPc2d1	Locarnská
dohod	dohoda	k1gFnPc2	dohoda
arbitrážní	arbitrážní	k2eAgFnSc4d1	arbitrážní
dohodu	dohoda	k1gFnSc4	dohoda
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zavázaly	zavázat	k5eAaPmAgFnP	zavázat
vzájemné	vzájemný	k2eAgFnPc1d1	vzájemná
spory	spora	k1gFnPc1	spora
řešit	řešit	k5eAaImF	řešit
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
arbitrážních	arbitrážní	k2eAgFnPc2d1	arbitrážní
smluv	smlouva	k1gFnPc2	smlouva
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
plnomocníků	plnomocník	k1gMnPc2	plnomocník
<g/>
,	,	kIx,	,
Stálého	stálý	k2eAgInSc2d1	stálý
dvora	dvůr	k1gInSc2	dvůr
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
,	,	kIx,	,
Rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
Stálé	stálý	k2eAgFnSc2d1	stálá
smírčí	smírčí	k2eAgFnSc2d1	smírčí
komise	komise	k1gFnSc2	komise
nebo	nebo	k8xC	nebo
spor	spora	k1gFnPc2	spora
předložit	předložit	k5eAaPmF	předložit
Radě	rada	k1gFnSc3	rada
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Německo-české	německo-český	k2eAgInPc1d1	německo-český
vztahy	vztah	k1gInPc1	vztah
byly	být	k5eAaImAgInP	být
konfrontační	konfrontační	k2eAgInSc4d1	konfrontační
již	již	k6eAd1	již
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
začal	začít	k5eAaPmAgInS	začít
zrychlený	zrychlený	k2eAgInSc1d1	zrychlený
růst	růst	k1gInSc1	růst
nejdříve	dříve	k6eAd3	dříve
německého	německý	k2eAgNnSc2d1	německé
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
českého	český	k2eAgInSc2d1	český
nacionalismu	nacionalismus	k1gInSc2	nacionalismus
a	a	k8xC	a
utváření	utváření	k1gNnSc4	utváření
se	se	k3xPyFc4	se
moderního	moderní	k2eAgInSc2d1	moderní
německého	německý	k2eAgInSc2d1	německý
a	a	k8xC	a
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
národní	národní	k2eAgInSc1d1	národní
program	program	k1gInSc1	program
se	se	k3xPyFc4	se
opíral	opírat	k5eAaImAgInS	opírat
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
české	český	k2eAgNnSc1d1	české
státní	státní	k2eAgInSc1d1	státní
právo	právo	k1gNnSc4	právo
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kterého	který	k3yRgMnSc4	který
Češi	Čech	k1gMnPc1	Čech
požadovali	požadovat	k5eAaImAgMnP	požadovat
jednotu	jednota	k1gFnSc4	jednota
Zemí	zem	k1gFnPc2	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
rovnoprávnost	rovnoprávnost	k1gFnSc4	rovnoprávnost
češtiny	čeština	k1gFnSc2	čeština
s	s	k7c7	s
němčinou	němčina	k1gFnSc7	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Němci	Němec	k1gMnSc3	Němec
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
požadovali	požadovat	k5eAaImAgMnP	požadovat
své	své	k1gNnSc4	své
vlastní	vlastní	k2eAgNnSc4d1	vlastní
oddělené	oddělený	k2eAgNnSc4d1	oddělené
území	území	k1gNnSc4	území
a	a	k8xC	a
nechtěli	chtít	k5eNaImAgMnP	chtít
být	být	k5eAaImF	být
národnostní	národnostní	k2eAgFnSc7d1	národnostní
menšinou	menšina	k1gFnSc7	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
tak	tak	k9	tak
Němci	Němec	k1gMnPc1	Němec
žijící	žijící	k2eAgMnPc1d1	žijící
na	na	k7c6	na
území	území	k1gNnSc6	území
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
požadovali	požadovat	k5eAaImAgMnP	požadovat
rozdělení	rozdělení	k1gNnSc4	rozdělení
těchto	tento	k3xDgFnPc2	tento
zemí	zem	k1gFnPc2	zem
podle	podle	k7c2	podle
etnického	etnický	k2eAgNnSc2d1	etnické
hlediska	hledisko	k1gNnSc2	hledisko
a	a	k8xC	a
připojení	připojení	k1gNnSc2	připojení
území	území	k1gNnSc2	území
s	s	k7c7	s
německým	německý	k2eAgNnSc7d1	německé
osídlením	osídlení	k1gNnSc7	osídlení
k	k	k7c3	k
německému	německý	k2eAgNnSc3d1	německé
Rakousku	Rakousko	k1gNnSc3	Rakousko
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
Německé	německý	k2eAgFnSc6d1	německá
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Češi	Čech	k1gMnPc1	Čech
chtěli	chtít	k5eAaImAgMnP	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
české	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
jako	jako	k8xC	jako
celek	celek	k1gInSc1	celek
staly	stát	k5eAaPmAgFnP	stát
součástí	součást	k1gFnSc7	součást
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
česko-německé	českoěmecký	k2eAgInPc4d1	česko-německý
vztahy	vztah	k1gInPc4	vztah
na	na	k7c4	na
území	území	k1gNnSc4	území
těchto	tento	k3xDgFnPc2	tento
zemí	zem	k1gFnPc2	zem
byly	být	k5eAaImAgFnP	být
řešeny	řešen	k2eAgFnPc1d1	řešena
jako	jako	k8xC	jako
vnitrostátní	vnitrostátní	k2eAgFnSc1d1	vnitrostátní
otázka	otázka	k1gFnSc1	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
reprezentace	reprezentace	k1gFnSc1	reprezentace
se	se	k3xPyFc4	se
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
pohraničních	pohraniční	k2eAgNnPc6d1	pohraniční
území	území	k1gNnSc6	území
vzdát	vzdát	k5eAaPmF	vzdát
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
nový	nový	k2eAgInSc1d1	nový
československý	československý	k2eAgInSc1d1	československý
stát	stát	k1gInSc1	stát
nemůže	moct	k5eNaImIp3nS	moct
ekonomicky	ekonomicky	k6eAd1	ekonomicky
<g/>
,	,	kIx,	,
sociálně	sociálně	k6eAd1	sociálně
ani	ani	k8xC	ani
politicky	politicky	k6eAd1	politicky
existovat	existovat	k5eAaImF	existovat
bez	bez	k7c2	bez
pohraničí	pohraničí	k1gNnSc2	pohraničí
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
jinak	jinak	k6eAd1	jinak
<g/>
,	,	kIx,	,
než	než	k8xS	než
jako	jako	k9	jako
koloniální	koloniální	k2eAgInSc1d1	koloniální
protektorát	protektorát	k1gInSc1	protektorát
Velkého	velký	k2eAgNnSc2d1	velké
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
pro	pro	k7c4	pro
vojenské	vojenský	k2eAgNnSc4d1	vojenské
obsazení	obsazení	k1gNnSc4	obsazení
nárokovaného	nárokovaný	k2eAgNnSc2d1	nárokované
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
obsazení	obsazení	k1gNnSc1	obsazení
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
Mostu	most	k1gInSc2	most
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
bojovalo	bojovat	k5eAaImAgNnS	bojovat
<g/>
)	)	kIx)	)
klidně	klidně	k6eAd1	klidně
<g/>
,	,	kIx,	,
k	k	k7c3	k
větším	veliký	k2eAgInPc3d2	veliký
střetům	střet	k1gInPc3	střet
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
sejít	sejít	k5eAaPmF	sejít
nově	nově	k6eAd1	nově
zvolený	zvolený	k2eAgInSc4d1	zvolený
německo-rakouský	německoakouský	k2eAgInSc4d1	německo-rakouský
parlament	parlament	k1gInSc4	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
Němci	Němec	k1gMnPc1	Němec
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
svolali	svolat	k5eAaPmAgMnP	svolat
demonstrace	demonstrace	k1gFnPc4	demonstrace
za	za	k7c4	za
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
sebeurčení	sebeurčení	k1gNnSc4	sebeurčení
<g/>
,	,	kIx,	,
spojené	spojený	k2eAgInPc4d1	spojený
se	se	k3xPyFc4	se
všeobecnou	všeobecný	k2eAgFnSc7d1	všeobecná
stávkou	stávka	k1gFnSc7	stávka
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
demonstrací	demonstrace	k1gFnPc2	demonstrace
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
míst	místo	k1gNnPc2	místo
ke	k	k7c3	k
střetům	střet	k1gInPc3	střet
Němců	Němec	k1gMnPc2	Němec
s	s	k7c7	s
českými	český	k2eAgFnPc7d1	Česká
vojenskými	vojenský	k2eAgFnPc7d1	vojenská
jednotkami	jednotka	k1gFnPc7	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
střetů	střet	k1gInPc2	střet
bylo	být	k5eAaImAgNnS	být
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
Němců	Němec	k1gMnPc2	Němec
a	a	k8xC	a
dva	dva	k4xCgMnPc1	dva
zastřelení	zastřelený	k2eAgMnPc1d1	zastřelený
čeští	český	k2eAgMnPc1d1	český
vojáci	voják	k1gMnPc1	voják
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
mrtvých	mrtvý	k1gMnPc2	mrtvý
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Kadani	Kadaň	k1gFnSc6	Kadaň
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
krajního	krajní	k2eAgInSc2d1	krajní
německého	německý	k2eAgInSc2d1	německý
pohledu	pohled	k1gInSc2	pohled
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
poklidné	poklidný	k2eAgFnPc4d1	poklidná
demonstrace	demonstrace	k1gFnPc4	demonstrace
<g/>
,	,	kIx,	,
zmasakrované	zmasakrovaný	k2eAgNnSc1d1	zmasakrované
českým	český	k2eAgNnSc7d1	české
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
krajního	krajní	k2eAgInSc2d1	krajní
českého	český	k2eAgInSc2d1	český
pohledu	pohled	k1gInSc2	pohled
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
včasné	včasný	k2eAgNnSc4d1	včasné
potlačení	potlačení	k1gNnSc4	potlačení
připraveného	připravený	k2eAgNnSc2d1	připravené
povstání	povstání	k1gNnSc2	povstání
českých	český	k2eAgMnPc2d1	český
Němců	Němec	k1gMnPc2	Němec
proti	proti	k7c3	proti
Československu	Československo	k1gNnSc3	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
pravidelně	pravidelně	k6eAd1	pravidelně
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
Československu	Československo	k1gNnSc6	Československo
smlouvu	smlouva	k1gFnSc4	smlouva
o	o	k7c6	o
neútočení	neútočení	k1gNnSc6	neútočení
podle	podle	k7c2	podle
vzoru	vzor	k1gInSc2	vzor
polsko-německé	polskoěmecký	k2eAgFnSc2d1	polsko-německý
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
upravovala	upravovat	k5eAaImAgFnS	upravovat
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
menšinou	menšina	k1gFnSc7	menšina
a	a	k8xC	a
kde	kde	k6eAd1	kde
Německo	Německo	k1gNnSc4	Německo
fakticky	fakticky	k6eAd1	fakticky
uznalo	uznat	k5eAaPmAgNnS	uznat
existující	existující	k2eAgFnSc4d1	existující
německo-polskou	německoolský	k2eAgFnSc4d1	německo-polská
hranici	hranice	k1gFnSc4	hranice
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
ale	ale	k9	ale
Beneš	Beneš	k1gMnSc1	Beneš
opakovaně	opakovaně	k6eAd1	opakovaně
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Československo	Československo	k1gNnSc1	Československo
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
16	[number]	k4	16
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1935	[number]	k4	1935
pakt	pakt	k1gInSc4	pakt
o	o	k7c6	o
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
pomoci	pomoc	k1gFnSc6	pomoc
se	s	k7c7	s
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
,	,	kIx,	,
začalo	začít	k5eAaPmAgNnS	začít
být	být	k5eAaImF	být
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
tisku	tisk	k1gInSc6	tisk
napadáno	napadán	k2eAgNnSc1d1	napadáno
jako	jako	k8xS	jako
bolševické	bolševický	k2eAgNnSc1d1	bolševické
centrum	centrum	k1gNnSc1	centrum
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Neustávaly	ustávat	k5eNaImAgInP	ustávat
ani	ani	k8xC	ani
německé	německý	k2eAgInPc1d1	německý
útoky	útok	k1gInPc1	útok
na	na	k7c4	na
Československo	Československo	k1gNnSc4	Československo
jako	jako	k8xS	jako
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pošlapává	pošlapávat	k5eAaImIp3nS	pošlapávat
práva	právo	k1gNnPc4	právo
svých	svůj	k3xOyFgFnPc2	svůj
menšin	menšina	k1gFnPc2	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Beneš	Beneš	k1gMnSc1	Beneš
na	na	k7c4	na
nenávistné	návistný	k2eNgInPc4d1	nenávistný
propagandistické	propagandistický	k2eAgInPc4d1	propagandistický
útoky	útok	k1gInPc4	útok
německého	německý	k2eAgInSc2d1	německý
tisku	tisk	k1gInSc2	tisk
reagoval	reagovat	k5eAaBmAgMnS	reagovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
označil	označit	k5eAaPmAgInS	označit
otázku	otázka	k1gFnSc4	otázka
národnostních	národnostní	k2eAgFnPc2d1	národnostní
menšin	menšina	k1gFnPc2	menšina
za	za	k7c4	za
čistě	čistě	k6eAd1	čistě
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
záležitost	záležitost	k1gFnSc4	záležitost
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
také	také	k9	také
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
porozumění	porozumění	k1gNnSc1	porozumění
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
lze	lze	k6eAd1	lze
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Německo	Německo	k1gNnSc1	Německo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pro	pro	k7c4	pro
Československo	Československo	k1gNnSc4	Československo
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
zabránit	zabránit	k5eAaPmF	zabránit
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
izolaci	izolace	k1gFnSc4	izolace
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
přesvědčoval	přesvědčovat	k5eAaImAgMnS	přesvědčovat
západní	západní	k2eAgFnPc4d1	západní
mocnosti	mocnost	k1gFnPc4	mocnost
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
případné	případný	k2eAgInPc4d1	případný
ústupky	ústupek	k1gInPc4	ústupek
Československa	Československo	k1gNnSc2	Československo
Německu	Německo	k1gNnSc6	Německo
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
značný	značný	k2eAgInSc4d1	značný
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
západní	západní	k2eAgFnSc4d1	západní
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
neuspokojivým	uspokojivý	k2eNgInPc3d1	neuspokojivý
vztahům	vztah	k1gInPc3	vztah
Čechů	Čech	k1gMnPc2	Čech
a	a	k8xC	a
Němců	Němec	k1gMnPc2	Němec
přijala	přijmout	k5eAaPmAgFnS	přijmout
československá	československý	k2eAgFnSc1d1	Československá
vláda	vláda	k1gFnSc1	vláda
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1937	[number]	k4	1937
Program	program	k1gInSc1	program
národnostní	národnostní	k2eAgFnSc2d1	národnostní
politiky	politika	k1gFnSc2	politika
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
vstříc	vstříc	k6eAd1	vstříc
požadavkům	požadavek	k1gInPc3	požadavek
sudetských	sudetský	k2eAgInPc2d1	sudetský
Němců	Němec	k1gMnPc2	Němec
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
dokumentem	dokument	k1gInSc7	dokument
souhlasily	souhlasit	k5eAaImAgFnP	souhlasit
všechny	všechen	k3xTgFnPc1	všechen
německé	německý	k2eAgFnPc1d1	německá
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Sudetoněmecká	sudetoněmecký	k2eAgFnSc1d1	Sudetoněmecká
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
SdP	SdP	k1gFnSc1	SdP
<g/>
)	)	kIx)	)
ho	on	k3xPp3gNnSc4	on
ihned	ihned	k6eAd1	ihned
zpochybnila	zpochybnit	k5eAaPmAgFnS	zpochybnit
a	a	k8xC	a
zavrhla	zavrhnout	k5eAaPmAgFnS	zavrhnout
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
ve	v	k7c4	v
stejný	stejný	k2eAgInSc4d1	stejný
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
britský	britský	k2eAgMnSc1d1	britský
ministerský	ministerský	k2eAgMnSc1d1	ministerský
předseda	předseda	k1gMnSc1	předseda
Neville	Neville	k1gFnSc2	Neville
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
schází	scházet	k5eAaImIp3nS	scházet
s	s	k7c7	s
Hitlerem	Hitler	k1gMnSc7	Hitler
v	v	k7c6	v
Obersalzbergu	Obersalzberg	k1gInSc6	Obersalzberg
<g/>
,	,	kIx,	,
pověřuje	pověřovat	k5eAaImIp3nS	pověřovat
prezident	prezident	k1gMnSc1	prezident
Beneš	Beneš	k1gMnSc1	Beneš
ministra	ministr	k1gMnSc2	ministr
sociálních	sociální	k2eAgInPc2d1	sociální
věci	věc	k1gFnPc4	věc
Jaromíra	Jaromír	k1gMnSc2	Jaromír
Nečase	Nečas	k1gMnSc2	Nečas
tajným	tajný	k2eAgNnSc7d1	tajné
posláním	poslání	k1gNnSc7	poslání
s	s	k7c7	s
osmibodovým	osmibodový	k2eAgInSc7d1	osmibodový
plánem	plán	k1gInSc7	plán
pro	pro	k7c4	pro
francouzského	francouzský	k2eAgMnSc4d1	francouzský
ministerského	ministerský	k2eAgMnSc4d1	ministerský
předsedu	předseda	k1gMnSc4	předseda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
plánu	plán	k1gInSc6	plán
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
odstoupení	odstoupení	k1gNnSc4	odstoupení
4000-6000	[number]	k4	4000-6000
km2	km2	k4	km2
a	a	k8xC	a
přesídlení	přesídlení	k1gNnSc4	přesídlení
1,5	[number]	k4	1,5
až	až	k9	až
2	[number]	k4	2
milionů	milion	k4xCgInPc2	milion
německých	německý	k2eAgMnPc2d1	německý
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
instrukci	instrukce	k1gFnSc6	instrukce
Beneš	Beneš	k1gMnSc1	Beneš
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
Nečasovi	Nečas	k1gMnSc3	Nečas
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nikdy	nikdy	k6eAd1	nikdy
nepřipustit	připustit	k5eNaPmF	připustit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
plán	plán	k1gInSc1	plán
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
<g/>
"	"	kIx"	"
dále	daleko	k6eAd2	daleko
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Neříci	říct	k5eNaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
pochází	pocházet	k5eAaImIp3nS	pocházet
ode	ode	k7c2	ode
mne	já	k3xPp1nSc2	já
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
neříci	říct	k5eNaPmF	říct
to	ten	k3xDgNnSc4	ten
Osuskému	Osuský	k1gMnSc3	Osuský
a	a	k8xC	a
žádat	žádat	k5eAaImF	žádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
nebylo	být	k5eNaImAgNnS	být
mluveno	mluven	k2eAgNnSc1d1	mluveno
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
tyto	tento	k3xDgInPc4	tento
papíry	papír	k1gInPc4	papír
zničit	zničit	k5eAaPmF	zničit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
17	[number]	k4	17
<g/>
.	.	kIx.	.
<g/>
října	říjen	k1gInSc2	říjen
1937	[number]	k4	1937
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Teplicích	Teplice	k1gFnPc6	Teplice
k	k	k7c3	k
zinscenovanému	zinscenovaný	k2eAgInSc3d1	zinscenovaný
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
incidentu	incident	k1gInSc3	incident
poslance	poslanec	k1gMnSc2	poslanec
SdP	SdP	k1gMnSc2	SdP
K.	K.	kA	K.
H.	H.	kA	H.
Franka	Frank	k1gMnSc2	Frank
s	s	k7c7	s
policií	policie	k1gFnSc7	policie
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
Frank	Frank	k1gMnSc1	Frank
bezdůvodně	bezdůvodně	k6eAd1	bezdůvodně
napadl	napadnout	k5eAaPmAgMnS	napadnout
a	a	k8xC	a
zranil	zranit	k5eAaPmAgMnS	zranit
tři	tři	k4xCgMnPc4	tři
policisty	policista	k1gMnPc4	policista
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
henleinovskou	henleinovský	k2eAgFnSc7d1	henleinovská
propagandou	propaganda	k1gFnSc7	propaganda
zneužito	zneužit	k2eAgNnSc4d1	zneužito
a	a	k8xC	a
vyloženo	vyložen	k2eAgNnSc4d1	vyloženo
jako	jako	k8xS	jako
útok	útok	k1gInSc4	útok
proti	proti	k7c3	proti
sudetoněmeckému	sudetoněmecký	k2eAgMnSc3d1	sudetoněmecký
poslanci	poslanec	k1gMnSc3	poslanec
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Teplický	teplický	k2eAgInSc1d1	teplický
incident	incident	k1gInSc1	incident
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
neustálému	neustálý	k2eAgNnSc3d1	neustálé
stupňování	stupňování	k1gNnSc3	stupňování
sudetoněmeckých	sudetoněmecký	k2eAgInPc2d1	sudetoněmecký
požadavků	požadavek	k1gInPc2	požadavek
vůči	vůči	k7c3	vůči
vládě	vláda	k1gFnSc3	vláda
Československa	Československo	k1gNnSc2	Československo
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
pozadí	pozadí	k1gNnSc6	pozadí
stály	stát	k5eAaImAgInP	stát
zájmy	zájem	k1gInPc1	zájem
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1938	[number]	k4	1938
Hitler	Hitler	k1gMnSc1	Hitler
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mezi	mezi	k7c4	mezi
zájem	zájem	k1gInSc4	zájem
Německa	Německo	k1gNnSc2	Německo
patří	patřit	k5eAaImIp3nS	patřit
ochrana	ochrana	k1gFnSc1	ochrana
Němců	Němec	k1gMnPc2	Němec
v	v	k7c6	v
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
následoval	následovat	k5eAaImAgInS	následovat
anšlus	anšlus	k1gInSc1	anšlus
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
Československo	Československo	k1gNnSc1	Československo
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
ještě	ještě	k9	ještě
do	do	k7c2	do
většího	veliký	k2eAgNnSc2d2	veliký
ohrožení	ohrožení	k1gNnSc2	ohrožení
<g/>
.	.	kIx.	.
</s>
<s>
Anšlus	anšlus	k1gInSc1	anšlus
Rakouska	Rakousko	k1gNnSc2	Rakousko
zároveň	zároveň	k6eAd1	zároveň
podstatně	podstatně	k6eAd1	podstatně
změnil	změnit	k5eAaPmAgInS	změnit
teritoriální	teritoriální	k2eAgInSc1d1	teritoriální
status	status	k1gInSc1	status
quo	quo	k?	quo
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
Versailleskou	versailleský	k2eAgFnSc7d1	Versailleská
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
situace	situace	k1gFnSc2	situace
došlo	dojít	k5eAaPmAgNnS	dojít
bez	bez	k7c2	bez
souhlasu	souhlas	k1gInSc2	souhlas
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgFnPc1d1	západní
mocnosti	mocnost	k1gFnPc1	mocnost
protestovaly	protestovat	k5eAaBmAgFnP	protestovat
pouze	pouze	k6eAd1	pouze
formálně	formálně	k6eAd1	formálně
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgInS	být
narušen	narušen	k2eAgInSc1d1	narušen
celý	celý	k2eAgInSc1d1	celý
princip	princip	k1gInSc1	princip
evropské	evropský	k2eAgFnSc2d1	Evropská
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1938	[number]	k4	1938
navštívil	navštívit	k5eAaPmAgInS	navštívit
Konrad	Konrad	k1gInSc1	Konrad
Henlein	Henlein	k1gInSc1	Henlein
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Hitler	Hitler	k1gMnSc1	Hitler
Henleinovi	Henlein	k1gMnSc3	Henlein
deklaroval	deklarovat	k5eAaBmAgMnS	deklarovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
SdP	SdP	k1gMnPc1	SdP
musí	muset	k5eAaImIp3nP	muset
předkládat	předkládat	k5eAaImF	předkládat
takové	takový	k3xDgInPc4	takový
požadavky	požadavek	k1gInPc4	požadavek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
budou	být	k5eAaImBp3nP	být
pro	pro	k7c4	pro
československou	československý	k2eAgFnSc4d1	Československá
vládu	vláda	k1gFnSc4	vláda
nepřijatelné	přijatelný	k2eNgInPc1d1	nepřijatelný
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
Hitler	Hitler	k1gMnSc1	Hitler
a	a	k8xC	a
německé	německý	k2eAgNnSc4d1	německé
vojenské	vojenský	k2eAgNnSc4d1	vojenské
velení	velení	k1gNnSc4	velení
rozpracovali	rozpracovat	k5eAaPmAgMnP	rozpracovat
podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
zásady	zásada	k1gFnPc4	zásada
plánu	plán	k1gInSc2	plán
na	na	k7c4	na
útok	útok	k1gInSc4	útok
proti	proti	k7c3	proti
Československu	Československo	k1gNnSc3	Československo
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
krycí	krycí	k2eAgInSc4d1	krycí
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
Fall	Fall	k1gMnSc1	Fall
Grün	Grün	k1gMnSc1	Grün
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c6	na
hlášení	hlášení	k1gNnSc6	hlášení
čs	čs	kA	čs
<g/>
.	.	kIx.	.
zpravodajské	zpravodajský	k2eAgFnPc4d1	zpravodajská
služby	služba	k1gFnPc4	služba
ze	z	k7c2	z
dne	den	k1gInSc2	den
19	[number]	k4	19
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
o	o	k7c6	o
pohybech	pohyb	k1gInPc6	pohyb
německých	německý	k2eAgFnPc2d1	německá
vojsk	vojsko	k1gNnPc2	vojsko
k	k	k7c3	k
československým	československý	k2eAgFnPc3d1	Československá
hranicím	hranice	k1gFnPc3	hranice
<g/>
,	,	kIx,	,
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
Československo	Československo	k1gNnSc1	Československo
následující	následující	k2eAgNnSc1d1	následující
den	den	k1gInSc4	den
mobilizaci	mobilizace	k1gFnSc3	mobilizace
záložních	záložní	k2eAgInPc2d1	záložní
ročníků	ročník	k1gInPc2	ročník
a	a	k8xC	a
zesílilo	zesílit	k5eAaPmAgNnS	zesílit
ostrahu	ostraha	k1gFnSc4	ostraha
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
hrozícím	hrozící	k2eAgInSc6d1	hrozící
útoku	útok	k1gInSc6	útok
se	se	k3xPyFc4	se
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
jako	jako	k9	jako
mylné	mylný	k2eAgFnPc1d1	mylná
a	a	k8xC	a
do	do	k7c2	do
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
tato	tento	k3xDgNnPc1	tento
tzv.	tzv.	kA	tzv.
květnová	květnový	k2eAgFnSc1d1	květnová
krize	krize	k1gFnSc1	krize
pominula	pominout	k5eAaPmAgFnS	pominout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejím	její	k3xOp3gInSc6	její
důsledku	důsledek	k1gInSc6	důsledek
začala	začít	k5eAaPmAgFnS	začít
znepokojená	znepokojený	k2eAgFnSc1d1	znepokojená
Británie	Británie	k1gFnSc1	Británie
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
tlačit	tlačit	k5eAaImF	tlačit
československou	československý	k2eAgFnSc4d1	Československá
vládu	vláda	k1gFnSc4	vláda
k	k	k7c3	k
dohodě	dohoda	k1gFnSc3	dohoda
se	s	k7c7	s
Sudetoněmeckou	sudetoněmecký	k2eAgFnSc7d1	Sudetoněmecká
stranou	strana	k1gFnSc7	strana
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1938	[number]	k4	1938
Henlein	Henleina	k1gFnPc2	Henleina
na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
SdP	SdP	k1gFnSc2	SdP
předložil	předložit	k5eAaPmAgInS	předložit
Karlovarský	karlovarský	k2eAgInSc1d1	karlovarský
program	program	k1gInSc1	program
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vycházel	vycházet	k5eAaImAgInS	vycházet
z	z	k7c2	z
jeho	on	k3xPp3gMnSc2	on
jednání	jednání	k1gNnPc2	jednání
s	s	k7c7	s
Adolfem	Adolf	k1gMnSc7	Adolf
Hitlerem	Hitler	k1gMnSc7	Hitler
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
dokument	dokument	k1gInSc1	dokument
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
i	i	k9	i
takové	takový	k3xDgInPc4	takový
nároky	nárok	k1gInPc4	nárok
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
národnostního	národnostní	k2eAgNnSc2d1	národnostní
<g/>
,	,	kIx,	,
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
principů	princip	k1gInPc2	princip
právního	právní	k2eAgInSc2d1	právní
demokratického	demokratický	k2eAgInSc2d1	demokratický
státu	stát	k1gInSc2	stát
i	i	k8xC	i
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
státní	státní	k2eAgFnSc2d1	státní
suverenity	suverenita	k1gFnSc2	suverenita
pro	pro	k7c4	pro
Československo	Československo	k1gNnSc4	Československo
nepřijatelné	přijatelný	k2eNgNnSc4d1	nepřijatelné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnových	květnový	k2eAgFnPc6d1	květnová
a	a	k8xC	a
červnových	červnový	k2eAgFnPc6d1	červnová
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
obecních	obecní	k2eAgNnPc2d1	obecní
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
získala	získat	k5eAaPmAgFnS	získat
SdP	SdP	k1gFnSc1	SdP
kolem	kolem	k7c2	kolem
90	[number]	k4	90
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
německých	německý	k2eAgMnPc2d1	německý
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
Hitler	Hitler	k1gMnSc1	Hitler
vydal	vydat	k5eAaPmAgMnS	vydat
podrobné	podrobný	k2eAgFnPc4d1	podrobná
válečné	válečná	k1gFnPc4	válečná
směrnice	směrnice	k1gFnSc2	směrnice
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
Československo	Československo	k1gNnSc4	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1938	[number]	k4	1938
bylo	být	k5eAaImAgNnS	být
čs	čs	kA	čs
<g/>
.	.	kIx.	.
vládou	vláda	k1gFnSc7	vláda
schváleno	schválen	k2eAgNnSc1d1	schváleno
znění	znění	k1gNnSc1	znění
tzv.	tzv.	kA	tzv.
národnostního	národnostní	k2eAgInSc2d1	národnostní
statutu	statut	k1gInSc2	statut
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
velkým	velký	k2eAgInSc7d1	velký
ústupkem	ústupek	k1gInSc7	ústupek
vůči	vůči	k7c3	vůči
sudetským	sudetský	k2eAgMnPc3d1	sudetský
Němcům	Němec	k1gMnPc3	Němec
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
i	i	k9	i
tento	tento	k3xDgInSc1	tento
dokument	dokument	k1gInSc1	dokument
byl	být	k5eAaImAgInS	být
SdP	SdP	k1gFnSc4	SdP
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
"	"	kIx"	"
<g/>
doporučení	doporučení	k1gNnSc2	doporučení
<g/>
"	"	kIx"	"
Adolfa	Adolf	k1gMnSc2	Adolf
Hitlera	Hitler	k1gMnSc2	Hitler
odmítnut	odmítnout	k5eAaPmNgInS	odmítnout
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1938	[number]	k4	1938
zahájil	zahájit	k5eAaPmAgMnS	zahájit
britský	britský	k2eAgMnSc1d1	britský
lord	lord	k1gMnSc1	lord
Walter	Walter	k1gMnSc1	Walter
Runciman	Runciman	k1gMnSc1	Runciman
pražskou	pražský	k2eAgFnSc4d1	Pražská
misi	mise	k1gFnSc4	mise
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
urovnat	urovnat	k5eAaPmF	urovnat
spory	spor	k1gInPc4	spor
mezi	mezi	k7c7	mezi
československou	československý	k2eAgFnSc7d1	Československá
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
německou	německý	k2eAgFnSc7d1	německá
menšinou	menšina	k1gFnSc7	menšina
<g/>
,	,	kIx,	,
reprezentovanou	reprezentovaný	k2eAgFnSc7d1	reprezentovaná
SdP	SdP	k1gFnSc7	SdP
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
i	i	k9	i
tato	tento	k3xDgFnSc1	tento
mise	mise	k1gFnSc1	mise
skončila	skončit	k5eAaPmAgFnS	skončit
bezvýsledně	bezvýsledně	k6eAd1	bezvýsledně
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
SdP	SdP	k1gFnSc1	SdP
stále	stále	k6eAd1	stále
odmítala	odmítat	k5eAaImAgFnS	odmítat
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
kompromis	kompromis	k1gInSc4	kompromis
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
září	září	k1gNnSc2	září
byl	být	k5eAaImAgInS	být
československou	československý	k2eAgFnSc7d1	Československá
vládou	vláda	k1gFnSc7	vláda
schválen	schválit	k5eAaPmNgInS	schválit
Návrh	návrh	k1gInSc1	návrh
o	o	k7c6	o
postupu	postup	k1gInSc6	postup
jednání	jednání	k1gNnSc2	jednání
ohledně	ohledně	k7c2	ohledně
úpravy	úprava	k1gFnSc2	úprava
národnostních	národnostní	k2eAgFnPc2d1	národnostní
otázek	otázka	k1gFnPc2	otázka
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
základě	základ	k1gInSc6	základ
přímých	přímý	k2eAgInPc2d1	přímý
rozhovorů	rozhovor	k1gInPc2	rozhovor
vyjednavačů	vyjednavač	k1gMnPc2	vyjednavač
SdP	SdP	k1gFnPc2	SdP
<g/>
,	,	kIx,	,
a	a	k8xC	a
který	který	k3yIgMnSc1	který
vycházel	vycházet	k5eAaImAgMnS	vycházet
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
vstříc	vstříc	k7c3	vstříc
většině	většina	k1gFnSc3	většina
požadavků	požadavek	k1gInPc2	požadavek
Karlovarského	karlovarský	k2eAgInSc2d1	karlovarský
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
plán	plán	k1gInSc4	plán
přijala	přijmout	k5eAaPmAgFnS	přijmout
jak	jak	k8xS	jak
Runcimanova	Runcimanův	k2eAgFnSc1d1	Runcimanova
mise	mise	k1gFnSc1	mise
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
část	část	k1gFnSc1	část
z	z	k7c2	z
umírněnějších	umírněný	k2eAgInPc2d2	umírněnější
předáků	předák	k1gInPc2	předák
SdP	SdP	k1gMnPc2	SdP
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
uznávali	uznávat	k5eAaImAgMnP	uznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
plní	plnit	k5eAaImIp3nS	plnit
většinu	většina	k1gFnSc4	většina
Karlovarského	karlovarský	k2eAgInSc2d1	karlovarský
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Henleinových	Henleinův	k2eAgNnPc6d1	Henleinovo
dalších	další	k2eAgNnPc6d1	další
jednáních	jednání	k1gNnPc6	jednání
s	s	k7c7	s
Hitlerem	Hitler	k1gMnSc7	Hitler
ale	ale	k8xC	ale
SdP	SdP	k1gFnSc7	SdP
7	[number]	k4	7
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
vyprovokovala	vyprovokovat	k5eAaPmAgFnS	vyprovokovat
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc4	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
incident	incident	k1gInSc4	incident
v	v	k7c6	v
Moravské	moravský	k2eAgFnSc6d1	Moravská
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
vedení	vedení	k1gNnSc1	vedení
SdP	SdP	k1gFnSc4	SdP
využilo	využít	k5eAaPmAgNnS	využít
jako	jako	k8xS	jako
záminku	záminka	k1gFnSc4	záminka
k	k	k7c3	k
úplnému	úplný	k2eAgNnSc3d1	úplné
a	a	k8xC	a
konečnému	konečný	k2eAgNnSc3d1	konečné
přerušení	přerušení	k1gNnSc3	přerušení
jednání	jednání	k1gNnSc2	jednání
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
a	a	k8xC	a
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
prezident	prezident	k1gMnSc1	prezident
Beneš	Beneš	k1gMnSc1	Beneš
pronáší	pronášet	k5eAaImIp3nS	pronášet
rozhlasový	rozhlasový	k2eAgInSc4d1	rozhlasový
projev	projev	k1gInSc4	projev
adresovaný	adresovaný	k2eAgInSc4d1	adresovaný
Československu	Československo	k1gNnSc3	Československo
i	i	k8xC	i
celému	celý	k2eAgInSc3d1	celý
světu	svět	k1gInSc3	svět
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
je	být	k5eAaImIp3nS	být
tlumočen	tlumočit	k5eAaImNgInS	tlumočit
do	do	k7c2	do
13	[number]	k4	13
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Mám	mít	k5eAaImIp1nS	mít
nezlomnou	zlomný	k2eNgFnSc4d1	nezlomná
víru	víra	k1gFnSc4	víra
v	v	k7c4	v
náš	náš	k3xOp1gInSc4	náš
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
v	v	k7c4	v
jeho	jeho	k3xOp3gFnSc4	jeho
zdravost	zdravost	k1gFnSc4	zdravost
<g/>
,	,	kIx,	,
v	v	k7c4	v
jeho	jeho	k3xOp3gFnSc4	jeho
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
v	v	k7c4	v
jeho	jeho	k3xOp3gFnSc4	jeho
odolnost	odolnost	k1gFnSc4	odolnost
<g/>
,	,	kIx,	,
v	v	k7c4	v
jeho	jeho	k3xOp3gFnSc4	jeho
skvělou	skvělý	k2eAgFnSc4d1	skvělá
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
v	v	k7c4	v
nezdolatelného	zdolatelný	k2eNgMnSc4d1	nezdolatelný
ducha	duch	k1gMnSc4	duch
a	a	k8xC	a
oddanost	oddanost	k1gFnSc4	oddanost
všeho	všecek	k3xTgInSc2	všecek
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
vím	vědět	k5eAaImIp1nS	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
náš	náš	k3xOp1gInSc1	náš
stát	stát	k1gInSc1	stát
z	z	k7c2	z
dnešních	dnešní	k2eAgFnPc2d1	dnešní
obtíží	obtíž	k1gFnPc2	obtíž
vyjde	vyjít	k5eAaPmIp3nS	vyjít
vítězně	vítězně	k6eAd1	vítězně
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
titulky	titulek	k1gInPc7	titulek
novin	novina	k1gFnPc2	novina
hlásají	hlásat	k5eAaImIp3nP	hlásat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Anglie	Anglie	k1gFnSc1	Anglie
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
ve	v	k7c6	v
střehu	střeh	k1gInSc6	střeh
–	–	k?	–
celý	celý	k2eAgInSc1d1	celý
den	den	k1gInSc1	den
porad	porada	k1gFnPc2	porada
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
Ženevě	Ženeva	k1gFnSc6	Ženeva
o	o	k7c6	o
situaci	situace	k1gFnSc6	situace
<g/>
"	"	kIx"	"
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Hitler	Hitler	k1gMnSc1	Hitler
chce	chtít	k5eAaImIp3nS	chtít
ještě	ještě	k6eAd1	ještě
stupňovat	stupňovat	k5eAaImF	stupňovat
nátlak	nátlak	k1gInSc4	nátlak
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
Evropa	Evropa	k1gFnSc1	Evropa
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
spíše	spíše	k9	spíše
Československo	Československo	k1gNnSc1	Československo
s	s	k7c7	s
napětím	napětí	k1gNnSc7	napětí
očekává	očekávat	k5eAaImIp3nS	očekávat
Hitlerův	Hitlerův	k2eAgInSc1d1	Hitlerův
projev	projev	k1gInSc4	projev
naplánovaný	naplánovaný	k2eAgInSc4d1	naplánovaný
na	na	k7c4	na
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
sjezdu	sjezd	k1gInSc2	sjezd
NSDAP	NSDAP	kA	NSDAP
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
republiky	republika	k1gFnSc2	republika
ve	v	k7c6	v
smíšených	smíšený	k2eAgInPc6d1	smíšený
krajích	kraj	k1gInPc6	kraj
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
henleinovské	henleinovský	k2eAgFnPc1d1	henleinovská
demonstrace	demonstrace	k1gFnPc1	demonstrace
<g/>
,	,	kIx,	,
jednotně	jednotně	k6eAd1	jednotně
organizované	organizovaný	k2eAgFnPc1d1	organizovaná
<g/>
,	,	kIx,	,
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
podobným	podobný	k2eAgInSc7d1	podobný
průběhem	průběh	k1gInSc7	průběh
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
začalo	začít	k5eAaPmAgNnS	začít
Sudetoněmecké	sudetoněmecký	k2eAgNnSc4d1	Sudetoněmecké
povstání	povstání	k1gNnSc4	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
NSDAP	NSDAP	kA	NSDAP
v	v	k7c6	v
Norimberku	Norimberk	k1gInSc6	Norimberk
Hitler	Hitler	k1gMnSc1	Hitler
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
Československou	československý	k2eAgFnSc4d1	Československá
vládu	vláda	k1gFnSc4	vláda
za	za	k7c4	za
útlak	útlak	k1gInSc4	útlak
Němců	Němec	k1gMnPc2	Němec
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
podnítilo	podnítit	k5eAaPmAgNnS	podnítit
sudetské	sudetský	k2eAgMnPc4d1	sudetský
Němce	Němec	k1gMnPc4	Němec
k	k	k7c3	k
vyvolání	vyvolání	k1gNnSc3	vyvolání
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Nepokoje	nepokoj	k1gInPc1	nepokoj
byly	být	k5eAaImAgInP	být
za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
Československou	československý	k2eAgFnSc7d1	Československá
armádou	armáda	k1gFnSc7	armáda
i	i	k8xC	i
četnictvem	četnictvo	k1gNnSc7	četnictvo
násilně	násilně	k6eAd1	násilně
potlačeny	potlačen	k2eAgInPc1d1	potlačen
a	a	k8xC	a
vedoucí	vedoucí	k2eAgMnPc1d1	vedoucí
činitelé	činitel	k1gMnPc1	činitel
SdP	SdP	k1gFnPc2	SdP
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Povstání	povstání	k1gNnSc1	povstání
eskalovalo	eskalovat	k5eAaImAgNnS	eskalovat
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
nepokoje	nepokoj	k1gInSc2	nepokoj
zachvátily	zachvátit	k5eAaPmAgFnP	zachvátit
další	další	k2eAgFnPc1d1	další
oblasti	oblast	k1gFnPc1	oblast
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
Československá	československý	k2eAgFnSc1d1	Československá
<g/>
.	.	kIx.	.
vláda	vláda	k1gFnSc1	vláda
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
stanné	stanný	k2eAgNnSc4d1	stanné
právo	právo	k1gNnSc4	právo
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
pohraničních	pohraniční	k2eAgInPc6d1	pohraniční
okresech	okres	k1gInPc6	okres
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začalo	začít	k5eAaPmAgNnS	začít
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
bojům	boj	k1gInPc3	boj
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgInSc1d3	nejznámější
střet	střet	k1gInSc1	střet
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
Habersbirku	Habersbirek	k1gInSc6	Habersbirek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
němečtí	německý	k2eAgMnPc1d1	německý
ordneři	ordner	k1gMnPc1	ordner
přepadli	přepadnout	k5eAaPmAgMnP	přepadnout
četnickou	četnický	k2eAgFnSc4d1	četnická
stanici	stanice	k1gFnSc4	stanice
a	a	k8xC	a
zavraždili	zavraždit	k5eAaPmAgMnP	zavraždit
čtyři	čtyři	k4xCgMnPc4	čtyři
četníky	četník	k1gMnPc4	četník
<g/>
.	.	kIx.	.
</s>
<s>
SdP	SdP	k?	SdP
dává	dávat	k5eAaImIp3nS	dávat
vládě	vláda	k1gFnSc3	vláda
šestihodinové	šestihodinový	k2eAgNnSc1d1	šestihodinové
ultimátum	ultimátum	k1gNnSc1	ultimátum
na	na	k7c4	na
odvolání	odvolání	k1gNnSc4	odvolání
stanného	stanný	k2eAgNnSc2d1	stanné
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
stažení	stažení	k1gNnSc2	stažení
české	český	k2eAgFnSc2d1	Česká
policie	policie	k1gFnSc2	policie
ze	z	k7c2	z
Sudet	Sudety	k1gFnPc2	Sudety
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
budou	být	k5eAaImBp3nP	být
požadavky	požadavek	k1gInPc1	požadavek
splněny	splnit	k5eAaPmNgInP	splnit
<g/>
,	,	kIx,	,
slibuje	slibovat	k5eAaImIp3nS	slibovat
zde	zde	k6eAd1	zde
vedení	vedení	k1gNnSc1	vedení
SdP	SdP	k1gFnSc2	SdP
zajistit	zajistit	k5eAaPmF	zajistit
klid	klid	k1gInSc1	klid
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
probíhala	probíhat	k5eAaImAgFnS	probíhat
policejní	policejní	k2eAgFnSc1d1	policejní
razie	razie	k1gFnSc1	razie
v	v	k7c6	v
chebských	chebský	k2eAgInPc6d1	chebský
hotelích	hotel	k1gInPc6	hotel
Welzel	Welzel	k1gFnSc2	Welzel
a	a	k8xC	a
Victoria	Victorium	k1gNnSc2	Victorium
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
sloužil	sloužit	k5eAaImAgInS	sloužit
jako	jako	k8xS	jako
místní	místní	k2eAgFnSc1d1	místní
centrála	centrála	k1gFnSc1	centrála
SdP	SdP	k1gFnSc2	SdP
<g/>
.	.	kIx.	.
</s>
<s>
Policie	policie	k1gFnSc1	policie
pátrala	pátrat	k5eAaImAgFnS	pátrat
po	po	k7c6	po
ukrytých	ukrytý	k2eAgFnPc6d1	ukrytá
ilegálních	ilegální	k2eAgFnPc6d1	ilegální
zbraních	zbraň	k1gFnPc6	zbraň
(	(	kIx(	(
<g/>
ve	v	k7c6	v
Victorii	Victorie	k1gFnSc6	Victorie
později	pozdě	k6eAd2	pozdě
nalezla	naleznout	k5eAaPmAgFnS	naleznout
9	[number]	k4	9
pistolí	pistol	k1gFnPc2	pistol
a	a	k8xC	a
850	[number]	k4	850
nábojů	náboj	k1gInPc2	náboj
<g/>
)	)	kIx)	)
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
ze	z	k7c2	z
zálohy	záloha	k1gFnSc2	záloha
napadena	napaden	k2eAgFnSc1d1	napadena
střelbou	střelba	k1gFnSc7	střelba
ze	z	k7c2	z
dvou	dva	k4xCgInPc2	dva
kulometů	kulomet	k1gInPc2	kulomet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přestřelce	přestřelka	k1gFnSc6	přestřelka
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
šest	šest	k4xCc1	šest
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
jeden	jeden	k4xCgMnSc1	jeden
policista	policista	k1gMnSc1	policista
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc1	dva
železničáři	železničář	k1gMnPc1	železničář
a	a	k8xC	a
tři	tři	k4xCgMnPc1	tři
náhodní	náhodný	k2eAgMnPc1d1	náhodný
kolemjdoucí	kolemjdoucí	k1gMnPc1	kolemjdoucí
<g/>
.	.	kIx.	.
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
s	s	k7c7	s
tajným	tajný	k2eAgInSc7d1	tajný
pověření	pověření	k1gNnPc4	pověření
prezidenta	prezident	k1gMnSc2	prezident
Beneše	Beneš	k1gMnSc2	Beneš
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
Jaromír	Jaromír	k1gMnSc1	Jaromír
Nečas	Nečas	k1gMnSc1	Nečas
tlumočit	tlumočit	k5eAaImF	tlumočit
Benešovu	Benešův	k2eAgFnSc4d1	Benešova
ochotu	ochota	k1gFnSc4	ochota
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
části	část	k1gFnSc2	část
pohraničního	pohraniční	k2eAgNnSc2d1	pohraniční
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
se	se	k3xPyFc4	se
sešel	sejít	k5eAaPmAgMnS	sejít
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
s	s	k7c7	s
britským	britský	k2eAgMnSc7d1	britský
ministerským	ministerský	k2eAgMnSc7d1	ministerský
předsedou	předseda	k1gMnSc7	předseda
Chamberlainem	Chamberlain	k1gMnSc7	Chamberlain
na	na	k7c6	na
Berghofu	Berghof	k1gInSc6	Berghof
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Hitler	Hitler	k1gMnSc1	Hitler
žádal	žádat	k5eAaImAgMnS	žádat
připojení	připojení	k1gNnSc4	připojení
českého	český	k2eAgNnSc2d1	české
pohraničního	pohraniční	k2eAgNnSc2d1	pohraniční
území	území	k1gNnSc2	území
k	k	k7c3	k
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
Sudetendeutsche	Sudetendeutsch	k1gInSc2	Sudetendeutsch
Partei	Parte	k1gFnSc2	Parte
je	být	k5eAaImIp3nS	být
oficiálně	oficiálně	k6eAd1	oficiálně
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
vydán	vydán	k2eAgInSc1d1	vydán
zatykač	zatykač	k1gInSc1	zatykač
na	na	k7c4	na
Konrada	Konrada	k1gFnSc1	Konrada
Henleina	Henlein	k1gMnSc2	Henlein
a	a	k8xC	a
Karla	Karel	k1gMnSc2	Karel
Hermanna	Hermann	k1gMnSc2	Hermann
Franka	Frank	k1gMnSc2	Frank
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
kterým	který	k3yQgMnPc3	který
je	být	k5eAaImIp3nS	být
vedeno	veden	k2eAgNnSc4d1	vedeno
trestní	trestní	k2eAgNnSc4d1	trestní
řízení	řízení	k1gNnSc4	řízení
pro	pro	k7c4	pro
paragrafy	paragraf	k1gInPc4	paragraf
1	[number]	k4	1
a	a	k8xC	a
3	[number]	k4	3
zákona	zákon	k1gInSc2	zákon
na	na	k7c4	na
ochranu	ochrana	k1gFnSc4	ochrana
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
úklady	úklad	k1gInPc1	úklad
a	a	k8xC	a
ohrožování	ohrožování	k1gNnSc1	ohrožování
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lord	lord	k1gMnSc1	lord
Runciman	Runciman	k1gMnSc1	Runciman
odlétá	odlétat	k5eAaImIp3nS	odlétat
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
ministerským	ministerský	k2eAgMnSc7d1	ministerský
předsedou	předseda	k1gMnSc7	předseda
Chamberlainem	Chamberlain	k1gMnSc7	Chamberlain
a	a	k8xC	a
podal	podat	k5eAaPmAgMnS	podat
mu	on	k3xPp3gMnSc3	on
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
průběhu	průběh	k1gInSc6	průběh
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
jako	jako	k8xC	jako
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
zákaz	zákaz	k1gInSc4	zákaz
SdP	SdP	k1gMnSc1	SdP
byl	být	k5eAaImAgMnS	být
Konrádem	Konrád	k1gMnSc7	Konrád
Henleinem	Henlein	k1gInSc7	Henlein
schválen	schválen	k2eAgInSc4d1	schválen
vznik	vznik	k1gInSc4	vznik
Sudetoněmeckého	sudetoněmecký	k2eAgInSc2d1	sudetoněmecký
freikorpsu	freikorps	k1gInSc2	freikorps
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zahájil	zahájit	k5eAaPmAgMnS	zahájit
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
na	na	k7c4	na
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1938	[number]	k4	1938
přepady	přepad	k1gInPc1	přepad
československých	československý	k2eAgFnPc2d1	Československá
celnic	celnice	k1gFnPc2	celnice
a	a	k8xC	a
policejních	policejní	k2eAgFnPc2d1	policejní
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Začalo	začít	k5eAaPmAgNnS	začít
docházet	docházet	k5eAaImF	docházet
k	k	k7c3	k
ozbrojeným	ozbrojený	k2eAgInPc3d1	ozbrojený
střetům	střet	k1gInPc3	střet
<g/>
.	.	kIx.	.
</s>
<s>
Generál	generál	k1gMnSc1	generál
Ludvík	Ludvík	k1gMnSc1	Ludvík
Krejčí	Krejčí	k1gMnSc1	Krejčí
se	se	k3xPyFc4	se
obrací	obracet	k5eAaImIp3nS	obracet
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
Beneše	Beneš	k1gMnSc4	Beneš
se	s	k7c7	s
žádostí	žádost	k1gFnSc7	žádost
o	o	k7c4	o
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
povolání	povolání	k1gNnSc4	povolání
dvou	dva	k4xCgInPc2	dva
ročníků	ročník	k1gInPc2	ročník
zálohy	záloha	k1gFnSc2	záloha
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
požadavek	požadavek	k1gInSc1	požadavek
je	být	k5eAaImIp3nS	být
odmítnut	odmítnout	k5eAaPmNgInS	odmítnout
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
zpravodajská	zpravodajský	k2eAgFnSc1d1	zpravodajská
služba	služba	k1gFnSc1	služba
dodala	dodat	k5eAaPmAgFnS	dodat
hodnověrné	hodnověrný	k2eAgFnPc4d1	hodnověrná
informace	informace	k1gFnPc4	informace
o	o	k7c4	o
stahování	stahování	k1gNnSc4	stahování
německých	německý	k2eAgFnPc2d1	německá
divizí	divize	k1gFnPc2	divize
k	k	k7c3	k
hranicím	hranice	k1gFnPc3	hranice
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
této	tento	k3xDgFnSc2	tento
situace	situace	k1gFnSc2	situace
podává	podávat	k5eAaImIp3nS	podávat
náčelník	náčelník	k1gMnSc1	náčelník
hlavního	hlavní	k2eAgInSc2d1	hlavní
štábu	štáb	k1gInSc2	štáb
armádní	armádní	k2eAgMnSc1d1	armádní
generál	generál	k1gMnSc1	generál
Krejčí	Krejčí	k2eAgFnSc4d1	Krejčí
demisi	demise	k1gFnSc4	demise
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
se	se	k3xPyFc4	se
zřizuje	zřizovat	k5eAaImIp3nS	zřizovat
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
propagandy	propaganda	k1gFnSc2	propaganda
<g/>
,	,	kIx,	,
ministerský	ministerský	k2eAgMnSc1d1	ministerský
předseda	předseda	k1gMnSc1	předseda
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Milan	Milan	k1gMnSc1	Milan
Hodža	Hodža	k1gMnSc1	Hodža
pronáší	pronášet	k5eAaImIp3nS	pronášet
rozhlasový	rozhlasový	k2eAgInSc4d1	rozhlasový
projev	projev	k1gInSc4	projev
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
přehled	přehled	k1gInSc4	přehled
o	o	k7c6	o
československém	československý	k2eAgNnSc6d1	Československé
úsilí	úsilí	k1gNnSc6	úsilí
na	na	k7c6	na
zachování	zachování	k1gNnSc6	zachování
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Francouzi	Francouz	k1gMnPc1	Francouz
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
souhlas	souhlas	k1gInSc4	souhlas
se	s	k7c7	s
stanoviskem	stanovisko	k1gNnSc7	stanovisko
britské	britský	k2eAgFnSc2d1	britská
vlády	vláda	k1gFnSc2	vláda
o	o	k7c4	o
revizi	revize	k1gFnSc4	revize
čs	čs	kA	čs
<g/>
.	.	kIx.	.
hranic	hranice	k1gFnPc2	hranice
podle	podle	k7c2	podle
sedmého	sedmý	k4xOgInSc2	sedmý
tajného	tajný	k2eAgInSc2d1	tajný
návrhu	návrh	k1gInSc2	návrh
E.	E.	kA	E.
Beneše	Beneš	k1gMnSc2	Beneš
předloženého	předložený	k2eAgMnSc2d1	předložený
Jaromírem	Jaromír	k1gMnSc7	Jaromír
Nečasem	nečas	k1gInSc7	nečas
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
podle	podle	k7c2	podle
tajného	tajný	k2eAgInSc2d1	tajný
sedmého	sedmý	k4xOgInSc2	sedmý
návrhu	návrh	k1gInSc2	návrh
E.	E.	kA	E.
Beneše	Beneš	k1gMnSc2	Beneš
předloženého	předložený	k2eAgMnSc2d1	předložený
Jaromírem	Jaromír	k1gMnSc7	Jaromír
Nečasem	nečas	k1gInSc7	nečas
navrhly	navrhnout	k5eAaPmAgFnP	navrhnout
oficiálně	oficiálně	k6eAd1	oficiálně
vlády	vláda	k1gFnSc2	vláda
Anglie	Anglie	k1gFnSc2	Anglie
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
československé	československý	k2eAgFnSc2d1	Československá
vládě	vláda	k1gFnSc3	vláda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
hrozící	hrozící	k2eAgInSc1d1	hrozící
ozbrojený	ozbrojený	k2eAgInSc1d1	ozbrojený
konflikt	konflikt	k1gInSc1	konflikt
mezi	mezi	k7c7	mezi
národnostními	národnostní	k2eAgFnPc7d1	národnostní
skupinami	skupina	k1gFnPc7	skupina
v	v	k7c6	v
ČSR	ČSR	kA	ČSR
řešila	řešit	k5eAaImAgFnS	řešit
postoupením	postoupení	k1gNnSc7	postoupení
pohraniční	pohraniční	k2eAgFnSc2d1	pohraniční
oblasti	oblast	k1gFnSc2	oblast
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
<g/>
%	%	kIx~	%
německého	německý	k2eAgNnSc2d1	německé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
posledního	poslední	k2eAgNnSc2d1	poslední
rakouského	rakouský	k2eAgNnSc2d1	rakouské
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
<g/>
,	,	kIx,	,
zjišťujícího	zjišťující	k2eAgInSc2d1	zjišťující
národnost	národnost	k1gFnSc4	národnost
podle	podle	k7c2	podle
tzv.	tzv.	kA	tzv.
obcovací	obcovací	k2eAgFnSc2d1	obcovací
řeči	řeč	k1gFnSc2	řeč
<g/>
)	)	kIx)	)
Německu	Německo	k1gNnSc3	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
vláda	vláda	k1gFnSc1	vláda
však	však	k9	však
tento	tento	k3xDgInSc4	tento
požadavek	požadavek	k1gInSc4	požadavek
odmítla	odmítnout	k5eAaPmAgFnS	odmítnout
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
zaslalo	zaslat	k5eAaPmAgNnS	zaslat
vedení	vedení	k1gNnSc1	vedení
SSSR	SSSR	kA	SSSR
E.	E.	kA	E.
Benešovi	Beneš	k1gMnSc3	Beneš
telegram	telegram	k1gInSc4	telegram
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
Stalin	Stalin	k1gMnSc1	Stalin
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
sovětští	sovětský	k2eAgMnPc1d1	sovětský
činitelé	činitel	k1gMnPc1	činitel
potvrzovali	potvrzovat	k5eAaImAgMnP	potvrzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
připraveni	připraven	k2eAgMnPc1d1	připraven
Československu	Československo	k1gNnSc3	Československo
pomoci	pomoct	k5eAaPmF	pomoct
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
pomoci	pomoc	k1gFnSc2	pomoc
zúčastní	zúčastnit	k5eAaPmIp3nS	zúčastnit
i	i	k9	i
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
del	del	k?	del
uzavřených	uzavřený	k2eAgFnPc2d1	uzavřená
smluv	smlouva	k1gFnPc2	smlouva
mezi	mezi	k7c7	mezi
SSSR	SSSR	kA	SSSR
a	a	k8xC	a
ČSR	ČSR	kA	ČSR
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nP	zářit
vyslanci	vyslanec	k1gMnPc1	vyslanec
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
předložili	předložit	k5eAaPmAgMnP	předložit
Československu	Československo	k1gNnSc6	Československo
ultimativní	ultimativní	k2eAgInPc4d1	ultimativní
požadavky	požadavek	k1gInPc4	požadavek
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yQnSc4	což
vláda	vláda	k1gFnSc1	vláda
přistoupila	přistoupit	k5eAaPmAgFnS	přistoupit
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
však	však	k9	však
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
demonstrace	demonstrace	k1gFnSc2	demonstrace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
požadovaly	požadovat	k5eAaImAgFnP	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
svolán	svolat	k5eAaPmNgInS	svolat
parlament	parlament	k1gInSc1	parlament
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
ustavena	ustaven	k2eAgFnSc1d1	ustavena
vláda	vláda	k1gFnSc1	vláda
skutečné	skutečný	k2eAgFnSc2d1	skutečná
obrany	obrana	k1gFnSc2	obrana
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
Československa	Československo	k1gNnSc2	Československo
ve	v	k7c6	v
Společnosti	společnost	k1gFnSc6	společnost
národů	národ	k1gInPc2	národ
sovětský	sovětský	k2eAgMnSc1d1	sovětský
ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Maxim	maxim	k1gInSc1	maxim
Litvinov	Litvinovo	k1gNnPc2	Litvinovo
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
podrobil	podrobit	k5eAaPmAgMnS	podrobit
nátlak	nátlak	k1gInSc4	nátlak
na	na	k7c6	na
ČSR	ČSR	kA	ČSR
velké	velký	k2eAgFnSc6d1	velká
kritice	kritika	k1gFnSc6	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
SSSR	SSSR	kA	SSSR
ani	ani	k8xC	ani
částečně	částečně	k6eAd1	částečně
nemobilizovalo	mobilizovat	k5eNaBmAgNnS	mobilizovat
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
učinila	učinit	k5eAaImAgFnS	učinit
jen	jen	k6eAd1	jen
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
SSSR	SSSR	kA	SSSR
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
pakt	pakt	k1gInSc1	pakt
o	o	k7c6	o
neútočení	neútočení	k1gNnSc6	neútočení
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
SSSR	SSSR	kA	SSSR
pakt	pakt	k1gInSc1	pakt
o	o	k7c6	o
neútočení	neútočení	k1gNnSc6	neútočení
s	s	k7c7	s
druhou	druhý	k4xOgFnSc7	druhý
největší	veliký	k2eAgFnSc7d3	veliký
fašistickou	fašistický	k2eAgFnSc7d1	fašistická
zemí	zem	k1gFnSc7	zem
Japonskem	Japonsko	k1gNnSc7	Japonsko
<g/>
.	.	kIx.	.
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
generální	generální	k2eAgFnSc1d1	generální
stávka	stávka	k1gFnSc1	stávka
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
výsledkem	výsledek	k1gInSc7	výsledek
byl	být	k5eAaImAgInS	být
pád	pád	k1gInSc1	pád
Hodžovy	Hodžův	k2eAgFnSc2d1	Hodžova
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
nastolení	nastolení	k1gNnSc2	nastolení
úřednické	úřednický	k2eAgFnSc2d1	úřednická
vlády	vláda	k1gFnSc2	vláda
vedené	vedený	k2eAgFnSc2d1	vedená
generálem	generál	k1gMnSc7	generál
Janem	Jan	k1gMnSc7	Jan
Syrovým	syrový	k2eAgMnSc7d1	syrový
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
a	a	k8xC	a
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
probíhala	probíhat	k5eAaImAgFnS	probíhat
jednání	jednání	k1gNnSc4	jednání
mezi	mezi	k7c7	mezi
Hitlerem	Hitler	k1gMnSc7	Hitler
a	a	k8xC	a
Chamberlainem	Chamberlain	k1gMnSc7	Chamberlain
<g/>
.	.	kIx.	.
</s>
<s>
Henleinovcům	henleinovec	k1gMnPc3	henleinovec
se	se	k3xPyFc4	se
daří	dařit	k5eAaImIp3nS	dařit
obsadit	obsadit	k5eAaPmF	obsadit
celý	celý	k2eAgInSc1d1	celý
Varnsdorf	Varnsdorf	k1gInSc1	Varnsdorf
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
Šluknovského	šluknovský	k2eAgInSc2d1	šluknovský
výběžku	výběžek	k1gInSc2	výběžek
<g/>
.	.	kIx.	.
</s>
<s>
Chamberlain	Chamberlain	k1gInSc1	Chamberlain
znovu	znovu	k6eAd1	znovu
jedná	jednat	k5eAaImIp3nS	jednat
s	s	k7c7	s
Hitlerem	Hitler	k1gMnSc7	Hitler
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
v	v	k7c6	v
Bad	Bad	k1gFnSc6	Bad
Godesbergu	Godesberg	k1gInSc2	Godesberg
<g/>
.	.	kIx.	.
</s>
<s>
Hitler	Hitler	k1gMnSc1	Hitler
žádá	žádat	k5eAaImIp3nS	žádat
také	také	k9	také
splnění	splnění	k1gNnSc3	splnění
územních	územní	k2eAgInPc2d1	územní
nároků	nárok	k1gInPc2	nárok
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
Československé	československý	k2eAgFnSc2d1	Československá
síly	síla	k1gFnSc2	síla
znovu	znovu	k6eAd1	znovu
dobývají	dobývat	k5eAaImIp3nP	dobývat
Šluknovsko	Šluknovsko	k1gNnSc4	Šluknovsko
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
povstalcům	povstalec	k1gMnPc3	povstalec
podporovaným	podporovaný	k2eAgMnPc3d1	podporovaný
jednotkami	jednotka	k1gFnPc7	jednotka
SA	SA	kA	SA
a	a	k8xC	a
SS	SS	kA	SS
nastupují	nastupovat	k5eAaImIp3nP	nastupovat
dva	dva	k4xCgInPc4	dva
prapory	prapor	k1gInPc4	prapor
čs	čs	kA	čs
<g/>
.	.	kIx.	.
pěchoty	pěchota	k1gFnSc2	pěchota
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
útočné	útočný	k2eAgFnSc2d1	útočná
vozby	vozba	k1gFnSc2	vozba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
je	být	k5eAaImIp3nS	být
vyhlášena	vyhlášen	k2eAgFnSc1d1	vyhlášena
mobilizace	mobilizace	k1gFnSc1	mobilizace
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
mobilizačním	mobilizační	k2eAgInSc7d1	mobilizační
dnem	den	k1gInSc7	den
je	být	k5eAaImIp3nS	být
stanovena	stanovit	k5eAaPmNgFnS	stanovit
neděle	neděle	k1gFnSc1	neděle
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
probíhá	probíhat	k5eAaImIp3nS	probíhat
mobilizace	mobilizace	k1gFnSc1	mobilizace
podle	podle	k7c2	podle
nástupového	nástupový	k2eAgInSc2d1	nástupový
plánu	plán	k1gInSc2	plán
VII	VII	kA	VII
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
válečný	válečný	k2eAgInSc4d1	válečný
plán	plán	k1gInSc4	plán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bruntále	Bruntál	k1gInSc6	Bruntál
před	před	k7c7	před
půlnocí	půlnoc	k1gFnSc7	půlnoc
začíná	začínat	k5eAaImIp3nS	začínat
velká	velký	k2eAgFnSc1d1	velká
přestřelka	přestřelka	k1gFnSc1	přestřelka
mezi	mezi	k7c7	mezi
čs	čs	kA	čs
<g/>
.	.	kIx.	.
ozbrojenými	ozbrojený	k2eAgFnPc7d1	ozbrojená
složkami	složka	k1gFnPc7	složka
a	a	k8xC	a
SdFK	SdFK	k1gFnPc7	SdFK
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
částečnou	částečný	k2eAgFnSc4d1	částečná
mobilizaci	mobilizace	k1gFnSc4	mobilizace
své	svůj	k3xOyFgFnSc2	svůj
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
britský	britský	k2eAgMnSc1d1	britský
velvyslanec	velvyslanec	k1gMnSc1	velvyslanec
předává	předávat	k5eAaImIp3nS	předávat
čs	čs	kA	čs
<g/>
.	.	kIx.	.
vládě	vláda	k1gFnSc3	vláda
text	text	k1gInSc4	text
Hitlerova	Hitlerův	k2eAgNnSc2d1	Hitlerovo
memoranda	memorandum	k1gNnSc2	memorandum
z	z	k7c2	z
Godesbergu	Godesberg	k1gInSc2	Godesberg
<g/>
,	,	kIx,	,
a	a	k8xC	a
ta	ten	k3xDgFnSc1	ten
jej	on	k3xPp3gMnSc4	on
následně	následně	k6eAd1	následně
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
oznamuje	oznamovat	k5eAaImIp3nS	oznamovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
případě	případ	k1gInSc6	případ
německého	německý	k2eAgInSc2d1	německý
útoku	útok	k1gInSc2	útok
přijde	přijít	k5eAaPmIp3nS	přijít
Československu	Československo	k1gNnSc3	Československo
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
vyslal	vyslat	k5eAaPmAgMnS	vyslat
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
svého	své	k1gNnSc2	své
vyjednavače	vyjednavač	k1gMnSc2	vyjednavač
H.	H.	kA	H.
Wilsona	Wilson	k1gMnSc2	Wilson
k	k	k7c3	k
jednání	jednání	k1gNnSc3	jednání
s	s	k7c7	s
Hitlerem	Hitler	k1gMnSc7	Hitler
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
Hitler	Hitler	k1gMnSc1	Hitler
zasílá	zasílat	k5eAaImIp3nS	zasílat
britskému	britský	k2eAgMnSc3d1	britský
ministerskému	ministerský	k2eAgMnSc3d1	ministerský
předsedovi	předseda	k1gMnSc3	předseda
Chamberlainovi	Chamberlain	k1gMnSc3	Chamberlain
ultimativní	ultimativní	k2eAgInSc4d1	ultimativní
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
domáhá	domáhat	k5eAaImIp3nS	domáhat
svých	svůj	k3xOyFgInPc2	svůj
požadavků	požadavek	k1gInPc2	požadavek
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
splnění	splnění	k1gNnSc1	splnění
žádá	žádat	k5eAaImIp3nS	žádat
do	do	k7c2	do
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
případě	případ	k1gInSc6	případ
hrozí	hrozit	k5eAaImIp3nS	hrozit
anšlusem	anšlus	k1gInSc7	anšlus
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Nadále	nadále	k6eAd1	nadále
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
československá	československý	k2eAgFnSc1d1	Československá
mobilizace	mobilizace	k1gFnSc1	mobilizace
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
ozbrojené	ozbrojený	k2eAgInPc1d1	ozbrojený
střety	střet	k1gInPc1	střet
v	v	k7c6	v
čs	čs	kA	čs
<g/>
.	.	kIx.	.
pohraničí	pohraničí	k1gNnSc1	pohraničí
<g/>
.	.	kIx.	.
</s>
<s>
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
promluvil	promluvit	k5eAaPmAgMnS	promluvit
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
rozhlase	rozhlas	k1gInSc6	rozhlas
a	a	k8xC	a
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
Jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
hrozné	hrozný	k2eAgNnSc1d1	hrozné
<g/>
,	,	kIx,	,
fantastické	fantastický	k2eAgNnSc1d1	fantastické
<g/>
,	,	kIx,	,
neuvěřitelné	uvěřitelný	k2eNgNnSc1d1	neuvěřitelné
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
<g/>
-li	i	k?	-li
Britové	Brit	k1gMnPc1	Brit
připravovat	připravovat	k5eAaImF	připravovat
zákopy	zákop	k1gInPc4	zákop
a	a	k8xC	a
zkoušet	zkoušet	k5eAaImF	zkoušet
plynové	plynový	k2eAgFnSc2d1	plynová
masky	maska	k1gFnSc2	maska
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
pro	pro	k7c4	pro
spor	spor	k1gInSc4	spor
v	v	k7c6	v
daleké	daleký	k2eAgFnSc6d1	daleká
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
mezi	mezi	k7c7	mezi
lidem	lid	k1gInSc7	lid
<g/>
,	,	kIx,	,
o	o	k7c6	o
<g />
.	.	kIx.	.
</s>
<s>
němž	jenž	k3xRgMnSc6	jenž
my	my	k3xPp1nPc1	my
nevíme	vědět	k5eNaImIp1nP	vědět
nic	nic	k3yNnSc4	nic
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
A	a	k9	a
i	i	k9	i
kdybychom	kdyby	kYmCp1nP	kdyby
měli	mít	k5eAaImAgMnP	mít
sebevětší	sebevětší	k2eAgFnPc4d1	sebevětší
sympatie	sympatie	k1gFnPc4	sympatie
s	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
národem	národ	k1gInSc7	národ
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
němuž	jenž	k3xRgNnSc3	jenž
stojí	stát	k5eAaImIp3nS	stát
veliký	veliký	k2eAgInSc1d1	veliký
a	a	k8xC	a
mocný	mocný	k2eAgMnSc1d1	mocný
soused	soused	k1gMnSc1	soused
<g/>
,	,	kIx,	,
nemůžeme	moct	k5eNaImIp1nP	moct
jednat	jednat	k5eAaImF	jednat
za	za	k7c2	za
všech	všecek	k3xTgFnPc2	všecek
okolností	okolnost	k1gFnPc2	okolnost
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
přivedli	přivést	k5eAaPmAgMnP	přivést
celou	celý	k2eAgFnSc4d1	celá
britskou	britský	k2eAgFnSc4d1	britská
říši	říše	k1gFnSc4	říše
do	do	k7c2	do
války	válka	k1gFnSc2	válka
prostě	prostě	k9	prostě
jen	jen	k9	jen
kvůli	kvůli	k7c3	kvůli
němu	on	k3xPp3gMnSc3	on
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
Neville	Neville	k1gNnSc2	Neville
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
upozornil	upozornit	k5eAaPmAgMnS	upozornit
v	v	k7c6	v
dopise	dopis	k1gInSc6	dopis
Hitlera	Hitler	k1gMnSc2	Hitler
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
vše	všechen	k3xTgNnSc1	všechen
podstatné	podstatný	k2eAgNnSc1d1	podstatné
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
řešeno	řešit	k5eAaImNgNnS	řešit
bez	bez	k7c2	bez
války	válka	k1gFnSc2	válka
a	a	k8xC	a
bez	bez	k7c2	bez
odkladu	odklad	k1gInSc2	odklad
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Zároveň	zároveň	k6eAd1	zároveň
požádal	požádat	k5eAaPmAgMnS	požádat
B.	B.	kA	B.
Mussoliniho	Mussolini	k1gMnSc2	Mussolini
o	o	k7c6	o
prostřednictví	prostřednictví	k1gNnSc6	prostřednictví
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
jednání	jednání	k1gNnSc6	jednání
o	o	k7c6	o
německých	německý	k2eAgInPc6d1	německý
požadavcích	požadavek	k1gInPc6	požadavek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Mussoliniho	Mussolini	k1gMnSc4	Mussolini
výzvu	výzva	k1gFnSc4	výzva
dal	dát	k5eAaPmAgMnS	dát
Hitler	Hitler	k1gMnSc1	Hitler
souhlas	souhlas	k1gInSc4	souhlas
ke	k	k7c3	k
schůzce	schůzka	k1gFnSc3	schůzka
představitelů	představitel	k1gMnPc2	představitel
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
uskutečnila	uskutečnit	k5eAaPmAgFnS	uskutečnit
o	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
jsou	být	k5eAaImIp3nP	být
zahájena	zahájit	k5eAaPmNgNnP	zahájit
jednání	jednání	k1gNnPc1	jednání
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
půlnoci	půlnoc	k1gFnSc6	půlnoc
byla	být	k5eAaImAgFnS	být
dohoda	dohoda	k1gFnSc1	dohoda
podepsána	podepsat	k5eAaPmNgFnS	podepsat
a	a	k8xC	a
předána	předat	k5eAaPmNgFnS	předat
vyslanci	vyslanec	k1gMnSc3	vyslanec
Mastnému	mastný	k2eAgMnSc3d1	mastný
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
doručil	doručit	k5eAaPmAgMnS	doručit
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Beneš	Beneš	k1gMnSc1	Beneš
se	se	k3xPyFc4	se
o	o	k7c6	o
jejím	její	k3xOp3gNnSc6	její
podepsání	podepsání	k1gNnSc6	podepsání
dozvěděl	dozvědět	k5eAaPmAgInS	dozvědět
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
tajemníka	tajemník	k1gMnSc2	tajemník
z	z	k7c2	z
německého	německý	k2eAgNnSc2d1	německé
rádia	rádio	k1gNnSc2	rádio
ve	v	k7c4	v
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
ráno	ráno	k6eAd1	ráno
<g/>
.	.	kIx.	.
</s>
<s>
Toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
dopoledne	dopoledne	k6eAd1	dopoledne
zasedla	zasednout	k5eAaPmAgFnS	zasednout
na	na	k7c6	na
mimořádném	mimořádný	k2eAgNnSc6d1	mimořádné
zasedání	zasedání	k1gNnSc6	zasedání
československá	československý	k2eAgFnSc1d1	Československá
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
po	po	k7c6	po
pouhých	pouhý	k2eAgNnPc6d1	pouhé
patnácti	patnáct	k4xCc6	patnáct
minutách	minuta	k1gFnPc6	minuta
jednání	jednání	k1gNnPc2	jednání
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
hlasování	hlasování	k1gNnSc2	hlasování
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
dohodu	dohoda	k1gFnSc4	dohoda
přijímá	přijímat	k5eAaImIp3nS	přijímat
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
podepsal	podepsat	k5eAaPmAgMnS	podepsat
generál	generál	k1gMnSc1	generál
Krejčí	Krejčí	k1gMnSc1	Krejčí
rozkaz	rozkaz	k1gInSc4	rozkaz
potvrzující	potvrzující	k2eAgNnSc4d1	potvrzující
vydání	vydání	k1gNnSc4	vydání
území	území	k1gNnSc2	území
a	a	k8xC	a
již	již	k6eAd1	již
následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
započalo	započnout	k5eAaPmAgNnS	započnout
obsazování	obsazování	k1gNnSc1	obsazování
pohraničí	pohraničí	k1gNnSc2	pohraničí
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
podpisu	podpis	k1gInSc3	podpis
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
např.	např.	kA	např.
náčelník	náčelník	k1gMnSc1	náčelník
francouzské	francouzský	k2eAgFnSc2d1	francouzská
vojenské	vojenský	k2eAgFnSc2d1	vojenská
mise	mise	k1gFnSc2	mise
v	v	k7c6	v
ČSR	ČSR	kA	ČSR
generál	generál	k1gMnSc1	generál
Louis	Louis	k1gMnSc1	Louis
Faucher	Fauchra	k1gFnPc2	Fauchra
nebo	nebo	k8xC	nebo
První	první	k4xOgMnSc1	první
lord	lord	k1gMnSc1	lord
admirality	admiralita	k1gFnSc2	admiralita
Duff	Duff	k1gMnSc1	Duff
Cooper	Cooper	k1gMnSc1	Cooper
<g/>
,	,	kIx,	,
dohodu	dohoda	k1gFnSc4	dohoda
silně	silně	k6eAd1	silně
kritizovala	kritizovat	k5eAaImAgFnS	kritizovat
taky	taky	k6eAd1	taky
Labouristická	labouristický	k2eAgFnSc1d1	labouristická
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
Vilém	Vilém	k1gMnSc1	Vilém
poslal	poslat	k5eAaPmAgMnS	poslat
z	z	k7c2	z
exilu	exil	k1gInSc2	exil
gratulaci	gratulace	k1gFnSc4	gratulace
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
královně	královna	k1gFnSc3	královna
Mary	Mary	k1gFnPc3	Mary
<g/>
;	;	kIx,	;
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
si	se	k3xPyFc3	se
myslel	myslet	k5eAaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
dohoda	dohoda	k1gFnSc1	dohoda
odvrátí	odvrátit	k5eAaPmIp3nS	odvrátit
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
jeho	jeho	k3xOp3gInSc1	jeho
první	první	k4xOgInSc1	první
dopis	dopis	k1gInSc1	dopis
anglickým	anglický	k2eAgMnSc7d1	anglický
příbuzným	příbuzný	k1gMnSc7	příbuzný
od	od	k7c2	od
I.	I.	kA	I.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
tzv.	tzv.	kA	tzv.
Vláda	vláda	k1gFnSc1	vláda
obrany	obrana	k1gFnSc2	obrana
republiky	republika	k1gFnSc2	republika
Jana	Jan	k1gMnSc2	Jan
Syrového	syrový	k2eAgMnSc2d1	syrový
rezignuje	rezignovat	k5eAaBmIp3nS	rezignovat
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1938	[number]	k4	1938
prezident	prezident	k1gMnSc1	prezident
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
rezignuje	rezignovat	k5eAaBmIp3nS	rezignovat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
dohody	dohoda	k1gFnSc2	dohoda
začíná	začínat	k5eAaImIp3nS	začínat
preambulí	preambule	k1gFnSc7	preambule
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
dohodě	dohoda	k1gFnSc3	dohoda
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
ve	v	k7c6	v
věci	věc	k1gFnSc6	věc
odstoupení	odstoupení	k1gNnSc2	odstoupení
území	území	k1gNnSc2	území
Sudet	Sudety	k1gInPc2	Sudety
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
ujednali	ujednat	k5eAaPmAgMnP	ujednat
následující	následující	k2eAgFnPc4d1	následující
podmínky	podmínka	k1gFnPc4	podmínka
uvedeného	uvedený	k2eAgNnSc2d1	uvedené
odstoupení	odstoupení	k1gNnSc2	odstoupení
a	a	k8xC	a
opatření	opatření	k1gNnSc2	opatření
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
vyplývajících	vyplývající	k2eAgNnPc2d1	vyplývající
<g/>
,	,	kIx,	,
a	a	k8xC	a
touto	tento	k3xDgFnSc7	tento
dohodou	dohoda	k1gFnSc7	dohoda
se	se	k3xPyFc4	se
zavazují	zavazovat	k5eAaImIp3nP	zavazovat
k	k	k7c3	k
učinění	učinění	k1gNnSc3	učinění
opatření	opatření	k1gNnPc2	opatření
nezbytných	zbytný	k2eNgNnPc2d1	nezbytné
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
naplnění	naplnění	k1gNnSc4	naplnění
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Následuje	následovat	k5eAaImIp3nS	následovat
osm	osm	k4xCc1	osm
odstavců	odstavec	k1gInPc2	odstavec
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
popisují	popisovat	k5eAaImIp3nP	popisovat
podmínky	podmínka	k1gFnPc4	podmínka
podstoupení	podstoupení	k1gNnSc2	podstoupení
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
odstavec	odstavec	k1gInSc1	odstavec
určují	určovat	k5eAaImIp3nP	určovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyklízení	vyklízení	k1gNnSc1	vyklízení
pohraničí	pohraničí	k1gNnSc2	pohraničí
započne	započnout	k5eAaPmIp3nS	započnout
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
provedeno	provést	k5eAaPmNgNnS	provést
do	do	k7c2	do
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
během	během	k7c2	během
něj	on	k3xPp3gMnSc2	on
nesmí	smět	k5eNaImIp3nS	smět
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
ničení	ničení	k1gNnSc3	ničení
žádných	žádný	k3yNgNnPc2	žádný
existujících	existující	k2eAgNnPc2d1	existující
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgInSc1	třetí
odstavec	odstavec	k1gInSc1	odstavec
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
podmínky	podmínka	k1gFnPc1	podmínka
vyklizení	vyklizení	k1gNnSc2	vyklizení
určí	určit	k5eAaPmIp3nP	určit
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
výbor	výbor	k1gInSc4	výbor
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
své	své	k1gNnSc4	své
zástupce	zástupce	k1gMnSc2	zástupce
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
a	a	k8xC	a
Československo	Československo	k1gNnSc1	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Odstavec	odstavec	k1gInSc1	odstavec
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
pak	pak	k9	pak
definuje	definovat	k5eAaBmIp3nS	definovat
čtyři	čtyři	k4xCgNnPc4	čtyři
pásma	pásmo	k1gNnPc4	pásmo
záboru	zábor	k1gInSc2	zábor
<g/>
,	,	kIx,	,
vyznačené	vyznačený	k2eAgFnPc1d1	vyznačená
na	na	k7c6	na
přiložené	přiložený	k2eAgFnSc6d1	přiložená
mapě	mapa	k1gFnSc6	mapa
<g/>
,	,	kIx,	,
a	a	k8xC	a
data	datum	k1gNnPc1	datum
jejich	jejich	k3xOp3gNnSc4	jejich
obsazení	obsazení	k1gNnSc4	obsazení
německou	německý	k2eAgFnSc7d1	německá
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
odstavci	odstavec	k1gInSc6	odstavec
pátém	pátý	k4xOgInSc6	pátý
a	a	k8xC	a
šestém	šestý	k4xOgInSc6	šestý
je	být	k5eAaImIp3nS	být
popsáno	popsán	k2eAgNnSc1d1	popsáno
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
a	a	k8xC	a
kdy	kdy	k6eAd1	kdy
mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
výbor	výbor	k1gInSc1	výbor
zajistí	zajistit	k5eAaPmIp3nS	zajistit
konání	konání	k1gNnSc4	konání
lidového	lidový	k2eAgNnSc2d1	lidové
hlasování	hlasování	k1gNnSc2	hlasování
na	na	k7c6	na
určeném	určený	k2eAgNnSc6d1	určené
zbývajícím	zbývající	k2eAgNnSc6d1	zbývající
území	území	k1gNnSc6	území
a	a	k8xC	a
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
základě	základ	k1gInSc6	základ
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
o	o	k7c6	o
konečné	konečný	k2eAgFnSc6d1	konečná
podobě	podoba	k1gFnSc6	podoba
pátého	pátý	k4xOgNnSc2	pátý
pásma	pásmo	k1gNnSc2	pásmo
záboru	zábor	k1gInSc2	zábor
a	a	k8xC	a
nových	nový	k2eAgFnPc2d1	nová
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
hlasování	hlasování	k1gNnSc1	hlasování
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
provedeno	provést	k5eAaPmNgNnS	provést
do	do	k7c2	do
konce	konec	k1gInSc2	konec
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
Hitler	Hitler	k1gMnSc1	Hitler
již	již	k9	již
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
učil	učit	k5eAaImAgMnS	učit
rozsah	rozsah	k1gInSc4	rozsah
pátého	pátý	k4xOgInSc2	pátý
pásma	pásmo	k1gNnPc1	pásmo
sám	sám	k3xTgMnSc1	sám
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c6	na
základě	základ	k1gInSc6	základ
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
<g/>
.	.	kIx.	.
</s>
<s>
Zabrané	zabraný	k2eAgNnSc1d1	zabrané
území	území	k1gNnSc1	území
tak	tak	k6eAd1	tak
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
všechny	všechen	k3xTgFnPc4	všechen
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
roce	rok	k1gInSc6	rok
nadpoloviční	nadpoloviční	k2eAgFnSc1d1	nadpoloviční
většina	většina	k1gFnSc1	většina
německého	německý	k2eAgNnSc2d1	německé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Sedmý	sedmý	k4xOgInSc1	sedmý
odstavec	odstavec	k1gInSc1	odstavec
nařizuje	nařizovat	k5eAaImIp3nS	nařizovat
zavedení	zavedení	k1gNnSc4	zavedení
opčního	opční	k2eAgNnSc2d1	opční
práva	právo	k1gNnSc2	právo
pro	pro	k7c4	pro
přesídlení	přesídlení	k1gNnPc4	přesídlení
do	do	k7c2	do
postoupených	postoupený	k2eAgNnPc2d1	postoupené
území	území	k1gNnPc2	území
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
pro	pro	k7c4	pro
vystěhování	vystěhování	k1gNnSc4	vystěhování
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnosti	podrobnost	k1gFnPc1	podrobnost
opce	opce	k1gFnSc2	opce
pak	pak	k6eAd1	pak
měl	mít	k5eAaImAgInS	mít
určit	určit	k5eAaPmF	určit
německo-československý	německo-československý	k2eAgInSc1d1	německo-československý
výbor	výbor	k1gInSc1	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
odstavec	odstavec	k1gInSc1	odstavec
pak	pak	k6eAd1	pak
určuje	určovat	k5eAaImIp3nS	určovat
povinnost	povinnost	k1gFnSc4	povinnost
československé	československý	k2eAgFnSc2d1	Československá
vlády	vláda	k1gFnSc2	vláda
propustit	propustit	k5eAaPmF	propustit
z	z	k7c2	z
vojenských	vojenský	k2eAgFnPc2d1	vojenská
a	a	k8xC	a
politických	politický	k2eAgFnPc2d1	politická
jednotek	jednotka	k1gFnPc2	jednotka
všechny	všechen	k3xTgMnPc4	všechen
Němce	Němec	k1gMnPc4	Němec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc1	ten
budou	být	k5eAaImBp3nP	být
přát	přát	k5eAaImF	přát
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
jí	on	k3xPp3gFnSc3	on
nakazuje	nakazovat	k5eAaImIp3nS	nakazovat
propustit	propustit	k5eAaPmF	propustit
všechny	všechen	k3xTgMnPc4	všechen
sudetoněmecké	sudetoněmecký	k2eAgMnPc4d1	sudetoněmecký
vězně	vězeň	k1gMnPc4	vězeň
zadržené	zadržený	k2eAgMnPc4d1	zadržený
za	za	k7c4	za
politickou	politický	k2eAgFnSc4d1	politická
trestnou	trestný	k2eAgFnSc4d1	trestná
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
je	být	k5eAaImIp3nS	být
datována	datován	k2eAgFnSc1d1	datována
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1938	[number]	k4	1938
(	(	kIx(	(
<g/>
přestože	přestože	k8xS	přestože
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
až	až	k9	až
po	po	k7c6	po
půlnoci	půlnoc	k1gFnSc6	půlnoc
<g/>
)	)	kIx)	)
a	a	k8xC	a
podepsána	podepsat	k5eAaPmNgFnS	podepsat
Adolfem	Adolf	k1gMnSc7	Adolf
Hitlerem	Hitler	k1gMnSc7	Hitler
<g/>
,	,	kIx,	,
Édouardem	Édouard	k1gMnSc7	Édouard
Daladierem	Daladier	k1gMnSc7	Daladier
<g/>
,	,	kIx,	,
Benitem	Benit	k1gMnSc7	Benit
Mussolinim	Mussolini	k1gNnSc7	Mussolini
a	a	k8xC	a
Nevillem	Nevill	k1gMnSc7	Nevill
Chamberlainem	Chamberlain	k1gMnSc7	Chamberlain
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Chamberlain	Chamberlain	k1gMnSc1	Chamberlain
jednal	jednat	k5eAaImAgMnS	jednat
s	s	k7c7	s
Hitlerem	Hitler	k1gMnSc7	Hitler
celkem	celkem	k6eAd1	celkem
9	[number]	k4	9
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
tři	tři	k4xCgInPc4	tři
dodatky	dodatek	k1gInPc4	dodatek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
týkaly	týkat	k5eAaImAgInP	týkat
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
záruk	záruka	k1gFnPc2	záruka
Československu	Československo	k1gNnSc6	Československo
v	v	k7c6	v
nově	nově	k6eAd1	nově
vzniklých	vzniklý	k2eAgFnPc6d1	vzniklá
hranicích	hranice	k1gFnPc6	hranice
<g/>
,	,	kIx,	,
řešení	řešení	k1gNnSc6	řešení
problému	problém	k1gInSc2	problém
polské	polský	k2eAgFnSc2d1	polská
a	a	k8xC	a
maďarské	maďarský	k2eAgFnSc2d1	maďarská
menšiny	menšina	k1gFnSc2	menšina
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
otázek	otázka	k1gFnPc2	otázka
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
dodatků	dodatek	k1gInPc2	dodatek
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
německo-anglické	německonglický	k2eAgNnSc1d1	německo-anglický
prohlášení	prohlášení	k1gNnSc1	prohlášení
o	o	k7c6	o
vzájemném	vzájemný	k2eAgNnSc6d1	vzájemné
neútočení	neútočení	k1gNnSc6	neútočení
a	a	k8xC	a
míru	mír	k1gInSc6	mír
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
se	se	k3xPyFc4	se
doslova	doslova	k6eAd1	doslova
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
My	my	k3xPp1nPc1	my
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
Führer	Führer	k1gMnSc1	Führer
a	a	k8xC	a
kancléř	kancléř	k1gMnSc1	kancléř
a	a	k8xC	a
britský	britský	k2eAgMnSc1d1	britský
ministerský	ministerský	k2eAgMnSc1d1	ministerský
předseda	předseda	k1gMnSc1	předseda
<g/>
,	,	kIx,	,
jsme	být	k5eAaImIp1nP	být
dnes	dnes	k6eAd1	dnes
měli	mít	k5eAaImAgMnP	mít
další	další	k2eAgFnSc4d1	další
schůzku	schůzka	k1gFnSc4	schůzka
a	a	k8xC	a
shodli	shodnout	k5eAaPmAgMnP	shodnout
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
v	v	k7c6	v
poznání	poznání	k1gNnSc6	poznání
<g/>
,	,	kIx,	,
že	že	k8xS	že
otázka	otázka	k1gFnSc1	otázka
anglo-německých	angloěmecký	k2eAgInPc2d1	anglo-německý
vztahů	vztah	k1gInPc2	vztah
má	mít	k5eAaImIp3nS	mít
prvořadou	prvořadý	k2eAgFnSc4d1	prvořadá
důležitost	důležitost	k1gFnSc4	důležitost
pro	pro	k7c4	pro
obě	dva	k4xCgFnPc4	dva
země	zem	k1gFnPc4	zem
a	a	k8xC	a
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Považujeme	považovat	k5eAaImIp1nP	považovat
dohodu	dohoda	k1gFnSc4	dohoda
podepsanou	podepsaný	k2eAgFnSc4d1	podepsaná
včera	včera	k6eAd1	včera
v	v	k7c4	v
noci	noc	k1gFnPc4	noc
a	a	k8xC	a
anglo-německou	angloěmecký	k2eAgFnSc4d1	anglo-německý
dohodu	dohoda	k1gFnSc4	dohoda
o	o	k7c6	o
námořních	námořní	k2eAgFnPc6d1	námořní
silách	síla	k1gFnPc6	síla
za	za	k7c4	za
symboly	symbol	k1gInPc4	symbol
přání	přání	k1gNnPc2	přání
obou	dva	k4xCgNnPc2	dva
našich	náš	k3xOp1gMnPc2	náš
národů	národ	k1gInPc2	národ
nikdy	nikdy	k6eAd1	nikdy
již	již	k6eAd1	již
nejít	jít	k5eNaImF	jít
do	do	k7c2	do
války	válka	k1gFnSc2	válka
jeden	jeden	k4xCgMnSc1	jeden
proti	proti	k7c3	proti
druhému	druhý	k4xOgMnSc3	druhý
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
německá	německý	k2eAgFnSc1d1	německá
armáda	armáda	k1gFnSc1	armáda
připravovala	připravovat	k5eAaImAgFnS	připravovat
na	na	k7c4	na
vojenský	vojenský	k2eAgInSc4d1	vojenský
vpád	vpád	k1gInSc4	vpád
do	do	k7c2	do
Československa	Československo	k1gNnSc2	Československo
(	(	kIx(	(
<g/>
Fall	Fall	k1gMnSc1	Fall
Grün	Grün	k1gMnSc1	Grün
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
mírového	mírový	k2eAgNnSc2d1	Mírové
obsazení	obsazení	k1gNnSc2	obsazení
pohraničí	pohraničí	k1gNnSc2	pohraničí
vyžadovala	vyžadovat	k5eAaImAgFnS	vyžadovat
jiné	jiný	k2eAgNnSc4d1	jiné
uspořádání	uspořádání	k1gNnSc4	uspořádání
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
zábor	zábor	k1gInSc1	zábor
Sudet	Sudety	k1gFnPc2	Sudety
provázely	provázet	k5eAaImAgInP	provázet
rozsáhlé	rozsáhlý	k2eAgInPc1d1	rozsáhlý
manévry	manévr	k1gInPc1	manévr
motorizovaných	motorizovaný	k2eAgFnPc2d1	motorizovaná
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
první	první	k4xOgInPc1	první
oddíly	oddíl	k1gInPc1	oddíl
překročily	překročit	k5eAaPmAgInP	překročit
bývalou	bývalý	k2eAgFnSc4d1	bývalá
hranici	hranice	k1gFnSc4	hranice
hned	hned	k6eAd1	hned
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
ve	v	k7c4	v
14	[number]	k4	14
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
a	a	k8xC	a
československou	československý	k2eAgFnSc7d1	Československá
armádou	armáda	k1gFnSc7	armáda
byl	být	k5eAaImAgInS	být
udržován	udržovat	k5eAaImNgInS	udržovat
odstup	odstup	k1gInSc1	odstup
dvou	dva	k4xCgInPc2	dva
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
předešlo	předejít	k5eAaPmAgNnS	předejít
incidentům	incident	k1gInPc3	incident
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
pak	pak	k6eAd1	pak
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
na	na	k7c4	na
Těšínsko	Těšínsko	k1gNnSc4	Těšínsko
polská	polský	k2eAgNnPc4d1	polské
vojska	vojsko	k1gNnPc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
podnikl	podniknout	k5eAaPmAgMnS	podniknout
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
cestu	cesta	k1gFnSc4	cesta
po	po	k7c6	po
některých	některý	k3yIgNnPc6	některý
nově	nově	k6eAd1	nově
obsazených	obsazený	k2eAgNnPc6d1	obsazené
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
19	[number]	k4	19
hodin	hodina	k1gFnPc2	hodina
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
byla	být	k5eAaImAgNnP	být
obsazena	obsadit	k5eAaPmNgNnP	obsadit
všechna	všechen	k3xTgNnPc1	všechen
území	území	k1gNnPc1	území
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
předpokládala	předpokládat	k5eAaImAgFnS	předpokládat
mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byla	být	k5eAaImAgNnP	být
obsazená	obsazený	k2eAgNnPc1d1	obsazené
území	území	k1gNnPc1	území
pod	pod	k7c7	pod
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
správou	správa	k1gFnSc7	správa
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
byla	být	k5eAaImAgFnS	být
předána	předat	k5eAaPmNgFnS	předat
do	do	k7c2	do
správy	správa	k1gFnSc2	správa
nově	nově	k6eAd1	nově
jmenovanému	jmenovaný	k2eAgMnSc3d1	jmenovaný
říšskému	říšský	k2eAgMnSc3d1	říšský
komisaři	komisař	k1gMnSc3	komisař
pro	pro	k7c4	pro
sudetoněmecké	sudetoněmecký	k2eAgFnPc4d1	Sudetoněmecká
oblasti	oblast	k1gFnPc4	oblast
Konrádu	Konrád	k1gMnSc3	Konrád
Henleinovi	Henlein	k1gMnSc3	Henlein
<g/>
.	.	kIx.	.
</s>
<s>
Československu	Československo	k1gNnSc3	Československo
byl	být	k5eAaImAgInS	být
výsledek	výsledek	k1gInSc1	výsledek
jednání	jednání	k1gNnPc2	jednání
oznámen	oznámit	k5eAaPmNgInS	oznámit
velvyslanci	velvyslanec	k1gMnSc3	velvyslanec
Mastnému	mastný	k2eAgMnSc3d1	mastný
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
tu	ten	k3xDgFnSc4	ten
smutnou	smutný	k2eAgFnSc4d1	smutná
povinnost	povinnost	k1gFnSc4	povinnost
oznámit	oznámit	k5eAaPmF	oznámit
výsledek	výsledek	k1gInSc4	výsledek
jednání	jednání	k1gNnSc2	jednání
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
abdikaci	abdikace	k1gFnSc6	abdikace
prezidenta	prezident	k1gMnSc2	prezident
Edvarda	Edvard	k1gMnSc2	Edvard
Beneše	Beneš	k1gMnSc2	Beneš
5	[number]	k4	5
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1938	[number]	k4	1938
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
novým	nový	k2eAgMnSc7d1	nový
prezidentem	prezident	k1gMnSc7	prezident
30	[number]	k4	30
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
1938	[number]	k4	1938
Emil	Emil	k1gMnSc1	Emil
Hácha	Hácha	k1gMnSc1	Hácha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
této	tento	k3xDgFnSc2	tento
dohody	dohoda	k1gFnSc2	dohoda
přišlo	přijít	k5eAaPmAgNnS	přijít
Československo	Československo	k1gNnSc1	Československo
o	o	k7c4	o
svá	svůj	k3xOyFgNnPc4	svůj
historická	historický	k2eAgNnPc4d1	historické
pohraniční	pohraniční	k2eAgNnPc4d1	pohraniční
území	území	k1gNnPc4	území
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
náležely	náležet	k5eAaImAgInP	náležet
zemím	zem	k1gFnPc3	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc6d1	Česká
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
náhradu	náhrada	k1gFnSc4	náhrada
za	za	k7c4	za
tato	tento	k3xDgNnPc4	tento
ztracená	ztracený	k2eAgNnPc4d1	ztracené
území	území	k1gNnPc4	území
poskytla	poskytnout	k5eAaPmAgFnS	poskytnout
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
Československu	Československo	k1gNnSc6	Československo
tzv.	tzv.	kA	tzv.
mnichovský	mnichovský	k2eAgInSc4d1	mnichovský
úvěr	úvěr	k1gInSc4	úvěr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
pronásledování	pronásledování	k1gNnSc3	pronásledování
německých	německý	k2eAgMnPc2d1	německý
antifašistů	antifašista	k1gMnPc2	antifašista
a	a	k8xC	a
Židů	Žid	k1gMnPc2	Žid
a	a	k8xC	a
k	k	k7c3	k
vyhánění	vyhánění	k1gNnSc3	vyhánění
Čechů	Čech	k1gMnPc2	Čech
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
v	v	k7c6	v
pohraničí	pohraničí	k1gNnSc6	pohraničí
zůstali	zůstat	k5eAaPmAgMnP	zůstat
<g/>
,	,	kIx,	,
odebrali	odebrat	k5eAaPmAgMnP	odebrat
Němci	Němec	k1gMnPc1	Němec
národnostní	národnostní	k2eAgFnSc2d1	národnostní
a	a	k8xC	a
některá	některý	k3yIgNnPc4	některý
občanská	občanský	k2eAgNnPc4d1	občanské
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
dále	daleko	k6eAd2	daleko
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
soustavu	soustava	k1gFnSc4	soustava
pohraničních	pohraniční	k2eAgFnPc2d1	pohraniční
pevností	pevnost	k1gFnPc2	pevnost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
začalo	začít	k5eAaPmAgNnS	začít
budovat	budovat	k5eAaImF	budovat
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
také	také	k9	také
podotknout	podotknout	k5eAaPmF	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
mobilizace	mobilizace	k1gFnSc1	mobilizace
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
předseda	předseda	k1gMnSc1	předseda
úřednické	úřednický	k2eAgFnSc2d1	úřednická
vlády	vláda	k1gFnSc2	vláda
gen.	gen.	kA	gen.
Jan	Jan	k1gMnSc1	Jan
Syrový	syrový	k2eAgMnSc1d1	syrový
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vláda	vláda	k1gFnSc1	vláda
Milana	Milan	k1gMnSc2	Milan
Hodži	Hodža	k1gMnSc2	Hodža
podala	podat	k5eAaPmAgFnS	podat
demisi	demise	k1gFnSc4	demise
<g/>
,	,	kIx,	,
po	po	k7c6	po
přijetí	přijetí	k1gNnSc6	přijetí
anglo-francouzské	anglorancouzský	k2eAgFnSc2d1	anglo-francouzská
nóty	nóta	k1gFnSc2	nóta
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jí	on	k3xPp3gFnSc3	on
nařizovala	nařizovat	k5eAaImAgFnS	nařizovat
odevzdat	odevzdat	k5eAaPmF	odevzdat
Německu	Německo	k1gNnSc6	Německo
pohraniční	pohraniční	k2eAgNnSc4d1	pohraniční
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
%	%	kIx~	%
Němců	Němec	k1gMnPc2	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
půlnocí	půlnoc	k1gFnSc7	půlnoc
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
odevzdala	odevzdat	k5eAaPmAgFnS	odevzdat
polská	polský	k2eAgFnSc1d1	polská
vláda	vláda	k1gFnSc1	vláda
vládě	vláda	k1gFnSc6	vláda
československé	československý	k2eAgNnSc4d1	Československé
ultimátum	ultimátum	k1gNnSc4	ultimátum
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
požadovala	požadovat	k5eAaImAgFnS	požadovat
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
Těšínska	Těšínsko	k1gNnSc2	Těšínsko
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Zaolzie	Zaolzie	k1gFnSc2	Zaolzie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
části	část	k1gFnSc2	část
Oravy	Orava	k1gFnSc2	Orava
<g/>
,	,	kIx,	,
Spiše	Spiš	k1gFnSc2	Spiš
<g/>
,	,	kIx,	,
Kysuc	Kysuc	k1gFnSc4	Kysuc
a	a	k8xC	a
Šariš	Šariš	k1gInSc4	Šariš
<g/>
.	.	kIx.	.
</s>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
vláda	vláda	k1gFnSc1	vláda
s	s	k7c7	s
ultimátem	ultimátum	k1gNnSc7	ultimátum
následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
územním	územní	k2eAgFnPc3d1	územní
ztrátám	ztráta	k1gFnPc3	ztráta
Československa	Československo	k1gNnSc2	Československo
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
listopadu	listopad	k1gInSc2	listopad
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
skončila	skončit	k5eAaPmAgNnP	skončit
arbitrážní	arbitrážní	k2eAgNnPc1d1	arbitrážní
jednání	jednání	k1gNnPc1	jednání
o	o	k7c6	o
československo-maďarské	československoaďarský	k2eAgFnSc6d1	československo-maďarský
hranici	hranice	k1gFnSc6	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
muselo	muset	k5eAaImAgNnS	muset
Československo	Československo	k1gNnSc1	Československo
odstoupit	odstoupit	k5eAaPmF	odstoupit
část	část	k1gFnSc4	část
jižního	jižní	k2eAgNnSc2d1	jižní
a	a	k8xC	a
východního	východní	k2eAgNnSc2d1	východní
Slovenska	Slovensko	k1gNnSc2	Slovensko
jakož	jakož	k8xC	jakož
i	i	k9	i
téměř	téměř	k6eAd1	téměř
polovinu	polovina	k1gFnSc4	polovina
Podkarpatské	podkarpatský	k2eAgFnSc3d1	Podkarpatská
Rusi	Rus	k1gFnSc3	Rus
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgFnPc1d1	celková
ztráty	ztráta	k1gFnPc1	ztráta
pomnichovského	pomnichovský	k2eAgNnSc2d1	pomnichovské
Československa	Československo	k1gNnSc2	Československo
tak	tak	k6eAd1	tak
činily	činit	k5eAaImAgInP	činit
41	[number]	k4	41
098	[number]	k4	098
km	km	kA	km
<g/>
2	[number]	k4	2
a	a	k8xC	a
4	[number]	k4	4
879	[number]	k4	879
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskuse	diskuse	k1gFnSc1	diskuse
o	o	k7c6	o
Mnichovské	mnichovský	k2eAgFnSc6d1	Mnichovská
dohodě	dohoda	k1gFnSc6	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Případné	případný	k2eAgNnSc1d1	případné
odmítnutí	odmítnutí	k1gNnSc1	odmítnutí
dohody	dohoda	k1gFnSc2	dohoda
a	a	k8xC	a
vojenská	vojenský	k2eAgFnSc1d1	vojenská
akce	akce	k1gFnSc1	akce
by	by	kYmCp3nS	by
znamenala	znamenat	k5eAaImAgFnS	znamenat
porušení	porušení	k1gNnSc4	porušení
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
dohod	dohoda	k1gFnPc2	dohoda
a	a	k8xC	a
na	na	k7c4	na
Československo	Československo	k1gNnSc4	Československo
by	by	kYmCp3nP	by
bylo	být	k5eAaImAgNnS	být
nahlíženo	nahlížen	k2eAgNnSc1d1	nahlíženo
jako	jako	k9	jako
na	na	k7c4	na
původce	původce	k1gMnSc4	původce
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
dokázalo	dokázat	k5eAaPmAgNnS	dokázat
během	během	k7c2	během
5	[number]	k4	5
dnů	den	k1gInPc2	den
shromáždit	shromáždit	k5eAaPmF	shromáždit
1	[number]	k4	1
128	[number]	k4	128
000	[number]	k4	000
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
připraveno	připraven	k2eAgNnSc1d1	připraveno
se	se	k3xPyFc4	se
bránit	bránit	k5eAaImF	bránit
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
ale	ale	k8xC	ale
po	po	k7c4	po
připojení	připojení	k1gNnSc4	připojení
Rakouska	Rakousko	k1gNnSc2	Rakousko
k	k	k7c3	k
"	"	kIx"	"
<g/>
Třetí	třetí	k4xOgFnSc4	třetí
Říši	říše	k1gFnSc4	říše
<g/>
"	"	kIx"	"
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1938	[number]	k4	1938
obklopovalo	obklopovat	k5eAaImAgNnS	obklopovat
Československo	Československo	k1gNnSc1	Československo
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
<g/>
,	,	kIx,	,
západu	západ	k1gInSc2	západ
i	i	k8xC	i
jihu	jih	k1gInSc2	jih
<g/>
.	.	kIx.	.
</s>
<s>
Útok	útok	k1gInSc1	útok
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
na	na	k7c4	na
západní	západní	k2eAgFnSc4d1	západní
polovinu	polovina	k1gFnSc4	polovina
státu	stát	k1gInSc2	stát
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
nejspíše	nejspíše	k9	nejspíše
zdrcující	zdrcující	k2eAgInSc1d1	zdrcující
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
byl	být	k5eAaImAgInS	být
také	také	k9	také
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
argumentů	argument	k1gInPc2	argument
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
padly	padnout	k5eAaImAgInP	padnout
při	při	k7c6	při
jednání	jednání	k1gNnSc6	jednání
o	o	k7c6	o
dohodě	dohoda	k1gFnSc6	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
musela	muset	k5eAaImAgFnS	muset
pokusit	pokusit	k5eAaPmF	pokusit
stáhnout	stáhnout	k5eAaPmF	stáhnout
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
veden	veden	k2eAgInSc1d1	veden
odpor	odpor	k1gInSc1	odpor
maximálně	maximálně	k6eAd1	maximálně
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
několika	několik	k4yIc2	několik
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Mobilizačního	mobilizační	k2eAgInSc2d1	mobilizační
rozkazu	rozkaz	k1gInSc2	rozkaz
neuposlechlo	uposlechnout	k5eNaPmAgNnS	uposlechnout
zhruba	zhruba	k6eAd1	zhruba
126	[number]	k4	126
tisíc	tisíc	k4xCgInPc2	tisíc
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
Němců	Němec	k1gMnPc2	Němec
<g/>
.	.	kIx.	.
</s>
<s>
Polovina	polovina	k1gFnSc1	polovina
československých	československý	k2eAgMnPc2d1	československý
Němců	Němec	k1gMnPc2	Němec
tak	tak	k8xS	tak
mobilizačního	mobilizační	k2eAgInSc2d1	mobilizační
rozkazu	rozkaz	k1gInSc2	rozkaz
uposlechla	uposlechnout	k5eAaPmAgFnS	uposlechnout
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Němci	Němec	k1gMnPc1	Němec
sloužili	sloužit	k5eAaImAgMnP	sloužit
<g/>
,	,	kIx,	,
nebyly	být	k5eNaImAgInP	být
většinou	většinou	k6eAd1	většinou
dislokovány	dislokovat	k5eAaBmNgInP	dislokovat
v	v	k7c6	v
kritických	kritický	k2eAgFnPc6d1	kritická
oblastech	oblast	k1gFnPc6	oblast
nebo	nebo	k8xC	nebo
v	v	k7c6	v
první	první	k4xOgFnSc6	první
linii	linie	k1gFnSc6	linie
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
týlové	týlový	k2eAgFnSc2d1	týlová
jednotky	jednotka	k1gFnSc2	jednotka
a	a	k8xC	a
zásobování	zásobování	k1gNnSc2	zásobování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Překvapivě	překvapivě	k6eAd1	překvapivě
loajální	loajální	k2eAgMnPc1d1	loajální
se	se	k3xPyFc4	se
ukázali	ukázat	k5eAaPmAgMnP	ukázat
maďarští	maďarský	k2eAgMnPc1d1	maďarský
a	a	k8xC	a
rusínští	rusínský	k2eAgMnPc1d1	rusínský
odvedenci	odvedenec	k1gMnPc1	odvedenec
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
očekávanou	očekávaný	k2eAgFnSc4d1	očekávaná
odvahu	odvaha	k1gFnSc4	odvaha
a	a	k8xC	a
bojovou	bojový	k2eAgFnSc4d1	bojová
rozhodnost	rozhodnost	k1gFnSc4	rozhodnost
Československé	československý	k2eAgFnSc2d1	Československá
armády	armáda	k1gFnSc2	armáda
nemohla	moct	k5eNaImAgNnP	moct
vojska	vojsko	k1gNnPc1	vojsko
společného	společný	k2eAgInSc2d1	společný
státu	stát	k1gInSc2	stát
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
odolávat	odolávat	k5eAaImF	odolávat
mnohem	mnohem	k6eAd1	mnohem
silnějšímu	silný	k2eAgMnSc3d2	silnější
nepříteli	nepřítel	k1gMnSc3	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Nemohla	moct	k5eNaImAgFnS	moct
okamžitě	okamžitě	k6eAd1	okamžitě
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
žádnou	žádný	k3yNgFnSc7	žádný
pomocí	pomoc	k1gFnSc7	pomoc
od	od	k7c2	od
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
musela	muset	k5eAaImAgFnS	muset
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
a	a	k8xC	a
Polsko	Polsko	k1gNnSc1	Polsko
využijí	využít	k5eAaPmIp3nP	využít
jejího	její	k3xOp3gNnSc2	její
oslabení	oslabení	k1gNnSc2	oslabení
u	u	k7c2	u
svých	svůj	k3xOyFgFnPc2	svůj
hranic	hranice	k1gFnPc2	hranice
a	a	k8xC	a
pokusí	pokusit	k5eAaPmIp3nS	pokusit
se	se	k3xPyFc4	se
přiživit	přiživit	k5eAaPmF	přiživit
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
kořisti	kořist	k1gFnSc6	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Diskuse	diskuse	k1gFnSc1	diskuse
o	o	k7c6	o
možnosti	možnost	k1gFnSc6	možnost
obrany	obrana	k1gFnSc2	obrana
budou	být	k5eAaImBp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
stále	stále	k6eAd1	stále
přetrvávat	přetrvávat	k5eAaImF	přetrvávat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
Československo	Československo	k1gNnSc1	Československo
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
své	svůj	k3xOyFgNnSc4	svůj
pohraniční	pohraniční	k2eAgNnSc4d1	pohraniční
opevnění	opevnění	k1gNnSc4	opevnění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
podle	podle	k7c2	podle
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
muselo	muset	k5eAaImAgNnS	muset
vojsko	vojsko	k1gNnSc1	vojsko
do	do	k7c2	do
10	[number]	k4	10
dnů	den	k1gInPc2	den
opustit	opustit	k5eAaPmF	opustit
bez	bez	k7c2	bez
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
pevnosti	pevnost	k1gFnSc3	pevnost
poškodilo	poškodit	k5eAaPmAgNnS	poškodit
<g/>
,	,	kIx,	,
nebylo	být	k5eNaImAgNnS	být
pro	pro	k7c4	pro
německou	německý	k2eAgFnSc4d1	německá
armádu	armáda	k1gFnSc4	armáda
těžké	těžký	k2eAgInPc1d1	těžký
zbytek	zbytek	k1gInSc4	zbytek
státu	stát	k1gInSc2	stát
obsadit	obsadit	k5eAaPmF	obsadit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
došlo	dojít	k5eAaPmAgNnS	dojít
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
Protektorát	protektorát	k1gInSc1	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
(	(	kIx(	(
<g/>
Protektorat	Protektorat	k1gInSc1	Protektorat
Böhmen	Böhmen	k2eAgInSc1d1	Böhmen
und	und	k?	und
Mähren	Mährno	k1gNnPc2	Mährno
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
o	o	k7c4	o
den	den	k1gInSc4	den
dříve	dříve	k6eAd2	dříve
Slovenská	slovenský	k2eAgFnSc1d1	slovenská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Odstoupením	odstoupení	k1gNnSc7	odstoupení
pohraničních	pohraniční	k2eAgNnPc2d1	pohraniční
území	území	k1gNnPc2	území
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
byla	být	k5eAaImAgFnS	být
pozice	pozice	k1gFnSc1	pozice
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
značně	značně	k6eAd1	značně
oslabena	oslaben	k2eAgFnSc1d1	oslabena
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
ztratila	ztratit	k5eAaPmAgFnS	ztratit
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
část	část	k1gFnSc4	část
svého	svůj	k3xOyFgNnSc2	svůj
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
přetrhání	přetrhání	k1gNnSc1	přetrhání
dlouholetých	dlouholetý	k2eAgFnPc2d1	dlouholetá
vazeb	vazba	k1gFnPc2	vazba
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
ekonomice	ekonomika	k1gFnSc6	ekonomika
a	a	k8xC	a
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
přeorganizována	přeorganizován	k2eAgFnSc1d1	přeorganizována
železniční	železniční	k2eAgFnSc1d1	železniční
síť	síť	k1gFnSc1	síť
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
některé	některý	k3yIgFnPc1	některý
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
hlavní	hlavní	k2eAgFnSc2d1	hlavní
železniční	železniční	k2eAgFnSc2d1	železniční
trati	trať	k1gFnSc2	trať
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
Českou	český	k2eAgFnSc4d1	Česká
Třebovou	Třebová	k1gFnSc4	Třebová
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc4d1	český
Těšín	Těšín	k1gInSc4	Těšín
<g/>
,	,	kIx,	,
či	či	k8xC	či
Košice	Košice	k1gInPc1	Košice
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
staly	stát	k5eAaPmAgFnP	stát
peážními	peážní	k2eAgInPc7d1	peážní
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vedenými	vedený	k2eAgInPc7d1	vedený
přes	přes	k7c4	přes
cizí	cizí	k2eAgNnSc4d1	cizí
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
vypracovány	vypracován	k2eAgInPc1d1	vypracován
projekty	projekt	k1gInPc1	projekt
nových	nový	k2eAgFnPc2d1	nová
tratí	trať	k1gFnPc2	trať
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
byl	být	k5eAaImAgInS	být
upraven	upravit	k5eAaPmNgInS	upravit
i	i	k9	i
projekt	projekt	k1gInSc4	projekt
první	první	k4xOgFnSc2	první
československé	československý	k2eAgFnSc2d1	Československá
dálnice	dálnice	k1gFnSc2	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
Páteřní	páteřní	k2eAgInPc1d1	páteřní
spoje	spoj	k1gInPc1	spoj
nyní	nyní	k6eAd1	nyní
procházely	procházet	k5eAaImAgInP	procházet
přes	přes	k7c4	přes
Havlíčkův	Havlíčkův	k2eAgInSc4d1	Havlíčkův
Brod	Brod	k1gInSc4	Brod
<g/>
,	,	kIx,	,
Vsetín	Vsetín	k1gInSc4	Vsetín
<g/>
,	,	kIx,	,
či	či	k8xC	či
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
s	s	k7c7	s
Podkarpatskou	podkarpatský	k2eAgFnSc7d1	Podkarpatská
Rusí	Rus	k1gFnSc7	Rus
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nyní	nyní	k6eAd1	nyní
na	na	k7c6	na
území	území	k1gNnSc6	území
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
jej	on	k3xPp3gNnSc4	on
nutné	nutný	k2eAgNnSc1d1	nutné
opět	opět	k6eAd1	opět
vystavět	vystavět	k5eAaPmF	vystavět
<g/>
.	.	kIx.	.
</s>
<s>
Oblasti	oblast	k1gFnPc1	oblast
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
získalo	získat	k5eAaPmAgNnS	získat
hlavně	hlavně	k9	hlavně
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
průmyslově	průmyslově	k6eAd1	průmyslově
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
rozvinuté	rozvinutý	k2eAgNnSc4d1	rozvinuté
a	a	k8xC	a
provázané	provázaný	k2eAgNnSc4d1	provázané
se	s	k7c7	s
zbytkem	zbytek	k1gInSc7	zbytek
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
souvislé	souvislý	k2eAgNnSc4d1	souvislé
území	území	k1gNnSc4	území
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
již	již	k6eAd1	již
od	od	k7c2	od
začátků	začátek	k1gInPc2	začátek
zprůmyslnění	zprůmyslnění	k1gNnSc2	zprůmyslnění
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
právě	právě	k6eAd1	právě
tyto	tento	k3xDgFnPc4	tento
oblasti	oblast	k1gFnPc4	oblast
ale	ale	k8xC	ale
silněji	silně	k6eAd2	silně
než	než	k8xS	než
české	český	k2eAgNnSc4d1	české
vnitrozemí	vnitrozemí	k1gNnSc4	vnitrozemí
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
již	již	k6eAd1	již
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
30	[number]	k4	30
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Sudety	Sudety	k1gFnPc1	Sudety
ekonomicky	ekonomicky	k6eAd1	ekonomicky
orientovaly	orientovat	k5eAaBmAgFnP	orientovat
na	na	k7c4	na
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
poměrně	poměrně	k6eAd1	poměrně
blízko	blízko	k6eAd1	blízko
<g/>
.	.	kIx.	.
</s>
<s>
Ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
potenciál	potenciál	k1gInSc1	potenciál
těchto	tento	k3xDgFnPc2	tento
oblastí	oblast	k1gFnPc2	oblast
mohla	moct	k5eAaImAgFnS	moct
Třetí	třetí	k4xOgFnSc1	třetí
říše	říše	k1gFnSc1	říše
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
využít	využít	k5eAaPmF	využít
<g/>
.	.	kIx.	.
</s>
<s>
Hůře	zle	k6eAd2	zle
na	na	k7c6	na
tom	ten	k3xDgMnSc6	ten
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
získalo	získat	k5eAaPmAgNnS	získat
hlavně	hlavně	k9	hlavně
území	území	k1gNnSc4	území
zemědělského	zemědělský	k2eAgInSc2d1	zemědělský
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
<s>
Neplatnost	neplatnost	k1gFnSc1	neplatnost
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
není	být	k5eNaImIp3nS	být
vykládána	vykládat	k5eAaImNgFnS	vykládat
jednoznačně	jednoznačně	k6eAd1	jednoznačně
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
nejednoznačnost	nejednoznačnost	k1gFnSc4	nejednoznačnost
výkladu	výklad	k1gInSc2	výklad
jsou	být	k5eAaImIp3nP	být
rozdílné	rozdílný	k2eAgInPc1d1	rozdílný
pohledy	pohled	k1gInPc1	pohled
znalců	znalec	k1gMnPc2	znalec
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
donucení	donucení	k1gNnSc4	donucení
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
protiprávní	protiprávní	k2eAgNnSc1d1	protiprávní
<g/>
)	)	kIx)	)
a	a	k8xC	a
právní	právní	k2eAgFnSc4d1	právní
kontinuitu	kontinuita	k1gFnSc4	kontinuita
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
K	k	k7c3	k
neplatnosti	neplatnost	k1gFnSc3	neplatnost
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
by	by	kYmCp3nS	by
nejlépe	dobře	k6eAd3	dobře
posloužilo	posloužit	k5eAaPmAgNnS	posloužit
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
vynucena	vynutit	k5eAaPmNgFnS	vynutit
pod	pod	k7c7	pod
nátlakem	nátlak	k1gInSc7	nátlak
a	a	k8xC	a
pod	pod	k7c7	pod
hrozbou	hrozba	k1gFnSc7	hrozba
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byl	být	k5eAaImAgMnS	být
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Německa	Německo	k1gNnSc2	Německo
porušen	porušen	k2eAgInSc1d1	porušen
Briand-Kelloggův	Briand-Kelloggův	k2eAgInSc1d1	Briand-Kelloggův
pakt	pakt	k1gInSc1	pakt
<g/>
.	.	kIx.	.
</s>
<s>
Československo	Československo	k1gNnSc1	Československo
bylo	být	k5eAaImAgNnS	být
tak	tak	k9	tak
postaveno	postavit	k5eAaPmNgNnS	postavit
do	do	k7c2	do
nevýhodné	výhodný	k2eNgFnSc2d1	nevýhodná
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
buďto	buďto	k8xC	buďto
se	se	k3xPyFc4	se
podvolit	podvolit	k5eAaPmF	podvolit
Mnichovské	mnichovský	k2eAgFnSc3d1	Mnichovská
dohodě	dohoda	k1gFnSc3	dohoda
nebo	nebo	k8xC	nebo
riskovat	riskovat	k5eAaBmF	riskovat
válku	válka	k1gFnSc4	válka
v	v	k7c6	v
nevýhodné	výhodný	k2eNgFnSc6d1	nevýhodná
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
situaci	situace	k1gFnSc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
Norimberský	norimberský	k2eAgInSc1d1	norimberský
soud	soud	k1gInSc1	soud
v	v	k7c6	v
1946	[number]	k4	1946
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
Mnichovskou	mnichovský	k2eAgFnSc4d1	Mnichovská
dohodu	dohoda	k1gFnSc4	dohoda
za	za	k7c4	za
neplatnou	platný	k2eNgFnSc4d1	neplatná
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
trpí	trpět	k5eAaImIp3nS	trpět
nedostatkem	nedostatek	k1gInSc7	nedostatek
vůle	vůle	k1gFnSc2	vůle
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Německo	Německo	k1gNnSc1	Německo
v	v	k7c6	v
době	doba	k1gFnSc6	doba
jejího	její	k3xOp3gNnSc2	její
uzavření	uzavření	k1gNnSc2	uzavření
nemělo	mít	k5eNaImAgNnS	mít
v	v	k7c6	v
úmyslu	úmysl	k1gInSc6	úmysl
ji	on	k3xPp3gFnSc4	on
dodržet	dodržet	k5eAaPmF	dodržet
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vyhrožování	vyhrožování	k1gNnSc3	vyhrožování
válkou	válka	k1gFnSc7	válka
lze	lze	k6eAd1	lze
poukázat	poukázat	k5eAaPmF	poukázat
na	na	k7c4	na
německý	německý	k2eAgInSc4d1	německý
vojenský	vojenský	k2eAgInSc4d1	vojenský
plán	plán	k1gInSc4	plán
Fall	Fall	k1gMnSc1	Fall
Grün	Grün	k1gMnSc1	Grün
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
argumentem	argument	k1gInSc7	argument
pro	pro	k7c4	pro
neplatnost	neplatnost	k1gFnSc4	neplatnost
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gNnSc4	její
vlastní	vlastní	k2eAgNnSc4d1	vlastní
porušení	porušení	k1gNnSc4	porušení
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
nahlíženo	nahlížen	k2eAgNnSc1d1	nahlíženo
na	na	k7c4	na
Mnichovskou	mnichovský	k2eAgFnSc4d1	Mnichovská
dohodu	dohoda	k1gFnSc4	dohoda
jako	jako	k8xC	jako
na	na	k7c4	na
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
smlouvu	smlouva	k1gFnSc4	smlouva
signovanou	signovaný	k2eAgFnSc7d1	signovaná
vládou	vláda	k1gFnSc7	vláda
ČSR	ČSR	kA	ČSR
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
nebyla	být	k5eNaImAgFnS	být
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
československého	československý	k2eAgNnSc2d1	Československé
ústavního	ústavní	k2eAgNnSc2d1	ústavní
práva	právo	k1gNnSc2	právo
představuje	představovat	k5eAaImIp3nS	představovat
postup	postup	k1gInSc1	postup
přijetí	přijetí	k1gNnSc2	přijetí
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
porušení	porušení	k1gNnSc2	porušení
§	§	k?	§
3	[number]	k4	3
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
1	[number]	k4	1
a	a	k8xC	a
§	§	k?	§
64	[number]	k4	64
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
1	[number]	k4	1
bodu	bod	k1gInSc2	bod
1	[number]	k4	1
ústavní	ústavní	k2eAgFnSc2d1	ústavní
listiny	listina	k1gFnSc2	listina
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yRgInPc2	který
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
měnit	měnit	k5eAaImF	měnit
státní	státní	k2eAgNnSc4d1	státní
území	území	k1gNnSc4	území
republiky	republika	k1gFnSc2	republika
pouze	pouze	k6eAd1	pouze
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
Národního	národní	k2eAgNnSc2d1	národní
shromáždění	shromáždění	k1gNnSc2	shromáždění
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ústavním	ústavní	k2eAgInSc7d1	ústavní
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
a	a	k8xC	a
vlády	vláda	k1gFnPc1	vláda
tedy	tedy	k9	tedy
nedisponovaly	disponovat	k5eNaBmAgFnP	disponovat
pravomocemi	pravomoc	k1gFnPc7	pravomoc
k	k	k7c3	k
přijetí	přijetí	k1gNnSc3	přijetí
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
a	a	k8xC	a
vlády	vláda	k1gFnSc2	vláda
Mnichovskou	mnichovský	k2eAgFnSc4d1	Mnichovská
dohodu	dohoda	k1gFnSc4	dohoda
nepředložili	předložit	k5eNaPmAgMnP	předložit
Národnímu	národní	k2eAgNnSc3d1	národní
shromáždění	shromáždění	k1gNnSc3	shromáždění
ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nebyla	být	k5eNaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
a	a	k8xC	a
rezignovali	rezignovat	k5eAaBmAgMnP	rezignovat
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vykládat	vykládat	k5eAaImF	vykládat
jinak	jinak	k6eAd1	jinak
z	z	k7c2	z
českého	český	k2eAgNnSc2d1	české
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
československého	československý	k2eAgInSc2d1	československý
<g/>
)	)	kIx)	)
či	či	k8xC	či
britského	britský	k2eAgMnSc2d1	britský
nebo	nebo	k8xC	nebo
německého	německý	k2eAgInSc2d1	německý
pohledu	pohled	k1gInSc2	pohled
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
česká	český	k2eAgFnSc1d1	Česká
strana	strana	k1gFnSc1	strana
soudí	soudit	k5eAaImIp3nS	soudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dohoda	dohoda	k1gFnSc1	dohoda
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
začátku	začátek	k1gInSc2	začátek
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
"	"	kIx"	"
<g/>
ab	ab	k?	ab
initio	initio	k1gMnSc1	initio
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
neplatná	platný	k2eNgFnSc1d1	neplatná
<g/>
,	,	kIx,	,
německá	německý	k2eAgFnSc1d1	německá
strana	strana	k1gFnSc1	strana
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
neopouštěl	opouštět	k5eNaImAgMnS	opouštět
Československo	Československo	k1gNnSc4	Československo
jako	jako	k8xS	jako
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jako	jako	k9	jako
soukromá	soukromý	k2eAgFnSc1d1	soukromá
osoba	osoba	k1gFnSc1	osoba
a	a	k8xC	a
jistá	jistý	k2eAgFnSc1d1	jistá
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
právní	právní	k2eAgFnSc1d1	právní
platnost	platnost	k1gFnSc1	platnost
československé	československý	k2eAgFnSc2d1	Československá
vlády	vláda	k1gFnSc2	vláda
existovala	existovat	k5eAaImAgFnS	existovat
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
ve	v	k7c6	v
válečném	válečný	k2eAgInSc6d1	válečný
stavu	stav	k1gInSc6	stav
se	se	k3xPyFc4	se
Československo	Československo	k1gNnSc1	Československo
či	či	k8xC	či
později	pozdě	k6eAd2	pozdě
Česko-Slovensko	Česko-Slovensko	k1gNnSc1	Česko-Slovensko
nemohlo	moct	k5eNaImAgNnS	moct
nalézat	nalézat	k5eAaImF	nalézat
dřív	dříve	k6eAd2	dříve
než	než	k8xS	než
po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
národů	národ	k1gInPc2	národ
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
právním	právní	k2eAgInSc7d1	právní
stavem	stav	k1gInSc7	stav
okupací	okupace	k1gFnPc2	okupace
okleštěné	okleštěný	k2eAgFnSc2d1	okleštěná
ČSR	ČSR	kA	ČSR
a	a	k8xC	a
následným	následný	k2eAgInSc7d1	následný
vznikem	vznik	k1gInSc7	vznik
protektorátu	protektorát	k1gInSc2	protektorát
zabývat	zabývat	k5eAaImF	zabývat
v	v	k7c6	v
září	září	k1gNnSc6	září
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
jednání	jednání	k1gNnSc3	jednání
již	již	k6eAd1	již
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
událostem	událost	k1gFnPc3	událost
(	(	kIx(	(
<g/>
útok	útok	k1gInSc4	útok
na	na	k7c4	na
Polsko	Polsko	k1gNnSc4	Polsko
<g/>
)	)	kIx)	)
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Úporné	úporný	k2eAgFnPc1d1	úporná
snahy	snaha	k1gFnPc1	snaha
československé	československý	k2eAgFnSc2d1	Československá
exilové	exilový	k2eAgFnSc2d1	exilová
vlády	vláda	k1gFnSc2	vláda
o	o	k7c4	o
"	"	kIx"	"
<g/>
oduznání	oduznání	k1gNnSc4	oduznání
<g/>
"	"	kIx"	"
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
úzce	úzko	k6eAd1	úzko
souvisela	souviset	k5eAaImAgFnS	souviset
s	s	k7c7	s
vytvořením	vytvoření	k1gNnSc7	vytvoření
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
,	,	kIx,	,
že	že	k8xS	že
Československá	československý	k2eAgFnSc1d1	Československá
republiky	republika	k1gFnSc2	republika
byla	být	k5eAaImAgNnP	být
ve	v	k7c6	v
válečném	válečný	k2eAgInSc6d1	válečný
stavu	stav	k1gInSc6	stav
s	s	k7c7	s
Německem	Německo	k1gNnSc7	Německo
již	již	k6eAd1	již
od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
před	před	k7c7	před
vlastním	vlastní	k2eAgNnSc7d1	vlastní
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
útokem	útok	k1gInSc7	útok
na	na	k7c4	na
Polsko	Polsko	k1gNnSc4	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Úplné	úplný	k2eAgNnSc1d1	úplné
zrušení	zrušení	k1gNnSc1	zrušení
dohody	dohoda	k1gFnSc2	dohoda
nebylo	být	k5eNaImAgNnS	být
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
možné	možný	k2eAgFnSc6d1	možná
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
neexistence	neexistence	k1gFnSc2	neexistence
retroaktivity	retroaktivita	k1gFnSc2	retroaktivita
v	v	k7c6	v
angloamerickém	angloamerický	k2eAgNnSc6d1	angloamerické
právu	právo	k1gNnSc6	právo
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
platnou	platný	k2eAgFnSc4d1	platná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
zneplatněna	zneplatnit	k5eAaPmNgFnS	zneplatnit
postojem	postoj	k1gInSc7	postoj
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Její	její	k3xOp3gInSc1	její
"	"	kIx"	"
<g/>
zneplatnění	zneplatnění	k1gNnSc1	zneplatnění
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
opírá	opírat	k5eAaImIp3nS	opírat
především	především	k9	především
o	o	k7c4	o
stanovisko	stanovisko	k1gNnSc4	stanovisko
ve	v	k7c6	v
výroku	výrok	k1gInSc6	výrok
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
precedens	precedens	k1gNnSc4	precedens
<g/>
)	)	kIx)	)
premiéra	premiér	k1gMnSc4	premiér
W.	W.	kA	W.
Churchilla	Churchill	k1gMnSc4	Churchill
ze	z	k7c2	z
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1940	[number]	k4	1940
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
rozhlase	rozhlas	k1gInSc6	rozhlas
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
svůj	svůj	k3xOyFgInSc4	svůj
úsudek	úsudek	k1gInSc4	úsudek
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
je	být	k5eAaImIp3nS	být
mrtvá	mrtvý	k2eAgFnSc1d1	mrtvá
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
muži	muž	k1gMnPc7	muž
bez	bez	k7c2	bez
skrupulí	skrupule	k1gFnPc2	skrupule
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
kontrolují	kontrolovat	k5eAaImIp3nP	kontrolovat
osud	osud	k1gInSc4	osud
Německa	Německo	k1gNnSc2	Německo
<g/>
"	"	kIx"	"
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1940	[number]	k4	1940
dokonce	dokonce	k9	dokonce
britské	britský	k2eAgNnSc1d1	Britské
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sice	sice	k8xC	sice
není	být	k5eNaImIp3nS	být
vázáno	vázat	k5eAaImNgNnS	vázat
smlouvou	smlouva	k1gFnSc7	smlouva
z	z	k7c2	z
Mnichova	Mnichov	k1gInSc2	Mnichov
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
československých	československý	k2eAgFnPc2d1	Československá
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
nechce	chtít	k5eNaImIp3nS	chtít
hledat	hledat	k5eAaImF	hledat
jiné	jiný	k2eAgNnSc4d1	jiné
řešení	řešení	k1gNnSc4	řešení
než	než	k8xS	než
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
mnichovské	mnichovský	k2eAgFnSc6d1	Mnichovská
konferenci	konference	k1gFnSc6	konference
nalezeno	nalézt	k5eAaBmNgNnS	nalézt
<g/>
,	,	kIx,	,
k	k	k7c3	k
porušení	porušení	k1gNnSc3	porušení
této	tento	k3xDgFnSc2	tento
dohody	dohoda	k1gFnSc2	dohoda
a	a	k8xC	a
k	k	k7c3	k
jejímu	její	k3xOp3gMnSc3	její
zneplatnění	zneplatnění	k1gNnSc1	zneplatnění
došlo	dojít	k5eAaPmAgNnS	dojít
samovolně	samovolně	k6eAd1	samovolně
Německem	Německo	k1gNnSc7	Německo
dnem	dnem	k7c2	dnem
zničení	zničení	k1gNnSc2	zničení
či	či	k8xC	či
okupací	okupace	k1gFnPc2	okupace
okleštěné	okleštěný	k2eAgFnSc2d1	okleštěná
Československé	československý	k2eAgFnSc2d1	Československá
republiky	republika	k1gFnSc2	republika
tedy	tedy	k9	tedy
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výsledek	výsledek	k1gInSc1	výsledek
dohody	dohoda	k1gFnSc2	dohoda
akceptuje	akceptovat	k5eAaBmIp3nS	akceptovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
změnilo	změnit	k5eAaPmAgNnS	změnit
svůj	svůj	k3xOyFgInSc4	svůj
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
Mnichovské	mnichovský	k2eAgFnSc3d1	Mnichovská
dohodě	dohoda	k1gFnSc3	dohoda
až	až	k9	až
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
atentátu	atentát	k1gInSc2	atentát
na	na	k7c4	na
říšského	říšský	k2eAgMnSc4d1	říšský
protektora	protektor	k1gMnSc4	protektor
Reinharda	Reinhard	k1gMnSc4	Reinhard
Heydricha	Heydrich	k1gMnSc4	Heydrich
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
prohlásilo	prohlásit	k5eAaPmAgNnS	prohlásit
dohodu	dohoda	k1gFnSc4	dohoda
za	za	k7c4	za
neplatnou	platný	k2eNgFnSc4d1	neplatná
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1942	[number]	k4	1942
s	s	k7c7	s
odůvodněním	odůvodnění	k1gNnSc7	odůvodnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
samo	sám	k3xTgNnSc1	sám
Německo	Německo	k1gNnSc1	Německo
porušilo	porušit	k5eAaPmAgNnS	porušit
už	už	k6eAd1	už
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
již	již	k9	již
smlouvou	smlouva	k1gFnSc7	smlouva
též	též	k9	též
není	být	k5eNaImIp3nS	být
vázané	vázané	k1gNnSc1	vázané
<g/>
.	.	kIx.	.
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1990	[number]	k4	1990
britská	britský	k2eAgFnSc1d1	britská
ministerská	ministerský	k2eAgFnSc1d1	ministerská
předsedkyně	předsedkyně	k1gFnSc1	předsedkyně
Margaret	Margareta	k1gFnPc2	Margareta
Thatcherová	Thatcherová	k1gFnSc1	Thatcherová
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc1	její
země	země	k1gFnSc1	země
zklamala	zklamat	k5eAaPmAgFnS	zklamat
Československo	Československo	k1gNnSc4	Československo
svou	svůj	k3xOyFgFnSc7	svůj
účastí	účast	k1gFnSc7	účast
při	při	k7c6	při
podpisu	podpis	k1gInSc6	podpis
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
<g/>
,	,	kIx,	,
a	a	k8xC	a
dodala	dodat	k5eAaPmAgFnS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
vzpomíná	vzpomínat	k5eAaImIp3nS	vzpomínat
s	s	k7c7	s
pocitem	pocit	k1gInSc7	pocit
studu	stud	k1gInSc2	stud
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
podpis	podpis	k1gInSc1	podpis
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
odvolala	odvolat	k5eAaPmAgFnS	odvolat
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1942	[number]	k4	1942
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
podpisem	podpis	k1gInSc7	podpis
generála	generál	k1gMnSc2	generál
Charlese	Charles	k1gMnSc2	Charles
de	de	k?	de
Gaullea	Gaulleus	k1gMnSc2	Gaulleus
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
odvolání	odvolání	k1gNnSc1	odvolání
je	být	k5eAaImIp3nS	být
také	také	k9	také
podepsáno	podepsat	k5eAaPmNgNnS	podepsat
zahraničním	zahraniční	k2eAgMnPc3d1	zahraniční
komisařem	komisař	k1gMnSc7	komisař
Maurice	Maurika	k1gFnSc3	Maurika
Dejeanem	Dejean	k1gMnSc7	Dejean
a	a	k8xC	a
formuluje	formulovat	k5eAaImIp3nS	formulovat
neplatnost	neplatnost	k1gFnSc4	neplatnost
od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
počátku	počátek	k1gInSc2	počátek
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
formulace	formulace	k1gFnSc1	formulace
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
jakou	jaký	k3yQgFnSc4	jaký
požadovala	požadovat	k5eAaImAgFnS	požadovat
československá	československý	k2eAgFnSc1d1	Československá
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
československo-francouzské	československorancouzský	k2eAgFnSc2d1	československo-francouzská
deklarace	deklarace	k1gFnSc2	deklarace
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
formulace	formulace	k1gFnSc1	formulace
o	o	k7c6	o
neplatnosti	neplatnost	k1gFnSc6	neplatnost
od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
počátku	počátek	k1gInSc2	počátek
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
nuls	nuls	k6eAd1	nuls
et	et	k?	et
non-avenus	nonvenus	k1gInSc1	non-avenus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Francouzské	francouzský	k2eAgNnSc1d1	francouzské
stanovisko	stanovisko	k1gNnSc1	stanovisko
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
protesty	protest	k1gInPc4	protest
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
po	po	k7c6	po
Mnichovu	Mnichov	k1gInSc6	Mnichov
okupovalo	okupovat	k5eAaBmAgNnS	okupovat
Těšínsko	Těšínsko	k1gNnSc1	Těšínsko
<g/>
.	.	kIx.	.
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1942	[number]	k4	1942
prohlásil	prohlásit	k5eAaPmAgInS	prohlásit
Francouzský	francouzský	k2eAgInSc1d1	francouzský
národní	národní	k2eAgInSc4d1	národní
výbor	výbor	k1gInSc4	výbor
dohodu	dohoda	k1gFnSc4	dohoda
za	za	k7c4	za
zcela	zcela	k6eAd1	zcela
neplatnou	platný	k2eNgFnSc4d1	neplatná
(	(	kIx(	(
<g/>
nulitní	nulitní	k2eAgFnSc4d1	nulitní
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Francouzský	francouzský	k2eAgInSc1d1	francouzský
národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
...	...	k?	...
zavrhuje	zavrhovat	k5eAaImIp3nS	zavrhovat
dohody	dohoda	k1gFnPc4	dohoda
podepsané	podepsaný	k2eAgFnPc4d1	podepsaná
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
</s>
<s>
Francouzský	francouzský	k2eAgInSc1d1	francouzský
národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
slavnostně	slavnostně	k6eAd1	slavnostně
vyhlašuje	vyhlašovat	k5eAaImIp3nS	vyhlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokládá	pokládat	k5eAaImIp3nS	pokládat
tyto	tento	k3xDgFnPc4	tento
dohody	dohoda	k1gFnPc4	dohoda
hned	hned	k6eAd1	hned
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
za	za	k7c4	za
neplatné	platný	k2eNgNnSc4d1	neplatné
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
veškeré	veškerý	k3xTgInPc4	veškerý
další	další	k2eAgInPc4d1	další
akty	akt	k1gInPc4	akt
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yRgFnPc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
při	při	k7c6	při
provádění	provádění	k1gNnSc6	provádění
nebo	nebo	k8xC	nebo
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
těchto	tento	k3xDgFnPc2	tento
dohod	dohoda	k1gFnPc2	dohoda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podle	podle	k7c2	podle
Jana	Jan	k1gMnSc2	Jan
Němečka	Němeček	k1gMnSc2	Němeček
Francie	Francie	k1gFnSc1	Francie
(	(	kIx(	(
<g/>
exilová	exilový	k2eAgFnSc1d1	exilová
vláda	vláda	k1gFnSc1	vláda
<g/>
)	)	kIx)	)
neuznala	uznat	k5eNaPmAgFnS	uznat
žádné	žádný	k3yNgFnPc4	žádný
územní	územní	k2eAgFnPc4d1	územní
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yQgFnPc3	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
,	,	kIx,	,
a	a	k8xC	a
zavazovala	zavazovat	k5eAaImAgFnS	zavazovat
se	se	k3xPyFc4	se
podporovat	podporovat	k5eAaImF	podporovat
obnovu	obnova	k1gFnSc4	obnova
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
předmnichovských	předmnichovský	k2eAgFnPc6d1	předmnichovská
hranicích	hranice	k1gFnPc6	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
stanovisko	stanovisko	k1gNnSc1	stanovisko
potvrdilo	potvrdit	k5eAaPmAgNnS	potvrdit
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
francouzské	francouzský	k2eAgFnSc2d1	francouzská
prozatímní	prozatímní	k2eAgFnSc2d1	prozatímní
vlády	vláda	k1gFnSc2	vláda
i	i	k8xC	i
společné	společný	k2eAgNnSc4d1	společné
prohlášení	prohlášení	k1gNnSc4	prohlášení
obou	dva	k4xCgFnPc2	dva
exilových	exilový	k2eAgFnPc2d1	exilová
vlád	vláda	k1gFnPc2	vláda
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
prohlásilo	prohlásit	k5eAaPmAgNnS	prohlásit
mnichovské	mnichovský	k2eAgFnPc4d1	Mnichovská
dohody	dohoda	k1gFnPc4	dohoda
se	s	k7c7	s
všemi	všecek	k3xTgInPc7	všecek
jejich	jejich	k3xOp3gInPc7	jejich
následky	následek	k1gInPc7	následek
za	za	k7c2	za
"	"	kIx"	"
<g/>
neplatné	platný	k2eNgFnPc1d1	neplatná
hned	hned	k6eAd1	hned
od	od	k7c2	od
jejich	jejich	k3xOp3gInSc2	jejich
počátku	počátek	k1gInSc2	počátek
(	(	kIx(	(
<g/>
nul	nula	k1gFnPc2	nula
et	et	k?	et
non	non	k?	non
avenu	aven	k1gInSc2	aven
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k5eAaPmF	opět
stanovisko	stanovisko	k1gNnSc4	stanovisko
k	k	k7c3	k
dohodě	dohoda	k1gFnSc3	dohoda
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc3	který
zní	znět	k5eAaImIp3nS	znět
nuls	nuls	k1gInSc1	nuls
et	et	k?	et
non-avenus	nonvenus	k1gInSc1	non-avenus
čili	čili	k8xC	čili
od	od	k7c2	od
samotného	samotný	k2eAgInSc2d1	samotný
počátku	počátek	k1gInSc2	počátek
<g/>
.	.	kIx.	.
</s>
<s>
Italské	italský	k2eAgNnSc1d1	italské
stanovisko	stanovisko	k1gNnSc1	stanovisko
bylo	být	k5eAaImAgNnS	být
nejvstřícnějším	vstřícný	k2eAgInSc7d3	nejvstřícnější
krokem	krok	k1gInSc7	krok
odvolání	odvolání	k1gNnSc2	odvolání
mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
a	a	k8xC	a
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
snad	snad	k9	snad
i	i	k8xC	i
prvky	prvek	k1gInPc1	prvek
jakési	jakýsi	k3yIgFnSc2	jakýsi
omluvy	omluva	k1gFnSc2	omluva
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
postoj	postoj	k1gInSc4	postoj
(	(	kIx(	(
<g/>
porušení	porušení	k1gNnSc4	porušení
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
celistvosti	celistvost	k1gFnSc2	celistvost
státu	stát	k1gInSc2	stát
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Italský	italský	k2eAgInSc1d1	italský
kabinet	kabinet	k1gInSc1	kabinet
ho	on	k3xPp3gMnSc4	on
vydal	vydat	k5eAaPmAgInS	vydat
pod	pod	k7c7	pod
nátlakem	nátlak	k1gInSc7	nátlak
spojenců	spojenec	k1gMnPc2	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1944	[number]	k4	1944
se	se	k3xPyFc4	se
vláda	vláda	k1gFnSc1	vláda
Itálie	Itálie	k1gFnSc2	Itálie
usnesla	usnést	k5eAaPmAgFnS	usnést
na	na	k7c6	na
stanovisku	stanovisko	k1gNnSc6	stanovisko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
formuloval	formulovat	k5eAaImAgInS	formulovat
tehdejší	tehdejší	k2eAgInSc1d1	tehdejší
kabinet	kabinet	k1gInSc1	kabinet
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Italská	italský	k2eAgFnSc1d1	italská
vláda	vláda	k1gFnSc1	vláda
slavnostně	slavnostně	k6eAd1	slavnostně
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokládá	pokládat	k5eAaImIp3nS	pokládat
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
za	za	k7c4	za
neplatné	platný	k2eNgInPc1d1	neplatný
dohodu	dohoda	k1gFnSc4	dohoda
mnichovskou	mnichovský	k2eAgFnSc4d1	Mnichovská
z	z	k7c2	z
29	[number]	k4	29
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1938	[number]	k4	1938
a	a	k8xC	a
arbitrážní	arbitrážní	k2eAgNnSc4d1	arbitrážní
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
Ciano-Ribbentrop	Ciano-Ribbentrop	k1gInSc1	Ciano-Ribbentrop
<g/>
,	,	kIx,	,
formulované	formulovaný	k2eAgInPc1d1	formulovaný
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
všechny	všechen	k3xTgInPc4	všechen
ostatní	ostatní	k2eAgInPc4d1	ostatní
akty	akt	k1gInPc4	akt
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
jakožto	jakožto	k8xS	jakožto
důsledek	důsledek	k1gInSc1	důsledek
těchto	tento	k3xDgFnPc2	tento
dohod	dohoda	k1gFnPc2	dohoda
a	a	k8xC	a
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
směřovaly	směřovat	k5eAaImAgFnP	směřovat
ke	k	k7c3	k
škodě	škoda	k1gFnSc3	škoda
nezávislosti	nezávislost	k1gFnSc2	nezávislost
a	a	k8xC	a
celistvosti	celistvost	k1gFnSc2	celistvost
Republiky	republika	k1gFnSc2	republika
československé	československý	k2eAgFnSc2d1	Československá
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
italská	italský	k2eAgFnSc1d1	italská
prohlašuje	prohlašovat	k5eAaImIp3nS	prohlašovat
před	před	k7c7	před
světem	svět	k1gInSc7	svět
a	a	k8xC	a
dějinami	dějiny	k1gFnPc7	dějiny
<g/>
,	,	kIx,	,
že	že	k8xS	že
všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
akty	akt	k1gInPc1	akt
a	a	k8xC	a
dohody	dohoda	k1gFnPc1	dohoda
byly	být	k5eAaImAgFnP	být
zradou	zrada	k1gFnSc7	zrada
smýšlení	smýšlení	k1gNnSc2	smýšlení
a	a	k8xC	a
vůle	vůle	k1gFnSc2	vůle
italského	italský	k2eAgInSc2d1	italský
lidu	lid	k1gInSc2	lid
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
Italský	italský	k2eAgInSc1d1	italský
kabinet	kabinet	k1gInSc1	kabinet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nekomentoval	komentovat	k5eNaBmAgMnS	komentovat
případ	případ	k1gInSc4	případ
samostatnosti	samostatnost	k1gFnSc2	samostatnost
Slovenska	Slovensko	k1gNnSc2	Slovensko
a	a	k8xC	a
nijak	nijak	k6eAd1	nijak
se	se	k3xPyFc4	se
nezmiňoval	zmiňovat	k5eNaImAgMnS	zmiňovat
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
německého	německý	k2eAgInSc2d1	německý
satelitu	satelit	k1gInSc2	satelit
-	-	kIx~	-
Slovenského	slovenský	k2eAgInSc2d1	slovenský
státu	stát	k1gInSc2	stát
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc6	jeho
právním	právní	k2eAgInSc6d1	právní
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
Itálii	Itálie	k1gFnSc3	Itálie
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
během	během	k7c2	během
slovenského	slovenský	k2eAgNnSc2d1	slovenské
dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
řada	řada	k1gFnSc1	řada
smluv	smlouva	k1gFnPc2	smlouva
mezi	mezi	k7c7	mezi
Itálií	Itálie	k1gFnSc7	Itálie
a	a	k8xC	a
Slovenským	slovenský	k2eAgInSc7d1	slovenský
státem	stát	k1gInSc7	stát
a	a	k8xC	a
vedena	veden	k2eAgNnPc1d1	vedeno
různá	různý	k2eAgNnPc1d1	různé
koaliční	koaliční	k2eAgNnPc1d1	koaliční
jednání	jednání	k1gNnPc1	jednání
atd.	atd.	kA	atd.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Itálie	Itálie	k1gFnSc1	Itálie
mnichovskou	mnichovský	k2eAgFnSc4d1	Mnichovská
dohodu	dohoda	k1gFnSc4	dohoda
uznala	uznat	k5eAaPmAgFnS	uznat
za	za	k7c4	za
neplatnou	platný	k2eNgFnSc4d1	neplatná
od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
počátku	počátek	k1gInSc2	počátek
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Ministr	ministr	k1gMnSc1	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
SSSR	SSSR	kA	SSSR
(	(	kIx(	(
<g/>
Molotov	Molotov	k1gInSc1	Molotov
<g/>
)	)	kIx)	)
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
obnovu	obnova	k1gFnSc4	obnova
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
předmnichovských	předmnichovský	k2eAgFnPc6d1	předmnichovská
hranicích	hranice	k1gFnPc6	hranice
a	a	k8xC	a
pomoc	pomoc	k1gFnSc4	pomoc
v	v	k7c6	v
případném	případný	k2eAgNnSc6d1	případné
jednání	jednání	k1gNnSc6	jednání
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
(	(	kIx(	(
<g/>
opětovný	opětovný	k2eAgInSc4d1	opětovný
spor	spor	k1gInSc4	spor
o	o	k7c4	o
Těšínsko	Těšínsko	k1gNnSc4	Těšínsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejsložitější	složitý	k2eAgFnSc1d3	nejsložitější
situace	situace	k1gFnSc1	situace
panovala	panovat	k5eAaImAgFnS	panovat
ohledně	ohledně	k7c2	ohledně
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
mezi	mezi	k7c7	mezi
Československem	Československo	k1gNnSc7	Československo
a	a	k8xC	a
Německem	Německo	k1gNnSc7	Německo
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
právní	právní	k2eAgInPc4d1	právní
důsledky	důsledek	k1gInPc4	důsledek
bylo	být	k5eAaImAgNnS	být
potřeba	potřeba	k6eAd1	potřeba
řešit	řešit	k5eAaImF	řešit
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
SRN	srna	k1gFnPc2	srna
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	s	k7c7	s
sovětským	sovětský	k2eAgInSc7d1	sovětský
satelitem	satelit	k1gInSc7	satelit
NDR	NDR	kA	NDR
<g/>
.	.	kIx.	.
</s>
<s>
Problémy	problém	k1gInPc1	problém
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
v	v	k7c6	v
německém	německý	k2eAgInSc6d1	německý
okruhu	okruh	k1gInSc6	okruh
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
vyřešit	vyřešit	k5eAaPmF	vyřešit
už	už	k6eAd1	už
při	při	k7c6	při
kapitulaci	kapitulace	k1gFnSc6	kapitulace
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
zkomplikovala	zkomplikovat	k5eAaPmAgFnS	zkomplikovat
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
problémy	problém	k1gInPc1	problém
<g/>
:	:	kIx,	:
stanovení	stanovení	k1gNnPc1	stanovení
počátku	počátek	k1gInSc2	počátek
válečného	válečný	k2eAgInSc2d1	válečný
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
československá	československý	k2eAgFnSc1d1	Československá
strana	strana	k1gFnSc1	strana
požaduje	požadovat	k5eAaImIp3nS	požadovat
rok	rok	k1gInSc4	rok
1938	[number]	k4	1938
<g/>
,	,	kIx,	,
německá	německý	k2eAgFnSc1d1	německá
až	až	k6eAd1	až
září	zářit	k5eAaImIp3nS	zářit
1939	[number]	k4	1939
<g/>
:	:	kIx,	:
vypuknutí	vypuknutí	k1gNnSc2	vypuknutí
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
NDR	NDR	kA	NDR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
a	a	k8xC	a
1967	[number]	k4	1967
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
neplatnost	neplatnost	k1gFnSc1	neplatnost
dohody	dohoda	k1gFnSc2	dohoda
od	od	k7c2	od
samého	samý	k3xTgInSc2	samý
počátku	počátek	k1gInSc2	počátek
SRN	SRN	kA	SRN
stanovila	stanovit	k5eAaPmAgFnS	stanovit
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1973	[number]	k4	1973
nulitu	nulita	k1gFnSc4	nulita
mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
a	a	k8xC	a
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
bodech	bod	k1gInPc6	bod
zastávala	zastávat	k5eAaImAgFnS	zastávat
odlišný	odlišný	k2eAgInSc4d1	odlišný
její	její	k3xOp3gInSc4	její
výklad	výklad	k1gInSc4	výklad
<g/>
,	,	kIx,	,
problematický	problematický	k2eAgInSc1d1	problematický
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
také	také	k9	také
březen	březen	k1gInSc1	březen
1939	[number]	k4	1939
jako	jako	k8xS	jako
krok	krok	k1gInSc1	krok
nutný	nutný	k2eAgInSc1d1	nutný
pro	pro	k7c4	pro
stabilizaci	stabilizace	k1gFnSc4	stabilizace
hospodářství	hospodářství	k1gNnSc2	hospodářství
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
české	český	k2eAgFnPc1d1	Česká
země	zem	k1gFnPc1	zem
po	po	k7c6	po
ztrátě	ztráta	k1gFnSc6	ztráta
25	[number]	k4	25
procent	procento	k1gNnPc2	procento
území	území	k1gNnPc1	území
nebyly	být	k5eNaImAgFnP	být
životaschopné	životaschopný	k2eAgFnPc4d1	životaschopná
atd.	atd.	kA	atd.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
mělo	mít	k5eAaImAgNnS	mít
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
vyjasnění	vyjasnění	k1gNnSc3	vyjasnění
problematiky	problematika	k1gFnSc2	problematika
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Česko-německá	českoěmecký	k2eAgFnSc1d1	česko-německá
deklarace	deklarace	k1gFnSc1	deklarace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
byla	být	k5eAaImAgFnS	být
Mnichovská	mnichovský	k2eAgFnSc1d1	Mnichovská
dohoda	dohoda	k1gFnSc1	dohoda
ve	v	k7c6	v
vzájemných	vzájemný	k2eAgInPc6d1	vzájemný
vztazích	vztah	k1gInPc6	vztah
mezi	mezi	k7c7	mezi
Německou	německý	k2eAgFnSc7d1	německá
spolkovou	spolkový	k2eAgFnSc7d1	spolková
republikou	republika	k1gFnSc7	republika
a	a	k8xC	a
Československou	československý	k2eAgFnSc7d1	Československá
socialistickou	socialistický	k2eAgFnSc7d1	socialistická
republikou	republika	k1gFnSc7	republika
prohlášena	prohlásit	k5eAaPmNgFnS	prohlásit
pražskou	pražský	k2eAgFnSc7d1	Pražská
smlouvou	smlouva	k1gFnSc7	smlouva
za	za	k7c4	za
nicotnou	nicotný	k2eAgFnSc4d1	nicotná
a	a	k8xC	a
nulitní	nulitní	k2eAgFnSc4d1	nulitní
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1968	[number]	k4	1968
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
ji	on	k3xPp3gFnSc4	on
konference	konference	k1gFnSc1	konference
OSN	OSN	kA	OSN
o	o	k7c6	o
správnosti	správnost	k1gFnSc6	správnost
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
smluv	smlouva	k1gFnPc2	smlouva
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
za	za	k7c4	za
neplatnou	platný	k2eNgFnSc4d1	neplatná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
resp.	resp.	kA	resp.
k	k	k7c3	k
1.1	[number]	k4	1.1
1993	[number]	k4	1993
zaniko	zanika	k1gFnSc5	zanika
Československo	Československo	k1gNnSc1	Československo
jako	jako	k8xC	jako
subjekt	subjekt	k1gInSc1	subjekt
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
tedy	tedy	k9	tedy
ten	ten	k3xDgInSc1	ten
stát	stát	k1gInSc1	stát
vůči	vůči	k7c3	vůči
komu	kdo	k3yRnSc3	kdo
tato	tento	k3xDgFnSc1	tento
nulitní	nulitní	k2eAgFnSc1d1	nulitní
dohoda	dohoda	k1gFnSc1	dohoda
směřovala	směřovat	k5eAaImAgFnS	směřovat
<g/>
.	.	kIx.	.
</s>
<s>
Vznikem	vznik	k1gInSc7	vznik
dvou	dva	k4xCgNnPc6	dva
suveréních	suveréní	k1gNnPc6	suveréní
státu	stát	k1gInSc2	stát
Slovenska	Slovensko	k1gNnSc2	Slovensko
a	a	k8xC	a
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
až	až	k9	až
zápisem	zápis	k1gInSc7	zápis
do	do	k7c2	do
jakési	jakýsi	k3yIgFnSc2	jakýsi
,,	,,	k?	,,
matriky	matrika	k1gFnSc2	matrika
států	stát	k1gInPc2	stát
<g/>
́ ́	́ ́	k?	́ ́
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
1993	[number]	k4	1993
(	(	kIx(	(
na	na	k7c6	na
základě	základ	k1gInSc6	základ
žádosti	žádost	k1gFnSc2	žádost
Valnému	valný	k2eAgNnSc3d1	Valné
shromáždění	shromáždění	k1gNnSc3	shromáždění
OSN	OSN	kA	OSN
)	)	kIx)	)
lze	lze	k6eAd1	lze
určit	určit	k5eAaPmF	určit
vznik	vznik	k1gInSc4	vznik
nových	nový	k2eAgInPc2d1	nový
dvou	dva	k4xCgInPc2	dva
suverenních	suverenní	k2eAgInPc2d1	suverenní
států	stát	k1gInPc2	stát
čili	čili	k8xC	čili
subjektů	subjekt	k1gInPc2	subjekt
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
tedy	tedy	k9	tedy
těch	ten	k3xDgFnPc2	ten
které	který	k3yQgFnPc1	který
nebyly	být	k5eNaImAgFnP	být
účastny	účasten	k2eAgInPc4d1	účasten
Pařížských	pařížský	k2eAgFnPc2d1	Pařížská
mírových	mírový	k2eAgFnPc2d1	mírová
dohod	dohoda	k1gFnPc2	dohoda
směřovaných	směřovaný	k2eAgInPc2d1	směřovaný
vůči	vůči	k7c3	vůči
Německu	Německo	k1gNnSc3	Německo
a	a	k8xC	a
Maďarsku	Maďarsko	k1gNnSc3	Maďarsko
.	.	kIx.	.
</s>
