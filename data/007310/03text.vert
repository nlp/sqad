<s>
Cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
(	(	kIx(	(
<g/>
Saccharum	Saccharum	k1gInSc1	Saccharum
officinarum	officinarum	k1gInSc1	officinarum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
jednoděložné	jednoděložný	k2eAgFnSc2d1	jednoděložná
rostliny	rostlina	k1gFnSc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
lipnicovitých	lipnicovitý	k2eAgFnPc2d1	lipnicovitý
(	(	kIx(	(
<g/>
Poaceae	Poaceae	k1gFnPc2	Poaceae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
hojně	hojně	k6eAd1	hojně
pěstovaná	pěstovaný	k2eAgFnSc1d1	pěstovaná
především	především	k9	především
kvůli	kvůli	k7c3	kvůli
vysokému	vysoký	k2eAgInSc3d1	vysoký
obsahu	obsah	k1gInSc3	obsah
cukru	cukr	k1gInSc2	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
je	být	k5eAaImIp3nS	být
víceletá	víceletý	k2eAgFnSc1d1	víceletá
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
mohutná	mohutný	k2eAgFnSc1d1	mohutná
tráva	tráva	k1gFnSc1	tráva
se	s	k7c7	s
vzpřímenými	vzpřímený	k2eAgNnPc7d1	vzpřímené
masivními	masivní	k2eAgNnPc7d1	masivní
stébly	stéblo	k1gNnPc7	stéblo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
až	až	k9	až
6	[number]	k4	6
m	m	kA	m
vysoká	vysoká	k1gFnSc1	vysoká
a	a	k8xC	a
5	[number]	k4	5
cm	cm	kA	cm
tlustá	tlustý	k2eAgFnSc1d1	tlustá
<g/>
.	.	kIx.	.
</s>
<s>
Stéblo	stéblo	k1gNnSc1	stéblo
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
asi	asi	k9	asi
20	[number]	k4	20
až	až	k9	až
40	[number]	k4	40
kolének	kolénko	k1gNnPc2	kolénko
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vyplněno	vyplnit	k5eAaPmNgNnS	vyplnit
měkkou	měkký	k2eAgFnSc7d1	měkká
dření	dřeň	k1gFnSc7	dřeň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
až	až	k9	až
20	[number]	k4	20
%	%	kIx~	%
cukru	cukr	k1gInSc2	cukr
(	(	kIx(	(
<g/>
především	především	k9	především
tedy	tedy	k9	tedy
sacharózy	sacharóza	k1gFnPc1	sacharóza
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
také	také	k9	také
malé	malý	k2eAgNnSc1d1	malé
množství	množství	k1gNnSc1	množství
fruktózy	fruktóza	k1gFnSc2	fruktóza
a	a	k8xC	a
glukózy	glukóza	k1gFnSc2	glukóza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
ukládá	ukládat	k5eAaImIp3nS	ukládat
jako	jako	k9	jako
produkt	produkt	k1gInSc1	produkt
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
každým	každý	k3xTgNnSc7	každý
kolénkem	kolénko	k1gNnSc7	kolénko
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
adventivních	adventivní	k2eAgInPc2d1	adventivní
kořenů	kořen	k1gInPc2	kořen
<g/>
,	,	kIx,	,
nazývaných	nazývaný	k2eAgInPc2d1	nazývaný
kořenový	kořenový	k2eAgInSc4d1	kořenový
prstenec	prstenec	k1gInSc4	prstenec
<g/>
,	,	kIx,	,
uspořádaná	uspořádaný	k2eAgFnSc1d1	uspořádaná
případně	případně	k6eAd1	případně
i	i	k9	i
v	v	k7c6	v
několika	několik	k4yIc6	několik
kruzích	kruh	k1gInPc6	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgInPc2	tento
kořínků	kořínek	k1gInPc2	kořínek
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
při	při	k7c6	při
vegetativním	vegetativní	k2eAgNnSc6d1	vegetativní
rozmnožování	rozmnožování	k1gNnSc6	rozmnožování
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rostlina	rostlina	k1gFnSc1	rostlina
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
zakoření	zakořenit	k5eAaPmIp3nP	zakořenit
a	a	k8xC	a
vyrostou	vyrůst	k5eAaPmIp3nP	vyrůst
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
nové	nový	k2eAgInPc4d1	nový
výhonky	výhonek	k1gInPc4	výhonek
<g/>
.	.	kIx.	.
</s>
<s>
Cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
na	na	k7c6	na
plantážích	plantáž	k1gFnPc6	plantáž
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
rozmnožuje	rozmnožovat	k5eAaImIp3nS	rozmnožovat
výhradně	výhradně	k6eAd1	výhradně
řízky	řízek	k1gInPc4	řízek
stébel	stéblo	k1gNnPc2	stéblo
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
nad	nad	k7c7	nad
prstencem	prstenec	k1gInSc7	prstenec
kořenů	kořen	k1gInPc2	kořen
leží	ležet	k5eAaImIp3nS	ležet
interkalární	interkalární	k2eAgInSc1d1	interkalární
meristém	meristém	k1gInSc1	meristém
(	(	kIx(	(
<g/>
dělivé	dělivý	k2eAgNnSc1d1	dělivé
pletivo	pletivo	k1gNnSc1	pletivo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
rychlým	rychlý	k2eAgInSc7d1	rychlý
růstem	růst	k1gInSc7	růst
opět	opět	k6eAd1	opět
vzpřímí	vzpřímit	k5eAaPmIp3nP	vzpřímit
polehlé	polehlý	k2eAgNnSc4d1	polehlé
stéblo	stéblo	k1gNnSc4	stéblo
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
délky	délka	k1gFnPc1	délka
100-200	[number]	k4	100-200
cm	cm	kA	cm
a	a	k8xC	a
šířky	šířka	k1gFnSc2	šířka
okolo	okolo	k7c2	okolo
7	[number]	k4	7
cm	cm	kA	cm
a	a	k8xC	a
jejich	jejich	k3xOp3gNnPc2	jejich
uspořádání	uspořádání	k1gNnPc2	uspořádání
je	být	k5eAaImIp3nS	být
střídavé	střídavý	k2eAgNnSc1d1	střídavé
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
listů	list	k1gInPc2	list
se	se	k3xPyFc4	se
odvíjí	odvíjet	k5eAaImIp3nS	odvíjet
od	od	k7c2	od
počtu	počet	k1gInSc2	počet
kolének	kolénko	k1gNnPc2	kolénko
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
z	z	k7c2	z
každého	každý	k3xTgNnSc2	každý
kolénka	kolénko	k1gNnSc2	kolénko
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
právě	právě	k9	právě
jeden	jeden	k4xCgInSc1	jeden
list	list	k1gInSc1	list
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
mají	mít	k5eAaImIp3nP	mít
mohutné	mohutný	k2eAgNnSc4d1	mohutné
bílé	bílý	k2eAgNnSc4d1	bílé
střední	střední	k2eAgNnSc4d1	střední
žebro	žebro	k1gNnSc4	žebro
a	a	k8xC	a
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
jsou	být	k5eAaImIp3nP	být
jemně	jemně	k6eAd1	jemně
zoubkované	zoubkovaný	k2eAgFnPc1d1	zoubkovaná
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
ukládání	ukládání	k1gNnSc3	ukládání
kyseliny	kyselina	k1gFnSc2	kyselina
křemičité	křemičitý	k2eAgFnSc2d1	křemičitá
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
tvrdé	tvrdý	k2eAgInPc1d1	tvrdý
a	a	k8xC	a
ostré	ostrý	k2eAgInPc1d1	ostrý
<g/>
.	.	kIx.	.
</s>
<s>
Listové	listový	k2eAgFnPc1d1	listová
pochvy	pochva	k1gFnPc1	pochva
přiléhají	přiléhat	k5eAaImIp3nP	přiléhat
pevně	pevně	k6eAd1	pevně
ke	k	k7c3	k
stéblu	stéblo	k1gNnSc3	stéblo
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
zelené	zelený	k2eAgFnPc1d1	zelená
barvy	barva	k1gFnPc1	barva
<g/>
,	,	kIx,	,
žlutavé	žlutavý	k2eAgFnPc1d1	žlutavá
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
fialově	fialově	k6eAd1	fialově
zbarvené	zbarvený	k2eAgFnPc4d1	zbarvená
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc4	některý
odrůdy	odrůda	k1gFnPc4	odrůda
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
barevně	barevně	k6eAd1	barevně
pruhované	pruhovaný	k2eAgNnSc1d1	pruhované
<g/>
.	.	kIx.	.
</s>
<s>
Květem	květ	k1gInSc7	květ
třtiny	třtina	k1gFnSc2	třtina
je	být	k5eAaImIp3nS	být
bohatá	bohatý	k2eAgFnSc1d1	bohatá
jehlancovitá	jehlancovitý	k2eAgFnSc1d1	jehlancovitá
<g/>
,	,	kIx,	,
až	až	k9	až
80	[number]	k4	80
cm	cm	kA	cm
vysoká	vysoký	k2eAgFnSc1d1	vysoká
lata	lata	k1gFnSc1	lata
<g/>
.	.	kIx.	.
</s>
<s>
Květ	květ	k1gInSc1	květ
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
tyčinek	tyčinka	k1gFnPc2	tyčinka
<g/>
,	,	kIx,	,
plenek	plenka	k1gFnPc2	plenka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
představují	představovat	k5eAaImIp3nP	představovat
zbytek	zbytek	k1gInSc4	zbytek
okvětí	okvětí	k1gNnSc2	okvětí
<g/>
,	,	kIx,	,
a	a	k8xC	a
pestíku	pestík	k1gInSc2	pestík
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
pérovitými	pérovitý	k2eAgFnPc7d1	pérovitý
bliznami	blizna	k1gFnPc7	blizna
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
je	být	k5eAaImIp3nS	být
obilka	obilka	k1gFnSc1	obilka
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
na	na	k7c6	na
plantážích	plantáž	k1gFnPc6	plantáž
je	být	k5eAaImIp3nS	být
květenství	květenství	k1gNnSc1	květenství
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
nežádoucí	žádoucí	k2eNgFnPc1d1	nežádoucí
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
s	s	k7c7	s
květem	květ	k1gInSc7	květ
se	se	k3xPyFc4	se
snižuje	snižovat	k5eAaImIp3nS	snižovat
obsah	obsah	k1gInSc1	obsah
cukru	cukr	k1gInSc2	cukr
ve	v	k7c6	v
stéblech	stéblo	k1gNnPc6	stéblo
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
snižuje	snižovat	k5eAaImIp3nS	snižovat
hodnota	hodnota	k1gFnSc1	hodnota
rostliny	rostlina	k1gFnSc2	rostlina
pro	pro	k7c4	pro
pěstitele	pěstitel	k1gMnPc4	pěstitel
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
kukuřicí	kukuřice	k1gFnSc7	kukuřice
<g/>
,	,	kIx,	,
rozličnými	rozličný	k2eAgInPc7d1	rozličný
druhy	druh	k1gInPc7	druh
prosa	proso	k1gNnSc2	proso
a	a	k8xC	a
některými	některý	k3yIgFnPc7	některý
dalšími	další	k2eAgFnPc7d1	další
užitkovými	užitkový	k2eAgFnPc7d1	užitková
rostlinami	rostlina	k1gFnPc7	rostlina
představuje	představovat	k5eAaImIp3nS	představovat
cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
typ	typ	k1gInSc1	typ
tzv.	tzv.	kA	tzv.
tropických	tropický	k2eAgFnPc2d1	tropická
vysoce	vysoce	k6eAd1	vysoce
výnosných	výnosný	k2eAgFnPc2d1	výnosná
plodin	plodina	k1gFnPc2	plodina
<g/>
.	.	kIx.	.
</s>
<s>
Pomocí	pomocí	k7c2	pomocí
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
fyziologického	fyziologický	k2eAgInSc2d1	fyziologický
triku	trik	k1gInSc2	trik
mohou	moct	k5eAaImIp3nP	moct
tyto	tento	k3xDgFnPc1	tento
rostliny	rostlina	k1gFnPc1	rostlina
během	během	k7c2	během
časové	časový	k2eAgFnSc2d1	časová
jednotky	jednotka	k1gFnSc2	jednotka
vázat	vázat	k5eAaImF	vázat
podstatně	podstatně	k6eAd1	podstatně
více	hodně	k6eAd2	hodně
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
než	než	k8xS	než
ostatní	ostatní	k2eAgFnPc1d1	ostatní
zelené	zelený	k2eAgFnPc1d1	zelená
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Obsahují	obsahovat	k5eAaImIp3nP	obsahovat
enzymatický	enzymatický	k2eAgInSc4d1	enzymatický
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pracuje	pracovat	k5eAaImIp3nS	pracovat
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
pumpa	pumpa	k1gFnSc1	pumpa
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
a	a	k8xC	a
při	při	k7c6	při
poměrně	poměrně	k6eAd1	poměrně
malé	malý	k2eAgFnSc6d1	malá
spotřebě	spotřeba	k1gFnSc6	spotřeba
vody	voda	k1gFnSc2	voda
přináší	přinášet	k5eAaImIp3nS	přinášet
významné	významný	k2eAgInPc4d1	významný
produkční	produkční	k2eAgInPc4d1	produkční
výnosy	výnos	k1gInPc4	výnos
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přídatný	přídatný	k2eAgInSc1d1	přídatný
mechanismu	mechanismus	k1gInSc3	mechanismus
je	být	k5eAaImIp3nS	být
však	však	k9	však
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
vázán	vázat	k5eAaImNgMnS	vázat
na	na	k7c4	na
dosti	dosti	k6eAd1	dosti
vysoké	vysoký	k2eAgFnPc4d1	vysoká
teploty	teplota	k1gFnPc4	teplota
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
užitkové	užitkový	k2eAgFnPc1d1	užitková
rostliny	rostlina	k1gFnPc1	rostlina
s	s	k7c7	s
typem	typ	k1gInSc7	typ
látkové	látkový	k2eAgFnSc2d1	látková
výměny	výměna	k1gFnSc2	výměna
podobné	podobný	k2eAgFnSc2d1	podobná
právě	právě	k6eAd1	právě
cukrové	cukrový	k2eAgFnSc3d1	cukrová
třtině	třtina	k1gFnSc3	třtina
přednostně	přednostně	k6eAd1	přednostně
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
teplých	teplý	k2eAgFnPc6d1	teplá
oblastech	oblast	k1gFnPc6	oblast
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
Optimální	optimální	k2eAgFnSc1d1	optimální
teplota	teplota	k1gFnSc1	teplota
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
je	být	k5eAaImIp3nS	být
30	[number]	k4	30
°	°	k?	°
<g/>
C.	C.	kA	C.
Při	při	k7c6	při
20	[number]	k4	20
°	°	k?	°
<g/>
C	C	kA	C
se	se	k3xPyFc4	se
její	její	k3xOp3gInSc1	její
růst	růst	k1gInSc1	růst
silně	silně	k6eAd1	silně
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
a	a	k8xC	a
pod	pod	k7c7	pod
15	[number]	k4	15
°	°	k?	°
<g/>
C	C	kA	C
úplně	úplně	k6eAd1	úplně
zastavuje	zastavovat	k5eAaImIp3nS	zastavovat
<g/>
.	.	kIx.	.
</s>
<s>
Rostlina	rostlina	k1gFnSc1	rostlina
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
propustnou	propustný	k2eAgFnSc4d1	propustná
půdu	půda	k1gFnSc4	půda
bohatou	bohatý	k2eAgFnSc4d1	bohatá
na	na	k7c4	na
humus	humus	k1gInSc4	humus
a	a	k8xC	a
živiny	živina	k1gFnPc4	živina
<g/>
.	.	kIx.	.
</s>
<s>
Cukrovou	cukrový	k2eAgFnSc4d1	cukrová
třtinu	třtina	k1gFnSc4	třtina
nelze	lze	k6eNd1	lze
skladovat	skladovat	k5eAaImF	skladovat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
okamžitě	okamžitě	k6eAd1	okamžitě
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgFnP	být
vyšlechtěny	vyšlechtit	k5eAaPmNgFnP	vyšlechtit
vysoce	vysoce	k6eAd1	vysoce
výkonné	výkonný	k2eAgFnPc1d1	výkonná
odrůdy	odrůda	k1gFnPc1	odrůda
poskytující	poskytující	k2eAgFnPc1d1	poskytující
až	až	k9	až
22	[number]	k4	22
tun	tuna	k1gFnPc2	tuna
cukru	cukr	k1gInSc2	cukr
z	z	k7c2	z
1	[number]	k4	1
hektaru	hektar	k1gInSc2	hektar
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sklizni	sklizeň	k1gFnSc6	sklizeň
se	se	k3xPyFc4	se
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
zanechávají	zanechávat	k5eAaImIp3nP	zanechávat
kořeny	kořen	k1gInPc1	kořen
a	a	k8xC	a
části	část	k1gFnPc1	část
stébel	stéblo	k1gNnPc2	stéblo
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
pak	pak	k6eAd1	pak
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
nové	nový	k2eAgFnPc4d1	nová
rostliny	rostlina	k1gFnPc4	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jasný	jasný	k2eAgInSc1d1	jasný
<g/>
.	.	kIx.	.
</s>
<s>
Nejpravděpodobněji	pravděpodobně	k6eAd3	pravděpodobně
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
planého	planý	k2eAgInSc2d1	planý
<g/>
,	,	kIx,	,
10	[number]	k4	10
m	m	kA	m
vysokého	vysoký	k2eAgInSc2d1	vysoký
druhu	druh	k1gInSc2	druh
Saccharum	Saccharum	k1gNnSc1	Saccharum
robustum	robustum	k1gNnSc1	robustum
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
Nové	Nové	k2eAgInSc6d1	Nové
Guneji	Gunej	k1gInSc6	Gunej
<g/>
,	,	kIx,	,
okolních	okolní	k2eAgInPc6d1	okolní
ostrovech	ostrov	k1gInPc6	ostrov
a	a	k8xC	a
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Známe	znát	k5eAaImIp1nP	znát
však	však	k9	však
ještě	ještě	k9	ještě
jeden	jeden	k4xCgInSc4	jeden
planý	planý	k2eAgInSc4d1	planý
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
Saccharum	Saccharum	k1gInSc4	Saccharum
spontaneum	spontaneum	k1gNnSc4	spontaneum
<g/>
,	,	kIx,	,
rostoucí	rostoucí	k2eAgInSc4d1	rostoucí
od	od	k7c2	od
severovýchodní	severovýchodní	k2eAgFnSc2d1	severovýchodní
Afriky	Afrika	k1gFnSc2	Afrika
po	po	k7c6	po
Asii	Asie	k1gFnSc6	Asie
až	až	k9	až
k	k	k7c3	k
Pacifiku	Pacifik	k1gInSc3	Pacifik
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
tráva	tráva	k1gFnSc1	tráva
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
výšky	výška	k1gFnSc2	výška
8	[number]	k4	8
m	m	kA	m
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
šlechtění	šlechtění	k1gNnSc6	šlechtění
ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
vlastností	vlastnost	k1gFnPc2	vlastnost
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
její	její	k3xOp3gFnPc1	její
odolnosti	odolnost	k1gFnPc1	odolnost
vůči	vůči	k7c3	vůči
chorobám	choroba	k1gFnPc3	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Benátčané	Benátčan	k1gMnPc1	Benátčan
naučili	naučit	k5eAaPmAgMnP	naučit
rafinovat	rafinovat	k5eAaImF	rafinovat
cukr	cukr	k1gInSc4	cukr
a	a	k8xC	a
obchodovali	obchodovat	k5eAaImAgMnP	obchodovat
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
i	i	k9	i
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
třtinový	třtinový	k2eAgInSc1d1	třtinový
cukr	cukr	k1gInSc1	cukr
užívat	užívat	k5eAaImF	užívat
až	až	k9	až
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
po	po	k7c6	po
křižáckých	křižácký	k2eAgFnPc6d1	křižácká
válkách	válka	k1gFnPc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
sladilo	sladit	k5eAaPmAgNnS	sladit
medem	med	k1gInSc7	med
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
dovážený	dovážený	k2eAgInSc1d1	dovážený
cukr	cukr	k1gInSc1	cukr
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
drahý	drahý	k2eAgInSc1d1	drahý
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
spíše	spíše	k9	spíše
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
lék	lék	k1gInSc4	lék
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
Ameriky	Amerika	k1gFnSc2	Amerika
přivezl	přivézt	k5eAaPmAgMnS	přivézt
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
cukrovou	cukrový	k2eAgFnSc4d1	cukrová
třtinu	třtina	k1gFnSc4	třtina
na	na	k7c4	na
Haiti	Haiti	k1gNnSc4	Haiti
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
byla	být	k5eAaImAgFnS	být
šířena	šířit	k5eAaImNgFnS	šířit
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
Kubu	Kuba	k1gFnSc4	Kuba
<g/>
,	,	kIx,	,
do	do	k7c2	do
Mexika	Mexiko	k1gNnSc2	Mexiko
a	a	k8xC	a
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
.	.	kIx.	.
</s>
<s>
Příznivé	příznivý	k2eAgFnPc1d1	příznivá
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
,	,	kIx,	,
kterými	který	k3yIgFnPc7	který
byly	být	k5eAaImAgFnP	být
především	především	k9	především
podnebí	podnebí	k1gNnSc2	podnebí
a	a	k8xC	a
levná	levný	k2eAgFnSc1d1	levná
pracovní	pracovní	k2eAgFnSc1d1	pracovní
síla	síla	k1gFnSc1	síla
<g/>
,	,	kIx,	,
umožnily	umožnit	k5eAaPmAgFnP	umožnit
pěstovat	pěstovat	k5eAaImF	pěstovat
třtinu	třtina	k1gFnSc4	třtina
na	na	k7c6	na
velkých	velký	k2eAgFnPc6d1	velká
plochách	plocha	k1gFnPc6	plocha
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
rychlému	rychlý	k2eAgNnSc3d1	rychlé
zvýšení	zvýšení	k1gNnSc3	zvýšení
produkce	produkce	k1gFnSc2	produkce
levného	levný	k2eAgInSc2d1	levný
cukru	cukr	k1gInSc2	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Cukr	cukr	k1gInSc1	cukr
dovážený	dovážený	k2eAgInSc1d1	dovážený
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
zaplavil	zaplavit	k5eAaPmAgMnS	zaplavit
evropský	evropský	k2eAgInSc4d1	evropský
trh	trh	k1gInSc4	trh
a	a	k8xC	a
vytlačil	vytlačit	k5eAaPmAgMnS	vytlačit
pěstování	pěstování	k1gNnSc4	pěstování
třtiny	třtina	k1gFnSc2	třtina
ze	z	k7c2	z
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Třtinový	třtinový	k2eAgInSc4d1	třtinový
cukr	cukr	k1gInSc4	cukr
dovážený	dovážený	k2eAgInSc4d1	dovážený
z	z	k7c2	z
Nového	Nového	k2eAgInSc2d1	Nového
světa	svět	k1gInSc2	svět
byl	být	k5eAaImAgInS	být
hnědý	hnědý	k2eAgInSc1d1	hnědý
a	a	k8xC	a
připálený	připálený	k2eAgInSc1d1	připálený
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
začaly	začít	k5eAaPmAgFnP	začít
stavět	stavět	k5eAaImF	stavět
speciální	speciální	k2eAgFnPc4d1	speciální
čistírny	čistírna	k1gFnPc4	čistírna
zvané	zvaný	k2eAgFnPc4d1	zvaná
rafinérie	rafinérie	k1gFnPc4	rafinérie
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
takováto	takovýto	k3xDgFnSc1	takovýto
rafinérie	rafinérie	k1gFnPc4	rafinérie
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
Holandsku	Holandsko	k1gNnSc6	Holandsko
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Leyden	Leydna	k1gFnPc2	Leydna
<g/>
.	.	kIx.	.
</s>
<s>
Dovoz	dovoz	k1gInSc1	dovoz
třtinového	třtinový	k2eAgInSc2d1	třtinový
cukru	cukr	k1gInSc2	cukr
ze	z	k7c2	z
zámoří	zámoří	k1gNnSc2	zámoří
trval	trvat	k5eAaImAgInS	trvat
až	až	k6eAd1	až
do	do	k7c2	do
konce	konec	k1gInSc2	konec
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Napoleon	napoleon	k1gInSc1	napoleon
Bonaparte	bonapart	k1gInSc5	bonapart
vydal	vydat	k5eAaPmAgInS	vydat
zákaz	zákaz	k1gInSc1	zákaz
dovážet	dovážet	k5eAaImF	dovážet
anglické	anglický	k2eAgNnSc4d1	anglické
zboží	zboží	k1gNnSc4	zboží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
se	s	k7c7	s
šlechtěním	šlechtění	k1gNnSc7	šlechtění
cukrové	cukrový	k2eAgFnSc2d1	cukrová
řepy	řepa	k1gFnSc2	řepa
a	a	k8xC	a
třtinový	třtinový	k2eAgInSc1d1	třtinový
cukr	cukr	k1gInSc1	cukr
byl	být	k5eAaImAgInS	být
postupně	postupně	k6eAd1	postupně
nahrazován	nahrazovat	k5eAaImNgInS	nahrazovat
cukrem	cukr	k1gInSc7	cukr
řepným	řepný	k2eAgInSc7d1	řepný
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
pěstování	pěstování	k1gNnSc3	pěstování
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
potřeba	potřeba	k6eAd1	potřeba
stálá	stálý	k2eAgFnSc1d1	stálá
teplota	teplota	k1gFnSc1	teplota
nad	nad	k7c7	nad
30	[number]	k4	30
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
plodina	plodina	k1gFnSc1	plodina
především	především	k9	především
v	v	k7c6	v
tropickém	tropický	k2eAgNnSc6d1	tropické
podnebí	podnebí	k1gNnSc6	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc1d3	veliký
plantáže	plantáž	k1gFnPc1	plantáž
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
na	na	k7c6	na
Kubě	Kuba	k1gFnSc6	Kuba
<g/>
,	,	kIx,	,
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
se	se	k3xPyFc4	se
sklízí	sklízet	k5eAaImIp3nS	sklízet
speciálnímí	speciálnímí	k1gNnSc1	speciálnímí
sklízecími	sklízecí	k2eAgInPc7d1	sklízecí
stroji	stroj	k1gInPc7	stroj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
stroje	stroj	k1gInPc1	stroj
nedostanou	dostat	k5eNaPmIp3nP	dostat
<g/>
,	,	kIx,	,
nastupuje	nastupovat	k5eAaImIp3nS	nastupovat
sklízení	sklízení	k1gNnSc1	sklízení
ruční	ruční	k2eAgNnSc1d1	ruční
pomocí	pomoc	k1gFnSc7	pomoc
mačet	mačeta	k1gFnPc2	mačeta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sklizni	sklizeň	k1gFnSc6	sklizeň
se	se	k3xPyFc4	se
třtinová	třtinový	k2eAgNnPc1d1	třtinové
stébla	stéblo	k1gNnPc1	stéblo
odvážejí	odvážet	k5eAaImIp3nP	odvážet
do	do	k7c2	do
cukrovarů	cukrovar	k1gInPc2	cukrovar
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
lisují	lisovat	k5eAaImIp3nP	lisovat
a	a	k8xC	a
z	z	k7c2	z
vyteklé	vyteklý	k2eAgFnSc2d1	vyteklá
šťávy	šťáva	k1gFnSc2	šťáva
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
cukr	cukr	k1gInSc1	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
samotného	samotný	k2eAgInSc2d1	samotný
cukru	cukr	k1gInSc2	cukr
mají	mít	k5eAaImIp3nP	mít
význam	význam	k1gInSc4	význam
i	i	k9	i
vedlejší	vedlejší	k2eAgInPc4d1	vedlejší
produkty	produkt	k1gInPc4	produkt
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
melasa	melasa	k1gFnSc1	melasa
(	(	kIx(	(
<g/>
odpadní	odpadní	k2eAgInSc1d1	odpadní
cukerný	cukerný	k2eAgInSc1d1	cukerný
sirup	sirup	k1gInSc1	sirup
<g/>
)	)	kIx)	)
a	a	k8xC	a
bagasa	bagasa	k1gFnSc1	bagasa
(	(	kIx(	(
<g/>
zbytky	zbytek	k1gInPc1	zbytek
stébel	stéblo	k1gNnPc2	stéblo
po	po	k7c6	po
vylisování	vylisování	k1gNnSc6	vylisování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
melasy	melasa	k1gFnSc2	melasa
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
rum	rum	k1gInSc1	rum
<g/>
,	,	kIx,	,
ze	z	k7c2	z
2-3	[number]	k4	2-3
l	l	kA	l
melasy	melasa	k1gFnSc2	melasa
se	se	k3xPyFc4	se
vyrobí	vyrobit	k5eAaPmIp3nS	vyrobit
1	[number]	k4	1
l	l	kA	l
rumu	rum	k1gInSc2	rum
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
gin	gin	k1gInSc1	gin
<g/>
,	,	kIx,	,
vodka	vodka	k1gFnSc1	vodka
<g/>
,	,	kIx,	,
kvasnice	kvasnice	k1gFnPc1	kvasnice
a	a	k8xC	a
jiné	jiný	k2eAgInPc1d1	jiný
výrobky	výrobek	k1gInPc1	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Melasa	melasa	k1gFnSc1	melasa
je	být	k5eAaImIp3nS	být
také	také	k9	také
vhodným	vhodný	k2eAgNnSc7d1	vhodné
krmivem	krmivo	k1gNnSc7	krmivo
pro	pro	k7c4	pro
hospodářská	hospodářský	k2eAgNnPc4d1	hospodářské
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Vylisovanými	vylisovaný	k2eAgNnPc7d1	vylisované
stébly	stéblo	k1gNnPc7	stéblo
se	se	k3xPyFc4	se
topí	topit	k5eAaImIp3nS	topit
v	v	k7c6	v
cukrovarech	cukrovar	k1gInPc6	cukrovar
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jako	jako	k8xS	jako
surovina	surovina	k1gFnSc1	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
lepenek	lepenka	k1gFnPc2	lepenka
a	a	k8xC	a
desek	deska	k1gFnPc2	deska
z	z	k7c2	z
umělé	umělý	k2eAgFnSc2d1	umělá
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
s	s	k7c7	s
melasou	melasa	k1gFnSc7	melasa
se	se	k3xPyFc4	se
zbytky	zbytek	k1gInPc1	zbytek
stébel	stéblo	k1gNnPc2	stéblo
také	také	k9	také
zkrmují	zkrmovat	k5eAaImIp3nP	zkrmovat
<g/>
.	.	kIx.	.
</s>
<s>
Stébla	stéblo	k1gNnPc1	stéblo
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
se	se	k3xPyFc4	se
také	také	k9	také
od	od	k7c2	od
nepaměti	nepaměť	k1gFnSc2	nepaměť
žvýkají	žvýkat	k5eAaImIp3nP	žvýkat
<g/>
.	.	kIx.	.
</s>
<s>
Sklizeň	sklizeň	k1gFnSc1	sklizeň
cukrové	cukrový	k2eAgFnSc2d1	cukrová
třtiny	třtina	k1gFnSc2	třtina
začíná	začínat	k5eAaImIp3nS	začínat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
opálí	opálit	k5eAaPmIp3nP	opálit
spodní	spodní	k2eAgInPc1d1	spodní
uschlé	uschlý	k2eAgInPc1d1	uschlý
listy	list	k1gInPc1	list
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
přijdou	přijít	k5eAaPmIp3nP	přijít
na	na	k7c4	na
řadu	řada	k1gFnSc4	řada
sklízecí	sklízecí	k2eAgInPc4d1	sklízecí
stroje	stroj	k1gInPc4	stroj
<g/>
,	,	kIx,	,
v	v	k7c6	v
obtížně	obtížně	k6eAd1	obtížně
dosažitelných	dosažitelný	k2eAgFnPc6d1	dosažitelná
polohách	poloha	k1gFnPc6	poloha
rolníci	rolník	k1gMnPc1	rolník
s	s	k7c7	s
mačetami	mačeta	k1gFnPc7	mačeta
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
okamžiku	okamžik	k1gInSc2	okamžik
useknutí	useknutí	k1gNnSc1	useknutí
stébla	stéblo	k1gNnSc2	stéblo
začíná	začínat	k5eAaImIp3nS	začínat
boj	boj	k1gInSc1	boj
s	s	k7c7	s
časem	čas	k1gInSc7	čas
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
začíná	začínat	k5eAaImIp3nS	začínat
do	do	k7c2	do
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
kazit	kazit	k5eAaImF	kazit
<g/>
.	.	kIx.	.
</s>
<s>
Rostliny	rostlina	k1gFnPc1	rostlina
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
rychle	rychle	k6eAd1	rychle
rozřežou	rozřezat	k5eAaPmIp3nP	rozřezat
a	a	k8xC	a
lisují	lisovat	k5eAaImIp3nP	lisovat
<g/>
.	.	kIx.	.
</s>
<s>
Získaná	získaný	k2eAgFnSc1d1	získaná
šťáva	šťáva	k1gFnSc1	šťáva
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
několikrát	několikrát	k6eAd1	několikrát
povaří	povařit	k5eAaPmIp3nS	povařit
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
rychle	rychle	k6eAd1	rychle
zchladí	zchladit	k5eAaPmIp3nS	zchladit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
šťávy	šťáva	k1gFnSc2	šťáva
vykrystalizuje	vykrystalizovat	k5eAaPmIp3nS	vykrystalizovat
jantarově	jantarově	k6eAd1	jantarově
zabarvený	zabarvený	k2eAgInSc4d1	zabarvený
třtinový	třtinový	k2eAgInSc4d1	třtinový
cukr	cukr	k1gInSc4	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
oddělení	oddělení	k1gNnSc6	oddělení
cukrové	cukrový	k2eAgFnSc2d1	cukrová
krusty	krusta	k1gFnSc2	krusta
zůstane	zůstat	k5eAaPmIp3nS	zůstat
v	v	k7c6	v
nádobě	nádoba	k1gFnSc6	nádoba
melasa	melasa	k1gFnSc1	melasa
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
hustá	hustý	k2eAgFnSc1d1	hustá
<g/>
,	,	kIx,	,
tmavá	tmavý	k2eAgFnSc1d1	tmavá
a	a	k8xC	a
poněkud	poněkud	k6eAd1	poněkud
nahořklá	nahořklý	k2eAgFnSc1d1	nahořklá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
posléze	posléze	k6eAd1	posléze
může	moct	k5eAaImIp3nS	moct
vyrábět	vyrábět	k5eAaImF	vyrábět
třeba	třeba	k6eAd1	třeba
rum	rum	k1gInSc4	rum
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
melasa	melasa	k1gFnSc1	melasa
je	být	k5eAaImIp3nS	být
následně	následně	k6eAd1	následně
zředěna	zředěn	k2eAgFnSc1d1	zředěna
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
prochází	procházet	k5eAaImIp3nS	procházet
filtračními	filtrační	k2eAgInPc7d1	filtrační
procesy	proces	k1gInPc7	proces
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
není	být	k5eNaImIp3nS	být
směs	směs	k1gFnSc1	směs
dostatečně	dostatečně	k6eAd1	dostatečně
čistá	čistý	k2eAgFnSc1d1	čistá
a	a	k8xC	a
dokud	dokud	k6eAd1	dokud
není	být	k5eNaImIp3nS	být
dosaženo	dosažen	k2eAgNnSc4d1	dosaženo
požadované	požadovaný	k2eAgFnPc4d1	požadovaná
hustoty	hustota	k1gFnPc4	hustota
a	a	k8xC	a
kyselosti	kyselost	k1gFnPc4	kyselost
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
přidá	přidat	k5eAaPmIp3nS	přidat
kultura	kultura	k1gFnSc1	kultura
z	z	k7c2	z
kvasnic	kvasnice	k1gFnPc2	kvasnice
a	a	k8xC	a
začíná	začínat	k5eAaImIp3nS	začínat
fermentace	fermentace	k1gFnSc1	fermentace
<g/>
.	.	kIx.	.
</s>
<s>
Tempo	tempo	k1gNnSc1	tempo
fermentace	fermentace	k1gFnSc2	fermentace
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
vlastnosti	vlastnost	k1gFnPc4	vlastnost
výsledného	výsledný	k2eAgInSc2d1	výsledný
produktu	produkt	k1gInSc2	produkt
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ovlivňováno	ovlivňován	k2eAgNnSc4d1	ovlivňováno
teplotou	teplota	k1gFnSc7	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Lehké	Lehké	k2eAgInPc1d1	Lehké
bílé	bílý	k2eAgInPc1d1	bílý
rumy	rum	k1gInPc1	rum
kvasí	kvasit	k5eAaImIp3nP	kvasit
přibližně	přibližně	k6eAd1	přibližně
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
těžké	těžký	k2eAgInPc1d1	těžký
tmavé	tmavý	k2eAgInPc1d1	tmavý
rumy	rum	k1gInPc1	rum
až	až	k9	až
12	[number]	k4	12
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Destiláty	destilát	k1gInPc1	destilát
vypálené	vypálený	k2eAgInPc1d1	vypálený
jakoukoliv	jakýkoliv	k3yIgFnSc7	jakýkoliv
technikou	technika	k1gFnSc7	technika
jsou	být	k5eAaImIp3nP	být
bezbarvé	bezbarvý	k2eAgFnPc1d1	bezbarvá
<g/>
,	,	kIx,	,
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
získána	získat	k5eAaPmNgFnS	získat
až	až	k6eAd1	až
zráním	zrání	k1gNnSc7	zrání
v	v	k7c6	v
dubových	dubový	k2eAgInPc6d1	dubový
sudech	sud	k1gInPc6	sud
(	(	kIx(	(
<g/>
toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgInSc4d1	tradiční
postup	postup	k1gInSc4	postup
<g/>
,	,	kIx,	,
právě	právě	k6eAd1	právě
dřevo	dřevo	k1gNnSc1	dřevo
dodá	dodat	k5eAaPmIp3nS	dodat
rumu	rum	k1gInSc2	rum
kromě	kromě	k7c2	kromě
barvy	barva	k1gFnSc2	barva
také	také	k6eAd1	také
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
aroma	aroma	k1gNnSc1	aroma
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
přidáním	přidání	k1gNnSc7	přidání
lihovarnického	lihovarnický	k2eAgInSc2d1	lihovarnický
karamelu	karamel	k1gInSc2	karamel
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
lacinější	laciný	k2eAgFnSc1d2	lacinější
varianta	varianta	k1gFnSc1	varianta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
