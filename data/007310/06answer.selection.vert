<s>
Cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
je	být	k5eAaImIp3nS	být
víceletá	víceletý	k2eAgFnSc1d1	víceletá
<g/>
,	,	kIx,	,
značně	značně	k6eAd1	značně
mohutná	mohutný	k2eAgFnSc1d1	mohutná
tráva	tráva	k1gFnSc1	tráva
se	s	k7c7	s
vzpřímenými	vzpřímený	k2eAgNnPc7d1	vzpřímené
masivními	masivní	k2eAgNnPc7d1	masivní
stébly	stéblo	k1gNnPc7	stéblo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
až	až	k9	až
6	[number]	k4	6
m	m	kA	m
vysoká	vysoká	k1gFnSc1	vysoká
a	a	k8xC	a
5	[number]	k4	5
cm	cm	kA	cm
tlustá	tlustý	k2eAgFnSc1d1	tlustá
<g/>
.	.	kIx.	.
</s>
