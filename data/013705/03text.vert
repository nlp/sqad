<s>
Laurin	Laurin	k1gInSc1
&	&	k?
Klement	Klement	k1gMnSc1
–	–	k?
Škoda	Škoda	k1gMnSc1
125	#num#	k4
</s>
<s>
Laurin	Laurin	k1gInSc1
&	&	k?
Klement	Klement	k1gMnSc1
–	–	k?
Škoda	škoda	k6eAd1
125	#num#	k4
hasičský	hasičský	k2eAgInSc1d1
autobus	autobus	k1gInSc1
Škoda	škoda	k6eAd1
125	#num#	k4
karosovaný	karosovaný	k2eAgInSc1d1
SodomkouVýrobce	SodomkouVýrobka	k1gFnSc6
</s>
<s>
Akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
Škodovy	Škodův	k2eAgInPc1d1
závody	závod	k1gInPc1
<g/>
,	,	kIx,
automobilka	automobilka	k1gFnSc1
v	v	k7c6
Mladé	mladý	k2eAgFnSc6d1
Boleslavi	Boleslaev	k1gFnSc6
Další	další	k2eAgNnPc1d1
jména	jméno	k1gNnPc1
</s>
<s>
Škoda	škoda	k1gFnSc1
125	#num#	k4
Roky	rok	k1gInPc1
produkce	produkce	k1gFnSc2
</s>
<s>
1927	#num#	k4
<g/>
–	–	k?
<g/>
1929	#num#	k4
Vyrobeno	vyrobit	k5eAaPmNgNnS
</s>
<s>
1	#num#	k4
650	#num#	k4
kusů	kus	k1gInPc2
Místa	místo	k1gNnSc2
výroby	výroba	k1gFnSc2
</s>
<s>
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
Technické	technický	k2eAgInPc4d1
údaje	údaj	k1gInPc4
Užitečná	užitečný	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
</s>
<s>
1500	#num#	k4
kg	kg	kA
Maximální	maximální	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
</s>
<s>
60	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
Motor	motor	k1gInSc1
Motor	motor	k1gInSc1
</s>
<s>
řadový	řadový	k2eAgMnSc1d1
<g/>
,	,	kIx,
SV	sv	kA
Objem	objem	k1gInSc1
</s>
<s>
1,9	1,9	k4
Počet	počet	k1gInSc4
válců	válec	k1gInPc2
</s>
<s>
4	#num#	k4
Výkon	výkon	k1gInSc1
</s>
<s>
19	#num#	k4
kW	kW	kA
Převodovky	převodovka	k1gFnSc2
Převodovka	převodovka	k1gFnSc1
</s>
<s>
4	#num#	k4
<g/>
+	+	kIx~
<g/>
1	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Laurin	Laurin	k1gInSc1
&	&	k?
Klement	Klement	k1gMnSc1
–	–	k?
Škoda	Škoda	k1gMnSc1
125	#num#	k4
nebo	nebo	k8xC
jen	jen	k9
Škoda	škoda	k1gFnSc1
125	#num#	k4
byl	být	k5eAaImAgInS
lehký	lehký	k2eAgInSc1d1
nákladní	nákladní	k2eAgInSc1d1
automobil	automobil	k1gInSc1
vyráběný	vyráběný	k2eAgInSc1d1
bývalou	bývalý	k2eAgFnSc7d1
automobilkou	automobilka	k1gFnSc7
Laurin	Laurin	k1gInSc1
&	&	k?
Klement	Klement	k1gMnSc1
mezi	mezi	k7c7
lety	let	k1gInPc7
1927	#num#	k4
<g/>
–	–	k?
<g/>
1929	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniklo	vzniknout	k5eAaPmAgNnS
1650	#num#	k4
vozů	vůz	k1gInPc2
<g/>
,	,	kIx,
vybavovaných	vybavovaný	k2eAgInPc2d1
řadou	řada	k1gFnSc7
nástaveb	nástavba	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k9
valníkovou	valníkový	k2eAgFnSc4d1
<g/>
,	,	kIx,
autobusovou	autobusový	k2eAgFnSc4d1
<g/>
,	,	kIx,
skříňovou	skříňový	k2eAgFnSc4d1
a	a	k8xC
dalšími	další	k2eAgMnPc7d1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Vůz	vůz	k1gInSc1
Škoda	škoda	k1gFnSc1
125	#num#	k4
byl	být	k5eAaImAgInS
vyvinut	vyvinout	k5eAaPmNgInS
ještě	ještě	k6eAd1
před	před	k7c7
fúzí	fúze	k1gFnSc7
automobilky	automobilka	k1gFnSc2
Laurin	Laurin	k1gInSc1
&	&	k?
Klement	Klement	k1gMnSc1
a	a	k8xC
koncernu	koncern	k1gInSc2
Akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
Škodovy	Škodův	k2eAgInPc1d1
závody	závod	k1gInPc1
v	v	k7c6
roce	rok	k1gInSc6
1925	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
konstrukční	konstrukční	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
se	se	k3xPyFc4
téměř	téměř	k6eAd1
nelišil	lišit	k5eNaImAgInS
od	od	k7c2
menšího	malý	k2eAgInSc2d2
typu	typ	k1gInSc2
Laurin	Laurin	k1gInSc1
&	&	k?
Klement	Klement	k1gMnSc1
115	#num#	k4
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
však	však	k9
vybaven	vybavit	k5eAaPmNgInS
větším	veliký	k2eAgInSc7d2
a	a	k8xC
výkonnějším	výkonný	k2eAgInSc7d2
motorem	motor	k1gInSc7
<g/>
,	,	kIx,
původem	původ	k1gInSc7
z	z	k7c2
osobního	osobní	k2eAgInSc2d1
typu	typ	k1gInSc2
Laurin	Laurin	k1gInSc1
&	&	k?
Klement	Klement	k1gMnSc1
120	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vůz	vůz	k1gInSc1
se	se	k3xPyFc4
vyráběl	vyrábět	k5eAaImAgInS
v	v	k7c6
letech	léto	k1gNnPc6
1927	#num#	k4
<g/>
–	–	k?
<g/>
1929	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářská	hospodářský	k2eAgFnSc1d1
konjunktura	konjunktura	k1gFnSc1
poloviny	polovina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
znamenala	znamenat	k5eAaImAgFnS
zvýšenou	zvýšený	k2eAgFnSc4d1
poptávku	poptávka	k1gFnSc4
po	po	k7c6
lehkých	lehký	k2eAgInPc6d1
užitkových	užitkový	k2eAgInPc6d1
automobilech	automobil	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
To	ten	k3xDgNnSc1
se	se	k3xPyFc4
odrazilo	odrazit	k5eAaPmAgNnS
na	na	k7c6
poměrně	poměrně	k6eAd1
vysokém	vysoký	k2eAgInSc6d1
vyrobeném	vyrobený	k2eAgInSc6d1
počtu	počet	k1gInSc6
1650	#num#	k4
vozů	vůz	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Technické	technický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Motor	motor	k1gInSc1
a	a	k8xC
převodovka	převodovka	k1gFnSc1
</s>
<s>
Motor	motor	k1gInSc1
byl	být	k5eAaImAgInS
řadový	řadový	k2eAgInSc1d1
čtyřválec	čtyřválec	k1gInSc1
s	s	k7c7
rozvodem	rozvod	k1gInSc7
SV.	sv.	kA
Měl	mít	k5eAaImAgMnS
výkon	výkon	k1gInSc4
22	#num#	k4
kW	kW	kA
(	(	kIx(
<g/>
30	#num#	k4
koní	kůň	k1gMnPc2
<g/>
)	)	kIx)
a	a	k8xC
objem	objem	k1gInSc1
válců	válec	k1gInPc2
1944	#num#	k4
cm³	cm³	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohl	moct	k5eAaImAgMnS
jet	jet	k5eAaImF
maximálně	maximálně	k6eAd1
60	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
HOŠŤÁLEK	HOŠŤÁLEK	kA
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ŠKODA	škoda	k6eAd1
nákladní	nákladní	k2eAgInSc4d1
typ	typ	k1gInSc4
125	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
:	:	kIx,
Jihočeské	jihočeský	k2eAgNnSc1d1
motocyklové	motocyklový	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KUBA	Kuba	k1gMnSc1
<g/>
,	,	kIx,
Adolf	Adolf	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atlas	Atlas	k1gInSc1
našich	náš	k3xOp1gInPc2
automobilů	automobil	k1gInPc2
1914	#num#	k4
<g/>
-	-	kIx~
<g/>
1928	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
NADAS	NADAS	kA
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
236	#num#	k4
s.	s.	k?
S.	S.	kA
183	#num#	k4
<g/>
-	-	kIx~
<g/>
186	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
CEDRYCH	CEDRYCH	kA
<g/>
,	,	kIx,
Mario	Mario	k1gMnSc1
René	René	k1gMnSc1
<g/>
;	;	kIx,
NACHTMAN	NACHTMAN	kA
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ŠKODA	škoda	k1gFnSc1
-	-	kIx~
auta	auto	k1gNnPc4
známá	známý	k2eAgNnPc4d1
i	i	k9
neznámá	známý	k2eNgFnSc1d1
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Grada	Grada	k1gFnSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
.	.	kIx.
288	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
247	#num#	k4
<g/>
-	-	kIx~
<g/>
1719	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Laurin	Laurin	k1gInSc1
&	&	k?
Klement	Klement	k1gMnSc1
125	#num#	k4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Laurin	Laurin	k1gInSc1
&	&	k?
Klement	Klement	k1gMnSc1
125	#num#	k4
na	na	k7c4
auta	auto	k1gNnPc4
<g/>
5	#num#	k4
<g/>
p.	p.	k?
<g/>
eu	eu	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
</s>
<s>
•	•	k?
kz	kz	k?
•	•	k?
d	d	k?
•	•	k?
e	e	k0
≺	≺	k?
1905	#num#	k4
<g/>
–	–	k?
<g/>
1925	#num#	k4
–	–	k?
Automobily	automobil	k1gInPc1
značky	značka	k1gFnSc2
Škoda	škoda	k6eAd1
1925	#num#	k4
<g/>
–	–	k?
<g/>
1950	#num#	k4
–	–	k?
1950	#num#	k4
<g/>
–	–	k?
<g/>
1990	#num#	k4
≻	≻	k?
</s>
<s>
Třída	třída	k1gFnSc1
</s>
<s>
1920	#num#	k4
</s>
<s>
19301940	#num#	k4
</s>
<s>
5678901234567890123456789	#num#	k4
</s>
<s>
Malé	Malé	k2eAgInPc1d1
osobní	osobní	k2eAgInPc1d1
</s>
<s>
422	#num#	k4
</s>
<s>
418	#num#	k4
</s>
<s>
Sagitta	Sagitta	k1gFnSc1
</s>
<s>
Popular	Popular	k1gInSc1
„	„	k?
<g/>
Liduška	Liduška	k1gFnSc1
<g/>
“	“	k?
</s>
<s>
420	#num#	k4
</s>
<s>
Popular	Popular	k1gMnSc1
</s>
<s>
1101	#num#	k4
<g/>
/	/	kIx~
<g/>
1102	#num#	k4
</s>
<s>
Střední	střední	k2eAgFnSc1d1
osobní	osobní	k2eAgFnSc1d1
</s>
<s>
110	#num#	k4
</s>
<s>
4	#num#	k4
R	R	kA
</s>
<s>
Rapid	rapid	k1gInSc1
</s>
<s>
120	#num#	k4
</s>
<s>
430	#num#	k4
</s>
<s>
Favorit	favorit	k1gInSc1
</s>
<s>
6	#num#	k4
R	R	kA
</s>
<s>
633	#num#	k4
</s>
<s>
637	#num#	k4
</s>
<s>
Velké	velký	k2eAgInPc1d1
osobní	osobní	k2eAgInPc1d1
</s>
<s>
350	#num#	k4
</s>
<s>
645	#num#	k4
</s>
<s>
360	#num#	k4
</s>
<s>
650	#num#	k4
</s>
<s>
Superb	Superb	k1gMnSc1
</s>
<s>
Superb	Superb	k1gMnSc1
</s>
<s>
Luxusní	luxusní	k2eAgFnSc1d1
</s>
<s>
860	#num#	k4
</s>
<s>
Superb	Superb	k1gInSc1
4000	#num#	k4
</s>
<s>
VOS	vosa	k1gFnPc2
</s>
<s>
Sportovní	sportovní	k2eAgFnPc1d1
</s>
<s>
Popular	Popular	k1gMnSc1
Monte	Mont	k1gMnSc5
Carlo	Carla	k1gMnSc5
</s>
<s>
Vojenské	vojenský	k2eAgInPc1d1
</s>
<s>
Kfz	Kfz	k?
15	#num#	k4
</s>
<s>
903	#num#	k4
</s>
<s>
RSO	RSO	kA
</s>
<s>
Užitkové	užitkový	k2eAgNnSc1d1
do	do	k7c2
2	#num#	k4
t	t	k?
</s>
<s>
104	#num#	k4
</s>
<s>
104	#num#	k4
<g/>
/	/	kIx~
<g/>
II	II	kA
</s>
<s>
115	#num#	k4
</s>
<s>
100	#num#	k4
</s>
<s>
125	#num#	k4
</s>
<s>
154	#num#	k4
</s>
<s>
150	#num#	k4
</s>
<s>
(	(	kIx(
<g/>
Aero	aero	k1gNnSc4
<g/>
/	/	kIx~
<g/>
Praga	Praga	k1gFnSc1
150	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
206	#num#	k4
</s>
<s>
Užitkové	užitkový	k2eAgNnSc4d1
2,5	2,5	k4
–	–	k?
5	#num#	k4
t	t	k?
</s>
<s>
254	#num#	k4
</s>
<s>
256	#num#	k4
</s>
<s>
304	#num#	k4
</s>
<s>
306	#num#	k4
</s>
<s>
404	#num#	k4
</s>
<s>
406	#num#	k4
</s>
<s>
504	#num#	k4
</s>
<s>
506	#num#	k4
</s>
<s>
Užitkové	užitkový	k2eAgNnSc1d1
nad	nad	k7c4
5	#num#	k4
t	t	k?
</s>
<s>
606	#num#	k4
</s>
<s>
656	#num#	k4
</s>
<s>
706	#num#	k4
</s>
<s>
(	(	kIx(
<g/>
706	#num#	k4
R	R	kA
<g/>
)	)	kIx)
</s>
<s>
806	#num#	k4
</s>
<s>
Trolejbusy	trolejbus	k1gInPc1
</s>
<s>
1	#num#	k4
<g/>
Tr	Tr	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
Tr	Tr	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
Tr	Tr	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Automobil	automobil	k1gInSc1
</s>
