<s>
Batole	batole	k1gNnSc1	batole
je	být	k5eAaImIp3nS	být
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
větší	veliký	k2eAgNnPc1d2	veliký
než	než	k8xS	než
kojenec	kojenec	k1gMnSc1	kojenec
<g/>
,	,	kIx,	,
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
1	[number]	k4	1
až	až	k9	až
do	do	k7c2	do
3	[number]	k4	3
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
věku	věk	k1gInSc6	věk
dítě	dítě	k1gNnSc1	dítě
začíná	začínat	k5eAaImIp3nS	začínat
lézt	lézt	k5eAaImF	lézt
<g/>
,	,	kIx,	,
chodit	chodit	k5eAaImF	chodit
<g/>
,	,	kIx,	,
mluvit	mluvit	k5eAaImF	mluvit
<g/>
,	,	kIx,	,
rozeznávat	rozeznávat	k5eAaImF	rozeznávat
blízké	blízký	k2eAgFnPc1d1	blízká
osoby	osoba	k1gFnPc1	osoba
(	(	kIx(	(
<g/>
máma	máma	k1gFnSc1	máma
<g/>
,	,	kIx,	,
táta	táta	k1gMnSc1	táta
<g/>
,	,	kIx,	,
prarodiče	prarodič	k1gMnPc1	prarodič
či	či	k8xC	či
sourozenci	sourozenec	k1gMnPc1	sourozenec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
života	život	k1gInSc2	život
rozeznává	rozeznávat	k5eAaImIp3nS	rozeznávat
tvary	tvar	k1gInPc4	tvar
<g/>
,	,	kIx,	,
obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
hračky	hračka	k1gFnPc4	hračka
a	a	k8xC	a
různé	různý	k2eAgInPc4d1	různý
předměty	předmět	k1gInPc4	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemu	co	k3yInSc3	co
slouží	sloužit	k5eAaImIp3nS	sloužit
lžíce	lžíce	k1gFnPc4	lžíce
a	a	k8xC	a
podobné	podobný	k2eAgFnPc4d1	podobná
věci	věc	k1gFnPc4	věc
praktické	praktický	k2eAgFnSc2d1	praktická
denní	denní	k2eAgFnSc2d1	denní
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Začínají	začínat	k5eAaImIp3nP	začínat
mu	on	k3xPp3gInSc3	on
růst	růst	k5eAaImF	růst
první	první	k4xOgInPc4	první
zuby	zub	k1gInPc4	zub
a	a	k8xC	a
vlasy	vlas	k1gInPc4	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Batole	batole	k1gNnSc1	batole
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
pohybově	pohybově	k6eAd1	pohybově
a	a	k8xC	a
působí	působit	k5eAaImIp3nS	působit
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc1	ten
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
radost	radost	k1gFnSc4	radost
<g/>
.	.	kIx.	.
</s>
<s>
Miluje	milovat	k5eAaImIp3nS	milovat
hry	hra	k1gFnPc4	hra
s	s	k7c7	s
materiálem	materiál	k1gInSc7	materiál
(	(	kIx(	(
<g/>
písek	písek	k1gInSc1	písek
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
napodobuje	napodobovat	k5eAaImIp3nS	napodobovat
dospělé	dospělý	k2eAgNnSc1d1	dospělé
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
i	i	k9	i
prosté	prostý	k2eAgFnSc2d1	prostá
symbolické	symbolický	k2eAgFnSc2d1	symbolická
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yQgInPc6	který
ono	onen	k3xDgNnSc1	onen
samo	sám	k3xTgNnSc1	sám
je	být	k5eAaImIp3nS	být
někým	někdo	k3yInSc7	někdo
jiným	jiný	k2eAgNnPc3d1	jiné
(	(	kIx(	(
<g/>
princeznou	princezna	k1gFnSc7	princezna
<g/>
,	,	kIx,	,
koněm	kůň	k1gMnSc7	kůň
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
a	a	k8xC	a
věci	věc	k1gFnPc4	věc
se	s	k7c7	s
"	"	kIx"	"
<g/>
proměňují	proměňovat	k5eAaImIp3nP	proměňovat
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
kostka	kostka	k1gFnSc1	kostka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
stejně	stejně	k6eAd1	stejně
dobře	dobře	k6eAd1	dobře
koláčem	koláč	k1gInSc7	koláč
i	i	k8xC	i
lokomotivou	lokomotiva	k1gFnSc7	lokomotiva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rozumový	rozumový	k2eAgInSc1d1	rozumový
vývoj	vývoj	k1gInSc1	vývoj
batolete	batole	k1gNnSc2	batole
přechází	přecházet	k5eAaImIp3nS	přecházet
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
hra	hra	k1gFnSc1	hra
od	od	k7c2	od
stadia	stadion	k1gNnSc2	stadion
senzoricko-motorického	senzorickootorický	k2eAgNnSc2d1	senzoricko-motorický
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
materiálem	materiál	k1gInSc7	materiál
myšlení	myšlení	k1gNnSc2	myšlení
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
lze	lze	k6eAd1	lze
vidět	vidět	k5eAaImF	vidět
a	a	k8xC	a
nahmatat	nahmatat	k5eAaPmF	nahmatat
<g/>
,	,	kIx,	,
k	k	k7c3	k
myšlení	myšlení	k1gNnSc3	myšlení
symbolickému	symbolický	k2eAgNnSc3d1	symbolické
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
myšlení	myšlení	k1gNnSc1	myšlení
pomocí	pomocí	k7c2	pomocí
představ	představa	k1gFnPc2	představa
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
pojmů	pojem	k1gInPc2	pojem
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
řeči	řeč	k1gFnSc2	řeč
se	se	k3xPyFc4	se
prudce	prudko	k6eAd1	prudko
rozbíhá	rozbíhat	k5eAaImIp3nS	rozbíhat
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
odrazí	odrazit	k5eAaPmIp3nP	odrazit
<g/>
"	"	kIx"	"
od	od	k7c2	od
obrazných	obrazný	k2eAgNnPc2d1	obrazné
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
pes	pes	k1gMnSc1	pes
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
haf-haf	hafaf	k1gInSc1	haf-haf
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
auto	auto	k1gNnSc1	auto
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
tú-tú	túú	k?	tú-tú
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
pochopí	pochopit	k5eAaPmIp3nP	pochopit
<g/>
,	,	kIx,	,
že	že	k8xS	že
slova	slovo	k1gNnPc1	slovo
jsou	být	k5eAaImIp3nP	být
domluvené	domluvený	k2eAgInPc1d1	domluvený
symboly	symbol	k1gInPc7	symbol
(	(	kIx(	(
<g/>
znaky	znak	k1gInPc7	znak
<g/>
)	)	kIx)	)
a	a	k8xC	a
pochytí	pochytit	k5eAaPmIp3nP	pochytit
zhruba	zhruba	k6eAd1	zhruba
dvě	dva	k4xCgFnPc1	dva
nová	nový	k2eAgFnSc1d1	nová
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
již	již	k6eAd1	již
skloňuje	skloňovat	k5eAaImIp3nS	skloňovat
<g/>
,	,	kIx,	,
časuje	časovat	k5eAaBmIp3nS	časovat
<g/>
,	,	kIx,	,
sestaví	sestavit	k5eAaPmIp3nS	sestavit
větu	věta	k1gFnSc4	věta
z	z	k7c2	z
několika	několik	k4yIc2	několik
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
zvládá	zvládat	k5eAaImIp3nS	zvládat
krátké	krátký	k2eAgFnPc4d1	krátká
říkánky	říkánka	k1gFnPc4	říkánka
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
ta	ten	k3xDgNnPc4	ten
první	první	k4xOgNnPc4	první
slova	slovo	k1gNnPc4	slovo
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
ušlechtilé	ušlechtilý	k2eAgFnSc2d1	ušlechtilá
vrstvy	vrstva	k1gFnSc2	vrstva
řeči	řeč	k1gFnSc2	řeč
a	a	k8xC	a
aby	aby	k9	aby
první	první	k4xOgFnPc1	první
říkánky	říkánka	k1gFnPc1	říkánka
"	"	kIx"	"
<g/>
udaly	udat	k5eAaPmAgFnP	udat
tón	tón	k1gInSc1	tón
<g/>
"	"	kIx"	"
celé	celý	k2eAgFnSc6d1	celá
následující	následující	k2eAgFnSc6d1	následující
výchově	výchova	k1gFnSc6	výchova
–	–	k?	–
rozumové	rozumový	k2eAgFnSc6d1	rozumová
<g/>
,	,	kIx,	,
citové	citový	k2eAgFnSc6d1	citová
<g/>
,	,	kIx,	,
charakterové	charakterový	k2eAgFnSc3d1	charakterová
i	i	k8xC	i
spirituální	spirituální	k2eAgFnSc3d1	spirituální
<g/>
.	.	kIx.	.
</s>
<s>
Vlastní	vlastnit	k5eAaImIp3nS	vlastnit
Já	já	k3xPp1nSc1	já
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
stává	stávat	k5eAaImIp3nS	stávat
důležitým	důležitý	k2eAgInSc7d1	důležitý
předmětem	předmět	k1gInSc7	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Batole	batole	k1gNnSc1	batole
už	už	k9	už
pozná	poznat	k5eAaPmIp3nS	poznat
samo	sám	k3xTgNnSc1	sám
sebe	sebe	k3xPyFc4	sebe
v	v	k7c6	v
zrcadle	zrcadlo	k1gNnSc6	zrcadlo
<g/>
,	,	kIx,	,
dovede	dovést	k5eAaPmIp3nS	dovést
se	se	k3xPyFc4	se
pochválit	pochválit	k5eAaPmF	pochválit
<g/>
,	,	kIx,	,
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
bývá	bývat	k5eAaImIp3nS	bývat
zklamáno	zklamat	k5eAaPmNgNnS	zklamat
vlastním	vlastní	k2eAgInSc7d1	vlastní
nezdarem	nezdar	k1gInSc7	nezdar
<g/>
,	,	kIx,	,
záleží	záležet	k5eAaImIp3nS	záležet
mu	on	k3xPp3gMnSc3	on
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
projevilo	projevit	k5eAaPmAgNnS	projevit
svou	svůj	k3xOyFgFnSc4	svůj
samostatnost	samostatnost	k1gFnSc4	samostatnost
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
sám	sám	k3xTgMnSc1	sám
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
o	o	k7c4	o
sobě	se	k3xPyFc3	se
mluvit	mluvit	k5eAaImF	mluvit
v	v	k7c6	v
první	první	k4xOgFnSc6	první
osobě	osoba	k1gFnSc6	osoba
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
mnoho	mnoho	k6eAd1	mnoho
i	i	k9	i
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	Dítě	k1gMnPc1	Dítě
si	se	k3xPyFc3	se
postupně	postupně	k6eAd1	postupně
začíná	začínat	k5eAaImIp3nS	začínat
hrát	hrát	k5eAaImF	hrát
nejen	nejen	k6eAd1	nejen
samo	sám	k3xTgNnSc4	sám
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
vedle	vedle	k7c2	vedle
jiných	jiný	k2eAgFnPc2d1	jiná
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
společně	společně	k6eAd1	společně
s	s	k7c7	s
jinými	jiný	k2eAgFnPc7d1	jiná
dětmi	dítě	k1gFnPc7	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
pohyblivost	pohyblivost	k1gFnSc1	pohyblivost
vede	vést	k5eAaImIp3nS	vést
zároveň	zároveň	k6eAd1	zároveň
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
schopno	schopen	k2eAgNnSc1d1	schopno
mnoho	mnoho	k4c4	mnoho
věcí	věc	k1gFnPc2	věc
zničit	zničit	k5eAaPmF	zničit
a	a	k8xC	a
ublížit	ublížit	k5eAaPmF	ublížit
jak	jak	k6eAd1	jak
jiným	jiný	k2eAgFnPc3d1	jiná
dětem	dítě	k1gFnPc3	dítě
(	(	kIx(	(
<g/>
a	a	k8xC	a
zvířatům	zvíře	k1gNnPc3	zvíře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
sobě	se	k3xPyFc3	se
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
si	se	k3xPyFc3	se
vystačit	vystačit	k5eAaBmF	vystačit
už	už	k6eAd1	už
–	–	k?	–
jako	jako	k8xS	jako
u	u	k7c2	u
kojence	kojenec	k1gMnSc2	kojenec
–	–	k?	–
s	s	k7c7	s
občasným	občasný	k2eAgNnSc7d1	občasné
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ještě	ještě	k9	ještě
napůl	napůl	k6eAd1	napůl
mazlivým	mazlivý	k2eAgMnPc3d1	mazlivý
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ne-ne	Ne	k1gMnSc5	Ne-n
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Do	do	k7c2	do
života	život	k1gInSc2	život
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
řád	řád	k1gInSc1	řád
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
soustavou	soustava	k1gFnSc7	soustava
různých	různý	k2eAgFnPc2d1	různá
"	"	kIx"	"
<g/>
Musíš	muset	k5eAaImIp2nS	muset
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Nesmíš	smět	k5eNaImIp2nS	smět
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
i	i	k9	i
trest	trest	k1gInSc1	trest
<g/>
.	.	kIx.	.
</s>
<s>
Batolecí	Batolecí	k2eAgInSc1d1	Batolecí
vzdor	vzdor	k1gInSc1	vzdor
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
první	první	k4xOgNnSc4	první
negativistické	negativistický	k2eAgNnSc4d1	negativistické
stadium	stadium	k1gNnSc4	stadium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velkým	velký	k2eAgInSc7d1	velký
problémem	problém	k1gInSc7	problém
batolecí	batolecí	k2eAgFnSc2d1	batolecí
socializace	socializace	k1gFnSc2	socializace
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
řádu	řád	k1gInSc2	řád
do	do	k7c2	do
jeho	jeho	k3xOp3gInSc2	jeho
života	život	k1gInSc2	život
dítě	dítě	k1gNnSc1	dítě
objeví	objevit	k5eAaPmIp3nS	objevit
význam	význam	k1gInSc4	význam
a	a	k8xC	a
rozkoš	rozkoš	k1gFnSc4	rozkoš
uvědomělého	uvědomělý	k2eAgMnSc2d1	uvědomělý
"	"	kIx"	"
<g/>
Chci	chtít	k5eAaImIp1nS	chtít
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Nechci	chtít	k5eNaImIp1nS	chtít
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Já	já	k3xPp1nSc1	já
nechci	chtít	k5eNaImIp1nS	chtít
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Miluje	milovat	k5eAaImIp3nS	milovat
experimentování	experimentování	k1gNnSc1	experimentování
ve	v	k7c6	v
vztazích	vztah	k1gInPc6	vztah
s	s	k7c7	s
dospělými	dospělí	k1gMnPc7	dospělí
<g/>
,	,	kIx,	,
i	i	k9	i
milované	milovaný	k2eAgFnSc3d1	milovaná
mamince	maminka	k1gFnSc3	maminka
dovede	dovést	k5eAaPmIp3nS	dovést
dát	dát	k5eAaPmF	dát
někdy	někdy	k6eAd1	někdy
najevo	najevo	k6eAd1	najevo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nechci	chtít	k5eNaImIp1nS	chtít
tě	ty	k3xPp2nSc4	ty
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
samoúčelný	samoúčelný	k2eAgInSc1d1	samoúčelný
vzdor	vzdor	k1gInSc1	vzdor
kvůli	kvůli	k7c3	kvůli
vzdoru	vzdor	k1gInSc3	vzdor
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
chybují	chybovat	k5eAaImIp3nP	chybovat
rodiče	rodič	k1gMnPc1	rodič
při	při	k7c6	při
zvládání	zvládání	k1gNnSc6	zvládání
vzdoru	vzdor	k1gInSc2	vzdor
<g/>
?	?	kIx.	?
</s>
<s>
První	první	k4xOgFnSc1	první
chyba	chyba	k1gFnSc1	chyba
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
dítě	dítě	k1gNnSc1	dítě
je	být	k5eAaImIp3nS	být
rozmazlováno	rozmazlovat	k5eAaImNgNnS	rozmazlovat
přílišným	přílišný	k2eAgNnSc7d1	přílišné
ustupováním	ustupování	k1gNnSc7	ustupování
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	s	k7c7	s
surovým	surový	k2eAgNnSc7d1	surové
bitím	bití	k1gNnSc7	bití
spolu	spolu	k6eAd1	spolu
se	se	k3xPyFc4	se
vzdorem	vzdor	k1gInSc7	vzdor
láme	lámat	k5eAaImIp3nS	lámat
i	i	k9	i
dětská	dětský	k2eAgFnSc1d1	dětská
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
svobodě	svoboda	k1gFnSc6	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Dítě	dítě	k1gNnSc1	dítě
má	mít	k5eAaImIp3nS	mít
prožít	prožít	k5eAaPmF	prožít
střetnutí	střetnutí	k1gNnSc4	střetnutí
s	s	k7c7	s
vůlí	vůle	k1gFnSc7	vůle
vychovatele	vychovatel	k1gMnSc2	vychovatel
<g/>
,	,	kIx,	,
kterému	který	k3yRgNnSc3	který
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
podřídit	podřídit	k5eAaPmF	podřídit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jenž	jenž	k3xRgMnSc1	jenž
zároveň	zároveň	k6eAd1	zároveň
dokáže	dokázat	k5eAaPmIp3nS	dokázat
dát	dát	k5eAaPmF	dát
najevo	najevo	k6eAd1	najevo
porozumění	porozumění	k1gNnSc4	porozumění
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
dítě	dítě	k1gNnSc1	dítě
prožívá	prožívat	k5eAaImIp3nS	prožívat
<g/>
,	,	kIx,	,
o	o	k7c4	o
co	co	k3yRnSc4	co
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
batolecí	batolecí	k2eAgFnSc3d1	batolecí
svévoli	svévole	k1gFnSc3	svévole
má	můj	k3xOp1gFnSc1	můj
výchova	výchova	k1gFnSc1	výchova
mocného	mocný	k2eAgMnSc2d1	mocný
spojence	spojenec	k1gMnSc2	spojenec
ve	v	k7c6	v
schopnosti	schopnost	k1gFnSc6	schopnost
dítěte	dítě	k1gNnSc2	dítě
cítit	cítit	k5eAaImF	cítit
zahanbení	zahanbení	k1gNnSc4	zahanbení
<g/>
,	,	kIx,	,
zastydět	zastydět	k5eAaPmF	zastydět
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Kojenec	kojenec	k1gMnSc1	kojenec
dokázal	dokázat	k5eAaPmAgMnS	dokázat
mít	mít	k5eAaImF	mít
nanejvýš	nanejvýš	k6eAd1	nanejvýš
strach	strach	k1gInSc4	strach
z	z	k7c2	z
trestu	trest	k1gInSc2	trest
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
dostává	dostávat	k5eAaImIp3nS	dostávat
jeho	jeho	k3xOp3gFnSc2	jeho
svévole	svévole	k1gFnSc2	svévole
novou	nový	k2eAgFnSc4d1	nová
<g/>
,	,	kIx,	,
jemnější	jemný	k2eAgFnSc4d2	jemnější
a	a	k8xC	a
často	často	k6eAd1	často
účinnější	účinný	k2eAgFnSc4d2	účinnější
"	"	kIx"	"
<g/>
brzdu	brzda	k1gFnSc4	brzda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Batole	batole	k1gNnSc1	batole
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
naučit	naučit	k5eAaPmF	naučit
milovat	milovat	k5eAaImF	milovat
zároveň	zároveň	k6eAd1	zároveň
svoji	svůj	k3xOyFgFnSc4	svůj
svobodu	svoboda	k1gFnSc4	svoboda
i	i	k8xC	i
řád	řád	k1gInSc4	řád
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
rámci	rámec	k1gInSc6	rámec
tato	tento	k3xDgFnSc1	tento
svoboda	svoboda	k1gFnSc1	svoboda
platí	platit	k5eAaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
věku	věk	k1gInSc6	věk
snadno	snadno	k6eAd1	snadno
najde	najít	k5eAaPmIp3nS	najít
zalíbení	zalíbení	k1gNnSc4	zalíbení
v	v	k7c6	v
učení	učení	k1gNnSc6	učení
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
a	a	k8xC	a
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
nemá	mít	k5eNaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Pochopí	pochopit	k5eAaPmIp3nS	pochopit
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
platí	platit	k5eAaImIp3nS	platit
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
včetně	včetně	k7c2	včetně
rodičů	rodič	k1gMnPc2	rodič
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
spolehlivou	spolehlivý	k2eAgFnSc4d1	spolehlivá
oporu	opora	k1gFnSc4	opora
pro	pro	k7c4	pro
své	svůj	k3xOyFgNnSc4	svůj
jednání	jednání	k1gNnSc4	jednání
a	a	k8xC	a
prožívání	prožívání	k1gNnSc4	prožívání
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výchově	výchova	k1gFnSc6	výchova
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
se	se	k3xPyFc4	se
vyhnout	vyhnout	k5eAaPmF	vyhnout
dvěma	dva	k4xCgFnPc7	dva
úskalím	úskalí	k1gNnSc7	úskalí
<g/>
:	:	kIx,	:
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
hrozí	hrozit	k5eAaImIp3nS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
dítě	dítě	k1gNnSc1	dítě
řád	řád	k1gInSc1	řád
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
a	a	k8xC	a
naučí	naučit	k5eAaPmIp3nS	naučit
se	se	k3xPyFc4	se
poslouchat	poslouchat	k5eAaImF	poslouchat
jen	jen	k9	jen
ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
prospěch	prospěch	k1gInSc4	prospěch
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
hrozí	hrozit	k5eAaImIp3nS	hrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
zlomí	zlomit	k5eAaPmIp3nS	zlomit
vůle	vůle	k1gFnSc1	vůle
ke	k	k7c3	k
svobodě	svoboda	k1gFnSc3	svoboda
a	a	k8xC	a
že	že	k8xS	že
dítě	dítě	k1gNnSc4	dítě
na	na	k7c4	na
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
možná	možná	k9	možná
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
oloupeno	oloupit	k5eAaPmNgNnS	oloupit
o	o	k7c4	o
radost	radost	k1gFnSc4	radost
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
i	i	k8xC	i
ze	z	k7c2	z
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgNnPc1d1	průměrné
batolata	batole	k1gNnPc1	batole
nosí	nosit	k5eAaImIp3nP	nosit
stále	stále	k6eAd1	stále
pleny	plena	k1gFnPc1	plena
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgNnPc1d2	veliký
batolata	batole	k1gNnPc1	batole
již	již	k6eAd1	již
dokáží	dokázat	k5eAaPmIp3nP	dokázat
jít	jít	k5eAaImF	jít
na	na	k7c4	na
nočník	nočník	k1gInSc4	nočník
<g/>
.	.	kIx.	.
</s>
<s>
Batole	batole	k1gNnSc1	batole
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
batolit	batolit	k1gInSc1	batolit
<g/>
"	"	kIx"	"
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
kolébat	kolébat	k5eAaImF	kolébat
se	se	k3xPyFc4	se
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
chůze	chůze	k1gFnSc1	chůze
nebo	nebo	k8xC	nebo
jiná	jiný	k2eAgFnSc1d1	jiná
činnost	činnost	k1gFnSc1	činnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
útlém	útlý	k2eAgInSc6d1	útlý
věku	věk	k1gInSc6	věk
ještě	ještě	k6eAd1	ještě
poněkud	poněkud	k6eAd1	poněkud
"	"	kIx"	"
<g/>
neohrabaná	neohrabaný	k2eAgFnSc1d1	neohrabaná
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Batole	batole	k1gNnSc1	batole
je	být	k5eAaImIp3nS	být
krmeno	krmen	k2eAgNnSc1d1	krmeno
rodiči	rodič	k1gMnPc7	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Vhodná	vhodný	k2eAgFnSc1d1	vhodná
strava	strava	k1gFnSc1	strava
<g/>
:	:	kIx,	:
např.	např.	kA	např.
přesnídávky	přesnídávka	k1gFnSc2	přesnídávka
<g/>
,	,	kIx,	,
kuřecí	kuřecí	k2eAgNnSc1d1	kuřecí
nesolené	solený	k2eNgNnSc1d1	nesolené
maso	maso	k1gNnSc1	maso
<g/>
,	,	kIx,	,
bramborová	bramborový	k2eAgFnSc1d1	bramborová
kaše	kaše	k1gFnSc1	kaše
nebo	nebo	k8xC	nebo
luštěniny	luštěnina	k1gFnPc1	luštěnina
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
věku	věk	k1gInSc6	věk
je	být	k5eAaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
myslet	myslet	k5eAaImF	myslet
na	na	k7c4	na
prevenci	prevence	k1gFnSc4	prevence
obezity	obezita	k1gFnSc2	obezita
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
lze	lze	k6eAd1	lze
vysledovat	vysledovat	k5eAaImF	vysledovat
podle	podle	k7c2	podle
hraničních	hraniční	k2eAgFnPc2d1	hraniční
hodnot	hodnota	k1gFnPc2	hodnota
kombinace	kombinace	k1gFnSc2	kombinace
výšky	výška	k1gFnSc2	výška
a	a	k8xC	a
váhy	váha	k1gFnSc2	váha
<g/>
.	.	kIx.	.
</s>
<s>
Batole	batole	k1gNnSc1	batole
už	už	k6eAd1	už
většinou	většinou	k6eAd1	většinou
nechce	chtít	k5eNaImIp3nS	chtít
a	a	k8xC	a
nenosí	nosit	k5eNaImIp3nS	nosit
dudlík	dudlík	k1gInSc4	dudlík
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
jej	on	k3xPp3gMnSc4	on
však	však	k9	však
nosí	nosit	k5eAaImIp3nS	nosit
až	až	k9	až
do	do	k7c2	do
svých	svůj	k3xOyFgFnPc2	svůj
dvou	dva	k4xCgFnPc2	dva
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
třech	tři	k4xCgFnPc6	tři
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
čtyř	čtyři	k4xCgNnPc2	čtyři
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
