<s>
Ervín	Ervín	k1gMnSc1	Ervín
Kovács	Kovácsa	k1gFnPc2	Kovácsa
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1911	[number]	k4	1911
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
československý	československý	k2eAgMnSc1d1	československý
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
,	,	kIx,	,
záložník	záložník	k1gMnSc1	záložník
<g/>
,	,	kIx,	,
reprezentant	reprezentant	k1gMnSc1	reprezentant
Československa	Československo	k1gNnSc2	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
jako	jako	k9	jako
záložník	záložník	k1gMnSc1	záložník
za	za	k7c2	za
ŠK	ŠK	kA	ŠK
Bratislava	Bratislava	k1gFnSc1	Bratislava
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1937	[number]	k4	1937
až	až	k9	až
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
československou	československý	k2eAgFnSc4d1	Československá
reprezentaci	reprezentace	k1gFnSc4	reprezentace
odehrál	odehrát	k5eAaPmAgMnS	odehrát
13	[number]	k4	13
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1937	[number]	k4	1937
přátelské	přátelský	k2eAgNnSc4d1	přátelské
utkání	utkání	k1gNnSc4	utkání
s	s	k7c7	s
Lotyšskem	Lotyšsko	k1gNnSc7	Lotyšsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
skončilo	skončit	k5eAaPmAgNnS	skončit
výhrou	výhra	k1gFnSc7	výhra
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Gól	gól	k1gInSc1	gól
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
nedal	dát	k5eNaPmAgMnS	dát
<g/>
.	.	kIx.	.
</s>
