<s>
Shakespeare	Shakespeare	k1gMnSc1	Shakespeare
byl	být	k5eAaImAgMnS	být
již	již	k6eAd1	již
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
významný	významný	k2eAgMnSc1d1	významný
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
tehdy	tehdy	k6eAd1	tehdy
údajně	údajně	k6eAd1	údajně
známý	známý	k2eAgInSc1d1	známý
dokonce	dokonce	k9	dokonce
i	i	k9	i
na	na	k7c6	na
českém	český	k2eAgNnSc6d1	české
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
