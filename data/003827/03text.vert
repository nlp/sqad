<s>
Albendazol	Albendazol	k1gInSc1	Albendazol
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Albendazole	Albendazole	k1gFnSc1	Albendazole
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
širokospektrální	širokospektrální	k2eAgNnSc1d1	širokospektrální
antiparazitikum	antiparazitikum	k1gNnSc1	antiparazitikum
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
benzimidazolů	benzimidazol	k1gInPc2	benzimidazol
s	s	k7c7	s
účinkem	účinek	k1gInSc7	účinek
proti	proti	k7c3	proti
střevním	střevní	k2eAgMnPc3d1	střevní
a	a	k8xC	a
tkáňovým	tkáňový	k2eAgMnPc3d1	tkáňový
červům	červ	k1gMnPc3	červ
a	a	k8xC	a
částečným	částečný	k2eAgNnSc7d1	částečné
působením	působení	k1gNnSc7	působení
proti	proti	k7c3	proti
některým	některý	k3yIgMnPc3	některý
parazitickým	parazitický	k2eAgMnPc3d1	parazitický
prvokům	prvok	k1gMnPc3	prvok
<g/>
.	.	kIx.	.
</s>
<s>
Mechanismus	mechanismus	k1gInSc1	mechanismus
účinku	účinek	k1gInSc2	účinek
albendazolu	albendazol	k1gInSc2	albendazol
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c4	v
inhibici	inhibice	k1gFnSc4	inhibice
polymerace	polymerace	k1gFnSc2	polymerace
tubulinu	tubulin	k2eAgFnSc4d1	tubulin
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
parazitů	parazit	k1gMnPc2	parazit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
rozvratu	rozvrat	k1gInSc3	rozvrat
metabolismu	metabolismus	k1gInSc2	metabolismus
a	a	k8xC	a
následné	následný	k2eAgFnSc2d1	následná
smrti	smrt	k1gFnSc2	smrt
parazita	parazita	k1gMnSc1	parazita
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
albendazol	albendazol	k1gInSc4	albendazol
širokospektrální	širokospektrální	k2eAgNnSc1d1	širokospektrální
anthelmintikum	anthelmintikum	k1gNnSc1	anthelmintikum
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
účinný	účinný	k2eAgInSc1d1	účinný
jak	jak	k8xS	jak
proti	proti	k7c3	proti
řadě	řada	k1gFnSc3	řada
střevních	střevní	k2eAgFnPc2d1	střevní
hlístic	hlístice	k1gFnPc2	hlístice
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
některým	některý	k3yIgFnPc3	některý
tasemnicím	tasemnice	k1gFnPc3	tasemnice
a	a	k8xC	a
motolicím	motolice	k1gFnPc3	motolice
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc4	jeho
použití	použití	k1gNnSc4	použití
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
omezeno	omezit	k5eAaPmNgNnS	omezit
jen	jen	k9	jen
na	na	k7c4	na
některé	některý	k3yIgFnPc4	některý
parazitózy	parazitóza	k1gFnPc4	parazitóza
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
veterinární	veterinární	k2eAgFnSc6d1	veterinární
medicíně	medicína	k1gFnSc6	medicína
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
především	především	k9	především
k	k	k7c3	k
odčervení	odčervení	k1gNnSc3	odčervení
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
helminty	helminta	k1gFnPc4	helminta
<g/>
,	,	kIx,	,
působí	působit	k5eAaImIp3nS	působit
také	také	k9	také
proti	proti	k7c3	proti
prvokům	prvok	k1gMnPc3	prvok
Giardia	Giardium	k1gNnSc2	Giardium
intestinalis	intestinalis	k1gFnSc2	intestinalis
a	a	k8xC	a
mikrosporidiím	mikrosporidie	k1gFnPc3	mikrosporidie
<g/>
.	.	kIx.	.
</s>
<s>
Ancylostoma	Ancylostoma	k1gFnSc1	Ancylostoma
spp	spp	k?	spp
<g/>
.	.	kIx.	.
</s>
<s>
Ascaris	Ascaris	k1gFnSc1	Ascaris
lumbricoides	lumbricoidesa	k1gFnPc2	lumbricoidesa
Encephalitozoon	Encephalitozoona	k1gFnPc2	Encephalitozoona
intestinalis	intestinalis	k1gFnSc2	intestinalis
(	(	kIx(	(
<g/>
terapeuticky	terapeuticky	k6eAd1	terapeuticky
i	i	k9	i
preventivně	preventivně	k6eAd1	preventivně
proti	proti	k7c3	proti
mikrosporidióze	mikrosporidióza	k1gFnSc3	mikrosporidióza
u	u	k7c2	u
pacinetů	pacinet	k1gInPc2	pacinet
s	s	k7c7	s
AIDS	AIDS	kA	AIDS
<g/>
)	)	kIx)	)
Enterobius	Enterobius	k1gMnSc1	Enterobius
vermicularis	vermicularis	k1gFnSc2	vermicularis
Echinococcus	Echinococcus	k1gMnSc1	Echinococcus
granulosus	granulosus	k1gMnSc1	granulosus
Echinococcus	Echinococcus	k1gMnSc1	Echinococcus
multilocularis	multilocularis	k1gFnPc2	multilocularis
Hymenolepis	Hymenolepis	k1gInSc1	Hymenolepis
nana	nana	k1gMnSc1	nana
Necator	Necator	k1gMnSc1	Necator
americanus	americanus	k1gMnSc1	americanus
Strongyloides	Strongyloides	k1gInSc4	Strongyloides
stercoralis	stercoralis	k1gFnSc2	stercoralis
Taenia	Taenium	k1gNnSc2	Taenium
solium	solium	k1gNnSc1	solium
(	(	kIx(	(
<g/>
cysticerkóza	cysticerkóza	k1gFnSc1	cysticerkóza
<g/>
)	)	kIx)	)
Trichuris	Trichuris	k1gFnSc4	Trichuris
trichiura	trichiura	k1gFnSc1	trichiura
Wuchereria	Wuchererium	k1gNnSc2	Wuchererium
bancrofti	bancroft	k5eAaPmF	bancroft
všechny	všechen	k3xTgFnPc4	všechen
GIT	Gita	k1gFnPc2	Gita
hlístice	hlístice	k1gFnSc1	hlístice
(	(	kIx(	(
<g/>
Bunostomum	Bunostomum	k1gInSc1	Bunostomum
phlebotomum	phlebotomum	k1gInSc1	phlebotomum
<g/>
,	,	kIx,	,
Chabertia	Chabertia	k1gFnSc1	Chabertia
<g />
.	.	kIx.	.
</s>
<s>
ovina	ovina	k1gFnSc1	ovina
<g/>
,	,	kIx,	,
Cooperia	Cooperium	k1gNnPc1	Cooperium
oncophora	oncophora	k1gFnSc1	oncophora
<g/>
,	,	kIx,	,
C.	C.	kA	C.
punctata	punctata	k1gFnSc1	punctata
<g/>
,	,	kIx,	,
Haemonchus	Haemonchus	k1gMnSc1	Haemonchus
contortus	contortus	k1gMnSc1	contortus
<g/>
,	,	kIx,	,
H.	H.	kA	H.
placei	placei	k1gNnSc1	placei
<g/>
,	,	kIx,	,
Marshallagia	Marshallagia	k1gFnSc1	Marshallagia
marshalli	marshalle	k1gFnSc4	marshalle
<g/>
,	,	kIx,	,
Nematodirus	Nematodirus	k1gMnSc1	Nematodirus
spathiger	spathiger	k1gMnSc1	spathiger
<g/>
,	,	kIx,	,
N.	N.	kA	N.
filicollis	filicollis	k1gInSc1	filicollis
<g/>
,	,	kIx,	,
N.	N.	kA	N.
helvetianus	helvetianus	k1gInSc1	helvetianus
<g/>
,	,	kIx,	,
Oesophagostomum	Oesophagostomum	k1gInSc1	Oesophagostomum
columbianum	columbianum	k1gInSc1	columbianum
<g/>
,	,	kIx,	,
O.	O.	kA	O.
radiatum	radiatum	k1gNnSc1	radiatum
<g/>
,	,	kIx,	,
Ostertagia	Ostertagia	k1gFnSc1	Ostertagia
ostertagi	ostertag	k1gFnSc2	ostertag
<g/>
,	,	kIx,	,
Teladorsagia	Teladorsagius	k1gMnSc2	Teladorsagius
circumcinta	circumcint	k1gMnSc2	circumcint
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Trichostrongylus	Trichostrongylus	k1gInSc4	Trichostrongylus
axei	axe	k1gFnSc2	axe
<g/>
,	,	kIx,	,
T.	T.	kA	T.
colubriformis	colubriformis	k1gFnSc1	colubriformis
)	)	kIx)	)
plicní	plicní	k2eAgMnPc1d1	plicní
nematodi	nematod	k1gMnPc1	nematod
(	(	kIx(	(
<g/>
Dictyocaulus	Dictyocaulus	k1gMnSc1	Dictyocaulus
viviparus	viviparus	k1gMnSc1	viviparus
<g/>
,	,	kIx,	,
Dictyocaulus	Dictyocaulus	k1gMnSc1	Dictyocaulus
filaria	filarium	k1gNnSc2	filarium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Protostrongylidae	Protostrongylidae	k1gFnSc1	Protostrongylidae
(	(	kIx(	(
<g/>
malé	malý	k2eAgInPc1d1	malý
plicnivky	plicnivek	k1gInPc1	plicnivek
<g/>
)	)	kIx)	)
tasemnice	tasemnice	k1gFnSc1	tasemnice
Moniezia	Moniezius	k1gMnSc2	Moniezius
expansa	expans	k1gMnSc2	expans
<g/>
,	,	kIx,	,
Moniezia	Moniezius	k1gMnSc2	Moniezius
benedeni	beneden	k2eAgMnPc1d1	beneden
motolice	motolice	k1gFnPc1	motolice
Fasciola	Fasciola	k1gFnSc1	Fasciola
hepatica	hepatica	k1gFnSc1	hepatica
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
proti	proti	k7c3	proti
dospělým	dospělý	k2eAgMnPc3d1	dospělý
červům	červ	k1gMnPc3	červ
starším	starý	k2eAgMnPc3d2	starší
12	[number]	k4	12
týdnů	týden	k1gInPc2	týden
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fascioloides	Fascioloides	k1gMnSc1	Fascioloides
magna	magno	k1gNnSc2	magno
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
proti	proti	k7c3	proti
dospělým	dospělý	k2eAgNnPc3d1	dospělé
stádiím	stádium	k1gNnPc3	stádium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dicrocoelium	Dicrocoelium	k1gNnSc1	Dicrocoelium
dendriticum	dendriticum	k1gInSc1	dendriticum
Giardia	Giardium	k1gNnSc2	Giardium
intestinalis	intestinalis	k1gFnSc2	intestinalis
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
experimentálně	experimentálně	k6eAd1	experimentálně
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
komerční	komerční	k2eAgInSc4d1	komerční
přípravek	přípravek	k1gInSc4	přípravek
<g/>
)	)	kIx)	)
U	u	k7c2	u
ovcí	ovce	k1gFnPc2	ovce
<g/>
,	,	kIx,	,
myší	myš	k1gFnPc2	myš
a	a	k8xC	a
potkanů	potkan	k1gMnPc2	potkan
byl	být	k5eAaImAgInS	být
zjištěn	zjištěn	k2eAgInSc1d1	zjištěn
teratogenní	teratogenní	k2eAgInSc1d1	teratogenní
a	a	k8xC	a
embryotoxický	embryotoxický	k2eAgInSc1d1	embryotoxický
účinek	účinek	k1gInSc1	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
prokázán	prokázat	k5eAaPmNgInS	prokázat
žádný	žádný	k3yNgInSc1	žádný
kancerogenní	kancerogenní	k2eAgInSc1d1	kancerogenní
účinek	účinek	k1gInSc1	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Albendazol	Albendazol	k1gInSc1	Albendazol
je	být	k5eAaImIp3nS	být
kontraindikován	kontraindikován	k2eAgInSc1d1	kontraindikován
v	v	k7c6	v
prvních	první	k4xOgInPc6	první
30	[number]	k4	30
dnech	den	k1gInPc6	den
březosti	březost	k1gFnSc2	březost
u	u	k7c2	u
ovcí	ovce	k1gFnPc2	ovce
a	a	k8xC	a
koz	koza	k1gFnPc2	koza
a	a	k8xC	a
prvních	první	k4xOgInPc2	první
45	[number]	k4	45
dní	den	k1gInPc2	den
gravidity	gravidita	k1gFnSc2	gravidita
u	u	k7c2	u
skotu	skot	k1gInSc2	skot
<g/>
.	.	kIx.	.
</s>
<s>
Albendazol	Albendazol	k1gInSc1	Albendazol
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
podávat	podávat	k5eAaImF	podávat
ovcím	ovce	k1gFnPc3	ovce
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
mléko	mléko	k1gNnSc1	mléko
je	být	k5eAaImIp3nS	být
určeno	určit	k5eAaPmNgNnS	určit
k	k	k7c3	k
lidské	lidský	k2eAgFnSc3d1	lidská
spotřebě	spotřeba	k1gFnSc3	spotřeba
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
žen	žena	k1gFnPc2	žena
je	být	k5eAaImIp3nS	být
kontraindikován	kontraindikovat	k5eAaImNgMnS	kontraindikovat
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
těhotenství	těhotenství	k1gNnSc2	těhotenství
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nS	smět
použít	použít	k5eAaPmF	použít
u	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
s	s	k7c7	s
hypersenzitivitou	hypersenzitivita	k1gFnSc7	hypersenzitivita
na	na	k7c4	na
benzimidazoly	benzimidazola	k1gFnPc4	benzimidazola
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
doporučován	doporučovat	k5eAaImNgInS	doporučovat
u	u	k7c2	u
dětí	dítě	k1gFnPc2	dítě
do	do	k7c2	do
2	[number]	k4	2
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Albendazol	Albendazol	k1gInSc1	Albendazol
se	se	k3xPyFc4	se
metabolizuje	metabolizovat	k5eAaImIp3nS	metabolizovat
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
na	na	k7c4	na
albendazolsulfoxid	albendazolsulfoxid	k1gInSc4	albendazolsulfoxid
a	a	k8xC	a
albendazolsulfon	albendazolsulfon	k1gInSc4	albendazolsulfon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
terapie	terapie	k1gFnSc2	terapie
masných	masný	k2eAgNnPc2d1	masné
či	či	k8xC	či
mléčných	mléčný	k2eAgNnPc2d1	mléčné
zvířat	zvíře	k1gNnPc2	zvíře
mohou	moct	k5eAaImIp3nP	moct
zůstávat	zůstávat	k5eAaImF	zůstávat
rezidua	reziduum	k1gNnPc4	reziduum
albendazolu	albendazol	k1gInSc2	albendazol
v	v	k7c6	v
mase	masa	k1gFnSc6	masa
nebo	nebo	k8xC	nebo
přecházet	přecházet	k5eAaImF	přecházet
do	do	k7c2	do
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
při	při	k7c6	při
léčbě	léčba	k1gFnSc6	léčba
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
zvířat	zvíře	k1gNnPc2	zvíře
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
vždy	vždy	k6eAd1	vždy
počítat	počítat	k5eAaImF	počítat
s	s	k7c7	s
ochrannou	ochranný	k2eAgFnSc7d1	ochranná
lhůtou	lhůta	k1gFnSc7	lhůta
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
<g/>
,	,	kIx,	,
orgány	orgán	k1gInPc4	orgán
a	a	k8xC	a
mléko	mléko	k1gNnSc4	mléko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
vychází	vycházet	k5eAaImIp3nS	vycházet
legislativa	legislativa	k1gFnSc1	legislativa
pro	pro	k7c4	pro
anthelmintika	anthelmintikum	k1gNnPc4	anthelmintikum
z	z	k7c2	z
Nařízení	nařízení	k1gNnSc2	nařízení
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
Rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
ES	ES	kA	ES
<g/>
)	)	kIx)	)
č.	č.	k?	č.
470	[number]	k4	470
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
ze	z	k7c2	z
dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
kterým	který	k3yRgNnSc7	který
se	se	k3xPyFc4	se
stanoví	stanovit	k5eAaPmIp3nP	stanovit
postupy	postup	k1gInPc1	postup
Společenství	společenství	k1gNnSc2	společenství
pro	pro	k7c4	pro
stanovení	stanovení	k1gNnSc4	stanovení
limitů	limit	k1gInPc2	limit
reziduí	reziduum	k1gNnPc2	reziduum
farmakologicky	farmakologicky	k6eAd1	farmakologicky
účinných	účinný	k2eAgFnPc2d1	účinná
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
potravinách	potravina	k1gFnPc6	potravina
živočišného	živočišný	k2eAgInSc2d1	živočišný
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
udává	udávat	k5eAaImIp3nS	udávat
maximální	maximální	k2eAgInPc4d1	maximální
retenční	retenční	k2eAgInPc4d1	retenční
limity	limit	k1gInPc4	limit
(	(	kIx(	(
<g/>
MRL	MRL	kA	MRL
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
albendazol	albendazol	k1gInSc4	albendazol
100	[number]	k4	100
μ	μ	k?	μ
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
i	i	k8xC	i
mléko	mléko	k1gNnSc4	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Ochranné	ochranný	k2eAgFnPc1d1	ochranná
lhůty	lhůta	k1gFnPc1	lhůta
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vypočítávají	vypočítávat	k5eAaImIp3nP	vypočítávat
právě	právě	k9	právě
na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgFnPc2	tento
MRL	MRL	kA	MRL
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
ochranná	ochranný	k2eAgFnSc1d1	ochranná
lhůta	lhůta	k1gFnSc1	lhůta
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
u	u	k7c2	u
přípravků	přípravek	k1gInPc2	přípravek
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c4	na
výrobci	výrobce	k1gMnSc3	výrobce
a	a	k8xC	a
příslušné	příslušný	k2eAgFnSc3d1	příslušná
zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
kravské	kravský	k2eAgNnSc4d1	kravské
mléko	mléko	k1gNnSc4	mléko
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
60	[number]	k4	60
hodin	hodina	k1gFnPc2	hodina
až	až	k8xS	až
4	[number]	k4	4
dny	den	k1gInPc7	den
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
maso	maso	k1gNnSc4	maso
od	od	k7c2	od
14	[number]	k4	14
do	do	k7c2	do
30	[number]	k4	30
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
v	v	k7c6	v
humánní	humánní	k2eAgFnSc6d1	humánní
medicíně	medicína	k1gFnSc6	medicína
<g/>
:	:	kIx,	:
originální	originální	k2eAgInSc4d1	originální
přípravek	přípravek	k1gInSc4	přípravek
ZENTEL	ZENTEL	kA	ZENTEL
<g/>
®	®	k?	®
(	(	kIx(	(
<g/>
generika	generik	k1gMnSc2	generik
např.	např.	kA	např.
<g/>
:	:	kIx,	:
Alben	Albena	k1gFnPc2	Albena
<g/>
®	®	k?	®
<g/>
,	,	kIx,	,
Albenza	Albenza	k1gFnSc1	Albenza
<g/>
®	®	k?	®
<g/>
,	,	kIx,	,
Aldazon	Aldazon	k1gMnSc1	Aldazon
<g/>
®	®	k?	®
<g/>
,	,	kIx,	,
Alworm	Alworm	k1gInSc1	Alworm
<g/>
®	®	k?	®
<g/>
,	,	kIx,	,
Andazol	Andazol	k1gInSc1	Andazol
<g/>
®	®	k?	®
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Eskazole	Eskazole	k1gFnSc1	Eskazole
<g/>
®	®	k?	®
<g/>
,	,	kIx,	,
Fintel	Fintel	k1gMnSc1	Fintel
<g/>
®	®	k?	®
<g/>
,	,	kIx,	,
Zirkon	zirkon	k1gInSc1	zirkon
<g/>
®	®	k?	®
<g/>
)	)	kIx)	)
ve	v	k7c6	v
veterinární	veterinární	k2eAgFnSc6d1	veterinární
medicíně	medicína	k1gFnSc6	medicína
<g/>
:	:	kIx,	:
originální	originální	k2eAgInSc4d1	originální
přípravek	přípravek	k1gInSc4	přípravek
VALBAZEN	VALBAZEN	kA	VALBAZEN
<g/>
®	®	k?	®
(	(	kIx(	(
<g/>
generika	generik	k1gMnSc2	generik
např	např	kA	např
<g/>
:	:	kIx,	:
Albacert	Albacert	k1gMnSc1	Albacert
<g/>
®	®	k?	®
<g/>
,	,	kIx,	,
Albesure	Albesur	k1gMnSc5	Albesur
<g/>
®	®	k?	®
<g/>
,	,	kIx,	,
Albex	Albex	k1gInSc1	Albex
<g/>
®	®	k?	®
<g/>
,	,	kIx,	,
Aldifal	Aldifal	k1gFnSc1	Aldifal
<g/>
®	®	k?	®
<g/>
,	,	kIx,	,
Vermitan	Vermitan	k1gInSc1	Vermitan
<g/>
®	®	k?	®
<g/>
)	)	kIx)	)
</s>
