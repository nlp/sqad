<s>
Albendazol	Albendazol	k1gInSc1	Albendazol
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Albendazole	Albendazole	k1gFnSc1	Albendazole
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
širokospektrální	širokospektrální	k2eAgNnSc1d1	širokospektrální
antiparazitikum	antiparazitikum	k1gNnSc1	antiparazitikum
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
benzimidazolů	benzimidazol	k1gInPc2	benzimidazol
s	s	k7c7	s
účinkem	účinek	k1gInSc7	účinek
proti	proti	k7c3	proti
střevním	střevní	k2eAgMnPc3d1	střevní
a	a	k8xC	a
tkáňovým	tkáňový	k2eAgMnPc3d1	tkáňový
červům	červ	k1gMnPc3	červ
a	a	k8xC	a
částečným	částečný	k2eAgNnSc7d1	částečné
působením	působení	k1gNnSc7	působení
proti	proti	k7c3	proti
některým	některý	k3yIgMnPc3	některý
parazitickým	parazitický	k2eAgMnPc3d1	parazitický
prvokům	prvok	k1gMnPc3	prvok
<g/>
.	.	kIx.	.
</s>
