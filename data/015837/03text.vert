<s>
Hagen	Hagen	k1gInSc1
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Hagen	Hagen	k1gInSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hagen	Hagen	k1gInSc1
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Poloha	poloha	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
51	#num#	k4
<g/>
°	°	k?
<g/>
22	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
7	#num#	k4
<g/>
°	°	k?
<g/>
29	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
106	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Časové	časový	k2eAgNnSc1d1
pásmo	pásmo	k1gNnSc1
</s>
<s>
SEČ	SEČ	kA
<g/>
/	/	kIx~
<g/>
SELČ	SELČ	kA
Stát	stát	k1gInSc1
</s>
<s>
Německo	Německo	k1gNnSc1
Německo	Německo	k1gNnSc4
spolková	spolkový	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Severní	severní	k2eAgNnSc4d1
Porýní-Vestfálsko	Porýní-Vestfálsko	k1gNnSc4
vládní	vládní	k2eAgInSc1d1
obvod	obvod	k1gInSc1
</s>
<s>
Arnsberg	Arnsberg	k1gMnSc1
</s>
<s>
Rozloha	rozloha	k1gFnSc1
a	a	k8xC
obyvatelstvo	obyvatelstvo	k1gNnSc4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
160,35	160,35	k4
km²	km²	k?
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
188	#num#	k4
814	#num#	k4
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
1	#num#	k4
177,5	177,5	k4
obyv	obyva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
Starosta	Starosta	k1gMnSc1
</s>
<s>
Erik	Erik	k1gMnSc1
O.	O.	kA
Schulz	Schulz	k1gMnSc1
Oficiální	oficiální	k2eAgMnSc1d1
web	web	k1gInSc4
</s>
<s>
www.hagen.de	www.hagen.de	k6eAd1
Adresa	adresa	k1gFnSc1
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
Rathausstraße	Rathausstraße	k6eAd1
1158095	#num#	k4
Hagen	Hagno	k1gNnPc2
Telefonní	telefonní	k2eAgFnSc1d1
předvolba	předvolba	k1gFnSc1
</s>
<s>
0	#num#	k4
<g/>
2331	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
2334	#num#	k4
<g/>
,	,	kIx,
0	#num#	k4
<g/>
2337	#num#	k4
<g/>
,	,	kIx,
02304	#num#	k4
PSČ	PSČ	kA
</s>
<s>
58089-58135	58089-58135	k4
Označení	označení	k1gNnSc1
vozidel	vozidlo	k1gNnPc2
</s>
<s>
HA	ha	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hagen	Hagen	k1gInSc1
je	být	k5eAaImIp3nS
město	město	k1gNnSc4
v	v	k7c6
německé	německý	k2eAgFnSc6d1
spolkové	spolkový	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
Severní	severní	k2eAgNnSc4d1
Porýní-Vestfálsko	Porýní-Vestfálsko	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
přibližně	přibližně	k6eAd1
189	#num#	k4
tisíc	tisíc	k4xCgInPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leží	ležet	k5eAaImIp3nS
na	na	k7c6
jihovýchodním	jihovýchodní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
Porúří	Porúří	k1gNnSc2
v	v	k7c6
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
do	do	k7c2
Rúru	Rúrus	k1gInSc2
vlévají	vlévat	k5eAaImIp3nP
Lenne	Lenn	k1gMnSc5
<g/>
,	,	kIx,
Volme	volit	k5eAaImRp1nP
a	a	k8xC
Ennepe	Ennep	k1gInSc5
<g/>
,	,	kIx,
přibližně	přibližně	k6eAd1
patnáct	patnáct	k4xCc4
kilometrů	kilometr	k1gInPc2
jižně	jižně	k6eAd1
od	od	k7c2
Dortmundu	Dortmund	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Partnerská	partnerský	k2eAgNnPc1d1
města	město	k1gNnPc1
</s>
<s>
Liévin	Liévina	k1gFnPc2
(	(	kIx(
<g/>
Francie	Francie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1960	#num#	k4
</s>
<s>
Kouvola	Kouvola	k1gFnSc1
(	(	kIx(
<g/>
Finsko	Finsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1963	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
</s>
<s>
Montluçon	Montluçon	k1gNnSc1
(	(	kIx(
<g/>
Francie	Francie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1965	#num#	k4
</s>
<s>
Bezirk	Bezirk	k1gInSc1
Steglitz-Zehlendorf	Steglitz-Zehlendorf	k1gInSc1
(	(	kIx(
<g/>
Berlín	Berlín	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1967	#num#	k4
</s>
<s>
Bruck	Bruck	k1gInSc1
an	an	k?
der	drát	k5eAaImRp2nS
Mur	mur	k1gInSc1
(	(	kIx(
<g/>
Rakousko	Rakousko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1975	#num#	k4
</s>
<s>
Smolensk	Smolensk	k1gInSc1
(	(	kIx(
<g/>
Russland	Russland	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1985	#num#	k4
</s>
<s>
Modi	Modi	k6eAd1
<g/>
’	’	k?
<g/>
in	in	k?
(	(	kIx(
<g/>
Izrael	Izrael	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Hagen	Hagna	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
2	#num#	k4
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Hagen	Hagna	k1gFnPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Městské	městský	k2eAgInPc1d1
a	a	k8xC
Zemské	zemský	k2eAgInPc1d1
okresy	okres	k1gInPc1
Severního	severní	k2eAgNnSc2d1
Porýní-Vestfálska	Porýní-Vestfálsko	k1gNnSc2
Městské	městský	k2eAgInPc1d1
okresy	okres	k1gInPc1
</s>
<s>
Bielefeld	Bielefeld	k1gInSc1
•	•	k?
Bochum	Bochum	k1gInSc1
•	•	k?
Bonn	Bonn	k1gInSc1
•	•	k?
Bottrop	Bottrop	k1gInSc1
•	•	k?
Dortmund	Dortmund	k1gInSc1
•	•	k?
Duisburg	Duisburg	k1gInSc1
•	•	k?
Düsseldorf	Düsseldorf	k1gInSc1
•	•	k?
Essen	Essen	k1gInSc1
•	•	k?
Gelsenkirchen	Gelsenkirchen	k1gInSc1
•	•	k?
Hagen	Hagen	k1gInSc1
•	•	k?
Hamm	Hamm	k1gInSc1
•	•	k?
Herne	Hern	k1gInSc5
•	•	k?
Kolín	Kolín	k1gInSc1
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
•	•	k?
Krefeld	Krefeld	k1gInSc1
•	•	k?
Leverkusen	Leverkusen	k1gInSc1
•	•	k?
Mönchengladbach	Mönchengladbach	k1gInSc1
•	•	k?
Mülheim	Mülheim	k1gInSc1
an	an	k?
der	drát	k5eAaImRp2nS
Ruhr	Ruhr	k1gInSc1
•	•	k?
Münster	Münster	k1gInSc1
•	•	k?
Oberhausen	Oberhausen	k1gInSc1
•	•	k?
Remscheid	Remscheid	k1gInSc1
•	•	k?
Solingen	Solingen	k1gInSc1
•	•	k?
Wuppertal	Wuppertal	k1gFnSc2
Zemské	zemský	k2eAgFnSc2d1
okresy	okres	k1gInPc1
</s>
<s>
Městský	městský	k2eAgInSc4d1
region	region	k1gInSc4
Cáchy	Cáchy	k1gFnPc4
(	(	kIx(
<g/>
Städteregion	Städteregion	k1gInSc4
Aachen	Aachen	k2eAgInSc4d1
<g/>
)	)	kIx)
•	•	k?
Borken	Borken	k1gInSc1
•	•	k?
Coesfeld	Coesfeld	k1gInSc1
•	•	k?
Düren	Düren	k1gInSc1
•	•	k?
Ennepe-Ruhr-Kreis	Ennepe-Ruhr-Kreis	k1gInSc1
•	•	k?
Euskirchen	Euskirchen	k1gInSc1
•	•	k?
Gütersloh	Gütersloh	k1gInSc1
•	•	k?
Heinsberg	Heinsberg	k1gInSc1
•	•	k?
Herford	Herford	k1gInSc1
•	•	k?
Hochsauerlandkreis	Hochsauerlandkreis	k1gInSc1
•	•	k?
Höxter	Höxter	k1gInSc1
•	•	k?
Kleve	Kleev	k1gFnSc2
•	•	k?
Lippe	Lipp	k1gInSc5
•	•	k?
Märkischer	Märkischra	k1gFnPc2
Kreis	Kreis	k1gFnSc1
•	•	k?
Mettmann	Mettmann	k1gInSc1
•	•	k?
Minden-Lübbecke	Minden-Lübbecke	k1gInSc1
•	•	k?
Oberbergischer	Oberbergischra	k1gFnPc2
Kreis	Kreis	k1gFnSc1
•	•	k?
Olpe	Olpe	k1gInSc1
•	•	k?
Paderborn	Paderborn	k1gInSc1
•	•	k?
Recklinghausen	Recklinghausen	k1gInSc1
•	•	k?
Rhein-Erft-Kreis	Rhein-Erft-Kreis	k1gInSc1
•	•	k?
Rheinisch-Bergischer	Rheinisch-Bergischra	k1gFnPc2
Kreis	Kreis	k1gFnSc1
•	•	k?
Rhein-Kreis	Rhein-Kreis	k1gInSc1
Neuss	Neuss	k1gInSc1
•	•	k?
Rhein-Sieg-Kreis	Rhein-Sieg-Kreis	k1gInSc1
•	•	k?
Siegen-Wittgenstein	Siegen-Wittgenstein	k1gInSc1
•	•	k?
Soest	Soest	k1gInSc1
•	•	k?
Steinfurt	Steinfurt	k1gInSc1
•	•	k?
Unna	Unn	k1gInSc2
•	•	k?
Viersen	Viersen	k2eAgInSc1d1
•	•	k?
Warendorf	Warendorf	k1gInSc1
•	•	k?
Wesel	Wesel	k1gInSc1
</s>
<s>
Města	město	k1gNnPc1
v	v	k7c6
Německu	Německo	k1gNnSc6
podle	podle	k7c2
počtu	počet	k1gInSc2
obyvatel	obyvatel	k1gMnSc1
1	#num#	k4
000	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
Berlín	Berlín	k1gInSc1
•	•	k?
Hamburk	Hamburk	k1gInSc1
•	•	k?
Mnichov	Mnichov	k1gInSc1
•	•	k?
Kolín	Kolín	k1gInSc1
nad	nad	k7c7
Rýnem	Rýn	k1gInSc7
500	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
Brémy	Brémy	k1gFnPc1
•	•	k?
Dortmund	Dortmund	k1gInSc1
•	•	k?
Drážďany	Drážďany	k1gInPc1
•	•	k?
Düsseldorf	Düsseldorf	k1gInSc1
•	•	k?
Essen	Essen	k1gInSc1
•	•	k?
Frankfurt	Frankfurt	k1gInSc1
nad	nad	k7c7
Mohanem	Mohan	k1gInSc7
•	•	k?
Hannover	Hannover	k1gInSc1
•	•	k?
Lipsko	Lipsko	k1gNnSc4
•	•	k?
Stuttgart	Stuttgart	k1gInSc1
200	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
Augsburg	Augsburg	k1gMnSc1
•	•	k?
Bielefeld	Bielefeld	k1gMnSc1
•	•	k?
Bochum	Bochum	k1gInSc1
•	•	k?
Bonn	Bonn	k1gInSc1
•	•	k?
Braunschweig	Braunschweig	k1gInSc1
•	•	k?
Cáchy	Cáchy	k1gFnPc4
•	•	k?
Duisburg	Duisburg	k1gInSc1
•	•	k?
Erfurt	Erfurt	k1gInSc1
•	•	k?
Freiburg	Freiburg	k1gInSc1
im	im	k?
Breisgau	Breisgaus	k1gInSc2
•	•	k?
Gelsenkirchen	Gelsenkirchen	k2eAgInSc1d1
•	•	k?
Halle	Halle	k1gInSc1
(	(	kIx(
<g/>
Saale	Saale	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Karlsruhe	Karlsruhe	k1gInSc1
•	•	k?
Kiel	Kiel	k1gInSc1
•	•	k?
Krefeld	Krefeld	k1gInSc1
•	•	k?
Lübeck	Lübeck	k1gInSc1
•	•	k?
Magdeburg	Magdeburg	k1gInSc1
•	•	k?
Mannheim	Mannheim	k1gInSc1
•	•	k?
Münster	Münster	k1gInSc1
•	•	k?
Mohuč	Mohuč	k1gFnSc1
•	•	k?
Mönchengladbach	Mönchengladbach	k1gInSc1
•	•	k?
Norimberk	Norimberk	k1gInSc1
•	•	k?
Oberhausen	Oberhausen	k1gInSc1
•	•	k?
Saská	saský	k2eAgFnSc1d1
Kamenice	Kamenice	k1gFnSc1
•	•	k?
Rostock	Rostock	k1gInSc1
•	•	k?
Wiesbaden	Wiesbaden	k1gInSc1
•	•	k?
Wuppertal	Wuppertal	k1gFnSc2
100	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
+	+	kIx~
</s>
<s>
Bergisch	Bergisch	k1gMnSc1
Gladbach	Gladbach	k1gMnSc1
•	•	k?
Bottrop	Bottrop	k1gInSc1
•	•	k?
Bremerhaven	Bremerhaven	k2eAgInSc1d1
•	•	k?
Darmstadt	Darmstadt	k1gInSc1
•	•	k?
Erlangen	Erlangen	k1gInSc1
•	•	k?
Fürth	Fürth	k1gInSc1
•	•	k?
Göttingen	Göttingen	k1gInSc1
•	•	k?
Hagen	Hagen	k1gInSc1
•	•	k?
Hamm	Hamm	k1gInSc1
•	•	k?
Heidelberg	Heidelberg	k1gInSc1
•	•	k?
Heilbronn	Heilbronn	k1gInSc1
•	•	k?
Herne	Hern	k1gInSc5
•	•	k?
Ingolstadt	Ingolstadt	k1gInSc1
•	•	k?
Jena	Jen	k1gInSc2
•	•	k?
Kassel	Kassel	k1gInSc1
•	•	k?
Koblenz	Koblenz	k1gInSc1
•	•	k?
Leverkusen	Leverkusen	k1gInSc1
•	•	k?
Ludwigshafen	Ludwigshafen	k1gInSc1
•	•	k?
Moers	Moers	k1gInSc1
•	•	k?
Mülheim	Mülheim	k1gInSc1
an	an	k?
der	drát	k5eAaImRp2nS
Ruhr	Ruhr	k1gInSc1
•	•	k?
Neuss	Neuss	k1gInSc1
•	•	k?
Offenbach	Offenbach	k1gInSc1
am	am	k?
Main	Main	k1gInSc1
•	•	k?
Oldenburg	Oldenburg	k1gInSc1
•	•	k?
Osnabrück	Osnabrück	k1gInSc1
•	•	k?
Paderborn	Paderborn	k1gInSc1
•	•	k?
Pforzheim	Pforzheim	k1gInSc1
•	•	k?
Postupim	Postupim	k1gFnSc1
•	•	k?
Recklinghausen	Recklinghausen	k1gInSc1
•	•	k?
Remscheid	Remscheid	k1gInSc1
•	•	k?
Reutlingen	Reutlingen	k1gInSc1
•	•	k?
Řezno	Řezno	k1gNnSc4
•	•	k?
Saarbrücken	Saarbrücken	k1gInSc1
•	•	k?
Salzgitter	Salzgitter	k1gInSc1
•	•	k?
Solingen	Solingen	k1gInSc1
•	•	k?
Trevír	Trevír	k1gInSc1
•	•	k?
Ulm	Ulm	k1gFnSc2
•	•	k?
Wolfsburg	Wolfsburg	k1gInSc1
•	•	k?
Würzburg	Würzburg	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
36495	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4022917-8	4022917-8	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79046412	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
143053076	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79046412	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Německo	Německo	k1gNnSc1
</s>
