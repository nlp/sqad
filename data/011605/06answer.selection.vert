<s>
Osoba	osoba	k1gFnSc1	osoba
samostatně	samostatně	k6eAd1	samostatně
výdělečně	výdělečně	k6eAd1	výdělečně
činná	činný	k2eAgFnSc1d1	činná
(	(	kIx(	(
<g/>
OSVČ	OSVČ	kA	OSVČ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc1	termín
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
českých	český	k2eAgInPc6d1	český
zákonech	zákon	k1gInPc6	zákon
o	o	k7c6	o
dani	daň	k1gFnSc6	daň
z	z	k7c2	z
příjmů	příjem	k1gInPc2	příjem
<g/>
,	,	kIx,	,
o	o	k7c6	o
sociálním	sociální	k2eAgNnSc6d1	sociální
zabezpečení	zabezpečení	k1gNnSc6	zabezpečení
a	a	k8xC	a
zdravotním	zdravotní	k2eAgNnSc6d1	zdravotní
pojištění	pojištění	k1gNnSc6	pojištění
pro	pro	k7c4	pro
takovou	takový	k3xDgFnSc4	takový
fyzickou	fyzický	k2eAgFnSc4d1	fyzická
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
příjmy	příjem	k1gInPc4	příjem
z	z	k7c2	z
podnikání	podnikání	k1gNnSc2	podnikání
nebo	nebo	k8xC	nebo
z	z	k7c2	z
jiné	jiný	k2eAgFnSc2d1	jiná
samostatně	samostatně	k6eAd1	samostatně
výdělečné	výdělečný	k2eAgFnSc2d1	výdělečná
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
