<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
samostatně	samostatně	k6eAd1	samostatně
výdělečně	výdělečně	k6eAd1	výdělečně
činná	činný	k2eAgFnSc1d1	činná
(	(	kIx(	(
<g/>
OSVČ	OSVČ	kA	OSVČ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc1	termín
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
českých	český	k2eAgInPc6d1	český
zákonech	zákon	k1gInPc6	zákon
o	o	k7c6	o
dani	daň	k1gFnSc6	daň
z	z	k7c2	z
příjmů	příjem	k1gInPc2	příjem
<g/>
,	,	kIx,	,
o	o	k7c6	o
sociálním	sociální	k2eAgNnSc6d1	sociální
zabezpečení	zabezpečení	k1gNnSc6	zabezpečení
a	a	k8xC	a
zdravotním	zdravotní	k2eAgNnSc6d1	zdravotní
pojištění	pojištění	k1gNnSc6	pojištění
pro	pro	k7c4	pro
takovou	takový	k3xDgFnSc4	takový
fyzickou	fyzický	k2eAgFnSc4d1	fyzická
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
příjmy	příjem	k1gInPc4	příjem
z	z	k7c2	z
podnikání	podnikání	k1gNnSc2	podnikání
nebo	nebo	k8xC	nebo
z	z	k7c2	z
jiné	jiný	k2eAgFnSc2d1	jiná
samostatně	samostatně	k6eAd1	samostatně
výdělečné	výdělečný	k2eAgFnSc2d1	výdělečná
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Typicky	typicky	k6eAd1	typicky
je	být	k5eAaImIp3nS	být
OSVČ	OSVČ	kA	OSVČ
například	například	k6eAd1	například
živnostník	živnostník	k1gMnSc1	živnostník
<g/>
,	,	kIx,	,
samostatný	samostatný	k2eAgMnSc1d1	samostatný
zemědělec	zemědělec	k1gMnSc1	zemědělec
<g/>
,	,	kIx,	,
samostatně	samostatně	k6eAd1	samostatně
výdělečný	výdělečný	k2eAgMnSc1d1	výdělečný
umělec	umělec	k1gMnSc1	umělec
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgMnSc1d1	soudní
znalec	znalec	k1gMnSc1	znalec
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osoba	osoba	k1gFnSc1	osoba
samostatně	samostatně	k6eAd1	samostatně
výdělečně	výdělečně	k6eAd1	výdělečně
činná	činný	k2eAgFnSc1d1	činná
==	==	k?	==
</s>
</p>
<p>
<s>
Za	za	k7c2	za
OSVČ	OSVČ	kA	OSVČ
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
ten	ten	k3xDgMnSc1	ten
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
kdo	kdo	k3yInSc1	kdo
vykonává	vykonávat	k5eAaImIp3nS	vykonávat
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
výdělečnou	výdělečný	k2eAgFnSc4d1	výdělečná
činnost	činnost	k1gFnSc4	činnost
na	na	k7c6	na
základě	základ	k1gInSc6	základ
oprávnění	oprávnění	k1gNnSc2	oprávnění
k	k	k7c3	k
výkonu	výkon	k1gInSc3	výkon
takové	takový	k3xDgFnSc2	takový
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
plyne	plynout	k5eAaImIp3nS	plynout
z	z	k7c2	z
českých	český	k2eAgInPc2d1	český
právních	právní	k2eAgInPc2d1	právní
předpisů	předpis	k1gInPc2	předpis
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
nebo	nebo	k8xC	nebo
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
při	při	k7c6	při
výkonu	výkon	k1gInSc6	výkon
takové	takový	k3xDgFnSc2	takový
samostatné	samostatný	k2eAgFnSc2d1	samostatná
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
patří	patřit	k5eAaImIp3nP	patřit
také	také	k9	také
členové	člen	k1gMnPc1	člen
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
zúčastňují	zúčastňovat	k5eAaImIp3nP	zúčastňovat
na	na	k7c6	na
provozu	provoz	k1gInSc6	provoz
rodinného	rodinný	k2eAgInSc2d1	rodinný
závodu	závod	k1gInSc2	závod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
OSVČ	OSVČ	kA	OSVČ
zároveň	zároveň	k6eAd1	zároveň
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ukončila	ukončit	k5eAaPmAgFnS	ukončit
povinnou	povinný	k2eAgFnSc4d1	povinná
školní	školní	k2eAgFnSc4d1	školní
docházku	docházka	k1gFnSc4	docházka
a	a	k8xC	a
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
věku	věk	k1gInSc6	věk
aspoň	aspoň	k9	aspoň
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nesplňuje	splňovat	k5eNaImIp3nS	splňovat
zmíněné	zmíněný	k2eAgFnPc4d1	zmíněná
podmínky	podmínka	k1gFnPc4	podmínka
nelze	lze	k6eNd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
OSVČ	OSVČ	kA	OSVČ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příjmy	příjem	k1gInPc1	příjem
OSVČ	OSVČ	kA	OSVČ
==	==	k?	==
</s>
</p>
<p>
<s>
Příjmy	příjem	k1gInPc1	příjem
z	z	k7c2	z
podnikání	podnikání	k1gNnSc2	podnikání
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
účel	účel	k1gInSc4	účel
podle	podle	k7c2	podle
§	§	k?	§
7	[number]	k4	7
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
1	[number]	k4	1
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
586	[number]	k4	586
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
o	o	k7c6	o
dani	daň	k1gFnSc6	daň
z	z	k7c2	z
příjmů	příjem	k1gInPc2	příjem
<g/>
,	,	kIx,	,
rozumí	rozumět	k5eAaImIp3nS	rozumět
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
příjmy	příjem	k1gInPc1	příjem
ze	z	k7c2	z
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
výroby	výroba	k1gFnSc2	výroba
<g/>
,	,	kIx,	,
lesního	lesní	k2eAgMnSc2d1	lesní
a	a	k8xC	a
vodního	vodní	k2eAgNnSc2d1	vodní
hospodářství	hospodářství	k1gNnSc2	hospodářství
</s>
</p>
<p>
<s>
příjmy	příjem	k1gInPc1	příjem
ze	z	k7c2	z
živnosti	živnost	k1gFnSc2	živnost
</s>
</p>
<p>
<s>
příjmy	příjem	k1gInPc1	příjem
z	z	k7c2	z
jiného	jiný	k2eAgNnSc2d1	jiné
podnikání	podnikání	k1gNnSc2	podnikání
podle	podle	k7c2	podle
zvláštních	zvláštní	k2eAgInPc2d1	zvláštní
předpisů	předpis	k1gInPc2	předpis
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
podíly	podíl	k1gInPc4	podíl
společníků	společník	k1gMnPc2	společník
veřejné	veřejný	k2eAgFnSc2d1	veřejná
obchodní	obchodní	k2eAgFnSc2d1	obchodní
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
komplementářů	komplementář	k1gMnPc2	komplementář
komanditní	komanditní	k2eAgFnSc2d1	komanditní
společnosti	společnost	k1gFnSc2	společnost
na	na	k7c6	na
zisku	zisk	k1gInSc6	zisk
<g/>
.	.	kIx.	.
<g/>
Příjmy	příjem	k1gInPc4	příjem
z	z	k7c2	z
jiné	jiný	k2eAgFnSc2d1	jiná
samostatné	samostatný	k2eAgFnSc2d1	samostatná
výdělečné	výdělečný	k2eAgFnSc2d1	výdělečná
činnosti	činnost	k1gFnSc2	činnost
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
§	§	k?	§
7	[number]	k4	7
odst	odsta	k1gFnPc2	odsta
<g/>
.	.	kIx.	.
2	[number]	k4	2
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
586	[number]	k4	586
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
rozumí	rozumět	k5eAaImIp3nS	rozumět
(	(	kIx(	(
<g/>
nespadají	spadat	k5eNaImIp3nP	spadat
<g/>
-li	i	k?	-li
mezi	mezi	k7c4	mezi
příjmy	příjem	k1gInPc4	příjem
ze	z	k7c2	z
závislé	závislý	k2eAgFnSc2d1	závislá
činnosti	činnost	k1gFnSc2	činnost
a	a	k8xC	a
funkční	funkční	k2eAgInPc1d1	funkční
požitky	požitek	k1gInPc1	požitek
podle	podle	k7c2	podle
§	§	k?	§
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
příjmy	příjem	k1gInPc1	příjem
z	z	k7c2	z
užití	užití	k1gNnSc2	užití
nebo	nebo	k8xC	nebo
poskytnutí	poskytnutí	k1gNnSc6	poskytnutí
práv	právo	k1gNnPc2	právo
z	z	k7c2	z
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
nebo	nebo	k8xC	nebo
jiného	jiný	k2eAgNnSc2d1	jiné
duševního	duševní	k2eAgNnSc2d1	duševní
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
<g/>
,	,	kIx,	,
autorských	autorský	k2eAgNnPc2d1	autorské
práv	právo	k1gNnPc2	právo
včetně	včetně	k7c2	včetně
práv	právo	k1gNnPc2	právo
příbuzných	příbuzný	k1gMnPc2	příbuzný
právu	právo	k1gNnSc3	právo
autorskému	autorský	k2eAgNnSc3d1	autorské
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
příjmů	příjem	k1gInPc2	příjem
z	z	k7c2	z
vydávání	vydávání	k1gNnSc2	vydávání
<g/>
,	,	kIx,	,
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
a	a	k8xC	a
rozšiřování	rozšiřování	k1gNnSc1	rozšiřování
literárních	literární	k2eAgNnPc2d1	literární
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
děl	dělo	k1gNnPc2	dělo
vlastním	vlastnit	k5eAaImIp1nS	vlastnit
nákladem	náklad	k1gInSc7	náklad
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
příjmy	příjem	k1gInPc1	příjem
z	z	k7c2	z
výkonu	výkon	k1gInSc2	výkon
nezávislého	závislý	k2eNgNnSc2d1	nezávislé
povolání	povolání	k1gNnSc2	povolání
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
není	být	k5eNaImIp3nS	být
živností	živnost	k1gFnSc7	živnost
ani	ani	k8xC	ani
podnikáním	podnikání	k1gNnSc7	podnikání
podle	podle	k7c2	podle
zvláštních	zvláštní	k2eAgInPc2d1	zvláštní
předpisů	předpis	k1gInPc2	předpis
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
příjmy	příjem	k1gInPc1	příjem
znalce	znalec	k1gMnSc2	znalec
<g/>
,	,	kIx,	,
tlumočníka	tlumočník	k1gMnSc2	tlumočník
<g/>
,	,	kIx,	,
zprostředkovatele	zprostředkovatel	k1gMnSc2	zprostředkovatel
kolektivních	kolektivní	k2eAgInPc2d1	kolektivní
sporů	spor	k1gInPc2	spor
<g/>
,	,	kIx,	,
zprostředkovatele	zprostředkovatel	k1gMnPc4	zprostředkovatel
kolektivních	kolektivní	k2eAgFnPc2d1	kolektivní
a	a	k8xC	a
hromadných	hromadný	k2eAgFnPc2d1	hromadná
smluv	smlouva	k1gFnPc2	smlouva
podle	podle	k7c2	podle
autorského	autorský	k2eAgInSc2d1	autorský
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
rozhodce	rozhodce	k1gMnSc2	rozhodce
za	za	k7c4	za
činnost	činnost	k1gFnSc4	činnost
podle	podle	k7c2	podle
zvláštních	zvláštní	k2eAgInPc2d1	zvláštní
právních	právní	k2eAgInPc2d1	právní
předpisů	předpis	k1gInPc2	předpis
</s>
</p>
<p>
<s>
příjmy	příjem	k1gInPc1	příjem
z	z	k7c2	z
činnosti	činnost	k1gFnSc2	činnost
insolvenčního	insolvenční	k2eAgMnSc2d1	insolvenční
správce	správce	k1gMnSc2	správce
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
příjmů	příjem	k1gInPc2	příjem
z	z	k7c2	z
činnosti	činnost	k1gFnSc2	činnost
předběžného	předběžný	k2eAgMnSc2d1	předběžný
insolvenčního	insolvenční	k2eAgMnSc2d1	insolvenční
správce	správce	k1gMnSc2	správce
<g/>
,	,	kIx,	,
zástupce	zástupce	k1gMnSc2	zástupce
insolvenčního	insolvenční	k2eAgMnSc2d1	insolvenční
správce	správce	k1gMnSc2	správce
<g/>
,	,	kIx,	,
odděleného	oddělený	k2eAgMnSc2d1	oddělený
insolvenčního	insolvenční	k2eAgMnSc2d1	insolvenční
správce	správce	k1gMnSc2	správce
a	a	k8xC	a
zvláštního	zvláštní	k2eAgMnSc2d1	zvláštní
insolvenčního	insolvenční	k2eAgMnSc2d1	insolvenční
správce	správce	k1gMnSc2	správce
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
živností	živnost	k1gFnSc7	živnost
ani	ani	k8xC	ani
podnikáním	podnikání	k1gNnSc7	podnikání
podle	podle	k7c2	podle
zvláštního	zvláštní	k2eAgInSc2d1	zvláštní
právního	právní	k2eAgInSc2d1	právní
předpisu	předpis	k1gInSc2	předpis
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Práva	právo	k1gNnPc4	právo
a	a	k8xC	a
povinnosti	povinnost	k1gFnPc4	povinnost
==	==	k?	==
</s>
</p>
<p>
<s>
OSVČ	OSVČ	kA	OSVČ
má	mít	k5eAaImIp3nS	mít
tato	tento	k3xDgNnPc4	tento
práva	právo	k1gNnPc4	právo
a	a	k8xC	a
povinnosti	povinnost	k1gFnPc4	povinnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
podniká	podnikat	k5eAaImIp3nS	podnikat
pod	pod	k7c7	pod
svým	svůj	k3xOyFgNnSc7	svůj
jménem	jméno	k1gNnSc7	jméno
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Josef	Josef	k1gMnSc1	Josef
Novák	Novák	k1gMnSc1	Novák
-	-	kIx~	-
účetní	účetní	k1gMnSc1	účetní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
ručí	ručit	k5eAaImIp3nS	ručit
celým	celý	k2eAgInSc7d1	celý
svým	svůj	k3xOyFgInSc7	svůj
majetkem	majetek	k1gInSc7	majetek
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
do	do	k7c2	do
určitého	určitý	k2eAgInSc2d1	určitý
obratu	obrat	k1gInSc2	obrat
(	(	kIx(	(
<g/>
dáno	dát	k5eAaPmNgNnS	dát
zákonem	zákon	k1gInSc7	zákon
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
účtovat	účtovat	k5eAaImF	účtovat
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
daňové	daňový	k2eAgFnSc2d1	daňová
evidence	evidence	k1gFnSc2	evidence
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
jednoduchého	jednoduchý	k2eAgNnSc2d1	jednoduché
účetnictví	účetnictví	k1gNnSc2	účetnictví
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
do	do	k7c2	do
určitého	určitý	k2eAgInSc2d1	určitý
obratu	obrat	k1gInSc2	obrat
(	(	kIx(	(
<g/>
dáno	dát	k5eAaPmNgNnS	dát
zákonem	zákon	k1gInSc7	zákon
<g/>
)	)	kIx)	)
nemusí	muset	k5eNaImIp3nS	muset
prokazovat	prokazovat	k5eAaImF	prokazovat
náklady	náklad	k1gInPc4	náklad
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
je	on	k3xPp3gNnSc4	on
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
paušálně	paušálně	k6eAd1	paušálně
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
do	do	k7c2	do
určitého	určitý	k2eAgInSc2d1	určitý
obratu	obrat	k1gInSc2	obrat
(	(	kIx(	(
<g/>
dáno	dát	k5eAaPmNgNnS	dát
zákonem	zákon	k1gInSc7	zákon
<g/>
)	)	kIx)	)
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
plátcem	plátce	k1gMnSc7	plátce
daně	daň	k1gFnSc2	daň
z	z	k7c2	z
přidané	přidaný	k2eAgFnSc2d1	přidaná
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
právnických	právnický	k2eAgFnPc2d1	právnická
osob	osoba	k1gFnPc2	osoba
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
zapsána	zapsat	k5eAaPmNgFnS	zapsat
v	v	k7c6	v
Obchodním	obchodní	k2eAgInSc6d1	obchodní
rejstříku	rejstřík	k1gInSc6	rejstřík
<g/>
,	,	kIx,	,
<g/>
má	mít	k5eAaImIp3nS	mít
povinnost	povinnost	k1gFnSc1	povinnost
platit	platit	k5eAaImF	platit
zdravotní	zdravotní	k2eAgInPc4d1	zdravotní
(	(	kIx(	(
<g/>
do	do	k7c2	do
8	[number]	k4	8
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
následujícího	následující	k2eAgInSc2d1	následující
kalendářního	kalendářní	k2eAgInSc2d1	kalendářní
měsíce	měsíc	k1gInSc2	měsíc
<g/>
)	)	kIx)	)
<g/>
a	a	k8xC	a
sociální	sociální	k2eAgNnSc4d1	sociální
pojištění	pojištění	k1gNnSc4	pojištění
(	(	kIx(	(
<g/>
do	do	k7c2	do
konce	konec	k1gInSc2	konec
měsíce	měsíc	k1gInSc2	měsíc
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
platí	platit	k5eAaImIp3nS	platit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
OSVČ	OSVČ	kA	OSVČ
účastní	účastnit	k5eAaImIp3nS	účastnit
na	na	k7c6	na
nemocenském	nemocenský	k2eAgNnSc6d1	nemocenský
pojištění	pojištění	k1gNnSc6	pojištění
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
povinnost	povinnost	k1gFnSc4	povinnost
platit	platit	k5eAaImF	platit
také	také	k9	také
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
má	mít	k5eAaImIp3nS	mít
povinnost	povinnost	k1gFnSc4	povinnost
podat	podat	k5eAaPmF	podat
přehled	přehled	k1gInSc4	přehled
o	o	k7c6	o
příjmech	příjem	k1gInPc6	příjem
a	a	k8xC	a
výdajích	výdaj	k1gInPc6	výdaj
za	za	k7c4	za
kalendářní	kalendářní	k2eAgInSc4d1	kalendářní
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
má	mít	k5eAaImIp3nS	mít
povinnost	povinnost	k1gFnSc4	povinnost
oznámit	oznámit	k5eAaPmF	oznámit
příslušným	příslušný	k2eAgInPc3d1	příslušný
úřadům	úřad	k1gInPc3	úřad
zahájení	zahájení	k1gNnSc2	zahájení
<g/>
,	,	kIx,	,
popř.	popř.	kA	popř.
znovuzahájení	znovuzahájení	k1gNnSc2	znovuzahájení
<g/>
,	,	kIx,	,
samostatné	samostatný	k2eAgFnSc2d1	samostatná
výdělečné	výdělečný	k2eAgFnSc2d1	výdělečná
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
má	můj	k3xOp1gFnSc1	můj
povinnost	povinnost	k1gFnSc1	povinnost
oznámit	oznámit	k5eAaPmF	oznámit
příslušným	příslušný	k2eAgMnPc3d1	příslušný
úřadům	úřada	k1gMnPc3	úřada
ukončení	ukončení	k1gNnSc4	ukončení
samostatné	samostatný	k2eAgFnSc2d1	samostatná
výdělečné	výdělečný	k2eAgFnSc2d1	výdělečná
činnosti	činnost	k1gFnSc2	činnost
nebo	nebo	k8xC	nebo
jakoukoliv	jakýkoliv	k3yIgFnSc4	jakýkoliv
její	její	k3xOp3gNnSc1	její
změnu	změna	k1gFnSc4	změna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Změna	změna	k1gFnSc1	změna
statusu	status	k1gInSc2	status
OSVČ	OSVČ	kA	OSVČ
===	===	k?	===
</s>
</p>
<p>
<s>
OSVČ	OSVČ	kA	OSVČ
může	moct	k5eAaImIp3nS	moct
kdykoliv	kdykoliv	k6eAd1	kdykoliv
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
ukončit	ukončit	k5eAaPmF	ukončit
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
pozastavit	pozastavit	k5eAaPmF	pozastavit
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vždy	vždy	k6eAd1	vždy
má	mít	k5eAaImIp3nS	mít
povinnost	povinnost	k1gFnSc4	povinnost
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
informovat	informovat	k5eAaBmF	informovat
příslušné	příslušný	k2eAgInPc4d1	příslušný
orgány	orgán	k1gInPc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
jí	on	k3xPp3gFnSc3	on
hrozí	hrozit	k5eAaImIp3nS	hrozit
pokuta	pokuta	k1gFnSc1	pokuta
a	a	k8xC	a
také	také	k9	také
povinnost	povinnost	k1gFnSc4	povinnost
nadále	nadále	k6eAd1	nadále
hradit	hradit	k5eAaImF	hradit
zálohy	záloha	k1gFnPc4	záloha
na	na	k7c6	na
pojistném	pojistné	k1gNnSc6	pojistné
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pouhém	pouhý	k2eAgNnSc6d1	pouhé
pozastavení	pozastavení	k1gNnSc6	pozastavení
podnikatelské	podnikatelský	k2eAgFnSc2d1	podnikatelská
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
jejího	její	k3xOp3gNnSc2	její
ukončení	ukončení	k1gNnSc2	ukončení
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
jedinci	jedinec	k1gMnPc1	jedinec
povinnost	povinnost	k1gFnSc4	povinnost
platit	platit	k5eAaImF	platit
zálohy	záloha	k1gFnPc4	záloha
na	na	k7c4	na
zdravotním	zdravotní	k2eAgNnSc7d1	zdravotní
pojištěním	pojištění	k1gNnSc7	pojištění
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
pak	pak	k6eAd1	pak
podnikání	podnikání	k1gNnSc4	podnikání
kdykoliv	kdykoliv	k6eAd1	kdykoliv
obnovit	obnovit	k5eAaPmF	obnovit
tzv.	tzv.	kA	tzv.
změnovým	změnový	k2eAgInSc7d1	změnový
listem	list	k1gInSc7	list
na	na	k7c6	na
živnostenském	živnostenský	k2eAgInSc6d1	živnostenský
úřadě	úřad	k1gInSc6	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
podnikatelská	podnikatelský	k2eAgFnSc1d1	podnikatelská
činnost	činnost	k1gFnSc1	činnost
pouze	pouze	k6eAd1	pouze
pozastavena	pozastaven	k2eAgFnSc1d1	pozastavena
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nezapočítává	započítávat	k5eNaImIp3nS	započítávat
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
platné	platný	k2eAgFnSc2d1	platná
pro	pro	k7c4	pro
výpočet	výpočet	k1gInSc4	výpočet
důchodu	důchod	k1gInSc2	důchod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Změnu	změna	k1gFnSc4	změna
činnosti	činnost	k1gFnSc2	činnost
má	mít	k5eAaImIp3nS	mít
podnikatel	podnikatel	k1gMnSc1	podnikatel
povinnost	povinnost	k1gFnSc4	povinnost
oznámit	oznámit	k5eAaPmF	oznámit
na	na	k7c6	na
okresní	okresní	k2eAgFnSc6d1	okresní
správě	správa	k1gFnSc6	správa
sociálního	sociální	k2eAgNnSc2d1	sociální
zabezpečení	zabezpečení	k1gNnSc2	zabezpečení
(	(	kIx(	(
<g/>
OSSZ	OSSZ	kA	OSSZ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
zdravotní	zdravotní	k2eAgFnSc6d1	zdravotní
pojišťovně	pojišťovna	k1gFnSc6	pojišťovna
a	a	k8xC	a
na	na	k7c6	na
finančním	finanční	k2eAgInSc6d1	finanční
úřadě	úřad	k1gInSc6	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Časově	časově	k6eAd1	časově
výhodnější	výhodný	k2eAgFnSc1d2	výhodnější
je	být	k5eAaImIp3nS	být
však	však	k9	však
tuto	tento	k3xDgFnSc4	tento
změnu	změna	k1gFnSc4	změna
nahlásit	nahlásit	k5eAaPmF	nahlásit
v	v	k7c6	v
živnostenském	živnostenský	k2eAgInSc6d1	živnostenský
úřadě	úřad	k1gInSc6	úřad
nebo	nebo	k8xC	nebo
v	v	k7c6	v
Czech	Cze	k1gFnPc6	Cze
POINT	pointa	k1gFnPc2	pointa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
osoba	osoba	k1gFnSc1	osoba
vyplní	vyplnit	k5eAaPmIp3nS	vyplnit
pouze	pouze	k6eAd1	pouze
tzv.	tzv.	kA	tzv.
změnový	změnový	k2eAgInSc1d1	změnový
list	list	k1gInSc1	list
na	na	k7c6	na
centrálním	centrální	k2eAgNnSc6d1	centrální
registračním	registrační	k2eAgNnSc6d1	registrační
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
skrze	skrze	k?	skrze
který	který	k3yRgInSc4	který
budou	být	k5eAaImBp3nP	být
následné	následný	k2eAgFnPc1d1	následná
informovány	informován	k2eAgFnPc1d1	informována
všechny	všechen	k3xTgFnPc1	všechen
zmíněné	zmíněný	k2eAgFnPc1d1	zmíněná
instituce	instituce	k1gFnPc1	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Změnu	změna	k1gFnSc4	změna
podnikatel	podnikatel	k1gMnSc1	podnikatel
musí	muset	k5eAaImIp3nS	muset
nahlásit	nahlásit	k5eAaPmF	nahlásit
do	do	k7c2	do
osmi	osm	k4xCc2	osm
dnů	den	k1gInPc2	den
v	v	k7c6	v
OSSZ	OSSZ	kA	OSSZ
a	a	k8xC	a
zdravotní	zdravotní	k2eAgFnSc3d1	zdravotní
pojišťovně	pojišťovna	k1gFnSc3	pojišťovna
a	a	k8xC	a
do	do	k7c2	do
15	[number]	k4	15
dnů	den	k1gInPc2	den
na	na	k7c4	na
finančního	finanční	k2eAgMnSc4d1	finanční
úřadě	úřad	k1gInSc6	úřad
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
mu	on	k3xPp3gMnSc3	on
hrozí	hrozit	k5eAaImIp3nS	hrozit
pokuta	pokuta	k1gFnSc1	pokuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
a	a	k8xC	a
vedlejší	vedlejší	k2eAgFnSc4d1	vedlejší
činnost	činnost	k1gFnSc4	činnost
==	==	k?	==
</s>
</p>
<p>
<s>
OSVČ	OSVČ	kA	OSVČ
může	moct	k5eAaImIp3nS	moct
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
vykonávat	vykonávat	k5eAaImF	vykonávat
jako	jako	k9	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vykonávání	vykonávání	k1gNnSc6	vykonávání
hlavní	hlavní	k2eAgFnSc2d1	hlavní
činnosti	činnost	k1gFnSc2	činnost
je	být	k5eAaImIp3nS	být
povinna	povinen	k2eAgFnSc1d1	povinna
hradit	hradit	k5eAaImF	hradit
alespoň	alespoň	k9	alespoň
minimální	minimální	k2eAgFnPc4d1	minimální
zálohy	záloha	k1gFnPc4	záloha
na	na	k7c4	na
zdravotní	zdravotní	k2eAgNnSc4d1	zdravotní
a	a	k8xC	a
sociální	sociální	k2eAgNnSc4d1	sociální
pojištění	pojištění	k1gNnSc4	pojištění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
činí	činit	k5eAaImIp3nS	činit
1	[number]	k4	1
752	[number]	k4	752
Kč	Kč	kA	Kč
na	na	k7c4	na
zdravotní	zdravotní	k2eAgNnSc4d1	zdravotní
pojištění	pojištění	k1gNnSc4	pojištění
a	a	k8xC	a
1	[number]	k4	1
894	[number]	k4	894
Kč	Kč	kA	Kč
na	na	k7c4	na
sociální	sociální	k2eAgNnSc4d1	sociální
pojištění	pojištění	k1gNnSc4	pojištění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
vedlejší	vedlejší	k2eAgFnSc2d1	vedlejší
činnosti	činnost	k1gFnSc2	činnost
se	se	k3xPyFc4	se
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
roce	rok	k1gInSc6	rok
zálohy	záloha	k1gFnSc2	záloha
neplatí	platit	k5eNaImIp3nP	platit
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
vypočtené	vypočtený	k2eAgFnSc6d1	vypočtená
ze	z	k7c2	z
skutečného	skutečný	k2eAgInSc2d1	skutečný
zisku	zisk	k1gInSc2	zisk
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
vedlejší	vedlejší	k2eAgFnSc4d1	vedlejší
činnost	činnost	k1gFnSc4	činnost
jde	jít	k5eAaImIp3nS	jít
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
osoba	osoba	k1gFnSc1	osoba
samostatně	samostatně	k6eAd1	samostatně
výdělečně	výdělečně	k6eAd1	výdělečně
činná	činný	k2eAgFnSc1d1	činná
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Vykonává	vykonávat	k5eAaImIp3nS	vykonávat
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
</s>
</p>
<p>
<s>
Pobírá	pobírat	k5eAaImIp3nS	pobírat
invalidní	invalidní	k2eAgNnSc1d1	invalidní
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
starobní	starobní	k2eAgInSc4d1	starobní
důchod	důchod	k1gInSc4	důchod
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
nárok	nárok	k1gInSc4	nárok
na	na	k7c4	na
rodičovský	rodičovský	k2eAgInSc4d1	rodičovský
příspěvek	příspěvek	k1gInSc4	příspěvek
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Peněžitou	peněžitý	k2eAgFnSc4d1	peněžitá
pomoc	pomoc	k1gFnSc4	pomoc
v	v	k7c6	v
mateřství	mateřství	k1gNnSc6	mateřství
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
nemocenské	nemocenské	k1gNnSc4	nemocenské
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
těhotenství	těhotenství	k1gNnSc2	těhotenství
a	a	k8xC	a
porodu	porod	k1gInSc2	porod
</s>
</p>
<p>
<s>
Osobně	osobně	k6eAd1	osobně
pečuje	pečovat	k5eAaImIp3nS	pečovat
o	o	k7c4	o
osobu	osoba	k1gFnSc4	osoba
mladší	mladý	k2eAgMnPc1d2	mladší
10	[number]	k4	10
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c4	na
péči	péče	k1gFnSc4	péče
jiné	jiný	k2eAgFnSc2d1	jiná
osoby	osoba	k1gFnSc2	osoba
</s>
</p>
<p>
<s>
Osobně	osobně	k6eAd1	osobně
pečuje	pečovat	k5eAaImIp3nS	pečovat
o	o	k7c4	o
osobu	osoba	k1gFnSc4	osoba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c4	na
péči	péče	k1gFnSc4	péče
jiné	jiný	k2eAgFnSc2d1	jiná
osoby	osoba	k1gFnSc2	osoba
ve	v	k7c6	v
stupni	stupeň	k1gInSc6	stupeň
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
osobu	osoba	k1gFnSc4	osoba
blízkou	blízký	k2eAgFnSc7d1	blízká
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
žijící	žijící	k2eAgMnPc1d1	žijící
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
domácnosti	domácnost	k1gFnSc6	domácnost
</s>
</p>
<p>
<s>
Vykonává	vykonávat	k5eAaImIp3nS	vykonávat
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
službu	služba	k1gFnSc4	služba
<g/>
,	,	kIx,	,
nejde	jít	k5eNaImIp3nS	jít
<g/>
-li	i	k?	-li
o	o	k7c4	o
vojáka	voják	k1gMnSc4	voják
z	z	k7c2	z
povolání	povolání	k1gNnSc2	povolání
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
nezaopatřeným	zaopatřený	k2eNgNnSc7d1	nezaopatřené
dítětem	dítě	k1gNnSc7	dítě
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Podnikatel	podnikatel	k1gMnSc1	podnikatel
</s>
</p>
<p>
<s>
Svobodné	svobodný	k2eAgNnSc1d1	svobodné
povolání	povolání	k1gNnSc1	povolání
</s>
</p>
<p>
<s>
Zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
bez	bez	k7c2	bez
zdanitelných	zdanitelný	k2eAgInPc2d1	zdanitelný
příjmů	příjem	k1gInPc2	příjem
</s>
</p>
