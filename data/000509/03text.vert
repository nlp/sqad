<s>
Petr	Petr	k1gMnSc1	Petr
Fiala	Fiala	k1gMnSc1	Fiala
(	(	kIx(	(
<g/>
*	*	kIx~	*
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1964	[number]	k4	1964
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
politolog	politolog	k1gMnSc1	politolog
<g/>
,	,	kIx,	,
univerzitní	univerzitní	k2eAgMnSc1d1	univerzitní
profesor	profesor	k1gMnSc1	profesor
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
zastává	zastávat	k5eAaImIp3nS	zastávat
poslanecký	poslanecký	k2eAgInSc4d1	poslanecký
mandát	mandát	k1gInSc4	mandát
v	v	k7c6	v
Poslanecké	poslanecký	k2eAgFnSc6d1	Poslanecká
sněmovně	sněmovna	k1gFnSc6	sněmovna
PČR	PČR	kA	PČR
<g/>
,	,	kIx,	,
do	do	k7c2	do
níž	jenž	k3xRgFnSc2	jenž
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
v	v	k7c6	v
předčasných	předčasný	k2eAgFnPc6d1	předčasná
volbách	volba	k1gFnPc6	volba
jako	jako	k8xS	jako
nestraník	nestraník	k1gMnSc1	nestraník
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
místa	místo	k1gNnSc2	místo
jihomoravské	jihomoravský	k2eAgFnSc2d1	Jihomoravská
kandidátky	kandidátka	k1gFnSc2	kandidátka
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
</s>
<s>
Členem	člen	k1gInSc7	člen
této	tento	k3xDgFnSc2	tento
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
stal	stát	k5eAaPmAgInS	stát
7	[number]	k4	7
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
a	a	k8xC	a
na	na	k7c4	na
24	[number]	k4	24
<g/>
.	.	kIx.	.
kongresu	kongres	k1gInSc2	kongres
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
reflektoval	reflektovat	k5eAaImAgInS	reflektovat
drtivou	drtivý	k2eAgFnSc4d1	drtivá
volební	volební	k2eAgFnSc4d1	volební
porážku	porážka	k1gFnSc4	porážka
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
zvolen	zvolit	k5eAaPmNgMnS	zvolit
čtvrtým	čtvrtý	k4xOgNnSc7	čtvrtý
předsedou	předseda	k1gMnSc7	předseda
Občanské	občanský	k2eAgFnSc2d1	občanská
demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
2	[number]	k4	2
<g/>
.	.	kIx.	.
květnem	květen	k1gInSc7	květen
2012	[number]	k4	2012
až	až	k8xS	až
10	[number]	k4	10
<g/>
.	.	kIx.	.
červencem	červenec	k1gInSc7	červenec
2013	[number]	k4	2013
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
ministr	ministr	k1gMnSc1	ministr
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
Nečasova	Nečasův	k2eAgInSc2d1	Nečasův
kabinetu	kabinet	k1gInSc2	kabinet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
akademickém	akademický	k2eAgNnSc6d1	akademické
prostředí	prostředí	k1gNnSc6	prostředí
byl	být	k5eAaImAgInS	být
rektorem	rektor	k1gMnSc7	rektor
a	a	k8xC	a
prorektorem	prorektor	k1gMnSc7	prorektor
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1983	[number]	k4	1983
<g/>
-	-	kIx~	-
<g/>
1988	[number]	k4	1988
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
český	český	k2eAgInSc1d1	český
jazyk	jazyk	k1gInSc4	jazyk
a	a	k8xC	a
literaturu	literatura	k1gFnSc4	literatura
a	a	k8xC	a
dějepis	dějepis	k1gInSc4	dějepis
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
Univerzita	univerzita	k1gFnSc1	univerzita
Jana	Jan	k1gMnSc2	Jan
Evangelisty	evangelista	k1gMnSc2	evangelista
Purkyně	Purkyně	k1gFnSc1	Purkyně
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
historik	historik	k1gMnSc1	historik
starších	starý	k2eAgFnPc2d2	starší
dějin	dějiny	k1gFnPc2	dějiny
v	v	k7c6	v
Muzeu	muzeum	k1gNnSc6	muzeum
Kroměřížska	Kroměřížsk	k1gInSc2	Kroměřížsk
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
(	(	kIx(	(
<g/>
1988	[number]	k4	1988
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Brna	Brno	k1gNnSc2	Brno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
krátce	krátce	k6eAd1	krátce
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
novinář	novinář	k1gMnSc1	novinář
v	v	k7c6	v
deníku	deník	k1gInSc6	deník
Lidová	lidový	k2eAgFnSc1d1	lidová
demokracie	demokracie	k1gFnSc1	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
redakce	redakce	k1gFnSc2	redakce
těchto	tento	k3xDgFnPc2	tento
novin	novina	k1gFnPc2	novina
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
1990	[number]	k4	1990
zástupcem	zástupce	k1gMnSc7	zástupce
šéfredaktora	šéfredaktor	k1gMnSc2	šéfredaktor
Revue	revue	k1gFnSc2	revue
Proglas	Proglasa	k1gFnPc2	Proglasa
<g/>
.	.	kIx.	.
</s>
<s>
Dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
předseda	předseda	k1gMnSc1	předseda
moravského	moravský	k2eAgInSc2d1	moravský
zemského	zemský	k2eAgInSc2d1	zemský
výboru	výbor	k1gInSc2	výbor
české	český	k2eAgFnSc2d1	Česká
sekce	sekce	k1gFnSc2	sekce
Panevropské	panevropský	k2eAgFnSc2d1	panevropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
Paneuropa-Union	Paneuropa-Union	k1gInSc1	Paneuropa-Union
Böhmen	Böhmen	k2eAgInSc1d1	Böhmen
und	und	k?	und
Mähren	Mährno	k1gNnPc2	Mährno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
založení	založení	k1gNnSc6	založení
oboru	obor	k1gInSc2	obor
politologie	politologie	k1gFnSc2	politologie
<g/>
,	,	kIx,	,
s	s	k7c7	s
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Čermákem	Čermák	k1gMnSc7	Čermák
zakládal	zakládat	k5eAaImAgMnS	zakládat
Katedru	katedra	k1gFnSc4	katedra
politologie	politologie	k1gFnSc2	politologie
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
MU	MU	kA	MU
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
vedení	vedení	k1gNnSc1	vedení
po	po	k7c6	po
něm	on	k3xPp3gNnSc6	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
převzal	převzít	k5eAaPmAgMnS	převzít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
habilitoval	habilitovat	k5eAaBmAgMnS	habilitovat
na	na	k7c6	na
Univerzitě	univerzita	k1gFnSc6	univerzita
Karlově	Karlův	k2eAgFnSc6d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
prvním	první	k4xOgMnSc7	první
profesorem	profesor	k1gMnSc7	profesor
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
politologie	politologie	k1gFnSc2	politologie
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Katedru	katedra	k1gFnSc4	katedra
politologie	politologie	k1gFnSc2	politologie
vedl	vést	k5eAaImAgMnS	vést
deset	deset	k4xCc1	deset
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
na	na	k7c6	na
nově	nova	k1gFnSc6	nova
založené	založený	k2eAgFnSc3d1	založená
Fakultě	fakulta	k1gFnSc3	fakulta
sociálních	sociální	k2eAgFnPc2d1	sociální
studií	studie	k1gFnPc2	studie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
také	také	k9	také
ředitelem	ředitel	k1gMnSc7	ředitel
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
politologického	politologický	k2eAgInSc2d1	politologický
ústavu	ústav	k1gInSc2	ústav
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
úzce	úzko	k6eAd1	úzko
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
významnými	významný	k2eAgFnPc7d1	významná
osobnostmi	osobnost	k1gFnPc7	osobnost
československého	československý	k2eAgInSc2d1	československý
exilu	exil	k1gInSc2	exil
Mojmírem	Mojmír	k1gMnSc7	Mojmír
Povolným	Povolný	k1gMnSc7	Povolný
a	a	k8xC	a
Ivanem	Ivan	k1gMnSc7	Ivan
Gaďourkem	Gaďourek	k1gMnSc7	Gaďourek
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
stáli	stát	k5eAaImAgMnP	stát
u	u	k7c2	u
jeho	jeho	k3xOp3gInSc2	jeho
vzniku	vznik	k1gInSc2	vznik
<g/>
.	.	kIx.	.
</s>
<s>
Podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
založení	založení	k1gNnSc4	založení
prvního	první	k4xOgNnSc2	první
českého	český	k2eAgNnSc2d1	české
odborného	odborný	k2eAgNnSc2d1	odborné
politologického	politologický	k2eAgNnSc2d1	politologické
periodika	periodikum	k1gNnSc2	periodikum
-	-	kIx~	-
Politologického	politologický	k2eAgInSc2d1	politologický
časopisu	časopis	k1gInSc2	časopis
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
vedoucím	vedoucí	k1gMnSc7	vedoucí
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
Katedry	katedra	k1gFnSc2	katedra
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
vztahů	vztah	k1gInPc2	vztah
a	a	k8xC	a
evropských	evropský	k2eAgFnPc2d1	Evropská
studií	studie	k1gFnPc2	studie
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
Jean	Jean	k1gMnSc1	Jean
Monnet	Monnet	k1gMnSc1	Monnet
Chair	Chair	k1gMnSc1	Chair
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2005	[number]	k4	2005
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
byl	být	k5eAaImAgMnS	být
vedoucím	vedoucí	k1gMnSc7	vedoucí
Institutu	institut	k1gInSc2	institut
pro	pro	k7c4	pro
srovnávací	srovnávací	k2eAgInSc4d1	srovnávací
politologický	politologický	k2eAgInSc4d1	politologický
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
děkanem	děkan	k1gMnSc7	děkan
Fakulty	fakulta	k1gFnSc2	fakulta
sociálních	sociální	k2eAgNnPc2d1	sociální
studií	studio	k1gNnPc2	studio
a	a	k8xC	a
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
rektorem	rektor	k1gMnSc7	rektor
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Rektorem	rektor	k1gMnSc7	rektor
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
byl	být	k5eAaImAgInS	být
dvě	dva	k4xCgNnPc1	dva
funkční	funkční	k2eAgNnPc1d1	funkční
období	období	k1gNnPc1	období
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
výrazný	výrazný	k2eAgInSc4d1	výrazný
kvantitativní	kvantitativní	k2eAgInSc4d1	kvantitativní
i	i	k8xC	i
kvalitativní	kvalitativní	k2eAgInSc4d1	kvalitativní
vzestup	vzestup	k1gInSc4	vzestup
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
významnou	významný	k2eAgFnSc7d1	významná
středoevropskou	středoevropský	k2eAgFnSc7d1	středoevropská
vzdělávací	vzdělávací	k2eAgFnSc7d1	vzdělávací
a	a	k8xC	a
výzkumnou	výzkumný	k2eAgFnSc7d1	výzkumná
institucí	instituce	k1gFnSc7	instituce
(	(	kIx(	(
<g/>
45	[number]	k4	45
tis	tis	k1gInSc1	tis
<g/>
.	.	kIx.	.
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
více	hodně	k6eAd2	hodně
<g />
.	.	kIx.	.
</s>
<s>
než	než	k8xS	než
14	[number]	k4	14
%	%	kIx~	%
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejžádanější	žádaný	k2eAgFnSc1d3	nejžádanější
českou	český	k2eAgFnSc7d1	Česká
vysokou	vysoký	k2eAgFnSc7d1	vysoká
školou	škola	k1gFnSc7	škola
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
zájmu	zájem	k1gInSc2	zájem
uchazečů	uchazeč	k1gMnPc2	uchazeč
<g/>
,	,	kIx,	,
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
celostátní	celostátní	k2eAgInSc4d1	celostátní
systém	systém	k1gInSc4	systém
na	na	k7c4	na
odhalování	odhalování	k1gNnSc4	odhalování
plagiátů	plagiát	k1gInPc2	plagiát
apod.	apod.	kA	apod.
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
Masarykova	Masarykův	k2eAgFnSc1d1	Masarykova
univerzita	univerzita	k1gFnSc1	univerzita
mj.	mj.	kA	mj.
postavila	postavit	k5eAaPmAgFnS	postavit
nový	nový	k2eAgInSc4d1	nový
kampus	kampus	k1gInSc4	kampus
pro	pro	k7c4	pro
biomedicínské	biomedicínský	k2eAgInPc4d1	biomedicínský
obory	obor	k1gInPc4	obor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
staveb	stavba	k1gFnPc2	stavba
a	a	k8xC	a
jednorázových	jednorázový	k2eAgFnPc2d1	jednorázová
investic	investice	k1gFnPc2	investice
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
220	[number]	k4	220
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
€	€	k?	€
<g/>
)	)	kIx)	)
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vysokého	vysoký	k2eAgNnSc2d1	vysoké
školství	školství	k1gNnSc2	školství
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
otevřela	otevřít	k5eAaPmAgFnS	otevřít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
výzkumnou	výzkumný	k2eAgFnSc4d1	výzkumná
stanici	stanice	k1gFnSc4	stanice
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
,	,	kIx,	,
koordinovala	koordinovat	k5eAaBmAgFnS	koordinovat
projekt	projekt	k1gInSc4	projekt
Středoevropského	středoevropský	k2eAgInSc2d1	středoevropský
technologického	technologický	k2eAgInSc2d1	technologický
institutu	institut	k1gInSc2	institut
(	(	kIx(	(
<g/>
CEITEC	CEITEC	kA	CEITEC
<g/>
)	)	kIx)	)
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
5,3	[number]	k4	5,3
mld.	mld.	k?	mld.
Kč	Kč	kA	Kč
z	z	k7c2	z
prostředků	prostředek	k1gInPc2	prostředek
evropských	evropský	k2eAgInPc2d1	evropský
strukturálních	strukturální	k2eAgInPc2d1	strukturální
fondů	fond	k1gInPc2	fond
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
rektorského	rektorský	k2eAgInSc2d1	rektorský
mandátu	mandát	k1gInSc2	mandát
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xC	jako
prorektor	prorektor	k1gMnSc1	prorektor
pro	pro	k7c4	pro
akademické	akademický	k2eAgFnPc4d1	akademická
záležitosti	záležitost	k1gFnPc4	záležitost
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
od	od	k7c2	od
září	září	k1gNnSc2	září
2011	[number]	k4	2011
byl	být	k5eAaImAgMnS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
vědeckým	vědecký	k2eAgMnSc7d1	vědecký
poradcem	poradce	k1gMnSc7	poradce
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
ministrem	ministr	k1gMnSc7	ministr
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
mládeže	mládež	k1gFnSc2	mládež
a	a	k8xC	a
tělovýchovy	tělovýchova	k1gFnSc2	tělovýchova
ve	v	k7c6	v
vládě	vláda	k1gFnSc6	vláda
Petra	Petr	k1gMnSc2	Petr
Nečase	Nečas	k1gMnSc2	Nečas
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
PČR	PČR	kA	PČR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
v	v	k7c6	v
Jihomoravském	jihomoravský	k2eAgInSc6d1	jihomoravský
kraji	kraj	k1gInSc6	kraj
jako	jako	k9	jako
lídr	lídr	k1gMnSc1	lídr
ODS	ODS	kA	ODS
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
se	s	k7c7	s
ziskem	zisk	k1gInSc7	zisk
devátého	devátý	k4xOgInSc2	devátý
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
počtu	počet	k1gInSc2	počet
preferenčních	preferenční	k2eAgInPc2d1	preferenční
hlasů	hlas	k1gInPc2	hlas
(	(	kIx(	(
<g/>
11	[number]	k4	11
372	[number]	k4	372
<g/>
)	)	kIx)	)
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
místopředsedou	místopředseda	k1gMnSc7	místopředseda
poslaneckého	poslanecký	k2eAgInSc2d1	poslanecký
klubu	klub	k1gInSc2	klub
ODS	ODS	kA	ODS
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
24	[number]	k4	24
<g/>
.	.	kIx.	.
kongresu	kongres	k1gInSc2	kongres
ODS	ODS	kA	ODS
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
byl	být	k5eAaImAgInS	být
18	[number]	k4	18
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
zvolen	zvolit	k5eAaPmNgMnS	zvolit
předsedou	předseda	k1gMnSc7	předseda
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
již	již	k6eAd1	již
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
kole	kolo	k1gNnSc6	kolo
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
získal	získat	k5eAaPmAgMnS	získat
hlasy	hlas	k1gInPc4	hlas
437	[number]	k4	437
delegátů	delegát	k1gMnPc2	delegát
a	a	k8xC	a
porazil	porazit	k5eAaPmAgMnS	porazit
tak	tak	k6eAd1	tak
Miroslavu	Miroslava	k1gFnSc4	Miroslava
Němcovou	Němcová	k1gFnSc4	Němcová
a	a	k8xC	a
Edvarda	Edvard	k1gMnSc4	Edvard
Kožušníka	Kožušník	k1gMnSc4	Kožušník
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
obhájil	obhájit	k5eAaPmAgInS	obhájit
na	na	k7c4	na
27	[number]	k4	27
<g/>
.	.	kIx.	.
kongresu	kongres	k1gInSc2	kongres
ODS	ODS	kA	ODS
v	v	k7c6	v
Ostravě	Ostrava	k1gFnSc6	Ostrava
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2016	[number]	k4	2016
získal	získat	k5eAaPmAgInS	získat
436	[number]	k4	436
hlasů	hlas	k1gInPc2	hlas
od	od	k7c2	od
470	[number]	k4	470
delagátů	delagát	k1gInPc2	delagát
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
93	[number]	k4	93
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
funkci	funkce	k1gFnSc6	funkce
předsedy	předseda	k1gMnSc2	předseda
strany	strana	k1gFnSc2	strana
byl	být	k5eAaImAgMnS	být
jediným	jediný	k2eAgMnSc7d1	jediný
kandidátem	kandidát	k1gMnSc7	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
vědecké	vědecký	k2eAgFnSc6d1	vědecká
práci	práce	k1gFnSc6	práce
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
srovnávací	srovnávací	k2eAgFnSc4d1	srovnávací
politologii	politologie	k1gFnSc4	politologie
a	a	k8xC	a
evropskou	evropský	k2eAgFnSc4d1	Evropská
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Publikace	publikace	k1gFnPc4	publikace
věnované	věnovaný	k2eAgFnSc3d1	věnovaná
problematice	problematika	k1gFnSc3	problematika
evropské	evropský	k2eAgFnSc2d1	Evropská
integrace	integrace	k1gFnSc2	integrace
významným	významný	k2eAgInSc7d1	významný
způsobem	způsob	k1gInSc7	způsob
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
i	i	k8xC	i
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
debatu	debata	k1gFnSc4	debata
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Monografie	monografie	k1gFnSc1	monografie
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
s	s	k7c7	s
M.	M.	kA	M.
Pitrovou	Pitrová	k1gFnSc7	Pitrová
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
standardním	standardní	k2eAgFnPc3d1	standardní
pracím	práce	k1gFnPc3	práce
oboru	obor	k1gInSc2	obor
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
také	také	k9	také
jako	jako	k9	jako
učebnice	učebnice	k1gFnPc4	učebnice
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
a	a	k8xC	a
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
k	k	k7c3	k
jeho	jeho	k3xOp3gFnPc3	jeho
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
knihám	kniha	k1gFnPc3	kniha
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
patří	patřit	k5eAaImIp3nS	patřit
Evropský	evropský	k2eAgInSc1d1	evropský
mezičas	mezičas	k1gInSc1	mezičas
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
Politický	politický	k2eAgInSc1d1	politický
extremismus	extremismus	k1gInSc4	extremismus
a	a	k8xC	a
radikalismus	radikalismus	k1gInSc4	radikalismus
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
uspořádal	uspořádat	k5eAaPmAgInS	uspořádat
<g/>
,	,	kIx,	,
stála	stát	k5eAaImAgFnS	stát
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
vědeckého	vědecký	k2eAgInSc2d1	vědecký
výzkumu	výzkum	k1gInSc2	výzkum
tohoto	tento	k3xDgInSc2	tento
fenoménu	fenomén	k1gInSc2	fenomén
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Značného	značný	k2eAgInSc2d1	značný
(	(	kIx(	(
<g/>
i	i	k8xC	i
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
<g/>
)	)	kIx)	)
ohlasu	ohlas	k1gInSc2	ohlas
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
jeho	jeho	k3xOp3gNnSc7	jeho
pracím	prací	k2eAgNnSc7d1	prací
věnovaným	věnovaný	k2eAgNnSc7d1	věnované
politické	politický	k2eAgFnSc3d1	politická
dimenzi	dimenze	k1gFnSc3	dimenze
náboženství	náboženství	k1gNnSc2	náboženství
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Katolicismus	katolicismus	k1gInSc1	katolicismus
a	a	k8xC	a
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
;	;	kIx,	;
Laboratoř	laboratoř	k1gFnSc1	laboratoř
sekularizace	sekularizace	k1gFnSc1	sekularizace
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
historický	historický	k2eAgInSc1d1	historický
a	a	k8xC	a
sociálně-vědným	sociálněědné	k1gNnSc7	sociálně-vědné
výzkum	výzkum	k1gInSc1	výzkum
podzemních	podzemní	k2eAgFnPc2d1	podzemní
církevních	církevní	k2eAgFnPc2d1	církevní
aktivit	aktivita	k1gFnPc2	aktivita
v	v	k7c6	v
období	období	k1gNnSc6	období
komunismu	komunismus	k1gInSc2	komunismus
(	(	kIx(	(
<g/>
s	s	k7c7	s
J.	J.	kA	J.
Hanušem	Hanuš	k1gMnSc7	Hanuš
<g/>
)	)	kIx)	)
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
monografie	monografie	k1gFnSc1	monografie
Skrytá	skrytý	k2eAgFnSc1d1	skrytá
církev	církev	k1gFnSc1	církev
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
Die	Die	k1gFnSc1	Die
Verborgene	Verborgen	k1gInSc5	Verborgen
Kirche	Kirche	k1gNnPc7	Kirche
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgFnPc1d1	významná
jsou	být	k5eAaImIp3nP	být
jeho	jeho	k3xOp3gFnPc1	jeho
práce	práce	k1gFnPc1	práce
věnované	věnovaný	k2eAgFnPc1d1	věnovaná
politickým	politický	k2eAgFnPc3d1	politická
stranám	strana	k1gFnPc3	strana
a	a	k8xC	a
zájmovým	zájmový	k2eAgFnPc3d1	zájmová
skupinám	skupina	k1gFnPc3	skupina
<g/>
,	,	kIx,	,
k	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
Teorie	teorie	k1gFnSc1	teorie
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
(	(	kIx(	(
<g/>
s	s	k7c7	s
M.	M.	kA	M.
Strmiskou	Strmiska	k1gFnSc7	Strmiska
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
autor	autor	k1gMnSc1	autor
a	a	k8xC	a
editor	editor	k1gInSc1	editor
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
mj.	mj.	kA	mj.
na	na	k7c6	na
monografiích	monografie	k1gFnPc6	monografie
Politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Partie	partie	k1gFnPc4	partie
i	i	k8xC	i
systemy	systema	k1gFnPc4	systema
partyjne	partyjnout	k5eAaImIp3nS	partyjnout
Europy	Europ	k1gInPc4	Europ
Środkowej	Środkowej	k1gInSc1	Środkowej
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Eurostrany	Eurostran	k1gInPc1	Eurostran
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Europeizace	Europeizace	k1gFnSc1	Europeizace
zájmů	zájem	k1gInPc2	zájem
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
pozornost	pozornost	k1gFnSc4	pozornost
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
jeho	jeho	k3xOp3gFnSc1	jeho
kritická	kritický	k2eAgFnSc1d1	kritická
analýza	analýza	k1gFnSc1	analýza
současné	současný	k2eAgFnSc2d1	současná
politiky	politika	k1gFnSc2	politika
Politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
jaká	jaký	k3yQgFnSc1	jaký
nemá	mít	k5eNaImIp3nS	mít
být	být	k5eAaImF	být
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
14	[number]	k4	14
monografií	monografie	k1gFnPc2	monografie
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
odborných	odborný	k2eAgFnPc2d1	odborná
studií	studie	k1gFnPc2	studie
publikovaných	publikovaný	k2eAgFnPc2d1	publikovaná
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
na	na	k7c4	na
řešení	řešení	k1gNnSc4	řešení
mnoha	mnoho	k4c2	mnoho
národních	národní	k2eAgInPc2d1	národní
i	i	k9	i
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
výzkumných	výzkumný	k2eAgInPc2d1	výzkumný
projektů	projekt	k1gInPc2	projekt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
nezávislých	závislý	k2eNgFnPc2d1	nezávislá
občanských	občanský	k2eAgFnPc2d1	občanská
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1984	[number]	k4	1984
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
tzv.	tzv.	kA	tzv.
podzemní	podzemní	k2eAgFnSc2d1	podzemní
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
bytových	bytový	k2eAgInPc2d1	bytový
seminářů	seminář	k1gInPc2	seminář
zaměřených	zaměřený	k2eAgInPc2d1	zaměřený
na	na	k7c4	na
politickou	politický	k2eAgFnSc4d1	politická
filozofii	filozofie	k1gFnSc4	filozofie
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
zapojen	zapojen	k2eAgInSc1d1	zapojen
do	do	k7c2	do
neoficiálních	oficiální	k2eNgFnPc2d1	neoficiální
křesťanských	křesťanský	k2eAgFnPc2d1	křesťanská
aktivit	aktivita	k1gFnPc2	aktivita
především	především	k9	především
v	v	k7c6	v
okruhu	okruh	k1gInSc6	okruh
tajně	tajně	k6eAd1	tajně
vysvěceného	vysvěcený	k2eAgMnSc2d1	vysvěcený
biskupa	biskup	k1gMnSc2	biskup
Stanislava	Stanislav	k1gMnSc2	Stanislav
Krátkého	Krátký	k1gMnSc2	Krátký
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
několika	několik	k4yIc7	několik
brněnskými	brněnský	k2eAgMnPc7d1	brněnský
studenty	student	k1gMnPc7	student
založil	založit	k5eAaPmAgMnS	založit
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1988	[number]	k4	1988
<g/>
-	-	kIx~	-
<g/>
1989	[number]	k4	1989
vydával	vydávat	k5eAaImAgInS	vydávat
samizdatový	samizdatový	k2eAgInSc1d1	samizdatový
vysokoškolský	vysokoškolský	k2eAgInSc1d1	vysokoškolský
časopis	časopis	k1gInSc1	časopis
Revue	revue	k1gFnSc2	revue
88	[number]	k4	88
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
listopadu	listopad	k1gInSc6	listopad
1989	[number]	k4	1989
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
publikačních	publikační	k2eAgFnPc6d1	publikační
a	a	k8xC	a
občanských	občanský	k2eAgFnPc6d1	občanská
aktivitách	aktivita	k1gFnPc6	aktivita
<g/>
,	,	kIx,	,
působil	působit	k5eAaImAgMnS	působit
jako	jako	k9	jako
redaktor	redaktor	k1gMnSc1	redaktor
a	a	k8xC	a
editor	editor	k1gInSc1	editor
např.	např.	kA	např.
v	v	k7c6	v
časopisech	časopis	k1gInPc6	časopis
Proglas	Proglasa	k1gFnPc2	Proglasa
<g/>
,	,	kIx,	,
Revue	revue	k1gFnSc1	revue
Politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
Kontexty	kontext	k1gInPc1	kontext
apod.	apod.	kA	apod.
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
založil	založit	k5eAaPmAgInS	založit
s	s	k7c7	s
přáteli	přítel	k1gMnPc7	přítel
občanské	občanský	k2eAgFnSc2d1	občanská
sdružení	sdružení	k1gNnSc6	sdružení
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
demokracie	demokracie	k1gFnSc2	demokracie
a	a	k8xC	a
kultury	kultura	k1gFnSc2	kultura
(	(	kIx(	(
<g/>
CDK	CDK	kA	CDK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
think-tank	thinkank	k1gMnSc1	think-tank
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgFnPc2d3	nejúspěšnější
neziskových	ziskový	k2eNgFnPc2d1	nezisková
vzdělávacích	vzdělávací	k2eAgFnPc2d1	vzdělávací
a	a	k8xC	a
kulturních	kulturní	k2eAgFnPc2d1	kulturní
organizací	organizace	k1gFnPc2	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
působí	působit	k5eAaImIp3nS	působit
také	také	k9	také
v	v	k7c6	v
institucích	instituce	k1gFnPc6	instituce
a	a	k8xC	a
orgánech	orgán	k1gInPc6	orgán
zabývajících	zabývající	k2eAgFnPc2d1	zabývající
se	se	k3xPyFc4	se
vysokým	vysoký	k2eAgNnSc7d1	vysoké
školstvím	školství	k1gNnSc7	školství
a	a	k8xC	a
výzkumem	výzkum	k1gInSc7	výzkum
v	v	k7c6	v
ČR	ČR	kA	ČR
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2005	[number]	k4	2005
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
opakovaně	opakovaně	k6eAd1	opakovaně
zvolen	zvolit	k5eAaPmNgInS	zvolit
místopředsedou	místopředseda	k1gMnSc7	místopředseda
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
předsedou	předseda	k1gMnSc7	předseda
České	český	k2eAgFnSc2d1	Česká
konference	konference	k1gFnSc2	konference
rektorů	rektor	k1gMnPc2	rektor
<g/>
,	,	kIx,	,
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
úrovni	úroveň	k1gFnSc6	úroveň
byl	být	k5eAaImAgMnS	být
mj.	mj.	kA	mj.
členem	člen	k1gInSc7	člen
Rady	rada	k1gFnPc4	rada
Evropské	evropský	k2eAgFnSc2d1	Evropská
univerzitní	univerzitní	k2eAgFnSc2d1	univerzitní
asociace	asociace	k1gFnSc2	asociace
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Parlamentem	parlament	k1gInSc7	parlament
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
zvolen	zvolit	k5eAaPmNgInS	zvolit
do	do	k7c2	do
Rady	rada	k1gFnSc2	rada
Ústavu	ústav	k1gInSc2	ústav
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
totalitních	totalitní	k2eAgInPc2d1	totalitní
režimů	režim	k1gInPc2	režim
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
vládou	vláda	k1gFnSc7	vláda
ČR	ČR	kA	ČR
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
členem	člen	k1gMnSc7	člen
Akreditační	akreditační	k2eAgFnSc2d1	akreditační
komise	komise	k1gFnSc2	komise
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
Rady	rada	k1gFnSc2	rada
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
<g/>
,	,	kIx,	,
vývoj	vývoj	k1gInSc1	vývoj
a	a	k8xC	a
inovace	inovace	k1gFnSc1	inovace
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
místopředsedou	místopředseda	k1gMnSc7	místopředseda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
mnoha	mnoho	k4c2	mnoho
vědeckých	vědecký	k2eAgMnPc2d1	vědecký
a	a	k8xC	a
akademických	akademický	k2eAgFnPc2d1	akademická
rad	rada	k1gFnPc2	rada
veřejných	veřejný	k2eAgFnPc2d1	veřejná
i	i	k8xC	i
soukromých	soukromý	k2eAgFnPc2d1	soukromá
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
výzkumných	výzkumný	k2eAgFnPc2d1	výzkumná
institucí	instituce	k1gFnPc2	instituce
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
i	i	k8xC	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
vědeckou	vědecký	k2eAgFnSc4d1	vědecká
a	a	k8xC	a
akademickou	akademický	k2eAgFnSc4d1	akademická
činnost	činnost	k1gFnSc4	činnost
obdržel	obdržet	k5eAaPmAgInS	obdržet
řadu	řada	k1gFnSc4	řada
ocenění	ocenění	k1gNnSc2	ocenění
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgInS	být
vyznamenán	vyznamenat	k5eAaPmNgInS	vyznamenat
Zlatou	zlatý	k2eAgFnSc7d1	zlatá
plaketou	plaketa	k1gFnSc7	plaketa
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
:	:	kIx,	:
Občané	občan	k1gMnPc1	občan
<g/>
,	,	kIx,	,
demokraté	demokrat	k1gMnPc1	demokrat
a	a	k8xC	a
straníci	straník	k1gMnPc1	straník
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7325	[number]	k4	7325
<g/>
-	-	kIx~	-
<g/>
320	[number]	k4	320
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
:	:	kIx,	:
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
bezstarostnosti	bezstarostnost	k1gFnSc2	bezstarostnost
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7485	[number]	k4	7485
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
38	[number]	k4	38
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
:	:	kIx,	:
Politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
jaká	jaký	k3yRgFnSc1	jaký
nemá	mít	k5eNaImIp3nS	mít
být	být	k5eAaImF	být
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7325	[number]	k4	7325
<g/>
-	-	kIx~	-
<g/>
216	[number]	k4	216
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Balík	balík	k1gInSc1	balík
<g/>
,	,	kIx,	,
S.	S.	kA	S.
-	-	kIx~	-
Císař	Císař	k1gMnSc1	Císař
<g/>
,	,	kIx,	,
O.	O.	kA	O.
-	-	kIx~	-
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Veřejné	veřejný	k2eAgFnSc2d1	veřejná
politiky	politika	k1gFnSc2	politika
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1989	[number]	k4	1989
<g/>
-	-	kIx~	-
<g/>
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7325	[number]	k4	7325
<g/>
-	-	kIx~	-
<g/>
236	[number]	k4	236
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Evropeizace	evropeizace	k1gFnSc1	evropeizace
zájmů	zájem	k1gInPc2	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
210	[number]	k4	210
<g/>
-	-	kIx~	-
<g/>
4920	[number]	k4	4920
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
-	-	kIx~	-
Foral	Foral	k1gMnSc1	Foral
<g/>
,	,	kIx,	,
J.	J.	kA	J.
-	-	kIx~	-
Konečný	Konečný	k1gMnSc1	Konečný
<g/>
,	,	kIx,	,
K.	K.	kA	K.
-	-	kIx~	-
Marek	Marek	k1gMnSc1	Marek
<g/>
,	,	kIx,	,
P.	P.	kA	P.
-	-	kIx~	-
Pehr	Pehr	k1gMnSc1	Pehr
<g/>
,	,	kIx,	,
M.	M.	kA	M.
-	-	kIx~	-
Trapl	Trapl	k1gMnSc1	Trapl
<g/>
,	,	kIx,	,
M.	M.	kA	M.
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Český	český	k2eAgInSc1d1	český
politický	politický	k2eAgInSc1d1	politický
katolicismus	katolicismus	k1gInSc1	katolicismus
1848	[number]	k4	1848
<g/>
-	-	kIx~	-
<g/>
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7325	[number]	k4	7325
<g/>
-	-	kIx~	-
<g/>
155	[number]	k4	155
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
:	:	kIx,	:
Evropský	evropský	k2eAgInSc1d1	evropský
mezičas	mezičas	k1gInSc1	mezičas
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87029	[number]	k4	87029
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
;	;	kIx,	;
2	[number]	k4	2
<g/>
.	.	kIx.	.
aktualizované	aktualizovaný	k2eAgNnSc1d1	aktualizované
a	a	k8xC	a
rozšířené	rozšířený	k2eAgNnSc1d1	rozšířené
vydání	vydání	k1gNnSc1	vydání
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87029	[number]	k4	87029
<g/>
-	-	kIx~	-
<g/>
99	[number]	k4	99
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
:	:	kIx,	:
Laboratoř	laboratoř	k1gFnSc1	laboratoř
sekularizace	sekularizace	k1gFnSc1	sekularizace
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7325	[number]	k4	7325
<g/>
-	-	kIx~	-
<g/>
141	[number]	k4	141
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
-	-	kIx~	-
Mareš	Mareš	k1gMnSc1	Mareš
<g/>
,	,	kIx,	,
M.	M.	kA	M.
-	-	kIx~	-
Sokol	Sokol	k1gMnSc1	Sokol
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
:	:	kIx,	:
Eurostrany	Eurostrana	k1gFnSc2	Eurostrana
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87029	[number]	k4	87029
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Dočkal	Dočkal	k1gMnSc1	Dočkal
<g/>
,	,	kIx,	,
V.	V.	kA	V.
-	-	kIx~	-
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
-	-	kIx~	-
Pitrová	Pitrová	k1gFnSc1	Pitrová
<g/>
,	,	kIx,	,
M.	M.	kA	M.
-	-	kIx~	-
Kaniok	Kaniok	k1gInSc1	Kaniok
<g/>
,	,	kIx,	,
P.	P.	kA	P.
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
politika	politika	k1gFnSc1	politika
v	v	k7c6	v
Evropské	evropský	k2eAgFnSc6d1	Evropská
unii	unie	k1gFnSc6	unie
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
210	[number]	k4	210
<g/>
-	-	kIx~	-
<g/>
4076	[number]	k4	4076
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Dančák	Dančák	k1gMnSc1	Dančák
<g/>
,	,	kIx,	,
B.	B.	kA	B.
-	-	kIx~	-
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
-	-	kIx~	-
Hloušek	Hloušek	k1gMnSc1	Hloušek
<g/>
,	,	kIx,	,
V.	V.	kA	V.
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Evropeizace	evropeizace	k1gFnSc1	evropeizace
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
210	[number]	k4	210
<g/>
-	-	kIx~	-
<g/>
3865	[number]	k4	3865
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
-	-	kIx~	-
Hanuš	Hanuš	k1gMnSc1	Hanuš
<g/>
,	,	kIx,	,
J.	J.	kA	J.
-	-	kIx~	-
Vybíral	Vybíral	k1gMnSc1	Vybíral
<g/>
,	,	kIx,	,
J.	J.	kA	J.
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Katolická	katolický	k2eAgFnSc1d1	katolická
sociální	sociální	k2eAgFnSc1d1	sociální
nauka	nauka	k1gFnSc1	nauka
a	a	k8xC	a
současná	současný	k2eAgFnSc1d1	současná
věda	věda	k1gFnSc1	věda
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7325	[number]	k4	7325
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
24	[number]	k4	24
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
-	-	kIx~	-
Hanuš	Hanuš	k1gMnSc1	Hanuš
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Die	Die	k1gMnSc1	Die
Verborgene	Verborgen	k1gInSc5	Verborgen
Kirche	Kirch	k1gInPc4	Kirch
<g/>
.	.	kIx.	.
</s>
<s>
Paderborn	Paderborn	k1gInSc1	Paderborn
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
506	[number]	k4	506
<g/>
-	-	kIx~	-
<g/>
72447	[number]	k4	72447
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Antoszewski	Antoszewski	k1gNnSc1	Antoszewski
<g/>
,	,	kIx,	,
A.	A.	kA	A.
-	-	kIx~	-
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
-	-	kIx~	-
Herbut	Herbut	k1gMnSc1	Herbut
<g/>
,	,	kIx,	,
R.	R.	kA	R.
-	-	kIx~	-
Sroka	Sroka	k1gMnSc1	Sroka
<g/>
,	,	kIx,	,
J.	J.	kA	J.
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Partie	partie	k1gFnPc4	partie
i	i	k8xC	i
systemy	systema	k1gFnPc4	systema
partyjne	partyjnout	k5eAaPmIp3nS	partyjnout
Europy	Europ	k1gMnPc4	Europ
Środkowej	Środkowej	k1gInSc4	Środkowej
<g/>
.	.	kIx.	.
</s>
<s>
Wroclaw	Wroclaw	k?	Wroclaw
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
83	[number]	k4	83
<g/>
-	-	kIx~	-
<g/>
229	[number]	k4	229
<g/>
-	-	kIx~	-
<g/>
2425	[number]	k4	2425
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
-	-	kIx~	-
Pitrová	Pitrová	k1gFnSc1	Pitrová
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7325	[number]	k4	7325
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
15	[number]	k4	15
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
;	;	kIx,	;
2	[number]	k4	2
<g/>
.	.	kIx.	.
dopl	dopnout	k5eAaPmAgInS	dopnout
<g/>
.	.	kIx.	.
a	a	k8xC	a
aktual	aktual	k1gInSc1	aktual
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7325	[number]	k4	7325
<g/>
-	-	kIx~	-
<g/>
180	[number]	k4	180
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
-	-	kIx~	-
Herbut	Herbut	k1gMnSc1	Herbut
<g/>
,	,	kIx,	,
R.	R.	kA	R.
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Středoevropské	středoevropský	k2eAgInPc1d1	středoevropský
systémy	systém	k1gInPc1	systém
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
210	[number]	k4	210
<g/>
-	-	kIx~	-
<g/>
3091	[number]	k4	3091
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
-	-	kIx~	-
Holzer	Holzer	k1gMnSc1	Holzer
<g/>
,	,	kIx,	,
J.	J.	kA	J.
-	-	kIx~	-
Strmiska	Strmiska	k1gFnSc1	Strmiska
<g/>
,	,	kIx,	,
M.	M.	kA	M.
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
210	[number]	k4	210
<g/>
-	-	kIx~	-
<g/>
3036	[number]	k4	3036
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
-	-	kIx~	-
Hanuš	Hanuš	k1gMnSc1	Hanuš
<g/>
,	,	kIx,	,
J.	J.	kA	J.
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
a	a	k8xC	a
totalitarismus	totalitarismus	k1gInSc1	totalitarismus
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85959	[number]	k4	85959
<g/>
-	-	kIx~	-
<g/>
98	[number]	k4	98
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
-	-	kIx~	-
Pitrová	Pitrová	k1gFnSc1	Pitrová
<g/>
,	,	kIx,	,
M.	M.	kA	M.
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Rozšiřování	rozšiřování	k1gNnSc1	rozšiřování
ES	es	k1gNnPc2	es
<g/>
/	/	kIx~	/
<g/>
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
210	[number]	k4	210
<g/>
-	-	kIx~	-
<g/>
2645	[number]	k4	2645
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
-	-	kIx~	-
Schubert	Schubert	k1gMnSc1	Schubert
<g/>
,	,	kIx,	,
K.	K.	kA	K.
<g/>
:	:	kIx,	:
Moderní	moderní	k2eAgFnSc1d1	moderní
analýza	analýza	k1gFnSc1	analýza
politiky	politika	k1gFnSc2	politika
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85947	[number]	k4	85947
<g/>
-	-	kIx~	-
<g/>
50	[number]	k4	50
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
-	-	kIx~	-
Mikš	Mikš	k1gMnSc1	Mikš
<g/>
,	,	kIx,	,
F.	F.	kA	F.
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
a	a	k8xC	a
liberální	liberální	k2eAgFnSc1d1	liberální
politika	politika	k1gFnSc1	politika
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85959	[number]	k4	85959
<g/>
-	-	kIx~	-
<g/>
73	[number]	k4	73
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
-	-	kIx~	-
Hanuš	Hanuš	k1gMnSc1	Hanuš
<g/>
,	,	kIx,	,
J.	J.	kA	J.
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Koncil	koncil	k1gInSc1	koncil
a	a	k8xC	a
česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85959	[number]	k4	85959
<g/>
-	-	kIx~	-
<g/>
75	[number]	k4	75
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
-	-	kIx~	-
Hanuš	Hanuš	k1gMnSc1	Hanuš
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Skrytá	skrytý	k2eAgFnSc1d1	skrytá
církev	církev	k1gFnSc1	církev
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85959	[number]	k4	85959
<g/>
-	-	kIx~	-
<g/>
39	[number]	k4	39
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
-	-	kIx~	-
Strmiska	Strmiska	k1gFnSc1	Strmiska
<g/>
,	,	kIx,	,
M.	M.	kA	M.
<g/>
:	:	kIx,	:
Teorie	teorie	k1gFnSc1	teorie
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85947	[number]	k4	85947
<g/>
-	-	kIx~	-
<g/>
31	[number]	k4	31
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
(	(	kIx(	(
<g/>
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Politický	politický	k2eAgInSc1d1	politický
extremismus	extremismus	k1gInSc1	extremismus
a	a	k8xC	a
radikalismus	radikalismus	k1gInSc1	radikalismus
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
210	[number]	k4	210
<g/>
-	-	kIx~	-
<g/>
1798	[number]	k4	1798
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
:	:	kIx,	:
Německá	německý	k2eAgFnSc1d1	německá
politologie	politologie	k1gFnSc1	politologie
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85959	[number]	k4	85959
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
:	:	kIx,	:
Katolicismus	katolicismus	k1gInSc1	katolicismus
a	a	k8xC	a
politika	politika	k1gFnSc1	politika
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85959	[number]	k4	85959
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Petr	Petr	k1gMnSc1	Petr
Fiala	Fiala	k1gMnSc1	Fiala
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Petr	Petr	k1gMnSc1	Petr
Fiala	Fiala	k1gMnSc1	Fiala
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Petr	Petr	k1gMnSc1	Petr
Fiala	Fiala	k1gMnSc1	Fiala
(	(	kIx(	(
<g/>
politik	politik	k1gMnSc1	politik
<g/>
)	)	kIx)	)
Osobní	osobní	k2eAgFnSc1d1	osobní
stránka	stránka	k1gFnSc1	stránka
Petra	Petr	k1gMnSc2	Petr
Fialy	Fiala	k1gMnSc2	Fiala
Petr	Petr	k1gMnSc1	Petr
Fiala	Fiala	k1gMnSc1	Fiala
v	v	k7c6	v
informačním	informační	k2eAgInSc6d1	informační
systému	systém	k1gInSc6	systém
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
Petr	Petr	k1gMnSc1	Petr
Fiala	Fiala	k1gMnSc1	Fiala
na	na	k7c6	na
Twitteru	Twitter	k1gInSc6	Twitter
Petr	Petr	k1gMnSc1	Petr
Fiala	Fiala	k1gMnSc1	Fiala
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
Blog	Bloga	k1gFnPc2	Bloga
Petra	Petr	k1gMnSc2	Petr
Fialy	fiala	k1gFnSc2	fiala
na	na	k7c4	na
Aktuálně	aktuálně	k6eAd1	aktuálně
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
