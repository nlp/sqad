<s>
Julius	Julius	k1gMnSc1	Julius
Zeyer	Zeyer	k1gMnSc1	Zeyer
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
duben	duben	k1gInSc4	duben
1841	[number]	k4	1841
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1901	[number]	k4	1901
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
přední	přední	k2eAgMnSc1d1	přední
představitel	představitel	k1gMnSc1	představitel
lumírovské	lumírovský	k2eAgFnSc2d1	lumírovská
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgMnS	označovat
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejvýraznějších	výrazný	k2eAgMnPc2d3	nejvýraznější
českých	český	k2eAgMnPc2d1	český
novoromantiků	novoromantik	k1gMnPc2	novoromantik
či	či	k8xC	či
předchůdce	předchůdce	k1gMnSc2	předchůdce
dekadentů	dekadent	k1gMnPc2	dekadent
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
byl	být	k5eAaImAgMnS	být
významný	významný	k2eAgMnSc1d1	významný
český	český	k2eAgMnSc1d1	český
novorenesanční	novorenesanční	k2eAgMnSc1d1	novorenesanční
architekt	architekt	k1gMnSc1	architekt
Jan	Jan	k1gMnSc1	Jan
Zeyer	Zeyer	k1gMnSc1	Zeyer
z	z	k7c2	z
generace	generace	k1gFnSc2	generace
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
letech	let	k1gInPc6	let
1873	[number]	k4	1873
<g/>
–	–	k?	–
<g/>
1880	[number]	k4	1880
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
architektem	architekt	k1gMnSc7	architekt
Antonínem	Antonín	k1gMnSc7	Antonín
Wiehlem	Wiehl	k1gMnSc7	Wiehl
<g/>
,	,	kIx,	,
vůdčí	vůdčí	k2eAgFnSc7d1	vůdčí
osobností	osobnost	k1gFnSc7	osobnost
novorenesance	novorenesance	k1gFnSc2	novorenesance
navazujícího	navazující	k2eAgMnSc2d1	navazující
na	na	k7c6	na
tradici	tradice	k1gFnSc6	tradice
české	český	k2eAgFnSc2d1	Česká
renesance	renesance	k1gFnSc2	renesance
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Náměty	námět	k1gInPc4	námět
čerpal	čerpat	k5eAaImAgInS	čerpat
nejčastěji	často	k6eAd3	často
z	z	k7c2	z
dávné	dávný	k2eAgFnSc2d1	dávná
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
,	,	kIx,	,
bájí	báj	k1gFnSc7	báj
a	a	k8xC	a
pověstí	pověst	k1gFnSc7	pověst
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
českých	český	k2eAgInPc2d1	český
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
evropských	evropský	k2eAgFnPc2d1	Evropská
-	-	kIx~	-
především	především	k6eAd1	především
starogermánských	starogermánský	k2eAgInPc2d1	starogermánský
<g/>
,	,	kIx,	,
starorománských	starorománský	k2eAgInPc2d1	starorománský
a	a	k8xC	a
slovanských	slovanský	k2eAgInPc2d1	slovanský
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
poezie	poezie	k1gFnSc1	poezie
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
epická	epický	k2eAgFnSc1d1	epická
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
často	často	k6eAd1	často
nostalgický	nostalgický	k2eAgInSc4d1	nostalgický
ráz	ráz	k1gInSc4	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
literárním	literární	k2eAgNnSc6d1	literární
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
už	už	k6eAd1	už
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
prózu	próza	k1gFnSc4	próza
či	či	k8xC	či
poezii	poezie	k1gFnSc4	poezie
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
vysnil	vysnít	k5eAaPmAgInS	vysnít
vlastní	vlastní	k2eAgInSc4d1	vlastní
svět	svět	k1gInSc4	svět
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
měl	mít	k5eAaImAgInS	mít
s	s	k7c7	s
realitou	realita	k1gFnSc7	realita
jen	jen	k9	jen
málo	málo	k4c4	málo
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fiktivní	fiktivní	k2eAgInSc1d1	fiktivní
svět	svět	k1gInSc1	svět
byl	být	k5eAaImAgInS	být
i	i	k9	i
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
hodnotami	hodnota	k1gFnPc7	hodnota
a	a	k8xC	a
pravidly	pravidlo	k1gNnPc7	pravidlo
pravým	pravý	k2eAgInSc7d1	pravý
opakem	opak	k1gInSc7	opak
tehdejšího	tehdejší	k2eAgNnSc2d1	tehdejší
provinciálního	provinciální	k2eAgNnSc2d1	provinciální
českého	český	k2eAgNnSc2d1	české
maloměšťáctví	maloměšťáctví	k1gNnSc2	maloměšťáctví
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
přicházel	přicházet	k5eAaImAgMnS	přicházet
Zeyer	Zeyer	k1gMnSc1	Zeyer
denně	denně	k6eAd1	denně
do	do	k7c2	do
styku	styk	k1gInSc2	styk
a	a	k8xC	a
které	který	k3yRgInPc4	který
tolik	tolik	k6eAd1	tolik
nenáviděl	návidět	k5eNaImAgInS	návidět
<g/>
.	.	kIx.	.
</s>
<s>
Mnohdy	mnohdy	k6eAd1	mnohdy
odrážel	odrážet	k5eAaImAgMnS	odrážet
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
krizi	krize	k1gFnSc4	krize
citlivého	citlivý	k2eAgMnSc2d1	citlivý
idealisty	idealista	k1gMnSc2	idealista
v	v	k7c6	v
pragmaticky	pragmaticky	k6eAd1	pragmaticky
založené	založený	k2eAgFnSc6d1	založená
měšťácké	měšťácký	k2eAgFnSc6d1	měšťácká
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Protiklady	protiklad	k1gInPc1	protiklad
a	a	k8xC	a
vztahy	vztah	k1gInPc1	vztah
naděje	naděje	k1gFnSc2	naděje
a	a	k8xC	a
zklamání	zklamání	k1gNnSc2	zklamání
<g/>
,	,	kIx,	,
smyslového	smyslový	k2eAgNnSc2d1	smyslové
okouzlení	okouzlení	k1gNnSc2	okouzlení
a	a	k8xC	a
touhy	touha	k1gFnSc2	touha
překonat	překonat	k5eAaPmF	překonat
niternou	niterný	k2eAgFnSc4d1	niterná
myšlenkovou	myšlenkový	k2eAgFnSc4d1	myšlenková
pomíjivost	pomíjivost	k1gFnSc4	pomíjivost
tohoto	tento	k3xDgNnSc2	tento
okouzlení	okouzlení	k1gNnSc2	okouzlení
tvoří	tvořit	k5eAaImIp3nP	tvořit
dominantu	dominanta	k1gFnSc4	dominanta
celého	celý	k2eAgNnSc2d1	celé
Zeyerova	Zeyerův	k2eAgNnSc2d1	Zeyerovo
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
díle	díl	k1gInSc6	díl
se	se	k3xPyFc4	se
také	také	k9	také
často	často	k6eAd1	často
objevuje	objevovat	k5eAaImIp3nS	objevovat
motiv	motiv	k1gInSc1	motiv
silného	silný	k2eAgMnSc2d1	silný
<g/>
,	,	kIx,	,
citového	citový	k2eAgNnSc2d1	citové
přátelství	přátelství	k1gNnSc2	přátelství
mezi	mezi	k7c7	mezi
muži	muž	k1gMnPc7	muž
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Román	román	k1gInSc1	román
o	o	k7c6	o
věrném	věrný	k2eAgNnSc6d1	věrné
přátelství	přátelství	k1gNnSc6	přátelství
Amise	Amise	k1gFnSc2	Amise
a	a	k8xC	a
Amila	Amilo	k1gNnSc2	Amilo
<g/>
)	)	kIx)	)
a	a	k8xC	a
až	až	k9	až
eroticky	eroticky	k6eAd1	eroticky
laděné	laděný	k2eAgInPc4d1	laděný
popisy	popis	k1gInPc4	popis
krásy	krása	k1gFnSc2	krása
mladých	mladý	k2eAgMnPc2d1	mladý
mužů	muž	k1gMnPc2	muž
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Vyšehrad	Vyšehrad	k1gInSc4	Vyšehrad
<g/>
,	,	kIx,	,
úvod	úvod	k1gInSc4	úvod
Ondřeje	Ondřej	k1gMnSc2	Ondřej
Černyševa	Černyšev	k1gMnSc2	Černyšev
nebo	nebo	k8xC	nebo
postava	postava	k1gFnSc1	postava
Inulta	Inulta	k1gFnSc1	Inulta
ze	z	k7c2	z
Tří	tři	k4xCgFnPc2	tři
legend	legenda	k1gFnPc2	legenda
o	o	k7c6	o
krucifixu	krucifix	k1gInSc6	krucifix
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgInPc2	tento
faktů	fakt	k1gInPc2	fakt
a	a	k8xC	a
pečlivého	pečlivý	k2eAgInSc2d1	pečlivý
rozboru	rozbor	k1gInSc2	rozbor
jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
došla	dojít	k5eAaPmAgFnS	dojít
řada	řada	k1gFnSc1	řada
literárních	literární	k2eAgMnPc2d1	literární
vědců	vědec	k1gMnPc2	vědec
a	a	k8xC	a
historiků	historik	k1gMnPc2	historik
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
Julius	Julius	k1gMnSc1	Julius
Zeyer	Zeyer	k1gMnSc1	Zeyer
byl	být	k5eAaImAgMnS	být
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
homosexuál	homosexuál	k1gMnSc1	homosexuál
či	či	k8xC	či
snad	snad	k9	snad
bisexuál	bisexuál	k1gMnSc1	bisexuál
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
teorii	teorie	k1gFnSc3	teorie
nasvědčuje	nasvědčovat	k5eAaImIp3nS	nasvědčovat
i	i	k9	i
Zeyerova	Zeyerův	k2eAgFnSc1d1	Zeyerova
osobní	osobní	k2eAgFnSc1d1	osobní
korespondence	korespondence	k1gFnSc1	korespondence
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
opakovaně	opakovaně	k6eAd1	opakovaně
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
o	o	k7c6	o
jakémsi	jakýsi	k3yIgInSc6	jakýsi
svém	svůj	k3xOyFgInSc6	svůj
velkém	velký	k2eAgInSc6d1	velký
a	a	k8xC	a
strašném	strašný	k2eAgNnSc6d1	strašné
tajemství	tajemství	k1gNnSc6	tajemství
<g/>
,	,	kIx,	,
o	o	k7c6	o
svých	svůj	k3xOyFgInPc6	svůj
pocitech	pocit	k1gInPc6	pocit
osamělosti	osamělost	k1gFnSc2	osamělost
<g/>
,	,	kIx,	,
smutku	smutek	k1gInSc2	smutek
a	a	k8xC	a
vyděděnosti	vyděděnost	k1gFnSc2	vyděděnost
ze	z	k7c2	z
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Pravdou	pravda	k1gFnSc7	pravda
je	být	k5eAaImIp3nS	být
také	také	k9	také
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neoženil	oženit	k5eNaPmAgInS	oženit
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
vážný	vážný	k2eAgInSc4d1	vážný
partnerský	partnerský	k2eAgInSc4d1	partnerský
vztah	vztah	k1gInSc4	vztah
se	s	k7c7	s
ženou	žena	k1gFnSc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gMnPc4	jeho
velmi	velmi	k6eAd1	velmi
blízké	blízký	k2eAgMnPc4d1	blízký
přátele	přítel	k1gMnPc4	přítel
však	však	k9	však
patřilo	patřit	k5eAaImAgNnS	patřit
hned	hned	k6eAd1	hned
několik	několik	k4yIc1	několik
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgFnPc7	jenž
si	se	k3xPyFc3	se
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozuměl	rozumět	k5eAaImAgInS	rozumět
–	–	k?	–
zde	zde	k6eAd1	zde
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zmínit	zmínit	k5eAaPmF	zmínit
především	především	k9	především
Annu	Anna	k1gFnSc4	Anna
Marii	Maria	k1gFnSc4	Maria
Stoneovou	Stoneův	k2eAgFnSc7d1	Stoneova
<g/>
,	,	kIx,	,
slečnu	slečna	k1gFnSc4	slečna
Kershawovou	Kershawová	k1gFnSc4	Kershawová
nebo	nebo	k8xC	nebo
Annu	Anna	k1gFnSc4	Anna
Lauermannovou	Lauermannový	k2eAgFnSc4d1	Lauermannový
Mikschovou	Mikschová	k1gFnSc4	Mikschová
<g/>
.	.	kIx.	.
</s>
<s>
Julius	Julius	k1gMnSc1	Julius
Zeyer	Zeyer	k1gMnSc1	Zeyer
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k9	jako
páté	pátá	k1gFnPc4	pátá
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
dětí	dítě	k1gFnPc2	dítě
tesaře	tesař	k1gMnSc2	tesař
Jana	Jan	k1gMnSc2	Jan
Zeyera	Zeyer	k1gMnSc2	Zeyer
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
Eleonory	Eleonora	k1gFnSc2	Eleonora
Alžběty	Alžběta	k1gFnSc2	Alžběta
rozené	rozený	k2eAgFnSc2d1	rozená
Weisseles	Weisseles	k1gInSc4	Weisseles
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
Julia	Julius	k1gMnSc2	Julius
Zeyera	Zeyer	k1gMnSc2	Zeyer
pocházel	pocházet	k5eAaImAgMnS	pocházet
ze	z	k7c2	z
staré	starý	k2eAgFnSc2d1	stará
francouzské	francouzský	k2eAgFnSc2d1	francouzská
šlechtické	šlechtický	k2eAgFnSc2d1	šlechtická
rodiny	rodina	k1gFnSc2	rodina
z	z	k7c2	z
Alsaska	Alsasko	k1gNnSc2	Alsasko
<g/>
,	,	kIx,	,
matka	matka	k1gFnSc1	matka
z	z	k7c2	z
židovsko-německé	židovskoěmecký	k2eAgFnSc2d1	židovsko-německý
pražské	pražský	k2eAgFnSc2d1	Pražská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
již	již	k6eAd1	již
delší	dlouhý	k2eAgInSc4d2	delší
čas	čas	k1gInSc4	čas
hlásila	hlásit	k5eAaImAgFnS	hlásit
ke	k	k7c3	k
katolictví	katolictví	k1gNnSc3	katolictví
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Zeyer	Zeyer	k1gMnSc1	Zeyer
se	se	k3xPyFc4	se
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
na	na	k7c4	na
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
firmu	firma	k1gFnSc4	firma
tesaře	tesař	k1gMnSc2	tesař
a	a	k8xC	a
velkoobchodníka	velkoobchodník	k1gMnSc2	velkoobchodník
s	s	k7c7	s
dřívím	dříví	k1gNnSc7	dříví
<g/>
,	,	kIx,	,
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
předčasné	předčasný	k2eAgFnSc6d1	předčasná
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
vedení	vedení	k1gNnSc3	vedení
podniku	podnik	k1gInSc2	podnik
ujala	ujmout	k5eAaPmAgFnS	ujmout
Eleonora	Eleonora	k1gFnSc1	Eleonora
a	a	k8xC	a
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
Julia	Julius	k1gMnSc2	Julius
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
chvíle	chvíle	k1gFnSc2	chvíle
připravovala	připravovat	k5eAaImAgFnS	připravovat
na	na	k7c4	na
budoucí	budoucí	k2eAgNnSc4d1	budoucí
převzetí	převzetí	k1gNnSc4	převzetí
rodinné	rodinný	k2eAgFnSc2d1	rodinná
firmy	firma	k1gFnSc2	firma
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
Emilian	Emilian	k1gMnSc1	Emilian
si	se	k3xPyFc3	se
zvolil	zvolit	k5eAaPmAgMnS	zvolit
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Julius	Julius	k1gMnSc1	Julius
se	se	k3xPyFc4	se
vyučil	vyučit	k5eAaPmAgMnS	vyučit
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
tesařem	tesař	k1gMnSc7	tesař
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
Julius	Julius	k1gMnSc1	Julius
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
obchodní	obchodní	k2eAgFnSc4d1	obchodní
reálku	reálka	k1gFnSc4	reálka
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
zkusil	zkusit	k5eAaPmAgMnS	zkusit
studium	studium	k1gNnSc4	studium
architektury	architektura	k1gFnSc2	architektura
na	na	k7c6	na
České	český	k2eAgFnSc6d1	Česká
technice	technika	k1gFnSc6	technika
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
jeho	jeho	k3xOp3gMnSc1	jeho
mladší	mladý	k2eAgMnSc1d2	mladší
bratr	bratr	k1gMnSc1	bratr
Jan	Jan	k1gMnSc1	Jan
Zeyer	Zeyer	k1gMnSc1	Zeyer
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
jediný	jediný	k2eAgMnSc1d1	jediný
z	z	k7c2	z
dětí	dítě	k1gFnPc2	dítě
zůstal	zůstat	k5eAaPmAgInS	zůstat
s	s	k7c7	s
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
vypomáhal	vypomáhat	k5eAaImAgInS	vypomáhat
jí	jíst	k5eAaImIp3nS	jíst
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
dvaceti	dvacet	k4xCc6	dvacet
jedna	jeden	k4xCgFnSc1	jeden
letech	léto	k1gNnPc6	léto
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c4	na
zkušenou	zkušená	k1gFnSc4	zkušená
nejprve	nejprve	k6eAd1	nejprve
do	do	k7c2	do
tesařské	tesařský	k2eAgFnSc2d1	tesařská
dílny	dílna	k1gFnSc2	dílna
ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
a	a	k8xC	a
následně	následně	k6eAd1	následně
procestoval	procestovat	k5eAaPmAgMnS	procestovat
Německo	Německo	k1gNnSc4	Německo
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
a	a	k8xC	a
Francii	Francie	k1gFnSc4	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
tesařině	tesařina	k1gFnSc3	tesařina
věnoval	věnovat	k5eAaImAgMnS	věnovat
studiu	studio	k1gNnSc3	studio
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
filosofie	filosofie	k1gFnSc2	filosofie
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
mytologie	mytologie	k1gFnSc2	mytologie
(	(	kIx(	(
<g/>
především	především	k9	především
keltské	keltský	k2eAgInPc4d1	keltský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
domů	domů	k6eAd1	domů
se	se	k3xPyFc4	se
matka	matka	k1gFnSc1	matka
Eleonora	Eleonora	k1gFnSc1	Eleonora
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
podporovat	podporovat	k5eAaImF	podporovat
svého	svůj	k3xOyFgMnSc4	svůj
milovaného	milovaný	k2eAgMnSc4d1	milovaný
syna	syn	k1gMnSc4	syn
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
literární	literární	k2eAgFnSc6d1	literární
kariéře	kariéra	k1gFnSc6	kariéra
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yIgFnSc6	který
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgInSc4	sám
již	již	k9	již
léta	léto	k1gNnPc4	léto
toužil	toužit	k5eAaImAgInS	toužit
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
zajištěn	zajištěn	k2eAgMnSc1d1	zajištěn
nemusel	muset	k5eNaImAgMnS	muset
mít	mít	k5eAaImF	mít
obavy	obava	k1gFnPc4	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
se	s	k7c7	s
psaním	psaní	k1gNnSc7	psaní
neuživil	uživit	k5eNaPmAgInS	uživit
<g/>
.	.	kIx.	.
</s>
<s>
Julius	Julius	k1gMnSc1	Julius
Zeyer	Zeyer	k1gMnSc1	Zeyer
se	se	k3xPyFc4	se
na	na	k7c6	na
filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
se	se	k3xPyFc4	se
zapsal	zapsat	k5eAaPmAgMnS	zapsat
na	na	k7c4	na
přednášky	přednáška	k1gFnPc4	přednáška
z	z	k7c2	z
estetiky	estetika	k1gFnSc2	estetika
<g/>
,	,	kIx,	,
filologie	filologie	k1gFnSc2	filologie
<g/>
,	,	kIx,	,
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
,	,	kIx,	,
starověké	starověký	k2eAgFnSc2d1	starověká
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
však	však	k9	však
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
jemu	on	k3xPp3gMnSc3	on
osobně	osobně	k6eAd1	osobně
vyhovovalo	vyhovovat	k5eAaImAgNnS	vyhovovat
<g/>
.	.	kIx.	.
</s>
<s>
Intenzivně	intenzivně	k6eAd1	intenzivně
se	se	k3xPyFc4	se
také	také	k9	také
věnoval	věnovat	k5eAaPmAgMnS	věnovat
studiu	studio	k1gNnSc3	studio
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
mluvil	mluvit	k5eAaImAgMnS	mluvit
německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
naučil	naučit	k5eAaPmAgMnS	naučit
se	se	k3xPyFc4	se
aktivně	aktivně	k6eAd1	aktivně
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
<g/>
,	,	kIx,	,
španělsky	španělsky	k6eAd1	španělsky
a	a	k8xC	a
rusky	rusky	k6eAd1	rusky
<g/>
.	.	kIx.	.
</s>
<s>
Osvojil	osvojit	k5eAaPmAgInS	osvojit
si	se	k3xPyFc3	se
také	také	k6eAd1	také
základy	základ	k1gInPc4	základ
sanskrtu	sanskrt	k1gInSc2	sanskrt
<g/>
,	,	kIx,	,
koptštiny	koptština	k1gFnSc2	koptština
a	a	k8xC	a
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
.	.	kIx.	.
</s>
<s>
Zeyer	Zeyer	k1gMnSc1	Zeyer
debutoval	debutovat	k5eAaBmAgMnS	debutovat
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Paleček	paleček	k1gInSc1	paleček
pod	pod	k7c7	pod
svým	svůj	k3xOyFgNnSc7	svůj
jménem	jméno	k1gNnSc7	jméno
povídkou	povídka	k1gFnSc7	povídka
Krásné	krásný	k2eAgInPc1d1	krásný
zoubky	zoubek	k1gInPc1	zoubek
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
vydal	vydat	k5eAaPmAgMnS	vydat
úspěšnou	úspěšný	k2eAgFnSc4d1	úspěšná
novelu	novela	k1gFnSc4	novela
Duhový	duhový	k2eAgMnSc1d1	duhový
pták	pták	k1gMnSc1	pták
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
kmenovým	kmenový	k2eAgMnSc7d1	kmenový
autorem	autor	k1gMnSc7	autor
časopisu	časopis	k1gInSc2	časopis
Lumír	Lumír	k1gInSc4	Lumír
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
otiskoval	otiskovat	k5eAaImAgMnS	otiskovat
většinu	většina	k1gFnSc4	většina
prací	práce	k1gFnPc2	práce
ještě	ještě	k9	ještě
před	před	k7c7	před
jejich	jejich	k3xOp3gNnSc7	jejich
vydáním	vydání	k1gNnSc7	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Skutečně	skutečně	k6eAd1	skutečně
uzvávaným	uzvávaný	k2eAgMnSc7d1	uzvávaný
autorem	autor	k1gMnSc7	autor
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
historického	historický	k2eAgInSc2d1	historický
románu	román	k1gInSc2	román
Ondřej	Ondřej	k1gMnSc1	Ondřej
Černyšev	Černyšev	k1gMnSc1	Černyšev
(	(	kIx(	(
<g/>
knižně	knižně	k6eAd1	knižně
1876	[number]	k4	1876
<g/>
,	,	kIx,	,
v	v	k7c6	v
předešlém	předešlý	k2eAgInSc6d1	předešlý
roce	rok	k1gInSc6	rok
vycházel	vycházet	k5eAaImAgMnS	vycházet
na	na	k7c4	na
pokračování	pokračování	k1gNnSc4	pokračování
v	v	k7c6	v
Lumíru	Lumír	k1gInSc6	Lumír
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
pozornost	pozornost	k1gFnSc1	pozornost
náhle	náhle	k6eAd1	náhle
obrátila	obrátit	k5eAaPmAgFnS	obrátit
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
–	–	k?	–
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
magie	magie	k1gFnSc2	magie
a	a	k8xC	a
okultismu	okultismus	k1gInSc2	okultismus
<g/>
.	.	kIx.	.
</s>
<s>
Zeyer	Zeyer	k1gMnSc1	Zeyer
obdivoval	obdivovat	k5eAaImAgMnS	obdivovat
středověké	středověký	k2eAgInPc4d1	středověký
ideály	ideál	k1gInPc4	ideál
a	a	k8xC	a
staré	starý	k2eAgInPc4d1	starý
cestopisy	cestopis	k1gInPc4	cestopis
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
častou	častý	k2eAgFnSc7d1	častá
inspirací	inspirace	k1gFnSc7	inspirace
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
další	další	k2eAgFnSc6d1	další
tvorbě	tvorba	k1gFnSc6	tvorba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Maeldunova	Maeldunův	k2eAgFnSc1d1	Maeldunův
výprava	výprava	k1gFnSc1	výprava
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kronika	kronika	k1gFnSc1	kronika
o	o	k7c6	o
svatém	svatý	k2eAgMnSc6d1	svatý
Brandanu	Brandan	k1gMnSc6	Brandan
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
život	život	k1gInSc1	život
irského	irský	k2eAgMnSc2d1	irský
apoštola	apoštol	k1gMnSc2	apoštol
svatého	svatý	k2eAgMnSc2d1	svatý
Patrika	Patrik	k1gMnSc2	Patrik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
V	v	k7c6	v
soumraku	soumrak	k1gInSc6	soumrak
bohů	bůh	k1gMnPc2	bůh
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
téma	téma	k1gNnSc1	téma
zápasu	zápas	k1gInSc2	zápas
dohasínajícího	dohasínající	k2eAgNnSc2d1	dohasínající
pohanství	pohanství	k1gNnSc2	pohanství
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
řádem	řád	k1gInSc7	řád
<g/>
,	,	kIx,	,
zvyky	zvyk	k1gInPc4	zvyk
a	a	k8xC	a
rituály	rituál	k1gInPc4	rituál
s	s	k7c7	s
počínajícím	počínající	k2eAgNnSc7d1	počínající
křesťanstvím	křesťanství	k1gNnSc7	křesťanství
<g/>
,	,	kIx,	,
přinášejícím	přinášející	k2eAgNnSc7d1	přinášející
nové	nový	k2eAgMnPc4d1	nový
hodnoty	hodnota	k1gFnSc2	hodnota
a	a	k8xC	a
postoje	postoj	k1gInSc2	postoj
–	–	k?	–
zejména	zejména	k9	zejména
odpuštění	odpuštění	k1gNnSc1	odpuštění
a	a	k8xC	a
soucit	soucit	k1gInSc1	soucit
<g/>
.	.	kIx.	.
</s>
<s>
Středověkou	středověký	k2eAgFnSc4d1	středověká
tematiku	tematika	k1gFnSc4	tematika
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
i	i	k9	i
v	v	k7c6	v
Románu	román	k1gInSc6	román
o	o	k7c6	o
věrném	věrný	k2eAgNnSc6d1	věrné
přátelství	přátelství	k1gNnSc6	přátelství
Amise	Amise	k1gFnSc2	Amise
a	a	k8xC	a
Amila	Amilo	k1gNnSc2	Amilo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Zeyer	Zeyer	k1gMnSc1	Zeyer
zobrazil	zobrazit	k5eAaPmAgMnS	zobrazit
kontrast	kontrast	k1gInSc4	kontrast
křesťanského	křesťanský	k2eAgInSc2d1	křesťanský
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
reprezentovaného	reprezentovaný	k2eAgInSc2d1	reprezentovaný
rytířskou	rytířský	k2eAgFnSc4d1	rytířská
Francií	Francie	k1gFnSc7	Francie
<g/>
,	,	kIx,	,
a	a	k8xC	a
pohanského	pohanský	k2eAgInSc2d1	pohanský
severu	sever	k1gInSc2	sever
<g/>
,	,	kIx,	,
zastoupeného	zastoupený	k2eAgInSc2d1	zastoupený
Islandem	Island	k1gInSc7	Island
a	a	k8xC	a
tamními	tamní	k2eAgFnPc7d1	tamní
valkýrami	valkýra	k1gFnPc7	valkýra
<g/>
.	.	kIx.	.
</s>
<s>
Zeyer	Zeyer	k1gMnSc1	Zeyer
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
prosadit	prosadit	k5eAaPmF	prosadit
také	také	k9	také
na	na	k7c6	na
divadle	divadlo	k1gNnSc6	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Drama	drama	k1gNnSc1	drama
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
struhující	struhující	k2eAgFnSc1d1	struhující
jevištní	jevištní	k2eAgFnSc1d1	jevištní
báseň	báseň	k1gFnSc1	báseň
<g/>
"	"	kIx"	"
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
vrchol	vrchol	k1gInSc4	vrchol
literárního	literární	k2eAgNnSc2d1	literární
snažení	snažení	k1gNnSc2	snažení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
dramatická	dramatický	k2eAgFnSc1d1	dramatická
tvorba	tvorba	k1gFnSc1	tvorba
připomínala	připomínat	k5eAaImAgFnS	připomínat
ze	z	k7c2	z
všeho	všecek	k3xTgNnSc2	všecek
nejvíce	hodně	k6eAd3	hodně
lyrickoepické	lyrickoepický	k2eAgFnPc1d1	lyrickoepická
básně	báseň	k1gFnPc1	báseň
<g/>
,	,	kIx,	,
určené	určený	k2eAgFnPc1d1	určená
k	k	k7c3	k
inscenování	inscenování	k1gNnSc3	inscenování
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
díla	dílo	k1gNnPc4	dílo
často	často	k6eAd1	často
literárně	literárně	k6eAd1	literárně
krásná	krásný	k2eAgFnSc1d1	krásná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
divadelní	divadelní	k2eAgNnPc4d1	divadelní
provedení	provedení	k1gNnPc4	provedení
nevhodná	vhodný	k2eNgFnSc1d1	nevhodná
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
hra	hra	k1gFnSc1	hra
Sulamit	Sulamit	k1gFnSc1	Sulamit
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
inscenace	inscenace	k1gFnPc1	inscenace
inspirované	inspirovaný	k2eAgFnPc1d1	inspirovaná
náměty	námět	k1gInPc4	námět
z	z	k7c2	z
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
být	být	k5eAaImF	být
rovnocenným	rovnocenný	k2eAgInSc7d1	rovnocenný
doplňkem	doplněk	k1gInSc7	doplněk
jeho	jeho	k3xOp3gFnSc2	jeho
české	český	k2eAgFnSc2d1	Česká
epopeje	epopej	k1gFnSc2	epopej
<g/>
.	.	kIx.	.
</s>
<s>
Zeyer	Zeyer	k1gMnSc1	Zeyer
proto	proto	k8xC	proto
vybíral	vybírat	k5eAaImAgMnS	vybírat
především	především	k6eAd1	především
klíčové	klíčový	k2eAgInPc4d1	klíčový
momenty	moment	k1gInPc4	moment
české	český	k2eAgFnSc2d1	Česká
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
mezníky	mezník	k1gInPc7	mezník
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
dovršení	dovršení	k1gNnSc3	dovršení
minulosti	minulost	k1gFnSc2	minulost
a	a	k8xC	a
předestření	předestření	k1gNnSc2	předestření
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
Neklan	Neklan	k1gMnSc1	Neklan
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Šárka	Šárka	k1gFnSc1	Šárka
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Libušin	Libušin	k2eAgInSc1d1	Libušin
hněv	hněv	k1gInSc1	hněv
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
hrou	hra	k1gFnSc7	hra
Julia	Julius	k1gMnSc2	Julius
Zeyera	Zeyer	k1gMnSc2	Zeyer
je	být	k5eAaImIp3nS	být
Radúz	Radúz	k1gMnSc1	Radúz
a	a	k8xC	a
Mahulena	Mahulena	k1gFnSc1	Mahulena
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
dodnes	dodnes	k6eAd1	dodnes
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
byla	být	k5eAaImAgFnS	být
dokonce	dokonce	k9	dokonce
zfilmována	zfilmován	k2eAgFnSc1d1	zfilmována
(	(	kIx(	(
<g/>
režie	režie	k1gFnSc1	režie
Petr	Petr	k1gMnSc1	Petr
Weigl	Weigl	k1gMnSc1	Weigl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Julius	Julius	k1gMnSc1	Julius
Zeyer	Zeyer	k1gMnSc1	Zeyer
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
velmi	velmi	k6eAd1	velmi
trpěl	trpět	k5eAaImAgMnS	trpět
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
neměl	mít	k5eNaImAgMnS	mít
na	na	k7c6	na
divadle	divadlo	k1gNnSc6	divadlo
příliš	příliš	k6eAd1	příliš
velké	velký	k2eAgInPc1d1	velký
úspěchy	úspěch	k1gInPc1	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
byl	být	k5eAaImAgInS	být
i	i	k9	i
důvodem	důvod	k1gInSc7	důvod
rozpadu	rozpad	k1gInSc2	rozpad
jeho	on	k3xPp3gNnSc2	on
přátelství	přátelství	k1gNnSc2	přátelství
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Vrchlickým	Vrchlický	k1gMnSc7	Vrchlický
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgMnS	být
jako	jako	k8xS	jako
dramatik	dramatik	k1gMnSc1	dramatik
mnohem	mnohem	k6eAd1	mnohem
známější	známý	k2eAgMnSc1d2	známější
<g/>
.	.	kIx.	.
</s>
<s>
Zeyerovo	Zeyerův	k2eAgNnSc4d1	Zeyerovo
dramatické	dramatický	k2eAgNnSc4d1	dramatické
dílo	dílo	k1gNnSc4	dílo
ale	ale	k8xC	ale
často	často	k6eAd1	často
zlákalo	zlákat	k5eAaPmAgNnS	zlákat
hudebníky	hudebník	k1gMnPc4	hudebník
–	–	k?	–
k	k	k7c3	k
Radúzovi	Radúz	k1gMnSc3	Radúz
a	a	k8xC	a
Mahuleně	Mahulena	k1gFnSc3	Mahulena
složil	složit	k5eAaPmAgMnS	složit
hudbu	hudba	k1gFnSc4	hudba
Josef	Josef	k1gMnSc1	Josef
Suk	Suk	k1gMnSc1	Suk
<g/>
,	,	kIx,	,
Leoš	Leoš	k1gMnSc1	Leoš
Janáček	Janáček	k1gMnSc1	Janáček
si	se	k3xPyFc3	se
zase	zase	k9	zase
zvolil	zvolit	k5eAaPmAgMnS	zvolit
Šárku	Šárka	k1gFnSc4	Šárka
jako	jako	k8xC	jako
libreto	libreto	k1gNnSc4	libreto
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
stejnojmennou	stejnojmenný	k2eAgFnSc4d1	stejnojmenná
operu	opera	k1gFnSc4	opera
a	a	k8xC	a
na	na	k7c4	na
libreto	libreto	k1gNnSc4	libreto
podle	podle	k7c2	podle
povídky	povídka	k1gFnSc2	povídka
Kunálovy	Kunálův	k2eAgFnSc2d1	Kunálův
oči	oko	k1gNnPc4	oko
složil	složit	k5eAaPmAgMnS	složit
operu	opera	k1gFnSc4	opera
Otakar	Otakar	k1gMnSc1	Otakar
Ostrčil	Ostrčil	k1gMnSc1	Ostrčil
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
románem	román	k1gInSc7	román
Julia	Julius	k1gMnSc2	Julius
Zeyera	Zeyer	k1gMnSc2	Zeyer
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
autobiograficky	autobiograficky	k6eAd1	autobiograficky
laděné	laděný	k2eAgNnSc1d1	laděné
dílo	dílo	k1gNnSc1	dílo
Jan	Jan	k1gMnSc1	Jan
Maria	Mario	k1gMnSc2	Mario
Plojhar	Plojhar	k1gMnSc1	Plojhar
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
také	také	k9	také
poprvé	poprvé	k6eAd1	poprvé
výrazněji	výrazně	k6eAd2	výrazně
objevují	objevovat	k5eAaImIp3nP	objevovat
prvky	prvek	k1gInPc1	prvek
mesianismu	mesianismus	k1gInSc2	mesianismus
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Zeyer	Zeyer	k1gMnSc1	Zeyer
přirovnává	přirovnávat	k5eAaImIp3nS	přirovnávat
osudy	osud	k1gInPc4	osud
české	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
k	k	k7c3	k
utrpení	utrpení	k1gNnSc3	utrpení
Kristovu	Kristův	k2eAgInSc3d1	Kristův
<g/>
.	.	kIx.	.
</s>
<s>
Téma	téma	k1gNnSc1	téma
mesianismu	mesianismus	k1gInSc2	mesianismus
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
silněji	silně	k6eAd2	silně
také	také	k9	také
v	v	k7c6	v
povídkovém	povídkový	k2eAgNnSc6d1	povídkové
triptychu	triptychon	k1gNnSc6	triptychon
Tři	tři	k4xCgFnPc1	tři
legendy	legenda	k1gFnPc1	legenda
o	o	k7c6	o
krucifixu	krucifix	k1gInSc6	krucifix
(	(	kIx(	(
<g/>
vydaném	vydaný	k2eAgNnSc6d1	vydané
posmrtně	posmrtně	k6eAd1	posmrtně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýraznější	výrazný	k2eAgFnSc7d3	nejvýraznější
skladbou	skladba	k1gFnSc7	skladba
ze	z	k7c2	z
závěru	závěr	k1gInSc2	závěr
Zeyerovy	Zeyerův	k2eAgFnSc2d1	Zeyerova
tvorby	tvorba	k1gFnSc2	tvorba
jsou	být	k5eAaImIp3nP	být
Troje	troje	k4xRgFnPc1	troje
paměti	paměť	k1gFnPc1	paměť
Víta	Vít	k1gMnSc2	Vít
Choráze	Choráze	k1gFnSc2	Choráze
(	(	kIx(	(
<g/>
posmrtně	posmrtně	k6eAd1	posmrtně
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rovněž	rovněž	k9	rovněž
autobiograficky	autobiograficky	k6eAd1	autobiograficky
laděné	laděný	k2eAgNnSc4d1	laděné
dílo	dílo	k1gNnSc4	dílo
s	s	k7c7	s
výrazně	výrazně	k6eAd1	výrazně
dekadentními	dekadentní	k2eAgInPc7d1	dekadentní
rysy	rys	k1gInPc7	rys
<g/>
,	,	kIx,	,
vypovídající	vypovídající	k2eAgInPc1d1	vypovídající
o	o	k7c6	o
překonání	překonání	k1gNnSc6	překonání
těžké	těžký	k2eAgFnSc2d1	těžká
krize	krize	k1gFnSc2	krize
nalezením	nalezení	k1gNnSc7	nalezení
životního	životní	k2eAgNnSc2d1	životní
poslání	poslání	k1gNnSc2	poslání
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
stavba	stavba	k1gFnSc1	stavba
venkovského	venkovský	k2eAgInSc2d1	venkovský
kostelíka	kostelík	k1gInSc2	kostelík
<g/>
)	)	kIx)	)
a	a	k8xC	a
přimknutím	přimknutí	k1gNnSc7	přimknutí
se	se	k3xPyFc4	se
ke	k	k7c3	k
Kristu	Krista	k1gFnSc4	Krista
a	a	k8xC	a
kontinuitě	kontinuita	k1gFnSc3	kontinuita
prostých	prostý	k2eAgMnPc2d1	prostý
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
náznaky	náznak	k1gInPc1	náznak
dekadence	dekadence	k1gFnSc2	dekadence
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
již	již	k6eAd1	již
u	u	k7c2	u
Jana	Jan	k1gMnSc2	Jan
Marii	Maria	k1gFnSc4	Maria
Plojhara	Plojhar	k1gMnSc2	Plojhar
a	a	k8xC	a
u	u	k7c2	u
díla	dílo	k1gNnSc2	dílo
Dům	dům	k1gInSc1	dům
U	u	k7c2	u
Tonoucí	tonoucí	k2eAgFnSc2d1	tonoucí
hvězdy	hvězda	k1gFnSc2	hvězda
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
</s>
<s>
Zeyer	Zeyer	k1gMnSc1	Zeyer
byl	být	k5eAaImAgMnS	být
veliký	veliký	k2eAgMnSc1d1	veliký
cestovatel	cestovatel	k1gMnSc1	cestovatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
působil	působit	k5eAaImAgMnS	působit
V	v	k7c6	v
Petrohradě	Petrohrad	k1gInSc6	Petrohrad
jako	jako	k8xS	jako
vychovatel	vychovatel	k1gMnSc1	vychovatel
u	u	k7c2	u
šlechtické	šlechtický	k2eAgFnSc2d1	šlechtická
rodiny	rodina	k1gFnSc2	rodina
Valujevových	Valujevová	k1gFnPc2	Valujevová
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
J.	J.	kA	J.
V.	V.	kA	V.
Sládkem	Sládek	k1gMnSc7	Sládek
podnikl	podniknout	k5eAaPmAgMnS	podniknout
cestu	cesta	k1gFnSc4	cesta
do	do	k7c2	do
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
procestoval	procestovat	k5eAaPmAgInS	procestovat
také	také	k9	také
Nizozemsko	Nizozemsko	k1gNnSc4	Nizozemsko
<g/>
,	,	kIx,	,
Belgii	Belgie	k1gFnSc4	Belgie
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
několikrát	několikrát	k6eAd1	několikrát
jako	jako	k8xS	jako
vychovatel	vychovatel	k1gMnSc1	vychovatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
<g/>
,	,	kIx,	,
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
a	a	k8xC	a
také	také	k9	také
Afriku	Afrika	k1gFnSc4	Afrika
-	-	kIx~	-
Tunis	Tunis	k1gInSc4	Tunis
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
Řecko	Řecko	k1gNnSc4	Řecko
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc4	Turecko
a	a	k8xC	a
Francii	Francie	k1gFnSc4	Francie
a	a	k8xC	a
řadu	řada	k1gFnSc4	řada
dalších	další	k2eAgNnPc2d1	další
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
exotické	exotický	k2eAgFnPc1d1	exotická
země	zem	k1gFnPc1	zem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
třeba	třeba	k6eAd1	třeba
ani	ani	k8xC	ani
reálně	reálně	k6eAd1	reálně
nenavštívil	navštívit	k5eNaPmAgMnS	navštívit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
staly	stát	k5eAaPmAgInP	stát
i	i	k9	i
tvůrčí	tvůrčí	k2eAgFnSc7d1	tvůrčí
inspirací	inspirace	k1gFnSc7	inspirace
–	–	k?	–
Japonsko	Japonsko	k1gNnSc1	Japonsko
pro	pro	k7c4	pro
román	román	k1gInSc4	román
Gompači	Gompač	k1gInPc7	Gompač
a	a	k8xC	a
Komurasaki	Komurasak	k1gInPc7	Komurasak
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
však	však	k9	však
není	být	k5eNaImIp3nS	být
poučným	poučný	k2eAgInSc7d1	poučný
či	či	k8xC	či
popisným	popisný	k2eAgInSc7d1	popisný
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
<g/>
,	,	kIx,	,
příběh	příběh	k1gInSc1	příběh
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
legendou	legenda	k1gFnSc7	legenda
o	o	k7c6	o
původu	původ	k1gInSc6	původ
hrobu	hrob	k1gInSc2	hrob
dvou	dva	k4xCgMnPc2	dva
milenců	milenec	k1gMnPc2	milenec
v	v	k7c6	v
tokijském	tokijský	k2eAgInSc6d1	tokijský
Meguru	Megur	k1gInSc6	Megur
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Japonsku	Japonsko	k1gNnSc3	Japonsko
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
také	také	k9	také
drama	drama	k1gNnSc1	drama
Lásky	láska	k1gFnSc2	láska
div	divit	k5eAaImRp2nS	divit
<g/>
.	.	kIx.	.
</s>
<s>
Čína	Čína	k1gFnSc1	Čína
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
eposu	epos	k1gInSc6	epos
Zrada	zrada	k1gFnSc1	zrada
v	v	k7c6	v
domě	dům	k1gInSc6	dům
Han	Hana	k1gFnPc2	Hana
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
či	či	k8xC	či
v	v	k7c6	v
dramatu	drama	k1gNnSc3	drama
Bratři	bratr	k1gMnPc1	bratr
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
v	v	k7c6	v
povídkách	povídka	k1gFnPc6	povídka
Kunálovy	Kunálův	k2eAgFnSc2d1	Kunálův
oči	oko	k1gNnPc4	oko
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
a	a	k8xC	a
Island	Island	k1gInSc4	Island
v	v	k7c6	v
již	již	k6eAd1	již
zmíněném	zmíněný	k2eAgInSc6d1	zmíněný
V	v	k7c6	v
soumraku	soumrak	k1gInSc6	soumrak
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
cizích	cizí	k2eAgFnPc2d1	cizí
kultur	kultura	k1gFnPc2	kultura
si	se	k3xPyFc3	se
ale	ale	k9	ale
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
tvorbu	tvorba	k1gFnSc4	tvorba
vybíral	vybírat	k5eAaImAgMnS	vybírat
jen	jen	k9	jen
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
jeho	jeho	k3xOp3gFnSc6	jeho
osobní	osobní	k2eAgFnSc6d1	osobní
představě	představa	k1gFnSc6	představa
o	o	k7c6	o
kráse	krása	k1gFnSc6	krása
<g/>
,	,	kIx,	,
ušlechtilosti	ušlechtilost	k1gFnSc6	ušlechtilost
a	a	k8xC	a
estetičnosti	estetičnost	k1gFnSc6	estetičnost
<g/>
.	.	kIx.	.
</s>
<s>
Mýty	mýtus	k1gInPc1	mýtus
a	a	k8xC	a
legendy	legenda	k1gFnPc1	legenda
těchto	tento	k3xDgFnPc2	tento
různých	různý	k2eAgFnPc2d1	různá
zemí	zem	k1gFnPc2	zem
tak	tak	k6eAd1	tak
přetvářel	přetvářet	k5eAaImAgMnS	přetvářet
k	k	k7c3	k
obrazu	obraz	k1gInSc3	obraz
svému	svůj	k3xOyFgInSc3	svůj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
těmito	tento	k3xDgInPc7	tento
náměty	námět	k1gInPc7	námět
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gNnSc2	on
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
obnovených	obnovený	k2eAgInPc6d1	obnovený
obrazech	obraz	k1gInPc6	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Obnovené	obnovený	k2eAgInPc1d1	obnovený
obrazy	obraz	k1gInPc1	obraz
jsou	být	k5eAaImIp3nP	být
názvem	název	k1gInSc7	název
tří	tři	k4xCgInPc2	tři
svazků	svazek	k1gInPc2	svazek
Zeyerových	Zeyerových	k2eAgFnPc2d1	Zeyerových
próz	próza	k1gFnPc2	próza
vydaných	vydaný	k2eAgFnPc2d1	vydaná
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
</s>
<s>
Spočívají	spočívat	k5eAaImIp3nP	spočívat
v	v	k7c6	v
inspiraci	inspirace	k1gFnSc6	inspirace
starou	starý	k2eAgFnSc7d1	stará
látkou	látka	k1gFnSc7	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
nahlížena	nahlížen	k2eAgFnSc1d1	nahlížena
novým	nový	k2eAgNnSc7d1	nové
prizmatem	prizma	k1gNnSc7	prizma
obraznosti	obraznost	k1gFnSc2	obraznost
a	a	k8xC	a
citovosti	citovost	k1gFnSc2	citovost
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíc	nejvíc	k6eAd1	nejvíc
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
životě	život	k1gInSc6	život
cestoval	cestovat	k5eAaImAgMnS	cestovat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
vychovatelem	vychovatel	k1gMnSc7	vychovatel
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
synů	syn	k1gMnPc2	syn
generála	generál	k1gMnSc4	generál
Popova	popův	k2eAgMnSc4d1	popův
na	na	k7c6	na
Krymu	Krym	k1gInSc6	Krym
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
u	u	k7c2	u
hraběte	hrabě	k1gMnSc2	hrabě
Harracha	Harrach	k1gMnSc2	Harrach
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Julius	Julius	k1gMnSc1	Julius
Zeyer	Zeyer	k1gMnSc1	Zeyer
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
sbíral	sbírat	k5eAaImAgMnS	sbírat
památky	památka	k1gFnPc4	památka
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
shromažďoval	shromažďovat	k5eAaImAgMnS	shromažďovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
vile	vila	k1gFnSc6	vila
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
–	–	k?	–
Horní	horní	k2eAgFnSc6d1	horní
Liboci	Liboc	k1gFnSc6	Liboc
(	(	kIx(	(
<g/>
bydlel	bydlet	k5eAaImAgMnS	bydlet
při	při	k7c6	při
ohradní	ohradní	k2eAgFnSc6d1	ohradní
zdi	zeď	k1gFnSc6	zeď
obory	obora	k1gFnSc2	obora
Hvězda	Hvězda	k1gMnSc1	Hvězda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zdědil	zdědit	k5eAaPmAgMnS	zdědit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
sám	sám	k3xTgMnSc1	sám
nakupoval	nakupovat	k5eAaBmAgInS	nakupovat
jak	jak	k6eAd1	jak
nábytek	nábytek	k1gInSc1	nábytek
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
zařízení	zařízení	k1gNnPc1	zařízení
interiéru	interiér	k1gInSc2	interiér
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
originály	originál	k1gInPc1	originál
soch	socha	k1gFnPc2	socha
<g/>
,	,	kIx,	,
obrazů	obraz	k1gInPc2	obraz
a	a	k8xC	a
klenoty	klenot	k1gInPc1	klenot
<g/>
,	,	kIx,	,
proslulá	proslulý	k2eAgFnSc1d1	proslulá
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
sbírka	sbírka	k1gFnSc1	sbírka
prstenů	prsten	k1gInPc2	prsten
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
sbíral	sbírat	k5eAaImAgMnS	sbírat
sádrové	sádrový	k2eAgInPc4d1	sádrový
odlitky	odlitek	k1gInPc4	odlitek
slavných	slavný	k2eAgMnPc2d1	slavný
středověkých	středověký	k2eAgMnPc2d1	středověký
či	či	k8xC	či
renesančních	renesanční	k2eAgMnPc2d1	renesanční
artefaktů	artefakt	k1gInPc2	artefakt
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
krucifixy	krucifix	k1gInPc1	krucifix
a	a	k8xC	a
madony	madona	k1gFnPc1	madona
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
kupoval	kupovat	k5eAaImAgInS	kupovat
exotická	exotický	k2eAgNnPc4d1	exotické
díla	dílo	k1gNnPc4	dílo
mimoevropského	mimoevropský	k2eAgInSc2d1	mimoevropský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
Dálného	dálný	k2eAgInSc2d1	dálný
Východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
sbírka	sbírka	k1gFnSc1	sbírka
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
přešla	přejít	k5eAaPmAgFnS	přejít
odkazem	odkaz	k1gInSc7	odkaz
do	do	k7c2	do
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
podle	podle	k7c2	podle
provenience	provenience	k1gFnSc2	provenience
starožitností	starožitnost	k1gFnPc2	starožitnost
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
exotika	exotikum	k1gNnPc1	exotikum
jsou	být	k5eAaImIp3nP	být
uchovávána	uchovávat	k5eAaImNgNnP	uchovávat
v	v	k7c4	v
Náprstkové	náprstkový	k2eAgInPc4d1	náprstkový
muzeu	muzeum	k1gNnSc6	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Život	život	k1gInSc1	život
Julia	Julius	k1gMnSc2	Julius
Zeyera	Zeyer	k1gMnSc2	Zeyer
byl	být	k5eAaImAgInS	být
vyplněn	vyplnit	k5eAaPmNgInS	vyplnit
tvorbou	tvorba	k1gFnSc7	tvorba
<g/>
,	,	kIx,	,
cestováním	cestování	k1gNnSc7	cestování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
čilým	čilý	k2eAgInSc7d1	čilý
společenským	společenský	k2eAgInSc7d1	společenský
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
hostem	host	k1gMnSc7	host
salónu	salón	k1gInSc2	salón
Anny	Anna	k1gFnSc2	Anna
Lauermannové-Mikschové	Lauermannové-Mikschová	k1gFnSc2	Lauermannové-Mikschová
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc7	jeho
obdivovatelkou	obdivovatelka	k1gFnSc7	obdivovatelka
a	a	k8xC	a
ctitelkou	ctitelka	k1gFnSc7	ctitelka
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
také	také	k9	také
docházel	docházet	k5eAaImAgInS	docházet
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
soustředěné	soustředěný	k2eAgFnSc2d1	soustředěná
kolem	kolem	k7c2	kolem
kulturního	kulturní	k2eAgMnSc2d1	kulturní
organizátora	organizátor	k1gMnSc2	organizátor
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
Náprstka	Náprstka	k1gFnSc1	Náprstka
či	či	k8xC	či
do	do	k7c2	do
salónu	salón	k1gInSc2	salón
Augusty	Augusta	k1gMnSc2	Augusta
Braunerové	Braunerová	k1gFnSc2	Braunerová
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
dcerou	dcera	k1gFnSc7	dcera
–	–	k?	–
malířkou	malířka	k1gFnSc7	malířka
Zdenkou	Zdenka	k1gFnSc7	Zdenka
Braunerovou	Braunerová	k1gFnSc7	Braunerová
navázal	navázat	k5eAaPmAgInS	navázat
velmi	velmi	k6eAd1	velmi
blízký	blízký	k2eAgInSc1d1	blízký
vztah	vztah	k1gInSc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Julius	Julius	k1gMnSc1	Julius
Zeyer	Zeyer	k1gMnSc1	Zeyer
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
osobním	osobní	k2eAgInSc6d1	osobní
životě	život	k1gInSc6	život
velmi	velmi	k6eAd1	velmi
komplikovaný	komplikovaný	k2eAgMnSc1d1	komplikovaný
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Svými	svůj	k3xOyFgMnPc7	svůj
přáteli	přítel	k1gMnPc7	přítel
byl	být	k5eAaImAgMnS	být
označován	označován	k2eAgMnSc1d1	označován
za	za	k7c4	za
člověka	člověk	k1gMnSc4	člověk
něžného	něžný	k2eAgNnSc2d1	něžné
a	a	k8xC	a
křehkého	křehký	k2eAgNnSc2d1	křehké
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jeho	jeho	k3xOp3gFnSc2	jeho
korespondence	korespondence	k1gFnSc2	korespondence
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
život	život	k1gInSc4	život
trpěl	trpět	k5eAaImAgMnS	trpět
depresemi	deprese	k1gFnPc7	deprese
a	a	k8xC	a
úzkostnými	úzkostný	k2eAgInPc7d1	úzkostný
stavy	stav	k1gInPc7	stav
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
výše	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
indicií	indicie	k1gFnPc2	indicie
dovozují	dovozovat	k5eAaImIp3nP	dovozovat
jeho	jeho	k3xOp3gFnSc4	jeho
homosexualitu	homosexualita	k1gFnSc4	homosexualita
nebo	nebo	k8xC	nebo
přinejmenším	přinejmenším	k6eAd1	přinejmenším
"	"	kIx"	"
<g/>
homoerotickou	homoerotický	k2eAgFnSc4d1	homoerotická
konstelaci	konstelace	k1gFnSc4	konstelace
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
společensky	společensky	k6eAd1	společensky
zcela	zcela	k6eAd1	zcela
nepřijatelné	přijatelný	k2eNgNnSc1d1	nepřijatelné
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
také	také	k9	také
trpěl	trpět	k5eAaImAgMnS	trpět
celoživotní	celoživotní	k2eAgFnSc7d1	celoživotní
osamělostí	osamělost	k1gFnSc7	osamělost
a	a	k8xC	a
dost	dost	k6eAd1	dost
přecitlivěle	přecitlivěle	k6eAd1	přecitlivěle
reagoval	reagovat	k5eAaBmAgMnS	reagovat
na	na	k7c4	na
nepříznivé	příznivý	k2eNgMnPc4d1	nepříznivý
kritiky	kritik	k1gMnPc4	kritik
svého	své	k1gNnSc2	své
díla	dílo	k1gNnSc2	dílo
a	a	k8xC	a
malý	malý	k2eAgInSc4d1	malý
zájem	zájem	k1gInSc4	zájem
čtenářů	čtenář	k1gMnPc2	čtenář
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
matkou	matka	k1gFnSc7	matka
Zeyer	Zeyer	k1gMnSc1	Zeyer
bydlel	bydlet	k5eAaImAgInS	bydlet
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
v	v	k7c6	v
novoměstském	novoměstský	k2eAgInSc6d1	novoměstský
domě	dům	k1gInSc6	dům
čp.	čp.	k?	čp.
736	[number]	k4	736
<g/>
/	/	kIx~	/
<g/>
II	II	kA	II
<g/>
,	,	kIx,	,
než	než	k8xS	než
si	se	k3xPyFc3	se
dali	dát	k5eAaPmAgMnP	dát
postavit	postavit	k5eAaPmF	postavit
vilu	vila	k1gFnSc4	vila
v	v	k7c6	v
Liboci	Liboc	k1gFnSc6	Liboc
při	při	k7c6	při
oboře	obora	k1gFnSc6	obora
Hvězda	Hvězda	k1gMnSc1	Hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnPc1d1	poslední
léta	léto	k1gNnPc1	léto
života	život	k1gInSc2	život
prožil	prožít	k5eAaPmAgMnS	prožít
ve	v	k7c6	v
Vodňanech	Vodňan	k1gMnPc6	Vodňan
a	a	k8xC	a
v	v	k7c6	v
samém	samý	k3xTgInSc6	samý
závěru	závěr	k1gInSc6	závěr
se	se	k3xPyFc4	se
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
ke	k	k7c3	k
svému	svůj	k3xOyFgMnSc3	svůj
blízkému	blízký	k2eAgMnSc3d1	blízký
příteli	přítel	k1gMnSc3	přítel
a	a	k8xC	a
mecenáši	mecenáš	k1gMnSc3	mecenáš
Josefu	Josef	k1gMnSc3	Josef
Hlávkovi	Hlávka	k1gMnSc3	Hlávka
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
zámek	zámek	k1gInSc4	zámek
v	v	k7c6	v
Lužanech	Lužan	k1gInPc6	Lužan
<g/>
,	,	kIx,	,
a	a	k8xC	a
pak	pak	k6eAd1	pak
do	do	k7c2	do
jeho	on	k3xPp3gInSc2	on
pražského	pražský	k2eAgInSc2d1	pražský
paláce	palác	k1gInSc2	palác
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
také	také	k9	také
u	u	k7c2	u
své	svůj	k3xOyFgFnSc2	svůj
sestry	sestra	k1gFnSc2	sestra
Heleny	Helena	k1gFnSc2	Helena
Jungfeldové	Jungfeldová	k1gFnSc2	Jungfeldová
<g/>
,	,	kIx,	,
u	u	k7c2	u
níž	jenž	k3xRgFnSc2	jenž
zemřel	zemřít	k5eAaPmAgInS	zemřít
29	[number]	k4	29
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1901	[number]	k4	1901
po	po	k7c6	po
těžké	těžký	k2eAgFnSc6d1	těžká
srdeční	srdeční	k2eAgFnSc6d1	srdeční
nemoci	nemoc	k1gFnSc6	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
si	se	k3xPyFc3	se
přál	přát	k5eAaImAgMnS	přát
být	být	k5eAaImF	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
na	na	k7c6	na
lesním	lesní	k2eAgInSc6d1	lesní
hřbitově	hřbitov	k1gInSc6	hřbitov
u	u	k7c2	u
Vodňan	Vodňana	k1gFnPc2	Vodňana
<g/>
,	,	kIx,	,
odpočívá	odpočívat	k5eAaImIp3nS	odpočívat
jeho	jeho	k3xOp3gNnSc4	jeho
tělo	tělo	k1gNnSc4	tělo
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
na	na	k7c6	na
Slavíně	Slavín	k1gInSc6	Slavín
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byl	být	k5eAaImAgInS	být
uložen	uložit	k5eAaPmNgInS	uložit
jako	jako	k8xS	jako
první	první	k4xOgFnSc4	první
31	[number]	k4	31
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1901	[number]	k4	1901
<g/>
.	.	kIx.	.
</s>
<s>
Ondřej	Ondřej	k1gMnSc1	Ondřej
Černyšev	Černyšev	k1gMnSc1	Černyšev
(	(	kIx(	(
<g/>
1876	[number]	k4	1876
<g/>
)	)	kIx)	)
–	–	k?	–
historický	historický	k2eAgInSc4d1	historický
román	román	k1gInSc4	román
z	z	k7c2	z
cizího	cizí	k2eAgNnSc2d1	cizí
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
doby	doba	k1gFnSc2	doba
vlády	vláda	k1gFnSc2	vláda
Kateřiny	Kateřina	k1gFnSc2	Kateřina
Veliké	veliký	k2eAgFnSc2d1	veliká
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
je	být	k5eAaImIp3nS	být
vykreslen	vykreslen	k2eAgInSc1d1	vykreslen
osud	osud	k1gInSc1	osud
mladého	mladý	k2eAgMnSc2d1	mladý
snílka	snílek	k1gMnSc2	snílek
plného	plný	k2eAgInSc2d1	plný
ideálů	ideál	k1gInPc2	ideál
<g/>
,	,	kIx,	,
konflikt	konflikt	k1gInSc1	konflikt
milostného	milostný	k2eAgInSc2d1	milostný
citu	cit	k1gInSc2	cit
s	s	k7c7	s
mocenskými	mocenský	k2eAgFnPc7d1	mocenská
ambicemi	ambice	k1gFnPc7	ambice
spojenými	spojený	k2eAgFnPc7d1	spojená
s	s	k7c7	s
nadosobním	nadosobní	k2eAgNnSc7d1	nadosobní
posláním	poslání	k1gNnSc7	poslání
(	(	kIx(	(
<g/>
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
k	k	k7c3	k
národu	národ	k1gInSc3	národ
a	a	k8xC	a
zemi	zem	k1gFnSc3	zem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
o	o	k7c6	o
věrném	věrný	k2eAgNnSc6d1	věrné
přátelství	přátelství	k1gNnSc6	přátelství
Amise	Amise	k1gFnSc2	Amise
a	a	k8xC	a
Amila	Amilo	k1gNnSc2	Amilo
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
–	–	k?	–
román	román	k1gInSc1	román
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
starofrancouzské	starofrancouzský	k2eAgNnSc4d1	starofrancouzské
chanson	chanson	k1gNnSc4	chanson
de	de	k?	de
geste	gest	k1gInSc5	gest
Amis	Amisa	k1gFnPc2	Amisa
a	a	k8xC	a
Amil	Amila	k1gFnPc2	Amila
<g/>
.	.	kIx.	.
</s>
<s>
Báje	báje	k1gFnSc1	báje
Šošany	Šošana	k1gFnSc2	Šošana
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
–	–	k?	–
sbírka	sbírka	k1gFnSc1	sbírka
krátkých	krátká	k1gFnPc2	krátká
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
čerpá	čerpat	k5eAaImIp3nS	čerpat
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
ghetta	ghetto	k1gNnSc2	ghetto
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
Fantastické	fantastický	k2eAgFnSc2d1	fantastická
povídky	povídka	k1gFnSc2	povídka
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
–	–	k?	–
čtyři	čtyři	k4xCgFnPc4	čtyři
povídky	povídka	k1gFnPc1	povídka
se	s	k7c7	s
smyslem	smysl	k1gInSc7	smysl
pro	pro	k7c4	pro
iracionálno	iracionálno	k1gNnSc4	iracionálno
Dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
Madrány	Madrán	k2eAgFnPc1d1	Madrán
(	(	kIx(	(
<g/>
1878	[number]	k4	1878
v	v	k7c6	v
Lumíru	Lumír	k1gInSc6	Lumír
<g/>
,	,	kIx,	,
1882	[number]	k4	1882
knižně	knižně	k6eAd1	knižně
<g/>
)	)	kIx)	)
–	–	k?	–
román	román	k1gInSc1	román
Grizelda	Grizelda	k1gFnSc1	Grizelda
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
–	–	k?	–
lyricko-epická	lyrickopický	k2eAgFnSc1d1	lyricko-epická
povídka	povídka	k1gFnSc1	povídka
Stratonika	Stratonika	k1gFnSc1	Stratonika
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
Gompači	Gompač	k1gMnSc3	Gompač
a	a	k8xC	a
Komurasaki	Komurasak	k1gMnSc3	Komurasak
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
Rokoko	rokoko	k1gNnSc4	rokoko
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
Jan	Jan	k1gMnSc1	Jan
Maria	Mario	k1gMnSc2	Mario
Plojhar	Plojhar	k1gMnSc1	Plojhar
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
–	–	k?	–
román	román	k1gInSc1	román
s	s	k7c7	s
autobiografickými	autobiografický	k2eAgInPc7d1	autobiografický
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
výjimečný	výjimečný	k2eAgMnSc1d1	výjimečný
hrdina	hrdina	k1gMnSc1	hrdina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zklamán	zklamat	k5eAaPmNgMnS	zklamat
společností	společnost	k1gFnSc7	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Prožije	prožít	k5eAaPmIp3nS	prožít
sérii	série	k1gFnSc4	série
ztrát	ztráta	k1gFnPc2	ztráta
(	(	kIx(	(
<g/>
ztráta	ztráta	k1gFnSc1	ztráta
domova	domov	k1gInSc2	domov
<g/>
,	,	kIx,	,
vlasteneckého	vlastenecký	k2eAgNnSc2d1	vlastenecké
sebevědomí	sebevědomí	k1gNnSc2	sebevědomí
<g/>
,	,	kIx,	,
smrt	smrt	k1gFnSc1	smrt
matky	matka	k1gFnSc2	matka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ho	on	k3xPp3gMnSc4	on
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
odchodu	odchod	k1gInSc3	odchod
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
umírá	umírat	k5eAaImIp3nS	umírat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dívkou	dívka	k1gFnSc7	dívka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
si	se	k3xPyFc3	se
volí	volit	k5eAaImIp3nS	volit
dobrovolnou	dobrovolný	k2eAgFnSc4d1	dobrovolná
smrt	smrt	k1gFnSc4	smrt
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
boku	bok	k1gInSc6	bok
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
legendy	legenda	k1gFnPc1	legenda
o	o	k7c6	o
krucifixu	krucifix	k1gInSc6	krucifix
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
–	–	k?	–
tři	tři	k4xCgFnPc1	tři
samostatné	samostatný	k2eAgFnPc1d1	samostatná
legendy	legenda	k1gFnPc1	legenda
(	(	kIx(	(
<g/>
Inultus	Inultus	k1gInSc1	Inultus
<g/>
,	,	kIx,	,
El	Ela	k1gFnPc2	Ela
Christo	Christa	k1gMnSc5	Christa
de	de	k?	de
la	la	k1gNnSc2	la
Luz	luza	k1gFnPc2	luza
<g/>
,	,	kIx,	,
Samko	Samko	k1gNnSc1	Samko
pták	pták	k1gMnSc1	pták
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
náboženská	náboženský	k2eAgFnSc1d1	náboženská
tematika	tematika	k1gFnSc1	tematika
a	a	k8xC	a
krucifix	krucifix	k1gInSc1	krucifix
(	(	kIx(	(
<g/>
kříž	kříž	k1gInSc1	kříž
s	s	k7c7	s
ukřižovaným	ukřižovaný	k2eAgMnSc7d1	ukřižovaný
Ježíšem	Ježíš	k1gMnSc7	Ježíš
Kristem	Kristus	k1gMnSc7	Kristus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Legendy	legenda	k1gFnPc1	legenda
vypovídají	vypovídat	k5eAaImIp3nP	vypovídat
o	o	k7c6	o
různých	různý	k2eAgInPc6d1	různý
projevech	projev	k1gInPc6	projev
mučednictví	mučednictví	k1gNnSc2	mučednictví
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
spojeného	spojený	k2eAgInSc2d1	spojený
zázraku	zázrak	k1gInSc2	zázrak
<g/>
.	.	kIx.	.
</s>
<s>
Maeldunova	Maeldunův	k2eAgFnSc1d1	Maeldunův
výprava	výprava	k1gFnSc1	výprava
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
V	v	k7c6	v
soumraku	soumrak	k1gInSc6	soumrak
bohů	bůh	k1gMnPc2	bůh
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
–	–	k?	–
převyprávění	převyprávění	k1gNnSc2	převyprávění
staroisladských	staroisladský	k2eAgFnPc2d1	staroisladský
ság	sága	k1gFnPc2	sága
Troje	troje	k4xRgFnPc1	troje
paměti	paměť	k1gFnPc1	paměť
Víta	Vít	k1gMnSc2	Vít
Choráze	Choráze	k1gFnSc2	Choráze
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
v	v	k7c6	v
Lumíru	Lumír	k1gInSc6	Lumír
<g/>
)	)	kIx)	)
–	–	k?	–
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
básnická	básnický	k2eAgFnSc1d1	básnická
poéma	poéma	k1gFnSc1	poéma
<g/>
,	,	kIx,	,
rozdělená	rozdělený	k2eAgFnSc1d1	rozdělená
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
Dům	dům	k1gInSc4	dům
U	u	k7c2	u
Tonoucí	tonoucí	k2eAgFnSc2d1	tonoucí
hvězdy	hvězda	k1gFnSc2	hvězda
–	–	k?	–
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
lékař	lékař	k1gMnSc1	lékař
působící	působící	k2eAgMnSc1d1	působící
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
obdivuje	obdivovat	k5eAaImIp3nS	obdivovat
dům	dům	k1gInSc4	dům
U	u	k7c2	u
Tonoucí	tonoucí	k2eAgFnSc2d1	tonoucí
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
Slovákem	Slovák	k1gMnSc7	Slovák
Rojkem	rojek	k1gInSc7	rojek
<g/>
.	.	kIx.	.
</s>
<s>
Novela	novela	k1gFnSc1	novela
končí	končit	k5eAaImIp3nS	končit
tragicky	tragicky	k6eAd1	tragicky
–	–	k?	–
dům	dům	k1gInSc1	dům
vzplane	vzplanout	k5eAaPmIp3nS	vzplanout
a	a	k8xC	a
uhoří	uhořet	k5eAaPmIp3nS	uhořet
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
hrdina	hrdina	k1gMnSc1	hrdina
Rojko	Rojko	k1gNnSc4	Rojko
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
obyvatelky	obyvatelka	k1gFnPc4	obyvatelka
–	–	k?	–
stařeny	stařena	k1gFnPc4	stařena
z	z	k7c2	z
"	"	kIx"	"
<g/>
klece	klec	k1gFnSc2	klec
tří	tři	k4xCgFnPc2	tři
pomatených	pomatený	k2eAgFnPc2d1	pomatená
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
líčena	líčen	k2eAgFnSc1d1	líčena
beznaděj	beznaděj	k1gFnSc1	beznaděj
<g/>
,	,	kIx,	,
souchotiny	souchotiny	k1gFnPc1	souchotiny
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
novela	novela	k1gFnSc1	novela
je	být	k5eAaImIp3nS	být
ponurá	ponurý	k2eAgFnSc1d1	ponurá
a	a	k8xC	a
mystická	mystický	k2eAgFnSc1d1	mystická
<g/>
,	,	kIx,	,
předkládá	předkládat	k5eAaImIp3nS	předkládat
téma	téma	k1gNnSc1	téma
ohrožení	ohrožení	k1gNnSc2	ohrožení
civilizace	civilizace	k1gFnSc2	civilizace
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Obnovené	obnovený	k2eAgInPc1d1	obnovený
obrazy	obraz	k1gInPc1	obraz
Pohádka	pohádka	k1gFnSc1	pohádka
o	o	k7c6	o
dobrém	dobrý	k2eAgMnSc6d1	dobrý
careviči	carevič	k1gMnSc6	carevič
Evstafovi	Evstaf	k1gMnSc6	Evstaf
(	(	kIx(	(
<g/>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
)	)	kIx)	)
–	–	k?	–
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
národní	národní	k2eAgInSc4d1	národní
epos	epos	k1gInSc4	epos
–	–	k?	–
cyklus	cyklus	k1gInSc1	cyklus
básní	báseň	k1gFnPc2	báseň
<g/>
;	;	kIx,	;
snaha	snaha	k1gFnSc1	snaha
předvést	předvést	k5eAaPmF	předvést
velikost	velikost	k1gFnSc4	velikost
české	český	k2eAgFnSc2d1	Česká
minulosti	minulost	k1gFnSc2	minulost
Kronika	kronika	k1gFnSc1	kronika
o	o	k7c6	o
sv.	sv.	kA	sv.
Brandanu	Brandan	k1gInSc6	Brandan
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
,	,	kIx,	,
knižně	knižně	k6eAd1	knižně
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
–	–	k?	–
epická	epický	k2eAgFnSc1d1	epická
báseň	báseň	k1gFnSc1	báseň
Poezie	poezie	k1gFnSc2	poezie
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
–	–	k?	–
lyrika	lyrik	k1gMnSc2	lyrik
Ossianův	Ossianův	k2eAgInSc4d1	Ossianův
návrat	návrat	k1gInSc4	návrat
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
,	,	kIx,	,
Světozor	světozor	k1gInSc1	světozor
<g/>
)	)	kIx)	)
–	–	k?	–
keltské	keltský	k2eAgInPc4d1	keltský
motivy	motiv	k1gInPc4	motiv
Z	z	k7c2	z
letopisů	letopis	k1gInPc2	letopis
lásky	láska	k1gFnSc2	láska
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
–	–	k?	–
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
Čechův	Čechův	k2eAgInSc1d1	Čechův
příchod	příchod	k1gInSc1	příchod
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
–	–	k?	–
z	z	k7c2	z
historie	historie	k1gFnSc2	historie
Čech	Čechy	k1gFnPc2	Čechy
Karolínská	karolínský	k2eAgFnSc1d1	karolínská
epopeja	epopeja	k1gFnSc1	epopeja
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
cyklus	cyklus	k1gInSc1	cyklus
básní	básnit	k5eAaImIp3nS	básnit
psaný	psaný	k2eAgInSc1d1	psaný
blankversem	blankvers	k1gInSc7	blankvers
<g/>
,	,	kIx,	,
středověké	středověký	k2eAgNnSc4d1	středověké
téma	téma	k1gNnSc4	téma
čerpající	čerpající	k2eAgNnSc4d1	čerpající
ze	z	k7c2	z
starofrancouzské	starofrancouzský	k2eAgFnSc2d1	starofrancouzská
epiky	epika	k1gFnSc2	epika
o	o	k7c6	o
Karlu	Karel	k1gMnSc6	Karel
Velikém	veliký	k2eAgMnSc6d1	veliký
a	a	k8xC	a
jeho	jeho	k3xOp3gNnPc2	jeho
rytířích	rytíř	k1gMnPc6	rytíř
Zahrada	zahrada	k1gFnSc1	zahrada
Mariánská	mariánský	k2eAgFnSc1d1	Mariánská
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
–	–	k?	–
<g/>
1898	[number]	k4	1898
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
životě	život	k1gInSc6	život
<g/>
)	)	kIx)	)
Nové	Nové	k2eAgFnPc1d1	Nové
básně	báseň	k1gFnPc1	báseň
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
–	–	k?	–
ohlasy	ohlas	k1gInPc1	ohlas
litevských	litevský	k2eAgInPc2d1	litevský
lidových	lidový	k2eAgInPc2d1	lidový
písní	píseň	k1gFnSc7	píseň
Zpěv	zpěv	k1gInSc1	zpěv
o	o	k7c6	o
pomstě	pomsta	k1gFnSc6	pomsta
za	za	k7c4	za
<g />
.	.	kIx.	.
</s>
<s>
Igora	Igor	k1gMnSc2	Igor
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
–	–	k?	–
ohlas	ohlas	k1gInSc4	ohlas
staroruské	staroruský	k2eAgFnSc2d1	staroruská
epiky	epika	k1gFnSc2	epika
Stará	starý	k2eAgFnSc1d1	stará
historie	historie	k1gFnSc1	historie
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
–	–	k?	–
komedie	komedie	k1gFnSc1	komedie
Sulamit	Sulamit	k1gFnSc1	Sulamit
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
–	–	k?	–
biblický	biblický	k2eAgInSc4d1	biblický
motiv	motiv	k1gInSc4	motiv
Legenda	legenda	k1gFnSc1	legenda
z	z	k7c2	z
Erinu	Erin	k1gInSc2	Erin
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
–	–	k?	–
inspirace	inspirace	k1gFnSc2	inspirace
keltskou	keltský	k2eAgFnSc7d1	keltská
mytologií	mytologie	k1gFnSc7	mytologie
<g/>
,	,	kIx,	,
tragédie	tragédie	k1gFnSc2	tragédie
Libušin	Libušin	k2eAgInSc1d1	Libušin
hněv	hněv	k1gInSc1	hněv
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
předání	předání	k1gNnSc1	předání
vlády	vláda	k1gFnSc2	vláda
po	po	k7c6	po
Krokově	krokově	k6eAd1	krokově
smrti	smrt	k1gFnSc2	smrt
jako	jako	k8xS	jako
naplnění	naplnění	k1gNnSc2	naplnění
věštby	věštba	k1gFnSc2	věštba
Doňa	doňa	k1gFnSc1	doňa
Sanča	Sanča	k1gFnSc1	Sanča
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
–	–	k?	–
drama	drama	k1gNnSc1	drama
o	o	k7c6	o
zklamané	zklamaný	k2eAgFnSc6d1	zklamaná
lásce	láska	k1gFnSc6	láska
a	a	k8xC	a
krvavé	krvavý	k2eAgFnSc6d1	krvavá
pomstě	pomsta	k1gFnSc6	pomsta
Neklan	Neklan	k1gMnSc1	Neklan
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
–	–	k?	–
období	období	k1gNnSc6	období
mocenských	mocenský	k2eAgInPc2d1	mocenský
bojů	boj	k1gInPc2	boj
o	o	k7c4	o
knížecí	knížecí	k2eAgInSc4d1	knížecí
stolec	stolec	k1gInSc4	stolec
a	a	k8xC	a
první	první	k4xOgNnSc4	první
pronikání	pronikání	k1gNnSc4	pronikání
křesťanství	křesťanství	k1gNnSc2	křesťanství
Radúz	Radúz	k1gMnSc1	Radúz
a	a	k8xC	a
Mahulena	Mahulena	k1gFnSc1	Mahulena
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
pohádkové	pohádkový	k2eAgNnSc1d1	pohádkové
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
scénickou	scénický	k2eAgFnSc4d1	scénická
hudbu	hudba	k1gFnSc4	hudba
ke	k	k7c3	k
hře	hra	k1gFnSc3	hra
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
Josef	Josef	k1gMnSc1	Josef
Suk	Suk	k1gMnSc1	Suk
Šárka	Šárka	k1gFnSc1	Šárka
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
)	)	kIx)	)
–	–	k?	–
konec	konec	k1gInSc1	konec
vlády	vláda	k1gFnSc2	vláda
pohanských	pohanský	k2eAgFnPc2d1	pohanská
kněžek	kněžka	k1gFnPc2	kněžka
a	a	k8xC	a
nástup	nástup	k1gInSc1	nástup
mužské	mužský	k2eAgFnSc2d1	mužská
vlády	vláda	k1gFnSc2	vláda
Bratří	bratr	k1gMnPc2	bratr
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
)	)	kIx)	)
–	–	k?	–
komedie	komedie	k1gFnSc2	komedie
Lásky	láska	k1gFnSc2	láska
div	div	k1gInSc1	div
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
)	)	kIx)	)
Z	z	k7c2	z
dob	doba	k1gFnPc2	doba
růžového	růžový	k2eAgNnSc2d1	růžové
jitra	jitro	k1gNnSc2	jitro
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
Příchod	příchod	k1gInSc4	příchod
ženichův	ženichův	k2eAgInSc4d1	ženichův
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
Pod	pod	k7c7	pod
jabloní	jabloň	k1gFnSc7	jabloň
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
)	)	kIx)	)
–	–	k?	–
mystická	mystický	k2eAgFnSc1d1	mystická
legenda	legenda	k1gFnSc1	legenda
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
srbské	srbský	k2eAgFnSc2d1	Srbská
pohádky	pohádka	k1gFnSc2	pohádka
<g/>
,	,	kIx,	,
scénickou	scénický	k2eAgFnSc4d1	scénická
hudbu	hudba	k1gFnSc4	hudba
opět	opět	k6eAd1	opět
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
Josef	Josef	k1gMnSc1	Josef
Suk	Suk	k1gMnSc1	Suk
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1878	[number]	k4	1878
se	se	k3xPyFc4	se
Julius	Julius	k1gMnSc1	Julius
Zeyer	Zeyer	k1gMnSc1	Zeyer
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
nastěhoval	nastěhovat	k5eAaPmAgMnS	nastěhovat
do	do	k7c2	do
vily	vila	k1gFnSc2	vila
v	v	k7c6	v
Libocké	libocký	k2eAgFnSc6d1	Libocká
16	[number]	k4	16
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
návštěvách	návštěva	k1gFnPc6	návštěva
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Markéty	Markéta	k1gFnSc2	Markéta
procházel	procházet	k5eAaImAgInS	procházet
po	po	k7c6	po
cestě	cesta	k1gFnSc6	cesta
stromovou	stromový	k2eAgFnSc7d1	stromová
alejí	alej	k1gFnSc7	alej
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
aleje	alej	k1gFnSc2	alej
zbyly	zbýt	k5eAaPmAgInP	zbýt
až	až	k6eAd1	až
do	do	k7c2	do
sklonku	sklonek	k1gInSc2	sklonek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tři	tři	k4xCgFnPc1	tři
třešně	třešeň	k1gFnPc1	třešeň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
také	také	k9	také
odstraněny	odstranit	k5eAaPmNgInP	odstranit
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
alej	alej	k1gFnSc1	alej
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
po	po	k7c6	po
Zeyerovi	Zeyer	k1gMnSc6	Zeyer
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
Zeyerova	Zeyerův	k2eAgFnSc1d1	Zeyerova
alej	alej	k1gFnSc1	alej
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
aleje	alej	k1gFnSc2	alej
nic	nic	k6eAd1	nic
nezbylo	zbýt	k5eNaPmAgNnS	zbýt
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
stromů	strom	k1gInPc2	strom
se	se	k3xPyFc4	se
na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
místě	místo	k1gNnSc6	místo
tyčí	tyčit	k5eAaImIp3nP	tyčit
sloupy	sloup	k1gInPc1	sloup
vysokého	vysoký	k2eAgNnSc2d1	vysoké
vedení	vedení	k1gNnSc2	vedení
<g/>
.	.	kIx.	.
</s>
