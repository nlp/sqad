<s>
Největším	veliký	k2eAgInSc7d3	veliký
románem	román	k1gInSc7	román
Julia	Julius	k1gMnSc2	Julius
Zeyera	Zeyer	k1gMnSc2	Zeyer
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
autobiograficky	autobiograficky	k6eAd1	autobiograficky
laděné	laděný	k2eAgNnSc1d1	laděné
dílo	dílo	k1gNnSc1	dílo
Jan	Jan	k1gMnSc1	Jan
Maria	Mario	k1gMnSc2	Mario
Plojhar	Plojhar	k1gMnSc1	Plojhar
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
také	také	k9	také
poprvé	poprvé	k6eAd1	poprvé
výrazněji	výrazně	k6eAd2	výrazně
objevují	objevovat	k5eAaImIp3nP	objevovat
prvky	prvek	k1gInPc1	prvek
mesianismu	mesianismus	k1gInSc2	mesianismus
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Zeyer	Zeyer	k1gMnSc1	Zeyer
přirovnává	přirovnávat	k5eAaImIp3nS	přirovnávat
osudy	osud	k1gInPc4	osud
české	český	k2eAgFnSc2d1	Česká
země	zem	k1gFnSc2	zem
k	k	k7c3	k
utrpení	utrpení	k1gNnSc3	utrpení
Kristovu	Kristův	k2eAgInSc3d1	Kristův
<g/>
.	.	kIx.	.
</s>
