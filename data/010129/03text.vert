<p>
<s>
Střevo	střevo	k1gNnSc1	střevo
(	(	kIx(	(
<g/>
intestinum	intestinum	k1gNnSc1	intestinum
<g/>
,	,	kIx,	,
enteron	enteron	k1gInSc1	enteron
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
úsek	úsek	k1gInSc1	úsek
trávicí	trávicí	k2eAgFnSc2d1	trávicí
trubice	trubice	k1gFnSc2	trubice
<g/>
.	.	kIx.	.
</s>
<s>
Navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
vrátník	vrátník	k1gInSc4	vrátník
žaludku	žaludek	k1gInSc2	žaludek
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
řitním	řitní	k2eAgInSc7d1	řitní
otvorem	otvor	k1gInSc7	otvor
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
orgán	orgán	k1gInSc1	orgán
přizpůsobený	přizpůsobený	k2eAgInSc1d1	přizpůsobený
trávení	trávení	k1gNnSc2	trávení
a	a	k8xC	a
vstřebávání	vstřebávání	k1gNnSc2	vstřebávání
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgFnPc2	některý
ryb	ryba	k1gFnPc2	ryba
slouží	sloužit	k5eAaImIp3nS	sloužit
střevo	střevo	k1gNnSc1	střevo
i	i	k8xC	i
k	k	k7c3	k
dýchání	dýchání	k1gNnSc3	dýchání
vzdušného	vzdušný	k2eAgInSc2d1	vzdušný
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
savců	savec	k1gMnPc2	savec
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
střevo	střevo	k1gNnSc4	střevo
rozdělujeme	rozdělovat	k5eAaImIp1nP	rozdělovat
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Tenké	tenký	k2eAgNnSc1d1	tenké
střevo	střevo	k1gNnSc1	střevo
(	(	kIx(	(
<g/>
intestinum	intestinum	k1gNnSc1	intestinum
tenue	tenu	k1gFnSc2	tenu
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tlusté	tlustý	k2eAgNnSc1d1	tlusté
střevo	střevo	k1gNnSc1	střevo
(	(	kIx(	(
<g/>
intestinum	intestinum	k1gNnSc1	intestinum
crassum	crassum	k1gNnSc1	crassum
<g/>
)	)	kIx)	)
<g/>
V	v	k7c6	v
tenkém	tenký	k2eAgNnSc6d1	tenké
střevě	střevo	k1gNnSc6	střevo
je	být	k5eAaImIp3nS	být
žaludkem	žaludek	k1gInSc7	žaludek
natrávená	natrávený	k2eAgFnSc1d1	natrávená
potrava	potrava	k1gFnSc1	potrava
dále	daleko	k6eAd2	daleko
štěpena	štěpen	k2eAgFnSc1d1	štěpena
enzymy	enzym	k1gInPc4	enzym
slinivky	slinivka	k1gFnPc1	slinivka
břišní	břišní	k2eAgFnPc1d1	břišní
<g/>
,	,	kIx,	,
smísena	smísit	k5eAaPmNgFnS	smísit
se	s	k7c7	s
žlučí	žluč	k1gFnSc7	žluč
a	a	k8xC	a
vstřebávána	vstřebáván	k2eAgFnSc1d1	vstřebávána
sliznicí	sliznice	k1gFnSc7	sliznice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tlustém	tlustý	k2eAgNnSc6d1	tlusté
střevě	střevo	k1gNnSc6	střevo
je	být	k5eAaImIp3nS	být
resorbována	resorbován	k2eAgFnSc1d1	resorbován
voda	voda	k1gFnSc1	voda
a	a	k8xC	a
trávenina	trávenina	k1gFnSc1	trávenina
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
zahušťuje	zahušťovat	k5eAaImIp3nS	zahušťovat
<g/>
,	,	kIx,	,
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
výkaly	výkal	k1gInPc1	výkal
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
vylučovány	vylučovat	k5eAaImNgInP	vylučovat
z	z	k7c2	z
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
také	také	k9	také
žijí	žít	k5eAaImIp3nP	žít
symbiotické	symbiotický	k2eAgFnPc1d1	symbiotická
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
jinak	jinak	k6eAd1	jinak
nestravitelné	stravitelný	k2eNgInPc1d1	nestravitelný
zbytky	zbytek	k1gInPc1	zbytek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
střeva	střevo	k1gNnSc2	střevo
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
7	[number]	k4	7
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
přes	přes	k7c4	přes
5	[number]	k4	5
metrů	metr	k1gInPc2	metr
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
tenké	tenký	k2eAgNnSc4d1	tenké
střevo	střevo	k1gNnSc4	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
relativní	relativní	k2eAgFnSc1d1	relativní
délka	délka	k1gFnSc1	délka
střeva	střevo	k1gNnSc2	střevo
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
potravě	potrava	k1gFnSc6	potrava
<g/>
,	,	kIx,	,
u	u	k7c2	u
masožravců	masožravec	k1gMnPc2	masožravec
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
troj	trojit	k5eAaImRp2nS	trojit
až	až	k6eAd1	až
pětinásobku	pětinásobek	k1gInSc3	pětinásobek
délky	délka	k1gFnSc2	délka
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
u	u	k7c2	u
koně	kůň	k1gMnSc2	kůň
desetinásobku	desetinásobek	k1gInSc2	desetinásobek
<g/>
,	,	kIx,	,
u	u	k7c2	u
prasete	prase	k1gNnSc2	prase
patnáctinásobku	patnáctinásobek	k1gInSc2	patnáctinásobek
u	u	k7c2	u
malých	malý	k2eAgMnPc2d1	malý
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
až	až	k6eAd1	až
dvacetipětinásobku	dvacetipětinásobek	k1gInSc2	dvacetipětinásobek
délky	délka	k1gFnSc2	délka
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
střevech	střevo	k1gNnPc6	střevo
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
u	u	k7c2	u
dravců	dravec	k1gMnPc2	dravec
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
tzv.	tzv.	kA	tzv.
pylorické	pylorický	k2eAgInPc1d1	pylorický
výběžky	výběžek	k1gInPc1	výběžek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
funkci	funkce	k1gFnSc4	funkce
sekrece	sekrece	k1gFnSc2	sekrece
trávících	trávící	k2eAgInPc2d1	trávící
enzymů	enzym	k1gInPc2	enzym
</s>
</p>
<p>
<s>
==	==	k?	==
Střevo	střevo	k1gNnSc1	střevo
ptáků	pták	k1gMnPc2	pták
==	==	k?	==
</s>
</p>
<p>
<s>
I	i	k9	i
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
tenké	tenký	k2eAgNnSc4d1	tenké
a	a	k8xC	a
tlusté	tlustý	k2eAgNnSc4d1	tlusté
střevo	střevo	k1gNnSc4	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Tlusté	tlustý	k2eAgNnSc1d1	tlusté
střevo	střevo	k1gNnSc1	střevo
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
kloaky	kloaka	k1gFnSc2	kloaka
<g/>
,	,	kIx,	,
společného	společný	k2eAgNnSc2d1	společné
vyústění	vyústění	k1gNnSc2	vyústění
trávicí	trávicí	k2eAgFnSc2d1	trávicí
<g/>
,	,	kIx,	,
močové	močový	k2eAgFnSc2d1	močová
a	a	k8xC	a
pohlavní	pohlavní	k2eAgFnSc2d1	pohlavní
soustavy	soustava	k1gFnSc2	soustava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Střevo	střevo	k1gNnSc4	střevo
u	u	k7c2	u
nižších	nízký	k2eAgMnPc2d2	nižší
živočichů	živočich	k1gMnPc2	živočich
==	==	k?	==
</s>
</p>
<p>
<s>
Trávicí	trávicí	k2eAgFnSc4d1	trávicí
trubici	trubice	k1gFnSc4	trubice
prakticky	prakticky	k6eAd1	prakticky
všech	všecek	k3xTgMnPc2	všecek
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
oddělený	oddělený	k2eAgInSc4d1	oddělený
přijímací	přijímací	k2eAgInSc4d1	přijímací
i	i	k8xC	i
vyvrhovací	vyvrhovací	k2eAgInSc4d1	vyvrhovací
otvor	otvor	k1gInSc4	otvor
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
nazývat	nazývat	k5eAaImF	nazývat
střevem	střevo	k1gNnSc7	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
takového	takový	k3xDgNnSc2	takový
střeva	střevo	k1gNnSc2	střevo
je	být	k5eAaImIp3nS	být
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
u	u	k7c2	u
střeva	střevo	k1gNnSc2	střevo
savců	savec	k1gMnPc2	savec
–	–	k?	–
trávit	trávit	k5eAaImF	trávit
a	a	k8xC	a
přijímat	přijímat	k5eAaImF	přijímat
živiny	živina	k1gFnPc4	živina
z	z	k7c2	z
přijaté	přijatý	k2eAgFnSc2d1	přijatá
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
</p>
