<p>
<s>
Estonsko	Estonsko	k1gNnSc1	Estonsko
(	(	kIx(	(
<g/>
estonsky	estonsky	k6eAd1	estonsky
Eesti	Eesti	k1gNnSc1	Eesti
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oficiálním	oficiální	k2eAgInSc7d1	oficiální
názvem	název	k1gInSc7	název
Estonská	estonský	k2eAgFnSc1d1	Estonská
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
Eesti	Eesti	k1gNnSc1	Eesti
Vabariik	Vabariika	k1gFnPc2	Vabariika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
území	území	k1gNnSc4	území
a	a	k8xC	a
stát	stát	k5eAaPmF	stát
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
nejsevernější	severní	k2eAgInSc1d3	nejsevernější
z	z	k7c2	z
pobaltských	pobaltský	k2eAgFnPc2d1	pobaltská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
přímořský	přímořský	k2eAgInSc4d1	přímořský
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
hranici	hranice	k1gFnSc4	hranice
tvoří	tvořit	k5eAaImIp3nS	tvořit
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
Baltské	baltský	k2eAgNnSc4d1	Baltské
moře	moře	k1gNnSc4	moře
<g/>
;	;	kIx,	;
80	[number]	k4	80
km	km	kA	km
široký	široký	k2eAgInSc1d1	široký
Finský	finský	k2eAgInSc1d1	finský
záliv	záliv	k1gInSc1	záliv
na	na	k7c6	na
severu	sever	k1gInSc6	sever
je	být	k5eAaImIp3nS	být
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
od	od	k7c2	od
Finska	Finsko	k1gNnSc2	Finsko
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
300	[number]	k4	300
km	km	kA	km
moře	moře	k1gNnSc1	moře
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
západě	západ	k1gInSc6	západ
dělí	dělit	k5eAaImIp3nS	dělit
od	od	k7c2	od
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Suchozemskou	suchozemský	k2eAgFnSc4d1	suchozemská
hranici	hranice	k1gFnSc4	hranice
má	mít	k5eAaImIp3nS	mít
Estonsko	Estonsko	k1gNnSc1	Estonsko
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
této	tento	k3xDgFnSc2	tento
hranice	hranice	k1gFnSc2	hranice
prochází	procházet	k5eAaImIp3nS	procházet
rozlehlým	rozlehlý	k2eAgNnSc7d1	rozlehlé
Čudským	čudský	k2eAgNnSc7d1	Čudské
jezerem	jezero	k1gNnSc7	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
suchozemská	suchozemský	k2eAgFnSc1d1	suchozemská
hranice	hranice	k1gFnSc1	hranice
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Lotyšskem	Lotyšsko	k1gNnSc7	Lotyšsko
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Tallinn	Tallinn	k1gNnSc1	Tallinn
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Většinu	většina	k1gFnSc4	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
Estonska	Estonsko	k1gNnSc2	Estonsko
tvoří	tvořit	k5eAaImIp3nP	tvořit
Estonci	Estonec	k1gMnPc1	Estonec
<g/>
,	,	kIx,	,
hovořící	hovořící	k2eAgInSc1d1	hovořící
estonštinou	estonština	k1gFnSc7	estonština
<g/>
,	,	kIx,	,
uralským	uralský	k2eAgInSc7d1	uralský
jazykem	jazyk	k1gInSc7	jazyk
blízkým	blízký	k2eAgFnPc3d1	blízká
finštině	finština	k1gFnSc6	finština
<g/>
,	,	kIx,	,
estonsky	estonsky	k6eAd1	estonsky
hovoří	hovořit	k5eAaImIp3nS	hovořit
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zemi	zem	k1gFnSc6	zem
85	[number]	k4	85
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
Estonci	Estonec	k1gMnPc1	Estonec
tvoří	tvořit	k5eAaImIp3nP	tvořit
pouze	pouze	k6eAd1	pouze
70	[number]	k4	70
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sovětské	sovětský	k2eAgFnSc2d1	sovětská
okupace	okupace	k1gFnSc2	okupace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
žije	žít	k5eAaImIp3nS	žít
též	též	k9	též
početná	početný	k2eAgFnSc1d1	početná
ruská	ruský	k2eAgFnSc1d1	ruská
menšina	menšina	k1gFnSc1	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
průmyslovými	průmyslový	k2eAgFnPc7d1	průmyslová
odvětvími	odvětví	k1gNnPc7	odvětví
jsou	být	k5eAaImIp3nP	být
průmysl	průmysl	k1gInSc1	průmysl
potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
a	a	k8xC	a
dřevozpracující	dřevozpracující	k2eAgInSc1d1	dřevozpracující
<g/>
,	,	kIx,	,
založené	založený	k2eAgInPc1d1	založený
na	na	k7c6	na
rozvinutém	rozvinutý	k2eAgNnSc6d1	rozvinuté
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
lesnictví	lesnictví	k1gNnSc6	lesnictví
a	a	k8xC	a
rybolovu	rybolov	k1gInSc6	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgMnPc7d1	hlavní
obchodními	obchodní	k2eAgMnPc7d1	obchodní
partnery	partner	k1gMnPc7	partner
jsou	být	k5eAaImIp3nP	být
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
a	a	k8xC	a
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
podíl	podíl	k1gInSc1	podíl
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
na	na	k7c6	na
zahraničním	zahraniční	k2eAgInSc6d1	zahraniční
obchodu	obchod	k1gInSc6	obchod
stále	stále	k6eAd1	stále
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Estonsko	Estonsko	k1gNnSc1	Estonsko
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
členem	člen	k1gMnSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
platí	platit	k5eAaImIp3nS	platit
eurem	euro	k1gNnSc7	euro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgInPc1d1	přírodní
poměry	poměr	k1gInPc1	poměr
==	==	k?	==
</s>
</p>
<p>
<s>
Estonsko	Estonsko	k1gNnSc1	Estonsko
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
estonsky	estonsky	k6eAd1	estonsky
Läänemeri	Läänemeri	k1gNnSc1	Läänemeri
<g/>
)	)	kIx)	)
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
Východoevropské	východoevropský	k2eAgFnSc2d1	východoevropská
roviny	rovina	k1gFnSc2	rovina
mezi	mezi	k7c7	mezi
57,3	[number]	k4	57,3
<g/>
°	°	k?	°
a	a	k8xC	a
59,5	[number]	k4	59,5
<g/>
°	°	k?	°
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
21,5	[number]	k4	21,5
<g/>
°	°	k?	°
a	a	k8xC	a
28,1	[number]	k4	28,1
<g/>
°	°	k?	°
východní	východní	k2eAgFnSc2d1	východní
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
činí	činit	k5eAaImIp3nS	činit
pouhých	pouhý	k2eAgInPc2d1	pouhý
50	[number]	k4	50
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
;	;	kIx,	;
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
Suur	Suur	k1gInSc1	Suur
Munamägi	Munamäg	k1gFnSc2	Munamäg
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
(	(	kIx(	(
<g/>
318	[number]	k4	318
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
tuto	tento	k3xDgFnSc4	tento
nízkou	nízký	k2eAgFnSc4d1	nízká
nadmořskou	nadmořský	k2eAgFnSc4d1	nadmořská
výšku	výška	k1gFnSc4	výška
je	být	k5eAaImIp3nS	být
reliéf	reliéf	k1gInSc1	reliéf
členěný	členěný	k2eAgInSc1d1	členěný
hranami	hrana	k1gFnPc7	hrana
jednotlivými	jednotlivý	k2eAgMnPc7d1	jednotlivý
geologických	geologický	k2eAgFnPc2d1	geologická
vrstev	vrstva	k1gFnPc2	vrstva
a	a	k8xC	a
povětšinou	povětšinou	k6eAd1	povětšinou
zvlněný	zvlněný	k2eAgInSc4d1	zvlněný
působením	působení	k1gNnSc7	působení
pevninského	pevninský	k2eAgInSc2d1	pevninský
ledovce	ledovec	k1gInSc2	ledovec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
málo	málo	k1gNnSc1	málo
nerostného	nerostný	k2eAgNnSc2d1	nerostné
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
hospodářství	hospodářství	k1gNnSc4	hospodářství
významnější	významný	k2eAgFnSc2d2	významnější
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
ložiska	ložisko	k1gNnSc2	ložisko
ropných	ropný	k2eAgFnPc2d1	ropná
břidlic	břidlice	k1gFnPc2	břidlice
a	a	k8xC	a
vápence	vápenec	k1gInSc2	vápenec
<g/>
.	.	kIx.	.
61	[number]	k4	61
<g/>
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
země	zem	k1gFnSc2	zem
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
lesy	les	k1gInPc1	les
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Estonsku	Estonsko	k1gNnSc6	Estonsko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1400	[number]	k4	1400
jezer	jezero	k1gNnPc2	jezero
a	a	k8xC	a
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
močálů	močál	k1gInPc2	močál
<g/>
,	,	kIx,	,
rašelinišť	rašeliniště	k1gNnPc2	rašeliniště
a	a	k8xC	a
bažin	bažina	k1gFnPc2	bažina
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Estonska	Estonsko	k1gNnSc2	Estonsko
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
5	[number]	k4	5
rozsáhlých	rozsáhlý	k2eAgInPc2d1	rozsáhlý
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
přizpůsobeny	přizpůsobit	k5eAaPmNgInP	přizpůsobit
pro	pro	k7c4	pro
turismus	turismus	k1gInSc4	turismus
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jezer	jezero	k1gNnPc2	jezero
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
malých	malý	k2eAgFnPc2d1	malá
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
největší	veliký	k2eAgNnSc4d3	veliký
Čudské	čudský	k2eAgNnSc4d1	Čudské
jezero	jezero	k1gNnSc4	jezero
(	(	kIx(	(
<g/>
Peipsi	Peipse	k1gFnSc4	Peipse
järv	järva	k1gFnPc2	järva
<g/>
)	)	kIx)	)
ležící	ležící	k2eAgFnSc2d1	ležící
na	na	k7c6	na
východě	východ	k1gInSc6	východ
země	zem	k1gFnSc2	zem
zabírá	zabírat	k5eAaImIp3nS	zabírat
úctyhodných	úctyhodný	k2eAgInPc2d1	úctyhodný
3555	[number]	k4	3555
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1	pobřeží
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
3794	[number]	k4	3794
km	km	kA	km
je	být	k5eAaImIp3nS	být
členité	členitý	k2eAgNnSc1d1	členité
<g/>
,	,	kIx,	,
s	s	k7c7	s
množstvím	množství	k1gNnSc7	množství
zálivů	záliv	k1gInPc2	záliv
<g/>
,	,	kIx,	,
zátok	zátoka	k1gFnPc2	zátoka
a	a	k8xC	a
průlivů	průliv	k1gInPc2	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1500	[number]	k4	1500
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
jsou	být	k5eAaImIp3nP	být
největší	veliký	k2eAgNnPc1d3	veliký
Saaremaa	Saaremaum	k1gNnPc1	Saaremaum
a	a	k8xC	a
Hiiumaa	Hiiumaa	k1gFnSc1	Hiiumaa
<g/>
.	.	kIx.	.
<g/>
Podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
mírné	mírný	k2eAgNnSc1d1	mírné
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
poněkud	poněkud	k6eAd1	poněkud
chladnější	chladný	k2eAgMnSc1d2	chladnější
než	než	k8xS	než
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
přibližně	přibližně	k6eAd1	přibližně
stejně	stejně	k6eAd1	stejně
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
ročními	roční	k2eAgNnPc7d1	roční
obdobími	období	k1gNnPc7	období
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
16,3	[number]	k4	16,3
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
do	do	k7c2	do
17,1	[number]	k4	17,1
°	°	k?	°
<g/>
C	C	kA	C
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
nejteplejším	teplý	k2eAgInSc7d3	nejteplejší
měsícem	měsíc	k1gInSc7	měsíc
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
-3,5	-3,5	k4	-3,5
°	°	k?	°
<g/>
C	C	kA	C
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
do	do	k7c2	do
-7,6	-7,6	k4	-7,6
°	°	k?	°
<g/>
C	C	kA	C
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
nejchladnější	chladný	k2eAgInSc1d3	nejchladnější
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
srážkový	srážkový	k2eAgInSc1d1	srážkový
úhrn	úhrn	k1gInSc1	úhrn
je	být	k5eAaImIp3nS	být
568	[number]	k4	568
milimetrů	milimetr	k1gInPc2	milimetr
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejvíce	nejvíce	k6eAd1	nejvíce
srážek	srážka	k1gFnPc2	srážka
spadne	spadnout	k5eAaPmIp3nS	spadnout
v	v	k7c6	v
pozdním	pozdní	k2eAgNnSc6d1	pozdní
létě	léto	k1gNnSc6	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
příležitost	příležitost	k1gFnSc1	příležitost
k	k	k7c3	k
osídlení	osídlení	k1gNnSc3	osídlení
Estonska	Estonsko	k1gNnSc2	Estonsko
se	se	k3xPyFc4	se
naskytla	naskytnout	k5eAaPmAgFnS	naskytnout
<g/>
,	,	kIx,	,
když	když	k8xS	když
přibližně	přibližně	k6eAd1	přibližně
před	před	k7c7	před
11	[number]	k4	11
000	[number]	k4	000
–	–	k?	–
13	[number]	k4	13
000	[number]	k4	000
lety	let	k1gInPc7	let
z	z	k7c2	z
území	území	k1gNnSc2	území
ustoupil	ustoupit	k5eAaPmAgInS	ustoupit
pevninský	pevninský	k2eAgInSc1d1	pevninský
ledovec	ledovec	k1gInSc1	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Stopy	stopa	k1gFnPc1	stopa
nejstaršího	starý	k2eAgNnSc2d3	nejstarší
známého	známý	k2eAgNnSc2d1	známé
osídlení	osídlení	k1gNnSc2	osídlení
v	v	k7c6	v
Estonsku	Estonsko	k1gNnSc6	Estonsko
byly	být	k5eAaImAgInP	být
nalezeny	nalézt	k5eAaBmNgInP	nalézt
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Pärnu	Pärn	k1gInSc2	Pärn
<g/>
,	,	kIx,	,
v	v	k7c6	v
katastru	katastr	k1gInSc6	katastr
vesnice	vesnice	k1gFnSc2	vesnice
Pulli	Pulle	k1gFnSc4	Pulle
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Sauga	Saug	k1gMnSc2	Saug
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
sídliště	sídliště	k1gNnSc1	sídliště
je	být	k5eAaImIp3nS	být
datováno	datovat	k5eAaImNgNnS	datovat
do	do	k7c2	do
9	[number]	k4	9
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
</p>
<p>
<s>
Nepočítáme	počítat	k5eNaImIp1nP	počítat
<g/>
-li	i	k?	-li
několik	několik	k4yIc4	několik
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
zmínek	zmínka	k1gFnPc2	zmínka
<g/>
,	,	kIx,	,
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
Estonsko	Estonsko	k1gNnSc1	Estonsko
do	do	k7c2	do
historických	historický	k2eAgInPc2d1	historický
pramenů	pramen	k1gInPc2	pramen
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
12	[number]	k4	12
<g/>
.	.	kIx.	.
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
dobýváním	dobývání	k1gNnSc7	dobývání
převážně	převážně	k6eAd1	převážně
pohanského	pohanský	k2eAgInSc2d1	pohanský
Pobaltí	Pobaltí	k1gNnSc1	Pobaltí
křesťanskými	křesťanský	k2eAgFnPc7d1	křesťanská
mocnostmi	mocnost	k1gFnPc7	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Estonska	Estonsko	k1gNnSc2	Estonsko
bylo	být	k5eAaImAgNnS	být
dobyvateli	dobyvatel	k1gMnSc3	dobyvatel
začleněno	začleněn	k2eAgNnSc1d1	začleněno
do	do	k7c2	do
nově	nova	k1gFnSc3	nova
vyhlášené	vyhlášený	k2eAgFnSc2d1	vyhlášená
Mariiny	Mariin	k2eAgFnSc2d1	Mariina
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
Terra	Terra	k1gFnSc1	Terra
Mariana	Mariana	k1gFnSc1	Mariana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsáhla	obsáhnout	k5eAaPmAgFnS	obsáhnout
celé	celý	k2eAgNnSc4d1	celé
Estonsko	Estonsko	k1gNnSc4	Estonsko
roku	rok	k1gInSc2	rok
1227	[number]	k4	1227
<g/>
,	,	kIx,	,
když	když	k8xS	když
Řád	řád	k1gInSc1	řád
mečových	mečový	k2eAgMnPc2d1	mečový
bratří	bratr	k1gMnPc2	bratr
(	(	kIx(	(
<g/>
Němečtí	německý	k2eAgMnPc1d1	německý
rytíři	rytíř	k1gMnPc1	rytíř
<g/>
)	)	kIx)	)
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
ostrov	ostrov	k1gInSc4	ostrov
Saaremaa	Saarema	k1gInSc2	Saarema
a	a	k8xC	a
sever	sever	k1gInSc1	sever
Estonska	Estonsko	k1gNnSc2	Estonsko
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Lyndanisse	Lyndanisse	k1gFnSc2	Lyndanisse
dobyt	dobyt	k2eAgInSc1d1	dobyt
dánskými	dánský	k2eAgInPc7d1	dánský
křižáky	křižák	k1gInPc7	křižák
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
králem	král	k1gMnSc7	král
Valdemarem	Valdemar	k1gMnSc7	Valdemar
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Terry	Terra	k1gFnSc2	Terra
Mariany	Mariana	k1gFnSc2	Mariana
byly	být	k5eAaImAgFnP	být
části	část	k1gFnPc1	část
Estonska	Estonsko	k1gNnSc2	Estonsko
poddány	poddán	k2eAgFnPc1d1	poddána
Řádu	řád	k1gInSc6	řád
mečových	mečový	k2eAgMnPc2d1	mečový
bratří	bratr	k1gMnPc2	bratr
(	(	kIx(	(
<g/>
posléze	posléze	k6eAd1	posléze
Livonskému	Livonský	k2eAgInSc3d1	Livonský
řádu	řád	k1gInSc3	řád
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Dánsku	Dánsko	k1gNnSc6	Dánsko
a	a	k8xC	a
pobaltským	pobaltský	k2eAgNnSc7d1	pobaltské
biskupstvím	biskupství	k1gNnSc7	biskupství
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc4d1	další
expanzi	expanze	k1gFnSc4	expanze
na	na	k7c4	na
východ	východ	k1gInSc4	východ
zastavil	zastavit	k5eAaPmAgInS	zastavit
roku	rok	k1gInSc2	rok
1242	[number]	k4	1242
ruský	ruský	k2eAgMnSc1d1	ruský
kníže	kníže	k1gMnSc1	kníže
Alexandr	Alexandr	k1gMnSc1	Alexandr
Něvský	něvský	k2eAgMnSc1d1	něvský
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Čudském	čudský	k2eAgNnSc6d1	Čudské
jezeře	jezero	k1gNnSc6	jezero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
po	po	k7c6	po
rozpadu	rozpad	k1gInSc6	rozpad
Livonské	Livonský	k2eAgFnSc2d1	Livonská
konfederace	konfederace	k1gFnSc2	konfederace
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc2d1	poslední
státní	státní	k2eAgFnSc2d1	státní
formy	forma	k1gFnSc2	forma
Terry	Terra	k1gFnSc2	Terra
Mariany	Mariana	k1gFnSc2	Mariana
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
většina	většina	k1gFnSc1	většina
estonského	estonský	k2eAgNnSc2d1	Estonské
území	území	k1gNnSc2	území
součástí	součást	k1gFnPc2	součást
Švédské	švédský	k2eAgFnSc2d1	švédská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc6d1	jižní
část	část	k1gFnSc4	část
součástí	součást	k1gFnPc2	součást
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
opakovaných	opakovaný	k2eAgInPc6d1	opakovaný
ozbrojených	ozbrojený	k2eAgInPc6d1	ozbrojený
konfliktech	konflikt	k1gInPc6	konflikt
roku	rok	k1gInSc2	rok
1629	[number]	k4	1629
postoupilo	postoupit	k5eAaPmAgNnS	postoupit
poražené	poražený	k2eAgNnSc1d1	poražené
Polsko	Polsko	k1gNnSc1	Polsko
Altmarským	Altmarský	k2eAgInSc7d1	Altmarský
mírem	mír	k1gInSc7	mír
Švédsku	Švédsko	k1gNnSc6	Švédsko
i	i	k9	i
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Estonska	Estonsko	k1gNnSc2	Estonsko
<g/>
.	.	kIx.	.
</s>
<s>
Hladomor	hladomor	k1gInSc1	hladomor
v	v	k7c6	v
letech	let	k1gInPc6	let
1695	[number]	k4	1695
<g/>
–	–	k?	–
<g/>
1697	[number]	k4	1697
zahubil	zahubit	k5eAaPmAgInS	zahubit
okolo	okolo	k7c2	okolo
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
estonské	estonský	k2eAgFnSc2d1	Estonská
a	a	k8xC	a
livonské	livonský	k2eAgFnSc2d1	livonská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
Švédska	Švédsko	k1gNnSc2	Švédsko
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
Estonsko	Estonsko	k1gNnSc1	Estonsko
přiřčeno	přiřknout	k5eAaPmNgNnS	přiřknout
roku	rok	k1gInSc2	rok
1721	[number]	k4	1721
Nystadskou	Nystadský	k2eAgFnSc7d1	Nystadský
smlouvou	smlouva	k1gFnSc7	smlouva
k	k	k7c3	k
Ruské	ruský	k2eAgFnSc3d1	ruská
říši	říš	k1gFnSc3	říš
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
však	však	k9	však
v	v	k7c4	v
zemi	zem	k1gFnSc4	zem
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
vůdčí	vůdčí	k2eAgFnSc7d1	vůdčí
vnitropolitickou	vnitropolitický	k2eAgFnSc7d1	vnitropolitická
silou	síla	k1gFnSc7	síla
německá	německý	k2eAgFnSc1d1	německá
šlechta	šlechta	k1gFnSc1	šlechta
a	a	k8xC	a
měšťanstvo	měšťanstvo	k1gNnSc1	měšťanstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
carského	carský	k2eAgNnSc2d1	carské
Ruska	Rusko	k1gNnSc2	Rusko
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
Velké	velký	k2eAgFnSc2d1	velká
říjnové	říjnový	k2eAgFnSc2d1	říjnová
revoluce	revoluce	k1gFnSc2	revoluce
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
Estonsko	Estonsko	k1gNnSc1	Estonsko
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1918	[number]	k4	1918
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yRnSc4	což
sovětské	sovětský	k2eAgNnSc4d1	sovětské
Rusko	Rusko	k1gNnSc4	Rusko
i	i	k8xC	i
císařské	císařský	k2eAgNnSc4d1	císařské
Německo	Německo	k1gNnSc4	Německo
reagovaly	reagovat	k5eAaBmAgInP	reagovat
vojenským	vojenský	k2eAgInSc7d1	vojenský
útokem	útok	k1gInSc7	útok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
válce	válka	k1gFnSc6	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
Estonsko	Estonsko	k1gNnSc4	Estonsko
zvítězilo	zvítězit	k5eAaPmAgNnS	zvítězit
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc3	jeho
suverenita	suverenita	k1gFnSc1	suverenita
a	a	k8xC	a
hranice	hranice	k1gFnSc1	hranice
vůči	vůči	k7c3	vůči
Rusku	Rusko	k1gNnSc3	Rusko
byla	být	k5eAaImAgFnS	být
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
Tartuskou	tartuský	k2eAgFnSc7d1	Tartuská
smlouvou	smlouva	k1gFnSc7	smlouva
<g/>
,	,	kIx,	,
podepsanou	podepsaný	k2eAgFnSc4d1	podepsaná
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Estonsko	Estonsko	k1gNnSc1	Estonsko
si	se	k3xPyFc3	se
udrželo	udržet	k5eAaPmAgNnS	udržet
nezávislost	nezávislost	k1gFnSc4	nezávislost
22	[number]	k4	22
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
bylo	být	k5eAaImAgNnS	být
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
republikou	republika	k1gFnSc7	republika
<g/>
;	;	kIx,	;
národní	národní	k2eAgInSc1d1	národní
parlament	parlament	k1gInSc1	parlament
(	(	kIx(	(
<g/>
Riigikogu	Riigikog	k1gInSc2	Riigikog
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
volen	volit	k5eAaImNgMnS	volit
občany	občan	k1gMnPc7	občan
staršími	starý	k2eAgMnPc7d2	starší
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
pokus	pokus	k1gInSc4	pokus
o	o	k7c4	o
fašistický	fašistický	k2eAgInSc4d1	fašistický
převrat	převrat	k1gInSc4	převrat
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
prezident	prezident	k1gMnSc1	prezident
Konstantin	Konstantin	k1gMnSc1	Konstantin
Päts	Päts	k1gInSc4	Päts
autoritářský	autoritářský	k2eAgInSc4d1	autoritářský
režim	režim	k1gInSc4	režim
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
trval	trvat	k5eAaImAgInS	trvat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1938	[number]	k4	1938
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
<g/>
:	:	kIx,	:
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
Hitler	Hitler	k1gMnSc1	Hitler
o	o	k7c6	o
vystěhování	vystěhování	k1gNnSc6	vystěhování
Baltských	baltský	k2eAgMnPc2d1	baltský
Němců	Němec	k1gMnPc2	Němec
z	z	k7c2	z
Estonska	Estonsko	k1gNnSc2	Estonsko
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
jich	on	k3xPp3gInPc2	on
poslechla	poslechnout	k5eAaPmAgFnS	poslechnout
a	a	k8xC	a
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
přestěhováno	přestěhován	k2eAgNnSc4d1	přestěhováno
cca	cca	kA	cca
14	[number]	k4	14
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
vystěhoval	vystěhovat	k5eAaPmAgInS	vystěhovat
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
Stalin	Stalin	k1gMnSc1	Stalin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
tajného	tajný	k2eAgInSc2d1	tajný
dodatku	dodatek	k1gInSc2	dodatek
paktu	pakt	k1gInSc2	pakt
Ribbentrop-Molotov	Ribbentrop-Molotov	k1gInSc1	Ribbentrop-Molotov
uzavřeného	uzavřený	k2eAgInSc2d1	uzavřený
mezi	mezi	k7c7	mezi
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
a	a	k8xC	a
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
Německem	Německo	k1gNnSc7	Německo
byla	být	k5eAaImAgFnS	být
země	země	k1gFnSc1	země
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1940	[number]	k4	1940
obsazena	obsadit	k5eAaPmNgFnS	obsadit
sovětskými	sovětský	k2eAgNnPc7d1	sovětské
vojsky	vojsko	k1gNnPc7	vojsko
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Estonská	estonský	k2eAgFnSc1d1	Estonská
sovětská	sovětský	k2eAgFnSc1d1	sovětská
socialistická	socialistický	k2eAgFnSc1d1	socialistická
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
počátkem	počátkem	k7c2	počátkem
srpna	srpen	k1gInSc2	srpen
1940	[number]	k4	1940
připojena	připojit	k5eAaPmNgFnS	připojit
k	k	k7c3	k
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
nastala	nastat	k5eAaPmAgFnS	nastat
doba	doba	k1gFnSc1	doba
těžkých	těžký	k2eAgFnPc2d1	těžká
stalinských	stalinský	k2eAgFnPc2d1	stalinská
represí	represe	k1gFnPc2	represe
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
intelektuálních	intelektuální	k2eAgMnPc2d1	intelektuální
a	a	k8xC	a
politických	politický	k2eAgMnPc2d1	politický
vůdců	vůdce	k1gMnPc2	vůdce
popraveno	popravit	k5eAaPmNgNnS	popravit
<g/>
,	,	kIx,	,
uvězněno	uvěznit	k5eAaPmNgNnS	uvěznit
nebo	nebo	k8xC	nebo
deportováno	deportovat	k5eAaBmNgNnS	deportovat
na	na	k7c4	na
Sibiř	Sibiř	k1gFnSc4	Sibiř
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
prezidenta	prezident	k1gMnSc2	prezident
Konstantina	Konstantin	k1gMnSc2	Konstantin
Pätse	Päts	k1gMnSc2	Päts
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1941	[number]	k4	1941
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
byla	být	k5eAaImAgFnS	být
země	země	k1gFnSc1	země
okupována	okupovat	k5eAaBmNgFnS	okupovat
německou	německý	k2eAgFnSc7d1	německá
Třetí	třetí	k4xOgFnSc7	třetí
říší	říš	k1gFnSc7	říš
a	a	k8xC	a
začleněna	začleněn	k2eAgFnSc1d1	začleněna
do	do	k7c2	do
říšského	říšský	k2eAgInSc2d1	říšský
komisariátu	komisariát	k1gInSc2	komisariát
Ostland	Ostlanda	k1gFnPc2	Ostlanda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vítězné	vítězný	k2eAgFnSc6d1	vítězná
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Generálního	generální	k2eAgInSc2d1	generální
plánu	plán	k1gInSc2	plán
Východ	východ	k1gInSc4	východ
plánovala	plánovat	k5eAaImAgFnS	plánovat
postupná	postupný	k2eAgFnSc1d1	postupná
germanizace	germanizace	k1gFnSc1	germanizace
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Židovská	židovský	k2eAgFnSc1d1	židovská
menšina	menšina	k1gFnSc1	menšina
byla	být	k5eAaImAgFnS	být
vyhlazena	vyhladit	k5eAaPmNgFnS	vyhladit
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
někteří	některý	k3yIgMnPc1	některý
Estonci	Estonec	k1gMnPc1	Estonec
vstupovali	vstupovat	k5eAaImAgMnP	vstupovat
do	do	k7c2	do
estonské	estonský	k2eAgFnSc2d1	Estonská
divize	divize	k1gFnSc2	divize
Waffen-SS	Waffen-SS	k1gFnSc2	Waffen-SS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1944	[number]	k4	1944
byla	být	k5eAaImAgFnS	být
země	země	k1gFnSc1	země
osvobozena	osvobodit	k5eAaPmNgFnS	osvobodit
Rudou	rudý	k2eAgFnSc7d1	rudá
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
46	[number]	k4	46
let	léto	k1gNnPc2	léto
sovětské	sovětský	k2eAgFnSc2d1	sovětská
okupace	okupace	k1gFnSc2	okupace
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc4	vytvoření
Estonské	estonský	k2eAgFnSc2d1	Estonská
SSR	SSR	kA	SSR
jako	jako	k8xS	jako
svazové	svazový	k2eAgFnSc2d1	svazová
republiky	republika	k1gFnSc2	republika
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Estonsko	Estonsko	k1gNnSc1	Estonsko
obnovilo	obnovit	k5eAaPmAgNnS	obnovit
svou	svůj	k3xOyFgFnSc4	svůj
nezávislost	nezávislost	k1gFnSc4	nezávislost
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
právně	právně	k6eAd1	právně
navázalo	navázat	k5eAaPmAgNnS	navázat
na	na	k7c4	na
předválečnou	předválečný	k2eAgFnSc4d1	předválečná
Estonskou	estonský	k2eAgFnSc4d1	Estonská
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
srpen	srpen	k1gInSc1	srpen
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
státním	státní	k2eAgInSc7d1	státní
svátkem	svátek	k1gInSc7	svátek
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nejdůležitější	důležitý	k2eAgInSc1d3	nejdůležitější
státní	státní	k2eAgInSc1d1	státní
svátek	svátek	k1gInSc1	svátek
nadále	nadále	k6eAd1	nadále
připadá	připadat	k5eAaImIp3nS	připadat
na	na	k7c4	na
den	den	k1gInSc4	den
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
ruské	ruský	k2eAgFnPc1d1	ruská
jednotky	jednotka	k1gFnPc1	jednotka
opustily	opustit	k5eAaPmAgFnP	opustit
zemi	zem	k1gFnSc4	zem
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Estonsko	Estonsko	k1gNnSc1	Estonsko
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
součástí	součást	k1gFnSc7	součást
NATO	NATO	kA	NATO
29	[number]	k4	29
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2004	[number]	k4	2004
a	a	k8xC	a
EU	EU	kA	EU
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
dlouhých	dlouhý	k2eAgNnPc6d1	dlouhé
letech	léto	k1gNnPc6	léto
sporů	spor	k1gInPc2	spor
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
Estonsko	Estonsko	k1gNnSc1	Estonsko
18	[number]	k4	18
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2005	[number]	k4	2005
hraniční	hraniční	k2eAgFnSc4d1	hraniční
dohodu	dohoda	k1gFnSc4	dohoda
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
lehce	lehko	k6eAd1	lehko
mění	měnit	k5eAaImIp3nP	měnit
hranici	hranice	k1gFnSc4	hranice
neformálně	formálně	k6eNd1	formálně
platnou	platný	k2eAgFnSc4d1	platná
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Estonský	estonský	k2eAgInSc1d1	estonský
parlament	parlament	k1gInSc1	parlament
Riigikogu	Riigikog	k1gInSc2	Riigikog
dohodu	dohoda	k1gFnSc4	dohoda
přijal	přijmout	k5eAaPmAgMnS	přijmout
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
nicméně	nicméně	k8xC	nicméně
k	k	k7c3	k
textu	text	k1gInSc3	text
přidal	přidat	k5eAaPmAgMnS	přidat
preambuli	preambule	k1gFnSc4	preambule
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
nepřerušenou	přerušený	k2eNgFnSc4d1	nepřerušená
právní	právní	k2eAgFnSc4d1	právní
kontinuitu	kontinuita	k1gFnSc4	kontinuita
Estonské	estonský	k2eAgFnSc2d1	Estonská
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
sovětské	sovětský	k2eAgFnSc2d1	sovětská
okupace	okupace	k1gFnSc2	okupace
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tento	tento	k3xDgInSc1	tento
odkaz	odkaz	k1gInSc1	odkaz
způsobil	způsobit	k5eAaPmAgInS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ruská	ruský	k2eAgFnSc1d1	ruská
strana	strana	k1gFnSc1	strana
následně	následně	k6eAd1	následně
stáhla	stáhnout	k5eAaPmAgFnS	stáhnout
svůj	svůj	k3xOyFgInSc4	svůj
podpis	podpis	k1gInSc4	podpis
z	z	k7c2	z
uzavřené	uzavřený	k2eAgFnSc2d1	uzavřená
dohody	dohoda	k1gFnSc2	dohoda
a	a	k8xC	a
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
přání	přání	k1gNnSc2	přání
otázku	otázka	k1gFnSc4	otázka
hranic	hranice	k1gFnPc2	hranice
znovu	znovu	k6eAd1	znovu
otevřít	otevřít	k5eAaPmF	otevřít
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
Estonsko	Estonsko	k1gNnSc1	Estonsko
opakovaně	opakovaně	k6eAd1	opakovaně
zdůraznilo	zdůraznit	k5eAaPmAgNnS	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vůči	vůči	k7c3	vůči
Rusku	Rusko	k1gNnSc3	Rusko
nemá	mít	k5eNaImIp3nS	mít
žádné	žádný	k3yNgInPc1	žádný
územní	územní	k2eAgInPc1d1	územní
nároky	nárok	k1gInPc1	nárok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
Estonsko	Estonsko	k1gNnSc1	Estonsko
je	být	k5eAaImIp3nS	být
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
republikou	republika	k1gFnSc7	republika
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gNnSc6	jejíž
čele	čelo	k1gNnSc6	čelo
stojí	stát	k5eAaImIp3nS	stát
prezident	prezident	k1gMnSc1	prezident
volený	volený	k2eAgMnSc1d1	volený
jednokomorovým	jednokomorový	k2eAgInSc7d1	jednokomorový
parlamentem	parlament	k1gInSc7	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
prezidenta	prezident	k1gMnSc2	prezident
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
let	léto	k1gNnPc2	léto
a	a	k8xC	a
jedna	jeden	k4xCgFnSc1	jeden
osoba	osoba	k1gFnSc1	osoba
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
prezidentem	prezident	k1gMnSc7	prezident
nanejvýš	nanejvýš	k6eAd1	nanejvýš
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
po	po	k7c6	po
sobě	se	k3xPyFc3	se
následujících	následující	k2eAgNnPc6d1	následující
funkčních	funkční	k2eAgNnPc6d1	funkční
obdobích	období	k1gNnPc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Vládu	vláda	k1gFnSc4	vláda
sestavuje	sestavovat	k5eAaImIp3nS	sestavovat
premiér	premiér	k1gMnSc1	premiér
jmenovaný	jmenovaný	k1gMnSc1	jmenovaný
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
ministrů	ministr	k1gMnPc2	ministr
není	být	k5eNaImIp3nS	být
pevně	pevně	k6eAd1	pevně
dán	dát	k5eAaPmNgInS	dát
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
měl	mít	k5eAaImAgInS	mít
kabinet	kabinet	k1gInSc1	kabinet
15	[number]	k4	15
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Vládu	vláda	k1gFnSc4	vláda
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
parlament	parlament	k1gInSc1	parlament
vyslovením	vyslovení	k1gNnSc7	vyslovení
důvěry	důvěra	k1gFnSc2	důvěra
většinou	většinou	k6eAd1	většinou
členů	člen	k1gMnPc2	člen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zákonodárná	zákonodárný	k2eAgFnSc1d1	zákonodárná
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
jednokomorového	jednokomorový	k2eAgInSc2d1	jednokomorový
parlamentu	parlament	k1gInSc2	parlament
zvaného	zvaný	k2eAgInSc2d1	zvaný
Riigikogu	Riigikog	k1gInSc2	Riigikog
<g/>
.	.	kIx.	.
</s>
<s>
Parlament	parlament	k1gInSc1	parlament
má	mít	k5eAaImIp3nS	mít
101	[number]	k4	101
poslanců	poslanec	k1gMnPc2	poslanec
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgMnS	volit
ve	v	k7c6	v
všeobecných	všeobecný	k2eAgFnPc6d1	všeobecná
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
každé	každý	k3xTgFnSc2	každý
4	[number]	k4	4
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
soudní	soudní	k2eAgFnSc7d1	soudní
instancí	instance	k1gFnSc7	instance
je	být	k5eAaImIp3nS	být
Národní	národní	k2eAgInSc1d1	národní
soud	soud	k1gInSc1	soud
(	(	kIx(	(
<g/>
Riigikohus	Riigikohus	k1gInSc1	Riigikohus
<g/>
)	)	kIx)	)
s	s	k7c7	s
19	[number]	k4	19
soudci	soudce	k1gMnPc7	soudce
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
soudu	soud	k1gInSc2	soud
je	být	k5eAaImIp3nS	být
nominován	nominován	k2eAgMnSc1d1	nominován
prezidentem	prezident	k1gMnSc7	prezident
a	a	k8xC	a
schvalován	schvalovat	k5eAaImNgInS	schvalovat
parlamentem	parlament	k1gInSc7	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgMnPc4d1	zbylý
soudce	soudce	k1gMnPc4	soudce
pak	pak	k6eAd1	pak
nominuje	nominovat	k5eAaBmIp3nS	nominovat
předseda	předseda	k1gMnSc1	předseda
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
potvrzení	potvrzení	k1gNnSc3	potvrzení
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
potřeba	potřeba	k6eAd1	potřeba
schválení	schválení	k1gNnSc1	schválení
parlamentem	parlament	k1gInSc7	parlament
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
běžného	běžný	k2eAgNnSc2d1	běžné
hlasování	hlasování	k1gNnSc2	hlasování
ve	v	k7c6	v
volební	volební	k2eAgFnSc6d1	volební
místnosti	místnost	k1gFnSc6	místnost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Estonsku	Estonsko	k1gNnSc6	Estonsko
možné	možný	k2eAgNnSc1d1	možné
hlasovat	hlasovat	k5eAaImF	hlasovat
i	i	k9	i
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
internetu	internet	k1gInSc2	internet
<g/>
.	.	kIx.	.
</s>
<s>
Elektronické	elektronický	k2eAgNnSc1d1	elektronické
hlasování	hlasování	k1gNnSc1	hlasování
bylo	být	k5eAaImAgNnS	být
poprvé	poprvé	k6eAd1	poprvé
použito	použít	k5eAaPmNgNnS	použít
v	v	k7c6	v
komunálních	komunální	k2eAgFnPc6d1	komunální
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Administrativní	administrativní	k2eAgNnSc4d1	administrativní
a	a	k8xC	a
samosprávné	samosprávný	k2eAgNnSc4d1	samosprávné
členění	členění	k1gNnSc4	členění
==	==	k?	==
</s>
</p>
<p>
<s>
Estonsko	Estonsko	k1gNnSc1	Estonsko
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
na	na	k7c4	na
15	[number]	k4	15
administrativních	administrativní	k2eAgFnPc2d1	administrativní
jednotek	jednotka	k1gFnPc2	jednotka
–	–	k?	–
krajů	kraj	k1gInPc2	kraj
(	(	kIx(	(
<g/>
estonsky	estonsky	k6eAd1	estonsky
maakond	maakond	k1gInSc1	maakond
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Harjumaa	Harjumaus	k1gMnSc4	Harjumaus
<g/>
,	,	kIx,	,
Hiiumaa	Hiiumaus	k1gMnSc4	Hiiumaus
<g/>
,	,	kIx,	,
Ida-Virumaa	Ida-Virumaus	k1gMnSc4	Ida-Virumaus
<g/>
,	,	kIx,	,
Järvamaa	Järvamaus	k1gMnSc4	Järvamaus
<g/>
,	,	kIx,	,
Jõ	Jõ	k1gMnSc4	Jõ
<g/>
,	,	kIx,	,
Läänemaa	Läänemaus	k1gMnSc4	Läänemaus
<g/>
,	,	kIx,	,
Lääne-Virumaa	Lääne-Virumaus	k1gMnSc4	Lääne-Virumaus
<g/>
,	,	kIx,	,
Pärnumaa	Pärnumaus	k1gMnSc4	Pärnumaus
<g/>
,	,	kIx,	,
Põ	Põ	k1gMnSc4	Põ
<g/>
,	,	kIx,	,
Raplamaa	Raplamaus	k1gMnSc4	Raplamaus
<g/>
,	,	kIx,	,
Saaremaa	Saaremaus	k1gMnSc4	Saaremaus
<g/>
,	,	kIx,	,
Tartumaa	Tartumaus	k1gMnSc4	Tartumaus
<g/>
,	,	kIx,	,
Valgamaa	Valgamaus	k1gMnSc4	Valgamaus
<g/>
,	,	kIx,	,
Viljandimaa	Viljandimaus	k1gMnSc4	Viljandimaus
a	a	k8xC	a
Võ	Võ	k1gMnSc4	Võ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kraje	kraj	k1gInPc1	kraj
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
samosprávné	samosprávný	k2eAgInPc4d1	samosprávný
celky	celek	k1gInPc4	celek
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgMnPc7	jenž
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
buď	buď	k8xC	buď
samosprávná	samosprávný	k2eAgNnPc4d1	samosprávné
statutární	statutární	k2eAgNnPc4d1	statutární
města	město	k1gNnPc4	město
(	(	kIx(	(
<g/>
estonsky	estonsky	k6eAd1	estonsky
linn	linn	k1gNnSc1	linn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
obce	obec	k1gFnSc2	obec
nemající	mající	k2eNgFnSc2d1	nemající
městský	městský	k2eAgInSc4d1	městský
status	status	k1gInSc4	status
(	(	kIx(	(
<g/>
estonsky	estonsky	k6eAd1	estonsky
vald	vald	k6eAd1	vald
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
sestávat	sestávat	k5eAaImF	sestávat
z	z	k7c2	z
jednoho	jeden	k4xCgMnSc2	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
sídel	sídlo	k1gNnPc2	sídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sídla	sídlo	k1gNnPc1	sídlo
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
významu	význam	k1gInSc2	význam
a	a	k8xC	a
historické	historický	k2eAgFnSc2d1	historická
tradice	tradice	k1gFnSc2	tradice
dělí	dělit	k5eAaImIp3nS	dělit
v	v	k7c4	v
města	město	k1gNnPc4	město
(	(	kIx(	(
<g/>
linn	linno	k1gNnPc2	linno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
městyse	městys	k1gInPc1	městys
(	(	kIx(	(
<g/>
alev	alev	k1gInSc1	alev
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
městečka	městečko	k1gNnSc2	městečko
(	(	kIx(	(
<g/>
alevik	alevik	k1gInSc1	alevik
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vesnice	vesnice	k1gFnSc1	vesnice
(	(	kIx(	(
<g/>
küla	küla	k1gFnSc1	küla
<g/>
)	)	kIx)	)
a	a	k8xC	a
usedlosti	usedlost	k1gFnSc2	usedlost
(	(	kIx(	(
<g/>
talu	talu	k6eAd1	talu
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
měst	město	k1gNnPc2	město
a	a	k8xC	a
městysů	městys	k1gInPc2	městys
tvoří	tvořit	k5eAaImIp3nP	tvořit
samostatnou	samostatný	k2eAgFnSc4d1	samostatná
samosprávnou	samosprávný	k2eAgFnSc4d1	samosprávná
jednotku	jednotka	k1gFnSc4	jednotka
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
městečka	městečko	k1gNnSc2	městečko
<g/>
,	,	kIx,	,
vesnice	vesnice	k1gFnSc2	vesnice
a	a	k8xC	a
usedlosti	usedlost	k1gFnSc2	usedlost
bývají	bývat	k5eAaImIp3nP	bývat
v	v	k7c4	v
samosprávné	samosprávný	k2eAgFnPc4d1	samosprávná
obce	obec	k1gFnPc4	obec
sdruženy	sdružen	k2eAgFnPc4d1	sdružena
s	s	k7c7	s
dalšími	další	k2eAgNnPc7d1	další
okolními	okolní	k2eAgNnPc7d1	okolní
sídly	sídlo	k1gNnPc7	sídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hospodářství	hospodářství	k1gNnSc1	hospodářství
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
získání	získání	k1gNnSc6	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
Estonsko	Estonsko	k1gNnSc4	Estonsko
významnou	významný	k2eAgFnSc7d1	významná
měrou	míra	k1gFnSc7wR	míra
soustředilo	soustředit	k5eAaPmAgNnS	soustředit
na	na	k7c4	na
obnovu	obnova	k1gFnSc4	obnova
hospodářství	hospodářství	k1gNnSc2	hospodářství
země	zem	k1gFnSc2	zem
a	a	k8xC	a
nastartování	nastartování	k1gNnSc1	nastartování
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
růstu	růst	k1gInSc2	růst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
uvedlo	uvést	k5eAaPmAgNnS	uvést
do	do	k7c2	do
oběhu	oběh	k1gInSc2	oběh
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
volně	volně	k6eAd1	volně
směnitelnou	směnitelný	k2eAgFnSc4d1	směnitelná
měnu	měna	k1gFnSc4	měna
–	–	k?	–
estonskou	estonský	k2eAgFnSc4d1	Estonská
korunu	koruna	k1gFnSc4	koruna
(	(	kIx(	(
<g/>
eesti	eesti	k1gNnPc1	eesti
kroon	kroona	k1gFnPc2	kroona
<g/>
,	,	kIx,	,
EEK	EEK	kA	EEK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nahradila	nahradit	k5eAaPmAgFnS	nahradit
sovětský	sovětský	k2eAgInSc4d1	sovětský
rubl	rubl	k1gInSc4	rubl
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
měna	měna	k1gFnSc1	měna
byla	být	k5eAaImAgFnS	být
pevně	pevně	k6eAd1	pevně
svázána	svázat	k5eAaPmNgFnS	svázat
s	s	k7c7	s
německou	německý	k2eAgFnSc7d1	německá
markou	marka	k1gFnSc7	marka
v	v	k7c6	v
kurzu	kurz	k1gInSc6	kurz
8	[number]	k4	8
EEK	EEK	kA	EEK
=	=	kIx~	=
1	[number]	k4	1
DEM	DEM	kA	DEM
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
Německo	Německo	k1gNnSc1	Německo
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
marku	marka	k1gFnSc4	marka
eurem	euro	k1gNnSc7	euro
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
kurz	kurz	k1gInSc1	kurz
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
15,646	[number]	k4	15,646
<g/>
6	[number]	k4	6
EEK	EEK	kA	EEK
za	za	k7c4	za
1	[number]	k4	1
euro	euro	k1gNnSc4	euro
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
privatizace	privatizace	k1gFnSc1	privatizace
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
telekomunikací	telekomunikace	k1gFnPc2	telekomunikace
<g/>
,	,	kIx,	,
železnic	železnice	k1gFnPc2	železnice
a	a	k8xC	a
energetiky	energetika	k1gFnSc2	energetika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
míře	míra	k1gFnSc6	míra
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
i	i	k9	i
nyní	nyní	k6eAd1	nyní
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
zavedlo	zavést	k5eAaPmAgNnS	zavést
Estonsko	Estonsko	k1gNnSc1	Estonsko
jako	jako	k9	jako
vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc1	první
země	země	k1gFnSc1	země
na	na	k7c6	na
světě	svět	k1gInSc6	svět
rovnou	rovný	k2eAgFnSc4d1	rovná
daň	daň	k1gFnSc4	daň
z	z	k7c2	z
příjmů	příjem	k1gInPc2	příjem
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
26	[number]	k4	26
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
však	však	k9	však
právnické	právnický	k2eAgFnPc1d1	právnická
osoby	osoba	k1gFnPc1	osoba
nemusí	muset	k5eNaImIp3nP	muset
tuto	tento	k3xDgFnSc4	tento
daň	daň	k1gFnSc4	daň
platit	platit	k5eAaImF	platit
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
pokud	pokud	k8xS	pokud
svůj	svůj	k3xOyFgInSc4	svůj
zisk	zisk	k1gInSc4	zisk
reinvestují	reinvestovat	k5eAaPmIp3nP	reinvestovat
<g/>
.	.	kIx.	.
</s>
<s>
Daň	daň	k1gFnSc1	daň
je	být	k5eAaImIp3nS	být
však	však	k9	však
stále	stále	k6eAd1	stále
platná	platný	k2eAgFnSc1d1	platná
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vyplácení	vyplácení	k1gNnSc4	vyplácení
dividend	dividenda	k1gFnPc2	dividenda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
se	se	k3xPyFc4	se
sazba	sazba	k1gFnSc1	sazba
daně	daň	k1gFnSc2	daň
snížila	snížit	k5eAaPmAgFnS	snížit
na	na	k7c4	na
24	[number]	k4	24
%	%	kIx~	%
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byla	být	k5eAaImAgFnS	být
sazba	sazba	k1gFnSc1	sazba
daně	daň	k1gFnSc2	daň
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
21	[number]	k4	21
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejhorším	zlý	k2eAgInSc7d3	Nejhorší
rokem	rok	k1gInSc7	rok
pro	pro	k7c4	pro
estonské	estonský	k2eAgNnSc4d1	Estonské
hospodářství	hospodářství	k1gNnSc4	hospodářství
od	od	k7c2	od
získání	získání	k1gNnSc2	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
byl	být	k5eAaImAgInS	být
rok	rok	k1gInSc4	rok
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
ruské	ruský	k2eAgFnSc2d1	ruská
krize	krize	k1gFnSc2	krize
z	z	k7c2	z
předchozího	předchozí	k2eAgInSc2d1	předchozí
roku	rok	k1gInSc2	rok
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
o	o	k7c4	o
několik	několik	k4yIc4	několik
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
HDP	HDP	kA	HDP
neustále	neustále	k6eAd1	neustále
roste	růst	k5eAaImIp3nS	růst
tempem	tempo	k1gNnSc7	tempo
okolo	okolo	k7c2	okolo
7	[number]	k4	7
%	%	kIx~	%
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
však	však	k9	však
estonské	estonský	k2eAgNnSc1d1	Estonské
hospodářství	hospodářství	k1gNnSc1	hospodářství
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
tempa	tempo	k1gNnSc2	tempo
růstu	růst	k1gInSc2	růst
11,4	[number]	k4	11,4
%	%	kIx~	%
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
doposud	doposud	k6eAd1	doposud
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
obnovení	obnovení	k1gNnSc2	obnovení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
přepočtený	přepočtený	k2eAgInSc1d1	přepočtený
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
obyvatele	obyvatel	k1gMnSc4	obyvatel
je	být	k5eAaImIp3nS	být
18,500	[number]	k4	18,500
USD	USD	kA	USD
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nejvíce	hodně	k6eAd3	hodně
z	z	k7c2	z
pobaltských	pobaltský	k2eAgFnPc2d1	pobaltská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Estonské	estonský	k2eAgNnSc1d1	Estonské
hospodářství	hospodářství	k1gNnSc1	hospodářství
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
službách	služba	k1gFnPc6	služba
<g/>
,	,	kIx,	,
významný	významný	k2eAgInSc1d1	významný
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
sektor	sektor	k1gInSc1	sektor
informačních	informační	k2eAgFnPc2d1	informační
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
telekomunikace	telekomunikace	k1gFnSc2	telekomunikace
a	a	k8xC	a
bankovnictví	bankovnictví	k1gNnSc2	bankovnictví
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
oborů	obor	k1gInPc2	obor
jsou	být	k5eAaImIp3nP	být
nejdůležitější	důležitý	k2eAgMnSc1d3	nejdůležitější
elektrotechnický	elektrotechnický	k2eAgMnSc1d1	elektrotechnický
<g/>
,	,	kIx,	,
dřevozpracující	dřevozpracující	k2eAgInSc1d1	dřevozpracující
a	a	k8xC	a
chemický	chemický	k2eAgInSc1d1	chemický
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
těžba	těžba	k1gFnSc1	těžba
ropných	ropný	k2eAgFnPc2d1	ropná
břidlic	břidlice	k1gFnPc2	břidlice
a	a	k8xC	a
stavba	stavba	k1gFnSc1	stavba
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
hraje	hrát	k5eAaImIp3nS	hrát
též	též	k9	též
rybolov	rybolov	k1gInSc1	rybolov
<g/>
.	.	kIx.	.
</s>
<s>
Rychlý	rychlý	k2eAgInSc1d1	rychlý
růst	růst	k1gInSc1	růst
estonského	estonský	k2eAgNnSc2d1	Estonské
hospodářství	hospodářství	k1gNnSc2	hospodářství
je	být	k5eAaImIp3nS	být
podporován	podporovat	k5eAaImNgInS	podporovat
přísunem	přísun	k1gInSc7	přísun
kapitálu	kapitál	k1gInSc2	kapitál
ze	z	k7c2	z
skandinávských	skandinávský	k2eAgFnPc2d1	skandinávská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
estonské	estonský	k2eAgInPc4d1	estonský
přístavy	přístav	k1gInPc4	přístav
prochází	procházet	k5eAaImIp3nS	procházet
i	i	k9	i
významná	významný	k2eAgFnSc1d1	významná
část	část	k1gFnSc1	část
ruského	ruský	k2eAgInSc2d1	ruský
zahraničního	zahraniční	k2eAgInSc2d1	zahraniční
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k8xC	i
ruská	ruský	k2eAgFnSc1d1	ruská
ropa	ropa	k1gFnSc1	ropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Estonsko	Estonsko	k1gNnSc1	Estonsko
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
členem	člen	k1gMnSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
členem	člen	k1gMnSc7	člen
Světové	světový	k2eAgFnSc2d1	světová
obchodní	obchodní	k2eAgFnSc2d1	obchodní
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
WTO	WTO	kA	WTO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
vyhotovena	vyhotoven	k2eAgFnSc1d1	vyhotovena
estonská	estonský	k2eAgFnSc1d1	Estonská
podoba	podoba	k1gFnSc1	podoba
estonských	estonský	k2eAgInPc2d1	estonský
euromincí	eurominký	k2eAgMnPc1d1	eurominký
<g/>
.	.	kIx.	.
</s>
<s>
Zavedení	zavedení	k1gNnSc1	zavedení
eura	euro	k1gNnSc2	euro
se	se	k3xPyFc4	se
původně	původně	k6eAd1	původně
plánovalo	plánovat	k5eAaImAgNnS	plánovat
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
leden	leden	k1gInSc4	leden
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
těžký	těžký	k2eAgInSc4d1	těžký
dopad	dopad	k1gInSc4	dopad
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
estonská	estonský	k2eAgFnSc1d1	Estonská
vláda	vláda	k1gFnSc1	vláda
masivními	masivní	k2eAgInPc7d1	masivní
škrty	škrt	k1gInPc7	škrt
splnění	splnění	k1gNnSc2	splnění
všech	všecek	k3xTgNnPc2	všecek
Maastrichtských	maastrichtský	k2eAgNnPc2d1	maastrichtské
kritérií	kritérion	k1gNnPc2	kritérion
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
Estonsko	Estonsko	k1gNnSc1	Estonsko
přijalo	přijmout	k5eAaPmAgNnS	přijmout
euro	euro	k1gNnSc4	euro
k	k	k7c3	k
počátku	počátek	k1gInSc3	počátek
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
bylo	být	k5eAaImAgNnS	být
ohlášeno	ohlášen	k2eAgNnSc1d1	ohlášeno
možné	možný	k2eAgNnSc1d1	možné
zavedení	zavedení	k1gNnSc1	zavedení
nové	nový	k2eAgInPc1d1	nový
kryptoměny	kryptoměn	k2eAgInPc1d1	kryptoměn
vydávané	vydávaný	k2eAgMnPc4d1	vydávaný
estonským	estonský	k2eAgInSc7d1	estonský
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Etničtí	etnický	k2eAgMnPc1d1	etnický
Estonci	Estonec	k1gMnPc1	Estonec
tvoří	tvořit	k5eAaImIp3nP	tvořit
necelých	celý	k2eNgInPc2d1	necelý
70	[number]	k4	70
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
25	[number]	k4	25
<g/>
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nP	tvořit
etničtí	etnický	k2eAgMnPc1d1	etnický
Rusové	Rus	k1gMnPc1	Rus
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
a	a	k8xC	a
druhá	druhý	k4xOgFnSc1	druhý
generace	generace	k1gFnSc1	generace
imigrantů	imigrant	k1gMnPc2	imigrant
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
částí	část	k1gFnPc2	část
bývalého	bývalý	k2eAgInSc2d1	bývalý
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
představuje	představovat	k5eAaImIp3nS	představovat
většinu	většina	k1gFnSc4	většina
ze	z	k7c2	z
neestonských	estonský	k2eNgMnPc2d1	estonský
obyvatel	obyvatel	k1gMnPc2	obyvatel
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Rusky	rusky	k6eAd1	rusky
mluvící	mluvící	k2eAgFnSc1d1	mluvící
menšina	menšina	k1gFnSc1	menšina
žije	žít	k5eAaImIp3nS	žít
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Tallinnu	Tallinn	k1gInSc6	Tallinn
a	a	k8xC	a
v	v	k7c6	v
průmyslových	průmyslový	k2eAgFnPc6d1	průmyslová
oblastech	oblast	k1gFnPc6	oblast
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Estonska	Estonsko	k1gNnSc2	Estonsko
(	(	kIx(	(
<g/>
Ida-Virumaa	Ida-Virumaum	k1gNnSc2	Ida-Virumaum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zmínku	zmínka	k1gFnSc4	zmínka
stojí	stát	k5eAaImIp3nS	stát
i	i	k9	i
nepočetná	početný	k2eNgFnSc1d1	nepočetná
finská	finský	k2eAgFnSc1d1	finská
menšina	menšina	k1gFnSc1	menšina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
obyvatel	obyvatel	k1gMnPc2	obyvatel
ruského	ruský	k2eAgInSc2d1	ruský
původu	původ	k1gInSc2	původ
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Estonsku	Estonsko	k1gNnSc6	Estonsko
v	v	k7c6	v
postavení	postavení	k1gNnSc6	postavení
občanů	občan	k1gMnPc2	občan
druhé	druhý	k4xOgFnSc2	druhý
kategorie	kategorie	k1gFnSc2	kategorie
bez	bez	k7c2	bez
základních	základní	k2eAgNnPc2d1	základní
práv	právo	k1gNnPc2	právo
tzv.	tzv.	kA	tzv.
neobčanů	neobčan	k1gMnPc2	neobčan
<g/>
.	.	kIx.	.
</s>
<s>
Nemohou	moct	k5eNaImIp3nP	moct
například	například	k6eAd1	například
získat	získat	k5eAaPmF	získat
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
správě	správa	k1gFnSc6	správa
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
status	status	k1gInSc1	status
nemá	mít	k5eNaImIp3nS	mít
oporu	opora	k1gFnSc4	opora
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
právu	právo	k1gNnSc6	právo
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
i	i	k9	i
parlamentním	parlamentní	k2eAgNnSc7d1	parlamentní
shromážděním	shromáždění	k1gNnSc7	shromáždění
Rady	rada	k1gFnSc2	rada
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
<g/>
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
estonština	estonština	k1gFnSc1	estonština
<g/>
,	,	kIx,	,
uralský	uralský	k2eAgInSc1d1	uralský
jazyk	jazyk	k1gInSc1	jazyk
blízký	blízký	k2eAgInSc1d1	blízký
finštině	finština	k1gFnSc3	finština
<g/>
.	.	kIx.	.
</s>
<s>
Ruština	ruština	k1gFnSc1	ruština
nemá	mít	k5eNaImIp3nS	mít
status	status	k1gInSc4	status
úředního	úřední	k2eAgInSc2d1	úřední
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
lidí	člověk	k1gMnPc2	člověk
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
a	a	k8xC	a
vyšším	vysoký	k2eAgInSc6d2	vyšší
věku	věk	k1gInSc6	věk
jí	on	k3xPp3gFnSc3	on
alespoň	alespoň	k9	alespoň
rozumí	rozumět	k5eAaImIp3nS	rozumět
<g/>
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
etničtí	etnický	k2eAgMnPc1d1	etnický
Rusové	Rus	k1gMnPc1	Rus
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
nekteří	nekteří	k1gNnPc2	nekteří
obývají	obývat	k5eAaImIp3nP	obývat
Tallinn	Tallinn	k1gInSc4	Tallinn
(	(	kIx(	(
<g/>
Revel	Revel	k1gInSc4	Revel
<g/>
)	)	kIx)	)
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
carského	carský	k2eAgNnSc2d1	carské
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byla	být	k5eAaImAgFnS	být
povinným	povinný	k2eAgInSc7d1	povinný
jazykem	jazyk	k1gInSc7	jazyk
ve	v	k7c6	v
školách	škola	k1gFnPc6	škola
v	v	k7c6	v
době	doba	k1gFnSc6	doba
okupace	okupace	k1gFnSc2	okupace
Sovětským	sovětský	k2eAgInSc7d1	sovětský
svazem	svaz	k1gInSc7	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Mladší	mladý	k2eAgMnPc1d2	mladší
lidé	člověk	k1gMnPc1	člověk
většinou	většinou	k6eAd1	většinou
mluví	mluvit	k5eAaImIp3nP	mluvit
dobře	dobře	k6eAd1	dobře
anglicky	anglicky	k6eAd1	anglicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Estonská	estonský	k2eAgFnSc1d1	Estonská
populace	populace	k1gFnSc1	populace
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
počtu	počet	k1gInSc2	počet
1	[number]	k4	1
569	[number]	k4	569
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
klesá	klesat	k5eAaImIp3nS	klesat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
začátku	začátek	k1gInSc3	začátek
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
ubylo	ubýt	k5eAaPmAgNnS	ubýt
okolo	okolo	k7c2	okolo
15	[number]	k4	15
%	%	kIx~	%
estonských	estonský	k2eAgMnPc2d1	estonský
obyvatel	obyvatel	k1gMnPc2	obyvatel
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
hlavně	hlavně	k9	hlavně
poklesem	pokles	k1gInSc7	pokles
porodnosti	porodnost	k1gFnSc2	porodnost
a	a	k8xC	a
emigrací	emigrace	k1gFnPc2	emigrace
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
porodnost	porodnost	k1gFnSc1	porodnost
relativně	relativně	k6eAd1	relativně
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
Problémem	problém	k1gInSc7	problém
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
i	i	k9	i
vysoká	vysoký	k2eAgFnSc1d1	vysoká
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
(	(	kIx(	(
<g/>
22	[number]	k4	22
212	[number]	k4	212
lidí	člověk	k1gMnPc2	člověk
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
15	[number]	k4	15
244	[number]	k4	244
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
se	se	k3xPyFc4	se
ale	ale	k9	ale
daří	dařit	k5eAaImIp3nS	dařit
postupně	postupně	k6eAd1	postupně
snižovat	snižovat	k5eAaImF	snižovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Estonci	Estonec	k1gMnPc1	Estonec
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
většiny	většina	k1gFnSc2	většina
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnSc2	vyznání
a	a	k8xC	a
vliv	vliv	k1gInSc1	vliv
církví	církev	k1gFnPc2	církev
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
malý	malý	k2eAgInSc1d1	malý
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
projektu	projekt	k1gInSc2	projekt
Eurobarometr	Eurobarometr	k1gInSc4	Eurobarometr
v	v	k7c6	v
průzkumech	průzkum	k1gInPc6	průzkum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
pouze	pouze	k6eAd1	pouze
16	[number]	k4	16
%	%	kIx~	%
Estonců	Estonec	k1gMnPc2	Estonec
prohlásilo	prohlásit	k5eAaPmAgNnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Bůh	bůh	k1gMnSc1	bůh
existuje	existovat	k5eAaImIp3nS	existovat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
54	[number]	k4	54
%	%	kIx~	%
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
existuje	existovat	k5eAaImIp3nS	existovat
duch	duch	k1gMnSc1	duch
nebo	nebo	k8xC	nebo
životní	životní	k2eAgFnSc1d1	životní
síla	síla	k1gFnSc1	síla
<g/>
"	"	kIx"	"
a	a	k8xC	a
26	[number]	k4	26
%	%	kIx~	%
nevěří	věřit	k5eNaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
existuje	existovat	k5eAaImIp3nS	existovat
nějaký	nějaký	k3yIgMnSc1	nějaký
duch	duch	k1gMnSc1	duch
<g/>
,	,	kIx,	,
Bůh	bůh	k1gMnSc1	bůh
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
životní	životní	k2eAgFnSc1d1	životní
síla	síla	k1gFnSc1	síla
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Převládajícím	převládající	k2eAgNnSc7d1	převládající
náboženstvím	náboženství	k1gNnSc7	náboženství
etnických	etnický	k2eAgMnPc2d1	etnický
Estonců	Estonec	k1gMnPc2	Estonec
je	být	k5eAaImIp3nS	být
křesťanství	křesťanství	k1gNnSc1	křesťanství
<g/>
,	,	kIx,	,
představované	představovaný	k2eAgNnSc1d1	představované
luteránskou	luteránský	k2eAgFnSc7d1	luteránská
evangelickou	evangelický	k2eAgFnSc7d1	evangelická
církví	církev	k1gFnSc7	církev
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
věřících	věřící	k1gFnPc2	věřící
patřících	patřící	k2eAgFnPc2d1	patřící
k	k	k7c3	k
ruské	ruský	k2eAgFnSc3d1	ruská
menšině	menšina	k1gFnSc3	menšina
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
ruské	ruský	k2eAgFnSc3d1	ruská
pravoslavné	pravoslavný	k2eAgFnSc3d1	pravoslavná
církvi	církev	k1gFnSc3	církev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
k	k	k7c3	k
různým	různý	k2eAgFnPc3d1	různá
církvím	církev	k1gFnPc3	církev
hlásí	hlásit	k5eAaImIp3nP	hlásit
méně	málo	k6eAd2	málo
než	než	k8xS	než
třetina	třetina	k1gFnSc1	třetina
dospělého	dospělý	k2eAgNnSc2d1	dospělé
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Přítomné	přítomný	k2eAgInPc1d1	přítomný
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
další	další	k2eAgFnPc1d1	další
menší	malý	k2eAgFnPc1d2	menší
protestantské	protestantský	k2eAgFnPc1d1	protestantská
církve	církev	k1gFnPc1	církev
<g/>
,	,	kIx,	,
Svědkové	svědek	k1gMnPc1	svědek
Jehovovi	Jehovův	k2eAgMnPc1d1	Jehovův
<g/>
,	,	kIx,	,
židé	žid	k1gMnPc1	žid
a	a	k8xC	a
novopohané	novopohan	k1gMnPc1	novopohan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Významná	významný	k2eAgNnPc1d1	významné
města	město	k1gNnPc1	město
===	===	k?	===
</s>
</p>
<p>
<s>
Haapsalu	Haapsala	k1gFnSc4	Haapsala
</s>
</p>
<p>
<s>
Jõ	Jõ	k?	Jõ
</s>
</p>
<p>
<s>
Kärdla	Kärdnout	k5eAaPmAgFnS	Kärdnout
</s>
</p>
<p>
<s>
Kohtla-Järve	Kohtla-Järvat	k5eAaPmIp3nS	Kohtla-Järvat
</s>
</p>
<p>
<s>
Kunda	Kunda	k1gFnSc1	Kunda
</s>
</p>
<p>
<s>
Kuressaare	Kuressaar	k1gMnSc5	Kuressaar
</s>
</p>
<p>
<s>
Narva	Narva	k6eAd1	Narva
</s>
</p>
<p>
<s>
Paldiski	Paldiski	k6eAd1	Paldiski
</s>
</p>
<p>
<s>
Pärnu	Pärnout	k5eAaImIp1nS	Pärnout
</s>
</p>
<p>
<s>
Rapla	Rapla	k?	Rapla
</s>
</p>
<p>
<s>
Tartu	Tarta	k1gFnSc4	Tarta
</s>
</p>
<p>
<s>
Tallinn	Tallinn	k1gMnSc1	Tallinn
</s>
</p>
<p>
<s>
Valga	Valga	k1gFnSc1	Valga
</s>
</p>
<p>
<s>
Võ	Võ	k?	Võ
</s>
</p>
<p>
<s>
Viljandi	Viljand	k1gMnPc1	Viljand
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
estonské	estonský	k2eAgFnSc2d1	Estonská
literatury	literatura	k1gFnSc2	literatura
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
Friedrich	Friedrich	k1gMnSc1	Friedrich
Reinhold	Reinhold	k1gMnSc1	Reinhold
Kreutzwald	Kreutzwald	k1gMnSc1	Kreutzwald
<g/>
,	,	kIx,	,
za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
moderní	moderní	k2eAgFnSc2d1	moderní
estonské	estonský	k2eAgFnSc2d1	Estonská
poezie	poezie	k1gFnSc2	poezie
jsou	být	k5eAaImIp3nP	být
označováni	označován	k2eAgMnPc1d1	označován
Marie	Maria	k1gFnPc4	Maria
Underová	Underová	k1gFnSc1	Underová
a	a	k8xC	a
Kristjan	Kristjan	k1gMnSc1	Kristjan
Jaak	Jaak	k1gMnSc1	Jaak
Peterson	Peterson	k1gMnSc1	Peterson
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
představitelkou	představitelka	k1gFnSc7	představitelka
estonského	estonský	k2eAgNnSc2d1	Estonské
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
byla	být	k5eAaImAgFnS	být
básnířka	básnířka	k1gFnSc1	básnířka
Lydia	Lydia	k1gFnSc1	Lydia
Koidula	Koidula	k1gFnSc1	Koidula
či	či	k8xC	či
spisovatel	spisovatel	k1gMnSc1	spisovatel
Carl	Carl	k1gMnSc1	Carl
Robert	Robert	k1gMnSc1	Robert
Jakobson	Jakobson	k1gMnSc1	Jakobson
<g/>
.	.	kIx.	.
</s>
<s>
Johann	Johann	k1gMnSc1	Johann
Voldemar	Voldemar	k1gMnSc1	Voldemar
Jannsen	Jannsen	k1gInSc4	Jannsen
napsal	napsat	k5eAaBmAgMnS	napsat
báseň	báseň	k1gFnSc4	báseň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
později	pozdě	k6eAd2	pozdě
textem	text	k1gInSc7	text
estonské	estonský	k2eAgFnSc2d1	Estonská
národní	národní	k2eAgFnSc2d1	národní
hymny	hymna	k1gFnSc2	hymna
<g/>
.	.	kIx.	.
</s>
<s>
Pentalogie	pentalogie	k1gFnSc1	pentalogie
Pravda	pravda	k1gFnSc1	pravda
a	a	k8xC	a
spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnSc7	jejíž
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Anton	Anton	k1gMnSc1	Anton
Hansen	Hansen	k2eAgMnSc1d1	Hansen
Tammsaare	Tammsaar	k1gMnSc5	Tammsaar
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pilířem	pilíř	k1gInSc7	pilíř
estonské	estonský	k2eAgFnSc2d1	Estonská
prózy	próza	k1gFnSc2	próza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
Estonsku	Estonsko	k1gNnSc6	Estonsko
vynikli	vyniknout	k5eAaPmAgMnP	vyniknout
básnířka	básnířek	k1gMnSc4	básnířek
Betti	Betti	k1gFnSc2	Betti
Alverová	Alverová	k1gFnSc1	Alverová
či	či	k8xC	či
básník	básník	k1gMnSc1	básník
Friedebert	Friedebert	k1gMnSc1	Friedebert
Tuglas	Tuglas	k1gMnSc1	Tuglas
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
do	do	k7c2	do
estonské	estonský	k2eAgFnSc2d1	Estonská
literatury	literatura	k1gFnSc2	literatura
vnesl	vnést	k5eAaPmAgInS	vnést
prvky	prvek	k1gInPc4	prvek
impresionismu	impresionismus	k1gInSc2	impresionismus
a	a	k8xC	a
symbolismu	symbolismus	k1gInSc2	symbolismus
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
prosadil	prosadit	k5eAaPmAgInS	prosadit
Jaan	Jaan	k1gInSc1	Jaan
Kross	Kross	k1gInSc1	Kross
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
oceňovaným	oceňovaný	k2eAgMnPc3d1	oceňovaný
současným	současný	k2eAgMnPc3d1	současný
autorům	autor	k1gMnPc3	autor
patří	patřit	k5eAaImIp3nS	patřit
Tõ	Tõ	k1gFnSc1	Tõ
Õ	Õ	k?	Õ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejproslulejšími	proslulý	k2eAgMnPc7d3	nejproslulejší
hudebními	hudební	k2eAgMnPc7d1	hudební
skladateli	skladatel	k1gMnPc7	skladatel
jsou	být	k5eAaImIp3nP	být
Arvo	Arvo	k1gMnSc1	Arvo
Pärt	Pärt	k1gMnSc1	Pärt
<g/>
,	,	kIx,	,
Evald	Evald	k1gMnSc1	Evald
Aav	Aav	k1gMnSc1	Aav
a	a	k8xC	a
Eduard	Eduard	k1gMnSc1	Eduard
Tubin	Tubin	k1gMnSc1	Tubin
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
interpretů	interpret	k1gMnPc2	interpret
lze	lze	k6eAd1	lze
jmenovat	jmenovat	k5eAaImF	jmenovat
dirigenta	dirigent	k1gMnSc4	dirigent
Neeme	Neem	k1gMnSc5	Neem
Järviho	Järviha	k1gMnSc5	Järviha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Estonsku	Estonsko	k1gNnSc6	Estonsko
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
významný	významný	k2eAgMnSc1d1	významný
americký	americký	k2eAgMnSc1d1	americký
architekt	architekt	k1gMnSc1	architekt
Louis	Louis	k1gMnSc1	Louis
Kahn	Kahn	k1gMnSc1	Kahn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
modelingu	modeling	k1gInSc6	modeling
se	se	k3xPyFc4	se
prosadila	prosadit	k5eAaPmAgFnS	prosadit
Carmen	Carmen	k1gInSc4	Carmen
Kassová	Kassový	k2eAgFnSc1d1	Kassový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Věda	věda	k1gFnSc1	věda
a	a	k8xC	a
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
významným	významný	k2eAgMnPc3d1	významný
vědcům	vědec	k1gMnPc3	vědec
patří	patřit	k5eAaImIp3nP	patřit
astrofyzik	astrofyzik	k1gMnSc1	astrofyzik
Ernst	Ernst	k1gMnSc1	Ernst
Öpik	Öpik	k1gMnSc1	Öpik
<g/>
,	,	kIx,	,
fyzikové	fyzik	k1gMnPc1	fyzik
Thomas	Thomas	k1gMnSc1	Thomas
Johann	Johann	k1gMnSc1	Johann
Seebeck	Seebeck	k1gMnSc1	Seebeck
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
objevil	objevit	k5eAaPmAgMnS	objevit
termoelektrický	termoelektrický	k2eAgInSc4d1	termoelektrický
jev	jev	k1gInSc4	jev
<g/>
,	,	kIx,	,
a	a	k8xC	a
Heinrich	Heinrich	k1gMnSc1	Heinrich
Lenz	Lenz	k1gMnSc1	Lenz
či	či	k8xC	či
optik	optik	k1gMnSc1	optik
Bernhard	Bernhard	k1gMnSc1	Bernhard
Schmidt	Schmidt	k1gMnSc1	Schmidt
<g/>
.	.	kIx.	.
</s>
<s>
Estonský	estonský	k2eAgInSc4d1	estonský
původ	původ	k1gInSc4	původ
měli	mít	k5eAaImAgMnP	mít
i	i	k9	i
zoologové	zoolog	k1gMnPc1	zoolog
Karl	Karl	k1gMnSc1	Karl
Ernst	Ernst	k1gMnSc1	Ernst
von	von	k1gInSc4	von
Baer	Baera	k1gFnPc2	Baera
a	a	k8xC	a
Alexander	Alexandra	k1gFnPc2	Alexandra
von	von	k1gInSc1	von
Middendorff	Middendorff	k1gInSc1	Middendorff
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
světovým	světový	k2eAgMnPc3d1	světový
sémiotikům	sémiotik	k1gMnPc3	sémiotik
patří	patřit	k5eAaImIp3nP	patřit
Jurij	Jurij	k1gMnSc1	Jurij
Michajlovič	Michajlovič	k1gMnSc1	Michajlovič
Lotman	Lotman	k1gMnSc1	Lotman
<g/>
.	.	kIx.	.
</s>
<s>
Lingvista	lingvista	k1gMnSc1	lingvista
Paul	Paul	k1gMnSc1	Paul
Ariste	Arist	k1gMnSc5	Arist
zasvětil	zasvětit	k5eAaPmAgInS	zasvětit
život	život	k1gInSc1	život
studiu	studio	k1gNnSc6	studio
umírající	umírající	k1gFnSc2	umírající
votštiny	votština	k1gFnSc2	votština
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
estonského	estonský	k2eAgInSc2d1	estonský
jazyka	jazyk	k1gInSc2	jazyk
sehrál	sehrát	k5eAaPmAgMnS	sehrát
zásadní	zásadní	k2eAgFnSc4d1	zásadní
roli	role	k1gFnSc4	role
filolog	filolog	k1gMnSc1	filolog
Johannes	Johannes	k1gMnSc1	Johannes
Aavik	Aavik	k1gMnSc1	Aavik
<g/>
.	.	kIx.	.
</s>
<s>
Tvůrcem	tvůrce	k1gMnSc7	tvůrce
umělého	umělý	k2eAgInSc2d1	umělý
jazyka	jazyk	k1gInSc2	jazyk
Occidental	Occidental	k1gMnSc1	Occidental
byl	být	k5eAaImAgMnS	být
baltský	baltský	k2eAgMnSc1d1	baltský
Němec	Němec	k1gMnSc1	Němec
Edgar	Edgar	k1gMnSc1	Edgar
de	de	k?	de
Wahl	Wahl	k1gMnSc1	Wahl
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
věhlas	věhlas	k1gInSc1	věhlas
získal	získat	k5eAaPmAgInS	získat
i	i	k9	i
filozof	filozof	k1gMnSc1	filozof
a	a	k8xC	a
kulturní	kulturní	k2eAgMnSc1d1	kulturní
teoretik	teoretik	k1gMnSc1	teoretik
Jaan	Jaan	k1gMnSc1	Jaan
Kaplinski	Kaplinski	k1gNnPc2	Kaplinski
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Tallinu	Tallin	k1gInSc6	Tallin
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
zakladatel	zakladatel	k1gMnSc1	zakladatel
gestalt	gestalt	k1gMnSc1	gestalt
psychologie	psychologie	k1gFnSc2	psychologie
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Köhler	Köhler	k1gMnSc1	Köhler
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základní	základní	k2eAgFnPc1d1	základní
školy	škola	k1gFnPc1	škola
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Estonsku	Estonsko	k1gNnSc6	Estonsko
devítileté	devítiletý	k2eAgFnSc2d1	devítiletá
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
povinné	povinný	k2eAgFnPc1d1	povinná
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
absolvují	absolvovat	k5eAaPmIp3nP	absolvovat
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
obdrží	obdržet	k5eAaPmIp3nS	obdržet
certifikát	certifikát	k1gInSc1	certifikát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jim	on	k3xPp3gMnPc3	on
dává	dávat	k5eAaImIp3nS	dávat
právo	právo	k1gNnSc4	právo
pokračovat	pokračovat	k5eAaImF	pokračovat
v	v	k7c6	v
dalším	další	k2eAgNnSc6d1	další
vzdělávání	vzdělávání	k1gNnSc6	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
2	[number]	k4	2
varianty	varianta	k1gFnPc4	varianta
dalšího	další	k2eAgNnSc2d1	další
studia	studio	k1gNnSc2	studio
(	(	kIx(	(
<g/>
obě	dva	k4xCgFnPc4	dva
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
letech	let	k1gInPc6	let
studia	studio	k1gNnSc2	studio
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
První	první	k4xOgFnSc7	první
variantou	varianta	k1gFnSc7	varianta
jsou	být	k5eAaImIp3nP	být
sekundární	sekundární	k2eAgFnPc1d1	sekundární
obecně	obecně	k6eAd1	obecně
vzdělávací	vzdělávací	k2eAgFnPc1d1	vzdělávací
školy	škola	k1gFnPc1	škola
(	(	kIx(	(
<g/>
gymnázia	gymnázium	k1gNnPc1	gymnázium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc7	druhý
pak	pak	k6eAd1	pak
odborně	odborně	k6eAd1	odborně
vzdělávací	vzdělávací	k2eAgFnPc4d1	vzdělávací
školy	škola	k1gFnPc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
vystudování	vystudování	k1gNnSc6	vystudování
gymnázia	gymnázium	k1gNnSc2	gymnázium
obdrží	obdržet	k5eAaPmIp3nP	obdržet
studenti	student	k1gMnPc1	student
certifikát	certifikát	k1gInSc4	certifikát
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jim	on	k3xPp3gMnPc3	on
dává	dávat	k5eAaImIp3nS	dávat
možnost	možnost	k1gFnSc4	možnost
studovat	studovat	k5eAaImF	studovat
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgNnSc1d2	vyšší
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
je	být	k5eAaImIp3nS	být
zajišťováno	zajišťovat	k5eAaImNgNnS	zajišťovat
univerzitami	univerzita	k1gFnPc7	univerzita
a	a	k8xC	a
také	také	k9	také
institucemi	instituce	k1gFnPc7	instituce
vyššího	vysoký	k2eAgNnSc2d2	vyšší
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
je	být	k5eAaImIp3nS	být
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
etapou	etapa	k1gFnSc7	etapa
jsou	být	k5eAaImIp3nP	být
bakalářská	bakalářský	k2eAgNnPc1d1	bakalářské
studia	studio	k1gNnPc1	studio
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
tříletá	tříletý	k2eAgFnSc1d1	tříletá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
druhou	druhý	k4xOgFnSc4	druhý
magisterská	magisterský	k2eAgNnPc1d1	magisterské
studia	studio	k1gNnPc1	studio
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
dvouletá	dvouletý	k2eAgFnSc1d1	dvouletá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
třetí	třetí	k4xOgFnSc1	třetí
pak	pak	k6eAd1	pak
doktorská	doktorský	k2eAgNnPc1d1	doktorské
studia	studio	k1gNnPc1	studio
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
čtyřletá	čtyřletý	k2eAgFnSc1d1	čtyřletá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnější	slavný	k2eAgFnSc7d3	nejslavnější
estonskou	estonský	k2eAgFnSc7d1	Estonská
institucí	instituce	k1gFnSc7	instituce
terciárního	terciární	k2eAgNnSc2d1	terciární
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
je	být	k5eAaImIp3nS	být
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Tartu	Tarto	k1gNnSc6	Tarto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
Samostatné	samostatný	k2eAgNnSc1d1	samostatné
Estonsko	Estonsko	k1gNnSc1	Estonsko
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
získalo	získat	k5eAaPmAgNnS	získat
již	již	k6eAd1	již
sedm	sedm	k4xCc1	sedm
zlatých	zlatý	k2eAgFnPc2d1	zlatá
olympijských	olympijský	k2eAgFnPc2d1	olympijská
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
první	první	k4xOgFnSc7	první
získala	získat	k5eAaPmAgFnS	získat
dráhová	dráhový	k2eAgFnSc1d1	dráhová
cyklistka	cyklistka	k1gFnSc1	cyklistka
Erika	Erik	k1gMnSc2	Erik
Salumäeová	Salumäeová	k1gFnSc1	Salumäeová
na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
zlato	zlato	k1gNnSc1	zlato
v	v	k7c6	v
Sydney	Sydney	k1gNnSc6	Sydney
2000	[number]	k4	2000
přidal	přidat	k5eAaPmAgMnS	přidat
desetibojař	desetibojař	k1gMnSc1	desetibojař
Erki	Erk	k1gFnSc2	Erk
Nool	Nool	k1gMnSc1	Nool
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
2008	[number]	k4	2008
ho	on	k3xPp3gMnSc4	on
napodobil	napodobit	k5eAaPmAgInS	napodobit
další	další	k2eAgMnSc1d1	další
atlet	atlet	k1gMnSc1	atlet
<g/>
,	,	kIx,	,
diskař	diskař	k1gMnSc1	diskař
Gerd	Gerd	k1gMnSc1	Gerd
Kanter	Kanter	k1gMnSc1	Kanter
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
lepší	dobrý	k2eAgFnSc4d2	lepší
bilanci	bilance	k1gFnSc4	bilance
mají	mít	k5eAaImIp3nP	mít
Estonci	Estonec	k1gMnPc1	Estonec
na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
zimní	zimní	k2eAgFnSc6d1	zimní
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
dvěma	dva	k4xCgMnPc3	dva
běžcům	běžec	k1gMnPc3	běžec
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
a	a	k8xC	a
dvojnásobným	dvojnásobný	k2eAgMnSc7d1	dvojnásobný
olympijským	olympijský	k2eAgMnSc7d1	olympijský
vítězům	vítěz	k1gMnPc3	vítěz
-	-	kIx~	-
Andrusu	Andrus	k1gMnSc3	Andrus
Veerpaluovi	Veerpalu	k1gMnSc3	Veerpalu
a	a	k8xC	a
Kristině	Kristina	k1gFnSc3	Kristina
Šmigunové	Šmigunový	k2eAgFnPc1d1	Šmigunová
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
i	i	k9	i
meziválečné	meziválečný	k2eAgNnSc1d1	meziválečné
samostatné	samostatný	k2eAgNnSc1d1	samostatné
Estonsko	Estonsko	k1gNnSc1	Estonsko
sbíralo	sbírat	k5eAaImAgNnS	sbírat
olympijské	olympijský	k2eAgInPc4d1	olympijský
vavříny	vavřín	k1gInPc4	vavřín
-	-	kIx~	-
zápasník	zápasník	k1gMnSc1	zápasník
Kristjan	Kristjan	k1gMnSc1	Kristjan
Palusalu	Palusala	k1gFnSc4	Palusala
přivezl	přivézt	k5eAaPmAgMnS	přivézt
dvě	dva	k4xCgFnPc4	dva
zlaté	zlatá	k1gFnPc4	zlatá
z	z	k7c2	z
Berlína	Berlín	k1gInSc2	Berlín
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
(	(	kIx(	(
<g/>
z	z	k7c2	z
volného	volný	k2eAgInSc2d1	volný
stylu	styl	k1gInSc2	styl
i	i	k8xC	i
zápasu	zápas	k1gInSc2	zápas
řecko-římského	řecko-římský	k2eAgInSc2d1	řecko-římský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
další	další	k2eAgFnSc6d1	další
zlaté	zlatá	k1gFnSc6	zlatá
brali	brát	k5eAaImAgMnP	brát
mezi	mezi	k7c7	mezi
válkami	válka	k1gFnPc7	válka
zápasníci	zápasník	k1gMnPc1	zápasník
<g/>
:	:	kIx,	:
Eduard	Eduard	k1gMnSc1	Eduard
Pütsep	Pütsep	k1gMnSc1	Pütsep
(	(	kIx(	(
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Osvald	Osvald	k1gMnSc1	Osvald
Käpp	Käpp	k1gMnSc1	Käpp
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
a	a	k8xC	a
Voldemar	Voldemar	k1gMnSc1	Voldemar
Väli	Väl	k1gFnSc2	Väl
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1	jediná
nezápasnická	zápasnický	k2eNgFnSc1d1	zápasnický
medaile	medaile	k1gFnSc1	medaile
byla	být	k5eAaImAgFnS	být
hned	hned	k6eAd1	hned
ta	ten	k3xDgFnSc1	ten
první	první	k4xOgMnSc1	první
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
jí	on	k3xPp3gFnSc3	on
vzpěrač	vzpěrač	k1gMnSc1	vzpěrač
Alfred	Alfred	k1gMnSc1	Alfred
Neuland	Neulanda	k1gFnPc2	Neulanda
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řada	řada	k1gFnSc1	řada
Estonců	Estonec	k1gMnPc2	Estonec
ovšem	ovšem	k9	ovšem
uspěla	uspět	k5eAaPmAgFnS	uspět
i	i	k9	i
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
vzpomenout	vzpomenout	k5eAaPmF	vzpomenout
rychlobruslaře	rychlobruslař	k1gMnSc2	rychlobruslař
Antse	Ants	k1gMnSc2	Ants
Antsona	Antson	k1gMnSc2	Antson
(	(	kIx(	(
<g/>
zlato	zlato	k1gNnSc1	zlato
OH	OH	kA	OH
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zápasníky	zápasník	k1gMnPc7	zápasník
Johannese	Johannese	k1gFnSc2	Johannese
Kotkase	Kotkasa	k1gFnSc3	Kotkasa
(	(	kIx(	(
<g/>
zlato	zlato	k1gNnSc4	zlato
z	z	k7c2	z
OH	OH	kA	OH
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
a	a	k8xC	a
Jaana	Jaana	k1gFnSc1	Jaana
Taltse	Taltse	k1gFnSc2	Taltse
(	(	kIx(	(
<g/>
zlato	zlato	k1gNnSc4	zlato
z	z	k7c2	z
OH	OH	kA	OH
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výškaře	výškař	k1gMnSc2	výškař
Jüri	Jür	k1gFnSc2	Jür
Tarmaka	Tarmak	k1gMnSc2	Tarmak
(	(	kIx(	(
<g/>
zlato	zlato	k1gNnSc4	zlato
z	z	k7c2	z
OH	OH	kA	OH
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cyklistu	cyklista	k1gMnSc4	cyklista
Aavo	Aavo	k1gMnSc1	Aavo
Pikkuuse	Pikkuuse	k1gFnSc2	Pikkuuse
(	(	kIx(	(
<g/>
zlato	zlato	k1gNnSc4	zlato
z	z	k7c2	z
OH	OH	kA	OH
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
či	či	k8xC	či
basketbalistu	basketbalista	k1gMnSc4	basketbalista
Tiita	Tiit	k1gMnSc4	Tiit
Sokka	Sokek	k1gMnSc4	Sokek
(	(	kIx(	(
<g/>
zlato	zlato	k1gNnSc4	zlato
z	z	k7c2	z
OH	OH	kA	OH
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mistry	mistr	k1gMnPc7	mistr
světa	svět	k1gInSc2	svět
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
oštěpař	oštěpař	k1gMnSc1	oštěpař
Andrus	Andrus	k1gMnSc1	Andrus
Värnik	Värnik	k1gMnSc1	Värnik
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šermíř	šermíř	k1gMnSc1	šermíř
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
Novosjolov	Novosjolov	k1gInSc1	Novosjolov
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zápasník	zápasník	k1gMnSc1	zápasník
August	August	k1gMnSc1	August
Englas	Englas	k1gMnSc1	Englas
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
či	či	k8xC	či
veslař	veslař	k1gMnSc1	veslař
Jüri	Jür	k1gFnSc2	Jür
Jaanson	Jaanson	k1gMnSc1	Jaanson
(	(	kIx(	(
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
byl	být	k5eAaImAgInS	být
i	i	k9	i
šachista	šachista	k1gMnSc1	šachista
Paul	Paul	k1gMnSc1	Paul
Keres	Keres	k1gMnSc1	Keres
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Estonsku	Estonsko	k1gNnSc6	Estonsko
vynalezen	vynalezen	k2eAgInSc1d1	vynalezen
nový	nový	k2eAgInSc1d1	nový
sport	sport	k1gInSc1	sport
-	-	kIx~	-
kiiking	kiiking	k1gInSc1	kiiking
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ŠVEC	Švec	k1gMnSc1	Švec
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
pobaltských	pobaltský	k2eAgFnPc2d1	pobaltská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7106	[number]	k4	7106
<g/>
-	-	kIx~	-
<g/>
154	[number]	k4	154
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInPc1d1	státní
symboly	symbol	k1gInPc1	symbol
Estonska	Estonsko	k1gNnSc2	Estonsko
</s>
</p>
<p>
<s>
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
</s>
</p>
<p>
<s>
Baltské	baltský	k2eAgInPc1d1	baltský
státy	stát	k1gInPc1	stát
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Estonsko	Estonsko	k1gNnSc4	Estonsko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Estonsko	Estonsko	k1gNnSc4	Estonsko
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Estonsko	Estonsko	k1gNnSc1	Estonsko
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gInSc4	OpenStreetMap
</s>
</p>
<p>
<s>
Digitální	digitální	k2eAgNnSc1d1	digitální
Estonsko	Estonsko	k1gNnSc1	Estonsko
<g/>
:	:	kIx,	:
kde	kde	k6eAd1	kde
leží	ležet	k5eAaImIp3nS	ležet
klíč	klíč	k1gInSc4	klíč
k	k	k7c3	k
efektivní	efektivní	k2eAgFnSc3d1	efektivní
státní	státní	k2eAgFnSc3d1	státní
správě	správa	k1gFnSc3	správa
(	(	kIx(	(
<g/>
PohledZvenku	PohledZvenka	k1gFnSc4	PohledZvenka
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Estonica	Estonica	k1gFnSc1	Estonica
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
–	–	k?	–
informační	informační	k2eAgInSc4d1	informační
server	server	k1gInSc4	server
o	o	k7c6	o
Estonsku	Estonsko	k1gNnSc6	Estonsko
(	(	kIx(	(
<g/>
estonsky	estonsky	k6eAd1	estonsky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Estonia	Estonium	k1gNnPc1	Estonium
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Estonia	Estonium	k1gNnSc2	Estonium
Country	country	k2eAgInSc4d1	country
Report	report	k1gInSc4	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
European	European	k1gInSc1	European
and	and	k?	and
Eurasian	Eurasian	k1gInSc1	Eurasian
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Not	k1gInSc2	Not
<g/>
:	:	kIx,	:
Estonia	Estonium	k1gNnSc2	Estonium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-06-21	[number]	k4	2011-06-21
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Estonia	Estonium	k1gNnSc2	Estonium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-08-16	[number]	k4	2011-08-16
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Tallinnu	Tallinn	k1gInSc6	Tallinn
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Estonsko	Estonsko	k1gNnSc1	Estonsko
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2010-05-01	[number]	k4	2010-05-01
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
ARUJA	ARUJA	kA	ARUJA
<g/>
,	,	kIx,	,
Endel	Endel	k1gInSc1	Endel
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Estonia	Estonium	k1gNnPc1	Estonium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
22	[number]	k4	22
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Expedition	Expedition	k1gInSc1	Expedition
in	in	k?	in
Estonia	Estonium	k1gNnSc2	Estonium
1987	[number]	k4	1987
–	–	k?	–
Expedice	expedice	k1gFnSc1	expedice
Balt	Balt	k1gInSc1	Balt
1987	[number]	k4	1987
do	do	k7c2	do
Estonska	Estonsko	k1gNnSc2	Estonsko
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
