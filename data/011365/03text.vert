<p>
<s>
Łaziska	Łaziska	k1gFnSc1	Łaziska
je	být	k5eAaImIp3nS	být
vesnice	vesnice	k1gFnPc4	vesnice
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Wodzisław	Wodzisław	k1gFnSc2	Wodzisław
ve	v	k7c6	v
Slezském	slezský	k2eAgNnSc6d1	Slezské
vojvodství	vojvodství	k1gNnSc6	vojvodství
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
,	,	kIx,	,
blízko	blízko	k7c2	blízko
města	město	k1gNnSc2	město
Gorzyce	Gorzyce	k1gFnSc2	Gorzyce
<g/>
.	.	kIx.	.
</s>
<s>
Administrativně	administrativně	k6eAd1	administrativně
spadá	spadat	k5eAaPmIp3nS	spadat
pod	pod	k7c4	pod
gminu	gmina	k1gFnSc4	gmina
Godów	Godów	k1gFnSc2	Godów
<g/>
.	.	kIx.	.
</s>
<s>
Vesnice	vesnice	k1gFnSc1	vesnice
má	mít	k5eAaImIp3nS	mít
1689	[number]	k4	1689
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
u	u	k7c2	u
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dřevěný	dřevěný	k2eAgInSc1d1	dřevěný
kostel	kostel	k1gInSc1	kostel
všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1467	[number]	k4	1467
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
také	také	k9	také
pěší	pěší	k2eAgInSc4d1	pěší
hraniční	hraniční	k2eAgInSc4d1	hraniční
přechod	přechod	k1gInSc4	přechod
Łaziska	Łaziska	k1gFnSc1	Łaziska
-	-	kIx~	-
Věřňovice	Věřňovice	k1gFnSc1	Věřňovice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Łaziska	Łaziska	k1gFnSc1	Łaziska
na	na	k7c4	na
wikimapia	wikimapius	k1gMnSc4	wikimapius
</s>
</p>
