<s>
Jako	jako	k9	jako
první	první	k4xOgFnSc1	první
židovská	židovský	k2eAgFnSc1d1	židovská
válka	válka	k1gFnSc1	válka
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
židovské	židovský	k2eAgNnSc4d1	Židovské
celonárodní	celonárodní	k2eAgNnSc4d1	celonárodní
povstání	povstání	k1gNnSc4	povstání
proti	proti	k7c3	proti
Římu	Řím	k1gInSc3	Řím
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
v	v	k7c6	v
roce	rok	k1gInSc6	rok
66	[number]	k4	66
<g/>
.	.	kIx.	.
</s>
<s>
Povstání	povstání	k1gNnSc4	povstání
potlačily	potlačit	k5eAaPmAgFnP	potlačit
až	až	k6eAd1	až
římské	římský	k2eAgFnPc4d1	římská
legie	legie	k1gFnPc4	legie
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
nejprve	nejprve	k6eAd1	nejprve
Vespasiana	Vespasian	k1gMnSc2	Vespasian
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
69	[number]	k4	69
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Vespasianus	Vespasianus	k1gMnSc1	Vespasianus
stal	stát	k5eAaPmAgMnS	stát
císařem	císař	k1gMnSc7	císař
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jeho	on	k3xPp3gMnSc4	on
syna	syn	k1gMnSc4	syn
Tita	Titus	k1gMnSc4	Titus
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
dobytím	dobytí	k1gNnSc7	dobytí
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
70	[number]	k4	70
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgInSc6	jenž
byl	být	k5eAaImAgInS	být
zničen	zničit	k5eAaPmNgInS	zničit
Druhý	druhý	k4xOgInSc1	druhý
chrám	chrám	k1gInSc1	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
prakticky	prakticky	k6eAd1	prakticky
skončila	skončit	k5eAaPmAgFnS	skončit
dobytím	dobytí	k1gNnSc7	dobytí
hlavního	hlavní	k2eAgNnSc2d1	hlavní
židovského	židovský	k2eAgNnSc2d1	Židovské
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
dohru	dohra	k1gFnSc4	dohra
spočívající	spočívající	k2eAgFnSc7d1	spočívající
v	v	k7c6	v
dobývání	dobývání	k1gNnSc6	dobývání
posledních	poslední	k2eAgFnPc2d1	poslední
pevností	pevnost	k1gFnPc2	pevnost
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
první	první	k4xOgFnSc1	první
byla	být	k5eAaImAgFnS	být
Machairús	Machairúsa	k1gFnPc2	Machairúsa
na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
břehu	břeh	k1gInSc6	břeh
Mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
a	a	k8xC	a
poslední	poslední	k2eAgFnSc1d1	poslední
Masada	Masada	k1gFnSc1	Masada
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
padla	padnout	k5eAaImAgFnS	padnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
73	[number]	k4	73
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jiných	jiný	k2eAgFnPc2d1	jiná
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
74	[number]	k4	74
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnou	podrobný	k2eAgFnSc4d1	podrobná
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
této	tento	k3xDgFnSc6	tento
válce	válka	k1gFnSc6	válka
zanechal	zanechat	k5eAaPmAgMnS	zanechat
její	její	k3xOp3gMnSc1	její
očitý	očitý	k2eAgMnSc1d1	očitý
svědek	svědek	k1gMnSc1	svědek
<g/>
,	,	kIx,	,
účastník	účastník	k1gMnSc1	účastník
a	a	k8xC	a
zpočátku	zpočátku	k6eAd1	zpočátku
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
velitelů	velitel	k1gMnPc2	velitel
židovských	židovský	k2eAgFnPc2d1	židovská
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
Flavius	Flavius	k1gMnSc1	Flavius
Iosephus	Iosephus	k1gMnSc1	Iosephus
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Válka	válka	k1gFnSc1	válka
židovská	židovská	k1gFnSc1	židovská
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
roku	rok	k1gInSc2	rok
66	[number]	k4	66
n.	n.	k?	n.
l.	l.	k?	l.
vypukla	vypuknout	k5eAaPmAgFnS	vypuknout
První	první	k4xOgFnSc1	první
židovská	židovský	k2eAgFnSc1d1	židovská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
nejednalo	jednat	k5eNaImAgNnS	jednat
se	se	k3xPyFc4	se
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
o	o	k7c4	o
překvapující	překvapující	k2eAgFnSc4d1	překvapující
událost	událost	k1gFnSc4	událost
<g/>
.	.	kIx.	.
</s>
<s>
Živná	živný	k2eAgFnSc1d1	živná
půda	půda	k1gFnSc1	půda
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
povstání	povstání	k1gNnSc4	povstání
vznikala	vznikat	k5eAaImAgFnS	vznikat
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Nespokojenost	nespokojenost	k1gFnSc1	nespokojenost
v	v	k7c6	v
židovské	židovský	k2eAgFnSc6d1	židovská
společnosti	společnost	k1gFnSc6	společnost
průběhem	průběh	k1gInSc7	průběh
času	čas	k1gInSc2	čas
celkově	celkově	k6eAd1	celkově
vzato	vzít	k5eAaPmNgNnS	vzít
stoupala	stoupat	k5eAaImAgFnS	stoupat
<g/>
.	.	kIx.	.
</s>
<s>
Projevem	projev	k1gInSc7	projev
tohoto	tento	k3xDgInSc2	tento
byly	být	k5eAaImAgFnP	být
myšlenkové	myšlenkový	k2eAgFnPc1d1	myšlenková
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
postupně	postupně	k6eAd1	postupně
vznikaly	vznikat	k5eAaImAgFnP	vznikat
již	již	k9	již
od	od	k7c2	od
makabejského	makabejský	k2eAgNnSc2d1	Makabejské
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
se	se	k3xPyFc4	se
zřejmě	zřejmě	k6eAd1	zřejmě
ještě	ještě	k6eAd1	ještě
během	během	k7c2	během
povstání	povstání	k1gNnSc2	povstání
vydělila	vydělit	k5eAaPmAgFnS	vydělit
skupina	skupina	k1gFnSc1	skupina
esejců	esejce	k1gMnPc2	esejce
<g/>
,	,	kIx,	,
když	když	k8xS	když
někteří	některý	k3yIgMnPc1	některý
zbožní	zbožní	k2eAgMnPc1d1	zbožní
Židé	Žid	k1gMnPc1	Žid
viděli	vidět	k5eAaImAgMnP	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
povstání	povstání	k1gNnSc1	povstání
nejenže	nejenže	k6eAd1	nejenže
nevede	vést	k5eNaImIp3nS	vést
ke	k	k7c3	k
kýženým	kýžený	k2eAgInPc3d1	kýžený
výsledkům	výsledek	k1gInPc3	výsledek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
že	že	k8xS	že
dokonce	dokonce	k9	dokonce
samotní	samotný	k2eAgMnPc1d1	samotný
Hasmonejci	Hasmonejec	k1gMnPc1	Hasmonejec
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Alexandr	Alexandr	k1gMnSc1	Alexandr
Janaj	Janaj	k1gMnSc1	Janaj
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
Jochanan	Jochanany	k1gInPc2	Jochanany
Hyrkán	Hyrkán	k2eAgMnSc1d1	Hyrkán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
koncentrují	koncentrovat	k5eAaBmIp3nP	koncentrovat
v	v	k7c6	v
rukách	ruka	k1gFnPc6	ruka
moc	moc	k6eAd1	moc
královskou	královský	k2eAgFnSc4d1	královská
i	i	k8xC	i
kněžskou	kněžský	k2eAgFnSc4d1	kněžská
a	a	k8xC	a
sami	sám	k3xTgMnPc1	sám
podléhají	podléhat	k5eAaImIp3nP	podléhat
vlivům	vliv	k1gInPc3	vliv
helénismu	helénismus	k1gInSc2	helénismus
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
odebrala	odebrat	k5eAaPmAgFnS	odebrat
do	do	k7c2	do
ústraní	ústraní	k1gNnSc2	ústraní
k	k	k7c3	k
Mrtvému	mrtvý	k2eAgNnSc3d1	mrtvé
moři	moře	k1gNnSc3	moře
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Kumránu	Kumrán	k1gInSc2	Kumrán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
však	však	k9	však
významná	významný	k2eAgFnSc1d1	významná
esejská	esejský	k2eAgFnSc1d1	esejská
komunita	komunita	k1gFnSc1	komunita
žila	žít	k5eAaImAgFnS	žít
i	i	k9	i
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
členové	člen	k1gMnPc1	člen
také	také	k6eAd1	také
roztroušeni	roztroušet	k5eAaImNgMnP	roztroušet
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
oblasti	oblast	k1gFnSc6	oblast
obývané	obývaný	k2eAgMnPc4d1	obývaný
Židy	Žid	k1gMnPc4	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačovali	vyznačovat	k5eAaImAgMnP	vyznačovat
se	se	k3xPyFc4	se
asketickým	asketický	k2eAgInSc7d1	asketický
životem	život	k1gInSc7	život
<g/>
,	,	kIx,	,
obzvláštní	obzvláštní	k2eAgFnSc7d1	obzvláštní
zbožností	zbožnost	k1gFnSc7	zbožnost
a	a	k8xC	a
horlivým	horlivý	k2eAgNnSc7d1	horlivé
studiem	studio	k1gNnSc7	studio
posvátných	posvátný	k2eAgInPc2d1	posvátný
textů	text	k1gInPc2	text
–	–	k?	–
část	část	k1gFnSc1	část
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zůstala	zůstat	k5eAaPmAgFnS	zůstat
dvě	dva	k4xCgNnPc4	dva
tisíciletí	tisíciletí	k1gNnPc2	tisíciletí
ukryta	ukryt	k2eAgFnSc1d1	ukryta
v	v	k7c6	v
jeskyních	jeskyně	k1gFnPc6	jeskyně
u	u	k7c2	u
Mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
byla	být	k5eAaImAgNnP	být
objevena	objevit	k5eAaPmNgNnP	objevit
až	až	k6eAd1	až
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
hnutím	hnutí	k1gNnSc7	hnutí
<g/>
,	,	kIx,	,
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
počátku	počátek	k1gInSc6	počátek
stála	stát	k5eAaImAgFnS	stát
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
farizeové	farizeus	k1gMnPc1	farizeus
<g/>
.	.	kIx.	.
</s>
<s>
Vymezovali	vymezovat	k5eAaImAgMnP	vymezovat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
proti	proti	k7c3	proti
saduceům	saduceus	k1gMnPc3	saduceus
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
byla	být	k5eAaImAgFnS	být
jeruzalémská	jeruzalémský	k2eAgFnSc1d1	Jeruzalémská
kněžská	kněžský	k2eAgFnSc1d1	kněžská
aristokracie	aristokracie	k1gFnSc1	aristokracie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
však	však	k9	však
spíše	spíše	k9	spíše
než	než	k8xS	než
na	na	k7c4	na
bohoslužbu	bohoslužba	k1gFnSc4	bohoslužba
dbala	dbát	k5eAaImAgFnS	dbát
na	na	k7c4	na
politické	politický	k2eAgInPc4d1	politický
a	a	k8xC	a
mocenské	mocenský	k2eAgInPc4d1	mocenský
zájmy	zájem	k1gInPc4	zájem
<g/>
,	,	kIx,	,
a	a	k8xC	a
která	který	k3yRgFnSc1	který
tak	tak	k6eAd1	tak
proto	proto	k8xC	proto
neměla	mít	k5eNaImAgFnS	mít
širší	široký	k2eAgFnSc4d2	širší
oporu	opora	k1gFnSc4	opora
v	v	k7c6	v
široké	široký	k2eAgFnSc6d1	široká
vrstvě	vrstva	k1gFnSc6	vrstva
zbožných	zbožný	k2eAgMnPc2d1	zbožný
prostých	prostý	k2eAgMnPc2d1	prostý
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
ty	ten	k3xDgFnPc4	ten
se	se	k3xPyFc4	se
naopak	naopak	k6eAd1	naopak
opírali	opírat	k5eAaImAgMnP	opírat
právě	právě	k9	právě
farizeové	farizeus	k1gMnPc1	farizeus
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
žádali	žádat	k5eAaImAgMnP	žádat
návrat	návrat	k1gInSc4	návrat
k	k	k7c3	k
původním	původní	k2eAgFnPc3d1	původní
myšlenkám	myšlenka	k1gFnPc3	myšlenka
makabejského	makabejský	k2eAgNnSc2d1	Makabejské
povstání	povstání	k1gNnSc2	povstání
–	–	k?	–
tedy	tedy	k9	tedy
návrat	návrat	k1gInSc4	návrat
ke	k	k7c3	k
službě	služba	k1gFnSc3	služba
Hospodinu	Hospodin	k1gMnSc6	Hospodin
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
esejců	esejce	k1gMnPc2	esejce
se	se	k3xPyFc4	se
farizeové	farizeus	k1gMnPc1	farizeus
neuzavírali	uzavírat	k5eNaImAgMnP	uzavírat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
naopak	naopak	k6eAd1	naopak
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
přenést	přenést	k5eAaPmF	přenést
náboženské	náboženský	k2eAgInPc4d1	náboženský
ideály	ideál	k1gInPc4	ideál
do	do	k7c2	do
politické	politický	k2eAgFnSc2d1	politická
praxe	praxe	k1gFnSc2	praxe
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
často	často	k6eAd1	často
sami	sám	k3xTgMnPc1	sám
dopouštěli	dopouštět	k5eAaImAgMnP	dopouštět
činů	čin	k1gInPc2	čin
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
kterým	který	k3yRgFnPc3	který
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
saduceje	saducej	k1gMnPc4	saducej
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
vstupování	vstupování	k1gNnSc1	vstupování
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
ovšem	ovšem	k9	ovšem
mnohdy	mnohdy	k6eAd1	mnohdy
naráželo	narážet	k5eAaPmAgNnS	narážet
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
saduceů	saduceus	k1gMnPc2	saduceus
a	a	k8xC	a
vládnoucích	vládnoucí	k2eAgMnPc2d1	vládnoucí
Hasmonejců	Hasmonejec	k1gMnPc2	Hasmonejec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
farizeje	farizej	k1gMnSc4	farizej
mnohdy	mnohdy	k6eAd1	mnohdy
krutě	krutě	k6eAd1	krutě
pronásledovali	pronásledovat	k5eAaImAgMnP	pronásledovat
<g/>
,	,	kIx,	,
především	především	k9	především
Alexandr	Alexandr	k1gMnSc1	Alexandr
Janaj	Janaj	k1gMnSc1	Janaj
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
vyvraždil	vyvraždit	k5eAaPmAgMnS	vyvraždit
během	během	k7c2	během
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
nastala	nastat	k5eAaPmAgFnS	nastat
po	po	k7c6	po
králově	králův	k2eAgFnSc6d1	králova
smrti	smrt	k1gFnSc6	smrt
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
královny	královna	k1gFnSc2	královna
Salómé	Salómý	k2eAgFnSc2d1	Salómý
Alexandry	Alexandra	k1gFnSc2	Alexandra
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
dělící	dělící	k2eAgFnSc7d1	dělící
čarou	čára	k1gFnSc7	čára
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
byla	být	k5eAaImAgFnS	být
averze	averze	k1gFnSc1	averze
mezi	mezi	k7c4	mezi
Židy	Žid	k1gMnPc4	Žid
a	a	k8xC	a
Samaritány	Samaritán	k1gMnPc4	Samaritán
<g/>
.	.	kIx.	.
</s>
<s>
Přátelští	přátelský	k2eAgMnPc1d1	přátelský
nebyli	být	k5eNaImAgMnP	být
ani	ani	k8xC	ani
Idumejci	Idumejec	k1gMnPc1	Idumejec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
expanzi	expanze	k1gFnSc4	expanze
Jochanana	Jochanana	k1gFnSc1	Jochanana
Hyrkána	Hyrkán	k2eAgFnSc1d1	Hyrkána
(	(	kIx(	(
<g/>
135	[number]	k4	135
<g/>
–	–	k?	–
<g/>
104	[number]	k4	104
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přinuceni	přinutit	k5eAaPmNgMnP	přinutit
nechat	nechat	k5eAaPmF	nechat
se	se	k3xPyFc4	se
obřezat	obřezat	k5eAaPmF	obřezat
a	a	k8xC	a
přijmout	přijmout	k5eAaPmF	přijmout
židovství	židovství	k1gNnSc4	židovství
<g/>
.	.	kIx.	.
</s>
<s>
Judsko	Judsko	k1gNnSc1	Judsko
sice	sice	k8xC	sice
v	v	k7c6	v
makabejském	makabejský	k2eAgNnSc6d1	Makabejské
povstání	povstání	k1gNnSc6	povstání
získalo	získat	k5eAaPmAgNnS	získat
samostatnost	samostatnost	k1gFnSc4	samostatnost
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
hrozba	hrozba	k1gFnSc1	hrozba
seleukovské	seleukovský	k2eAgFnSc2d1	seleukovská
nadvlády	nadvláda	k1gFnSc2	nadvláda
trvala	trvat	k5eAaImAgFnS	trvat
nadále	nadále	k6eAd1	nadále
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
mnohé	mnohý	k2eAgInPc1d1	mnohý
tehdejší	tehdejší	k2eAgInPc1d1	tehdejší
malé	malý	k2eAgInPc1d1	malý
státy	stát	k1gInPc1	stát
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
Středomoří	středomoří	k1gNnSc6	středomoří
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Pergamon	pergamon	k1gInSc1	pergamon
a	a	k8xC	a
Rhodos	Rhodos	k1gInSc1	Rhodos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k8xC	i
Judea	Judea	k1gFnSc1	Judea
se	se	k3xPyFc4	se
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
přikláněla	přiklánět	k5eAaImAgFnS	přiklánět
k	k	k7c3	k
římské	římský	k2eAgFnSc3d1	římská
říši	říš	k1gFnSc3	říš
a	a	k8xC	a
doufala	doufat	k5eAaImAgFnS	doufat
v	v	k7c4	v
její	její	k3xOp3gFnSc4	její
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
již	již	k9	již
od	od	k7c2	od
makabejské	makabejský	k2eAgFnSc2d1	Makabejská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
sice	sice	k8xC	sice
roku	rok	k1gInSc2	rok
64	[number]	k4	64
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
skutečně	skutečně	k6eAd1	skutečně
seleukovskou	seleukovský	k2eAgFnSc4d1	seleukovská
říši	říše	k1gFnSc4	říše
rozdrtili	rozdrtit	k5eAaPmAgMnP	rozdrtit
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
neustálé	neustálý	k2eAgInPc4d1	neustálý
spory	spor	k1gInPc4	spor
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
byly	být	k5eAaImAgFnP	být
obzvlášť	obzvlášť	k9	obzvlášť
vyhrocené	vyhrocený	k2eAgFnPc1d1	vyhrocená
–	–	k?	–
tehdy	tehdy	k6eAd1	tehdy
o	o	k7c4	o
vládu	vláda	k1gFnSc4	vláda
soupeřili	soupeřit	k5eAaImAgMnP	soupeřit
dva	dva	k4xCgMnPc1	dva
synové	syn	k1gMnPc1	syn
Salome	Salom	k1gInSc5	Salom
Alexandry	Alexandra	k1gFnPc1	Alexandra
<g/>
,	,	kIx,	,
Aristobulos	Aristobulos	k1gInSc1	Aristobulos
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Hyrkán	Hyrkán	k2eAgInSc1d1	Hyrkán
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
chtěl	chtít	k5eAaImAgMnS	chtít
využít	využít	k5eAaPmF	využít
římské	římský	k2eAgFnSc2d1	římská
přítomnosti	přítomnost	k1gFnSc2	přítomnost
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
právě	právě	k6eAd1	právě
jemu	on	k3xPp3gMnSc3	on
Římané	Říman	k1gMnPc1	Říman
pomohli	pomoct	k5eAaPmAgMnP	pomoct
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
prvotní	prvotní	k2eAgFnSc4d1	prvotní
náklonnost	náklonnost	k1gFnSc4	náklonnost
Aristobulovi	Aristobulův	k2eAgMnPc1d1	Aristobulův
nakonec	nakonec	k6eAd1	nakonec
podpořili	podpořit	k5eAaPmAgMnP	podpořit
Hyrkána	Hyrkán	k2eAgMnSc4d1	Hyrkán
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
těšil	těšit	k5eAaImAgInS	těšit
sympatiím	sympatie	k1gFnPc3	sympatie
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Pompeius	Pompeius	k1gInSc4	Pompeius
nakonec	nakonec	k6eAd1	nakonec
roku	rok	k1gInSc2	rok
63	[number]	k4	63
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vtáhl	vtáhnout	k5eAaPmAgMnS	vtáhnout
s	s	k7c7	s
vojskem	vojsko	k1gNnSc7	vojsko
do	do	k7c2	do
Palestiny	Palestina	k1gFnSc2	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
oblehl	oblehnout	k5eAaPmAgMnS	oblehnout
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
<g/>
,	,	kIx,	,
nastal	nastat	k5eAaPmAgInS	nastat
uvnitř	uvnitř	k7c2	uvnitř
města	město	k1gNnSc2	město
rozbroj	rozbroj	k1gInSc1	rozbroj
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
přívrženci	přívrženec	k1gMnPc1	přívrženec
Aristobúla	Aristobúlo	k1gNnSc2	Aristobúlo
město	město	k1gNnSc4	město
bránili	bránit	k5eAaImAgMnP	bránit
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Hyrkánovi	Hyrkánův	k2eAgMnPc1d1	Hyrkánův
stoupenci	stoupenec	k1gMnPc1	stoupenec
Římanům	Říman	k1gMnPc3	Říman
otevřeli	otevřít	k5eAaPmAgMnP	otevřít
brány	brána	k1gFnPc4	brána
a	a	k8xC	a
předali	předat	k5eAaPmAgMnP	předat
jim	on	k3xPp3gMnPc3	on
královský	královský	k2eAgInSc4d1	královský
poklad	poklad	k1gInSc4	poklad
<g/>
.	.	kIx.	.
</s>
<s>
Hyrkánovi	Hyrkánův	k2eAgMnPc1d1	Hyrkánův
však	však	k9	však
byl	být	k5eAaImAgMnS	být
Pompeiem	Pompeius	k1gMnSc7	Pompeius
svěřen	svěřit	k5eAaPmNgMnS	svěřit
pouze	pouze	k6eAd1	pouze
velekněžský	velekněžský	k2eAgInSc1d1	velekněžský
úřad	úřad	k1gInSc1	úřad
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
již	již	k6eAd1	již
královský	královský	k2eAgMnSc1d1	královský
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
Judea	Judea	k1gFnSc1	Judea
dostala	dostat	k5eAaPmAgFnS	dostat
pod	pod	k7c4	pod
římskou	římský	k2eAgFnSc4d1	římská
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Římský	římský	k2eAgMnSc1d1	římský
místodržící	místodržící	k1gMnSc1	místodržící
Gabinus	Gabinus	k1gMnSc1	Gabinus
zemi	zem	k1gFnSc4	zem
rozdělil	rozdělit	k5eAaPmAgMnS	rozdělit
na	na	k7c4	na
pět	pět	k4xCc4	pět
berních	berní	k2eAgInPc2d1	berní
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
oslabil	oslabit	k5eAaPmAgInS	oslabit
dřívější	dřívější	k2eAgInSc4d1	dřívější
centralismus	centralismus	k1gInSc4	centralismus
a	a	k8xC	a
snadněji	snadno	k6eAd2	snadno
prosazoval	prosazovat	k5eAaImAgMnS	prosazovat
římské	římský	k2eAgInPc4d1	římský
zájmy	zájem	k1gInPc4	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
bezprostřednosti	bezprostřednost	k1gFnSc2	bezprostřednost
římské	římský	k2eAgFnSc2d1	římská
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
měnila	měnit	k5eAaImAgFnS	měnit
<g/>
:	:	kIx,	:
od	od	k7c2	od
přímé	přímý	k2eAgFnSc2d1	přímá
vlády	vláda	k1gFnSc2	vláda
římského	římský	k2eAgMnSc2d1	římský
místodržitele	místodržitel	k1gMnSc2	místodržitel
<g/>
,	,	kIx,	,
po	po	k7c4	po
vládu	vláda	k1gFnSc4	vláda
Idumejce	Idumejec	k1gMnSc2	Idumejec
Heroda	Herod	k1gMnSc2	Herod
–	–	k?	–
i	i	k9	i
ten	ten	k3xDgInSc1	ten
však	však	k9	však
byl	být	k5eAaImAgInS	být
římským	římský	k2eAgMnSc7d1	římský
vazalem	vazal	k1gMnSc7	vazal
<g/>
,	,	kIx,	,
právo	práv	k2eAgNnSc1d1	právo
království	království	k1gNnSc1	království
zakládal	zakládat	k5eAaImAgInS	zakládat
na	na	k7c6	na
římském	římský	k2eAgNnSc6d1	římské
právu	právo	k1gNnSc6	právo
a	a	k8xC	a
byť	byť	k8xS	byť
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
(	(	kIx(	(
<g/>
např.	např.	kA	např.
velkolepou	velkolepý	k2eAgFnSc7d1	velkolepá
přestavbou	přestavba	k1gFnSc7	přestavba
jeruzalémského	jeruzalémský	k2eAgInSc2d1	jeruzalémský
chrámu	chrám	k1gInSc2	chrám
<g/>
)	)	kIx)	)
získat	získat	k5eAaPmF	získat
sympatie	sympatie	k1gFnPc4	sympatie
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
nikdy	nikdy	k6eAd1	nikdy
je	být	k5eAaImIp3nS	být
zcela	zcela	k6eAd1	zcela
nezískal	získat	k5eNaPmAgMnS	získat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sporech	spor	k1gInPc6	spor
o	o	k7c4	o
Herodovo	Herodův	k2eAgNnSc4d1	Herodovo
následnictví	následnictví	k1gNnSc4	následnictví
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
Judea	Judea	k1gFnSc1	Judea
stala	stát	k5eAaPmAgFnS	stát
roku	rok	k1gInSc2	rok
6	[number]	k4	6
n.	n.	k?	n.
l.	l.	k?	l.
římskou	římský	k2eAgFnSc7d1	římská
provincií	provincie	k1gFnSc7	provincie
<g/>
,	,	kIx,	,
spravovanou	spravovaný	k2eAgFnSc4d1	spravovaná
římskými	římský	k2eAgMnPc7d1	římský
prokurátory	prokurátor	k1gMnPc7	prokurátor
<g/>
,	,	kIx,	,
sídlícími	sídlící	k2eAgInPc7d1	sídlící
v	v	k7c6	v
Caesareji	Caesarej	k1gInSc6	Caesarej
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgNnPc2	jenž
nejznámější	známý	k2eAgMnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
Pontius	Pontius	k1gMnSc1	Pontius
Pilatus	Pilatus	k1gMnSc1	Pilatus
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
vůči	vůči	k7c3	vůči
židovským	židovský	k2eAgFnPc3d1	židovská
záležitostem	záležitost	k1gFnPc3	záležitost
dosti	dosti	k6eAd1	dosti
bezohledný	bezohledný	k2eAgInSc4d1	bezohledný
–	–	k?	–
přenesl	přenést	k5eAaPmAgInS	přenést
římské	římský	k2eAgFnPc4d1	římská
zástavy	zástava	k1gFnPc4	zástava
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
,	,	kIx,	,
výstavbu	výstavba	k1gFnSc4	výstavba
akvaduktu	akvadukt	k1gInSc2	akvadukt
financoval	financovat	k5eAaBmAgInS	financovat
z	z	k7c2	z
chrámového	chrámový	k2eAgInSc2d1	chrámový
pokladu	poklad	k1gInSc2	poklad
<g/>
,	,	kIx,	,
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
postavit	postavit	k5eAaPmF	postavit
v	v	k7c6	v
jeruzalémském	jeruzalémský	k2eAgInSc6d1	jeruzalémský
chrámu	chrám	k1gInSc6	chrám
sochu	socha	k1gFnSc4	socha
zbožštěného	zbožštěný	k2eAgMnSc2d1	zbožštěný
císaře	císař	k1gMnSc2	císař
Caliguly	Caligula	k1gFnSc2	Caligula
<g/>
.	.	kIx.	.
</s>
<s>
Takovými	takový	k3xDgInPc7	takový
a	a	k8xC	a
podobnými	podobný	k2eAgInPc7d1	podobný
kroky	krok	k1gInPc7	krok
byl	být	k5eAaImAgInS	být
podnícen	podnícen	k2eAgInSc1d1	podnícen
vznik	vznik	k1gInSc1	vznik
další	další	k2eAgFnSc2d1	další
skupiny	skupina	k1gFnSc2	skupina
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
nazývali	nazývat	k5eAaImAgMnP	nazývat
zelóti	zelót	k1gMnPc1	zelót
(	(	kIx(	(
<g/>
horlivci	horlivec	k1gMnPc1	horlivec
<g/>
,	,	kIx,	,
žárlivci	žárlivec	k1gMnPc1	žárlivec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hebrejsky	hebrejsky	k6eAd1	hebrejsky
kan	kan	k?	kan
<g/>
'	'	kIx"	'
<g/>
im	im	k?	im
(	(	kIx(	(
<g/>
ק	ק	k?	ק
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
–	–	k?	–
tedy	tedy	k9	tedy
"	"	kIx"	"
<g/>
horlivci	horlivec	k1gMnPc1	horlivec
<g/>
"	"	kIx"	"
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
římské	římský	k2eAgFnSc3d1	římská
nadvládě	nadvláda	k1gFnSc3	nadvláda
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
proti	proti	k7c3	proti
daním	daň	k1gFnPc3	daň
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
římskou	římský	k2eAgFnSc4d1	římská
hegemonii	hegemonie	k1gFnSc4	hegemonie
symbolizovaly	symbolizovat	k5eAaImAgInP	symbolizovat
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
učení	učení	k1gNnSc1	učení
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
mnohém	mnohé	k1gNnSc6	mnohé
podobné	podobný	k2eAgFnSc2d1	podobná
farizejskému	farizejský	k2eAgInSc3d1	farizejský
<g/>
,	,	kIx,	,
hnutí	hnutí	k1gNnSc1	hnutí
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
mnohem	mnohem	k6eAd1	mnohem
radikálnější	radikální	k2eAgNnSc1d2	radikálnější
a	a	k8xC	a
nabývalo	nabývat	k5eAaImAgNnS	nabývat
podoby	podoba	k1gFnSc2	podoba
banditismu	banditismus	k1gInSc2	banditismus
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
44	[number]	k4	44
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
dostala	dostat	k5eAaPmAgFnS	dostat
pod	pod	k7c4	pod
přímou	přímý	k2eAgFnSc4d1	přímá
římskou	římský	k2eAgFnSc4d1	římská
správu	správa	k1gFnSc4	správa
celá	celý	k2eAgFnSc1d1	celá
Palestina	Palestina	k1gFnSc1	Palestina
<g/>
.	.	kIx.	.
</s>
<s>
Prokurátoři	prokurátor	k1gMnPc1	prokurátor
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
však	však	k9	však
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
spravovali	spravovat	k5eAaImAgMnP	spravovat
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
neschopní	schopný	k2eNgMnPc1d1	neschopný
udržet	udržet	k5eAaPmF	udržet
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
pořádek	pořádek	k1gInSc4	pořádek
<g/>
,	,	kIx,	,
neuváženě	uváženě	k6eNd1	uváženě
zvyšovali	zvyšovat	k5eAaImAgMnP	zvyšovat
daně	daň	k1gFnSc2	daň
a	a	k8xC	a
spíše	spíše	k9	spíše
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
vyzískat	vyzískat	k5eAaPmF	vyzískat
co	co	k9	co
nejvíce	nejvíce	k6eAd1	nejvíce
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Narůstalo	narůstat	k5eAaImAgNnS	narůstat
srážek	srážka	k1gFnPc2	srážka
s	s	k7c7	s
Židy	Žid	k1gMnPc7	Žid
<g/>
,	,	kIx,	,
rostl	růst	k5eAaImAgInS	růst
vliv	vliv	k1gInSc4	vliv
zelótů	zelóta	k1gMnPc2	zelóta
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přišli	přijít	k5eAaPmAgMnP	přijít
s	s	k7c7	s
novou	nový	k2eAgFnSc7d1	nová
taktikou	taktika	k1gFnSc7	taktika
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
v	v	k7c6	v
nepřehledném	přehledný	k2eNgInSc6d1	nepřehledný
davu	dav	k1gInSc6	dav
vraždili	vraždit	k5eAaImAgMnP	vraždit
dýkami	dýka	k1gFnPc7	dýka
své	svůj	k3xOyFgMnPc4	svůj
odpůrce	odpůrce	k1gMnPc4	odpůrce
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
zelótů	zelót	k1gInPc2	zelót
vznikaly	vznikat	k5eAaImAgInP	vznikat
i	i	k9	i
další	další	k2eAgInPc1d1	další
malé	malý	k2eAgInPc1d1	malý
tlupy	tlup	k1gInPc1	tlup
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mohly	moct	k5eAaImAgInP	moct
díky	díky	k7c3	díky
úplatnosti	úplatnost	k1gFnSc3	úplatnost
římských	římský	k2eAgMnPc2d1	římský
úředníků	úředník	k1gMnPc2	úředník
beztrestně	beztrestně	k6eAd1	beztrestně
olupovat	olupovat	k5eAaImF	olupovat
umírněné	umírněný	k2eAgMnPc4d1	umírněný
občany	občan	k1gMnPc4	občan
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
i	i	k9	i
kněžská	kněžský	k2eAgFnSc1d1	kněžská
a	a	k8xC	a
městská	městský	k2eAgFnSc1d1	městská
aristokracie	aristokracie	k1gFnSc1	aristokracie
si	se	k3xPyFc3	se
začala	začít	k5eAaPmAgFnS	začít
vytvářet	vytvářet	k5eAaImF	vytvářet
bojůvky	bojůvka	k1gFnPc4	bojůvka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zmatené	zmatený	k2eAgFnSc6d1	zmatená
a	a	k8xC	a
nejisté	jistý	k2eNgFnSc6d1	nejistá
situaci	situace	k1gFnSc6	situace
zároveň	zároveň	k6eAd1	zároveň
přibývalo	přibývat	k5eAaImAgNnS	přibývat
mesianistických	mesianistický	k2eAgMnPc2d1	mesianistický
proroků	prorok	k1gMnPc2	prorok
i	i	k8xC	i
demagogů	demagog	k1gMnPc2	demagog
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
ocitla	ocitnout	k5eAaPmAgFnS	ocitnout
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
chaosu	chaos	k1gInSc2	chaos
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
napjaté	napjatý	k2eAgFnSc6d1	napjatá
situaci	situace	k1gFnSc6	situace
poslední	poslední	k2eAgMnSc1d1	poslední
římský	římský	k2eAgMnSc1d1	římský
prokurátor	prokurátor	k1gMnSc1	prokurátor
Gessius	Gessius	k1gMnSc1	Gessius
Florus	Florus	k1gMnSc1	Florus
(	(	kIx(	(
<g/>
64	[number]	k4	64
<g/>
–	–	k?	–
<g/>
66	[number]	k4	66
<g/>
)	)	kIx)	)
Židy	Žid	k1gMnPc7	Žid
neustále	neustále	k6eAd1	neustále
svými	svůj	k3xOyFgMnPc7	svůj
rozhodnutími	rozhodnutí	k1gNnPc7	rozhodnutí
provokoval	provokovat	k5eAaImAgMnS	provokovat
<g/>
,	,	kIx,	,
vrcholem	vrchol	k1gInSc7	vrchol
čehož	což	k3yQnSc2	což
bylo	být	k5eAaImAgNnS	být
vyplenění	vyplenění	k1gNnSc4	vyplenění
chrámového	chrámový	k2eAgInSc2d1	chrámový
pokladu	poklad	k1gInSc2	poklad
<g/>
,	,	kIx,	,
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
povstání	povstání	k1gNnSc1	povstání
–	–	k?	–
zelóti	zelóť	k1gFnSc2	zelóť
vypálili	vypálit	k5eAaPmAgMnP	vypálit
archív	archív	k1gInSc4	archív
jeruzalémské	jeruzalémský	k2eAgFnSc2d1	Jeruzalémská
radnice	radnice	k1gFnSc2	radnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
uschovány	uschován	k2eAgInPc4d1	uschován
dlužní	dlužní	k2eAgInPc4d1	dlužní
úpisy	úpis	k1gInPc4	úpis
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
si	se	k3xPyFc3	se
získali	získat	k5eAaPmAgMnP	získat
vrstvu	vrstva	k1gFnSc4	vrstva
chudých	chudý	k2eAgInPc2d1	chudý
<g/>
,	,	kIx,	,
dobyli	dobýt	k5eAaPmAgMnP	dobýt
pevnost	pevnost	k1gFnSc4	pevnost
Masadu	Masad	k1gInSc2	Masad
a	a	k8xC	a
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
přestaly	přestat	k5eAaPmAgFnP	přestat
být	být	k5eAaImF	být
přinášeny	přinášen	k2eAgFnPc1d1	přinášena
oběti	oběť	k1gFnPc1	oběť
za	za	k7c4	za
císaře	císař	k1gMnSc4	císař
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
již	již	k6eAd1	již
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
povstání	povstání	k1gNnSc2	povstání
se	se	k3xPyFc4	se
projevila	projevit	k5eAaPmAgFnS	projevit
obrovská	obrovský	k2eAgFnSc1d1	obrovská
rozštěpenost	rozštěpenost	k1gFnSc1	rozštěpenost
židovské	židovský	k2eAgFnSc2d1	židovská
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Zelóti	Zelót	k1gMnPc1	Zelót
bojovali	bojovat	k5eAaImAgMnP	bojovat
nejen	nejen	k6eAd1	nejen
proti	proti	k7c3	proti
římským	římský	k2eAgMnPc3d1	římský
vojákům	voják	k1gMnPc3	voják
(	(	kIx(	(
<g/>
ti	ten	k3xDgMnPc1	ten
město	město	k1gNnSc4	město
brzy	brzy	k6eAd1	brzy
opustili	opustit	k5eAaPmAgMnP	opustit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
proti	proti	k7c3	proti
všem	všecek	k3xTgMnPc3	všecek
zastáncům	zastánce	k1gMnPc3	zastánce
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yRgMnPc4	který
patřily	patřit	k5eAaImAgFnP	patřit
zejména	zejména	k6eAd1	zejména
vyšší	vysoký	k2eAgFnPc1d2	vyšší
bohatší	bohatý	k2eAgFnPc1d2	bohatší
vrstvy	vrstva	k1gFnPc1	vrstva
–	–	k?	–
velekněze	velekněz	k1gMnPc4	velekněz
Ananiáše	Ananiáš	k1gMnPc4	Ananiáš
dokonce	dokonce	k9	dokonce
zavraždili	zavraždit	k5eAaPmAgMnP	zavraždit
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
však	však	k9	však
začali	začít	k5eAaPmAgMnP	začít
štěpit	štěpit	k5eAaImF	štěpit
i	i	k9	i
samotní	samotný	k2eAgMnPc1d1	samotný
zelóti	zelót	k1gMnPc1	zelót
<g/>
.	.	kIx.	.
</s>
<s>
Pořádek	pořádek	k1gInSc1	pořádek
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
měl	mít	k5eAaImAgMnS	mít
zajistit	zajistit	k5eAaPmF	zajistit
Cestius	Cestius	k1gMnSc1	Cestius
Gallus	Gallus	k1gMnSc1	Gallus
<g/>
,	,	kIx,	,
římský	římský	k2eAgMnSc1d1	římský
místodržitel	místodržitel	k1gMnSc1	místodržitel
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
do	do	k7c2	do
Judska	Judsko	k1gNnSc2	Judsko
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
tímto	tento	k3xDgInSc7	tento
tlakem	tlak	k1gInSc7	tlak
byly	být	k5eAaImAgFnP	být
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
židovské	židovský	k2eAgFnPc1d1	židovská
frakce	frakce	k1gFnPc1	frakce
nuceny	nucen	k2eAgFnPc1d1	nucena
se	se	k3xPyFc4	se
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
sjednotit	sjednotit	k5eAaPmF	sjednotit
–	–	k?	–
a	a	k8xC	a
společně	společně	k6eAd1	společně
tak	tak	k6eAd1	tak
potom	potom	k6eAd1	potom
byli	být	k5eAaImAgMnP	být
schopni	schopen	k2eAgMnPc1d1	schopen
Galla	Gall	k1gMnSc2	Gall
porazit	porazit	k5eAaPmF	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
vítězství	vítězství	k1gNnSc6	vítězství
Židů	Žid	k1gMnPc2	Žid
již	jenž	k3xRgMnPc1	jenž
odpůrci	odpůrce	k1gMnPc1	odpůrce
povstání	povstání	k1gNnSc4	povstání
téměř	téměř	k6eAd1	téměř
neexistovali	existovat	k5eNaImAgMnP	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
samotný	samotný	k2eAgMnSc1d1	samotný
velekněz	velekněz	k1gMnSc1	velekněz
a	a	k8xC	a
kněží	kněz	k1gMnPc1	kněz
se	se	k3xPyFc4	se
ujali	ujmout	k5eAaPmAgMnP	ujmout
iniciativy	iniciativa	k1gFnSc2	iniciativa
v	v	k7c6	v
systematickém	systematický	k2eAgNnSc6d1	systematické
vedení	vedení	k1gNnSc6	vedení
války	válka	k1gFnSc2	válka
a	a	k8xC	a
do	do	k7c2	do
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
země	zem	k1gFnSc2	zem
vyslali	vyslat	k5eAaPmAgMnP	vyslat
velitele	velitel	k1gMnSc4	velitel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
povstání	povstání	k1gNnSc4	povstání
organizovat	organizovat	k5eAaBmF	organizovat
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
významných	významný	k2eAgMnPc2d1	významný
velitelů	velitel	k1gMnPc2	velitel
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Josef	Josef	k1gMnSc1	Josef
ben	ben	k?	ben
Matitjahu	Matitjaha	k1gFnSc4	Matitjaha
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
poslán	poslat	k5eAaPmNgMnS	poslat
do	do	k7c2	do
Galileje	Galilea	k1gFnSc2	Galilea
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
velel	velet	k5eAaImAgInS	velet
obraně	obraně	k6eAd1	obraně
Galileje	Galilea	k1gFnSc2	Galilea
a	a	k8xC	a
poté	poté	k6eAd1	poté
pevnosti	pevnost	k1gFnSc3	pevnost
Iótapata	Iótapat	k2eAgFnSc1d1	Iótapat
<g/>
.	.	kIx.	.
</s>
<s>
Vespasianus	Vespasianus	k1gMnSc1	Vespasianus
přitáhl	přitáhnout	k5eAaPmAgMnS	přitáhnout
se	s	k7c7	s
třemi	tři	k4xCgFnPc7	tři
legiemi	legie	k1gFnPc7	legie
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
pomocnými	pomocný	k2eAgInPc7d1	pomocný
sbory	sbor	k1gInPc7	sbor
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
do	do	k7c2	do
Galileje	Galilea	k1gFnSc2	Galilea
<g/>
.	.	kIx.	.
</s>
<s>
Židovské	židovský	k2eAgInPc1d1	židovský
oddíly	oddíl	k1gInPc1	oddíl
byly	být	k5eAaImAgInP	být
rozděleny	rozdělit	k5eAaPmNgInP	rozdělit
frakčními	frakční	k2eAgFnPc7d1	frakční
sváry	svár	k1gInPc1	svár
(	(	kIx(	(
<g/>
osobní	osobní	k2eAgNnSc1d1	osobní
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
panovalo	panovat	k5eAaImAgNnS	panovat
především	především	k6eAd1	především
mezi	mezi	k7c7	mezi
Janem	Jan	k1gMnSc7	Jan
z	z	k7c2	z
Giskaly	Giskal	k1gMnPc4	Giskal
a	a	k8xC	a
Josefem	Josef	k1gMnSc7	Josef
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
se	se	k3xPyFc4	se
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
posádkou	posádka	k1gFnSc7	posádka
uchýlil	uchýlit	k5eAaPmAgMnS	uchýlit
do	do	k7c2	do
pevnosti	pevnost	k1gFnSc2	pevnost
Iótapata	Iótapata	k1gFnSc1	Iótapata
<g/>
,	,	kIx,	,
při	při	k7c6	při
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
obléhání	obléhání	k1gNnSc1	obléhání
málem	málem	k6eAd1	málem
Vespasianus	Vespasianus	k1gInSc1	Vespasianus
přišel	přijít	k5eAaPmAgInS	přijít
o	o	k7c4	o
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
beznadějné	beznadějný	k2eAgFnSc6d1	beznadějná
situaci	situace	k1gFnSc6	situace
padlo	padnout	k5eAaPmAgNnS	padnout
mezi	mezi	k7c7	mezi
obránci	obránce	k1gMnPc7	obránce
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
-	-	kIx~	-
spáchat	spáchat	k5eAaPmF	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
poslední	poslední	k2eAgFnSc1d1	poslední
ji	on	k3xPp3gFnSc4	on
měl	mít	k5eAaImAgInS	mít
spáchat	spáchat	k5eAaPmF	spáchat
právě	právě	k9	právě
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
tak	tak	k9	tak
ale	ale	k9	ale
neučinil	učinit	k5eNaPmAgMnS	učinit
a	a	k8xC	a
vzdal	vzdát	k5eAaPmAgMnS	vzdát
se	se	k3xPyFc4	se
Římanům	Říman	k1gMnPc3	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
67	[number]	k4	67
tak	tak	k9	tak
byla	být	k5eAaImAgFnS	být
celá	celý	k2eAgFnSc1d1	celá
Galilea	Galilea	k1gFnSc1	Galilea
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
Římanů	Říman	k1gMnPc2	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
poslední	poslední	k2eAgMnSc1d1	poslední
padlo	padnout	k5eAaImAgNnS	padnout
rodné	rodný	k2eAgNnSc4d1	rodné
město	město	k1gNnSc4	město
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Giskaly	Giskal	k1gMnPc4	Giskal
<g/>
,	,	kIx,	,
Guš	Guš	k1gMnPc4	Guš
Chalav	Chalav	k1gFnSc4	Chalav
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgMnSc1d1	samotný
Josef	Josef	k1gMnSc1	Josef
se	se	k3xPyFc4	se
zachránil	zachránit	k5eAaPmAgMnS	zachránit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Vespasianovi	Vespasian	k1gMnSc3	Vespasian
prorokoval	prorokovat	k5eAaImAgMnS	prorokovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
císařem	císař	k1gMnSc7	císař
<g/>
.	.	kIx.	.
</s>
<s>
Vespasiana	Vespasian	k1gMnSc2	Vespasian
toto	tento	k3xDgNnSc1	tento
zaujalo	zaujmout	k5eAaPmAgNnS	zaujmout
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
jej	on	k3xPp3gMnSc4	on
Josef	Josef	k1gMnSc1	Josef
dále	daleko	k6eAd2	daleko
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
tažení	tažení	k1gNnSc6	tažení
jako	jako	k9	jako
zajatec	zajatec	k1gMnSc1	zajatec
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Vespasianus	Vespasianus	k1gInSc1	Vespasianus
roku	rok	k1gInSc2	rok
69	[number]	k4	69
skutečně	skutečně	k6eAd1	skutečně
císařem	císař	k1gMnSc7	císař
stal	stát	k5eAaPmAgMnS	stát
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Josef	Josef	k1gMnSc1	Josef
propuštěn	propustit	k5eAaPmNgMnS	propustit
a	a	k8xC	a
přijal	přijmout	k5eAaPmAgMnS	přijmout
jméno	jméno	k1gNnSc4	jméno
Josephus	Josephus	k1gMnSc1	Josephus
Flavius	Flavius	k1gMnSc1	Flavius
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
očitým	očitý	k2eAgMnSc7d1	očitý
svědkem	svědek	k1gMnSc7	svědek
Vespasianova	Vespasianův	k2eAgNnSc2d1	Vespasianovo
tažení	tažení	k1gNnSc2	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
předchozí	předchozí	k2eAgFnPc4d1	předchozí
události	událost	k1gFnPc4	událost
i	i	k8xC	i
celou	celý	k2eAgFnSc4d1	celá
válku	válka	k1gFnSc4	válka
popsal	popsat	k5eAaPmAgMnS	popsat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Válka	válka	k1gFnSc1	válka
židovská	židovská	k1gFnSc1	židovská
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
však	však	k9	však
nutno	nutno	k6eAd1	nutno
brát	brát	k5eAaImF	brát
s	s	k7c7	s
jistou	jistý	k2eAgFnSc7d1	jistá
rezervou	rezerva	k1gFnSc7	rezerva
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
o	o	k7c4	o
sebeobhajobu	sebeobhajoba	k1gFnSc4	sebeobhajoba
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
Josephus	Josephus	k1gMnSc1	Josephus
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
změnu	změna	k1gFnSc4	změna
svého	svůj	k3xOyFgInSc2	svůj
postoje	postoj	k1gInSc2	postoj
k	k	k7c3	k
Římanům	Říman	k1gMnPc3	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Josephus	Josephus	k1gMnSc1	Josephus
například	například	k6eAd1	například
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
autobiografii	autobiografie	k1gFnSc6	autobiografie
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
Galileje	Galilea	k1gFnSc2	Galilea
přišel	přijít	k5eAaPmAgMnS	přijít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
zklidnil	zklidnit	k5eAaPmAgMnS	zklidnit
a	a	k8xC	a
zachoval	zachovat	k5eAaPmAgMnS	zachovat
ji	on	k3xPp3gFnSc4	on
loajální	loajální	k2eAgFnSc4d1	loajální
vůči	vůči	k7c3	vůči
Římanům	Říman	k1gMnPc3	Říman
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jen	jen	k9	jen
část	část	k1gFnSc1	část
Galilejců	Galilejec	k1gMnPc2	Galilejec
podporovala	podporovat	k5eAaImAgFnS	podporovat
vzpouru	vzpoura	k1gFnSc4	vzpoura
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
však	však	k9	však
překroucení	překroucení	k1gNnSc1	překroucení
faktů	fakt	k1gInPc2	fakt
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
naopak	naopak	k6eAd1	naopak
téměř	téměř	k6eAd1	téměř
celá	celý	k2eAgFnSc1d1	celá
Galilea	Galilea	k1gFnSc1	Galilea
válku	válka	k1gFnSc4	válka
podporovala	podporovat	k5eAaImAgFnS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
Josephus	Josephus	k1gInSc1	Josephus
zavedl	zavést	k5eAaPmAgInS	zavést
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
židovských	židovský	k2eAgFnPc6d1	židovská
vojenských	vojenský	k2eAgFnPc6d1	vojenská
jednotkách	jednotka	k1gFnPc6	jednotka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
zahrnovaly	zahrnovat	k5eAaImAgFnP	zahrnovat
100	[number]	k4	100
000	[number]	k4	000
pěšáků	pěšák	k1gMnPc2	pěšák
a	a	k8xC	a
5	[number]	k4	5
000	[number]	k4	000
jezdců	jezdec	k1gMnPc2	jezdec
<g/>
,	,	kIx,	,
přísnou	přísný	k2eAgFnSc4d1	přísná
disciplínu	disciplína	k1gFnSc4	disciplína
a	a	k8xC	a
opevnil	opevnit	k5eAaPmAgInS	opevnit
galilejská	galilejský	k2eAgNnPc4d1	Galilejské
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zpacifikování	zpacifikování	k1gNnSc6	zpacifikování
Galileje	Galilea	k1gFnSc2	Galilea
postupoval	postupovat	k5eAaImAgInS	postupovat
Vespasianus	Vespasianus	k1gInSc1	Vespasianus
na	na	k7c4	na
jih	jih	k1gInSc4	jih
a	a	k8xC	a
dobyl	dobýt	k5eAaPmAgMnS	dobýt
okolí	okolí	k1gNnSc4	okolí
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
už	už	k6eAd1	už
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
městě	město	k1gNnSc6	město
zuřila	zuřit	k5eAaImAgFnS	zuřit
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
–	–	k?	–
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
stály	stát	k5eAaImAgFnP	stát
frakce	frakce	k1gFnPc1	frakce
jednotlivých	jednotlivý	k2eAgMnPc2d1	jednotlivý
vůdců	vůdce	k1gMnPc2	vůdce
povstání	povstání	k1gNnSc2	povstání
-	-	kIx~	-
Menachema	Menachem	k1gMnSc4	Menachem
Galilejského	Galilejský	k1gMnSc4	Galilejský
<g/>
,	,	kIx,	,
Jana	Jan	k1gMnSc4	Jan
z	z	k7c2	z
Giskaly	Giskal	k1gMnPc7	Giskal
a	a	k8xC	a
Šim	Šim	k1gFnPc7	Šim
<g/>
'	'	kIx"	'
<g/>
ona	onen	k3xDgFnSc1	onen
bar	bar	k1gInSc1	bar
Giory	Gior	k1gInPc1	Gior
<g/>
.	.	kIx.	.
</s>
<s>
Postup	postup	k1gInSc1	postup
Římanů	Říman	k1gMnPc2	Říman
byl	být	k5eAaImAgInS	být
nakrátko	nakrátko	k6eAd1	nakrátko
zdržen	zdržen	k2eAgInSc1d1	zdržen
událostmi	událost	k1gFnPc7	událost
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
68	[number]	k4	68
spáchal	spáchat	k5eAaPmAgMnS	spáchat
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
Nero	Nero	k1gMnSc1	Nero
sebevraždu	sebevražda	k1gFnSc4	sebevražda
a	a	k8xC	a
po	po	k7c6	po
rychlém	rychlý	k2eAgNnSc6d1	rychlé
vystřídání	vystřídání	k1gNnSc6	vystřídání
jeho	on	k3xPp3gInSc2	on
několika	několik	k4yIc7	několik
nástupců	nástupce	k1gMnPc2	nástupce
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
69	[number]	k4	69
císařem	císař	k1gMnSc7	císař
provolán	provolat	k5eAaPmNgMnS	provolat
právě	právě	k6eAd1	právě
Vespasianus	Vespasianus	k1gMnSc1	Vespasianus
<g/>
.	.	kIx.	.
</s>
<s>
Opustil	opustit	k5eAaPmAgMnS	opustit
tedy	tedy	k9	tedy
Judeu	Judea	k1gFnSc4	Judea
a	a	k8xC	a
velení	velení	k1gNnSc4	velení
předal	předat	k5eAaPmAgMnS	předat
svému	svůj	k3xOyFgMnSc3	svůj
synovi	syn	k1gMnSc3	syn
Titovi	Tita	k1gMnSc3	Tita
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
obklíčit	obklíčit	k5eAaPmF	obklíčit
a	a	k8xC	a
nechat	nechat	k5eAaPmF	nechat
vyhladovět	vyhladovět	k5eAaPmF	vyhladovět
<g/>
.	.	kIx.	.
</s>
<s>
Vůdcové	vůdce	k1gMnPc1	vůdce
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
židovských	židovský	k2eAgFnPc2d1	židovská
frakcí	frakce	k1gFnPc2	frakce
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
krizové	krizový	k2eAgFnSc6d1	krizová
situaci	situace	k1gFnSc6	situace
spatřili	spatřit	k5eAaPmAgMnP	spatřit
nutnost	nutnost	k1gFnSc4	nutnost
se	se	k3xPyFc4	se
sjednotit	sjednotit	k5eAaPmF	sjednotit
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
kvůli	kvůli	k7c3	kvůli
vyhladovění	vyhladovění	k1gNnSc3	vyhladovění
byla	být	k5eAaImAgFnS	být
obrana	obrana	k1gFnSc1	obrana
města	město	k1gNnSc2	město
velmi	velmi	k6eAd1	velmi
obtížná	obtížný	k2eAgFnSc1d1	obtížná
<g/>
.	.	kIx.	.
</s>
<s>
Římské	římský	k2eAgNnSc1d1	římské
vojsko	vojsko	k1gNnSc1	vojsko
dobylo	dobýt	k5eAaPmAgNnS	dobýt
postupně	postupně	k6eAd1	postupně
Nové	Nové	k2eAgNnSc4d1	Nové
město	město	k1gNnSc4	město
a	a	k8xC	a
přiblížili	přiblížit	k5eAaPmAgMnP	přiblížit
se	se	k3xPyFc4	se
k	k	k7c3	k
hlavnímu	hlavní	k2eAgInSc3d1	hlavní
opěrnému	opěrný	k2eAgInSc3d1	opěrný
bodu	bod	k1gInSc3	bod
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Antoniově	Antoniův	k2eAgFnSc3d1	Antoniova
pevnosti	pevnost	k1gFnSc3	pevnost
a	a	k8xC	a
Chrámové	chrámový	k2eAgFnSc3d1	chrámová
hoře	hora	k1gFnSc3	hora
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
byl	být	k5eAaImAgInS	být
9	[number]	k4	9
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
měsíce	měsíc	k1gInSc2	měsíc
avu	avu	k?	avu
roku	rok	k1gInSc2	rok
70	[number]	k4	70
vypálen	vypálen	k2eAgInSc1d1	vypálen
Chrám	chrám	k1gInSc1	chrám
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
města	město	k1gNnSc2	město
padl	padnout	k5eAaPmAgInS	padnout
během	během	k7c2	během
následujícího	následující	k2eAgInSc2d1	následující
měsíce	měsíc	k1gInSc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Titus	Titus	k1gInSc1	Titus
poručil	poručit	k5eAaPmAgInS	poručit
většinu	většina	k1gFnSc4	většina
města	město	k1gNnSc2	město
srovnat	srovnat	k5eAaPmF	srovnat
se	se	k3xPyFc4	se
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
tří	tři	k4xCgFnPc2	tři
věží	věž	k1gFnPc2	věž
nacházejících	nacházející	k2eAgFnPc2d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
hradbě	hradba	k1gFnSc6	hradba
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
tzv.	tzv.	kA	tzv.
Davidova	Davidův	k2eAgFnSc1d1	Davidova
věž	věž	k1gFnSc1	věž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
zničení	zničení	k1gNnSc6	zničení
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
začali	začít	k5eAaPmAgMnP	začít
Židé	Žid	k1gMnPc1	Žid
přemýšlet	přemýšlet	k5eAaImF	přemýšlet
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
–	–	k?	–
až	až	k9	až
pozdě	pozdě	k6eAd1	pozdě
si	se	k3xPyFc3	se
uvědomili	uvědomit	k5eAaPmAgMnP	uvědomit
osudné	osudný	k2eAgInPc4d1	osudný
důsledky	důsledek	k1gInPc4	důsledek
své	svůj	k3xOyFgFnSc2	svůj
nejednoty	nejednota	k1gFnSc2	nejednota
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
úvah	úvaha	k1gFnPc2	úvaha
je	být	k5eAaImIp3nS	být
zachycena	zachytit	k5eAaPmNgFnS	zachytit
i	i	k9	i
v	v	k7c6	v
Midraši	Midraše	k1gFnSc6	Midraše
<g/>
:	:	kIx,	:
Po	po	k7c6	po
dobytí	dobytí	k1gNnSc6	dobytí
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
kladlo	klást	k5eAaImAgNnS	klást
Římanům	Říman	k1gMnPc3	Říman
odpor	odpor	k1gInSc4	odpor
již	již	k6eAd1	již
jen	jen	k9	jen
několik	několik	k4yIc4	několik
pevností	pevnost	k1gFnPc2	pevnost
–	–	k?	–
Héródeion	Héródeion	k1gInSc1	Héródeion
<g/>
,	,	kIx,	,
Macháirús	Macháirús	k1gInSc1	Macháirús
a	a	k8xC	a
Masada	Masada	k1gFnSc1	Masada
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
poslední	poslední	k2eAgFnSc1d1	poslední
vydržela	vydržet	k5eAaPmAgFnS	vydržet
Masada	Masada	k1gFnSc1	Masada
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
proto	proto	k6eAd1	proto
obklíčena	obklíčit	k5eAaPmNgFnS	obklíčit
římskou	římska	k1gFnSc7	římska
10	[number]	k4	10
<g/>
.	.	kIx.	.
legií	legie	k1gFnSc7	legie
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
generála	generál	k1gMnSc2	generál
Lucia	Lucius	k1gMnSc2	Lucius
Flavia	Flavius	k1gMnSc2	Flavius
Silvy	Silva	k1gFnSc2	Silva
<g/>
.	.	kIx.	.
</s>
<s>
Pevnost	pevnost	k1gFnSc1	pevnost
však	však	k9	však
byla	být	k5eAaImAgFnS	být
odolná	odolný	k2eAgFnSc1d1	odolná
proti	proti	k7c3	proti
přímému	přímý	k2eAgInSc3d1	přímý
útoku	útok	k1gInSc3	útok
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c4	o
odříznutí	odříznutí	k1gNnSc4	odříznutí
přístupových	přístupový	k2eAgFnPc2d1	přístupová
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
vyhladovění	vyhladovění	k1gNnPc2	vyhladovění
osob	osoba	k1gFnPc2	osoba
na	na	k7c6	na
Masadě	Masada	k1gFnSc6	Masada
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
velmi	velmi	k6eAd1	velmi
dobrým	dobrý	k2eAgFnPc3d1	dobrá
zásobám	zásoba	k1gFnPc3	zásoba
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
židovským	židovský	k2eAgMnPc3d1	židovský
povstalcům	povstalec	k1gMnPc3	povstalec
přečkat	přečkat	k5eAaPmF	přečkat
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Římští	římský	k2eAgMnPc1d1	římský
ženisté	ženista	k1gMnPc1	ženista
však	však	k9	však
postupně	postupně	k6eAd1	postupně
postavili	postavit	k5eAaPmAgMnP	postavit
rampu	rampa	k1gFnSc4	rampa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jim	on	k3xPp3gMnPc3	on
umožnila	umožnit	k5eAaPmAgFnS	umožnit
beranidlem	beranidlo	k1gNnSc7	beranidlo
prorazit	prorazit	k5eAaPmF	prorazit
hradbu	hradba	k1gFnSc4	hradba
pevnosti	pevnost	k1gFnSc2	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
73	[number]	k4	73
tak	tak	k6eAd1	tak
byli	být	k5eAaImAgMnP	být
Římané	Říman	k1gMnPc1	Říman
připraveni	připravit	k5eAaPmNgMnP	připravit
k	k	k7c3	k
závěrečnému	závěrečný	k2eAgInSc3d1	závěrečný
útoku	útok	k1gInSc3	útok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
před	před	k7c7	před
předpokládaným	předpokládaný	k2eAgInSc7d1	předpokládaný
útokem	útok	k1gInSc7	útok
promluvil	promluvit	k5eAaPmAgMnS	promluvit
vůdce	vůdce	k1gMnSc1	vůdce
zélótů	zélóta	k1gMnPc2	zélóta
a	a	k8xC	a
pevnosti	pevnost	k1gFnSc2	pevnost
El	Ela	k1gFnPc2	Ela
<g/>
'	'	kIx"	'
<g/>
azar	azar	k1gInSc1	azar
ben	ben	k?	ben
Ja	Ja	k?	Ja
<g/>
'	'	kIx"	'
<g/>
ir	ir	k?	ir
ke	k	k7c3	k
svým	svůj	k3xOyFgMnPc3	svůj
stoupencům	stoupenec	k1gMnPc3	stoupenec
a	a	k8xC	a
připomněl	připomnět	k5eAaPmAgMnS	připomnět
jim	on	k3xPp3gMnPc3	on
společné	společný	k2eAgNnSc1d1	společné
odhodlání	odhodlání	k1gNnSc1	odhodlání
nikdy	nikdy	k6eAd1	nikdy
nebýt	být	k5eNaImF	být
sluhy	sluha	k1gMnPc4	sluha
Římanů	Říman	k1gMnPc2	Říman
ani	ani	k8xC	ani
jiného	jiný	k2eAgMnSc4d1	jiný
Boha	bůh	k1gMnSc4	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
všichni	všechen	k3xTgMnPc1	všechen
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
pro	pro	k7c4	pro
hromadnou	hromadný	k2eAgFnSc4d1	hromadná
sebevraždu	sebevražda	k1gFnSc4	sebevražda
a	a	k8xC	a
vypálení	vypálení	k1gNnSc4	vypálení
všech	všecek	k3xTgFnPc2	všecek
staveb	stavba	k1gFnPc2	stavba
na	na	k7c6	na
Masadě	Masada	k1gFnSc6	Masada
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
Římané	Říman	k1gMnPc1	Říman
vtrhli	vtrhnout	k5eAaPmAgMnP	vtrhnout
v	v	k7c6	v
plné	plný	k2eAgFnSc6d1	plná
zbroji	zbroj	k1gFnSc6	zbroj
do	do	k7c2	do
pevnosti	pevnost	k1gFnSc2	pevnost
<g/>
,	,	kIx,	,
objevili	objevit	k5eAaPmAgMnP	objevit
na	na	k7c6	na
tisíc	tisíc	k4xCgInPc2	tisíc
mrtvých	mrtvý	k2eAgMnPc2d1	mrtvý
obránců	obránce	k1gMnPc2	obránce
pevnosti	pevnost	k1gFnSc2	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Římané	Říman	k1gMnPc1	Říman
dozvěděli	dozvědět	k5eAaPmAgMnP	dozvědět
díky	díky	k7c3	díky
dvěma	dva	k4xCgFnPc3	dva
ženám	žena	k1gFnPc3	žena
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
před	před	k7c7	před
hromadnou	hromadný	k2eAgFnSc7d1	hromadná
sebevraždou	sebevražda	k1gFnSc7	sebevražda
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
pěti	pět	k4xCc7	pět
dětmi	dítě	k1gFnPc7	dítě
ukryly	ukrýt	k5eAaPmAgFnP	ukrýt
v	v	k7c6	v
nádrži	nádrž	k1gFnSc6	nádrž
na	na	k7c4	na
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
Titus	Titus	k1gMnSc1	Titus
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
zajatci	zajatec	k1gMnPc1	zajatec
vedeni	veden	k2eAgMnPc1d1	veden
v	v	k7c6	v
triumfálním	triumfální	k2eAgInSc6d1	triumfální
průvodu	průvod	k1gInSc6	průvod
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
předměty	předmět	k1gInPc1	předmět
z	z	k7c2	z
Chrámu	chrám	k1gInSc2	chrám
a	a	k8xC	a
Menora	Menora	k1gFnSc1	Menora
(	(	kIx(	(
<g/>
svícen	svícen	k1gInSc1	svícen
z	z	k7c2	z
jeruzalémského	jeruzalémský	k2eAgInSc2d1	jeruzalémský
chrámu	chrám	k1gInSc2	chrám
<g/>
)	)	kIx)	)
–	–	k?	–
tato	tento	k3xDgFnSc1	tento
událost	událost	k1gFnSc1	událost
je	být	k5eAaImIp3nS	být
zobrazena	zobrazit	k5eAaPmNgFnS	zobrazit
i	i	k9	i
na	na	k7c6	na
Titově	Titův	k2eAgInSc6d1	Titův
triumfálním	triumfální	k2eAgInSc6d1	triumfální
oblouku	oblouk	k1gInSc6	oblouk
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nechal	nechat	k5eAaPmAgInS	nechat
postavit	postavit	k5eAaPmF	postavit
jeho	jeho	k3xOp3gMnSc1	jeho
bratr	bratr	k1gMnSc1	bratr
Domitianus	Domitianus	k1gMnSc1	Domitianus
<g/>
.	.	kIx.	.
</s>
<s>
Zajatci	zajatec	k1gMnPc1	zajatec
pak	pak	k6eAd1	pak
byli	být	k5eAaImAgMnP	být
předhozeni	předhozen	k2eAgMnPc1d1	předhozen
šelmám	šelma	k1gFnPc3	šelma
v	v	k7c6	v
cirku	cirk	k1gInSc6	cirk
<g/>
,	,	kIx,	,
Šimon	Šimon	k1gMnSc1	Šimon
bar	bar	k1gInSc1	bar
Giora	Gior	k1gInSc2	Gior
popraven	popraven	k2eAgMnSc1d1	popraven
jako	jako	k8xS	jako
vůdce	vůdce	k1gMnSc1	vůdce
povstání	povstání	k1gNnSc2	povstání
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Giskaly	Giskal	k1gMnPc4	Giskal
strávil	strávit	k5eAaPmAgInS	strávit
zbytek	zbytek	k1gInSc1	zbytek
života	život	k1gInSc2	život
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
<g/>
.	.	kIx.	.
</s>
<s>
Důsledky	důsledek	k1gInPc4	důsledek
První	první	k4xOgFnSc2	první
židovské	židovský	k2eAgFnSc2d1	židovská
války	válka	k1gFnSc2	válka
byly	být	k5eAaImAgFnP	být
pro	pro	k7c4	pro
Židy	Žid	k1gMnPc4	Žid
drastické	drastický	k2eAgNnSc1d1	drastické
<g/>
.	.	kIx.	.
</s>
<s>
Jeruzalém	Jeruzalém	k1gInSc1	Jeruzalém
zůstal	zůstat	k5eAaPmAgInS	zůstat
v	v	k7c6	v
troskách	troska	k1gFnPc6	troska
<g/>
,	,	kIx,	,
mnohé	mnohý	k2eAgFnPc1d1	mnohá
osady	osada	k1gFnPc1	osada
v	v	k7c6	v
Judeji	Judea	k1gFnSc6	Judea
se	se	k3xPyFc4	se
vylidnily	vylidnit	k5eAaPmAgFnP	vylidnit
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
Židů	Žid	k1gMnPc2	Žid
odešla	odejít	k5eAaPmAgFnS	odejít
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
značně	značně	k6eAd1	značně
zchudlo	zchudnout	k5eAaPmAgNnS	zchudnout
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
horší	horšit	k5eAaImIp3nS	horšit
ranou	rána	k1gFnSc7	rána
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
zničení	zničení	k1gNnSc1	zničení
Chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
centrem	centrum	k1gNnSc7	centrum
židovského	židovský	k2eAgNnSc2d1	Židovské
náboženství	náboženství	k1gNnSc2	náboženství
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
chrámová	chrámový	k2eAgFnSc1d1	chrámová
daň	daň	k1gFnSc1	daň
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
odváděna	odvádět	k5eAaImNgFnS	odvádět
místo	místo	k6eAd1	místo
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
do	do	k7c2	do
Jupiterova	Jupiterův	k2eAgInSc2d1	Jupiterův
chrámu	chrám	k1gInSc2	chrám
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
Židy	Žid	k1gMnPc4	Žid
velmi	velmi	k6eAd1	velmi
ponižující	ponižující	k2eAgMnSc1d1	ponižující
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
to	ten	k3xDgNnSc1	ten
de	de	k?	de
facto	facto	k1gNnSc1	facto
znamenalo	znamenat	k5eAaImAgNnS	znamenat
modloslužbu	modloslužba	k1gFnSc4	modloslužba
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
nebyla	být	k5eNaImAgFnS	být
naděje	naděje	k1gFnSc1	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Chrám	chrám	k1gInSc1	chrám
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgFnP	začít
židovské	židovský	k2eAgFnPc4d1	židovská
autority	autorita	k1gFnPc4	autorita
přemýšlet	přemýšlet	k5eAaImF	přemýšlet
nad	nad	k7c7	nad
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
zachovat	zachovat	k5eAaPmF	zachovat
židovské	židovský	k2eAgNnSc4d1	Židovské
náboženství	náboženství	k1gNnSc4	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Vůdčí	vůdčí	k2eAgFnSc7d1	vůdčí
osobností	osobnost	k1gFnSc7	osobnost
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
rabi	rabi	k1gMnSc1	rabi
Jochanan	Jochanan	k1gMnSc1	Jochanan
ben	ben	k?	ben
Zakaj	Zakaj	k1gMnSc1	Zakaj
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
při	při	k7c6	při
obléhání	obléhání	k1gNnSc6	obléhání
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
zavřít	zavřít	k5eAaPmF	zavřít
do	do	k7c2	do
rakve	rakev	k1gFnSc2	rakev
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
se	se	k3xPyFc4	se
vynést	vynést	k5eAaPmF	vynést
z	z	k7c2	z
města	město	k1gNnSc2	město
ven	ven	k6eAd1	ven
<g/>
.	.	kIx.	.
</s>
<s>
Ihned	ihned	k6eAd1	ihned
poté	poté	k6eAd1	poté
požádal	požádat	k5eAaPmAgMnS	požádat
Římany	Říman	k1gMnPc4	Říman
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
založit	založit	k5eAaPmF	založit
malou	malý	k2eAgFnSc4d1	malá
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Javne	Javn	k1gInSc5	Javn
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
žádosti	žádost	k1gFnSc3	žádost
bylo	být	k5eAaImAgNnS	být
vyhověno	vyhověn	k2eAgNnSc1d1	vyhověno
a	a	k8xC	a
učenci	učenec	k1gMnPc7	učenec
v	v	k7c6	v
Javne	Javn	k1gInSc5	Javn
začali	začít	k5eAaPmAgMnP	začít
židovství	židovství	k1gNnSc4	židovství
přizpůsobovat	přizpůsobovat	k5eAaImF	přizpůsobovat
novým	nový	k2eAgFnPc3d1	nová
podmínkám	podmínka	k1gFnPc3	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zničení	zničení	k1gNnSc6	zničení
Chrámu	chrám	k1gInSc2	chrám
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
přenést	přenést	k5eAaPmF	přenést
posvátnost	posvátnost	k1gFnSc4	posvátnost
bohoslužby	bohoslužba	k1gFnSc2	bohoslužba
na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
oblasti	oblast	k1gFnPc4	oblast
každodenního	každodenní	k2eAgInSc2d1	každodenní
života	život	k1gInSc2	život
–	–	k?	–
každý	každý	k3xTgInSc4	každý
dům	dům	k1gInSc4	dům
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
stát	stát	k5eAaImF	stát
Chrámem	chrám	k1gInSc7	chrám
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc4	každý
stůl	stůl	k1gInSc4	stůl
oltářem	oltář	k1gInSc7	oltář
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc4	každý
člověk	člověk	k1gMnSc1	člověk
knězem	kněz	k1gMnSc7	kněz
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
židovská	židovský	k2eAgFnSc1d1	židovská
válka	válka	k1gFnSc1	válka
má	mít	k5eAaImIp3nS	mít
tak	tak	k9	tak
pro	pro	k7c4	pro
dějiny	dějiny	k1gFnPc4	dějiny
Židů	Žid	k1gMnPc2	Žid
zásadní	zásadní	k2eAgInSc4d1	zásadní
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
byl	být	k5eAaImAgInS	být
zničen	zničit	k5eAaPmNgInS	zničit
Chrám	chrám	k1gInSc1	chrám
(	(	kIx(	(
<g/>
na	na	k7c6	na
kterém	který	k3yIgNnSc6	který
byli	být	k5eAaImAgMnP	být
závislí	závislý	k2eAgMnPc1d1	závislý
saduceové	saduceus	k1gMnPc1	saduceus
<g/>
)	)	kIx)	)
i	i	k9	i
esejská	esejský	k2eAgFnSc1d1	esejská
komunita	komunita	k1gFnSc1	komunita
v	v	k7c6	v
Kumránu	Kumrán	k1gInSc6	Kumrán
<g/>
,	,	kIx,	,
určili	určit	k5eAaPmAgMnP	určit
další	další	k2eAgInSc4d1	další
směr	směr	k1gInSc4	směr
judaismu	judaismus	k1gInSc2	judaismus
farizeové	farizeus	k1gMnPc1	farizeus
–	–	k?	–
tato	tento	k3xDgFnSc1	tento
forma	forma	k1gFnSc1	forma
judaismu	judaismus	k1gInSc2	judaismus
bývá	bývat	k5eAaImIp3nS	bývat
nazývána	nazývat	k5eAaImNgFnS	nazývat
jako	jako	k8xS	jako
rabínský	rabínský	k2eAgInSc4d1	rabínský
judaismus	judaismus	k1gInSc4	judaismus
a	a	k8xC	a
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
centru	centrum	k1gNnSc6	centrum
stojí	stát	k5eAaImIp3nS	stát
Tóra	tóra	k1gFnSc1	tóra
a	a	k8xC	a
Talmud	talmud	k1gInSc1	talmud
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
také	také	k9	také
způsobila	způsobit	k5eAaPmAgFnS	způsobit
úbytek	úbytek	k1gInSc4	úbytek
a	a	k8xC	a
odliv	odliv	k1gInSc1	odliv
židovských	židovský	k2eAgMnPc2d1	židovský
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
definitivně	definitivně	k6eAd1	definitivně
svtrzen	svtrzen	k2eAgInSc1d1	svtrzen
druhým	druhý	k4xOgNnSc7	druhý
neúspěšným	úspěšný	k2eNgNnSc7d1	neúspěšné
protiřímským	protiřímský	k2eAgNnSc7d1	protiřímské
povstáním	povstání	k1gNnSc7	povstání
v	v	k7c6	v
letech	let	k1gInPc6	let
132	[number]	k4	132
<g/>
–	–	k?	–
<g/>
135	[number]	k4	135
(	(	kIx(	(
<g/>
Bar	bar	k1gInSc4	bar
Kochbovo	Kochbův	k2eAgNnSc4d1	Kochbovo
povstání	povstání	k1gNnSc4	povstání
či	či	k8xC	či
Druhá	druhý	k4xOgFnSc1	druhý
židovská	židovský	k2eAgFnSc1d1	židovská
válka	válka	k1gFnSc1	válka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
křesťany	křesťan	k1gMnPc4	křesťan
významnou	významný	k2eAgFnSc7d1	významná
dějinnou	dějinný	k2eAgFnSc7d1	dějinná
událostí	událost	k1gFnSc7	událost
byl	být	k5eAaImAgInS	být
zánik	zánik	k1gInSc1	zánik
malé	malý	k2eAgFnSc2d1	malá
komunity	komunita	k1gFnSc2	komunita
původních	původní	k2eAgMnPc2d1	původní
Ježíšových	Ježíšův	k2eAgMnPc2d1	Ježíšův
následovníků	následovník	k1gMnPc2	následovník
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
křesťanském	křesťanský	k2eAgNnSc6d1	křesťanské
společenství	společenství	k1gNnSc6	společenství
tak	tak	k8xS	tak
role	role	k1gFnSc1	role
židokřesťanů	židokřesťan	k1gMnPc2	židokřesťan
ustupila	ustupit	k5eAaImAgFnS	ustupit
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
a	a	k8xC	a
převahu	převaha	k1gFnSc4	převaha
získali	získat	k5eAaPmAgMnP	získat
pohanokřesťané	pohanokřesťan	k1gMnPc1	pohanokřesťan
(	(	kIx(	(
<g/>
křesťané	křesťan	k1gMnPc1	křesťan
z	z	k7c2	z
římských	římský	k2eAgMnPc2d1	římský
a	a	k8xC	a
řeckých	řecký	k2eAgMnPc2d1	řecký
pohanů	pohan	k1gMnPc2	pohan
<g/>
)	)	kIx)	)
–	–	k?	–
tento	tento	k3xDgInSc4	tento
proud	proud	k1gInSc4	proud
zastával	zastávat	k5eAaImAgMnS	zastávat
Pavel	Pavel	k1gMnSc1	Pavel
z	z	k7c2	z
Tarsu	Tars	k1gInSc2	Tars
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
definitivnímu	definitivní	k2eAgNnSc3d1	definitivní
odloučení	odloučení	k1gNnSc3	odloučení
židů	žid	k1gMnPc2	žid
a	a	k8xC	a
křesťanů	křesťan	k1gMnPc2	křesťan
pak	pak	k6eAd1	pak
vedlo	vést	k5eAaImAgNnS	vést
ustavení	ustavení	k1gNnSc4	ustavení
rabínského	rabínský	k2eAgInSc2d1	rabínský
judaismu	judaismus	k1gInSc2	judaismus
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
křesťanství	křesťanství	k1gNnSc4	křesťanství
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
