<s>
Jako	jako	k9	jako
první	první	k4xOgFnSc1	první
židovská	židovský	k2eAgFnSc1d1	židovská
válka	válka	k1gFnSc1	válka
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
židovské	židovský	k2eAgNnSc4d1	Židovské
celonárodní	celonárodní	k2eAgNnSc4d1	celonárodní
povstání	povstání	k1gNnSc4	povstání
proti	proti	k7c3	proti
Římu	Řím	k1gInSc3	Řím
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc3	který
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
v	v	k7c6	v
Jeruzalémě	Jeruzalém	k1gInSc6	Jeruzalém
v	v	k7c6	v
roce	rok	k1gInSc6	rok
66	[number]	k4	66
<g/>
.	.	kIx.	.
</s>
