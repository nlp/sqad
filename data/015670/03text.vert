<s>
Boeing	boeing	k1gInSc1
P-8	P-8	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
</s>
<s>
P-8	P-8	k4
Poseidon	Poseidon	k1gMnSc1
Určení	určení	k1gNnSc1
</s>
<s>
protiponorkový	protiponorkový	k2eAgMnSc1d1
a	a	k8xC
protilodní	protilodní	k2eAgNnSc1d1
letounnámořní	letounnámořní	k2eAgNnSc1d1
hlídkové	hlídkový	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
Výrobce	výrobce	k1gMnSc1
</s>
<s>
Boeing	boeing	k1gInSc4
Defense	defense	k1gFnSc2
<g/>
,	,	kIx,
Space	Space	k1gFnSc2
&	&	k?
Security	Securita	k1gFnSc2
První	první	k4xOgFnSc1
let	léto	k1gNnPc2
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2009	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Zařazeno	zařadit	k5eAaPmNgNnS
</s>
<s>
listopad	listopad	k1gInSc4
2013	#num#	k4
Charakter	charakter	k1gInSc1
</s>
<s>
Ve	v	k7c6
službě	služba	k1gFnSc6
Uživatel	uživatel	k1gMnSc1
</s>
<s>
US	US	kA
NavyIndické	NavyIndický	k2eAgFnPc1d1
námořnictvoRoyal	námořnictvoRoyal	k1gInSc4
Australian	Australiana	k1gFnPc2
Air	Air	k1gFnSc2
Force	force	k1gFnSc1
Výroba	výroba	k1gFnSc1
</s>
<s>
2009	#num#	k4
<g/>
–	–	k?
<g/>
současnost	současnost	k1gFnSc4
Vyrobeno	vyrobit	k5eAaPmNgNnS
kusů	kus	k1gInPc2
</s>
<s>
122	#num#	k4
(	(	kIx(
<g/>
květen	květen	k1gInSc4
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Vyvinuto	vyvinout	k5eAaPmNgNnS
z	z	k7c2
typu	typ	k1gInSc2
</s>
<s>
Boeing	boeing	k1gInSc4
737	#num#	k4
Next	Next	k2eAgInSc4d1
Generation	Generation	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Boeing	boeing	k1gInSc1
P-8	P-8	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
nazývaný	nazývaný	k2eAgInSc1d1
Multimission	Multimission	k1gInSc1
Maritime	Maritim	k1gInSc5
Aircraft	Aircraftum	k1gNnPc2
nebo	nebo	k8xC
MMA	MMA	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
námořní	námořní	k2eAgInSc1d1
hlídkový	hlídkový	k2eAgInSc1d1
letoun	letoun	k1gInSc1
námořnictva	námořnictvo	k1gNnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
určen	určit	k5eAaPmNgInS
k	k	k7c3
vedení	vedení	k1gNnSc3
protiponorkového	protiponorkový	k2eAgInSc2d1
boje	boj	k1gInSc2
<g/>
,	,	kIx,
boje	boj	k1gInSc2
proti	proti	k7c3
hladinovým	hladinový	k2eAgInPc3d1
cílům	cíl	k1gInPc3
a	a	k8xC
provádění	provádění	k1gNnSc3
elektronického	elektronický	k2eAgInSc2d1
průzkumu	průzkum	k1gInSc2
(	(	kIx(
<g/>
ELINT	ELINT	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letoun	letoun	k1gMnSc1
ponese	ponést	k5eAaPmIp3nS,k5eAaImIp3nS
torpéda	torpédo	k1gNnSc2
Mk	Mk	k1gFnSc2
<g/>
.54	.54	k4
<g/>
,	,	kIx,
hlubinné	hlubinný	k2eAgFnSc2d1
pumy	puma	k1gFnSc2
<g/>
,	,	kIx,
protilodní	protilodní	k2eAgFnSc2d1
střely	střela	k1gFnSc2
AGM-84	AGM-84	k1gMnSc1
Harpoon	Harpoon	k1gMnSc1
<g/>
,	,	kIx,
AGM-84K	AGM-84K	k1gMnSc1
SLAM-ER	SLAM-ER	k1gMnSc1
<g/>
,	,	kIx,
protizemní	protizemnit	k5eAaPmIp3nS
řízené	řízený	k2eAgInPc4d1
střely	střel	k1gInPc4
AGM-64	AGM-64	k1gFnSc2
Maverick	Mavericka	k1gFnPc2
<g/>
,	,	kIx,
protiletdlové	protiletdlový	k2eAgFnSc2d1
řízené	řízený	k2eAgFnSc2d1
střely	střela	k1gFnSc2
AIM-9	AIM-9	k1gMnSc1
Sidewinder	Sidewinder	k1gMnSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
upravené	upravený	k2eAgFnPc1d1
AIM-120	AIM-120	k1gFnPc1
AMRAAM	AMRAAM	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
schopen	schopen	k2eAgInSc1d1
vypouštět	vypouštět	k5eAaImF
a	a	k8xC
monitorovat	monitorovat	k5eAaImF
sonarové	sonarový	k2eAgFnPc4d1
bóje	bóje	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
navržen	navrhnout	k5eAaPmNgInS
ke	k	k7c3
spolupráci	spolupráce	k1gFnSc3
s	s	k7c7
bezpilotními	bezpilotní	k2eAgInPc7d1
letouny	letoun	k1gInPc7
v	v	k7c6
rámci	rámec	k1gInSc6
systému	systém	k1gInSc2
„	„	k?
<g/>
Broad	Broad	k1gInSc1
Area	area	k1gFnSc1
Maritime	Maritim	k1gInSc5
Surveillance	Surveillanec	k1gMnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
pro	pro	k7c4
neustálé	neustálý	k2eAgNnSc4d1
sledování	sledování	k1gNnSc4
námořního	námořní	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
P-8	P-8	k1gMnSc1
je	být	k5eAaImIp3nS
vyvíjen	vyvíjen	k2eAgMnSc1d1
divizí	divize	k1gFnSc7
společnosti	společnost	k1gFnSc2
Boeing	boeing	k1gInSc1
(	(	kIx(
<g/>
Boeing	boeing	k1gInSc1
Defense	defense	k1gFnSc2
<g/>
,	,	kIx,
Space	Space	k1gFnSc2
&	&	k?
Security	Securita	k1gFnSc2
<g/>
)	)	kIx)
z	z	k7c2
civilního	civilní	k2eAgInSc2d1
letounu	letoun	k1gInSc2
Boeing	boeing	k1gInSc1
737	#num#	k4
<g/>
-	-	kIx~
<g/>
800	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahraničními	zahraniční	k2eAgMnPc7d1
uživateli	uživatel	k1gMnPc7
typu	typ	k1gInSc2
jsou	být	k5eAaImIp3nP
Austrálie	Austrálie	k1gFnSc1
<g/>
,	,	kIx,
Indie	Indie	k1gFnSc1
<g/>
,	,	kIx,
Norsko	Norsko	k1gNnSc1
<g/>
,	,	kIx,
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
a	a	k8xC
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
května	květen	k1gInSc2
2020	#num#	k4
bylo	být	k5eAaImAgNnS
zákazníkům	zákazník	k1gMnPc3
předáno	předat	k5eAaPmNgNnS
122	#num#	k4
těchto	tento	k3xDgInPc2
letounů	letoun	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Americké	americký	k2eAgFnPc1d1
a	a	k8xC
australské	australský	k2eAgFnPc1d1
P-8	P-8	k1gFnPc1
budou	být	k5eAaImBp3nP
ve	v	k7c6
službě	služba	k1gFnSc6
spolupracovat	spolupracovat	k5eAaImF
s	s	k7c7
bezpilotními	bezpilotní	k2eAgInPc7d1
průzkumnými	průzkumný	k2eAgInPc7d1
letouny	letoun	k1gInPc7
Northrop	Northrop	k1gInSc1
Grumman	Grumman	k1gMnSc1
MQ-4C	MQ-4C	k1gMnSc1
Triton	triton	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoj	vývoj	k1gInSc1
</s>
<s>
Rollout	Rollout	k5eAaPmF
letounu	letoun	k1gInSc2
P-8	P-8	k1gFnSc2
30	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2009	#num#	k4
</s>
<s>
Protiponorkový	protiponorkový	k2eAgMnSc1d1
letoun	letoun	k1gMnSc1
Lockheed	Lockheed	k1gMnSc1
P-3	P-3	k1gMnSc1
Orion	orion	k1gInSc4
slouží	sloužit	k5eAaImIp3nS
v	v	k7c6
US	US	kA
Navy	Navy	k?
od	od	k7c2
roku	rok	k1gInSc2
1961	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
polovině	polovina	k1gFnSc6
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
US	US	kA
Navy	Navy	k?
začalo	začít	k5eAaPmAgNnS
uvažovat	uvažovat	k5eAaImF
o	o	k7c6
jejich	jejich	k3xOp3gFnSc6
náhradě	náhrada	k1gFnSc6
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
důsledku	důsledek	k1gInSc6
zvýšení	zvýšení	k1gNnSc2
hmotnosti	hmotnost	k1gFnSc2
se	se	k3xPyFc4
snížil	snížit	k5eAaPmAgInS
jejich	jejich	k3xOp3gInSc4
dolet	dolet	k1gInSc4
a	a	k8xC
výdrž	výdrž	k1gFnSc4
ve	v	k7c6
vzduchu	vzduch	k1gInSc6
a	a	k8xC
přibližoval	přibližovat	k5eAaImAgMnS
se	se	k3xPyFc4
i	i	k9
konec	konec	k1gInSc1
životnosti	životnost	k1gFnSc2
draku	drak	k1gInSc2
letadla	letadlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Požadavek	požadavek	k1gInSc1
námořnictva	námořnictvo	k1gNnSc2
rovněž	rovněž	k9
zahrnoval	zahrnovat	k5eAaImAgMnS
snížení	snížení	k1gNnSc4
provozních	provozní	k2eAgInPc2d1
nákladů	náklad	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
námořnictvo	námořnictvo	k1gNnSc1
objednalo	objednat	k5eAaPmAgNnS
u	u	k7c2
společnosti	společnost	k1gFnSc2
Lockheed	Lockheed	k1gInSc1
dva	dva	k4xCgInPc4
prototypy	prototyp	k1gInPc4
nového	nový	k2eAgInSc2d1
letounu	letoun	k1gInSc2
označeného	označený	k2eAgMnSc4d1
P-	P-	k1gMnSc4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
tento	tento	k3xDgInSc1
projekt	projekt	k1gInSc1
byl	být	k5eAaImAgInS
zrušen	zrušit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s>
Společnosti	společnost	k1gFnSc3
Boeing	boeing	k1gInSc4
a	a	k8xC
Lockheed	Lockheed	k1gInSc4
Martin	Martina	k1gFnPc2
se	se	k3xPyFc4
zúčastnily	zúčastnit	k5eAaPmAgFnP
nové	nový	k2eAgFnPc1d1
soutěže	soutěž	k1gFnPc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
vyhlášena	vyhlásit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lockheed	Lockheed	k1gInSc1
přišel	přijít	k5eAaPmAgInS
s	s	k7c7
letounem	letoun	k1gInSc7
Orion	orion	k1gInSc4
21	#num#	k4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
byla	být	k5eAaImAgFnS
vylepšená	vylepšený	k2eAgFnSc1d1
nová	nový	k2eAgFnSc1d1
verze	verze	k1gFnSc1
letounu	letoun	k1gInSc2
P-3	P-3	k1gMnPc2
s	s	k7c7
turbovrtulovými	turbovrtulový	k2eAgInPc7d1
motory	motor	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boeing	boeing	k1gInSc1
předložil	předložit	k5eAaPmAgInS
návrh	návrh	k1gInSc4
založený	založený	k2eAgInSc4d1
na	na	k7c6
civilním	civilní	k2eAgNnSc6d1
letadle	letadlo	k1gNnSc6
737	#num#	k4
<g/>
-	-	kIx~
<g/>
800	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soutěže	soutěž	k1gFnSc2
se	se	k3xPyFc4
zúčastnila	zúčastnit	k5eAaPmAgFnS
i	i	k9
společnost	společnost	k1gFnSc1
BAE	BAE	kA
Systems	Systems	k1gInSc1
s	s	k7c7
novou	nový	k2eAgFnSc7d1
verzí	verze	k1gFnSc7
letounu	letoun	k1gInSc2
Nimrod	Nimrod	k1gMnSc1
MRA	mřít	k5eAaImSgInS
<g/>
4	#num#	k4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
byla	být	k5eAaImAgFnS
nejnovější	nový	k2eAgFnSc1d3
varianta	varianta	k1gFnSc1
britského	britský	k2eAgNnSc2d1
námořního	námořní	k2eAgNnSc2d1
hlídkového	hlídkový	k2eAgNnSc2d1
letadla	letadlo	k1gNnSc2
Hawker	Hawker	k1gMnSc1
Siddeley	Siddelea	k1gFnSc2
Nimrod	Nimrod	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
BAE	BAE	kA
svou	svůj	k3xOyFgFnSc4
nabídku	nabídka	k1gFnSc4
stáhla	stáhnout	k5eAaPmAgFnS
v	v	k7c6
říjnu	říjen	k1gInSc6
2002	#num#	k4
<g/>
,	,	kIx,
protože	protože	k8xS
si	se	k3xPyFc3
uvědomovala	uvědomovat	k5eAaImAgFnS
politickou	politický	k2eAgFnSc4d1
realitu	realita	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
zakázka	zakázka	k1gFnSc1
nebude	být	k5eNaImBp3nS
přidělena	přidělen	k2eAgFnSc1d1
zahraničnímu	zahraniční	k2eAgInSc3d1
výrobci	výrobce	k1gMnPc1
bez	bez	k7c2
partnera	partner	k1gMnSc2
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2004	#num#	k4
byl	být	k5eAaImAgMnS
vítězem	vítěz	k1gMnSc7
soutěže	soutěž	k1gFnSc2
vyhlášen	vyhlášen	k2eAgInSc4d1
návrh	návrh	k1gInSc4
společnosti	společnost	k1gFnSc2
Boeing	boeing	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Následující	následující	k2eAgInSc4d1
měsíc	měsíc	k1gInSc4
uzavřelo	uzavřít	k5eAaPmAgNnS
US	US	kA
Navy	Navy	k?
se	s	k7c7
společností	společnost	k1gFnSc7
Boeing	boeing	k1gInSc4
kontrakt	kontrakt	k1gInSc4
na	na	k7c4
vývoj	vývoj	k1gInSc4
letounu	letoun	k1gInSc2
MMA	MMA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Uvedení	uvedení	k1gNnSc4
do	do	k7c2
operační	operační	k2eAgFnSc2d1
služby	služba	k1gFnSc2
je	být	k5eAaImIp3nS
plánováno	plánovat	k5eAaImNgNnS
na	na	k7c4
rok	rok	k1gInSc4
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projekt	projekt	k1gInSc1
předpokládá	předpokládat	k5eAaImIp3nS
dodání	dodání	k1gNnSc4
nejméně	málo	k6eAd3
108	#num#	k4
letounů	letoun	k1gInPc2
pro	pro	k7c4
US	US	kA
Navy	Navy	k?
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
pravděpodobně	pravděpodobně	k6eAd1
další	další	k2eAgInPc1d1
budou	být	k5eAaImBp3nP
dodány	dodat	k5eAaPmNgInP
dalším	další	k2eAgInPc3d1
státům	stát	k1gInPc3
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
používají	používat	k5eAaImIp3nP
letouny	letoun	k1gInPc1
P-	P-	k1gFnSc2
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodnota	hodnota	k1gFnSc1
projektu	projekt	k1gInSc2
je	být	k5eAaImIp3nS
odhadována	odhadovat	k5eAaImNgFnS
na	na	k7c4
15	#num#	k4
miliard	miliarda	k4xCgFnPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
dodavatelé	dodavatel	k1gMnPc1
určitých	určitý	k2eAgFnPc2d1
částí	část	k1gFnPc2
se	se	k3xPyFc4
na	na	k7c6
projektu	projekt	k1gInSc6
podílejí	podílet	k5eAaImIp3nP
i	i	k9
společnosti	společnost	k1gFnPc1
Raytheon	Raytheona	k1gFnPc2
<g/>
,	,	kIx,
Northrop	Northrop	k1gInSc4
Grumman	Grumman	k1gMnSc1
<g/>
,	,	kIx,
Spirit	Spirit	k1gInSc1
AeroSystems	AeroSystems	k1gInSc1
<g/>
,	,	kIx,
GE	GE	kA
Aviation	Aviation	k1gInSc1
Systems	Systems	k1gInSc1
<g/>
,	,	kIx,
Marshall	Marshall	k1gInSc1
Aerospace	Aerospace	k1gFnSc2
<g/>
,	,	kIx,
CFMI	CFMI	kA
a	a	k8xC
BAE	BAE	kA
Systems	Systems	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2005	#num#	k4
letouny	letoun	k1gInPc1
MMA	MMA	kA
obdržely	obdržet	k5eAaPmAgInP
označení	označení	k1gNnSc4
P-	P-	k1gFnPc2
<g/>
8	#num#	k4
<g/>
A.	A.	kA
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Letoun	letoun	k1gMnSc1
bude	být	k5eAaImBp3nS
součástí	součást	k1gFnSc7
systému	systém	k1gInSc2
„	„	k?
<g/>
Broad	Broad	k1gInSc1
Area	area	k1gFnSc1
Maritime	Maritim	k1gInSc5
Surveillance	Surveillanka	k1gFnSc3
UAV	UAV	kA
System	Syst	k1gInSc7
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
bude	být	k5eAaImBp3nS
poskytovat	poskytovat	k5eAaImF
nepřetržité	přetržitý	k2eNgNnSc1d1
námořní	námořní	k2eAgNnSc1d1
sledování	sledování	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Systém	systém	k1gInSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
začít	začít	k5eAaPmF
pracovat	pracovat	k5eAaImF
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
40	#num#	k4
bezpilotních	bezpilotní	k2eAgInPc2d1
letounů	letoun	k1gInPc2
založených	založený	k2eAgInPc2d1
na	na	k7c6
letounu	letoun	k1gInSc6
RQ-4	RQ-4	k1gFnSc2
Global	globat	k5eAaImAgInS
Hawk	Hawk	k1gInSc1
bude	být	k5eAaImBp3nS
do	do	k7c2
tohoto	tento	k3xDgInSc2
programu	program	k1gInSc2
zapojeno	zapojit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s>
Kokpit	kokpit	k1gInSc1
amerického	americký	k2eAgMnSc2d1
P-8A	P-8A	k1gMnSc2
</s>
<s>
V	v	k7c6
polovině	polovina	k1gFnSc6
roku	rok	k1gInSc2
2008	#num#	k4
námořní	námořní	k2eAgFnSc2d1
velitelství	velitelství	k1gNnSc6
NAVAIR	NAVAIR	kA
zrušilo	zrušit	k5eAaPmAgNnS
požadavek	požadavek	k1gInSc4
na	na	k7c4
vybavení	vybavení	k1gNnSc4
letounu	letoun	k1gInSc2
P-8A	P-8A	k1gMnPc2
detektorem	detektor	k1gInSc7
magnetických	magnetický	k2eAgFnPc2d1
anomálií	anomálie	k1gFnPc2
(	(	kIx(
<g/>
MAD	MAD	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účelem	účel	k1gInSc7
rozhodnutí	rozhodnutí	k1gNnSc2
NAVAIR	NAVAIR	kA
bylo	být	k5eAaImAgNnS
snížit	snížit	k5eAaPmF
hmotnost	hmotnost	k1gFnSc4
letounu	letoun	k1gInSc2
o	o	k7c4
asi	asi	k9
1	#num#	k4
600	#num#	k4
kg	kg	kA
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
má	mít	k5eAaImIp3nS
umožnit	umožnit	k5eAaPmF
větší	veliký	k2eAgInSc4d2
dolet	dolet	k1gInSc4
a	a	k8xC
vytrvalost	vytrvalost	k1gFnSc4
ve	v	k7c6
vzduchu	vzduch	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verze	verze	k1gFnSc1
P-8I	P-8I	k1gFnSc1
určená	určený	k2eAgFnSc1d1
pro	pro	k7c4
Indické	indický	k2eAgNnSc4d1
námořnictvo	námořnictvo	k1gNnSc4
s	s	k7c7
detektorem	detektor	k1gInSc7
magnetických	magnetický	k2eAgFnPc2d1
anomálií	anomálie	k1gFnPc2
i	i	k9
nadále	nadále	k6eAd1
počítá	počítat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letoun	letoun	k1gInSc1
P-8A	P-8A	k1gFnSc2
bude	být	k5eAaImBp3nS
vybaven	vybavit	k5eAaPmNgInS
i	i	k9
novým	nový	k2eAgInSc7d1
senzorem	senzor	k1gInSc7
uhlovodíků	uhlovodík	k1gInPc2
k	k	k7c3
vyhledávání	vyhledávání	k1gNnSc3
ponorek	ponorka	k1gFnPc2
na	na	k7c4
dieselový	dieselový	k2eAgInSc4d1
pohon	pohon	k1gInSc4
a	a	k8xC
ostatních	ostatní	k2eAgMnPc2d1
konvenčně	konvenčně	k6eAd1
poháněných	poháněný	k2eAgNnPc2d1
plavidel	plavidlo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgInSc1
let	let	k1gInSc1
letounu	letoun	k1gInSc2
se	se	k3xPyFc4
uskutečnil	uskutečnit	k5eAaPmAgInS
25	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Druhý	druhý	k4xOgInSc1
5	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2009	#num#	k4
a	a	k8xC
třetí	třetí	k4xOgInSc1
vyrobený	vyrobený	k2eAgInSc1d1
letoun	letoun	k1gInSc1
vzlétl	vzlétnout	k5eAaPmAgInS
a	a	k8xC
začal	začít	k5eAaPmAgInS
být	být	k5eAaImF
testován	testován	k2eAgInSc4d1
29	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgInPc1
letouny	letoun	k1gInPc1
se	se	k3xPyFc4
ihned	ihned	k6eAd1
zapojily	zapojit	k5eAaPmAgInP
do	do	k7c2
série	série	k1gFnSc2
letových	letový	k2eAgInPc2d1
testů	test	k1gInPc2
konaných	konaný	k2eAgInPc2d1
převážně	převážně	k6eAd1
z	z	k7c2
námořní	námořní	k2eAgFnSc2d1
letecké	letecký	k2eAgFnSc2d1
základny	základna	k1gFnSc2
NAS	NAS	kA
Patuxent	Patuxent	k1gMnSc1
River	River	k1gMnSc1
v	v	k7c6
Marylandu	Maryland	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Využívaly	využívat	k5eAaImAgInP,k5eAaPmAgInP
se	se	k3xPyFc4
především	především	k9
k	k	k7c3
testům	test	k1gInPc3
souvisejícím	související	k2eAgInPc3d1
s	s	k7c7
integrací	integrace	k1gFnSc7
jednotlivých	jednotlivý	k2eAgInPc2d1
druhů	druh	k1gInPc2
výzbroje	výzbroj	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sonarové	Sonarová	k1gFnSc3
bóje	bóje	k1gFnSc2
byly	být	k5eAaImAgInP
poprvé	poprvé	k6eAd1
vyzkoušeny	vyzkoušen	k2eAgInPc4d1
15	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2010	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
svrženo	svrhnout	k5eAaPmNgNnS
celkem	celkem	k6eAd1
6	#num#	k4
bójí	bóje	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Následně	následně	k6eAd1
byly	být	k5eAaImAgInP
vyrobeny	vyrobit	k5eAaPmNgInP
tři	tři	k4xCgInPc1
předsériové	předsériový	k2eAgInPc1d1
exempláře	exemplář	k1gInPc1
určené	určený	k2eAgInPc1d1
k	k	k7c3
vojskovým	vojskový	k2eAgFnPc3d1
operačním	operační	k2eAgFnPc3d1
zkouškám	zkouška	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
únoru	únor	k1gInSc6
2013	#num#	k4
byla	být	k5eAaImAgFnS
završena	završen	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
první	první	k4xOgFnSc2
šestikusové	šestikusový	k2eAgFnSc2d1
série	série	k1gFnSc2
P-	P-	k1gFnSc2
<g/>
8	#num#	k4
<g/>
A	A	kA
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
první	první	k4xOgInSc1
vzlétl	vzlétnout	k5eAaPmAgInS
7	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
kontraktů	kontrakt	k1gInPc2
z	z	k7c2
let	léto	k1gNnPc2
2011	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
přitom	přitom	k6eAd1
bylo	být	k5eAaImAgNnS
dosud	dosud	k6eAd1
objednáno	objednat	k5eAaPmNgNnS
24	#num#	k4
sériových	sériový	k2eAgFnPc2d1
P-	P-	k1gFnPc2
<g/>
8	#num#	k4
<g/>
A.	A.	kA
První	první	k4xOgInSc4
sériové	sériový	k2eAgInPc1d1
Poseidony	Poseidon	k1gMnPc7
byly	být	k5eAaImAgFnP
od	od	k7c2
března	březen	k1gInSc2
2012	#num#	k4
předávány	předávat	k5eAaImNgFnP
na	na	k7c4
námořní	námořní	k2eAgFnSc4d1
leteckou	letecký	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
NAS	NAS	kA
Jacksonville	Jacksonvill	k1gMnSc2
na	na	k7c6
Floridě	Florida	k1gFnSc6
k	k	k7c3
letce	letka	k1gFnSc3
VP-16	VP-16	k1gMnPc3
„	„	k?
<g/>
War	War	k1gMnSc1
Eagles	Eagles	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhou	druhý	k4xOgFnSc7
operační	operační	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
s	s	k7c7
P-8A	P-8A	k1gFnSc7
se	se	k3xPyFc4
na	na	k7c6
stejné	stejný	k2eAgFnSc6d1
základně	základna	k1gFnSc6
stala	stát	k5eAaPmAgFnS
letka	letka	k1gFnSc1
VP-5	VP-5	k1gFnSc2
„	„	k?
<g/>
Mad	Mad	k1gMnSc1
Foxes	Foxes	k1gMnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Námořnictvo	námořnictvo	k1gNnSc1
zvažuje	zvažovat	k5eAaImIp3nS
dodání	dodání	k1gNnSc4
celkem	celkem	k6eAd1
až	až	k9
117	#num#	k4
strojů	stroj	k1gInPc2
tohoto	tento	k3xDgInSc2
typu	typ	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
prosinci	prosinec	k1gInSc6
2013	#num#	k4
typ	typ	k1gInSc4
P-8A	P-8A	k1gFnSc2
dosáhl	dosáhnout	k5eAaPmAgMnS
částečné	částečný	k2eAgFnSc2d1
operační	operační	k2eAgFnSc2d1
schopnosti	schopnost	k1gFnSc2
(	(	kIx(
<g/>
IOC	IOC	kA
<g/>
,	,	kIx,
Initial	Initial	k1gMnSc1
Operational	Operational	k1gFnSc2
Capability	Capabilita	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
bylo	být	k5eAaImAgNnS
dodáno	dodat	k5eAaPmNgNnS
celkem	celkem	k6eAd1
12	#num#	k4
sériových	sériový	k2eAgFnPc2d1
P-	P-	k1gFnPc2
<g/>
8	#num#	k4
<g/>
A.	A.	kA
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
srpnu	srpen	k1gInSc6
2015	#num#	k4
stoupl	stoupnout	k5eAaPmAgInS
počet	počet	k1gInSc1
americkým	americký	k2eAgNnSc7d1
námořnictvem	námořnictvo	k1gNnSc7
objednaných	objednaný	k2eAgMnPc2d1
letounů	letoun	k1gInPc2
na	na	k7c4
celkových	celkový	k2eAgInPc2d1
68	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
počátku	počátek	k1gInSc6
ledna	leden	k1gInSc2
2017	#num#	k4
americké	americký	k2eAgNnSc4d1
námořnictvo	námořnictvo	k1gNnSc4
převzalo	převzít	k5eAaPmAgNnS
50	#num#	k4
<g/>
.	.	kIx.
sériový	sériový	k2eAgMnSc1d1
P-	P-	k1gMnSc1
<g/>
8	#num#	k4
<g/>
A.	A.	kA
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vývoz	vývoz	k1gInSc1
a	a	k8xC
zahraniční	zahraniční	k2eAgFnSc1d1
účast	účast	k1gFnSc1
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1
obrany	obrana	k1gFnSc2
USA	USA	kA
chce	chtít	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
na	na	k7c6
letounu	letoun	k1gInSc6
podíleli	podílet	k5eAaImAgMnP
i	i	k9
potenciální	potenciální	k2eAgMnPc1d1
zahraniční	zahraniční	k2eAgMnPc1d1
uživatelé	uživatel	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
Australský	australský	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
obrany	obrana	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2007	#num#	k4
oznámil	oznámit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
P-8A	P-8A	k1gMnSc1
byl	být	k5eAaImAgMnS
vybrán	vybrat	k5eAaPmNgMnS
jako	jako	k8xS,k8xC
preferované	preferovaný	k2eAgNnSc4d1
letadlo	letadlo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
nahradí	nahradit	k5eAaPmIp3nS
flotilu	flotila	k1gFnSc4
letounů	letoun	k1gMnPc2
AP-3C	AP-3C	k1gFnSc2
Orion	orion	k1gInSc1
sloužících	sloužící	k1gMnPc2
u	u	k7c2
Australského	australský	k2eAgNnSc2d1
královského	královský	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
(	(	kIx(
<g/>
RAAF	RAAF	kA
<g/>
)	)	kIx)
ve	v	k7c6
spojení	spojení	k1gNnSc6
s	s	k7c7
bezpilotním	bezpilotní	k2eAgMnSc7d1
letounem	letoun	k1gMnSc7
(	(	kIx(
<g/>
později	pozdě	k6eAd2
byl	být	k5eAaImAgInS
vybrán	vybrán	k2eAgInSc1d1
Northrop	Northrop	k1gInSc1
Grumman	Grumman	k1gMnSc1
MQ-4C	MQ-4C	k1gMnSc1
Triton	triton	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyřazení	vyřazení	k1gNnSc1
posledních	poslední	k2eAgInPc2d1
letounů	letoun	k1gInPc2
AP-3C	AP-3C	k1gFnSc1
Orion	orion	k1gInSc1
u	u	k7c2
RAAF	RAAF	kA
bylo	být	k5eAaImAgNnS
plánováno	plánovat	k5eAaImNgNnS
na	na	k7c4
rok	rok	k1gInSc4
2018	#num#	k4
<g/>
,	,	kIx,
tedy	tedy	k8xC
po	po	k7c6
jejich	jejich	k3xOp3gFnSc6
téměř	téměř	k6eAd1
třicetileté	třicetiletý	k2eAgFnSc6d1
službě	služba	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Připravován	připravovat	k5eAaImNgMnS
byl	být	k5eAaImAgMnS
rovněž	rovněž	k9
podpis	podpis	k1gInSc4
mezistátní	mezistátní	k2eAgFnSc2d1
dohody	dohoda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
Austrálii	Austrálie	k1gFnSc4
pomohla	pomoct	k5eAaPmAgFnS
v	v	k7c6
přístupu	přístup	k1gInSc6
k	k	k7c3
utajovaným	utajovaný	k2eAgInPc3d1
údajům	údaj	k1gInPc3
a	a	k8xC
pomohla	pomoct	k5eAaPmAgFnS
jí	on	k3xPp3gFnSc3
zadat	zadat	k5eAaPmF
vlastní	vlastní	k2eAgNnPc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
požadavky	požadavek	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
březnu	březen	k1gInSc6
2009	#num#	k4
velitel	velitel	k1gMnSc1
australského	australský	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
prohlásil	prohlásit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
RAAF	RAAF	kA
by	by	kYmCp3nS
mohlo	moct	k5eAaImAgNnS
letouny	letoun	k1gMnPc4
P-8A	P-8A	k1gFnSc2
zařadit	zařadit	k5eAaPmF
do	do	k7c2
služby	služba	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
březnu	březen	k1gInSc6
2014	#num#	k4
australská	australský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
schválila	schválit	k5eAaPmAgFnS
nákup	nákup	k1gInSc4
osmi	osm	k4xCc2
letounů	letoun	k1gInPc2
P-8A	P-8A	k1gFnPc2
v	v	k7c6
hodnotě	hodnota	k1gFnSc6
3,6	3,6	k4
miliard	miliarda	k4xCgFnPc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
se	se	k3xPyFc4
objednávka	objednávka	k1gFnSc1
zvětšila	zvětšit	k5eAaPmAgFnS
na	na	k7c4
12	#num#	k4
letounů	letoun	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
exemplář	exemplář	k1gInSc1
byl	být	k5eAaImAgInS
RAAF	RAAF	kA
předán	předat	k5eAaPmNgInS
17	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
březnu	březen	k1gInSc6
2017	#num#	k4
byly	být	k5eAaImAgFnP
ve	v	k7c6
službě	služba	k1gFnSc6
dva	dva	k4xCgInPc1
letouny	letoun	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Poslední	poslední	k2eAgInSc4d1
dvanáctý	dvanáctý	k4xOgInSc4
letoun	letoun	k1gInSc4
RAAN	RAAN	kA
převzalo	převzít	k5eAaPmAgNnS
v	v	k7c6
prosinci	prosinec	k1gInSc6
2019	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
V	V	kA
prosince	prosinec	k1gInSc2
2020	#num#	k4
byly	být	k5eAaImAgFnP
objednány	objednán	k2eAgInPc4d1
další	další	k2eAgInPc4d1
dva	dva	k4xCgInPc4
letouny	letoun	k1gInPc4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
jejich	jejich	k3xOp3gMnSc4
celkový	celkový	k2eAgInSc1d1
počet	počet	k1gInSc1
stoupne	stoupnout	k5eAaPmIp3nS
na	na	k7c4
14	#num#	k4
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Indie	Indie	k1gFnSc1
</s>
<s>
Operátoři	operátor	k1gMnPc1
indického	indický	k2eAgMnSc2d1
P-8I	P-8I	k1gMnSc2
</s>
<s>
Indický	indický	k2eAgMnSc1d1
P-8I	P-8I	k1gMnSc1
s	s	k7c7
protilodními	protilodní	k2eAgFnPc7d1
střelami	střela	k1gFnPc7
Harpoon	Harpoona	k1gFnPc2
pod	pod	k7c7
křídly	křídlo	k1gNnPc7
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
2008	#num#	k4
společnost	společnost	k1gFnSc1
Boeing	boeing	k1gInSc4
navrhla	navrhnout	k5eAaPmAgFnS
letoun	letoun	k1gInSc4
P-8I	P-8I	k1gMnSc5
Neptune	Neptun	k1gMnSc5
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
upravená	upravený	k2eAgFnSc1d1
exportní	exportní	k2eAgFnSc1d1
verze	verze	k1gFnSc1
letounu	letoun	k1gInSc2
P-8A	P-8A	k1gFnSc2
pro	pro	k7c4
indické	indický	k2eAgNnSc4d1
námořnictvo	námořnictvo	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
4	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
indický	indický	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
obrany	obrana	k1gFnSc2
podepsal	podepsat	k5eAaPmAgMnS
dohodu	dohoda	k1gFnSc4
se	s	k7c7
společností	společnost	k1gFnSc7
Boeing	boeing	k1gInSc4
na	na	k7c4
dodání	dodání	k1gNnSc4
osmi	osm	k4xCc2
letounů	letoun	k1gInPc2
P-8I	P-8I	k1gFnSc2
za	za	k7c4
celkovou	celkový	k2eAgFnSc4d1
cenu	cena	k1gFnSc4
2,1	2,1	k4
miliardy	miliarda	k4xCgFnSc2
dolarů	dolar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
letouny	letoun	k1gInPc4
nahradí	nahradit	k5eAaPmIp3nP
zastaralé	zastaralý	k2eAgInPc4d1
turbovrtulové	turbovrtulový	k2eAgInPc4d1
letouny	letoun	k1gInPc4
Tupolev	Tupolev	k1gFnSc2
Tu-	Tu-	k1gFnSc7
<g/>
142	#num#	k4
<g/>
M.	M.	kA
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgMnSc1
P-8I	P-8I	k1gMnSc1
vzlétl	vzlétnout	k5eAaPmAgMnS
v	v	k7c6
Rentonu	Renton	k1gInSc6
28	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2011	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
zástavbě	zástavba	k1gFnSc6
vojenských	vojenský	k2eAgInPc2d1
systémů	systém	k1gInPc2
podstoupil	podstoupit	k5eAaPmAgMnS
v	v	k7c6
USA	USA	kA
celý	celý	k2eAgInSc1d1
zkušební	zkušební	k2eAgInSc1d1
program	program	k1gInSc1
<g/>
,	,	kIx,
zakončený	zakončený	k2eAgMnSc1d1
7	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgMnSc1
Poseidon	Poseidon	k1gMnSc1
byl	být	k5eAaImAgMnS
Indii	Indie	k1gFnSc4
dodán	dodán	k2eAgMnSc1d1
15	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2013	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
čtvrtý	čtvrtý	k4xOgInSc1
pak	pak	k9
v	v	k7c6
květnu	květen	k1gInSc6
2014	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Všech	všecek	k3xTgInPc2
osm	osm	k4xCc4
strojů	stroj	k1gInPc2
bylo	být	k5eAaImAgNnS
dodáno	dodat	k5eAaPmNgNnS
do	do	k7c2
konce	konec	k1gInSc2
roku	rok	k1gInSc2
2015	#num#	k4
a	a	k8xC
indická	indický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
uplatnila	uplatnit	k5eAaPmAgFnS
opci	opce	k1gFnSc4
na	na	k7c4
další	další	k2eAgInPc4d1
čtyři	čtyři	k4xCgInPc4
kusy	kus	k1gInPc4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
mají	mít	k5eAaImIp3nP
u	u	k7c2
námořního	námořní	k2eAgNnSc2d1
letectva	letectvo	k1gNnSc2
nahradit	nahradit	k5eAaPmF
stroje	stroj	k1gInPc4
Iljušin	iljušin	k1gInSc1
Il-	Il-	k1gFnSc1
<g/>
38	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
prosinci	prosinec	k1gInSc6
2019	#num#	k4
indické	indický	k2eAgNnSc1d1
ministerstvo	ministerstvo	k1gNnSc1
obrany	obrana	k1gFnSc2
potvrdilo	potvrdit	k5eAaPmAgNnS
zakoupení	zakoupení	k1gNnSc4
dalších	další	k2eAgMnPc2d1
šesti	šest	k4xCc2
letounů	letoun	k1gInPc2
<g/>
,	,	kIx,
takže	takže	k8xS
jejich	jejich	k3xOp3gInSc1
celkový	celkový	k2eAgInSc1d1
počet	počet	k1gInSc1
se	se	k3xPyFc4
zvýší	zvýšit	k5eAaPmIp3nS
na	na	k7c4
18	#num#	k4
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Britská	britský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
v	v	k7c6
listopadu	listopad	k1gInSc6
2015	#num#	k4
oznámila	oznámit	k5eAaPmAgFnS
plán	plán	k1gInSc4
objednat	objednat	k5eAaPmF
devět	devět	k4xCc4
kusů	kus	k1gInPc2
letounu	letoun	k1gInSc2
pro	pro	k7c4
potřebu	potřeba	k1gFnSc4
Royal	Royal	k1gInSc1
Air	Air	k1gMnSc1
Force	force	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
červenci	červenec	k1gInSc6
2016	#num#	k4
britské	britský	k2eAgFnPc1d1
ministerstvo	ministerstvo	k1gNnSc4
obrany	obrana	k1gFnSc2
potvrdilo	potvrdit	k5eAaPmAgNnS
objednávku	objednávka	k1gFnSc4
devíti	devět	k4xCc2
letounů	letoun	k1gInPc2
P-	P-	k1gFnSc2
<g/>
8	#num#	k4
<g/>
A	A	kA
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
budou	být	k5eAaImBp3nP
operovat	operovat	k5eAaImF
ze	z	k7c2
základny	základna	k1gFnSc2
RAF	raf	k0
Lossiemouth	Lossiemoutha	k1gFnPc2
ve	v	k7c6
Skotsku	Skotsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
jejich	jejich	k3xOp3gInPc4
hlavní	hlavní	k2eAgInPc4d1
úkoly	úkol	k1gInPc4
bude	být	k5eAaImBp3nS
patřit	patřit	k5eAaImF
ochrana	ochrana	k1gFnSc1
britských	britský	k2eAgFnPc2d1
jaderných	jaderný	k2eAgFnPc2d1
ponorek	ponorka	k1gFnPc2
a	a	k8xC
nových	nový	k2eAgFnPc2d1
letadlových	letadlový	k2eAgFnPc2d1
lodí	loď	k1gFnPc2
třídy	třída	k1gFnSc2
Queen	Queen	k2eAgInSc4d1
Elizabeth	Elizabeth	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgInPc4
dosud	dosud	k6eAd1
poslední	poslední	k2eAgInPc4d1
námořní	námořní	k2eAgInPc4d1
hlídkové	hlídkový	k2eAgInPc4d1
letouny	letoun	k1gInPc4
Hawker	Hawker	k1gMnSc1
Siddeley	Siddelea	k1gFnSc2
Nimrod	Nimrod	k1gMnSc1
přitom	přitom	k6eAd1
RAF	raf	k0
bez	bez	k7c2
náhrady	náhrada	k1gFnSc2
vyřadila	vyřadit	k5eAaPmAgFnS
roku	rok	k1gInSc2
2010	#num#	k4
a	a	k8xC
zkušenosti	zkušenost	k1gFnPc1
britských	britský	k2eAgFnPc2d1
leteckých	letecký	k2eAgFnPc2d1
osádek	osádka	k1gFnPc2
na	na	k7c6
poli	pole	k1gNnSc6
námořního	námořní	k2eAgNnSc2d1
hlídkování	hlídkování	k1gNnSc2
byly	být	k5eAaImAgFnP
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
udržovány	udržovat	k5eAaImNgFnP
za	za	k7c2
pomoci	pomoc	k1gFnSc2
programu	program	k1gInSc2
stáží	stázat	k5eAaPmIp3nP
u	u	k7c2
spojeneckých	spojenecký	k2eAgFnPc2d1
ozbrojených	ozbrojený	k2eAgFnPc2d1
sil	síla	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
perutí	peruť	k1gFnPc2
US	US	kA
Navy	Navy	k?
vybavených	vybavený	k2eAgNnPc2d1
typem	typ	k1gInSc7
P-8	P-8	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Britské	britský	k2eAgInPc1d1
letouny	letoun	k1gInPc1
nesou	nést	k5eAaImIp3nP
označení	označení	k1gNnSc4
Poseidon	Poseidon	k1gMnSc1
MRA	mřít	k5eAaImSgInS
Mk	Mk	k1gMnSc7
<g/>
.1	.1	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgInSc1
letoun	letoun	k1gInSc1
RAF	raf	k0
převzalo	převzít	k5eAaPmAgNnS
v	v	k7c6
říjnu	říjen	k1gInSc6
2019	#num#	k4
a	a	k8xC
na	na	k7c6
britské	britský	k2eAgFnSc6d1
půdě	půda	k1gFnSc6
první	první	k4xOgInSc1
stroj	stroj	k1gInSc1
přistál	přistát	k5eAaPmAgInS,k5eAaImAgInS
4	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Počáteční	počáteční	k2eAgFnSc2d1
bojové	bojový	k2eAgFnSc2d1
připravenosti	připravenost	k1gFnSc2
IOC	IOC	kA
(	(	kIx(
<g/>
Initial	Initial	k1gMnSc1
Operational	Operational	k1gFnSc2
Capability	Capabilita	k1gFnSc2
<g/>
)	)	kIx)
britské	britský	k2eAgInPc1d1
letouny	letoun	k1gInPc1
Poseidon	Poseidon	k1gMnSc1
MRA	mřít	k5eAaImSgInS
Mk	Mk	k1gMnSc1
<g/>
.1	.1	k4
dosáhly	dosáhnout	k5eAaPmAgInP
v	v	k7c6
dubnu	duben	k1gInSc6
2020	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Všechny	všechen	k3xTgInPc4
objednané	objednaný	k2eAgInPc4d1
letouny	letoun	k1gInPc4
mají	mít	k5eAaImIp3nP
být	být	k5eAaImF
dodány	dodat	k5eAaPmNgInP
do	do	k7c2
roku	rok	k1gInSc2
2024	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Norsko	Norsko	k1gNnSc1
</s>
<s>
Norské	norský	k2eAgNnSc1d1
ministerstvo	ministerstvo	k1gNnSc1
obrany	obrana	k1gFnSc2
oznámilo	oznámit	k5eAaPmAgNnS
záměr	záměr	k1gInSc4
zakoupit	zakoupit	k5eAaPmF
pětici	pětice	k1gFnSc4
letounů	letoun	k1gInPc2
P-8A	P-8A	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
by	by	kYmCp3nP
nahradily	nahradit	k5eAaPmAgFnP
šestici	šestice	k1gFnSc4
norských	norský	k2eAgFnPc2d1
P-3	P-3	k1gFnPc2
Orion	orion	k1gInSc4
a	a	k8xC
trojici	trojice	k1gFnSc4
strojů	stroj	k1gInPc2
Dassault	Dassault	k1gMnSc1
DA-	DA-	k1gMnSc1
<g/>
20	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
2016	#num#	k4
s	s	k7c7
prodejem	prodej	k1gInSc7
souhlasila	souhlasit	k5eAaImAgFnS
americká	americký	k2eAgFnSc1d1
administrativa	administrativa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
březnu	březen	k1gInSc6
2017	#num#	k4
bylo	být	k5eAaImAgNnS
objednáno	objednat	k5eAaPmNgNnS
pět	pět	k4xCc1
letounů	letoun	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnPc1
dodávky	dodávka	k1gFnPc1
proběhnou	proběhnout	k5eAaPmIp3nP
v	v	k7c6
letech	let	k1gInPc6
2021	#num#	k4
<g/>
–	–	k?
<g/>
2022	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
</s>
<s>
V	v	k7c6
červenci	červenec	k1gInSc6
2018	#num#	k4
novozélandský	novozélandský	k2eAgInSc1d1
premiér	premiér	k1gMnSc1
Winston	Winston	k1gInSc1
Peters	Peters	k1gInSc4
oznámil	oznámit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
země	zem	k1gFnPc1
zakoupí	zakoupit	k5eAaPmIp3nP
čtyři	čtyři	k4xCgInPc4
letouny	letoun	k1gInPc4
P-8	P-8	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
ve	v	k7c6
službě	služba	k1gFnSc6
nahradí	nahradit	k5eAaPmIp3nS
stávající	stávající	k2eAgFnSc1d1
P-3K2	P-3K2	k1gFnSc1
Orion	orion	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
letouny	letoun	k1gInPc7
bude	být	k5eAaImBp3nS
vybudováno	vybudován	k2eAgNnSc1d1
zázemí	zázemí	k1gNnSc1
na	na	k7c6
letecké	letecký	k2eAgFnSc6d1
základně	základna	k1gFnSc6
Ohakea	Ohake	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
P-8	P-8	k4
je	být	k5eAaImIp3nS
vojenská	vojenský	k2eAgFnSc1d1
verze	verze	k1gFnSc1
letounu	letoun	k1gInSc2
Boeing	boeing	k1gInSc4
737-800ER	737-800ER	k4
s	s	k7c7
křídlem	křídlo	k1gNnSc7
letounu	letoun	k1gInSc2
Boeing	boeing	k1gInSc4
737	#num#	k4
<g/>
-	-	kIx~
<g/>
900	#num#	k4
<g/>
ER	ER	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Drak	drak	k1gInSc1
letounu	letoun	k1gInSc2
je	být	k5eAaImIp3nS
delší	dlouhý	k2eAgMnSc1d2
než	než	k8xS
trup	trup	k1gMnSc1
letounu	letoun	k1gInSc2
C-40	C-40	k1gFnPc2
Clipper	clipper	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
má	mít	k5eAaImIp3nS
trup	trup	k1gInSc1
odvozen	odvodit	k5eAaPmNgInS
od	od	k7c2
letounu	letoun	k1gInSc2
Boeing	boeing	k1gInSc4
737	#num#	k4
<g/>
-	-	kIx~
<g/>
700	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
P-8	P-8	k1gMnSc1
má	mít	k5eAaImIp3nS
zesílený	zesílený	k2eAgInSc4d1
trup	trup	k1gInSc4
a	a	k8xC
zkosené	zkosený	k2eAgInPc4d1
konce	konec	k1gInPc4
křídla	křídlo	k1gNnSc2
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
Boeing	boeing	k1gInSc4
767-400ER	767-400ER	k4
místo	místo	k7c2
zalomených	zalomený	k2eAgInPc2d1
konců	konec	k1gInPc2
křídla	křídlo	k1gNnSc2
používaných	používaný	k2eAgInPc2d1
u	u	k7c2
letounů	letoun	k1gInPc2
řady	řada	k1gFnSc2
737	#num#	k4
<g/>
NG	NG	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
Letoun	letoun	k1gInSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
zvýšení	zvýšení	k1gNnSc4
doletu	dolet	k1gInSc2
vybaven	vybavit	k5eAaPmNgInS
dalšími	další	k2eAgInPc7d1
třemi	tři	k4xCgFnPc7
palivovými	palivový	k2eAgFnPc7d1
nádržemi	nádrž	k1gFnPc7
v	v	k7c6
trupu	trup	k1gInSc6
<g/>
,	,	kIx,
tři	tři	k4xCgInPc1
jsou	být	k5eAaImIp3nP
umístěny	umístit	k5eAaPmNgInP
vpředu	vpředu	k6eAd1
v	v	k7c6
nákladním	nákladní	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
a	a	k8xC
tři	tři	k4xCgMnPc1
vzadu	vzadu	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
P-8	P-8	k4
je	být	k5eAaImIp3nS
vybaven	vybavit	k5eAaPmNgInS
multifunkčním	multifunkční	k2eAgInSc7d1
vyhledávacím	vyhledávací	k2eAgInSc7d1
radarem	radar	k1gInSc7
Raytheon	Raytheona	k1gFnPc2
AN	AN	kA
<g/>
/	/	kIx~
<g/>
APY-	APY-	k1gMnSc1
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Varianty	varianta	k1gFnPc1
</s>
<s>
P-8	P-8	k4
v	v	k7c6
letu	let	k1gInSc6
nad	nad	k7c7
Pacifikem	Pacifik	k1gInSc7
</s>
<s>
P-8A	P-8A	k4
</s>
<s>
Varianta	varianta	k1gFnSc1
vyráběná	vyráběný	k2eAgFnSc1d1
pro	pro	k7c4
US	US	kA
Navy	Navy	k?
a	a	k8xC
Royal	Royal	k1gMnSc1
Australian	Australian	k1gMnSc1
Air	Air	k1gMnSc1
Force	force	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
P-8I	P-8I	k4
</s>
<s>
Varianta	varianta	k1gFnSc1
vyráběná	vyráběný	k2eAgFnSc1d1
pro	pro	k7c4
indické	indický	k2eAgNnSc4d1
námořnictvo	námořnictvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Poseidon	Poseidon	k1gMnSc1
MRA	mřít	k5eAaImSgInS
Mk	Mk	k1gMnSc7
<g/>
.1	.1	k4
</s>
<s>
Varianta	varianta	k1gFnSc1
vyráběná	vyráběný	k2eAgFnSc1d1
pro	pro	k7c4
Britské	britský	k2eAgNnSc4d1
královské	královský	k2eAgNnSc4d1
letectvo	letectvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Uživatelé	uživatel	k1gMnPc1
</s>
<s>
Uživatelé	uživatel	k1gMnPc1
Boeingu	boeing	k1gInSc2
P-8	P-8	k1gFnPc2
Poseidon	Poseidon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
Austrálie	Austrálie	k1gFnSc2
</s>
<s>
Royal	Royal	k1gMnSc1
Australian	Australian	k1gMnSc1
Air	Air	k1gMnSc1
Force	force	k1gFnSc2
–	–	k?
v	v	k7c6
letech	let	k1gInPc6
2017	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
dodáno	dodat	k5eAaPmNgNnS
celkem	celek	k1gInSc7
12	#num#	k4
letounů	letoun	k1gInPc2
P-	P-	k1gFnSc2
<g/>
8	#num#	k4
<g/>
A.	A.	kA
Provozuje	provozovat	k5eAaImIp3nS
je	být	k5eAaImIp3nS
11	#num#	k4
<g/>
.	.	kIx.
squadrona	squadrona	k1gFnSc1
RAAF	RAAF	kA
<g/>
.	.	kIx.
</s>
<s>
Indie	Indie	k1gFnSc1
Indie	Indie	k1gFnSc2
</s>
<s>
Indické	indický	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
–	–	k?
8	#num#	k4
letounů	letoun	k1gInPc2
P-8I	P-8I	k1gFnPc2
ve	v	k7c6
službě	služba	k1gFnSc6
v	v	k7c6
květnu	květen	k1gInSc6
2016	#num#	k4
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
další	další	k2eAgMnPc1d1
4	#num#	k4
objednané	objednaný	k2eAgFnSc2d1
v	v	k7c6
červnu	červen	k1gInSc6
2016	#num#	k4
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
šest	šest	k4xCc1
v	v	k7c6
prosinci	prosinec	k1gInSc6
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
</s>
<s>
Royal	Royal	k1gMnSc1
New	New	k1gMnSc1
Zealand	Zealanda	k1gFnPc2
Air	Air	k1gMnSc1
Force	force	k1gFnSc2
–	–	k?
4	#num#	k4
stroje	stroj	k1gInSc2
P-8A	P-8A	k1gFnSc2
objednány	objednán	k2eAgFnPc4d1
<g/>
,	,	kIx,
k	k	k7c3
dodání	dodání	k1gNnSc3
od	od	k7c2
roku	rok	k1gInSc2
2022	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Norsko	Norsko	k1gNnSc1
Norsko	Norsko	k1gNnSc1
</s>
<s>
Norské	norský	k2eAgNnSc1d1
královské	královský	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
–	–	k?
objednaných	objednaný	k2eAgInPc2d1
5	#num#	k4
letounů	letoun	k1gInPc2
P-	P-	k1gFnSc2
<g/>
8	#num#	k4
<g/>
A	A	kA
<g/>
,	,	kIx,
dodání	dodání	k1gNnSc3
v	v	k7c6
letech	léto	k1gNnPc6
2022	#num#	k4
a	a	k8xC
2023	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
<s>
Royal	Royal	k1gInSc1
Air	Air	k1gFnSc2
Force	force	k1gFnSc4
–	–	k?
objednaných	objednaný	k2eAgInPc2d1
9	#num#	k4
letounů	letoun	k1gInPc2
Poseidon	Poseidon	k1gMnSc1
MRA	mřít	k5eAaImSgInS
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
USA	USA	kA
USA	USA	kA
</s>
<s>
Americké	americký	k2eAgNnSc1d1
námořnictvo	námořnictvo	k1gNnSc1
–	–	k?
zakoupeno	zakoupen	k2eAgNnSc1d1
122	#num#	k4
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
89	#num#	k4
letounů	letoun	k1gInPc2
P-8A	P-8A	k1gFnSc2
dodáno	dodat	k5eAaPmNgNnS
v	v	k7c6
červnu	červen	k1gInSc6
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Specifikace	specifikace	k1gFnSc1
(	(	kIx(
<g/>
P-	P-	k1gFnSc1
<g/>
8	#num#	k4
<g/>
A	A	kA
<g/>
)	)	kIx)
</s>
<s>
Nejnovější	nový	k2eAgNnSc1d3
námořní	námořní	k2eAgNnSc1d1
hlídkové	hlídkový	k2eAgNnSc1d1
a	a	k8xC
průzkumné	průzkumný	k2eAgNnSc1d1
letadlo	letadlo	k1gNnSc1
US	US	kA
Navy	Navy	k?
P-8A	P-8A	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
letí	letět	k5eAaImIp3nS
společně	společně	k6eAd1
s	s	k7c7
letounem	letoun	k1gInSc7
P-3	P-3	k1gFnPc2
Orion	orion	k1gInSc4
poblíž	poblíž	k7c2
NAS	NAS	kA
Patuxent	Patuxent	k1gInSc1
River	River	k1gInSc1
<g/>
,	,	kIx,
Maryland	Maryland	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Data	datum	k1gNnPc1
převzata	převzat	k2eAgNnPc1d1
z	z	k7c2
US	US	kA
Navy	Navy	k?
P-8A	P-8A	k1gMnSc2
Fact	Fact	k2eAgInSc4d1
File	File	k1gInSc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
firemních	firemní	k2eAgFnPc2d1
specifikací	specifikace	k1gFnPc2
Boeing	boeing	k1gInSc1
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Technické	technický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Posádka	posádka	k1gFnSc1
<g/>
:	:	kIx,
9	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
letový	letový	k2eAgInSc1d1
personál	personál	k1gInSc1
<g/>
,	,	kIx,
7	#num#	k4
obsluha	obsluha	k1gFnSc1
systémů	systém	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
Rozpětí	rozpětí	k1gNnSc1
<g/>
:	:	kIx,
37,64	37,64	k4
m	m	kA
</s>
<s>
Délka	délka	k1gFnSc1
<g/>
:	:	kIx,
39,47	39,47	k4
m	m	kA
</s>
<s>
Výška	výška	k1gFnSc1
<g/>
:	:	kIx,
12,83	12,83	k4
m	m	kA
</s>
<s>
Nosná	nosný	k2eAgFnSc1d1
plocha	plocha	k1gFnSc1
<g/>
:	:	kIx,
?	?	kIx.
</s>
<s desamb="1">
m²	m²	k?
</s>
<s>
Plošné	plošný	k2eAgNnSc1d1
zatížení	zatížení	k1gNnSc1
<g/>
:	:	kIx,
?	?	kIx.
</s>
<s desamb="1">
kg	kg	kA
<g/>
/	/	kIx~
<g/>
m²	m²	k?
</s>
<s>
Prázdná	prázdný	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
<g/>
:	:	kIx,
62	#num#	k4
730	#num#	k4
kg	kg	kA
</s>
<s>
Max	Max	k1gMnSc1
<g/>
.	.	kIx.
vzletová	vzletový	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
:	:	kIx,
85	#num#	k4
370	#num#	k4
kg	kg	kA
</s>
<s>
Pohonná	pohonný	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
<g/>
:	:	kIx,
2	#num#	k4
<g/>
×	×	k?
dvouproudový	dvouproudový	k2eAgInSc1d1
motor	motor	k1gInSc1
CFM56-7B	CFM56-7B	k1gFnSc2
</s>
<s>
Výkon	výkon	k1gInSc1
pohonné	pohonný	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
:	:	kIx,
120	#num#	k4
kN	kN	k?
</s>
<s>
Výkony	výkon	k1gInPc1
</s>
<s>
Cestovní	cestovní	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
:	:	kIx,
818	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
ve	v	k7c6
výšce	výška	k1gFnSc6
?	?	kIx.
</s>
<s desamb="1">
m	m	kA
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1
rychlost	rychlost	k1gFnSc1
<g/>
:	:	kIx,
907	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
ve	v	k7c6
výšce	výška	k1gFnSc6
?	?	kIx.
</s>
<s desamb="1">
m	m	kA
</s>
<s>
Dolet	dolet	k1gInSc1
<g/>
:	:	kIx,
2	#num#	k4
220	#num#	k4
km	km	kA
(	(	kIx(
<g/>
+	+	kIx~
<g/>
4	#num#	k4
h	h	k?
vytrvalost	vytrvalost	k1gFnSc4
na	na	k7c4
pozici	pozice	k1gFnSc4
<g/>
)	)	kIx)
</s>
<s>
Dostup	dostup	k1gInSc1
<g/>
:	:	kIx,
12	#num#	k4
500	#num#	k4
m	m	kA
(	(	kIx(
<g/>
41	#num#	k4
000	#num#	k4
ft	ft	k?
<g/>
)	)	kIx)
</s>
<s>
Stoupavost	stoupavost	k1gFnSc1
<g/>
:	:	kIx,
?	?	kIx.
</s>
<s desamb="1">
m	m	kA
<g/>
/	/	kIx~
<g/>
s	s	k7c7
(	(	kIx(
<g/>
?	?	kIx.
</s>
<s desamb="1">
m	m	kA
<g/>
/	/	kIx~
<g/>
min	min	kA
<g/>
)	)	kIx)
</s>
<s>
Výzbroj	výzbroj	k1gFnSc1
</s>
<s>
5	#num#	k4
<g/>
×	×	k?
střela	střela	k1gFnSc1
<g/>
,	,	kIx,
mina	mina	k1gFnSc1
nebo	nebo	k8xC
torpédo	torpédo	k1gNnSc1
ve	v	k7c6
vnitřní	vnitřní	k2eAgFnSc6d1
pumovnici	pumovnice	k1gFnSc6
</s>
<s>
6	#num#	k4
<g/>
×	×	k?
střela	střela	k1gFnSc1
<g/>
,	,	kIx,
mina	mina	k1gFnSc1
nebo	nebo	k8xC
torpédo	torpédo	k1gNnSc1
na	na	k7c6
vnějších	vnější	k2eAgInPc6d1
závěsnících	závěsník	k1gInPc6
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Boeing	boeing	k1gInSc1
P-8	P-8	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.1	.1	k4
2	#num#	k4
"	"	kIx"
<g/>
Boeing	boeing	k1gInSc1
P-8A	P-8A	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
vykonal	vykonat	k5eAaPmAgMnS
první	první	k4xOgInSc4
úspěšný	úspěšný	k2eAgInSc4d1
let	let	k1gInSc4
<g/>
"	"	kIx"
Archivováno	archivovat	k5eAaBmNgNnS
30	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boeing	boeing	k1gInSc1
<g/>
,	,	kIx,
27	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
<g/>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
Boeing	boeing	k1gInSc1
Delivers	Delivers	k1gInSc1
100	#num#	k4
<g/>
th	th	k?
P-8A	P-8A	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
Built	Built	k1gMnSc1
for	forum	k1gNnPc2
the	the	k?
U.	U.	kA
<g/>
S.	S.	kA
Navy	Navy	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navalnews	Navalnews	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2020-05-15	2020-05-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
US	US	kA
Navy	Navy	k?
marks	marks	k1gInSc1
new	new	k?
steps	steps	k1gInSc1
in	in	k?
manned	manned	k1gInSc1
<g/>
/	/	kIx~
<g/>
unmanned	unmanned	k1gInSc1
cooperation	cooperation	k1gInSc1
with	witha	k1gFnPc2
latest	latest	k1gMnSc1
MQ-4C	MQ-4C	k1gMnSc1
&	&	k?
P-8A	P-8A	k1gMnSc1
Test	test	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navyrecognition	Navyrecognition	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2016-06-24	2016-06-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
LEWIS	LEWIS	kA
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BAE	BAE	kA
stahuje	stahovat	k5eAaImIp3nS
konkurenční	konkurenční	k2eAgFnSc4d1
nabídku	nabídka	k1gFnSc4
proti	proti	k7c3
MMA	MMA	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flight	Flight	k1gMnSc1
International	International	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reed	Reed	k1gInSc1
Business	business	k1gInSc1
Information	Information	k1gInSc4
UK	UK	kA
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
"	"	kIx"
<g/>
Boeing	boeing	k1gInSc1
Team	team	k1gInSc1
Wins	Wins	k1gInSc1
$	$	kIx~
<g/>
3.89	3.89	k4
Billion	Billion	k1gInSc1
Multi-Mission	Multi-Mission	k1gInSc4
Maritime	Maritim	k1gInSc5
Aircraft	Aircraft	k1gMnSc1
Program	program	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boeing	boeing	k1gInSc1
<g/>
,	,	kIx,
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2004	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Boeing	boeing	k1gInSc4
to	ten	k3xDgNnSc1
Develop	Develop	k1gInSc1
Navy	Navy	k?
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Multi-Mission	Multi-Mission	k1gInSc1
Maritime	Maritim	k1gInSc5
Aircraft	Aircrafta	k1gFnPc2
<g/>
"	"	kIx"
Archivováno	archivovat	k5eAaBmNgNnS
25	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Navy	Navy	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2004.1	2004.1	k4
2	#num#	k4
"	"	kIx"
<g/>
P-	P-	k1gFnPc2
<g/>
8	#num#	k4
<g/>
A	a	k9
Multi-mission	Multi-mission	k1gInSc1
Maritime	Maritim	k1gInSc5
Aircraft	Aircraftum	k1gNnPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gMnSc1
States	States	k1gMnSc1
Navy	Navy	k?
<g/>
,	,	kIx,
25	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2007	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Boeing-led	Boeing-led	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
Team	team	k1gInSc4
Begins	Begins	k1gInSc1
Production	Production	k1gInSc1
of	of	k?
First	First	k1gInSc1
P-8A	P-8A	k1gMnSc2
Fuselage	Fuselag	k1gMnSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boeing	boeing	k1gInSc1
<g/>
,	,	kIx,
12	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2007	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Hatcher	Hatchra	k1gFnPc2
<g/>
,	,	kIx,
Renee	Renee	k1gFnSc1
<g/>
.	.	kIx.
"	"	kIx"
<g/>
MMA	MMA	kA
is	is	k?
designated	designated	k1gInSc1
P-	P-	k1gFnSc1
<g/>
8	#num#	k4
<g/>
A	A	kA
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
U.	U.	kA
<g/>
S.	S.	kA
Navy	Navy	k?
Naval	navalit	k5eAaPmRp2nS
Air	Air	k1gMnSc7
Systems	Systemsa	k1gFnPc2
Command	Command	k1gInSc1
(	(	kIx(
<g/>
NAVAIR	NAVAIR	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2005	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GAO-09-326SP	GAO-09-326SP	k1gFnSc2
Assessments	Assessmentsa	k1gFnPc2
of	of	k?
Major	major	k1gMnSc1
Weapon	Weapon	k1gMnSc1
Programs	Programs	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
GAO	GAO	kA
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Boeing	boeing	k1gInSc1
P-8A	P-8A	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
Aircraft	Aircraft	k1gMnSc1
T3	T3	k1gMnSc1
vstupuje	vstupovat	k5eAaImIp3nS
do	do	k7c2
letových	letový	k2eAgFnPc2d1
zkoušek	zkouška	k1gFnPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boeing	boeing	k1gInSc1
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2010	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
"	"	kIx"
<g/>
U.	U.	kA
<g/>
S.	S.	kA
Navy	Navy	k?
Boeing	boeing	k1gInSc1
P-8A	P-8A	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
launches	launches	k1gMnSc1
first	first	k1gMnSc1
sonobuoys	sonobuoys	k6eAd1
<g/>
"	"	kIx"
Seattle	Seattle	k1gFnSc1
Post-Intelligencer	Post-Intelligencra	k1gFnPc2
<g/>
,	,	kIx,
22	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2010	#num#	k4
<g/>
↑	↑	k?
INS	INS	kA
Vikramaditya	Vikramaditya	k1gMnSc1
completes	completes	k1gMnSc1
engine	enginout	k5eAaPmIp3nS
repairs	repairs	k6eAd1
and	and	k?
readies	readies	k1gMnSc1
for	forum	k1gNnPc2
sea	sea	k?
trials	trials	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naval-technology	Naval-technolog	k1gMnPc4
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
US	US	kA
Navy	Navy	k?
<g/>
'	'	kIx"
<g/>
s	s	k7c7
P-8A	P-8A	k1gFnSc7
Poseidon	Poseidon	k1gMnSc1
aircraft	aircraft	k1gMnSc1
achieves	achieves	k1gMnSc1
IOC	IOC	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naval-technology	Naval-technolog	k1gMnPc4
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
U.	U.	kA
<g/>
S.	S.	kA
Navy	Navy	k?
receives	receives	k1gInSc1
50	#num#	k4
<g/>
th	th	k?
P-8A	P-8A	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navaltoday	Navaltodaa	k1gFnPc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2017-01-06	2017-01-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gMnSc4
Hon	hon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dr	dr	kA
Brendan	Brendan	k1gMnSc1
Nelson	Nelson	k1gMnSc1
<g/>
,	,	kIx,
Minister	Minister	k1gMnSc1
for	forum	k1gNnPc2
Defence	Defence	k1gFnSc1
<g/>
:	:	kIx,
First	First	k1gFnSc1
Pass	Pass	k1gInSc1
Approval	Approval	k1gFnSc4
for	forum	k1gNnPc2
Orion	orion	k1gInSc1
Replacement	Replacement	k1gInSc1
<g/>
,	,	kIx,
tisková	tiskový	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
<g/>
,	,	kIx,
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Dostupné	dostupný	k2eAgFnSc2d1
on-line	on-lin	k1gInSc5
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
press	pressit	k5eAaPmRp2nS
release	release	k6eAd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
1	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Avalon	Avalon	k1gInSc1
2009	#num#	k4
<g/>
:	:	kIx,
Australia	Australium	k1gNnSc2
looks	looks	k6eAd1
set	set	k1gInSc4
to	ten	k3xDgNnSc1
join	join	k1gNnSc1
P-8	P-8	k1gFnSc4
programme	programit	k5eAaPmRp1nP
<g/>
,	,	kIx,
ASIA	ASIA	kA
PACIFIC	PACIFIC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jane	Jan	k1gMnSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
<g/>
,	,	kIx,
11	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2009	#num#	k4
<g/>
↑	↑	k?
Rozhovor	rozhovor	k1gInSc1
s	s	k7c7
velitelem	velitel	k1gMnSc7
RAAF	RAAF	kA
na	na	k7c6
webu	web	k1gInSc6
www.defensenews.com	www.defensenews.com	k1gInSc1
2	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2009	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
GROHMANN	GROHMANN	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
P-8A	P-8A	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
<g/>
:	:	kIx,
Australský	australský	k2eAgMnSc1d1
král	král	k1gMnSc1
moří	mořit	k5eAaImIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Armádní	armádní	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2014-03-03	2014-03-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
First	First	k1gMnSc1
RAAF	RAAF	kA
Poseidon	Poseidon	k1gMnSc1
Arrives	Arrives	k1gMnSc1
in	in	k?
Country	country	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
AirForces	AirForces	k1gMnSc1
Monthly	Monthly	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Leden	leden	k1gInSc1
2017	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
346	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
31	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
WATSON	WATSON	kA
<g/>
,	,	kIx,
Sean	Sean	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
First	First	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
off	off	k?
the	the	k?
rank	rank	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Air	Air	k1gFnSc1
Force	force	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Department	department	k1gInSc1
of	of	k?
Defence	Defence	k1gFnSc2
<g/>
,	,	kIx,
6	#num#	k4
October	October	k1gInSc1
2016	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
4	#num#	k4
October	October	k1gInSc1
2016	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Royal	Royal	k1gMnSc1
Australian	Australian	k1gMnSc1
Air	Air	k1gMnSc1
Forces	Forces	k1gMnSc1
receives	receives	k1gMnSc1
latest	latest	k1gMnSc1
P-8A	P-8A	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
aircraft	aircraft	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navyrecognition	Navyrecognition	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
2019-12-13	2019-12-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Australia	Australium	k1gNnSc2
orders	ordersa	k1gFnPc2
two	two	k?
more	mor	k1gInSc5
P-8A	P-8A	k1gFnSc2
Poseidon	Poseidon	k1gMnSc1
aircraft	aircraft	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navaltoday	Navaltodaa	k1gFnPc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2020-12-30	2020-12-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
India	indium	k1gNnSc2
eyes	eyesa	k1gFnPc2
$	$	kIx~
<g/>
2	#num#	k4
<g/>
b	b	k?
defence	defenec	k1gMnSc4
deal	deanout	k5eAaPmAgInS
with	with	k1gInSc1
US	US	kA
29	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2008	#num#	k4
<g/>
↑	↑	k?
PANDIT	pandit	k1gMnSc1
<g/>
,	,	kIx,
Rajat	Rajat	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
India	indium	k1gNnSc2
inks	inks	k6eAd1
largest-ever	largest-ever	k1gInSc1
defence	defence	k1gFnSc2
deal	deanout	k5eAaPmAgInS
with	with	k1gInSc1
US	US	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Times	Times	k1gMnSc1
of	of	k?
India	indium	k1gNnSc2
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Boeing	boeing	k1gInSc1
P-8I	P-8I	k1gMnSc1
Selected	Selected	k1gMnSc1
as	as	k1gNnSc2
Indian	Indiana	k1gFnPc2
Navy	Navy	k?
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Long-Range	Long-Range	k1gFnSc7
Maritime	Maritim	k1gInSc5
Patrol	Patrol	k?
Aircraft	Aircraft	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
19	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boeing	boeing	k1gInSc1
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Boeing	boeing	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
P-8I	P-8I	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
aircraft	aircraft	k1gMnSc1
begins	begins	k6eAd1
flight	flight	k2eAgInSc4d1
test	test	k1gInSc4
programme	programit	k5eAaPmRp1nP
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naval-technology	Naval-technolog	k1gMnPc4
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Indian	Indiana	k1gFnPc2
Navy	Navy	k?
takes	takes	k1gMnSc1
delivery	delivera	k1gFnSc2
of	of	k?
fourth	fourth	k1gInSc1
Boeing	boeing	k1gInSc1
P-8I	P-8I	k1gMnSc2
maritime	maritimat	k5eAaPmIp3nS
patrol	patrol	k?
aircraft	aircraft	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naval-technology	Naval-technolog	k1gMnPc4
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2014-05-28	2014-05-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
BURGESS	BURGESS	kA
<g/>
,	,	kIx,
Rick	Rick	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plane	planout	k5eAaImIp3nS
sailing	sailing	k1gInSc4
<g/>
:	:	kIx,
Boeing	boeing	k1gInSc4
<g/>
'	'	kIx"
<g/>
s	s	k7c7
P-8	P-8	k1gFnSc7
Poseidon	Poseidon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
AirForces	AirForces	k1gMnSc1
Monthly	Monthly	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Duben	duben	k1gInSc1
2016	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
337	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
34	#num#	k4
<g/>
-	-	kIx~
<g/>
40	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
955	#num#	k4
<g/>
-	-	kIx~
<g/>
7091	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Indian	Indiana	k1gFnPc2
Navy	Navy	k?
to	ten	k3xDgNnSc1
acquire	acquir	k1gInSc5
6	#num#	k4
more	mor	k1gInSc5
P-8I	P-8I	k1gMnPc1
maritime	maritimat	k5eAaPmIp3nS
aircraft	aircraft	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navyrecognition	Navyrecognition	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2019-12-02	2019-12-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
UK	UK	kA
MoD	MoD	k1gMnSc1
Confirmed	Confirmed	k1gMnSc1
Procurement	Procurement	k1gMnSc1
of	of	k?
Nine	Nin	k1gFnSc2
P-8A	P-8A	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
Maritime	Maritim	k1gInSc5
Patrol	Patrol	k?
Aircraft	Aircraft	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navyrecognition	Navyrecognition	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2016-07-18	2016-07-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
RAF	raf	k0
P-8A	P-8A	k1gMnSc1
Maritime	Maritim	k1gInSc5
Patrol	Patrol	k?
Aircraft	Aircraft	k2eAgMnSc1d1
to	ten	k3xDgNnSc1
be	be	k?
Known	Knowno	k1gNnPc2
as	as	k1gInSc1
'	'	kIx"
<g/>
Poseidon	Poseidon	k1gMnSc1
MRA	mřít	k5eAaImSgInS
Mk	Mk	k1gMnSc7
<g/>
.1	.1	k4
<g/>
'	'	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navyrecognition	Navyrecognition	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2018-08-21	2018-08-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
New	New	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
Maritime	Maritim	k1gInSc5
Patrol	Patrol	k?
Aircraft	Aircraft	k2eAgInSc1d1
lands	lands	k1gInSc1
in	in	k?
UK	UK	kA
for	forum	k1gNnPc2
</s>
<s>
first	first	k1gFnSc1
time	time	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
RAF	raf	k0
<g/>
,	,	kIx,
2020-02-04	2020-02-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
RAF	raf	k0
Poseidon	Poseidon	k1gMnSc1
MRA	mřít	k5eAaImSgInS
Mk	Mk	k1gMnSc2
<g/>
1	#num#	k4
MPA	MPA	kA
reaches	reaches	k1gMnSc1
Initial	Initial	k1gMnSc1
Operating	Operating	k1gInSc4
Capability	Capabilita	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navalnews	Navalnews	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2020-04-03	2020-04-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Norway	Norwaa	k1gFnSc2
to	ten	k3xDgNnSc1
Replace	Replace	k1gFnPc1
Ageing	Ageing	k1gInSc1
P-3	P-3	k1gFnSc2
MPA	MPA	kA
&	&	k?
DA-20	DA-20	k1gMnSc1
Falcon	Falcon	k1gMnSc1
EW	EW	kA
Aircraft	Aircraft	k1gMnSc1
with	with	k1gMnSc1
Five	Five	k1gFnPc2
Boeing	boeing	k1gInSc1
P-8A	P-8A	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navyrecognition	Navyrecognition	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2016-11-27	2016-11-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
US	US	kA
approves	approves	k1gInSc4
a	a	k8xC
$	$	kIx~
<g/>
1.75	1.75	k4
bn	bn	k?
FMS	FMS	kA
from	from	k1gMnSc1
Norway	Norwaa	k1gFnSc2
for	forum	k1gNnPc2
five	fivat	k5eAaPmIp3nS
P-8A	P-8A	k1gFnSc1
Poseidon	Poseidon	k1gMnSc1
MPA	MPA	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navyrecognition	Navyrecognition	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2016-12-22	2016-12-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Norway	Norwaa	k1gFnSc2
signs	signs	k6eAd1
contract	contract	k1gInSc4
for	forum	k1gNnPc2
five	fiv	k1gMnSc2
P-8A	P-8A	k1gMnSc2
Poseidon	Poseidon	k1gMnSc1
maritime	maritimat	k5eAaPmIp3nS
patrol	patrol	k?
aircraft	aircraft	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navaltoday	Navaltodaa	k1gFnPc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2017-03-30	2017-03-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
New	New	k1gFnSc1
Zealand	Zealand	k1gInSc1
Procuring	Procuring	k1gInSc1
Four	Foura	k1gFnPc2
Boeing	boeing	k1gInSc1
P-8A	P-8A	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
Maritime	Maritim	k1gInSc5
Patrol	Patrol	k?
Aircraft	Aircraft	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navyrecognition	Navyrecognition	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2018-07-09	2018-07-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
New	New	k1gFnPc4
MMA	MMA	kA
wingtips	wingtips	k6eAd1
combat	combat	k5eAaImF,k5eAaPmF
icing	icing	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
4	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flight	Flight	k1gMnSc1
International	International	k1gMnSc1
<g/>
,	,	kIx,
7	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2005	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
DIPLOMAT	diplomat	k1gMnSc1
<g/>
,	,	kIx,
Franz-Stefan	Franz-Stefan	k1gMnSc1
Gady	Gada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
India	indium	k1gNnSc2
Inducts	Inductsa	k1gFnPc2
First	First	k1gMnSc1
Squadron	Squadron	k1gMnSc1
of	of	k?
Anti-Submarine	Anti-Submarin	k1gInSc5
Warfare	Warfar	k1gMnSc5
Plane	planout	k5eAaImIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
DIMASCIO	DIMASCIO	kA
<g/>
,	,	kIx,
Jen	jen	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
India	indium	k1gNnSc2
Orders	Ordersa	k1gFnPc2
Four	Four	k1gMnSc1
More	mor	k1gInSc5
P-8I	P-8I	k1gFnPc3
Aircraft	Aircraft	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Defense	defense	k1gFnSc1
News	Newsa	k1gFnPc2
<g/>
,	,	kIx,
28	#num#	k4
July	Jula	k1gFnSc2
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
SACHDEVA	SACHDEVA	kA
<g/>
,	,	kIx,
Sam	Sam	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Govt	Govt	k2eAgInSc1d1
signs	signs	k1gInSc1
off	off	k?
$	$	kIx~
<g/>
2	#num#	k4
<g/>
b	b	k?
NZDF	NZDF	kA
plane	planout	k5eAaImIp3nS
deal	deal	k1gInSc4
by	by	kYmCp3nS
Sam	Sam	k1gMnSc1
Sachdeva	Sachdev	k1gMnSc2
-	-	kIx~
Newsroom	Newsroom	k1gInSc4
Pro	pro	k7c4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
9	#num#	k4
July	Jula	k1gFnSc2
2018	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Government	Government	k1gInSc1
<g/>
.	.	kIx.
<g/>
no	no	k9
<g/>
:	:	kIx,
Norway	Norwa	k2eAgFnPc4d1
has	hasit	k5eAaImRp2nS
<g />
.	.	kIx.
</s>
<s hack="1">
ordered	ordered	k1gInSc1
five	fivat	k5eAaPmIp3nS
Boeing	boeing	k1gInSc4
P-8A	P-8A	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
<g/>
,	,	kIx,
tisková	tiskový	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
<g/>
,	,	kIx,
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
5	#num#	k4
April	April	k1gInSc1
2017	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Dostupné	dostupný	k2eAgFnSc2d1
on-line	on-lin	k1gInSc5
<g/>
.	.	kIx.
<g/>
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
press	pressit	k5eAaPmRp2nS
release	release	k6eAd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Boeing	boeing	k1gInSc1
signs	signs	k1gInSc1
£	£	k?
<g/>
3	#num#	k4
<g/>
bn	bn	k?
deal	deal	k1gMnSc1
for	forum	k1gNnPc2
nine	nine	k6eAd1
marine	marinout	k5eAaPmIp3nS
patrol	patrol	k?
planes	planes	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
GAO-13-294SP	GAO-13-294SP	k1gMnPc1
DEFENSE	defense	k1gFnSc2
ACQUISITIONS	ACQUISITIONS	kA
Assessments	Assessments	k1gInSc1
of	of	k?
Selected	Selected	k1gInSc1
Weapon	Weapon	k1gMnSc1
Programs	Programs	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
US	US	kA
Government	Government	k1gMnSc1
Accountability	Accountabilita	k1gFnSc2
Office	Office	kA
<g/>
,	,	kIx,
March	March	k1gInSc1
2013	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
103	#num#	k4
<g/>
–	–	k?
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Moran	Morana	k1gFnPc2
<g/>
,	,	kIx,
Captain	Captain	k2eAgMnSc1d1
Michael	Michael	k1gMnSc1
T.	T.	kA
"	"	kIx"
<g/>
P-	P-	k1gMnSc1
<g/>
8	#num#	k4
<g/>
A	a	k8xC
Poseidon	Poseidon	k1gMnSc1
<g/>
.	.	kIx.
<g/>
"	"	kIx"
Archivováno	archivovat	k5eAaBmNgNnS
28	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
NAVAIR	NAVAIR	kA
–	–	k?
U.	U.	kA
<g/>
S.	S.	kA
Navy	Navy	k?
Naval	navalit	k5eAaPmRp2nS
Air	Air	k1gMnPc3
Systems	Systemsa	k1gFnPc2
Command	Command	k1gInSc4
–	–	k?
Navy	Navy	k?
and	and	k?
Marine	Marin	k1gInSc5
Corps	corps	k1gInSc4
Aviation	Aviation	k1gInSc1
Research	Research	k1gInSc1
<g/>
,	,	kIx,
Development	Development	k1gInSc1
<g/>
,	,	kIx,
Acquisition	Acquisition	k1gInSc1
<g/>
,	,	kIx,
Test	test	k1gInSc1
and	and	k?
Evaluation	Evaluation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retrieved	Retrieved	k1gInSc1
<g/>
:	:	kIx,
29	#num#	k4
August	August	k1gMnSc1
2012	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Navy	Navy	k?
accepts	accepts	k1gInSc1
its	its	k?
50	#num#	k4
<g/>
th	th	k?
P-8A	P-8A	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
UPI	UPI	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
U.	U.	kA
<g/>
S.	S.	kA
Navy	Navy	k?
officials	officials	k1gInSc1
accept	accepta	k1gFnPc2
delivery	delivera	k1gFnSc2
of	of	k?
50	#num#	k4
<g/>
th	th	k?
P-8A	P-8A	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
aircraft	aircraft	k1gMnSc1
–	–	k?
Military	Militara	k1gFnSc2
Embedded	Embedded	k1gMnSc1
Systems	Systems	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
P-8A	P-8A	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boeing	boeing	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
SOUŠEK	souška	k1gFnPc2
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
P-8	P-8	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
připraven	připravit	k5eAaPmNgMnS
k	k	k7c3
operačnímu	operační	k2eAgNnSc3d1
nasazení	nasazení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letectví	letectví	k1gNnSc1
a	a	k8xC
kosmonautika	kosmonautika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srpen	srpen	k1gInSc1
2013	#num#	k4
<g/>
,	,	kIx,
roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
92	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
8	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
50	#num#	k4
<g/>
-	-	kIx~
<g/>
54	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
0	#num#	k4
<g/>
0	#num#	k4
<g/>
24	#num#	k4
<g/>
-	-	kIx~
<g/>
1156	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Boeing	boeing	k1gInSc1
737	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Boeing	boeing	k1gInSc4
P-8	P-8	k1gMnSc1
Poseidon	Poseidon	k1gMnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Schematický	schematický	k2eAgInSc1d1
plánek	plánek	k1gInSc1
letounu	letoun	k1gInSc2
P-8A	P-8A	k1gFnSc4
s	s	k7c7
popisem	popis	k1gInSc7
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Údaje	údaj	k1gInPc1
o	o	k7c4
P-8	P-8	k1gFnSc4
na	na	k7c6
serveru	server	k1gInSc6
Navy	Navy	k?
<g/>
.	.	kIx.
<g/>
mil	míle	k1gFnPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
P-8	P-8	k4
na	na	k7c6
stránkách	stránka	k1gFnPc6
výrobce	výrobce	k1gMnSc1
Boeing	boeing	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Stránka	stránka	k1gFnSc1
o	o	k7c4
MMA	MMA	kA
na	na	k7c4
Globalsecurity	Globalsecurita	k1gFnPc4
<g/>
.	.	kIx.
<g/>
org	org	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Boeing	boeing	k1gInSc1
P-8A	P-8A	k1gFnSc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
Defense-Update	Defense-Updat	k1gInSc5
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Vojenská	vojenský	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
US	US	kA
Navy	Navy	k?
a	a	k8xC
USMC	USMC	kA
po	po	k7c6
roce	rok	k1gInSc6
1945	#num#	k4
Stíhací	stíhací	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
FH	FH	kA
•	•	k?
F2H	F2H	k1gMnSc1
•	•	k?
F3H	F3H	k1gMnSc1
•	•	k?
F-4	F-4	k1gMnSc1
•	•	k?
FR	fr	k0
•	•	k?
FJ	FJ	kA
•	•	k?
FJ-	FJ-	k1gMnSc1
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
-	-	kIx~
<g/>
3	#num#	k4
•	•	k?
F4D	F4D	k1gMnSc1
•	•	k?
F6U	F6U	k1gMnSc1
•	•	k?
F7U	F7U	k1gMnSc1
•	•	k?
F-8	F-8	k1gMnSc1
•	•	k?
F8F	F8F	k1gMnSc1
•	•	k?
F9F	F9F	k1gMnSc1
•	•	k?
F-9	F-9	k1gMnSc1
•	•	k?
F-10	F-10	k1gMnSc1
•	•	k?
F-11	F-11	k1gMnSc1
•	•	k?
F-14	F-14	k1gMnSc1
•	•	k?
F	F	kA
<g/>
/	/	kIx~
<g/>
A-	A-	k1gFnSc1
<g/>
18	#num#	k4
•	•	k?
F	F	kA
<g/>
/	/	kIx~
<g/>
A-	A-	k1gMnSc1
<g/>
18	#num#	k4
<g/>
E	E	kA
<g/>
/	/	kIx~
<g/>
F	F	kA
•	•	k?
F-35	F-35	k1gMnSc1
Bitevní	bitevní	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
AM	AM	kA
•	•	k?
A-1	A-1	k1gMnSc1
•	•	k?
A-2	A-2	k1gMnSc1
•	•	k?
A-3	A-3	k1gMnSc1
•	•	k?
A-4	A-4	k1gMnSc1
•	•	k?
A-5	A-5	k1gMnSc1
•	•	k?
A-6	A-6	k1gMnSc1
•	•	k?
A-7	A-7	k1gMnSc1
Hlídkové	hlídkový	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
S-2	S-2	k4
•	•	k?
S-3	S-3	k1gMnSc1
•	•	k?
P-2	P-2	k1gMnSc1
•	•	k?
P-3	P-3	k1gMnSc1
•	•	k?
P-5	P-5	k1gMnSc1
•	•	k?
P-8	P-8	k1gFnSc2
Průzkumné	průzkumný	k2eAgFnPc1d1
letounya	letounya	k6eAd1
letouny	letoun	k1gInPc7
pro	pro	k7c4
radioelektronický	radioelektronický	k2eAgInSc4d1
boj	boj	k1gInSc4
</s>
<s>
P4M	P4M	k4
•	•	k?
AF	AF	kA
•	•	k?
E-1	E-1	k1gMnSc1
•	•	k?
E-2	E-2	k1gMnSc1
•	•	k?
EP-3	EP-3	k1gMnSc1
•	•	k?
E-6	E-6	k1gMnSc1
•	•	k?
EA-6	EA-6	k1gMnSc1
•	•	k?
EA-18	EA-18	k1gMnSc1
•	•	k?
OE	OE	kA
<g/>
/	/	kIx~
<g/>
O-	O-	k1gFnSc1
<g/>
1	#num#	k4
•	•	k?
OV-10	OV-10	k1gMnSc1
Bitevní	bitevní	k2eAgInPc4d1
letouny	letoun	k1gInPc4
se	s	k7c7
svislým	svislý	k2eAgInSc7d1
startem	start	k1gInSc7
a	a	k8xC
přistáním	přistání	k1gNnSc7
</s>
<s>
AV-8A	AV-8A	k4
•	•	k?
AV-8B	AV-8B	k1gMnSc1
Transportní	transportní	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
JRM	JRM	kA
•	•	k?
JR2F	JR2F	k1gFnSc2
•	•	k?
RM	RM	kA
•	•	k?
R3Y	R3Y	k1gMnSc1
•	•	k?
R4Q	R4Q	k1gMnSc1
•	•	k?
R4Y	R4Y	k1gMnSc1
•	•	k?
R5D	R5D	k1gMnSc1
•	•	k?
R6D	R6D	k1gMnSc1
•	•	k?
R	R	kA
<g/>
7	#num#	k4
<g/>
V-	V-	k1gFnSc1
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
-	-	kIx~
<g/>
2	#num#	k4
•	•	k?
UC	UC	kA
•	•	k?
UO	UO	kA
•	•	k?
C-1	C-1	k1gMnSc1
•	•	k?
C-2	C-2	k1gMnSc1
•	•	k?
C-4	C-4	k1gMnSc1
•	•	k?
C-12	C-12	k1gMnSc1
•	•	k?
C-20	C-20	k1gMnSc1
•	•	k?
C-21	C-21	k1gMnSc1
•	•	k?
C-26	C-26	k1gMnSc1
•	•	k?
C-28	C-28	k1gMnSc1
•	•	k?
C-	C-	k1gMnSc1
<g/>
37	#num#	k4
<g/>
A	A	kA
<g/>
/	/	kIx~
<g/>
B	B	kA
•	•	k?
C-	C-	k1gMnSc1
<g/>
130	#num#	k4
<g/>
/	/	kIx~
<g/>
J	J	kA
•	•	k?
C-880	C-880	k1gMnSc1
•	•	k?
V-22	V-22	k1gMnSc1
Cvičné	cvičný	k2eAgInPc4d1
letouny	letoun	k1gInPc4
</s>
<s>
T-2	T-2	k4
•	•	k?
T-6	T-6	k1gMnSc1
•	•	k?
T-28	T-28	k1gMnSc1
•	•	k?
T-34	T-34	k1gMnSc1
•	•	k?
T-38	T-38	k1gMnSc1
•	•	k?
T-39	T-39	k1gMnSc1
•	•	k?
T-44	T-44	k1gMnSc1
•	•	k?
T-45	T-45	k1gMnSc1
•	•	k?
EC-24	EC-24	k1gFnSc1
Vrtulníky	vrtulník	k1gInPc4
</s>
<s>
HNS	HNS	kA
•	•	k?
HTL	HTL	kA
<g/>
/	/	kIx~
<g/>
HUL	houlit	k5eAaImRp2nS
•	•	k?
HOK	HOK	kA
<g/>
/	/	kIx~
<g/>
HTK	HTK	kA
<g/>
/	/	kIx~
<g/>
HUK	huk	k1gInSc1
•	•	k?
HOS	HOS	kA
<g/>
/	/	kIx~
<g/>
HO	on	k3xPp3gMnSc4
<g/>
2	#num#	k4
<g/>
S	s	k7c7
•	•	k?
HO3S	HO3S	k1gMnSc1
•	•	k?
HO4S	HO4S	k1gMnSc1
•	•	k?
HO5S	HO5S	k1gMnSc1
•	•	k?
HUS	Hus	k1gMnSc1
<g/>
/	/	kIx~
<g/>
HSS-	HSS-	k1gFnSc1
<g/>
1	#num#	k4
•	•	k?
AH-1G	AH-1G	k1gMnSc1
•	•	k?
AH-	AH-	k1gMnSc1
<g/>
1	#num#	k4
<g/>
J	J	kA
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
T	T	kA
<g/>
/	/	kIx~
<g/>
W	W	kA
•	•	k?
AH-1Z	AH-1Z	k1gMnSc1
•	•	k?
UH-1	UH-1	k1gMnSc1
•	•	k?
UH-1N	UH-1N	k1gMnSc1
•	•	k?
UH-1Y	UH-1Y	k1gMnSc1
•	•	k?
SH-2	SH-2	k1gMnSc1
•	•	k?
SH-2G	SH-2G	k1gMnSc1
•	•	k?
SH-3	SH-3	k1gMnSc1
•	•	k?
CH-37	CH-37	k1gMnSc1
•	•	k?
CH-46	CH-46	k1gMnSc1
•	•	k?
CH-53	CH-53	k1gMnSc1
•	•	k?
CH-53E	CH-53E	k1gMnSc1
•	•	k?
CH-53K	CH-53K	k1gMnSc1
•	•	k?
TH-	TH-	k1gMnSc1
<g/>
57	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
•	•	k?
SH-	SH-	k1gMnSc1
<g/>
60	#num#	k4
<g/>
/	/	kIx~
<g/>
MH-	MH-	k1gFnPc2
<g/>
60	#num#	k4
Bezpilotní	bezpilotní	k2eAgFnSc1d1
letadla	letadlo	k1gNnPc1
</s>
<s>
DSN	DSN	kA
•	•	k?
KDA	KDA	kA
•	•	k?
RQ-2	RQ-2	k1gMnSc1
•	•	k?
MQ-4C	MQ-4C	k1gMnSc1
•	•	k?
RQ-7	RQ-7	k1gMnSc1
•	•	k?
MQ-	MQ-	k1gMnSc1
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
C	C	kA
•	•	k?
RQ-11	RQ-11	k1gMnSc1
•	•	k?
RQ-12	RQ-12	k1gMnSc1
•	•	k?
RQ-14	RQ-14	k1gMnSc1
•	•	k?
RQ-15	RQ-15	k1gMnSc1
•	•	k?
RQ-20	RQ-20	k1gMnSc1
•	•	k?
RQ-21	RQ-21	k1gMnSc1
•	•	k?
CQ-24	CQ-24	k1gMnSc1
•	•	k?
RQ-27	RQ-27	k1gFnSc1
Prototypy	prototyp	k1gInPc4
</s>
<s>
A2D	A2D	k4
•	•	k?
XFY	XFY	kA
•	•	k?
F2Y	F2Y	k1gMnSc1
•	•	k?
XF2R	XF2R	k1gMnSc1
•	•	k?
F5D	F5D	k1gMnSc1
•	•	k?
XF-8U	XF-8U	k1gMnSc1
•	•	k?
XF10F	XF10F	k1gMnSc1
•	•	k?
F11F-1F	F11F-1F	k1gMnSc1
•	•	k?
YF-17	YF-17	k1gMnSc1
•	•	k?
XP5Y	XP5Y	k1gMnSc1
•	•	k?
P6M	P6M	k1gMnSc1
•	•	k?
R6V	R6V	k1gMnSc1
•	•	k?
X-32	X-32	k1gMnSc1
•	•	k?
XHJH	XHJH	kA
•	•	k?
XMQ-17	XMQ-17	k1gMnSc1
•	•	k?
YMQ-18	YMQ-18	k1gMnSc1
•	•	k?
MQ-25	MQ-25	k1gMnSc1
</s>
<s>
Americké	americký	k2eAgInPc1d1
námořní	námořní	k2eAgInPc1d1
hlídkové	hlídkový	k2eAgInPc1d1
letouny	letoun	k1gInPc1
po	po	k7c6
roce	rok	k1gInSc6
1962	#num#	k4
</s>
<s>
P-1	P-1	k4
•	•	k?
P-2	P-2	k1gMnSc1
•	•	k?
P-3	P-3	k1gMnSc1
•	•	k?
P-4	P-4	k1gMnSc1
•	•	k?
P-5	P-5	k1gMnSc1
•	•	k?
P-6	P-6	k1gMnSc1
•	•	k?
P-7	P-7	k1gMnSc1
•	•	k?
P-8	P-8	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
</s>
