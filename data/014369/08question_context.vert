<s>
Kraje	kraj	k1gInPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
jsou	být	k5eAaImIp3nP
vyšší	vysoký	k2eAgInPc1d2
územní	územní	k2eAgInPc1d1
samosprávné	samosprávný	k2eAgInPc1d1
celky	celek	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
územními	územní	k2eAgNnPc7d1
společenstvími	společenství	k1gNnPc7
občanů	občan	k1gMnPc2
s	s	k7c7
právem	právo	k1gNnSc7
na	na	k7c4
samosprávu	samospráva	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vytvořeny	vytvořen	k2eAgFnPc1d1
byly	být	k5eAaImAgFnP
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
ústavním	ústavní	k2eAgInSc7d1
zákonem	zákon	k1gInSc7
o	o	k7c4
vytvoření	vytvoření	k1gNnSc4
vyšších	vysoký	k2eAgInPc2d2
územních	územní	k2eAgInPc2d1
samosprávných	samosprávný	k2eAgInPc2d1
celků	celek	k1gInPc2
<g/>
,	,	kIx,
samosprávné	samosprávný	k2eAgFnPc1d1
kompetence	kompetence	k1gFnPc1
získaly	získat	k5eAaPmAgFnP
na	na	k7c6
základě	základ	k1gInSc6
zákona	zákon	k1gInSc2
o	o	k7c6
krajích	kraj	k1gInPc6
<g/>
.	.	kIx.
</s>