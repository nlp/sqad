<s>
Kraje	kraj	k1gInPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Přehled	přehled	k1gInSc1
krajů	kraj	k1gInPc2
Česka	Česko	k1gNnSc2
a	a	k8xC
jejich	jejich	k3xOp3gInPc1
znaky	znak	k1gInPc1
</s>
<s>
Kraje	kraj	k1gInPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
jsou	být	k5eAaImIp3nP
vyšší	vysoký	k2eAgInPc1d2
územní	územní	k2eAgInPc1d1
samosprávné	samosprávný	k2eAgInPc1d1
celky	celek	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
územními	územní	k2eAgNnPc7d1
společenstvími	společenství	k1gNnPc7
občanů	občan	k1gMnPc2
s	s	k7c7
právem	právo	k1gNnSc7
na	na	k7c4
samosprávu	samospráva	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vytvořeny	vytvořen	k2eAgFnPc1d1
byly	být	k5eAaImAgFnP
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
ústavním	ústavní	k2eAgInSc7d1
zákonem	zákon	k1gInSc7
o	o	k7c4
vytvoření	vytvoření	k1gNnSc4
vyšších	vysoký	k2eAgInPc2d2
územních	územní	k2eAgInPc2d1
samosprávných	samosprávný	k2eAgInPc2d1
celků	celek	k1gInPc2
<g/>
,	,	kIx,
samosprávné	samosprávný	k2eAgFnPc1d1
kompetence	kompetence	k1gFnPc1
získaly	získat	k5eAaPmAgFnP
na	na	k7c6
základě	základ	k1gInSc6
zákona	zákon	k1gInSc2
o	o	k7c6
krajích	kraj	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krajů	kraj	k1gInPc2
je	být	k5eAaImIp3nS
celkem	celkem	k6eAd1
13	#num#	k4
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
je	být	k5eAaImIp3nS
samostatným	samostatný	k2eAgInSc7d1
celkem	celek	k1gInSc7
<g/>
,	,	kIx,
fakticky	fakticky	k6eAd1
ale	ale	k8xC
s	s	k7c7
postavením	postavení	k1gNnSc7
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každé	každý	k3xTgInPc4
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
probíhají	probíhat	k5eAaImIp3nP
volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
krajů	kraj	k1gInPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
pak	pak	k6eAd1
vychází	vycházet	k5eAaImIp3nS
krajská	krajský	k2eAgFnSc1d1
rada	rada	k1gFnSc1
a	a	k8xC
hejtman	hejtman	k1gMnSc1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správu	správa	k1gFnSc4
kraje	kraj	k1gInSc2
vykonává	vykonávat	k5eAaImIp3nS
krajský	krajský	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Historie	historie	k1gFnSc2
krajů	kraj	k1gInPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Krajské	krajský	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
ve	v	k7c4
středověkuDvanáct	středověkuDvanáct	k1gInSc4
českých	český	k2eAgInPc2d1
krajů	kraj	k1gInPc2
vytvořil	vytvořit	k5eAaPmAgMnS
v	v	k7c6
polovině	polovina	k1gFnSc6
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
Přemysl	Přemysl	k1gMnSc1
Otakar	Otakar	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
nahradil	nahradit	k5eAaPmAgInS
jimi	on	k3xPp3gInPc7
původní	původní	k2eAgFnSc7d1
zřízení	zřízení	k1gNnSc4
hradské	hradská	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
Moravě	Morava	k1gFnSc6
existovaly	existovat	k5eAaImAgInP
předtím	předtím	k6eAd1
úděly	úděl	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
území	území	k1gNnSc6
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
představovaly	představovat	k5eAaImAgInP
kraje	kraj	k1gInPc1
od	od	k7c2
středověku	středověk	k1gInSc2
až	až	k6eAd1
do	do	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byly	být	k5eAaImAgInP
původní	původní	k2eAgInPc1d1
kraje	kraj	k1gInPc1
zrušeny	zrušen	k2eAgInPc1d1
<g/>
,	,	kIx,
správní	správní	k2eAgInPc1d1
celky	celek	k1gInPc1
stojící	stojící	k2eAgInPc1d1
mezi	mezi	k7c7
panstvími	panství	k1gNnPc7
a	a	k8xC
zeměmi	zem	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
vlády	vláda	k1gFnSc2
krále	král	k1gMnSc2
Jiřího	Jiří	k1gMnSc2
z	z	k7c2
Poděbrad	Poděbrady	k1gInPc2
do	do	k7c2
roku	rok	k1gInSc2
1714	#num#	k4
existovalo	existovat	k5eAaImAgNnS
v	v	k7c6
Čechách	Čechy	k1gFnPc6
krajů	kraj	k1gInPc2
14	#num#	k4
<g/>
,	,	kIx,
poté	poté	k6eAd1
došlo	dojít	k5eAaPmAgNnS
opět	opět	k6eAd1
k	k	k7c3
redukci	redukce	k1gFnSc3
na	na	k7c4
12	#num#	k4
krajů	kraj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
vzniku	vznik	k1gInSc6
Československa	Československo	k1gNnSc2
měly	mít	k5eAaImAgFnP
být	být	k5eAaImF
zavedeny	zaveden	k2eAgFnPc4d1
župy	župa	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
zemské	zemský	k2eAgNnSc1d1
zřízení	zřízení	k1gNnSc1
přetrvalo	přetrvat	k5eAaPmAgNnS
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1948	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tehdy	tehdy	k6eAd1
bylo	být	k5eAaImAgNnS
zákonem	zákon	k1gInSc7
o	o	k7c6
krajském	krajský	k2eAgNnSc6d1
zřízení	zřízení	k1gNnSc6
území	území	k1gNnSc2
Československa	Československo	k1gNnSc2
rozděleno	rozdělit	k5eAaPmNgNnS
na	na	k7c6
19	#num#	k4
„	„	k?
<g/>
malých	malý	k2eAgInPc2d1
<g/>
“	“	k?
krajů	kraj	k1gInPc2
(	(	kIx(
<g/>
13	#num#	k4
v	v	k7c6
české	český	k2eAgFnSc6d1
části	část	k1gFnSc6
státu	stát	k1gInSc2
a	a	k8xC
6	#num#	k4
na	na	k7c6
Slovensku	Slovensko	k1gNnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
byly	být	k5eAaImAgFnP
zrušeny	zrušit	k5eAaPmNgFnP
země	zem	k1gFnPc1
jako	jako	k8xC,k8xS
správní	správní	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
druhé	druhý	k4xOgNnSc4
(	(	kIx(
<g/>
po	po	k7c6
Protektorátu	protektorát	k1gInSc6
<g/>
)	)	kIx)
územně	územně	k6eAd1
správní	správní	k2eAgNnSc4d1
členění	členění	k1gNnSc4
v	v	k7c6
dějinách	dějiny	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
nerespektovalo	respektovat	k5eNaImAgNnS
historické	historický	k2eAgFnPc4d1
zemské	zemský	k2eAgFnPc4d1
hranice	hranice	k1gFnPc4
<g/>
,	,	kIx,
kterýžto	kterýžto	k?
stav	stav	k1gInSc1
se	se	k3xPyFc4
už	už	k6eAd1
nezměnil	změnit	k5eNaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počet	počet	k1gInSc1
13	#num#	k4
krajů	kraj	k1gInPc2
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gNnPc1
sídelní	sídelní	k2eAgNnPc1d1
města	město	k1gNnPc1
a	a	k8xC
částečně	částečně	k6eAd1
i	i	k9
rozloha	rozloha	k1gFnSc1
odpovídaly	odpovídat	k5eAaImAgFnP
současným	současný	k2eAgMnPc3d1
krajům	kraj	k1gInPc3
v	v	k7c6
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1960	#num#	k4
byly	být	k5eAaImAgFnP
nahrazeny	nahradit	k5eAaPmNgInP
většími	veliký	k2eAgInPc7d2
kraji	kraj	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
„	„	k?
<g/>
Velké	velký	k2eAgFnSc2d1
<g/>
“	“	k?
kraje	kraj	k1gInSc2
1960	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
</s>
<s>
Dřívější	dřívější	k2eAgInPc1d1
české	český	k2eAgInPc1d1
územní	územní	k2eAgInPc1d1
kraje	kraj	k1gInPc1
a	a	k8xC
jejich	jejich	k3xOp3gNnSc2
členění	členění	k1gNnSc2
na	na	k7c4
okresy	okres	k1gInPc4
</s>
<s>
Roku	rok	k1gInSc2
1960	#num#	k4
zákonem	zákon	k1gInSc7
o	o	k7c6
územním	územní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
vzniklo	vzniknout	k5eAaPmAgNnS
v	v	k7c6
Československu	Československo	k1gNnSc6
10	#num#	k4
(	(	kIx(
<g/>
z	z	k7c2
toho	ten	k3xDgNnSc2
7	#num#	k4
v	v	k7c6
české	český	k2eAgFnSc6d1
části	část	k1gFnSc6
<g/>
)	)	kIx)
větších	veliký	k2eAgInPc2d2
krajů	kraj	k1gInPc2
<g/>
,	,	kIx,
mimo	mimo	k7c4
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
Prahu	Praha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Orgány	orgán	k1gInPc1
nových	nový	k2eAgInPc2d1
„	„	k?
<g/>
velkých	velký	k2eAgInPc2d1
<g/>
“	“	k?
krajů	kraj	k1gInPc2
(	(	kIx(
<g/>
krajské	krajský	k2eAgInPc1d1
národní	národní	k2eAgInPc1d1
výbory	výbor	k1gInPc1
<g/>
)	)	kIx)
byly	být	k5eAaImAgInP
ustaveny	ustavit	k5eAaPmNgInP
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
byly	být	k5eAaImAgFnP
ale	ale	k9
zrušeny	zrušen	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Názvy	název	k1gInPc4
a	a	k8xC
sídla	sídlo	k1gNnPc4
tehdejších	tehdejší	k2eAgInPc2d1
krajů	kraj	k1gInPc2
na	na	k7c6
českém	český	k2eAgNnSc6d1
území	území	k1gNnSc6
(	(	kIx(
<g/>
samostatnou	samostatný	k2eAgFnSc7d1
územní	územní	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
z	z	k7c2
některých	některý	k3yIgNnPc2
hledisek	hledisko	k1gNnPc2
byla	být	k5eAaImAgFnS
postavena	postaven	k2eAgFnSc1d1
na	na	k7c4
roveň	roveň	k1gFnSc4
těmto	tento	k3xDgInPc3
krajům	kraj	k1gInPc3
<g/>
,	,	kIx,
bylo	být	k5eAaImAgNnS
území	území	k1gNnSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Jihočeský	jihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Českých	český	k2eAgInPc6d1
Budějovicích	Budějovice	k1gInPc6
</s>
<s>
Západočeský	západočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
</s>
<s>
Severočeský	severočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Ústí	ústí	k1gNnSc6
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
Východočeský	východočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Hradci	Hradec	k1gInSc6
Králové	Králová	k1gFnSc2
</s>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Brně	Brno	k1gNnSc6
</s>
<s>
Severomoravský	severomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
se	s	k7c7
sídlem	sídlo	k1gNnSc7
v	v	k7c6
Ostravě	Ostrava	k1gFnSc6
</s>
<s>
Jelikož	jelikož	k8xS
roku	rok	k1gInSc2
2000	#num#	k4
vznikly	vzniknout	k5eAaPmAgInP
nové	nový	k2eAgInPc1d1
samosprávné	samosprávný	k2eAgInPc1d1
kraje	kraj	k1gInPc1
<g/>
,	,	kIx,
existovaly	existovat	k5eAaImAgInP
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
dvě	dva	k4xCgFnPc4
různé	různý	k2eAgFnPc4d1
soustavy	soustava	k1gFnPc4
krajů	kraj	k1gInPc2
několik	několik	k4yIc4
let	léto	k1gNnPc2
vedle	vedle	k6eAd1
sebe	sebe	k3xPyFc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Definitivně	definitivně	k6eAd1
byly	být	k5eAaImAgInP
staré	starý	k2eAgInPc1d1
územní	územní	k2eAgInPc1d1
kraje	kraj	k1gInPc1
zrušeny	zrušen	k2eAgInPc1d1
zákonem	zákon	k1gInSc7
o	o	k7c6
územně	územně	k6eAd1
správním	správní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
ke	k	k7c3
dni	den	k1gInSc3
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2021	#num#	k4
(	(	kIx(
<g/>
krajských	krajský	k2eAgInPc2d1
soudů	soud	k1gInPc2
a	a	k8xC
krajských	krajský	k2eAgNnPc2d1
státních	státní	k2eAgNnPc2d1
zastupitelství	zastupitelství	k1gNnSc2
se	se	k3xPyFc4
však	však	k9
reforma	reforma	k1gFnSc1
nedotkla	dotknout	k5eNaPmAgFnS
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Samosprávné	samosprávný	k2eAgInPc1d1
kraje	kraj	k1gInPc1
od	od	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
</s>
<s>
Kraje	kraj	k1gInPc1
Česka	Česko	k1gNnSc2
</s>
<s>
Systém	systém	k1gInSc1
samosprávných	samosprávný	k2eAgInPc2d1
krajů	kraj	k1gInPc2
a	a	k8xC
obvodů	obvod	k1gInPc2
obcí	obec	k1gFnPc2
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
Česka	Česko	k1gNnSc2
</s>
<s>
Vytvoření	vytvoření	k1gNnSc1
vyšších	vysoký	k2eAgInPc2d2
územně	územně	k6eAd1
samosprávných	samosprávný	k2eAgInPc2d1
celků	celek	k1gInPc2
předpokládá	předpokládat	k5eAaImIp3nS
článek	článek	k1gInSc1
99	#num#	k4
Ústavy	ústava	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kraje	kraj	k1gInSc2
byly	být	k5eAaImAgInP
vytvořeny	vytvořen	k2eAgInPc1d1
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2000	#num#	k4
ústavním	ústavní	k2eAgInSc7d1
zákonem	zákon	k1gInSc7
o	o	k7c4
vytvoření	vytvoření	k1gNnSc4
vyšších	vysoký	k2eAgInPc2d2
územních	územní	k2eAgInPc2d1
samosprávných	samosprávný	k2eAgInPc2d1
celků	celek	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
stanoví	stanovit	k5eAaPmIp3nS
názvy	název	k1gInPc4
a	a	k8xC
sídla	sídlo	k1gNnPc4
krajů	kraj	k1gInPc2
a	a	k8xC
vymezil	vymezit	k5eAaPmAgMnS
jejich	jejich	k3xOp3gInPc4
územní	územní	k2eAgInPc4d1
obvody	obvod	k1gInPc4
výčtem	výčet	k1gInSc7
okresů	okres	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
kraje	kraj	k1gInPc1
zhruba	zhruba	k6eAd1
kopírují	kopírovat	k5eAaImIp3nP
kraje	kraj	k1gInPc1
z	z	k7c2
let	léto	k1gNnPc2
1948	#num#	k4
<g/>
–	–	k?
<g/>
1960	#num#	k4
<g/>
,	,	kIx,
zároveň	zároveň	k6eAd1
jsou	být	k5eAaImIp3nP
české	český	k2eAgInPc1d1
NUTS	NUTS	kA
3	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Samosprávné	samosprávný	k2eAgFnPc1d1
kompetence	kompetence	k1gFnPc1
získaly	získat	k5eAaPmAgFnP
na	na	k7c6
základě	základ	k1gInSc6
zákona	zákon	k1gInSc2
o	o	k7c6
krajích	kraj	k1gInPc6
(	(	kIx(
<g/>
krajské	krajský	k2eAgNnSc4d1
zřízení	zřízení	k1gNnSc4
<g/>
)	)	kIx)
dne	den	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2000	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
proběhly	proběhnout	k5eAaPmAgFnP
první	první	k4xOgFnPc1
volby	volba	k1gFnPc1
do	do	k7c2
jejich	jejich	k3xOp3gNnPc2
nově	nově	k6eAd1
zřízených	zřízený	k2eAgNnPc2d1
zastupitelstev	zastupitelstvo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
Prahu	práh	k1gInSc2
se	se	k3xPyFc4
ale	ale	k9
zákon	zákon	k1gInSc1
o	o	k7c6
krajích	kraj	k1gInPc6
nevztahuje	vztahovat	k5eNaImIp3nS
<g/>
,	,	kIx,
Praze	Praha	k1gFnSc6
přiznává	přiznávat	k5eAaImIp3nS
postavení	postavení	k1gNnSc4
a	a	k8xC
pravomoci	pravomoc	k1gFnPc4
obce	obec	k1gFnSc2
a	a	k8xC
zároveň	zároveň	k6eAd1
kraje	kraj	k1gInPc1
zákon	zákon	k1gInSc1
č.	č.	k?
131	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
hlavním	hlavní	k2eAgNnSc6d1
městě	město	k1gNnSc6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volby	volba	k1gFnSc2
do	do	k7c2
krajských	krajský	k2eAgNnPc2d1
zastupitelstev	zastupitelstvo	k1gNnPc2
se	se	k3xPyFc4
pak	pak	k6eAd1
konají	konat	k5eAaImIp3nP
podle	podle	k7c2
zákona	zákon	k1gInSc2
č.	č.	k?
130	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
Volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstva	zastupitelstvo	k1gNnSc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
se	se	k3xPyFc4
konají	konat	k5eAaImIp3nP
v	v	k7c6
termínech	termín	k1gInPc6
obecních	obecní	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
a	a	k8xC
podle	podle	k7c2
jejich	jejich	k3xOp3gInSc2
volebního	volební	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Agenda	agenda	k1gFnSc1
krajů	kraj	k1gInPc2
byla	být	k5eAaImAgFnS
významně	významně	k6eAd1
rozšířena	rozšířit	k5eAaPmNgFnS
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc3
2003	#num#	k4
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
se	s	k7c7
zrušením	zrušení	k1gNnSc7
okresních	okresní	k2eAgInPc2d1
úřadů	úřad	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Dvakrát	dvakrát	k6eAd1
byly	být	k5eAaImAgInP
některé	některý	k3yIgInPc1
z	z	k7c2
krajů	kraj	k1gInPc2
přejmenovány	přejmenován	k2eAgFnPc1d1
<g/>
:	:	kIx,
poprvé	poprvé	k6eAd1
byl	být	k5eAaImAgInS
k	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
květnu	květen	k1gInSc3
2001	#num#	k4
(	(	kIx(
<g/>
dnem	den	k1gInSc7
vyhlášení	vyhlášení	k1gNnSc2
<g/>
)	)	kIx)
novelizačním	novelizační	k2eAgInSc7d1
ústavním	ústavní	k2eAgInSc7d1
zákonem	zákon	k1gInSc7
č.	č.	k?
176	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
přejmenován	přejmenován	k2eAgInSc1d1
Budějovický	budějovický	k2eAgInSc1d1
kraj	kraj	k1gInSc1
na	na	k7c4
Jihočeský	jihočeský	k2eAgInSc4d1
kraj	kraj	k1gInSc4
<g/>
,	,	kIx,
Jihlavský	jihlavský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
na	na	k7c4
Vysočinu	vysočina	k1gFnSc4
<g/>
,	,	kIx,
Brněnský	brněnský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
na	na	k7c4
Jihomoravský	jihomoravský	k2eAgInSc4d1
kraj	kraj	k1gInSc4
a	a	k8xC
Ostravský	ostravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
na	na	k7c4
Moravskoslezský	moravskoslezský	k2eAgInSc4d1
kraj	kraj	k1gInSc4
<g/>
,	,	kIx,
podruhé	podruhé	k6eAd1
pak	pak	k6eAd1
ústavním	ústavní	k2eAgInSc7d1
zákonem	zákon	k1gInSc7
č.	č.	k?
135	#num#	k4
<g/>
/	/	kIx~
<g/>
2011	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
srpnu	srpen	k1gInSc6
2011	#num#	k4
Vysočina	vysočina	k1gFnSc1
na	na	k7c4
Kraj	kraj	k1gInSc4
Vysočina	vysočina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Zákonem	zákon	k1gInSc7
č.	č.	k?
387	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
bylo	být	k5eAaImAgNnS
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc6
2005	#num#	k4
přesunuto	přesunout	k5eAaPmNgNnS
25	#num#	k4
obcí	obec	k1gFnPc2
z	z	k7c2
kraje	kraj	k1gInSc2
Vysočina	vysočina	k1gFnSc1
do	do	k7c2
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
a	a	k8xC
tři	tři	k4xCgFnPc1
obce	obec	k1gFnPc1
(	(	kIx(
<g/>
Huzová	Huzová	k1gFnSc1
<g/>
,	,	kIx,
Moravský	moravský	k2eAgInSc1d1
Beroun	Beroun	k1gInSc1
<g/>
,	,	kIx,
Norberčany	Norberčan	k1gMnPc4
<g/>
)	)	kIx)
z	z	k7c2
Moravskoslezského	moravskoslezský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
do	do	k7c2
Olomouckého	olomoucký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1
změna	změna	k1gFnSc1
hranic	hranice	k1gFnPc2
krajů	kraj	k1gInPc2
proběhla	proběhnout	k5eAaPmAgFnS
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
se	s	k7c7
zmenšením	zmenšení	k1gNnSc7
území	území	k1gNnSc2
vojenských	vojenský	k2eAgInPc2d1
újezdů	újezd	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
vyčleněná	vyčleněný	k2eAgNnPc1d1
katastrální	katastrální	k2eAgNnPc1d1
území	území	k1gNnPc1
tak	tak	k8xC,k8xS
zákonem	zákon	k1gInSc7
č.	č.	k?
15	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
přešla	přejít	k5eAaPmAgFnS
z	z	k7c2
Karlovarského	karlovarský	k2eAgMnSc2d1
do	do	k7c2
Ústeckého	ústecký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
,	,	kIx,
z	z	k7c2
Jihomoravského	jihomoravský	k2eAgInSc2d1
do	do	k7c2
Olomouckého	olomoucký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
a	a	k8xC
naopak	naopak	k6eAd1
z	z	k7c2
Olomouckého	olomoucký	k2eAgMnSc2d1
do	do	k7c2
Moravskoslezského	moravskoslezský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Novému	nový	k2eAgNnSc3d1
krajskému	krajský	k2eAgNnSc3d1
uspořádání	uspořádání	k1gNnSc3
se	se	k3xPyFc4
postupně	postupně	k6eAd1
a	a	k8xC
částečně	částečně	k6eAd1
přizpůsobila	přizpůsobit	k5eAaPmAgFnS
i	i	k9
veřejná	veřejný	k2eAgFnSc1d1
správa	správa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
dosavadních	dosavadní	k2eAgInPc2d1
osm	osm	k4xCc4
krajských	krajský	k2eAgNnPc2d1
ředitelství	ředitelství	k1gNnPc2
policie	policie	k1gFnSc2
se	se	k3xPyFc4
dne	den	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
změnilo	změnit	k5eAaPmAgNnS
na	na	k7c4
14	#num#	k4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
odpovídají	odpovídat	k5eAaImIp3nP
územnímu	územní	k2eAgNnSc3d1
správnímu	správní	k2eAgNnSc3d1
členění	členění	k1gNnSc3
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Pouze	pouze	k6eAd1
organizace	organizace	k1gFnSc1
krajských	krajský	k2eAgMnPc2d1
soudů	soud	k1gInPc2
a	a	k8xC
krajských	krajský	k2eAgNnPc2d1
státních	státní	k2eAgNnPc2d1
zastupitelství	zastupitelství	k1gNnPc2
stále	stále	k6eAd1
odpovídá	odpovídat	k5eAaImIp3nS
dřívějšímu	dřívější	k2eAgInSc3d1
stavu	stav	k1gInSc3
<g/>
,	,	kIx,
v	v	k7c6
nově	nově	k6eAd1
ustavených	ustavený	k2eAgNnPc6d1
krajských	krajský	k2eAgNnPc6d1
městech	město	k1gNnPc6
(	(	kIx(
<g/>
vyjma	vyjma	k7c2
Karlových	Karlův	k2eAgInPc2d1
Varů	Vary	k1gInPc2
a	a	k8xC
v	v	k7c6
případě	případ	k1gInSc6
státního	státní	k2eAgNnSc2d1
zastupitelství	zastupitelství	k1gNnSc2
i	i	k8xC
Pardubic	Pardubice	k1gInPc2
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
ale	ale	k8xC
jejich	jejich	k3xOp3gFnPc4
pobočky	pobočka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Návrhy	návrh	k1gInPc1
změn	změna	k1gFnPc2
</s>
<s>
Po	po	k7c6
podzimních	podzimní	k2eAgFnPc6d1
krajských	krajský	k2eAgFnPc6d1
a	a	k8xC
doplňovacích	doplňovací	k2eAgFnPc6d1
senátních	senátní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
2012	#num#	k4
se	se	k3xPyFc4
ozývaly	ozývat	k5eAaImAgInP
i	i	k9
hlasy	hlas	k1gInPc1
volající	volající	k2eAgInPc1d1
po	po	k7c6
možném	možný	k2eAgNnSc6d1
budoucím	budoucí	k2eAgNnSc6d1
snížení	snížení	k1gNnSc6
počtu	počet	k1gInSc2
krajů	kraj	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Jinou	jiný	k2eAgFnSc7d1
možností	možnost	k1gFnSc7
je	být	k5eAaImIp3nS
úplné	úplný	k2eAgNnSc4d1
zrušení	zrušení	k1gNnSc4
krajů	kraj	k1gInPc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
naznačil	naznačit	k5eAaPmAgMnS
v	v	k7c6
rozhovoru	rozhovor	k1gInSc6
pro	pro	k7c4
Dvacet	dvacet	k4xCc4
minut	minuta	k1gFnPc2
Radiožurnálu	radiožurnál	k1gInSc2
Martin	Martin	k1gMnSc1
Půta	Půta	k1gMnSc1
<g/>
.	.	kIx.
„	„	k?
<g/>
Když	když	k8xS
se	se	k3xPyFc4
podívám	podívat	k5eAaImIp1nS,k5eAaPmIp1nS
do	do	k7c2
Saska	Sasko	k1gNnSc2
<g/>
,	,	kIx,
tak	tak	k9
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
velké	velký	k2eAgNnSc1d1
jako	jako	k8xC,k8xS
polovina	polovina	k1gFnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
a	a	k8xC
žádný	žádný	k3yNgInSc1
mezistupeň	mezistupeň	k1gInSc1
jako	jako	k9
kraje	kraj	k1gInSc2
tam	tam	k6eAd1
neexistuje	existovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čeká	čekat	k5eAaImIp3nS
nás	my	k3xPp1nPc4
debata	debata	k1gFnSc1
<g/>
,	,	kIx,
kolik	kolik	k4yRc1,k4yQc1,k4yIc1
samostatných	samostatný	k2eAgInPc2d1
stupňů	stupeň	k1gInPc2
ČR	ČR	kA
potřebuje	potřebovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Náklady	náklad	k1gInPc1
na	na	k7c4
výkon	výkon	k1gInSc4
samosprávy	samospráva	k1gFnSc2
jsou	být	k5eAaImIp3nP
poměrně	poměrně	k6eAd1
vysoké	vysoký	k2eAgFnPc1d1
<g/>
,	,	kIx,
<g/>
“	“	k?
míní	mínit	k5eAaImIp3nS
15	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2012	#num#	k4
kandidát	kandidát	k1gMnSc1
na	na	k7c4
hejtmana	hejtman	k1gMnSc4
Libereckého	liberecký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Místopředseda	místopředseda	k1gMnSc1
strany	strana	k1gFnPc1
Moravané	Moravan	k1gMnPc1
Ondřej	Ondřej	k1gMnSc1
Hýsek	Hýsek	k1gMnSc1
<g/>
,	,	kIx,
kritizoval	kritizovat	k5eAaImAgMnS
dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2014	#num#	k4
kraje	kraj	k1gInSc2
jako	jako	k8xS,k8xC
nefunkční	funkční	k2eNgNnSc4d1
a	a	k8xC
drahé	drahý	k2eAgNnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Naopak	naopak	k6eAd1
jiní	jiný	k2eAgMnPc1d1
politici	politik	k1gMnPc1
označili	označit	k5eAaPmAgMnP
jako	jako	k8xS,k8xC
hlavní	hlavní	k2eAgInSc4d1
problém	problém	k1gInSc4
krajů	kraj	k1gInPc2
přenesení	přenesení	k1gNnSc4
zodpovědnosti	zodpovědnost	k1gFnSc2
ze	z	k7c2
státní	státní	k2eAgFnSc2d1
úrovně	úroveň	k1gFnSc2
bez	bez	k7c2
odpovídajícího	odpovídající	k2eAgInSc2d1
přesunu	přesun	k1gInSc2
financí	finance	k1gFnPc2
pro	pro	k7c4
vykonávání	vykonávání	k1gNnSc4
těchto	tento	k3xDgFnPc2
povinností	povinnost	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Základní	základní	k2eAgNnPc1d1
data	datum	k1gNnPc1
o	o	k7c6
krajích	kraj	k1gInPc6
</s>
<s>
Základní	základní	k2eAgNnPc1d1
data	datum	k1gNnPc1
o	o	k7c6
krajích	kraj	k1gInPc6
</s>
<s>
Název	název	k1gInSc1
kraje	kraj	k1gInSc2
(	(	kIx(
<g/>
kraj	kraj	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Zkratka	zkratka	k1gFnSc1
ČSÚ	ČSÚ	kA
</s>
<s>
Zkratka	zkratka	k1gFnSc1
na	na	k7c4
RZ	RZ	kA
</s>
<s>
Krajské	krajský	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rozloha	rozloha	k1gFnSc1
(	(	kIx(
<g/>
km²	km²	k?
<g/>
)	)	kIx)
</s>
<s>
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
(	(	kIx(
<g/>
/	/	kIx~
<g/>
km²	km²	k?
<g/>
)	)	kIx)
</s>
<s>
HDP	HDP	kA
(	(	kIx(
<g/>
mld.	mld.	k?
Kč	Kč	kA
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
HDP	HDP	kA
na	na	k7c4
obyv	obyv	k1gInSc4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
PHA	pha	k0wR
</s>
<s>
A	a	k9
</s>
<s>
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
324	#num#	k4
277	#num#	k4
</s>
<s>
496,10	496,10	k4
</s>
<s>
2	#num#	k4
360	#num#	k4
</s>
<s>
1	#num#	k4
193,2	193,2	k4
</s>
<s>
937,542	937,542	k4
</s>
<s>
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
STČ	STČ	kA
</s>
<s>
S	s	k7c7
</s>
<s>
Praha	Praha	k1gFnSc1
</s>
<s>
1	#num#	k4
385	#num#	k4
141	#num#	k4
</s>
<s>
11	#num#	k4
0	#num#	k4
<g/>
14,97	14,97	k4
</s>
<s>
104	#num#	k4
</s>
<s>
552,5	552,5	k4
</s>
<s>
414,379	414,379	k4
</s>
<s>
Jihočeský	jihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
JHČ	JHČ	kA
</s>
<s>
C	C	kA
</s>
<s>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
</s>
<s>
644	#num#	k4
083	#num#	k4
</s>
<s>
10	#num#	k4
0	#num#	k4
<g/>
56,79	56,79	k4
</s>
<s>
62	#num#	k4
</s>
<s>
238,6	238,6	k4
</s>
<s>
373,833	373,833	k4
</s>
<s>
Plzeňský	plzeňský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
PLK	plk	k1gInSc1
</s>
<s>
P	P	kA
</s>
<s>
Plzeň	Plzeň	k1gFnSc1
</s>
<s>
589	#num#	k4
899	#num#	k4
</s>
<s>
7	#num#	k4
560,93	560,93	k4
</s>
<s>
73	#num#	k4
</s>
<s>
243,9	243,9	k4
</s>
<s>
422,251	422,251	k4
</s>
<s>
Karlovarský	karlovarský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
KVK	KVK	kA
</s>
<s>
K	k	k7c3
</s>
<s>
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
</s>
<s>
294	#num#	k4
664	#num#	k4
</s>
<s>
3	#num#	k4
314,46	314,46	k4
</s>
<s>
92	#num#	k4
</s>
<s>
89,5	89,5	k4
</s>
<s>
300,894	300,894	k4
</s>
<s>
Ústecký	ústecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
ULK	ULK	kA
</s>
<s>
U	u	k7c2
</s>
<s>
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
820	#num#	k4
965	#num#	k4
</s>
<s>
5	#num#	k4
334,52	334,52	k4
</s>
<s>
154	#num#	k4
</s>
<s>
274,3	274,3	k4
</s>
<s>
333,521	333,521	k4
</s>
<s>
Liberecký	liberecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
LBK	LBK	kA
</s>
<s>
L	L	kA
</s>
<s>
Liberec	Liberec	k1gInSc1
</s>
<s>
443	#num#	k4
690	#num#	k4
</s>
<s>
3	#num#	k4
162,93	162,93	k4
</s>
<s>
135	#num#	k4
</s>
<s>
155,1	155,1	k4
</s>
<s>
352,313	352,313	k4
</s>
<s>
Královéhradecký	královéhradecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
HKK	HKK	kA
</s>
<s>
H	H	kA
</s>
<s>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
551	#num#	k4
647	#num#	k4
</s>
<s>
4	#num#	k4
758,54	758,54	k4
</s>
<s>
115	#num#	k4
</s>
<s>
221,1	221,1	k4
</s>
<s>
401,056	401,056	k4
</s>
<s>
Pardubický	pardubický	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
PAK	pak	k6eAd1
</s>
<s>
E	E	kA
</s>
<s>
Pardubice	Pardubice	k1gInPc1
</s>
<s>
522	#num#	k4
662	#num#	k4
</s>
<s>
4	#num#	k4
519,00	519,00	k4
</s>
<s>
112	#num#	k4
</s>
<s>
186,2	186,2	k4
</s>
<s>
360,372	360,372	k4
</s>
<s>
Olomoucký	olomoucký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
OLK	OLK	kA
</s>
<s>
M	M	kA
</s>
<s>
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
632	#num#	k4
015	#num#	k4
</s>
<s>
5	#num#	k4
266,57	266,57	k4
</s>
<s>
123	#num#	k4
</s>
<s>
219,9	219,9	k4
</s>
<s>
346,789	346,789	k4
</s>
<s>
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
MSK	MSK	kA
</s>
<s>
T	T	kA
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
1	#num#	k4
200	#num#	k4
539	#num#	k4
</s>
<s>
5	#num#	k4
426,83	426,83	k4
</s>
<s>
227	#num#	k4
</s>
<s>
466,7	466,7	k4
</s>
<s>
385,247	385,247	k4
</s>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
JHM	JHM	kA
</s>
<s>
B	B	kA
</s>
<s>
Brno	Brno	k1gNnSc1
</s>
<s>
1	#num#	k4
191	#num#	k4
981	#num#	k4
</s>
<s>
7	#num#	k4
187,70	187,70	k4
</s>
<s>
165	#num#	k4
</s>
<s>
513,7	513,7	k4
</s>
<s>
436,043	436,043	k4
</s>
<s>
Zlínský	zlínský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
ZLK	ZLK	kA
</s>
<s>
Z	z	k7c2
</s>
<s>
Zlín	Zlín	k1gInSc1
</s>
<s>
582	#num#	k4
555	#num#	k4
</s>
<s>
3	#num#	k4
963,55	963,55	k4
</s>
<s>
149	#num#	k4
</s>
<s>
228,6	228,6	k4
</s>
<s>
391,336	391,336	k4
</s>
<s>
Kraj	kraj	k1gInSc1
Vysočina	vysočina	k1gFnSc1
</s>
<s>
VYS	VYS	kA
</s>
<s>
J	J	kA
</s>
<s>
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
509	#num#	k4
813	#num#	k4
</s>
<s>
6	#num#	k4
795,56	795,56	k4
</s>
<s>
75	#num#	k4
</s>
<s>
190,1	190,1	k4
</s>
<s>
373,421	373,421	k4
</s>
<s>
CZ	CZ	kA
</s>
<s>
10	#num#	k4
693	#num#	k4
939	#num#	k4
</s>
<s>
78	#num#	k4
864,92	864,92	k4
</s>
<s>
134	#num#	k4
</s>
<s>
4	#num#	k4
733,4	733,4	k4
</s>
<s>
451,932	451,932	k4
</s>
<s>
Pravomoce	pravomoc	k1gFnPc1
krajů	kraj	k1gInPc2
</s>
<s>
školstvíkraje	školstvíkrat	k5eAaImSgMnS
zřizují	zřizovat	k5eAaImIp3nP
střední	střední	k2eAgFnSc2d1
školy	škola	k1gFnSc2
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
zdravotnictvízodpovědnost	zdravotnictvízodpovědnost	k1gFnSc1
za	za	k7c2
nemocnice	nemocnice	k1gFnSc2
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
dopravastavba	dopravastavba	k1gFnSc1
a	a	k8xC
opravy	oprava	k1gFnPc1
silnic	silnice	k1gFnPc2
druhé	druhý	k4xOgFnSc2
a	a	k8xC
třetí	třetí	k4xOgFnSc2
třídy	třída	k1gFnSc2
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
integrované	integrovaný	k2eAgInPc1d1
dopravní	dopravní	k2eAgInPc1d1
systémy	systém	k1gInPc1
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
objednávání	objednávání	k1gNnSc1
veřejné	veřejný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
mezi	mezi	k7c7
městy	město	k1gNnPc7
a	a	k8xC
obcemi	obec	k1gFnPc7
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
vliv	vliv	k1gInSc4
na	na	k7c4
stavbu	stavba	k1gFnSc4
a	a	k8xC
umístění	umístění	k1gNnSc1
dálnic	dálnice	k1gFnPc2
skrze	skrze	k?
schvalování	schvalování	k1gNnSc2
plánů	plán	k1gInPc2
územního	územní	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
sociální	sociální	k2eAgFnSc1d1
službyzodpovědnost	službyzodpovědnost	k1gFnSc1
za	za	k7c4
domovy	domov	k1gInPc4
pro	pro	k7c4
seniory	senior	k1gMnPc4
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
kraje	kraj	k1gInPc1
rozdělují	rozdělovat	k5eAaImIp3nP
příspěvky	příspěvek	k1gInPc4
chudým	chudý	k1gMnPc3
a	a	k8xC
mají	mít	k5eAaImIp3nP
zodpovědnsot	zodpovědnsot	k1gInSc4
za	za	k7c4
sociální	sociální	k2eAgNnSc4d1
zařízení	zařízení	k1gNnSc4
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
ochrana	ochrana	k1gFnSc1
přírodykraje	přírodykrat	k5eAaImSgInS
vyhlašují	vyhlašovat	k5eAaImIp3nP
přírodní	přírodní	k2eAgInSc4d1
park	park	k1gInSc4
<g/>
,	,	kIx,
přírodní	přírodní	k2eAgFnSc4d1
rezervaci	rezervace	k1gFnSc4
nebo	nebo	k8xC
památku	památka	k1gFnSc4
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
legislativazastupitelstvo	legislativazastupitelstvo	k1gNnSc1
kraje	kraj	k1gInSc2
smí	smět	k5eAaImIp3nS
navrhovat	navrhovat	k5eAaImF
Poslanecké	poslanecký	k2eAgFnSc3d1
sněmovně	sněmovna	k1gFnSc3
zákony	zákon	k1gInPc4
a	a	k8xC
podávat	podávat	k5eAaImF
stížnosti	stížnost	k1gFnPc4
k	k	k7c3
Ústavnímu	ústavní	k2eAgInSc3d1
soudu	soud	k1gInSc3
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
rozdělování	rozdělování	k1gNnSc4
dotací	dotace	k1gFnPc2
z	z	k7c2
EUkraje	EUkraje	k1gFnSc2
spolupracují	spolupracovat	k5eAaImIp3nP
na	na	k7c4
rozdělování	rozdělování	k1gNnSc4
financí	finance	k1gFnPc2
z	z	k7c2
EU	EU	kA
v	v	k7c6
rámci	rámec	k1gInSc6
tzv.	tzv.	kA
regionů	region	k1gInPc2
soudružnosti	soudružnost	k1gFnSc2
(	(	kIx(
<g/>
NUTS-	NUTS-	k1gFnSc1
<g/>
2	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
integrovaný	integrovaný	k2eAgInSc1d1
záchraný	záchraný	k2eAgInSc1d1
systémkrajský	systémkrajský	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
hejtman	hejtman	k1gMnSc1
a	a	k8xC
hasičský	hasičský	k2eAgInSc1d1
záchranný	záchranný	k2eAgInSc1d1
sbor	sbor	k1gInSc1
kraje	kraj	k1gInSc2
plní	plnit	k5eAaImIp3nS
některé	některý	k3yIgInPc4
úkoly	úkol	k1gInPc4
v	v	k7c6
rámci	rámec	k1gInSc6
integrovaného	integrovaný	k2eAgInSc2d1
záchranného	záchranný	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Financování	financování	k1gNnSc1
</s>
<s>
Kraje	kraj	k1gInPc1
hospodaří	hospodařit	k5eAaImIp3nP
s	s	k7c7
velkým	velký	k2eAgNnSc7d1
množstvím	množství	k1gNnSc7
peněz	peníze	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
skoro	skoro	k6eAd1
165	#num#	k4
miliard	miliarda	k4xCgFnPc2
korun	koruna	k1gFnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
odpovídá	odpovídat	k5eAaImIp3nS
sedmině	sedmina	k1gFnSc3
státního	státní	k2eAgInSc2d1
rozpočtu	rozpočet	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Přitom	přitom	k6eAd1
však	však	k9
kraje	kraj	k1gInPc1
získávají	získávat	k5eAaImIp3nP
pouhých	pouhý	k2eAgInPc2d1
9	#num#	k4
procent	procento	k1gNnPc2
svého	svůj	k3xOyFgInSc2
rozpočtu	rozpočet	k1gInSc2
přímo	přímo	k6eAd1
z	z	k7c2
daní	daň	k1gFnPc2
<g/>
,	,	kIx,
přesněji	přesně	k6eAd2
z	z	k7c2
celostátního	celostátní	k2eAgInSc2d1
hrubého	hrubý	k2eAgInSc2d1
výnosu	výnos	k1gInSc2
DPH	DPH	kA
a	a	k8xC
daně	daň	k1gFnSc2
z	z	k7c2
příjmu	příjem	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvě	dva	k4xCgFnPc1
třetiny	třetina	k1gFnPc1
příjmů	příjem	k1gInPc2
krajů	kraj	k1gInPc2
jsou	být	k5eAaImIp3nP
státní	státní	k2eAgFnPc1d1
dotace	dotace	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkový	celkový	k2eAgInSc1d1
rozpočet	rozpočet	k1gInSc1
krajů	kraj	k1gInPc2
byl	být	k5eAaImAgInS
160	#num#	k4
miliard	miliarda	k4xCgFnPc2
korun	koruna	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
2015	#num#	k4
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
239	#num#	k4
miliard	miliarda	k4xCgFnPc2
<g/>
,	,	kIx,
přitom	přitom	k6eAd1
více	hodně	k6eAd2
než	než	k8xS
polovina	polovina	k1gFnSc1
peněz	peníze	k1gInPc2
přišla	přijít	k5eAaPmAgFnS
od	od	k7c2
Ministerstva	ministerstvo	k1gNnSc2
školství	školství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kraje	kraj	k1gInSc2
tedy	tedy	k9
především	především	k6eAd1
přerozdělují	přerozdělovat	k5eAaImIp3nP
peníze	peníz	k1gInPc1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
investice	investice	k1gFnPc4
jde	jít	k5eAaImIp3nS
asi	asi	k9
čtvrtina	čtvrtina	k1gFnSc1
rozpočtu	rozpočet	k1gInSc2
a	a	k8xC
to	ten	k3xDgNnSc1
s	s	k7c7
velkým	velký	k2eAgInSc7d1
podílem	podíl	k1gInSc7
peněz	peníze	k1gInPc2
z	z	k7c2
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
Například	například	k6eAd1
Ústecký	ústecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
měl	mít	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
rozpočet	rozpočet	k1gInSc4
okolo	okolo	k7c2
20	#num#	k4
miliard	miliarda	k4xCgFnPc2
korun	koruna	k1gFnPc2
<g/>
,	,	kIx,
tedy	tedy	k9
asi	asi	k9
24	#num#	k4
tisíc	tisíc	k4xCgInPc2
korun	koruna	k1gFnPc2
na	na	k7c4
každěho	každěze	k6eAd1
obyvatele	obyvatel	k1gMnPc4
kraje	kraj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgNnSc2
60	#num#	k4
procent	procento	k1gNnPc2
šlo	jít	k5eAaImAgNnS
do	do	k7c2
škol	škola	k1gFnPc2
<g/>
.	.	kIx.
17	#num#	k4
procent	procento	k1gNnPc2
šlo	jít	k5eAaImAgNnS
na	na	k7c4
objednávku	objednávka	k1gFnSc4
dopravy	doprava	k1gFnSc2
a	a	k8xC
opravy	oprava	k1gFnSc2
silnic	silnice	k1gFnPc2
<g/>
.	.	kIx.
11	#num#	k4
procent	procento	k1gNnPc2
bylo	být	k5eAaImAgNnS
určeno	určit	k5eAaPmNgNnS
sociální	sociální	k2eAgNnSc4d1
oblasti	oblast	k1gFnPc4
<g/>
,	,	kIx,
především	především	k9
chudým	chudý	k2eAgFnPc3d1
domácnostem	domácnost	k1gFnPc3
<g/>
.	.	kIx.
2	#num#	k4
procenta	procento	k1gNnSc2
šla	jít	k5eAaImAgFnS
na	na	k7c4
záchranku	záchranka	k1gFnSc4
a	a	k8xC
zbytek	zbytek	k1gInSc4
financí	finance	k1gFnPc2
je	být	k5eAaImIp3nS
použit	použít	k5eAaPmNgMnS
na	na	k7c4
všechno	všechen	k3xTgNnSc4
jiné	jiný	k2eAgNnSc4d1
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
kraj	kraj	k1gInSc1
dělá	dělat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Symboly	symbol	k1gInPc1
krajů	kraj	k1gInPc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Symboly	symbol	k1gInPc1
krajů	kraj	k1gInPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Jihočeský	jihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Plzeňský	plzeňský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Karlovarský	karlovarský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Ústecký	ústecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Liberecký	liberecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Královéhradecký	královéhradecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Pardubický	pardubický	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Kraj	kraj	k1gInSc1
Vysočina	vysočina	k1gFnSc1
</s>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Olomoucký	olomoucký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Zlínský	zlínský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
Politika	politika	k1gFnSc1
</s>
<s>
Volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
krajů	kraj	k1gInPc2
se	se	k3xPyFc4
konají	konat	k5eAaImIp3nP
jednou	jednou	k6eAd1
za	za	k7c4
čtyři	čtyři	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
byly	být	k5eAaImAgInP
uspořádány	uspořádat	k5eAaPmNgInP
v	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
a	a	k8xC
jejich	jejich	k3xOp3gMnSc7
vítězem	vítěz	k1gMnSc7
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
ODS	ODS	kA
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
roku	rok	k1gInSc2
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2008	#num#	k4
a	a	k8xC
2012	#num#	k4
v	v	k7c6
krajských	krajský	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
zvítězila	zvítězit	k5eAaPmAgFnS
ČSSD	ČSSD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
krajských	krajský	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
nejvíce	nejvíce	k6eAd1,k6eAd3
hlasů	hlas	k1gInPc2
získalo	získat	k5eAaPmAgNnS
hnutí	hnutí	k1gNnSc1
ANO	ano	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
volbách	volba	k1gFnPc6
2020	#num#	k4
získalo	získat	k5eAaPmAgNnS
ANO	ano	k9
také	také	k9
většinu	většina	k1gFnSc4
hlasů	hlas	k1gInPc2
<g/>
,	,	kIx,
avšak	avšak	k8xC
ve	v	k7c6
většině	většina	k1gFnSc6
krajů	kraj	k1gInPc2
skončilo	skončit	k5eAaPmAgNnS
v	v	k7c6
opozici	opozice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Vítězové	vítěz	k1gMnPc1
voleb	volba	k1gFnPc2
podle	podle	k7c2
krajů	kraj	k1gInPc2
</s>
<s>
Mapa	mapa	k1gFnSc1
výsledků	výsledek	k1gInPc2
voleb	volba	k1gFnPc2
roku	rok	k1gInSc2
2000	#num#	k4
ODS	ODS	kA
Čtyřkoalice	Čtyřkoalice	k1gFnSc2
KSČM	KSČM	kA
</s>
<s>
Mapa	mapa	k1gFnSc1
výsledků	výsledek	k1gInPc2
voleb	volba	k1gFnPc2
roku	rok	k1gInSc2
2004	#num#	k4
ODS	ODS	kA
KDU-ČSL	KDU-ČSL	k1gFnPc2
</s>
<s>
Mapa	mapa	k1gFnSc1
výsledků	výsledek	k1gInPc2
voleb	volba	k1gFnPc2
roku	rok	k1gInSc2
2008	#num#	k4
ČSSD	ČSSD	kA
</s>
<s>
Mapa	mapa	k1gFnSc1
výsledků	výsledek	k1gInPc2
voleb	volba	k1gFnPc2
roku	rok	k1gInSc2
2012	#num#	k4
ČSSD	ČSSD	kA
KSČM	KSČM	kA
ODS	ODS	kA
SLK	SLK	kA
</s>
<s>
Mapa	mapa	k1gFnSc1
výsledků	výsledek	k1gInPc2
voleb	volba	k1gFnPc2
roku	rok	k1gInSc2
2016	#num#	k4
ANO	ano	k9
ČSSD	ČSSD	kA
SLK	SLK	kA
KDU-ČSL	KDU-ČSL	k1gMnSc1
</s>
<s>
Mapa	mapa	k1gFnSc1
výsledků	výsledek	k1gInPc2
voleb	volba	k1gFnPc2
roku	rok	k1gInSc2
2020	#num#	k4
ANO	ano	k9
ODS	ODS	kA
a	a	k8xC
koalice	koalice	k1gFnSc1
STAN	stan	k1gInSc1
SLK	SLK	kA
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1
</s>
<s>
Podnikání	podnikání	k1gNnSc1
</s>
<s>
Počet	počet	k1gInSc1
podnikatelských	podnikatelský	k2eAgInPc2d1
subjektů	subjekt	k1gInPc2
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
kraj	kraj	k1gInSc1
</s>
<s>
firem	firma	k1gFnPc2
<g/>
[	[	kIx(
<g/>
ujasnit	ujasnit	k5eAaPmF
<g/>
]	]	kIx)
</s>
<s>
fyzických	fyzický	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
</s>
<s>
ostatních	ostatní	k2eAgInPc2d1
</s>
<s>
celkem	celkem	k6eAd1
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
157	#num#	k4
771	#num#	k4
</s>
<s>
243	#num#	k4
715	#num#	k4
</s>
<s>
60	#num#	k4
600	#num#	k4
</s>
<s>
462	#num#	k4
086	#num#	k4
</s>
<s>
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
28	#num#	k4
959	#num#	k4
</s>
<s>
191	#num#	k4
907	#num#	k4
</s>
<s>
29	#num#	k4
070	#num#	k4
</s>
<s>
249	#num#	k4
936	#num#	k4
</s>
<s>
Jihočeský	jihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
12	#num#	k4
980	#num#	k4
</s>
<s>
94	#num#	k4
576	#num#	k4
</s>
<s>
16	#num#	k4
821	#num#	k4
</s>
<s>
124	#num#	k4
377	#num#	k4
</s>
<s>
Plzeňský	plzeňský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
12	#num#	k4
859	#num#	k4
</s>
<s>
84	#num#	k4
094	#num#	k4
</s>
<s>
15	#num#	k4
958	#num#	k4
</s>
<s>
112	#num#	k4
911	#num#	k4
</s>
<s>
Karlovarský	karlovarský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
8	#num#	k4
397	#num#	k4
</s>
<s>
42	#num#	k4
429	#num#	k4
</s>
<s>
9	#num#	k4
111	#num#	k4
</s>
<s>
59	#num#	k4
937	#num#	k4
</s>
<s>
Ústecký	ústecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
15	#num#	k4
090	#num#	k4
</s>
<s>
98	#num#	k4
541	#num#	k4
</s>
<s>
17	#num#	k4
845	#num#	k4
</s>
<s>
131	#num#	k4
476	#num#	k4
</s>
<s>
Liberecký	liberecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
9	#num#	k4
480	#num#	k4
</s>
<s>
71	#num#	k4
740	#num#	k4
</s>
<s>
10	#num#	k4
142	#num#	k4
</s>
<s>
91	#num#	k4
362	#num#	k4
</s>
<s>
Královéhradecký	královéhradecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
10	#num#	k4
645	#num#	k4
</s>
<s>
76	#num#	k4
876	#num#	k4
</s>
<s>
13	#num#	k4
137	#num#	k4
</s>
<s>
100	#num#	k4
658	#num#	k4
</s>
<s>
Pardubický	pardubický	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
8	#num#	k4
781	#num#	k4
</s>
<s>
71	#num#	k4
305	#num#	k4
</s>
<s>
12	#num#	k4
097	#num#	k4
</s>
<s>
92	#num#	k4
183	#num#	k4
</s>
<s>
Kraj	kraj	k1gInSc1
Vysočina	vysočina	k1gFnSc1
</s>
<s>
7	#num#	k4
046	#num#	k4
</s>
<s>
64	#num#	k4
947	#num#	k4
</s>
<s>
11	#num#	k4
765	#num#	k4
</s>
<s>
83	#num#	k4
758	#num#	k4
</s>
<s>
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
39	#num#	k4
352	#num#	k4
</s>
<s>
167	#num#	k4
515	#num#	k4
</s>
<s>
28	#num#	k4
831	#num#	k4
</s>
<s>
235	#num#	k4
698	#num#	k4
</s>
<s>
Olomoucký	olomoucký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
10	#num#	k4
813	#num#	k4
</s>
<s>
84	#num#	k4
330	#num#	k4
</s>
<s>
13	#num#	k4
525	#num#	k4
</s>
<s>
108	#num#	k4
668	#num#	k4
</s>
<s>
Zlínský	zlínský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
12	#num#	k4
223	#num#	k4
</s>
<s>
84	#num#	k4
994	#num#	k4
</s>
<s>
11	#num#	k4
514	#num#	k4
</s>
<s>
108	#num#	k4
731	#num#	k4
</s>
<s>
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
23	#num#	k4
308	#num#	k4
</s>
<s>
150	#num#	k4
090	#num#	k4
</s>
<s>
21	#num#	k4
802	#num#	k4
</s>
<s>
195	#num#	k4
200	#num#	k4
</s>
<s>
Celkem	celkem	k6eAd1
</s>
<s>
357	#num#	k4
704	#num#	k4
</s>
<s>
1	#num#	k4
527	#num#	k4
059	#num#	k4
</s>
<s>
272	#num#	k4
218	#num#	k4
</s>
<s>
2	#num#	k4
156	#num#	k4
981	#num#	k4
</s>
<s>
Grafický	grafický	k2eAgInSc1d1
přehled	přehled	k1gInSc1
krajů	kraj	k1gInPc2
</s>
<s>
14	#num#	k4
krajů	kraj	k1gInPc2
je	být	k5eAaImIp3nS
uvedeno	uvést	k5eAaPmNgNnS
v	v	k7c6
číslovaném	číslovaný	k2eAgNnSc6d1
pořadí	pořadí	k1gNnSc6
podle	podle	k7c2
zákona	zákon	k1gInSc2
<g/>
,	,	kIx,
za	za	k7c7
pomlčkou	pomlčka	k1gFnSc7
je	být	k5eAaImIp3nS
uvedeno	uveden	k2eAgNnSc1d1
sídlo	sídlo	k1gNnSc1
kraje	kraj	k1gInSc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
krajské	krajský	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgInSc4d1
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
–	–	k?
Praha	Praha	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihočeský	jihočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2001	#num#	k4
Budějovický	budějovický	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plzeňský	plzeňský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
–	–	k?
Plzeň	Plzeň	k1gFnSc1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karlovarský	karlovarský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
–	–	k?
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústecký	ústecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
–	–	k?
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liberecký	liberecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
–	–	k?
Liberec	Liberec	k1gInSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Královéhradecký	královéhradecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
–	–	k?
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pardubický	pardubický	k2eAgInSc1d1
kraj	kraj	k1gInSc1
–	–	k?
Pardubice	Pardubice	k1gInPc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kraj	kraj	k1gInSc1
Vysočina	vysočina	k1gFnSc1
(	(	kIx(
<g/>
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2001	#num#	k4
Jihlavský	jihlavský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
poté	poté	k6eAd1
do	do	k7c2
1	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
jen	jen	k9
Vysočina	vysočina	k1gFnSc1
<g/>
)	)	kIx)
–	–	k?
Jihlava	Jihlava	k1gFnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2001	#num#	k4
Brněnský	brněnský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
Brno	Brno	k1gNnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olomoucký	olomoucký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
–	–	k?
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2001	#num#	k4
Ostravský	ostravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
)	)	kIx)
–	–	k?
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zlínský	zlínský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
–	–	k?
Zlín	Zlín	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
čl	čl	kA
<g/>
.	.	kIx.
99	#num#	k4
Ústavy	ústava	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
↑	↑	k?
Ústavní	ústavní	k2eAgInSc1d1
zákon	zákon	k1gInSc1
<g/>
,	,	kIx,
kterým	který	k3yQgInSc7,k3yRgInSc7,k3yIgInSc7
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
a	a	k8xC
doplňuje	doplňovat	k5eAaImIp3nS
Ústava	ústava	k1gFnSc1
Československé	československý	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
ústavní	ústavní	k2eAgInSc1d1
zákon	zákon	k1gInSc1
č.	č.	k?
143	#num#	k4
<g/>
/	/	kIx~
<g/>
1968	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
československé	československý	k2eAgFnSc6d1
federaci	federace	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
kterým	který	k3yRgNnSc7,k3yQgNnSc7,k3yIgNnSc7
se	se	k3xPyFc4
zkracuje	zkracovat	k5eAaImIp3nS
volební	volební	k2eAgNnSc4d1
období	období	k1gNnSc4
národních	národní	k2eAgInPc2d1
výborů	výbor	k1gInPc2
<g/>
,	,	kIx,
čl	čl	kA
<g/>
.	.	kIx.
I	I	kA
a	a	k8xC
III	III	kA
<g/>
.	.	kIx.
<g/>
↑	↑	k?
§	§	k?
72	#num#	k4
zákona	zákon	k1gInSc2
o	o	k7c6
obcích	obec	k1gFnPc6
<g/>
↑	↑	k?
Důvodová	důvodový	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
k	k	k7c3
vládnímu	vládní	k2eAgInSc3d1
návrhu	návrh	k1gInSc3
zákona	zákon	k1gInSc2
o	o	k7c6
územně	územně	k6eAd1
správním	správní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
11	#num#	k4
<g/>
–	–	k?
<g/>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Příloha	příloha	k1gFnSc1
č.	č.	k?
2	#num#	k4
k	k	k7c3
zákonu	zákon	k1gInSc3
č.	č.	k?
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
soudech	soud	k1gInPc6
a	a	k8xC
soudcích	soudek	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
↑	↑	k?
§	§	k?
7	#num#	k4
zákona	zákon	k1gInSc2
č.	č.	k?
283	#num#	k4
<g/>
/	/	kIx~
<g/>
1993	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
státním	státní	k2eAgNnSc6d1
zastupitelství	zastupitelství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
↑	↑	k?
Jmenování	jmenování	k1gNnSc1
krajských	krajský	k2eAgMnPc2d1
ředitelů	ředitel	k1gMnPc2
policie	policie	k1gFnSc2
<g/>
,	,	kIx,
URL	URL	kA
<g/>
:	:	kIx,
http://www.policie.cz/clanek/jmenovani-krajskych-reditelu-policie.aspx	http://www.policie.cz/clanek/jmenovani-krajskych-reditelu-policie.aspx	k1gInSc1
<g/>
↑	↑	k?
Strany	strana	k1gFnSc2
<g/>
:	:	kIx,
Je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
debatovat	debatovat	k5eAaImF
o	o	k7c6
počtu	počet	k1gInSc6
a	a	k8xC
racionalizaci	racionalizace	k1gFnSc3
krajského	krajský	k2eAgNnSc2d1
zřízení	zřízení	k1gNnSc2
<g/>
↑	↑	k?
Počet	počet	k1gInSc1
krajů	kraj	k1gInPc2
by	by	kYmCp3nS
se	se	k3xPyFc4
mohl	moct	k5eAaImAgInS
v	v	k7c6
budoucnu	budoucno	k1gNnSc6
snížit	snížit	k5eAaPmF
<g/>
↑	↑	k?
Méně	málo	k6eAd2
krajů	kraj	k1gInPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
?	?	kIx.
</s>
<s desamb="1">
V	v	k7c6
budoucnu	budoucno	k1gNnSc6
možná	možná	k9
ano	ano	k9
<g/>
↑	↑	k?
Koalice	koalice	k1gFnSc1
s	s	k7c7
Korytářem	korytář	k1gMnSc7
a	a	k8xC
ODS	ODS	kA
je	být	k5eAaImIp3nS
na	na	k7c6
Liberecku	Liberecko	k1gNnSc6
stále	stále	k6eAd1
otevřená	otevřený	k2eAgFnSc1d1
<g/>
,	,	kIx,
věří	věřit	k5eAaImIp3nS
šéf	šéf	k1gMnSc1
Starostů	Starosta	k1gMnPc2
<g/>
↑	↑	k?
http://prehravac.rozhlas.cz/audio/2743645	http://prehravac.rozhlas.cz/audio/2743645	k4
čas	čas	k1gInSc1
16	#num#	k4
<g/>
:	:	kIx,
<g/>
40	#num#	k4
<g/>
,	,	kIx,
Dvacet	dvacet	k4xCc4
minut	minuta	k1gFnPc2
Radiožurnálu	radiožurnál	k1gInSc2
-	-	kIx~
Rozhovor	rozhovor	k1gInSc1
s	s	k7c7
lídrem	lídr	k1gMnSc7
Starostů	Starosta	k1gMnPc2
na	na	k7c6
Liberecku	Liberecko	k1gNnSc6
Martinem	Martin	k1gMnSc7
Půtou	Půta	k1gMnSc7
<g/>
↑	↑	k?
čas	čas	k1gInSc1
15	#num#	k4
<g/>
:	:	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
541	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Kompetence	kompetence	k1gFnSc2
krajů	kraj	k1gInPc2
jsou	být	k5eAaImIp3nP
jasné	jasný	k2eAgInPc1d1
<g/>
,	,	kIx,
rozdělování	rozdělování	k1gNnSc1
státních	státní	k2eAgInPc2d1
peněz	peníze	k1gInPc2
už	už	k6eAd1
méně	málo	k6eAd2
<g/>
,	,	kIx,
řekl	říct	k5eAaPmAgMnS
v	v	k7c6
předvolební	předvolební	k2eAgFnSc6d1
debatě	debata	k1gFnSc6
Wintr	Wintr	k1gInSc1
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Superdebata	Superdebata	k1gFnSc1
<g/>
:	:	kIx,
Většina	většina	k1gFnSc1
politiků	politik	k1gMnPc2
by	by	k9
nevracela	vracet	k5eNaImAgFnS
kompetence	kompetence	k1gFnSc1
krajů	kraj	k1gInPc2
zpět	zpět	k6eAd1
na	na	k7c4
centrální	centrální	k2eAgFnSc4d1
úroveň	úroveň	k1gFnSc4
<g/>
.	.	kIx.
ct	ct	k?
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
ceskatelevize	ceskatelevize	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Co	co	k9
vlastně	vlastně	k9
dělají	dělat	k5eAaImIp3nP
kraje	kraj	k1gInPc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
-	-	kIx~
VDO	VDO	kA
<g/>
.	.	kIx.
www.vychovakobcanstvi.cz	www.vychovakobcanstvi.cz	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Silnice	silnice	k1gFnPc1
<g/>
,	,	kIx,
nemocnice	nemocnice	k1gFnPc1
<g/>
,	,	kIx,
školy	škola	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
všechno	všechen	k3xTgNnSc4
se	se	k3xPyFc4
starají	starat	k5eAaImIp3nP
kraje	kraj	k1gInPc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ohlasy	ohlas	k1gInPc1
dění	dění	k1gNnSc4
na	na	k7c4
Boskovicku	Boskovicka	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zdopravy	Zdoprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-10-13	2019-10-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Pojďte	jít	k5eAaImRp2nP
volit	volit	k5eAaImF
<g/>
,	,	kIx,
má	mít	k5eAaImIp3nS
to	ten	k3xDgNnSc1
smysl	smysl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
o	o	k7c4
peníze	peníz	k1gInPc4
pro	pro	k7c4
osm	osm	k4xCc4
ministerstev	ministerstvo	k1gNnPc2
<g/>
,	,	kIx,
vysvětlují	vysvětlovat	k5eAaImIp3nP
ve	v	k7c6
videu	video	k1gNnSc6
politologové	politolog	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-10-02	2016-10-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
239	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
integrovaném	integrovaný	k2eAgInSc6d1
záchranném	záchranný	k2eAgInSc6d1
systému	systém	k1gInSc6
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
některých	některý	k3yIgInPc2
zákonů	zákon	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
In	In	k1gFnSc1
<g/>
:	:	kIx,
Sbírka	sbírka	k1gFnSc1
zákonů	zákon	k1gInPc2
<g/>
.	.	kIx.
2000	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
§	§	k?
10	#num#	k4
<g/>
–	–	k?
<g/>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
K	k	k7c3
čemu	co	k3yRnSc3,k3yQnSc3,k3yInSc3
nám	my	k3xPp1nPc3
jsou	být	k5eAaImIp3nP
kraje	kraj	k1gInPc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
jen	jen	k9
obrovské	obrovský	k2eAgInPc1d1
stroje	stroj	k1gInPc1
na	na	k7c4
přerozdělování	přerozdělování	k1gNnSc4
peněz	peníze	k1gInPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
-	-	kIx~
MALL	MALL	kA
<g/>
.	.	kIx.
<g/>
TV	TV	kA
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
[	[	kIx(
<g/>
s.	s.	k?
<g/>
n.	n.	k?
<g/>
]	]	kIx)
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Počet	počet	k1gInSc1
podnikatelských	podnikatelský	k2eAgInPc2d1
subjektů	subjekt	k1gInPc2
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
-	-	kIx~
rozděleno	rozdělit	k5eAaPmNgNnS
podle	podle	k7c2
krajů	kraj	k1gInPc2
a	a	k8xC
podle	podle	k7c2
typu	typ	k1gInSc2
subjektu	subjekt	k1gInSc2
<g/>
.	.	kIx.
www.merk.cz	www.merk.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
MACEK	Macek	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
;	;	kIx,
ŽÁČEK	Žáček	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krajská	krajský	k2eAgFnSc1d1
správa	správa	k1gFnSc1
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
a	a	k8xC
její	její	k3xOp3gInPc1
archivní	archivní	k2eAgInPc1d1
fondy	fond	k1gInPc1
(	(	kIx(
<g/>
1605	#num#	k4
<g/>
–	–	k?
<g/>
1868	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Archivní	archivní	k2eAgFnSc1d1
správa	správa	k1gFnSc1
Ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
<g/>
,	,	kIx,
1958	#num#	k4
<g/>
.	.	kIx.
350	#num#	k4
<g/>
,	,	kIx,
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
s.	s.	k?
<g/>
,	,	kIx,
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
mapy	mapa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Historie	historie	k1gFnSc1
krajů	kraj	k1gInPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
Asociace	asociace	k1gFnSc1
krajů	kraj	k1gInPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Krajský	krajský	k2eAgInSc1d1
národní	národní	k2eAgInSc1d1
výbor	výbor	k1gInSc1
</s>
<s>
Krajský	krajský	k2eAgInSc4d1
úřad	úřad	k1gInSc4
</s>
<s>
Zastupitelstvo	zastupitelstvo	k1gNnSc1
kraje	kraj	k1gInSc2
</s>
<s>
Obecně	obecně	k6eAd1
závazná	závazný	k2eAgFnSc1d1
vyhláška	vyhláška	k1gFnSc1
kraje	kraj	k1gInSc2
</s>
<s>
Administrativní	administrativní	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
Česka	Česko	k1gNnSc2
</s>
<s>
Okresy	okres	k1gInPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
</s>
<s>
NUTS	NUTS	kA
<g/>
,	,	kIx,
CZ-NUTS	CZ-NUTS	k1gFnSc1
</s>
<s>
Metropolitní	metropolitní	k2eAgFnPc1d1
oblasti	oblast	k1gFnPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
kraj	kraj	k1gInSc4
v	v	k7c6
Česku	Česko	k1gNnSc6
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Dílo	dílo	k1gNnSc1
Zákon	zákon	k1gInSc1
o	o	k7c6
krajích	kraj	k1gInPc6
(	(	kIx(
<g/>
129	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Asociace	asociace	k1gFnSc1
krajů	kraj	k1gInPc2
ČR	ČR	kA
</s>
<s>
Krajské	krajský	k2eAgFnSc2d1
reprezentace	reprezentace	k1gFnSc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
ČSÚ	ČSÚ	kA
</s>
<s>
Územněsprávní	územněsprávní	k2eAgFnPc1d1
reformy	reforma	k1gFnPc1
v	v	k7c6
Česku	Česko	k1gNnSc6
v	v	k7c6
letech	let	k1gInPc6
1848	#num#	k4
–	–	k?
2000	#num#	k4
</s>
<s>
Právní	právní	k2eAgInPc1d1
předpisy	předpis	k1gInPc1
</s>
<s>
Zákon	zákon	k1gInSc1
o	o	k7c6
územním	územní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
(	(	kIx(
<g/>
č.	č.	k?
<g/>
36	#num#	k4
<g/>
/	/	kIx~
<g/>
1960	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Ústavní	ústavní	k2eAgInSc1d1
zákon	zákon	k1gInSc1
o	o	k7c4
vytvoření	vytvoření	k1gNnSc4
vyšších	vysoký	k2eAgInPc2d2
územních	územní	k2eAgInPc2d1
samosprávných	samosprávný	k2eAgInPc2d1
celků	celek	k1gInPc2
(	(	kIx(
<g/>
č.	č.	k?
347	#num#	k4
<g/>
/	/	kIx~
<g/>
1997	#num#	k4
Sb	sb	kA
<g/>
)	)	kIx)
</s>
<s>
Nový	nový	k2eAgInSc1d1
zákon	zákon	k1gInSc1
o	o	k7c6
územně	územně	k6eAd1
správním	správní	k2eAgNnSc6d1
členění	členění	k1gNnSc6
státu	stát	k1gInSc2
na	na	k7c4
Zákony	zákon	k1gInPc4
pro	pro	k7c4
lidi	člověk	k1gMnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Zákon	zákon	k1gInSc1
o	o	k7c6
krajích	kraj	k1gInPc6
(	(	kIx(
<g/>
krajské	krajský	k2eAgNnSc4d1
zřízení	zřízení	k1gNnSc4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
č.	č.	k?
129	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Administrativní	administrativní	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
Česka	Česko	k1gNnSc2
Samosprávné	samosprávný	k2eAgInPc1d1
kraje	kraj	k1gInPc1
(	(	kIx(
<g/>
od	od	k7c2
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
Praha	Praha	k1gFnSc1
•	•	k?
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Jihočeský	jihočeský	k2eAgInSc4d1
kraj	kraj	k1gInSc4
(	(	kIx(
<g/>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Plzeňský	plzeňský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Plzeň	Plzeň	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Karlovarský	karlovarský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Karlovy	Karlův	k2eAgInPc1d1
Vary	Vary	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Ústecký	ústecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
)	)	kIx)
•	•	k?
Liberecký	liberecký	k2eAgInSc4d1
kraj	kraj	k1gInSc4
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Liberec	Liberec	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Královéhradecký	královéhradecký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Pardubický	pardubický	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Pardubice	Pardubice	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Kraj	kraj	k1gInSc1
Vysočina	vysočina	k1gFnSc1
(	(	kIx(
<g/>
Jihlava	Jihlava	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Olomoucký	olomoucký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Olomouc	Olomouc	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Ostrava	Ostrava	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Zlínský	zlínský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Zlín	Zlín	k1gInSc1
<g/>
)	)	kIx)
Regiony	region	k1gInPc1
soudržnosti	soudržnost	k1gFnSc2
</s>
<s>
Praha	Praha	k1gFnSc1
•	•	k?
Střední	střední	k2eAgFnPc4d1
Čechy	Čechy	k1gFnPc4
•	•	k?
Jihozápad	jihozápad	k1gInSc1
•	•	k?
Severozápad	severozápad	k1gInSc1
•	•	k?
Severovýchod	severovýchod	k1gInSc1
•	•	k?
Jihovýchod	jihovýchod	k1gInSc1
•	•	k?
Střední	střední	k2eAgFnSc1d1
Morava	Morava	k1gFnSc1
•	•	k?
Moravskoslezsko	Moravskoslezsko	k1gNnSc1
Územní	územní	k2eAgInPc1d1
kraje	kraj	k1gInPc1
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
samostatná	samostatný	k2eAgFnSc1d1
územní	územní	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
na	na	k7c6
úrovni	úroveň	k1gFnSc6
kraje	kraj	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Jihočeský	jihočeský	k2eAgInSc4d1
kraj	kraj	k1gInSc4
(	(	kIx(
<g/>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
<g/>
)	)	kIx)
•	•	k?
Západočeský	západočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Plzeň	Plzeň	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Severočeský	severočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Ústí	ústí	k1gNnSc1
nad	nad	k7c7
Labem	Labe	k1gNnSc7
<g/>
)	)	kIx)
•	•	k?
Východočeský	východočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Jihomoravský	jihomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Brno	Brno	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Severomoravský	severomoravský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
Ostrava	Ostrava	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Kraje	kraj	k1gInPc1
v	v	k7c6
evropských	evropský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
Země	zem	k1gFnSc2
</s>
<s>
Albánie	Albánie	k1gFnSc1
•	•	k?
Andorra	Andorra	k1gFnSc1
•	•	k?
Arménie	Arménie	k1gFnSc2
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Belgie	Belgie	k1gFnSc1
•	•	k?
Bělorusko	Bělorusko	k1gNnSc1
•	•	k?
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
•	•	k?
Bulharsko	Bulharsko	k1gNnSc1
•	•	k?
Černá	černat	k5eAaImIp3nS
Hora	hora	k1gFnSc1
•	•	k?
Česko	Česko	k1gNnSc1
•	•	k?
Dánsko	Dánsko	k1gNnSc1
•	•	k?
Estonsko	Estonsko	k1gNnSc1
•	•	k?
Finsko	Finsko	k1gNnSc1
•	•	k?
Francie	Francie	k1gFnSc2
•	•	k?
Gruzie	Gruzie	k1gFnSc2
•	•	k?
Chorvatsko	Chorvatsko	k1gNnSc1
•	•	k?
Irsko	Irsko	k1gNnSc4
•	•	k?
Island	Island	k1gInSc1
•	•	k?
Itálie	Itálie	k1gFnSc2
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
•	•	k?
Litva	Litva	k1gFnSc1
•	•	k?
Lotyšsko	Lotyšsko	k1gNnSc1
•	•	k?
Lucembursko	Lucembursko	k1gNnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Maďarsko	Maďarsko	k1gNnSc1
•	•	k?
Malta	Malta	k1gFnSc1
•	•	k?
Moldavsko	Moldavsko	k1gNnSc1
•	•	k?
Monako	Monako	k1gNnSc1
•	•	k?
Německo	Německo	k1gNnSc1
•	•	k?
Nizozemsko	Nizozemsko	k1gNnSc1
•	•	k?
Norsko	Norsko	k1gNnSc1
•	•	k?
Polsko	Polsko	k1gNnSc1
•	•	k?
Portugalsko	Portugalsko	k1gNnSc1
•	•	k?
Rakousko	Rakousko	k1gNnSc1
•	•	k?
Rumunsko	Rumunsko	k1gNnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Řecko	Řecko	k1gNnSc1
•	•	k?
San	San	k1gMnSc1
Marino	Marina	k1gFnSc5
•	•	k?
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
•	•	k?
Slovensko	Slovensko	k1gNnSc1
•	•	k?
Slovinsko	Slovinsko	k1gNnSc1
•	•	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Srbsko	Srbsko	k1gNnSc1
•	•	k?
Španělsko	Španělsko	k1gNnSc1
•	•	k?
Švédsko	Švédsko	k1gNnSc1
•	•	k?
Švýcarsko	Švýcarsko	k1gNnSc1
•	•	k?
Turecko	Turecko	k1gNnSc1
•	•	k?
Ukrajina	Ukrajina	k1gFnSc1
•	•	k?
Vatikán	Vatikán	k1gInSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
koloniea	koloniea	k6eAd1
zámořská	zámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Faerské	Faerský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
DK	DK	kA
<g/>
)	)	kIx)
•	•	k?
Gibraltar	Gibraltar	k1gInSc1
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
•	•	k?
Guernsey	Guernsea	k1gFnSc2
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
•	•	k?
Jersey	Jersea	k1gFnSc2
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
•	•	k?
Man	Man	k1gMnSc1
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
<g/>
Území	území	k1gNnSc4
se	s	k7c7
sporným	sporný	k2eAgNnSc7d1
postavením	postavení	k1gNnSc7
<g/>
:	:	kIx,
Abcházie	Abcházie	k1gFnSc2
•	•	k?
Jižní	jižní	k2eAgFnSc2d1
Osetie	Osetie	k1gFnSc2
•	•	k?
Kosovo	Kosův	k2eAgNnSc1d1
•	•	k?
Podněstří	Podněstří	k1gFnPc3
</s>
