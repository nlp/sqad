<s>
Geheime	Geheimat	k5eAaPmIp3nS	Geheimat
Staatspolizei	Staatspolizei	k1gNnSc1	Staatspolizei
(	(	kIx(	(
<g/>
zkrác	zkrác	k1gFnSc1	zkrác
<g/>
.	.	kIx.	.
gestapo	gestapo	k1gNnSc1	gestapo
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
tajná	tajný	k2eAgFnSc1d1	tajná
státní	státní	k2eAgFnSc1d1	státní
policie	policie	k1gFnSc1	policie
<g/>
)	)	kIx)	)
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
přeměnou	přeměna	k1gFnSc7	přeměna
tajné	tajný	k2eAgFnSc2d1	tajná
pruské	pruský	k2eAgFnSc2d1	pruská
státní	státní	k2eAgFnSc2d1	státní
policie	policie	k1gFnSc2	policie
dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1933	[number]	k4	1933
<g/>
.	.	kIx.	.
</s>
<s>
Gestapo	gestapo	k1gNnSc4	gestapo
založil	založit	k5eAaPmAgMnS	založit
Hermann	Hermann	k1gMnSc1	Hermann
Göring	Göring	k1gInSc4	Göring
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
pod	pod	k7c4	pod
vedení	vedení	k1gNnSc4	vedení
SS	SS	kA	SS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
bylo	být	k5eAaImAgNnS	být
převedeno	převést	k5eAaPmNgNnS	převést
pod	pod	k7c4	pod
RSHA	RSHA	kA	RSHA
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
bylo	být	k5eAaImAgNnS	být
prohlášeno	prohlásit	k5eAaPmNgNnS	prohlásit
za	za	k7c4	za
zločineckou	zločinecký	k2eAgFnSc4d1	zločinecká
organizaci	organizace	k1gFnSc4	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
Hitler	Hitler	k1gMnSc1	Hitler
stal	stát	k5eAaPmAgMnS	stát
německým	německý	k2eAgMnSc7d1	německý
kancléřem	kancléř	k1gMnSc7	kancléř
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
Hermann	Hermann	k1gMnSc1	Hermann
Göring	Göring	k1gInSc1	Göring
pruským	pruský	k2eAgMnSc7d1	pruský
ministrem	ministr	k1gMnSc7	ministr
vnitra	vnitro	k1gNnSc2	vnitro
<g/>
.	.	kIx.	.
</s>
<s>
Oddělil	oddělit	k5eAaPmAgInS	oddělit
politickou	politický	k2eAgFnSc4d1	politická
a	a	k8xC	a
výzvědnou	výzvědný	k2eAgFnSc4d1	výzvědná
složku	složka	k1gFnSc4	složka
od	od	k7c2	od
pořádkové	pořádkový	k2eAgFnSc2d1	pořádková
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
do	do	k7c2	do
jejího	její	k3xOp3gNnSc2	její
vedení	vedení	k1gNnSc2	vedení
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
řadu	řada	k1gFnSc4	řada
členů	člen	k1gInPc2	člen
SS	SS	kA	SS
a	a	k8xC	a
její	její	k3xOp3gFnPc4	její
bezpečnostní	bezpečnostní	k2eAgFnPc4d1	bezpečnostní
složky	složka	k1gFnPc4	složka
<g/>
,	,	kIx,	,
Sicherheitsdienstu	Sicherheitsdiensta	k1gFnSc4	Sicherheitsdiensta
(	(	kIx(	(
<g/>
SD	SD	kA	SD
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
32	[number]	k4	32
až	až	k9	až
46	[number]	k4	46
tisíc	tisíc	k4xCgInPc2	tisíc
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
Gestapa	gestapo	k1gNnSc2	gestapo
nebyli	být	k5eNaImAgMnP	být
nacisté	nacista	k1gMnPc1	nacista
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
bylo	být	k5eAaImAgNnS	být
jen	jen	k6eAd1	jen
asi	asi	k9	asi
15	[number]	k4	15
%	%	kIx~	%
členy	člen	k1gMnPc4	člen
SS	SS	kA	SS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
to	ten	k3xDgNnSc1	ten
však	však	k9	však
loajální	loajální	k2eAgMnSc1d1	loajální
úředníci	úředník	k1gMnPc1	úředník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
přesně	přesně	k6eAd1	přesně
plnili	plnit	k5eAaImAgMnP	plnit
rozkazy	rozkaz	k1gInPc4	rozkaz
nadřízených	nadřízený	k1gMnPc2	nadřízený
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
Hitler	Hitler	k1gMnSc1	Hitler
sjednotil	sjednotit	k5eAaPmAgMnS	sjednotit
tradičně	tradičně	k6eAd1	tradičně
zemské	zemský	k2eAgFnSc2d1	zemská
policie	policie	k1gFnSc2	policie
v	v	k7c4	v
jednu	jeden	k4xCgFnSc4	jeden
říšskou	říšský	k2eAgFnSc4d1	říšská
organizaci	organizace	k1gFnSc4	organizace
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
ji	on	k3xPp3gFnSc4	on
propojil	propojit	k5eAaPmAgMnS	propojit
s	s	k7c7	s
SS	SS	kA	SS
a	a	k8xC	a
SD	SD	kA	SD
a	a	k8xC	a
zákonem	zákon	k1gInSc7	zákon
stanovil	stanovit	k5eAaPmAgMnS	stanovit
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc1	její
činnost	činnost	k1gFnSc1	činnost
nepodléhá	podléhat	k5eNaImIp3nS	podléhat
soudnímu	soudní	k2eAgNnSc3d1	soudní
přezkoumání	přezkoumání	k1gNnSc3	přezkoumání
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
se	s	k7c7	s
hlavní	hlavní	k2eAgFnSc7d1	hlavní
zbraní	zbraň	k1gFnSc7	zbraň
Gestapa	gestapo	k1gNnSc2	gestapo
stalo	stát	k5eAaPmAgNnS	stát
právo	právo	k1gNnSc4	právo
kohokoli	kdokoli	k3yInSc2	kdokoli
zatknout	zatknout	k5eAaPmF	zatknout
a	a	k8xC	a
uvěznit	uvěznit	k5eAaPmF	uvěznit
<g/>
,	,	kIx,	,
třeba	třeba	k6eAd1	třeba
i	i	k9	i
v	v	k7c6	v
koncentračním	koncentrační	k2eAgInSc6d1	koncentrační
táboře	tábor	k1gInSc6	tábor
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
soudního	soudní	k2eAgInSc2d1	soudní
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
Gestapa	gestapo	k1gNnSc2	gestapo
se	se	k3xPyFc4	se
nikdy	nikdy	k6eAd1	nikdy
neprokazovali	prokazovat	k5eNaImAgMnP	prokazovat
legitimací	legitimace	k1gFnSc7	legitimace
s	s	k7c7	s
fotografií	fotografia	k1gFnSc7	fotografia
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
jen	jen	k9	jen
odznakem	odznak	k1gInSc7	odznak
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
Gestapem	gestapo	k1gNnSc7	gestapo
setkali	setkat	k5eAaPmAgMnP	setkat
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
přeceňovali	přeceňovat	k5eAaImAgMnP	přeceňovat
jeho	jeho	k3xOp3gFnSc4	jeho
všudypřítomnost	všudypřítomnost	k1gFnSc4	všudypřítomnost
a	a	k8xC	a
vševědoucnost	vševědoucnost	k1gFnSc4	vševědoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
bylo	být	k5eAaImAgNnS	být
úředníků	úředník	k1gMnPc2	úředník
Gestapa	gestapo	k1gNnPc4	gestapo
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k6eAd1	málo
<g/>
:	:	kIx,	:
například	například	k6eAd1	například
ve	v	k7c6	v
Frankfurtu	Frankfurt	k1gInSc6	Frankfurt
nad	nad	k7c7	nad
Mohanem	Mohan	k1gInSc7	Mohan
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
asi	asi	k9	asi
45	[number]	k4	45
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
a	a	k8xC	a
o	o	k7c4	o
něco	něco	k3yInSc4	něco
větší	veliký	k2eAgInSc1d2	veliký
počet	počet	k1gInSc1	počet
placených	placený	k2eAgMnPc2d1	placený
agentů	agent	k1gMnPc2	agent
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
to	ten	k3xDgNnSc4	ten
větší	veliký	k2eAgInSc4d2	veliký
význam	význam	k1gInSc4	význam
měla	mít	k5eAaImAgNnP	mít
běžná	běžný	k2eAgNnPc1d1	běžné
udání	udání	k1gNnPc1	udání
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
způsobila	způsobit	k5eAaPmAgFnS	způsobit
až	až	k6eAd1	až
80	[number]	k4	80
%	%	kIx~	%
pronásledování	pronásledování	k1gNnSc6	pronásledování
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
asi	asi	k9	asi
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
měla	mít	k5eAaImAgFnS	mít
politický	politický	k2eAgInSc4d1	politický
motiv	motiv	k1gInSc4	motiv
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
osobní	osobní	k2eAgInPc4d1	osobní
spory	spor	k1gInPc4	spor
a	a	k8xC	a
snahu	snaha	k1gFnSc4	snaha
prokázat	prokázat	k5eAaPmF	prokázat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
loajalitu	loajalita	k1gFnSc4	loajalita
<g/>
.	.	kIx.	.
</s>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Diels	Diels	k1gInSc4	Diels
1933	[number]	k4	1933
<g/>
–	–	k?	–
<g/>
1934	[number]	k4	1934
Heinrich	Heinrich	k1gMnSc1	Heinrich
Himmler	Himmler	k1gMnSc1	Himmler
1933	[number]	k4	1933
<g/>
–	–	k?	–
<g/>
1936	[number]	k4	1936
Reinhard	Reinhard	k1gMnSc1	Reinhard
Heydrich	Heydrich	k1gMnSc1	Heydrich
1936	[number]	k4	1936
<g/>
–	–	k?	–
<g/>
1939	[number]	k4	1939
Heinrich	Heinrich	k1gMnSc1	Heinrich
Müller	Müller	k1gMnSc1	Müller
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
Komunisté	komunista	k1gMnPc1	komunista
a	a	k8xC	a
Sociální	sociální	k2eAgMnPc1d1	sociální
demokrati	demokrat	k1gMnPc1	demokrat
<g/>
(	(	kIx(	(
<g/>
A	a	k9	a
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Protisabotážní	protisabotážní	k2eAgFnSc2d1	protisabotážní
(	(	kIx(	(
<g/>
A	a	k9	a
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Reakcionáři	reakcionář	k1gMnPc1	reakcionář
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
liberálové	liberál	k1gMnPc1	liberál
(	(	kIx(	(
<g/>
A	a	k9	a
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
Atentátníci	atentátník	k1gMnPc1	atentátník
(	(	kIx(	(
<g/>
A	a	k9	a
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
Katolíci	katolík	k1gMnPc1	katolík
(	(	kIx(	(
<g/>
B	B	kA	B
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Protestanti	protestant	k1gMnPc1	protestant
(	(	kIx(	(
<g/>
B	B	kA	B
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Svobodní	svobodný	k2eAgMnPc1d1	svobodný
zednáři	zednář	k1gMnPc1	zednář
(	(	kIx(	(
<g/>
B	B	kA	B
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
Židé	Žid	k1gMnPc1	Žid
(	(	kIx(	(
<g/>
B	B	kA	B
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
Centrální	centrální	k2eAgFnSc1d1	centrální
administrativní	administrativní	k2eAgFnSc1d1	administrativní
kancelář	kancelář	k1gFnSc1	kancelář
gestapa	gestapo	k1gNnSc2	gestapo
<g/>
,	,	kIx,	,
zodpovědné	zodpovědný	k2eAgInPc4d1	zodpovědný
za	za	k7c4	za
kartové	kartový	k2eAgFnPc4d1	kartová
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
celém	celý	k2eAgInSc6d1	celý
personálu	personál	k1gInSc6	personál
<g/>
.	.	kIx.	.
</s>
<s>
Oponenti	oponent	k1gMnPc1	oponent
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
D	D	kA	D
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Církve	církev	k1gFnSc2	církev
a	a	k8xC	a
sekty	sekta	k1gFnSc2	sekta
(	(	kIx(	(
<g/>
D	D	kA	D
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
D	D	kA	D
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
Západní	západní	k2eAgNnSc1d1	západní
území	území	k1gNnSc1	území
(	(	kIx(	(
<g/>
D	D	kA	D
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
Kontrašpionáž	kontrašpionáž	k1gFnSc1	kontrašpionáž
(	(	kIx(	(
<g/>
D	D	kA	D
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
Spojenci	spojenec	k1gMnPc1	spojenec
(	(	kIx(	(
<g/>
D	D	kA	D
<g/>
6	[number]	k4	6
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
V	v	k7c6	v
říši	říš	k1gFnSc6	říš
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
Politické	politický	k2eAgFnPc1d1	politická
formace	formace	k1gFnPc1	formace
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
Ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
(	(	kIx(	(
<g/>
E	E	kA	E
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
E	E	kA	E
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
Vykonávala	vykonávat	k5eAaImAgFnS	vykonávat
dozor	dozor	k1gInSc4	dozor
nad	nad	k7c7	nad
pohraničím	pohraničí	k1gNnSc7	pohraničí
Zajišťovala	zajišťovat	k5eAaImAgFnS	zajišťovat
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
německých	německý	k2eAgFnPc2d1	německá
hranic	hranice	k1gFnPc2	hranice
Odhalovala	odhalovat	k5eAaImAgFnS	odhalovat
utečence	utečenec	k1gMnPc4	utečenec
<g/>
,	,	kIx,	,
překupníky	překupník	k1gMnPc4	překupník
a	a	k8xC	a
pašeráky	pašerák	k1gMnPc4	pašerák
I.	I.	kA	I.
-	-	kIx~	-
organizační	organizační	k2eAgFnSc1d1	organizační
<g/>
,	,	kIx,	,
správní	správní	k2eAgFnSc1d1	správní
<g/>
,	,	kIx,	,
osobní	osobní	k2eAgFnSc1d1	osobní
II	II	kA	II
<g/>
.	.	kIx.	.
-	-	kIx~	-
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
a	a	k8xC	a
správní	správní	k2eAgFnSc2d1	správní
záležitosti	záležitost	k1gFnSc2	záležitost
III	III	kA	III
<g/>
.	.	kIx.	.
-	-	kIx~	-
skupina	skupina	k1gFnSc1	skupina
pro	pro	k7c4	pro
zvláštní	zvláštní	k2eAgInPc4d1	zvláštní
úkoly	úkol	k1gInPc4	úkol
IV	IV	kA	IV
<g/>
.	.	kIx.	.
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
exekutiva	exekutiva	k1gFnSc1	exekutiva
(	(	kIx(	(
<g/>
nepřetržitá	přetržitý	k2eNgFnSc1d1	nepřetržitá
služba	služba	k1gFnSc1	služba
pro	pro	k7c4	pro
příjem	příjem	k1gInSc4	příjem
zatčených	zatčený	k1gMnPc2	zatčený
<g/>
,	,	kIx,	,
příjem	příjem	k1gInSc1	příjem
hlášení	hlášení	k1gNnSc2	hlášení
od	od	k7c2	od
konfidentů	konfident	k1gMnPc2	konfident
a	a	k8xC	a
důvěrníků	důvěrník	k1gMnPc2	důvěrník
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
jednotka	jednotka	k1gFnSc1	jednotka
pro	pro	k7c4	pro
operační	operační	k2eAgNnSc4d1	operační
určení	určení	k1gNnSc4	určení
-	-	kIx~	-
formace	formace	k1gFnSc1	formace
pro	pro	k7c4	pro
boj	boj	k1gInSc4	boj
s	s	k7c7	s
odbojovým	odbojový	k2eAgNnSc7d1	odbojové
hnutím	hnutí	k1gNnSc7	hnutí
<g/>
,	,	kIx,	,
provádění	provádění	k1gNnSc3	provádění
důležitých	důležitý	k2eAgFnPc2d1	důležitá
prohlídek	prohlídka	k1gFnPc2	prohlídka
<g/>
,	,	kIx,	,
intervence	intervence	k1gFnSc1	intervence
v	v	k7c6	v
případech	případ	k1gInPc6	případ
sabotáže	sabotáž	k1gFnSc2	sabotáž
a	a	k8xC	a
vloupání	vloupání	k1gNnSc3	vloupání
politického	politický	k2eAgInSc2d1	politický
charakteru	charakter	k1gInSc2	charakter
jednotka	jednotka	k1gFnSc1	jednotka
pro	pro	k7c4	pro
zvláštní	zvláštní	k2eAgNnSc4d1	zvláštní
určení	určení	k1gNnSc4	určení
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
archív	archív	k1gInSc1	archív
úřadovny	úřadovna	k1gFnSc2	úřadovna
(	(	kIx(	(
<g/>
zabavené	zabavený	k2eAgFnPc1d1	zabavená
tiskoviny	tiskovina	k1gFnPc1	tiskovina
<g/>
,	,	kIx,	,
letáky	leták	k1gInPc1	leták
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
V.	V.	kA	V.
-	-	kIx~	-
Kriminální	kriminální	k2eAgFnSc2d1	kriminální
policie	policie	k1gFnSc2	policie
(	(	kIx(	(
<g/>
Kripo	kripo	k1gMnSc1	kripo
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
od	od	k7c2	od
půlky	půlka	k1gFnSc2	půlka
r.	r.	kA	r.
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
Gellately	Gellatela	k1gFnSc2	Gellatela
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
,	,	kIx,	,
The	The	k1gFnSc1	The
Gestapo	gestapo	k1gNnSc1	gestapo
and	and	k?	and
German	German	k1gMnSc1	German
Society	societa	k1gFnSc2	societa
<g/>
:	:	kIx,	:
Enforcing	Enforcing	k1gInSc1	Enforcing
Racial	Racial	k1gInSc1	Racial
Policy	Polica	k1gFnSc2	Polica
1933	[number]	k4	1933
<g/>
-	-	kIx~	-
<g/>
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Press	k1gInSc4	Press
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-0-19820-297-4	[number]	k4	978-0-19820-297-4
VON	von	k1gInSc4	von
LANG	Lang	k1gMnSc1	Lang
<g/>
,	,	kIx,	,
Jochen	Jochen	k2eAgMnSc1d1	Jochen
<g/>
.	.	kIx.	.
</s>
<s>
Gestapo	gestapo	k1gNnSc1	gestapo
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Naše	náš	k3xOp1gNnSc1	náš
vojsko	vojsko	k1gNnSc1	vojsko
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
206	[number]	k4	206
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
599	[number]	k4	599
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Tauchen	Tauchen	k1gInSc1	Tauchen
<g/>
,	,	kIx,	,
Jaromír	Jaromír	k1gMnSc1	Jaromír
<g/>
:	:	kIx,	:
Organizace	organizace	k1gFnSc1	organizace
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
složek	složka	k1gFnPc2	složka
a	a	k8xC	a
správa	správa	k1gFnSc1	správa
na	na	k7c6	na
úseku	úsek	k1gInSc6	úsek
obrany	obrana	k1gFnSc2	obrana
ve	v	k7c4	v
Třetí	třetí	k4xOgFnSc4	třetí
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Právní	právní	k2eAgInPc4d1	právní
a	a	k8xC	a
ekonomické	ekonomický	k2eAgInPc4d1	ekonomický
problémy	problém	k1gInPc4	problém
současnosti	současnost	k1gFnSc2	současnost
IX	IX	kA	IX
<g/>
.	.	kIx.	.
</s>
<s>
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
KEY	KEY	kA	KEY
Publishing	Publishing	k1gInSc4	Publishing
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
s.	s.	k?	s.
151	[number]	k4	151
-	-	kIx~	-
158	[number]	k4	158
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7418	[number]	k4	7418
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
16	[number]	k4	16
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
Gerhard	Gerhard	k1gMnSc1	Gerhard
Paul	Paul	k1gMnSc1	Paul
<g/>
,	,	kIx,	,
Klaus	Klaus	k1gMnSc1	Klaus
<g/>
/	/	kIx~	/
<g/>
Michael	Michael	k1gMnSc1	Michael
Mallman	Mallman	k1gMnSc1	Mallman
<g/>
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s>
Gestapo	gestapo	k1gNnSc4	gestapo
ve	v	k7c6	v
Druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2010	[number]	k4	2010
(	(	kIx(	(
<g/>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1856	[number]	k4	1856
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
gestapo	gestapo	k1gNnSc4	gestapo
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Gestapo	gestapo	k1gNnSc4	gestapo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
O	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
gestapa	gestapo	k1gNnSc2	gestapo
na	na	k7c6	na
území	území	k1gNnSc6	území
Protektorátu	protektorát	k1gInSc2	protektorát
Konečně	konečně	k6eAd1	konečně
vydat	vydat	k5eAaPmF	vydat
seznamy	seznam	k1gInPc4	seznam
gestapáků	gestapáků	k?	gestapáků
</s>
