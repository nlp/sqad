<s>
Nejnižším	nízký	k2eAgNnSc7d3	nejnižší
místem	místo	k1gNnSc7	místo
je	být	k5eAaImIp3nS	být
hladina	hladina	k1gFnSc1	hladina
řeky	řeka	k1gFnSc2	řeka
Seiny	Seina	k1gFnSc2	Seina
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
25	[number]	k4	25
m	m	kA	m
<g/>
,	,	kIx,	,
kolísá	kolísat	k5eAaImIp3nS	kolísat
podle	podle	k7c2	podle
stavu	stav	k1gInSc2	stav
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
u	u	k7c2	u
Pont	Pont	k1gInSc1	Pont
aval	aval	k1gInSc4	aval
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
opouští	opouštět	k5eAaImIp3nS	opouštět
město	město	k1gNnSc4	město
a	a	k8xC	a
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
přirozeným	přirozený	k2eAgInSc7d1	přirozený
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
vrcholek	vrcholek	k1gInSc1	vrcholek
kopce	kopec	k1gInSc2	kopec
Montmartre	Montmartr	k1gInSc5	Montmartr
(	(	kIx(	(
<g/>
130	[number]	k4	130
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
