<s>
Hebrejština	hebrejština	k1gFnSc1	hebrejština
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
jen	jen	k9	jen
pomocí	pomocí	k7c2	pomocí
22	[number]	k4	22
konsonantů	konsonant	k1gInPc2	konsonant
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
abecedu	abeceda	k1gFnSc4	abeceda
(	(	kIx(	(
<g/>
alefbejt	alefbejt	k2eAgInSc1d1	alefbejt
<g/>
/	/	kIx~	/
<g/>
alefbet	alefbet	k1gInSc1	alefbet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
