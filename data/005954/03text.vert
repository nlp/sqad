<s>
Hebrejské	hebrejský	k2eAgNnSc1d1	hebrejské
písmo	písmo	k1gNnSc1	písmo
je	být	k5eAaImIp3nS	být
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
se	se	k3xPyFc4	se
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
hebrejština	hebrejština	k1gFnSc1	hebrejština
<g/>
,	,	kIx,	,
jidiš	jidiš	k1gNnSc1	jidiš
a	a	k8xC	a
ladino	ladino	k1gNnSc1	ladino
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dějin	dějiny	k1gFnPc2	dějiny
se	se	k3xPyFc4	se
podoba	podoba	k1gFnSc1	podoba
písma	písmo	k1gNnSc2	písmo
proměňovala	proměňovat	k5eAaImAgFnS	proměňovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
10	[number]	k4	10
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
používalo	používat	k5eAaImAgNnS	používat
tzv.	tzv.	kA	tzv.
paleohebrejské	paleohebrejský	k2eAgNnSc1d1	paleohebrejský
písmo	písmo	k1gNnSc1	písmo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
vycházelo	vycházet	k5eAaImAgNnS	vycházet
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
fénického	fénický	k2eAgNnSc2d1	fénické
písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
vlivem	vliv	k1gInSc7	vliv
aramejštiny	aramejština	k1gFnSc2	aramejština
se	se	k3xPyFc4	se
však	však	k9	však
k	k	k7c3	k
zápisu	zápis	k1gInSc3	zápis
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
textu	text	k1gInSc2	text
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
užívaly	užívat	k5eAaImAgFnP	užívat
aramejské	aramejský	k2eAgInPc4d1	aramejský
znaky	znak	k1gInPc4	znak
(	(	kIx(	(
<g/>
rovněž	rovněž	k9	rovněž
z	z	k7c2	z
fénického	fénický	k2eAgInSc2d1	fénický
základu	základ	k1gInSc2	základ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
byly	být	k5eAaImAgInP	být
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
uzpůsobeny	uzpůsobit	k5eAaPmNgFnP	uzpůsobit
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
kvadrátního	kvadrátní	k2eAgNnSc2d1	kvadrátní
písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
průběhu	průběh	k1gInSc6	průběh
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
další	další	k2eAgInPc1d1	další
typy	typ	k1gInPc1	typ
hebrejského	hebrejský	k2eAgNnSc2d1	hebrejské
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
výsadní	výsadní	k2eAgFnSc4d1	výsadní
pozici	pozice	k1gFnSc4	pozice
si	se	k3xPyFc3	se
však	však	k9	však
dodnes	dodnes	k6eAd1	dodnes
uchoval	uchovat	k5eAaPmAgInS	uchovat
kvadrátní	kvadrátní	k2eAgInSc1d1	kvadrátní
typ	typ	k1gInSc1	typ
<g/>
.	.	kIx.	.
</s>
<s>
Hebrejština	hebrejština	k1gFnSc1	hebrejština
se	se	k3xPyFc4	se
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
a	a	k8xC	a
zpravidla	zpravidla	k6eAd1	zpravidla
jen	jen	k9	jen
pomocí	pomocí	k7c2	pomocí
22	[number]	k4	22
konsonantů	konsonant	k1gInPc2	konsonant
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
abecedu	abeceda	k1gFnSc4	abeceda
(	(	kIx(	(
<g/>
alefbejt	alefbejt	k2eAgInSc1d1	alefbejt
<g/>
/	/	kIx~	/
<g/>
alefbet	alefbet	k1gInSc1	alefbet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vokály	vokál	k1gInPc1	vokál
(	(	kIx(	(
<g/>
samohlásky	samohláska	k1gFnPc1	samohláska
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
akcenty	akcent	k1gInPc4	akcent
a	a	k8xC	a
případně	případně	k6eAd1	případně
další	další	k2eAgInPc4d1	další
znaky	znak	k1gInPc4	znak
do	do	k7c2	do
abecedy	abeceda	k1gFnSc2	abeceda
nepatří	patřit	k5eNaImIp3nP	patřit
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
jen	jen	k9	jen
jako	jako	k8xS	jako
pomůcka	pomůcka	k1gFnSc1	pomůcka
k	k	k7c3	k
zajištění	zajištění	k1gNnSc3	zajištění
správného	správný	k2eAgNnSc2d1	správné
čtení	čtení	k1gNnSc2	čtení
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
v	v	k7c6	v
případě	případ	k1gInSc6	případ
posvátných	posvátný	k2eAgInPc2d1	posvátný
hebrejských	hebrejský	k2eAgInPc2d1	hebrejský
textů	text	k1gInPc2	text
(	(	kIx(	(
<g/>
nikoli	nikoli	k9	nikoli
ale	ale	k9	ale
u	u	k7c2	u
textů	text	k1gInPc2	text
určených	určený	k2eAgInPc2d1	určený
k	k	k7c3	k
předčítání	předčítání	k1gNnSc3	předčítání
v	v	k7c6	v
synagoze	synagoga	k1gFnSc6	synagoga
–	–	k?	–
ty	ten	k3xDgMnPc4	ten
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
nevokalizované	vokalizovaný	k2eNgFnSc6d1	vokalizovaný
podobě	podoba	k1gFnSc6	podoba
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
pomůcka	pomůcka	k1gFnSc1	pomůcka
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
nebo	nebo	k8xC	nebo
obecně	obecně	k6eAd1	obecně
pro	pro	k7c4	pro
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
hebrejsky	hebrejsky	k6eAd1	hebrejsky
učí	učit	k5eAaImIp3nS	učit
<g/>
.	.	kIx.	.
</s>
<s>
Hebrejské	hebrejský	k2eAgNnSc1d1	hebrejské
písmo	písmo	k1gNnSc1	písmo
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
písma	písmo	k1gNnSc2	písmo
fénického	fénický	k2eAgNnSc2d1	fénické
(	(	kIx(	(
<g/>
foinické	foinický	k2eAgNnSc4d1	foinický
<g/>
,	,	kIx,	,
také	také	k9	také
"	"	kIx"	"
<g/>
kenaanské	kenaanský	k2eAgFnPc4d1	kenaanská
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
ze	z	k7c2	z
syropalestinské	syropalestinský	k2eAgFnSc2d1	syropalestinský
obrázkové	obrázkový	k2eAgFnSc2d1	obrázková
abecedy	abeceda	k1gFnSc2	abeceda
(	(	kIx(	(
<g/>
cca	cca	kA	cca
1800	[number]	k4	1800
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
a	a	k8xC	a
protosinajského	protosinajský	k2eAgNnSc2d1	protosinajský
písma	písmo	k1gNnSc2	písmo
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
1500	[number]	k4	1500
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
dochované	dochovaný	k2eAgInPc1d1	dochovaný
nápisy	nápis	k1gInPc1	nápis
v	v	k7c6	v
samotném	samotný	k2eAgNnSc6d1	samotné
kenaanském	kenaanský	k2eAgNnSc6d1	kenaanské
písmu	písmo	k1gNnSc6	písmo
byly	být	k5eAaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
u	u	k7c2	u
Betléma	Betlém	k1gInSc2	Betlém
a	a	k8xC	a
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
kenaanským	kenaanský	k2eAgNnSc7d1	kenaanské
písmem	písmo	k1gNnSc7	písmo
existovalo	existovat	k5eAaImAgNnS	existovat
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
také	také	k9	také
klínopisné	klínopisný	k2eAgNnSc1d1	klínopisný
ugaritské	ugaritský	k2eAgNnSc1d1	ugaritský
písmo	písmo	k1gNnSc1	písmo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
invaze	invaze	k1gFnSc2	invaze
mořských	mořský	k2eAgInPc2d1	mořský
národů	národ	k1gInPc2	národ
zaniklo	zaniknout	k5eAaPmAgNnS	zaniknout
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
plně	plně	k6eAd1	plně
prosadilo	prosadit	k5eAaPmAgNnS	prosadit
kenaanské	kenaanský	k2eAgNnSc1d1	kenaanské
písmo	písmo	k1gNnSc1	písmo
<g/>
,	,	kIx,	,
založené	založený	k2eAgNnSc1d1	založené
na	na	k7c6	na
konsonantním	konsonantní	k2eAgInSc6d1	konsonantní
principu	princip	k1gInSc6	princip
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
písmo	písmo	k1gNnSc4	písmo
pak	pak	k6eAd1	pak
přejali	přejmout	k5eAaPmAgMnP	přejmout
a	a	k8xC	a
upravili	upravit	k5eAaPmAgMnP	upravit
Řekové	Řek	k1gMnPc1	Řek
i	i	k8xC	i
Římané	Říman	k1gMnPc1	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Izraelité	izraelita	k1gMnPc1	izraelita
kenaanskou	kenaanský	k2eAgFnSc4d1	kenaanská
abecedu	abeceda	k1gFnSc4	abeceda
používali	používat	k5eAaImAgMnP	používat
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
do	do	k7c2	do
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
tento	tento	k3xDgInSc4	tento
jazyk	jazyk	k1gInSc4	jazyk
představoval	představovat	k5eAaImAgMnS	představovat
"	"	kIx"	"
<g/>
lidovou	lidový	k2eAgFnSc4d1	lidová
řeč	řeč	k1gFnSc4	řeč
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
kenaanského	kenaanský	k2eAgNnSc2d1	kenaanské
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
se	se	k3xPyFc4	se
zapisovala	zapisovat	k5eAaImAgFnS	zapisovat
hebrejština	hebrejština	k1gFnSc1	hebrejština
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
paleohebrejské	paleohebrejský	k2eAgNnSc1d1	paleohebrejský
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
hebrejsko-kenaanské	hebrejskoenaanský	k2eAgNnSc1d1	hebrejsko-kenaanský
písmo	písmo	k1gNnSc1	písmo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
typem	typ	k1gInSc7	typ
písma	písmo	k1gNnSc2	písmo
jsou	být	k5eAaImIp3nP	být
zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
nejstarší	starý	k2eAgFnPc1d3	nejstarší
hebrejské	hebrejský	k2eAgFnPc1d1	hebrejská
písemné	písemný	k2eAgFnPc1d1	písemná
památky	památka	k1gFnPc1	památka
pocházející	pocházející	k2eAgFnPc1d1	pocházející
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
izraelského	izraelský	k2eAgMnSc2d1	izraelský
a	a	k8xC	a
judského	judský	k2eAgNnSc2d1	Judské
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Gezerský	Gezerský	k2eAgInSc4d1	Gezerský
kalendář	kalendář	k1gInSc4	kalendář
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Samařská	samařský	k2eAgFnSc1d1	samařská
ostraka	ostrak	k1gMnSc2	ostrak
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Šíloašský	Šíloašský	k2eAgInSc1d1	Šíloašský
nápis	nápis	k1gInSc1	nápis
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
700	[number]	k4	700
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Lakíšská	Lakíšský	k2eAgFnSc1d1	Lakíšský
ostraka	ostrak	k1gMnSc2	ostrak
z	z	k7c2	z
Lachiše	Lachiše	k1gFnSc2	Lachiše
a	a	k8xC	a
Tel	tel	kA	tel
Aradu	Arad	k1gInSc2	Arad
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
600	[number]	k4	600
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
pádem	pád	k1gInSc7	pád
izraelského	izraelský	k2eAgInSc2d1	izraelský
(	(	kIx(	(
<g/>
722	[number]	k4	722
<g/>
)	)	kIx)	)
a	a	k8xC	a
judského	judský	k2eAgNnSc2d1	Judské
království	království	k1gNnSc2	království
(	(	kIx(	(
<g/>
586	[number]	k4	586
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
paleohebrejské	paleohebrejský	k2eAgNnSc1d1	paleohebrejský
písmo	písmo	k1gNnSc1	písmo
vytlačeno	vytlačen	k2eAgNnSc1d1	vytlačeno
do	do	k7c2	do
pozadí	pozadí	k1gNnSc2	pozadí
<g/>
,	,	kIx,	,
nezaniklo	zaniknout	k5eNaPmAgNnS	zaniknout
však	však	k9	však
úplně	úplně	k6eAd1	úplně
–	–	k?	–
užívalo	užívat	k5eAaImAgNnS	užívat
se	se	k3xPyFc4	se
jako	jako	k9	jako
vyjádření	vyjádření	k1gNnPc4	vyjádření
židovské	židovský	k2eAgFnSc2d1	židovská
identity	identita	k1gFnSc2	identita
například	například	k6eAd1	například
na	na	k7c6	na
mincích	mince	k1gFnPc6	mince
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
hasmoneovského	hasmoneovský	k2eAgNnSc2d1	hasmoneovský
království	království	k1gNnSc2	království
(	(	kIx(	(
<g/>
142	[number]	k4	142
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc2	první
židovské	židovská	k1gFnSc2	židovská
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
66	[number]	k4	66
<g/>
-	-	kIx~	-
<g/>
74	[number]	k4	74
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
či	či	k8xC	či
Bar	bar	k1gInSc1	bar
Kochbova	Kochbův	k2eAgNnSc2d1	Kochbovo
povstání	povstání	k1gNnSc2	povstání
(	(	kIx(	(
<g/>
132	[number]	k4	132
<g/>
-	-	kIx~	-
<g/>
135	[number]	k4	135
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kumránské	Kumránský	k2eAgInPc1d1	Kumránský
texty	text	k1gInPc1	text
dokládají	dokládat	k5eAaImIp3nP	dokládat
také	také	k9	také
časté	častý	k2eAgNnSc1d1	časté
užívání	užívání	k1gNnSc1	užívání
paleohebrejského	paleohebrejský	k2eAgNnSc2d1	paleohebrejský
písma	písmo	k1gNnSc2	písmo
pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
posvátných	posvátný	k2eAgInPc2d1	posvátný
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Paleohebrejské	Paleohebrejský	k2eAgNnSc1d1	Paleohebrejský
písmo	písmo	k1gNnSc1	písmo
se	se	k3xPyFc4	se
přestalo	přestat	k5eAaPmAgNnS	přestat
používat	používat	k5eAaImF	používat
až	až	k9	až
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rabíni	rabín	k1gMnPc1	rabín
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
posvátné	posvátný	k2eAgInPc4d1	posvátný
texty	text	k1gInPc4	text
dále	daleko	k6eAd2	daleko
nepoužitelné	použitelný	k2eNgInPc1d1	nepoužitelný
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rostoucím	rostoucí	k2eAgInSc7d1	rostoucí
vlivem	vliv	k1gInSc7	vliv
asyrské	asyrský	k2eAgFnSc2d1	Asyrská
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
prosazovala	prosazovat	k5eAaImAgFnS	prosazovat
také	také	k9	také
aramejština	aramejština	k1gFnSc1	aramejština
a	a	k8xC	a
aramejské	aramejský	k2eAgNnSc1d1	aramejské
písmo	písmo	k1gNnSc1	písmo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
z	z	k7c2	z
kenaanského	kenaanský	k2eAgNnSc2d1	kenaanské
(	(	kIx(	(
<g/>
fénického	fénický	k2eAgNnSc2d1	fénické
<g/>
)	)	kIx)	)
písma	písmo	k1gNnSc2	písmo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
izraelského	izraelský	k2eAgNnSc2d1	izraelské
a	a	k8xC	a
judského	judský	k2eAgNnSc2d1	Judské
království	království	k1gNnSc2	království
přejali	přejmout	k5eAaPmAgMnP	přejmout
aramejštinu	aramejština	k1gFnSc4	aramejština
také	také	k9	také
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
si	se	k3xPyFc3	se
aramejské	aramejský	k2eAgNnSc4d1	aramejské
písmo	písmo	k1gNnSc4	písmo
přizpůsobovali	přizpůsobovat	k5eAaImAgMnP	přizpůsobovat
<g/>
,	,	kIx,	,
až	až	k9	až
ve	v	k7c4	v
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
židovsko-aramejské	židovskoramejský	k2eAgNnSc4d1	židovsko-aramejský
písmo	písmo	k1gNnSc4	písmo
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
to	ten	k3xDgNnSc1	ten
procházelo	procházet	k5eAaImAgNnS	procházet
dalším	další	k2eAgInSc7d1	další
vývojem	vývoj	k1gInSc7	vývoj
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
pak	pak	k6eAd1	pak
kromě	kromě	k7c2	kromě
kurzívy	kurzíva	k1gFnSc2	kurzíva
a	a	k8xC	a
polokurzívy	polokurzíva	k1gFnSc2	polokurzíva
existovalo	existovat	k5eAaImAgNnS	existovat
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
kvadrátní	kvadrátní	k2eAgNnSc1d1	kvadrátní
písmo	písmo	k1gNnSc1	písmo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
100	[number]	k4	100
pak	pak	k6eAd1	pak
rabíni	rabín	k1gMnPc1	rabín
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
kvadrátní	kvadrátní	k2eAgFnSc1d1	kvadrátní
forma	forma	k1gFnSc1	forma
židovsko-aramejského	židovskoramejský	k2eAgNnSc2d1	židovsko-aramejský
písma	písmo	k1gNnSc2	písmo
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
používat	používat	k5eAaImF	používat
k	k	k7c3	k
zápisu	zápis	k1gInSc3	zápis
posvátných	posvátný	k2eAgInPc2d1	posvátný
textů	text	k1gInPc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kvadrátního	kvadrátní	k2eAgNnSc2d1	kvadrátní
písma	písmo	k1gNnSc2	písmo
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dvou	dva	k4xCgNnPc2	dva
tisíciletí	tisíciletí	k1gNnPc2	tisíciletí
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
množství	množství	k1gNnSc1	množství
modifikací	modifikace	k1gFnPc2	modifikace
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
uvádíme	uvádět	k5eAaImIp1nP	uvádět
nejdůležitější	důležitý	k2eAgInPc1d3	nejdůležitější
<g/>
:	:	kIx,	:
Stam	Stam	k1gInSc1	Stam
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
ס	ס	k?	ס
<g/>
"	"	kIx"	"
<g/>
ם	ם	k?	ם
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zkratkou	zkratka	k1gFnSc7	zkratka
pro	pro	k7c4	pro
ס	ס	k?	ס
ת	ת	k?	ת
ת	ת	k?	ת
ו	ו	k?	ו
(	(	kIx(	(
<g/>
Sifrej	Sifrej	k1gMnSc2	Sifrej
Tora	Tor	k1gMnSc2	Tor
<g/>
,	,	kIx,	,
tfilin	tfilin	k2eAgInSc1d1	tfilin
u-mezuzot	uezuzot	k1gInSc1	u-mezuzot
<g/>
,	,	kIx,	,
Svitky	svitek	k1gInPc1	svitek
Tóry	tóra	k1gFnSc2	tóra
<g/>
,	,	kIx,	,
tfilin	tfilina	k1gFnPc2	tfilina
a	a	k8xC	a
mezuzy	mezuza	k1gFnSc2	mezuza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
typem	typ	k1gInSc7	typ
písma	písmo	k1gNnSc2	písmo
jsou	být	k5eAaImIp3nP	být
ručně	ručně	k6eAd1	ručně
psány	psán	k2eAgInPc1d1	psán
svitky	svitek	k1gInPc1	svitek
do	do	k7c2	do
těchto	tento	k3xDgInPc2	tento
předmětů	předmět	k1gInPc2	předmět
a	a	k8xC	a
také	také	k9	také
pět	pět	k4xCc4	pět
svátečních	sváteční	k2eAgInPc2d1	sváteční
svitků	svitek	k1gInPc2	svitek
(	(	kIx(	(
<g/>
Rút	Rút	k1gFnSc1	Rút
<g/>
,	,	kIx,	,
Kazatel	kazatel	k1gMnSc1	kazatel
<g/>
,	,	kIx,	,
Píseň	píseň	k1gFnSc1	píseň
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
Pláč	pláč	k1gInSc1	pláč
Jeremjášův	Jeremjášův	k2eAgInSc1d1	Jeremjášův
a	a	k8xC	a
Ester	ester	k1gInSc1	ester
<g/>
.	.	kIx.	.
</s>
<s>
Typu	typa	k1gFnSc4	typa
písma	písmo	k1gNnSc2	písmo
Stam	Stama	k1gFnPc2	Stama
existují	existovat	k5eAaImIp3nP	existovat
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgInPc4	tři
druhy	druh	k1gInPc4	druh
<g/>
:	:	kIx,	:
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Bejt	Bejt	k?	Bejt
Josef	Josef	k1gMnSc1	Josef
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
díla	dílo	k1gNnSc2	dílo
Josefa	Josefa	k1gFnSc1	Josefa
Kara	kara	k1gFnSc1	kara
<g/>
,	,	kIx,	,
užívaný	užívaný	k2eAgInSc1d1	užívaný
především	především	k6eAd1	především
aškenázskými	aškenázský	k2eAgMnPc7d1	aškenázský
židy	žid	k1gMnPc7	žid
<g/>
)	)	kIx)	)
Arizal	Arizal	k1gFnSc4	Arizal
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
zkratky	zkratka	k1gFnSc2	zkratka
jména	jméno	k1gNnSc2	jméno
Jicchaka	Jicchak	k1gMnSc2	Jicchak
Lurii	Lurie	k1gFnSc6	Lurie
א	א	k?	א
<g/>
"	"	kIx"	"
<g/>
ל	ל	k?	ל
א	א	k?	א
ר	ר	k?	ר
י	י	k?	י
ז	ז	k?	ז
ל	ל	k?	ל
<g/>
,	,	kIx,	,
Aškenazi	Aškenah	k1gMnPc1	Aškenah
rabi	rabi	k1gMnSc1	rabi
Jicchak	Jicchak	k1gMnSc1	Jicchak
<g/>
,	,	kIx,	,
zichrono	zichrona	k1gFnSc5	zichrona
li-vracha	liracha	k1gFnSc1	li-vracha
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
především	především	k9	především
chasidy	chasid	k1gMnPc4	chasid
<g/>
)	)	kIx)	)
Veliš	Veliš	k1gInSc1	Veliš
(	(	kIx(	(
<g/>
užívaný	užívaný	k2eAgInSc1d1	užívaný
sefardskými	sefardský	k2eAgMnPc7d1	sefardský
Židy	Žid	k1gMnPc7	Žid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
v	v	k7c6	v
určitých	určitý	k2eAgInPc6d1	určitý
detailech	detail	k1gInPc6	detail
liší	lišit	k5eAaImIp3nP	lišit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
laika	laik	k1gMnSc4	laik
jsou	být	k5eAaImIp3nP	být
rozdíly	rozdíl	k1gInPc1	rozdíl
neznatelné	znatelný	k2eNgInPc1d1	neznatelný
<g/>
.	.	kIx.	.
</s>
<s>
Charakteristickým	charakteristický	k2eAgInSc7d1	charakteristický
znakem	znak	k1gInSc7	znak
písma	písmo	k1gNnSc2	písmo
Stam	Stam	k1gInSc1	Stam
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
korunky	korunka	k1gFnSc2	korunka
(	(	kIx(	(
<g/>
tagin	tagin	k1gInSc1	tagin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
třemi	tři	k4xCgNnPc7	tři
malými	malý	k2eAgInPc7d1	malý
písmeny	písmeno	k1gNnPc7	písmeno
zajin	zajina	k1gFnPc2	zajina
a	a	k8xC	a
píší	psát	k5eAaImIp3nP	psát
se	se	k3xPyFc4	se
na	na	k7c4	na
všechna	všechen	k3xTgNnPc4	všechen
písmena	písmeno	k1gNnPc4	písmeno
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc4	který
v	v	k7c6	v
sobě	se	k3xPyFc3	se
zajin	zajin	k2eAgMnSc1d1	zajin
"	"	kIx"	"
<g/>
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
šin	šin	k?	šin
<g/>
,	,	kIx,	,
ajin	ajina	k1gFnPc2	ajina
<g/>
,	,	kIx,	,
tet	teta	k1gFnPc2	teta
<g/>
,	,	kIx,	,
nun	nun	k?	nun
<g/>
,	,	kIx,	,
zajin	zajin	k1gMnSc1	zajin
<g/>
,	,	kIx,	,
gimel	gimel	k1gMnSc1	gimel
a	a	k8xC	a
cade	cade	k1gFnSc1	cade
a	a	k8xC	a
koncové	koncový	k2eAgFnPc1d1	koncová
varianty	varianta	k1gFnPc1	varianta
od	od	k7c2	od
nun	nun	k?	nun
a	a	k8xC	a
cade	cadat	k5eAaPmIp3nS	cadat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Aškenázské	Aškenázský	k2eAgNnSc1d1	Aškenázský
písmo	písmo	k1gNnSc1	písmo
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
variant	varianta	k1gFnPc2	varianta
hebrejského	hebrejský	k2eAgNnSc2d1	hebrejské
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
během	během	k7c2	během
středověku	středověk	k1gInSc2	středověk
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Písmo	písmo	k1gNnSc1	písmo
je	být	k5eAaImIp3nS	být
podobného	podobný	k2eAgInSc2d1	podobný
charakteru	charakter	k1gInSc2	charakter
jako	jako	k8xC	jako
latinská	latinský	k2eAgFnSc1d1	Latinská
gotická	gotický	k2eAgFnSc1d1	gotická
minuskule	minuskule	k1gFnSc1	minuskule
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
používáním	používání	k1gNnSc7	používání
stejných	stejná	k1gFnPc2	stejná
psacích	psací	k2eAgInPc2d1	psací
prostředků	prostředek	k1gInPc2	prostředek
(	(	kIx(	(
<g/>
husí	husí	k2eAgInSc1d1	husí
brk	brk	k1gInSc1	brk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
písmem	písmo	k1gNnSc7	písmo
je	být	k5eAaImIp3nS	být
napsána	napsat	k5eAaPmNgFnS	napsat
nebo	nebo	k8xC	nebo
vysázena	vysázet	k5eAaPmNgFnS	vysázet
většina	většina	k1gFnSc1	většina
hebrejských	hebrejský	k2eAgInPc2d1	hebrejský
středověkých	středověký	k2eAgInPc2d1	středověký
manuskriptů	manuskript	k1gInPc2	manuskript
nebo	nebo	k8xC	nebo
tisků	tisk	k1gInPc2	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sefardského	sefardský	k2eAgInSc2d1	sefardský
středověkého	středověký	k2eAgInSc2d1	středověký
skriptu	skript	k1gInSc2	skript
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
dnešní	dnešní	k2eAgFnSc1d1	dnešní
moderní	moderní	k2eAgFnSc1d1	moderní
hebrejská	hebrejský	k2eAgFnSc1d1	hebrejská
abeceda	abeceda	k1gFnSc1	abeceda
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Tzv.	tzv.	kA	tzv.
písmo	písmo	k1gNnSc1	písmo
Raši	Raši	k1gNnSc2	Raši
je	být	k5eAaImIp3nS	být
připisováno	připisovat	k5eAaImNgNnS	připisovat
učenci	učenec	k1gMnPc7	učenec
Rašimu	Rašim	k1gInSc2	Rašim
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
ten	ten	k3xDgMnSc1	ten
jej	on	k3xPp3gNnSc4	on
nikdy	nikdy	k6eAd1	nikdy
nepoužíval	používat	k5eNaImAgMnS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
severoafrickou	severoafrický	k2eAgFnSc4d1	severoafrická
kurzívní	kurzívní	k2eAgFnSc4d1	kurzívní
variantu	varianta	k1gFnSc4	varianta
hebrejského	hebrejský	k2eAgNnSc2d1	hebrejské
písma	písmo	k1gNnSc2	písmo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
v	v	k7c6	v
tištěných	tištěný	k2eAgNnPc6d1	tištěné
vydáních	vydání	k1gNnPc6	vydání
Talmudu	talmud	k1gInSc2	talmud
a	a	k8xC	a
Bible	bible	k1gFnSc2	bible
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
odlišil	odlišit	k5eAaPmAgInS	odlišit
vlastní	vlastní	k2eAgInSc1d1	vlastní
biblický	biblický	k2eAgInSc1d1	biblický
nebo	nebo	k8xC	nebo
talmudický	talmudický	k2eAgInSc1d1	talmudický
text	text	k1gInSc1	text
od	od	k7c2	od
Rašiho	Raši	k1gMnSc2	Raši
komentáře	komentář	k1gInSc2	komentář
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišení	rozlišení	k1gNnSc1	rozlišení
se	se	k3xPyFc4	se
ujalo	ujmout	k5eAaPmAgNnS	ujmout
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
písmo	písmo	k1gNnSc1	písmo
začalo	začít	k5eAaPmAgNnS	začít
nazývat	nazývat	k5eAaImF	nazývat
Raši	Raši	k1gNnSc1	Raši
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
כ	כ	k?	כ
ר	ר	k?	ר
<g/>
"	"	kIx"	"
<g/>
י	י	k?	י
<g/>
)	)	kIx)	)
a	a	k8xC	a
sází	sázet	k5eAaImIp3nS	sázet
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
tradičně	tradičně	k6eAd1	tradičně
všechny	všechen	k3xTgMnPc4	všechen
Rašiho	Raši	k1gMnSc4	Raši
(	(	kIx(	(
<g/>
a	a	k8xC	a
nejen	nejen	k6eAd1	nejen
jeho	jeho	k3xOp3gInSc2	jeho
<g/>
)	)	kIx)	)
komentáře	komentář	k1gInSc2	komentář
až	až	k9	až
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Hebrejská	hebrejský	k2eAgFnSc1d1	hebrejská
abeceda	abeceda	k1gFnSc1	abeceda
je	být	k5eAaImIp3nS	být
jakožto	jakožto	k8xS	jakožto
abeceda	abeceda	k1gFnSc1	abeceda
semitského	semitský	k2eAgInSc2d1	semitský
jazyka	jazyk	k1gInSc2	jazyk
tvořena	tvořen	k2eAgFnSc1d1	tvořena
konsonanty	konsonant	k1gInPc7	konsonant
a	a	k8xC	a
čte	číst	k5eAaImIp3nS	číst
se	se	k3xPyFc4	se
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
<g/>
.	.	kIx.	.
</s>
<s>
Pořadí	pořadí	k1gNnSc1	pořadí
konsonantů	konsonant	k1gInPc2	konsonant
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
pořadí	pořadí	k1gNnSc2	pořadí
ugaritského	ugaritský	k2eAgNnSc2d1	ugaritský
písma	písmo	k1gNnSc2	písmo
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
biblických	biblický	k2eAgInPc6d1	biblický
textech	text	k1gInPc6	text
jej	on	k3xPp3gInSc2	on
dosvědčují	dosvědčovat	k5eAaImIp3nP	dosvědčovat
akrostichické	akrostichický	k2eAgInPc1d1	akrostichický
poetické	poetický	k2eAgInPc1d1	poetický
texty	text	k1gInPc1	text
<g/>
.	.	kIx.	.
</s>
<s>
Znaků	znak	k1gInPc2	znak
hebrejské	hebrejský	k2eAgFnSc2d1	hebrejská
abecedy	abeceda	k1gFnSc2	abeceda
je	být	k5eAaImIp3nS	být
22	[number]	k4	22
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
pět	pět	k4xCc4	pět
má	mít	k5eAaImIp3nS	mít
krom	krom	k7c2	krom
základního	základní	k2eAgInSc2d1	základní
tvaru	tvar	k1gInSc2	tvar
ještě	ještě	k9	ještě
odlišný	odlišný	k2eAgInSc4d1	odlišný
druhý	druhý	k4xOgInSc4	druhý
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jen	jen	k9	jen
na	na	k7c6	na
koncích	konec	k1gInPc6	konec
slov	slovo	k1gNnPc2	slovo
(	(	kIx(	(
<g/>
ץ	ץ	k?	ץ
,	,	kIx,	,
<g/>
ף	ף	k?	ף
,	,	kIx,	,
<g/>
ן	ן	k?	ן
,	,	kIx,	,
<g/>
ם	ם	k?	ם
,	,	kIx,	,
<g/>
ך	ך	k?	ך
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
koncového	koncový	k2eAgMnSc2d1	koncový
kaf	kaf	k?	kaf
(	(	kIx(	(
<g/>
ך	ך	k?	ך
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
ve	v	k7c6	v
vokalizovaném	vokalizovaný	k2eAgInSc6d1	vokalizovaný
textu	text	k1gInSc6	text
kvůli	kvůli	k7c3	kvůli
jasnému	jasný	k2eAgNnSc3d1	jasné
odlišení	odlišení	k1gNnSc3	odlišení
od	od	k7c2	od
koncového	koncový	k2eAgInSc2d1	koncový
nun	nun	k?	nun
(	(	kIx(	(
<g/>
ן	ן	k?	ן
<g/>
)	)	kIx)	)
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
šva	šva	k1gNnSc1	šva
<g/>
.	.	kIx.	.
</s>
<s>
Slova	slovo	k1gNnPc1	slovo
se	se	k3xPyFc4	se
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
hebrejštině	hebrejština	k1gFnSc6	hebrejština
nemohou	moct	k5eNaImIp3nP	moct
dělit	dělit	k5eAaImF	dělit
<g/>
,	,	kIx,	,
z	z	k7c2	z
estetických	estetický	k2eAgInPc2d1	estetický
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
písmena	písmeno	k1gNnPc4	písmeno
ת	ת	k?	ת
,	,	kIx,	,
<g/>
ם	ם	k?	ם
,	,	kIx,	,
<g/>
ל	ל	k?	ל
,	,	kIx,	,
<g/>
ה	ה	k?	ה
,	,	kIx,	,
<g/>
א	א	k?	א
mohou	moct	k5eAaImIp3nP	moct
graficky	graficky	k6eAd1	graficky
roztáhnout	roztáhnout	k5eAaPmF	roztáhnout
do	do	k7c2	do
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hebrejské	hebrejský	k2eAgFnSc2d1	hebrejská
číslice	číslice	k1gFnSc2	číslice
<g/>
.	.	kIx.	.
</s>
<s>
Hebrejské	hebrejský	k2eAgFnPc1d1	hebrejská
číslice	číslice	k1gFnPc1	číslice
se	se	k3xPyFc4	se
zprvu	zprvu	k6eAd1	zprvu
zapisovaly	zapisovat	k5eAaImAgInP	zapisovat
pomocí	pomocí	k7c2	pomocí
svislých	svislý	k2eAgFnPc2d1	svislá
čárek	čárka	k1gFnPc2	čárka
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
6	[number]	k4	6
se	se	k3xPyFc4	se
zapisovalo	zapisovat	k5eAaImAgNnS	zapisovat
jako	jako	k9	jako
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
dostaly	dostat	k5eAaPmAgFnP	dostat
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
helénismu	helénismus	k1gInSc2	helénismus
znaky	znak	k1gInPc1	znak
svou	svůj	k3xOyFgFnSc4	svůj
číselnou	číselný	k2eAgFnSc4d1	číselná
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Číslo	číslo	k1gNnSc1	číslo
632	[number]	k4	632
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
například	například	k6eAd1	například
zapisováno	zapisován	k2eAgNnSc1d1	zapisováno
jako	jako	k8xS	jako
ת	ת	k?	ת
<g/>
״	״	k?	״
<g/>
ב	ב	k?	ב
(	(	kIx(	(
<g/>
400	[number]	k4	400
<g/>
+	+	kIx~	+
<g/>
200	[number]	k4	200
<g/>
+	+	kIx~	+
<g/>
30	[number]	k4	30
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
dvě	dva	k4xCgFnPc1	dva
svislé	svislý	k2eAgFnPc1d1	svislá
čárky	čárka	k1gFnPc1	čárka
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c6	o
číslici	číslice	k1gFnSc6	číslice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
biblických	biblický	k2eAgInPc6d1	biblický
textech	text	k1gInPc6	text
se	se	k3xPyFc4	se
tohoto	tento	k3xDgInSc2	tento
způsobu	způsob	k1gInSc2	způsob
zápisu	zápis	k1gInSc2	zápis
čísel	číslo	k1gNnPc2	číslo
neužívá	užívat	k5eNaImIp3nS	užívat
a	a	k8xC	a
čísla	číslo	k1gNnPc1	číslo
se	se	k3xPyFc4	se
rozepisují	rozepisovat	k5eAaImIp3nP	rozepisovat
slovy	slovo	k1gNnPc7	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
v	v	k7c6	v
biblických	biblický	k2eAgInPc6d1	biblický
textech	text	k1gInPc6	text
neobjevují	objevovat	k5eNaImIp3nP	objevovat
zkratky	zkratka	k1gFnPc1	zkratka
<g/>
,	,	kIx,	,
středověké	středověký	k2eAgInPc1d1	středověký
hebrejské	hebrejský	k2eAgInPc1d1	hebrejský
texty	text	k1gInPc1	text
se	se	k3xPyFc4	se
však	však	k9	však
zkratkami	zkratka	k1gFnPc7	zkratka
hemží	hemžit	k5eAaImIp3nS	hemžit
<g/>
.	.	kIx.	.
</s>
<s>
Zkratky	zkratka	k1gFnPc1	zkratka
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
vyznačeny	vyznačit	k5eAaPmNgFnP	vyznačit
svislou	svislý	k2eAgFnSc7d1	svislá
čárkou	čárka	k1gFnSc7	čárka
(	(	kIx(	(
<g/>
ו	ו	k?	ו
<g/>
׳	׳	k?	׳
=	=	kIx~	=
ו	ו	k?	ו
"	"	kIx"	"
<g/>
a	a	k8xC	a
tak	tak	k9	tak
dále	daleko	k6eAd2	daleko
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
akronymy	akronym	k1gInPc1	akronym
pak	pak	k8xC	pak
dvěma	dva	k4xCgFnPc7	dva
svislými	svislý	k2eAgFnPc7d1	svislá
čárkami	čárka	k1gFnPc7	čárka
(	(	kIx(	(
<g/>
ת	ת	k?	ת
<g/>
״	״	k?	״
<g/>
ך	ך	k?	ך
=	=	kIx~	=
ת	ת	k?	ת
נ	נ	k?	נ
כ	כ	k?	כ
Tanach	Tanach	k1gInSc1	Tanach
=	=	kIx~	=
"	"	kIx"	"
<g/>
Zákon	zákon	k1gInSc1	zákon
<g/>
,	,	kIx,	,
Proroci	prorok	k1gMnPc5	prorok
[	[	kIx(	[
<g/>
a	a	k8xC	a
<g/>
]	]	kIx)	]
Spisy	spis	k1gInPc1	spis
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fonologie	fonologie	k1gFnSc2	fonologie
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
.	.	kIx.	.
</s>
<s>
Přesnou	přesný	k2eAgFnSc4d1	přesná
výslovnost	výslovnost	k1gFnSc4	výslovnost
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
znaků	znak	k1gInPc2	znak
stanovili	stanovit	k5eAaPmAgMnP	stanovit
až	až	k9	až
masoreti	masoret	k1gMnPc1	masoret
v	v	k7c6	v
9	[number]	k4	9
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
znaků	znak	k1gInPc2	znak
proto	proto	k8xC	proto
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
vyslovovaly	vyslovovat	k5eAaImAgFnP	vyslovovat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
tisíciletí	tisíciletí	k1gNnSc2	tisíciletí
se	se	k3xPyFc4	se
výslovnost	výslovnost	k1gFnSc1	výslovnost
dále	daleko	k6eAd2	daleko
proměňovala	proměňovat	k5eAaImAgFnS	proměňovat
a	a	k8xC	a
diferenciovala	diferenciovat	k5eAaImAgFnS	diferenciovat
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
základní	základní	k2eAgInPc4d1	základní
typy	typ	k1gInPc4	typ
<g/>
:	:	kIx,	:
sefardskou	sefardský	k2eAgFnSc4d1	sefardská
a	a	k8xC	a
aškenázskou	aškenázský	k2eAgFnSc4d1	aškenázská
a	a	k8xC	a
jemenskou	jemenský	k2eAgFnSc4d1	Jemenská
výslovnost	výslovnost	k1gFnSc4	výslovnost
<g/>
.	.	kIx.	.
</s>
<s>
Výslovnost	výslovnost	k1gFnSc1	výslovnost
moderní	moderní	k2eAgFnSc2d1	moderní
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
výslovnosti	výslovnost	k1gFnPc4	výslovnost
sefardské	sefardský	k2eAgFnPc4d1	sefardská
<g/>
,	,	kIx,	,
mnozí	mnohý	k2eAgMnPc1d1	mnohý
Izraelci	Izraelec	k1gMnPc1	Izraelec
si	se	k3xPyFc3	se
však	však	k9	však
ponechali	ponechat	k5eAaPmAgMnP	ponechat
aškenázskou	aškenázský	k2eAgFnSc4d1	aškenázská
výslovnost	výslovnost	k1gFnSc4	výslovnost
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
hebrejštině	hebrejština	k1gFnSc6	hebrejština
existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc1	dva
skupiny	skupina	k1gFnPc1	skupina
specifických	specifický	k2eAgInPc2d1	specifický
konsonantů	konsonant	k1gInPc2	konsonant
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
pak	pak	k6eAd1	pak
zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
způsobem	způsob	k1gInSc7	způsob
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
výslovnost	výslovnost	k1gFnSc4	výslovnost
<g/>
,	,	kIx,	,
tvorbu	tvorba	k1gFnSc4	tvorba
i	i	k8xC	i
význam	význam	k1gInSc4	význam
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
skupinu	skupina	k1gFnSc4	skupina
tvoří	tvořit	k5eAaImIp3nS	tvořit
tzv.	tzv.	kA	tzv.
skupina	skupina	k1gFnSc1	skupina
"	"	kIx"	"
<g/>
begadkefat	begadkefat	k5eAaPmF	begadkefat
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
ת	ת	k?	ת
פ	פ	k?	פ
כ	כ	k?	כ
ד	ד	k?	ד
כ	כ	k?	כ
ד	ד	k?	ד
ג	ג	k?	ג
ב	ב	k?	ב
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
znaky	znak	k1gInPc1	znak
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyslovovat	vyslovovat	k5eAaImF	vyslovovat
dvojím	dvojí	k4xRgInSc7	dvojí
způsobem	způsob	k1gInSc7	způsob
<g/>
:	:	kIx,	:
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
<g/>
-li	i	k?	-li
dageš	dagat	k5eAaPmIp2nS	dagat
(	(	kIx(	(
<g/>
tečku	tečka	k1gFnSc4	tečka
uprostřed	uprostřed	k7c2	uprostřed
písmene	písmeno	k1gNnSc2	písmeno
<g/>
,	,	kIx,	,
např.	např.	kA	např.
כ	כ	k?	כ
<g/>
ּ	ּ	k?	ּ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
se	se	k3xPyFc4	se
explozivně	explozivně	k6eAd1	explozivně
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
raženě	raženě	k6eAd1	raženě
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
b	b	k?	b
g	g	kA	g
d	d	k?	d
k	k	k7c3	k
p	p	k?	p
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
neobsahují	obsahovat	k5eNaImIp3nP	obsahovat
<g/>
-li	i	k?	-li
dageš	dagat	k5eAaPmIp2nS	dagat
<g/>
,	,	kIx,	,
vyslovují	vyslovovat	k5eAaImIp3nP	vyslovovat
se	se	k3xPyFc4	se
spirantně	spirantně	k6eAd1	spirantně
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
třeně	třeň	k1gInPc1	třeň
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
v	v	k7c6	v
g	g	kA	g
d	d	k?	d
ch	ch	k0	ch
f	f	k?	f
t	t	k?	t
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
skupinu	skupina	k1gFnSc4	skupina
tvoří	tvořit	k5eAaImIp3nP	tvořit
laryngály	laryngála	k1gFnPc1	laryngála
(	(	kIx(	(
<g/>
hrdelnice	hrdelnice	k1gFnPc1	hrdelnice
<g/>
,	,	kIx,	,
ע	ע	k?	ע
ח	ח	k?	ח
ה	ה	k?	ה
א	א	k?	א
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
se	se	k3xPyFc4	se
často	často	k6eAd1	často
připojuje	připojovat	k5eAaImIp3nS	připojovat
reš	reš	k?	reš
(	(	kIx(	(
<g/>
ר	ר	k?	ר
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hebrejština	hebrejština	k1gFnSc1	hebrejština
disponuje	disponovat	k5eAaBmIp3nS	disponovat
několika	několik	k4yIc7	několik
prostředky	prostředek	k1gInPc7	prostředek
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
pozměnit	pozměnit	k5eAaPmF	pozměnit
výslovnost	výslovnost	k1gFnSc1	výslovnost
některých	některý	k3yIgInPc2	některý
konsonantů	konsonant	k1gInPc2	konsonant
<g/>
.	.	kIx.	.
</s>
<s>
Dageš	Dagat	k5eAaBmIp2nS	Dagat
(	(	kIx(	(
<g/>
tečka	tečka	k1gFnSc1	tečka
uprostřed	uprostřed	k7c2	uprostřed
písmene	písmeno	k1gNnSc2	písmeno
<g/>
)	)	kIx)	)
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
prodloužení	prodloužení	k1gNnSc4	prodloužení
či	či	k8xC	či
důraznou	důrazný	k2eAgFnSc4d1	důrazná
výslovnost	výslovnost	k1gFnSc4	výslovnost
konsonantu	konsonant	k1gInSc2	konsonant
(	(	kIx(	(
<g/>
dageš	dagat	k5eAaPmIp2nS	dagat
forte	forte	k6eAd1	forte
<g/>
,	,	kIx,	,
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
hebrejštině	hebrejština	k1gFnSc6	hebrejština
dageš	dagat	k5eAaPmIp2nS	dagat
tuto	tento	k3xDgFnSc4	tento
vlastnost	vlastnost	k1gFnSc4	vlastnost
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dageš	Dagat	k5eAaBmIp2nS	Dagat
také	také	k9	také
označuje	označovat	k5eAaImIp3nS	označovat
explozivní	explozivní	k2eAgFnSc1d1	explozivní
výslovnost	výslovnost	k1gFnSc1	výslovnost
konsonantů	konsonant	k1gInPc2	konsonant
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
"	"	kIx"	"
<g/>
begadkefat	begadkefat	k5eAaPmF	begadkefat
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
dageš	dagat	k5eAaPmIp2nS	dagat
lene	lene	k1gFnSc1	lene
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgMnSc3	ten
spirantní	spirantní	k2eAgFnSc1d1	spirantní
výslovnost	výslovnost	k1gFnSc1	výslovnost
lze	lze	k6eAd1	lze
zdůraznit	zdůraznit	k5eAaPmF	zdůraznit
pomocí	pomocí	k7c2	pomocí
horizontální	horizontální	k2eAgFnSc2d1	horizontální
čárky	čárka	k1gFnSc2	čárka
nad	nad	k7c7	nad
konsonantem	konsonant	k1gInSc7	konsonant
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgInPc4d1	nazývaný
rafe	raf	k1gInPc4	raf
<g/>
.	.	kIx.	.
</s>
<s>
Graficky	graficky	k6eAd1	graficky
je	on	k3xPp3gInPc4	on
dageš	dagat	k5eAaPmIp2nS	dagat
podobné	podobný	k2eAgNnSc1d1	podobné
znaménku	znaménko	k1gNnSc6	znaménko
mappik	mappik	k1gInSc4	mappik
(	(	kIx(	(
<g/>
ה	ה	k?	ה
<g/>
ּ	ּ	k?	ּ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
písmene	písmeno	k1gNnSc2	písmeno
het	het	k?	het
(	(	kIx(	(
<g/>
ה	ה	k?	ה
<g/>
)	)	kIx)	)
na	na	k7c6	na
konci	konec	k1gInSc6	konec
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Mappik	Mappik	k1gMnSc1	Mappik
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc1	tento
het	het	k?	het
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
vyslovit	vyslovit	k5eAaPmF	vyslovit
jako	jako	k9	jako
konsonant	konsonant	k1gInSc4	konsonant
h	h	k?	h
(	(	kIx(	(
<g/>
a	a	k8xC	a
nemá	mít	k5eNaImIp3nS	mít
tedy	tedy	k9	tedy
funkci	funkce	k1gFnSc4	funkce
vokálu	vokál	k1gInSc2	vokál
–	–	k?	–
mater	mater	k1gFnSc2	mater
lectionis	lectionis	k1gFnSc2	lectionis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Nikud	Nikud	k1gInSc1	Nikud
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
vokály	vokál	k1gInPc7	vokál
nezapisovaly	zapisovat	k5eNaImAgInP	zapisovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
však	však	k9	však
nezřídka	nezřídka	k6eAd1	nezřídka
způsobovalo	způsobovat	k5eAaImAgNnS	způsobovat
nejednoznačnou	jednoznačný	k2eNgFnSc4d1	nejednoznačná
výslovnost	výslovnost	k1gFnSc4	výslovnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
nejasný	jasný	k2eNgInSc1d1	nejasný
význam	význam	k1gInSc1	význam
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
začala	začít	k5eAaPmAgFnS	začít
uplatňovat	uplatňovat	k5eAaImF	uplatňovat
lineární	lineární	k2eAgFnSc1d1	lineární
vokalizace	vokalizace	k1gFnSc1	vokalizace
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
ovšem	ovšem	k9	ovšem
nevytvořila	vytvořit	k5eNaPmAgFnS	vytvořit
nové	nový	k2eAgInPc4d1	nový
znaky	znak	k1gInPc4	znak
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
k	k	k7c3	k
naznačení	naznačení	k1gNnSc3	naznačení
samohlásek	samohláska	k1gFnPc2	samohláska
používala	používat	k5eAaImAgFnS	používat
již	již	k6eAd1	již
existující	existující	k2eAgInPc4d1	existující
symboly	symbol	k1gInPc4	symbol
souhlásek	souhláska	k1gFnPc2	souhláska
(	(	kIx(	(
<g/>
י	י	k?	י
ו	ו	k?	ו
ה	ה	k?	ה
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
א	א	k?	א
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
čtyři	čtyři	k4xCgInPc1	čtyři
znaky	znak	k1gInPc1	znak
tedy	tedy	k9	tedy
mohou	moct	k5eAaImIp3nP	moct
v	v	k7c6	v
hebrejském	hebrejský	k2eAgInSc6d1	hebrejský
textu	text	k1gInSc6	text
označovat	označovat	k5eAaImF	označovat
jak	jak	k6eAd1	jak
konsonanty	konsonant	k1gInPc4	konsonant
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
vokály	vokál	k1gInPc1	vokál
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
čtyři	čtyři	k4xCgInPc1	čtyři
znaky	znak	k1gInPc1	znak
(	(	kIx(	(
<g/>
nazývané	nazývaný	k2eAgNnSc1d1	nazývané
v	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
matres	matres	k1gMnSc1	matres
lectionis	lectionis	k1gFnSc2	lectionis
–	–	k?	–
"	"	kIx"	"
<g/>
matky	matka	k1gFnPc1	matka
čtení	čtení	k1gNnSc2	čtení
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
mohly	moct	k5eAaImAgInP	moct
ovšem	ovšem	k9	ovšem
označovat	označovat	k5eAaImF	označovat
více	hodně	k6eAd2	hodně
vokálů	vokál	k1gInPc2	vokál
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
mohly	moct	k5eAaImAgFnP	moct
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
původní	původní	k2eAgInSc4d1	původní
plnohodnotný	plnohodnotný	k2eAgInSc4d1	plnohodnotný
konsonant	konsonant	k1gInSc4	konsonant
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
symbol	symbol	k1gInSc1	symbol
ו	ו	k?	ו
(	(	kIx(	(
<g/>
vav	vav	k?	vav
<g/>
)	)	kIx)	)
mohl	moct	k5eAaImAgInS	moct
označovat	označovat	k5eAaImF	označovat
pomocný	pomocný	k2eAgInSc1d1	pomocný
znak	znak	k1gInSc1	znak
pro	pro	k7c4	pro
vokály	vokál	k1gInPc4	vokál
o	o	k7c4	o
a	a	k8xC	a
u	u	k7c2	u
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
pro	pro	k7c4	pro
konsonant	konsonant	k1gInSc4	konsonant
v	v	k7c4	v
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
lineární	lineární	k2eAgFnSc2d1	lineární
vokalizace	vokalizace	k1gFnSc2	vokalizace
navíc	navíc	k6eAd1	navíc
zůstal	zůstat	k5eAaPmAgMnS	zůstat
nepovinným	povinný	k2eNgMnSc7d1	nepovinný
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
dva	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
psaní	psaní	k1gNnSc2	psaní
<g/>
:	:	kIx,	:
v	v	k7c6	v
případě	případ	k1gInSc6	případ
užití	užití	k1gNnSc2	užití
mater	mater	k1gFnSc2	mater
lectionis	lectionis	k1gFnSc2	lectionis
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
"	"	kIx"	"
<g/>
plném	plný	k2eAgNnSc6d1	plné
psaní	psaní	k1gNnSc6	psaní
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
scriptio	scriptio	k6eAd1	scriptio
plena	plena	k1gFnSc1	plena
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
י	י	k?	י
<g/>
)	)	kIx)	)
a	a	k8xC	a
o	o	k7c6	o
"	"	kIx"	"
<g/>
defektivním	defektivní	k2eAgNnSc6d1	defektivní
psaní	psaní	k1gNnSc6	psaní
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
scriptio	scriptio	k6eAd1	scriptio
defectiva	defectiva	k1gFnSc1	defectiva
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
י	י	k?	י
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výslovnosti	výslovnost	k1gFnSc6	výslovnost
ani	ani	k8xC	ani
významu	význam	k1gInSc6	význam
slova	slovo	k1gNnSc2	slovo
není	být	k5eNaImIp3nS	být
žádný	žádný	k3yNgInSc1	žádný
rozdíl	rozdíl	k1gInSc1	rozdíl
(	(	kIx(	(
<g/>
v	v	k7c6	v
obou	dva	k4xCgInPc6	dva
případech	případ	k1gInPc6	případ
jakumu	jakum	k1gInSc2	jakum
=	=	kIx~	=
"	"	kIx"	"
<g/>
povstanou	povstat	k5eAaPmIp3nP	povstat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nedokonalosti	nedokonalost	k1gFnSc3	nedokonalost
lineární	lineární	k2eAgFnSc2d1	lineární
vokalizace	vokalizace	k1gFnSc2	vokalizace
se	se	k3xPyFc4	se
přibližně	přibližně	k6eAd1	přibližně
od	od	k7c2	od
5	[number]	k4	5
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
n.	n.	k?	n.
l.	l.	k?	l.
začaly	začít	k5eAaPmAgFnP	začít
vytvářet	vytvářet	k5eAaImF	vytvářet
další	další	k2eAgInPc4d1	další
vokalizační	vokalizační	k2eAgInPc4d1	vokalizační
systémy	systém	k1gInPc4	systém
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
jednak	jednak	k8xC	jednak
usnadnily	usnadnit	k5eAaPmAgFnP	usnadnit
četbu	četba	k1gFnSc4	četba
posvátných	posvátný	k2eAgInPc2d1	posvátný
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
aby	aby	kYmCp3nP	aby
zajistily	zajistit	k5eAaPmAgFnP	zajistit
jednoznačný	jednoznačný	k2eAgInSc4d1	jednoznačný
smysl	smysl	k1gInSc4	smysl
jejich	jejich	k3xOp3gNnPc2	jejich
slov	slovo	k1gNnPc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
tak	tak	k6eAd1	tak
samaritánská	samaritánský	k2eAgFnSc1d1	samaritánská
<g/>
,	,	kIx,	,
palestinská	palestinský	k2eAgFnSc1d1	palestinská
<g/>
,	,	kIx,	,
babylonská	babylonský	k2eAgFnSc1d1	Babylonská
a	a	k8xC	a
tiberiadská	tiberiadský	k2eAgFnSc1d1	tiberiadský
vokalizace	vokalizace	k1gFnSc1	vokalizace
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
prosadil	prosadit	k5eAaPmAgInS	prosadit
tiberiadský	tiberiadský	k2eAgInSc1d1	tiberiadský
systém	systém	k1gInSc1	systém
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fonologie	fonologie	k1gFnSc2	fonologie
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
.	.	kIx.	.
</s>
<s>
Tiberiadská	Tiberiadský	k2eAgFnSc1d1	Tiberiadský
vokalizace	vokalizace	k1gFnSc1	vokalizace
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
pouze	pouze	k6eAd1	pouze
kvalitu	kvalita	k1gFnSc4	kvalita
(	(	kIx(	(
<g/>
barvu	barva	k1gFnSc4	barva
zvuku	zvuk	k1gInSc2	zvuk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
kvantitu	kvantita	k1gFnSc4	kvantita
(	(	kIx(	(
<g/>
délku	délka	k1gFnSc4	délka
<g/>
)	)	kIx)	)
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
hlásek	hláska	k1gFnPc2	hláska
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
vokály	vokál	k1gInPc1	vokál
se	se	k3xPyFc4	se
od	od	k7c2	od
sebe	se	k3xPyFc2	se
lišily	lišit	k5eAaImAgFnP	lišit
jen	jen	k6eAd1	jen
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
historická	historický	k2eAgFnSc1d1	historická
výslovnost	výslovnost	k1gFnSc1	výslovnost
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
vokálů	vokál	k1gInPc2	vokál
je	být	k5eAaImIp3nS	být
nejasná	jasný	k2eNgFnSc1d1	nejasná
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
hypotetické	hypotetický	k2eAgInPc1d1	hypotetický
pak	pak	k6eAd1	pak
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
úvahy	úvaha	k1gFnPc4	úvaha
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
vokálů	vokál	k1gInPc2	vokál
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
plných	plný	k2eAgInPc2d1	plný
vokálů	vokál	k1gInPc2	vokál
(	(	kIx(	(
<g/>
ָ	ָ	k?	ָ
ַ	ַ	k?	ַ
ֶ	ֶ	k?	ֶ
ֵ	ֵ	k?	ֵ
ִ	ִ	k?	ִ
ֻ	ֻ	k?	ֻ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
lze	lze	k6eAd1	lze
v	v	k7c6	v
hebrejštině	hebrejština	k1gFnSc6	hebrejština
kombinovat	kombinovat	k5eAaImF	kombinovat
s	s	k7c7	s
lineární	lineární	k2eAgFnSc7d1	lineární
vokalizací	vokalizace	k1gFnSc7	vokalizace
(	(	kIx(	(
<g/>
mater	mater	k1gFnSc1	mater
lectionis	lectionis	k1gFnSc2	lectionis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pak	pak	k6eAd1	pak
vznikají	vznikat	k5eAaImIp3nP	vznikat
kombinace	kombinace	k1gFnPc1	kombinace
jako	jako	k8xC	jako
ִ	ִ	k?	ִ
<g/>
י	י	k?	י
<g/>
,	,	kIx,	,
ֵ	ֵ	k?	ֵ
<g/>
י	י	k?	י
<g/>
,	,	kIx,	,
ֶ	ֶ	k?	ֶ
<g/>
י	י	k?	י
<g/>
,	,	kIx,	,
ו	ו	k?	ו
<g/>
ֹ	ֹ	k?	ֹ
<g/>
,	,	kIx,	,
ו	ו	k?	ו
<g/>
ּ	ּ	k?	ּ
<g/>
,	,	kIx,	,
ֶ	ֶ	k?	ֶ
<g/>
ה	ה	k?	ה
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
hebrejštině	hebrejština	k1gFnSc6	hebrejština
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
znak	znak	k1gInSc1	znak
šva	šva	k1gNnSc2	šva
(	(	kIx(	(
<g/>
zapisovaný	zapisovaný	k2eAgInSc1d1	zapisovaný
jako	jako	k8xC	jako
dvojtečka	dvojtečka	k1gFnSc1	dvojtečka
pod	pod	k7c7	pod
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Šva	šva	k1gNnSc1	šva
se	se	k3xPyFc4	se
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
buď	buď	k8xC	buď
jako	jako	k8xC	jako
redukovaná	redukovaný	k2eAgFnSc1d1	redukovaná
samohláska	samohláska	k1gFnSc1	samohláska
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
šva	šva	k1gNnSc1	šva
v	v	k7c6	v
International	International	k1gFnSc6	International
Phonetic	Phonetice	k1gFnPc2	Phonetice
Alphabet	Alphabeta	k1gFnPc2	Alphabeta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
zapisuje	zapisovat	k5eAaImIp3nS	zapisovat
jako	jako	k9	jako
ə	ə	k?	ə
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
nevyslovuje	vyslovovat	k5eNaImIp3nS	vyslovovat
vůbec	vůbec	k9	vůbec
(	(	kIx(	(
<g/>
jde	jít	k5eAaImIp3nS	jít
pak	pak	k6eAd1	pak
o	o	k7c4	o
tzv.	tzv.	kA	tzv.
němé	němý	k2eAgNnSc1d1	němé
písmeno	písmeno	k1gNnSc1	písmeno
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Akcenty	akcent	k1gInPc1	akcent
plní	plnit	k5eAaImIp3nP	plnit
v	v	k7c6	v
hebrejštině	hebrejština	k1gFnSc6	hebrejština
trojí	trojit	k5eAaImIp3nS	trojit
funkci	funkce	k1gFnSc4	funkce
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
<s>
Jednak	jednak	k8xC	jednak
označují	označovat	k5eAaImIp3nP	označovat
přízvučné	přízvučný	k2eAgFnPc4d1	přízvučná
slabiky	slabika	k1gFnPc4	slabika
slova	slovo	k1gNnSc2	slovo
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
funkci	funkce	k1gFnSc4	funkce
interpunkčního	interpunkční	k2eAgNnSc2d1	interpunkční
znaménka	znaménko	k1gNnSc2	znaménko
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
vyznačení	vyznačení	k1gNnSc3	vyznačení
melodie	melodie	k1gFnSc2	melodie
při	při	k7c6	při
liturgickém	liturgický	k2eAgInSc6d1	liturgický
přednesu	přednes	k1gInSc6	přednes
textu	text	k1gInSc2	text
<g/>
.	.	kIx.	.
</s>
<s>
Akcent	akcent	k1gInSc1	akcent
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
kvalitu	kvalita	k1gFnSc4	kvalita
vokálů	vokál	k1gInPc2	vokál
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
pauzální	pauzální	k2eAgInSc4d1	pauzální
tvar	tvar	k1gInSc4	tvar
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
také	také	k9	také
může	moct	k5eAaImIp3nS	moct
dokonce	dokonce	k9	dokonce
určovat	určovat	k5eAaImF	určovat
význam	význam	k1gInSc4	význam
slova	slovo	k1gNnSc2	slovo
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
makkef	makkef	k1gInSc1	makkef
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
spojení	spojení	k1gNnSc4	spojení
většinou	většina	k1gFnSc7	většina
krátkých	krátký	k2eAgNnPc2d1	krátké
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
tvoří	tvořit	k5eAaImIp3nP	tvořit
sousloví	sousloví	k1gNnSc4	sousloví
či	či	k8xC	či
podobný	podobný	k2eAgInSc4d1	podobný
logický	logický	k2eAgInSc4d1	logický
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
přízvuk	přízvuk	k1gInSc1	přízvuk
pak	pak	k6eAd1	pak
nese	nést	k5eAaImIp3nS	nést
pouze	pouze	k6eAd1	pouze
poslední	poslední	k2eAgNnSc4d1	poslední
slovo	slovo	k1gNnSc4	slovo
sousloví	sousloví	k1gNnPc2	sousloví
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgNnPc1d1	ostatní
slova	slovo	k1gNnPc1	slovo
nesou	nést	k5eAaImIp3nP	nést
vedlejší	vedlejší	k2eAgInSc4d1	vedlejší
přízvuk	přízvuk	k1gInSc4	přízvuk
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
přízvuku	přízvuk	k1gInSc2	přízvuk
zbavena	zbavit	k5eAaPmNgFnS	zbavit
zcela	zcela	k6eAd1	zcela
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
často	často	k6eAd1	často
dochází	docházet	k5eAaImIp3nS	docházet
také	také	k9	také
ke	k	k7c3	k
zkrácení	zkrácení	k1gNnSc3	zkrácení
vokálu	vokál	k1gInSc2	vokál
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
כ	כ	k?	כ
<g/>
ֹ	ֹ	k?	ֹ
<g/>
ּ	ּ	k?	ּ
<g/>
ל	ל	k?	ל
א	א	k?	א
<g/>
ָ	ָ	k?	ָ
<g/>
ד	ד	k?	ד
<g/>
ָ	ָ	k?	ָ
<g/>
ם	ם	k?	ם
(	(	kIx(	(
<g/>
kol	kola	k1gFnPc2	kola
adam	adam	k1gMnSc1	adam
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
každý	každý	k3xTgMnSc1	každý
člověk	člověk	k1gMnSc1	člověk
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
po	po	k7c6	po
spojení	spojení	k1gNnSc6	spojení
pomocí	pomocí	k7c2	pomocí
makkefu	makkef	k1gInSc2	makkef
nabývá	nabývat	k5eAaImIp3nS	nabývat
tvaru	tvar	k1gInSc3	tvar
כ	כ	k?	כ
<g/>
ָ	ָ	k?	ָ
<g/>
ּ	ּ	k?	ּ
<g/>
ל	ל	k?	ל
<g/>
-	-	kIx~	-
<g/>
א	א	k?	א
<g/>
ָ	ָ	k?	ָ
<g/>
ד	ד	k?	ד
<g/>
ָ	ָ	k?	ָ
<g/>
ם	ם	k?	ם
(	(	kIx(	(
<g/>
kol-adam	koldam	k1gInSc1	kol-adam
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Meteg	Meteg	k1gInSc1	Meteg
(	(	kIx(	(
<g/>
ֽ	ֽ	k?	ֽ
<g/>
)	)	kIx)	)
mj.	mj.	kA	mj.
označuje	označovat	k5eAaImIp3nS	označovat
vedlejší	vedlejší	k2eAgInSc1d1	vedlejší
přizvuk	přizvuk	k1gInSc1	přizvuk
ve	v	k7c6	v
slově	slovo	k1gNnSc6	slovo
a	a	k8xC	a
klade	klást	k5eAaImIp3nS	klást
se	se	k3xPyFc4	se
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
otevřené	otevřený	k2eAgFnSc2d1	otevřená
slabiky	slabika	k1gFnSc2	slabika
před	před	k7c7	před
hlavním	hlavní	k2eAgInSc7d1	hlavní
přízvukem	přízvuk	k1gInSc7	přízvuk
<g/>
.	.	kIx.	.
</s>
<s>
Znaménko	znaménko	k1gNnSc1	znaménko
tak	tak	k6eAd1	tak
napomáhá	napomáhat	k5eAaImIp3nS	napomáhat
čtení	čtení	k1gNnSc1	čtení
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jasně	jasně	k6eAd1	jasně
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
následující	následující	k2eAgNnSc4d1	následující
šva	šva	k1gNnSc4	šva
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
již	již	k6eAd1	již
další	další	k2eAgFnSc2d1	další
slabiky	slabika	k1gFnSc2	slabika
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tudíž	tudíž	k8xC	tudíž
znělé	znělý	k2eAgNnSc1d1	znělé
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
staletí	staletý	k2eAgMnPc1d1	staletý
masoreti	masoret	k1gMnPc1	masoret
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
při	při	k7c6	při
opisování	opisování	k1gNnSc6	opisování
biblického	biblický	k2eAgInSc2d1	biblický
textu	text	k1gInSc2	text
systém	systém	k1gInSc1	systém
poznámek	poznámka	k1gFnPc2	poznámka
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
masora	masor	k1gMnSc2	masor
<g/>
.	.	kIx.	.
</s>
<s>
Masora	Masora	k1gFnSc1	Masora
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
na	na	k7c4	na
masora	masor	k1gMnSc4	masor
parva	parva	k?	parva
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
"	"	kIx"	"
<g/>
malá	malý	k2eAgFnSc1d1	malá
masora	masora	k1gFnSc1	masora
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
masora	masora	k1gFnSc1	masora
magna	magna	k1gFnSc1	magna
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
"	"	kIx"	"
<g/>
velká	velký	k2eAgFnSc1d1	velká
masora	masora	k1gFnSc1	masora
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Masora	Masora	k1gFnSc1	Masora
parva	parva	k?	parva
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
především	především	k9	především
statistické	statistický	k2eAgFnPc4d1	statistická
poznámky	poznámka	k1gFnPc4	poznámka
o	o	k7c6	o
počtu	počet	k1gInSc6	počet
výskytů	výskyt	k1gInPc2	výskyt
slov	slovo	k1gNnPc2	slovo
a	a	k8xC	a
frází	fráze	k1gFnPc2	fráze
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
tzv.	tzv.	kA	tzv.
ketiv	ketiv	k6eAd1	ketiv
(	(	kIx(	(
<g/>
aramejsky	aramejsky	k6eAd1	aramejsky
כ	כ	k?	כ
<g/>
ְ	ְ	k?	ְ
<g/>
ּ	ּ	k?	ּ
<g/>
ת	ת	k?	ת
<g/>
ִ	ִ	k?	ִ
<g/>
י	י	k?	י
"	"	kIx"	"
<g/>
napsané	napsaný	k2eAgInPc1d1	napsaný
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
kere	kere	k6eAd1	kere
(	(	kIx(	(
<g/>
aramejsky	aramejsky	k6eAd1	aramejsky
ק	ק	k?	ק
<g/>
ְ	ְ	k?	ְ
<g/>
ר	ר	k?	ר
<g/>
ֵ	ֵ	k?	ֵ
<g/>
י	י	k?	י
"	"	kIx"	"
<g/>
čti	číst	k5eAaImRp2nS	číst
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jsou	být	k5eAaImIp3nP	být
poznámky	poznámka	k1gFnPc4	poznámka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
navrhují	navrhovat	k5eAaImIp3nP	navrhovat
nahradit	nahradit	k5eAaPmF	nahradit
slovo	slovo	k1gNnSc4	slovo
v	v	k7c6	v
textu	text	k1gInSc6	text
při	při	k7c6	při
čtení	čtení	k1gNnSc6	čtení
jiným	jiný	k2eAgInSc7d1	jiný
výrazem	výraz	k1gInSc7	výraz
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
případě	případ	k1gInSc6	případ
zřejmých	zřejmý	k2eAgFnPc2d1	zřejmá
písařských	písařský	k2eAgFnPc2d1	písařská
chyb	chyba	k1gFnPc2	chyba
či	či	k8xC	či
nejasných	jasný	k2eNgInPc2d1	nejasný
tvarů	tvar	k1gInPc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Masora	Masora	k1gFnSc1	Masora
magna	magn	k1gInSc2	magn
pak	pak	k6eAd1	pak
rozsáhle	rozsáhle	k6eAd1	rozsáhle
rozpracovává	rozpracovávat	k5eAaImIp3nS	rozpracovávat
materiál	materiál	k1gInSc1	materiál
masory	masora	k1gFnSc2	masora
parvy	parvus	k1gInPc4	parvus
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vydávána	vydáván	k2eAgFnSc1d1	vydávána
v	v	k7c6	v
samostatných	samostatný	k2eAgInPc6d1	samostatný
svazcích	svazek	k1gInPc6	svazek
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
masora	masora	k1gFnSc1	masora
parva	parva	k?	parva
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
moderních	moderní	k2eAgNnPc2d1	moderní
kritických	kritický	k2eAgNnPc2d1	kritické
vydání	vydání	k1gNnSc2	vydání
hebrejské	hebrejský	k2eAgFnSc2d1	hebrejská
bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
písem	písmo	k1gNnPc2	písmo
Hebrejština	hebrejština	k1gFnSc1	hebrejština
Fonologie	fonologie	k1gFnSc1	fonologie
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
Hebrejské	hebrejský	k2eAgFnSc2d1	hebrejská
číslice	číslice	k1gFnSc2	číslice
</s>
