<s>
Con	Con	k?	Con
(	(	kIx(	(
<g/>
z	z	k7c2	z
anglického	anglický	k2eAgMnSc2d1	anglický
convention	convention	k1gInSc1	convention
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
setkání	setkání	k1gNnSc1	setkání
fanoušků	fanoušek	k1gMnPc2	fanoušek
sci-fi	scii	k1gNnSc2	sci-fi
<g/>
,	,	kIx,	,
fantasy	fantas	k1gInPc1	fantas
<g/>
,	,	kIx,	,
určitého	určitý	k2eAgNnSc2d1	určité
televizní	televizní	k2eAgInSc1d1	televizní
seriálu	seriál	k1gInSc6	seriál
<g/>
,	,	kIx,	,
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
spisovatele	spisovatel	k1gMnPc4	spisovatel
<g/>
,	,	kIx,	,
herce	herec	k1gMnPc4	herec
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnPc4d1	jiná
zábavy	zábava	k1gFnPc4	zábava
(	(	kIx(	(
<g/>
počítačové	počítačový	k2eAgFnPc4d1	počítačová
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
anime	animat	k5eAaPmIp3nS	animat
<g/>
,	,	kIx,	,
stolní	stolní	k2eAgFnPc4d1	stolní
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
karetní	karetní	k2eAgFnPc4d1	karetní
hry	hra	k1gFnPc4	hra
<g/>
)	)	kIx)	)
na	na	k7c6	na
předem	předem	k6eAd1	předem
domluveném	domluvený	k2eAgNnSc6d1	domluvené
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
předem	předem	k6eAd1	předem
domluvené	domluvený	k2eAgFnSc6d1	domluvená
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
