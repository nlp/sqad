<s>
Con	Con	k?	Con
(	(	kIx(	(
<g/>
z	z	k7c2	z
anglického	anglický	k2eAgMnSc2d1	anglický
convention	convention	k1gInSc1	convention
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
setkání	setkání	k1gNnSc1	setkání
fanoušků	fanoušek	k1gMnPc2	fanoušek
sci-fi	scii	k1gNnSc2	sci-fi
<g/>
,	,	kIx,	,
fantasy	fantas	k1gInPc1	fantas
<g/>
,	,	kIx,	,
určitého	určitý	k2eAgNnSc2d1	určité
televizní	televizní	k2eAgInSc1d1	televizní
seriálu	seriál	k1gInSc6	seriál
<g/>
,	,	kIx,	,
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
spisovatele	spisovatel	k1gMnPc4	spisovatel
<g/>
,	,	kIx,	,
herce	herec	k1gMnPc4	herec
nebo	nebo	k8xC	nebo
jiné	jiný	k2eAgFnPc4d1	jiná
zábavy	zábava	k1gFnPc4	zábava
(	(	kIx(	(
<g/>
počítačové	počítačový	k2eAgFnPc4d1	počítačová
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
anime	animat	k5eAaPmIp3nS	animat
<g/>
,	,	kIx,	,
stolní	stolní	k2eAgFnPc4d1	stolní
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
karetní	karetní	k2eAgFnPc4d1	karetní
hry	hra	k1gFnPc4	hra
<g/>
)	)	kIx)	)
na	na	k7c6	na
předem	předem	k6eAd1	předem
domluveném	domluvený	k2eAgNnSc6d1	domluvené
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
předem	předem	k6eAd1	předem
domluvené	domluvený	k2eAgFnSc6d1	domluvená
době	doba	k1gFnSc6	doba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
jakékoliv	jakýkoliv	k3yIgNnSc4	jakýkoliv
setkání	setkání	k1gNnSc4	setkání
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Např.	např.	kA	např.
Gatecon	Gatecon	k1gNnSc1	Gatecon
-	-	kIx~	-
setkání	setkání	k1gNnSc1	setkání
příznivců	příznivec	k1gMnPc2	příznivec
seriálu	seriál	k1gInSc6	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
;	;	kIx,	;
Advik	Advik	k1gInSc1	Advik
-	-	kIx~	-
Anime	Anim	k1gInSc5	Anim
Dance	Danka	k1gFnSc3	Danka
Víkend	víkend	k1gInSc1	víkend
-	-	kIx~	-
anime	animat	k5eAaPmIp3nS	animat
a	a	k8xC	a
japonská	japonský	k2eAgFnSc1d1	japonská
kultura	kultura	k1gFnSc1	kultura
<g/>
;	;	kIx,	;
Trpaslicon	Trpaslicon	k1gInSc1	Trpaslicon
–	–	k?	–
setkání	setkání	k1gNnSc2	setkání
fanoušků	fanoušek	k1gMnPc2	fanoušek
seriálu	seriál	k1gInSc2	seriál
Červený	Červený	k1gMnSc1	Červený
trpaslík	trpaslík	k1gMnSc1	trpaslík
<g/>
;	;	kIx,	;
BabCon	BabCon	k1gMnSc1	BabCon
–	–	k?	–
setkání	setkání	k1gNnSc2	setkání
fanoušků	fanoušek	k1gMnPc2	fanoušek
seriálu	seriál	k1gInSc2	seriál
Babylon	Babylon	k1gInSc1	Babylon
5	[number]	k4	5
<g/>
;	;	kIx,	;
GleeCon	GleeCona	k1gFnPc2	GleeCona
–	–	k?	–
setkání	setkání	k1gNnSc6	setkání
fanoušků	fanoušek	k1gMnPc2	fanoušek
seriálu	seriál	k1gInSc2	seriál
Glee	Glee	k1gFnPc2	Glee
<g/>
)	)	kIx)	)
Cony	Cona	k1gFnPc1	Cona
bývají	bývat	k5eAaImIp3nP	bývat
z	z	k7c2	z
pravidla	pravidlo	k1gNnSc2	pravidlo
vícedenní	vícedenní	k2eAgFnSc1d1	vícedenní
akce	akce	k1gFnSc1	akce
s	s	k7c7	s
alespoň	alespoň	k9	alespoň
částečně	částečně	k6eAd1	částečně
předem	předem	k6eAd1	předem
daným	daný	k2eAgInSc7d1	daný
programem	program	k1gInSc7	program
<g/>
.	.	kIx.	.
</s>
<s>
Běžným	běžný	k2eAgInSc7d1	běžný
programem	program	k1gInSc7	program
takového	takový	k3xDgNnSc2	takový
setkání	setkání	k1gNnSc2	setkání
bývají	bývat	k5eAaImIp3nP	bývat
přednášky	přednáška	k1gFnPc4	přednáška
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
s	s	k7c7	s
dabéry	dabér	k1gInPc7	dabér
<g/>
,	,	kIx,	,
či	či	k8xC	či
překladateli	překladatel	k1gMnPc7	překladatel
<g/>
,	,	kIx,	,
hry	hra	k1gFnPc4	hra
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc4	možnost
zakoupení	zakoupení	k1gNnSc2	zakoupení
suvenýrů	suvenýr	k1gInPc2	suvenýr
(	(	kIx(	(
<g/>
triček	tričko	k1gNnPc2	tričko
<g/>
,	,	kIx,	,
hrníčků	hrníček	k1gInPc2	hrníček
<g/>
,	,	kIx,	,
placek	placka	k1gFnPc2	placka
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
či	či	k8xC	či
burzy	burza	k1gFnPc1	burza
<g/>
,	,	kIx,	,
soutěže	soutěž	k1gFnPc1	soutěž
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
také	také	k9	také
Cosplay	Cosplaa	k1gFnSc2	Cosplaa
a	a	k8xC	a
promítání	promítání	k1gNnSc2	promítání
fanfilmů	fanfilm	k1gInPc2	fanfilm
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
i	i	k9	i
parodií	parodie	k1gFnSc7	parodie
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
žánru	žánr	k1gInSc6	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
jako	jako	k9	jako
místo	místo	k7c2	místo
pořádání	pořádání	k1gNnSc2	pořádání
conu	conus	k1gInSc2	conus
vybírají	vybírat	k5eAaImIp3nP	vybírat
školy	škola	k1gFnPc1	škola
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgNnPc1d1	kulturní
centra	centrum	k1gNnPc1	centrum
a	a	k8xC	a
multikina	multikino	k1gNnPc1	multikino
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
akcí	akce	k1gFnSc7	akce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
conů	con	k1gInPc2	con
najednou	najednou	k6eAd1	najednou
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
pořádaný	pořádaný	k2eAgInSc1d1	pořádaný
Festival	festival	k1gInSc1	festival
fantazie	fantazie	k1gFnSc2	fantazie
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
většinou	většina	k1gFnSc7	většina
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
července	červenec	k1gInSc2	červenec
v	v	k7c6	v
Chotěboři	Chotěboř	k1gFnSc6	Chotěboř
a	a	k8xC	a
trvá	trvat	k5eAaImIp3nS	trvat
přibližně	přibližně	k6eAd1	přibližně
11	[number]	k4	11
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světě	svět	k1gInSc6	svět
na	na	k7c4	na
podobné	podobný	k2eAgFnPc4d1	podobná
akce	akce	k1gFnPc4	akce
příliš	příliš	k6eAd1	příliš
nenarazíte	narazit	k5eNaPmIp2nP	narazit
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
soustřeďují	soustřeďovat	k5eAaImIp3nP	soustřeďovat
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
téma	téma	k1gNnSc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
con	con	k?	con
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Kalendář	kalendář	k1gInSc1	kalendář
conů	con	k1gMnPc2	con
</s>
