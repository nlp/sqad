<s>
Bimetalový	bimetalový	k2eAgInSc1d1
teploměr	teploměr	k1gInSc1
</s>
<s>
ručičkový	ručičkový	k2eAgInSc1d1
teploměr	teploměr	k1gInSc1
se	s	k7c7
spirálovým	spirálový	k2eAgInSc7d1
bimetalovým	bimetalový	k2eAgInSc7d1
páskem	pásek	k1gInSc7
</s>
<s>
Bimetalový	bimetalový	k2eAgInSc1d1
teploměr	teploměr	k1gInSc1
je	být	k5eAaImIp3nS
teploměr	teploměr	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
na	na	k7c4
měření	měření	k1gNnSc4
teploty	teplota	k1gFnSc2
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
bimetalový	bimetalový	k2eAgInSc4d1
(	(	kIx(
<g/>
dvojkovový	dvojkovový	k2eAgInSc4d1
<g/>
)	)	kIx)
pásek	pásek	k1gInSc4
složený	složený	k2eAgInSc4d1
ze	z	k7c2
dvou	dva	k4xCgInPc2
kovů	kov	k1gInPc2
s	s	k7c7
různými	různý	k2eAgMnPc7d1
činiteli	činitel	k1gMnPc7
tepelné	tepelný	k2eAgFnSc2d1
roztažnosti	roztažnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
změně	změna	k1gFnSc6
teploty	teplota	k1gFnSc2
se	se	k3xPyFc4
pásek	pásek	k1gInSc1
ohýbá	ohýbat	k5eAaImIp3nS
a	a	k8xC
tento	tento	k3xDgInSc1
pohyb	pohyb	k1gInSc1
se	se	k3xPyFc4
přenáší	přenášet	k5eAaImIp3nS
na	na	k7c4
ručičku	ručička	k1gFnSc4
přístroje	přístroj	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
bimetal	bimetal	k1gInSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Bimetalový	bimetalový	k2eAgInSc4d1
teplomer	teplomer	k1gInSc4
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
