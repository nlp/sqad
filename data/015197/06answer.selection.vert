<s>
Bimetalový	bimetalový	k2eAgInSc1d1
teploměr	teploměr	k1gInSc1
je	být	k5eAaImIp3nS
teploměr	teploměr	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
na	na	k7c4
měření	měření	k1gNnSc4
teploty	teplota	k1gFnSc2
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
bimetalový	bimetalový	k2eAgInSc4d1
(	(	kIx(
<g/>
dvojkovový	dvojkovový	k2eAgInSc4d1
<g/>
)	)	kIx)
pásek	pásek	k1gInSc4
složený	složený	k2eAgInSc4d1
ze	z	k7c2
dvou	dva	k4xCgInPc2
kovů	kov	k1gInPc2
s	s	k7c7
různými	různý	k2eAgMnPc7d1
činiteli	činitel	k1gMnPc7
tepelné	tepelný	k2eAgFnSc2d1
roztažnosti	roztažnost	k1gFnSc2
<g/>
.	.	kIx.
</s>