<s>
Merkur	Merkur	k1gInSc1	Merkur
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
přinejmenším	přinejmenším	k6eAd1	přinejmenším
již	již	k9	již
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
starověkých	starověký	k2eAgInPc2d1	starověký
Sumerů	Sumer	k1gInPc2	Sumer
asi	asi	k9	asi
3	[number]	k4	3
tisíce	tisíc	k4xCgInSc2	tisíc
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Jejich	jejich	k3xOp3gNnSc4	jejich
pozorování	pozorování	k1gNnSc4	pozorování
jsou	být	k5eAaImIp3nP	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
klínovým	klínový	k2eAgNnSc7d1	klínové
písmem	písmo	k1gNnSc7	písmo
na	na	k7c6	na
hliněných	hliněný	k2eAgFnPc6d1	hliněná
tabulkách	tabulka	k1gFnPc6	tabulka
<g/>
.	.	kIx.	.
</s>
