<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Kalista	Kalista	k1gMnSc1	Kalista
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1890	[number]	k4	1890
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1954	[number]	k4	1954
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
divadelní	divadelní	k2eAgMnSc1d1	divadelní
režisér	režisér	k1gMnSc1	režisér
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
domě	dům	k1gInSc6	dům
Č.	Č.	kA	Č.
420	[number]	k4	420
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
Smíchově	Smíchov	k1gInSc6	Smíchov
<g/>
,	,	kIx,	,
obuvníkovi	obuvník	k1gMnSc3	obuvník
Janu	Jan	k1gMnSc3	Jan
Kalistovi	Kalista	k1gMnSc3	Kalista
a	a	k8xC	a
matce	matka	k1gFnSc3	matka
Marii	Maria	k1gFnSc3	Maria
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Bendové	Bendové	k2eAgFnSc1d1	Bendové
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
mu	on	k3xPp3gMnSc3	on
již	již	k6eAd1	již
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
14	[number]	k4	14
letech	léto	k1gNnPc6	léto
zemřela	zemřít	k5eAaPmAgFnS	zemřít
a	a	k8xC	a
zůstali	zůstat	k5eAaPmAgMnP	zůstat
s	s	k7c7	s
jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
a	a	k8xC	a
dvěma	dva	k4xCgFnPc7	dva
sestrami	sestra	k1gFnPc7	sestra
sami	sám	k3xTgMnPc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Jan	Jan	k1gMnSc1	Jan
Kalista	Kalista	k1gMnSc1	Kalista
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
již	již	k9	již
ovdovělou	ovdovělý	k2eAgFnSc4d1	ovdovělá
Anastázii	Anastázie	k1gFnSc4	Anastázie
Pražskou	pražský	k2eAgFnSc4d1	Pražská
-	-	kIx~	-
Srpovou	srpový	k2eAgFnSc4d1	Srpová
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
<g/>
dubna	duben	k1gInSc2	duben
1866	[number]	k4	1866
-	-	kIx~	-
Nový	nový	k2eAgInSc1d1	nový
Knín	Knín	k1gInSc1	Knín
<g/>
)	)	kIx)	)
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejprve	nejprve	k6eAd1	nejprve
byl	být	k5eAaImAgInS	být
režisérem	režisér	k1gMnSc7	režisér
v	v	k7c6	v
Divadle	divadlo	k1gNnSc6	divadlo
Pavla	Pavel	k1gMnSc2	Pavel
Švandy	Švanda	k1gMnSc2	Švanda
na	na	k7c6	na
Smíchově	Smíchov	k1gInSc6	Smíchov
odkud	odkud	k6eAd1	odkud
poté	poté	k6eAd1	poté
ve	v	k7c6	v
40	[number]	k4	40
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
Jihočeského	jihočeský	k2eAgNnSc2d1	Jihočeské
divadla	divadlo	k1gNnSc2	divadlo
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
Buějovicích	Buějovice	k1gFnPc6	Buějovice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
divadla	divadlo	k1gNnSc2	divadlo
na	na	k7c6	na
Smíchově	Smíchov	k1gInSc6	Smíchov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Byl	být	k5eAaImAgMnS	být
táké	táké	k6eAd1	táké
rozhlasovým	rozhlasový	k2eAgMnSc7d1	rozhlasový
hercem	herec	k1gMnSc7	herec
hrál	hrát	k5eAaImAgMnS	hrát
na	na	k7c6	na
stanici	stanice	k1gFnSc6	stanice
Praha	Praha	k1gFnSc1	Praha
Českoslovesnského	Českoslovesnský	k2eAgInSc2d1	Českoslovesnský
rozhlasu	rozhlas	k1gInSc2	rozhlas
v	v	k7c6	v
hrách	hra	k1gFnPc6	hra
Oldřicha	Oldřich	k1gMnSc2	Oldřich
Nového	Nový	k1gMnSc2	Nový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Kalista	Kalista	k1gMnSc1	Kalista
měl	mít	k5eAaImAgMnS	mít
3	[number]	k4	3
sourozence	sourozenka	k1gFnSc6	sourozenka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kalistová	Kalistový	k2eAgFnSc1d1	Kalistová
Emilie	Emilie	k1gFnSc1	Emilie
(	(	kIx(	(
<g/>
1889	[number]	k4	1889
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kalistová	Kalistová	k1gFnSc1	Kalistová
Josefa	Josef	k1gMnSc2	Josef
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Srp	srp	k1gInSc1	srp
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
<g/>
Hrob	hrob	k1gInSc1	hrob
rodiny	rodina	k1gFnSc2	rodina
Kalistových	Kalistův	k2eAgMnPc2d1	Kalistův
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
Malvazinky	malvazinka	k1gFnSc2	malvazinka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
1947	[number]	k4	1947
Housle	housle	k1gFnPc1	housle
a	a	k8xC	a
sen	sen	k1gInSc1	sen
(	(	kIx(	(
<g/>
generál	generál	k1gMnSc1	generál
Kučera	Kučera	k1gMnSc1	Kučera
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1947	[number]	k4	1947
Nikola	Nikola	k1gMnSc1	Nikola
Šuhaj	šuhaj	k1gMnSc1	šuhaj
(	(	kIx(	(
<g/>
gazda	gazda	k1gMnSc1	gazda
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1947	[number]	k4	1947
Parohy	paroh	k1gInPc4	paroh
(	(	kIx(	(
<g/>
vrátný	vrátný	k2eAgInSc4d1	vrátný
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1927	[number]	k4	1927
Pražské	pražský	k2eAgFnPc4d1	Pražská
děti	dítě	k1gFnPc4	dítě
(	(	kIx(	(
<g/>
šikovatel	šikovatel	k1gMnSc1	šikovatel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
1924	[number]	k4	1924
Záhadný	záhadný	k2eAgInSc1d1	záhadný
případ	případ	k1gInSc1	případ
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Kalista	Kalista	k1gMnSc1	Kalista
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Kalista	Kalista	k1gMnSc1	Kalista
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Kalista	Kalista	k1gMnSc1	Kalista
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Kalista	Kalista	k1gMnSc1	Kalista
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
