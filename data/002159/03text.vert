<s>
Lansing	Lansing	k1gInSc1	Lansing
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
státu	stát	k1gInSc2	stát
Michigan	Michigan	k1gInSc4	Michigan
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Detroitu	Detroit	k1gInSc2	Detroit
<g/>
,	,	kIx,	,
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Grand	grand	k1gMnSc1	grand
River	River	k1gMnSc1	River
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
114	[number]	k4	114
297	[number]	k4	297
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
aglomeraci	aglomerace	k1gFnSc6	aglomerace
pak	pak	k6eAd1	pak
464	[number]	k4	464
0	[number]	k4	0
<g/>
36	[number]	k4	36
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
osídleno	osídlit	k5eAaPmNgNnS	osídlit
kmenem	kmen	k1gInSc7	kmen
Odžibvejů	Odžibvej	k1gMnPc2	Odžibvej
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc1d1	hlavní
vlna	vlna	k1gFnSc1	vlna
bílých	bílý	k2eAgMnPc2d1	bílý
osadníků	osadník	k1gMnPc2	osadník
sem	sem	k6eAd1	sem
dorazila	dorazit	k5eAaPmAgFnS	dorazit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1840	[number]	k4	1840
z	z	k7c2	z
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
požadovali	požadovat	k5eAaImAgMnP	požadovat
pozemky	pozemek	k1gInPc4	pozemek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
si	se	k3xPyFc3	se
zakoupili	zakoupit	k5eAaPmAgMnP	zakoupit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ty	ten	k3xDgFnPc1	ten
existovaly	existovat	k5eAaImAgFnP	existovat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
papíře	papír	k1gInSc6	papír
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
zklamání	zklamání	k1gNnSc4	zklamání
zde	zde	k6eAd1	zde
některé	některý	k3yIgFnPc1	některý
rodiny	rodina	k1gFnPc1	rodina
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
a	a	k8xC	a
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
město	město	k1gNnSc1	město
pojmenovaly	pojmenovat	k5eAaPmAgInP	pojmenovat
podle	podle	k7c2	podle
Johna	John	k1gMnSc2	John
Lansinga	Lansing	k1gMnSc2	Lansing
<g/>
,	,	kIx,	,
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
právníka	právník	k1gMnSc2	právník
a	a	k8xC	a
předního	přední	k2eAgMnSc2d1	přední
politika	politik	k1gMnSc2	politik
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
roku	rok	k1gInSc2	rok
1847	[number]	k4	1847
byl	být	k5eAaImAgInS	být
Lansing	Lansing	k1gInSc1	Lansing
zvolen	zvolen	k2eAgInSc1d1	zvolen
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
pozici	pozice	k1gFnSc6	pozice
tak	tak	k6eAd1	tak
vystřídal	vystřídat	k5eAaPmAgInS	vystřídat
Detroit	Detroit	k1gInSc1	Detroit
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
zde	zde	k6eAd1	zde
Ransom	Ransom	k1gInSc1	Ransom
E.	E.	kA	E.
Olds	Olds	k1gInSc1	Olds
a	a	k8xC	a
Frank	Frank	k1gMnSc1	Frank
G.	G.	kA	G.
Clark	Clark	k1gInSc1	Clark
vyrobili	vyrobit	k5eAaPmAgMnP	vyrobit
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
automobil	automobil	k1gInSc4	automobil
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
umožnili	umožnit	k5eAaPmAgMnP	umožnit
nebývalý	nebývalý	k2eAgInSc4d1	nebývalý
průmyslový	průmyslový	k2eAgInSc4d1	průmyslový
rozkvět	rozkvět	k1gInSc4	rozkvět
celého	celý	k2eAgInSc2d1	celý
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
Michigan	Michigan	k1gInSc1	Michigan
upevnil	upevnit	k5eAaPmAgInS	upevnit
svou	svůj	k3xOyFgFnSc4	svůj
pozici	pozice	k1gFnSc4	pozice
srdce	srdce	k1gNnSc2	srdce
amerického	americký	k2eAgInSc2d1	americký
automobilového	automobilový	k2eAgInSc2d1	automobilový
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
krize	krize	k1gFnSc2	krize
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1979-1982	[number]	k4	1979-1982
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
vybudovaných	vybudovaný	k2eAgFnPc2d1	vybudovaná
továren	továrna	k1gFnPc2	továrna
a	a	k8xC	a
až	až	k9	až
třetina	třetina	k1gFnSc1	třetina
všech	všecek	k3xTgMnPc2	všecek
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
ztratila	ztratit	k5eAaPmAgFnS	ztratit
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
regionu	region	k1gInSc6	region
tak	tak	k6eAd1	tak
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
množství	množství	k1gNnSc1	množství
nevyužitých	využitý	k2eNgInPc2d1	nevyužitý
a	a	k8xC	a
opuštěných	opuštěný	k2eAgInPc2d1	opuštěný
továrních	tovární	k2eAgInPc2d1	tovární
prostorů	prostor	k1gInPc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
114	[number]	k4	114
297	[number]	k4	297
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
61,2	[number]	k4	61,2
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
23,7	[number]	k4	23,7
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
0,8	[number]	k4	0,8
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
3,7	[number]	k4	3,7
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,0	[number]	k4	0,0
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
4,3	[number]	k4	4,3
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
6,2	[number]	k4	6,2
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
12,5	[number]	k4	12,5
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Lansing	Lansing	k1gInSc1	Lansing
je	být	k5eAaImIp3nS	být
tradiční	tradiční	k2eAgNnSc4d1	tradiční
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
centrum	centrum	k1gNnSc4	centrum
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c4	na
automobily	automobil	k1gInPc4	automobil
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
automobilová	automobilový	k2eAgFnSc1d1	automobilová
továrna	továrna	k1gFnSc1	továrna
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgNnPc7d1	další
odvětvími	odvětví	k1gNnPc7	odvětví
je	být	k5eAaImIp3nS	být
pak	pak	k9	pak
průmysl	průmysl	k1gInSc1	průmysl
chemický	chemický	k2eAgInSc1d1	chemický
a	a	k8xC	a
metalurgický	metalurgický	k2eAgInSc1d1	metalurgický
<g/>
.	.	kIx.	.
</s>
<s>
Burt	Burt	k2eAgInSc1d1	Burt
Reynolds	Reynolds	k1gInSc1	Reynolds
(	(	kIx(	(
<g/>
*	*	kIx~	*
1936	[number]	k4	1936
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
John	John	k1gMnSc1	John
Hughes	Hughes	k1gMnSc1	Hughes
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
-	-	kIx~	-
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
Steven	Steven	k2eAgInSc4d1	Steven
Seagal	Seagal	k1gInSc4	Seagal
(	(	kIx(	(
<g/>
*	*	kIx~	*
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Magic	Magic	k1gMnSc1	Magic
Johnson	Johnson	k1gMnSc1	Johnson
(	(	kIx(	(
<g/>
*	*	kIx~	*
1959	[number]	k4	1959
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
basketbalista	basketbalista	k1gMnSc1	basketbalista
Matthew	Matthew	k1gMnSc1	Matthew
Lillard	Lillard	k1gMnSc1	Lillard
(	(	kIx(	(
<g/>
*	*	kIx~	*
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Akuapim	Akuapim	k1gMnSc1	Akuapim
<g/>
,	,	kIx,	,
Ghana	Ghana	k1gFnSc1	Ghana
Cosenza	Cosenza	k1gFnSc1	Cosenza
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
Guadalajara	Guadalajara	k1gFnSc1	Guadalajara
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
Lan-čou	Lan-ča	k1gFnSc7	Lan-ča
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
Ócu	Ócu	k1gFnSc1	Ócu
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
Petrohrad	Petrohrad	k1gInSc1	Petrohrad
<g/>
,	,	kIx,	,
Rusko	Rusko	k1gNnSc1	Rusko
Sakaide	Sakaid	k1gInSc5	Sakaid
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
Saltillo	Saltillo	k1gNnSc1	Saltillo
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
San-ming	Saning	k1gInSc1	San-ming
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lansing	Lansing	k1gInSc4	Lansing
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
města	město	k1gNnSc2	město
Turistické	turistický	k2eAgNnSc1d1	turistické
informace	informace	k1gFnPc4	informace
</s>
