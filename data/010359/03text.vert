<p>
<s>
Dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
pavoučka	pavouček	k1gMnSc2	pavouček
Štěstíka	Štěstík	k1gMnSc2	Štěstík
je	být	k5eAaImIp3nS	být
československý	československý	k2eAgInSc1d1	československý
animovaný	animovaný	k2eAgInSc1d1	animovaný
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
poprvé	poprvé	k6eAd1	poprvé
vysílaný	vysílaný	k2eAgInSc4d1	vysílaný
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Večerníčku	večerníček	k1gInSc2	večerníček
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
scénáře	scénář	k1gInSc2	scénář
Heleny	Helena	k1gFnSc2	Helena
Sýkorové	Sýkorová	k1gFnSc2	Sýkorová
a	a	k8xC	a
Garika	Garikum	k1gNnSc2	Garikum
Seka	seko	k1gNnSc2	seko
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
současně	současně	k6eAd1	současně
ujal	ujmout	k5eAaPmAgInS	ujmout
režie	režie	k1gFnPc4	režie
<g/>
.	.	kIx.	.
</s>
<s>
Výtvarně	výtvarně	k6eAd1	výtvarně
zpracovala	zpracovat	k5eAaPmAgFnS	zpracovat
Marie	Marie	k1gFnSc1	Marie
Satrapová	Satrapová	k1gFnSc1	Satrapová
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
i	i	k8xC	i
seriál	seriál	k1gInSc4	seriál
namluvila	namluvit	k5eAaPmAgFnS	namluvit
<g/>
.	.	kIx.	.
</s>
<s>
Kameramanem	kameraman	k1gMnSc7	kameraman
byl	být	k5eAaImAgMnS	být
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kuchař	Kuchař	k1gMnSc1	Kuchař
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
7	[number]	k4	7
epizod	epizoda	k1gFnPc2	epizoda
<g/>
,	,	kIx,	,
každá	každý	k3xTgFnSc1	každý
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
7	[number]	k4	7
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc4	seznam
dílů	díl	k1gInPc2	díl
==	==	k?	==
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
Štěstík	Štěstík	k1gInSc1	Štěstík
vydal	vydat	k5eAaPmAgInS	vydat
do	do	k7c2	do
světa	svět	k1gInSc2	svět
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
Štěstík	Štěstík	k1gInSc1	Štěstík
choval	chovat	k5eAaImAgInS	chovat
motýlí	motýlí	k2eAgNnPc4d1	motýlí
miminka	miminko	k1gNnPc4	miminko
</s>
</p>
<p>
<s>
Jak	jak	k8xC	jak
Štěstík	Štěstík	k1gMnSc1	Štěstík
zachránil	zachránit	k5eAaPmAgMnS	zachránit
komáří	komáří	k2eAgFnSc4d1	komáří
parádnici	parádnice	k1gFnSc4	parádnice
</s>
</p>
<p>
<s>
Jak	jak	k8xS	jak
Štěstík	Štěstík	k1gMnSc1	Štěstík
potkal	potkat	k5eAaPmAgMnS	potkat
mravenčí	mravenčí	k2eAgMnPc4d1	mravenčí
loupežníky	loupežník	k1gMnPc4	loupežník
</s>
</p>
<p>
<s>
Jak	jak	k6eAd1	jak
Štěstík	Štěstík	k1gInSc1	Štěstík
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
stonožku	stonožka	k1gFnSc4	stonožka
</s>
</p>
<p>
<s>
Jak	jak	k8xS	jak
Štěstík	Štěstík	k1gMnSc1	Štěstík
vykouzlil	vykouzlit	k5eAaPmAgMnS	vykouzlit
sedm	sedm	k4xCc4	sedm
teček	tečka	k1gFnPc2	tečka
</s>
</p>
<p>
<s>
Jak	jak	k8xS	jak
Štěstík	Štěstík	k1gMnSc1	Štěstík
si	se	k3xPyFc3	se
chytil	chytit	k5eAaPmAgMnS	chytit
pavoučí	pavoučí	k2eAgFnSc4d1	pavoučí
vílu	víla	k1gFnSc4	víla
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
pavoučka	pavouček	k1gMnSc2	pavouček
Štěstíka	Štěstík	k1gMnSc2	Štěstík
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
Dobrodružství	dobrodružství	k1gNnSc1	dobrodružství
pavoučka	pavouček	k1gMnSc2	pavouček
Štěstíka	Štěstík	k1gMnSc2	Štěstík
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
