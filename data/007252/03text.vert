<s>
Ještěrka	ještěrka	k1gFnSc1	ještěrka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Lacerta	Lacerta	k1gFnSc1	Lacerta
agilis	agilis	k1gFnSc1	agilis
<g/>
,	,	kIx,	,
Linnaeus	Linnaeus	k1gInSc1	Linnaeus
<g/>
,	,	kIx,	,
1758	[number]	k4	1758
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejrozšířenějších	rozšířený	k2eAgInPc2d3	nejrozšířenější
druhů	druh	k1gInPc2	druh
ještěrky	ještěrka	k1gFnSc2	ještěrka
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
pod	pod	k7c4	pod
kameny	kámen	k1gInPc4	kámen
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
skulinách	skulina	k1gFnPc6	skulina
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
ještěrky	ještěrka	k1gFnSc2	ještěrka
obecné	obecná	k1gFnSc2	obecná
činí	činit	k5eAaImIp3nS	činit
kolem	kolem	k7c2	kolem
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
měří	měřit	k5eAaImIp3nS	měřit
až	až	k9	až
9	[number]	k4	9
cm	cm	kA	cm
a	a	k8xC	a
ocas	ocas	k1gInSc1	ocas
je	být	k5eAaImIp3nS	být
jedenapůlkrát	jedenapůlkrát	k6eAd1	jedenapůlkrát
delší	dlouhý	k2eAgMnSc1d2	delší
než	než	k8xS	než
tělo	tělo	k1gNnSc1	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
širší	široký	k2eAgInSc1d2	širší
s	s	k7c7	s
krátkým	krátký	k2eAgInSc7d1	krátký
čumákem	čumák	k1gInSc7	čumák
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
krátkých	krátký	k2eAgFnPc6d1	krátká
končetinách	končetina	k1gFnPc6	končetina
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
prstů	prst	k1gInPc2	prst
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
zakončeny	zakončen	k2eAgInPc1d1	zakončen
drápky	drápek	k1gInPc1	drápek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
dobrý	dobrý	k2eAgInSc4d1	dobrý
pohyb	pohyb	k1gInSc4	pohyb
po	po	k7c6	po
kamenech	kámen	k1gInPc6	kámen
a	a	k8xC	a
různorodých	různorodý	k2eAgInPc6d1	různorodý
materiálech	materiál	k1gInPc6	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Ocas	ocas	k1gInSc1	ocas
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
lepší	dobrý	k2eAgFnSc4d2	lepší
dynamiku	dynamika	k1gFnSc4	dynamika
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
také	také	k9	také
při	při	k7c6	při
útěku	útěk	k1gInSc6	útěk
před	před	k7c7	před
nepřáteli	nepřítel	k1gMnPc7	nepřítel
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
se	se	k3xPyFc4	se
od	od	k7c2	od
těla	tělo	k1gNnSc2	tělo
při	při	k7c6	při
napadení	napadení	k1gNnSc6	napadení
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
regeneruje	regenerovat	k5eAaBmIp3nS	regenerovat
a	a	k8xC	a
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
druhem	druh	k1gInSc7	druh
chráněným	chráněný	k2eAgInSc7d1	chráněný
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
druhů	druh	k1gInPc2	druh
ještěrek	ještěrka	k1gFnPc2	ještěrka
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nejrozšířenějších	rozšířený	k2eAgFnPc2d3	nejrozšířenější
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
až	až	k9	až
po	po	k7c4	po
západ	západ	k1gInSc4	západ
Číny	Čína	k1gFnSc2	Čína
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gFnSc1	její
početnost	početnost	k1gFnSc1	početnost
silně	silně	k6eAd1	silně
poklesla	poklesnout	k5eAaPmAgFnS	poklesnout
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
ztrátě	ztráta	k1gFnSc3	ztráta
stanovišť	stanoviště	k1gNnPc2	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
především	především	k9	především
suchá	suchý	k2eAgNnPc4d1	suché
a	a	k8xC	a
slunná	slunný	k2eAgNnPc4d1	slunné
místa	místo	k1gNnPc4	místo
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
stráně	stráň	k1gFnPc1	stráň
<g/>
,	,	kIx,	,
i	i	k8xC	i
okraje	okraj	k1gInPc1	okraj
lesů	les	k1gInPc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
pobývá	pobývat	k5eAaImIp3nS	pobývat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
2500	[number]	k4	2500
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Při	při	k7c6	při
větších	veliký	k2eAgInPc6d2	veliký
výkyvech	výkyv	k1gInPc6	výkyv
teplot	teplota	k1gFnPc2	teplota
raději	rád	k6eAd2	rád
zalézá	zalézat	k5eAaImIp3nS	zalézat
do	do	k7c2	do
úkrytů	úkryt	k1gInPc2	úkryt
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
jedinec	jedinec	k1gMnSc1	jedinec
má	mít	k5eAaImIp3nS	mít
svoje	svůj	k3xOyFgNnSc4	svůj
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
loví	lovit	k5eAaImIp3nP	lovit
<g/>
,	,	kIx,	,
klade	klást	k5eAaImIp3nS	klást
vejce	vejce	k1gNnSc1	vejce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jídelníčku	jídelníček	k1gInSc6	jídelníček
ještěrky	ještěrka	k1gFnSc2	ještěrka
obecné	obecná	k1gFnSc2	obecná
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
menší	malý	k2eAgInSc4d2	menší
hmyz	hmyz	k1gInSc4	hmyz
(	(	kIx(	(
<g/>
mouchy	moucha	k1gFnPc4	moucha
<g/>
,	,	kIx,	,
brouky	brouk	k1gMnPc4	brouk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
také	také	k9	také
pavouky	pavouk	k1gMnPc4	pavouk
<g/>
,	,	kIx,	,
červy	červ	k1gMnPc4	červ
<g/>
,	,	kIx,	,
plže	plž	k1gMnPc4	plž
a	a	k8xC	a
pozemní	pozemní	k2eAgMnPc4d1	pozemní
korýše	korýš	k1gMnPc4	korýš
(	(	kIx(	(
<g/>
svinky	svinka	k1gFnSc2	svinka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnSc1d2	menší
kořist	kořist	k1gFnSc1	kořist
rovnou	rovnou	k6eAd1	rovnou
polyká	polykat	k5eAaImIp3nS	polykat
<g/>
.	.	kIx.	.
</s>
<s>
Ještěrka	ještěrka	k1gFnSc1	ještěrka
loví	lovit	k5eAaImIp3nS	lovit
především	především	k9	především
ve	v	k7c6	v
slunných	slunný	k2eAgInPc6d1	slunný
dnech	den	k1gInPc6	den
a	a	k8xC	a
dokáže	dokázat	k5eAaPmIp3nS	dokázat
ulovit	ulovit	k5eAaPmF	ulovit
i	i	k9	i
letící	letící	k2eAgFnSc4d1	letící
kořist	kořist	k1gFnSc4	kořist
<g/>
.	.	kIx.	.
</s>
<s>
Ještěrky	ještěrka	k1gFnPc1	ještěrka
se	se	k3xPyFc4	se
rozmnožují	rozmnožovat	k5eAaImIp3nP	rozmnožovat
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
–	–	k?	–
květnu	květen	k1gInSc6	květen
<g/>
,	,	kIx,	,
červnu	červen	k1gInSc6	červen
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
naklade	naklást	k5eAaPmIp3nS	naklást
do	do	k7c2	do
vyhloubené	vyhloubený	k2eAgFnSc2d1	vyhloubená
jamky	jamka	k1gFnSc2	jamka
v	v	k7c6	v
písku	písek	k1gInSc6	písek
<g/>
,	,	kIx,	,
mechu	mech	k1gInSc6	mech
<g/>
,	,	kIx,	,
hlíně	hlína	k1gFnSc6	hlína
či	či	k8xC	či
suché	suchý	k2eAgFnSc3d1	suchá
trávě	tráva	k1gFnSc3	tráva
3-15	[number]	k4	3-15
kožovitých	kožovitý	k2eAgNnPc2d1	kožovité
měkkých	měkký	k2eAgNnPc2d1	měkké
vajíček	vajíčko	k1gNnPc2	vajíčko
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
cca	cca	kA	cca
15	[number]	k4	15
<g/>
×	×	k?	×
<g/>
8	[number]	k4	8
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
půlky	půlka	k1gFnSc2	půlka
měsíce	měsíc	k1gInSc2	měsíc
července	červenec	k1gInSc2	červenec
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
po	po	k7c6	po
56	[number]	k4	56
dnech	den	k1gInPc6	den
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vylíhnou	vylíhnout	k5eAaPmIp3nP	vylíhnout
mláďata	mládě	k1gNnPc1	mládě
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
délky	délka	k1gFnPc4	délka
kolem	kolem	k7c2	kolem
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
vylíhnutí	vylíhnutí	k1gNnSc2	vylíhnutí
jsou	být	k5eAaImIp3nP	být
odkázána	odkázán	k2eAgFnSc1d1	odkázána
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
a	a	k8xC	a
potravu	potrava	k1gFnSc4	potrava
si	se	k3xPyFc3	se
musí	muset	k5eAaImIp3nP	muset
shánět	shánět	k5eAaImF	shánět
sama	sám	k3xTgMnSc4	sám
<g/>
.	.	kIx.	.
</s>
<s>
Dospělosti	dospělost	k1gFnPc1	dospělost
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
v	v	k7c6	v
1,5	[number]	k4	1,5
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Ještěrka	ještěrka	k1gFnSc1	ještěrka
je	být	k5eAaImIp3nS	být
nejaktivnější	aktivní	k2eAgFnSc1d3	nejaktivnější
v	v	k7c6	v
ranních	ranní	k2eAgFnPc6d1	ranní
a	a	k8xC	a
podvečerních	podvečerní	k2eAgFnPc6d1	podvečerní
hodinách	hodina	k1gFnPc6	hodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
upadá	upadat	k5eAaPmIp3nS	upadat
do	do	k7c2	do
zimního	zimní	k2eAgInSc2d1	zimní
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
zalézá	zalézat	k5eAaImIp3nS	zalézat
do	do	k7c2	do
opuštěných	opuštěný	k2eAgFnPc2d1	opuštěná
nor	nora	k1gFnPc2	nora
savců	savec	k1gMnPc2	savec
(	(	kIx(	(
<g/>
např.	např.	kA	např.
hraboši	hraboš	k1gMnPc1	hraboš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kořeny	kořen	k1gInPc7	kořen
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
různých	různý	k2eAgFnPc2d1	různá
skulinek	skulinka	k1gFnPc2	skulinka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
se	se	k3xPyFc4	se
probouzí	probouzet	k5eAaImIp3nS	probouzet
ze	z	k7c2	z
zimního	zimní	k2eAgInSc2d1	zimní
spánku	spánek	k1gInSc2	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
okolí	okolí	k1gNnSc6	okolí
mnoho	mnoho	k4c1	mnoho
nepřátel	nepřítel	k1gMnPc2	nepřítel
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gMnPc4	on
patří	patřit	k5eAaImIp3nP	patřit
například	například	k6eAd1	například
lišky	liška	k1gFnPc1	liška
<g/>
,	,	kIx,	,
volavky	volavka	k1gFnPc1	volavka
<g/>
,	,	kIx,	,
čápi	čáp	k1gMnPc1	čáp
<g/>
,	,	kIx,	,
<g/>
kolem	kolem	k7c2	kolem
měst	město	k1gNnPc2	město
kočky	kočka	k1gFnSc2	kočka
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
predátoři	predátor	k1gMnPc1	predátor
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
poškození	poškození	k1gNnSc6	poškození
ocasu	ocas	k1gInSc2	ocas
se	se	k3xPyFc4	se
v	v	k7c6	v
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
svaly	sval	k1gInPc4	sval
nejtenčí	tenký	k2eAgInPc4d3	nejtenčí
<g/>
,	,	kIx,	,
poruší	porušit	k5eAaPmIp3nS	porušit
a	a	k8xC	a
ocas	ocas	k1gInSc1	ocas
se	se	k3xPyFc4	se
oddělí	oddělit	k5eAaPmIp3nS	oddělit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
zvláště	zvláště	k6eAd1	zvláště
chráněna	chránit	k5eAaImNgFnS	chránit
jako	jako	k8xC	jako
silně	silně	k6eAd1	silně
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
zakázán	zakázán	k2eAgInSc4d1	zakázán
i	i	k8xC	i
její	její	k3xOp3gInSc4	její
odchyt	odchyt	k1gInSc4	odchyt
<g/>
,	,	kIx,	,
chov	chov	k1gInSc4	chov
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
a	a	k8xC	a
prodej	prodej	k1gInSc1	prodej
<g/>
.	.	kIx.	.
</s>
<s>
Chráněna	chráněn	k2eAgNnPc1d1	chráněno
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
její	její	k3xOp3gNnPc4	její
vývojová	vývojový	k2eAgNnPc4d1	vývojové
stádia	stádium	k1gNnPc4	stádium
a	a	k8xC	a
sídla	sídlo	k1gNnPc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Steinbachův	Steinbachův	k2eAgMnSc1d1	Steinbachův
velký	velký	k2eAgMnSc1d1	velký
průvodce	průvodce	k1gMnSc1	průvodce
přírodou	příroda	k1gFnSc7	příroda
<g/>
,	,	kIx,	,
GeoCenter	GeoCenter	k1gInSc1	GeoCenter
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Ještěrka	ještěrka	k1gFnSc1	ještěrka
obecná	obecný	k2eAgFnSc1d1	obecná
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Ještěrka	ještěrka	k1gFnSc1	ještěrka
obecná	obecná	k1gFnSc1	obecná
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
videoukázka	videoukázka	k1gFnSc1	videoukázka
ještěrky	ještěrka	k1gFnSc2	ještěrka
obecné	obecný	k2eAgInPc4d1	obecný
obrázky	obrázek	k1gInPc4	obrázek
ještěrky	ještěrka	k1gFnSc2	ještěrka
obecné	obecný	k2eAgInPc4d1	obecný
rendy	rend	k1gInPc4	rend
<g/>
.	.	kIx.	.
<g/>
eu	eu	k?	eu
–	–	k?	–
Ještěrka	ještěrka	k1gFnSc1	ještěrka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Lacerta	Lacerta	k1gFnSc1	Lacerta
agilis	agilis	k1gFnSc1	agilis
<g/>
)	)	kIx)	)
obec	obec	k1gFnSc1	obec
Blučina	Blučin	k2eAgFnSc1d1	Blučina
–	–	k?	–
Ještěrka	ještěrka	k1gFnSc1	ještěrka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Lacerta	Lacerta	k1gFnSc1	Lacerta
agilis	agilis	k1gFnSc2	agilis
<g/>
)	)	kIx)	)
Inifeniny	Inifenin	k2eAgFnSc2d1	Inifenin
dračí	dračí	k2eAgFnSc2d1	dračí
stránky	stránka	k1gFnSc2	stránka
–	–	k?	–
Ještěrka	ještěrka	k1gFnSc1	ještěrka
obecná	obecná	k1gFnSc1	obecná
Arachnomania	Arachnomanium	k1gNnSc2	Arachnomanium
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Anatomie	anatomie	k1gFnSc2	anatomie
ještěrky	ještěrka	k1gFnSc2	ještěrka
obecné	obecná	k1gFnSc2	obecná
Terarista	terarista	k1gMnSc1	terarista
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
–	–	k?	–
Lacerta	Lacerta	k1gFnSc1	Lacerta
agilis	agilis	k1gFnSc1	agilis
–	–	k?	–
ještěrka	ještěrka	k1gFnSc1	ještěrka
obecná	obecná	k1gFnSc1	obecná
</s>
