<s>
Zatímco	zatímco	k8xS	zatímco
gotický	gotický	k2eAgInSc1d1	gotický
román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
módní	módní	k2eAgInSc4d1	módní
dobový	dobový	k2eAgInSc4d1	dobový
žánr	žánr	k1gInSc4	žánr
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
literární	literární	k2eAgFnSc1d1	literární
kritika	kritika	k1gFnSc1	kritika
zpravidla	zpravidla	k6eAd1	zpravidla
upírá	upírat	k5eAaImIp3nS	upírat
umělecké	umělecký	k2eAgFnSc2d1	umělecká
kvality	kvalita	k1gFnSc2	kvalita
<g/>
,	,	kIx,	,
díla	dílo	k1gNnSc2	dílo
hnutí	hnutí	k1gNnSc1	hnutí
Sturm	Sturm	k1gInSc1	Sturm
und	und	k?	und
Drang	Drang	k1gInSc1	Drang
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
čelnými	čelný	k2eAgMnPc7d1	čelný
představili	představit	k5eAaPmAgMnP	představit
byli	být	k5eAaImAgMnP	být
Johann	Johann	k1gMnSc1	Johann
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Goethe	Goethe	k1gFnSc1	Goethe
a	a	k8xC	a
Friedrich	Friedrich	k1gMnSc1	Friedrich
Schiller	Schiller	k1gMnSc1	Schiller
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
čtenáři	čtenář	k1gMnPc1	čtenář
i	i	k8xC	i
kritikou	kritika	k1gFnSc7	kritika
ceněna	cenit	k5eAaImNgFnS	cenit
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
vysokou	vysoký	k2eAgFnSc4d1	vysoká
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
