<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
více	hodně	k6eAd2	hodně
než	než	k8xS	než
9	[number]	k4	9
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
s	s	k7c7	s
předměstími	předměstí	k1gNnPc7	předměstí
okolo	okolo	k7c2	okolo
40	[number]	k4	40
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
10	[number]	k4	10
%	%	kIx~	%
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
25	[number]	k4	25
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
japonské	japonský	k2eAgFnPc1d1	japonská
populace	populace	k1gFnPc1	populace
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
Japonska	Japonsko	k1gNnSc2	Japonsko
<g/>
.	.	kIx.	.
</s>
