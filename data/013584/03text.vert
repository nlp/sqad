<s>
Macocha	Macocha	k1gFnSc1
</s>
<s>
Na	na	k7c6
dně	dno	k1gNnSc6
propasti	propast	k1gFnSc2
</s>
<s>
Propast	propast	k1gFnSc1
Macocha	Macocha	k1gFnSc1
na	na	k7c4
podzim	podzim	k1gInSc4
</s>
<s>
Propast	propast	k1gFnSc1
Macocha	Macocha	k1gFnSc1
je	být	k5eAaImIp3nS
nejznámější	známý	k2eAgFnSc1d3
propast	propast	k1gFnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
Moravského	moravský	k2eAgInSc2d1
krasu	kras	k1gInSc2
a	a	k8xC
součást	součást	k1gFnSc4
komplexu	komplex	k1gInSc2
Punkevních	punkevní	k2eAgFnPc2d1
jeskyní	jeskyně	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hloubka	hloubka	k1gFnSc1
suché	suchý	k2eAgFnSc2d1
části	část	k1gFnSc2
propasti	propast	k1gFnSc2
je	být	k5eAaImIp3nS
138,4	138,4	k4
m	m	kA
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
na	na	k7c6
jejím	její	k3xOp3gNnSc6
dně	dno	k1gNnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
Horní	horní	k2eAgFnSc1d1
(	(	kIx(
<g/>
hloubka	hloubka	k1gFnSc1
11	#num#	k4
m	m	kA
<g/>
)	)	kIx)
a	a	k8xC
Dolní	dolní	k2eAgNnSc1d1
macošské	macošský	k2eAgNnSc1d1
jezírko	jezírko	k1gNnSc1
(	(	kIx(
<g/>
hloubka	hloubka	k1gFnSc1
min	min	kA
<g/>
.	.	kIx.
50	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Propast	propast	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
obce	obec	k1gFnSc2
Vilémovice	Vilémovice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pověst	pověst	k1gFnSc1
</s>
<s>
Macocha	Macocha	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1830	#num#	k4
dle	dle	k7c2
F.	F.	kA
Richtera	Richter	k1gMnSc2
</s>
<s>
První	první	k4xOgInSc1
záznam	záznam	k1gInSc1
pověsti	pověst	k1gFnSc2
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1793	#num#	k4
z	z	k7c2
pera	pero	k1gNnSc2
Františka	František	k1gMnSc2
Josefa	Josef	k1gMnSc2
Schwoye	Schwoy	k1gMnSc2
(	(	kIx(
<g/>
Topographie	Topographie	k1gFnSc1
von	von	k1gInSc4
Markgrafthum	Markgrafthum	k1gInSc1
Mahren	Mahrna	k1gFnPc2
<g/>
,	,	kIx,
Vídeň	Vídeň	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
K	k	k7c3
názvu	název	k1gInSc3
propasti	propast	k1gFnSc2
se	se	k3xPyFc4
vztahuje	vztahovat	k5eAaImIp3nS
pověst	pověst	k1gFnSc1
<g/>
,	,	kIx,
podle	podle	k7c2
všeho	všecek	k3xTgNnSc2
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
založená	založený	k2eAgFnSc1d1
na	na	k7c6
skutečné	skutečný	k2eAgFnSc6d1
události	událost	k1gFnSc6
<g/>
:	:	kIx,
jeden	jeden	k4xCgMnSc1
místní	místní	k2eAgMnSc1d1
vesničan	vesničan	k1gMnSc1
ovdověl	ovdovět	k5eAaPmAgMnS
a	a	k8xC
zůstal	zůstat	k5eAaPmAgMnS
sám	sám	k3xTgMnSc1
s	s	k7c7
malým	malý	k1gMnSc7
synkem	synek	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oženil	oženit	k5eAaPmAgMnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
a	a	k8xC
tím	ten	k3xDgNnSc7
přivedl	přivést	k5eAaPmAgMnS
synkovi	synek	k1gMnSc3
nevlastní	vlastnit	k5eNaImIp3nP
matku	matka	k1gFnSc4
(	(	kIx(
<g/>
macechu	macecha	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc2
averze	averze	k1gFnSc2
vůči	vůči	k7c3
pastorkovi	pastorek	k1gMnSc3
byla	být	k5eAaImAgFnS
tak	tak	k6eAd1
velká	velký	k2eAgFnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gInSc2
jednoho	jeden	k4xCgInSc2
dne	den	k1gInSc2
vylákala	vylákat	k5eAaPmAgFnS
do	do	k7c2
lesa	les	k1gInSc2
k	k	k7c3
propasti	propast	k1gFnSc3
a	a	k8xC
shodila	shodit	k5eAaPmAgFnS
ho	on	k3xPp3gMnSc4
do	do	k7c2
ní	on	k3xPp3gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chlapec	chlapec	k1gMnSc1
se	se	k3xPyFc4
však	však	k9
zachytil	zachytit	k5eAaPmAgMnS
nehluboko	hluboko	k6eNd1
pod	pod	k7c7
okrajem	okraj	k1gInSc7
propasti	propast	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
ho	on	k3xPp3gMnSc4
posléze	posléze	k6eAd1
vesničané	vesničan	k1gMnPc1
našli	najít	k5eAaPmAgMnP
a	a	k8xC
vytáhli	vytáhnout	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
zjistili	zjistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
události	událost	k1gFnPc1
seběhly	seběhnout	k5eAaPmAgFnP
<g/>
,	,	kIx,
svrhli	svrhnout	k5eAaPmAgMnP
do	do	k7c2
propasti	propast	k1gFnSc2
macechu	macecha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
pověst	pověst	k1gFnSc1
vypráví	vyprávět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
chudý	chudý	k2eAgMnSc1d1
otec	otec	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
měl	mít	k5eAaImAgMnS
jen	jen	k9
synka	synek	k1gMnSc4
<g/>
,	,	kIx,
si	se	k3xPyFc3
vzal	vzít	k5eAaPmAgMnS
macechu	macecha	k1gFnSc4
(	(	kIx(
<g/>
podle	podle	k7c2
které	který	k3yIgFnSc2,k3yQgFnSc2,k3yRgFnSc2
se	se	k3xPyFc4
propast	propast	k1gFnSc1
jmenuje	jmenovat	k5eAaBmIp3nS,k5eAaImIp3nS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Macecha	macecha	k1gFnSc1
svého	svůj	k3xOyFgMnSc2
nevlastního	vlastní	k2eNgMnSc2d1
syna	syn	k1gMnSc2
nenáviděla	nenávidět	k5eAaImAgFnS,k5eNaImAgFnS
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
ho	on	k3xPp3gMnSc4
shodila	shodit	k5eAaPmAgFnS
do	do	k7c2
propasti	propast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chlapec	chlapec	k1gMnSc1
se	se	k3xPyFc4
však	však	k9
zachránil	zachránit	k5eAaPmAgMnS
a	a	k8xC
utekl	utéct	k5eAaPmAgMnS
do	do	k7c2
vesnice	vesnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Macecha	macecha	k1gFnSc1
to	ten	k3xDgNnSc1
však	však	k9
nevěděla	vědět	k5eNaImAgFnS
a	a	k8xC
sama	sám	k3xTgFnSc1
z	z	k7c2
lítosti	lítost	k1gFnSc2
nad	nad	k7c7
svým	svůj	k3xOyFgInSc7
činem	čin	k1gInSc7
do	do	k7c2
propasti	propast	k1gFnSc2
skočila	skočit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
objevů	objev	k1gInPc2
</s>
<s>
Poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
o	o	k7c6
propasti	propast	k1gFnSc6
Macoše	Macocha	k1gFnSc6
zmiňuje	zmiňovat	k5eAaImIp3nS
v	v	k7c6
písemných	písemný	k2eAgInPc6d1
dokumentech	dokument	k1gInPc6
řeholník	řeholník	k1gMnSc1
zábrdovického	zábrdovický	k2eAgInSc2d1
kláštera	klášter	k1gInSc2
Martin	Martin	k1gMnSc1
Alexander	Alexandra	k1gFnPc2
Vigsius	Vigsius	k1gInSc1
roku	rok	k1gInSc2
1663	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
člověkem	člověk	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
prokazatelně	prokazatelně	k6eAd1
sestoupil	sestoupit	k5eAaPmAgMnS
na	na	k7c4
dno	dno	k1gNnSc4
Macochy	Macocha	k1gFnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
Lazar	Lazar	k1gMnSc1
Schopper	Schopper	k1gMnSc1
<g/>
,	,	kIx,
pozdější	pozdní	k2eAgMnSc1d2
provinciál	provinciál	k1gMnSc1
řádu	řád	k1gInSc2
Minoritů	minorita	k1gMnPc2
na	na	k7c6
Moravě	Morava	k1gFnSc6
–	–	k?
stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
roku	rok	k1gInSc2
1723	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1748	#num#	k4
Macochu	Macocha	k1gFnSc4
zdolala	zdolat	k5eAaPmAgFnS
z	z	k7c2
iniciativy	iniciativa	k1gFnSc2
Johanna	Johanna	k1gFnSc1
Nagela	Nagela	k1gFnSc1
expedice	expedice	k1gFnSc1
horníků	horník	k1gMnPc2
<g/>
,	,	kIx,
sám	sám	k3xTgInSc1
Nagel	Nagel	k1gInSc1
se	se	k3xPyFc4
však	však	k9
sestupu	sestup	k1gInSc3
neúčastnil	účastnit	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1776	#num#	k4
se	se	k3xPyFc4
konal	konat	k5eAaImAgInS
další	další	k2eAgInSc1d1
sestup	sestup	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
vedl	vést	k5eAaImAgMnS
majitel	majitel	k1gMnSc1
rájeckého	rájecký	k2eAgNnSc2d1
panství	panství	k1gNnSc2
Karel	Karel	k1gMnSc1
Josef	Josef	k1gMnSc1
Salm	Salm	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc4
vědeckou	vědecký	k2eAgFnSc4d1
expedici	expedice	k1gFnSc4
na	na	k7c4
dno	dno	k1gNnSc4
Macochy	Macocha	k1gFnSc2
vedl	vést	k5eAaImAgMnS
roku	rok	k1gInSc2
1784	#num#	k4
Karel	Karel	k1gMnSc1
Rudzinský	Rudzinský	k2eAgMnSc1d1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
odsud	odsud	k6eAd1
přinesl	přinést	k5eAaPmAgMnS
mnoho	mnoho	k4c4
cenných	cenný	k2eAgInPc2d1
poznatků	poznatek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1808	#num#	k4
uskutečnil	uskutečnit	k5eAaPmAgInS
na	na	k7c4
dno	dno	k1gNnSc4
Macochy	Macocha	k1gFnSc2
sestup	sestup	k1gInSc1
Hugo	Hugo	k1gMnSc1
František	František	k1gMnSc1
Salm	Salm	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
polovině	polovina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
začala	začít	k5eAaPmAgFnS
éra	éra	k1gFnSc1
dalších	další	k2eAgInPc2d1
sestupů	sestup	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yQgInPc4,k3yRgInPc4
zahájil	zahájit	k5eAaPmAgInS
roku	rok	k1gInSc2
1856	#num#	k4
Jindřich	Jindřich	k1gMnSc1
Wankel	Wankel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1855	#num#	k4
napsal	napsat	k5eAaBmAgMnS,k5eAaPmAgMnS
sloupský	sloupský	k2eAgMnSc1d1
kaplan	kaplan	k1gMnSc1
Jan	Jan	k1gMnSc1
Nepomuk	Nepomuk	k1gMnSc1
Soukop	Soukop	k1gInSc4
báseň	báseň	k1gFnSc4
o	o	k7c6
Macoše	Macocha	k1gFnSc6
a	a	k8xC
o	o	k7c4
tři	tři	k4xCgNnPc4
léta	léto	k1gNnPc4
později	pozdě	k6eAd2
prvního	první	k4xOgMnSc2
cestopisného	cestopisný	k2eAgMnSc2d1
průvodce	průvodce	k1gMnSc2
o	o	k7c6
Moravském	moravský	k2eAgInSc6d1
krasu	kras	k1gInSc6
„	„	k?
<g/>
Macocha	Macocha	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gNnSc1
okolí	okolí	k1gNnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1864	#num#	k4
zdolal	zdolat	k5eAaPmAgMnS
Macochu	Macocha	k1gFnSc4
další	další	k2eAgMnSc1d1
známý	známý	k1gMnSc1
badatel	badatel	k1gMnSc1
Martin	Martin	k1gMnSc1
Kříž	Kříž	k1gMnSc1
a	a	k8xC
po	po	k7c6
něm	on	k3xPp3gNnSc6
se	se	k3xPyFc4
konaly	konat	k5eAaImAgInP
další	další	k2eAgInPc1d1
sestupy	sestup	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláštním	zvláštní	k2eAgInSc7d1
druhem	druh	k1gInSc7
sestupů	sestup	k1gInPc2
na	na	k7c4
dno	dno	k1gNnSc4
Macochy	Macocha	k1gFnSc2
byly	být	k5eAaImAgFnP
výpravy	výprava	k1gFnPc1
pro	pro	k7c4
těla	tělo	k1gNnPc4
sebevrahů	sebevrah	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yQgMnPc4,k3yIgMnPc4,k3yRgMnPc4
podnikali	podnikat	k5eAaImAgMnP
vilémovičtí	vilémovický	k2eAgMnPc1d1
občané	občan	k1gMnPc1
Martin	Martin	k1gMnSc1
Kala	Kala	k1gMnSc1
a	a	k8xC
Josef	Josef	k1gMnSc1
Nejezchleb	Nejezchleb	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1891	#num#	k4
napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
známý	známý	k2eAgMnSc1d1
krasový	krasový	k2eAgMnSc1d1
badatel	badatel	k1gMnSc1
profesor	profesor	k1gMnSc1
Richard	Richard	k1gMnSc1
Trampler	Trampler	k1gMnSc1
monografii	monografie	k1gFnSc4
o	o	k7c6
Macoše	Macocha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1901	#num#	k4
vykonal	vykonat	k5eAaPmAgMnS
do	do	k7c2
Macochy	Macocha	k1gFnSc2
první	první	k4xOgFnSc1
sestup	sestup	k1gInSc4
známý	známý	k2eAgMnSc1d1
krasový	krasový	k2eAgMnSc1d1
badatel	badatel	k1gMnSc1
Karel	Karel	k1gMnSc1
Absolon	Absolon	k1gMnSc1
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
začala	začít	k5eAaPmAgFnS
éra	éra	k1gFnSc1
dalších	další	k2eAgInPc2d1
objevů	objev	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1909	#num#	k4
byly	být	k5eAaImAgFnP
objeveny	objevit	k5eAaPmNgFnP
Punkevní	punkevní	k2eAgFnPc1d1
jeskyně	jeskyně	k1gFnPc1
a	a	k8xC
roku	rok	k1gInSc3
1914	#num#	k4
bylo	být	k5eAaImAgNnS
propojeno	propojen	k2eAgNnSc1d1
dno	dno	k1gNnSc1
Macochy	Macocha	k1gFnSc2
suchou	suchý	k2eAgFnSc7d1
cestou	cesta	k1gFnSc7
z	z	k7c2
Pustého	pustý	k2eAgInSc2d1
žlebu	žleb	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cestu	cesta	k1gFnSc4
ponornou	ponorný	k2eAgFnSc7d1
říčkou	říčka	k1gFnSc7
Punkvou	Punkva	k1gFnSc7
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
zdolat	zdolat	k5eAaPmF
a	a	k8xC
zpřístupnit	zpřístupnit	k5eAaPmF
roku	rok	k1gInSc2
1933	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Vyhlídkové	vyhlídkový	k2eAgInPc1d1
můstky	můstek	k1gInPc1
</s>
<s>
Horní	horní	k2eAgInSc1d1
můstek	můstek	k1gInSc1
</s>
<s>
Horní	horní	k2eAgInSc1d1
můstek	můstek	k1gInSc1
z	z	k7c2
dolního	dolní	k2eAgInSc2d1
můstku	můstek	k1gInSc2
</s>
<s>
Na	na	k7c6
nejvyšším	vysoký	k2eAgNnSc6d3
místě	místo	k1gNnSc6
propasti	propast	k1gFnSc2
stával	stávat	k5eAaImAgInS
od	od	k7c2
počátku	počátek	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
dřevěný	dřevěný	k2eAgInSc4d1
gloriet	gloriet	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
1882	#num#	k4
nahrazen	nahradit	k5eAaPmNgInS
železným	železný	k2eAgInSc7d1
můstkem	můstek	k1gInSc7
z	z	k7c2
blanenských	blanenský	k2eAgFnPc2d1
Salmových	Salmův	k2eAgFnPc2d1
železáren	železárna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Můstek	můstek	k1gInSc1
získal	získat	k5eAaPmAgInS
roku	rok	k1gInSc2
1882	#num#	k4
pojmenování	pojmenování	k1gNnSc4
Ripka	Ripko	k1gNnSc2
Warte	Wart	k1gInSc5
<g/>
,	,	kIx,
podle	podle	k7c2
brněnského	brněnský	k2eAgMnSc2d1
průmyslníka	průmyslník	k1gMnSc2
<g/>
,	,	kIx,
politika	politik	k1gMnSc2
a	a	k8xC
organizátora	organizátor	k1gMnSc2
turistického	turistický	k2eAgInSc2d1
ruchu	ruch	k1gInSc2
Adolfa	Adolf	k1gMnSc2
Ripky	Ripka	k1gMnSc2
(	(	kIx(
<g/>
1812	#num#	k4
<g/>
–	–	k?
<g/>
1884	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pohled	pohled	k1gInSc1
z	z	k7c2
horního	horní	k2eAgInSc2d1
můstku	můstek	k1gInSc2
</s>
<s>
Můstek	můstek	k1gInSc1
byl	být	k5eAaImAgInS
vždy	vždy	k6eAd1
celoročně	celoročně	k6eAd1
přístupný	přístupný	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Původně	původně	k6eAd1
patřil	patřit	k5eAaImAgMnS
klubu	klub	k1gInSc3
turistů	turist	k1gMnPc2
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1945	#num#	k4
přešel	přejít	k5eAaPmAgInS
na	na	k7c4
stát	stát	k1gInSc4
<g/>
,	,	kIx,
do	do	k7c2
správy	správa	k1gFnSc2
jej	on	k3xPp3gNnSc4
pak	pak	k6eAd1
dostala	dostat	k5eAaPmAgFnS
státní	státní	k2eAgFnSc1d1
Správa	správa	k1gFnSc1
jeskyní	jeskyně	k1gFnPc2
Moravského	moravský	k2eAgInSc2d1
krasu	kras	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Ta	ten	k3xDgFnSc1
jej	on	k3xPp3gNnSc4
v	v	k7c6
červnu	červen	k1gInSc6
2007	#num#	k4
nákladem	náklad	k1gInSc7
necelých	celý	k2eNgFnPc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
400	#num#	k4
tisíc	tisíc	k4xCgInPc2
Kč	Kč	kA
opravila	opravit	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Pozemky	pozemka	k1gFnSc2
(	(	kIx(
<g/>
dno	dno	k1gNnSc4
Macochy	Macocha	k1gFnSc2
a	a	k8xC
přístupovou	přístupový	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
<g/>
)	)	kIx)
získala	získat	k5eAaPmAgFnS
po	po	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
obec	obec	k1gFnSc1
v	v	k7c6
rámci	rámec	k1gInSc6
vracení	vracení	k1gNnSc2
historického	historický	k2eAgInSc2d1
majetku	majetek	k1gInSc2
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
věcné	věcný	k2eAgNnSc4d1
břemeno	břemeno	k1gNnSc4
k	k	k7c3
zajištění	zajištění	k1gNnSc3
průchodu	průchod	k1gInSc2
však	však	k9
zapsáno	zapsán	k2eAgNnSc1d1
nebylo	být	k5eNaImAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obec	obec	k1gFnSc1
Vilémovice	Vilémovice	k1gFnSc1
vybírala	vybírat	k5eAaImAgFnS
vstupné	vstupné	k1gNnSc4
na	na	k7c4
horní	horní	k2eAgInSc4d1
můstek	můstek	k1gInSc4
již	již	k6eAd1
za	za	k7c4
první	první	k4xOgFnPc4
republiky	republika	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnPc2
vybíráním	vybírání	k1gNnSc7
vstupného	vstupné	k1gNnSc2
na	na	k7c4
můstek	můstek	k1gInSc4
vydělala	vydělat	k5eAaPmAgFnS
na	na	k7c4
plynofikaci	plynofikace	k1gFnSc4
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
poté	poté	k6eAd1
od	od	k7c2
vybírání	vybírání	k1gNnSc2
vstupného	vstupné	k1gNnSc2
ustoupila	ustoupit	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Později	pozdě	k6eAd2
obec	obec	k1gFnSc1
požadovala	požadovat	k5eAaImAgFnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
do	do	k7c2
její	její	k3xOp3gFnSc2
obecní	obecní	k2eAgFnSc2d1
pokladny	pokladna	k1gFnSc2
plynula	plynout	k5eAaImAgFnS
část	část	k1gFnSc1
peněz	peníze	k1gInPc2
z	z	k7c2
turistického	turistický	k2eAgInSc2d1
ruchu	ruch	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
odehrává	odehrávat	k5eAaImIp3nS
na	na	k7c6
sousedních	sousední	k2eAgInPc6d1
pozemcích	pozemek	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
ve	v	k7c4
vlastnictví	vlastnictví	k1gNnSc4
Lesů	les	k1gInPc2
ČR	ČR	kA
<g/>
,	,	kIx,
Správy	správa	k1gFnPc4
jeskyní	jeskyně	k1gFnPc2
Moravského	moravský	k2eAgInSc2d1
krasu	kras	k1gInSc2
(	(	kIx(
<g/>
parkoviště	parkoviště	k1gNnSc2
<g/>
)	)	kIx)
či	či	k8xC
soukromých	soukromý	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yRnSc1,k3yQnSc1
se	se	k3xPyFc4
se	s	k7c7
správou	správa	k1gFnSc7
jeskyní	jeskyně	k1gFnPc2
nedohodla	dohodnout	k5eNaPmAgFnS
na	na	k7c6
právu	právo	k1gNnSc6
průchodu	průchod	k1gInSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
cestu	cesta	k1gFnSc4
i	i	k8xC
dno	dno	k1gNnSc4
propasti	propast	k1gFnSc2
pronajala	pronajmout	k5eAaPmAgFnS
na	na	k7c4
50	#num#	k4
let	léto	k1gNnPc2
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
podnikateli	podnikatel	k1gMnSc5
Jiřími	Jiří	k1gMnPc7
Tichopádovi	Tichopádův	k2eAgMnPc1d1
za	za	k7c4
100	#num#	k4
tisíc	tisíc	k4xCgInPc2
Kč	Kč	kA
ročně	ročně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
4	#num#	k4
<g/>
]	]	kIx)
Ten	ten	k3xDgInSc1
požadoval	požadovat	k5eAaImAgInS
od	od	k7c2
Správy	správa	k1gFnSc2
jeskyní	jeskyně	k1gFnPc2
kompenzaci	kompenzace	k1gFnSc4
za	za	k7c4
průchod	průchod	k1gInSc4
návštěvníků	návštěvník	k1gMnPc2
přes	přes	k7c4
jeho	jeho	k3xOp3gInPc4
pozemky	pozemek	k1gInPc4
a	a	k8xC
když	když	k8xS
nebylo	být	k5eNaImAgNnS
jeho	jeho	k3xOp3gFnPc4
žádosti	žádost	k1gFnPc4
vyhověno	vyhověn	k2eAgNnSc1d1
<g/>
,	,	kIx,
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2008	#num#	k4
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
či	či	k8xC
od	od	k7c2
poloviny	polovina	k1gFnSc2
dubna	duben	k1gInSc2
2008	#num#	k4
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
krátce	krátce	k6eAd1
za	za	k7c4
vstup	vstup	k1gInSc4
na	na	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
můstek	můstek	k1gInSc1
prostřednictvím	prostřednictvím	k7c2
výběrčího	výběrčí	k1gMnSc2
doprovázeného	doprovázený	k2eAgInSc2d1
ochrankou	ochranka	k1gFnSc7
vybíral	vybírat	k5eAaImAgMnS
vstupné	vstupné	k1gNnSc4
<g/>
,	,	kIx,
poté	poté	k6eAd1
vedl	vést	k5eAaImAgInS
spor	spor	k1gInSc1
se	s	k7c7
Správou	správa	k1gFnSc7
jeskyní	jeskyně	k1gFnSc7
Moravského	moravský	k2eAgInSc2d1
krasu	kras	k1gInSc2
o	o	k7c6
vlastnictví	vlastnictví	k1gNnSc6
můstku	můstek	k1gInSc2
a	a	k8xC
na	na	k7c4
dobu	doba	k1gFnSc4
jednání	jednání	k1gNnSc2
vybírání	vybírání	k1gNnSc2
poplatku	poplatek	k1gInSc2
přerušil	přerušit	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Klub	klub	k1gInSc1
českých	český	k2eAgMnPc2d1
turistů	turist	k1gMnPc2
vyzýval	vyzývat	k5eAaImAgInS
k	k	k7c3
neplacení	neplacení	k1gNnSc3
poplatku	poplatek	k1gInSc2
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Krajský	krajský	k2eAgInSc1d1
úřad	úřad	k1gInSc1
Jihomoravského	jihomoravský	k2eAgInSc2d1
kraje	kraj	k1gInSc2
označil	označit	k5eAaPmAgMnS
vybírání	vybírání	k1gNnSc4
poplatku	poplatek	k1gInSc2
za	za	k7c4
nezákonné	zákonný	k2eNgInPc4d1
a	a	k8xC
zaslal	zaslat	k5eAaPmAgMnS
obci	obec	k1gFnSc3
Vilémovice	Vilémovice	k1gFnSc2
výzvu	výzva	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jako	jako	k8xS,k8xC
příslušný	příslušný	k2eAgInSc1d1
silniční	silniční	k2eAgInSc1d1
správní	správní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
zahájila	zahájit	k5eAaPmAgFnS
správní	správní	k2eAgNnSc4d1
řízení	řízení	k1gNnSc4
ve	v	k7c6
věci	věc	k1gFnSc6
podezření	podezření	k1gNnSc2
ze	z	k7c2
spáchání	spáchání	k1gNnSc2
správního	správní	k2eAgInSc2d1
deliktu	delikt	k1gInSc2
proti	proti	k7c3
zákonu	zákon	k1gInSc3
o	o	k7c6
pozemních	pozemní	k2eAgFnPc6d1
komunikacích	komunikace	k1gFnPc6
spočívajícího	spočívající	k2eAgInSc2d1
v	v	k7c6
neoprávněném	oprávněný	k2eNgNnSc6d1
vybírání	vybírání	k1gNnSc6
poplatku	poplatek	k1gInSc2
za	za	k7c4
užívání	užívání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tichopád	Tichopáda	k1gFnPc2
zastával	zastávat	k5eAaImAgMnS
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
tato	tento	k3xDgFnSc1
cesta	cesta	k1gFnSc1
není	být	k5eNaImIp3nS
pozemní	pozemní	k2eAgFnSc7d1
komunikací	komunikace	k1gFnSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Správa	správa	k1gFnSc1
jeskyní	jeskyně	k1gFnPc2
Moravského	moravský	k2eAgInSc2d1
krasu	kras	k1gInSc2
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
parcely	parcela	k1gFnSc2
je	být	k5eAaImIp3nS
evidována	evidován	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
veřejná	veřejný	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
hodlal	hodlat	k5eAaImAgMnS
Jiří	Jiří	k1gMnSc1
Tichopád	Tichopáda	k1gFnPc2
zahradit	zahradit	k5eAaPmF
přístup	přístup	k1gInSc4
k	k	k7c3
můstku	můstek	k1gInSc3
<g/>
,	,	kIx,
opětovně	opětovně	k6eAd1
zavést	zavést	k5eAaPmF
vstupné	vstupné	k1gNnSc4
a	a	k8xC
zavést	zavést	k5eAaPmF
provozní	provozní	k2eAgInSc4d1
řád	řád	k1gInSc4
<g/>
,	,	kIx,
podle	podle	k7c2
nějž	jenž	k3xRgInSc2
by	by	kYmCp3nS
přístup	přístup	k1gInSc4
na	na	k7c4
můstek	můstek	k1gInSc4
reguloval	regulovat	k5eAaImAgInS
hlídač	hlídač	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
by	by	kYmCp3nS
dohlížel	dohlížet	k5eAaImAgInS
i	i	k9
na	na	k7c4
chování	chování	k1gNnSc4
návštěvníků	návštěvník	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2011	#num#	k4
začal	začít	k5eAaPmAgMnS
podnikatel	podnikatel	k1gMnSc1
opět	opět	k6eAd1
vybírat	vybírat	k5eAaImF
vstupné	vstupné	k1gNnSc4
za	za	k7c4
průchod	průchod	k1gInSc4
k	k	k7c3
můstku	můstek	k1gInSc3
20	#num#	k4
Kč	Kč	kA
za	za	k7c4
dospělého	dospělý	k1gMnSc4
a	a	k8xC
10	#num#	k4
Kč	Kč	kA
za	za	k7c4
dítě	dítě	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k2eAgFnPc4d1
Správy	správa	k1gFnPc4
jeskyní	jeskyně	k1gFnPc2
Moravského	moravský	k2eAgInSc2d1
krasu	kras	k1gInSc2
Jiří	Jiří	k1gMnSc1
Hebelka	Hebelka	k1gMnSc1
považoval	považovat	k5eAaImAgMnS
vybírání	vybírání	k1gNnSc4
za	za	k7c4
neoprávněné	oprávněný	k2eNgFnPc4d1
a	a	k8xC
oznámil	oznámit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
věcí	věc	k1gFnSc7
budou	být	k5eAaImBp3nP
zabývat	zabývat	k5eAaImF
jejich	jejich	k3xOp3gMnPc1
právníci	právník	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
ukončení	ukončení	k1gNnSc3
vybírání	vybírání	k1gNnSc2
vstupného	vstupné	k1gNnSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
správa	správa	k1gFnSc1
jeskyní	jeskyně	k1gFnPc2
dohodla	dohodnout	k5eAaPmAgFnS
s	s	k7c7
vedením	vedení	k1gNnSc7
obce	obec	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
Vilémovice	Vilémovice	k1gFnSc1
Jiřímu	Jiří	k1gMnSc3
Tichopádovi	Tichopád	k1gMnSc3
už	už	k6eAd1
pozemky	pozemka	k1gFnPc4
pronajímat	pronajímat	k5eAaImF
nebudou	být	k5eNaImBp3nP
a	a	k8xC
vybírání	vybírání	k1gNnSc6
vstupného	vstupné	k1gNnSc2
skončí	skončit	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
oplátku	oplátka	k1gFnSc4
bude	být	k5eAaImBp3nS
obec	obec	k1gFnSc1
Vilémovice	Vilémovice	k1gFnSc2
participovat	participovat	k5eAaImF
na	na	k7c6
ziscích	zisk	k1gInPc6
z	z	k7c2
turistického	turistický	k2eAgInSc2d1
ruchu	ruch	k1gInSc2
na	na	k7c6
Macoše	Macocha	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dolní	dolní	k2eAgInSc1d1
můstek	můstek	k1gInSc1
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
dno	dno	k1gNnSc4
z	z	k7c2
dolního	dolní	k2eAgInSc2d1
můstku	můstek	k1gInSc2
</s>
<s>
Byl	být	k5eAaImAgInS
postaven	postaven	k2eAgInSc1d1
roku	rok	k1gInSc2
1899	#num#	k4
nákladem	náklad	k1gInSc7
800	#num#	k4
zlatých	zlatá	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
opatřen	opatřit	k5eAaPmNgMnS
výběžkem	výběžek	k1gInSc7
s	s	k7c7
padacími	padací	k2eAgNnPc7d1
dvířky	dvířka	k1gNnPc7
<g/>
,	,	kIx,
ke	k	k7c3
kterému	který	k3yRgMnSc3,k3yIgMnSc3,k3yQgMnSc3
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
1913	#num#	k4
instalován	instalován	k2eAgInSc1d1
železný	železný	k2eAgInSc1d1
žebřík	žebřík	k1gInSc1
až	až	k9
na	na	k7c4
dno	dno	k1gNnSc4
Macochy	Macocha	k1gFnSc2
(	(	kIx(
<g/>
žebřík	žebřík	k1gInSc1
byl	být	k5eAaImAgInS
roku	rok	k1gInSc2
1933	#num#	k4
demontován	demontován	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
menšími	malý	k2eAgFnPc7d2
úpravami	úprava	k1gFnPc7
byl	být	k5eAaImAgInS
můstek	můstek	k1gInSc1
v	v	k7c6
provozu	provoz	k1gInSc6
až	až	k6eAd1
roku	rok	k1gInSc2
2000	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byl	být	k5eAaImAgInS
ve	v	k7c6
špatném	špatný	k2eAgInSc6d1
technickém	technický	k2eAgInSc6d1
stavu	stav	k1gInSc6
a	a	k8xC
musel	muset	k5eAaImAgInS
být	být	k5eAaImF
demontován	demontovat	k5eAaBmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc4d1
opravený	opravený	k2eAgInSc4d1
můstek	můstek	k1gInSc4
byl	být	k5eAaImAgInS
otevřen	otevřen	k2eAgInSc1d1
roku	rok	k1gInSc2
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Turistická	turistický	k2eAgFnSc1d1
chata	chata	k1gFnSc1
</s>
<s>
Chata	chata	k1gFnSc1
Macocha	Macocha	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1894	#num#	k4
byla	být	k5eAaImAgFnS
zásluhou	zásluhou	k7c2
Adolfa	Adolf	k1gMnSc2
Podroužka	Podroužek	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
sám	sám	k3xTgMnSc1
vykonal	vykonat	k5eAaPmAgInS
tři	tři	k4xCgInPc4
sestupy	sestup	k1gInPc4
do	do	k7c2
Macochy	Macocha	k1gFnSc2
<g/>
,	,	kIx,
postavena	postaven	k2eAgFnSc1d1
turistická	turistický	k2eAgFnSc1d1
chata	chata	k1gFnSc1
Klubu	klub	k1gInSc2
českých	český	k2eAgMnPc2d1
turistů	turist	k1gMnPc2
<g/>
,	,	kIx,
zvaná	zvaný	k2eAgFnSc1d1
„	„	k?
<g/>
Útulna	útulna	k1gFnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otevřena	otevřen	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
25	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1895	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
chata	chata	k1gFnSc1
však	však	k9
počátkem	počátkem	k7c2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
vyhořela	vyhořet	k5eAaPmAgFnS
a	a	k8xC
tak	tak	k6eAd1
byla	být	k5eAaImAgFnS
postavená	postavený	k2eAgFnSc1d1
nová	nový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
tehdy	tehdy	k6eAd1
Moravskému	moravský	k2eAgInSc3d1
krasu	kras	k1gInSc3
říkalo	říkat	k5eAaImAgNnS
„	„	k?
<g/>
Moravské	moravský	k2eAgNnSc1d1
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
styl	styl	k1gInSc4
švýcarské	švýcarský	k2eAgFnSc2d1
chaty	chata	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chata	chata	k1gFnSc1
Macocha	Macocha	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1996	#num#	k4
zrekonstruována	zrekonstruovat	k5eAaPmNgFnS
a	a	k8xC
modernizována	modernizovat	k5eAaBmNgFnS
<g/>
.	.	kIx.
</s>
<s>
Salmova	Salmův	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
</s>
<s>
Stezku	stezka	k1gFnSc4
nechal	nechat	k5eAaPmAgMnS
koncem	koncem	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
zbudovat	zbudovat	k5eAaPmF
hrabě	hrabě	k1gMnSc1
Hugo	Hugo	k1gMnSc1
Salm-Reifferscheidt	Salm-Reifferscheidt	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
velkému	velký	k2eAgNnSc3d1
převýšení	převýšení	k1gNnSc3
a	a	k8xC
skalnatému	skalnatý	k2eAgInSc3d1
terénu	terén	k1gInSc3
musely	muset	k5eAaImAgInP
být	být	k5eAaImF
některé	některý	k3yIgFnPc4
její	její	k3xOp3gFnPc4
části	část	k1gFnPc4
vytesány	vytesán	k2eAgFnPc4d1
do	do	k7c2
skály	skála	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karel	Karel	k1gMnSc1
Absolon	Absolon	k1gMnSc1
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
roku	rok	k1gInSc2
1898	#num#	k4
byly	být	k5eAaImAgFnP
při	při	k7c6
vybírání	vybírání	k1gNnSc6
kamene	kámen	k1gInSc2
na	na	k7c4
stavbu	stavba	k1gFnSc4
Salmovy	Salmův	k2eAgFnPc1d1
stezky	stezka	k1gFnPc1
objeveny	objevit	k5eAaPmNgFnP
dvě	dva	k4xCgFnPc1
propasti	propast	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
byly	být	k5eAaImAgFnP
nazvány	nazvat	k5eAaBmNgInP,k5eAaPmNgInP
Koudelkovými	Koudelkův	k2eAgInPc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stezka	stezka	k1gFnSc1
je	být	k5eAaImIp3nS
dlouhá	dlouhý	k2eAgFnSc1d1
asi	asi	k9
1	#num#	k4
km	km	kA
a	a	k8xC
vede	vést	k5eAaImIp3nS
od	od	k7c2
propasti	propast	k1gFnSc2
Macocha	Macocha	k1gFnSc1
do	do	k7c2
Pustého	pustý	k2eAgInSc2d1
žlebu	žleb	k1gInSc2
do	do	k7c2
lokality	lokalita	k1gFnSc2
„	„	k?
<g/>
Pod	pod	k7c7
Salmovkou	Salmovka	k1gFnSc7
<g/>
“	“	k?
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
leží	ležet	k5eAaImIp3nS
cca	cca	kA
600	#num#	k4
m	m	kA
od	od	k7c2
Punkevních	punkevní	k2eAgFnPc2d1
jeskyní	jeskyně	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Květena	květena	k1gFnSc1
</s>
<s>
Ve	v	k7c6
stěně	stěna	k1gFnSc6
propasti	propast	k1gFnSc2
roste	růst	k5eAaImIp3nS
glaciální	glaciální	k2eAgInSc1d1
relikt	relikt	k1gInSc1
kruhatky	kruhatka	k1gFnSc2
Mattioliho	Mattioli	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Fascinující	fascinující	k2eAgNnPc1d1
čísla	číslo	k1gNnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ABSOLON	Absolon	k1gMnSc1
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravský	moravský	k2eAgInSc1d1
kras	kras	k1gInSc1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
.	.	kIx.
415	#num#	k4
s.	s.	k?
Kapitola	kapitola	k1gFnSc1
F.	F.	kA
J.	J.	kA
Schwoy	Schwoa	k1gMnSc2
<g/>
,	,	kIx,
s.	s.	k?
31	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Adolf	Adolf	k1gMnSc1
Ripka	Ripka	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
encyklopedie	encyklopedie	k1gFnPc1
<g/>
.	.	kIx.
<g/>
brna	brna	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Za	za	k7c4
pohled	pohled	k1gInSc4
do	do	k7c2
Macochy	Macocha	k1gFnSc2
si	se	k3xPyFc3
turisté	turist	k1gMnPc1
zaplatí	zaplatit	k5eAaPmIp3nP
<g/>
,	,	kIx,
podnikatel	podnikatel	k1gMnSc1
zavedl	zavést	k5eAaPmAgMnS
„	„	k?
<g/>
průchodné	průchodný	k2eAgNnSc1d1
<g/>
“	“	k?
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
,	,	kIx,
bom	bom	k?
(	(	kIx(
<g/>
Martin	Martin	k1gMnSc1
Bořil	Bořil	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ČTK1	ČTK1	k1gFnSc1
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Pavla	Pavla	k1gFnSc1
Komárková	Komárková	k1gFnSc1
<g/>
:	:	kIx,
Za	za	k7c4
pohled	pohled	k1gInSc4
do	do	k7c2
Macochy	Macocha	k1gFnSc2
už	už	k9
lidé	člověk	k1gMnPc1
neplatí	platit	k5eNaImIp3nP
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
12	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
↑	↑	k?
Pohled	pohled	k1gInSc1
do	do	k7c2
Macochy	Macocha	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Nejdřív	dříve	k6eAd3
dvacet	dvacet	k4xCc1
korun	koruna	k1gFnPc2
<g/>
1	#num#	k4
2	#num#	k4
Pavla	Pavla	k1gFnSc1
Komárková	Komárková	k1gFnSc1
<g/>
:	:	kIx,
Kraj	kraj	k1gInSc1
<g/>
:	:	kIx,
Podnikatel	podnikatel	k1gMnSc1
vybíral	vybírat	k5eAaImAgMnS
peníze	peníz	k1gInPc4
za	za	k7c4
pohled	pohled	k1gInSc4
do	do	k7c2
Macochy	Macocha	k1gFnSc2
nezákonně	zákonně	k6eNd1
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
20081	#num#	k4
2	#num#	k4
Zuzana	Zuzana	k1gFnSc1
Klementová	Klementová	k1gFnSc1
<g/>
:	:	kIx,
Pozemek	pozemek	k1gInSc1
nad	nad	k7c7
propastí	propast	k1gFnSc7
Macocha	Macocha	k1gFnSc1
je	být	k5eAaImIp3nS
dobrý	dobrý	k2eAgInSc4d1
kšeft	kšeft	k?
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
12	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
20081	#num#	k4
2	#num#	k4
Pavla	Pavla	k1gFnSc1
Komárková	Komárková	k1gFnSc1
<g/>
:	:	kIx,
Bude	být	k5eAaImBp3nS
se	se	k3xPyFc4
za	za	k7c4
pohled	pohled	k1gInSc4
do	do	k7c2
Macochy	Macocha	k1gFnSc2
platit	platit	k5eAaImF
<g/>
?	?	kIx.
</s>
<s desamb="1">
Nájemce	nájemce	k1gMnSc1
cesty	cesta	k1gFnSc2
chce	chtít	k5eAaImIp3nS
vstupné	vstupné	k1gNnSc4
i	i	k8xC
větší	veliký	k2eAgFnSc4d2
bezpečnost	bezpečnost	k1gFnSc4
<g/>
,	,	kIx,
iDnes	iDnes	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
5	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
↑	↑	k?
Vybírání	vybírání	k1gNnSc1
vstupného	vstupné	k1gNnSc2
na	na	k7c4
Horní	horní	k2eAgInSc4d1
můstek	můstek	k1gInSc4
na	na	k7c6
Macoše	Macocha	k1gFnSc6
ve	v	k7c4
čtvrtek	čtvrtek	k1gInSc4
skončí	skončit	k5eAaPmIp3nS
<g/>
↑	↑	k?
http://brnensky.denik.cz/serialy/kvetina-z-doby-ledove-roste-jedine-v-macose-20120304.html	http://brnensky.denik.cz/serialy/kvetina-z-doby-ledove-roste-jedine-v-macose-20120304.html	k1gInSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Lanová	lanový	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
Punkevní	punkevní	k2eAgFnSc2d1
jeskyně	jeskyně	k1gFnSc2
–	–	k?
Macocha	Macocha	k1gFnSc1
</s>
<s>
Punkevní	punkevní	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
</s>
<s>
Koňský	koňský	k2eAgInSc1d1
spád	spád	k1gInSc1
</s>
<s>
Tis	tis	k1gInSc1
u	u	k7c2
Macochy	Macocha	k1gFnSc2
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Absolon	Absolon	k1gMnSc1
<g/>
,	,	kIx,
Moravský	moravský	k2eAgInSc1d1
kras	kras	k1gInSc1
<g/>
,	,	kIx,
vydala	vydat	k5eAaPmAgFnS
Academia	academia	k1gFnSc1
Praha	Praha	k1gFnSc1
roku	rok	k1gInSc2
1970	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
online	onlinout	k5eAaPmIp3nS
</s>
<s>
SOUKOP	SOUKOP	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Nepomuk	Nepomuk	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Macocha	Macocha	k1gFnSc1
a	a	k8xC
její	její	k3xOp3gNnSc1
okolí	okolí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Jan	Jan	k1gMnSc1
Nepomucký	Nepomucký	k1gMnSc1
Soukop	Soukop	k1gInSc1
<g/>
,	,	kIx,
1858	#num#	k4
<g/>
,	,	kIx,
71	#num#	k4
<g/>
,	,	kIx,
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
s.	s.	k?
<g/>
,	,	kIx,
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdigitalizováno	Zdigitalizován	k2eAgNnSc1d1
v	v	k7c6
rámci	rámec	k1gInSc6
služby	služba	k1gFnSc2
Elektronické	elektronický	k2eAgFnSc2d1
knihy	kniha	k1gFnSc2
na	na	k7c4
objednávku	objednávka	k1gFnSc4
(	(	kIx(
<g/>
EOD	EOD	kA
<g/>
)	)	kIx)
Moravskou	moravský	k2eAgFnSc7d1
zemskou	zemský	k2eAgFnSc7d1
knihovnou	knihovna	k1gFnSc7
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnPc4d1
online	onlinout	k5eAaPmIp3nS
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Macocha	Macocha	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Virtuální	virtuální	k2eAgFnSc1d1
prohlídka	prohlídka	k1gFnSc1
Macochy	Macocha	k1gFnSc2
</s>
<s>
Propast	propast	k1gFnSc1
Macocha	Macocha	k1gFnSc1
</s>
<s>
Moravský	moravský	k2eAgInSc1d1
kras	kras	k1gInSc1
a	a	k8xC
propast	propast	k1gFnSc1
Macocha	Macocha	k1gFnSc1
</s>
<s>
Punkevní	punkevní	k2eAgFnSc1d1
jeskyně	jeskyně	k1gFnSc1
a	a	k8xC
propast	propast	k1gFnSc1
Macocha	Macocha	k1gFnSc1
Archivováno	archivován	k2eAgNnSc4d1
13	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ola	ola	k?
<g/>
2013767482	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
242288152	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
|	|	kIx~
Morava	Morava	k1gFnSc1
|	|	kIx~
Příroda	příroda	k1gFnSc1
</s>
