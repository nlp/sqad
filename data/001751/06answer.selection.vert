<s>
Prvním	první	k4xOgInSc7	první
vydaným	vydaný	k2eAgInSc7d1	vydaný
dílem	díl	k1gInSc7	díl
byla	být	k5eAaImAgFnS	být
krátká	krátký	k2eAgFnSc1d1	krátká
povídka	povídka	k1gFnSc1	povídka
The	The	k1gFnSc2	The
Hades	Hadesa	k1gFnPc2	Hadesa
Business	business	k1gInSc1	business
(	(	kIx(	(
<g/>
Kšeft	Kšeft	k?	Kšeft
v	v	k7c6	v
Hádesu	Hádes	k1gInSc6	Hádes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
uveřejněná	uveřejněný	k2eAgFnSc1d1	uveřejněná
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
školním	školní	k2eAgInSc6d1	školní
časopise	časopis	k1gInSc6	časopis
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
13	[number]	k4	13
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
Science	Science	k1gFnSc2	Science
Fantasy	fantas	k1gInPc1	fantas
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
obdržel	obdržet	k5eAaPmAgMnS	obdržet
14	[number]	k4	14
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
