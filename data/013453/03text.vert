<p>
<s>
Pavla	Pavla	k1gFnSc1	Pavla
Lidmilová	Lidmilová	k1gFnSc1	Lidmilová
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1932	[number]	k4	1932
Zlín	Zlín	k1gInSc1	Zlín
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2019	[number]	k4	2019
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
česká	český	k2eAgFnSc1d1	Česká
překladatelka	překladatelka	k1gFnSc1	překladatelka
z	z	k7c2	z
portugalštiny	portugalština	k1gFnSc2	portugalština
a	a	k8xC	a
španělštiny	španělština	k1gFnSc2	španělština
<g/>
.	.	kIx.	.
</s>
<s>
Věnovala	věnovat	k5eAaImAgFnS	věnovat
se	se	k3xPyFc4	se
především	především	k6eAd1	především
literatuře	literatura	k1gFnSc3	literatura
portugalské	portugalský	k2eAgFnSc3d1	portugalská
a	a	k8xC	a
brazilské	brazilský	k2eAgFnSc3d1	brazilská
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
překladům	překlad	k1gInPc3	překlad
z	z	k7c2	z
afrických	africký	k2eAgMnPc2d1	africký
portugalsky	portugalsky	k6eAd1	portugalsky
psaných	psaný	k2eAgFnPc2d1	psaná
literatur	literatura	k1gFnPc2	literatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biografie	biografie	k1gFnSc2	biografie
==	==	k?	==
</s>
</p>
<p>
<s>
Vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
češtinu	čeština	k1gFnSc4	čeština
a	a	k8xC	a
španělštinu	španělština	k1gFnSc4	španělština
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1959	[number]	k4	1959
a	a	k8xC	a
1962	[number]	k4	1962
pracovala	pracovat	k5eAaImAgFnS	pracovat
jako	jako	k9	jako
redaktorka	redaktorka	k1gFnSc1	redaktorka
v	v	k7c6	v
Československém	československý	k2eAgInSc6d1	československý
rozhlasu	rozhlas	k1gInSc6	rozhlas
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
pak	pak	k6eAd1	pak
v	v	k7c6	v
Československé	československý	k2eAgFnSc6d1	Československá
akademii	akademie	k1gFnSc6	akademie
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
v	v	k7c6	v
Kabinetu	kabinet	k1gInSc6	kabinet
pro	pro	k7c4	pro
moderní	moderní	k2eAgFnSc4d1	moderní
filologii	filologie	k1gFnSc4	filologie
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Ústavu	ústav	k1gInSc6	ústav
pro	pro	k7c4	pro
českou	český	k2eAgFnSc4d1	Česká
a	a	k8xC	a
světovou	světový	k2eAgFnSc4d1	světová
literaturu	literatura	k1gFnSc4	literatura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Doktorát	doktorát	k1gInSc1	doktorát
z	z	k7c2	z
filozofie	filozofie	k1gFnSc2	filozofie
získala	získat	k5eAaPmAgFnS	získat
na	na	k7c4	na
FF	ff	kA	ff
UK	UK	kA	UK
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
práci	práce	k1gFnSc4	práce
o	o	k7c6	o
vývoji	vývoj	k1gInSc6	vývoj
brazilské	brazilský	k2eAgFnSc2d1	brazilská
regionální	regionální	k2eAgFnSc2d1	regionální
prózy	próza	k1gFnSc2	próza
a	a	k8xC	a
její	její	k3xOp3gFnSc3	její
proměně	proměna	k1gFnSc3	proměna
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Joã	Joã	k1gMnSc2	Joã
Guimarã	Guimarã	k1gMnSc2	Guimarã
Rosy	Rosa	k1gMnSc2	Rosa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Překlady	překlad	k1gInPc4	překlad
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc4	výběr
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
uvedla	uvést	k5eAaPmAgFnS	uvést
např.	např.	kA	např.
portugalského	portugalský	k2eAgMnSc4d1	portugalský
básníka	básník	k1gMnSc4	básník
a	a	k8xC	a
prozaika	prozaik	k1gMnSc4	prozaik
Fernanda	Fernando	k1gNnSc2	Fernando
Pessou	Pessa	k1gFnSc7	Pessa
(	(	kIx(	(
<g/>
výborem	výbor	k1gInSc7	výbor
Heteronyma	Heteronymum	k1gNnSc2	Heteronymum
[	[	kIx(	[
<g/>
1968	[number]	k4	1968
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Josefem	Josef	k1gMnSc7	Josef
Hiršalem	Hiršal	k1gMnSc7	Hiršal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
překládala	překládat	k5eAaImAgFnS	překládat
básně	báseň	k1gFnPc4	báseň
Luíse	Luíse	k1gFnSc2	Luíse
de	de	k?	de
Camőese	Camőese	k1gFnSc2	Camőese
<g/>
,	,	kIx,	,
Eugénia	Eugénium	k1gNnSc2	Eugénium
de	de	k?	de
Andrade	Andrad	k1gInSc5	Andrad
či	či	k8xC	či
romány	román	k1gInPc1	román
Josého	Josý	k1gMnSc2	Josý
Cardosa	Cardosa	k1gFnSc1	Cardosa
Pirese	Pirese	k1gFnSc1	Pirese
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
brazilskými	brazilský	k2eAgMnPc7d1	brazilský
autory	autor	k1gMnPc7	autor
přeložila	přeložit	k5eAaPmAgFnS	přeložit
kupříkladu	kupříkladu	k6eAd1	kupříkladu
Joã	Joã	k1gMnSc4	Joã
Guimarã	Guimarã	k1gMnSc4	Guimarã
Rosu	Rosa	k1gMnSc4	Rosa
<g/>
,	,	kIx,	,
Murila	Muril	k1gMnSc4	Muril
Rubiã	Rubiã	k1gFnSc2	Rubiã
<g/>
,	,	kIx,	,
Lygiu	Lygium	k1gNnSc6	Lygium
Fagundes	Fagundesa	k1gFnPc2	Fagundesa
Tellesovou	Tellesový	k2eAgFnSc4d1	Tellesová
<g/>
,	,	kIx,	,
Clarice	Clarika	k1gFnSc6	Clarika
Lispectorovou	Lispectorový	k2eAgFnSc4d1	Lispectorový
a	a	k8xC	a
Paula	Paul	k1gMnSc2	Paul
Coelha	Coelh	k1gMnSc2	Coelh
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
jiného	jiné	k1gNnSc2	jiné
sestavila	sestavit	k5eAaPmAgFnS	sestavit
a	a	k8xC	a
přeložila	přeložit	k5eAaPmAgFnS	přeložit
i	i	k9	i
sbírku	sbírka	k1gFnSc4	sbírka
brazilských	brazilský	k2eAgFnPc2d1	brazilská
indiánských	indiánský	k2eAgFnPc2d1	indiánská
bájí	báj	k1gFnPc2	báj
a	a	k8xC	a
pověstí	pověst	k1gFnPc2	pověst
(	(	kIx(	(
<g/>
Poronominare	Poronominar	k1gMnSc5	Poronominar
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
etc	etc	k?	etc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ocenění	ocenění	k1gNnSc1	ocenění
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
jí	on	k3xPp3gFnSc2	on
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc1d1	udělen
portugalský	portugalský	k2eAgInSc1d1	portugalský
Řád	řád	k1gInSc1	řád
Infanta	infant	k1gMnSc2	infant
Jindřicha	Jindřich	k1gMnSc2	Jindřich
komturského	komturský	k2eAgInSc2d1	komturský
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
jí	on	k3xPp3gFnSc2	on
byl	být	k5eAaImAgInS	být
udělen	udělen	k2eAgInSc1d1	udělen
brazilský	brazilský	k2eAgInSc1d1	brazilský
Řád	řád	k1gInSc1	řád
Rio	Rio	k1gFnSc2	Rio
Branca	Branca	k?	Branca
rytířského	rytířský	k2eAgInSc2d1	rytířský
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
jí	on	k3xPp3gFnSc3	on
byla	být	k5eAaImAgFnS	být
udělena	udělit	k5eAaPmNgFnS	udělit
Státní	státní	k2eAgFnSc1d1	státní
cena	cena	k1gFnSc1	cena
za	za	k7c4	za
překladatelské	překladatelský	k2eAgNnSc4d1	překladatelské
dílo	dílo	k1gNnSc4	dílo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Pavla	Pavla	k1gFnSc1	Pavla
Lidmilová	Lidmilový	k2eAgFnSc1d1	Lidmilový
</s>
</p>
<p>
<s>
Heslo	heslo	k1gNnSc1	heslo
Pavla	Pavel	k1gMnSc2	Pavel
Lidmilová	Lidmilová	k1gFnSc1	Lidmilová
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
'	'	kIx"	'
<g/>
KDO	kdo	k3yRnSc1	kdo
BYL	být	k5eAaImAgMnS	být
KDO	kdo	k3yInSc1	kdo
–	–	k?	–
Čeští	český	k2eAgMnPc1d1	český
a	a	k8xC	a
slovenští	slovenský	k2eAgMnPc1d1	slovenský
orientalisté	orientalista	k1gMnPc1	orientalista
<g/>
,	,	kIx,	,
afrikanisté	afrikanista	k1gMnPc1	afrikanista
a	a	k8xC	a
iberoamerikanisté	iberoamerikanista	k1gMnPc1	iberoamerikanista
<g/>
'	'	kIx"	'
</s>
</p>
<p>
<s>
SMRZ	SMRZ	kA	SMRZ
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Traductora	Traductor	k1gMnSc2	Traductor
Pavla	Pavel	k1gMnSc2	Pavel
Lidmilová	Lidmilová	k1gFnSc1	Lidmilová
galardonada	galardonada	k1gFnSc1	galardonada
por	por	k?	por
su	su	k?	su
obra	obr	k1gMnSc2	obr
de	de	k?	de
toda	toda	k1gMnSc1	toda
la	la	k1gNnSc2	la
vida	vida	k?	vida
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Rozhlas	rozhlas	k1gInSc1	rozhlas
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2005-10-27	[number]	k4	2005-10-27
</s>
</p>
