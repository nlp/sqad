<s>
Doktorát	doktorát	k1gInSc1	doktorát
z	z	k7c2	z
filozofie	filozofie	k1gFnSc2	filozofie
získala	získat	k5eAaPmAgFnS	získat
na	na	k7c4	na
FF	ff	kA	ff
UK	UK	kA	UK
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
práci	práce	k1gFnSc4	práce
o	o	k7c6	o
vývoji	vývoj	k1gInSc6	vývoj
brazilské	brazilský	k2eAgFnSc2d1	brazilská
regionální	regionální	k2eAgFnSc2d1	regionální
prózy	próza	k1gFnSc2	próza
a	a	k8xC	a
její	její	k3xOp3gFnSc3	její
proměně	proměna	k1gFnSc3	proměna
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Joã	Joã	k1gMnSc2	Joã
Guimarã	Guimarã	k1gMnSc2	Guimarã
Rosy	Rosa	k1gMnSc2	Rosa
<g/>
.	.	kIx.	.
</s>
