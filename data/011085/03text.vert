<p>
<s>
Dánsko	Dánsko	k1gNnSc1	Dánsko
se	se	k3xPyFc4	se
účastnilo	účastnit	k5eAaImAgNnS	účastnit
Letní	letní	k2eAgNnSc1d1	letní
olympiády	olympiáda	k1gFnSc2	olympiáda
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Zastupovalo	zastupovat	k5eAaImAgNnS	zastupovat
ho	on	k3xPp3gNnSc4	on
162	[number]	k4	162
sportovců	sportovec	k1gMnPc2	sportovec
(	(	kIx(	(
<g/>
144	[number]	k4	144
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
18	[number]	k4	18
žen	žena	k1gFnPc2	žena
<g/>
)	)	kIx)	)
v	v	k7c6	v
17	[number]	k4	17
sportech	sport	k1gInPc6	sport
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Medailisté	medailista	k1gMnPc5	medailista
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Dánsko	Dánsko	k1gNnSc1	Dánsko
na	na	k7c4	na
LOH	LOH	kA	LOH
1948	[number]	k4	1948
</s>
</p>
