<p>
<s>
Mazut	mazut	k1gInSc1	mazut
je	být	k5eAaImIp3nS	být
neodpařený	odpařený	k2eNgInSc1d1	odpařený
zbytek	zbytek	k1gInSc1	zbytek
při	při	k7c6	při
frakční	frakční	k2eAgFnSc6d1	frakční
destilaci	destilace	k1gFnSc6	destilace
ropy	ropa	k1gFnSc2	ropa
za	za	k7c2	za
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
destiluje	destilovat	k5eAaImIp3nS	destilovat
za	za	k7c2	za
sníženého	snížený	k2eAgInSc2d1	snížený
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vakuová	vakuový	k2eAgFnSc1d1	vakuová
destilace	destilace	k1gFnSc1	destilace
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
oddělit	oddělit	k5eAaPmF	oddělit
složky	složka	k1gFnPc4	složka
směsi	směs	k1gFnPc4	směs
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nestabilní	stabilní	k2eNgMnPc4d1	nestabilní
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
varu	var	k1gInSc2	var
za	za	k7c2	za
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
snížením	snížení	k1gNnSc7	snížení
tlaku	tlak	k1gInSc2	tlak
se	se	k3xPyFc4	se
teploty	teplota	k1gFnSc2	teplota
varu	var	k1gInSc2	var
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
složek	složka	k1gFnPc2	složka
výrazně	výrazně	k6eAd1	výrazně
snižují	snižovat	k5eAaImIp3nP	snižovat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
oddělit	oddělit	k5eAaPmF	oddělit
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
složky	složka	k1gFnPc4	složka
bez	bez	k7c2	bez
jejich	jejich	k3xOp3gFnSc2	jejich
tepelné	tepelný	k2eAgFnSc2d1	tepelná
degradace	degradace	k1gFnSc2	degradace
<g/>
.	.	kIx.	.
</s>
<s>
Vakuovou	vakuový	k2eAgFnSc7d1	vakuová
destilací	destilace	k1gFnSc7	destilace
se	se	k3xPyFc4	se
z	z	k7c2	z
mazutu	mazut	k1gInSc2	mazut
získávají	získávat	k5eAaImIp3nP	získávat
především	především	k9	především
minerální	minerální	k2eAgInPc1d1	minerální
oleje	olej	k1gInPc1	olej
a	a	k8xC	a
asfalt	asfalt	k1gInSc1	asfalt
<g/>
.	.	kIx.	.
</s>
<s>
Mazut	mazut	k1gInSc1	mazut
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
palivo	palivo	k1gNnSc4	palivo
v	v	k7c6	v
tepelných	tepelný	k2eAgFnPc6d1	tepelná
elektrárnách	elektrárna	k1gFnPc6	elektrárna
<g/>
,	,	kIx,	,
lodích	loď	k1gFnPc6	loď
a	a	k8xC	a
parních	parní	k2eAgFnPc6d1	parní
lokomotivách	lokomotiva	k1gFnPc6	lokomotiva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Oleje	olej	k1gInPc1	olej
==	==	k?	==
</s>
</p>
<p>
<s>
Oleje	olej	k1gInPc1	olej
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
ropné	ropný	k2eAgInPc1d1	ropný
minerální	minerální	k2eAgInPc1d1	minerální
oleje	olej	k1gInPc1	olej
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jako	jako	k9	jako
mazadla	mazadlo	k1gNnPc4	mazadlo
olejových	olejový	k2eAgInPc2d1	olejový
strojů	stroj	k1gInPc2	stroj
a	a	k8xC	a
technických	technický	k2eAgNnPc2d1	technické
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
se	se	k3xPyFc4	se
též	též	k9	též
jako	jako	k9	jako
ochrana	ochrana	k1gFnSc1	ochrana
kovů	kov	k1gInPc2	kov
před	před	k7c7	před
korozí	koroze	k1gFnSc7	koroze
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
olejů	olej	k1gInPc2	olej
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
získává	získávat	k5eAaImIp3nS	získávat
parafín	parafín	k1gInSc1	parafín
a	a	k8xC	a
vazelína	vazelína	k1gFnSc1	vazelína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Asfalt	asfalt	k1gInSc4	asfalt
==	==	k?	==
</s>
</p>
<p>
<s>
Asfalt	asfalt	k1gInSc1	asfalt
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
zejména	zejména	k9	zejména
k	k	k7c3	k
úpravě	úprava	k1gFnSc3	úprava
povrchu	povrch	k1gInSc2	povrch
silnic	silnice	k1gFnPc2	silnice
a	a	k8xC	a
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
jako	jako	k8xS	jako
izolační	izolační	k2eAgInSc1d1	izolační
materiál	materiál	k1gInSc1	materiál
proti	proti	k7c3	proti
vlhkosti	vlhkost	k1gFnSc3	vlhkost
<g/>
.	.	kIx.	.
</s>
</p>
