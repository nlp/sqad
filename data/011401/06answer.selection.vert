<s>
Mazut	mazut	k1gInSc1	mazut
je	být	k5eAaImIp3nS	být
neodpařený	odpařený	k2eNgInSc1d1	odpařený
zbytek	zbytek	k1gInSc1	zbytek
při	při	k7c6	při
frakční	frakční	k2eAgFnSc6d1	frakční
destilaci	destilace	k1gFnSc6	destilace
ropy	ropa	k1gFnSc2	ropa
za	za	k7c2	za
atmosférického	atmosférický	k2eAgInSc2d1	atmosférický
tlaku	tlak	k1gInSc2	tlak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
destiluje	destilovat	k5eAaImIp3nS	destilovat
za	za	k7c2	za
sníženého	snížený	k2eAgInSc2d1	snížený
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
